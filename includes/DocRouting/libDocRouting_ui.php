<?php
// editing by 

/*****************************************************************************
 * Modification Log:
 * Date:    2020-04-24 Tommy: modifed getDocRoutingEditForm(), add pastRecordStatus for checking it is draft rounting in edit_doc_route_update.php
 * Date:	2019-11-15 Sam: Bulk delete routings
 *                          modified getRoutingDisplayTable()
 *                          added getDeleteDocumentsStep1DisplayTable() and getDeleteDocumentsStep2DisplayTable()   
 * Date:	2019-04-29 Henry: Cross Site Scripting handling
 * Date:	2019-01-11 Carlos: modified getCurrentRoutingDisplayItem() and getCurrentRoutingPrintItem(), batch get data to improve performance, avoid many queries run in loops.
 * Date:	2018-03-28 Carlos: modified getRoutingDisplayTable(), getDocDetailForm(), getRoutingDetailBlock(), getDocumentDetailPrintPage(), getAdvanceSearchDiv(), cater deleted routings.
 * Date:	2017-05-16 Carlos: $sys_custom['DocRouting']['RequestNumber'] - modified getCurrentRoutingDisplayItem(), getDocDetailForm(), getCurrentRoutingPrintItem(), display DocumentID as [Request Number].
 * 					  		   $sys_custom['DocRouting']['DeleteFeedback'] - modified getRoutingDetailBlock(), added delete feedback button.
 * Date:	2017-01-16 (Carlos) [ip2.5.8.1.1] - fixed getDocumentDetailPrintPage() show reply slip users answers if settings allow show it or current user is module admin or document creator or the feedback creator.
 * Date:	2016-03-23 (Carlos) [ip2.5.7.4.1] - modified getCopyDocumentsStep1DisplayTable(), admin can copy all routings, non-admin can only copy involved routings.
 * Date:	2016-02-19 (Carlos) [ip2.5.7.3.1] - modified getCurrentRoutingDisplayItem(), getCurrentRoutingPrintItem(), getDocDetailForm($DocumentID), getAdvanceSearchDiv(), added getAcademicYearSelection(), to handle Followed routings page.  
 * Date:	2015-11-30 (Carlos) [ip2.5.7.1.1] - modified getDocDetailForm() and getRoutingDetailBlock(), added last modified time besides the creator name.
 * Date: 	2015-05-04 (Carlos) [ip2.5.6.5.1] - modified addNewRouting(), added &FromDR=1 to /home/common_choose/index.php
 * Date:	2015-04-24 (Carlos) [ip2.5.6.5.1] - modified getDocDetailForm(), added change password button for independent signing page and require flag $sys_custom['DocRouting']['RequirePasswordToSign']. 
 * Date:	2015-03-24 (Carlos) [ip2.5.6.5.1] - modified getDocDetailForm() admin user can complete/re-open routings, not necessary the creator.
 * Date:	2015-01-05 (Carlos) [ip2.5.6.1.1] - modified addNewRouting() and getRoutingDetailBlock(), handle INTRANET_DR_ROUTES.ReadOnlyUserViewDetail which enable read-only users can view all comments and attachments
 * Date:	2014-12-03 (Carlos) [ip2.5.5.12.1] - modified getDocumentDetailPrintPage(), added getSendArchivedDocumentEmailForm() and getArchivedDocument() to send archived document via emails
 * Date:	2014-11-21 (Ivan) [ip2.5.5.12.1] - 	modified getDocumentDetailPrintPage() hide wording "instruction" for route info, show reply slip statistics for each routings
 * 												modified getRoutingDetailBlock() default show statistics block if statistics is available and (user has signed the reply slip or user is not related to the routing)
 * Date:	2014-11-20 (Carlos) [ip2.5.5.12.1] - Modified getDocumentDetailPrintPage(), added get signer's feedbacks mode
 * Date:	2014-11-19 (Carlos) [ip2.5.5.12.1] - Modified getRoutingDetailBlock(), only doc or route creator can send notification email
 * Date:	2014-11-18 (Carlos) [ip2.5.5.12.1] - Modified getDocEditForm(), added getDocumentDetailPrintPage() for print version and archive document
 * 
 * Date:	2014-11-05 (Carlos) [ip2.5.5.12.1] - Sign by token email
 * 			modified getCurrentRoutingDisplayItem(), getDocDetailForm(), getRoutingDetailBlock(),getDocEditForm(), added getSendSignEmailForm()
 * 
 * Date:	2014-10-23 (Carlos) [ip2.5.5.10.1]
 * 			modified getRoutingDisplayTable() hide star status selection filter. Sql query do not use the star status filter, only for sorting and put stared to the top.
 * 
 * Date:	2014-07-15 (Henry)
 * 			added getCurrentRoutingPrintItem() and getRoutingPrintTable()
 *
 * Date:	2014-05-22 (Carlos)
 * 			modified getPresetDisplayTable() - add back the flag $sys_custom['PrintRoutingInstruction'] checking on print function
 * 
 * Date:	2014-05-13 (Carlos)
 * 			modified getRoutingDetailBlock(), getDocDetailForm(), added getStarredFilesDiv()
 * 
 * Date:	2014-03-14 (Ivan) [2014-0220-1638-49184]
 * 			modified getCurrentRoutingDisplayItem(), in process routing with any not completed users => yellow background
 * 
 * Date:    2014-02-19 (Tiffany)
 *          add from digital archive, modified getDocDetailForm(),getDocEditForm()
 * 
 * Date:    2014-02-11 (Henry)
 *          added DisplayInternalRecipientGroup=1 in the querystring of new common choose window
 * 
 * Date:    2014-02-07 (Tiffany)
 *          modified js_Show_Hide_Comment();
 * 
 * Date:	2013-12-17 (Henry)
 *  		show routing Publish Date by default (case: 2013-1209-1036-20071)
 * 
 * Date:	2013-10-07 (Henry)
 * 			Modified cancel button javascript from "window.location.href='index.php?pe=".$cancel_param."'" to javascript:history.back() in getDocEditForm()
 * 			Modified cancel button javascript from "window.location.href='index.php?pe=".$cancel_param."'" to javascript:history.go(-2) in getDocRoutingEditForm()
 * 
 * Date:	2013-10-07 (Henry)
 * 			Add parameter 'comment' in getRoutingUploadAttachmentForm
 * 
 * Date:	2013-10-04 (Henry)
 * 			Fixed bugs for TO and Read-only user: first load and copy cannot display selected options
 * 
 * Date:	2013-10-03 (Henry)
 * 			added getAdvanceSearchDiv()
 * 			added parameter $advanceSearchArray in getCurrentRoutingDisplayItem()
 * 
 * Date: 	2013-09-30 (Henry)
 * 			modified addNewRouting() to support show/hide the user selection in "To" and "View only user" column
 * 			modified addNewRouting() to disable the selection('Anytime' and 'After submission') of Release result when 'Hide user name' is selected
 * 			added  Warning Msg Box in function getRoutingDetailBlock
 * 			modified getCurrentRoutingDisplayItem() to support show/hide the details information of routings
 * 
 * Date: 	2013-09-27 (Henry)
 * 			modified getDocEditForm() to support show/hide the instruction
 * 			modified addNewRouting() to support show/hide the instruction and the start time and end time
 * 
 * Date: 	2013-06-18 (Rita)
 * 			modified getRoutingDetailBlock(),getCurrentRoutingDisplayItem()
 * 
 * Date:	2013-06-17 (Carlos)
 * 			modified addNewRouting(), exchange position of reply slip [Release result:] and [User name display:]. And if select [Hide user name], auto check [After route compeleted]
 * 
 * Date:	2013-06-14 (Ivan)
 * 			modified addNewRouting() to change the display presentation of view-only user option
 * 
 * Date:	2013-05-22 (Rita)
 * 			add print instruction btn in getPresetDisplayTable()
 * 
 * Date:	2013-05-21 (Ivan)
 * 			modified addNewRouting() to hide alumni in the coomon choose pop up
 * 
 * Date:	2013-05-08 (Rita)
 * 			modified addNewRouting() change AllowAdHocRoute field default as 'enable'
 * 
 * Date:	2013-04-16 (Carlos)
 * 			modified getDocDetailForm(), always show Complete button for creator
 * 			modified getRoutingDisplayTable() and getCurrentRoutingDisplayItem(), add filter expireStatus for current routings
 * 
 * Date:	2013-03-19 (Rita)
 * 			amend addNewRouting() add checking for show copy route btn
 * 	
 * Date:	2013-03-05 (Carlos)
 * 			updated getRoutingDetailBlock() drafted comment css
 * 
 * Date:	2013-03-01 (Carlos)
 * 			modified addNewRouting() - cater table reply slip
 * 
 * Date:    2013-02-20 (Rita)
 * 			modified getCopyDocumentsStep1DisplayTable(), getPrintInstructionStep1(), getPrintInstructionStep2()
 * 
 * Date:	2013-02-01 (Rita)
 * 			add getPrintInstructionStep1(), getPrintInstructionStep2()
 * 
 * Date:	2013-01-21(Rita)
 * 			modified getUserPresetSelection(), add class attribute
 * 
 * Date: 	2013-01-16 (Rita)
 * 			modified addNewRouting(); add copy route layer , getDocRoutingEditForm()
 * 
 * Date: 	2013-01-09 (Rita)
 * 			add getCopyDocumentsStep1DisplayTable(), getCopyDocumentsStep2DisplayTable();
 * 
 * Date: 	2013-01-09 (Rita)
 * 			change addNewRouting() add release option;
 * 
 * Date:	2013-01-08 (Rita)
 * Details:	change commment textarea as fckEditor in getRoutingDetailBlock(), 
 * 			release online form statistic control getRoutingDetailBlock();
 * 
 * 
 *****************************************************************************/

if (!defined("LIBDOCROUTING_UI_DEFINED")) {
	define("LIBDOCROUTING_UI_DEFINED", true);
	
	class libDocRouting_ui extends interface_html {
		function libDocRouting_ui($custTemplate='') {
			$template = ($custTemplate=='')? 'default.html' : $custTemplate;
			$this->interface_html($template);
		}
		
		function returnModuleCss() {
			global $PATH_WRT_ROOT, $LAYOUT_SKIN;
			
			return '<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/css/dr.css" type="text/css" />';
		}
		
		function returnModuleJs() {
			global $PATH_WRT_ROOT, $LAYOUT_SKIN, $docRoutingConfig, $indexVar;
			
			ob_start();
				include_once('templates/commonJs.php');
				$jsContent = ob_get_contents();
			ob_end_clean();
			
			return $jsContent;
		}
		
		function echoModuleLayoutStart($curPageCode, $returnMsg='', $forPopup=false) {
			global $indexVar, $CurrentPage, $MODULE_OBJ, $TAGS_OBJ, $CurrentPageArr, $PATH_WRT_ROOT, $intranet_session_language;
			
			$CurrentPage = $curPageCode;
			$MODULE_OBJ = $indexVar['libDocRouting']->getModuleObjArr();
			$CurrentPageArr['DocRouting'] = 1;
			
			if ($forPopup) {
				$this->interface_html('popup.html');
			}
			
			$this->LAYOUT_START($returnMsg);
			echo $this->returnModuleCss();
			echo $this->returnModuleJs();
			echo $this->Include_ReplySlip_JS_CSS();
			echo $this->Include_TableReplySlip_JS_CSS();
			echo '<!-- task: '.$indexVar['taskScript'].'-->';
			echo '<!-- template: '.$indexVar['templateScript'].'-->';
		}
		
		function echoModuleLayoutStop() {
			$this->LAYOUT_STOP();
		}
		
		function echoReplySlipJsCss() {
			global $PATH_WRT_ROOT, $intranet_session_language;
			echo $this->Include_ReplySlip_JS_CSS();
			echo $this->Include_TableReplySlip_JS_CSS();
		}
		
		function getDocEditForm($dataAry)
		{
			global $Lang, $PATH_WRT_ROOT, $xmsg, $indexVar, $docRoutingConfig;
			
			$lfilesystem = $indexVar['libfilesystem'];
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			
			$documentID = $dataAry['DocumentID'];
			$pageType = $dataAry['PageType'];
			$title = $dataAry['Title'];
			$instruction = $dataAry['Instruction'];
			$documentType = $dataAry['DocumentType'];
			$physicalLocation = $dataAry['PhysicalLocation'];
			//$docTags = $dataAry['DocTags'];
			$allowAdHocRoute = $dataAry['AllowAdHocRoute'];
			//$documentUploaded = $dataAry['DocumentUploaded'];
			$recordStatus = $dataAry['RecordStatus'];
			$tmpRecordStatus = $dataAry['tmpRecordStatus'];
			
			if($documentID != ''){
				$docRecord = $ldocrouting->getEntryData(array($documentID));
				$title = $docRecord[0]['Title'];
				$instruction = $docRecord[0]['Instruction'];
				$tagNames = $ldocrouting->getEntryTagName($documentID);
				if(count($tagNames) > 0){
					$docTags = implode(", ",$tagNames);
				}
				
				$docStatusRecord = $ldocrouting->getDocRoutingUserStatusCount($documentID);
			}
			
			if($documentID != '' && $tmpRecordStatus != $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp']) {
				// edit old entry
				if(!isset($docRecord)){
					$docRecord = $ldocrouting->getEntryData(array($documentID));
				}
				//$documentID = $docRecord[0]['DocumentID'];
				$title = $docRecord[0]['Title'];
				$instruction = $docRecord[0]['Instruction'];
				$documentType = $docRecord[0]['DocumentType'];
				$physicalLocation = $docRecord[0]['PhysicalLocation'];
				//$docTags = $docRecord[0]['DocTags'];
				$allowAdHocRoute = $docRecord[0]['AllowAdHocRoute'];
				$recordStatus = $docRecord[0]['RecordStatus'];
				
				if(!isset($tagNames)){
					$tagNames = $ldocrouting->getEntryTagName($documentID);
					if(count($tagNames) > 0){
						$docTags = implode(", ",$tagNames);
					}
				}
				$navigationText = $Lang['DocRouting']['EditDocRouting'];
			}else{
				$tmpRecordStatus = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp']; // new record,
				
				$navigationText = $Lang['DocRouting']['NewDocRouting']; 
			}
			
			if($documentID != '' && $documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']){
				$existing_attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], array($documentID));
			}
			if($documentID != '' && $documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA']){
				$existing_attachmentsDA = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], array($documentID));
			}
			# step information
			$STEPS_OBJ[] = array($Lang['DocRouting']['NewDocRoutingStep1'], 1);
			$STEPS_OBJ[] = array($Lang['DocRouting']['NewDocRoutingStep2'], 0);
			
			$PAGE_NAVIGATION[] = array($navigationText, "");
			
			$form_action = $ldocrouting->getEncryptedParameter('management','edit_doc_route',array());
			if ($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']) {
				$cancelPage = 'draft_routings';
			}
			else {
				$cancelPage = 'index';
			}
			$cancel_param = $ldocrouting->getEncryptedParameter('management',$cancelPage,array());
			
			# Check If Check allowAdHocRoute Field 
			$isCheckAllowAdHocRoute = ''; //default checked			
			if(!isset($allowAdHocRoute) || $allowAdHocRoute==1)
			{
				$isCheckAllowAdHocRoute = 'checked';		
			}else
			{
				$isCheckAllowAdHocRoute = '';
			}	
			
			$x = '<form name="form1" method="post" action="index.php?pe='.$form_action.'" onsubmit="js_Check_Submit_Form(this);return false;" enctype="multipart/form-data">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>'.$this->GET_NAVIGATION($PAGE_NAVIGATION).'<br /><br /></td>
							<td align="right">'.$xmsg.'</td>
						</tr>
						<tr>
							<td height="40" colspan="2">'.$this->GET_STEPS($STEPS_OBJ).'</td>
						</tr>
						<tr>
						<td class="board_menu_closed">
					        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					        	<tr>
					        		<td class="main_content">
					        			<div class="table_board">
					        				<table class="form_table_v30" width="90%" cellpadding="4" cellspacing="0" border="0" align="center">
									            <tr>
									                <td width="20%" class="field_title"><span class="tabletextrequire">*</span>'.$Lang['DocRouting']['Title'].'</td>
													<td><input id="Title" name="Title" type="text" class="textboxtext" maxlength="128" value="'.htmlspecialchars($title,ENT_QUOTES).'" />'.
														$this->Get_Form_Warning_Msg("TitleWarningDiv",$Lang['General']['PleaseFillIn']." ".$Lang['DocRouting']['Title'], "WarnMsgDiv").'</td>
									            </tr>
									            <tr>
									            	<td width="20%" class="field_title"><span class="tabletextrequire">*</span>'.$Lang['DocRouting']['Instruction'].'</td>
									            	<td>';
									            	//Henry Adding [Start]
									            	$divName = 'InstructionDiv';
									            	$x .= '<span id="spanShowOption_'.$divName.'" style="display:none">'."\n";
													$x .= $this->Get_Show_Option_Link("javascript:js_Show_Option_Div('$divName');","",$Lang['Btn']['Show']);
													$x .= '</span>'."\n";
													$x .= '<span id="spanHideOption_'.$divName.'" ">'."\n";
													$x .= $this->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('$divName');","",$Lang['Btn']['Hide']);
													$x .= '</span>'."\n";
													$x .= '<div id="'.$divName.'">'."\n";
													//Henry Adding [End]
													$x .= $this->getUserPresetSelection($docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['DocumentInstruction'], $indexVar['drUserId'], 'PresetNotes', 'PresetNotes', 'js_Select_PresetNotes();');
													$x .= '<br />';
									            	include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
													//$objHtmlEditor = new FCKeditor ( 'Instruction' , "100%", "320", "", "Basic2", intranet_undo_htmlspecialchars($instruction));
													//$objHtmlEditor = new FCKeditor ( 'Instruction' , "100%", "160", "", "Basic2", $instruction);
													//$objHtmlEditor = new FCKeditor ( 'Instruction' , "100%", "220", "", "Basic2", $instruction);
													$objHtmlEditor = new FCKeditor ( 'Instruction' , "100%", "450", "", "Basic2", $instruction);
													//$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['DocRouting'], $id);
													$x .= $objHtmlEditor->Create2();
													//$x .= $this->Get_Form_Warning_Msg("InstructionWarningDiv",$Lang['General']['PleaseFillIn']." ".$Lang['General']['Instruction'], "WarnMsgDiv");
											//$x .= '<br />'.$this->GET_SELECTION_BOX($Lang['DocRouting']['PresetNotes'], 'id="PresetNotes" name="PresetNotes" onchange="js_Select_PresetNotes();" ', $Lang['DocRouting']['PresetNotesSelection']).'
									   				$x .="</div>";  //Henry Adding	
									   				$x .= $this->Get_Form_Warning_Msg("InstructionWarningDiv",$Lang['General']['PleaseFillIn']." ".$Lang['General']['Instruction'], "WarnMsgDiv");
									          $x .= '</td>
									            </tr>';
									       
									    if($documentID != '' && $docStatusRecord[0]['CompletedTotal']>0){
									    	// Routing has been signed by one or more users, do not allow change attachments
									    	$hide_attachment = true;
									    	$x.='<tr>
									            	<td width="20%" class="field_title">'.$Lang['DocRouting']['Documents'].'</td>';
									    	  $x.= '<td>';
									    	  		if($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']){
									    	  			$x.= $Lang['DocRouting']['DocumentUpload'];
														for($i=0;$i<count($existing_attachments);$i++){
															$filename = $existing_attachments[$i]['FileName'];
															$fileid = $existing_attachments[$i]['FileID'];
															$version_num = $existing_attachments[$i]['VersionNumber'];
															$display_version = '';
															$file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],'LinkToID'=>$documentID,'FileID'=>$fileid));
															$x .= '<div><a href="index.php?pe='.$file_param.'" target="_blank" style="float:left">'.$filename.$display_version.'</a></div>';
														}
									    	  		}else if($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA']){
									    	  			$x .= $Lang['DocRouting']['DocumentDA'];
									    	  			for($i=0;$i<count($existing_attachmentsDA);$i++){
															$filename = $existing_attachmentsDA[$i]['FileName'];
															$fileid = $existing_attachmentsDA[$i]['FileID'];
															$version_num = $existing_attachmentsDA[$i]['VersionNumber'];																
															$display_version = '';																
															$file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],'LinkToID'=>$documentID,'FileID'=>$fileid));
															$x .= '<div><a href="index.php?pe='.$file_param.'" target="_blank" style="float:left">'.$filename.$display_version.'</a></div>';
														} 
									    	  		}else if($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location']){
									    	  			$x.=$Lang['DocRouting']['DocumentPhysical'];
									    	  			$x.='<div>'.$physicalLocation.'</div>';
									    	  		}else if($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['NA']){
									    	  			$x.=$Lang['DocRouting']['DocumentNA'];
									    	  		}
									    	  		$x .= '<div class="tabletextremark" style="clear:both;">'.$Lang['DocRouting']['AttachmentRemark'].'</div>';
									    	  $x .= '</td>';
									    	  $x.= '</tr>';
									    }
									    	$x.='<tr'.($hide_attachment?' style="display:none;"':'').'>
									            	<td width="20%" class="field_title">'.$Lang['DocRouting']['Documents'].'</td>';       	
									           $x.='<td>
												
														<input type="radio" name="DocumentType" id="DocumentType1" onclick="js_Select_DocumentType();" value="'.$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File'].'" '.($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']?'checked':'').' /><label for="DocumentType1"> '.$Lang['DocRouting']['DocumentUpload'].'</label> &nbsp;';
												        global $intranet_root,$intranet_session_language;
												        include_once($PATH_WRT_ROOT.'includes/libdigitalarchive_moduleupload.php');
												        $libdamu = new libdigitalarchive_moduleupload();
												        if($libdamu->User_Can_Archive_File()){
												   $x.='<input type="radio" name="DocumentType" id="DocumentType4" onclick="js_Select_DocumentType();" value="'.$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA'].'" '.($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA']?'checked':'').' /><label for="DocumentType4"> '.$Lang['DocRouting']['DocumentDA'].'</label> &nbsp;';					            		
												        }
                                                   $x.='<input type="radio" name="DocumentType" id="DocumentType2" onclick="js_Select_DocumentType();js_Set_Input_Width();" value="'.$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location'].'" '.($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location']?'checked':'').' /><label for="DocumentType2"> '.$Lang['DocRouting']['DocumentPhysical'].'</label> &nbsp;
									            		<input type="radio" name="DocumentType" id="DocumentType3" onclick="js_Select_DocumentType();" value="'.$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['NA'].'" '.(($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['NA'] || $documentType=='')?'checked':'').' /><label for="DocumentType3"> '.$Lang['DocRouting']['DocumentNA'].'</label> &nbsp;
									            		<!-- Henry Modifying -->

		                                                <div id="DigitalArchiveDiv" '.($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA']?'':'style="display:none"').'>';                                                
                                                        for($i=0;$i<count($existing_attachmentsDA);$i++){
																$filename = $existing_attachmentsDA[$i]['FileName'];
																$fileid = $existing_attachmentsDA[$i]['FileID'];
																$version_num = $existing_attachmentsDA[$i]['VersionNumber'];																
																$display_version = '';																
																$file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],'LinkToID'=>$documentID,'FileID'=>$fileid));
																$x .= '<div><a href="index.php?pe='.$file_param.'" target="_blank" style="float:left">'.$filename.$display_version.'</a><span class="table_row_tool"><a href="javascript:void(0);" class="delete_dim" onclick="js_Delete_File(this,'.$fileid.');" title="'.$Lang['Btn']['Delete'].'"></a></span><br style="clear:both"/></div>';
																$x .= '<input type="hidden" name="FileID_DA[]" value="'.$fileid.'" />';
															} 
                                                      $x.='<p><a href="#TB_inline?height=480&width=600&inlineId=DigitalArchiveDiv1" title="'.$Lang['DocRouting']['ChooseFile'].'" class="thickbox" type="button" >'.$Lang['DocRouting']['ChooseFile'].'</a></p>
															<div class="tabletextremark" style="clear:both;">'.$Lang['DocRouting']['AttachmentRemark'].'</div>
														 </div>
                                                     	   
                                                           <div id="DigitalArchiveDiv1" style="display:none"><div style="height:400px;overflow-x:auto; overflow-y:auto">';
                                                        
                                                           include_once($PATH_WRT_ROOT.'includes/libdigitalarchive.php');
                                                           $libda = new libdigitalarchive();
                                                           $myGroupList = $libda->GetMyGroup();
                                                           $folderID=0;                                                        
                                                           for($i=0;$i<sizeof( $myGroupList);$i++){
                                                              //$x.='<tr id="tr_'.$i.'"><td><a href="javascript:js_New_Level('.$i.');" id="'.$i.'" >+</a></td><td align="left">'.$myGroupList[$i][1].'</td></tr>';                                                      
                                                                $x.='<p id="pg_'.$myGroupList[$i][0].'"><a href="javascript:js_New_Level('.$myGroupList[$i][0].','.$folderID.');" id="'.$myGroupList[$i][0].'" >&nbsp;&nbsp;+&nbsp;&nbsp;</a>'.$myGroupList[$i][1].'</p>';
                                                               }
                                                       
                                                  $x .='</div><div class="edit_bottom" style="height:10px;">
                                                        <p class="spacer"></p>
			                                            <input name="submit_button" type="button" class="formbutton" onclick="js_show_file_DA();" onmouseover="this.className="formbuttonon"" onmouseout="this.className="formbutton"" value="'.$Lang['Btn']['Submit'].'" />
			                                            <input name="cancel_button" type="button" class="formbutton" onclick="tb_remove();" onmouseover="this.className="formbuttonon"" onmouseout="this.className="formbutton"" value="'.$Lang['Btn']['Cancel'].'" />
                                                        <p class="spacer"></p>
                                                        </div></div>


														<div id="PhysicalLocationDiv" '.($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location']?'':'style="display:none"').'>					
															'.$this->GET_TEXTBOX('PhysicalLocationTb', 'PhysicalLocation', $physicalLocation, $OtherClass='inputselect', array('maxlength' => $docRoutingConfig['maxLength']['RoutePhysicalLocation'])).'
															'.$this->Get_Form_Warning_Msg("PhysicalLocationWarningDiv",$Lang['General']['PleaseFillIn']." ".$Lang['DocRouting']['PhysicalLocation'], "WarnMsgDiv").'
														</div>
														
														<div id="DocUploadDiv" '.($documentType==$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']?'':'style="display:none"').'>';
															for($i=0;$i<count($existing_attachments);$i++){
																$filename = $existing_attachments[$i]['FileName'];
																$fileid = $existing_attachments[$i]['FileID'];
																$version_num = $existing_attachments[$i]['VersionNumber'];
																//if($existing_attachments[$i+1]['FileName'] == $filename || $version_num > 1) {
																//	$display_version = '&nbsp;('.$Lang['DocRouting']['Version'].'&nbsp;'.$version_num.')';
																//}else{
																	$display_version = '';
																//}
																$file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],'LinkToID'=>$documentID,'FileID'=>$fileid));
																$x .= '<div><a href="index.php?pe='.$file_param.'" target="_blank" style="float:left">'.$filename.$display_version.'</a><span class="table_row_tool"><a href="javascript:void(0);" class="delete_dim" onclick="js_Delete_File(this,'.$fileid.');" title="'.$Lang['Btn']['Delete'].'"></a></span><br style="clear:both"/></div>';
																$x .= '<input type="hidden" name="FileID[]" value="'.$fileid.'" />';
															}
													
															$x .= '<div><input type="file" name="DocumentUploaded_0" size="80" /></div>';
															//$x .= '<div><a href="javascript:void(0);" onclick="js_Add_More_Upload(this);" title="'.$Lang['AddMore'].'">[ + ]</a><div>';
															//$x .= '<br style="clear:both;" />';
															$x .= '<div class="table_row_tool row_content_tool" style="float:left;">
																		<a class="add_dim" title="'.$Lang['Btn']['AddMore'].'" onclick="js_Add_More_Upload(this);" href="javascript:void(0);"></a>
																	</div>';
															$x .= '<div class="tabletextremark" style="clear:both;">'.$Lang['DocRouting']['AttachmentRemark'].'</div>';
													$x .= '</div>
									            	</td>
												</tr>';
									       $x.='<tr>
													<td width="20%" class="field_title">'.$Lang['DocRouting']['AllowAdHocRoute'].'</td>

													<td><input type="checkbox" id="AllowAdHocRoute" name="AllowAdHocRoute" value="1" '.$isCheckAllowAdHocRoute.' /></td>
												</tr>
									            <tr>
									                <td width="20%" class="field_title">'.$Lang['DocRouting']['Tags'].'</td>
													<td>
														<input id="DocTags" name="DocTags" type="text" class="textboxtext" maxlength="255" value="'.htmlspecialchars($docTags,ENT_QUOTES).'"  />
														<span class="tabletextremark">'.$this->getTagInputRemarks().'</span>
													';
														$x .= $this->Get_Form_Warning_Msg("TagsWarningDiv", $Lang['DocRouting']['WarningMsg']['MoreThanAllowedTags'], "WarnMsgDiv");
										$x .= '		</td>
									            </tr>
												<!--tr>
													<td width="20%" class="field_title">'.$Lang['DocRouting']['Status'].'</td>
													<td>
														<input type="radio" id="RecordStatusActive" name="RecordStatus" value="'.$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active'].'" '.($recordStatus==$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']?'checked':'').' /><label for="RecordStatusActive">'.$Lang['DocRouting']['Active'].'</label>&nbsp;
														<input type="radio" id="RecordStatusDraft" name="RecordStatus" value="'.$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft'].'" '.(($recordStatus==$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft'] || $recordStatus=='')?'checked':'').' /><label for="RecordStatusDraft">'.$Lang['DocRouting']['Draft'].'</label>
													</td>
												</tr-->
									        </table>
											<p class="spacer"></p>
										</div>
										'.$this->MandatoryField().'
										<div class="edit_bottom_v30">
					                        <p class="spacer"></p>
					                        '.$this->GET_ACTION_BTN($Lang['Btn']['Next'], "submit", "", "btnNext").'
					                        '.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location.href='index.php?pe=".$cancel_param."'").'
											<p class="spacer"></p>
					                    </div>
									</td>
								</tr>
					        </table>
						</td>
					</tr>
					</table>

					<input type="hidden" id="DocumentID" name="DocumentID" value="'.cleanCrossSiteScriptingCode($documentID).'" />
					<input type="hidden" id="RecordStatus" name="RecordStatus" value="'.cleanCrossSiteScriptingCode($recordStatus).'" />
					<input type="hidden" id="tmpRecordStatus" name="tmpRecordStatus" value="'.cleanCrossSiteScriptingCode($tmpRecordStatus).'" />
					<input type="hidden" id="pageType" name="pageType" value="'.cleanCrossSiteScriptingCode($pageType).'" />
					</form>';
					
			return $x;
		}
		
		function addNewRouting($routingNumber,$routeDataAry=array(),$documentID='', $newRouteType=''){
			global $Lang, $button_remove, $button_select, $intranet_session_language,$replySlipConfig,$tableReplySlipConfig;
			global $PATH_WRT_ROOT, $docRoutingConfig, $indexVar;
			include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");		    
	
			$libDocRouting_prset = new libDocRouting_preset();
			
	//		/include_once($PATH_WRT_ROOT."lang/dr_lang.$intranet_session_language.php");
			//include_once($PATH_WRT_ROOT."includes/libinterface.php");
			//$linterface = new interface_html();
		
			$ldocrouting = $indexVar['libDocRouting'];
			$routeID = isset($routeDataAry['RouteID'])? $routeDataAry['RouteID'] : '';	
			
			$libReplySlipMgr = new libReplySlipMgr();
			$libTableReplySlipMgr = new libTableReplySlipMgr();			
			//$indexVar['FromAddAdHocRouting'];
			
			$note = isset($routeDataAry['Note'])? $routeDataAry['Note'] : '';
			
			//debug_pr($note);
					
			$effectiveType = isset($routeDataAry['EffectiveType'])? $routeDataAry['EffectiveType'] : '';
			if($effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){
				$effectiveStartDate = substr($routeDataAry['EffectiveStartDate'],0,10);
				$effectiveEndDate = substr($routeDataAry['EffectiveEndDate'],0,10);
				$effectiveStartTime = substr($routeDataAry['EffectiveStartDate'],10);
				$effectiveEndTime = substr($routeDataAry['EffectiveEndDate'],10);
			}else{
				$effectiveStartDate = date("Y-m-d");
				//$effectiveStartTime = "00:00:00";
				$effectiveStartTime = date("H:i:s");
				$effectiveEndDate = date("Y-m-d",time() + 86400*7);
				$effectiveEndTime = "23:59:59";
			}
			$viewRight = isset($routeDataAry['ViewRight'])? $routeDataAry['ViewRight'] : '';
			$replySlipContent = isset($routeDataAry['ReplySlipContent'])? $routeDataAry['ReplySlipContent'] : '';
			$actionsAry = explode(",",isset($routeDataAry['Actions'])?$routeDataAry['Actions']:'');
			$replySlipID = isset($routeDataAry['ReplySlipID'])? $routeDataAry['ReplySlipID'] : '';
			$replySlipType = isset($routeDataAry['ReplySlipType'])? $routeDataAry['ReplySlipType'] : '';
			$enableLock = isset($routeDataAry['EnableLock']) && $routeDataAry['EnableLock'] == 1;
			$lockDuration = !(isset($routeDataAry['LockDuration']) && $routeDataAry['LockDuration']>0)?$docRoutingConfig['routeLockTimeout']:$routeDataAry['LockDuration'];
			$readOnlyUserViewDetail = isset($routeDataAry['ReadOnlyUserViewDetail']) && $routeDataAry['ReadOnlyUserViewDetail'] == 1?1:0;
			
			$targetUserID = array();
			$extraTargetUserID = array();
		
			if($routeID!=''){
				$tmpUsers = $ldocrouting->getRouteTargetUsers(array($routeID));
				for($i=0;$i<count($tmpUsers);$i++){
					if($tmpUsers[$i]['AccessRight']==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['TargetUserOnly']){
						$targetUserID[] = array($tmpUsers[$i]['UserID'],$tmpUsers[$i]['UserName']);
					}else if($tmpUsers[$i]['AccessRight']==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser']){
						$extraTargetUserID[] = array($tmpUsers[$i]['UserID'],$tmpUsers[$i]['UserName']);
					}
				}
			}
			//debug_r($routeDataAry);
			# User Selection 
			//$userSelection ='';
			//$userSelection = "<select id='users_{$routingNumber}[]' class='users_{$routingNumber}' name='users_{$routingNumber}[]' size='6' multiple='multiple'>";
			//$userSelection .= "</select>";
			$userSelection = $this->GET_SELECTION_BOX($targetUserID,"id='users_{$routingNumber}' class='CopyClass_{$routingNumber}' name='users_{$routingNumber}[]' size='6' multiple","");
			$usersRemoveBtn = $this->GET_SMALL_BTN($Lang['Btn']['Remove'], "button", "javascript:js_User_Option_Remove($routingNumber)");
			
			
			# Extra User Selection
			//$extraUserSelection='';
			//$extraUserSelection = "<select id='extraUsers_{$routingNumber}[]' class='extraUsers_{$routingNumber}' name='extraUsers_{$routingNumber}[]' size='6' multiple='multiple'>";
			//$extraUserSelection .= "</select>";
			$extraUserSelection = $this->GET_SELECTION_BOX($extraTargetUserID,"id='extraUsers_{$routingNumber}' class='CopyClass_{$routingNumber}' name='extraUsers_{$routingNumber}[]' size='6' multiple","");
			$extraUsersRemoveBtn = $this->GET_SMALL_BTN($Lang['Btn']['Remove'], "button", "javascript:js_Extra_User_Option_Remove($routingNumber)");
			$extraUserViewDetailCheckbox = $this->Get_Checkbox("ReadOnlyUserViewDetail_".$routingNumber, "ReadOnlyUserViewDetail_".$routingNumber, 1, $readOnlyUserViewDetail, '', $Lang['DocRouting']['CanViewAllCommentsAndAttachments']);
			
			
			##### Reply Slip Handling ######
			
			//$replySlipParam = $ldocrouting->getEncryptedParameter('common','reply_slip_form',array('ReplySlipFileField'=>'ReplySlipFile_'+$routingNumber));
			$replySlipBtn = '';

				$replySlipBtn .= '<span id="replySlipLayer_'.$routingNumber.'">';
				$replySlipBtn .= '<input type="file" name="ReplySlipFile_'.$routingNumber.'" id="ReplySlipFile_'.$routingNumber.'" size="80" onchange="validateReplySlipCsv(\''.$routingNumber.'\');" />&nbsp;';
				$replySlipBtn .= $this->GET_SMALL_BTN($Lang['DocRouting']['PreviewReplySlip'], "button", "javascript:js_Submit_View_ReplySlip($routingNumber);","PreviewReplySlipBtn_".$routingNumber);
			//	$replySlipBtn .= '<br />';
			//	$replySlipBtn .= $libReplySlipMgr->returnGetSampleCsvLink();
				
				$replySlipBtn .= '<input type="hidden" id="replySlipId_'.$routingNumber.'" name="replySlipId_'.$routingNumber.'" value="'.$replySlipID.'" />';
			//	$replySlipBtn .= '</span>';
			
			
			if ($replySlipID != '') {
			//	$replySlipBtn .= '<span id="replySlipLayer_'.$routingNumber.'">';
				$previewCurrentReplySlipBtn = $this->GET_SMALL_BTN($Lang['DocRouting']['PreviewCurrentReplySlip'], "button", "javascript:js_Submit_View_ReplySlip($routingNumber, '".$replySlipID."');", "PreviewCurrentReplySlipBtn_".$routingNumber);			
				
				if($replySlipType == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
					$hasAnswer = (count($libTableReplySlipMgr->returnUserAnswer($replySlipID)) > 0)? true : false;
				}else{		
					$hasAnswer = (count($libReplySlipMgr->returnUserAnswer($replySlipID)) > 0)? true : false;
				}
//				// To be added for checking differet reply slip type answer
//				if($replySlipType!=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']){
//					INTRANET_REPLY_SLIP		
//				}else{
//					INTRANET_TABLE_REPLY_SLIP
//				}
				
				$replySlipOptionDisable = false;
				if ($hasAnswer) {
					$replySlipBtn .= '<br /><span class="tabletextrequire">'.$Lang['DocRouting']['WarningMsg']['ReplySlipHasAnswerAlready'].'</span>';
					$replySlipOptionDisable = true;
				}
				
			//	$replySlipBtn .= '</span>';
			}
				$replySlipBtn .= '</span>';
			##### Reply Slip Handling ######
			
			
			# Routing Order Display 
			$routingOrderDisplay ='';
			$routingOrderDisplay = $Lang['DocRouting']['Routing'] . ' ' . '<span class="classRouteNumber" >'.$routingNumber.'</span>';
								
			# Copy content from existing route btn
			$routeDataArr = $indexVar['libDocRouting']->getRouteData($documentID);
			$numOfExistingRoute = count($routeDataArr);
			
			# Check if this is a new route (show only in new route)	
			$copyFromExistingRouteBtn='';	
			$copyRouteURL = $ldocrouting->getEncryptedParameter('management','copy_route',array());			
			$copyFromExistingRouteBtn = $this->Get_Thickbox_Link(450, 500, "copy_dim", $Lang['DocRouting']['Button']['CopyFromExistingRounting'], "newWindow('?pe=$copyRouteURL&routingNumber=$routingNumber&documentID=$documentID&newRouteType=$newRouteType', 9);");	
					
			# Remove Route Btn
			if($indexVar['FromAddAdHocRouting'] != 1){
				$removeBtn = '';
				$removeBtn = '<a href="javascript:void(0);" class="delete_dim" onclick="js_Remove_Route('.$routingNumber.');" title="'.$Lang['Btn']['Delete'].'"></a>';
			}
			
			$routingOrderDisplay.= '<span class="table_row_tool" style="float:right;">';
			if($numOfExistingRoute>=1){		
				$routingOrderDisplay.= $copyFromExistingRouteBtn;		
			}
			$routingOrderDisplay.= $removeBtn; 				
			$routingOrderDisplay.= '</span>';
			
			# HTML Editor			
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$lfilesystem = new libfilesystem();
			include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
			//$objHtmlEditor = new FCKeditor ("Note_$routingNumber" , "100%", "220", "", "Basic2", intranet_undo_htmlspecialchars($note));
			$objHtmlEditor = new FCKeditor ("Note_$routingNumber" , "100%", "160", "", "Basic2", $note);
			//$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['DocRouting'], $id);
			$HTMLEditor = $objHtmlEditor->Create2();
			
			
			$routingTableDisplay = '<div id="DivRoute_'.$routingNumber.'">';
			$routingTableDisplay .= '<span class="counter"><input type="hidden" name="routeCounter[]" value="'.$routingNumber.'" /><input type="hidden" id="RouteID_'.$routingNumber.'" name="RouteID_'.$routingNumber.'" value="'.$routeID.'" /></span>';
			$routingTableDisplay .= '<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">';
			$routingTableDisplay .= '<tr>';
			$routingTableDisplay .= '<td width="100%">'. $this->GET_NAVIGATION2($routingOrderDisplay) . '</td>' ;
			$routingTableDisplay .= '</tr>';
							
			//$routingTableDisplay .= '<tr>';
			//$routingTableDisplay .= '<td >' . $Lang['DocRouting']['To']. ': </td>';
			//$routingTableDisplay .= '</tr>';
			$routingTableDisplay .= '</table>';
				
				       
			$routingTableDisplay .= '<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">';

			$routingTableDisplay .= '<tr>';
			$routingTableDisplay .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['To'] . '</td>';
			$routingTableDisplay .= '<td>';		
			//Henry Modified [Start]
			$divName = 'ToUserDiv'.$routingNumber;
        	$routingTableDisplay .= '<span id="spanShowOption_'.$divName.'" style="display:none">'."\n";
			$routingTableDisplay .= $this->Get_Show_Option_Link("javascript:js_Show_Option_Div('$divName');js_Hide_Option_Div('".$divName."_list"."')","",$Lang['Btn']['Show']);
			$routingTableDisplay .= '</span>'."\n";
			$routingTableDisplay .= '<span id="spanHideOption_'.$divName.'" >'."\n";
			$routingTableDisplay .= $this->Get_Hide_Option_Link("javascript:js_update_User_List_Div('users_".$routingNumber."','".$divName."_list');js_Hide_Option_Div('$divName');js_Show_Option_Div('".$divName."_list"."')","",$Lang['Btn']['Hide']);
			$routingTableDisplay .= '</span>'."\n";
				//Show the list of the user in a line
				$routingTableDisplay .= '<div id="'.$divName."_list".'">'."\n";
				$routingTableDisplay .= '</div>';
			$routingTableDisplay .= '<div id="'.$divName.'">'."\n";
			//Henry Modified [End]
			$routingTableDisplay .= '<table class="inside_form_table" border="0" cellpadding="0" cellspacing="0">';
			$routingTableDisplay .= '<tr>';
			$routingTableDisplay .= '<td>'. $userSelection .'</td>';
			$routingTableDisplay .= '<td valign="bottom">';
			$routingTableDisplay .= $this->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=users_{$routingNumber}[]&ppl_type=pic&permitted_type=1&excluded_type=4&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1".($indexVar['drUserIsAdmin']?"&FromDR=1":"")."', 9)") . '<br />';
			$routingTableDisplay .= $usersRemoveBtn;
			$routingTableDisplay .= '</td>';
			$routingTableDisplay .= '</tr>';
			$routingTableDisplay .= '</table>';
			$routingTableDisplay .= '</div>';//Henry Added
			$routingTableDisplay .= $this->Get_Thickbox_Warning_Msg_Div("UserWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['PleaseSelectUsers'], "WarnMsgDiv");
			$routingTableDisplay .= '</td>';
			$routingTableDisplay .= '</tr>';
			
			
			### View-only user settings
			$routingTableDisplay .= '<tr>';
				$routingTableDisplay .= '<td class="field_title">' . $Lang['DocRouting']['ReadOnlyUser'] . '</td>';
				$routingTableDisplay .= '<td>';
//				$routingTableDisplay .= $this->Get_Radio_Button('ViewRightAll_'.$routingNumber, "ViewRight_$routingNumber", $docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['TargetUserOnly'], $viewRight==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['TargetUserOnly'] || $viewRight=='', "CopyClass_{$routingNumber}", $Lang['DocRouting']['StepAccessAll'], 'js_Extra_User_Option_Hide('.$routingNumber.');');
//				$routingTableDisplay .= '<br />';
//				//$routingTableDisplay .= $this->Get_Radio_Button('StepAccessRelated', "DocumentAccess_$routingNumber", '2', 0, '', $Lang['DocRouting']['StepAccessRelated'], 'js_Extra_User_Option_Hide('.$routingNumber.');');
//				//$routingTableDisplay .= '<br />';
//				$routingTableDisplay .= $this->Get_Radio_Button('ViewRightViewByUser_'.$routingNumber, "ViewRight_$routingNumber", $docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser'], $viewRight==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser'], "CopyClass_{$routingNumber}", $Lang['DocRouting']['StepAccessRelatedAndMore'], 'js_Extra_User_Option_Show('.$routingNumber.');');
//				$routingTableDisplay .= '<div id="VisibleAccessUsers_'.$routingNumber.'" '.($viewRight==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser']?'':'style="display:none;"').'>';
//					$routingTableDisplay .= '<div id="VisibleAccessUsers_'.$routingNumber.'">';
						//Henry Modified [Start]
						$divName = 'ViewUserDiv'.$routingNumber;
			        	$routingTableDisplay .= '<span id="spanShowOption_'.$divName.'">'."\n";
						$routingTableDisplay .= $this->Get_Show_Option_Link("javascript:js_Show_Option_Div('$divName');js_Hide_Option_Div('".$divName."_list"."')","",$Lang['Btn']['Show']);
						$routingTableDisplay .= '</span>'."\n";
						$routingTableDisplay .= '<span id="spanHideOption_'.$divName.'" style="display:none">'."\n";
						$routingTableDisplay .= $this->Get_Hide_Option_Link("javascript:js_update_User_List_Div('extraUsers_".$routingNumber."','".$divName."_list');js_Hide_Option_Div('$divName');js_Show_Option_Div('".$divName."_list"."')","",$Lang['Btn']['Hide']);
						$routingTableDisplay .= '</span>'."\n";
							//Show the list of the user in a line
							$routingTableDisplay .= '<div id="'.$divName."_list".'">'."\n";
								foreach($extraTargetUserID as $aUser){
									$tempUserList .= $aUser[1].', ';
								}
								$tempUserList = substr($tempUserList, 0, -2);
								$routingTableDisplay .= $tempUserList;
							$routingTableDisplay .= '</div>';
						$routingTableDisplay .= '<div id="'.$divName.'" style="display:none">'."\n";
						//Henry Modified [End]
						$routingTableDisplay .= '<table class="inside_form_table" border="0" cellpadding="0" cellspacing="0">';
							$routingTableDisplay .= '<tr>';
								$routingTableDisplay .= '<td>' . $extraUserSelection .'<br />'.$extraUserViewDetailCheckbox. '</td>';
								$routingTableDisplay .= '<td valign="bottom">';
									$routingTableDisplay .=  $this->GET_SMALL_BTN($button_select, "button", "clickedSelectViewOnlyUser($routingNumber);");
									$routingTableDisplay .= '<br />';
									$routingTableDisplay .= $extraUsersRemoveBtn;
									$routingTableDisplay .= '</td>';
							$routingTableDisplay .= '</tr>';
						$routingTableDisplay .= '</table>';
						$routingTableDisplay .= '</div>'; //Henry Added
//					$routingTableDisplay .= '</div>';
				$routingTableDisplay .= '</td>';
			$routingTableDisplay .= '</tr>';
			
			$routingTableDisplay .= '<tr>';
			$routingTableDisplay .= '<td class="field_title">' . $Lang['DocRouting']['Instruction']. '</td>';
			$routingTableDisplay .= '<td>';
			//Henry Adding [Start]
        	$divName = 'InstructionDiv'.$routingNumber;
        	$routingTableDisplay .= '<span id="spanShowOption_'.$divName.'">'."\n";
			$routingTableDisplay .= $this->Get_Show_Option_Link("javascript:js_Show_Option_Div('$divName');","",$Lang['Btn']['Show']);
			$routingTableDisplay .= '</span>'."\n";
			$routingTableDisplay .= '<span id="spanHideOption_'.$divName.'" style="display:none">'."\n";
			$routingTableDisplay .= $this->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('$divName');","",$Lang['Btn']['Hide']);
			$routingTableDisplay .= '</span>'."\n";
			$routingTableDisplay .= '<div id="'.$divName.'" style="display:none">'."\n";
			//Henry Adding [End]
			//$routingTableDisplay .= $linterface->GET_TEXTAREA("Note_$routingNumber", "", "90", "5", "", "", "name='Note_'$routingNumber", "");
			
			$routingTableDisplay .= '<div class="content_top">';	
			$routingTableDisplay .= $this->getUserPresetSelection($docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote'], $indexVar['drUserId'], "presetNotesSelection_$routingNumber", "presetNotesSelection_$routingNumber", "docRoutingObj.applySelectedPresetNoteContent($routingNumber);", "class='CopyClass_{$routingNumber}'");
			
			$routingTableDisplay .= '<div class="Conntent_tool" style="float:right">';
			if($ldocrouting->enablePrintRoutingInstructionRight()){
				//$routingTableDisplay .='<a href="javascript:void(0);" class="print option_layer" id="btn_print" onclick="js_Clicked_Option_Layer(\'print_option\', \'btn_print\'); hideAllOptionLayer();"> Print</a>';
				
				$printInstructionLink = "index.php?pe=".$ldocrouting->getEncryptedParameter('management','print_instruction_step1'). "&routingNumber=" . $routingNumber;
				$routingTableDisplay .= $this->GET_LNK_PRINT($ParHref="", $button_text="", $ParOnClick="newWindow('$printInstructionLink', 10);return false", $ParOthers="", $ParClass="", $useThickBox=1);
			}
			
			$routingTableDisplay .= '</div>';
			$routingTableDisplay .= '</div>';
			
			$routingTableDisplay .= '<br />';
			$routingTableDisplay .= $HTMLEditor;
		//	$routingTableDisplay .= '<br />'. $this->GET_SELECTION_BOX($Lang['DocRouting']['PresetNotes'], "name=\"presetNotesSelection_{$routingNumber}\" id=\"presetNotesSelection_{$routingNumber}\" onChange=\"js_Change_Selection($routingNumber)\" ", $Lang['DocRouting']['PresetNotesSelection']) ;
			$routingTableDisplay .= '</div>'; //Henry Adding
			$routingTableDisplay .= '</td>';
			$routingTableDisplay .= '</tr>';
			$routingTableDisplay .= '<tr>';
			$routingTableDisplay .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['Action'] . '</td>';
			$routingTableDisplay .= '<td>';
			//$routingTableDisplay .= $this->Get_Radio_Button('ActionAllowSIGN', "ActionAllow_$routingNumber", 'SIGN', '0', '', $Lang['DocRouting']['Actions']["SIGN"], '', '');
		//	$routingTableDisplay .= $this->Get_Checkbox('ActionAllowSign_'.$routingNumber,"ActionAllow_".$routingNumber."[]",$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['SignConfirm'],in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['SignConfirm'],$actionsAry),'',$Lang['DocRouting']['Actions']["SIGN"],'js_Enable_ReplySlip('.$routingNumber.',this.checked);');
		//	$routingTableDisplay .= '<br />';
			//$routingTableDisplay .= $this->Get_Radio_Button('ActionAllowONLINEFORM', "ActionAllow_$routingNumber" , 'ONLINEFORM', '1', '', $Lang['DocRouting']['Actions']["ONLINEFORM"], '', '' );
		
			$routingTableDisplay .= $this->Get_Checkbox('ActionAllowReplySlip_'.$routingNumber,"ActionAllow_".$routingNumber."[]",$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'],in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'],$actionsAry),"CopyClass_{$routingNumber}",$Lang['DocRouting']['Actions']["ONLINEFORM"],'js_Enable_Edit_ReplySlip('.$routingNumber.',this.checked);' /*,!in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['SignConfirm'],$actionsAry)*/);
			
			# Preview Current Reply Slip Btn Layer
			$routingTableDisplay .= ' <span id="replySlipCurrentBtnLayer_'.$routingNumber.'">'.$previewCurrentReplySlipBtn . '</span>';
			
			# Layer for containing copy Reply Slip information
			$routingTableDisplay .= '<span id="replySlipCurrentHiddenFieldLayer_'.$routingNumber.'"></span>';
			
			$routingTableDisplay .= '<br />';
		
			$styleTag = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'], $actionsAry)? '' : 'style="display:none;"';
			$routingTableDisplay .= '<div id="routeReplySlipSettingsDiv_'.$routingNumber.'" '.$styleTag.'>';
				//$routingTableDisplay .= '<input type="hidden" name="ReplySlipContent_'.$routingNumber.'" id="ReplySlipContent_'.$routingNumber.'" value="" />';
				$routingTableDisplay .= '<div style="padding-left:25px;">';
					$routingTableDisplay .= $replySlipBtn;
					
					# Reply Slip Type
					$routingTableDisplay .= '<br />';
					$routingTableDisplay .= '<div>';
					$routingTableDisplay .= $Lang['DocRouting']['ONLINEFORM']['ReplySlipType'].":&nbsp;";
					$routingTableDisplay .= $this->Get_Radio_Button('ReplySlipType_'.$routingNumber.'_'.$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['List'], 'ReplySlipType_'.$routingNumber, $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['List'], $replySlipType!=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table'], "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['ReplySlipTypeList'], "", $replySlipOptionDisable);
					$routingTableDisplay .= '&nbsp;'.$libReplySlipMgr->returnGetSampleCsvLink();
					$routingTableDisplay .= '&nbsp;&nbsp;';
					$routingTableDisplay .= $this->Get_Radio_Button('ReplySlipType_'.$routingNumber.'_'.$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table'], 'ReplySlipType_'.$routingNumber, $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table'], $replySlipType==$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table'], "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['ReplySlipTypeTable'], "", $replySlipOptionDisable);
					$routingTableDisplay .= '&nbsp;'.$libTableReplySlipMgr->returnGetSampleCsvLink();
					$routingTableDisplay .= '</div>';
					
					
					# Release result settings for each routes
				
			//		$displayControl = '';
			//		if(in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'],$actionsAry)){
			//			$displayControl = '';
			//		}else{
			//			$displayControl = 'style="display:none;"';	
			//		}
					
					
					$isReleaseAnytimeChecked = false;
					$isReleaseAfterSubmissionChecked = false;
					$isReleaseAfterRouteChecked = false;
					$isReleaseNeverChecked = false; //Henry added [20140522]
					
					if($routeID){
						$rountDataArr = array();
						$rountDataArr = $indexVar['libDocRouting']->getRouteData($documentID, $routeID);
						$replySlipReleaseType = $rountDataArr[0]['ReplySlipReleaseType'];
				
					}
			
					# Check exisiting value of release type
					if($replySlipReleaseType == $docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseAnytime']){
						$isReleaseAnytimeChecked = true;
					}elseif($replySlipReleaseType == $docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterSubmission']){
						$isReleaseAfterSubmissionChecked = true;
					}elseif($replySlipReleaseType == $docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterRouteCompeleted']){
						$isReleaseAfterRouteChecked = true;
					}elseif($replySlipReleaseType == $docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseNever']){ //Henry added [20140522]
						$isReleaseNeverChecked = true;
					}elseif($replySlipReleaseType == '') {
						$isReleaseAfterRouteChecked = true;
					}
					
					# Display User Name Option
					$isDisplayUserNameChecked = false;
					$isHideUserNameChecked = false;
					
					if($replySlipType == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
						$libTableReplySlip = new libTableReplySlip($replySlipID);
						$ShowUserInfoInResult = $libTableReplySlip->getShowUserInfoInResult();
					}else {
						$libReplySlip = new libReplySlip($replySlipID);
						$ShowUserInfoInResult = $libReplySlip->getShowUserInfoInResult();
					}
					if($ShowUserInfoInResult == $replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['show']){
						$isDisplayUserNameChecked = true;
					}else{
						$isHideUserNameChecked = true;
					}
					
			
								
					$routingTableDisplay .= '<div id="showUserNameDisplaySettingsDiv_'.$routingNumber.'">';
						$routingTableDisplay .= $Lang['DocRouting']['ONLINEFORM']['UserNameDisplaySettings'];
						$routingTableDisplay .= '&nbsp;';
						$routingTableDisplay .= $this->Get_Radio_Button('DisplayUserName_'.$routingNumber, 'UserNameDisplayType_'.$routingNumber.'[]', $replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['show'], $isDisplayUserNameChecked, "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['DisplayUserName'], $Onclick="js_onChangeShowHideReplySlipUserOption('".$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['hide']."', '".'ReleaseResultAfterRouteCompeleted_'.$routingNumber."', '".'ReleaseAnytime_'.$routingNumber."', '".'ReleaseResultAfterSubmission_'.$routingNumber."',false);", $replySlipOptionDisable);			
						$routingTableDisplay .= '&nbsp;';
						$routingTableDisplay .= $this->Get_Radio_Button('HideUserName_'.$routingNumber, 'UserNameDisplayType_'.$routingNumber.'[]', $replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['hide'], $isHideUserNameChecked, "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['HideUserName'], $Onclick="js_onChangeShowHideReplySlipUserOption('".$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['hide']."', '".'ReleaseResultAfterRouteCompeleted_'.$routingNumber."', '".'ReleaseAnytime_'.$routingNumber."', '".'ReleaseResultAfterSubmission_'.$routingNumber."',true);", $replySlipOptionDisable);			
						$routingTableDisplay .= '&nbsp;';
					$routingTableDisplay .= '</div>';
					
					$routingTableDisplay .= '<div id="releaseResultSettingsDiv_'.$routingNumber.'" >';
						$routingTableDisplay .= $Lang['DocRouting']['ONLINEFORM']['ReleaseResultSettings'];
						$routingTableDisplay .= '&nbsp;';
						$routingTableDisplay .= $this->Get_Radio_Button('ReleaseAnytime_'.$routingNumber, "ReplySlipReleaseType_".$routingNumber."[]", $docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseAnytime'], $isReleaseAnytimeChecked, "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['ReleaseAnytime'], $Onclick="", $replySlipOptionDisable || $isHideUserNameChecked);
						$routingTableDisplay .= '&nbsp;';
						$routingTableDisplay .= $this->Get_Radio_Button('ReleaseResultAfterSubmission_'.$routingNumber, "ReplySlipReleaseType_".$routingNumber."[]", $docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterSubmission'], $isReleaseAfterSubmissionChecked, "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['ReleaseResultAfterSubmission'], $Onclick="", $replySlipOptionDisable || $isHideUserNameChecked);
						$routingTableDisplay .= '&nbsp;';
						$routingTableDisplay .= $this->Get_Radio_Button('ReleaseResultAfterRouteCompeleted_'.$routingNumber, "ReplySlipReleaseType_".$routingNumber."[]", $docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterRouteCompeleted'], $isReleaseAfterRouteChecked || $isHideUserNameChecked, "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['ReleaseResultAfterRouteCompeleted'], $Onclick="", $replySlipOptionDisable); //Henry Modified 20131007 added $isHideUserNameChecked
						$routingTableDisplay .= '&nbsp;';
						$routingTableDisplay .= $this->Get_Radio_Button('ReleaseNever_'.$routingNumber, "ReplySlipReleaseType_".$routingNumber."[]", $docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseNever'], $isReleaseNeverChecked, "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['ReleaseNever'], $Onclick="", $replySlipOptionDisable); //Henry added [20140522]
						
						//$routingTableDisplay .= $this->Get_Checkbox('ActionAllowReplySlip_'.$routingNumber,"ActionAllow_".$routingNumber."[]",$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'],in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'],$actionsAry),'',$Lang['DocRouting']['Actions']["ONLINEFORM"],'js_Enable_Edit_ReplySlip('.$routingNumber.',this.checked);' /*,!in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['SignConfirm'],$actionsAry)*/);
					//	$routingTableDisplay .= '<br />';
					$routingTableDisplay .= '</div>';
					
					$routingTableDisplay .= $this->Get_Thickbox_Warning_Msg_Div("ActionAllowReplySlipWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['PleaseUploadReplySlipFile'], "WarnMsgDiv");
					$routingTableDisplay .= $this->Get_Thickbox_Warning_Msg_Div("ReplySlipCsvInvalidWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['ReplySlipCsvNotCorrect'], "WarnMsgDiv");
					$routingTableDisplay .= '<iframe id="replySlipIFrame_'.$routingNumber.'" name="replySlipIFrame_'.$routingNumber.'" src="" scrolling="no" frameborder="0" style="display:none;"></iframe>';
				$routingTableDisplay .= '</div>';
			$routingTableDisplay .= '</div>';
			
			
					
			//$routingTableDisplay .= '</td>';
			//$routingTableDisplay .= '</tr>';
			//$routingTableDisplay .= '<tr>';
			//$routingTableDisplay .= '<td class="field_title">' . $Lang['DocRouting']['OptionalAction']. '</td>';
			//$routingTableDisplay .= '<td>';
			$routingTableDisplay .= $this->Get_Checkbox('ActionAllowComment_'.$routingNumber, 'ActionAllow_'.$routingNumber.'[]', $docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'], in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'],$actionsAry) || in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'],$actionsAry), "CopyClass_{$routingNumber}", $Lang['DocRouting']['Actions']["COMMENT"], 'js_Enable_Edit_Comment('.$routingNumber.',this.checked);', '');
			$routingTableDisplay .= '<br />';
			// Henry Added [20140522][start] 
			$styleTag = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'], $actionsAry) || in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'],$actionsAry)? '' : 'style="display:none;"';
			$routingTableDisplay .= '<div id="ActionAllowCommentSettingsDiv_'.$routingNumber.'" '.$styleTag.'>';
				$routingTableDisplay .= '<div style="padding-left:25px;">';
					$routingTableDisplay .= $Lang['DocRouting']['ONLINEFORM']['ReadByOthers'];
					$routingTableDisplay .= '&nbsp;';
					$routingTableDisplay .= $this->Get_Radio_Button('AllowCommentReadByOthers_'.$routingNumber, 'CommentReadByOthers_'.$routingNumber, 1, in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'],$actionsAry) || (!in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'],$actionsAry) && !in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'],$actionsAry)), "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['Yes']);			
					$routingTableDisplay .= '&nbsp;';
					$routingTableDisplay .= $this->Get_Radio_Button('DisallowCommentReadByOthers_'.$routingNumber, 'CommentReadByOthers_'.$routingNumber, 0, in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'],$actionsAry), "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['No']);			
					$routingTableDisplay .= '&nbsp;';
				$routingTableDisplay .= '</div>';
			$routingTableDisplay .= '</div>';
			// Henry Added [20140522][end]
			$routingTableDisplay .= $this->Get_Checkbox('ActionAllowAttachment_'.$routingNumber, 'ActionAllow_'.$routingNumber.'[]', $docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'], in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'],$actionsAry) || in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'],$actionsAry), "CopyClass_{$routingNumber}", $Lang['DocRouting']['Actions']["ATTACHMENT"], 'js_Enable_Edit_Attachment('.$routingNumber.',this.checked);', '');
			// Henry Added [20140522][start] 
			$styleTag = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'], $actionsAry) || in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'],$actionsAry)? '' : 'style="display:none;"';
			$routingTableDisplay .= '<div id="ActionAllowAttachmentSettingsDiv_'.$routingNumber.'" '.$styleTag.'>';
				$routingTableDisplay .= '<div style="padding-left:25px;">';
					$routingTableDisplay .= $Lang['DocRouting']['ONLINEFORM']['ReadByOthers'];
					$routingTableDisplay .= '&nbsp;';
					$routingTableDisplay .= $this->Get_Radio_Button('AllowAttachmentReadByOthers_'.$routingNumber, 'AttachmentReadByOthers_'.$routingNumber, 1, in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'],$actionsAry) || (!in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'],$actionsAry) && !in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'],$actionsAry)), "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['Yes']);			
					$routingTableDisplay .= '&nbsp;';
					$routingTableDisplay .= $this->Get_Radio_Button('DisallowAttachmentReadByOthers_'.$routingNumber, 'AttachmentReadByOthers_'.$routingNumber, 0, in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'],$actionsAry), "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['No']);			
					$routingTableDisplay .= '&nbsp;';
				$routingTableDisplay .= '</div>';
			$routingTableDisplay .= '</div>';
			// Henry Added [20140522][end]								
			$routingTableDisplay .= $this->Get_Thickbox_Warning_Msg_Div("ActionWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['PleaseSelectAllowedActions'], "WarnMsgDiv");
			$routingTableDisplay .= '</td>';
			$routingTableDisplay .= '</tr>';
			$routingTableDisplay .= '<tr>';
			$routingTableDisplay .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['Visible']. '</td>';
			$routingTableDisplay .= '<td>';
			$routingTableDisplay .= $this->Get_Radio_Button('EffectiveTypePeriod_'.$routingNumber, "EffectiveType_$routingNumber", $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period'], $effectiveType==$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period'], "CopyClass_{$routingNumber}", $Lang['DocRouting']['Period'], 'js_Document_Show('.$routingNumber.');');
			$routingTableDisplay .= '<br />';
			$routingTableDisplay .= '<div id="VisiblePeriod_'.$routingNumber.'" '.($effectiveType==$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']?'':'style="display:none;"').'>';
			// Henry Added [20140522][start] 
			$routingTableDisplay .= '<div style="padding-left:25px;">';
			// Henry Added [20140522][end]
			$routingTableDisplay .= $this->GET_DATE_PICKER("EffectiveStartDate_$routingNumber", $effectiveStartDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="CopyClass_{$routingNumber}"). '&nbsp;';
			$routingTableDisplay .= $this->Get_Time_Selection("EffectiveStartTime_$routingNumber", $effectiveStartTime, "class='CopyClass_{$routingNumber}'", '', array(1,1,1));
			$routingTableDisplay .= '-&nbsp;'.$this->GET_DATE_PICKER("EffectiveEndDate_$routingNumber", $effectiveEndDate, $OtherMember="", $DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="CopyClass_{$routingNumber}").'&nbsp;'.$this->Get_Time_Selection("EffectiveEndTime_$routingNumber", $effectiveEndTime, "class='CopyClass_{$routingNumber}'", '' , array(1,1,1));
			$routingTableDisplay .= $this->Get_Thickbox_Warning_Msg_Div("EffectiveTypeWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange'], "WarnMsgDiv");
			$routingTableDisplay .= '<br />';
			// Henry Added [20140522][start] 
			$routingTableDisplay .= $Lang['DocRouting']['ONLINEFORM']['SubmitAfterDeadline'];
			$routingTableDisplay .= '&nbsp;';
			$routingTableDisplay .= $this->Get_Radio_Button('AllowSubmitAfterDeadline_'.$routingNumber, 'SubmitAfterDeadline_'.$routingNumber, 1, (isset($routeDataAry['EnableSubmitAfterEndDate']) && $routeDataAry['EnableSubmitAfterEndDate']==1) || !$routeDataAry, "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['Yes']);			
			$routingTableDisplay .= '&nbsp;';
			$routingTableDisplay .= $this->Get_Radio_Button('DisallowSubmitAfterDeadline_'.$routingNumber, 'SubmitAfterDeadline_'.$routingNumber, 0, $routeDataAry && $routeDataAry['EnableSubmitAfterEndDate']==0, "CopyClass_{$routingNumber}", $Lang['DocRouting']['ONLINEFORM']['No']);			
			$routingTableDisplay .= '&nbsp;';
			$routingTableDisplay .= '</div>';
			// Henry Added [20140522][end]
			$routingTableDisplay .= '</div>';
			
			$routingTableDisplay .= '<div class="EffectiveByPreRouteDiv">';
			$routingTableDisplay .= $this->Get_Radio_Button('EffectiveTypePrevRouteFinished_'.$routingNumber, "EffectiveType_$routingNumber", $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished'], $effectiveType==$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished'] || $effectiveType=='', "CopyClass_{$routingNumber}", '<span id="optionFinishedSpan_'.$routingNumber.'">'.$Lang['DocRouting']['PreviewRouteDone'].'<span class="tabletextremark"> ('.$Lang['DocRouting']['NotApplicableToFirstRoute'].')</span></span>', 'js_Document_Hide('.$routingNumber.');');	
			//$routingTableDisplay .= '<span class="tabletextremark"> ('.$Lang['DocRouting']['NotApplicableToFirstRoute'].')</span>';
			$routingTableDisplay .= '</div>';
			
			$routingTableDisplay .= $this->Get_Thickbox_Warning_Msg_Div("EffectiveTypePrevRouteWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['PleaseSelectEffectiveTypeByPeriod'], "WarnMsgDiv");
			
			
			$routingTableDisplay .= '</td>';
			$routingTableDisplay .= '</tr>';
//			$routingTableDisplay .= '<tr>';
//			$routingTableDisplay .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['ViewBy'] . '</td>';
//			$routingTableDisplay .= '<td>';
//			$routingTableDisplay .= $this->Get_Radio_Button('ViewRightAll_'.$routingNumber, "ViewRight_$routingNumber", $docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['TargetUserOnly'], $viewRight==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['TargetUserOnly'] || $viewRight=='', "CopyClass_{$routingNumber}", $Lang['DocRouting']['StepAccessAll'], 'js_Extra_User_Option_Hide('.$routingNumber.');');
//			$routingTableDisplay .= '<br />';
//			//$routingTableDisplay .= $this->Get_Radio_Button('StepAccessRelated', "DocumentAccess_$routingNumber", '2', 0, '', $Lang['DocRouting']['StepAccessRelated'], 'js_Extra_User_Option_Hide('.$routingNumber.');');
//			//$routingTableDisplay .= '<br />';
//			$routingTableDisplay .= $this->Get_Radio_Button('ViewRightViewByUser_'.$routingNumber, "ViewRight_$routingNumber", $docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser'], $viewRight==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser'], "CopyClass_{$routingNumber}", $Lang['DocRouting']['StepAccessRelatedAndMore'], 'js_Extra_User_Option_Show('.$routingNumber.');');
//										
//			$routingTableDisplay .= '<div id="VisibleAccessUsers_'.$routingNumber.'" '.($viewRight==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser']?'':'style="display:none;"').'>';
//			$routingTableDisplay .= '<table class="inside_form_table" border="0" cellpadding="0" cellspacing="0">';
//			$routingTableDisplay .= '<tr>';
//			$routingTableDisplay .= '<td>' . $extraUserSelection .$this->Get_Thickbox_Warning_Msg_Div("ExtraUserWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['PleaseSelectUsers'], "WarnMsgDiv"). '</td>';
//			$routingTableDisplay .= '<td valign="bottom">';
//			$routingTableDisplay .=  $this->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=extraUsers_{$routingNumber}[]&ppl_type=pic&permitted_type=1&excluded_type=4', 9)");
//			$routingTableDisplay .= '<br />';
//			$routingTableDisplay .= $extraUsersRemoveBtn;
//			$routingTableDisplay .= '</td>';
//			$routingTableDisplay .= '</tr>';
//			$routingTableDisplay .= '</table>';
//			$routingTableDisplay .= '</div>';
//			$routingTableDisplay .= '</td>';
//			$routingTableDisplay .= '</tr>';
			
			$routingTableDisplay .= '<tr>';
				$routingTableDisplay .= '<td class="field_title">'.$Lang['DocRouting']['LockRoutingWhenGivingFeedback'].'</td>';
				$routingTableDisplay .= '<td>';
					$routingTableDisplay .= $this->Get_Checkbox('EnableLock_'.$routingNumber,'EnableLock_'.$routingNumber,'1',$enableLock,"CopyClass_{$routingNumber}",'','js_Enable_Lock(this,\''."lockDurationDiv_".$routingNumber.'\');');
					$cssDisplay = $enableLock?'':'style="display:none;"';
					$routingTableDisplay .= '<div id="lockDurationDiv_'.$routingNumber.'" '.$cssDisplay.'>';
						$routingTableDisplay .= $Lang['DocRouting']['LockFor'].'&nbsp;';
						$routingTableDisplay .= '<input id="LockDuration_'.$routingNumber.'" name="LockDuration_'.$routingNumber.'" type="text" maxlength="10" value="'.$lockDuration.'" size="10" />';
						$routingTableDisplay .= '&nbsp;'.$Lang['DocRouting']['Minutes'];
						$routingTableDisplay .= $this->Get_Thickbox_Warning_Msg_Div("LockDurationWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['RequestInputPositiveInteger'], "WarnMsgDiv");
					$routingTableDisplay .= '</div>';
				$routingTableDisplay .= '</td>';
			$routingTableDisplay .= '</tr>';
			
			$routingTableDisplay .= '</table>';
			$routingTableDisplay .= '</div>';
	
			//$routingTableDisplay .= '</div>';
			return $routingTableDisplay;
		}
		
		function getDocRoutingEditForm($dataAry)
		{
			global $Lang, $PATH_WRT_ROOT, $xmsg, $indexVar, $docRoutingConfig;
			
			$lfilesystem = $indexVar['libfilesystem'];
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			
			$pageType = $dataAry['PageType'];
			$documentID = $dataAry['DocumentID'];
			$title = $dataAry['Title'];
			$instruction = $dataAry['Instruction'];
			$documentType = $dataAry['DocumentType'];
			$physicalLocation = $dataAry['PhysicalLocation'];
			$docTags = $dataAry['DocTags'];
			$allowAdHocRoute = $dataAry['AllowAdHocRoute'];
			//$documentUploaded = $dataAry['DocumentUploaded'];
			$recordStatus = $dataAry['RecordStatus'];
			$tmpRecordStatus = $dataAry['tmpRecordStatus'];
			
			if($documentID != '' && $tmpRecordStatus != $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp']) {
				$navigationText = $Lang['DocRouting']['EditDocRouting'];
			}else{
				$navigationText = $Lang['DocRouting']['NewDocRouting']; 
			}
			
			$routes = $ldocrouting->getRouteData(array($documentID));
			
			# step information
			$STEPS_OBJ[] = array($Lang['DocRouting']['NewDocRoutingStep1'], 0);
			$STEPS_OBJ[] = array($Lang['DocRouting']['NewDocRoutingStep2'], 1);
			
			# Navigation
			//$PAGE_NAVIGATION[] = array($Lang['DocRouting']['CurrentRoutings'], "javascript:js_Go_Back();");
			$PAGE_NAVIGATION[] = array($navigationText, "");
			
			$form_action = $ldocrouting->getEncryptedParameter('management','edit_doc_route_update',array());
			if ($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']) {
				$cancelPage = 'draft_routings';
			}
			else {
				$cancelPage = 'index';
			}
			$cancel_param = $ldocrouting->getEncryptedParameter('management',$cancelPage,array());
			
			if ($recordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']) {
				$submitBtnDisplay = $Lang['Btn']['Submit'];
			}
			else {
				$submitBtnDisplay = $Lang['DocRouting']['Management']['Publish'];
			}
			
			$x = '<form id="form1" name="form1" method="post" action="index.php?pe='.$form_action.'" enctype="multipart/form-data">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>'.$this->GET_NAVIGATION_IP25($PAGE_NAVIGATION).'<br /><br /></td>
							<td align="right">'.$xmsg.'</td>
						</tr>
						<tr><td height="40" colspan="2">'.$this->GET_STEPS($STEPS_OBJ).'</td></tr>
						<tr>
						<td class="board_menu_closed">
					        <table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
					        	<tr>
					        		<td class="main_content">
					        			<div class="table_board">
					        				<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
									            <tr>
													<td bgcolor="#FFEECC"><b>'.$title.'</b></td>
									            </tr>
									        </table>';
							if(count($routes)>0){
								for($i=0;$i<count($routes);$i++){
									$x .= $this->addNewRouting($i+1,$routes[$i],$documentID);
								}
							}else{
								$x .= $this->addNewRouting('1', '', $documentID);
							}
					//		if($allowAdHocRoute == 1){
									$x .= '<br />
					        				<div id="new_routing_display"></div>
	        								<table class="form_table" width="90%" cellpadding="4" cellspacing="0" border="0" align="center">
					            				<tr>
					            					<td colspan="2" height="35" valign="bottom">&nbsp; <a id="addNewRouting" href="javaScript:js_Add_New_Routing('.$documentID.');">
														<img src="/images/2009a/icon_add_s.gif" border="0" align="absmiddle" />'.$Lang['DocRouting']['AddMoreFollowUp'].'</a>
													</td>
					            				</tr>
					        				</table>';
						//	}
									$x .= '<p class="spacer"></p>
										</div>
										<div class="edit_bottom">
					                        <p class="spacer"></p>
					                        '.$this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();").'&nbsp;
					                        '.$this->GET_ACTION_BTN($submitBtnDisplay, "button", "js_Submit_Form('".$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']."');", "btnSubmit").'&nbsp;
											'.$this->GET_ACTION_BTN($Lang['DocRouting']['SaveAsDraft'], "button", "js_Submit_Form('".$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']."');", "btnSaveAsDraft").'&nbsp;
					                        '.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location.href='index.php?pe=".$cancel_param."'").'
											<p class="spacer"></p>
					                    </div>
									</td>
								</tr>
					        </table>
							<input type="hidden" id="DocumentID" name="DocumentID" value="'.cleanCrossSiteScriptingCode($documentID).'" />';
					/*
					$x .=  '<input type="hidden" id="Title" name="Title" value="'.$title.'" />
							<input type="hidden" id="Instruction" name="Instruction" value="'.$instruction.'"/>
							<input type="hidden" id="DocTags" name="DocTags" value="'.$docTags.'"/>';
					*/
					$x .=  '<input type="hidden" id="DocumentType" name="DocumentType" value="'.cleanCrossSiteScriptingCode($documentType).'"/>
							<input type="hidden" id="PhysicalLocation" name="PhysicalLocation" value="'.intranet_htmlspecialchars($physicalLocation).'"/>
							<input type="hidden" id="AllowAdHocRoute" name="AllowAdHocRoute" value="'.cleanCrossSiteScriptingCode($allowAdHocRoute).'" />
							<input type="hidden" id="RecordStatus" name="RecordStatus" value="'.cleanCrossSiteScriptingCode($recordStatus).'" />
							<input type="hidden" id="tmpRecordStatus" name="tmpRecordStatus" value="'.cleanCrossSiteScriptingCode($tmpRecordStatus).'" />
                            <input type="hidden" id="pastRecordStatus" name="pastRecordStatus" value="'.cleanCrossSiteScriptingCode($recordStatus).'" />
							<input type="hidden" id="BackToStep1" name="BackToStep1" value="" />
							<input type="hidden" id="pageType" name="pageType" value="'.cleanCrossSiteScriptingCode($pageType).'" />
							';
							
						//$x .= '<input type="hidden" id="RouteCount" name="RouteCount" value="1" />';
				/*			
				if($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']){
					$x .= '<div style="display:none;">';
					for($i=0;$i<count($documentUploaded);$i++){
						if(trim($documentUploaded[$i])!=''){
							$x .= '<input type="file" name="DocumentUploaded" value="'.$documentUploaded[$i].'" />';
						}
					}
					$x .= '</div>';
				}
				*/
					$x .= '</td>
						</tr>
						</table>	
					</form>';
			
			return $x;
		}
		
		function getDocDetailForm($DocumentID)
		{
			global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $xmsg, $indexVar, $docRoutingConfig, $sys_custom, $i_admintitle_sa_password;
			
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			$ldamu = $indexVar['libdigitalarchive_moduleupload'];
			$canArchiveFile = $ldamu->User_Can_Archive_File();
			
			if(isset($indexVar['entryData'])){
				$entry = $indexVar['entryData'];
			}else{
				$entry = $ldocrouting->getEntryData($DocumentID);
			}
			$indexVar['DocEntry'] = $entry;
			$title = $entry[0]['Title'];
			$creatorUserID = $entry[0]['UserID'];
			$creatorName = $entry[0]['CreatorName'];
			$docInputTime = $entry[0]['DateInput'];
			$docModifiedDate = $entry[0]['DateModified'];
			$instruction = $entry[0]['Instruction'];
			$documentType = $entry[0]['DocumentType'];
			$physicalLocation = $entry[0]['PhysicalLocation'];
			$recordStatus = $entry[0]['RecordStatus'];
			$allowAdHocRoute = $entry[0]['AllowAdHocRoute'];
			$stared = $entry[0]['Stared'] > 0;
			//debug_r($entry[0]);
			$isDraft = $recordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft'];
			$isCompleted = $recordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete'];
			$isDeleted = $recordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Deleted'];
			
			if($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']){
				$attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $DocumentID);
			}
			if($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA']){
				$attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $DocumentID);
			}
			$numOfAttachment = count($attachments);
			$routes = $ldocrouting->getRouteData($DocumentID,'',$isDeleted?$docRoutingConfig['INTRANET_DR_ROUTES']['RecordStatus']['Deleted']:$docRoutingConfig['INTRANET_DR_ROUTES']['RecordStatus']['Active']);
			$numOfRoutes = count($routes);
			
			$routeStatusAssoc = $ldocrouting->getRouteCompleteStatus($DocumentID);
			$indexVar['RouteStatusAssoc'] = $routeStatusAssoc;
			$is_all_routes_completed = true;
			//debug_r($routeStatusAssoc[$DocumentID]);
			/*
			if(isset($routeStatusAssoc[$DocumentID])){
				foreach($routeStatusAssoc[$DocumentID] as $route_id => $route_status){
					if($route_status != $docRoutingConfig['routeStatus']['completed']){
						$is_all_routes_completed = false;
						break;
					}
				}
			}
			*/
			$route_status_ary = $ldocrouting->getDocRoutingUserStatusCount($DocumentID);
			for($j=0;$j<count($route_status_ary);$j++){
				if($route_status_ary[$j]['UserTotal'] != $route_status_ary[$j]['CompletedTotal']){
					$is_all_routes_completed = false;
					break;
				}
			}
			
			// find the ordered routing entry list to get previous and next routing entries
			$needToFollowEntryAry = $ldocrouting->getUserCurrentInvolvedEntry($indexVar['drUserId'], true);
			$prevDocumentId = 0;
			$nextDocumentId = 0;
			for($i=0;$i<count($needToFollowEntryAry);$i++){
				if($needToFollowEntryAry[$i] == $DocumentID){
					if(isset($needToFollowEntryAry[$i-1])){
						$nextDocumentId = $needToFollowEntryAry[$i-1];
					}
					if(isset($needToFollowEntryAry[$i+1])){
						$prevDocumentId = $needToFollowEntryAry[$i+1];
					}
					break;
				}
			}
			if($prevDocumentId > 0)
				$prevDocumentLink = 'index.php?pe='.$ldocrouting->getEncryptedParameter('management', 'dr_detail', array('documentId' => $prevDocumentId));
			if($nextDocumentId > 0)
				$nextDocumentLink = 'index.php?pe='.$ldocrouting->getEncryptedParameter('management', 'dr_detail', array('documentId' => $nextDocumentId));
			
			
			$tok = $indexVar['tok'];
			
			$link_print = 'javascript:newWindow(\'index.php?pe='.$ldocrouting->getEncryptedParameter('management','print_document_detail',array('DocumentID'=>$DocumentID)).($tok!=''?'&tok='.$tok:'').'\',37);';
			$archive_doc_link = 'javascript:void(0);';
			
			$x = '';
			$x .= '<div class="content_top_tool">';
				$x .= $this->GET_LNK_PRINT_IP25($link_print);
			if($canArchiveFile && $tok == ''){
				$x .= $this->GET_LNK_UPLOAD($archive_doc_link,'LoadArchiveDocumentTBForm('.$DocumentID.');',$Lang['DocRouting']['ArchiveDocument'], '', '', 0);
			}
			if($isCompleted){
				$send_doc_email_link = $ldocrouting->getEncryptedParameter('management', 'ajax_get_send_archived_document_email_form', array('DocumentID' => $DocumentID));	
				$x .= $this->GET_LNK_EMAIL("javascript:js_GetSendArchivedDocumentEmailForm('".$send_doc_email_link."');", $Lang['DocRouting']['SendEmail'], "", "", "", 0);
			}
			if($tok != '' && $sys_custom['DocRouting']['RequirePasswordToSign'] && $_SESSION['DR_TMP_USERID']>0){
				$x .= '<div style="float:right;">'.$this->GET_SMALL_BTN($i_admintitle_sa_password, "button", "jsGetChangePasswordForm();", "ChangePasswordBtn", "", "", $i_admintitle_sa_password).'</div>';
			}
			$x .= '</div>';
			$x .= '<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr> 
					  <td class="main_content"> 
					  	<div class="dr_list_header dr_list_header_detail">';
					 $x .= '<a href="javascript:void(0);" onclick="js_Update_Star_Status(this);" class="'.($stared?'btn_set_important_on':'btn_set_important').'"></a>';
					 $x .= '<h1>'.intranet_htmlspecialchars($title).'</h1>';
					 if($sys_custom['DocRouting']['RequestNumber']){
					 	$x .= '<span class="date_time"> '.$Lang['DocRouting']['RequestNumber'].': '.$DocumentID.'</span>';
					 }
					 	$x .= '<span class="member_pic_name" title="'.$Lang['DocRouting']['Creator'].'">'.$creatorName.$Lang['General']['On'].$docInputTime.'</span>';
					 
					 $x .= '<p class="spacer"></p>
					   </div>';
			if($isDeleted){
			 	$libuser = new libuser($entry[0]['ModifiedBy']);
			 	$x .= '<div style="float:right;margin-bottom:20px;font-weight:bold;">'.str_replace(array('<!--USERNAME-->','<!--DATETIME-->'),array($libuser->StandardDisplayName,$docModifiedDate),$Lang['DocRouting']['DeletedBy']).'</div>';
			 	$x .= '<br style="clear:both;" />';
			}
			$x .= '<div class="dr_intru_file">
						<div class="dr_intru_detail">'.$instruction.'</div>';
									
						if ($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location']) {
							$x .= '<div class="dr_physical_detail">';
								$x .= $Lang['DocRouting']['PhysicalLocation'].': '.intranet_htmlspecialchars($physicalLocation);
								$x .= '<p class="spacer"></p>';
							$x .= '</div>';
						}
						else if ($numOfAttachment > 0){	
							$param_tok = $tok!=''?'&tok='.$tok:'';
								
							$x .= '<div class="dr_file_detail">';
								$x .= Get_Plural_Display($numOfAttachment,$Lang['DocRouting']['Attachment']);
								if($numOfAttachment > 1){
									//$x .= '<a href="javascript:void(0);" class="download_all">('.$Lang['DocRouting']['DownloadAll'].')</a>';
								}
								$x .= '<ul>';
								for($i=0;$i<$numOfAttachment;$i++){
									$attachment_path = $attachments[$i]['FilePath'];
									$attachment_name = $attachments[$i]['FileName'];
									$attachment_id = $attachments[$i]['FileID'];
									$version_num = $attachments[$i]['VersionNumber'];
									//if($attachment_name == $attachments[$i+1]['FileName'] || $version_num > 1) {
									//	$display_version = '&nbsp;('.$Lang['DocRouting']['Version'].'&nbsp;'.$version_num.')';
									//}else{
										$display_version = '';
									//}
									if($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']){
									   $file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],'LinkToID'=>$DocumentID,'FileID'=>$attachment_id));
									}
									if($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA']){
									   $file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],'LinkToID'=>$DocumentID,'FileID'=>$attachment_id));
									}
									$file_icon = $this->Get_File_Type_Icon(get_file_ext($attachment_name));
									
									$x .= '<li>
												<a href="index.php?pe='.$file_param.$param_tok.'" target="_blank">'.$file_icon.$attachment_name.$display_version.'</a>
											</li>';
								}
								if($canArchiveFile && $tok == '') {
									$x .= '<li><a href="javascript:void(0);" onclick="LoadArchiveTBForm(\''.$DocumentID.'\',\'document\');" title="'.$Lang['DocRouting']['ArchiveFilesToDigitalArchive'].'">[ '.$Lang['DigitalArchiveModuleUpload']['Archive'].' ]</a></li>';
								}
								$x .= '</ul>';
								$x .= '<p class="spacer"></p>
									</div>';
						}
			$x .= '</div>';
			
			$x .= $this->getStarredFilesDiv($DocumentID);
			
			for($i=0;$i<$numOfRoutes;$i++)
			{
				$route_id = $routes[$i]['RouteID'];
				
				if($isDraft || $isCompleted || $isDeleted || (isset($routeStatusAssoc[$DocumentID][$route_id]) 
						&& $routeStatusAssoc[$DocumentID][$route_id] == $docRoutingConfig['routeStatus']['inProgress']
						|| $routeStatusAssoc[$DocumentID][$route_id] == $docRoutingConfig['routeStatus']['completed']))
				{
					$x .= '<div id="DivRoute_'.$route_id.'">';
						$x .= $this->getRoutingDetailBlock($route_id,$routes[$i]);
					$x .= '</div>';
				}
			}
			
			switch($recordStatus)
		    {
		    	case $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']:
		    		$page_path = 'completed_routings';
		    	break;
		    	case $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Deleted']:
		    		$page_path = 'deleted_routings';
		    	break;
		    	case $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']:
		    		$page_path = 'draft_routings';
		    	break;
		    	default :
		    		if(isset($indexVar['paramAry']['pageType']) && $indexVar['paramAry']['pageType'] == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
		    			$page_path = 'followed_routings';
		    		}else{
		    			$page_path ='index';
		    		}
		    } 
			$back_btn_param = $ldocrouting->getEncryptedParameter('management',$page_path,array('recordStatus'=>$recordStatus,'pageType'=>$indexVar['paramAry']['pageType']));
			
			if($tok == '')
			{
				$x .= '<div class="edit_bottom_v30">
			               <p class="spacer"></p>';
			        if($prevDocumentLink){
			        	$x .= $this->GET_ACTION_BTN('<<< '.$Lang['DocRouting']['PreviousRouting'], "button", "window.location.href='".$prevDocumentLink."'").'&nbsp;';
			        }
			        $x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location.href='index.php?pe=".$back_btn_param."'");
			        if(($creatorUserID == $indexVar['drUserId'] || $indexVar['drUserIsAdmin']) && !$isDraft && !$isDeleted){
			        	if(!$isCompleted){
			        		$status_word = $Lang['DocRouting']['CompleteRouting'];
			        		$status_flag = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete'];
			        	}else{
			        		$status_word = $Lang['DocRouting']['ActivateDocument'];
			        		$status_flag = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active'];
			        	}
			        	//if($is_all_routes_completed){
			        		$x .= '&nbsp;'.$this->GET_ACTION_BTN($status_word,"button", "js_Update_Doc_Status(".$DocumentID.",'".$status_flag."');");
			        	//}
			        }
			        if($allowAdHocRoute==1 && !$isCompleted && !$isDraft && !$isDeleted){
			        	$x .= '&nbsp;'.$this->GET_ACTION_BTN($Lang['DocRouting']['AddAdHocRoute'].' ('.$Lang['DocRouting']['Route'].' '.($numOfRoutes + 1).')',"button", "js_Get_AddHoc_Routing_Form(".$DocumentID.");");
			        }
			        if($nextDocumentLink){
			        	$x .= '&nbsp;'.$this->GET_ACTION_BTN($Lang['DocRouting']['NextRouting'].' >>>', "button", "window.location.href='".$nextDocumentLink."'");
			        }
			        $x .= '<p class="spacer"></p>
			           </div>';
			}
			$x .= '<p>&nbsp;</p>';
			$x .= '</td>';
			
			//$x .= '<td width="11" background="'.$image_path.'/'.$LAYOUT_SKIN.'/content_06.gif">&nbsp;</td>';
			
			$x .= '</tr></table><br />';
			
			return $x;
		}
		
		function getRoutingDetailBlock($routeID,$routeDataAry=array())
		{
			global $Lang, $PATH_WRT_ROOT, $xmsg, $indexVar, $docRoutingConfig, $plugin, $replySlipConfig, $intranet_root, $intranet_session_language, $sys_custom;

			$ldocrouting = $indexVar['libDocRouting'];
			$libReplySlipMgr = $indexVar['libReplySlipMgr'];
			$libTableReplySlipMgr = $indexVar['libTableReplySlipMgr'];
			
			$ldamu = $indexVar['libdigitalarchive_moduleupload'];	    
			if(!$ldamu){
				include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");
				$ldamu = new libdigitalarchive_moduleupload();
			}    	
			$canArchiveFile = $ldamu->User_Can_Archive_File();
				
			if(count($routeDataAry)==0){
				$tmpAry = $ldocrouting->getRouteData('', $routeID);
				$routeDataAry = $tmpAry[0];
			}
			$document_id = $routeDataAry['DocumentID'];
			
			if(!isset($indexVar['DocEntry'])){
				$indexVar['DocEntry'] = $ldocrouting->getEntryData($document_id);
			}
			$docEntry = $indexVar['DocEntry'];
			
			//$document_id = $docEntry[0]['DocumentID'];
			$doc_status = $docEntry[0]['RecordStatus'];
			$doc_creator_id = $docEntry[0]['UserID'];
			$is_doc_drafting = $doc_status == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft'];
			$is_doc_completed = $doc_status == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete'];
			$is_doc_deleted = $doc_status == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Deleted'];
			
			$tok = $indexVar['tok'];
			if(isset($indexVar['tok']) && $tok != '' && !isset($indexVar['signDocumentData'])){
				$signDocumentDataArr = $ldocrouting->getSignEmailDocumentByToken($tok);
				$indexVar['signDocumentData'] = $signDocumentDataArr[0];
				$signRouteAry = $ldocrouting->getSignEmailRoute($indexVar['signDocumentData']['RecordID']);
				$signRouteIdToRecord = array();
				for($i=0;$i<count($signRouteAry);$i++){
					$signRouteIdToRecord[$signRouteAry[$i]['RouteID']] = $signRouteAry[$i];
				}
				$indexVar['signRouteIdToRecord'] = $signRouteIdToRecord;
			}
			
			//if(count($routeDataAry)==0){
			//	$tmpAry = $ldocrouting->getRouteData('', $routeID);
			//	$routeDataAry = $tmpAry[0];
			//}
			
			$route_id = $routeDataAry['RouteID'];
			$route_creator_id = $routeDataAry['UserID'];
			$creator_name = $routeDataAry['CreatorName'];
			$routeInputDate = $routeDataAry['DateInput'];
			$routeModifiedDate = $routeDataAry['DateModified'];
			$note = $routeDataAry['Note'];
			$effective_type = $routeDataAry['EffectiveType'];
			$effective_startdate = $routeDataAry['EffectiveStartDate'];
			$effective_enddate = $routeDataAry['EffectiveEndDate'];
			$view_right = $routeDataAry['ViewRight'];
			$enable_lock = $routeDataAry['EnableLock'];
			$readOnlyUserViewDetail = $routeDataAry['ReadOnlyUserViewDetail'];
			$actions_ary = explode(",",$routeDataAry['Actions']);
			//$allow_sign = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['SignConfirm'],$actions_ary);
			$allow_sign = true;
			$allow_replyslip = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'],$actions_ary);
			$allow_comment = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'],$actions_ary) || in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'],$actions_ary);
			//Henry added [20140523][start]
			$allow_comment_read_by_own = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'],$actions_ary);
			//Henry added [20140523][end]
			$allow_attachment = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'],$actions_ary) || in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'],$actions_ary);
			//Henry added [20140523][start]
			$allow_attachment_read_by_own = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'],$actions_ary);
			$allowSubmitAfterEndDate = true;
			if($effective_type == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period'] && $routeDataAry['EffectiveEndDate'] < date('Y-m-d H:i:s') && $routeDataAry['EnableSubmitAfterEndDate'] == 0)
				$allowSubmitAfterEndDate = false;
			//Henry added [20140523][end]
			$reply_slip_id = $routeDataAry['ReplySlipID'];
			$reply_slip_type = $routeDataAry['ReplySlipType'];
			$reply_slip_release_type = $routeDataAry['ReplySlipReleaseType'];
			$route_user_feedbacks = $ldocrouting->getRouteUserFeedbacks($route_id,array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']));
			$route_users = $ldocrouting->getRouteTargetUsers($route_id, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']));
			$route_readonly_users = $ldocrouting->getRouteTargetUsers($route_id, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['View']));
			$route_readonly_user_ids = Get_Array_By_Key($route_readonly_users,'UserID');
			$is_readonly_user = in_array($indexVar['drUserId'],$route_readonly_user_ids);
			$route_draft_record = $ldocrouting->getRouteDraftRecords($indexVar['drUserId'],$routeID);
			$route_draft_content = '';
			if(count($route_draft_record)>0) {
				$route_draft_content = $route_draft_record[0]['Comment'];
				$route_draft_time = $route_draft_record[0]['DateModified'];
			}
			
			//debug_r($route_user_feedbacks);
			$pass_current_css = 'current';
			$status_header_css = '';
			if($effective_type == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){
				$ts = time();
				if($ts < strtotime($effective_startdate)){
					$pass_current_css = 'pass';
				}
				$status_header_css = ' status_board_header_time';
			}
			
			//debug_r($route_users);
			$userStatusAry = array();
			//$feedbackAry = array();
			$feedbackIdAry = Get_Array_By_Key($route_user_feedbacks,'FeedbackID');
			$userInRouteTarget = (in_array($indexVar['drUserId'], Get_Array_By_Key($route_users, 'UserID')))? true : false;
			$self_status = $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'];
			
			for($j=0;$j<count($route_users);$j++){
				$record_status = $route_users[$j]['RecordStatus'];
				if(!isset($userStatusAry[$record_status])){
					$userStatusAry[$record_status] = array();
				}
				if($route_users[$j]['UserID'] == $indexVar['drUserId']){
					if($record_status == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']){
						$ldocrouting->updateTargetUserStatus($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'],$route_id,$indexVar['drUserId']);
				 		$self_status = $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'];
				 	}else{
				 		$self_status = $record_status;
				 	}
				}
				$userStatusAry[$record_status][] = $route_users[$j];
			}
			
			$feedback_attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'], $feedbackIdAry);
			$feedbackIdToFile = array();
			//debug_r($feedback_attachments);
			for($j=0;$j<count($feedback_attachments);$j++){
				if(!isset($feedbackIdToFile[$feedback_attachments[$j]['LinkToID']])){
					$feedbackIdToFile[$feedback_attachments[$j]['LinkToID']] = array();
				}
				$feedbackIdToFile[$feedback_attachments[$j]['LinkToID']][] = $feedback_attachments[$j];
			}
			//debug_r($feedbackIdToFile);
			$x .= '<div class="status_board" id="status_board_'.$pass_current_css.'">
					  	<div class="status_board_header'.$status_header_css.'">
					    	<div class="status_icon">
								<div class="status_arrow"></div>';
						if($effective_type == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){
						 	$format_startdate = date("d/m/Y",strtotime($effective_startdate));
						 	$format_enddate = date("d/m/Y",strtotime($effective_enddate));
						 	$x .= '<span class="status_time">'.$format_startdate.' ~ '.$format_enddate.'</span>';
						}
						 $x .= '<span class="icon_member_status">';
//						 if($sys_custom['EditRoutingPeriodAndSendEmail']){
//							 if($effective_type != $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period'])
//							 	$x .= '<a href="javascript:js_Get_Edit_Routing_Period_Send_Email('.$document_id.');">['.$Lang['Btn']['Edit'].']</a>';						 
//						 }
						 $x .= '</span>';
						 //Henry Added [20140528]
						 if($sys_custom['EditRoutingPeriodAndSendEmail'] && !$is_doc_completed && !$is_doc_deleted && ($indexVar['drUserIsAdmin'] || $routeDataAry['UserID'] == $indexVar['drUserId']) && $tok==''){
						 	if($effective_type == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){
						 		$x .= '<span style="position: relative; top: -20px; float: right; margin-right: -25px; margin-bottom: -25px;"><a href="javascript:js_Get_Edit_Routing_Period_Send_Email('.$document_id.');">['.$Lang['Btn']['Edit'].']</a></span>';
						 	}
						 	else{
						 		$x .= '<br/><span style="position: relative; float: right; margin-right: -25px; white-space:nowrap;"><a href="javascript:js_Get_Edit_Routing_Period_Send_Email('.$document_id.');">['.$Lang['Btn']['Edit'].']</a></span>';						 
						 	}
						 }
						 $x .= '</div>
					        <div class="member_status">';
					     $x .= '<ul class="member_status_summary" id="MemberListBtn_'.$route_id.'">';
					     	$x .= '<li>';
					     		$x .= '<a href="javascript:void(0);" onclick="js_Show_Hide_Member_Status(this,'.$route_id.','.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'].');">';
					     			$x .= '<span class="member_done">'.$Lang['General']['Completed'].' ('.count($userStatusAry[$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']]).')</span>';
					     		$x .= '</a>';
					     	$x .= '</li>';
					     	$x .= '<li>';
					     		$x .= '<a href="javascript:void(0);" onclick="js_Show_Hide_Member_Status(this,'.$route_id.','.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'].');">';
					     			$x .= '<span class="memmber_read">'.$Lang['DocRouting']['InProgress'].'('.count($userStatusAry[$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']]).')</span>';
					     		$x .= '</a>';
					     	$x .= '</li>';
					     	$x .= '<li>';
					     		$x .= '<a href="javascript:void(0);" onclick="js_Show_Hide_Member_Status(this,'.$route_id.','.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'].');">';
					     			$x .= '<span class="member_waiting">'.$Lang['DocRouting']['NotViewedYet'].'('.count($userStatusAry[$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']]).')</span>';
					     		$x .= '</a>';
					     	$x .= '</li>';
					     	$x .= '<p class="spacer"></p>';
					     $x .= '</ul>';
					     //$x .= '<br style="clear:both;"/>';
					     /*
						$x .= '<a href="javascript:void(0);" class="member_status_summary" onclick="js_Show_Hide_Member_Status('.$route_id.');">
					            	<span class="member_done">'.$Lang['General']['Completed'].' ('.count($userStatusAry[$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']]).')</span>
					                <span class="memmber_read">'.$Lang['eDiscipline']['InProgress'].'('.count($userStatusAry[$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']]).')</span>
					                <span class="member_waiting">'.$Lang['DocRouting']['NotViewedYet'].'('.count($userStatusAry[$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']]).')</span>                  
					                <p class="spacer"></p>                            
					            </a>';
					      */      
					    $completed_users = $userStatusAry[$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']];
						$inprogress_users = $userStatusAry[$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']];
						$notviewed_users = $userStatusAry[$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']];
						// Completed users
						$x .= '<div class="member_status_list" id="member_status_list_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'].'" style="visibility:visible;display:none;clear:both;">          
							   		<ul>'."\n";
								for($j=0;$j<count($completed_users);$j++){
									$user_name = $completed_users[$j]['UserName'];
									//$modify_date = date("d/m/Y", strtotime($completed_users[$j]['FeedbackModifyDate']));
									$x .= '<li><em></em><span class="member_done">'.$user_name.'</span></li>'."\n";
								}
						/*	$x .= '<li class="member_status_list_btn">
											<em><input name="CheckAllUsers_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'].'" id="CheckAllUsers_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'].'" type="checkbox" value="" onclick="js_Check_All_Users('.$route_id.',this.checked);" /></em><label for="CheckAllUsers_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'].'">'.$Lang['Btn']['All'].'</label>';
									if(!$is_doc_drafting && !$is_doc_completed){
										$x .= '<a href="javascript:void(0);" class="btn_sent_reminder" onclick="js_Send_Reminder('.$route_id.');">'.$Lang['DocRouting']['SendReminder'].'</a>';
									}
							$x .= '</li>'."\n"; */
							$x .= '</ul>           		
						    		<p class="spacer"></p>
					    		</div>'."\n";
					    // In progress users	
					    $x .= '<div class="member_status_list" id="member_status_list_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'].'" style="visibility:visible;display:none;clear:both;">          
							   		<ul>'."\n";
								for($j=0;$j<count($inprogress_users);$j++){
									$uid = $inprogress_users[$j]['UserID'];
									$user_name = $inprogress_users[$j]['UserName'];
									//$modify_date = date("d/m/Y", strtotime($inprogress_users[$j]['FeedbackModifyDate']));
									$x .= '<li>';
									if($tok == '' && ($indexVar['drUserIsAdmin'] || $indexVar['drUserId']==$doc_creator_id || $indexVar['drUserId']==$route_creator_id)){
										$x .= '<em><input name="InprogressUsers_'.$route_id.'[]" id="InprogressUsers_'.$route_id.'_'.$uid.'" type="checkbox" value="'.$uid.'" /></em>';
									}
										$x .= '<label for="InprogressUsers_'.$route_id.'_'.$uid.'"><span class="memmber_read">'.$user_name.'</span></label>';
									$x .= '</li>'."\n";
								}
							if($tok=='' && ($indexVar['drUserIsAdmin'] || $indexVar['drUserId']==$doc_creator_id || $indexVar['drUserId']==$route_creator_id))
							{
								$x .= '<li class="member_status_list_btn">
												<em><input name="CheckAllUsers_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'].'" id="CheckAllUsers_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'].'" type="checkbox" value="" onclick="js_Check_All_Users('.$route_id.',this.checked);" /></em><label for="CheckAllUsers_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'].'">'.$Lang['Btn']['All'].'</label>';
										if(!$is_doc_drafting && !$is_doc_completed && !$is_doc_deleted){
											$x .= '<a href="javascript:void(0);" class="btn_sent_reminder" onclick="js_Send_Reminder('.$route_id.');">'.$Lang['DocRouting']['SendReminder'].'</a>';
										}
								$x .= '</li>'."\n";
							}
							$x .= '</ul>           		
						    		<p class="spacer"></p>
					    		</div>'."\n";
					    // Not viewed users 
					    $x .= '<div class="member_status_list" id="member_status_list_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'].'" style="visibility:visible;display:none;clear:both;">          
							   		<ul>';
								for($j=0;$j<count($notviewed_users);$j++){
									$uid = $notviewed_users[$j]['UserID'];
									$user_name = $notviewed_users[$j]['UserName'];
									//$modify_date = date("d/m/Y", strtotime($notviewed_users[$j]['FeedbackModifyDate']));
									$x .= '<li>';
									if($tok == '' && ($indexVar['drUserIsAdmin'] || $indexVar['drUserId']==$doc_creator_id || $indexVar['drUserId']==$route_creator_id)){
										$x .= '<em><input name="NotViewedUsers_'.$route_id.'[]" id="NotViewedUsers_'.$route_id.'_'.$uid.'" type="checkbox" value="'.$uid.'" /></em>';
									}	
										$x .= '<label for="NotViewedUsers_'.$route_id.'_'.$uid.'"><span class="member_waiting">'.$user_name.'</span></label>';
									$x .= '</li>';
								}
								if($tok=='' && ($indexVar['drUserIsAdmin'] || $indexVar['drUserId']==$doc_creator_id || $indexVar['drUserId']==$route_creator_id))
								{
									$x .= '<li class="member_status_list_btn">
											<em><input name="CheckAllUsers_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'].'" id="CheckAllUsers_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'].'" type="checkbox" value="" onclick="js_Check_All_Users('.$route_id.',this.checked);" /></em><label for="CheckAllUsers_'.$route_id.'_'.$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'].'">'.$Lang['Btn']['All'].'</label>';
									if(!$is_doc_drafting && !$is_doc_completed && !$is_doc_deleted){
										$x .= '<a href="javascript:void(0);" class="btn_sent_reminder" onclick="js_Send_Reminder('.$route_id.');">'.$Lang['DocRouting']['SendReminder'].'</a>';
									}
									$x .= '</li>';
								}
							$x .= '</ul>           		
						    		<p class="spacer"></p>
					    		</div>
					   		</div>
					    	<p class="spacer"></p>
						</div>';
				
			$x .= '<p class="spacer"></p>
				  	<div class="status_comment">
				    	<h1><span title="'.$Lang['DocRouting']['Creator'].'">'.$creator_name.$Lang['General']['On'].$routeInputDate.'</span></h1><br /><br /><div style="padding:8px;">'.$note.'</div>
				        <div class="member_comment">
				        	<ul class="triangle-border ">';
						
			for($k=0;$k<count($route_user_feedbacks);$k++)
			{
				$user_id = $route_user_feedbacks[$k]['UserID'];
				$user_name = $route_user_feedbacks[$k]['UserName'];
				$feedback_id = $route_user_feedbacks[$k]['FeedbackID'];
				$comment = $route_user_feedbacks[$k]['Comment'];
				$modify_date = $route_user_feedbacks[$k]['FeedbackModifyDate'];
				
				// Henry Added [20140523] [start]
				$allow_show_comment = true;
				$allow_show_attachment = true;
				if(!($routeDataAry['UserID'] == $indexVar['drUserId'] || $user_id == $indexVar['drUserId'] || $indexVar['drUserIsAdmin'])){
					if($allow_comment_read_by_own)
						$allow_show_comment = false;
					if($allow_attachment_read_by_own)
						$allow_show_attachment = false;
					if($is_readonly_user && $readOnlyUserViewDetail){
						$allow_show_comment = true;
						$allow_show_attachment = true;
					}
				}
//				debug_pr($feedbackIdToFile[$feedback_id]);
//				debug_pr($comment);
				if(($allow_comment_read_by_own && $comment && !$feedbackIdToFile[$feedback_id] && !$allow_show_comment) || ($allow_attachment_read_by_own && !$comment && $feedbackIdToFile[$feedback_id] && !$allow_show_attachment) || ($allow_comment_read_by_own && $allow_attachment_read_by_own && !$allow_show_comment && !$allow_show_attachment)){
					continue;
				}
				// Henry Added [20140523] [end]
				
				$x .= '<li>
							<span>'.$user_name.'</span>';
				if($sys_custom['DocRouting']['RequestNumber'] && $user_id == $indexVar['drUserId'] && !$is_doc_deleted){
					$x .= '<span class="table_row_tool row_content_tool" style="float:right"><a href="javascript:void(0);" class="delete" title="'.$Lang['Btn']['Delete'].'" onclick="js_Delete_Feedback('.$route_id.','.$feedback_id.');" style="padding-left:0px;"></a></span>';
				}
					$x .= '<br />';
				
				if($allow_comment && $allow_show_comment){
					//$x .= nl2br(intranet_htmlspecialchars($comment));
					$x .= $comment;
				}
				$x .= '<p class="spacer"></p>
		                <div class="comment_info2">
		                    <span>'.$modify_date.'</span>';
		        if($allow_attachment && $allow_show_attachment){
		        	$user_attachments = $feedbackIdToFile[$feedback_id];
		        	$tmp_file_ids = Get_Array_By_Key($user_attachments,'FileID');
		        	$user_file_stars = $ldocrouting->getUserFileStarRecords($tmp_file_ids);
		        	for($n=0;$n<count($user_attachments);$n++){
		        		$file_id = $user_attachments[$n]['FileID'];
		        		$file_name = $user_attachments[$n]['FileName'];
		        		$file_path = $user_attachments[$n]['FilePath'];
		        		$file_type = $user_attachments[$n]['RecordType'];
		        		$file_star_on = in_array($file_id,$user_file_stars);
		        		$file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'],'LinkToID'=>$feedback_id,'FileID'=>$file_id));
		        		if($tok != ''){
		        			$file_param .= '&tok='.$tok;
		        		}
		        		if($file_type == $docRoutingConfig['INTRANET_DR_FILE']['RecordType']['PowerVoice']){
		        			if($plugin['power_voice']){
		        				//$full_path = $docRoutingConfig['filePath'].str_replace($docRoutingConfig['dbFilePath'],"",$file_path);
		        				$full_path = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=".$file_param;
		        				$x .= '<span class="sep">|</span>';
		        				$x .= '<div class="dr_list_header" style="float:left;padding:0px 0px 0px 16px;margin:0px;border:0px none;width:8px;">
											<a class="btn_set_important'.($file_star_on?'_on':'').'" href="javaScript:void(0)" onclick="js_Star_File(this,'.$file_id.')"></a>
										</div>';
								//$x .= '<a href="index.php?pe='.$file_param.'" class="icon_voice_attach">'.$file_name.'</a>';
								$x .= '<a href="javascript:void(0);" class="icon_voice_attach" onclick="listenPVoice2(\''.$full_path.'\');">'.$file_name.'</a>';
		        			}
		        		}else{
							$x .= '<span class="sep">|</span>
									<div class="dr_list_header" style="float:left;padding:0px 0px 0px 16px;margin:0px;border:0px none;width:8px;">
										<a class="btn_set_important'.($file_star_on?'_on':'').'" href="javaScript:void(0)" onclick="js_Star_File(this,'.$file_id.')"></a>
									</div>
		                    		<a href="index.php?pe='.$file_param.'" class="icon_attachement">'.$file_name.'</a>';
		        		}
		        		
		        		if($user_id == $indexVar['drUserId'] && 
		        			$self_status != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']
		        			&& !$is_doc_drafting && !$is_doc_completed && !$is_doc_deleted && $allowSubmitAfterEndDate){
		                	$x .= '<span class="table_row_tool row_content_tool">';
							$x .= '<a href="javascript:void(0);" class="delete" title="'.$Lang['Btn']['Delete'].'" onclick="js_Delete_File('.$route_id.','.$file_id.','.$feedback_id.');" style="padding-left:0px;"></a>';
							$x .= '</span>';
		                }		             	
		        	}
		        }
				//Henry Modified
				
					if(count($user_attachments) > 0 && $canArchiveFile && $tok=='') {
						$x .= '<span><a href="javascript:void(0);" style="font-weight: normal;" onclick="LoadArchiveTBForm(\''.$feedback_id.'\',\'feedback\');" title="'.$Lang['DocRouting']['ArchiveFilesToDigitalArchive'].'">[ '.$Lang['DigitalArchiveModuleUpload']['Archive'].' ]</a></span>';
					}
	                 $x .= '<p class="spacer"></p>';
	                $x .= '</div>';
	                $x .= '</li>';
			}
				
				
								
				if($userInRouteTarget && !$is_doc_drafting && !$is_doc_completed && !$is_doc_deleted && $self_status != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'] && $allowSubmitAfterEndDate){
				 	
				 	if($allow_comment && $route_draft_content != ''){
				 		$x .= '<li class="comment_draft" id="LiRouteDraft_'.$route_id.'">
								<span></span><br />';
						$x .= $route_draft_content;
				 		$x .= '<p class="spacer"></p>
		                		<div class="comment_info2">
		                    		<span>'.str_replace('<!--DATETIME-->',$route_draft_time,$Lang['DocRouting']['DraftCommentRemark']).'</span>';
				 		$x .= '</div>
				                 <p class="spacer"></p>
				                </li>';
				 	}
				 	
				 	
//				 	if($enable_lock == 1) {
//						$js_give_feedback = 'js_Request_Lock('.$route_id.');';
//					}else{
//						$js_give_feedback = 'js_Show_Hide_Comment('.$route_id.');';
//					}
				 					 	
				 	if($enable_lock == 1) {
						$js_give_feedback = 'js_Request_Lock('.$route_id.');';
  			    	}else{
						if($allow_comment) $js_give_feedback = 'js_Show_Hide_Comment('.$route_id.', true);';
						
						else $js_give_feedback = 'js_Show_Hide_Comment('.$route_id.');';
					}
				 	
				 	
				 	## Comment Box Display
					if(($allow_comment || $allow_attachment)){    
						 $x .= '<li class="write_comment" id="write_comment_'.$route_id.'">';
						 	if($allow_comment){
						 		//$x .= '<textarea cols="" rows="2" wrap="virtual" id="Comment_'.$route_id.'"></textarea>';
						 		
						 		include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
								# HTML Editor
								$objHtmlEditor = new FCKeditor ( "Comment_$route_id" , "100%", "220", "", "",$route_draft_content);
							
								$HTMLEditor = $objHtmlEditor->Create2();
						 		$x .= $HTMLEditor;					 		
						 	}
							$x .= '<div class="comment_info2">';
							if($allow_attachment){
								$x .= '<a href="javascript:js_Get_Upload_Form('.$route_id.',\'\');" class="icon_add_attachement">'.$Lang['DocRouting']['AddAttachment'].'</a>';
								if($plugin['power_voice']){
									//$x .= '<a href="javascript:void(0);" class="icon_voice_attach">Add Voice</a>';
									$x .= $this->getPowerVoiceEditor($route_id);
								}
							}	
								$x .= '<input class="formsubbutton" type="button" value="'.$Lang['Btn']['Cancel'].'" onClick="'.$js_give_feedback.'" />';
							if($allow_comment){	
								//debug_r($document_id);
								$x .= '<input class="formsubbutton" type="button" value="'.$Lang['DocRouting']['SaveAsDraft'].'" onclick="js_Save_Draft(\''.$route_id.'\');" />';
								$x .= '<input class="formsubbutton" type="button" value="'.$Lang['DocRouting']['SubmitComment'].'" onclick="js_Submit_Comment(\''.$document_id.'\',\''.$route_id.'\');" />';
							}	
								$x .= '</div>
				                 	<p class="spacer"></p>
				              	</li>';
					 }
					        
						$x .= '</ul>';
																						
						$x .= '<span class="comment_board_arrow">
					            '.$Lang['DocRouting']['Management']['BeforeSigningRemind'];
							$x .= '<a href="javascript:void(0);" onClick="'.$js_give_feedback.'">&nbsp;';
					      	if($allow_comment){	
					      		if($route_draft_content != '') {
					      			$x .= $Lang['DocRouting']['Management']['EditYourComment'];
					      		}else {
					      			$x .= $Lang['DocRouting']['Management']['WriteYourComment'];
					      		}
					      	}
					      	if($allow_attachment){
					      		if($allow_comment){
					      			$x .= ", ";
					      		}
					      		$x .= $Lang['DocRouting']['UploadAttachments'];
					      	}
							$x .= '</a>
							  </span>';
					
						//if($allow_comment && $route_draft_content != ''){	
						//	$x .= '<div class="tabletextremark" style="padding:5px;">('.str_replace('<!--DATETIME-->',$route_draft_time,$Lang['DocRouting']['DraftCommentRemark']).')</div>';
						//}
				}
			
				$x .= '</div>
				    </div>';
				   
			
			if($allow_replyslip && $reply_slip_id > 0){
				
				if($reply_slip_type == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
					$libTableReplySlip = new libTableReplySlip($reply_slip_id);
					$ShowUserInfoInResult = $libTableReplySlip->getShowUserInfoInResult();
				}else {
					$libReplySlip = new libReplySlip($reply_slip_id);
					$ShowUserInfoInResult = $libReplySlip->getShowUserInfoInResult();
				}

				$userNameDisplayRemark ='';
				if($ShowUserInfoInResult == $replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['show'])
				{											
					$userNameDisplayRemark = '<span style="color:red">' .$Lang['DocRouting']['WarningMsg']['UserNameWillBeDisplayInReplySlipResult']. '</span>';
				}else{
					$userNameDisplayRemark = '<span style="color:red">' .$Lang['DocRouting']['WarningMsg']['UserNameWillNotBeDisplayInReplySlipResult']. '</span>';	
				}
				
				# Online Release statistic form setting control
				//$routeDataArr = $indexVar['libDocRouting']->getRouteData($document_id, $route_id);
				$displyReleaseStatistic = false;
				if($reply_slip_release_type==$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseAnytime'])
				{
					$displyReleaseStatistic = true;
				}elseif($reply_slip_release_type==$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterSubmission'])
				{
					//$currentUserIDforReleaseStatistic =  $indexVar['drUserId'];
					//$replySlipIDforReleaseStatistic = $reply_slip_id;
					//$replySlipAnswer = $indexVar['libDocRouting']->getReplySlipUserAnswer($replySlipIDforReleaseStatistic, $currentUserIDforReleaseStatistic);
					
					//if(count($replySlipAnswer)>0){
					if($self_status == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']){
						$displyReleaseStatistic = true;
					}
					//}
					
				}elseif($reply_slip_release_type==$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterRouteCompeleted'])
				{
					if($doc_status == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']){
						$displyReleaseStatistic = true;
					}
				}
				
				// if cannot sign and statistics is available, default show statistics instead of reply slip answer
				$defaultShowStatistics = false;
				if ($displyReleaseStatistic) {
					if($tok != '' && $indexVar['signRouteIdToRecord'][$route_id]['Signed']==1){
						$defaultShowStatistics = true;
					}
					else {
						if( $userInRouteTarget && ($allow_sign || $allow_replyslip) && !$is_doc_drafting && !$is_doc_completed && !$is_doc_deleted
								&& $self_status != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'] && $allowSubmitAfterEndDate) {
						}
						else {
							$defaultShowStatistics = true;
						}
					}
				}
				if ($defaultShowStatistics) {
					$initJS = 'js_Show_Hide_Replyslip_Stat('.$route_id.','.$reply_slip_id.',\''.$reply_slip_type.'\');';
				}
				else {
					$initJS = '';
				}
							
				$x .= '<div class="reply_slip">
						  	<h1>
								<div class="reply_slip_header">'.$Lang['DocRouting']['WarningMsg']['PleaseFillInBelowReplySlip']. '<!-- ' . $userNameDisplayRemark . '--></div>';
				if($displyReleaseStatistic){			
					$x .='<a href="javascript:js_Show_Hide_Replyslip_Stat('.$route_id.','.$reply_slip_id.',\''.$reply_slip_type.'\');" class="btn_view_stat" title="'.$Lang['DocRouting']['ViewReplySlipStatistics'].'"></a>';
				}
					$x .='</h1>';
					
					//Henry Modified [Start]
					 $x .='<br/>';
					### Warning Msg Box
		 			$WarningBox = $this->GET_WARNING_TABLE('', '', '',$userNameDisplayRemark);
					$x .= '<table width="96%" align="center"><tr><td>'.$WarningBox.'</tr></td></table>';
					//Henry Modified [End]	
				
						    $x .='<p class="spacer"></p>
						    <div class="reply_slip_content" id="reply_slip_content_'.$route_id.'">';
						if($reply_slip_type == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
							$libTableReplySlipMgr->setCurUserId($indexVar['drUserId']);
							$libTableReplySlipMgr->setReplySlipId($reply_slip_id);
							$x .= $libTableReplySlipMgr->returnReplySlipHtml();
						} else {	    
						    $libReplySlipMgr->setCurUserId($indexVar['drUserId']);
							$libReplySlipMgr->setReplySlipId($reply_slip_id);
							$x .= $libReplySlipMgr->returnReplySlipHtml();
						}
					$x .= '</div>';
					$x .= '<p class="spacer"></p>';
					//$x .= '<div class="reply_slip_stat triangle-border top" id="reply_slip_stat_'.$route_id.'">';
					$x .= '<div id="reply_slip_stat_'.$route_id.'" style="display:none;">';
							
					$x .= '</div>
						</div>';
				if($is_doc_drafting || $is_doc_completed || $is_doc_deleted || $self_status == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'] || !$allowSubmitAfterEndDate){
					$disable_answer = '1';
				}else{
					$disable_answer = '0';
				}
					
				$x .= '<script type="text/javascript" language="JavaScript">'."\n";
					$x .= '$(document).ready(function(){'."\n";
					$x .= 'js_Get_Reply_Slip_Answer('.$route_id.','.$indexVar['drUserId'].','.$reply_slip_id.','.$disable_answer.','.$reply_slip_type.', "'.$initJS.'");'."\n";
					$x .= '});'."\n";
				$x .= '</script>'."\n";
				
			}
			
			if($tok != '' && $indexVar['signRouteIdToRecord'][$route_id]['Signed']==1){
				$signed_on = str_replace('<!--TIME-->',$indexVar['signRouteIdToRecord'][$route_id]['DateModified'],$Lang['DcoRouting']['SignedOn']);
				$x .= '<div class="reply_slip_content" >';
					$x .= '<div style="text-align:right;color:#157727;font-weight:bold;">'.$signed_on.'</div>';
				$x .= '</div>';
				/*else{
					$sign_link = $ldocrouting->getEncryptedParameter('management','sign_document_update',array('RecordID'=>$indexVar['signRouteIdToRecord'][$route_id]['RecordID'])).'&tok='.$tok;
					$x .= '<div class="reply_slip_content" >';
								$x .= '<ul>';
									$x .= '<li class="reply_slip_btn">';
									$x .= '<input type="button" value="'.$Lang['DocRouting']['SignAndConfirm'].'" class="formbutton" onclick="js_Sign_Routing_Update(\''.$sign_link.'\');" />';
									$x .= '</li>';
								$x .= '</ul>';
					$x .= '</div>';
				}*/
			}else{
				$x .= '<div class="reply_slip_content" >';
							$x .= '<ul>';
								$x .= '<li class="reply_slip_btn">';
								
							if( $userInRouteTarget && ($allow_sign || $allow_replyslip) && !$is_doc_drafting && !$is_doc_completed && !$is_doc_deleted 
								&& $self_status != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'] && $allowSubmitAfterEndDate){
								$x .= '<input type="button" value="'.$Lang['DocRouting']['SignAndConfirm'].'" class="formbutton" onclick="js_Sign_Routing('.$route_id.',\''.$reply_slip_id.'\',\''.$reply_slip_type.'\');" />';
								
							}
								$x .= '</li>';
							$x .= '</ul>';
				$x .= '</div>';
			}
			
			$x .= '</div>';
			
			return $x;
		}
		
		function getRoutingUploadAttachmentForm($routeId,$feedbackId, $comment)
		{
			global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $intranet_root,  $indexVar, $docRoutingConfig;
			
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			
			# Get Btns
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"Button","js_Check_Upload_Form();");
			$CloseBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"Button","js_Hide_ThickBox()");
			
			$form_param = $ldocrouting->getEncryptedParameter('management','ajax_feedback_upload_attachment',array());
			
			$x = '';
			$x .= '<form id="TBEditForm" name="TBEditForm" action="index.php?pe='.$form_param.'" method="post" encoding="multipart/form-data">'."\n";
				$x .= '<div style="overflow-x:hidden;overflow-y:auto;height:200px;">';
				$x .= '<table width="98%" cellpadding=10px cellspacing=10px align="center">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td valign="top">'."\n";	
							$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
								$x .= '<col class="field_title">';
								$x .= '<col class="field_c">';
								
								$FieldUpload = $Lang['DocRouting']['UploadAttachment'];
								
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title" nowrap><span class="tabletextrequire">*</span>'.$FieldUpload.'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= '<div id="DivUploadFile">';
											$x .= '<input name="UploadFile_0" type="file" class="Mandatory"><br />'."\n";
										$x .= '</div>';
										$x .= '<div class="table_row_tool row_content_tool" style="float:left;">';
										$x .= '<a href="javascript:void(0);" onclick="js_Add_File_Field();" class="add_dim" title="'.$Lang['Btn']['AddMore'].'"></a>';
										$x .= '</div>';
										$x .= $this->Get_Thickbox_Warning_Msg_Div("UploadFileWarningDiv",$Lang['DocRouting']['WarningMsg']['PleaseUploadAtLeastOneFile'], "WarnMsgDiv");
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
		
							$x .= '</table>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
				$x .= '</div>';
				$x .= $this->MandatoryField();		
				
				# Btns
				$x .= '<div class="edit_bottom_v30">'."\n";
					$x .= $SubmitBtn;
					$x .= "&nbsp;".$CloseBtn;
				$x .= '</div>'."\n";
				
				$x .= '<input type="hidden" name="TargetRouteID" value="'.$routeId.'" />';
				$x .= '<input type="hidden" name="TargetFeedbackID" value="'.$feedbackId.'" />';
				$x .= '<input type="hidden" name="TargetComment" value="'.$comment.'" />';
				
			$x .= '</form>';	
			$x .= '<iframe id="FileUploadFrame" name="FileUploadFrame" style="display:none;"></iframe>'."\n";
			//$x .= $this->FOCUS_ON_LOAD("TBForm.UploadImage");
			
			return $x;
		}
		
		function getPowerVoiceEditor($routeId)
		{
			global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $iPowerVoice, $intranet_root, $indexVar, $docRoutingConfig;
			global $button_clear;
			
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			
			$RecBtn = '<a href="javascript:editPVoice('.$routeId.');" id="pv_rec_btn_'.$routeId.'" class="icon_voice_attach">'.$Lang['DocRouting']['AddVoice'].'</a>';
			//$RecBtn = "<input type='button' name='pv_rec_btn_".$routeId."' value='".$iPowerVoice['powervoice']."' onclick='editPVoice()' />";
			$ClearBtn = '<input type="button" name="pv_clear_btn_'.$routeId.'" value="'.$button_clear.'" onclick="clearPVoice('.$routeId.')" />';
			$HiddenHtml = "<input type=\"hidden\" name=\"voiceFile_".$routeId."\" ><input type=\"hidden\" name=\"voicePath_".$routeId."\" >";
			$HiddenHtml .= "<input type=\"hidden\" name=\"voiceFileFromWhere_".$routeId."\" value=\"1\" >";
			$HiddenHtml .= '<input type="hidden" name="is_empty_voice_'.$routeId.'" value="" />';
			$VoiceClip = " <img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_voice.gif\" border=\"0\" align=\"absmiddle\" /> ";
					
			$voiceContent .= "<table cellpadding=\"0\" cellspacing=\"0\" class=\"inside_form_table\" >";
			$voiceContent .= "<tr>";
			$voiceContent .= "	<td><span id=\"voiceDiv_".$routeId."\" ></span></td>";
			$voiceContent .= "	<td>{$RecBtn}{$HiddenHtml}</td>";
			$voiceContent .= "	<td><span id=\"clearDiv_".$routeId."\" style=\"display:none\" >{$ClearBtn}</span></td>";
			$voiceContent .= "</tr>";
			$voiceContent .= "</table>";	
			
			//$PVHtml = "<tr>";
			//$PVHtml .= "<td class=\"field_title\">".$iPowerVoice['sound_recording']."</td>";
			//$PVHtml .= "<td>".$voiceContent."</td>"; 
			//$PVHtml .= "</tr>";
			
			$x  = '<span>'.$voiceContent."</span>";
			
			return $x;
		}
		
		function getAddHocRoutingForm($DocumentID)
		{
			global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $iPowerVoice, $intranet_root, $indexVar, $docRoutingConfig;
			
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			
			$indexVar['FromAddAdHocRouting'] = 1;
			
			$entry = $ldocrouting->getEntryData($DocumentID);
			$routes = $ldocrouting->getRouteData($DocumentID);
			$dataAry = $entry[0];
			$next_display_order = $routes[count($routes)-1]['DisplayOrder'] + 1;
			
			$title = $dataAry['Title'];
			//$instruction = $dataAry['Instruction'];
			//$documentType = $dataAry['DocumentType'];
			//$docTags = $dataAry['DocTags'];
			//$allowAdHocRoute = $dataAry['AllowAdHocRoute'];
			//$recordStatus = $dataAry['RecordStatus'];
			
			$form_action = $ldocrouting->getEncryptedParameter('management','addhoc_routing_update',array());
					
			$x = '<form name="form1" method="post" action="index.php?pe='.$form_action.'" enctype="multipart/form-data">';
			$x.= '<div id="TBEditDiv" style="height:530px; overflow-x:hidden; overflow-y:auto;">';
			$x.= '<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		        	<tr>
		        		<td class="main_content">
		        			<div class="table_board">
		        				<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						            <tr>
										<td bgcolor="#FFEECC"><b>'.$title.'</b></td>
						            </tr>
						        </table>';
			
			$x .= $this->addNewRouting($next_display_order, '', $DocumentID, $docRoutingConfig['INTRANET_DR_ROUTES']['RouteType']['AddHocRoute']);
			
						$x .= '<br />
								<div id="new_routing_display"></div>';
						$x .= '<p class="spacer"></p>
								</div>
							</td>
						</tr>
			        </table>
					<input type="hidden" id="DocumentID" name="DocumentID" value="'.$DocumentID.'" />';
			$x .= '<input type="hidden" id="DisplayOrder" name="DisplayOrder" value="'.$next_display_order.'">';
				$x .= '</td>
					</tr>
					</table>
				</div>	
				</form>';
			$x .= '<iframe id="UpdateFrame" name="UpdateFrame" style="display:none;"></iframe>'."\n";
			$x .= $this->MandatoryField();
			$x .= '<div class="edit_bottom">
			            <p class="spacer"></p>
			            '.$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Submit_AddHoc_Routing(".$DocumentID.");").'&nbsp;
			            '.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.tb_remove();").'
						<p class="spacer"></p>
			        </div>';	
			
			return $x;
		}
		
		
		
		function getRoutingDisplayTable($newRoutingLink, $DisplayUpdateLink,$searchBox,$instructionMessage, $newBtn='', $pageType='', $expireStatus=''){
			global $indexVar, $Lang, $docRoutingConfig;
			global $ck_doc_management_current_rountings_star_staus, $ck_doc_management_current_rountings_create_status, $ck_doc_management_current_rountings_follow_status;			
			global $ck_doc_management_current_routings_expire_status;
				
			$libuser = new libuser();
						
			# Check User's Access Right						
			if($indexVar['drUserIsAdmin']){
				$involvedOnly = false;
			}
			else{
				$involvedOnly = true;
			}
						
			$htmlAry['CurrentDisplay'] ='';
			$htmlAry['CurrentDisplay'] = '<form name="form1" method="post" action="'. '?pe='.$DisplayUpdateLink.'">';
				$htmlAry['CurrentDisplay'] .= '<table width="99%" border="0" cellspacing="0" cellpadding="0">';	
					$htmlAry['CurrentDisplay'] .= '<tr>';
						$htmlAry['CurrentDisplay'] .= '<td class="main_content">';
						//if($newBtn == true){
							$htmlAry['CurrentDisplay'] .= '<div class="content_top_tool">';
								$htmlAry['CurrentDisplay'] .= '<div class="Conntent_tool">';
									if ($newBtn) {
										$htmlAry['CurrentDisplay'] .= '<a class="new" href="'. '?pe='. $newRoutingLink.'">' . $Lang['Btn']['New']. '</a>';
									}
									if(!in_array($pageType,array($docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings'],$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DeletedRountings']))){
										# Add Copy Routing Link
										$copyRoutingLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'copy_doc', array('pageType' => $pageType));							
										$htmlAry['CurrentDisplay'] .= '<a class="copy" href="'. '?pe='. $copyRoutingLink.'">' . $Lang['DocRouting']['Button']['CopyExistingRounting']. '</a>';
										
										# Add Delete Routing Link
										$deleteRoutingLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'delete_doc', array('pageType' => $pageType));
										$htmlAry['CurrentDisplay'] .= '<a class="clear_record" href="'. '?pe='. $deleteRoutingLink.'">' . $Lang['DocRouting']['Button']['DeleteExistingRounting']. '</a>';
									}
									# Add Print Routing Link (Henry Added 20140704)
									$printRoutingLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'print_doc', array('pageType' => $pageType));							
									$htmlAry['CurrentDisplay'] .= '<a class="print" target = "_blank" href="'. '?pe='. $printRoutingLink.'">' .$Lang['Btn']['Print']. '</a>';
									
								$htmlAry['CurrentDisplay'] .= '</div>';		
						//}
							$htmlAry['CurrentDisplay'] .= $searchBox;
							$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p>';
							$htmlAry['CurrentDisplay'] .= '</div>';		
							$htmlAry['CurrentDisplay'] .= $instructionMessage;
							
							$htmlAry['CurrentDisplay'] .= '<div class="table_board">';
								$htmlAry['CurrentDisplay'] .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">';
									$htmlAry['CurrentDisplay'] .= '<tbody><tr>';
										$htmlAry['CurrentDisplay'] .= '<td valign="bottom">';
										
										if(in_array($pageType,array($docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings'],$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DeletedRountings']))){
											$academic_year_selection = $this->getAcademicYearSelection("academic_year", "academic_year", Get_Current_Academic_Year_ID());
											$htmlAry['CurrentDisplay'] .= $academic_year_selection;
										}
																				
										$starParSelected ='';
										
										if(isset($ck_doc_management_current_rountings_star_staus) && $ck_doc_management_current_rountings_star_staus!=''){
											$starParSelected = $ck_doc_management_current_rountings_star_staus;
										}
					
										# Star Filter
										$starParData []= array('', $Lang['DocRouting']['Management']['FilterStatus']['AllStarStatus'] );
										$starParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['StarRemarked'], $Lang['DocRouting']['Management']['FilterStatus']['StarRemarked'] );
										$starParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotStarRemarked'],$Lang['DocRouting']['Management']['FilterStatus']['NotStarRemarked']);										
										$starParTags = 'name ="starStatus" class="starStatus" id="starStatus" onchange="jsReloadCurrentRouting(0)" style="display:none;" ';
										
										
										//$starParDefault	= $Lang['DocRouting']['Management']['FilterStatus']['AllStarStatus'];
													
										$htmlAry['CurrentDisplay'] .= 	$this->GET_SELECTION_BOX($starParData, $starParTags, $starParDefault, $starParSelected);
										
										if($pageType!=$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']){
										# Create Filter 
											$createParDefault ='';
											$ParSelected =''; 
											if($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings']){
																						
												$createParData []= array('',$Lang['DocRouting']['Management']['FilterStatus']['AllCreateStatus']);	
											}
											else{
												$createParDefault = $Lang['DocRouting']['Management']['FilterStatus']['AllCreateStatus'];	
											}
											
											$ParSelected = $ck_doc_management_current_rountings_create_status;
											
											if($pageType != $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
												$createParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['CreatedByYou'],$Lang['DocRouting']['Management']['FilterStatus']['CreatedByYou'] );
												$createParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotCreatedByYou'],$Lang['DocRouting']['Management']['FilterStatus']['NotCreatedByYou']);										
												$createParTags = 'name ="createStatus" class="createStatus" id="createStatus"';
												
												$htmlAry['CurrentDisplay'] .= $this->GET_SELECTION_BOX($createParData, $createParTags, $createParDefault, $ParSelected);
											}
											# Follow-up Filter 
											if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings']){
												$followParData []= array('',$Lang['DocRouting']['Management']['FilterStatus']['AllFollowUpStatus'] );	
												$followParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['FollowedByYouNow'],$Lang['DocRouting']['Management']['FilterStatus']['FollowedByYouNow'] );
												$followParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotFollowedByYouNow'],$Lang['DocRouting']['Management']['FilterStatus']['NotFollowedByYouNow']);																																
												$followParTags = 'name ="followStatus" class="followStatus" id="followStatus"';
												$followParDefault='';
												$ParSelected ='';
														
												if(!isset($ck_doc_management_current_rountings_follow_status)){
													$ParSelected = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['FollowedByYouNow'];
												}
												else{						
													$ParSelected = $ck_doc_management_current_rountings_follow_status;										
												}
												$htmlAry['CurrentDisplay'] .= 	$this->GET_SELECTION_BOX($followParData, $followParTags, $followParDefault, $ParSelected);																															
											
												# Expire Filter
												$expireParData[] = array('',$Lang['DocRouting']['Management']['FilterStatus']['AllExpireStatus']);
												$expireParData[] = array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['ExpiredNotFinished'],$Lang['DocRouting']['Management']['FilterStatus']['ExpiredButNotYetFinished']);																															
												$expireParTags = ' name="expireStatus" class="expireStatus" id="expireStatus" ';
												$expireParDefault = '';
												$expireParSelected = '';
											
												if(!isset($ck_doc_management_current_routings_expire_status)){
													$expireParSelected = '';
												}else{
													$expireParSelected = $ck_doc_management_current_routings_expire_status;
												}
												
												$htmlAry['CurrentDisplay'] .= 	$this->GET_SELECTION_BOX($expireParData, $expireParTags, $expireParDefault, $expireParSelected);
											}
										}
																												
										$htmlAry['CurrentDisplay'] .= '</td>';																				
										$htmlAry['CurrentDisplay'] .= '<td valign="bottom"></td>';
									$htmlAry['CurrentDisplay'] .= '</tr>';
								$htmlAry['CurrentDisplay'] .= '</tbody></table>';
							
							
							###   Display Items By Ajax     ###		
							$htmlAry['CurrentDisplay'] .='<div id ="currentRoutingItemDisplayReload" class="currentRoutingItemDisplayReload" >';															
							$htmlAry['CurrentDisplay'] .= '</div>';
							### End Of Display Item By Ajax ###
							
							$htmlAry['CurrentDisplay'] .= '<input type="hidden" id="selectedTag" name="selectedTag"/>';
						
						$htmlAry['CurrentDisplay'] .= '</td>';
					$htmlAry['CurrentDisplay'] .= '</tr>';
				$htmlAry['CurrentDisplay'] .= '</table>';
			$htmlAry['CurrentDisplay'] .= '</form>';
		
			return $htmlAry['CurrentDisplay'];
		}
		
		function getCurrentRoutingDisplayItem($status, $keyword, $accessRight, $starStatus,$createStatus, $followStatus=0, $pageType='', $selectedTag='', $expireStatus='', $advanceSearchArray='', $selectedAcademicYear=''){
			global $Lang, $PATH_WRT_ROOT, $xmsg, $indexVar, $docRoutingConfig, $sys_custom;
			$libuser = new libuser();
			
			
			# Check User's Access Right						
			if($indexVar['drUserIsAdmin']){
				$involvedOnly = false;
				$inputBy = '';
			}
			else{
				$involvedOnly = true;
				
				if ($pageType == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']) {
					$inputBy = $indexVar['drUserId'];
				}
			}
				
			
			
			# Get User DR Entry Data
			$userDocumentInfoAry = $indexVar['libDocRouting']->getUserDREntry($indexVar['drUserId'], $involvedOnly, $status, $keyword, $starStatus, $createStatus, $selectedTag, $inputBy, $expireStatus, $advanceSearchArray, $selectedAcademicYear);	
			
			//debug_pr($userDocumentInfoAry);
			//debug_pr($documentInfoAry);
				
//			//$documentIdStarArr='';	
//			for($i=0;$i<count($documentInfoAry);$i++){			
//				$documentIdArrUnique []= $documentInfoAry[$i]['DocumentID'];					
//				//$documentIdStarArr [] = $documentInfoAry[$i]['StarSequence'];
//			}	
			$documentIdArrUnique = Get_Array_By_Key($userDocumentInfoAry, 'DocumentID');
			//debug_pr($documentIdArrUnique);
			
//			if($documentIdArrUnique){		
//				$documentIdArrUnique = array_unique($documentIdArrUnique);
//				foreach ($documentIdArrUnique as $documentIdArrKey => $documentIdArrValue){
//					$documentIdArrUnique[] = $documentIdArrValue;					
//				}
//			}	

			# Get Route Entry Data 
			$documentDataAry = $indexVar['libDocRouting']->getEntryData($documentIdArrUnique);
			$documentDataAssoAry = BuildMultiKeyAssoc($documentDataAry, 'DocumentID');
			
   			# Get Route Data
			$routeDataAry = $indexVar['libDocRouting']->getRouteData($documentIdArrUnique);
			$routeDataAssoAry = BuildMultiKeyAssoc($routeDataAry, 'DocumentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);		
																			
			# Get File Data 																	
			$fileDataAry = $indexVar['libDocRouting']->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $documentIdArrUnique);
			$fileDataAssoAry = BuildMultiKeyAssoc($fileDataAry, 'LinkToID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			# Get Star Sequence 
			$routeStarArr = $indexVar['libDocRouting']->getStarStatus($indexVar['drUserId']);
			$routeStarAssoAry = BuildMultiKeyAssoc($routeStarArr, 'DocumentID');
   								
   			# Get Current User Responsible Routing  				
   			$currentUserResponsibleRoutingAssoAry = $indexVar['libDocRouting']->getUserCurrentInvolvedEntry($indexVar['drUserId']);
   			$currentUserResponsibleDocumentIdArr = array_keys($currentUserResponsibleRoutingAssoAry);
   			
   			# Get Route Complete Status
   			$routeCompleteStatusAry = $indexVar['libDocRouting']->getRouteCompleteStatus($documentIdArrUnique);
   			
   			$allRouteIdAry = Get_Array_By_Key($routeDataAry, 'RouteID');
   			
   			# Get Route Target Users in batch
   			$allRouteTargetUsers = $indexVar['libDocRouting']->getRouteTargetUsers($allRouteIdAry);
   			$allRouteTargetUsersSize = count($allRouteTargetUsers);
   			$routeIdToRouteTargetUsersAry = array();
   			for($n=0;$n<$allRouteTargetUsersSize;$n++){
   				if(!isset($routeIdToRouteTargetUsersAry[$allRouteTargetUsers[$n]['RouteID']])){
   					$routeIdToRouteTargetUsersAry[$allRouteTargetUsers[$n]['RouteID']] = array();
   				}
   				$routeIdToRouteTargetUsersAry[$allRouteTargetUsers[$n]['RouteID']][] = $allRouteTargetUsers[$n];
   			}
   			
   			# Find out route targets to check sign status for followed and signed routings but not completed yet. And find current user last followed time by either signed or give feedback.
   			if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
				//$allRouteIdAry = Get_Array_By_Key($routeDataAry, 'RouteID');
				$allRouteTargetRecordAry = $indexVar['libDocRouting']->getRouteTargetUsers($allRouteIdAry, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']), array($indexVar['drUserId']));
				$docIdToRouteTargetAry = array();
				$docIdToLastFollowTimeForCurrentUser = array();
				for($n=0;$n<count($allRouteTargetRecordAry);$n++){
					if(!isset($docIdToRouteTargetAry[$allRouteTargetRecordAry[$n]['DocumentID']])){
						$docIdToRouteTargetAry[$allRouteTargetRecordAry[$n]['DocumentID']] = array();
					}
					$docIdToRouteTargetAry[$allRouteTargetRecordAry[$n]['DocumentID']][] = $allRouteTargetRecordAry[$n];
					if($allRouteTargetRecordAry[$n]['DateModified'] != ''){
						$docIdToLastFollowTimeForCurrentUser[$allRouteTargetRecordAry[$n]['DocumentID']] = $allRouteTargetRecordAry[$n]['DateModified'];
					}
				}
				//debug_pr($docIdToLastFollowTimeForCurrentUser);
				$allLastFeedbacksForCurrentUser = $indexVar['libDocRouting']->getRouteUserFeedbacks($allRouteIdAry, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']), array($indexVar['drUserId']));
				for($n=0;$n<count($allLastFeedbacksForCurrentUser);$n++){
					if(!isset($docIdToLastFollowTimeForCurrentUser[$allLastFeedbacksForCurrentUser[$n]['DocumentID']])){
						$docIdToLastFollowTimeForCurrentUser[$allLastFeedbacksForCurrentUser[$n]['DocumentID']] = $allLastFeedbacksForCurrentUser[$n]['FeedbackModifyDate'];
					}else if($allLastFeedbacksForCurrentUser[$n]['FeedbackModifyDate'] > $docIdToLastFollowTimeForCurrentUser[$allLastFeedbacksForCurrentUser[$n]['DocumentID']]){
						$docIdToLastFollowTimeForCurrentUser[$allLastFeedbacksForCurrentUser[$n]['DocumentID']] = $allLastFeedbacksForCurrentUser[$n]['FeedbackModifyDate'];
					}
				}
				//debug_pr($docIdToLastFollowTimeForCurrentUser);
			}
   			
			#####    Routing Display Item    ####
			$htmlAry['CurrentDisplay'] .= '<div class="dr_list">';
			$htmlAry['CurrentDisplay'] .= '<ul>';
																			
			#### Loop Each User Entry ####										
			$numOfDocument = count($documentDataAssoAry);
			
			
			if($numOfDocument>0){
				if($selectedTag>0){	
					$__tagNameArr = returnModuleTagNameByTagID($selectedTag);
					$__tagName = $__tagNameArr[0];	
					$htmlAry['CurrentDisplay'] .= '<table class="form_table_v30">';	
					$htmlAry['CurrentDisplay'] .= '<tr>';
					$htmlAry['CurrentDisplay'] .= '<td class="field_title">';
					$htmlAry['CurrentDisplay'] .= $Lang['eDiscipline']['TagName'];										
					$htmlAry['CurrentDisplay'] .= '</td>';	
					$htmlAry['CurrentDisplay'] .= '<td>';
					$htmlAry['CurrentDisplay'] .= $__tagName;
					$htmlAry['CurrentDisplay'] .= '</td>';
					$htmlAry['CurrentDisplay'] .= '</tr>';
					$htmlAry['CurrentDisplay'] .= '</table>';
					$htmlAry['CurrentDisplay'] .= '<br />';
				}
				
				# Get all tags in batch
				$allTagAry = $indexVar['libDocRouting']->getEntryTag($documentIdArrUnique);	
				$allTagArySize = count($allTagAry);
				$documentIdToTagAry = array();
				for($i=0;$i<$allTagArySize;$i++){
					if(!isset($documentIdToTagAry[$allTagAry[$i]['DocumentID']])){
						$documentIdToTagAry[$allTagAry[$i]['DocumentID']] = array();
					}
					$documentIdToTagAry[$allTagAry[$i]['DocumentID']][] = $allTagAry[$i];
				}
				
				$tagIdToTagNamesAry = array();
				if($allTagArySize > 0){
					$allTagIds = Get_Array_By_Key($allTagAry,'TagID');
					$tagIdToTagNamesAry = returnTagIDToModuleTagNameByTagID(implode(',',$allTagIds));
				}
				
				$numOfItemAlreadyDisplay=0;		
				for ($i=0; $i<$numOfDocument; $i++) {
					$documentStatus = '';	
					$userIconControlCount = 0;
					$arrowDisplayCout = 0  ;	
					$generalFinishedItem = 0;
					$generalFinishedItemCounter = 0;	
														
					$_documentId = $documentIdArrUnique[$i];													
					$_documentDataAry = $documentDataAssoAry[$_documentId];
					$_documentType = $_documentDataAry['DocumentType'];
					$_physicalLocation = $_documentDataAry['PhysicalLocation'];
					$_documentCreatorID = $_documentDataAry['UserID'];
					$_documentTitle = $_documentDataAry['Title'];	
					$_documentDateInput = $_documentDataAry['DateInput'];		
					$_documentRecordStatus = $_documentDataAry['RecordStatus'];
										
					# Num Of Route related to this Entry
					$_documentRouteAry = $routeDataAssoAry[$_documentId];									
					$_numOfRoute = count($_documentRouteAry);
	
					# Num Of Files Attached
					$_documentFileAry = $fileDataAssoAry[$_documentId];
					$_numOfFile = count($_documentFileAry);
	
					# Get Tag Inform 							
					//$_tagArr = $indexVar['libDocRouting']-> getEntryTag($_documentId); // use $documentIdToTagAryto avoid performance issue
					$_tagArr = array();
					if(isset($documentIdToTagAry[$_documentId])){
						$_tagArr = $documentIdToTagAry[$_documentId];
					}
					
					# User Name Array
					$_documentCreatorName = $_documentDataAry['CreatorName'];
					
					## Star Icon CSS Display Control ###
					$_routeStar = $routeStarAssoAry[$_documentId]['StarSequence'];			
					$starDisplayCss='';					
					
					if($_routeStar == $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['StarRemarked']){
						$starDisplayCss = 'btn_set_important_on';
					}
					else{
						$starDisplayCss = 'btn_set_important';
						$_routeStar = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotStarRemarked'];
					}
					### Star CSS Display Control ###
										
					### Display Box Color Css Control & Assign recordEntryStatus Value for Filter Checking ###								
					if(in_array($_documentId, $currentUserResponsibleDocumentIdArr)){
						$documentStatus = $Lang['DocRouting']['StatusDisplay']['Waiting'];
						$statusStyle = 'waiting';	
						$recordEntryStatus = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['FollowedByYouNow'];
					}
					else if ($_documentRecordStatus != $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']) {
						$_allUserCompleted = true;
						for ($j=0; $j<$_numOfRoute; $j++){
							$__routeId = $_documentRouteAry[$j]['RouteID'];
							//$__targetRouteUserArr = $indexVar['libDocRouting']->getRouteTargetUsers(array($__routeId)); // use $routeIdToRouteTargetUsersAry to avoid performance issue
							$__targetRouteUserArr = array();
							if(isset($routeIdToRouteTargetUsersAry[$__routeId])){
								$__targetRouteUserArr = $routeIdToRouteTargetUsersAry[$__routeId];
							}
							$__numOftargetRouteUsers = count($__targetRouteUserArr);
							
							
							for($k=0;$k<$__numOftargetRouteUsers;$k++){
								$___targetRouteUserID = $__targetRouteUserArr[$k]['UserID'];						
								$___targetRouteUserRecordStatus = $__targetRouteUserArr[$k]['RecordStatus'];
								$___targetRouteUserAccessRight = $__targetRouteUserArr[$k]['AccessRight'];
								
								if ($___targetRouteUserAccessRight == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route'] && $___targetRouteUserRecordStatus != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']) {
									$_allUserCompleted = false;
									break;
								}
							}
						}
						
						$_isAfterDeadline = false;
						$_lastRouteType = $_documentRouteAry[$_numOfRoute-1]['EffectiveType'];
						if ($_lastRouteType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']) {
							$_lastRouteDeadline = $_documentRouteAry[$_numOfRoute-1]['EffectiveEndDate'];
							if (time() > strtotime($_lastRouteDeadline)) {
								$_isAfterDeadline = true;
							}
						}
						
						//2014-0220-1638-49184
						if ($_allUserCompleted) {
							$statusStyle = 'processing';
						}
						else if ($_isAfterDeadline) {
							$statusStyle = 'after_deadline';
						}
						else {
							$statusStyle = 'waiting_reply';
						}
						$documentStatus = $Lang['DocRouting']['StatusDisplay']['Processing'];
						$recordEntryStatus = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotFollowedByYouNow'];	
					}
					elseif($_documentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']){
						$documentStatus = $Lang['DocRouting']['StatusDisplay']['Completed'];
						$statusStyle ='';
						$recordEntryStatus = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotFollowedByYouNow'];	
					}else{
						$statusStyle ='';
						$documentStatus = $Lang['DocRouting']['StatusDisplay']['Processing'];
						$recordEntryStatus = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotFollowedByYouNow'];
					}
							
					### End of Display Box Color Css Control ###
					
					### Filter controls Display Items ###	
					$displayItem = true;
					
					if ($followStatus != '') {
						### Show all signed DR Entry for Creator
						if($recordEntryStatus != $followStatus){		
							
							## Check if all target user signed/confirmed 					
							if(trim($_documentDataAry['InputBy'])==trim($indexVar['drUserId']) || $_routeStar == $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['StarRemarked']){
								// 2014-0416-1055-26177: show routing even if not all user signed if the routing is created by this user
//								$thisTargetUserCompleteStatus = $routeCompleteStatusAry[$_documentId];
//								$noOfNotAllSign = 0;
//								foreach ((array)$thisTargetUserCompleteStatus as $eachTargetUserID => $eachTargetUserCompleteStatus){
//									if($eachTargetUserCompleteStatus!=3){
//										$noOfNotAllSign++;
//									}
//								}
//								if($noOfNotAllSign!=0){									
//									$displayItem = false;
//								}
							}else{
								$displayItem = false;	
							}							
						}
					}
				
					if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
						$routeTargetRecordAry = $docIdToRouteTargetAry[$_documentId];
						for($n=0;$n<count($routeTargetRecordAry);$n++){
							if($routeTargetRecordAry[$n]['RecordStatus'] != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']){
								$displayItem = false;
							}
						}
						if(count($routeTargetRecordAry)==0){
							$displayItem = false;
						}
					}
				
					if($displayItem){
					
						$numOfItemAlreadyDisplay ++;
						# Detail Page Link
						$detailLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'dr_detail', array('documentId' => $_documentId, 'pageType' => $pageType));	
						
						#Onlty
						if($indexVar['drUserIsAdmin'] || $_documentCreatorID == $indexVar['drUserId']){
							$editLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'edit_doc', array('DocumentID' => $_documentId, 'pageType' => $pageType));	
							$deleteLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'delete_doc_update', array('documentId' => $_documentId, 'pageType' => $pageType));	
						}		
						
				
										
						$htmlAry['CurrentDisplay'] .= '<li class='.$statusStyle.'>';
							$htmlAry['CurrentDisplay'] .=  '<div class="" style="float:right">';
						
						if($indexVar['drUserIsAdmin'] || $_documentCreatorID == $indexVar['drUserId']){
							if(!in_array($pageType,array($docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings'],$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DeletedRountings']))){
								$htmlAry['CurrentDisplay'] .=  $indexVar['libDocRouting_ui']-> GET_ACTION_LNK("?pe='.$editLink.'", $ParTitle="", $ParOnClick="", $ParClass="") ;
							}
							if($pageType != $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DeletedRountings']){
								$htmlAry['CurrentDisplay'] .= $indexVar['libDocRouting_ui']->GET_LNK_DELETE("javascript:void(0);", $ParTitle="delete", "js_Delete_Alert(document.form1, '$deleteLink');", $ParClass="delete", $WithSpan=1);
							}
							if($_documentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']){
								$send_email_link = $indexVar['libDocRouting']->getEncryptedParameter('management', 'ajax_get_send_sign_email_form', array('DocumentID' => $_documentId));	
								//$htmlAry['CurrentDisplay'] .=  $indexVar['libDocRouting_ui']->GET_ACTION_LNK("javascript:void(0);", $Lang['DocRouting']['SendEmail'], "js_GetSendSignEmailForm('".$send_email_link."')", "select_preset") ;
								$htmlAry['CurrentDisplay'] .= '<div class="Conntent_tool"><a href="javascript:void(0);" class="email" title="'.$Lang['DocRouting']['SendEmail'].'" alt="'.$Lang['DocRouting']['SendEmail'].'" onclick="js_GetSendSignEmailForm(\''.$send_email_link.'\')"></a></div>';
							}
							
							if($_documentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']){
								$send_doc_email_link = $indexVar['libDocRouting']->getEncryptedParameter('management', 'ajax_get_send_archived_document_email_form', array('DocumentID' => $_documentId));	
								$htmlAry['CurrentDisplay'] .= '<div class="Conntent_tool"><a href="javascript:void(0);" class="email" title="'.$Lang['DocRouting']['SendEmail'].'" alt="'.$Lang['DocRouting']['SendEmail'].'" onclick="js_GetSendArchivedDocumentEmailForm(\''.$send_doc_email_link.'\')"></a></div>';
							}
							
							$printLink = 'newWindow(\'index.php?pe='.$indexVar['libDocRouting']->getEncryptedParameter('management','print_document_detail',array('DocumentID'=>$_documentId)).'\',37);';
							//$htmlAry['CurrentDisplay'] .= $indexVar['libDocRouting_ui']->GET_LNK_PRINT('javascript:void(0);', "", $printLink, "", "", 0);
							$htmlAry['CurrentDisplay'] .= '<div class="Conntent_tool"><a href="javascript:void(0);" class="print" title="'.$Lang['Btn']['Print'].'" alt="'.$Lang['Btn']['Print'].'" onclick="'.$printLink.'"></a></div>';
						}		
							$htmlAry['CurrentDisplay'] .='<br />';
						
						$htmlAry['CurrentDisplay'] .='</div>';
							$htmlAry['CurrentDisplay'] .= '<br />';								
							$htmlAry['CurrentDisplay'] .= '<div class="dr_list_header">';
								$htmlAry['CurrentDisplay'] .= '<a class="'.$starDisplayCss.'" href="javaScript:void(0)" onclick="js_Update_Star_Status('.$_documentId.', '.$_routeStar.', '.$indexVar['drUserId'].')"></a>';
								$htmlAry['CurrentDisplay'] .= '<h1><a href="?pe='.$detailLink.'">'.intranet_htmlspecialchars($_documentTitle).'</a></h1>';
								if($sys_custom['DocRouting']['RequestNumber'])
								{
									$htmlAry['CurrentDisplay'] .= '<span class="date_time">'.$Lang['DocRouting']['RequestNumber'].': '.$_documentId.'</span>';
								}
								$htmlAry['CurrentDisplay'] .= ' <span class="member_pic_name" title="'.$Lang['DocRouting']['Creator'].'">'. intranet_htmlspecialchars($_documentCreatorName) .  '</span>';
								
								$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p>';
							$htmlAry['CurrentDisplay'] .= '</div>';
							//Henry Added [Start]
							$divName = 'dr_content_detail_'.$i;
							$htmlAry['CurrentDisplay'] .=  '<div style="float:right">';
							//modified at 20131217
			            	$htmlAry['CurrentDisplay'] .= '&nbsp;<span id="spanShowOption_'.$divName.'_hide">'."\n";
							$htmlAry['CurrentDisplay'] .= $this->Get_Show_Option_Link("javascript:js_Show_Option_Div('".$divName."_hide');","",$Lang['StaffAttendance']['ShowDetail']);
							$htmlAry['CurrentDisplay'] .= '</span>'."\n";
							//modified at 20131217
							$htmlAry['CurrentDisplay'] .= '<span id="spanHideOption_'.$divName.'_hide" style="display:none">'."\n";
							$htmlAry['CurrentDisplay'] .= $this->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('".$divName."_hide');","",$Lang['StaffAttendance']['HideDetail']);
							$htmlAry['CurrentDisplay'] .= '</span>&nbsp;'."\n";
							$htmlAry['CurrentDisplay'] .=  '</div>';
							//Henry Added [End]
							$htmlAry['CurrentDisplay'] .= '<div class="dr_content_detail" id="'.$divName.'">';
								//added at 20131217
								$htmlAry['CurrentDisplay'] .= '<div id="'.$divName.'_hide" style="display:none">';
								$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p>'; //Henry Added
								$htmlAry['CurrentDisplay'] .= '<div class="dr_content_file">';
								
								# Show if attached files
								if($_numOfFile>0){								
									$htmlAry['CurrentDisplay'] .= '<span class="file_attach">'. Get_Plural_Display($_numOfFile,$Lang['DocRouting']['File']). '</span>';				                                        
								}
								
								# show physical location
								if ($_documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location']) {
									$htmlAry['CurrentDisplay'] .= $Lang['DocRouting']['PhysicalLocation'].': '.intranet_htmlspecialchars($_physicalLocation);
								}
								
								
								$htmlAry['CurrentDisplay'] .= '</div>';
								
								$htmlAry['CurrentDisplay'] .= '<ul class="member_list">';
																		
								##############  Show Each Routing ####################
					
									# Check if This Route Not Yet Start (For General Route) 
									$displayItemInGrey ='';
																				
									for ($j=0; $j<$_numOfRoute; $j++){
									
										$__routeId = $_documentRouteAry[$j]['RouteID'];
										$__routUserId = $_documentRouteAry[$j]['UserID'];
										//$__routUserName = $libuser -> getNameWithClassNumber($__routUserId, 0);
										
										$__effectiveType = $_documentRouteAry[$j]['EffectiveType'];									
										$__effectiveStartDate = $_documentRouteAry[$j]['EffectiveStartDate'];										
										$__effectiveEndDate = $_documentRouteAry[$j]['EffectiveEndDate'];										
										//$__routFeedBackArr = $indexVar['libDocRouting']->getRouteUserFeedbacks($__routeId);							
																			
										//$__targetRouteUserArr = $indexVar['libDocRouting']->getRouteTargetUsers(array($__routeId)); // use $routeIdToRouteTargetUsersAry to avoid performance issue
										$__targetRouteUserArr = array();
										if(isset($routeIdToRouteTargetUsersAry[$__routeId])){
											$__targetRouteUserArr = $routeIdToRouteTargetUsersAry[$__routeId];
										}
																	
										$__numOftargetRouteUsers = count($__targetRouteUserArr);									
										//$routFeedBackAssoAry = BuildMultiKeyAssoc($__routFeedBackArr, 'UserID');
															
											
									    # For Debug Route Status 	
										//debug_pr($_documentId . ' - ' . $__routeId . ' - '. $_documentTitle . '-' . $routeCompleteStatusAry[$_documentId][$__routeId] );
										#### Check Each Route Status #####
										
										$statusCss ='';
										
										//debug_pr($_documentTitle . '-' . $_documentId . '-' . $__routeId . '-'. $routeCompleteStatusAry[$_documentId][$__routeId].'-'.$__effectiveType);
										
										
										if($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){
											$_routeStatusDisplay='';
											
											if($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['inProgress']){
												$arrowDisplayCout ++;
												$calendarCss = 'time_current';
												$statusCss = 'status_current';
												
												$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Processing'];
												
							
											}elseif($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['completed']){
												
												$calendarCss = 'time_pass';
												$statusCss = 'status_pass';
											
												$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];
												
											}elseif($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['notStarted']){
												
												$calendarCss ='time_coming';
												$statusCss = '';	
												$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];
											}else{
												$_routeStatusDisplay='';
												$statusCss = '';
											}		
											
										}
										elseif($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished']){
											
											if($arrowDisplayCout > 0){
												$statusCss = '';	
												$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];
												
											}else{
											
												$_routeStatusDisplay='';
												if($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['inProgress']){
													$arrowDisplayCout ++;							
													$statusCss = 'status_current';
													$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Processing'];
								
												}elseif($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['completed']){
													
													$statusCss = 'status_pass';
													$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];
													
												}elseif($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['notStarted']){
																	
													$statusCss = '';	
													$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];
												}else{
													$_routeStatusDisplay = '';
													$statusCss = '';	
												}
											}
												
										}
										//debug_pr($statusCss);
										#### End of Check Each Route Status #####		
										
												
																					
										## Route And Users In Route Display 																				
										if($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){	
											$htmlAry['CurrentDisplay'] .= '<li class="' . $statusCss .'" title="'.$_routeStatusDisplay.'">';
											$htmlAry['CurrentDisplay'] .= '<span class="' . $calendarCss . '">';
											$htmlAry['CurrentDisplay'] .=  "$__effectiveStartDate" . ' ~ ' . "$__effectiveEndDate";
											$htmlAry['CurrentDisplay'] .= '</span>';									
											$_userStatusDisplay = '';	
											for($k=0;$k<$__numOftargetRouteUsers;$k++){
												
												$___targetRouteUserID = $__targetRouteUserArr[$k]['UserID'];						
												$___targetRouteUserRecordStatus = $__targetRouteUserArr[$k]['RecordStatus'];
												$___targetRouteUserAccessRight	= $__targetRouteUserArr[$k]['AccessRight'];		
																																									
												if($___targetRouteUserAccessRight == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['View']){
														$_routeStatusCss = 'memmber_View_Only';	
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['ViewOnly'];
												}
												else{
													if($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'] && $___targetRouteUserID == $indexVar['drUserId'] && $routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['inProgress']){
												
														$_routeStatusCss = 'memmber_alert';	
														$_userStatusDisplay = $Lang['DocRouting']['NotViewedYet'];	
															
														$userIconControlCount ++;			
													}
													elseif($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']){
														$_routeStatusCss = 'memmber_read';
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Processing'];
														$userIconControlCount ++;																																					
													}
													elseif($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']){
														$_routeStatusCss = 'memmber_done';	
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];	
										
													}
													
													else{
														$_routeStatusCss = 'memmber_empty_css';		
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];
													}	
												}
												if ($_routeStatusCss == 'memmber_View_Only'){
													$htmlAry['CurrentDisplay'] .= '<span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">('.$__targetRouteUserArr[$k]['UserName'].')</span>';
											
												}else{
													$htmlAry['CurrentDisplay'] .= '<span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">'.$__targetRouteUserArr[$k]['UserName'].'</span>';
												}
											}
																						
											$htmlAry['CurrentDisplay'] .= '</li>';
										}	
										else{
											
						
											  $htmlAry['CurrentDisplay'] .='<li class="' . $statusCss .'" title="'.$_routeStatusDisplay.'">';
											
										
											if($userIconControlCount > 0){
												for($k=0;$k<$__numOftargetRouteUsers;$k++){																									
													$___targetRouteUserID = $__targetRouteUserArr[$k]['UserID'];												
													$___targetRouteUserRecordStatus = $__targetRouteUserArr[$k]['RecordStatus'];																															
															
													$_routeStatusCss = '';		
													
	
													$htmlAry['CurrentDisplay'] .= '<span class="'.$_routeStatusCss.'">'.$__targetRouteUserArr[$k]['UserName'].'</span>';
												}
											}
											else{
												$_userStatusDisplay = '';	
												for($k=0;$k<$__numOftargetRouteUsers;$k++){																									
													$___targetRouteUserID = $__targetRouteUserArr[$k]['UserID'];												
													$___targetRouteUserRecordStatus = $__targetRouteUserArr[$k]['RecordStatus'];																															
														$___targetRouteUserAccessRight	= $__targetRouteUserArr[$k]['AccessRight'];	
														
													if($___targetRouteUserAccessRight == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['View']){
														$_routeStatusCss = 'memmber_View_Only';	
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['ViewOnly'];
													}
													else{
																										
														if($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'] && $___targetRouteUserID == $indexVar['drUserId'] && $routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['inProgress']){
															
															$_routeStatusCss = 'memmber_alert';	
															$_userStatusDisplay = $Lang['DocRouting']['NotViewedYet'];	
															$userIconControlCount ++;										
														}
														elseif($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']){
															$_routeStatusCss = 'memmber_read';
															$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Processing'];
															$userIconControlCount ++;		
																																																									
														}elseif($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']){
															$_routeStatusCss = 'memmber_done';	
															$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];	
														}
														else{
															$_routeStatusCss = 'memmber_empty_css';	
															$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];	
														}	
														
													}
													
													
													
													//$htmlAry['CurrentDisplay'] .= '<span class="'.$_routeStatusCss.'">'.$__targetRouteUserArr[$k]['UserName'].'</span>';
												
													if ($_routeStatusCss == 'memmber_View_Only'){
													$htmlAry['CurrentDisplay'] .= '<span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">('.$__targetRouteUserArr[$k]['UserName'].')</span>';
											
												}else{
													$htmlAry['CurrentDisplay'] .= '<span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">'.$__targetRouteUserArr[$k]['UserName'].'</span>';
												}
												
												
												}
											
											}								
											
											
											$htmlAry['CurrentDisplay'] .= '</li>';
										}		
				
									}
									
									$htmlAry['CurrentDisplay'] .= '</ul><p class="spacer"></p>';
									
									if($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']){
										$dateDisplayLangText = $Lang['DocRouting']['Management']['DraftDate'];
									}else{
										$dateDisplayLangText = $Lang['DocRouting']['Management']['PublishDate'];
									}
									//Henry Added at 20131217 [Start]
									$htmlAry['CurrentDisplay'] .= '</div>';	
									//Henry Added at 20131217 [End]
									
									$htmlAry['CurrentDisplay'] .= '<span class="date_time"> '.$Lang['DocRouting']['Status'].': '.$documentStatus.' &nbsp; '.$dateDisplayLangText. ' ' . $_documentDateInput;
									if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
										if(isset($docIdToLastFollowTimeForCurrentUser[$_documentId])){
											$htmlAry['CurrentDisplay'] .= ' &nbsp; '.$Lang['DocRouting']['LastFollowedTime'].': '.$docIdToLastFollowTimeForCurrentUser[$_documentId];
										}
									}
									$htmlAry['CurrentDisplay'] .= '</span>';
									
									# Tags
																	
									$_numOfTagArr = count($_tagArr);
									if($_numOfTagArr>0){
										$htmlAry['CurrentDisplay'] .= '<span class="tag"><em>'.$Lang['DocRouting']['Tag']['label'].'</em>';	
									}
									
									$__tagCommaCount = 0;
									for($j=0;$j<$_numOfTagArr;$j++){
										$__tagId = $_tagArr[$j]['TagID'];
										//$__tagNameArr = returnModuleTagNameByTagID($__tagId); // use $tagIdToTagNamesAry to avoid performance issue
										$__tagNameArr = array();
										if(isset($tagIdToTagNamesAry[$__tagId])){
											$__tagNameArr = $tagIdToTagNamesAry[$__tagId];
										}
										$__tagName = '';
										if(count($__tagNameArr)>0){
											$__tagName = $__tagNameArr[0];		
										}
										
										$__tagCommaCount ++;																		
										if($_numOfTagArr>1){
											$htmlAry['CurrentDisplay'] .= '<a href="javaScript:js_Filter_Selected_Tag('.$__tagId.')">'. intranet_htmlspecialchars($__tagName);
											if($__tagCommaCount ==  $_numOfTagArr){
												$htmlAry['CurrentDisplay'] .= '</a>';
											}
											else{
												$htmlAry['CurrentDisplay'] .= '</a>, ';
											}
										}else{					
											$htmlAry['CurrentDisplay'] .= '<a href="javaScript:js_Filter_Selected_Tag('.$__tagId.')">' . intranet_htmlspecialchars($__tagName) .'</a>';
										}
									}
										
								
									$htmlAry['CurrentDisplay'] .='</span>';								
								$htmlAry['CurrentDisplay'] .= '</div>';		
								//$htmlAry['CurrentDisplay'] .= '</div>'; //Henry Added	
										
								$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p>';		                                                          
							$htmlAry['CurrentDisplay'] .= '</li>';	
							
						
						}																											
						
					}
					if($selectedTag>0){	
						$htmlAry['CurrentDisplay'] .= '<div class="edit_bottom">';											
						$htmlAry['CurrentDisplay'] .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['DocRouting']['Button']['Back'], "button", "js_Filter_Selected_Tag('');");
						$htmlAry['CurrentDisplay'] .= '</div>';
					}	
					if($numOfItemAlreadyDisplay==0){							
						$htmlAry['CurrentDisplay'] .= '<li class="">';
						$htmlAry['CurrentDisplay'] .= '<br />';
						$htmlAry['CurrentDisplay'] .= '<center>'. $Lang['General']['NoRecordFound']. '</center>';
						$htmlAry['CurrentDisplay'] .= '<br />';	
						$htmlAry['CurrentDisplay'] .= '</li>';							
					}
			}else{
				
				$htmlAry['CurrentDisplay'] .= '<li class="">';
				$htmlAry['CurrentDisplay'] .= '<br />';
				$htmlAry['CurrentDisplay'] .= '<center>'. $Lang['General']['NoRecordFound']. '</center>';
				$htmlAry['CurrentDisplay'] .= '<br />';	
				$htmlAry['CurrentDisplay'] .= '</li>';	
			}

			return  $htmlAry['CurrentDisplay'];
		}
			
		
		
				
		function getPresetDisplayTable($newPresetInstructionLink, $PresetType, $field='',$order='',$page='', $targetUser=''){
			global $PATH_WRT_ROOT, $indexVar, $Lang, $docRoutingConfig, $image_path, $LAYOUT_SKIN, $button_delete, $button_edit;	   
		   	global  $page_size, $doc_settings_preset_doc_instruction_page_size, $doc_settings_preset_routing_note_page_size;
			include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");		    
			include_once($PATH_WRT_ROOT."/includes/libdbtable.php");
			include_once($PATH_WRT_ROOT."/includes/libdbtable2007a.php");
			
					
			$libDocRouting_prset = new libDocRouting_preset();
			$pageSizeChangeEnabled = true;
			
			$field = ($field=='')? 0 : $field;
			$order = ($order=='')? 1 : $order;
			$page = ($page=='')? 1 : $page;
			
			if($PresetType == $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['DocumentInstruction']){
				if (isset($doc_settings_preset_doc_instruction_page_size) && $doc_settings_preset_doc_instruction_page_size != "") {
					$page_size = $doc_settings_preset_doc_instruction_page_size;
				} 
				$pageDirectory = 'settings/preset_doc_instruction';		
			}
			else if($PresetType==$docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote']){
				if (isset($doc_settings_preset_routing_note_page_size) && $doc_settings_preset_routing_note_page_size != "") {
					$page_size = $doc_settings_preset_routing_note_page_size;
				}
				$pageDirectory = 'settings/preset_routing_note';
			}
			$li = new libdbtable2007($field,$order,$page);
		
			$sql = $libDocRouting_prset->getPresetRecords($PresetType, '', $targetUser);

			$li->sql = $sql;		
			$li->IsColOff = "IP25_table";
			
			//$li->field_array = array("Title", "TargetTypeDisplay");
			$li->field_array = array("Title");
			$li->column_array = array(0,0,0,0);
			$li->wrap_array = array(0,0,0,0);
			$li->no_col = count($li->field_array) + 2;
			
			$pos = 0;
			$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
			$li->column_list .= "<th width='95%' >".$li->column_IP25($pos++, $Lang['DocRouting']['TargetType']['Title'])."</th>\n";
			//$li->column_list .= "<th width='40%'>".$li->column_IP25($pos++, $Lang['DocRouting']['TargetType']['TargetType'] )."</th>\n";
			$li->column_list .= "<th width='1'>".$li->check("PresetID[]")."</th>\n";
			$li->no_col = $pos + 2;
			
//			if($PresetType == $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['DocumentInstruction']){	
//				if($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPersonalView']){
//					$actionLink = "?pe='".$indexVar['libDocRouting']->getEncryptedParameter('settings/preset_doc_instruction', 'index', array('personalTag' => '1', 'clearCoo' => '0'))."'";			
//				}
//				elseif($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView']){		
//					$actionLink = "?pe='".$indexVar['libDocRouting']->getEncryptedParameter('settings/preset_doc_instruction', 'index', array('publicTag' => '1', 'clearCoo' => '0'))."'";	
//				}
//				else{
//					$actionLink = "?pe='".$indexVar['libDocRouting']->getEncryptedParameter('settings/preset_doc_instruction', 'index', array('clearCoo' => '0'))."'";	
//				}
//			}
//			elseif($PresetType == $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote']){
//							
//				if($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPersonalView']){
//					$actionLink = "?pe='".$indexVar['libDocRouting']->getEncryptedParameter('settings/preset_routing_note', 'index', array('personalTag' => '1', 'clearCoo' => '0'))."'";			
//				}
//				elseif($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView']){		
//					$actionLink = "?pe='".$indexVar['libDocRouting']->getEncryptedParameter('settings/preset_routing_note', 'index', array('publicTag' => '1', 'clearCoo' => '0'))."'";	
//				}
//				else{
//					$actionLink = "?pe='".$indexVar['libDocRouting']->getEncryptedParameter('settings/preset_routing_note', 'index', array('clearCoo' => '0'))."'";	
//				}
//			}
			if($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPersonalView']){
				$actionLink = "?pe='".$indexVar['libDocRouting']->getEncryptedParameter($pageDirectory, 'index', array('personalTag' => '1', 'clearCoo' => '0'));			
			}
			elseif($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView']){		
				$actionLink = "?pe='".$indexVar['libDocRouting']->getEncryptedParameter($pageDirectory, 'index', array('publicTag' => '1', 'clearCoo' => '0'));	
			}
			else{
				$actionLink = "?pe='".$indexVar['libDocRouting']->getEncryptedParameter($pageDirectory, 'index', array('clearCoo' => '0'))."'";	
			}
			
	
																			
			$htmlAry['PresetDocDisplay'] .= '<form action="'.$actionLink.'" method="post" name="form1" id="form1">';		
			$htmlAry['PresetDocDisplay'] .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
			$htmlAry['PresetDocDisplay'] .= '<td>'."\n";
			$htmlAry['PresetDocDisplay'] .= '<div class="content_top_tool">';
			$htmlAry['PresetDocDisplay'] .= '<div class="Conntent_tool">';
			$htmlAry['PresetDocDisplay'] .= '<a class="new" href="'. '?pe='. $newPresetInstructionLink.'">' . $Lang['Btn']['New']. '</a>';
			
			if($PresetType==$docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote'] && $indexVar['libDocRouting']->enablePrintRoutingInstructionRight()){	
				$printInstructionLink = "index.php?pe=".$indexVar['libDocRouting']->getEncryptedParameter('management','print_instruction_step1');
				$htmlAry['PresetDocDisplay'] .= $this->GET_LNK_PRINT($ParHref="", $button_text="", $ParOnClick="newWindow('$printInstructionLink', 10);return false", $ParOthers="", $ParClass="", $useThickBox=1);
			}
			
			$htmlAry['PresetDocDisplay'] .= '</div>';
			$htmlAry['PresetDocDisplay'] .= '<tr>'."\n";
			$htmlAry['PresetDocDisplay'] .= '</td>'."\n";
			$htmlAry['PresetDocDisplay'] .= '<tr>'."\n";
			$htmlAry['PresetDocDisplay'] .= '<tr>'."\n";
			$htmlAry['PresetDocDisplay'] .= '<td>'."\n";
			$htmlAry['PresetDocDisplay'] .= '<div class="common_table_tool">';
			$htmlAry['PresetDocDisplay'] .= '<a href="javascript:js_Edit_Preset_Record(\'\')" class="tool_edit">'.$button_edit.'</a>';
			//$htmlAry['PresetDocDisplay'] .= '<a href="javascript:EditBookingRequest(\'Cancel\',\'edit_booking_request.php\')" class="tool_other">'.$button_cancel.'</a>';
			$htmlAry['PresetDocDisplay'] .= '<a href="javascript:js_Check_Remove()" class="tool_delete">'.$button_delete.'</a>';
			$htmlAry['PresetDocDisplay'] .= '</div>';
			$htmlAry['PresetDocDisplay'] .= '</td>'."\n";
			$htmlAry['PresetDocDisplay'] .= '</tr>'."\n";
			
			$htmlAry['PresetDocDisplay'] .= '<tr>'."\n";
			$htmlAry['PresetDocDisplay'] .= '<td>'."\n";			
			$htmlAry['PresetDocDisplay'] .= $li->display();			
			$htmlAry['PresetDocDisplay'] .= '</td>'."\n";
			$htmlAry['PresetDocDisplay'] .= '</tr>'."\n";			
			$htmlAry['PresetDocDisplay'] .= '</table>'."\n";
					
			$htmlAry['PresetDocDisplay'] .= '<input type="hidden" name="targetUser" value="'.$targetUser.'" />';
			$htmlAry['PresetDocDisplay'] .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
			$htmlAry['PresetDocDisplay'] .= '<input type="hidden" name="order" value="'.$li->order.'" />';
			$htmlAry['PresetDocDisplay'] .= '<input type="hidden" name="field" value="'.$li->field.'" />';
			$htmlAry['PresetDocDisplay'] .= '<input type="hidden" name="page_size_change" value="" />';
			$htmlAry['PresetDocDisplay'] .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';
			$htmlAry['PresetDocDisplay'] .= '<input type="hidden" id="PresetID" name="PresetID[]">';	
			$htmlAry['PresetDocDisplay'] .='</form>';
			
			return $htmlAry['PresetDocDisplay'];

		}
		
		function getNewPresetItemDisplay($newInstructionUpdateLink, $PAGE_NAVIGATION, $PresetType, $PresetID, $targetUser)		
		{
		 	global $PATH_WRT_ROOT, $Lang, $indexVar, $docRoutingConfig, $button_submit, $button_cancel, $cfg, $id;
			include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");		
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
			//debug_pr($PresetID);
			$lfilesystem = new libfilesystem();
			$libDocRouting_preset = new libDocRouting_preset();
			
			if($PresetID!=''){
			
				$sql = $libDocRouting_preset->getPresetRecords($PresetType,$PresetID, $targetUser);
				$presetRecordArr = $libDocRouting_preset->returnResultSet($sql);
						
			}
			
			$presetTitle = $presetRecordArr[0]['Title'];
			$presetContents = $presetRecordArr[0]['Contents'];
			$presetTargetType = $presetRecordArr[0]['TargetType'];
			
//			$selfCheck='';
//			$allCheck='';
//			if($presetTargetType == $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self']){
//				$selfCheck = 1;
//				$allCheck = 0;
//			}
//			elseif($presetTargetType == $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['All']){
//				$selfCheck = 0;
//				$allCheck = 1;
//			}
//			elseif($presetTargetType==''){
//				
//				$selfCheck = 1;
//			}

			switch ($targetUser) {
				case $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPersonalView']:
				case $docRoutingConfig['INTRANET_DR_ROUTE_Page']['NonAdminView']:
					$presetTargetTypeCode = $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self'];
				break;
				case $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView']:
					$presetTargetTypeCode = $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['All'];
				break;
				default:
					$presetTargetTypeCode = $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self'];
			}
			 
						
			# HTML Editor
			//$objHtmlEditor = new FCKeditor ( 'presetContents' , "100%", "220", "", "", intranet_undo_htmlspecialchars($presetContents));
			$objHtmlEditor = new FCKeditor ( 'presetContents' , "100%", "220", "", "", $presetContents);
			//$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['DocRouting'], $id);
			$HTMLEditor = $objHtmlEditor->Create2();
			 
			 
			$x = '<form name="form1" method="post" action="'. '?pe='.$newInstructionUpdateLink.'">';
			$x .= '<table width="99%" border="0" cellspacing="0" cellpadding="0">';
				$x .= '<tr>';
					$x .= '<td>';
					$x .= $indexVar['libDocRouting_ui']->GET_NAVIGATION_IP25($PAGE_NAVIGATION); 
					$x .= '<br />';
				$x .= '</td>';
				$x .= '</tr>';
				
				$x .= '<tr>';
					$x .= '<td>';
					$x .= '<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">';
						
				
						$x .= '<tr>';
							$x .= '<td class="field_title" ><span class="tabletextrequire">*</span>'. $Lang['DocRouting']['TargetType']['Title']. '</td>';
							$x .= '<td>'. $indexVar['libDocRouting_ui']->GET_TEXTBOX('presetTitle', 'presetTitle', $presetTitle, 'maxlength="100"').'</td>';					
						$x .= '</tr>';
						$x .= '<tr>';
							$x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['PresetDocInstructionContent']  . '</td>';
							$x .= '<td>'.$HTMLEditor.'</td>';									
						$x .= '</tr>';
						
//						$x .= '<tr>';
//						$x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['TargetType']['TargetType']  . '</td>';
//						
//						
//						$x .= '<td>';
//						
//								
//						if($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPublicView']){
//							$x .=  $indexVar['libDocRouting_ui']->Get_Radio_Button($Lang['DocRouting']['TargetType']['ForAllUsers'],'targetType', $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['All'], 1 , $Lang['DocRouting']['TargetType']['ForAllUsers'], $Lang['DocRouting']['TargetType']['ForAllUsers'], '').'<br />';
//						
//						}elseif($targetUser == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['AdminPersonalView'])
//						{
//							
//												$x .= $indexVar['libDocRouting_ui']->Get_Radio_Button($Lang['DocRouting']['TargetType']['ForMySelf'],'targetType', $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self'], $selfCheck, $Lang['DocRouting']['TargetType']['ForMySelf'], $Lang['DocRouting']['TargetType']['ForMySelf'], '');
//						
//						}else{	
//							$x .=  $indexVar['libDocRouting_ui']->Get_Radio_Button($Lang['DocRouting']['TargetType']['ForAllUsers'],'targetType', $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['All'], $allCheck, $Lang['DocRouting']['TargetType']['ForAllUsers'], $Lang['DocRouting']['TargetType']['ForAllUsers'], '').'<br />';
//							$x .= $indexVar['libDocRouting_ui']->Get_Radio_Button($Lang['DocRouting']['TargetType']['ForMySelf'],'targetType', $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self'], $selfCheck, $Lang['DocRouting']['TargetType']['ForMySelf'], $Lang['DocRouting']['TargetType']['ForMySelf'], '');
//						}							
//						$x .= '</td>';
//						$x .= '</tr>';
			
					$x .= '</table>';	
				$x .= '</td>';
				$x .= '</tr>';
				
				$x .= '<td>';
				$x .= '<tr>';				
					$x .= '<tr>';
					$x .= '<td>';		
					$x .= '<div class="edit_bottom">';
				    $x .= '<p class="spacer"></p>';	    	 
				    $x .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_submit_form();");
				    $x .= '&nbsp';	
				    $x .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();");
					$x .= '<p class="spacer"></p>';
				    $x .= '</div>';
					$x .= '</td>';
					$x .= '</tr>';
				$x .= '</td>';
				$x .= '</tr>';	
					
			$x .= '</table>';
			
			$x .= '<input type="hidden" name="targetType" value="'.$presetTargetTypeCode.'">';
			$x .= '<input type="hidden" name="PresetType" value="'.$PresetType.'">';	
			$x .= '<input type="hidden" id="PresetID" name="PresetID" value="'.$PresetID.'">';	
			
			$x .= '</form>';	
			
			return $x;
		
		}
		
		function getUserPresetSelection($presetType, $targetUserId, $id, $name, $onchange='',$class='') {
			global $Lang, $docRoutingConfig;
			
			$libDocRouting_preset = new libDocRouting_preset();
			
			$presetInfoAry = $libDocRouting_preset->getUserPresetRecords($presetType, $targetUserId);
			$numOfPreset = count($presetInfoAry);
			
			
			// personal first, then public
			$selectAry = array();
			$selectAry[$Lang['DocRouting']['Personal']] = array();
			$selectAry[$Lang['DocRouting']['Public']] = array();
			
			for ($i=0; $i<$numOfPreset; $i++) {
				$_presetId = $presetInfoAry[$i]['PresetID'];
				$_title = $presetInfoAry[$i]['Title'];
				$_targetType = $presetInfoAry[$i]['TargetType'];
				
				if ($_targetType == $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self']) {
					$_key = $Lang['DocRouting']['Personal'];
				}
				elseif ($_targetType == $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['All']) {
					$_key = $Lang['DocRouting']['Public'];
				}
				
				$selectAry[$_key][$_presetId] = $_title;
			}
			
			if (count($selectAry[$Lang['DocRouting']['Personal']]) == 0) {
				unset($selectAry[$Lang['DocRouting']['Personal']]);
			}
			if (count($selectAry[$Lang['DocRouting']['Public']]) == 0) {
				unset($selectAry[$Lang['DocRouting']['Public']]);
			}
			
			$onchangeAttribute = '';
			if ($onchange !== '') {
				$onchangeAttribute = 'onchange="'.$onchange.'"';
			}
			
			$selectionAttribute = ' id="'.$id.'" name="'.$name.'" class="'.$class.'" '.$onchangeAttribute;
			$firstTitle = $Lang['DocRouting']['PresetNotesSelection'];
//			debug_pr($presetInfoAry);
//			
//			debug_pr($selectAry);			
			return getSelectByAssoArray($selectAry, $selectionAttribute, '', $all=0, $noFirst=0, $firstTitle);
		}
		
		function getTagInputRemarks() {
			global $Lang, $docRoutingConfig;
			
			return str_replace('<!--maxNumOfTags-->', $docRoutingConfig['maxNumOfTags'], $Lang['DocRouting']['TagsInputRemarks']);
		}
		
		function getCopyDocumentsStep1DisplayTable($pageType){
			global $Lang, $indexVar, $docRoutingConfig, $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			
			$PAGE_NAVIGATION[] = array($Lang['DocRouting']['Button']['CopyExistingRounting'], "");
				
			$STEPS_OBJ[] = array($Lang['Group']['Options'], 1);
			$STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 0);
			//$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 0);
			
			$ldocrouting = $indexVar['libDocRouting'];
			
			$recordStatus = ($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings'])? $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete'] : $pageType;
			if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
				$recordStatus = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active'];
			}
			$userDocumentInfoAry = $ldocrouting->getUserDREntry($indexVar['drUserId'], !$indexVar['drUserIsAdmin'], $recordStatus);
			$documentIdArrUnique = Get_Array_By_Key($userDocumentInfoAry, 'DocumentID');
			
			# Get Route Entry Data 
			$documentDataAry = $indexVar['libDocRouting']->getEntryData($documentIdArrUnique, '', '', 'Title');
			$documentDataAssoAry = BuildMultiKeyAssoc($documentDataAry, 'DocumentID');
	
			# Check All Documents
			$CheckAllDocs = $this->Get_Checkbox($ID = 'CheckAllDoc', $Name = '', $Value='', $isAllGroupChecked, $Class='CheckAllDoc', $Display='', $Onclick='CheckAll(\'Doc\'); ', $Disabled='');
		
		
			### Get Btn
			$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:history.back()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
			$NextBtn = $this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "javascript:js_Submit_Form();","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
			$Btn = $NextBtn."&nbsp;".$BackBtn;
			
			$copyRoutesDisplayTable = '';
			$actionLink =  $ldocrouting->getEncryptedParameter('management','copy_doc_confirm',array());
			$copyRoutesDisplayTable .= '<form name="frm1" id="frm1" method="POST" action="index.php?pe='.$actionLink.'" onsubmit="return CheckForm();" >';
				
				$copyRoutesDisplayTable .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">';
				$copyRoutesDisplayTable .= '<tr><td >' . $this->GET_NAVIGATION($PAGE_NAVIGATION). '</td></tr>';
				$copyRoutesDisplayTable .= '</table>';
				
				$copyRoutesDisplayTable .= '<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">';
				
				$copyRoutesDisplayTable .= '<tr>';
				$copyRoutesDisplayTable .= '<td >' . $this->GET_STEPS($STEPS_OBJ) . '</td>';
				$copyRoutesDisplayTable .= '</tr>';
				
				$copyRoutesDisplayTable .= '<tr><td>'. $this->GET_NAVIGATION2($Lang['DocRouting']['ExistingRoutings']) . '</td></tr>';
					
				$copyRoutesDisplayTable .= '<tr>';
					$copyRoutesDisplayTable .= '<td>';
						$copyRoutesDisplayTable .= '<table border="0" cellpadding="0" cellspacing="0" class="common_table_list">';
							$copyRoutesDisplayTable .= '<thead>';
								$copyRoutesDisplayTable .= '<tr>';
								$copyRoutesDisplayTable .= '<th width="30%">'.$Lang['DocRouting']['Title'].'</td>';
								$copyRoutesDisplayTable .= '<th width="20%">'.$Lang['DocRouting']['Creator'].'</td>';
								$copyRoutesDisplayTable .= '<th width="20%">'.$Lang['DocRouting']['Status'].'</td>';
								$copyRoutesDisplayTable .= '<th width="15%" style="text-align:center">'.$CheckAllDocs.'</td>';					
								
								$copyRoutesDisplayTable .= '</tr>';
							$copyRoutesDisplayTable .= '</thead>';
							
							$copyRoutesDisplayTable .= '<tbody>';
							$numOfdocumentInfoArr = count($documentDataAry);
						//	debug_pr($documentDataAry);
							
							for($i=0;$i<$numOfdocumentInfoArr;$i++){
								$thisDocumentID =  $documentDataAry[$i]['DocumentID'];
								$thisDocumentTitle =  intranet_htmlspecialchars($documentDataAry[$i]['Title']);
								$thisDocumentCreatorName =  intranet_htmlspecialchars($documentDataAry[$i]['CreatorName']);
								
								$thisDocumentCreatorID = $documentDataAry[$i]['UserID'];
								
								## Archive User 
								if($thisDocumentCreatorName==''){
									$libuser = new libuser();							
									$thisArchivedUserInforArr = $libuser->getNameWithClassNumber($thisDocumentCreatorID, 1);
									$thisDocumentCreatorName = '<span class="tabletextrequire">*</span>' . $thisArchivedUserInforArr;
								}	
								
								
								$thisDocumentRecordStatus =  $documentDataAry[$i]['RecordStatus'];
								
								$thisDocumentRecordStatusDisplay ='';	
								if($thisDocumentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']){
									$thisDocumentRecordStatusDisplay = $Lang['DocRouting']['Active'];	
								}elseif($thisDocumentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']){
									$thisDocumentRecordStatusDisplay = $Lang['DocRouting']['Draft'] = "Draft";	
								}elseif($thisDocumentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']){
									$thisDocumentRecordStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];	
								}
								
								# Individual Checkbox
								$eachDocCheckBox = $this->Get_Checkbox($ID = 'eachDocCheckBox_'.$thisDocumentID, $Name = 'CopyDoc[]', $Value=$thisDocumentID, $isGroupChecked, $Class='Doc', $Display='', $Onclick='', $Disabled='');
								
						
								$copyRoutesDisplayTable .= '<tr>';
									$copyRoutesDisplayTable .= '<td>';
										$copyRoutesDisplayTable .= $thisDocumentTitle;
									$copyRoutesDisplayTable .= '</td>';
									$copyRoutesDisplayTable .= '<td>';
										$copyRoutesDisplayTable .= $thisDocumentCreatorName;
									$copyRoutesDisplayTable .= '</td>';
									$copyRoutesDisplayTable .= '<td>';
									
										$copyRoutesDisplayTable .= $thisDocumentRecordStatusDisplay;
									$copyRoutesDisplayTable .= '</td>';
									$copyRoutesDisplayTable .= '<td align="center">';
										$copyRoutesDisplayTable .= $eachDocCheckBox;
									$copyRoutesDisplayTable .= '</td>';
																
								
								$copyRoutesDisplayTable .= '</tr>';
							}
							$copyRoutesDisplayTable .= '</tbody>';
						$copyRoutesDisplayTable .= '</table>';
						$copyRoutesDisplayTable .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'];
					$copyRoutesDisplayTable .= '</td>';
				$copyRoutesDisplayTable .= '</tr>';
				$copyRoutesDisplayTable .= '<tr>';
					$copyRoutesDisplayTable .= '<td align="center">';
					$copyRoutesDisplayTable .= $Btn;
					$copyRoutesDisplayTable .= '</td>';
				$copyRoutesDisplayTable .= '</tr>';			
				$copyRoutesDisplayTable .= '</table>';
				$copyRoutesDisplayTable .= '<input type="hidden" id="pageType" name="pageType" value="'.cleanCrossSiteScriptingCode($pageType).'">';
			$copyRoutesDisplayTable .= '</form>';
			
			return $copyRoutesDisplayTable;
			
		}
				
		function getCopyDocumentsStep2DisplayTable($CopyDoc,$CopyRoute,$pageType){
			global $Lang, $indexVar, $docRoutingConfig;
			
			$PAGE_NAVIGATION[] = array($Lang['DocRouting']['Button']['CopyExistingRounting'], "");
					
			$STEPS_OBJ[] = array($Lang['Group']['Options'], 0);
			$STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 1);
		//	$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 0);
	
			
			# Get Route Entry Data 
			$numOfDoc = count($CopyDoc);
			if($numOfDoc>0){
				$documentDataAry = $indexVar['libDocRouting']->getEntryData($CopyDoc);
				$documentDataAssoAry = BuildMultiKeyAssoc($documentDataAry, 'DocumentID');
			}
		
			### Get Btn
			$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "javascript:history.back()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
			$NextBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
			$Btn = $NextBtn."&nbsp;".$BackBtn;
			
			$copyRoutesDisplayTable = '';
			$actionLink =  $indexVar['libDocRouting']->getEncryptedParameter('management','copy_doc_update',array());
			$copyRoutesDisplayTable .= '<form name="frm1" method="POST" action="index.php?pe='.$actionLink.'" onsubmit="return js_Check_Form();" >';
				
				$copyRoutesDisplayTable .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">';
				$copyRoutesDisplayTable .= '<tr><td >' . $this->GET_NAVIGATION($PAGE_NAVIGATION). '</td></tr>';
				$copyRoutesDisplayTable .= '</table>';
				
				$copyRoutesDisplayTable .= '<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">';
				
				$copyRoutesDisplayTable .= '<tr>';
					$copyRoutesDisplayTable .= '<td>' . $this->GET_STEPS($STEPS_OBJ) . '</td>';
				$copyRoutesDisplayTable .= '</tr>';
				
				$copyRoutesDisplayTable .= '<tr><td>'. $this->GET_NAVIGATION2($Lang['DocRouting']['NewDocRoutings']) . '</td></tr>';
					
				$copyRoutesDisplayTable .= '<tr>';
					$copyRoutesDisplayTable .= '<td>';
						$copyRoutesDisplayTable .= '<table border="0" cellpadding="0" cellspacing="0" class="common_table_list">';
							$copyRoutesDisplayTable .= '<thead>';
								$copyRoutesDisplayTable .= '<tr>';
								$copyRoutesDisplayTable .= '<th width="30%">'.$Lang['DocRouting']['Title'].'</td>';
								$copyRoutesDisplayTable .= '<th width="20%">'.$Lang['DocRouting']['Status'].'</td>';				
								$copyRoutesDisplayTable .= '</tr>';
							$copyRoutesDisplayTable .= '</thead>';
							
							$copyRoutesDisplayTable .= '<tbody>';
							$numOfdocumentInfoArr = count($documentDataAry);
							
							for($i=0;$i<$numOfdocumentInfoArr;$i++){
								$thisDocumentID =  $documentDataAry[$i]['DocumentID'];
								$thisDocumentTitle =  intranet_htmlspecialchars($documentDataAry[$i]['Title']);			
								$thisDocumentRecordStatus =  $documentDataAry[$i]['RecordStatus'];
								$documentTitleDisplayTextbox = $this->GET_TEXTBOX('documentTitle', 'documentTitle[]', $thisDocumentTitle, 'documentTitle', $OtherPar=array());
								
								$statusSelectionData = array();
								$statusSelectionData []= array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active'] , $Lang['DocRouting']['Active']);
								$statusSelectionData []= array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft'], $Lang['DocRouting']['Draft']);
								$statusSelectionData []= array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete'], $Lang['DocRouting']['StatusDisplay']['Completed']);
								
								$statusDisplay = $this->GET_SELECTION_BOX($statusSelectionData, 'name="documentStatus[]"', $defaultStatus, $thisDocumentRecordStatus);
								
								$copyRoutesDisplayTable .= '<tr>';
									$copyRoutesDisplayTable .= '<td>';
										$copyRoutesDisplayTable .= $documentTitleDisplayTextbox;
										$copyRoutesDisplayTable .= "<br />";
										$copyRoutesDisplayTable .= "<input type='hidden' name='DocumentID[]' value='".$thisDocumentID."'>";
									$copyRoutesDisplayTable .= '</td>';
									$copyRoutesDisplayTable .= '<td>'.$statusDisplay. '</td>';								
								$copyRoutesDisplayTable .= '</tr>';
								
							}
							
							$copyRoutesDisplayTable .= '</tbody>';
						$copyRoutesDisplayTable .= '</table>';
					$copyRoutesDisplayTable .= '</td>';
				$copyRoutesDisplayTable .= '</tr>';
				$copyRoutesDisplayTable .= '<tr>';
					$copyRoutesDisplayTable .= '<td align="center">';
					$copyRoutesDisplayTable .= $Btn;
					$copyRoutesDisplayTable .= '</td>';
				$copyRoutesDisplayTable .= '</tr>';			
				
				$copyRoutesDisplayTable .= '</table>';
				$copyRoutesDisplayTable .= '<input type="hidden" name="pageType" value="'.cleanCrossSiteScriptingCode($pageType) .'">';
				
			$copyRoutesDisplayTable .= '</form>';
			
			return $copyRoutesDisplayTable;
			
		}
		
		function getDeleteDocumentsStep1DisplayTable($pageType){
		    global $Lang, $indexVar, $docRoutingConfig, $PATH_WRT_ROOT;
		    include_once($PATH_WRT_ROOT."includes/libuser.php");
		    
		    $PAGE_NAVIGATION[] = array($Lang['DocRouting']['Button']['DeleteExistingRounting'], "");
		    
		    $STEPS_OBJ[] = array($Lang['Group']['Options'], 1);
		    $STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 0);
		    //$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 0);
		    
		    $ldocrouting = $indexVar['libDocRouting'];
		    
		    $recordStatus = ($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings'])? $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete'] : $pageType;
		    if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
		        $recordStatus = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active'];
		    }
		    $userDocumentInfoAry = $ldocrouting->getUserDREntry($indexVar['drUserId'], !$indexVar['drUserIsAdmin'], $recordStatus);
		    $documentIdArrUnique = Get_Array_By_Key($userDocumentInfoAry, 'DocumentID');
		    
		    # Get Route Entry Data
		    $documentDataAry = $indexVar['libDocRouting']->getEntryData($documentIdArrUnique, '', '', 'Title');
		    $documentDataAssoAry = BuildMultiKeyAssoc($documentDataAry, 'DocumentID');
		    
		    # Check All Documents
		    $CheckAllDocs = $this->Get_Checkbox($ID = 'CheckAllDoc', $Name = '', $Value='', $isAllGroupChecked, $Class='CheckAllDoc', $Display='', $Onclick='CheckAll(\'Doc\'); ', $Disabled='');
		    
		    
		    ### Get Btn
		    $BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:history.back()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
		    $NextBtn = $this->GET_ACTION_BTN($Lang['Btn']['Next'], "button", "javascript:js_Submit_Form();","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
		    $Btn = $NextBtn."&nbsp;".$BackBtn;
		    
		    $deleteRoutesDisplayTable = '';
		    $actionLink =  $ldocrouting->getEncryptedParameter('management','delete_doc_confirm',array());
		    $deleteRoutesDisplayTable .= '<form name="frm1" id="frm1" method="POST" action="index.php?pe='.$actionLink.'" onsubmit="return CheckForm();" >';
		    
		    $deleteRoutesDisplayTable .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">';
		    $deleteRoutesDisplayTable .= '<tr><td >' . $this->GET_NAVIGATION($PAGE_NAVIGATION). '</td></tr>';
		    $deleteRoutesDisplayTable .= '</table>';
		    
		    $deleteRoutesDisplayTable .= '<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">';
		    
		    $deleteRoutesDisplayTable .= '<tr>';
		    $deleteRoutesDisplayTable .= '<td >' . $this->GET_STEPS($STEPS_OBJ) . '</td>';
		    $deleteRoutesDisplayTable .= '</tr>';
		    
		    $deleteRoutesDisplayTable .= '<tr><td>'. $this->GET_NAVIGATION2($Lang['DocRouting']['ExistingRoutings']) . '</td></tr>';
		    
		    $deleteRoutesDisplayTable .= '<tr>';
		    $deleteRoutesDisplayTable .= '<td>';
		    $deleteRoutesDisplayTable .= '<table border="0" cellpadding="0" cellspacing="0" class="common_table_list">';
		    $deleteRoutesDisplayTable .= '<thead>';
		    $deleteRoutesDisplayTable .= '<tr>';
		    $deleteRoutesDisplayTable .= '<th width="30%">'.$Lang['DocRouting']['Title'].'</td>';
		    $deleteRoutesDisplayTable .= '<th width="20%">'.$Lang['DocRouting']['Creator'].'</td>';
		    $deleteRoutesDisplayTable .= '<th width="20%">'.$Lang['DocRouting']['Status'].'</td>';
		    $deleteRoutesDisplayTable .= '<th width="15%" style="text-align:center">'.$CheckAllDocs.'</td>';
		    
		    $deleteRoutesDisplayTable .= '</tr>';
		    $deleteRoutesDisplayTable .= '</thead>';
		    
		    $deleteRoutesDisplayTable .= '<tbody>';
		    $numOfdocumentInfoArr = count($documentDataAry);
		    //	debug_pr($documentDataAry);
		    
		    for($i=0;$i<$numOfdocumentInfoArr;$i++){
		        $thisDocumentID =  $documentDataAry[$i]['DocumentID'];
		        $thisDocumentTitle =  intranet_htmlspecialchars($documentDataAry[$i]['Title']);
		        $thisDocumentCreatorName =  intranet_htmlspecialchars($documentDataAry[$i]['CreatorName']);
		        
		        $thisDocumentCreatorID = $documentDataAry[$i]['UserID'];
		        
		        ## Archive User
		        if($thisDocumentCreatorName==''){
		            $libuser = new libuser();
		            $thisArchivedUserInforArr = $libuser->getNameWithClassNumber($thisDocumentCreatorID, 1);
		            $thisDocumentCreatorName = '<span class="tabletextrequire">*</span>' . $thisArchivedUserInforArr;
		        }
		        
		        
		        $thisDocumentRecordStatus =  $documentDataAry[$i]['RecordStatus'];
		        
		        $thisDocumentRecordStatusDisplay ='';
		        if($thisDocumentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']){
		            $thisDocumentRecordStatusDisplay = $Lang['DocRouting']['Active'];
		        }elseif($thisDocumentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']){
		            $thisDocumentRecordStatusDisplay = $Lang['DocRouting']['Draft'] = "Draft";
		        }elseif($thisDocumentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']){
		            $thisDocumentRecordStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];
		        }
		        
		        # Individual Checkbox
		        $eachDocCheckBox = $this->Get_Checkbox($ID = 'eachDocCheckBox_'.$thisDocumentID, $Name = 'DeleteDoc[]', $Value=$thisDocumentID, $isGroupChecked, $Class='Doc', $Display='', $Onclick='', $Disabled='');
		        
		        
		        $deleteRoutesDisplayTable .= '<tr>';
		        $deleteRoutesDisplayTable .= '<td>';
		        $deleteRoutesDisplayTable .= $thisDocumentTitle;
		        $deleteRoutesDisplayTable .= '</td>';
		        $deleteRoutesDisplayTable .= '<td>';
		        $deleteRoutesDisplayTable .= $thisDocumentCreatorName;
		        $deleteRoutesDisplayTable .= '</td>';
		        $deleteRoutesDisplayTable .= '<td>';
		        
		        $deleteRoutesDisplayTable .= $thisDocumentRecordStatusDisplay;
		        $deleteRoutesDisplayTable .= '</td>';
		        $deleteRoutesDisplayTable .= '<td align="center">';
		        $deleteRoutesDisplayTable .= $eachDocCheckBox;
		        $deleteRoutesDisplayTable .= '</td>';
		        
		        
		        $deleteRoutesDisplayTable .= '</tr>';
		    }
		    $deleteRoutesDisplayTable .= '</tbody>';
		    $deleteRoutesDisplayTable .= '</table>';
		    $deleteRoutesDisplayTable .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'];
		    $deleteRoutesDisplayTable .= '</td>';
		    $deleteRoutesDisplayTable .= '</tr>';
		    $deleteRoutesDisplayTable .= '<tr>';
		    $deleteRoutesDisplayTable .= '<td align="center">';
		    $deleteRoutesDisplayTable .= $Btn;
		    $deleteRoutesDisplayTable .= '</td>';
		    $deleteRoutesDisplayTable .= '</tr>';
		    $deleteRoutesDisplayTable .= '</table>';
		    $deleteRoutesDisplayTable .= '<input type="hidden" id="pageType" name="pageType" value="'.cleanCrossSiteScriptingCode($pageType).'">';
		    $deleteRoutesDisplayTable .= '</form>';
		    
		    return $deleteRoutesDisplayTable;
		    
		}
		
		function getDeleteDocumentsStep2DisplayTable($DeleteDoc,$pageType){
		    global $Lang, $indexVar, $docRoutingConfig;
		    
		    $PAGE_NAVIGATION[] = array($Lang['DocRouting']['Button']['DeleteExistingRounting'], "");
		    
		    $STEPS_OBJ[] = array($Lang['Group']['Options'], 0);
		    $STEPS_OBJ[] = array($Lang['Group']['Confirmation'], 1);
		    //	$STEPS_OBJ[] = array($Lang['Group']['CopyResult'], 0);
		    
		    
		    # Get Route Entry Data
		    $numOfDoc = count($DeleteDoc);
		    if($numOfDoc>0){
		        $documentDataAry = $indexVar['libDocRouting']->getEntryData($DeleteDoc);
		        $documentDataAssoAry = BuildMultiKeyAssoc($documentDataAry, 'DocumentID');
		    }
		    
		    ### Get Btn
		    $BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "javascript:history.back()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
		    $NextBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
		    $Btn = $NextBtn."&nbsp;".$BackBtn;
		    
		    $deleteRoutesDisplayTable = '';
		    
		    $actionLink =  $indexVar['libDocRouting']->getEncryptedParameter('management','delete_doc_update',array());
		    $deleteRoutesDisplayTable .= '<form name="frm1" method="POST" action="index.php?pe='.$actionLink.'" onsubmit="return js_Check_Form();" >';
		    
		    $deleteRoutesDisplayTable .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">';
		    $deleteRoutesDisplayTable .= '<tr><td >' . $this->GET_NAVIGATION($PAGE_NAVIGATION). '</td></tr>';
		    $deleteRoutesDisplayTable .= '</table>';
		    
		    $deleteRoutesDisplayTable .= '<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">';
		    
		    $deleteRoutesDisplayTable .= '<tr>';
		    $deleteRoutesDisplayTable .= '<td>' . $this->GET_STEPS($STEPS_OBJ) . '</td>';
		    $deleteRoutesDisplayTable .= '</tr>';
		    
		    $deleteRoutesDisplayTable .= '<tr><td>'. $this->GET_NAVIGATION2($Lang['DocRouting']['DeleteDocRoutings']) . '</td></tr>';
		    
		    $deleteRoutesDisplayTable .= '<tr>';
		    $deleteRoutesDisplayTable .= '<td>';
		    $deleteRoutesDisplayTable .= '<table border="0" cellpadding="0" cellspacing="0" class="common_table_list">';
		    $deleteRoutesDisplayTable .= '<thead>';
		    $deleteRoutesDisplayTable .= '<tr>';
		    $deleteRoutesDisplayTable .= '<th width="30%">'.$Lang['DocRouting']['Title'].'</td>';
		    $deleteRoutesDisplayTable .= '<th width="20%">'.$Lang['DocRouting']['Status'].'</td>';
		    $deleteRoutesDisplayTable .= '</tr>';
		    $deleteRoutesDisplayTable .= '</thead>';
		    
		    $deleteRoutesDisplayTable .= '<tbody>';
		    $numOfdocumentInfoArr = count($documentDataAry);
		    
		    for($i=0;$i<$numOfdocumentInfoArr;$i++){
		        $thisDocumentID =  $documentDataAry[$i]['DocumentID'];
		        $thisDocumentTitle =  intranet_htmlspecialchars($documentDataAry[$i]['Title']);
		        $thisDocumentRecordStatus =  $documentDataAry[$i]['RecordStatus'];
		        		        
		        $thisDocumentRecordStatusDisplay ='';
		        if($thisDocumentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']){
		            $thisDocumentRecordStatusDisplay = $Lang['DocRouting']['Active'];
		        }elseif($thisDocumentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']){
		            $thisDocumentRecordStatusDisplay = $Lang['DocRouting']['Draft'] = "Draft";
		        }elseif($thisDocumentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']){
		            $thisDocumentRecordStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];
		        }
		        
		        $deleteRoutesDisplayTable .= '<tr>';
		        $deleteRoutesDisplayTable .= '<td>';
		        $deleteRoutesDisplayTable .= $thisDocumentTitle;
		        $deleteRoutesDisplayTable .= "<br />";
		        $deleteRoutesDisplayTable .= "<input type='hidden' name='documentId[]' value='".$thisDocumentID."'>";
		        $deleteRoutesDisplayTable .= '</td>';
		        $deleteRoutesDisplayTable .= '<td>'.$thisDocumentRecordStatusDisplay. '</td>';
		        $deleteRoutesDisplayTable .= '</tr>';
		        
		    }
		    
		    $deleteRoutesDisplayTable .= '</tbody>';
		    $deleteRoutesDisplayTable .= '</table>';
		    $deleteRoutesDisplayTable .= '</td>';
		    $deleteRoutesDisplayTable .= '</tr>';
		    $deleteRoutesDisplayTable .= '<tr>';
		    $deleteRoutesDisplayTable .= '<td align="center">';
		    $deleteRoutesDisplayTable .= $Btn;
		    $deleteRoutesDisplayTable .= '</td>';
		    $deleteRoutesDisplayTable .= '</tr>';
		    
		    $deleteRoutesDisplayTable .= '</table>';
		    $deleteRoutesDisplayTable .= '<input type="hidden" name="pageType" value="'.cleanCrossSiteScriptingCode($pageType) .'">';
		    
		    $deleteRoutesDisplayTable .= '</form>';
		    
		    return $deleteRoutesDisplayTable;
		}
		
		function getPrintInstructionStep1($routingNumber){
			global $indexVar, $PATH_WRT_ROOT, $Lang, $docRoutingConfig,$button_close ;
			include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");
			$libDocRouting_preset = new libDocRouting_preset();

			# Action Link 
			$actionLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'print_instruction_step2', array('PageType' => $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings'], 'routingNumber' => $routingNumber));
				
			# Personal Preset Instruction Data
			$personalPresetInfoAry = $libDocRouting_preset->getPresetData('', $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote'], $indexVar['drUserId'], 'Title', $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['Self']);
			$numOfPersonalPreset = count($personalPresetInfoAry);
			
			# Public Preset Instruction Data
			$publicPresetInfoAry = $libDocRouting_preset->getPresetData('', $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote'], '', 'Title', $docRoutingConfig['INTRANET_DR_PRESET']['TargetType']['All']);
			$numOfPublicPreset = count($publicPresetInfoAry);
		
			$printInstructionStep1Table = '';
			$printInstructionStep1Table .= '<form id="printInstructionForm" name="printInstructionForm" action="index.php?pe='.$actionLink.'" enctype="multipart/form-data">';
				$printInstructionStep1Table .= '<table width="100%" align="center" class="form_table_v30">';
					$printInstructionStep1Table .= '<tr>';
						$printInstructionStep1Table .= '<td>' . $Lang['DocRouting']['PrintInstruction']['PageTitle']. '</td>';
					$printInstructionStep1Table .= '</tr>';
				
					$printInstructionStep1Table .= '<tr>';
						$printInstructionStep1Table .= '<td align="center">';

							###### Table Header #####
							$tableHeader = '';
							$selectAllOption = $this->Get_Checkbox('InstructionIdAll_personal', 'InstructionIdAll', "", "", 'InstructionIdAll_personal', '', 'javascript:js_Select_All(\'personal\')');
											
							$tableHeader .= '<thead>';
								$tableHeader .= '<tr>';			
									$tableHeader .= '<th class="field_title" style="width:10px;">#</th>';							
									$tableHeader .= '<th class="field_title">' . $Lang['DocRouting']['TargetType']['Title']. '</th>';							
									$tableHeader .= '<th class="field_title">'. $Lang['DocRouting']['PresetDocInstructionContent'].'</th>';
									$tableHeader .= '<th class="field_title" style="width:15px;"><center>';
									$tableHeader .= $selectAllOption;
									$tableHeader .= '</center></th>';
								$tableHeader .= '</tr>';		
							$tableHeader .= '</thead>';
							###### Table Header #####

		
							##### Personal Preset Instruction Start #####
							# Personal Title
							$printInstructionStep1Table .=  $this->GET_NAVIGATION2($Lang['DocRouting']['Personal']);
							 
							$printInstructionStep1Table .='<table class="common_table_list_v30" width="100%" align="center">';
							
							# Table Header
							$printInstructionStep1Table .= $tableHeader;
					
							# Table Body
							$printInstructionStep1Table .= '<tbody>';						
							$displayNum = 0;
							for($i=0;$i<$numOfPersonalPreset;$i++){
								$displayNum = $i+1;
								$thisPresetId = $personalPresetInfoAry[$i]['PresetID'];
								
								$thisInstructionTitle = intranet_htmlspecialchars($personalPresetInfoAry[$i]['Title']);
								$thisInstructionContent = strip_tags($personalPresetInfoAry[$i]['Contents']);
								$thisOptionChk = $this->Get_Checkbox('InstructionId', 'InstructionId[]', "$thisPresetId", '', 'InstructionId_personal', "");
								
								$printInstructionStep1Table .= '<tr>';	
									$printInstructionStep1Table .= '<td >' . $displayNum .'</td>';
									$printInstructionStep1Table .= '<td >' . $thisInstructionTitle .'</td>';
									$printInstructionStep1Table .= '<td >' . ($thisInstructionContent? $thisInstructionContent: '&nbsp' ). '</td>';
									$printInstructionStep1Table .= '<td >' . $thisOptionChk .'</td>';
								$printInstructionStep1Table .= '</tr>';		
							}
							
							$printInstructionStep1Table .= '</tbody>';
						$printInstructionStep1Table .='</table><br />';	
						##### Personal Preset Instruction End #####
						
						##### Public Preset Instruction Start #####
						# Public Title 
						$printInstructionStep1Table .= $this->GET_NAVIGATION2($Lang['DocRouting']['Public']); ; 
						
						$printInstructionStep1Table .='<table class="common_table_list_v30" width="100%" align="center">';
						# Table Header
						$printInstructionStep1Table .= str_replace('personal','public',$tableHeader);
						
						# Table Body	
						$printInstructionStep1Table .= '<tbody>';
							$displayNum ='';
							for($i=0;$i<$numOfPublicPreset;$i++){
								$displayNum = $i+1;
								$thisPresetId = $publicPresetInfoAry[$i]['PresetID'];	
								$thisInstructionTitle = intranet_htmlspecialchars($publicPresetInfoAry[$i]['Title']);
								$thisInstructionContent = strip_tags($publicPresetInfoAry[$i]['Contents']);
								$thisOptionChk = $this->Get_Checkbox('InstructionId', 'InstructionId[]', "$thisPresetId", '', 'InstructionId_public', "");
		
								$printInstructionStep1Table .= '<tr>';
									$printInstructionStep1Table .= '<td >'.$displayNum.'</td>';							
									$printInstructionStep1Table .= '<td >'.$thisInstructionTitle.'</td>';										
									$printInstructionStep1Table .= '<td >'.$thisInstructionContent.'</td>';															
									$printInstructionStep1Table .= '<td >'.$thisOptionChk.'</td>';						
								$printInstructionStep1Table .= '</tr>';		
							}					
						$printInstructionStep1Table .= '</tbody>';	
							
						$printInstructionStep1Table .= '</table>';
						##### Public Preset Instruction End #####
						
						$printInstructionStep1Table .= '</td>';	
					$printInstructionStep1Table .= '</tr>';	
						
					# Action Btn
					$printInstructionStep1Table .= '<tr>';
						$printInstructionStep1Table .= '<td align="center" colspan="2">';	
							$printInstructionStep1Table .= '<center>';
							$printInstructionStep1Table .= $this->GET_ACTION_BTN($Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Submit'], "button", "js_Submit_Print_Option(document.printInstructionForm, '$actionLink');","submit2");
							$printInstructionStep1Table .= $this->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn");
							$printInstructionStep1Table .= '</center>';
						$printInstructionStep1Table .= '</td>';	
					$printInstructionStep1Table .= '</tr>';	
				
				$printInstructionStep1Table.= '</table>';		
			$printInstructionStep1Table .= '</form>';
		
			return $printInstructionStep1Table;		 
		}
		
		function getPrintInstructionStep2($routingNumber, $instructionId){
			global $indexVar, $Lang, $docRoutingConfig, $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");
			$libDocRouting_preset = new libDocRouting_preset();
			
			# Get Preset Data
			$presetInfoAry = $libDocRouting_preset->getPresetData($instructionId, $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['RoutingNote'], '', 'Title');
			$numOfPreset = count($presetInfoAry);
			
			# Print Button
			$PrintBtn = '';
			$PrintBtn .= '<table border="0" cellpadding="2" width="95%" cellspacing="0" class="print_hide" >'."\n";
				$PrintBtn .= '<tr><td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</span></td></tr>'."\n";
			$PrintBtn .= '</table>';
			
			# Display Print Form
			$printInstructionStep2Table = '';
			$printInstructionStep2Table .= $PrintBtn ; 
			
			$printInstructionStep2Table .= "<table width='97%' cellpadding='2' cellspacing='0' style='margin:20px auto; padding:15px; border: 2px solid #000;'>\n";
				$printInstructionStep2Table .= "<tr>";
					$printInstructionStep2Table .=	"<td align='center'>";	
							# Header 
							$printInstructionStep2Table .= "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
								$printInstructionStep2Table .= "<tr style='height:25px;'>";
									$printInstructionStep2Table .= "<td colspan='3' style='font-family:Times New Roman; font-size:20px; color:#000; padding-left:15px;' align='center'>";
									$printInstructionStep2Table .= "<b><i>" . $Lang['DocRouting']['PrintInstruction']['SchoolName'] . "</i></b>";
									$printInstructionStep2Table .= "</td>";
								$printInstructionStep2Table .= "</tr>";
								
								$printInstructionStep2Table .= "<tr style='height:20px;'>";
									$printInstructionStep2Table .= "<td width='30%' >";
									$printInstructionStep2Table .= "</td>";
									$printInstructionStep2Table .= "<td width='30%' style='font-family:Times New Roman; font-size: 20px;' align='center'><b><i>";
									$printInstructionStep2Table .= "<span style='border: 1px solid #000; padding-right:8px; padding-left:8px;'>" . $Lang['DocRouting']['PrintInstruction']['Memo'] . "</span>";
									$printInstructionStep2Table .= "</b></i></td>";
									$printInstructionStep2Table .= "<td width='30%' style='font-family:Times New Roman; font-size: 20px;' align='left'><b><i>";
									$printInstructionStep2Table .= "<span style='float:right; margin-right:20px;'><ul><li>N</li><li>R</li><li>R+S</li></ul></span>";
									$printInstructionStep2Table .= "</b></i></td>";
								$printInstructionStep2Table .= "</tr>";
								
								$printInstructionStep2Table .= "<tr>";
								$printInstructionStep2Table .= "<td colspan='2' style='font-family:Times New Roman; font-size: 20px;  padding-left:15px;'><b><i>" . $Lang['DocRouting']['PrintInstruction']['FromThePrincipal'];
								$printInstructionStep2Table .= "</b></i></td>";
								$printInstructionStep2Table .= "<td style='font-family:Times New Roman; font-size:20px; padding-left:15px;'><b><i>" . $Lang['DocRouting']['PrintInstruction']['Date'] . ': </b></i>';
								$printInstructionStep2Table .= "</td>";
								$printInstructionStep2Table .= "</tr>";
								
								$printInstructionStep2Table .= "<tr'>";
									$printInstructionStep2Table .= "<td width='100%' colspan='3' style='font-family:Times New Roman; font-size: 20px; padding: 5 15px;' align='left'>";
									$printInstructionStep2Table .= "<div width='100% ' style='width:100%;height: 100%; border: 1px solid #000; line-height: 30px;'>";
									$printInstructionStep2Table .= "<b><i>" . $Lang['DocRouting']['PrintInstruction']['TO'] . ': ' . "</i></b>";
									$printInstructionStep2Table .= "</div>";
									$printInstructionStep2Table .= "</td>";
								$printInstructionStep2Table .= "</tr>";					
							
								# Instruction Content 
								$printInstructionStep2Table .= "<tr>";
									$printInstructionStep2Table .= "<td colspan='3' style='font-family:Times New Roman; font-size: 18px;  padding: 5 15px; ' align='left'><b><i>";
									$printInstructionStep2Table .= "<div id='instructionDisplayLayer' style='min-height:350px; '>";

									$displayNum = 0;
									for($i=0;$i<$numOfPreset;$i++){
										$displayNum = $i+1;
										
										$thisContent = $displayNum . '. ' . strip_tags ($presetInfoAry[$i]['Contents'], '<br />');
										$printInstructionStep2Table .= $thisContent;
										$printInstructionStep2Table .= "<br /><br />";
									}

									$printInstructionStep2Table .= "</div>"; 
									$printInstructionStep2Table .= "</i></b>";
									$printInstructionStep2Table .= "</td>";
								$printInstructionStep2Table .= "</tr>";
															
								$printInstructionStep2Table .= "<tr style='height:25px;'>";
									$printInstructionStep2Table .= "<td width='30%'>";
									$printInstructionStep2Table .= "</td>";
									$printInstructionStep2Table .= "<td width='30%'>";
									$printInstructionStep2Table .= "</td>";
									$printInstructionStep2Table .= "<td width='30%' style='font-family:Times New Roman; font-size: 20px; padding-right:15px;' align='center'><b><i>";
									$printInstructionStep2Table .= "<br />";
									$printInstructionStep2Table .= "<span style='width:100%; line-height: 35px; float:right;  border-top:1px solid; margin-bottom:30px;'>" . $Lang['DocRouting']['PrintInstruction']['Signature'] . "</span>";
									$printInstructionStep2Table .= "</td>";
								$printInstructionStep2Table .= "</tr>";
								
								# Footer
								$printInstructionStep2Table .= "<tr>";
									$printInstructionStep2Table .= "<td colspan='3' style='font-family:Times New Roman; font-size: 17px; border-top: 1.5px dotted #000; ' align='center'><b><i>";
									$printInstructionStep2Table .= $Lang['DocRouting']['PrintInstruction']['CommentsAndRemarks'] . "</i></b>";
									$printInstructionStep2Table .= "<div style='min-height:100px; height: 100px; width:100%'; padding: 0 15px;></div>";
									$printInstructionStep2Table .= "</td>";
								$printInstructionStep2Table .= "</tr>";
								
							$printInstructionStep2Table .= "</table>";							
						$printInstructionStep2Table .= "</td>";
					$printInstructionStep2Table .= "</tr>";
				
			$printInstructionStep2Table .= "</table>";	
			
			return $printInstructionStep2Table;			
		}	
		
		function getCopyRouteDisplay($documentID, $routingNumber, $newRouteType){
			global $indexVar, $Lang, $button_close, $docRoutingConfig;
			
			# Get Route Data
			$routeDataArr = $indexVar['libDocRouting']->getRouteData($documentID);
			$routeDataIDAssocArr = BuildMultiKeyAssoc($routeDataArr, 'RouteID');
			
			# Copy Route Display Table
			$copyRouteOptionTable = '';
			$copyRouteOptionTable .= '<table width="100%" align="center">';
	
				# Form Title 
				$copyRouteOptionTable .= '<tr>';
					$copyRouteOptionTable .= '<td>'.$Lang['DocRouting']['Button']['CopyExistingRounting'].'</td>';
				$copyRouteOptionTable .= '</tr>';	
				
				$copyRouteOptionTable .= '<tr>';			
					$copyRouteOptionTable .= '<td align="center">';
	
						$copyRouteOptionTable .='<table width="50%" class="common_table_list_v30" align="center" cellspacing="0" cellspading="0">';
							# Header 
							$copyRouteOptionTable .='<thead>';
								$copyRouteOptionTable .='<tr>';
									$copyRouteOptionTable .='<th>' . $Lang['DocRouting']['ExistingRoutings'] . '</th>';
									$copyRouteOptionTable .='<th>' . $Lang['DocRouting']['Users'] .'</th>';
									$copyRouteOptionTable .='<th>' . $Lang['DocRouting']['Status'] . '</th>';
									$copyRouteOptionTable .='<th>&nbsp</th>';
								$copyRouteOptionTable .='</tr>';
							$copyRouteOptionTable .='</thead>';
							
							# Row
							$copyRouteOptionTable .='<tbody>';
							$numOfRouteDataArr = count($routeDataArr);
							$routeNum = 0;
							
							# If no routing records
							if($numOfRouteDataArr==0){
								$copyRouteOptionTable .='<tr>';
									$copyRouteOptionTable .='<td colspan="4"> '. $Lang['General']['NoRecordFound'].'</td>';
								$copyRouteOptionTable .='</tr>';
							}
							
							for($i=0;$i<$numOfRouteDataArr;$i++){
								$routeID = $routeDataArr[$i]['RouteID'];
								$thisRouteStatus=$routeDataArr[$i]['RecordStatus'];
								
								$targetUserIDArr = array();
								$targetUserNameArr = array();
								$extraTargetUserIDArr = array();
								$extraTargetUserNameArr = array();
								
								$tmpUsers = $indexVar['libDocRouting']->getRouteTargetUsers($routeID);
								for($j=0;$j<count($tmpUsers);$j++){
									if($tmpUsers[$j]['AccessRight']==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['TargetUserOnly']){
										$targetUserIDArr[] = $tmpUsers[$j]['UserID'];
										$targetUserNameArr[] = $tmpUsers[$j]['UserName'];
			
									}else if($tmpUsers[$j]['AccessRight']==$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser']){
										$extraTargetUserIDArr[] = $tmpUsers[$j]['UserID'];
										$extraTargetUserNameArr[] = $tmpUsers[$j]['UserName'];
									}
								}
								
								$note = $routeDataArr[$i]['Note'];
								
								$targetUserIDArrList = implode($targetUserIDArr,',');
								$targetUserNameArrList = implode($targetUserNameArr,',');
								$extraTargetUserIDArrList = implode($extraTargetUserIDArr,',');
								$extraTargetUserNameArrList = implode($extraTargetUserNameArr,',');
												
							   	$effectiveType = $routeDataArr[$i]['EffectiveType'];
							    $effectiveStartDateAndTime = $routeDataArr[$i]['EffectiveStartDate']; 
							 	$effectiveStartDateArr = explode(' ', $effectiveStartDateAndTime);
							 	$effectiveStartDate = $effectiveStartDateArr[0];
								$effectiveStartTimeArr = explode(':', $effectiveStartDateArr[1]);
								$effectiveStartTimeHour = $effectiveStartTimeArr[0];
								$effectiveStartTimeMinute = $effectiveStartTimeArr[1];
								$effectiveStartTimeSecond = $effectiveStartTimeArr[2];
		
							    $effectiveEndDateAndTime = $routeDataArr[$i]['EffectiveEndDate'];
							    $effectiveEndDateArr = explode(' ', $effectiveEndDateAndTime);
							    $effectiveEndDate = $effectiveEndDateArr[0];
							    $effectiveEndTimeArr = explode(':', $effectiveEndDateArr[1]);
							    $effectiveEndTimeHour = $effectiveEndTimeArr[0];
							    $effectiveEndTimeMinute = $effectiveEndTimeArr[1];
							    $effectiveEndTimeSecond = $effectiveEndTimeArr[2];
							
								$replySlipType = $routeDataArr[$i]['ReplySlipType'];
								
							    $viewRight = $routeDataArr[$i]['ViewRight'];
							  	$replySlipReleaseType = $routeDataArr[$i]['ReplySlipReleaseType'];
							  	$replySlipID = $routeDataArr[$i]['ReplySlipID'];
							  	$actions = $routeDataArr[$i]['Actions'];
								$enableLock = $routeDataArr[$i]['EnableLock'];
								$lockDuration= $routeDataArr[$i]['LockDuration']; 
								
								$libReplySlip = new libReplySlip($replySlipID);
								$userNameDisplayType = $libReplySlip->getShowUserInfoInResult();
							
								$routeNum = $i + 1;
								$eachRoutingRadioBtn = '';
								$eachRoutingRadioBtn = $this->Get_Radio_Button($ID = $routeNum, $Name = 'selectedRouting', $routeID, $isAllGroupChecked, $Class='selectedRouting', $Display='', $Onclick='', $Disabled='');
										
								$copyRouteOptionTable .='<tr>';
									# Routing No.
									$copyRouteOptionTable .='<td>'. $Lang['DocRouting']['Routing'] . ' ' . $routeNum . '</td>';
									
									# Target User(s)
									$copyRouteOptionTable .='<td >';							
										$numOfTargetUsers = count($targetUserNameArr);
										for($j=0;$j<$numOfTargetUsers;$j++){		 
											$copyRouteOptionTable .= $targetUserNameArr[$j] . '<br />';
										}
									$copyRouteOptionTable .='</td>';
									
									# Routing Status
									if($thisRouteStatus==$docRoutingConfig['routeStatus']['notStarted']){
										$thisRouteStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];
									}elseif($thisRouteStatus == $docRoutingConfig['routeStatus']['inProgress']){
										$thisRouteStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Processing'];
									}elseif($thisRouteStatus == $docRoutingConfig['routeStatus']['completed']){
										$thisRouteStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];
									}
									$copyRouteOptionTable .='<td >' . $thisRouteStatusDisplay .'</td>';
															
									# Option Btn 
									$copyRouteOptionTable .='<td >' . $eachRoutingRadioBtn . '</td>';
							
									# Hidden Field Value for JS
									# Taeget User
									$copyRouteOptionTable .= '<input type="hidden" id="TargetUserIDList_'.$routeID.'" value="'.$targetUserIDArrList.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="TargetUserNameList_'.$routeID.'" value="'.$targetUserNameArrList.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="UserNameDisplayType_'.$routeID.'" value="'.$userNameDisplayType.'">';
										
									# Note
									$copyRouteOptionTable .= '<input type="hidden" id="Note_'.$routeID.'" value="'.$note.'">';
									# Action
									$copyRouteOptionTable .= '<input type="hidden" id="Actions_'.$routeID.'" value="'.$actions.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="ReplySlipID_'.$routeID.'" value="'.$replySlipID.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="ReplySlipType_'.$routeID.'" value="'.$replySlipType.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="ReplySlipReleaseType_'.$routeID.'" value="'.$replySlipReleaseType.'">';
				
									# Effective								
									$copyRouteOptionTable .= '<input type="hidden" id="EffectiveType_'.$routeID.'" value="'.$effectiveType.'">';								
									$copyRouteOptionTable .= '<input type="hidden" id="EffectiveStartDate_'.$routeID.'" value="'.$effectiveStartDate.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="EffectiveStartTimeHour_'.$routeID.'" value="'.$effectiveStartTimeHour.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="EffectiveStartTimeMinute_'.$routeID.'" value="'.$effectiveStartTimeMinute.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="EffectiveStartTimeSecond_'.$routeID.'" value="'.$effectiveStartTimeSecond.'">';								
									$copyRouteOptionTable .= '<input type="hidden" id="EffectiveEndDate_'.$routeID.'" value="'.$effectiveEndDate.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="EffectiveEndTimeHour_'.$routeID.'" value="'.$effectiveEndTimeHour.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="EffectiveEndTimeMinute_'.$routeID.'" value="'.$effectiveEndTimeMinute.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="EffectiveEndTimeSecond_'.$routeID.'" value="'.$effectiveEndTimeSecond.'">';
									
									# View By 
									$copyRouteOptionTable .= '<input type="hidden" id="ViewRight_'.$routeID.'" value="'.$viewRight.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="ExtraTargetUserIDList_'.$routeID.'" value="'.$extraTargetUserIDArrList.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="ExtraTargetUserNameList_'.$routeID.'" value="'.$extraTargetUserNameArrList.'">';
									
									# Feedback Lock
									$copyRouteOptionTable .= '<input type="hidden" id="EnableLock_'.$routeID.'" value="'.$enableLock.'">';
									$copyRouteOptionTable .= '<input type="hidden" id="LockDuration_'.$routeID.'" value="'.$lockDuration.'">';
		
									$copyRouteOptionTable .='</tr>';
								
							}
						
							$copyRouteOptionTable .= '<div id="routeHiddenValueLayer"></div>';

							$copyRouteOptionTable .= '<input type="hidden" id="newRouteType" value="'.$newRouteType.'">';
							$copyRouteOptionTable .= '<input type="hidden" id="RouteNumber" value="'.$routingNumber.'">';
							
							$copyRouteOptionTable .='</tbody>';							
						$copyRouteOptionTable .= '</table>';
				
				$copyRouteOptionTable .= '</td>';
				$copyRouteOptionTable .= '</tr>';
				
				# Action Btns
				$copyRouteOptionTable .= '<tr>';
					$copyRouteOptionTable .= '<td align="center">';
						$copyRouteOptionTable .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Submit'], "submit", "copyback(); return false;","submit2");
						$copyRouteOptionTable .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn");
					$copyRouteOptionTable .= '</td>';
				$copyRouteOptionTable .= '</tr>';
			
			$copyRouteOptionTable .= '</table>';
			
			return $copyRouteOptionTable;
		}
		
		function getAcademicYearSelection($id, $name, $selectedAcademicYear, $displayAll=true, $allValue='-1', $class='')
		{
			global $Lang, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");
			$lfcm = new form_class_manage();
			
			// get school year selection
			$SchoolYearList = $lfcm->Get_Academic_Year_List();
			$academic_year_selection .= '<select name="'.$name.'" id="'.$id.'" '.($class!=''?' class="" ':'').'>';
			$Selected = '';
			if(stripslashes(stripslashes($selectedAcademicYear)) == -1)
				$Selected = 'selected';
			$academic_year_selection .= '<option value="-1" '.$Selected.'>'.$Lang['SysMgr']['FormClassMapping']['All']['AcademicYear'].'</option>';
			for ($i=0; $i< sizeof($SchoolYearList); $i++) {
				$SchoolYearName = Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN']);
				
				//if (!$NoCheckCurrent) {
					if ($SchoolYearList[$i]['CurrentSchoolYear'] == '1') 
						$AcademicYearID = $SchoolYearList[$i]['AcademicYearID'];
				//}
				
				//unset($Selected);
				if(stripslashes(stripslashes($selectedAcademicYear)))
					$Selected = (stripslashes(stripslashes($selectedAcademicYear)) == $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
				else
					$Selected = (Get_Current_Academic_Year_ID() == $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
				$academic_year_selection .= '<option value="'.$SchoolYearList[$i]['AcademicYearID'].'" '.$Selected.'>'.$SchoolYearName.'</option>';
			}
			$academic_year_selection .= '</select>';
			
			return $academic_year_selection;
		}
		
		function getAdvanceSearchDiv($pageType=''){
			global $Lang,$PATH_WRT_ROOT, $docRoutingConfig, $routingTitle, $byDateRange, $StartDate, $EndDate, $byDateRange2, $StartDate2, $EndDate2, $createdBy, $tag, $academic_year;
			/*
			include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");
			$lfcm = new form_class_manage();

			// get school year selection
			$SchoolYearList = $lfcm->Get_Academic_Year_List();
			$academic_year_selection .= '<select name="academic_year" id="academic_year">';
			$Selected = '';
			if(stripslashes(stripslashes($academic_year)) == -1)
				$Selected = 'selected';
			$academic_year_selection .= '<option value="-1" '.$Selected.'>'.$Lang['SysMgr']['FormClassMapping']['All']['AcademicYear'].'</option>';
			for ($i=0; $i< sizeof($SchoolYearList); $i++) {
				$SchoolYearName = Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN']);
				
				//if (!$NoCheckCurrent) {
					if ($SchoolYearList[$i]['CurrentSchoolYear'] == '1') 
						$AcademicYearID = $SchoolYearList[$i]['AcademicYearID'];
				//}
				
				//unset($Selected);
				if(stripslashes(stripslashes($academic_year)))
					$Selected = (stripslashes(stripslashes($academic_year)) == $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
				
				else
				$Selected = (Get_Current_Academic_Year_ID() == $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
				$academic_year_selection .= '<option value="'.$SchoolYearList[$i]['AcademicYearID'].'" '.$Selected.'>'.$SchoolYearName.'</option>';
			}
			$academic_year_selection .= '</select>';
			*/
			$academic_year_selection = $this->getAcademicYearSelection("academic_year", "academic_year", $academic_year);
			
			$searchDiv = '<div style="float:right; padding:3px">';
			//$searchDiv .= '<input type="button" value="' . $Lang['Btn']['Search'] . '" class="formsmallbutton" onclick="jsReloadCurrentRouting(0)"/> | <a href="javascript:void(0)" onclick="DisplayAdvanceSearch();" id="AdvanceSearchText"> ' . $Lang['DocRouting']['Advanced'] . '</a>';
			$searchDiv .= ' | <a href="javascript:void(0)" onclick="DisplayAdvanceSearch();" id="AdvanceSearchText"> ' . $Lang['DocRouting']['Advanced'] . '</a>';
			$searchDiv .= '</div>';
			$searchDiv .= '<div id="DR_search" class="DR_search_advance" style="top:0px;left:0px;display:none;">
			                     <a href="javascript:void(0)" class="DA_search_advance_close" onclick="HiddenAdvanceSearch();" style="float:right">' . $Lang['Btn']['Close'] . '</a>
			                     <p class="spacer"></p>
		
								<div class="resource_search_advance_form">
									<div class="table_board">
		
										<table class="form_table">

											<tr>
				                             <td class="field_title">'.$Lang['DocRouting']['TargetType']['Title'].'</td>
				                             <td><label>
				                               <input name="routingTitle" id="routingTitle" type="text" class="keywords" style="" value="' . stripslashes(stripslashes($routingTitle)) . '" />
				                             </label></td>
				                           </tr>
		
											<tr>
												<td class="field_title">'.$Lang['DocRouting']['StartDate'].'</td>
												<td>
													<input type="checkbox" name="byDateRange" id="byDateRange" '.(stripslashes(stripslashes($byDateRange))=="true"?"checked":"").' /> 
													' . $this->GET_DATE_PICKER("StartDate", stripslashes(stripslashes($StartDate))) . ' ' . $Lang['General']['To'] . ' ' . $this->GET_DATE_PICKER("EndDate", stripslashes(stripslashes($EndDate))) . '
												</td>
											</tr>

											<tr>
												<td class="field_title">'.$Lang['DocRouting']['EndDate'].'</td>
												<td>
													<input type="checkbox" name="byDateRange2" id="byDateRange2" '.(stripslashes(stripslashes($byDateRange2))=="true"?"checked":"").' /> 
													' . $this->GET_DATE_PICKER("StartDate2", stripslashes(stripslashes($StartDate2))) . ' ' . $Lang['General']['To'] . ' ' . $this->GET_DATE_PICKER("EndDate2", stripslashes(stripslashes($EndDate2))) . '
												</td>
											</tr>

											<tr>
												<td class="field_title">'.$Lang['DocRouting']['Creator'].'</td>
												<td>
													<input type="text" class="tabletext" name="createdBy" ID="createdBy" value="' . stripslashes(stripslashes($createdBy)) . '">
												</td>
											</tr>

											<tr>
												<td class="field_title">'.$Lang['DocRouting']['Tags'].'</td>
												<td>
													<input type="text" class="tabletext" name="tag" ID="tag" value="' . stripslashes(stripslashes($tag)) . '">								
												</td>
											</tr>';
					if(!in_array($pageType,array($docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings'],$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DeletedRountings'])))
					{		
							$searchDiv .= '<tr>
												<td class="field_title">'.$Lang['General']['AcademicYear'].'</td>
												<td>
													'.$academic_year_selection.'
												</td>
											</tr>';
					}		
						$searchDiv .= '</table>
									</div>
									<div class="edit_bottom">
											' . $this->GET_ACTION_BTN($Lang['Btn']['Search'], "button", "jsReloadCurrentRouting(1)") . '
											' . $this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "HiddenAdvanceSearch()") . '
									</div>
								</div>
							</div>';
			return $searchDiv;
		}
		
		function getStarredFilesDiv($DocumentId)
		{
			global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $xmsg, $indexVar, $docRoutingConfig;
			
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			$ldamu = $indexVar['libdigitalarchive_moduleupload'];
			$canArchiveFile = $ldamu->User_Can_Archive_File();
			
			$attachments = $ldocrouting->getUserStarredFiles($DocumentId);
			$numOfAttachment = count($attachments);
			$feedbackIdAry = array();
			
			$tok = $indexVar['tok'];
			$param_tok = $tok !=''? '&tok='.$tok : '';
			
			$x .= '<div class="dr_intru_file" id="StarFileDiv">
						<div class="dr_intru_detail">'.$Lang['DocRouting']['StarredFile'].'</div>';
					$x .= '<div class="dr_file_detail">';
						$x .= Get_Plural_Display($numOfAttachment,$Lang['DocRouting']['Attachment']);
						
						$x .= '<ul>';
						for($i=0;$i<$numOfAttachment;$i++){
							$attachment_path = $attachments[$i]['FilePath'];
							$attachment_name = $attachments[$i]['FileName'];
							$attachment_id = $attachments[$i]['FileID'];
							$link_to_id = $attachments[$i]['LinkToID'];
							$feedbackIdAry[] = $link_to_id;
							
							$file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'],'LinkToID'=>$link_to_id,'FileID'=>$attachment_id));
							$file_icon = $this->Get_File_Type_Icon(get_file_ext($attachment_name));
							
							$x .= '<li>
										<a href="index.php?pe='.$file_param.$param_tok.'" target="_blank">'.$file_icon.$attachment_name.'</a>
									</li>';
						}
						if($numOfAttachment > 0 && $canArchiveFile && $tok == '') {
							$x .= '<li><a href="javascript:void(0);" onclick="LoadArchiveTBFormForStarFile(['.implode(',',$feedbackIdAry).'],\'feedback\');" title="'.$Lang['DocRouting']['ArchiveFilesToDigitalArchive'].'">[ '.$Lang['DigitalArchiveModuleUpload']['Archive'].' ]</a></li>';
						}
						$x .= '</ul>';
						$x .= '<p class="spacer"></p>
							</div>';
			
			$x .= '</div>';
			
			return $x;
		}
		//Henry Added [20140528]
		function getEditRoutingPeriodAndSendEmailForm($DocumentID)
		{
			global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $iPowerVoice, $intranet_root, $indexVar, $docRoutingConfig;
			
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			
			$indexVar['FromAddAdHocRouting'] = 1;
			
			$entry = $ldocrouting->getEntryData($DocumentID);
			$routes = $ldocrouting->getRouteData($DocumentID);
//			debug_pr($entry);
//			debug_pr($routes);
			$dataAry = $entry[0];
			$routeDataAry = $routes[0];
			//$next_display_order = $routes[count($routes)-1]['DisplayOrder'] + 1;
			
			$title = $dataAry['Title'];
			//$instruction = $dataAry['Instruction'];
			//$documentType = $dataAry['DocumentType'];
			//$docTags = $dataAry['DocTags'];
			//$allowAdHocRoute = $dataAry['AllowAdHocRoute'];
			//$recordStatus = $dataAry['RecordStatus'];
			
			$effectiveType = $routeDataAry['EffectiveType'];
			if($effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){
				$effectiveStartDate = substr($routeDataAry['EffectiveStartDate'],0,10);
				$effectiveEndDate = substr($routeDataAry['EffectiveEndDate'],0,10);
				$effectiveStartTime = substr($routeDataAry['EffectiveStartDate'],10);
				$effectiveEndTime = substr($routeDataAry['EffectiveEndDate'],10);
			}else{
				$effectiveStartDate = date("Y-m-d");
				//$effectiveStartTime = "00:00:00";
				$effectiveStartTime = date("H:i:s");
				$effectiveEndDate = date("Y-m-d",time() + 86400*7);
				$effectiveEndTime = "23:59:59";
			}
			$routingNumber = 1;
			
			$form_action = $ldocrouting->getEncryptedParameter('management','edit_routing_period_send_email_update',array());
					
			$x = '<form name="form1" method="post" action="index.php?pe='.$form_action.'" enctype="multipart/form-data">';
			$x.= '<div id="TBEditDiv" style="height:390px; overflow-x:hidden; overflow-y:auto;">';
			$x.= '<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		        	<tr>
		        		<td class="main_content">
		        			<div class="table_board">
		        				<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
						            <tr>
										<td bgcolor="#FFEECC" colspan="2"><b>'.$title.'</b></td>
						            </tr>
						        ';
			
						
						//The Routing Period Settings [Start]
						$x .= '<tr>';
						$x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['Visible']. '</td>';
						$x .= '<td>';
						$x .= $this->Get_Radio_Button('EffectiveTypePeriod_'.$routingNumber, "EffectiveType_$routingNumber", $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period'], $effectiveType==$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period'], "CopyClass_{$routingNumber}", $Lang['DocRouting']['Period'], 'js_Document_Show('.$routingNumber.');');
						$x .= '<br />';
						$x .= '<div id="VisiblePeriod_'.$routingNumber.'" '.($effectiveType==$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']?'':'style="display:none;"').'>';
						
						$x .= $this->GET_DATE_PICKER("EffectiveStartDate_$routingNumber", $effectiveStartDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="CopyClass_{$routingNumber}"). '&nbsp;';
						$x .= $this->Get_Time_Selection("EffectiveStartTime_$routingNumber", $effectiveStartTime, "class='CopyClass_{$routingNumber}'", '' , array(1,1,1));
						$x .= '-&nbsp;'.$this->GET_DATE_PICKER("EffectiveEndDate_$routingNumber", $effectiveEndDate, $OtherMember="", $DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="CopyClass_{$routingNumber}").'&nbsp;'.$this->Get_Time_Selection("EffectiveEndTime_$routingNumber", $effectiveEndTime, "class='CopyClass_{$routingNumber}'", '', array(1,1,1));
						$x .= $this->Get_Thickbox_Warning_Msg_Div("EffectiveTypeWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange'], "WarnMsgDiv");
						$x .= '<br />';
						$x .= '</div>';
						$x .= '<div class="EffectiveByPreRouteDiv">';
						$x .= $this->Get_Radio_Button('EffectiveTypePrevRouteFinished_'.$routingNumber, "EffectiveType_$routingNumber", $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished'], $effectiveType==$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished'] || $effectiveType=='', "CopyClass_{$routingNumber}", '<span id="optionFinishedSpan_'.$routingNumber.'">'.$Lang['DocRouting']['StartRouteNow'].'</span>', 'js_Document_Hide('.$routingNumber.');');
						$x .= '</div>';
						
						$x .= $this->Get_Thickbox_Warning_Msg_Div("EffectiveTypePrevRouteWarningDiv_".$routingNumber,$Lang['DocRouting']['WarningMsg']['PleaseSelectEffectiveTypeByPeriod'], "WarnMsgDiv");
						
						
						$x .= '</td>';
						$x .= '</tr>';
						//The Routing Period Settings [End]
						
						//The Send Email Settings [Start]
						$x .= '<tr>';
						$x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['SendReminder'] . '</td>';
						$x .= '<td>';
						$x .= $this->Get_Radio_Button('SendEmailYes', "SendEmail", "Y", 1, "", $Lang['General']['Yes'], '$(\'#EmailTargetInProgress\').attr(\'disabled\',false);$(\'#EmailTargetNotViewedYet\').attr(\'disabled\',false);$(\'#EmailContent\').attr(\'disabled\',false);');
						$x .= '&nbsp;';
						$x .= $this->Get_Radio_Button('SendEmailNo', "SendEmail", "N", 0, "", $Lang['General']['No'], '$(\'#EmailTargetInProgress\').attr(\'disabled\',true);$(\'#EmailTargetNotViewedYet\').attr(\'disabled\',true);$(\'#EmailContent\').attr(\'disabled\',true);');
						
						$x .= '</td>';
						$x .= '</tr>';
						
						$x .= '<tr>';
						$x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['EmailTarget'] . '</td>';
						$x .= '<td>';
						
						$x .= $this->Get_Checkbox('EmailTargetInProgress', 'EmailTarget[]', $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'], '1', "", $Lang['DocRouting']['InProgress']);
						$x .= '&nbsp;';
						$x .= $this->Get_Checkbox('EmailTargetNotViewedYet', 'EmailTarget[]', $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'], '1', "", $Lang['DocRouting']['NotViewedYet']);
						
						$x .= '</td>';
						$x .= '</tr>';
						
						$x .= '<tr>';
						$x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['DocRouting']['EmailContent'] . '</td>';
						$x .= '<td>';
						
						$x .= $this->GET_TEXTAREA('EmailContent', $ldocrouting->applyDocumentAndRouteDataInText(implode("\r\n", $Lang['DocRouting']['EmailNotification_newRouteReceived']['EmailContentAry']), $title), $taCols=70, $taRows=5, $OnFocus = "", $readonly = "", $other='', $class='', 'EmailContent', $CommentMaxLength='');
						
						$x .= '</td>';
						$x .= '</tr>';
						
						//The Send Email Settings [End]
						$x .= '</table>';
						$x .= '<p class="spacer"></p>
								</div>
							</td>
						</tr>
			        </table>
					<input type="hidden" id="DocumentID" name="DocumentID" value="'.$DocumentID.'" />';
			$x .= '<input type="hidden" id="RouteID" name="RouteID" value="'.$routeDataAry['RouteID'].'">';
				$x .= '</td>
					</tr>
					</table>
				</div>	
				</form>';
			$x .= '<iframe id="UpdateFrame" name="UpdateFrame" style="display:none;"></iframe>'."\n";
			$x .= $this->MandatoryField();
			$x .= '<div class="edit_bottom">
			            <p class="spacer"></p>
			            '.$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Submit_Edit_Routing_Period_Send_Email(".$DocumentID.");").'&nbsp;
			            '.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.tb_remove();").'
						<p class="spacer"></p>
			        </div>';	
			
			return $x;
		}
		//[Henry added 20140704]
		function getRoutingPrintTable($newRoutingLink, $DisplayUpdateLink,$searchBox,$instructionMessage, $newBtn='', $pageType='', $expireStatus=''){
			global $indexVar, $Lang, $docRoutingConfig,$button_print;
			global $ck_doc_management_current_rountings_star_staus, $ck_doc_management_current_rountings_create_status, $ck_doc_management_current_rountings_follow_status;			
			global $ck_doc_management_current_routings_expire_status;
				
			$libuser = new libuser();
						
			# Check User's Access Right						
			if($indexVar['drUserIsAdmin']){
				$involvedOnly = false;
			}
			else{
				$involvedOnly = true;
			}
						
			$htmlAry['CurrentDisplay'] ='';
			$htmlAry['CurrentDisplay'] = '<form name="form1" method="post" action="'. '?pe='.$DisplayUpdateLink.'">';
				$htmlAry['CurrentDisplay'] .= '<table width="99%" border="0" cellspacing="0" cellpadding="0">';	
					$htmlAry['CurrentDisplay'] .= '<tr>';
						$htmlAry['CurrentDisplay'] .= '<td class="main_content">';
						//if($newBtn == true){
							$htmlAry['CurrentDisplay'] .= '<div class="content_top_tool" style="display:none">';
								$htmlAry['CurrentDisplay'] .= '<div class="Conntent_tool">';
									if ($newBtn) {
										$htmlAry['CurrentDisplay'] .= '<a class="new" href="'. '?pe='. $newRoutingLink.'">' . $Lang['Btn']['New']. '</a>';
									}
									
									# Add Copy Routing Link
									$copyRoutingLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'copy_doc', array('pageType' => $pageType));							
									$htmlAry['CurrentDisplay'] .= '<a class="copy" href="'. '?pe='. $copyRoutingLink.'">' . $Lang['DocRouting']['Button']['CopyExistingRounting']. '</a>';
									
									# Add Print Routing Link (Henry Added 20140704)
									$printRoutingLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'print_doc', array('pageType' => $pageType));							
									$htmlAry['CurrentDisplay'] .= '<a class="print" target = "_blank" href="'. '?pe='. $printRoutingLink.'">' .$Lang['Btn']['Print']. '</a>';
									
								$htmlAry['CurrentDisplay'] .= '</div>';		
						//}
							$htmlAry['CurrentDisplay'] .= $searchBox;
							$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p>';
							$htmlAry['CurrentDisplay'] .= '</div>';		
//							$htmlAry['CurrentDisplay'] .= $instructionMessage;
							
							$htmlAry['CurrentDisplay'] .= '<div class="table_board" style="display:none">';
								$htmlAry['CurrentDisplay'] .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">';
									$htmlAry['CurrentDisplay'] .= '<tbody><tr>';
										$htmlAry['CurrentDisplay'] .= '<td valign="bottom">';
																				
										$starParSelected ='';
										
										if(isset($ck_doc_management_current_rountings_star_staus) && $ck_doc_management_current_rountings_star_staus!=''){
											$starParSelected = $ck_doc_management_current_rountings_star_staus;
										}
					
										# Star Filter
										$starParData []= array('', $Lang['DocRouting']['Management']['FilterStatus']['AllStarStatus'] );
										$starParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['StarRemarked'], $Lang['DocRouting']['Management']['FilterStatus']['StarRemarked'] );
										$starParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotStarRemarked'],$Lang['DocRouting']['Management']['FilterStatus']['NotStarRemarked']);										
										$starParTags = 'name ="starStatus" class="starStatus" id="starStatus" onchange="jsReloadCurrentRouting(0)"';
										
										
										//$starParDefault	= $Lang['DocRouting']['Management']['FilterStatus']['AllStarStatus'];
													
										$htmlAry['CurrentDisplay'] .= 	$this->GET_SELECTION_BOX($starParData, $starParTags, $starParDefault, $starParSelected);
										
										if($pageType!=$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']){
										# Create Filter 
											$createParDefault ='';
											$ParSelected =''; 
											if($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings']){
																						
												$createParData []= array('',$Lang['DocRouting']['Management']['FilterStatus']['AllCreateStatus']);	
											}
											else{
												$createParDefault = $Lang['DocRouting']['Management']['FilterStatus']['AllCreateStatus'];	
											}
											
											$ParSelected = $ck_doc_management_current_rountings_create_status;
											
											$createParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['CreatedByYou'],$Lang['DocRouting']['Management']['FilterStatus']['CreatedByYou'] );
											$createParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotCreatedByYou'],$Lang['DocRouting']['Management']['FilterStatus']['NotCreatedByYou']);										
											$createParTags = 'name ="createStatus" class="createStatus" id="createStatus"';
													
											
											$htmlAry['CurrentDisplay'] .= $this->GET_SELECTION_BOX($createParData, $createParTags, $createParDefault, $ParSelected);
											
											# Follow-up Filter 
											if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings']){
												$followParData []= array('',$Lang['DocRouting']['Management']['FilterStatus']['AllFollowUpStatus'] );	
												$followParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['FollowedByYouNow'],$Lang['DocRouting']['Management']['FilterStatus']['FollowedByYouNow'] );
												$followParData []= array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotFollowedByYouNow'],$Lang['DocRouting']['Management']['FilterStatus']['NotFollowedByYouNow']);																																
												$followParTags = 'name ="followStatus" class="followStatus" id="followStatus"';
												$followParDefault='';
												$ParSelected ='';
														
												if(!isset($ck_doc_management_current_rountings_follow_status)){
													$ParSelected = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['FollowedByYouNow'];
												}
												else{						
													$ParSelected = $ck_doc_management_current_rountings_follow_status;										
												}
												$htmlAry['CurrentDisplay'] .= 	$this->GET_SELECTION_BOX($followParData, $followParTags, $followParDefault, $ParSelected);																															
											
												# Expire Filter
												$expireParData[] = array('',$Lang['DocRouting']['Management']['FilterStatus']['AllExpireStatus']);
												$expireParData[] = array($docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['ExpiredNotFinished'],$Lang['DocRouting']['Management']['FilterStatus']['ExpiredButNotYetFinished']);																															
												$expireParTags = ' name="expireStatus" class="expireStatus" id="expireStatus" ';
												$expireParDefault = '';
												$expireParSelected = '';
											
												if(!isset($ck_doc_management_current_routings_expire_status)){
													$expireParSelected = '';
												}else{
													$expireParSelected = $ck_doc_management_current_routings_expire_status;
												}
												
												$htmlAry['CurrentDisplay'] .= 	$this->GET_SELECTION_BOX($expireParData, $expireParTags, $expireParDefault, $expireParSelected);
											}
										}
																												
										$htmlAry['CurrentDisplay'] .= '</td>';																				
										$htmlAry['CurrentDisplay'] .= '<td valign="bottom"></td>';
									$htmlAry['CurrentDisplay'] .= '</tr>';
								$htmlAry['CurrentDisplay'] .= '</tbody></table></div>';
							
							
							###   Display Items By Ajax     ###
							$htmlAry['CurrentDisplay'] .= '<table width="98%" border="0" cellspacing="0" cellpadding="5" valign="top">';
							$htmlAry['CurrentDisplay'] .= '<tr>';
							$htmlAry['CurrentDisplay'] .= '<td align="right" class="print_hide">';
							$htmlAry['CurrentDisplay'] .= $this->GET_BTN($button_print, "button","javascript:window.print()");
							$htmlAry['CurrentDisplay'] .= '</td>';
							$htmlAry['CurrentDisplay'] .= '</tr>';
							$htmlAry['CurrentDisplay'] .= '</table>';		
							$htmlAry['CurrentDisplay'] .='<div id ="currentRoutingItemDisplayReload" class="currentRoutingItemDisplayReload" >';															
							$htmlAry['CurrentDisplay'] .= '</div>';
							### End Of Display Item By Ajax ###
							
							$htmlAry['CurrentDisplay'] .= '<input type="hidden" id="selectedTag" name="selectedTag"/>';
						
						$htmlAry['CurrentDisplay'] .= '</td>';
					$htmlAry['CurrentDisplay'] .= '</tr>';
				$htmlAry['CurrentDisplay'] .= '</table>';
			$htmlAry['CurrentDisplay'] .= '</form>';
		
			return $htmlAry['CurrentDisplay'];
		}
		
		function getCurrentRoutingPrintItem($status, $keyword, $accessRight, $starStatus,$createStatus, $followStatus=0, $pageType='', $selectedTag='', $expireStatus='', $advanceSearchArray='', $selectedAcademicYear=''){
			global $Lang, $PATH_WRT_ROOT, $xmsg, $indexVar, $docRoutingConfig, $sys_custom;
			$libuser = new libuser();
			
			
			# Check User's Access Right						
			if($indexVar['drUserIsAdmin']){
				$involvedOnly = false;
				$inputBy = '';
			}
			else{
				$involvedOnly = true;
				
				if ($pageType == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']) {
					$inputBy = $indexVar['drUserId'];
				}
			}
				
		
			
			# Get User DR Entry Data
			$userDocumentInfoAry = $indexVar['libDocRouting']->getUserDREntry($indexVar['drUserId'], $involvedOnly, $status, $keyword, $starStatus, $createStatus, $selectedTag, $inputBy, $expireStatus, $advanceSearchArray, $selectedAcademicYear);	
			
			//debug_pr($userDocumentInfoAry);
			//debug_pr($documentInfoAry);
				
//			//$documentIdStarArr='';	
//			for($i=0;$i<count($documentInfoAry);$i++){			
//				$documentIdArrUnique []= $documentInfoAry[$i]['DocumentID'];					
//				//$documentIdStarArr [] = $documentInfoAry[$i]['StarSequence'];
//			}	
			$documentIdArrUnique = Get_Array_By_Key($userDocumentInfoAry, 'DocumentID');
			//debug_pr($documentIdArrUnique);
			
//			if($documentIdArrUnique){		
//				$documentIdArrUnique = array_unique($documentIdArrUnique);
//				foreach ($documentIdArrUnique as $documentIdArrKey => $documentIdArrValue){
//					$documentIdArrUnique[] = $documentIdArrValue;					
//				}
//			}	

			# Get Route Entry Data 
			$documentDataAry = $indexVar['libDocRouting']->getEntryData($documentIdArrUnique);
			$documentDataAssoAry = BuildMultiKeyAssoc($documentDataAry, 'DocumentID');
			
   			# Get Route Data
			$routeDataAry = $indexVar['libDocRouting']->getRouteData($documentIdArrUnique);
			$routeDataAssoAry = BuildMultiKeyAssoc($routeDataAry, 'DocumentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);		
																			
//			# Get File Data 																	
//			$fileDataAry = $indexVar['libDocRouting']->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $documentIdArrUnique);
//			$fileDataAssoAry = BuildMultiKeyAssoc($fileDataAry, 'LinkToID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
//			
//			# Get Star Sequence 
//			$routeStarArr = $indexVar['libDocRouting']->getStarStatus($indexVar['drUserId']);
//			$routeStarAssoAry = BuildMultiKeyAssoc($routeStarArr, 'DocumentID');
//   								
   			# Get Current User Responsible Routing  				
   			$currentUserResponsibleRoutingAssoAry = $indexVar['libDocRouting']->getUserCurrentInvolvedEntry($indexVar['drUserId']);
   			$currentUserResponsibleDocumentIdArr = array_keys($currentUserResponsibleRoutingAssoAry);
   			
   			# Get Route Complete Status
   			$routeCompleteStatusAry = $indexVar['libDocRouting']->getRouteCompleteStatus($documentIdArrUnique);
   			
   			
   			$allRouteIdAry = Get_Array_By_Key($routeDataAry, 'RouteID');

   			# Get Route Target Users in batch
   			$allRouteTargetUsers = $indexVar['libDocRouting']->getRouteTargetUsers($allRouteIdAry);
   			$allRouteTargetUsersSize = count($allRouteTargetUsers);
   			$routeIdToRouteTargetUsersAry = array();
   			for($n=0;$n<$allRouteTargetUsersSize;$n++){
   				if(!isset($routeIdToRouteTargetUsersAry[$allRouteTargetUsers[$n]['RouteID']])){
   					$routeIdToRouteTargetUsersAry[$allRouteTargetUsers[$n]['RouteID']] = array();
   				}
   				$routeIdToRouteTargetUsersAry[$allRouteTargetUsers[$n]['RouteID']][] = $allRouteTargetUsers[$n];
   			}
   			
   			# Get route feedbacks in batch
   			$allRouteUserFeedbacks = $indexVar['libDocRouting']->getRouteUserFeedbacks($allRouteIdAry);
   			$allRouteUserFeedbacksSize = count($allRouteUserFeedbacks);
   			$routeIdToRouteUserFeedbacksAry = array();
   			for($n=0;$n<$allRouteUserFeedbacksSize;$n++){
   				if(!isset($routeIdToRouteUserFeedbacksAry[$allRouteUserFeedbacks[$n]['RouteID']])){
   					$routeIdToRouteUserFeedbacksAry[$allRouteUserFeedbacks[$n]['RouteID']] = array();
   				}
   				$routeIdToRouteUserFeedbacksAry[$allRouteUserFeedbacks[$n]['RouteID']][] = $allRouteUserFeedbacks[$n];
   			}
   			
   			
   			# Find out route targets to check sign status for followed and signed routings but not completed yet. And find current user last followed time by either signed or give feedback.
   			if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
				//$allRouteIdAry = Get_Array_By_Key($routeDataAry, 'RouteID');
				$allRouteTargetRecordAry = $indexVar['libDocRouting']->getRouteTargetUsers($allRouteIdAry, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']), array($indexVar['drUserId']));
				$docIdToRouteTargetAry = array();
				$docIdToLastFollowTimeForCurrentUser = array();
				for($n=0;$n<count($allRouteTargetRecordAry);$n++){
					if(!isset($docIdToRouteTargetAry[$allRouteTargetRecordAry[$n]['DocumentID']])){
						$docIdToRouteTargetAry[$allRouteTargetRecordAry[$n]['DocumentID']] = array();
					}
					$docIdToRouteTargetAry[$allRouteTargetRecordAry[$n]['DocumentID']][] = $allRouteTargetRecordAry[$n];
					if($allRouteTargetRecordAry[$n]['DateModified'] != ''){
						$docIdToLastFollowTimeForCurrentUser[$allRouteTargetRecordAry[$n]['DocumentID']] = $allRouteTargetRecordAry[$n]['DateModified'];
					}
				}
				$allLastFeedbacksForCurrentUser = $indexVar['libDocRouting']->getRouteUserFeedbacks($allRouteIdAry, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']), array($indexVar['drUserId']));
				for($n=0;$n<count($allLastFeedbacksForCurrentUser);$n++){
					if(!isset($docIdToLastFollowTimeForCurrentUser[$allLastFeedbacksForCurrentUser[$n]['DocumentID']])){
						$docIdToLastFollowTimeForCurrentUser[$allLastFeedbacksForCurrentUser[$n]['DocumentID']] = $allLastFeedbacksForCurrentUser[$n]['FeedbackModifyDate'];
					}else if($allLastFeedbacksForCurrentUser[$n]['FeedbackModifyDate'] > $docIdToLastFollowTimeForCurrentUser[$allLastFeedbacksForCurrentUser[$n]['DocumentID']]){
						$docIdToLastFollowTimeForCurrentUser[$allLastFeedbacksForCurrentUser[$n]['DocumentID']] = $allLastFeedbacksForCurrentUser[$n]['FeedbackModifyDate'];
					}
				}
			}
   			
			#####    Routing Display Item    ####
			$htmlAry['CurrentDisplay'] .= '<div class="dr_list">';
			$htmlAry['CurrentDisplay'] .= '<ul>';
																			
			#### Loop Each User Entry ####										
			$numOfDocument = count($documentDataAssoAry);
			
			//debug_pr($documentDataAssoAry);
			//debug_pr($routeDataAssoAry);
			//debug_pr($fileDataAssoAry);
			//debug_pr($routeStarAssoAry);
			//debug_pr($currentUserResponsibleDocumentIdArr);
			
			if($numOfDocument>0){
//				if($selectedTag>0){	
//					$__tagNameArr = returnModuleTagNameByTagID($selectedTag);
//					$__tagName = $__tagNameArr[0];	
//					$htmlAry['CurrentDisplay'] .= '<table class="form_table_v30">';	
//					$htmlAry['CurrentDisplay'] .= '<tr>';
//					$htmlAry['CurrentDisplay'] .= '<td class="field_title">';
//					$htmlAry['CurrentDisplay'] .= $Lang['eDiscipline']['TagName'];										
//					$htmlAry['CurrentDisplay'] .= '</td>';	
//					$htmlAry['CurrentDisplay'] .= '<td>';
//					$htmlAry['CurrentDisplay'] .= $__tagName;
//					$htmlAry['CurrentDisplay'] .= '</td>';
//					$htmlAry['CurrentDisplay'] .= '</tr>';
//					$htmlAry['CurrentDisplay'] .= '</table>';
//					$htmlAry['CurrentDisplay'] .= '<br />';
//				}	
				
				
				# Get all tags in batch
				$allTagAry = $indexVar['libDocRouting']->getEntryTag($documentIdArrUnique);	
				$allTagArySize = count($allTagAry);
				$documentIdToTagAry = array();
				for($i=0;$i<$allTagArySize;$i++){
					if(!isset($documentIdToTagAry[$allTagAry[$i]['DocumentID']])){
						$documentIdToTagAry[$allTagAry[$i]['DocumentID']] = array();
					}
					$documentIdToTagAry[$allTagAry[$i]['DocumentID']][] = $allTagAry[$i];
				}
				
				$tagIdToTagNamesAry = array();
				if($allTagArySize > 0){
					$allTagIds = Get_Array_By_Key($allTagAry,'TagID');
					$tagIdToTagNamesAry = returnTagIDToModuleTagNameByTagID(implode(',',$allTagIds));
				}
						
				
				$numOfItemAlreadyDisplay=0;		
				for ($i=0; $i<$numOfDocument; $i++) {
					$documentStatus = '';	
					$userIconControlCount = 0;
					$arrowDisplayCout = 0  ;	
					$generalFinishedItem = 0;
					$generalFinishedItemCounter = 0;	
														
					$_documentId = $documentIdArrUnique[$i];													
					$_documentDataAry = $documentDataAssoAry[$_documentId];
					$_documentType = $_documentDataAry['DocumentType'];
					$_physicalLocation = $_documentDataAry['PhysicalLocation'];
					$_documentCreatorID = $_documentDataAry['UserID'];
					$_documentTitle = $_documentDataAry['Title'];	//useful
					$_documentDateInput = $_documentDataAry['DateInput'];//useful		
					$_documentRecordStatus = $_documentDataAry['RecordStatus'];//useful
										
					# Num Of Route related to this Entry
					$_documentRouteAry = $routeDataAssoAry[$_documentId];									
					$_numOfRoute = count($_documentRouteAry);
	
					# Num Of Files Attached
					$_documentFileAry = $fileDataAssoAry[$_documentId];
					$_numOfFile = count($_documentFileAry);
	
					# Get Tag Inform 							
					//$_tagArr = $indexVar['libDocRouting']-> getEntryTag($_documentId);
					$_tagArr = array();
					if(isset($documentIdToTagAry[$_documentId])){
						$_tagArr = $documentIdToTagAry[$_documentId];
					}
					
					# User Name Array
					$_documentCreatorName = $_documentDataAry['CreatorName'];
					
					## Star Icon CSS Display Control ###
					$_routeStar = $routeStarAssoAry[$_documentId]['StarSequence'];			
					$starDisplayCss='';					
					
					if($_routeStar == $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['StarRemarked']){
						$starDisplayCss = 'btn_set_important_on';
					}
					else{
						$starDisplayCss = 'btn_set_important';
						$_routeStar = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotStarRemarked'];
					}
					### Star CSS Display Control ###
										
					### Display Box Color Css Control & Assign recordEntryStatus Value for Filter Checking ###								
					if(in_array($_documentId, $currentUserResponsibleDocumentIdArr)){
						$documentStatus = $Lang['DocRouting']['StatusDisplay']['Waiting'];
						$statusStyle = 'waiting';	
						$recordEntryStatus = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['FollowedByYouNow'];
					}
					else if ($_documentRecordStatus != $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']) {
						$_allUserCompleted = true;
						for ($j=0; $j<$_numOfRoute; $j++){
							$__routeId = $_documentRouteAry[$j]['RouteID'];
							//$__targetRouteUserArr = $indexVar['libDocRouting']->getRouteTargetUsers(array($__routeId));
							$__targetRouteUserArr = array();
							if(isset($routeIdToRouteTargetUsersAry[$__routeId])){
								$__targetRouteUserArr = $routeIdToRouteTargetUsersAry[$__routeId];
							}
							$__numOftargetRouteUsers = count($__targetRouteUserArr);
							//debug_pr($__targetRouteUserArr);
							
							for($k=0;$k<$__numOftargetRouteUsers;$k++){
								$___targetRouteUserID = $__targetRouteUserArr[$k]['UserID'];						
								$___targetRouteUserRecordStatus = $__targetRouteUserArr[$k]['RecordStatus'];
								$___targetRouteUserAccessRight = $__targetRouteUserArr[$k]['AccessRight'];
								
								if ($___targetRouteUserAccessRight == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route'] && $___targetRouteUserRecordStatus != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']) {
									$_allUserCompleted = false;
									break;
								}
							}
						}
						
						$_isAfterDeadline = false;
						$_lastRouteType = $_documentRouteAry[$_numOfRoute-1]['EffectiveType'];
						if ($_lastRouteType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']) {
							$_lastRouteDeadline = $_documentRouteAry[$_numOfRoute-1]['EffectiveEndDate'];
							if (time() > strtotime($_lastRouteDeadline)) {
								$_isAfterDeadline = true;
							}
						}
						
						//2014-0220-1638-49184
						if ($_allUserCompleted) {
							$statusStyle = 'processing';
						}
						else if ($_isAfterDeadline) {
							$statusStyle = 'after_deadline';
						}
						else {
							$statusStyle = 'waiting_reply';
						}
						$documentStatus = $Lang['DocRouting']['StatusDisplay']['Processing'];
						$recordEntryStatus = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotFollowedByYouNow'];	
					}
					elseif($_documentRecordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']){
						$documentStatus = $Lang['DocRouting']['StatusDisplay']['Completed'];
						$statusStyle ='';
						$recordEntryStatus = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotFollowedByYouNow'];	
					}else{
						$statusStyle ='';
						$documentStatus = $Lang['DocRouting']['StatusDisplay']['Processing'];
						$recordEntryStatus = $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotFollowedByYouNow'];
					}
							
					### End of Display Box Color Css Control ###
					
					### Filter controls Display Items ###	
					$displayItem = true;
					
					if ($followStatus != '') {
						### Show all signed DR Entry for Creator
						if($recordEntryStatus != $followStatus){		
							
							## Check if all target user signed/confirmed 					
							if(trim($_documentDataAry['InputBy'])==trim($indexVar['drUserId'])){
								// 2014-0416-1055-26177: show routing even if not all user signed if the routing is created by this user
//								$thisTargetUserCompleteStatus = $routeCompleteStatusAry[$_documentId];
//								$noOfNotAllSign = 0;
//								foreach ((array)$thisTargetUserCompleteStatus as $eachTargetUserID => $eachTargetUserCompleteStatus){
//									if($eachTargetUserCompleteStatus!=3){
//										$noOfNotAllSign++;
//									}
//								}
//								if($noOfNotAllSign!=0){									
//									$displayItem = false;
//								}
							}else{
								$displayItem = false;	
							}							
						}
					}
					
					if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
						$routeTargetRecordAry = $docIdToRouteTargetAry[$_documentId];
						for($n=0;$n<count($routeTargetRecordAry);$n++){
							if($routeTargetRecordAry[$n]['RecordStatus'] != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']){
								$displayItem = false;
							}
						}
						if(count($routeTargetRecordAry)==0){
							$displayItem = false;
						}
					}
					
					if($displayItem){
					
						$numOfItemAlreadyDisplay ++;
						# Detail Page Link
						//$detailLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'dr_detail', array('documentId' => $_documentId));	
						
						#Onlty
						/*if($indexVar['drUserIsAdmin'] || $_documentCreatorID == $indexVar['drUserId']){
							$editLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'edit_doc', array('DocumentID' => $_documentId, 'pageType' => $pageType));	
							$deleteLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'delete_doc_update', array('documentId' => $_documentId, 'pageType' => $pageType));	
						}*/		
						
				
						# todo [start] -----------------------------------
						# todo [end] -------------------------------------				
						$htmlAry['CurrentDisplay'] .= '<li class='.$statusStyle.'>';
							
														
							$htmlAry['CurrentDisplay'] .= '<div class="dr_list_header">';
								//$htmlAry['CurrentDisplay'] .= '<a class="'.$starDisplayCss.'" href="javaScript:void(0)" onclick="js_Update_Star_Status('.$_documentId.', '.$_routeStar.', '.$indexVar['drUserId'].')"></a>';
								$htmlAry['CurrentDisplay'] .= '<h2>'.intranet_htmlspecialchars($_documentTitle).'</h2>';
								if($sys_custom['DocRouting']['RequestNumber']){
									$htmlAry['CurrentDisplay'] .= '<h3> '.$Lang['DocRouting']['RequestNumber'].': '.$_documentId.'</h3>';
								}
								//$htmlAry['CurrentDisplay'] .= ' <span class="member_pic_name" title="'.$Lang['DocRouting']['Creator'].'">'. intranet_htmlspecialchars($_documentCreatorName) .  '</span>';
								if($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']){
									$dateDisplayLangText = $Lang['DocRouting']['Management']['DraftDate'];
								}else{
									$dateDisplayLangText = $Lang['DocRouting']['Management']['PublishDate'];
								}
								$htmlAry['CurrentDisplay'] .= '<p> '.$Lang['DocRouting']['Status'].': '.$documentStatus.'</p>';
								$htmlAry['CurrentDisplay'] .= '<p>'.$dateDisplayLangText. ' ' . $_documentDateInput .  '</p>';
								if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
									if(isset($docIdToLastFollowTimeForCurrentUser[$_documentId])){
										$htmlAry['CurrentDisplay'] .= '<p>'.$Lang['DocRouting']['LastFollowedTime'].': '.$docIdToLastFollowTimeForCurrentUser[$_documentId].'</p>';	
									}	
								}
								$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p>';
							$htmlAry['CurrentDisplay'] .= '</div>';
							//Henry Added [Start]
							$divName = 'dr_content_detail_'.$i;
//							$htmlAry['CurrentDisplay'] .=  '<div style="float:right">';
//							//modified at 20131217
//			            	$htmlAry['CurrentDisplay'] .= '&nbsp;<span id="spanShowOption_'.$divName.'_hide">'."\n";
//							$htmlAry['CurrentDisplay'] .= $this->Get_Show_Option_Link("javascript:js_Show_Option_Div('".$divName."_hide');","",$Lang['StaffAttendance']['ShowDetail']);
//							$htmlAry['CurrentDisplay'] .= '</span>'."\n";
//							//modified at 20131217
//							$htmlAry['CurrentDisplay'] .= '<span id="spanHideOption_'.$divName.'_hide" style="display:none">'."\n";
//							$htmlAry['CurrentDisplay'] .= $this->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('".$divName."_hide');","",$Lang['StaffAttendance']['HideDetail']);
//							$htmlAry['CurrentDisplay'] .= '</span>&nbsp;'."\n";
//							$htmlAry['CurrentDisplay'] .=  '</div>';
							//Henry Added [End]
							$htmlAry['CurrentDisplay'] .= '<div class="dr_content_detail" id="'.$divName.'">';
								//added at 20131217
								$htmlAry['CurrentDisplay'] .= '<div id="'.$divName.'_hide" style="">';
								$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p>'; //Henry Added
//								$htmlAry['CurrentDisplay'] .= '<div class="dr_content_file">';
//								
//								# Show if attached files
//								if($_numOfFile>0){								
//									$htmlAry['CurrentDisplay'] .= '<span class="file_attach">'. Get_Plural_Display($_numOfFile,$Lang['DocRouting']['File']). '</span>';				                                        
//								}
//								
//								# show physical location
//								if ($_documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location']) {
//									$htmlAry['CurrentDisplay'] .= $Lang['DocRouting']['PhysicalLocation'].': '.intranet_htmlspecialchars($_physicalLocation);
//								}
//								
//								
//								$htmlAry['CurrentDisplay'] .= '</div>';
								
								//$htmlAry['CurrentDisplay'] .= '<ul class="member_list">';
								//$htmlAry['CurrentDisplay'] .= '<table>'; //table				
								##############  Show Each Routing ####################
					
									# Check if This Route Not Yet Start (For General Route) 
									$displayItemInGrey ='';
																				
									for ($j=0; $j<$_numOfRoute; $j++){
									
										$__routeId = $_documentRouteAry[$j]['RouteID'];
										$__routUserId = $_documentRouteAry[$j]['UserID'];
										//$__routUserName = $libuser -> getNameWithClassNumber($__routUserId, 0);
										
										$__effectiveType = $_documentRouteAry[$j]['EffectiveType'];//useful									
										$__effectiveStartDate = $_documentRouteAry[$j]['EffectiveStartDate'];//useful										
										$__effectiveEndDate = $_documentRouteAry[$j]['EffectiveEndDate'];//useful										
										//$__routFeedBackArr = $indexVar['libDocRouting']->getRouteUserFeedbacks($__routeId);							
										$__routFeedBackArr = array();
										if(isset($routeIdToRouteUserFeedbacksAry[$__routeId])){
											$__routFeedBackArr = $routeIdToRouteUserFeedbacksAry[$__routeId];
										}
																			
										//$__targetRouteUserArr = $indexVar['libDocRouting']->getRouteTargetUsers(array($__routeId));
										$__targetRouteUserArr = array();
										if(isset($routeIdToRouteTargetUsersAry[$__routeId])){
											$__targetRouteUserArr = $routeIdToRouteTargetUsersAry[$__routeId];
										}
																	
										$__numOftargetRouteUsers = count($__targetRouteUserArr);									
										$routFeedBackAssoAry = BuildMultiKeyAssoc($__routFeedBackArr, 'UserID');
												
										//debug_pr($routFeedBackAssoAry);					
											
									    # For Debug Route Status 	
										//debug_pr($_documentId . ' - ' . $__routeId . ' - '. $_documentTitle . '-' . $routeCompleteStatusAry[$_documentId][$__routeId] );
										#### Check Each Route Status #####
										
										$statusCss ='';
										
										//debug_pr($_documentTitle . '-' . $_documentId . '-' . $__routeId . '-'. $routeCompleteStatusAry[$_documentId][$__routeId].'-'.$__effectiveType);
										
										
										if($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){
											$_routeStatusDisplay='';
											
											if($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['inProgress']){
												$arrowDisplayCout ++;
												$calendarCss = 'time_current';
												$statusCss = 'status_current';
												
												$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Processing'];
												
							
											}elseif($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['completed']){
												
												$calendarCss = 'time_pass';
												$statusCss = 'status_pass';
											
												$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];
												
											}elseif($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['notStarted']){
												
												$calendarCss ='time_coming';
												$statusCss = '';	
												$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];
											}else{
												$_routeStatusDisplay='';
												$statusCss = '';
											}		
											
										}
										elseif($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished']){
											
											if($arrowDisplayCout > 0){
												$statusCss = '';	
												$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];
												
											}else{
											
												$_routeStatusDisplay='';
												if($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['inProgress']){
													$arrowDisplayCout ++;							
													$statusCss = 'status_current';
													$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Processing'];
								
												}elseif($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['completed']){
													
													$statusCss = 'status_pass';
													$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];
													
												}elseif($routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['notStarted']){
																	
													$statusCss = '';	
													$_routeStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];
												}else{
													$_routeStatusDisplay = '';
													$statusCss = '';	
												}
											}
												
										}
										//debug_pr($statusCss);
										#### End of Check Each Route Status #####		
										
												
																					
										## Route And Users In Route Display 																				
										if($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){	
											$htmlAry['CurrentDisplay'] .= '<table class="common_table_list_v30  view_table_list_v30" style="width:98%" align="left"  border="0" cellSpacing="0" cellPadding="4"><tr class="' . $statusCss .'" title="'.$_routeStatusDisplay.'">';
											$htmlAry['CurrentDisplay'] .= '<th class="tabletop tabletopnolink" colspan="2" class="' . $calendarCss . '">';
											$htmlAry['CurrentDisplay'] .=  $Lang['DocRouting']['Route'].' '.($j+1)." ($__effectiveStartDate" . ' ~ ' . "$__effectiveEndDate)";
											$htmlAry['CurrentDisplay'] .= '</th></tr>';	
											$htmlAry['CurrentDisplay'] .= '<tr><th width="50%">'.$Lang['DocRouting']['Users'].'</th><th width="50%">'.$Lang['DocRouting']['Status'].'</th><!--<th>Status Change Date</th>--></tr>';							
											$_userStatusDisplay = '';	
											for($k=0;$k<$__numOftargetRouteUsers;$k++){
												
												$___targetRouteUserID = $__targetRouteUserArr[$k]['UserID'];						
												$___targetRouteUserRecordStatus = $__targetRouteUserArr[$k]['RecordStatus'];
												$___targetRouteUserAccessRight	= $__targetRouteUserArr[$k]['AccessRight'];	
	
												$___routeUserFeedbackLastModified = $routFeedBackAssoAry[$___targetRouteUserID]['FeedbackModifyDate'];
																																							
												if($___targetRouteUserAccessRight == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['View']){
														$_routeStatusCss = 'memmber_View_Only';	
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['ViewOnly'];
												}
												else{
													if($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'] && $___targetRouteUserID == $indexVar['drUserId'] && $routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['inProgress']){
												
														$_routeStatusCss = 'memmber_alert';	
														$_userStatusDisplay = $Lang['DocRouting']['NotViewedYet'];	
															
														$userIconControlCount ++;			
													}
													elseif($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']){
														$_routeStatusCss = 'memmber_read';
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Processing'];
														$userIconControlCount ++;																																					
													}
													elseif($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']){
														$_routeStatusCss = 'memmber_done';	
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];	
										
													}
													
													else{
														$_routeStatusCss = 'memmber_empty_css';		
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];
													}	
												}
												if ($_routeStatusCss == 'memmber_View_Only'){
													$htmlAry['CurrentDisplay'] .= '<tr><td><span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">('.$__targetRouteUserArr[$k]['UserName'].')</span></td>';
											
												}else{
													$htmlAry['CurrentDisplay'] .= '<tr><td><span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">'.$__targetRouteUserArr[$k]['UserName'].'</span></td>';
												}
												$htmlAry['CurrentDisplay'] .= '<td><span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">'.$_userStatusDisplay.'</span></td><!--<td>'.($___routeUserFeedbackLastModified?$___routeUserFeedbackLastModified:'---').'</td>--></tr>';
											
											}
																						
											$htmlAry['CurrentDisplay'] .= '</table>';
											$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p><br/>';
										}	
										else{
											
						
											  $htmlAry['CurrentDisplay'] .= '<table class="common_table_list_v30  view_table_list_v30" style="width:98%" align="left"  border="0" cellSpacing="0" cellPadding="4"><tr class="' . $statusCss .'" title="'.$_routeStatusDisplay.'">';
											$htmlAry['CurrentDisplay'] .= '<th class="tabletop tabletopnolink" colspan="2" class="' . $calendarCss . '">';
											$htmlAry['CurrentDisplay'] .=  $Lang['DocRouting']['Route'].' '.($j+1);
											$htmlAry['CurrentDisplay'] .= '</th></tr>';	
											$htmlAry['CurrentDisplay'] .= '<tr><th width="50%">'.$Lang['DocRouting']['Users'].'</th><th width="50%">'.$Lang['DocRouting']['Status'].'</th><!--<th>Status Change Date</th>--></tr>';							
											
										
											if($userIconControlCount > 0){
												for($k=0;$k<$__numOftargetRouteUsers;$k++){																									
													$___targetRouteUserID = $__targetRouteUserArr[$k]['UserID'];												
													$___targetRouteUserRecordStatus = $__targetRouteUserArr[$k]['RecordStatus'];																															
															
													$_routeStatusCss = '';		
													
	
													$htmlAry['CurrentDisplay'] .= '<tr><td><span class="'.$_routeStatusCss.'">'.$__targetRouteUserArr[$k]['UserName'].'</span></td>';
													$htmlAry['CurrentDisplay'] .= '<td><span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">--</span></td><!--<td>---</td>--></tr>';
											
												}
											}
											else{
												$_userStatusDisplay = '';	
												for($k=0;$k<$__numOftargetRouteUsers;$k++){																									
													$___targetRouteUserID = $__targetRouteUserArr[$k]['UserID'];//useful												
													$___targetRouteUserRecordStatus = $__targetRouteUserArr[$k]['RecordStatus'];//useful																															
													$___targetRouteUserAccessRight	= $__targetRouteUserArr[$k]['AccessRight'];	
							
												    $___routeUserFeedbackLastModified = $routFeedBackAssoAry[$___targetRouteUserID]['FeedbackModifyDate'];
													
													if($___targetRouteUserAccessRight == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['View']){
														$_routeStatusCss = 'memmber_View_Only';	
														$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['ViewOnly'];
													}
													else{
																										
														if($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet'] && $___targetRouteUserID == $indexVar['drUserId'] && $routeCompleteStatusAry[$_documentId][$__routeId] == $docRoutingConfig['routeStatus']['inProgress']){
															
															$_routeStatusCss = 'memmber_alert';	
															$_userStatusDisplay = $Lang['DocRouting']['NotViewedYet'];	
															$userIconControlCount ++;										
														}
														elseif($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']){
															$_routeStatusCss = 'memmber_read';
															$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Processing'];
															$userIconControlCount ++;		
																																																									
														}elseif($___targetRouteUserRecordStatus == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']){
															$_routeStatusCss = 'memmber_done';	
															$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['Completed'];	
														}
														else{
															$_routeStatusCss = 'memmber_empty_css';	
															$_userStatusDisplay = $Lang['DocRouting']['StatusDisplay']['NotYetStarted'];	
														}	
														
													}
													
													
													
													//$htmlAry['CurrentDisplay'] .= '<span class="'.$_routeStatusCss.'">'.$__targetRouteUserArr[$k]['UserName'].'</span>';
												
													if ($_routeStatusCss == 'memmber_View_Only'){
													$htmlAry['CurrentDisplay'] .= '<tr><td><span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">('.$__targetRouteUserArr[$k]['UserName'].')</span></td>';
													
												}else{
													$htmlAry['CurrentDisplay'] .= '<tr><td><span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">'.$__targetRouteUserArr[$k]['UserName'].'</span></td>';
												}
												$htmlAry['CurrentDisplay'] .= '<td><span class="'.$_routeStatusCss.'" title="'.$_userStatusDisplay.'">'.$_userStatusDisplay.'</span></td><!--<td>'.($___routeUserFeedbackLastModified?$___routeUserFeedbackLastModified:'---').'</td>--></tr>';
											
												
												}
											
											}								
											
											
											$htmlAry['CurrentDisplay'] .= '</table>';
											$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p><br/>';
										}		
				
									}
									
									//$htmlAry['CurrentDisplay'] .= '</ul><p class="spacer"></p>';
									
									if($pageType==$docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']){
										$dateDisplayLangText = $Lang['DocRouting']['Management']['DraftDate'];
									}else{
										$dateDisplayLangText = $Lang['DocRouting']['Management']['PublishDate'];
									}
									//Henry Added at 20131217 [Start]
									$htmlAry['CurrentDisplay'] .= '</div>';	
									//Henry Added at 20131217 [End]
									
//									$htmlAry['CurrentDisplay'] .= '<span class="date_time"> '.$Lang['DocRouting']['Status'].': '.$documentStatus.' &nbsp '.$dateDisplayLangText. ' ' . $_documentDateInput .  '</span>';
//									
//									# Tags
//																	
//									$_numOfTagArr = count($_tagArr);
//									if($_numOfTagArr>0){
//										$htmlAry['CurrentDisplay'] .= '<span class="tag"><em>'.$Lang['DocRouting']['Tag']['label'].'</em>';	
//									}
//									
//									$__tagCommaCount = 0;
//									for($j=0;$j<$_numOfTagArr;$j++){
//										$__tagId = $_tagArr[$j]['TagID'];
//										$__tagNameArr = returnModuleTagNameByTagID($__tagId);
//										$__tagName = $__tagNameArr[0];		
//										
//										$__tagCommaCount ++;																		
//										if($_numOfTagArr>1){
//											$htmlAry['CurrentDisplay'] .= '<a href="javaScript:js_Filter_Selected_Tag('.$__tagId.')">'. intranet_htmlspecialchars($__tagName);
//											if($__tagCommaCount ==  $_numOfTagArr){
//												$htmlAry['CurrentDisplay'] .= '</a>';
//											}
//											else{
//												$htmlAry['CurrentDisplay'] .= '</a>, ';
//											}
//										}else{					
//											$htmlAry['CurrentDisplay'] .= '<a href="javaScript:js_Filter_Selected_Tag('.$__tagId.')">' . intranet_htmlspecialchars($__tagName) .'</a>';
//										}
//									}
//										
//								
//									$htmlAry['CurrentDisplay'] .='</span>';								
//								$htmlAry['CurrentDisplay'] .= '</div>';		
//								//$htmlAry['CurrentDisplay'] .= '</div>'; //Henry Added	
//										
//								$htmlAry['CurrentDisplay'] .= '<p class="spacer"></p>';		                                                          
//							$htmlAry['CurrentDisplay'] .= '</li>';	
							
						
						}																											
						
					}
//					if($selectedTag>0){	
//						$htmlAry['CurrentDisplay'] .= '<div class="edit_bottom">';											
//						$htmlAry['CurrentDisplay'] .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['DocRouting']['Button']['Back'], "button", "js_Filter_Selected_Tag('');");
//						$htmlAry['CurrentDisplay'] .= '</div>';
//					}	
					if($numOfItemAlreadyDisplay==0){							
						$htmlAry['CurrentDisplay'] .= '<li class="">';
						$htmlAry['CurrentDisplay'] .= '<br />';
						$htmlAry['CurrentDisplay'] .= '<center>'. $Lang['General']['NoRecordFound']. '</center>';
						$htmlAry['CurrentDisplay'] .= '<br />';	
						$htmlAry['CurrentDisplay'] .= '</li>';							
					}
			}else{
				
				$htmlAry['CurrentDisplay'] .= '<li class="">';
				$htmlAry['CurrentDisplay'] .= '<br />';
				$htmlAry['CurrentDisplay'] .= '<center>'. $Lang['General']['NoRecordFound']. '</center>';
				$htmlAry['CurrentDisplay'] .= '<br />';	
				$htmlAry['CurrentDisplay'] .= '</li>';	
			}

			return  $htmlAry['CurrentDisplay'];
		}
		
		function getSendSignEmailForm($DocumentID)
		{
			global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $iPowerVoice, $intranet_root, $indexVar, $docRoutingConfig, $plugin, $SYS_CONFIG;
			
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			
			$entry = $ldocrouting->getEntryData($DocumentID);
			$routes = $ldocrouting->getRouteData($DocumentID);
			$dataAry = $entry[0];
			$routeIdAry = Get_Array_By_Key($routes,'RouteID');
			
			$title = $dataAry['Title'];
			
			$target_route_users = $ldocrouting->getRouteTargetUsers($routeIdAry, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']));
			$selection_users = array();
			$selected_user_ids = array();
			for($i=0;$i<count($target_route_users);$i++){
				if(!in_array($target_route_users[$i]['UserID'],$selected_user_ids)){
					$selection_users[] = array('U'.$target_route_users[$i]['UserID'],$target_route_users[$i]['UserName']);
					$selected_user_ids[] = $target_route_users[$i]['UserID'];
				}
			}
			
			$form_action = $ldocrouting->getEncryptedParameter('management','ajax_send_sign_email',array('DocumentID'=>$DocumentID));
			
			$userSelection = $this->GET_SELECTION_BOX(array(),' id="TargetUser" name="TargetUser[]" size="6" multiple="multiple" style="min-width:16em;" ',"");
			//$usersRemoveBtn = $this->GET_SMALL_BTN($Lang['Btn']['Remove'], "button", "javascript:remove_selection_option('TargetUser', 1);");
			//$selectUsersBtn = $this->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('/home/common_choose/index.php?OpenerFormName=TBForm1&fieldname=TargetUser[]&ppl_type=pic&permitted_type=1&excluded_type=4&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1', 9)");
			$involvedUserSelection = $this->GET_SELECTION_BOX($selection_users,' id="InvolvedUser" name="InvolvedUser[]" size="6" multiple="multiple" style="min-width:16em;" ',"");
			$addUserBtn = $this->GET_SMALL_BTN("<< ".$Lang['Btn']['Add'], "button", "javascript:checkOptionTransfer(document.getElementById('InvolvedUser'),document.getElementById('TargetUser'));");
			$removeUserBtn = $this->GET_SMALL_BTN(">> ".$Lang['Btn']['Remove'], "button", "javascript:checkOptionTransfer(document.getElementById('TargetUser'),document.getElementById('InvolvedUser'));");
			
			$user_table = '<table class="inside_form_table" border="0" width="100%" cellpadding="5" cellspacing="0">';
				$user_table .= '<tr>';
					$user_table .= '<td>'.$userSelection.'</td>';
					$user_table .= '<td align="center" valign="middle">'.$addUserBtn.'<br /><br />'.$removeUserBtn.'</td>';
					$user_table .= '<td>'.$involvedUserSelection.'</td>';
				$user_table .= '</tr>';
			$user_table.= '</table>';
			
			$user_email_checkbox = $this->Get_Checkbox("AlternativeEmail", "TargetEmail[]", "UserEmail", $isChecked_=1, $Class_='', $Display_=$Lang['Gamma']['UserEmail'], $Onclick_='', $Disabled_='');
			if($plugin['imail_gamma']){
				$imail_checkbox = $this->Get_Checkbox("iMailPlus", "TargetEmail[]", "iMailPlus", $isChecked_=0, $Class_='', $Display_=$Lang['Header']['Menu']['iMailGamma'], $Onclick_='', $Disabled_='');
			}else{
				$imail_checkbox = $this->Get_Checkbox("iMail", "TargetEmail[]", "iMail", $isChecked_=0, $Class_='', $Display_=$Lang['Header']['Menu']['iMail'], $Onclick_='', $Disabled_='');
			}
			
			$route_field = '';
			for($i=0;$i<count($routes);$i++){
				$route_field .= $this->Get_Checkbox("TargetRoute".$routes[$i]['RouteID'], "TargetRoute[]", $routes[$i]['RouteID'], $isChecked_=1, $Class_='', $Display_=$Lang['DocRouting']['Routing'].'&nbsp;'.($i+1), $Onclick_='', $Disabled_='').'&nbsp;';
			}
			
			$remark = $Lang['DocRouting']['SendSignEmailRemark'];
			$last_sent_time = $ldocrouting->getSignEmailDocumentLastSentTime($DocumentID);
			if($last_sent_time != ''){
				$remark .= '<br />('.$Lang['DocRouting']['LastSentTime'].': '.$last_sent_time.')';
			}
			
			$x = '<form id="TBForm1" name="TBForm1" method="post" action="index.php?pe='.$form_action.'" onsubmit="return:false;">';
			$x.= '<div id="TBEditDiv" style="height:380px; overflow-x:hidden; overflow-y:auto;">';
			$x.= $this->Get_Warning_Message_Box($Lang['DocRouting']['Instruction'], $Lang['DocRouting']['SendSignEmailInstruction'], $others="");
			$x .= '<div class="dr_list_header_detail" style="padding:5px;"><h1>'.$title.'</h1></div>';
			$x.= '<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['DocRouting']['Users'].':</td>
						<td>'.$user_table.$this->Get_Thickbox_Warning_Msg_Div("TargetUserWarning", $Lang['DocRouting']['WarningMsg']['PleaseSelectUsers'], 'Warning').'</td>
					</tr>
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['DocRouting']['Email'].':</td>
						<td>'.$user_email_checkbox.'&nbsp;'.$imail_checkbox.'&nbsp;'.$this->Get_Thickbox_Warning_Msg_Div("TargetEmailWarning", $Lang['DocRouting']['WarningMsg']['PleaseSelectEmailType'], 'Warning').'</td>
					</tr>
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['DocRouting']['Route'].':</td>
						<td>'.$route_field.'&nbsp;'.$this->Get_Thickbox_Warning_Msg_Div("TargetRouteWarning", $Lang['DocRouting']['WarningMsg']['PleaseSelectRoutes'], 'Warning').'</td>
					</tr>
					<tr>
						<td colspan="2">'.$this->MandatoryField().'</td>
					</tr>
				  </table>';
			$x.='</div>';
			//$x.='<input type="hidden" id="DocumentID" name="DocumentID" value="'.$DocumentID.'" />';	
			$x.='</form>';
			$x .= '<div class="tabletextremark">'.$remark.'</div>';
			$x .= '<div class="edit_bottom">
			            <p class="spacer"></p>
			            '.$this->GET_ACTION_BTN($Lang['Btn']['Send'], "button", "js_SendSignEmail('$form_action');").'&nbsp;
			            '.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.tb_remove();").'
						<p class="spacer"></p>
			        </div>';	
			
			return $x;
		}
		
		function getSendArchivedDocumentEmailForm($DocumentID)
		{
			global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $iPowerVoice, $intranet_root, $indexVar, $docRoutingConfig, $plugin, $SYS_CONFIG;
			
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			
			$entry = $ldocrouting->getEntryData($DocumentID);
			$routes = $ldocrouting->getRouteData($DocumentID);
			$dataAry = $entry[0];
			$routeIdAry = Get_Array_By_Key($routes,'RouteID');
			
			$title = $dataAry['Title'];
			
			$target_route_users = $ldocrouting->getRouteTargetUsers($routeIdAry, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']));
			$selection_users = array();
			$selected_user_ids = array();
			for($i=0;$i<count($target_route_users);$i++){
				if(!in_array($target_route_users[$i]['UserID'],$selected_user_ids)){
					$selection_users[] = array('U'.$target_route_users[$i]['UserID'],$target_route_users[$i]['UserName']);
					$selected_user_ids[] = $target_route_users[$i]['UserID'];
				}
			}
			
			$form_action = $ldocrouting->getEncryptedParameter('management','ajax_send_archived_document_email',array('DocumentID'=>$DocumentID));
			
			$userSelection = $this->GET_SELECTION_BOX(array(),' id="TargetUser" name="TargetUser[]" size="6" multiple="multiple" style="min-width:16em;" ',"");
			//$usersRemoveBtn = $this->GET_SMALL_BTN($Lang['Btn']['Remove'], "button", "javascript:remove_selection_option('TargetUser', 1);");
			//$selectUsersBtn = $this->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('/home/common_choose/index.php?OpenerFormName=TBForm1&fieldname=TargetUser[]&ppl_type=pic&permitted_type=1&excluded_type=4&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1', 9)");
			$involvedUserSelection = $this->GET_SELECTION_BOX($selection_users,' id="InvolvedUser" name="InvolvedUser[]" size="6" multiple="multiple" style="min-width:16em;" ',"");
			$addUserBtn = $this->GET_SMALL_BTN("<< ".$Lang['Btn']['Add'], "button", "javascript:checkOptionTransfer(document.getElementById('InvolvedUser'),document.getElementById('TargetUser'));");
			$removeUserBtn = $this->GET_SMALL_BTN(">> ".$Lang['Btn']['Remove'], "button", "javascript:checkOptionTransfer(document.getElementById('TargetUser'),document.getElementById('InvolvedUser'));");
			
			$user_table = '<table class="inside_form_table" border="0" width="100%" cellpadding="5" cellspacing="0">';
				$user_table .= '<tr>';
					$user_table .= '<td>'.$userSelection.'</td>';
					$user_table .= '<td align="center" valign="middle">'.$addUserBtn.'<br /><br />'.$removeUserBtn.'</td>';
					$user_table .= '<td>'.$involvedUserSelection.'</td>';
				$user_table .= '</tr>';
			$user_table.= '</table>';
			
			$user_email_checkbox = $this->Get_Checkbox("AlternativeEmail", "TargetEmail[]", "UserEmail", $isChecked_=1, $Class_='', $Display_=$Lang['Gamma']['UserEmail'], $Onclick_='', $Disabled_='');
			if($plugin['imail_gamma']){
				$imail_checkbox = $this->Get_Checkbox("iMailPlus", "TargetEmail[]", "iMailPlus", $isChecked_=0, $Class_='', $Display_=$Lang['Header']['Menu']['iMailGamma'], $Onclick_='', $Disabled_='');
			}else{
				$imail_checkbox = $this->Get_Checkbox("iMail", "TargetEmail[]", "iMail", $isChecked_=0, $Class_='', $Display_=$Lang['Header']['Menu']['iMail'], $Onclick_='', $Disabled_='');
			}
			
			$x = '<form id="TBForm1" name="TBForm1" method="post" action="index.php?pe='.$form_action.'" onsubmit="return:false;">';
			$x.= '<div id="TBEditDiv" style="height:350px; overflow-x:hidden; overflow-y:auto;">';
			$x.= $this->Get_Warning_Message_Box($Lang['DocRouting']['Instruction'], $Lang['DocRouting']['SendArchivedDocumentEmailInstruction'], $others="");
			$x .= '<div class="dr_list_header_detail" style="padding:5px;"><h1>'.$title.'</h1></div>';
			$x.= '<table class="form_table_v30" align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['DocRouting']['Users'].':</td>
						<td>'.$user_table.$this->Get_Thickbox_Warning_Msg_Div("TargetUserWarning", $Lang['DocRouting']['WarningMsg']['PleaseSelectUsers'], 'Warning').'</td>
					</tr>
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['DocRouting']['Email'].':</td>
						<td>'.$user_email_checkbox.'&nbsp;'.$imail_checkbox.'&nbsp;'.$this->Get_Thickbox_Warning_Msg_Div("TargetEmailWarning", $Lang['DocRouting']['WarningMsg']['PleaseSelectEmailType'], 'Warning').'</td>
					</tr>
					<tr>
						<td colspan="2">'.$this->MandatoryField().'</td>
					</tr>
				  </table>';
			$x.='</div>';
			//$x.='<input type="hidden" id="DocumentID" name="DocumentID" value="'.$DocumentID.'" />';	
			$x.='</form>';
			//$x .= '<div class="tabletextremark">'.$remark.'</div>';
			$x .= '<div class="edit_bottom">
			            <p class="spacer"></p>
			            '.$this->GET_ACTION_BTN($Lang['Btn']['Send'], "button", "js_SendArchivedDocumentEmail('$form_action');").'&nbsp;
			            '.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.tb_remove();").'
						<p class="spacer"></p>
			        </div>';	
			
			return $x;
		}
		
		/*
		 * $DocumentID : must provided
		 * $IsArchive : true for archive mode, attachment links are as place holders ###DR_FILE_[ID]###, then replace with data uri
		 * $SelfFeedback: true for getting signed user's own feedbacks, to be appended to notification email
		 * $FilterRouteId: use with $SelfFeedback to get the desired routing feedbacks
		 */
		function getDocumentDetailPrintPage($DocumentID,$IsArchive=false,$SelfFeedback=false,$FilterRouteId='')
		{
			global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $xmsg, $indexVar, $docRoutingConfig, $plugin, $replySlipConfig, $tableReplySlipConfig, $sys_custom;
			
			$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			//$ldamu = $indexVar['libdigitalarchive_moduleupload'];
			//$canArchiveFile = $ldamu->User_Can_Archive_File();
			$libReplySlipMgr = $indexVar['libReplySlipMgr'];
			$libTableReplySlipMgr = $indexVar['libTableReplySlipMgr'];
			
			if(isset($indexVar['entryData'])){
				$entry = $indexVar['entryData'];
			}else{
				$entry = $ldocrouting->getEntryData($DocumentID);
			}
			$indexVar['DocEntry'] = $entry;
			$title = $entry[0]['Title'];
			$creatorUserID = $entry[0]['UserID'];
			$creatorName = $entry[0]['CreatorName'];
			$instruction = $entry[0]['Instruction'];
			$documentType = $entry[0]['DocumentType'];
			$physicalLocation = $entry[0]['PhysicalLocation'];
			$recordStatus = $entry[0]['RecordStatus'];
			$allowAdHocRoute = $entry[0]['AllowAdHocRoute'];
			$published_date = $entry[0]['DateInput'];
			$stared = $entry[0]['Stared'] > 0;
			
			$isDraft = $recordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft'];
			$isCompleted = $recordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete'];
			$isDeleted = $recordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Deleted'];
			
			if($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']){
				$attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $DocumentID);
			}
			if($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA']){
				$attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $DocumentID);
			}
			$numOfAttachment = count($attachments);
			$routes = $ldocrouting->getRouteData($DocumentID,'',$isDeleted?$docRoutingConfig['INTRANET_DR_ROUTES']['RecordStatus']['Deleted']:$docRoutingConfig['INTRANET_DR_ROUTES']['RecordStatus']['Active']);
			$numOfRoutes = count($routes);
			
			$routeStatusAssoc = $ldocrouting->getRouteCompleteStatus($DocumentID);
			$indexVar['RouteStatusAssoc'] = $routeStatusAssoc;
			$is_all_routes_completed = true;
			
			$route_status_ary = $ldocrouting->getDocRoutingUserStatusCount($DocumentID);
			for($j=0;$j<count($route_status_ary);$j++){
				if($route_status_ary[$j]['UserTotal'] != $route_status_ary[$j]['CompletedTotal']){
					$is_all_routes_completed = false;
					break;
				}
			}
			
			$diplay_status = '';
			if($isCompleted){
				$display_status = $Lang['DocRouting']['StatusDisplay']['Completed'];
			}else if($isDraft){
				$display_status = $Lang['DocRouting']['Draft'];
			}else if($isDeleted){
				$display_status = $Lang['DocRouting']['Deleted']; 
			}else{
				$display_status = $Lang['DocRouting']['StatusDisplay']['Processing'];
			}
			
			$tagNames = $ldocrouting->getEntryTagName($DocumentID);
			if(count($tagNames) > 0){
				$docTags = implode(", ",$tagNames);
			}
			
			if($SelfFeedback){
				$http = $_SERVER['SERVER_PORT'] == '443'? "https" : "http"; 
				$server_link = "$http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
			}
			
			$tok = $indexVar['tok'];
			$x = '';
			if(!$IsArchive && !$SelfFeedback){
			$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","print_button").'</td>
						</tr>
					</table>';
			}
			$x .= '<table style="width:96%" align="center" border="0" cellspacing="0" cellpadding="4">
					<tbody>
						<tr>
							<td>';
				
						$x .= '<h2>'.intranet_htmlspecialchars($title).'</h2>';
						if($sys_custom['DocRouting']['RequestNumber']){
							$x .= '<h3> '.$Lang['DocRouting']['RequestNumber'].': '.$DocumentID.'</h3>';
						}
							$x .= '<table class="common_table_list_v30  view_table_list_v30" style="width:100%" align="center" border="0" cellspacing="0" cellpadding="15">
									<tbody>
										<tr >
											<td nowrap="nowrap">'.$Lang['DocRouting']['Instruction'].':</td>
											<td width="95%">'.$instruction.'</td>
										</tr>';
						if ($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location']) {
								$x .= '<tr>
											<td nowrap="nowrap">'.$Lang['DocRouting']['PhysicalLocation'].':</td>
											<td>'.intranet_htmlspecialchars($physicalLocation).'</td>
									   </tr>';
						}else if ($numOfAttachment > 0){
								$param_tok = $tok!=''?'&tok='.$tok:'';
								
								 $x .= '<tr>
											<td nowrap="nowrap">'.$Lang['DocRouting']['Documents'].':</td>
											<td>';
										$x .= '<ol>';
										for($i=0;$i<$numOfAttachment;$i++){
											$attachment_path = $attachments[$i]['FilePath'];
											$attachment_name = $attachments[$i]['FileName'];
											$attachment_id = $attachments[$i]['FileID'];
											$attachment_size = $ldocrouting->getAttachmentDisplaySize($attachments[$i]['SizeInBytes']);
											$attachment_time = $attachments[$i]['DateModified'];
											
											if($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']){
											   $file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],'LinkToID'=>$DocumentID,'FileID'=>$attachment_id));
											}
											if($documentType == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA']){
											   $file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],'LinkToID'=>$DocumentID,'FileID'=>$attachment_id));
											}
											
											$file_size_time = ' ('.$attachment_size.', '.$attachment_time.')';
											if($IsArchive){
												$x .= '<li>
															<a href="###DR_FILE_'.$attachment_id.'###" target="_blank" download="'.htmlspecialchars($attachment_name).'">'.$attachment_name.'</a>'.$file_size_time.'
														</li>';
											}else{
												$x .= '<li>
															'.$attachment_name.$file_size_time.'
														</li>';
											}
										}		
								 		$x .= '</ol>
											</td>
										</tr>';
						}
							if(trim($docTags) != ''){	
								 $x .= '<tr>
											<td nowrap="nowrap">'.$Lang['DocRouting']['Tag']['label'].'</td>
											<td>'.$docTags.'</td>
										</tr>';
							}
								 $x .= '<tr>
											<td nowrap="nowrap">'.$Lang['DocRouting']['Creator'].':</td>
											<td>'.$creatorName.'</td>
										</tr>
										<tr>
											<td nowrap="nowrap">'.$Lang['DocRouting']['Management']['PublishDate'].'</td>
											<td>'.$published_date.'</td>
										</tr>
										<tr>
											<td nowrap="nowrap">'.$Lang['DocRouting']['Status'].':</td>
											<td>'.$display_status.'</td>
										</tr>
									</tbody>
								</table>';
				
				for($i=0;$i<$numOfRoutes;$i++)
				{
					$route_id = $routes[$i]['RouteID'];
					$note = $routes[$i]['Note'];
					$effective_type = $routes[$i]['EffectiveType'];
					$creator_name = $routes[$i]['CreatorName'];
					$effective_startdate = $routes[$i]['EffectiveStartDate'];
					$effective_enddate = $routes[$i]['EffectiveEndDate'];
					$view_right = $routes[$i]['ViewRight'];
					$actions_ary = explode(",",$routes[$i]['Actions']);
					
					if($SelfFeedback && $FilterRouteId != $route_id){
						continue;
					}
					
					$allow_sign = true;
					$allow_replyslip = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'],$actions_ary);
					$allow_comment = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'],$actions_ary) || in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'],$actions_ary);
					//Henry added [20140523][start]
					$allow_comment_read_by_own = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'],$actions_ary);
					//Henry added [20140523][end]
					$allow_attachment = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'],$actions_ary) || in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'],$actions_ary);
					//Henry added [20140523][start]
					$allow_attachment_read_by_own = in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'],$actions_ary);
					
					$reply_slip_id = $routes[$i]['ReplySlipID'];
					$reply_slip_type = $routes[$i]['ReplySlipType'];
					$reply_slip_release_type = $routes[$i]['ReplySlipReleaseType'];
					$route_user_feedbacks = $ldocrouting->getRouteUserFeedbacks($route_id,array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']));
					$route_users = $ldocrouting->getRouteTargetUsers($route_id, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']));
					$feedbackIdAry = Get_Array_By_Key($route_user_feedbacks,'FeedbackID');
					$feedback_attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'], $feedbackIdAry);
					
					$self_status = $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'];
					$userStatusAry = array();
					for($j=0;$j<count($route_users);$j++){
						$record_status = $route_users[$j]['RecordStatus'];
						if(!isset($userStatusAry[$record_status])){
							$userStatusAry[$record_status] = array();
						}
						if($route_users[$j]['UserID'] == $indexVar['drUserId']){
							if($record_status == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']){
								//$ldocrouting->updateTargetUserStatus($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'],$route_id,$indexVar['drUserId']);
						 		$self_status = $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress'];
						 	}else{
						 		$self_status = $record_status;
						 	}
						}
						$userStatusAry[$record_status][] = $route_users[$j];
					}
					
					$feedbackIdToFile = array();
					for($j=0;$j<count($feedback_attachments);$j++){
						if(!isset($feedbackIdToFile[$feedback_attachments[$j]['LinkToID']])){
							$feedbackIdToFile[$feedback_attachments[$j]['LinkToID']] = array();
						}
						$feedbackIdToFile[$feedback_attachments[$j]['LinkToID']][] = $feedback_attachments[$j];
					}
					
					
					$displyReleaseStatistic = false;
					if($reply_slip_release_type==$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseAnytime'])
					{
						$displyReleaseStatistic = true;
					}elseif($reply_slip_release_type==$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterSubmission'])
					{
						if($self_status == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']){
							$displyReleaseStatistic = true;
						}
					}elseif($reply_slip_release_type==$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterRouteCompeleted'])
					{
						if($recordStatus == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']){
							$displyReleaseStatistic = true;
						}
					}
					
						$x.= '<br /><br /><br /><hr />';
						$x.= '<h3>'.$Lang['DocRouting']['Routing'].' #'.($i+1).'</h3>';
						if($effective_type == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period'])
						{
							$x.= '<p>'.$effective_startdate.' ~ '.$effective_enddate.'</p>';
						}
					if(strip_tags($note) == '' || $note == ''){
						// empty instruction
					}else{
						$x.= '<table class="view_table_list_v30" style="width:100%" align="center" border="0" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<!--td nowrap="nowrap" valign="top">'.$Lang['DocRouting']['Instruction'].':</td>
										<td width="99%">'.$note.'</td-->
										<td>'.$note.'</td>
									</tr>
								</tbody>
							   </table>
							   <br />';
					}
						$x .= '<table class="common_table_list_v30  view_table_list_v30" style="width:99%" align="center" border="0" cellspacing="0" cellpadding="4">
								<thead>
									<tr>
										<th width="10%" nowrap="nowrap">'.$Lang['DocRouting']['Users'].'</th>
										<th width="10%" nowrap="nowrap">'.$Lang['DocRouting']['ActionAndTime'].'</th>
										<th>'.$Lang['DocRouting']['CommentAndDocument'].'</th>
									</tr>
								</thead>
								<tbody>';
						
						for($k=0;$k<count($route_user_feedbacks);$k++)
						{
							$user_id = $route_user_feedbacks[$k]['UserID'];
							$user_name = $route_user_feedbacks[$k]['UserName'];
							$feedback_id = $route_user_feedbacks[$k]['FeedbackID'];
							$comment = $route_user_feedbacks[$k]['Comment'];
							$modify_date = $route_user_feedbacks[$k]['FeedbackModifyDate'];
							if($SelfFeedback && $user_id != $indexVar['drUserId']){
								continue;
							}
							$user_attachments = $feedbackIdToFile[$feedback_id];
							
							$allow_show_comment = true;
							$allow_show_attachment = true;
							if(!($routes[$i]['UserID'] == $indexVar['drUserId'] || $user_id == $indexVar['drUserId'] || $indexVar['drUserIsAdmin'])){
								if($allow_comment_read_by_own)
									$allow_show_comment = false;
								if($allow_attachment_read_by_own)
									$allow_show_attachment = false;
							}
							
							if(($allow_comment_read_by_own && $comment && !$feedbackIdToFile[$feedback_id] && !$allow_show_comment) || ($allow_attachment_read_by_own && !$comment && $feedbackIdToFile[$feedback_id] && !$allow_show_attachment) || ($allow_comment_read_by_own && $allow_attachment_read_by_own && !$allow_show_comment && !$allow_show_attachment)){
								continue;
							}
							
							$display_attachment = '<ol>';
							for($n=0;$n<count($user_attachments);$n++){
				        		$file_id = $user_attachments[$n]['FileID'];
				        		$file_name = $user_attachments[$n]['FileName'];
				        		$file_path = $user_attachments[$n]['FilePath'];
				        		$file_type = $user_attachments[$n]['RecordType'];
				        		$file_size = $ldocrouting->getAttachmentDisplaySize($user_attachments[$n]['SizeInBytes']);
				        		$file_time = $user_attachments[$n]['DateModified'];
				        		if($IsArchive){
				        			$display_attachment.= '<li><a href="###DR_FILE_'.$file_id.'###" target="_blank" download="'.htmlspecialchars($file_name).'">'.$file_name.'</a> ('.$file_size.', '.$file_time.')</li>';
				        		}else{
				        			$display_attachment.= '<li>'.$file_name.' ('.$file_size.', '.$file_time.')</li>';
				        		}
				        	/*	else if($SelfFeedback){
				        			$file_param = $ldocrouting->getEncryptedParameter('common','view_attachment',array('LinkToType'=>$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'],'LinkToID'=>$feedback_id,'FileID'=>$file_id)).'&tok='.$tok;
				        			$download_link = $server_link."/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=".$file_param;
				        			$display_attachment.= '<li><a href="'.$download_link.'" target="_blank">'.$file_name.'</a> ('.$file_size.', '.$file_time.')</li>';
				        		} */
				        		
							}
							$display_attachment .= '</ol>';
							$display_action = '';
							$action_detail = '';
							if($comment !='' && count($user_attachments)>0){
								$display_action = $Lang['DocRouting']['CommentedAndAttached'];
								if($allow_comment && $allow_show_comment){
									$action_detail .= $comment;
								}
								if($allow_attachment && $allow_show_attachment){
									$action_detail .= $display_attachment;
								}
							}else if($comment != ''){
								$display_action = $Lang['DocRouting']['Commented'];
								if($allow_comment && $allow_show_comment){
									$action_detail .= $comment;
								}
							}else if(count($user_attachments)>0){
								$display_action = $Lang['DocRouting']['Attached'];
								if($allow_attachment && $allow_show_attachment){
									$action_detail .= $display_attachment;
								}
							}
							
							$x .= '<tr>
									<td nowrap="nowrap"><span class="memmber_empty_css">'.$user_name.'</span></td>
									<td nowrap="nowrap">
									<span class="memmber_empty_css">';
								$x .= $display_action .'<br />'. $modify_date;		
								$x.='</span>
									</td>
									<td>';
								$x .= $action_detail;
							$x .=  '</td>
								   </tr>';
						}
						
						$not_yet_signed_names = array();
						for($j=0;$j<count($route_users);$j++){
							if($route_users[$j]['RecordStatus'] == $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']){
								
								if($SelfFeedback && $route_users[$j]['UserID'] != $indexVar['drUserId']){
									continue;
								}
								$display_reply_slip = '';
								if($allow_replyslip && $reply_slip_id > 0){
									if($reply_slip_type == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
										$libTableReplySlip = new libTableReplySlip($reply_slip_id);
										$ShowUserInfoInResult = $libTableReplySlip->getShowUserInfoInResult();
										$libTableReplySlipMgr->setCurUserId($route_users[$j]['UserID']);
										$libTableReplySlipMgr->setReplySlipId($reply_slip_id);
										$showUserAnswers = $ShowUserInfoInResult==$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['show'] || $indexVar['drUserIsAdmin'] || $creatorUserID == $indexVar['drUserId'] || $route_users[$j]['UserID'] == $indexVar['drUserId'];
										$display_reply_slip = $libTableReplySlipMgr->returnReplySlipHtml(false,$showUserAnswers,true,true);
									} else {
										$libReplySlip = new libReplySlip($reply_slip_id);
										$ShowUserInfoInResult = $libReplySlip->getShowUserInfoInResult();	    
									    $libReplySlipMgr->setCurUserId($route_users[$j]['UserID']);
										$libReplySlipMgr->setReplySlipId($reply_slip_id);
										$showUserAnswers = $ShowUserInfoInResult==$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['show'] || $indexVar['drUserIsAdmin'] || $creatorUserID == $indexVar['drUserId'] || $route_users[$j]['UserID'] == $indexVar['drUserId'];
										$display_reply_slip = $libReplySlipMgr->returnReplySlipHtml(false,$showUserAnswers,true,true);
									}
								}
								
								$x .= '<tr>
									<td nowrap="nowrap"><span class="memmber_empty_css">'.$route_users[$j]['UserName'].'</span></td>
									<td nowrap="nowrap">
									<span class="memmber_empty_css">';
								$x .= $Lang['DocRouting']['SignedConfirmed'].'<br />'.$route_users[$j]['DateModified'];	
								$x.='</span>
									</td>
									<td>'.($display_reply_slip!=''?$display_reply_slip:$Lang['General']['EmptySymbol']).'</td>
								   </tr>';
							}else{
								$not_yet_signed_names[] = $route_users[$j]['UserName'];
							}
						}
						$x .= '</tbody>
							</table>
							<p class="spacer"></p>';
							
						if(count($not_yet_signed_names)>0){
							$x .= '<p><span style="font-weight:bold">'.$Lang['DocRouting']['NotYetSigned'].':</span> '.implode(',&nbsp;',$not_yet_signed_names).'</p>';
							$x .= '<p class="spacer"></p>';
						}
						
					if($allow_replyslip && $reply_slip_id > 0 && $displyReleaseStatistic && !$SelfFeedback){
						if($reply_slip_type == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
							$libTableReplySlipMgr->setReplySlipId($reply_slip_id);
							$display_reply_slip_stat = $libTableReplySlipMgr->returnReplySlipStatisticsHtml(true);
						} else {	    
							$libReplySlipMgr->setReplySlipId($reply_slip_id);
							$display_reply_slip_stat = $libReplySlipMgr->returnReplySlipStatisticsHtml(true);
						}
						// remove the export button
						$striptag_start = stripos($display_reply_slip_stat,'<div class="Conntent_tool">',0);
						if($striptag_start !== false){
							$striptag_end = stripos($display_reply_slip_stat,'</div>',$striptag_start);
							$striptag_str = substr($display_reply_slip_stat,$striptag_start,$striptag_end);
							if($striptag_str != ''){
								$display_reply_slip_stat = str_replace($striptag_str,'',$display_reply_slip_stat);
							}
						}
						
						$x .= '<br />';
						$x .= '<table class="common_table_list_v30  view_table_list_v30" style="width:99%" align="center" border="0" cellspacing="0" cellpadding="4">
								<thead>
									<tr><th>'.$Lang['DocRouting']['ReplySlipStatistics'].'</th></tr>
								</thead>
								<tbody>
									<tr><td>';
									
						$x .= $display_reply_slip_stat;
						
						$x .= '	 </td></tr>
								</tbody>
							</table>
							<p class="spacer"></p>';
					}		
							
				}	// end of route	
				
							
					 $x .= '<p class="spacer"></p>
							<br /><br />
							</td>
						</tr>
					</tbody>
				  </table>';	
			
			return $x;
		}
		
		/*
		 * Get a complete html document with all attachments embedded(data uri, work with modern browsers, except IE) from url /home/eAdmin/ResourcesMgmt/DocRouting/management/print_document_detail.php
		 */
		function getArchivedDocument($DocumentID,$IsArchive=true,$SelfFeedback=false,$FilterRouteId='',$HtmlContentOnly=false)
		{
			global $Lang, $intranet_root, $PATH_WRT_ROOT, $indexVar, $docRoutingConfig, $replySlipConfig, $tableReplySlipConfig, $intranet_session_language;
			
			//$ldocrouting_ui = $indexVar['libDocRouting_ui'];
			$ldocrouting = $indexVar['libDocRouting'];
			
			$docAry = $ldocrouting->getEntryData($DocumentID);
			
			$title = $docAry[0]['Title'];
			$title_for_doc_file = str_replace(array('\\','/','<','>','?',':','*','"','|'),'',$title);
			
			$doc_attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'],array($DocumentID));
			$routeAry = $ldocrouting->getRouteData(array($DocumentID));
			$routeIdAry = Get_Array_By_Key($routeAry,'RouteID');
			$feedbackAry = $ldocrouting->getRouteUserFeedbacks($routeIdAry);
			$feedbackIdAry = Get_Array_By_Key($feedbackAry,'FeedbackID');
			$feedback_attachments = $ldocrouting->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'],$feedbackIdAry);
			
			//$attachAry = array(); // 0=> id, 1=> final file name, 2=> tmp full file path
			$rawAttachAry = array(); // 0=> id, 1=> final file name, 2=> full file path
			for($i=0;$i<count($doc_attachments);$i++) 
			{
				$filePath = str_replace($docRoutingConfig['dbFilePath'],"",$doc_attachments[$i]['FilePath']);
				$fileName = $doc_attachments[$i]['FileName'];
				$fullpath = $docRoutingConfig['filePath'].$filePath;
				
				$rawAttachAry[] = array($doc_attachments[$i]['FileID'],$fileName,$fullpath);
			}
			for($i=0;$i<count($feedback_attachments);$i++) 
			{
				$filePath = str_replace($docRoutingConfig['dbFilePath'],"",$feedback_attachments[$i]['FilePath']);
				$fileName = $feedback_attachments[$i]['FileName']; 
				$fullpath = $docRoutingConfig['filePath'].$filePath;
				
				$rawAttachAry[] = array($feedback_attachments[$i]['FileID'],$fileName,$fullpath);
			}
			
			$http = $_SERVER['SERVER_PORT'] == '443'? "https" : "http"; 
			$server_link = "$http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
			
			$params = array('DocumentID'=>$DocumentID,'IsArchive'=>($IsArchive?1:0),'SelfFeedback'=>($SelfFeedback?1:0),'FilterRouteId'=>$FilterRouteId,'HtmlContentOnly'=>($HtmlContentOnly?1:0));
			$pe = $ldocrouting->getEncryptedParameter('management','print_document_detail',$params);
			
			$doc_url = $server_link.'/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe='.$pe.($indexVar['tok']!=''?'&tok='.$indexVar['tok']:'');
			
			$html_content = getHtmlByUrl($doc_url);
			
			for($i=0;$i<count($rawAttachAry);$i++){
				$mime_type = mime_content_type($rawAttachAry[$i][2]);
				$base64_data = base64_encode(file_get_contents($rawAttachAry[$i][2]));
				$data_uri = "data:$mime_type;base64,".$base64_data;
				
				$html_content = str_replace('###DR_FILE_'.$rawAttachAry[$i][0].'###',$data_uri,$html_content);
			}
			
			return array('Title'=>$title_for_doc_file,'Content'=>$html_content);
		}
		
	}
}
?>