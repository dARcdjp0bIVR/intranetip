<?php
// editing by 
 
/********************
 * 2019-04-30 Henry: security issue fix for SQL injection
 * 2018-03-28 Carlos: modified getRouteData(), added optional parameter $recordStatus.
 * 					  modified getModuleObjArr(), added [Deleted Routings] page.
 * 2017-07-11 Anna: added $AuthenticationSetting and allow access ip setting
 * 2017-05-16 Carlos: $sys_custom['DocRouting']['RequestNumber'] - modified getUserDREntry(), search DocumentID for request number.
 * 					  $sys_custom['DocRouting']['DeleteFeedback'] - added deleteFeedback($feedbackId).
 * 2016-02-19 Carlos: [ip2.5.7.3.1] modified getUserDREntry(), getModuleObjArr(), getRouteTargetUsers(), getUserCurrentInvolvedEntry(), improved for [Followed routings] page.
 * 2016-02-02 Carlos: [ip2.5.7.3.1] modified getUserDREntry(), 1) creator condition include UserLogin; 2) order by star status then entry creation time in descending order.
 * 2015-04-24 Carlos: [ip2.5.6.5.1] modified unsetSignDocumentSession(), added flag $sys_custom['DocRouting']['RequirePasswordToSign'] to handle tmp session.
 * 2015-01-05 Carlos: [ip2.5.6.1.1] modified addDRRoute() and copyRoute(), added data field INTRANET_DR_ROUTES.ReadOnlyUserViewDetail which enable read-only users can view all comments and attachments
 * 2014-12-16 Henry: [ip.2.5.6.1.1] modified addDRRoute() to fix bug of Allow Attachment Read By Others selection
 * 2014-12-16 Ivan: [ip.2.5.6.1.1] modified copyDocument() to add Get_Safe_Sql_Query() handling in the title and description of SQL
 * 2014-12-03 Carlos: [ip2.5.5.12.1] Modified sendNotificationEmailForRouteSigned() attached document to email, added sendArchivedDocumentEmail() for sending archived document via emails
 * 2014-11-20 Carlos: [ip2.5.5.12.1] Modified sendNotificationEmailForRouteSigned(), append the user's signed route feedbacks to the email
 * 2014-11-18 Carlos: [ip2.5.5.12.1] Added sendNotificationEmailForRouteSigned(), getAttachmentDisplaySize()
 * 2014-11-05 Carlos: Added getSignEmailDocument(),getSignEmailDocumentByToken(),getSignEmailDocumentLastSentTime(),getSignEmailRoute(),getRoutesSignCountByToken(),sendSignEmail(),signDocumentRouting()
 * 2014-10-24 Carlos: Modified getUserDREntry(), modified filter conditions as (stared or recordStatus) and (stared or createStatus) and (stared or expireStatus) 
 * 2014-09-17 Carlos: Modified createDocFromModule(), pass in iMail plus Folder name need to be base64 decoded()
 * 2014-05-29 Henry: Add parameter $additionalContent in sendNotificationEmailForNewRouteReceived()
 * 2014-05-13 Carlos: Added getUserFileStarRecords(),addUserFileStarRecord(),deleteUserFileStarRecord(),getUserStarredFiles(). Modified deleteDRAttachment()
 * 2014-02-19 Tiffany: Add addDAFile() for add DA files to DR, modified  updateDocFileVersioning().
 * 2014-01-28 Carlos: Fix getRouteCompleteStatus(), getRouteTargetUsers() only count ROUTE users, excluding VIEW users
 * 2013-11-01 Ivan:	 Modified deleteDREntry(), deleteDRRoute(), deleteDRAttachment(), deleteComment(), deleteRouteCommentDraft() to add delete log
 * 2013-10-03 Henry: Added parameter $advanceSearchArray in getUserDREntry() and modified the sql to support advance search
 * 2013-10-02 Henry: Modified the $rootpath in deleteDRAttachment()
 * 2013-10-02 Henry: Added getLocationSuggestion() and handle_chars()
 * 2013-09-18 Carlos: modified addDRRoute(), update reply slip show/hide user name and release type
 * 2013-06-14 Ivan	: modified getUserDREntry() to add tag name for keyword searching
 * 2013-04-15 Carlos: modified getUserDREntry() add optional parameter $expireStatus
 * 2013-03-01 Carlos: modified copyRoute() and copyReplySlip() - added table type reply slip
 * 2013-02-20 Rita 	: modified getEntryData() add $orderBy
 * 2013-02-08 Rita	: add copyReplySlip()
 * 2013-01-30 Carlos: modified addDRRoute() to handle different type of reply slip
 * 2013-01-23 Rita	: add enablePrintRoutingInstructionRight() for Customization
 * 2013-01-17 Rita	: modified addDRRoute();
 * 2013-01-11 Carlos: add createDocFromModule()
 * 2013-01-08 Carlos: add updateDocFileVersioning(),updateRouteCommentDraft(),getRouteDraftRecords(),
 * 						requestRouteLock(),releaseRouteLock(), copyDocument(), copyRoute()
 * 2013-01-04 Carlos: add deleteComment()
 * 2012-11-28 Carlos: fix addDRRoute() Route owner ID
 * 2012-10-12 Rita: revise getEntryData() add keyword
 * 				   revise getUserDREntry() add keyword, recordstatus, starstatus
 * 				   add returnRoutePeriod(), 
 * 
 ********************/
if (!defined("LIBDOCROUTING_DEFINED")) {
	define("LIBDOCROUTING_DEFINED", true);
	
	class libDocRouting extends libdb {
		var $libfs;
		var $libReplySlipMgr;
		var $libTableReplySlipMgr;
		var $settingAry;
		var $ModuleTitle;
		var $AuthenticationSetting;
		
		function libDocRouting() {
			$this->libdb();
			$this->ModuleTitle = 'DocumentRouting';
			$SettingAry =  $this->returnSettingAry();
			$this->AuthenticationSetting = $SettingAry['AuthenticationSetting'];
		}
		
		function getModuleObjArr() {
			global $PATH_WRT_ROOT, $intranet_root, $image_path, $LAYOUT_SKIN, $CurrentPage, $sys_custom;
			global $plugin, $special_feature, $docRoutingConfig, $indexVar;
			global $intranet_session_language, $Lang;
		
			$allowed_IPs = trim($this->returnSettingValueByName("AllowAccessIP"));
			$ip_addresses = explode("\n", $allowed_IPs);
			checkCurrentIP($ip_addresses, $Lang['DocRouting']['DocRouting']);

			if($this->AuthenticationSetting && $_SESSION['module_session_login']['DocRouting']!=1){
				$x = "";
				$x .= "<form id='form1' name='form1' method='post' action='$PATH_WRT_ROOT/home/module_access_checking.php'>";
				$x .= "<input type='hidden' name='module' value='DocRouting' />";
				$x .= "<input type='hidden' name='module_path' value='".$_SERVER["REQUEST_URI"]."' />";
				$x .= "</form>";
				$x .= "<script language='javascript'>";
				$x .= "document.form1.submit();";
				$x .= "</script>";
				echo $x;
				exit();
			}
			// no need authentication - set session value
			else if(!isset($_SESSION['module_session_login']['DocRouting'])){
				$_SESSION['module_session_login']['DocRouting'] = 1;
			}
			
	        # Current Page Information init
			$PageManagement	= 0;
			$PageSettings = 0;
			
			switch ($CurrentPage) {
				case "Mgmt_CurrentRoutings":
		        case "Mgmt_DraftRoutings":
		        case "Mgmt_CompletedRoutings":
		        case "Mgmt_FollowedRoutings":
		        case "Mgmt_DeletedRoutings":
		        	$PageManagement = 1;
		        	break; 
		        case "Settings_PresetDocInstruction":
		        case "Settings_PresetRoutingNote":
				case "Settings_SystemProperties":
				case "Settings_AllowAccessIP":
					$PageSettings = 1;
		        	break;
		    }
		    
		    $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT.$docRoutingConfig['eAdminPath'];
		    
		    # Management 
			$MenuArr["Management"] = array($Lang['Menu']['AccountMgmt']['Management'], "#", $PageManagement);
			
			// Current Routings
			$parameterText = $this->getEncryptedParameter('management', 'index', array('recordStatus' => array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']), 'clearCoo' => 1));
			$MenuArr["Management"]["Child"]["CurrentRoutings"] = array($Lang['DocRouting']['CurrentRoutings'], '?pe='.$parameterText, ($CurrentPage=='Mgmt_CurrentRoutings'));
			
			// Draft Routings
			$parameterText = $this->getEncryptedParameter('management', 'draft_routings', array('recordStatus' => array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']), 'clearCoo' => 1));
			$MenuArr["Management"]["Child"]["DraftRoutings"] = array($Lang['DocRouting']['DraftRoutings'], '?pe='.$parameterText, ($CurrentPage=='Mgmt_DraftRoutings'));
			
			// Followed Routings
			$parameterText = $this->getEncryptedParameter('management', 'followed_routings', array('recordStatus' => array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']), 'clearCoo' => 1));
			$MenuArr["Management"]["Child"]["FollowedRoutings"] = array($Lang['DocRouting']['FollowedRoutings'], '?pe='.$parameterText, ($CurrentPage=='Mgmt_FollowedRoutings'));
			
			// Completed Routings
			$parameterText = $this->getEncryptedParameter('management', 'completed_routings', array('recordStatus' => array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']), 'clearCoo' => 1));
			$MenuArr["Management"]["Child"]["CompletedRoutings"] = array($Lang['DocRouting']['CompletedRoutings'], '?pe='.$parameterText, ($CurrentPage=='Mgmt_CompletedRoutings'));
			
			if($sys_custom['DocRouting']['ShowDeletedRoutings']){
				// Deleted Routings
				$parameterText = $this->getEncryptedParameter('management', 'deleted_routings', array('recordStatus' => array($docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Deleted']), 'clearCoo' => 1));
				$MenuArr["Management"]["Child"]["DeletedRoutings"] = array($Lang['DocRouting']['DeletedRoutings'], '?pe='.$parameterText, ($CurrentPage=='Mgmt_DeletedRoutings'));
			}
			
		    # Settings 
			$MenuArr["Settings"] = array($Lang['Menu']['AccountMgmt']['Settings'], "#", $PageSettings);
			
			// System Properties
			if ($this->returnIsAdminUser($indexVar['drUserId'])) {
				$parameterText = $this->getEncryptedParameter('settings/system_properties', 'index');
				$MenuArr["Settings"]["Child"]["SystemProperties"] = array($Lang['General']['SystemProperties'], '?pe='.$parameterText, ($CurrentPage=='Settings_SystemProperties'));
			}
			
			// Preset Document Instruction
			$parameterText = $this->getEncryptedParameter('settings/preset_doc_instruction', 'index', array('clearCoo'=>1));
			$MenuArr["Settings"]["Child"]["PresetDocInstruction"] = array($Lang['DocRouting']['PresetDocInstruction'], '?pe='.$parameterText, ($CurrentPage=='Settings_PresetDocInstruction'));
			
			// Preset Routing Note
			$parameterText = $this->getEncryptedParameter('settings/preset_routing_note', 'index', array('clearCoo'=>1));
			$MenuArr["Settings"]["Child"]["PresetRoutingNote"] = array($Lang['DocRouting']['PresetRoutingInstruction'], '?pe='.$parameterText, ($CurrentPage=='Settings_PresetRoutingNote'));
			
			//Allow Access IP
			if ($this->returnIsAdminUser($indexVar['drUserId'])) {
				$parameterText = $this->getEncryptedParameter('settings/allow_access_ip', 'index', array('clearCoo'=>1));
				$MenuArr["Settings"]["Child"]["AllowAccessIP"] = array($Lang['DocRouting']['AllowAccessIP'], '?pe='.$parameterText, ($CurrentPage=='Settings_AllowAccessIP'));
			}

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass Digital Routing";'."\n";
            $js.= '</script>'."\n";
			
			### module information
	        $MODULE_OBJ['title'] = $Lang['Header']['Menu']['DocRouting'].$js;
	        $MODULE_OBJ['title_css'] = "menu_opened";
	        $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_doc_routing.png";
	        $MODULE_OBJ['menu'] = $MenuArr;
	        
	        return $MODULE_OBJ;
		}
		
		function getEncryptedParameter($path, $fileName, $variableAry='') {
			global $docRoutingConfig;
			
			//p=test/||index||z-1/y-2012-08-09/		=>		test/index.php?z=1&y=2012-08-09
			
			$variableTextAry = array();
			foreach ((array)$variableAry as $_variableName => $_variableValue) {
				$variableTextAry[] = $_variableName.$docRoutingConfig['urlVariableKeyValueSeparator'].$_variableValue;
			}
			$variableText = implode($docRoutingConfig['urlVariableSeparator'], $variableTextAry);
			$parameterText = $path.$docRoutingConfig['urlPathParamSeparator'].$fileName.$docRoutingConfig['urlPathParamSeparator'].$variableText;
			
			return getEncryptedText($parameterText, $docRoutingConfig['encryptKey']);
		}
		
		function returnIsAdminUser($targetUserId) {
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/user_right_target.php');
			$liburt = new user_right_target();
			
			$accessRightAry = $liburt->Load_User_Right($targetUserId);
			return $accessRightAry['eAdmin-DocRouting']? true : false;
		}
		
		function checkAccessRight() {
			global $plugin;
			
			$canAccess = true;
			if (!$plugin['DocRouting']) {
				$canAccess = false;
			}
			
			if (!$canAccess) {
				No_Access_Right_Pop_Up();
			}
		}
		
		function getLibFileSystemInstance() {
			global $PATH_WRT_ROOT;
			
			if ($this->libfs === null) {
				include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
				$this->libfs = new libfilesystem();	
			}
			
			return $this->libfs;
		}
		
		function getLibReplySlipMgrInstance() {
			global $PATH_WRT_ROOT;
			
			if ($this->libReplySlipMgr === null) {
				$this->libReplySlipMgr = new libReplySlipMgr();	
			}
			
			return $this->libReplySlipMgr;
		}
		
		function getLibTableReplySlipMgrInstance() {
			global $PATH_WRT_ROOT;
			
			if ($this->libTableReplySlipMgr === null) {
				$this->libTableReplySlipMgr = new libTableReplySlipMgr();	
			}
			
			return $this->libTableReplySlipMgr;
		}
		
		function updateDREntry($dataAry)
		{
			global $docRoutingConfig, $indexVar;
			
			$inputUserID = IntegerSafe($indexVar['drUserId']);
			$documentID = IntegerSafe($dataAry['DocumentID']);
			$title = $this->Get_Safe_Sql_Query($dataAry['Title']);
			$instruction = $this->Get_Safe_Sql_Query($dataAry['Instruction']);
			$documentType = IntegerSafe($dataAry['DocumentType']);
			$physicalLocation = $this->Get_Safe_Sql_Query($dataAry['PhysicalLocation']);
			$allowAdHocRoute = IntegerSafe($dataAry['AllowAdHocRoute']);
			$recordStatus = IntegerSafe($dataAry['RecordStatus']);
						
			if($documentID != '') {
				$sql = "UPDATE INTRANET_DR_ENTRY SET 
							Title='".$title."',
							Instruction='".$instruction."',
							DocumentType='".$documentType."',
							PhysicalLocation='".$physicalLocation."',
							AllowAdHocRoute='".$allowAdHocRoute."',
							RecordStatus='".$recordStatus."',
							DateModified=NOW(),
							InputBy='$inputUserID' 
						WHERE DocumentID='".$documentID."'";
				 $result = $this->db_db_query($sql);
			}else{
				$sql = "INSERT INTO INTRANET_DR_ENTRY (UserID,Title,Instruction,DocumentType,AllowAdHocRoute,RecordStatus,DateInput,DateModified,InputBy,ModifiedBy) 
						VALUES ('$inputUserID.','$title','$instruction','$documentType','$allowAdHocRoute','$recordStatus',NOW(),NOW(),'$inputUserID','$inputUserID')";
				$result = $this->db_db_query($sql);
				if($result){
					$documentID = $this->db_insert_id();
				}
			}
			
			return $documentID;
		}
		
		function deleteDREntry($documentIdAry) {
			global $docRoutingConfig, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			$liblog = new liblog();
		    
		    $documentIdAry = IntegerSafe($documentIdAry);
		    
		    $routeIdAry = Get_Array_By_Key($this->getRouteData($documentIdAry), 'RouteID');
			
			$successAry = array();
			$successAry['deleteRelatedRoute'] =	$this->deleteDRRoute($routeIdAry);
			
			$sql = "Update INTRANET_DR_ENTRY Set RecordStatus = '".$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Deleted']."', DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' Where DocumentID In ('".implode("','", (array)$documentIdAry)."')";
			$successAry['deleteDocument'] =	$this->db_db_query($sql);
			
			if (!in_array(false, $successAry)) {
				if (!is_array($documentIdAry)) {
					$documentIdAry = array($documentIdAry);
				}
				$numOfDoc = count($documentIdAry);
				for ($i=0; $i<$numOfDoc; $i++) {
					$_documentId = $documentIdAry[$i];
					$successAry['deleteLog'][$_documentId] = $liblog->INSERT_LOG($this->ModuleTitle, 'DeleteDocument', '', 'INTRANET_DR_ENTRY', $_documentId);
				}
			}
			
			return !in_array(false, $successAry);
		}
		// for 1 dimensional array
		function stripWordFromArray($ary, $replace_word)
		{
			$return_ary = array();
			if(count($ary) > 0){
				foreach($ary as $val){
					$new_val = str_replace($replace_word,"",$val);
					$return_ary[] = $new_val;
				}
			}
			return $return_ary;
		}
		
		function addDRRoute($postData, $filesData, $adhocRouting=false)
		{
			global $docRoutingConfig, $indexVar, $replySlipConfig, $tableReplySlipConfig;
			
			$libReplySlipMgr = $this->getLibReplySlipMgrInstance();
			$libTableReplySlipMgr = $this->getLibTableReplySlipMgrInstance();
			
			$creatorID = IntegerSafe($indexVar['drUserId']);
			$documentID = IntegerSafe($postData['DocumentID']);
			$routeCounters = $postData['routeCounter'];
			$toRemoveRouteID = $postData['ToRemoveRouteID'];
			
			
			
			//$routeCount = count($postData['routeCounter']);
			/*
			$startRoutingNumber = 1;
			$endRoutingNumber = $routeCount;
			if($adhocRouting){
				$startRoutingNumber = $postData['DisplayOrder'];
				$endRoutingNumber = $postData['DisplayOrder'];
			}
			*/
			$result = array();
			if($adhocRouting){
				$order = $routeCounters[0];
			}else{
				$order = 1;
			}
			if(count($toRemoveRouteID)>0){
				$result['removeRoutes'] = $this->deleteDRRoute($toRemoveRouteID);
			}
			//for($i=$startRoutingNumber;$i<=$endRoutingNumber;$i++){
			foreach($routeCounters as $i){
				$routeID = IntegerSafe($postData['RouteID_'.$i]);
				$usersAry = $postData['users_'.$i]; // UserID
				$extraUsersAry = $postData['extraUsers_'.$i]; // 
				$note = $this->Get_Safe_Sql_Query($postData['Note_'.$i]); // Note
				$actionAllow = $postData['ActionAllow_'.$i]; // ActionType
				$effectiveType = $postData['EffectiveType_'.$i]; // EffectiveType
				$startDate = $postData['EffectiveStartDate_'.$i]; // EffectiveStartDate;
				$startTime = $postData['EffectiveStartTime_'.$i.'_hour'].':'.$postData['EffectiveStartTime_'.$i.'_min'].':'.$postData['EffectiveStartTime_'.$i.'_sec'];
				$startDatetime = $startDate." ".$startTime;
				$endDate = $postData['EffectiveEndDate_'.$i]; // EffectiveEndDate
				$endTime = $postData['EffectiveEndTime_'.$i.'_hour'].':'.$postData['EffectiveEndTime_'.$i.'_min'].':'.$postData['EffectiveEndTime_'.$i.'_sec'];
				$endDatetime = $endDate." ".$endTime;
				$readOnlyUserViewDetail = $postData['ReadOnlyUserViewDetail_'.$i] == 1? 1: 0; // if 1 Read-only user can view comments and attachments 
				
				// Henry Added [20140523] [start]
				if(in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'],$actionAllow) && $postData['CommentReadByOthers_'.$i] == 0){
					for($j=0;$j<count($actionAllow);$j++){
						if($actionAllow[$j] == $docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment'])
							$actionAllow[$j] = $docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['CommentReadByOwn'];
					}
				}
				if(in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'],$actionAllow) && $postData['AttachmentReadByOthers_'.$i] == 0){
					for($j=0;$j<count($actionAllow);$j++){
						if($actionAllow[$j] == $docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment'])
							$actionAllow[$j] = $docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['AttachmentReadByOwn'];
					}
				}
					
				$submitAfterDeadline = $postData['SubmitAfterDeadline_'.$i];
				// Henry Added [20140523] [end]
				
				//$viewRight = $postData['ViewRight_'.$i]; // ViewRight & INTRANET_DR_ROUTE_TARGET.AccessRight
				if (is_array($extraUsersAry) && count($extraUsersAry) > 0) {
					$viewRight = $docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser'];
				}
				else {
					$viewRight = $docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['TargetUserOnly'];
				}
								
				$replySlipId = $postData['replySlipId_'.$i];
				$replySlipReleaseType = $postData['ReplySlipReleaseType_'.$i][0]; 
				$enableLock = $postData['EnableLock_'.$i];
				$lockDuration = $postData['LockDuration_'.$i];
				$userNameDisplayType = $postData['UserNameDisplayType_'.$i][0]; 
				$replySlipType = $postData['ReplySlipType_'.$i];
				
				$copyReplySlip = $postData['CopyReplySlip_'.$i];
				
				$usersAry = $this->stripWordFromArray($usersAry, "U");
				$extraUsersAry = $this->stripWordFromArray($extraUsersAry,"U");
				
				$newTargetUsersAry = $usersAry;
				
				if($routeID!=''){
					$sql = "UPDATE INTRANET_DR_ROUTES SET Note='$note',EffectiveType='$effectiveType',
								EffectiveStartDate='$startDatetime',EffectiveEndDate='$endDatetime',EnableSubmitAfterEndDate='$submitAfterDeadline',ViewRight='$viewRight',
								EnableLock='$enableLock',LockDuration='$lockDuration',ReadOnlyUserViewDetail='$readOnlyUserViewDetail',DisplayOrder='$order',DateModified=NOW(),ModifiedBy='$creatorID' 
							WHERE RouteID='".$routeID."' ";
							
					$sql_clean_action = "DELETE FROM INTRANET_DR_ROUTE_ACTION WHERE RouteID='$routeID'";
					$result['cleanAction'.$i] = $this->db_db_query($sql_clean_action);
					$sql_clean_users = "DELETE FROM INTRANET_DR_ROUTE_TARGET WHERE RouteID='$routeID' AND AccessRight='".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['View']."'";
					$result['cleanUsers'.$i] = $this->db_db_query($sql_clean_users);
					
					$sql_get_target_users = "SELECT UserID FROM INTRANET_DR_ROUTE_TARGET WHERE RouteID='$routeID' AND AccessRight='".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']."'";
					$existing_users = $this->returnVector($sql_get_target_users);
					$users_to_add = array_unique(array_diff($usersAry,$existing_users)); // find new users to add
					$users_to_delete = array_unique(array_diff($existing_users,$usersAry)); // find users to be removed
					if(count($users_to_delete)>0){
						$sql_delete_targets = "DELETE FROM INTRANET_DR_ROUTE_TARGET WHERE RouteID='$routeID' AND UserID IN (".implode(",",$users_to_delete).")";
						$result['deleteTargetUsers'.$i] = $this->db_db_query($sql_delete_targets);
					}
					$newTargetUsersAry = $users_to_add;
					
				}else{
					$sql = "INSERT INTO INTRANET_DR_ROUTES (DocumentID,UserID,Note,EffectiveType,EffectiveStartDate,EffectiveEndDate,ViewRight,EnableLock,LockDuration,ReadOnlyUserViewDetail,ReplySlipType,DisplayOrder,DateInput,DateModified,InputBy,ModifiedBy) VALUES ";
					$sql.= "('$documentID','$creatorID','$note','$effectiveType','$startDatetime','$endDatetime','$viewRight','$enableLock','$lockDuration','$readOnlyUserViewDetail','$replySlipType','$order',NOW(),NOW(),'$creatorID','$creatorID')";
				}
				$result['InsertUpdateRoute'.$i] = $this->db_db_query($sql);
				if($result['InsertUpdateRoute'.$i]){
					if($routeID == ''){
						$routeID = $this->db_insert_id();
					}
					
					$_routeInfoAry = $this->getRouteData('', $routeID);
					
					$sql = "INSERT INTO INTRANET_DR_ROUTE_ACTION (RouteID,Action,DateInput,InputBy) VALUES";
					$values = "";
					$delimiter = "";
					for($j=0;$j<count($actionAllow);$j++){
						$values .= $delimiter."('$routeID','".$actionAllow[$j]."',NOW(),'$creatorID')";
						$delimiter = ",";
					}
					if($values != ""){
						$sql .= $values;
						$result['InsertAction'.$i] = $this->db_db_query($sql);
					}
					
					if(count($newTargetUsersAry)>0){
						$sql = "INSERT INTO INTRANET_DR_ROUTE_TARGET (RouteID,UserID,AccessRight,RecordStatus,DateInput) VALUES ";
						$values = "";
						$delimiter = "";
						foreach($newTargetUsersAry as $k => $uid){
							$values .= $delimiter."('$routeID','".$uid."','".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']."','".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']."',NOW())";
							$delimiter = ",";
						}
						if($values != ""){
							$sql .= $values;
							$result['InsertTargetUser'.$i] = $this->db_db_query($sql);
						}
					}
					
					if($viewRight == $docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser'] && count($extraUsersAry)>0){
						$sql = "INSERT INTO INTRANET_DR_ROUTE_TARGET (RouteID,UserID,AccessRight,RecordStatus,DateInput) VALUES ";
						$values = "";
						$delimiter = "";
						foreach($extraUsersAry as $k => $uid){
							$values .= $delimiter."('$routeID','$uid','".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['View']."','".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']."',NOW())";
							$delimiter = ",";
						}
						if($values != ""){
							$sql .= $values;
							$result['InsertViewUser'.$i] = $this->db_db_query($sql);
						}
					}
					
					if (in_array($docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip'], $actionAllow)) {
						
						if ($filesData['ReplySlipFile_'.$i]['tmp_name'] != '') {
							if($replySlipType == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
								$libReplySlipMgr_ref = $libTableReplySlipMgr;
							}else {
								$libReplySlipMgr_ref = $libReplySlipMgr;
							}
							// have new attachment => update
							if ($replySlipId != '') {
								// soft delete the reply slip
								$libReplySlipMgr_ref->setReplySlipId($replySlipId);
								$libReplySlipMgr_ref->deleteReplySlipData();
								
								// create a new reply slip for the route
								$libReplySlipMgr_ref->setReplySlipId('');
							}
							
							$libReplySlipMgr_ref->setCsvFilePath($filesData['ReplySlipFile_'.$i]['tmp_name']);
							//$libReplySlipMgr->setReplySlipId($_routeInfoAry[0]['ReplySlipID']);
							$libReplySlipMgr_ref->setCurUserId($indexVar['drUserId']);
							$libReplySlipMgr_ref->setLinkToModule($docRoutingConfig['moduleCode']);
							$libReplySlipMgr_ref->setLinkToType($docRoutingConfig['replySlip']['linkToType']['route']);
							$libReplySlipMgr_ref->setLinkToId($routeID);
							
							if($replySlipType == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
								$libReplySlipMgr_ref->setAnsAllQuestion($tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['AnsAllQuestion']['no']);
								$libReplySlipMgr_ref->setShowUserInfoInResult($userNameDisplayType);
								$libReplySlipMgr_ref->setRecordType($tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordType']['realForm']);
								$libReplySlipMgr_ref->setRecordStatus($tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['RecordStatus']['active']);
							} else {
								$libReplySlipMgr_ref->setShowQuestionNum($replySlipConfig['INTRANET_REPLY_SLIP']['ShowQuestionNum']['hide']);
								$libReplySlipMgr_ref->setAnsAllQuestion($replySlipConfig['INTRANET_REPLY_SLIP']['AnsAllQuestion']['no']);
								$libReplySlipMgr_ref->setShowUserInfoInResult($userNameDisplayType);
								$libReplySlipMgr_ref->setRecordType($replySlipConfig['INTRANET_REPLY_SLIP']['RecordType']['realForm']);
								$libReplySlipMgr_ref->setRecordStatus($replySlipConfig['INTRANET_REPLY_SLIP']['RecordStatus']['active']);
							}
							
							if ($libReplySlipMgr_ref->isCsvFileValid()) {
								$result['InsertReplySlip'.$i] = $libReplySlipMgr_ref->saveCsvDataToDb();
								$replySlipId = $libReplySlipMgr_ref->getReplySlipId();
								
								$sql = "Update 
											INTRANET_DR_ROUTES 
										Set 
											ReplySlipID = '".$replySlipId."', 
											ReplySlipReleaseType =  '".$replySlipReleaseType."' 
										Where RouteID = '".$routeID."'";
								$result['UpdateRouteReplySlipRelationship'.$i] = $this->db_db_query($sql);
							}
							else {
								$result['InsertReplySlip'.$i] = false;
							}
						}
						elseif($filesData['ReplySlipFile_'.$i]['tmp_name'] == '')
						{
							if($copyReplySlip!=''){
								$this->copyReplySlip($routeID, $copyReplySlip);
							}
							else if($replySlipId != ''){
								// no attachment => do nth to remain the old reply slip
								
								if ($replySlipReleaseType=='') {
									
								}
								else {
									# Update Reply Slip Release Type Only
									$sql = "Update 
													INTRANET_DR_ROUTES 
												Set 
													ReplySlipReleaseType = '".$replySlipReleaseType."' 
												Where RouteID = '".$routeID."' ";
									$result['UpdateRouteReplySlipRelationship'.$i] = $this->db_db_query($sql);
									
									# Update Reply Slip > Show/Hide User Name Option
									if($replySlipType == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
										$libReplySlipMgr_ref = $libTableReplySlipMgr;
									}else {
										$libReplySlipMgr_ref = $libReplySlipMgr;
									}
									$libReplySlipMgr_ref->setReplySlipId($replySlipId);
									$libReplySlipMgr_ref->updateShowUserInfoInResult($userNameDisplayType);
								}
							}
						}	
						
					}
					else {
						$sql = "Update INTRANET_DR_ROUTES Set ReplySlipID = null Where RouteID = '".$routeID."'";
						$result['CancelRouteReplySlipRelationship'.$i] = $this->db_db_query($sql);
					}
				}
				$order+=1;
			}
						
			return !in_array(false,$result);
		}
		
		function deleteDRRoute($routeIdAry) {
			global $docRoutingConfig, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			$liblog = new liblog();
			$successAry = array();
			
			$routeIdAry = IntegerSafe($routeIdAry);
			
			$sql = "Update INTRANET_DR_ROUTES set RecordStatus = '".$docRoutingConfig['INTRANET_DR_ROUTES']['RecordStatus']['Deleted']."', DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' Where RouteID In ('".implode("','", (array)$routeIdAry)."')";
			$successAry['deleteRoute'] = $this->db_db_query($sql);
			
			if ($successAry['deleteRoute']) {
				if (!is_array($routeIdAry)) {
					$routeIdAry = array($routeIdAry);
				}
				$numOfRoute = count($routeIdAry);
				for ($i=0; $i<$numOfRoute; $i++) {
					$_routeId = $routeIdAry[$i];
					$successAry['deleteLog'][$_routeId] = $liblog->INSERT_LOG($this->ModuleTitle, 'DeleteRoute', '', 'INTRANET_DR_ROUTES', $_routeId);
				}
			}
			 
			return !in_array(false, $successAry);
		}
		
		function addFile($linkToType, $linkToId, $fileAry, $fileType=1) {
			global $PATH_WRT_ROOT, $indexVar, $docRoutingConfig;
			
			include_once($PATH_WRT_ROOT.'includes/DocRouting/libDocRouting_file.php');
			$libfs = $this->getLibFileSystemInstance();
			
			$drFilePath = '';
			$drFilePath .= $docRoutingConfig['filePath'];
			if (!file_exists($drFilePath)) {
				$successAry['createRootFolder'] = $libfs->folder_new($drFilePath);
			}
			
			$successAry = array();
			foreach((array)$fileAry as $_fileKey => $_fileInfoAry) {
				$_fileName = stripslashes($_fileInfoAry['name']);
				$_filePath = $_fileInfoAry['tmp_name'];
				
				if($_fileName == '') {
					continue;
				}
				
				$_fileSize = filesize($_filePath);
				
				$_fileObj = new libDocRouting_file();
				$_fileObj->setFilePath('temp');
				$_fileObj->setFileTmpPath($_filePath);
				$_fileObj->setFileName($_fileName);
				$_fileObj->setLinkToType($linkToType);
				$_fileObj->setLinkToId($linkToId);
				$_fileObj->setSizeInBytes($_fileSize);
				
				$successAry[$_fileKey]['insertDbRecord'] = $_fileObj->save();
				if ($successAry[$_fileKey]['insertDbRecord']) {
					$_fileId = $_fileObj->getFileId();
					$_fileName = $_fileObj->getFileName();
					$_fileTmpPath = $_fileObj->getFileTmpPath();
					$_folderDivision = $_fileObj->returnFolderDivisionNum();
					
					$_docFilePath = '';
					$_docFilePath .= $drFilePath.'/'.$_folderDivision;
					if (!file_exists($_docFilePath)) {
						$successAry['createDivisionFolder'] = $libfs->folder_new($_docFilePath);
					}
					
					$_fileExt = get_file_ext($_fileName);
					$_targetFileName = $_fileId.$_fileExt;
					$_docFilePath .= '/'.$_targetFileName;
					$successAry[$_fileKey]['copyFileToDivisionFolder'] = $libfs->lfs_copy($_fileTmpPath, $_docFilePath);
					
					$_dbFilePath = $docRoutingConfig['dbFilePath'].'/'.$_folderDivision.'/'.$_targetFileName;
					$_fileObj->setFilePath($_dbFilePath);
					$_fileId = $_fileObj->save();
					if($fileType != 1){
						$sql = "UPDATE INTRANET_DR_FILE SET RecordType='".$fileType."' WHERE FileID='".$_fileId."'";
						$this->db_db_query($sql);
					}
					
					$successAry[$_fileKey]['updateDbFilePath'] = ($_fileId > 0)? true : false;
				}
			}
			
			return in_multi_array(false, $successAry)? false : true;
		}		
		
		function addDAFile($chosen_file,$documentID,$DA,$fileType=1){
		    global $PATH_WRT_ROOT, $indexVar, $docRoutingConfig;
		    include_once($PATH_WRT_ROOT.'includes/DocRouting/libDocRouting_file.php');
		
            $FolderPath = $PATH_WRT_ROOT."../intranetdata/digital_archive/admin_doc/";
            for($i=0;$i<sizeof($chosen_file);$i++){
                 
	              $sql = "SELECT Title, FileFolderName, CONCAT('$FolderPath', FileHashName),SizeInBytes FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD WHERE DocumentID = '$chosen_file[$i]'";
	              $array =  $this->returnArray($sql);                             
                  list($title, $name, $path, $size)=$array[0];
                                   
		 		
			$libfs = $this->getLibFileSystemInstance();
			
			$drFilePath = '';
			$drFilePath .= $docRoutingConfig['filePath'];
			if (!file_exists($drFilePath)) {
				$successAry['createRootFolder'] = $libfs->folder_new($drFilePath);
			}
			
			$successAry = array();
			
		
				$_fileObj = new libDocRouting_file();
				$_fileObj->setFilePath('temp');
				$_fileObj->setFileTmpPath($path);
				$_fileObj->setFileName($name);
				$_fileObj->setLinkToType($DA);
				$_fileObj->setLinkToId($documentID);
				$_fileObj->setSizeInBytes($size);
				
				$successAry[$chosen_file[$i]]['insertDbRecord'] = $_fileObj->save();
				if ($successAry[$chosen_file[$i]]['insertDbRecord']) {
					$_fileId = $_fileObj->getFileId();
					$_fileName = $_fileObj->getFileName();
					$_fileTmpPath = $_fileObj->getFileTmpPath();
					$_folderDivision = $_fileObj->returnFolderDivisionNum();
					
					$_docFilePath = '';
					$_docFilePath .= $drFilePath.'/'.$_folderDivision;
					if (!file_exists($_docFilePath)) {
						$successAry['createDivisionFolder'] = $libfs->folder_new($_docFilePath);
					}
					
					$_fileExt = get_file_ext($_fileName);
					$_targetFileName = $_fileId.$_fileExt;
					$_docFilePath .= '/'.$_targetFileName;
					$successAry[$chosen_file[$i]]['copyFileToDivisionFolder'] = $libfs->lfs_copy($_fileTmpPath, $_docFilePath);
					
					$_dbFilePath = $docRoutingConfig['dbFilePath'].'/'.$_folderDivision.'/'.$_targetFileName;
					$_fileObj->setFilePath($_dbFilePath);
					$_fileId = $_fileObj->save();
					if($fileType != 1){
						$sql = "UPDATE INTRANET_DR_FILE SET RecordType='".$fileType."' WHERE FileID='".$_fileId."'";
						$this->db_db_query($sql);
					}
					
					$successAry[$chosen_file[$i]]['updateDbFilePath'] = ($_fileId > 0)? true : false;
				}
			 }
            
			return in_multi_array(false, $successAry)? false : true;
		
		}

		function saveEntryTag($documentId, $tagNameText) {
			global $docRoutingConfig;
			
			$successAry = array();
			$tagNameText = trim($tagNameText);
			
			// will auto create tag records if the tag does not exist
			$targetTagIdAry = returnModuleTagIDByTagName($tagNameText, $docRoutingConfig['moduleCode']);
			$numOfTargetTag = count($targetTagIdAry);
			
			### Delete records of removed tags
			$originalTagIdAry = Get_Array_By_Key($this->getEntryTag($documentId), 'TagID');
			$removedTagIdAry = array_values(array_diff((array)$originalTagIdAry, (array)$targetTagIdAry));
			if (count($removedTagIdAry) > 0) {
				$successAry['deleteDocumentTag'] = $this->deleteEntryTagDbRecord($documentId, $removedTagIdAry);
			}
			
			### Insert new tags
			$newTagIdAry = array();
			for ($i=0; $i<$numOfTargetTag; $i++) {
				$_tagId = $targetTagIdAry[$i];
				
				$_tagDataAry = $this->getEntryTag($documentId, $_tagId);
				if (count($_tagDataAry) == 0) {
					$newTagIdAry[] = $_tagId;
				}
			}
			if (count($newTagIdAry) > 0) {
				$successAry['insertDocumentTag'] = $this->insertEntryTagDbRecord($documentId, $newTagIdAry);
			}
			
			return !in_multi_array(false, $successAry);
		}
		
		function getEntryTag($documentIdAry='', $tagIdAry='') {
			if ($documentIdAry !== '') {
				$documentIdAry = IntegerSafe($documentIdAry);
				$condsDocumentId = " And DocumentID In ('".implode("','", (array)$documentIdAry)."') ";
			}
			if ($tagIdAry !== '') {
				$tagIdAry = IntegerSafe($tagIdAry);
				$condsTagId = " And TagID In ('".implode("','", (array)$tagIdAry)."') ";
			}
			
			$sql = "Select DocumentID, TagID from INTRANET_DR_TAG Where 1 $condsDocumentId $condsTagId";
			return $this->returnResultSet($sql); 
		}
		
		function getEntryTagName($documentId)
		{
			global $docRoutingConfig;
			$documentId = IntegerSafe($documentId);
			$sql = "SELECT TagID FROM INTRANET_DR_TAG WHERE DocumentID='$documentId' ";
			$tagIDs =  $this->returnVector($sql);
			
			$tagNameAry = array();
			if(count($tagIDs)>0){
				$tagNameAry = returnModuleTagNameByTagID(implode(",",$tagIDs), $docRoutingConfig['moduleCode']);
			}
			return $tagNameAry;
		}
		
		function deleteEntryTagDbRecord($documentId, $tagIdAry) {
			global $docRoutingConfig;
			
			if (count((array)$tagIdAry) == 0) {
				return false;
			}
			else {
				$documentId = IntegerSafe($documentId);
				$tagIdAry = IntegerSafe($tagIdAry);
				
				$successAry = array();
				$sql = "Delete From INTRANET_DR_TAG Where DocumentID = '".$documentId."' And TagID In ('".implode("','", (array)$tagIdAry)."')";
				$successAry['deleteDocumentTag'] = $this->db_db_query($sql);
				
				$sql = "Select TagID, Count(*) as NumOfDocument From INTRANET_DR_TAG Where TagID In ('".implode("','", (array)$tagIdAry)."') Group By TagID";
				$tagCountAssoAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'TagID', array('NumOfDocument'), $SingleValue=1);
				
				$numOfTag = count((array)$tagIdAry);
				for ($i=0; $i<$numOfTag; $i++) {
					$_tagId = $tagIdAry[$i];
					$_numOfDocument = $tagCountAssoAry[$_tagId];
					
					if ($_numOfDocument == 0 || $_numOfDocument == '') {
						removeTagModuleRelation($_tagId, $docRoutingConfig['moduleCode']);
					}
				}
				
				return !in_array(false, $successAry);
			}
		}
		
		function insertEntryTagDbRecord($documentId, $tagIdAry) {
			global $indexVar;
			
			$documentId = IntegerSafe($documentId);
			$tagIdAry = IntegerSafe($tagIdAry);
			
			$numOfTag = count((array)$tagIdAry);
			for ($i=0; $i<$numOfTag; $i++) {
				$_tagId = $tagIdAry[$i];
				
				$insertValueAry[] = " ('".$documentId."', '".$_tagId."', now(), '".$indexVar['drUserId']."') ";
			}
			
			$success = false;
			if (count($insertValueAry) > 0) {
				$sql = "Insert Into INTRANET_DR_TAG (DocumentID, TagID, DateInput, InputBy)
							Values ".implode(',', (array)$insertValueAry);
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		function getDRAttachment($LinkToType, $LinkToIDAry, $FileIDAry='', $GetUserStarredFiles=false)
		{
			global $PATH_WRT_ROOT, $indexVar, $docRoutingConfig;
			
			$LinkToType = $this->Get_Safe_Sql_Query($LinkToType);
			$LinkToIDAry = IntegerSafe($LinkToIDAry);
			$FileIDAry = IntegerSafe($FileIDAry);
			
			if($FileIDAry != ''){
				$cond = " AND f.FileID IN (".implode(",",(array($FileIDAry))).") ";
			}
			if($GetUserStarredFiles){
				$sql = "SELECT f.* FROM INTRANET_DR_FILE as f INNER JOIN INTRANET_DR_FILE_STAR as s ON s.FileID=f.FileID AND s.UserID='".$indexVar['drUserId']."' WHERE f.LinkToType='".$LinkToType."' AND f.LinkToID In ('".implode("','", (array)$LinkToIDAry)."') AND s.UserID='".$indexVar['drUserId']."' $cond ORDER BY f.FileName,f.VersionNumber";
			}else{
				$sql = "SELECT f.* FROM INTRANET_DR_FILE as f WHERE f.LinkToType='".$LinkToType."' AND f.LinkToID In ('".implode("','", (array)$LinkToIDAry)."') $cond ORDER BY f.FileName,f.VersionNumber";
			}
			return $this->returnResultSet($sql);
		}
		
//		function getUserDREntry($targetUserId, $involvedOnly=true) {
//			if ($involvedOnly) {
//				$condsInvolvedOnly = " And (	
//												e.UserID = '".$targetUserId."'
//												Or r.UserID = '".$targetUserId."' 
//											)";
//			}
//			
//			$sql = "Select
//							e.DocumentID,
//							IF (star.UserID != NULL, 1, 2) as StarSequence
//					From
//							INTRANET_DR_ENTRY as e
//							Inner Join INTRANET_DR_ROUTES as r On (e.DocumentID = r.DocumentID)
//							Left Outer Join INTRANET_DR_STAR as star On (e.DocumentID = star.DocumentID And star.UserID = '".$targetUserId."')
//					Where
//							1
//							$condsInvolvedOnly
//					Order By
//							StarSequence, e.Title
//					";
//			return $this->returnResultSet($sql);
//		}
		
		function getUserDREntry($targetUserId, $involvedOnly=true, $recordStatus='', $keyword='', $starStatus='', $createStatus='', $selectedTagID='', $inputByAry='', $expireStatus='', $advanceSearchArray='', $selectedAcademicYear='') {
			global $indexVar, $docRoutingConfig, $PATH_WRT_ROOT, $sys_custom;
			
			$targetUserId = IntegerSafe($targetUserId);
			$recordStatus = IntegerSafe($recordStatus);
			$starStatus = IntegerSafe($starStatus);
			$createStatus = IntegerSafe($createStatus);
			$selectedTagID = IntegerSafe($selectedTagID);
			
			$or_cond_ary = array();
			//$creator_namefield = getNameFieldByLang("u.");
			if ($involvedOnly) {
				$condsInvolvedOnly = " And (	
												e.UserID = '".$targetUserId."'
												Or r.UserID = '".$targetUserId."'
												Or idrt.UserID = '".$targetUserId."' 
											)";
			}
			
			if ($recordStatus != '') {
				$cond_status = " AND e.RecordStatus IN ('".implode("','",(array)$recordStatus)."') ";
				//$or_cond_ary[] = " AND (star.UserID IS NOT NULL OR e.RecordStatus IN ('".implode("','",(array)$recordStatus)."')) ";
			}
			
//			if ($keyword != ''){
//				
//				$cond_keyword = " 	And 
//									(
//										e.Title Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
//										Or
//										$creator_namefield Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
//									)
//								";		
//			}

			if (is_array($advanceSearchArray) && $advanceSearchArray['flag'] != 1 && $keyword != ''){
				
				$cond_keyword = " 	And 
									(
										e.Title Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or
										tag.TagName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%' 
										".($sys_custom['DocRouting']['RequestNumber']?" OR e.DocumentID='".$this->Get_Safe_Sql_Like_Query($keyword)."' ":"")."
									)
								";		
			}
			
			
			if ($starStatus != ''){				
				$cond_starStatus = "AND IF (star.UserID IS NOT NULL, 1, 2) IN ('".implode("','",(array)$starStatus)."')  ";
				$or_cond_ary[] = " AND (star.UserID IS NOT NULL OR IF (star.UserID IS NOT NULL, 1, 2) IN ('".implode("','",(array)$starStatus)."')) ";				
			}
				
			if ($createStatus != ''){			
				if ($createStatus == $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['CreatedByYou'] ){	
					$cond_createStatus = " AND e.InputBy = '".$targetUserId."' ";
					$or_cond_ary[] = " AND (star.UserID IS NOT NULL OR e.InputBy = '".$targetUserId."') ";	
				}
				elseif($createStatus == $docRoutingConfig['INTRANET_DR_ROUTE_STATUS']['FilterStatus']['NotCreatedByYou']){
					$cond_createStatus = " AND e.InputBy != '".$targetUserId."' ";
					$or_cond_ary[] = " AND (star.UserID IS NOT NULL OR e.InputBy != '".$targetUserId."') ";			
				}
				else{				
					$cond_createStatus = "";
				}
			}
			
			if($selectedTagID !=''){
				$cond_selectedTag = " AND idt.TagID = '".$selectedTagID."' ";		
			}
			
			if ($inputByAry != '') {
				$cond_inputBy = " AND e.InputBy IN ('".implode("','", (array)$inputByAry)."') ";
			}
			
			if($expireStatus != '') {
				$cond_expire = " AND e.RecordStatus='".$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']."' AND r.EffectiveType='".$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']."' AND r.EffectiveEndDate < NOW() " ;
				$or_cond_ary[] = " AND (star.UserID IS NOT NULL OR (e.RecordStatus='".$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']."' AND r.EffectiveType='".$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']."' AND r.EffectiveEndDate < NOW())) " ;
			}
			
			//Henry Added
			if(is_array($advanceSearchArray) && $advanceSearchArray['flag'] == 1 && $advanceSearchArray['routingTitle'] != '') {
				$cond_routingTitle = " 	And 
									(
										e.Title Like '%".$this->Get_Safe_Sql_Like_Query($advanceSearchArray['routingTitle'])."%' 
									)
								";
			}
			if(is_array($advanceSearchArray) &&  $advanceSearchArray['flag'] == 1 && $advanceSearchArray['byDateRange'] != '') {
//				if($advanceSearchArray['StartDate'] != '') {
//				}
//				if($advanceSearchArray['EndDate'] != '') {
//				}
				$cond_StartDate = " 	And 
									(
										LEFT(r.EffectiveStartDate,10) >= '".$advanceSearchArray['StartDate']."' AND LEFT(r.EffectiveStartDate,10) <= '".$advanceSearchArray['EndDate']."'
									)
								";
			}
			
			if(is_array($advanceSearchArray) && $advanceSearchArray['flag'] == 1 && $advanceSearchArray['byDateRange2'] != '') {
//				if($advanceSearchArray['StartDate'] != '') {
//				}
//				if($advanceSearchArray['EndDate'] != '') {
//				}
				$cond_EndDate = " 	And 
									(
										LEFT(r.EffectiveEndDate,10) >= '".$advanceSearchArray['StartDate2']."' AND LEFT(r.EffectiveEndDate,10) <= '".$advanceSearchArray['EndDate2']."'
									)
								";
			}
			
			if(is_array($advanceSearchArray) &&  $advanceSearchArray['flag'] == 1 && $advanceSearchArray['createdBy'] != '') {
				$cond_createdBy = " 	And 
									(
										u.EnglishName Like '%".$this->Get_Safe_Sql_Like_Query($advanceSearchArray['createdBy'])."%'
										OR
										u.ChineseName Like '%".$this->Get_Safe_Sql_Like_Query($advanceSearchArray['createdBy'])."%'
										OR u.UserLogin LIKE '%".$this->Get_Safe_Sql_Like_Query($advanceSearchArray['createdBy'])."%' 
									)
								";
			}
			if(is_array($advanceSearchArray) && $advanceSearchArray['flag'] == 1 && $advanceSearchArray['tag'] != '') {
				$cond_tag = " 	And 
									(
										tag.TagName Like '%".$this->Get_Safe_Sql_Like_Query($advanceSearchArray['tag'])."%'
									)
								";
			}
			if(is_array($advanceSearchArray) && $advanceSearchArray['flag'] == 1 && ($advanceSearchArray['academic_year'] != '' && $advanceSearchArray['academic_year'] != '-1')) {
				//The school year
				include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");
				$lfcm = new academic_year($advanceSearchArray['academic_year']);
				$cond_academicYear = " 	And 
									(
										LEFT(r.EffectiveStartDate,10) >= '".$lfcm->AcademicYearStart."' AND LEFT(r.EffectiveStartDate,10) <= '".$lfcm->AcademicYearEnd."'
									)
								";
			}
			if($selectedAcademicYear!='' && $selectedAcademicYear > 0){
				include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");
				$lfcm2 = new academic_year($advanceSearchArray['academic_year']);
				$cond_academicYearFilter = " AND (e.DateInput BETWEEN '".$lfcm2->AcademicYearStart." 00:00:00' AND '".$lfcm2->AcademicYearEnd." 23:59:59') ";
			}
			
			if(count($or_cond_ary)>0){
				$combined_or_cond = implode("",$or_cond_ary);
			}
			
			$sql = "Select
							e.DocumentID,
							IF (star.UserID IS NOT NULL, 1, 2) as StarSequence
					From
							INTRANET_DR_ENTRY as e
							Inner Join INTRANET_DR_ROUTES as r On (e.DocumentID = r.DocumentID)
							Left Outer Join INTRANET_DR_ROUTE_TARGET idrt ON (r.RouteID = idrt.RouteID And idrt.UserID = '".$targetUserId."')
							Left Outer Join INTRANET_DR_STAR as star On (e.DocumentID = star.DocumentID And star.UserID = '".$targetUserId."')
							Left Outer Join INTRANET_DR_TAG idt On (r.DocumentID = idt.DocumentID)
							Left Outer Join TAG as tag On (idt.TagID = tag.TagID)
							Inner Join INTRANET_USER as u ON (u.UserID = e.UserID)
					Where
							1 
							$cond_status 
							$condsInvolvedOnly 
							$combined_or_cond 
							$cond_keyword
							$cond_selectedTag
							$cond_inputBy 
							$cond_routingTitle
							$cond_StartDate 
							$cond_EndDate
							$cond_createdBy
							$cond_tag
							$cond_academicYear $cond_academicYearFilter
					Group By
							e.DocumentID
					Order By
							StarSequence, e.DateInput desc 
					";
				//debug_pr($sql);
			return $this->returnResultSet($sql);
		}
		
		// return: $involvedEntryAry[$_documentId][$__routeId] = true / false;
		function getUserCurrentInvolvedEntry($targetUserId, $returnIndexedArray=false) {
			global $docRoutingConfig;
			
			$targetUserId = IntegerSafe($targetUserId);
			
			$documentIdAry = Get_Array_By_Key($this->getUserDREntry($targetUserId, $involvedOnly=true, $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']), 'DocumentID');
			$numOfDocument = count($documentIdAry);
			
			$routeInfoAry = $this->getRouteData($documentIdAry);
			$routeIdAry = Get_Array_By_Key($routeInfoAry, 'RouteID');
			$routeInfoAssoAry = BuildMultiKeyAssoc($routeInfoAry, 'DocumentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
						
			$routeTargetUserAssoAry = BuildMultiKeyAssoc($this->getRouteTargetUsers($routeIdAry, $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']), 'RouteID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
						
			$involvedEntryAry = array();
			$indexedInvolvedEntryAry=array();
			for ($i=0; $i<$numOfDocument; $i++) {
				$_documentId = $documentIdAry[$i];
				$_routeInfoAry = (array)$routeInfoAssoAry[$_documentId];
				$_numOfRoute = count($_routeInfoAry);
				
				$_foundPrevRouteFinished = false;
				for ($j=$_numOfRoute-1; $j>=0; $j--) {
					$__routeId = $_routeInfoAry[$j]['RouteID'];
					$__effectiveType = $_routeInfoAry[$j]['EffectiveType'];
					$__effectiveStartDate = $_routeInfoAry[$j]['EffectiveStartDate'];
					$__effectiveEndDate = $_routeInfoAry[$j]['EffectiveEndDate'];
					
					$__routeTargetUserAssoAry = BuildMultiKeyAssoc((array)$routeTargetUserAssoAry[$__routeId], 'UserID');
					$__targetUserRouteStatus = $__routeTargetUserAssoAry[$targetUserId]['RecordStatus'];
					
					// check if user is in this route
					if (isset($__routeTargetUserAssoAry[$targetUserId]) && $__targetUserRouteStatus != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']) {
						// pending records
					}
					else {
						continue;
					}
					
					if ($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']) {
						// route by period => check route date range
						$__effectiveStartDateTs = strtotime($__effectiveStartDate);
						$__effectiveEndDateTs = strtotime($__effectiveEndDate);
						$__curTs = strtotime('now');
						
						if ($__effectiveStartDateTs <= $__curTs && $__curTs <= $__effectiveEndDateTs) {
							$involvedEntryAry[$_documentId][$__routeId] = true;
						}
					}
					else if ($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished']) {
						if (!$_foundPrevRouteFinished) {
							if ($j == 0) {
								// all routes are not finished => should be in first route
								$involvedEntryAry[$_documentId][$__routeId] = true;
							}
							else {
								// check if the previous route are all finished => if so, this is the current route
								$__previousRouteId = $_routeInfoAry[$j-1]['RouteID'];
								$__previousRouteEffectiveType = $_routeInfoAry[$j-1]['EffectiveType'];
								
								if ($__previousRouteEffectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']) {
									// route by period => check route date range
									$__previousRouteEffectiveStartDate = $_routeInfoAry[$j-1]['EffectiveStartDate'];
									$__previousRouteEffectiveEndDate = $_routeInfoAry[$j-1]['EffectiveEndDate'];
					
									$__previousRouteEffectiveEndDateTs = strtotime($__previousRouteEffectiveEndDate);
									$__curTs = strtotime('now');
									
									if ($__previousRouteEffectiveEndDateTs <= $__curTs) {
										// last route period ended
										$involvedEntryAry[$_documentId][$__routeId] = true;
										$_foundPrevRouteFinished = true;
									}
								}
								else {
									$__routeTargetUserStatusAry = Get_Array_By_Key((array)$routeTargetUserAssoAry[$__previousRouteId], 'RecordStatus');
									$__numOfStatus = count($__routeTargetUserStatusAry);
									
									if ($__numOfStatus == 0) {
										$__isAllCompleted = false;
									}
									else{
										$__isAllCompleted = true;
										for ($k=0; $k<$__numOfStatus; $k++) {
											$___recordStatus = $__routeTargetUserStatusAry[$k];
											
											if ($___recordStatus != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']) {
												$__isAllCompleted = false;
												break;
											}
										}
									}
									
									if ($__isAllCompleted) {
										$involvedEntryAry[$_documentId][$__routeId] = true;
										$_foundPrevRouteFinished = true;
									}
								}
							}
						}
					}
				}
				if($returnIndexedArray && isset($involvedEntryAry[$_documentId]) && count($involvedEntryAry[$_documentId])>0){
					$indexedInvolvedEntryAry[] = $_documentId;
				}
			}
			if($returnIndexedArray)
				return $indexedInvolvedEntryAry;
				
			return $involvedEntryAry;
		}
		
		
		function getEntryData($documentIdAry, $recordStatus='', $keyword='', $orderBy='') {
			global $indexVar;		
			
			$documentIdAry = IntegerSafe($documentIdAry);
			
			$creator_namefield = getNameFieldByLang("u.");
			if($recordStatus !== ''){
				$recordStatus = IntegerSafe($recordStatus);
				//$cond = " AND e.RecordStatus='".$recordStatus."' ";
				$cond_recordStatus = " AND e.RecordStatus IN ('".implode("','",(array)$recordStatus)."') ";
			}
					
			
			$cond_keyword = '';
			$keyword = TRIM($keyword);
	        if ($keyword != '')
			{
				$cond_keyword .= " 	And 
									(
										e.Title Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
										Or
										$creator_namefield Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
									)
								";							
			}
			
			
			if($orderBy!=''){
				$cond_orderBy = " Order By e.$orderBy ";
				
			}
			
			$sql = "Select
							e.DocumentID,
							e.UserID,
							e.Title,
							e.Instruction,
							e.DocumentType,
							e.PhysicalLocation,
							e.AllowAdHocRoute, 
							e.RecordStatus, 
							e.DateInput, 
							e.DateModified, 
							e.InputBy,
							e.ModifiedBy,
							$creator_namefield as CreatorName,
							s.DocumentID as Stared 
					From
							INTRANET_DR_ENTRY as e 
							LEFT JOIN INTRANET_USER as u ON u.UserID=e.UserID 
							LEFT JOIN INTRANET_DR_STAR as s ON s.DocumentID=e.DocumentID AND s.UserID='".$indexVar['drUserId']."' 
					Where
							e.DocumentID In ('".implode("','", (array)$documentIdAry)."') 
							$cond_keyword
							$cond_recordStatus

					$cond_orderBy

					";
			return $this->returnResultSet($sql);
		}
		
		function getRouteData($documentIdAry='', $routeIdAry='',$recordStatus='') {
			global $docRoutingConfig;
			
			$recordStatus = IntegerSafe($recordStatus);
			
			if ($documentIdAry !== '') {
				$documentIdAry = IntegerSafe($documentIdAry);
				$condsDocumentId = " And r.DocumentID In ('".implode("','", (array)$documentIdAry)."') ";
			}
			
			if ($routeIdAry !== '') {
				$routeIdAry = IntegerSafe($routeIdAry);
				$condsRouteId = " And r.RouteID In ('".implode("','", (array)$routeIdAry)."') ";
			}
			
			$creator_namefield = getNameFieldByLang("u.");
			
			$sql = "Select
							r.*,
							GROUP_CONCAT(a.Action) as Actions,
							$creator_namefield as CreatorName  
					From 
							INTRANET_DR_ROUTES as r 
							LEFT JOIN INTRANET_USER as u ON u.UserID=r.UserID 
							LEFT JOIN INTRANET_DR_ROUTE_ACTION as a ON a.RouteID=r.RouteID 
					Where 
							r.RecordStatus='".($recordStatus!=''?$recordStatus:$docRoutingConfig['INTRANET_DR_ROUTES']['RecordStatus']['Active'])."' 
							$condsDocumentId 
							$condsRouteId 
					GROUP BY r.RouteID 
					ORDER BY r.DisplayOrder,r.RouteID";
			return $this->returnResultSet($sql);
		}
		
		function getRouteTargetUsers($routeIdAry, $accessRightAry='', $targetUserIdAry='')
		{
			global $docRoutingConfig;
			
			$routeIdAry = IntegerSafe($routeIdAry);
			
			if ($accessRightAry !== '') {
				$accessRightAry = IntegerSafe($accessRightAry);
				$condsAccessRight = " And t.AccessRight IN ('".implode("','", (array)$accessRightAry)."') ";
			}
			
			if($targetUserIdAry != ''){
				$targetUserIdAry = IntegerSafe($targetUserIdAry);
				$condTargetUser = " AND u.UserID IN (".implode(",",(array)$targetUserIdAry).") ";
			}
			
			$name_field = getNameFieldByLang("u.");
			$sql = "SELECT  
						u.UserID,
						$name_field as UserName,
						t.RouteID,
						t.AccessRight,
						t.RecordStatus,
						CASE t.AccessRight
						     WHEN ".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']." THEN 1
						     WHEN ".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['View']." THEN 2
						END as AccessRightOrder,
						r.DocumentID,
						t.DateModified 
					FROM INTRANET_USER as u 
					INNER JOIN INTRANET_DR_ROUTE_TARGET as t ON t.UserID=u.UserID 
					INNER JOIN INTRANET_DR_ROUTES as r ON t.RouteID=r.RouteID
					WHERE t.RouteID IN (".implode(",", (array)$routeIdAry).") $condTargetUser $condsAccessRight 
					ORDER BY t.RouteID, AccessRightOrder, UserName";
			return $this->returnResultSet($sql);
		}
		
		/*
		 * @return: $ary[$documentId][$routeId] = $status
		 * 			
		 * 			$status:
		 * 			$docRoutingConfig['routeStatus']['notStarted'] = 1;
		 *			$docRoutingConfig['routeStatus']['inProgress'] = 2;
		 *			$docRoutingConfig['routeStatus']['completed'] = 3;
		 */
		function getRouteCompleteStatus($documentIdAry) {
			global $docRoutingConfig;
			
			$documentIdAry = (array)$documentIdAry;
			$numOfDocument = count($documentIdAry);
			
			// get all routes of all documents
			$routeInfoAry = $this->getRouteData($documentIdAry);
			$routeIdAry = Get_Array_By_Key($routeInfoAry, 'RouteID');
			$routeInfoAssoAry = BuildMultiKeyAssoc($routeInfoAry, 'DocumentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			// get all users info for all routes
			$routeTargetUserInfoAry = $this->getRouteTargetUsers($routeIdAry, array($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']));
			$routeTargetUserAssoAry = BuildMultiKeyAssoc($routeTargetUserInfoAry, array('DocumentID', 'RouteID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$routeStatusAry = array();
			$previousRouteStatus = '';
			for ($i=0; $i<$numOfDocument; $i++) {
				$_documentId = $documentIdAry[$i];
				
				$_routeInfoAry = (array)$routeInfoAssoAry[$_documentId];
				$_numOfRoute = count($_routeInfoAry);
				for ($j=0; $j<$_numOfRoute; $j++) {
					$__routeId = $_routeInfoAry[$j]['RouteID'];
					$__effectiveType = $_routeInfoAry[$j]['EffectiveType'];
					$__effectiveStartDate = $_routeInfoAry[$j]['EffectiveStartDate'];
					$__effectiveEndDate = $_routeInfoAry[$j]['EffectiveEndDate'];
					
					if ($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']) {
						// route by period => check route date range
						$__effectiveStartDateTs = strtotime($__effectiveStartDate);
						$__effectiveEndDateTs = strtotime($__effectiveEndDate);
						$__curTs = strtotime('now');
						
						if ($__effectiveStartDateTs <= $__curTs && $__curTs <= $__effectiveEndDateTs) {
							$routeStatusAry[$_documentId][$__routeId] = $docRoutingConfig['routeStatus']['inProgress'];
						}
						else if ($__curTs < $__effectiveStartDateTs) {
							$routeStatusAry[$_documentId][$__routeId] = $docRoutingConfig['routeStatus']['notStarted'];
						}
						else if ($__curTs > $__effectiveEndDate) {
							$routeStatusAry[$_documentId][$__routeId] = $docRoutingConfig['routeStatus']['completed'];
						}
					}
					else if ($__effectiveType == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['PrevRouteFinished']) {
						// check if users in this route are all signed or not
						// if all signed => complete
						// if not => check previous route status to determin is this route is "in progress" or "not started"
						$__routeTargetUserAry = $routeTargetUserAssoAry[$_documentId][$__routeId];
						$__routeTargetUserRecordStatusAry = Get_Array_By_Key($__routeTargetUserAry, 'RecordStatus');
						$__numOfStatus = count($__routeTargetUserRecordStatusAry);
						
						if ($__numOfStatus == 0) {
							$__isAllCompleted = false;
						}
						else {
							$__isAllCompleted = true;
							for ($k=0; $k<$__numOfStatus; $k++) {
								$___recordStatus = $__routeTargetUserRecordStatusAry[$k];
								
								if ($___recordStatus != $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']) {
									$__isAllCompleted = false;
									break;
								}
							}
						}
						
						if ($__isAllCompleted) {
							// all this route users signed
							$routeStatusAry[$_documentId][$__routeId] = $docRoutingConfig['routeStatus']['completed'];
						}
						else {
							// not all this route users signed
							if ($j == 0) {
								// first route => should be "in progress" if not "completed"
								$routeStatusAry[$_documentId][$__routeId] = $docRoutingConfig['routeStatus']['inProgress'];
							}
							else {
								// check previous route status
								if ($previousRouteStatus == $docRoutingConfig['routeStatus']['completed']) {
									// previous route is completed => this is route is "in progress"
									$routeStatusAry[$_documentId][$__routeId] = $docRoutingConfig['routeStatus']['inProgress'];
								}
								else if ($previousRouteStatus == $docRoutingConfig['routeStatus']['notStarted'] || $previousRouteStatus == $docRoutingConfig['routeStatus']['inProgress']) {
									
									// previous route is not completed => this route is not started yet
									$routeStatusAry[$_documentId][$__routeId] = $docRoutingConfig['routeStatus']['notStarted'];
								}
							}
						}
					}
					
					$previousRouteStatus = $routeStatusAry[$_documentId][$__routeId];
				}	// end loop route
			}	// end loop document
			
			return $routeStatusAry;
		}
		
		function deleteDRAttachment($FileIDAry)
		{
			global $intranet_root, $PATH_WRT_ROOT, $indexVar, $docRoutingConfig;
			$libfs = $this->getLibFileSystemInstance();
			
			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			$liblog = new liblog();

			$FileIDAry = IntegerSafe($FileIDAry);
			
			$sql = "SELECT FilePath FROM INTRANET_DR_FILE WHERE FileID IN (".implode(",",(array)$FileIDAry).")";
			$filePaths = $this->returnVector($sql);
			
			$rootpath = str_replace($docRoutingConfig['dbFilePath'],"",$docRoutingConfig['filePath']);
			
			$result = array();
			for($i=0;$i<count($filePaths);$i++){
				$filepath = trim($filePaths[$i]);
				if($filepath != ''){
					$fullpath = $rootpath.$filepath;
					$result['removefile'.$i] = $libfs->file_remove($fullpath);
				}
			}
			
			$sql = "Select * From INTRANET_DR_FILE WHERE FileID IN (".implode(",",(array)$FileIDAry).")";
			$fileInfoAry = $this->returnResultSet($sql);
			$numOfFile = count($fileInfoAry);
			for ($i=0; $i<$numOfFile; $i++) {
				$_fileId = $fileInfoAry[$i]['FileID'];
				$result['deleteLog'][$_fileId] = $liblog->INSERT_LOG($this->ModuleTitle, 'DeleteRoutingAttachment', $fileInfoAry[$i], 'INTRANET_DR_FILE', $_fileId);
			}
			
						
			$sql = "DELETE FROM INTRANET_DR_FILE WHERE FileID IN (".implode(",",(array)$FileIDAry).")";
			$result['DeleteQuery'] = $this->db_db_query($sql);
			
			$sql = "DELETE FROM INTRANET_DR_FILE_STAR WHERE FileID IN (".implode(",",(array)$FileIDAry).")";
			$result['DeleteStarRecords'] = $this->db_db_query($sql);
			
			return !in_array(false,$result);
		}
		
		function cleanTempDRRecords()
		{
			global $intranet_root, $PATH_WRT_ROOT, $indexVar, $docRoutingConfig;
			
			$sql = "SELECT DocumentID FROM INTRANET_DR_ENTRY WHERE RecordStatus='".$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp']."' AND UNIX_TIMESTAMP() > (UNIX_TIMESTAMP(DateInput)+86400)";
			$documentIDs = $this->returnVector($sql);
			
			for($i=0;$i<count($documentIDs);$i++){
				$docID = $documentIDs[$i];
				
				$sql = "SELECT FileID FROM INTRANET_DR_FILE WHERE LinkToType='".$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document']."' AND LinkToID='$docID'";
				$fileIDs = $this->returnVector($sql);
				$this->deleteDRAttachment($fileIDs);
				
				$sql = "SELECT RouteID FROM INTRANET_DR_ROUTES WHERE DocumentID='$docID'";
				$routeIDs = $this->returnVector($sql);
				
				if(count($routeIDs)>0){
					$sql = "DELETE FROM INTRANET_DR_ROUTE_ACTION WHERE RouteID IN (".implode(",",$routeIDs).")";
					$this->db_db_query($sql);
					
					$sql = "DELETE FROM INTRANET_DR_ROUTE_TARGET WHERE RouteID In (".implode(",",$routeIDs).")";
					$this->db_db_query($sql);
					
					$sql = "DELETE FROM INTRANET_DR_ROUTE_FEEDBACK WHERE RouteID IN (".implode(",",$routeIDs).")";
					$this->db_db_query($sql);
				}
			}
			if(count($documentIDs)>0){
				$sql = "DELETE FROM INTRANET_DR_ENTRY WHERE DocumentID IN (".implode(",",$documentIDs).")";
				$this->db_db_query($sql);
			}
		}
		
		function getFeedbackData($feedbackIdAry) {
			
			$feedbackIdAry = IntegerSafe($feedbackIdAry);
			
			$nameField = getNameFieldByLang('iu.');
			$sql = "Select
							fb.FeedbackID,
							fb.DocumentID,
							fb.RouteID,
							fb.UserID,
							$nameField as UserName
					From
							INTRANET_DR_ROUTE_FEEDBACK as fb
							Left Outer Join INTRANET_USER as iu ON (fb.UserID = iu.UserID)
					Where
							fb.FeedbackID IN ('".implode("','", (array)$feedbackIdAry)."')
					";
			return $this->returnResultSet($sql);
		}
		
		function getRouteUserFeedbacks($routeIdAry, $accessRightAry=array(), $targetUserIdAry=array())
		{
			global $intranet_root, $PATH_WRT_ROOT, $indexVar, $docRoutingConfig;
			
			$routeIdAry = IntegerSafe($routeIdAry);
			
			$name_field = getNameFieldByLang("u.");
			
			//if(count($accessRightAry)==0){
			//	$accessRightAry[] = $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route'];
			//}
			if(count($accessRightAry)>0){
				$accessRightAry = IntegerSafe($accessRightAry);
				$accessRightCond = " AND t.AccessRight IN ('".implode("','",$accessRightAry)."') ";
			}
			if(count($targetUserIdAry)>0){
				$targetUserIdAry = IntegerSafe($targetUserIdAry);
				$targetUserCond = " AND u.UserID IN ('".implode("','", $targetUserIdAry)."') ";
			}
			
			$sql = "SELECT 
						u.UserID,
						$name_field as UserName,
						f.FeedbackID,
						f.Comment,
						f.DateModified as FeedbackModifyDate,
						r.DocumentID,
						r.RouteID   
					FROM INTRANET_DR_ROUTES as r 
					INNER JOIN INTRANET_DR_ROUTE_TARGET as t ON r.RouteID=t.RouteID  
					INNER JOIN INTRANET_USER as u ON u.UserID=t.UserID 
					INNER JOIN INTRANET_DR_ROUTE_FEEDBACK as f ON f.RouteID=t.RouteID AND t.UserID=f.UserID 
					WHERE r.RouteID IN (".implode(",",(array)$routeIdAry).") $accessRightCond $targetUserCond
					ORDER BY r.DisplayOrder,r.RouteID,f.DateInput";
			//debug_r($sql);		
			return $this->returnResultSet($sql);
		}
		
		function returnRoutePeriod($startDate, $endDate, $CssType){
			global $docRoutingConfig;
								
			# Get Today's Time Stamp
			$today = getdate();
			$todaytimestamp = $today[0];
			$routePeriod ='';
			
			if(($todaytimestamp >= strtotime($startDate)) && ($todaytimestamp <= strtotime($endDate))){					
				$routePeriod = 'Current';
			}
			elseif($todaytimestamp > strtotime($endDate)){		
				$routePeriod = 'Pass';
			}
			elseif($todaytimestamp < strtotime($startDate)){
				$routePeriod = 'Coming';		
			}	
						
			return $routePeriod;					
		}
		/*
		function updateFeedbackStatus($recordStatus,$routeId='',$userId='',$feedbackIdAry='')
		{
			global $docRoutingConfig;
			
			if(!in_array($recordStatus,$docRoutingConfig['INTRANET_DR_ROUTE_FEEDBACK']['RecordStatus'])){
				return false;
			}
			
			if($routeId != '' && $userId != ''){
				$cond = " RouteID='$routeID' AND UserID='".$userId."' ";
			}
			
			if($feedbackIdAry != ''){
				$cond = " FeedbackID IN (".implode(",",(array)$feedbackIdAry).") ";
			}
			
			$result = false;
			if($cond != ''){
				$sql = "UPDATE INTRANET_DR_ROUTE_FEEDBACK SET RecordStatus='".$recordStatus."'  
						WHERE $cond";
						
				$result = $this->db_db_query($sql);
			}
			return $result;
		}
		*/
		function updateTargetUserStatus($recordStatus,$routeId,$userId)
		{
			global $docRoutingConfig;
			
			if(!in_array($recordStatus,$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus'])){
				return false;
			}
			
			$recordStatus = IntegerSafe($recordStatus);
			$routeId = IntegerSafe($routeId);
			$userId = IntegerSafe($userId);
			
			$sql = "UPDATE INTRANET_DR_ROUTE_TARGET SET RecordStatus='".$recordStatus."',DateModified=NOW() 
					WHERE RouteID='$routeId' AND UserID='".$userId."'";
						
			$result = $this->db_db_query($sql);
			
			return $result;
		}
		
		function updateComment($feedbackId,$comment)
		{
			global $indexVar;
			
			$modifyUserID = IntegerSafe($indexVar['drUserId']);
			$feedbackId = IntegerSafe($feedbackId);
			$comment = $this->Get_Safe_Sql_Query($comment);
			
			$sql = "UPDATE INTRANET_DR_ROUTE_FEEDBACK 
					SET Comment='".$comment."',DateModified=NOW(),ModifiedBy='".$modifyUserID."' 
					WHERE FeedbackID='".$feedbackId."'";
			
			return $this->db_db_query($sql);
		}
		
		function insertComment($documentId,$routeId,$user_Id,$comment)
		{
			global $indexVar;
			
			$modifyUserID = IntegerSafe($indexVar['drUserId']);
			$documentId = IntegerSafe($documentId);
			$routeId = IntegerSafe($routeId);
			$user_Id = IntegerSafe($user_Id);
			$comment = $this->Get_Safe_Sql_Query($comment);
			
			$sql = "INSERT INTO INTRANET_DR_ROUTE_FEEDBACK (DocumentID,RouteID,UserID,Comment,DateInput,DateModified,InputBy,ModifiedBy) 
					VALUES ('$documentId','$routeId','$user_Id','$comment',NOW(),NOW(),'$modifyUserID','$modifyUserID')";
		
			$result = $this->db_db_query($sql);
			if($result){
				return $this->db_insert_id();
			}
			return false;
		}
		
		function deleteComment($feedbackId) 
		{
			global $docRoutingConfig, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			$liblog = new liblog();

			$feedbackId = IntegerSafe($feedbackId);
			
			$sql = "SELECT COUNT(*) FROM INTRANET_DR_FILE WHERE LinkToType='".$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback']."' AND LinkToID='".$feedbackId."'";
			$counter = $this->returnVector($sql);
			
			$result = true;
			if($counter[0]==0) {
				$sql = "Select * From INTRANET_DR_ROUTE_FEEDBACK WHERE FeedbackID='".$feedbackId."'";
				$commentInfoAry = $this->returnResultSet($sql);
				
				if (trim(strip_tags($commentInfoAry[0]['Comment'])) == '') {
					$successAry['deleteLog'] = $liblog->INSERT_LOG($this->ModuleTitle, 'DeleteComment', $commentInfoAry[0], 'INTRANET_DR_ROUTE_FEEDBACK', $feedbackId);
				
					$sql = "DELETE FROM INTRANET_DR_ROUTE_FEEDBACK WHERE FeedbackID='".$feedbackId."'";
					$result = $this->db_db_query($sql);
				}
			}
			return $result;
		}
		
		function deleteFeedback($feedbackId)
		{
			global $docRoutingConfig, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			$liblog = new liblog();
			$libfs = $this->getLibFileSystemInstance();
			
			$feedbackId = IntegerSafe($feedbackId);

			$sql = "SELECT * FROM INTRANET_DR_FILE WHERE LinkToType='".$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback']."' AND LinkToID='".$feedbackId."'";
			$file_ary = $this->returnVector($sql);
			
			$result = array();
			
			$sql = "Select * From INTRANET_DR_ROUTE_FEEDBACK WHERE FeedbackID='".$feedbackId."'";
			$commentInfoAry = $this->returnResultSet($sql);
			
			if (count($commentInfoAry)>0)
			{
				$successAry['deleteLog'] = $liblog->INSERT_LOG($this->ModuleTitle, 'DeleteComment', $commentInfoAry[0], 'INTRANET_DR_ROUTE_FEEDBACK', $feedbackId);
			
				$sql = "DELETE FROM INTRANET_DR_ROUTE_FEEDBACK WHERE FeedbackID='".$feedbackId."'";
				$result = $this->db_db_query($sql);
				
				$rootpath = str_replace($docRoutingConfig['dbFilePath'],"",$docRoutingConfig['filePath']);
				
				for($i=0;$i<count($file_ary);$i++){
					$filepath = trim($file_ary[$i]['FilePath']);
					if($filepath != ''){
						$fullpath = $rootpath.$filepath;
						$result['removefile'.$i] = $libfs->file_remove($fullpath);
					}
				}
			}
			return !in_array(false,$result);
		}
		
		function addDocStar($documentId,$userId)
		{
			$documentId = IntegerSafe($documentId);
			$userId = IntegerSafe($userId);
			
			$sql = "INSERT INTO INTRANET_DR_STAR (DocumentID,UserID,DateInput) VALUES ('".$documentId."','".$userId."', Now())";
			
			return $this->db_db_query($sql);
		}
		
		function removeDocStar($documentId,$userId)
		{
			$documentId = IntegerSafe($documentId);
			$userId = IntegerSafe($userId);
			
			$sql = "DELETE FROM INTRANET_DR_STAR WHERE DocumentID='".$documentId."' AND UserID='".$userId."'";
			return $this->db_db_query($sql);
		}
		
		function getDocRoutingUserStatusCount($documentId)
		{
			global $docRoutingConfig;
			
			$documentId = IntegerSafe($documentId);
			
			$sql = "SELECT 
						r.RouteID,
						r.EffectiveType,
						r.EffectiveStartDate,
						r.EffectiveEndDate,
						COUNT(DISTINCT t.UserID) as UserTotal,
						COUNT(DISTINCT t2.UserID) as CompletedTotal 
					FROM INTRANET_DR_ENTRY as e 
					INNER JOIN INTRANET_DR_ROUTES as r ON r.DocumentID=e.DocumentID 
					LEFT JOIN INTRANET_DR_ROUTE_TARGET as t ON t.RouteID=r.RouteID AND t.AccessRight='".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']."' 
					LEFT JOIN INTRANET_DR_ROUTE_TARGET as t2 ON t2.RouteID=r.RouteID AND t2.AccessRight='".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']."' 
						AND t2.RecordStatus='".$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']."' 
					WHERE e.DocumentID='".$documentId."' 
					GROUP BY r.RouteID 
					ORDER BY r.DisplayOrder,r.RouteID";
			
			return $this->returnResultSet($sql);
		}
		
		// Go through a document's attachments, group with same name and sort by input time, assign version number start from 1
		function updateDocFileVersioning($DocumentId,$LinkToType)
		{
			global $docRoutingConfig;
			
			$DocumentId = IntegerSafe($DocumentId);
			$LinkToType = $this->Get_Safe_Sql_Query($LinkToType);
			
			$sql = "SELECT 
						f.FileID,
						f.FileName, 
						f.VersionNumber 
					FROM INTRANET_DR_FILE as f 
					WHERE f.LinkToType='".$LinkToType."' 
						AND f.LinkToID='".$DocumentId."' 
					ORDER BY f.DateInput";
			
			$records =  $this->returnResultSet($sql);
			
			$fileNameToFileID = array();
			for($i=0;$i<count($records);$i++) {
				$tmpFileName = $records[$i]['FileName'];
				$tmpFileID = $records[$i]['FileID'];
				if(!isset($fileNameToFileID[$tmpFileName])) {
					$fileNameToFileID[$tmpFileName] = array();
				}
				$fileNameToFileID[$tmpFileName][] = $tmpFileID;
			}
			
			$result = array();
			if(count($fileNameToFileID)>0) {
				foreach($fileNameToFileID as $tmpFileName => $tmpFileIdAry) {
					$tmpNumOfFile = count($tmpFileIdAry);
					for($i=0;$i<$tmpNumOfFile;$i++) {
						$sql = "UPDATE INTRANET_DR_FILE SET VersionNumber='".($i+1)."' WHERE FileID='".$tmpFileIdAry[$i]."' ";
						$result['UpdateVersion_'.$tmpFileIdAry[$i]] = $this->db_db_query($sql);
					}
				}
			}
			
			return !in_array(false,$result);
		}
		
		function updateRouteCommentDraft($RouteId, $User_Id, $Comment) 
		{
			global $docRoutingConfig;
			
			$RouteId = IntegerSafe($RouteId);
			$User_Id = IntegerSafe($User_Id);
			
			$result = array();
			$escComment = $this->Get_Safe_Sql_Query(trim($Comment));
			$sql = "UPDATE INTRANET_DR_COMMENT_DRAFT SET Comment='".$escComment."',DateModified=NOW() WHERE RouteID='".$RouteId."' AND UserID='".$User_Id."'";
			$result['update'] = $this->db_db_query($sql);
			
			$affected_count = $this->db_affected_rows();
			
			if($affected_count == 0) {
				$sql = "INSERT INTO INTRANET_DR_COMMENT_DRAFT (RouteID,UserID,Comment,DateInput,DateModified) VALUES ('".$RouteId."','".$User_Id."','".$escComment."',NOW(),NOW())";
				$result['insert'] = $this->db_db_query($sql);
			}
			
			return !in_array(false,$result);
		}
		
		function deleteRouteCommentDraft($RouteId, $User_Id)
		{
			global $docRoutingConfig, $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			$liblog = new liblog();
			
			$RouteId = IntegerSafe($RouteId);
			$User_Id = IntegerSafe($User_Id);
			
			$sql = "SELECT * FROM INTRANET_DR_COMMENT_DRAFT WHERE RouteID='".$RouteId."' AND UserID='".$User_Id."'";
			$tempAry = $this->returnResultSet($sql);
			$successAry['deleteLog'] = $liblog->INSERT_LOG($this->ModuleTitle, 'DeleteCommentDraft', $tempAry[0], 'INTRANET_DR_COMMENT_DRAFT', $tempAry[0]['DraftID']);
			
			$sql = "DELETE FROM INTRANET_DR_COMMENT_DRAFT WHERE RouteID='".$RouteId."' AND UserID='".$User_Id."'";
			return $this->db_db_query($sql);
		}
		
		function getRouteDraftRecords($User_Id,$RouteIdAry,$InAssocArray=false) 
		{
			global $docRoutingConfig;
			
			$RouteIdAry = IntegerSafe($RouteIdAry);
			$User_Id = IntegerSafe($User_Id);
			
			$sql = "SELECT * FROM INTRANET_DR_COMMENT_DRAFT WHERE UserID='".$User_Id."' AND RouteID IN (".implode(",",(array)$RouteIdAry).") ORDER BY RouteID";
			$records = $this->returnResultSet($sql);
			
			if($InAssocArray) {
				$routeIdToRecord = array();
				for($i=0;$i<count($records);$i++) {
					$tmpRouteId = $records[$i]['RouteID'];
					$routeIdToRecord[$tmpRouteId] = $records[$i];
				}
				return $routeIdToRecord;
			}else{
				return $records;
			}
		}
		
		// call this before you want to edit a route comment
		function requestRouteLock($RouteId, $User_Id='') 
		{
			global $docRoutingConfig, $indexVar;
			
			$drUserID = $User_Id == ''? $indexVar['drUserId'] : $User_Id;
			
			$drUserID = IntegerSafe($drUserID);
			$RouteId = IntegerSafe($RouteId);
			
			$sql = "SELECT LockDuration FROM INTRANET_DR_ROUTES WHERE RouteID='".$RouteId."'";
			$records = $this->returnVector($sql);
			$duration = $records[0] * 60; // minute to second
			$resultAry = array(0,$records[0]); // [0] free to use or not, [1] lock timeout minutes
			
			$sql = "SELECT LockID,LastLockBy,LastLockTime FROM INTRANET_DR_ROUTE_LOCK WHERE RouteID='".$RouteId."'";
			$records = $this->returnResultSet($sql);
			
			$canUse = 0;
			if(count($records)==0) {
				// no lock yet, free to edit
				// lock it immediately
				$sql = "INSERT INTO INTRANET_DR_ROUTE_LOCK (RouteID,LastLockBy,LastLockTime) VALUES ('".$RouteId."','".$drUserID."',NOW())";
				$success = $this->db_db_query($sql);
				$canUse = $success ? 1:0;
			}else {
				$tmpLockId = $records[0]['LockID'];
				$tmpLastLockBy = trim($records[0]['LastLockBy']);
				$tmpLastLockTime = $records[0]['LastLockTime'];
				$ts_now = time();
				$ts_lastLockTime = strtotime($tmpLastLockTime);
				$ts_delta = $ts_now - $ts_lastLockTime;
				
				if($tmpLastLockBy == '') {
					// no body is locking
					// lock it
					$sql = "UPDATE INTRANET_DR_ROUTE_LOCK SET LastLockBy='".$drUserID."',LastLockTime=NOW() WHERE LockID='".$tmpLockId."'";
					$success = $this->db_db_query($sql);
					$canUse = $success ? 1:0;
				}else if($drUserID != $tmpLastLockBy) {
					if($ts_delta > $duration) {
						// locking by others but has timeout, grab the lock
						$sql = "UPDATE INTRANET_DR_ROUTE_LOCK SET LastLockBy='".$drUserID."',LastLockTime=NOW() WHERE LockID='".$tmpLockId."'";
						$success = $this->db_db_query($sql);
						$canUse = $success ? 1:0;
					}else {
						// wait until release
						$canUse = 0; 
					}
				}else {
					// locking by self, refresh lock time
					$sql = "UPDATE INTRANET_DR_ROUTE_LOCK SET LastLockBy='".$drUserID."',LastLockTime=NOW() WHERE LockID='".$tmpLockId."'";
					$success = $this->db_db_query($sql);
					$canUse = $success ? 1:0;
				}
			}
			$resultAry[0] = $canUse;
			return $resultAry;
		}
		
		// after submit or cancel route comment, call this to release route
		function releaseRouteLock($RouteId,$User_Id='')
		{
			global $docRoutingConfig, $indexVar;
			
			$drUserID = $User_Id == ''? $indexVar['drUserId'] : $User_Id;
			
			$RouteId = IntegerSafe($RouteId);
			$drUserID = IntegerSafe($drUserID);
			
			$sql = "UPDATE INTRANET_DR_ROUTE_LOCK SET LastLockBy=NULL WHERE RouteID='".$RouteId."' AND LastLockBy='".$drUserID."'";
			$result = $this->db_db_query($sql);
			
			return $result;
		}
		
		/*
		 * @param int $DocumentId : new DocumentID linked to
		 * @param int $CopyRouteId : the RouteID that copy from
		 * @return success : new RouteID generated
		 * 		   fail : false
		 */
		function copyRoute($DocumentId,$CopyRouteId)
		{
			global $docRoutingConfig, $indexVar;
			
			$drUserID = IntegerSafe($indexVar['drUserId']);
			$DocumentId = IntegerSafe($DocumentId);
			$CopyRouteId = IntegerSafe($CopyRouteId);
			
			$result = array();
			// copy route record
			$sql = "INSERT INTO INTRANET_DR_ROUTES (DocumentID,UserID,Note,EffectiveType,EffectiveStartDate,EffectiveEndDate,ViewRight,ReplySlipID,ReplySlipReleaseType,EnableLock,LockDuration,ReadOnlyUserViewDetail,RecordStatus,DisplayOrder,DateInput,DateModified,InputBy,ModifiedBy) 
						 SELECT '$DocumentId','$drUserID',Note,EffectiveType,EffectiveStartDate,EffectiveEndDate,ViewRight,NULL,ReplySlipReleaseType,EnableLock,LockDuration,ReadOnlyUserViewDetail,RecordStatus,DisplayOrder,NOW(),NOW(),'$drUserID','$drUserID' FROM INTRANET_DR_ROUTES WHERE RouteID='".$CopyRouteId."'";
			$result['CopyRoute'] = $this->db_db_query($sql);
			
			if(!$result['CopyRoute']) {
				return false;
			}
			
			$newRouteId = $this->db_insert_id();
			
			// copy route action records
			$sql = "INSERT INTO INTRANET_DR_ROUTE_ACTION (RouteID,Action,DateInput,InputBy) 
						SELECT '$newRouteId',Action,NOW(),NOW() FROM INTRANET_DR_ROUTE_ACTION WHERE RouteID='".$CopyRouteId."'";
			$result['CopyRouteAction'] = $this->db_db_query($sql);
			
			
			// copy route user targets
			$sql = "INSERT INTO INTRANET_DR_ROUTE_TARGET (RouteID,UserID,AccessRight,RecordStatus,DateInput) 
						SELECT '$newRouteId',UserID,AccessRight,'1',NOW() FROM INTRANET_DR_ROUTE_TARGET WHERE RouteID='$CopyRouteId'";
			$result['CopyRouteTarget'] = $this->db_db_query($sql);
			
			$sql = "SELECT ReplySlipID,ReplySlipType FROM INTRANET_DR_ROUTES WHERE RouteID='$CopyRouteId'";
			$records = $this->returnResultSet($sql);
			$copyReplySlipId = trim($records[0]['ReplySlipID']);
			$copyReplySlipType = $records[0]['ReplySlipType'];
			
			if($copyReplySlipId > 0) {
				if($copyReplySlipType == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) { // table type
					// copy reply slip
					$sql = "INSERT INTO INTRANET_TABLE_REPLY_SLIP (LinkToModule,LinkToType,LinkToID,Title,Description,AnsAllQuestion,ShowUserInfoInResult,RecordType,RecordStatus,InputDate,InputBy,ModifiedDate,ModifiedBy) 
								SELECT LinkToModule,LinkToType,'$newRouteId',Title,Description,AnsAllQuestion,ShowUserInfoInResult,RecordType,RecordStatus,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_TABLE_REPLY_SLIP 
								WHERE LinkToModule='".$docRoutingConfig['moduleCode']."' AND LinkToType='".$docRoutingConfig['replySlip']['linkToType']['route']."' AND LinkToID='".$CopyRouteId."'";
					$result['CopyReplySlip'] = $this->db_db_query($sql);
					
					if($result['CopyReplySlip']) {
						$newReplySlipId = $this->db_insert_id();
						
						$sql = "INSERT INTO INTRANET_TABLE_REPLY_SLIP_ITEM (ReplySlipID,RecordType,Content,RowOrder,ColumnOrder,InputDate,InputBy,ModifiedDate,ModifiedBy)
									SELECT '$newReplySlipId',RecordType,Content,RowOrder,ColumnOrder,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_TABLE_REPLY_SLIP_ITEM WHERE ReplySlipID='".$copyReplySlipId."'";
						$result['CopyReplySlipItem_'.$copyReplySlipId] = $this->db_db_query($sql);
						
						$sql = "INSERT INTO INTRANET_TABLE_REPLY_SLIP_OPTION (ReplySlipID,UniqueID,RecordType,Content,DisplayOrder,InputDate,InputBy,ModifiedDate,ModifiedBy)
									SELECT '$newReplySlipId',UniqueID,RecordType,Content,DisplayOrder,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_TABLE_REPLY_SLIP_OPTION WHERE ReplySlipID='".$copyReplySlipId."'";
						$result['CopyReplySlipItemOption_'.$copyReplySlipId] = $this->db_db_query($sql);
						
						$sql = "UPDATE INTRANET_DR_ROUTES SET ReplySlipID='".$newReplySlipId."',ReplySlipType='".$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']."' WHERE RouteID='".$newRouteId."'";
						$result['UpdateRouteReplySlipID'] = $this->db_db_query($sql);
					}
				} else { // original list type
					// copy reply slip
					$sql = "INSERT INTO INTRANET_REPLY_SLIP (LinkToModule,LinkToType,LinkToID,Title,Description,ShowQuestionNum,AnsAllQuestion,ShowUserInfoInResult,RecordType,RecordStatus,InputDate,InputBy,ModifiedDate,ModifiedBy) 
								SELECT LinkToModule,LinkToType,'$newRouteId',Title,Description,ShowQuestionNum,AnsAllQuestion,ShowUserInfoInResult,RecordType,RecordStatus,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_REPLY_SLIP 
								WHERE LinkToModule='".$docRoutingConfig['moduleCode']."' AND LinkToType='".$docRoutingConfig['replySlip']['linkToType']['route']."' AND LinkToID='".$CopyRouteId."'";
					$result['CopyReplySlip'] = $this->db_db_query($sql);
					
					if($result['CopyReplySlip']) {
						$newReplySlipId = $this->db_insert_id();
						
						$sql = "SELECT * FROM INTRANET_REPLY_SLIP_QUESTION WHERE ReplySlipID='".$copyReplySlipId."'";
						$copyQuestionAry = $this->returnResultSet($sql);
						
						// copy each question one by one and its related options
						for($i=0;$i<count($copyQuestionAry);$i++) {
							$copyQuestionId = $copyQuestionAry[$i]['QuestionID'];
							
							$sql = "INSERT INTO INTRANET_REPLY_SLIP_QUESTION (ReplySlipID,RecordType,Content,DisplayOrder,InputDate,InputBy,ModifiedDate,ModifiedBy) 
										SELECT '$newReplySlipId',RecordType,Content,DisplayOrder,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_REPLY_SLIP_QUESTION WHERE ReplySlipID='".$copyReplySlipId."' AND QuestionID='".$copyQuestionId."'";
							$result['CopyQuestion_'.$copyQuestionId] = $this->db_db_query($sql);
							
							if($result['CopyQuestion_'.$copyQuestionId]) {
								$newQuestionId = $this->db_insert_id();
								
								$sql = "INSERT INTO INTRANET_REPLY_SLIP_QUESTION_OPTION (QuestionID,Content,DisplayOrder,InputDate,InputBy,ModifiedDate,ModifiedBy)
											SELECT '$newQuestionId',Content,DisplayOrder,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_REPLY_SLIP_QUESTION_OPTION WHERE QuestionID='".$copyQuestionId."'";
								$result['CopyQuestionOptions_'.$copyQuestionId.'_'.$newQuestionId] = $this->db_db_query($sql);
							}
						}
						
						$sql = "UPDATE INTRANET_DR_ROUTES SET ReplySlipID='".$newReplySlipId."',ReplySlipType='".$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['List']."' WHERE RouteID='".$newRouteId."'";
						$result['UpdateRouteReplySlipID'] = $this->db_db_query($sql);
					}
				}
			}
			
			return $newRouteId;
		}
		
		/*
		 * @param int $CopyDocumentId : the DocumentID to be copied
		 * @return bool : success - true ; fail : false
		 */
		function copyDocument($CopyDocumentId, $CopyDocumentTitle, $CopyDocumentStatus)
		{
			global $docRoutingConfig, $indexVar, $Lang;
			
			$drUserID = IntegerSafe($indexVar['drUserId']);
			$CopyDocumentId = IntegerSafe($CopyDocumentId);
			
			$result = array();
			// copy document record
			//$Lang['SysMgr']['SubjectClassMapping']['CopyFrom'];
//			$sql = "INSERT INTO INTRANET_DR_ENTRY (UserID,Title,Instruction,DocumentType,PhysicalLocation,AllowAdHocRoute,RecordStatus,DateInput,DateModified,InputBy,ModifiedBy) 
//						SELECT '$drUserID',Title,Instruction,DocumentType,PhysicalLocation,AllowAdHocRoute,RecordStatus,NOW(),NOW(),'$drUserID','$drUserID' FROM INTRANET_DR_ENTRY WHERE DocumentID='".$CopyDocumentId."'";
//			
			# Obtain 'From' DR Entry Info 
			$FromDREntryData = $this->getEntryData($CopyDocumentId);
			//$title = $Lang['SysMgr']['SubjectClassMapping']['CopyFrom'] . ' - ' . $FromDREntryData[0]['Title'] ;
			$instruction = $FromDREntryData[0]['Instruction'];
			$documentType = $FromDREntryData[0]['DocumentType'];
			$physicalLocation = $FromDREntryData[0]['PhysicalLocation'];
			$allowAdHocRoute = $FromDREntryData[0]['AllowAdHocRoute'];
			//$recordStatus = $FromDREntryData[0]['RecordStatus'];
			$title = $CopyDocumentTitle;
			$recordStatus = $CopyDocumentStatus;
			
			# Insert New Record	
			$sql = "INSERT INTO INTRANET_DR_ENTRY (UserID,Title,Instruction,DocumentType,PhysicalLocation,AllowAdHocRoute,RecordStatus,DateInput,DateModified,InputBy,ModifiedBy) 
					VALUES ('$drUserID','".$this->Get_Safe_Sql_Query($title)."','".$this->Get_Safe_Sql_Query($instruction)."','$documentType','$physicalLocation','$allowAdHocRoute','$recordStatus',NOW(),NOW(),'$drUserID','$drUserID')";
			$result['CopyEntry'] = $this->db_db_query($sql);
			
			if(!$result['CopyEntry']){
				return false;
			}
			
			$newDocumentId = $this->db_insert_id();
			
			// copy tag if any
			$copyTagAry = $this->getEntryTagName($CopyDocumentId);
			if(count($copyTagAry)>0) {
				$copyTag = implode(",",$copyTagAry);
				$result['CopyTag'] = $this->saveEntryTag($newDocumentId,$copyTag);
			}
			
			// copy attachments
			$sql = "SELECT * FROM INTRANET_DR_FILE WHERE LinkToType='".$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document']."' AND LinkToID='".$CopyDocumentId."'";
			$fileRecords = $this->returnResultSet($sql);
			$filesAry = array();
			if(count($fileRecords)>0) {
				for($i=0;$i<count($fileRecords);$i++) {
					$copyFileFullPath = $docRoutingConfig['filePath'].str_replace($docRoutingConfig['dbFilePath'],"",$fileRecords[$i]['FilePath']);
					$filesAry[] = array('name'=>$fileRecords[$i]['FileName'],'tmp_name'=>$copyFileFullPath);
				}
				if(count($filesAry)>0) {
					$result['CopyAttachment'] =  $this->addFile($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $newDocumentId, $filesAry);
				}
			}
			
			// copy routes one by one
			$sql = "SELECT RouteID FROM INTRANET_DR_ROUTES WHERE DocumentID='".$CopyDocumentId."'";
			$copyRouteIdAry = $this->returnVector($sql);
			
			for($i=0;$i<count($copyRouteIdAry);$i++) {
				$copyRouteId = $copyRouteIdAry[$i];
				$result['CopyRoute_'.$copyRouteId] = $this->copyRoute($newDocumentId,$copyRouteId);
			}
			
			$result['updateDocFileVersioning'] = $this->updateDocFileVersioning($newDocumentId, $docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document']);
			
			return !in_array(false,$result);
		}
		
		function createDocFromModule($module,$paramAry)
		{
			global $docRoutingConfig, $indexVar, $PATH_WRT_ROOT, $intranet_root,$intranet_session_language, $Lang;
			
			error_reporting(E_ERROR & ~E_WARNING); // turn off false reported warnings
			
			$newDocumentId = '';
			if($module == 'imailplus')
			{
				include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
				include_once($PATH_WRT_ROOT."includes/imap_cache_agent.php");
				include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
				include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");
				
				$Folder = base64_decode($paramAry['Folder']);
				$Uid = $paramAry['Uid'];
				
				if($Folder == '' || $Uid == ''){
					return $newDocumentId;
				}
				
				$ldamu = new libdigitalarchive_moduleupload();
				$IMap = new imap_gamma();
				$IMapCache = $IMap->getImapCacheAgent();
				$libfs = $this->getLibFileSystemInstance();
				
				$drUserID = $indexVar['drUserId'];
				$tmp_folder = $docRoutingConfig['archiveToDigitalArchiveTempPath'];
				$dir_path = $tmp_folder."/u".$drUserID;
				
				$parentFolderPath = substr($tmp_folder,0,strrpos($tmp_folder,'/'));
				//debug_r($parentFolderPath);
				if (!file_exists($parentFolderPath)) {
					$libfs->folder_new($parentFolderPath);
					$libfs->chmod_R($parentFolderPath,0777);
				}
				//debug_r($tmp_folder);
				if (!file_exists($tmp_folder)) {
					$libfs->folder_new($tmp_folder);
					$libfs->chmod_R($tmp_folder,0777);
				}
				
				if(file_exists($dir_path)){
					$libfs->folder_remove_recursive($dir_path);
				}
				if(!is_dir($dir_path)) {
					//debug_r($dir_path);
					$libfs->folder_new($dir_path);
					$libfs->chmod_R($dir_path, 0777);
				}
				$MailContent = $IMap->getMailRecord($Folder, $Uid);
				$Attachment = $MailContent[0]["attach_parts"];
				
				$docData = array();
				$docData['Title'] = $MailContent[0]["subject"];
				$docData['Instruction'] = $MailContent[0]["message"];
				$docData['DocumentType'] = $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File'];
				$docData['RecordStatus'] = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp'];
				
				$newDocumentId = $this->updateDREntry($docData);
				
				if ($newDocumentId && !(is_null($Attachment) || $Attachment == "" || empty($Attachment))) {
					$filesList = array();
					for ($i=0; $i< sizeof($Attachment); $i++) 
					{	
						$FileName = $Attachment[$i]["FileName"];
						$filepath = $dir_path."/".$FileName;
						
						while(file_exists($filepath)){
							$FileName = $ldamu->Rename_File($FileName);
							$filepath = $dir_path."/".$FileName;
						}
						
						if(!file_exists($filepath))
						{
							$thisAttachment= $IMapCache->Get_Attachment($Attachment[$i]['PartID'],$Uid,$Folder);
							$libfs->file_write($thisAttachment["Content"],$filepath);
							
							$filesList[] = array('tmp_name'=>$filepath,'name'=>$FileName);
						}
					}
					if(count($filesList)>0) {
						$this->addFile($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $newDocumentId, $filesList);
					}
				}
				if(file_exists($dir_path)){
					$libfs->folder_remove_recursive($dir_path);
				}
			}else if($module == 'ifolder')
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libftp.php");
				include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
				//include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");
				
				$drUserID = $indexVar['drUserId'];
				$lu = new libuser($drUserID);
				$lftp = new libftp();
				$libfs = $this->getLibFileSystemInstance();
				//$ldamu = new libdigitalarchive_moduleupload();
				//debug_r($paramAry);
				$current_dir = $paramAry['current_dir'];
				$filenameAry = $paramAry['filename'];
				$filetypeAry = $paramAry['filetype'];
				
				$filesAry = array();
				for($i=0;$i<count($filenameAry);$i++) {
					if($filenameAry[$i] != '' && $filetypeAry[$i] == 'F') {
						$filesAry[] = $filenameAry[$i];
					}
				}
				
				if($current_dir == '' || count($filesAry)==0) {
					return $newDocumentId;
				}
				
				if (!$lftp->connect($lu->UserLogin,$lu->UserPassword)){
					return $newDocumentId;
				}
				
				$tmp_folder = $docRoutingConfig['archiveToDigitalArchiveTempPath'];
				$dir_path = $tmp_folder."/u".$drUserID;
				
				$parentFolderPath = substr($tmp_folder,0,strrpos($tmp_folder,'/'));
				if (!file_exists($parentFolderPath)) {
					$libfs->folder_new($parentFolderPath);
					$libfs->chmod_R($parentFolderPath,0777);
				}
				if (!file_exists($tmp_folder)) {
					$libfs->folder_new($tmp_folder);
					$libfs->chmod_R($tmp_folder,0777);
				}
				
				if(file_exists($dir_path)){
					$libfs->folder_remove_recursive($dir_path);
				}
				if(!is_dir($dir_path)) {
					$libfs->folder_new($dir_path);
					$libfs->chmod_R($dir_path, 0777);
				}
				
				$lftp->chdir($current_dir);
				$current_dir = $lftp->pwd();
				
				$docData = array();
				$docData['Title'] = $Lang['Header']['Menu']['iFolder'];
				$docData['Instruction'] = "";
				$docData['DocumentType'] = $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File'];
				$docData['RecordStatus'] = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp'];
				
				$newDocumentId = $this->updateDREntry($docData);
				//debug_r($current_dir);
				$filesList = array();
				for($i=0;$i<count($filesAry);$i++) {
					$filename = $filesAry[$i];
					$temp_filename = rand();
					
                	$get = @ftp_get($lftp->connection, $lftp->download_temp."/".$temp_filename, $current_dir."/".rawurldecode($filename), FTP_BINARY);
					
					if (!$get) continue;
					
					$content = get_file_content($lftp->download_temp."/".$temp_filename);
					
					$filepath = $dir_path."/".$filename;
						
					//while(file_exists($filepath)){
					//	$filename = $ldamu->Rename_File($filename);
					//	$filepath = $dir_path."/".$filename;
					//}
					
					$libfs->file_write($content,$filepath);
					
					$filesList[] = array('tmp_name'=>$filepath,'name'=>$filename);
					
					unlink($lftp->download_temp."/".$temp_filename);
				}
				
				if(count($filesList)>0) {
					$this->addFile($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $newDocumentId, $filesList);
				}
				
				if(file_exists($dir_path)){
					$libfs->folder_remove_recursive($dir_path);
				}
			}
			
			return $newDocumentId;
		}
		
		###################################################################################
		############################ System Properties [Start] ############################
		###################################################################################
		function loadSettingsFromStorage(){
			global $docRoutingConfig;
			
			$sql = 'Select * From INTRANET_DR_SETTING';
			$this->settingAry = BuildMultiKeyAssoc($this->returnResultSet($sql), 'SettingName', 'SettingValue', $SingleValue=1);
			
			// set default value if no settings value yet 
			foreach((array)$docRoutingConfig['settings'] as $_settingName => $_settingOptionAry) {
				if (!isset($this->settingAry[$_settingName])) {
					$this->settingAry[$_settingName] = $_settingOptionAry['defaultVal'];
				}
			}
		}
	
		function returnSettingValueByName($settingName){
			if ($this->settingAry === null) {
				$this->loadSettingsFromStorage();
			}
			return $this->settingAry[$settingName];
		}
		
		function returnSettingAry() {
			if ($this->settingAry === null) {
				$this->loadSettingsFromStorage();
			}
			return $this->settingAry;
		}
		
		function updateSetting($settingName, $settingValue) {
			global $indexVar;
			
			$settingName = trim($settingName);
			$settingValue = trim($settingValue);
			
			$sql = "Insert Into INTRANET_DR_SETTING (SettingName, SettingValue, DateInput, InputBy, DateModified, ModifiedBy)
						Values ('".$settingName."', '".$this->Get_Safe_Sql_Query($settingValue)."', now(), '".$indexVar['drUserId']."', now(), '".$indexVar['drUserId']."')
						On Duplicate Key Update SettingValue = VALUES(SettingValue), DateModified = VALUES(DateModified), ModifiedBy = VALUES(ModifiedBy)
					 ";
			$success = $this->db_db_query($sql);
			$this->loadSettingsFromStorage();
		
			return $success;
		}	
		
		###################################################################################
		############################# System Properties [End] #############################
		###################################################################################
		
		function sendNotificationEmailForNewRouteReceived($routeId, $targetUserIdAry='', $customContent='') {
			global $Lang, $docRoutingConfig;
			
			$lwebmail = new libwebmail();
			
			$routeId = IntegerSafe($routeId);
			$targetUserIdAry = IntegerSafe($targetUserIdAry);
			
			// get route info
			$routeInfoAry = $this->getRouteData('', $routeId);
			if(count($routeInfoAry)==0) return false;
			
			$documentId = $routeInfoAry[0]['DocumentID'];
			
			// get document info
			$documentInfoAry = $this->getEntryData($documentId);
			$documentName = $documentInfoAry[0]['Title'];
			
			// get target user of the route
			if($targetUserIdAry === ''){
				$targetUserIdAry = Get_Array_By_Key($this->getRouteTargetUsers($routeId, $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']), 'UserID');
			}
			else if (!is_array($targetUserIdAry)) {
				$targetUserIdAry = array($targetUserIdAry);
			}
			
			// build email title and content
			$emailTitle = $Lang['DocRouting']['EmailNotification_newRouteReceived']['EmailTitle'];
			$emailTitle = $this->applyDocumentAndRouteDataInText($emailTitle, $documentName);
			
			$emailContent = implode('<br />', $Lang['DocRouting']['EmailNotification_newRouteReceived']['EmailContentAry']);
			if($customContent != '')
				$emailContent = $customContent;
			$emailContent = $this->applyDocumentAndRouteDataInText($emailContent, $documentName);
			
			//debug_pr($targetUserIdAry);
			return $lwebmail->sendModuleMail($targetUserIdAry, $emailTitle, $emailContent);
		}
		
		function sendNotificationEmailForNewFeedbackReceived($feedbackId) {
			global $Lang;
			
			$lwebmail = new libwebmail();
			
			// get feedback info
			$feedbackInfoAry = $this->getFeedbackData($feedbackId);
			$documentId = $feedbackInfoAry[0]['DocumentID'];
			$routeId = $feedbackInfoAry[0]['RouteID'];
			$userName = $feedbackInfoAry[0]['UserName'];
			
			// get document info
			$documentInfoAry = $this->getEntryData($documentId);
			$documentName = $documentInfoAry[0]['Title'];
			$documentCreatorUserId = $documentInfoAry[0]['UserID'];
			
			// get route info
			$routeInfoAry = $this->getRouteData($documentId, $routeId);
			$routeNum = $routeInfoAry[0]['DisplayOrder'];
			
			// build email title and content
			$emailTitle = $Lang['DocRouting']['EmailNotification_newFeedbackInRoute']['EmailTitle'];
			$emailTitle = $this->applyDocumentAndRouteDataInText($emailTitle, $documentName, $routeNum);
			
			$emailContent = implode('<br />', $Lang['DocRouting']['EmailNotification_newFeedbackInRoute']['EmailContentAry']);
			$emailContent = $this->applyDocumentAndRouteDataInText($emailContent, $documentName, $routeNum);
			$emailContent = str_replace('<!--userName-->', $userName, $emailContent);
			
			return $lwebmail->sendModuleMail(array($documentCreatorUserId), $emailTitle, $emailContent);
		}
		
		function applyDocumentAndRouteDataInText($text, $documentName='', $routeNum='') {
			$text = str_replace('<!--docTitle-->', $documentName, $text);
			$text = str_replace('<!--routeNum-->', $routeNum, $text);
			
			return $text;
		}
		
		function getStarStatus($userID){
			
			$userID = IntegerSafe($userID);
			
			$sql = "SELECT 
						UserID,
						DocumentID,
						IF (UserID IS NOT NULL, 1, 2) As StarSequence 
					FROM INTRANET_DR_STAR
					WHERE UserID='".$userID."' 
					";
			
			return $this->returnResultSet($sql);
		}
		
		
		function getReplySlipUserAnswer($replySlipID, $userID){
			if($replySlipID!=''){
				$replySlipID = IntegerSafe($replySlipID);
				$replySlipID_cond = "AND ReplySlipID IN ('".implode("','", (array)$replySlipID)."') "; 
			}
		
			if($userID!=''){
				$userID = IntegerSafe($userID);
				$userID_cond = "AND UserID IN ('".implode("','", (array)$userID)."') "; 
			}
		
		
			$sql = " SELECT 
					UserAnswerID,
					UserID, 		
					ReplySlipID,
					QuestionID,
					OptionID,
					Content
				FROM
					INTRANET_REPLY_SLIP_USER_ANSWER
				WHERE
					1
					$replySlipID_cond
					$userID_cond
				";
	
			
			return $this->returnResultSet($sql);
		}
		
		
		function enablePrintRoutingInstructionRight() {
	       global $sys_custom;
	       return $sys_custom['PrintRoutingInstruction'];
	    }
		
		
		
		function copyReplySlip($newRouteId, $copyRouteId)
		{
			global $docRoutingConfig, $indexVar;
			
			$drUserID = $indexVar['drUserId'];
			
			$copyRouteId = IntegerSafe($copyRouteId);
			$newRouteId = IntegerSafe($newRouteId);
			
			if($copyRouteId!='' && $newRouteId!='') {
				$sql = "SELECT ReplySlipID,ReplySlipType FROM INTRANET_DR_ROUTES WHERE RouteID='$copyRouteId'";
				$records = $this->returnResultSet($sql);
				$copyFromReplySlipId = trim($records[0]['ReplySlipID']);
				$copyFromReplySlipType = $records[0]['ReplySlipType'];
				
				if($copyFromReplySlipId != '') {
					if($copyFromReplySlipType == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) { // table type
						// copy reply slip
						$sql = "INSERT INTO INTRANET_TABLE_REPLY_SLIP (LinkToModule,LinkToType,LinkToID,Title,Description,AnsAllQuestion,ShowUserInfoInResult,RecordType,RecordStatus,InputDate,InputBy,ModifiedDate,ModifiedBy) 
									SELECT LinkToModule,LinkToType,'$newRouteId',Title,Description,AnsAllQuestion,ShowUserInfoInResult,RecordType,RecordStatus,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_TABLE_REPLY_SLIP 
									WHERE LinkToModule='".$docRoutingConfig['moduleCode']."' AND LinkToType='".$docRoutingConfig['replySlip']['linkToType']['route']."' AND LinkToID='".$copyRouteId."'";
						$result['CopyReplySlip'] = $this->db_db_query($sql);
						
						if($result['CopyReplySlip']) {
							$newReplySlipId = $this->db_insert_id();
							
							$sql = "INSERT INTO INTRANET_TABLE_REPLY_SLIP_ITEM (ReplySlipID,RecordType,Content,RowOrder,ColumnOrder,InputDate,InputBy,ModifiedDate,ModifiedBy)
										SELECT '$newReplySlipId',RecordType,Content,RowOrder,ColumnOrder,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_TABLE_REPLY_SLIP_ITEM WHERE ReplySlipID='".$copyFromReplySlipId."'";
							$result['CopyReplySlipItem_'.$copyFromReplySlipId] = $this->db_db_query($sql);
							
							$sql = "INSERT INTO INTRANET_TABLE_REPLY_SLIP_OPTION (ReplySlipID,UniqueID,RecordType,Content,DisplayOrder,InputDate,InputBy,ModifiedDate,ModifiedBy)
										SELECT '$newReplySlipId',UniqueID,RecordType,Content,DisplayOrder,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_TABLE_REPLY_SLIP_OPTION WHERE ReplySlipID='".$copyFromReplySlipId."'";
							$result['CopyReplySlipItemOption_'.$copyFromReplySlipId] = $this->db_db_query($sql);
							
							$sql = "UPDATE INTRANET_DR_ROUTES SET ReplySlipID='".$newReplySlipId."',ReplySlipType='".$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']."' WHERE RouteID='".$newRouteId."'";
							$result['UpdateRouteReplySlipID'] = $this->db_db_query($sql);
						}
						
					} else { // original list type
					
						// copy reply slip
						$sql = "INSERT INTO INTRANET_REPLY_SLIP (LinkToModule,LinkToType,LinkToID,Title,Description,ShowQuestionNum,AnsAllQuestion,ShowUserInfoInResult,RecordType,RecordStatus,InputDate,InputBy,ModifiedDate,ModifiedBy) 
									SELECT LinkToModule,LinkToType,'$newRouteId',Title,Description,ShowQuestionNum,AnsAllQuestion,ShowUserInfoInResult,RecordType,RecordStatus,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_REPLY_SLIP 
									WHERE LinkToModule='".$docRoutingConfig['moduleCode']."' AND LinkToType='".$docRoutingConfig['replySlip']['linkToType']['route']."' AND LinkToID='".$copyRouteId."'";
						$result['CopyReplySlip'] = $this->db_db_query($sql);
						
						if($result['CopyReplySlip']) {
							$newReplySlipId = $this->db_insert_id();
							
							$sql = "SELECT ReplySlipID FROM INTRANET_DR_ROUTES WHERE RouteID='$copyRouteId'";
							$records = $this->returnVector($sql);
							$copyReplySlipId = trim($records[0]);
							
							$sql = "SELECT * FROM INTRANET_REPLY_SLIP_QUESTION WHERE ReplySlipID='".$copyReplySlipId."'";
							$copyQuestionAry = $this->returnResultSet($sql);
							
							// copy each question one by one and its related options
							for($i=0;$i<count($copyQuestionAry);$i++) {
								$copyQuestionId = $copyQuestionAry[$i]['QuestionID'];
								
								$sql = "INSERT INTO INTRANET_REPLY_SLIP_QUESTION (ReplySlipID,RecordType,Content,DisplayOrder,InputDate,InputBy,ModifiedDate,ModifiedBy) 
											SELECT '$newReplySlipId',RecordType,Content,DisplayOrder,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_REPLY_SLIP_QUESTION WHERE ReplySlipID='".$copyReplySlipId."' AND QuestionID='".$copyQuestionId."'";
								$result['CopyQuestion_'.$copyQuestionId] = $this->db_db_query($sql);
								
								if($result['CopyQuestion_'.$copyQuestionId]) {
									$newQuestionId = $this->db_insert_id();
									
									$sql = "INSERT INTO INTRANET_REPLY_SLIP_QUESTION_OPTION (QuestionID,Content,DisplayOrder,InputDate,InputBy,ModifiedDate,ModifiedBy)
												SELECT '$newQuestionId',Content,DisplayOrder,NOW(),'$drUserID',NOW(),'$drUserID' FROM INTRANET_REPLY_SLIP_QUESTION_OPTION WHERE QuestionID='".$copyQuestionId."'";
									$result['CopyQuestionOptions_'.$copyQuestionId.'_'.$newQuestionId] = $this->db_db_query($sql);
								}
							}
							
							$sql = "UPDATE INTRANET_DR_ROUTES SET ReplySlipID='".$newReplySlipId."',ReplySlipType='".$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['List']."' WHERE RouteID='".$newRouteId."'";
							$result['UpdateRouteReplySlipID'] = $this->db_db_query($sql);
						}
					}
				}
			}
			
			return $newRouteId;
		}
		
		function getLocationSuggestion(){
			if (!function_exists('json_encode')) {
				function json_encode($data) {
					switch ($type = gettype($data)) {
						case 'NULL':
							return 'null';
						case 'boolean':
							return ($data ? 'true' : 'false');
						case 'integer':
						case 'double':
						case 'float':
							return $data;
						case 'string':
							$tmp = str_replace('"', '\\"' ,$data);
							$tmp = str_replace("\n", '\\n' ,$tmp);
							$tmp = str_replace("\t", '\\t' ,$tmp);
							$tmp = str_replace("\r", '\\r' ,$tmp);
							
							return '"' . $tmp . '"';
						case 'object':
							$data = get_object_vars($data);
						case 'array':
							$output_index_count = 0;
							$output_indexed = array();
							$output_associative = array();
							foreach ($data as $key => $value) {
								$output_indexed[] = json_encode($value);
								$output_associative[] = json_encode($key) . ':' . json_encode($value);
								if ($output_index_count !== NULL && $output_index_count++ !== $key) {
									$output_index_count = NULL;
								}
							}
							if ($output_index_count !== NULL) {
								return '[' . implode(',', $output_indexed) . ']';
							} else {
								return '{' . implode(',', $output_associative) . '}';
							}
						default:
							return ''; // Not supported
					}
				}
			}
			$data = array();
			$sql = "select DISTINCT PhysicalLocation from INTRANET_DR_ENTRY WHERE PhysicalLocation IS NOT NULL AND PhysicalLocation <> '' ORDER BY PhysicalLocation";
			$result = $this->returnArray($sql);
		
			if(!empty($result)){
				foreach ($result as $row){
					$data[] = array('value' => $this->handle_chars(htmlspecialchars($row[0])), 'title' => $this->handle_chars(htmlspecialchars($row[0])));
				}
			}
			return json_encode($data);
		}
		
		function handle_chars($str){
			$x = '';
			if($str != ''){
				$x = str_replace("\\", "\\\\", $str);
			}
			return $x;
		}
		
		function getUserFileStarRecords($FileIdAry='')
		{
			$sql = "SELECT FileID FROM INTRANET_DR_FILE_STAR WHERE UserID='".$_SESSION['UserID']."'";
			if($FileIdAry!='' && count($FileIdAry)>0){
				$FileIdAry = IntegerSafe($FileIdAry);
				$sql .= " AND FileID IN ('".implode("','",$FileIdAry)."')";
			}
			$records = $this->returnVector($sql);
			return $records;
		}
		
		function addUserFileStarRecord($ParUserId,$FileId)
		{
			$ParUserId = IntegerSafe($ParUserId);
			$FileId = IntegerSafe($FileId);
			$sql = "INSERT IGNORE INTO INTRANET_DR_FILE_STAR (FileID,UserID,DateInput) VALUES ('$FileId','$ParUserId',NOW())";
			$result = $this->db_db_query($sql);
			return $result;
		}
		
		function deleteUserFileStarRecord($FileIdAry)
		{
			$result = false;
			if(count($FileIdAry)>0){
				$FileIdAry = IntegerSafe($FileIdAry);
				$sql = "DELETE FROM INTRANET_DR_FILE_STAR WHERE FileID IN ('".implode("','",(array)$FileIdAry)."')";
				$result = $this->db_db_query($sql);
			}
			return $result;
		}
		
		/*
		 * @Description: Get all starred files related to a document entry's routing feedbacks
		 */
		function getUserStarredFiles($DocumentId)
		{
			global $docRoutingConfig;
			
			$DocumentId = IntegerSafe($DocumentId);
			
			//$files = $this->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $DocumentId, '', true);
			$sql = "SELECT DISTINCT f.FeedbackID 
					FROM INTRANET_DR_ROUTES as r 
					INNER JOIN INTRANET_DR_ROUTE_FEEDBACK as f On f.RouteID=r.RouteID 
					WHERE r.DocumentID='$DocumentId'";
			$feedbackIdAry = $this->returnVector($sql);
			
			$files = $this->getDRAttachment($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'], $feedbackIdAry, '', true);
			
			return $files;
		}
		
		function getSignEmailDocument($User_Id,$DocumentId)
		{
			$User_Id = IntegerSafe($User_Id);
			$DocumentId = IntegerSafe($DocumentId);
			
			$sql = "SELECT * FROM INTRANET_DR_SIGN_DOCUMENT WHERE UserID='$User_Id' AND DocumentID='$DocumentId'";
			return $this->returnResultSet($sql);
		}
		
		function getSignEmailDocumentByToken($Token)
		{
			$sql = "SELECT d.*,u.RecordType as UserType FROM INTRANET_DR_SIGN_DOCUMENT as d INNER JOIN INTRANET_USER as u ON u.UserID=d.UserID WHERE d.Token='$Token'";
			return $this->returnResultSet($sql);
		}
		
		function getSignEmailDocumentLastSentTime($DocumentId)
		{
			$DocumentId = IntegerSafe($DocumentId);
			
			$sql = "SELECT MAX(DateModified) FROM INTRANET_DR_SIGN_DOCUMENT WHERE DocumentID='$DocumentId'";
			$record = $this->returnVector($sql);
			return $record[0];
		}
		
		function getSignEmailRoute($SignDocumentID, $RouteID='')
		{
			$SignDocumentID = IntegerSafe($SignDocumentID);
			
			$sql = "SELECT * FROM INTRANET_DR_SIGN_ROUTE WHERE SignDocumentID='$SignDocumentID' ";
			if($RouteID != ''){
				$RouteID = IntegerSafe($RouteID);
				$sql .= " AND RouteID='$RouteID' ";
			}
			return $this->returnResultSet($sql);
		}
		
		function getRoutesSignCountByToken($Token)
		{
			$sql = "SELECT SUM(r.Signed) as TotalSigned,COUNT(*) as TotalRoute 
					FROM INTRANET_DR_SIGN_DOCUMENT as d 
					INNER JOIN INTRANET_DR_SIGN_ROUTE as r ON r.SignDocumentID=d.RecordID  
					WHERE d.Token='$Token'
					GROUP BY d.RecordID";
			$result = $this->returnResultSet($sql);
			//debug_r($result);
			//$returnAry = array('TotalSigned'=>$result[0]['TotalSigned'],'TotalRoute'=>$result[0]['TotalRoute']);
			return $result[0];
		}
		
		function sendSignEmail($params)
		{
			global $docRoutingConfig, $plugin, $sys_custom, $intranet_root, $Lang, $indexVar;
			
			include_once($intranet_root.'/includes/libwebmail.php');
			
			$lwebmail = new libwebmail();
			$input_user_id = IntegerSafe($indexVar['drUserId']);
			$document_id = IntegerSafe($params['DocumentID']);
			$target_user = IntegerSafe($this->stripWordFromArray($params['TargetUser'],'U')); // UserID array
			$target_user_count = count($target_user);
			$target_email = $params['TargetEmail']; // Email type to send : [UserEmail], [iMailPlus], [iMail]
			$target_email_count = count($target_email);
			$target_route = $params['TargetRoute']; // RouteID array
			$target_route_count = count($target_route);
			
			//$Lang['DocRouting']['SignEmailTitle'] = "New routing titled <!--TITLE--> from <!--SCHOOL_NAME-->";
			//$Lang['DocRouting']['SignEmailContent'] = '<p>You are invited to follow/sign a document issued by <!--SCHOOL_NAME-->.</p><p>Click the link below to start: </p><p><a href="<!--LINK-->" target="_blank"><!--LINK--></a></p>';
			
			$entry = $this->getEntryData($document_id);
			$document_title = $entry[0]['Title'];
			$instruction = $entry[0]['Instruction'];
			$school_name = GET_SCHOOL_NAME();
			
			$email_title = str_replace('<!--TITLE-->',$document_title,$Lang['DocRouting']['SendSignEmailTitle']);
			$email_title = str_replace('<!--SCHOOL_NAME-->',$school_name,$email_title);
			$email_content = str_replace('<!--SCHOOL_NAME-->',$school_name,$Lang['DocRouting']['SendSignEmailContent']);
			
			$from_email = $lwebmail->GetWebmasterMailAddress();
			
			$sql = "SELECT UserID,UserEmail,ImapUserEmail FROM INTRANET_USER WHERE UserID IN ('".implode("','",$target_user)."')";
			$user_ary = $this->returnResultSet($sql);
			$user_info = array();
			for($i=0;$i<count($user_ary);$i++){
				$user_info[$user_ary[$i]['UserID']] = $user_ary[$i];
			}
			
			$result = array();
			for($i=0;$i<$target_user_count;$i++){
				
				$user_id = $target_user[$i];
				if($user_id == '') continue;
				
				$token = md5(uniqid($document_id.'_'.$user_id));
				
				$exist_sign_document = $this->getSignEmailDocument($user_id,$document_id);
				if(count($exist_sign_document)>0){
					$sign_document_id = $exist_sign_document[0]['RecordID'];
					$token = $exist_sign_document[0]['Token'];
					$sql = "UPDATE INTRANET_DR_SIGN_DOCUMENT SET DateModified=NOW(),ModifiedBy='$input_user_id' WHERE UserID='$user_id' AND DocumentID='$document_id'";
					$result[$user_id.'_UpdateSignDoc'] = $this->db_db_query($sql);
					$exist_sign_route_ary = $this->getSignEmailRoute($sign_document_id);
					$exist_route_id_ary = Get_Array_By_Key($exist_sign_route_ary,'RouteID');
					$new_route_id_ary = array_values(array_unique(array_diff($target_route,$exist_route_id_ary)));
				}else{
					$sql = "INSERT INTO INTRANET_DR_SIGN_DOCUMENT (Token,UserID,DocumentID,DateInput,DateModified,InputBy,ModifiedBy) 
							VALUES ('$token','$user_id','$document_id',NOW(),NOW(),'$input_user_id','$input_user_id')";
					$result[$user_id.'_AddSignDoc'] = $this->db_db_query($sql); 
					$sign_document_id = $this->db_insert_id();
					$new_route_id_ary = $target_route;
				}
				
				for($j=0;$j<count($new_route_id_ary);$j++){
					$route_id = $new_route_id_ary[$j];
					$sql = "INSERT INTO INTRANET_DR_SIGN_ROUTE (SignDocumentID,RouteID,DateInput,DateModified) VALUES ('$sign_document_id','$route_id',NOW(),NOW())";
					$result[$user_id.'_AddSignRoute'] = $this->db_db_query($sql);
				}
				
				$pe_link = $this->getEncryptedParameter('management','sign_document',array('token'=>$token));
				//$link = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=".$pe_link."&tok=".$token;
				$link = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/home/drs.php?tok=".$token;
				$send_email_content = str_replace('<!--LINK-->',$link,$email_content);
				$send_email_content .= '<hr><p>'.$instruction.'</p>';
				
				if(in_array('iMail',$target_email)){
					$result[$user_id.'_SendCampusMail'] = $lwebmail->insertCampusMail($user_id, $input_user_id, "U".$user_id, "", "", "", "", "", $email_title, $send_email_content, 'utf-8', '', '', 1, 2, '');
				}
				if(in_array('iMailPlus',$target_email) && $user_info[$user_id]['ImapUserEmail']!=''){
					$result[$user_id.'_SendiMailPlus'] = $lwebmail->sendMail($email_title,$send_email_content,$from_email,array($user_info[$user_id]['ImapUserEmail']),array(),array(),"","","","",null,0);
				}
				if(in_array('UserEmail',$target_email) && $user_info[$user_id]['UserEmail']!=''){
					$result[$user_id.'_SendUserEmail'] = $lwebmail->sendMail($email_title,$send_email_content,$from_email,array($user_info[$user_id]['UserEmail']),array(),array(),"","","","",null,0);
				}
			}
			//debug_r($params);
			//debug_r($result);
			
			return !in_array(false,$result);
		}
		
		function signDocumentRouting($RecordId)
		{
			$RecordId = IntegerSafe($RecordId);
			$sql = "UPDATE INTRANET_DR_SIGN_ROUTE SET Signed='1',IPAddress='".getRemoteIpAddress()."',DateModified=NOW() WHERE RecordID='$RecordId' AND (Signed IS NULL OR Signed<>'1')";
			$result = $this->db_db_query($sql);
			return $result;
		}
		
		function unsetSignDocumentSession()
		{
			global $indexVar, $sys_custom;
			
			if(isset($_REQUEST['tok']) && $_REQUEST['tok']!='' && isset($indexVar['tok'])){
				if(isset($_SESSION['DR_TMP_USERID'])){ // if is current logined user, restore the session
					//$_SESSION['UserID'] = $_SESSION['DR_TMP_USERID'];
					//$_SESSION['UserType'] = $_SESSION['DR_TMP_USERTYPE'];
					unset($_SESSION['UserID']);
					unset($_SESSION['UserType']);
					if(!$sys_custom['DocRouting']['RequirePasswordToSign'])
					{
						unset($_SESSION['DR_TMP_USERID']);
						unset($_SESSION['DR_TMP_USERTYPE']);
					}
				}else{ // not logined user, destroy all session data
					unset($_SESSION['UserID']);
					unset($_SESSION['UserType']);
					//session_destroy();
				}
			}
		}
		
		function sendNotificationEmailForRouteSigned($routeId, $signedUserId, $customContent='') {
			global $Lang, $docRoutingConfig, $intranet_root, $indexVar, $PATH_WRT_ROOT, $replySlipConfig, $tableReplySlipConfig;
			include_once($intranet_root."/includes/replySlip/libReplySlipMgr.php");
			include_once($intranet_root."/includes/tableReplySlip/libTableReplySlipMgr.php");
			include_once($intranet_root."/includes/libfilesystem.php");
			
			$lwebmail = new libwebmail();
			$libReplySlipMgr = new libReplySlipMgr();
			$libTableReplySlipMgr = new libTableReplySlipMgr();
			$indexVar['libReplySlipMgr'] = $libReplySlipMgr;
			$indexVar['libTableReplySlipMgr'] = $libTableReplySlipMgr;
			
			// get route info
			$routeInfoAry = $this->getRouteData('', $routeId);
			$documentId = $routeInfoAry[0]['DocumentID'];
			
			// get document info
			$documentInfoAry = $this->getEntryData($documentId);
			$documentName = $documentInfoAry[0]['Title'];
			
			// get target user of the route
			$signedTime = '';
			$routingUserIdAry = $this->getRouteTargetUsers($routeId, $docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['AccessRight']['Route']);
			for($i=0;$i<count($routingUserIdAry);$i++){
				if($routingUserIdAry[$i]['UserID'] == $signedUserId){
					$signedTime = $routingUserIdAry[$i]['DateModified'];
					break;
				}
			}
			
			// build email title and content
			$email_title = $Lang['DocRouting']['EmailNotification_RouteSigned']['EmailTitle'];
			$email_title = str_replace('<!--DocTitle-->',$documentName,$email_title);
			$email_content = $Lang['DocRouting']['EmailNotification_RouteSigned']['EmailContent'];
			if($customContent != '')
				$email_content = $customContent;
			$email_content = str_replace('<!--DocTitle-->',$documentName,$email_content);
			$email_content = str_replace('<!--SignedTime-->',$signedTime,$email_content);
			
			if(!$indexVar['libDocRouting_ui']){
				$indexVar['libDocRouting_ui'] = new libDocRouting_ui();
			}
			//ob_start();
			//include($intranet_root.'/home/eAdmin/ResourcesMgmt/DocRouting/templates/management/print_document_style.tmpl.php');
			//$styleheet = ob_get_contents();
			//ob_end_clean();
			$send_success = false;
			if($indexVar['libDocRouting_ui']){
				
				$libfs = $this->getLibFileSystemInstance();
				$drUserID = IntegerSafe($indexVar['drUserId']);
				$tmp_folder = $docRoutingConfig['archiveToDigitalArchiveTempPath'];
				$dir_path = $tmp_folder."/u".$drUserID.'_alertemail';
				
				$parentFolderPath = substr($tmp_folder,0,strrpos($tmp_folder,'/'));
				if (!file_exists($parentFolderPath)) {
					$libfs->folder_new($parentFolderPath);
					$libfs->chmod_R($parentFolderPath,0777);
				}
				if (!file_exists($tmp_folder)) {
					$libfs->folder_new($tmp_folder);
					$libfs->chmod_R($tmp_folder,0777);
				}
				
				if(file_exists($dir_path)){
					$libfs->folder_remove_recursive($dir_path);
				}
				
				$libfs->folder_new($dir_path);
				$libfs->chmod_R($dir_path, 0777);
				
				$own_feedbacks = $indexVar['libDocRouting_ui']->getDocumentDetailPrintPage($documentId,false,true,$routeId);
				$docData = $indexVar['libDocRouting_ui']->getArchivedDocument($documentId);
				//$title_for_doc_file = $docData['Title'];
				$title_for_doc_file = 'document';
				$html_content = $docData['Content'];
				$doc_file_full_path = $dir_path.'/'.$title_for_doc_file.'.html';

				$libfs->file_write($html_content,$doc_file_full_path);
				
				$doc_file_name = $title_for_doc_file.'.html';
				$gzip_file_name = $title_for_doc_file.'.html.gz';
				$gzip_cmd = 'gzip \''.$doc_file_name.'\'';
				chdir($dir_path);
				shell_exec($gzip_cmd);
				
				$file_size = filesize($dir_path.'/'.$gzip_file_name);
				$target_folders = array();
				if($file_size > 7*1024*1024){ // if file size > 7MB, split it
					$split_success = false;
					$gzip_file_name_000 = $dir_path.'/'.$gzip_file_name.'.000';
					$cmd = 'split -d -a 3 -b 7m \''.$gzip_file_name.'\' \''.$gzip_file_name.'.\'';
					shell_exec($cmd);
					if(file_exists($gzip_file_name_000)){
						// remove the .html.gz file, keep the splitted files
						$libfs->file_remove($dir_path.'/'.$gzip_file_name);
						$split_success = true;
					}
					if($split_success){
						$files_full_path_ary = explode("\n",trim(shell_exec('find \''.$dir_path.'\' -type f')));
						sort($files_full_path_ary, SORT_STRING);
						for($i=0;$i<count($files_full_path_ary);$i++){
							$tmp_full_path = trim($files_full_path_ary[$i]);
							if($tmp_full_path == '') continue;
							$last_dot_pos = strrpos($tmp_full_path,'.');
							$last_slash_pos = strrpos($tmp_full_path,'/');
							$subfix = substr($tmp_full_path,$last_dot_pos+1); // 000, 001, 002, etc
							$prefix = substr($tmp_full_path,$last_slash_pos + 1,$last_dot_pos - $last_slash_pos); // extract file name up to and include the last dot
							$numeric_subfix = intval($subfix)+1; // increment one 000 >>> 001 as splitted zip file starts from .001
							$rename_subfix = sprintf("%03s",$numeric_subfix);
							$rename_filename = $prefix.$rename_subfix;
							if(strlen($subfix) == 3){ // should be three digits
								$new_subfolder = $dir_path.'/'.$rename_subfix;
								$libfs->folder_new($new_subfolder);
								$libfs->chmod_R($new_subfolder, 0777);
								//$libfs->file_copy($tmp_full_path, $new_subfolder);
								copy($tmp_full_path,$new_subfolder.'/'.$rename_filename);
								$target_folders[] = $new_subfolder;
							}
						}
					}
				}else
				{
					$target_folders[] = $dir_path;
				}
				
				//$tmp_content .= $styleheet;
				$tmp_content .= $own_feedbacks;
				$tmp_content = str_replace(array("\r\n","\n","\r"),"",$tmp_content);
				$email_content .= $tmp_content;
				
				$target_folder_count = count($target_folders);
				$email_remark = str_replace('<!--NUMBER_OF_FILE-->',$target_folder_count,$Lang['DocRouting']['EmailSendArchivedDocument']['EmailRemark']);
				for($k=0;$k<$target_folder_count;$k++)
				{
					$attachment_folder = $target_folders[$k];
					if($target_folder_count > 1){
						$cur_email_title = $email_title.' [Part #'.($k+1).']';
						$cur_email_content = $email_content.'<br /><p>'.$email_remark.'</p>';
					}else{
						$cur_email_title = $email_title;
						$cur_email_content = $email_content;
					}
				
					$send_success = $lwebmail->sendModuleMail(array($signedUserId), $cur_email_title, $cur_email_content, 1, $attachment_folder);
				}
				if(file_exists($dir_path)){
					$libfs->folder_remove_recursive($dir_path);
				}
			}
			
			return $send_success;
		}
		
		function sendArchivedDocumentEmail($params)
		{
			global $Lang, $docRoutingConfig, $intranet_root, $indexVar, $PATH_WRT_ROOT, $replySlipConfig, $tableReplySlipConfig;
			include_once($intranet_root."/includes/libfilesystem.php");
			include_once($intranet_root.'/includes/libwebmail.php');
			
			$lwebmail = new libwebmail();
			
			$input_user_id = $indexVar['drUserId'];
			$document_id = $params['DocumentID'];
			$target_user = $this->stripWordFromArray($params['TargetUser'],'U'); // UserID array
			$target_user_count = count($target_user);
			$target_email = $params['TargetEmail']; // Email type to send : [UserEmail], [iMailPlus], [iMail]
			$target_email_count = count($target_email);
			
			if(!$indexVar['libDocRouting_ui']){
				$indexVar['libDocRouting_ui'] = new libDocRouting_ui();
			}
			
			// get document info
			$documentInfoAry = $this->getEntryData($document_id);
			$documentName = $documentInfoAry[0]['Title'];
			
			$email_title = $Lang['DocRouting']['EmailSendArchivedDocument']['EmailTitle'];
			$email_title = str_replace('<!--DocTitle-->',$documentName,$email_title);
			$email_content = $Lang['DocRouting']['EmailSendArchivedDocument']['EmailContent'];
			$email_content = str_replace('<!--DocTitle-->',$documentName,$email_content);
			
			$result = array();
			if($indexVar['libDocRouting_ui']){
				
				$libfs = $this->getLibFileSystemInstance();
				$drUserID = IntegerSafe($indexVar['drUserId']);
				$tmp_folder = $docRoutingConfig['archiveToDigitalArchiveTempPath'];
				$dir_path = $tmp_folder."/u".$drUserID."_email";
				
				$parentFolderPath = substr($tmp_folder,0,strrpos($tmp_folder,'/'));
				if (!file_exists($parentFolderPath)) {
					$libfs->folder_new($parentFolderPath);
					$libfs->chmod_R($parentFolderPath,0777);
				}
				if (!file_exists($tmp_folder)) {
					$libfs->folder_new($tmp_folder);
					$libfs->chmod_R($tmp_folder,0777);
				}
				
				if(file_exists($dir_path)){
					$libfs->folder_remove_recursive($dir_path);
				}
				
				$libfs->folder_new($dir_path);
				$libfs->chmod_R($dir_path, 0777);
				
				$docData = $indexVar['libDocRouting_ui']->getArchivedDocument($document_id);
				//$title_for_doc_file = $docData['Title'];
				$title_for_doc_file = 'document';
				$html_content = $docData['Content'];
				$doc_file_full_path = $dir_path.'/'.$title_for_doc_file.'.html';

				$libfs->file_write($html_content,$doc_file_full_path);
				//$file_size = filesize($doc_file_full_path);
				
				$target_folders = array();
				
				$doc_file_name = $title_for_doc_file.'.html';
				$gzip_file_name = $title_for_doc_file.'.html.gz';
				$gzip_cmd = 'gzip \''.$doc_file_name.'\'';
				chdir($dir_path);
				shell_exec($gzip_cmd);
				$file_size = filesize($dir_path.'/'.$gzip_file_name);
				
				if($file_size > 7*1024*1024){ // if file size > 7MB, split it
					$split_success = false;
					//$doc_file_name = $title_for_doc_file.'.html';
					//$gzip_file_name = $title_for_doc_file.'.html.gz';
					$gzip_file_name_000 = $dir_path.'/'.$gzip_file_name.'.000';
					
					//$gzip_cmd = 'gzip \''.$doc_file_name.'\'';
					$cmd = 'split -d -a 3 -b 7m \''.$gzip_file_name.'\' \''.$gzip_file_name.'.\'';
					//chdir($dir_path);
					//shell_exec($gzip_cmd);
					shell_exec($cmd);
					if(file_exists($gzip_file_name_000)){
						// remove the .html.gz file, keep the splitted files
						//$libfs->file_remove($doc_file_full_path);
						$libfs->file_remove($dir_path.'/'.$gzip_file_name);
						$split_success = true;
					}
					if($split_success){
						$files_full_path_ary = explode("\n",trim(shell_exec('find \''.$dir_path.'\' -type f')));
						sort($files_full_path_ary, SORT_STRING);
						for($i=0;$i<count($files_full_path_ary);$i++){
							$tmp_full_path = trim($files_full_path_ary[$i]);
							if($tmp_full_path == '') continue;
							$last_dot_pos = strrpos($tmp_full_path,'.');
							$last_slash_pos = strrpos($tmp_full_path,'/');
							$subfix = substr($tmp_full_path,$last_dot_pos+1); // 000, 001, 002, etc
							$prefix = substr($tmp_full_path,$last_slash_pos + 1,$last_dot_pos - $last_slash_pos); // extract file name up to and include the last dot
							$numeric_subfix = intval($subfix)+1; // increment one 000 >>> 001 as splitted zip file starts from .001
							$rename_subfix = sprintf("%03s",$numeric_subfix);
							$rename_filename = $prefix.$rename_subfix;
							if(strlen($subfix) == 3){ // should be three digits
								$new_subfolder = $dir_path.'/'.$rename_subfix;
								$libfs->folder_new($new_subfolder);
								$libfs->chmod_R($new_subfolder, 0777);
								//$libfs->file_copy($tmp_full_path, $new_subfolder);
								copy($tmp_full_path,$new_subfolder.'/'.$rename_filename);
								$target_folders[] = $new_subfolder;
							}
						}
					}
				}else
				{
					$target_folders[] = $dir_path;
				}
				//debug_r($target_folders);
				$from_email = $lwebmail->GetWebmasterMailAddress();
			
				$sql = "SELECT UserID,UserEmail,ImapUserEmail FROM INTRANET_USER WHERE UserID IN ('".implode("','",$target_user)."')";
				$user_ary = $this->returnResultSet($sql);
				$user_info = array();
				for($i=0;$i<count($user_ary);$i++){
					$user_info[$user_ary[$i]['UserID']] = $user_ary[$i];
				}
				
				$target_folder_count = count($target_folders);
				$email_remark = str_replace('<!--NUMBER_OF_FILE-->',$target_folder_count,$Lang['DocRouting']['EmailSendArchivedDocument']['EmailRemark']);
				for($k=0;$k<$target_folder_count;$k++)
				{
					$attachment_folder = $target_folders[$k];
					if($target_folder_count > 1){
						$cur_email_title = $email_title.' [Part #'.($k+1).']';
						$cur_email_content = $email_content.'<br /><p>'.$email_remark.'</p>';
					}else{
						$cur_email_title = $email_title;
						$cur_email_content = $email_content;
					}
					for($i=0;$i<$target_user_count;$i++)
					{
						$user_id = $target_user[$i];
						if($user_id == '') continue;
						
						if(in_array('iMail',$target_email)){
							$result[$user_id.'_SendCampusMail'] = $lwebmail->insertCampusMail($user_id, $input_user_id, "U".$user_id, "", "", "", "", "", $cur_email_title, $cur_email_content, 'utf-8', '', '', 1, 2, $attachment_folder);
						}
						if(in_array('iMailPlus',$target_email) && $user_info[$user_id]['ImapUserEmail']!=''){
							$result[$user_id.'_SendiMailPlus'] = $lwebmail->sendMail($cur_email_title,$cur_email_content,$from_email,array($user_info[$user_id]['ImapUserEmail']),array(),array(),$attachment_folder,"","","",null,0);
						}
						if(in_array('UserEmail',$target_email) && $user_info[$user_id]['UserEmail']!=''){
							$result[$user_id.'_SendUserEmail'] = $lwebmail->sendMail($cur_email_title,$cur_email_content,$from_email,array($user_info[$user_id]['UserEmail']),array(),array(),$attachment_folder,"","","",null,0);
						}
					}
				}
				if(file_exists($dir_path)){
					$libfs->folder_remove_recursive($dir_path);
				}
			}
			
			return !in_array(false,$result);
		}
		
		function getAttachmentDisplaySize($size_bytes)
		{
			if($size_bytes < 1024){
				$display_size = $size_bytes.' bytes';
			}else if($size_bytes < 1024 * 1024){
				$display_size = sprintf("%.2f",$size_bytes/1024).' KB';
			}else{
				$display_size = sprintf("%.2f",($size_bytes/(1024*1024))).' MB';
			}
			return $display_size;
		}
	}
}
?>