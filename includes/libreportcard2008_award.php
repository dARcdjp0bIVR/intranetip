<?php
/** 
 * [Modification Log] Modifying By: Bill
 * *******************************************
 * 
 * 2016-08-09: Bill	[2016-0224-1423-31073]
 * - modified Get_Report_Award_Csv_Header_Info(), display Subject WebSAMS Code as optional field
 *  ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_MGR_NotRequireSubj'] = true)
 * 
 * 2016-06-30: Bill [2015-1104-1130-08164]
 * - added Load_All_School_Award_Info(), to return all awards in school
 * - added Update_All_Award_Info(), Get_Import_All_Temp_Data, Delete_Import_All_Temp_Data(), to handle import awards
 * 
 * 2016-02-03: Bill [2016-0202-1501-00207]
 * - modified Load_Report_Award_Info(), Get_Report_Award_Full_List(), support change in award name display
 * 
 * 2015-02-04: Bill [2014-1014-1144-21164]
 * - modified Generate_Satisfied_Criteria_PersonalChar_Student_Array() - support CUST
 * 
 * *******************************************
 */ 

class libreportcard_award extends libreportcardcustom {
	
	/*
	 * $GlobalInfoArr['ReportAwardInfoArr'][$ReportID][$thisAwardID]['BasicInfo']['AwardCode', 'AwardNameEn', 'AwardNameCh', ...] = $Value
	 * $GlobalInfoArr['ReportAwardInfoArr'][$ReportID][$thisAwardID]['ApplicableFormInfo'][$YearID]['Quota', ...] = $Value
	 * $GlobalInfoArr['ReportAwardInfoArr'][$ReportID][$thisAwardID]['CriteriaInfo'][$CriteriaID]['CriteriaType', 'RankField', ...] = $Value
	 * 
	 * $GlobalInfoArr['AwardInfoArr'][$AwardID]['AwardCode', 'AwardNameEn', 'AwardNameCh', ...] = $Value
	 * 
	 * $GlobalInfoArr['AwardGenerationReportType'][$ReportID] = 'T' or 'W'
	 */
	private $GlobalInfoArr;
	
	public function libreportcard_award() {
		parent::libreportcardcustom();
	}
	
	public function Insert_Award_Record($DataArr)
	{
		if (count((array)$DataArr) == 0)
			return false;
			
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value)
		{
			$fieldArr[] = $field;
			$valueArr[] = "'".$this->Get_Safe_Sql_Query($value)."'";
		}
		
		## set others fields
		$InsertUserID = ($_SESSION['UserID'])? "'".$_SESSION['UserID']."'" : 'NULL';
		# Input By
		$fieldArr[] = 'InputBy';
		$valueArr[] = $InsertUserID;
		# DateInput
		$fieldArr[] = 'InputDate';
		$valueArr[] = 'now()';
		# Last Modified By
		$fieldArr[] = 'LastModifiedBy';
		$valueArr[] = $InsertUserID;		
		# DateModified
		$fieldArr[] = 'LastModifiedDate';
		$valueArr[] = 'now()';
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		# Insert Record
		$RC_AWARD = $this->DBName.".RC_AWARD";
		$sql = "Insert Into $RC_AWARD 
					( ".$fieldText." )
					Values
					( ".$valueText." )
				";		
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	private function Delete_Award_Applicable_Form_Info($AwardID, $ReportID) {
		$RC_AWARD_APPLICABLE_FORM = $this->DBName.".RC_AWARD_APPLICABLE_FORM_INFO";
		$sql = "Delete From $RC_AWARD_APPLICABLE_FORM Where ReportID = '".$ReportID."' And AwardID = '".$AwardID."'";
		return $this->db_db_query($sql);
	}
	
	public function Update_Award_Applicable_Form_Info($AwardID, $ReportID, $ApplicableFormInfoArr) {
		if (count((array)$ApplicableFormInfoArr) == 0 ) {
			return false;
		}
		
		$this->Start_Trans();
		
		### Delete old records
		$SuccessArr['Delete'] = $this->Delete_Award_Applicable_Form_Info($AwardID, $ReportID);
		
		$valueArr = array();
		$numOfForm = count($ApplicableFormInfoArr);
		for ($i=0; $i<$numOfForm; $i++) {
			$thisYearID = $ApplicableFormInfoArr[$i]['YearID'];
			$thisQuota = $ApplicableFormInfoArr[$i]['Quota'];
			$thisQuota = ($thisQuota=='')? 0 : $thisQuota;
			
			$valueArr[] = "('".$AwardID."', '".$ReportID."', '".$thisYearID."', '".$thisQuota."')";
		}
		$valueText = implode(',', $valueArr);
		
		### Insert new records
		$RC_AWARD_APPLICABLE_FORM = $this->DBName.".RC_AWARD_APPLICABLE_FORM_INFO";
		$sql = "Insert Into $RC_AWARD_APPLICABLE_FORM (AwardID, ReportID, YearID, Quota) Values $valueText";
		$SuccessArr['Insert'] = $this->db_db_query($sql);
		
		if (in_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
//	private function Delete_Award_Special_Settings($AwardID, $ReportID) {
//		$RC_AWARD_SPECIAL_SETTINGS = $this->DBName.".RC_AWARD_SPECIAL_SETTINGS";
//		$sql = "Delete From $RC_AWARD_SPECIAL_SETTINGS Where ReportID = '".$ReportID."' And AwardID = '".$AwardID."'";
//		return $this->db_db_query($sql);
//	}
//	
//	public function Update_Award_Special_Settings($AwardID, $ReportID, $DataArr) {
//		if (count((array)$DataArr) == 0 ) {
//			return false;
//		}
//		
//		$this->Start_Trans();
//		
//		### Delete old records
//		$SuccessArr['Delete'] = $this->Delete_Award_Special_Settings($AwardID, $ReportID);
//		
//		# set field and value string
//		$fieldArr = array();
//		$valueArr = array();
//		$fieldArr[] = 'AwardID';
//		$valueArr[] = $AwardID;
//		$fieldArr[] = 'ReportID';
//		$valueArr[] = $ReportID;
//		foreach ($DataArr as $field => $value)
//		{
//			$fieldArr[] = $field;
//			$valueArr[] = "'".$this->Get_Safe_Sql_Query($value)."'";
//		}
//		$fieldText = implode(", ", $fieldArr);
//		$valueText = implode(", ", $valueArr);
//		
//		### Insert new records
//		$RC_AWARD_SPECIAL_SETTINGS = $this->DBName.".RC_AWARD_SPECIAL_SETTINGS";
//		$sql = "Insert Into $RC_AWARD_SPECIAL_SETTINGS 
//					( $fieldText ) 
//					Values 
//					( $valueText )";
//		$SuccessArr['Insert'] = $this->db_db_query($sql);
//		
//		if (in_array(false, $SuccessArr))
//		{
//			$this->RollBack_Trans();
//			return 0;
//		}
//		else
//		{
//			$this->Commit_Trans();
//			return 1;
//		}
//	}
	
	private function Delete_Award_Criteria($AwardID, $ReportID) {
		$RC_AWARD_CRITERIA = $this->DBName.".RC_AWARD_CRITERIA";
		$sql = "Delete From $RC_AWARD_CRITERIA Where ReportID = '".$ReportID."' And AwardID = '".$AwardID."'";
		return $this->db_db_query($sql);
	}
	
	public function Update_Award_Criteria($AwardID, $ReportID, $DataArr) {
		$numOfData = count((array)$DataArr);
		if ($numOfData == 0) {
			return false;
		}
		
		$this->Start_Trans();
		
		### Delete old records
		$SuccessArr['Delete'] = $this->Delete_Award_Criteria($AwardID, $ReportID);
		
		# set field and value string
		$RC_AWARD_CRITERIA = $this->DBName.".RC_AWARD_CRITERIA";
		for ($i=0; $i<$numOfData; $i++) {
			$_dataArr = $DataArr[$i];
			
			$_fieldArr = array();
			$_valueArr = array();
			$_fieldArr[] = 'AwardID';
			$_valueArr[] = $AwardID;
			$_fieldArr[] = 'ReportID';
			$_valueArr[] = $ReportID;			
			foreach ($_dataArr as $_field => $_value)
			{
				$_fieldArr[] = $_field;
				$_valueArr[] = "'".$this->Get_Safe_Sql_Query($_value)."'";
			}
			$_fieldText = implode(", ", $_fieldArr);
			$_valueText = implode(", ", $_valueArr);
			
			### Insert new records
			$sql = "Insert Into $RC_AWARD_CRITERIA 
						( $_fieldText ) 
						Values 
						( $_valueText )";
			(array)$SuccessArr['Insert'][] = $this->db_db_query($sql);
		}
		
		
		if (in_multi_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	public function Delete_Award_Generated_Student_Record($ReportIDArr='', $RecordIDArr='', $AwardIDArr='', $StudentIDArr='') {
		
		if ($ReportIDArr != '') {
			$conds_ReportID = " And ReportID In ('".implode("','", (array)$ReportIDArr)."') ";
		}
		
		if ($RecordIDArr != '') {
			$conds_RecordID = " And RecordID In ('".implode("','", (array)$RecordIDArr)."') ";
		}
		
		if ($AwardIDArr != '') {
			$conds_AwardID = " And AwardID In ('".implode("','", (array)$AwardIDArr)."') ";
		}
		
		if ($StudentIDArr != '') {
			$conds_StudentID = " And StudentID In ('".implode("','", (array)$StudentIDArr)."') ";
		}
		
		$RC_AWARD_GENERATED_STUDENT_RECORD = $this->DBName.".RC_AWARD_GENERATED_STUDENT_RECORD";
		
		// get related award to re-generate ranking after the data deletion
		$sql = "Select
						ReportID, AwardID, SubjectID
				From
						$RC_AWARD_GENERATED_STUDENT_RECORD
				Where
						1
						$conds_ReportID
						$conds_RecordID
						$conds_AwardID
						$conds_StudentID
				";
		$ReorderRankAwardArr = $this->returnArray($sql);
		$ReorderRankAwardAssoArr = BuildMultiKeyAssoc($ReorderRankAwardArr, array('ReportID', 'AwardID', 'SubjectID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
		
		$SuccessArr = array();
		$sql = "Delete From 
						$RC_AWARD_GENERATED_STUDENT_RECORD 
				Where 
						1
						$conds_ReportID
						$conds_RecordID
						$conds_AwardID
						$conds_StudentID
				";
		$SuccessArr['Delete'] = $this->db_db_query($sql);
		
		foreach ((array)$ReorderRankAwardAssoArr as $thisReportID => $thisReportInfoArr) {
			foreach ((array)$thisReportInfoArr as $thisAwardID => $thisReportAwardInfoArr) {
				foreach ((array)$thisReportAwardInfoArr as $thisSubjectID => $thisReportAwardSubjectInfoArr) {
					$SuccessArr['Reorder'][$thisReportID.'_'.$thisAwardID.'_'.$thisSubjectID] = $this->Regenerate_Award_Generated_Student_AwardRank($thisReportID, $thisAwardID, $thisSubjectID);
				}
			}
		}
		
		return (in_array(false, $SuccessArr))? false : true;
	}
	
	public function Update_Award_Generated_Student_Record($AwardID, $ReportID, $DataArr) {
		$numOfData = count((array)$DataArr);
		if ($numOfData == 0) {
			return true;
		}
		
		$this->Start_Trans();
		
		# set field and value string
		$RC_AWARD_GENERATED_STUDENT_RECORD = $this->DBName.".RC_AWARD_GENERATED_STUDENT_RECORD";
		for ($i=0; $i<$numOfData; $i++) {
			$_dataArr = $DataArr[$i];
			
			$_fieldArr = array();
			$_valueArr = array();
			$_fieldArr[] = 'AwardID';
			$_valueArr[] = $AwardID;
			$_fieldArr[] = 'ReportID';
			$_valueArr[] = $ReportID;			
			foreach ($_dataArr as $_field => $_value)
			{
				$_fieldArr[] = $_field;
				$_valueArr[] = "'".$this->Get_Safe_Sql_Query($_value)."'";
			}
			$_fieldArr[] = 'InputDate';
			$_valueArr[] = 'now()';
			$_fieldArr[] = 'InputBy';
			$_valueArr[] = $_SESSION['UserID'];
			
			$_fieldText = implode(", ", $_fieldArr);
			$_valueText = implode(", ", $_valueArr);
			
			### Insert new records
			$sql = "Insert Into $RC_AWARD_GENERATED_STUDENT_RECORD 
						( $_fieldText ) 
						Values 
						( $_valueText )";
			(array)$SuccessArr['Insert'][] = $this->db_db_query($sql);
		}
		
		if (in_multi_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
	public function Update_Award_Generated_Student_AwardRank($RecordID, $AwardRank) {
		$RC_AWARD_GENERATED_STUDENT_RECORD = $this->DBName.".RC_AWARD_GENERATED_STUDENT_RECORD";
		
		$sql = "Update $RC_AWARD_GENERATED_STUDENT_RECORD Set AwardRank = '".$AwardRank."' Where RecordID = '".$RecordID."'";
		$Success = $this->db_db_query($sql);
		
		return $Success;
	}
	
	private function Regenerate_Award_Generated_Student_AwardRank($ReportID, $AwardID, $SubjectID) {
		$RecordInfoArr = $this->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr='', $ReturnAsso=0, $AwardNameWithSubject=1, $AwardID, $SubjectID, $ShowInReportCardOnly=0, $OrderByAwardRank=1);
		$numOfRecord = count($RecordInfoArr);
		
		$SuccessArr = array();
		$AwardRank = 0;
		for ($i=0; $i<$numOfRecord; $i++) {
			$thisRecordID = $RecordInfoArr[$i]['RecordID'];
			$SuccessArr[$thisRecordID] = $this->Update_Award_Generated_Student_AwardRank($thisRecordID, ++$AwardRank);
		}
		
		return (in_array(false, $SuccessArr))? false : true;
	}
	
	protected function Delete_Award_Student_Record($ReportID) {
		$RC_AWARD_STUDENT_RECORD = $this->DBName.".RC_AWARD_STUDENT_RECORD";
		$sql = "Delete From $RC_AWARD_STUDENT_RECORD Where ReportID = '".$ReportID."'";
		return $this->db_db_query($sql);
	}
	
	public function Update_Award_Student_Record($ReportID, $DataArr, $UpdateLastModified=0)
	{
		// return true becos really have chance that no one get award
		if (count((array)$DataArr) == 0 ) {
			//return false;
			return true;
		}
		
		$this->Start_Trans();
		
		$RC_AWARD_STUDENT_RECORD = $this->DBName.".RC_AWARD_STUDENT_RECORD";
		$valueArr = array();
		$numOfStudent = count((array)$DataArr);
		for ($i=0; $i<$numOfStudent; $i++) {
			$thisRecordID = $DataArr[$i]['RecordID'];
			
			if ($thisRecordID == '') {
				// insert
				$_fieldArr = array();
				$_valueArr = array();
				$_fieldArr[] = 'ReportID';
				$_valueArr[] = $ReportID;			
				foreach ($DataArr[$i] as $_field => $_value)
				{
					$_fieldArr[] = $_field;
					$_valueArr[] = "'".$this->Get_Safe_Sql_Query($_value)."'";
				}
				$_fieldArr[] = 'InputDate';
				$_valueArr[] = 'now()';
				$_fieldArr[] = 'InputBy';
				$_valueArr[] = $_SESSION['UserID'];
				
				if ($UpdateLastModified) {
					$_fieldArr[] = 'LastModifiedDate';
					$_valueArr[] = 'now()';
					$_fieldArr[] = 'LastModifiedBy';
					$_valueArr[] = $_SESSION['UserID'];
				}
				
				$_fieldText = implode(", ", $_fieldArr);
				$_valueText = implode(", ", $_valueArr);
				
				### Insert new records
				$sql = "Insert Into $RC_AWARD_STUDENT_RECORD 
							( $_fieldText ) 
							Values 
							( $_valueText )";
				$SuccessArr['Insert'][] = $this->db_db_query($sql);
			}
			else {
				// update
				$valueFieldText = '';
				foreach ($DataArr[$i] as $field => $value)
				{
					$valueFieldText .= $field." = '".$this->Get_Safe_Sql_Query($value)."', ";
				}
				$valueFieldText .= ' LastModifiedDate = now(), ';
				$LastModifiedBy = ($_SESSION['UserID'])? "'".$_SESSION['UserID']."'" : 'NULL';
				$valueFieldText .= ' LastModifiedBy = '.$LastModifiedBy.' ';
				
				### Insert new records
				$sql = "Update $RC_AWARD_STUDENT_RECORD Set $valueFieldText Where RecordID = '".$thisRecordID."'";
				$SuccessArr['Update'][$thisRecordID] = $this->db_db_query($sql);
			}
		}
				
		if (in_multi_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
	public function Update_All_Award_Info($parAwardId, $dataAry, $updateLastModified=true) {
		if (!is_array($dataAry) || count($dataAry) == 0)
			return false;
		
		// Build field update values string
		$valueFieldAry = array();
		foreach ($dataAry as $field => $value) {
			$valueFieldAry[] = " $field = '".$this->Get_Safe_Sql_Query($value)."' ";
		}
		
		if ($updateLastModified) {
			$valueFieldAry[] = " LastModifiedDate = now() ";
			$valueFieldAry[] = " LastModifiedBy = '".$_SESSION['UserID']."' ";
		}
		$valueFieldText .= implode(',', $valueFieldAry);
		
		$table = $this->DBName.".RC_AWARD";
		$sql = "Update $table Set $valueFieldText Where AwardID = '$parAwardId'";
		$success = $this->db_db_query($sql);
		
		return ($success)? $parAwardId : -1;
	}
	
	public function Get_Award_Info($AwardID='') {
		if (!isset($this->GlobalInfoArr['AwardInfoArr'])) {
			$this->GlobalInfoArr['AwardInfoArr'] = BuildMultiKeyAssoc($this->Load_Batch_Award_Info(), 'AwardID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
		}
		
		if ($AwardID == '') {
			return $this->GlobalInfoArr['AwardInfoArr'];
		}
		else {
			return $this->GlobalInfoArr['AwardInfoArr'][$AwardID];
		}
	}
	
	public function Get_Award_Info_By_Code($AwardCode='') {
		if (!isset($this->GlobalInfoArr['AwardInfoCodeArr'])) {
			$this->GlobalInfoArr['AwardInfoCodeArr'] = BuildMultiKeyAssoc($this->Load_Batch_Award_Info(), 'AwardCode', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
		}
		
		if ($AwardCode == '') {
			return $this->GlobalInfoArr['AwardInfoCodeArr'];
		}
		else {
			return $this->GlobalInfoArr['AwardInfoCodeArr'][$AwardCode];
		}
	}
	
	private function Load_Batch_Award_Info() {
		global $ReportCardCustomSchoolName;
		
		$RC_AWARD = $this->DBName.".RC_AWARD";
		$sql = "Select
						AwardID,
						AwardCode,
						AwardNameEn,
						AwardNameCh,
						AwardType
				From
						$RC_AWARD
				Where
						SchoolCode = '".$ReportCardCustomSchoolName."'
				Order By
						DisplayOrder
				";
		return $this->returnArray($sql, null, 1);
	}
	
	public function Get_Report_Award_Info($ReportID, $MapByCode=0) 
	{
		if (!isset($this->GlobalInfoArr['ReportAwardInfoArr'][$ReportID][$MapByCode])) {
			$this->Load_Report_Award_Info($ReportID, $MapByCode);
		}
		
		return $this->GlobalInfoArr['ReportAwardInfoArr'][$ReportID][$MapByCode];
	}
	
	private function Load_Report_Award_Info($ReportID, $MapByCode)
	{
		global $ReportCardCustomSchoolName, $eRCTemplateSetting;
		
		### Get Report Info
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportTitle = $ReportInfoArr['ReportTitle'];
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semseter = $ReportInfoArr['Semester'];
		$SemseterNo = $this->Get_Semester_Seq_Number($Semseter);
		$ReportType = $this->Get_Award_Generate_Report_Type($ReportID);
		
		$RC_AWARD = $this->DBName.".RC_AWARD";
		$RC_AWARD_APPLICABLE_FORM_INFO = $this->DBName.".RC_AWARD_APPLICABLE_FORM_INFO";
		$RC_AWARD_SPECIAL_SETTINGS = $this->DBName.".RC_AWARD_SPECIAL_SETTINGS";
		$RC_AWARD_CRITERIA = $this->DBName.".RC_AWARD_CRITERIA";
		$sql = "Select
						a.AwardID,
						a.AwardCode,
						a.AwardNameEn,
						a.AwardNameCh,
						a.AwardType
				From
						$RC_AWARD as a
						Inner Join $RC_AWARD_APPLICABLE_FORM_INFO as aafi On (a.AwardID = aafi.AwardID)
				Where
						a.SchoolCode = '".$ReportCardCustomSchoolName."'
						And (a.ApplicableReportType = 'ALL' Or a.ApplicableReportType = '".$ReportType."')
						And aafi.YearID = '".$ClassLevelID."'
				Order By
						a.DisplayOrder
				";
		$AwardInfoArr = $this->returnArray($sql, null, 1);
		$AwardIDArr = Get_Array_By_Key($AwardInfoArr, 'AwardID');
		$numOfAward = count($AwardInfoArr);
		
		$AwardInfoAssoArr = array();
		$DefaultReportID = 0;
		if ($numOfAward > 0)
		{
			### Get Applicable Form Info
			$sql = "Select
							AwardID,
							ReportID,
							YearID,
							Quota
					From
							$RC_AWARD_APPLICABLE_FORM_INFO
					Where
							(ReportID = '".$DefaultReportID."' Or ReportID = '".$ReportID."')
							And AwardID In ('".implode("','", (array)$AwardIDArr)."')
					";
			$ApplicableFormInfoArr = $this->returnArray($sql, null, 1);
			$ApplicableFormInfoAssoArr = BuildMultiKeyAssoc($ApplicableFormInfoArr, array('AwardID', 'ReportID', 'YearID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
			
			
			### Get Criteria Info
			$sql = "Select
							CriteriaID,
							AwardID,
							ReportID,
							CriteriaType,
							PreviousCriteriaRelation,
							RankField,
							FromRank,
							ToRank,
							MinPersonalCharCode,
							ImprovementField,
							MinImprovement,
							FromReportID,
							FromReportColumnID,
							FromMustBePass,
							ToReportID,
							ToReportColumnID,
							ToMustBePass,
							LinkedAwardID,
							ExcludeOriginalAward
					From
							$RC_AWARD_CRITERIA
					Where
							(ReportID = '".$DefaultReportID."' Or ReportID = '".$ReportID."')
							And
							AwardID In ('".implode("','", (array)$AwardIDArr)."')
					Order By
							CriteriaOrder
					";
			$CriteriaInfoArr = $this->returnArray($sql, null, 1);
			$CriteriaInfoAssoArr = BuildMultiKeyAssoc($CriteriaInfoArr, array('AwardID', 'ReportID', 'CriteriaID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
			
			
			### Get Special Settings
//			$sql = "Select
//							SpecialSettingsID,
//							AwardID,
//							ReportID,
//							FromRank,
//							ToRank,
//							SpecialAwardNameEn,
//							SpecialAwardNameCh
//					From
//							$RC_AWARD_SPECIAL_SETTINGS
//					Where
//							(ReportID = '".$DefaultReportID."' Or ReportID = '".$ReportID."')
//							And AwardID In ('".implode("','", (array)$AwardIDArr)."')
//					";
//			$SpecialInfoArr = $this->returnArray($sql);
//			$SpecialInfoAssoArr = BuildMultiKeyAssoc($SpecialInfoArr, array('AwardID', 'ReportID', 'SpecialSettingsID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
			
			
			### Build Asso Array
			for ($i=0; $i<$numOfAward; $i++)
			{
				$thisAwardID = $AwardInfoArr[$i]['AwardID'];
				$thisAwardCode = $AwardInfoArr[$i]['AwardCode'];
				$thisAwardKey = ($MapByCode==1)? $thisAwardCode : $thisAwardID;
				
				// [2016-0202-1501-00207] Change Award Name if config is set
				if(isset($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardNameDisplay'][$thisAwardID][$ClassLevelID][$SemseterNo])){
					$AwardInfoArr[$i]["AwardNameEn"] = $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardNameDisplay'][$thisAwardID][$ClassLevelID][$SemseterNo];
					$AwardInfoArr[$i]["AwardNameCh"] = $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardNameDisplay'][$thisAwardID][$ClassLevelID][$SemseterNo];
				}
				
				$AwardInfoAssoArr[$thisAwardKey]['BasicInfo'] = $AwardInfoArr[$i];
				
				# Applicable Form Info
				if (isset($ApplicableFormInfoAssoArr[$thisAwardID][$ReportID])) {
					$AwardInfoAssoArr[$thisAwardKey]['ApplicableFormInfo'] = $ApplicableFormInfoAssoArr[$thisAwardID][$ReportID];
				}
				else if (isset($ApplicableFormInfoAssoArr[$thisAwardID][$DefaultReportID])) {
					$AwardInfoAssoArr[$thisAwardKey]['ApplicableFormInfo'] = $ApplicableFormInfoAssoArr[$thisAwardID][$DefaultReportID];
				}
				else {
					$AwardInfoAssoArr[$thisAwardKey]['ApplicableFormInfo'] = array();
				}
				
				# Criteria Info
				if (isset($CriteriaInfoAssoArr[$thisAwardID][$ReportID])) {
					$AwardInfoAssoArr[$thisAwardKey]['CriteriaInfo'] = $CriteriaInfoAssoArr[$thisAwardID][$ReportID];
				}
				else if (isset($CriteriaInfoAssoArr[$thisAwardID][$DefaultReportID])) {
					$AwardInfoAssoArr[$thisAwardKey]['CriteriaInfo'] = $CriteriaInfoAssoArr[$thisAwardID][$DefaultReportID];
				}
				else {
					$AwardInfoAssoArr[$thisAwardKey]['CriteriaInfo'] = array();
				}
				
				# Special Settings
//				if (isset($SpecialInfoAssoArr[$thisAwardID][$ReportID])) {
//					$AwardInfoAssoArr[$thisAwardKey]['SpecialSettings'] = $SpecialInfoAssoArr[$thisAwardID][$ReportID];
//				}
//				else if (isset($SpecialInfoAssoArr[$thisAwardID][$DefaultReportID])) {
//					$AwardInfoAssoArr[$thisAwardKey]['SpecialSettings'] = $SpecialInfoAssoArr[$thisAwardID][$DefaultReportID];
//				}
//				else {
//					$AwardInfoAssoArr[$thisAwardKey]['SpecialSettings'] = array();
//				}
			}
		}
		
		$this->GlobalInfoArr['ReportAwardInfoArr'][$ReportID][$MapByCode] = $AwardInfoAssoArr;
	}
	
	public function Get_Report_Award_Full_List($ReportID, $FromAwardTextOnly=0) {
		global $eRCTemplateSetting;
		
		# Get Report Info
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$Semseter = $ReportInfoArr['Semester'];
		$SemseterNo = $this->Get_Semester_Seq_Number($Semseter);
		
		$AwardInfoArr = $this->Get_Report_Award_Info($ReportID);
		
		$AwardNameArr = array();
		$AwardReturnArr = array();
		$AwardCounter = 0;
		if ($FromAwardTextOnly) {
			// do nth
		}
		else {
			foreach ((array)$AwardInfoArr as $thisAwardID => $thisAwardInfoArr) {
				$AwardReturnArr[$AwardCounter]['AwardName'] = trim($thisAwardInfoArr['BasicInfo']['AwardNameEn']);
				$AwardReturnArr[$AwardCounter]['AwardNameDisplay'] = $this->Get_Award_Name_Display($thisAwardID);
				
				// [2016-0202-1501-00207] Change Award Name if config is set
				if(isset($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardNameDisplay'][$thisAwardID][$ClassLevelID][$SemseterNo])){
					$AwardReturnArr[$AwardCounter]['AwardNameDisplay'] = $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardNameDisplay'][$thisAwardID][$ClassLevelID][$SemseterNo];
				}
				
				$AwardReturnArr[$AwardCounter]['AwardID'] = $thisAwardID;
				$AwardReturnArr[$AwardCounter]['AwardType'] = $thisAwardInfoArr['BasicInfo']['AwardType'];
				$AwardCounter++;
				
				$AwardNameArr[] = trim($thisAwardInfoArr['BasicInfo']['AwardNameEn']);
			}
		}
		
		
		$StudentAwardInfoArr = $this->Get_Award_Student_Record($ReportID, $StudentIDArr='', $ReturnAsso=0);
		$AwardTextArr = Get_Array_By_Key($StudentAwardInfoArr, 'AwardText');
		$numOfAwardText = count($AwardTextArr);
		for ($i=0; $i<$numOfAwardText; $i++) {
			$thisAwardText = trim($AwardTextArr[$i]);
			$thisAwardTextArr = explode("\n", $thisAwardText);
			$thisNumOfAward = count($thisAwardTextArr);
			
			for ($j=0; $j<$thisNumOfAward; $j++) {
				$thisAwardName = trim($thisAwardTextArr[$j]);
				
				if ($thisAwardName != '' && !in_array($thisAwardName, $AwardNameArr)) {
					$AwardReturnArr[$AwardCounter]['AwardName'] = $thisAwardName;
					$AwardReturnArr[$AwardCounter]['AwardNameDisplay'] = $thisAwardName;
					$AwardNameArr[] = $thisAwardName;
					
					$AwardCounter++;
				}
			}
		}
		
		// Sort by the Award Name
		array_multisort($AwardNameArr, SORT_STRING, $AwardReturnArr);
		
		return $AwardReturnArr;
	}
	
	public function Load_All_School_Award_Info($MapByCode=0, $awardBasicInfo=false)
	{
		global $ReportCardCustomSchoolName, $eRCTemplateSetting;
		
		if (!isset($this->GlobalInfoArr['SchoolAllAwardInfoArr']) || !isset($this->GlobalInfoArr['SchoolAllAwardBasicInfoArr'])) {
			$AwardInfoAssoArr = array();
			$AwardBasicInfoAssoArr = array();
			
			$RC_AWARD = $this->DBName.".RC_AWARD";
			$RC_AWARD_APPLICABLE_FORM_INFO = $this->DBName.".RC_AWARD_APPLICABLE_FORM_INFO";
			
			// Get Awards
			$sql = "Select
							a.AwardID,
							a.AwardCode,
							a.AwardNameEn,
							a.AwardNameCh,
							a.AwardType,
							a.InternalAwardCode,
							IF(a.InternalAwardCode IS NULL, '1', '0') As NotGenerateAward
					From
							$RC_AWARD as a
					Where
							a.SchoolCode = '".$ReportCardCustomSchoolName."'
					Order By
							a.DisplayOrder
					";				
			$AwardInfoArr = $this->returnArray($sql, null, 1);
			$AwardIDArr = Get_Array_By_Key($AwardInfoArr, 'AwardID');
			
			// loop awards
			$numOfAward = count($AwardInfoArr);
			if ($numOfAward > 0)
			{
				// Get Applicable Form Info
				$sql = "Select
								rc.AwardID,
								rc.ReportID,
								rc.YearID,
								rc.Quota,
								y.YearName
						From
								$RC_AWARD_APPLICABLE_FORM_INFO AS rc
								LEFT JOIN YEAR AS y ON (rc.YearID = y.YearID)
						Where
								rc.AwardID In ('".implode("','", (array)$AwardIDArr)."')
						Order By
								y.Sequence
						";
				$ApplicableFormInfoArr = $this->returnArray($sql, null, 1);
				$ApplicableFormInfoAssoArr = BuildMultiKeyAssoc($ApplicableFormInfoArr, array('AwardID', 'YearID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
				
				// loop awards
				for ($i=0; $i<$numOfAward; $i++)
				{
					$thisAwardID = $AwardInfoArr[$i]['AwardID'];
					$thisAwardCode = $AwardInfoArr[$i]['AwardCode'];
					$thisAwardKey = ($MapByCode==1)? $thisAwardCode : $thisAwardID;
					
					// Basic Info
					$AwardInfoAssoArr[$thisAwardKey]['BasicInfo'] = $AwardInfoArr[$i];
					$AwardBasicInfoAssoArr[$thisAwardKey] = $AwardInfoArr[$i];
						
					// Applicable Form Info
					$AwardInfoAssoArr[$thisAwardKey]['ApplicableFormInfo'] = $ApplicableFormInfoAssoArr[$thisAwardID];
					if (empty($AwardInfoAssoArr[$thisAwardKey]['ApplicableFormInfo'])) {
						$AwardInfoAssoArr[$thisAwardKey]['ApplicableFormInfo'] = array();
					}
				}
			}
			
			$this->GlobalInfoArr['SchoolAllAwardInfoArr'] = $AwardInfoAssoArr;
			$this->GlobalInfoArr['SchoolAllAwardBasicInfoArr'] = $AwardBasicInfoAssoArr;
		}
		
		$targetAry = $awardBasicInfo? $this->GlobalInfoArr['SchoolAllAwardBasicInfoArr'] : $this->GlobalInfoArr['SchoolAllAwardInfoArr'];
		return $targetAry;
	}
	
	public function Get_Student_With_AwardText($ReportID, $AwardText) {
		$AwardText = trim($AwardText);
		
		$RC_AWARD_STUDENT_RECORD = $this->DBName.".RC_AWARD_STUDENT_RECORD";
		$sql = "Select
						RecordID,
						StudentID,
						AwardText
				From
						$RC_AWARD_STUDENT_RECORD
				Where
						ReportID = '".$ReportID."'
						And AwardText Like '%".$this->Get_Safe_Sql_Like_Query($AwardText)."%'
				";
		return $this->returnArray($sql, null, 1);
	}
	
	public function Get_Award_Generate_Report_Type($ReportID) {
		if (!isset($this->GlobalInfoArr['AwardGenerationReportType'][$ReportID])) {
			$this->Load_Award_Generate_Report_Type($ReportID);
		}
		return $this->GlobalInfoArr['AwardGenerationReportType'][$ReportID];
	}
	
	private function Load_Award_Generate_Report_Type($ReportID) {
		global $eRCTemplateSetting, $PATH_WRT_ROOT;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ReportTerm = $ReportInfoArr['Semester'];
		$ReportType = ($ReportTerm == 'F')? 'W' : 'T';
		
		if ($ReportType=='T' && $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_UseLastTermReportAsConsolidatedReport']) {
			include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
			$libFCM = new form_class_manage();	
			$TermInfoArr = $libFCM->Get_Academic_Year_Term_List($this->schoolYearID);
			$numOfTerm = count($TermInfoArr);
			$LastYearTermID = $TermInfoArr[$numOfTerm-1]['YearTermID'];
			
			if ($ReportTerm == $LastYearTermID) {
				$ReportType = 'W';
			}
		}
		
		# Added for Sha Tin Methodist College - Form 6 - Term 1 - Set Consolidated Report - Set Report Type
		if(isset($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_TermReportAsConsolidatedReport_FormNumberAry']) 
			&& $this->Get_Semester_Seq_Number($ReportTerm) == 1 
			&& in_array($this->GET_FORM_NUMBER($ReportInfoArr['ClassLevelID'], true), $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_TermReportAsConsolidatedReport_FormNumberAry'])){
			$ReportType = 'W';
		}

		$this->GlobalInfoArr['AwardGenerationReportType'][$ReportID] = $ReportType;
	}
	
	public function Generate_Report_Award($ReportID, $AwardPageSettingsArr) {
		global $eRCTemplateSetting;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$yearTermId = $ReportInfoArr['Semester'];
		
		$AwardInfoArr = $this->Get_Report_Award_Info($ReportID);
		
		$awardIdWithCriteriaAry = array();
		foreach ((array)$AwardInfoArr as $_awardId => $_awardInfoAry) {
			$_numOfCriteria = count((array)$_awardInfoAry['CriteriaInfo']);
			
			if ($_numOfCriteria) {
				$awardIdWithCriteriaAry[] = $_awardId;
			}
		}
		
		// Loop each Awards
		$this->Start_Trans();
		$SuccessArr = array();
		$AwardStudentInfoArr = array();
		$ReGenerateAwardIDArr = array();
		foreach ((array)$AwardPageSettingsArr as $thisAwardID => $thisAwardPageSettingsArr) {
			
			if (!in_array($thisAwardID, $awardIdWithCriteriaAry)) {
				continue;
			}
			
			$thisAwardType = $AwardInfoArr[$thisAwardID]['BasicInfo']['AwardType']; 
			$thisAwardQuota = $AwardInfoArr[$thisAwardID]['ApplicableFormInfo'][$ClassLevelID]['Quota'];
			
			// Loop each Criteria
			$thisAwardStudentInfoArr = array();
			$thisCriteriaCount = 0;
			foreach ((array)$thisAwardPageSettingsArr as $thisCriteriaID => $thisCriteriaPageSettingsArr) {
				$thisCriteriaType = $AwardInfoArr[$thisAwardID]['CriteriaInfo'][$thisCriteriaID]['CriteriaType'];
				$thisPreviousCriteriaRelation = $AwardInfoArr[$thisAwardID]['CriteriaInfo'][$thisCriteriaID]['PreviousCriteriaRelation'];
				
				// Override the default AwardQuota if quota has been set in Award Generation page
				$thisAwardQuota = ($thisCriteriaPageSettingsArr['Quota']=='')? $thisAwardQuota : $thisCriteriaPageSettingsArr['Quota'];
				
				$thisSatisfiedStudentInfoArr = array();
				if ($thisCriteriaType == 'RANK') {
					$thisSatisfiedStudentInfoArr = $this->Generate_Satisfied_Criteria_Rank_Student_Array($ReportID, $thisAwardID, $thisCriteriaID, $thisCriteriaPageSettingsArr);
				}
				else if ($thisCriteriaType == 'PERSONAL_CHAR') {
					$thisSatisfiedStudentInfoArr = $this->Generate_Satisfied_Criteria_PersonalChar_Student_Array($ReportID, $thisAwardID, $thisCriteriaID, $thisCriteriaPageSettingsArr);
				}
				else if ($thisCriteriaType == 'IMPROVEMENT') {
					$thisSatisfiedStudentInfoArr = $this->Generate_Satisfied_Criteria_Improvement_Student_Array($ReportID, $thisAwardID, $thisCriteriaID, $thisCriteriaPageSettingsArr);
				}
				else if ($thisCriteriaType == 'AWARD_LINKAGE') {
					// Generate the Award List later after generating all awards first
					$ReGenerateAwardIDArr[] = $thisAwardID;
					continue;
				}
				
				// Merge the Array for the Award
				if ($thisCriteriaCount > 0) {
					$thisAwardStudentInfoArr = $this->Get_Combined_Satisfied_Criteria_Student_Array($thisPreviousCriteriaRelation, $thisAwardStudentInfoArr, $thisSatisfiedStudentInfoArr);
				}
				else {
					$thisAwardStudentInfoArr = $thisSatisfiedStudentInfoArr;
				}
				$thisCriteriaCount++;
			}
			
			# Remove the Student from the award list if excessed the quota
			if ($thisAwardQuota == 0) {
				// No Quota
			}
			else {
				foreach ((array)$thisAwardStudentInfoArr as $thisSubjectID => $thisAwardSubjectInfoArr) {
					foreach ((array)$thisAwardSubjectInfoArr as $thisStudentID => $thisAwardSubjectStudentInfoArr) {
						$thisAwardRank = $thisAwardSubjectStudentInfoArr['AwardRank'];
						if (!$this->Check_Within_Quota($thisAwardQuota, $thisAwardRank)) {
							unset($thisAwardStudentInfoArr[$thisSubjectID][$thisStudentID]);
						}
					}
				}
			}
			
			$AwardStudentInfoArr[$thisAwardID] = $thisAwardStudentInfoArr;
			unset($thisAwardStudentInfoArr);
		}	// End foreach ((array)$AwardPageSettingsArr as $thisAwardID => $thisAwardPageSettingsArr)
		
		
		### Generate the award again if the award has Award Linkage
		$ReGenerateAwardIDArr = array_values(array_unique($ReGenerateAwardIDArr));
		$numOfReGenerateAward = count($ReGenerateAwardIDArr);
		for ($i=0; $i<$numOfReGenerateAward; $i++) {
			$thisAwardID = $ReGenerateAwardIDArr[$i];
			
			if (!in_array($thisAwardID, $awardIdWithCriteriaAry)) {
				continue;
			}
			
			$thisAwardType = $AwardInfoArr[$thisAwardID]['BasicInfo']['AwardType']; 
			$thisAwardQuota = $AwardInfoArr[$thisAwardID]['ApplicableFormInfo'][$ClassLevelID]['Quota'];
			
			// Loop each Criteria
			$thisAwardStudentInfoArr = array();
			$thisCriteriaCount = 0;
			$thisAwardPageSettingsArr = $AwardPageSettingsArr[$thisAwardID];
			foreach ((array)$thisAwardPageSettingsArr as $thisCriteriaID => $thisCriteriaPageSettingsArr) {
				$thisCriteriaType = $AwardInfoArr[$thisAwardID]['CriteriaInfo'][$thisCriteriaID]['CriteriaType'];
				$thisPreviousCriteriaRelation = $AwardInfoArr[$thisAwardID]['CriteriaInfo'][$thisCriteriaID]['PreviousCriteriaRelation'];
				
				// Override the default AwardQuota if quota has been set in Award Generation page
				$thisAwardQuota = ($thisCriteriaPageSettingsArr['Quota']=='')? $thisAwardQuota : $thisCriteriaPageSettingsArr['Quota'];
				
				$thisSatisfiedStudentInfoArr = array();
				if ($thisCriteriaType == 'RANK') {
					$thisSatisfiedStudentInfoArr = $this->Generate_Satisfied_Criteria_Rank_Student_Array($ReportID, $thisAwardID, $thisCriteriaID, $thisCriteriaPageSettingsArr);
				}
				else if ($thisCriteriaType == 'PERSONAL_CHAR') {
					$thisSatisfiedStudentInfoArr = $this->Generate_Satisfied_Criteria_PersonalChar_Student_Array($ReportID, $thisAwardID, $thisCriteriaID, $thisCriteriaPageSettingsArr);
				}
				else if ($thisCriteriaType == 'IMPROVEMENT') {
					$thisSatisfiedStudentInfoArr = $this->Generate_Satisfied_Criteria_Improvement_Student_Array($ReportID, $thisAwardID, $thisCriteriaID, $thisCriteriaPageSettingsArr);
				}
				else if ($thisCriteriaType == 'AWARD_LINKAGE') {
					$thisLinkedAwardID = $thisCriteriaPageSettingsArr['LinkedAwardID'];
					$thisFromRank = $thisCriteriaPageSettingsArr['FromRank'];
					$thisToRank = $thisCriteriaPageSettingsArr['ToRank'];
					$thisExcludeOriginalAward = $thisCriteriaPageSettingsArr['ExcludeOriginalAward'];
					
					// Loop the Linked Award Student List to get the satisfy Students
					foreach ((array)$AwardStudentInfoArr[$thisLinkedAwardID] as $thisSubjectID => $thisAwardSubjectInfoArr) {
						foreach ((array)$thisAwardSubjectInfoArr as $thisStudentID => $thisAwardSubjectStudentInfoArr) {
							$thisAwardRank = $thisAwardSubjectStudentInfoArr['AwardRank'];
							
							if ($thisFromRank <= $thisAwardRank && $thisAwardRank <= $thisToRank) {
								$thisSatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['DetermineText'] = $AwardInfoArr[$thisLinkedAwardID]['BasicInfo']['AwardCode'];
								$thisSatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['AwardRank'] = $thisAwardRank;
								
								if ($thisExcludeOriginalAward) {
									unset($AwardStudentInfoArr[$thisLinkedAwardID][$thisSubjectID][$thisStudentID]);
								}
							}
						}
					}
				}
				
				// Merge the Array for the Award
				if ($thisCriteriaCount > 0) {
					$thisAwardStudentInfoArr = $this->Get_Combined_Satisfied_Criteria_Student_Array($thisPreviousCriteriaRelation, $thisAwardStudentInfoArr, $thisSatisfiedStudentInfoArr);
				}
				else {
					$thisAwardStudentInfoArr = $thisSatisfiedStudentInfoArr;
				}
				$thisCriteriaCount++;
			}
			
			# Remove the Student from the award list if excessed the quota
			if ($thisAwardQuota == 0) {
				// No Quota
			}
			else {
				foreach ((array)$thisAwardStudentInfoArr as $thisSubjectID => $thisAwardSubjectInfoArr) {
					foreach ((array)$thisAwardSubjectInfoArr as $thisStudentID => $thisAwardSubjectStudentInfoArr) {
						$thisAwardRank = $thisAwardSubjectStudentInfoArr['AwardRank'];
						if (!$this->Check_Within_Quota($thisAwardQuota, $thisAwardRank)) {
							unset($thisAwardStudentInfoArr[$thisSubjectID][$thisStudentID]);
						}
					}
				}
			}
			
			$AwardStudentInfoArr[$thisAwardID] = $thisAwardStudentInfoArr;
			unset($thisAwardStudentInfoArr);
		}
		
		if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_HideFormSubjectAwardIfOnlyOneSubjectGroup']) {
			if ($yearTermId != 'F') {
				$subjectGroupAssoAry = $this->Get_Subject_Group_List_Of_Report($ReportID, '', '', $IncludeSubject=true);
				
				foreach ((array)$AwardPageSettingsArr as $thisAwardID => $thisAwardPageSettingsArr) {
					
					if (!isset($AwardStudentInfoArr[$thisAwardID])) {
						continue;
					}
					
					$_awardCode = $AwardInfoArr[$thisAwardID]['BasicInfo']['AwardCode'];
					if ($_awardCode=='form_subject_prize_1' || $_awardCode=='form_subject_prize_2' || $_awardCode=='form_subject_prize_3') {
						$_subjectIdAry = array_values(array_unique(array_keys($AwardStudentInfoArr[$thisAwardID])));
						$_numOfSubject = count($_subjectIdAry);
						
						for ($i=0; $i<$_numOfSubject; $i++) {
							$__subjectId = $_subjectIdAry[$i];
							
							if (count((array)$subjectGroupAssoAry[$__subjectId])==1) {
								// do not have Form Subject Prize if there are only one subject group for the subject
								unset($AwardStudentInfoArr[$thisAwardID][$__subjectId]);
							}
						}
					}
				}
			}
		}
		
		unset($AwardInfoArr);
		
		### Convert to Update function format and Save to DB
		$SuccessArr['Delete_Old_Report_Award'] = $this->Delete_Award_Generated_Student_Record($ReportID, '', $awardIdWithCriteriaAry);
		
		foreach ((array)$AwardStudentInfoArr as $thisAwardID => $thisAwardStudentInfoArr) {
			if (!in_array($thisAwardID, $awardIdWithCriteriaAry)) {
				continue;
			}
			
			$AwardUpdateDBInfoArr = array();
			foreach ((array)$thisAwardStudentInfoArr as $thisSubjectID => $thisAwardSubjectInfoArr) {
				foreach ((array)$thisAwardSubjectInfoArr as $thisStudentID => $thisAwardSubjectStudentInfoArr) {
					$thisUpdateDBInfoArr = array();
					$thisUpdateDBInfoArr = $thisAwardSubjectStudentInfoArr;
					$thisUpdateDBInfoArr['SubjectID'] = $thisSubjectID;
					$thisUpdateDBInfoArr['StudentID'] = $thisStudentID;
					$AwardUpdateDBInfoArr[] = $thisUpdateDBInfoArr;
					
					unset($thisUpdateDBInfoArr);
				}
			}
			
			if (count((array)$AwardUpdateDBInfoArr) > 0) {
				$SuccessArr['Update_Award_Generated_Student_Record'][$thisAwardID] = $this->Update_Award_Generated_Student_Record($thisAwardID, $ReportID, $AwardUpdateDBInfoArr);
			}
			unset($AwardUpdateDBInfoArr);
		}
		
		### Save the Award Text
		$SuccessArr['Delete_Old_Report_Award_Text'] = $this->Delete_Award_Student_Record($ReportID);
		$SuccessArr['Generate_Report_Award_Text'] = $this->Generate_Report_Award_Text($ReportID);
		
		
		### Update Last Generated Date 
		$SuccessArr['Update_LastGeneratedAward_Date'] = $this->UPDATE_REPORT_LAST_DATE($ReportID, 'LastGeneratedAward');
		
		if (in_multi_array(false, $SuccessArr))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
	/*
	 * @return: If $AwardType = 'OVERALL', returns $Arr[0][$StudentID]['DetermineValue', ...] = $Value
	 * 			If $AwardType = 'SUBJECT', returns $Arr[$SubjectID][$StudentID]['DetermineValue', ...] = $Value
	 */
	private function Generate_Satisfied_Criteria_Rank_Student_Array($ReportID, $AwardID, $CriteriaID, $PageSettingsArr) {
		$AwardInfoArr = $this->Get_Report_Award_Info($ReportID);
		
		$AwardType = $AwardInfoArr[$AwardID]['BasicInfo']['AwardType'];
		$RankField = $PageSettingsArr['RankField'];
		$FromRank = $PageSettingsArr['FromRank'];
		$ToRank = $PageSettingsArr['ToRank'];
		
		### Consolidate the Students who satisfy the criteria
		$ReportColumnID = 0;
		// Get Marks
		if ($AwardType == 'OVERALL') {
			$GrandMarkArr = $this->getReportResultScore($ReportID, $ReportColumnID, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderInfoArr='');
			$MarkArr = $this->Add_SubjectID_In_GrandMarkArr($GrandMarkArr);
			unset($GrandMarkArr);
		}
		else if ($AwardType == 'SUBJECT') {
			$MarkArr = $this->getMarks($ReportID, $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=0, $OrderBy='', $SubjectID='', $ReportColumnID, $CheckPositionDisplay=0, $OrderInfoArr='');
		}
				
		
				
		// Consolidate Data
		$SatisfiedStudentInfoArr = array();
		foreach ((array)$MarkArr as $thisStudentID => $thisStudentMarkArr) {
			foreach ((array)$thisStudentMarkArr as $thisSubjectID => $thisStudentSubjectMarkArr) {
				$thisTargetValue = $thisStudentSubjectMarkArr[$ReportColumnID][$RankField];
				
				if ($FromRank <= $thisTargetValue && $thisTargetValue <= $ToRank) {
					$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisTargetValue;
					$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['RankField'] = $RankField;
				}
			}
		}
		
		### Sort the result
		$SatisfiedStudentInfoArr = $this->Sort_And_Add_AwardRank_In_InfoArr($SatisfiedStudentInfoArr, 'DetermineValue');
			
		unset($AwardInfoArr);
		unset($MarkArr);
		
		return $SatisfiedStudentInfoArr;
	}
	
	/*
	 * @return: If $AwardType = 'OVERALL', returns $Arr[0][$StudentID]['DetermineText'] = Value
	 * 			If $AwardType = 'SUBJECT', returns $Arr[$SubjectID][$StudentID]['DetermineText'] = Value
	 */
	private function Generate_Satisfied_Criteria_PersonalChar_Student_Array($ReportID, $AwardID, $CriteriaID, $PageSettingsArr) {
		global $eRCTemplateSetting;
		
		$AwardInfoArr = $this->Get_Report_Award_Info($ReportID, 0);
		$AwardType = $AwardInfoArr[$AwardID]['BasicInfo']['AwardType'];
		unset($AwardInfoArr);
		
		$MinPersonalCharCode = $PageSettingsArr['MinPersonalCharCode'];
		
		
		### Get Personal Char Grade which satisfy is equal to or higher than the MinPersonalCharCode
		$PersonalCharGradeCodeArr = array_keys($this->returnPersonalCharOptions($br=0, $order='', $Lang_Par=''));
		$TargetGradeCodeArr = array();
		$numOfCode = count($PersonalCharGradeCodeArr);
		for ($i=0; $i<$numOfCode; $i++) {
			$thisCode = $PersonalCharGradeCodeArr[$i];
			$TargetGradeCodeArr[] = $thisCode;
			
			if ($thisCode == $MinPersonalCharCode) {
				break;
			}
		}
		unset($PersonalCharGradeCodeArr);
		
		if($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ConsolidatedReportUseLastTermPersonalCharData']){
			$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
			if($ReportInfoArr["Semester"] == 'F'){
				$relatedReport = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
				$ReportID = $relatedReport[(count($relatedReport)-1)]['ReportID'];
			}
		}
		
		### Get Student Personal Char Result
		$StudentPersonalCharArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentID='', $ReportID, $SubjectID='', $ReturnGradeCode=1, $ReturnCharID=1);
		
		
		### Consolidate the Students who satisfy the criteria
		$SatisfiedStudentInfoArr = array();
		foreach ((array)$StudentPersonalCharArr[$ReportID] as $thisSubjectID => $thisSubjectInfoArr) {
			if ($AwardType == 'OVERALL' && $thisSubjectID != 0) {	// Overall Personal Char only
				continue;
			}
			if ($AwardType == 'SUBJECT' && $thisSubjectID == 0) {	// Subject Personal Char only
				continue;
			}
			
			foreach ((array)$thisSubjectInfoArr as $thisStudentID => $thisSubjectStudentPersonalCharArr) {
								
				// Check if all Grade is higher than the $MinPersonalCharCode
				$thisStudentSatisfied = true;
				$thisDetermineTextArr = array();
				foreach((array)$thisSubjectStudentPersonalCharArr as $thisCharID => $thisGradeCode) {
					if (!in_array($thisGradeCode, (array)$TargetGradeCodeArr)) {
						$thisStudentSatisfied = false;
						break;
					}
					else {
						$thisDetermineTextArr[] = $thisCharID.':'.$thisGradeCode;
					}
				}
				
				if ($thisStudentSatisfied) {
					$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['DetermineText'] = implode(',', (array)$thisDetermineTextArr);
				}
			}
		}
		unset($StudentPersonalCharArr);
		
		return $SatisfiedStudentInfoArr;
	}
	
	private function Generate_Satisfied_Criteria_Improvement_Student_Array($ReportID, $AwardID, $CriteriaID, $PageSettingsArr) {
		$AwardInfoArr = $this->Get_Report_Award_Info($ReportID);
		$AwardType = $AwardInfoArr[$AwardID]['BasicInfo']['AwardType'];
		unset($AwardInfoArr);
		
		### Get Criteria
		$FromReportID = $PageSettingsArr['FromReportID'];
		$FromReportColumnID = $PageSettingsArr['FromReportColumnID'];
		$FromMustBePass = ($PageSettingsArr['FromMustBePass']=='')? 0 : $PageSettingsArr['FromMustBePass'];
		$ToReportID = $PageSettingsArr['ToReportID'];
		$ToReportColumnID = $PageSettingsArr['ToReportColumnID'];
		$ToMustBePass = ($PageSettingsArr['ToMustBePass']=='')? 0 : $PageSettingsArr['ToMustBePass'];
		$ImprovementField = $PageSettingsArr['ImprovementField'];
		$MinImprovement = $PageSettingsArr['MinImprovement'];
		
		
		### Get Students
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
		$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
		$numOfStudent = count($StudentIDArr);
		unset($StudentInfoArr);
		
		
		### Get Subjects
		$SubjectsInfoArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=1, $ReportID);
		$SubjectIDArr = array_keys($SubjectsInfoArr);
		$SubjectIDArr[] = 0;
		$numOfSubject = count($SubjectIDArr);
		unset($StudentInfoArr);
		
		
		### Generate Result
		if ($AwardType == 'OVERALL') {
			// Get Mark Info
			$FromMarkArr = $this->getReportResultScore($FromReportID, $FromReportColumnID, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='');
			$ToMarkArr = $this->getReportResultScore($ToReportID, $ToReportColumnID, $StudentID='', $other_conds='', $debug=0, $includeAdjustedMarks=0, $CheckPositionDisplay=0, $FilterNoRanking=1, $OrderFieldArr='');
			
			// Get Mark Nature Info
			$FromMarkNatureArr = $this->Get_Student_Grand_Mark_Nature_Arr($FromReportID, $ClassLevelID, $FromMarkArr);
			$ToMarkNatureArr = $this->Get_Student_Grand_Mark_Nature_Arr($ToReportID, $ClassLevelID, $ToMarkArr);
			
			// Convert Array Format
			$FromMarkArr = $this->Add_SubjectID_In_GrandMarkArr($FromMarkArr);
			$ToMarkArr = $this->Add_SubjectID_In_GrandMarkArr($ToMarkArr);
			$FromMarkNatureArr = $this->Add_SubjectID_In_GrandMarkArr($FromMarkNatureArr);
			$ToMarkNatureArr = $this->Add_SubjectID_In_GrandMarkArr($ToMarkNatureArr);
		}
		else if ($AwardType == 'SUBJECT') {
			// Get Mark Info
			$FromMarkArr = $this->getMarks($FromReportID, $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=0, $OrderBy='', $SubjectID='', $FromReportColumnID, $CheckPositionDisplay=0, $SubOrderArr='');
			$ToMarkArr = $this->getMarks($ToReportID, $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=0, $OrderBy='', $SubjectID='', $ToReportColumnID, $CheckPositionDisplay=0, $SubOrderArr='');
			
			// Get Mark Nature Info
			$FromMarkNatureArr = $this->Get_Student_Subject_Mark_Nature_Arr($FromReportID, $ClassLevelID, $FromMarkArr);
			$ToMarkNatureArr = $this->Get_Student_Subject_Mark_Nature_Arr($ToReportID, $ClassLevelID, $ToMarkArr);
		}
		
		
		### Must Loop from $StudentIDArr and $SubjectIDArr as $MarkArr may not contains all Students and Subjects
		$SatisfiedStudentInfoArr = array();
		for ($i=0; $i<$numOfSubject; $i++) {
			$thisSubjectID = $SubjectIDArr[$i];
			if ($AwardType == 'OVERALL' && $thisSubjectID != 0) {	// Overall Personal Char only
				continue;
			}
			if ($AwardType == 'SUBJECT' && $thisSubjectID == 0) {	// Subject Personal Char only
				continue;
			}
			
			for ($j=0; $j<$numOfStudent; $j++) {
				$thisStudentID = $StudentIDArr[$j];
				
				// Check Special Case
				$thisFromGrade = $FromMarkArr[$thisStudentID][$thisSubjectID][$FromReportColumnID]['Grade'];
				$thisToGrade = $ToMarkArr[$thisStudentID][$thisSubjectID][$ToReportColumnID]['Grade'];
				if ($this->Check_If_Grade_Is_SpecialCase($thisFromGrade) || $this->Check_If_Grade_Is_SpecialCase($thisToGrade)) {
					continue;
				}
				
				// Get the From and To Value
				$thisFromValue = $FromMarkArr[$thisStudentID][$thisSubjectID][$FromReportColumnID][$ImprovementField];
				$thisToValue = $ToMarkArr[$thisStudentID][$thisSubjectID][$ToReportColumnID][$ImprovementField];
				
				$thisFromMarkNature = $FromMarkNatureArr[$thisStudentID][$thisSubjectID][$FromReportColumnID];
				$thisToMarkNature = $ToMarkNatureArr[$thisStudentID][$thisSubjectID][$ToReportColumnID];
				
				
				// Passing Checking
				$thisFromPass = true;
				if ($FromMustBePass && !$this->Check_MarkNature_Is_Pass($thisFromMarkNature)) {
					$thisFromPass = false;
				}
				
				$thisToPass = true;
				if ($ToMustBePass && !$this->Check_MarkNature_Is_Pass($thisToMarkNature)) {
					$thisToPass = false;
				}
				
				
				// Record the satisfied students in the array
				if (is_numeric($thisFromValue) && is_numeric($thisToValue) && $thisFromPass && $thisToPass) {
					$thisImprovement = $thisToValue - $thisFromValue; 
					if ($thisImprovement >= $MinImprovement) {
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['DetermineValue'] = $thisImprovement;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['ImprovementField'] = $ImprovementField;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['FromValue'] = $thisFromValue;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['FromNature'] = $thisFromMarkNature;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['ToValue'] = $thisToValue;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['ToNature'] = $thisToMarkNature;
						$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['ImprovementValue'] = $thisImprovement;
					}
				}
			}
		}
		
		### Sort and determine the improvement ranking
		$SatisfiedStudentInfoArr = $this->Sort_And_Add_AwardRank_In_InfoArr($SatisfiedStudentInfoArr, 'DetermineValue', 'desc');
		
		unset($FromMarkArr);
		unset($ToMarkArr);
		unset($FromMarkNatureArr);
		unset($ToMarkNatureArr);
		
		return $SatisfiedStudentInfoArr;
	}
	
	public function Sort_And_Add_AwardRank_In_InfoArr($SatisfiedStudentInfoArr, $RankingDetermineField, $Sorting='') {
		foreach ((array)$SatisfiedStudentInfoArr as $thisSubjectID => $thisSubjectInfoArr) {
			// Get all the values
			$thisSubjectTargetValueArr = array();
			foreach ((array)$thisSubjectInfoArr as $thisStudentID => $thisStudentSubjectMarkArr) {
				$thisSubjectTargetValueArr[] = $thisStudentSubjectMarkArr[$RankingDetermineField];
			}
			
			// Sort the array (no need array_unique so that the ranking is 1,2,2,4)
			if ($Sorting == 'desc') {
				arsort($thisSubjectTargetValueArr, SORT_NUMERIC);
				$thisSubjectTargetValueArr = array_values($thisSubjectTargetValueArr);
			}
			else {
				sort($thisSubjectTargetValueArr, SORT_NUMERIC);
			}
			
			// Search the index of the value in the array to calculate the ranking 
			foreach ((array)$thisSubjectInfoArr as $thisStudentID => $thisStudentSubjectMarkArr) {
				$thisAwardRank = array_search($thisStudentSubjectMarkArr[$RankingDetermineField], $thisSubjectTargetValueArr) + 1;
				$SatisfiedStudentInfoArr[$thisSubjectID][$thisStudentID]['AwardRank'] = $thisAwardRank;
			}
			unset($thisSubjectTargetValueArr);
		}
		
		return $SatisfiedStudentInfoArr;
	}
	
	private function Get_Combined_Satisfied_Criteria_Student_Array($CriteriaRelation, $AwardStudentInfoArr, $SatisfiedCriteriaStudentInfoArr) {
		### Get All SubjectID
		$SubjectIDArr = array_merge(array_keys((array)$AwardStudentInfoArr), (array)array_keys($SatisfiedCriteriaStudentInfoArr));
		$SubjectIDArr = array_values(array_unique($SubjectIDArr));
		$numOfSubject = count($SubjectIDArr);
				
		$CombinedArr = array();
		for ($i=0; $i<$numOfSubject; $i++) {
			$thisSubjectID = $SubjectIDArr[$i];
			
			### Get All Students involved in this Subject Awards
			$thisStudentIDArr = array_merge(array_keys((array)$AwardStudentInfoArr[$thisSubjectID]), array_keys((array)$SatisfiedCriteriaStudentInfoArr[$thisSubjectID]));
			$thisStudentIDArr = array_values(array_unique($thisStudentIDArr));
			$numOfStudent = count($thisStudentIDArr);
			
			for ($j=0; $j<$numOfStudent; $j++) {
				$thisStudentID = $thisStudentIDArr[$j];
				
				$thisStudentInAward = (isset($AwardStudentInfoArr[$thisSubjectID][$thisStudentID]))? true : false;
				$thisStudentSatisfyCriteria = (isset($SatisfiedCriteriaStudentInfoArr[$thisSubjectID][$thisStudentID]))? true : false;
				
				$thisHasAward = false;
				if ($CriteriaRelation == 'AND' && $thisStudentInAward && $thisStudentSatisfyCriteria) {
					$thisHasAward = true;
				}
				else if ($CriteriaRelation == 'OR' && ($thisStudentInAward || $thisStudentSatisfyCriteria)) {
					$thisHasAward = true;
				}
				
				if ($thisHasAward) {
					$CombinedArr[$thisSubjectID][$thisStudentID] = array_merge_recursive((array)$AwardStudentInfoArr[$thisSubjectID][$thisStudentID], (array)$SatisfiedCriteriaStudentInfoArr[$thisSubjectID][$thisStudentID]);
				}
			}
		}
		$CombinedArr = $this->Sort_And_Add_AwardRank_In_InfoArr($CombinedArr, 'DetermineValue');
		
		return $CombinedArr;
	}
	
	private function Check_Within_Quota($Quota, $Rank) {
		if ($Quota == 0) {
			$WithinQuota = true;
		}
		if ($Rank === null || $Rank === '' || $Rank <= $Quota) {
			$WithinQuota = true;
		}
		else {
			$WithinQuota = false;
		}
		
		return $WithinQuota;
	}
	
	private function Generate_Report_Award_Text($ReportID) {
		$StudentAwardAssoArr = $this->Get_Award_Generated_Student_Record($ReportID);
		
		$DataArr = array();
		foreach((array)$StudentAwardAssoArr as $thisStudentID => $thisStudentInfoArr) {
			$thisStudentAwardArr = array();
			foreach((array)$thisStudentInfoArr as $thisAwardID => $thisStudentAwardInfoArr) {
				foreach((array)$thisStudentAwardInfoArr as $thisSubjectID => $thisStudentAwardSubjectInfoArr) {
					$thisStudentAwardArr[] = $thisStudentAwardSubjectInfoArr['AwardName'];
				}
			}	
			
			if (count((array)$thisStudentAwardArr) > 0) {
				$thisDataArr = array();
				$thisDataArr['StudentID'] = $thisStudentID;
				$thisDataArr['AwardText'] = implode("\r\n", (array)$thisStudentAwardArr);
				$DataArr[] = $thisDataArr;
				unset($thisDataArr);
			}
			unset($thisStudentAwardArr);
		}
		
		return $this->Update_Award_Student_Record($ReportID, $DataArr);
	}
	
	public function Get_Award_Generated_Student_Record($ReportID, $StudentIDArr='', $ReturnAsso=1, $AwardNameWithSubject=1, $AwardIDArr='', $SubjectIDArr='', $ShowInReportCardOnly=0, $OrderByAwardRank=0) {
		global $eRCTemplateSetting, $intranet_session_language;
		
		$PreloadArrKey = 'Get_Award_Generated_Student_Record';
		$FuncArgArr = get_defined_vars();
		$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
    	if ($returnArr !== false) {
    		return $returnArr;
    	}
		
		$AwardInfoArr = $this->Get_Report_Award_Info($ReportID);
				
		if ($StudentIDArr != '') {
			$conds_StudentID = " And agsr.StudentID In ('".implode("','", (array)$StudentIDArr)."') ";
		}
		
		if ($AwardIDArr != '') {
			$conds_AwardID = " And agsr.AwardID In ('".implode("','", (array)$AwardIDArr)."') ";
		}
		
		if ($SubjectIDArr != '') {
			$conds_SubjectID = " And agsr.SubjectID In ('".implode("','", (array)$SubjectIDArr)."') ";
		}
		
		if ($ShowInReportCardOnly != 0) {
			$conds_ShowInReportCardOnly = " And award.ShowInReportCard = 1 ";
		}
		
		$RC_AWARD = $this->DBName.".RC_AWARD";
		$RC_AWARD_GENERATED_STUDENT_RECORD = $this->DBName.".RC_AWARD_GENERATED_STUDENT_RECORD";
		$sql = "Select
						agsr.RecordID,
						agsr.StudentID,
						agsr.AwardID,
						agsr.SubjectID,
						agsr.AwardRank,
						award.AwardCode,
						award.ShowInReportCard,
						award.AwardType
				From
						$RC_AWARD_GENERATED_STUDENT_RECORD as agsr
						Inner Join $RC_AWARD as award On (agsr.AwardID = award.AwardID)
				Where
						agsr.ReportID = '".$ReportID."'
						$conds_StudentID
						$conds_AwardID
						$conds_SubjectID
						$conds_ShowInReportCardOnly
				Order By
						award.DisplayOrder, agsr.AwardRank, agsr.InputDate
				";
		$StudentAwardArr = $this->returnArray($sql, null, 1);
		$numOfStudentAward = count($StudentAwardArr);
		
		for ($i=0; $i<$numOfStudentAward; $i++) {
			$thisStudentID = $StudentAwardArr[$i]['StudentID'];
			$thisAwardID = $StudentAwardArr[$i]['AwardID'];
			$thisSubjectID = $StudentAwardArr[$i]['SubjectID'];
			
			$thisAwardName = $AwardInfoArr[$thisAwardID]['BasicInfo']['AwardNameEn'];
								
			// Append Subject Name after the Award Name if the prize is Subject-based
			if ($thisSubjectID != 0 && $AwardNameWithSubject == 1) {
				$TargetLang = ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_SubjectNameAlwaysInEnglish'])? 'en' : $intranet_session_language;
				$thisSubjectName = $this->GET_SUBJECT_NAME_LANG($thisSubjectID, $TargetLang);
				$thisAwardName = $this->Get_Subject_Prize_Display($thisAwardName, $thisSubjectName);
			}
			$StudentAwardArr[$i]['AwardName'] = $thisAwardName;
			
			$StudentAwardArr[$i]['AwardNameEn'] = $AwardInfoArr[$thisAwardID]['BasicInfo']['AwardNameEn'];
			$StudentAwardArr[$i]['AwardNameCh'] = $AwardInfoArr[$thisAwardID]['BasicInfo']['AwardNameCh'];
		}
		
		if ($ReturnAsso) {
			$ReturnArr = BuildMultiKeyAssoc($StudentAwardArr, array('StudentID', 'AwardID', 'SubjectID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
		}
		else {
			$ReturnArr = $StudentAwardArr;
		} 
		
		$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
		return $ReturnArr;
	}
	
	public function Get_Award_Generated_Student_Record_Max_AwardRank($ReportID, $AwardID, $SubjectID) {
		$RC_AWARD_GENERATED_STUDENT_RECORD = $this->DBName.".RC_AWARD_GENERATED_STUDENT_RECORD";
		$sql = "Select 
						Max(AwardRank) as MaxAwardRank
				From
						$RC_AWARD_GENERATED_STUDENT_RECORD
				Where
						ReportID = '".$ReportID."'
						And AwardID = '".$AwardID."'
						And SubjectID = '".$SubjectID."'
				";
		$ReturnArr = $this->returnArray($sql);
		
		return $ReturnArr[0]['MaxAwardRank'];
	}
	
	public function Get_Award_Student_Record($ReportIDArr='', $StudentIDArr='', $ReturnAsso=1) {
		
		if ($ReportIDArr != '') {
			$conds_ReportID = " And ReportID In ('".implode("','", (array)$ReportIDArr)."') ";
		}
		if ($StudentIDArr != '') {
			$conds_StudentID = " And StudentID In ('".implode("','", (array)$StudentIDArr)."') ";
		}
		
		$RC_AWARD_STUDENT_RECORD = $this->DBName.".RC_AWARD_STUDENT_RECORD";
		$sql = "Select
						RecordID,
						ReportID,
						StudentID,
						AwardText
				From
						$RC_AWARD_STUDENT_RECORD
				Where
						1
						$conds_ReportID
						$conds_StudentID
				";
		$InfoArr = $this->returnArray($sql, null, 1);
		if ($ReturnAsso) {
			$ReturnArr = BuildMultiKeyAssoc($InfoArr, array('ReportID', 'StudentID'));
		}
		else {
			$ReturnArr = $InfoArr;
		}
		return $ReturnArr;
	}
	
	public function Get_Report_Award_Class_Last_Modified_Info($ReportID) {
		global $eRCTemplateSetting;
		
		$ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportInfoArr['ClassLevelID'];
		
		$StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
		$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
		
		if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
			$ModifiedDateField = 'InputDate';
			$ModifiedByField = 'InputBy';
			$RecordTable = $this->DBName.".RC_AWARD_GENERATED_STUDENT_RECORD";
		}
		else {
			$ModifiedDateField = 'LastModifiedDate';
			$ModifiedByField = 'LastModifiedBy';
			$RecordTable = $this->DBName.".RC_AWARD_STUDENT_RECORD";
		}
		
		$NameField = getNameFieldByLang("iu.");
		$RC_AWARD_STUDENT_RECORD = $this->DBName.".RC_AWARD_STUDENT_RECORD";
		$sql = "Select
						yc.YearClassID,
						Max(asr.$ModifiedDateField) as LastModifiedDate,
						asr.$ModifiedByField as LastModifiedBy,
						$NameField as LastModifiedUserName
				From
						$RecordTable as asr
						Inner Join YEAR_CLASS_USER as ycu On (asr.StudentID = ycu.UserID)
						Inner Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Inner Join INTRANET_USER as iu On (asr.$ModifiedByField = iu.UserID)
				Where
						asr.ReportID = '".$ReportID."'
						And asr.StudentID In ('".implode("','", (array)$StudentIDArr)."')
						And yc.AcademicYearID = '".$this->schoolYearID."'
				Group By
						yc.YearClassID
				";
		return BuildMultiKeyAssoc($this->returnArray($sql, null, 1), 'YearClassID');
	}
	
	public function Get_Award_Name_Display($AwardID, $WithAwardType=1) {
		global $Lang;
		
		$AwardInfoArr = $this->Get_Award_Info($AwardID);
				
		$AwardType = $AwardInfoArr['AwardType'];
		$AwardNameEn = $AwardInfoArr['AwardNameEn'];
		$AwardNameCh = $AwardInfoArr['AwardNameCh'];
		
		$AwardName = Get_Lang_Selection($AwardNameCh, $AwardNameEn);
		if ($WithAwardType && $AwardType=='SUBJECT') {
			$AwardName .= ' '.$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['SubjectPrizeSuffix'];
		}
		
		return $AwardName;
	}
	
	public function Get_Report_Award_Csv_Header_Info() {
		global $Lang, $eRCTemplateSetting;
		
		$InfoArr = array();
		$InfoArr[] = array(	"Property" => 1,
							"En" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['ClassName'],
							"Ch" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['ClassName'],
							);
		$InfoArr[] = array(	"Property" => 1,
							"En" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['ClassNumber'],
							"Ch" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['ClassNumber'],
							);
		$InfoArr[] = array(	"Property" => 1,
							"En" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['UserLogin'],
							"Ch" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['UserLogin'],
							);
		$InfoArr[] = array(	"Property" => 1,
							"En" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['WebSAMSRegNo'],
							"Ch" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['WebSAMSRegNo'],
							);
		$InfoArr[] = array(	"Property" => 2,
							"En" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['StudentName'],
							"Ch" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['StudentName'],
							);
		$InfoArr[] = array(	"Property" => ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly'])? 2 : 1,
							"En" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['Award'],
							"Ch" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['Award'],
							);
		
		if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
			$InfoArr[] = array(	"Property" => 1,
								"En" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardCode'],
								"Ch" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['AwardCode'],
								);
			$InfoArr[] = array(	"Property" => 2,
								"En" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['Subject'],
								"Ch" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['Subject'],
								);
			$InfoArr[] = array(	"Property" => ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_MGR_NotRequireSubj']? 2 : 1),
								"En" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['SubjectWebSAMSCode'],
								"Ch" => $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['SubjectWebSAMSCode'],
								);
		}					
		
		return $InfoArr;
	}
	
	public function Get_Report_Award_Csv_Header_Title()
	{
		$InfoArr = $this->Get_Report_Award_Csv_Header_Info();
		
		$ColumnTitleArr = array();
		$ColumnTitleArr['En'] = Get_Array_By_Key($InfoArr, 'En');
		$ColumnTitleArr['Ch'] = Get_Array_By_Key($InfoArr, 'Ch');
		
		return $ColumnTitleArr;
	}
	
	public function Get_Report_Award_Csv_Header_Property($forGetAllCsvContent=0)
	{
		global $oea_cfg;
		$PropertyArr = Get_Array_By_Key($this->Get_Report_Award_Csv_Header_Info(), 'Property');
		
		if ($forGetAllCsvContent == 1)
		{
			$numOfProperty = count($PropertyArr);
			for ($i=0; $i<$numOfProperty; $i++) {
				$PropertyArr[$i] = 1;
			}
		}
		
		return $PropertyArr;
	}
	
	public function Delete_Import_Award_Temp_Data($ReportID)
	{
		$RC_TEMP_IMPORT_AWARD = $this->DBName.'.RC_TEMP_IMPORT_AWARD';
		$sql = "Delete From $RC_TEMP_IMPORT_AWARD Where ImportUserID = '".$_SESSION["UserID"]."' And ReportID = '".$ReportID."'";
		$Success = $this->db_db_query($sql);
				
		return $Success;
	}
	
	public function Insert_Import_Award_Temp_Data($InsertValueArr) {
		if (count((array)$InsertValueArr) > 0)
		{
			$RC_TEMP_IMPORT_AWARD = $this->DBName.'.RC_TEMP_IMPORT_AWARD';
			$sql = "Insert Into $RC_TEMP_IMPORT_AWARD
						(ImportUserID, RowNumber, ReportID, ClassName, ClassNumber, UserLogin, WebSAMSRegNo, StudentID, StudentName, AwardText, AwardName, AwardCode, AwardID, SubjectName, SubjectWebSAMSCode, SubjectID, DateInput)
					Values
						".implode(',', (array)$InsertValueArr)."
					";
			return $this->db_db_query($sql);
		}
		else
		{
			return false;
		}
	}
	
	public function Get_Import_Award_Temp_Data($ReportID, $RowNumberArr='') {
		$conds_RowNumber = '';
		if ($RowNumberArr != '') {
			$conds_RowNumber = " And RowNumber In ('".implode("','", (array)$RowNumberArr)."') ";
		}
		
		$RC_TEMP_IMPORT_AWARD = $this->DBName.'.RC_TEMP_IMPORT_AWARD';
		$sql = "Select * From $RC_TEMP_IMPORT_AWARD Where ImportUserID = '".$_SESSION["UserID"]."' And ReportID = '".$ReportID."' $conds_RowNumber";
		return $this->returnArray($sql, null, 1);
	}
		
	function Get_Import_All_Temp_Data() {
		$table = $this->DBName.".RC_TEMP_IMPORT_ALL_SCHOOL_AWARD";
		$sql = "Select * from $table Where ImportUserID = '".$_SESSION["UserID"]."' ";
		return $this->returnResultSet($sql);
	}
	
	public function Delete_Import_All_Temp_Data() {
		$table = $this->DBName.".RC_TEMP_IMPORT_ALL_SCHOOL_AWARD";
		$sql = "Delete from $table Where ImportUserID = '".$_SESSION["UserID"]."' ";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	private function Get_Subject_Prize_Display_Format() {
		global $eRCTemplateSetting;
		
		$Format = '';
		if (isset($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['SubjectPrizeDisplayFormat'])) {
			$Format = $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['SubjectPrizeDisplayFormat'];
		}
		else {
			$Format = "<!--AwardName--> <!--SubjectName-->"; 
		}
		
		return $Format;
	}
	
	public function Get_Subject_Prize_Display($AwardName, $SubjectName) {
		$SubjectPrizeFormat = $this->Get_Subject_Prize_Display_Format();
		
		$FindArr = array('<!--AwardName-->', '<!--SubjectName-->');
		$ReplaceArr = array($AwardName, $SubjectName);
		return str_replace($FindArr, $ReplaceArr, $SubjectPrizeFormat);
	}
}
?>