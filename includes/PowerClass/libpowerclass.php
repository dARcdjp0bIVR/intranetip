<?php
/* 	Using:
 *
 *  2020-09-14 Bill     [DM#3780]
 *      - modified getSettingList(), only allow eNotice admin to access settings page
 *
 *  2020-09-07 Bill     [2020-0604-1821-16170]
 *      - modified getAppList(), to seperate issue right checking for pages display
 *
 *  2020-07-16 Bill     [2020-0714-1331-55313]
 *      - modified getAppList(), to allow access eCircular even cannot access eNotice
 *
 *  2020-07-14 Bill     [2020-0710-1622-57315]
 *      - modified getSettingList(), to allow access eCircular settings even without eNotice admin / issue right
 *
 *  2020-07-07 Ray
 *      - added eAttendanceLesson in getSettingList
 *
 *  2020-06-09 Ray
 * 		- added eAttendanceLesson for TW
 *
 *  2020-02-24 Bill     [2020-0224-1158-50313]
 *      - modified getReportList(), to change access right of "eHomework" to admin or viewer group only
 *
 *  2020-02-21 Bill     [2020-0221-1531-42315]
 *      - modified getSettingList(), to add "Basic Settings (eCircular)"
 *
 *  2020-02-19 Bill     [2020-0219-1104-09313]
 *      - modified getSettingList(), to add "Admin Settings (eCircular)"
 *
 *  2020-01-17 Ray
 *      - added ePayment MerchantAccount, SubsidyIdentitySettings in getSettingList
 *
 *  2019-02-19 Isaac 
 *      - added indexModulefunctions() to handle all group all index page functions
 *  
 *  2019-02-15 Thomas
 *      - Added STEM x PL2 in Top Menu 
 *      
 *  2019-01-28  Isaac
 *      - add Student Entry/ Leave Date to setting menu for eAttendance
 *
 *  2018-12-14  Cameron
 *      - add $_SESSION['EBOOKING_BOOKING_FROM_EADMIN'] = 0; to eBooking Menu in getAppList() to simulate like from eService [case #E154326]
 *      
 * 	2018-01-19  Pun
 * 		- modified getTopMenu(), removed HanAcademy cust checking 
 *
 * 	2017-11-28  Pun
 * 		- modified getTopMenu(), added flipped channels 
 *  
 * 	2017-11-28  Cameron
 * 		- add eInventory to getReportList() and $this->app_list['reports'] 
 *  
 * 	2017-08-14  Paul
 * 		- use the sub theme for "View" instead if Project_Use_Sub is turned on
 * 
 * 	2017-08-08  Cameron
 * 		- change eLib+ title to pure Chinese ($Lang['Header']['Menu']['eLibraryPlus4PowerClass']) to save space
 * 
 * 	2017-08-01 	Cameron
 * 		- enable eLib+
 */
if (!class_exists("libdb", false)) {
	include_once($intranet_root."/includes/libdb.php");
}
class libpowerclass extends libdb {
	public function __construct() {
		
		$this->libdb();
		$this->is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");
		
		global $sys_custom, $intranet_session_language;
		$this->lang = $intranet_session_language;
		$this->config['PowerClass'] = $sys_custom['PowerClass'];
		$this->config['Project_Custom'] = $sys_custom['Project_Custom'];
		if($sys_custom['Project_Use_Sub']){
			$this->config['Project_Label'] = $sys_custom['Project_Sub_Label'];
		}else{
			$this->config['Project_Label'] = $sys_custom['Project_Label'];
		}
		
		$this->PowerClassConfig['INTRANET_USER'] = "INTRANET_USER";
		/*
		 * Full list of PowerClass App:
		 * 	"Message", "eNotice", "SchoolNews", "eHomework", "eAttendance", "ePayment", "eBooking", "eClassApp", "eClassStudentApp", "eClassTeacherApp"
		 *
		 * On Hold:
		 * "eEnrolment", "eInventory", "eSchoolBus" , "StudentProfile"
		 *
		 */
		/* $app_list is to control the
		 * 	1.	which app to be used in the system
		 *	2.	order of the app upon display
		 */
		$this->app_list['app'] = array("Message", "eNotice", "SchoolNews", "eHomework", "eAttendance", "eAttendanceLesson", "ePayment", "eBooking", "eInventory");
		$this->app_list['app_settings'] = array("Message", "eNotice", "SchoolNews", "eHomework", "eAttendance", "eAttendanceLesson", "ePayment", "eBooking", "eInventory", "eClassApp", "eClassStudentApp", "eClassTeacherApp");
		$this->app_list['reports'] = array("eNotice", "eHomework", "eAttendance", "eAttendanceLesson", "ePayment", "eBooking", "eInventory");
	}
	
	public function isModuleOn()
	{
		return $this->config['PowerClass'];
	}
	
	public function getProjectIdentify() {
		return $this->config['Project_Label'];
	}
	
	public function getTopMenu($Lang, $CurrentPageArr)
	{
	    global $lhomework, $plugin, $sys_custom, $intranet_default_lang, $intranet_session_language;		
		
		if (!isset($lhomework)) {
			include_once(dirname(dirname(__FILE__))."/libhomework.php");
			include_once(dirname(dirname(__FILE__))."/libhomework2007a.php");
			$lhomework = new libhomework2007();
		}
		$canAccessOfficalPhotoSetting = $_SESSION["SSV_PRIVILEGE"]["canAccessOfficalPhotoSetting"];
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || ($plugin['AccountMgmt_StudentRegistry'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($_SESSION['SESSION_ACCESS_RIGHT']['STUDENTREGISTRY']))) || ($plugin['imail_gamma']==true && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"]) || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting)
			$AccountMgmt = 1;

		$pc_menu = array();
		$pc_menu["leftMenu"]["PC_App"]["link"] = "/home/PowerClass/app.php";
		$pc_menu["leftMenu"]["PC_App"]["title"] = $Lang['Header']['Menu']['PC_App'];
		$pc_menu["leftMenu"]["PC_App"]["selected"] = #;
		$pc_menu["leftMenu"]["PC_App"]["class"] = "";
		$pc_menu["leftMenu"]["PC_App"]["status"] = true;
		
		if($plugin['power_lesson_2']){
			$pc_menu["leftMenu"]["PowerLesson2"]["link"] = "#";
			$pc_menu["leftMenu"]["PowerLesson2"]["title"] = $Lang['Header']['Menu']['PowerLesson2'];
			$pc_menu["leftMenu"]["PowerLesson2"]["selected"] = #;
			$pc_menu["leftMenu"]["PowerLesson2"]["class"] = "newtab PowerLesson2";
			$pc_menu["leftMenu"]["PowerLesson2"]["status"] = true;
		}

		if($plugin['stem_x_pl2'] && ($_SESSION['UserType']==USERTYPE_STAFF || $_SESSION['UserType']==USERTYPE_STUDENT)){
		    if($this->isAbleToAccessStemXPowerLesson2()){
		        $pc_menu["leftMenu"]["StemXPl2"]["link"] = "/home/stem/powerlesson/sso.php";
		        $pc_menu["leftMenu"]["StemXPl2"]["title"] = $Lang['Header']['Menu']['StemXPl2'];
		        $pc_menu["leftMenu"]["StemXPl2"]["selected"] = #;
		        $pc_menu["leftMenu"]["StemXPl2"]["class"] = "newtab StemXPL2";
		        $pc_menu["leftMenu"]["StemXPl2"]["status"] = true;
		    }
		}
		
		if($plugin['FlippedChannels'] && $_SESSION['UserType']==USERTYPE_STAFF){
			$pc_menu["leftMenu"]["FlippedChannels"]["link"] = "/home/asp/flipchan.php";
			$pc_menu["leftMenu"]["FlippedChannels"]["title"] = $Lang['General']['FlippedChannels'];
			$pc_menu["leftMenu"]["FlippedChannels"]["selected"] = #;
			$pc_menu["leftMenu"]["FlippedChannels"]["class"] = "newtab FlippedChannels";
			$pc_menu["leftMenu"]["FlippedChannels"]["status"] = true;
		}
		
		if($plugin['library_management_system']){
			$pc_menu["leftMenu"]["eLib"]["link"] = "/home/eLearning/elibplus2/";
			$pc_menu["leftMenu"]["eLib"]["title"] = $Lang['Header']['Menu']['eLibraryPlus4PowerClass'];
			$pc_menu["leftMenu"]["eLib"]["selected"] = #;
			$pc_menu["leftMenu"]["eLib"]["class"] = "newtab eLibraryPlus";
			$pc_menu["leftMenu"]["eLib"]["status"] = true;
		}
		/*
		if($plugin['iPortfolio']){
			$pc_menu["leftMenu"]["iPortfolio"]["link"] = "/home/portfolio/";
			$pc_menu["leftMenu"]["iPortfolio"]["title"] = $Lang['Header']['Menu']['iPortfolio'];
			$pc_menu["leftMenu"]["iPortfolio"]["selected"] = #;
			$pc_menu["leftMenu"]["iPortfolio"]["class"] = "";
			$pc_menu["leftMenu"]["iPortfolio"]["status"] = true;
		}
		*/
		$pc_menu["leftMenu"]["PowerClassReport"]["link"] = "/home/PowerClass/reports.php";
		$pc_menu["leftMenu"]["PowerClassReport"]["title"] = $Lang['Header']['Menu']['PowerClassReport'];
		$pc_menu["leftMenu"]["PowerClassReport"]["selected"] = #;
		$pc_menu["leftMenu"]["PowerClassReport"]["class"] = "";
		$pc_menu["leftMenu"]["PowerClassReport"]["status"] = true;
		
		if($plugin['DigitalChannels']){
			$pc_menu["leftMenu"]["DigitalChannels"]["link"] = "/home/eAdmin/ResourcesMgmt/DigitalChannels/";
			$pc_menu["leftMenu"]["DigitalChannels"]["title"] = $Lang['Header']['Menu']['DigitalChannels'];
			$pc_menu["leftMenu"]["DigitalChannels"]["selected"] = #;
			$pc_menu["leftMenu"]["DigitalChannels"]["class"] = "newtab DigitalChannels";
			$pc_menu["leftMenu"]["DigitalChannels"]["status"] = true;
		}
		
		/*
		if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"]
				&& (
						$_SESSION['UserType']==USERTYPE_STUDENT
						|| ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])
						)
				){
					$pc_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["link"] = "/home/eService/homework/index.php";
					$pc_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["title"] = $Lang['Header']['Menu']['eHomework'];
					$pc_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["selected"] = $CurrentPageArr['eServiceHomework'];
					$pc_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["class"] = "";
					$pc_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["status"] = true;
					
		}
		
		### eService -> eNotice
		if ($sys_custom['eNotice']['eServiceDisableParent'] && $_SESSION['UserType']==USERTYPE_PARENT) {
			$_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
		}
		if ($sys_custom['eNotice']['eServiceDisableStudent'] && $_SESSION['UserType']==USERTYPE_STUDENT) {
			$_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
		}
		if($plugin['notice'] && !$_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"])
		{
			$pc_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["link"] = "/home/eService/notice/";
			$pc_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["title"] = $Lang['Header']['Menu']['eNotice'];
			$pc_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["selected"] = $CurrentPageArr['eServiceNotice'];
			$pc_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["class"] = "";
			$pc_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["status"] = true;
		}
		### eService -> eEnrolment
		$eEnrolTrialPast = false;
		if ($plugin['eEnrollment_trial'] != '' && date('Y-m-d') > $plugin['eEnrollment_trial'])
			$eEnrolTrialPast = true;
			
		// 2012-0814-1021-26073: student and parent can be eEnrol admin now
		//if($plugin['eEnrollment'] && !$eEnrolTrialPast && (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]))
		//if($plugin['eEnrollment'] && !$eEnrolTrialPast && (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] || $_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_PARENT))
		if($plugin['eEnrollment'] && !$eEnrolTrialPast)
		{
			$showButton = true;
			if ($_SESSION['UserType'] == USERTYPE_STAFF) {
				$showButton = false;
			}
			else if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]) {
				$showButton = false;
			}
			
			if ($showButton) {
				$pc_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["link"] = "/home/eService/enrollment/";
				$pc_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["title"] = $Lang['Header']['Menu']['eEnrolment'];
				$pc_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["selected"] = $CurrentPageArr['eServiceeEnrolment'];
				$pc_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["class"] = "";
				$pc_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["status"] = true;
			}
		}
		
		if (count($pc_menu["leftMenu"]["eService"]["child"]) > 0) {
			
			if ($CurrentPageArr['eServiceHomework']) {
				$CurrentPageArr['eService'] = true;
			}
			
			$pc_menu["leftMenu"]["eService"]["link"] = "#";
			$pc_menu["leftMenu"]["eService"]["title"] = $Lang['Header']['Menu']['eService'];
			$pc_menu["leftMenu"]["eService"]["selected"] = $CurrentPageArr['eService'];
			$pc_menu["leftMenu"]["eService"]["class"] = "lm";
			$pc_menu["leftMenu"]["eService"]["status"] = true;
		} else {
			unset($pc_menu["leftMenu"]["eService"]);
		}
		if((
				$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] ||
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] ||
				$_SESSION['isTeaching'] ||
				($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) ||
				$lhomework->isViewerGroupMember($UserID)
			) || $_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework'])
		{
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["link"] = "/home/eAdmin/StudentMgmt/homework/index.php";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["title"] = $Lang['Header']['Menu']['eHomework'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["selected"] = $CurrentPageArr['eAdminHomework'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["class"] = "lm";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["status"] = true;
		}
		
		if($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) {
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["link"] = "/home/eAdmin/StudentMgmt/notice/";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["title"] = $Lang['Header']['Menu']['eNotice'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["selected"] = $CurrentPageArr['eAdminNotice'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["class"] = "";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["status"] = true;
		}
		
		## Special checking for eEnrolment
		$show_eAdmin_eEnrol = false;
		if ($plugin['eEnrollment'] && !$eEnrolTrialPast)
		{
			/* 20100604 Ivan: Now, get admin status in lib.php, function UPDATE_CONTROL_VARIABLE()
			 include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
			 $libenroll = new libclubsenrol();
			 
			 if (isset($header_lu) == false)
			 {
			 include_once ($PATH_WRT_ROOT."/includes/libuser.php");
			 $header_lu = new libuser($_SESSION['UserID']);
			 }
			 * /
			
			if ($_SESSION['UserType']!=USERTYPE_STUDENT && $_SESSION['UserType']!=USERTYPE_PARENT)
			{
				if ($_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_admin"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_master"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_pic"])
					$show_eAdmin_eEnrol = true;
			}
		}
		## End Special checking for eEnrolment
		if($plugin['eEnrollment'])
		{
			if ($show_eAdmin_eEnrol)
			{
				$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["link"] = "/home/eAdmin/StudentMgmt/enrollment/";
				$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["title"] = $Lang['Header']['Menu']['eEnrolment'];
				$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["selected"] = $CurrentPageArr['eEnrolment'];
				$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["class"] = "";
				$pc_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["status"] = true;
			}
		}
		
		if (($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && !$plugin['DisableNews']) {
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["link"] = "/home/eAdmin/GeneralMgmt/schoolnews/";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["title"] = $Lang['Header']['Menu']['schoolNews'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["selected"] = $CurrentPageArr['schoolNews'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["class"] = "";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["status"] = true;
		}
		if (($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && !$plugin['DisableNews']) {
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["link"] = "/home/eAdmin/GeneralMgmt/schoolnews/";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["title"] = $Lang['Header']['Menu']['schoolNews'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["selected"] = $CurrentPageArr['schoolNews'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["class"] = "";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["status"] = true;
		}
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])
		{
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["link"] = "/home/system_settings/group/?clearCoo=1";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["title"] = $Lang['Header']['Menu']['Group'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["selected"] = $CurrentPageArr['Group'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["class"] = "lm";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["status"] = true;
		}
		if ($AccountMgmt) {
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["AccountMgmt"]["link"] = "/home/eAdmin/AccountMgmt/StaffMgmt/";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["AccountMgmt"]["title"] = $Lang['Header']['Menu']['StaffAccount'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["AccountMgmt"]["selected"] = $CurrentPageArr['StaffMgmt'];
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["AccountMgmt"]["class"] = "lm";
			$pc_menu["leftMenu"]["SchoolSettings"]["child"]["AccountMgmt"]["status"] = true;
		}
		
		if (count($pc_menu["leftMenu"]["SchoolSettings"]["child"]) > 0) {
			if ($CurrentPageArr['StaffMgmt'] || $CurrentPageArr['schoolNews'] || $CurrentPageArr['eAdminNotice']
					|| $CurrentPageArr['Group'] || $CurrentPageArr['eAdminHomework'] || $CurrentPageArr['eAdminNotice']) {
						$CurrentPageArr['SchoolSettings'] = true;
					} else {
						$CurrentPageArr['SchoolSettings'] = false;
					}
					
					$pc_menu["leftMenu"]["SchoolSettings"]["link"] = "#";
					$pc_menu["leftMenu"]["SchoolSettings"]["title"] = $Lang['Header']['Menu']['SchoolSettings'];
					$pc_menu["leftMenu"]["SchoolSettings"]["selected"] = $CurrentPageArr['SchoolSettings'];
					$pc_menu["leftMenu"]["SchoolSettings"]["class"] = "lm";
					$pc_menu["leftMenu"]["SchoolSettings"]["status"] = true;
		} else {
			unset($pc_menu["leftMenu"]["SchoolSettings"]);
		}
		*/
	   
		if ($intranet_default_lang == "gb" || get_client_region()=="zh_MY" || get_client_region()=="zh_CN") {
		    $chineseLang = "gb";
		    $chineseLangCode = 'ChineseSimplified';
		}
		else {
		    $chineseLang = "b5";
		    $chineseLangCode = 'Chinese';
		}   
		$pc_menu["rightMenu"]["language"]["link"] = "#";
		$pc_menu["rightMenu"]["language"]["title"] = $this->lang=="en" ?  $Lang['PowerClass']['English'] : $Lang['PowerClass'][$chineseLangCode];
		$pc_menu["rightMenu"]["language"]["selected"] = false;
		$pc_menu["rightMenu"]["language"]["class"] = "language rm";
		$pc_menu["rightMenu"]["language"]["status"] = true;
		$pc_menu["rightMenu"]["language"]["child"]["eng"]["link"] = "/lang.php?lang=en";
		$pc_menu["rightMenu"]["language"]["child"]["eng"]["title"] = $Lang['PowerClass']['English'];
		$pc_menu["rightMenu"]["language"]["child"]["eng"]["selected"] = ($this->lang== "en");
		$pc_menu["rightMenu"]["language"]["child"]["eng"]["class"] = " menu_button lang_button lang_en";
		$pc_menu["rightMenu"]["language"]["child"]["eng"]["status"] = true;
		
		$pc_menu["rightMenu"]["language"]["child"]["b5"]["link"] = "/lang.php?lang=".$chineseLang;
		$pc_menu["rightMenu"]["language"]["child"]["b5"]["title"] = $Lang['PowerClass'][$chineseLangCode];
		$pc_menu["rightMenu"]["language"]["child"]["b5"]["selected"] = ($this->lang != "en");
		$pc_menu["rightMenu"]["language"]["child"]["b5"]["class"] = " menu_button lang_button lang_b5";
		$pc_menu["rightMenu"]["language"]["child"]["b5"]["status"] = true;
		
		$pc_menu["rightMenu"]["userIdentify"]["link"] = "#";
		$pc_menu["rightMenu"]["userIdentify"]["title"] = "";
		$pc_menu["rightMenu"]["userIdentify"]["selected"] = false;
		$pc_menu["rightMenu"]["userIdentify"]["class"] = "rm";
		$pc_menu["rightMenu"]["userIdentify"]["status"] = true;
		
		$pc_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["link"] = "/home/iaccount/account/";
		$pc_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["title"] = $Lang['Header']['Menu']['iAccount'];
		$pc_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["selected"] = false;
		$pc_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["class"] = "menu_button";
		$pc_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["status"] = true;
		
		$pc_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["link"] = "/home/iaccount/account/login_password.php";
		$pc_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["title"] = $Lang['Header']['Menu']['ChangePassword'];
		$pc_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["selected"] = false;
		$pc_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["class"] = "menu_button";
		$pc_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["status"] = true;
		
		$pc_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["link"] = "#";
		$pc_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["title"] = $Lang['Header']['Logout'];
		$pc_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["selected"] = false;
		$pc_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["class"] = "menu_button logout";
		$pc_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["status"] = true;
		return $pc_menu;
	}
	
	public function getNumberOfParent() {
		global $lclass;
		$targetClassMenu = $lclass->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm()\"", $targetClass, $Lang['AccountMgmt']['AllClassesParent'], "");
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
		if($targetClass != "") {
			if($targetClass=='0') {					# all classes
				# do nothing
			} else if(is_numeric($targetClass)) {	# All students in specific form
				$conds .= " AND y.YearID=$targetClass";
			} else {								# All students in specific class
				$conds .= " AND yc.YearClassID=".substr($targetClass, 2);
			}
		}
		
		# record status
		if(isset($recordstatus) && $recordstatus==1)
			$conds .= " AND USR.RecordStatus=".STATUS_APPROVED;
			else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
				$conds .= " AND USR.RecordStatus=".STATUS_SUSPENDED;
		
		$sql = "SELECT YearClassID FROM YEAR_CLASS Where AcademicYearID IN ($AcademicYearID)";
		$currentYearClassIdAry = $this->ReturnVector($sql);
		$currentYearClassId = implode("','" ,$currentYearClassIdAry);
		
		
		$strSQL = "SELECT
			COUNT(distinct USR.UserID) as Total
		FROM 
			" . $this->PowerClassConfig['INTRANET_USER'] . " USR
		INNER JOIN
			INTRANET_PARENTRELATION pr ON (pr.ParentID=USR.UserID)
		LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (ycu.UserID=pr.StudentID AND ycu.YearClassID in ('".$currentYearClassId."'))
		LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearID')
		LEFT OUTER JOIN
			YEAR y ON (y.YearID=yc.YearID)
			LEFT OUTER JOIN INTRANET_USER stu ON (pr.StudentID = stu.UserID)
		WHERE
			USR.RecordType = '".TYPE_PARENT."' and USR.RecordStatus IN (0,1,2)
			$conds
			";
		$result = $this->returnResultSet($strSQL);
		if (count($result) > 0) {
			return $result[0]["Total"];
		} else {
			return 0;
		}
	}
	public function getNumberOfStudent($studentType = 'all') {
		
		$AcademicYearID = get_Current_Academic_Year_ID();
		$conds = "";
		if ($studentType == "notInClass") {
			
			# record status
			if(isset($recordstatus) && $recordstatus==1)
				$conds .= " AND USR.RecordStatus=".STATUS_APPROVED;
			else if(isset($recordstatus) && $recordstatus==3)
				$conds .= " AND USR.RecordStatus=".STATUS_GRADUATE;
			else if(isset($recordstatus) && $recordstatus!=1 && $recordstatus!="")
				$conds .= " AND (USR.RecordStatus=".STATUS_SUSPENDED." OR USR.RecordStatus=".STATUS_PENDING.")";
			
			$sql = "SELECT
				distinct USR.UserID
			FROM
				" . $this->PowerClassConfig['INTRANET_USER'] . " USR
			LEFT OUTER JOIN
				YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
			LEFT OUTER JOIN
				YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
			LEFT OUTER JOIN
				YEAR y ON (y.YearID=yc.YearID)
			WHERE
				RecordType = ".TYPE_STUDENT." AND yc.AcademicYearID='".$AcademicYearID . "'";
			$studentInClasses = $this->returnVector($sql);
			if(sizeof($studentInClasses)>0) $conds .= " AND USR.UserID NOT IN (".implode(', ', $studentInClasses).")";
		} else {
			$conds .= " AND yc.AcademicYearID='".$AcademicYearID. "'";
		}
		$strSQL = "SELECT
			COUNT(distinct USR.UserID) as Total
		FROM 
			" . $this->PowerClassConfig['INTRANET_USER'] . " USR";
		
		if ($studentType == "notInClass") {
			$strSQL .= "
				LEFT JOIN
					PROFILE_CLASS_HISTORY as b on (b.UserID = USR.UserID )
				";
		} else {
			$strSQL .= "
				LEFT OUTER JOIN
					INTRANET_PARENTRELATION pr ON (pr.StudentID = USR.UserID)
				INNER JOIN
					YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
				INNER JOIN
						YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
				INNER JOIN
					YEAR y ON (y.YearID=yc.YearID)
				LEFT OUTER JOIN
					INTRANET_USER par ON (pr.ParentID = par.UserID)  LEFT JOIN INTRANET_USERGROUP as ug ON ug.UserID=USR.UserID 
				LEFT JOIN 
					INTRANET_GROUP as g ON g.GroupID=ug.GroupID AND g.RecordType=4 AND g.AcademicYearID='$AcademicYearID'";
		}
		$strSQL .= " WHERE
			USR.RecordType = '".TYPE_STUDENT."'
			$conds";
		$result = $this->returnResultSet($strSQL);
		
		if (count($result) > 0) {
			return $result[0]["Total"];
		} else {
			return 0;
		}
	}
	public function getNumberOfStaff() {
		
		# Teaching type
		if($TeachingType==1) {
			$conds = " AND Teaching=$TeachingType";
		}
		else if($TeachingType=='S') {
			$conds = " AND Teaching = 'S'";
		}
		else if(isset($TeachingType) && $TeachingType!='') {
			$conds = " AND ((Teaching!=".TEACHING." AND Teaching != 'S') OR Teaching IS NULL)";
		}
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID = '".$AcademicYearID."'";
		$yearClassIdAry = $this->ReturnVector($sql);
		
		$strSQL = "SELECT
			COUNT(distinct iu.UserID) as Total
		FROM
			" . $this->PowerClassConfig['INTRANET_USER'] . " as iu
		LEFT OUTER JOIN
			YEAR_CLASS_TEACHER as yct ON iu.UserID = yct.UserID AND yct.YearClassID IN  ('".implode("','",$yearClassIdAry)."')
		LEFT OUTER JOIN
			YEAR_CLASS as yc ON yct.YearClassID = yc.YearClassID  LEFT JOIN INTRANET_USERGROUP as ug ON ug.UserID=iu.UserID
		LEFT JOIN
			INTRANET_GROUP as g ON g.GroupID=ug.GroupID AND g.RecordType=4 AND g.AcademicYearID='$AcademicYearID'
		 WHERE
			iu.RecordType='" . TYPE_TEACHER . "'";
		$result = $this->returnResultSet($strSQL);
		if (count($result) > 0) {
			return $result[0]["Total"];
		} else {
			return 0;
		}
	}
	
	public function getStatInfo($Lang) {
		$timeArr = array(9, 10, 11, 12, 13, 14, 15, 17, 18, 19);
		
		$statInfo = array();
		$today = date("Y-m-d");
		$statInfo["today"] = array(
			"categories" => array(),
			"data" => array()
		);
		$statInfo["yesterday"] = array(
				"categories" => array(),
				"data" => array()
		);
		$strSQL = "SELECT COUNT(LoginSessionID) AS Total FROM INTRANET_LOGIN_SESSION WHERE StartTime LIKE ";
		$findDate = array( "today" => date("Y-m-d"), "yesterday" => date("Y-m-d", strtotime("-1 days")));
		foreach ($findDate as $findKey => $findVal) {
			foreach ($timeArr as $kk => $vv) {
				if ($vv == 9) {
					$sql = $strSQL . "'" . $findVal. "%' AND StartTime <= '" . $findVal. " " . sprintf("%02d", $vv) . ":00:00'";
				} else {
					$sql = $strSQL . "'" . $findVal. "%' AND StartTime > '" . $findVal . " " . sprintf("%02d", $vv - 1). ":00:00' AND StartTime <= '" . $findVal. " " . sprintf("%02d", $vv) . ":00:00'";
				}
				$result = $this->returnResultSet($sql);
				$statInfo[$findKey]["categories"][$kk] = sprintf("%02d:00", $vv);
				if (isset($result[0]["Total"])) $statInfo[$findKey]["data"][$kk] = $result[0]["Total"];
				else $statInfo[$findKey]["data"][$kk] = 0;
			}
		}
		$statInfo["weekday"] = array();
		for($i=0;$i<7;$i++) {
			$val = 6 - $i;
			$statInfo["weekday"]["categories"][$i] = date("Y-m-d", strtotime( " -" . $val ." day"));
			$sql = $strSQL . "'" . $statInfo["weekday"]["categories"][$i] . "%'";
			$result = $this->returnResultSet($sql);
			if (isset($result[0]["Total"])) $statInfo["weekday"]["data"][$i] = $result[0]["Total"];
			else $statInfo["weekday"]["data"][$i] = 0;
		}
		$statInfo["month"] = array();
		for($i=0;$i<12;$i++) {
			$val = 11 - $i;
			$statInfo["month"]["categories"][$i] = date("Y-m", strtotime( " -" . $val ." month"));
			$sql = $strSQL . "'" . $statInfo["month"]["categories"][$i] . "%'";
			$result = $this->returnResultSet($sql);
			if (isset($result[0]["Total"])) $statInfo["month"]["data"][$i] = $result[0]["Total"];
			else $statInfo["month"]["data"][$i] = 0;
		}
		return $statInfo;
	}
	
	public function getAppList($UserID, $AcademicYearID, $currentYearTerm){
	    global $Lang,$plugin,$intranet_root, $sys_custom;
		
		$AppMenu = array();

		# SchoolNews
		if (($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && !$plugin['DisableNews']) {
			$AppMenu['SchoolNews']['icon_class'] = "SchoolNews";
			$AppMenu['SchoolNews']['home_href'] = "/home/eAdmin/GeneralMgmt/schoolnews/";
			$AppMenu['SchoolNews']['Title'] = $Lang['Header']['Menu']['schoolNews'];
			$AppMenu['SchoolNews']['Add']["isGroup"] = false;
			$AppMenu['SchoolNews']['Add']['Link'] = "/home/eAdmin/GeneralMgmt/schoolnews/new.php";
			$AppMenu['SchoolNews']['Add']['Lang'] = $Lang["PowerClass"]["Add"];
		}
		
		# eNotice & eCircular
		//if($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"]))
        if (($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) ||
            ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || (!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])))
		{
		    /*
            // [2020-0714-1331-55313]
			$AppMenu['eNotice']['icon_class'] = "eNotice";
			$AppMenu['eNotice']['home_href'] = "/home/eAdmin/StudentMgmt/notice/";
			$AppMenu['eNotice']['Title'] = $Lang['Header']['Menu']['eNotice'];
			$AppMenu['eNotice']['Add']["isGroup"] = true;
			$AppMenu['eNotice']['Add']['Top'] = $Lang["PowerClass"]["Add"];
			$AppMenu['eNotice']['Add']['List'][0]['Link'] = "/home/eAdmin/StudentMgmt/notice/new.php";
			$AppMenu['eNotice']['Add']['List'][0]['Lang'] = $Lang["PowerClass"]["Parent"];
			$AppMenu['eNotice']['Add']['List'][1]['Link'] = "/home/eAdmin/StudentMgmt/notice/student_notice/new.php";
			$AppMenu['eNotice']['Add']['List'][1]['Lang'] = $Lang["PowerClass"]["Student"];
			
			include_once($intranet_root."/includes/libgeneralsettings.php");
			$lgeneralsettings = new libgeneralsettings();
			$moduleName = 'ePayment';
			$settingName = array("'PaymentNoticeEnable'");
			$enablePaymentNotice = $lgeneralsettings->Get_General_Setting($moduleName, $settingName); 
			if($enablePaymentNotice['PaymentNoticeEnable']){
			    $AppMenu['eNotice']['Add']['List'][2]['Link'] = "/home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice_new.php";
			    $AppMenu['eNotice']['Add']['List'][2]['Lang'] = $Lang["PowerClass"]["Payment"];
			}
			$AppMenu['eNotice']['Add']['List'][3]['Link'] = "/home/eAdmin/StaffMgmt/circular/new.php";
			$AppMenu['eNotice']['Add']['List'][3]['Lang'] = $Lang["PowerClass"]["TeacherNotice"];
		    */

            $AppMenu['eNotice']['icon_class'] = "eNotice";
		    if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) {
		        // [2020-0604-1821-16170] define home_href by issue right
                //$AppMenu['eNotice']['home_href'] = "/home/eAdmin/StudentMgmt/notice/";
                if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"]) {
                    $AppMenu['eNotice']['home_href'] = "/home/eAdmin/StudentMgmt/notice/";
                } else {
                    $AppMenu['eNotice']['home_href'] = "/home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice.php";
                }
            } else {
                $AppMenu['eNotice']['home_href'] = "/home/eAdmin/StaffMgmt/circular/index.php";
            }
            $AppMenu['eNotice']['Title'] = $Lang['Header']['Menu']['eNotice'];
            $AppMenu['eNotice']['Add']["isGroup"] = true;
            $AppMenu['eNotice']['Add']['Top'] = $Lang["PowerClass"]["Add"];
            $AppMenu['eNotice']['Add']['List'] = array();

            # eNotice
            if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"]))
            {
                // [2020-0604-1821-16170] seperate issue right checking
                // School Notice
                if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"])
                {
                    $AppMenu['eNotice']['Add']['List'][] = array(
                        'Link' => "/home/eAdmin/StudentMgmt/notice/new.php",
                        'Lang' => $Lang["PowerClass"]["Parent"]
                    );
                    $AppMenu['eNotice']['Add']['List'][] = array(
                        'Link' => "/home/eAdmin/StudentMgmt/notice/student_notice/new.php",
                        'Lang' => $Lang["PowerClass"]["Student"]
                    );
                }

                // Payment Notice
                if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasPaymentNoticeIssueRight"])
                {
                    include_once($intranet_root."/includes/libgeneralsettings.php");
                    $lgeneralsettings = new libgeneralsettings();
                    $moduleName = 'ePayment';
                    $settingName = array("'PaymentNoticeEnable'");
                    $enablePaymentNotice = $lgeneralsettings->Get_General_Setting($moduleName, $settingName);
                    if($enablePaymentNotice['PaymentNoticeEnable'])
                    {
                        $AppMenu['eNotice']['Add']['List'][] = array(
                            'Link' => "/home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice_new.php",
                            'Lang' => $Lang["PowerClass"]["Payment"]
                        );
                    }
                }
            }

            # eCircular
            if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || (!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]))
            {
                $AppMenu['eNotice']['Add']['List'][] = array(
                    'Link' => "/home/eAdmin/StaffMgmt/circular/new.php",
                    'Lang' => $Lang["PowerClass"]["TeacherNotice"]
                );
            }
		}
		
		# eHomework
		include_once($intranet_root."/includes/libhomework.php");
		include_once($intranet_root."/includes/libhomework2007a.php");
		$lhomework = new libhomework2007();
		if(
			(
				$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || 
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || 
				$_SESSION['isTeaching'] ||
				($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) ||	
				$lhomework->isViewerGroupMember($UserID)
			) || $_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework']){
		
			$AppMenu['eHomework']['icon_class'] = "eHomework";
			$AppMenu['eHomework']['home_href'] = "/home/eAdmin/StudentMgmt/homework/management/homeworklist/index.php";
			$AppMenu['eHomework']['Title'] = $Lang['Header']['Menu']['eHomework'];
			$AppMenu['eHomework']['Add']["isGroup"] = false;
			$AppMenu['eHomework']['Add']['Link'] = "/home/eAdmin/StudentMgmt/homework/management/homeworklist/add.php?yearID=".$AcademicYearID."&yearTermID=".$currentYearTerm;
			$AppMenu['eHomework']['Add']['Lang'] = $Lang["PowerClass"]["Add"];
		}
		
		# Message Centre
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"]
				|| $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"] || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"])
		{
			if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/MassMailing/";
			} elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/SMS/";
			} elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] ||
					$_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"] ||
					$_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/";
			} elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/TeacherNotification/";
			}
			elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/StudentNotification/";
			}
			$AppMenu['Message']['icon_class'] = "Message";
			$AppMenu['Message']['home_href'] = $mc_first_page;
			$AppMenu['Message']['Title'] = $Lang['General']['PushMessage'];
			$AppMenu['Message']['Add']["isGroup"] = true;
			$AppMenu['Message']['Add']['Top'] = $Lang["PowerClass"]["SendTo"];
			$AppMenu['Message']['Add']['List'][0]['Link'] = "/home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/new.php?appType=P";
			$AppMenu['Message']['Add']['List'][0]['Lang'] = $Lang["PowerClass"]["Parent"];
			$AppMenu['Message']['Add']['List'][1]['Link'] = "/home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/new.php?appType=S";
			$AppMenu['Message']['Add']['List'][1]['Lang'] = $Lang["PowerClass"]["Student"];
			$AppMenu['Message']['Add']['List'][2]['Link'] = "/home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/new.php?appType=T";
			$AppMenu['Message']['Add']['List'][2]['Lang'] = $Lang["PowerClass"]["Teacher"];
		}
		
		# eAttendance
		if (($plugin['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) || ($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])) {
			$AppMenu['eAttendance']['icon_class'] = "eAttendance";
			$AppMenu['eAttendance']['home_href'] = "/home/eAdmin/StudentMgmt/attendance/index.php";
			$AppMenu['eAttendance']['Title'] = $Lang['Header']['Menu']['eAttednance'];
			$AppMenu['eAttendance']['Add']["isGroup"] = false;
			$AppMenu['eAttendance']['Add']['Link'] = "/home/eAdmin/StudentMgmt/attendance/dailyoperation/class/class_status.php";
			$AppMenu['eAttendance']['Add']['Lang'] = $Lang["PowerClass"]["TakeAttendance"];
		}

		if(get_client_region() == 'zh_TW') {
			if (($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])) {
				$AppMenu['eAttendanceLesson']['icon_class'] = "LessonAttendance";
				$AppMenu['eAttendanceLesson']['home_href'] = "/home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php";
				$AppMenu['eAttendanceLesson']['Title'] = $Lang['Header']['Menu']['eAttendanceLesson'];
				$AppMenu['eAttendanceLesson']['Add']["isGroup"] = false;
				$AppMenu['eAttendanceLesson']['Add']['Link'] = "/home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php";
				$AppMenu['eAttendanceLesson']['Add']['Lang'] = $Lang["PowerClass"]["TakeAttendance"];
			}
		}

		# ePayment
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && $plugin['payment']){
			$AppMenu['ePayment']['icon_class'] = "ePayment";
			$AppMenu['ePayment']['home_href'] = "/home/eAdmin/GeneralMgmt/payment/settings/payment_item/";
			$AppMenu['ePayment']['Title'] = $Lang['Header']['Menu']['ePayment'];
			$AppMenu['ePayment']['Add']["isGroup"] = false;
			$AppMenu['ePayment']['Add']['Link'] = "/home/eAdmin/GeneralMgmt/payment/settings/payment_item/new.php";
			$AppMenu['ePayment']['Add']['Lang'] = $Lang["PowerClass"]["Add"];
		}
		
		# eBooking
		if($plugin['eBooking'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"] || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER")){
		    $_SESSION['EBOOKING_BOOKING_FROM_EADMIN'] = 0;        // simulate from eService
			$AppMenu['eBooking']['icon_class'] = "eBooking";
			$AppMenu['eBooking']['home_href'] = "/home/eAdmin/ResourcesMgmt/eBooking/index.php";
			$AppMenu['eBooking']['Title'] = $Lang['Header']['Menu']['eBooking'];
			$AppMenu['eBooking']['Add']["isGroup"] = false;
			$AppMenu['eBooking']['Add']['Link'] = "/home/eAdmin/ResourcesMgmt/eBooking/management/reserve_step1.php";
			$AppMenu['eBooking']['Add']['Lang'] = $Lang["PowerClass"]["Add"];
		}
		
		# eInventory
		if($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != "")){
			$AppMenu['eInventory']['icon_class'] = "eInventory";
			$AppMenu['eInventory']['home_href'] = "/home/eAdmin/ResourcesMgmt/eInventory/";
			$AppMenu['eInventory']['Title'] = $Lang['Header']['Menu']['eInventory'];
			$AppMenu['eInventory']['Add']["isGroup"] = false;
			$AppMenu['eInventory']['Add']['Link'] = "/home/eAdmin/ResourcesMgmt/eInventory/management/inventory/new_item.php";
			$AppMenu['eInventory']['Add']['Lang'] = $Lang["PowerClass"]["Add"];
		}
		
		return $AppMenu;
	}
	
	public function getReportList($UserID){
		global $Lang,$plugin,$intranet_root;
	
		$AppMenu = array();
	
		# eNotice
		if($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"]))
		{
			$AppMenu['eNotice']['icon_class'] = "eNotice";
			$AppMenu['eNotice']['href'] = "/home/eAdmin/StudentMgmt/notice/reports/notice_summary/index.php";
			$AppMenu['eNotice']['Title'] = $Lang['Header']['Menu']['eNotice'];
		}
	
		# eHomework
		include_once($intranet_root."/includes/libhomework.php");
		include_once($intranet_root."/includes/libhomework2007a.php");
		$lhomework = new libhomework2007();
		//if(
		//		(
		//				$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] ||
		//				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
		//				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] ||
		//				$_SESSION['isTeaching'] ||
		//				($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) ||
		//				$lhomework->isViewerGroupMember($UserID)
		//				) || $_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework']){
        if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || $lhomework->isViewerGroupMember($UserID)) {
            $AppMenu['eHomework']['icon_class'] = "eHomework";
            $AppMenu['eHomework']['href'] = "/home/eAdmin/StudentMgmt/homework/reports/homeworkreport/index.php";
            $AppMenu['eHomework']['Title'] = $Lang['Header']['Menu']['eHomework'];
		}
	
		# eAttendance
		if (($plugin['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) || ($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])) {
			$AppMenu['eAttendance']['icon_class'] = "eAttendance";
			$AppMenu['eAttendance']['href'] = "/home/eAdmin/StudentMgmt/attendance/report/class_month_full.php";
			$AppMenu['eAttendance']['Title'] = $Lang['Header']['Menu']['eAttednance'];
		}

		if(get_client_region() == 'zh_TW') {
			if (($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])) {
				$AppMenu['eAttendanceLesson']['icon_class'] = "LessonAttendance";
				$AppMenu['eAttendanceLesson']['href'] = "/home/eAdmin/StudentMgmt/attendance/report/lesson_class_detail_report.php";
				$AppMenu['eAttendanceLesson']['Title'] = $Lang['Header']['Menu']['eAttendanceLesson'];
			}
		}

		# ePayment
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && $plugin['payment']){
			$AppMenu['ePayment']['icon_class'] = "ePayment";
			$AppMenu['ePayment']['href'] = "/home/eAdmin/GeneralMgmt/payment/report/schoolaccount/";
			$AppMenu['ePayment']['Title'] = $Lang['Header']['Menu']['ePayment'];
		}
	
		# eBooking
		if($plugin['eBooking'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"] || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER")){
			$AppMenu['eBooking']['icon_class'] = "eBooking";
			$AppMenu['eBooking']['href'] = "/home/eAdmin/ResourcesMgmt/eBooking/reports/monthly_usage_report/index.php";
			$AppMenu['eBooking']['Title'] = $Lang['Header']['Menu']['eBooking'];
		}
	
		# eInventory
		if($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != "")){
			$AppMenu['eInventory']['icon_class'] = "eInventory";
			$AppMenu['eInventory']['href'] = "/home/eAdmin/ResourcesMgmt/eInventory/report/stocktake_result.php";
			$AppMenu['eInventory']['Title'] = $Lang['Header']['Menu']['eInventory'];
		}
	
		return $AppMenu;
	}
	
	public function getSettingList($UserID){
		global $Lang,$plugin,$intranet_root;
	
		$AppMenu = array();
	
		# SchoolNews
		if (($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && !$plugin['DisableNews']) {
			$AppMenu['SchoolNews']['icon_class'] = "SchoolNews";
			$AppMenu['SchoolNews']['home_href'] = "/home/eAdmin/GeneralMgmt/schoolnews/";
			$AppMenu['SchoolNews']['Title'] = $Lang['Header']['Menu']['schoolNews'];
			$AppMenu['SchoolNews']['Setting'] = array(
					array(
						'Link' => "/home/eAdmin/GeneralMgmt/schoolnews/settings/index.php",
						'Lang' => $Lang["PowerClass"]["SchoolNews_BasicSettings"]
					)
			);
		}
	
		# eNotice & eCircular
		//if($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"]))
        if (($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
		{
			$AppMenu['eNotice']['icon_class'] = "eNotice";
            if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) {
                // [2020-0604-1821-16170] define home_href by issue right
                //$AppMenu['eNotice']['home_href'] = "/home/eAdmin/StudentMgmt/notice/";
                if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"]) {
			        $AppMenu['eNotice']['home_href'] = "/home/eAdmin/StudentMgmt/notice/";
                } else {
                    $AppMenu['eNotice']['home_href'] = "/home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice.php";
                }
            } else {
                $AppMenu['eNotice']['home_href'] = "/home/eAdmin/StaffMgmt/circular/";
            }
			$AppMenu['eNotice']['Title'] = $Lang['Header']['Menu']['eNotice'];
            $AppMenu['eNotice']['Setting'] = array();

            // [2020-0710-1622-57315]
            //$AppMenu['eNotice']['Setting'] = array(
            //    array(
            //        'Link' => "/home/eAdmin/StudentMgmt/notice/settings/basic_settings/",
            //        'Lang' => $Lang["PowerClass"]["SchoolNews_BasicSettings"]
            //    ),
            //    array(
            //        'Link' => "/home/eClassApp/common/pushMessageTemplate/template_view.php?Module=eNotice&Section=all",
            //        'Lang' => $Lang["PowerClass"]["eNotice_PushMessageTemplate"]
            //    )
            //);

            // eNotice
            // [DM#3780] only eNotice admin can access
            //if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"]))
            if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
            {
                $AppMenu['eNotice']['Setting'][] = array(
                                                        'Link' => "/home/eAdmin/StudentMgmt/notice/settings/basic_settings/",
                                                        'Lang' => $Lang["PowerClass"]["SchoolNews_BasicSettings"]
                );
                $AppMenu['eNotice']['Setting'][] = array(
                                                        'Link' => "/home/eClassApp/common/pushMessageTemplate/template_view.php?Module=eNotice&Section=all",
                                                        'Lang' => $Lang["PowerClass"]["eNotice_PushMessageTemplate"]
                );
            }

            // eCircular
            if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
            {
                $AppMenu['eNotice']['Setting'][] = array(
                                                        'Link' => "/home/eAdmin/StaffMgmt/circular/settings/basic_settings/",
                                                        'Lang' => $Lang["PowerClass"]["eCircular_BasicSettings"]
                );
                $AppMenu['eNotice']['Setting'][] = array(
                                                        'Link' => "/home/eAdmin/StaffMgmt/circular/settings/admin_settings/",
                                                        'Lang' => $Lang["PowerClass"]["eCircular_AdminSettings"]
                                                    );
            }
		}

		# eHomework
		include_once($intranet_root."/includes/libhomework.php");
		include_once($intranet_root."/includes/libhomework2007a.php");
		$lhomework = new libhomework2007();
		if(
				(
						$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] ||
						$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
						$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] ||
						$_SESSION['isTeaching'] ||
						($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) ||
						$lhomework->isViewerGroupMember($UserID)
						) || $_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework']){
	
							$AppMenu['eHomework']['icon_class'] = "eHomework";
							$AppMenu['eHomework']['home_href'] = "/home/eAdmin/StudentMgmt/homework/management/homeworklist/index.php";
							$AppMenu['eHomework']['Title'] = $Lang['Header']['Menu']['eHomework'];
							$AppMenu['eHomework']['Setting'] = array(
									array(
										'Link' => "/home/eAdmin/StudentMgmt/homework/settings/index.php",
										'Lang' => $Lang["PowerClass"]["eHomework_BasicSettings"]
									),
									array(
										'Link' => "/home/eAdmin/StudentMgmt/homework/settings/viewergroup.php",
										'Lang' => $Lang["PowerClass"]["eHomework_ViewerGroup"]
									)
							);
		}
	
		# Message Centre
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"]
				|| $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"] || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"])
		{
			if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/MassMailing/";
			} elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/SMS/";
			} elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] ||
					$_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"] ||
					$_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/";
			} elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/TeacherNotification/";
			}
			elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/StudentNotification/";
			}
			$AppMenu['Message']['icon_class'] = "Message";
			$AppMenu['Message']['home_href'] = $mc_first_page;
			$AppMenu['Message']['Title'] = $Lang['General']['PushMessage'];
			$AppMenu['Message']['Setting'] = array(
				array(
					'Link' => "/home/eClassApp/common/pushMessageTemplate/template_view.php?Module=MessageCenter",
					'Lang' => $Lang["PowerClass"]["MessageCenter_MessageTemplate"]
				)
			);
		}
	
		# eAttendance
		if (($plugin['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) || ($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])) {
			$AppMenu['eAttendance']['icon_class'] = "eAttendance";
			$AppMenu['eAttendance']['home_href'] = "/home/eAdmin/StudentMgmt/attendance/index.php";
			$AppMenu['eAttendance']['Title'] = $Lang['Header']['Menu']['eAttednance'];
			
			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='StudentAttendance' AND SettingName='TimeTableMode'";
			$time_table_mode = current($this->returnVector($sql));
			if (($time_table_mode == 0)||($time_table_mode == '')){
				$time_table_setting = array(
						'Link' => "/home/eAdmin/StudentMgmt/attendance/settings/slot/school/",
						'Lang' => $Lang['PowerClass']['StudentAttendance_TimeSlotSettings']
				);
			}else if ($time_table_mode == 1){
				$time_table_setting = array(
					'Link' => "/home/eAdmin/StudentMgmt/attendance/settings/slot_session/session_setting/",
					'Lang' => $Lang['PowerClass']['StudentAttendance_TimeSlotSettings']
				);
			}

			include_once($intranet_root."/includes/libcardstudentattend2.php");
			$lcardstudentattend2 = new libcardstudentattend2();
			if ($lcardstudentattend2->EnableEntryLeavePeriod){
                $EntryLeaveSetting = array('Link' => "/home/eAdmin/StudentMgmt/attendance/settings/entry_leave_date/",
                                           'Lang' => $Lang['StudentAttendance']['EntryLeaveDate'] );
			}

			$ApplyLeaveAppSetting = array();
			$LeaveTypeAppSetting = array();
			if ($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) {
				if(get_client_region() == 'zh_TW') {
					if ($plugin['eClassApp']) {
						$ApplyLeaveAppSetting = array('Link' => '/home/eAdmin/StudentMgmt/attendance/settings/apply_leave/',
														'Lang' => $Lang['StudentAttendance']['ApplyLeaveAry']['ApplyLeave(App)']);
					}
				}

				if ($sys_custom['StudentAttendance']['LeaveType'] || get_client_region() == 'zh_TW') {
					$LeaveTypeAppSetting = array('Link'=>'/home/eAdmin/StudentMgmt/attendance/settings/leave_type/',
												'Lang'=>$Lang['StudentAttendance']['LeaveType']);
				}
			}


			$AppMenu['eAttendance']['Setting'] = array(
				array(
					'Link' => "/home/eAdmin/StudentMgmt/attendance/settings/basic/",
					'Lang' => $Lang["PowerClass"]["SchoolNews_BasicSettings"]
				),
				array(
					'Link' => "/home/eAdmin/StudentMgmt/attendance/settings/terminal/",
					'Lang' => $Lang['ePOS']['TerminalSetting']
				)
			);
			if(!empty($time_table_setting)) {
				$AppMenu['eAttendance']['Setting'][] = $time_table_setting;
			}

			$AppMenu['eAttendance']['Setting'][] = array(
					'Link' => "/home/eClassApp/common/pushMessageTemplate/template_view.php?Module=StudentAttendance&Section=",
					'Lang' => $Lang['eClassApp']['PushMessageTemplate']
				);

			if(!empty($lesson_attendance_setting)) {
				$AppMenu['eAttendance']['Setting'][] = $lesson_attendance_setting;
			}

			if(!empty($lesson_attendance_reason)) {
				$AppMenu['eAttendance']['Setting'][] = $lesson_attendance_reason;
			}

			if(!empty($EntryLeaveSetting)) {
				$AppMenu['eAttendance']['Setting'][] = $EntryLeaveSetting;
			}

			if(!empty($ApplyLeaveAppSetting)) {
				$AppMenu['eAttendance']['Setting'][] = $ApplyLeaveAppSetting;
			}

			if(!empty($LeaveTypeAppSetting)) {
				$AppMenu['eAttendance']['Setting'][] = $LeaveTypeAppSetting;
			}
		}

		# eAttendance lesson
		if (($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])) {
			if(get_client_region() == 'zh_TW') {
				$AppMenu['eAttendanceLesson']['icon_class'] = "LessonAttendance";
				$AppMenu['eAttendanceLesson']['home_href'] = "/home/eAdmin/StudentMgmt/attendance/dailyoperation/daily_lesson_attendance/index2.php";
				$AppMenu['eAttendanceLesson']['Title'] = $Lang['Header']['Menu']['eAttendanceLesson'];

				$lesson_attendance_setting = array();
				$lesson_attendance_reason = array();
				$lesson_attendance_wordlist = array();
				global $i_wordtemplates_attendance;
				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"] && $_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson']){
					$lesson_attendance_setting = array(
						'Link' => "/home/eAdmin/StudentMgmt/attendance/settings/lesson/",
						'Lang' => $Lang['LessonAttendance']['LessionAttendanceSetting']
					);
					$lesson_attendance_reason = array(
						'Link' => "/home/eAdmin/StudentMgmt/attendance/settings/lesson_attendance_reasons/",
						'Lang' => $Lang['LessonAttendance']['LessonAttendanceReasons']
					);
					$lesson_attendance_wordlist = array(
						'Link' => "/home/eAdmin/StudentMgmt/attendance/settings/wordtemplates/",
						'Lang' => $i_wordtemplates_attendance
					);
				}

				if(!empty($lesson_attendance_setting)) {
					$AppMenu['eAttendanceLesson']['Setting'][] = $lesson_attendance_setting;
				}

				if(!empty($lesson_attendance_reason)) {
					$AppMenu['eAttendanceLesson']['Setting'][] = $lesson_attendance_reason;
				}

				if(!empty($lesson_attendance_wordlist)) {
					$AppMenu['eAttendanceLesson']['Setting'][] = $lesson_attendance_wordlist;
				}
				
				if(count($AppMenu['eAttendanceLesson']['Setting']) == 0) {
					unset($AppMenu['eAttendanceLesson']);
				}
			}
		}

		# ePayment
		global $i_Payment_Menu_Settings_TerminalIP,$i_Payment_Menu_Settings_TerminalAccount,$i_Payment_Menu_Settings_PaymentCategory,$i_Payment_Menu_Settings_Letter,$i_Payment_Menu_Settings_PPS_Setting;
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && $plugin['payment']){
			$AppMenu['ePayment']['icon_class'] = "ePayment";
			$AppMenu['ePayment']['home_href'] = "/home/eAdmin/GeneralMgmt/payment/settings/payment_item/";
			$AppMenu['ePayment']['Title'] = $Lang['Header']['Menu']['ePayment'];
			$AppMenu['ePayment']['Add']["isGroup"] = false;
			$AppMenu['ePayment']['Add']['Link'] = "/home/eAdmin/GeneralMgmt/payment/settings/payment_item/new.php";
			$AppMenu['ePayment']['Add']['Lang'] = $Lang["PowerClass"]["Add"];
			$temp_epayment_setting = array(
					array(
						"Link"=>"/home/eAdmin/GeneralMgmt/payment/settings/system_properties/",
						"Lang"=> $Lang['ePayment']['SystemProperties']
					),
					array(
						"Link"=>"/home/eAdmin/GeneralMgmt/payment/settings/terminal/",
						"Lang"=>$Lang['PowerClass']['ePayment']['Settings_TerminalIP']
					),
					array(
						"Link"=>"/home/eAdmin/GeneralMgmt/payment/settings/account/",
						"Lang"=>$Lang['PowerClass']['ePayment']['Settings_TerminalAccount']
					),
					array(
						"Link"=>"/home/eAdmin/GeneralMgmt/payment/settings/payment_cat/",
						"Lang"=>$Lang['PowerClass']['ePayment']['Settings_PaymentCategory']
					)
			);

			include_once($intranet_root."/includes/libpayment.php");
			$lpayment = new libpayment();
			if($lpayment->useEWalletMerchantAccount() || $lpayment->isEWalletTopUpEnabled()) {
				$temp_epayment_setting[] = array(
					"Link"=>"/home/eAdmin/GeneralMgmt/payment/settings/merchant_account/",
					"Lang"=> $Lang['ePayment']['MerchantAccount']
				);
			}

			$temp_epayment_setting = array_merge($temp_epayment_setting, array(
					array(
						"Link"=>"/home/eAdmin/GeneralMgmt/payment/settings/letter/",
						"Lang"=>$Lang['PowerClass']['ePayment']['Settings_Letter']
					),
					array(
						"Link"=>"/home/eAdmin/GeneralMgmt/payment/settings/pps/",
						"Lang"=>$Lang['PowerClass']['ePayment']['PPS_Setting']
					),
					array(
						"Link"=>"/home/eAdmin/GeneralMgmt/payment/settings/subsidy_identity/",
						"Lang"=>$Lang['ePayment']['SubsidyIdentitySettings']
					),
					array(
						"Link"=>"/home/eAdmin/GeneralMgmt/payment/settings/payment_templates/",
						"Lang"=>$Lang['eClassApp']['PushMessageTemplate']
					)
			));

			$AppMenu['ePayment']['Setting'] = $temp_epayment_setting;
		}
	
		# eBooking
		if($plugin['eBooking'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"] || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER")){
			$AppMenu['eBooking']['icon_class'] = "eBooking";
			$AppMenu['eBooking']['home_href'] = "/home/eAdmin/ResourcesMgmt/eBooking/index.php";
			$AppMenu['eBooking']['Title'] = $Lang['Header']['Menu']['eBooking'];
			$AppMenu['eBooking']['Setting'] = array(
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eBooking/settings/system_property/setting.php",
					'Lang' => $Lang['General']['SystemProperties']
				),	
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eBooking/settings/general_permission/room/view.php?clearCoo=1",
					'Lang' => $Lang['eBooking']['Settings']['FieldTitle']['GeneralPermission']
				),	
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eBooking/settings/management_group/management_group.php?clearCoo=1",
					'Lang' => $Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup']
				),	
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eBooking/settings/follow_up_group/follow_up_group.php",
					'Lang' => $Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup']
				),	
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eBooking/settings/booking_rule/user_booking_rules.php?clearCoo=1",
					'Lang' => $Lang['eBooking']['Settings']['FieldTitle']['UserBookingRule']
				),	
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eBooking/settings/booking_period/general_available_period.php?clearCoo=1",
					'Lang' => $Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod']
				),	
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eBooking/settings/category/category_setting.php?clearCoo=1",
					'Lang' => $Lang['eBooking']['Settings']['FieldTitle']['Category']
				)
			);
		}
	
		# eInventory
		if($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != "")){
			$AppMenu['eInventory']['icon_class'] = "eInventory";
			$AppMenu['eInventory']['home_href'] = "/home/eAdmin/ResourcesMgmt/eInventory/";
			$AppMenu['eInventory']['Title'] = $Lang['Header']['Menu']['eInventory'];
			$AppMenu['eInventory']['Setting'] = array(
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eInventory/settings/category/category_setting.php",
					'Lang' => $Lang['PowerClass']['eInventory']['Category']
				),
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eInventory/settings/group/group_setting.php?clearCoo=1",
					'Lang' => $Lang['PowerClass']['eInventory']['Caretaker']
				),
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eInventory/settings/funding/fundingsource_setting.php",
					'Lang' => $Lang['PowerClass']['eInventory']['FundingSource']
				),
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eInventory/settings/write_off_reason/write_off_reason_setting.php",
					'Lang' => $Lang['PowerClass']['eInventory']['WriteOffReason']
				),
				array(
					'Link' => "/home/eAdmin/ResourcesMgmt/eInventory/settings/others/others_setting.php",
					'Lang' => $Lang['PowerClass']['eInventory']['General_Settings']
				)
			);
		}
		
		# Parent Apps
		if($plugin['eClassApp'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"]){
			$AppMenu['eClassApp']['icon_class'] = "ParentApp";
			$AppMenu['eClassApp']['home_href'] = "/home/eAdmin/GeneralMgmt/eClassApp/";
			$AppMenu['eClassApp']['Title'] = $Lang["PowerClass"]["ParentApp"];
			$AppMenu['eClassApp']['Setting'] = array(
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/",
							'Lang' => $Lang["PowerClass"]["ParentApp_FunctionAccessRight"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=P&task=parentApp/school_image/list",
							'Lang' => $Lang["PowerClass"]["ParentApp_SchoolImage"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=P&task=parentApp/login_status/list",
							'Lang' => $Lang["PowerClass"]["ParentApp_LoginStatus"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=P&task=parentApp/blacklist/list",
							'Lang' => $Lang["PowerClass"]["ParentApp_Blacklist"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?task=parentApp/advanced_setting/push_message_right/list",
							'Lang' => $Lang["PowerClass"]["ParentApp_PushMessageRight"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=C&task=commonFunction/group_message/index",
							'Lang' => $Lang["PowerClass"]["App_GroupMessage"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=C&task=commonFunction/school_info/index",
							'Lang' => $Lang["PowerClass"]["App_SchoolInfo"]
					)
			);
		}
		
		# Student Apps
		if($plugin['eClassStudentApp'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"]){
			$AppMenu['eClassStudentApp']['icon_class'] = "StudentApp";
			$AppMenu['eClassStudentApp']['home_href'] = "/home/eAdmin/GeneralMgmt/eClassApp/?appType=S&task=studentApp/access_right/list";
			$AppMenu['eClassStudentApp']['Title'] = $Lang["PowerClass"]["StudentApp"];
			$AppMenu['eClassStudentApp']['Setting'] = array(
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=S&task=studentApp/access_right/list",
							'Lang' => $Lang["PowerClass"]["StudentApp_FunctionAccessRight"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?task=parentApp/school_image/list&appType=S",
							'Lang' => $Lang["PowerClass"]["StudentApp_SchoolImage"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=S&task=studentApp/login_status/list",
							'Lang' => $Lang["PowerClass"]["StudentApp_LoginStatus"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=C&task=commonFunction/school_info/index",
							'Lang' => $Lang["PowerClass"]["App_SchoolInfo"]
					)
			);
		}
		
		# Teacher Apps
		if($plugin['eClassTeacherApp'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]){
			$AppMenu['eClassTeacherApp']['icon_class'] = "TeacherApp";
			$AppMenu['eClassTeacherApp']['home_href'] = "/home/eAdmin/GeneralMgmt/eClassApp/?appType=T&task=teacherApp/access_right/list";
			$AppMenu['eClassTeacherApp']['Title'] = $Lang["PowerClass"]["TeacherApp"];
			$AppMenu['eClassTeacherApp']['Setting'] = array(
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=T&task=teacherApp/access_right/list",
							'Lang' => $Lang["PowerClass"]["TeacherApp_FunctionAccessRight"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?task=parentApp/school_image/list&appType=T",
							'Lang' => $Lang["PowerClass"]["TeacherApp_SchoolImage"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=T&task=teacherApp/login_status/list",
							'Lang' => $Lang["PowerClass"]["TeacherApp_LoginStatus"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=T&task=teacherApp/advanced_setting/push_message_right/list",
							'Lang' => $Lang["PowerClass"]["TeacherApp_PushMessageRight"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?task=teacherApp/advanced_setting/teacher_status_right/list&appType=T",
							'Lang' => $Lang["PowerClass"]["TeacherApp_StaffList"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?task=teacherApp/advanced_setting/student_list_right/list&appType=T",
							'Lang' => $Lang["PowerClass"]["TeacherApp_StudentList"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=C&task=commonFunction/group_message/index",
							'Lang' => $Lang["PowerClass"]["App_GroupMessage"]
					),
					array(
							'Link' => "/home/eAdmin/GeneralMgmt/eClassApp/?appType=C&task=commonFunction/school_info/index",
							'Lang' => $Lang["PowerClass"]["App_SchoolInfo"]
					)
			);
		}
		
		return $AppMenu;
	}
	
	public function orderAppDisplay($app_details, $from){
		$app_list = $this->app_list;
		$app_order = array_flip($app_list[$from]);
		$app_details = array_intersect_key($app_details, $app_order);

		foreach($app_details as $key=>$value){
			$app_details[$key]['order'] = $app_order[$key];
		}
		
		function cmp($a,$b){
			if($a['order'] > $b['order'] ){
				return 1;
			}
			return -1;
		}		
		uasort($app_details, "cmp");
		
		return $app_details;
	}
	
	public function isAbleToAccessStemXPowerLesson2()
	{
	    global $config_school_code, $stemLicense_config;
	    
	    $luser = new libuser($_SESSION['UserID']);
	    
	    $stemLicenseConfig = require dirname(dirname(__FILE__))."/StemLicense/StemLicense.inc.php";
	    $stemLicenseApiUrl = $stemLicenseConfig['serverPath'] . 'eclass40/src/stem/license/api';
	    
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $stemLicenseApiUrl . '/accessibility/school/'.$config_school_code . '/user/' . $luser->UserLogin);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 300);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Accept: application/json"));
	    curl_exec($ch);
	    
	    $statusCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $result     = !(curl_errno($ch) > 0) && $statusCode >= 200 && $statusCode <= 299;
	    
	    curl_close($ch);
	    
	    return $result;
	}

    public function indexModuleFunctions(){
        global $intranet_root;
        ###################################################################
        # eInventory
        ###################################################################
        if($_SESSION['SSV_PRIVILEGE']['plugin']['Inventory'])
        {
            include_once($intranet_root."/includes/libinventory.php");
            $linventory  = new libinventory();
            if($_SESSION['inventory']['role'] != "")
            {
                $inventory_access_level = $linventory->getAccessLevel();
                
                if(($inventory_access_level == 1) || ($inventory_access_level == 2))
                {
                    $arr_expiry = $linventory->getWarrantyExpiry();
                    
                    ##### eInventory warranty expiry warning popup
                    if($arr_expiry[0]>0) {
                        echo $linventory->warrantyExpiryWarningPopup();
                    }
                }
            }
        }
        ###################################################################
        # eInventory [END]
        ###################################################################
    }
}