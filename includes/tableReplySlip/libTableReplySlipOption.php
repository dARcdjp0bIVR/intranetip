<?php
// editing by Carlos

if (!defined("LIBTABLEREPLYSLIPOPTION_DEFINED")) {
	define("LIBTABLEREPLYSLIPOPTION_DEFINED", true);
	
	class libTableReplySlipOption extends libdbobject {
		private $optionId;
		private $replySlipId;
		private $uniqueId;
		private $recordType;
		private $content;
		private $displayOrder;
		private $inputDate;
		private $inputBy;
		private $modifiedDate;
		private $modifiedBy;
		
		public function __construct($objectId='') {
			parent::__construct('INTRANET_TABLE_REPLY_SLIP_OPTION', 'OptionID', $this->returnFieldMappingAry(), $objectId);
		}
		
		public function setOptionId($val) {
			$this->optionId = $val;
		}
		public function getOptionId() {
			return $this->optionId;
		}
		
		public function setReplySlipId($val) {
			$this->replySlipId = $val;
		}
		public function getReplySlipId() {
			return $this->replySlipId;
		}
		
		public function setUniqueId($val) {
			$this->uniqueId = $val;
		}
		public function getUniqueId() {
			return $this->uniqueId;
		}
		
		public function setRecordType($val) {
			$this->recordType = $val;
		}
		public function getRecordType() {
			return $this->recordType;
		}
		
		public function setContent($val) {
			$this->content = $val;
		}
		public function getContent() {
			return $this->content;
		}
		
		public function setDisplayOrder($val) {
			$this->displayOrder = $val;
		}
		public function getDisplayOrder() {
			return $this->displayOrder;
		}
		
		public function setInputDate($val) {
			$this->inputDate = $val;
		}
		public function getInputDate() {
			return $this->inputDate;
		}
		
		public function setInputBy($val) {
			$this->inputBy = $val;
		}
		public function getInputBy() {
			return $this->inputBy;
		}
		
		public function setModifiedDate($val) {
			$this->modifiedDate = $val;
		}
		public function getModifiedDate() {
			return $this->modifiedDate;
		}
		
		public function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		public function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		private function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('OptionID', 'int', 'setOptionId', 'getOptionId');
			$fieldMappingAry[] = array('ReplySlipID', 'int', 'setReplySlipId', 'getReplySlipId');
			$fieldMappingAry[] = array('UniqueID', 'str', 'setUniqueId', 'getUniqueId');
			$fieldMappingAry[] = array('RecordType', 'str', 'setRecordType', 'getRecordType');
			$fieldMappingAry[] = array('Content', 'str', 'setContent', 'getContent');
			$fieldMappingAry[] = array('DisplayOrder', 'int', 'setDisplayOrder', 'getDisplayOrder');
			$fieldMappingAry[] = array('InputDate', 'date', 'setInputDate', 'getInputDate');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('ModifiedDate', 'date', 'setModifiedDate', 'getModifiedDate');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
		
		protected function newRecordBeforeHandling() {
			$this->setInputDate('now()');
			$this->setInputBy($this->getInputBy());
			$this->setModifiedDate('now()');
			$this->setModifiedBy($this->getModifiedBy());
			
			return true;
		}	
	}
}
?>