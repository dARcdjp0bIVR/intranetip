<?php
// editing by Carlos

if (!defined("LIBTABLEREPLYSLIPITEM_DEFINED")) {
	define("LIBTABLEREPLYSLIPITEM_DEFINED", true);
	
	class libTableReplySlipItem extends libdbobject {
		private $itemId;
		private $replySlipId;
		private $recordType;
		private $content;
		private $rowOrder;
		private $columnOrder;
		private $inputDate;
		private $inputBy;
		private $modifiedDate;
		private $modifiedBy;
		
		public function __construct($objectId='') {
			parent::__construct('INTRANET_TABLE_REPLY_SLIP_ITEM', 'ItemID', $this->returnFieldMappingAry(), $objectId);
		}
		
		public function setItemId($val) {
			$this->itemId = $val;
		}
		public function getItemId() {
			return $this->itemId;
		}
		
		public function setReplySlipId($val) {
			$this->replySlipId = $val;
		}
		public function getReplySlipId() {
			return $this->replySlipId;
		}
		
		public function setRecordType($val) {
			$this->recordType = $val;
		}
		public function getRecordType() {
			return $this->recordType;
		}
		
		public function setContent($val) {
			$this->content = $val;
		}
		public function getContent() {
			return $this->content;
		}
		
		public function setRowOrder($val) {
			$this->rowOrder = $val;
		}
		public function getRowOrder() {
			return $this->rowOrder;
		}
		
		public function setColumnOrder($val) {
			$this->columnOrder = $val;
		}
		public function getColumnOrder() {
			return $this->columnOrder;
		}
		
		public function setInputDate($val) {
			$this->inputDate = $val;
		}
		public function getInputDate() {
			return $this->inputDate;
		}
		
		public function setInputBy($val) {
			$this->inputBy = $val;
		}
		public function getInputBy() {
			return $this->inputBy;
		}
		
		public function setModifiedDate($val) {
			$this->modifiedDate = $val;
		}
		public function getModifiedDate() {
			return $this->modifiedDate;
		}
		
		public function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		public function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		private function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('ItemID', 'int', 'setItemId', 'getItemId');
			$fieldMappingAry[] = array('ReplySlipID', 'int', 'setReplySlipId', 'getReplySlipId');
			$fieldMappingAry[] = array('RecordType', 'str', 'setRecordType', 'getRecordType');
			$fieldMappingAry[] = array('Content', 'str', 'setContent', 'getContent');
			$fieldMappingAry[] = array('RowOrder', 'int', 'setRowOrder', 'getRowOrder');
			$fieldMappingAry[] = array('ColumnOrder', 'int', 'setColumnOrder', 'getColumnOrder');
			$fieldMappingAry[] = array('InputDate', 'date', 'setInputDate', 'getInputDate');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('ModifiedDate', 'date', 'setModifiedDate', 'getModifiedDate');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
		
		protected function newRecordBeforeHandling() {
			$this->setInputDate('now()');
			$this->setInputBy($this->getInputBy());
			$this->setModifiedDate('now()');
			$this->setModifiedBy($this->getModifiedBy());
			
			return true;
		}
		
		protected function deleteRecordAfterHandling() {
			/*
			$optionIdAry = Get_Array_By_Key($this->returnOptionData(), 'OptionID');
			$numOfOption = count($optionIdAry);
			
			$successAry = array();
			for ($i=0; $i<$numOfOption; $i++) {
				$_optionId = $optionIdAry[$i];
				$_optionObj = new libReplySlipQuestionOption($_optionId);
				$successAry['deleteOption'][$_optionId] = $_optionObj->deleteRecord();
			}
			
			return !in_multi_array(false, $successAry);
			*/
		}
		
		private function returnOptionData() {
			global $tableReplySlipConfig;
			
			$replySlipId = $this->getReplySlipId();
			$recordType = $this->getRecordType();
			$content = $this->getContent();
			
			$optionAry = array($tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleSelection'],
								$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleSelection'],
								$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['singleCheckbox'],
								$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP_ITEM']['RecordType']['multipleCheckbox']);
			
			if(in_array($recordType,$optionAry)) {
				$sql = "Select OptionID,Content From INTRANET_TABLE_REPLY_SLIP_OPTION Where ReplySlipID = '".$replySlipId."' And UniqueID='".$content."'";
				return $this->objDb->returnResultSet($sql);
			}else {
				return $content;
			}
		}
	}
}
?>