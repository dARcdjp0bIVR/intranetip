<?php
# using: yat

if (!defined("LIBBATCH_DEFINED"))         // Preprocessor directives
{

define("LIBBATCH_DEFINED",true);

class libbatch extends libdb{
      var $BatchID;
      var $BatchName;
      var $RecordStatus;
      var $slots;
      var $Description;

      function libbatch($bid="")
      {
               $this->libdb();
               if ($bid=="")
               {
                   $sql = "SELECT BatchID FROM INTRANET_SLOT_BATCH WHERE RecordStatus = 1";
                   $result = $this->returnVector($sql);
                   $bid = $result[0];
               }
               $this->BatchID = $bid;
               $sql = "SELECT Title, Description, RecordStatus FROM INTRANET_SLOT_BATCH WHERE BatchID = $bid";
               $result = $this->returnArray($sql,3);
               $this->BatchName = $result[0][0];
               $this->Description = $result[0][1];
               $this->RecordStatus = $result[0][2];
               $sql = "SELECT SlotID, Title, TimeRange, SlotSeq FROM INTRANET_SLOT WHERE BatchID = $bid ORDER BY SlotSeq ASC";
               $this->slots = $this->returnArray($sql,4);
      }

      function returnSlots($bid="")
      {
               if ($bid=="")
               {
                   $sql = "SELECT BatchID FROM INTRANET_SLOT_BATCH WHERE RecordStatus = 1";
                   $result = $this->returnVector($sql);
                   $bid = $result[0];
               }
               $sql = "SELECT SlotID, Title, SlotSeq FROM INTRANET_SLOT WHERE BatchID = $bid ORDER BY SlotSeq ASC";
               $this->slots = $this->returnArray($sql,3);
               return $this->slots;
      }

      function returnBatchSelect($attrb, $selected="")
      {
               global $i_Batch_Default;
               if ($selected == "")
                   $selected = $this->BatchID;
               $sql = "SELECT BatchID, Title, RecordStatus FROM INTRANET_SLOT_BATCH ORDER BY RecordStatus DESC, Title ASC";
               $result = $this->returnArray($sql,3);
               $x = "<SELECT $attrb>\n";
               for ($i=0; $i<sizeof($result); $i++)
               {
                    list($id, $title, $status) = $result[$i];
                    if ($status == 1) $title .= $i_Batch_Default;
                    if ($selected == $id) $str = "SELECTED";
                    else $str = "";
                    $x .= "<OPTION $str value=$id> $title </OPTION>";
               }
               $x .= "</SELECT>";
               return $x;

      }

}


} // End of directives
?>