<?php
if (!defined("LIBCTMSMS_DEFINED"))                     // Preprocessor directive
{
define("LIBCTMSMS_DEFINED", true);


include_once("$intranet_root/includes/libhttpclient.php");
include_once("$intranet_root/includes/libwebmail.php");
include_once("$intranet_root/lang/email.php");

class libctmsms {

      var $service_host;
      var $service_port;
      var $service_action;
      var $data_action;
      var $usage_action;
      var $status_action;
      var $login;
      var $password;
      var $school_code;
      var $simulate_only;
      var $cancel_previous_action;
      var $start_transaction;
      var $end_transaction;
      
      var $FailureReturnCodeArr;

        function libctmsms ()
        {
                 global $sms_ctm_school_code, $sms_ctm_host, $sms_ctm_port, $sms_ctm_login, $sms_ctm_passwd,
                        $sms_ctm_api_send_request, $sms_ctm_api_get_usage, $sms_ctm_api_get_status,
                        $sms_ctm_simulate_only;
                 $this->service_host = $sms_ctm_host;
                 $this->service_port = $sms_ctm_port;
                 $this->service_action = $sms_ctm_api_send_request;
                 $this->usage_action = $sms_ctm_api_get_usage;
                 $this->status_action = $sms_ctm_api_get_status;
                 $this->login = $sms_ctm_login;
                 $this->password = $sms_ctm_passwd;
                 $this->school_code = $sms_ctm_school_code;
                 $this->simulate_only = $sms_ctm_simulate_only;
                 
                 if ($this->school_code == "")
                 {
                     die("Bad Configuration. Please contact eClass support. <support@broadlearning.com>");
                 }
                 
                 $this->FailureReturnCodeArr = array('-1', '-2', '-4', '-31', '-32', '-100');

                 /*
                 $this->service_host = "sms.ctm-mobile.com";
                 $this->service_port = 80;
                 $this->service_action = "/corpsmswebtest/http/Send?char=UTF-8";
                 //$this->data_action = "/sms/DownloadReportBroadLearning.php";
                 $this->login = "ectest";
                 $this->password = "ectest";
                 $this->school_code = "10060188912648";
                 */
        }

        function sendSMS($recipient, $message, $message_code="", $delivery_time="", $isReplyMessage)
        {
	        	 global $sms_ctm_debug_email, $sms_ctm_debug_mobile, $webmaster;
	        	 
	        	 
	             // add debug mobile
	        	 if($sms_ctm_debug_mobile!=""){
		        	 $recipient.=",".$sms_ctm_debug_mobile;
		        	 $message_code.=",0";
		         }
		         
                 $message = stripslashes($message);
                 $post_data = "login_id=".$this->login . "&password=".$this->password."&recipient=".$recipient;
                 $post_data .= "&school_code=".$this->school_code;
                 $post_data .= "&message=".urlencode($message)."&message_code=".$message_code."&delivery_time=".$delivery_time;

                 if ($this->simulate_only)
                 {
                    global $intranet_root;
                    $logpath = "$intranet_root/file/ctm.send";
                    $content = get_file_content($logpath);
                    write_file_content ($content."\n".$post_data,$logpath);

                    //test XML23OBj library blackbox
                    $xml_return="<?xml version=\"1.0\" encoding=\"utf-8\" ?><sms><item id=\"1\"><recipient>60871494</recipient><reference_id>1206436510641</reference_id></item></sms>";
                    $records = XML2Obj($xml_return);
                     
                    //echo 'record object'.$records[0]->recipient.'<br>';
                    //echo 'record object'.$records[0]->reference_id.'<br>';
					
                    return "SIMULATE";
                 }
                 else  //not simulate               
                 {
	                 // send mail
			         if($sms_ctm_debug_email!="") {
		        	 	$lwebmail = new libwebmail();
		        	 	$subject ="Send SMS Notification";
		        	 	$mail_message = "Message : $message\n<BR>";
		        	 	$mail_message.= "Recipient: $recipient\n<BR>";
		        	 	$mail_message.= "School Code : ".$this->school_code."\n<BR>";
		        	 	if($delivery_time!=""){
			        	 	$mail_message.= "Delivery Time : ".$delivery_time."\n<BR>";
			        	}
			        	if($lwebmail->has_webmail){
			         		$lwebmail->sendMail($subject,$mail_message,$webmaster,array($sms_ctm_debug_email),array(),array(),"","",$webmaster,$webmaster);
			         	}
			         }
	              
                     # Try to connect to service host
                     $lhttp = new libhttpclient($this->service_host,$this->service_port);
                     if (!$lhttp->connect()){    # Exit if failed to connect
   
                          return -99;
                     }
                     
					//send sms
                     $lhttp->set_path($this->service_action);
                     $lhttp->post_data = $post_data;
                     $response = $lhttp->send_request();
                     
                     # check if failure
                     $responseArr = explode("\n", $response);
                     $lastIndex = count($responseArr) - 1;
                     $reponseStatus = $responseArr[$lastIndex];
                     
                     # if failure => return the failure code
                     if (in_array($reponseStatus, $this->FailureReturnCodeArr))
                     	return $reponseStatus;
                     	
                     
                     # Put in log for debug
                     global $intranet_root;
                     write_file_content ($post_data,"$intranet_root/file/ctm.send");
                     write_file_content ($response,"$intranet_root/file/ctm.log");

                     # Filter the HTTP protocol message and extract the XML document only
                     $pos = strpos($response,"<?xml ");
                     if ($pos===false)
                     {
                                $pos = strpos($response,"<?XML ");
                     }
                     $response = substr($response,$pos);
                     $records = XML2Obj($response);
                     return $records;

                 }

        }
        
        # recipeintData : list of array(phone,message,message code,delivery time)
        #pre-condition: $isReplyMessage always = 0 for 1 way sms
        #post-condition: return record format (?)
        function sendBulkSMS($recipientData,$isReplyMessage)
        {
			global $sms_ctm_debug_email, $sms_ctm_debug_mobile, $webmaster;
			
			if($recipientData=="" || sizeof($recipientData)<=0){
				return false;
			}
			// add debug mobile
			if($sms_ctm_debug_mobile!=""){
			//$recipient.=",".$sms_kanhan_debug_mobile;
			//$recipientData[] = ($sms_kanhan_debug_mobile, $message, $message_code,$delivery_time) 
			}
			if (!$this->simulate_only){
				    
		             # Try to connect to service host
		             $lhttp = new libhttpclient($this->service_host,$this->service_port);
		             if (!$lhttp->connect()){    # Exit if failed to connect
		                  return -99;
		             }
			}			
			$records = array();
			
			for($i=0;$i<sizeof($recipientData);$i++){
				list($recipient, $message, $message_code,$delivery_time) = $recipientData[$i];
				
				$message = stripslashes($message);
             	$post_data = "login_id=".$this->login . "&password=".$this->password."&recipient=".$recipient;
             	$post_data .= "&school_code=".$this->school_code;
             	$post_data .= "&message=".urlencode($message)."&message_code=".$message_code."&delivery_time=".$delivery_time;

                 if ($this->simulate_only)
                 {
                     global $intranet_root;
                     $logpath = "$intranet_root/file/ctm.send";
                     $content = get_file_content($logpath);
                     write_file_content ($content."\n".$post_data,$logpath);

                     // return "SIMULATE";
                     $records[] = "SIMULATE";
                 }
                 else                 
	             {
   					//send sms
   					$lhttp->set_path($this->service_action);
                    $lhttp->post_data = $post_data;
                    $response = $lhttp->send_request();
                    
                    # Put in log for debug
                     global $intranet_root;
                     write_file_content ($post_data,"$intranet_root/file/ctm.send");
                     write_file_content ($response,"$intranet_root/file/ctm.log");
                     
                     # check if failure
                     $responseArr = explode("\n", $response);
                     $lastIndex = count($responseArr) - 1;
                     $reponseStatus = $responseArr[$lastIndex];
                     
                     # if failure => return the failure code
                     if (in_array($reponseStatus, $this->FailureReturnCodeArr))
                     {
                     	$records[] = $reponseStatus;
                     	continue;
                 	 }
                     	

                     # Filter the HTTP protocol message and extract the XML document only
                     
                     
                     
                     
                     $pos = strpos($response,"<?xml ");
                     if ($pos==false)
                     {
                                $pos = strpos($response,"<?XML ");
                     }
                     $response = substr($response,$pos);
                     
                     
                     
                     //replace response wiht dummy response for testing
                     
                     $records[] = XML2Obj($response);
             	 }
         	}
         	if(!$this->simulate_only){
             		if($isReplyMessage){
       	         	     //close transaction
		                 $target_url = "http://".$this->service_host.$this->end_transaction_action."?login_id=".$this->login."&password=".$this->password."&school_code=".$this->school_code;
		             	 $fp =@fopen($target_url,"r");
		                 fclose($fp);
					}
            }
            return $records;
        }//end function
        
        //data format from report page call from smsv2/usages2/refresh.php
        //date("Y-m-d H:i:s", mktime(0, 0, 0, $month, 1, $year));
    	//return value: total messages send out on this month (local + international)
        function retrieveMessageCount($start, $end)
		{
			global $intranet_root;
			$target_url = "http://".$this->service_host.$this->usage_action
				."?login_id=".$this->login
				."&password=".$this->password
				."&school_code=".$this->school_code
				."&start_date=".urlencode($start)
				."&end_date=".urlencode($end);
			
			
			//echo $target_url.'<br>';
				
			$fp =@fopen($target_url,"r");

			if (!$fp)
			{
				# Server Down
				return 0;
			}
                 
			$data = array();
			while (!feof($fp)) 
			{
				$data[] = fgets($fp, 4096);
			}
			fclose($fp);
			
			
			list($total_sms_sent,$local_sms_sent,$inti_sms_sent,$fail_local_sms,$fail_inti_sms) = split(",",$data[0]);
			
			/*	
			echo 'total_sms_sent: '.$total_sms_sent.'<br>';
			echo 'local_sms_sent: '.$local_sms_sent.'<br>';
			echo 'inti_sms_sent: '.$inti_sms_sent.'<br>';
			echo 'fail_local_sms: '.$fail_local_sms.'<br>';
			echo 'fail_inti_sms: '.$fail_inti_sms.'<br>';
			*/

			return trim($total_sms_sent);

			return 0;
		}
		
		//pre-condition: reference id array
		//post-condition: return array with status
		//BUG: not finish coding
		function retrieveSMSStatus($ref_id_array)
		{
			//pre-process $ref_id_array to comma-separated format
			foreach ($ref_id_array as $value)
			{
				$ref_id_string.=$value.',';
			}
			//remove last unused comma
			$ref_id_string=substr($ref_id_string, 0, -1);
			
			$target_url = "http://".$this->service_host.$this->status_action
				."?login_id=".$this->login
				."&password=".$this->password
				."&school_code=".$this->school_code
				."&ref_id=".$ref_id_string
				."&type=XML";
				
			//echo $target_url.'<br>';
			
			
			
				
			$fp =@fopen($target_url,"r");

			if (!$fp)
			{
				# Server Down
				return 0;
			}
                 
			
			while (!feof($fp)) 
			{
				$data.=fgets($fp);
			}
			fclose($fp);
			
     //post process
     //Filter the HTTP protocol message and extract the XML document only
     $pos = strpos($data,"<?xml ");
            
     if ($pos==false)
     {
        $pos = strpos($data,"<?XML ");
     }
     $data = substr($data,$pos);
     $records = XML2Obj($data);
     return $records;
		}//end function
}//end class


function XML2Obj ($xDoc)
{
         $data = $xDoc;
         $parser = xml_parser_create();
         xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
         xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
         xml_parse_into_struct($parser,$data,$values,$tags);
         xml_parser_free($parser);

         // loop through the structures
         foreach ($tags as $key=>$val)
         {
                  if ($key == "item")   # Specified in KanHan Document, each record is inside tag <msg>
                  {
                      $recordranges = $val;
                      // each contiguous pair of array entries are the
                      // lower and upper range for each record definition
                      for ($i=0; $i < count($recordranges); $i+=2)
                      {
                           $offset = $recordranges[$i] + 1;
                           $len = $recordranges[$i + 1] - $offset;
                           $tdb[] = parseRecord(array_slice($values, $offset, $len));
                      }
                  }
                  else
                  {
                      continue;
                  }
         }
         return $tdb;

}


function parseRecord($mvalues) {
    for ($i=0; $i < count($mvalues); $i++)
        $mol[$mvalues[$i]["tag"]] = $mvalues[$i]["value"];
    return new ctmSMSRecord($mol);
}

# Data Structure only
class ctmSMSRecord
{
      var $recipient;
      var $reference_id;
      var $message_code;

      function ctmSMSRecord($aa)
      {
               foreach ($aa as $k=>$v)
                        $this->$k = $aa[$k];
      }
}


}        // End of directive
?>