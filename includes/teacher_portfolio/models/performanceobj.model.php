<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("PerformanceObj")) {
		class PerformanceObj extends CommonLibdb {
			public function __construct() {
				parent::__construct();
			}

			public function getDataTableArray($UserID=0, $postData = array()) {
				$data = array();
				if (!empty($postData["selyear"]) && $postData["selyear"] != "all") {
					$selectedYear = $postData["selyear"];
				} else {
					$selectedYear = 'all';
				}
				$jsonData = array(
					"recordsTotal" => count($data),
					"recordsFiltered" => count($data),
					"allowExpand" => false,
					"acdyear" => $selectedYear,
					"debugInfo" => $this->core->getMemoryInfo(false),
					"data" => array_values($data)
				);
				return $jsonData;
			}
		}
	}
}