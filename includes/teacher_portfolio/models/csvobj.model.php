<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("parseCSV")) include_once(dirname(dirname(__FILE__)) . "/third_party/parsecsv.lib.php");
	if (!class_exists("CSVObj")) {
		class CSVObj extends parseCSV {
			public function __construct() {
				parent::__construct();
			}

			public function outputCSVHeader($filename) {
				set_time_limit(0);
				/*
				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: private");
				header('Content-Type: text/csv; charset=utf-8');
				header('Content-Disposition: attachment; filename=' . $filename . '.csv');
				/* for debug
				*/
				// header('Content-type: text/plain; charset=utf-8');
			}

			public function outputData($data, $filename) {
				$headerAry = $data[0];
				unset($data[0]);
				$dataAry = array_values($data);
				
				global $PATH_WRT_ROOT;
				if (file_exists($PATH_WRT_ROOT . "includes/libexporttext.php")) include($PATH_WRT_ROOT . "includes/libexporttext.php");
				$lexport = new libexporttext();
				$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="\t", $LineBreak="\r\n", $ColumnDefDelimiter="\t", $DataSize=0, $Quoted="11", $includeLineBreak=1);
				
				$pos = strpos($filename, "sample");
				if ($pos !== false) {
					$fileName = $filename . '.csv';
				} else {
					$fileName = $filename . '_' . date("YmdHis") . '.csv';
				}
				$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");
				exit;
			}
			
			public function writeLine($data) {
				/*
				if (count($data) > 0) {
					// echo implode($this->delimiter, $data) . $this->newLine;
				}
				 */
			}
		}
	}
}