<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("FileInfoObj")) {
		class FileInfoObj extends CommonLibdb {
			public function __construct() {
				parent::__construct();
				$this->mainTable = "INTRANET_TEACHER_PORTFOLIO_FILEINFO";
				$this->userTable = $this->defaultUserTable;
				$this->yearTable = "ACADEMIC_YEAR";
				$this->primaryKey = "InfoID";
				$this->infoModuleSection = "";
				/****************************************************************************/
				/* data field for generating SQL */
				/****************************************************************************/
				$this->dataField = array(
						"UserID", "InfoDate", "InfoNameEn", "InfoNameChi", "TypeNameEn", "TypeNameChi", "InfoOrganizationEn", "InfoOrganizationChi", "InfoSubjectEn", "InfoSubjectChi", "InfoPermission", "InfoRemarkEn", "InfoRemarkChi" );
				
				$this->dataMapping = array();
				/****************************************************************************/
				/* data mapping for making diffenret fields name on Front end */
				/****************************************************************************/
			}
			
			public function setModuleSection($module_section) {
				$this->infoModuleSection = $module_section;
				$this->dataMapping = array(
					"date" => array( "field" => "InfoDate", "required" => true, "type" => "date" ),
					"TeacherName" => array( "field" => Get_Lang_Selection("ChineseName", "EnglishName"), "required" => false, "type" => "text" ),
					"infoName" => array( "field" => Get_Lang_Selection("InfoNameChi", "InfoNameEn"), "required" => false, "type" => "text" ),
					"infoRemark" => array( "field" => Get_Lang_Selection("InfoRemarkEn", "InfoRemarkChi"), "required" => false, "type" => "text" ),
					"infoType" => array( "field" => Get_Lang_Selection("TypeNameChi", "TypeNameEn"), "required" => true, "type" => "text" ),
					"infoOrganization" => array( "field" => Get_Lang_Selection("InfoOrganizationChi", "InfoOrganizationEn"), "required" => false, "type" => "text" ),
					"infoSubject" => array( "field" => Get_Lang_Selection("InfoSubjectChi", "InfoSubjectEn"), "required" => false, "type" => "text" ),
					"chiInfoName" => array( "field" => "InfoNameChi", "required" => true, "type" => "text" ),
					"engInfoName" => array( "field" => "InfoNameEn", "required" => true, "type" => "text" ),
					"chiInfoType" => array( "field" => "TypeNameChi", "required" => false, "type" => "text" ),
					"engInfoType" => array( "field" => "TypeNameEn", "required" => false, "type" => "text" ),
					"chiInfoOrganization" => array( "field" => "InfoOrganizationChi", "required" => false, "type" => "text" ),
					"engInfoOrganization" => array( "field" => "InfoOrganizationEn", "required" => false, "type" => "text" ),
					"chiInfoSubject" => array( "field" => "InfoSubjectChi", "required" => false, "type" => "text" ),
					"engInfoSubject" => array( "field" => "InfoSubjectEn", "required" => false, "type" => "text" ),
					"InfoPermission" => array( "field" => "InfoPermission", "required" => false, "type" => "text" ),
					"engInfoRemark" => array( "field" => "InfoRemarkEn", "required" => false, "type" => "text" ),
					"chiInfoRemark" => array( "field" => "InfoRemarkChi", "required" => false, "type" => "text" ),
					"selectedUserID" => array( "field" => "UserID", "required" => false, "type" => "text" )
				);
				switch ($this->infoModuleSection) {
					case "PERFORMANCE" :
						$this->dataMapping["chiInfoName"]["required"] = true;
						$this->dataMapping["engInfoName"]["required"] = true;
						$this->dataMapping["chiInfoType"]["required"] = true;
						$this->dataMapping["engInfoType"]["required"] = true;
						break;
					case "OTHERS" :
						$this->dataMapping["chiInfoType"]["required"] = true;
						$this->dataMapping["engInfoType"]["required"] = true;
						break;
				}
			}
			
			public function getRecordByID($id) {
				$result = false;
				if ($id > 0) {
					$strSQL = "SELECT * FROM " . $this->mainTable;
					$strSQL .= " WHERE " . $this->primaryKey . "='" . $this->Get_Safe_Sql_Query($id) . "' limit 1";
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) == 1) {
						$result = $result[0];
					}
				}
				return $result;
			}
			
			public function getDataTableArray($UserID=0, $postData = array(), $isAdminMode = false) {
				$data = array();
				$jsonData = array(
					"recordsTotal" => 0,
					"recordsFiltered" => 0,
					"data" => $data
				);
				$recordsTotal = 0;
				$recordsFiltered = 0;
				if (($isAdminMode || $UserID > 0) && isset($postData["columns"]) && count($postData["columns"]) > 0) {
					$main_prefix = "mt.";
					$cpdParam = $this->dataField;
					if ($this->infoModuleSection == "PERFORMANCE") {
						// unset($cpdParam["TypeNameEn"]);
						// unset($cpdParam["TypeNameChi"]);
						$strParam = $main_prefix . "" . $this->primaryKey . ", EnglishName, ChineseName, UserLogin, mt.DateModified, mt.InputBy, " . $main_prefix . implode(", mt.", $cpdParam) . ", YearNameB5 as TypeNameChi, YearNameEn as TypeNameEn";
					} else {
						$strParam = $main_prefix . "" . $this->primaryKey . ", EnglishName, ChineseName, UserLogin, mt.DateModified, mt.InputBy, " . $main_prefix . implode(", mt.", $cpdParam);
					}
					$strTable = $this->mainTable . " AS mt";
					if ($isAdminMode) {
						$strTable .= " JOIN " . $this->userTable . " AS ut ON (mt.UserID=ut.UserID)";
					} else {
						$strTable .= " JOIN " . $this->userTable . " AS ut ON (mt.UserID=ut.UserID and ut.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "')";
					}
					if ($this->infoModuleSection == "PERFORMANCE") {
						if (isset($postData["selyear"]) && $postData["selyear"] != "all") {
							$strTable .= " JOIN " . $this->yearTable . " AS ay ON (AcademicYearID=TypeNameEn AND AcademicYearID='" . $this->Get_Safe_Sql_Query($postData["selyear"]) . "')";
						} else {
							$strTable .= " JOIN " . $this->yearTable . " AS ay ON (AcademicYearID=TypeNameEn)";
						}
					}
					if ($isAdminMode) {
						$strCondition = " WHERE moduleSection ='" . $this->infoModuleSection . "'";
						if (!empty($postData["search"]["value"])) {
							$strCondition .= " AND (EnglishName like '%" . $postData["search"]["value"] . "%' or ChineseName like '%" . $postData["search"]["value"] . "%' OR UserLogin like '%" . $postData["search"]["value"] . "%')";
						}
					} else {
						$strCondition = " WHERE mt.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "' AND moduleSection ='" . $this->infoModuleSection . "'";
					}
					$strSQL = "SELECT COUNT(" . $main_prefix . $this->primaryKey . ") as totalrecord" ;
					$strSQL .= " FROM " . $strTable;
					$strSQL .= $strCondition;
					/************************************************************************/
					$totalRec = $this->returnDBResultSet($strSQL);
					$recordsTotal = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;
					$recordsFiltered = $totalRec[0]["totalrecord"] ? $totalRec[0]["totalrecord"] : 0;
					/************************************************************************/
					$strSQL = "SELECT " . $main_prefix . $this->primaryKey . ", " . $strParam;
					$strSQL .= " FROM " . $strTable;
					$strSQL .= $strCondition;
					/************************************************************************/
					if ($this->infoModuleSection == "PERFORMANCE" && $postData["columns"][$postData["order"][0]["column"]]["data"] == "academicYear") {
						$order_field = Get_Lang_Selection("YearNameB5", "YearNameEn");
					} else {
						$order_field = $this->dataMapping[$postData["columns"][$postData["order"][0]["column"]]["data"]]["field"];
					}
					$strOrder = " ORDER BY " . $order_field . " " . $postData["order"][0]["dir"];
					$strSQL .= $strOrder;
					/************************************************************************/
					$start = $postData["start"] ? $postData["start"] : 0;
					$pagelimit = $postData["length"] > 0 ? $postData["length"] : 0;
					$strLimit = " LIMIT " . $start . ", " . $pagelimit;
					if ($pagelimit > 0) $strSQL .= $strLimit;
					/************************************************************************/
					$queryData = $this->returnDBResultSet($strSQL);

					if (count($queryData) > 0) {
						$queryData = $this->convertArrKeyToID($queryData, $this->primaryKey);
						foreach ($queryData aS $pkey => $RECORD) {
							$data[$pkey] = $RECORD;
							foreach ($postData["columns"] as $kk => $vv) {
								switch ($vv["data"]) {
									case "date":
										$record_data_chi = "-";
										$record_data_eng = "-";
										if (isset($RECORD["InfoDate"])) {
											$record_data_chi = $RECORD["InfoDate"];
											$record_data_eng = $RECORD["InfoDate"];
										}
										$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
										break;
									case "DateModified":
										$record_data = $RECORD["DateModified"];
										break;
									case "TeacherName":
										$record_data_chi = "-";
										$record_data_eng = "-";
										$record_data_chi = $RECORD["ChineseName"];
										$record_data_eng = $RECORD["EnglishName"];
										$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
										if (empty($record_data)) $record_data = $record_data_eng;
										if (empty($record_data)) {
											$record_data = $RECORD["UserLogin"];
										} else {
											// $record_data .= " (" . $RECORD["UserLogin"] . ")"; 
										}
										break;
									case "infoType":
									case "academicYear":
										$record_data_chi = "-";
										$record_data_eng = "-";
										if (isset($RECORD["TypeNameChi"])) $record_data_chi = $RECORD["TypeNameChi"];
										if (isset($RECORD["TypeNameEn"])) $record_data_eng = $RECORD["TypeNameEn"];
										$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
										break;
									case "infoName":
										$record_data_chi = "-";
										$record_data_eng = "-";
										if (isset($RECORD["InfoNameChi"])) $record_data_chi = $RECORD["InfoNameChi"];
										if (isset($RECORD["InfoNameEn"])) $record_data_eng = $RECORD["InfoNameEn"];
										$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
										$data[$pkey]["listTitle"] = $record_data;
										break;
									case "infoRemark":
										$record_data_chi = "-";
										$record_data_eng = "-";
										if (isset($RECORD["InfoRemarkChi"])) $record_data_chi = $RECORD["InfoRemarkChi"];
										if (isset($RECORD["InfoRemarkEn"])) $record_data_eng = $RECORD["InfoRemarkEn"];
										$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
										if (empty($record_data)) $record_data = "-";
										break;
									case "infoOrganization":
										$record_data_chi = "-";
										$record_data_eng = "-";
										if (isset($RECORD["InfoOrganizationChi"])) $record_data_chi = $RECORD["InfoOrganizationChi"];
										if (isset($RECORD["InfoOrganizationEn"])) $record_data_eng = $RECORD["InfoOrganizationEn"];
										$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
										break;
									case "infoSubject":
										$record_data_chi = "-";
										$record_data_eng = "-";
										if (isset($RECORD["InfoSubjectChi"])) $record_data_chi = $RECORD["InfoSubjectChi"];
										if (isset($RECORD["InfoSubjectEn"])) $record_data_eng = $RECORD["InfoSubjectEn"];
										$record_data = Get_Lang_Selection($record_data_chi, $record_data_eng);
										break;
									case "InfoPermission":
										if (isset($RECORD["InfoPermission"])) $record_data = $RECORD["InfoPermission"];
										if ($record_data != "N") $record_data = "Y";
										break;
									case "func":
										$isAllowEdit = false;
										switch ($this->infoModuleSection) {
											case "PERFORMANCE":
												if ($this->core->userInfo["tpConfigs"]["allowOwnerAddOrEditPerformance"] == "Y") {
													$isAllowEdit = true;
												}
												break;
											case "CERTIFICATION":
												if ($this->core->userInfo["tpConfigs"]["allowOwnerAddOrEditCertificate"] == "Y") {
													$isAllowEdit = true;
												}
												break;
											case "OTHERS":
												if ($this->core->userInfo["tpConfigs"]["allowOwnerAddOrEditOthers"] == "Y") {
													$isAllowEdit = true;
												}
												break;
										}
										
										if ($isAdminMode || $UserID == $RECORD["InputBy"] && $isAllowEdit) {
											$record_data = "<div class='text-center'>";
											$record_data .= '<a href="#" class="btn-del" rel="' . $this->core->data["section"] . '"><i class="fa fa-trash"></i></a>';
											$record_data .= '<a href="#" class="btn-edit" rel="' . $this->core->data["section"] . '"><i class="fa fa-pencil"></i></a>';
											$record_data .= '</div>';
										} else {
											$record_data = "-";
										}
										break;
									default:
										if (isset($this->dataMapping[$vv["data"]]["field"])) {
											$org_name = $this->dataMapping[$vv["data"]]["field"];
											switch ($org_name) {
												default:
													$record_data = $RECORD[$org_name];
													break;
											}
										} else {
											$record_data = "";
										}
										break;
								}
								if (empty($record_data)) $record_data = "-";
								$data[$pkey][$vv["data"]] = $record_data;
							}
						}
						/***************************************************************/
						if (count($data) > 0) {
							
						}
						/***************************************************************/
					}
				}
				$jsonData = array(
					"recordsTotal" => $recordsTotal,
					"recordsFiltered" => $recordsFiltered,
					"allowExpand" => false,
					"infonum" => array_keys($data),
					"data" => array_values($data),
					"org_data" => $data
				);
				return $jsonData;
			}

			public function dataHandler($userInfo, $data, $action, $id) {
				$output = array(
					"result" => "fail",
					"data" => "Input Error! Please check"
				);
				$isError = false;
				if ($data["hasError"] == "N") {
					if (empty($data["dataInfo"]["UserID"]["data"])) {
						$data["dataInfo"]["UserID"] = array( "field" => "UserID", "data" => $userInfo["info"]["SelectedUserID"]);
					}
					$cpdParam = $this->dataField;
					
					$cpdParam[] = "moduleSection";
					$cpdParam[] = "DateModified";
					$cpdParam[] = "ModifyBy";
					
					$data["dataInfo"]["moduleSection"] = array( "field" => "moduleSection", "data" => $this->infoModuleSection);
					$data["dataInfo"]["DateModified"] = array( "field" => "DateInput", "data" => "__dbnow__");
					$data["dataInfo"]["ModifyBy"] = array( "field" => "InputBy", "data" => $userInfo["info"]["UserID"]);
					switch ($action) {
						case "INSERT":
							/********* extra field for Insert ***********/
							$cpdParam[] = "DateInput";
							$cpdParam[] = "InputBy";
							$data["dataInfo"]["InputBy"] = array( "field" => "InputBy", "data" => $userInfo["info"]["UserID"]);
							$data["dataInfo"]["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
							
							/********* extra field for Insert ***********/
							
							$strSQL = $this->buildInsertSQL($this->mainTable, $cpdParam, $data["dataInfo"]);
							$result = $this->DBdb_query($strSQL);
							if (!$result) {
								$isError = true;
							} else {
								$id = $this->db_insert_id();
								
								if ($id > 0) {
									$log_name = "";
									$strSQL = "SELECT EnglishName FROM " . $this->userTable . " WHERE UserID='" . $data["dataInfo"]["UserID"]["data"] . "'";
									$result = $this->returnDBResultSet($strSQL);
									if (count($result) > 0) {
										$log_name = $result[0]["EnglishName"];
									}
									if (strtoupper($data["dataInfo"]["moduleSection"]["data"]) == "PERFORMANCE") {
										$log_year = "";
										$strSQL = "SELECT YearNameEN FROM " . $this->yearTable . " WHERE AcademicYearID='" . $data["dataInfo"]["TypeNameEn"]["data"] . "'";
										$result = $this->returnDBResultSet($strSQL);
										if (count($result) > 0) {
											$log_year = $result[0]["YearNameEN"];
										}
									} else {
										$log_year = "-";
									}
									if (!empty($log_name) && !empty($log_year)) {
										$logDetail = "Add Record:<div class='log_detail'>";
										$logDetail .= "<small>"  . $log_name . " / ";
										switch (strtoupper($data["dataInfo"]["moduleSection"]["data"])) {
											case "PERFORMANCE":
												if ($data["dataInfo"]["InfoPermission"]["data"] == "N") {
													$permission = " Permission: N";
												} else {
													$permission = " Permission: Y";
												}
												$logDetail .= $log_year . " / " . $data["dataInfo"]["InfoDate"]["data"] . " / " . $data["dataInfo"]["InfoSubjectEn"]["data"] . " / " . $data["dataInfo"]["InfoNameEn"]["data"] . " / " . $permission;
												break;
											case "CERTIFICATION":
												$logDetail .= $data["dataInfo"]["InfoDate"]["data"] . " / " . $data["dataInfo"]["InfoNameEn"]["data"] . " / " . $data["dataInfo"]["InfoOrganizationEn"]["data"] . " / " . $data["dataInfo"]["InfoSubjectEn"]["data"];
												break;
											case "OTHERS":
												$logDetail .= $data["dataInfo"]["InfoDate"]["data"] . " / " . $data["dataInfo"]["InfoNameEn"]["data"] . " / " . $data["dataInfo"]["TypeNameEn"]["data"];
												break;
										}
										$logDetail .= "</small>";
										$logDetail .= "</div>";
										$this->core->load_model('LogObj');
										$this->core->LogObj->addRecord($userInfo["info"]["UserID"], str_replace("_", " ", strtoupper($data["dataInfo"]["moduleSection"]["data"])), $logDetail);
									}
								}
							}
							$msg = $this->core->data["lang"]["TeacherPortfolio"]["InsertSuccess"];
							break;
						case "UPDATE":
							/********* extra field for Update ***********/
							$condition = $this->primaryKey . "='" . $id . "' AND moduleSection='" . $this->infoModuleSection . "'";
							$strSQL = $this->buildUpdateSQL($this->mainTable, $cpdParam, $data["dataInfo"], $condition);
							$result = $this->DBdb_query($strSQL);
							if (!$result) {
								$isError = true;
							} else {
								$log_name = "";
								$strSQL = "SELECT EnglishName FROM " . $this->userTable . " WHERE UserID='" . $data["dataInfo"]["UserID"]["data"] . "'";
								$result = $this->returnDBResultSet($strSQL);
								if (count($result) > 0) {
									$log_name = $result[0]["EnglishName"];
								}
								if (strtoupper($data["dataInfo"]["moduleSection"]["data"]) == "PERFORMANCE") {
									$log_year = "";
									$strSQL = "SELECT YearNameEN FROM " . $this->yearTable . " WHERE AcademicYearID='" . $data["dataInfo"]["TypeNameEn"]["data"] . "'";
									$result = $this->returnDBResultSet($strSQL);
									if (count($result) > 0) {
										$log_year = $result[0]["YearNameEN"];
									}
								} else {
									$log_year = "-";
								}
								if (!empty($log_name) && !empty($log_year)) {
									$logDetail = "Update Record:<div class='log_detail'>";
									$logDetail .= "<small>"  . $log_name . " / ";
									switch (strtoupper($data["dataInfo"]["moduleSection"]["data"])) {
										case "PERFORMANCE":
											if ($data["dataInfo"]["InfoPermission"]["data"] == "N") {
												$permission = " Permission: N";
											} else {
												$permission = " Permission: Y";
											}
											$logDetail .= $log_year . " / " . $data["dataInfo"]["InfoDate"]["data"] . " / " . $data["dataInfo"]["InfoSubjectEn"]["data"] . " / " . $data["dataInfo"]["InfoNameEn"]["data"] . " / " . $permission;
											break;
										case "CERTIFICATION":
											$logDetail .= $data["dataInfo"]["InfoDate"]["data"] . " / " . $data["dataInfo"]["InfoNameEn"]["data"] . " / " . $data["dataInfo"]["InfoOrganizationEn"]["data"] . " / " . $data["dataInfo"]["InfoSubjectEn"]["data"];
											break;
										case "OTHERS":
											$logDetail .= $data["dataInfo"]["InfoDate"]["data"] . " / " . $data["dataInfo"]["InfoNameEn"]["data"] . " / " . $data["dataInfo"]["TypeNameEn"]["data"];
											break;
									}
									$logDetail .= "</small>";
									$logDetail .= "</div>";
									$this->core->load_model('LogObj');
									$this->core->LogObj->addRecord($userInfo["info"]["UserID"], str_replace("_", " ", strtoupper($data["dataInfo"]["moduleSection"]["data"])), $logDetail);
								}
							}
							$msg = $this->core->data["lang"]["TeacherPortfolio"]["UpdateSuccess"];
							break;
					}
					if (!$isError) {
						$output = array(
							"result" => "success",
							"data" => array( "id" => $id, "msg" => $msg )
						);
					}
					/*
					if (!$isError && $id > 0 && count($data["dataInfo"]["CPDCategoryID"]["data"]) > 0) {
						
						$strSQL = "DELETE FROM " . $this->optionsInfo["category"]["table"] . "_RELATION WHERE CpdID='" . $id . "'";
						$result = $this->DBdb_query($strSQL);
						foreach($data["dataInfo"]["CPDCategoryID"]["data"] as $kk => $vv) {
							$cpdParam = array( "CpdID", "CPDCategoryID" );
							$related_data = array(
								"CpdID" => array( "data" => $id ),
								"CPDCategoryID" => array( "data" => $vv )
							);
							$strSQL = $this->buildInsertSQL($this->optionsInfo["category"]["table"] . "_RELATION", $cpdParam, $related_data);
							$result = $this->DBdb_query($strSQL);
						}
					}
					if (!$isError) {
						$output = array(
							"result" => "success",
							"data" => array( "id" => $id, "msg" => $msg )
						);
					}
					*/
				}
				return $output;
			}
			
			public function deleteRecord($id, $UserID=0) {
				if ($id > 0) {
					if ($UserID > 0) {
						$strSQL = "SELECT * FROM " . $this->mainTable;
						$strSQL .= " WHERE " . $this->primaryKey . "='" . $this->Get_Safe_Sql_Query($id) . "'";
						$result = $this->returnDBResultSet($strSQL);
						
						if (isset($result[0]) && count($result[0]) > 0) {
							$delete_rec = $result[0];
							
							$log_name = "";
							$strSQL = "SELECT EnglishName FROM " . $this->userTable . " WHERE UserID='" . $delete_rec["UserID"] . "'";
							$result = $this->returnDBResultSet($strSQL);
							if (count($result) > 0) {
								$log_name = $result[0]["EnglishName"];
							}
							if (strtoupper($delete_rec["moduleSection"]) == "PERFORMANCE") {
								$log_year = "";
								$strSQL = "SELECT YearNameEN FROM " . $this->yearTable . " WHERE AcademicYearID='" . $delete_rec["TypeNameEn"] . "'";
								$result = $this->returnDBResultSet($strSQL);
								if (count($result) > 0) {
									$log_year = $result[0]["YearNameEN"];
								}
							} else {
								$log_year = "-";
							}
							if (!empty($log_name) && !empty($log_year)) {
								$logDetail = "Delete Record:<div class='log_detail'>";
								$logDetail .= "<small>"  . $log_name . " / ";
								switch (strtoupper($delete_rec["moduleSection"])) {
									case "PERFORMANCE":
										if ($delete_rec["InfoPermission"] == "N") {
											$permission = " Permission: N";
										} else {
											$permission = " Permission: Y";
										}
										$logDetail .= $log_year . " / " . $delete_rec["InfoDate"] . " / " . $delete_rec["InfoSubjectEn"] . " / " . $delete_rec["InfoNameEn"] . " / " . $permission;
										break;
									case "CERTIFICATION":
										$logDetail .= $delete_rec["InfoDate"] . " / " . $delete_rec["InfoNameEn"] . " / " . $delete_rec["InfoOrganizationEn"] . " / " . $delete_rec["InfoSubjectEn"];
										break;
									case "OTHERS":
										$logDetail .= $delete_rec["InfoDate"] . " / " . $delete_rec["InfoNameEn"] . " / " . $delete_rec["InfoOrganizationEn"];
										break;
								}
								$logDetail .= "</small>";
								$logDetail .= "</div>";
								$this->core->load_model('LogObj');
								$this->core->LogObj->addRecord($UserID, str_replace("_", " ", strtoupper($delete_rec["moduleSection"])), $logDetail);
							}
						}
					}
					$strSQL = "DELETE FROM " . $this->mainTable;
					$strSQL .= " WHERE " . $this->primaryKey . "='" . $this->Get_Safe_Sql_Query($id) . "'";
					$result = $this->DBdb_query($strSQL);
					return true;
				}
				return false;
			}
			
			public function getAllData($UserID, $fullFieldData = FALSE) {
				$rowData = array();
				$currLang = Get_Lang_Selection("chi", "eng");
				$main_prefix = "mt.";
				$cpdParam = $this->dataField;
				if (strtolower($this->core->data["section"]) == "performance") {
					unset($cpdParam["TypeNameEn"]);
					unset($cpdParam["TypeNameChi"]);
					$strParam = $main_prefix . "" . $this->primaryKey . ", EnglishName, ChineseName, " . $main_prefix . implode(", mt.", $cpdParam) . ", YearNameB5 as TypeNameChi, YearNameEn as TypeNameEn";
				} else {
					$strParam = $main_prefix . "" . $this->primaryKey . ", EnglishName, ChineseName, " . $main_prefix . implode(", mt.", $cpdParam);
				}
				$strTable = $this->mainTable . " AS mt";
				if ($this->core->data["userInfo"]["Role"]["isAdminMode"]) {
					$strTable .= " JOIN " . $this->userTable . " AS ut ON (mt.UserID=ut.UserID)";
				} else {
					$strTable .= " JOIN " . $this->userTable . " AS ut ON (mt.UserID=ut.UserID and ut.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "')";
				}
				if (strtolower($this->core->data["section"]) == "performance") {
					if (isset($postData["selyear"]) && $postData["selyear"] != "all") {
						$strTable .= " JOIN " . $this->yearTable . " AS ay ON (AcademicYearID=TypeNameEn AND AcademicYearID='" . $this->Get_Safe_Sql_Query($postData["selyear"]) . "')";
					} else {
						$strTable .= " JOIN " . $this->yearTable . " AS ay ON (AcademicYearID=TypeNameEn)";
					}
				}
				if ($this->core->data["userInfo"]["Role"]["isAdminMode"]) {
					$strCondition = " WHERE moduleSection ='" . $this->infoModuleSection . "'";
				} else {
					$strCondition = " WHERE mt.UserID='" . $this->Get_Safe_Sql_Query($UserID) . "' AND moduleSection ='" . $this->infoModuleSection . "'";
				}
				$strSQL = "SELECT " . $main_prefix . $this->primaryKey . ", " . $strParam;
				$strSQL .= " FROM " . $strTable;
				$strSQL .= $strCondition;
				$strSQL .= " ORDER BY EnglishName ASC, mt.InfoDate ASC, mt.InfoNameEn ASC";
				$queryData = $this->returnDBResultSet($strSQL);
				if (count($queryData) > 0) {
					$i = 0;
					$j = 0;
					$rowData[$i]= array();
					if ($fullFieldData) {
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["TeacherName"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
						$j++;
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["TeacherName"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
						$j++;
					} else {
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["TeacherName"];
						$j++;
					}
					if (strtolower($this->core->data["section"]) == "performance") {
						if ($fullFieldData) {
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["SchoolYear"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["SchoolYear"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
							$j++;
						} else {
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["SchoolYear"];
							$j++;
						}
						if ($fullFieldData) {
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["AppraisalType"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["AppraisalType"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
							$j++;
						} else {
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["AppraisalType"];
							$j++;
						}
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["AppraisalFile"] . "";
						$j++;
					}
						
					if (strtolower($this->core->data["section"]) == "others") {
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Date"];
						$j++;
						if ($fullFieldData) {
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Name"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Name"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Type"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Type"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
							$j++;
						} else {
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Name"];
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Type"];
							$j++;
						}
					}
						
					if (strtolower($this->core->data["section"]) == "cert") {
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Date"];
						$j++;
						if ($fullFieldData) {
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Name"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Name"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
							$j++;
				
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Organization"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Organization"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
							$j++;
				
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Subject"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Subject"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
							$j++;
						} else {
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Name"];
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Organization"];
							$j++;
							$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Subject"];
							$j++;
						}
					}
					if ($fullFieldData) {
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Remarks"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_eng"] . ")";
						$j++;
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Remarks"] . " (" . $this->core->data["lang"]["TeacherPortfolio"]["lang_chi"] . ")";
						$j++;
					} else {
						$rowData[$i][$j] = $this->core->data["lang"]["TeacherPortfolio"]["Remarks"];
						$j++;
					}
					$i++;
					foreach ($queryData as $qkk => $qvv) {
						$j = 0;
						if ($fullFieldData) {
							$rowData[$i][$j] = $qvv["EnglishName"];
							$j++;
							$rowData[$i][$j] = $qvv["ChineseName"];
							$j++;
						} else {
							if ($currLang == "eng") {
								$rowData[$i][$j] = $qvv["EnglishName"];
								$j++;
							} else {
								$rowData[$i][$j] = $qvv["ChineseName"];
								$j++;
							}
						}
						if (strtolower($this->core->data["section"]) == "performance") {
							if ($fullFieldData) {
								$rowData[$i][$j] = $qvv["TypeNameEn"];
								$j++;
								$rowData[$i][$j] = $qvv["TypeNameChi"];
								$j++;
								$rowData[$i][$j] = $qvv["InfoSubjectEn"];
								$j++;
								$rowData[$i][$j] = $qvv["InfoSubjectChi"];
								$j++;
								/* filename */
								$rowData[$i][$j] = $qvv["InfoNameEn"];
								$j++;
							} else {
								if ($currLang == "eng") {
									$rowData[$i][$j] = $qvv["TypeNameEn"];
									$j++;
									$rowData[$i][$j] = $qvv["InfoSubjectEn"];
									$j++;
									$rowData[$i][$j] = $qvv["InfoNameEn"];
									$j++;
								} else {
									$rowData[$i][$j] = $qvv["TypeNameChi"];
									$j++;
									$rowData[$i][$j] = $qvv["InfoSubjectChi"];
									$j++;
									$rowData[$i][$j] = $qvv["InfoNameChi"];
									$j++;
								}
							}
						}
						if (strtolower($this->core->data["section"]) == "others") {
							$rowData[$i][$j] = $qvv["InfoDate"];
							$j++;
							if ($fullFieldData) {
								$rowData[$i][$j] = $qvv["InfoNameEn"];
								$j++;
								$rowData[$i][$j] = $qvv["InfoNameChi"];
								$j++;

								$rowData[$i][$j] = $qvv["TypeNameEn"];
								$j++;
								$rowData[$i][$j] = $qvv["TypeNameChi"];
								$j++;
							} else {
								if ($currLang == "eng") {
									$rowData[$i][$j] = $qvv["InfoNameEn"];
									$j++;
									$rowData[$i][$j] = $qvv["TypeNameEn"];
									$j++;
								} else {
									$rowData[$i][$j] = $qvv["InfoNameChi"];
									$j++;
									$rowData[$i][$j] = $qvv["TypeNameChi"];
									$j++;
								}
							}
						}
				
						if (strtolower($this->core->data["section"]) == "cert") {
							$rowData[$i][$j] = $qvv["InfoDate"];
							$j++;
							if ($fullFieldData) {
								$rowData[$i][$j] = $qvv["InfoNameEn"];
								$j++;
								$rowData[$i][$j] = $qvv["InfoNameChi"];
								$j++;
								$rowData[$i][$j] = $qvv["InfoOrganizationEn"];
								$j++;
								$rowData[$i][$j] = $qvv["InfoOrganizationChi"];
								$j++;
								$rowData[$i][$j] = $qvv["InfoSubjectEn"];
								$j++;
								$rowData[$i][$j] = $qvv["InfoSubjectChi"];
								$j++;
							} else {
								if ($currLang == "eng") {
									$rowData[$i][$j] = $qvv["InfoNameEn"];
									$j++;
									$rowData[$i][$j] = $qvv["InfoOrganizationEn"];
									$j++;
									$rowData[$i][$j] = $qvv["InfoSubjectEn"];
									$j++;
								} else {
									$rowData[$i][$j] = $qvv["InfoNameChi"];
									$j++;
									$rowData[$i][$j] = $qvv["InfoOrganizationChi"];
									$j++;
									$rowData[$i][$j] = $qvv["InfoSubjectChi"];
									$j++;
								}
							}
						}
						if ($fullFieldData) {
							$rowData[$i][$j] = $qvv["InfoRemarkEn"];
							$j++;
							$rowData[$i][$j] = $qvv["InfoRemarkChi"];
							$j++;
						} else {
							if ($currLang == "eng") {
								$rowData[$i][$j] = $qvv["InfoRemarkEn"];
								$j++;
							} else {
								$rowData[$i][$j] = $qvv["InfoRemarkChi"];
								$j++;
							}
						}
						$i++;
					}
				}
				return $rowData;
			}
			
			public function exportAllData($CSVObj, $UserID, $isAdminMode = false) {
				$rowData = $this->getAllData($UserID, $isAdminMode, TRUE);
				if (count($rowData) > 0) {
					$total = count($rowData) - 1;
					if ($total > 0) {
						$logDetail = "Data Export: ";
						$logDetail .= "<small>Total " . $total . " Record(s)</small>";
						$this->core->load_model('LogObj');
						$this->core->LogObj->addRecord($this->core->userInfo["info"]["UserID"],  str_replace("_", " ", strtoupper($this->core->data["section"])), $logDetail);
					}
					$CSVObj->outputData($rowData, strtolower($this->core->data["section"]));
					exit;
				} else {
					header('Content-type: text/plain; charset=utf-8');
					echo "Data not found";
					exit;
				}
			}
		}
	}
}