<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("FileObj")) {
		class FileObj extends CommonLibdb {
			public function __construct() {
				parent::__construct();
				$this->primaryKey = "FileID";
				$this->mainTable = "INTRANET_TEACHER_PORTFOLIO_FILE";
				$this->infoTable = "INTRANET_TEACHER_PORTFOLIO_FILEINFO";
				$this->coreData = array();
				$this->fileExtIgnore = array("exe");
				$this->coreKey = "";
				$this->numOfLevel = 3;
				$this->e_version = 'A1';
				$this->customConfig = array();
				$this->fileParam = array( "InfoID", "UserID", "Session", "SystemPath", "OrgFileName", "Checksum", "FileSize", "isDeleted" );
			}

			public function setConfig($config) {
				$this->customConfig = $config;
			}

			public function getMainTable() {
				return $this->mainTable;
			}
			
			public function setCoreData($data) {
				$this->coreData = $data;
			}
			
			public function setPathInfo($pathInfo) {
				$this->corePathInfo = $pathInfo;
			}
			public function setCoreKey($coreKey) {
				$this->coreKey = $coreKey;
			}
			
			public function getFileListByInfoIDs($infoIDs) {
				$output = array();
				if (count($infoIDs) > 0) {
					$strSQL = "SELECT " . $this->primaryKey . ", " . implode(", ", $this->fileParam) . " FROM " . $this->mainTable . " WHERE InfoID in (" . implode(",", $infoIDs) . ") AND isDeleted != '1' ORDER BY InfoID asc, OrgFileName asc";
					$queryData = $this->returnDBResultSet($strSQL);
					if (count($queryData) > 0) {
						foreach ($queryData as $kk => $val) {
							$output[$val["InfoID"]][$val[$this->primaryKey]] = $val;
						}
					}
				}
				return $output;
			}
			
			public function buildFolder($path, $filename) {
				$fileTemp = $filename;
				$path = rtrim($path, '/');
				if (is_dir($path)) {
					for($i=0; $i < $this->numOfLevel; $i++) {
						$path .= DIRECTORY_SEPARATOR . substr($fileTemp, 0, 1);
						$fileTemp = substr($fileTemp, 1);
						if (!is_dir($path)) {
							mkdir($path, 0755);
						}
					}
				}
				return $path;
			}
			
			public function getFileStoragePath($path, $filename) {
				return $filename;
			}

			public function checkFolder($section) {
				$this->recursiveFolderCreation($this->corePathInfo["uploadFilePathMain"] ."/" . $section);
				if (!is_dir($this->corePathInfo["uploadFilePathMain"])) {
					echo "<div class='text-danger'><i class='fa fa-warning'></i> Attachment folder is not found. Please check.</div>";
				}
				if (!is_writable($this->corePathInfo["uploadFilePathMain"])) {
					echo "<div class='text-danger'><i class='fa fa-warning'></i> Attachment folder is not writable. Please check.</div>";
				} else {
					if (!is_dir($this->corePathInfo["uploadFilePathMain"] ."/" . $section)) {
						@mkdir($this->corePathInfo["uploadFilePathMain"] ."/" . $section, 0755);						
					}
				}
				if (!is_dir($this->corePathInfo["uploadFilePathMain"] ."/" . $section)) {
					echo "<div class='text-danger'><i class='fa fa-warning'></i> Attachment folder for " . strtoupper($section) . " is not found. Please check.</div>";
				}
				if (!is_writable($this->corePathInfo["uploadFilePathMain"])) {
					echo "<div class='text-danger'><i class='fa fa-warning'></i> Attachment folder for " . strtoupper($section) . " is not writable. Please check.</div>";
				}
				$this->recursiveFolderCreation($this->corePathInfo["uploadFilePathTemp"]);
				if (!is_dir($this->corePathInfo["uploadFilePathTemp"])) {
					echo "<div class='text-danger'><i class='fa fa-warning'></i> Temp folder is not found. Please check.</div>";
				}
				if (!is_writable($this->corePathInfo["uploadFilePathMain"])) {
					echo "<div class='text-danger'><i class='fa fa-warning'></i> Temp folder is not writable. Please check.</div>";
				}
			}
			
			public function setTPModuleConfig($sys_custom) {
				$this->sys_custom = $sys_custom;
			}
			
			public function uploadFileHandler($section) {
				if (count($this->coreData["userInfo"]) > 0) {
					header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
					header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
					header("Cache-Control: no-store, no-cache, must-revalidate");
					header("Cache-Control: post-check=0, pre-check=0", false);
					header("Pragma: no-cache");
					@set_time_limit(5 * 60);
					
					$session_id = session_id();
					// $targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
					
					$targetDir = $this->corePathInfo["uploadFilePathTemp"];
					$cleanupTargetDir = true; // Remove old files
					$maxFileAge = 10 * 3600; // Temp file age in seconds

					// Create target dir
					if (!file_exists($targetDir)) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenTempFolder"].'"}, "id" : "id"}');
					}
					
					if (isset($_REQUEST["name"])) {
						$fileName = $_REQUEST["name"];
					} elseif (!empty($_FILES)) {
						$fileName = $_FILES["file"]["name"];
					} else {
						$fileName = uniqid("file_");
					}
					
					$ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
					if (in_array($ext, $this->fileExtIgnore)) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_file_extension_err"].'"}, "id" : "id"}');
					}
					
					$upload_date = date("YmdHis");
					
					$encrytFileName = md5($this->coreKey . "_" . $section . "_" . $this->coreData["userInfo"]["info"]["UserID"] . "_" .  $this->coreData["userInfo"]["info"]["SelectedUserID"] . "_" .  $fileName . "_" . $session_id . "_" . $upload_date);
					
					$filePath = $targetDir . DIRECTORY_SEPARATOR . $encrytFileName;
					
					// Chunking might be enabled
					$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
					$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

					// Remove old temp files
					if ($cleanupTargetDir) {
						if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
							die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenTempFolder"].'"}, "id" : "id"}');
						}
						while (($file = readdir($dir)) !== false) {
							$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
							// If temp file is current file proceed to the next
							if ($tmpfilePath == "{$filePath}.part") {
								continue;
							}
							if (filemtime($tmpfilePath) < time() - $maxFileAge) {
								@unlink($tmpfilePath);
							}
						}
						closedir($dir);
					}	
					// Open temp file
					if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenOutputStream"].'"}, "id" : "id"}');
					}
					if (!empty($_FILES)) {
						if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
							die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToMoveUploadedFile"].'"}, "id" : "id"}');
						}
						// Read binary input stream and append it to temp file
						if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
							die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenInputStream"].'"}, "id" : "id"}');
						}
					} else {
						if (!$in = @fopen("php://input", "rb")) {
							die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenInputStream"].'"}, "id" : "id"}');
						}
					}

					while ($buff = fread($in, 4096)) {
						fwrite($out, $buff);
					}
					@fclose($out);
					@fclose($in);

					// Check if file has been uploaded
					if (!$chunks || $chunk == $chunks - 1) {
						// Strip the temp .part suffix off 
						rename("{$filePath}.part", $filePath);
						sleep(1);
					}
					
					if (file_exists($filePath)) {
						$filesize = filesize($filePath);
						
						// $targetPath = $this->buildFolder($this->corePathInfo["uploadFilePathMain"] . DIRECTORY_SEPARATOR . $section . DIRECTORY_SEPARATOR, $encrytFileName);
						$ssl_arr = $this->getEncryptionKeyIV($section . "_" . $this->coreData["userInfo"]["info"]["SelectedUserID"] . "_" .  $fileName);
						$ssl_key = $ssl_arr['key'];
						$ssl_iv = $ssl_arr['iv'];
						
						$enc_file = $targetDir . DIRECTORY_SEPARATOR . $encrytFileName . ".eclass";
						$saveFileResult = $this->saveFile($filePath, $enc_file, $ssl_key, $ssl_iv, 1);
						if (($saveFileResult["msgcode"] == 0) && file_exists($enc_file)) {
							$fileParam = $this->fileParam;
							$fileData = array(
								"InfoID" => array( "field" => "InfoID", "data" => "0" ),
								"UserID" => array( "field" => "UserID", "data" => $this->coreData["userInfo"]["info"]["SelectedUserID"] ),
								"Session" => array( "field" => "Session", "data" => $session_id ),
								"SystemPath" => array( "field" => "SystemPath", "data" => $this->corePathInfo["tbFolder"] . DIRECTORY_SEPARATOR . $section . "" ),
								"OrgFileName" => array( "field" => "OrgFileName", "data" => $fileName ),
								"Checksum" => array( "field" => "Checksum", "data" => $encrytFileName ),
								"isDeleted" => array( "field" => "isDeleted", "data" => '0' ),
								"FileSize" => array( "field" => "FileSize", "data" => $filesize )
							);
							$fileParam[] = "DateInput";
							$fileParam[] = "InputBy";
							$fileData["InputBy"] = array( "field" => "InputBy", "data" => $this->coreData["userInfo"]["info"]["UserID"]);
							$fileData["DateInput"] = array( "field" => "DateInput", "data" => "__dbnow__");
							/********* extra field for Insert ***********/
							
							$strSQL = 'SELECT FileID FROM ' . $this->mainTable;
							$condition = ' InfoID="0" AND Session="' . $session_id . '" AND UserID="' . $this->coreData["userInfo"]["info"]["SelectedUserID"] . '" AND InputBy="' . $this->coreData["userInfo"]["info"]["UserID"] . '" AND OrgFileName="' . $this->Get_Safe_Sql_Query($fileName) . '" AND Checksum="' . $encrytFileName . "'";
							$strSQL .= ' WHERE ' . $condition;
							$result = $this->returnDBResultSet($strSQL);
							if (count($result) > 0) {
								$strSQL = $this->buildUpdateSQL($this->mainTable, $fileParam, $fileData, $condition);
							} else {
								$strSQL = $this->buildInsertSQL($this->mainTable, $fileParam, $fileData);
							}
							$rec_file_list = $fileName;
							
							if (!empty($rec_file_list)) {
								$logDetail = "Upload File:<div class='log_detail'>";
								$logDetail .= $rec_file_list;
								$logDetail .= "</div>";
								$this->core->load_model('LogObj');
								$this->core->LogObj->addRecord($this->coreData["userInfo"]["info"]["UserID"], str_replace("_", " ", strtoupper($this->coreData["section"])), $logDetail);
							}
							
							$result = $this->DBdb_query($strSQL);
							if (!$result) {
								/* if (file_exists($filePath) && !empty($filePath) && !is_dir($filePath)) {
									unlink($filePath);
								} */
								$isError = true;
							} else {
								$id = $this->db_insert_id();
								die('{"jsonrpc" : "2.0", "result" : "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_Success"].'", "id" : "' . $id . '"}');
							}
						} else {
							/*
							if (file_exists($filePath) && !empty($filePath) && !is_dir($filePath)) {
								unlink($filePath);
							}
							*/
							die('{"jsonrpc" : "2.0", "result" : "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_Success"].'", "id" : "id"}');
						}
					} else if ($chunks > 0) {
						die('{"jsonrpc" : "2.0", "error" : {""message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedUploadingProcessing"].'"}, "id" : "id"}');
					}
				}
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failed"].'"}, "id" : "id"}');
			}
			
			public function getEncryptionKeyIV($fileName) {
				$passphrase = $this->customConfig['passphrase'];
				$masterKey = $this->customConfig['masterKey'];
				$version = $this->e_version;
				switch($version){
					case 'A1':			
						$partA = hash_hmac('sha512', $masterKey, $passphrase);
						$partB = hash_hmac('sha512', $fileName, $passphrase . $masterKey);
						$key = hash_hmac('sha256', $partA . $passphrase . $fileName, $partB);
						$IV = hash_hmac('md5', $partB . $passphrase . $fileName, $fileName);
						break;
				}
				$Arr['key'] = $key;
				$Arr['iv'] = $IV;
				$Arr['verions'] = $version;
				return $Arr;
			}
			
			public function saveFile($inputFilePath, $outputFilePath, $key, $iv, $overwrittern = 0){
				$output = array( "result" => "failed", "msg" => "Error", "msgcode" => -1 );
				$isInputFileExist = file_exists($inputFilePath);
				if(! $isInputFileExist){
					$output = array( "result" => "failed", "msg" => "input file is not exist", "msgcode" => -1 );
					return $output;
				}
				$isOutputFileExist = file_exists($outputFilePath);
				if($overwrittern){
					if ($isOutputFileExist && !is_dir($outputFilePath) && !empty($outputFilePath)) {
						@unlink($isOutputFileExist);
					}
				} else {
					if($isOutputFileExist){
						$output = array( "result" => "failed", "msg" => "output file is already exist, overwritten is turned off", "msgcode" => -3 );
						return $output;
					}
				}
				if (defined('isDev')) {
					rename($inputFilePath, $outputFilePath);
				} else {
					$command = 'openssl aes-256-cbc -in ' . escapeshellarg($inputFilePath) . ' -out ' . escapeshellarg($outputFilePath) . ' -K ' . $key . ' -iv ' . $iv . '';
					$result_e = trim(shell_exec($command));
				}
				$isOutputFileExist = file_exists($outputFilePath);
				if(!$isOutputFileExist){
					$output = array( "result" => "failed", "msg" => "output file is not exist, something wrong!!", "msgcode" => -2 );
					return $output;
				}else{
					$output = array( "result" => "success", "msg" => "success", "msgcode" => 0 );
					return $output;
				}
			}
			
			public function filePrepareToUser($section, $fileInfo) {
				$curr_date = date("YmdHis");
				$encrytFileName = $fileInfo["Checksum"];
				$targetPath = $this->buildFolder($this->corePathInfo["uploadFilePathMain"] . DIRECTORY_SEPARATOR . $section, $encrytFileName);
				$filePath = $targetPath . DIRECTORY_SEPARATOR . $encrytFileName;
				if (file_exists($filePath) && !is_dir($filePath) && !empty($encrytFileName)) {
					$targetDir = $this->corePathInfo["uploadFilePathTemp"];
					$encrytFileNameForTmp = md5( $this->coreKey . "_" . $section . "_" . $fileInfo["UserID"] . "_" .  $fileInfo["OrgFileName"] . "_tmp" . $this->coreData["userInfo"]["info"]["SelectedUserID"]) . "_" . $curr_date;
					$targetFile = $this->corePathInfo["uploadFilePathTemp"] . DIRECTORY_SEPARATOR . $encrytFileNameForTmp;

					$ssl_arr = $this->getEncryptionKeyIV($section . "_" . $fileInfo["UserID"] . "_" .  $fileInfo["OrgFileName"]);
					$ssl_key = $ssl_arr['key'];
					$ssl_iv = $ssl_arr['iv'];

					$openFileResult = $this->getFile($filePath, $targetFile, $ssl_key, $ssl_iv, 0);
					if (($openFileResult["msgcode"] == 0) && file_exists($targetFile)) {
						return $targetFile;
					}
				}
				return false;
			}

			public function getFile($inputFilePath, $outputFilePath, $key, $iv, $overwrittern = 0){
				$output = array( "result" => "failed", "msg" => "Error", "msgcode" => -1 );
				$isInputFileExist = file_exists($inputFilePath);
				if(! $isInputFileExist){
					$output = array( "result" => "failed", "msg" => "input file is not exist", "msgcode" => -1 );
					return $output;
				}
				$isOutputFileExist = file_exists($outputFilePath);
				if(!$overwrittern){
					if($isOutputFileExist){
						$output = array( "result" => "failed", "msg" => "output file is already exist, overwritten is turned off", "msgcode" => -3 );
						return $output;
					}
				} else {
					if ($isOutputFileExist && !is_dir($outputFilePath) && !empty($outputFilePath)) {
						@unlink($isOutputFileExist);
					}
				}
				if (defined('isDev')) {
					copy($inputFilePath, $outputFilePath);
				} else {
					$command = 'openssl aes-256-cbc -d -in ' . escapeshellarg($inputFilePath) . ' -out ' . escapeshellarg($outputFilePath) . ' -K ' . $key . ' -iv ' . $iv . '';
					$result_e = trim(shell_exec($command));
				}
				$isOutputFileExist = file_exists($outputFilePath);
				if(!$isOutputFileExist){
					$output = array( "result" => "failed", "msg" => "output file is not exist, something wrong!!", "msgcode" => -2 );
					return $output;
				}else{
					$output = array( "result" => "success", "msg" => "success", "msgcode" => 0 );
					return $output;
				}
			}

			public function getFilesByInfoID($infoID, $UserID, $condition = "") {
				$filesArr = array();
				$strSQL = 'SELECT ' . $this->primaryKey . ', ' . implode(', ', $this->fileParam) . " FROM " . $this->mainTable . "";
				if (!empty($condition)) {
					$condition .= ' AND ';
				}
				$condition .= ' InfoID="' . $infoID . '" AND isDeleted="0"';
				$strSQL .= " WHERE " . $condition;
				$strSQL .= " ORDER BY InfoID asc, OrgFileName asc";
				$result = $this->returnDBResultSet($strSQL);
				if (count($result) > 0) {
					$filesArr = $this->convertArrKeyToID($result, $this->primaryKey);
				}
				return $filesArr;
			}

			public function validParam($getData = array(), $UserId) {
				if (count($getData) > 0 && isset($getData["d"]) && isset($getData["flid"]) && isset($getData["skey"])) {
					if ($this->checkInfoPermission($getData["flid"])) {
						$result = $this->getFileByID($getData["flid"], $UserId);
						if (isset($result)) {
							$file_md5 = md5($result["FileID"] . "_" . $result["FileSize"] . '_' . $getData["d"]);
							if ($file_md5 == $getData["skey"]) {
								return $result;
							}
						}
					}
				}
				return false;
			}

			public function checkInfoPermission($infoId) {
				if (!empty($infoId)) {
					$strSQL = "SELECT InfoPermission FROM " . $this->mainTable . " AS file";
					$strSQL .= " JOIN " . $this->infoTable . " AS info ON (file.InfoID=info.InfoID)";
					$strSQL .= " WHERE file.FileID='" . $infoId . "' LIMIT 1";
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) > 0) {
						if ($result[0]["InfoPermission"] != "N") {
							return true;
						}
					}
				}
				return false;
			}
			
			public function getFileByID($fileID, $UserID, $condition = "") {
				$filesArr = array();
				$strSQL = 'SELECT ' . $this->primaryKey . ', ' . implode(', ', $this->fileParam) . " FROM " . $this->mainTable . "";
				if (!empty($condition)) {
					$condition .= ' AND ';
				}
				$condition .= ' FileID="' . $fileID . '" AND isDeleted="0"';
				// $condition .= ' FileID="' . $fileID . '" AND UserID="' . $UserID . '" AND isDeleted="0"';
				$strSQL .= " WHERE " . $condition;
				$result = $this->returnDBResultSet($strSQL);
				if (count($result) > 0) {
					$filesArr = $this->convertArrKeyToID($result, $this->primaryKey);
					$filesArr = $filesArr[$fileID];
				}
				return $filesArr;
			}

			public function dataHandler($section, $infoID, $filesID, $UserID) {
				if ($filesID == null) {
					$strSQL = "UPDATE " . $this->mainTable . " set isDeleted='1' WHERE UserID='" . $UserID . "' AND InfoID='" . $infoID . "'";
					$result = $this->DBdb_query($strSQL);
				} else if (count($filesID) > 0) {
					$currDate = date("YmdHis");
					$currDate_str = date("Y-m-d H:i:s");
					$condition = "";
					$strSQL = "UPDATE " . $this->mainTable . " set isDeleted='1' WHERE UserID='" . $UserID . "' AND InfoID='" . $infoID . "' AND FileID NOT IN (" . implode(", ", $filesID) . ")";
					$result = $this->DBdb_query($strSQL);
					
					$strSQL = 'SELECT ' . $this->primaryKey . ', inputBy, ' . implode(', ', $this->fileParam) . " FROM " . $this->mainTable . "";
					$condition .= ' FileID IN (' . implode(", ", $filesID) . ') AND UserID="' . $UserID . '"';
					$strSQL .= " WHERE " . $condition;
					$result = $this->returnDBResultSet($strSQL);
					if (count($result) > 0) {
						$filesArr = $this->convertArrKeyToID($result, $this->primaryKey);
						$targetDir = $this->corePathInfo["uploadFilePathTemp"];
						if (count($filesArr) > 0) {
							foreach($filesArr as $kk => $vv) {
								$enc_file = $targetDir . DIRECTORY_SEPARATOR . $vv["Checksum"] . ".eclass";
								if (file_exists($enc_file)) {
									$encrytFileName = md5($this->coreKey . "_" . $section . "_" . $vv["inputBy"] . "_" .  $UserID . "_" .  $vv["OrgFileName"] . "_system" . "_" . $vv["FileID"] . "_" . $currDate);
									$finalPath = $this->buildFolder($this->corePathInfo["uploadFilePathMain"] . DIRECTORY_SEPARATOR . $section, $encrytFileName);
									$final_filepath = $finalPath . DIRECTORY_SEPARATOR . $encrytFileName;
									rename($enc_file, $final_filepath);
									$param = array(
										"Session" => "system",
										"Checksum" => $encrytFileName,
										"isDeleted" => 0,
										"FileSize" => filesize($final_filepath),
										"InfoID" => $infoID,
										"DateInput" => $currDate_str
									);
									$strSQL = "UPDATE " . $this->mainTable . " set ";
									$update_param = "";
									foreach ($param as $pkey => $pval) {
										if (!empty($update_param)) {
											$update_param .= ",";
										}
										$update_param .= $pkey . '="' . $pval . '"';
									}
									if (!empty($update_param)) {
										$strSQL .= $update_param;
										$strSQL .= " WHERE FileID='" . $vv["FileID"] . "'";
										$result = $this->DBdb_query($strSQL);
									}
								}
							}
						}
					}
				}
				$this->clearFileProcess($section, $infoID);
			}
			
			private function clearFileProcess($section, $infoID) {
				$strSQL = 'SELECT ' . $this->primaryKey . ', inputBy, ' . implode(', ', $this->fileParam) . " FROM " . $this->mainTable . "";
				$condition = ' InfoID="' . $infoID . '" AND isDeleted="1"';
				$strSQL .= " WHERE " . $condition;
				$result = $this->returnDBResultSet($strSQL);
				if (count($result) > 0) {
					$filesArr = $this->convertArrKeyToID($result, $this->primaryKey);
					if (count($filesArr) > 0) {
						foreach ($filesArr as $key => $val) {
							// $encrytFileName = md5($this->coreKey . "_" . $section . "_" . $vv["inputBy"] . "_" .  $UserID . "_" .  $vv["OrgFileName"] . "_system" . "_" . $vv["FileID"] . "_" . $currDate);
							$finalPath = $this->buildFolder($this->corePathInfo["uploadFilePathMain"] . DIRECTORY_SEPARATOR . $section, $val["Checksum"]);
							$final_filepath = $finalPath . DIRECTORY_SEPARATOR . $val["Checksum"];
							if (is_file($final_filepath) && file_exists($final_filepath)) @unlink($final_filepath);
						}
						if (!empty($condition)) {
							$strSQL = "DELETE FROM " . $this->mainTable . "";
							$strSQL .= " WHERE " . $condition;
							$result = $this->DBdb_query($strSQL);
						}
					}
				}
				$this->clearfiles($section);
			}
			
			public function removeAllFile($section, $infoID, $UserID = 0) {
				$strSQL = 'SELECT ' . $this->primaryKey . ', inputBy, ' . implode(', ', $this->fileParam) . " FROM " . $this->mainTable . "";
				$condition = ' InfoID="' . $infoID . '"';
				$strSQL .= " WHERE " . $condition;
				$result = $this->returnDBResultSet($strSQL);
				if (count($result) > 0) {
					$filesArr = $this->convertArrKeyToID($result, $this->primaryKey);
					if (count($filesArr) > 0) {
						$rec_file_list = "";
						foreach ($filesArr as $key => $val) {
							// $encrytFileName = md5($this->coreKey . "_" . $section . "_" . $vv["inputBy"] . "_" .  $UserID . "_" .  $vv["OrgFileName"] . "_system" . "_" . $vv["FileID"] . "_" . $currDate);
							$finalPath = $this->buildFolder($this->corePathInfo["uploadFilePathMain"] . DIRECTORY_SEPARATOR . $section, $val["Checksum"]);
							$final_filepath = $finalPath . DIRECTORY_SEPARATOR . $val["Checksum"];
							if (is_file($final_filepath) && file_exists($final_filepath)) @unlink($final_filepath);

							$rec_file_list .= "<small>" . $val["OrgFileName"] . "</small><br>";
							$module = trim(str_replace("teacher_portfolio/", "", $val["SystemPath"]));
						}
						if (!empty($condition)) {
							if (!empty($rec_file_list)) {
								$logDetail = "Delete File:<div class='log_detail'>";
								$logDetail .= $rec_file_list;
								$logDetail .= "</div>";
								$this->core->load_model('LogObj');
								$this->core->LogObj->addRecord($UserID, str_replace("_", " ", strtoupper($module)), $logDetail);
							}
							$strSQL = "DELETE FROM " . $this->mainTable . "";
							$strSQL .= " WHERE " . $condition;
							$result = $this->DBdb_query($strSQL);
						}
					}
				}
			}
			
			public function importCSVFileHandler($section) {
				if (count($this->coreData["userInfo"]) > 0) {
					header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
					header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
					header("Cache-Control: no-store, no-cache, must-revalidate");
					header("Cache-Control: post-check=0, pre-check=0", false);
					header("Pragma: no-cache");
					@set_time_limit(5 * 60);
					
					$session_id = session_id();
					// $targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
					
					$targetDir = $this->corePathInfo["uploadFilePathTemp"];
					$cleanupTargetDir = true; // Remove old files
					$maxFileAge = 10 * 3600; // Temp file age in seconds

					// Create target dir
					if (!file_exists($targetDir)) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenTempFolder"].'"}, "id" : "id"}');
					}
					
					if (isset($_REQUEST["name"])) {
						$fileName = $_REQUEST["name"];
					} elseif (!empty($_FILES)) {
						$fileName = $_FILES["file"]["name"];
					} else {
						$fileName = uniqid("file_");
					}
					
					$ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
					if (in_array($ext, $this->fileExtIgnore)) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_file_extension_err"].'"}, "id" : "id"}');
					}
					
					$upload_date = date("YmdHis");
					
					$encrytFileName = $section . "_" . md5($this->coreKey . "_" . $section . "_" . $this->coreData["userInfo"]["info"]["UserID"] . "_" .  $this->coreData["userInfo"]["info"]["SelectedUserID"] . "_" . $session_id) . ".csv";
					
					$filePath = $targetDir . DIRECTORY_SEPARATOR . $encrytFileName;
					if (file_exists($filePath)) unlink($filePath);

					// Chunking might be enabled
					$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
					$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

					// Remove old temp files
					if ($cleanupTargetDir) {
						if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
							die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenTempFolder"].'"}, "id" : "id"}');
						}
						while (($file = readdir($dir)) !== false) {
							$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
							// If temp file is current file proceed to the next
							if ($tmpfilePath == "{$filePath}.part") {
								continue;
							}
							if (filemtime($tmpfilePath) < time() - $maxFileAge) {
								@unlink($tmpfilePath);
							}
						}
						closedir($dir);
					}	
					// Open temp file
					if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
						die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenOutputStream"].'"}, "id" : "id"}');
					}
					if (!empty($_FILES)) {
						if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
							die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToMoveUploadedFile"].'"}, "id" : "id"}');
						}
						// Read binary input stream and append it to temp file
						if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
							die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenInputStream"].'"}, "id" : "id"}');
						}
					} else {
						if (!$in = @fopen("php://input", "rb")) {
							die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failedToOpenInputStream"].'"}, "id" : "id"}');
						}
					}

					while ($buff = fread($in, 4096)) {
						fwrite($out, $buff);
					}
					@fclose($out);
					@fclose($in);

					// Check if file has been uploaded
					if (!$chunks || $chunk == $chunks - 1) {
						// Strip the temp .part suffix off 
						rename("{$filePath}.part", $filePath);
						sleep(1);
					}
					if (file_exists($filePath)) {
						// $output = array( "result" => "success", "msg" => "success", "filename" => basename($filePath), "msgcode" => 0 );
						die('{"jsonrpc" : "2.0", "result" : "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_Success"].'", "fname":"' . basename($fileName) . '", "id" : "id"}');
					}
				}
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "'.$this->coreData["lang"]['TeacherPortfolio']["fileupload_failed"].'"}, "id" : "id"}');
			}
			
			public function validImportedCSV($CSVObj, $section) {
				$output = array(
					"jsonrpc" => "2.0",
					"errorCode" => "200",
					"Msg" => $this->coreData["lang"]["TeacherPortfolio"]["fileupload_failed"],
					"csvData" => array()
				);
				
				$targetDir = $this->corePathInfo["uploadFilePathTemp"];
				$cleanupTargetDir = true; // Remove old files
				$maxFileAge = 10 * 3600; // Temp file age in seconds
				
				$session_id = session_id();
				$csvFileName = $section . "_" . md5($this->coreKey . "_" . $section . "_" . $this->coreData["userInfo"]["info"]["UserID"] . "_" .  $this->coreData["userInfo"]["info"]["SelectedUserID"] . "_" . $session_id);
				
				$filePath = $targetDir . DIRECTORY_SEPARATOR . $csvFileName . ".csv";
				if (file_exists($filePath)) {
					$CSVObj->encoding('UTF-16LE','UTF-8');
					$CSVObj->delimiter = "\t";
					$CSVObj->parse($filePath);
					if (count($CSVObj->data) > 0) {
						$limit = 5;
						if (count($CSVObj->data) <= $limit) {
							$limit = count($CSVObj->data);
						}
						$latestHeader = array();
						for($i=0;$i < $limit; $i++) {
							$tmp_header = array_keys($CSVObj->data[$i]);
							if (count($tmp_header) > count($latestHeader)) {
								$latestHeader = $tmp_header;
							}
							
						}
						$output = array(
							"jsonrpc" => "2.0",
							"result" => "success",
							"totalrec" => count($CSVObj->data),
							"csvDataHead" => $latestHeader,
							"csvData" => $CSVObj->data
						);
					} else {
						$output = array(
							"jsonrpc" => "2.0",
							"errorCode" => "200",
							"Msg" => $this->coreData["lang"]["TeacherPortfolio"]["csv_formatInCorrect"],
							"csvData" => array()
						);
					}
				}
				return $output;
			}
			
			public function scanCustDir($path) {
				$this->core->load_thirdparty("directoryiterator.php");
				$files = array();
				$folder = new DirectoryIterator($path);
				while ($folder->valid()) {
					if (!$folder->isDot()) {
						$filename = $folder->getFilename();
						$path = $folder->getPathname();
						if ($folder->isDir()) {
							$files[$filename] = $this->scanCustDir($path);
							if (count($files[$filename]) == 0) {
								@rmdir($path);
								unset($files[$filename]);
							}
						} else {
							$files[$filename] = array(
								$folder->getPath(),
								$folder->getType(),
								$folder->isWritable(),
								$folder->isReadable(),
								$folder->isExecutable()
							);
						}
					}
					$folder->next();
				}
				return $files;
			}
			
			public function clearfiles($section) {
				$check_path = $this->corePathInfo["uploadFilePathMain"];
				$strSQL = "SELECT " . $this->primaryKey . ", " . implode(", ", $this->fileParam) . ", DateInput FROM " . $this->mainTable . " WHERE InfoID='0' AND DateInput < NOW() - INTERVAL 1 DAY";
				$result = $this->returnDBResultSet($strSQL);
				$filesArr = array();
				if (count($result) > 0) {
					$filesArr = $this->convertArrKeyToID($result, $this->primaryKey);
					if (count($filesArr) > 0) {
						foreach ($filesArr as $key => $val) {
							$finalPath = $this->buildFolder($this->corePathInfo["uploadFilePathMain"] . DIRECTORY_SEPARATOR . $section, $val["Checksum"]);
							$final_filepath = $finalPath . DIRECTORY_SEPARATOR . $val["Checksum"];
							if (is_file($final_filepath) && file_exists($final_filepath)) @unlink($final_filepath);
						}
						$strSQL = "DELETE FROM " . $this->mainTable . " WHERE InfoID='0' AND " . $this->primaryKey . " IN (" . implode(", ", array_keys($filesArr)) . ")";
						$result = $this->DBdb_query($strSQL);
					}
				}
				$files = $this->scanCustDir($check_path);
			}
			
			private function recursiveFolderCreation($path){
				$pathArr = explode("/",$path);
				$currPath = "";
				foreach($pathArr as $idx=>$pathComp){
					if($idx==0)continue;
					$currPath = $currPath."/".$pathComp;
					if(!is_dir($currPath)){
						@mkdir($currPath, 0755);
					}
				}
			}
		}
	}
}
