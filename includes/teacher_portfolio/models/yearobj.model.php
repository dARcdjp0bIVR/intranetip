<?php
if (defined('TEACHER_PORTFOLIO')) {
	if (!class_exists("CommonLibdb")) include_once(dirname(__FILE__) . "/commonlibdb.model.php");
	if (!class_exists("YearObj")) {
		class YearObj extends CommonLibdb {
			public function __construct() {
				parent::__construct();
				$this->primaryKey = "AcademicYearID";
				$this->tableName = "ACADEMIC_YEAR";
			}
			
			public function getMainTable() {
				return $this->tableName;
			}

			public function getAcademicYear() {
				$yearArr = array();
				$sqlParam = array(
					"param" => $this->primaryKey . ", YearNameEN, YearNameB5",
					"table" => $this->tableName,
					"condition" => null,
					"order" => "Sequence ASC",
					"limit" => null
				);
				$strSQL = $this->buildSelectSQL($sqlParam);
				if (!empty($strSQL)) {
					$result = $this->returnDBResultSet($strSQL);
					$yearArr = $this->convertArrKeyToID($result, $this->primaryKey);
				}
				return $yearArr;
			}

			public function getYearPanel($yearArr = array(), $lang = array(), $section = "", $selectedItem = "") {
				$html = "";
				$label = $lang["TeacherPortfolio"]["allSchoolYear"];
				if (!empty($selectedItem) && $selectedItem != "all") {
					$label =  Get_Lang_Selection($yearArr[$selectedItem]["YearNameB5"], $yearArr[$selectedItem]["YearNameEN"]);
				}
				$html .= '<button type="button" class="btn btn-default selectYearElt dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $label . '<span class="caret"></span></button>';
				$html .= '<ul class="dropdown-menu">';
				if ($selectedItem != "") {
					$html .= '<li><a href="#" class="AcademicYear active year_all" rel="' . $section . '/all">' . $lang["TeacherPortfolio"]["allSchoolYear"] . '</a></li>';
				} else {
					$html .= '<li><a href="#" class="AcademicYear year_all" rel="' . $section . '/all">' . $lang["TeacherPortfolio"]["allSchoolYear"] . '</a></li>';
				}
				if (isset($yearArr) && count($yearArr) > 0) {
					foreach ($yearArr as $kk => $vv) {
						if ($selectedItem == $kk) {
							$html .= '<li><a href="#" class="AcademicYear active year_' . $kk . '" rel="' . $section . "/" . $kk . '">' . Get_Lang_Selection($vv["YearNameB5"], $vv["YearNameEN"]) . '</a></li>';
						} else {
							$html .= '<li><a href="#" class="AcademicYear year_' . $kk . '" rel="' . $section . "/" . $kk . '">' . Get_Lang_Selection($vv["YearNameB5"], $vv["YearNameEN"]) . '</a></li>';
						}
					}
				}
				$html .= '</ul>';
				return $html;
			}
		}
	}
}