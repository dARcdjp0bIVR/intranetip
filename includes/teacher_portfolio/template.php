<?php
if (defined('TEACHER_PORTFOLIO')) {
	class Template {
		public function __construct() {
			$this->apppath = dirname(dirname(__FILE__));
			$this->templatePath = $this->apppath . "/views";
		}
		
		public function setTemplatePath($path) {
			$this->apppath = $path;
			$this->templatePath = $this->apppath . "/views";
		}
		
		public function view($tpl, $data = array(), $echoAllow = true) {
			$output = "";
			$templateName = $tpl . ".tpl.php";
			if (!file_exists($this->templatePath . "/" . $templateName)) {
				$templateName = "404.tpl.php";
			}
			$data["_CUSTOMIZATION_"] = $data["userInfo"]["customization"];
			$data["_ROLE_"] = $data["userInfo"]["Role"];
			unset($data["userInfo"]["Role"]);
			unset($data["userInfo"]["customization"]);
//			$data["defaultFooter"] = '<div class="footer"> <span>Powered by <a href="http://www.eclass.com.hk" target="_blank"><img src="' . $data["AssetsPath"] . '/images/logo_eclass_footer.gif" width="39" height="15" border="0" align="absmiddle"></a></span></div>';
            $data["defaultFooter"] = '<div class="footer"> <span><a href="http://www.eclass.com.hk" target="_blank"><img src="' . $data["AssetsPath"] . '/images/logo_eclass_footer.png" width="59" height="21" border="0" align="absmiddle"></a></span></div>';
			if (isset($data["debugMsg"])) $data["defaultFooter"] .= $data["debugMsg"];
			require_once($this->templatePath . "/" . $templateName);
		}
	}
}