<?php
if (defined('TEACHER_PORTFOLIO')) {
	class libtbcore {
		public function __construct() {
			$this->PATH_WRT_ROOT = "";
			$this->task = "";
			$this->assets_file = true;
			$this->urlInfo = array();
			$this->pathInfo = array();
			$this->configs = array();
			$this->debugInfo = array();
			$this->debugInfo["db_query_count"] = 0;
			$this->debugInfo["db_query_arr"] = array();
			$this->configFile = "";
			$this->modelPath = dirname(__FILE__) . "/models";
			$this->helperPath = dirname(__FILE__) . "/helpers";
			$this->thirdpartyPath = dirname(__FILE__) . "/third_party";
			$this->tempPath = dirname(__FILE__) . "/tmp";
			
			$this->coreKey = "aksjtl98275";
			$this->sslkey = "aks98275jtl45678";
			$this->sslIV = "98aksj27tl085";
			$this->data["ajaxKey"] = md5($this->coreKey + "_" + $this->get_client_ip());
			$this->data['max_upload'] = $this->_Byte2Size($this->_GetMaxAllowedUploadSize());
		}

		public function autoLoad() {
			if (isset($this->configs["autoLoad"]["helpers"]) && count($this->configs["autoLoad"]["helpers"]) > 0) {
				foreach ($this->configs["autoLoad"]["helpers"] as $helperkey => $helper_name) {
					$this->load_helper($helper_name);
				}
			}
			if (isset($this->configs["autoLoad"]["models"]) && count($this->configs["autoLoad"]["models"]) > 0) {
				foreach ($this->configs["autoLoad"]["models"] as $modelkey => $model_name) {
					$this->load_model($model_name);
				}
			}
		}

		public function setPath($pathInfo) {
			$this->pathInfo = $pathInfo;
			$this->curr_path = $this->pathInfo["curr_path"];
			$this->configFile = $this->curr_path . "/configs/";
		}
		
		public function load_config($configFile = "teacher_portfolio.php") {
			if (file_exists($this->configFile . $configFile)) {
				require_once($this->configFile . $configFile);
				$this->configs = $_configs;
				if (defined('isDev')) {
					$this->configs["isDev"] = true;
				} else {
					$this->configs["isDev"] = false;
				}
				unset($_configs);
			}
		}

		public function getDBLink() {
			if (defined('isDev')) {
				/* for Development only */
				$serverName = "localhost";
				$userName = "bldev";
				$password = "bldev";
				$database = "bl_teacher_portfolio";
				// Create connection
				try {
					$this->dbLink = @mysql_connect($serverName, $userName, $password) or die ("Database not found");
					if (!$this->dbLink) {
						die('Could not connect: ' . mysql_error());
					}
				} catch (Exception $e) {
					 die ("Database not found");
				}
				$db_selected = mysql_select_db($database, $this->dbLink);
				if (!$db_selected) { die ('Database access error : ' . mysql_error());}
				mysql_query('SET NAMES utf8', $this->dbLink);
				// return $this->dbLink;
				return $database;
			}
		}
		
		public function templateSetup() {
			if (file_exists(dirname(__FILE__) . "/template.php")) {
				include_once(dirname(__FILE__) . "/template.php");
				$this->tpl = new Template();
			} else {
				if (file_exists($this->curr_path . "/template.php")) {
					include_once($this->curr_path . "/template.php");
					$this->tpl = new Template();
				} else {
					$this->tpl = null;
				}
			}
			if ($this->tpl != null) {
				$this->tpl->setTemplatePath($this->curr_path);
			}
		}
		
		public function getCustomConfig() {
			return $this->configs;
		}
		
		public function getTMConfig() {
			return $this->TPModuleConfig;
		}
		
		public function setTeacherPortfolioConfigs($TPConfig) {
			$this->TPModuleConfig = $TPConfig;
			$this->taskSeparator = $TPConfig["taskSeparator"];
			if (empty($this->taskSeparator)) $this->taskSeparator = ".";
		}

		public function setCurrentRootFolder($path, $controller = "controllers", $view = "views", $assets = "assets", $model='models', $default_controller = "welcome") {
			/****************************************************/
			$this->PATH_WRT_ROOT = $path;
			$this->defaultTask = $default_controller;
			$this->controllers_path = $controller;
			$this->views_path = $view;
			$this->assets_path = $assets;
			/****************************************************/
			$this->data["PATH_WRT_ROOT"] = $this->PATH_WRT_ROOT;
			
			$this->ControllerPath = $this->PATH_WRT_ROOT . $this->pathInfo["path"] . "/" . $this->controllers_path;
			if (!is_dir($this->ControllerPath)) {
				echo "<div class='error'>Controller Folder not found</div>";
			}
			$this->ViewsPath = $this->PATH_WRT_ROOT . $this->pathInfo["path"] . "/" . $this->views_path;
			if (!is_dir($this->ViewsPath)) {
				echo "<div class='error'>Views Folder not found</div>";
			}
			if (!is_dir($this->modelPath)) {
				echo "<div class='error'>Model Folder not found</div>";
			}
			$this->data["AssetsPath"] = $this->PATH_WRT_ROOT . ltrim($this->pathInfo["path"], "/") . "/" . $this->assets_path;
			if (!is_dir($this->data["AssetsPath"])) {
				$this->assetsfile = false;
			}
			$this->autoLoad();
		}

		/***************************************************************************/
		/***************************************************************************/
		public function dataHandler() {
			global $_GET;
			global $_POST;
			$this->data["getData"] = $this->RequestDataHandler($_GET);
			$this->data["postData"] = $this->RequestDataHandler($_POST);
			unset($_GET);
			unset($_POST);
		}
		
		public function RequestDataHandler($data) {
			$reqvArr = array( ">" => "&gt;", "<" => "&lt;", "'" => "&#39;", '"' => "&#34;" );
			if (is_array($data)) {
				if (count($data) > 0) {
					foreach ($data as $kk => $val) {
						if ($kk !== "csvData") {
							$data[$kk] = $this->RequestDataHandler($val);
						} else {
							if (get_magic_quotes_gpc()) {
								$val = stripslashes($val);
							}
							$data[$kk] = $val;
						}
					}
				}
			} else {
				if (get_magic_quotes_gpc()) {
					$data = stripslashes($data);
				}
				$data = str_replace(array_keys($reqvArr), array_values($reqvArr), $data);				
			}
			return $data;
		}
		
		public function setLangArr($lang) {
			$this->data["lang"] = $lang;
		}
		
		function check_is_ajax() {
			$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
			if(isset($this->data["postData"]["tp"]) && isset($this->data["postData"]["k"]) && $this->data["postData"]["tp"] == "ajax" && !$isAjax && $this->data["ajaxKey"] == $this->data["postData"]["k"]) {
				echo 'Access denied';
				exit;
			}
		}
		
		public function routing() {
			$this->check_is_ajax();
			$this->data["curr_task"] = $this->defaultTask;
			if (isset($this->data["getData"]["task"])) {
				$this->data["curr_task"] = $this->data["getData"]["task"];
			} else if (isset($this->data["postData"]["task"])) {
				$this->data["curr_task"] = $this->data["postData"]["task"];
			}
			$this->data["curr_task"] = strtolower($this->data["curr_task"]);
			$exts = explode(".", $this->data["curr_task"]);
			if(isset($exts[0])) {
				$this->classOption = trim($exts[0]);
				$this->className = str_replace("-", "_", strtolower($exts[0]));
			}
			if ($this->userInfo["currMode"] == "sys") {
				if (empty($this->className)) $this->className = $this->configs["sys_section"][0];
				$this->methodName = isset($exts[1]) ? strtolower($exts[1]) : "index";
			} else {
				if (empty($this->className)) $this->className = $this->configs["default_section"][0];
				$this->methodName = isset($exts[1]) ? strtolower($exts[1]) : "index";
			}
			
			$this->data["controllerScript"] = $this->ControllerPath . '/'. $this->className .'.php';
			
		}

		// Function to get the client IP address
		public function get_client_ip() {
			$ipaddress = '';
			if (isset($_SERVER['HTTP_CLIENT_IP']))
				$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
			else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_X_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
			else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
			else if(isset($_SERVER['HTTP_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_FORWARDED'];
			else if(isset($_SERVER['REMOTE_ADDR']))
				$ipaddress = $_SERVER['REMOTE_ADDR'];
			else
				$ipaddress = 'UNKNOWN';
			return $ipaddress;
		}

		public function errorPageHandle() {
			$this->data["controllerScript"] = $this->ControllerPath . '/404.php';
			$this->data["viewScript"] = "";
		}

		public function userValid($userInfo) {
			if (isset($_SESSION["TPSession"]["lastAction"]) && ($_SESSION["TPSession"]["lastAction"] < (date("YmdHis") - 10000))) {
				// if (isset($_SESSION["TPSession"]["lastAction"]) && ($_SESSION["TPSession"]["lastAction"] < (date("YmdHis") - 1))) {
				// unset($_SESSION["TPSession"]);
			}
			$this->eClassSession = $_SESSION;
			$allowTeacherPortfolio = true;
			if (!$this->eClassSession["SSV_PRIVILEGE"]["plugin"]["TeacherPortfolio"]) {
				$allowTeacherPortfolio = false;
			}
			$tp_UserID = 0;
			if (isset($this->eClassSession["TPSession"]["info"]["UserID"])) {
				$tp_UserID = $this->eClassSession["TPSession"]["info"]["UserID"];
				$tp_selectedUser = $this->eClassSession["TPSession"]["info"]["SelectedUserID"];
			} else {
				if (isset($this->eClassSession["UserID"])) {
					$tp_UserID = $this->eClassSession["UserID"];
					$tp_selectedUser = $this->eClassSession["UserID"];
				}
			}
			$isSystemAdmin = "N";
			$isModuleAdmin = "N";
			if ($tp_UserID == 0) {
				$allowTeacherPortfolio = false;
			}
			$allow_mode = array();
			if (isset($this->eClassSession["TPSession"]["currMode"])) {
				$tp_userMode = $this->eClassSession["TPSession"]["currMode"];
				$allow_mode = $this->eClassSession["TPSession"]["allowMode"];
				$isSystemAdmin = $this->eClassSession["TPSession"]["info"]["isSystemAdmin"];
				$isModuleAdmin = $this->eClassSession["TPSession"]["info"]["isModuleAdmin"];
			} else {
				if ($this->eClassSession["SSV_USER_ACCESS"]["eAdmin-TeacherPortfolio"] || $this->eClassSession["SSV_USER_ACCESS"]["other-TeacherPortfolio"]) {
					$tp_userMode = "sys";
					$allow_mode = array( "sys" );
					$isSystemAdmin = "Y";
					$isModuleAdmin = "Y";
				} else {
					if ($this->eClassSession["SSV_PRIVILEGE"]["TeacherPortfolio"]["isViewerUser"]) {
						$tp_userMode = "reader";
						$allow_mode = array( "reader" );
						if ($this->eClassSession["UserType"] != 1 || $this->eClassSession["isTeaching"] != 1) {
							$tp_selectedUser = 0;
						}
						$isSystemAdmin = "N";
						$isModuleAdmin = "N";
					} else {
						$tp_userMode = "owner";
						$allow_mode = array( "owner" );
						$isSystemAdmin = "N";
						$isModuleAdmin = "N";
					}
				}
			}
			
			if (count($allow_mode) == 0) {
				$allowTeacherPortfolio = false;
			}
			
			if ($allowTeacherPortfolio) {
				global $sys_custom;
				if($sys_custom['TeacherPortfolio']["displayCPDOnly"]){
					$allowSection = array( "manage", "settings", "cpd");
				}else{
					$allowSection = array( "manage", "settings", "profile", "teaching_record", "activities", "attendance", "cpd", "performance", "cert", "others" );
				}
				$tp_userInfo = array(
					"language" => $userInfo["language"],
					"currMode" => $tp_userMode,
					// "allowSection" => array( "manage", "profile", "teach-record", "activities", "attendance", "develop", "performance", "cert", "others" ),
					"allowSection" => $allowSection,
					"allowMode" => $allow_mode,
					"info" => array(
								"UserID" => $tp_UserID,
								"EnglishName" => $this->eClassSession["EnglishName"],
								"ChineseName" => $this->eClassSession["ChineseName"],
								"SSV_LOGIN_EMAIL" => $this->eClassSession["SSV_LOGIN_EMAIL"],
								"UserType" => $this->eClassSession["UserType"],
								"isTeaching" => $this->eClassSession["isTeaching"],
								"SSV_PRIVILEGE" => $this->eClassSession["SSV_PRIVILEGE"]["plugin"]["TeacherPortfolio"],
								"SelectedUserID" => $tp_selectedUser,
								"isSystemAdmin" => $isSystemAdmin,
								"isModuleAdmin" => $isModuleAdmin
							),
					"lastAction" => date("YmdHis")
				);
				$this->userInfo = $tp_userInfo;
				$this->userInfo["tpConfigs"] = $this->getTPCustomConfigs();
				$_SESSION["TPSession"] = $this->userInfo;
			} else {
				No_Access_Right_Pop_Up();
				exit;
			}
			/*echo "<pre>";
			print_r($this->eClassSession["SSV_USER_ACCESS"]);
			print_r($_SESSION["TPSession"]);
			exit;*/
		}
		
		public function run($forceTeacherMode = false) {
// if ($forceTeacherMode) die("----");
			$this->dataHandler();
			$this->templateSetup();
			$this->routing();
			if ($this->checkAccessRight($forceTeacherMode) === NULL) {
				No_Access_Right_Pop_Up();
				exit;
			}
			if (is_dir($this->data["controllerScript"]) || !file_exists($this->data["controllerScript"])) {
				$this->errorPageHandle();
			}
			if (!empty($this->data["controllerScript"])) {
				require($this->data["controllerScript"]);
				if (class_exists(ucfirst($this->className))) {
					$this->currObj = new $this->className($this);
					if (!empty($this->methodName) && $this->methodName != "list") {
						if (method_exists($this->currObj, $this->methodName)) call_user_func(array($this->currObj, $this->methodName));
						else echo "Page not found";
					} else {
						if (method_exists($this->currObj, "index")) call_user_func(array($this->currObj, "index"));
					}
				}
			}
			exit;
		}
		
		public function _GetMaxAllowedUploadSize(){
			$Sizes = array();
			$Sizes[] = ini_get('upload_max_filesize');
			$Sizes[] = ini_get('post_max_size');
			$Sizes[] = ini_get('memory_limit');
			for($x=0;$x<count($Sizes);$x++){
				$Last = strtolower($Sizes[$x][strlen($Sizes[$x])-1]);
				if($Last == 'k'){
					$Sizes[$x] *= 1024;
				} elseif($Last == 'm'){
					$Sizes[$x] *= 1024;
					$Sizes[$x] *= 1024;
				} elseif($Last == 'g'){
					$Sizes[$x] *= 1024;
					$Sizes[$x] *= 1024;
					$Sizes[$x] *= 1024;
				} elseif($Last == 't'){
					$Sizes[$x] *= 1024;
					$Sizes[$x] *= 1024;
					$Sizes[$x] *= 1024;
					$Sizes[$x] *= 1024;
				}
			}
			return min($Sizes);
		}
		
		public function _Byte2Size($bytes,$RoundLength=1) {
			$kb = 1024;         // Kilobyte
			$mb = 1024 * $kb;   // Megabyte
			$gb = 1024 * $mb;   // Gigabyte
			$tb = 1024 * $gb;   // Terabyte

			if($bytes < $kb) {
				if(!$bytes){
					$bytes = '0';
				}
				return (($bytes + 1)-1).'b';
			} else if($bytes < $mb) {
				return round($bytes/$kb,$RoundLength).'kb';
			} else if($bytes < $gb) {
				return round($bytes/$mb,$RoundLength).'mb';
			} else if($bytes < $tb) {
				return round($bytes/$gb,$RoundLength).'gb';
			} else {
				return round($bytes/$tb,$RoundLength).'tb';
			}
		}
		
		public function getMemoryInfo($html_output = true) {
			global $time_start;
			$time_end = microtime(true);
			$execution_time = ceil((($time_end - $time_start)) * 10000) / 10000;
			$num_query = 0;
			if ($html_output) {
				$debug = '<div id="devobj" style="display:none; position:absolute; bottom:0px; right:0px; z-index:9999; background-color:#000;">';
				$debug .= '<div style="color:#fff; padding:5px; font-family:Arial; font-size:12px;" clsas="msgbox">Page generated in <span style="color:#f00">'.$execution_time.'</span> seconds.';
				$debug .= 'Total memory used in this page: <span style="color:yellow">' . strtoupper($this->getMemoryUsage()) . '</span>. QUERIES : <span style="color:red">' . $this->debugInfo["db_query_count"] . '</span></div>';
				$debug .= '</div>';
			} else {
				$debug = array(
					"second" => $execution_time,
					"memory" => strtoupper($this->getMemoryUsage()),
					"numOfQuery" => $this->debugInfo["db_query_count"]
				);
			}
			return $debug;
		}
		public function getMemoryUsage() {
			$size = memory_get_usage();
			$unit = array('b','kb','mb','gb','tb','pb');
			return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
		}
			
		public function load_model($fileName, $custom_name = "") {
			$this->data["modelScript"] = $this->modelPath . '/' . strtolower($fileName).'.model.php';
			if (is_dir($this->data["modelScript"]) || !file_exists($this->data["modelScript"])) $this->data["modelScript"] = "";
			if (!empty($this->data["modelScript"])) include($this->data["modelScript"]);
			
			$class_name = $fileName;
			$model_name = $fileName;
			if (!empty($custom_name)) $model_name = $custom_name;
			
			$this->$model_name = new $class_name;
			if (method_exists($this->$model_name,'setCore')) {
				$this->$model_name->setCore($this);
			}
		}
		
		public function load_helper($fileName) {
			$this->data["helperScript"] = $this->helperPath . '/' . strtolower($fileName).'.helper.php';
			if (is_dir($this->data["helperScript"]) || !file_exists($this->data["helperScript"])) $this->data["helperScript"] = "";
			if (!empty($this->data["helperScript"])) include($this->data["helperScript"]);
		}
		
		public function load_thirdparty($fileName) {
			$thirdPartyScript = $this->thirdpartyPath . '/' . strtolower($fileName);
			if (is_dir($thirdPartyScript) || !file_exists($thirdPartyScript)) $thirdPartyScript = "";
			if (!empty($thirdPartyScript)) include($thirdPartyScript);
		}
		
		public function addQuerySQL($strSQL) {
			$this->debugInfo["db_query_count"]++;
			$this->debugInfo["db_query_arr"][] = $strSQL;
		}
		
		public function loadAssets($file, $owner = "SELF") {
			switch (strtoupper($owner)) {
				case "ROOT":
					return $this->urlInfo["PATH_WRT_ROOT"];
					break;
				default:
					return $this->data["AssetsPath"] . "/" . $file;
					break;
			}
		}
		
		public function checkAccessRight($forceTeacherMode = false) {
			global $sys_custom;
			/****** get from session *******/
			$this->eClassSession = $_SESSION;
			
			if ($forceTeacherMode) {
				
				$this->userInfo["Role"]["isAdminMode"] = false;
				$this->userInfo["Role"]["isViewerMode"] = false;
				$this->userInfo["Role"]["isOwnerMode"] = true;
	
				$this->userInfo["Role"]["hasAdminRight"] = false;
				$this->userInfo["Role"]["hasViewerRight"] = false;
				$this->userInfo["Role"]["hasOwnerRight"] = true;
				
				 
				$this->userInfo["currMode"] = "owner";
				$this->userInfo["allowMode"] = array( "owner" );
				$this->userInfo["info"]["isSystemAdmin"] = "N";
				$this->userInfo["info"]["isModuleAdmin"] = "N";
				$this->userInfo["info"]["SelectedUserID"] = $this->userInfo["info"]["UserID"];
			} else {
				/****************************************************************************/
				/* check permission from eClass System
				/****************************************************************************/
				$this->userInfo["Role"]["isAdminMode"] = false;
				$this->userInfo["Role"]["isViewerMode"] = false;
				$this->userInfo["Role"]["isOwnerMode"] = false;
	
				$this->userInfo["Role"]["hasAdminRight"] = false;
				$this->userInfo["Role"]["hasViewerRight"] = false;
				$this->userInfo["Role"]["hasOwnerRight"] = false;
	
				if ($this->isAdmin()) $this->userInfo["Role"]["isAdminMode"] = true;
				if ($this->isViewer()) $this->userInfo["Role"]["isViewerMode"] = true;
				if ($this->isOwner()) $this->userInfo["Role"]["isOwnerMode"] = true;
	
				if ($this->hasAdminRight()) $this->userInfo["Role"]["hasAdminRight"] = true;
				if ($this->hasViewerRight()) $this->userInfo["Role"]["hasViewerRight"] = true;
				if ($this->hasOwnerRight()) $this->userInfo["Role"]["hasOwnerRight"] = true;
				/****************************************************************************/
				/* check permission from eClass System
				/****************************************************************************/
			}
			
			/************************************************************************/
			/* Teacher Portfolio - System Configs
			/************************************************************************/
			$this->userInfo["customization"] = $sys_custom['TeacherPortfolio'];
			/************************************************************************/
			
			$grantSection = array();
			if (count($this->configs["sys_section"]) > 0 && count($this->configs["default_section"]) > 0 && count($this->userInfo["allowSection"]) > 0) {
				if ($this->isAdmin()) {
					$grantSection = array_intersect($this->configs["sys_section"], $this->userInfo["allowSection"]);
				} else {
					$grantSection = array_intersect($this->configs["default_section"], $this->userInfo["allowSection"]);
				}
			} else {
				// No_Access_Right_Pop_Up();
			}

			if (count($grantSection) > 0) {
				$this->userInfo["grantSection"] = $grantSection;
				$this->data["userInfo"] = $this->userInfo;
				if ($this->data["userInfo"]["Role"]["hasAdminRight"] || $this->data["userInfo"]["Role"]["hasViewerRight"] || $this->data["userInfo"]["Role"]["hasOwnerRight"]) {
				} else {
					return NULL;
				}
				unset($grantSection);
			} else {
				return NULL;
			}
			if ($this->classOption != "welcome" && $this->classOption != "nav") {
				if (!in_array($this->classOption, $this->userInfo["grantSection"])) {
					return NULL;
				}
			}
			return TRUE;
			/****** get from session *******/
		}
		
		public function isAdmin() {
			if (isset($this->userInfo["currMode"]) && isset($this->userInfo["info"]["isSystemAdmin"])) {
				if (($this->userInfo["currMode"] == "sys") && ($this->userInfo["info"]["isSystemAdmin"] == "Y" || $this->userInfo["info"]["isModuleAdmin"] == "Y")) {
					return true;
				}
			}
			return false;
		}
		
		public function isViewer() {
			if (isset($this->userInfo["currMode"]) && $this->userInfo["currMode"] == "reader") {
				return true;
			}
			return false;
		}
		
		public function isOwner() {
			if (isset($this->userInfo["currMode"]) && $this->userInfo["currMode"] == "owner") {
				return true;
			}
			return false;
		}
		
		public function hasAdminRight() {
			if (in_array("sys", $this->userInfo["allowMode"])) {
				return true;
			}
			return false;
		}
		
		public function hasViewerRight() {
			if (in_array("reader", $this->userInfo["allowMode"])) {
				return true;
			}
			return false;
		}
		
		public function hasOwnerRight() {
			if (in_array("owner", $this->userInfo["allowMode"])) {
				return true;
			}
			return false;
		}

		private function getTPCustomConfigs() {
			$this->load_model("CustomConfigs");
			$this->CustomConfigs->setCore($this);
			return $this->CustomConfigs->getConfigs(); 
		}
		
		public function saveConfig($postData) {
			$this->load_model("CustomConfigs");
			$this->CustomConfigs->setCore($this);
			return $this->CustomConfigs->saveConfig($postData);
		}
	
	}
}
?>