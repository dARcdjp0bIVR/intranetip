<?php

// Modifing by : yat

##### Change Log [Start] #####
#	Date 	:	2013-09-26 (Roy)
#				improved archiveIntranetUser(), add more columns which follows INTRANET_USER
#	Date	:	2011-09-27 (YatWoon)
#				modified archiveIntranetUser(), archiveStudentsInfo(), add $YearOfLeft parameter
#
#	Date	:	2011-04-26 (Henry Chow)
#				modified archiveIntranetUser(), store "DateArchive" & "ArchivedBy" when archive the user account
#
###### Change Log [End] ######

if (!defined("LIBSTUDENTPROMOTION_DEFINED"))                     // Preprocessor directive
{
define("LIBSTUDENTPROMOTION_DEFINED", true);

class libstudentpromotion extends libclass {

        # ++++++ for finalize the promotion process ++++++#
        var $AcademicYear;
        var $unsetStudentIDList;
        var $archivalList;

        function libstudentpromotion ()
        {
                $this->libclass();
        }
        function getClassStatus ()
        {
                 $sql = "SELECT NewClass, COUNT(UserID) FROM INTRANET_PROMOTION_STATUS GROUP BY NewClass";
                 return $this->returnArray($sql,2);
        }
        function getAssignedStatus()
        {
                 $sql = "SELECT a.ClassName, COUNT(b.UserID)
                         FROM INTRANET_USER as a LEFT JOIN INTRANET_PROMOTION_STATUS as b
                              ON b.UserID = a.UserID
                         WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                         GROUP BY a.ClassName";
                 return $this->returnArray($sql,2);
        }
        function returnNumOfStudentsByClass()
        {
                 $sql = "SELECT ClassName, COUNT(UserID) FROM INTRANET_USER
                                WHERE RecordType = 2 AND RecordStatus IN (0,1,2)
                                GROUP BY ClassName";
                 return $this->returnArray($sql,2);
        }
        function returnNumOfStudents()
        {
                 $sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2)";
                 $temp = $this->returnVector($sql);
                 return $temp[0]+0;
        }
        function getStudentStatusByClassName($ClassName)
        {
                 $sql = "SELECT a.UserID,a.ChineseName, a.EnglishName, a.ClassNumber, b.NewClass, b.NewClassNumber
                         FROM INTRANET_USER as a LEFT OUTER JOIN INTRANET_PROMOTION_STATUS as b
                              ON a.UserID = b.UserID
                         WHERE a.ClassName = '$ClassName' AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                         ORDER BY a.ClassNumber";
                 return $this->returnArray($sql,6);
        }
        function getNumOfLeftStudents()
        {
                 $sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 3";
                 $temp = $this->returnVector($sql);
                 return $temp[0]+0;
        }

        function reset()
        {
                 $sql = "DELETE FROM INTRANET_PROMOTION_STATUS";
                 $this->db_db_query($sql);
        }
        function returnNumOfAssigned()
        {
                 $sql = "SELECT COUNT(*) FROM INTRANET_PROMOTION_STATUS";
                 $temp = $this->returnVector($sql);
                 return $temp[0]+0;
        }
        function returnUnsetStudentList()
        {
                 $sql = "SELECT a.UserID, a.UserLogin, a.EnglishName, a.ChineseName, a.ClassName, a.ClassNumber
                         FROM INTRANET_USER as a LEFT OUTER JOIN INTRANET_PROMOTION_STATUS as b ON a.UserID = b.UserID
                         WHERE b.UserID IS NULL AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                         ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
                 return $this->returnArray($sql,6);
        }
        function returnUnsetStudentIDList()
        {
                 $sql = "SELECT a.UserID
                         FROM INTRANET_USER as a LEFT OUTER JOIN INTRANET_PROMOTION_STATUS as b ON a.UserID = b.UserID
                         WHERE b.UserID IS NULL AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)";
                 return $this->returnVector($sql);
        }
        function getLeftStudentIDs()
        {
                 $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 3";
                 return $this->returnVector($sql);
        }
        function setArchivalList($id_array)
        {
                 if (sizeof($id_array)==0) return;
                 $this->archivalList = implode(",",$id_array);
        }


        # effecive.php - step 1
        # archive_left.php - step 1
        function setAcademicYear($AcademicYear)
        {
                 $this->AcademicYear = $AcademicYear;
        }

        # effective.php
        # step 2. Update This Year to Class History of ALL students(insert data to PROFILE_CLASS_HISTORY)
        function updateClassHistory()
        {
                 $sql = "INSERT INTO PROFILE_CLASS_HISTORY
                         (UserID, ClassName, ClassNumber, AcademicYear, DateInput, DateModified)
                         SELECT UserID, ClassName, ClassNumber, '". $this->AcademicYear ."', NOW(), NOW() FROM INTRANET_USER
                         WHERE RecordType = 2 AND RecordStatus IN (0,1,2)
                         ";
                 $this->db_db_query($sql);
        }
        # Step 3. Update New Class Name Class Number (update INTRANET_USER)
        function updateStudentInfoPromotion()
        {
                 $sql = "SELECT a.UserID, c.ClassName, a.NewClassNumber " .
                         "FROM INTRANET_PROMOTION_STATUS AS a
                               LEFT OUTER JOIN INTRANET_USER AS b ON a.UserID = b.UserID
                               LEFT OUTER JOIN INTRANET_CLASS AS c ON a.NewClass= c.ClassID" ;

                 # [0] -> UserID  |  [1] -> NewClassName |  [2] ->  NewClassNumber
                 $newStuInfo = $this->returnArray($sql, 3);

                 for ( $i=0; $i<sizeOf($newStuInfo); $i++)
                 {
                       list ($StudentID, $newClass, $newClassNum) = $newStuInfo[$i];
                       $sql = "UPDATE INTRANET_USER SET DateModified=NOW(), ClassName='$newClass',  ClassNumber= '$newClassNum'
                               WHERE UserID = $StudentID";
                       $this->db_db_query($sql);
                 }
        }

        # Step 4. Archive Class Groups (Rename group, e.g. 1A -> 1A-2004 , 2004 is Current Year, not academic year)
        function archiveGroup()
        {
                 $RecordType = 3;       # Class Group ONLY
                 $year = "-".date('Y');
                 $update_fields = "Title=CONCAT(Title,'$year'), Description=CONCAT(Description,'$year')";
                 $sql = "UPDATE INTRANET_GROUP SET $update_fields, DateModified=NOW() WHERE RecordType=" . $RecordType;
                 $this->db_db_query($sql);
        }

        # Step 5. Create new groups (Refer to class table)
        function createNewGroups()
        {
                 #default storeage quota
                 $StorageQuota = 5;
                 #class recordtype
                 $RecordType = 3;

                 $sql = "INSERT INTO INTRANET_GROUP
                         (Title, RecordType, StorageQuota,  DateInput, DateModified )
                         SELECT ClassName, $RecordType, $StorageQuota, NOW(), NOW()
                         FROM INTRANET_CLASS";

                 $this->db_db_query($sql);
        }

        # Step 6. Build Class-Group relation (execute lclass->fixClassGroupRelation()  )
        # Inherited from libclass

        # Step 7. Add students to new class group (insert user to INTRANET_USERGROUP)
        function addStuToNewClassGroup()
        {

                 $sql = "INSERT IGNORE INTO INTRANET_USERGROUP
                         (GroupID, UserID, RoleID, DateInput, DateModified )
                         SELECT
                               c.GroupID , a.UserID,  NULL, NOW(), NOW()
                                FROM INTRANET_PROMOTION_STATUS AS a LEFT OUTER JOIN INTRANET_CLASS AS c ON a.NewClass = c.ClassID
                         ";
                 $this->db_db_query($sql);

                 # Update to default Role
                 $this->UpdateRole_UserGroup();
        }

        # Step 8. Set unset students status to left (update INTRANET_USER set RecordStatus = 3)
        function setUnsetStudentToStatusLeft()
        {
                 # Grab unset students
                 $students = $this->returnUnsetStudentIDList();
                 if (sizeof($students)==0) return;

                 $RecordStatus = 3;
                 
                 $yearOfLeft = date('Y');
                 
                 //$MonthOfLeft = date('m');
                 // Modified by key [2008-12-11]: If the day is after 1st Sept, change the year of left to next year
                 //if($MonthOfLeft >= 9)
				 //$yearOfLeft += 1;
                 
                 $list = implode(",",$students);

                 $sql = "UPDATE INTRANET_USER SET RecordStatus='$RecordStatus', DateModified=NOW(), YearOfLeft='$yearOfLeft' WHERE UserID IN ($list)";
                 $this->db_db_query($sql);
        }

        # Add current Class and Class Number to new Class History Record
        # Param : $list - List of UserIDs /w comma separated
        function addClassHistory($list)
        {
                 $academicYear = getCurrentAcademicYear();
                 $sql = "INSERT IGNORE INTO PROFILE_CLASS_HISTORY
                                (UserID, ClassName, ClassNumber, AcademicYear, DateInput, DateModified)
                         SELECT UserID, ClassName, ClassNumber,'$academicYear',now(),now()
                                FROM INTRANET_USER
                                WHERE RecordType = 2 AND RecordStatus IN (0,1,2) AND UserID IN ($list)
                                AND (ClassName != '' OR ClassNumber != '')
                         ";
                 $this->db_db_query($sql);
        }

        #########################################################
        # archive_left.php

        # Archive Information for student
        # Param : $list (List of UserID for SQL stmt)
        function archiveStudentsInfo($list, $YearOfLeft="")
        {
                 if ($list=="") return "";
                 $this->archiveClassHistory($list);
                 $this->archiveStudentProfile($list);
                 $this->archiveIntranetUser($list, $YearOfLeft);
        }

        # step 1 > Set academic Year
        # step 2. Archive Class History in $list (insert data to PROFILE_ARCHIVE_CLASS_HISTORY and remove from PROFILE_CLASS_HISTORY)
        # Param : $list - comma separated UserIDs
        function archiveClassHistory($list)
        {
                 $sql = "INSERT IGNORE INTO PROFILE_ARCHIVE_CLASS_HISTORY
                                (UserID, EnglishName, ChineseName, ClassName, ClassNumber, AcademicYear, DateInput, DateModified)
                         SELECT a.UserID, b.EnglishName, b.ChineseName, a.ClassName, a.ClassNumber, a.AcademicYear, a.DateInput, a.DateModified
                                FROM PROFILE_CLASS_HISTORY as a LEFT OUTER JOIN INTRANET_USER as b On a.UserID = b.UserID
                                WHERE a.UserID IN ($list)
                         ";
                 $this->db_db_query($sql);

                 $sql = "DELETE FROM PROFILE_CLASS_HISTORY WHERE UserID IN ($list)";
                 $this->db_db_query($sql);
        }

        #step 3. Archive Student profile information in $list
        function archiveStudentProfile($list)
        {
                 $this->archiveStudentAttendance($list);
                 $this->archiveStudentMerit($list);
                 $this->archiveStudentService($list);
                 $this->archiveStudentActivity($list);
                 $this->archiveStudentAward($list);
                 $this->archiveStudentAssessment($list);
                 $this->archiveStudentFiles($list);
        }
        # step 3a
        function archiveStudentAttendance($list)
        {
                 # insert record to PROFILE_ARCHIVE_ATTENDANCE in $list
                 $sql = "INSERT INTO PROFILE_ARCHIVE_ATTENDANCE
                                (ClassName, ClassNumber, RecordDate, Year, Semester, Period, DayType,Reason,
                                RecordType, Remark, UserID, EnglishName, ChineseName,  DateInput, DateModified)
                         SELECT IF(a.ClassName IS NULL OR a.ClassName = '',b.ClassName,a.ClassName),
                                IF(a.ClassNumber IS NULL OR a.ClassNumber = '',b.ClassNumber,a.ClassNumber),
                                a.AttendanceDate,a.Year,a.Semester,a.Periods,
                                CASE a.DayType
                                     WHEN '1' THEN 'WD'
                                     WHEN '2' THEN 'AM'
                                     WHEN '3' THEN 'PM' END,
                                a.Reason,
                                CASE a.RecordType
                                     WHEN '1' THEN 'ABSENT'
                                     WHEN '2' THEN 'LATE'
                                     WHEN '3' THEN 'EARLY LEAVE' END,
                                     a.Remark, a.UserID, b.EnglishName, b.ChineseName,  a.DateInput, a.DateModified
                         FROM PROFILE_STUDENT_ATTENDANCE as a
                              LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         WHERE a.UserID IN ($list)
                         ";
                 $this->db_db_query($sql);

                 # Remove from active database
                 $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE UserID IN ($list)";
                 $this->db_db_query($sql);
        }

        #step 3b
        function archiveStudentMerit($list)
        {
                 $sql = "INSERT INTO PROFILE_ARCHIVE_MERIT
                                (ClassName,ClassNumber,RecordDate,Year,Semester,NumberOfUnit,Reason,MeritType,Remark
                                ,UserID, EnglishName, ChineseName,  DateInput, DateModified
                                )
                         SELECT IF(a.ClassName IS NULL OR a.ClassName = '',b.ClassName,a.ClassName),
                                IF(a.ClassNumber IS NULL OR a.ClassNumber = '',b.ClassNumber,a.ClassNumber),
                                a.MeritDate,a.Year,a.Semester,a.NumberOfUnit,a.Reason,
                                CASE a.RecordType
                                     WHEN '1' THEN 'Merit'
                                     WHEN '2' THEN 'Minor Credit'
                                     WHEN '3' THEN 'Major Credit'
                                     WHEN '4' THEN 'Super Credit'
                                     WHEN '5' THEN 'Ultra Credit'
                                     WHEN '-1' THEN 'Black Mark'
                                     WHEN '-2' THEN 'Minor Demerit'
                                     WHEN '-3' THEN 'Major Demerit'
                                     WHEN '-4' THEN 'Super Demerit'
                                     WHEN '-5' THEN 'Ultra Demerit' END,
                                a.Remark, a.UserID, b.EnglishName, b.ChineseName,  a.DateInput, a.DateModified
                         FROM PROFILE_STUDENT_MERIT as a
                              LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                              WHERE a.UserID IN ($list)
                         ";
                 $this->db_db_query($sql);

                 $sql = "DELETE FROM PROFILE_STUDENT_MERIT WHERE UserID IN ($list)";
                 $this->db_db_query($sql);
        }

        #step 3c
        function archiveStudentService($list)
        {
                 $sql = "INSERT INTO PROFILE_ARCHIVE_SERVICE
                                (ClassName, ClassNumber, RecordDate, Year, Semester, ServiceName, Role,Performance,Remark
                                ,UserID, EnglishName, ChineseName,  DateInput, DateModified)
                         SELECT IF(a.ClassName IS NULL OR a.ClassName = '',b.ClassName,a.ClassName),
                                IF(a.ClassNumber IS NULL OR a.ClassNumber = '',b.ClassNumber,a.ClassNumber),
                                a.ServiceDate,a.Year,a.Semester,a.ServiceName, a.Role,a.Performance,a.Remark
                                ,a.UserID,b.EnglishName,b.ChineseName, a.DateInput, a.DateModified
                         FROM PROFILE_STUDENT_SERVICE as a
                              LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         WHERE a.UserID IN ($list)
                        ";
                 $this->db_db_query($sql);

                 $sql = "DELETE FROM PROFILE_STUDENT_SERVICE WHERE UserID IN ($list)";
                 $this->db_db_query($sql);
        }

        #step 3d
        function archiveStudentActivity($list)
        {
                 $sql = "INSERT INTO PROFILE_ARCHIVE_ACTIVITY
                                (ClassName,ClassNumber,Year,Semester,ActivityName,Role,Performance,Remark
                                ,UserID,EnglishName,ChineseName, DateInput, DateModified)
                         SELECT IF(a.ClassName IS NULL OR a.ClassName = '',b.ClassName,a.ClassName),
                                IF(a.ClassNumber IS NULL OR a.ClassNumber = '',b.ClassNumber,a.ClassNumber),
                                a.Year,a.Semester,a.ActivityName,a.Role,a.Performance,a.Remark
                                ,a.UserID,b.EnglishName,b.ChineseName, a.DateInput, a.DateModified
                         FROM PROFILE_STUDENT_ACTIVITY as a
                              LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         WHERE a.UserID IN ($list)
                         ";
                 $this->db_db_query($sql);

                 $sql = "DELETE FROM PROFILE_STUDENT_ACTIVITY WHERE UserID IN ($list)";
                 $this->db_db_query($sql);
        }

        #step 3e
        function archiveStudentAward($list)
        {
                 $sql = "INSERT INTO PROFILE_ARCHIVE_AWARD
                                (ClassName, ClassNumber, RecordDate, Year, Semester, AwardName, Remark
                                ,UserID,EnglishName,ChineseName, DateInput, DateModified)
                         SELECT IF(a.ClassName IS NULL OR a.ClassName = '',b.ClassName,a.ClassName),
                                IF(a.ClassNumber IS NULL OR a.ClassNumber = '',b.ClassNumber,a.ClassNumber),
                                a.AwardDate, a.Year,a.Semester,a.AwardName,a.Remark
                                ,a.UserID,b.EnglishName,b.ChineseName, a.DateInput, a.DateModified
                         FROM PROFILE_STUDENT_AWARD as a
                              LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         WHERE a.UserID IN ($list)
                         ";
                 $this->db_db_query($sql);

                 $sql = "DELETE FROM PROFILE_STUDENT_AWARD WHERE UserID IN ($list)";
                 $this->db_db_query($sql);
        }

        #step 3f
        # libform.php required
        function archiveStudentAssessment($list)
        {
                 # Grab form details
                 $name_field = getNameFieldForRecord("b.");
                 $name_field2 = getNameFieldForRecord("d.");
                 $sql = "SELECT $name_field,
                                IF(a.ClassName IS NULL OR a.ClassName = '',b.ClassName,a.ClassName),
                                IF(a.ClassNumber IS NULL OR a.ClassNumber = '',b.ClassNumber,a.ClassNumber),
                                c.QueString,a.AnsString,a.Year,a.Semester,DATE_FORMAT(a.AssessmentDate,'%Y-%m-%d'),
                                IF(d.UserID IS NULL,a.AssessByName,$name_field2),
                                a.UserID,b.EnglishName,b.ChineseName,d.EnglishName,d.ChineseName, c.FormName
                                , DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i:%s')
                                , DATE_FORMAT(a.DateModified,'%Y-%m-%d %H:%i:%s')
                         FROM PROFILE_STUDENT_ASSESSMENT as a
                              LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                              LEFT OUTER JOIN INTRANET_FORM as c ON a.FormID = c.FormID
                              LEFT OUTER JOIN INTRANET_USER as d ON a.AssessBy = d.UserID
                         WHERE a.UserID IN ($list)";
                 $answers = $this->returnArray($sql,17);
                 $lform = new libform();
                 $values = "";
                 $formsql_delim = "";
                 for ($i=0; $i<sizeof($answers); $i++)
                 {
                      list($name,$classname,$classnumber,$question,$answer,$year,$sem,$assessdate,$assessBy
                      ,$studentid,$st_eng,$st_chi,$assess_eng,$assess_chi, $formname, $dateInput, $dateModified) = $answers[$i];
                      $queArray = $lform->splitQuestion($question);
                      $ansArray = $lform->parseAnswerStr($answer);
                      $formContent = "";
                      for ($j=0; $j<sizeof($queArray); $j++)
                      {
                           $formContent .= $queArray[$j][1]."\n".$ansArray[$j]."\n";
                      }

                      $values .= "$formsql_delim ('$classname','$classnumber','$assessdate','$year'
                                  ,'$sem','$assessBy','$formContent','$studentid','$st_eng','$st_chi'
                                  ,'$assess_eng','$assess_chi', '$formname', '$dateInput', '$dateModified')";
                      $formsql_delim = ",";
                 }
                 if ($values != "")
                 {
                     $sql = "INSERT INTO PROFILE_ARCHIVE_ASSESSMENT
                                    (ClassName,ClassNumber,RecordDate,Year,Semester,AssessorName,FormContent
                                    ,UserID,EnglishName,ChineseName,AssessEngName,AssessChiName, FormName , DateInput, DateModified)
                            VALUES $values";
                     $this->db_db_query($sql);

                     $sql = "DELETE FROM PROFILE_STUDENT_ASSESSMENT WHERE UserID IN ($list)";
                     $this->db_db_query($sql);
                 }
        }

        #step 3g
        function archiveStudentFiles($list)
        {
                 $sql = "INSERT INTO PROFILE_ARCHIVE_FILES
                                (ClassName,ClassNumber,Year,Semester,Title,Description,FileType,FileName,FileSize,Path
                                ,UserID,EnglishName,ChineseName, DateInput, DateModified)
                         SELECT IF(a.ClassName IS NULL OR a.ClassName = '',b.ClassName,a.ClassName),
                                IF(a.ClassNumber IS NULL OR a.ClassNumber = '',b.ClassNumber,a.ClassNumber),
                                a.Year,a.Semester,a.Title,a.Description,a.FileType,a.FileName,a.FileSize,a.Path
                                ,a.UserID,b.EnglishName,b.ChineseName, NOW(), NOW()
                         FROM PROFILE_STUDENT_FILES as a
                              LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         WHERE a.UserID IN ($list)
                         ";

                 $this->db_db_query($sql);

                 $sql = "DELETE FROM PROFILE_STUDENT_FILES WHERE UserID IN ($list)";
                 $this->db_db_query($sql);
        }

        # Step 4 - Archive User info to INTRANET_ARCHIVE_USER
        # Param : $list - comma separated UserIDs
        #Modified by key [2008-12-11]: If the day is after 1st Sept, change the year of left to next year
        function archiveIntranetUser($list, $NewYearOfLeft="")
        {
        	global $UserID;
        	
                 $yearOfLeft = date('Y');    # Grab year of left
                 $MonthOfLeft = date('m');
                 
                 $yearOfLeft = $NewYearOfLeft ? $NewYearOfLeft : $yearOfLeft;
//                  if($MonthOfLeft >= 9)
// 				 $yearOfLeft += 1;

                 // $sql = "INSERT INTO INTRANET_ARCHIVE_USER
                         // (UserID, UserLogin,UserPassword,EnglishName,ChineseName,
                         // NickName,DisplayName,Title,Gender,DateOfBirth,HomeTelNo,
                         // OfficeTelNo,MobileTelNo,FaxNo,ICQNo,Address,URL,
                         // Country,Info,Remark,
                         // ClassName,ClassNumber,CardID,RecordType,YearOfLeft,
                         // WebSAMSRegNo, PhotoLink, HashedPass, DateArchive, ArchivedBy)

                         // SELECT UserID, UserLogin,UserPassword, EnglishName,ChineseName,
                         // NickName,DisplayName,Title,Gender,DateOfBirth,HomeTelNo,
                         // OfficeTelNo,MobileTelNo,FaxNo,ICQNo,Address,URL,
                         // Country,Info,Remark,
                         // ClassName,ClassNumber,CardID,RecordType,$yearOfLeft,
                         // WebSAMSRegNo, PhotoLink, HashedPass, NOW(), '$UserID'
                         // FROM INTRANET_USER WHERE UserID IN ($list)
                         // ";
						 
				$sql = "INSERT INTO INTRANET_ARCHIVE_USER
                         (UserID, UserLogin, UserPassword, HashedPass, EnglishName, ChineseName,
						 NickName, DisplayName, Title, Gender, DateOfBirth, HomeTelNo, OfficeTelNo,
						 MobileTelNo, FaxNo, ICQNo, Address, URL, Country, Info, Remark, PhotoLink,
						 ClassName, ClassNumber, CardID, WebSAMSRegNo, RecordType, YearOfLeft, DateArchive,
						 ArchivedBy, UserEmail, FirstName, LastName, TitleEnglish, TitleChinese, ClassLevel,
						 RFID, Teaching, LastUsed, LastModifiedPwd, SessionKey, SessionLastUpdated,
						 RecordStatus, HKID, STRN, InternalUserID, IDNumber, PlaceOfBirth, IntranetUserDateInput,
						 IntranetUserInputBy, IntranetUserDateModified, IntranetUserModifyBy, ImapUserEmail,
						 LastLang, PersonalPhotoLink, isSystemAdmin, HKJApplNo, StaffCode, Barcode, ApiDateInput,
						 ApiInputBy, ApiDateModified, ApiModifiyBy, CFirstName, CLastName)

                         SELECT UserID, UserLogin, UserPassword, HashedPass, EnglishName, ChineseName,
						 NickName, DisplayName, Title, Gender, DateOfBirth, HomeTelNo, OfficeTelNo,
						 MobileTelNo, FaxNo, ICQNo, Address, URL, Country, Info, Remark, PhotoLink,
						 ClassName, ClassNumber, CardID, WebSAMSRegNo, RecordType, $yearOfLeft, NOW(),
						 '$UserID', UserEmail, FirstName, LastName, TitleEnglish, TitleChinese, ClassLevel,
						 RFID, Teaching, LastUsed, LastModifiedPwd, SessionKey, SessionLastUpdated,
						 RecordStatus, HKID, STRN, InternalUserID, IDNumber, PlaceOfBirth, DateInput,
						 InputBy, DateModified, ModifyBy, ImapUserEmail,
						 LastLang, PersonalPhotoLink, isSystemAdmin, HKJApplNo, StaffCode, Barcode, ApiDateInput,
						 ApiInputBy, ApiDateModified, ApiModifiyBy, CFirstName, CLastName
						 
                         FROM INTRANET_USER WHERE UserID IN ($list)
                         ";
//echo $sql;
                 $this->db_db_query($sql);
        }
                #+++++ end of finalize +++++#

            #
            function removeAllArchiveRecord()
            {
                     $sql = "TRUNCATE INTRANET_ARCHIVE_USER";
                     $this->db_db_query($sql);

                     $sql = "TRUNCATE PROFILE_ARCHIVE_CLASS_HISTORY";
                     $this->db_db_query($sql);

                     $sql = "TRUNCATE PROFILE_ARCHIVE_ATTENDANCE";
                     $this->db_db_query($sql);

                     $sql = "TRUNCATE PROFILE_ARCHIVE_AWARD";
                     $this->db_db_query($sql);

                     $sql = "TRUNCATE PROFILE_ARCHIVE_ASSESSMENT";
                     $this->db_db_query($sql);

                     $sql = "TRUNCATE PROFILE_ARCHIVE_MERIT";
                     $this->db_db_query($sql);

                     $sql = "TRUNCATE PROFILE_ARCHIVE_SERVICE";
                     $this->db_db_query($sql);

                     $sql = "TRUNCATE PROFILE_ARCHIVE_ACTIVITY";
                     $this->db_db_query($sql);

                     # Remove files
                     global $intranet_root;
                     $lf = new libfilesystem();
                     $sql = "SELECT Path,FileName FROM PROFILE_ARCHIVE_FILES";
                     $files = $this->returnArray($sql,2);
                     for ($i=0; $i<sizeof($files); $i++)
                     {
                          list($path,$filename) = $files[$i];
                          $full_path = "$intranet_root/file/studentfiles/$path/$filename";
                          if (is_file($full_path))
                          {
                              $lf->file_remove($full_path);
                          }
                     }
                     $sql = "TRUNCATE PROFILE_ARCHIVE_FILES";
                     $this->db_db_query($sql);

            }

}
}        // End of directive
?>
