<?
// using: 

class libHandinLayout {

//	private $actionBarDisplay;
//	private $studentNameListDisplay;
//	private $teacherCommentDisplay;
	
	private $contentCode;
	private $navigationHtml;
	private $unitTitle;
	private $unitName;
	private $schemeTitle;
	private $schemeName;
	private $actionButtonAry;
	private $studentSelectionHtml;
	private $teacherCommentHtml;
	private $sharingToStudentHtml;
	private $peerCommentHtml;
	private $stepHtml;
	private $contentHtml;
	

	public function __construct() {
		global $w2_cfg,$intranet_db;
		
		$this->actionButtonAry = array();
    }

////START GET SET ////
//	public function setActionBarDisplay($val){
//		$this->actionBarDisplay = $val;
//	}
//	public function getActionBarDisplay(){
//		return $this->actionBarDisplay;
//	}
//
//	public function setStudentNameListDisplay($val){
//		$this->studentNameListDisplay = $val;
//	}
//
//	public function getStudentNameListDisplay($val){
//		return $this->studentNameListDisplay;
//	}
//
//	public function setTeacherCommentDisplay($val){
//		$this->setStudentNameListDisplay = $val;
//	}
//
//	public function getTeacherCommentDisplay($val){
//		return $this->teacherCommentDisplay;
//	}
	
	public function setContentCode($str) {
		$this->contentCode = $str;
	}
	public function getContentCode() {
		return $this->contentCode;
	}
	
	public function setNavigationHtml($str) {
		$this->navigationHtml = $str;
	}
	public function getNavigationHtml() {
		return $this->navigationHtml;
	}
	
	public function setUnitTitle($str) {
		$this->unitTitle = $str;
	}
	public function getUnitTitle() {
		return $this->unitTitle;
	}
	public function setUnitName($str) {
		$this->unitName = $str;
	}
	public function getUnitName() {
		return $this->unitName;
	}
	public function setSchemeTitle($str) {
		$this->schemeTitle = $str;
	}
	public function getSchemeTitle() {
		return $this->schemeTitle;
	}
	public function setSchemeName($str) {
		$this->schemeName = $str;
	}
	public function getSchemeName() {
		return $this->schemeName;
	}
	
	public function setStudentSelectionHtml($str) {
		$this->studentSelectionHtml = $str;
	}
	public function getStudentSelectionHtml() {
		return $this->studentSelectionHtml;
	}
	
	public function setTeacherCommentHtml($str) {
		$this->teacherCommentHtml = $str;
	}
	public function getTeacherCommentHtml() {
		return $this->teacherCommentHtml;
	}
	
	public function setSharingToStudentHtml($str) {
		$this->sharingToStudentHtml = $str;
	}
	public function getSharingToStudentHtml() {
		return $this->sharingToStudentHtml;
	}



	
	public function setPeerCommentHtml($str) {
		$this->peerCommentHtml = $str;
	}
	public function getPeerCommentHtml() {
		return $this->peerCommentHtml;
	}
	
	public function setStepHtml($str) {
		$this->stepHtml = $str;
	}
	public function getStepHtml() {
		return $this->stepHtml;
	}
	
	public function setContentHtml($str){
		$this->contentHtml = $str;
	}
	public function getContentHtml(){
		return $this->contentHtml;
	}

////END GET SET   ////
	
	
	
	private function generateSchemeNameHtml() {
		$html = $this->getSchemeNameDisplayTemplate();
		$html = str_replace('{{{unitTitle}}}', $this->getUnitTitle(), $html);
		$html = str_replace('{{{unitName}}}', $this->getUnitName(), $html);
		$html = str_replace('{{{schemeTitle}}}', $this->getSchemeTitle(), $html);
		$html = str_replace('{{{schemeName}}}', $this->getSchemeName(), $html);
		
		return $html;
	}
	private function getSchemeNameDisplayTemplate() {
		global $Lang, $w2_cfg;
		
		$html = '';
		if ($this->getContentCode() == $w2_cfg["contentArr"]["sci"]["contentCode"]) {
			// no unit display for science
			return <<<html
<div class="themename">{{{schemeTitle}}}: <span>{{{schemeName}}}</span></div>
html;
		}else if($this->getSchemeTitle()==''&&$this->getSchemeName()==''){
			return <<<html
<div class="themename">{{{unitTitle}}}: <span>{{{unitName}}}</span></div>
html;
		}else {
			return <<<html
<div class="themename_main"><span>{{{unitTitle}}}: {{{unitName}}}</span></div>
<div class="themename"><span>{{{schemeTitle}}}: {{{schemeName}}}</span></div>
html;
		}
				
//		return <<<html
//<div class="themename">{{{unitTitle}}}: <span>{{{unitName}}}</span></div>
//<div class="themename" style="background:none">{{{schemeTitle}}}: <span>{{{schemeName}}}</span></div>
//html;
	}


	public function addActionButton($html) {
		$this->actionButtonAry[] = $html;
	}
	private function getActionButtonAry() {
		return $this->actionButtonAry;
	}
	
	
	private function generateToolbarHtml() {
		$actionBtnAry = $this->getActionButtonAry();

		$numOfActionBtn = count((array)$actionBtnAry);
		$studentSelectionHtml = $this->getStudentSelectionHtml();
		
		$html = '';
		if ($numOfActionBtn==0 && $studentSelectionHtml=='') {
			//$html = '<div class="approve_area"><br style="clear:both"></div>';
			$html = '';

		}
		else {
			$html = $this->getToolbarDisplayTemplate();
			
			if ($numOfActionBtn == 0) {
				$actionBtnHtml = '';
			}
			else {
				$actionBtnHtml = '<div class="btn">'.implode(' ', (array)$actionBtnAry).'</div>';
			}
			
			$html = str_replace('{{{buttonHtml}}}', $actionBtnHtml, $html);
			$html = str_replace('{{{studentSelectionHtml}}}', $studentSelectionHtml, $html);
		}
		$html = '<span id="w2_leftPanelArea">'.$html.'</span>';
		return $html;
	}
	private function getToolbarDisplayTemplate() {
		global $Lang;
		
		return <<<html
<div class="approve_area">
    {{{buttonHtml}}}
    {{{studentSelectionHtml}}}
	<br style="clear:both">
</div>
html;
	}
	
	
	private function getPageHtmlTemplate() {
		return <<<html
<div class="main_top">
	{{{navigationHtml}}}
</div>
<p class="spacer"></p>            
{{{themeHtml}}}
{{{toolbarHtml}}}
<div class="icon_right_grp">
	{{{sharingToStudentHtml}}}
	{{{peerCommentHtml}}}
	{{{teacherCommentHtml}}}
	{{{stepHtml}}}
</div>
{{{contentHtml}}}
html;
            
	}
	
	public function display() {
		$html = $this->getPageHtmlTemplate();
			
		$html = str_replace('{{{navigationHtml}}}', $this->getNavigationHtml(), $html);
		$html = str_replace('{{{themeHtml}}}', $this->generateSchemeNameHtml(), $html);
		$html = str_replace('{{{toolbarHtml}}}', $this->generateToolbarHtml(), $html);
		$html = str_replace('{{{teacherCommentHtml}}}', $this->getTeacherCommentHtml(), $html);
		$html = str_replace('{{{sharingToStudentHtml}}}', $this->getSharingToStudentHtml(), $html);
		$html = str_replace('{{{peerCommentHtml}}}', $this->getPeerCommentHtml(), $html);
		$html = str_replace('{{{stepHtml}}}', $this->getStepHtml(), $html);
		$html = str_replace('{{{contentHtml}}}', $this->getContentHtml(), $html);
		
		return $html;
	}
}
?>
