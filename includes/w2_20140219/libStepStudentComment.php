<?
// using: ivan
/*
 * Modification Log:
 * 	20120117 Ivan
 * 	- added CommentStatus logic
 */

class libStepStudentComment {
	private $objDb;
	private $tableName;
	private $primaryKeyName;
	
	private $commentId;
	private $content;
	private $commentStatus;
	private $dateInput;
	private $inputBy;
	private $dateModified;
	private $modifiedBy;
	private $sticker;
	private $version;
	private $userId;
	private $stepId;
	private $commentBank = array("Good use of tenses.","Good choice of words.","Good job!","Well done.","Well organized.");
		

	public function __construct($commentId = NULL) {
		global $w2_cfg,$intranet_db;

		$this->objDb = new libdb();
		
		$this->setTableName('W2_STEP_STUDENT_COMMENT');
		$this->setPrimaryKeyName('COMMENT_ID');

		if(is_numeric($commentId)){
			$this->setCommentId($commentId);
			$this->loadRecordFromStorage();
		}
    }
    
    /**
	* LOAD writing details from storage with a writing ID
	* @owner : Ivan (20111111)
    * @input : NIL
	* @return : NIL
	*/
	private function loadRecordFromStorage(){
		$tableName = $this->getTableName();
		$primaryKeyName = $this->getPrimaryKeyName();
		$primaryKeyValue = $this->getPrimaryKeyValue();
		
		$sql = "select * from $tableName where $primaryKeyName = $primaryKeyValue";
		$result = current($this->objDb->returnResultSet($sql));
		
		$this->setCommentId($result['COMMENT_ID']);
		$this->setContent($result['CONTENT']);
		$this->setCommentStatus($result['COMMENT_STATUS']);
		$this->setDateInput($result['DATE_INPUT']);
		$this->setInputBy($result['INPUT_BY']);
		$this->setDateModified($result['DATE_MODIFIED']);
		$this->setModifiedBy($result['MODIFIED_BY']);
	}

////START GET SET///
	private function setTableName($str) {
		$this->tableName = $str;
	}
	private function getTableName() {
		return $this->tableName;
	}
	
	private function setPrimaryKeyName($str) {
		$this->primaryKeyName = $str;
	}
	private function getPrimaryKeyName() {
		return $this->primaryKeyName;
	}
	
	private function getPrimaryKeyValue() {
		return $this->commentId;
	}
	
	public function setCommentId($int) {
		$this->commentId = $int;
	}
	public function getCommentId() {
		return $this->commentId;
	}
	
	public function setContent($str) {
		$this->content = $str;
	}
	public function getContent() {
		return $this->content;
	}
	
	public function setCommentStatus($int) {
		$this->commentStatus = $int;
	}
	public function getCommentStatus() {
		return $this->commentStatus;
	}
	
	public function setDateInput($date) {
		$this->dateInput = $date;
	}
	public function getDateInput() {
		return $this->dateInput;
	}
	
	public function setInputBy($int) {
		$this->inputBy = $int;
	}
	public function getInputBy() {
		return $this->inputBy;
	}
	
	public function setDateModified($date) {
		$this->dateModified = $date;
	}
	public function getDateModified() {
		return $this->dateModified;
	}
	
	public function setModifiedBy($int) {
		$this->modifiedBy = $int;
	}
	public function getModifiedBy() {
		return $this->modifiedBy;
	}
	public function setSticker($str) {
		$this->sticker = $str;
	}
	public function getSticker() {
		return $this->sticker;
	}
	
	public function setVersion($int) {
		$this->version = $int;
	}
	public function getVersion() {
		return $this->version;
	}	
	public function setUserId($int) {
		$this->userId = $int;
	}
	public function getUserId() {
		return $this->userId;
	}
	
	public function setStepId($int) {
		$this->stepId = $int;
	}
	public function getStepId() {
		return $this->stepId;
	}	
////END GET SET///

	public function save() {
		$primaryKey = $this->getPrimaryKeyValue();
		if (is_numeric($primaryKey)) {
			$newPrimaryKey = $this->updateDb();
		}
		else {
			$newPrimaryKey = $this->insertDb();
		}
		
		return $newPrimaryKey;
	}
	
	private function updateDb() {
		# Prepare value for SQL update
		$DataArr = array();
		$DataArr["CONTENT"]			= $this->objDb->pack_value($this->getContent(), "str");
		$DataArr["COMMENT_STATUS"]	= $this->objDb->pack_value($this->getCommentStatus(), "int");
		$DataArr["DATE_MODIFIED"]	= $this->objDb->pack_value($this->getDateModified(), "date");
		$DataArr["MODIFIED_BY"]		= $this->objDb->pack_value($this->getModifiedBy(), "int");
		$DataArr["STICKER"]			= $this->objDb->pack_value($this->getSticker(), "str");
				
		# Build field update values string
		$valueFieldArr = array();
		foreach ($DataArr as $_field => $_value) {
			$valueFieldArr[] = " $_field = $_value ";
		}
		$valueFieldText .= implode(',', $valueFieldArr);
		
		$tableName = $this->getTableName();
		$primaryKeyName = $this->getPrimaryKeyName();
		$primaryKeyValue = $this->getPrimaryKeyValue();
		$sql = "Update $tableName Set $valueFieldText Where $primaryKeyName = '$primaryKeyValue'";
		$success = $this->objDb->db_db_query($sql);
		
		$this->loadRecordFromStorage();
		return $success;
	}
	
	private function insertDb() {
		$this->setDateInput('now()');
		$this->setInputBy($_SESSION['UserID']);
		
		$DataArr = array();
		$DataArr["CONTENT"]			= $this->objDb->pack_value($this->getContent(), "str");
		$DataArr["COMMENT_STATUS"]	= $this->objDb->pack_value($this->getCommentStatus(), "int");
		$DataArr["DATE_INPUT"]		= $this->objDb->pack_value($this->getDateInput(), "date");
		$DataArr["INPUT_BY"]		= $this->objDb->pack_value($this->getInputBy(), "int");
		$DataArr["DATE_MODIFIED"]	= $this->objDb->pack_value($this->getDateModified(), "date");
		$DataArr["MODIFIED_BY"]		= $this->objDb->pack_value($this->getModifiedBy(), "int");
		$DataArr["STEP_ID"]			= $this->objDb->pack_value($this->getStepId(), "int");
		$DataArr["USER_ID"]			= $this->objDb->pack_value($this->getUserId(), "int");
		$DataArr["STICKER"]			= $this->objDb->pack_value($this->getSticker(), "str");
		$DataArr["VERSION"]			= $this->objDb->pack_value($this->getVersion(), "int");
								
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value){
			$fieldArr[] = $field;
			$valueArr[] = $value;
		}
			
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
			
		# Insert Record
		$tableName = $this->getTableName();
		$primaryKeyName = $this->getPrimaryKeyName();
		$primaryKeyValue = $this->getPrimaryKeyValue();
		$sql = "Insert Into $tableName ($fieldText) Values ($valueText)";
		$success = $this->objDb->db_db_query($sql);		

		$commentId = $this->objDb->db_insert_id();
		$this->setCommentId($commentId);
		$this->loadRecordFromStorage();
		
		return $success;
	}
	public function getCommentBankSelection(){
		global $Lang;
		$html = '<select name="r_commentBank" id="r_commentBank" onchange="addComment()">';
		$html .= '<option value="">'.$Lang['W2']['pleaseSelect'].'</option>';
		$commentCnt = count($this->commentBank);
		for($i=0;$i<$commentCnt;$i++){
			$html .= '<option value="'.$i.'">'.$this->commentBank[$i].'</option>';
		}
		$html .= '</select>';
		return $html;
	}
}
?>