<?php
//editing : fai

include_once ($PATH_WRT_ROOT."includes/libxml.php");
class libW2EngContentParser
{
	function parseStepData($stepContent, $tagName)
	{
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');

		$xml = simplexml_load_string( $stepContent );
		$numOfCol = count($xml);
		foreach( (array)$tagName['stepTag'] as $tagCotentName)
		{
			$stepData[$tagCotentName] = (string)$xml->$tagCotentName ;
		}
		//restore settings
		ini_set('zend.ze1_compatibility_mode', $mode);
		return $stepData;
	}
	function parseStep4Data($stepContent, $tagName)
	{
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');
		
		
		$libXML = new libXML();
		$xml = simplexml_load_string( $stepContent );
		
		
		$xml = $libXML->XML_ToArray($stepContent);
		$stepData[$tagName['stepTag'][0]] = $xml[$tagName['stepName']][$tagName['stepTag'][0]] ;
		
		foreach( (array)$xml[$tagName['stepName']][$tagName['stepTag'][1]][$tagName['stepTag'][2]] as $value)
		{
			$stepData[$tagName['stepTag'][1]][] =  $value;
		}
		//restore settings
		
		ini_set('zend.ze1_compatibility_mode', $mode);
		return $stepData;
	}
	function parseResource($resource)
	{
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');

		$libXML = new libXML();
		$xml = simplexml_load_string( $resource );

		$resourceData = $libXML->XML_ToArray($resource);
		
		//restore settings
		ini_set('zend.ze1_compatibility_mode', $mode);
		return $resourceData;
	}
	function parseStep3Data($Data)
	{
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');

		$libXML = new libXML();
		$xml = simplexml_load_string( $Data );
		$returnData = $libXML->XML_ToArray($Data);

		ini_set('zend.ze1_compatibility_mode', $mode);
		return $returnData['step3'];
	}	
	
	function parseTeacherAttachment($attachment){
		//allow XML Parser
		$mode = ini_get('zend.ze1_compatibility_mode');
		ini_set('zend.ze1_compatibility_mode', '0');

		$libXML = new libXML();
		$xml = simplexml_load_string( $attachment );
		$attachmentData = $libXML->XML_ToArray($attachment);
	
		//restore settings
		ini_set('zend.ze1_compatibility_mode', $mode);
		return $attachmentData;
	}	
	
	function parseEngContent($cid,$sch_code='',$scheme_num='')
	{

		if($cid == '' && ($sch_code=='' || $scheme_num=='')){
			return false;
		}

		$objDB = new libdb();
		if(!empty($sch_code)&&!empty($scheme_num)){
			$sql= "SELECT * FROM W2_CONTENT_DATA WHERE schoolCode = '".$sch_code."' AND schemeNum = '".$scheme_num."'";
		}else{
			$sql= "SELECT * FROM W2_CONTENT_DATA WHERE cid='$cid'; ";
		}
		
		$rs = $objDB->returnResultSet($sql);

		if(count($rs) != 1){
			//should return one and only one result only
			return false;
		}

		$rs = $rs[0];
		$cid = $rs['cid'];
		$topicName = $rs['topicName'];
		$topicIntro = $rs['topicIntro'];
		$level = $rs['level'];
		$category = $rs['category'];
		$step1Content = $rs['step1Content'];
		$step2Content = $rs['step2Content'];
		$step3Content = $rs['step3Content'];
		$step4Content = $rs['step4Content'];
		$step5Content = $rs['step5Content'];
		$resource = $rs['resource'];
		$r_contentCode = $rs['r_contentCode'];
		$schoolCode = $rs['schoolCode'];
		$schemeNum = $rs['schemeNum'];
		$status = $rs['status'];
		$dataInput = $rs['dataInput'];
		$createdBy = $rs['createdBy'];
		$dataModified = $rs['dataModified'];
		$modifiedBy = $rs['modifiedBy'];
		$powerConceptID = $rs['powerConceptID'];
		$powerBoardID = $rs['powerBoardID'];
		$attachment = $rs['attachment'];
		$conceptType = $rs['conceptType'];		
		$resource = $rs['resource'];
		$grade = $rs['grade']; 
		$teacherAttachment = $rs['teacherAttachment']; 

		$tagName = array('stepName' =>'step1',
			'stepTag'=>array('step1Int', 'step1TextType', 'step1Purpose', 'step1Content')
		);
		$step1Data = $this->parseStepData($step1Content,$tagName);
		
		$tagName = array('stepName' =>'step2',
			'stepTag'=>array('step2Int')
		);
		$step2Data = $this->parseStepData($step2Content,$tagName);
		
		
//		$tagName = array('stepName' =>'step3',
//			'stepTag'=>array('step3Int', 'step3SampleWriting', 'step3Grammar','step3Vocab')
//		);
//		$step3Data = $this->parseStepData($step3Content,$tagName);
		$step3Data = $this->parseStep3Data($step3Content);
		
		$tagName = array('stepName' =>'step4',
			'stepTag'=>array('step4Int', 'step4DraftQList','step4DraftQ')
		);
		$step4Data = $this->parseStep4Data($step4Content,$tagName);
		
		
		$tagName = array('stepName' =>'step5',
			'stepTag'=>array('step5Int', 'step5DefaultWriting')
		);
		$step5Data = $this->parseStepData($step5Content,$tagName);

		$resourceData = $this->parseResource($resource);
		$teacherAttachmentData = $this->parseTeacherAttachment($teacherAttachment);
		
		$resultArray = array('cid'=>$cid,'topicName'=>$topicName, 'topicIntro'=>$topicIntro, 'level'=>$level, 'category'=>$category,
				'step1Data'=>$step1Data, 'step2Data'=>$step2Data, 'step3Data'=>$step3Data, 'step4Data'=>$step4Data, 'step5Data'=>$step5Data,
				'r_contentCode'=>$r_contentCode, 'schoolCode'=>$schoolCode, 'schemeNum'=>$schemeNum,  'conceptType'=>$conceptType, 
					'powerConceptID'=>$powerConceptID,  'powerBoardID'=>$powerBoardID,  'attachment'=>$attachment, 
				'resourceData' =>$resourceData, 'status' => $status, 'grade' =>$grade, 'teacherAttachmentData'=>$teacherAttachmentData['TeacherAttachment']
		);
		
		return $resultArray;
	}
	
}
?>