<?
// using: ivan
class libW2ContentCHI extends libW2Content  {

	public function __construct() {
		global $w2_cfg;
		
		parent::__construct();
		$this->setContentCode($w2_cfg["contentArr"]["chi"]["contentCode"]);
	}

	public function getConceptMapStepCode(){
		return 'c2';
	}
	public function getConceptMapTitle(){
		return 'W2 CHINESE POWERCONCEPT';
	}

	public function getConceptMapInstruction(){
		return 'W2 CHINESE POWERCONCEPT INSTRUCTION';
	}
	
	
	public function getUnitTitle() {
		return '體裁';
	}
	
//	public function getUnitName() {
//		return '描寫';
//	}
	
	public function getSchemeTitle() {
		return '主題';
	}
	
//	public function getSchemeName() {
//		return '茶樓眾生相';
//	}
	
	public function getWritingTaskStepCode() {
		return 'c5';
	}
}
?>