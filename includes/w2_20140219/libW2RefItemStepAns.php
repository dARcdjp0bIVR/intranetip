<?
// using: 

class libW2RefItemStepAns extends libW2RefItem {
	
	// item-related
	private $code;
	
	public function __construct() {
		global $w2_cfg;
		
		parent::__construct();
		$this->setSourceType($w2_cfg["refItemObjectSource"]["stepAns"]);
    }
    
    #################################################
    ########### Get Set Functions [Start] ###########
    #################################################
    
    public function setCode($str) {
    	$this->code = $str;
    }
    private function getCode() {
    	return $this->code;
    }
    
    #################################################
    ############ Get Set Functions [End] ############
    #################################################
    
    public function returnDisplayHtmlManageMode() {
    	$html = $this->returnDivHtmlTemplate();
    	$html = str_replace('{{{itemToolbarDivId}}}', $this->returnToolbarDivID(), $html);
    	$html = str_replace('{{{itemTitle}}}', $this->getTitle(), $html);
    	$html = str_replace('{{{itemToolbarHtml}}}', '', $html);	// cannot edit and delete for this type of item
    	
    	return $html;
    }
    
    public function returnToolbarDivID() {
    	return 'gloss_tool_'.$this->getInfoboxCode().'_category_'.$this->getRefCategoryCode().'_item_'.$this->getCode();
    }
}
?>