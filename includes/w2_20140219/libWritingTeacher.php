<?
// using: 

class libWritingTeacher {

	private $SELECT_WRITING_STMT; 

	private $objDB;
	private $teacherUserId;
	private $writingId;

	public function __construct($teacherUserId = NULL) {
		global $w2_cfg, $intranet_db;

		$this->objDB = new libdb();
		if(is_numeric($teacherUserId) && $teacherUserId >0){
			$this->teacherUserId = $teacherUserId;
		}
    }

////START GET SET///
	public function setWritingId($id){
		$this->writingId = $id;
	}
	public function getWritingId() {
		return $this->studentUserId;
	}
	
	public function getTeacherUserId() {
		return $this->teacherUserId;
	} 

////END GET SET///
	
	/**
	* GET a set of writing that a teacher has of a content
	* @owner : Ivan (20111102)
	* @param : String $content content code of the writing
	* @param : Boolean $accessibleOnly, return accessible (is writing teacher) writing only
	* @return : Array, array of a set of writing of the content
	*/
	public function findTeacherWritingByContent($content, $accessibleOnly=true){
		global $intranet_db,$w2_cfg;
		$sql = $this->findSqlOfTeacherWritingByContent($content, $accessibleOnly);
		$result = $this->objDB->returnResultset($sql);
		return $result;
	}
	
	/**
	* GET the SQL statement to retrieve the teacher writing of a content
	* @owner : Ivan (20111102)
	* @param : String $content content code of the writing
	* @param : Boolean $accessibleOnly, return accessible (is writing teacher) writing only
	* @return : String, SQL statement to retrieve the teacher writing of a content
	*/
	public function findSqlOfTeacherWritingByContent($content, $accessibleOnly=true){
		global $intranet_db, $w2_cfg;
		
		$teacherUserID = $this->getTeacherUserId();
		
		if ($accessibleOnly) {
			$condsWritingUserId = " And wt.USER_ID = '".$teacherUserID."' ";
		}
		
		$W2_WRITING_TEACHER = $intranet_db.'.W2_WRITING_TEACHER';
		$W2_WRITING = $intranet_db.'.W2_WRITING';
		$sql = "Select 
						w.WRITING_ID , w.TITLE, w.CONTENT_CODE, w.SCHEME_CODE, w.START_DATE_TIME, w.END_DATE_TIME, w.RECORD_STATUS
				From 
						$W2_WRITING_TEACHER as wt
						Inner Join $W2_WRITING as w On (wt.WRITING_ID = w.WRITING_ID) 
				Where 
						wt.DELETE_STATUS = '".$w2_cfg["DB_W2_WRITING_TEACHER_DELETE_STATUS"]["active"]."'
						And w.DELETE_STATUS = '".$w2_cfg["DB_W2_WRITING_DELETE_STATUS"]["active"]."'
						And w.CONTENT_CODE = '".$content."'
						$condsWritingUserId
				Group By
						w.WRITING_ID
				";
//debug_r($sql);
		return $sql;
	}
}
?>