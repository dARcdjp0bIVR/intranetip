<?
// using: ivan
class libW2ContentENG extends libW2Content  {

	public function __construct() {
		global $w2_cfg;
		
		parent::__construct();
		$this->setContentCode($w2_cfg["contentArr"]["eng"]["contentCode"]);
	}

	public function getConceptMapStepCode(){
		return 'c2';
	}
	public function getConceptMapTitle(){
		return 'W2 ENGLISH POWERCONCEPT';
	}

	public function getConceptMapInstruction(){
		return 'W2 ENGLISH POWERCONCEPT INSTRUCTION';
	}
	
	
	public function getUnitTitle() {
		return 'Text-type';
	}
	
//	public function getUnitName() {
//		return 'Diary';
//	}
	
	public function getSchemeTitle() {
		return 'Theme';
	}
	
//	public function getSchemeName() {
//		return 'An Unforgettable day';
//	}
	
	public function getWritingTaskStepCode() {
		return 'c5';
	}
	
	public function getTextTypeSelection($ansCode, $answeredAnsCode, $disabledAttr) {
		global $w2_libW2;
		
		return <<<html
		<select name="r_question[select][{$ansCode}]" style="width:80px;" {$disabledAttr}>
	    	<option value="2" {$w2_libW2->getAnsSelected($answeredAnsCode, 2)}>Article</option>
	        <option value="3" {$w2_libW2->getAnsSelected($answeredAnsCode, 3)}>Blog</option>
	        <option value="1" {$w2_libW2->getAnsSelected($answeredAnsCode, 1)}>Diary</option>
	        <option value="4" {$w2_libW2->getAnsSelected($answeredAnsCode, 4)}>Email</option>
		</select>
html;
		
	}
}
?>