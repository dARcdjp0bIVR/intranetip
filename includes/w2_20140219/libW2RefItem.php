<?php
abstract class libW2RefItem {
	
	private $isEditable;
	private $infoboxCode;
	private $displayMode;
	
	private $refCategoryType;
	private $refCategoryCode;
	
	private $title;
	private $sourceType;
	
	public function __construct() {
		
    }
    
    #################################################
    ########### Get Set Functions [Start] ###########
    #################################################
    
    public function setIsEditable($bool) {
    	$this->isEditable = $bool;
    }
    protected function getIsEditable() {
    	return $this->isEditable;
    }
    
    public function setInfoboxCode($str) {
    	$this->infoboxCode = $str;
    }
    protected function getInfoboxCode() {
    	return $this->infoboxCode;
    }
    
    public function setDisplayMode($str) {
    	$this->displayMode = $str;
    }
    protected function getDisplayMode() {
    	return $this->displayMode;
    }
    
    public function setRefCategoryType($str) {
    	$this->refCategoryType = $str;
    }
    protected function getRefCategoryType() {
    	return $this->refCategoryType;
    }
    
    public function setRefCategoryCode($str) {
    	$this->refCategoryCode = $str;
    }
    protected function getRefCategoryCode() {
    	return $this->refCategoryCode;
    }
    
    public function setTitle($str) {
    	$this->title = $str;
    }
    public function getTitle() {
    	return $this->title;
    }
    
    protected function setSourceType($str) {
    	$this->sourceType = $str;
    }
    protected function getSourceType() {
    	return $this->sourceType;
    }
    
    
    #################################################
    ############ Get Set Functions [End] ############
    #################################################
    
    /**
	* Return the html of the item display for manage mode
	* @owner	Ivan (20120216)
	* @return	String of html of the item display
	*/
    abstract public function returnDisplayHtmlManageMode();
    
    /**
	* Return the unique toolbar div id
	* @owner	Ivan (20120216)
	* @return	String of the div id
	*/
    abstract public function returnToolbarDivID();
    
    /**
	* Return the html of the display
	* @owner	Ivan (20120216)
	* @return	String of html of the item display
	*/
    public function returnDisplayHtml() {
    	global $w2_cfg;
    	
    	switch ($this->getDisplayMode()) {
    		case $w2_cfg["refDisplayMode"]["manage"]:
    			$html = $this->returnDisplayHtmlManageMode();
    			break;
    		case $w2_cfg["refDisplayMode"]["view"]:
    			$html = $this->returnDisplayHtmlViewMode();
    			break;
    	}
    	
    	return $html;
    }
    
    /**
	* Return the html of the item display of view mode
	* @owner	Ivan (20120216)
	* @return	String of the html of the item display of view mode
	*/
    private function returnDisplayHtmlViewMode() {
    	return '<li>'.$this->getTitle().'</li>';
    }
    
    /**
	* Return the html of the item display template of manage mode
	* @owner	Ivan (20120216)
	* @return	String of the html of the item display template of manage mode
	*/
    protected function returnDivHtmlTemplate() {
    	return <<<html
<div onmouseout="MM_showHideLayers('{{{itemToolbarDivId}}}','','hide');this.className='gloss_item'" onmouseover="MM_showHideLayers('{{{itemToolbarDivId}}}','','show');this.className='gloss_item_over'" class="gloss_item">
    <h1>{{{itemTitle}}}</h1>
    {{{itemToolbarHtml}}}
    <p class="spacer"></p>
</div>
html;
    }
}
?>