<?
// using: ivan
class libW2ContentSCI extends libW2Content  {

	public function __construct() {
		global $w2_cfg;
		
		parent::__construct();
		$this->setContentCode($w2_cfg["contentArr"]["sci"]["contentCode"]);
	}

	public function getConceptMapStepCode(){
		return 'c3';
	}
	public function getConceptMapTitle(){
		return 'W2 SCIENCE POWERCONCEPT';
	}

	public function getConceptMapInstruction(){
		return 'W2 SCIENCE POWERCONCEPT INSTRUCTION';
	}
	
	
	public function getUnitTitle() {
		return '體裁';
	}
	
//	public function getUnitName() {
//		return '描寫';
//	}
	
	public function getSchemeTitle() {
		return 'Theme';
	}
	
//	public function getSchemeName() {
//		return 'Renewable energy';
//	}
	
	public function getWritingTaskStepCode() {
		return 'c6';
	}
}
?>