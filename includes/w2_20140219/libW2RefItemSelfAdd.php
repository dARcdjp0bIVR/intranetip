<?
// using: 

class libW2RefItemSelfAdd extends libW2RefItem {
	
	private $refItemId;
	private $writingStudentId;
	private $dateInput;
	private $inputBy;
	private $dateModified;
	private $modifiedBy;
	private $deleteStatus;
	private $deletedBy;
	
	private $objDb;
	
	public function __construct($id=null) {
		global $w2_cfg;
		
		parent::__construct();
		$this->setSourceType($w2_cfg["refItemObjectSource"]["selfAdd"]);
		
		$this->objDb = new libdb();
		
		if(is_numeric($id)){
			$this->setRefItemId($id);
			$this->loadRecordFromStorage();
		}
    }
    
    #################################################
    ########### Get Set Functions [Start] ###########
    #################################################
    
    public function setRefItemId($int) {
    	$this->refItemId = $int;
    }
    private function getRefItemId() {
    	return $this->refItemId;
    }
    
    public function setWritingStudentId($int) {
    	$this->writingStudentId = $int;
    }
    private function getWritingStudentId() {
    	return $this->writingStudentId;
    }
    
    public function setDateInput($str){
		$this->dateInput = $str;
	}
	private function getDateInput(){
		return $this->dateInput;
	}
    
    public function setInputBy($id){
		$this->inputBy = $id;
	}
	private function getInputBy(){
		return $this->inputBy;
	}
	
	public function setDateModified($str){
		$this->dateModified = $str;
	}
	private function getDateModified(){
		return $this->dateModified;
	}
	
	public function setModifiedBy($id){
		$this->modifiedBy = $id;
	}
	private function getModifiedBy(){
		return $this->modifiedBy;
	}
	
	public function setDeleteStatus($val){
		$this->deleteStatus = $val;
	}
	private function getDeleteStatus(){
		return $this->deleteStatus;
	}
	
	public function setDeletedBy($id){
		$this->deletedBy = $id;
	}
	private function getDeletedBy(){
		return $this->deletedBy;
	}
    #################################################
    ############ Get Set Functions [End] ############
    #################################################
    
    private function loadRecordFromStorage(){
		global $intranet_db, $w2_cfg;
		
		$W2_REF_ITEM = $intranet_db.'.W2_REF_ITEM';
		$sql = "Select
						REF_ITEM_ID,
						REF_CATEGORY_TYPE,
						REF_CATEGORY_CODE,
						INFOBOX_CODE,
						WRITING_STUDENT_ID,
						TITLE,
						DATE_INPUT,
						INPUT_BY,
						DATE_MODIFIED,
						MODIFIED_BY,
						DELETE_STATUS,
						DELETED_BY
				From
						$W2_REF_ITEM
				Where
						REF_ITEM_ID = '".$this->getRefItemId()."'
				";
		$resultAry = $this->objDb->returnResultSet($sql);
		
		$this->setRefItemId($resultAry[0]['REF_ITEM_ID']);
		$this->setRefCategoryType($resultAry[0]['REF_CATEGORY_TYPE']);
		$this->setRefCategoryCode($resultAry[0]['REF_CATEGORY_CODE']);
		$this->setInfoboxCode($resultAry[0]['INFOBOX_CODE']);
		$this->setWritingStudentId($resultAry[0]['WRITING_STUDENT_ID']);
		$this->setTitle($resultAry[0]['TITLE']);
		$this->setDateInput($resultAry[0]['DATE_INPUT']);
		$this->setInputBy($resultAry[0]['INPUT_BY']);
		$this->setDateModified($resultAry[0]['DATE_MODIFIED']);
		$this->setModifiedBy($resultAry[0]['MODIFIED_BY']);
		$this->setDeleteStatus($resultAry[0]['DELETE_STATUS']);
		$this->setDeletedBy($resultAry[0]['DELETED_BY']);
	}
	
	public function save(){
		$refItemId = $this->getRefItemId();

		$recordId = null;
		if(is_numeric($refItemId) && $refItemId > 0) {
			$recordId = $this->updateDb();
		}else{
			$recordId = $this->insertDb();
		}

		$this->setRefItemId($recordId);
		$this->loadRecordFromStorage();

		return $recordId;
	}
	
	private function insertDb(){
		global $w2_cfg, $intranet_db;

		$this->setDateInput('now()');
		$this->setDateModified('now()');
		$this->setDeleteStatus($w2_cfg["DB_W2_REF_ITEM_DELETE_STATUS"]["active"]);

		$dataAry = array();
		$dataAry["WRITING_STUDENT_ID"]	= $this->objDb->pack_value($this->getWritingStudentId(), "int");
		$dataAry["INFOBOX_CODE"]		= $this->objDb->pack_value($this->getInfoboxCode(), "str");
		$dataAry["REF_CATEGORY_TYPE"]	= $this->objDb->pack_value($this->getRefCategoryType(), "str");
		$dataAry["REF_CATEGORY_CODE"]	= $this->objDb->pack_value($this->getRefCategoryCode(), "str");
		$dataAry["TITLE"]				= $this->objDb->pack_value($this->getTitle(), "str");
		$dataAry["DATE_INPUT"]			= $this->objDb->pack_value($this->getDateInput(), "date");
		$dataAry["INPUT_BY"]			= $this->objDb->pack_value($this->getInputBy(), "int");
		$dataAry["DATE_MODIFIED"]		= $this->objDb->pack_value($this->getDateModified(), "date");
		$dataAry["MODIFIED_BY"]			= $this->objDb->pack_value($this->getModifiedBy(), "int");	
		$dataAry["DELETE_STATUS"]		= $this->objDb->pack_value($this->getDeleteStatus(), "date");
		$dataAry["DELETED_BY"]			= $this->objDb->pack_value($this->getDeletedBy(), "int");
		
		$sqlStrAry = $this->objDb->concatFieldValueToSqlStr($dataAry);
		$fieldStr = $sqlStrAry['sqlField'];
		$valueStr = $sqlStrAry['sqlValue'];
			
		# Insert Record
		$successAry = array();
		$W2_REF_ITEM = $intranet_db.'.W2_REF_ITEM';
		$sql = "Insert Into $W2_REF_ITEM ($fieldStr) Values ($valueStr)";
		$successAry['insertRecord'] = $this->objDb->db_db_query($sql);
						
		$recordId = $this->objDb->db_insert_id();
		$this->setRefItemId($recordId);
		$this->loadRecordFromStorage();
		
		return $this->getRefItemId();		
	}
	
	private function updateDb(){
		global $intranet_db;
		
		if(is_numeric($this->getRefItemId()) && $this->getRefItemId() > 0){
			// it is a valid writing id
			//do nothing 
		}else{
			return null;
		}

		$dataAry = array();
		$dataAry["WRITING_STUDENT_ID"]	= $this->objDb->pack_value($this->getWritingStudentId(), "int");
		$dataAry["INFOBOX_CODE"]		= $this->objDb->pack_value($this->getInfoboxCode(), "str");
		$dataAry["REF_CATEGORY_TYPE"]	= $this->objDb->pack_value($this->getRefCategoryType(), "str");
		$dataAry["REF_CATEGORY_CODE"]	= $this->objDb->pack_value($this->getRefCategoryCode(), "str");
		$dataAry["TITLE"]				= $this->objDb->pack_value($this->getTitle(), "str");
		$dataAry["DATE_MODIFIED"]		= $this->objDb->pack_value($this->getDateModified(), "date");
		$dataAry["MODIFIED_BY"]			= $this->objDb->pack_value($this->getModifiedBy(), "int");	
		$dataAry["DELETE_STATUS"]		= $this->objDb->pack_value($this->getDeleteStatus(), "date");
		$dataAry["DELETED_BY"]			= $this->objDb->pack_value($this->getDeletedBy(), "int");

		$valueFieldTextAry = array();
		foreach ($dataAry as $field => $value) {
			$valueFieldTextAry[] = $field." = ".$value;
		}
		$valueFieldText = implode(',', $valueFieldTextAry);
		
		$W2_REF_ITEM = $intranet_db.'.W2_REF_ITEM';
		$sql = "Update $W2_REF_ITEM Set ".$valueFieldText." Where REF_ITEM_ID = ".$this->getRefItemId();
		$this->objDb->db_db_query($sql);
		
		return $this->getRefItemId();
	}
	
	public function delete() {
		global $w2_cfg;
		
		$this->setDeleteStatus($w2_cfg["DB_W2_REF_ITEM_DELETE_STATUS"]["isDeleted"]);
		$tempId = $this->updateDB();
		
		return ($tempId > 0)? true : false;
	}
    
    
    public function returnDisplayHtmlManageMode() {
    	global $Lang;
    	
    	$titleDisplayDivId = 'itemTitleDisplayDiv_'.$this->getRefItemId();
    	$titleEditDivId = 'itemTitleEditDiv_'.$this->getRefItemId();
    	$titleEditTbId = 'itemTitleEditTb_'.$this->getRefItemId();
    	$editBtnOnclickTag = 'onclick="saveRefItem(\''.$titleEditDivId.'\', \''.$titleEditTbId.'\', \''.$this->getRefCategoryType().'\', \''.$this->getRefCategoryCode().'\', \''.$this->getInfoboxCode().'\', \''.$this->getRefItemId().'\');"';
    	$cancelBtnOnclickTag = 'onclick="cancelEditRefItem(\''.$titleDisplayDivId.'\', \''.$titleEditDivId.'\', \''.$titleEditTbId.'\');"';
    	
    	$titleHtml = '';
    	$titleHtml .= '<div id="'.$titleDisplayDivId.'"><h1>'.$this->getTitle().'</h1></div>'."\r\n";
    	$titleHtml .= '<div id="'.$titleEditDivId.'" style="display:none;">'."\r\n";
			$titleHtml .= '<input type="text" size="20" id="'.$titleEditTbId.'" value="'.intranet_htmlspecialchars($this->getTitle()).'" />'."\r\n";
			$titleHtml .= '<input type="button" class="formsmallbutton" value="'.$Lang['Btn']['Save'].'" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" '.$editBtnOnclickTag.' />'."\r\n";
			$titleHtml .= '<input type="button" class="formsmallbutton" value="'.$Lang['Btn']['Cancel'].'" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" '.$cancelBtnOnclickTag.' />'."\r\n";
		$titleHtml .= '</div>'."\r\n";
    	
    	$html = $this->returnDivHtmlTemplate();
    	$html = str_replace('{{{itemToolbarDivId}}}', $this->returnToolbarDivID(), $html);
    	$html = str_replace('{{{itemTitle}}}', $titleHtml, $html);
    	
    	$h_toolbar = '';
    	if ($this->getIsEditable()) {
    		$h_toolbar = $this->returnToolbarHtml();
    	}
    	$html = str_replace('{{{itemToolbarHtml}}}', $h_toolbar, $html);
    	
    	return $html;
    }
    
    public function returnToolbarDivID() {
    	global $w2_cfg;
    	
    	return 'gloss_tool_'.$this->getInfoboxCode().'_category_'.$this->getRefCategoryCode().'_item_'.$this->getRefItemId();
    }
    
    private function returnToolbarHtml() {
    	global $w2_cfg, $Lang;
    	
    	$toolbarDivId = $this->returnToolbarDivID();
    	
    	$html = '';
    	$html .= '<div id="'.$toolbarDivId.'" class="gloss_tool">'."\r\n";
			$html .= '<a title="'.$Lang['Btn']['Edit'].'" class="tool_edit" href="javascript:void(0);" onclick="triggerEditRefItemUi(\''.$this->getRefItemId().'\')"></a>'."\r\n";
			$html .= '<a title="'.$Lang['Btn']['Delete'].'" class="tool_delete" href="javascript:void(0);" onclick="deleteRefItem(\''.$Lang['W2']['jsWarningAry']['deleteVocab'].'\', \''.$this->getRefItemId().'\', \''.$this->getInfoboxCode().'\');"></a>'."\r\n";
		$html .= '</div>'."\r\n";
    	
    	return $html;
    }
}
?>