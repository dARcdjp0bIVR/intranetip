<?php
/**
 * Change Log:
 * 2019-08-07 Crabbe
 *  - added in getMembersFromGroupByEmail()
 * 2018-11-13
 *  //Ammended so to not accidentally delete users in the future CC on Authorisation of JL
 * -forced soft delete
 * 2018-07-23 Pun [138670] [ip.2.5.9.10.1]
 *  - Modified initialize(), added flag for readonly API
 */
include_once $ssoservice["Google"]['api_php_client_root'] . '/vendor/autoload.php';

class libGoogleAPI{

    public static function getAllGoogleAPIs($ssoservice){
        $google_apis = array();
        for($i=0;$i<count($ssoservice["Google"]['api']);$i++){
            $google_apis[$i] = new libGoogleAPI($i);
        }
        return $google_apis;
    }

    private $application_name;
    private $client_secret_path;
    private $client;
    private $scopes;
    private $service;
    private $domain;
    private $config_index;
    private $soft_delete;
    private $batch = null;
    private $batch_item_count = 1;
    private $batch_item_prefix = 'response-';

    public function __construct($config_index = '0'){
        $this->config_index = $config_index;
        $this->initialize();
    }

    public function test(){

        $users = $this->getAllGoogleAccounts();

        if (count($users) == 0) {
            print "No users found.\n";
        } else {
            print "Users:\n";
            foreach ($users as $user) {
                printf("%s (%s)\n", $user->getPrimaryEmail(),
                    $user->getName()->getFullName());
            }
        }
    }

    public function getDomain(){
        return $this->domain;
    }

    public function getConfigIndex(){
        return $this->config_index;
    }

    public function startBatch(){
        $this->startGoogleBatch();
    }

    public function sendBatch(){
        return $this->sendGoogleBatch();
    }

    /************************************
     * $code
     * 409: Duplicated Entry
     * 412: Domain user limit reached
     */
    public function isError($result,$code=''){
        if (get_class($result) == 'Google_Service_Exception'){
            $obj_error=json_decode($result->getMessage(),true);
//			echo $obj_error['error']['code'];
            if($obj_error['error']['code']==$code){
                return true;
            }
            if($code==''){
                return true;
            }
            return false;
        }else{
            return false;
        }
    }

    public function getErrorMessage($result){
        if (get_class($result) == 'Google_Service_Exception'){
            $obj_error=json_decode($result->getMessage(),true);
            return $obj_error['error']['message'];
        }else{
            return null;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    //business-level functions

    //user accounts function
    public function numberOfAccounts(){
        $users = $this->getAllGoogleAccounts();
        return count($users);
    }

    public function newGoogleAccountByFirstNameAndLastName($first_name,$last_name,$email='',$password=''){
        if($email == ''){
            $email = $first_name.$last_name.'@'.$this->domain;
        }

        $parameters = array(
            'GivenName' => $first_name,
            'FamilyName' => $last_name,
            'PrimaryEmail' => $email,
            'Password' => $password
        );

        $email = $this->insertGoogleDirectoryUser($parameters);

        return $email;
    }

    public function newGoogleAccountByBatch($batch=array()){
        $array_parameters = array();
        foreach((array)$batch as $record){
            $parameters = array(
                'GivenName' => $record['first_name'],
                'FamilyName' => $record['last_name'],
                'PrimaryEmail' => $record['email'],
                'Password' => $record['password'],
                'uid' => $record['uid']
            );
            array_push($array_parameters, $parameters);
        }

        $result = $this->batchInsertGoogleDirectoryUser($array_parameters);
        return $result;
    }

    public function getAllGoogleAccounts(){
        $parameters = array(
            'Mode' => 'ALL'
        );

        $users = $this->selectGoogleDirectoryUser($parameters);
        return $users;
    }

    public function getGoogleAccountByEmail($email){
        $parameters = array(
            'Mode' => 'BY_EMAIL',
            'PrimaryEmail' => $email
        );

        $users = $this->selectGoogleDirectoryUser($parameters);
        if(count($users)>0){
            return $users[0];
        }else{
            return null;
        }
    }

    public function suspendGoogleAccountByEmail($email){
        $user = $this->getGoogleAccountByEmail($email);
        if($user != null){
            $user->setSuspended(true);

            $parameters = array(
                'User' => $user
            );

            return $this->updateGoogleDirectoryUser($parameters);

        }else{
            return false;
        }
    }

    public function reactivateGoogleAccountByEmail($email){
        $user = $this->getGoogleAccountByEmail($email);
        if($user != null){
            $user->setSuspended(false);

            $parameters = array(
                'User' => $user
            );

            return $this->updateGoogleDirectoryUser($parameters);

        }else{
            return false;
        }
    }

    public function updateGoogleAccountInformation($email,$surname,$given_name){
        $user = $this->getGoogleAccountByEmail($email);
        if($user != null){
            $name = new Google_Service_Directory_UserName();
            $name->setGivenName($given_name);
            $name->setFamilyName($surname);
            $user->setName($name);

            $parameters = array(
                'User' => $user
            );

            return $this->updateGoogleDirectoryUser($parameters);
        }else{
            return false;
        }
    }

    public function deleteGoogleAccountByEmail($email){ //Ammended so to not accidentally delete users in the future CC on Authorisation of JL
        $user = $this->getGoogleAccountByEmail($email);
        if($user != null){
            //if($this->soft_delete){
            return $this->suspendGoogleAccountByEmail($email);
            //}else{
// 				$parameters = array(
// 					'User' => $user
// 				);

// 				return $this->deleteGoogleDirectoryUser($parameters);
            //}
        }else{
            return false;
        }
    }
    //End of user accounts function

    //---------------------------------------

    //group functions

    public function newGoogleGroup($name,$email){

        $parameters=array(
            'email'=>$email,
            'name'=>$name,
            'description'=>$name
        );
        $this->insertGoogleDirectoryGroup($parameters);
    }

    public function newGoogleGroupByBatch($batch=array()){
        $array_parameters = array();
        foreach((array)$batch as $record){
            $parameters = array(
                'email' => $record['email'],
                'name' => $record['name'],
                'description' => $record['name'],
                'group_id' => $record['group_id']
            );
            array_push($array_parameters, $parameters);
        }
        return $this->batchInsertGoogleDirectoryGroup($array_parameters);
    }

    public function getAllGoogleGroups(){
        $parameters = array(
            'Mode' => 'ALL'
        );

        $groups = $this->selectGoogleDirectoryGroup($parameters);
        return $groups;
    }

    public function getGoogleGroupByEmail($email){
        $parameters = array(
            'Mode' => 'BY_EMAIL',
            'GroupEmail' => $email
        );

        $groups = $this->selectGoogleDirectoryGroup($parameters);
        if(count($groups)>0){
            return $groups[0];
        }else{
            return null;
        }
    }

    public function updateGoogleGroupByEmail($email,$name,$new_email){
        $group = $this->getGoogleGroupByEmail($email);
        if($group != null){
            $group->setName($name);
            $group->setEmail($new_email);

            $parameters = array(
                'Group' => $group
            );

            return $this->updateGoogleDirectoryGroup($parameters);
        }else{
            return false;
        }
    }

    public function deleteGoogleGroupByEmail($email){
        $group = $this->getGoogleGroupByEmail($email);
        if($group != null){
            $parameters = array(
                'GroupEmail' => $email
            );

            return $this->deleteGoogleDirectoryGroup($parameters);
        }else{
            return false;
        }
    }

    public function addMemberToGroup($group_email,$member_email){

        $parameters=array(
            'email'=>$member_email,
            'group_email'=>$group_email
        );
        $this->insertGoogleDirectoryGroupMember($parameters);
    }

    public function addMemberToGroupByBatch($batch=array()){
        return $this->batchInsertGoogleDirectoryGroupMember($batch);
    }

    public function getMemberFromGroupByEmail($group_email, $email){
        $parameters = array(
            'Mode' => 'BY_EMAIL',
            'GroupEmail' => $group_email,
            'PrimaryEmail' => $email
        );

        $members = $this->selectGoogleDirectoryGroupMember($parameters);
        if(count($members)>0){
            return $members[0];
        }else{
            return null;
        }
    }

    public function getMembersFromGroupByEmail($group_email){
        $parameters = array(
            'Mode' => 'ALL',
            'GroupEmail' => $group_email
        );
        $available_user_logins = array();
        $members = $this->selectGoogleDirectoryGroupMember($parameters);
        foreach ($members as $member) {
            $email = $member['email'];
            $location = strpos($email,'@');
            $length =$location-strlen($email);
            $available_user_logins[] = substr($email,0,$length);
        }
        if(count($available_user_logins)>0){
            return $available_user_logins;
        }else{
            return null;
        }
    }

    public function removeMemberFromGoogleByEmail($group_email, $email){
        $member = $this->getMemberFromGroupByEmail($group_email, $email);
        if($member != null){
            $parameters = array(
                'GroupEmail' => $group_email,
                'PrimaryEmail' => $email
            );

            return $this->deleteGoogleDirectoryGroupMember($parameters);
        }else{
            return false;
        }
    }

    public function getGroupsByGoogleAccountEmail($email){
        $parameters = array(
            'Mode' => 'BY_PRIMARY_EMAIL',
            'PrimaryEmail' => $email
        );

        return $this->selectGoogleDirectoryGroup($parameters);
    }

    //End of group functions

    //---------------------------------------

    ///////////////////////////////////////////////////////////////////////////////////////////////

    //functions that call google api
    private function startGoogleBatch(){
        $this->client->setUseBatch(true);
        $this->batch = new Google_Http_Batch($this->client);
    }

    private function sendGoogleBatch(){
        return array(
            'array_result'=>$this->batch->execute(),
            'result_prefix'=>$this->batch_item_prefix,
        );
    }

    //user accounts function
    private function insertGoogleDirectoryUser($parameters = array()){

        if(count($parameters)>0){
            $user = new Google_Service_Directory_User();
            $name = new Google_Service_Directory_UserName();
            $new_person = array();

            $name->setGivenName($parameters['GivenName']);
            $name->setFamilyName($parameters['FamilyName']);
            $user->setName($name);
            $user->setHashFunction("SHA-1");
            $user->setPrimaryEmail($parameters['PrimaryEmail']);
            $user->setPassword(hash("sha1",$parameters['Password']));
            // the JSON object shows us that externalIds is an array, so that's how we set it here
            #$user->setExternalIds(array("value"=>28790,"type"=>"custom","customType"=>"EmployeeID"));

            $result = $this->service->users->insert($user);
            return $result->primaryEmail;
        }else{
            return '';
        }
    }

    private function batchInsertGoogleDirectoryUser($array_parameters = array()){
        $count_begin = $this->batch_item_count;
        $array_request_parameters=array();

        foreach($array_parameters as $parameters){
            if(count($parameters)>0){
                $user = new Google_Service_Directory_User();
                $name = new Google_Service_Directory_UserName();
                $new_person = array();

                $name->setGivenName($parameters['GivenName']);
                $name->setFamilyName($parameters['FamilyName']);
                $user->setName($name);
                $user->setHashFunction("SHA-1");
                $user->setPrimaryEmail($parameters['PrimaryEmail']);
                $user->setPassword(hash("sha1",$parameters['Password']));
                // the JSON object shows us that externalIds is an array, so that's how we set it here
                #$user->setExternalIds(array("value"=>28790,"type"=>"custom","customType"=>"EmployeeID"));

                $request = $this->service->users->insert($user);
                $this->batch->add($request, $this->batch_item_count);
                $array_request_parameters[$this->batch_item_count]=$parameters;
                $this->batch_item_count++;
            }
        }
        $count_end = $this->batch_item_count-1;
        return array(
            'count_begin'=>$count_begin,
            'count_end'=>$count_end,
            'parameters'=>$array_request_parameters
        );
    }

    private function selectGoogleDirectoryUser($parameters = array()){

        if(count($parameters)>0){
            if($parameters['Mode']=='ALL'){
                $parameters2 = array(
                    'customer' => 'my_customer',
                    'orderBy' => 'email',
                    'maxResults' => 500,
                );

                $results = $this->service->users->listUsers($parameters2);
                return $results->getUsers();

            }elseif($parameters['Mode']=='BY_EMAIL'){
                $user = $this->service->users->get($parameters['PrimaryEmail']);
                return array($user);
            }

        }else{
            return array();
        }
    }

    private function updateGoogleDirectoryUser($parameters = array()){

        if(count($parameters)>0){
            $this->service->users->update($parameters['User']['id'],$parameters['User']);
            return true;
        }else{
            return false;
        }
    }

    private function deleteGoogleDirectoryUser($parameters = array()){ //Ammended so to not accidentally delete users in the future CC on Authorisation of JL

        if(count($parameters)>0){
            //$this->service->users->delete($parameters['User']['id']);
            return true;
        }else{
            return false;
        }
    }
    //End of user accounts function

    //---------------------------------------

    //group functions

    private function insertGoogleDirectoryGroup($parameters = array()){

        $group = new Google_Service_Directory_Group();
        $group->setEmail($parameters['email']);
        $group->setName($parameters['name']);
        $group->setDescription($parameters['description']);

        $this->service->groups->insert($group);
    }

    private function batchInsertGoogleDirectoryGroup($array_parameters = array()){
        $count_begin = $this->batch_item_count;
        $array_request_parameters=array();

        foreach($array_parameters as $parameters){
            if(count($parameters)>0){
                $group = new Google_Service_Directory_Group();
                $group->setEmail($parameters['email']);
                $group->setName($parameters['name']);
                $group->setDescription($parameters['description']);

                $request = $this->service->groups->insert($group);

                $this->batch->add($request, $this->batch_item_count);
                $array_request_parameters[$this->batch_item_count]=$parameters;
                $this->batch_item_count++;
            }
        }
        $count_end = $this->batch_item_count-1;
        return array(
            'count_begin'=>$count_begin,
            'count_end'=>$count_end,
            'parameters'=>$array_request_parameters
        );
    }

    private function selectGoogleDirectoryGroup($parameters = array()){

        if(count($parameters)>0){
            if($parameters['Mode']=='ALL'){
                $parameters2 = array(
                    'customer' => 'my_customer',
                    'maxResults' => 500,
                );

                $results = $this->service->groups->listGroups($parameters2);
                return $results->getGroups();

            }elseif($parameters['Mode']=='BY_EMAIL'){
                $group = $this->service->groups->get($parameters['GroupEmail']);
                return array($group);
            }elseif($parameters['Mode']=='BY_PRIMARY_EMAIL'){
                $parameters2 = array(
                    'maxResults' => 500,
                    'userKey'=>$parameters['PrimaryEmail'],
                );

                $results = $this->service->groups->listGroups($parameters2);
                return $results->getGroups();
            }

        }else{
            return array();
        }
    }

    private function updateGoogleDirectoryGroup($parameters = array()){
        if(count($parameters)>0){
            $this->service->groups->update($parameters['Group']['id'],$parameters['Group']);
            return true;
        }else{
            return false;
        }
    }

    private function deleteGoogleDirectoryGroup($parameters = array()){

        if(count($parameters)>0){
            $this->service->groups->delete($parameters['GroupEmail']);
            return true;
        }else{
            return false;
        }
    }

    private function insertGoogleDirectoryGroupMember($parameters = array()){

        $member = new Google_Service_Directory_Member();
        $member->setEmail($parameters['email']);
        $member->setRole('MEMBER');

        $this->service->members->insert($parameters['group_email'], $member);
    }

    private function batchInsertGoogleDirectoryGroupMember($array_parameters = array()){
        $count_begin = $this->batch_item_count;
        $array_request_parameters=array();

        foreach($array_parameters as $parameters){
            if(count($parameters)>0){
                $member = new Google_Service_Directory_Member();
                $member->setEmail($parameters['email']);
                $member->setRole('MEMBER');

                $request = $this->service->members->insert($parameters['group_email'], $member);

                $this->batch->add($request, $this->batch_item_count);
                $array_request_parameters[$this->batch_item_count]=$parameters;
                $this->batch_item_count++;
            }
        }
        $count_end = $this->batch_item_count-1;
        return array(
            'count_begin'=>$count_begin,
            'count_end'=>$count_end,
            'parameters'=>$array_request_parameters
        );
    }

    private function selectGoogleDirectoryGroupMember($parameters = array()){

        if(count($parameters)>0){
            if($parameters['Mode']=='ALL'){
                $parameters2 = array(
                    'roles'=>'MEMBER',
                    'maxResults' => 500,
                );

                $results = $this->service->members->listMembers($parameters['GroupEmail'],$parameters2);
                return $results->getMembers();

            }elseif($parameters['Mode']=='BY_EMAIL'){
                $member = $this->service->members->get($parameters['GroupEmail'],$parameters['PrimaryEmail']);
                return array($member);
            }

        }else{
            return array();
        }
    }

    private function updateGoogleDirectoryGroupMember(){

    }

    private function deleteGoogleDirectoryGroupMember($parameters = array()){

        if(count($parameters)>0){
            $this->service->members->delete($parameters['GroupEmail'],$parameters['PrimaryEmail']);
            return true;
        }else{
            return false;
        }
    }

    //End of group functions

    //---------------------------------------

    private function initialize(){
        global $intranet_root;
        global $ssoservice;

        $this->application_name = $ssoservice["Google"]['api'][$this->config_index]['application_name'];
        $this->client_secret_path = $ssoservice["Google"]['api'][$this->config_index]['client_secret_path'];
        $this->domain = $ssoservice["Google"]['api'][$this->config_index]['domain'];
        $this->soft_delete = isset($ssoservice["Google"]['api'][$this->config_index]['not_soft_delete'])?$ssoservice["Google"]['api'][$this->config_index]['not_soft_delete']==false:true;

        $this->scopes = '';
        if($ssoservice["Google"]["service_directory"]["user"]["is_readonly"]){
            $this->scopes .= ' '. Google_Service_Directory::ADMIN_DIRECTORY_USER_READONLY;
        }else{
            $this->scopes .= ' '. Google_Service_Directory::ADMIN_DIRECTORY_USER;
        }

        if($ssoservice["Google"]['mode']['user_and_group']){
            if($ssoservice["Google"]["service_directory"]["group"]["is_readonly"]){
                $this->scopes .= ' '. Google_Service_Directory::ADMIN_DIRECTORY_GROUP_READONLY;
            }else{
                $this->scopes .= ' '. Google_Service_Directory::ADMIN_DIRECTORY_GROUP;
            }
        }
        $this->scopes = trim($this->scopes);

        $this->client = new Google_Client();
        $this->client->setApplicationName($this->application_name);
        $this->client->setScopes($this->scopes);
        $this->client->setAuthConfig($this->client_secret_path);
        $this->client->setAccessType('offline');
        $this->client->setSubject($ssoservice["Google"]['api'][$this->config_index]['project_holder']); # script need to run as a admin user

        $this->service = new Google_Service_Directory($this->client);
    }
}

?>