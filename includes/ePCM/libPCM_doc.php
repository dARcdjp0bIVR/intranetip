<?php
class libPCM_doc
{
	public function __construct()
	{

	}

	public function getWordDocument($html,$filename){
		$ContentType = "application/x-msword";
		$tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body");
		foreach ($tags_to_strip as $tag)
		{
			$report_html = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", "", $html);
		}
		$stylesheet = $this->returnStyleSheet();
		
		$output = "<HTML xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\">\n";
		$output .= "<HEAD>\n";
		$output .= returnHtmlMETA();
		$output .= $stylesheet;
		$output .= "</HEAD>\n";
		$output .= "<BODY LANG=\"zh-HK\">\n";
		$output .= $html;
		$output .= "</BODY>\n";
		$output .= "</HTML>\n";
		
		
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		 
		header('Content-type: '.$ContentType);
		header('Content-Length: '.strlen($output));
		 
		header('Content-Disposition: attachment; filename="'.$filename.'";');
		
		echo $output;
	}
	public function returnStyleSheet(){
		$style = '<style>
					@page {size: A4 portrait; margin: 0 0 0 0;}
					@media print
						{
						  .signTable 		{ page-break-after:auto }
						  .signTable tr    { page-break-inside:avoid; page-break-after:auto;}
						  .signTable td    { page-break-inside:avoid; page-break-after:auto;}
						}
					body { padding:0; margin:0; font-family: "Times New Roman","新細明體",Arial;font-size:22px;}
					table {width:100%;}
					caption {text-align: left;}
					.text_center { text-align: center;}
					
					.small {font-size:18px;}
					td.border ,.border td,.border th{border:1px solid black; font-weight:normal; border-collapse:collapse;}
					.no_border,.no_border td,table.no_border,.no_border tr, .no_border th {border:0px;}
					.underlined {border-bottom: 1px solid black !important;}
					
				</style>';
	
		return $style;
	}
}

?>