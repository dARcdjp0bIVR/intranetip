<?php 
require_once 'Validator.php';
class ValidationHelper extends Validator{
	var $data;
	var $mapper;
	var $rules;
	var $models;
	var $dbConn;
	
	public function __construct($data, $mapper, $rules, $dbConn){
		parent::__construct();
		
		if(!is_array($data)){
			throw new exception('data must be an array');
		}
		if(!is_array($mapper)){
			throw new exception('mapper must be an array');
		}
		if(count($data[0]) != count($mapper)){
			throw new exception('number of columns  != number of mappers');
		}
		
		$this->data = $data;
		$this->mapper = $mapper;
		$this->dbConn = $dbConn;
		$this->translateRules($rules);
		$this->prepareModels();
	}
	private function arrayDepth($array){
		if(is_array($array)){
			$max_depth = 1;
			foreach ($array as $value) {
		        if (is_array($value)) {
		            $depth = $this->arrayDepth($value) + 1;
		
		            if ($depth > $max_depth) {
		                $max_depth = $depth;
		            }
		        }
		    }
		}else{
			$max_depth = 0;
		}
		return $max_depth;
	}
	private function translateRules($ruleAssocArr){
		$this->models = array();
		$translatedRules = array();
		foreach($ruleAssocArr as $_key => $_rules){
			## translate rules from string
			$_thisRules = explode('|', $_rules);
			$translatedRules[$_key] = $_thisRules;
			
			## check if the rule need model
			foreach($_thisRules as $__ruleAndParms){
				## use , to separate rule & parms
				$__ruleAndParmsArr = explode(',', $__ruleAndParms);
				$__rule = $__ruleAndParmsArr[0];
				array_shift($__ruleAndParmsArr); # first is rule
				$__parmsArr = $__ruleAndParmsArr;
				unset($__ruleAndParmsArr);
				
				## check if seperator : exist
				$pos = strpos($__rule,':');
				if($pos > -1){
					$model = substr($__rule, $pos+1);
					$this->models[$model] = -1;
					if($pos == 0){
						throw new exception('please specify a function for the model: '.$model);
					}
				}
			}
		}
		$this->rules = $translatedRules;
	}
	private function prepareModels(){
		## base on translation to prepare model
		foreach($this->models as $_model => &$_modelObj){
			## status -1 = undefined
			if($_modelObj == -1){
				$modelName = $_model.'Model';
				require_once 'models/'.$modelName.'.php';
				$_modelObj = new $modelName($this->dbConn);
			}
		}
	}

	private $processedData;
	public function getProcessedData(){
		return $this->processedData;
	}
	private $processingRow;
	public function process(){
		$this->processedData = array();
		$rows = $this->data;
		$result = array();
		foreach($rows as $_key => $_row){
			$this->processingRow = $_key;
			$_valid = $this->validate( $_row);
			// for debug
// 			$debug = $this->validateResult;
// 			debug_pr($debug);
			if(!$_valid){
				$result[$_key] = $this->validateError;
			}
			$this->processedData[$_key] = $this->processedRow;
		}
		return $result;
	}
	
	var $validateResult;
	var $validateError;
	private $processedRow;
	private function validate($row){
		$this->validateResult = array();
		$this->validateError = array();
		$this->processedRow = array();
		$rules = $this->rules;
		$mapper = $this->mapper;
		
		foreach($row as $_key => $_value){
			$_rules = $rules[$mapper[$_key]];
			if(count($_rules) > 0){
				foreach($_rules as $__ruleAndParms){
					if($__ruleAndParms != ''){
						## use , to separate rule & parms
						$__ruleAndParmsArr = explode(',', $__ruleAndParms);
						$__rule = $__ruleAndParmsArr[0];
						array_shift($__ruleAndParmsArr); # first is rule
						$__tempParmsArr = $__ruleAndParmsArr;
						$__parmsArr = $this->translateParms($__tempParmsArr);
						unset($__ruleAndParmsArr);
						
						if(strpos($__rule,':') > -1){
							## call model to validate
							$__ruleInfo = explode(':', $__rule);
							$__method = 'validate_'.$__ruleInfo[0];
							$__model = $__ruleInfo[1];
							$__parms = array($_value);
							$__parms = array_merge($__parms , $__parmsArr);
							$__result = call_user_func_array(array($this->models[$__model],$__method), $__parms);
						}else{
							## call Validator to validate
							$__method = $__rule;
							$__parms = array($_value);
							$__parms = array_merge($__parms , $__parmsArr);
							$__result = call_user_func_array(array($this,$__rule), $__parms);
						}
		
						$__valid = $__result['valid'];
						$this->validateResult[$_key][$__rule] = $__valid;
						$resultArr[] = $__valid;
						if(isset($__result['mapTo']) && $__result['mapTo'] != ''){
							// overwrite by the last rule return mapTo
							$this->processedRow[$_key] = $__result['mapTo'];
						}
						if(!$__valid){
							$this->validateError[$_key][] =  $__result['error'];
						}
					}else{
						// empty rule always return valid
						$resultArr[] = $__valid;
					}
				}
			}else{
				// undefined rule always return valid
				$resultArr[] = $__valid;
			}
		}
		if(!in_array(false, $resultArr)){
			return true;
		}else{
			return false;
		}
	}
	
	private function translateParms($parms){
		$resultArr = array();
		if(!empty($parms)){
			foreach($parms as $_parm){
				$_parmInArr = str_split($_parm);
				if($_parmInArr[0] == '&'){
					# get value from other column as a parm for validation
					array_shift($_parmInArr); # remove &
					$_translatedParmName = implode('',$_parmInArr);
					$_parm = $this->getParmValue($_translatedParmName);
					$resultArr[] = $_parm;
				}else{
					$resultArr[] = $_parm;
				}
			}
		}
		return $resultArr;
	}
	
	private function getParmValue($parmName){
		$mapper = array_flip($this->mapper);
		$parmKey = $mapper[$parmName];
		return $this->data[$this->processingRow][$parmKey];
	}
}