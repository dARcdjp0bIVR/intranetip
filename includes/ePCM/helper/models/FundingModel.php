<?php 
include_once "importHelperModel.php";
class FundingModel implements importHelperModel{
	var $db; 
	
	public function __construct($db){
		$this->db = $db;
	}
	public function getExportData(){
		global $Lang;
		$sql = "Select 
					FundingNameEN,
					FundingNameCHI,
					FundingCategoryNameEN, 
					FundingCategoryNameCHI,
					Budget,
					RecordStatus,
					DeadLineDate
				FROM 
					INTRANET_PCM_FUNDING_SOURCE as funding
					INNER JOIN INTRANET_PCM_FUNDING_CATEGORY as fundingCat on funding.FundingCategoryID = fundingCat.FundingCategoryID AND fundingCat.IsDeleted  = '0'
				WHERE 
					funding.IsDeleted  = '0'
				ORDER BY 
					fundingCat.FundingCategoryNameEN , funding.DeadLineDate desc";
		$fundingArr = $this->db->returnResultSet($sql);
		$dataArr = array();
		if(!empty($fundingArr)){
			foreach($fundingArr as $_funding){
				$_temp = array();
				$_temp[0] = Get_Lang_Selection($_funding['FundingCategoryNameCHI'], $_funding['FundingCategoryNameEN']);
				$_temp[1] = $_funding['FundingNameEN'];
				$_temp[2] = $_funding['FundingNameCHI'];
				$_temp[3] = $_funding['Budget'];
				$_temp[4] = $Lang['ePCM']['FundingSource']['RecordStatus'][$_funding['RecordStatus']];
				$_temp[5] = $_funding['DeadLineDate'];
				$dataArr[] = $_temp;
			}
		}
		return $dataArr;
	}
	public function insertToDb($data){
		
		$tablename = "INTRANET_PCM_FUNDING_SOURCE";
		$fieldName = array('FundingCategoryID','FundingNameCHI','FundingNameEN','Budget','RecordStatus','DeadLineDate','DisplayOrder');
		$displayOrder = $this->db->getFundingLastestDisplayOrder();
		
		$successCount = 0;
		foreach($data as $_rowNum => &$_rowArr){
			$displayOrder++;
			$_rowArr[] = $displayOrder;
			$_rowArr[5] = date("Y-m-d",strtotime($_rowArr[5]));
		}
		$success = $this->db->insertData($tablename,$fieldName,$data);
		return $this->db->db_affected_rows();
	}
	
	public function getRefContent($refType){
		if($refType == 'fundingcat'){
			$field = Get_Lang_Selection("IF(FundingCategoryNameCHI != '', FundingCategoryNameCHI, FundingCategoryNameEN) as name" ,"FundingCategoryNameEN as name");
			$sql = "SELECT FundingCategoryID, $field  FROM INTRANET_PCM_FUNDING_CATEGORY WHERE isDeleted = 0";
			$result = $this->db->returnResultSet($sql);
		}
		return $result;
	}
	
	public function validate_existedFundingCat($value){
		global $Lang;
		
		$matchData = $this->db->getFundingCategoryRawData($value);
		$valid = !empty($matchData) && count($matchData) == 1;
		
		if(!$valid){
			$error = $Lang['ePCM']['FundingSource']['FundingCatIncorrect'];
		}else{
			reset($matchData);
			$first_key = key($matchData);
			$mapTo = Get_Lang_Selection($matchData[$first_key]['FundingCategoryNameCHI'], $matchData[$first_key]['FundingCategoryNameEN']);
		}
		return array('valid' => $valid, 'error' => $error, 'mapTo' => $mapTo);
	}
	
	public function validate_recordStatus($value){
		
		if(trim($value) != ''){
			global $Lang;
			
			$validValue = array(1,2);
			$valid = in_array($value, $validValue)? true : false;
			if(!$valid){
				$error = $Lang['ePCM']['FundingSource']['RecordStatusIncorrect'];
			}else{
				$mapTo = $Lang['ePCM']['FundingSource']['RecordStatus'][$value];
			}
		}else{
			$valid = false;
		}
		return array('valid' => $valid, 'error' => $error, 'mapTo' => $mapTo);
	}
}