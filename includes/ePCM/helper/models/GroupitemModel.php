<?php 
include_once "importHelperModel.php";
class GroupitemModel implements importHelperModel{
	var $db; 
	
	public function __construct($db){
		$this->db = $db;
	}
	public function insertToDb($data){
		
		$tablename = "INTRANET_PCM_GROUP_FINANCIAL_ITEM";
		$fieldName = array('Code','GroupID','ItemName','ItemNameChi');
		
		$successCount = 0;
		
		foreach($data as $_rowNum => $_rowArr){
			# group item
			$groupItem[$_rowNum] = $_rowArr;
			$sql = " select GroupID from INTRANET_PCM_GROUP where Code = '$_rowArr[0]' and IsDeleted = 0 ";
			$groupId = $this->db->returnVector($sql);
			$groupItem[$_rowNum][0] = $_rowArr[1];
			$groupItem[$_rowNum][1] = $groupId[0];
		}
		$success = $this->db->insertData($tablename,$fieldName,$groupItem);
		return $this->db->db_affected_rows();
	}
	public function getExportData($keyword){
		
		$dataArr = array();
		$_temp = array();

        if($keyword!==''&&$keyword!==NULL){
            $conds = " AND 
						(
							ipg.Code Like '%".$this->db->Get_Safe_Sql_Like_Query($keyword)."%'
							OR ipg.GroupName Like '%".$this->db->Get_Safe_Sql_Like_Query($keyword)."%'
							OR ipg.GroupNameChi Like '%".$this->db->Get_Safe_Sql_Like_Query($keyword)."%'
						)			";
        }

        $sort = "ORDER BY ipg.Code Asc";

		$sql = "SELECT ipg.Code as GroupCode, ipgfi.Code, ipgfi.ItemName, ipgfi.ItemNameChi
		FROM
			INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipgfi
		INNER JOIN
			INTRANET_PCM_GROUP AS ipg
			ON ipgfi.GroupID = ipg.GroupID
		WHERE
			ipgfi.isDeleted = 0 AND ipg.IsDeleted = 0 $conds
		$sort";
		$groupItemArr = $this->db->returnResultSet($sql);
		
		foreach($groupItemArr as $groupItem){
			$_temp[0] = $groupItem['GroupCode'];
			$_temp[1] = $groupItem['Code'];
			$_temp[2] = $groupItem['ItemName'];
			$_temp[3] = $groupItem['ItemNameChi'];
			$dataArr[] = $_temp;
		}

		return $dataArr;
	}
	
	public function getRefContent($refType){
		if($refType == 'groupCode'){
			$field = Get_Lang_Selection("IF(GroupNameChi != '', GroupNameChi, GroupName) as name" ,"GroupName as name");
			$sql = "SELECT Code, $field FROM INTRANET_PCM_GROUP WHERE isDeleted = 0";
			$result = $this->db->returnResultSet($sql);
		}
		return $result;
	}
	
	public function validate_itemExisted($value, $groupCode){
		global $Lang;
		$valid = true;
		
		$sql = "SELECT count(*) as count 
		FROM INTRANET_PCM_GROUP_FINANCIAL_ITEM as groupitem 
		INNER JOIN INTRANET_PCM_GROUP as pcmgroup ON groupitem.GroupID = pcmgroup.GroupID 
		WHERE groupitem.Code = '$value' AND pcmgroup.Code = '$groupCode' AND groupitem.isDeleted = 0 AND pcmgroup.isDeleted = 0";
		$data = $this->db->returnResultSet($sql);
		
		if($data[0]['count'] != 1){
			$error = $Lang['ePCM']['Group']['ItemNotExist'];
			$valid = false;
		}
		
		return array('valid' => $valid, 'error' => $error);
	}
	
	public function validate_unique($value){
		
		global $Lang;
		
		
		$table = 'INTRANET_PCM_GROUP_FINANCIAL_ITEM';
		$field = 'Code';
		$excluded_value = '';
		$valid = $this->db->checkDuplicate($table,$field,$value,$excluded_value);
		
		if(!$valid){
			$error = $Lang['ePCM']['Group']['Item']['DuplicatedCode'];
		}
		return array('valid' => $valid, 'error' => $error);
	}
}