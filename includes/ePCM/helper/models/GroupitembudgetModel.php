<?php 
include_once "importHelperModel.php";
class GroupitembudgetModel implements importHelperModel{
	var $db; 
	
	public function __construct($db){
		$this->db = $db;
	}
	public function insertToDb($data){
		
		$successCount = 0;
		
		$settings = $this->db->getGeneralSettings();
		$ayId = isset($settings['defaultAcademicYearID'])? $settings['defaultAcademicYearID'] : Get_Current_Academic_Year_ID();
		
		$tempArrForGroupId = array(); 
		$tempArrForGroupBudget = array();
		$itemBudgetDataArr = array();
		foreach($data as $_row){
			$_groupCode = $_row[0];
			
			// group id
			if(!isset($tempArrForGroupId[$_groupCode])){
				$sql = " select GroupID from INTRANET_PCM_GROUP where Code = '$_groupCode' and IsDeleted = 0 ";
				$_groupId = $this->db->returnVector($sql);
				$_groupId = $_groupId[0];
				$tempArrForGroupId[$_groupCode] = $_groupId;
			}else{
				$_groupId = $tempArrForGroupId[$_groupCode];
			}
			
			// group item id 
			$sql = " select ItemID from INTRANET_PCM_GROUP_FINANCIAL_ITEM where GroupID = '$_groupId[0]' AND Code = '$_row[1]' and IsDeleted = 0 ";
			$_itemId = $this->db->returnVector($sql);
			$_itemId = $_itemId[0];
			
			// count group budget total
			if(!isset($tempArrForGroupBudget[$_groupId])){
				$tempArrForGroupBudget[$_groupId] = 0;
			}
			$tempArrForGroupBudget[$_groupId] += $_row[2];
			
			// clear previous budget
			$sql = "UPDATE INTRANET_PCM_FINANCIAL_ITEM_BUDGET SET IsDeleted = 1, DateModified = now(), ModifiedBy = '{$_SESSION['UserID']}' 
					WHERE ItemID = '$_itemId' AND AcademicYearID = '$ayId'";
			$this->db->db_db_query($sql);
			$itemBudgetDataArr[] = array($_itemId, $ayId, 0, $_row[2], $_row[3]);
		}

		// Group Item Budget
		$tableName = "INTRANET_PCM_FINANCIAL_ITEM_BUDGET";
		$fieldName = array('ItemID','AcademicYearID','TermID','Budget', 'FundingID');
		$success = $this->db->insertData($tableName, $fieldName, $itemBudgetDataArr);
		$itemCount = $this->db->db_affected_rows();

		foreach($tempArrForGroupId as $_groupCode => $_groupId){
			// clear previous budget
			$sql = "UPDATE INTRANET_PCM_GROUP_BUDGET SET IsDeleted = 1, DateModified = now(), ModifiedBy = '{$_SESSION['UserID']}'
			WHERE GroupID = '$_groupId' AND AcademicYearID = '$ayId'";
			$this->db->db_db_query($sql);
			$groupBudgetDataArr[] = array($_groupId, $ayId, 0, $tempArrForGroupBudget[$_groupId]);
		}

		// Group Budget
		$tableName = "INTRANET_PCM_GROUP_BUDGET";
		// clear previous budget
		$fieldName = array('GroupID','AcademicYearID','TermID','Budget');
		$success = $this->db->insertData($tableName, $fieldName, $groupBudgetDataArr);
		$groupCount = $this->db->db_affected_rows();
		
		if($itemCount == count($data) && $groupCount == count($tempArrForGroupId)){
			$successCount = $itemCount;
		}
		return $successCount;
	}
	public function getExportData($keyword){
		$dataArr = array();
		$_temp = array();
		
		$sql = "SELECT GroupID FROM INTRANET_PCM_GROUP WHERE IsDeleted = 0";
		$groupIdArr = $this->db->returnVector($sql);

        if($keyword!==''&&$keyword!==NULL){
            $conds = " AND 
						(
							ipg.Code Like '%".$this->db->Get_Safe_Sql_Like_Query($keyword)."%'
							OR ipg.GroupName Like '%".$this->db->Get_Safe_Sql_Like_Query($keyword)."%'
							OR ipg.GroupNameChi Like '%".$this->db->Get_Safe_Sql_Like_Query($keyword)."%'
						)			";
        }
        $sort = "ORDER BY ipg.Code Asc";
		foreach($groupIdArr as $groupId){
			$sql = "SELECT 	ipg.Code as GroupCode, 
							ipgfi.Code as Code, 
							ipfib.Budget as Budget, 
							ipfib.FundingID as FundingID,
							ipfs.FundingNameEN ,
							ipfs.FundingNameCHI
					FROM
						INTRANET_PCM_GROUP_FINANCIAL_ITEM AS ipgfi
					INNER JOIN
						INTRANET_PCM_GROUP AS ipg
					ON ipgfi.GroupID = ipg.GroupID
					INNER JOIN
						INTRANET_PCM_FINANCIAL_ITEM_BUDGET AS ipfib
					ON ipfib.ItemID = ipgfi.ItemID
					LEFT JOIN
						INTRANET_PCM_FUNDING_SOURCE as ipfs
					ON ipfs.FundingID = ipfib.FundingID
					WHERE
					ipgfi.isDeleted = 0 AND ipgfi.GroupID = '$groupId' $conds 
					$sort";

			$groupItemArr = $this->db->returnResultSet($sql);
			foreach($groupItemArr as $groupItem){
				$_temp[0] = $groupItem['GroupCode'];
				$_temp[1] = $groupItem['Code'];
				$_temp[2] = $groupItem['Budget'];
				$_temp[3] = Get_Lang_Selection($groupItem['FundingNameCHI'], $groupItem['FundingNameEN']);
				$dataArr[] = $_temp;
			}
		}
		return $dataArr;
	}
	
	public  function getRefContent($refType){
		if($refType == 'departmentOrGroup'){
			$field = Get_Lang_Selection("IF(GroupNameChi != '', GroupNameChi, GroupName) as name" ,"GroupName as name");
			$sql = "SELECT Code, $field FROM INTRANET_PCM_GROUP WHERE isDeleted = 0";
			$result = $this->db->returnResultSet($sql);
		}
		if($refType == 'financialItemBudget'){
			$groupNamefield = Get_Lang_Selection("IF(ipg.GroupNameChi != '', ipg.GroupNameChi, ipg.GroupName) as GroupName" ,"ipg.GroupName as GroupName");
			$itemNamefield = Get_Lang_Selection("IF(ipgfi.ItemNameChi != '', ipgfi.ItemNameChi, ipgfi.ItemName) as ItemName" ,"ipgfi.ItemName as ItemName");
			$sql = "SELECT 
						ipg.Code as GroupCode, 
						$groupNamefield, 
						ipgfi.Code as ItemCode, 
						$itemNamefield  
					FROM 
						INTRANET_PCM_GROUP as ipg 
					INNER JOIN 
						INTRANET_PCM_GROUP_FINANCIAL_ITEM  as ipgfi
					ON ipg.GroupID = ipgfi.GroupID
					WHERE ipg.IsDeleted = 0 AND ipgfi.IsDeleted = 0
					";
						//debug_pr($sql);
			$result = $this->db->returnResultSet($sql);
		}
		if($refType == 'fundingSource'){
			$data = $this->db->getFundingSourceTableData();

			$result = array();
			if(!empty($data)){
				foreach($data as $_fundingCatInfo){
					$_fundings = $_fundingCatInfo['Funding'];
					if(!empty($_fundings)){
						foreach($_fundings as $__fundingInfo){
							$result[] = array( $__fundingInfo['FundingID'], $__fundingInfo['FundingNameEN'], $_fundingCatInfo['FundingCategoryNameEN'] );
						}
					}
				}
			}
			
		}
		return $result;
	}
	public function validate_existedFunding($value){
		global $Lang;
		$valid = true;
		
		if($value != ''){
			$sql = " select FundingNameCHI, FundingNameEN from INTRANET_PCM_FUNDING_SOURCE where FundingID = '$value' and isDeleted = 0";
			$data = $this->db->returnResultSet($sql);
			
			if(count($data) != 1){
				$error = $Lang['ePCM']['FundingSource']['FundingIncorrect'];
				$valid = false;
			}else{
				$mapTo = Get_Lang_Selection($data[0]['FundingNameCHI'], $data[0]['FundingNameEN']);
			}
		}else{
			$mapTo = Get_String_Display('');
		}
		return array('valid' => $valid, 'error' => $error, 'mapTo' => $mapTo);
	}
}