<?php 
include_once "importHelperModel.php";
class GroupModel implements importHelperModel{
	var $db; 
	
	public function __construct($db){
		$this->db = $db;
	}
	public function insertToDb($data){
		global $sys_custom ;
		$tablename = "INTRANET_PCM_GROUP";
		$fieldName = array('Code','GroupName','GroupNameChi');
		$userTable = $sys_custom['INTRANET_USER'];

		$successCount = 0;
		
		foreach($data as $_rowNum => $_rowArr){
			$group = array();
			$group_member = array();
			
			# group			
			$group[$_rowNum] = array_slice($_rowArr, 0, 3);
			$success = $this->db->insertData($tablename,$fieldName,$group);
			$groupId = $this->db->db_insert_id();
			
			$group_member[$_rowNum] = array_slice($_rowArr, 3, 2);
			$group_head[$_rowNum] = array_slice($_rowArr, 3, 1);
			
			# group member
			$new_gp_member = implode(',', $group_member[$_rowNum]);
			$new_gp_member = preg_replace('/\s*/', '', $new_gp_member);
			$new_gp_member = strtolower($new_gp_member);
			
			$explode_new_gp_member = explode(',', $new_gp_member);
			$implode_group_member = implode("','", $explode_new_gp_member);
			
			$sql = " select UserID from INTRANET_USER where UserLogin in ( '".$implode_group_member."' ) ";
			$userId_ary_member = $this->db->returnVector($sql);

			$inserted_member_row = $this->db->insertGroupMember($userId_ary_member, $groupId);
			$insertMemberSuccess = $inserted_member_row == count($userId_ary_member);
			
			# group head
			$temp_head_arr = $group_head[$_rowNum][0];
			
			$new_head_arr = explode(',', $temp_head_arr);
			
			$implode_head = implode("','", $new_head_arr);
			$implode_head = preg_replace('/\s*/', '', $implode_head);
			$implode_head = strtolower($implode_head);
			
			$sql = " select UserID from INTRANET_USER where UserLogin in ( '".$implode_head."' ) ";
			$userId_ary_head = $this->db->returnVector($sql);
			
			$get_group_member_id_Arr = $this->db->GetGroupMemberIdArr($groupId, $userId_ary_head);
			$this->db->GroupMemberAssignAdmin($get_group_member_id_Arr, 1);
			$updateSuccess = $this->db->db_affected_rows() == count($get_group_member_id_Arr);
			
			if( $success == 1 && $insertMemberSuccess && $updateSuccess ){
				$successCount++;
			}
		}
		
		return $successCount;
	}
	public function getExportData($keyword){

	    global $sys_custom;

		$groupArr = $this->db->getGroupInfo(array(),$keyword);
        $userTable = $sys_custom['INTRANET_USER'];
		
		$dataArr = array();
		$groupMemberArr = array();
		if(!empty($groupArr)){
			foreach($groupArr as $_group){
				
				$_temp = array();
				$_temp[0] = $_group['Code'];
				$_temp[1] = $_group['GroupName'];
				$_temp[2] = $_group['GroupNameChi'];
				
				$groupId = $_group['GroupID'];
				
				$field = Get_Lang_Selection("if(iu.ChineseName != '', iu.ChineseName, iu.EnglishName) as name" ,"iu.EnglishName as name");
				
				# getGroupHeadInfo
				$sql = " select $field
						from
							INTRANET_PCM_GROUP_MEMBER as ipgm
						inner join
							$userTable as iu
						on ipgm.UserID = iu.UserID
						where
							ipgm.GroupID = '$groupId'
							and ipgm.IsGroupHead = 1
						";
				$groupHeadArr = $this->db->returnVector($sql);
				$implodeGroupHead = implode(', ', $groupHeadArr);
				
				# getGroupMemberInfo
				$sql = " select $field
						from
							INTRANET_PCM_GROUP_MEMBER as ipgm
						inner join
							$userTable as iu
						on ipgm.UserID = iu.UserID
						where
							ipgm.GroupID = '$groupId'
						";
				
				$groupMemberArr =  $this->db->returnVector($sql);
				$implode_groupMember = implode(', ',$groupMemberArr);
				
				$_temp[3] = $implodeGroupHead;
				$_temp[4] = $implode_groupMember;
				$dataArr[] = $_temp;
			}
		}
		return $dataArr;
	}
	public function getRefContent($refType){
		
	}
	public function validate_existedUser($value){
		global $Lang;
		$valid = true;
		
		if($value != ''){
			$val = $value;
			$val = explode(',',$val);
			
			foreach($val as $var){
				$var = preg_replace('/\s*/', '', $var);
				$var = strtolower($var);
				$sql = "select count(*) as count, Max(UserLogin) as UserLogin, Max(RecordType) as RecordType from INTRANET_USER where UserLogin = '$var' ";
				$data = $this->db->returnResultSet($sql);

				if($data[0]['count'] == 0){
					$error = $var . '&nbsp;' . $Lang['ePCM']['Group']['GroupHeadIncorrect'];
					$valid = false;
				}
				
				if($data[0]['RecordType'] != 1){
					$error = $var . '&nbsp;' . $Lang['ePCM']['Group']['NotATeacher'];
					$valid = false;
				}
			}
		}
		return array('valid' => $valid, 'error' => $error);
		
	}
	public function validate_existed($value){
		global $Lang;
		$valid = true;
		
		$value= preg_replace('/\s*/', '', $value);
		
		$sql = " select count(*) as count from INTRANET_PCM_GROUP where Code = '$value' and IsDeleted = 0 ";
		$data = $this->db->returnResultSet($sql);
	
		if($data[0]['count'] == 0){
			$error = $Lang['ePCM']['Group']['GroupNotExist'];
			$valid = false;
		}
	
		return array('valid' => $valid, 'error' => $error);
	
	}
	public function validate_unique($value){
		
		global $Lang;
		
		$value= preg_replace('/\s*/', '', $value);
		
		$table = 'INTRANET_PCM_GROUP';
		$field = 'Code';
		$excluded_value = '';
		$valid = $this->db->checkDuplicate($table,$field,$value,$excluded_value);
		
		if(!$valid){
			$error = $Lang['ePCM']['Group']['Duplicated code'];
		}
		return array('valid' => $valid, 'error' => $error);
	}
}