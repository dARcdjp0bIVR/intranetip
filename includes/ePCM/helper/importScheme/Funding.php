<?php 
include_once "importScheme.php";
class Funding implements importScheme{
	
	public function __construct(){
		
	}
	### For Import Template (Start) ###
	public function getHeaderTemplate(){
		$header = array();
		$header['En'] = array('Funding Source Category','English Name','Chinese Name', 'Amount', 'Record Status', 'Expiry Date');
		$header['Ch'] = array('資金來源類別','中文名稱','英文名稱', '金額', '狀況', '到期日');
		return $header;
	}
	
	public function getHeaderProperty(){
		// 1 = complusory , 2 =ref , 3 = optional
		// pls set all 1, otherwise will not able to extract the value
		return array(1,1,1,1,1,1);
	}
	
	public function getContentTemplate(){
		$contentArr = array();
		$contentArr[] = array('1', '持續進修基金2017', 'CEF2017', '1000000', '1', '2017-10-31');
		return $contentArr;
	}
	### For Import Template (End) ###
	
	### For All Step (Start) ###
	public function setCurrentSectionAndPage(){
		global $_PAGE;
		
		$_PAGE['CurrentSection'] = 'Settings';
		$_PAGE['CurrentPage'] = 'Funding';
	}
	public function getNavigationAry(){
		global $Lang;
		
		$originPath = $this->getOriginPath();
		
		$navigationAry = array();
		$navigationAry[] = array($Lang['ePCM']['FundingSource']['FundingSource'], 'javascript: window.location=\'index.php?p='.$originPath.'\';');
		$navigationAry[] = array($Lang['Btn']['Import']);
		return $navigationAry;
	}
	public function getOriginPath(){
		return 'setting.funding';
	}
	public function getTempFilePath(){
		global $intranet_root, $ePCMcfg;
		return $intranet_root.'/'.$ePCMcfg['INTERNATDATA'].'temp/import/funding/';
	}
	### For All Step (End) ###
	
	### For Import Step 1 (Start) ###
	public function getColumnTitleArr(){
		global $Lang;
		
		$ColumnTitleArr = array();
		$mapping = $this->getContentMapping();
		$desc = $this->getContentDescripiton();
		foreach($mapping as $_key){
			$ColumnTitleArr[] = $desc[$_key];
		}
		return $ColumnTitleArr; 
	}
	public function getColumnPropertyArr(){
		return array(1,1,1,1,0,0);
	}
	public function getRemarksArr(){
		global $Lang;
		return array(
				'<a id="fundingcat" class="tablelink" href="javascript:void(0);" onclick="Load_Reference(\'fundingcat\')">['.$Lang['ePCM']['FundingSource']['ImportCheckFundingCategory'] .']</a>',
				'',
				'',
				'',
				'<span class="tabletextremark">(1-'.$Lang['ePCM']['FundingSource']['RecordStatus'][1].'; 2-'.$Lang['ePCM']['FundingSource']['RecordStatus'][2].')</span>',
				'<span class="tabletextremark">('.$Lang['ePCM']['Import']['DateFormatRemarks'] .')</span>'
		);
	}
	public function getRemarksThickBoxHeader($refType){
		global $Lang;
		$headerArr = array();
		switch($refType){
			case 'fundingcat';			
				$headerArr[] = $Lang['ePCM']['Import']['Ref']['Code'];
				$headerArr[] = $Lang['ePCM']['FundingSource']['Category'];
				break;
		}
		return $headerArr;
	}
	### For Import Step 1 (End) ###
	
	### For Import Step 2 (Start) ###
	public function getContentDescripiton(){
		global $Lang;
		
		$desc = array();
		$desc['fundingCat'] =  $Lang['ePCM']['FundingSource']['Category'];
		$desc['engName'] = $Lang['ePCM']['FundingSource']['EnglishName'];
		$desc['chiName'] = $Lang['ePCM']['FundingSource']['ChineseName'];
		$desc['amount'] = $Lang['ePCM']['FundingSource']['Budget'];
		$desc['recordStatus'] = $Lang['ePCM']['FundingSource']['RecordStatus']['RecordStatus'];
		$desc['expiryDate'] = $Lang['ePCM']['FundingSource']['DeadLine'];
		return $desc;
	}
	
	public function getContentMapping(){
		$desc = array('fundingCat', 'engName', 'chiName', 'amount', 'recordStatus', 'expiryDate');
		return $desc;
	}
	
	# for error display table
	public function getContentWidth(){
		$widthArr = array(10,15,15,15,10,10);
		return $widthArr;
	}
	
	public function getValidationRule(){
		$desc = array('fundingCat', 'engName', 'chiName', 'amount', 'recordStatus', 'expiryDate');
		$rule = array();
		$rule['fundingCat'] = 'required|existedFundingCat:Funding';
		$rule['engName'] = 'required';
		$rule['chiName'] = 'required';
		$rule['amount'] = 'required|numeric';
		$rule['recordStatus'] = 'required|recordStatus:Funding';
		$rule['expiryDate'] = 'required|date';
		
		return $rule; 
	}
	### For Import Step 2 (End) ###
	
	### For Export (Start) ###
	public function getExportHeader(){
		return $this->getHeaderTemplate();
// 		$header = array();
// 		$header['En'] = array('Funding Source Category', 'English Name', 'Chinese Name', 'Amount', 'Record Status', 'Expiry Date');
// 		$header['Ch'] = array('資金來源類別', '中文名稱', '英文名稱', '金額', '狀況', '到期日');
// 		return $header;
	}
	### For Export (End) ###
}