<?php 
include_once "importScheme.php";
class Category implements importScheme{
	
	public function __construct(){
		
	}
	### For Import Template (Start) ###
	public function getHeaderTemplate(){
		$header = array();
		$header['En'] = array('Code','Category name (English)','Category name (Chinese)');
		$header['Ch'] = array('代碼','類別名稱 (英文)','類別名稱 (中文)');
		return $header;
	}
	
	public function getHeaderProperty(){
		// 1 = complusory , 2 =ref , 3 = optional
		return array(1,1,1);
	}
	
	public function getContentTemplate(){
		$contentArr = array();
		$contentArr[] = array('CI', 'Campus Improvment ', '校園改善');
		$contentArr[] = array('IT', 'Information Technology', '資訊科技');
		$contentArr[] = array('LT', 'Learning & Teaching', '學與教');
		$contentArr[] = array('O', 'Others', '其它');
		return $contentArr;
	}
	### For Import Template (End) ###
	
	### For All Step (Start) ###
	public function setCurrentSectionAndPage(){
		global $_PAGE;
		
		$_PAGE['CurrentSection'] = 'Settings';
		$_PAGE['CurrentPage'] = 'Category';
	}
	public function getNavigationAry(){
		global $Lang;
		
		$originPath = $this->getOriginPath();
		
		$navigationAry = array();
		$navigationAry[] = array($Lang['ePCM']['SettingsArr']['CategoryArr']['MenuTitle'], 'javascript: window.location=\'index.php?p='.$originPath.'\';');
		$navigationAry[] = array($Lang['Btn']['Import']);
		return $navigationAry;
	}
	public function getOriginPath(){
		return 'setting.category';
	}
	public function getTempFilePath(){
		global $intranet_root, $ePCMcfg;
		return $intranet_root.'/'.$ePCMcfg['INTERNATDATA'].'temp/import/category/';
	}
	### For All Step (End) ###
	
	### For Import Step 1 (Start) ###
	public function getColumnTitleArr(){
		global $Lang;
		
		$ColumnTitleArr = array();
		$mapping = $this->getContentMapping();
		$desc = $this->getContentDescripiton();
		foreach($mapping as $_key){
			$ColumnTitleArr[] = $desc[$_key];
		}
		return $ColumnTitleArr; 
	}
	public function getColumnPropertyArr(){
		return array(1,1,1);
	}
	public function getRemarksArr(){
		return array();
	}
	public function getRemarksThickBoxHeader($refType){
		return array();
	}
	### For Import Step 1 (End) ###
	
	### For Import Step 2 (Start) ###
	public function getContentDescripiton(){
		global $Lang;
		
		$desc = array();
		$desc['code'] =  $Lang['ePCM']['Category']['Code'];
		$desc['catNameEn'] = $Lang['ePCM']['Category']['CategoryName'];
		$desc['catNameChi'] = $Lang['ePCM']['Category']['CategoryNameChi'];
		return $desc;
	}
	
	public function getContentMapping(){
		$desc = array('code', 'catNameEn', 'catNameChi');
		return $desc;
	}
	
	# for error display table
	public function getContentWidth(){
		$widthArr = array(15,30,30);
		return $widthArr;
	}
	
	public function getValidationRule(){
		
		$rule = array();
		$rule['code'] = 'required|unique:Category';
		$rule['catNameEn'] = 'required';
		$rule['catNameChi'] = 'required';
		
		return $rule; 
	}
	### For Import Step 2 (End) ###
	
	### For Export (Start) ###
	public function getExportHeader(){
		return $this->getHeaderTemplate();
	}
	### For Export (End) ###
}