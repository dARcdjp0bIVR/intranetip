<?php 
include_once "importScheme.php";
class Groupitembudget implements importScheme{
	
	public function __construct(){
		
	}
	### For Import Template (Start) ###
	public function getHeaderTemplate(){
		$header = array();
		$header['En'] = array('Department/Group code','Financial item code','Budget of the item ','Default Funding Source');
		$header['Ch'] = array('部門/科組代碼','財政項目代碼','項目預算','預設對應資金來源');
		return $header;
	}
	
	public function getHeaderProperty(){
		// 1 = complusory , 2 =ref , 3 = optional
		// pls set all 1, otherwise will not able to extract the value
		return array(1,1,1,1);
	}
	
	public function getContentTemplate(){
		$contentArr = array();
		$contentArr[] = array('CHIN', 'BOOK', '100', '1');
		$contentArr[] = array('ENG', '', '200', '1');
		$contentArr[] = array('MATH', 'BOOK', '200', '');
		$contentArr[] = array('MATH', 'OTHER', '200', '');
		return $contentArr;
	}
	### For Import Template (End) ###
	
	### For All Step (Start) ###
	public function setCurrentSectionAndPage(){
		global $_PAGE;
		
		$_PAGE['CurrentSection'] = 'Settings';
		$_PAGE['CurrentPage'] = 'Group';
	}
	public function getNavigationAry(){
		global $Lang;
		
		$originPath = $this->getOriginPath();
		
		$navigationAry = array();
		$navigationAry[] = array($Lang['ePCM']['Group']['Budget'], 'javascript: window.location=\'index.php?p='.$originPath.'\';');
		$navigationAry[] = array($Lang['Btn']['Import']);
		return $navigationAry;
	}
	public function getOriginPath(){
		return 'setting.group';
	}
	public function getTempFilePath(){
		global $intranet_root, $ePCMcfg;
		return $intranet_root.'/'.$ePCMcfg['INTERNATDATA'].'temp/import/groupitembudget/';
	}
	### For All Step (End) ###
	
	### For Import Step 1 (Start) ###
	public function getColumnTitleArr(){
		global $Lang;
		
		$ColumnTitleArr = array();
		$mapping = $this->getContentMapping();
		$desc = $this->getContentDescripiton();
		foreach($mapping as $_key){
			$ColumnTitleArr[] = $desc[$_key];
		}
		return $ColumnTitleArr; 
	}
	public function getColumnPropertyArr(){
		return array(1,1,1,0);
	}
	public function getRemarksArr(){
		global $Lang;
		return array(
				'<a id="departmentOrGroup" class="tablelink" href="javascript:void(0);" onclick="Load_Reference(\'departmentOrGroup\')">'.$Lang["ePCM"]["Group"]["GroupCodeEnquiry"].'</a>',
				'<a id="financialItemBudget" class="tablelink" href="javascript:void(0);" onclick="Load_Reference(\'financialItemBudget\')">'.$Lang['ePCM']['Group']['RelatedGroupFinancialItemEnquiry'].'</a>',
				'',
				'<a id="fundingSource" class="tablelink" href="javascript:void(0);" onclick="Load_Reference(\'fundingSource\')">'.$Lang['ePCM']['Group']['FundingSourceEnquiry'].'</a>'
		);
	}
	public function getRemarksThickBoxHeader($refType){
		global $Lang;
		$headerArr = array();
		switch($refType){
			case 'departmentOrGroup';			
			$headerArr[] = $Lang['ePCM']['Import']['Ref']['Code'];
			$headerArr[] = Get_Lang_Selection($Lang['ePCM']['Group']['GroupNameChi'], $Lang['ePCM']['Group']['GroupName']);
				break;
			case 'financialItemBudget':
				$headerArr[] = $Lang['ePCM']['Group']['GroupCode'];
				$headerArr[] = Get_Lang_Selection($Lang['ePCM']['Group']['GroupNameChi'], $Lang['ePCM']['Group']['GroupName']);
				$headerArr[] = $Lang['ePCM']['Group']['ItemCode'];
				$headerArr[] = Get_Lang_Selection($Lang['ePCM']['Group']['Item']['FinancialItemChi'], $Lang['ePCM']['Group']['Item']['FinancialItemEng']);
				break;
			case 'fundingSource';
				$headerArr[] = $Lang['ePCM']['Import']['Ref']['Code'];
				$headerArr[] = $Lang['ePCM']['FundingSource']['FundingSource'];
				$headerArr[] = $Lang['ePCM']['FundingSource']['Category'];
				break;
		}
		return $headerArr;
	}
	### For Import Step 1 (End) ###
	
	### For Import Step 2 (Start) ###
	public function getContentDescripiton(){
		global $Lang;
		
		$desc = array();
		$desc['departmentOrGroup'] = $Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroup'];
		$desc['financialItemBudget'] =  $Lang['ePCM']['Group']['Item']['Budget']['financialItem'];
		$desc['departmentOrGroupBudget'] = $Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroupBudget'];
		$desc['fundingSource'] = $Lang['ePCM']['Group']['Item']['Budget']['fundingSource'];
		return $desc;
	}
	
	public function getContentMapping(){
		$desc = array('departmentOrGroup', 'financialItemBudget', 'departmentOrGroupBudget', 'fundingSource');
		return $desc;
	}
	
	# for error display table
	public function getContentWidth(){
		$widthArr = array(15,15,15,15);
		return $widthArr;
	}
	
	public function getValidationRule(){
		$rule = array();
		$rule['departmentOrGroup'] = 'required|existed:Group';
		# & = passing another field &{fieldname} in to the validation function   
		$rule['financialItemBudget'] = 'required|itemExisted:Groupitem,&departmentOrGroup';
		$rule['departmentOrGroupBudget'] = 'required';
		$rule['fundingSource'] = 'existedFunding:Groupitembudget';
		return $rule; 
	}
	### For Import Step 2 (End) ###
	# export function
	public function getExportHeader(){
		return $this->getHeaderTemplate();
	}
}