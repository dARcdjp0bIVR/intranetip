<?php 
class importHelper{
	
	var $tempPath;
	var $importFile;
	var $fileHandler;
	var $processResult;
	
	public function __construct(){
		
	}
	
	private function validateFile(){
		
		$error = 0;
	
		$ext = strtolower(pathinfo($this->importFile['name'], PATHINFO_EXTENSION));
		if(!($ext == 'csv' || $ext == 'txt')){
			$error = 1;
		}
		
		$this->processResult['ext'] = $ext;
		$this->processResult['error'] = $error;
		return $error;
	}
	
	private function transferToTemp(){
		
		$lfs = $this->fileHandler;
		$folderPrefix = $this->tempPath;
		$ext = $this->processResult['ext'];
		if($ext == ''){
			$ext = 'undefine';
		}
		
		if (!file_exists($folderPrefix)) {
			$lfs->folder_new($folderPrefix);
		}
		
		$targetFileName = date('Ymd_His').'_'.$_SESSION['UserID'];
		$targetFilePath = stripslashes($folderPrefix.$targetFileName);
		$success = $lfs->lfs_move($this->importFile['tmp_name'], $targetFilePath);
		
		$this->processResult['filename'] = $targetFileName;
		$this->processResult['path'] = $targetFilePath;
		$this->processResult['success'] = $success;
		return $success;
	}
	
	public function processToTemp(){
		if(!isset($this->importFile) || !is_uploaded_file($this->importFile['tmp_name'])){
			throw new Exception('importHelper >> CANNOT processToTemp() without any files.');
		}
		if(!isset($this->fileHandler)){
			throw new Exception('importHelper >> CANNOT processToTemp() without file handler.');
		}
		if(!isset($this->tempPath)){
			throw new Exception('importHelper >> CANNOT processToTemp() without temp path.');
		}
		
		$success = 0;
		$error = $this->validateFile();
		if($error == 0){
			$success = $this->transferToTemp();
		}
		return $success;
	}
}