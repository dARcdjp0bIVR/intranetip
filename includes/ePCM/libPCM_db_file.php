<?php
// Editing by 
// ################ Change Log [Start] #####
//
// Date :
//
// ################# Change Log [End] ######
include_once ($intranet_root . "/includes/ePCM/libPCM_db.php");
include_once ($intranet_root . "/includes/ePCM/libPCM_cfg.php");
class libPCM_db_file extends libPCM_db {
	var $FilePath;
	var $e_version;
	public function __construct(){
		// $config_school_code is from setting.php
		global $file_path, $PATH_WRT_ROOT, $config_school_code,$ePCMcfg;
		parent::__construct();
		
		$this->e_version = 'A1';
		$this->FilePath = $PATH_WRT_ROOT . $ePCMcfg['INTERNATDATA'];
		// $this->BakPathArr = array($file_path.'/file/ePCM/');
	}
	
	// #####################################################
	// Upload and delete functions (start)
	// #####################################################
	private function getMailPath(){
		$rootPath = $this->FilePath;
		return $rootPath.'mail/';
	}
	private function getApiFilePath(){
		$rootPath = $this->FilePath;
		return $rootPath.'api/';
	}
	
	private function getPersonalFolderName(){
		return 'u' . $_SESSION['UserID'] . '/';
	}

	private function getTempPath($sessionToken){
		$rootPath = $this->FilePath;
		$personalFolderName = $this->getPersonalFolderName();
		return $rootPath . 'temp/' . $personalFolderName . $sessionToken;
	}

	private function getStorageMethod(){
		return date("Y");
	}

	private function getStoragePath(){
		$rootPath = $this->FilePath;
		$folderName = $this->getStorageMethod();
		return $rootPath . 'storage/' . $folderName;
	}
	public function getStoragePathForPCM($folderName){
		$rootPath = $this->FilePath;
		return $rootPath . $folderName;
	}

	private function getStoragePathDB(){
		$folderName = $this->getStorageMethod();
		return 'storage/' . $folderName . '/';
	}

	private function getFileStoragePath($fileDbPath){
		$severPath = $this->FilePath;
		return $severPath . $fileDbPath;
	}

	public function uploadToTempPath($sessionToken){
		global $_PAGE, $PATH_WRT_ROOT;
		
		$lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
		
		$plupload_tmp_path = $this->getTempPath($sessionToken);
		
		if(! is_dir($plupload_tmp_path)){
			$lfs->folder_new($plupload_tmp_path);
		}
		
		$pluploadPar['targetDir'] = $plupload_tmp_path;
		
		include_once ($PATH_WRT_ROOT . "includes/plupload.php");
	}

	public function processTempToStorage($sessionToken, $fileName, $ruleID='', $stageID=''){
		global $_PAGE, $PATH_WRT_ROOT;
		
		$lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
		$db = $_PAGE['Controller']->requestObject('libPCM_db_file', 'includes/ePCM/libPCM_db_file.php');
		
		$plupload_tmp_path = $this->getTempPath($sessionToken);
		
		$errorCode = '';
		
		if(! is_dir($plupload_tmp_path)){
			$errorCode .= 'E1';
		}
		
		// temp Path of the file
		$tempPath = $plupload_tmp_path . "/" . $fileName;
		$timenow = date('Y-m-d H:i:s');
		if($fileName != ""){
			$fieldNameArr = array(
					'CaseID',
					'FilePath',
					'FileName',
					'UploadBy',
					'Session',
					'Owner',
					'DateInput',
                    'RuleID',
                    'StageID'
			);
			$multiRowValueArr = array();
			$multiRowValueArr[] = array(
					'0',
					$tempPath,
					$fileName,
					$_SESSION['UserID'],
					$sessionToken,
					$_SESSION['UserID'],
					$timenow,
                    $ruleID,
                    $stageID
			);
			$result = $db->insertData('INTRANET_PCM_ATTACHMENT', $fieldNameArr, $multiRowValueArr, $excludeCommonField = true);
			$attachmentID = $db->db_insert_id();
		}
		$pcm_path = $this->getStoragePath();
		$storageName = $sessionToken . $attachmentID;
		$dbPath = $this->getStoragePathDB() . $storageName . 'e';
		$des = $pcm_path . '/' . $storageName;
		
		if(! is_dir($des)){
			$lfs->folder_new($pcm_path);
		}
		
		if(! is_file($tempPath)){
			$errorCode .= 'E2';
		}else{
			if(strpos($fileName, ".") == 0){
				$errorCode .= 'E3';
			}else{
				$checksum = md5_file($tempPath);
				$fileSize = filesize($tempPath);
				$lfs->lfs_copy($tempPath, $des);
				$enc_des = $des . 'e';
				$enc_version = $this->saveFile($des, $enc_des);
				$eFileSize = filesize($enc_des);
				$checkDiff = $eFileSize - $fileSize;
				$errorCode .= $checkDiff;
				if(is_file($enc_des) && $checkDiff < 17){
					$changefieldNameArr = array(
							'FilePath',
							'Checksum',
							'FileSize',
							'ErrorCode',
							'eVersion' 
					);
					$multiRowValueArr = array();
					$multiRowValueArr[] = array(
							$dbPath,
							$checksum,
							$fileSize,
							$errorCode,
							$enc_version 
					);
					$conditions = " AttachmentID = '$attachmentID' ";
					$this->updateData('INTRANET_PCM_ATTACHMENT', $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField = true);
					// remove two temp file
					$lfs->file_remove($tempPath);
					$lfs->file_remove($des);
					@$lfs->folder_remove($plupload_tmp_path);
					$success = true;
				}else{
					$errorCode .= 'E4';
					$success = false;
					$changefieldNameArr = array(
							'ErrorCode',
							'IsDeleted' 
					);
					$multiRowValueArr = array();
					$multiRowValueArr[] = array(
							$errorCode,
							'1' 
					);
					$conditions = " AttachmentID = '$attachmentID' ";
					$this->updateData('INTRANET_PCM_ATTACHMENT', $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField = true);
				}
			}
		}
		
		if($errorCode != ''){
			$changefieldNameArr = array(
					'ErrorCode' 
			);
			$multiRowValueArr = array();
			$multiRowValueArr[] = array(
					$errorCode 
			);
			$conditions = " AttachmentID = '$attachmentID' ";
			$this->updateData('INTRANET_PCM_ATTACHMENT', $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField = true);
		}
		
		$js = '';
		if($success){
			$AttachList = $db->getUserSessionFileList($sessionToken, $ruleID, $stageID);
			$js = $this->getRefreshFileListJs($AttachList);
		}
		
		$js .= 'if(bindClickEventToElements){bindClickEventToElements();}' . "\n";
		
		return $js;
	}

	public function getFileToBrowser($AttachmentID){
		global $_PAGE,$intranet_root;
		
			$lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
		
		
		$fileInfo = $this->getFileById($AttachmentID);
		$filePath = $fileInfo[0]['FilePath'];
		if($filePath != ''){
			// $severPath = $this->FilePath;
			// $storagePath = $severPath.$filePath;
			$storagePath = $this->getFileStoragePath($filePath);
			$new_des = $storagePath . '_dec';
			$this->getFile($storagePath, $new_des, '', '', 1);
			
			$decryptCheckSum = md5_file($new_des);
			if(file_exists($new_des)){
				if($decryptCheckSum != $fileInfo[0]['Checksum']){
					$outFileName = 'E01_' . $fileInfo[0]['FileName'];
				}else{
					$outFileName = $fileInfo[0]['FileName'];
				}
				$this->outputToBrowserNoExit($new_des, $outFileName);
				$lfs->file_remove($new_des);
				exit();
			}
		}else{
			// no such file
		}
	}
	
// 	public function getTemplateToBrowser($TemplateType){
// 		$attachmentArr =  $this->getCaseStageAttachment($CaseID='-1', $Stage=$TemplateType);
// 		foreach((array)$attachmentArr as $_infoArr){
// 			$this->getFileToBrowser($_infoArr['AttachmentID']);
// 		}
// 	}
	
	// copy from lib.php output2browserByPath()
	private function outputToBrowserNoExit($file, $filename, $content_type = ""){
		if($content_type == ""){
			$content_type = "application/octet-stream";
		}
		$filename = str_replace(" ", "_", $filename);
		
		global $userBrowser;
		
		if(isset($userBrowser)){
			$browser = $userBrowser->browsertype;
			$operation_system = $userBrowser->platform;
			if($operation_system == "Andriod"){
				// $ToUnicode = true;
				/*
				 * if (str_lang($filename)!="ENG")
				 * {
				 * $file_extention = getFileExtention($filename, 1);
				 * $filename = time().".".$file_extention;
				 * }
				 */
			}
		}else{
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			if(preg_match('|MSIE ([0-9].[0-9]{1,2})|', $useragent, $matched)){
				$browser_version = $matched[1];
				$browser = 'IE';
			}elseif(preg_match('|Opera ([0-9].[0-9]{1,2})|', $useragent, $matched)){
				$browser_version = $matched[1];
				$browser = 'Opera';
			}elseif(preg_match('|Firefox/([0-9\.]+)|', $useragent, $matched)){
				$browser_version = $matched[1];
				$browser = 'Firefox';
			}elseif(preg_match('|Safari/([0-9\.]+)|', $useragent, $matched)){
				$browser_version = $matched[1];
				$browser = 'Safari';
			}else{
				// browser not recognized!
				$browser_version = 0;
				$browser = 'other';
			}
		}
		
		$encoded_filename = $filename;
		$encoded_filename = urlencode($filename);
		$encoded_filename = str_replace("+", "%20", $encoded_filename);
		
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-type: $content_type");
		
		header("Content-Length: " . filesize($file));
		
		if($browser == "IE"){
			header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
		}else if($browser == "Firefox"){
			// header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
			header('Content-Disposition: attachment; filename*=utf8\'\'' . $encoded_filename);
		}else if($browser == "Safari") // for Chrome
{
			header('Content-Disposition: attachment; filename="' . $filename . '"');
		}else if($browser == "Chrome") // for Chrome
{
			header('Content-Disposition: attachment; filename="' . $filename . '"');
		}else{
			header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
		}
		readfile($file);
	}

	public function deleteAttachmentFromDb($AttachmentID){
		$tableName = 'INTRANET_PCM_ATTACHMENT';
		$changefieldNameArr = array(
				'IsDeleted',
				'WillDelete' 
		);
		$multiRowValueArr = array();
		$multiRowValueArr[] = array(
				'1',
				'0' 
		);
		$conditions = " AttachmentID = '" . $AttachmentID . "' ";
		return $this->updateData($tableName, $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField = true);
	}

	public function removeTempAttachment($sessionToken, $AttachmentID, $ruleID='', $stageID=''){
		global $_PAGE;
		$lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
		$fileInfoArr = $this->getFileById($AttachmentID, " AND CaseID = '0' ");
		// $AttachList = $this->getUserSessionFileList($sessionToken);
		if(count($fileInfoArr) == 1){
			$this->deleteAttachmentFromDb($AttachmentID);
			$storagePath = $this->getFileStoragePath($fileInfoArr[0]['FilePath']);
			$lfs->file_remove($storagePath);
			$AttachList = $this->getUserSessionFileList($sessionToken, $ruleID, $stageID);
			$js = $this->getRefreshFileListJs($AttachList);
		}
		
		return $js;
	}

	public function removeAttachmentStep1($sessionToken, $AttachmentID, $ruleID='', $template_key=''){
		global $_PAGE;
		$lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
		$fileInfoArr = $this->getFileById($AttachmentID, "");
		// $AttachList = $this->getUserSessionFileList($sessionToken);
		if(count($fileInfoArr) == 1){
			
			$tableName = 'INTRANET_PCM_ATTACHMENT';
			$changefieldNameArr = array(
					'WillDelete' 
			);
			$multiRowValueArr = array();
			$multiRowValueArr[] = array(
					'1' 
			);
			$conditions = " AttachmentID = '" . $AttachmentID . "' ";
			$this->updateData($tableName, $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField = true);
			// $storagePath = $this->getFileStoragePath($fileInfoArr[0]['FilePath']);
			$AttachList = $this->getUserSessionFileList($sessionToken, $ruleID, $template_key);
			$js = $this->getRefreshFileListJs($AttachList);
		}
		
		return $js;
	}

	public function removeAttachmentStep2($AttachmentID, $keepAttachment=false){
		global $_PAGE;
		$lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
		$fileInfoArr = $this->getFileById($AttachmentID, "");
		if(count($fileInfoArr) == 1){
			$this->deleteAttachmentFromDb($AttachmentID);
			if(!$keepAttachment) {
                $storagePath = $this->getFileStoragePath($fileInfoArr[0]['FilePath']);
                $lfs->file_remove($storagePath);
            }
		}
	}

	public function recoverTempRemoveAttachment($session, $WillDeletedList){
		$tableName = 'INTRANET_PCM_ATTACHMENT';
		$changefieldNameArr = array(
				'WillDelete' 
		);
		$multiRowValueArr = array();
		$multiRowValueArr[] = array(
				'0' 
		);
		$conditions = " Session = '" . $session . "' ";
		return $this->updateData($tableName, $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField = true);
	}

	public function getRefreshFileListJs($AttachList){
		global $Lang, $_PAGE;
		$x = '';
		$x .= 'var ObjAttachedDisplay = document.getElementById("FileListDiv");' . "\n";
		$x .= 'var AttachmentList = "";' . "\n";
		$x .= 'AttachmentList = "";' . "\n";
		$x .= 'var deleteFileContainer = document.getElementById("willDeleteFileDiv");' . "\n";
		$x .= 'var WillDeletedList = "";' . "\n";
		$x .= 'WillDeletedList = "";' . "\n";
		for($i = 0; $i < sizeof($AttachList); $i ++){
			$fileName = str_replace("'", "\'", $AttachList[$i]['FileName']);
			$fileSize = $_PAGE['libPCM_ui']->displayFileSize($AttachList[$i]['FileSize']);
			$isTempFile = $AttachList[$i]['isTempFile'];
			$isWillDeleteFile = $AttachList[$i]['WillDelete'];
// 			debug_pr($AttachList[$i]);
			if(! $isWillDeleteFile){
				$AttachDisplay = '<input type="checkbox" id="FileID[]" name="FileID[]" value="' . $AttachList[$i]['AttachmentID'] . '" checked style="visiblity:hidden; display:none;">';
				$AttachDisplay .= '<a class="tabletool" href="index.php?p=file.download.' . $AttachList[$i]['AttachmentID'] . '">';
				$AttachDisplay .= $fileName . '(' . $fileSize . ')';
				$AttachDisplay .= '</a>';
				// $fileLinkLang = $fileName.'('.$fileSize.')';
				// $AttachDisplay .= $_PAGE['libPCM_ui']->displayFileDownloadLinkSize($AttachList[$i]['AttachmentID'], $fileLinkLang);
				if($isTempFile){
					$AttachDisplay .= ' [<a class="tabletool" href="javascript:void(0);" onclick="ajax_delete_attachment(\\\'' . $AttachList[$i]['AttachmentID'] . '\\\')">' . $Lang['Btn']['Delete'] . '</a>]';
				}else{
					$AttachDisplay .= ' [<a class="tabletool" href="javascript:void(0);" onclick="ajax_delete_saved_attachment(\\\'' . $AttachList[$i]['AttachmentID'] . '\\\')">' . $Lang['Btn']['Delete'] . '</a>]';
				}
				$AttachDisplay .= '&nbsp;<span>'.$AttachList[$i]['OwnerName'].'</span>';
				$AttachDisplay .= '&nbsp;<span>'.$AttachList[$i]['DateInput'].'</span>';
				$AttachDisplay .= '<input type="hidden" id="FileSize[]" name="FileSize[]" value="' . $fileSize . '">';
				$AttachDisplay .= '<br>';
				$x .= 'AttachmentList += \'' . $AttachDisplay . '\';' . "\n";
			}else{
				$WillDeletedList = '';
				$WillDeletedList .= '<input type="hidden" id="WillDeletedList" name="WillDeletedList[]" value="' . $AttachList[$i]['AttachmentID'] . '">';
				$x .= 'WillDeletedList += \'' . $WillDeletedList . '\';' . "\n";
			}
		}
		
		$x .= 'AttachmentList += "";' . "\n";
		$x .= 'ObjAttachedDisplay.innerHTML = AttachmentList;' . "\n";
		$x .= 'WillDeletedList += "";' . "\n";
		$x .= 'deleteFileContainer.innerHTML = WillDeletedList;' . "\n";
		
		$x .= 'Check_Total_Filesize(1);' . "\n"; // only check the total file size of newly added attachement

		return $x;
	}


	public function getFileSizeDisplay($sizeInByte, $recursiveNum = 0){
		$arr = array(
				'',
				'K',
				'M',
				'G',
				'T' 
		);
		$callNum = $recursiveNum;
		if($sizeInByte < 1024){
			return round($sizeInByte, 1) . $arr[$callNum] . 'B';
		}else{
			$callNum ++;
			// for safety
			if($callNum > 4){
				return false;
			}else{
				return $this->getFileSizeDisplay(($sizeInByte / 1024), $callNum);
			}
		}
	}

	public function updateFileSession($caseID, $stageID='', $sessionToken, $quoID='', $type='0', $ruleID=''){
        if ($ruleID!=''){
            $attachArr = $this->getRuleAttachment($ruleID, $AssocWithStage=false);
            $AttachmentIDArr = Get_Array_By_Key($attachArr, 'AttachmentID');
			if($stageID!=''){
				$changefieldNameArr = array(
					'Session',
					'UploadBy',
					'RuleID',
					'StageID'
				);
				$multiRowValueArr = array();
				$multiRowValueArr[] = array(
					$sessionToken,
					$_SESSION['UserID'],
					$ruleID,
					$stageID
				);
			}else{
                $changefieldNameArr = array(
                    'Session',
                    'UploadBy',
                    'RuleID'
                );
                $multiRowValueArr = array();
                $multiRowValueArr[] = array(
                    $sessionToken,
                    $_SESSION['UserID'],
                    $ruleID
                );
			}
			$conditions = " AttachmentID IN ('" . implode("','", (array)$AttachmentIDArr) . "') ";
			return $this->updateData('INTRANET_PCM_ATTACHMENT', $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField = true);
	}	elseif($stageID != 3 || $caseID == -1){
			$attachArr = $this->getCaseStageAttachment($caseID, $stageID, false, $type);
			$AttachmentIDArr = Get_Array_By_Key($attachArr, 'AttachmentID');
			
			$changefieldNameArr = array(
					'Session',
					'UploadBy'
			);
			$multiRowValueArr = array();
			$multiRowValueArr[] = array(
					$sessionToken,
					$_SESSION['UserID']
			);
			$conditions = " AttachmentID IN ('" . implode("','", (array)$AttachmentIDArr) . "') ";
			return $this->updateData('INTRANET_PCM_ATTACHMENT', $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField = true);
		}else{
			$attachArr = $this->getQuoAttachment($quoID);
			$AttachmentIDArr = Get_Array_By_Key($attachArr, 'AttachmentID');
			
			$changefieldNameArr = array(
					'Session',
					'UploadBy'
			);
			$multiRowValueArr = array();
			$multiRowValueArr[] = array(
					$sessionToken ,
					$_SESSION['UserID']
			);
			$conditions = " AttachmentID IN ('" . implode("','", (array)$AttachmentIDArr) . "') ";
			return $this->updateData('INTRANET_PCM_ATTACHMENT', $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField = true);
		}
	}
	
	public function getEmailAttachmentReady($caseID){
		global $_PAGE;
		
		$lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
		
		$fileInfoArr = $this->getCaseStageAttachment($caseID, $Stage=2, $AssocWithStage=false, $type=2);
		$result = array();
		if(!empty($fileInfoArr)){
			$mailPath = $this->getMailPath().$caseID.'_'.$_SESSION['UserID'];
			if(! is_dir($mailPath)){
				$lfs->folder_new($mailPath);
			}
			else{
				$lfs->folder_remove_recursive($mailPath);
				$lfs->folder_new($mailPath);
			}
			foreach((array)$fileInfoArr as $_infoArr){
				$_AttachmentID = $_infoArr['AttachmentID'];
				$result[] = $this->decryptAndMoveToMailFolder($_AttachmentID,$mailPath);
			}
		}
		if(!in_array(0,$result)){
			return $mailPath;
		}
		else{
			return false;
		}
	}
	public function getApiAttachmentReady($caseID){
		global $_PAGE;
		
		$lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
		
		$fileInfoArr = $this->getCaseStageAttachment($caseID, $Stage=2, $AssocWithStage=false, $type=2);
		$result = array();
		if(!empty($fileInfoArr)){
			$apiPath = $this->getApiFilePath().sha1($caseID);
			if(! is_dir($apiPath)){
				$lfs->folder_new($apiPath);
			}
			else{
				$lfs->folder_remove_recursive($apiPath);
				$lfs->folder_new($apiPath);
			}
			foreach((array)$fileInfoArr as $_infoArr){
				$_AttachmentID = $_infoArr['AttachmentID'];
				$result[] = $this->decryptAndMoveToMailFolder($_AttachmentID,$apiPath);
			}
		}
		if(!in_array(0,$result)){
			return $apiPath;
		}
		else{
			return false;
		}
	}
	
	public function decryptAndMoveToMailFolder($AttachmentID, $mailPath){
		global $_PAGE;
		if($mailPath != ''){
			$lfs = $_PAGE['Controller']->requestObject('libfilesystem', 'includes/libfilesystem.php');
			
			$fileInfo = $this->getFileById($AttachmentID);
			$filePath = $fileInfo[0]['FilePath'];
			if($filePath != ''){
				$storagePath = $this->getFileStoragePath($filePath);
				$new_des = $storagePath . '_dec';
				$this->getFile($storagePath, $new_des, '', '', 1);
				$decryptCheckSum = md5_file($new_des);
				if(file_exists($new_des)){
					if($decryptCheckSum != $fileInfo[0]['Checksum']){
						$outFileName = 'E01_' . $fileInfo[0]['FileName'];
					}else{
						$outFileName = $fileInfo[0]['FileName'];
					}
				}
				$mailFile = $mailPath.'/'.$outFileName;
				$lfs->lfs_copy($new_des, $mailFile);
				$lfs->file_remove($new_des);
				if(file_exists($mailFile)){
					return 1;
				}
			}
		}
	}
	
	// #####################################################
	// Upload and delete functions (end)
	// #####################################################
	
	// #####################################################
	// encrypt decrypt handling (start)
	// #####################################################
	// # Move and Encrpyt file
	public function saveFile($inputFilePath, $outputFilePath, $key = '', $iv = '', $overwrittern = 0){
		if($key == '' || $iv == ''){
			$arr = $this->getEncryptionKeyIV($outputFilePath);
			$key = $arr['key'];
			$iv = $arr['iv'];
		}
		
		$isInputFileExist = file_exists($inputFilePath);
		if(! $isInputFileExist){
			echo "input file is not exist";
			return - 1;
		}
		if(! $overwrittern){
			$isOutputFileExist = file_exists($outputFilePath);
			if($isOutputFileExist){
				echo "output file is already exist, overwritten is turned off";
				return - 3;
			}
		}
		$command = 'openssl aes-256-cbc -in ' . escapeshellarg($inputFilePath) . ' -out ' . escapeshellarg($outputFilePath) . ' -K ' . $key . ' -iv ' . $iv . '';
		$result_e = trim(shell_exec($command));
		$isOutputFileExist = file_exists($outputFilePath);
		if(! $isOutputFileExist){
			echo "output file is not exist, something wrong!!";
			return - 2;
		}else{
			return $arr['verions'];
		}
	}
	
	// # Decrpyt file
	public function getFile($inputFilePath, $outputFilePath, $key = '', $iv = '', $overwrittern = 0){
		if($key == '' || $iv == ''){
			$arr = $this->getEncryptionKeyIV($inputFilePath);
			$key = $arr['key'];
			$iv = $arr['iv'];
		}
		
		$isInputFileExist = file_exists($inputFilePath);
		if(! $isInputFileExist){
			echo "input file is not exist";
			return - 1;
		}
		if(! $overwrittern){
			$isOutputFileExist = file_exists($outputFilePath);
			if($isOutputFileExist){
				echo "output file is already exist, overwritten is turned off";
				return - 3;
			}
		}
		$command = 'openssl aes-256-cbc -d -in ' . escapeshellarg($inputFilePath) . ' -out ' . escapeshellarg($outputFilePath) . ' -K ' . $key . ' -iv ' . $iv . '';
		$result_d = trim(shell_exec($command));
		$isOutputFileExist = file_exists($outputFilePath);
		if(! $isOutputFileExist){
			echo "output file is not exist, something wrong!!";
			return - 2;
		}else{
			return 1;
		}
	}

	private function getEncryptionKeyIV($fileName){
		global $config_school_code, $ePCMcfg;
		
		$passphrase = $ePCMcfg['passphrase'];
		$masterKey = $ePCMcfg['masterKey'];
		$version = $this->e_version;
		
		switch($version){
			case 'A1':			
				$partA = hash_hmac('sha512', $masterKey, $passphrase);
				$partB = hash_hmac('sha512', $fileName, $passphrase . $masterKey);
				$key = hash_hmac('sha256', $partA . $passphrase . $fileName, $partB);
				$IV = hash_hmac('md5', $partB . $passphrase . $fileName, $fileName);
				break;
		}
		
		$Arr['key'] = $key;
		$Arr['iv'] = $IV;
		$Arr['verions'] = $version;
		return $Arr;
	}
	// #####################################################
	// encrypt decrypt handling (end)
	// #####################################################
}
?>
