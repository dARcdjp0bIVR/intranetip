#!/usr/bin/env python
# -*- coding: utf-8 -*-
# unzip.py
import os
import sys
import zipfile
def unzipFile(zipFilePath, destDir):
    zfile = zipfile.ZipFile(zipFilePath)
    for name in zfile.namelist():
        (dirName, fileName) = os.path.split(name)
        if fileName == '':
            # directory
            newDir = destDir + os.path.sep + dirName
            if not os.path.exists(newDir):
                os.mkdir(newDir)
        else:
            # file
            fd = open(destDir + os.path.sep + name, 'wb')
            fd.write(zfile.read(name))
            fd.close()
    zfile.close()
fileName = sys.argv[1]
destDir = os.path.dirname(fileName)
if len(sys.argv) > 2:
	destDir = sys.argv[2]
unzipFile(fileName, destDir)