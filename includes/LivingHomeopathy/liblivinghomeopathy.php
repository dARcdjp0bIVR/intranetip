<?php
// Editing by 
/*
 * 2019-11-01 (Cameron): Fix sql in getPortalCourseData()
 * 2019-10-31 (Cameron): Modified getTopMenu(), change class from lm to dropdown for SchoolSettings for portal.php [case #P166116]
 * 2019-06-06 (Cameron): Modified getTopMenu(), add eService > School News [case #P161545]
 * 2018-05-16 (Carlos): Modified getTopMenu($Lang, $CurrentPageArr), added Staff Account Management.
 * 2017-06-30 (Carlos): Modified getPortalHomeworkData() to get counting of number of submitted, number of marked, and attachments info.
 */
if (!class_exists("libdb", false)) {
	include_once($intranet_root."/includes/libdb.php");
}
class liblivinghomeopathy extends libdb {
	public function __construct() {
		
		$this->libdb();
		$this->is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");
		
		global $sys_custom, $intranet_session_language;
		$this->lang = $intranet_session_language;
		$this->config['LivingHomeopathy'] = $sys_custom['LivingHomeopathy'];
		$this->config['Project_Custom'] = $sys_custom['Project_Custom'];
		$this->config['Project_Label'] = $sys_custom['Project_Label'];
	}
	
	public function isModuleOn()
	{
		return $this->config['LivingHomeopathy'];
	}
	
	public function getProjectIdentify() {
		return $this->config['Project_Label'];
	}
	
	public function getTopMenu($Lang, $CurrentPageArr)
	{
		global $lhomework, $plugin, $sys_custom, $stand_alone;
		
		//$DisableHomework = true;
		
		if (!isset($lhomework)) {
			include_once(dirname(dirname(__FILE__))."/libhomework.php");
			include_once(dirname(dirname(__FILE__))."/libhomework2007a.php");
			$lhomework = new libhomework2007();
		}
		$canAccessOfficalPhotoSetting = $_SESSION["SSV_PRIVILEGE"]["canAccessOfficalPhotoSetting"];
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || ($plugin['AccountMgmt_StudentRegistry'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($_SESSION['SESSION_ACCESS_RIGHT']['STUDENTREGISTRY']))) || ($plugin['imail_gamma']==true && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"]) || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting)
			$AccountMgmt = 1;

		$dhl_menu = array();
		$dhl_menu["leftMenu"]["home"]["link"] = "/home/index.php";
		$dhl_menu["leftMenu"]["home"]["title"] = $Lang['Header']['Menu']['Home'];
		$dhl_menu["leftMenu"]["home"]["selected"] = false;
		$dhl_menu["leftMenu"]["home"]["class"] = "home lm";
		$dhl_menu["leftMenu"]["home"]["status"] = true;
		
		if ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] || true) {
			$dhl_menu["leftMenu"]["eCalander"]["link"] = "/home/system_settings/school_calendar/";
			$dhl_menu["leftMenu"]["eCalander"]["title"] = $Lang['Header']['Menu']['SchoolCalendar'];
			$dhl_menu["leftMenu"]["eCalander"]["selected"] = $CurrentPageArr['SchoolCalendar'];
			$dhl_menu["leftMenu"]["eCalander"]["class"] = "lm";
			$dhl_menu["leftMenu"]["eCalander"]["status"] = true;
		}
		
		if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && !$DisableHomework 
				&& (
						$_SESSION['UserType']==USERTYPE_STUDENT
						|| ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])
						)
				){
					$dhl_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["link"] = "/home/eService/homework/index.php";
					$dhl_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["title"] = $Lang['Header']['Menu']['eHomework'];
					$dhl_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["selected"] = $CurrentPageArr['eServiceHomework'];
					$dhl_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["class"] = "";
					$dhl_menu["leftMenu"]["eService"]["child"]["eServiceHomework"]["status"] = true;
		}
		### eService -> eNotice
		if ($sys_custom['eNotice']['eServiceDisableStudent'] && $_SESSION['UserType']==USERTYPE_STUDENT) {
			$_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
		}
		if ($sys_custom['eNotice']['eServiceDisableParent'] && $_SESSION['UserType']==USERTYPE_PARENT) {
			$_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
		}
		if($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) {
			$_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
		}
		if($plugin['notice'] && !$_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"])
		{
			$dhl_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["link"] = "/home/eService/notice/student_notice/";
			$dhl_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["title"] = $Lang['Header']['Menu']['eNotice'];
			$dhl_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["selected"] = $CurrentPageArr['eServiceNotice'];
			$dhl_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["class"] = "";
			$dhl_menu["leftMenu"]["eService"]["child"]["eServiceNotice"]["status"] = true;
		}
		### eService -> eEnrolment
		$eEnrolTrialPast = false;
		if ($plugin['eEnrollment_trial'] != '' && date('Y-m-d') > $plugin['eEnrollment_trial'])
			$eEnrolTrialPast = true;
			
		// 2012-0814-1021-26073: student and parent can be eEnrol admin now
		//if($plugin['eEnrollment'] && !$eEnrolTrialPast && (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]))
		//if($plugin['eEnrollment'] && !$eEnrolTrialPast && (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] || $_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_PARENT))
		
		if($plugin['eEnrollment'] && !$eEnrolTrialPast)
		{
			$showButton = true;
			if ($_SESSION['UserType'] == USERTYPE_STAFF) {
				$showButton = false;
			}
			
			if ($showButton) {
				$CurrentPageArr['eService'] = true;
				$dhl_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["link"] = "/home/eService/enrollment/";
				$dhl_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["title"] = $Lang['Header']['Menu']['eEnrolment'];
				$dhl_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["selected"] = $CurrentPageArr['eServiceeEnrolment'];
				$dhl_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["class"] = "";
				$dhl_menu["leftMenu"]["eService"]["child"]["eServiceeEnrolment"]["status"] = true;
			}
		}
		
		if($plugin['iPortfolio'] && $_SESSION['UserType'] == USERTYPE_STUDENT) {
			$CurrentPageArr['eService'] = true;
			$dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["link"] = "/home/portfolio/";
			$dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["title"] = $Lang['Header']['Menu']['iPortfolio'];
			$dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["selected"] = $CurrentPageArr['iPortfolio'];
			$dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["class"] = "";
			$dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["status"] = true;
		}

		if(!$plugin['DisableNews'] && $_SESSION['UserType'] == USERTYPE_STUDENT) {
		    $CurrentPageArr['eService'] = true;
		    $dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["link"] = "/home/eService/schoolnews/";
		    $dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["title"] = $Lang['Header']['Menu']['schoolNews'];
		    $dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["selected"] = $CurrentPageArr['schoolNews'];
		    $dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["class"] = "";
		    $dhl_menu["leftMenu"]["eService"]["child"]["eServicePortfolio"]["status"] = true;
		}
		
		if (count($dhl_menu["leftMenu"]["eService"]["child"]) > 0) {
			
			if ($CurrentPageArr['eServiceHomework']) {
				$CurrentPageArr['eService'] = true;
			}
			
			$dhl_menu["leftMenu"]["eService"]["link"] = "#";
			$dhl_menu["leftMenu"]["eService"]["title"] = $Lang['Header']['Menu']['eService'];
			$dhl_menu["leftMenu"]["eService"]["selected"] = $CurrentPageArr['eService'];
			$dhl_menu["leftMenu"]["eService"]["class"] = "lm";
			$dhl_menu["leftMenu"]["eService"]["status"] = true;
		} else {
			unset($dhl_menu["leftMenu"]["eService"]);
		}
		
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"])
		{
			// added Staff Account on 2018-05-16
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StaffMgmt"]["link"] = "/home/eAdmin/AccountMgmt/StaffMgmt/";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StaffMgmt"]["title"] = $Lang['Header']['Menu']['StaffAccount'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StaffMgmt"]["selected"] = $CurrentPageArr['StaffMgmt'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StaffMgmt"]["class"] = "lm";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StaffMgmt"]["status"] = true;
			
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StudentMgmt"]["link"] = "/home/eAdmin/AccountMgmt/StudentMgmt/";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StudentMgmt"]["title"] = $Lang['Header']['Menu']['StudentAccount'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StudentMgmt"]["selected"] = $CurrentPageArr['StudentMgmt'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StudentMgmt"]["class"] = "lm";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["StudentMgmt"]["status"] = true;
		}
		
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"])
		{
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Class"]["link"] = "/home/system_settings/form_class_management/";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Class"]["title"] = $Lang['Header']['Menu']['Class'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Class"]["selected"] = $CurrentPageArr['Class'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Class"]["class"] = "lm";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Class"]["status"] = true;
		}
		
		if($plugin['iPortfolio']) {
			if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"] && $_SESSION['UserType']==USERTYPE_STAFF) {
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Portfolio"]["link"] = "/home/portfolio/";
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Portfolio"]["title"] = $Lang['Header']['Menu']['iPortfolio'];
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Portfolio"]["selected"] = $CurrentPageArr['iPortfolio'];
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Portfolio"]["class"] = "lm";
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Portfolio"]["status"] = true;
			}
		}
		
		/* if((
				$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] ||
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] ||
				$_SESSION['isTeaching'] ||
				($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) ||
				$lhomework->isViewerGroupMember($UserID)
			) || $_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework'])
		*/	
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] && !$DisableHomework)
		{
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["link"] = "/home/eAdmin/StudentMgmt/homework/index.php";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["title"] = $Lang['Header']['Menu']['eHomework'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["selected"] = $CurrentPageArr['eAdminHomework'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["class"] = "lm";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminHomework"]["status"] = true;
		}
		
		if($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) {
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["link"] = "/home/eAdmin/StudentMgmt/notice/student_notice/";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["title"] = $Lang['Header']['Menu']['eNotice'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["selected"] = $CurrentPageArr['eAdminNotice'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["class"] = "";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eAdminNotice"]["status"] = true;
		}
		
		## Special checking for eEnrolment
		$show_eAdmin_eEnrol = false;
		if ($plugin['eEnrollment'] && !$eEnrolTrialPast)
		{
			/* 20100604 Ivan: Now, get admin status in lib.php, function UPDATE_CONTROL_VARIABLE()
			 include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
			 $libenroll = new libclubsenrol();
			 
			 if (isset($header_lu) == false)
			 {
			 include_once ($PATH_WRT_ROOT."/includes/libuser.php");
			 $header_lu = new libuser($_SESSION['UserID']);
			 }
			 */
			
			if ($_SESSION['UserType']!=USERTYPE_STUDENT && $_SESSION['UserType']!=USERTYPE_PARENT)
			{
				if ($_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_admin"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_master"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_pic"])
					$show_eAdmin_eEnrol = true;
			}
		}
		## End Special checking for eEnrolment
		if($plugin['eEnrollment'])
		{
			if ($show_eAdmin_eEnrol)
			{
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["link"] = "/home/eAdmin/StudentMgmt/enrollment/";
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["title"] = $Lang['Header']['Menu']['eEnrolment'];
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["selected"] = $CurrentPageArr['eEnrolment'];
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["class"] = "";
				$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["eEnrolment"]["status"] = true;
			}
		}
		
		if (($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && !$plugin['DisableNews']) {
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["link"] = "/home/eAdmin/GeneralMgmt/schoolnews/";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["title"] = $Lang['Header']['Menu']['schoolNews'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["selected"] = $CurrentPageArr['schoolNews'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["class"] = "";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["SchoolNews"]["status"] = true;
		}

		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])
		{
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["link"] = "/home/system_settings/group/?clearCoo=1";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["title"] = $Lang['Header']['Menu']['Group'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["selected"] = $CurrentPageArr['Group'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["class"] = "lm";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Group"]["status"] = true;
		}
		
		if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) )
		{
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Subject"]["link"] = "/home/system_settings/subject_class_mapping/";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Subject"]["title"] = $Lang['Header']['Menu']['Subject'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Subject"]["selected"] = $CurrentPageArr['Subjects'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Subject"]["class"] = "lm";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Subject"]["status"] = true;
		}
		
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
		{
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Role"]["link"] = "/home/system_settings/role_management/";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Role"]["title"] = $Lang['Header']['Menu']['Role'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Role"]["selected"] = $CurrentPageArr['Role'];
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Role"]["class"] = "lm";
			$dhl_menu["leftMenu"]["SchoolSettings"]["child"]["Role"]["status"] = true;
		}
		
		if (count($dhl_menu["leftMenu"]["SchoolSettings"]["child"]) > 0) {
			if ($CurrentPageArr['StaffMgmt'] || $CurrentPageArr['StudentMgmt'] || $CurrentPageArr['schoolNews'] || $CurrentPageArr['eAdminNotice']
					|| $CurrentPageArr['Group'] || $CurrentPageArr['Class'] || $CurrentPageArr['Subjects'] || $CurrentPageArr['eAdminHomework'] || $CurrentPageArr['eAdminNotice'] || $CurrentPageArr['iPortfolio'] || $CurrentPageArr['Role']) {
						$CurrentPageArr['SchoolSettings'] = true;
					} else {
						$CurrentPageArr['SchoolSettings'] = false;
					}
					
					$dhl_menu["leftMenu"]["SchoolSettings"]["link"] = "#";
					$dhl_menu["leftMenu"]["SchoolSettings"]["title"] = $Lang['Header']['Menu']['SchoolSettings'];
					$dhl_menu["leftMenu"]["SchoolSettings"]["selected"] = $CurrentPageArr['SchoolSettings'];
                    if ($sys_custom['LivingHomeopathy'] && stristr($_SERVER['REQUEST_URI'], 'templates/LivingHomeopathy/portal.php') !== false) {
                        $class = 'dropdown';
                    }
                    else {
                        $class = 'lm';
                    }
					$dhl_menu["leftMenu"]["SchoolSettings"]["class"] = $class;
					$dhl_menu["leftMenu"]["SchoolSettings"]["status"] = true;
		} else {
			unset($dhl_menu["leftMenu"]["SchoolSettings"]);
		}
		
		$dhl_menu["rightMenu"]["userIdentify"]["link"] = "#";
		$dhl_menu["rightMenu"]["userIdentify"]["title"] = "";
		$dhl_menu["rightMenu"]["userIdentify"]["selected"] = false;
		$dhl_menu["rightMenu"]["userIdentify"]["class"] = "rm";
		$dhl_menu["rightMenu"]["userIdentify"]["status"] = true;
		
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["link"] = "/home/iaccount/account/";
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["title"] = $Lang['Header']['Menu']['iAccount'];
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["selected"] = false;
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["class"] = "";
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["iAccount"]["status"] = true;
		
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["link"] = "/home/iaccount/account/login_password.php";
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["title"] = $Lang['Header']['Menu']['ChangePassword'];
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["selected"] = false;
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["class"] = "";
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["ChangePassword"]["status"] = true;
		
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["link"] = "#";
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["title"] = $Lang['Header']['Logout'];
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["selected"] = false;
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["class"] = "logout";
		$dhl_menu["rightMenu"]["userIdentify"]["child"]["Logout"]["status"] = true;
		
		$dhl_menu["rightMenu"]["language"]["link"] = "#";
		$dhl_menu["rightMenu"]["language"]["title"] = $this->lang=="en" ?  $Lang[$this->config['Project_Label']]['English'] : $Lang[$this->config['Project_Label']]['Chinese'];
		$dhl_menu["rightMenu"]["language"]["selected"] = false;
		$dhl_menu["rightMenu"]["language"]["class"] = "language rm";
		$dhl_menu["rightMenu"]["language"]["status"] = true;
		$dhl_menu["rightMenu"]["language"]["child"]["eng"]["link"] = "/lang.php?lang=en";
		$dhl_menu["rightMenu"]["language"]["child"]["eng"]["title"] = $Lang[$this->config['Project_Label']]['English'];
		$dhl_menu["rightMenu"]["language"]["child"]["eng"]["selected"] = ($this->lang== "en");
		$dhl_menu["rightMenu"]["language"]["child"]["eng"]["class"] = "language_en";
		$dhl_menu["rightMenu"]["language"]["child"]["eng"]["status"] = true;
		
		$dhl_menu["rightMenu"]["language"]["child"]["b5"]["link"] = "/lang.php?lang=b5";
		$dhl_menu["rightMenu"]["language"]["child"]["b5"]["title"] = $Lang[$this->config['Project_Label']]['Chinese'];
		$dhl_menu["rightMenu"]["language"]["child"]["b5"]["selected"] = ($this->lang != "en");
		$dhl_menu["rightMenu"]["language"]["child"]["b5"]["class"] = "language_b5";
		$dhl_menu["rightMenu"]["language"]["child"]["b5"]["status"] = true;
		
		$dhl_menu["rightMenu"]["otherFunction"]["link"] = "#";
		$dhl_menu["rightMenu"]["otherFunction"]["title"] = "";
		$dhl_menu["rightMenu"]["otherFunction"]["selected"] = false;
		$dhl_menu["rightMenu"]["otherFunction"]["class"] = "";
		$dhl_menu["rightMenu"]["otherFunction"]["status"] = $_SESSION['UserType'] == USERTYPE_STAFF;
		
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["Documentation"]["link"] = "javascript:newWindow('http://support.broadlearning.com/doc/help/portal/',24);";
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["Documentation"]["target"] = "";
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["Documentation"]["title"] = $Lang['Header']['OnlineDocs'];
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["Documentation"]["selected"] = false;
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["Documentation"]["class"] = "";
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["Documentation"]["status"] = true;
		
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["ReprintCardSystem"]["link"] = "/home/eClassStore/redirect.php";
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["ReprintCardSystem"]["target"] = "_blank";
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["ReprintCardSystem"]["title"] = $Lang['Header']['ReprintCardSystem'];
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["ReprintCardSystem"]["selected"] = false;
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["ReprintCardSystem"]["class"] = "";
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["ReprintCardSystem"]["status"] = true;
		
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["iPortfolioBuy"]["link"] = "/home/eClassStore/redirect.php?module=iPortfolio";
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["iPortfolioBuy"]["target"] = "_blank";
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["iPortfolioBuy"]["title"] = $Lang['Header']['iPortbuy'];
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["iPortfolioBuy"]["selected"] = false;
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["iPortfolioBuy"]["class"] = "";
		$dhl_menu["rightMenu"]["otherFunction"]["child"]["iPortfolioBuy"]["status"] = true;
		
		return $dhl_menu;
	}
	
	public function getPortalSchoolNewsData($args = array()) {
		global $Lang, $user_field, $i_AnnouncementWholeSchool, $i_AnnouncementSystemAdmin, $intranet_root;
		if (count($args) > 0) extract($args);
		
		if (!isset($pagelimit) || empty($pagelimit)) $pagelimit = 2;
		
		include_once($intranet_root.'/includes/libschoolnews.php');
		$lschoolnews = new libschoolnews();
		include_once($intranet_root."/includes/libuser.php");
		$LibUser = new libuser($_SESSION['UserID']);
		
		if ($_SESSION['UserType'] != USERTYPE_STUDENT) {
			$schoolNewsURL = "/home/eAdmin/GeneralMgmt/schoolnews/read.php?AnnouncementID=";
		} else {
			$schoolNewsURL = "/home/eAdmin/GeneralMgmt/schoolnews/view.php?AnnouncementID=";
		}
		$schoolNewsURL = "/home/view_announcement.php?ct=0&AnnouncementID=";
		
		$schoolNewsArr = array();
		$user_field = getNameFieldWithClassNumberByLang("b.");
		
		# SQL statement
		$allStatus = " a.RecordStatus IN (1)";
		$status = ($status == "") ? $allStatus : "a.RecordStatus = $status";
		
		$postedByField = " IF (a.UserID IS NOT NULL OR a.UserID != 0, $user_field,'$i_AnnouncementSystemAdmin'), ";
		$fromGroupField = " IF (a.OwnerGroupID IS NOT NULL OR a.OwnerGroupID != 0, c.Title, '--'), ";
		$targetField = " IF(d.GroupAnnouncementID IS NULL, '$i_AnnouncementWholeSchool','". $Lang['SchoolNews']['SpecificGroup'] ."') as target_group, ";
		
		$fields = "a.AnnouncementID, DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d') as AnnouncementDate,
		DATE_FORMAT(a.EndDate, '%Y-%m-%d') as EndDate, a.Title,
		a.DateModified,
		a.OwnerGroupID,
		$targetField
		a.RecordStatus";

		$dbtables = "INTRANET_ANNOUNCEMENT AS a
				LEFT JOIN INTRANET_USER AS b ON (b.UserID = a.UserID)
				LEFT JOIN INTRANET_GROUP AS c ON (c.GroupID = a.OwnerGroupID)
				LEFT JOIN INTRANET_GROUPANNOUNCEMENT AS d on (d.AnnouncementID=a.AnnouncementID)
				 ";
		// $conds = " a.AnnouncementDate > '" . date("Y-m-d") . "' AND ";
		$conds .= " $status";
		if ($LibUser->IsStudent()) {
			$conds .= " AND (d.GroupAnnouncementID IS NULL OR d.GroupID IN (SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID like '" . $_SESSION["UserID"] . "'))";
		}
		$conds .= " AND a.AnnouncementDate <= CURDATE() ";
		if (!$lschoolnews->allowUserToViewPastNews) {
			$conds .= " AND a.EndDate >= CURDATE() ";
		}
		$groupby = " group by a.AnnouncementID ";
		$strSQL = "SELECT $fields FROM $dbtables WHERE $conds $groupby";
		$strSQL .= "  ORDER BY a.AnnouncementDate DESC, a.EndDate DESC, a.Title ASC LIMIT 0, " . $pagelimit;
		// echo $strSQL;
		$result = $this->returnResultSet($strSQL);
		if (count($result) > 0) {
			foreach ($result as $sc_index => $sc_value) {
				$schoolNewsArr[$sc_value["AnnouncementID"]] = array(
					"link" => $schoolNewsURL . $sc_value["AnnouncementID"],
					"title" => $sc_value["Title"],	
					"start_date" => $sc_value["AnnouncementDate"]
				);
			}
		}
		/*
		$schoolNewsArr = array(
				"992" => array(
						"link" => $schoolNewsURL . "992",
						"title" => "van test push message school news",
						"start_date" => "2017-04-10"
				)	,
				"993" => array(
						"link" => $schoolNewsURL . "99e",
						"title" => "van test push message school news",
						"start_date" => "2017-04-10"
				)
		);*/
		return $schoolNewsArr;
	}
	public function getPortalCourseData($args) {
		global $intranet_root, $PATH_WRT_ROOT;
		if (count($args) > 0) extract($args);
		if (!isset($pagelimit) || empty($pagelimit)) $pagelimit = 2;
		
		$AcademicYearID = Get_Current_Academic_Year_ID();
		include_once($intranet_root."/includes/libclubsenrol.php");
		include_once($intranet_root."/includes/libuser.php");
		$LibUser = new libuser($_SESSION['UserID']);
		$libenroll = new libclubsenrol($AcademicYearID);
		
		
		
		### Get the data of all clubs
		$clubInfoAry = $libenroll->Get_All_Club_Info();
		$clubEnrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
		$clubInfoAssoAry = BuildMultiKeyAssoc($clubInfoAry, 'EnrolGroupID');
		unset($clubInfoAry);

		$strSQL = "SELECT iee.EnrolEventID, EventTitle AS Title, Description FROM INTRANET_ENROL_EVENTINFO AS iee";
		$strSQL .= " JOIN INTRANET_ENROL_EVENT_DATE AS ieed ON (iee.EnrolEventID=ieed.EnrolEventID AND ieed.RecordStatus='1')";
		$strSQL .= " WHERE ActivityDateStart >= NOW() AND ieed.RecordStatus='1'";
		$strSQL .= " GROUP BY iee.EnrolEventID";
		$result = $this->returnResultSet($strSQL);
		$eventInfoAssoAry = BuildMultiKeyAssoc($result, 'EnrolEventID');

		if ($LibUser->isTeacherStaff()){
			$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
			$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
			$currentUserType = "T";
			if ($isEnrolAdmin || $isEnrolMaster) {
				$myEnrolGroupID = array_keys($clubInfoAssoAry);
				$myEnrolEventID = array_keys($eventInfoAssoAry);
			} else {
				$strSQL = "SELECT EnrolGroupID FROM INTRANET_ENROL_GROUPSTAFF WHERE UserID='" . $_SESSION["UserID"] . "' GROUP BY EnrolGroupID";
				$result = $this->returnResultSet($strSQL);
				$myEnrolGroupID = Get_Array_By_Key($result, 'EnrolGroupID');
				$strSQL = "SELECT EnrolEventID FROM INTRANET_ENROL_EVENTSTAFF WHERE UserID='" . $_SESSION["UserID"] . "' GROUP BY EnrolEventID";
				$result = $this->returnResultSet($strSQL);
				$myEnrolEventID = Get_Array_By_Key($result, 'EnrolEventID');
			}
		} else if ($LibUser->IsStudent()) {
			$ApplicantID = $_SESSION['UserID'];
			$currentUserType = "S";
			$strSQL = "SELECT EnrolGroupID FROM INTRANET_ENROL_GROUPSTUDENT WHERE RecordStatus='2' AND StudentID='" . $ApplicantID. "' GROUP BY EnrolGroupID";
			$result = $this->returnResultSet($strSQL);
			$myEnrolGroupID = Get_Array_By_Key($result, 'EnrolGroupID');

			$strSQL = "SELECT EnrolEventID FROM INTRANET_ENROL_EVENTSTUDENT WHERE RecordStatus='2' AND StudentID='" . $ApplicantID. "' GROUP BY EnrolEventID";
			$result = $this->returnResultSet($strSQL);
			$myEnrolEventID = Get_Array_By_Key($result, 'EnrolEventID');
		}

		$CourseArr = array();
		$FullCourseArr = array();
		if (count($myEnrolGroupID) > 0) {
			$strSQL = "SELECT iegd.EnrolGroupID, MIN(iegd.ActivityDateStart) AS ActivityDateStart";
			$strSQL .= " FROM INTRANET_ENROL_GROUP_DATE as iegd";
			$strSQL .= " WHERE iegd.ActivityDateStart >= NOW() AND iegd.RecordStatus='1' AND iegd.EnrolGroupID in (" . implode(", ", $myEnrolGroupID) . ")";
			$strSQL .= " GROUP BY iegd.EnrolGroupID ORDER BY iegd.ActivityDateStart DESC LIMIT 0, " . $pagelimit;
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				foreach ($result as $kk => $vv) {
					if (!$LibUser->IsStudent()) {
						if (!isset($GroupApplicantID) || empty($GroupApplicantID)) {
							$strSQL = "SELECT StudentID FROM INTRANET_ENROL_GROUPSTUDENT WHERE RecordStatus='2' AND EnrolGroupID='" . $vv["EnrolGroupID"]. "' LIMIT 1";
							$result = $this->returnResultSet($strSQL);
							$GroupApplicantID = $result[0]["StudentID"];
							$ApplicantID = $GroupApplicantID;
							$GroupApplicantID = "";
						}
					}
					if ($ApplicantID > 0) {
						$FullCourseArr["CLUB_" . $vv["EnrolGroupID"]] = array(
							"link" => "/home/eService/enrollment/group_info.php?EnrolGroupID=" . $vv["EnrolGroupID"] . "&applicantId=" . $ApplicantID,
							"ID" => $vv["EnrolGroupID"],
							"title" => $clubInfoAssoAry[$vv["EnrolGroupID"]]["Title"],
							"description" => $clubInfoAssoAry[$vv["EnrolGroupID"]]["Description"],
							"Next_Lesson" => $vv["ActivityDateStart"]
						);
						$key = sprintf("%d_C_%010d", strtotime($vv["ActivityDateStart"]), $vv["EnrolGroupID"]);
						$sortArr[$key] = "CLUB_" . $vv["EnrolGroupID"];
					}
				}
			}
		}
		if (count($myEnrolEventID) > 0) {
			$strSQL = "SELECT ieed.EnrolEventID, MIN(ieed.ActivityDateStart) AS ActivityDateStart";
			$strSQL .= " FROM INTRANET_ENROL_EVENT_DATE as ieed";
			$strSQL .= " JOIN INTRANET_ENROL_EVENTINFO  as iee on (iee.EnrolEventID=ieed.EnrolEventID) ";
			$strSQL .= " WHERE ieed.ActivityDateStart >= NOW() AND ieed.RecordStatus='1' AND ieed.EnrolEventID in (" . implode(", ", $myEnrolEventID) . ")";
			$strSQL .= " GROUP BY ieed.EnrolEventID ORDER BY ieed.ActivityDateStart DESC LIMIT 0, " . $pagelimit;
			
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				foreach ($result as $kk => $vv) {
					if (!$LibUser->IsStudent()) {
						if (!isset($EventApplicantID) || empty($EventApplicantID)) {
							$strSQL = "SELECT StudentID FROM INTRANET_ENROL_EVENTSTUDENT WHERE RecordStatus='2' AND EnrolEventID='" . $vv["EnrolEventID"]. "' LIMIT 1";
							$result = $this->returnResultSet($strSQL);
							$EventApplicantID= $result[0]["StudentID"];
							$ApplicantID = $EventApplicantID;
							$EventApplicantID = "";
						}
					}
					if ($ApplicantID > 0) {
						$FullCourseArr["ACTIVITY_" . $vv["EnrolEventID"]] = array(
								"link" => "/home/eService/enrollment/event_info.php?EnrolEventID=" . $vv["EnrolEventID"] . "&applicantId=" . $ApplicantID,
								"ID" => $vv["EnrolEventID"],
								"title" => $eventInfoAssoAry[$vv["EnrolEventID"]]["Title"],
								"description" => $eventInfoAssoAry[$vv["EnrolEventID"]]["Description"],
								"Next_Lesson" => $vv["ActivityDateStart"]
						);
						$key = sprintf("%d_E_%010d", strtotime($vv["ActivityDateStart"]), $vv["EnrolEventID"]);
						$sortArr[$key] = "ACTIVITY_" . $vv["EnrolEventID"];
					}
				}
			}
		}
		if (count($sortArr) > 0) {
			ksort($sortArr);
			$i = 0;
			foreach ($sortArr as $kk => $vv) {
				$CourseArr[$vv] = $FullCourseArr[$vv];
				$i++;
				if ($pagelimit <= $i) {
					break;
				}
			}
		}
		return $CourseArr;
	}
	
	public function getPortalHomeworkData($args = array()) {
		if (count($args) > 0) extract($args);
		global $intranet_root, $file_path, $sys_custom, $lhomework, $intranet_session_language;

		if (!isset($pagelimit) || empty($pagelimit)) $pagelimit = 4;
		$HomeworkArr = array();
		$today = date("Y-m-d");
		$UserID = $_SESSION['UserID'];
		$yearID = Get_Current_Academic_Year_ID();
		# Current Year Term
		$currentYearTerm = getAcademicYearAndYearTermByDate($today);
		$yearTermID = $currentYearTerm[0];
		$yearTermID = $yearTermID ? $yearTermID : 0;

		if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
			
			$subject = $lhomework->getStudyingSubjectList($UserID, "", $yearID, $yearTermID);
			if (count($subject) > 0) {
				$subjectAssoAry = BuildMultiKeyAssoc($subject, 'RecordID');
				$subject_cond = "a.SubjectID IN (" . implode(", ", array_keys($subjectAssoAry)) . ") AND ";
			}
			$strSQL = "	SELECT
							a.HomeworkID,
							IF('b5' = '"  .$intranet_session_language . "', b.EN_DES, b.CH_DES) AS Subject,
							IF('b5' = '"  .$intranet_session_language . "', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,
							a.Title,
							a.Description,
							a.StartDate, a.DueDate,
							a.AttachmentPath 
						FROM
							INTRANET_HOMEWORK as a
						LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
						LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
						LEFT OUTER JOIN SUBJECT_TERM_CLASS_USER AS d ON d.SubjectGroupID = a.ClassGroupID
						LEFT OUTER JOIN INTRANET_HOMEWORK_HANDIN_LIST AS e ON (e.HomeworkID = a.HomeworkID AND e.StudentID = d.UserID)
						WHERE " . $subject_cond . " d.UserID ='" . $UserID. "' AND a.HandinRequired=1 AND a.AcademicYearID='" . $yearID . "' AND e.RecordStatus NOT IN (1)
						GROUP BY a.HomeworkID ORDER BY a.StartDate DESC";
			$strSQL .= " LIMIT 0, " . $pagelimit;
			$result = $this->returnResultSet($strSQL);

			if (count($result) > 0) {
				foreach ($result as $key => $value) {
					
					$attachment = array();
					if($value["AttachmentPath"] != ''){
						$attachment_dir_path = $file_path.'/file/homework/'.$value["AttachmentPath"].$value["HomeworkID"];
						$attachment_file_paths = explode("\n",trim(shell_exec('find \''.$attachment_dir_path.'\' -maxdepth 1 -type f')));
						for($j=0;$j<count($attachment_file_paths);$j++){
							$attachment[] = array("fullpath"=>$attachment_file_paths[$j],
													"filename"=>substr($attachment_file_paths[$j],strrpos($attachment_file_paths[$j],'/')+1),
													"filepath"=>str_replace($file_path,'',$attachment_file_paths[$j]),
													"is_img"=>isImage($attachment_file_paths[$j])
											);
						}
					}
					
					$HomeworkArr[$value["HomeworkID"]] = array(
							"link" => "#", // /home/eService/homework/management/homeworklist/index.php",
							"topic" => $value["Title"],
							"start_date" => $value["StartDate"],
							"due_date" => $value["DueDate"],
							"attachment" => $attachment,
							"handin_total" => 0,
							"handin_submitted" => "0",
							"handin_marked" => "0"
					);
				}
			}
		} else if($_SESSION['UserType'] == USERTYPE_STAFF){
			$UserIDTmp = $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ? "" : $UserID;
			
			$subject = $lhomework->getTeachingSubjectList($UserIDTmp, $yearID, $yearTermID, $classID);
			$subjectAssoAry = BuildMultiKeyAssoc($subject, 'RecordID');
			$subjectGroups = $lhomework->getTeachingSubjectGroupList($UserIDTmp,"", $yearID, $yearTermID, $classID);
			if (count($subjectGroups) > 0) {
				$subjectGroupsAssoAry = BuildMultiKeyAssoc($subjectGroups, 'SubjectGroupID');
				$subjectGroup_cond = "a.ClassGroupID in (" . implode(",", array_keys($subjectGroupsAssoAry)) . ") AND ";
			}
			$strSQL = "SELECT
							a.HomeworkID,
							IF('b5' = '"  .$intranet_session_language . "', b.EN_DES, b.CH_DES) AS Subject,
							IF('b5' = '"  .$intranet_session_language . "', c.ClassTitleEN, c.ClassTitleB5) AS SubjectGroup,
							a.Title,
							a.Description,
							a.StartDate,
							a.DueDate,
							a.AttachmentPath, 
							COUNT(h.RecordID) as handin_total,
							SUM(IF(h.StudentDocument IS NOT NULL,1,0)) as handin_submitted,
							SUM(IF(h.TeacherDocument IS NOT NULL,1,0)) as handin_marked 
						FROM INTRANET_HOMEWORK as a
						LEFT OUTER JOIN ASSESSMENT_SUBJECT as b ON b.RecordID = a.SubjectID
						LEFT OUTER JOIN SUBJECT_TERM_CLASS as c ON c.SubjectGroupID = a.ClassGroupID
						LEFT JOIN SUBJECT_TERM_CLASS_TEACHER stct ON (stct.SubjectGroupID=a.ClassGroupID AND stct.UserID='1') 
						LEFT JOIN INTRANET_HOMEWORK_HANDIN_LIST AS h ON h.HomeworkID = a.HomeworkID 
						WHERE
							" . $subjectGroup_cond . "
							a.AcademicYearID='" . $yearID . "'
							AND a.HandinRequired=1
						GROUP BY a.HomeworkID ORDER BY a.StartDate DESC";
			$strSQL .= " LIMIT 0, " . $pagelimit;
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				foreach ($result as $key => $value) {
					
					$attachment = array();
					if($value["AttachmentPath"] != ''){
						$attachment_dir_path = $file_path.'/file/homework/'.$value["AttachmentPath"].$value["HomeworkID"];
						$attachment_file_paths = explode("\n",trim(shell_exec('find \''.$attachment_dir_path.'\' -maxdepth 1 -type f')));
						for($j=0;$j<count($attachment_file_paths);$j++){
							$attachment[] = array("fullpath"=>$attachment_file_paths[$j],
													"filename"=>substr($attachment_file_paths[$j],strrpos($attachment_file_paths[$j],'/')+1),
													"filepath"=>str_replace($file_path,'',$attachment_file_paths[$j]),
													"is_img"=>isImage($attachment_file_paths[$j])
											);
						}
					}
					
					$HomeworkArr[$value["HomeworkID"]] = array(
						"link" => "",
						"topic" => $value["Title"],	
						"start_date" => $value["StartDate"],
						"due_date" => $value["DueDate"],
						"attachment" => $attachment,
						"handin_total" => $value["handin_total"],
						"handin_submitted" => $value["handin_submitted"],
						"handin_marked" => $value["handin_marked"]
					);
				}
			}
		}
		return $HomeworkArr;
	}
	
	public function getPortalNoticeData($args) {
		global $intranet_root, $file_path;
		if (count($args) > 0) extract($args);
		
		include_once($intranet_root.'/includes/libnotice.php');
		include_once($intranet_root."/includes/libuser.php");
		include_once($intranet_root."/includes/libfilesystem.php");
		include_once($intranet_root.'/includes/libfiletable.php');
		
		$lnotice = new libnotice();
		
		if (!isset($pagelimit) || empty($pagelimit)) $pagelimit = 4;
		if ($status != 2 && $status != 3) $status = 1;
		
		$isPIC = $lnotice->isNoticePIC();
		$TargetType = "S";
		if ($UserType == USERTYPE_STUDENT) {
			$conds = "";
			if ($signStatus == 0){
				$signStatus_conds = "";
			}else if($signStatus == 1){
				$signStatus_conds = " AND c.SignerID IS NULL ";
			}else if($signStatus == 2){
				$signStatus_conds = " AND c.SignerID IS NOT NULL ";
			}
			if($keyword!=''){
				$keywords_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%')) ";
			}
			else{
				$keywords_conds = "";
			}
			
			if ($UserType == USERTYPE_STUDENT) {
				$conds .= " AND c.StudentID = '" . $_SESSION["UserID"] . "'";
			}
			$name_field = getNameFieldWithClassNumberByLang("b.");
			$name_field2 = getNameFieldWithLoginByLang("d.");
			$strSQL = "SELECT
						a.NoticeID,
						DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i') AS DateStart,
						DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i') AS DateEnd,
						if(a.IsModule=1,'--',a.NoticeNumber),a.Title,
						a.RecordType, c.RecordType, c.RecordStatus,
						IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
						c.DateModified
					FROM INTRANET_NOTICE_REPLY as c
					LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
					LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
					LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
					WHERE
						a.RecordStatus = 1 
						AND a.DateStart<= NOW()
						$conds
						AND a.IsDeleted=0 and a.TargetType='$TargetType'
						$signStatus_conds
						ORDER BY a.DateStart DESC LIMIT 0, " . $pagelimit;
			
			$result = $this->returnResultSet($strSQL);
		} else {
			$strSQL = $lnotice->returnNoticeListTeacherView($status,$year,$month,1, $keyword, "ALL", $TargetType, 1);
			$strSQL .= " ORDER BY a.DateStart DESC LIMIT 0, " . $pagelimit;
			$result = $this->returnResultSet($strSQL);
		}
		$NoticeArr = array();
		if (count($result) > 0) {
			foreach ($result as $eN_index => $eN_data) {
				if (isset($eN_data["DateStart"])) {
					$notice_date= $eN_data["DateStart"];
				} else {
					$tmpData = array_values($eN_data);
					$notice_date = $tmpData[1];
				}
				
				$NoticeArr[$eN_data["NoticeID"]] = array(
					"link" => "#",
					"title" => $eN_data["Title"],
					"date" => $notice_date,
					"attachment" => "",
					"is_img" => false
				);
			}
		}
		if (count($NoticeArr) > 0) {
			$strSQL = "SELECT NoticeID, Attachment FROM INTRANET_NOTICE WHERE NoticeID in (" . implode(", ", array_keys($NoticeArr)) . ")";
			$result = $this->returnResultSet($strSQL);
			
			$path = "$file_path/file/notice/";
			$imageArr = array(".jpg", ".png", ".gif", ".bmp");
			
			if (count($result) > 0) {
				foreach ($result as $index => $attrData) {
					$notice_path = $path . $attrData["Attachment"];
					$a = new libfiletable("", $notice_path, 0, 0, "");
					$f = new libfilesystem();
					$files = $a->files;
					if (count($files) > 0) {
						while (list($key, $value) = each($files))
						{
							$NoticeArr[$attrData["NoticeID"]]["attachment"][$key] = $value;
							$NoticeArr[$attrData["NoticeID"]]["attachment"][$key]["filepath"] = "/file/notice/" . $attrData["Attachment"] . "/" . $value[0];
							$NoticeArr[$attrData["NoticeID"]]["attachment"][$key]["fullpath"] = $path . $attrData["Attachment"] . "/" . $value[0];
							$NoticeArr[$attrData["NoticeID"]]["attachment"][$key]["ext"] = strtolower($f->file_ext($value[0]));
							$NoticeArr[$attrData["NoticeID"]]["attachment"][$key]["size"] = ceil(((int)$value[1] / 1024) * 100) / 100;
							if (in_array($NoticeArr[$attrData["NoticeID"]]["attachment"][$key]["ext"], $imageArr)) {
								$NoticeArr[$attrData["NoticeID"]]["attachment"][$key]["is_img"] = true;
							} else {
								$NoticeArr[$attrData["NoticeID"]]["attachment"][$key]["is_img"] = false;
							}
						}
					}
				}
			}
		}
		return $NoticeArr;
	}
	
	public function hasIPortfolio($UserID) {
		global $eclass_db;
		$strSQL = "SELECT RecordID FROM " . $eclass_db . ".PORTFOLIO_STUDENT WHERE UserID='" . $UserID . "' AND IsSuspend='0'";
		$result = $this->returnResultSet($strSQL);
		if (count($result) > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public function getLivingHomeopathyPersonalProfileRecords($filterMap)
	{
		global $intranet_db, $eclass_db;
		
		$conds = "";
		if(isset($filterMap['UserID'])){
			$user_id = IntegerSafe($filterMap['UserID']);
			$conds .= " AND UserID ".(is_array($user_id)?" IN (".implode(",",$user_id).") " : "='$user_id' ");
		}
		
		$sql = "SELECT * FROM ".$eclass_db.".LIVING_HOMEOPATHY_PERSONAL_PROFILE WHERE 1 ".$conds;
		$records = $this->returnResultSet($sql);
		return $records;
	}
	
	public function upsertLivingHomeopathyPersonalProfileRecord($user_id, $profile)
	{
		global $intranet_db, $eclass_db;
		
		$sql = "UPDATE ".$eclass_db.".LIVING_HOMEOPATHY_PERSONAL_PROFILE SET Profile='".$profile."',DateModified=NOW(),ModifiedBy='".$_SESSION['UserID']."' WHERE UserID='".IntegerSafe($user_id)."' ";
		$success = $this->db_db_query($sql);
		if($this->db_affected_rows() == 0){
			$sql = "INSERT INTO ".$eclass_db.".LIVING_HOMEOPATHY_PERSONAL_PROFILE (UserID,Profile,DateInput,DateModified,InputBy,ModifiedBy) VALUES ('".IntegerSafe($user_id)."','".$profile."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
			$success = $this->db_db_query($sql);
		}
		
		return $success;
	}
	
	public function getLivingHomeopathyPersonalProfileRecordStructure($libuser=null)
	{
		$user_fields = array("ChineseName",
							"EnglishName",
							"Photo",
							"Gender",
							"HKID",
							"DateOfBirth",
							"UserEmail",
							"HomeTelNo",
							"MobileTelNo",
							"Address");
		
		$genders = array(array("",""),array("男","男"), array("女","女"));
		$gender_map = array("M"=>"男","F"=>"女");
		$education_levels = array(array("小學","小學"), array("中學","中學"), array("大專","大專"), array("大學","大學"));
		$ability_levels = array(array("優良","優良"), array("一般","一般"), array("較差","較差"));
		
		$data = array(
				"ChineseName"=>"",
				"EnglishName"=>"",
				"Photo"=>"",
				"Gender"=>"",
				"HKID"=>"",
				"DateOfBirth"=>"",
				"UserEmail"=>"",
				"HomeTelNo"=>"",
				"MobileTelNo"=>"",
				"Address"=>"",
				"Education"=>
					array(
						//array("Level"=>"","SchoolName"=>"","Department"=>"","Major"=>"","Minor"=>"","Start"=>"","End"=>"","Period"=>"")
					),
				"Training"=>
					array(
						//array("Organization"=>"","CourseDescription"=>"","Start"=>"","End"=>"","Period"=>"")
					),
				"WorkingExperience"=>
					array(
						//array("Position"=>"","JobTitle"=>"","Company"=>"","Start"=>"","End"=>"")
					),
				"SocialWork"=>
					array(
						//array("Community"=>"","Start"=>"","End"=>"","Period"=>"","Job"=>"")
					),
				"LanguageSkills"=>
					array(
						array("Language"=>"中文","Speech"=>"","Reading"=>"","Writing"=>"","Listening"=>""),
						array("Language"=>"英語","Speech"=>"","Reading"=>"","Writing"=>"","Listening"=>"")
					),
				"Expertise"=>
					array(
						"","",""
					),
				"ProfessionalSkills"=>
					array(
						"","",""
					),
				"Others"=>""
				);
		
		if($libuser){
			foreach($user_fields as $field){
				if(isset($libuser->$field)){
					if($field == 'Gender'){
						$data[$field] = $gender_map[$libuser->$field];
					}else{
						$data[$field] = $libuser->$field;
					}
				}
			}
		}		
		
		return $data;
	}
	
}

?>