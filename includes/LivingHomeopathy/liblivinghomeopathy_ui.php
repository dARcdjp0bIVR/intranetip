<?php
// Editing by 
/*
 *  2019-10-31 Cameron
 *      - change pagelimit form 2/4 to 5 in getPortalHTML() [case #P166116]
 *      - set include file to top_menu_appview.php for portal page in getTopMenuHTML()
 *      - set include file to portal_main_appview.php for portal page in getPortalHTML()
 */
if (!class_exists("liblivinghomeopathy", false)) {
	include_once($intranet_root."/includes/LivingHomeopathy/liblivinghomeopathy.php");
}
class liblivinghomeopathy_ui extends liblivinghomeopathy {
	public function __construct() {
		parent::__construct();
		$this->view_path = dirname(dirname(dirname(__FILE__))) . "/templates/" . $this->config['Project_Label']. "/views/";
	}

	public function getViewPath() {
		return $this->view_path;
	}
	
	public function getTopMenuHTML($args)
	{
		global $li_hp, $CurrentPageArr, $lhomework, $sys_custom;
		extract($args);
		$dhl_menu = $this->getTopMenu($Lang, $CurrentPageArr);
		unset($args);

        if ($sys_custom['LivingHomeopathy'] && stristr($_SERVER['REQUEST_URI'], 'templates/LivingHomeopathy/portal.php') !== false) {
            include_once($this->view_path . "top_menu_appview.php");
        }
        else {
            include_once($this->view_path . "top_menu.php");
        }

		unset($source_path);
		unset($dhl_menu);
		return $topMenuCustomization;
	}
	
	public function getPortalHTML($args) {
		global $li_hp, $ln, $lp, $li, $lhomework, $sys_custom, $intranet_root, $file_path;
		extract($args);
		$dhl_menu = $this->getTopMenu($Lang, $CurrentPageArr);
		unset($args);

		$no_eNotice = 0;
		$no_homework = 0;
		
		$hasIPortfolio = $this->hasIPortfolio($_SESSION["UserID"]);
		
		$schoolNewsArr = $this->getPortalSchoolNewsData(array("pagelimit" => 5));
		$CourseArr = $this->getPortalCourseData(array("pagelimit" => 5));
		$HomeworkArr = $this->getPortalHomeworkData(array("pagelimit" => 5));
		$NoticeArr = $this->getPortalNoticeData(array("pagelimit" => 5, "UserType" => $_SESSION["UserType"]));
		
		$no_homework = count($HomeworkArr);
		$no_eNotice = count($NoticeArr);

//        if ($sys_custom['LivingHomeopathy'] && stristr($_SERVER['REQUEST_URI'], 'templates/LivingHomeopathy/portal.php') !== false) {
            include_once($this->view_path . "portal_main_appview.php");
//        }
//        else {
//            include_once($this->view_path . "portal_main.php");
//        }
		unset($source_path);
		unset($dhl_menu);
		return $topMenuCustomization;
	}
}