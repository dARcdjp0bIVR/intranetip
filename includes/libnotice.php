<?php
# Using : 
/*
 *  before IP.2.5.11.5.1 - Need to upload /addon/ip25/sql_table_update.php and run schema script
 *  before IP.2.5.10.10.1 - Need to upload related files    [2019-0923-1111-44235]
 */

####### Change log [Start] #######
#
#   Date:   2020-11-06 (Bill)   [2020-1102-1704-16073]
#           modified getNoticeContent(), to skip cleanHtmlJavascript() handling     ($sys_custom['eNotice']['SkipCleanHtmlLogicNoticeIds'])
#
#   Date:   2020-10-20 (Bill)
#           modified return_FormContent(), to fix approval right checking > check if user can approve school notice
#
#	Date:	2020-10-08 (YatWoon)
#			update returnEmailNotificationData(), add $senderEmail parameter for display sender [Case#2020-0609-1346-18073]
#
#	Date:	2020-09-24	YatWoon [2020-0921-1147-02066]
#			modified return_FormContent, add flag checking $sys_custom['eNotice']['HideCurrentNoticeList']
#
#   Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#           added static values for notice type
#           added parms for payment notice settings
#           modified libnotice(), to handle payment notice settings
#           modified GET_MODULE_OBJ_ARR(), to seperate issue right checking for pages display
#           - added checking of different payment notice settings
#               modified isNoticeAccessGroupMember(), hasIssueRight(), hasNormalRight(), hasFullRight()
#               added isSchoolNoticeAccessGroupMember(), isPaymentNoticeAccessGroupMember(), hasSchoolNoticeIssueRight(), hasPaymentNoticeIssueRight(),
#                           hasSchoolNoticeNormalRight(), hasPaymentNoticeNormalRight(), hasSchoolNoticeFullRight(), hasPaymentNoticeFullRight()
#           - applied notice settings of different notice type in checking (if has NoticeID)
#               modified hasViewRight(), hasViewRightForStudent(), hasFullRightForStudent(), hasFullRightForClass(), hasRemoveRight(), isEditAllowed(), isNoticeEditable()
#           - applied approval settings of different notice type
#               modified getPendingApprovalNotice(), getApprovalUserInfo(), updateApprovalUserInfo()
#               added hasSchoolNoticeApprovalRight(),hasPaymentNoticeApprovalRight(), isSchoolNoticeApprovalUser(), isPaymentNoticeApprovalUser()
#           modified returnNoticeListTeacherView, returnAllNotice(), to update SQL to ensure can return notice module
#
#   Date:   2020-08-03  Bill    [2020-0731-1529-14235]
#           modified getNoticeCSVUserInfo(), UserLogin must be case sensitive
#
#   Date:   2020-05-04  Bill    [2020-0407-1445-44292]
#           added isClassTeacherAudienceTargetLimited(), getClassTeacherCurrentTeachingClass(), to support class teacher checking ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])
#
#	Date:	2020-04-15  Philips [2020-0310-1051-05235]
#			Added SpecialNotice
#
#	Date:	2019-11-29	Philips [2019-1126-1210-58206]
#			Added defaultDisNumDays
#
#	Date:	2019-11-11  Philips [2019-1031-1227-12235]
#			modified libnotice(), added accessGroupCanSeeAllNotice
#
#   Date:   2019-11-04  Bill    [2019-1104-1100-11206]
#           modified retrieveNoticeYears(), to return years from valid notice only
#
#	Date:	2019-10-29  Carlos
#			modified splitQuestion() and parseAnswerStr(), added question type 17 which is multiple choices with input quantity.
#
#   Date:   2019-10-14  Bill    [DM#3677]
#           modified returnAllPaymentNotice(), to add new parm - $classTeacherRecordOnly, to return class teacher payment notice
#           modified returnMyNotice(), to add new parm - $module, to return notice from target module
#
#	Date:	2019-09-30	Philips [2019-0704-1449-17235]
#			modified libnotice() and return_FormContent(), add $this->sendEmail
#
#	Date:	2019-09-30	Philips [2019-0930-0954-24073]
#			modified return_FormContent(), added <!--TEMPLATE_SELECTION--> on the top of table
#
#	Date:	2019-09-27	Philips [2019-0704-1449-17235]
#			modified return_FormContent(), added $isTemplate for $sendPushMsg Option
#
#	Date:	2019-09-25	Philips [2019-0830-1524-38206]
#			modified returnTableViewGroupByClass(), returnAnswerStringGroupByClass(), added $withoutClass to allow get student has no class and class number
#
#   Date:   2019-09-23  Bill    [2019-0923-1111-44235]
#           modified return_FormContent(), to set iframe scrollable
#
#	Date:	2019-09-19 Carlos
#			modified splitQuestion(), fine tune the replace #TAR# regular expression.
#
#   Date:   2019-09-09 Bill     [2019-0905-1012-28207]
#           added isNoticeAccessGroupMember(), to check if user is access group member
#
#   Date:   2019-08-26 Bill
#           modified handleQuestionRelation(), getQuestionRelation()    ($sys_custom['eNotice']['ImproveGenReplySlipPerformance'])
#           added buildQuestionRelation(), improved logic to build question relation + loading performance
#
#	Date:	2019-07-29 Carlos
#			added getReplySlipModifyAnswerRecords($data) and upsertReplySlipModifyAnswerRecord($map).
#
#   Date:   2019-07-23 Bill     [2019-0715-0929-00235]
#           modified returnNoticeListTeacherView(), to support Class Teacher view mode
#
#   Date:   2019-06-26 Bill     [2019-0621-0947-35276]
#           modified returnRecipientNames2(), to return class with form ordering + chinese / english name
#
#	Date:	2019-06-14 Carlos
#			Modified returnAnswerString(), returnAnswerStringGroupByClass(), returnTableViewAll(), returnTableViewByClass(), fetch new data field NoticePaymentMethod from query.
#
#   Date:   2019-05-02 (Bill)
#           modified deleteNotice(), prevent Command Injection
#
#	Date: 	2019-04-10 Carlos
#			Modified parseAnswerStr($aStr) and splitQuestion($qStr, $moreFunct=false) to handle Payment Notice more question types and features.
#
#   Date:   2019-03-06 Bill
#           modified displayAttachment(), displayAttachment_showImage(), to support token parm in attachment url
#
#   Date:   2019-01-25 Bill     [2019-0118-1125-41206]
#           modified LoadNoticeData(), getNoticeContent(), support payment notice to display text content from FCKEditor
#
#   Date:   2018-11-12 Cameron
#           consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#
#	Date:	2018-09-28	(Bill)	[2018-0809-0935-01276]
#			modified splitQuestion(), handleQuestionAnswer(), parseAnswerStr(), returnPaymentItemAndCategoryID(), returnPaymentItemID(), returnPaymentItemDetails()
#           - support New Question Type - Must Submit
#
#   Date:   2018-09-10 (Philips)
#           modified returnRecord(), LoadNoticeData() - add 2 columns for emailnotify_PIC and emailnotify_ClassTeachers
#           modified return_FormContent(), add checkboxes for emailnotify_PIC and emailnotify_ClassTeachers
#
#   Date:   2018-08-08 (Bill)
#           modified getNoticeContent(), use function cleanHtmlJavascript() for XSS issues
#
#   Date:   2018-07-23 (Tiffany)[G141899]
#           Modified returnReplySlipContent(), let reply slip content can be drag 
#
#   Date:   2018-06-26 (Philips)
#           Added returnPICNoticeByKeyword()
#           Modified returnNoticeListTeacherView()
#           Modified returnAllPaymentNotice()
#
#   Date:   2018-05-21 (Philips)
#           modified returnRecord(), LoadNoticeData(), return_FormContent(), to add pushmessagenotify_PIC & pushmessagenotify_ClassTeachers
#
#
#   Date:   2018-03-26 (Bill)   [2018-0221-1225-19206]
#           modified returnTemplates(), to support order notice templates by notice number and notice title 
#
#   Date:   2018-03-23 (Bill)  [2018-0322-1718-26276]
#           modified returnRecord(), LoadNoticeData(), load Notice data field (isDeleted)
#
#   Date:	2018-03-14 (Isaac) [DM#3390]
#           fixed $keyword for returnNoticeListTeacherView(), returnPaymentNoticeListTeacherView(), returnAllNotice() and added EnglishName & ChineseName to returnAllPaymentNotice();
#
#   Date:   2018-03-06 (Anna)
#           Added notice settings for send push massage
#
#   Date:	2017-01-25 (Isaac)
#	        Improved: added EnglishName & ChineseName to returnNoticeListTeacherView() and returnPaymentNoticeListTeacherView() functions.
#                     
#
#	Date:	2017-12-22  (Cameron)
#			- modified getCusteNotice() so that it won't apply end date condition if isLateSignAllow is true, thus consistent with number shown in portal
#			- modify GET_MODULE_OBJ_ARR() to hide "Parent-signed Notice" and "Payment Notice" in left menu for Oaks
#			- fix: getCusteNotice() and getCusteNoticeForStaff(), should compare to now() for DateStart		
#
#	Date:	2017-12-08	(Bill)	[2017-1207-0941-45235]
#			modified getNoticeContent(), return_FormContent(), to support display HTML entity in Notice Content	($sys_custom['eNotice']['NoticeContentDecodeEntity'])
#
#	Date:	2017-10-13	(Anna)
#			modified returnNoticeListTeacherView(), add module = enrolment
#
#	Date:	2017-09-05	(Bill)
#			modified GET_MODULE_OBJ_ARR(), return_FormContent(), to add $sys_custom['eNotice']['HideMsgTemplateSettings'] & $sys_custom['eNotice']['HideParentAppNotify'] for SFOC project
#
#	Date:	2017-09-04	(Bill)	[2017-0901-1641-36236]
#			modified handleRecusiveRelation(), to improve performance of relation calculation using recursion
#
#	Date:	2017-09-01 (Ivan)
#			modified GET_MODULE_OBJ_ARR() to add $sys_custom['eNotice']['HideNoticeReport'] fro SFOC project
#
#	Date:	2017-07-14	(Bill)	[2017-0711-1820-12206]
#			modified getNoticeCSVUserInfo()
#			- add CSV User Sorting (Class Name and Class Number)
#			- fix cannot map both Chinese and English Class Name (as old logic use INTRANET_USER for checking only)
#			modified getNoticeContent(), previewNoticeContent()
#			- fix cannot map both Chinese and English Class Name
#
#	Date:	2017-06-09	(Bill)	[2017-0607-1442-37206]
#			- modified splitTargetGroupUserID(), to fix cannot get recipient user type due to preceding space
#
#	Date:	2017-05-09	(Bill)	[2017-0428-1605-58206]
#			- modified returnAnswerString(), returnAnswerStringGroupByClass(), retrieveReply(), to handle draft problem in admin view (hide draft input)
#
#	Date:	2016-11-25 (Villa)
#			-modified returnEmailNotificationData() add space after the url link
#
#	Date:	2016-11-18	(Tiffany)
#			-modified displayAttachment_showImage() let the attachment link encrypt.
#
#	Date:	2016-11-09 (Tiffany)
#			add notice setting option "teacherCanSeeAllNotice"
#
#	Date:	2016-10-17	Bill	 [ip.2.5.7.10.1]	[2016-0928-0903-29236]
#			modified getPendingApprovalNotice(), to fix returning incorrect number of notice that waiting for approval
#
#	Date	2016-10-14	Omas [ip.2.5.7.10.1]
#			modified GET_MODULE_OBJ_ARR() - changed menu MessageTemplate &Section=all
#
#	Date:	2016-09-26	Villa
#			-modified return_FormContent() - only check the box in new.php according to the default setting
#
# 	Date:	2016-09-23	(Villa)
#			-modified return_FormContent()	libcircular()
#			add to module setting - "default using eclass App to notify"
#
#	Date:	2016-09-23	Villa
#			return_FormContent change "Sync with notice issue date" to smallBtn
#
#	Date:	2016-09-21	Villa
#			modified return_FormContent, -add div showing div Push Message Setting
#
#	Date:	2016-09-12	Bill	[2016-0912-1001-12226]
#			modified handleRecusiveRelation(), to reduce loading when calculate MC option relation
#
#	Date:	2016-08-29	Ivan [ip.2.5.7.10.1]
#			modified returnMyNotice(), returnNoticeListTeacherView() to fix teacher cannot view today's notice in eService page
#
#	Date:	2016-08-25	Omas
#			modified returnIndividualTypeNames2(), get groupName according to current lang
#
#	Date:	2016-08-19	Kenneth
#			modified return_FormContent(), fix default end date error
#
#	Date:	2016-08-19	Bill	[2016-0819-1158-52236]
#			modified return_FormContent()
#			- fix cannot view notice content when user create or edit notice if client not using eClassApp
#			- fix cannot display correct Issue Date	when user create notice if client not using eClassApp
#
#	Date:	2016-08-17	Kenneth [ip.2.5.7.9.1]
#			modified return_FormContent() js:syncIssueTimeToPushMsg(), fix 15m->30m issues
#
#	Date:	2016-08-04	Ivan [ip.2.5.7.9.1]
#			modified returnAnswerString(), returnTableViewByClass(), returnTableViewGroupByClass(), returnAnswerStringGroupByClass() to support export UserLogin if $sys_custom['eNotice']['ExportUserLogin'] is on
#
#	Date:	2016-07-20	Bill	[2016-0719-1134-29206]
#			modified return_FormContent(), to hide must submit and display question number settings when edit
#
#	Date:	2016-07-19	Bill	[2016-0719-1134-29206]
#			modified returnTargetUserIDArray(), add Class Name and Class Number sorting
#
#	Date:	2016-07-18	Tiffany
#			support push message to student app
#
#	Date:	2016-07-14	Bill	[2016-0113-1514-09066]
#			modified returnReplySlipContent(), handleRecusiveRelation() to support MC question to set interdependence
#
#	Date:	2016-07-11	Kenneth
#			modified returnAllPaymentNotice(), adding approval logic
#
#	Date:	2016-07-08	Kenneth
#			modified return_FormContent(), changes text for push msg (now) -> (instant)
#
#	Date:	2016-06-10	Bill	[2016-0113-1514-09066]
#			added handleQuestionAnswer(), returnQuestionString(), buildReplySlipTemplatesInArray(), returnReplySlipContent()
#			- support new reply slip layout generation
#			added handleQuestionRelation(), handleRecusiveRelation()
#			- handle interdependence of question
#			modified splitQuestion()
#			- support target question syntax in question string
#
#	Date:	2016-06-10	Tiffany
#			modified displayAttachment_showImage(),let the image width=100%
#
#	Date:	2016-06-03 Kenneth
#			modified returnPushMessageData() & returnEmailNotificationData, get obj attributes if param is empty
#			added getNotifiedStudentList()
#			modified return_FormContent(), add approval logic
#			added sendApprovalNotice(), send Approval Notice
#
#	Date: 	2016-05-19	Kenneth
#			- add isApprovalUser() to check whether user is approver
#
#	Date:	2016-04-20	Bill	[2016-0405-1450-49066]
#			modified GET_MODULE_OBJ_ARR(), for Advanced Control Group to access Notice Summary
#
#	Date:	2016-03-22	Kenneth
#			add general setting: needApproval
#			add getApprovalUserInfo(),updateApprovalUserInfo() - add db table
#
#	Date:	2016-02-12	Bill	[2015-1118-1620-50207]
#			modified getNoticeContent() to display line break for payment notice
#
#	Date:	2016-01-12	Bill	[DM#2949]
#			modified return_FormContent() to add remarks for input Merge Content in CSV
#
#	Date:	2016-01-12  Cameron
#			always show "Display All eNotice" in displayCusteNotice() so that user can view expired eNotice [Case#P91044]
#
#	Date:	2016-01-06	Bill	[2016-0106-1042-16066]
#			modified isEditAllowed(), for Class Teacher with multiple classes
#
#	Date:	2016-01-04	Bill
#			modified retrieveAmount(), retrieveAmountWithNonPayItem(), replace split() by explode() for PHP 5.4
#
#	Date:	2015-12-18  Cameron
#			modify return_FormContent() to hide "Notify parents using email" and "Notice Content Setting"
#
#	Date:	2015-12-14	Bill
#			modified getNoticeContent(), previewNoticeContent() to prevent error if file not exist
#
#	Date:	2015-12-11  Cameron
#			modify GET_MODULE_OBJ_ARR() to hide "Parent-signed Notice" and "Payment Notice" in left menu for Amway
#
#	Date:	2015-12-10 	Cameron
#			add function getCusteNotice(), getCusteNoticeForStaff() and displayCusteNotice()
#
#   Date:	2015-11-25	Tiffany
#			add displayAttachment_showImage() to show image
#
#	Date:	2015-11-19	Bill	[EJ-DM#649]
#			modified getNoticeContent(), previewNoticeContent(), for merge format 2, fixed same content will only display once
#
#	Date:	2015-11-13	Bill	[2015-0615-1438-48014]
#			modified splitQuestion(), remove Question settings from answer
#
#	Date:	2015-11-12	Ivan
#			modified return_FormContent() to add push message send by batch remarks
#
#	Date:	2015-11-05	Bill	[2015-1103-1025-35066]
#			modified updateNoticeFlashUploadPath()
#			use $file_path instead of $PATH_WRT_ROOT for uploading images embedded in FCKeditor
#			prevent image in FCKeditor cannot view in KIS site
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			modified returnRecord(), LoadNoticeData(), return_FormContent(), support merge notice content
#			added getNoticeContent(), previewNoticeContent()
#
#	Date:	2015-10-14 (Roy)
#			modified return_FormContent(), fix schedule push message time picker display wrong hour value
#
#	Date:	2015-09-18 (Bill)
#			modified libnotice(), get settings of "Allow eNotice admin to sign payment notice for parents." [2015-0917-1053-13066]
#
#	Date:	2015-07-27 (Bill) [2015-0428-1214-11207]
#			modified returnNoticePICMapping(), to return associative array of eNotice and PICs
#
#	Date:	2015-06-10 (Roy)
#			modified return_FormContent(), add radio button and time picker for scheduled push message
#
#	Date:	2015-04-16 (Omas)
#			modified returnNoticeListTeacherView(),returnMyNotice(),returnAllPaymentNotice(),returnPaymentNoticeListTeacherView() will not where keyword when there is no keyword
#
#	Date:	2015-04-14 (Bill)
#			modified GET_MODULE_OBJ_ARR(), fix eAdmin and eService - eNotice tag highlight problem
#			modified libnotice(), to get settings of "Allow class teacher to send push message to parents." [2015-0303-1237-15073]
#			added isNoticePIC(), returnNoticePICNames(), to check if user is eNotice PIC and return selected PIC in eNotice [2015-0323-1602-46073]
#			modified libnotice(), returnMyNotice(), returnPaymentNoticeListTeacherView(), to support eNotice PIC function [2015-0323-1602-46073]
#
#	Date:	2015-04-02 (Omas)	[#J73013]
#			modified returnEmailNotificationData() comment original footer
#
#	Date:	2014-12-30 (Bill)	[#B73042]
#			modified returnResultOfClass(), returnAllResult(), getUnsignList(), to handle multiple classes input and return signed result
#
#	Date:	2014-12-11 (Omas)
#			insertUpdateNoticeMessageTemplate() getParentNotificationTemplate() getParentNotificationTemplateSQL()
#			- moved to libeClassApp_template
#
#	Date:	2014-12-05 (Omas)
#			- add insertUpdateNoticeMessageTemplate() getParentNotificationTemplate() getParentNotificationTemplateSQL()
#
#	Date:	2014-10-13 (Roy)
#			- add returnPushMessageData()
#			- update return_FormContent(), add send push message option
#
#	Date:	2014-09-30 (Bill)
#			modified libnotice() for staff to view all students' reply
#
#	Date:	2014-09-08 (YatWoon)
#			modified returnTotalCountByClass(), returnSignedCountByClass(), returnResultOfClass(), returnAnswerString()
#
#	Date:	2014-09-02 (YatWoon)
#			improved: returnEmailNotificationData(), hide "eServer" navigation in notice mail content [Case#U59266]
#			Deploy: ip.2.5.5.10.1
#
#	Date:	2014-05-15 (Carlos)
#			fix retrieveAmount() - fail to parse the last number X in pattern ||#AMT#X...#AMT#X|| with split()
#
#	Date:	2014-05-05 (YatWoon)
#			add rebuildNoticeReply()
#			add returnIndividualTypeNames2(), fix: incorrect U/G ID for audience
#
#	Date:	2014-04-29 (YatWoon)
#			add returnRecipientNames2()
#
#	Date:	2014-04-16 (YatWoon)
#			modified GET_MODULE_OBJ_ARR(), add Class Teacher access right for eService > eNotice > Discipline Notice
#
#	Date:	2014-04-15 (YatWoon)
#			add notice setting option "ClassTeacherCanAccessDisciplineNotice" [Case#J59310]
#
#	Date:	2014-04-01 (Ivan)
#			modified displayAttachment() added param $targetBlank to download attachment in _blank target for eClassApp
#
#	Date:	2014-03-25 (YatWoon)
#			modified return_FormContent(), add wordings [Case#P59398]
#
#	Date:	2014-02-26 (YatWoon)
#			modified returnAnswerString(), support 1 teacher with multiple classes [Case#U58796]
#			add isStudentInNotice()
#
#	Date:	2013-10-08 (Roy)
#			copy getUnsignList() from EJ
#
#	Date:	2013-09-23 (YatWoon)
#			modified returnAnswerString(), returnTableViewByClass(), returnAnswerStringGroupByClass(), returnTableViewGroupByClass()
#				add WebSAMSRegNo, for customization $sys_custom['eNotice']['ExportWebSAMSRegNo']  [Case#2012-1018-1523-11140]
#
#	Date:	2013-09-17 (Siuwan)
#			modified return_FormContent() - set Fckeditor Toolbarset - Basic2_withInsertImageFlash
#
#	Date:	2013-09-06 (YatWoon)
#			add displayAttachmentEdit() - copied from school news
#			add genAttachmentFolderName()
#
#	Date:	2013-04-02 (YatWoon)
#			add getParentNotice()
#
#	Date:	2012-09-03 (YatWoon)
#			update buildReplySlipTemplates(), cater the " in question [Case#2012-0903-0949-32066]
#
#	Date:	2012-07-06 (YatWoon)
#			update displayAttachment(), download_attachment.php with encrypt logic
#
#	Date:	2011-12-20 (YatWoon)
#			update returnNoticeSignedCountOnce(), the query missing "WHERE" cause sql error
#
#	Date:	2011-12-14 (YatWoon)
#			update returnTargetUserIDArray(), add parameter $UserRecordStatus
#
#	Date:	2011-12-13 (Henry Chow)
#			modified returnAllNotice(), exclude the eNotice from customized report [2011-1213-1154-33066]
#
#	Date:	2011-12-09 (Yuen)
#			optimized for counting number of notice signed and total expected number by adding returnNoticeSignedCountOnce()
#
#	Date:	2011-11-01 (Henry Chow)
#			updated returnPaymentItemDetails(), add non-payment item (textarea, question type = 10)
#
#	Date:	2011-09-30	(YatWoon)
#			updated return_FormContent(), add parameter $copied
#
#	Date:	2011-06-02	(YatWoon)
#			modified returnNoticeListTeacherView(),returnAllPaymentNotice(),returnPaymentNoticeListTeacherView(),returnAllNotice(),returnMyNotice()
#			Improved: Search keyword > include notice content
#
#	Date:	2011-05-23	(Henry Chow)
#			modified updateSignatureFooter(), remove 1st blank row & revise the width of footer
#
#	Date:	2011-03-25	YatWoon
#			add function returnEmailNotificationData(), return notification email subject & content
#
#	Date:	2011-03-03	Carlos
#			html decode issued "Description" in return_FormContent()
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
#	Date:	2011-02-16	YatWoon
#			update return_FormContent(), Add option "Display question number"
#
#	Date:	2011-01-27 [Henry Chow]
#			added retrieveAmountWithNonPayItem(), cater "Non Payment Item"
#
#	Date:	2011-01-27 [Henry Chow]
#			update returnPaymentItemAndCategoryID(), cater "Non Payment Item"
#
#	Date:	2010-12-29	YatWoon
#			update returnAnswerStringGroupByClass(), remove classname and class number after student name [#2010-1228-1534-37069]
#
#	Date:	2010-12-16 (Henry Chow)
#			update updateSignatureFooter(), revise the signature option & display
#
#	Date:	2010-12-07 (Henry Chow)
#			update updateSignatureFooter(), increase the height of signature
#
#	Date:	2010-11-18 (Henry Chow)
#			update updateSignatureFooter(), revise the display of footer
#
#	Date:	2010-11-10 YatWoon
#
#			update returnAllNotice(), returnMyNotice(), returnNoticeListTeacherView(), add date checking, don't display future Notice
#
#	Date:	2010-11-02 (Henry Chow)
#			update returnRecord(), loadNoticeData(), add updateSignatureFooter(), handle on "Footer"
#
#	Date:	2010-10-20 YatWoon
#			update retrieveNoticeYears(), returnNoticeListTeacherView()
#
#	Date:	2010-10-25 ronald
#			modified return_FormContent(), add one more option - send email notification to student
#
#	Date:	2010-10-13 Ivan
#			added AssignToBatchNotice(), LoadNoticeData() and modified returnRecord(), returnClassList() for performance tunning
#
#	Date:	2010-10-12 YatWoon
#			update return_FormContent(), client can modify attachment for distributed notice [wish list]
#
#	Date:	2010-09-17 Ronald (Rollback, please contact Ronald for detail)
#			update hasIssueRight(), refer to EJ, teaching staff can have right to view notice icon in "Admin Centre"
#
#	Date: 	2010-09-13 YatWoon
#			update parseAnswerStr(), add ";" to separate the answers
#
#	Date: 	2010-09-03 (Henry Chow)
#			modified returnAllNotice(), allow parent/student can view notice when select "All school notices" in eService
#
#	Date:	2010-09-03 Henry Chow
#			update returnMyNotice() & returnNoticeListTeacherView(), teacher can view payment notice in "eService>notice"
#
#	Date:	2010-08-04 YatWoon
#			update returnAnswerStringGroupByClass(), only retrieve active student
#			update returnRecipientNames(), update retrieve class / Level MySql
#
#	Date:	2010-07-30 YatWoon
#			update returnNoticeTotalCount(), returnNoticeSignedCount()
#			pass NoticeID to retrieve the result only for specific notice
#
#	Date:	2010-06-18	YatWoon
#			Admin can edit notice even if the notice is not created by own
#
#	Date:	2010-06-02	YatWoon
#			add option allow re-sign the notice or not ($lnotice->NotAllowReSign)
#			update returnAllNotice(), All notice for student/parent, don't display Module's notice
#
#	Date:	2010-05-25 (Henry chow)
#			Modified returnMyNotice(), returnNoticeListTeacherView(), only display School Notice in "eAdmin>Student Mgmt>eNotice>School Notice"
#
#	Date:	2010-05-03 (YatWoon)
#			Parent allow sign past notice
#			Can edit past notice (just only some fields)
#
#	 Date:	2010-03-25	YatWoon
#			update returnRecord()
#			Add field "AllFieldsReq" for option  "All questions are required to be answered"
#
#	Date:	2010-03-17	[YatWoon]
#			update displayAttachment()
#			add checking the notice has attachment or not
#
#	Date:	2010-02-10	(Henry)
#			modified GET_MODULE_OBJ_ARR()
#			display "Payment Notice" in menu base on the value of "PaymentNoticeEnable" in GENERAL_SETTING (1:enable, 0:disable)
#
#	Date:	2010-02-03	Ronald
#			modified returnPaymentNoticeListTeacherView(),
#			add one more where condition - "isDeleted = 0"
#
#	Date:	2010-02-03	YatWoon
#			add retrieveNoticeYears()
#			retrieve the year which includes any notice.
#
#	Date:	2010-02-01 [Ronald]
#			create function returnPaymentItemAndCategoryID(), used to return PaymentItem & PaymentCategory by passing a $qStr
#
#	Date:	2010-01-26 [Henry]
#			add getPaymentCategory(), retrieve PAYMENT categories
#
#	Date:	2010-01-25 [Henry]
#			modified  returnAllNoticeWithStatus(), added a parameter $moduleName in the function (defualt "NULL")
#
#	Date:	2010-01-25 [Henry]
#			modified  GET_MODULE_OBJ_ARR(), added an option "eNotice > Payment Notice"
#
#	Date:	2010-01-25 [Henry]
#			add $this->enablePaymentNotice
#
#	Date:	2010-01-19 [YatWoon]
#			update buildReplySlipTemplates()
#			strip the line break in the question field
#
#
#	Date:	2010-01-14 [YatWoon]
#			add buildReplySlipTemplates()
#			build the reply slip tempates array
#
####### Change log [End] #######


if (!defined("LIBNOTICE_DEFINED"))                     // Preprocessor directive
{
	define("LIBNOTICE_DEFINED", true);
	
	# added by Kelvin Ho 2008-11-29
	#### Record Status in INTRANET_NOTICE ####
	define("DISTRIBUTED_NOTICE_RECORD", "1");
	define("SUSPENDED_NOTICE_RECORD", "2");
	define("TEMPLATE_RECORD", "3");
	
	#### Record Types in INTRANET_NOTICE ####
	define("NOTICE_TO_WHOLE_SCHOOL", "1");
	define("NOTICE_TO_SOME_LEVELS", "2");
	define("NOTICE_TO_SOME_CLASSES", "3");
	define("NOTICE_TO_SOME_STUDENTS", "4");

    #### 2020-09-07 Bill   [2020-0604-1821-16170]
    #### Notice Types in Settings ####
    define("NOTICE_SETTING_TYPE_SCHOOL", "School");
    define("NOTICE_SETTING_TYPE_PAYMENT", "Payment");
	
	class libnotice extends libdb
	{
		# Settings
		var $disabled;
		var $fullAccessGroupID;
		var $normalAccessGroupID;
		var $isClassTeacherEditDisabled;
		var $isAllAllowed;
		var $defaultNumDays;
		var $setting_file;
		var $showAllEnabled;            # Add at 20031212 (Kenneth)
        var $DisciplineGroupID;
		var $enablePaymentNotice;		# Add at 20100125 (Henry)
		var $isLateSignAllow;			# 2010-05-03 [YatWoon]
		var $NotAllowReSign;			# 2010-06-02  [YatWoon]
		var $MaxReplySlipOption;		# 2011-02-24 [YatWoon]
		var $ClassTeacherCanAccessDisciplineNotice;		# 2014-04-15 [YatWoon]
        var $staffview;
        var $isPICAllowReply;
        var $isAllowClassTeacherSendeNoticeMessage;
        var $isAllowAdminToSignPaymentNotice;
		var $needApproval;				# 2016-03-22 Kenneth
		var $sendPushMsg;				# 2016-09-23 Villa
        var $sendEmail;
		var $teacherCanSeeAllNotice;    # 2016-11-09 Tiffany
		var $accessGroupCanSeeAllNotice;# 2019-11-11 Philips
		var $defaultDisNumDays; 		# 2019-11-29 Philips

        # Settings - Payment Notice     # 2020-09-07 Bill   [2020-0604-1821-16170]
        //var $payment_disabled         // controlled in ePayment module
        var $payment_fullAccessGroupID;
        var $payment_normalAccessGroupID;
        //var $payment_isClassTeacherEditDisabled;  // not allowed in payment notice
        var $payment_isAllAllowed;
        var $payment_defaultNumDays;
        //var $payment_setting_file;    // controlled in Admin Console for whole module
        //var $payment_showAllEnabled;  // not in use
        //var $payment_DisciplineGroupID;   // not related to payment notice
        //var $payment_enablePaymentNotice; // controlled in ePayment module
        //var $payment_isLateSignAllow; // not allowed in payment notice
        //var $payment_NotAllowReSign;  // not allowed in payment notice
        var $payment_MaxReplySlipOption;
        //var $payment_ClassTeacherCanAccessDisciplineNotice    // not related to payment notice
        var $payment_staffview;
        //var $payment_isPICAllowReply; // not allowed in payment notice
        var $payment_isAllowClassTeacherSendeNoticeMessage;
        var $payment_needApproval;
        var $payment_sendPushMsg;
        var $payment_sendEmail;
        var $payment_teacherCanSeeAllNotice;
        var $payment_accessGroupCanSeeAllNotice;
        //var $payment_defaultDisNumDays;   // not related to payment notice
		
		# Record
		var $NoticeID;
		var $NoticeNumber;
		var $Title;
		var $Module;            # added by Kelvin Ho 2008-11-12 for ucc
		var $DateStart;
		var $DateEnd;
		var $Description;
		var $IssueUserID;
		var $RecipientID;
		var $Question;
		var $ReplySlipContent;	# 20090611 added for reply slip content
		var $Attachment;
		var $RecordType;
		var $RecordStatus;
		var $DebitMethod;		# added by Henry 20100125
		var $IssueUserName;
		var $AllFieldsReq;
		var $DisplayQuestionNumber;
		var $ContentType;		# added [2015-0416-1040-06164]
		var $MergeFormat;
		var $MergeType;
		var $MergeFile;
		var $pushmessagenotify;
		var $pushmessagenotify_Students;
		var $pushmessagenotify_PIC;               # added by Philips 2018-05-21
		var $pushmessagenotify_ClassTeachers;     # added by Philips 2018-05-21
		var $pushmessagenotifyMode;
		var $pushmessagenotifyTime;
		var $emailnotify;
		var $emailnotify_Students;
		var $emailnotify_PIC;                     # added by Philips 2018-09-10
		var $emailnotify_ClassTeachers;           # added by Philips 2018-09-10
		var $ApprovedBy;
		var $ApprovedTime;
		var $ApprovalComment;
		
		# Reply record
		var $answer;
		var $studentID;
		var $signerID;
		var $replyType;
		var $replyStatus;
		var $signedTime;
		var $signerName;
		var $footer;
		
		var $question_array;
		var $IsModule;                            # add by Kelvin Ho 2009-02-25
		var $TargetType;
		var $isDeletedNotice;
		var $isPaymentNoticeApplyEditor;          # added by Bill 2019-01-28
		
		var $AllNoticeReplyCountArr;
		var $NoticeReplyCountArr;
		
		var $SpecialNotice;						
		
		#########################################################################
		# Start in IP25
		#########################################################################
		/*
		 * Get MODULE_OBJ array
		 */
		function GET_MODULE_OBJ_ARR()
		{
			global $UserID, $PATH_WRT_ROOT, $plugin, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $intranet_session_language, $intranet_root, $special_feature, $sys_custom;
			
			include_once("libgeneralsettings.php");
			$lgeneralsettings = new libgeneralsettings();
			$moduleName = 'ePayment';
			$settingName = array("'PaymentNoticeEnable'");
			$enablePaymentNotice = $lgeneralsettings->Get_General_Setting($moduleName, $settingName);
			
			### wordings
			global $Lang;
			
			# Current Page Information init
			$PageNotice 							= 0;
			$PageNotice_Discipline					= 0;
			$PageNoticeReports						= 0;
			$PageNoticeReports_NoticeSummary		= 0;
			$PageNoticeSettings 					= 0;
			$PageNoticeSettings_BasicSettings 		= 0;
			$PageNoticeSettings_NoticeSettings      = 0;
			$PageNoticeSettings_MessageTemplate	    = 0;
			$PaymentNotice 							= 0;
			
			switch ($CurrentPage) {
				case "PageNotice":
					$PageNotice = 1;
					break;
				case "PageNotice_Discipline":
					$PageNotice_Discipline = 1;
					break;
				case "PageNoticeReports_NoticeSummary":
					$PageNoticeReports = 1;
					$PageNoticeReports_NoticeSummary = 1;
					break;
				case "PageNoticeSettings_BasicSettings":
					$PageNoticeSettings = 1;
					$PageNoticeSettings_BasicSettings = 1;
					break;
				case "PageNoticeSettings_NoticeSettings":
				    $PageNoticeSettings = 1;
				    $PageNoticeSettings_NoticeSettings= 1;
				    break;
				case "PageNoticeSettings_MessageTemplate":
					$PageNoticeSettings = 1;
					$PageNoticeSettings_MessageTemplate = 1;
					break;
				case "PaymentNotice":
					$PaymentNotice = 1;
					break;
				case "PageStudentNotice":
					$PageStudentNotice = 1;
					break;
			}
			

			if($CurrentPageArr['eAdminNotice'])
			{
				$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/";
				if ($sys_custom['project']['CourseTraining']['IsEnable']) {
					$MODULE_OBJ['root_path'] .= "student_notice/";	// don't show Parent-signed Notice for Amway, Oaks, CEM
				}

				if(!$this->disabled && $_SESSION['UserType'] == USERTYPE_STAFF && $this->hasIssueRight())
				{
					// highlight eNotice tag if select Parent-signed Notice or Student-signed Notice or Payment Notice
					$MenuArr["Notice"] = array($Lang['Header']['Menu']['eNotice'], "#", ($PageNotice || $PageStudentNotice || $PaymentNotice));
					
					$hideParentNotice = false;
					if ($sys_custom['project']['CourseTraining']['IsEnable'] || $sys_custom['eNotice']['HideParenteNotice']) {	// don't show Parent-signed Notice for Amway, Oaks, CEM
						$hideParentNotice = true;
					}

                    /*
                    if (!$hideParentNotice) {
                        $MenuArr["Notice"]["Child"]["SchoolNotice"] 	= array($Lang['eNotice']['SchoolNotice'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/", $PageNotice);
                    }
                    $MenuArr["Notice"]["Child"]["SchoolStudentNotice"] 	= array($Lang['eNotice']['SchoolStudentNotice'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/student_notice/", $PageStudentNotice);
                    if(($enablePaymentNotice['PaymentNoticeEnable']==1) && ($special_feature['ePaymentNotice']) && (!$sys_custom['project']['CourseTraining']['IsEnable']))
                        $MenuArr["Notice"]["Child"]["PaymentNotice"] 	= array($Lang['eNotice']['PaymentNotice'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice.php", $PaymentNotice);
                    */

					// [2020-0604-1821-16170] seperate issue right checking
					if($this->hasSchoolNoticeIssueRight()) {
                        if (!$hideParentNotice) {
                            $MenuArr["Notice"]["Child"]["SchoolNotice"] = array($Lang['eNotice']['SchoolNotice'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/", $PageNotice);
                        }
                        $MenuArr["Notice"]["Child"]["SchoolStudentNotice"] = array($Lang['eNotice']['SchoolStudentNotice'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/student_notice/", $PageStudentNotice);
                    }
                    if($this->hasPaymentNoticeIssueRight()) {
                        if(($enablePaymentNotice['PaymentNoticeEnable'] == 1) && ($special_feature['ePaymentNotice']) && (!$sys_custom['project']['CourseTraining']['IsEnable'])) {
                            $MenuArr["Notice"]["Child"]["PaymentNotice"] = array($Lang['eNotice']['PaymentNotice'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice.php", $PaymentNotice);
                        }
                    }
                }
				
				// [2016-0405-1450-49066] allow Advanced Control Group to access Notice Summary
				// if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])
				if(!$sys_custom['eNotice']['HideNoticeReport'] && $this->hasFullRight())
				{
					$MenuArr["Reports"] = array($Lang['SysMgr']['Homework']['Reports'], "#", $PageNoticeReports);
					$MenuArr["Reports"]["Child"]["NoticeSummary"] = array($Lang['eNotice']['NoticeSummary'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/reports/notice_summary/index.php", $PageNoticeReports_NoticeSummary);
				}
				
				if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])
				{
					$MenuArr["Settings"] = array($Lang['eNotice']['Settings'],"#",$PageNoticeSettings );
					$MenuArr["Settings"]["Child"]["BasicSettings"] = array($Lang['eNotice']['BasicSettings'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/settings/basic_settings/", $PageNoticeSettings_BasicSettings);
					if($plugin['eClassApp']) {
					    $MenuArr["Settings"]["Child"]["NoticeSettings"] = array($Lang['eNotice']['NoticeSettings'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/settings/notice_settings/", $PageNoticeSettings_NoticeSettings);
					}
					if (!$sys_custom['eNotice']['HideMsgTemplateSettings'] && $plugin['eClassApp']) {
						// $MenuArr["Settings"]["Child"]["MessageTemplate"] = array($Lang['eClassApp']['PushMessageTemplate'],$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/notice/settings/message_template/", $PageNoticeSettings_MessageTemplate);
						// $MenuArr["Settings"]["Child"]["MessageTemplate"] = array($Lang['eClassApp']['PushMessageTemplate'],$PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/template_view.php?Module=eNotice&Section=NoticeSummaryReport", $PageNoticeSettings_MessageTemplate);
						$MenuArr["Settings"]["Child"]["MessageTemplate"] = array($Lang['eClassApp']['PushMessageTemplate'],$PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/template_view.php?Module=eNotice&Section=all", $PageNoticeSettings_MessageTemplate);
					}
				}
			}
			else if($CurrentPageArr['eServiceNotice'])
			{
				$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eService/notice/";
				if ($sys_custom['project']['CourseTraining']['IsEnable']) {
					$MODULE_OBJ['root_path'] .= "student_notice/";	// don't show Parent-signed Notice for Amway, Oaks, CEM
				}
				
				// highlight eNotice tag if select Parent-signed Notice or Student-signed Notice or Discipline Notice
				$MenuArr["Notice"]	= array($Lang['Header']['Menu']['eNotice'], "#", ($PageNotice || $PageStudentNotice || $PageNotice_Discipline));
				
				$hideParentNotice = false;
				if ($sys_custom['project']['CourseTraining']['IsEnable'] || $sys_custom['eNotice']['HideParenteNotice']) {	// don't show Parent-signed Notice for Amway, Oaks, CEM
					$hideParentNotice = true;
				}
				// don't show Parent-signed Notice for Amway, Oaks, CEM
				if (!$hideParentNotice) {
					$MenuArr["Notice"]["Child"]["SchoolNotice"] = array($Lang['eNotice']['SchoolNotice'], $PATH_WRT_ROOT."home/eService/notice/", $PageNotice);
				}
				$MenuArr["Notice"]["Child"]["SchoolStudentNotice"] = array($Lang['eNotice']['SchoolStudentNotice'], $PATH_WRT_ROOT."home/eService/notice/student_notice/", $PageStudentNotice);
				if($plugin['Disciplinev12'] && (
				            ($_SESSION['UserType'] == USERTYPE_STAFF && $this->isDisciplineNoticeGroup()) ||
                            ($this->ClassTeacherCanAccessDisciplineNotice && $_SESSION['USER_BASIC_INFO']['is_class_teacher'])
                    ))
				{
					$MenuArr["Notice"]["Child"]["DisciplineNotice"] = array($Lang['eNotice']['DisciplineNotice'], $PATH_WRT_ROOT."home/eService/notice/discipline/", $PageNotice_Discipline);
				}
			}
			
            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass eNotice";'."\n";
            $js.= '</script>'."\n";

			### module information
			$MODULE_OBJ['title'] = $Lang['Header']['Menu']['eNotice'].$js;
			$MODULE_OBJ['title_css'] = "menu_opened";
			$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eoffice.gif";
			
			$MODULE_OBJ['menu'] = $MenuArr;
			
			return $MODULE_OBJ;
		}

		function libnotice($ID="", $NoticeIDArr=null)
		{
			global $intranet_root, $intranet_version, $_SESSION;
			
			$this->libdb();
			$this->ModuleName = "eNotice";
			
			if (!isset($_SESSION["SSV_PRIVILEGE"]["notice"]))
			{
				include_once("libgeneralsettings.php");
				$lgeneralsettings = new libgeneralsettings();
				
				$settings_ary = $lgeneralsettings->Get_General_Setting($this->ModuleName);
				if(!empty($settings_ary))
				{
					foreach($settings_ary as $key => $data)
					{
						$_SESSION["SSV_PRIVILEGE"]["notice"][$key] = $data;
						$this->$key = $data;

						// [2020-0604-1821-16170] if no valid payment notice setting > apply school notice setting
						$payment_key = 'payment_'.$key;
						if(!isset($settings_ary[$payment_key]) && property_exists($this, $payment_key)) {
                            $_SESSION["SSV_PRIVILEGE"]["notice"][$payment_key] = $data;
                            $this->$payment_key = $data;
                        }
					}
				}
				else
				{
					$this->disabled = false;
					$this->fullAccessGroupID = "";
					$this->normalAccessGroupID = "";
					$this->isClassTeacherEditDisabled = false;
					$this->isAllAllowed = false;
					$this->defaultNumDays = 4;
					$this->showAllEnabled = false;
					$this->DisciplineGroupID = "";
					$this->enablePaymentNotice = false;
					$this->isLateSignAllow = false;
					$this->NotAllowReSign = false;
					$this->MaxReplySlipOption = 50;
					$this->ClassTeacherCanAccessDisciplineNotice = false;
					$this->staffview = false;
					// [2015-0323-1602-46073]
					$this->isPICAllowReply = false;
					// [2015-0303-1237-15073]
					$this->isAllowClassTeacherSendeNoticeMessage = false;
					// [2015-0917-1053-13066]
					$this->isAllowAdminToSignPaymentNotice = false;
					$this->needApproval = false;
					$this->sendPushMsg = false;
                    $this->sendEmail = false;
					$this->teacherCanSeeAllNotice = false;
					$this->accessGroupCanSeeAllNotice = true;
					$this->defaultDisNumDays = 7;

                    # 2020-09-07 Bill   [2020-0604-1821-16170]
					# Settings - Payment Notice > default values
                    //$this->payment_disabled = false;
                    $this->payment_fullAccessGroupID = "";
                    $this->payment_normalAccessGroupID = "";
                    //$this->payment_isClassTeacherEditDisabled = false;
                    $this->payment_isAllAllowed = false;
                    $this->payment_defaultNumDays = 4;
                    //$this->payment_showAllEnabled = false;
                    //$this->payment_DisciplineGroupID = "";
                    //$this->payment_enablePaymentNotice = false;
                    //$this->payment_isLateSignAllow = false;
                    //$this->payment_NotAllowReSign = false;
                    $this->payment_MaxReplySlipOption = 50;
                    //$this->payment_ClassTeacherCanAccessDisciplineNotice = false;
                    $this->payment_staffview = false;
                    //$this->payment_isPICAllowReply = false;
                    $this->payment_isAllowClassTeacherSendeNoticeMessage = false;
                    $this->payment_needApproval = false;
                    $this->payment_sendPushMsg = false;
                    $this->payment_sendEmail = false;
                    $this->payment_teacherCanSeeAllNotice = false;
                    $this->payment_accessGroupCanSeeAllNotice = true;
                    //$this->payment_defaultDisNumDays = 7;
				}
			}
			else
			{
				$this->disabled = $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"];
				$this->fullAccessGroupID = $_SESSION["SSV_PRIVILEGE"]["notice"]["fullAccessGroupID"];
				$this->normalAccessGroupID = $_SESSION["SSV_PRIVILEGE"]["notice"]["normalAccessGroupID"];
				$this->isClassTeacherEditDisabled = $_SESSION["SSV_PRIVILEGE"]["notice"]["isClassTeacherEditDisabled"];
				$this->isAllAllowed = $_SESSION["SSV_PRIVILEGE"]["notice"]["isAllAllowed"];
				$this->defaultNumDays = $_SESSION["SSV_PRIVILEGE"]["notice"]["defaultNumDays"];
				$this->showAllEnabled = $_SESSION["SSV_PRIVILEGE"]["notice"]["showAllEnabled"];
				$this->DisciplineGroupID = $_SESSION["SSV_PRIVILEGE"]["notice"]["DisciplineGroupID"];
				$this->enablePaymentNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["enablePaymentNotice"];
				$this->isLateSignAllow = $_SESSION["SSV_PRIVILEGE"]["notice"]["isLateSignAllow"];
				$this->NotAllowReSign = $_SESSION["SSV_PRIVILEGE"]["notice"]["NotAllowReSign"];
				$this->MaxReplySlipOption = $_SESSION["SSV_PRIVILEGE"]["notice"]["MaxReplySlipOption"];
				$this->ClassTeacherCanAccessDisciplineNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["ClassTeacherCanAccessDisciplineNotice"];
				$this->staffview = $_SESSION["SSV_PRIVILEGE"]["notice"]["staffview"];
				// [2015-0323-1602-46073]
				$this->isPICAllowReply = $_SESSION["SSV_PRIVILEGE"]["notice"]["isPICAllowReply"];
				// [2015-0303-1237-15073]
				$this->isAllowClassTeacherSendeNoticeMessage = $_SESSION["SSV_PRIVILEGE"]["notice"]["isAllowClassTeacherSendeNoticeMessage"];
				// [2015-0917-1053-13066]
				$this->isAllowAdminToSignPaymentNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["isAllowAdminToSignPaymentNotice"];
				$this->needApproval = $_SESSION["SSV_PRIVILEGE"]["notice"]["needApproval"];
				$this->sendPushMsg = $_SESSION["SSV_PRIVILEGE"]["notice"]["sendPushMsg"];
				$this->sendEmail = $_SESSION["SSV_PRIVILEGE"]["notice"]["sendEmail"];
				$this->teacherCanSeeAllNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["teacherCanSeeAllNotice"];
				$this->accessGroupCanSeeAllNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["accessGroupCanSeeAllNotice"];
				$this->defaultDisNumDays = $_SESSION["SSV_PRIVILEGE"]["notice"]["defaultDisNumDays"];

                # 2020-09-07 Bill   [2020-0604-1821-16170]
                # Settings - Payment Notice > session values
                //$this->payment_disabled = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_disabled"];
                $this->payment_fullAccessGroupID = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_fullAccessGroupID"];
                $this->payment_normalAccessGroupID = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_normalAccessGroupID"];
                //$this->payment_isClassTeacherEditDisabled = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_isClassTeacherEditDisabled"];
                $this->payment_isAllAllowed = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_isAllAllowed"];
                $this->payment_defaultNumDays = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_defaultNumDays"];
                //$this->payment_showAllEnabled = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_showAllEnabled"];
                //$this->payment_DisciplineGroupID = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_DisciplineGroupID"];
                //$this->payment_enablePaymentNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_enablePaymentNotice"];
                //$this->payment_isLateSignAllow = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_isLateSignAllow"];
                //$this->payment_NotAllowReSign = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_NotAllowReSign"];
                $this->payment_MaxReplySlipOption = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_MaxReplySlipOption"];
                //$this->payment_ClassTeacherCanAccessDisciplineNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_ClassTeacherCanAccessDisciplineNotice"];
                $this->payment_staffview = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_staffview"];
                //$this->payment_isPICAllowReply = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_isPICAllowReply"];
                $this->payment_isAllowClassTeacherSendeNoticeMessage = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_isAllowClassTeacherSendeNoticeMessage"];
                $this->payment_needApproval = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_needApproval"];
                $this->payment_sendPushMsg = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_sendPushMsg"];
                $this->payment_sendEmail = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_sendEmail"];
                $this->payment_teacherCanSeeAllNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_teacherCanSeeAllNotice"];
                $this->payment_accessGroupCanSeeAllNotice = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_accessGroupCanSeeAllNotice"];
                //$this->payment_defaultDisNumDays = $_SESSION["SSV_PRIVILEGE"]["notice"]["payment_defaultDisNumDays"];
			}
			
			if ($ID != "" || (is_array($NoticeIDArr) && count($NoticeIDArr) > 0))
			{
				$BatchNotice = $this->returnRecord($ID, $NoticeIDArr, 0);
				$numOfNotice = count($BatchNotice);
				
				if ($numOfNotice > 1)
				{
					$this->AssignToBatchNotice($BatchNotice);
				}
				
				$this->Notice = $BatchNotice;
				# load this first notice
				$this->LoadNoticeData($this->Notice[0]['NoticeID']);
			}
		}

		function AssignToBatchNotice($BatchNotice)
		{
			$numOfNotice = count($BatchNotice);
			for ($i=0; $i<$numOfNotice; $i++)
			{
				# store using UserID
				$this->BatchNotice[$BatchNotice[$i]['NoticeID']] = $BatchNotice[$i];
			}
		}

		function returnRecord($ID, $NoticeIDArr=null, $loadNoticeData=1)
		{
			if (((is_array($this->BatchNotice[$ID]) && sizeof($this->BatchNotice[$ID])>0) || $this->Notice[0] != '') && $loadNoticeData==1)
			{
				// Load Data if the data has been pre-loaded
				$this->LoadNoticeData($ID);
			}
			else
			{
				// Retrieve notice data
				$cond_NoticeID = '';
				if (!is_array($ID) && $ID != '') {
					$cond_NoticeID = " AND NoticeID = '".$ID."' ";
				}
				else if (is_array($NoticeIDArr) && count($NoticeIDArr) > 0) {
					$cond_NoticeID = " AND NoticeID IN (".implode(',', $NoticeIDArr).") ";
				}
				
				$sql = "SELECT 
                                NoticeNumber, Title, Module, DATE_FORMAT(DateStart,'%Y-%m-%d %H:%i:%s'), DATE_FORMAT(DateEnd,'%Y-%m-%d %H:%i:%s'), Description, IssueUserID,
            					RecipientID, Question, Attachment, RecordType, RecordStatus, IssueUserName, IsModule, ReplySlipContent, DebitMethod, AllFieldsReq, NoticeID, Footer, TargetType, DisplayQuestionNumber,
            					ContentType, MergeFormat, MergeType, MergeFile,
            					pushmessagenotify, pushmessagenotify_Students, pushmessagenotifyMode, pushmessagenotifyTime, emailnotify, emailnotify_Students,
            					ApprovedBy, ApprovedTime, ApprovalComment, isDeleted, pushmessagenotify_PIC, pushmessagenotify_ClassTeachers, emailnotify_PIC, emailnotify_ClassTeachers, isPaymentNoticeApplyEditor, SpecialNotice
				        FROM
                                INTRANET_NOTICE 
                        WHERE
                                1 $cond_NoticeID";
				$NoticeInfoArr = $this->returnArray($sql);
				
				if ($loadNoticeData == 1)
				{
					if (count($NoticeInfoArr) > 1)
					{
						$this->AssignToBatchNotice($NoticeInfoArr);
					}
					
					$this->Notice = $NoticeInfoArr;
					$this->LoadNoticeData($this->Notice[0]['NoticeID']);
				}
				else
				{
					return $NoticeInfoArr;
				}
			}
		}
		
		function LoadNoticeData($NoticeID)
		{
			if (is_array($this->BatchNotice[$NoticeID]) && sizeof($this->BatchNotice[$NoticeID]) > 0)
			{
				$notice_obj = $this->BatchNotice[$NoticeID];
			}
			else
			{
				$notice_obj = $this->Notice[0];
			}

			list(   $this->NoticeNumber, $this->Title, $this->Module, $this->DateStart, $this->DateEnd,
					$this->Description, $this->IssueUserID, $this->RecipientID,
					$this->Question, $this->Attachment, $this->RecordType,
					$this->RecordStatus, $this->IssueUserName, $this->IsModule, $this->ReplySlipContent, $this->DebitMethod, $this->AllFieldsReq, $this->NoticeID, $this->footer, $this->TargetType, $this->DisplayQuestionNumber,
					$this->ContentType, $this->MergeFormat, $this->MergeType, $this->MergeFile,
					$this->pushmessagenotify, $this->pushmessagenotify_Students, $this->pushmessagenotifyMode, $this->pushmessagenotifyTime, $this->emailnotify, $this->emailnotify_Students,
			        $this->ApprovedBy, $this->ApprovedTime, $this->ApprovalComment, $this->isDeletedNotice, $this->pushmessagenotify_PIC, $this->pushmessagenotify_ClassTeachers, $this->emailnotify_PIC, $this->emailnotify_ClassTeachers, $this->isPaymentNoticeApplyEditor,
					$this->SpecialNotice) = $notice_obj;
		}
		
		function retrieveReply($StudentID, $ShowDraftAnswer=false)
		{
			$answerField = $ShowDraftAnswer ? "Answer" : "IF(RecordStatus = 0, NULL, Answer) as Answer";
			
			$sql = "SELECT $answerField, SignerID, RecordType, RecordStatus, DateModified, SignerName, TempItemAmount
			        FROM INTRANET_NOTICE_REPLY 
			        WHERE StudentID = '$StudentID' AND NoticeID = '".$this->NoticeID."'";
			$result = $this->returnArray($sql,7);

			list ($this->answer,$this->signerID,$this->replyType,$this->replyStatus,$this->signedTime,$this->signerName, $this->TempItemAmount) = $result[0];
		}
		
		function retrieveAmount($StudentID)
		{
			$question = $this->Question;
			
			$answer = $this->answer;
			//$amtArr = (split("#AMT#",$question));
			$amtArr = explode("#AMT#",$question);
			
			$i = 0;
			foreach($amtArr as $s) {
				//$tmp = split("#QUE#", $s);
				$tmp = explode("#QUE#", $s);
				foreach($tmp as $val) {
					$val = str_replace("||","",$val); // remove the very end || in pattern ||#AMT#X...#AMT#X||
					if(is_numeric($val)) {
						$tempArr[$i] = $val;
						$i++;
					}
				}
			}

			$pos = 0;
			$strLen = strlen($question);
			//$tmpArr = split("#QUE#", $question);
			$tmpArr = explode("#QUE#", $question);
			
			foreach($tmpArr as $val) {
				$ct = substr_count($val, "#AMT#");
				if($ct>0) {
					$countAmt[$pos] = $ct;
					$pos++;
				}
			}

			$this->countAmount = $countAmt;
			$this->AmountValue = $tempArr;
		}
		
		function retrieveAmountWithNonPayItem($StudentID)
		{
			$question = $this->Question;
			
			/*
			 $amtArr = (split("#AMT#",$question));
			 $i = 0;
			 //debug_pr($amtArr);
			 foreach($amtArr as $s) {
			 $tmp = split("#NonPaymentItem#", $s);
			 foreach($tmp as $val) {
			 if(is_numeric($val)) {
			 $tempArr[$i] = $val;
			 $i++;
			 }
			 }
			 
			 }
			 */

			//$amtArr = (split("#AMT#",$question));
			$amtArr = (explode("#AMT#",$question));
			$i = 0;
			foreach($amtArr as $s) {
				//$tmp = split("\|\|#NonPaymentItem#", $s);
				$tmp = explode("\|\|#NonPaymentItem#", $s);
				foreach($tmp as $val) {
					if(is_numeric($val)) {
						$tempArr[$i] = $val;
						$i++;
					}
				}
			}

			$pos = 0;
			$strLen = strlen($question);
			//$tmpArr = split("#QUE#", $question);
			$tmpArr = explode("#QUE#", $question);
			//debug_pr($tmpArr);
			foreach($tmpArr as $val) {
				$ct = substr_count($val, "#AMT#");
				if($ct>0) {
					$countAmt[$pos] = $ct;
					$pos++;
				}
			}
			//debug_pr($countAmt);
			
			$this->countAmount = $countAmt;
			$this->AmountValue = $tempArr;
		}
		
		function returnTemplates($module="", $orderByTitle=false)
		{
			$cond = "";
			if(strtoupper($module) == "PAYMENT")
			{
				$cond .= " AND Module = 'Payment' ";
			}
			else
			{
				$cond .= " AND Module IS NULL ";
			}
			
			$orderTitle = "";
			if($orderByTitle)
			{
			    $orderTitle = " ORDER BY CONCAT(NoticeNumber,' - ',Title) ";
			}
			
			$sql = "SELECT NoticeID, CONCAT(NoticeNumber,' - ',Title) FROM INTRANET_NOTICE WHERE RecordStatus = 3 $cond $orderTitle";
			return $this->returnArray($sql, 2);
		}
		
		function isTeacherStaff()
		{
			return ($_SESSION['UserType']==USERTYPE_STAFF);
		}
		
		function isNoticePIC($targetNoticeID="")
		{
			global $UserID;
			
			if($targetNoticeID == "" && $_SESSION["SSV_PRIVILEGE"]["notice"]["eNoticePIC"]) return true;
			
			// specify which notice is checking
			if($targetNoticeID != "") $cond = "AND PIC.NoticeID = '$targetNoticeID'";
			
			$sql = "SELECT Count(*) FROM INTRANET_NOTICE_PIC PIC
			            INNER JOIN INTRANET_NOTICE NOTICE ON (PIC.NoticeID = NOTICE.NoticeID)
			        WHERE PIC.PICUserID = $UserID AND NOTICE.isDeleted = 0 $cond";
			$result = $this->returnVector($sql);
			
			// return if user is PIC of any enotice
			return ($result[0]>0);
		}

        function isNoticeAccessGroupMember($uid)
        {
            # eNotice Admin
            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

            # Teacher / Staff Only
            $sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '$uid' ";
            $result = $this->returnVector($sql);
            if ($result[0]!=1) return false;

            # Access Group Member
            if ($this->fullAccessGroupID) $groups[] = $this->fullAccessGroupID;
            if ($this->normalAccessGroupID) $groups[] = $this->normalAccessGroupID;
            // [2020-0604-1821-16170] added access groups from payment notice settings
            if ($this->payment_fullAccessGroupID) $groups[] = $this->payment_fullAccessGroupID;
            if ($this->payment_normalAccessGroupID) $groups[] = $this->payment_normalAccessGroupID;
            if (sizeof($groups) != 0)
            {
                $list = implode("', '",(array)$groups);
                $sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$uid' AND GroupID IN ('$list')";
                $result = $this->returnVector($sql);
                if ($result[0]!=0) return true;
            }

            return false;
        }

        function isSchoolNoticeAccessGroupMember($uid)
        {
            # eNotice Admin
            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

            # Teacher / Staff Only
            $sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '$uid' ";
            $result = $this->returnVector($sql);
            if ($result[0]!=1) return false;

            # Access Group Member
            if ($this->fullAccessGroupID) $groups[] = $this->fullAccessGroupID;
            if ($this->normalAccessGroupID) $groups[] = $this->normalAccessGroupID;
            if (sizeof($groups) != 0)
            {
                $list = implode("', '",(array)$groups);
                $sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$uid' AND GroupID IN ('$list')";
                $result = $this->returnVector($sql);
                if ($result[0]!=0) return true;
            }

            return false;
        }

        function isPaymentNoticeAccessGroupMember($uid)
        {
            # eNotice Admin
            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

            # Teacher / Staff Only
            $sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '$uid' ";
            $result = $this->returnVector($sql);
            if ($result[0]!=1) return false;

            # Access Group Member
            if ($this->payment_fullAccessGroupID) $groups[] = $this->payment_fullAccessGroupID;
            if ($this->payment_normalAccessGroupID) $groups[] = $this->payment_normalAccessGroupID;
            if (sizeof($groups) != 0)
            {
                $list = implode("', '",(array)$groups);
                $sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$uid' AND GroupID IN ('$list')";
                $result = $this->returnVector($sql);
                if ($result[0]!=0) return true;
            }

            return false;
        }
		
		function hasIssueRight()
		{
			global $UserID;
			
			if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;
			
			# Teacher / Staff Only
			if (!$this->isTeacherStaff()) return false;			##(original)
			//if ($this->isTeacherStaff()) return true;			##(from junior)
            // [2020-0604-1821-16170] added payment notice settings
            // if ($this->isAllAllowed) return true;
            if ($this->isAllAllowed || $this->payment_isAllAllowed) return true;
            if ($this->hasNormalRight()) return true;
            if ($this->isApprovalUser()) return true;

			return false;
		}

        function hasSchoolNoticeIssueRight()
        {
            global $UserID;

            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

            # Teacher / Staff Only
            if (!$this->isTeacherStaff()) return false;			##(original)
            //if ($this->isTeacherStaff()) return true;			##(from junior)
            if ($this->isAllAllowed) return true;
            if ($this->hasSchoolNoticeNormalRight()) return true;
            if ($this->isSchoolNoticeApprovalUser()) return true;

            return false;
        }

        function hasPaymentNoticeIssueRight()
        {
            global $UserID;

            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

            # Teacher / Staff Only
            if (!$this->isTeacherStaff()) return false;			##(original)
            //if ($this->isTeacherStaff()) return true;			##(from junior)
            if ($this->payment_isAllAllowed) return true;
            if ($this->hasPaymentNoticeNormalRight()) return true;
            if ($this->isPaymentNoticeApprovalUser()) return true;

            return false;
        }
		
		function hasNormalRight()
		{
			global $UserID;

			if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

			# Teacher / Staff Only
			if (!$this->isTeacherStaff()) return false;
			if ($this->fullAccessGroupID) $groups[] = $this->fullAccessGroupID;
			if ($this->normalAccessGroupID) $groups[] = $this->normalAccessGroupID;
            // [2020-0604-1821-16170] added access groups from payment notice settings
            if ($this->payment_fullAccessGroupID) $groups[] = $this->payment_fullAccessGroupID;
            if ($this->payment_normalAccessGroupID) $groups[] = $this->payment_normalAccessGroupID;
			if (sizeof($groups) != 0)
			{
				$list = implode(",",$groups);
				$sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$UserID' AND GroupID IN ($list)";
				$result = $this->returnVector($sql);
				if ($result[0]!=0) return true;
			}

			return false;
		}

        function hasSchoolNoticeNormalRight()
        {
            global $UserID;

            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

            # Teacher / Staff Only
            if (!$this->isTeacherStaff()) return false;
            if ($this->fullAccessGroupID) $groups[] = $this->fullAccessGroupID;
            if ($this->normalAccessGroupID) $groups[] = $this->normalAccessGroupID;
            if (sizeof($groups) != 0)
            {
                $list = implode(",",$groups);
                $sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$UserID' AND GroupID IN ($list)";
                $result = $this->returnVector($sql);
                if ($result[0]!=0) return true;
            }
            return false;
        }

        function hasPaymentNoticeNormalRight()
        {
            global $UserID;

            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

            # Teacher / Staff Only
            if (!$this->isTeacherStaff()) return false;
            if ($this->payment_fullAccessGroupID) $groups[] = $this->payment_fullAccessGroupID;
            if ($this->payment_normalAccessGroupID) $groups[] = $this->payment_normalAccessGroupID;
            if (sizeof($groups) != 0)
            {
                $list = implode(",",$groups);
                $sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$UserID' AND GroupID IN ($list)";
                $result = $this->returnVector($sql);
                if ($result[0]!=0) return true;
            }
            return false;
        }

		function hasFullRight()
		{
			global $UserID;

			if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

			# Teacher / Staff Only
			if (!$this->isTeacherStaff()) return false;
			/*
			if ($this->fullAccessGroupID!="")
            {
                $sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$UserID' AND GroupID = '".$this->fullAccessGroupID."'";
                $result = $this->returnVector($sql);
                if ($result[0]!=0) return true;
            }
			*/
            // [2020-0604-1821-16170] aadded access group from payment notice settings
            if ($this->fullAccessGroupID) $groups[] = $this->fullAccessGroupID;
            if ($this->payment_fullAccessGroupID) $groups[] = $this->payment_fullAccessGroupID;
            if (sizeof($groups) != 0)
            {
                $list = implode(",",$groups);
                $sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$UserID' AND GroupID IN ($list)";
                $result = $this->returnVector($sql);
                if ($result[0]!=0) return true;
            }

			return false;
		}

        function hasSchoolNoticeFullRight()
        {
            global $UserID;

            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

            # Teacher / Staff Only
            if (!$this->isTeacherStaff()) return false;
            if ($this->fullAccessGroupID != "")
            {
                $sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$UserID' AND GroupID = '".$this->fullAccessGroupID."'";
                $result = $this->returnVector($sql);
                if ($result[0]!=0) return true;
            }

            return false;
        }

        function hasPaymentNoticeFullRight()
        {
            global $UserID;

            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])	return true;

            # Teacher / Staff Only
            if (!$this->isTeacherStaff()) return false;
            if ($this->payment_fullAccessGroupID != "")
            {
                $sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$UserID' AND GroupID = '".$this->payment_fullAccessGroupID."'";
                $result = $this->returnVector($sql);
                if ($result[0]!=0) return true;
            }

            return false;
        }
		
		function hasApprovalRight()
        {
			global $UserID;

			$approvalGroup = $this->getApprovalUserInfo();
			$approvalGroupUserIDArray = Get_Array_By_Key($approvalGroup,'UserID');

			// if UserID in Array -> this user is in Approval Group
			if(in_array($UserID,$approvalGroupUserIDArray)){
				return true;
			}else{
				return false;
			}
		}

        function hasSchoolNoticeApprovalRight()
        {
            global $UserID;

            $approvalGroup = $this->getApprovalUserInfo(NOTICE_SETTING_TYPE_SCHOOL);
            $approvalGroupUserIDArray = Get_Array_By_Key($approvalGroup,'UserID');

            // if UserID in Array -> this user is in Approval Group
            if(in_array($UserID,$approvalGroupUserIDArray)){
                return true;
            }else{
                return false;
            }
        }

        function hasPaymentNoticeApprovalRight()
        {
            global $UserID;
            
            $approvalGroup = $this->getApprovalUserInfo(NOTICE_SETTING_TYPE_PAYMENT);
            $approvalGroupUserIDArray = Get_Array_By_Key($approvalGroup,'UserID');

            // if UserID in Array -> this user is in Approval Group
            if(in_array($UserID,$approvalGroupUserIDArray)){
                return true;
            }else{
                return false;
            }
        }
		
		# 20090306
		function isDisciplineNoticeGroup()
		{
			global $UserID;

			# Teacher / Staff Only
			if (!$this->isTeacherStaff()) return false;
			if ($this->DisciplineGroupID != "")
			{
				$sql = "SELECT COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE UserID = '$UserID' AND GroupID = '".$this->DisciplineGroupID."'";
				$result = $this->returnVector($sql);
				if ($result[0]!=0) return true;
			}

			return false;
		}
		
		function hasViewRight()
		{
			global $UserID;

            // [2020-0604-1821-16170] has NoticeID > check by notice type
            $hasNormalRight = $this->hasNormalRight();
            if ($this->NoticeID != '') {
                if ($this->Module == "Payment") {
                    $hasNormalRight = $this->hasPaymentNoticeNormalRight();
                } else {
                    $hasNormalRight = $this->hasSchoolNoticeNormalRight();
                }
            }

            //return ($this->hasNormalRight() || $this->IssueUserID == $UserID);
			return ($hasNormalRight || $this->IssueUserID == $UserID);
		}

		function hasViewRightForStudent($StudentID)
		{
            //if ($this->hasFullRight()) return true;

            // [2020-0604-1821-16170] has NoticeID > check by notice type
            $hasFullRight = $this->hasFullRight();
            if ($this->NoticeID != '') {
                if ($this->Module == "Payment") {
                    $hasFullRight = $this->hasPaymentNoticeFullRight();
                } else {
                    $hasFullRight = $this->hasSchoolNoticeFullRight();
                }
            }
            if ($hasFullRight) {
                return true;
            }

			if ($this->hasViewRight()) return true;

			$sql = "SELECT ClassName FROM INTRANET_USER WHERE UserID = '$StudentID'";
			$result = $this->returnVector($sql);
			return $this->hasViewRightForClass($result[0]);
		}

		function hasViewRightForClass($ClassName)
		{
			if ($this->hasViewRight()) return true;

			global $UserID;
			
			include_once("libclass.php");
			$lclass = new libclass();
			return ($lclass->returnHeadingClass($UserID)==$ClassName);
		}

		function hasFullRightForStudent($StudentID)
		{
			//if ($this->hasFullRight()) return true;

            // [2020-0604-1821-16170] has NoticeID > check by notice type
            $hasFullRight = $this->hasFullRight();
            if ($this->NoticeID != '') {
                if ($this->Module == "Payment") {
                    $hasFullRight = $this->hasPaymentNoticeFullRight();
                } else {
                    $hasFullRight = $this->hasSchoolNoticeFullRight();
                }
            }
            if ($hasFullRight) {
                return true;
            }

			$sql = "SELECT ClassName FROM INTRANET_USER WHERE UserID = '$StudentID'";
			$result = $this->returnVector($sql);
			return $this->hasFullRightForClass($result[0]);
		}

		function hasFullRightForClass($ClassName)
		{
            //if ($this->hasFullRight()) return true;

            // [2020-0604-1821-16170] has NoticeID > check by notice type
            $hasFullRight = $this->hasFullRight();
            if ($this->NoticeID != '') {
                if ($this->Module == "Payment") {
                    $hasFullRight = $this->hasPaymentNoticeFullRight();
                } else {
                    $hasFullRight = $this->hasSchoolNoticeFullRight();
                }
            }
            if ($hasFullRight) {
                return true;
            }

			global $UserID;

			include_once("libclass.php");
			$lclass = new libclass();
			return (!$this->isClassTeacherEditDisabled && $lclass->returnHeadingClass($UserID)==$ClassName);
		}

		function hasRemoveRight()
		{
			global $UserID;

            if ($this->NoticeID != "" && $this->IssueUserID == $UserID) return true;
            //if ($this->hasFullRight()) return true;

            // [2020-0604-1821-16170] has NoticeID > check by notice type
            $hasFullRight = $this->hasFullRight();
            if ($this->NoticeID != '') {
                if ($this->Module == "Payment") {
                    $hasFullRight = $this->hasPaymentNoticeFullRight();
                } else {
                    $hasFullRight = $this->hasSchoolNoticeFullRight();
                }
            }
            if ($hasFullRight) {
                return true;
            }

			return false;
		}

		function getClassTeacherCurrentTeachingClass()
        {
            include_once("form_class_manage.php");
            $fcm = new form_class_manage();

            // Get teaching classes
            $teachingClassIDArr = $fcm->Get_Class_Teacher_Class($_SESSION['UserID']);
            return $teachingClassIDArr;
        }

		function isClassTeacherAudienceTargetLimited()
        {
            global $sys_custom;
            if(!$sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly']) {
                return false;
            }
            /*
            if($_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'] || !$this->isTeacherStaff()) {
                return false;
            }
            */

            if(!$this->isTeacherStaff()) {
                return false;
            }

            $teachingClassIDArr = $this->getClassTeacherCurrentTeachingClass();
            return !empty($teachingClassIDArr);
        }

		function returnEditLink($id, $TargetType="P")
		{
			global $image_path, $button_edit, $LAYOUT_SKIN;
			
			if($this->Module == "Payment"){
				return "<A HREF='/home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice_edit.php?NoticeID=$id'><img src=\"$image_path/$LAYOUT_SKIN/icon_edit_b.gif\" alt=\"$button_edit\" border=0></A>";
			}
			else {
				$student_notice_path = $TargetType=="S" ? "student_notice/":"";
				return "<A HREF='/home/eAdmin/StudentMgmt/notice/". $student_notice_path ."edit.php?NoticeID=$id'><img src=\"$image_path/$LAYOUT_SKIN/icon_edit_b.gif\" alt=\"$button_edit\" border=0></A>";
			}
		}

		function returnDeleteLink ($id)
		{
			global $image_path, $button_remove;
			return "<A HREF=/home/eAdmin/StudentMgmt/notice/remove.php?NoticeID=$id><img src=\"$image_path/eraser_icon.gif\" alt=\"$button_remove\" border=0></A>";
		}
		
		function returnIssuerName()
		{
			$name_field = getNameFieldByLang("");
			$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '".$this->IssueUserID."'";
			$result = $this->returnVector($sql);
			return ($result[0]==""? $this->IssueUserName: $result[0]);
		}

		function getParentAnswer($StudentID)
		{
			$sql = "SELECT Answer FROM INTRANET_NOTICE_REPLY WHERE StudentID = '$StudentID' AND NoticeID = '".$this->NoticeID."'";
			$result = $this->returnVector($sql);
			return $result[0];
		}
		
		# Split group and user target
		function splitTargetGroupUserID($target)
		{
			$GroupIDList = "";
			$UserIDsList = "";
			$group_delimiter = "";
			$user_delimiter = "";
			$row = explode(",",$target);
			for($i=0; $i<sizeof($row); $i++)
			{
				$row[$i] = trim($row[$i]);
				$targetType = substr($row[$i],0,1);
				$targetID = substr($row[$i],1);
				if ($targetType=="G")
				{
					$GroupIDList .= $group_delimiter.$targetID;
					$group_delimiter = ",";
				}
				else
				{
					$UserIDsList .= $user_delimiter.$targetID;
					$user_delimiter = ",";
				}
			}
			$x[0] = ($GroupIDList == ""? 0:$GroupIDList);
			$x[1] = ($UserIDsList == ""? 0:$UserIDsList);
			return $x;
		}
		
		# Find the UserID of target for type 4
		function returnTargetUserIDArray($target, $UserRecordStatus=1, $sortByClass=false)
		{
			$row = $this->splitTargetGroupUserID($target);
			$GroupIDList = $row[0];
			$UserIDsList = $row[1];
			
			$username_field = getNameFieldWithClassNumberEng("a.");
			
			# Group
			if ($GroupIDList!="")
			{
				$conds = "b.GroupID IN ($GroupIDList)";
			}
			else
			{
				$conds = "";
			}
			
			if ($UserIDsList!="")
			{
				if ($conds == "") {
					$conds = "AND a.UserID IN ($UserIDsList)";
                } else {
				    $conds = "AND ($conds OR a.UserID IN ($UserIDsList))";
                }
			}
			else
			{
				$conds = "AND $conds";
			}
			
			$sortings = "";
			if($sortByClass)
			{
				$sortings = " ORDER BY a.ClassName, a.ClassNumber+0";
			}
			
			if ($UserIDsList=="" && $GroupIDList=="") return array();
			
			$sql = "SELECT DISTINCT a.UserID,$username_field,a.RecordType
			        FROM INTRANET_USER as a, INTRANET_USERGROUP as b
			        WHERE a.UserID = b.UserID AND a.RecordType = 2
			              AND a.RecordStatus in ($UserRecordStatus)
			              $conds
			        $sortings";
			$result = $this->returnArray($sql,3);
			
			return $result;
		}

		function returnIndividualTypeNames($target)
		{
			$IDs = $this->splitTargetGroupUserID($target);
			$groups = $IDs[0];
			$users = $IDs[1];
			
			$sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID IN ($groups) ORDER BY Title";
			$result1 = $this->returnVector($sql);
			
			$namefield = getNameFieldWithClassNumberByLang();
			$sql = "SELECT $namefield FROM INTRANET_USER WHERE UserID IN ($users) ORDER BY ClassName, ClassNumber+0, ClassNumber";
			$result2 = $this->returnVector($sql);
			
			return array_merge($result1,$result2);
		}
		
		function returnIndividualTypeNames2($target)
		{
			$IDs = $this->splitTargetGroupUserID($target);
			$groups = $IDs[0];
			$users = $IDs[1];
			
			$groupNameField = Get_Lang_Selection('TitleChinese','Title');
			$sql = "SELECT concat('G',GroupID) as tempID, $groupNameField as tempName FROM INTRANET_GROUP WHERE GroupID IN ($groups) ORDER BY Title";
			$result1 = $this->returnArray($sql);
			
			$namefield = getNameFieldWithClassNumberByLang();
			$sql = "SELECT concat('U',UserID) as tempID, $namefield as tempName FROM INTRANET_USER WHERE UserID IN ($users) ORDER BY ClassName, ClassNumber+0, ClassNumber";
			$result2 = $this->returnArray($sql);
			
			return array_merge($result1,$result2);
		}
		
		/*
		 # Please call libclass => returnHeadingClass();
		 function returnClassTeacher($uid)
		 {
		 $sql = "SELECT a.ClassName FROM INTRANET_CLASS as a, INTRANET_CLASSTEACHER as b
		 WHERE a.ClassID = b.ClassID AND b.UserID = $uid";
		 $result = $this->returnVector($sql);
		 return $result[0];
		 }
		 */
		/*function returnNoticeListTeacherView($status,$year,$month,$returnSQL=0)
		 {
		 
		 $conds = "";
		 if ($year != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
		 }
		 if ($month != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
		 }
		 $name_field = getNameFieldByLang("b.");
		 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),a.NoticeNumber,a.Title,
		 IF (b.UserID IS NULL,CONCAT('<I>',a.IssueUserName,'</I>'),$name_field),a.RecordType
		 FROM INTRANET_NOTICE as a LEFT OUTER JOIN INTRANET_USER as b ON a.IssueUserID = b.UserID
		 WHERE a.RecordStatus = $status $conds";
		 
		 if(!$returnSQL) $sql .= " ORDER BY a.DateStart DESC";
		 return $returnSQL ? $sql : $this->returnArray($sql,6);
		 }*/
		
		function returnNoticeListTeacherView($status, $year, $month, $returnSQL=0, $keyword='', $module='', $TargetType="P", $hiddenFuture=0, $classTeacherRecordOnly=false)
		{
			global $i_Notice_ModuleID;

			if($keyword != "")
			{
				$keyword = intranet_htmlspecialchars(trim($keyword));
				$PICNoticeIDArr = $this->returnPICNoticeByKeyword($keyword);
				$PICNoticeIDArr = implode('\',\'', $PICNoticeIDArr);
				$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%') OR (b.EnglishName like '%".$keyword."%')
				                       OR (b.ChineseName like '%".$keyword."%') OR (approver.EnglishName like '%".$keyword."%') OR (approver.ChineseName like '%".$keyword."%') OR (a.NoticeID IN('$PICNoticeIDArr'))) ";
			}
			else
            {
				$keyword_conds = "";
			}

			$conds = "";
			if ($year != "")
			{
				$conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
			}
			if ($month != "")
			{
				$conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
			}
			
			if(strtoupper($module) == "ALL")
			{
				$conds .= "";
			}
			else if(strtoupper($module) == "PAYMENT")
			{
				$conds .= " AND a.Module = 'Payment'";
			}
			else
			{
				// Show school notice only
				$conds .= " AND (a.Module IS NULL OR a.Module = 'eEnrolment')";
			}

			// debug_pr('$hiddenFuture='.$hiddenFuture);
			if($hiddenFuture)
			{
				//$today = date("Y-m-d");
				$today = date("Y-m-d H:i:s");
				$date_cond = " AND a.DateStart <= '$today' ";
			}

			// [2019-0715-0929-00235] filter Class Teacher records only
			if($classTeacherRecordOnly)
            {
                include_once("form_class_manage.php");
                $fcm = new form_class_manage();

                // Get teaching classes
                $teachingClassIDArr = $fcm->Get_Class_Teacher_Class($_SESSION['UserID']);
                $teachingClassIDArr = Get_Array_By_Key($teachingClassIDArr, 'ClassID');

                // Get students in teaching classes
                $teachingStudentIDArr = $fcm->Get_Student_By_Class($teachingClassIDArr);
                $teachingStudentIDArr = Get_Array_By_Key($teachingStudentIDArr, 'UserID');

                // Get notice for teaching classes
                $sql = "SELECT 
                            DISTINCT a.NoticeID 
                        FROM
                            INTRANET_NOTICE a
                            INNER JOIN INTRANET_NOTICE_REPLY b ON (a.NoticeID = b.NoticeID)
                        WHERE
						    a.RecordStatus IN ($status) AND 
						    (a.IsModule = 0 OR a.Module = 'eEnrolment')
						    $conds AND 
						    a.IsDeleted = 0 AND
						    a.TargetType = '$TargetType' AND 
						    b.StudentID IN ('".implode("','", (array)$teachingStudentIDArr)."')
						    $date_cond ";
                $ClassNoticeIdArr = $this->returnVector($sql);
                $class_notice_cond = empty($ClassNoticeIdArr)? " AND 0 " : " AND a.NoticeID IN ('".implode("','", (array)$ClassNoticeIdArr)."') ";
            }
			
			$name_field = getNameFieldByLang("b.");
			$approver_name_field = getNameFieldByLang("approver.");
			$sql = "SELECT
						a.NoticeID,
						DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),
						DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),
						a.NoticeNumber,
						a.Title,
						IF(b.UserID IS NULL,CONCAT('<I>',a.IssueUserName,'</I>'),$name_field),
						a.RecordType,
						IF(a.Module IS NULL,'',
                            CASE
                                WHEN Module = 1 THEN '$i_Notice_ModuleID[1]'
                                WHEN Module = 2 THEN '$i_Notice_ModuleID[2]'
                                ELSE Module
                            END
						) as Module,
						$approver_name_field as Approver,
						ApprovedTime,
						ApprovalComment
					FROM
					    INTRANET_NOTICE as a 
                        LEFT OUTER JOIN INTRANET_USER as b ON a.IssueUserID = b.UserID
					    LEFT JOIN INTRANET_USER AS approver ON (approver.UserID = a.ApprovedBy)
					WHERE
						a.RecordStatus IN ($status) AND
						(a.IsModule = 0 OR a.Module = 'eEnrolment')
						$keyword_conds
						$conds AND 
						a.IsDeleted = 0 AND
						a.TargetType = '$TargetType'
						$date_cond
						$class_notice_cond ";
			if(!$returnSQL) $sql .= " ORDER BY a.DateStart DESC";

			//debug_pr($sql);//die();
			return $returnSQL ? $sql : $this->returnArray($sql);
		}
		
		function returnAllPaymentNotice($status, $year, $month, $returnSQL=0, $keyword='', $classTeacherRecordOnly=false)
		{
			global $i_Notice_ModuleID;

			$conds = "";
			if ($year != "") {
				$conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
			}
			if ($month != "") {
				$conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
			}

			if($keyword != "")
			{
				$keyword = intranet_htmlspecialchars(trim($keyword));
				$PICNoticeIDArr = $this->returnPICNoticeByKeyword($keyword);
				$PICNoticeIDArr = implode('\',\'', $PICNoticeIDArr);
				$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%') OR (b.EnglishName like '%".$keyword."%')
				                       OR (b.ChineseName like '%".$keyword."%') OR (approver.EnglishName like '%".$keyword."%') OR (approver.ChineseName like '%".$keyword."%') OR (a.NoticeID IN('$PICNoticeIDArr'))) ";
				//$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%') OR (b.EnglishName like '%".$keyword."%')
				//                       OR (b.ChineseName like '%".$keyword."%'))";
			}
			else{
				$keyword_conds = "";
			}

            // [2019-0715-0929-00235] filter Class Teacher records only
            if($classTeacherRecordOnly)
            {
                include_once("form_class_manage.php");
                $fcm = new form_class_manage();

                // Get teaching classes
                $teachingClassIDArr = $fcm->Get_Class_Teacher_Class($_SESSION['UserID']);
                $teachingClassIDArr = Get_Array_By_Key($teachingClassIDArr, 'ClassID');

                // Get students in teaching classes
                $teachingStudentIDArr = $fcm->Get_Student_By_Class($teachingClassIDArr);
                $teachingStudentIDArr = Get_Array_By_Key($teachingStudentIDArr, 'UserID');

                // Get notice for teaching classes
                $sql = "SELECT 
                            DISTINCT a.NoticeID 
                        FROM
                            INTRANET_NOTICE a
                            INNER JOIN INTRANET_NOTICE_REPLY b ON (a.NoticeID = b.NoticeID)
                        WHERE
						    a.IsDeleted = 0 AND
			                a.Module = 'Payment' AND
						    a.RecordStatus IN ($status) AND 
						    b.StudentID IN ('".implode("','", (array)$teachingStudentIDArr)."') AND 
						    a.IsModule = 0 $conds $keyword_conds ";
                $ClassNoticeIdArr = $this->returnVector($sql);
                $class_notice_cond = empty($ClassNoticeIdArr)? " AND 0 " : " AND a.NoticeID IN ('".implode("','", (array)$ClassNoticeIdArr)."') ";
            }

			$name_field = getNameFieldByLang("b.");
			$approver_name_field = getNameFieldByLang("approver.");
			
			## Modified By : Ronald - add one more where condition "isDeleted = 0"
			$sql = "SELECT
			          a.NoticeID, DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i:%s'),DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i:%s'), a.NoticeNumber, a.Title,
                      IF (b.UserID IS NULL,CONCAT('<I>',a.IssueUserName,'</I>'),$name_field),a.RecordType,
                      if(a.Module='Payment', 'Payment','') as Module,
                      $approver_name_field as  ApprovedBy,
                      ApprovedTime,
                      ApprovalComment
			        FROM
			          INTRANET_NOTICE as a LEFT OUTER JOIN
			          INTRANET_USER as b ON a.IssueUserID = b.UserID
			          LEFT JOIN INTRANET_USER AS approver ON approver.UserID = a.ApprovedBy
			        WHERE
			          isDeleted = 0 AND
			          Module = 'Payment' AND
			          a.RecordStatus in ($status) AND
			          a.IsModule = 0 $conds $keyword_conds $class_notice_cond ";
			if(!$returnSQL)
				$sql .= " ORDER BY a.DateStart DESC";
				
            return $returnSQL ? $sql : $this->returnArray($sql);
		}
		
		function returnPaymentNoticeListTeacherView($status, $year, $month, $returnSQL=0, $keyword='', $onlyPICRecords=0)
		{
			global $UserID, $i_Notice_ModuleID;
			
			$conds = "";
			if ($year != "") {
				$conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
			}
			if ($month != "") {
				$conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
			}
			if($keyword != "")
			{
				$keyword = intranet_htmlspecialchars(trim($keyword));
				$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%') OR (b.EnglishName like '%".$keyword."%')
				                       OR (b.ChineseName like '%".$keyword."%')) ";
			}
			else{
				$keyword_conds = "";
			}
			$name_field = getNameFieldByLang("b.");
			
			// [2015-0323-1602-46073] Handle sql of Issuer and eNotice PIC
			if($onlyPICRecords){
				$PIC_table = "INNER JOIN INTRANET_NOTICE_PIC PIC ON (PIC.NoticeID = a.NoticeID)";
				$UserIDcond = "PIC.PICUserID = '$UserID' AND";
				$UserField = "PIC.PICUserID";
			} else {
				$PIC_table = "";
				$UserIDcond = "a.IssueUserID = '$UserID' AND";
				$UserField = "a.IssueUserID";
			}
			## Modified By : Ronald - add one more where condition "isDeleted = 0"
			$sql = "SELECT 
                        a.NoticeID, DATE_FORMAT(a.DateStart, '%Y-%m-%d %H:%i:%s'), DATE_FORMAT(a.DateEnd, '%Y-%m-%d %H:%i:%s'), 
                        a.NoticeNumber, a.Title,
                        IF (b.UserID IS NULL, CONCAT('<I>', a.IssueUserName, '</I>'), $name_field), 
                        a.RecordType,
                        IF (a.Module = 'Payment', 'Payment', '') as Module
                    FROM
                        INTRANET_NOTICE as a
                        $PIC_table 
                        LEFT OUTER JOIN INTRANET_USER as b ON $UserField = b.UserID
                    WHERE
                        $UserIDcond
                        isDeleted = 0 AND
                        Module = 'Payment' AND
                        a.RecordStatus IN ($status) AND
                        a.IsModule = 0 
                        $conds $keyword_conds";
			if(!$returnSQL)
				$sql .= " ORDER BY a.DateStart DESC";
				
            return $returnSQL ? $sql : $this->returnArray($sql);
		}
		
		function returnAllNotice($year,$month, $returnSQL=0, $keyword, $studentID="", $TargetType="P", $hiddenFuture=0)
		{
			global $sys_custom;
			
			$conds = "";
			if ($year != "")
			{
				$conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
			}
			if ($month != "")
			{
				$conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
			}
			if($keyword != "")
			{
				$keyword = intranet_htmlspecialchars(trim($keyword));
			}
			if($studentID != "")
            {
				$conds .= " AND c.StudentID IN ($studentID)";
            }

            if($hiddenFuture)
            {
                $today = date("Y-m-d");
                $str = " and a.DateStart<='$today' ";
            }
            $name_field = getNameFieldByLang("b.");

            $sql = "SELECT
                        a.NoticeID,
                        DATE_FORMAT(a.DateStart, '%Y-%m-%d'),
                        DATE_FORMAT(a.DateEnd, '%Y-%m-%d'),
                        IF (a.IsModule = 1, '--', a.NoticeNumber),
                        a.Title,
                        a.RecordType,
                        IF(a.Module IS NULL, '',
                            CASE
                                WHEN Module = 1 THEN '$i_Notice_ModuleID[1]'
                                WHEN Module = 2 THEN '$i_Notice_ModuleID[2]'
                                ELSE Module
                            END
                        ) as Module,
                        c.StudentID
                    FROM INTRANET_NOTICE_REPLY as c
                        LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                        LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                        LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.StudentID.InputBy
                    WHERE
                        ((a.NoticeNumber LIKE '%$keyword%') OR (a.Title LIKE '%$keyword%') OR (a.Description LIKE '%$keyword%') OR 
                           (d.EnglishName LIKE '%".$keyword."%') OR (d.ChineseName LIKE '%".$keyword."%')) AND
                        a.RecordStatus = 1 AND a.IsModule = 0
                        $conds
                        AND a.IsDeleted=0
                        AND a.TargetType='$TargetType'
                        $str
				";
				if($sys_custom['skss']['StudentAwardReport']) {
					$sql .= " AND a.Module!='DISCIPLINE_STUDENTAWARDREPORT'";
				}
				
				if(!$returnSQL) $sql .= " ORDER BY a.DateStart DESC, a.NoticeID Desc";
				
				return $returnSQL ? $sql : $this->returnArray($sql);
		}
		
		function returnAllNoticeWithStatus($moduleName="", $TargetType="P")
		{
			if($moduleName!='')
				$conds = " AND Module='$moduleName'";
            else
                $conds = " AND (Module='' or Module is NULL)";

            $sql = "SELECT 
                        a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),a.NoticeNumber,a.Title,
					    a.RecordType, a.RecordStatus
					FROM INTRANET_NOTICE as a
					WHERE (a.RecordStatus = 1 OR a.RecordStatus = 2) $conds
					    AND a.TargetType = '$TargetType'
					ORDER BY a.DateStart DESC, a.NoticeID DESC";
            return $this->returnArray($sql,6);
		}
		
		function returnMyNotice($year="", $month="", $status="", $returnSQL = 0, $keyword='', $TargetType='P', $hiddenFuture=0, $targetPICNotice=0, $module='ALL')
		{
			global $UserID;
			
			if($keyword != "")
			{
				$keyword = intranet_htmlspecialchars(trim($keyword));
				$keyword_conds = " AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%') OR (a.Description like '%$keyword%')) ";
			}
			else{
				$keyword_conds = "";
			}

			$conds = "";
			if ($year != "")
			{
				$conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
			}
			if ($month != "")
			{
				$conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
			}
			if($status != "")
			{
				$conds .= " AND a.RecordStatus='$status'";
			}
            if(strtoupper($module) == "ALL")
            {
                $conds .= "";
            }
            else if(strtoupper($module) == "PAYMENT")
            {
                $conds .= " AND a.Module = 'Payment'";
            }
            else if(strtoupper($module) == "SCHOOL")
            {
                // Show school notice only
                $conds .= " AND (a.Module IS NULL OR a.Module = 'eEnrolment')";
            }
			//$conds .= " AND a.Module IS NULL ";

			if($hiddenFuture)
			{
				//$today = date("Y-m-d");
				$today = date("Y-m-d H:i:s");
				$str = " and a.DateStart<='$today' ";
			}

			// [2015-0323-1602-46073] get PIC or Issuer sql condition
			if($targetPICNotice){
				$PIC_table = "INNER JOIN INTRANET_NOTICE_PIC AS PIC ON (PIC.NoticeID = a.NoticeID)";
				$UserIDcond = "AND PIC.PICUserID = '$UserID'";
			} else {
				$PIC_table = "";
				$UserIDcond = "AND a.IssueUserID = '$UserID'";
			}

			$sql = "SELECT
			          a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d %H:%i'),DATE_FORMAT(a.DateEnd,'%Y-%m-%d %H:%i'),a.NoticeNumber,a.Title,a.RecordStatus,a.RecordType
			        FROM 
                      INTRANET_NOTICE as a
			          $PIC_table
			        WHERE 1
			          $keyword_conds
                      $UserIDcond
                      $conds
			          AND a.IsModule=0
			          AND a.IsDeleted=0
			          AND a.TargetType='$TargetType'
			          $str
			        GROUP BY a.NoticeID
			";
			if(!$returnSQL)	$sql .= " ORDER BY a.DateStart DESC";
			return $returnSQL ? $sql : $this->returnArray($sql);
		}
		
		function returnNoticeTotalCount($nid='')
		{
			/*
			 if($nid)
			 {
			 $cond = " where NoticeID = " . $nid;
			 }
			 
			 $sql = "SELECT NoticeID,COUNT(NoticeReplyID) FROM INTRANET_NOTICE_REPLY
			 $cond
			 GROUP BY NoticeID";
			 return build_assoc_array($this->returnArray($sql,2));
			 */
			
			return $this->returnNoticeSignedCountOnce($nid, "TOTAL");
		}
		
		function returnNoticeSignedCount($nid='')
		{
			/*
			 if($nid)
			 {
			 $cond = "and NoticeID = " . $nid;
			 }
			 
			 $sql = "SELECT NoticeID,COUNT(NoticeReplyID) FROM INTRANET_NOTICE_REPLY
			 WHERE RecordStatus = 2
			 $cond
			 GROUP BY NoticeID";
			 $raws = $this->returnArray($sql,2);
			 $rows = build_assoc_array($this->returnArray($sql,2));
			 return $rows;
			 */
			
			return $this->returnNoticeSignedCountOnce($nid, "SIGNED");
		}
		
		function returnNoticeSignedCountOnce($nid='', $TotalOrSigned)
		{
			# 1. return from global if loaded before
			if ($nid!="")
			{
				# particular notice
				if (is_array($this->NoticeReplyCountArr[$nid]) && sizeof($this->NoticeReplyCountArr[$nid])>0)
				{
					return $this->NoticeReplyCountArr[$nid][$TotalOrSigned];
				}
			}
			else
			{
				# all notices
				if (is_array($this->AllNoticeReplyCountArr) && sizeof($this->AllNoticeReplyCountArr)>0)
				{
					return $this->AllNoticeReplyCountArr[$TotalOrSigned];
				}
			}
			
			# 2. else to retrieve and consolidate
			if($nid)
			{
				$cond = "and NoticeID = " . $nid;
			}
			
			$sql = "SELECT NoticeID, RecordStatus, COUNT(NoticeReplyID) AS TotalNumber FROM INTRANET_NOTICE_REPLY
			        WHERE 1 $cond
			        GROUP BY NoticeID, RecordStatus ORDER BY NoticeID, RecordStatus ";
			$rows = $this->returnResultSet($sql);
			
			$ArrTotal = array();
			$ArrSigned = array();
			for ($i=0; $i<sizeof($rows); $i++)
			{
				$ArrTotal[$rows[$i]["NoticeID"]] += 0 + $rows[$i]["TotalNumber"];
				if ($rows[$i]["RecordStatus"]==2)
				{
					$ArrSigned[$rows[$i]["NoticeID"]] = 0 + $rows[$i]["TotalNumber"];
				}
			}
			
			if ($nid!="")
			{
				# particular notice
				$this->NoticeReplyCountArr[$nid]["SIGNED"] = $ArrSigned;
				$this->NoticeReplyCountArr[$nid]["TOTAL"] = $ArrTotal;
				return $this->NoticeReplyCountArr[$nid][$TotalOrSigned];
			}
			else
			{
				# particular notices
				$this->AllNoticeReplyCountArr["SIGNED"] = $ArrSigned;
				$this->AllNoticeReplyCountArr["TOTAL"] = $ArrTotal;
				return $this->AllNoticeReplyCountArr[$TotalOrSigned];
			}
		}
		
		function returnClassList($nid="")
		{
			$cond_NoticeID = '';
			if (is_array($nid) && count($nid) > 0) {
				$cond_NoticeID = " AND NoticeID IN (".implode(',', $nid).") ";
			}
			else {
				if(!$nid) {
					$nid = $this->NoticeID;
				}
				$cond_NoticeID = " AND NoticeID = '".$nid."' ";
			}
			
			$sql = "SELECT DISTINCT b.ClassName, NoticeID
    				FROM INTRANET_NOTICE_REPLY as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
    				WHERE 1 $cond_NoticeID
    				ORDER BY b.ClassName";
			$ClassArr = $this->returnArray($sql);
			
			if (!is_array($nid))
			{
				return Get_Array_By_Key($ClassArr, 'ClassName');
			}
			else
			{
				$ReturnArr = array();
				$numOfClass = count($ClassArr);
				for($i=0; $i<$numOfClass; $i++)
				{
					$thisNoticeID = $ClassArr[$i]['NoticeID'];
					$thisClassName = $ClassArr[$i]['ClassName'];
					(array)$ReturnArr[$thisNoticeID][] = $thisClassName;
				}
				return $ReturnArr;
			}
		}
		
		function returnTotalCountByClass()
		{
			$sql = "SELECT b.ClassName,COUNT(a.NoticeReplyID)
                         FROM INTRANET_NOTICE_REPLY as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                         WHERE a.NoticeID = '".$this->NoticeID."'
                         GROUP BY b.ClassName
                         ";
			$result = $this->returnArray($sql,2);
			
			# cater ClassName is null or ClassName is empty
			$result_tmp = array();
			foreach($result as $k=>$d)
			{
				list($this_class, $this_count) = $d;
				if($this_class=='' || $this_class=='0')
				{
					$result_tmp['NULL'] += $this_count;
				}
				else
				{
					$result_tmp[$this_class] = $this_count;
				}
			}
			//return build_assoc_array($this->returnArray($sql,2));
			return $result_tmp;
		}

		function returnSignedCountByClass()
		{
			$sql = "SELECT b.ClassName,COUNT(a.NoticeReplyID)
                     FROM INTRANET_NOTICE_REPLY as a 
                          LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                     WHERE a.NoticeID = '".$this->NoticeID."' AND a.RecordStatus = 2
                     GROUP BY b.ClassName
                         ";
			$result = $this->returnArray($sql,2);
			
			# cater ClassName is null or ClassName is empty
			$result_tmp = array();
			foreach($result as $k=>$d)
			{
				list($this_class, $this_count) = $d;
				if($this_class!='')
				{
					$result_tmp[$this_class] = $this_count;
				}
				else
				{
					$result_tmp['NULL'] += $this_count;
				}
			}
			//return build_assoc_array($this->returnArray($sql,2));
			return $result_tmp;
		}

		function returnStudentClass($StudentID)
		{
			$sql = "SELECT ClassName FROM INTRANET_USER WHERE UserID = '$StudentID'";
			$result = $this->returnVector($sql);
			return $result[0];
		}

		function isEditAllowed($StudentID)
		{
			global $UserID;

            // [2020-0604-1821-16170] has NoticeID > check by notice type
            $hasFullRight = $this->hasFullRight();
            if ($this->NoticeID != '') {
                if ($this->Module == "Payment") {
                    $hasFullRight = $this->hasPaymentNoticeFullRight();
                } else {
                    $hasFullRight = $this->hasSchoolNoticeFullRight();
                }
            }

            //if ($this->hasFullRight() || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]) return true;
			if ($hasFullRight || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]) return true;
			
			include_once("libclass.php");
			$lclass = new libclass();
			
			// [2016-0106-1042-16066] for multiple teaching classes
			/*
			 $class = $lclass->returnHeadingClass($UserID);
			 if ($class=="") return false;
			 
			 return (!$this->isClassTeacherEditDisabled && $this->returnStudentClass($StudentID)==$class);
			 */
			// get all teaching classes
			$class = $lclass->returnHeadingClass($UserID, 1);
            if(empty($class)) return false;
			$class = Get_Array_by_Key($class,"ClassTitleEN");
			
			return (!$this->isClassTeacherEditDisabled && in_array($this->returnStudentClass($StudentID),$class));
		}

		function isNoticeEditable($noticeID="")
		{
			global $UserID;

			if ($noticeID == "") {
				$noticeID = $this->NoticeID;
			}
			if ($noticeID == "") {
				return false;
			}
			$this->returnRecord($noticeID);
			
			$now = time();
			$start = strtotime($this->DateStart);
			$status = $this->RecordStatus;
			
			# user can edit notice even the notice is past <== just only edit some fields
			// if ($start > $now || $status == 2 || $status == 3)

            // [2020-0604-1821-16170] has NoticeID > check by notice type
            $hasFullRight = $this->hasFullRight();
            if ($this->NoticeID != '') {
                if ($this->Module == "Payment") {
                    $hasFullRight = $this->hasPaymentNoticeFullRight();
                } else {
                    $hasFullRight = $this->hasSchoolNoticeFullRight();
                }
            }

            // return ($this->IssueUserID == $UserID || $this->hasFullRight());
			return ($this->IssueUserID == $UserID || $hasFullRight);
			// return false;
		}

		function returnRecipientNames()
		{
			global $i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,$i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual;
			global $intranet_session_language;
			
			if ($this->NoticeID == "") return array("","");
			//                 if ($this->RecordType == 1) return array($i_Notice_RecipientTypeAllStudents,"");
			//                 $recipient = $this->RecipientID;
			return $this->returnRecipientNames2($this->RecordType, $this->RecipientID);
			
			/*
			 if ($this->RecordType == 2)
			 {
			 //$sql = "SELECT LevelName FROM INTRANET_CLASSLEVEL WHERE ClassLevelID IN ($recipient) ORDER BY LevelName";
			 $sql = "SELECT YearName FROM YEAR WHERE YearID IN ($recipient) ORDER BY Sequence";
			 $result = $this->returnVector($sql);
			 return array($i_Notice_RecipientTypeLevel,implode(", ",$result));
			 }
			 else if ($this->RecordType == 3)
			 {
			 //$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassID IN ($recipient) ORDER BY ClassName";
			 $sql = "SELECT ClassTitleEN, ClassTitleB5 FROM YEAR_CLASS WHERE YearClassID IN ($recipient) ORDER BY Sequence";
			 $result = $this->returnArray($sql);
			 $class_list = array();
			 foreach($result as $k=>$d)
			 {
			 $class_list[] = $intranet_session_language=="en" ? $d[0] : $d[1];
			 }
			 
			 return array($i_Notice_RecipientTypeClass,implode(", ",$class_list));
			 }
			 else if ($this->RecordType == 4)
			 {
			 $result = $this->returnIndividualTypeNames($recipient);
			 return array($i_Notice_RecipientTypeIndividual,implode(", ",$result));
			 }
			 return array("","");
			 */
		}
		
		function returnRecipientNames2($RecordType, $recipient="")
		{
			global $i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,$i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual;
			global $intranet_session_language;
			
			if ($RecordType == 1) return array($i_Notice_RecipientTypeAllStudents,"");
			if ($RecordType == 2)
			{
				$sql = "SELECT YearName FROM YEAR WHERE YearID IN ($recipient) ORDER BY Sequence";
				$result = $this->returnVector($sql);
				return array($i_Notice_RecipientTypeLevel,implode(", ",$result));
			}
			else if ($RecordType == 3)
			{
				$sql = "SELECT yc.ClassTitleEN, yc.ClassTitleB5 FROM YEAR_CLASS yc INNER JOIN YEAR y ON (yc.YearID = y.YearID) WHERE yc.YearClassID IN ($recipient) ORDER BY y.Sequence, yc.Sequence";
				$result = $this->returnArray($sql);
				$class_list = array();
				foreach($result as $k=>$d)
				{
					// $class_list[] = $intranet_session_language=="en" ? $d[0] : $d[1];
				    $class_list[] = Get_Lang_Selection($d[1], $d[0]);
				}
				
				return array($i_Notice_RecipientTypeClass,implode(", ",$class_list));
			}
			else if ($RecordType == 4)
			{
				//$result = $this->returnIndividualTypeNames($recipient);
				// changed to get target students from INTRANET_NOTICE_REPLY instead of from RecipientID, because now allow to add more students when edit the notice
	            $namefield = getNameFieldWithClassNumberByLang("u.");
	         	$sql = "SELECT $namefield FROM INTRANET_NOTICE_REPLY as r INNER JOIN INTRANET_USER as u ON u.UserID=r.StudentID WHERE r.NoticeID='".$this->NoticeID."' ORDER BY u.ClassName, u.ClassNumber+0";
	         	$result = $this->returnVector($sql);
				return array($i_Notice_RecipientTypeIndividual,implode(", ",$result));
			}
			return array("","");
		}
		
		function returnNoticePICNames()
        {
			if ($this->NoticeID == "") return array();
			
			$namefield = getNameFieldByLang("IU.");
			$sql = "SELECT CONCAT('U',PIC.PICUserID) AS UserID, $namefield AS UserName
                    FROM INTRANET_NOTICE_PIC PIC INNER JOIN INTRANET_USER AS IU ON (PIC.PICUserID = IU.UserID)
                    WHERE PIC.NoticeID = '".$this->NoticeID."'";
			$result = $this->returnArray($sql);
			
			return $result;
		}
		
		function returnNoticePICMapping($noticeArray)
        {
			if (!$noticeArray || $noticeArray=="") return array();
			
			$namefield = getNameFieldByLang("IU.");
			$sql = "SELECT PIC.NoticeID, IU.UserID, $namefield AS UserName
                    FROM INTRANET_NOTICE_PIC PIC 
                        INNER JOIN INTRANET_USER AS IU ON (PIC.PICUserID = IU.UserID)
                    WHERE PIC.NoticeID IN ('".implode("','", (array)$noticeArray)."')";
			$result = $this->returnArray($sql);
			
			if($result){
				$result = BuildMultiKeyAssoc((array)$result, array("NoticeID", "UserID"));
			}
			
			return $result;
		}
		
		/*
		 function displayTeacherView($status,$year="",$month="")
		 {
		 global $i_Notice_DateStart,$i_Notice_NoticeNumber,$i_Notice_Title,$i_Notice_Issuer,
		 $i_Notice_RecipientType,$i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual,$i_Notice_Signed,$i_Notice_Total,
		 $i_Notice_ViewResult,$i_Notice_ViewOwnClass,$i_Notice_NoRecord;
		 global $UserID;
		 global $image_path, $LAYOUT_SKIN;
		 
		 $class = $this->returnClassTeacher($UserID);
		 $isClassTeacher = ($class!="");
		 $viewRight = $this->hasNormalRight();
		 $fullRight = $this->hasFullRight();
		 if ($viewRight || $fullRight)
		 {
		 $totalCounts = $this->returnNoticeTotalCount();
		 $signedCounts = $this->returnNoticeSignedCount();
		 }
		 $notices = $this->returnNoticeListTeacherView($status,$year,$month);
		 $targetType = array("",$i_Notice_RecipientTypeAllStudents,
		 $i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,
		 $i_Notice_RecipientTypeIndividual);
		 
		 $cols = 6;
		 
		 $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
		 $x .= "<tr>\n";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateStart."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_NoticeNumber."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>&nbsp;</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Title."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Issuer."</td>";
		 if ($isClassTeacher && $status == 1)
		 {
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_ViewOwnClass."</td>";
		 $cols++;
		 }
		 if ($viewRight && $status == 1)
		 {
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Signed ."/". $i_Notice_Total."</td>";
		 $cols++;
		 }
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_RecipientType."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'><input type='checkbox' onClick=(this.checked)?setChecked(1,this.form,'NoticeID[]'):setChecked(0,this.form,'NoticeID[]')></td>";
		 $x .= "</tr>\n";
		 
		 $now = mktime(0,0,0,date('m'),date('d'),date('Y'));
		 for ($i=0; $i<sizeof($notices); $i++)
		 {
		 list ($noticeID,$issueDate,$noticeNumber,$title,$issuer,$type) = $notices[$i];
		 
		 $thisClassList = $this->returnClassList($noticeID);
		 
		 $control = "";
		 if ($fullRight)
		 {
		 $start = strtotime($issueDate);
		 
		 ## edit icon
		 if (!($status == 1 && compareDate($start,$now) <= 0))
		 {
		 $editIcon = $this->returnEditLink($noticeID);
		 $title_link = "<a href='/home/notice/edit.php?NoticeID=$noticeID' class='tablelink'>$title</a>";
		 }
		 else
		 {
		 $editIcon = "";
		 $title_link = $title;
		 }
		 }
		 
		 $viewIcon = "<a href='javascript:viewNotice($noticeID)'><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_edit\" border=0></a>";
		 
		 $x .= "<tr class='tablerow". ( $i%2 + 1 )."'>";
		 $x .= "<td class='tabletext' nowrap>$issueDate</td>";
		 $x .= "<td class='tabletext'>$noticeNumber</td>";
		 $x .= "<td class='tabletext' align='left' nowrap>$viewIcon $editIcon</td>";
		 $x .= "<td class='tabletext'>$title_link</td>";
		 $x .= "<td class='tabletext'>$issuer</td>";
		 if ($isClassTeacher && $status == 1)
		 {
		 $class_link = "<a href='javascript:viewNoticeClass($noticeID)' class='tablelink'><img src=\"$image_path/enotice/icon_classview.gif\" border=0 width=32 height=22 align=absmiddle></a>";
		 $x .= "<td class='tabletext'>". (in_array($class, $thisClassList)? $class_link:"") ."</td>";
		 }
		 if ($viewRight && $status == 1)
		 {
		 $numSigned = $signedCounts[$noticeID]+0;
		 $numTotal = $totalCounts[$noticeID]+0;
		 $result_link = "<a href='javascript:viewResult($noticeID)' class='tablelink'><img src=\"$image_path/enotice/icon_resultview.gif\" border=0 width=37 height=22 align=absmiddle>";
		 $x .= "<td class='tabletext'>$result_link $numSigned/$numTotal</a></td>";
		 }
		 $x .= "<td class='tabletext'>".$targetType[$type]."</td>";
		 $x .= "<td class='tabletext' nowrap>". ($this->returnDeleteLink($noticeID) ? "<input type='checkbox' value='$noticeID' name='NoticeID[]'>" : "") ."</td>";
		 $x .= "</tr>\n";
		 }
		 if (sizeof($notices)==0)
		 {
		 $x .= "<tr><td colspan='20' align='center' class='tabletext'>$i_Notice_NoRecord</td></tr>\n";
		 }
		 $x .= "</table>\n";
		 
		 return $x;
		 }
		 */
		
		/*
		 function displayAllNotice($year="",$month="")
		 {
		 global $i_Notice_DateStart,$i_Notice_NoticeNumber,$i_Notice_Title,$i_Notice_Issuer,
		 $i_Notice_RecipientType,$i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual,$i_Notice_Signed,$i_Notice_Total,
		 $i_Notice_ViewResult,$i_Notice_ViewOwnClass,$i_Notice_NoRecord;
		 global $UserID;
		 global $image_path;
		 $notices = $this->returnAllNotice($year,$month);
		 $targetType = array("",$i_Notice_RecipientTypeAllStudents,
		 $i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,
		 $i_Notice_RecipientTypeIndividual);
		 
		 $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
		 $x .= "<tr>\n";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateStart."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_NoticeNumber."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Title."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_RecipientType."</td>";
		 $x .= "</tr>\n";
		 
		 for ($i=0; $i<sizeof($notices); $i++)
		 {
		 list ($noticeID,$issueDate,$noticeNumber,$title,$type) = $notices[$i];
		 $title_link = "<a class='tablelink' href='javascript:viewNotice($noticeID)'>$title</a>";
		 $x .= "<tr class='tablerow". ( $i%2 + 1 )."'>";
		 $x .= "<td class='tabletext'>$issueDate</td><td class='tabletext'>$noticeNumber</td><td class='tabletext'>$title_link</td>";
		 $x .= "<td class='tabletext'>".$targetType[$type]."</td></tr>\n";
		 }
		 
		 if (sizeof($notices)==0)
		 {
		 $x .= "<tr class='tablerow". ( $i%2 + 1 )."'>";
		 $x .= "<td colspan='4' align='center' class='tabletext'>$i_Notice_NoRecord</td></tr>\n";
		 }
		 $x .= "</table>\n";
		 return $x;
		 }
		 */
		
		/*
		 function displayMyNotice($year="",$month="")
		 {
		 global $i_Notice_DateStart,$i_Notice_NoticeNumber,$i_Notice_Title,$i_Notice_Issuer,
		 $i_Notice_RecipientType,$i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual,$i_Notice_Signed,$i_Notice_Total,
		 $i_Notice_ViewResult,$i_Notice_ViewOwnClass,$i_Notice_NoRecord,$i_Notice_Type,
		 $i_Notice_StatusPublished,$i_Notice_StatusSuspended,$i_Notice_StatusTemplate, $status;
		 global $UserID;
		 global $image_path, $LAYOUT_SKIN;
		 $totalCounts = $this->returnNoticeTotalCount();
		 $signedCounts = $this->returnNoticeSignedCount();
		 
		 $notices = $this->returnMyNotice($year,$month,$status);
		 $targetType = array("",$i_Notice_RecipientTypeAllStudents,
		 $i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,
		 $i_Notice_RecipientTypeIndividual);
		 $noticeStatus = array("",$i_Notice_StatusPublished,
		 $i_Notice_StatusSuspended,
		 $i_Notice_StatusTemplate);
		 
		 $cols = 7;
		 
		 $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
		 $x .= "<tr>\n";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateStart."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_NoticeNumber."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>&nbsp;</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Title."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Type."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Signed."/".$i_Notice_Total."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_RecipientType."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'><input type='checkbox' onClick=(this.checked)?setChecked(1,this.form,'NoticeID[]'):setChecked(0,this.form,'NoticeID[]')></td>";
		 $x .= "</tr>";
		 
		 for ($i=0; $i<sizeof($notices); $i++)
		 {
		 list ($noticeID,$issueDate,$noticeNumber,$title,$status,$type) = $notices[$i];
		 
		 ## edit icon
		 if ($this->isNoticeEditable($noticeID))
		 {
		 $editIcon = $this->returnEditLink($noticeID);
		 $title_link = "<a href='/home/notice/edit.php?NoticeID=$noticeID' class='tablelink'>$title</a>";
		 }
		 else
		 {
		 $editIcon = "";
		 $title_link = $title;
		 }
		 
		 ## view icon
		 $viewIcon = "<a href='javascript:viewNotice($noticeID)'><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_edit\" border=0></a>";
		 
		 
		 $x .= "<tr class='tablerow". ( $i%2 + 1 )."'>";
		 $x .= "<td class='tabletext' nowrap>$issueDate</td><td class='tabletext'>$noticeNumber</td>";
		 $x .= "<td class='tabletext' align='left'>$viewIcon $editIcon</td>";
		 $x .= "<td class='tabletext'>$title_link</td>";
		 $x .= "<td class='tabletext'>".$noticeStatus[$status]."</td>";
		 if ($status == 1)
		 {
		 $numSigned = $signedCounts[$noticeID]+0;
		 $numTotal = $totalCounts[$noticeID]+0;
		 $result_link = "<a href='javascript:viewResult($noticeID)' class='tablelink'><img src=\"$image_path/enotice/icon_resultview.gif\" border='0' align='absmiddle'>";
		 $x .= "<td class='tabletext'>$result_link $numSigned/$numTotal</a></td>";
		 }
		 else
		 {
		 $x .= "<td class='tabletext'> -- </td>";
		 }
		 $x .= "<td class='tabletext'>".$targetType[$type]."</td>\n";
		 $x .= "<td class='tabletext' nowrap><input type='checkbox' value='$noticeID' name='NoticeID[]'></td>";
		 $x .= "</tr>";
		 }
		 
		 if (sizeof($notices)==0)
		 {
		 $x .= "<tr><td colspan='$cols' align='center' class='tabletext'>$i_Notice_NoRecord</td></tr>\n";
		 }
		 
		 $x .= "</table>";
		 
		 
		 return $x;
		 }
		 */
		
		/*
		 function displayStudentView($year="",$month="")
		 {
		 global $i_Notice_DateStart,$i_Notice_DateEnd,$i_Notice_NoticeNumber,$i_Notice_Title,$i_Notice_Issuer,
		 $i_Notice_RecipientType,$i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual,$i_Notice_Signed,$i_Notice_Total,
		 $i_Notice_ViewResult,$i_Notice_ViewOwnClass,$i_Notice_StudentName,
		 $i_Notice_OpenSign,$i_Notice_NoRecord;
		 global $UserID;
		 
		 $conds = "";
		 if ($year != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
		 }
		 if ($month != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
		 }
		 
		 $name_field = getNameFieldWithClassNumberByLang("b.");
		 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
		 DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),a.NoticeNumber,a.Title,
		 a.RecordType, c.RecordType, c.RecordStatus
		 FROM INTRANET_NOTICE_REPLY as c
		 LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
		 LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
		 WHERE c.StudentID = $UserID AND a.RecordStatus = 1
		 AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE() $conds
		 ORDER BY a.DateStart DESC";
		 
		 $replies = $this->returnArray($sql,8);
		 $targetType = array("",$i_Notice_RecipientTypeAllStudents,
		 $i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,
		 $i_Notice_RecipientTypeIndividual);
		 
		 $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
		 $x .= "<tr>\n";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateStart."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateEnd."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_NoticeNumber."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Title."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_RecipientType."</td>";
		 $x .= "</tr>";
		 
		 for ($i=0; $i<sizeof($replies); $i++)
		 {
		 list($id,$start,$end,$number,$title,$noticeType,$replyType,$status) = $replies[$i];
		 
		 $css = (($status == 0 || $status == "")? "attendanceabsent":"attendancepresent");
		 if ($replyType==2) $css = "attendanceouting";
		 
		 $title_link = "<a class='tablelink' href='javascript:viewReply($id,$UserID)'>$title</a>";
		 $x .= "<tr class='tabletext $css'><td class='tabletext $css'>$start</td><td class='tabletext $css'>$end</td><td class='tabletext $css'>$number</td>";
		 $x .= "<td class='tabletext $css'>$title_link</td>";
		 $x .= "<td class='tabletext $css'>".$targetType[$noticeType]."</td></tr>\n";
		 }
		 if (sizeof($replies)==0)
		 {
		 $x .= "<tr><td align='center' colspan='5' class='tabletext'>$i_Notice_NoRecord</td></tr>";
		 }
		 $x .= "</table>\n";
		 return $x;
		 }
		 */
		/*
		 function displayStudentHistory($year="",$month="")
		 {
		 global $i_Notice_DateStart,$i_Notice_DateEnd,$i_Notice_NoticeNumber,$i_Notice_Title,$i_Notice_Issuer,
		 $i_Notice_RecipientType,$i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual,$i_Notice_Signed,$i_Notice_Total,
		 $i_Notice_ViewResult,$i_Notice_ViewOwnClass,$i_Notice_StudentName,
		 $i_Notice_OpenSign,$i_Notice_SignerNoColon,$i_Notice_SignedAt,$i_Notice_NoRecord,
		 $i_Notice_Unsigned;
		 global $UserID;
		 
		 $conds = "";
		 if ($year != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
		 }
		 if ($month != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
		 }
		 
		 $name_field = getNameFieldWithClassNumberByLang("b.");
		 #$name_field2 = getNameFieldWithClassNumberByLang("d.");
		 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
		 DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),a.NoticeNumber,a.Title,
		 a.RecordType, c.RecordType, c.RecordStatus,
		 IF(b.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field),
		 c.DateModified
		 FROM INTRANET_NOTICE_REPLY as c
		 LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
		 LEFT OUTER JOIN INTRANET_USER as b ON c.SignerID = b.UserID
		 WHERE c.StudentID = $UserID AND a.DateEnd < CURDATE() $conds AND a.RecordStatus=1
		 ORDER BY a.DateStart DESC";
		 $replies = $this->returnArray($sql,10);
		 $targetType = array("",$i_Notice_RecipientTypeAllStudents,
		 $i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,
		 $i_Notice_RecipientTypeIndividual);
		 
		 
		 $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
		 $x .= "<tr>\n";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateStart."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateEnd."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_NoticeNumber."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Title."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_RecipientType."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_SignerNoColon."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_SignedAt."</td>";
		 $x .= "</tr>";
		 
		 for ($i=0; $i<sizeof($replies); $i++)
		 {
		 list($id,$start,$end,$number,$title,$noticeType,$replyType,$status,$signed,$signedAt) = $replies[$i];
		 if ($signed=="")
		 {
		 $signed = "&nbsp;";
		 $signedAt = "$i_Notice_Unsigned";
		 }
		 $css = (($status == 0 || $status == "")? "attendanceabsent":"attendancepresent");
		 if ($replyType==2) $css = "attendanceouting";
		 
		 $title_link = "<a class='tablelink' href='javascript:viewReply($id,$UserID)'>$title</a>";
		 $x .= "<tr class='tabletext $css'><td class='tabletext $css'>$start</td><td class='tabletext $css'>$end</td><td class='tabletext $css'>$number</td>";
		 $x .= "<td class='tabletext $css'>$title_link</td>";
		 $x .= "<td class='tabletext $css'>".$targetType[$noticeType]."</td><td class='tabletext $css'>$signed</td><td class='tabletext $css'>$signedAt</td></tr>\n";
		 }
		 if (sizeof($replies)==0)
		 {
		 $x .= "<tr><td align='center' colspan='5' class='tabletext'>$i_Notice_NoRecord</td></tr>";
		 }
		 $x .= "</table>\n";
		 return $x;
		 }
		 */
		
		/*
		 function displayParentView($year="",$month="")
		 {
		 global $i_Notice_DateStart,$i_Notice_DateEnd,$i_Notice_NoticeNumber,$i_Notice_Title,$i_Notice_Issuer,
		 $i_Notice_RecipientType,$i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual,$i_Notice_Signed,$i_Notice_Total,
		 $i_Notice_ViewResult,$i_Notice_ViewOwnClass,$i_Notice_StudentName, $i_Notice_SignerNoColon, $i_Notice_SignedAt,
		 $i_Notice_OpenSign,$i_Notice_NoRecord, $i_general_yes, $i_general_no, $i_Notice_Unsigned;
		 global $UserID;
		 
		 # Grab children ids
		 $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
		 $children = $this->returnVector($sql);
		 if (sizeof($children)==0) return "";
		 
		 $child_list = implode(",",$children);
		 $name_field = getNameFieldWithClassNumberByLang("b.");
		 $name_field2 = getNameFieldWithLoginByLang("d.");
		 $conds = "";
		 if ($year != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
		 }
		 if ($month != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
		 }
		 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
		 DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),a.NoticeNumber,a.Title,c.StudentID,
		 IF(b.UserID IS NULL,CONCAT('<I>',c.StudentName,'</I>'),$name_field),
		 a.RecordType, c.RecordType, c.RecordStatus,
		 IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
		 c.DateModified
		 FROM INTRANET_NOTICE_REPLY as c
		 LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
		 LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
		 LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
		 WHERE c.StudentID IN ($child_list) AND a.RecordStatus = 1
		 AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()
		 $conds
		 ORDER BY a.DateStart DESC";
		 $replies = $this->returnArray($sql,12);
		 $targetType = array("",$i_Notice_RecipientTypeAllStudents,
		 $i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,
		 $i_Notice_RecipientTypeIndividual);
		 
		 
		 $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
		 $x .= "<tr>\n";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateStart."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateEnd."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_NoticeNumber."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Title."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_StudentName."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_RecipientType."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_SignerNoColon."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_SignedAt."</td>";
		 $x .= "</tr>\n";
		 
		 for ($i=0; $i<sizeof($replies); $i++)
		 {
		 list($id,$start,$end,$number,$title,$studentID,$name,$noticeType,$replyType,$status,$signer,$signedAt) = $replies[$i];
		 $css = (($status == 0 || $status == "")? "unsigned":"signed");
		 
		 if ($signer=="")
		 {
		 $signer = "&nbsp;";
		 $signedAt = "$i_Notice_Unsigned";
		 }
		 
		 $title_link = "<a class='tablelink' href='javascript:sign($id,$studentID)'>$title</a>";
		 #$sign_link = "<a href=javascript:sign($id,$studentID)>$i_Notice_OpenSign</a>";
		 $x .= "<tr class='tablerow". ( $i%2 + 1 )."'>";
		 $x .= "<td class='tabletext' nowrap>$start</td><td class='tabletext' nowrap>$end</td><td class='tabletext'>$number</td>";
		 $x .= "<td class='tabletext'>$title_link</td><td class='tabletext'>$name</td>";
		 $x .= "<td class='tabletext'>".$targetType[$noticeType]."</td>\n";
		 $x .= "<td class='tabletext'>$signer</td><td class='tabletext'>$signedAt</td>";
		 $x .= "</tr>\n";
		 }
		 if (sizeof($replies)==0)
		 {
		 $x .= "<tr><td align=center colspan=6>$i_Notice_NoRecord</td></tr>";
		 }
		 $x .= "</table>\n";
		 return $x;
		 }
		 */
		
		function getParentNoticeUnsignedCount()
		{
			global $UserID, $sys_custom;

			# Grab children ids
			$sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
			$children = $this->returnVector($sql);
			if (sizeof($children)==0) return "";

			$child_list = implode(",",$children);
			$sql = "SELECT COUNT(c.NoticeReplyID)
                    FROM INTRANET_NOTICE_REPLY as c
                        LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                        LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                    WHERE c.StudentID IN ($child_list) AND a.DateStart <= CURDATE()
                        AND a.DateEnd >= CURDATE() AND c.RecordStatus = 0
                        AND a.RecordStatus = 1
			";
			if($sys_custom['skss']['StudentAwardReport']) {
				$sql .= " AND a.Module!='DISCIPLINE_STUDENTAWARDREPORT'";
			}
			
			$replies = $this->returnVector($sql);
			return $replies[0];
		}

		function getStudentNoticeUnsignedCount()
		{
			global $UserID, $sys_custom;
			
			$sql = "SELECT COUNT(c.NoticeReplyID)
                    FROM INTRANET_NOTICE_REPLY as c
                        LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                        LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                    WHERE c.StudentID = '$UserID' AND a.DateStart <= CURDATE()
                        AND a.DateEnd >= CURDATE() AND c.RecordStatus = 0
                        AND a.RecordStatus in (1)
			";
			if($sys_custom['skss']['StudentAwardReport']) {
				$sql .= " AND a.Module!='DISCIPLINE_STUDENTAWARDREPORT'";
			}
			
			$replies = $this->returnVector($sql);
			return $replies[0];
		}
		
		function getPendingApprovalNotice($targetType='', $module='')
        {
			if ($targetType != '') {
				$cond = " AND TargetType = '$targetType' ";
			}

            // [2020-0604-1821-16170] module type for approval notice
			if ($module == NOTICE_SETTING_TYPE_SCHOOL) {
				$cond .= " AND Module IS NULL AND isModule = 0 ";
			}
			else if ($module == NOTICE_SETTING_TYPE_PAYMENT) {
				$cond .= " AND Module = '$module' ";
			}
			
			// [2016-0928-0903-29236] not only apply to whole school only
			//$sql = "SELECT COUNT(*) as count FROM INTRANET_NOTICE  WHERE RecordType = '1' AND RecordStatus = '4'  $cond";
			$sql = "SELECT COUNT(*) as count FROM INTRANET_NOTICE WHERE RecordStatus = '4' $cond";
			$ary = $this->returnResultSet($sql);
			
			return $ary[0]['count'];
		}
		
		/*
		 function displayParentHistory($year="",$month="")
		 {
		 global $i_Notice_DateStart,$i_Notice_DateEnd,$i_Notice_NoticeNumber,$i_Notice_Title,$i_Notice_Issuer,
		 $i_Notice_RecipientType,$i_Notice_RecipientTypeAllStudents,$i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,$i_Notice_RecipientTypeIndividual,$i_Notice_Signed,$i_Notice_Total,
		 $i_Notice_ViewResult,$i_Notice_ViewOwnClass,$i_Notice_StudentName,
		 $i_Notice_OpenSign,$i_Notice_SignerNoColon,$i_Notice_SignedAt,
		 $i_Notice_NoRecord,$i_Notice_Unsigned;
		 global $UserID;
		 # Grab children ids
		 $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
		 $children = $this->returnVector($sql);
		 if (sizeof($children)==0) return "";
		 
		 $child_list = implode(",",$children);
		 $name_field = getNameFieldWithClassNumberByLang("b.");
		 $name_field2 = getNameFieldWithLoginByLang("d.");
		 $conds = "";
		 if ($year != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
		 }
		 if ($month != "")
		 {
		 $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
		 }
		 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
		 DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),a.NoticeNumber,a.Title,c.StudentID,
		 IF(b.UserID IS NULL,CONCAT('<I>',c.StudentName,'</I>'),$name_field),
		 a.RecordType, c.RecordType, c.RecordStatus,
		 IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
		 c.DateModified
		 FROM INTRANET_NOTICE_REPLY as c
		 LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
		 LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
		 LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
		 WHERE c.StudentID IN ($child_list)
		 AND a.RecordStatus = 1
		 AND a.DateEnd < CURDATE()
		 $conds
		 ORDER BY a.DateStart DESC";
		 
		 $replies = $this->returnArray($sql,12);
		 $targetType = array("",$i_Notice_RecipientTypeAllStudents,
		 $i_Notice_RecipientTypeLevel,
		 $i_Notice_RecipientTypeClass,
		 $i_Notice_RecipientTypeIndividual);
		 
		 $x = "<table width='100%' border='0' cellpadding='4' cellspacing='0'>\n";
		 $x .= "<tr>\n";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateStart."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_DateEnd."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_NoticeNumber."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_Title."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_StudentName."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_RecipientType."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_SignerNoColon."</td>";
		 $x .= "<td class='tabletop tabletopnolink' align='left'>".$i_Notice_SignedAt."</td>";
		 $x .= "</tr>\n";
		 
		 for ($i=0; $i<sizeof($replies); $i++)
		 {
		 list($id,$start,$end,$number,$title,$studentID,$name,$noticeType,$replyType,$status,$signer,$signedAt) = $replies[$i];
		 if ($signer=="")
		 {
		 $signer = "&nbsp;";
		 $signedAt = "$i_Notice_Unsigned";
		 }
		 $css = (($status == 0 || $status == "")? "attendanceabsent":"attendancepresent");
		 
		 if ($replyType==2) $css = "attendanceouting";
		 $title_link = "<a href='javascript:viewReply($id,$studentID)' class='tablelink'>$title</a>";
		 
		 $x .= "<tr>";
		 $x .= "<td class='tabletext $css' nowrap>$start</td><td class='tabletext $css' nowrap>$end</td><td class='tabletext $css'>$number</td>";
		 $x .= "<td class='tabletext $css'>$title_link</td><td class='tabletext $css'>$name</td>";
		 $x .= "<td class='tabletext $css'>".$targetType[$noticeType]."</td><td class='tabletext $css'>$signer</td><td class='tabletext $css'>$signedAt</td></tr>\n";
		 }
		 if (sizeof($replies)==0)
		 {
		 $x .= "<tr><td align='center' colspan='8' class='tabletext'>$i_Notice_NoRecord</td></tr>";
		 }
		 $x .= "</table>\n";
		 return $x;
		 }
		 */
		
		function displayAttachment($targetBlank=false, $token='')
		{
			global $file_path, $image_path, $intranet_httppath, $intranet_root;
			
			if(!empty($this->Attachment))
			{
			    $token_parms = '';
			    if($token != '') {
			        $token_parms = "&token=".$token;
			    }
			    
				$path = "$file_path/file/notice/".$this->Attachment;
				$real_path = $this->Attachment;
				$a = new libfiletable("", $path, 0, 0, "");
				$files = $a->files;
				
				if ($targetBlank == true) {
					$targetAttr = ' target="_blank" ';
				}
				
				while (list($key, $value) = each($files))
				{
					//$url = str_replace(" ", "%20", str_replace($file_path, "", $path)."/".$files[$key][0]);
					//$real_url = str_replace(" ", "%20", str_replace($file_path, "", $real_path)."/".$files[$key][0]);
					$url = str_replace($file_path, "", $path)."/".$files[$key][0];
					// 	                        $url = rawurlencode($intranet_root."/".$file_path.$url);
					$x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
					//$x .= "<a class='tablelink' target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
					//$x .= "<a class='tablelink' target=_blank href=\"download_attachment.php?target=$real_url\" >".$files[$key][0]."</a>";
					//$x .= "<a class='tablelink' target=_blank href=\"/home/download_attachment.php?target=$url\" >".$files[$key][0]."</a>";
					$x .= " <a class=\"tablelink\" $targetAttr href=\"/home/download_attachment.php?target_e=".getEncryptedText($intranet_root.$url).$token_parms."\" >".$files[$key][0]."</a>";
					
					$x .= " (".ceil($files[$key][1]/1000)."Kb)";
					$x .= "<br>\n";
				}

				return $x;
			}
		}
		
		function displayAttachment_showImage($token='')
		{
			global $file_path, $image_path, $intranet_httppath, $intranet_root;
			
			if(!empty($this->Attachment))
			{
			    $token_parms = '';
			    if($token != '') {
                    $token_parms = "&token=".$token;
			    }
			    
				$path = "$file_path/file/notice/".$this->Attachment;
				
				$a = new libfiletable("", $path, 0, 0, "");
				$files = $a->files;
				while (list($key, $value) = each($files))
				{
// 	                $url = str_replace(" ", "%20", str_replace($file_path, "", $path)."/".$files[$key][0]);
// 	                $url = rawurlencode($url);
					$target_filepath = $path."/".$files[$key][0];
					$showImagePath = str_replace($file_path, "", $target_filepath);
					$url = rawurlencode($target_filepath);
					if (isImage($target_filepath))
					{
						$size = GetImageSize($target_filepath);
						list($width, $height, $type, $attr) = $size;
						if ($width > 412 || $height > 550) {
							if($height>$width) {
								$image_html_tag = " height=550";
                            } else if ($height<$width) {
								$image_html_tag = "width=412";
                            }
						}
						else
						{
							$image_html_tag = "";
						}
					}
					$x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
					// $x .= "<a target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
					// $x .= "<a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target=".$url."\" >".$files[$key][0]."</a>";
					$x .= " <a class=\"indexpoplink\" target=\"_blank\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($intranet_root.$showImagePath).$token_parms."\" >".$files[$key][0]."</a>";
					
					$x .= " (".ceil($files[$key][1]/1000)."Kb)";
					if (isImage($target_filepath))
					{
						//$x .= "</br><img border=0 src=\"$intranet_httppath$showImagePath\" $image_html_tag>";
						$x .= "</br><img border=0 src=\"$intranet_httppath$showImagePath\" width='100%'>";
					}
					
					$x .= "<br>\n";
				}

				return $x;
			}
		}
		
		function displayModuleAttachment()   # added by Kelvin Ho 2008-11-12 for ucc
		{
			### Notice : Need to modify later, if the Module Notice can add an attachment ###
			### If you need to do any enhancement in this function, please contact Ronald first. Thanks ###
			global $file_path, $image_path, $intranet_httppath;
			
			//$Module = $this->getModuleName($this->Module);
			$Module = $this->Module;
			$path = "$file_path/file/notice/$Module/".$this->Attachment;
			
			$a = new libfiletable("", $path, 0, 0, "");
			$files = $a->files;
			while (list($key, $value) = each($files))
			{
				$url = str_replace(" ", "%20", str_replace($file_path, "", $path)."/".$files[$key][0]);
				$x .= "<img src=$image_path/file.gif hspace=2 vspace=2 border=0 align=absmiddle>";
				$x .= "<a class='tablelink' target=_blank href=\"$intranet_httppath$url\" onMouseOver=\"window.status='".$files[$key][0]."';return true;\" onMouseOut=\"window.status='';return true;\">".$files[$key][0]."</a>";
				$x .= " (".ceil($files[$key][1]/1000)."Kb)";
				$x .= "<br>\n";
			}
			return $x;
		}
		
		function splitQuestion($qStr, $moreFunct=false)
		{
			$qSeparator = "#QUE#";
			$pSeparator = "||";
			$oSeparator = "#OPT#";
			// [2016-0113-1514-09066] target question syntax
			$rSeparator = "#TAR#";
			
			$questions = explode($qSeparator,$qStr);
			for ($i=1; $i<sizeof($questions); $i++)
			{
				$que = $questions[$i];
				//$is_payment_notice_question = strpos($que,'#PAYMENTITEMID#')!==false;
				
				$parts = explode($pSeparator,$que);
				$mustSubmit = 0;
				
				// [2015-0615-1438-48014] remove Question must be submitted setting
				if(trim($parts[2])!="")
				{
					// [2016-0113-1514-09066] allow return question array with must be submitted Option
					if($moreFunct){
						$mustSubmit = strpos($parts[2], "#MSUB#1")!==false? 1 : 0;
					}
					
					$parts[2] = str_replace("#MSUB#0", "", $parts[2]);
					$parts[2] = str_replace("#MSUB#1", "", $parts[2]);
				}
				$temp = explode(",", $parts[0]);
				$type = $temp[0];
				
				$numOp = 0;
				$options = "";
				$relatedQuestion = array();
				switch ($type)
				{
					// T/F and MC Question
					case 1:
					case 2:
					case 3:
					case 12:
					case 13:
					case 14:
					case 15:
					case 17:
						// Options
					    $numOp = $type==1? 2 : $temp[1];
					    $numOp = $type==12 ? 1 : $numOp;
					    
						$opStr = $parts[2];
						//preg_replace('/(#TAR#-*\d+)/','',$opStr);
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						
						// [2016-0113-1514-09066] Handle Question with Skip Question Settings
						for($optCount=0; $optCount<count($options); $optCount++)
						{
							$currentOption = $options[$optCount];
							list($options[$optCount], $relatedQ) = explode($rSeparator, $currentOption);
							$relatedQ = $relatedQ && strlen($relatedQ)>1 ? substr($relatedQ, 0, -1) : $relatedQ;
							
							if($moreFunct){
								$relatedQuestion[$optCount] = $relatedQ;
							}
						}
						break;
						
						// Text Question
					case 4:
					case 5:
					case 10:
					case 11:
						// Options
						$opStr = $parts[2];
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						
						// [2016-0113-1514-09066] Handle Question with Skip Question Settings
						$currentOption = $options[0];
						list($options[$optCount], $relatedQ) = explode($rSeparator, $currentOption);
						$relatedQ = $relatedQ && strlen($relatedQ)>1 ? substr($relatedQ, 0, -1) : $relatedQ;
						
						if($moreFunct){
							$relatedQuestion[0] = $relatedQ;
						}
						
						// return empty options
						$options = array();
						break;
						
						// Not Applicable
					case 6:
						$options = array();
						break;
					case 7:
					case 8:
					case 9:
						$numOp = $temp[1];
						$opStr = $parts[2];
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						break;
				}
				$mainQ = $parts[1];
				
				for($j=0;$j<count($options);$j++){
					// remove the jump question pattern #TAR#\d+ or #TAR#-1 from option string
					$options[$j] = preg_replace('/(#TAR#[-*\d+]*)/','',$options[$j]);
				}
				
				// [2016-0113-1514-09066] return more details
				if($moreFunct){
					$result[] = array($type, $mainQ, $options, $mustSubmit, $relatedQuestion);
				}
				else{
					$result[] = array($type, $mainQ, $options);
				}
			}
			$this->question_array = $result;
			return $result;
		}
		
		function handleQuestionAnswer($qAry, $optionStr)
		{
			$qSeparator = "#QUE#";
			$oSeparator = "#OPT#";
			$rSeparator = "#TAR#";
			
			// return Question if empty
			if($optionStr=="")
				return $qAry;
				
				// get Question Options
				$tempOptionAry = explode($qSeparator, $optionStr);
				
				// loop Questions
				for($i=0; $i<count($qAry); $i++)
				{
					// Question Info
					$question = $qAry[$i];
					$questionType = $question[0];
					$QOptionStr = $tempOptionAry[$i];
					$QOptionStr = str_replace(">", "&gt;", $QOptionStr);
					$QOptionStr = str_replace("<", "&lt;", $QOptionStr);
					
					switch($questionType)
					{
						// T/F and MC Question
						case 1:
						case 2:
						case 3:
						case 12:
							// loop Options
							$relatedQuestion = array();
							$optionAry = explode($oSeparator, $QOptionStr);
							for($optCount=0; $optCount<count($optionAry); $optCount++)
							{
								$currentOption = $optionAry[$optCount];
								list($optionAry[$optCount], $relatedQ) = explode($rSeparator, $currentOption);
								$relatedQ = $relatedQ && strlen($relatedQ) > 1 ? substr($relatedQ, 0, -1) : $relatedQ;
								$relatedQuestion[$optCount] = $relatedQ;
							}
							
							// Update Question array
							$qAry[$i][2] = $optionAry;
							$qAry[$i][4] = $relatedQuestion;
							break;
							
							// Text Question
						case 4:
						case 5:
							// loop Options
							$relatedQuestion = array();
							list($emptyOption, $relatedQ) = explode($rSeparator, $QOptionStr);
							$relatedQ = $relatedQ && strlen($relatedQ) > 1 ? substr($relatedQ, 0, -1) : $relatedQ;
							$relatedQuestion[0] = $relatedQ;
							
							// Update Question array
							$qAry[$i][4] = $relatedQuestion;
							break;
							
							// Other Question
						default:
							break;
					}
				}
				
				return $qAry;
		}
		
		function handleQuestionRelation($submitTypeArr, $OptionTargetArr)
		{
		    global $sys_custom;
		    
		    // init.
		    $QuestionRelation = array();
		    $QuestionTargetIDArr = array();
			
			// Get Question Relation
			// $this->handleRecusiveRelation($returnAry, $mustSubmitArray, $optionArray, 0);
		    $this->getQuestionRelation($QuestionRelation, $submitTypeArr, $OptionTargetArr, $startIndex=0, $SplitFrom=false, $SpliOption=false, $targetQuestion='', $targetOption='', $QuestionTargetIDArr);

			// Build Question Relation
		    if($sys_custom['eNotice']['ImproveGenReplySlipPerformance']) {
		        $this->buildQuestionRelation($QuestionRelation, $OptionTargetArr, $QuestionTargetIDArr);
		    }
		    
		    return $QuestionRelation;
		}
		
		function getQuestionRelation(&$Relation, $mustSubmitArray, $optionArray, $startIndex, $SplitFrom=false, $SpliOption=false, $targetQuestion, $targetOption, &$QuestionTargetIDArr)
		{
		    global $sys_custom;
		    
			// loop all remaining questions
			for($i=$startIndex; $i<count($mustSubmitArray); $i++)
			{
				$isLastQuestion = $i==(count($mustSubmitArray)-1);
				
				/*
				 * Need to loop
				 * 	1. Must submit question
				 * 	2. T/F OR MC Qestion with skip settings
				 * 	3. 2 options skip to different questions
				 */
				$needToloop = $mustSubmitArray[$i]==1 && $optionArray[1][$i]!="" && ($optionArray[0][$i] != $optionArray[1][$i]);
				$loopOptions = 2;

				// MC Question
				if($mustSubmitArray[$i] > 1)
				{
					$needToloop = true;
					$loopOptions = $mustSubmitArray[$i];
				}
				
				// Checking for specific question and option
				if($SplitFrom !== false && $SpliOption !== false)
				{
					$Relation[$SplitFrom][$SpliOption][] = $i + 1;
				}

				// Build Question Target
				if($sys_custom['eNotice']['ImproveGenReplySlipPerformance'])
				{
				    if($targetQuestion !== '' && $targetOption !== '')
				    {
    				    $QuestionTargetIDArr[$targetQuestion][$targetOption][] = $i + 1;
    				    $QuestionTargetIDArr[$targetQuestion][$targetOption] = array_unique((array)$QuestionTargetIDArr[$targetQuestion][$targetOption]);
    				    asort($QuestionTargetIDArr[$targetQuestion][$targetOption]);
				    }
				}

				/*
				 * Stop recusive checking
				 * 	1. Current Question is the last question
				 * 	2. The branch is end (Option is end the reply slip)
				 */
				if($isLastQuestion || ($SplitFrom!==false && $SpliOption!==false && !$needToloop && $optionArray[0][$i]=="end")) {
					return;
				}
				
				// Calculate Question Relations
				if($needToloop)
				{
					$fromQ = $SplitFrom!==false? $SplitFrom : $i;
					$existingOptionAry = array();
					$endChecking = false;
					
					// loop Options
					for($j=0; $j<$loopOptions; $j++)
					{
						$targetQ = $optionArray[$j][$i];
						$fromOption = $SplitFrom !== false && $SpliOption !== false? $SpliOption : $j;
						
						// Need to continue checking for this option?
						$stopOptionAction = $targetQ == "end";
						if(!$stopOptionAction)
						{
						    $targetQ = $targetQ==""? $i + 1 : $targetQ;
						    
						    // Copy Question Target
						    if($sys_custom['eNotice']['ImproveGenReplySlipPerformance'])
						    {
						        if(isset($existingOptionAry[$targetQ]))
						        {
    						        for($k=0; $k<$loopOptions; $k++) {
    						            if($k != $j && $targetQ == $optionArray[$k][$i]) {
    						                $QuestionTargetIDArr[$i][$j] = $QuestionTargetIDArr[$i][$k];
    						            }
    						        }
                                }
						    }
						    
							// [2016-0912-1001-12226]
						    // Copy Question Relations if in main loop
							if(isset($existingOptionAry[$targetQ]) && $SplitFrom===false && $SpliOption===false)
							{
								$Relation[$fromQ][$fromOption] = $existingOptionAry[$targetQ];
							}
							// Calculate Question Relations
							else if(!isset($existingOptionAry[$targetQ]))
							{
								// [2017-0901-1641-36236]
							    // Remove duplicated relation content
							    $this->getQuestionRelation($Relation, $mustSubmitArray, $optionArray, $targetQ, $fromQ, $fromOption, $i, $j, $QuestionTargetIDArr);
							    
								$Relation[$fromQ][$fromOption] = array_unique((array)$Relation[$fromQ][$fromOption]);
								sort($Relation[$fromQ][$fromOption]);
								$existingOptionAry[$targetQ] = $Relation[$fromQ][$fromOption];
							}
						}
						else
						{
							$endChecking = true;
						}
					}
					unset($existingOptionAry);
					
					/*
					 * Stop recusive checking
					 * 	1. after checking both options
					 */
					if($SplitFrom!==false && $SpliOption!==false)
					{
						return;
					}
				}
				// skip to target question
				else if($SplitFrom!==false && $SpliOption!==false && !$needToloop && ($optionArray[0][$i] > 0) && ($optionArray[0][$i] > ($i + 1)) && $optionArray[0][$i]!="end")
				{
					$i = $optionArray[0][$i] - 1;
				}

				// looping for Q1 only
				if($sys_custom['eNotice']['ImproveGenReplySlipPerformance'])
				{
				    if($startIndex == 0) {
                        break;
				    }
				}
			}

			/*
			 * Stop recusive checking
			 * 	1. after checking on all remaining questions
			 */
			return;
		}
		
		function buildQuestionRelation(&$QuestionRelation, $OptionTargetArr, $QuestionTargetIDArr)
		{
		    // Sort Questions (from last question)
		    $QuestionIDArr = array();
		    foreach((array)$QuestionTargetIDArr as $thisQuestionID => $thisQuestionRelation) {
		        $QuestionIDArr[] = $thisQuestionID;
		    }
		    asort($QuestionIDArr);
		    $QuestionIDArr = array_reverse($QuestionIDArr);
		    
		    /*
		     * Group Target Questions from all Options
		     * 1. Get related Questions directly     ($QuestionTargetIDArr[$thisQuestionID])
		     * 2. Get related Questions indirectly   ($QuestionAllTargetIDArr[$thisQuestionTargetID])
		     */
		    $QuestionAllTargetIDArr = array();
		    foreach((array)$QuestionIDArr as $thisQuestionIndex => $thisQuestionID)
		    {
		        $thisQuestionTargetIDArr = $QuestionTargetIDArr[$thisQuestionID];
		        $thisQuestionTargetIDArr = array_unique(array_values_recursive((array)$thisQuestionTargetIDArr));
		        foreach((array)$thisQuestionTargetIDArr as $thisQuestionTargetID)
		        {
		            $thisQuestionTargetID = $thisQuestionTargetID - 1;
		            if(isset($QuestionAllTargetIDArr[$thisQuestionTargetID])) {
		                $thisQuestionTargetIDArr = array_merge((array)$thisQuestionTargetIDArr, (array)$QuestionAllTargetIDArr[$thisQuestionTargetID]);
		                $thisQuestionTargetIDArr = array_unique(array_values($thisQuestionTargetIDArr));
		            }
		        }
		        asort($thisQuestionTargetIDArr);
		        $QuestionAllTargetIDArr[$thisQuestionID] = $thisQuestionTargetIDArr;
		    }
		    
		    /*
		     * Build Question Relation
		     * 1. Get related Questions directly     ($QuestionTargetIDArr[$thisQuestionID][$thisOptionIndex])
		     * 2. Get related Questions indirectly   ($QuestionAllTargetIDArr[$thisQuestionTargetID])
		     */
		    foreach((array)$QuestionIDArr as $thisQuestionIndex => $thisQuestionID)
		    {
		        if($thisQuestionIndex == 0)
		        {
		            $thisQuestionTargetIDArr = $QuestionTargetIDArr[$thisQuestionID];
		            $QuestionRelation[$thisQuestionID] = $thisQuestionTargetIDArr;
		        }
		        else
		        {
		            // loop Question Options
		            $thisQuestionTargetIDArr = $QuestionTargetIDArr[$thisQuestionID];
		            foreach((array)$thisQuestionTargetIDArr as $thisOptionIndex => $thisQuestionOptionTargetIDArr)
		            {
		                $thisQuestionTargetID = '';
		                foreach((array)$thisQuestionOptionTargetIDArr as $thisQuestionOptionTargetID)
		                {
// 		                    foreach((array)$OptionTargetArr as $thisOptionTargetIDArr)
// 		                    {
// 		                        $thisTargetID = $thisOptionTargetIDArr[$thisQuestionOptionTargetID];
// 		                        if($thisTargetID != '' && $thisTargetID != 'end' && $thisQuestionOptionTargetID != ($thisOptionTargetIDArr[($thisQuestionOptionTargetID - 1)] + 1)) {
// 		                            $thisQuestionTargetID = $thisQuestionOptionTargetID;
// 	                            }
// 		                    }
		                    $thisQuestionTargetID = $thisQuestionOptionTargetID;
		                }
		                
		                // Get Questions from Option Target Settings
		                if($thisQuestionTargetID)
		                {
		                    $thisQuestionTargetID = $thisQuestionTargetID - 1;
		                    if(isset($QuestionAllTargetIDArr[$thisQuestionTargetID]))
		                    {
		                        $thisQuestionOptionTargetIDArr = array_merge((array)$thisQuestionOptionTargetIDArr, (array)$QuestionAllTargetIDArr[$thisQuestionTargetID]);
		                        $thisQuestionOptionTargetIDArr = array_unique(array_values($thisQuestionOptionTargetIDArr));
		                        asort($thisQuestionOptionTargetIDArr);
	                        }
	                    }
	                    $QuestionRelation[$thisQuestionID][$thisOptionIndex] = $thisQuestionOptionTargetIDArr;
	                }
	            }
	        }
	        ksort($QuestionRelation);
	        
	        return $QuestionRelation;
	    }
	    
		function parseAnswerStr ($aStr)
		{
			if ($aStr == "") return array();

			$aSeparator = "#ANS#";
			$answers = explode($aSeparator,$aStr);
			for ($i=1; $i<sizeof($answers); $i++)
			{
				$qType = $this->question_array[$i-1][0];
				if ($qType == 2 || $qType == 1 || $qType == 12 || $qType == 9 || $qType == 13 || $qType == 14) // single choice answer
				{
					$options = $this->question_array[$i-1][2];
					$ansPart = $answers[$i]=='' || !isset($options[$answers[$i]])? '' : $options[$answers[$i]];
				}
				else if ($qType == 3 || $qType == 15) // multiple choices answer
				{
					$options = $this->question_array[$i-1][2];
					$selected = explode(",",$answers[$i]);
					$ansPart = "";
					$sep = "";
					for ($j=0; $j<sizeof($selected); $j++)
					{
						$ansPart .= $sep . $options[$selected[$j]];
						$sep = "; ";
					}
				}
				else if($qType == 17) // multiple choices with input quantity
                {
                  	  $options = $this->question_array[$i-1][2];
                      $selected = explode(",",$answers[$i]);
                      $ansPart = "";
					  $sep = "";
                      for ($j=0; $j<sizeof($selected); $j++)
                      {
                      	   $opt_parts = explode('_',$selected[$j]);
                           $ansPart .= $sep.$options[$opt_parts[0]].' x '.$opt_parts[1];
						   $sep = "; ";
                      }
                }
				else if ($qType == 6 || $qType == 16) // not applicable
				{
					$ansPart = "";
				}
				else // text answer
				{
					$ansPart = $answers[$i];
				}
				$result[] = $ansPart;
			}

			return $result;
		}
		
		function returnAnswerString($ClassName = "")
		{
			if($ClassName == "NULL")
			{
				$conds = " AND (b.ClassName is NULL or b.ClassName = '' or b.ClassName = '0')";
			}
			else if ($ClassName != "")
			{
				if(strpos($ClassName,",")===false)
				{
					$conds = " AND b.ClassName = '$ClassName'";
				}
				else
				{
					$conds = " AND (";
					$classes = explode(",",$ClassName);
					$sep = "";
					foreach($classes as $k=>$d)
					{
						$conds .= $sep . " b.ClassName = '$d' ";
						$sep = " OR ";
					}
					$conds .= ")";
				}
			}
			/*
			 $namefield = getNameFieldWithClassNumberByLang("b.");
			 $namefield2 = getNameFieldWithClassNumberByLang("c.");
			 $sql = "SELECT a.StudentID,
			 IF(b.UserID IS NULL,CONCAT('<I>',a.StudentName,'</I>'),$namefield),
			 a.RecordType,a.RecordStatus,a.Answer,
			 IF(c.UserID IS NULL,CONCAT('<I>',a.SignerName,'</I>'),$namefield2),
			 a.DateModified
			 FROM INTRANET_NOTICE_REPLY as a
			 LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
			 LEFT OUTER JOIN INTRANET_USER as c ON a.SignerID = c.UserID
			 WHERE NoticeID = ".$this->NoticeID ." $conds
			 ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
			 
			 return $this->returnArray($sql,7);
			 */
			
			$namefield = getNameFieldByLang2("b.");
			$namefield2 = getNameFieldByLang2("c.");
			$classField = getClassNumberField("b.");
			$classField2 = getClassNumberField("c.");
			
			$sql = "SELECT
                        a.StudentID,
                        IF(b.UserID IS NULL, CONCAT('<I>', a.StudentName, '</I>'), $namefield),
                        b.ClassNumber,
                        a.RecordType,
                        a.RecordStatus,
                        IF(a.RecordStatus = 0, NULL, a.Answer) as Answer,
                        IF(c.UserID IS NULL, CONCAT('<I>', a.SignerName, '</I>'), $namefield2),
                        a.DateModified,
                        b.ClassName,
                        b.WebSAMSRegNo,
                        a.SignerAuthCode,
                        b.UserLogin,
                        b.RecordStatus as StudentStatus, 
                        a.NoticePaymentMethod 
                    FROM
                        INTRANET_NOTICE_REPLY as a
                        LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                        LEFT OUTER JOIN INTRANET_USER as c ON a.SignerID = c.UserID
                    WHERE
                        NoticeID = '".$this->NoticeID ."' $conds
                    ORDER BY
                        b.ClassName, b.ClassNumber, b.EnglishName";
			return $this->returnArray($sql);
			// ($sid,$name,$classNo,$type,$status,$aStr,$signer,$last) = $answers[$i];
		}
		
		function returnAnswerStringGroupByClass($withoutClass = false)
		{
			// $namefield = getNameFieldWithClassNumberByLang("b.");
			$namefield = getNameFieldByLang("b.");
			$namefield2 = getNameFieldWithClassNumberByLang("c.");
			if(!$withoutClass){
				$classCond = "AND (b.ClassName!='' OR b.ClassNumber!='')";
			}

			$sql = "SELECT
                        a.StudentID,
                        IF(b.UserID IS NULL, CONCAT('<I>', a.StudentName, '</I>'), $namefield),
                        a.RecordType,
                        a.RecordStatus,
                        IF(a.RecordStatus = 0, NULL, a.Answer) as Answer,
                        IF(c.UserID IS NULL, CONCAT('<I>', a.SignerName, '</I>'), $namefield2),
                        a.DateModified,
                        b.ClassName,
                        b.ClassNumber,
                        b.WebSAMSRegNo,
                        a.SignerAuthCode,
                        b.UserLogin,
                        b.RecordStatus as StudentStatus,
                        a.NoticePaymentMethod 
			        FROM
                        INTRANET_NOTICE_REPLY as a
                        LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                        LEFT OUTER JOIN INTRANET_USER as c ON a.SignerID = c.UserID
                    WHERE
                        NoticeID = '".$this->NoticeID ."' $classCond
                    ORDER BY
                        b.ClassName, b.ClassNumber, b.EnglishName";
			$result = $this->returnArray($sql);
			return $result;
			
			/*
			 $prevClass = "";
			 for ($i=0; $i<sizeof($result); $i++)
			 {
			 $current = $result[$i][7];
			 if ($current != $prevClass)
			 {
			 if ($i!=0)
			 {
			 $class_result[] = array($prevClass,$temp_result);
			 unset($temp_result);
			 }
			 $prevClass = $current;
			 }
			 $temp_result[] = $result[$i];
			 }
			 $class_result[] = $temp_result;
			 */
			return $class_result;
		}
		
		function returnQuestionString($qAry)
		{
			$qSeparator = "#QUE#";
			$pSeparator = "||";
			$oSeparator = "#OPT#";
			$msSeparator = "#MSUB#";
			$rSeparator = "#TAR#";
			
			// loop Question
			$qStr = "";
			for ($i=0; $i<sizeof($qAry); $i++)
			{
				// Question Info
				$question = $qAry[$i];
				$questionType = $question[0];
				$questionTitle = $question[1];
				$questionOption = $question[2];
				$questionMSUB = $question[3];
				$questionRelated = $question[4];
				$answerNum = $i+1;
				
				// T/F Question
				$TFQ = $questionType==1;
				$QTargetCount = count((array)$questionRelated);
				
				// MC Question
				$MCQ = $questionType==2 || $questionType==3;
				$MCOptCount = count((array)$questionOption);
				
				// Build return String
				// Question Settings
				$qStr .= $qSeparator;
				$qStr .= $questionType;
				// MC Question
				if($MCQ && $MCOptCount > 1){
					$qStr .= ",";
					$qStr .= $MCOptCount;
				}
				
				// Question Title
				$qStr .= $pSeparator;
				$qStr .= $questionTitle;
				
				// Question Options
				$qStr .= $pSeparator;
				
				// T/F & MC Question
				if(($TFQ || $MCQ) && $QTargetCount > 0){
					for($TFC=0; $TFC<$QTargetCount; $TFC++)
					{
						$qStr .= $oSeparator;
						$qStr .= $questionOption[$TFC];
						$qStr .= $questionRelated[$TFC]==""? "" : $rSeparator.$questionRelated[$TFC]."#";
					}
				}
				// Text Question
				else if(!($TFQ || $MCQ) && $QTargetCount > 0){
					$qStr .= $oSeparator;
					$qStr .= $questionRelated[0]==""? "" : $rSeparator.$questionRelated[0]."#";
				}
				
				// Must Submit Settings
				$qStr .= $msSeparator;
				$qStr .= $questionMSUB;
			}
			
			$qStr = stripslashes($qStr);
			$qStr = str_replace('"','&quot;',$qStr);
			$qStr = str_replace(">", "&gt;", $qStr);
			$qStr = str_replace("<", "&lt;", $qStr);
			return $qStr;
		}
		
		function returnTableViewAll()
		{
			$answers = $this->returnAnswerString();
			
			# Parse answers
			for ($i=0; $i<sizeof($answers); $i++)
			{
				//list ($sid,$name,$type,$status,$aStr,$signer,$last) = $answers[$i];
				list ($sid,$name,$class_no,$type,$status,$aStr,$signer,$last,$class_name,$websamsRegNo,$signerAuthCode,$studentUserLogin,$student_status,$notice_payment_method) = $answers[$i];
				$parsed = $this->parseAnswerStr($aStr);
				$result[] = array($sid,$name,$class_no,$type,$status,$parsed,$signer,$last,$class_name,$websamsRegNo,$signerAuthCode,$studentUserLogin,$student_status,$notice_payment_method);
				
				/*list ($sid,$name,$classNo,$type,$status,$aStr,$signer,$last) = $answers[$i];
				 $parsed = $this->parseAnswerStr($aStr);
				 $result[] = array($sid,$name,$classNo,$type,$status,$parsed,$signer,$last);*/
			}
			return $result;
		}
		
		function returnTableViewByClass($ClassName)
		{
			#$questions = $this->splitQuestion($this->Question);
			$answers = $this->returnAnswerString($ClassName);
			
			# Parse answers
			for ($i=0; $i<sizeof($answers); $i++)
			{
				list ($sid,$name,$classNo,$type,$status,$aStr,$signer,$last,$class_name, $regno, $signerAuthCode, $studentUserLogin, $student_status, $notice_payment_method) = $answers[$i];
				$parsed = $this->parseAnswerStr($aStr);
				$result[] = array($sid,$name,$classNo,$type,$status,$parsed,$signer,$last,$class_name, $regno, $signerAuthCode, $studentUserLogin, $student_status, $notice_payment_method);
			}
			return $result;
		}
		
		function returnTableViewGroupByClass($withoutClass = false)
		{
			$result = $this->returnAnswerStringGroupByClass($withoutClass);
			
			$prevClass = "";
			for ($j=0; $j<sizeof($result); $j++)
			{
				$current = $result[$j][7];
				if ($current != $prevClass)
				{
					if ($j!=0)
					{
						$class_result[] = array($prevClass,$temp_result);
						unset($temp_result);
					}
					$prevClass = $current;
				}
				list ($sid,$name,$type,$status,$aStr,$signer,$last, $classTmp, $classno, $regno,$signerAuthCode, $studentUserLogin, $student_status, $notice_payment_method) = $result[$j];
				$parsed = $this->parseAnswerStr($aStr);
				$temp_result[] = array($sid,$name,$type,$status,$parsed,$signer,$last, $classno, $regno,$signerAuthCode, $studentUserLogin, $student_status, $notice_payment_method);
			}
			$class_result[] = array($prevClass,$temp_result);
			return $class_result;
		}
		
		function returnResultOfClass($ClassName)
		{
			if($ClassName=="NULL")
			{
				$class_str = "(b.ClassName is NULL or b.ClassName='' or b.ClassName='0') ";
			}
			else
			{
				// [2014-1217-1446-17207] prevent sql error when teacher teach more than 1 classes
				//        		$class_str = "b.ClassName = '$ClassName' ";
				$class_str = " b.ClassName IN ('".str_replace(",", "','", addslashes($ClassName))."') ";
			}
			$sql = "SELECT a.Answer FROM INTRANET_NOTICE_REPLY as a
			            LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                    WHERE $class_str AND a.RecordStatus = 2 AND
                        a.NoticeID = '".$this->NoticeID."'";
			return $this->returnVector($sql);
		}
		
		function returnAllResult()
		{
			// [2014-1217-1446-17207] Remove a.Answer != ''
			$sql = "SELECT a.Answer FROM INTRANET_NOTICE_REPLY as a
                     WHERE a.RecordStatus = 2 AND a.NoticeID = '".$this->NoticeID."'";
			return $this->returnVector($sql);
		}
		
		# added by Kelvin Ho 2008-11-12 for ucc
		/*function getModuleName($Module)
		 {
		 global $i_Notice_ModuleID;
		 
		 return $i_Notice_ModuleID[$ModuleID];
		 }*/
		
		# added by Yat Woon 2008-12-01
		function changeNoticeStatus($NoticeID, $Status)
		{
			$sql = "update INTRANET_NOTICE set RecordStatus='$Status', DateModified=now() where NoticeID = '$NoticeID'";
			$this->db_db_query($sql);
		}
		
		function deleteNotice($NoticeID='')
		{
			global $PATH_WRT_ROOT, $bug_tracing, $file_path;
			
			if($NoticeID == "") {
				$NoticeID = $this->NoticeID;
			}
			$NoticeID = IntegerSafe($NoticeID);
			$NoticeAttachment = OsCommandSafe($this->Attachment);
			
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$lf = new libfilesystem();
			
			$sql = "DELETE FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$NoticeID'";
			$this->db_db_query($sql);
			
			# Grab attachment
			// $path = "$file_path/file/notice/".$this->Attachment;
			$path = "$file_path/file/notice/".$NoticeAttachment;
			if (trim($this->Attachment) != "" && is_dir($path))
			{
				if($bug_tracing['notice_attachment_log']){
					$temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
					$temp_time = date("Y-m-d H:i:s");
					$temp_user = $UserID;
					$temp_page = 'notice_remove_update.php';
					$temp_action="remove path:".$path;
					$temp_content = get_file_content($temp_log_filepath);
					$temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
					$temp_content .= $temp_logentry;
					write_file_content($temp_content, $temp_log_filepath);
					$temp_cmd = "mv $path ".$path."_bak";
					exec($temp_cmd);
				}
				else{
					$lf->lfs_remove($path);
				}
			}
			
			$sql = "DELETE FROM INTRANET_NOTICE WHERE NoticeID = '$NoticeID'";
			$this->db_db_query($sql);
		}
		
		# 2010-01-14 [YatWoon]
		function buildReplySlipTemplates($module="")
		{
			$cond = "";
			if(strtoupper($module) == "PAYMENT")
			{
				$cond .= " AND Module = 'Payment' ";
			}
			else
			{
				$cond .= " AND Module IS NULL ";
			}
			$sql = "select Title, Question from INTRANET_NOTICE where RecordStatus=3 and Question>'' $cond";
			$result = $this->returnArray($sql);
			
			for($i=0;$i<sizeof($result);$i++)
			{
				$q = $result[$i]['Question'];
				$q = str_replace("\r\n"," ",$q);
				$q = str_replace(array("\t","\n","\r")," ",$q);
				$q = str_replace(array("<",">"),array("&lt;","&gt;"),$q);
				$q = str_replace('"','\"',$q);
				$x .= "form_templates[". $i ."] = new Array(\"". $result[$i]['Title'] ."\", \"". $q  ."\");\n";
			}
			
			return $x;
		}
		
		function buildReplySlipTemplatesInArray($module="")
		{
			$cond = "";
			if(strtoupper($module) == "PAYMENT")
			{
				$cond .= " AND Module = 'Payment' ";
			}
			else
			{
				$cond .= " AND Module IS NULL ";
			}
			$sql = "select Question, Title from INTRANET_NOTICE where RecordStatus=3 and Question>'' $cond";
			
			return $this->returnArray($sql);
		}
		
		function returnReplySlipContent($displayType, $qAry, $aAry="", $AllFieldsReq, $DisplayQNum, $isFromApp=false)
		{
			global $linterface, $Lang, $button_continue, $button_delete;
			
			$isAnyMustSubmit = false;
			$isEdit = $displayType=="edit";
			$isPreview = $displayType=="preview";
			$questionCount = count($qAry);
			
			// Question Topic List
			// loop updated Questions
			$AllQuestionTopicList = array();
			for($i=0; $i<$questionCount; $i++){
				$AllQuestionTopicList[] = array($i, $Lang['eNotice']['SkipToQuestion'].($i+1));
			}
			
			// Build Content
			$x = "";
			$x .= "<div id='replySlipDiv' style='width: 100%; overflow-x: auto; overflow-y: hidden;'><table width='100%' border='0' cellpadding='0' cellspacing='5' id='ContentTable'>";
			
			// loop Question
			for($i=0; $i<$questionCount; $i++)
			{
				// Question Info
				$question = $qAry[$i];
				$questionType = $question[0];
				$questionTitle = $question[1];
				$questionOption = $question[2];
				$questionMSUB = $question[3];
				$questionRelated = $question[4];
				$answerNum = $i+1;
				
				// Answer Info
				$answer = $aAry[$questionCount+$i];
				
				// Any Must Submit T/F OR MC Question before
				if(!$isAnyMustSubmit){
					$isAnyMustSubmit = ($questionType==1 || $questionType==2) && ($questionMSUB==1 || $AllFieldsReq);
				}
				
				// Any Must Submit T/F Question before
				$isBeforeTFQuestion = true;
				//if(!$isBeforeTFQuestion){
				//    if(isset($qAry[$i+1]))
				//    {
				//	      $nextQuestion = $qAry[$i+1];
				//		  $nextQType = $nextQuestion[0];
				//		  $nextQMSUB = $nextQuestion[3];
				//		  $isBeforeTFQuestion = ($i+1)<($questionCount-1) && ($nextQType==1 || $nextQType==2) && ($AllFieldsReq || $nextQMSUB);
				//	  }
				//}
				
				// Skip Settings in drop down list
				$belowQList = array();
				if($i<$questionCount-1){
					// available Report Titles
					$belowQList = array_slice($AllQuestionTopicList, $i+1);
				}
				// default Settings
				array_push($belowQList, array("end", $Lang['eNotice']['SkipToAllQuestion']));
				
				// Display Content
				$questionStyle = $isEdit? "style='vertical-align: top;'" : "";
				$questionDisplay = "";
				$questionDisplay .= ($isEdit || $isPreview || $DisplayQNum)? "<span $questionStyle>".$answerNum.". </span>" : "";
				$questionDisplay .= ($AllFieldsReq || $questionMSUB) && $questionType!=6? "<span class='tabletextrequire' $questionStyle>*</span>" : "";
				//$questionDisplay .= $isEdit? "<textarea name='QTitle".$answerNum."' cols='50' rows='3'>".preg_replace("(\r\n|\n)", "<br>", stripslashes($questionTitle))."</textarea>" : $questionTitle;
				$questionDisplay .= $isEdit? "<textarea name='QTitle".$answerNum."' cols='50' rows='3'>".str_replace("<br>", "\n", stripslashes($questionTitle))."</textarea>" : $questionTitle;
				
				// Question Type
				$x .= "	<input id='typeOf".$answerNum."' type='hidden' value='".$question[0][0]."' sum='".$question[0][2]."'/>";
				
				// Question Message
				// View
                $QMsg = "";
				if(!$isEdit)
				{
					$QMsg = "<td id='msg_$answerNum' class='tabletext' style='display:none' align='right'>";
					$QMsg .= "<table border='0' cellpadding='3' cellspacing='0' class='systemmsg' style='border-color:#000000; color:#000000;'>
									<tr><td nowrap='nowrap'>&nbsp; ".$Lang['eNotice']['NoNeedToAnswerThisQuestion']." &nbsp;</td></tr>
								</table>\n";
					$QMsg .= "</td>";
					if($isFromApp) {
						$QMsg = "";
						$questionDisplay .= "<span id='msg_$answerNum' style='display:none;'><br><span id='rcorners_green' style='color:white;text-shadow: 0px 0px;font-size:16px;vertical-align:text-top;'>".$Lang['eNotice']['NoNeedToAnswerThisQuestion']."</span></span>";
					}
				}
				// Edit Question (escape non question)
				else if($isEdit && $questionType!=6)
				{
					$QMsg = "<td class='tabletext' align='right' valign='top'>
								<span>".$Lang['eNotice']['QuestionNeedToReply']."</span>
								<input type='checkbox' value='1' name='MS".$answerNum."' id='MS".$answerNum."' ".($questionMSUB || $AllFieldsReq? "checked " : "")." ".($AllFieldsReq? "disabled" : "")." onClick='updateReplySlip();'>
							</td>";
				}
				else
				{
					$QMsg = "<td class='tabletext' align='right' valign='top'>&nbsp;</td>";
				}
				
				// Question Title
				$x .= "	<tr id='row".$answerNum."' class='sub_row'><td align='center' bgcolor='#EFEFEF'>
								<table border='0' width='95%' align='center' bgcolor='#EFEFEF'>
								<tr>
									<td bgcolor='#EFEFEF' class='tabletext'>".$questionDisplay."</td>
									".$QMsg."
								</tr>";
				
				switch($questionType)
                {
					// T/F Question
					case 1:
						// Check if line break is needed
						$thisQStr = implode("", (array)$questionOption);
						$needLineBreak = $isEdit || $isPreview || utf8_strlen($thisQStr, 2)>=60;
						
						// Options
						$x .= " <tr><td>";
						for($TFC=0; $TFC<2; $TFC++)
						{
							$TFChecked = !$isEdit && isset($answer) && trim($answer)!="null" && $answer==$TFC? " checked " : "";
							$TFClickAction = !$isEdit? "onClick='updateAllAvailableOption();'" : "";
							$x .= "		<input type='radio' value='".$TFC."' name='F".$answerNum."' id='FF".$TFC.$answerNum."' ".$TFChecked." ".$TFClickAction.">";
							
							// Display Input Field OR Label
							if($isEdit) {
								$x .= "<input type='text' value=\"".stripslashes($questionOption[$TFC])."\" name='FD".$answerNum."_".$TFC."' size='25' class='tabletext'>";
                            } else {
                                $x .= "<label for='FF".$TFC.$answerNum."'>".$questionOption[$TFC]."</label>";
                            }

                            // for Apps display
                            if($isFromApp) {
                                // if(($AllFieldsReq || $questionMSUB) && $TFC==1){
                                // $settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."_$TFC' style='border-style:hidden' disabled", $questionRelated[$TFC], "", 1);

                                //$x .= "<br>";
                                // $x .= $Lang['eNotice']['ActionAfterSelect'].":&nbsp;".$settingDisplay;
                                // $x .= $TFC==0? "&nbsp;<br>" : "&nbsp;";
                                if($TFC==1) {
                                    $x .= "&nbsp;";
                                }
                            }
                            else {
                                $x .= $TFC==0 && $needLineBreak? "<br>" : "&nbsp;";
                            }
						}
						
						// Skip Question Settings
						// 1. Need to fill all	OR	Current Question must be submitted
						// 2. Not the last Question
						if($isFromApp && ($AllFieldsReq || $questionMSUB) && $i<$questionCount-1)
						{
							// for Apps display
							// do nothing
						}
						else if(($AllFieldsReq || $questionMSUB) && $i<$questionCount-1)
						{
							// Skip Settings Display
							$x .= " </td>";
							$x .= " <td align='right'>";
							for($TFC=0; $TFC<2; $TFC++)
							{
								// Display drop down list OR Target Question
								if($isEdit) {
									$settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."_$TFC'", $questionRelated[$TFC], "", 1);
                                } else if($isPreview) {
                                    $settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."_$TFC' style='border-style:hidden' disabled", $questionRelated[$TFC], "", 1);
                                }

                                //$x .= $Lang['eNotice']['ActionAfterSelect'].":&nbsp;".$settingDisplay;
                                $x .= $settingDisplay;
                                $x .= $TFC==0 && $needLineBreak? "&nbsp;<br>" : "&nbsp;";
							}
						}
						else if ($isAnyMustSubmit && $isBeforeTFQuestion && $i<$questionCount-1)
						{
							// Skip Settings Display
							if($isFromApp) {
								// do nothing
							}
							else {
								$x .= " </td>
										<td align='right' valign='bottom'>&nbsp;";
							}
							
							// Display drop down list OR Target Question
							if($isEdit) {
								$settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."'", $questionRelated[0], "", 1);
                            } else if($isPreview) {
                                $settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."' style='border-style:hidden' disabled", $questionRelated[0], "", 1);
                            }

                            //$x .= $Lang['eNotice']['ActionAfterAnswer'].":&nbsp;".$settingDisplay;
                            if($isFromApp) {
                                // $x .= "<br>";
                            }
                            else {
                                $x .= $settingDisplay."&nbsp;";
                            }
						}
						$x .= " </td>";
						$x .= " </tr>";
						break;
						
                    // MC Question
					case 2:
						// Check if line break is needed
						$thisQStr = implode("", (array)$questionOption);
						$needLineBreak = $isEdit || $isPreview || utf8_strlen($thisQStr, 2)>=60;
						//$needLineBreak = true;
						
						// loop Options
						$x .= " <tr><td>";
						for($j=0; $j<count($questionOption); $j++)
						{
							$TFChecked = !$isEdit && isset($answer) && trim($answer)!="null" && $answer==$j? " checked" : "";
							$TFClickAction = !$isEdit? "onClick='updateAllAvailableOption();'" : "";
							$x .= "		<input type='radio' value='".$j."' name='F".$answerNum."' id='FF0".$answerNum."_".$j."' ".$TFChecked." ".$TFClickAction.">";
							
							// Display Input Field OR Label
							if($isEdit) {
								$x .= "<input type='text' value=\"".stripslashes($questionOption[$j])."\" name='FD".$answerNum."_".$j."' id='FF0".$answerNum."' size='25' class='tabletext'>";
                            } else {
                                $x .= "<label for='FF0".$answerNum."_".$j."'>".$questionOption[$j]."</label>";
                            }

                            // Next Line OR Space
                            if(!$isFromApp)
                            {
                                if($needLineBreak) {
                                    $x .= "	<br>";
                                } else {
                                    $x .= " &nbsp; ";
                                }
                                if(!$isEdit && !$needLineBreak && $j==count($questionOption)-1) {
                                    $x .= "	<br>";
                                }
                            }
                            else if($isFromApp && $j==count($questionOption)-1)
                            {
                                $x .= "	<br>";
                            }
						}
						
						// Skip Question Settings
						// 1. Need to fill all	OR	Current Question must be submitted
						// 2. Not the last Question
						if($isFromApp && ($AllFieldsReq || $questionMSUB) && $i<$questionCount-1)
						{
							// for Apps display
							// do nothing
						}
						else if(($AllFieldsReq || $questionMSUB) && $i<$questionCount-1)
						{
							// Skip Settings Display
							$x .= " </td>";
							$x .= " <td align='right'>";
							for($TFC=0; $TFC<count($questionOption); $TFC++)
							{
								// Display drop down list OR Target Question
								if($isEdit) {
									$settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."_$TFC'", $questionRelated[$TFC], "", 1);
                                } else if($isPreview) {
                                    $settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."_$TFC' style='border-style:hidden' disabled", $questionRelated[$TFC], "", 1);
                                }
                                //$x .= $Lang['eNotice']['ActionAfterSelect'].":&nbsp;".$settingDisplay;
                                $x .= $settingDisplay;
                                $x .= ($TFC<count($questionOption)-1) && $needLineBreak? "&nbsp;<br>" : "&nbsp;";
							}
						}
						else if ($isAnyMustSubmit && $isBeforeTFQuestion && $i<$questionCount-1 && count($questionOption)>0)
						{
							// Skip Settings Display
							if($isFromApp){
								// do nothing
							}
							else{
								$x .= " </td>
										<td align='right' valign='bottom'>&nbsp;";
							}
							
							// Display drop down list OR Target Question
							if($isEdit) {
								$settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."'", $questionRelated[0], "", 1);
                            } else if($isPreview) {
                                $settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."' style='border-style:hidden' disabled", $questionRelated[0], "", 1);
                            }
                            //$x .= $Lang['eNotice']['ActionAfterAnswer'].":&nbsp;".$settingDisplay;
                            if($isFromApp)
                            {
                                // $x .= "<br>";
                            }
                            else
                            {
                                $x .= $settingDisplay."&nbsp;";
                            }
						}
						$x .= " </td>";
						$x .= "</tr>";
						break;
						
                    // MC Question (Multiple)
					case 3:
						// Check if line break is needed
						$thisQStr = implode("", (array)$questionOption);
						$needLineBreak = $isEdit || $isPreview || utf8_strlen($thisQStr, 2)>=60;
						
						// break answer if muliple
						$MultiAnswer = array();
						if(!$isEdit && isset($answer) && trim($answer)!="null"){
							$MultiAnswer = explode(",", $answer);
						}
						
						// loop Options
						$x .= " <tr><td>";
						for($j=0; $j<count($questionOption); $j++)
						{
							$TFChecked = !empty($MultiAnswer) && in_array($j, $MultiAnswer)? " checked" : "";
							$x .= "		<input type='checkbox' name='F".$answerNum."' value='".$j."' id='FF0".$answerNum."_".$j."' ".$TFChecked.">";
							
							// Display Input Field OR Label
							if($isEdit) {
								$x .= "<input type='text' value=\"".stripslashes($questionOption[$j])."\" name='FD".$answerNum."_".$j."' size='25' class='tabletext'>";
                            } else {
                                $x .= "<label for='FF0".$answerNum."_".$j."'>".$questionOption[$j]."</label>";
                            }

                            // Next Line OR Space
                            if(!$isFromApp)
                            {
                                if($needLineBreak) {
                                    $x .= "	<br>";
                                } else {
                                    $x .= " &nbsp; ";
                                }
                                if(!$isEdit && !$needLineBreak && $j==count($questionOption)-1) {
                                    $x .= "	<br>";
                                }
                            }
                            else if($isFromApp && $j==count($questionOption)-1)
                            {
                                $x .= "	<br>";
                            }
						}
						
						// Skip Question Settings
						if ($isAnyMustSubmit && $isBeforeTFQuestion && $i<$questionCount-1 && count($questionOption)>0)
						{
							// Skip Settings Display
							if($isFromApp){
								// do nothing
							}
							else{
								$x .= " </td>
										<td align='right' valign='bottom'>&nbsp;";
							}
							
							// Display drop down list OR Target Question
							if($isEdit) {
								$settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."'", $questionRelated[0], "", 1);
                            } else if($isPreview) {
                                $settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."' style='border-style:hidden' disabled", $questionRelated[0], "", 1);
                            }
                            //$x .= $Lang['eNotice']['ActionAfterAnswer'].":&nbsp;".$settingDisplay;
                            if($isFromApp){
                                //$x .= "<br>";
                            }
                            else{
                                $x .= $settingDisplay."&nbsp;";
                            }
						}
						
						$x .= " </td>";
						$x .= " </tr>";
						break;
						
                    // Short Text
					case 4:
						$textContent = !$isEdit && isset($answer) && trim($answer)!=""? $answer : "";
						$x .= " 	<tr><td>
											<input type='text' name='F".$answerNum."' value='".$textContent."' class='tabletext'>";
						
						// Skip Question Settings
						if ($isAnyMustSubmit && $isBeforeTFQuestion && $i<$questionCount-1)
						{
							// Skip Settings Display
							if($isFromApp){
								// do nothing
							}
							else{
								$x .= " </td>
										<td align='right' valign='bottom'>&nbsp;";
							}
							
							// Display drop down list OR Target Question
							if($isEdit) {
								$settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."'", $questionRelated[0], "", 1);
                            } else if($isPreview) {
                                $settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."' style='border-style:hidden' disabled", $questionRelated[0], "", 1);
                            }
                            //$x .= $Lang['eNotice']['ActionAfterAnswer'].":&nbsp;".$settingDisplay;
                            if($isFromApp){
                                //$x .= "<br>";
                            }
                            else{
                                $x .= $settingDisplay."&nbsp;";
                            }
						}
						
						if($isFromApp){
							$x .= "&nbsp;";
						}
						
						$x .= "	</td>";
						$x .= "	</tr>";
						break;
						
                    // Long Text
					case 5:
						$textContent = !$isEdit && isset($answer) && trim($answer)!=""? str_replace("<br>", "\n", $answer) : "";
						$x .= " 	<tr><td>
											<textarea name='F".$answerNum."' cols='50' rows='3'>".$textContent."</textarea>";
						
						// Skip Question Settings
						if ($isAnyMustSubmit && $isBeforeTFQuestion && $i<$questionCount-1)
						{
							// Skip Settings Display
							if($isFromApp){
								// do nothing
							}
							else{
								$x .= " </td>
										<td align='right' valign='bottom'>&nbsp;";
							}
							
							// Display drop down list OR Target Question
							if($isEdit) {
								$settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."'", $questionRelated[0], "", 1);
                            } else if($isPreview) {
                                $settingDisplay = getSelectByArray($belowQList, "name='FI".$answerNum."' style='border-style:hidden' disabled", $questionRelated[0], "", 1);
                            }
                            //$x .= $Lang['eNotice']['ActionAfterAnswer'].":&nbsp;".$settingDisplay;
                            if($isFromApp){
                                //$x .= "<br>";
                            }
                            else{
                                $x .= $settingDisplay."&nbsp;";
                            }
						}
						
						if($isFromApp){
							$x .= "&nbsp;";
						}
						
						$x .= "	</td>";
						$x .= "	</tr>";
						break;
						
						// Not Applicable
					case 6:
					default:
						break;
				}
				$x .= "	</table></td>";
				
				// Drag / Delete
				if($isEdit){
					$x .= "	<td class='Dragable' bgcolor='#EFEFEF' width=48px' valign='top'>
								<div class='table_row_tool' align='center'>&nbsp;".
								($questionCount > 1? $linterface->GET_LNK_MOVE("#", $Lang['eNotice']['MoveQuestion']) : "").
								$linterface->GET_LNK_DELETE("#", $button_delete, "document.ansForm.DisplayQOrder.value=".$i."; updateReplySlip('remove'); return false;")."
								</div>
							</td>
							</tr>";
				}
			}
			$x .= "	</table></div>";
			
			return $x;
		}
		
		function getPaymentCategory()
		{
			$sql = "SELECT * FROM PAYMENT_PAYMENT_CATEGORY ORDER BY DisplayOrder";
			return $result = $this->returnArray($sql);
			
		}
		
		function returnPaymentItemAndCategoryID($qStr)
		{
			$qSeparator = "#QUE#";		## question
			$pSeparator = "||";
			$oSeparator = "#OPT#";		## answer
			$catSeparator = "#CATEGORYID#";		## payment category
			$amtSeparator = "#AMT#";		## amount for the item
			$nonPayItemSeparator = "#NonPaymentItem#";		## non payment item
			
			$questions = explode($qSeparator,$qStr);
			for ($i=1; $i<sizeof($questions); $i++)
			{
				$que = $questions[$i];
				$parts = explode($pSeparator,$que);
				$temp = explode(",",$parts[0]);
				$type = $temp[0];
				$numOp = 0;
				$options = "";
				switch ($type)
				{
				    case 1:
				    case 12:
				        $numOp = $type==1? 2 : 1;
				        
						$opStr = $parts[2];
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						
						$payment_item = $parts[1];
						$arrPaymentDatails[$i]['PaymentItem'] = $payment_item;
						
						$category = explode($catSeparator,$parts[4]);
						array_shift($category);
						$arrPaymentDatails[$i]['PaymentCategory'] = $category[0];
						
						$nonPayItem = explode($nonPayItemSeparator,$parts[6]);
						array_shift($nonPayItem);
						$arrPaymentDatails[$i]['NonPayItem'] = $nonPayItem[0];
						
						break;
					case 2:
						$numOp = $temp[1];
						$opStr = $parts[2];
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						
						$payment_item = $parts[1];
						$arrPaymentDatails[$i]['PaymentItem'] = $payment_item;
						
						$category = explode($catSeparator,$parts[4]);
						array_shift($category);
						$arrPaymentDatails[$i]['PaymentCategory'] = $category[0];
						
						$nonPayItem = explode($nonPayItemSeparator,$parts[5]);
						array_shift($nonPayItem);
						$arrPaymentDatails[$i]['NonPayItem'] = $nonPayItem[0];
						break;
					case 3:
						$numOp = $temp[1];
						$opStr = $parts[2];
						
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						if(sizeof($options)>0)
						{
							for($j=0; $j<sizeof($options); $j++)
							{
								$arrPaymentDatails[$i]['PaymentItem'][] = $options[$j];
							}
						}
						
						$category = explode($catSeparator,$parts[4]);
						array_shift($category);
						if(sizeof($category)>0)
						{
							for($j=0; $j<sizeof($category); $j++)
							{
								$arrPaymentDatails[$i]['PaymentCategory'][] = $category[$j];
							}
						}
						break;
					case 4:
					case 5:
					case 6:
						$options = array();
						break;
					case 7:
					case 8:
					case 9:
						$numOp = $temp[1];
						$opStr = $parts[2];
						
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						if(sizeof($options)>0)
						{
							for($j=0; $j<sizeof($options); $j++)
							{
								$arrPaymentDatails[$i]['PaymentItem'][] = $options[$j];
							}
						}
						
						$category = explode($catSeparator,$parts[4]);
						array_shift($category);
						if(sizeof($category)>0)
						{
							for($j=0; $j<sizeof($category); $j++)
							{
								$arrPaymentDatails[$i]['PaymentCategory'][] = $category[$j];
							}
						}
						break;
				}
				$mainQ = $parts[1];
				//$result[] = array($type, $mainQ, $options);
			}
			//$this->question_array = $result;
			$result = $arrPaymentDatails;
			return $result;
		}
		
		function returnPaymentItemID($qStr)
		{
			$qSeparator = "#QUE#";		## question
			$pSeparator = "||";
			$oSeparator = "#OPT#";		## answer
			$catSeparator = "#CATEGORYID#";		## payment category
			$amtSeparator = "#AMT#";		## amount for the item
			$itemidSeparator = "#PAYMENTITEMID#";		## PaymentItem ID
			
			$questions = explode($qSeparator,$qStr);
			for ($i=1; $i<sizeof($questions); $i++)
			{
				$que = $questions[$i];
				$parts = explode($pSeparator,$que);
				$temp = explode(",",$parts[0]);
				$type = $temp[0];
				$numOp = 0;
				$options = "";
				switch ($type)
				{
				    case 1:
				    case 12:
						$arrPaymentItemID = $parts[3];
						$PaymentItemID = explode($itemidSeparator,$parts[3]);
						array_shift($PaymentItemID);
						
						if(sizeof($PaymentItemID)>0){
							for($j=0; $j<sizeof($PaymentItemID); $j++){
								$targetPaymentIDs[] = $PaymentItemID[$j];
							}
						}
						break;
					case 2:
						$arrPaymentItemID = $parts[3];
						$PaymentItemID = explode($itemidSeparator,$parts[3]);
						array_shift($PaymentItemID);
						
						if(sizeof($PaymentItemID)>0){
							for($j=0; $j<sizeof($PaymentItemID); $j++){
								$targetPaymentIDs[] = $PaymentItemID[$j];
							}
						}
						break;
					case 3:
						$arrPaymentItemID = $parts[3];
						$PaymentItemID = explode($itemidSeparator,$parts[3]);
						array_shift($PaymentItemID);
						
						if(sizeof($PaymentItemID)>0){
							for($j=0; $j<sizeof($PaymentItemID); $j++){
								$targetPaymentIDs[] = $PaymentItemID[$j];
							}
						}
						break;
						
					case 4:
					case 5:
					case 6:
						$options = array();
						break;
					case 7:
					case 8:
					case 9:
						$arrPaymentItemID = $parts[3];
						$PaymentItemID = explode($itemidSeparator,$parts[3]);
						array_shift($PaymentItemID);
						
						if(sizeof($PaymentItemID)>0){
							for($j=0; $j<sizeof($PaymentItemID); $j++){
								$targetPaymentIDs[] = $PaymentItemID[$j];
							}
						}
						break;
				}
			}
			//$result = $arrPaymentDatails;
			return $targetPaymentIDs;
		}
		
		function returnPaymentItemDetails($qStr)
		{
			$qSeparator = "#QUE#";		## question
			$pSeparator = "||";
			$oSeparator = "#OPT#";		## answer
			$catSeparator = "#CATEGORYID#";		## payment category
			$amtSeparator = "#AMT#";		## amount for the item
			$itemidSeparator = "#PAYMENTITEMID#";		## PaymentItem ID
			$nonPaymentItemSeparator = "#NonPaymentItem#";		## Non Payment Item
			
			$questions = explode($qSeparator,$qStr);
			for ($i=1; $i<sizeof($questions); $i++)
			{
				$que = $questions[$i];
				$parts = explode($pSeparator,$que);
				
				$temp = explode(",",$parts[0]);
				$type = $temp[0];
				$numOp = 0;
				$options = "";
				switch ($type)
				{
				    case 1:
				    case 12:
				        $numOp = $type==1? 2 : 1;
				        
						$opStr = $parts[2];
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						
						$payment_item = $parts[1];
						$arrPaymentDatails[$i]['PaymentItem'] = $payment_item;
						
						$arrID = explode($itemidSeparator,$parts[3]);
						array_shift($arrID);
						$arrPaymentDatails[$i]['PaymentItemID'] = $arrID[0];
						
						$category = explode($catSeparator,$parts[4]);
						array_shift($category);
						$arrPaymentDatails[$i]['PaymentCategory'] = $category[0];
						
						$amount = explode($amtSeparator,$parts[5]);
						array_shift($amount);
						$arrPaymentDatails[$i]['PaymentAmount'] = $amount[0];
						
						$nonPayItem = explode($nonPaymentItemSeparator,$parts[6]);
						array_shift($nonPayItem);
						$arrPaymentDatails[$i]['NonPayItem'] = $nonPayItem[0];
						
						break;
					case 2:
						
						$numOp = $temp[1];
						$opStr = $parts[2];
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						
						$payment_item = $parts[1];
						$arrPaymentDatails[$i]['PaymentItem'] = $payment_item;
						
						$arrID = explode($itemidSeparator,$parts[3]);
						array_shift($arrID);
						$arrPaymentDatails[$i]['PaymentItemID'] = $arrID[0];
						
						$category = explode($catSeparator,$parts[4]);
						array_shift($category);
						$arrPaymentDatails[$i]['PaymentCategory'] = $category[0];
						
						$amount = explode($oSeparator,$parts[2]);
						array_shift($amount);
						if(sizeof($amount)>0 || true){
							for($j=0; $j<sizeof($amount); $j++){
								$arrPaymentDatails[$i]['PaymentAmount'][] = $amount[$j];
							}
						}
						
						$nonPayItem = explode($nonPaymentItemSeparator,$parts[5]);
						array_shift($nonPayItem);
						$arrPaymentDatails[$i]['NonPayItem'] = $nonPayItem[0];
						
						break;
					case 3:
						$numOp = $temp[1];
						$opStr = $parts[2];
						
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						if(sizeof($options)>0)
						{
							for($j=0; $j<sizeof($options); $j++)
							{
								$arrPaymentDatails[$i]['PaymentItem'][] = $options[$j];
							}
						}
						/*
						 $arrID = explode($itemidSeparator,$parts[3]);
						 array_shift($arrID);
						 if(sizeof($arrID)>0)
						 {
						 for($j=0; $j<sizeof($arrID); $j++)
						 {
						 $arrPaymentDatails[$i]['PaymentItemID'][] = $arrID[$j];
						 }
						 }
						 */
						$temp_arrID = explode($itemidSeparator,$parts[3]);
						array_shift($temp_arrID);
						if(sizeof($temp_arrID) != sizeof($options))
						{
							$arrID = explode(",",$temp_arrID[0]);
							
							if(sizeof($arrID)>0)
							{
								for($j=0; $j<sizeof($arrID); $j++)
								{
									$arrPaymentDatails[$i]['PaymentItemID'][] = $arrID[$j];
								}
							}
						}
						else
						{
							if(sizeof($temp_arrID)>0)
							{
								for($j=0; $j<sizeof($temp_arrID); $j++)
								{
									$arrPaymentDatails[$i]['PaymentItemID'][] = $temp_arrID[$j];
								}
							}
						}
						
						$category = explode($catSeparator,$parts[4]);
						array_shift($category);
						if(sizeof($category)>0)
						{
							for($j=0; $j<sizeof($category); $j++)
							{
								$arrPaymentDatails[$i]['PaymentCategory'][] = $category[$j];
							}
						}
						
						$amount = explode($amtSeparator,$parts[5]);
						array_shift($amount);
						if(sizeof($amount)>0)
						{
							for($j=0; $j<sizeof($amount); $j++)
							{
								$arrPaymentDatails[$i]['PaymentAmount'][] = $amount[$j];
							}
						}
						break;
					case 4:
					case 5:
					case 6:
						$options = array();
						break;
					case 7:
					case 8:
					case 9:
						$numOp = $temp[1];
						$opStr = $parts[2];
						
						$options = explode($oSeparator,$opStr);
						array_shift($options);
						if(sizeof($options)>0)
						{
							for($j=0; $j<sizeof($options); $j++)
							{
								$arrPaymentDatails[$i]['PaymentItem'][] = $options[$j];
							}
						}
						/*
						 $arrID = explode($itemidSeparator,$parts[3]);
						 array_shift($arrID);
						 if(sizeof($arrID)>0)
						 {
						 for($j=0; $j<sizeof($arrID); $j++)
						 {
						 $arrPaymentDatails[$i]['PaymentItemID'][] = $arrID[$j];
						 }
						 }
						 */
						$temp_arrID = explode($itemidSeparator,$parts[3]);
						array_shift($temp_arrID);
						if(sizeof($temp_arrID) != sizeof($options))
						{
							$arrID = explode(",",$temp_arrID[0]);
							
							if(sizeof($arrID)>0)
							{
								for($j=0; $j<sizeof($arrID); $j++)
								{
									$arrPaymentDatails[$i]['PaymentItemID'][] = $arrID[$j];
								}
							}
						}
						else
						{
							if(sizeof($temp_arrID)>0)
							{
								for($j=0; $j<sizeof($temp_arrID); $j++)
								{
									$arrPaymentDatails[$i]['PaymentItemID'][] = $temp_arrID[$j];
								}
							}
						}
						
						$category = explode($catSeparator,$parts[4]);
						array_shift($category);
						if(sizeof($category)>0)
						{
							for($j=0; $j<sizeof($category); $j++)
							{
								$arrPaymentDatails[$i]['PaymentCategory'][] = $category[$j];
							}
						}
						
						$amount = explode($amtSeparator,$parts[5]);
						array_shift($amount);
						if(sizeof($amount)>0)
						{
							for($j=0; $j<sizeof($amount); $j++)
							{
								$arrPaymentDatails[$i]['PaymentAmount'][] = $amount[$j];
							}
						}
						break;
					case 10:
						$arrPaymentDatails[$i]['NonPayItem'] = 1;
						$arrPaymentDatails[$i]['ItemName'] = $que;
						break;
				}
				$mainQ = $parts[1];
				//$result[] = array($type, $mainQ, $options);
			}
			//$this->question_array = $result;
			$result = $arrPaymentDatails;
			return $result;
		}
		
		function isPaymentNotice($NoticeID){
			$sql = "SELECT COUNT(*) FROM INTRANET_NOTICE WHERE UPPER(Module) = 'PAYMENT' AND NoticeID = '$NoticeID'";
			$result = $this->returnVector($sql);
			if($result[0] > 0){
				return true;
			}
			return false;
		}
		
		# 2010-02-03 YatWoon
		function retrieveNoticeYears($TargetType="P")
		{
			// $sql = "select distinct(DATE_FORMAT(DateStart,'%Y')) as StartYear from INTRANET_NOTICE where TargetType='$TargetType' and DateStart IS NOT NULL order by StartYear";
            $sql = "SELECT DISTINCT(DATE_FORMAT(DateStart,'%Y')) as StartYear FROM INTRANET_NOTICE WHERE TargetType = '$TargetType' AND DateStart != '0000-00-00 00:00:00' AND DateStart IS NOT NULL AND isDeleted = 0 ORDER BY StartYear";
            return $this->returnVector($sql);
		}
		
		function isTargetNoticeSigned($NoticeID,$StudentID)
		{
			$sql = "SELECT RecordStatus FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$NoticeID' AND StudentID = '$StudentID'";
			$result = $this->returnVector($sql);
			
			## if RecordStatus == 2, Notice signed already.
			
			if($result[0] == 2)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		function return_FormContent($start='', $end='', $isIssuedNotice=0, $Title='', $Description='', $Question='', $NoticeNumber='', $AllFieldsReq=0, $RecordStatus=1, $noticeAttFolder='', $DisplayQuestionNumber=0, $copied=0, $eClassApp=0, $sendTimeMode='', $sendTimeString='', $ContentType=1, $MergeFormat=1, $MergeType=1, $MergeFile="",$NoticeID=0, $isTemplate = false)
		{
			global $linterface, $PATH_WRT_ROOT, $Lang, $cfg, $intranet_root;
			global $i_Notice_NoticeNumber, $i_Notice_CurrentList, $i_Notice_Title, $i_Notice_DateStart, $i_Notice_DateEnd, $i_Notice_Description, $i_Notice_Type;
			global $i_Notice_Attachment, $i_frontpage_campusmail_attach, $i_frontpage_campusmail_remove, $button_edit, $i_Survey_AllRequire2Fill;
			global $i_Notice_StatusPublished, $i_Notice_StatusSuspended, $i_Notice_StatusTemplate, $i_Notice_TemplateNotes, $i_Notice_SendEmailToParent, $eclassAppConfig;
			global $sys_custom,$UserID;
			global $plugin;
			
			$RecordStatus = $RecordStatus ? $RecordStatus : 1;
			
			include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			
			$li = new libfilesystem();
			$objHtmlEditor = new FCKeditor('Description', "100%", "320", "", "Basic2_withInsertImageFlash", htmlspecialchars_decode($Description), $is_scrollable=true);
			$objHtmlEditor->Config['FlashImageInsertPath'] = $li->returnFlashImageInsertPath($cfg['fck_image']['eNotice'], $id);
			
			// Get temp saved email pushmsg choices
			if($NoticeID > 0)
			{
				if($RecordStatus==4)
				{
					// Get temp saved email and push msg choices
					// $sql = "SELECT pushmessagenotify, pushmessagenotifyMode, pushmessagenotifyTime, emailnotify, emailnotify_Students FROM INTRANET_NOTICE WHERE NoticeID = '$NoticeID'";
					// $emailPushMsgChoicesArray = $this->returnResultSet($sql);
					// $emailPushMsgChoicesArray = $emailPushMsgChoicesArray[0];
					$emailPushMsgChoicesArray = array(
							'pushmessagenotify'=>$this->pushmessagenotify,
							'pushmessagenotify_Students'=>$this->pushmessagenotify_Students,
					        'pushmessagenotify_PIC' => $this->pushmessagenotify_PIC,
					        'pushmessagenotify_ClassTeachers' => $this->pushmessagenotify_ClassTeachers,
							'pushmessagenotifyMode'=>$this->pushmessagenotifyMode,
							'pushmessagenotifyTime'=>$this->pushmessagenotifyTime,
							'emailnotify'=>$this->emailnotify,
							'emailnotify_Students'=>$this->emailnotify_Students,
					        'emailnotify_PIC'=>$this->emailnotify_PIC,
					        'emailnotify_ClassTeachers'=>$this->emailnotify_ClassTeachers
					);
				}
			}
			
			// [2016-0819-1158-52236] Get Current Time
			$nowTime = strtotime("now");
			$enableTeacherApp = $plugin['eClassTeacherApp'];
			//$eClassApp = false;
			if ($eClassApp)
			{
				include_once($PATH_WRT_ROOT.'includes/eClassApp/eClassAppConfig.inc.php');
				include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
				
				$leClassApp = new libeClassApp();
				
				// [2016-0819-1158-52236]
				$eClassAppSettingsObj = $leClassApp->getAppSettingsObj();
				$enableStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableStudentApp']);
				
				$nowChecked = false;
				$scheduledChecked = false;
				
				// 26-9-2016 Villa
				$check = ""; 			// check the box by default setting
				if($Title=="" || $isTemplate){ 		// check if the mode is edit(edit mode should have title) -> new.php
					$check= $this->sendPushMsg? "checked" : "";
					$scheduledChecked=true;
					$emailNotifyParentChecked = $this->sendEmail ? "checked" : "";
					$emailNotifyStudentChecked = $this->sendEmail ? "checked" : "";
					$emailNotifyPICChecked = $this->sendEmail ? "checked" : "";
					$emailNotifyClassTeachersChecked= $this->sendEmail ? "checked" : "";
				}
				
				if ($sendTimeMode == "scheduled" && $sendTimeString != "") {
					$sendTime = strtotime($sendTimeString);
					if ($sendTime > $nowTime) {
						// $scheduledMessageChecked = "checked";
						$nowChecked = false;
						$scheduledChecked = true;
					}
					else {
						$scheduledMessageChecked = "";
						$scheduledStudentsMessageChecked = "";
						$sendTimeString = "";
					}
				}
				else {
					$sendTimeString = "";
					// $nowChecked = true;
					// $scheduledChecked = false;	//26-9-2016
				}
				######################
				# Assign back temp saved choices
				######################
				if($RecordStatus==4)
				{
				    if($emailPushMsgChoicesArray['pushmessagenotify']||$emailPushMsgChoicesArray['pushmessagenotify_Students']||$emailPushMsgChoicesArray['pushmessagenotify_PIC']||$emailPushMsgChoicesArray['pushmessagenotify_ClassTeachers']){
						if($emailPushMsgChoicesArray['pushmessagenotify']){
							$scheduledMessageChecked = "checked";
						}
						if($emailPushMsgChoicesArray['pushmessagenotify_Students']){
							$scheduledStudentsMessageChecked = "checked";
						}
						if($emailPushMsgChoicesArray['pushmessagenotify_PIC']){
						    $scheduledPICMessageChecked = "checked";
						}
						if($emailPushMsgChoicesArray['pushmessagenotify_ClassTeachers']){
						    $scheduledClassTeachersMessageChecked = "checked";
						}
						if($emailPushMsgChoicesArray['pushmessagenotifyMode']=='now'){
							$nowChecked = true;
							$scheduledChecked = false;
						}
						else if($emailPushMsgChoicesArray['pushmessagenotifyMode']=='scheduled'){
							$scheduledChecked = true;
							$nowChecked = false;
							$sendTimeString = $emailPushMsgChoicesArray['pushmessagenotifyTime'];
							$sendTime = strtotime($sendTimeString);
						}
					}
					if($emailPushMsgChoicesArray['emailnotify']){
						$emailNotifyParentChecked = "checked";
					}
					if($emailPushMsgChoicesArray['emailnotify_Students']){
						$emailNotifyStudentChecked = "checked";
					}
					if($emailPushMsgChoicesArray['emailnotify_PIC']){
					    $emailNotifyPICChecked = "checked";
					}
					if($emailPushMsgChoicesArray['emailnotify_ClassTeachers']){
					    $emailNotifyClassTeachersChecked = "checked";
					}
				}
				
				$htmlAry['sendTimeNowRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_now', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['now'], $nowChecked, $Class="", $Lang['AppNotifyMessage']['Now'], "clickedSendTimeRadio(this.value);");
				$htmlAry['sendTimeScheduledRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_scheduled', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['scheduled'], $scheduledChecked, $Class="", $Lang['AppNotifyMessage']['SpecificTime'], "clickedSendTimeRadio(this.value);");
				
				// reference: http://stackoverflow.com/questions/2480637/round-minute-down-to-nearest-quarter-hour
				$rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
				$nextTimeslotHour = date('H', $rounded_seconds);
				$nextTimeslotMinute = date('i', $rounded_seconds);
				if ($sendTimeString == "") {
					$selectionBoxDate = date('Y-m-d', $nowTime);
					$selectionBoxHour = $nextTimeslotHour;
					$selectionBoxMinute = $nextTimeslotMinute;
				}
				else {
					$selectionBoxDate = date('Y-m-d', $sendTime);
					$selectionBoxHour = date('H', $sendTime);
					$selectionBoxMinute = date('i', $sendTime);
				}
				
				/*
					if ($start == "") {
						$startDate = date('Y-m-d', $nowTime);
						$startDateHour = $nextTimeslotHour;
						$startDateMin = $nextTimeslotMinute;
					}
					else {
						$startDate = date('Y-m-d', strtotime($start));
						$startDateHour = date('H', strtotime($start));
						$startDateMin = date('i', strtotime($start));
					}
					if ($end == "") {
						$endDate = date('Y-m-d', $nowTime);
						$endDateHour = $nextTimeslotHour;
						$endDateMin = $nextTimeslotMinute;
					}
					else {
						$endDate = date('Y-m-d', strtotime($end));
						$endDateHour = '23';
						$endDateMin = '59';
					}
					// debug_pr($startDate);
				*/
				
				$x = "";
				$x .= "&nbsp;&nbsp;";
				$x .= $linterface->GET_DATE_PICKER('sendTime_date', $selectionBoxDate);
				$x .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour', $selectionBoxHour);
				$x .= " : ";
				$x .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min', $selectionBoxMinute, $others_tab='', $interval=15);
				$x .= "&nbsp;&nbsp;";
				$x .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['SyncWithIssueDate'],'button','javascript:syncIssueTimeToPushMsg()');
				$x .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id='div_push_message_date_err_msg'></span>";
				$x .= "<input type='hidden' id='sendTimeString' name='sendTimeString' value='' />";
				$htmlAry['sendTimeDateTimeDisplay'] = $x;
				
				$htmlAry['sendInBatchRemarks'] = '';
				if ($leClassApp->isEnabledSendBulkPushMessageInBatches()) {
					$htmlAry['sendInBatchRemarks'] = '<span class="tabletextremark">('.$Lang['AppNotifyMessage']['SendInBatchesRemarks'].')</span>';
				}
			}
			
			$rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
			$nextTimeslotHour = date('H', $rounded_seconds);
			$nextTimeslotMinute = date('i', $rounded_seconds);
			if ($start == "") {
				$startDate = date('Y-m-d', $nowTime);
				$startDateHour = $nextTimeslotHour;
				$startDateMin = $nextTimeslotMinute;
			}
			else {
				$startDate = date('Y-m-d', strtotime($start));
				$startDateHour = date('H', strtotime($start));
				$startDateMin = date('i', strtotime($start));
			}
			if ($end == "") {
				$endDate = date('Y-m-d', $nowTime);
				$endDateHour = $nextTimeslotHour;
				$endDateMin = $nextTimeslotMinute;
			}
			else {
				$endDate = date('Y-m-d', strtotime($end));
				$endDateHour = date('H', strtotime($end));
				$endDateMin = date('i', strtotime($end));
			}
			
			$x = "
				<table class=\"form_table_v30\">
					<!--TEMPLATE_SELECTION-->
					<tr>
						<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Notice_NoticeNumber."</td>
						<td class='field_content_short'><input type='text' name='NoticeNumber' value='". $NoticeNumber ."' maxlength='50' class='textboxnum'> ";
						
						if(!$sys_custom['eNotice']['HideCurrentNoticeList'])
						{
							$x .= "&nbsp;<a href=javascript:newWindow('currentlist.php',1) class='tablelink'>[". $i_Notice_CurrentList."]</a>";
						}
						
						$x .= "<br><span id='div_NoticeNumber_err_msg'></span>
						</td>
					</tr>
								
					<tr>
						<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Notice_Title."</td>
						<td class='field_content_short'><input type='text' name='Title' maxlength='255' value='". $Title."' class='textboxtext'>
						<span id='div_Title_err_msg'></span>
						</td>
					</tr>";
			
			// [2015-0416-1040-06164] merge notice settings
			// Not issused notice
			if(!$isIssuedNotice)
			{
				global $file_path, $sample_csv_file1, $sample_csv_file1_userlogin, $sample_csv_file2, $sample_csv_file2_userlogin;
				
				$changedCSVinput = "<input type='hidden' id='changeCSV' name='changeCSV' value='1' />";
				$previewCSVinput = "<input type='hidden' id='previewCSV' name='previewCSV' value='0' />";
				
				if (!$sys_custom['project']['CourseTraining']['IsEnable'])
				{
					// already uploaded merge csv file
					if (strlen($MergeFile)>1)
					{
						$DisplayCSVUpload = "none";
						
						$csvpath = getEncryptedText("$file_path/file/mergenotice/".ceil($this->NoticeID/10000)."/".$this->NoticeID."/".$this->MergeFile);
						$CSV_download = "<a href='/home/download_attachment.php?target_e={$csvpath}'>".$Lang['Button']['Download']."</a> | <a href='javascript:jsChangeCSV()' >".$Lang['MassMailing']['BtnChange']."</a>";
						$changedCSVinput = "<input type='hidden' id='changeCSV' name='changeCSV' value='0' />";
					}
					
					// Content Type
					$x .= " <tr valign='top'>
								<td class='field_title'><span class='tabletextrequire'>*</span>".$Lang['eNotice']['ContentSetting']."</td>
								<td>
									<input type='radio' name='ContentType' id='ContentType1' onClick='ShowMergeSetting(1)' value='1' ".($ContentType!=2?"checked":"")."/> <label for='ContentType1'>".$Lang['eNotice']['StaticContent']."</label> &nbsp;
									<input type='radio' name='ContentType' id='ContentType2' onClick='ShowMergeSetting(2)' value='2' ".($ContentType==2?"checked":"")."/> <label for='ContentType2'>".$Lang['eNotice']['MergeContent']."</label>
								</td>
							</tr>";
					
					// Merge CSV
					$x .= " <tr style='display:none' id='MergeCSVTr'>
								<td class='field_title'><span class='tabletextrequire'>*</span>".$Lang['MassMailing']['CSVFile']."</td>
								<td>
									<input type='radio' name='MergeFormat' id='MergeFormat1' onClick='ShowCSVFormat(1)' value='1' ".($MergeFormat!=2?"checked":"")."/> <label for='MergeFormat1'>".$Lang['MassMailing']['CSVFormat1']."</label> &nbsp;
									<input type='radio' name='MergeFormat' id='MergeFormat2' onClick='ShowCSVFormat(2)' value='2' ".($MergeFormat==2?"checked":"")."/> <label for='MergeFormat2'>".$Lang['MassMailing']['CSVFormat2']."</label>
									<!--&nbsp;&nbsp;".$linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['SyncWithIssueDate'], "button" , "javascript:updateNoticeTargetfromCSV()")."-->
									<br />$CSV_download
									<div id='UploadCSV' style='display:$DisplayCSVUpload'>
										<input type='file' id='FileCSV' name='FileCSV' size='60' />
										<input type='hidden' name='FileCSV_Name' />
										$changedCSVinput
										$previewCSVinput
										<div id='CSV_Sample1' ".($MergeFormat==2?"style='display:none'":"").">
											<a class='tablelink' href='$sample_csv_file1'> ".$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['CSV_CLASS_NO']." </a>
											<br /><a class='tablelink' href='$sample_csv_file1_userlogin'> ".$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['UserLogin']."</a>
											<br /><span class='tabletextremark'>* ".$Lang['eNotice']['MergeCSVRemarks1']."</span>
										</div>
										<div id='CSV_Sample2' ".($MergeFormat!=2?"style='display:none'":"").">
											<a class='tablelink' href='$sample_csv_file2'> ".$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['CSV_CLASS_NO']."</a>
											<br /><a class='tablelink' href='$sample_csv_file2_userlogin'> ".$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['UserLogin']."</a>
											<br /><span class='tabletextremark'>* ".$Lang['eNotice']['MergeCSVRemarks2']."</span>
										</div>
									</div>
								</td>
							</tr>";
					
					// CSV Type
					$x .= " <tr style='display:none' id='MergeTypeTr'>
								<td class='field_title'><span class='tabletextrequire'>*</span>".$Lang['eNotice']['MergeCSVFormat']."</td>
								<td>
									<input type='radio' name='MergeType' id='MergeType1' value='1' ".($MergeType!=2?"checked":"")."/> <label for='MergeType1'>".$Lang['eNotice']['MergeCSVFormat1']."</label> &nbsp;
									<input type='radio' name='MergeType' id='MergeType2' value='2' ".($MergeType==2?"checked":"")."/> <label for='MergeType2'>".$Lang['eNotice']['MergeCSVFormat2']."</label>
								</td>
							</tr>";
				}	// not Amway and Oaks
			}
			// Issued notice
			else
			{
				global $file_path;
				
				if (!$sys_custom['project']['CourseTraining']['IsEnable'])
				{
					// already uploaded merge csv file
					if (strlen($MergeFile)>1)
					{
						$csvpath = getEncryptedText("$file_path/file/mergenotice/".ceil($this->NoticeID/10000)."/".$this->NoticeID."/".$this->MergeFile);
						$CSV_download = "<a href='/home/download_attachment.php?target_e={$csvpath}'>".$Lang['Button']['Download']."</a>";
						// $previewCSVinput = "<input type='hidden' id='previewCSV' name='previewCSV' value='0' />";
					}
					
					// Content Type
					$x .= " <tr valign='top'>
								<td class='field_title'>".$Lang['eNotice']['ContentSetting']."</td>
								<td>".($ContentType!=2? $Lang['eNotice']['StaticContent'] : $Lang['eNotice']['MergeContent'])."</td>
							</tr>";
					
					if($ContentType==2){
						// Merge CSV
						$x .= " <tr id='MergeCSVTr'>
									<td class='field_title'>".$Lang['MassMailing']['CSVFile']."</td>
									<td>".
									($MergeFormat!=2? $Lang['MassMailing']['CSVFormat1'] : $Lang['MassMailing']['CSVFormat2'])."
									<br />$CSV_download
									$previewCSVinput
									</td>
									</tr>";
									
						// CSV Type
						$x .= " <tr id='MergeTypeTr'>
									<td class='field_title'>".$Lang['eNotice']['MergeCSVFormat']."</td>
									<td>".($MergeType!=2? $Lang['eNotice']['MergeCSVFormat1'] : $Lang['eNotice']['MergeCSVFormat2'])."</td>
								</tr>";
					}
				}	// Not Amway and Oaks
			}
			$x .= "<!--___STUDENT_INPUT___-->";
			
			if($isIssuedNotice) {
				$x .= "	<tr valign='top'>
							<td class='field_title'>". $i_Notice_DateStart."</span></td>
							<td>$start</td>
						</tr>
							";
			}
			else {
				$x .= "	<tr valign='top'>
							<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Notice_DateStart."</span></td>
							<td>
								".$linterface->GET_DATE_PICKER("DateStart", $startDate) ."
								".$linterface->Get_Time_Selection_Box('DateStart_hour', 'hour', $startDateHour, $others_tab='').':'.$linterface->Get_Time_Selection_Box('DateStart_min', 'min', $startDateMin, $others_tab='')."
								".$linterface->GET_HIDDEN_INPUT('DateStartTime', 'DateStartTime', $startDateHour.':'.$startDateMin.':00')."<br><span id='div_DateStart_err_msg'></span>
							</td>
						</tr>
						";
			}
			
			//<td>". ($isIssuedNotice ? $Description : $linterface->GET_TEXTAREA("Description", $Description)) ."</td>
			$x .= " <tr valign='top'>
						<td class='field_title'><span class='tabletextrequire'>*</span>". $i_Notice_DateEnd."</span></td>
						<td>".$linterface->GET_DATE_PICKER("DateEnd", $endDate) . $linterface->Get_Time_Selection_Box('DateEnd_hour', 'hour', $endDateHour, $others_tab='').':'.$linterface->Get_Time_Selection_Box('DateEnd_min', 'min', $endDateMin, $others_tab='')."
						".$linterface->GET_HIDDEN_INPUT('DateEndTime', 'DateEndTime', $endDateHour.':'.$endDateMin.':00')."<br><span id='div_DateEnd_err_msg'></span>
						</td>
					</tr>";
			
			// [2017-1207-0941-45235]
			if($sys_custom['eNotice']['NoticeContentDecodeEntity']) {
				$Description = html_entity_decode($Description);
			}
			
			$x .= " <tr valign='top'>
						<td class='field_title'>". $i_Notice_Description."</td>
						<td>".
						($isIssuedNotice ? htmlspecialchars_decode($Description)."&nbsp;" : $objHtmlEditor->CreateHtml()) .
						($isIssuedNotice ? "" : "
								<div id='mergeRemark' ".($ContentType!=2?"style='display:none'":"").">
									<span class='tabletextremark'>* ".$Lang['MassMailing']['HTMLContents_Code_remark']."</span><br>
									".$linterface->Get_Thickbox_Link(480, 780, "", $Lang['eNotice']['Preview'], "", $InlineID="csvPreviewDiv", $Content="", $LinkID="previewLink")."
									". $linterface->GET_BTN($Lang['eNotice']['Preview'], "button","previewCSVFile();","PreviewBtn")."
								</div>"
								)."
						</td>
					</tr>
										
					<tr valign='top'>
						<td class='field_title'>". $i_Notice_Attachment."</td>
						<td>
					";
			
			$displayAttachment = $this->displayAttachmentEdit("file2delete[]", $noticeAttFolder);
			$x .= "	<table>". $displayAttachment ." </table>";
			$x .= "	<table id='upload_file_list' border='0' cellpadding='0' cellpadding='0'>
						<tr><td>
								<input class='file' type='file' name='filea0' size='40'>
								<input type='hidden' name='hidden_userfile_name0'>
						</td></tr>
					</table>
					<input type=button value=' + ' onClick='add_field()'>";
					/*
					<table border='0' cellspacing='1' cellpadding='1'>
						<tr>
						<td>
						 <select name='Attachment[]' size='4' multiple>
							 <option>";
							 	for($i = 0; $i < 40; $i++) { $x .="&nbsp;"; }
							 $x .= "</option>
						 </select>
					 	</td>
					 	<td>
							 ". $linterface->GET_BTN($i_frontpage_campusmail_attach, "button","newWindow('attach.php?folder=$noticeAttFolder',2)") ."<br>
							 ". $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['Attachment[]'])") ."<br>
						 </td>
					 	</tr>
					 </table>
					 */

			if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
			{
				$snYesChecked = $this->SpecialNotice ? 'checked' : '';
				$snNoChecked = $this->SpecialNotice ? '' : 'checked';

				$displaySpecialNotice = "<tr valign='top'>
						<td class='field_title'>".$Lang['eNotice']['SpecialNotice']."</td>
						<td>
							<input type='radio' value='1' name='SpecialNotice' id='SpecialNotice_yes' onClick='js_handle_special_notice_options_display();' $snYesChecked /> <label for='SpecialNotice_yes'>".$Lang['General']['Yes']."</label> 
							<input type='radio' value='0' name='SpecialNotice' id='SpecialNotice_no' onClick='js_handle_special_notice_options_display();' $snNoChecked /> <label for='SpecialNotice_no'>".$Lang['General']['No']."</label>
                        </td>
				</tr>";
			}

			$displayPreviewButton = $isIssuedNotice && $NoticeID > 0 && trim($this->Question) && $this->Module=='';
			$x .="		</td>
					</tr>
					
					<tr valign='top'>
						<td class='field_title'>". $Lang['eNotice']['ReplySlipForm']."</td>
						<td>
							". /*($isIssuedNotice ? "" : $linterface->GET_BTN($button_edit, "button","newWindow('editform.php',1)"))*/ "" ."
							". ($isIssuedNotice ? ($displayPreviewButton? $linterface->GET_BTN($Lang['eNotice']['PreviewSlipSetting'], "button","newWindow('/home/eAdmin/StudentMgmt/notice/editform_preview.php?NoticeID=$NoticeID',1)") : "") : $linterface->GET_BTN($button_edit, "button","newWindow('/home/eAdmin/StudentMgmt/notice/editform2.php',1)"))."
							". $linterface->GET_BTN($Lang['eNotice']['Preview'], "button","newWindow('preview.php',10)")."
							<br>".
							($isIssuedNotice?
									"<input name='AllFieldsReq' type='checkbox' value='1' id='AllFieldsReq' ". ($isIssuedNotice ? "disabled":"") ." " . ($AllFieldsReq ? "checked":"") . "> <label for='AllFieldsReq'>". $i_Survey_AllRequire2Fill."</label> <br>
							<input name='DisplayQuestionNumber' type='checkbox' value='1' id='DisplayQuestionNumber' " . ($DisplayQuestionNumber ? "checked":"") . "> <label for='DisplayQuestionNumber'>". $Lang['eNotice']['DisplayQuestionNumber'] ."</label>" :
									"<input name='AllFieldsReq' type='hidden' value='".$AllFieldsReq."' id='AllFieldsReq'>
							<input name='DisplayQuestionNumber' type='hidden' value='".$DisplayQuestionNumber."' id='DisplayQuestionNumber'>"
									).
						"</td>
					</tr>
					$displaySpecialNotice
					
					<tr valign='top'>
						<td class='field_title'>".$Lang['eNotice']['Type']."</td>
						<td>
							<table border='0' cellpadding='1' cellspacing='1' width='100%'>";
								if($this->needApproval){
									// if UserID in Array -> this user is in Approval Group
                                    // Check if user can approve school notice
									//if($this->hasApprovalRight()){
                                    if($this->hasSchoolNoticeApprovalRight()){
										$needApprove = false;
									}
									else{
										if($this->RecordStatus==1){
											$needApprove = false;
										}
										else{
											$needApprove = true;
										}
									}
								}
								
								$x .= "<tr><td>
											<input type='radio' name='sus_status' value='".(($needApprove)?4:1)."' id='status1'  ". ($RecordStatus==1 || $RecordStatus==4 || ($RecordStatus==5&&!$this->hasSchoolNoticeApprovalRight()) ?"checked":"") ." onClick='js_show_email_notification(this.value);' ><label for='status1'>". (($needApprove)?$Lang['eNotice']['NeedApproval']:$Lang['eNotice']['ApproveAndIssue'])."</label><br />
											<span style='margin-left:20px; ".($eClassApp ? "" : "display: none;")."'>";
								if(!$sys_custom['eNotice']['HideParentAppNotify']) {
									$x .= "		<input type='checkbox' name='pushmessagenotify' value='1' id='pushmessagenotify' onclick='checkedSendPushMessage(this);' $scheduledMessageChecked ".(($RecordStatus==1 || $RecordStatus==4)? "":"disabled")." $check > <label for='pushmessagenotify'>". $Lang['eNotice']['NotifyParentsByPushMessage']."</label><br/>";
								}
								
								if($enableStudentApp) {
									$thisSpanStyle = $sys_custom['eNotice']['HideParentAppNotify']? "" : "style='margin-left:20px;";
									$x .= "		<span ".$thisSpanStyle." ".($eClassApp ? "" : "display: none;")."'>";
									$x .= "			<input type='checkbox' name='pushmessagenotify_Students' value='1' id='pushmessagenotify_Students' onclick='checkedSendPushMessage(this);' $scheduledStudentsMessageChecked ".(($RecordStatus==1 || $RecordStatus==4)? "":"disabled")."  $check> <label for='pushmessagenotify_Students'>". $Lang['eNotice']['NotifyStudentsByPushMessage']."</label><br/>";
									$x .= "		</span>";
								}
								// 2018-05-18 11:38 [Philips] Push Message Notify for PIC
								if($enableTeacherApp){
								    $thisSpanStyle = !$enableTeacherApp ? "" : "style='margin-left:20px;";
								    $x .= "		<span ".$thisSpanStyle." ".($eClassApp ? "" : "display: none;")."'>";
								    $x .= "			<input type='checkbox' name='pushmessagenotify_PIC' value='1' id='pushmessagenotify_PIC' onclick='checkedSendPushMessage(this);' $scheduledPICMessageChecked ".(($RecordStatus==1 || $RecordStatus==4)? "":"disabled")."  $check> <label for='pushmessagenotify_PIC'>". $Lang['eNotice']['NotifyPICByPushMessage']."</label><br/>";
								    $x .= "		</span>";
								}
								// 2018-05-18 11:38 [Philips] Push Message Notify for Class Teachers
								if($enableTeacherApp){
								    $thisSpanStyle = !$enableTeacherApp ? "" : "style='margin-left:20px;";
								    $hideThisSpan = $sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $this->SpecialNotice;
								    $x .= "		<span id='span_Pushmessagenotify_ClassTeachers' ".$thisSpanStyle." ".($eClassApp && !$hideThisSpan? "" : "display: none;")."'>";
								    $x .= "			<input type='checkbox' name='pushmessagenotify_ClassTeachers' value='1' id='pushmessagenotify_ClassTeachers' onclick='checkedSendPushMessage(this);' $scheduledClassTeachersMessageChecked ".(($RecordStatus==1 || $RecordStatus==4)? "":"disabled")."  $check> <label for='pushmessagenotify_ClassTeachers'>". $Lang['eNotice']['NotifyClassTeachersByPushMessage']."</label><br/>";
								    $x .= "		</span>";
								}
								
								$x .= "		</span>";
								
								$y = "";
								if($NoticeID!=0){	// To check if it is edit mode. If yes, display the selection only send to new selected audience
									$y = "<input style='margin-left:60px;'type='checkbox' name='sendNewSelect' value='1' id='sendNewSelect'><label id= 'sendNewSelect' for='sendNewSelect'>".$Lang['eClassApps']['SendToNew']."</br></label>";
								}
								
								// Added By Philips 2018-05-24
								if($eClassApp)
								{
								    $x .= "		<span ".$thisSpanStyle." ".($eClassApp ? "" : "display: none;")."'>";
								    $x .= $htmlAry['sendInBatchRemarks'];
								    $x .= "		</span>";
								}
								// Added By Philips 2018-05-24
								
								if($eClassApp)
								{
									$x .= "<div id = 'PushMessageSetting'>";
									$x .= $linterface->Get_Warning_Message_Box($Lang['eClassApps']['PushMessage']." ".$Lang['ePost']['Setting'], "<div id='sendTimeSettingDiv' style='".(($scheduledMessageChecked == "checked"||$scheduledStudentsMessageChecked == "checked") ? "" : "display: none;")."' >
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeNowRadio']."
												<br />".$y.
											"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeScheduledRadio']."
												<br />
												<div id='specificSendTimeDiv' style='".($scheduledChecked ? "" : "display: none;")."'>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeDateTimeDisplay'].
											"</div>
											</div>");
									$x .= "</div>";
								}
								
								// Added By Philips 2018-05-24
								if($eClassApp)
								{
								    $x .= "		<div ".$thisSpanStyle." ".($eClassApp ? "" : "display: none;")."'>";
								    $x .= "<hr>";
								    $x .= "		</div>";
								}
								// Added By Philips 2018-05-24
								
								// $htmlAry['sendTimeDateTimeDisplay']. "<a href=\"javascript:syncIssueTimeToPushMsg();\">".$Lang['AppNotifyMessage']['SyncWithIssueDate']."</a>
								// $x .= "	<div id='sendTimeSettingDiv' style='".(($scheduledMessageChecked == "checked"||$scheduledStudentsMessageChecked == "checked") ? "" : "display: none;")."'>
								// 				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeNowRadio']."
								// 				<br />
								// 				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeScheduledRadio']."
								// 				<br />
								// 				<div id='specificSendTimeDiv' style='".($scheduledChecked ? "" : "display: none;")."'>
								// 					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeDateTimeDisplay']. "<a href=\"javascript:syncIssueTimeToPushMsg();\">".$Lang['AppNotifyMessage']['SyncWithIssueDate']."</a>
								// 				</div>
								// 			</div>
								// 			</span>";
								
								if (!$sys_custom['eNotice']['HideParenteNotice'])
								{
									$allowEmailNotify = true;
									if ($sys_custom['project']['CourseTraining']['IsEnable'] || $sys_custom['DHL']) {
										$allowEmailNotify = false;
									}
									if ($allowEmailNotify) {
										$x .= " <span style='margin-left:20px;' >
										<input type='checkbox' name='emailnotify' value='1' id='emailnotify' $emailNotifyParentChecked ".(($RecordStatus==1 || $RecordStatus==4)? "":"disabled")."> <label for='emailnotify'>". $Lang['eNotice']['NotifyParentsByEmail']."</label><br>
										</span>";
										$x .= " <span style='margin-left:20px;' >
										<input type='checkbox' name='emailnotify_PIC' value='1' id='emailnotify_PIC' $emailNotifyPICChecked ".(($RecordStatus==1 || $RecordStatus==4)? "":"disabled")."> <label for='emailnotify_PIC'>". $Lang['eNotice']['NotifyPICByEmail']."</label><br>
										</span>";

                                        $hideThisSpan = $sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $this->SpecialNotice;
										$x .= " <span id='span_Emailnotify_ClassTeachers' style='margin-left:20px; ".(!$hideThisSpan? "" : "display: none;")."' >
										<input type='checkbox' name='emailnotify_ClassTeachers' value='1' id='emailnotify_ClassTeachers' $emailNotifyClassTeachersChecked ".(($RecordStatus==1 || $RecordStatus==4)? "":"disabled")."> <label for='emailnotify_ClassTeachers'>". $Lang['eNotice']['NotifyClassTeachersByEmail']."</label><br>
										</span>";
									}
									$x .= " <span style='margin-left:20px;'>
									<input type='checkbox' name='emailnotify_Students' value='1' id='emailnotify_Students' $emailNotifyStudentChecked  ".(($RecordStatus==1 || $RecordStatus==4)? "":"disabled")."> <label for='emailnotify_Students'>". $Lang['eNotice']['NotifyStudentsByEmail']."</label>
										</span>
										<br>
										<span style='margin-left:20px;'>(". $Lang['eNotice']['NotifyRemark'] .")</span>
									";
								}
								
								if($this->needApproval)
								{
									if(!$needApprove && $RecordStatus>=4) {
										$x .= "<p><input type='radio' name='sus_status' value='5' id='status5'  ". ($RecordStatus==5 ? "checked":"") ." onClick='js_show_email_notification(this.value);'><label for='status5'>". $Lang['eNotice']['Reject']."</label> <br />";
										
										// Need Approval is "ON" in System Settings && 'I' can Approve eNotice (have e-Notice Approval Right)
										$x .= '<p>
												<label for="approvalComment">'.$Lang['eNotice']['RejectedComment'].': </label><br>
												<textarea rows="4" cols="50" id="approvalComment" name="approvalComment" >'.$this->ApprovalComment.'</textarea>';
									}
								}
								$x .= "</td>
									</tr>";
								
								// if($this->IssueUserID==$UserID||$this->IssueUserID==''){
								if(!($this->needApproval && !$needApprove && $RecordStatus>=4)){
									$x .= "<tr>
										<td>
											<input type='radio' name='sus_status' value='2' id='status2'  ". ($RecordStatus==2 ? "checked":"") ." onClick='js_show_email_notification(this.value);'><label for='status2'>". $Lang['eNotice']['NotToBeDistributed']."</label> ". ($copied ? "<font color=red>".$Lang['Notice']['CopiedSatusRemark']."</font>": "") ."<br />
										</td>
									</tr>
									<tr>
										<td>
											<input type='radio' name='sus_status' value='3' id='status3'   ". ($RecordStatus==3 ? "checked":"") ." onClick='js_show_email_notification(this.value);'><label for='status3'>". $i_Notice_StatusTemplate."</label><br />". $i_Notice_TemplateNotes."<br />
										</td>
									</tr>" ;
									
									// }
									// if($this->needApproval){
									// $x .= "	<tr>
									//				<td>
									//					<input type='radio' name='sus_status' value='4' id='status4' ". ($RecordStatus==4 ? "checked":"") ." onClick='js_show_email_notification(this.value);'><label for='status4'>". $Lang['eNotice']['NeedApproval'] ."
									//				</td>
									//			</tr>";
								}
								$x .= "</table>
						</td>
					</tr>
					<!--
					<tr valign='top'>
						<td class='field_title'>&nbsp;</td>
						<td>
							<input type='checkbox' name='emailnotify' value='1' id='emailnotify'> <label for='emailnotify'>". $Lang['eNotice']['NotifyParentsByEmail']."</label><br>
							<input type='checkbox' name='emailnotify_Students' value='1' id='emailnotify_Students'> <label for='emailnotify_Students'>". $Lang['eNotice']['NotifyStudentsByEmail']."</label>
						</td>
					</tr>
					-->
				</table>
									
				<input type='hidden' name='qStr' value='". $Question. "'>
				<input type='hidden' name='aStr' value=''>
			";
									
			// JS for sync issue time and push msg send time
			$script = '';
			$script .= '<script>';
			if($sendTimeString==''){
				$script .= 'var no_default_send_time = true;';
			}
			else{
				$script .= 'var no_default_send_time = false;';
			}
			
			$script .= 'function syncIssueTimeToPushMsg(){';
				$script .= 'var issueDate = $(\'#DateStart\').val();';
				$script .= 'var issueHour = parseInt($(\'#DateStart_hour option:selected\').val());';
				$script .= 'var issueMin = parseInt($(\'#DateStart_min option:selected\').val());';
			
				$script .= '	
							if(issueMin==0){
        						issueMin = 00;
        					}else if(issueMin<=15){
								issueMin = 15;
        					}else if(issueMin<=30){
								issueMin = 30;
							}else if(issueMin<=45){
								issueMin = 45;
							}else{
								issueMin = 00;
								issueHour++
							}
											
							if(issueHour==24){
								issueHour = 0;
								var today = new Date(issueDate);
								today.setDate(today.getDate() + 1);
								var issueDate = (today.getFullYear()+"-"+pad((today.getMonth()+1),2)+"-"+pad(today.getDate(),2));
							}
							
							';
				$script .= '$(\'#sendTime_date\').val(issueDate);';
				$script .= '$(\'#sendTime_hour\').val(issueHour);';
				$script .= '$(\'#sendTime_minute\').val(issueMin);';
			$script .= '}';
			
			$script .= '$(function() {';
				$script .= 'if(no_default_send_time){';
					$script .= 'syncIssueTimeToPushMsg();';
				$script .= '}';
			$script .= '});';
			
			$script .= "function pad(n, width, z) {
						  z = z || '0';
						  n = n + '';
						  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
						}";
									
			$script .= '</script>';
			$x .= $script;
			
			return $x;
		}
		
		function updateSignatureFooter($noticeIDAry, $option="", $issueDate="", $StudentID="", $footerTextAry=array())
		{
			global $Lang;
			
			# change issueDate from "YYYY-MM-DD" to "DD/MM/YYYY"
			if($issueDate!="")
				$issueDate = date("j/n/Y", mktime(0,0,0,substr($issueDate,5,2),substr($issueDate,8,2),substr($issueDate,0,4)));
			
			if($option!="") {
				$optionAry = explode(',', $option);
			}
			
			//$footer = "<p>&nbsp;</p><p>&nbsp;</p>";
			//$footer = "<p>&nbsp;</p>";
			
			# space with underline
			for($i=0; $i<40; $i++)
				$space .= "&nbsp;";
			$space = "<u>$space</u>";
			
			# space with short underline
			for($i=0; $i<11; $i++)
				$shortSpace .= "&nbsp;";
			
			/*
			 foreach($optionAry as $val) {
			 
			 for($a=0; $a<sizeof($Lang['eDiscipline']['NoticeFooterArray'][$val]); $a++) {
			 $footer .= $Lang['eDiscipline']['NoticeFooterArray'][$val][$a]." : <u>";
			 for($i=0; $i<40; $i++)
			 $footer .= "&nbsp;";
			 $footer .= "</u>";
			 }
			 
			 $footer .= "<p>&nbsp;</p>";
			 
			 }
		 	*/
			if($option!="") {
				$footer .= "<style type=\"text/css\">
								.td_bottom {
									border-bottom: 1px solid #000000;
								}
								td{
									font-family: 璅扑嚙踝蕭嚙踝蕭嚙踝蕭嚙踝蕭嚙踝蕭嚙�				font-size: 15px;
								}
							</style>";
			 	$footer .= "<table width=\'100%\' border=\'0\'>";
				
			 	if(!isset($footerTextAry) || sizeof($footerTextAry)==0)
			 		$footerTextAry = $Lang['eDiscipline']['NoticeFooterArray'];
		 			//$footerTextAry = $Lang['eDiscipline']['NoticeFooterArrayForStudentAwardReport'];
			 	
		 		foreach($footerTextAry as $id=>$nameAry) {
		 			if(in_array($id, $optionAry)) {
		 				$footer .= "<tr>";
		 				
		 				$i = 0;
		 				foreach($nameAry as $n) {
		 					if($id==1 && $i==1 &&  $issueDate!="")
		 						$footer .= "<td valign=\'bottom\' height=\'50\' width=\'20%\'>$n</td><td valign=\'bottom\'>:</td><td valign=\'bottom\' align=\'center\' class=\'td_bottom\'>$issueDate</td><td>&nbsp;&nbsp;</td>";
	 						else if($id==1 && $i==0 && $StudentID!="") {
	 							$name_field = getNameFieldByLang('USR.', 'b5');
	 							$sql = "SELECT $name_field as name FROM INTRANET_USER USR LEFT OUTER JOIN YEAR_CLASS_TEACHER yct ON (yct.UserID=USR.UserID) LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."') LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID) WHERE ycu.UserID='$StudentID' AND yc.AcademicYearID='".Get_Current_Academic_Year_ID()."'";
	 							$result = $this->returnVector($sql);
	 							
	 							$footer .= "<td valign=\'bottom\' height=\'50\' width=\'20%\'>$n</td><td valign=\'bottom\'>:</td><td valign=\'bottom\' align=\'center\' class=\'td_bottom\' width=\'30%\'>".implode(' / ',$result)."</td><td>&nbsp;&nbsp;</td>";
	 						}
	 						else
	 							$footer .= "<td valign=\'bottom\' height=\'50\' width=\'20%\'>$n</td><td valign=\'bottom\'>:</td><td valign=\'bottom\' class=\'td_bottom\' width=\'30%\'>&nbsp;</td><td>&nbsp;&nbsp;</td>";
	 						
	 						$i++;
		 				}
		 				$footer .= "</tr>";
		 			}
		 		}
		 		$footer .= "</table>";
			 }
			 //echo $footer;
			 if(sizeof($noticeIDAry)>0) {
			 	$sql = "UPDATE INTRANET_NOTICE SET Footer='$footer' WHERE NoticeID IN (".implode(',', $noticeIDAry).")";
			 	$this->db_db_query($sql);
			 }
		}
		
		function returnPushMessageData($startdate='', $enddate='', $title='', $noticeNumber='')
		{
			global $Lang;
			
			if($startdate===''){
				$startdate = $this->DateStart;
			}
			if($enddate===''){
				$enddate = $this->DateEnd;
			}
			if($title===''){
				$title = $this->Title;
			}
			if($noticeNumber===''){
				$noticeNumber = $this->NoticeNumber;
			}
			
			$website = get_website();
			$webmaster = get_webmaster();
			
			$Today = date("Y-m-d");
			if ($startdate>$Today)
			{
				$Term = $Lang['EmailNotification']['eNotice']['Term1'];
			}
			else
			{
				$Term = $Lang['EmailNotification']['eNotice']['Term2'];
			}
			
			$messageTitle = $Lang['AppNotifyMessage']['eNotice']['Subject'];
			$messageTitle = str_replace("__NOTICENUMBER__", $noticeNumber, $messageTitle);
			$content = str_replace("__TITLE__", $title, $Lang['AppNotifyMessage']['eNotice']['Content']);
			$content = str_replace("__NOTICENUMBER__", $noticeNumber, $content);
			$content = str_replace("__TERM__", $Term, $content);
			$content = str_replace("__STARTDATE__", $startdate, $content);
			$content = str_replace("__ENDDATE__", $enddate, $content);
			
			return array($messageTitle, $content);
		}
		
		function returnEmailNotificationData($startdate='', $enddate='', $title='', $senderEmail='')
		{
			global $Lang;
			
			if($startdate===''){
				$startdate = $this->DateStart;
			}
			if($enddate===''){
				$enddate = $this->DateEnd;
			}
			if($title===''){
				$title = $this->Title;
			}
			
			
			$website = get_website();
			//$webmaster = get_webmaster();
			$webmaster = $senderEmail ? $senderEmail : get_webmaster();
			
			$Today = date("Y-m-d");
			if ($startdate>$Today)
			{
				$Term = $Lang['EmailNotification']['eNotice']['Term1'];
			}
			else
			{
				$Term = $Lang['EmailNotification']['eNotice']['Term2'];
			}
			
			$email_subject = $Lang['EmailNotification']['eNotice']['Subject'] . $title;
			
			if($_SESSION["platform"] =="KIS")
			{
				$email_content = str_replace("__TITLE__", $title, $Lang['EmailNotification']['eNotice']['KIS_Content']);
			}
			else
			{
				$email_content = str_replace("__TITLE__", $title, $Lang['EmailNotification']['eNotice']['Content']);
			}
			$email_content = str_replace("__TERM__", $Term, $email_content);
			$email_content = str_replace("__STARTDATE__", $startdate, $email_content);
			$email_content = str_replace("__ENDDATE__", $enddate, $email_content);
			$email_content = str_replace("__WEBSITE__", $website."  ", $email_content);
			
			//$email_footer = $Lang['EmailNotification']['Footer'];
			$email_footer = str_replace("__WEBSITE__", $website."  ", $email_footer);
			$email_footer = str_replace("__WEBMASTER__", $webmaster."  ", $email_footer);
			$email_content .= $email_footer;
			
			return array($email_subject, $email_content);
		}
		
		function getParentNotice($child_list="")
		{
			global $UserID, $sys_custom;
			
			if(!$child_list)
			{
				# Grab children ids
				$sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
				$children = $this->returnVector($sql);
				if (sizeof($children)==0) return "";
				$child_list = implode(",",$children);
			}
			
			$name_field = getNameFieldByLang("b.");
			$sql = "SELECT
						a.NoticeID,
						a.Title,
						c.StudentID,
						c.SignerID,
						c.DateModified,
						$name_field as student_name
					FROM
                        INTRANET_NOTICE_REPLY as c
                        LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                        LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
					WHERE c.StudentID IN ($child_list) AND a.DateStart <= CURDATE()
                        AND a.DateEnd >= CURDATE()
                        AND a.RecordStatus = 1
				";
			if($sys_custom['skss']['StudentAwardReport']) {
				$sql .= " AND a.Module!='DISCIPLINE_STUDENTAWARDREPORT'";
			}
			
			return $this->returnArray($sql);
		}
		
		function displayAttachmentEdit($name, $notice_folder)
		{
			global $file_path, $image_path;
			
			$path = "$file_path/file/notice/$notice_folder";
			
			$templf = new libfiletable("", $path,0,0,"");
			$files = $templf->files;
			$x1 = "";
			
			while (list($key, $value) = each($files))
			{
				$url = str_replace($file_path, "", $path)."/".$files[$key][0];
				$x1 .= "<tr><td width=\"1\"><input type=\"checkbox\" name=\"$name\" value=\"".urlencode($files[$key][0])."\"  onClick=\"setRemoveFile($key,this.checked)\"></td>";
				$x1 .="<td id=\"a_".$key."\" class=\"tabletext\"><img src=\"$image_path/file.gif\" hspace=\"2\" vspace=\"2\" border=\"0\" align=\"absmiddle\">";
				
				$dl_url = str_replace($file_path, "", $path)."/".$files[$key][0];
				$dl_url = $file_path.$url;
				
				$x1 .= "<a class='tablelink' target=_blank href=\"/home/download_attachment.php?target_e=".getEncryptedText($dl_url)."\" >".$files[$key][0]."</a>";
				
				$x1 .= " (".ceil($files[$key][1]/1000)."Kb)";
			}
			$x1 .= "</tr>\n";
			return $x1;
		}
		
		function genAttachmentFolderName()
		{
			//$t = time();
			$t = date("YmdHis_").substr(time(), -3);
			do{
				$fn = session_id().".". $t;
				# check the foler name is exists or not
				$sql = "select count(*) from INTRANET_NOTICE where Attachment='". $fn ."'";
				$result = $this->returnVector($sql);
				$t++;
			}while ($result[0] > 0);
			
			return $fn;
		}
		
		function updateNoticeFlashUploadPath($NoticeID,$Content,$action='new'){
			global $lf,$cfg,$file_path;
			$Content = stripslashes(htmlspecialchars_decode($Content));
			
			// [2015-1103-1025-35066] prevent image cannot embed in eNotice at KIS site, replace $PATH_WRT_ROOT by $file_path
			if($action=='copy'){
				//				$Content = $lf->copy_fck_flash_image_upload_to_new_id_loc($this->NoticeID, $NoticeID, $Content, $PATH_WRT_ROOT, $cfg['fck_image']['eNotice']);
				$Content = $lf->copy_fck_flash_image_upload_to_new_id_loc($this->NoticeID, $NoticeID, $Content, $file_path, $cfg['fck_image']['eNotice']);
			}else{
				//				$Content = ($lf->copy_fck_flash_image_upload($NoticeID, $Content, $PATH_WRT_ROOT, $cfg['fck_image']['eNotice']));
				$Content = ($lf->copy_fck_flash_image_upload($NoticeID, $Content, $file_path, $cfg['fck_image']['eNotice']));
			}
			
			$Content = trim(intranet_htmlspecialchars($Content));
			$sql = "UPDATE INTRANET_NOTICE SET Description = '".$this->Get_Safe_Sql_Query($Content)."' WHERE NoticeID = '$NoticeID'";
			$this->db_db_query($sql);
		}
		
		function getUnsignList($NoticeID, $SelectedClass="")
		{
			if ($SelectedClass!="")
			{
				// [2014-1217-1446-17207] prevent sql error when teacher teach more than 1 classes
				//        		$sql_cond = " AND iu.ClassName='".addslashes($SelectedClass)."' ";
				$sql_cond = " AND iu.ClassName IN ('".str_replace(",", "','", addslashes($SelectedClass))."') ";
			}
			$sql = "SELECT nr.StudentID FROM INTRANET_NOTICE_REPLY AS nr, INTRANET_USER AS iu" .
					" WHERE nr.NoticeID='$NoticeID' AND (nr.RecordStatus='' OR nr.RecordStatus IS NULL OR nr.RecordStatus='0') " .
					" AND iu.UserID=nr.StudentID $sql_cond ";
			// AND iu.RecordStatus='1'
			
			$result = $this->returnVector($sql);
			
			return $result;
		}
		
		function isStudentInNotice($NoticeID, $StudentID)
		{
			$sql = "select count(*) from INTRANET_NOTICE_REPLY where NoticeID='$NoticeID' and StudentID='$StudentID'";
			$result = $this->returnVector($sql);
			return $result[0];
		}
		
		function rebuildNoticeReply($thisNoticeID, $original_audience, $new_audience)
		{
			# remove student
			$removed_student = array();
			foreach($original_audience as $ok=>$ou)
			{
				if( !in_array($ou, $new_audience) )
				{
					array_push($removed_student, $ou);
				}
			}
			if(!empty($removed_student))
			{
				$sql = "delete from INTRANET_NOTICE_REPLY where NoticeID=$thisNoticeID and StudentID in (". implode(",",$removed_student) .")";
				$this->db_db_query($sql) or die(mysql_error());
			}
			
			# add new student
			$username_field = getNameFieldWithClassNumberEng("");
			$new_student = array();
			foreach($new_audience as $nk=>$nu)
			{
				if( !in_array($nu, $original_audience) )
				{
					array_push($new_student, $nu);
				}
			}
			if(!empty($new_student))
			{
				$sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
					SELECT $thisNoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus in (0,1) AND UserID in (". implode(",", $new_student) .")";
				$this->db_db_query($sql) or die(mysql_error());
			}
		}
		
		function getNoticeContent($studentid="")
        {
			global $PATH_WRT_ROOT, $file_path, $sys_custom;
			
			$content = $this->Description;
			
			// [2017-1207-0941-45235]
			if($sys_custom['eNotice']['NoticeContentDecodeEntity']) {
			    $content = html_entity_decode($content, ENT_COMPAT | ENT_HTML401, 'UTF-8');
			}
			
			// [2015-1118-1620-50207] display line break for payment notice
			// [2019-0118-1125-41206] for text content not from FCKEditor
			if(strtoupper($this->Module) == "PAYMENT" && !$this->isPaymentNoticeApplyEditor) {
				$content = nl2br($content);
			}
			
			// Merge Content only
			if($this->ContentType==2 && $studentid!=""){
				include_once($PATH_WRT_ROOT."includes/libimporttext.php");
				$limport = new libimporttext();
				
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$luser = new libuser($studentid);
				
				$ay_id = getAcademicYearAndYearTermByDate($this->DateStart);
				$ay_id = $ay_id["AcademicYearID"];
				
				// get csv content
				$csvpath = "$file_path/file/mergenotice/".ceil($this->NoticeID/10000)."/".$this->NoticeID."/".$this->MergeFile;
				if(file_exists($csvpath)){
					$data = $limport->GET_IMPORT_TXT($csvpath);
				}
				
				if($data!="" && is_array($data) && count($data)>0){
					// Format 1: one person per row
					if($this->MergeFormat!=2){
						// data header
						$data_col = array_shift($data);
						
						// Class and ClassNumber in CSV
						if($this->MergeType!=2){
							$luser_info = $luser->getStudentInfoByAcademicYear($ay_id);
							$this_user_info = $luser_info[0];
							
							$data = BuildMultiKeyAssoc($data, array(0, 1));
							$data = $data[$this_user_info["ClassNameEn"]][$this_user_info["ClassNumber"]]? $data[$this_user_info["ClassNameEn"]][$this_user_info["ClassNumber"]] : $data[$this_user_info["ClassNameCh"]][$this_user_info["ClassNumber"]];
						}
						// UserLogin in CSV
						else{
							$data = BuildMultiKeyAssoc($data, array(0));
							$data = $data[$luser->UserLogin];
						}
						
						// Merge data
						for ($i=0; $i<sizeof($data_col); $i++)
						{
							$dat = $data[$i];
							if ($dat=="" || $dat=="---")
							{
								$dat = "--";
							}
							$content = str_ireplace("[=".$data_col[$i]."=]", $dat, $content);
						}
					}
					// Format 2: multi-rows data per person
					else{
						// csv header
						$csv_col = array_shift($data);
						
						// Class and ClassNumber in CSV
						if($this->MergeType!=2){
							$luser_info = $luser->getStudentInfoByAcademicYear($ay_id);
							$this_user_info = $luser_info[0];
							
							// [EJ-DM#649] for same content, fixed data will only display once
							//$data = BuildMultiKeyAssoc($data, array(0, 1, 3, 4));
							$data = BuildMultiKeyAssoc($data, array(0, 1, 3), array(), 0, 1);
							$data = $data[$this_user_info["ClassNameEn"]][$this_user_info["ClassNumber"]]? $data[$this_user_info["ClassNameEn"]][$this_user_info["ClassNumber"]] : $data[$this_user_info["ClassNameCh"]][$this_user_info["ClassNumber"]];
							$data_loc = 4;
						}
						// UserLogin in CSV
						else{
							// [EJ-DM#649] for same content, fixed data will only display once
							//$data = BuildMultiKeyAssoc($data, array(0, 2, 3));
							$data = BuildMultiKeyAssoc($data, array(0, 2), array(), 0, 1);
							$data = $data[$luser->UserLogin];
							$data_loc = 3;
						}
						
						// data header
						$data_col = array_keys((array)$data);
						
						// Merge data
						$getStudentInfo = false;
						if(is_array($data_col)){
							// loop content type
							for ($i=0; $i<sizeof($data_col); $i++)
							{
								$dkey = $data_col[$i];
								$dat = $data[$dkey];
								
								// Empty
								$currentData = "";
								if ($dat=="" || !is_array($dat) || empty($dat))
								{
									$currentData = "--";
								}
								// Merge multiple content
								else
								{
									$delim = "";
									// [EJ-DM#649]
									//foreach((array)$dat as $currentcontent => $dataDetails){
									for($dCount=0; $dCount<count($dat); $dCount++){
										$currentcontent = $dat[$dCount][$data_loc];
										if ($currentcontent=="" || $currentcontent=="---")
										{
											$currentcontent = "--";
										}
										$currentData .= $delim.$currentcontent;
										$delim = "<br/>";
										
										// Merge Stundet Info
										if(!$getStudentInfo){
											if($this->MergeType!=2){
												$content = str_ireplace("[=".$csv_col[0]."=]", $dat[$dCount][0], $content);
												$content = str_ireplace("[=".$csv_col[1]."=]", $dat[$dCount][1], $content);
												$content = str_ireplace("[=".$csv_col[2]."=]", $dat[$dCount][2], $content);
											}
											// UserLogin in CSV
											else{
												$content = str_ireplace("[=".$csv_col[0]."=]", $dat[$dCount][0], $content);
												$content = str_ireplace("[=".$csv_col[1]."=]", $dat[$dCount][1], $content);
											}
											$getStudentInfo = true;
										}
									}
								}
								$content = str_ireplace("[=".$dkey."=]", $currentData, $content);
							}
						}
					}
				}
			}

			// [2020-1102-1704-16073]
			$skipCleanHtmlLogic = false;
			if(isset($sys_custom['eNotice']['SkipCleanHtmlLogicNoticeIds']) && $this->NoticeID != "")
            {
			    $SkipCleanHtmlLogicNoticeIds = $sys_custom['eNotice']['SkipCleanHtmlLogicNoticeIds'];
                if(!empty($SkipCleanHtmlLogicNoticeIds)) {
                    $skipCleanHtmlLogic = in_array($this->NoticeID, (array)$SkipCleanHtmlLogicNoticeIds);
                }
            }

			if(!$this->IsModule && !$skipCleanHtmlLogic) {
    			$content = html_entity_decode($content, ENT_COMPAT | ENT_HTML401, 'UTF-8');
    			$content = htmlentities(cleanHtmlJavascript($content), ENT_COMPAT | ENT_HTML401, 'UTF-8');
			}

			return $content;
		}
		
		function getNoticeCSVUserInfo($previewCSV = false, $filePathAndName = "") {
			global $PATH_WRT_ROOT, $file_path;
			
			include_once($PATH_WRT_ROOT."includes/libimporttext.php");
			$limport = new libimporttext();
			
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$luser = new libuser($studentid);
			
			$ay_id = getAcademicYearAndYearTermByDate($this->DateStart);
			$ay_id = $ay_id["AcademicYearID"];
			
			// Get CSV Content
			if($previewCSV) {
				$csvpath = "$file_path/file/mergenotice/temp/".$filePathAndName;
			}
			else {
				$csvpath = "$file_path/file/mergenotice/".ceil($this->NoticeID/10000)."/".$this->NoticeID."/".$this->MergeFile;
			}
			if(file_exists($csvpath)) {
				$data = $limport->GET_IMPORT_TXT($csvpath);
			}
			
			$outputData = array();
			if($data!="" && is_array($data) && count($data)>0) {
				$i = 0;
				$isUserLogin = false;
				$scheme = array();
				foreach ($data as $linekey => $lineRec) {
					if ($linekey == 0) {
						if (count($lineRec) > 0) {
							foreach ($lineRec as $kk => $vv) {
								$scheme[$kk] = strtoupper($vv);
							}
							if (in_array("USERLOGIN", $scheme)) {
								$isUserLogin = true;
							}
						}
					}
					else {
						if (count($lineRec) > 0) {
							foreach ($lineRec as $kk => $vv) {
								$csvData[$i][$scheme[$kk]] = $vv;
							}
							if ($isUserLogin) {
								$userLoginArr[$csvData[$i]["USERLOGIN"]] = $csvData[$i];
							}
							else {
								$userClassArr[strtolower($csvData[$i]["CLASS"] . $csvData[$i]["CLASSNUMBER"])] = $csvData[$i];
							}
							$i++;
						}
					}
				}
				
				$result = array();
				if ($isUserLogin) {
					if (count($userLoginArr) > 0)
					{
                        $noticeUserLoginArr = array_keys($userLoginArr);

						$strSQL = "	SELECT
										UserID, UserLogin
									FROM
										INTRANET_USER
									WHERE
										RecordStatus = 1 AND RecordType = 2 AND
										UserLogin IN ('".implode("', '", array_keys($userLoginArr))."')
									ORDER BY
										ClassName, ClassNumber+0";
						$result = $this->returnResultSet($strSQL);

						// [2020-0731-1529-14235] UserLogin - must be case sensitive
                        $newResult = array();
                        foreach((array)$result as $thisResult)
                        {
                            $thisUserLogin = $thisResult['UserLogin'];
                            if($thisUserLogin != '' && in_array($thisUserLogin, $noticeUserLoginArr)) {
                                $newResult[] = $thisResult;
                            }
                        }
                        $result = $newResult;
					}
				}
				else {
					if (count($userClassArr) > 0) {
						/*
						 * [2017-0711-1820-12206]
						 * 1. LOWER(iu.ClassName), iu.classNumber		 - INTRANET_USER 	(English Class Name only)
						 * 2. LOWER(yc.ClassTitleB5/EN), ycu.classNumber - YEAR_CLASS 		(Current Year Chinese / English Class Name)
						 */
						$strSQL = "	SELECT
										iu.UserID, iu.UserLogin
									FROM
										INTRANET_USER AS iu
									LEFT JOIN
										YEAR_CLASS_USER AS ycu ON (ycu.UserID = iu.UserID)
									LEFT JOIN
										YEAR_CLASS AS yc ON (ycu.YearClassID = yc.YearClassID and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."')
									WHERE
										iu.RecordStatus = 1 AND RecordType = 2 AND
										(
											CONCAT(LOWER(iu.ClassName), iu.classNumber) IN ('".implode("', '", array_keys($userClassArr))."') OR
											CONCAT(LOWER(yc.ClassTitleEN), ycu.classNumber) IN ('".implode("', '", array_keys($userClassArr))."') OR
											CONCAT(LOWER(yc.ClassTitleB5), ycu.classNumber) IN ('".implode("', '", array_keys($userClassArr))."')
										)
									GROUP BY
										iu.UserID
									ORDER BY
										iu.ClassName, iu.ClassNumber+0 ";
						$result = $this->returnResultSet($strSQL);
					}
				}
				
				if (count($result) > 0) {
					foreach ($result as $kk => $vv) {
						$outputData[] = $vv["UserID"];
					}
				}
			}
			return $outputData;
		}
		
		function previewNoticeContent($studentid="", $PreviewCSVFile="", $PreviewDescription="", $ContentType, $MergeFormat, $MergeType){
			global $PATH_WRT_ROOT, $file_path;
			
			$content = $PreviewDescription;
			
			// Merge Content only
			if($ContentType==2 && $studentid!=""){
				include_once($PATH_WRT_ROOT."includes/libimporttext.php");
				$limport = new libimporttext();
				
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				$luser = new libuser($studentid);
				
				$ay_id = Get_Current_Academic_Year_ID();
				
				// get csv content
				if($PreviewCSVFile!="")
					$data = $limport->GET_IMPORT_TXT($PreviewCSVFile);
				else{
					$csvpath = "$file_path/file/mergenotice/".ceil($this->NoticeID/10000)."/".$this->NoticeID."/".$this->MergeFile;
					if(file_exists($csvpath)){
						$data = $limport->GET_IMPORT_TXT($csvpath);
					}
				}
					
				if($data!="" && is_array($data) && count($data)>0){
					// Format 1: one person per row
					if($MergeFormat!=2){
						// data header
						$data_col = array_shift($data);
						
						// Class and ClassNumber in CSV
						if($MergeType != 2){
							$luser_info = $luser->getStudentInfoByAcademicYear($ay_id);
							$this_user_info = $luser_info[0];
							
							$data = BuildMultiKeyAssoc($data, array(0, 1));
							$data = $data[$this_user_info["ClassNameEn"]][$this_user_info["ClassNumber"]]? $data[$this_user_info["ClassNameEn"]][$this_user_info["ClassNumber"]] : $data[$this_user_info["ClassNameCh"]][$this_user_info["ClassNumber"]];
						}
						// UserLogin in CSV
						else{
							$data = BuildMultiKeyAssoc($data, array(0));
							$data = $data[$luser->UserLogin];
						}
						
						// Merge data
						for ($i=0; $i<sizeof($data_col); $i++)
						{
							$dat = $data[$i];
							if ($dat=="" || $dat=="---")
							{
								$dat = "--";
							}
							$content = str_ireplace("[=".$data_col[$i]."=]", "<span style='background:#88FF88;'>".$dat."</span>", $content);
						}
					}
					// Format 2: multi-rows data per person
					else{
						// csv header
						$csv_col = array_shift($data);
						
						// Class and ClassNumber in CSV
						if($MergeType!=2){
							$luser_info = $luser->getStudentInfoByAcademicYear($ay_id);
							$this_user_info = $luser_info[0];
							
							// [EJ-DM#649] for same content, fixed data will only display once
							//$data = BuildMultiKeyAssoc($data, array(0, 1, 3, 4));
							$data = BuildMultiKeyAssoc($data, array(0, 1, 3), array(), 0, 1);
							$data = $data[$this_user_info["ClassNameEn"]][$this_user_info["ClassNumber"]]? $data[$this_user_info["ClassNameEn"]][$this_user_info["ClassNumber"]] : $data[$this_user_info["ClassNameCh"]][$this_user_info["ClassNumber"]];
							$data_loc = 4;
						}
						// UserLogin in CSV
						else{
							// [EJ-DM#649] for same content, fixed data will only display once
							//$data = BuildMultiKeyAssoc($data, array(0, 2, 3));
							$data = BuildMultiKeyAssoc($data, array(0, 2), array(), 0, 1);
							$data = $data[$luser->UserLogin];
							$data_loc = 3;
						}
						
						// data header
						$data_col = array_keys((array)$data);
						
						// Merge data
						$getStudentInfo = false;
						if(is_array($data_col)){
							// loop content type
							for ($i=0; $i<sizeof($data_col); $i++)
							{
								$dkey = $data_col[$i];
								$dat = $data[$dkey];
								
								$currentData = "";
								
								// Empty
								if ($dat=="" || !is_array($dat) || empty($dat))
								{
									$currentData = "--";
								}
								// Merge multiple content
								else
								{
									$delim = "";
									// [EJ-DM#649]
									//foreach((array)$dat as $currentcontent => $dataDetails){
									for($dCount=0; $dCount<count($dat); $dCount++){
										$currentcontent = $dat[$dCount][$data_loc];
										if ($currentcontent=="" || $currentcontent=="---")
										{
											$currentcontent = "--";
										}
										$currentData .= $delim.$currentcontent;
										$delim = "<br/>";
										
										// Merge Stundet Info
										if(!$getStudentInfo){
											if($MergeType!=2){
												$content = str_ireplace("[=".$csv_col[0]."=]", "<span style='background:#88FF88;'>".$dat[$dCount][0]."</span>", $content);
												$content = str_ireplace("[=".$csv_col[1]."=]", "<span style='background:#88FF88;'>".$dat[$dCount][1]."</span>", $content);
												$content = str_ireplace("[=".$csv_col[2]."=]", "<span style='background:#88FF88;'>".$dat[$dCount][2]."</span>", $content);
											}
											// UserLogin in CSV
											else{
												$content = str_ireplace("[=".$csv_col[0]."=]", "<span style='background:#88FF88;'>".$dat[$dCount][0]."</span>", $content);
												$content = str_ireplace("[=".$csv_col[1]."=]", "<span style='background:#88FF88;'>".$dat[$dCount][1]."</span>", $content);
											}
											$getStudentInfo = true;
										}
									}
								}
								$content = str_ireplace("[=".$dkey."=]", "<span style='background:#88FF88;'>".$currentData."</span>", $content);
							}
						}
					}
				}
					
				// Display fail to merge content
				$ContentArr = explode("[=", $content);
				$unMerged = "";
				if (sizeof($ContentArr)>1)
				{
					for ($i=0; $i<sizeof($ContentArr); $i++)
					{
						$TagCloseArr = explode("=]", $ContentArr[$i]);
						if (sizeof($TagCloseArr)>1)
						{
							for ($k=0; $k<sizeof($TagCloseArr); $k++)
							{
								if ($k==0)
								{
									$unMerged .= "<span style='background:#FF6666;'>[=".$TagCloseArr[$k]."=]</span>";
								}
								else
								{
									$unMerged .= $TagCloseArr[$k];
								}
							}
						}
						else
						{
							$unMerged .= $ContentArr[$i];
						}
					}
					$content = $unMerged;
				}
				else
				{
					# no tag left!
				}
			}
			return $content;
		}
		
		function getApprovalUserInfo($notice_type='')
        {
            // [2020-0604-1821-16170] module type for approval notice
            if($notice_type != '') {
                $cond = $notice_type == NOTICE_SETTING_TYPE_PAYMENT ? " WHERE Module = 'Payment' " : " WHERE Module IS NULL OR Module = '' ";
            }

			/**
			 * NOTE: only get approval user info from INTRANET_NOTICE_APPROVAL_USER Only!!!!
			 * Admin Group is excluded!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			 */
			$sql = "SELECT 
                        iu.UserID, iu.UserLogin, iu.EnglishName, iu.ChineseName
					FROM 
					    INTRANET_NOTICE_APPROVAL_USER AS inau
					    INNER JOIN INTRANET_USER AS iu ON (iu.UserID=inau.UserID)
					$cond ";
			return $this->returnResultSet($sql);
		}
		
		function updateApprovalUserInfo($approvalUserIDArray, $notice_type)
        {
			global $UserID;

            // [2020-0604-1821-16170] module type for approval notice
            $moduleValue = $notice_type == NOTICE_SETTING_TYPE_PAYMENT ? "'Payment'" : "NULL";
            $cond = $notice_type == NOTICE_SETTING_TYPE_PAYMENT ? " AND Module = 'Payment' " : " AND (Module IS NULL OR Module = '') ";

            // Step 1 : Delete
			$sql = "DELETE FROM INTRANET_NOTICE_APPROVAL_USER 
                    WHERE UserID IN ( 
                        SELECT * FROM (
						    SELECT UserID FROM INTRANET_NOTICE_APPROVAL_USER AS b 
						    WHERE b.UserID NOT IN ('".implode("','",$approvalUserIDArray)."') $cond
                        )
                    as t)";
			$this->db_db_query($sql);

			foreach($approvalUserIDArray as $approvalUser)
			{
				// Step 2 : Insert
				$sql = "INSERT INTO INTRANET_NOTICE_APPROVAL_USER (UserID, InputUser, Module, DateInput)
							SELECT * FROM (
							    SELECT '$approvalUser' AS au, '$UserID' AS u, $moduleValue AS m, now() AS di) AS tmp
							    WHERE NOT EXISTS (
								    SELECT UserID FROM INTRANET_NOTICE_APPROVAL_USER WHERE UserID = '$approvalUser' $cond
							)";
				$this->db_db_query($sql);
			}
			
			return $this->getApprovalUserInfo($notice_type);
		}
		
		//20160519 Kenneth
		function isApprovalUser($userID='')
        {
			global $UserID;

			if($userID === '') {
				$userID = $UserID;
			}

			$sql = "SELECT * FROM INTRANET_NOTICE_APPROVAL_USER WHERE UserID = '$userID'";
			$returnAry = $this->returnResultSet($sql);
			if(empty($returnAry)){
				return false;
			}else{
				return true;
			}
		}

        function isSchoolNoticeApprovalUser($userID='')
        {
            global $UserID;

            if($userID === '') {
                $userID = $UserID;
            }

            $sql = "SELECT * FROM INTRANET_NOTICE_APPROVAL_USER WHERE UserID = '$userID' AND (Module IS NULL OR Module = '') ";
            $returnAry = $this->returnResultSet($sql);
            if(empty($returnAry)){
                return false;
            }else{
                return true;
            }
        }

        function isPaymentNoticeApprovalUser($userID='')
        {
            global $UserID;

            if($userID === '') {
                $userID = $UserID;
            }

            $sql = "SELECT * FROM INTRANET_NOTICE_APPROVAL_USER WHERE UserID = '$userID' AND Module = 'Payment'";
            $returnAry = $this->returnResultSet($sql);
            if(empty($returnAry)){
                return false;
            }else{
                return true;
            }
        }
		
		//20160603 Kenneth
		function getNotifiedStudentList($NoticeID='')
        {
			if($NoticeID===''){
				$NoticeID = $this->NoticeID;
			}
			$sql = "SELECT DISTINCT StudentID FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$NoticeID'";
			return $this->returnVector($sql);
		}
		
		function sendApprovalNotice($approval=true, $comment='')
        {
			global $plugin,$Lang,$PATH_WRT_ROOT;

			//get noticed users
			$notifiedUser = $this->IssueUserID;
			$emailEnable = true;
			$eClassAppEnable = $plugin['eClassTeacherApp'];
			
			if($approval){
				$emailContent['Heading'] = $Lang['eNotice']['ApprovalMsg']['email']['Title'];
				$emailContent['Content'] = $Lang['eNotice']['ApprovalMsg']['email']['Content'];
				$appContent['Heading'] = $Lang['eNotice']['ApprovalMsg']['eClassApp']['Title'];
				$appContent['Content'] = $Lang['eNotice']['ApprovalMsg']['eClassApp']['Content'];
			}else{
				$emailContent['Heading'] = $Lang['eNotice']['RejectMsg']['email']['Title'];
				$emailContent['Content'] = $Lang['eNotice']['RejectMsg']['email']['Content'];
				$appContent['Heading'] = $Lang['eNotice']['RejectMsg']['eClassApp']['Title'];
				$appContent['Content'] = $Lang['eNotice']['RejectMsg']['eClassApp']['Content'];
			}

			$emailContent['Content'] = str_replace('<--NoticeNumber-->',$this->NoticeNumber,$emailContent['Content']);
			$emailContent['Content'] = str_replace('<--Title-->',$this->Title,$emailContent['Content']);
			$emailContent['Content'] = str_replace('<--DateStart-->',$this->DateStart,$emailContent['Content']);
			$emailContent['Content'] = str_replace('<--ApprovalComment-->',$comment,$emailContent['Content']);

			$appContent['Content'] = str_replace('<--NoticeNumber-->',$this->NoticeNumber,$appContent['Content']);
			$appContent['Content'] = str_replace('<--Title-->',$this->Title,$appContent['Content']);
			$appContent['Content'] = str_replace('<--DateStart-->',$this->DateStart,$appContent['Content']);
			$appContent['Content'] = str_replace('<--ApprovalComment-->',$comment,$appContent['Content']);
			
			// eClassApp Push msg
			if($eClassAppEnable){
				include_once($PATH_WRT_ROOT.'includes/eClassApp/eClassAppConfig.inc.php');
				include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
				
				$individualMessageInfoAry = array();
				$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = array($notifiedUser=>$notifiedUser);
				$leClassApp = new libeClassApp();
//				$leClassApp->sendPushMessage($individualMessageInfoAry, $appContent['Heading'], $appContent['Content'], $isPublic='', $recordStatus=1, $appType='T', $sendTimeMode='now', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='eNotice', $moduleRecordID=$this->NoticeID, $createRecordOnly=false);
				$leClassApp->sendPushMessage($individualMessageInfoAry, $appContent['Heading'], $appContent['Content'], $isPublic='', $recordStatus=1, $appType='T', $sendTimeMode='now', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='', $moduleRecordID=$this->NoticeID, $createRecordOnly=false);
			}
			
			if($emailEnable){
				include_once($PATH_WRT_ROOT."includes/libemail.php");
				include_once($PATH_WRT_ROOT."includes/libsendmail.php");
				include_once($PATH_WRT_ROOT."includes/libwebmail.php");
				$lwebmail = new libwebmail();
				$lwebmail->sendModuleMail((array)$notifiedUser,$emailContent['Heading'],$emailContent['Content'],1,'','User',true);
			}
		}
		
		#########################################################################################
		# Amway Customized eNotice  [Start]
		#########################################################################################
		
		// Filter: Target for student, unexpired record, distributed (RecordStatus = 1)
		function getCusteNotice($UserID, $CountRecordOnly=false)
        {
			if ($CountRecordOnly) {
				$fields = "COUNT(*) AS NumOfRec";
				$limit = "";
			}
			else {
				$fields = "a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d') as DateStart,
                            DATE_FORMAT(a.DateEnd,'%Y-%m-%d') as DateEnd, if(a.IsModule=1,'--',a.NoticeNumber) as NoticeNumber,
							a.Title, a.Attachment, c.RecordStatus";
				$limit = " LIMIT 7";
			}
		    if(!$this->isLateSignAllow){
		     	$date_cond = " AND a.DateEnd >= CURDATE()";
	     	}
	     	else {
	     		$date_cond = "";
	     	}

			$sql = "SELECT 	{$fields}
					FROM INTRANET_NOTICE_REPLY as c
						LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
						LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
						LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
					WHERE
						c.StudentID = '$UserID'
						AND a.RecordStatus = 1
						AND a.DateStart <= NOW() 
						{$date_cond}
						AND a.IsDeleted=0 and a.TargetType='S'
					ORDER BY a.DateStart DESC {$limit}";
				$rs = $this->returnArray($sql,null,1);	// associated array only

			return $rs;
		}
		
		// Filter: Target for staff, unexpired record, distributed (RecordStatus = 1)
		// don't show new for staff as he/she should not sign eNotice (return RecordStatus=1)
		function getCusteNoticeForStaff($CountRecordOnly=false)
        {
			if ($CountRecordOnly) {
				$fields = "COUNT(*) AS NumOfRec";
				$limit = "";
			}
			else {
				$fields = "a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d') as DateStart,
                            DATE_FORMAT(a.DateEnd,'%Y-%m-%d') as DateEnd, if(a.IsModule=1,'--',a.NoticeNumber) as NoticeNumber,
							a.Title, a.Attachment, '1' as RecordStatus";
				$limit = " LIMIT 7";
			}
			$sql = "SELECT 	{$fields}
					FROM INTRANET_NOTICE as a
					WHERE
						a.RecordStatus = 1
						AND a.DateStart <= NOW() AND a.DateEnd >= CURDATE()
						AND a.IsDeleted=0 and a.TargetType='S'
					ORDER BY a.DateStart DESC {$limit}";
			$rs = $this->returnArray($sql,null,1);	// associated array only
			
			return $rs;
		}
		
		// Filter: Target for student, unexpired record
		function displayCusteNotice($UserID,$IsStaff=false)
		{
			global $i_no_record_exists_msg, $Lang;

			if ($IsStaff) {
				$rs = $this->getCusteNoticeForStaff();
			}
			else {
				$rs = $this->getCusteNotice($UserID);
			}
			$iMax = count($rs);
			$x = "";
			// if ($iMax == 0)
			//	{
			//		$x .= "<div class=\"portal_module_list indextabclassiconoff\">$i_no_record_exists_msg</div>\n";
			//	}
			//	else
			//	{
			for($i=0; $i<$iMax; $i++)
			{
				$r = $rs[$i];
				$NoticeID 		= $r['NoticeID'];
				$DateStart 		= $r['DateStart'];
				$DateEnd 		= $r['DateEnd'];
				$NoticeNumber 	= $r['NoticeNumber'];
				$Title 			= $r['Title'];
				$Attachment 	= $r['Attachment'];
				$RecordStatus 	= $r['RecordStatus'];
				$clip = ($Attachment == ""? "":"<span class=\"icon_attachment\"></span>");
				
				$x .= " <div class=\"portal_module_list".(($RecordStatus == 0)?" portal_module_list_new":"")."\">
				            <span class=\"module_icon icon_enotice\"></span>
				            <div class=\"module_group\">
				            	<div class=\"module_group_right\">".
					            	(($RecordStatus == 0)?"<span class=\"icon_new\"></span>":"").
					            	"<span class=\"end_date\"><em>".$Lang['Portal']['eNotice']['DueOn'].": </em>{$DateEnd}</span>
				            	</div>
				            	<div class=\"module_name\">
					            	<a href=\"javascript:sign($NoticeID, $UserID)\">[{$NoticeNumber}]{$Title}{$clip}</a>
					            	<em>".$Lang['Portal']['eNotice']['IssuedOn'].": {$DateStart}</em>
				            	</div>
			            	</div>
			            	<p class=\"spacer\"></p>
		            	</div>";
			}
			
			$x .= "<div class=\"portal_module_list_more\"><a href=\"#\" onclick='javascript:allNotice()'>".$Lang['Portal']['eNotice']['DisplayAll']."</a></div>";
			//	}
			return $x;
		}
		
		#########################################################################################
		# Amway Customized eNotice  [End]
		#########################################################################################
		
		function returnPICNoticeByKeyword($keyword)
        {
		    //selcet notice > pic njoin user where name like keyword
		    //return ventor
		    $namefield = getNameFieldByLang("IU.");
		    $sql = "SELECT PIC.NoticeID
                     FROM INTRANET_NOTICE_PIC PIC INNER JOIN INTRANET_USER AS IU ON (PIC.PICUserID = IU.UserID)
                     WHERE ((IU.EnglishName like '%".$keyword."%') OR (IU.ChineseName like '%".$keyword."%'))";
		    $result = $this->returnVector($sql);
		    return $result;
		}
		
		function getReplySlipModifyAnswerRecords($data)
		{
			$conds = "";
			$more_fields = "";
			$joins = "";
			$order_by = "";
			
			if(isset($data['RecordID'])){
				$conds .= " AND r.RecordID ";
				if(is_array($data['RecordID'])){
					$conds .= " IN (".implode(",",IntegerSafe($data['RecordID'])).") ";
				}else{
					$conds .= "='".IntegerSafe($data['RecordID'])."' ";
				}
			}
			
			if(isset($data['NoticeID'])){
				$conds .= " AND r.NoticeID ";
				if(is_array($data['NoticeID'])){
					$conds .= " IN (".implode(",",IntegerSafe($data['NoticeID'])).") ";
				}else{
					$conds .= "='".IntegerSafe($data['NoticeID'])."' ";
				}
			}
			
			if(isset($data['StudentID'])){
				$conds .= " AND r.StudentID ";
				if(is_array($data['StudentID'])){
					$conds .= " IN (".implode(",",IntegerSafe($data['StudentID'])).") ";
				}else{
					$conds .= "='".IntegerSafe($data['StudentID'])."' ";
				}
			}
			
			if(isset($data['GetModifierUserInfo']) && $data['GetModifierUserInfo']){
				$name_field = getNameFieldByLang2("u.");
				$more_fields .= ",u.UserLogin,u.EnglishName,u.ChineseName,$name_field as ModifierUserName ";
				$joins .= " LEFT JOIN INTRANET_USER as u ON u.UserID=r.InputBy ";
				$conds .= " AND u.RecordType='".USERTYPE_STAFF."' ";
			}
			
			if(isset($data['OrderByClause']) && $data['OrderByClause']!=''){
				$order_by = $data['OrderByClause'];
			}else{
				$order_by = " ORDER BY r.InputDate ";
			}
			
			$sql = "SELECT r.* $more_fields FROM INTRANET_NOTICE_MODIFY_ANSWER_RECORD as r $joins WHERE 1 ".$conds.$order_by;
			//debug_pr($sql);
			$records = $this->returnResultSet($sql);
			return $records;
		}
		
		function upsertReplySlipModifyAnswerRecord($map)
		{
			$fields = array('RecordID','NoticeID','StudentID','OldAnswer','NewAnswer');
			$input_by_user_id = isset($map['InputBy']) && $map['InputBy']>0 ? $map['InputBy'] : $_SESSION['UserID'];
			
			if(count($map) == 0) return false;
			
			if(isset($map['RecordID']) && $map['RecordID']!='' && $map['RecordID'] > 0)
			{
				$sql = "UPDATE INTRANET_NOTICE_MODIFY_ANSWER_RECORD SET ";
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields) || $key == 'RecordID') continue;
					
					$sql .= $sep."$key='".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$sql.= " WHERE RecordID='".$map['RecordID']."'";
				$success = $this->db_db_query($sql);
			}
			else
            {
				$keys = '';
				$values = '';
				$sep = "";
				foreach($map as $key => $val){
					if(!in_array($key, $fields)) continue;
					$keys .= $sep."$key";
					$values .= $sep."'".$this->Get_Safe_Sql_Query(trim($val))."'";
					$sep = ",";
				}
				$keys .= ",InputDate,InputBy";
				$values .= ",NOW(),'$input_by_user_id'";
				
				$sql = "INSERT INTO INTRANET_NOTICE_MODIFY_ANSWER_RECORD ($keys) VALUES ($values)";
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
	}
}        // End of directive
?>