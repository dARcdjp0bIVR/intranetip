<?php 
/*
  * Using:
  *
  * Date:	2020-11-13 (Bill)	[2020-1009-1753-46096]
  * 		modified assignRecordNoticeByAPI(), assignMeritNoticeByAPI(), updateMeritRecordStatusByAPI(), to delay notice start date & end date   ($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
  *
  * Date:   2020-06-22 (Bill)   [2019-1105-1700-25235]
  *         modified insertMeritRecordByAPI(), updateMeritRecordByAPI(), support AP Subject Name settings
  *         added getSubjectNameDropDownOption()
 */

class libdisciplinev12_app_api extends libdisciplinev12
{
	private $UserID;
	private $ImagePath;
	
	# Construtor
	public function libdisciplinev12_app_api()
	{
		$this->libdisciplinev12();
		$this->UserID = $_SESSION["UserID"];
	}
	
	# Store Image Path
	public function setDisciplineImagePath($path)
	{
		$this->ImagePath = $path;
	}
	
	private function isEJ()
	{
		global $junior_mck;
		return isset($junior_mck);
	}
	
	# Get Conduct Records
	public function GetConductRecord($condAry=array(), $targetID="")
	{
		if($targetID) {
			$condAry["RecordID"] = $targetID;
		}
		$ConductRecordArr = $this->GetConductRecordInfo($condAry);
		return $ConductRecordArr;
	}
	private function GetConductRecordInfo($condAry=array())
	{
		global $i_Discipline_GoodConduct, $i_Discipline_Misconduct;
		global $i_Discipline_System_Award_Punishment_Pending, $i_Discipline_System_Award_Punishment_Approved, $i_Discipline_System_Award_Punishment_Rejected, $i_Discipline_System_Award_Punishment_Waived;
		
		if(!$this->isEJ())
		{
			# Settings
			$use_intranet_homework = $this->accumulativeUseIntranetSubjectList();
			$CurrentYearID = Get_Current_Academic_Year_ID();
			$targetSchoolYear = $CurrentYearID;
			
			# Fields
			$clsName = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
			$sbjName = Get_Lang_Selection("SUB.CH_DES", "SUB.EN_DES");
			$stuName = getNamefieldByLang("b.");
			
			# Conditions
			$conds = "";
			$currentYearConds = " AND yc.AcademicYearID = '".$CurrentYearID."' ";
			if(!empty($condAry))
			{
				# Record Condition
				$targetRecordID = $condAry["RecordID"];
				if($targetRecordID != "" && $targetRecordID != 0) {
				    $conds .= " AND a.RecordID = '".$targetRecordID."'";
				}
				
				# Year Condition
				$targetSchoolYear = $condAry["selectYear"];
				$targetSchoolYear = ($targetSchoolYear == "") ? $CurrentYearID : $targetSchoolYear;
				if($targetSchoolYear != "" && $targetSchoolYear != 0 && $conds=="") {
					$conds .= " AND a.AcademicYearID = '".$targetSchoolYear."' ";
				}
			
				# Semester Condition
				$targetSemester = $condAry["selectSemester"];
				if($targetSemester != "" && $targetSemester != 0 && $targetSemester != "WholeYear") {
					$conds .= " AND a.YearTermID = '".$targetSemester."' ";
				}
				
				# Class Condition
				$targetClass = $condAry["selectClass"];
				if ($targetClass != "" && $targetClass != "0") {
				    if(is_numeric($targetClass)) {
				    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID = '".$targetClass."'";
				    	$tempClass = $this->returnVector($sql);
				    	$conds .= (sizeof($tempClass) > 0) ? " AND yc.YearClassID IN (".implode(",", $tempClass).")" : "";
					}
				    else {
						$conds .= " AND yc.YearClassID = '".substr($targetClass, 2)."'";
				    }
				}
				
				# Class / Subject Teacher - View Condition
				$targetOwnClassOnly = $condAry["ViewOwnClassOnly"];
				if($targetOwnClassOnly) {
					$currentYearConds = "  AND yc.AcademicYearID = '".$targetSchoolYear."' ";
					
					// Get Class and Subject Students
					$ClassStudentIDAry = $this->getStudentListByClassTeacherID("", $_SESSION["UserID"], $targetSchoolYear);
					$SubjectStudentIDAry = $this->getStudentListBySubjectTeacherID("", $_SESSION["UserID"], $targetSchoolYear);
					$StudentAry = array_merge($ClassStudentIDAry, $SubjectStudentIDAry);
					$StudentAry = array_unique($StudentAry);
					$conds .= " AND a.StudentID IN (".implode(",", (array)$StudentAry).") ";
				}
				
				# PIC Condition
				$targetPIC = $condAry["selectPIC"];
				if($targetPIC != "") {
					$conds .= " AND CONCAT(',', a.PICID, ',') LIKE '%,".$targetPIC.",%'";
				}
			}
			
			// Get Conduct Records
			$sql = "	SELECT
							a.RecordID, 
							a.StudentID, 
							".$stuName." as StudentName,
							CONCAT(".$clsName.", ' - ', ycu.ClassNumber) as ClassNameNum,
							LEFT(a.RecordDate, 10) as RecordDate,
							a.CategoryID, ";
			if($use_intranet_homework) {
				$sql .= " 	IF(a.CategoryID = ".PRESET_CATEGORY_HOMEWORK.", SUB.RecordID, d.ItemID) as ItemID, ";
				$sql .= " 	IF(a.CategoryID = ".PRESET_CATEGORY_HOMEWORK.", CONCAT(c.Name, ' - ', ".$sbjName."), IF(d.Name IS NULL, c.Name, CONCAT(c.Name, ' - ', d.Name))) as ItemName, ";
				$check_cat = " AND a.CategoryID != ".PRESET_CATEGORY_HOMEWORK." ";
			}
			else {
				$sql .= " 	d.ItemID as ItemID, ";
				$sql .= " 	IF(d.Name IS NULL, c.Name, CONCAT(c.Name, ' - ', d.Name)) as ItemName, ";
				$check_cat = "";
			}
			$sql .= "		a.RecordType,
							IF(a.RecordType = ".GOOD_CONDUCT.", '".$i_Discipline_GoodConduct."', '".$i_Discipline_Misconduct."') as RecordTypeName,
							IF(a.RecordType = ".GOOD_CONDUCT.", '<img src=\"".$this->ImagePath."/icon_gd_conduct.gif\" width=\"20px\">', 
																'<img src=\"".$this->ImagePath."/icon_misconduct.gif\" width=\"20px\">') as RecordTypeImg,
							a.RecordStatus,
							CASE a.RecordStatus	
								WHEN ".DISCIPLINE_STATUS_PENDING." THEN '".$i_Discipline_System_Award_Punishment_Pending."'
								WHEN ".DISCIPLINE_STATUS_APPROVED." THEN '".$i_Discipline_System_Award_Punishment_Approved."'
								WHEN ".DISCIPLINE_STATUS_REJECTED." THEN '".$i_Discipline_System_Award_Punishment_Rejected."'
								WHEN ".DISCIPLINE_STATUS_WAIVED." THEN '".$i_Discipline_System_Award_Punishment_Waived."'
	  							ELSE '---'
							END as RecordStatusName,
							CASE a.RecordStatus	
								WHEN ".DISCIPLINE_STATUS_PENDING." THEN '<img src=\"".$this->ImagePath."/icon_waiting.png\" width=\"20px\">'
								WHEN ".DISCIPLINE_STATUS_APPROVED." THEN '<img src=\"".$this->ImagePath."/icon_approve.png\" width=\"20px\">'
								WHEN ".DISCIPLINE_STATUS_REJECTED." THEN '<img src=\"".$this->ImagePath."/icon_reject.png\" width=\"20px\">'
								WHEN ".DISCIPLINE_STATUS_WAIVED." THEN '<img src=\"".$this->ImagePath."/icon_waived.png\" width=\"20px\">'
	  							ELSE '---'
							END as RecordStatusImg,
							IF(a.Remark IS NULL OR a.Remark = '', '---', a.Remark) as Remark,
							a.PICID as PICs,
							a.NoticeID,
							a.UpgradedRecordID,
							a.PushMessageID,
							a.isWaitingForDeletion,
							LEFT(a.DateModified, 10) as ModifiedDate,
							a.DateModified as ModifiedDateTime
						FROM
							DISCIPLINE_ACCU_RECORD as a
						INNER JOIN 
							INTRANET_USER as b ON (a.StudentID = b.UserID AND b.RecordStatus IN (0, 1, 2))
						INNER JOIN 
							DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
						LEFT OUTER JOIN 
							DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID $check_cat)
						LEFT OUTER JOIN 
							YEAR_CLASS_USER as ycu ON (b.UserID = ycu.UserID)
						LEFT OUTER JOIN 
							YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID) ";
			if($use_intranet_homework){
				$sql .= " 	LEFT OUTER JOIN 
								ASSESSMENT_SUBJECT as SUB ON (SUB.RecordID = a.ItemID) ";
			}
			$sql .=	"	WHERE
							a.DateInput IS NOT NULL
							$conds
							$currentYearConds
						GROUP BY 
							a.RecordID ";
			$dataAry = $this->returnResultSet($sql);
		}
		else 
		{
			# Settings
			$use_intranet_homework = $this->accumulativeUseIntranetSubjectList();
			$CurrentYearID = getCurrentAcademicYear(); 
			$targetSchoolYear = $CurrentYearID;
			
			# Fields
			$stuName = getNamefieldByLang("b.");
			
			# Conditions
			$conds = "";
			if(!empty($condAry))
			{
				# Record Condition
				$targetRecordID = $condAry["RecordID"];
				if($targetRecordID != "" && $targetRecordID != 0) {
				    $conds .= " AND a.RecordID = '".$targetRecordID."'";
				}
				
				# Year Condition
				$targetSchoolYear = $condAry["selectYear"];
				$targetSchoolYear = convert2unicode($targetSchoolYear, 1, 0);
				$targetSchoolYear = ($targetSchoolYear == "") ? $CurrentYearID : $targetSchoolYear;
				if($targetSchoolYear != "" && $targetSchoolYear != 0 && $conds=="") {
					$conds .= " AND a.Year = '".$targetSchoolYear."' ";
				}
			
				# Semester Condition
				$targetSemester = $condAry["selectSemester"];
				$targetSemester = convert2unicode($targetSemester, 1, 0);
				if($targetSemester != "" && $targetSemester != "0" && $targetSemester != "WholeYear") {
					$conds .= " AND a.Semester = '".$targetSemester."' ";
				}
				
				# Class Condition
				$targetClass = $condAry["selectClass"];
				$targetClass = convert2unicode($targetClass, 1, 0);
				if ($targetClass != "" && $targetClass != "0") {
					$conds .= " AND b.ClassName = '".$targetClass."'";
				}
				
				# PIC Condition
				$targetPIC = $condAry["selectPIC"];
				if($targetPIC != "") {
					$conds .= " AND CONCAT(',', a.PICID, ',') LIKE '%,".$targetPIC.",%'";
				}
			}
			
			// Get Conduct Records
			$sql = "	SELECT
							a.RecordID, 
							a.StudentID, 
							".$stuName." as StudentName,
							CONCAT(b.ClassName, ' - ', b.ClassNumber) as ClassNameNum,
							LEFT(a.RecordDate, 10) as RecordDate,
							a.Semester,
							a.CategoryID, ";
			if($use_intranet_homework) {
				$sql .= " 	IF(a.CategoryID = ".PRESET_CATEGORY_HOMEWORK.", SUB.SubjectID, d.ItemID) as ItemID, ";
				$sql .= " 	IF(a.CategoryID = ".PRESET_CATEGORY_HOMEWORK.", CONCAT(c.Name, ' - ', SUB.SubjectID), IF(d.Name IS NULL, c.Name, CONCAT(c.Name, ' - ', d.Name))) as ItemName, ";
				$check_cat = " AND a.CategoryID != ".PRESET_CATEGORY_HOMEWORK." ";
			}
			else {
				$sql .= " 	d.ItemID as ItemID, ";
				$sql .= " 	IF(d.Name IS NULL, c.Name, CONCAT(c.Name, ' - ', d.Name)) as ItemName, ";
				$check_cat = "";
			}
			$sql .= "		a.RecordType,
							IF(a.RecordType = ".GOOD_CONDUCT.", '".$i_Discipline_GoodConduct."', '".$i_Discipline_Misconduct."') as RecordTypeName,
							IF(a.RecordType = ".GOOD_CONDUCT.", '<img src=\"".$this->ImagePath."/icon_gd_conduct.gif\" width=\"20px\">', 
																'<img src=\"".$this->ImagePath."/icon_misconduct.gif\" width=\"20px\">') as RecordTypeImg,
							a.RecordStatus,
							CASE a.RecordStatus	
								WHEN ".DISCIPLINE_STATUS_PENDING." THEN '".$i_Discipline_System_Award_Punishment_Pending."'
								WHEN ".DISCIPLINE_STATUS_APPROVED." THEN '".$i_Discipline_System_Award_Punishment_Approved."'
								WHEN ".DISCIPLINE_STATUS_REJECTED." THEN '".$i_Discipline_System_Award_Punishment_Rejected."'
								WHEN ".DISCIPLINE_STATUS_WAIVED." THEN '".$i_Discipline_System_Award_Punishment_Waived."'
	  							ELSE '---'
							END as RecordStatusName,
							CASE a.RecordStatus	
								WHEN ".DISCIPLINE_STATUS_PENDING." THEN '<img src=\"".$this->ImagePath."/icon_waiting.png\" width=\"20px\">'
								WHEN ".DISCIPLINE_STATUS_APPROVED." THEN '<img src=\"".$this->ImagePath."/icon_approve.png\" width=\"20px\">'
								WHEN ".DISCIPLINE_STATUS_REJECTED." THEN '<img src=\"".$this->ImagePath."/icon_reject.png\" width=\"20px\">'
								WHEN ".DISCIPLINE_STATUS_WAIVED." THEN '<img src=\"".$this->ImagePath."/icon_waived.png\" width=\"20px\">'
	  							ELSE '---'
							END as RecordStatusImg,
							IF(a.Remark IS NULL OR a.Remark = '', '---', a.Remark) as Remark,
							a.PICID as PICs,
							a.NoticeID,
							a.UpgradedRecordID,
							LEFT(a.DateModified, 10) as ModifiedDate,
							a.DateModified as ModifiedDateTime
						FROM
							DISCIPLINE_ACCU_RECORD as a
						INNER JOIN 
							INTRANET_USER as b ON (a.StudentID = b.UserID AND b.RecordStatus IN (0, 1, 2))
						LEFT OUTER JOIN 
							DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
						LEFT OUTER JOIN 
							DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID $check_cat) ";
			if($use_intranet_homework){
				$sql .= " 	LEFT OUTER JOIN 
								INTRANET_SUBJECT as SUB ON (SUB.SubjectID = a.ItemID) ";
			}
			$sql .=	"	WHERE
							a.DateInput IS NOT NULL
							$conds
						GROUP BY 
							a.RecordID ";
			$dataAry = $this->returnResultSet($sql);
			
//			# Convert to Unicode (EJ)
//			foreach ((array)$dataAry as $thisIndex => $thisDataAry) {
//				foreach((array)$thisDataAry as $thisKey => $thisValue) {
//					$thisValue = convert2unicode($thisValue, 1, 1);
//					$dataAry[$thisIndex][$thisKey] = $thisValue;
//				}
//			}
		}
		
		$dataAry = ($targetRecordID > 0)? $dataAry[0] : $dataAry;
		return $dataAry;
	}
	
	# Get Conduct Actions
	public function GetConductAction($dataAry)
	{
		$ConductActionInfo = $this->GetConductActionInfo($dataAry);
		return $ConductActionInfo;
	}
	private function GetConductActionInfo($dataAry)
	{
		global $Lang, $i_Discipline_System_Award_Punishment_Send_Notice, $i_Notice_StatusTemplate, $i_Discipline_System_Award_Punishment_Detention;
		
		$returnAry = array();
		$RecordActionAry = array();
		
    	# Send Notice
    	$NoticeID = $dataAry["NoticeID"];
    	$TemplateID = $dataAry["TemplateID"];
    	if($NoticeID != "" && $NoticeID != 0) {
		     $RecordActionAry[] = $i_Discipline_System_Award_Punishment_Send_Notice;
    	}
    	else if ($TemplateID != "" && $TemplateID != 0) {
		     $RecordActionAry[] = $i_Discipline_System_Award_Punishment_Send_Notice." (".$i_Notice_StatusTemplate.")";
    	}
		
    	# Send Push Message
    	if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']) {
    		$PushMessageID = $dataAry["PushMessageID"];
    		if($PushMessageID && $PushMessageID != "" && $PushMessageID != 0 && $PushMessageID != -1){
    			$RecordActionAry[] = $Lang['eDiscipline']['DetentionMgmt']['SendPushMessage'];
    		}
    	}
		
		# Detention
		$RecordID = $dataAry["RecordID"];
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE GMID = '".$RecordID."'";
		if(!$this->isEJ()) {
			$sql .= " AND RecordStatus = 1";
		}
        $DetentionCount = $this->returnVector($sql);
    	if($DetentionCount[0] != 0) {
        	$RecordActionAry[] = $i_Discipline_System_Award_Punishment_Detention;
		}
        
        # Cancel Request
        $isWaitingForDeletion = $dataAry["isWaitingForDeletion"];
		if($isWaitingForDeletion) {
			$RecordActionAry[] = $Lang['eDiscipline']['CancelRequestMessage'];
			
     	   # Cancel Notice
        	$UpgradedRecordID = $dataAry["UpgradedRecordID"];
			if($UpgradedRecordID) {
				$RecordActionAry[] = $Lang['eDiscipline']['CancelAPNotice'];
			}
		}
		
		# Conduct Action
		if(empty($RecordActionAry)) {
			$returnAry["RecordAction"] = "---";
		}
		else {
			$returnAry["RecordAction"] = implode("<br/>", (array)$RecordActionAry);
		}
		
		# Conduct PICs
		$RecordPICs = $dataAry["PICs"];
		if($RecordPICs != "" && $RecordPICs != 0) { 
	        $RecordPICs = $this->getPICNameList($RecordPICs);
		}
		$returnAry["RecordPICs"] .= $RecordPICs ? $RecordPICs : "---";
		
//		# Convert to Unicode (EJ)
//		if($this->isEJ()) {
//			$returnAry["RecordAction"] = convert2unicode($returnAry["RecordAction"] , 1, 1);
//			$returnAry["RecordPICs"] = convert2unicode($returnAry["RecordPICs"] , 1, 1);
//		}
		return $returnAry;
	}
	
	# Get Notice Content
	public function GetNoticeByNoticeID($NoticeID)
	{
		$ConductNoticeContent = array();
		if($NoticeID) {
			$ConductNoticeContent = $this->GetNoticeContentByNoticeID($NoticeID);
		}
		return $ConductNoticeContent;
	}
	private function GetNoticeContentByNoticeID($NoticeID)
	{
		global $intranet_root;
		global $i_Notice_Signed, $i_Notice_Unsigned, $i_Notice_Signer, $i_Notice_Editor, $i_Notice_At;
		
		# Load Notice Info
		include_once $intranet_root."/includes/libnotice.php";
		$lnotice = new libnotice($NoticeID);
		
		// Get Notice PICs
		$NoticePICs = "---";
		$NoticePICNames = $lnotice->returnNoticePICNames();
		$noticePICCount = count($NoticePICNames);
		if($noticePICCount > 0) {
			$NoticePICs = "";
			$delim = "";
			for($thisCount=0; $thisCount<$noticePICCount; $thisCount++){
				$NoticePICs .= $delim.$NoticePICNames[$thisCount]["UserName"];
				$delim = ", ";
		  	}
		}
		
		# Load Notice Reply
		$NoticeRecipients = $lnotice->splitTargetGroupUserID($lnotice->RecipientID);
		$NoticeRecipients = $NoticeRecipients[1];
		if(!empty($NoticeRecipients)) {
			$lnotice->retrieveReply($NoticeRecipients);
		}
		
		// Get Notice Reply Status
		// Unsigned
		if ($lnotice->replyStatus != 2) {
            $statusString = "$i_Notice_Unsigned";
        }
    	// Signed
        else {
        	// Get Signer User Info
			include_once $intranet_root."/includes/libuser.php";
			$lu = new libuser();
            $signer = $lu->getNameWithClassNumber($lnotice->signerID);
            $signby = ($lnotice->replyType==1? $i_Notice_Signer : $i_Notice_Editor);
            $statusString = "$i_Notice_Signed <I>($signby $signer $i_Notice_At ".$lnotice->signedTime.")</I>";
        }
        
        # Return Data
		$NoticeContent = array();
		$NoticeContent["DateStart"] = $lnotice->DateStart;
		$NoticeContent["DateEnd"] = $lnotice->DateEnd;
		$NoticeContent["Title"] = $lnotice->Title;
		$NoticeContent["NoticeNumber"] = $lnotice->NoticeNumber;
		$NoticeContent["Description"] = $lnotice->Description;
		$NoticeContent["NoticeIssuer"] = $lnotice->returnIssuerName();
		$NoticeContent["NoticePIC"] = $NoticePICs;
		$NoticeContent["NoticeStatus"] = $statusString;
		$NoticeContent["Recipient"] = $lnotice->returnRecipientNames();
		return $NoticeContent;
	}
	
	# Insert Conduct Records
	public function insertConductRecordDataChecking($dataAry)
	{
		global $sys_custom;
		
		// Get POST Data
		$selected_student = $dataAry["sld_student"];
		$selected_gm_sem  = $dataAry["selectSemester"];
		$selected_gm_date = $dataAry["date"];
		$selected_gm_type = $dataAry["gm_type_select"];
		$selected_gm_cat  = $dataAry["gm_category_select"];
		$selected_gm_item = $dataAry["gm_item_select"];
		$selected_gm_pics = $dataAry["pics"];
		$selected_gm_remarks = $dataAry["remarks"];
		
		# Check Empty Form Data
		if (empty($selected_student)) {
			return $jsError = "noSelectedStudents";
		}
		if (empty($selected_gm_date)) {
			return $jsError = "noSelectedDate";
		}
		if (empty($selected_gm_type) || empty($selected_gm_cat) || empty($selected_gm_item)) {
			return $jsError = "noSelectedItem";
		}
		if (empty($selected_gm_pics)) {
			return $jsError = "noSelectedPICs";
		}
		
		# Check Record Date - Empty / Invalid Date
		$selected_gm_date_stamp = strtotime($selected_gm_date);
		$selected_gm_date = date("Y-m-d", $selected_gm_date_stamp);
		if (is_date_empty($selected_gm_date)) {
			return $jsError = "dateNotAllow";
		}
		
		# Check Record Date - Valid Date Range
		// Get max no. of day(s) before
		if(!$this->isEJ())
		{
			$NumOfMaxRecordDays = $this->MaxRecordDayBeforeAllow;
			if($NumOfMaxRecordDays > 0) {
				$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
				$DateMinLimit = strtotime($DateMinLimit);
				$DateMaxLimit = strtotime(date("Y-m-d"));
				if(($DateMinLimit > $selected_gm_date_stamp) || ($selected_gm_date_stamp > $DateMaxLimit)) {
					return $jsError = "dateNotAllow";
				}
			}
			else {
				$DateMaxLimit = strtotime(date("Y-m-d"));
				if($selected_gm_date_stamp > $DateMaxLimit) {
					return $jsError = "dateNotAllow";
				}
			}
		}
		
		# Check Record Date - Valid Semester
		if($this->isEJ())
		{
			if(getSemesterMode()==2 && retrieveSemester($selected_gm_date)==false) {
				return $jsError = "noSemesterSettings";
			}	
		}
		
		# Check Record Date - GM Item Conversion Period
		$category_period_info = $this->RETRIEVE_TARGET_CATEGORY_PERIOD_SETTING($selected_gm_date, $selected_gm_type, $selected_gm_cat);
		if(empty($category_period_info) && !(($sys_custom["eDiscipline"]["PooiToMiddleSchool"] || $sys_custom["eDiscipline"]["SkipGMPeriodChecking"]) && $selected_gm_type==-1)) {
			return $jsError = "CatPeriodWarning";
		}
		
		# Check Record Date - System Setting > Not allow same student with same "Award & Punishment" or "Good Conduct & Misconduct" item record in a same day
		if(!$this->isEJ())
		{
			if($this->NotAllowSameItemInSameDay) {
				foreach((array)$selected_student as $this_studentid) {
					if($this->checkSameGMItemInSameDay($this_studentid, $selected_gm_cat, $selected_gm_item, $selected_gm_date)==1) {
						return $jsError = "GMInSameDay";
					}
				}
			}
		}
		
		return $jsError = "";
	}
	public function insertConductRecordByAPI($dataAry)
	{
		global $sys_custom;
		
		// Get POST data
		$thisStudentAry = $dataAry["sld_student"];
		$thisRecordType = $dataAry["gm_type_select"];
		$thisCatID = $dataAry["gm_category_select"];
		$thisItemID = $dataAry["gm_item_select"];
		$thisRecordDate = $dataAry["date"];
		$thisRecordDate = date("Y-m-d", strtotime($thisRecordDate));
		$thisRecordSemester  = $dataAry["selectSemester"];
		$thisPIC = $dataAry["pics"];
		$thisPIC = array_unique((array)$thisPIC);
		$thisPIC = (sizeof($thisPIC) > 0)? implode(",", $thisPIC) : $_SESSION["UserID"];
		$thisRemark = $dataAry["remarks"];
		$thisRemark = intranet_htmlspecialchars($thisRemark);
		if($this->isEJ()) {
			$thisRemark = convert2unicode($thisRemark, 1, 0);
		}
		//$thisAttachment = $sessionTime."_".$StudentID."tmp";
		//$thisActionID = ${"ActionID_".$StudentID};
		//$thisGMCount = ${"GMCount_".$StudentID};		
		
		// Year & Semester
		if(!$this->isEJ())
		{
			$thisYearName = GET_ACADEMIC_YEAR3($thisRecordDate);
			$thisYearID = $this->getAcademicYearIDByYearName($thisYearName);
			$thisSemInfo = getAcademicYearAndYearTermByDate($thisRecordDate);
			$thisSemesterID = $thisSemInfo[0];
			$thisSemesterName = $thisSemInfo[1];
		}
		else
		{
			$thisYearName = getCurrentAcademicYear();
			if(getSemesterMode()==2) {
				$thisSemesterName = retrieveSemester($thisRecordDate);
			}
			else {
				$thisSemesterName = convert2unicode($thisRecordSemester, 1, 0);
			}
		}
		
		// loop Students
		$NewConductIDAry = array();
		$thisStudentAry = array_unique((array)$thisStudentAry);
		foreach((array)$thisStudentAry as $thisStudentID) {
			# Check Record Date - Conversion Period
			$period_info = $this->RETRIEVE_TARGET_CATEGORY_PERIOD_SETTING($thisRecordDate, $thisRecordType, $thisCatID);
			if(sizeof($period_info) || (($sys_custom["eDiscipline"]["PooiToMiddleSchool"] || $sys_custom["eDiscipline"]["SkipGMPeriodChecking"]) && $thisRecordType==-1))
			{
				# INSERT INTO [DISCIPLINE_ACCU_RECORD]
				$dataAry = array();
				if(!$this->isEJ())
				{
					$dataAry["RecordDate"] = $thisRecordDate;
					$dataAry["Year"] = addslashes($thisYearName);
					$dataAry["Semester"] = addslashes($thisSemesterName);
					$dataAry["AcademicYearID"] = $thisYearID;
					$dataAry["YearTermID"] = $thisSemesterID;
					$dataAry["StudentID"] = $thisStudentID;
					$dataAry["RecordType"] = $thisRecordType;
					$dataAry["CategoryID"] = $thisCatID;
					$dataAry["ItemID"] = $thisItemID;
					$dataAry["PICID"] = $thisPIC;
					$dataAry["Remark"] = $thisRemark;
				}
				else
				{
					$dataAry["RecordDate"] = $thisRecordDate;
					$dataAry["Year"] = $thisYearName;
					$dataAry["Semester"] = $thisSemesterName;
					$dataAry["StudentID"] = $thisStudentID;
					$dataAry["RecordType"] = $thisRecordType;
					$dataAry["CategoryID"] = $thisCatID;
					$dataAry["ItemID"] = $thisItemID;
					$dataAry["PICID"] = $thisPIC;
					$dataAry["Remark"] = $thisRemark;
				}
				$accu_record_id = $this->INSERT_MISCONDUCT_RECORD($dataAry);
				
				# Re-group Student Conducts 
				if($accu_record_id) {
					$this->REGROUP_CONDUCT_RECORDS($accu_record_id);
					$NewConductIDAry[] = $accu_record_id;
				}
			}
		}
		
		return $NewConductIDAry;
	}
	
	# Edit Conduct Records
	public function updateConductRecordByAPI($dataAry)
	{
		// Get POST data
		$thisConductID = $dataAry["ConductID"][0];
		$thisItemID = $dataAry["gm_item_select"];
		$thisPIC = $dataAry["pics"];
		$thisRemark = $dataAry["remarks"];
		if($this->isEJ()) {
			$thisRemark = convert2unicode($thisRemark, 1, 0);
		}
		//$thisRemark = intranet_htmlspecialchars($thisRemark);
		
		// Get Conduct Info
		$ConductInfo = $this->RETRIEVE_CONDUCT_INFO($thisConductID);
		$ConductInfo = $ConductInfo[0];
		$ConductStudent = $ConductInfo["StudentID"];
		$ConductCategory = $ConductInfo["CategoryID"];
		$ConductDate = $ConductInfo["RecordDate"];
		$ConductUpgradedAP = $ConductInfo["UpgradedRecordID"];
		
		# Check Late Record
		$isLateRecords = $ConductCategory == PRESET_CATEGORY_LATE;
		
		# Check PICs > System Setting - Only allow admin to select record's PIC
		$IsCannotSelectPICs = $this->isEJ() || ($this->OnlyAdminSelectPIC && !$this->IS_ADMIN_USER($_SESSION["UserID"]));
		
		# Check Empty data
		if (empty($thisConductID)) {
			return $jsErrorMsg = "noFormData";
		}
		if (empty($thisItemID) && !$isLateRecords) {
			return $jsErrorMsg = "noSelectedItem";
		}
		if (empty($thisPIC) && !$isLateRecords && !$IsCannotSelectPICs) {
			return $jsErrorMsg = "noSelectedPICs";
		}
		$thisPIC = empty($thisPIC)? "NULL" : "'".implode(",", (array)$thisPIC)."'";
		
		# Check Record Date - System Setting > Not allow same student with same "Award & Punishment" or "Good Conduct & Misconduct" item record in a same day
		if(!$this->isEJ())
		{
			if($this->NotAllowSameItemInSameDay) {
				if($this->checkSameGMItemInSameDay($ConductStudent, $ConductCategory, $thisItemID, $ConductDate)==1) {
					return $jsError = "GMInSameDay";
				}
			}
		}
		
		if(!$this->isEJ())
		{
			// Get Semester Info
			list($SchoolYearID, $SchoolYear, $SemesterID, $Semester) = getAcademicYearInfoAndTermInfoByDate($ConductDate);
			
			# Prepare Update Fields
			$UpdateFields = "";
			if(!$isLateRecords) {
				$UpdateFields .= " ItemID = '$thisItemID', ";
			}
			if(!$IsCannotSelectPICs) {
				$UpdateFields .= " PICID = $thisPIC, ";
			}
			
			# Update Conduct Records
			$sql = "UPDATE DISCIPLINE_ACCU_RECORD SET $UpdateFields Remark = '$thisRemark', YearTermID = '$SemesterID', DateModified = NOW() WHERE RecordID = '$thisConductID'";
			$result = $this->db_db_query($sql) or die(mysql_error());
			
			# Check if Grouped AP need to change
			if($result) 
			{
				if($ConductUpgradedAP) {
					// Get last Conduct in AP
					$sql1 = "SELECT RecordID FROM DISCIPLINE_ACCU_RECORD WHERE UpgradedRecordID = '".$ConductUpgradedAP."' ORDER BY RecordDate DESC LIMIT 1";
					$result1 = $this->returnVector($sql1);
					
					# Update AP Semester according to last Conduct
					if($result1[0] == $thisConductID) {
						$sql2 = "UPDATE DISCIPLINE_MERIT_RECORD SET YearTermID = '".$SemesterID."', Semester = '".$Semester."' WHERE RecordID = '".$ConductUpgradedAP."'";
						$this->db_db_query($sql2) or die(mysql_error());
					}
				}
				
				return $jsErrorMsg = "";
			}
		}
		else
		{
			# Update Conduct Records
			$this->updateGoodConductMisconductRecord($thisConductID, $thisItemID, $thisRemark);
			return $jsErrorMsg = "";
		}
	}
	
	# Delete Conduct Record
	public function deleteConductRecordByAPI($RecordID)
	{
		global $file_path;
		
		# Remove Conduct Attachments
		if(!$this->isEJ())
		{
			$sql = "SELECT Attachment FROM DISCIPLINE_ACCU_RECORD WHERE RecordID = '$RecordID'";
			$attachmentLoc = $this->returnVector($sql);
			if($attachmentLoc[0] != "") {
				$path = $file_path."/file/disciplinev12/goodconduct_misconduct/".$attachmentLoc[0];
				$this->deleteDirectory($path);
			}
		}
		
		return $this->DELETE_CONDUCT_RECORD($RecordID);
	}
	
	# Update Conduct Record Status
	public function updateConductRecordStatusByAPI($dataAry)
	{
		global $sys_custom;
		
		// Get POST data
		$thisConductID = $dataAry["ConductID"][0];
		$thisConductStatus = $dataAry["gm_record_status"];
		
		// Get Current Conduct Status
		$ConductRecord = $this->GetConductRecord("", $thisConductID);
		$OldRecordStatus = $ConductRecord["RecordStatus"];
		
		# Record Status: Pending / Rejected > Approved
		if($thisConductStatus == DISCIPLINE_STATUS_APPROVED && ($OldRecordStatus == DISCIPLINE_STATUS_PENDING || $OldRecordStatus == DISCIPLINE_STATUS_REJECTED))
		{
			// Get Conduct Info
			$thisRecordInfo = $this->RETRIEVE_CONDUCT_INFO($thisConductID);
			$thisRecordInfo = $thisRecordInfo[0];
			$thisRecordDate = substr($thisRecordInfo["RecordDate"], 0, 10);
			$thisRecordType = $thisRecordInfo["RecordType"];
			$thisCatID = $thisRecordInfo["CategoryID"];
			
			// Get Conversion Period Settings
			$thisPeriodInfo = $this->RETRIEVE_TARGET_CATEGORY_PERIOD_SETTING($thisRecordDate, $thisRecordType, $thisCatID);
			list($thisPeriodType, $thisPeriodSettings) = $thisPeriodInfo;
			
			# Conversion Period : Static
			if($thisPeriodType=="static")
			{
				// Get Conduct Info
				$sql = "SELECT CategoryID, RecordDate FROM DISCIPLINE_ACCU_RECORD WHERE RecordID = '".$thisConductID."'";
				$arr_result = $this->returnArray($sql, 2);
				if(sizeof($arr_result) > 0){
					list($CategoryID, $RecordDate) = $arr_result[0];
				}
				
				// Get Conversion Period
		 		$sql = "SELECT SetID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD WHERE CategoryID = '".$CategoryID."' AND RecordStatus = 1";
		 		$tmp_SetID = $this->returnVector($sql);
				$SetID = $tmp_SetID[0]? $tmp_SetID[0] : 0;	
				
				// Get Conversion Period Settings
				$sql = "SELECT PeriodID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING WHERE CategoryID = '".$CategoryID."'";
				$arr_PeriodID = $this->returnVector($sql);
				if(sizeof($arr_PeriodID) > 0){
					$TargetPeriod = implode(",", $arr_PeriodID);
				}
				
				# Check if within Conversion Period
				$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE PeriodID IN (".$TargetPeriod.") AND SetID = '".$SetID."' AND (DateStart <= '".$RecordDate."' AND DateEnd >= '".$RecordDate."')";
				$temp = $this->returnVector($sql);
				$InDateRange = sizeof($temp) > 0 && $temp[0] > 0? 1 : 0;
			}
			# Conversion Period : Float
			else
			{
				# Check if within Conversion Period
				for($i=0; $i<sizeof($thisPeriodSettings); $i++) {
					if($thisPeriodSettings[$i]["DateStart"] <= $thisRecordDate && $thisPeriodSettings[$i]["DateEnd"] >= $thisRecordDate) {
						$InDateRange = 1;
						break;
					}
				}
			}
			
			# Approve Record after Checking
			if($InDateRange || $sys_custom["eDiscipline"]["SkipGMPeriodChecking"]){
				$this->APPROVE_CONDUCT_RECORD($thisConductID);
			}
		}
		// Record Status : Approved / Pending > Rejected
		else if($thisConductStatus == DISCIPLINE_STATUS_REJECTED && ($OldRecordStatus == DISCIPLINE_STATUS_APPROVED || $OldRecordStatus == DISCIPLINE_STATUS_PENDING))
		{
			$this->REJECT_CONDUCT_RECORD($thisConductID);
		}
		// Record Status: Pending / Wavied > SKIP
		else if($thisConductStatus == DISCIPLINE_STATUS_PENDING || $thisConductStatus == DISCIPLINE_STATUS_WAIVED)
		{
			// continue;
		}
	}
	
	# Assign Notice to Conduct Records
	public function assignRecordNoticeDataChecking($dataAry)
	{
		// Get POST Data
		$send_notice = $dataAry["send_notice"];
		$notice_cat = $dataAry["notice_category"];
		$notice_template = $dataAry["notice_template"];
		
		# Check empty data
		if ($send_notice) {
			if (empty($notice_cat) || empty($notice_template)) {
				return $jsError = "noSelectedTemplate";
			}
		}
		return $jsError = "";
	}
	public function assignRecordNoticeByAPI($dataAry)
	{
		global $intranet_root, $PATH_WRT_ROOT, $sys_custom;
		
		include_once $intranet_root."/includes/libnotice.php";
		$lnotice = new libnotice();
		
		// Get POST Data
		$conduct_ids = $dataAry["ConductID"];
		$send_notice = $dataAry["send_notice"];
		$notice_template = $dataAry["notice_template"];
		$additional_info = $dataAry["additional_info"];
		$additional_info = stripslashes((stripslashes($additional_info)));
		if($this->isEJ()) {
			$additional_info = convert2unicode($additional_info, 1, 0);
		}
		$send_email_notify = $dataAry["email_notify"];
		
		# Check if allow to send eNotice
		if(!$lnotice->disabled && $send_notice && !empty($conduct_ids))
		{
			$template_info = $this->RETRIEVE_NOTICE_TEMPLATE_INFO($notice_template);
			if(sizeof($template_info))
			{
				foreach((array)$conduct_ids as $this_conductid) {
					# Check if Conduct exist
					$ConductRecord = $this->GetConductRecord("", $this_conductid);
					if(!empty($ConductRecord))
					{
						// Get Conduct Info
						$conduct_category = $ConductRecord["CategoryID"];
						$conduct_studentid = $ConductRecord["StudentID"];
						
						# Build Conduct Data
						$SpecialData = array();
						$SpecialData["ConductRecordID"] = $this_conductid;
						if($sys_custom["eDisciplinev12_eNoticeTemplate_AccuNumber"]) {
							$SpecialData["AccuNumber"] = $this->RETURN_ACCUMULATED_CONDUCT_NUMBER_FOR_ENOTICE_TEMPLATE($conduct_studentid, $this_conductid, $conduct_category);
						}
						
						# Build Template Data
						$template_category = $template_info[0]["CategoryID"];
						$template_data = $this->TEMPLATE_VARIABLE_CONVERSION($template_category, $conduct_studentid, $SpecialData, $additional_info);
						$UserName = $this->getUserNameByID($this->UserID);
						$Module = $this->Module;
						$NoticeNumber = time();
						$TemplateID = $notice_template;
						$Variable = $template_data;
						$IssueUserID = $_SESSION["UserID"];
						$IssueUserName = $UserName[0];
						$TargetRecipientType = "U";
						$RecipientID = array($conduct_studentid);
						$RecordType = NOTICE_TO_SOME_STUDENTS;
						
						# Set eNotice Parameter
						include_once $intranet_root."/includes/libucc.php";
						$lc = new libucc();
						$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

                        // [2020-1009-1753-46096] delay notice start date & end date
                        if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
                        {
                            $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                            $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                            $lc->setDateStart($DateStart);

                            if ($lc->defaultDisNumDays > 0) {
                                $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                                $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                                $lc->setDateEnd($DateEnd);
                            }
                        }
						
						# Send Notice & Update Conduct
						$NoticeID = $lc->sendNotice($send_email_notify);
						$this->updateMisconductRecordNoticeID($NoticeID, $this_conductid);
						
						# Suspend eNotice
						if($NoticeID != -1)
						{
							if($sys_custom["eDiscipline"]["NotReleaseNotice"]) {
								$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
							}
							else {
								if($ConductRecord["RecordStatus"] != DISCIPLINE_STATUS_APPROVED) {
									$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
								}
							}
						}
					}
				}
			}
		}
	}
	
	# Get Merit Records
	public function GetMeritRecord($condAry=array(), $targetID="")
	{
		if($targetID) {
			$condAry["RecordID"] = $targetID;
		}
		$MeritRecordArr = $this->GetMeritRecordInfo($condAry);
		return $MeritRecordArr;
	}
	private function GetMeritRecordInfo($condAry=array())
	{
		global $intranet_session_language;
		global $Lang;
		global $i_Merit_Award, $i_Merit_Punishment, $i_Merit_Warning, $i_Merit_Merit, $i_Merit_MinorCredit, $i_Merit_MajorCredit, $i_Merit_SuperCredit, $i_Merit_UltraCredit, $i_Merit_BlackMark, $i_Merit_MinorDemerit, $i_Merit_MajorDemerit, $i_Merit_SuperDemerit, $i_Merit_UltraDemerit;
		global $i_Discipline_System_Award_Punishment_Pending, $i_Discipline_System_Award_Punishment_Approved, $i_Discipline_System_Award_Punishment_Rejected, $i_Discipline_System_Award_Punishment_Waived, $i_Discipline_Released_To_Student;
		
		if(!$this->isEJ())
		{
			# Settings
			$CurrentYearID = Get_Current_Academic_Year_ID();
			
			# Fields
			$stuName = getNamefieldByLang("b.");
			$clsName = Get_Lang_Selection("g.ClassTitleB5", "g.ClassTitleEN");
			$MeritImg = "<img src=\"".$this->ImagePath."/icon_gd_conduct.gif\" width=\"20px\">";
			$DemeritImg = "<img src=\"".$this->ImagePath."/icon_misconduct.gif\" width=\"20px\">";
			if($intranet_session_language=="en") {
				$MeritStr = "CONCAT(
								a.ProfileMeritCount, 
								' ',
								CASE (a.ProfileMeritType)
									WHEN 0 THEN '".$i_Merit_Warning."'
									WHEN 1 THEN '".$i_Merit_Merit."'
									WHEN 2 THEN '".$i_Merit_MinorCredit."'
									WHEN 3 THEN '".$i_Merit_MajorCredit."'
									WHEN 4 THEN '".$i_Merit_SuperCredit."'
									WHEN 5 THEN '".$i_Merit_UltraCredit."'
									WHEN -1 THEN '".$i_Merit_BlackMark."'
									WHEN -2 THEN '".$i_Merit_MinorDemerit."'
									WHEN -3 THEN '".$i_Merit_MajorDemerit."'
									WHEN -4 THEN '".$i_Merit_SuperDemerit."'
									WHEN -5 THEN '".$i_Merit_UltraDemerit."'
								ELSE 'Error' END, 
								'(s)'
							)";
			}
			else {
				$MeritStr = "CONCAT(
								CASE (a.ProfileMeritType)
									WHEN 0 THEN '".$i_Merit_Warning."'
									WHEN 1 THEN '".$i_Merit_Merit."'
									WHEN 2 THEN '".$i_Merit_MinorCredit."'
									WHEN 3 THEN '".$i_Merit_MajorCredit."'
									WHEN 4 THEN '".$i_Merit_SuperCredit."'
									WHEN 5 THEN '".$i_Merit_UltraCredit."'
									WHEN -1 THEN '".$i_Merit_BlackMark."'
									WHEN -2 THEN '".$i_Merit_MinorDemerit."'
									WHEN -3 THEN '".$i_Merit_MajorDemerit."'
									WHEN -4 THEN '".$i_Merit_SuperDemerit."'
									WHEN -5 THEN '".$i_Merit_UltraDemerit."'
								ELSE 'Error' END, 
								CAST(a.ProfileMeritCount as char), 
								'".$Lang['eDiscipline']['Times']."', 
								' '
							)";
			}
			$ConductScoreFields = "";		
			if(!$this->Hidden_ConductMark) {
				$ConductScoreFields .= " a.ConductScoreChange, ";
			}
			if($this->UseSubScore) {
				$ConductScoreFields .= " a.SubScore1Change, ";
			}
			if($this->UseActScore){
				$ConductScoreFields .= " a.SubScore2Change, ";
			}
			
			# Conditions
			$conds = "";
			$currentYearConds = " AND g.AcademicYearID = '".$CurrentYearID."' ";
			if(!empty($condAry))
			{
				// Record Condition
				$targetRecordID = $condAry["RecordID"];
				if($targetRecordID != "" && $targetRecordID != 0) {
				    $conds .= " AND a.RecordID = '".$targetRecordID."'";
				}
				
				// Year Condition
				$targetSchoolYear = $condAry["selectYear"];
				$targetSchoolYear = ($targetSchoolYear == "") ? $CurrentYearID : $targetSchoolYear;
				if($targetSchoolYear != "" && $targetSchoolYear != 0 && $conds == "") {
					$conds .= " AND a.AcademicYearID = '".$targetSchoolYear."'";
				}
			
				// Semester Condition
				$targetSemester = $condAry["selectSemester"];
				if($targetSemester != "" && $targetSemester != 0 && $targetSemester != "WholeYear") {
					$conds .= " AND (a.YearTermID = '".$targetSemester."')";
				}
				
				// Class Condition
				$targetClass = $condAry["selectClass"];
				if ($targetClass != "" && $targetClass != "0") {
				    if(is_numeric($targetClass)) {
				    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID = '".$targetClass."'";
				    	$tempClass = $this->returnVector($sql);
				    	$conds .= (sizeof($tempClass) > 0) ? " AND g.YearClassID IN (".implode(",", $tempClass).")" : "";
					}
				    else {
						$conds .= " AND g.YearClassID = '".substr($targetClass, 2)."'";
				    }
				}
				$targetOwnClassOnly = $condAry["ViewOwnClassOnly"];
				if($targetOwnClassOnly) {
					$currentYearConds = " AND g.AcademicYearID = '".$targetSchoolYear."' ";
					
					$ClassStudentIDAry = $this->getStudentListByClassTeacherID("", $_SESSION["UserID"], $targetSchoolYear);
					$SubjectStudentIDAry = $this->getStudentListBySubjectTeacherID("", $_SESSION["UserID"], $targetSchoolYear);
					$StudentAry = array_merge($ClassStudentIDAry, $SubjectStudentIDAry);
					$StudentAry = array_unique($StudentAry);
					$conds.= " AND a.StudentID IN (".implode(",", (array)$StudentAry).") ";
				}
				
				// PIC Condition
				$targetPICs = $condAry["selectPIC"];
				if($targetPICs != "") {
					$conds .= " AND CONCAT(',', a.PICID, ',') LIKE '%,".$targetPICs.",%'";
				}
			}
			
			// Get Merit Info
			$sql = "SELECT
						a.RecordID,
						a.StudentID,
						".$stuName." as StudentName,
						CONCAT(".$clsName.", ' - ', f.ClassNumber) as ClassNameNum,
						a.RecordDate,
						a.ItemID,
						c.CatID,
						IF(a.ItemID = 0, c.ItemCode, CONCAT(c.ItemCode, ' - ', c.ItemName)) as ItemName,
						a.MeritType,
						IF(a.MeritType > 0, '".$i_Merit_Award."', '".$i_Merit_Punishment."') as MeritTypeName,
						IF(a.MeritType > 0, '".$MeritImg."', '".$DemeritImg."') as MeritTypeImg,
						a.ProfileMeritType,
						a.ProfileMeritCount,
						IF(a.ProfileMeritCount = 0, '--', ".$MeritStr.") as ReceivedMeritType,
						".$ConductScoreFields."
						a.RecordStatus,
						CASE a.RecordStatus	
							WHEN ".DISCIPLINE_STATUS_PENDING." THEN '".$i_Discipline_System_Award_Punishment_Pending."'
							WHEN ".DISCIPLINE_STATUS_APPROVED." THEN '".$i_Discipline_System_Award_Punishment_Approved."'
							WHEN ".DISCIPLINE_STATUS_REJECTED." THEN '".$i_Discipline_System_Award_Punishment_Rejected."'
							WHEN ".DISCIPLINE_STATUS_WAIVED." THEN '".$i_Discipline_System_Award_Punishment_Waived."'
							ELSE '---'
						END as RecordStatusName,
						CASE a.RecordStatus	
							WHEN ".DISCIPLINE_STATUS_PENDING." THEN '<img src=\"".$this->ImagePath."/icon_waiting.png\" width=\"20px\">'
							WHEN ".DISCIPLINE_STATUS_APPROVED." THEN '<img src=\"".$this->ImagePath."/icon_approve.png\" width=\"20px\">'
							WHEN ".DISCIPLINE_STATUS_REJECTED." THEN '<img src=\"".$this->ImagePath."/icon_reject.png\" width=\"20px\">'
							WHEN ".DISCIPLINE_STATUS_WAIVED." THEN '<img src=\"".$this->ImagePath."/icon_waived.png\" width=\"20px\">'
							ELSE '---'
						END as RecordStatusImg,
						a.ReleaseStatus,
						IF(a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED.", '".$i_Discipline_Released_To_Student."', '') as ReleaseStatusName,
						IF(a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED.", '<img src=\"".$this->ImagePath."/icon_to_student.png\" width=\"20px\">', '') as ReleaseStatusImg,
						a.RedeemID,
						IF(a.Remark IS NULL OR a.Remark = '', '---', a.Remark) as Remark,
						a.PICID as PICs,
						a.SubjectID,
						a.TemplateID,
						a.NoticeID,
						a.fromConductRecords,
						a.CaseID,
						a.PushMessageID,
						a.RehabilPIC as RehabilPICs,
						LEFT(a.DateModified,10) as ModifiedDate,
						a.DateModified as ModifiedDateTime,
						a.ApprovedBy,
						a.ApprovedDate,
						a.RejectedBy,
						a.RejectedDate,
						a.ReleasedBy,
						a.ReleasedDate,
						a.WaivedBy,
						a.WaivedDate
					FROM
						DISCIPLINE_MERIT_RECORD as a
					INNER JOIN
						INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN
						DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
					LEFT OUTER JOIN
						DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
					LEFT OUTER JOIN
						YEAR_CLASS_USER as f ON (a.StudentID = f.UserID AND f.UserID = a.StudentID)
					LEFT OUTER JOIN
						YEAR_CLASS as g ON (f.YearClassID = g.YearClassID)
						$extraTable
					WHERE
						a.DateInput IS NOT NULL AND b.RecordStatus IN (0,1,2) ".$conds." ".$currentYearConds;
			$dataAry = $this->returnResultSet($sql);
		}
		else
		{
			# Settings
			$CurrentYearID = getCurrentAcademicYear();
			
			# Fields
			$stuName = getNamefieldByLang("b.");
			$MeritImg = "<img src=\"".$this->ImagePath."/icon_gd_conduct.gif\" width=\"20px\">";
			$DemeritImg = "<img src=\"".$this->ImagePath."/icon_misconduct.gif\" width=\"20px\">";
			if($intranet_session_language=="en") {
				$MeritStr = "CONCAT(
								a.ProfileMeritCount, 
								' ',
								CASE (a.ProfileMeritType)
									WHEN 0 THEN '".$i_Merit_Warning." '
									WHEN 1 THEN '".$i_Merit_Merit." '
									WHEN 2 THEN '".$i_Merit_MinorCredit." '
									WHEN 3 THEN '".$i_Merit_MajorCredit." '
									WHEN 4 THEN '".$i_Merit_SuperCredit." '
									WHEN 5 THEN '".$i_Merit_UltraCredit." '
									WHEN -1 THEN '".$i_Merit_BlackMark." '
									WHEN -2 THEN '".$i_Merit_MinorDemerit." '
									WHEN -3 THEN '".$i_Merit_MajorDemerit." '
									WHEN -4 THEN '".$i_Merit_SuperDemerit." '
									WHEN -5 THEN '".$i_Merit_UltraDemerit." '
								ELSE 'Error' END, 
								'(s)'
							)";
			}
			else {
				$MeritStr = "CONCAT(
								CASE (a.ProfileMeritType)
									WHEN 0 THEN '".$i_Merit_Warning." '
									WHEN 1 THEN '".$i_Merit_Merit." '
									WHEN 2 THEN '".$i_Merit_MinorCredit." '
									WHEN 3 THEN '".$i_Merit_MajorCredit." '
									WHEN 4 THEN '".$i_Merit_SuperCredit." '
									WHEN 5 THEN '".$i_Merit_UltraCredit." '
									WHEN -1 THEN '".$i_Merit_BlackMark." '
									WHEN -2 THEN '".$i_Merit_MinorDemerit." '
									WHEN -3 THEN '".$i_Merit_MajorDemerit." '
									WHEN -4 THEN '".$i_Merit_SuperDemerit." '
									WHEN -5 THEN '".$i_Merit_UltraDemerit." '
								ELSE 'Error' END, 
								CAST(a.ProfileMeritCount as char), 
								'".$Lang['eDiscipline']['Times']."', 
								' '
							)";
			}
			$ConductScoreFields = "";		
			if(!$this->Hidden_ConductMark) {
				$ConductScoreFields .= " a.ConductScoreChange, ";
			}
			if($this->UseSubScore) {
				$ConductScoreFields .= " a.SubScore1Change, ";
			}
			if($this->UseActScore){
				$ConductScoreFields .= " a.SubScore2Change, ";
			}
			$ConductScoreFields = " a.ConductScoreChange, ";
			
			# Conditions
			$conds = "";
			if(!empty($condAry))
			{
				// Record Condition
				$targetRecordID = $condAry["RecordID"];
				if($targetRecordID != "" && $targetRecordID != 0) {
				    $conds .= " AND a.RecordID = '".$targetRecordID."'";
				}
				
				# Year Condition
				$targetSchoolYear = $condAry["selectYear"];
				$targetSchoolYear = convert2unicode($targetSchoolYear, 1, 0);
				$targetSchoolYear = ($targetSchoolYear == "") ? $CurrentYearID : $targetSchoolYear;
				if($targetSchoolYear != "" && $targetSchoolYear != 0 && $conds=="") {
					$conds .= " AND a.Year = '".$targetSchoolYear."' ";
				}
			
				# Semester Condition
				$targetSemester = $condAry["selectSemester"];
				$targetSemester = convert2unicode($targetSemester, 1, 0);
				if($targetSemester != "" && $targetSemester != "0" && $targetSemester != "WholeYear") {
					$conds .= " AND a.Semester = '".$targetSemester."' ";
				}
				
				# Class Condition
				$targetClass = $condAry["selectClass"];
				$targetClass = convert2unicode($targetClass, 1, 0);
				if ($targetClass != "" && $targetClass != "0") {
					$conds .= " AND b.ClassName = '".$targetClass."'";
				}
				
				# PIC Condition
				$targetPIC = $condAry["selectPIC"];
				if($targetPIC != "") {
					$conds .= " AND CONCAT(',', a.PICID, ',') LIKE '%,".$targetPIC.",%'";
				}
			}
			
			// Get Merit Info
			$sql = "SELECT
						a.RecordID,
						a.StudentID,
						".$stuName." as StudentName,
						CONCAT(b.ClassName, ' - ', b.ClassNumber) as ClassNameNum,
						a.RecordDate,
						a.Semester,
						a.ItemID,
						c.CatID,
						IF(a.ItemID = 0, c.ItemCode, CONCAT(c.ItemCode, ' - ', c.ItemName)) as ItemName,
						a.MeritType,
						IF(a.MeritType > 0, '".$i_Merit_Award."', '".$i_Merit_Punishment."') as MeritTypeName,
						IF(a.MeritType > 0, '".$MeritImg."', '".$DemeritImg."') as MeritTypeImg,
						a.ProfileMeritType,
						a.ProfileMeritCount,
						IF(a.ProfileMeritCount = 0, '--', ".$MeritStr.") as ReceivedMeritType,
						".$ConductScoreFields."
						a.RecordStatus,
						CASE a.RecordStatus	
							WHEN ".DISCIPLINE_STATUS_PENDING." THEN '".$i_Discipline_System_Award_Punishment_Pending."'
							WHEN ".DISCIPLINE_STATUS_APPROVED." THEN '".$i_Discipline_System_Award_Punishment_Approved."'
							WHEN ".DISCIPLINE_STATUS_REJECTED." THEN '".$i_Discipline_System_Award_Punishment_Rejected."'
							WHEN ".DISCIPLINE_STATUS_WAIVED." THEN '".$i_Discipline_System_Award_Punishment_Waived."'
							ELSE '---'
						END as RecordStatusName,
						CASE a.RecordStatus	
							WHEN ".DISCIPLINE_STATUS_PENDING." THEN '<img src=\"".$this->ImagePath."/icon_waiting.png\" width=\"20px\">'
							WHEN ".DISCIPLINE_STATUS_APPROVED." THEN '<img src=\"".$this->ImagePath."/icon_approve.png\" width=\"20px\">'
							WHEN ".DISCIPLINE_STATUS_REJECTED." THEN '<img src=\"".$this->ImagePath."/icon_reject.png\" width=\"20px\">'
							WHEN ".DISCIPLINE_STATUS_WAIVED." THEN '<img src=\"".$this->ImagePath."/icon_waived.png\" width=\"20px\">'
							ELSE '---'
						END as RecordStatusImg,
						a.ReleaseStatus,
						IF(a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED.", '".$i_Discipline_Released_To_Student."', '') as ReleaseStatusName,
						IF(a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED.", '<img src=\"".$this->ImagePath."/icon_to_student.png\" width=\"20px\">', '') as ReleaseStatusImg,
						a.RedeemID,
						IF(a.Remark IS NULL OR a.Remark = '', '---', a.Remark) as Remark,
						a.PICID as PICs,
						a.TemplateID,
						a.NoticeID,
						a.fromConductRecords,
						a.CaseID,
						LEFT(a.DateModified,10) as ModifiedDate,
						a.DateModified as ModifiedDateTime,
						a.ApprovedBy,
						a.ApprovedDate,
						a.RejectedBy,
						a.RejectedDate,
						a.ReleasedBy,
						a.ReleasedDate,
						a.WaivedBy,
						a.WaivedDate
					FROM
						DISCIPLINE_MERIT_RECORD as a
					INNER JOIN
						INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN
						DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
					LEFT OUTER JOIN
						DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
						$extraTable
					WHERE
						a.DateInput IS NOT NULL AND a.MeritType IN (-1, 0, 1) ".$conds." ";
			$dataAry = $this->returnResultSet($sql);
		}
		
		$dataAry = ($targetRecordID > 0)? $dataAry[0] : $dataAry;
		return $dataAry;
	}
	
	# Get Merit Actions
	public function GetMeritAction($dataAry)
	{
		$MeritActionInfo = $this->GetMeritActionInfo($dataAry);
		return $MeritActionInfo;
	}
	private function GetMeritActionInfo($dataAry)
	{
		global $Lang, $eDiscipline, $i_Discipline_System_Award_Punishment_Send_Notice, $i_Notice_StatusTemplate, $i_Discipline_System_Award_Punishment_Detention, $i_Discipline_System_Discipline_Case_Record_Case;
		
		$returnAry = array();
		$RecordActionAry = array();
		
    	# Send Notice
    	$NoticeID = $dataAry["NoticeID"];
    	$TemplateID = $dataAry["TemplateID"];
    	if($NoticeID != "" && $NoticeID != 0) {
		     $RecordActionAry[] = $i_Discipline_System_Award_Punishment_Send_Notice;
    	}
    	else if ($TemplateID != "" && $TemplateID != 0) {
		     $RecordActionAry[] = $i_Discipline_System_Award_Punishment_Send_Notice." (".$i_Notice_StatusTemplate.")";
    	}
		                            	
    	# Send Push Message
    	if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']) {
    		$PushMessageID = $dataAry["PushMessageID"];
    		if($PushMessageID && $PushMessageID != "" && $PushMessageID != 0 && $PushMessageID != -1) {
    			$RecordActionAry[] = $Lang['eDiscipline']['DetentionMgmt']['SendPushMessage'];
    		}
    	}
    	
    	# Rehabilitation
    	if($sys_custom['eDiscipline']['CSCProbation']) {
    		$RehabilPICs = $dataAry["RehabilPICs"];
    		if(isset($RehabilPICs) && $RehabilPICs != "") {
				$RecordActionAry[] = $Lang['eDiscipline']['CWCRehabil']['Title'];
    		}
    	}
		
		# Detention
		$RecordID = $dataAry["RecordID"];
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DemeritID = '".$RecordID."'";
		if(!$this->isEJ()) {
			$sql .= " AND RecordStatus = 1";
		}
        $DetentionCount = $this->returnVector($sql);
    	if($DetentionCount[0] != 0) {
        	$RecordActionAry[] = $i_Discipline_System_Award_Punishment_Detention;
		}
		
		# Merit Actions
		if(empty($RecordActionAry)) {
			$returnAry["RecordAction"] = "---";
		}
		else {
			$returnAry["RecordAction"] = implode("<br/>", (array)$RecordActionAry);
		}
		
		# Merit PICs
		$RecordPICs = $dataAry["PICs"];
		if($RecordPICs != "" && $RecordPICs != 0) {
	        $RecordPICs = $this->getPICNameList($RecordPICs);
		}
		$returnAry["RecordPICs"] .= $RecordPICs ? $RecordPICs : "---";
		
		# Merit History
		$fromConductRecords = $dataAry["fromConductRecords"];
		$CaseID = $dataAry["CaseID"];
		if($fromConductRecords) {
			$returnAry["RecordReference"] = $eDiscipline["HISTORY"];
		}
		else if($CaseID) {
			$CaseRecordInfo = $this->getCaseRecordByCaseID($CaseID, $this);
			$returnAry["RecordReference"] = $i_Discipline_System_Discipline_Case_Record_Case." ". $CaseRecordInfo[0]["CaseNumber"] ."</a>";
		}
		else {
			$returnAry["RecordReference"] = "---";
		}
		
		return $returnAry;
	}
	
	# Insert Merit Records
	public function insertMeritRecordDataChecking($dataAry)
	{
		global $sys_custom;
		
		# Get POST Data
		$student_ids = $dataAry["sld_student"];
		$ap_record_date = $dataAry["date"];
		$ap_record_type = $dataAry["ap_type_select"];
		$ap_record_cat = $dataAry["ap_category_select"];
		$ap_record_item = $dataAry["ap_item_select"];
		$ap_record_merit_num = $dataAry["ap_merit_num"];
		$ap_record_conduct_score = $dataAry["ap_merit_conduct"];
		$ap_record_study_score = $dataAry["ap_merit_study_score"];
		$ap_record_activity_score = $dataAry["ap_merit_activity_score"];
		$ap_record_pics = $dataAry["pics"];
		//$selected_ap_merit_type = $ap_record_type=="1"? $dataAry["ap_merit_type"] : $dataAry["ap_demerit_type"];
		//$selected_ap_conduct_type = $ap_record_type;
		//$ap_record_remarks = $dataAry["remarks"];
		//$selected_ap_files = $dataAry["attachment"];
		
		# Check Empty Input
		if (empty($student_ids)) {
			return $jsError = "noSelectedStudents";
		}
		if (empty($ap_record_date)) {
			return $jsError = "noSelectedDate";
		}
		if (empty($ap_record_type) || empty($ap_record_cat) || empty($ap_record_item)) {
			return $jsError = "noSelectedItem";
		}
		if (empty($ap_record_pics)) {
			return $jsError = "noSelectedPICs";
		}
		
		# Check Record Date - Empty Date
		$ap_record_date_stamp = strtotime($ap_record_date);
		$ap_record_date = date("Y-m-d", $ap_record_date_stamp);
		if (is_date_empty($ap_record_date)) {
			return $jsError = "dateNotAllow";
		}
		
		# Check Record Date - Date Range
		// Get max no. of day(s) before
		$NumOfMaxRecordDays = $this->MaxRecordDayBeforeAllow;
		if(!$this->isEJ() && $NumOfMaxRecordDays > 0) {
			$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
			$DateMinLimit = strtotime($DateMinLimit);
			$DateMaxLimit = strtotime(date("Y-m-d"));
			if(($DateMinLimit > $ap_record_date_stamp) || ($ap_record_date_stamp > $DateMaxLimit)) {
				return $jsError = "dateNotAllow";
			}
		}
		else {
			$DateMaxLimit = strtotime(date("Y-m-d"));
			if($ap_record_date_stamp > $DateMaxLimit) {
				return $jsError = "dateNotAllow";
			}
		}
		
		# Check Record Date - Valid Semester
		if($this->isEJ())
		{
			if(getSemesterMode()==2 && retrieveSemester($ap_record_date)==false) {
				return $jsError = "noSemesterSettings";
			}	
		}
		
		# Check Record Date - System Setting > Not allow same student with same "Award & Punishment" or "Good Conduct & Misconduct" item record in a same day
		if(!$this->isEJ())
		{
			if($this->NotAllowSameItemInSameDay) {
				foreach((array)$student_ids as $this_studentid) {
					if($this->checkSameAPItemInSameDay($this_studentid, $ap_record_item, $ap_record_date)==1) {
						return $jsError = "APInSameDay";
					}
				}
			}
		}
		
		# Check Merit Details - Empty
		if($this->NotAllowEmptyInputInAP) {
			$MeritNumCheck = $ap_record_merit_num <= 0;
			$ConductMarkCheck = $this->Hidden_ConductMark || (!$this->Hidden_ConductMark && $ap_record_conduct_score <= 0);
			$StudyScoreCheck =  !$this->UseSubScore || ($this->UseSubScore && $ap_record_study_score <= 0);
			$ActivityScoreCheck = !$this->UseActScore || ($this->UseActScore && $ap_record_activity_score <= 0);
		    if($MeritNumCheck && $ConductMarkCheck && $StudyScoreCheck && $ActivityScoreCheck) {
				return $jsError = "emptyMeritInput";
		    }
    	}
    	
    	# Check Merit Item Study Score - Maximum Gain / Deduct Mark
    	if(!$this->isEJ())
    	{
    		if($this->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
    			$thisMeritItem = $this->returnMeritItemByID($ap_record_item);
    			$thisMeritMaxStudyScore = $thisMeritItem["MaxGainDeductSubScore1"];
    			if($thisMeritMaxStudyScore > 0 && $ap_record_study_score > $thisMeritMaxStudyScore) {
    				return $jsError = "exceedInputPoint";
    			}
    		}
    	}
    	
		return $jsError = "";
	}
	public function insertMeritRecordByAPI($dataAry)
	{
		// Get POST data
		$thisStudentAry = $dataAry["sld_student"];
		$thisRecordDate = $dataAry["date"];
		$thisRecordDate = date("Y-m-d", strtotime($thisRecordDate));
		$thisRecordSemester = $dataAry["selectSemester"];
		$thisMeritType = $dataAry["ap_type_select"];
		$thisMeritCatID = $dataAry["ap_category_select"];
		$thisMeritItemID = $dataAry["ap_item_select"];
		$thisMeritItem = $this->returnMeritItemByID($thisMeritItemID);
		$thisMeritItem = $thisMeritItem["ItemName"];
		$thisMeritItem = addslashes($thisMeritItem);
		$thisMeritRecordType = $thisMeritType=="1"? $dataAry["ap_merit_type"] : $dataAry["ap_demerit_type"];
		$thisMeritRecordCount = $dataAry["ap_merit_num"];
		$thisMeritConductScore = $dataAry["ap_merit_conduct"];
		$thisMeritConductScore = $thisMeritConductScore * $thisMeritType;
		$thisMeritStudyScore = $dataAry["ap_merit_study_score"];
		$thisMeritStudyScore = $thisMeritStudyScore * $thisMeritType;
		$thisMeritActivityScore = $dataAry["ap_merit_activity_score"];
		$thisMeritActivityScore = $thisMeritActivityScore * $thisMeritType;
		$thisMeritPIC = $dataAry["pics"];
		$thisMeritPIC = implode(",", (array)$thisMeritPIC);
		$thisRemark = $dataAry["remarks"];
		$thisRemark = intranet_htmlspecialchars($thisRemark);
		if($this->isEJ()) {
			$thisRemark = convert2unicode($thisRemark, 1, 0);
		}
		//$thisAttachment = $sessionTime."_".$StudentID."tmp";
		//$thisActionID = ${"ActionID_".$StudentID};
		//$thisGMCount = ${"GMCount_".$StudentID};

        // [2019-1105-1700-25235]
        if($this->use_subject)
        {
            global $intranet_root;

            include_once $intranet_root."/includes/libteaching.php";
            $lteaching = new libteaching();

            $thisMeritSubjectID = $dataAry["ap_subject_select"];
            $thisMeritSubjectName = $lteaching->returnSubjectName($thisMeritSubjectID);
        }
		
		// Get Year & Semester
		if(!$this->isEJ())
		{
			$thisYearName = GET_ACADEMIC_YEAR3($thisRecordDate);
			$thisYearID = $this->getAcademicYearIDByYearName($thisYearName);
			$thisSemInfo = getAcademicYearAndYearTermByDate($thisRecordDate);
			$thisSemesterID = $thisSemInfo[0];
			$thisSemesterName = $thisSemInfo[1];
		}
		else
		{
			$thisYearName = GET_ACADEMIC_YEAR_WITH_FORMAT($thisRecordDate);
			if(getSemesterMode()==2) {
				$thisSemesterName = retrieveSemester($thisRecordDate);
			}
			else {
				$thisSemesterName = convert2unicode($thisRecordSemester, 1, 0);
			}
		}
		
		// Update Merit Profile Type and Count
		$thisMeritRecordType = $thisMeritRecordCount=="0.00"? "-999" : $thisMeritRecordType;
		$thisMeritRecordCount = $thisMeritRecordType=="-999"? "0.00" : $thisMeritRecordCount;
		
		// loop Students
		$NewMeritIDAry = array();
		$thisStudentAry = array_unique((array)$thisStudentAry);
		foreach((array)$thisStudentAry as $thisStudentID)
		{
			# INSERT INTO [DISCIPLINE_MERIT_RECORD (Pending)]
			$dataAry = array();
			if(!$this->isEJ())
			{
				$dataAry["RecordDate"] = $thisRecordDate;
				$dataAry["RecordInputDate"] = $RecordInputDate ? $RecordInputDate : date("Y-m-d");
				$dataAry["Year"] = $thisYearName;
				$dataAry["Semester"] = addslashes($thisSemesterName);
				$dataAry["AcademicYearID"] = $thisYearID;
				$dataAry["YearTermID"] = $thisSemesterID;
				$dataAry["StudentID"] = $thisStudentID;
				$dataAry["ItemID"] = $thisMeritItemID;
				$dataAry["ItemText"] = $thisMeritItem;
				$dataAry["MeritType"] = $thisMeritType;
				$dataAry["ProfileMeritType"] = $thisMeritRecordType;
				$dataAry["ProfileMeritCount"] = $thisMeritRecordCount;
				$dataAry["ConductScoreChange"] = $thisMeritConductScore;
				$dataAry["SubScore1Change"] = $thisMeritStudyScore;
				$dataAry["SubScore2Change"] = $thisMeritActivityScore;
				$dataAry["PICID"] = $thisMeritPIC;
				$dataAry["Remark"] = $thisRemark;
				$dataAry["RecordStatus"] = DISCIPLINE_STATUS_PENDING;
				//$dataAry["ActionDueDate"] = $ActionDueDate;
				//$dataAry["Subject"] = $SubjectName;
				//$dataAry["SubjectID"] = $SubjectID;
				//$dataAry["Attachment"] = $sessionTime."_".$StudentID."tmp";

                // [2019-1105-1700-25235]
                if($this->use_subject) {
                    $dataAry['Subject'] = $thisMeritSubjectName;
                    $dataAry['SubjectID'] = $thisMeritSubjectID;
                }
			}
			else
			{
				$dataAry["RecordDate"] = $thisRecordDate;
				$dataAry["Year"] = $thisYearName;
				$dataAry["Semester"] = $thisSemesterName;
				$dataAry["StudentID"] = $thisStudentID;
				$dataAry["ItemID"] = $thisMeritItemID;
				$dataAry["ItemText"] = $thisMeritItem;
				$dataAry["MeritType"] = $thisMeritType;
				$dataAry["ProfileMeritType"] = $thisMeritRecordType;
				$dataAry["ProfileMeritCount"] = $thisMeritRecordCount;
				$dataAry["ConductScoreChange"] = $thisMeritConductScore;
				$dataAry["PICID"] = $thisMeritPIC;
				$dataAry["Remark"] = $thisRemark;
				$dataAry["RecordStatus"] = DISCIPLINE_STATUS_PENDING;

                // [2019-1105-1700-25235]
                if($this->use_subject) {
                    $dataAry['Subject'] = $thisMeritSubjectName;
                }
			}
			
			$MeritRecordID = $this->INSERT_MERIT_RECORD($dataAry);
			if($MeritRecordID > 0) {
				$NewMeritIDAry[] = $MeritRecordID;
				
				# Set Auto Approve
				if(!$this->isEJ())
				{
					if($this->AutoApproveAPRecord) {
						# Check if need approval or not
						$NeedApproval = $this->CHECK_APPROVAL_REQUIRED($thisMeritRecordType, $thisMeritRecordCount);
						$UserCanApprove = $this->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($MeritRecordID);
						if($NeedApproval && ($UserCanApprove || $this->IS_ADMIN_USER($_SESSION["UserID"]))) {
							$this->APPROVE_MERIT_RECORD($MeritRecordID, 1, 0);
						}
						else if(!$NeedApproval && ($this->IS_ADMIN_USER($_SESSION["UserID"]) || $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval"))) {
							$this->APPROVE_MERIT_RECORD($MeritRecordID, 1, 0);
						}
					}
				}
				else
				{
					# Check if need approval or not
					$NeedApproval = $this->CHECK_APPROVAL_REQUIRED($thisMeritRecordType, $thisMeritRecordCount);
					if($NeedApproval) {
						// do nothing
					}
					else {
						$this->APPROVE_MERIT_RECORD($MeritRecordID);
					}
				}
			}
		}
		return $NewMeritIDAry;
	}
	
	# Delete Merit Record
	public function deleteMeritRecordByAPI($RecordID)
	{
		if($RecordID) {
			$this->DELETE_MERIT_RECORD($RecordID);
		}
		
		return true;
	}
	
	# Edit Merit Records
	public function updateMeritRecordByAPI($dataAry)
	{
		global $sys_custom;
		
		// Get POST data
		$thisMeritID = $dataAry["MeritID"][0];
		$thisRecordDate = $dataAry["date"];
		$thisRecordSemester = $dataAry["selectSemester"];
		$thisCatID = $dataAry["ap_category_select"];
		$thisItemID = $dataAry["ap_item_select"];
		$thisMeritType = $dataAry["ap_merit_type"];
		$thisMeritCount = $dataAry["ap_merit_num"];
		$thisConductScore = $dataAry["ap_merit_conduct"];
		$thisStudyScore = $dataAry["ap_merit_study_score"];
		$thisActivityScore = $dataAry["ap_merit_activity_score"];
		$thisPIC = $dataAry["pics"];
		$thisRemark = $dataAry["remarks"];
		$thisRemark = intranet_htmlspecialchars($thisRemark);
		if($this->isEJ()) {
			$thisRemark = convert2unicode($thisRemark, 1, 0);
		}

        // [2019-1105-1700-25235]
        if($this->use_subject)
        {
            global $intranet_root;

            include_once $intranet_root."/includes/libteaching.php";
            $lteaching = new libteaching();

            $thisMeritSubjectID = $dataAry["ap_subject_select"];
            $thisMeritSubjectName = $lteaching->returnSubjectName($thisMeritSubjectID);
        }
		
		// Get Merit Info
		$MeritRecord = $this->GetMeritRecord("", $thisMeritID);
		$MeritStudentID = $MeritRecord["StudentID"];
		$MeritCatID = $MeritRecord["CatID"];
		$MeritItemID = $MeritRecord["ItemID"];
		$MeritRecordType = $MeritRecord["MeritType"];
		$MeritRecordStatus = $MeritRecord["RecordStatus"];
		$MeritWaived = $MeritRecordStatus==2;
		$MeritFromConducts = $MeritRecord["fromConductRecords"];

		# Check if User can access Item
		if(!$this->isEJ())
		{
			$AccessItem = 0;
			if($this->IS_ADMIN_USER($_SESSION["UserID"])) {
				$AccessItem = 1;
			}
			else {
				$AccessItemList = $this->Retrieve_User_Can_Access_AP_Category_Item($_SESSION["UserID"], "item");
				if($AccessItemList[$MeritCatID])
					$AccessItem = in_array($MeritItemID, $AccessItemList[$MeritCatID]);
			}
		}
		else
		{
			$AccessItem = 1;
		}
		
		# Check Empty Input
		if (empty($thisMeritID)) {
			return $jsErrorMsg = "noFormData";
		}

		# Check Record Date (DISABLED - Upgraded from Conduct Records / Wavied Merit Records)
		$skipDateChecking = $MeritFromConducts || $MeritWaived;
		if (!$skipDateChecking)
		{
			# Check Empty Input
			if (empty($thisRecordDate)) {
				return $jsError = "noSelectedDate";
			}
			
			# Check Record Date - Empty Date
			$thisRecordDateStamp = strtotime($thisRecordDate);
			$thisRecordDate = date("Y-m-d", $thisRecordDateStamp);
			if (is_date_empty($thisRecordDate)) {
				return $jsError = "dateNotAllow";
			}
		
			# Check Record Date - Date Range
			// Get max no. of day(s) before
			$NumOfMaxRecordDays = $this->MaxRecordDayBeforeAllow;
			if(!$this->isEJ() && $NumOfMaxRecordDays > 0) {
				$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
				$DateMaxLimit = strtotime(date("Y-m-d"));
				if(($DateMinLimit > $thisRecordDateStamp) || ($thisRecordDateStamp > $DateMaxLimit)) {
					return $jsError = "dateNotAllow";
				}
			}
			else {
				$DateMaxLimit = strtotime(date("Y-m-d"));
				if($thisRecordDateStamp > $DateMaxLimit) {
					return $jsError = "dateNotAllow";
				}
			}
		
			# Check Record Date - Valid Semester
			if($this->isEJ())
			{
				if(getSemesterMode()==2 && retrieveSemester($thisRecordDate)==false) {
					return $jsError = "noSemesterSettings";
				}	
			}
			
			# Check Record Date - System Setting > Not allow same student with same "Award & Punishment" or "Good Conduct & Misconduct" item record in a same day
			if(!$this->isEJ())
			{
				if($this->NotAllowSameItemInSameDay) {
					$thisCheckItemID = $thisItemID? $thisItemID : $MeritItemID;
					if($this->checkSameAPItemInSameDay($MeritStudentID, $thisCheckItemID, $thisRecordDate, $thisMeritID)==1) {
						return $jsError = "APInSameDay";
					}
				}
			}
		}
		
		# Check Merit Details (DISABLED - Cannot access Item / Upgraded from Conduct Records / Wavied Merit Records)
		$skipDetailsChecking = !$AccessItem || $MeritFromConducts || $MeritWaived;
		if (!$skipDetailsChecking)
		{
			# Check Empty Input
			if (empty($thisItemID)) {
				return $jsErrorMsg = "noSelectedItem";
			}
			
			// Update Merit Profile Type and Count
			$thisMeritType = $thisMeritCount=="0.00"? "-999" : $thisMeritType;
			$thisMeritCount = $thisMeritType=="-999"? "0.00" : $thisMeritCount;
			
			# Check Merit Details - Empty
			if($this->NotAllowEmptyInputInAP) {
				$MeritNumCheck = $thisMeritCount <= 0;
				$ConductMarkCheck = $this->Hidden_ConductMark || (!$this->Hidden_ConductMark && $thisConductScore <= 0);
				$StudyScoreCheck =  !$this->UseSubScore || ($this->UseSubScore && $thisStudyScore <= 0);
				$ActivityScoreCheck = !$this->UseActScore || ($this->UseActScore && $thisActivityScore <= 0);
			    if($MeritNumCheck && $ConductMarkCheck && $StudyScoreCheck && $ActivityScoreCheck) {
					return $jsError = "emptyMeritInput";
			    }
	    	}
	    	
	    	# Check Merit Item Study Score - Maximum Gain / Deduct Mark
	    	if(!$this->isEJ())
	    	{
	    		if($this->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
	    			$thisMeritItem = $this->returnMeritItemByID($thisItemID);
	    			$thisMeritMaxStudyScore = $thisMeritItem["MaxGainDeductSubScore1"];
	    			if($thisMeritMaxStudyScore > 0 && $thisStudyScore > $thisMeritMaxStudyScore) {
	    				return $jsError = "exceedInputPoint";
	    			}
	    		}
	    	}
		}
		$thisConductScore = $thisConductScore * $MeritRecordType;
		$thisStudyScore = $thisStudyScore * $MeritRecordType;
		$thisActivityScore = $thisActivityScore * $MeritRecordType;
		
		# Check PICs (DISABLED - System Setting - Only allow admin to select record's PIC
		$skipPICsChecking = $MeritFromConducts || $MeritWaived || ($this->OnlyAdminSelectPIC && !$this->IS_ADMIN_USER($_SESSION["UserID"]));
		$skipPICsChecking = $skipPICsChecking && !$this->isEJ();
		if (empty($thisPIC) && !$skipPICsChecking) {
			return $jsErrorMsg = "noSelectedPICs";
		}
		$thisPIC = (empty($thisPIC)) ? "" : implode(",", (array)$thisPIC);
		
		# Update Merit Info Data
		$dataAry = array();
		$dataAry["Remark"] = $thisRemark;
		$dataAry["PICID"] = $thisPIC;
		
		# Merit Info Data - Date
		if (!$skipDateChecking)
		{
			// Get Year & Semester
			if(!$this->isEJ())
			{
				$SchoolYear = GET_ACADEMIC_YEAR3($thisRecordDate);
				$Semester = retrieveSemester($thisRecordDate);
				$AcademicInfoArray = getAcademicYearInfoAndTermInfoByDate($thisRecordDate);
				if(sizeof($AcademicInfoArray) > 0){
					$AcademicYearID = $AcademicInfoArray[0];
					$YearTermID = $AcademicInfoArray[2];
				}
				
				$dataAry["StudentID"] = $MeritStudentID;
				$dataAry["RecordDate"] = $thisRecordDate;
				$dataAry["Year"] = $SchoolYear;
				$dataAry["Semester"] = $Semester;
				if($AcademicYearID != "") {
					$dataAry["AcademicYearID"] = $AcademicYearID;
				}
				if($YearTermID != "") {
					$dataAry["YearTermID"] = $YearTermID;
				}
			}
			else
			{
				$thisYearName = GET_ACADEMIC_YEAR_WITH_FORMAT($thisRecordDate);
				if(getSemesterMode()==2) {
					$thisSemesterName = retrieveSemester($thisRecordDate);
				}
				else {
					$thisSemesterName = convert2unicode($thisRecordSemester, 1, 0);
				}
				
				$dataAry["StudentID"] = $MeritStudentID;
				$dataAry["RecordDate"] = $thisRecordDate;
				$dataAry["Year"] = $thisYearName;
				$dataAry["Semester"] = $thisSemesterName;
			}
		}
		
		# Merit Info Data - Details
		if(!$skipDetailsChecking)
		{
			$dataAry["StudentID"] = $MeritStudentID;
			if($AccessItem) 
			{
				if(!$this->isEJ())
				{
					$dataAry["ItemID"] = $thisItemID;
					$MeritItemInfo = $this->returnMeritItemByID($thisItemID);
					$MeritItemText = $MeritItemInfo["ItemName"];
					$MeritItemText = addslashes($MeritItemText);
					$dataAry["ItemText"] = $MeritItemText;
					$dataAry["MeritType"] = $MeritRecordType;
					$dataAry["ProfileMeritType"] = $thisMeritType;
					$dataAry["ProfileMeritCount"] = $thisMeritCount;
					$dataAry["ConductScoreChange"] = $thisConductScore;
					$dataAry["SubScore1Change"] = $thisStudyScore;
					$dataAry["SubScore2Change"] = $thisActivityScore;
				}
				else
				{
					$dataAry["ItemID"] = $thisItemID;
					$MeritItemInfo = $this->returnMeritItemByID($thisItemID);
					$MeritItemText = $MeritItemInfo["ItemName"];
					$MeritItemText = addslashes($MeritItemText);
					$dataAry["ItemText"] = $MeritItemText;
					$dataAry["MeritType"] = $MeritRecordType;
					$dataAry["ProfileMeritType"] = $thisMeritType;
					$dataAry["ProfileMeritCount"] = $thisMeritCount;
					$dataAry["ConductScoreChange"] = $thisConductScore;
				}
			}

            // [2019-1105-1700-25235]
            if($this->use_subject) {
                $dataAry['Subject'] = $thisMeritSubjectName;
                if(!$this->isEJ()) {
                    $dataAry['SubjectID'] = $thisMeritSubjectID;
                }
            }
		}
		
		# Update Merit Record
		$this->UPDATE_MERIT_RECORD($dataAry, $thisMeritID);
		
		return "";
	}
	
	# Update Merit Record Status
	public function updateMeritRecordStatusByAPI($dataAry)
	{
		// Get POST data
		$thisMeritID = $dataAry["MeritID"][0];
		$thisRecordStatus = $dataAry["ap_record_status"];
		$thisRecordReleaseStatus = $dataAry["ap_record_release_status"];
		
		// Get Merit Info
		$MeritRecord = $this->RETRIEVE_MERIT_RECORD_INFO_BY_ID($thisMeritID);
		$MeritRecord = $MeritRecord[0];
		$thisRecordRedeemID = $MeritRecord["RedeemID"];
		$thisRecordOldStatus = $MeritRecord["RecordStatus"];
		$thisRecordOldReleaseStatus = $MeritRecord["ReleaseStatus"];
					
		# CHANGED: Record Status
		if($thisRecordStatus != $thisRecordOldStatus)
		{
			# APPROVED
			if($thisRecordStatus == DISCIPLINE_STATUS_APPROVED) {
				if(!$this->isEJ()) 
				{
					$CanApprove = $this->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($thisMeritID);
				}
				else
				{
					$CanApprove = true;
				}
				if($CanApprove) {
					$this->APPROVE_MERIT_RECORD($thisMeritID);
				}
			}
			# REJECTED
			else if($thisRecordStatus == DISCIPLINE_STATUS_REJECTED) {
				if(!$this->isEJ()) 
				{
					$CanApprove = $this->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($thisMeritID);
				}
				else
				{
					$CanApprove = true;
				}
				if($CanApprove) {
					$this->REJECT_MERIT_RECORD($thisMeritID);
				}
			}
			# WAIVED
			else if($thisRecordStatus == DISCIPLINE_STATUS_WAIVED) {
				$this->WAIVE_MERIT_RECORD($thisMeritID, ""); 
			}
			# UNWAVIE
			else if($thisRecordStatus == 9 && $thisRecordOldStatus == DISCIPLINE_STATUS_WAIVED) {
				$this->UNWAIVE_MERIT_RECORD($thisMeritID);
			}
			# UNREDEEM
			else if($thisRecordStatus == 99 && $thisRecordOldStatus == DISCIPLINE_STATUS_WAIVED && $thisRecordRedeemID > 0) {
				if(!$this->isEJ()) 
				{
					$this->Start_Trans();
					$success = $this->unredeemMeritRecords($thisRecordRedeemID);
					if ($success) {
						$this->Commit_Trans();
					}
					else {
						$this->RollBack_Trans();
					}
				}
				else
				{
					$this->unredeemMeritRecords($thisRecordRedeemID);
				}
			}
		}
		// CHANGED: Record Release Status
		else if ($thisRecordReleaseStatus != $thisRecordOldReleaseStatus)
		{
			# RELEASED
			if($thisRecordReleaseStatus == DISCIPLINE_STATUS_RELEASED)
			{
				// Release Merit Records
				if(($thisRecordStatus == DISCIPLINE_STATUS_APPROVED || $thisRecordStatus == DISCIPLINE_STATUS_WAIVED) && $thisRecordOldReleaseStatus != DISCIPLINE_STATUS_RELEASED)
				{
					global $PATH_WRT_ROOT, $intranet_root, $sys_custom, $eDiscipline;
					include_once $intranet_root."/includes/libnotice.php";
					$lnotice = new libnotice();
				
					# UPDATE [DISCIPLINE_MERIT_RECORD]
					$this->UPDATE_MERIT_RECORD_RELEASE_STATUS($thisMeritID, DISCIPLINE_STATUS_RELEASED);
					
					# Send eNotice if no notice is sent
					$TemplateID = $MeritRecord["TemplateID"];
					$NoticeID = $MeritRecord["NoticeID"];
					if($thisRecordStatus == DISCIPLINE_STATUS_APPROVED && !$lnotice->disabled && $TemplateID && !$NoticeID)
					{
						$TemplateInfo = $this->RETRIEVE_NOTICE_TEMPLATE_INFO($TemplateID);
						if(sizeof($TemplateInfo))
						{
							# Build Special Data
							$SpecialData = array();
							$SpecialData["MeritRecordID"] = $thisMeritID;
							
							# Build Template Data
							$template_category = $TemplateInfo[0]["CategoryID"];
							$student_id = $MeritRecord["StudentID"];
							$template_other_info = $MeritRecord["TemplateOtherInfo"];
							$template_data = $this->TEMPLATE_VARIABLE_CONVERSION($template_category, $student_id, $SpecialData, $template_other_info);
							//$UserName = $this->getUserNameByID($_SESSION["UserID"]);
							$Module = $this->Module;
							$NoticeNumber = time();
							$TemplateID = $TemplateID;
							$Variable = $template_data;
							$IssueUserID = $_SESSION["UserID"];
							$IssueUserName = $eDiscipline["DisciplineName"];
							$TargetRecipientType = "U";
							$RecipientID = array($student_id);
							$RecordType = NOTICE_TO_SOME_STUDENTS;
							
							# Set Notice Parameter
							include_once($PATH_WRT_ROOT."includes/libucc.php");
							$lc = new libucc();
							$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

                            // [2020-1009-1753-46096] delay notice start date & end date
                            if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
                            {
                                $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                                $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                                $lc->setDateStart($DateStart);

                                if ($lc->defaultDisNumDays > 0) {
                                    $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                                    $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                                    $lc->setDateEnd($DateEnd);
                                }
                            }
							
							# Send Notice
							$NoticeID = $lc->sendNotice();
							
							# Update Merit Record
							if($NoticeID != -1) {
								$updateDataAry = array();
								$updateDataAry["NoticeID"] = $NoticeID;
								$this->UPDATE_MERIT_RECORD($updateDataAry, $thisMeritID);
							}
						} 
					}
					// Distribute Notice
					else if(!$lnotice->disabled && !$sys_custom["eDiscipline"]["NotReleaseNotice"] && $NoticeID)
					{
						$lnotice->changeNoticeStatus($NoticeID, DISTRIBUTED_NOTICE_RECORD);
					}
				}
			}
			# UNRELEASED
			else if($thisRecordReleaseStatus == DISCIPLINE_STATUS_UNRELEASED)
			{
				$this->UPDATE_MERIT_RECORD_RELEASE_STATUS($thisMeritID, DISCIPLINE_STATUS_UNRELEASED);
			}
		}
	}
	
	# Assign Notice to Merit Records
	public function assignMeritNoticeByAPI($dataAry)
	{
		global $intranet_root, $PATH_WRT_ROOT, $sys_custom;
		
		include_once $intranet_root."/includes/libnotice.php";
		$lnotice = new libnotice();
		
		// Get POST Data
		$merit_ids = $dataAry["MeritID"];
		$send_notice = $dataAry["send_notice"];
		$notice_template = $dataAry["notice_template"];
		$additional_info = $dataAry["additional_info"];
		if($this->isEJ()) {
			$additional_info = convert2unicode($additional_info, 1, 0);
		}
		$send_email_notify = $dataAry["email_notify"];
		//$notice_cat = $dataAry["notice_category"];
		
		# Check if allow to send Notice
		if(!$lnotice->disabled && $send_notice && !empty($merit_ids)) {
			$template_info = $this->RETRIEVE_NOTICE_TEMPLATE_INFO($notice_template);
			if(sizeof($template_info) && $notice_template > 0)
			{
				foreach((array)$merit_ids as $this_meritid) {
					# Check if Merit exist
					$MeritRecord = $this->GetMeritRecord("", $this_meritid);
					if(!empty($MeritRecord))
					{
						# Get Merit Info
						$merit_category = $MeritRecord["CatID"];
						$merit_studentid = $MeritRecord["StudentID"];
						
						# Build Special Data
						$SpecialData = array();
						$SpecialData["MeritRecordID"] = $this_meritid;
						
						# Build Template Data
						$template_category = $template_info[0]["CategoryID"];
						$includeReferenceNo = 1;
						$template_data = $this->TEMPLATE_VARIABLE_CONVERSION($template_category, $merit_studentid, $SpecialData, $additional_info, $includeReferenceNo);
						$UserName = $this->getUserNameByID($this->UserID);
						$Module = $this->Module;
						$NoticeNumber = time();
						$TemplateID = $notice_template;
						$Variable = $template_data;
						$IssueUserID = $_SESSION["UserID"];
						$IssueUserName = $UserName[0];
						$TargetRecipientType = "U";
						$RecipientID = array($merit_studentid);
						$RecordType = NOTICE_TO_SOME_STUDENTS;
						
						# Set Notice Parameter
						include_once $intranet_root."/includes/libucc.php";
						$lc = new libucc();
						$lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

                        // [2020-1009-1753-46096] delay notice start date & end date
                        if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
                        {
                            $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                            $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                            $lc->setDateStart($DateStart);

                            if ($lc->defaultDisNumDays > 0) {
                                $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                                $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                                $lc->setDateEnd($DateEnd);
                            }
                        }
						
						# Send Notice (Suspend Notice if not released)
						if(!isset($send_email_notify) || $send_email_notify=="") $send_email_notify = 0;
						$NoticeID = $lc->sendNotice($send_email_notify);
						if($MeritRecord["ReleaseStatus"] != DISCIPLINE_STATUS_RELEASED) {
							$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
						}
						
						# Update Merit Record
						$dataAry = array();
						$dataAry["TemplateID"] = $TemplateID;
						$dataAry["TemplateOtherInfo"] = $additional_info;
						if($NoticeID != -1) {
							$dataAry["NoticeID"] = $NoticeID;
						}
						$this->UPDATE_MERIT_RECORD($dataAry, $this_meritid);
					}
				}
			}
		}
	}
	
	/*************** UI Functions ****************/
	public function GetConductStatusButtons($dataAry)
	{
		global $Lang;
		
		$thisConductID = $dataAry["RecordID"];
		$isOwnConduct = $this->isConductPIC($thisConductID) || $this->isConductOwn($thisConductID);
		
		// 1. Conduct Details
		$thisActionButtons = "";
		$thisActionButtons .= "	<a href='#' rel='ediscipline/goodconduct_misconduct/management/detail' title='Browse Detail' class='link_target'>
									<img src='".$this->ImagePath."/icon_search.png' class='img_target' data-id='".$thisConductID."' width='20px'>
								</a>";

		// 2. Edit Conduct 
		$HasEditRight = $this->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || ($isOwnConduct && $this->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn"));
		$HasApprovalRight = $this->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Approval");
		if($HasEditRight || $HasApprovalRight) {
			$thisActionButtons .= "	<a href='#' rel='ediscipline/goodconduct_misconduct/management/edit_record' title='Edit' class='link_target'>
										<img src='".$this->ImagePath."/icon_edit.png' class='img_target' data-id='".$thisConductID."' width='20px'>
									</a>";
		}
		
		// 3. Send Conduct Notice (Block if already sent notice)
		if($HasEditRight) {
			$NoticeID = $dataAry["NoticeID"];
			if($NoticeID > 0) {
				$thisActionButtons .= "	<a href='javascript:void(0)' title='Add Notice' onclick='alert(\"".$Lang['eDiscipline']['App']['RecordHasSentNotice']."\")'>
											<img src='".$this->ImagePath."/icon_enotice.png' class='img_target' data-id='".$thisConductID."' width='20px'>
										</a>";
			}
			else {
				$thisActionButtons .= "	<a href='#' rel='ediscipline/goodconduct_misconduct/management/edit_step3' title='Add Notice' class='link_target'>
											<img src='".$this->ImagePath."/icon_enotice.png' class='img_target' data-id='".$thisConductID."' width='20px'>
										</a>";
			}
		}
		
		// 4. Delete Conduct (Block if late or wavied)
		$HasDeleteRight = $this->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteAll") || ($isOwnConduct && $this->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteOwn"));
		if($HasDeleteRight) {
			$CategoryID = $dataAry["CategoryID"];
			$RecordStatus = $dataAry["RecordStatus"];
			if($CategoryID == PRESET_CATEGORY_LATE) {
				$thisActionButtons .= "	<a href='javascript:void(0)' title='Delete' onclick='alert(\"".$Lang['eDiscipline']['App']['PleaseDeleteLateRecordInAttendance']."\")'>
											<img src='".$this->ImagePath."/icon_trash.png' class='img_target' data-id='".$thisConductID."' width='20px'>
										</a>";
			}
			else if ($RecordStatus == DISCIPLINE_STATUS_WAIVED) {
				$thisActionButtons .= "	<a href='javascript:void(0)' title='Delete' onclick='alert(\"".$Lang['eDiscipline']['App']['CannotDeleteWaivedRecords']."\")'>
											<img src='".$this->ImagePath."/icon_trash.png' class='img_target' data-id='".$thisConductID."' width='20px'>
										</a>";
			}
			else {
				$thisActionButtons .= "	<a href='#myModal' rel='ediscipline/goodconduct_misconduct/management/delete' title='Delete' class='modallink link_target'>
											<img src='".$this->ImagePath."/icon_trash.png' class='img_target' data-id='".$thisConductID."' width='20px'>
										</a>";
			}
		}
		
//		# Convert to Unicode (EJ)
//		if($this->isEJ()) {
//			$thisActionButtons = convert2unicode($thisActionButtons, 1, 1);
//		}
		return $thisActionButtons;
	}
	
	public function GetMeritActionButtons($dataAry, $HasApprovalRight="")
	{
		global $Lang, $eDiscipline;
		
		$thisMeritID = $dataAry["RecordID"];
		$isOwnConduct = $this->isMeritRecordPIC($thisMeritID) || $this->isMeritRecordOwn($thisMeritID);
		
		// 1. Merit Details
		$thisActionButtons = "";
		$thisActionButtons .= "	<a href='#' rel='ediscipline/award_punishment/management/detail' title='Browse Detail' class='link_target'>
									<img src='".$this->ImagePath."/icon_search.png' class='img_target' data-id='".$thisMeritID."' width='20px'>
								</a>";
		
//		if($this->IS_ADMIN_USER()) {
//			$HasApprovalRight = true;
//			$CanApproveRecord = true;
//		}
//		else {
//			if($HasApprovalRight==="") {
//				$HasRightToRejectRecord = $this->RETURN_AP_RECORDID_CAN_BE_APPROVED($_SESSION["UserID"], "");
//				$HasApprovalRight = (sizeof($HasRightToRejectRecord)>0) ? true : false;
//			}
//			//$CanApproveRecord = $this->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($thisMeritID);
//			$CanApproveRecord = true;
//		}
//		$HasApprovalRight = $HasApprovalRight || $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-APPROVAL");
		
		// 2. Edit Merit 
		$HasEditRight = $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || ($isOwnConduct && $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn"));
		$HasApprovalRight = $this->isEJ()? $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval") : $this->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($thisMeritID);
		$HasReleaseRight = $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release");
		$HasWaiveRight = $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive");
		if($HasEditRight || $HasApprovalRight || $HasReleaseRight || $HasWaiveRight) {
			$thisActionButtons .= "	<a href='#' rel='ediscipline/award_punishment/management/edit_record' title='Edit' class='link_target'>
										<img src='".$this->ImagePath."/icon_edit.png' class='img_target' data-id='".$thisMeritID."' width='20px'>
									</a>";
		}
		
		// 3. Send Merit Notice (Block if already sent notice)
		$NoticeID = $dataAry["NoticeID"];
		if($NoticeID > 0) {
			$thisActionButtons .= "	<a href='javascript:void(0)' title='Add Notice' onclick='alert(\"".$Lang['eDiscipline']['App']['RecordHasSentNotice']."\")'>
										<img src='".$this->ImagePath."/icon_enotice.png' class='img_target' data-id='".$thisMeritID."' width='20px'>
									</a>";
		}
		else {
			$thisActionButtons .= "	<a href='#' rel='ediscipline/award_punishment/management/edit_step3' title='Add Notice' class='link_target'>
										<img src='".$this->ImagePath."/icon_enotice.png' class='img_target' data-id='".$thisMeritID."' width='20px'>
									</a>";
		}
		
		// 4. Delete Merit (Block if from Case / Conduct or redeemed)
		$HasDeleteRight = $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll") || ($isOwnConduct && $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn"));
		if($HasDeleteRight) {
			$fromConductRecords = $dataAry["fromConductRecords"];
			$CaseID = $dataAry["CaseID"];
			$RedeemID = $dataAry["RedeemID"];
			if($fromConductRecords > 0) {
				$thisActionButtons .= "	<a href='javascript:void(0)' title='Delete' onclick='alert(\"".$Lang['eDiscipline']['App']["RecordIsAcc"]."\")'>
											<img src='".$this->ImagePath."/icon_trash.png' class='img_target' data-id='".$thisMeritID."' width='20px'>
										</a>";
			}
			else if($CaseID > 0) {
				$thisActionButtons .= "	<a href='javascript:void(0)' title='Delete' onclick='alert(\"".$Lang['eDiscipline']['App']["RecordIsCase"]."\")'>
											<img src='".$this->ImagePath."/icon_trash.png' class='img_target' data-id='".$thisMeritID."' width='20px'>
										</a>";
			}
			else if($RedeemID > 0) {
				$thisActionButtons .= "	<a href='javascript:void(0)' title='Delete' onclick='alert(\"".$Lang['eDiscipline']['App']["RecordIsRedeemed"]."\")'>
											<img src='".$this->ImagePath."/icon_trash.png' class='img_target' data-id='".$thisMeritID."' width='20px'>
										</a>";
			}
			else {
				$thisActionButtons .= "	<a href='#myModal' rel='ediscipline/award_punishment/management/delete' title='Delete' class='modallink link_target'>
											<img src='".$this->ImagePath."/icon_trash.png' class='img_target' data-id='".$thisMeritID."' width='20px'>
										</a>";
			}
		}
//		if($this->isEJ()) {
//			$thisActionButtons = convert2unicode($thisActionButtons, 1, 1);
//		}
		return $thisActionButtons;
	}
	
	public function GetYearWebViewSelection($year_id, $from="goodconduct_misconduct")
	{
		global $i_Discipline_System_Award_Punishment_All_School_Year;
		
		// Get School Year
		$SchoolYearAry = $from=="goodconduct_misconduct"? $this->getGMSchoolYear($year_id) : $this->getAPSchoolYear($year_id);
		
		if(!$this->isEJ())
		{
			// Get Current Year Info
			$CurrentYearID = Get_Current_Academic_Year_ID();
			$CurrentYearName = $this->getAcademicYearNameByYearID($CurrentYearID);
					
			# Build Selection
			$selectHTML = "";
			$selectHTML .= "<select id='selectYear' name='selectYear' rel='selectSemester' class='select_ajaxload' target-module='".$from."'>";
			if($this->CHECK_ACCESS("Discipline-VIEWING-Award_Punishment-View")) {
				$selectHTML .= "<option value='0'";
				$selectHTML .= $year_id==0? " selected" : "";
				$selectHTML .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
			}
			for($i=0; $i<sizeof($SchoolYearAry); $i++) {
				list($thisYearID, $thisYearName) = $SchoolYearAry[$i];
				$selectHTML .= "<option value='".$thisYearID."'";
				if($thisYearID == $year_id) {
					$selectHTML .= " SELECTED";
				}
				if($thisYearID == $CurrentYearID) {
					$yearSelected = 1;
				}
				$selectHTML .= ">".$thisYearName."</option>";
			}
			if($yearSelected == 0) {
				$selectHTML .= "<option value='".$CurrentYearID."'";
				$selectHTML .= $year_id==$CurrentYearID? " selected" : "";
				$selectHTML .= ">".$CurrentYearName."</option>";
			}
			$selectHTML .= "</select>";
		}
		else
		{
			// Get Current Year Info
			$CurrentYear = getCurrentAcademicYear();
			
			# Build Selection
			$selectHTML = "";
			$selectHTML .= "<select id='selectYear' name='selectYear' rel='selectSemester' target-module='".$from."'>";
			$selectHTML .= "<option value='0'";
			$selectHTML .= $year_id==0? " selected" : "";
			$selectHTML .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
			for($i=0; $i<sizeof($SchoolYearAry); $i++) {
				$thisYearName = $SchoolYearAry[$i];
				$selectHTML .= "<option value='".$thisYearName."'";
				if($thisYearName == $year_id) {
					$selectHTML .= " SELECTED";
				}
				if($thisYearName == $CurrentYear) {
					$yearSelected = 1;
				}
				$selectHTML .= ">".$thisYearName."</option>";
			}
			if($yearSelected == 0) {
				$selectHTML .= "<option value='".$CurrentYear."'";
				$selectHTML .= $year_id==$CurrentYear? " selected" : "";
				$selectHTML .= ">".$CurrentYear."</option>";
			}
			$selectHTML .= "</select>";
			//$selectHTML = convert2unicode($selectHTML, 1, 1);
		}
		
		return $selectHTML;
	}
	
	public function GetSemesterWebViewSelection($year_id, $sem_id, $from="goodconduct_misconduct", $hideWholeYear=false)
	{
		global $intranet_root, $i_Discipline_System_Award_Punishment_Whole_Year;
		
		if(!$this->isEJ())
		{
			// Get Semester List
			$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$year_id."' ORDER BY TermStart";
			$SemesterAry = $this->returnArray($sql, 3);
				
			# Build Selection
			$selectHTML = "";
			$selectHTML .= "<select id='selectSemester' name='selectSemester' class='select_ajaxload' target-module='".$from."'>";
			$selectHTML .= "<option value='WholeYear'";
			$selectHTML .= $sem_id=="WholeYear" ? " selected" : "";
			$selectHTML .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
			for($i=0; $i<sizeof($SemesterAry); $i++) {
				list($this_id, $this_nameEN, $this_nameB5) = $SemesterAry[$i];
				$this_name = Get_Lang_Selection($this_nameB5, $this_nameEN);
				$selectHTML .= "<option value='".$this_id."'";
				$selectHTML .= $this_id==$sem_id? " selected" : "";
				$selectHTML .= ">".$this_name."</option>";
			}
			$selectHTML .= "</select>";
		}
		else
		{
			// Get Semester List
			$SemesterAry = explode("\n", get_file_content($intranet_root."/file/semester.txt"));
			
			# Build Selection
			$selectHTML = "";
			$selectHTML .= "<select id='selectSemester' name='selectSemester' target-module='".$from."'>";
			if(!$hideWholeYear) {
				$selectHTML .= "<option value='WholeYear'";
				$selectHTML .= $sem_id=="WholeYear" ? " selected" : "";
				$selectHTML .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
			}
			for($i=0; $i<sizeof($SemesterAry); $i++) {
				$CurrentSemester = $SemesterAry[$i];
				$CurrentSemester = explode("::", $CurrentSemester);
				list($this_name, $this_current) = $CurrentSemester;
				$selectHTML .= "<option value='".$this_name."'";
				$selectHTML .= $this_name==$sem_id? " selected" : "";
				$selectHTML .= ">".$this_name."</option>";
			}
			$selectHTML .= "</select>";
			//$selectHTML = convert2unicode($selectHTML, 1, 1);
		}
		return $selectHTML;
	}
	
	public function GetClassWebViewSelection($class_id, $year_id, $from="goodconduct_misconduct")
	{
		global $intranet_root;
		global $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select;
		
		if(!$this->isEJ())
		{
			include_once $intranet_root."/includes/libclass.php";	
			$lclass = new libclass();
			$select_class = $lclass->getSelectClassWithWholeForm("name=\"selectClass\" id=\"selectClass\" target-module=\"".$from."\" ", $class_id, $i_Discipline_System_Award_Punishment_All_Classes);
		}
		else
		{
			$select_class = $this->getSelectClass("name=\"selectClass\" id=\"selectClass\" target-module=\"".$from."\" ", $class_id, $i_Discipline_System_Award_Punishment_All_Classes);
			//$select_class = convert2unicode($select_class, 1, 1);
		}
		return $select_class;
	}

    public function getSubjectNameDropDownOption($Subject='')
    {
        global $intranet_root, $i_notapplicable;

        include_once $intranet_root."/includes/libteaching.php";
        $lteaching = new libteaching();

        $subjectList = $lteaching->returnSubjectList();

        $subject_option = "";
        $subject_option .= "<OPTION value=''>-- $i_notapplicable --</OPTION>\n";
        foreach ($subjectList as $k => $d)
        {
            $this_val = $this->isEJ() ? $d['SubjectName'] : $d['SubjectID'];
            $selected = $Subject == $this_val ? " selected" : "";
            $subject_option .= "<OPTION value='". $d[0] ."' $selected>". $d[1] ."</OPTION>\n";
        }
        $subject_option .= "</SELECT>\n";

        return $subject_option;
    }
	
	public function getTargetUserDropDownOption($UserIDAry)
	{
		if(!$this->isEJ())
		{
			# Fields
			$classField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
			
			// Get Student Info
			$ay_id = Get_Current_Academic_Year_ID();
			$sql = "SELECT 
						iu.UserID,
						iu.ChineseName,
						iu.EnglishName,
						CONCAT(".$classField.", '-', ycu.ClassNumber) as ClassNameNum
					FROM
						INTRANET_USER iu
					INNER JOIN
						YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID
					INNER JOIN
						YEAR_CLASS yc ON yc.YearClassID = ycu.YearClassID and yc.AcademicYearID = '".$ay_id."'
					WHERE
						iu.UserID IN ('".implode("', '", (array)$UserIDAry)."')
					ORDER BY	
						yc.ClassTitleEN, ycu.ClassNumber ";
			$UserInfoAry = $this->returnArray($sql);
			
			# Build Selection Options
			$student_option = "";
			foreach((array)$UserInfoAry as $this_user_info) {
				list($this_userid, $this_chi_name, $this_eng_name, $this_classname) = $this_user_info;
				$this_username = Get_Lang_Selection($this_chi_name, $this_eng_name);
				$student_option .= " <option class=\"".$this_userid."\" value=\"".$this_userid."\" selected>".$this_username."(".$this_classname.")</option> \n\r ";
			}
		}
		else
		{
			// Get Student Info
			$sql = "SELECT 
						UserID,
						ChineseName,
						EnglishName,
						CONCAT(ClassName, '-', ClassNumber) as ClassNameNum
					FROM
						INTRANET_USER
					WHERE
						UserID IN ('".implode("', '", (array)$UserIDAry)."')
					ORDER BY	
						ClassName, ClassNumber ";
			$UserInfoAry = $this->returnArray($sql);
			
			# Build Selection Options
			$student_option = "";
			foreach((array)$UserInfoAry as $this_user_info) {
				list($this_userid, $this_chi_name, $this_eng_name, $this_classname) = $this_user_info;
				$this_username = Get_Lang_Selection($this_chi_name, $this_eng_name);
				$student_option .= " <option class=\"".$this_userid."\" value=\"".$this_userid."\" selected>".$this_username."(".$this_classname.")</option> \n\r ";
			}
		}
		
		return $student_option;
	}
	
	public function getSelectNoticeDropDown($dataAry, $isAPRecord=false)
	{
		global $intranet_root, $sys_custom;
		
		include_once $intranet_root."/includes/libnotice.php";
		$lnotice = new libnotice();
		
		// Get Record Template Settings
		if($isAPRecord) {
			$RecordCategory = $dataAry["ap_category_select"];
			$RecordCategoryInfo = $this->getAPCategoryInfoBYCatID($RecordCategory);
			$RecordCategoryType = $RecordCategoryInfo["RecordType"];
		}
		else {
			$RecordCategory = $dataAry["gm_category_select"];
			$RecordCategoryInfo = $this->getGMCategoryInfoByCategoryID($RecordCategory);
			$RecordCategoryType = $RecordCategoryInfo["MeritType"];
		}
		$DefaultTemplateID = $RecordCategoryInfo["TemplateID"];
		$IsAutoSelectNotice = $RecordCategoryInfo["AutoSelecteNotice"] || ($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked'] && $isAPRecord);
		
		$CategoryOptions = "";
		$TemplateOptions = "";
		if(!$lnotice->disabled)
		{
			// Get Default Template Category
			if($DefaultTemplateID) {
				$DefaultTemplateInfo = $this->retrieveTemplateDetails($DefaultTemplateID);
				$DefaultTemplateCategory = $DefaultTemplateInfo[0]["CategoryID"];
			}
			
			$AllNoticeTemplates = $this->RETRIEVE_NOTICE_TEMPLATE_INFO("", 1);
			if (!empty($AllNoticeTemplates))
			{
				# Build Template Category Option
				$TemplateCategoryList = $this->TemplateCategory();
				foreach((array)$TemplateCategoryList as $CategoryID => $CategoryName) {
					$thisCategoryInfo = $this->RETRIEVE_NOTICE_TEMPLATE_INFO("", 1, $CategoryID);
					if(!empty($thisCategoryInfo)) {
						$isCategorySelected = (!empty($DefaultTemplateCategory) && strtolower($DefaultTemplateCategory)==strtolower($CategoryID))? " selected" : "";
						$CategoryOptions .= " <option value=\"".$CategoryID."\" ".$isCategorySelected.">".$CategoryName."</option> \n\r ";
					}
				}
				
				# Build Template Option
				if($DefaultTemplateCategory && $DefaultTemplateID) {
					$DefaultTemplateList = $this->getDetentionENoticeByCategoryID("DISCIPLINE", $DefaultTemplateCategory);
					foreach((array)$DefaultTemplateList as $thisTemplate) {
						list($thisTemplateID, $thisTemplateName) = $thisTemplate;
						$isTemplateSelected = (!empty($DefaultTemplateID) && $DefaultTemplateID==$thisTemplateID)? " selected" : "";
						$TemplateOptions .= " <option value=\"".$thisTemplateID."\" ".$isTemplateSelected.">".$thisTemplateName."</option> \n\r ";
					}
				}
			}
		}
		
		$NoticeData = array();
		$NoticeData["DefaultSelectNotice"] = $IsAutoSelectNotice? " checked" : "";
		$NoticeData["DefaultShowNoticeDiv"] = $IsAutoSelectNotice? "" : " style=\"display: none\" ";
		$NoticeData["CategorySelectOptions"] = $CategoryOptions;
		$NoticeData["NoticeSelectOptions"] = $TemplateOptions;
		$NoticeData["DefaultCheckSendEmail"] = ($sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox'] || ($sys_custom['eDiscipline']['SendDetentionPushMessage'] && $RecordCategoryType==-1))? "" : " checked ";
		$NoticeData["DisableCheckSemdEmail"] = $this->isEJ();
		return $NoticeData;
	}
	
	function getRecordStatusDropDown($RecordStatus)
	{
		global $i_Discipline_System_Award_Punishment_Pending, $i_Discipline_System_Award_Punishment_Approved, $i_Discipline_System_Award_Punishment_Rejected, $i_Discipline_System_Award_Punishment_Waived;
		
		$StatusOptions = "";
		$HasApprovalRight = $this->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Approval");
		
		# Record Status: APPROVED
		if($RecordStatus == DISCIPLINE_STATUS_APPROVED) {
			$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_APPROVED."\" data-icon=\"".$this->ImagePath."/icon_approve.png\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_Approved."</option> \n\r ";
			if ($HasApprovalRight) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_REJECTED."\" data-icon=\"".$this->ImagePath."/icon_reject.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Rejected."</option> \n\r ";
			}
		}
		else if ($RecordStatus == DISCIPLINE_STATUS_PENDING) {
			$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_PENDING."\" data-icon=\"".$this->ImagePath."/icon_waiting.png\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_Pending."</option> \n\r ";
			if($HasApprovalRight) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_APPROVED."\" data-icon=\"".$this->ImagePath."/icon_approve.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Approved."</option> \n\r ";
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_REJECTED."\" data-icon=\"".$this->ImagePath."/icon_reject.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Rejected."</option> \n\r ";
			}
		}
		else if ($RecordStatus == DISCIPLINE_STATUS_REJECTED) {
			$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_REJECTED."\" data-icon=\"".$this->ImagePath."/icon_reject.png\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_Rejected."</option> \n\r ";
			if($HasApprovalRight) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_APPROVED."\" data-icon=\"".$this->ImagePath."/icon_approve.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Approved."</option> \n\r ";
			}
		}
		else if($RecordStatus == DISCIPLINE_STATUS_WAIVED) {
			$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_WAIVED."\" data-icon=\"".$this->ImagePath."/icon_waived.png\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_Waived."</option> \n\r ";
		}
		return $StatusOptions;
	}
	
	function getMeritRelatedConductTable($merit_id)
	{
		$table = "";
		
		// Get Merit related Conducts
		$related_conduct = $this->RETRIEVE_GROUPED_CONDUCT_RECORD($merit_id, $orderByTime=true);
		for($i=0; $i<sizeof($related_conduct); $i++)
		{
			// Get Conduct Info
			$this_conduct = $related_conduct[$i];
			$thisRecordTypeImg = ($this_conduct["RecordType"]==GOOD_CONDUCT)? "icon_gd_conduct.gif":"icon_misconduct.gif";
			$thisCatID = $this_conduct["CategoryID"];
			$thisItemID = $this_conduct["ItemID"];
			$thisReason = $this->RETURN_CONDUCT_REASON($thisItemID, $thisCatID);
			$thisRecordDate = substr($this_conduct["RecordDate"], 0, 10);
			$thisPIC = $this->RETRIEVE_CONDUCT_PIC($this_conduct["RecordID"]);
			$thisPIC = $thisPIC ? $thisPIC : "---";
			
			$table .= "<tr class='row_approved'>
				                <th><img src=\"".$this->ImagePath."/".$thisRecordTypeImg."\" width='20' height='20'>".$thisReason."</th>
								<th>".$thisRecordDate."</th>
								<th>".$thisPIC."</th>
							</tr>";
		}
		return $table;
	}
	
	function getMeritStatusDropDown($RecordInfo)
	{
		global $Lang, $eDiscipline, $i_Discipline_System_Award_Punishment_Pending, $i_Discipline_System_Award_Punishment_Approved, $i_Discipline_System_Award_Punishment_Rejected, $i_Discipline_System_Award_Punishment_Waived;
		global $i_Discipline_System_Award_Punishment_Released, $i_Discipline_System_Award_Punishment_UnReleased;
		
		// Get Merit Info
		$RecordID = $RecordInfo["RecordID"];
		$RecordStatus = $RecordInfo["RecordStatus"];
		$ReleaseStatus = $RecordInfo["ReleaseStatus"];
		$RedeemRecord = $RecordInfo["RedeemID"];
		$RecordCaseRecord = $RecordInfo["CaseID"];
		
		$StatusOptions = "";
		$releaseStatusOptions = "";
		
		# Check Case Record FINISHED or not
		$isFinishedCase = $this->CASE_IS_FINISHED($RecordCaseRecord);
		
		# Check User Access Right
		$HasApprovalRight = $this->isEJ()? $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval") : $this->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($RecordID);
		$HasReleaseRight = $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release");
		$HasWaiveRight = $this->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive");
		
		// Record Status: APPROVED
		if($RecordStatus == DISCIPLINE_STATUS_APPROVED) {
			$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_APPROVED."\" data-icon=\"".$this->ImagePath."/icon_approve.png\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_Approved."</option> \n\r ";
			if ($HasApprovalRight) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_REJECTED."\" data-icon=\"".$this->ImagePath."/icon_reject.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Rejected."</option> \n\r ";
			}
			if ($HasWaiveRight && !$isFinishedCase) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_WAIVED."\" data-icon=\"".$this->ImagePath."/icon_waived.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Waived."</option> \n\r ";
			}
		}
		// Record Status: PEDNING
		else if($RecordStatus == DISCIPLINE_STATUS_PENDING) {
			$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_PENDING."\" data-icon=\"".$this->ImagePath."/icon_waiting.png\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_Pending."</option> \n\r ";
			if ($HasApprovalRight && !$isFinishedCase) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_APPROVED."\" data-icon=\"".$this->ImagePath."/icon_approve.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Approved."</option> \n\r ";
			}
			if ($HasApprovalRight) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_REJECTED."\" data-icon=\"".$this->ImagePath."/icon_reject.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Rejected."</option> \n\r ";
			}
		}
		// Record Status: REJECTED
		else if($RecordStatus == DISCIPLINE_STATUS_REJECTED) {
			$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_REJECTED."\" data-icon=\"".$this->ImagePath."/icon_reject.png\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_Rejected."</option> \n\r ";
			if ($HasApprovalRight && !$isFinishedCase) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_APPROVED."\" data-icon=\"".$this->ImagePath."/icon_approve.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Approved."</option> \n\r ";
			}
			if ($HasWaiveRight && !$isFinishedCase && !$this->isEJ()) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_WAIVED."\" data-icon=\"".$this->ImagePath."/icon_waived.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Waived."</option> \n\r ";
			}
		}
		// Record Status: REDEEMED
		else if($RecordStatus == DISCIPLINE_STATUS_WAIVED && $RedeemRecord > 0) {
			$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_WAIVED."\" data-icon=\"".$this->ImagePath."/icon_waived.png\" class=\"left\" selected>".$eDiscipline['Redemption']."</option> \n\r ";
			if ($HasWaiveRight) {
				$StatusOptions .= " <option value=\"99\" class=\"left\">".$eDiscipline['CancelRedemption']."</option> \n\r ";
			}
		}
		// Record Status: WAIVED
		else if($RecordStatus == DISCIPLINE_STATUS_WAIVED) {
			$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_WAIVED."\" data-icon=\"".$this->ImagePath."/icon_waived.png\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_Waived."</option> \n\r ";
			if ($HasWaiveRight) {
				$StatusOptions .= " <option value=\"9\" class=\"left\">".$Lang['eDiscipline']['UnwaiveRecord']."</option> \n\r ";
			}
			if ($HasApprovalRight) {
				$StatusOptions .= " <option value=\"".DISCIPLINE_STATUS_REJECTED."\" data-icon=\"".$this->ImagePath."/icon_reject.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Rejected."</option> \n\r ";
			}
		}
		
		// Release Status: RELEASED
		if($ReleaseStatus == DISCIPLINE_STATUS_RELEASED) {
			$releaseStatusOptions .= " <option value=\"".DISCIPLINE_STATUS_RELEASED."\" data-icon=\"".$this->ImagePath."/icon_to_student.png\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_Released."</option> \n\r ";
			if($HasReleaseRight) {
				$releaseStatusOptions .= " <option value=\"".DISCIPLINE_STATUS_UNRELEASED."\" class=\"left\">".$i_Discipline_System_Award_Punishment_UnReleased."</option> \n\r ";
			}
		}
		// Release Status: UNRELEASED
		else {
			$releaseStatusOptions .= " <option value=\"".DISCIPLINE_STATUS_UNRELEASED."\" class=\"left\" selected>".$i_Discipline_System_Award_Punishment_UnReleased."</option> \n\r ";
			if(!$this->isEJ())
			{
				if(($RecordStatus == DISCIPLINE_STATUS_APPROVED || $RecordStatus == DISCIPLINE_STATUS_WAIVED) && $HasReleaseRight) {
					$releaseStatusOptions .= " <option value=\"".DISCIPLINE_STATUS_RELEASED."\" data-icon=\"".$this->ImagePath."/icon_to_student.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Released."</option> \n\r ";
				}
			}
			else
			{
				if(($RecordStatus == DISCIPLINE_STATUS_APPROVED || $RecordStatus == DISCIPLINE_STATUS_PENDING || $RecordStatus == DISCIPLINE_STATUS_WAIVED) && $HasReleaseRight) {
					$releaseStatusOptions .= " <option value=\"".DISCIPLINE_STATUS_RELEASED."\" data-icon=\"".$this->ImagePath."/icon_to_student.png\" class=\"left\">".$i_Discipline_System_Award_Punishment_Released."</option> \n\r ";
				}
			}
		}
		
		return array($StatusOptions, $releaseStatusOptions);
	}
}
?>