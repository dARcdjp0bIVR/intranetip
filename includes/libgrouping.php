<?php
// Modifing by:

############# Change Log [Start] ################
#
# Date: 2020-09-28 [Tommy]
#       modified returnUserIDByGroup(), added set $id_groups is array for implode()
#
# Date: 2019-05-13 [Anna]
#       Added '' to avoid sql injection
#
# Date: 2019-01-22 Cameron
#       cast array for implode argument in returnGroupUsersInIdentity()
#
# Date: 2019-01-18 Cameron
#       modified returnCategoryGroups(), add parameter $returnAssoc
#
# Date:	2018-11-19	Paul
#       modified getGroupTitleByLang(), show alternative lang if the current session language has no input of the group title
#
# Date:	2018-10-29	Rox
#       Add returnGroupUsersExcludeCourseByType() to add group of users by their type.
#
# Date:	2017-04-03	Frankie
#		Add displayPollingGroupsFrontend for Polling
#
# Date:	2016-11-14	Villa
#		modified displayGroupSelection2(), displayAnnouncementGroups() - select self group by default (eCommunity-new.php)
#
# Date:	2015-11-24	Kenneth
#		modified returnGroupUsersInIdentity(), added system flag control($sys_custom['hideTeacherTitle']) to hide teacher title
#		
# Date:	2015-08-27 (Pun) [ip.2.5.6.10.1.0]
#		modified returnSubjectGroupUsersExcludeCourse(), returnGroupUsersExcludeCourse() to add param $cond
#
# Date:	2015-06-03 (Ivan) [V79280] [ip.2.5.6.8.1.0]
#		modified returnSelectedEventGroups() to add param $parAcademicYearId
#
# Date:	2015-05-04 (Carlos)
#		modified returnGroupUsersInIdentity(), added optional parameter $includeGroupIdAry to allow choose users in some special groups. 
#
# Date: 2014-09-15 (Charles Ma) - 20140915-P67672 
#		add returnSubjectGroupUsersExcludeCourse 
#
# Date: 2013-12-18 (Ivan)
#		updated returnGroupUsersInIdentity added param $includeUserIdAry
#
# Date: 2013-08-06 (YatWoon)
#		fixed: returnUserIDForIdentityGroups(), add empty array checking
#
# Date: 2013-06-17 (Siuwan)
#		modified returnUserForTypeExcludeGroup(),returnGroupUsersExcludeGroup(),returnUserForTypeExcludeCourse()
#		add sql order handling
#
# Date: 2013-05-14 (Rita)
#		modified getGroupInfoByUserID() add GROUP BY g.GroupID
#
# Date: 2013-04-19 (Yuen)
#		update returnSelectedCategoryGroupsFromType() and some similiar functions to support showing alumni groups (which are created by system when adding alumni)
#
# Date: 2012-07-03 (Ivan) [2012-0627-1344-10073]
#		update returnSelectedCategoryGroupsFromType(), to fix the broken SQL problem if $ID is empty
#
# Date: 2011-12-14 (YatWoon)
#		update returnGroupUsersInIdentity(), add new parameter $UserRecordStatus
#
# Date: 2011-08-24 (Carlos)
#		added getGroupTitleByLang(), update all Title field with this method
#
# Date: 2011-02-25	YatWoon
#		update returnUserForTypeWithMobile(), update order by statement
#
# Date:	2010-12-13 YatWoon
#		update returnSelectedCategoryGroupsFromType(), add "TitleChinese" 
#	
# Date:	2010-12-13 YatWoon
#		update returnCategoryGroups(), add "TitleChinese" and update order by
#
# Date: 2010-11-19 (Henry Chow)
#		add getGroupInfoByUserID(), retrieve group info by UserID (for "Student Mgmt")
#
# Date: 2010-10-11 (Ivan)
#		modified returnUserForTypeWithMobile() improved the parent name display format
#
# Date: 2010-09-09 (Kelvin)
#		modified returnUserForTypeExcludeCourse, Add Param $ParJoin,$ParUsernameField,$ParDistinct,$ParOrder
#
# Date: 2010-09-08 (Marcus)
#		modified displayGroupSelection2, displayAnnouncementGroups, Add Param $showECA
#
# Date: 2010-08-11 (Marcus)
#		modified returnGroupUsersInIdentity, check student existence by checking YEAR_CLASS_USER.ClassNumber , YEAR_CLASS.YearClassID instead of checking INTRANET_USER
#
# Date: 2010-05-19 (Henry Chow)
#		add: displayUserEmailOptionByConds(), display "Email" by $conds
#
# Date: 2010-04-29 (Henry)
#		add: displayUserEmailOptionByUserID(), display "Email" by UserID
#
# Date: 2010-04-29 (Henry)
#		update: displayUsersEmailOption(), returnUsersEmailOption(), change "$filter" as optional
#
# Date: 2010-03-24 YatWoon
#		update: displayGroupSelection2()
#		fixed: no need include the file with path
#
# Date: 2010-03-23 Marcus
#		update: returnGroupUsersInIdentity
#		fixed: cannot display teaching/non teaching staff. (has not join YEAR_CLASS_TEACHER for teachers)
#
# Date:	2010-02-27 Ivan
#		update: displayGroupSelection2(), returnSelectedCategoryGroupsFromType()
#		Improved: If the system is using eEnrol and have the term-based club enhancement, do not show the ECA Groups in the selection box
# Date:	2010-01-13 [YatWoon]
#		update displayGroupSelection2()
#		Fixed: cannot display Identity group in group selection.  (due to academic year id is null)
#
############# Change Log [End] ################


class libgrouping extends libdb {

        var $adminGroups;
        var $adminRetrieved;
        var $CurrentAcademicYearID;

        # ------------------------------------------------------------------------------------
        # /admin/resource/ - new.php, edit.php
        # /admin/polling/ - new.php, edit.php
        # /admin/timetable/ - new.php, edit.php
        # /admin/event/ - new.php, edit.php
        # /admin/announcement/ - new.php, new_update.php, edit.php, edit_update.php
        # /admin/user/ - new.php, edit.php, email.php, import.php, export_update.php
        # /admin/eclass/user/ - import.php
        # /admin/group/info/ - add.php
        # /home/campusmail/choose/ - index.php
        # /home/school/ - list.php, index.php
        # ------------------------------------------------------------------------------------
        function libgrouping(){
                $this->libdb();
                $this->adminRetrieved = false;
                $this->CurrentAcademicYearID = Get_Current_Academic_Year_ID();
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/announcement/ - new_update.php, edit_update.php
        # ------------------------------------------------------------------------------------
        function returnUsersEmailGroup($GroupID){
                global $webmaster;
                if(sizeof($GroupID)==0){
                        $sql = "SELECT UserEmail FROM INTRANET_USER WHERE RecordStatus = '1'";
                }else{
                    $sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID IN ('".implode("','",$this->Get_Safe_Sql_Query($GroupID))."')";
                        $sql = "SELECT UserEmail FROM INTRANET_USER WHERE RecordStatus = '1' AND UserID IN (".$this->db_sub_select($sql).")";
                }
                $row = $this->returnArray($sql,1);
                for($i=0; $i<sizeof($row); $i++){
                        $x .= ($i==0) ? "" : ",";
                        $x .= $row[$i][0];
                }
                return $x;
        }

        function returnUsersMobileGroup($GroupID){
                global $webmaster;
                if(sizeof($GroupID)==0){
                        $sql = "SELECT UserLogin, MobileTelNo FROM INTRANET_USER WHERE RecordStatus = '1' AND MobileTelNo IS NOT NULL";
                }else{
                    $sql = "SELECT DISTINCT UserID FROM INTRANET_USERGROUP WHERE GroupID IN ('".implode("','",$this->Get_Safe_Sql_Query($GroupID))."')";
                        $sql = "SELECT UserLogin, MobileTelNo FROM INTRANET_USER WHERE RecordStatus = '1' AND UserID IN (".$this->db_sub_select($sql).") AND MobileTelNo IS NOT NULL";
                }
                return $this->returnArray($sql,2);
        }
        # ------------------------------------------------------------------------------------


        # ------------------------------------------------------------------------------------
        # /admin/user/ - email.php
        # ------------------------------------------------------------------------------------
        function returnUsersEmailOption($filter="",$keyword="",$GroupID){
                 if ($GroupID == "") {
                     $conds = "";
                 } else {
                     /*
                     $sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '$GroupID'";
                     $conds = "UserID IN (".$this->db_sub_select($sql).") AND ";
                     */
                     $conds = " AND RecordType = $GroupID ";
                 }
                 if($filter != "")
                 	$conds .= " AND RecordStatus = '$filter'";
                 	
                $sql = "SELECT UserEmail FROM INTRANET_USER WHERE (UserLogin like '%$keyword%' OR UserEmail like '%$keyword%' OR FirstName like '%$keyword%' OR LastName like '%$keyword%') $conds ORDER BY UserEmail ASC";
                return $this->returnArray($sql,1);
        }
        function displayUsersEmailOption($filter="",$keyword="",$GroupID){
                $row = $this->returnUsersEmailOption($filter,$keyword,$GroupID);
                for($i=0; $i<sizeof($row); $i++)
                $x .= "<option value=".$row[$i][0]." SELECTED>".$row[$i][0]."</option>\n";
                return $x;
        }
        
        function displayUserEmailOptionByUserID($useridAry=array())
        {
	        if(sizeof($useridAry)>0) {
		     	$sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserID IN (".implode(',',$useridAry).")";
		     	$row = $this->returnVector($sql);
		     	
                for($i=0; $i<sizeof($row); $i++)
                	$x .= "<option value=".$row[$i]." SELECTED>".$row[$i]."</option>\n";
                return $x;
	     	}
        }
        
        function displayUserEmailOptionByConds($recordType, $conds="")
        {
	        
	        $sql = "SELECT UserEmail FROM INTRANET_USER WHERE RecordType=$recordType $conds";
	     	$row = $this->returnVector($sql);
	     	
			for($i=0; $i<sizeof($row); $i++)
				$x .= "<option value=".$row[$i]." SELECTED>".$row[$i]."</option>\n";
				
			return $x;
	        
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/user/ - export_update.php
        # ------------------------------------------------------------------------------------
        function returnUsersExport($Fields, $filter, $keyword, $GroupID){
                $sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '".IntegerSafe($GroupID)."'";
                $sql = "SELECT ".implode(",", $Fields)." FROM INTRANET_USER WHERE UserID IN (".$this->db_sub_select($sql).") AND RecordStatus = '$filter' AND (UserLogin like '%$keyword%' OR UserEmail like '%$keyword%' OR FirstName like '%$keyword%' OR LastName like '%$keyword%')";
                return $this->returnArray($sql,sizeof($Fields));
        }
        function displayUsersExport($FieldsTerminator, $Fields, $filter, $keyword, $GroupID){
                $row = $this->returnUsersExport($Fields, $filter, $keyword, $GroupID);
                for($i=0; $i<sizeof($row); $i++){
                        $x .= implode($FieldsTerminator, $row[$i])."\n";
                }
                return $x;
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/resource/ - new.php, edit.php
        # /admin/polling/ - new.php, edit.php
        # /admin/timetable/ - new.php, edit.php
        # /admin/event/ - new.php, edit.php
        # /admin/announcement/ - new.php, edit.php
        # /admin/user/ - new.php, edit.php
        # ------------------------------------------------------------------------------------
        function returnGroups(){
                global $i_GroupRole;
                $fieldname  = "CASE RecordType ";
                for($i=0; $i<sizeof($i_GroupRole); $i++)
                $fieldname .= "WHEN $i THEN '".$i_GroupRole[$i]."' ";
                $fieldname .= "END ";
                $title_field = $this->getGroupTitleByLang();
                $sql  = "SELECT GroupID, $title_field as Title, RecordType, $fieldname FROM INTRANET_GROUP WHERE AcademicYearID = '".IntegerSafe($this->CurrentAcademicYearID)."' ORDER BY RecordType, $title_field";
                return $this->returnArray($sql,4);
        }

        function returnAdminGroupsDetails()
        {
                global $i_GroupRole, $UserID;
                $fieldname  = "CASE a.RecordType ";
                for($i=0; $i<sizeof($i_GroupRole); $i++)
                $fieldname .= "WHEN $i THEN '".$i_GroupRole[$i]."' ";
                $fieldname .= "END ";
                $title_field = $this->getGroupTitleByLang("a.");
                $sql  = "SELECT a.GroupID, $title_field as Title, a.RecordType, $fieldname FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b WHERE b.RecordType = 'A' AND b.UserID = $UserID AND a.GroupID = b.GroupID AND a.AcademicYearID = ".$this->CurrentAcademicYearID." ORDER BY a.RecordType, $title_field";
                return $this->returnArray($sql,4);
        }

        function returnFirstGroup(){
                $row = $this->returnGroups();
                return $row[0][0];
        }

        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/eclass/user/ - import.php
        # ------------------------------------------------------------------------------------
        function returnGroupUsers($GroupID)
        {
	        	if(is_array($GroupID))
					$GroupID_str = implode(",", $GroupID);
				else
					$GroupID_str = $GroupID;

                 $username_field = getNameFieldByLang("a.");

                $sql  = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) ) FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b WHERE a.UserID = b.UserID and b.GroupID IN (". $GroupID_str .") GROUP BY a.UserID ORDER BY a.EnglishName";
                return $this->returnArray($sql,2);
        }

        # ------------------------------------------------------------------------------------
        # /admin/group/info/ - add.php
        # [2008-12-05] Fixed by Key: update the $sub_sql to select correct UserID exist in Course already
        # ------------------------------------------------------------------------------------
        function returnGroupUsersExcludeGroup($GroupID,$exGroupID, $course_id=""){
	        global $eclass_db;

                 $username_field = getNameFieldByLang("a.");

                 if($course_id != "")
                 {
	             	$sub_sql = "SELECT a.UserID FROM INTRANET_USER as a,
	             	{$eclass_db}.course as c, {$eclass_db}.user_course as uc
                    WHERE 1
                    AND c.course_id = uc.course_id
                    AND uc.course_id = '$course_id'
                    AND uc.user_email = a.UserEmail AND (uc.status is NULL OR uc.status NOT IN ('deleted'))
                    ";
                 }
                 else
                 {
                 	$sub_sql = "SELECT a.UserID FROM INTRANET_USER as a, INTRANET_USERGROUP as b
                             WHERE a.UserID = b.UserID AND b.GroupID = '$exGroupID'";
   				 }
                          //$row = $this->returnArray($sub_sql);
                          //debug_r($row);

                $sql  = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) )
                         FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b
                         WHERE a.UserID = b.UserID and a.UserID NOT IN (".$this->db_sub_select($sub_sql).") AND
                         b.GroupID IN (".implode(",", $GroupID).") GROUP BY a.UserID 
						 ORDER BY a.ClassName, ".getClassNumberForOrderBy('a.').", a.EnglishName";

                return $this->returnArray($sql,2);
        }
        function returnUserForTypeExcludeGroup ($type,$exGroupID,$ParConds="")
        {
                 $sub_sql = "SELECT a.UserID FROM INTRANET_USER as a, INTRANET_USERGROUP as b
                             WHERE a.UserID = b.UserID AND b.GroupID = $exGroupID";
                 $username_field = getNameFieldWithClassNumberByLang("");
                 $sql = "SELECT UserID,$username_field FROM INTRANET_USER WHERE RecordType = $type
                 $ParConds
                 AND UserID NOT IN (".$this->db_sub_select($sub_sql).")
                 ORDER BY ClassName, ".getClassNumberForOrderBy().", TRIM(EnglishName)";
                 
                 return $this->returnArray($sql,2);
        }
		
		 function returnSubjectGroupUsersExcludeCourse($GroupID,$exCourseID, $cond = '') // 20140915-P67672
        {
                 $username_field = getNameFieldByLang("a.");
                 global $eclass_db,$eclass_prefix;

                 $sub_sql = "SELECT user_email FROM $eclass_db.user_course WHERE course_id = $exCourseID";
                 $temp = $this->returnVector($sub_sql);

                 if (count($temp) > 0)
                 {

					$EmailCond = "";
					for ($k=0;$k<count($temp);$k++)
					{
						$EmailCond .= " AND a.UserEmail != '".addslashes($temp[$k])."' ";
					}

					$username_field = getNameFieldWithClassNumberByLang("a.");
	                $sql  = "	SELECT
	                				a.UserID,$username_field
	                         	FROM
	                         		INTRANET_USER AS a, SUBJECT_TERM_CLASS_USER AS b
	                         	WHERE
	                         		a.UserID = b.UserID
	                         		AND a.RecordStatus IN (0, 1, 2)
	                         		AND b.SubjectGroupID IN (".implode(",", $GroupID).")
	                         		$EmailCond
	                         		$cond
	                         	GROUP BY
	                         		a.UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
	                         	";
                 }
                 else
                 {
	                 $email_list = -1;

	                $sql  = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) )
	                         FROM INTRANET_USER AS a, SUBJECT_TERM_CLASS_USER AS b
	                         WHERE a.UserID = b.UserID and a.UserEmail NOT IN ($email_list) AND a.RecordStatus IN (0, 1, 2) AND
	                         b.SubjectGroupID IN (".implode(",", $GroupID).") $cond GROUP BY a.UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
				}
                return $this->returnArray($sql,2);
        }
		
        function returnGroupUsersExcludeCourse($GroupID,$exCourseID, $cond='')
        {
                 $username_field = getNameFieldByLang("a.");
                 global $eclass_db,$eclass_prefix;

                 $sub_sql = "SELECT user_email FROM $eclass_db.user_course WHERE course_id = $exCourseID";
                 $temp = $this->returnVector($sub_sql);

                 if (count($temp) > 0)
                 {

					$EmailCond = "";
					for ($k=0;$k<count($temp);$k++)
					{
						$EmailCond .= " AND a.UserEmail != '".addslashes($temp[$k])."' ";
					}

					$username_field = getNameFieldWithClassNumberByLang("a.");
	                $sql  = "	SELECT
	                				a.UserID,$username_field
	                         	FROM
	                         		INTRANET_USER AS a, INTRANET_USERGROUP AS b
	                         	WHERE
	                         		a.UserID = b.UserID
	                         		AND a.RecordStatus IN (0, 1, 2)
	                         		AND b.GroupID IN (".implode(",", $GroupID).")
	                         		$EmailCond
	                         		$cond
	                         	GROUP BY
	                         		a.UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
	                         	";
                 }
                 else
                 {
	                 $email_list = -1;

	                $sql  = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) )
	                         FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b
	                         WHERE a.UserID = b.UserID and a.UserEmail NOT IN ($email_list) AND a.RecordStatus IN (0, 1, 2) AND
	                         b.GroupID IN (".implode(",", $GroupID).") $cond GROUP BY a.UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
				}
                return $this->returnArray($sql,2);
        }
        
        function returnGroupUsersExcludeCourseByType($GroupID,$exCourseID, $cond='',$userType)
        {
            if ($userType == "T") {
                $userType = 1;
            }else{
                $userType = 2;
            }
            $username_field = getNameFieldByLang("a.");
            global $eclass_db,$eclass_prefix;
            
            $sub_sql = "SELECT user_email FROM $eclass_db.user_course WHERE course_id = '$exCourseID'";
            $temp = $this->returnVector($sub_sql);
            
            
            if (count($temp) > 0)
            {
                
                $EmailCond = "";
                for ($k=0;$k<count($temp);$k++)
                {
                    $EmailCond .= " AND a.UserEmail != '".addslashes($temp[$k])."' ";
                }
                
                $username_field = getNameFieldWithClassNumberByLang("a.");
                $sql  = "	SELECT
	                				a.UserID,$username_field
	                         	FROM
	                         		INTRANET_USER AS a, INTRANET_USERGROUP AS b
	                         	WHERE
	                         		a.UserID = b.UserID
                                    AND a.RecordType=$userType
	                         		AND a.RecordStatus IN (0, 1, 2)
	                         		AND b.GroupID IN (".implode(",", $GroupID).")
	                         		$EmailCond
	                         		$cond
	                         	GROUP BY
	                         		a.UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
	                         	";
            }
            else
            {
                $email_list = -1;
                
                $sql  = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) )
	                         FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b
	                         WHERE a.UserID = b.UserID and a.UserEmail NOT IN ($email_list) AND a.RecordStatus IN (0, 1, 2) AND a.RecordType = $userType AND
	                         b.GroupID IN (".implode(",", $GroupID).") $cond GROUP BY a.UserID ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
            }
            return $this->returnArray($sql,2);
        }
        
        # 2010-09-09 added $ParJoin,$ParUsernameField,$ParDistinct
        # updated on 18 Sept adding $ParConds to get users from specific class
        function returnUserForTypeExcludeCourse($type,$exCourseID,$ParConds="",$ParJoin="",$ParUsernameField="",$ParDistinct="",$ParOrder="")
        {
        		
                 global $eclass_db,$eclass_prefix;
                 $sub_sql = "
                 					SELECT
                 						user_email
                 					FROM
                 						$eclass_db.user_course
                 					WHERE
                 						course_id = $exCourseID
                 				 ";
                 $temp = $this->returnVector($sub_sql);
				
                 if (count($temp) > 0)
                 { 
					$EmailCond = "";
					for ($k=0;$k<count($temp);$k++)
					{
						$EmailCond .= " AND u.UserEmail != '".addslashes($temp[$k])."' ";
					}

	                 $username_field = ($ParUsernameField=='')?getNameFieldWithClassNumberByLang("u."):$ParUsernameField;
	                 $sql = "
	                 			SELECT
	                 				$ParDistinct u.UserID,$username_field
	                 			FROM
	                 				INTRANET_USER as u 
									$ParJoin
	                 			WHERE
	                 				u.RecordType = $type AND u.RecordStatus IN (0, 1, 2)
	                 				$EmailCond
	                 				$ParConds
	                 			ORDER BY 
	                 				u.ClassName, ".getClassNumberForOrderBy('u.').", TRIM(u.EnglishName)
	                 			";
                 }
                 else
                 {

	                 $username_field = ($ParUsernameField=='')?getNameFieldWithClassNumberByLang("u."):$ParUsernameField;
	                 $sql = "
	                 			SELECT
	                 				$ParDistinct u.UserID,$username_field
	                 			FROM
	                 				INTRANET_USER u
									$ParJoin
	                 			WHERE
	                 				u.RecordType = $type AND u.RecordStatus IN (0, 1, 2)
	                 				$ParConds
	                 			ORDER BY 
	                 				u.ClassName, ".getClassNumberForOrderBy('u.').", TRIM(u.EnglishName)
	                 			";
             	}
				return $this->returnArray($sql,2);
        }

        # ------------------------------------------------------------------------------------
        # /home/campusmail/choose/ - index.php
        # ------------------------------------------------------------------------------------
        function returnGroupUsersInIdentity($GroupID, $id_groups, $YearID='', $OwnClassStudentOnly=0, $UserRecordStatus=1, $includeUserIdAry='', $includeGroupIdAry=array()){
        	
        		global $sys_custom;
				if($sys_custom['hideTeacherTitle']){
					$isTitleDisabled = true;
				}else{
					$isTitleDisabled = false;
				}
                 $username_field = getNameFieldWithClassNumberByLang("a.",$isTitleDisabled);

                 $users_array = $this->returnUserIDForIdentityGroups($id_groups);
                 if (sizeof($users_array) == 0)
                 {
                     return array();
                 }
                 
                 $list = implode(",",$users_array);
				# Henry added "GROUP BY" on 200910223
                //$sql  = "SELECT a.UserID, $username_field FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b WHERE a.UserID = b.UserID and b.GroupID IN (".implode(",", $GroupID).") AND a.RecordType IN (".implode(",",$id_groups).") GROUP BY a.UserID ORDER BY a.ClassName, a.ClassNumber, TRIM(a.EnglishName)";
                
                # 20100531 Ivan: return user from specific form only
                $cond_YearID = '';
                if ($YearID != '')
                	$cond_YearID = " And yc.YearID = '$YearID' ";
                	
                # 20100602 Ivan: return users in the Class Teacher's Classes only
                $cond_ClassTeacher = '';
                if ($OwnClassStudentOnly == 1)
                {
                	$sql = "SELECT
								yc.YearClassID
							FROM
								YEAR_CLASS_TEACHER as yct
								INNER JOIN
								YEAR_CLASS as yc
								ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->CurrentAcademicYearID."' )
							WHERE
								yct.UserID='".$_SESSION['UserID']."'
							";
					$YearClassIDArr = $this->returnVector($sql);
					
					$cond_ClassTeacher = " And yc.YearClassID = '".implode(',', (array)$YearClassIDArr)."' ";
                }
                
                if ($includeUserIdAry != '') {
                	$conds_userId = " AND a.UserID IN ('".implode("','", (array)$includeUserIdAry)."') ";
                }
                	
                # only display the students who already assigned to classes in current year, by Henry on 20091126
                 $sql  = "SELECT 
                			a.UserID, 
                			$username_field 
                		FROM 
                			INTRANET_USER AS a LEFT OUTER JOIN 
                			INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) LEFT OUTER JOIN
							INTRANET_GROUP AS ig ON (ig.GroupID = b.GroupID) LEFT OUTER JOIN
                  			YEAR_CLASS_USER ycu ON (ycu.UserID=a.UserID) LEFT OUTER JOIN
							YEAR_CLASS_TEACHER yct ON (yct.UserID=a.UserID) LEFT OUTER JOIN
                 			YEAR_CLASS yc ON ((yc.YearClassID=ycu.YearClassID) OR (yc.YearClassID=yct.YearClassID)) AND yc.AcademicYearID = ig.AcademicYearID 
               			WHERE 
               				b.GroupID IN (".implode(",", (array)$GroupID).") AND 
                			a.RecordType IN (".implode(",",(array)$id_groups).")
							$cond_YearID
							$cond_ClassTeacher ";
				  if(is_array($includeGroupIdAry) && count($includeGroupIdAry)>0){
				  	$sql .= " AND ( (a.RecordStatus in ($UserRecordStatus) AND (a.RecordType <> 2 OR (ycu.ClassNumber IS NOT NULL AND yc.YearClassID IS NOT NULL AND TRIM(ycu.ClassNumber) <> '' AND TRIM(yc.YearClassID) <> '' ))) 
								OR (ig.GroupID IN (".implode(",",(array)$includeGroupIdAry).")) ) ";
				  }else{
				  	$sql .= " AND a.RecordStatus in ($UserRecordStatus) AND (a.RecordType <> 2 OR (ycu.ClassNumber IS NOT NULL AND yc.YearClassID IS NOT NULL AND TRIM(ycu.ClassNumber) <> '' AND TRIM(yc.YearClassID) <> '' )) ";
				  }
				  $sql .= " $conds_userId 
                		GROUP BY a.UserID 
                		ORDER BY a.ClassName, a.ClassNumber, TRIM(a.EnglishName)
                		";
                		
                		$result = $this->returnArray($sql,2);
                		
                return $result;
                
        }

        function returnUserIDForIdentityGroups ($id_groups)
        {
	        	 if(!empty($id_groups))
                 	$list = implode(",", (array)$id_groups);
                 //$sql = "SELECT DISTINCT a.UserID FROM INTRANET_USER as a, INTRANET_USERGROUP as b WHERE a.UserID = b.UserID AND a.RecordStatus = 1 AND b.GroupID IN ($list)";
                 $sql = "SELECT 
                 			DISTINCT a.UserID 
                 		FROM 
                 			INTRANET_USER as a LEFT OUTER JOIN
                 			INTRANET_USERGROUP as b ON (a.UserID = b.UserID) LEFT OUTER JOIN
                 			YEAR_CLASS_USER ycu ON (ycu.UserID=a.UserID) LEFT OUTER JOIN
                 			YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
                 		WHERE 
                 			(yc.AcademicYearID = '".$this->CurrentAcademicYearID."' Or yc.AcademicYearID is Null) AND
                 			a.RecordStatus = 1 AND 
                 			b.GroupID IN ($list)
                 		";
                 return $this->returnVector($sql);
        }
        
        function returnUserIDWithCondition($cond='', $RecordStatus='1')
		{
			$sql = "select UserID from INTRANET_USER where ";
			$sql .= ($RecordStatus==-1) ? "" : " RecordStatus in (". $RecordStatus .")";
			$sql .= $cond;
			$x = $this->returnVector($sql,1);
			return $x;
		}
		
		function returnUserIDByGroup($id_groups, $RecordStatus='1')
		{
			$list = implode(",",(array)$id_groups);
			$sql = "
					select 
						a.UserID 
					from 
						INTRANET_USERGROUP as a
						inner join INTRANET_USER as b on(b.UserID = a.UserID)
					where 
						a.GroupID IN ($list)
					";
			$sql .= ($RecordStatus==-1) ? "" : " and b.RecordStatus in (". $RecordStatus .")";
			$x = $this->returnVector($sql,1);
			return $x;
		}
		
        function returnUserForType ($type)
        {
                 $username_field = getNameFieldWithClassNumberByLang("");
                 $sql = "SELECT UserID,$username_field FROM INTRANET_USER WHERE RecordType = $type ORDER BY ClassName, ClassNumber, TRIM(EnglishName)";
                 return $this->returnArray($sql,2);
        }
        function returnUserForTypeWithMobile ($type)
        {
        	// not parent
//    		$username_field = getNameFieldWithClassNumberByLang("");
//    		$sql = "SELECT 
//							UserID,
//							$username_field
//					 FROM 
//							INTRANET_USER
//                     WHERE RecordType = $type
//                           AND TRIM(MobileTelNo) <> '' AND MobileTelNo IS NOT NULL
//                     ORDER BY ClassName, ClassNumber, TRIM(EnglishName)";
             
                         
        	if ($type == 3)
        	{
        		$username_field = getParentNameWithStudentInfo('iu_child.', 'iu_target_user.', '', 1);
        		$order = "iu_child.ClassName, iu_child.ClassNumber, TRIM(iu_child.EnglishName)";
    		}
        	else
        	{
        		$username_field = getNameFieldWithClassNumberByLang('iu_target_user.');
        		$order = "iu_target_user.ClassName, iu_target_user.ClassNumber, TRIM(iu_target_user.EnglishName)";
    		}
        	
    		$sql = "Select
							iu_target_user.UserID,
							$username_field as display_name,
							iu_target_user.ClassName,
							iu_target_user.ClassNumber
					From
							INTRANET_USER as iu_target_user
							Left Outer Join
							INTRANET_PARENTRELATION as ipr On (iu_target_user.UserID = ipr.ParentID)
							Left Outer Join
							INTRANET_USER as iu_child On (ipr.StudentID = iu_child.UserID)
					WHERE 
							iu_target_user.RecordType = '".$type."'
                           	AND TRIM(iu_target_user.MobileTelNo) <> '' AND iu_target_user.MobileTelNo IS NOT NULL
							And $username_field Is Not Null
                    ORDER BY 
							$order
					";      
					
					//iu_target_user.ClassName, iu_target_user.ClassNumber, TRIM(iu_target_user.EnglishName)  	
        	return $this->returnArray($sql);
        }
        function returnUserForGroupWithMobile($GroupID)
        {
                 $username_field = getNameFieldWithClassNumberByLang("a.");
                 $sql = "SELECT a.UserID,$username_field FROM INTRANET_USERGROUP as b
                         LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                         WHERE b.GroupID IN (".implode(",",$GroupID).")
                               AND TRIM(a.MobileTelNo) <> '' AND a.MobileTelNo IS NOT NULL
                         ORDER BY a.ClassName, a.ClassNumber, TRIM(a.EnglishName)";
                 return $this->returnArray($sql,2);
        }

        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/eclass/user/ - import.php
        # /admin/group/info/ - add.php
        # /home/campusmail/choose/ - index.php
        # /home/school/ - list.php
        # ------------------------------------------------------------------------------------
        function returnCategoryGroups($RecordType, $returnAssoc=false){
	        global $intranet_session_language;
	        //$order_by = $intranet_session_language=="en" ? "Title":"TitleChinese";
	        $title_field = $this->getGroupTitleByLang();
	        $sql  = "SELECT GroupID, $title_field as Title, RecordType, TitleChinese FROM INTRANET_GROUP WHERE RecordType = '".$this->Get_Safe_Sql_Query($RecordType)."' AND AcademicYearID = '".$this->CurrentAcademicYearID."' ORDER BY ". $title_field;
            if ($returnAssoc) {
                return $this->returnResultSet($sql);
            }
            else {
                return $this->returnArray($sql);
            }
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        function returnCategoryGroupsUserNumber($GroupID){
                $sql  = "SELECT a.UserGroupID FROM INTRANET_USERGROUP AS a, INTRANET_USER AS b WHERE a.UserID = b.UserID AND a.GroupID = '$GroupID'";
                return sizeof($this->returnArray($sql,1));
        }
        function returnSelectedGroups($ID, $table, $tableID){
        		$title_field = $this->getGroupTitleByLang("a.");
                $sql = "SELECT a.GroupID, $title_field as Title, b.GroupID, a.RecordType FROM INTRANET_GROUP AS a LEFT OUTER JOIN $table AS b ON a.GroupID = b.GroupID AND b.$tableID = $ID AND a.AcademicYearID = '".$this->CurrentAcademicYearID."' ORDER BY $title_field";
                return $this->returnArray($sql,4);
        }
        function returnSelectedCategoryGroups($ID, $table, $tableID, $RecordType){
        		$title_field = $this->getGroupTitleByLang("a.");
                $sql = "SELECT a.GroupID, $title_field as Title, b.GroupID, a.RecordType FROM INTRANET_GROUP AS a LEFT OUTER JOIN $table AS b ON a.GroupID = b.GroupID AND b.$tableID = $ID WHERE a.RecordType = '$RecordType' AND a.AcademicYearID = '".$this->CurrentAcademicYearID."' ORDER BY $title_field";
                return $this->returnArray($sql,4);
        }
        function returnSelectedCategoryGroupsFromType($ID, $table, $tableID, $RecordType, $showAlumni=0)
        {
	        global $intranet_session_language;
	        if(empty($ID))
        		$DoNotJoin= " AND 0 ";
        		
        	//$title_field = $intranet_session_language=="en" ? "a.Title" : "a.TitleChinese";
        	$title_field = $this->getGroupTitleByLang("a.");
        	if (!$showAlumni)
        	{
        		$sql_cond_alumni = " AND (a.GroupCode IS NULL OR a.GroupCode NOT LIKE 'ALUMNI_%') ";
        	}
        	
	        if ($tableID == "AlbumID") {
		        $sql = "SELECT GroupAllowedList FROM $table WHERE AlbumID='$ID'";
	        	$row = $this->returnVector($sql);

	        	$sql = "SELECT GroupID, $title_field as Title, GroupID, CategoryName, a.RecordType
	        			FROM INTRANET_GROUP AS a
	        			LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b ON a.RecordType = b.GroupCategoryID
	        			WHERE a.GroupID IN ($row[0]) AND (a.AcademicYearID = ".$this->CurrentAcademicYearID." or a.AcademicYearID is NULL)";
        	}
        	else {
	            $sql = "SELECT a.GroupID, $title_field as Title, b.GroupID, c.CategoryName, a.RecordType
	                    FROM INTRANET_GROUP AS a
	                         LEFT OUTER JOIN $table AS b ON (a.GroupID = b.GroupID AND b.$tableID = '$ID' $DoNotJoin)
	                         LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as c ON a.RecordType = c.GroupCategoryID
	                    WHERE a.RecordType >= $RecordType AND (a.AcademicYearID = ".$this->CurrentAcademicYearID." or a.AcademicYearID is NULL) {$sql_cond_alumni} ORDER BY c.GroupCategoryID, " .$title_field ;
	                    
            }
            return $this->returnArray($sql,4);
        }

        function returnSelectedCategoryGroupsFromType2($ID, $table, $tableID, $RecordType){
	        $sql = "SELECT GroupAllowedList FROM $table WHERE AlbumID=$ID";
	        $row = $this->returnVector($sql);

			$title_field = $this->getGroupTitleByLang("a.");
	        $sql = "SELECT GroupID, $title_field as Title, GroupID, CategoryName, a.RecordType
	        			FROM INTRANET_GROUP AS a
	        			LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b ON a.RecordType = b.GroupCategoryID
	        			WHERE a.GroupID IN ($row[0]) AND a.AcademicYearID = ".$this->CurrentAcademicYearID;


	       /*
                $sql = "SELECT a.GroupID, a.Title, b.GroupID, c.CategoryName
                        FROM INTRANET_GROUP AS a
                             LEFT OUTER JOIN $table AS b ON a.GroupID = b.GroupID AND b.$tableID = $ID
                             LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as c ON a.RecordType = c.GroupCategoryID
                        WHERE a.RecordType >= $RecordType ORDER BY c.GroupCategoryID, a.Title";
                        */

                return $this->returnArray($sql,4);
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/resource/ - new.php
        # /admin/polling/ - new.php
        # /admin/timetable/ - new.php
        # /admin/event/ - new.php
        # /admin/announcement/ - new.php
        # /admin/user/ - new.php
        # ------------------------------------------------------------------------------------
        function displayGroups($id=""){
                global $i_GroupRole;
                for($i=0; $i<sizeof($i_GroupRole); $i++){
                        $groups = $this->returnCategoryGroups($i);
                        if(sizeof($groups)<>0){
                                $x .= "<p>".$i_GroupRole[$i]."<br>\n";
                                $x .= $this->displayCategoryGroups($groups, $id);
                        }
                }
                return $x;
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        function displayGroupsSelection($SelectedGroupID=""){
                $groups = $this->returnGroups();
                for($i=0; $i<sizeof($groups); $i++){
                        $GroupID = $groups[$i][0];
                        $Title = $groups[$i][1];
                        $RecordType = $groups[$i][2];
                        $RecordTypeName = $groups[$i][3];
                        $x .= "<option value=$GroupID ".(($SelectedGroupID == $GroupID) ? "SELECTED" : "").">($RecordTypeName) $Title</option>\n";
                }
                return $x;
        }

        function displayAdminGroupsSelection($SelectedGroupID=""){
                $groups = $this->returnAdminGroupsDetails();
                for($i=0; $i<sizeof($groups); $i++){
                        $GroupID = $groups[$i][0];
                        $Title = $groups[$i][1];
                        $RecordType = $groups[$i][2];
                        $RecordTypeName = $groups[$i][3];
                        $x .= "<option value=$GroupID ".(($SelectedGroupID == $GroupID) ? "SELECTED" : "").">($RecordTypeName) $Title</option>\n";
                }
                return $x;
        }

        function displaySelectedGroups($ID, $table, $tableID){
                global $i_GroupRole;
                for($i=0; $i<sizeof($i_GroupRole); $i++){
                        $groups = $this->returnSelectedCategoryGroups($ID, $table, $tableID, $i);
                        if(sizeof($groups)<>0){
                                $x .= "<p>".$i_GroupRole[$i]."<br>\n";
                                $x .= $this->displaySelectedCategoryGroups($groups);
                        }
                }
                return $x;
        }
        function displayCategoryGroups($groups, $id){
                $x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= "<tr>";
                for($i=0; $i<sizeof($groups); $i++){
                        $GroupID = $groups[$i][0];
                        $Title = $groups[$i][1];
                        $RecordType = $groups[$i][2];
                        $x .= ($i%3==0) ? "</tr><tr>" : "";
                        $x .= "<td width=33%><input type=checkbox name=GroupID[] value=$GroupID ".(($GroupID==$id)?"CHECKED":"")."> $Title</td>\n";
                }
                $x .= "</tr>\n";
                $x .= "</table>\n";
                return $x;
        }
        function displaySelectedCategoryGroups($groups){
                $x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= "<tr>";
                for($i=0; $i<sizeof($groups); $i++){
                        $GroupID = $groups[$i][0];
                        $Title = $groups[$i][1];
                        $GroupIDSelect = $groups[$i][2];
                        $RecordType = $groups[$i][3];
                        $x .= ($i%3==0) ? "</tr><tr>" : "";
                        $x .= "<td width=33%><input type=checkbox name=GroupID[] value=$GroupID";
                        $x .= ($GroupID == $GroupIDSelect) ? " CHECKED" : "";
                        $x .= "> $Title</td>\n";
                }
                $x .= "</tr>\n";
                $x .= "</table>\n";
                return $x;
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/user/ - import.php
        # ------------------------------------------------------------------------------------
        function displayIdentityGroupCheckField($UserID=0){
                global $TabID;
                $row = $this->returnSelectedCategoryGroups($UserID, "INTRANET_USERGROUP", "UserID", 0);
                for($i=0; $i<sizeof($row); $i++){
                        $GroupID = $row[$i][0];
                        $Title = $row[$i][1];
                        $Check = ($TabID == $GroupID || $row[$i][2] > 0) ? "CHECKED" : "";
                        $x .= "<input type=checkbox name=GroupID[] value=$GroupID $Check> $Title <br>\n";
                }
                return $x;
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/user/ - new.php, edit.php
        # ------------------------------------------------------------------------------------
        function displayIdentityGroupHiddenField(){
                $groups = $this->returnCategoryGroups(0);
                for($i=0; $i<sizeof($groups); $i++){
                        $GroupID = $groups[$i][0];
                        $Title = $groups[$i][1];
                        $RecordType = $groups[$i][2];
                        $x .= "<input type=hidden name=IdentityGroupID[] value=$GroupID>\n";
                }
                return $x;
        }
        function displayIdentityCheckField($UserID=0){
                global $TabID;
                $row = $this->returnSelectedCategoryGroups($UserID, "INTRANET_USERGROUP", "UserID", 0);
                for($i=0; $i<sizeof($row); $i++){
                        $GroupID = $row[$i][0];
                        $Title = $row[$i][1];
                        $Check = ($TabID == $GroupID || $row[$i][2] > 0) ? "CHECKED" : "";
                        $x .= "<input type=checkbox name=IdentityGroupID[] value=$GroupID $Check> $Title <br>\n";
                }
                return $x;
        }
        # ------------------------------------------------------------------------------------

 		# ------------------------------------------------------------------------------------
        # /admin/photoalbum/ - edit.php
        # ------------------------------------------------------------------------------------
        function displayPhotoalbumGroups($AlbumID=0){
                # return $this->displaySelectedGroups($AnnouncementID, "INTRANET_GROUPANNOUNCEMENT", "AnnouncementID");
                return $this->displayGroupSelection3($AlbumID, "INTRANET_PHOTO_ALBUM", "AlbumID", 0);
        }

        # ------------------------------------------------------------------------------------
        # /admin/announcement/ - edit.php
        # ------------------------------------------------------------------------------------
        function displayAnnouncementGroups($AnnouncementID=0,$showECA=0, $showAlumni=0, $DisplayGroupID=''){
                # return $this->displaySelectedGroups($AnnouncementID, "INTRANET_GROUPANNOUNCEMENT", "AnnouncementID");
                return $this->displayGroupSelection2($AnnouncementID, "INTRANET_GROUPANNOUNCEMENT", "AnnouncementID", 0,$showECA, $showAlumni, $DisplayGroupID);
        }

        # /home/school/announce/ - new, edit.php
        function displayAnnouncementGroupsFrontend($AnnouncementID=0,$option_name=""){
                # return $this->displaySelectedGroups($AnnouncementID, "INTRANET_GROUPANNOUNCEMENT", "AnnouncementID");
                return $this->displayGroupSelectionFrontend($AnnouncementID, "INTRANET_GROUPANNOUNCEMENT", "AnnouncementID", 0, $option_name);
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/event/ - edit.php
        # ------------------------------------------------------------------------------------
        function displayEventGroups($EventID=0){
                # return $this->displaySelectedGroups($EventID, "INTRANET_GROUPEVENT", "EventID");
                return $this->displayGroupSelection2($EventID, "INTRANET_GROUPEVENT", "EventID", 0);
        }
        # /home/school/event/ - new, edit.php
        function displayEventGroupsFrontend($EventID=0,$option_name=""){
                return $this->displayGroupSelectionFrontend($EventID, "INTRANET_GROUPEVENT", "EventID", 0, $option_name);
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/polling/ - edit.php
        # ------------------------------------------------------------------------------------
        function displayPollingGroups($PollingID=0){
                # return $this->displaySelectedGroups($PollingID, "INTRANET_GROUPPOLLING", "PollingID");
                return $this->displayGroupSelection($PollingID, "INTRANET_GROUPPOLLING", "PollingID", 0);
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/timetable/ - edit.php
        # ------------------------------------------------------------------------------------
        function displayTimetableGroups($TimetableID=0){
                # return $this->displaySelectedGroups($TimetableID, "INTRANET_GROUPTIMETABLE", "TimetableID");
                return $this->displayGroupSelection($TimetableID, "INTRANET_GROUPTIMETABLE", "TimetableID", 0);
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/resource/ - edit.php
        # ------------------------------------------------------------------------------------
        function displayResourceGroups($ResourceID=0){
                # return $this->displaySelectedGroups($ResourceID, "INTRANET_GROUPRESOURCE", "ResourceID");
                return $this->displayGroupSelection2($ResourceID, "INTRANET_GROUPRESOURCE", "ResourceID", 0);
        }
        # ------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------
        # /admin/user/ - new.php, edit.php
        # ------------------------------------------------------------------------------------
        function displayUserGroups($UserID=0){
                # return $this->displaySelectedGroups($UserID, "INTRANET_USERGROUP", "UserID");
                return $this->displayGroupSelection2($UserID, "INTRANET_USERGROUP", "UserID", 1);
        }
        # ------------------------------------------------------------------------------------
        # /home/school/survey/ - new.php, edit.php
        # ------------------------------------------------------------------------------------
        function displaySurveyGroupsFrontend($SurveyID=0,$option_name="", $displayGroupTitle=1, $showAlumni=0){
                # return $this->displaySelectedGroups($AnnouncementID, "INTRANET_GROUPANNOUNCEMENT", "AnnouncementID");
                return $this->displayGroupSelectionFrontend($SurveyID, "INTRANET_GROUPSURVEY", "SurveyID", 0, $option_name, $displayGroupTitle, $showAlumni);
        }
        
        # ------------------------------------------------------------------------------------
        # /home/eAdmin/GeneralMgmt/polling - new.php, edit.php
        # ------------------------------------------------------------------------------------
        function displayPollingGroupsFrontend($PollingID=0,$option_name="", $displayGroupTitle=1, $showAlumni=0){
        	# return $this->displaySelectedGroups($AnnouncementID, "INTRANET_GROUPANNOUNCEMENT", "AnnouncementID");
        	return $this->displayGroupSelectionFrontend($PollingID, "INTRANET_GROUPPOLLING", "PollingID", 0, $option_name, $displayGroupTitle, $showAlumni);
        }

        # ------------------------------------------------------------------------------------
        # /home/school/ - list.php
        # ------------------------------------------------------------------------------------
        function displayOrganizationGroups($groups, $col){
                $x .= "<table width=90% border=0 cellpadding=2 cellspacing=1>\n";
                if(sizeof($groups)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td>".$i_no_record_exists_msg."</td></tr>\n";
                }else{
                        $x .= "<tr>";
                        for($i=0; $i<sizeof($groups); $i++){
                                $GroupID = $groups[$i][0];
                                $Title = $groups[$i][1];
                                $RecordType = $groups[$i][2];
                                $Num = $this->returnCategoryGroupsUserNumber($GroupID);
                                $x .= ($i%$col==0) ? "</tr><tr>" : "";
                                $x .= "<td width=20%>\n";
                                $x .= ($Num==0) ? "$Title ($Num)\n" : "<a href=javascript:fe_directory($GroupID)>$Title ($Num)</a>\n";
                                $x .= "</td>\n";
                        }
                        $x .= "</tr>\n";
                }
                $x .= "</table>\n";
                return $x;
        }
        # ------------------------------------------------------------------------------------

        function displayGroupSelection($ID, $Table, $TableID, $Index){
                global $i_GroupRole, $i_admintitle_group, $button_add, $button_remove, $intranet_session_language;
                for($i=$Index; $i<sizeof($i_GroupRole); $i++)
                $row = array_merge($row, $this->returnSelectedCategoryGroups($ID, $Table, $TableID, $i));
#                if(sizeof($row) > 0){
                        $x .= "<table width=100% border=0 cellpadding=5 cellspacing=0>\n";
                        $x .= "<tr><td colspan=3>$i_admintitle_group:</td></tr>\n";
                        $x .= "<tr>\n";
                        $x .= "<td class=tableContent width=50%>\n";
                        $x .= "<select name=GroupID[] size=10 multiple>\n";
                        for($i=0; $i<sizeof($row); $i++){
                                $GroupID = $row[$i][0];
                                $Title = $row[$i][1];
                                $IsCheck = ($row[$i][2] > 0) ? 1 : 0;
                                $RecordType = $row[$i][3];
                                $RecordTypeName = $i_GroupRole[$RecordType];
                                $x .= ($IsCheck) ? "<option value=$GroupID>($RecordTypeName) $Title</option>\n" : "";
                        }
                        $x .= "<option>\n";
                        for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
                        $x .= "</option>\n";
                        $x .= "</select>\n";
                        $x .= "</td>\n";
                        $x .= "<td class=tableContent align=center>\n";
                        $x .= "<p><input type='image' src='/images/admin/button/s_btn_addto_$intranet_session_language.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"AvailableGroupID[]\"],this.form.elements[\"GroupID[]\"]);return false;'></p>\n";
                        $x .= "<p><input type='image' src='/images/admin/button/s_btn_deleteto_$intranet_session_language.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"GroupID[]\"],this.form.elements[\"AvailableGroupID[]\"]);return false;'></p>\n";
                        $x .= "</td>\n";
                        $x .= "<td class=tableContent width=50%>\n";
                        $x .= "<select name=AvailableGroupID[] size=10 multiple>\n";
                        for($i=0; $i<sizeof($row); $i++){
                                $GroupID = $row[$i][0];
                                $Title = $row[$i][1];
                                $IsCheck = ($row[$i][2] > 0) ? 1 : 0;
                                $RecordType = $row[$i][3];
                                $RecordTypeName = $i_GroupRole[$RecordType];
                                $x .= ($IsCheck) ? "" : "<option value=$GroupID>($RecordTypeName) $Title</option>\n";
                        }
                        $x .= "<option>\n";
                        for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
                        $x .= "</option>\n";
                        $x .= "</select>\n";
                        $x .= "</td>\n";
                        $x .= "</tr>\n";
                        $x .= "</table>\n";
#                }
                return $x;
        }

                function displayGroupSelection2($ID, $Table, $TableID, $Index, $showECA=0, $showAlumni=0, $DisplayGroupID='')
                {
                global $i_GroupRole, $i_admintitle_group, $button_add, $button_remove, $intranet_session_language, $linterface, $i_eNews_AddTo, $i_eNews_DeleteTo, $PATH_WRT_ROOT, $plugin;
                /*
                for($i=$Index; $i<sizeof($i_GroupRole); $i++)
                $row = array_merge($row, $this->returnSelectedCategoryGroups($ID, $Table, $TableID, $i));
                */
                $row = $this->returnSelectedCategoryGroupsFromType($ID,$Table,$TableID,$Index, $showAlumni);
                # Hide ECA Groups if eEnrolment is updated to use Year-based Term-based Club (20100227 Ivan)
                //include_once($PATH_WRT_ROOT."/includes/libclubsenrol.php");
                include_once("libclubsenrol.php");
                $libenroll = new libclubsenrol();
                
#                if(sizeof($row) > 0){
                        $x .= "<table width=100% border=0 cellpadding=5 cellspacing=0 class=\"inside_form_table\">\n";
                        $x .= "<tr>\n";
                        $x .= "<td class=tableContent width=50%>\n";
                        $x .= "<select name=GroupID[] id=GroupID size=10 multiple>\n";
                        for($i=0; $i<sizeof($row); $i++)
                        {
                        	# do not display ECA Group (20100227 Ivan)
                        	if ($plugin['eEnrollment'] && $showECA!=1 && $libenroll->isUsingYearTermBased && $row[$i]['RecordType'] == 5)
                        		continue;
	                        		
                                $GroupID = $row[$i][0];
                                $Title = $row[$i][1];
                                if($DisplayGroupID==''){
                                	$IsCheck = ($row[$i][2] > 0) ? 1 : 0;
                                }else{
                                	if($DisplayGroupID==$GroupID){
                                		$IsCheck = 1;
                                	}else{
                                		$IsCheck = ($row[$i][2] > 0) ? 1 : 0;
                                	}
                                }
                                $RecordTypeName = $row[$i][3];
                                /*
                                $RecordType = $row[$i][3];
                                $RecordTypeName = $i_GroupRole[$RecordType];
                                */
                                $x .= ($IsCheck) ? "<option value=$GroupID>($RecordTypeName) $Title</option>\n" : "";
                        }
                        $x .= "<option>\n";
                        for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
                        $x .= "</option>\n";
                        $x .= "</select>\n";
                        $x .= "</td>\n";
                        $x .= "<td class=tableContent align=center>\n";

                        //$x .= "<p><input type='image' src='/images/admin/button/s_btn_addto_$intranet_session_language.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"AvailableGroupID[]\"],this.form.elements[\"GroupID[]\"]);return false;'></p>\n";
                        //$x .= "<p><input type='image' src='/images/admin/button/s_btn_deleteto_$intranet_session_language.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"GroupID[]\"],this.form.elements[\"AvailableGroupID[]\"]);return false;'></p>\n";

                        if($PATH_WRT_ROOT=="")	$PATH_WRT_ROOT = "../../";
                        include_once($PATH_WRT_ROOT."includes/libinterface.php");
                        $linterface 	= new interface_html();
                        $x .= $linterface->GET_BTN("<< ".$i_eNews_AddTo, "submit", "checkOptionTransfer(this.form.elements['AvailableGroupID[]'],this.form.elements['GroupID[]']);return false;", "submit2") . "<br /><br />";
                        $x .= $linterface->GET_BTN($i_eNews_DeleteTo . " >>", "submit", "checkOptionTransfer(this.form.elements['GroupID[]'],this.form.elements['AvailableGroupID[]']);return false;", "submit23");

                        $x .= "</td>\n";
                        $x .= "<td class=tableContent width=50%>\n";
                        $x .= "<select name=AvailableGroupID[] id=AvailableGroupID size=10 multiple>\n";
                        for($i=0; $i<sizeof($row); $i++)
                        {
                        		# do not display ECA Group (20090904 Ivan)
	                        	if ($plugin['eEnrollment'] && $showECA!=1 && $libenroll->isUsingYearTermBased && $row[$i]['RecordType'] == 5)
	                        		continue;
	                        		
                                $GroupID = $row[$i][0];
                                $Title = $row[$i][1];
                                $IsCheck = ($row[$i][2] > 0) ? 1 : 0;
                                $RecordTypeName = $row[$i][3];
                                /*
                                $RecordType = $row[$i][3];
                                $RecordTypeName = $i_GroupRole[$RecordType];
                                */
                                $x .= ($IsCheck) ? "" : "<option value=$GroupID>($RecordTypeName) $Title</option>\n";
                        }
                       // $x .= "<option>\n";
                       // for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
                       // $x .= "</option>\n";
                        $x .= "</select>\n";
                        $x .= "</td>\n";
                        $x .= "</tr>\n";
                        $x .= "</table>\n";
#                }
                return $x;
        }

        #--------------------------------------------------------------------------------------------------------
        # admin/photoalbum : edit_album.php
        #--------------------------------------------------------------------------------------------------------
        function displayGroupSelection3($ID, $Table, $TableID, $Index){
	        global $i_GroupRole, $i_admintitle_group, $button_add, $button_remove, $intranet_session_language;

	        $row = $this->returnSelectedCategoryGroupsFromType($ID,$Table,$TableID,$Index);
	        $available_row = $this->returnSelectedCategoryGroupsFromType(0,"INTRANET_USERGROUP","UserID",$Index);
            $x .= "<table width=100% border=0 cellpadding=5 cellspacing=0>\n";
            $x .= "<tr>\n";
            $x .= "<td class=tableContent width=50%>\n";
            $x .= "<select name=GroupID[] size=10 multiple>\n";
            for($i=0; $i<sizeof($row); $i++){
                    $GroupID = $row[$i][0];
                    $Title = $row[$i][1];
                    $IsCheck = ($row[$i][2] > 0) ? 1 : 0;
                    $RecordTypeName = $row[$i][3];
                    $x .= ($IsCheck) ? "<option value=$GroupID>($RecordTypeName) $Title</option>\n" : "";
            }
            $x .= "<option>\n";
            for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
            $x .= "</option>\n";
            $x .= "</select>\n";
            $x .= "</td>\n";
            $x .= "<td class=tableContent align=center>\n";
            $x .= "<p><input type='image' src='/images/admin/button/s_btn_addto_$intranet_session_language.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"AvailableGroupID[]\"],this.form.elements[\"GroupID[]\"]);return false;'></p>\n";
            $x .= "<p><input type='image' src='/images/admin/button/s_btn_deleteto_$intranet_session_language.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"GroupID[]\"],this.form.elements[\"AvailableGroupID[]\"]);return false;'></p>\n";
            $x .= "</td>\n";
            $x .= "<td class=tableContent width=50%>\n";
            $x .= "<select name=AvailableGroupID[] size=10 multiple>\n";
            for($i=0; $i<sizeof($available_row); $i++){
                    $GroupID = $available_row[$i][0];
                    $Title = $available_row[$i][1];
                    $IsCheck = ($available_row[$i][2] > 0) ? 1 : 0;
                    $RecordTypeName = $available_row[$i][3];
                    /*
                    $RecordType = $row[$i][3];
                    $RecordTypeName = $i_GroupRole[$RecordType];
                    */
                    $x .= ($IsCheck) ? "" : "<option value=$GroupID>($RecordTypeName) $Title</option>\n";
            }
            $x .= "<option>\n";
            for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
            $x .= "</option>\n";
            $x .= "</select>\n";
            $x .= "</td>\n";
            $x .= "</tr>\n";
            $x .= "</table>\n";
        	return $x;
        }

        function displayGroupSelectionFrontend($ID, $Table, $TableID, $Index, $option_name="", $displayGroupTitle=1, $showAlumni=0)
        {
                 global $i_GroupRole, $i_admintitle_group, $button_add, $button_remove,$intranet_session_language;
                 global $image_path, $PATH_WRT_ROOT,$i_eNews_AddTo,$i_eNews_DeleteTo;
                 if ($option_name == "")
                 {
                     $option_name = "TargetGroupID[]";
                 }
                 $row = $this->returnSelectedCategoryGroupsFromType($ID,$Table,$TableID,$Index, $showAlumni);
                 $x .= "<table width=50% border=0 cellpadding=10 cellspacing=0 align=center>\n";
                 if($displayGroupTitle)
                 {
                 	$x .= "<tr><td class=tableContent colspan=3>$i_admintitle_group:</td></tr>\n";
             	}
                 $x .= "<tr>\n";
                 $x .= "<td class=tableContent width=50%>\n";
                 $x .= "<select id=". str_replace("[]","",$option_name) ." name=$option_name size=10 multiple>\n";
                 for($i=0; $i<sizeof($row); $i++){
                     $GroupID = $row[$i][0];
                     $Title = $row[$i][1];
                     $IsCheck = ($row[$i][2] != "") ? 1 : 0;
                     $RecordTypeName = $row[$i][3];
                     $x .= ($IsCheck) ? "<option value=$GroupID>($RecordTypeName) $Title</option>\n" : "";
                 }
                 $x .= "<option>\n";
                 for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
                 $x .= "</option>\n";
                 $x .= "</select>\n";
                 $x .= "</td>\n";
                 $x .= "<td class=tableContent align=center>\n";
                // $x .= "<p><input type='image' src='$image_path/btn_add_to_$intranet_session_language.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"AvailableGroupID[]\"],this.form.elements[\"$option_name\"]);return false;'></p>\n";
                // $x .= "<p><input type='image' src='$image_path/btn_remove_to_$intranet_session_language.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"$option_name\"],this.form.elements[\"AvailableGroupID[]\"]);return false;'></p>\n";
								if($PATH_WRT_ROOT=="")	$PATH_WRT_ROOT = "../../";
								include_once($PATH_WRT_ROOT."includes/libinterface.php");
								$linterface 	= new interface_html();
								$x .= $linterface->GET_BTN("<< ".$i_eNews_AddTo, "button", "checkOptionTransfer(this.form.elements['AvailableGroupID[]'],this.form.elements['$option_name'])", "addbtn") . "<br /><br />";
								$x .= $linterface->GET_BTN($i_eNews_DeleteTo . " >>", "button", "checkOptionTransfer(this.form.elements['$option_name'],this.form.elements['AvailableGroupID[]'])", "deletebtn");




                 //$x .= "<p><input class=button type=button value=\"&lt;&lt; $button_add\" onClick=checkOptionTransfer(this.form.elements[\"AvailableGroupID[]\"],this.form.elements[\"$option_name\"])></p>\n";
                 //$x .= "<p><input class=button type=button value=\"$button_remove &gt;&gt;\" onClick=checkOptionTransfer(this.form.elements[\"$option_name\"],this.form.elements[\"AvailableGroupID[]\"])></p>\n";

                 $x .= "</td>\n";
                 $x .= "<td class=tableContent width=50%>\n";
                 $x .= "<select id=AvailableGroupID name=AvailableGroupID[] size=10 multiple>\n";
                 for($i=0; $i<sizeof($row); $i++){
                     $GroupID = $row[$i][0];
                     $Title = $row[$i][1];
                     $IsCheck = ($row[$i][2] > 0) ? 1 : 0;
                     $RecordTypeName = $row[$i][3];
                     $x .= ($IsCheck) ? "" : "<option value=$GroupID>($RecordTypeName) $Title</option>\n";
                 }
                 $x .= "<option>\n";
                 for($i = 0; $i < 20; $i++) $x .= "&nbsp;";
                 $x .= "</option>\n";
                 $x .= "</select>\n";
                 $x .= "</td>\n";
                 $x .= "</tr>\n";
                 $x .= "</table>\n";
                 return $x;
        }
        # ------------------------------------------------------------------------------------
        # /home/school/ - index.php
        # ------------------------------------------------------------------------------------

        function retrieveAdminGroups ($uid)
        {
                 $sql = "SELECT GroupID from INTRANET_USERGROUP WHERE UserID = $uid AND RecordType = 'A'";
                 $this->adminGroups = $this->returnVector($sql);
                 if ($this->adminGroups == "")
                     $this->adminGroups = array();
                 $this->adminRetrieved = true;
        }

        function isGroupAdmin ($uid , $gid)   // UserID and GroupID
        {
                 if (!$this->adminRetrieved)
                 {
                      $this->retrieveAdminGroups($uid);
                 }
                 return (in_array($gid, $this->adminGroups));
        }

        function returnAdminGroups($uid)
        {
                 $conds = "a.GroupID = b.GroupID AND b.UserID = $uid AND b.RecordType = 'A'";
                 $title_field = $this->getGroupTitleByLang("a.");
                 $sql = "SELECT a.GroupID, $title_field as Title FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b WHERE $conds";
                 return $this->returnArray($sql,2);
        }

        # ------------------------------------------------------------------------------------
        # Intranet 1.2 development

        # Replace functions in list.php - returnCategoryGroups, displayOrganizationGroups
        function listAllGroups()
        {
                 global $i_GroupRoleOrganization;
                 global $i_schoolinfo_organization_iconleft_admin,
$i_schoolinfo_organization_iconright_admin,
$i_schoolinfo_organization_iconbottom_admin,
$i_schoolinfo_organization_iconleft_academic,
$i_schoolinfo_organization_iconright_academic,
$i_schoolinfo_organization_iconbottom_academic,
$i_schoolinfo_organization_iconleft_class ,
$i_schoolinfo_organization_iconright_class ,
$i_schoolinfo_organization_iconbottom_class ,
$i_schoolinfo_organization_iconleft_house ,
$i_schoolinfo_organization_iconright_house ,
$i_schoolinfo_organization_iconbottom_house ,
$i_schoolinfo_organization_iconleft_eca ,
$i_schoolinfo_organization_iconright_eca ,
$i_schoolinfo_organization_iconbottom_eca ,
$i_schoolinfo_organization_iconleft_misc ,
$i_schoolinfo_organization_iconright_misc ,
$i_schoolinfo_organization_iconbottom_misc ;
				
				 $title_field = $this->getGroupTitleByLang();
				 $sql = "SELECT GroupID, $title_field as Title, RecordType, URL FROM INTRANET_GROUP WHERE RecordType <> 0 AND AcademicYearID = '".IntegerSafe($this->CurrentAcademicYearID)."' ORDER BY RecordType ASC, $title_field ASC";
                 $groups = $this->returnArrays($sql,4);
                 $sql = "SELECT GroupID, COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE GroupID NOT IN (1,2,3,4) GROUP BY GroupID ORDER BY GroupID";
                 $counts = $this->returnArray($sql,2);
                 $groups2 = array();
                 for ($i=0; $i<sizeof($counts); $i++)
                 {
                      $groups2[$counts[$i][0]]=$counts[$i][1];
                 }
                 $prevType = -1;
                 $row_count = 0;
                 for ($i=0;$i<sizeof($groups); $i++)
                 {
                      list($gid, $title, $type,$url) = $groups[$i];

                      # Bound length of title
                      $s_title = intranet_wordwrap($title,20,"\n",1);
                      //$s_title = $title;
                      /*
                      if (strlen($title)>40)
                      {
                          $s_title = substr($title,0,30)."...".substr($title,-7);
                      }
                      else
                      {
                          $s_title = $title;
                      }*/

                      $num = ($groups2[$gid]==""? 0:$groups2[$gid]);
                      if ($prevType !== $type)
                      {
                          $sType = $i_GroupRoleOrganization[$type];
                          $pType = $i_GroupRoleOrganization[$prevType];
                          $image_lf = ${"i_schoolinfo_organization_iconleft_$sType"};
                          $image_rt = ${"i_schoolinfo_organization_iconright_$sType"};
                          $image_bt = ${"i_schoolinfo_organization_iconbottom_$pType"};
                          if ($prevType != -1)
                          {
                              $last = $row_count % 4;
                              $empty = ($last==0? 0 : 4 - $last);
                              for ($j=0; $j<$empty; $j++)
                              {
                                   $x .= "<td width=25%>&nbsp;</td>";
                              }
                              $x .= "</tr></table></td></tr></table>\n";
                              $x .= "<table width=710 border=0 cellspacing=0 cellpadding=0>
                                      <tr><td>$image_bt</td></tr></table><br>\n\n\n";
                          }
                          $x .= "<table class=schoolinfo_organization_$sType"."_m_bg width=710 border=0 cellspacing=0 cellpadding=0>
                                  <tr><td width=94>$image_lf</td><td class=td_corner>$image_rt</td></tr></table>\n";
                          $x .= "<table class=schoolinfo_organization_$sType"."_b_bg width=710 border=0 cellspacing=0 cellpadding=5>";
                          $x .= "<tr><td class=td_center_middle><table width=670 border=1 cellspacing=0 cellpadding=5 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5>";
                          $row_count = 0;
                          $prevType = $type;
                      }
                      if ($row_count%4==0 && $row_count!=0)
                      {
//                          $x .= "<td width=5%></td></tr>\n<tr><td width=5%></td>\n";
                          $x .= "</tr>\n<tr>\n";
                      }
                      if ($row_count ==0)
                      {
//                          $x .= "<tr><td width=5%></td>\n";
                          $x .= "<tr>\n";
//                          $row_count =1;
                      }
                      $row_count++;
                      $url = $this->returnImageIconForURL($url);
                      $x .= "<td width=25%><a href=javascript:fe_directory($gid)>$s_title <br>($num)</a> $url</td>\n";
                 }

                 $pType = $i_GroupRoleOrganization[$prevType];
                 $image_bt = ${"i_schoolinfo_organization_iconbottom_$pType"};
                 $last = $row_count % 4;
                 $empty = ($last==0? 0 : 4 - $last);
                 for ($j=0; $j<$empty; $j++)
                 {
                      $x .= "<td width=25%>&nbsp;</td>";
                 }
                 $x .= "</tr></table></td></tr></table>\n";
                 $x .= "<table width=710 border=0 cellspacing=0 cellpadding=0>
                         <tr><td>$image_bt</td></tr></table><br>\n";
                 return $x;
        }

        function returnClassTeacherArray()
        {
                 $name_field = getNameFieldByLang("b.");
                 $sql = "SELECT a.ClassName, $name_field
                                FROM INTRANET_CLASS as a, INTRANET_USER as b
                                     , INTRANET_CLASSTEACHER as c
                                WHERE a.ClassID = c.ClassID AND b.UserID = c.UserID";
                 $temp = $this->returnArray($sql,2);
                 for ($i=0; $i<sizeof($temp); $i++)
                 {
                      list($id,$data) = $temp[$i];
                      if ($result[$id]!="")
                          $result[$id] .= ", $data";
                      else $result[$id] = $data;
                 }
                 return $result;

                 #return build_assoc_array($this->returnArray($sql,2));
        }

        # list.php      eClass IP first release
        function ListAllGroupsForCat($category="", $hidegroups="")
        {
                 global $i_no_record_exists_msg,$image_path,$i_Teaching_ClassTeacher;
                 global $hideMember;                        # Option for hiding member list (number)
                 if ($category=="")
                 {
                     $conds = "WHERE a.RecordType != 0";
                 }
                 else
                 {
                     $conds = "WHERE a.RecordType = $category";
                 }
                 if (is_array($hidegroups) && sizeof($hidegroups)!=0)
                 {
                     $list = implode(",",$hidegroups);
                     $conds .= " AND a.GroupID NOT IN ($list)";
                 }
                 $title_field = $this->getGroupTitleByLang("a.");
                 $sql = "SELECT a.GroupID, $title_field as Title, a.RecordType, a.URL, b.CategoryName
                         FROM INTRANET_GROUP as a
                              LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b ON a.RecordType = b.GroupCategoryID
                         $conds AND a.AcademicYearID = ".$this->CurrentAcademicYearID." ORDER BY a.RecordType+0 ASC, $title_field ASC";
                 $groups = $this->returnArray($sql,5);
                 #$sql = "SELECT GroupID, COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE GroupID NOT IN (1,2,3,4) GROUP BY GroupID ORDER BY GroupID";
                 $sql = "SELECT a.GroupID, COUNT(a.UserGroupID)
                         FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                         WHERE a.GroupID NOT IN (1,2,3,4) AND b.RecordStatus IN (0,1,2) GROUP BY a.GroupID ORDER BY a.GroupID";
                 $counts = $this->returnArray($sql,2);
                 $groups2 = array();
                 for ($i=0; $i<sizeof($counts); $i++)
                 {
                      $groups2[$counts[$i][0]]=$counts[$i][1];
                 }

                 # Get Class Teacher Name
                 if ($category=="" || $category==3)
                 {
                     $class_teachers = $this->returnClassTeacherArray();
                 }

                 # define style classes
                 $css_array_top = array("","organization_admin_cell_top"
                                            ,"organization_academic_cell_top"
                                            ,"organization_class_cell_top"
                                            ,"organization_house_cell_top"
                                            ,"organization_eca_cell_top");
                 $css_array_left = array("","organization_admin_cell_left"
                                             ,"organization_academic_cell_left"
                                             ,"organization_class_cell_left"
                                             ,"organization_house_cell_left"
                                             ,"organization_eca_cell_left");
                 $css_array_bg = array("","organization_admin_cell_bg"
                                           ,"organization_academic_cell_bg"
                                           ,"organization_class_cell_bg"
                                           ,"organization_house_cell_bg"
                                           ,"organization_eca_cell_bg");
                 $css_array_right = array("","organization_admin_cell_right"
                                              ,"organization_academic_cell_right"
                                              ,"organization_class_cell_right"
                                              ,"organization_house_cell_right"
                                              ,"organization_eca_cell_right");
                 $css_array_bottom = array("","organization_admin_cell_bottom"
                                               ,"organization_academic_cell_bottom"
                                               ,"organization_class_cell_bottom"
                                               ,"organization_house_cell_bottom"
                                               ,"organization_eca_cell_bottom");

                 $other_css_top = "organization_others_cell_top";
                 $other_css_left = "organization_others_cell_left";
                 $other_css_bg = "organization_others_cell_bg";
                 $other_css_right = "organization_others_cell_right";
                 $other_css_bottom = "organization_others_cell_bottom";

                 # define images
                 $image_array_icon = array("","admin_icon.gif"
                                               ,"academic_icon.gif"
                                               ,"class_icon.gif"
                                               ,"house_icon.gif"
                                               ,"eca_icon.gif"
                                               );
                 $image_array_top_right = array("","admin_top_right.gif"
                                                    ,"academic_top_right.gif"
                                                    ,"class_top_right.gif"
                                                    ,"house_top_right.gif"
                                                    ,"eca_top_right.gif"
                                                    );
                 $image_array_graphic = array("","admin_graphic.gif"
                                                  ,"academic_graphic.gif"
                                                  ,"class_graphic.gif"
                                                  ,"house_graphic.gif"
                                                  ,"eca_graphic.gif"
                                                  );
                 $image_array_bottom_left = array("","admin_bottom_left.gif"
                                                      ,"academic_bottom_left.gif"
                                                      ,"class_bottom_left.gif"
                                                      ,"house_bottom_left.gif"
                                                      ,"eca_bottom_left.gif"
                                                      );
                 $image_array_bottom_right = array("","admin_bottom_right.gif"
                                                       ,"academic_bottom_right.gif"
                                                       ,"class_bottom_right.gif"
                                                       ,"house_bottom_right.gif"
                                                       ,"eca_bottom_right.gif"
                                                       );

                 $image_other_icon = "others_icon.gif";
                 $image_other_top_right = "others_top_right.gif";
                 $image_other_graphic = "others_graphic.gif";
                 $image_other_bottom_left = "others_bottom_left.gif";
                 $image_other_bottom_right = "others_bottom_right.gif";

                 $row_count = 0;
                 $prevType = -1;
                 if (sizeof($groups)!=0)
                 {
                     for ($i=0;$i<sizeof($groups); $i++)
                     {
                          list($gid, $title, $type,$url,$catName) = $groups[$i];
                          $cell_height = ($type==3? 130:100);

                          # Bound length of title
                          $s_title = intranet_wordwrap($title,20,"\n",1);

                          $num = ($groups2[$gid]==""? 0:$groups2[$gid]);
                          if ($prevType != $type)
                          {
                              if ($prevType != -1)
                              {
                                  # End tr tag


                                  $last = $row_count % 4;
                                  $empty = ($last==0? 0 : 4 - $last);
                                  for ($j=0; $j<$empty; $j++)
                                  {
                                       $x .= "<td width=164>&nbsp;</td>";
                                       if ($j!=$empty-1)
                                       {
                                           $x .= "<td width=11>&nbsp;</td>";
                                       }
                                  }

                                  $css_right = ($css_array_right[$prevType]==""? $other_css_right :$css_array_right[$prevType]);
                                  $css_left = ($css_array_left[$prevType]==""? $other_css_left :$css_array_left[$prevType]);
                                  $x .= "<td width=31 class=$css_right>&nbsp;</td></tr>
                                          <tr>
                                            <td class=$css_left>&nbsp;</td>
                                            <td colspan=7>&nbsp;</td>
                                            <td class=$css_right>&nbsp;</td>
                                          </tr>
                                        </table>";
                                  $css_bottom = ($css_array_bottom[$prevType]==""? $other_css_bottom :$css_array_bottom[$prevType]);
                                  $image_bottom_left = ($image_array_bottom_left[$prevType]==""? $image_other_bottom_left :$image_array_bottom_left[$prevType]);
                                  $image_bottom_right = ($image_array_bottom_right[$prevType]==""? $image_other_bottom_right :$image_array_bottom_right[$prevType]);
                                  $x .= "<tr>
                                           <td class=$css_bottom><img src=\"$image_path/organization/$image_bottom_left\"></td>
                                           <td class=$css_bottom>&nbsp;</td>
                                           <td><img src=\"$image_path/organization/$image_bottom_right\"></td>
                                         </tr>\n
                                         </table><br>\n";
/*
                                  $x .= "<tr>
                                           <td><img src=\"$image_path/organization/$icon_left_bottom\" width=30 height=20></td>
                                           <td ><img src=\"$image_path/organization/$icon_left_bottom\"</td>
                                           <td><img src=\"$image_path/organization/$icon_right_bottom\" width=31 height=20></td>
                                         </tr></table>\n";

                                  $x .= "<table width=750 border=0 cellspacing=0 cellpadding=0>
                                  <tr><td>&nbsp;</td></tr></table><br>\n\n\n";
                                  */
                              }
                              # New tr tag
                              $image_icon = ($image_array_icon[$type]==""?$image_other_icon:$image_array_icon[$type]);
                              $css_top = ($css_array_top[$type]==""?$other_css_top:$css_array_top[$type]);
                              $image_top_right = ($image_array_top_right[$type]==""?$image_other_top_right:$image_array_top_right[$type]);
                              $x .= "<table width=750 border=0 cellspacing=0 cellpadding=0>
                                       <tr>
                                         <td width=93><img src=\"$image_path/organization/$image_icon\" width=93 height=53></td>
                                         <td width=626 align=left valign=top class=$css_top>$catName</td>
                                         <td width=31 align=right class=td_bottom><img src=\"$image_path/organization/$image_top_right\"></td>
                                       </tr>\n";
                              $x .= "<tr>
                                       <td colspan=3>\n";
                              $row_count = 0;
                              $prevType = $type;
                          }
                          if ($row_count%4==0 && $row_count!=0)   # end tr, new tr
                          {
                              $css_right = ($css_array_right[$prevType]==""? $other_css_right :$css_array_right[$prevType]);
                              $css_left = ($css_array_left[$prevType]==""? $other_css_left :$css_array_left[$prevType]);
                              $x .= "<td width=31 class=$css_right>&nbsp;</td></tr>
                                      <tr>
                                      <td class=$css_left>&nbsp;</td>
                                      <td colspan=7>&nbsp;</td>
                                      <td class=$css_right>&nbsp;</td>
                                      </tr>
                                      </table>";
                              $css_left = ($css_array_left[$type]==""? $other_css_left :$css_array_left[$type]);
                              $x .= "<table width=750 border=0 cellpadding=0 cellspacing=0 class=body>
                                       <tr><td width=30 class=$css_left><img src=\"$image_path/spacer.gif\" width=10 height=$cell_height></td>\n";
                          }
                          else if ($row_count ==0)  # only new tr
                          {
                              $css_left = ($css_array_left[$type]==""? $other_css_left :$css_array_left[$type]);
                              $x .= "<table width=750 border=0 cellpadding=0 cellspacing=0 class=body>\n<tr>
                                       <td width=30 class=$css_left><img src=\"$image_path/spacer.gif\" width=10 height=$cell_height></td>\n";
                          }
                          $row_count++;
                          $url = $this->returnImageIconForURL($url);
                          $css_bg = ($css_array_bg[$type]==""? $other_css_bg :$css_array_bg[$type]);
                          $image_graphic = ($image_array_graphic[$type] == ""? $image_other_graphic :$image_array_graphic[$type]);
                          if ($type == 3)
                          {
                              # Get Teacher Name
                              $teacherName = $class_teachers[$title];
                              if ($teacherName != "")
                              {
                                  $cField = "$i_Teaching_ClassTeacher: <br>$teacherName<br>";
                              }
                              else
                              {
                                  $cField = "";
                              }
                          }
                          else
                          {
                              $cField = "";
                          }

                          $x .= "<td width=164 align=center valign=top class=$css_bg><a href=javascript:fe_directory($gid)>
                                  <img src=\"$image_path/organization/$image_graphic\" border=0><br>
                                   <font color=#006600><strong>$s_title</a></strong></font>
                                   <br>$cField ";
                          if (!$hideMember)
                               $x .= "<img src=\"$image_path/icon_no_of_person.gif\" width=16 height=14 align=absmiddle>($num)";
                          $x .= "$url</td>\n";
                          if ($row_count%4!=0)
                          {
                              $x .= "<td width=11>&nbsp;</td>\n";
                          }
                          #$x .= "<td width=25%><a href=javascript:fe_directory($gid)>$s_title <br>($num)</a> $url</td>\n";
                     }


                     $last = $row_count % 4;
                     $empty = ($last==0? 0 : 4 - $last);
                     for ($j=0; $j<$empty; $j++)
                     {
                          $x .= "<td width=164>&nbsp;</td>";
                          if ($j!=$empty-1)
                          {
                              $x .= "<td width=11>&nbsp;</td>";
                          }
                     }

                     $css_right = ($css_array_right[$type]==""? $other_css_right :$css_array_right[$type]);
                     $x .= "<td width=31 class=$css_right>&nbsp;</td></tr></table>\n";
                     #$x .= "</tr></table></td></tr></table>\n";
                     /*
                                  $x .= "</td></tr>\n";
                                  $icon_left_bottom = ($type_left_bottom[$prevType]==""? $other_left_bottom: $type_left_bottom[$prevType]);
                                  $icon_right_bottom = ($type_right_bottom[$prevType]==""? $other_right_bottom: $type_right_bottom[$prevType]);
                                  $cell_bottom_css = ($type_bottom_css[$prevType]==""? $other_bottom_css: $type_bottom_css[$prevType]);

                                  $x .= "<tr>
                                           <td class=$cell_bottom_css><img src=\"$image_path/organization/$icon_left_bottom\" width=30 height=20></td>
                                           <td class=$cell_bottom_css>&nbsp;</td>
                                           <td><img src=\"$image_path/organization/$icon_right_bottom\" width=31 height=20></td>
                                         </tr></table>\n";
                     */

                     $css_bottom = ($css_array_bottom[$prevType]==""? $other_css_bottom :$css_array_bottom[$prevType]);
                     $image_bottom_left = ($image_array_bottom_left[$prevType]==""? $image_other_bottom_left :$image_array_bottom_left[$prevType]);
                     $image_bottom_right = ($image_array_bottom_right[$prevType]==""? $image_other_bottom_right :$image_array_bottom_right[$prevType]);
                     $x .= "<tr>
                              <td class=$css_bottom><img src=\"$image_path/organization/$image_bottom_left\"></td>
                              <td class=$css_bottom>&nbsp;</td>
                              <td><img src=\"$image_path/organization/$image_bottom_right\"></td>
                            </tr>\n
                            </table>\n";
                 }
                 else
                 {
                     $type = $category;
                     $cell_height = ($type==3? 130:100);
                     $css_left = ($css_array_left[$type]==""? $other_css_left :$css_array_left[$type]);
                     $css_right = ($css_array_right[$type]==""? $other_css_right :$css_array_right[$type]);
                     $image_icon = ($image_array_icon[$type]==""?$image_other_icon:$image_array_icon[$type]);
                     $css_top = ($css_array_top[$type]==""?$other_css_top:$css_array_top[$type]);
                     $image_top_right = ($image_array_top_right[$type]==""?$image_other_top_right:$image_array_top_right[$type]);
                     $x = "<br><table width=750 border=0 cellspacing=0 cellpadding=0>
                              <tr>
                                <td width=93><img src=\"$image_path/organization/$image_icon\" width=93 height=53></td>
                                <td width=626 align=left valign=top class=$css_top>$catName</td>
                                <td width=31 align=right class=td_bottom><img src=\"$image_path/organization/$image_top_right\"></td>
                              </tr>\n";
                     $x .= "<tr>
                              <td colspan=3>
                                <table width=750 border=0 cellpadding=0 cellspacing=0 class=body>\n";

                     $x .= "<tr>
                              <td width=30 class=$css_left><img src=\"$image_path/spacer.gif\" width=10 height=$cell_height></td>\n";
                     $x .= "<td class=td_center_middle_h1 width=689>$i_no_record_exists_msg</td>\n";
                     $x .= "<td width=31 class=$css_right>&nbsp;</td>\n";
                     $x .= "</tr>\n";
                     $x .= "<tr>
                              <td class=$css_left>&nbsp;</td>
                              <td >&nbsp;</td>
                              <td class=$css_right>&nbsp;</td>
                            </tr>\n";
                     $css_bottom = ($css_array_bottom[$prevType]==""? $other_css_bottom :$css_array_bottom[$prevType]);
                     $image_bottom_left = ($image_array_bottom_left[$prevType]==""? $image_other_bottom_left :$image_array_bottom_left[$prevType]);
                     $image_bottom_right = ($image_array_bottom_right[$prevType]==""? $image_other_bottom_right :$image_array_bottom_right[$prevType]);
                     $x .= "</table></td></tr>\n";
                     $x .= "<tr>
                              <td class=$css_bottom><img src=\"$image_path/organization/$image_bottom_left\"></td>
                              <td class=$css_bottom>&nbsp;</td>
                              <td><img src=\"$image_path/organization/$image_bottom_right\"></td>
                            </tr>\n
                            </table>\n";
#                     $x .= "</table>\n";
                     #$x .= "</td></tr></table>\n";
/*
                          $x .= "<table width=750 border=0 cellspacing=0 cellpadding=0>
                                  <tr><td width=94></td><td class=td_corner></td></tr></table>\n";
                          $x .= "<table width=750 border=0 cellspacing=0 cellpadding=5>";
                          $x .= "<tr><td class=td_center_middle><table width=670 border=1 cellspacing=0 cellpadding=5 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5>";
                          $x .= "<tr><td>$i_no_record_exists_msg</td>";
                          */
                 }
                 return $x;
        }

        /*
        function ListAllGroupsForCat($category="")
        {
                 global $i_no_record_exists_msg;
                 if ($category=="")
                 {
                     $conds = "";
                 }
                 else
                 {
                     $conds = "WHERE a.RecordType = $category";
                 }
                 $sql = "SELECT a.GroupID, a.Title, a.RecordType, a.URL, b.CategoryName
                         FROM INTRANET_GROUP as a
                              LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b ON a.RecordType = b.GroupCategoryID
                         $conds AND a.AcademicYearID = ".$this->CurrentAcademicYearID." ORDER BY a.RecordType+0 ASC, a.Title ASC";
                 $groups = $this->returnArray($sql,5);
                 $sql = "SELECT GroupID, COUNT(UserGroupID) FROM INTRANET_USERGROUP WHERE GroupID NOT IN (1,2,3,4) GROUP BY GroupID ORDER BY GroupID";
                 $counts = $this->returnArray($sql,2);
                 $groups2 = array();
                 for ($i=0; $i<sizeof($counts); $i++)
                 {
                      $groups2[$counts[$i][0]]=$counts[$i][1];
                 }
                 $row_count = 0;
                 if (sizeof($groups)!=0)
                 {
                     for ($i=0;$i<sizeof($groups); $i++)
                     {
                          list($gid, $title, $type,$url,$catName) = $groups[$i];

                          # Bound length of title
                          $s_title = intranet_wordwrap($title,20,"\n",1);

                          $num = ($groups2[$gid]==""? 0:$groups2[$gid]);
                          if ($prevType != $type)
                          {
                              if ($prevType != -1)
                              {
                                  $last = $row_count % 4;
                                  $empty = ($last==0? 0 : 4 - $last);
                                  for ($j=0; $j<$empty; $j++)
                                  {
                                       $x .= "<td width=25%>&nbsp;</td>";
                                  }
                                  $x .= "</tr></table></td></tr></table>\n";
                                  $x .= "<table width=710 border=0 cellspacing=0 cellpadding=0>
                                  <tr><td>&nbsp;</td></tr></table><br>\n\n\n";
                              }
                              $x .= "<table width=710 border=0 cellspacing=0 cellpadding=0>
                                      <tr><td width=94>$catName</td><td class=td_corner></td></tr></table>\n";
                              $x .= "<table width=710 border=0 cellspacing=0 cellpadding=5>";
                              $x .= "<tr><td class=td_center_middle><table width=670 border=1 cellspacing=0 cellpadding=5 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5>";
                              $row_count = 0;
                              $prevType = $type;
                          }
                          if ($row_count%4==0 && $row_count!=0)
                          {
                              $x .= "</tr>\n<tr>\n";
                          }
                          if ($row_count ==0)
                          {
                              $x .= "<tr>\n";
                          }
                          $row_count++;
                          $url = $this->returnImageIconForURL($url);
                          $x .= "<td width=25%><a href=javascript:fe_directory($gid)>$s_title <br>($num)</a> $url</td>\n";
                     }

                     $last = $row_count % 4;
                     $empty = ($last==0? 0 : 4 - $last);
                     for ($j=0; $j<$empty; $j++)
                     {
                          $x .= "<td width=25%>&nbsp;</td>";
                     }
                 }
                 else
                 {
                          $x .= "<table width=710 border=0 cellspacing=0 cellpadding=0>
                                  <tr><td width=94></td><td class=td_corner></td></tr></table>\n";
                          $x .= "<table width=710 border=0 cellspacing=0 cellpadding=5>";
                          $x .= "<tr><td class=td_center_middle><table width=670 border=1 cellspacing=0 cellpadding=5 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5>";
                          $x .= "<tr><td>$i_no_record_exists_msg</td>";
                 }

                 $x .= "</tr></table></td></tr></table>\n";
                 $x .= "<table width=710 border=0 cellspacing=0 cellpadding=0>
                             <tr><td>&nbsp;</td></tr></table><br>\n";
                 return $x;
        }
        */
        function returnImageIconForURL ($target)
        {
                 global $i_image_home;
                 if ($target != "")
                     $x = "<A HREF=\"$target\" target=_blank> $i_image_home </a>";
                 else $x ="";
                 return $x;
        }
        function returnGroupNames($ids)
        {
                 $list = implode(",",$ids);
                 $title_field = $this->getGroupTitleByLang();
                 $sql = "SELECT $title_field as Title FROM INTRANET_GROUP WHERE GroupID IN ($list) ORDER BY RecordType, $title_field";
                 return $this->returnVector($sql);
        }
        function returnSelectedEventGroups($Type, $ID, $table, $tableID, $RecordType, $parAcademicYearId='')
        {
        	if ($parAcademicYearId == '') {
        		$targetAcademicYearId = $this->CurrentAcademicYearID;
        	}
        	else {
        		$targetAcademicYearId = $parAcademicYearId;
        	}
        	
        	$title_field = $this->getGroupTitleByLang("a.");
        	if($Type == "all")
        	{
        		if($RecordType == 0) {
        			$Cond_YearID = " (a.AcademicYearID = ".$targetAcademicYearId." OR a.AcademicYearID IS NULL) ";
        		}else{
        			$Cond_YearID = " (a.AcademicYearID = ".$targetAcademicYearId.") ";
        		}
        		$sql = "SELECT a.GroupID, CONCAT('(',c.CategoryName,') ', $title_field), b.GroupID
	                    FROM INTRANET_GROUP AS a
	                         LEFT OUTER JOIN $table AS b ON (a.GroupID = b.GroupID AND b.$tableID = $ID)
	                         LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as c ON a.RecordType = c.GroupCategoryID
	                    WHERE a.RecordType >= $RecordType AND $Cond_YearID ORDER BY c.GroupCategoryID, $title_field";
        	}
        	else if($Type == "selected")
        	{
        		if($RecordType == 0) {
        			$Cond_YearID = " (a.AcademicYearID = ".$targetAcademicYearId." OR a.AcademicYearID IS NULL) ";
        		}else{
        			$Cond_YearID = " (a.AcademicYearID = ".$targetAcademicYearId.") ";
        		}
        		
	            $sql = "SELECT a.GroupID, CONCAT('(',c.CategoryName,') ', $title_field), b.GroupID
	                    FROM INTRANET_GROUP AS a
	                         INNER JOIN $table AS b ON (a.GroupID = b.GroupID AND b.$tableID = $ID)
	                         INNER JOIN INTRANET_GROUP_CATEGORY as c ON a.RecordType = c.GroupCategoryID
	                    WHERE a.RecordType >= $RecordType AND $Cond_YearID ORDER BY c.GroupCategoryID, $title_field";
			}
			else
			{
				if($RecordType == 0) {
        			$Cond_YearID = " (a.AcademicYearID = ".$targetAcademicYearId." OR a.AcademicYearID IS NULL) ";
        		}else{
        			$Cond_YearID = " (a.AcademicYearID = ".$targetAcademicYearId.") ";
        		}
        		
				$sql = "SELECT a.GroupID, CONCAT('(',c.CategoryName,') ', $title_field), b.GroupID
	                    FROM INTRANET_GROUP AS a
	                         LEFT OUTER JOIN $table AS b ON (a.GroupID = b.GroupID AND b.$tableID = $ID)
	                         LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as c ON a.RecordType = c.GroupCategoryID
	                    WHERE a.RecordType >= $RecordType AND $Cond_YearID AND b.GroupID IS NULL ORDER BY c.GroupCategoryID, $title_field";
			}
            return $this->returnArray($sql,4);
        }

        // added by Ronald - used in /home/imail/choose/new/index.php, used to get the related groups
        function returnCurrentGroupsWithGroupCategory($all='', $IdentityGroup=''){
			Global $UserID;
			$SchoolYearID = $_SESSION['CurrentSchoolYearID'];
			$title_field = $this->getGroupTitleByLang("b.");
			if($IdentityGroup == "")
			{
				$cond = " AND a.GroupCategoryID != 0 ";
			}
			if($all == "all")
			{
        		$sql = "SELECT b.GroupID, CONCAT('(',a.CategoryName,') ',$title_field) FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) WHERE b.AcademicYearID = '$SchoolYearID' $cond ORDER BY a.CategoryName, $title_field";
        	}else{
        		$sql = "SELECT b.GroupID, CONCAT('(',a.CategoryName,') ',$title_field) FROM INTRANET_GROUP_CATEGORY AS a INNER JOIN INTRANET_GROUP AS b ON (a.GroupCategoryID = b.RecordType) INNER JOIN INTRANET_USERGROUP AS c ON (b.GroupID = c.GroupID) WHERE b.AcademicYearID = '$SchoolYearID' AND c.UserID = '$UserID' $cond ORDER BY a.CategoryName, $title_field";
        	}
            return $this->returnArray($sql,2);
        }
        
        function return_eCirculeUserIDForIdentityGroups ($id_groups)
        {
	        	 global $intranet_session_language;
			
				 $name_field = getNameFieldWithClassNumberByLang("a.");
				 
                 $list = implode(",",$id_groups);

                 $sql = "SELECT 
                 			DISTINCT a.UserID, $name_field
                 		FROM 
                 			INTRANET_USER as a LEFT OUTER JOIN
                 			INTRANET_USERGROUP as b ON (a.UserID = b.UserID) LEFT OUTER JOIN
                 			INTRANET_GROUP as c ON (b.GroupID = c.GroupID)
                 		WHERE 
                 			c.AcademicYearID = ".$this->CurrentAcademicYearID." AND
                 			a.RecordStatus = 1 AND 
                 			a.RecordType = 1 AND
                 			c.GroupID IN ($list)
                 		";
                 return $this->returnArray($sql,1);
        }
        
        function getGroupInfoByUserID($userid, $conds="") {
        	$title_field = $this->getGroupTitleByLang("g.");
        	$sql = "SELECT c.CategoryName, $title_field as Title FROM INTRANET_GROUP g LEFT OUTER JOIN INTRANET_GROUP_CATEGORY c ON (c.GroupCategoryID=g.RecordType) LEFT OUTER JOIN INTRANET_USERGROUP u ON (u.GroupID=g.GroupID) WHERE u.UserID='$userid' $conds GROUP BY g.GroupID";
        	return $this->returnArray($sql,2);
        }
        
        function getGroupTitleByLang($prefix="", $displayLang=""){
        	global $intranet_session_language;
        	
        	$displayLang = $displayLang ? $displayLang : $intranet_session_language;
        	$chi = ($displayLang == "b5" || $displayLang == "gb");
        	$title_field = $chi ? "TitleChinese" : "Title";
//         	$alt_title_field = $chi ? "Title" : "TitleChinese";
//         	$title_field =  "IF(".$prefix.$title_field." IS NULL OR TRIM(".$prefix.$title_field.") = '',".$prefix.$alt_title_field.",".$prefix.$title_field.")";
        	$title_field = $prefix.$title_field;
        	
        	return $title_field;
        }
}
?>