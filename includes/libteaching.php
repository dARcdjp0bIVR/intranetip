<?php
# using: 
##### Change Log [Start] #####
#
#	Date	:	2010-11-12 (Henry Chow)
#				modified returnSubjectList(), assign alias to table field
#
###### Change Log [End] ######
if (!defined("LIBTEACHING_DEFINED"))         // Preprocessor directives
{

 define("LIBTEACHING_DEFINED",true);

 class libteaching extends libdb{
       var $TeacherID;
       var $tableFormat;
       var $titleClass;
       var $classList;
       var $subjectList;
	   var $ReportCardSubjectList;

       function libteaching($tid="")
       {
                $this->libdb();
                $this->tableFormat = array(
                "width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0"
                );
                $this->titleClass = array(
                "tableTitle"
                );
                $this->classList = "";
                $this->subjectList = "";
                if ($tid != "")
                {
                    $this->TeacherID = $tid;
                    //retrieveRecord($tid);
                }
       }
       function retrieveRecord($id)
       {
                return $result;
       }
       function returnTeacherList()
       {
                $sql = "SELECT UserID, EnglishName, ChineseName
                        FROM INTRANET_USER
                        WHERE RecordType = 1 AND Teaching = 1 AND RecordStatus=1
                        ORDER BY EnglishName";
                return $this->returnArray($sql,3);
       }
       
       function returnTeacherClass ($uid, $academicYearID='')
       {
	       if ($academicYearID == '')
	       		$academicYearID = Get_Current_Academic_Year_ID();
	       		
	       $sql = 'select 
   						yc.YearClassID as ClassID,
						yc.ClassTitleEN as ClassName
					From 
						YEAR_CLASS as yc 
						inner join 
						YEAR as y 
						on yc.YearID = y.YearID 
						inner join 
						YEAR_CLASS_TEACHER yct 
						on (yc.YearClassID = yct.YearClassID and yc.AcademicYearID = \''.$academicYearID.'\' and yct.UserID = \''.$uid.'\') 
					order by 
						y.sequence, yc.sequence 
					';
				
				$result = $this->returnArray($sql,2);
				
				return $result;
	       /*
                $sql = "SELECT a.ClassID, a.ClassName
                        FROM INTRANET_CLASSTEACHER as b LEFT OUTER JOIN INTRANET_CLASS as a ON a.ClassID = b.ClassID
                        WHERE b.UserID = $tid";
                return $this->returnArray($sql,2);
                */
       }
       
       function returnTeacherClassWithLevel ($tid, $AcademicYearID='')
       {
	       /*
                $sql = "SELECT 
                			a.ClassID, 
                			a.ClassName, 
                			a.ClassLevelID, 
                			c.LevelName
                        FROM 
	                        INTRANET_CLASSTEACHER as b 
	                        LEFT OUTER JOIN INTRANET_CLASS as a ON a.ClassID = b.ClassID
	                        left join INTRANET_CLASSLEVEL as c on (c.ClassLevelID = a.ClassLevelID)
                        WHERE b.UserID = $tid";
           */
           
				global $intranet_session_language;
				$ClassTitleField = ($intranet_session_language=='en')? 'yc.ClassTitleEn' : 'yc.ClassTitleB5';
				$AcademicYearID = ($AcademicYearID=='')? Get_Current_Academic_Year_ID()  : $AcademicYearID;
				$sql = "	SELECT
										yc.YearClassID as ClassID,
										$ClassTitleField as ClassName,
										y.YearID as ClassLevelID,
										y.YearName as LevelName
							FROM
										YEAR_CLASS_TEACHER as yct
										INNER JOIN
										YEAR_CLASS as yc
										ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '$AcademicYearID' AND yct.UserID = '$tid')
										INNER JOIN
										YEAR as y
										ON (yc.YearID = y.YearID)
							";
                return $this->returnArray($sql,2);
               
       }
       /*
       function returnTeacherClassLevel ($tid)
       {
                $sql = "SELECT 
                			distinct(a.ClassLevelID),
                			c.LevelName
                        FROM 
                        	INTRANET_CLASSTEACHER as b 
                        	LEFT OUTER JOIN INTRANET_CLASS as a ON a.ClassID = b.ClassID
                        	left join INTRANET_CLASSLEVEL as c on (c.ClassLevelID = a.ClassLevelID)
                        WHERE 
                        	b.UserID = $tid
            	";
                return $this->returnArray($sql,2);
       }
       */
       function returnSubjectCount ($tid)
       {
                $sql = "SELECT COUNT(SubjectTeacherID) FROM INTRANET_SUBJECT_TEACHER WHERE UserID = '$tid'";
                $result = $this->returnVector($sql);
                return $result[0]+0;
       }
       function returnSubjectClassIDTaught($tid)
       {
                $sql = "SELECT a.SubjectID, a.ClassID FROM INTRANET_SUBJECT_TEACHER as a
                        LEFT OUTER JOIN INTRANET_CLASS as b ON a.ClassID = b.ClassID
                        WHERE a.UserID = '$tid' ORDER BY b.ClassName";
                return $this->returnArray($sql,2);
       }
       function returnClassList ()
       {
                if ($this->classList == "")
                {
                    $sql = "SELECT ClassID, ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
                    $this->classList = $this->returnArray($sql,2);
                }
                return $this->classList;
       }
       function returnSubjectList ($withSubSubject=0)
       {
                if ($this->subjectList == "")
                {
                    //$sql = "SELECT SubjectID, SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus = 1 ORDER BY SubjectName";
                    //$this->subjectList = $this->returnArray($sql,2);
                    $sql = "SELECT RecordID as SubjectID, ".Get_Lang_Selection("CH_DES","EN_DES")." as SubjectName FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1 ";
                    if(!$withSubSubject)
						$sql .= " and (CMP_CODEID is NULL or CMP_CODEID = '') ";
					$sql .=" ORDER BY DisplayOrder";
					$this->subjectList = $this->returnArray($sql,2);
			
                }
                return $this->subjectList;
       }
       # add on 20081128 yatwoon (eDiscipline1.2)
       function returnSubjectName($subject_id)
       {
	       $sql = "SELECT EN_DES FROM ASSESSMENT_SUBJECT WHERE RecordID='$subject_id'";
	       $result = $this->returnVector($sql);
			return $result[0];
	       /*
            $sql = "SELECT SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus = 1 and SubjectID=$subject_id";
            $result = $this->returnVector($sql);
                return $result[0];
                            */

       }
       function getClassSelect ($tags,$selected="")
       {
                global $i_Teaching_NotApplicable;
                $list = $this->returnClassList();
                $x = "<SELECT $tags>\n";
                $x .= "<OPTION value=0>$i_Teaching_NotApplicable</OPTION>\n";
                for ($i=0; $i<sizeof($list); $i++)
                {
                     list ($id, $name) = $list[$i];
                     $sel_str = ($id==$selected? "SELECTED":"");
                     $x .= "<OPTION value=\"".htmlspecialchars($id,ENT_QUOTES)."\" $sel_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
       function getSubjectSelect ($tags,$selected="")
       {
                global $i_Teaching_NotApplicable;
                $list = $this->returnSubjectList();
                $x = "<SELECT $tags>\n";
                $x .= "<OPTION value=0>$i_Teaching_NotApplicable</OPTION>\n";
                for ($i=0; $i<sizeof($list); $i++)
                {
                     list ($id, $name) = $list[$i];
                     $sel_str = ($id==$selected? "SELECTED":"");
                     $x .= "<OPTION value=$id $sel_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
       function getSelect ($list, $tags,$selected="",$noFirst=0)
       {
                global $i_Teaching_NotApplicable;
                $x = "<SELECT $tags>\n";
                $x .= ($noFirst==0) ? "<OPTION value=0>$i_Teaching_NotApplicable</OPTION>\n" : "";
                for ($i=0; $i<sizeof($list); $i++)
                {
                    list ($id, $name) = $list[$i];
										if(is_array($selected))
										{
											$sel_str = (in_array($id, $selected)) ? "SELECTED" : "";
										}
										else
										{
											$sel_str = ($id==$selected) ? "SELECTED" : "";
										}
										
                    $x .= "<OPTION value=\"".htmlspecialchars($id,ENT_QUOTES)."\" $sel_str>$name</OPTION>\n";
                }
                $x .= "</SELECT>\n";
                return $x;
       }
       function displayAllTeachingTable($format=0)
       {
                global $i_Teaching_NotClassTeacher,$i_no_record_exists_msg;
                global $i_Teaching_TeacherName,$i_Teaching_ClassTeacher,$i_Teaching_SubjectTaught;

                $x = "<table ".$this->tableFormat[$format]." >\n";
                $x .= "<tr><td class=".$this->titleClass[$format].">$i_Teaching_TeacherName</td><td class=".$this->titleClass[$format].">$i_Teaching_ClassTeacher</td><td class=".$this->titleClass[$format].">$i_Teaching_SubjectTaught</td></tr>\n";
                $teacherList = $this->returnTeacherList();
                for ($i=0; $i<sizeof($teacherList); $i++)
                {
                     $css = ($i%2==0? "":"2");
                     list ($id,$eng,$chi) = $teacherList[$i];
                     $result = $this->returnTeacherClass($id);
                     $teacherClass = ($result[0][1]==""? $i_Teaching_NotClassTeacher :$result[0][1] );
                     $numSubject = $this->returnSubjectCount($id);
                     $chiname = ($chi==""? "": "($chi)" );
                     $link = "<a class=functionlink href=\"edit.php?tid=$id\"> $eng $chiname </a>";
                     $x .= "<tr class=tableContent$css><td>$link</td><td>$teacherClass</td><td>$numSubject</td></tr>\n";
                }
                if (sizeof($teacherList)==0)
                {
                    $x .= "<tr><td colspan=3 align=center>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       function displayTeacherEdit($tid)
       {
                global $i_Teaching_ClassTeacher,$i_Teaching_ClassSubjects,$i_Teaching_Class,$i_Teaching_Subject;
                $availSubjects = $this->returnSubjectList();
                $availClasses = $this->returnClassList();
                $x = "<table width=500 border=0 cellpadding=3 cellspacing=1>\n";
                $class = $this->returnTeacherClass($tid);
                $classSelection = $this->getSelect($availClasses,"name=classT",$class[0][0]);
                $x .= "<tr><td>$i_Teaching_ClassTeacher:</td><td>$classSelection</td></tr>\n";
                $x .= "<tr><td>$i_Teaching_ClassSubjects:</td><td>&nbsp;</td></tr>\n";
                $x .= "<tr><td class=tableTitle_new align=center><u>$i_Teaching_Class</u></td><td class=tableTitle_new align=center><u>$i_Teaching_Subject</u></td></tr>\n";

                $subjects = $this->returnSubjectClassIDTaught($tid);
                $num = sizeof($subjects);
                $length = ($num+5>10? $num+5 : 10);
                for ($i=0; $i<$length; $i++)
                {
                     list ($sid,$cid) = $subjects[$i];
                     $classSelection = $this->getSelect($availClasses,"name=class$i",$cid);
                     $subjectSelection = $this->getSelect($availSubjects,"name=subject$i",$sid);
                     $x .= "<tr><td align=center>$classSelection</td><td align=center>$subjectSelection</td></tr>\n";
                }
                $x .= "</table>\n";
                $x .= "<input type=hidden name=size value=$length>\n";
                return $x;
       }
       function getTeachingInfo($teacherID)
       {
                $sql = "SELECT a.SubjectID, b.SubjectName, a.ClassID, c.ClassName, c.GroupID
                        FROM INTRANET_SUBJECT_TEACHER as a
                             LEFT OUTER JOIN INTRANET_SUBJECT as b ON a.SubjectID = b.SubjectID AND b.RecordStatus = 1
                             LEFT OUTER JOIN INTRANET_CLASS as c ON a.ClassID = c.ClassID
                        WHERE a.UserID = '$teacherID' ORDER BY c.ClassName, b.SubjectName";
                return $this->returnArray($sql,5);
       }
	
		#########################################################################################
		# Functions for Report Card System teaching settings
		function returnReportCardSubjectCount ($tid)
        {
                $sql = "SELECT COUNT(SubjectTeacherID) FROM RC_SUBJECT_TEACHER WHERE UserID = '$tid'";
                $result = $this->returnVector($sql);

                return $result[0]+0;
        }

		function displayReportCardTeachingTable($format=0)
		{
			global $i_no_record_exists_msg;
			global $i_Teaching_TeacherName,$i_Teaching_ClassTeacher,$i_Teaching_SubjectTaught;

			$x = "<table ".$this->tableFormat[$format]." >\n";
			$x .= "<tr><td class=".$this->titleClass[$format].">$i_Teaching_TeacherName</td><td class=".$this->titleClass[$format].">$i_Teaching_SubjectTaught</td></tr>\n";
			$teacherList = $this->returnTeacherList();
			for ($i=0; $i<sizeof($teacherList); $i++)
            {
                     $css = ($i%2==0? "":"2");
                     list ($id,$eng,$chi) = $teacherList[$i];
					 $numSubject = $this->returnReportCardSubjectCount($id);
                     $chiname = ($chi==""? "": "($chi)" );
                     $link = "<a class=functionlink href=\"teaching_edit.php?tid=$id\"> $eng $chiname </a>";
                     $x .= "<tr class=tableContent$css><td>$link</td><td>$numSubject</td></tr>\n";
			}
			if (sizeof($teacherList)==0)
			{
				$x .= "<tr><td colspan=3 align=center>$i_no_record_exists_msg</td></tr>\n";
			}
			$x .= "</table>\n";
			
			return $x;
		}

		function returnReportCardSubjectClassIDTaught($tid)
        {
			$sql = "SELECT 
						a.SubjectID, 
						a.ClassID,
						b.ClassName
					FROM 
						RC_SUBJECT_TEACHER as a
                        LEFT OUTER JOIN INTRANET_CLASS as b ON a.ClassID = b.ClassID
                    WHERE 
						a.UserID = '$tid' 
					ORDER BY 
						b.ClassName
					";
			$row = $this->returnArray($sql,3);

			for($k=0; $k<sizeof($row); $k++)
			{
				list($sid, $cid, $cname) = $row[$k];
				$ReturnArray[$sid][] = array($cid, $cname);
			}

			return $ReturnArray;
        }
		
		function returnReportCardSubjectList ()
        {
			global $eclass_db, $intranet_session_language;
			
			if($this->ReportCardSubjectList=="")
			{
				$NameField = ($intranet_session_language=="en") ? "EN_DES" : "CH_DES";
				$sql = "SELECT RecordID, $NameField FROM {$eclass_db}.ASSESSMENT_SUBJECT WHERE CMP_CODEID IS NULL ORDER BY DisplayOrder";
				$this->ReportCardSubjectList = $this->returnArray($sql,2);
			}

			return $this->ReportCardSubjectList;
        }

		function displayReportCardTeacherEdit($tid)
		{
			global $i_Teaching_ClassSubjects,$i_Teaching_Class,$i_Teaching_Subject, $intranet_session_language;

			$availSubjects = $this->returnReportCardSubjectList();

			$x = "<table width=500 border=0 cellpadding=3 cellspacing=1>\n";
			$x .= "<tr><td colspan='3'>$i_Teaching_ClassSubjects:</td></tr>\n";
			$x .= "<tr><td class=tableTitle_new align=center><u>$i_Teaching_Subject</u></td><td class=tableTitle_new align=center colspan='2'><u>$i_Teaching_Class</u></td></tr>\n";

			$SubjectClassArr = $this->returnReportCardSubjectClassIDTaught($tid);
			if(is_array($SubjectClassArr))
				$SubjectIDArray = array_keys($SubjectClassArr);
			
			$num = sizeof($SubjectIDArray);
			$length = ($num+5>10? $num+5 : 10);
			for ($i=0; $i<$length; $i++)
			{
				$sid = $SubjectIDArray[$i];
				$cid_array = $SubjectClassArr[$sid];

				$subjectSelection = $this->getSelect($availSubjects,"name=subject$i",$sid);
				$ClassList = "<div id=class_list$i>";
				$CNameList = "";
				$CIDList = "";
				if(!empty($cid_array))
				{
					for($j=0; $j<sizeof($cid_array); $j++)
					{
						list($cid, $cname) = $cid_array[$j];
						$CIDList .= (($j+1)==sizeof($cid_array)) ? $cid : $cid.", ";
						$CNameList .= (($j+1)==sizeof($cid_array)) ? $cname : $cname.", ";
					}
				}
				$ClassList .= $CNameList;
				$ClassList .= "</div>";
				$ClassList .= "<input type=hidden name=class_id_list$i value='".$CIDList."' />";
				
				$edit_btn = "<a href=\"javascript: jSELECT_CLASSES('$i');\"><img src='/images/admin/button/s_btn_edit_".$intranet_session_language.".gif' border='0'></a>";

				$classSelection = $this->getSelect($cid_array,$tag,"",$noFirst);
                $x .= "<tr><td align=center>$subjectSelection</td><td align=center>".$ClassList."</td><td valign='top' align='center'>".$edit_btn."</td></tr>\n";
				
				$x .= "<input type='hidden' name='ClassList$i' />";
			}
            $x .= "</table>\n";
            $x .= "<input type=hidden name=size value=$length>\n";
            
			return $x;
		}
		
		function Is_Class_Teacher($ParUserID, $AcademicYearID='')
		{
			$AcademicYearID = ($AcademicYearID=='')? Get_Current_Academic_Year_ID()  : $AcademicYearID;
			
			$sql = "SELECT
							DISTINCT(yct.YearClassID)
					FROM
							YEAR_CLASS_TEACHER as yct
							INNER JOIN
							YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID AND yc.AcademicYearID = '$AcademicYearID')
					WHERE
							yct.UserID = '$ParUserID'
					";
			$resultSet = $this->returnVector($sql);
			
			if (count($resultSet) > 0)
				return true;
			else
				return false;
		}
		
		function Is_Subject_Teacher($ParUserID, $AcademicYearID='')
		{
			$AcademicYearID = ($AcademicYearID=='')? Get_Current_Academic_Year_ID()  : $AcademicYearID;
			
			$sql = "SELECT
							DISTINCT(stc.SubjectGroupID)
					FROM
							SUBJECT_TERM_CLASS_TEACHER as stct
							INNER JOIN
							SUBJECT_TERM_CLASS as stc
							ON (stct.SubjectGroupID = stc.SubjectGroupID AND stct.UserID = '$ParUserID')
							INNER JOIN
							SUBJECT_TERM as st ON (stc.SubjectGroupID = st.SubjectGroupID)
							INNER JOIN
							ACADEMIC_YEAR_TERM as ayt ON (st.YearTermID = ayt.YearTermID AND ayt.AcademicYearID = '$AcademicYearID')
					";
			$resultSet = $this->returnVector($sql);
			
			if (count($resultSet) > 0)
				return true;
			else
				return false;
		}

 }


} // End of directives
?>