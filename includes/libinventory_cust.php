<?php
## Using By : 
###############################################

###########################################
#	Date:	2016-02-01 
#			PHP 5.4 fix: change split to explode
#
#	Date:	2014-04-01	YatWoon
#			SKH
#
###########################################

if (!defined("LIBINVENTORY_CUST_DEFINED"))                     
{

	define("LIBINVENTORY_CUST_DEFINED",true);
	
	class libinventory_cust extends libinventory 
	{
		function libinventory_cust ()
		{
			$this->libdb();
		}
		
		####################################################################################### 
		# [START] SKH
		#######################################################################################
		function returnYearFormat($date_var='')
		{
			list($y,$m,$d) = explode("-", $date_var);
			return $m.$d >= "0901" ? $y . "-" . substr($y+1,2,2) : $y-1 . "-" . substr($y,2,2);
		}
		####################################################################################### 
		# [END] SKH
		#######################################################################################
		
	}
}
?>