<?php
// ---------------------------
// class name must be the same
// as the php file name
// ---------------------------
//$ecCourseID = "eclassIP_dev";
//$ecUserKey = "2923_3449_1107408578";

class libportalmain
{
	//var $user_id="3449";			// local testing
	var $user_id;
	var $user_info = array();
	var $session_id;
	var $errMsg = array(
				"No error",
				"Permission denied!",
				"Failed to save due to unkown reason, please try again.",
				"Record not found."
				);

	//var $db = "eclassIP_dev";		// local testing
	var $db;		// if testing: set to a database
	//var $eclass_db = "ec3dev_eclass";		// local testing
	var $eclass_db = "localhost";
	var $eclass_prefix;



	// constructor function
	function libportalmain()
	{
		// -----------------------------------------
		// the method table describes all the
		// available methods for this class to flash
		// and define the roles of these methods
		// -----------------------------------------
		$this->methodTable = array(
			// name of the function

			"getLoadeClass" => array(
				"description" => "return eClass data",
				"access" => "remote",	// available values are private, public, remote
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			),

			"getLoadCommunity" => array(
				"description" => "return community/group data",
				"access" => "remote",	// available values are private, public, remote
				"returns" => array("boolean", "string", "string", "array")	// describe the argument to be passed
			)
		);
		$this->doConnectDB();

		return;
	}



	##########################* PORTAL INFORMATION FUNCTIONS *#########################

	function getLoadCommunity(){
		$user_id = $this->user_id;
		$data = "";
		$row_group = $this->returnUserGroupInfo($user_id);
		for ($i=0; $i<sizeof($row_group); $i++)
		{
			$groupList .= (($groupList!="") ? "," : "") . $row_group[$i][0];
		}
		if (sizeof($row_group)>0)
		{
			$count_file = $this->returnGroupsNewFile($groupList);
			$count_announcement = $this->returnGroupsNewAnnounce($groupList);
			$count_link = $this->returnGroupsNewLink($groupList);
			$count_bulletin = $this->returnGroupsNewBulletin($groupList);
			for ($i=0; $i<sizeof($row_group); $i++)
			{
				list($group_id, $group_title, $functions, $RecordTypeAdmin, $RecordType) = $row_group[$i];
				$isAnnouncement = true;
				if ($functions == "ALL")
				{
					$isBulletin = true;
					$isLink = true;
					$isFile = true;
				} else
				{
					$isBulletin = (substr($functions,2,1)=='1');
					$isLink = (substr($functions,3,1)=='1');
					$isFile = (substr($functions,4,1)=='1');
				}
				$record = Array(  "title" => $this->convertEncoding($group_title),
							"id" => $group_id,
							"accessible" => Array(	"file"=>$isFile,
											"announcement"=>$isAnnouncement,
											"link"=>$isLink,
											"bulletin"=>$isBulletin
										),
							"file" => (int) $count_file[$group_id],
							"announcement" => (int) $count_announcement[$group_id],
							"link" => (int) $count_link[$group_id],
							"bulletin" => (int) $count_bulletin[$group_id]
							);
				$data[] = $record;
			}
			$success = true;
			$errCode = 0;				// no error
		} else
		{
			$success = false;
			$errCode = 3;				// no error
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode], "data"=>$data);
		return $r_arr;
	}


	function returnUserGroupInfo($user_id){
	    $sql = "SELECT a.GroupID, a.Title, IF(a.FunctionAccess IS NULL,'ALL',REVERSE(BIN(a.FunctionAccess))), b.RecordType, a.RecordType FROM INTRANET_GROUP AS a, INTRANET_USERGROUP AS b WHERE a.GroupID = b.GroupID AND b.UserID = '".IntegerSafe($user_id) . "' AND a.GroupID = 27 ORDER BY a.RecordType+0, a.Title";
		$special = $this->getSqlResult($sql,5);
		$sql = "SELECT a.GroupID, a.Title, IF(a.FunctionAccess IS NULL,'ALL',REVERSE(BIN(a.FunctionAccess))), b.RecordType, a.RecordType FROM INTRANET_GROUP AS a, INTRANET_USERGROUP AS b WHERE a.GroupID = b.GroupID AND b.UserID = '".IntegerSafe($user_id) . "' AND a.GroupID != 27 ORDER BY a.RecordType+0, a.Title";
		$others = $this->getSqlResult($sql,5);
		$result = array_merge($special, $others);

		# Field : FunctionAccess
		#         - Bit-1 (LSB) : timetable
		#         - Bit-2       : chat
		#         - Bit-3       : bulletin
		#         - Bit-4       : shared links
		#         - Bit-5       : shared files
		#         - Bit-6       : question bank
		#         - Bit-7       : Photo Album
		#         - Bit-8 (MSB) : Survey

		return $result;
	}



	function returnGroupsNewFile($groupList=""){
		if ($groupList != "")
		{
			$conds = "AND GroupID IN ($groupList)";
		}
		$uid = $this->user_id;
		$sql = "SELECT GroupID, COUNT(FileID) FROM INTRANET_FILE
				WHERE locate(';$uid;',ReadFlag)=0 $conds
				GROUP BY GroupID";

		$result = $this->getSqlResult($sql,2);
		return $this->build_assoc_array($result);
	}


	function returnGroupsNewAnnounce($groupList=""){
		$uid = $this->user_id;
		if ($groupList != "")
		{
			$conds = "AND a.GroupID IN ($groupList)";
		}
		$sql = "SELECT a.GroupID, COUNT(DISTINCT b.AnnouncementID)
			FROM INTRANET_GROUPANNOUNCEMENT as b LEFT OUTER JOIN INTRANET_GROUP as a ON a.GroupID = b.GroupID
			LEFT OUTER JOIN INTRANET_ANNOUNCEMENT as c ON b.AnnouncementID = c.AnnouncementID
			WHERE c.EndDate IS NULL OR (CURDATE()>=c.AnnouncementDate AND CURDATE() <= c.EndDate) $conds
			AND (c.ReadFlag IS NULL OR locate(';$uid;', c.ReadFlag)=0)
			GROUP BY a.GroupID";

		$result = $this->getSqlResult($sql,2);
		return $this->build_assoc_array($result);
	}


	function returnGroupsNewLink($groupList=""){
		if ($groupList != "")
		{
			$conds = "AND GroupID IN ($groupList)";
		}
		$uid = $this->user_id;
		$sql = "SELECT GroupID, COUNT(LinkID) FROM INTRANET_LINK
				WHERE locate(';$uid;',ReadFlag)=0 $conds
				GROUP BY GroupID";

		$result = $this->getSqlResult($sql,2);
		return $this->build_assoc_array($result);
	}


	function returnGroupsNewBulletin($groupList="")
	{
		if ($groupList != "")
		{
			$conds = "AND GroupID IN ($groupList)";
		}
		$uid = $this->user_id;
		$sql = "SELECT GroupID, COUNT(BulletinID) FROM INTRANET_BULLETIN
				WHERE locate(';$uid;',ReadFlag)=0 $conds
				GROUP BY GroupID";

		$result = $this->getSqlResult($sql,2);
		return $this->build_assoc_array($result);
	}


	function getLoadeClass(){
		$user_id = $this->user_id;
		$data = "";
		$row_course = $this->returnUserIDCourseInfo($user_id);
		if (sizeof($row_course)>0)
		{
			for ($i=0; $i<sizeof($row_course); $i++)
			{
				list($course_id, $course_code, $course_name, $user_id_now, $memberType, $user_course_id) = $row_course[$i];
				$record = Array(  "title" => $this->convertEncoding("$course_code - $course_name"),
							"id" => $user_course_id,
							"contents" => $this->returnNewContent($course_id, $user_id_now, $memberType),
							"announcement" => $this->returnNewAnnouncement($course_id, $user_id_now),
							"link" => $this->returnNewLink($course_id, $user_id_now),
							"bulletin" => $this->returnNewBulletin($course_id, $user_id_now, $memberType)
							);

				$data[] = $record;
			}
			$success = true;
			$errCode = 0;				// no error
		} else
		{
			$success = false;
			$errCode = 3;				// no error
		}

		$r_arr = array("success"=>$success, "errCode"=>$errCode, "errMsg"=>$this->errMsg[$errCode], "data"=>$data);
		return $r_arr;
	}


	function returnUserIDCourseInfo($user_id){
		$eclass_db = $this->eclass_db;
		$intranet_db = $this->db;
		$fieldname  = "a.course_id, a.course_code, a.course_name, ";
		$fieldname .= "b.user_id, b.memberType, b.user_course_id ";
		$sql = "SELECT $fieldname FROM {$eclass_db}.course AS a, {$eclass_db}.user_course AS b, {$intranet_db}.INTRANET_USER AS c
				WHERE a.RoomType = 0 AND a.course_id = b.course_id
					AND (b.status is NULL OR b.status NOT IN ('deleted')) AND b.user_email=c.UserEmail
					AND c.UserID='$user_id'
				GROUP BY a.course_id
				ORDER BY a.course_code, a.course_name ";

		return $this->getSqlResult($sql,6);
	}


	function returnNewContent($course_id, $user_id, $memberType){
		if ($memberType=="G" || $memberType=="S")
		{
			# find all parent nodes
			$sql = "SELECT n.notes_id, n.a_no FROM ".$this->eclass_prefix."c".$course_id.".notes AS n WHERE b_no=0 AND c_no=0 AND d_no=0 AND e_no=0 ";
			$row_notes = $this->getSqlResult($sql, 2);
			for ($i=0; $i<sizeof($row_notes); $i++)
			{
				$notes_ids .= (($notes_ids!="") ? "," : "") . $row_notes[$i][0];
			}

			# filter all unaccessible nodes (group permission)
			if ($notes_ids!="")
			{
				$sql = "SELECT DISTINCT gf.function_id, ug.user_id FROM ".$this->eclass_prefix."c".$course_id.".grouping_function AS gf LEFT JOIN ".$this->eclass_prefix."c".$course_id.".user_group AS ug
					ON ug.group_id=gf.group_id
					WHERE gf.function_id IN ($notes_ids) AND gf.function_type='NOTE' AND ug.user_id
					ORDER BY gf.function_id ";
				$row_grouping = $this->getSqlResult($sql, 2);

				for ($i=0; $i<sizeof($row_grouping); $i++)
				{
					list($notes_id, $uid) = $row_grouping[$i];
					# set to be blocked initially
					if (!$notes_flag[$notes_id])
					{
						$notes_id_blocked[$notes_id] = true;
						$notes_flag[$notes_id] = true;
					}
					# set to be unblocked if the user is in a group with permission
					if ($uid==$user_id)
					{
						$notes_id_blocked[$notes_id] = false;
					}
				}
			}
			for ($i=0; $i<sizeof($row_notes); $i++)
			{
				list($notes_id, $a_no) = $row_notes[$i];
				if (!$notes_id_blocked[$notes_id])
				{
					$allowed_no .= (($allowed_no!="") ? "," : "") . $a_no;
				}
			}

			# count new nodes
			if ($allowed_no!="")
			{
				$sql = "SELECT COUNT(*) FROM ".$this->eclass_prefix."c".$course_id.".notes WHERE status = '1' AND a_no IN ($allowed_no) AND (readflag IS NULL OR readflag NOT LIKE '%;".$user_id.";%') ";
				$count= $this->getSqlResult($sql, 1);
			}
		} else
		{
			$sql = "SELECT COUNT(*) FROM ".$this->eclass_prefix."c".$course_id.".notes WHERE status = '1' AND (readflag IS NULL OR readflag NOT LIKE '%;".$user_id.";%') ";
			$count = $this->getSqlResult($sql, 1);
		}

		return (int) $count[0][0];
	}


	function returnNewAnnouncement($course_id, $user_id){
		$sql = "SELECT COUNT(announcement_id) FROM ".$this->eclass_prefix."c".$course_id.".announcement WHERE (readflag is null OR readflag NOT LIKE '%;$user_id;%')";
		$count = $this->getSqlResult($sql,1);

		return (int) $count[0][0];
	}


	function returnNewLink($course_id, $user_id){
		$sql = "SELECT COUNT(weblink_id) FROM ".$this->eclass_prefix."c".$course_id.".weblink WHERE (readflag is null OR readflag NOT LIKE '%;$user_id;%')";
		$count = $this->getSqlResult($sql,1);

		return (int) $count[0][0];
	}


	function returnNewBulletin($course_id, $user_id, $memberType){
		$count = 0;
		$sql = "SELECT bulletin_id, forum_id
				FROM ".$this->eclass_prefix."c".$course_id.".bulletin
				WHERE (readflag is null OR readflag NOT LIKE '%;$user_id;%')
				ORDER BY forum_id ";
		$row_bulletin = $this->getSqlResult($sql,2);

		$forum_ids = Array();
		for ($i=0; $i<sizeof($row_bulletin); $i++)
		{
			list($bulletin_id, $forum_id) = $row_bulletin[$i];
			if (!in_array($forum_id, $forum_ids))
			{
				$forum_ids[] = $forum_id;
			}
		}

		# verify group access right
		if (sizeof($forum_ids)>0)
		{
			$sql = "SELECT DISTINCT gf.function_id, ug.user_id FROM ".$this->eclass_prefix."c".$course_id.".grouping_function AS gf LEFT JOIN ".$this->eclass_prefix."c".$course_id.".user_group AS ug
					ON ug.group_id=gf.group_id
					WHERE gf.function_id IN (".implode(",", $forum_ids).") AND gf.function_type='B' AND ug.user_id
					ORDER BY gf.function_id ";
			$row = $this->getSqlResult($sql,2);
			for ($i=0; $i<sizeof($row); $i++)
			{
				list($forum_id, $uid) = $row[$i];
				# set to be blocked initially
				if (!$forum_flag[$forum_id])
				{
					$forum_id_blocked[$forum_id] = true;
					$forum_flag[$forum_id] = true;
				}
				# set to be unblocked if the user is in a group with permission
				if ($uid==$user_id)
				{
					$forum_id_blocked[$forum_id] = false;
				}
			}
		}
		for ($i=0; $i<sizeof($row_bulletin); $i++)
		{
			list($bulletin_id, $forum_id) = $row_bulletin[$i];
			if (!$forum_id_blocked[$forum_id])
			{
				$count++;
			}
		}

		return (int) $count;
	}





	###########################* COMMON FUNCTIONS *##########################


	// This function will pretend as authentication but used to initialize
	function _authenticate($db, $userKey){
		if ($db!="" && $userKey!="")
		{
			$this->db = $db;

			# KEY: login_session_id + user_id + timestamp
			list($session_id, $user_id, $ts) = explode("_", $userKey);

			# normal access
			$sql = "SELECT LoginSessionID FROM INTRANET_LOGIN_SESSION
					WHERE LoginSessionID='$session_id' AND UserID='$user_id'
						AND UNIX_TIMESTAMP(StartTime)='$ts' ";
			$row = $this->getSqlResult($sql, 1);

			if (sizeof($row)>0)
			{
				$this->user_id = $user_id;
				$this->session_id = $session_id;
				return true;
			} else
			{
				# authentication failed => load wrong DB
				$this->db = "login failed";
			}
		}

		return false;
	}


	function getParseArray(){
		$rArr = array();
		for ($i=0; $i<func_num_args(); $i+=2)
		{
			$value = is_array(func_get_arg($i+1)) ? func_get_arg($i+1) : trim(func_get_arg($i+1));
			$arrB = array(func_get_arg($i)=>$value);
			$rArr = array_merge($rArr, $arrB);
		}

		return $rArr;
	}



	function getSqlResult($sql, $field_no){
		$i = 0;
		$result = mysql_db_query($this->db, $sql);
		if ($result && mysql_num_rows($result)!=0)
		{
			while ($row = mysql_fetch_array($result))
			{
				for ($k=0; $k<$field_no; $k++)
				{
					$x[$i][$k] = $row[$k];
				}
				$i++;
			}
		}
		mysql_free_result($result);

		return $x;
	}


	function doConnectDB(){
		# load eClass config and connect to MySql
		$BLOCK_LIB_LOADING = true;
		include_once("../../global.php");
		$this->eclass_db = $eclass_db;
		$this->eclass_prefix = $eclass_prefix;
		$link = mysql_connect("localhost", $intranet_db_user, $intranet_db_pass);

		return;
	}


	function doQuery($query){
		return mysql_db_query($this->db, $query);
	}


	function getSqlInsertID(){
		return mysql_insert_id();
	}


	function convertEncoding($txt){
		//return iconv("big5", "UTF-8", $txt);
		return $txt;
	}


	# Change 2D array to 1D with the first column as index
	function build_assoc_array($array){
		for ($i=0; $i<sizeof($array); $i++)
		{
			list($id,$data) = $array[$i];
			$result[$id] = $data;
		}

		return $result;
	}


	function testData($answers){
		$sqlstr = (is_array($answers)) ? implode(", ", $answers) : $answers;
		$sql = "INSERT INTO ec3dev_eclass.test (sqlstr, inputtime) VALUES ('$sqlstr', now()) ";

		return $this->doQuery($sql);
	}
}
?>