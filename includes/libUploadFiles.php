<?php
if (!defined("LIBUPLOADFILES_DEFINED"))         // Preprocessor directives
{

	define("LIBUPLOADFILES_DEFINED",true);
	class libUploadFiles{
		const SIZE_EXIST = 1;
		const CANNOT_UPLOAD_TO_SERVER = 2;
		const RENAME_FILE_EXIST = 3;
		const CANNOT_MOVE_TO_DIST = 4;
		const SERVER_DIRECTORY_ERROR = 5;

		private $uploadFile;
		private $uploadReport;
		private $uploadFileRoot; 

		private $Name;
		private $FileType;
		private $Tmp_Name;
		private $Error;
		private $Size;

		public function libUploadFiles($uploadFileRoot){
			$this->uploadFile = array();
//			$this->uploadReport = array();
			$this->uploadFileRoot = $uploadFileRoot;
		}


		public function setName($val){$this->Name=$val;} 
		public function getName(){return $this->Name;}

		public function setFileType($val){$this->Type=$val;} 
		public function getFileType(){return $this->Type;}

		public function setTmp_Name($val){$this->Tmp_Name=$val;} 
		public function getTmp_Name(){return $this->Tmp_Name;}

		public function setError($val){$this->Error=$val;} 
		public function getError(){return $this->Error;}

		public function setSize($val){$this->Size=$val;} 
		public function getSize(){return $this->Size;}

		public function upload(){

			$uniqueKey = session_id().time().uniqid();

			/////////////////////////////////////////////////////
			//check the upload document exist or not , if not exist, stop the upload
			/////////////////////////////////////////////////////
			if(!file_exists($this->uploadFileRoot)){
				$this->uploadReport = self::SERVER_DIRECTORY_ERROR;
				return $this->uploadReport;
			}

			$today = date('Ymd');
			/////////////////////////////////////////////////////
			//prepare the upload directory with a date folder
			/////////////////////////////////////////////////////
			$uploadDirectory = $this->uploadFileRoot.'/'.$today;
			if(!file_exists($uploadDirectory))
			{
				$result = mkdir($uploadDirectory);
				if(!$result)
				{
					$this->uploadReport = self::SERVER_DIRECTORY_ERROR;
					return $this->uploadReport;
				}else{
					if(!file_exists($uploadDirectory))
					{
						$this->uploadReport = self::SERVER_DIRECTORY_ERROR;
						return $this->uploadReport;
					}
					/*
					else{
						//do nothing , and go on the program
					}
					*/
				}
			}

			/////////////////////
			//handle the upload
			/////////////////////
			$_distFile = '';
			$_failCode = '';
			$_uploadSuccess = false;
			$_uniqueFullPath = '';
			$_renameFile = '';

			$_name = $this->getName();
			$_type = $this->getFileType();
			$_tmp_name = $this->getTmp_Name();
			$_error = $this->getError();
			$_size = $this->getSize();
			$_renameFile= $uniqueKey;

			if($_error == 0)
			{
				//no error report , then start upload
				if(file_exists($_tmp_name))
				{
					$_uniqueFullPath = $uploadDirectory.'/'.$_renameFile;
					if(file_exists($_uniqueFullPath))
					{
						//an file with equal name exist, don't upload
						$_uploadSuccess = false;
						$_failCode = self::RENAME_FILE_EXIST;
					}
					else
					{
						move_uploaded_file($_tmp_name,$_uniqueFullPath);
						if(file_exists($_uniqueFullPath))
						{
							$_uploadSuccess = true;					
								}else{
									$_uploadSuccess = false;
									$_failCode = self::CANNOT_MOVE_TO_DIST;
								}
					}
				}
				else
				{
							$_uploadSuccess = false;
							$_failCode = self::CANNOT_UPLOAD_TO_SERVER;
				}
			}

			$tmpAry['name'] = $_name;
			$tmpAry['type'] = $_type;
			$tmpAry['tmp_name'] = $_tmp_name;
			$tmpAry['error'] = $_error;
			$tmpAry['size'] = $_size;
			$tmpAry['uploadSuccess'] = $_uploadSuccess;
			$tmpAry['failCode'] = $_failCode;
			$tmpAry['renameFile'] = $_renameFile;
			$tmpAry['destFolder'] = $today;
			$tmpAry['fullPath'] = $_uniqueFullPath;
					
			$this->uploadReport = $tmpAry;
			return $this->uploadReport;
		}
		public function toString(){
			//debug_r($this->uploadFile);
		}

	}

}
?>