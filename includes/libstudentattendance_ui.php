<?php
# Using:   
################################################# Change log ####################################################
# 2020-10-05 by Ray: modify Get_Search_Report, change $ColumnClassName, $ColumnClassNumber
# 2020-06-17 by Ray: modify Get_Preset_Absence_Import_Finish_Page, Get_Preset_Absence_Import_Confirm_Page, add TW
# 2020-06-03 by Ray: modify Get_Lesson_Attendance_Panel_Form, add TW
# 2020-02-06 by Ray: modify Get_Class_Monthly_Attendance_Report, Get_Class_Monthly_Attendance_Report_By_Date_Range add $non_schoolday_reason
# 2020-01-03 by Ray: modify Get_Class_Monthly_Attendance_Report, Get_Class_Monthly_Attendance_Report_By_Date_Range add $sys_custom['StudentAttendance']['KIS_AttendanceUseClassGroup']
# 2019-12-10 by YatWoon: modified Get_Remove_Past_Data_Calendar()
# 2019-12-09 by YatWoon: add target class, modified function Get_Remove_Past_Data_Form();
# 2019-11-14 by Carlos: modified Get_No_Tapping_Card_Report(), LunchOutTime only check AMStatus is enough, thus removed the PMStatus to check.
# 2019-08-02 by Ray: added SeriousLate in Get_Search_Report for all recordtype
# 2019-02-15 by Carlos: [ip2.5.10.2.1] modified Get_Lesson_Daily_Status_Overview2(), do not new libuser() to get user name in the for loop that cause slow performance.
# 2018-11-15 by Cameron:[ip2.5.10.1.1] add route selection filter in Get_Confirm_Selection_Form() for eSchoolBus. It's for waiving late record in case of traffic jam, add $onDatePickSelectedFunction to DatePicker
# 2018-11-12 by Carlos: [ip2.5.9.11.1] $sys_custom['SmartCardAttendance_ReasonCountAbsentSession'] - modified Get_Class_Monthly_Attendance_Report() and Get_Class_Monthly_Attendance_Report_By_Date_Range() to count absent reason with absent sessions. 
# 2018-02-06 by Carlos: [ip2.5.9.3.1] fixed Get_Search_Report(), for early leave, display submitted prove document status value.
# 2017-12-21 by Carlos: [ip2.5.9.1.1] modified Get_No_Miss_Conduct_Report() do not include no class students.
# 2017-08-08 by Carlos: [ip2.5.8.10.1] $sys_custom['StudentAttendance']['HostelAttendance'] modified Get_Confirm_Selection_Form() to hide session slot radio selection for hostel attendance.
# 2016-08-16 by Carlos: [ip2.5.7.10.1] $sys_custom['LessonAttendance_LaSalleCollege'] - modified Get_Lesson_Attendance_Panel_Form().
# 2016-06-24 by Carlos: [ip2.5.7.8.1] modified Get_Preset_Outing_Import_confirm(), add fields [LeavesAt],[Location] and [Remark].
# 2016-06-08 by Carlos: [ip2.5.7.7.1] modified Get_Lesson_Attendance_Panel_Form(), added reason input.
# 2016-03-08 by Carlos: [ip2.5.7.3.1] modified Get_Lesson_Attendance_Panel_Form(), added flag $sys_custom['LessonAttendance_DisplayTime'] to display in time.
# 2016-02-04 by Carlos: [ip2.5.7.3.1] modified Get_Search_Report(), added Outing attendance type. 
# 2015-10-27 by Carlos: [ip2.5.6.12.1] modified Get_Search_Report(), display leave time for early leave.
# 2015-09-21 by Carlos: [ip2.5.6.10.1] modified Get_Lesson_Attendance_Panel_Form(), added hidden field LastLoadTime for checking page load time against data updated time. 
# 2015-07-09 by Carlos: [ip2.5.6.7.1] modified Get_Search_Report(), added Prove Document data column.
# 2015-07-06 by Carlos: [ip2.5.6.7.1] modified Get_No_Miss_Conduct_Report(), split finding absent records query into two queries, i.e. AM and PM. (One query is impossible to map the correct dailylog status).
# 2015-05-27 by Carlos: [ip2.5.6.7.1] modified Get_Search_Report(), added param $ReportType, put all column options to one array param. 
# 2015-01-28 by Carlos: [ip2.5.6.3.1] Modified Get_Preset_Outing_Form(), new outing records support input multiple dates
# 2015-01-23 by Carlos: [ip2.5.6.3.1] Modified Get_Class_Monthly_Attendance_Report_By_Date_Range(), limit school day count by date range
# 2014-12-09 by Omas: [ip2.5.5.12.1] Modified Get_Preset_Absence_Form(), Get_Preset_Absence_Import_Confirm_Page() new field document status for hand-in prove document status
# 2014-11-11 by Carlos: [ip2.5.5.12.1] Modified Get_Class_Monthly_Attendance_Report() and Get_Class_Monthly_Attendance_Report_By_Date_Range() continuous absence counting skip counting non-school days(assume days without data are non-school days).
# 2014-10-22 by Omas:	-modified Get_Class_Monthly_Attendance_Report(), Get_Class_Monthly_Attendance_Report_By_Date_Range(), will not show AP/PM in report when using AM/PM attendance mode
#						-[KIS] modified Get_Class_Monthly_Attendance_Report(), Get_Class_Monthly_Attendance_Report_By_Date_Range() , to Show BirthDate & EntryDate
# 2014-09-25 by Carlos: modified Get_Preset_Outing_Form(), only get active staff users for PIC
# 2014-07-16 by Bill:	modified Get_Confirm_Selection_Form(), Get_LateAbsentEarlyClassGroup_Confirm_JS() to show radio button instead of drop down list and correct calendar display
# 2014-06-23 by Carlos: modified Get_Waive_Absence_Table()
# 2014-03-18 by Carlos: added Get_Waive_Absence_Table(), getOverdueNotice(), getNotWaiveAbsenceReport()
# 2014-02-28 by Carlos: modified Get_Class_Monthly_Attendance_Report() and Get_Class_Monthly_Attendance_Report_By_Date_Range(), still display student list even if no daily log data found
# 2014-01-28 by Carlos: modified Get_Search_Report(), get remark data
# 2013-12-17 by Carlos: modified Get_Preset_Outing_Form(), added a warning div layer
# 2013-08-30 by Carlos: modified Class_Lesson_Daily_Report(), added Class_Lesson_Daily_Report_Export() to generate a detail csv report
# 2013-08-28 by Henry:	added parameter "$CheckedValue" in Get_Student_Selection_List()
# 2013-08-12 by Henry:  added Get_Lesson_Daily_Status_Overview2 from the new layout of Lesson Daily Status 
# 2013-08-06 by Henry:  added Get_Class_Lesson_Daily_Report_Form_Student() and Get_Lesson_Attendance_Panel_Form()
#						added a parameter to call Call_Lesson_Attendance_Flash in Get_Lesson_Daily_Status_Overview()
# 2013-08-06 by Carlos: added Get_Lesson_Non_Confirmed_Report_Form(), Get_Lesson_Non_Confirmed_Report_Table()
# 2013-05-14 by Carlos: fixed Get_Class_Monthly_Attendance_Report_By_Date_Range_Get_Month() 
# 2012-12-28 by Carlos: modified Get_Class_Monthly_Attendance_Report() and Get_Class_Monthly_Attendance_Report_By_Date_Range()
#						- display remark at RHS of name for archived/left users
#						- display absent data in red when continuous absent for certain days
#						- display table cell background in grey for holiday dates
#						- display preset absence data
# 2012-09-10 by Carlos: modified Get_Entry_Leave_Date_Index(), added Export button and [Remove all entry/leave dates] button
# 2012-08-02 by Carlos: modified Get_Student_Selection_List() remove fixed width for <select>, set min-width to 200px
# 2012-07-18 by Rita : Add Get_Class_Monthly_Attendance_Report_By_Date_Range(), Get_Class_Monthly_Attendance_Report_By_Date_Range_Get_Month()
# 2012-07-10 by Carlos : modified all queries that order by ClassNumber by casting to number for sorting
# 2012-01-10 by YatWoon: update Get_Class_Monthly_Attendance_Report(), add page break for the report
# 2012-01-05 by Carlos : modified Class_Lesson_Daily_Report() and Get_Lesson_Summary_Report() added print and csv report format
# 2011-12-21 by Carlos : modified Get_No_Tapping_Card_Report() added status field to LunchOut and LeaveSchool
# 2011-12-19 by Carlos : modified Get_Class_Monthly_Attendance_Report() to optionally show left/archived students
# 2011-12-14 by Carlos : added Get_Remove_Past_Data_Form(), Get_Remove_Past_Data_Calendar()
# 2011-11-03 by Carlos : fix record time display in Get_Search_Report()
#	Date	:	2011-09-21 Henry Chow
#				modified Get_Edit_eNotice_Template_Form(), check the checkbox option base on "SendReplySlip"				 
#
# 2011-07-04 by Carlos : Modified Get_LateAbsentEarlyClassGroup_Confirm_JS() to explicitly check confirm status 
# 2010-10-05 by Carlos : Added on time stat to Get_Class_Monthly_Attendance_Report()
# 2010-06-18 by Carlos : Added Get_Class_Monthly_Attendance_Report()
#################################################################################################################
include_once ('libcardstudentattend2.php');

class libstudentattendance_ui extends libcardstudentattend2 {
	var $ThickBoxWidth;
	var $ThickBoxHeight;

	function libstudentattendance_ui() {
		parent :: libcardstudentattend2();

		$this->thickBoxWidth = 750;
		$this->thickBoxHeight = 450;
	}

	function Include_JS_CSS() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$x = '
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/' . $LAYOUT_SKIN . '/js/lesson_attendance.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.js"></script>
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.css" type="text/css" />
		
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />
					';

		return $x;
	}

	function Get_Lesson_Daily_Status_Form($TargetDate = "") {
		global $Lang;

		$TargetDate = ($TargetDate != "") ? $TargetDate : date('Y-m-d');
		$linterface = new interface_html();
		$x = $this->Include_JS_CSS();

		$x .= '<form name="form1" method="post" action="">
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['General']['Date'] . '
										</td>
										<td class="tabletext" width="30%">
											' . $linterface->GET_DATE_PICKER("TargetDate", $TargetDate) . '
											<span style="color:red" id="TargetDateWarningLayer"></span>
										</td>
										<td class="tabletext" width="50%">
											<table border=0 width="100%">
												<tr>
													<td class="tabletext" width="5%" style="background-color:#660033;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['LessonStatusAllNotConfirmed'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#009900;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['LessonStatusAllConfirmed'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#CCCC00;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['LessonStatusPartialConfirmed'] . '
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
								    	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
								    </tr>
								    <tr>
									    <td align="center">
											' . $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "View_Lesson_Daily_Overview();") . '
										</td>
									</tr>
								</table>
								</form>';

		$x .= '<div id="DetailTableLayer" name="DetailTableLayer">';
		$x .= $this->Get_Lesson_Daily_Status_Overview($TargetDate);
		$x .= '</div>';

		return $x;
	}

	function Get_Lesson_Daily_Status_Overview($TargetDate) {
		global $Lang;

		include_once ('form_class_manage.php');

		$fcm = new form_class_manage();
		$YearClassList = $fcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
		$TimeSlotList = $this->Get_Time_Slot_List($TargetDate);
		$Temp = $this->Get_Class_Lesson_Attendance_Overview($TargetDate);
		//debug_pr($Temp);
		for ($i = 0; $i < sizeof($Temp); $i++) {
			$AttendDetail[$Temp[$i]['YearClassID']][$Temp[$i]['TimeSlotID']][] = $Temp[$i];
		}

		$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
		$x .= "<tr class=\"tabletop\">";
		$x .= "	<td class=\"tabletoplink\">" . $Lang['SysMgr']['FormClassMapping']['ClassTitle'] . "</td>";

		for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
			$x .= "<td class=\"tabletoplink\">" . $TimeSlotList[$i]['TimeSlotName'] . "</td>";
		}
		$x .= "</tr>\n";

		for ($i = 0; $i < sizeOf($YearClassList); $i++) {
			$ClassName = Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'], $YearClassList[$i]['ClassTitleEN']);

			$x .= '<tr class="tablerow2">';
			$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' . $ClassName . '</td>';
			for ($j = 0; $j < sizeof($TimeSlotList); $j++) {
				$SlotClassDetail = $AttendDetail[$YearClassList[$i]['YearClassID']][$TimeSlotList[$j]['TimeSlotID']];
				if (sizeof($SlotClassDetail) > 0) {
					$SlotConfirmed = 0;
					$SlotNotConfirmed = 0;
					$SlotDropDown = '';
					for ($k = 0; $k < sizeof($SlotClassDetail); $k++) {
						$SlotDropDown .= '<a href="#" onclick="Call_Lesson_Attendance_Flash(' . $SlotClassDetail[$k]['SubjectGroupID'] . ',\'' . Get_Lang_Selection($SlotClassDetail[$k]['SubjectGroupTitleB5'], $SlotClassDetail[$k]['SubjectGroupTitleEN']) . '\',' . $SlotClassDetail[$k]['RoomAllocationID'] . ',\'' . $TimeSlotList[$j]['StartTime'] . '\',\'' . $TimeSlotList[$j]['EndTime'] . '\',' . $SlotClassDetail[$k]['AttendOverviewID'] . ');">';
						$SlotDropDown .= Get_Lang_Selection($SlotClassDetail[$k]['SubjectNameB5'], $SlotClassDetail[$k]['SubjectNameEN']);
						$SlotDropDown .= '-';
						$SlotDropDown .= Get_Lang_Selection($SlotClassDetail[$k]['SubjectGroupTitleB5'], $SlotClassDetail[$k]['SubjectGroupTitleEN']);
						$SlotDropDown .= '</a>';

						if ($SlotClassDetail[$k]['AttendOverviewID'] < 0) {
							$SlotNotConfirmed++;
							$SlotDropDown .= '(' . $Lang['LessonAttendance']['NotConfirmed'] . ')';
						} else {
							$SlotConfirmed++;
							$SlotDropDown .= '(' . $Lang['LessonAttendance']['Confirmed'] . ')';
						}
						$SlotDropDown .= '<br>';
					}

					$BackgroundColor = "#660033"; // default all not confirmed, red
					if ($SlotConfirmed == sizeof($SlotClassDetail)) {
						$BackgroundColor = "#009900"; // all confirmed, green
					} else
						if ($SlotConfirmed > 0) {
							$BackgroundColor = "#CCCC00"; // some confirmed, yellow
						}
					$x .= '<td style="background-color:' . $BackgroundColor . '; border-bottom: solid 1px white; border-right: solid 1px white;">';
					$x .= '<!-- Draft Detail layer-->
															<span class="member_list_group_layer" id="SlotDropDownLayer' . $YearClassList[$i]['YearClassID'] . $TimeSlotList[$j]['TimeSlotID'] . '" style="display:none; visibility: visible; width:280px;">
																<h1>
																	<span style="float:left;">' . $ClassName . '-' . $TimeSlotList[$j]['TimeSlotName'] . '</span>
																	<span class="close_group_list" style="float:right;">
											      	 		<a href="#" title="' . $Lang['SysMgr']['SubjectClassMapping']['CloseFilter'] . '" onclick="$(\'span#SlotDropDownLayer' . $YearClassList[$i]['YearClassID'] . $TimeSlotList[$j]['TimeSlotID'] . '\').slideUp(); return false;">[' . $Lang['Btn']['Close'] . ']</a>
											      	 		</span>
											         		<p class="spacer"></p>
											         	</h1>
											          <div class="select_group" align="left">
													';
					$x .= $SlotDropDown;
					$x .= '
										           	</div>
											       	</span>
											       <!--Draft Detail layer end -->
											       
											       <div class="member_list_group" style="cursor: pointer; width:100%;" onclick="Show_Time_Slot_Layer(\'SlotDropDownLayer' . $YearClassList[$i]['YearClassID'] . $TimeSlotList[$j]['TimeSlotID'] . '\',event); return false;">
											       	<span class="group_list_name">
											       		&nbsp;
											       	</span>
											       </div>
											       ';
					$x .= '</td>';
				} else {
					$x .= '<td style="border-bottom: solid 1px white; border-right: solid 1px white;">&nbsp;</td>';
				}

			}
			$x .= '</tr>';
		}
		$x .= "</table>\n";

		return $x;
	}

	function Get_Lesson_Daily_Status_Form2($TargetDate = "") {
		global $Lang, $image_path, $LAYOUT_SKIN, $button_print;

		$TargetDate = ($TargetDate != "") ? $TargetDate : date('Y-m-d');
		$linterface = new interface_html();
		$x = $this->Include_JS_CSS();

		$x .= '<form name="form1" method="post" action="">
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['General']['Date'] . '
										</td>
										<td class="tabletext" width="80%">
											' . $linterface->GET_DATE_PICKER("TargetDate", $TargetDate) . '
											<span style="color:red" id="TargetDateWarningLayer"></span>
										</td>
										<!--<td class="tabletext" width="50%">
											<table border=0 width="100%">
												<tr>
													<td class="tabletext" width="5%" style="background-color:#660033;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['LessonStatusAllNotConfirmed'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#009900;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['LessonStatusAllConfirmed'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#CCCC00;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['LessonStatusPartialConfirmed'] . '
													</td>
												</tr>
											</table>
										</td>-->
									</tr>
											
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['LessonAttendance']['Status'] . '
										</td>
										<td class="tabletext" width="30%">
											'.$linterface->Get_Checkbox("chk_UnConfirmed", "AttendStatus[]", 2, 1, '', '<span style="color:#660033">'.$Lang['StudentAttendance']['UnConfirmed'].'</span>').'
											'.$linterface->Get_Checkbox("chk_Confirmed", "AttendStatus[]", 3, 1, '', '<span style="color:#009900">'.$Lang['StudentAttendance']['Confirmed'].'</span>').'		
											<!--<input type="checkbox" name="AttendStatus[]" id="chk_UnConfirmed" value="2" checked>
											 		
											<span style="color:#660033">' . $Lang['StudentAttendance']['UnConfirmed'] . '</span></input>
											<input type="checkbox" name="AttendStatus[]" id="chk_Confirmed" value="3" checked>
											
											<span style="color:#009900">' . $Lang['StudentAttendance']['Confirmed'] . '</span></input>-->
										</td>
										
								</tr>
					
							</table>
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
								    	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
								    </tr>
								    <tr>
									    <td align="center">
											' . $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "View_Lesson_Daily_Overview();") . '
										</td>
									</tr>	
								</table>
								</form>';

		$x .= '<div id="DetailTableLayer" name="DetailTableLayer">';
		$x .= $this->Get_Lesson_Daily_Status_Overview2($TargetDate);
		$x .= '</div>';

		return $x;
	}

	function Get_Lesson_Daily_Status_Overview2($TargetDate, $Filter = 0, $Format = "web") {
		global $Lang, $PATH_WRT_ROOT, $i_StudentAttendance_Field_ConfirmedBy, $i_StudentAttendance_Field_LastConfirmedTime, $button_print, $module_version;
		$linterface = new interface_html();
		//include_once ('form_class_manage.php');
		include_once ('libuser.php');
		
		//if($module_version['StudentAttendance'] == 3.0 || strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/StudentMgmt/eAttendance/')!==false){
		if($this->isStudentAttendance30()){	
			$StudentAttendance30 = true;
		}
		
		//$fcm = new form_class_manage();
		//$YearClassList = $fcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID());
		//$TimeSlotList = $this->Get_Time_Slot_List($TargetDate);
		$Temp = $this->Get_Class_Lesson_Attendance_Overview2($TargetDate, $Filter);
		//debug_pr($Temp);
//		for ($i = 0; $i < sizeof($Temp); $i++) {
//			$AttendDetail[$Temp[$i]['YearClassID']][$Temp[$i]['TimeSlotID']][] = $Temp[$i];
//		}

		//		if($Format=="print"){
		//		$x.='<table width="95%">
		//					<tr width="100%">
		//						<td width="100%" align="right">'.$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2").'</td>
		//					</tr>
		//				   </table>';
		//		}
		if ($Format == "print") {
			$x .= '<table width="96%" align="center" class="print_hide" border=0>
								<tr>
									<td align="right">' . $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submitPrint") . '</td>
								</tr>
							   </table>' . "\n";
		} else {
			$x .= '<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
								<td width="100%">		
									<div class="content_top_tool">
							        	<div class="Conntent_tool">
										' . $linterface->GET_LNK_PRINT("javascript:PrintPage('$TargetDate');", "", "", "", "", 1);
			if (get_client_region() != 'zh_TW') {
				$x .= $linterface->GET_LNK_IMPORT($PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/" . ($StudentAttendance30 ? "eAttendance/management/lesson_attendance/import_lesson_attendance/import.php" : "attendance/dailyoperation/import_lesson_attendance/import.php"), "", "", "", "", 1);
			}
			$x .= '</div>
									</div></td></tr></table>';
		}

		if ($Format == 'print') {
			$x .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
			$x .= '<tr>' . "\n";
			$x .= '<td>' . $Lang['LessonAttendance']['DailyLessonStatus'] . '</td>' . "\n";
			$x .= '</tr>' . "\n";
			$x .= '</table>' . "\n";

			$x .= '<table class="eSporttableborder" width="96%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
			$x .= '<tr>' . "\n";
			$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['SysMgr']['FormClassMapping']['ClassTitle'] . '</td>' . "\n";
			$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'] . '</td>' . "\n";
			$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period'] . '</td>' . "\n";
			$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['RepairSystem']['Location'] . '</td>' . "\n";
			$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['General']['Status2'] . '</td>' . "\n";
			$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $i_StudentAttendance_Field_ConfirmedBy . '</td>' . "\n";
			$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $i_StudentAttendance_Field_LastConfirmedTime . '</td>' . "\n";
			$x .= '</tr>' . "\n";

			if (sizeOf($Temp) <= 0) {
				$x .= '<tr>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttext" colspan="7" align="center">' . $Lang['General']['NoRecordFound'] . '</td>' . "\n";
				$x .= '</tr>' . "\n";
			} else {
				for ($i = 0; $i < sizeOf($Temp); $i++) {
					//$css_num = ($i % 2) == 0? '2': '';
					$x .= '<tr>';
					$x .= '<td class="eSporttdborder eSportprinttext">' . Get_Lang_Selection($Temp[$i][2], $Temp[$i][3]) . '</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">' .

					Get_Lang_Selection($Temp[$i][14], $Temp[$i][15]) . ' - ' . Get_Lang_Selection($Temp[$i][4], $Temp[$i][5]) . '</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">' . $Temp[$i][6] . '</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">' . Get_Lang_Selection($Temp[$i][7], $Temp[$i][8]) . '</td>';
					if ($Temp[$i][9] == -1) {
						//$BackgroundColor = "#660033";
						$status = $Lang['StudentAttendance']['UnConfirmed'];
					} else {
						//$BackgroundColor = "#009900";//'<span style="background-color:#009900;">&nbsp;&nbsp;&nbsp;&nbsp;</span> '.
						$status = $Lang['StudentAttendance']['Confirmed'];
					}
					$x .= '<td class="eSporttdborder eSportprinttext"><b>' . $status . '</b></td>';
					if ($Temp[$i][10] == NULL) {
						$confirmBy = $Lang['General']['EmptySymbol'];
					} else {
						
						//$lu = new libuser($Temp[$i][10]);
						//$confirmBy = Get_Lang_Selection($lu->ChineseName, $lu->EnglishName);
						$confirmBy =  Get_Lang_Selection($Temp[$i]['LastModifiedUserChineseName'], $Temp[$i]['LastModifiedUserEnglishName']);
					}
					$x .= '<td class="eSporttdborder eSportprinttext">' . $confirmBy . '</td>';
					if ($Temp[$i][10] == NULL)
						$modifiedDate = $Lang['General']['EmptySymbol'];
					else
						$modifiedDate = $Temp[$i][11];
					$x .= '<td class="eSporttdborder eSportprinttext">' . $modifiedDate . '</td>';
					$x .= '</tr>';

					//			if($i< sizeOf($Temp)){
					//				if($Temp[$i][2] != $Temp[$i+1][2]){
					//					if($css == 2)
					//						$css = '';
					//					else
					//						$css = 2;
					//				}
					//			}
				}
			}
		} else {
			$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
			$x .= "<tr class=\"tabletop\">";
			$x .= "	<td>" . $Lang['SysMgr']['FormClassMapping']['ClassTitle'] . "</td>";

			$x .= "<td>" . $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'] . "</td>";
			$x .= "<td>" . $Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period'] . "</td>";
			$x .= "<td>" . $Lang['RepairSystem']['Location'] . "</td>";
			$x .= "<td>" . $Lang['General']['Status2'] . "</td>";
			$x .= "<td>" . $i_StudentAttendance_Field_ConfirmedBy . "</td>";
			$x .= "<td>" . $i_StudentAttendance_Field_LastConfirmedTime . "</td>";

			//		for ($i=0; $i< sizeof($TimeSlotList); $i++) {
			//			$x .= "<td class=\"tabletoplink\">".$TimeSlotList[$i]['TimeSlotName']."</td>";
			//		}
			$x .= "</tr>\n";

			//$css = '';
			if (sizeOf($Temp) <= 0) {
				$x .= '<tr>' . "\n";
				$x .= '<td colspan="7" align="center" class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' . $Lang['General']['NoRecordFound'] . '</td>' . "\n";
				$x .= '</tr>' . "\n";
			} else {
				for ($i = 0; $i < sizeOf($Temp); $i++) {
					$css_num = ($i % 2) == 0 ? '2' : '';
					$x .= '<tr class="tablerow' . $css_num . '">';
					$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' . Get_Lang_Selection($Temp[$i][2], $Temp[$i][3]) . '</td>';
					$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' .
					'<a href="#" onclick="Call_Lesson_Attendance_Flash(' . $Temp[$i][1] . ',\'' . Get_Lang_Selection($Temp[$i][2], $Temp[$i][3]) . '\',' . $Temp[$i][12] . ',\'' . $Temp[$i][16] . '\',\'' . $Temp[$i][17] . '\',' . $Temp[$i][9] . ');">' .
					Get_Lang_Selection($Temp[$i][14], $Temp[$i][15]) . ' - ' . Get_Lang_Selection($Temp[$i][4], $Temp[$i][5]) . '</a></td>';
					$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' . $Temp[$i][6] . '</td>';
					$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' . Get_Lang_Selection($Temp[$i][7], $Temp[$i][8]) . '</td>';
					if ($Temp[$i][9] == -1) {
						$BackgroundColor = "#660033";
						$status = $Lang['StudentAttendance']['UnConfirmed'];
					} else {
						$BackgroundColor = "#009900"; //'<span style="background-color:#009900;">&nbsp;&nbsp;&nbsp;&nbsp;</span> '.
						$status = $Lang['StudentAttendance']['Confirmed'];
					}
					$x .= '<td style="color:' . $BackgroundColor . ';" class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;"><b>' . $status . '</b></td>';
					if ($Temp[$i][10] == NULL) {
						$confirmBy = $Lang['General']['EmptySymbol'];
					} else {
						
						//$lu = new libuser($Temp[$i][10]);
						//$confirmBy = Get_Lang_Selection($lu->ChineseName, $lu->EnglishName);
						$confirmBy =  Get_Lang_Selection($Temp[$i]['LastModifiedUserChineseName'], $Temp[$i]['LastModifiedUserEnglishName']);
					}
					$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' . $confirmBy . '</td>';
					if ($Temp[$i][10] == NULL)
						$modifiedDate = $Lang['General']['EmptySymbol'];
					else
						$modifiedDate = $Temp[$i][11];
					$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' . $modifiedDate . '</td>';
					$x .= '</tr>';

					//			if($i< sizeOf($Temp)){
					//				if($Temp[$i][2] != $Temp[$i+1][2]){
					//					if($css == 2)
					//						$css = '';
					//					else
					//						$css = 2;
					//				}
					//			}
				}
			}
		}
		//		for($i=0; $i< sizeOf($YearClassList); $i++)	{
		//			$ClassName = Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']);
		//			
		//			$x .= '<tr class="tablerow2">';
		//			$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">'.$ClassName.'</td>';
		//			for ($j=0; $j< sizeof($TimeSlotList); $j++) {
		//				$SlotClassDetail = $AttendDetail[$YearClassList[$i]['YearClassID']][$TimeSlotList[$j]['TimeSlotID']];
		//				if (sizeof($SlotClassDetail) > 0) {
		//					$SlotConfirmed = 0;
		//					$SlotNotConfirmed = 0;
		//					$SlotDropDown = '';
		//					for ($k=0; $k < sizeof($SlotClassDetail); $k++) {
		//						$SlotDropDown .= '<a href="#" onclick="Call_Lesson_Attendance_Flash('.$SlotClassDetail[$k]['SubjectGroupID'].',\''.Get_Lang_Selection($SlotClassDetail[$k]['SubjectGroupTitleB5'],$SlotClassDetail[$k]['SubjectGroupTitleEN']).'\','.$SlotClassDetail[$k]['RoomAllocationID'].',\''.$TimeSlotList[$j]['StartTime'].'\',\''.$TimeSlotList[$j]['EndTime'].'\','.$SlotClassDetail[$k]['AttendOverviewID'].');">';
		//						$SlotDropDown .= Get_Lang_Selection($SlotClassDetail[$k]['SubjectNameB5'],$SlotClassDetail[$k]['SubjectNameEN']);
		//						$SlotDropDown .= '-';
		//						$SlotDropDown .= Get_Lang_Selection($SlotClassDetail[$k]['SubjectGroupTitleB5'],$SlotClassDetail[$k]['SubjectGroupTitleEN']);
		//						$SlotDropDown .= '</a>';
		//						
		//						if ($SlotClassDetail[$k]['AttendOverviewID'] < 0) {
		//							$SlotNotConfirmed++;
		//							$SlotDropDown .= '('.$Lang['LessonAttendance']['NotConfirmed'].')';
		//						}
		//						else {
		//							$SlotConfirmed++;
		//							$SlotDropDown .= '('.$Lang['LessonAttendance']['Confirmed'].')';
		//						}
		//						$SlotDropDown .= '<br>';
		//					}
		//					
		//					$BackgroundColor = "#660033"; // default all not confirmed, red
		//					if ($SlotConfirmed == sizeof($SlotClassDetail)) {
		//						$BackgroundColor = "#009900"; // all confirmed, green
		//					}
		//					else if ($SlotConfirmed > 0) {
		//						$BackgroundColor = "#CCCC00"; // some confirmed, yellow
		//					}
		//					$x .= '<td style="background-color:'.$BackgroundColor.'; border-bottom: solid 1px white; border-right: solid 1px white;">';
		//					$x .= '<!-- Draft Detail layer-->
		//										<span class="member_list_group_layer" id="SlotDropDownLayer'.$YearClassList[$i]['YearClassID'].$TimeSlotList[$j]['TimeSlotID'].'" style="display:none; visibility: visible; width:280px;">
		//											<h1>
		//												<span style="float:left;">'.$ClassName.'-'.$TimeSlotList[$j]['TimeSlotName'].'</span>
		//												<span class="close_group_list" style="float:right;">
		//						      	 		<a href="#" title="'.$Lang['SysMgr']['SubjectClassMapping']['CloseFilter'].'" onclick="$(\'span#SlotDropDownLayer'.$YearClassList[$i]['YearClassID'].$TimeSlotList[$j]['TimeSlotID'].'\').slideUp(); return false;">['.$Lang['Btn']['Close'].']</a>
		//						      	 		</span>
		//						         		<p class="spacer"></p>
		//						         	</h1>
		//						          <div class="select_group" align="left">
		//								';
		//					$x .= $SlotDropDown;
		//					$x .= '
		//					           	</div>
		//						       	</span>
		//						       <!--Draft Detail layer end -->
		//						       
		//						       <div class="member_list_group" style="cursor: pointer; width:100%;" onclick="Show_Time_Slot_Layer(\'SlotDropDownLayer'.$YearClassList[$i]['YearClassID'].$TimeSlotList[$j]['TimeSlotID'].'\',event); return false;">
		//						       	<span class="group_list_name">
		//						       		&nbsp;
		//						       	</span>
		//						       </div>
		//						       ';
		//					$x .= '</td>';
		//				}
		//				else {
		//					$x .= '<td style="border-bottom: solid 1px white; border-right: solid 1px white;">&nbsp;</td>';
		//				}
		//				
		//			}
		//	    $x .= '</tr>';
		//		}
		$x .= "</table>\n";

		return $x;
	}

	function Get_Class_Lesson_Daily_Report_Form() {
		global $Lang, $image_path, $LAYOUT_SKIN, $sys_custom;

		$TargetDate = ($TargetDate != "") ? $TargetDate : date('Y-m-d');
		$TargetEndDate = $TargetDate;
		$linterface = new interface_html();
		$x = $this->Include_JS_CSS();
		
		if($sys_custom['LessonAttendance_CustomReportStatusSymbol']) {
			$symbol_legend = '<table border=0 width="100%">
								<tr>
									<td class="tabletext" width="20%">
										'.$sys_custom['LessonAttendance_CustomReportStatusSymbolArr'][2].' : '. $Lang['LessonAttendance']['Late'] . '
									</td>
									<td class="tabletext" width="20%">
										'.$sys_custom['LessonAttendance_CustomReportStatusSymbolArr'][3].' : '.$Lang['LessonAttendance']['Absent'] . '
									</td>
									<td class="tabletext" width="20%">
										'.$sys_custom['LessonAttendance_CustomReportStatusSymbolArr'][1].' : '.$Lang['LessonAttendance']['Outing'] . '
									</td>
									<td class="tabletext" width="40%">&nbsp;</td>
								</tr>
							  </table>';
		}
		
		$x .= '<form name="form1" method="post" action="">
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['General']['StartDate'] . '
										</td>
										<td class="tabletext" width="30%">
											' . $linterface->GET_DATE_PICKER("TargetDate", $TargetDate,' onchange="startDateChanged();" ',"yy-mm-dd","","","","startDateChanged();") . '
											<span style="color:red" id="TargetDateWarningLayer"></span>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['General']['EndDate'] . '
										</td>
										<td class="tabletext" width="30%">
											' . $linterface->GET_DATE_PICKER("TargetEndDate", $TargetEndDate,' onchange="endDateChanged();" ',"yy-mm-dd","","","","endDateChanged();") . '
											<span style="color:red" id="TargetEndDateWarningLayer"></span>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['Btn']['View'] . '
										</td>
										<td class="tabletext" width="30%">';
							$x.=$linterface->Get_Checkbox("AttendStatusLate", "AttendStatus[]", "2", 1, '', $Lang['LessonAttendance']['Late'], '', '');
							$x.=$linterface->Get_Checkbox("AttendStatusAbsent", "AttendStatus[]", "3", 1, '', $Lang['LessonAttendance']['Absent'], '', '');
							$x.=$linterface->Get_Checkbox("AttendStatusOuting", "AttendStatus[]", "1", 1, '', $Lang['LessonAttendance']['Outing'], '', '');
							/*		
											'<input type="checkbox" name="AttendStatus[]" value="2" checked>
											' . $Lang['LessonAttendance']['Late'] . '
											<input type="checkbox" name="AttendStatus[]" value="3" checked>
											' . $Lang['LessonAttendance']['Absent'] . */
								$x .=  '</td>
										<td class="tabletext" width="50%">
											<table border=0 width="100%">
												<tr>';
											$x .= '<td class="tabletext" width="5%" style="background-color:#FFCCCC;">
														&nbsp;
													</td>
													<td class="tabletext" width="10%">
														' . $Lang['LessonAttendance']['Late'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#CC0000;">
														&nbsp;
													</td>
													<td class="tabletext" width="10%">
														' . $Lang['LessonAttendance']['Absent'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#6699FF;">
														&nbsp;
													</td>
													<td class="tabletext" width="10%">
														' . $Lang['LessonAttendance']['Outing'] . '
													</td>
													<td class="tabletext" width="55%">
														&nbsp;
													</td>';
									/*			
											$x .= '<td class="tabletext" width="5%" style="background-color:#009900;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['Present'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#6699FF;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['Outing'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#FFCCCC;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['Late'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#CC0000;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['Absent'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#CCCC66;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['NotConfirmed'] . '
													</td>';
										*/		
											$x .= '</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td><span class="tabletextremark">('.$Lang['LessonAttendance']['LessonDailyReportCSVReminder'].')</span></td>
										<td>'.($sys_custom['LessonAttendance_CustomReportStatusSymbol']?$symbol_legend:'&nbsp;').'</td>
									</tr>
								</table>
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
								    	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
								    </tr>
								    <tr>
									    <td align="center">
											' . $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "View_Lesson_Daily_Overview();", "submitButton") . '
										</td>
									</tr>
								</table>
								</form>';

		$x .= '<div id="DetailTableLayer" name="DetailTableLayer">';
		$x .= $this->Class_Lesson_Daily_Report();
		$x .= '</div>';

		return $x;
	}

	//Henry Adding
	function Get_Class_Lesson_Daily_Report_Form_Student($TargetUserID = "") {
		global $Lang, $image_path, $LAYOUT_SKIN;

		$FromDate = date('Y-m-d');
		$ToDate = date('Y-m-d');

		$TargetDate = ($TargetDate != "") ? $TargetDate : date('Y-m-d');
		$linterface = new interface_html();
		$x = $this->Include_JS_CSS();

		$x .= '<form name="form1" method="post" action="">
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">							
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="20%">
											' . $Lang['LessonAttendance']['FromDate'] . '
										</td>
										<td class="tabletext" width="40%">
											' . $linterface->GET_DATE_PICKER("FromDate", $FromDate, "", "yy-mm-dd", "", "", "", "$('#ToDateWarningLayer').html('" . $Lang['General']['JS_warning']['InvalidDateRange'] . "').hide();") . '
											<span style="color:red" id="FromDateWarningLayer"></span>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="20%">
											' . $Lang['LessonAttendance']['ToDate'] . '
										</td>
										<td class="tabletext" width="40%">
											' . $linterface->GET_DATE_PICKER("ToDate", $ToDate, "", "yy-mm-dd", "", "", "", "$('#ToDateWarningLayer').html('" . $Lang['General']['JS_warning']['InvalidDateRange'] . "').hide();") . '
											<span style="color:red" id="ToDateWarningLayer"></span>
										</td>
									</tr>											
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="20%">
											' . $Lang['Btn']['View'] . '
										</td>
										<td class="tabletext" width="40%">' .
		$linterface->Get_Checkbox("AttendStatus4", "AttendStatus[]", 0, 1, '', $Lang['LessonAttendance']['Present']) .
		$linterface->Get_Checkbox("AttendStatus1", "AttendStatus[]", 1, 1, '', $Lang['LessonAttendance']['Outing']) .
		$linterface->Get_Checkbox("AttendStatus2", "AttendStatus[]", 2, 1, '', $Lang['LessonAttendance']['Late']) .
		$linterface->Get_Checkbox("AttendStatus3", "AttendStatus[]", 3, 1, '', $Lang['LessonAttendance']['Absent']) .
		$linterface->Get_Checkbox("AttendStatus5", "AttendStatus[]", -1, 1, '', $Lang['LessonAttendance']['NotConfirmed']) .
		'		
										</td>
										<td class="tabletext" width="40%">
											<table border=0 width="100%">
												<tr>
													<td class="tabletext" width="5%" style="background-color:#009900;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['Present'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#6699FF;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['Outing'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#FFCCCC;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['Late'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#CC0000;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['Absent'] . '
													</td>
													<td class="tabletext" width="5%" style="background-color:#CCCC66;">
														&nbsp;
													</td>
													<td class="tabletext">
														' . $Lang['LessonAttendance']['NotConfirmed'] . '
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
								    	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
								    </tr>
								    <tr>
									    <td align="center">
											' . $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "View_Lesson_Daily_Overview();") . '
										</td>
									</tr>
								</table>
								</form>';

		$x .= '<div id="DetailTableLayer" name="DetailTableLayer">';
		$x .= $this->Class_Lesson_Daily_Report_Student("", "", array (
			1,
			2,
			3,
			-1,
			0
		), $TargetUserID);
		$x .= '</div>';

		return $x;
	}

	function Get_Lesson_Attendance_Panel_Form($RecordID, $RecordType = "SubjectGroup", $LessonDate = "", $AttendOverviewID = -1, $Format = "web", $isSmartCard = 0) {
		global $intranet_root, $Lang, $button_save, $button_reset, $button_print, $image_path, $LAYOUT_SKIN, $sys_custom, $i_SmartCard_Frontend_Take_Attendance_In_School_Time, $DisplayAllPhoto, $HiddenAllPhoto;
		$linterface = new interface_html();
		//		global $PATH_WRT_ROOT;
		//		include_once($PATH_WRT_ROOT."includes/flashservices_ver_1_2/services/liblessonattendance.php");
		//		debug_pr("testing!");
		//		$lla = new liblessonattendance();
		//		debug_pr("after testing!");
		$ts_loaded = time();
		
		$statusMapping = $this->LessonAttendanceStatusMapping();
		
		if ($AttendOverviewID < 0) {
			$StudentList = $this->Get_Student_List($RecordID, $RecordType, $LessonDate);
		} else {
			$StudentList = $this->Get_Lesson_Attendance_Record($AttendOverviewID);
			/*if(get_client_region() == 'zh_TW') {
				$StudentList_temp = array();
				$all_StudentList = $this->Get_Student_List($RecordID, $RecordType, $LessonDate);
				$temp_list = BuildMultiKeyAssoc($StudentList,0, array(),0,1);
				foreach($all_StudentList as $temp) {
					if(isset($temp_list[$temp[0]])) {
						$StudentList_temp[] = $temp_list[$temp[0]][0];
						continue;
					}
					$StudentList_temp[] = $temp;
				}
				$StudentList = $StudentList_temp;
			}*/
		}
		$StudentListCount = count($StudentList);
		//debug_pr($StudentList);
		if($sys_custom['LessonAttendance_DisplayTime']){
			$date_ts = strtotime($LessonDate);
			$year = date("Y", $date_ts);
			$month = date("m", $date_ts);
			$day = date("j", $date_ts);
			$dailylog_table = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
			$student_id_ary = Get_Array_By_Key($StudentList,0);
			$sql = "SELECT UserID,InSchoolTime FROM $dailylog_table WHERE DayNumber='$day' AND UserID IN (".implode(",",$student_id_ary).") AND InSchoolTime IS NOT NULL";
			$records = $this->returnResultSet($sql);
			$studentIdToTime = array();
			for($i=0;$i<count($records);$i++){
				$studentIdToTime[$records[$i]['UserID']] = $records[$i]['InSchoolTime'];
			}
		}
		

		//if($AttendOverviewID < 0){
		$x = '';
		if ($Format == "print") {
			$x .= '<table width="96%" align="center" class="print_hide" border=0>
									<tr>
										<td align="right">' . $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submitPrint") . '</td>
									</tr>
								   </table>' . "\n";
		}
		//The information of Take Attendance [Start]
		if($AttendOverviewID == -1)
		$filter = 1;
		else
		$filter = 2;
		$Temp = $this->Get_Class_Lesson_Attendance_Overview2($LessonDate, $filter, $RecordID);
		$j = 0;
		//debug_pr(sizeof($Temp));
		for($i=0; $i<sizeof($Temp);$i++){
			if($Temp[$i]['SubjectGroupID'] == $RecordID && $Temp[$i]['StartTime'] == $_REQUEST["StartTime"] && $Temp[$i]['EndTime'] == $_REQUEST["EndTime"]){
				$j = $i;
				break;
			}
		}


		$studentIdToLeaveRecord = array();
		if(get_client_region() == 'zh_TW') {
			$student_id_ary = Get_Array_By_Key($StudentList,0);
			if($Temp[$j]['SessionType'] == 'am') {
				$DayPeriod = '2';
			} else {
				$DayPeriod = '3';
			}

			$sql = "SELECT StudentID,Reason,LeaveTypeID,AttendanceType,SessionFrom,SessionTo
 					FROM CARD_STUDENT_PRESET_LEAVE
 					WHERE RecordDate='$LessonDate'
 					AND DayPeriod='$DayPeriod'
					AND StudentID IN (".implode(",",$student_id_ary).") 
					ORDER BY DateInput DESC
 					";
			$records = $this->returnResultSet($sql);
			for($i=0;$i<count($records);$i++){
				$match = false;
				if($records[$i]['AttendanceType'] == 0) {
					$match = true;
				} elseif($records[$i]['AttendanceType'] == 1) {
					if($Temp[$j]['SessionType'] == 'am') {
						$match = true;
					}
				} elseif($records[$i]['AttendanceType'] == 2) {
					if($Temp[$j]['SessionType'] == 'pm') {
						$match = true;
					}
				} elseif($records[$i]['AttendanceType'] == 3) {
					$current_session = $Temp[$j]['DisplayOrder'];
					if($current_session >= $records[$i]['SessionFrom'] && $current_session <= $records[$i]['SessionTo']) {
						$match = true;
					}
				}

				if($match) {
					if(!isset($studentIdToLeaveRecord[$records[$i]['StudentID']])) {
						$studentIdToLeaveRecord[$records[$i]['StudentID']] = array("type" => "preset", "reason" => $records[$i]['Reason'], "leavetype" => $records[$i]['LeaveTypeID']);
					}
				}
			}

			$sql = "SELECT * 
					FROM CARD_STUDENT_APPLY_LEAVE_RECORD
					WHERE StartDate='$LessonDate'
					AND StudentID IN (".implode(",",$student_id_ary).") 
					AND IsDeleted=0 AND ApprovalStatus=0
					ORDER BY InputDate DESC
					";

			$records = $this->returnResultSet($sql);
			for($i=0;$i<count($records);$i++){
				$match = false;
				if($records[$i]['AttendanceType'] == 0) {
					$match = true;
				} elseif($records[$i]['AttendanceType'] == 1) {
					if($Temp[$j]['SessionType'] == 'am') {
						$match = true;
					}
				} elseif($records[$i]['AttendanceType'] == 2) {
					if($Temp[$j]['SessionType'] == 'pm') {
						$match = true;
					}
				} elseif($records[$i]['AttendanceType'] == 3) {
					$current_session = $Temp[$j]['DisplayOrder'];
					if($current_session >= $records[$i]['SessionFrom'] && $current_session <= $records[$i]['SessionTo']) {
						$match = true;
					}
				}

				if($match) {
					if(!isset($studentIdToLeaveRecord[$records[$i]['StudentID']])) {
						$studentIdToLeaveRecord[$records[$i]['StudentID']] = array("type" => "apply", "reason" => $records[$i]['Reason'], "leavetype" => $records[$i]['LeaveTypeID']);
					}
				}
			}

			//var_dump($studentIdToLeaveRecord);
		}

		//debug_pr($Temp);
		$x .= "<br/><table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
		$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">". $Lang['General']['Date']."</td>";
		$x .= "<td class=\"tabletext\" width=\"70%\">$LessonDate</td></tr>\n";
		
		$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">". $Lang['SysMgr']['FormClassMapping']['ClassTitle']."</td>";
		$x .= "<td class=\"tabletext\" width=\"70%\">".Get_Lang_Selection($Temp[$j]['ClassTitleB5'], $Temp[$j]['ClassTitleEN'])."</td></tr>\n";
		
		$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">". $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'] ."</td>";
		$x .= "<td class=\"tabletext\" width=\"70%\">".Get_Lang_Selection($Temp[$j][14], $Temp[$j][15]) . ' - ' . Get_Lang_Selection($Temp[$j]['SubjectGroupTitleB5'], $Temp[$j]['SubjectGroupTitleEN'])."</td></tr>\n";
		
		$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">". $Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period'] ."</td>";
		$x .= "<td class=\"tabletext\" width=\"70%\">".$Temp[$j]['TimeSlotName']."</td></tr>\n";
		
		$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">". $Lang['RepairSystem']['Location'] ."</td>";
		$x .= "<td class=\"tabletext\" width=\"70%\">".Get_Lang_Selection($Temp[$j]['LocationNameB5'], $Temp[$j]['LocationNameEN'])."</td></tr>\n";
		
		$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">". $Lang['General']['Status2'] ."</td>";
		if ($Temp[$j]['AttendOverviewID'] == -1) {
						$BackgroundColor = "#660033";
						$status = $Lang['StudentAttendance']['UnConfirmed'];
					} else {
						$BackgroundColor = "#009900"; //'<span style="background-color:#009900;">&nbsp;&nbsp;&nbsp;&nbsp;</span> '.
						$status = $Lang['StudentAttendance']['Confirmed'];
					}
		$x .= "<td style=\"color:" . $BackgroundColor . ";\" class=\"tabletext\" width=\"70%\">".$status."</td></tr>\n";
		
		$x .= "</table>\n<br/>";
		//The information of Take Attendance [End]
		$x .= '<form name="form3" method="post" action="">';
		if ($Format == "web") {
			$x .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
			//			$x .= '<tr>
			//								<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
			//									'.$Lang['SysMgr']['FormClassMapping']['Class'].' '.getSelectByArray($ResultShortcut," name=\"pageShortCut\" onChange=\"changePage(this.value)\" ").$this->getSelectClassID('id="ClassID" name="ClassID"  onchange="View_Student_In_Class()"',"").'
			//								</td>			
			//							</tr>';

			$x .= "<tr>";
			//						$x .= "<td class=\"tabletext\">".$Lang['StaffAttendance']['SetupStatus']." <select id=\"all_drop_down_status\" name=\"all_drop_down_status[]\" onChange=\"Change_All_Status(this.value);this.className=this.options[this.selectedIndex].className\">\n";
			//				        $x .= "<option value=\"-1\" SELECTED >-- ".$Lang['Btn']['Select']." --</option>\n";
			//				        $x .= "<option value=\"0\" class=\"greenText\">".$Lang['StudentAttendance']['Present']."</option>\n";
			//				        $x .= "<option value=\"2\" class=\"orangeText\">".$Lang['StudentAttendance']['Late']."</option>\n";
			//				        $x .= "<option value=\"3\" class=\"redText\">".$Lang['StudentAttendance']['Absent']."</option>\n";
			//				        $x .= "<option value=\"1\" class=\"blueText\">".$Lang['StudentAttendance']['Outing']."</option>\n";
			//				        $x .= "</select></td>";
			$x .= "<td>";
			$x .= '<div class="content_top_tool">
						               		<div class="Conntent_tool">';
			$x .= $linterface->GET_LNK_PRINT("javascript:PrintPage($AttendOverviewID,'$LessonDate');", "", "", "", "", 1);
			$x .= '  </div>
										    <br style="clear: both;">
										  </div>';
			$x .= "</td>";
			$x .= "</tr>\n";
			$x .= '</table>';

			$temp = " <select id=\"all_drop_down_status\" name=\"all_drop_down_status[]\" onChange=\"this.className=this.options[this.selectedIndex].className\">";
				$temp .= "<option value=\"-1\" class=\"-1\" SELECTED >-- " . $Lang['Btn']['Select'] . " --</option>";
					/*
						$temp .= "<option value=\"0\" class=\"greenText\">" . $Lang['StudentAttendance']['Present'] . "</option>
							        <option value=\"2\" class=\"orangeText\">" . $Lang['StudentAttendance']['Late'] . "</option>
							        <option value=\"3\" class=\"redText\">" . $Lang['StudentAttendance']['Absent'] . "</option>
							        <option value=\"1\" class=\"blueText\">" . $Lang['StudentAttendance']['Outing'] . "</option>";
					*/
					foreach($statusMapping as $value)
					{
						$temp .= '<option value="'.$value['code'].'" class="'.$value['class'].'">'.$value['text'].'</option>';
					}
				$temp .= "</select>";
			$temp .= '<span class="table_row_tool" style="float:none;display:inline;"><a class="icon_batch_assign" style="float:none;display:inline-block;" href="javascript:Change_All_Status($(\'#all_drop_down_status option:selected\').val());"></a></span>';
		} else
			if ($Format == "print") {
//				$x .= '<table width="96%" align="center" class="print_hide" border=0>
//									<tr>
//										<td align="right">' . $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submitPrint") . '</td>
//									</tr>
//								   </table>' . "\n";

				$x .= '<table class="eSporttableborder" width="96%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
				$x .= '<tr>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['StudentAttendance']['Class'] . '</td>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['StudentAttendance']['ClassNumber'] . '</td>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['StudentAttendance']['StudentName'] . '</td>' . "\n";
				if($sys_custom['LessonAttendance_DisplayTime']){
					$x .= '<td class="eSporttdborder eSportprinttabletitle">'.$i_SmartCard_Frontend_Take_Attendance_In_School_Time.'</td>'."\n";
				}
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['StudentAttendance']['eNoticeTemplateStatus'] . ' ' . $temp . '</td>' . "\n";
				if(get_client_region() == 'zh_TW') {
					$x .= '<td class="eSporttdborder eSportprinttabletitle">' .$Lang['StudentAttendance']['LeaveType'] .'</td>'."\n";
				}
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['LessonAttendance']['Reason'].'</td>'."\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['StudentAttendance']['OutingRemark'] . '</td>' . "\n";
				$x .= '</tr>' . "\n";

				if ($StudentListCount > 0) {
					for ($i = 0; $i < $StudentListCount; $i++) {
						$css_num = ($i % 2) == 0 ? '2' : '';

						$x .= '<tr>';
						$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display(Get_Lang_Selection($StudentList[$i][9], $StudentList[$i][4])) . '</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($StudentList[$i][3]) . '</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display(Get_Lang_Selection($StudentList[$i][2], $StudentList[$i][1])) . '</td>';
						//$selections = array('','','','');
						$selections = "";
						/*
						if ($StudentList[$i][5] === '0') { // present
							//$x .= '<td class="tabletext" style="background-color:#009900; border-bottom: solid 1px white; border-right: solid 1px white;">'.$Lang['LessonAttendance']['Present'].'</td>';
							//$selections[0] = "SELECTED";
							//$class="greenText";
							$selections = $Lang['StudentAttendance']['Present'];
						} else
						if ($StudentList[$i][5] === '1') { // outing
							//$x .= '<td class="tabletext" style="background-color:#6699FF; border-bottom: solid 1px white; border-right: solid 1px white;">'.$Lang['LessonAttendance']['Outing'].'</td>';
							//$selections[1] = "SELECTED";
							//$class="blueText";
							$selections = $Lang['StudentAttendance']['Outing'];
						} else
						if ($StudentList[$i][5] === '2') { // late
							//$x .= '<td class="tabletext" style="background-color:#FFCCCC; border-bottom: solid 1px white; border-right: solid 1px white;">'.$Lang['LessonAttendance']['Late'].'</td>';
							//$selections[2] = "SELECTED";
							//$class="orangeText";
							$selections = $Lang['StudentAttendance']['Late'];
						} else
						if ($StudentList[$i][5] === '3') { // Absent
							//$x .= '<td class="tabletext" style="background-color:#CC0000; border-bottom: solid 1px white; border-right: solid 1px white;">'.$Lang['LessonAttendance']['Absent'].'</td>';
							//$selections[3] = "SELECTED";
							//$class="redText";
							$selections = $Lang['StudentAttendance']['Absent'];
						}
						*/
						foreach($statusMapping as $value)
						{
							if($StudentList[$i][5] == $value['code']){
								$selections = $value['text'];
							}
						}
						//						$x .= "<td class=\"eSporttdborder eSportprinttext\"><select class=\"drop_down_status $class\" id=\"drop_down_status[$i]\" name=\"drop_down_status[]\" onChange=\"this.className='drop_down_status '+this.options[this.selectedIndex].className\">\n";
						//				        $x .= "<option value=\"0\" ".$selections[0]." class=\"greenText\">".$Lang['StudentAttendance']['Present']."</option>\n";
						//				        $x .= "<option value=\"2\" ".$selections[2]." class=\"orangeText\">".$Lang['StudentAttendance']['Late']."</option>\n";
						//				        $x .= "<option value=\"3\" ".$selections[3]." class=\"redText\">".$Lang['StudentAttendance']['Absent']."</option>\n";
						//				        $x .= "<option value=\"1\" ".$selections[1]." class=\"blueText\">".$Lang['StudentAttendance']['Outing']."</option>\n";
						//				        $x .= "</select></td>\n";
						if($sys_custom['LessonAttendance_DisplayTime']){
							$x .= "<td class=\"eSporttdborder eSportprinttext\">".(Get_String_Display($studentIdToTime[$StudentList[$i][0]]))."</td>";
						}
						$x .= "<td class=\"eSporttdborder eSportprinttext\">";
						$x .= $selections;
						$x .= "</td>\n";
						//$x .= '<td class="tabletext">'.Get_String_Display($StudentListCount[$i][5]).'</td>';
						//$x .= '<td class="tabletext">'.Get_String_Display($StudentList[$i][6]).'</td>';
						if (Get_String_Display($StudentList[$i][6]) != "---")
							$remarks = Get_String_Display($StudentList[$i][6]);
						else
							$remarks = '---';

						if(get_client_region() == 'zh_TW') {
							$sql ="SELECT TypeID,TypeName FROM CARD_STUDENT_LEAVE_TYPE WHERE TypeID='".$StudentList[$i][13]."' ORDER BY TypeSymbol,TypeName";
							$temp = $this->returnArray($sql,2);

							$leave_status = '';
							if($AttendOverviewID < 0) {

							} else {
								if(isset($studentIdToLeaveRecord[$StudentList[$i][0]])) {
									if ($studentIdToLeaveRecord[$StudentList[$i][0]]['type'] == 'apply') {
										$leave_status = "<span class=\"tabletextrequire\">#</span>";
									} else {
										if ($StudentList[$i][12] != $studentIdToLeaveRecord[$StudentList[$i][0]]['reason'] || $StudentList[$i][13] != $studentIdToLeaveRecord[$StudentList[$i][0]]['leavetype']) {
											$leave_status = "<span class=\"tabletextrequire\">*</span>";
										}
									}
								}
							}

							$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $leave_status. $temp[0]['TypeName'] . "</td>";
						}
						//$x .= "<td class=\"eSporttdborder eSportprinttext\"><input type=\"text\" name=\"remarks$i\" id=\"remarks$i\" class=\"textboxnum\" maxlength=\"255\" value=\"".$remarks."\"></td>";
						$x .= "<td class=\"eSporttdborder eSportprinttext\">" . Get_String_Display($StudentList[$i][12]) . "</td>";
						$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $remarks . "</td>";
						$x .= '</tr>' . "\n";
					}

				} else {
					$x .= '<tr>' . "\n";
					$x .= '<td class="eSporttdborder eSportprinttext" colspan="7" align="center">' . $Lang['General']['NoRecordFound'] . '</td>' . "\n";
					$x .= '</tr>' . "\n";
				}

				if ($AttendOverviewID >= 0) {
					include_once ('libuser.php');
					$lu = new libuser($StudentList[0][10]);
					//				$x .= '<tr><td colspan="5"><table width="auto" border="0" cellpadding="0" cellspacing="0">'."\n";
					//				$x .= '<tr><td style="padding:10px 50px 0px 0px;">'.$Lang['General']['LastModifiedBy'].': '.Get_Lang_Selection($lu->ChineseName,$lu->EnglishName).'</td>';
					//				$x .= '<td style="padding:10px 50px 0px 0px;">'.$Lang['SysMgr']['CycleDay']['LastModifiedDate'].': '.$StudentList[0][11].'</td></tr>';
					//				$x .='</table></td></tr>';

					$x .= '<tr><td class="eSporttdborder eSportprinttext" colspan="7"> 
					              	  		' . Get_Last_Modified_Remark($StudentList[0][11], Get_Lang_Selection($lu->ChineseName, $lu->EnglishName));


					if(get_client_region() == 'zh_TW') {
						$x .= '<br/>'.$Lang['StudentAttendance']['AbsentProveReport']['RemarksInconsistencyApply'];
						$x .= '<br/>'.$Lang['StudentAttendance']['AbsentProveReport']['RemarksInconsistencyPreset'];
					}

					$x .= '</td></tr>';
				}


			}

		if ($Format == "web") {
			$tool_btns = array();
			$tool_btns[] = array('other','javascript:void(0);',$DisplayAllPhoto,'photoBtn',' onclick="if($(\'.photo\').next(\'img\').is(\':visible\')){$(\'.photo\').show().next(\'img\').hide();$(this).html(\''.$DisplayAllPhoto.'\');}else{$(\'.photo\').hide().next(\'img\').show();$(this).html(\''.$HiddenAllPhoto.'\');}" ');
			$x .= '<div style="width:98%;">';
			$x .= $linterface->Get_DBTable_Action_Button_IP25($tool_btns);
			$x .= '<br style="clear:both">';
			$x .= '</div>';
			$x .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
			$x .= '<tr class="tabletop">' . "\n";
			$x .= '<td>' . $Lang['StudentAttendance']['Class'] . '</td>' . "\n";
			$x .= '<td>' . $Lang['StudentAttendance']['ClassNumber'] . '</td>' . "\n";
			$x .= '<td>' . $Lang['StudentAttendance']['StudentName'] . '</td>' . "\n";
			if($sys_custom['LessonAttendance_DisplayTime']){
				$x .= '<td>'.$i_SmartCard_Frontend_Take_Attendance_In_School_Time.'</td>'."\n";
			}
			if($sys_custom['LessonAttendance_LaSalleCollege']){
				$all_checkbox = '<input type="checkbox" id="all_checkbox" name="all_checkbox" value="" onclick="$(\'.checkbox\').attr(\'checked\',this.checked);" />';
			}
			$x .= '<td>' .$all_checkbox . $Lang['StudentAttendance']['eNoticeTemplateStatus'] . ' ' . $temp . '</td>' . "\n";
			if(get_client_region() == 'zh_TW') {
				$x .= '<td>' .$Lang['StudentAttendance']['LeaveType'] .'</td>'."\n";
			}
			$x .= '<td>' . $Lang['LessonAttendance']['Reason'].'</td>'."\n";
			$x .= '<td>' . $Lang['StudentAttendance']['OutingRemark'] . '</td>' . "\n";
			$x .= '</tr>' . "\n";

			if ($StudentListCount > 0) {
				for ($i = 0; $i < $StudentListCount; $i++) {
					$css_num = ($i % 2) == 0 ? '2' : '';
					
					if ($AttendOverviewID < 0) {
						if(isset($studentIdToLeaveRecord[$StudentList[$i][0]])) {
							$StudentList[$i][5] = $statusMapping['Absent']['code'];
						}
					}
					
					$photo_div = '';
					if($StudentList[$i][7] != ''){
						$photo_div = '&nbsp;<span class="photo" onmouseover="$(this).find(\'img\').attr(\'src\',\''.str_replace($intranet_root,'',$StudentList[$i][7]).'\').css({\'position\':\'absolute\',\'border\':\'1px solid #000\'});" onmouseleave="$(this).find(\'img\').attr(\'src\',\'/images/'.$LAYOUT_SKIN.'/icon_photo.gif\').css({\'position\':\'relative\',\'border\':\'none\'});" >';
						$photo_div .= '<img src="/images/'.$LAYOUT_SKIN.'/icon_photo.gif" /></span><img src="'.str_replace($intranet_root,'',$StudentList[$i][7]).'" style="display:none;" />';
					}
					
					$x .= '<tr class="tablerow' . $css_num . '">';
					$x .= '<td class="tabletext">' . Get_String_Display(Get_Lang_Selection($StudentList[$i][9], $StudentList[$i][4])) . '</td>';
					$x .= '<td class="tabletext">' . Get_String_Display($StudentList[$i][3]) . '</td>';
					$x .= '<td class="tabletext">' . Get_String_Display(Get_Lang_Selection($StudentList[$i][2], $StudentList[$i][1])). $photo_div . '</td>';
					
					$selections = array (
						'',
						'',
						'',
						''
					);
					/*
					if ($StudentList[$i][5] === '0') { // present
						//$x .= '<td class="tabletext" style="background-color:#009900; border-bottom: solid 1px white; border-right: solid 1px white;">'.$Lang['LessonAttendance']['Present'].'</td>';
						$selections[0] = "SELECTED";
						$class = "greenText";
					} else if ($StudentList[$i][5] === '1') { // outing
						//$x .= '<td class="tabletext" style="background-color:#6699FF; border-bottom: solid 1px white; border-right: solid 1px white;">'.$Lang['LessonAttendance']['Outing'].'</td>';
						$selections[1] = "SELECTED";
						$class = "blueText";
					} else if ($StudentList[$i][5] === '2') { // late
						//$x .= '<td class="tabletext" style="background-color:#FFCCCC; border-bottom: solid 1px white; border-right: solid 1px white;">'.$Lang['LessonAttendance']['Late'].'</td>';
						$selections[2] = "SELECTED";
						$class = "orangeText";
					} else if ($StudentList[$i][5] === '3') { // Absent
						//$x .= '<td class="tabletext" style="background-color:#CC0000; border-bottom: solid 1px white; border-right: solid 1px white;">'.$Lang['LessonAttendance']['Absent'].'</td>';
						$selections[3] = "SELECTED";
						$class = "redText";
					}
					*/
					$class = '';
					$visible_css = $sys_custom['LessonAttendance_DisplayTime'] && $StudentList[$i][5]=='-1'? ' style="visibility:hidden;" ' : '';
					foreach($statusMapping as $key => $value)
					{
						if($StudentList[$i][5] == $value['code'])
						{
							$class = $value['class'];
							if($key == 'Present') $visible_css = ' style="visibility:hidden;" ';
						}
					}
					
					if($sys_custom['LessonAttendance_DisplayTime']){
						$x .= "<td class=\"tabletext\">".(Get_String_Display($studentIdToTime[$StudentList[$i][0]]))."</td>";
					}
					$x .= "<td class=\"tabletext\">";
					if($sys_custom['LessonAttendance_LaSalleCollege']){
						$x .= '<input type="checkbox" id="checkbox['.$i.']" name="checkbox[]" class="checkbox" value="" />';
					}
					$x .= "<select class=\"drop_down_status $class\" id=\"drop_down_status[$i]\" name=\"drop_down_status[]\" onChange=\"Change_Single_Status($i, this);\">\n";
					/*
					$x .= "<option value=\"0\" " . $selections[0] . " class=\"greenText\">" . $Lang['StudentAttendance']['Present'] . "</option>\n";
					$x .= "<option value=\"2\" " . $selections[2] . " class=\"orangeText\">" . $Lang['StudentAttendance']['Late'] . "</option>\n";
					$x .= "<option value=\"3\" " . $selections[3] . " class=\"redText\">" . $Lang['StudentAttendance']['Absent'] . "</option>\n";
					$x .= "<option value=\"1\" " . $selections[1] . " class=\"blueText\">" . $Lang['StudentAttendance']['Outing'] . "</option>\n";
					*/
					if($sys_custom['LessonAttendance_LaSalleCollege']){
						$x .= '<option value="-1" '.($StudentList[$i][5] == '-1'?' selected="selected" ':'').'>-- ' . $Lang['Btn']['Select'] . ' --</option>'."\n";
					}
					foreach($statusMapping as $value)
					{
						$x .= '<option value="'.$value['code'].'" class="'.$value['class'].'" '.($StudentList[$i][5] == $value['code']?' selected="selected" ':'').'>'.$value['text'].'</option>'."\n";
					}
					
					$x .= "</select></td>\n";
					
					if(get_client_region() == 'zh_TW') {
						$sql ="SELECT TypeID,TypeName FROM CARD_STUDENT_LEAVE_TYPE ORDER BY TypeSymbol,TypeName";
						$temp = $this->returnArray($sql,2);
						$leave_status = '';
						if($AttendOverviewID < 0) {
							if(isset($studentIdToLeaveRecord[$StudentList[$i][0]])) {
								$StudentList[$i][12] = $studentIdToLeaveRecord[$StudentList[$i][0]]['reason'];
								$StudentList[$i][13] = $studentIdToLeaveRecord[$StudentList[$i][0]]['leavetype'];
								if ($studentIdToLeaveRecord[$StudentList[$i][0]]['type'] == 'apply') {
									$leave_status = "<span class=\"tabletextrequire\">#</span>";
								}
							}
						} else {
							if(isset($studentIdToLeaveRecord[$StudentList[$i][0]])) {
								if ($studentIdToLeaveRecord[$StudentList[$i][0]]['type'] == 'apply') {
									$leave_status = "<span class=\"tabletextrequire\">#</span>";
								} else {
									if ($StudentList[$i][12] != $studentIdToLeaveRecord[$StudentList[$i][0]]['reason'] || $StudentList[$i][13] != $studentIdToLeaveRecord[$StudentList[$i][0]]['leavetype']) {
										$leave_status = "<span class=\"tabletextrequire\">*</span>";
									}
								}
							}
						}


						$selLeaveType = getSelectByArray($temp," name=\"leaveType$i\"", $StudentList[$i][13],0,0);
						$leavetype_visible_css = ' style="visibility:hidden;"';
						if($StudentList[$i][5] == '3') { //absent
							$leavetype_visible_css = '';
						}
						$x .= "<td class=\"tabletext\">
									<span id=\"leaveTypeSpan$i\" $leavetype_visible_css>
									$leave_status
									$selLeaveType
									</span>				
								</td>";
					}

					//$x .= '<td class="tabletext">'.Get_String_Display($StudentListCount[$i][5]).'</td>';
					//$x .= '<td class="tabletext">'.Get_String_Display($StudentList[$i][6]).'</td>';
					if (Get_String_Display($StudentList[$i][6]) != "--")
						$remarks = Get_String_Display($StudentList[$i][6]);
					else
						$remarks = '';
						
					$select_input_word = $linterface->GET_PRESET_LIST("getLessonAttendanceReasons($i)", 'la_'.$i, "reason$i");	
						
					$x .= "<td class=\"tabletext\"><span id=\"reasonSpan$i\" $visible_css>".$linterface->GET_TEXTBOX_NAME("reason$i", "reason$i", $StudentList[$i][12], '', array()).'&nbsp;'.$select_input_word."</span></td>";
					$x .= "<td class=\"tabletext\"><input type=\"text\" name=\"remarks$i\" id=\"remarks$i\" class=\"textboxnum\" maxlength=\"255\" value=\"" . $remarks . "\"></td>";
					$x .= '</tr>' . "\n";
				}

			} else {
				$x .= '<tr class="tablerow2">' . "\n";
				$x .= '<td class="tabletext" colspan="7" align="center">' . $Lang['General']['NoRecordFound'] . '</td>' . "\n";
				$x .= '</tr>' . "\n";
			}

			if ($AttendOverviewID >= 0) {
				include_once ('libuser.php');
				$lu = new libuser($StudentList[0][10]);
				//				$x .= '<tr><td colspan="5"><table width="auto" border="0" cellpadding="0" cellspacing="0">'."\n";
				//				$x .= '<tr><td style="padding:10px 50px 0px 0px;">'.$Lang['General']['LastModifiedBy'].': '.Get_Lang_Selection($lu->ChineseName,$lu->EnglishName).'</td>';
				//				$x .= '<td style="padding:10px 50px 0px 0px;">'.$Lang['SysMgr']['CycleDay']['LastModifiedDate'].': '.$StudentList[0][11].'</td></tr>';
				//				$x .='</table></td></tr>';

				$x .= '<tr><td colspan="7"><span class="row_content tabletextremark"> 
				              	  		' . Get_Last_Modified_Remark($StudentList[0][11], Get_Lang_Selection($lu->ChineseName, $lu->EnglishName)) . '
				              	  	</span></td></tr>';
			}

			if(get_client_region() == 'zh_TW') {
				$x .= "<tr><td class='tabletextremark' colspan = 7'>" . $Lang['StudentAttendance']['AbsentProveReport']['RemarksInconsistencyApply']. "</td><tr>";
				$x .= "<tr><td class='tabletextremark' colspan = 7'>" . $Lang['StudentAttendance']['AbsentProveReport']['RemarksInconsistencyPreset']. "</td><tr>";
			}

			$x .= '<tr>
					<td colspan="6">
						<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
					    	<tr>
			                	<td class="dotline"><img src="' . "{$image_path}/{$LAYOUT_SKIN}" . '/10x10.gif" width="10" height="1" /></td>
			                </tr>
					    </table>	
					</td>
				</tr>';

			$x .= '<tr>
					<td colspan="6">
						<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
					    	<tr>
							    <td align="center" colspan="2">';
			if ($StudentListCount > 0) {
				$x .= $linterface->GET_ACTION_BTN($button_save, "button", "Check_Form();");
				$x .= " ";
				$x .= $linterface->GET_ACTION_BTN($button_reset, "reset", "drop_down_status_onchange()", "reset2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
				$x .= " ";
				if ($isSmartCard == 1)
					$x .= $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "Cancel_Form();") . "\n";
				else
					$x .= $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "window.location.href='index2.php?TargetDate=$LessonDate';") . "\n";
			}
			$x .= '</td>
							</tr>
						</table>
					</td>
				</tr>';
		}
		$x .= '</table>' . "\n";
		$x .= '<input name="RecordID" type="hidden" value="' . $RecordID . '">';
		$x .= '<input name="RecordType" type="hidden" value="' . $RecordType . '">';
		$x .= '<input name="LessonDate" type="hidden" value="' . $LessonDate . '">';
		$x .= '<input name="AttendOverviewID" type="hidden" value="' . $AttendOverviewID . '">';
		$x .= '<input name="RoomAllocationID" type="hidden" value="' . $_REQUEST['RoomAllocationID'] . '">';
		$x .= '<input name="StartTime1" type="hidden" value="' . $_REQUEST['StartTime'] . '">';
		$x .= '<input name="EndTime1" type="hidden" value="' . $_REQUEST['EndTime'] . '">';
		$x .= '<input name="SubjectGroupID" type="hidden" value="' . $_REQUEST['SubjectGroupID'] . '">';
		$x .= '<input name="LastLoadTime" type="hidden" value="'.$ts_loaded.'" />';
		for ($i = 0; $i < $StudentListCount; $i++) {
			$x .= '<input name="StudentID' . $i . '" type="hidden" value="' . $StudentList[$i][0] . '">';
		}
		$x .= '</form>';

		//}
		return $x;
		//return $this->Get_Student_List($RecordID,$RecordType,$LessonDate);
	}

	function Class_Lesson_Daily_Report($TargetDate = "", $AttendStatus = array(1,2,3), $Format = "web") {
		global $Lang, $button_print;

		$linterface = new interface_html();
		$TargetDate = ($TargetDate != "") ? $TargetDate : date('Y-m-d');

		$TimeSlotList = $this->Get_Time_Slot_List($TargetDate);
		//debug_r($TimeSlotList);
		$Temp = $this->Get_Class_Lesson_Daily_Late_Absent($TargetDate, $AttendStatus);
		//debug_r($Temp);
		for ($i = 0; $i < sizeof($Temp); $i++) {
			$UserIDList[$Temp[$i]['UserID']] = $Temp[$i]['StudentName'];
			$AttendDetailList[$Temp[$i]['UserID']][$Temp[$i]['TimeSlotID']] = $Temp[$i];
		}
		//debug_r($UserIDList);
		//debug_r($AttendDetailList);
		if ($Format == "web") {
			$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
			$x .= "<tr><td>";
			$x .= '<div class="content_top_tool">
			               		<div class="Conntent_tool">';
			$x .= $linterface->GET_LNK_PRINT("javascript:PrintPage();", "", "", "", "", 1);
			$x .= $linterface->GET_LNK_EXPORT("lesson_class_detail_report_export.php?TargetDate=" . $TargetDate . "&AttendStatus[]=" . (sizeof($AttendStatus) > 0 ? implode("&AttendStatus[]=", $AttendStatus) : "") . "", "", "", "", "", 1);
			$x .= '  </div>
							    <br style="clear: both;">
							  </div>';
			$x .= "</td></tr><table>";
		} else
			if ($Format == "print") {
				$x .= '<table width="95%" align="center" class="print_hide" border=0>
									<tr>
										<td align="right">' . $linterface->GET_BTN($button_print, "button", "javascript:window.print();", "submit2") . '</td>
									</tr>
								   </table>';
			}

		if ($Format == "export") {
			include_once ("libexporttext.php");
			$lexport = new libexporttext();

			$Rows = array ();
			$Details = array ();
			$ExportColumn = array ();

			$ExportColumn[] = $Lang['SysMgr']['FormClassMapping']['StudentName'];
			for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
				$ExportColumn[] = $TimeSlotList[$i]['TimeSlotName'];
			}
			if (sizeof($UserIDList) > 0) {
				foreach ($UserIDList as $key => $val) {
					unset ($Details);
					$Details = array ();
					$Details[] = $val;
					for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
						$AttendDetail = $AttendDetailList[$key][$TimeSlotList[$i]['TimeSlotID']];

						if ($AttendDetail['AttendStatus'] == -1 && $AttendDetail['AttendOverviewID'] == -1) { //  not confirmed
							$Details[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['NotConfirmed'] . ')';
						} else
							if ($AttendDetail['AttendStatus'] === '0') { // 0=present, 1=outing, 2=late, 3=absent
								$Details[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['Present'] . ')';
							} else
								if ($AttendDetail['AttendStatus'] === '1') { // 0=present, 1=outing, 2=late, 3=absent
									$Details[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['Outing'] . ')';
								} else
									if ($AttendDetail['AttendStatus'] === '2') { // 0=present, 1=outing, 2=late, 3=absent
										$Details[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['Late'] . ')';
									} else
										if ($AttendDetail['AttendStatus'] === '3') { // 0=present, 1=outing, 2=late, 3=absent
											$Details[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['Absent'] . ')';
										} else { // no class
											$Details[] = '';
										}
					}
					$Rows[] = $Details;
				}
			} else {
				$Details[] = $Lang['General']['NoRecordFound'];
				for ($i = 1; $i < sizeof($TimeSlotList); $i++) {
					$Details[] = '';
				}
				$Rows[] = $Details;
			}

			$exportContent = $lexport->GET_EXPORT_TXT($Rows, $ExportColumn);
			$lexport->EXPORT_FILE('lesson_daily_report_' . $TargetDate . '.csv', $exportContent);

			return;
		}

		$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
		$x .= "<tr class=\"tabletop\">";
		$x .= "	<td class=\"tabletoplink\">" . $Lang['SysMgr']['FormClassMapping']['StudentName'] . "</td>";

		for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
			$x .= "<td class=\"tabletoplink\">" . $TimeSlotList[$i]['TimeSlotName'] . "</td>";
		}
		$x .= "</tr>\n";

		if (sizeof($UserIDList) > 0) {
			foreach ($UserIDList as $key => $val) {
				$x .= '<tr class="tablerow2">';
				$x .= '	<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' . $val . '</td>';

				for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
					$AttendDetail = $AttendDetailList[$key][$TimeSlotList[$i]['TimeSlotID']];

					//echo $val.'-'.$TimeSlotList[$i]['TimeSlotName'].'-'.Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']).' - '.$AttendDetail['AttendStatus'].'<br>';
					if ($AttendDetail['AttendStatus'] == -1 && $AttendDetail['AttendOverviewID'] == -1) { //  not confirmed
						$x .= '<td class="tabletext" style="background-color:#CCCC66; border-bottom: solid 1px white; border-right: solid 1px white;">';
						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']);
						$x .= '-';
						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']);
						$x .= '</td>';
					} else
						if ($AttendDetail['AttendStatus'] === '0') { // present
							$x .= '<td class="tabletext" style="background-color:#009900; border-bottom: solid 1px white; border-right: solid 1px white;">';
							$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']);
							$x .= '-';
							$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']);
							$x .= '</td>';
						} else
							if ($AttendDetail['AttendStatus'] === '1') { // outing
								$x .= '<td class="tabletext" style="background-color:#6699FF; border-bottom: solid 1px white; border-right: solid 1px white;">';
								$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']);
								$x .= '-';
								$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']);
								$x .= '</td>';
							} else
								if ($AttendDetail['AttendStatus'] === '2') { // late
									$x .= '<td class="tabletext" style="background-color:#FFCCCC; border-bottom: solid 1px white; border-right: solid 1px white;">';
									$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']);
									$x .= '-';
									$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']);
									$x .= '</td>';
								} else
									if ($AttendDetail['AttendStatus'] === '3') { // Absent
										$x .= '<td class="tabletext" style="background-color:#CC0000; border-bottom: solid 1px white; border-right: solid 1px white;">';
										$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']);
										$x .= '-';
										$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']);
										$x .= '</td>';
									} else { // no class
										$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">';
										$x .= '&nbsp;';
										$x .= '</td>';
									}
				}
				$x .= "</tr>";
			}
		} else {
			$x .= '<tr class="tablerow2">';
			$x .= '	<td class="tabletext" align="center" colspan="' . (sizeof($TimeSlotList) + 1) . '">
										' . $Lang['General']['NoRecordFound'] . '
										</td>';
			$x .= "</tr>";
		}

		$x .= "</table>";

		if ($Format == "web") {
			$x .= '<script type="text/JavaScript" language="JavaScript">';
			$x .= 'function PrintPage()
							   {
									url="lesson_class_detail_report_print.php?TargetDate=' . $TargetDate . '&AttendStatus[]=' . (sizeof($AttendStatus) > 0 ? implode("&AttendStatus[]=", $AttendStatus) : '') . '"; 
									newWindow(url,10);
							   }';
			$x .= '</script>';
		}

		return $x;
	}
	
	function Class_Lesson_Daily_Report_Export($StartDate,$EndDate,$AttendStatus=array(2,3,1))
	{
		global $Lang, $sys_custom;
		
		include_once ("libexporttext.php");
		$lexport = new libexporttext();
		
		$header = array();
		$rows = array();
		$tmp_rows = array();
		
		$tsStartDate = strtotime($StartDate);
		$tsEndDate = strtotime($EndDate);
		$tsCurDate = $tsEndDate;
		$prevTimeSlotPeriodID = 0;
		$prevTimeSlotList = array();
		$table_row_count = 0;
		$numRecord = 0;
		
		while ($tsCurDate >= $tsStartDate) {
			$curDate = date("Y-m-d",$tsCurDate);
			$TimeSlotList = $this->Get_Time_Slot_List2($curDate);
			if(count($TimeSlotList) == 0){
				$tsCurDate -= 86400;
				continue;
			}
			
			$curTimeSlotPeriodID = $TimeSlotList[0]['PeriodID'];	
			
			if($prevTimeSlotPeriodID != $curTimeSlotPeriodID) {
				if($numRecord > 0 || ($prevTimeSlotPeriodID != 0 && $table_row_count==0)) {
					$table_row_count = 0;
					if(count($tmp_rows)>1){
						$rows = array_merge($rows,$tmp_rows);
					}
					$tmp_rows = array();
				}
				$prevTimeSlotList = $TimeSlotList;
				$row = array();
				$row[] = $Lang['General']['Date'];
				$row[] = $Lang['StudentAttendance']['ClassName'];
				$row[] = $Lang['General']['ClassNumber'];
				$row[] = $Lang['General']['EnglishName'];
				$row[] = $Lang['General']['ChineseName'];
				for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
					$row[] = $TimeSlotList[$i]['TimeSlotName'];
				}
				$tmp_rows[] = $row;
			}
			
			$Temp = $this->Get_Class_Lesson_Daily_Late_Absent($curDate, $AttendStatus);

			//debug_pr($Temp);
			$UserIDList = array ();
			$AttendDetailList = array ();
			
			for ($i = 0; $i < sizeof($Temp); $i++) {
				$UserIDList[$Temp[$i]['UserID']] = $Temp[$i];
				$AttendDetailList[$Temp[$i]['UserID']][$Temp[$i]['TimeSlotID']] = $Temp[$i];
			}
			
			if (sizeof($UserIDList) > 0) {
				
				$hasRecord = 1;

				foreach ($UserIDList as $key => $val) {
					
					$row = array();
					$row[] = $curDate;
					$row[] = $UserIDList[$key]['ClassName'];
					$row[] = $UserIDList[$key]['ClassNumber'];
					$row[] = $UserIDList[$key]['EnglishName'];
					$row[] = $UserIDList[$key]['ChineseName'];
					
					$table_row_count++;
					for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
						$AttendDetail = $AttendDetailList[$key][$TimeSlotList[$i]['TimeSlotID']];
						$numRecord++;
						
						if ($AttendDetail['AttendStatus'] == -1 && $AttendDetail['AttendOverviewID'] == -1 && in_array(-1, $AttendStatus)) { //  not confirmed
							if($sys_custom['LessonAttendance_CustomReportStatusSymbol']) {
								$row[] = $sys_custom['LessonAttendance_CustomReportStatusSymbolArr'][$AttendDetail['AttendStatus']];
							}else{
								$row[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['NotConfirmed'] . ')';
							}
						}else if ($AttendDetail['AttendStatus'] === '0' && in_array(0, $AttendStatus)) { // present
							if($sys_custom['LessonAttendance_CustomReportStatusSymbol']) {
								$row[] = $sys_custom['LessonAttendance_CustomReportStatusSymbolArr'][$AttendDetail['AttendStatus']];
							}else{
								$row[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['Present'] . ')';
							}
						} else if ($AttendDetail['AttendStatus'] === '1' && in_array(1, $AttendStatus)) { // outing
							if($sys_custom['LessonAttendance_CustomReportStatusSymbol']) {
								$row[] = $sys_custom['LessonAttendance_CustomReportStatusSymbolArr'][$AttendDetail['AttendStatus']];
							}else{
								$row[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['Outing'] . ')';
							}
						} else if ($AttendDetail['AttendStatus'] === '2' && in_array(2, $AttendStatus)) { // late
							if($sys_custom['LessonAttendance_CustomReportStatusSymbol']) {
								$row[] = $sys_custom['LessonAttendance_CustomReportStatusSymbolArr'][$AttendDetail['AttendStatus']];
							}else{
								$row[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['Late'] . ')';
							}
						} else if ($AttendDetail['AttendStatus'] === '3' && in_array(3, $AttendStatus)) { // Absent
							if($sys_custom['LessonAttendance_CustomReportStatusSymbol']) {
								$row[] = $sys_custom['LessonAttendance_CustomReportStatusSymbolArr'][$AttendDetail['AttendStatus']];
							}else{
								$row[] = Get_Lang_Selection($AttendDetail['SubjectNameB5'], $AttendDetail['SubjectNameEN']) . '-' . Get_Lang_Selection($AttendDetail['ClassTitleB5'], $AttendDetail['ClassTitleEN']) . '(' . $Lang['LessonAttendance']['Absent'] . ')';
							}
						} else { // no class
							$row[] = '';
						}
					} // End of for TimeLostList
					$tmp_rows[] = $row;
					
				} // end of looping UserIDList
			} // end of if size of UserIDList > 0
			
			if($tsStartDate == $tsCurDate && $prevTimeSlotPeriodID == $curTimeSlotPeriodID) {
				$table_row_count = 0;
				if(count($tmp_rows)>1) {
					$rows = array_merge($rows,$tmp_rows);
				}
			}
			
			$prevTimeSlotPeriodID = $curTimeSlotPeriodID;
			$tsCurDate -= 86400;
		}
		
		$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
		$lexport->EXPORT_FILE('lesson_daily_report_'.$StartDate.'-'.$EndDate.'.csv', $exportContent);
	}
	
	//Henry Adding
	function returnDaysBeforeToOpenData() {
		//Henry added
		include_once ("libgeneralsettings.php");
		$GeneralSetting = new libgeneralsettings();
		$SettingList[] = "'EnablePSLAD'";
		$SettingList[] = "'DaysBeforeToOpenData'";
		$Settings = $GeneralSetting->Get_General_Setting('LessonAttendance', $SettingList);
		$date = date("Y-m-d");
		$return_date = "";
		if ($Settings['EnablePSLAD'] == 1) {
			$add_days = $Settings['DaysBeforeToOpenData'];
			$return_date = date('Y-m-d', strtotime($date) - (24 * 3600 * $add_days));
		}
		return $return_date;
	}
	function Class_Lesson_Daily_Report_student($FromDate = "", $ToDate = "", $AttendStatus = array (
		1,
		2,
		3,
		-1,
		0
	), $StudentID = "") {
		global $Lang;
		$linterface = new interface_html();
		//$TargetDate = ($TargetDate != "")? $TargetDate:'2013-08-02';
		$FromDate = ($FromDate != "") ? $FromDate : date('Y-m-d');
		$ToDate = ($ToDate != "") ? $ToDate : date('Y-m-d');
		$remarks = "";

		//setting checking and warning users
		if ($this->returnDaysBeforeToOpenData() && strtotime($this->returnDaysBeforeToOpenData()) < strtotime($ToDate)) {
			$ToDate = $this->returnDaysBeforeToOpenData();
			$remarks = "*" . $Lang['LessonAttendance']['LARShownBeforeDate'] . " " . $ToDate;
			$remarks = '<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center"><tr><td><span class="tabletextremark">' . $remarks . '</span></td></tr></table>';
		}
		if ($this->returnDaysBeforeToOpenData() == '') {
			$remarks = "*" . $Lang['LessonAttendance']['LARDisable'];
			$remarks = '<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center"><tr><td><span style="color:red">' . $remarks . '</span></td></tr></table>';
			return $remarks;
		}

		//		$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
		//		$x .= "<tr class=\"tabletop\">";
		//		$x .= "	<td class=\"tabletoplink\">".$Lang['General']['Date']."</td>";
		//		
		//		$TimeSlotList1 = $this->Get_Time_Slot_List2($FromDate);
		//		for ($i=0; $i< sizeof($TimeSlotList); $i++) {
		//			$x .= "<td class=\"tabletoplink\">".$TimeSlotList[$i]['TimeSlotName']."</td>";
		//		}
		//		$x .= "</tr>\n";

		$hasRecord = 0;
		$numRecord = 0;
		$numRecord1 = 0;
		//$j = 0;
		$initFromDate = $FromDate;
		$x1 = '';
		
		$tsFromDate = strtotime($FromDate);
		$tsToDate = strtotime($ToDate);
		$prevTimeSlotPeriodID = 0;
		$prevTimeSlotList = array();
		$table_row_count = 0;
		
		while ($tsFromDate <= $tsToDate) {
			$curDate = date("Y-m-d",$tsFromDate);
			$TimeSlotList = $this->Get_Time_Slot_List2($curDate);
			if(count($TimeSlotList) == 0){
				$tsFromDate += 86400;
				continue;
			}
			
			$curTimeSlotPeriodID = $TimeSlotList[0]['PeriodID'];
//			if(strtotime($FromDate) < strtotime($ToDate))
//				$TimeSlotList2 = $this->Get_Time_Slot_List2(date("Y-m-d", strtotime("+1 day", strtotime($FromDate))));
//			else
//				$TimeSlotList2 = $TimeSlotList;		
			
			if($prevTimeSlotPeriodID != $curTimeSlotPeriodID) {
				if($numRecord1 > 0 || ($prevTimeSlotPeriodID != 0 && $table_row_count==0)) {
					if($table_row_count == 0) {
						$x1 .= '<tr class="tablerow2">';
						$x1 .= '	<td class="tabletext" align="center" colspan="' . (sizeof($prevTimeSlotList) + 1) . '">
									' . $Lang['General']['NoRecordFound'] . '
								</td>';
						$x1 .= "</tr>";
					}
					$table_row_count = 0;
					$x1 .= "</table>";
					$x1 .= "<br />";
				}
				$prevTimeSlotList = $TimeSlotList;
				$x1 .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
					$x1 .= "<tr class=\"tabletop\">";
					$x1 .= "	<td>" . $Lang['General']['Date'] . "</td>";
					for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
						$x1 .= "<td>" . $TimeSlotList[$i]['TimeSlotName'] . "</td>";
					}
					$x1 .= "</tr>\n";
			}
			
			$Temp = $this->Get_Class_Lesson_Daily_Late_Absent($curDate, $AttendStatus, $StudentID);

			//debug_pr($Temp);
			$UserIDList = array ();
			$AttendDetailList = array ();
			
			for ($i = 0; $i < sizeof($Temp); $i++) {
				$UserIDList[$Temp[$i]['UserID']] = $Temp[$i]['StudentName'];
				$AttendDetailList[$Temp[$i]['UserID']][$Temp[$i]['TimeSlotID']] = $Temp[$i];
			}
			//debug_r($UserIDList);
			//debug_r($AttendDetailList);
			//			$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
			//			$x .= "<tr><td>";
			//			$x .='<div class="content_top_tool">
			//               		<div class="Conntent_tool">';
			//			$x .= $linterface->GET_LNK_PRINT("javascript:PrintPage();","","","","",1);
			//			$x .= $linterface->GET_LNK_EXPORT("lesson_class_detail_report_export.php?TargetDate=".$TargetDate."&AttendStatus[]=".(sizeof($AttendStatus)>0?implode(",",$AttendStatus):"")."","","","","",1);
			//			$x .= '  </div>
			//				    <br style="clear: both;">
			//				  </div>';
			//			$x .= "</td></tr><table>";
			
			if (sizeof($UserIDList) > 0) {

				$hasRecord = 1;

				foreach ($UserIDList as $key => $val) {

					$y = '<tr class="tablerow2">';
					$y .= '	<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">' . $curDate . '</td>';
					$noRecord = 0;
					$table_row_count++;
					for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
						$AttendDetail = $AttendDetailList[$key][$TimeSlotList[$i]['TimeSlotID']];
						$numRecord1++;
						//echo $val.'-'.$TimeSlotList[$i]['TimeSlotName'].'-'.Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']).' - '.$AttendDetail['AttendStatus'].'<br>';
						if ($AttendDetail['AttendStatus'] == -1 && $AttendDetail['AttendOverviewID'] == -1 && in_array(-1, $AttendStatus)) { //  not confirmed
							$y .= '<td class="tabletext" style="background-color:#CCCC66; border-bottom: solid 1px white; border-right: solid 1px white;">';
							//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
							//						$x .= '-';
							//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
							$y .= '</td>';
						} else
							if ($AttendDetail['AttendStatus'] === '0' && in_array(0, $AttendStatus)) { // present
								$y .= '<td class="tabletext" style="background-color:#009900; border-bottom: solid 1px white; border-right: solid 1px white;">';
								//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
								//						$x .= '-';
								//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
								$y .= '</td>';
							} else
								if ($AttendDetail['AttendStatus'] === '1' && in_array(1, $AttendStatus)) { // outing
									$y .= '<td class="tabletext" style="background-color:#6699FF; border-bottom: solid 1px white; border-right: solid 1px white;">';
									//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
									//						$x .= '-';
									//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
									$y .= '</td>';
								} else
									if ($AttendDetail['AttendStatus'] === '2' && in_array(2, $AttendStatus)) { // late
										//debug_pr("AttendStatus = 2");
										$y .= '<td class="tabletext" style="background-color:#FFCCCC; border-bottom: solid 1px white; border-right: solid 1px white;">';
										//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
										//						$x .= '-';
										//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
										$y .= '</td>';
									} else
										if ($AttendDetail['AttendStatus'] === '3' && in_array(3, $AttendStatus)) { // Absent
											$y .= '<td class="tabletext" style="background-color:#CC0000; border-bottom: solid 1px white; border-right: solid 1px white;">';
											//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
											//						$x .= '-';
											//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
											$y .= '</td>';
										} else { // no class
											$y .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">';
											$y .= '&nbsp;';
											$y .= '</td>';
											$noRecord++;
											$numRecord++;
											//debug_pr("in else");
										}
					}
					$y .= "</tr>";
					
					
//						if (strtotime($FromDate) == strtotime($initFromDate)) {//debug_pr("hihi");
						
//							$x1 .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
//							$x1 .= "<tr class=\"tabletop\">";
//							$x1 .= "	<td class=\"tabletoplink\">" . $Lang['General']['Date'] . "</td>";

							//$TimeSlotList = $this->Get_Time_Slot_List2($FromDate);
//							for ($i = 0; $i < sizeof($TimeSlotList); $i++) {
//								$x1 .= "<td class=\"tabletoplink\">" . $TimeSlotList[$i]['TimeSlotName'] . "</td>";
//							}
//							$x1 .= "</tr>\n";
//						}
						
						//Testing...Start
//						if ($noRecord != sizeof($TimeSlotList)) {
//							$x1 .= $y;
//						}
						//Testing...End
						
//						if ((strtotime($FromDate) < strtotime($ToDate) && ($TimeSlotList[0]['PeriodID'] != $TimeSlotList2[0]['PeriodID']))) {
							
//							$x1 .= "</table>";
//							$x1 .= "<br/>";
//						}
						//					if(!$hasRecord || $numRecord == $numRecord1){
						//		$x .= '<tr class="tablerow2">';
						//		//$x .= '	<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">'.$FromDate.'</td>';
						//		$x .= '	<td class="tabletext" align="center" colspan="'.(sizeof($TimeSlotList)+1).'">
						//						'.$Lang['General']['NoRecordFound'].'
						//						</td>';
						//		$x .= "</tr>";
						//		}
//						if ((strtotime($FromDate) < strtotime($ToDate) && ($TimeSlotList[0]['PeriodID'] != $TimeSlotList2[0]['PeriodID']))) {
							
//							$x1 .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
//							$x1 .= "<tr class=\"tabletop\">";
//							$x1 .= "	<td class=\"tabletoplink\">" . $Lang['General']['Date'] . "</td>";

							//$TimeSlotList = $this->Get_Time_Slot_List2(date("Y-m-d", strtotime("+1 day", strtotime($FromDate))));
//							for ($i = 0; $i < sizeof($TimeSlotList2); $i++) {
//								$x1 .= "<td class=\"tabletoplink\">" . $TimeSlotList2[$i]['TimeSlotName'] . "</td>";
//							}
//							$x1 .= "</tr>\n";
//						}
//						if (strtotime($FromDate) == strtotime($ToDate)) {
//							$x1 .= "</table>";
//						}

					//}
					
					$x1 .= $y;
					
				} // end of looping UserIDList

				
				//$j++;
			} // end of if size of UserIDList > 0
			
			if($tsFromDate == $tsToDate && $prevTimeSlotPeriodID == $curTimeSlotPeriodID) {
				$table_row_count = 0;
				$x1 .= "</table>";
				$x1 .= "<br />";
			}
			
			
			//else {
				//			$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
				//			$x .= '<tr class="tablerow2">';
				//			$x .= '	<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">'.$FromDate.'</td>';
				//			$x .= '	<td class="tabletext" align="center" colspan="'.(sizeof($TimeSlotList)+1).'">
				//							'.$Lang['General']['NoRecordFound'].'
				//							</td>';
				//			$x .= "</tr>";
				//			$x .= "</table>";
			//}
			///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			//$TimeSlotList2[$FromDate] = $this->Get_Time_Slot_List($FromDate);
			
//			$FromDate = date("Y-m-d", strtotime("+1 day", strtotime($FromDate)));
			$prevTimeSlotPeriodID = $curTimeSlotPeriodID;
			$tsFromDate += 86400;
		}

		//		$Temp = $this->Get_Class_Lesson_Daily_Late_Absent($TargetDate,$AttendStatus, $StudentID);
		//		
		//		//debug_r($Temp);
		//		for ($i=0; $i< sizeof($Temp); $i++) {
		//			//if($StudentID == $Temp[$i]['UserID']){
		//			$UserIDList[$Temp[$i]['UserID']] = $Temp[$i]['StudentName'];
		//			$AttendDetailList[$Temp[$i]['UserID']][$Temp[$i]['TimeSlotID']] = $Temp[$i];
		//			//break;
		//			//}
		//		}
		//		//debug_r($UserIDList);
		//		//debug_r($AttendDetailList);
		////			$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
		////			$x .= "<tr><td>";
		////			$x .='<div class="content_top_tool">
		////               		<div class="Conntent_tool">';
		////			$x .= $linterface->GET_LNK_PRINT("javascript:PrintPage();","","","","",1);
		////			$x .= $linterface->GET_LNK_EXPORT("lesson_class_detail_report_export.php?TargetDate=".$TargetDate."&AttendStatus[]=".(sizeof($AttendStatus)>0?implode(",",$AttendStatus):"")."","","","","",1);
		////			$x .= '  </div>
		////				    <br style="clear: both;">
		////				  </div>';
		////			$x .= "</td></tr><table>";
		//		
		//		
		//		
		//		if (sizeof($UserIDList) > 0) {
		//			foreach ($UserIDList as $key => $val) {
		//				$x .= '<tr class="tablerow2">';
		//				$x .= '	<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">'.$TargetDate.'</td>';
		//				
		//				for ($i=0; $i< sizeof($TimeSlotList); $i++) {
		//					$AttendDetail = $AttendDetailList[$key][$TimeSlotList[$i]['TimeSlotID']];
		//					
		//					//echo $val.'-'.$TimeSlotList[$i]['TimeSlotName'].'-'.Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']).' - '.$AttendDetail['AttendStatus'].'<br>';
		//					if ($AttendDetail['AttendStatus'] == -1 && $AttendDetail['AttendOverviewID'] == -1) {//  not confirmed
		//						$x .= '<td class="tabletext" style="background-color:#CCCC66; border-bottom: solid 1px white; border-right: solid 1px white;">';
		//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
		//						$x .= '-';
		//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
		//						$x .= '</td>';
		//					}
		//					else if ($AttendDetail['AttendStatus'] === '0') { // present
		//						$x .= '<td class="tabletext" style="background-color:#009900; border-bottom: solid 1px white; border-right: solid 1px white;">';
		//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
		//						$x .= '-';
		//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
		//						$x .= '</td>';
		//					}
		//					else if ($AttendDetail['AttendStatus'] === '1') { // outing
		//						$x .= '<td class="tabletext" style="background-color:#6699FF; border-bottom: solid 1px white; border-right: solid 1px white;">';
		//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
		//						$x .= '-';
		//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
		//						$x .= '</td>';
		//					}
		//					else if ($AttendDetail['AttendStatus'] === '2') { // late
		//						$x .= '<td class="tabletext" style="background-color:#FFCCCC; border-bottom: solid 1px white; border-right: solid 1px white;">';
		//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
		//						$x .= '-';
		//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
		//						$x .= '</td>';
		//					}
		//					else if ($AttendDetail['AttendStatus'] === '3') { // Absent
		//						$x .= '<td class="tabletext" style="background-color:#CC0000; border-bottom: solid 1px white; border-right: solid 1px white;">';
		//						$x .= Get_Lang_Selection($AttendDetail['SubjectNameB5'],$AttendDetail['SubjectNameEN']);
		//						$x .= '-';
		//						$x .= Get_Lang_Selection($AttendDetail['ClassTitleB5'],$AttendDetail['ClassTitleEN']);
		//						$x .= '</td>';
		//					}
		//					else { // no class
		//						$x .= '<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">';
		//						$x .= '&nbsp;';
		//						$x .= '</td>';
		//					}
		//				}
		//				$x .= "</tr>";
		//			}
		//		}
		//		else {
		//			$x .= '<tr class="tablerow2">';
		//			$x .= '	<td class="tabletext" align="center" colspan="'.(sizeof($TimeSlotList)+1).'">
		//							'.$Lang['General']['NoRecordFound'].'
		//							</td>';
		//			$x .= "</tr>";
		//		}

		if (!$hasRecord || $numRecord == $numRecord1) {
			$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
			$x .= "<tr class=\"tabletop\">";
			$x .= "	<td>" . $Lang['General']['Date'] . "</td>";

			$TimeSlotList1 = $this->Get_Time_Slot_List2($initFromDate);
			for ($i = 0; $i < sizeof($TimeSlotList1); $i++) {
				$x .= "<td>" . $TimeSlotList1[$i]['TimeSlotName'] . "</td>";
			}
			$x .= "</tr>\n";
			$x .= '<tr class="tablerow2">';
			//$x .= '	<td class="tabletext" style="border-bottom: solid 1px white; border-right: solid 1px white;">'.$FromDate.'</td>';
			$x .= '	<td class="tabletext" align="center" colspan="' . (sizeof($TimeSlotList1) + 1) . '">
										' . $Lang['General']['NoRecordFound'] . '
										</td>';
			$x .= "</tr>";
			$x .= "</table>";
		}
		else
			$x .= $x1;
		//		
		//		$x .= "</table>";

		$x .= $remarks;
		//if($Format == "web"){
//		$x .= '<script type="text/JavaScript" language="JavaScript">';
//		$x .= 'function PrintPage()
//						   {
//								url="lesson_class_detail_report_print.php?TargetDate=' . $TargetDate . '&AttendStatus[]=' . (sizeof($AttendStatus) > 0 ? implode(",", $AttendStatus) : '') . '"; 
//								newWindow(url,10);
//						   }';
//		$x .= '</script>';
		//}
		
		return $x;

	}

	function Get_Lesson_Summary_Form() {
		global $Lang, $image_path, $LAYOUT_SKIN;

		$FromDate = date('Y-m-d');
		$ToDate = date('Y-m-d');
		$linterface = new interface_html();
		//!!!!!!!!!!!!!!!!!should be modify [Start]
		//		if($class!=""){
		//			$namefield = getNameFieldWithClassNumberByLang("");
		//			$sql ="SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus IN (0,1,2) AND ClassName='$class' ORDER BY ClassName,ClassNumber";
		//			$temp = $lc->returnArray($sql,2);
		//			$select_student=getSelectByArray($temp,"name='studentID'",$studentID,0,0);
		//		}
		//		$select_class=$this->getSelectClass("name='class' onChange='changeClass()'",$class, 0);
		//!!!!!!!!!!!!!!!!!should be modify [End]

		$x = $this->Include_JS_CSS();

		$x .= '<form name="form1" method="post" action="">
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['SysMgr']['FormClassMapping']['Class'] . '
										</td>
										<td class="tabletext" width="35%">
											' . $this->getSelectClassID('id="ClassID" name="ClassID"  onchange="View_Student_In_Class()"', "") . '
										</td>			
									</tr>
									
									<tr>		
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['PersonalSelection'] . '</td>
									<td width="70%"class="tabletext">
										<input type="checkbox" id="PersonalSelection" value="1" onclick="View_Student_In_Class();">
									</td>
									</tr>
									<tr id="StudentListRow" style="display:none;">
										<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['Identity']['Student'] . '</td>
										<td width="70%"class="tabletext">
											<div id="StudentListLayer">
											</div>
										</td>
									</tr>							
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['LessonAttendance']['FromDate'] . '
										</td>
										<td class="tabletext" width="70%">
											' . $linterface->GET_DATE_PICKER("FromDate", $FromDate, "", "yy-mm-dd", "", "", "", "$('#ToDateWarningLayer').html('" . $Lang['General']['JS_warning']['InvalidDateRange'] . "').hide();") . '
											<span style="color:red" id="FromDateWarningLayer"></span>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['LessonAttendance']['ToDate'] . '
										</td>
										<td class="tabletext" width="70%">
											' . $linterface->GET_DATE_PICKER("ToDate", $ToDate, "", "yy-mm-dd", "", "", "", "$('#ToDateWarningLayer').html('" . $Lang['General']['JS_warning']['InvalidDateRange'] . "').hide();") . '
											<span style="color:red" id="ToDateWarningLayer"></span>
										</td>
									</tr>
									<!-- Added by Henry -->		
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
											' . $Lang['LessonAttendance']['Status'] . '
										</td>
										<td class="tabletext" width="30%">
											'.$linterface->Get_Checkbox("AttendStatus1", "AttendStatus[]", 2, 1, '', $Lang['LessonAttendance']['Late']).'
											'.$linterface->Get_Checkbox("AttendStatus2", "AttendStatus[]", 3, 1, '', $Lang['LessonAttendance']['Absent']).'
											<!--<input type="checkbox" name="AttendStatus[]" value="2" checked>
											' . $Lang['LessonAttendance']['Late'] . '
											<input type="checkbox" name="AttendStatus[]" value="3" checked>
											' . $Lang['LessonAttendance']['Absent'] . '-->
										</td>
									</tr>						
								</table>
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
								    	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
								    </tr>
								    <tr>
									    <td align="center">
											' . $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "View_Lesson_Summary();") . '
										</td>
									</tr>
								</table>
								</form>';

		$x .= '<div id="DetailTableLayer" name="DetailTableLayer">';
		$x .= $this->Get_Lesson_Summary_Report();
		$x .= '</div>';

		return $x;
	}

	function Get_Lesson_Summary_Report($FromDate = "", $ToDate = "", $ClassID = "", $Format = "web", $AttendStatus = array (
		1
	), $StudentID = "") {
		global $Lang, $button_print;

		include_once ('libgeneralsettings.php');

		$linterface = new interface_html();
		$SettingList[] = "'LessonSessionCountAsDay'"; // absent
		$SettingList[] = "'LessonSessionLateCountAsDay'"; // late
		$GeneralSetting = new libgeneralsettings();

		$Settings = $GeneralSetting->Get_General_Setting('LessonAttendance', $SettingList);
		$LessonSessionCountAsDay = (trim($Settings['LessonSessionCountAsDay']) == "") ? 1 : $Settings['LessonSessionCountAsDay'];
		$LessonSessionLateCountAsDay = (trim($Settings['LessonSessionLateCountAsDay']) == "") ? 1 : $Settings['LessonSessionLateCountAsDay'];

		$FromDate = ($FromDate != "") ? $FromDate : date('Y-m-d');
		$ToDate = ($ToDate != "") ? $ToDate : date('Y-m-d');

		//Henry: Check the attendance status
		$tempAttendStatus = 0;
		$lateChecked = 0;
		$absentChecked = 0;
		if ($AttendStatus != '') {
			foreach ($AttendStatus as $status) {
				if ($status == 2)
					$lateChecked = 1;
				else
					if ($status == 3)
						$absentChecked = 1;
			}
		}
		if ($lateChecked == 1 && $absentChecked == 1)
			$tempAttendStatus = 3;
		else
			if ($lateChecked == 1)
				$tempAttendStatus = 1;
			else
				if ($absentChecked == 1)
					$tempAttendStatus = 2;

		$Temp = $this->Get_Lesson_Summary_Report_Data($FromDate, $ToDate, $ClassID, $tempAttendStatus, $StudentID);
		//debug_pr($Temp);
		for ($i = 0; $i < sizeof($Temp); $i++) {
			$StudentStat[$Temp[$i]['UserID']][$Temp[$i]['AttendStatus']] += $Temp[$i]['TotalCount'];
			$StudentStat[$Temp[$i]['UserID']]['Total'] += $Temp[$i]['TotalCount'];
			$StudentStat[$Temp[$i]['UserID']]['Name'] = $Temp[$i]['StudentName'];
		}
		//debug_pr($StudentStat);
		if ($Format == "web") {
			$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
			$x .= "<tr><td>";
			$x .= '<div class="content_top_tool">
			               		<div class="Conntent_tool">';
			$x .= $linterface->GET_LNK_PRINT("javascript:PrintPage();", "", "", "", "", 1);

			if ($StudentID == "") {
				$x .= $linterface->GET_LNK_EXPORT("lesson_summary_report_export.php?FromDate=" . $FromDate . "&ToDate=" . $ToDate . "&ClassID=" . $ClassID, "", "", "", "", 1);
			} else {
				$x .= $linterface->GET_LNK_EXPORT("lesson_summary_report_export.php?FromDate=" . $FromDate . "&ToDate=" . $ToDate . "&ClassID=" . $ClassID . '&tempAttendStatus=' . $tempAttendStatus . '&StudentID=' . implode(',', $StudentID), "", "", "", "", 1);
			}

			$x .= '  </div>
							    <br style="clear: both;">
							  </div>';
			$x .= "</td></tr><table>";
		} else
			if ($Format == "print") {
				$x .= '<table width="95%" align="center" class="print_hide" border=0>
									<tr>
										<td align="right">' . $linterface->GET_BTN($button_print, "button", "javascript:window.print();", "submit2") . '</td>
									</tr>
								   </table>';
				$x.= '<p width=\'96%\' align=\'center\'>Lesson Summary Report (' . $FromDate . ' to ' . $ToDate . ') </p>';
				$x .= "<table class='eSporttableborder' width='96%' border='0' cellpadding='5' cellspacing='0' align='center'>\n";
				$x .= "<tr>";
				$x .= "	<td class='eSporttdborder eSportprinttabletitle'>" . $Lang['SysMgr']['FormClassMapping']['StudentName'] . "</td>";
				if ($tempAttendStatus == 3 || $tempAttendStatus == 1 || $tempAttendStatus == 0) {
					$x .= "	<td class='eSporttdborder eSportprinttabletitle'>" . $Lang['LessonAttendance']['Late'] . "</td>";
					$x .= "	<td class='eSporttdborder eSportprinttabletitle'>" . $Lang['LessonAttendance']['CountAsDay'] . "</td>";
				}
				if ($tempAttendStatus == 3 || $tempAttendStatus == 2 || $tempAttendStatus == 0) {
					$x .= "	<td class='eSporttdborder eSportprinttabletitle'>" . $Lang['LessonAttendance']['Absent'] . "</td>";
					$x .= "	<td class='eSporttdborder eSportprinttabletitle'>" . $Lang['LessonAttendance']['CountAsDay'] . "</td>";
				}
				$x .= "</tr>";

				$noRecord = 0;
				if (sizeof($StudentStat) > 0) {
					foreach ($StudentStat as $key => $val) {
						$val['2'] += 0;
						$val['3'] += 0;
						//Henry: attendance checking filtering checking
						if (($tempAttendStatus == 3 && ($val['2'] > 0 || $val['3'] > 0)) || ($tempAttendStatus == 1 && $val['2'] > 0) || ($tempAttendStatus == 2 && $val['3'] > 0) || $tempAttendStatus == 0) {
							$x .= '<tr>';
							$x .= '<td class="eSporttdborder eSportprinttext">' . $val['Name'] . '</td>';
							//					if($val['2'] > 0){
							//						$x .= '<td class="eSporttdborder eSportprinttext"><a href="lesson_summary_report_detail.php?class='.$this->getClassName($ClassID).'&studentID='.$key.'&filter=2" onClick="javascript:window.open(\'lesson_summary_report_detail.php?class='.$this->getClassName($ClassID).'&studentID='.$key.'&filter=2\');return false;">'.$val['2'].'/'.$val['Total'].'</a></td>';
							//					}
							//					else

							if (($tempAttendStatus == 3 && ($val['2'] > 0 || $val['3'] > 0)) || ($tempAttendStatus == 1 && $val['2'] > 0) || $tempAttendStatus == 0) {
								$x .= '<td class="eSporttdborder eSportprinttext">' . $val['2'] . '/' . $val['Total'] . '</td>';
								if ($val['2'] > 0) {
									$x .= '<td class="eSporttdborder eSportprinttext">' . round(($val['2'] / $LessonSessionLateCountAsDay), 2) . '</td>';
								} else {
									$x .= '<td class="eSporttdborder eSportprinttext">' . $val['2'] . '</td>';
								}
							}
							//					if($val['3'] > 0){
							//						$x .= '<td class="eSporttdborder eSportprinttext"><a href="lesson_summary_report_detail.php?class='.$this->getClassName($ClassID).'&studentID='.$key.'&filter=3" onClick="javascript:window.open(\'lesson_summary_report_detail.php?class='.$this->getClassName($ClassID).'&studentID='.$key.'&filter=3\');return false;">'.$val['3'].'/'.$val['Total'].'</a></td>';
							//					}
							//					else
							if (($tempAttendStatus == 3 && ($val['2'] > 0 || $val['3'] > 0)) || ($tempAttendStatus == 2 && $val['3'] > 0) || $tempAttendStatus == 0) {
								$x .= '<td class="eSporttdborder eSportprinttext">' . $val['3'] . '/' . $val['Total'] . '</td>';
								if ($val['3'] > 0) {
									$x .= '<td class="eSporttdborder eSportprinttext">' . round(($val['3'] / $LessonSessionCountAsDay), 2) . '</td>';
								} else {
									$x .= '<td class="eSporttdborder eSportprinttext">' . $val['3'] . '</td>';
								}
							}
							$x .= '</tr>';
						} else {
							$noRecord++;
						}
					}
				} else {
					$x .= '<tr>';
					$x .= '<td class="eSporttdborder eSportprinttext" colspan="5" align="center">' . $Lang['General']['NoRecordFound'] . '</td>';
					$x .= '</tr>';
				}
				if ($noRecord == sizeof($StudentStat) && sizeof($StudentStat) > 0) {
					$x .= '<tr>';
					$x .= '<td class="eSporttdborder eSportprinttext" colspan="5" align="center">' . $Lang['General']['NoRecordFound'] . '</td>';
					$x .= '</tr>';
				}
				$x .= '</table>';
			}

		if ($Format == "export") {
			include_once ("libexporttext.php");
			$lexport = new libexporttext();

			$Rows = array ();
			$Details = array ();
			$ExportColumn = array ();

			$ExportColumn[] = $Lang['SysMgr']['FormClassMapping']['StudentName'];
			if ($tempAttendStatus == 3 || $tempAttendStatus == 1 || $tempAttendStatus == 0) {
				$ExportColumn[] = $Lang['LessonAttendance']['Late'];
				$ExportColumn[] = $Lang['LessonAttendance']['CountAsDay'];
			}
			if ($tempAttendStatus == 3 || $tempAttendStatus == 2 || $tempAttendStatus == 0) {
				$ExportColumn[] = $Lang['LessonAttendance']['Absent'];
				$ExportColumn[] = $Lang['LessonAttendance']['CountAsDay'];
			}
			if (sizeof($StudentStat) > 0) {
				$noRecord = 0;
				foreach ($StudentStat as $key => $val) {
					unset ($Details);
					$Details = array ();
					$val['2'] += 0;
					$val['3'] += 0;
					if (($tempAttendStatus == 3 && ($val['2'] > 0 || $val['3'] > 0)) || ($tempAttendStatus == 1 && $val['2'] > 0) || ($tempAttendStatus == 2 && $val['3'] > 0) || $tempAttendStatus == 0) {
						$Details[] = $val['Name'];
						if (($tempAttendStatus == 3 && ($val['2'] > 0 || $val['3'] > 0)) || ($tempAttendStatus == 1 && $val['2'] > 0) || $tempAttendStatus == 0) {
							$Details[] = $val['2'] . '/' . $val['Total'];
							if ($val['2'] > 0) {
								$Details[] = round(($val['2'] / $LessonSessionLateCountAsDay), 2);
							} else {
								$Details[] = $val['2'];
							}
						}
						if (($tempAttendStatus == 3 && ($val['2'] > 0 || $val['3'] > 0)) || ($tempAttendStatus == 2 && $val['3'] > 0) || $tempAttendStatus == 0) {
							$Details[] = $val['3'] . '/' . $val['Total'];
							if ($val['3'] > 0) {
								$Details[] = round(($val['3'] / $LessonSessionCountAsDay), 2);
							} else {
								$Details[] = $val['3'];
							}
						}
						$Rows[] = $Details;
					} else {
						$noRecord++;
					}

				}
			}
			if ($noRecord == sizeof($StudentStat) && sizeof($StudentStat) > 0) {
				$Details[] = $Lang['General']['NoRecordFound'];
				for ($i = 1; $i <= 4; $i++) {
					$Details[] = '';
				}
				$Rows[] = $Details;
			}

			$exportContent = $lexport->GET_EXPORT_TXT($Rows, $ExportColumn);
			$lexport->EXPORT_FILE('lesson_summary_report_' . $FromDate . '_to_' . $ToDate . '.csv', $exportContent);

			return;
		}
		if ($Format == "web") {
			$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
			$x .= "<tr class=\"tabletop\">";
			$x .= "	<td>" . $Lang['SysMgr']['FormClassMapping']['StudentName'] . "</td>";
			if ($tempAttendStatus == 3 || $tempAttendStatus == 1 || $tempAttendStatus == 0) {
				$x .= "	<td>" . $Lang['LessonAttendance']['Late'] . "</td>";
				$x .= "	<td>" . $Lang['LessonAttendance']['CountAsDay'] . "</td>";
			}
			if ($tempAttendStatus == 3 || $tempAttendStatus == 2 || $tempAttendStatus == 0) {
				$x .= "	<td>" . $Lang['LessonAttendance']['Absent'] . "</td>";
				$x .= "	<td>" . $Lang['LessonAttendance']['CountAsDay'] . "</td>";
			}
			$x .= "</tr>";

			$noRecord = 0;
			if (sizeof($StudentStat) > 0) {
				$i = 0;
				foreach ($StudentStat as $key => $val) {
					//$css = ($i++ % 2 ? "2" : "");
					$val['2'] += 0;
					$val['3'] += 0;
					//Henry: attendance checking filtering checking
					if (($tempAttendStatus == 3 && ($val['2'] > 0 || $val['3'] > 0)) || ($tempAttendStatus == 1 && $val['2'] > 0) || ($tempAttendStatus == 2 && $val['3'] > 0) || $tempAttendStatus == 0) {
						$css = ($i++ % 2 ? "2" : "");
						$x .= '<tr class="tablerow'.$css.'">';
						$x .= '<td class="tabletext">' . $val['Name'] . '</td>';
						if (($tempAttendStatus == 3 && ($val['2'] > 0 || $val['3'] > 0)) || ($tempAttendStatus == 1 && $val['2'] > 0) || $tempAttendStatus == 0) {
							if ($val['2'] > 0) {
								$x .= '<td class="tabletext"><a href="lesson_summary_report_detail.php?class=%3A%3A' . $ClassID . '&StudentList[]=' . $key . '&FromDate='.$FromDate.'&ToDate='.$ToDate.'&filter=2" onClick="javascript:window.open(\'lesson_summary_report_detail.php?class=%3A%3A' . $ClassID . '&StudentList[]=' . $key . '&FromDate='.$FromDate.'&ToDate='.$ToDate.'&filter=2\');return false;">' . $val['2'] . '/' . $val['Total'] . '</a></td>';
							} else
								$x .= '<td class="tabletext">' . $val['2'] . '/' . $val['Total'] . '</td>';
							if ($val['2'] > 0) {
								$x .= '<td class="tabletext">' . round(($val['2'] / $LessonSessionLateCountAsDay), 2) . '</td>';
							} else {
								$x .= '<td class="tabletext">' . $val['2'] . '</td>';
							}
						}
						if (($tempAttendStatus == 3 && ($val['2'] > 0 || $val['3'] > 0)) || ($tempAttendStatus == 2 && $val['3'] > 0) || $tempAttendStatus == 0) {
							if ($val['3'] > 0) {
								$x .= '<td class="tabletext"><a href="lesson_summary_report_detail.php?class=%3A%3A' . $ClassID . '&StudentList[]=' . $key . '&FromDate='.$FromDate.'&ToDate='.$ToDate.'&filter=3" onClick="javascript:window.open(\'lesson_summary_report_detail.php?class=%3A%3A' . $ClassID . '&StudentList[]=' . $key . '&FromDate='.$FromDate.'&ToDate='.$ToDate.'&filter=3\');return false;">' . $val['3'] . '/' . $val['Total'] . '</a></td>';
							} else
								$x .= '<td class="tabletext">' . $val['3'] . '/' . $val['Total'] . '</td>';
							if ($val['3'] > 0) {
								$x .= '<td class="tabletext">' . round(($val['3'] / $LessonSessionCountAsDay), 2) . '</td>';
							} else {
								$x .= '<td class="tabletext">' . $val['3'] . '</td>';
							}
						}
						$x .= '</tr>';
					} else {
						$noRecord++;
					}
				}
			} else {
				$x .= '<tr class="tablerow2">';
				$x .= '<td class="tabletext" colspan="5" align="center">' . $Lang['General']['NoRecordFound'] . '</td>';
				$x .= '</tr>';
			}
			if ($noRecord == sizeof($StudentStat) && sizeof($StudentStat) > 0) {
				$x .= '<tr class="tablerow2">';
				$x .= '<td class="tabletext" colspan="5" align="center">' . $Lang['General']['NoRecordFound'] . '</td>';
				$x .= '</tr>';
			}
			$x .= '</table>';
			
			$x .= '<table width="95%" cellspacing="0" cellpadding="5" border="0" align="center">';
				$x .= '<tbody><tr><td class="tabletextremark">'.$Lang['LessonAttendance']['LessonSummaryReportRemark'].'</td></tr></tbody>';
			$x .= '</table>'."\n";
			
			$x .= '<script type="text/JavaScript" language="JavaScript">';
			if ($StudentID == "") {
				$x .= 'function PrintPage()
									   {
											url="lesson_summary_report_print.php?FromDate=' . $FromDate . '&ToDate=' . $ToDate . '&ClassID=' . $ClassID . '&tempAttendStatus=' . $tempAttendStatus . '"; 
											newWindow(url,10);
									   }';
				$x .= '</script>';
			} else {
				$x .= 'function PrintPage()
									   {
											url="lesson_summary_report_print.php?FromDate=' . $FromDate . '&ToDate=' . $ToDate . '&ClassID=' . $ClassID . '&tempAttendStatus=' . $tempAttendStatus . '&StudentID=' . implode(',', $StudentID) . '"; 
											newWindow(url,10);
									   }';
				$x .= '</script>';
			}
		}

		return $x;
	}

	function Get_Entry_Leave_Date_Index() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $image_path;

		$x .= $this->Include_JS_CSS();
		$linterface = new interface_html();
		$x .= '<br>
						<div id="EntryLeaveDateDiv">
							<div class="content_top_tool" id="SearchInputLayer" style="width:96%;">
								<div class="Conntent_tool">
									<a href="import.php" class="import"> ' . $Lang['Btn']['Import'] . ' </a>
									<a href="export.php" class="export"> ' . $Lang['Btn']['Export'] . ' </a>
								</div>
			          			<div class="Conntent_search">
			          				<!--<input name="Keyword" id="Keyword" type="text" onkeyup="Check_Go_Search(event);"/>-->
			          			</div>
		          				<br style="clear:both" />
		         			</div>
		          			<div class="table_board" id="UserListLayer" style="text-align:left; width:96%; margin-bottom: 3px;">
								' . $this->getSelectClassID('id="ClassID" name="ClassID"', "") . '&nbsp;
								' . $linterface->GET_SMALL_BTN($Lang['Btn']['View'], "button", "View_Entry_Leave_Date();") . '&nbsp;
								' . $linterface->GET_SMALL_BTN($Lang['StudentAttendance']['RemoveAllEntryLeaveDates'], "button", "Remove_All_EntryLeaveDates();") . '
							</div>
							<div id="EntryLeaveTableLayer" style="width:96%;">
							</div>
						</div>';
		return $x;
	}

	function Get_Entry_Leave_Date_Table($ClassID = "") {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

		$StudAttend = new libcardstudentattend2();
		$UserList = $StudAttend->Get_Entry_Leave_Student_List($ClassID);

		$x = '<table class="common_table_list">
		              <tr>
		              	<th>#</th>
		                <th> ' . $Lang['StudentAttendance']['StudentName'] . ' </th>
		                <th> ' . $Lang['StudentAttendance']['EntryDate'] . '</th>
		                <th> ' . $Lang['StudentAttendance']['LeaveDate'] . '</th>
		                <th>
		                	&nbsp;
		                </th>
		              </tr>
		              <col />
		              <col />
		              <col />
		              <tbody>';
		for ($i = 0; $i < sizeof($UserList); $i++) {
			$EntryLeaveList = $StudAttend->Get_Entry_Leave_List($UserList[$i]['UserID']);
			$x .= '<tr>
										<td rowspan="' . (sizeof($EntryLeaveList) + 2) . '">' . ($i +1) . '</td>
				            <td rowspan="' . (sizeof($EntryLeaveList) + 2) . '">' . $UserList[$i]['StudentName'] . '</td>
				            <td colspan="3" style="display:none;">&nbsp;</td>
				          </tr>';
			for ($j = 0; $j < sizeof($EntryLeaveList); $j++) {
				$x .= '<tr class="sub_row">
				                  <td>' . (($EntryLeaveList[$j]['PeriodStart'] == "1970-01-01") ? '--' : $EntryLeaveList[$j]['PeriodStart']) . '</td>
				                  <td>' . (($EntryLeaveList[$j]['PeriodEnd'] == "2099-12-31" || $EntryLeaveList[$j]['PeriodEnd'] == "2037-12-31") ? '--' : $EntryLeaveList[$j]['PeriodEnd']) . '</td>
				                  <td>
				                  	<div class="table_row_tool">
					                  	<a onclick="Get_Entry_Leave_Form(\'' . $UserList[$i]['UserID'] . '\',\'' . $EntryLeaveList[$j]['RecordID'] . '\');" href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox edit_dim" title="' . $Lang['Btn']['Edit'] . '"></a>
											        <a href="#" class="delete_dim" title="' . $Lang['Btn']['Delete'] . '" onclick="Delete_Entry_Period(\'' . $EntryLeaveList[$j]['RecordID'] . '\'); return false;"></a>';
				$x .= '     </div>
				                  </td>
				                </tr>';
			}
			$x .= '<tr>
				            <td colspan="2">&nbsp;</td>
				            <td>
				    				   <span class="table_row_tool row_content_tool" style="float:right;">
													<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Entry_Leave_Form(\'' . $UserList[$i]['UserID'] . '\');" class="add_dim thickbox" title="' . $Lang['Btn']['Add'] . '"></a>
												</span>';
			$x .= '	</td>
				          </tr>';
		}

		$x .= '</tbody>
		        		</table> ';

		return $x;
	}

	function Get_Entry_Leave_Date_Form($StudentID, $RecordID) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

		$StudAttend = new libcardstudentattend2();
		$linterface = new interface_html();
		if ($RecordID != "") {
			$PeriodDetail = $StudAttend->Get_Entry_Leave_Detail($RecordID);
		} else {
			$PeriodDetail = $StudAttend->Get_Entry_Leave_Default_Detail($StudentID);
		}
		list ($RecordID, $PeriodStart, $PeriodEnd) = $PeriodDetail[0];

		$PeriodStart = ($PeriodStart == "1970-01-01") ? " " : $PeriodStart;
		$PeriodEnd = ($PeriodEnd == "2099-12-31") ? " " : $PeriodEnd;
		$x .= '<div class="edit_pop_board" style="height:400px;">
							<div class="edit_pop_board_write" style="height:374px;">
							<form name="EntryLeaveForm" id="EntryLeaveForm" onsubmit="return false;">
							<input type="hidden" name="RecordID" id="RecordID" value="' . $RecordID . '">
							<input type="hidden" name="StudentID" id="StudentID" value="' . $StudentID . '">
							
							<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
									<table class="form_table">
										<tr>
											<td>' . $Lang['StudentAttendance']['EntryDate'] . '</td>
											<td>:</td>
											<td>
												' . $linterface->GET_DATE_PICKER('PeriodStart', $PeriodStart) . '
											</td> 
										</tr>
										<tr id="GroupNameWarningRow" name="GroupNameWarningRow" style="display:none;">
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td><div id="GroupNameWarningLayer" style="color:red;"></div></td> 
										</tr>
										<col class="field_title" />
										<col class="field_c" />
										<tr>
											<td>' . $Lang['StudentAttendance']['LeaveDate'] . '</td>
											<td>:</td>
											<td>
												' . $linterface->GET_DATE_PICKER('PeriodEnd', $PeriodEnd) . '
											</td> 
										</tr>
										<tr id="PeriodWarningRow" name="PeriodWarningRow" style="display:none;">
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td><div id="PeriodWarningLayer" style="color:red;"></div></td> 
										</tr>
									</table>
								</div>
								
								
							</form>
							</div>
							<div class="edit_bottom" style="height:10px;">
								<p class="spacer"></p>
								<input name="submit2" type="button" class="formbutton" onclick="Check_Entry_Leave_Period();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Submit'] . '" />
								<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />
								<!--<p class="spacer"></p>-->
							</div>
						</div>';
		return $x;
	}

	function Get_Entry_Period_Import_Form() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

		$linterface = new interface_html();
		$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import']
		);

		# step information
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['SelectCSVFile'],
			1
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['CSVConfirmation'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['ImportResult'],
			0
		);

		$x .= '<br />
						<form name="form1" method="POST" action="import_confirm.php" enctype="multipart/form-data">
						<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>' . $linterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
							</tr>
							<tr>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td>' . $linterface->GET_STEPS($STEPS_OBJ) . '</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						<table>
						<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
									' . $Lang['Btn']['Select'] . '
								</td>
								<td width="70%"class="tabletext">
									<input type="file" class="file" name="userfile"><br />
								</td>
							</tr>
							<tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
								<td width="70%"class="tabletextremark">
									' . $Lang['StudentAttendance']['EntryPeriodImportDesc'] . '<br /><br />
									<a class="tablelink" href="' . GET_CSV("sample.csv") . '" target="_blank">' . $Lang['General']['ClickHereToDownloadSample'] . '</a>
								</td>
							</tr>
						</table>
						<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr>
						    	<td class="dotline"><img src="' . $PATH_WRT_ROOT . "images/" . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
						    </tr>
						</table>
						<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr>
							    <td align="center" colspan="2">
									' . $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . '
									' . $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location = 'index.php';", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . '
								</td>
							</tr>
						</table>
						</form>
						<br />';

		return $x;
	}

	function Get_Entry_Period_Import_Confirm_Page($data) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

		$linterface = new interface_html();

		$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import']
		);

		# step information
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['SelectCSVFile'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['CSVConfirmation'],
			1
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['ImportResult'],
			0
		);

		$x = '<br/>
						<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>' . $linterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
							</tr>
							<tr>
								<td>' . $linterface->GET_STEPS($STEPS_OBJ) . '</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						<table width="90%" border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['TotalRecord'] . '</td>
								<td class="tabletext">' . sizeof($data) . '</td>
							</tr>';
		if (sizeof($data) > 0) {
			$ErrorArray = $this->Import_Entry_Period_Info($data);
			$x .= '
								<tr>
									<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['SuccessfulRecord'] . '</td>
									<td class="tabletext">' . (sizeof($data) - sizeof($ErrorArray)) . '</td>
								</tr>
								<tr>
									<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['FailureRecord'] . '</td>
									<td class="tabletext">' . sizeof($ErrorArray) . '</td>
								</tr>
							</table>
							';
			$x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0">
									<tr>
										<td class="tablebluetop tabletopnolink" width="10">#</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['UserLogin'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['EntryDate'] . '</td>
			              <td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['LeaveDate'] . '</td>
			              <td class="tablebluetop tabletopnolink">' . $Lang['General']['Remark'] . '</td>
									</tr>';
			for ($i = 0; $i < sizeof($ErrorArray); $i++) {
				$css_i = ($i % 2) ? "2" : "";
				$x .= '
											<tr>
												<td valign="top" class="tablebluerow' . $css_i . '" width="10">
													' . $ErrorArray[$i]['RowNumber'] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][0] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][1] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][2] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													<font style="color:red;">*' . $ErrorArray[$i]['ErrorMsg'] . '</font>
												</td>
											</tr>';
			}
			$x .= '</table>';

			$x .= '
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td class="dotline"><img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							</tr>
							<tr>
								<td align="center">';
			if (sizeof($ErrorArray) == 0)
				$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "window.location='import_update.php';") . '&nbsp;';

			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
			$x .= '</td>
							</tr>
							</table>';
		} else {
			$x .= '</table>';

			$x .= '
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td class="dotline"><img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							</tr>
							<tr>
								<td align="center">';
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
			$x .= '</td>
							</tr>
							</table>';
		}

		return $x;
	}

	function Get_Entry_Period_Import_Finish_Page() {
		$Result = $this->Finalize_Entry_Period_Info();

		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

		$linterface = new interface_html();
		$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import']
		);

		# step information
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['SelectCSVFile'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['CSVConfirmation'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['ImportResult'],
			1
		);

		$x .= '<br />
							<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>' . $linterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
								</tr>
								<tr>
									<td>' . $linterface->GET_STEPS($STEPS_OBJ) . '</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
									' . $Result . ' ' . $Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'] . '
									</td>
								</tr>
								<tr>
									<td class="dotline">
										<img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" />
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
										' . $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='index.php';") . '
									</td>
								</tr>
							</table>';

		return $x;
	}

	/*
	 * $Classes : Array[i]['ClassName']
	 * $Year : YYYY
	 * $Month : MM
	 * $Columns : Array
	 */
	function Get_Class_Monthly_Attendance_Report($Classes, $Year, $Month, $Columns, $HideNoData, $IsGroup = false, $AcademicYear = "", $StudentStatus = "0,1,2", $PrintVersion = 0, $ContinuousAbsentDay = 0) {
		global $PATH_WRT_ROOT, $Lang, $sys_custom;
		global $i_SmartCard_GroupName, $intranet_session_language;

		include_once ($PATH_WRT_ROOT . "includes/global.php");
		include_once ($PATH_WRT_ROOT . "includes/libdb.php");

		//$attendance_mode = $this->attendance_mode;
		$count_value = $this->ProfileAttendCount == 1 ? 0.5 : 1;
		$absent_count_value = $count_value;
		$present_count_value = $count_value;
		$late_count_value = 1;
		$early_leave_count_value = 1;

		$reason_count_with_absent_session = isset($sys_custom['SmartCardAttendance_ReasonCountAbsentSession']) && $sys_custom['SmartCardAttendance_ReasonCountAbsentSession'];

		
		#set colWidth for attandance data
		if ($intranet_session_language=='en') {
			$colWidth = 40;
		}
		else {
			$colWidth = 30;
		}
		
		# get user customized status symbols
		$sql = "SELECT StatusID, StatusSymbol FROM CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL";
		$SymbolsArr = $this->returnArray($sql);
		$notation_symbol[0] = ($SymbolsArr[0]['StatusSymbol'] == NULL || $SymbolsArr[0]['StatusSymbol'] == "") ? $Lang['StudentAttendance']['SymbolPresent'] : $SymbolsArr[0]['StatusSymbol'];
		$notation_symbol[1] = ($SymbolsArr[1]['StatusSymbol'] == NULL || $SymbolsArr[1]['StatusSymbol'] == "") ? $Lang['StudentAttendance']['SymbolAbsent'] : $SymbolsArr[1]['StatusSymbol'];
		$notation_symbol[2] = ($SymbolsArr[2]['StatusSymbol'] == NULL || $SymbolsArr[2]['StatusSymbol'] == "") ? $this->DefaultAttendanceSymbol[CARD_STATUS_LATE] : $SymbolsArr[2]['StatusSymbol'];
		$notation_symbol[3] = $Lang['StudentAttendance']['SymbolOuting'];
		$notation_symbol[4] = ($SymbolsArr[3]['StatusSymbol'] == NULL || $SymbolsArr[3]['StatusSymbol'] == "") ? $Lang['StudentAttendance']['SymbolEarlyLeave'] : $SymbolsArr[3]['StatusSymbol'];
		$notation_symbol[5] = $Lang['StudentAttendance']['SymbolSL'];
		$notation_symbol[6] = $Lang['StudentAttendance']['SymbolAR'];
		$notation_symbol[7] = $Lang['StudentAttendance']['SymbolLE'];
		$notation_symbol[8] = $Lang['StudentAttendance']['SymbolTruancy'];
		$notation_symbol[9] = ($SymbolsArr[4]['StatusSymbol'] == NULL || $SymbolsArr[4]['StatusSymbol'] == "") ? $Lang['StudentAttendance']['SymbolWaived'] : $SymbolsArr[4]['StatusSymbol'];

		# get reason symbols
		$absent_reason_symbol = $this->Get_Reason_Symbol(PROFILE_TYPE_ABSENT);
		$late_reason_symbol = $this->Get_Reason_Symbol(PROFILE_TYPE_LATE);
		$earlyleave_reason_symbol = $this->Get_Reason_Symbol(PROFILE_TYPE_EARLY);

		$this->retrieveSettings();

		$months = $Lang['StudentAttendance']['MonthShortForm'];

		if ($Year == "") {
			$Year = date('Y');
		}
		if ($Month == "") {
			$Month = date('m');
		}
		if (strlen($Month) == 1) {
			$Month = "0" . $Month;
		}
		$Year = sprintf("%4d", $Year);
		$Month = sprintf("%02d", $Month);

		$IncludeLeftArchivedStudents = strstr($StudentStatus, "3") !== FALSE;

		$content = "";
		for ($z = 0; $z < sizeof($Classes); $z++) {
			$attendance_mode = $this->attendance_mode;
			#improvement hide AM/PM header in AM/PM mode
			if ($attendance_mode == '0' || $attendance_mode == '1' ){
				$Set_rowspan = 1;
				$Set_rowspan2 = 2;
			}
			else{
				$Set_rowspan = 2;
				$Set_rowspan2 = 3;
			}
			if($sys_custom['StudentAttendance']['KIS_AttendanceUseClassGroup']) {
				$YearClass = new year_class();
				$ClassGroupInfo = $YearClass->Get_Class_Group_Info($Classes[$z]['ClassID']);
				$class_attendance_mode = '';
				if(is_array($ClassGroupInfo)) {
					$class_attendance_type = $ClassGroupInfo[0]['AttendanceType'];
				}

				if($class_attendance_type == '1') {
					$attendance_mode = 0;
					$Set_rowspan = 2;
					$Set_rowspan2 = 3;
				} else if($class_attendance_type == '2') {
					$attendance_mode = 1;
					$Set_rowspan = 2;
					$Set_rowspan2 = 3;;
				} else if($class_attendance_type == '3') {
					$attendance_mode = 2;
					$Set_rowspan = 2;
					$Set_rowspan2 = 3;
				} else if($class_attendance_type == '') {
					$Set_rowspan = 2;
					$Set_rowspan2 = 3;
				}
			}

			# Get Student List
			if ($IsGroup) {
				$sql = "SELECT a.UserID, a.ClassNumber, a.ChineseName, a.EnglishName, a.Gender, a.Remark, a.RecordStatus, '0' as IsArchivedUser FROM INTRANET_USER a, INTRANET_GROUP b, INTRANET_USERGROUP c WHERE a.RecordStatus IN (" . IntegerSafe($StudentStatus) . ") AND a.RecordType = 2 AND b.groupid = '" . IntegerSafe($Classes[$z]['ClassID']) . "' AND b.groupid = c.groupid AND c.userid = a.userid ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName";
				$students = $this->returnArray($sql, 5);
			} else {
			//	if ($AcademicYear == "" || ($lstudentattendance_ui->Platform == "IP" && $AcademicYear == Get_Current_Academic_Year_ID())) {
			//		$sql = "SELECT UserID, ClassNumber, ChineseName, EnglishName, Gender, Remark, RecordStatus, '0' as IsArchivedUser FROM INTRANET_USER WHERE ClassName = '" . $Classes[$z]['ClassName'] . "' AND RecordStatus IN (0,1,2) AND RecordType = 2 ORDER BY ClassName, ClassNumber+0, EnglishName";
			//		$students = $this->returnArray($sql, 5);
			//	} else {
					if ($this->Platform == "IP") {
						$students = $this->Get_Student_List_By_Class_Academic_Year($Classes[$z]['ClassID'], $AcademicYear);
					} else { // EJ
						$students = $this->Get_Student_List_By_Class_Academic_Year($Classes[$z]['ClassName'], $AcademicYear);
					}
			//	}
			}
			
			unset ($StudentIDs);
			for ($i = 0; $i < sizeof($students); $i++) {
				$StudentIDs[] = $students[$i]['UserID'];
			}

			$this->createTable_Card_Student_Daily_Log($Year, $Month);

			$sql = "SELECT 
										DISTINCT a.DayNumber 
									FROM 
										CARD_STUDENT_DAILY_LOG_" . $Year . "_" . $Month . " AS a 
										LEFT OUTER JOIN 
										INTRANET_USER AS b ON (a.UserID=b.UserID) 
										LEFT OUTER JOIN 
										INTRANET_ARCHIVE_USER AS au ON au.UserID = a.UserID 
									WHERE 
										(b.RecordType=2 OR au.RecordType = 2)  
										AND 
										a.UserID in (" . ((sizeof($StudentIDs) > 0) ? implode(',', IntegerSafe($StudentIDs)) : '-1') . ")
										AND (b.RecordStatus IN (" . IntegerSafe($StudentStatus) . ") OR au.UserID IS NOT NULL)";
			$total_day = $this->returnArray($sql, 1);
			$day_numbers_ary = Get_Array_By_Key($total_day,'DayNumber'); // school days array
			
			// ###### Prepare preset absence records that after today ######
			$tomorrow_day_number = date("d", time() + 86400);
			$month_total_days = date("t", mktime(0, 0, 0, intval($Month), 1, intval($Year)));
			$tmp_preset_absent_records = $this->Get_Preset_Absent_Student_Records($StudentIDs, sprintf("%4d-%02d-%02d", $Year, $Month, $tomorrow_day_number), sprintf("%4d-%02d-%02d", $Year, $Month, $month_total_days));
			$preset_absent_data = array ();
			for ($i = 0; $i < count($tmp_preset_absent_records); $i++) {
				$tmp_student_id = $tmp_preset_absent_records[$i]['StudentID'];
				$tmp_day = date("j", strtotime($tmp_preset_absent_records[$i]['RecordDate']));
				$tmp_day_type = $tmp_preset_absent_records[$i]['DayPeriod'];
				$tmp_waive = $tmp_preset_absent_records[$i]['Waive'];
				$tmp_reason = $tmp_preset_absent_records[$i]['Reason'];
				if (in_array_col($tmp_day, $total_day, 'DayNumber') === false) {
					$total_day[] = array (
						'DayNumber' => $tmp_day
					);
				}
				if (!isset ($preset_absent_data[$tmp_student_id])) {
					$preset_absent_data[$tmp_student_id] = array ();
				}
				if (!isset ($preset_absent_data[$tmp_student_id][$tmp_day])) {
					$preset_absent_data[$tmp_student_id][$tmp_day] = array ();
				}
				$preset_absent_data[$tmp_student_id][$tmp_day][0] = ($tmp_day_type == PROFILE_DAY_TYPE_AM ? CARD_STATUS_ABSENT : $preset_absent_data[$tmp_student_id][$tmp_day][0]); // AM status
				$preset_absent_data[$tmp_student_id][$tmp_day][1] = ($tmp_day_type == PROFILE_DAY_TYPE_PM ? CARD_STATUS_ABSENT : $preset_absent_data[$tmp_student_id][$tmp_day][1]); // PM status
				$preset_absent_data[$tmp_student_id][$tmp_day][2] = ''; // leave status
				$preset_absent_data[$tmp_student_id][$tmp_day][3] = ''; // AM late waive
				$preset_absent_data[$tmp_student_id][$tmp_day][4] = ''; // PM late waive
				$preset_absent_data[$tmp_student_id][$tmp_day][5] = ($tmp_day_type == PROFILE_DAY_TYPE_AM ? $tmp_waive : $preset_absent_data[$tmp_student_id][$tmp_day][5]); // AM absent waive
				$preset_absent_data[$tmp_student_id][$tmp_day][6] = ($tmp_day_type == PROFILE_DAY_TYPE_PM ? $tmp_waive : $preset_absent_data[$tmp_student_id][$tmp_day][6]); // PM absent waive
				$preset_absent_data[$tmp_student_id][$tmp_day][7] = ''; // AM early leave waive
				$preset_absent_data[$tmp_student_id][$tmp_day][8] = ''; // PM early leave waive
				$preset_absent_data[$tmp_student_id][$tmp_day][9] = ''; // AM late reason
				$preset_absent_data[$tmp_student_id][$tmp_day][10] = ''; // PM late reason
				$preset_absent_data[$tmp_student_id][$tmp_day][11] = ($tmp_day_type == PROFILE_DAY_TYPE_AM ? $tmp_reason : $preset_absent_data[$tmp_student_id][$tmp_day][11]); // AM absent reason
				$preset_absent_data[$tmp_student_id][$tmp_day][12] = ($tmp_day_type == PROFILE_DAY_TYPE_PM ? $tmp_reason : $preset_absent_data[$tmp_student_id][$tmp_day][12]); // PM absent reason
				$preset_absent_data[$tmp_student_id][$tmp_day][13] = ''; // AM early leave reason
				$preset_absent_data[$tmp_student_id][$tmp_day][14] = ''; // PM early leave reason
			}
			// ###### end of preparing preset absence records that after today ######

			// ###### Get holidays ######
			$holiday_css = 'background-color:#CCCCCC;';
			$tmp_holiday_arr = $this->Get_School_Holiday_Events(date("Y-m-d", mktime(0, 0, 0, intval($Month), 1, intval($Year))), date("Y-m-d", mktime(0, 0, 0, intval($Month), $month_total_days, intval($Year))));
			$holiday_data = array ();
			for ($i = 0; $i < count($tmp_holiday_arr); $i++) {
				$tmp_day = date("j", strtotime($tmp_holiday_arr[$i]['EventDate']));
				if (!isset ($holiday_data[$tmp_day])) {
					$holiday_data[$tmp_day] = array ();
				}
				$holiday_data[$tmp_day][] = $tmp_holiday_arr[$i]['Title'];
			}
			// ###### end of getting holidays #####
			$continuous_absent_css = 'color:red;';

			$count_total_student = 0;
			/*
			if (sizeof($total_day) <= 0) { # No records
				$content .= "<br />";
				$content .= "<table width=\"95%\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
				$content .= "<tr class=\"tablebluetop\"><td class=\"tabletoplink\">" . $Lang['StudentAttendance']['ClassName'] . ": " . $Classes[$z]['ClassName'] . " (" . $Year . " " . $months[$Month -1] . ")</td></tr>";
				$content .= "<tr class=\"tablebluerow2\"><td align=\"center\">" . $Lang['General']['NoRecordFound'] . "</td></tr>";
				$content .= "</table>";
				// 				$content .= "<br />"; 
			} else */
			{
				if ($HideNoData == 1) {
					unset ($DayWithData);
					$DayWithData = array();
					for ($i = 0; $i < sizeof($total_day); $i++) {
						$DayWithData[] = $total_day[$i]['DayNumber'] + 0;
					}
				}
				$day_num = date("t", mktime(0, 0, 0, $Month, 1, $Year));

				$col_span = 3;
				$toprow_colspan = 2;
				if($Columns['Remark']) $toprow_colspan += 1;
				$content .= "<table id=\"contentTable1\" width=\"95%\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\" style=\"border-collapse:collapse;\">";
				$content .= "<tr class=\"tablebluetop\"><td class=\"tabletoplink\" colspan=\"$toprow_colspan\">
														" . (($IsGroup) ? $i_SmartCard_GroupName : $Lang['StudentAttendance']['ClassName']) . ": " . $Classes[$z]['ClassName'] . " (" . $Year . " " . $months[$Month -1] . ")</td></tr>";
				$content .= "<tr>";
				$content .= "<td valign=\"top\">";

				// main content
				$content .= "<table id=\"contentTable2\" width=\"100%\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" style=\"border-collapse:collapse;\">";
				//        menu header

				$non_schoolday_reason_colspan = 0;
				$non_schoolday_reason_colspan_end = 0;
				$content .= "<tr class=\"tablebluetop\" align=\"center\">";
				$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">" . $Lang['StudentAttendance']['ClassNumber'] . "</td>";
				$non_schoolday_reason_colspan++;
				switch ($this->ReportDisplayStudentNameFormat()) {
					case 1 :
						if ($Columns['ChineseName'] == "1") {
							$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['General']['ChineseName'] . "</td>";
							$non_schoolday_reason_colspan++;
						}
						break;
					case 2 :
						if ($Columns['EnglishName'] == "1") {
							$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['General']['EnglishName'] . "</td>";
							$non_schoolday_reason_colspan++;
						}
						break;
					default :
						if ($Columns['ChineseName'] == "1") {
							$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['General']['ChineseName'] . "</td>";
							$non_schoolday_reason_colspan++;
						}
						if ($Columns['EnglishName'] == "1") {
							$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['General']['EnglishName'] . "</td>";
							$non_schoolday_reason_colspan++;
						}
						break;
				}
				if ($Columns['Gender'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['StudentAttendance']['Gender'] . "</td>";
					$non_schoolday_reason_colspan++;
				}
#improvement for KIS showing birth and entry date
				if ($Columns['BirthDate'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['StudentRegistry']['BirthDate'] . "</td>";
					$non_schoolday_reason_colspan++;
				}
				if ($Columns['EntryDate'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['StudentRegistry']['EntryDate'] . "</td>";
					$non_schoolday_reason_colspan++;
				}
				if ($Columns['Data'] == "1") {
					$content .= "<td class=\"tabletoplink\">" . $Lang['General']['Date'] . "</td>";
					$non_schoolday_reason_colspan++;
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						$content .= "<td class=\"tabletoplink\" colspan=\"2\" style=\"width:".$colWidth."px;\">" . $i . "</td>";
					}
				}

				if ($Columns['Schooldays'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"3\">";
					$non_schoolday_reason_colspan_end++;

					$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
					$content .= "<tr>";
					$content .= "<td colspan=\"5\">" . $Lang['StudentAttendance']['NumberOfSchoolDays'] . ":" . count($total_day) . $Lang['StudentAttendance']['Day'] . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['Attend'] . "</td>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['Absent'] . "</td>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['Late'] . "</td>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['EarlyLeave'] . "</td>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['Present'] . "</td>";
					$content .= "</tr>";
					$content .= "</table>";

					$content .= "</td>";
				}

				if ($Columns['ReasonStat'] == "1") {
					# Reason Stat Header
					# Absent Reasons
					if (sizeof($absent_reason_symbol) > 0) {
						$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">";
						$non_schoolday_reason_colspan_end++;
						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						$absent_colspan = sizeof($absent_reason_symbol);
						$content .= "<td width=\"100%\" align=\"center\" valign=\"middle\" nowrap colspan=\"$absent_colspan\">" . $Lang['StudentAttendance']['Absent'] . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$width_percent = 100 / $absent_colspan;
						for ($n = 0; $n < $absent_colspan; $n++) {
							$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $absent_reason_symbol[$n]['Reason'] . "</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";
						$content .= "</td>";
					}
					# Late Reasons
					if (sizeof($late_reason_symbol) > 0) {
						$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">";
						$non_schoolday_reason_colspan_end++;
						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						$late_colspan = sizeof($late_reason_symbol);
						$content .= "<td width=\"100%\" align=\"center\" valign=\"middle\" nowrap colspan=\"$late_colspan\">" . $Lang['StudentAttendance']['Late'] . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$width_percent = 100 / $late_colspan;
						for ($n = 0; $n < $late_colspan; $n++) {
							$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $late_reason_symbol[$n]['Reason'] . "</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";
						$content .= "</td>";
					}
					# EarlyLeave Reasons
					if (sizeof($earlyleave_reason_symbol)) {
						$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">";
						$non_schoolday_reason_colspan_end++;
						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						$earlyleave_colspan = sizeof($earlyleave_reason_symbol);
						$content .= "<td width=\"100%\" align=\"center\" valign=\"middle\" nowrap colspan=\"$earlyleave_colspan\">" . $Lang['StudentAttendance']['EarlyLeave'] . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$width_percent = 100 / $earlyleave_colspan;
						for ($n = 0; $n < $earlyleave_colspan; $n++) {
							$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $earlyleave_reason_symbol[$n]['Reason'] . "</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";
						$content .= "</td>";
					}
					# End of Reason Stat Header
				}

				if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">";
					$non_schoolday_reason_colspan_end++;
					$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
					$content .= "<tr>";
					$content .= "<td width=\"100%\" align=\"center\" valign=\"middle\" nowrap colspan=\"".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?5:4)."\">" . $Lang['StudentAttendance']['AbsentSessions'] . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					$width_percent = 100 / ($sys_custom['SmartCardAttendance_StudentAbsentSession2']?5:4);
					$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $Lang['StudentAttendance']['Late'] . "</td>";
					$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $Lang['StudentAttendance']['RequestLeave'] . "</td>";
					$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $Lang['StudentAttendance']['PlayTruant'] . "</td>";
					$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $Lang['StudentAttendance']['OfficalLeave'] . "</td>";
					if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
						$content .= "<td rowspan=\"2\" width='$width_percent%'>".$Lang['StudentAttendance']['Absent']."</td>";
					}
					$content .= "</tr>";
					$content .= "</table>";
					$content .= "</td>";

				}

				$content .= "</tr>";
				#Weekday header
				$content .= "<tr class=\"tablebluetop\">";
				if ($Columns['Data'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan."\" style=\"text-align:center;\">" . $Lang['StudentAttendance']['Weekday'] . "</td>";
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						$timestamp = mktime(0, 0, 0, $Month, $i, $Year);
						$content .= "<td class=\"tabletoplink\" colspan=\"2\" align=\"center\" style=\"width:$colWidth\">" . $Lang['General']['DayType4'][date("w", $timestamp)] . "</td>";
					}
				}
				$content .= "</tr>";

				$content .= "<tr class=\"tablebluetop\" align=\"center\">";
				
				#AM/PM header
				if ($Columns['Data'] == "1") {
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0" || $attendance_mode == "1") {
							if($sys_custom['StudentAttendance']['KIS_AttendanceUseClassGroup']) {
								$content .= "<td class=\"tabletoplink\" colspan=\"2\"><font size=\"-2\">";
								if ($attendance_mode == "0") {
									$content .= $Lang['StudentAttendance']['AM'];
								} else if($attendance_mode == "1") {
									$content .= $Lang['StudentAttendance']['PM'];
								}
								$content .= "</font></td>";
							}
							// not display AM / PM
						} else {
							$content .= "<td class=\"tabletoplink\"><font size=\"-2\">" . $Lang['StudentAttendance']['AM'] . "</font></td>";
							$content .= "<td class=\"tabletoplink\"><font size=\"-2\">" . $Lang['StudentAttendance']['PM'] . "</font></td>";
						}
						
					}
				}
				$content .= "</tr>";

				if ($Columns['NonSchoolDayReason'] == "1" && $Columns['Data'] == "1") {
					$content .= "<tr class=\"tablebluetop\" align=\"center\">";
					$content .= "<td class=\"tabletoplink\" colspan=\"".$non_schoolday_reason_colspan."\">".$Lang['StudentAttendance']['NonSchoolDayReason']."</td>";
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						$content .= "<td class=\"tabletoplink\" colspan=\"2\">";
						if (isset ($holiday_data[$i]) && count($holiday_data[$i]) > 0) {
							$content .= implode('/', $holiday_data[$i]);
						}
						$content .= "</td>";
					}
					if($non_schoolday_reason_colspan_end > 0) {
						$content .= "<td class=\"tabletoplink\" colspan=\"" . $non_schoolday_reason_colspan_end . "\"></td>";
					}
					$content .= "</tr>";
				}

				for ($i = 1; $i <= $day_num; $i++) {
					if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
						continue;
					}
					#############
					# init month count
					# 0 - Present
					# 1 - Absent
					# 2 - Late
					# 3 - Truancy
					# 4 - EarlyLeave
					# 5 - OnTime
					$new_monthly_report[$i]['0']['am'] = 0;
					$new_monthly_report[$i]['0']['pm'] = 0;
					$new_monthly_report[$i]['1']['am'] = 0;
					$new_monthly_report[$i]['1']['pm'] = 0;
					$new_monthly_report[$i]['2']['am'] = 0;
					$new_monthly_report[$i]['2']['pm'] = 0;
					$new_monthly_report[$i]['4']['am'] = 0;
					$new_monthly_report[$i]['4']['pm'] = 0;
					$new_monthly_report[$i]['5']['am'] = 0;
					$new_monthly_report[$i]['5']['pm'] = 0;

					### init reasons count statistic ###
					for ($k = 0; $k < sizeof($absent_reason_symbol); $k++) {
						### [day index][reason index][am], [day index][reason index][pm]
						$absent_reasons_stat[$i][$k]['am'] = 0;
						$absent_reasons_stat[$i][$k]['pm'] = 0;
					}
					for ($k = 0; $k < sizeof($late_reason_symbol); $k++) {
						### [day index][reason index][am], [day index][reason index][pm]
						$late_reasons_stat[$i][$k]['am'] = 0;
						$late_reasons_stat[$i][$k]['pm'] = 0;
					}
					for ($k = 0; $k < sizeof($earlyleave_reason_symbol); $k++) {
						### [day index][reason index][am], [day index][reason index][pm]
						$earlyleave_reasons_stat[$i][$k]['am'] = 0;
						$earlyleave_reasons_stat[$i][$k]['pm'] = 0;
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
						// session stat init 
						$absent_sessions_stat[$i]['Late']['am'] = 0;
						$absent_sessions_stat[$i]['Late']['pm'] = 0;
						$absent_sessions_stat[$i]['RequestLeave']['am'] = 0;
						$absent_sessions_stat[$i]['RequestLeave']['pm'] = 0;
						$absent_sessions_stat[$i]['OfficalLeave']['am'] = 0;
						$absent_sessions_stat[$i]['OfficalLeave']['pm'] = 0;
						$absent_sessions_stat[$i]['PlayTruant']['am'] = 0;
						$absent_sessions_stat[$i]['PlayTruant']['pm'] = 0;
						$absent_sessions_stat[$i]['Absenteeism']['am'] = 0;
						$absent_sessions_stat[$i]['Absenteeism']['pm'] = 0;
					}
				}

				if ($AcademicYear == "" || ($lstudentattendance_ui->Platform == "IP" && $AcademicYear == Get_Current_Academic_Year_ID())) {
					if ($IsGroup){
						$result = $this->retrieveGroupMonthData($Classes[$z]['ClassID'], $Year, $Month);
					}else{
						$result = $this->retrieveClassMonthData($Classes[$z]['ClassName'], $Year, $Month);
					}
				} else {
					$result = $this->retrieveCustomizedIndividualMonthData($StudentIDs, $Year, $Month);
				}

				// append preset absent data to result set
				if (count($preset_absent_data) > 0) {
					foreach ($preset_absent_data as $tmp_uid => $tmp_data) {
						foreach ($tmp_data as $tmp_day => $tmp_row) {
							if (!isset ($result[$tmp_uid][$tmp_day])) {
								$result[$tmp_uid][$tmp_day] = $tmp_row;
							}
						}
					}
				}

				// ###### prepare continuous absent data ######
				$ContinuousAbsentData = array ();
				if (count($result) > 0 && $ContinuousAbsentDay > 0) {
					foreach ($result as $tmp_uid => $tmp_data) {
						if (!isset ($ContinuousAbsentData[$tmp_uid])) {
							$ContinuousAbsentData[$tmp_uid] = array ();
						}
						$tmp_days = array ();
						for ($i = 1; $i <= $month_total_days; $i++) {
							if (isset ($result[$tmp_uid][$i]) && (($attendance_mode != "1" && $result[$tmp_uid][$i][0] == CARD_STATUS_ABSENT) || ($attendance_mode == "1" && $result[$tmp_uid][$i][0] == "")) && (($attendance_mode != "0" && $result[$tmp_uid][$i][1] == CARD_STATUS_ABSENT) || ($attendance_mode == "0" && $result[$tmp_uid][$i][1] == ""))) {
								$tmp_days[$i] = 1;
							} else if (count($tmp_days) >= 1 && ((!isset($tmp_days[$i-1]) && in_array($i-1, $day_numbers_ary)) || in_array($i,$day_numbers_ary)) ) { // if accumulated cont absence >=1, previous day is school day but not absent, or this day is not absent, reset continuous absent array
								$tmp_days = array ();
							}
							if (count($tmp_days) >= $ContinuousAbsentDay) {
								foreach ($tmp_days as $tmp_key => $tmp_val) {
									$ContinuousAbsentData[$tmp_uid][$tmp_key] = $tmp_val;
								}
							}
						}
					}
				}
				// ###### end of preparing continuous absent data ######
				for ($i = 0; $i < sizeof($students); $i++) {
					list ($sid, $class_num, $cname, $ename, $gender, $remark,$recordstatus, $isArchieved, $classname ,$dob, $doe) = $students[$i];
					$IsLeftOrArchived = $students[$i]['RecordStatus'] == 3 || $students[$i]['IsArchivedUser'] == 1;
					$red_asteriok = $IsLeftOrArchived || $students[$i]['RecordStatus'] == '0' ? '<font color="red">*</font>' : '';
					if ($IsLeftOrArchived && !$IncludeLeftArchivedStudents)
						continue;
					$user_remark = ($IsLeftOrArchived && $remark != '') ? "&nbsp;[" . intranet_htmlspecialchars($remark) . "]" : "";

					$count_total_student++;

					#############
					# init total count
					# 0 - Present
					# 1 - Absent
					# 2 - Late
					# 3 - Truancy
					# 4 - EarlyLeave
					# 5 - OnTime
					$s_stat[0] = 0;
					$s_stat[1] = 0;
					$s_stat[2] = 0;
					$s_stat[3] = 0;
					$s_stat[4] = 0;
					$s_stat[5] = 0;

					# Init individual student reason stat
					$s_stat_late = array ();
					$s_stat_absent = array ();
					$s_stat_earlyleave = array ();
					for ($k = 0; $k < sizeof($late_reason_symbol); $k++) {
						$s_stat_late[$k] = 0;
					}
					for ($k = 0; $k < sizeof($absent_reason_symbol); $k++) {
						$s_stat_absent[$k] = 0;
					}
					for ($k = 0; $k < sizeof($earlyleave_reason_symbol); $k++) {
						$s_stat_earlyleave[$k] = 0;
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$s_stat_absent_sessions['Late'] = 0;
						$s_stat_absent_sessions['RequestLeave'] = 0;
						$s_stat_absent_sessions['OfficalLeave'] = 0;
						$s_stat_absent_sessions['PlayTruant'] = 0;
						$s_stat_absent_sessions['Absenteeism'] = 0;
						/*
						if($Columns['AbsentSession']=="1")
						{
							# init individual student absent sessions stat where $k is the k-th reason
							for($k=0;$k<sizeof($absent_reason_symbol);$k++)
							{
								$s_stat_absent_sessions[$k] = 0;
							}
							$s_stat_absent_sessions_others = 0;
						}
						if($Columns['LateSession']=="1")
						{
							# init individual student late sessions stat where $k is the k-th reason
							for($k=0;$k<sizeof($late_reason_symbol);$k++)
							{
								$s_stat_late_sessions[$k] = 0;
							}
							$s_stat_late_sessions_others = 0;
						}
						*/
					}

#to give empty space if the date is empty					
					$dob_display = substr(trim($dob),0,10);
					if(is_date_empty($dob_display)) {
						$dob_display = "&nbsp;";
					}else{
						$dob_display = $dob_display;
					}
					$doe_display = $doe;
					if(is_date_empty($doe_display)){
						$doe_display = "&nbsp;"; 
					}else{
						$doe_display = $doe_display;
					}

					$css = ($i % 2 ? "2" : "");
					$content .= "<tr align=\"center\" class='tablebluerow$css'>";
					$content .= "<td class=\"tabletext\">" . $class_num . "</td>";
					switch ($this->ReportDisplayStudentNameFormat()) {
						case 1 :
							if ($Columns['ChineseName'] == "1")
								$content .= "<td class=\"tabletext\">" . $red_asteriok . $cname . $user_remark . "</td>";
							break;
						case 2 :
							if ($Columns['EnglishName'] == "1")
								$content .= "<td class=\"tabletext\">" . $red_asteriok . $ename . $user_remark . "</td>";
							break;
						default :
							if ($Columns['ChineseName'] == "1")
								$content .= "<td class=\"tabletext\">" . $red_asteriok . $cname . $user_remark . "</td>";
							if ($Columns['EnglishName'] == "1")
								$content .= "<td class=\"tabletext\">" . $red_asteriok . $ename . $user_remark . "</td>";
							break;
					}

					if ($Columns['Gender'] == "1")
						$content .= "<td class=\"tabletext\">" . $gender . "</td>";
#improvement for KIS showing birth and entry date
					if ($Columns['BirthDate'] == "1")
						$content .= "<td class=\"tabletext\">" . $dob_display . "</td>";
					if ($Columns['EntryDate'] == "1")
						$content .= "<td class=\"tabletext\">" . $doe_display . "</td>";
						
					if ($Columns['Data'] == "1")
						$content .= "<td class=\"tabletext\">&nbsp;</td>";

					for ($j = 1; $j <= $day_num; $j++) {
						if ($HideNoData == 1 && !in_array($j, $DayWithData)) {
							continue;
						}
						if (sizeof($result[$sid][$j]) != 0) {
							list ($am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive, $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason, $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $result[$sid][$j];

							if ($am == 3) // outing = > present
								$am = '0';

							if ($pm == 3) // outing = > present
								$pm = '0';

							$c = "";

							if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
								$s_stat_absent_sessions['Late'] += ($am_late_session + $pm_late_session);
								$s_stat_absent_sessions['RequestLeave'] += ($am_request_leave_session + $pm_request_leave_session);
								$s_stat_absent_sessions['OfficalLeave'] += ($am_offical_leave_session + $pm_offical_leave_session);
								$s_stat_absent_sessions['PlayTruant'] += ($am_play_truant_session + $pm_play_truant_session);
								$s_stat_absent_sessions['Absenteeism'] += $am_absent_session + $pm_absent_session;
								$absent_sessions_stat[$j]['Late']['am'] += $am_late_session;
								$absent_sessions_stat[$j]['Late']['pm'] += $pm_late_session;
								$absent_sessions_stat[$j]['RequestLeave']['am'] += $am_request_leave_session;
								$absent_sessions_stat[$j]['RequestLeave']['pm'] += $pm_request_leave_session;
								$absent_sessions_stat[$j]['OfficalLeave']['am'] += $am_offical_leave_session;
								$absent_sessions_stat[$j]['OfficalLeave']['pm'] += $pm_offical_leave_session;
								$absent_sessions_stat[$j]['PlayTruant']['am'] += $am_play_truant_session;
								$absent_sessions_stat[$j]['PlayTruant']['pm'] += $pm_play_truant_session;
								$absent_sessions_stat[$j]['Absenteeism']['am'] += $am_absent_session;
								$absent_sessions_stat[$j]['Absenteeism']['pm'] += $pm_absent_session;
							}

							if ($am != "" && $attendance_mode != "1") {
								//$content .= "<td>".$notation_symbol[$am]."</td>";
								$c .= $notation_symbol[$am];

								//$s_stat[$am]++; // count
								//$monthly_report[$j][$am] = $monthly_report[$j][$am] + $count_value;

								if ($am == '0') {
									$s_stat[$am] += $present_count_value;
									//$monthly_report[$j][$am] = $monthly_report[$j][$am] + $present_count_value;
									//$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + $present_count_value;
									$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + 1;

									$s_stat[5] += $present_count_value; // on time stat
									$new_monthly_report[$j]['5']['am'] += 1;
								} else
									if ($am == '1') {
										$s_stat[$am] += $absent_count_value;
										//$monthly_report[$j][$am] = $monthly_report[$j][$am] + $absent_count_value;
										//$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + $absent_count_value;
										$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + 1;
										# Find reason symbol
										$symbol_index = -1;
										for ($p = 0; $p < sizeof($absent_reason_symbol); $p++) {
											if ($am_absent_reason == $absent_reason_symbol[$p]['Reason']) {
												$symbol_index = $p;
												break;
											}
										}
										if ($symbol_index != -1) {
											$c = $absent_reason_symbol[$symbol_index]['StatusSymbol'];
											$temp_count_value = 1; // count number of times
											if($reason_count_with_absent_session){
												$temp_count_value = max($am_request_leave_session,$am_offical_leave_session,$am_absent_session);
											}
											$absent_reasons_stat[$j][$symbol_index]['am'] += $temp_count_value;
											$s_stat_absent[$symbol_index] += $reason_count_with_absent_session? $temp_count_value : $absent_count_value;
										}
										# End of find reason symbol
										if ($am_absent_waive == 1) {
											if ($c != "")
												$c .= "<br>";
											$c .= $notation_symbol[9];
											$s_stat[$am] -= $absent_count_value;
											//$monthly_report[$j][$am] = $monthly_report[$j][$am] - $absent_count_value;
											//$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - $absent_count_value;
											$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - 1;
											$s_stat['0'] += $present_count_value;
											//$monthly_report[$j]['0'] = $monthly_report[$j]['0'] + $present_count_value;
											//$new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + $present_count_value;
											$new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + 1;
										}
									} else
										if ($am == '2') {
											$s_stat['0'] += $present_count_value;
											$s_stat[$am] += $late_count_value;
											//$monthly_report[$j]['0'] = $monthly_report[$j]['0'] + $present_count_value;
											//$monthly_report[$j][$am] = $monthly_report[$j][$am] + $late_count_value;
											//$new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + $present_count_value;
											//$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + $late_count_value;
											$new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + 1;
											$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + 1;
											# Find reason symbol
											$symbol_index = -1;
											for ($p = 0; $p < sizeof($late_reason_symbol); $p++) {
												if ($am_late_reason == $late_reason_symbol[$p]['Reason']) {
													$symbol_index = $p;
													break;
												}
											}
											if ($symbol_index != -1) {
												$c = $late_reason_symbol[$symbol_index]['StatusSymbol'];
												$late_reasons_stat[$j][$symbol_index]['am'] += 1;
												$s_stat_late[$symbol_index] += $late_count_value;
											}
											# End of find reason symbol
											if ($am_late_waive == 1) {
												if ($c != "")
													$c .= "<br>";
												$c .= $notation_symbol[9];
												$s_stat[$am] -= $late_count_value;
												//$monthly_report[$j][$am] = $monthly_report[$j][$am] - $late_count_value;
												//$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - $late_count_value;
												$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - 1;
											}
										}
							}
							// eary leave info, added on 2008-03-13
							if ($leave == 1 && $attendance_mode != "1") {
								if ($c != "")
									$c .= "<br>";
								# Find reason symbol
								$symbol_index = -1;
								for ($p = 0; $p < sizeof($earlyleave_reason_symbol); $p++) {
									if ($am_early_leave_reason == $earlyleave_reason_symbol[$p]['Reason']) {
										$symbol_index = $p;
										break;
									}
								}
								if ($symbol_index != -1) {
									$c .= $earlyleave_reason_symbol[$symbol_index]['StatusSymbol'];
									$earlyleave_reasons_stat[$j][$symbol_index]['am'] += 1;
									$s_stat_earlyleave[$symbol_index] += $early_leave_count_value;
								} else {
									$c .= $notation_symbol[4];
								}
								# End of find reason symbol
								# $c.= $notation_symbol[4];
								$s_stat[4] += $early_leave_count_value;
								//$monthly_report[$j][4] = $monthly_report[$j][4] + $early_leave_count_value;
								//$new_monthly_report[$j][4]['am'] = $new_monthly_report[$j][4]['am'] + $early_leave_count_value;
								$new_monthly_report[$j][4]['am'] = $new_monthly_report[$j][4]['am'] + 1;

								if ($am_early_waive == 1) {
									$c .= $notation_symbol[9];
									$s_stat[4] -= $early_leave_count_value;
									$new_monthly_report[$j][4]['am'] = $new_monthly_report[$j][4]['am'] - 1;
								}
							}
							if ($Columns['Data'] == "1") {
								$td_css = "";
								$td_css_arr = array ();
								if (isset ($holiday_data[$j]) && count($holiday_data[$j]) > 0) {
									$td_css_arr[] = $holiday_css;
								}
								if (isset ($ContinuousAbsentData[$sid][$j])) {
									$td_css_arr[] = $continuous_absent_css;
								}
								if (count($td_css_arr) > 0) {
									$td_css = 'style="' . implode("", $td_css_arr) . '"';
								}
								if ($attendance_mode == "0") {
									if ($c != "")
										$content .= "<td class=\"tabletext\" colspan=\"2\" " .
										$td_css . ">$c</td>";
									else
										$content .= "<td class=\"tabletext\" colspan=\"2\" " . $td_css . ">&nbsp;</td>";
								} else
									if ($attendance_mode != "1") {
										if ($c != "")
											$content .= "<td class=\"tabletext\" " .
											$td_css . ">$c</td>";
										else
											$content .= "<td class=\"tabletext\" " . $td_css . ">&nbsp;</td>";
									}
							}

							$d = "";
							if ($pm != "" && $attendance_mode != "0") {
								//$content .= "<td>".$notation_symbol[$pm]."</td>";
								$d .= $notation_symbol[$pm];

								//$s_stat[$pm]++;
								//$monthly_report[$j][$pm] = $monthly_report[$j][$pm] + $count_value;
								if ($pm == '0') {
									$s_stat[$pm] += $present_count_value;
									//$monthly_report[$j][$pm] = $monthly_report[$j][$pm] + $present_count_value;
									//$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + $present_count_value;
									$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + 1;

									$s_stat[5] += $present_count_value; // on time stat
									$new_monthly_report[$j]['5']['pm'] += 1;
								} else
									if ($pm == '1') {
										$s_stat[$pm] += $absent_count_value;
										//$monthly_report[$j][$pm] = $monthly_report[$j][$pm] + $absent_count_value;
										//$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + $absent_count_value;
										$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + 1;
										# Find reason symbol
										$symbol_index = -1;
										for ($p = 0; $p < sizeof($absent_reason_symbol); $p++) {
											if ($pm_absent_reason == $absent_reason_symbol[$p]['Reason']) {
												$symbol_index = $p;
												break;
											}
										}
										if ($symbol_index != -1) {
											$d = $absent_reason_symbol[$symbol_index]['StatusSymbol'];
											$temp_count_value = 1; // count number of times
											if($reason_count_with_absent_session){
												$temp_count_value = max($pm_request_leave_session,$pm_offical_leave_session,$pm_absent_session);
											}
											$absent_reasons_stat[$j][$symbol_index]['pm'] += $temp_count_value;
											$s_stat_absent[$symbol_index] += $reason_count_with_absent_session? $temp_count_value : $absent_count_value;
										}
										# End of find reason symbol
										if ($pm_absent_waive == 1) {
											if ($d != "")
												$d .= "<br>";
											$d .= $notation_symbol[9];
											$s_stat[$pm] -= $absent_count_value;
											//$monthly_report[$j][$pm] = $monthly_report[$j][$pm] - $absent_count_value;
											//$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - $absent_count_value;
											$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - 1;
											$s_stat['0'] += $present_count_value;
											//$monthly_report[$j]['0'] = $monthly_report[$j]['0'] + $present_count_value;
											//$new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + $present_count_value;
											$new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + 1;
										}
									} else
										if ($pm == '2') {
											$s_stat['0'] += $present_count_value;
											$s_stat[$pm] += $late_count_value;
											//$monthly_report[$j]['0'] = $monthly_report[$j]['0'] + $present_count_value;
											//$monthly_report[$j][$pm] = $monthly_report[$j][$pm] + $late_count_value;
											//$new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + $present_count_value;
											//$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + $late_count_value;
											$new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + 1;
											$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + 1;
											# Find reason symbol
											$symbol_index = -1;
											for ($p = 0; $p < sizeof($late_reason_symbol); $p++) {
												if ($pm_late_reason == $late_reason_symbol[$p]['Reason']) {
													$symbol_index = $p;
													break;
												}
											}
											if ($symbol_index != -1) {
												$d = $late_reason_symbol[$symbol_index]['StatusSymbol'];
												$late_reasons_stat[$j][$symbol_index]['pm'] += 1;
												$s_stat_late[$symbol_index] += $late_count_value;
											}
											# End of find reason symbol
											if ($pm_late_waive == 1) {
												if ($d != "")
													$d .= "<br>";
												$d .= $notation_symbol[9];
												$s_stat[$pm] -= $late_count_value;
												//$monthly_report[$j][$pm] = $monthly_report[$j][$pm] - $late_count_value;
												//$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - $late_count_value;
												$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - 1;
											}
										}
							}
							// eary leave info, added on 2008-03-13
							if ($leave == 2 && $attendance_mode != "0") {
								if ($d != "")
									$d .= "<Br>";
								# Find reason symbol
								$symbol_index = -1;
								for ($p = 0; $p < sizeof($earlyleave_reason_symbol); $p++) {
									if ($pm_early_leave_reason == $earlyleave_reason_symbol[$p]['Reason']) {
										$symbol_index = $p;
										break;
									}
								}
								if ($symbol_index != -1) {
									$d .= $earlyleave_reason_symbol[$symbol_index]['StatusSymbol'];
									$earlyleave_reasons_stat[$j][$symbol_index]['pm'] += 1;
									$s_stat_earlyleave[$symbol_index] += $early_leave_count_value;
								} else {
									$d .= $notation_symbol[4];
								}
								# End of find reason symbol
								# $d .= $notation_symbol[4];
								$s_stat[4] += $early_leave_count_value;
								//$monthly_report[$j][4] = $monthly_report[$j][4] + $early_leave_count_value;
								//$new_monthly_report[$j][4]['pm'] = $new_monthly_report[$j][4]['pm'] + $early_leave_count_value;
								$new_monthly_report[$j][4]['pm'] = $new_monthly_report[$j][4]['pm'] + 1;

								if ($pm_early_waive == 1) {
									$d .= $notation_symbol[9];
									$s_stat[4] -= $early_leave_count_value;
									$new_monthly_report[$j][4]['pm'] = $new_monthly_report[$j][4]['pm'] - 1;
								}
							}
							if ($Columns['Data'] == "1") {
								$td_css = "";
								$td_css_arr = array ();
								if (isset ($holiday_data[$j]) && count($holiday_data[$j]) > 0) {
									$td_css_arr[] = $holiday_css;
								}
								if (isset ($ContinuousAbsentData[$sid][$j])) {
									$td_css_arr[] = $continuous_absent_css;
								}
								if (count($td_css_arr) > 0) {
									$td_css = 'style="' . implode("", $td_css_arr) . '"';
								}
								if ($attendance_mode == "1") {
									if ($d != "")
										$content .= "<td class=\"tabletext\" colspan=\"2\" " . $td_css . ">$d</td>";
									else
										$content .= "<td class=\"tabletext\" colspan=\"2\" " . $td_css . ">&nbsp;</td>";
								} else
									if ($attendance_mode != "0") {
										if ($d != "")
											$content .= "<td class=\"tabletext\" " . $td_css . ">$d</td>";
										else
											$content .= "<td class=\"tabletext\" " . $td_css . ">&nbsp;</td>";
									}
							}
						} else {
							$td_css = "";
							$td_css_arr = array ();
							if (isset ($holiday_data[$j]) && count($holiday_data[$j]) > 0) {
								$td_css_arr[] = $holiday_css;
							}
							if (isset ($ContinuousAbsentData[$sid][$j])) {
								$td_css_arr[] = $continuous_absent_css;
							}
							if (count($td_css_arr) > 0) {
								$td_css = 'style="' . implode("", $td_css_arr) . '"';
							}
							if ($Columns['Data'] == "1") {
								if ($attendance_mode == "0" || $attendance_mode == "1")
									$content .= "<td class=\"tabletext\" colspan=\"2\" " . $td_css . ">&nbsp;</td>";
								else
									$content .= "<td class=\"tabletext\" " . $td_css . ">&nbsp;</td><td class=\"tabletext\" " . $td_css . ">&nbsp;</td>";
							}
						}
					}

					if ($Columns['Schooldays'] == "1") {
						$content .= "<td>";

						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						for ($k = 0; $k < sizeof($s_stat); $k++) {
							if ($k == 3)
								continue;
							$percent_width = sizeof($s_stat) - 1 > 0 ? (100 / sizeof($s_stat) - 1) . "%" : '100%';
							$content .= "<td class=\"tabletext\" width='$percent_width'>" . $s_stat[$k] . "</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";

						$content .= "</td>";
					}

					if ($Columns['ReasonStat'] == "1") {
						# Reason Stat
						# Absent Reasons
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td>";
							$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
							$content .= "<tr>";
							$width_percent = 100 / sizeof($s_stat_absent);
							for ($k = 0; $k < sizeof($s_stat_absent); $k++) {
								$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent[$k] . "</td>";
							}
							$content .= "</tr>";
							$content .= "</table>";
							$content .= "</td>";
						}
						# Late Reasons
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td>";
							$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
							$content .= "<tr>";
							$width_percent = 100 / sizeof($s_stat_late);
							for ($k = 0; $k < sizeof($s_stat_late); $k++) {
								$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_late[$k] . "</td>";
							}
							$content .= "</tr>";
							$content .= "</table>";
							$content .= "</td>";
						}
						# EarlyLeave Reasons
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td>";
							$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
							$content .= "<tr>";
							$width_percent = 100 / sizeof($s_stat_earlyleave);
							for ($k = 0; $k < sizeof($s_stat_earlyleave); $k++) {
								$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_earlyleave[$k] . "</td>";
							}
							$content .= "</tr>";
							$content .= "</table>";
							$content .= "</td>";
						}
						# End of Reason Stat
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
						# Show Absent Sessions Stat for each student
						$content .= "<td>";
						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						$width_percent = 100 / ($sys_custom['SmartCardAttendance_StudentAbsentSession2']?5:4);
						$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent_sessions['Late'] . "</td>";
						$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent_sessions['RequestLeave'] . "</td>";
						$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent_sessions['PlayTruant'] . "</td>";
						$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent_sessions['OfficalLeave'] . "</td>";
						if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
							$content .= "<td class=\"tabletext\" width='$width_percent%'>".$s_stat_absent_sessions['Absenteeism']."</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";
						$content .= "</td>";
						/*
							if($Columns['AbsentSession']=="1")
							{
								# Show Absent Sessions Stat for each student
								$content .= "<td>";
								$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
								$content .= "<tr>";
								$width_percent = 100/(sizeof($s_stat_absent)+1);
								for($k=0; $k<sizeof($s_stat_absent); $k++)
								{
									$content .= "<td class=\"tabletext\" width='$width_percent%'>".$s_stat_absent_sessions[$k]."</td>";
								}
								$content .= "<td class=\"tabletext\" width='$width_percent%'>".$s_stat_absent_sessions_others."</td>";
								$content .= "</tr>";
								$content .= "</table>";
								$content .= "</td>";
								# End of showing Absent Sessions Stat for each student
							}
							if($Columns['LateSession']=="1")
							{
								# Show Late Sessions Stat for each student
								$content .= "<td>";
								$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
								$content .= "<tr>";
								$width_percent = 100/(sizeof($s_stat_late)+1);
								for($k=0; $k<sizeof($s_stat_late); $k++)
								{
									$content .= "<td class=\"tabletext\" width='$width_percent%'>".$s_stat_late_sessions[$k]."</td>";
								}
								$content .= "<td class=\"tabletext\" width='$width_percent%'>".$s_stat_late_sessions_others."</td>";
								$content .= "</tr>";
								$content .= "</table>";
								$content .= "</td>";
								# End of showing Late Sessions Stat for each student
							}
							*/
					}

					$content .= "</tr>";
				}

				### Daily Statistic ###
				if ($Columns['DailyStat'] == "1" && $Columns['Data'] == "1") {
					$col_span = 6;
					if ($Columns['ChineseName'] != "1")
						$col_span -= 1;
					if ($Columns['EnglishName'] != "1")
						$col_span -= 1;
					if ($Columns['Gender'] != "1")
						$col_span -= 1;
					if ($Columns['BirthDate'] != "1")
						$col_span -= 1;
					if ($Columns['EntryDate'] != "1")
						$col_span -= 1;

					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"5\" colspan=\"$col_span\" align=\"right\" valign=\"top\">" . $Lang['StudentAttendance']['DailyStatistic'] . "</td>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfPresent'] . "</td>";
					$totalPresent = 0;
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['0']."</td>";
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['0']['am'] . "</td>";
							$totalPresent = $totalPresent + $new_monthly_report[$i]['0']['am'];
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['0']['pm'] . "</td>";
								$totalPresent = $totalPresent + $new_monthly_report[$i]['0']['pm'];
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['0']['am'] . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['0']['pm'] . "</td>";
								$totalPresent = $totalPresent + $new_monthly_report[$i]['0']['am'] + $new_monthly_report[$i]['0']['pm'];
							}
					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";

					// Row of On Time Daily Stat
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfOnTime'] . "</td>";
					$totalOnTime = 0;
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['1']."</td>";
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['5']['am'] . "</td>";
							$totalOnTime = $totalOnTime + $new_monthly_report[$i]['5']['am'];
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['5']['pm'] . "</td>";
								$totalOnTime = $totalOnTime + $new_monthly_report[$i]['5']['pm'];
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['5']['am'] . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['5']['pm'] . "</td>";
								$totalOnTime = $totalOnTime + $new_monthly_report[$i]['5']['am'] + $new_monthly_report[$i]['5']['pm'];
							}
					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					// End row of On Time Daily Stat

					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfAbsent'] . "</td>";
					$totalAbsent = 0;
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['1']."</td>";
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['1']['am'] . "</td>";
							$totalAbsent = $totalAbsent + $new_monthly_report[$i]['1']['am'];
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['1']['pm'] . "</td>";
								$totalAbsent = $totalAbsent + $new_monthly_report[$i]['1']['pm'];
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['1']['am'] . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['1']['pm'] . "</td>";
								$totalAbsent = $totalAbsent + $new_monthly_report[$i]['1']['am'] + $new_monthly_report[$i]['1']['pm'];
							}
					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";

					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfLate'] . "</td>";
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['2']."</td>";
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['2']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['2']['pm'] . "</td>";
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['2']['am'] . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['2']['pm'] . "</td>";
							}
					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";

					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfEarlyLeave'] . "</td>";
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['4']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$i]['4']['pm'] . "</td>";
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['4']['am'] . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . $new_monthly_report[$i]['4']['pm'] . "</td>";
							}
					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
				}
				### End of Daily Statistic ###
				### Reasons Daily Statistic ###
				if ($Columns['ReasonStat'] == "1" && $Columns['Data'] == "1") {
					$col_span = 6;
					if ($Columns['ChineseName'] != "1")
						$col_span -= 1;
					if ($Columns['EnglishName'] != "1")
						$col_span -= 1;
					if ($Columns['Gender'] != "1")
						$col_span -= 1;
					if ($Columns['BirthDate'] != "1")
						$col_span -= 1;
					if ($Columns['EntryDate'] != "1")
						$col_span -= 1;
					$row_span = sizeof($absent_reason_symbol);
					if ($row_span > 0) {
						$temp = $row_span +1;
						$content .= "<tr>";
						$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"$temp\" colspan=\"$col_span\" align=\"right\" valign=\"top\">" . $Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat'] . "(" . $Lang['StudentAttendance']['Absent'] . ")</td>";
						for ($k = 0; $k < $row_span; $k++) {
							$content .= "<tr>";
							$content .= "<td class=\"tablebluetop tabletoplink\">" . $absent_reason_symbol[$k]['Reason'] . "</td>";
							for ($i = 1; $i <= $day_num; $i++) {
								if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
									continue;
								}
								if ($attendance_mode == "0") {
									$content .= "<td align=\"center\" colspan=\"2\">" . $absent_reasons_stat[$i][$k]['am'] . "</td>";
								} else
									if ($attendance_mode == "1") {
										$content .= "<td align=\"center\" colspan=\"2\">" . $absent_reasons_stat[$i][$k]['pm'] . "</td>";
									} else {
										$content .= "<td align=\"center\">" . $absent_reasons_stat[$i][$k]['am'] . "</td>";
										$content .= "<td align=\"center\">" . $absent_reasons_stat[$i][$k]['pm'] . "</td>";
									}
							}
							// dummy column insert
							if ($Columns['Schooldays'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if ($Columns['ReasonStat'] == "1") {
								if (sizeof($absent_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($late_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($earlyleave_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
							}
							if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							$content .= "</tr>";
						}
						$content .= "</tr>";
					}

					$row_span = sizeof($late_reason_symbol);
					if ($row_span > 0) {
						$temp = $row_span +1;
						$content .= "<tr>";
						$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"$temp\" colspan=\"$col_span\" align=\"right\" valign=\"top\">" . $Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat'] . "(" . $Lang['StudentAttendance']['Late'] . ")</td>";
						for ($k = 0; $k < $row_span; $k++) {
							$content .= "<tr>";
							$content .= "<td class=\"tablebluetop tabletoplink\">" . $late_reason_symbol[$k]['Reason'] . "</td>";
							for ($i = 1; $i <= $day_num; $i++) {
								if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
									continue;
								}
								if ($attendance_mode == "0") {
									$content .= "<td align=\"center\" colspan=\"2\">" . $late_reasons_stat[$i][$k]['am'] . "</td>";
								} else
									if ($attendance_mode == "1") {
										$content .= "<td align=\"center\" colspan=\"2\">" . $late_reasons_stat[$i][$k]['pm'] . "</td>";
									} else {
										$content .= "<td align=\"center\">" . $late_reasons_stat[$i][$k]['am'] . "</td>";
										$content .= "<td align=\"center\">" . $late_reasons_stat[$i][$k]['pm'] . "</td>";
									}
							}
							// dummy column insert
							if ($Columns['Schooldays'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if ($Columns['ReasonStat'] == "1") {
								if (sizeof($absent_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($late_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($earlyleave_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
							}
							if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							$content .= "</tr>";
						}

						$content .= "</tr>";
					}

					$row_span = sizeof($earlyleave_reason_symbol);
					if ($row_span > 0) {
						$temp = $row_span +1;
						$content .= "<tr>";
						$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"$temp\" colspan=\"$col_span\" align=\"right\" valign=\"top\">" . $Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat'] . "(" . $Lang['StudentAttendance']['EarlyLeave'] . ")</td>";
						for ($k = 0; $k < $row_span; $k++) {
							$content .= "<tr>";
							$content .= "<td class=\"tablebluetop tabletoplink\">" . $earlyleave_reason_symbol[$k]['Reason'] . "</td>";
							for ($i = 1; $i <= $day_num; $i++) {
								if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
									continue;
								}
								if ($attendance_mode == "0") {
									$content .= "<td align=\"center\" colspan=\"2\">" . $earlyleave_reasons_stat[$i][$k]['am'] . "</td>";
								} else
									if ($attendance_mode == "1") {
										$content .= "<td align=\"center\" colspan=\"2\">" . $earlyleave_reasons_stat[$i][$k]['pm'] . "</td>";
									} else {
										$content .= "<td align=\"center\">" . $earlyleave_reasons_stat[$i][$k]['am'] . "</td>";
										$content .= "<td align=\"center\">" . $earlyleave_reasons_stat[$i][$k]['pm'] . "</td>";
									}
							}
							// dummy column insert
							if ($Columns['Schooldays'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if ($Columns['ReasonStat'] == "1") {
								if (sizeof($absent_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($late_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($earlyleave_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
							}
							if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							$content .= "</tr>";
						}

						$content .= "</tr>";
					}
				}
				### End of Reasons Daily Statistic ###

				if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1" && $Columns['Data'] == "1") {
					### Absent sessions Daily Statistic ###
					$col_span = 6;
					if ($Columns['ChineseName'] != "1")
						$col_span -= 1;
					if ($Columns['EnglishName'] != "1")
						$col_span -= 1;
					if ($Columns['Gender'] != "1")
						$col_span -= 1;
					if ($Columns['BirthDate'] != "1")
						$col_span -= 1;
					if ($Columns['EntryDate'] != "1")
						$col_span -= 1;
					
					$row_span = 4;
					if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
						$row_span += 1; 
					}
					$temp = $row_span +1;

					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"$temp\" colspan=\"$col_span\" align=\"right\">" . $Lang['StudentAttendance']['AbsentSessions'] . "</td>";
					// late session
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['Late'] . "</td>";
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$i]['Late']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$i]['Late']['pm'] . "</td>";
							} else {
								$content .= "<td align=\"center\">" . $absent_sessions_stat[$i]['Late']['am'] . "</td>";
								$content .= "<td align=\"center\">" . $absent_sessions_stat[$i]['Late']['pm'] . "</td>";
							}
					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					// RequestLeave
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['RequestLeave'] . "</td>";
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$i]['RequestLeave']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$i]['RequestLeave']['pm'] . "</td>";
							} else {
								$content .= "<td align=\"center\">" . $absent_sessions_stat[$i]['RequestLeave']['am'] . "</td>";
								$content .= "<td align=\"center\">" . $absent_sessions_stat[$i]['RequestLeave']['pm'] . "</td>";
							}
					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					// PlayTruant
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['PlayTruant'] . "</td>";
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$i]['PlayTruant']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$i]['PlayTruant']['pm'] . "</td>";
							} else {
								$content .= "<td align=\"center\">" . $absent_sessions_stat[$i]['PlayTruant']['am'] . "</td>";
								$content .= "<td align=\"center\">" . $absent_sessions_stat[$i]['PlayTruant']['pm'] . "</td>";
							}
					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					// OfficalLeave
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['OfficalLeave'] . "</td>";
					for ($i = 1; $i <= $day_num; $i++) {
						if ($HideNoData == 1 && !in_array($i, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$i]['OfficalLeave']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$i]['OfficalLeave']['pm'] . "</td>";
							} else {
								$content .= "<td align=\"center\">" . $absent_sessions_stat[$i]['OfficalLeave']['am'] . "</td>";
								$content .= "<td align=\"center\">" . $absent_sessions_stat[$i]['OfficalLeave']['pm'] . "</td>";
							}
					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					
					
					if($sys_custom['SmartCardAttendance_StudentAbsentSession2'])
					{
						// Absenteeism
						$content .= "<tr>";
						$content .= "<td class=\"tablebluetop tabletoplink\">".$Lang['StudentAttendance']['Absent']."</td>";
						for ($i=1;$i<=$day_num;$i++)
						{
							if ($HideNoData == 1 && !in_array($i,$DayWithData)) {
								continue;
							}
							if($attendance_mode=="0")
							{
								$content .= "<td align=\"center\" colspan=\"2\">".$absent_sessions_stat[$i]['Absenteeism']['am']."</td>";
							}else if($attendance_mode=="1")
							{
								$content .= "<td align=\"center\" colspan=\"2\">".$absent_sessions_stat[$i]['Absenteeism']['pm']."</td>";
							}else
							{
								$content .= "<td align=\"center\">".$absent_sessions_stat[$i]['Absenteeism']['am']."</td>";
								$content .= "<td align=\"center\">".$absent_sessions_stat[$i]['Absenteeism']['pm']."</td>";
							}
						}
						// dummy column insert
						if($Columns['Schooldays']=="1")
						{
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if($Columns['ReasonStat']=="1")
					    {
							if(sizeof($absent_reason_symbol)>0)
							{
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if(sizeof($late_reason_symbol)>0)
							{
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if(sizeof($earlyleave_reason_symbol)>0)
							{
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
						}
						if($sys_custom['SmartCardAttendance_StudentAbsentSession2'])
						{
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						$content .= "</tr>";
					}
				}
				$content .= "</tr>";
				### End of Absent sessions Daily Statistic ###

				$content .= "</table>";

				$content .= "</td>";
				if ($Columns['Remark'] == "1") {
					$content .= "<td valign=\"top\">";

					$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\" height=\"75\" align=\"center\">";
					//$content .= "<td rowspan=\"3\" algin=\"center\">";
					$content .= $Lang['General']['Remark'];
					$content .= "</td>";
					$content .= "</tr>";
					$content .= "<tr></tr>";
					$content .= "<tr></tr>";
					$content .= "</table>";

					$content .= "</td>";
				}
				$content .= "<td valign=\"top\">";
				if ($Columns['MonthlyStat'] == "1") {
					$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=0 cellspacing=0>";
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\" height=\"75\" align=\"center\">" . $Lang['StudentAttendance']['MonthlyStatistic'] . "</td>";
					$content .= "</tr>";
					$content .= "</table>
										
																<table id=\"contentTable3\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
					$content .= "<tr>";
					$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['NumberOfAttendants'] . ": " . $count_total_student . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['AveragePresentNumber'] . ": " . round(count($total_day)==0? '0' : ($totalPresent / count($total_day)), 2) . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['AverageAbsentNumber'] . ": " . round(count($total_day)==0? '0' : ($totalAbsent / count($total_day)), 2) . "</td>";
					$content .= "</tr>";
					$content .= "</table>";
				}
				$content .= "<br>";

				$content .= "<table id=\"contentTable4\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['NotationSymbols'] . ": </td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[0] . " - " . $Lang['StudentAttendance']['Present'] . "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[1] . " - " . $Lang['StudentAttendance']['Absent'] . "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[2] . " - " . $Lang['StudentAttendance']['Late'] . "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[4] . " - " . $Lang['StudentAttendance']['EarlyLeave'] . "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[9] . " - " . $Lang['StudentAttendance']['Waived'] . "</td>";
				$content .= "</tr>";
				$content .= "</table>";
				### Customize Reason Symbols ###
				if (sizeof($absent_reason_symbol) > 0 || sizeof($late_reason_symbol) > 0 || sizeof($earlyleave_reason_symbol) > 0) {
					$content .= "<br>";
					$content .= "<table id=\"contentTable4\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
					$content .= "<tr>";
					$content .= "<td nowrap class=\"tabletext\">" . $Lang['SmartCard']['StudentAttendance']['CustomizeReasonSymbols'] . ": </td>";
					$content .= "</tr>";
					if (sizeof($absent_reason_symbol) > 0) {
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">&nbsp;</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['Absent'] . ": </td>";
						$content .= "</tr>";
						for ($k = 0; $k < sizeof($absent_reason_symbol); $k++) {
							$content .= "<tr>";
							$content .= "<td nowrap class=\"tabletext\">" . $absent_reason_symbol[$k]['StatusSymbol'] . " - " . $absent_reason_symbol[$k]['Reason'] . "</td>";
							$content .= "</tr>";
						}
					}
					if (sizeof($late_reason_symbol) > 0) {
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">&nbsp;</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['Late'] . ": </td>";
						$content .= "</tr>";
						for ($k = 0; $k < sizeof($late_reason_symbol); $k++) {
							$content .= "<tr>";
							$content .= "<td nowrap class=\"tabletext\">" . $late_reason_symbol[$k]['StatusSymbol'] . " - " . $late_reason_symbol[$k]['Reason'] . "</td>";
							$content .= "</tr>";
						}
					}
					if (sizeof($earlyleave_reason_symbol) > 0) {
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">&nbsp;</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['EarlyLeave'] . ": </td>";
						$content .= "</tr>";
						for ($k = 0; $k < sizeof($earlyleave_reason_symbol); $k++) {
							$content .= "<tr>";
							$content .= "<td nowrap class=\"tabletext\">" . $earlyleave_reason_symbol[$k]['StatusSymbol'] . " - " . $earlyleave_reason_symbol[$k]['Reason'] . "</td>";
							$content .= "</tr>";
						}
					}
					$content .= "</table>";
				}
				### End of Customize Reason Symbols ###

				$content .= "<br />";
				$content .= "<table id=\"contentTable4\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
				$content .= "<tr>";
				$content .= "<td><span style=\"border:1px solid black;background-color:#CCCCCC;width:16px;height:16px;display:block;float:left;\"></span>:<br style=\"clear:both;\"/>" . $Lang['EventType']['PublicHoliday'] . " / " . $Lang['EventType']['SchoolHoliday'] . "</td>";
				$content .= "</tr>";
				$content .= "</table>";

				$content .= "</td>";
				$content .= "</tr>";
				$content .= "</table>";
				//$content .= "<br/>";

			}

			if ($PrintVersion) {
				$breakStyle = $z < sizeof($Classes) - 1 ? "style='page-break-after:always'" : "";
			}

			$content .= "<div " . $breakStyle . ">&nbsp;</div>";
		}
		return $content;
	}

	function Get_LateAbsentEarlyClassGroup_Confirm_JS($DayType, $RecordType, $External = "", $TargetDate = "", $RadioDisplay=0) {
		$AMConfirmInfo = $this->Check_LateAbsentEarlyClassGroup_Confirm_Status(PROFILE_DAY_TYPE_AM, $RecordType, $TargetDate);
		$PMConfirmInfo = $this->Check_LateAbsentEarlyClassGroup_Confirm_Status(PROFILE_DAY_TYPE_PM, $RecordType, $TargetDate);
		$x .= '
							<script>
							var css_array = new Array;
							css_array[0] = "dynCalendar_card_no_data";//no attendance data
							css_array[1] = "dynCalendar_card_not_confirmed";// all not confirmed
							css_array[2] = "dynCalendar_card_confirmed";// all confirmed
							css_array[3] = "dynCalendar_card_partial_confirmed";// partially confirmed
							var date_array = new Array;
							var am_date_array = new Array;
							var pm_date_array = new Array;';

		foreach ($AMConfirmInfo as $Key => $Var) {
			if ($Var == PARTIALCONFIRM) {
				$x .= "am_date_array['" . $Key . "'] = css_array[3];";
			} else
				if ($Var == NONECONFIRM) {
					$x .= "am_date_array['" . $Key . "'] = css_array[1];";
				} else
					if ($Var == ALLCONFIRM) {
						$x .= "am_date_array['" . $Key . "'] = css_array[2];";
					}
		}
		foreach ($PMConfirmInfo as $Key => $Var) {
			if ($Var == PARTIALCONFIRM) {
				$x .= "pm_date_array['" . $Key . "'] = css_array[3];";
			} else
				if ($Var == NONECONFIRM) {
					$x .= "pm_date_array['" . $Key . "'] = css_array[1];";
				} else
					if ($Var == ALLCONFIRM) {
						$x .= "pm_date_array['" . $Key . "'] = css_array[2];";
					}
		}

		if ($External == "") {
			if ($DayType == PROFILE_DAY_TYPE_AM) {
				$x .= 'date_array = am_date_array;';
			} else {
				$x .= 'date_array = pm_date_array;';
			}
		} else {
			// handle calendar display in different radio situation
			$targetDayType = "";
			if($RadioDisplay == 0){
				$targetDayType=' var DayType = window.parent.document.getElementById(\'DayType\').value;';
			} else if($RadioDisplay == 1){
				$targetDayType=' var DayType = window.parent.$(\'input[name=DayType]:checked\').val();';
			} else {
				$targetDayType=' var DayType = '.$DayType;
			}
			$x .= '
									window.parent.am_date_array = am_date_array;
									window.parent.pm_date_array = pm_date_array;
						
									'.$targetDayType.'
									if (DayType == ' . PROFILE_DAY_TYPE_AM . ') {
										window.parent.date_array = am_date_array;
									}
									else {
										window.parent.date_array = pm_date_array;
									}';	
		}

		$x .= '</script>';

		return $x;
	}

	function Get_Mask_Confirmed_Date_Function() {
	    
		$x .= "function Mask_Confirmed_Date(date) {
		        var Month = ((date.getMonth() + 1)>=10)? (date.getMonth() + 1):'0'+(date.getMonth() + 1);
				var Day = (date.getDate() >= 10)? date.getDate():'0'+date.getDate();
				var CSS = '';
				if (date_array[date.getFullYear()+'-'+Month+'-'+Day]) {
					CSS = date_array[date.getFullYear()+'-'+Month+'-'+Day];
				}
                return [true, CSS];
			}";
        
		return $x;
	}

	function Get_Confirm_Selection_Form($FormName = "form1", $action = "", $TargetType = "", $TargetDate = "", $DayType = "", $radioDisplay=0) {
	    global $Lang, $PATH_WRT_ROOT, $sys_custom, $linterface, $LAYOUT_SKIN, $image_path, $plugin, $intranet_session_language;
		global $i_DayTypeAM, $i_DayTypePM, $i_StudentAttendance_Field_Date, $i_StudentAttendance_Slot, $button_view;

		if (!($linterface instanceof  interface_html)) {
			include_once ($PATH_WRT_ROOT . 'includes/libinterface.php');
			$linterface = new interface_html();
		}

		if ($TargetDate == "") {
			$TargetDate = date('Y-m-d');
		}
		if ($this->attendance_mode != 1) {
			$data[] = array (
				PROFILE_DAY_TYPE_AM,
				$i_DayTypeAM
			);
		}
		if ($this->attendance_mode != 0) {
			$data[] = array (
				PROFILE_DAY_TYPE_PM,
				$i_DayTypePM
			);
		}
		if ($DayType == "") {
			if ($this->attendance_mode == 0) {
				$DayType = PROFILE_DAY_TYPE_AM;
			} else
				if ($this->attendance_mode == 1) {
					$DayType = PROFILE_DAY_TYPE_PM;
				} else {
					$DayType = PROFILE_DAY_TYPE_AM;
				}
		}
		
//		debug_pr($DayType);
//		debug_pr($data);

		$selection_period = getSelectByArray($data, " name=\"DayType\" id=\"DayType\" onChange=\"if (this.value == " . PROFILE_DAY_TYPE_AM . ") {date_array = am_date_array;} else {date_array = pm_date_array;}\" ", $DayType, 0, 1);
		
		// Display Radio button
		if($radioDisplay){
			
			$dataTypeLength = sizeof($data);
			$selection_period = "";
			// Only 1 button is required: Display AM/PM Only
			if($dataTypeLength == 1){
//				$selection_period .= $linterface->Get_Radio_Button('radio_1', 'DayType', $data[0][0], true, $Class="", $data[0][1], $Onclick="if (this.value == \" . PROFILE_DAY_TYPE_AM . \") {date_array = am_date_array;} else {date_array = pm_date_array;}",$isDisabled=1);
				$selection_period .= $data[0][1];
				// Set for calendar display
				$radioDisplay = 2;
			} else {
				
				for ($i=0; $i<$dataTypeLength; $i++){	
						$radioSelected = false;
						// Set target radio button to checked
						if($DayType == $data[$i][0]){
							$radioSelected = true;
						}
						$selection_period .= $linterface->Get_Radio_Button("radio_$i", 'DayType', $data[$i][0], $radioSelected, $Class="", $data[$i][1], $Onclick="if (this.value == ".PROFILE_DAY_TYPE_AM.") {date_array = am_date_array;} else {date_array = pm_date_array;}",$isDisabled=0);
				}
			}
		}
		/*	
		switch ($TargetType) {
			case "Class":
				$x .= $this->Get_LateAbsentEarlyClassGroup_Confirm_JS($DayType,"Class");
				break;
			case "Group":
				$x .= $this->Get_LateAbsentEarlyClassGroup_Confirm_JS($DayType,"Group");
				break;
			case CARD_STATUS_LATE:
				$x .= $this->Get_LateAbsentEarlyClassGroup_Confirm_JS($DayType,CARD_STATUS_LATE);
				break;
			case CARD_STATUS_ABSENT:
				$x .= $this->Get_LateAbsentEarlyClassGroup_Confirm_JS($DayType,CARD_STATUS_ABSENT);
				break;
			case PROFILE_TYPE_EARLY:
				$x .= $this->Get_LateAbsentEarlyClassGroup_Confirm_JS($DayType,PROFILE_TYPE_EARLY);
				break;
			default:
				break;
		}
		*/

		if ($plugin['eSchoolBus'] && $TargetType == PROFILE_TYPE_LATE) {
		    $onDatePickSelectedFunction = 'getRouteSelection();';
		}
		else {
		    $onDatePickSelectedFunction = '';
		}
		
		$x .= '<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">';
		$x .= "
							<script>
							var date_array = new Array;
							var am_date_array = new Array;
							var pm_date_array = new Array;
							
							function Check_View_Form() {
								if ($('#DPWL-TargetDate').html() == '') {
									document.getElementById(\"" . $FormName . "\").submit();
								}
							}
							
							" . $this->Get_Mask_Confirmed_Date_Function() . "
							</script>";
		$x .= '<form name="' . $FormName . '" id="' . $FormName . '" method="post" action="' . $action . '">
								<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
										<td valign="top" nowrap="nowrap" class="tabletext">' . $i_StudentAttendance_Field_Date . '</td>
										<td class="tabletext" width="70%">
											' . $linterface->GET_DATE_PICKER("TargetDate", $TargetDate, "", "yy-mm-dd", "Mask_Confirmed_Date", "","",$onDatePickSelectedFunction) . '
										</td>
									</tr>';
						if($sys_custom['StudentAttendance']['HostelAttendance'] && $TargetType == "HostelGroup"){
							// do not require session slot
						}else{
							 $x .= '<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
										<td class="tabletext" width="70%" id="ConfirmColorCell">
											<img src="' . $image_path . '/' . $LAYOUT_SKIN . '/loadingAnimation.gif" alt="' . $Lang['General']['Loading'] . '" border="0" align="absmiddle">
										</td>
									</tr>';
							 $x .= '<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $i_StudentAttendance_Slot . '</td>
										<td class="tabletext" width="70%">' . $selection_period . '</td>
									</tr>';
						}

						if ($plugin['eSchoolBus'] && $TargetType == PROFILE_TYPE_LATE) {
						    include_once($PATH_WRT_ROOT."lang/eSchoolBus_lang.$intranet_session_language.php");
						    
						    $x .= '<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['eSchoolBus']['Management']['Attendance']['Route']. '</td>
										<td class="tabletext" width="70%"><span id="ajax_route_selection"></span></td>
									</tr>';
						}
							$x .= '</table>
								<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
								    	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
								    </tr>
								</table>
								<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
									<tr>
									    <td align="center" colspan="2">
											' . $linterface->GET_ACTION_BTN($button_view, "button", "Check_View_Form();") . '
										</td>
									</tr>
								</table>
								<input type="hidden" name="flag" value="1">
								<input type="hidden" name="Msg" id="Msg" value="">
								</form>';
		if($sys_custom['StudentAttendance']['HostelAttendance'] && $TargetType == "HostelGroup"){
			
		}else{
			$x .= '<iframe src="' . $PATH_WRT_ROOT . 'home/eAdmin/StudentMgmt/attendance/dailyoperation/hidden_get_confirm_color_status.php?TargetType=' . $TargetType . '&TargetDate=' . $TargetDate . '&DayType=' . $DayType . '&radioTarget='. $radioDisplay .'" style="display:none; width:0px; height:0px;"></iframe>';
		}
		return $x;
	}

	function Get_Class_Status_Shortcut($TargetType) {
		global $Lang, $PATH_WRT_ROOT, $module_version, $i_SmartCard_DailyOperation_ViewLateStatus, $i_SmartCard_DailyOperation_ViewAbsenceStatus, $i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, $i_SmartCard_DailyOperation_ViewClassStatus;
		global $i_StudentAttendance_ShortCut;
		
		//if($module_version['StudentAttendance'] == 3.0 || strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/StudentMgmt/eAttendance/')!==false){
		if($this->isStudentAttendance30()){	
			$StudentAttendance30 = true;	
		}
		if($StudentAttendance30){
			$Path = "home/eAdmin/StudentMgmt/eAttendance/management/";
			if ($TargetType != "Class") {
				$ResultShortcut[] = array (
					$PATH_WRT_ROOT . $Path . "school_attendance/class/class_status.php",
					$i_SmartCard_DailyOperation_ViewClassStatus
				);
			}
		}else{
			$Path = "home/eAdmin/StudentMgmt/attendance/dailyoperation/";
			if ($TargetType != "Class") {
				$ResultShortcut[] = array (
					$PATH_WRT_ROOT . $Path . "class/class_status.php",
					$i_SmartCard_DailyOperation_ViewClassStatus
				);
			}
		}
		if ($TargetType != PROFILE_TYPE_LATE) {
			$ResultShortcut[] = array (
				$PATH_WRT_ROOT . $Path . ($StudentAttendance30?"profile/":""). "late/showlate.php",
				$i_SmartCard_DailyOperation_ViewLateStatus
			);
		}
		if ($TargetType != PROFILE_TYPE_ABSENT) {
			$ResultShortcut[] = array (
				$PATH_WRT_ROOT . $Path . ($StudentAttendance30?"profile/":""). "absence/show.php",
				$i_SmartCard_DailyOperation_ViewAbsenceStatus
			);
		}
		if ($TargetType != PROFILE_TYPE_EARLY) {
			$ResultShortcut[] = array (
				$PATH_WRT_ROOT . $Path . ($StudentAttendance30?"profile/":""). "early/show.php",
				$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus
			);
		}

		$pageShortCut_Selection = getSelectByArray($ResultShortcut, " name=\"pageShortCut\" onChange=\"changePage(this.value)\" ");
		$ShortCutTable = "<tr>
														<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_StudentAttendance_ShortCut . "</td>
														<td class=\"tabletext\" width=\"70%\">" . $pageShortCut_Selection . "</td>
													</tr>
													<script>
													function changePage(val)
													{
														var DayType = document.getElementById('DayType').value;
														var TargetDate = document.getElementById('TargetDate').value;
														
														window.location = val+\"?TargetDate=\"+TargetDate+\"&DayType=\"+DayType;
													}
													</script>
													";

		return $ShortCutTable;
	}

	function Get_Preset_Outing_Form($OutingID = "") {
		global $PATH_WRT_ROOT, $Lang, $image_path, $LAYOUT_SKIN;

		include_once ($PATH_WRT_ROOT . 'includes/libinterface.php');
		$linterface = new interface_html();
		if ($OutingID != "") {
			$OutingDetail = $this->Get_Preset_Outing_Detail($OutingID);
			$namefield = getNameFieldWithClassNumberByLang();
			$sql = 'select ' . $namefield . ' from INTRANET_USER where UserId = \'' . $OutingDetail['UserID'] . '\'';
			$Temp = $this->returnVector($sql);
			$select_student .= $Temp[0];

			$PAGE_NAVIGATION[] = array (
				$button_edit
			);

			$OutTime = $OutingDetail['OutTime'];

			if ($OutingDetail['DayType'] == PROFILE_DAY_TYPE_AM)
				$select_datetype = $Lang['StudentAttendance']['DayTypeAM'];
			else
				$select_datetype = $Lang['StudentAttendance']['DayTypePM'];
		} else {
			$select_class = $this->getSelectClass("name=ClassName onChange=\"Get_Student_Selection(this.value);\"", $ClassName);
			$select_student = $this->getStudentSelectByClass($ClassName, "name=StudentID");

			# select day type
			if ($this->attendance_mode != 1) {
				$select_datetype .= "<input type=\"checkbox\" name=\"DayType[]\" id=\"dateTypeAM\" value=\"" . PROFILE_DAY_TYPE_AM . "\"><label for=\"dateTypeAM\">" . $Lang['StudentAttendance']['DayTypeAM'] . "</label>&nbsp;&nbsp;";
			}
			if ($this->attendance_mode != 0) {
				$select_datetype .= "<input type=\"checkbox\" name=\"DayType[]\" id=\"dateTypePM\" value=\"" . PROFILE_DAY_TYPE_PM . "\"><label for=\"dateTypePM\">" . $Lang['StudentAttendance']['DayTypePM'] . "</label>";
			}
			$PAGE_NAVIGATION[] = array (
				$button_new
			);

			$OutTime = date('H:i:s');
		}
		$fromWhereTemplate = getSelectByValue($this->getWordList(1), "onChange=\"this.form.FromWhere.value=this.value\"");
		$locationTemplate = getSelectByValue($this->getWordList(2), "onChange=\"this.form.Location.value=this.value\"");

		$namefield = getNameFieldByLang();
		$sql = "SELECT $namefield FROM INTRANET_USER WHERE RecordType = 1 AND RecordStatus='1' ORDER BY EnglishName";
		$staff = $this->returnVector($sql);
		$picTemplate = getSelectByValue($staff, "onChange=\"addPIC(this.form,this.value)\"");

		$x = $this->Include_JS_CSS();
		$x .= '<br />
							<form id="form1" name="form1" action="update.php" method="POST" ONSUBMIT="checkform(this);return false;">
							<input type="hidden" name="OutingID" id="OutingID" value="' . $OutingID . '">
							<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>' . $linterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
							<table>
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
						 ';
		if ($OutingID == "") {
			$x .= '
									<tr>
										<td colspan="2">' . $Lang['CommonChoose']['SelectTarget'] . '</td>
									</tr>
									<tr>
										<td colspan="2">
											<table width="100%" border="0" cellpadding"0" cellspacing="0" align="center">
											<tr>
												<td class="tabletext" width="40%">' . $Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent'] . '</td>
												<td class="tabletext"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1"></td>
												<td class="tabletext" width="60%">' . $Lang['eBooking']['Settings']['ManagementGroup']['SelectedUser'] . '</td>
											</tr>
											<tr>
												<td class="tablerow2" valign="top">
													<table width="100%" border="0" cellpadding="3" cellspacing="0">
													<tr>
														<td class="tabletext">' . $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "Get_Student_Selection_Pop_Up();") . '</td>
													</tr>
													<tr>
														<td class="tabletext"><i>' . $Lang['General']['Or'] . '</i></td>
													</tr>
													<tr>
														<td class="tabletext">
															' . $Lang['CommonChoose']['SearchByInputFormat'] . '
															<br />
															<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />
														</td>
													</tr>
													<tr>
														<td class="tabletext"><i>' . $Lang['General']['Or'] . '</i></td>
													</tr>
													<tr>
														<td class="tabletext">
															' . $Lang['CommonChoose']['SearchByLoginID'] . '
															<br />
															<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />
														</td>
													</tr>
													</table>
												</td>
												<td class="tabletext" ><img src="' . $image_path . '/' . $LAYOUT_SKIN . '"/10x10.gif" width="10" height="1"></td>
												<td align="left" valign="top">
													<table width="100%" border="0" cellpadding="5" cellspacing="0">
													<tr>
														<td align="left">
															<span style="vertical-align: bottom;">' . $linterface->GET_SELECTION_BOX(array (), "name='SelectedUserIDArr[]' id='SelectedUserIDArr[]' class='select_studentlist' size='15' multiple='multiple'", "") . '</span>
															' . $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected_Student();") . '
														</td>
													</tr>
													<tr>
														<td>
															' . $linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* ' . $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']) . '
														</td>
													</tr>
													</table>
												</td>
											</tr>
											<tr><td>&nbsp;</td></tr>
											</table>
										</td>
									</tr>';
			/*
			$x .= '
						<tr>
							<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['StudentAttendance']['Class'].'</td>
							<td width="70%"class="tabletext">'.$select_class.'</td>
						</tr>';
			*/
		} else {
			$x .= '
									<tr>
										<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['StudentName'] . '</td>
										<td width="70%"class="tabletext" id="StudentSelectionLayer">' . $select_student . '</td>
									</tr>';
		}
		$x .= '
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['OutingDate'] . '</td>
									<td width="70%"class="tabletext">';
		if ($OutingID == "") {
			$default_date = (($OutingDetail['RecordDate'] != "") ? $OutingDetail['RecordDate'] : date('Y-m-d'));
			$date_picker = $linterface->GET_DATE_PICKER("OutingDatePicker", $default_date, ' style="visibility:hidden;width:0px;" ', "yy-mm-dd", "", "", "", "onDatePickerSelected(dateText);", "OutingDatePickerText");
			
			$x .= '<table border="0" cellpadding="4" cellspacing="0" width="30%">
						<tbody>
							<tr>
								<td style="border:1px solid #CCCCCC" valign="top">
									<table id="TableDate" border="0" cellpadding="4" cellspacing="0" width="100%">
										<tbody>
											<tr class="tabletop">
												<td>#</td>
												<td nowrap="nowrap">'.$Lang['StudentAttendance']['DateSelected'].'</td>
												<td>'.$date_picker.'</td>
											</tr>
											<tr class="tablerow1 data-row">
												<td>1</td>
												<td class="data-col">'.$default_date.'<input type="hidden" name="OutingDate[]" value="'.$default_date.'" /></td>
												<td>'.$linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['Btn']['Delete'], "onDateDeleted(this);", "", 1).'</td>
											</tr>
										</tbody>
									</table>
								</td>';
							//$x .= '<td>';
							//$x .= $linterface->GET_DATE_PICKER("OutingDatePicker", (($OutingDetail['RecordDate'] != "") ? $OutingDetail['RecordDate'] : date('Y-m-d')), "", "yy-mm-dd", "", "", "", "onDatePickerSelected(dateText);");
							//$x .= '</td>';
					$x .= '</tr>';
				$x .= '</tbody>';
			$x .= '</table>';
		} else {
			$x .= $OutingDetail['RecordDate'];
			$x .= '<input type="hidden" name="OutingDate[]" value="'.$OutingDetail['RecordDate'].'" />';
		}
		$x .= '		</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['DayType'] . '</td>
									<td width="70%"class="tabletext">' . $select_datetype . '</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['OutingStartTime'] . '</td>
									<td width="70%"class="tabletext">
										<input type="text" name="OutTime" id="OutTime" class="textboxnum" maxlength="10" value="' . $OutTime . '">(HH:mm:ss 24-hr)
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['OutingEndTime'] . '</td>
									<td width="70%"class="tabletext">
										<input type="text" name="BackTime" id="BackTime" class="textboxnum" maxlength="10" value="' . $OutingDetail['BackTime'] . '">(HH:mm:ss 24-hr)
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['OutingPIC'] . '</td>
									<td width="70%"class="tabletext">
										<input type="text" class="textboxtext" name="PIC" id="PIC" value="' . htmlspecialchars($OutingDetail['PIC'], ENT_QUOTES) . '">' . $picTemplate . '
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['OutingFrom'] . '</td>
									<td width="70%"class="tabletext">
										<input style="margin-right: 20px" type="text" class="textboxnum" name="FromWhere" id="FromWhere" value="' . htmlspecialchars($OutingDetail['FromWhere'], ENT_QUOTES) . '">' . $fromWhereTemplate . '
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['OutingTo'] . '</td>
									<td width="70%"class="tabletext">
										<input style="margin-right: 20px" type="text" class="textboxnum" name="Location" id="Location" value="' . htmlspecialchars($OutingDetail['Location'], ENT_QUOTES) . '">' . $locationTemplate . '
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['OutingReason'] . '</td>
									<td width="70%"class="tabletext">';
		$words_outing = $this->Get_Outing_Reason();
		foreach ($words_outing as $key => $word)
			$words_outing[$key] = htmlspecialchars($word);
		$x .= $linterface->CONVERT_TO_JS_ARRAY($words_outing, "OutingArrayWords", 1, 1);
		$x .= '			<input type="text" class="textboxnum" name="Objective" id="Objective" value="' . htmlspecialchars($OutingDetail['Objective'], ENT_QUOTES) . '">
										' . $linterface->GET_PRESET_LIST("OutingArrayWords", 2, "Objective") . '
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['OutingRemark'] . '</td>
									<td width="70%"class="tabletext">
										' . $linterface->GET_TEXTAREA("Detail", htmlspecialchars($OutingDetail['Detail'], ENT_QUOTES)) . '
									</td>
								</tr>
							</table>
							<div id="WarningLayer"></div>
							<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
								<tr>
							    	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							    </tr>
							</table>
							<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
								<tr>
								    <td align="center" colspan="2">' .
		$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "") .
		$linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset", "", "reset2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") .
		$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:history.back()", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . '
									</td>
								</tr>
							</table>
							</form>
							<br />';
		return $x;
	}

	function Get_Preset_Outing_Import_confirm($data) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

		include_once ($PATH_WRT_ROOT . 'includes/libinterface.php');
		$linterface = new interface_html();

		$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import']
		);

		# step information
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['SelectCSVFile'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['CSVConfirmation'],
			1
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['ImportResult'],
			0
		);

		$x = '<br/>
						<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>' . $linterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
							</tr>
							<tr>
								<td>' . $linterface->GET_STEPS($STEPS_OBJ) . '</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
						<table width="90%" border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['TotalRecord'] . '</td>
								<td class="tabletext">' . sizeof($data) . '</td>
							</tr>';
		if (sizeof($data) > 0) {
			$ErrorArray = $this->Import_Preset_Outing_Record_To_Temp($data);
			$x .= '
								<tr>
									<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['SuccessfulRecord'] . '</td>
									<td class="tabletext">' . (sizeof($data) - sizeof($ErrorArray)) . '</td>
								</tr>
								<tr>
									<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['FailureRecord'] . '</td>
									<td class="tabletext">' . sizeof($ErrorArray) . '</td>
								</tr>
							</table>
							';
			$x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0">
									<tr>
										<td class="tablebluetop tabletopnolink" width="10">#</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['ClassName'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['ClassNumber'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['OutingDate'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['DayType'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['OutingPIC'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['OutingStartTime'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['OutingEndTime'] . '</td>
										<td class="tablebluetop tabletopnolink">'.$Lang['StudentAttendance']['OutingFrom'].'</td>
										<td class="tablebluetop tabletopnolink">'.$Lang['StudentAttendance']['OutingTo'].'</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['OutingReason'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['General']['Remark'] . '</td>
										<td class="tablebluetop tabletopnolink">'.$Lang['General']['Error'].'</td>
									</tr>';
			for ($i = 0; $i < sizeof($ErrorArray); $i++) {
				$css_i = ($i % 2) ? "2" : "";
				$x .= '
											<tr>
												<td valign="top" class="tablebluerow' . $css_i . '" width="10">
													' . $ErrorArray[$i]['RowNumber'] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][0] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][1] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][2] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][3] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][4] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][5] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][6] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][7] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][8] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][9] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][10] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													<font style="color:red;">' . $ErrorArray[$i]['ErrorMsg'] . '</font>
												</td>
											</tr>';
			}
			$x .= '</table>';

			$x .= '
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td class="dotline"><img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							</tr>
							<tr>
								<td align="center">';
			if (sizeof($ErrorArray) == 0)
				$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "window.location='import_update.php';");

			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
			$x .= '</td>
							</tr>
							</table>';
		} else {
			$x .= '</table>';

			$x .= '
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td class="dotline"><img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							</tr>
							<tr>
								<td align="center">';
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
			$x .= '</td>
							</tr>
							</table>';
		}

		return $x;
	}

	function Get_Preset_Outing_Import_Finish_Page() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

		$Result = $this->Finalize_Import_Preset_Outing();

		include_once ($PATH_WRT_ROOT . 'includes/libinterface.php');
		$linterface = new interface_html();

		$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import']
		);

		# step information
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['SelectCSVFile'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['CSVConfirmation'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StudentAttendance']['ImportResult'],
			1
		);

		$x .= '<br />
							<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>' . $linterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
								</tr>
								<tr>
									<td>' . $linterface->GET_STEPS($STEPS_OBJ) . '</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
									' . $Result . ' ' . $Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'] . '
									</td>
								</tr>
								<tr>
									<td class="dotline">
										<img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" />
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
										' . $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='index.php';") . '
									</td>
								</tr>
							</table>';

		return $x;
	}

	function Get_Specified_Attendance_Mode_Selection_List($Type = "Class", $ExcludeID = "") {
		global $PATH_WRT_ROOT;
		include_once ($PATH_WRT_ROOT . 'includes/libinterface.php');
		$linterface = new interface_html();
		if ($Type == "Class")
			$ClassList = $this->Get_Class_List_By_Attendance_Mode(1, $ExcludeID);
		else
			$ClassList = $this->Get_Group_List_By_Attendance_Mode(1, $ExcludeID);
		$x = '<select name="ApplyAlsoList[]" id="ApplyAlsoList" class="formtextbox" MULTIPLE size="10" style="min-width:150px;">';
		for ($i = 0; $i < sizeof($ClassList); $i++) {
			if ($ClassList[$i]['YearID'] != $ClassList[$i -1]['YearID']) {
				if ($i == 0)
					$x .= '<optgroup label="' . $ClassList[$i]['YearName'] . '">';
				else {
					$x .= '</optgroup>';
					$x .= '<optgroup label="' . $ClassList[$i]['YearName'] . '">';
				}
			}

			$x .= '<option value="' . $ClassList[$i]['ID'] . '">' . $ClassList[$i]['Title'] . '</option>';

			if ($i == (sizeof($ClassList) - 1))
				$x .= '</optgroup>';
		}
		$x .= '</select>&nbsp;';
		$x .= $linterface->GET_SMALL_BTN("Select/ Deselect All", "button", "var SelectedAlready = false; var SelectObj = document.getElementById('ApplyAlsoList'); for (var i=0; i< SelectObj.length; i++) {if (SelectObj.options[i].selected) {SelectedAlready = true;}} for (var i=0; i< SelectObj.length; i++) {SelectObj.options[i].selected = !(SelectedAlready);}");

		return $x;
	}

	function Get_No_Tapping_Card_Report($StartDate, $EndDate, $TargetType, $OrderBy, $Format, $AM, $LunchOut, $PM, $LeaveSchool, $Details = "", $ClassName = array ()) {
		global $Lang, $PATH_WRT_ROOT;
		global $i_StudentAttendance_Report_NoCardTab, $i_general_class, $i_identity_student, $i_general_count, $i_ClassName;
		global $i_UserStudentName, $i_ClassName, $i_ClassNumber, $i_StudentAttendance_LeaveSchool;
		global $MODULE_OBJ;

		if ($AM == 1) {
			$CheckField[] = array (
				'InSchoolTime',
				'AMStatus',
				'AM'
			);
		}
		if ($LunchOut == 1) {
			$CheckField[] = array (
				'LunchOutTime',
				array (
					'AMStatus'
				/*	,'PMStatus'  */  // removed check PMStatus for lunch out time, should only check AMStatus is enough
				),
				'Lunch'
			);
		}
		if ($PM == 1) {
			$CheckField[] = array (
				'LunchBackTime',
				'PMStatus',
				'PM'
			);
		}
		if ($LeaveSchool == 1) {
			$CheckField[] = array (
				'LeaveSchoolTime',
				array (
					'AMStatus',
					'PMStatus'
				),
				'Out'
			);
		}

		include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
		include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

		$MODULE_OBJ['title'] = $i_StudentAttendance_Report_NoCardTab . ' (' . intranet_htmlspecialchars($StartDate) . ' ' . $Lang['General']['To'] . ' ' . intranet_htmlspecialchars($EndDate) . ')';
		$linterface = new interface_html("popup.html");
		$lexport = new libexporttext();

		$ReportData = $this->Get_No_Tapping_Card_Report_Data($StartDate, $EndDate, $TargetType, $OrderBy, $CheckField, $Details, $ClassName);

		$report_title = "$i_StudentAttendance_Report_NoCardTab - ";
		if ($TargetType == "Class") {
			$report_title .= $i_general_class;
		} else
			if ($TargetType == "Student") {
				$report_title .= $i_identity_student;
			}

		$display = "$report_title\n
										<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$csv_display = "\"" . $Lang['General']['Date'] . "\"\t\"" . $StartDate . ' ' . $Lang['General']['To'] . ' ' . $EndDate . "\"\r\n\r\n";
		# Iterate temp table
		if ($TargetType == "Class") {
			$csv_display .= "\"$i_ClassName\"\t\"$i_general_count\"\r\n";
			$display .= "<tr class=\"tablebluetop\">
													<td class=\"tabletoplink\">$i_ClassName</td>
													<td class=\"tabletoplink\">$i_general_count</td>\n";
			$result = $ReportData;

			$count_total = 0;
			for ($i = 0; $i < sizeof($result); $i++) {
				list ($t_name, $t_count) = $result[$i];
				$count_total += $t_count;
				$t_name = (trim($t_name) == "") ? "No Class" : $t_name;
				$css = ($i % 2 == 0) ? "tablebluerow1" : "tablebluerow2";
				$display .= "<tr class=\"$css\"><td>$t_name</td><td>$t_count</tr>\n";
				$csv_display .= "\"$t_name\"\t\"$t_count\"\r\n";
			}
			$display .= "<tr><td colspan=2></td></tr>\n";
			$display .= "<tr><td><b>$list_total</b></td><td><b>$count_total</b></td></tr>\n";
			$display .= "</table>\n";
		} else
			if ($TargetType == "Student") {
				$csv_display .= "\"$i_UserStudentName\"\t\"$i_ClassName\"\t\"$i_ClassNumber\"\t\"$i_general_count\"";
				$display .= "<tr class=\"tablebluetop\">
														<td class=\"tabletoplink\">$i_UserStudentName</td>
														<td class=\"tabletoplink\">$i_ClassName</td>
														<td class=\"tabletoplink\">$i_ClassNumber</td>
														<td class=\"tabletoplink\">$i_general_count</td>";
				$colspan = 4;
				if ($Details == 1) {
					if ($AM == 1) {
						$colspan++;
						$csv_display .= "\t\"" . $Lang['StudentAttendance']['DayTypeAM'] . "\"";
						$display .= "<td class=\"tabletoplink\" width=\"15%\">" . $Lang['StudentAttendance']['DayTypeAM'] . "</td>";
					}
					if ($LunchOut == 1) {
						$colspan++;
						$csv_display .= "\t\"" . $Lang['StudentAttendance']['LunchOut'] . "\"";
						$display .= "<td class=\"tabletoplink\" width=\"15%\">" . $Lang['StudentAttendance']['LunchOut'] . "</td>";
					}
					if ($PM == 1) {
						$colspan++;
						$csv_display .= "\t\"" . $Lang['StudentAttendance']['DayTypePM'] . "\"";
						$display .= "<td class=\"tabletoplink\" width=\"15%\">" . $Lang['StudentAttendance']['DayTypePM'] . "</td>";
					}
					if ($LeaveSchool == 1) {
						$colspan++;
						$csv_display .= "\t\"$i_StudentAttendance_LeaveSchool\"";
						$display .= "<td class=\"tabletoplink\" width=\"15%\">$i_StudentAttendance_LeaveSchool</td>";
					}

				}

				$csv_display .= "\r\n";
				$display .= "</tr>\n";

				$result = $ReportData[0];
				$records = $ReportData[1];

				$count_total = 0;
				for ($i = 0; $i < sizeof($result); $i++) {
					list ($t_userID, $t_name, $t_class, $t_classnum, $t_count) = $result[$i];
					$count_total += $t_count;
					$css = ($i % 2 == 0) ? "tablebluerow1" : "tablebluerow2";
					$display .= "<tr class=\"$css\">
								  							<td valign=\"top\">$t_name</td>
								  							<td valign=\"top\">$t_class</td>
								  							<td valign=\"top\">$t_classnum</td>
								  							<td valign=\"top\">$t_count</td>";
					$csv_display .= "\"$t_name\"\t\"$t_class\"\t\"$t_classnum\"\t\"$t_count\"";

					if ($Details == 1) {
						for ($j = 0; $j < sizeof($CheckField); $j++) {
							list ($TimeField, $StatusField, $Type) = $CheckField[$j];
							$t_datesInList = "";
							$t_datesInList_csv = "";
							if (is_array($records[$t_userID][$Type]) && sizeof($records[$t_userID][$Type]) > 0) {
								$t_datesInList = implode(", ", $records[$t_userID][$Type]);
								#$t_datesInList_csv = "\"".implode("\", \"",$records[$t_userID]["IN"])."\"";
								$t_datesInList_csv = implode(", ", $records[$t_userID][$Type]);
							}
							$display .= "<td valign=\"top\">$t_datesInList&nbsp;</td>";
							$csv_display .= "\t\"" . $t_datesInList_csv . "\"";
						}
					}
					$display .= "</tr>\n";
					$csv_display .= "\r\n";

				}
				$display .= "<tr><td colspan='$colspan'>&nbsp;</td></tr>\n";
				$display .= "<tr><td><b>$list_total</b></td><td>&nbsp;</td><td>&nbsp;</td><td><b>$count_total</b></td>";
				if ($Details == 1) {
					if ($AM == 1)
						$display .= "<td>&nbsp;</td>";
					if ($LunchOut == 1)
						$display .= "<td>&nbsp;</td>";
					if ($PM == 1)
						$display .= "<td>&nbsp;</td>";
					if ($LeaveSchool == 1)
						$display .= "<td>&nbsp;</td>";
				}
				$display .= "</tr>\n";
				$display .= "</table>\n";
			} else {
			}

		$display .= "<br><br>
										<table width='95%' border='0' class='print_hide'>
											<tr>
												<td align='center'>
												" . $linterface->GET_BTN($Lang['Btn']['Print'], "button", "window.print();", "") . "
												</td>
											</tr>
										</table>";

		if ($Format == "print") { # Web
			$i_title = $report_title;
			$linterface->LAYOUT_START();
			echo $display;
			$linterface->LAYOUT_STOP();
		} else
			if ($Format == "csv") { # CSV
				$file_name = "attendance_report_$StartDate_$EndDate.csv";
				//$export_content = $lexport->GET_EXPORT_TXT($Rows, $ExportColumn);
				$lexport->EXPORT_FILE($file_name, $csv_display);
			} else {
				echo "Error in parameters";
			}
	}

	function Get_Edit_eNotice_Template_Form($TemplateID = "") {
		global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $i_general_required_field;

		if ($TemplateID != "") {
			$TemplateDetail = $this->Get_Template_Detail($TemplateID);
			list ($Module, $CategoryID, $Title, $Subject, $Content, $ReplySlip, $RecordType, $RecordStatus, $ReplySlipContent, $SendReplySlip) = $TemplateDetail[0];
		}
		include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
		$linterface = new interface_html();

		$PAGE_NAVIGATION[] = array (
			$Lang['StudentAttendance']['eNoticeTemplate'],
			"index.php"
		);
		if ($TemplateID != "")
			$PAGE_NAVIGATION[] = array (
				$Lang['Btn']['Edit']
			);
		else
			$PAGE_NAVIGATION[] = array (
				$Lang['Btn']['New']
			);

		$catTmpArr = $this->eNoticeTemplateCategorySetting;
		if ($CategoryID == '') {
			$CategoryID = $catTmpArr[0][0];
		}

		if (is_array($catTmpArr)) {
			foreach ($catTmpArr as $Key => $Value) {
				$catArr[] = array (
					$Key,
					$Value
				);
			}
		}
		$catArr = array_merge(array (
			array (
				"",
				$Lang['SysMgr']['Homework']['PleaseSelect']
			)
		), $catArr);
		$catSelection = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID" name="CategoryID" onChange="changeGenVariables(this.value)"', "", $CategoryID);
		//$editReplySlipBtn = $linterface->GET_BTN($button_edit, 'button', "newWindow('editform.php',1)", $ParName, $ParOtherAttribute);
		$editReplySlipBtn = $linterface->GET_BTN($Lang['Btn']['Edit'], 'button', "newWindow('select_reply_slip_type.php',1)", $ParName, $ParOtherAttribute);
		$previewBtn = $linterface->GET_BTN($Lang['StudentAttendance']['eNoticeTemplatePreview'], "button", "newWindow('preview.php',10)");

		$x = '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/html_editor/fckeditor.js"></script>';
		$x .= '<form name="form1" method="post" action="update.php" onSubmit="return checkForm(form1)">
							<table width="98%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td align="center">
										<table width="100%" border="0" cellspacing="0" cellpadding="5">
											<tr>
												<td class="navigation">' . $linterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
											</tr>
										</table>
										<table width="88%" border="0" cellpadding="5" cellspacing="0">
											<tr>
												<td>
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
														<tr valign="top">
															<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">' . $Lang['StudentAttendance']['eNoticeTemplateName'] . '
															<span class="tabletextrequire">* </span></td>
															<td class="tabletext">
																<INPUT maxLength="80" name="Title" class="textboxtext" value="' . $Title . '">
															</td>
														</tr>
													</table>
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
														<tr valign="top">
															<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
																' . $Lang['StudentAttendance']['eNoticeTemplateCategory'] . '
																<span class="tabletextrequire">* </span>
															</td>
															<td class="tabletext">
																' . (($TemplateID == "") ? $catSelection : ($catTmpArr[$CategoryID]) . '<input type="hidden" name="CategoryID" id="CategoryID" value="' . $CategoryID . '">') . '
															</td>
														</tr>
													</table>
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
														<tr valign="top">
															<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
																' . $Lang['StudentAttendance']['eNoticeTemplateReplySlip'] . '
															</td>
															<td class="tabletext">
																<!--<INPUT type="checkbox" name="ReplySlip" id="ReplySlip" value="1" ' . (($ReplySlip || $ReplySlipContent) ? "checked" : "") . '>-->
																<INPUT type="checkbox" name="ReplySlip" id="ReplySlip" value="1" ' . (($SendReplySlip) ? "checked" : "") . '>
																' . $Lang['General']['Yes'] .' &nbsp;&nbsp;&nbsp;'. $editReplySlipBtn .' &nbsp;'. $previewBtn . '
															</td>
														</tr>
													</table>
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
														<tr valign="top">
															<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
																' . $Lang['StudentAttendance']['eNoticeTemplateSubject'] . '
															<span class="tabletextrequire">* </span></td>
															<td class="tabletext">
																<INPUT type="text" maxLength="80" name="Subject" id="Subject" class="textboxtext" value="' . $Subject . '">
															</td>
														</tr>
													</table>
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
														<tr valign="top">
															<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
															' . $Lang['StudentAttendance']['eNoticeTemplateStatus'] . '
															<span class="tabletextrequire">* </span></td>
															<td class="tabletext">
															<input type="radio" id="RecordStatus0" name="RecordStatus" value=0 ' . (($RecordStatus == "" || $RecordStatus == "0") ? "checked" : "") . '>
															<label for="RecordStatus0">' . $Lang['StudentAttendance']['eNoticeTemplateInActive'] . '</label>
															<input type="radio" id="RecordStatus1" name="RecordStatus" value=1 ' . (($RecordStatus == "1") ? "checked" : "") . '>
															<label for="RecordStatus1">' . $Lang['StudentAttendance']['eNoticeTemplateActive'] . '</label>
															</td>
														</tr>
													</table>
													<br><br>
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
														<tr valign="top">
															<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
															' . $Lang['StudentAttendance']['eNoticeTemplateContent'] . ' <span class="tabletextrequire">* </span></td>
															<td>
																<div id="genVariableDiv"></div>
															</td>
														</tr>
														<tr>
															<td colspan="2">';
		include_once ($PATH_WRT_ROOT . "templates/html_editor/fckeditor.php");
		$oFCKeditor = new FCKeditor('Content', "100%", "320", "", "", intranet_undo_htmlspecialchars($Content));
		$x .= $oFCKeditor->Create2();
		$x .= '								</td>
														</tr>
													</table>
													<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
														<tr>
															<td valign="top" nowrap="nowrap" class="tabletextremark">
																' . $i_general_required_field . '
															</td>
															<td width="80%">&nbsp;</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td height="1" class="dotline">
													<img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1">
												</td>
											</tr>
											<tr>
												<td align="right">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td align="center">
																<span class="dotline">
																' . $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit") . '&nbsp;
																' . $linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset") . '&nbsp;
																' . $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location='index.php'") . '
																</span></td>
														</tr>
													</table>
												</td>
											</tr>
										</table><br>
									</td>
								</tr>
							</table>
							<input type="hidden" name="qStr" value="' . $ReplySlip . '">
							<input type="hidden" name="aStr" value="">
							<input type="hidden" name="cStr" value="' . $ReplySlipContent . '">
							<input type="hidden" name="temp_form_display_alert" value="0">
							<input type="hidden" name="TemplateID" value="' . $TemplateID . '">
							</form>';
		$x .= "<script>
								$(document).ready( function() {
									changeGenVariables('" . $CategoryID . "');
								});
							</script>";
		return $x;
	}

	function Get_Confirm_Color_Table() {
		global $Lang;

		$x = '<table border=0>
								<tr>
									<td class="dynCalendar_card_not_confirmed" width="15px">
										&nbsp;
									</td>
									<td class="tabletext">
										' . $Lang['LessonAttendance']['LessonStatusAllNotConfirmed'] . '
									</td>
									<td class="dynCalendar_card_partial_confirmed" width="15px">
										&nbsp;
									</td>
									<td class="tabletext">
										' . $Lang['LessonAttendance']['LessonStatusPartialConfirmed'] . '
									</td>
									<td class="dynCalendar_card_confirmed" width="15px">
										&nbsp;
									</td>
									<td class="tabletext">
										' . $Lang['LessonAttendance']['LessonStatusAllConfirmed'] . '
									</td>
								</tr>
							</table>';
		return $x;
	}

	function Get_Student_Selection_List($ClassName = array (), $IsMulti = true, $DefaultChecked = true, $CheckedValue = array ()) {
		$StudentList = $this->Get_Student_By_Classes($ClassName);

		$Return = '<select name="StudentList[]" id="StudentList[]" size="10" style="min-width:200px;" ' . (($IsMulti) ? 'MULTIPLE' : '') . '>';
		for ($i = 0; $i < sizeof($StudentList); $i++) {
			list ($StudentID, $StudentName, $ClassName) = $StudentList[$i];

			if ($PrevClass != $ClassName) {
				if ($i != 0) {
					$Return .= '</optgroup>';
				}

				$Return .= '<optgroup label="' . $ClassName . '">';
			}
			if(in_array($StudentID, $CheckedValue))
				$DefaultChecked = true;
				
			$Return .= '<option value="' . $StudentID . '" ' . (($DefaultChecked) ? 'Selected' : '') . '>' . $StudentName . '</option>';
			
			if(in_array($StudentID, $CheckedValue))
				$DefaultChecked = false;
				
			if ($i == (sizeof($StudentList) - 1)) {
				$Return .= '</optgroup>';
			}
			$PrevClass = $ClassName;
		}
		$Return .= '</select>';

		return $Return;
	}

	function Get_Search_Report($startStr, $endStr, $attendance_type, $session, $reason, $match, $waived, $class_name = array (), $StudentList = array (), $format = '1', $ReportType=array(1), $ColumnOptions = array()) {
		global $Lang, $PATH_WRT_ROOT, $i_UserLogin, $i_UserStudentName, $i_ClassNameNumber;
		global $i_Attendance_Date, $i_Attendance_attendance_type, $i_Attendance_DayType;
		global $i_Attendance_Reason, $i_SmartCard_Frontend_Take_Attendance_Waived, $i_Attendance_Remark;
		global $i_Profile_Late, $i_Profile_Absent, $i_Profile_EarlyLeave, $i_general_yes, $i_general_no;
		global $i_StudentAttendance_Stat_Absent, $i_StudentAttendance_Stat_Late, $i_StudentAttendance_Stat_EarlyLeave, $i_SmartCard_Frontend_Take_Attendance_Waived;
		global $i_Profile_Attendance, $i_Profile_To, $i_general_report_creation_time, $i_ClassName, $i_StudentAttendance_Slot_AM, $i_StudentAttendance_Slot_PM;
		global $MODULE_OBJ;
		global $sys_custom;

		extract($ColumnOptions);
		
		$show_detail_records = in_array(1, $ReportType);
		$show_summary_stat = in_array(2, $ReportType);
		$show_class_summary_stat = in_array(3, $ReportType);

		$show_loginid = ($ColumnLoginID == "1" || $ShowAllColumns == "1");
		$show_studentname = ($ColumnStudentName == "1" || $ShowAllColumns == "1");
		$show_classname = ($ColumnClassName == "1" || $ShowAllColumns == "1");
		$show_classnumber = ($ColumnClassNumber == "1" || $ShowAllColumns == "1");
		$show_reason = ($ColumnReason == "1" || $ShowAllColumns == "1");
		$show_remark = ($ColumnRemark=="1" || $ShowAllColumns == "1");
		$show_office_remark = ($ColumnOfficeRemark == "1" || $ShowAllColumns == "1");
		$show_waived = ($ColumnWaived == "1" || $ShowAllColumns == "1");
		$show_prove_document = ($ColumnProveDocument == "1" || $ShowAllColumns == "1");
		//$show_summary_stat = ($ColumnSummaryStat == "1" || $ShowAllColumns == "1");
		//$show_class_summary_stat = (($ColumnClassSummaryStat == "1" || $ShowAllColumns == "1") && sizeof($StudentList) == 0);

		if (sizeof($class_name) <= 0 && sizeof($StudentList) <= 0)
			header("Location: search_report.php");

		if ($startStr == "" || $endStr == "") {
			$ts = strtotime(date('Y-m-d'));
			$startDate = getStartOfAcademicYear($ts);
			$endDate = getEndOfAcademicYear($ts);
		} else {
			$startDate = $startStr;
			$endDate = $endStr;
		}

		include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
		# Get Profile Count for Late/Absent/Earlyleave
		$GeneralSetting = new libgeneralsettings();
		$SettingList = array (
			"'ProfileAttendCount'"
		);
		$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance', $SettingList);
		$count_value = $Settings['ProfileAttendCount'] == 1 ? 0.5 : 1;
		$absent_count_value = $count_value;
		$late_count_value = 1;
		$early_leave_count_value = 1;

		list ($final_result, $attendance_data) = $this->Get_Search_Report_Data($startDate, $endDate, $session, $attendance_type, $match, $reason, $waived, $class_name, $StudentList);
		//debug_r($final_result);
		//debug_r($attendance_data);
		##################### output ######################
		# csv
		include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
		$lexport = new libexporttext();
		//$exportColumn = array("$i_UserLogin", "$i_UserStudentName", "$i_ClassNameNumber", "$i_Attendance_Date", "$i_Attendance_attendance_type", "$i_Attendance_DayType", "$i_Attendance_Reason", "$i_SmartCard_Frontend_Take_Attendance_Waived");
		
		$display = "";
			
		$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$display .= "<tr>";
		$display .= "<td align=\"right\" class=\"tabletext\">$i_general_report_creation_time: " . date('Y-m-d H:i:s') . "</td>";
		$display .= "</tr>";
		$display .= "</table>";
		if ($show_detail_records || $show_loginid || $show_studentname || $show_classname || $show_classnumber || $show_reason || $show_remark || $show_office_remark || $show_waived || $show_prove_document) {
			$exportColumn = array ();
			if ($show_loginid)
				$exportColumn[] = "$i_UserLogin";
			if ($show_studentname)
				$exportColumn[] = "$i_UserStudentName";
			if ($show_classname)
				$exportColumn[] = $Lang['StudentAttendance']['Class'];
			if($show_classnumber)
				$exportColumn[] = $Lang['StudentAttendance']['ClassNumber'];
			$exportColumn[] = "$i_Attendance_Date";
			$exportColumn[] = $Lang['StudentAttendance']['RecordTime'];
			$exportColumn[] = "$i_Attendance_attendance_type";
			if($sys_custom['StudentAttendance']['SeriousLate']) {
				$exportColumn[] = $Lang['StudentAttendance']['SeriousLate'];
			}
			$exportColumn[] = "$i_Attendance_DayType";
			if($show_reason) $exportColumn[] = "$i_Attendance_Reason";
			//if($show_remark) $exportColumn[] = "$i_Attendance_Remark";
			if($show_remark) $exportColumn[] = $Lang['StudentAttendance']['iSmartCardRemark'];
			if($show_office_remark) $exportColumn[] = $Lang['StudentAttendance']['OfficeRemark'];
			if($show_waived) $exportColumn[] = "$i_SmartCard_Frontend_Take_Attendance_Waived";
			if($show_prove_document) $exportColumn[] = $Lang['StudentAttendance']['ProveDocument'];

			# DISPLAY
			#$display  = "<link rel=\"stylesheet\" href=\"/templates/style.css\">";
			$col_span = 4;
			$display0 .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
										<tr class=\"tablebluetop\">";
			if ($show_loginid) {
				$display0 .= "<td class=\"tabletoplink\">$i_UserLogin</td>";
				$col_span += 1;
			}
			if ($show_studentname) {
				$display0 .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
				$col_span += 1;
			}
			if ($show_classname) {
				$display0 .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['Class']."</td>";
				$col_span += 1;
			}
			if ($show_classnumber) {
				$display0 .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['ClassNumber']."</td>";
				$col_span += 1;
			}
			$display0 .= "<td class=\"tabletoplink\">$i_Attendance_Date</td>";
			$display0 .= "<th class=\"tabletoplink\">" . $Lang['StudentAttendance']['RecordTime'] . "</th>";
			$display0 .= "<td class=\"tabletoplink\">$i_Attendance_attendance_type</td>";
			if($sys_custom['StudentAttendance']['SeriousLate']){
				$display0 .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['SeriousLate'] . "</td>";
				$col_span += 1;
			}
			$display0 .= "<td class=\"tabletoplink\">$i_Attendance_DayType</td>";
			if ($show_reason) {
				$display0 .= "<td class=\"tabletoplink\">$i_Attendance_Reason</td>";
				$col_span += 1;
			}
			//if($show_remark){$display0 .= "<td class=\"tabletoplink\">$i_Attendance_Remark</td>";$col_span+=1;}
			if($show_remark){$display0 .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['iSmartCardRemark']."</td>";$col_span+=1;}
			if($show_office_remark){$display0 .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['OfficeRemark']."</td>";$col_span+=1;}
			if ($show_waived) {
				$display0 .= "<td class=\"tabletoplink\">$i_SmartCard_Frontend_Take_Attendance_Waived</td>";
				$col_span += 1;
			}
			if($show_prove_document){
				$display0 .= "<td class=\"tabletoplink\">".$Lang['StudentAttendance']['ProveDocument']."</td>";
				$col_span += 1;
			}
			$display0 .= "</tr>";
		}
		if (sizeof($final_result) > 0) {
			$css = "tablebluerow2";
			$j = 0;

			$summary_data = array ();
			$class_summary_data = array ();
			for ($i = 0; $i < sizeof($final_result); $i++) {
				$uid = $final_result[$i]['UserID'];
				$u_login = $final_result[$i]['UserLogin'];
				$sname = $final_result[$i]['StudentName'];
				$cname = $final_result[$i]['ClassName'];
				$cnum = $final_result[$i]['ClassNumber'];
				$record_date = $final_result[$i]['RecordDate'];
				$attend_type = $final_result[$i]['AttendanceType'];
				$t_session = $final_result[$i]['Session'];
				$t_reason = $final_result[$i]['Reason'];
				$t_waived = $final_result[$i]['RecordStatus'];
				$t_absent_session = $final_result[$i]['AbsentSession'];
				$t_remark = $final_result[$i]['Remark'];
				$t_office_remark = $final_result[$i]['OfficeRemark'];
				$t_document_status = $final_result[$i]['DocumentStatus'];
				if($sys_custom['StudentAttendance']['SeriousLate']){
				    $seriousLate = $final_result[$i]['SeriousLate'];
				}
				
				if ($t_session == PROFILE_DAY_TYPE_AM) {
					if ($attendance_data[$uid][$record_date]['AM'] == $attend_type) {
					} else
						if ($attendance_data[$uid][$record_date]['EarlyLeave'] == PROFILE_TYPE_EARLY && $attend_type == PROFILE_TYPE_EARLY) {
						} else {
							continue;
						}
				}
				if ($t_session == PROFILE_DAY_TYPE_PM) {
					if ($attendance_data[$uid][$record_date]['PM'] == $attend_type) {
					} else
						if ($attendance_data[$uid][$record_date]['EarlyLeave'] == PROFILE_TYPE_EARLY && $attend_type == PROFILE_TYPE_EARLY) {
						} else {
							continue;
						}
				}

				$str_session = $t_session == PROFILE_DAY_TYPE_AM ? $i_StudentAttendance_Slot_AM : $i_StudentAttendance_Slot_PM;

				// init summary status
				$summary_data[$final_result[$i]['UserID']]['UserLogin'] = $final_result[$i]['UserLogin'];
				$summary_data[$final_result[$i]['UserID']]['StudentName'] = $final_result[$i]['StudentName'];
				$summary_data[$final_result[$i]['UserID']]['ClassName'] = $final_result[$i]['ClassName'];
				$summary_data[$final_result[$i]['UserID']]['ClassNumber'] = $final_result[$i]['ClassNumber'];
				if (!$summary_data[$final_result[$i]['UserID']]['TotalAbsent'])
					$summary_data[$final_result[$i]['UserID']]['TotalAbsent'] = 0;
				if (!$summary_data[$final_result[$i]['UserID']]['TotalLate'])
					$summary_data[$final_result[$i]['UserID']]['TotalLate'] = 0;
				if (!$summary_data[$final_result[$i]['UserID']]['TotalEarlyleave'])
					$summary_data[$final_result[$i]['UserID']]['TotalEarlyleave'] = 0;
				if (!$summary_data[$final_result[$i]['UserID']]['TotalAbsentWaived'])
					$summary_data[$final_result[$i]['UserID']]['TotalAbsentWaived'] = 0;
				if (!$summary_data[$final_result[$i]['UserID']]['TotalLateWaived'])
					$summary_data[$final_result[$i]['UserID']]['TotalLateWaived'] = 0;
				if (!$summary_data[$final_result[$i]['UserID']]['TotalEarlyleaveWaived'])
					$summary_data[$final_result[$i]['UserID']]['TotalEarlyleaveWaived'] = 0;
				if (!$summary_data[$final_result[$i]['UserID']]['TotalOutingWaived'])
					$summary_data[$final_result[$i]['UserID']]['TotalOutingWaived'] = 0;
				if ($sys_custom['StudentAttendance']['SeriousLate']){
				    if(!$summary_data[$final_result[$i]['UserID']]['TotalSeriousLate'])
				        !$summary_data[$final_result[$i]['UserID']]['TotalSeriousLate'] = 0;
				}
				if ($attend_type == CARD_STATUS_LATE) {
					$RecordTime = $t_session == PROFILE_DAY_TYPE_AM ? $attendance_data[$uid][$record_date]['InSchoolTime'] : $attendance_data[$uid][$record_date]['LunchBackTime'];
					if ($format == 1) {
						$RecordTime .= "&nbsp;";
					}
					$str_attend_type = $i_Profile_Late;

					// count summary 
					$summary_data[$final_result[$i]['UserID']]['TotalLate'] += $late_count_value;
					if ($final_result[$i]['RecordStatus'] == 1)
						$summary_data[$final_result[$i]['UserID']]['TotalLateWaived'] += $late_count_value;
					
					if($sys_custom['StudentAttendance']['SeriousLate']){
					    $summary_data[$final_result[$i]['UserID']]['TotalSeriousLate'] += $seriousLate;
					}

					// count class summary
					$class_summary_data[$final_result[$i]['ClassName']]['TotalLate'] += $late_count_value;
					if ($final_result[$i]['RecordStatus'] == 1)
						$class_summary_data[$final_result[$i]['ClassName']]['TotalLateWaived'] += $late_count_value;
					
					if($sys_custom['StudentAttendance']['SeriousLate']){
					    $class_summary_data[$final_result[$i]['ClassName']]['TotalSeriousLate'] += $seriousLate;
					}
				}
				else if ($attend_type == CARD_STATUS_ABSENT) {
					if ($format == 1) {
						$RecordTime = "&nbsp;";
					} else {
						$RecordTime = '';
					}
					$str_attend_type = $i_Profile_Absent;

					// count summary
					$summary_data[$final_result[$i]['UserID']]['TotalAbsent'] += $absent_count_value;
					if ($final_result[$i]['RecordStatus'] == 1)
						$summary_data[$final_result[$i]['UserID']]['TotalAbsentWaived'] += $absent_count_value;

					// count class summary
					$class_summary_data[$final_result[$i]['ClassName']]['TotalAbsent'] += $absent_count_value;
					if ($final_result[$i]['RecordStatus'] == 1)
						$class_summary_data[$final_result[$i]['ClassName']]['TotalAbsentWaived'] += $absent_count_value;
				}else if($attend_type == (CARD_STATUS_OUTING+CARD_STATUS_ABSENT)){
					if ($format == 1) {
						$RecordTime = "&nbsp;";
					} else {
						$RecordTime = '';
					}
					$str_attend_type = $Lang['StudentAttendance']['Outing'];

					// count summary
					$summary_data[$final_result[$i]['UserID']]['TotalOuting'] += $absent_count_value;
					//if ($final_result[$i]['RecordStatus'] == 1)
					//	$summary_data[$final_result[$i]['UserID']]['TotalOutingWaived'] += $absent_count_value;

					// count class summary
					$class_summary_data[$final_result[$i]['ClassName']]['TotalOuting'] += $absent_count_value;
					//if ($final_result[$i]['RecordStatus'] == 1)
					//	$class_summary_data[$final_result[$i]['ClassName']]['TotalOutingWaived'] += $absent_count_value;
				} else {
					$RecordTime = $attendance_data[$uid][$record_date]['LeaveSchoolTime'];
					if ($format == 1) {
						$RecordTime .= "&nbsp;";
					}
					$str_attend_type = $i_Profile_EarlyLeave;

					// count summary
					$summary_data[$final_result[$i]['UserID']]['TotalEarlyleave'] += $early_leave_count_value;
					if ($final_result[$i]['RecordStatus'] == 1)
						$summary_data[$final_result[$i]['UserID']]['TotalEarlyleaveWaived'] += $early_leave_count_value;

					// count class summary
					$class_summary_data[$final_result[$i]['ClassName']]['TotalEarlyleave'] += $early_leave_count_value;
					if ($final_result[$i]['RecordStatus'] == 1)
						$class_summary_data[$final_result[$i]['ClassName']]['TotalEarlyleaveWaived'] += $early_leave_count_value;
				}
				if ($show_detail_records || $show_loginid || $show_studentname || $show_classname || $show_classnumber || $show_reason || $show_remark || $show_waived || $show_prove_document) {
					$t_reason = intranet_undo_htmlspecialchars($t_reason);
					$str_waived = $str_attend_type==$Lang['StudentAttendance']['Outing']? Get_String_Display('') : ($t_waived == 1 ? $i_general_yes : $i_general_no);
					$str_prove_document = $str_attend_type==$Lang['StudentAttendance']['Outing']? Get_String_Display('') : ($attend_type==CARD_STATUS_ABSENT || $attend_type==PROFILE_TYPE_EARLY? ($t_document_status == '1'? $Lang['General']['Yes'] : $Lang['General']['No']) : Get_String_Display(''));
					$str_seriousLate = ($seriousLate == '1'? $Lang['General']['Yes'] : $Lang['General']['No']);
					$css = ($css == "tablebluerow2") ? "tablebluerow1" : "tablebluerow2";
					$row = "<tr class=\"$css\">";
					if ($show_loginid)
						$row .= "<Td class=tableContent$css>$u_login</td>";
					if ($show_studentname)
						$row .= "<Td class=tableContent$css>$sname</td>";
					if ($show_classname)
						$row .= "<td class=\"tabletext\">$cname" . "&nbsp;</td>";
					if ($show_classnumber)
						$row .= "<td class=\"tabletext\">".($cnum == "" ? "" : $cnum) . "&nbsp;</td>";
					$row .= "<Td class=\"tabletext\">$record_date</td>
												<Td class=\"tabletext\">$RecordTime</td>
												<Td class=\"tabletext\">$str_attend_type</td>";
					if($sys_custom['StudentAttendance']['SeriousLate']){
						$row .= "<td class=\"tabletext\">" . $str_seriousLate . "</td>";
					}
					$row .=	"<td class=\"tabletext\">$str_session</td>";
					if ($show_reason)
						$row .= "<Td class=\"tabletext\">$t_reason&nbsp;</td>";
					if($show_remark) $row .= "<Td class=\"tabletext\">$t_remark&nbsp;</td>";
					if($show_office_remark) $row .= "<Td class=\"tabletext\">$t_office_remark&nbsp;</td>";
					if ($show_waived)
						$row .= "<Td class=\"tabletext\">$str_waived</td>";
					if($show_prove_document){
						$row .= "<td class=\"tabletext\">$str_prove_document</td>";
					}
					$row .= "</tr>";
					$display0 .= $row;
					//$csv_content.="\"$u_login\",\"$sname\",\"$cname".($cnum==""?"":"($cnum)")."\",\"$record_date\",\"$str_attend_type\",\"$str_session\",\"$t_reason\",\"$str_waived\"\n";
					if ($show_loginid) {
						$csv_content .= "\"$u_login\"";
						$comma = ",";
					} else {
						$comma = "";
					}
					if ($show_studentname) {
						$csv_content .= "$comma\"$sname\"";
						$comma = ",";
					} else {
						$comma = "";
					}
					if ($show_classname) {
						$csv_content .= "$comma\"$cname" . "\"";
						$comma = ",";
					} else {
						$comma = "";
					}
					if ($show_classnumber) {
						$csv_content .= "$comma\"" . ($cnum == "" ? "" : $cnum) . "\"";
						$comma = ",";
					} else {
						$comma = "";
					}
					$csv_content .= "$comma\"$record_date\",\"$str_attend_type\"";
					if($sys_custom['StudentAttendance']['SeriousLate']){
						$csv_content .= ",\"".$str_seriousLate."\"";
					}
					$csv_content .= ",\"$str_session\"";
					if ($show_reason)
						$csv_content .= ",\"$t_reason\"";
					if($show_remark) $csv_content.=",\"$t_remark\"";
					if ($show_waived)
						$csv_content .= ",\"$str_waived\"";
					if($show_prove_document){
						$csv_content .= ",\"$str_prove_document\"";
					}
					$csv_content .= "\n";
					
					
					//$resultArray[$j] = array($u_login, $sname, $cname.($cnum==""?"":"($cnum)"), $record_date, $str_attend_type, $str_session, $t_reason, $str_waived);
					$resultArray[$j] = array ();
					if ($show_loginid)
						$resultArray[$j][] = $u_login;
					if ($show_studentname)
						$resultArray[$j][] = $sname;
					if ($show_classname)
						$resultArray[$j][] = $cname;
					if ($show_classnumber)
						$resultArray[$j][] = ($cnum == "" ? "" : "$cnum");
					$resultArray[$j][] = $record_date;
					$resultArray[$j][] = $RecordTime;
					$resultArray[$j][] = $str_attend_type;
					if($sys_custom['StudentAttendance']['SeriousLate']) {
						$resultArray[$j][] = $str_seriousLate;
					}
					$resultArray[$j][] = $str_session;
					if ($show_reason)
						$resultArray[$j][] = $t_reason;
					if($show_remark) $resultArray[$j][] = $t_remark;
					if($show_office_remark) $resultArray[$j][] = $t_office_remark;
					if ($show_waived)
						$resultArray[$j][] = $str_waived;
					if($show_prove_document){
						$resultArray[$j][] = $str_prove_document;
					}
					$j++;
				}
			}
		} else {
			if ($show_detail_records || $show_loginid || $show_studentname || $show_classname || $show_classnumber || $show_reason || $show_remark || $show_office_remark || $show_waived || $show_prove_document) {
				$display0 .= "<tr class=\"tablebluerow2\">";
				$display0 .= "<td class=\"tabletext\" colspan=\"$col_span\" align=\"center\">$i_no_record_exists_msg</td>";
				$display0 .= "</tr>";
			}
		}
		if ($show_detail_records || $show_loginid || $show_studentname || $show_classname || $show_classnumber || $show_reason || $show_remark || $show_office_remark || $show_waived || $show_prove_document) {
			$display0 .= "</table>";
		}

		if($show_detail_records){
			$display .= $display0; 
		}

		# Show Summary Statistic Of Total Number of Days of Absent/Late/Early Leave
		if ($show_summary_stat) {
			# build data array for summary statistic
			$exportSummaryColumn = array ();
			$exportSummaryResult = array ();

			# construct display table
			$display .= "<br />";
			$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$display .= "<tr>";
			$display .= "<td align=\"left\" class=\"tabletext\"><b>" . $Lang['StudentAttendance']['SummaryStatistic'] . "</b></td>";
			$display .= "</tr>";
			$display .= "</table>";
			$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$display .= "<tr class=\"tablebluetop\">";
			$display .= "<td class=\"tabletoplink\">$i_UserLogin</td>";
			$display .= "<td class=\"tabletoplink\">$i_ClassName</td>";
			$display .= "<td class=\"tabletoplink\">$i_ClassNumber</td>";
			$display .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
			$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Stat_Absent . "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")</td>";
			$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Stat_Late . "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")</td>";
			if($sys_custom['StudentAttendance']['SeriousLate']){
				$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['NumberOfSeriousLate'] . "</td>";
			}
			$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Stat_EarlyLeave . "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")</td>";
			$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['NumberOfOuting']. "</td>";
			$display .= "</tr>";

			$exportSummaryColumn[] = "$i_UserLogin";
			$exportSummaryColumn[] = "$i_ClassName";
			$exportSummaryColumn[] = "$i_ClassNumber";
			$exportSummaryColumn[] = "$i_UserStudentName";
			$exportSummaryColumn[] = $i_StudentAttendance_Stat_Absent;
			$exportSummaryColumn[] = "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")";
			$exportSummaryColumn[] = $i_StudentAttendance_Stat_Late;
			$exportSummaryColumn[] =  "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")";
			if($sys_custom['StudentAttendance']['SeriousLate']){
				$exportSummaryColumn[] = $Lang['StudentAttendance']['NumberOfSeriousLate'];
			}
			$exportSummaryColumn[] = $i_StudentAttendance_Stat_EarlyLeave;
			$exportSummaryColumn[] = "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")";
			$exportSummaryColumn[] = $Lang['StudentAttendance']['NumberOfOuting'];
			//$exportSummaryColumn[] = "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")";
			
			$css = "tablebluerow2";
			if (sizeof($summary_data) > 0) {
				$j = 0;
				foreach ($summary_data as $student_id => $value) {
					$css = ($css == "tablebluerow2") ? "tablebluerow1" : "tablebluerow2";
					$display .= "<tr class=\"$css\">";
					$display .= "<td>" . $summary_data[$student_id]['UserLogin'] . "</td>";
					$display .= "<td>" . $summary_data[$student_id]['ClassName'] . "</td>";
					$display .= "<td>" . $summary_data[$student_id]['ClassNumber'] . "</td>";
					$display .= "<td>" . $summary_data[$student_id]['StudentName'] . "</td>";
					$display .= "<td>" . $summary_data[$student_id]['TotalAbsent'] . "(" . $summary_data[$student_id]['TotalAbsentWaived'] . ")</td>";
					$display .= "<td>" . $summary_data[$student_id]['TotalLate'] . "(" . $summary_data[$student_id]['TotalLateWaived'] . ")</td>";
					if($sys_custom['StudentAttendance']['SeriousLate']){
						$display .= "<td>" . $summary_data[$student_id]['TotalSeriousLate'] . "</td>";
					}
					$display .= "<td>" . $summary_data[$student_id]['TotalEarlyleave'] . "(" . $summary_data[$student_id]['TotalEarlyleaveWaived'] . ")</td>";
					$display .= "<td>" . ($summary_data[$student_id]['TotalOuting']+0) . "</td>";
					$display .= "</tr>";

					$exportSummaryResult[$j] = array ();
					$exportSummaryResult[$j][] = $summary_data[$student_id]['UserLogin'];
					$exportSummaryResult[$j][] = $summary_data[$student_id]['ClassName'];
					$exportSummaryResult[$j][] = $summary_data[$student_id]['ClassNumber'];
					$exportSummaryResult[$j][] = $summary_data[$student_id]['StudentName'];
					$exportSummaryResult[$j][] = $summary_data[$student_id]['TotalAbsent'];
					$exportSummaryResult[$j][] = $summary_data[$student_id]['TotalAbsentWaived'];
					$exportSummaryResult[$j][] = $summary_data[$student_id]['TotalLate'];
					$exportSummaryResult[$j][] = $summary_data[$student_id]['TotalLateWaived'];
					if($sys_custom['StudentAttendance']['SeriousLate']){
						$exportSummaryResult[$j][] = $summary_data[$student_id]['TotalSeriousLate']+0;
					}
					$exportSummaryResult[$j][] = $summary_data[$student_id]['TotalEarlyleave'];
					$exportSummaryResult[$j][] = $summary_data[$student_id]['TotalEarlyleaveWaived'];
					$exportSummaryResult[$j][] = $summary_data[$student_id]['TotalOuting']+0;
					//$exportSummaryResult[$j][] = $summary_data[$student_id]['TotalOutingWaived'];
					
					$j++;
				}
			} else {
				$colspan = 8;
				if($sys_custom['StudentAttendance']['SeriousLate']){
					$colspan += 1;
				}
				$display .= "<tr class=\"tablebluerow2\">";
				$display .= "<td class=\"tabletext\" colspan=\"".$colspan."\" align=\"center\">$i_no_record_exists_msg</td>";
				$display .= "</tr>";
			}

			$display .= "</table>";
		}

		if ($show_class_summary_stat) {
			# build data array for summary statistic
			$exportClassSummaryColumn = array ();
			$exportClassSummaryResult = array ();
			
			$colspan = 5;
			if($sys_custom['StudentAttendance']['SeriousLate']){
				$colspan += 1;
			}
			
			# construct display table
			$display .= "<br />";
			$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$display .= "<tr>";
			$display .= "<td align=\"left\" class=\"tabletext\"><b>" . $Lang['StudentAttendance']['Class'] . ' ' . $Lang['StudentAttendance']['SummaryStatistic'] . "</b></td>";
			$display .= "</tr>";
			$display .= "</table>";
			$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
			$display .= "<tr class=\"tablebluetop\">";
			$display .= "<td class=\"tabletoplink\">$i_ClassName</td>";
			$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Stat_Absent . "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")</td>";
			$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Stat_Late . "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")</td>";
			if($sys_custom['StudentAttendance']['SeriousLate']){
				$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['NumberOfSeriousLate'] . "</td>";
			}
			$display .= "<td class=\"tabletoplink\">" . $i_StudentAttendance_Stat_EarlyLeave . "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")</td>";
			$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['NumberOfOuting'] . "</td>";
			$display .= "</tr>";

			$exportClassSummaryColumn[] = "$i_ClassName";
			$exportClassSummaryColumn[] = $i_StudentAttendance_Stat_Absent;
			$exportClassSummaryColumn[] = "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")";
			$exportClassSummaryColumn[] = $i_StudentAttendance_Stat_Late;
			$exportClassSummaryColumn[] = "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")";
			if($sys_custom['StudentAttendance']['SeriousLate']) {
				$exportClassSummaryColumn[] = $Lang['StudentAttendance']['NumberOfSeriousLate'];
			}
			$exportClassSummaryColumn[] = $i_StudentAttendance_Stat_EarlyLeave;
			$exportClassSummaryColumn[] = "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")";
			$exportClassSummaryColumn[] = $Lang['StudentAttendance']['NumberOfOuting'];

			//$exportClassSummaryColumn[] = "(" . $i_SmartCard_Frontend_Take_Attendance_Waived . ")";
			
			$css = "tablebluerow2";
			if (sizeof($class_summary_data) > 0) {
				$j = 0;
				foreach ($class_summary_data as $ClassName => $value) {
					$css = ($css == "tablebluerow2") ? "tablebluerow1" : "tablebluerow2";
					$display .= "<tr class=\"$css\">";
					$display .= "<td>" . $ClassName . "</td>";
					$display .= "<td>" . ($class_summary_data[$ClassName]['TotalAbsent'] + 0) . "(" . ($class_summary_data[$ClassName]['TotalAbsentWaived'] + 0) . ")</td>";
					$display .= "<td>" . ($class_summary_data[$ClassName]['TotalLate'] + 0) . "(" . ($class_summary_data[$ClassName]['TotalLateWaived'] + 0) . ")</td>";
					if($sys_custom['StudentAttendance']['SeriousLate']){
						$display .= "<td>" . ($class_summary_data[$ClassName]['TotalSeriousLate'] + 0) . "</td>";
					}
					$display .= "<td>" . ($class_summary_data[$ClassName]['TotalEarlyleave'] + 0) . "(" . ($class_summary_data[$ClassName]['TotalEarlyleaveWaived'] + 0) . ")</td>";
					$display .= "<td>" . ($class_summary_data[$ClassName]['TotalOuting'] + 0) . "</td>";
					$display .= "</tr>";

					$exportClassSummaryResult[$j] = array ();
					$exportClassSummaryResult[$j][] = $ClassName;
					$exportClassSummaryResult[$j][] = ($class_summary_data[$ClassName]['TotalAbsent'] + 0);
					$exportClassSummaryResult[$j][] = ($class_summary_data[$ClassName]['TotalAbsentWaived'] + 0);
					$exportClassSummaryResult[$j][] = ($class_summary_data[$ClassName]['TotalLate'] + 0);
					$exportClassSummaryResult[$j][] = ($class_summary_data[$ClassName]['TotalLateWaived'] + 0);
					if($sys_custom['StudentAttendance']['SeriousLate']){
						$exportClassSummaryResult[$j][]  = ($class_summary_data[$ClassName]['TotalSeriousLate'] + 0);
					}
					$exportClassSummaryResult[$j][] = ($class_summary_data[$ClassName]['TotalEarlyleave'] + 0);
					$exportClassSummaryResult[$j][] = ($class_summary_data[$ClassName]['TotalEarlyleaveWaived'] + 0);
					$exportClassSummaryResult[$j][] = ($class_summary_data[$ClassName]['TotalOuting'] + 0);
					//$exportClassSummaryResult[$j][] = ($class_summary_data[$ClassName]['TotalOutingWaived'] + 0);
					
					$j++;
				}
			} else {
				$display .= "<tr class=\"tablebluerow2\">";
				$display .= "<td class=\"tabletext\" colspan=\"".$colspan."\" align=\"center\">$i_no_record_exists_msg</td>";
				$display .= "</tr>";
			}

			$display .= "</table>";
		}

		if ($format == 1) { # Web
			$MODULE_OBJ['title'] = $i_Profile_Attendance . " ( " . intranet_htmlspecialchars($startDate) . " " . $i_Profile_To . " " . intranet_htmlspecialchars($endDate) . " )";
			$linterface = new interface_html("popup.html");
			$linterface->LAYOUT_START();
			echo "<br />";
			echo $display;
			echo "<br />";
			$linterface->LAYOUT_STOP();
		} else
			if ($format == 2) { # CSV
				if ($show_detail_records && ($show_loginid || $show_studentname || $show_classname || $show_classnumber || $show_reason || $show_remark || $show_waived || $show_prove_document)) {
					$export_content_final = "$i_Profile_Attendance ( $startDate $i_Profile_To $endDate )\t$i_general_report_creation_time: " . date('Y-m-d H:i:s') . "\r\n";
					$export_content_final .= $lexport->GET_EXPORT_TXT($resultArray, $exportColumn);
					$export_content_final .= "\r\n";
				}

				if ($show_summary_stat) {
					$export_content_final .= $Lang['StudentAttendance']['SummaryStatistic'] . "\r\n";
					$export_summary_content = $lexport->GET_EXPORT_TXT($exportSummaryResult, $exportSummaryColumn);
					$export_content_final .= $export_summary_content;
					$export_content_final .= "\r\n";
				}

				if ($show_class_summary_stat) {
					$export_content_final .= $Lang['StudentAttendance']['Class'] . ' ' . $Lang['StudentAttendance']['SummaryStatistic'] . "\r\n";
					$export_content_final .= $lexport->GET_EXPORT_TXT($exportClassSummaryResult, $exportClassSummaryColumn);
				}
				$filename = "attendance_report_$startDate_$endDate.csv";
				$lexport->EXPORT_FILE($filename, $export_content_final);
			}
	}

	function Get_Search_Sessions_Report($class_name = array (), $startStr = "", $endStr = "", $day_type = "", $report_type = 1, $se_late = 0, $se_request_leave = 0, $se_play_truant = 0, $se_official_leave = 0, $format = 1, $DisplayAllStudent = 0) {
		global $Lang, $PATH_WRT_ROOT, $module_version, $i_UserLogin, $i_UserStudentName, $i_ClassNameNumber, $i_Attendance_Date, $MODULE_OBJ, $i_general_report_creation_time, $i_From, $i_To;
		global $button_find, $i_StudentAttendance_Menu_Report, $i_Profile_To, $i_no_record_exists_msg, $i_StudentAttendance_Slot_AM, $i_StudentAttendance_Slot_PM;

		$StudentAttendance30 = $this->isStudentAttendance30();// $module_version['StudentAttendance'] == 3.0 || strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/StudentMgmt/eAttendance/')!==false;

		$selected_classes = implode("','", $class_name);
		$selected_classes = "'" . $selected_classes . "'";
		$classes = explode(",", $selected_classes);

		if ($startStr == "" || $endStr == "") {
			$ts = strtotime(date('Y-m-d'));
			$startDate = getStartOfAcademicYear($ts);
			$endDate = getEndOfAcademicYear($ts);
		} else {
			$startDate = $startStr;
			$endDate = $endStr;
		}

		$daytype_cond = "";
		if ($day_type == PROFILE_DAY_TYPE_AM) {
			$daytype_cond = " AND sc.DayType = '" . PROFILE_DAY_TYPE_AM . "' ";
		}
		if ($day_type == PROFILE_DAY_TYPE_PM) {
			$daytype_cond = " AND sc.DayType = '" . PROFILE_DAY_TYPE_PM . "' ";
		}

		$username_field = getNameFieldByLang('u.');

		$sql = "
					SELECT
						u.UserID,
						u.UserLogin,
						$username_field as StudentName,
						u.ClassName,
						u.ClassNumber,
						sc.RecordDate,
						sc.DayType,";
		if ($report_type == 1) // summary
			{
			$sql .= "SUM(IFNULL(sc.LateSession,'0')) as TotalLate,
								 SUM(IFNULL(sc.RequestLeaveSession,'0')) as TotalRequestLeave,
								 SUM(IFNULL(sc.PlayTruantSession,'0')) as TotalPlayTruant,
								 SUM(IFNULL(sc.OfficalLeaveSession,'0')) as TotalOfficialLeave ";
		} else // 2 = detail
			{
			$sql .= " sc.LateSession,
								  sc.RequestLeaveSession,
								  sc.PlayTruantSession,
								  sc.OfficalLeaveSession ";
		}
		$sql .= "
					FROM 
						INTRANET_USER AS u  ";
		if ($report_type == 2 || ($report_type == 1 && !$DisplayAllStudent)) {
			$sql .= "
							INNER JOIN 
							CARD_STUDENT_STATUS_SESSION_COUNT AS sc 
							ON  
								u.UserID = sc.StudentID 
								AND u.RecordType=2 
								AND u.RecordStatus IN(0,1,2) 
								AND u.ClassName IN($selected_classes) 
								AND sc.RecordDate between '$startDate' and '$endDate 23:59:59' ";
		} else {
			$sql .= "
							LEFT JOIN 
							CARD_STUDENT_STATUS_SESSION_COUNT AS sc 
							ON  
								u.UserID = sc.StudentID 
								AND sc.RecordDate between '$startDate' and '$endDate 23:59:59' 
							";
		}
		$sql .= "
					WHERE 
						1=1 ";
		if ($report_type == 1 && $DisplayAllStudent) {
			$sql .= "
							AND u.RecordType=2 
							AND u.RecordStatus IN(0,1,2) 
							AND u.ClassName IN($selected_classes) 
							";
		}
		$sql .= "
						$daytype_cond 
					";
		if ($report_type == 1) // summary
			{
			$sql .= "GROUP BY u.UserID ";
		}
		$sql .= "ORDER BY u.ClassName,u.ClassNumber+0,sc.RecordDate,sc.DayType ";
		$result = $this->returnArray($sql);

		$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$display .= "<tr>";
		$display .= "<td valign=\"bottom\" align=\"right\" class=\"tabletext\">$i_general_report_creation_time: " . date('Y-m-d H:i:s') . "</td>";
		$display .= "</tr>";
		$display .= "</table>";

		$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
								<tr class=\"tablebluetop\">";
		$display .= "<td class=\"tabletoplink\">#</td>";
		$display .= "<td class=\"tabletoplink\">$i_UserLogin</td>";
		$display .= "<td class=\"tabletoplink\">$i_UserStudentName</td>";
		$display .= "<td class=\"tabletoplink\">$i_ClassNameNumber</td>";
		if ($report_type == 2)
			$display .= "<td class=\"tabletoplink\">$i_Attendance_Date</td>";
		if ($report_type == 2)
			$display .= "<td class=\"tabletoplink\">$i_Attendance_DayType</td>";
		if ($se_late == 1)
			$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['NumberOfLate'] . "</td>";
		if ($se_request_leave == 1)
			$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['NumberOfRequestLeave'] . "</td>";
		if ($se_play_truant == 1)
			$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['NumberOfPlayTruant'] . "</td>";
		if ($se_official_leave == 1)
			$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['NumberOfOfficialLeave'] . "</td>";
		$display .= "</tr>";

		$exportResult = array ();

		$exportColumn = array ();
		$exportColumn[] = " ";
		$exportColumn[] = ($StudentAttendance30? $Lang['StudentAttendance']['AbsentSessionTable'].' ' : ($button_find . " (" . $Lang['StudentAttendance']['AbsentSessions'] . ") ")) . ($report_type == 1 ? $Lang['StudentAttendance']['Summary'] : $Lang['StudentAttendance']['Detail']) . " $i_StudentAttendance_Menu_Report ( $startDate $i_Profile_To $endDate )";
		$exportColumn[] = "$i_general_report_creation_time: " . date('Y-m-d H:i:s');
		$exportColumn[] = " ";
		$exportColumn[] = " ";
		if ($report_type == 2)
			$exportColumn[] = " ";
		if ($se_late == 1)
			$exportColumn[] = " ";
		if ($se_request_leave == 1)
			$exportColumn[] = " ";
		if ($se_play_truant == 1)
			$exportColumn[] = " ";
		if ($se_official_leave == 1)
			$exportColumn[] = " ";

		$exportRow = array ();
		$exportRow[] = "#";
		$exportRow[] = "$i_UserLogin";
		$exportRow[] = "$i_UserStudentName";
		$exportRow[] = "$i_ClassNameNumber";
		if ($report_type == 2)
			$exportRow[] = "$i_Attendance_Date";
		if ($report_type == 2)
			$exportRow[] = "$i_Attendance_DayType";
		if ($se_late == 1)
			$exportRow[] = $Lang['StudentAttendance']['NumberOfLate'];
		if ($se_request_leave == 1)
			$exportRow[] = $Lang['StudentAttendance']['NumberOfRequestLeave'];
		if ($se_play_truant == 1)
			$exportRow[] = $Lang['StudentAttendance']['NumberOfPlayTruant'];
		if ($se_official_leave == 1)
			$exportRow[] = $Lang['StudentAttendance']['NumberOfOfficialLeave'];
		$exportResult[] = $exportRow;

		$css = "tablebluerow2";
		if (sizeof($result) == 0) {
			$col_span = 5;
			if ($report_type == 2)
				$col_span += 1;
			if ($se_late == 1)
				$col_span += 1;
			if ($se_request_leave == 1)
				$col_span += 1;
			if ($se_play_truant == 1)
				$col_span += 1;
			if ($se_official_leave == 1)
				$col_span += 1;
			$display .= "<tr class=\"tablebluerow2\">";
			$display .= "<td class=\"tabletext\" colspan=\"$col_span\" align=\"center\">$i_no_record_exists_msg</td>";
			$display .= "</tr>";

			$exportRow = array ();
			$exportRow[] = " ";
			$exportRow[] = $i_no_record_exists_msg;
			$exportRow[] = " ";
			$exportRow[] = " ";
			$exportRow[] = " ";
			if ($report_type == 2)
				$exportRow[] = " ";
			if ($se_late == 1)
				$exportRow[] = " ";
			if ($se_request_leave == 1)
				$exportRow[] = " ";
			if ($se_play_truant == 1)
				$exportRow[] = " ";
			if ($se_official_leave == 1)
				$exportRow[] = " ";
			$exportResult[] = $exportRow;
		}
		$j = 1;
		for ($i = 0; $i < sizeof($result); $i++) {
			list ($uid, $login, $name, $classname, $classnum, $date, $daytype, $late_total, $requestleave_total, $playtruant_total, $officialleave_total) = $result[$i];
			if (!(($se_late == 1 && $late_total > 0) || ($se_request_leave == 1 && $requestleave_total > 0) || ($se_play_truant == 1 && $playtruant_total > 0) || ($se_official_leave == 1 && $officialleave_total > 0)) && (!$DisplayAllStudent))
				continue;
			$css = ($css == "tablebluerow2") ? "tablebluerow1" : "tablebluerow2";
			$row = "<tr class=\"$css\">";
			$row .= "<td class=tableContent$css>" . $j . "</td>";
			$row .= "<td class=tableContent$css>$login</td>";
			$row .= "<td class=tableContent$css>$name</td>";
			$row .= "<td class=tableContent$css>$classname" . (trim($classnum) != "" ? "($classnum)" : "") . "</td>";
			if ($report_type == 2) {
				$row .= "<td class=tableContent$css>$date</td>";
				$row .= "<td class=tableContent$css>" . ($daytype == PROFILE_DAY_TYPE_AM ? $i_StudentAttendance_Slot_AM : $i_StudentAttendance_Slot_PM) . "</td>";
			}
			if ($se_late == 1)
				$row .= "<td class=tableContent$css>$late_total</td>";
			if ($se_request_leave == 1)
				$row .= "<td class=tableContent$css>$requestleave_total</td>";
			if ($se_play_truant == 1)
				$row .= "<td class=tableContent$css>$playtruant_total</td>";
			if ($se_official_leave == 1)
				$row .= "<td class=tableContent$css>$officialleave_total</td>";
			$row .= "</tr>";
			$display .= $row;

			$exportRow = array ();
			$exportRow[] = $j;
			$exportRow[] = $login;
			$exportRow[] = $name;
			$exportRow[] = $classname . (trim($classnum) != "" ? "($classnum)" : "");
			if ($report_type == 2) {
				$exportRow[] = $date;
				$exportRow[] = ($daytype == PROFILE_DAY_TYPE_AM ? $i_StudentAttendance_Slot_AM : $i_StudentAttendance_Slot_PM);
			}
			if ($se_late == 1)
				$exportRow[] = $late_total;
			if ($se_request_leave == 1)
				$exportRow[] = $requestleave_total;
			if ($se_play_truant == 1)
				$exportRow[] = $playtruant_total;
			if ($se_official_leave == 1)
				$exportRow[] = $officialleave_total;
			$exportResult[] = $exportRow;

			$j++;
		}
		$display .= "</table>";

		if ($format == 1) { # Web
			$MODULE_OBJ['title'] = ($StudentAttendance30? $Lang['StudentAttendance']['AbsentSessionTable'].' ' : $button_find . " (" . $Lang['StudentAttendance']['AbsentSessions'] . ") ") . ($report_type == 1 ? $Lang['StudentAttendance']['Summary'] : $Lang['StudentAttendance']['Detail']) . " $i_StudentAttendance_Menu_Report ( $startDate $i_Profile_To $endDate )";
			$linterface = new interface_html("popup.html");
			$linterface->LAYOUT_START();
			echo "<br />";
			echo $display;
			echo "<br />";
			$linterface->LAYOUT_STOP();
		} else
			if ($format == 2) { # CSV
				include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
				$lexport = new libexporttext();
				$export_content = $lexport->GET_EXPORT_TXT($exportResult, $exportColumn);
				$export_content_final = $export_content;
				$filename = ($StudentAttendance30? "absent_session_" : "search_absent_session_" ). ($report_type == 1 ? "sumamry" : "detail") . "_report_$startDate_$endDate.csv";
				$lexport->EXPORT_FILE($filename, $export_content_final);
			}
	}

	function Get_No_Miss_Conduct_Report($StartDate, $EndDate, $IncludeWaive, $RecordType, $Format) {
		global $Lang, $PATH_WRT_ROOT, $i_general_report_creation_time, $MODULE_OBJ, $i_Attendance_attendance_type;

		if (!$IncludeWaive)
			$RecordStatusCond = "and (prr.RecordStatus != '1' || prr.RecordStatus IS NULL) ";
		if ($RecordType != PROFILE_TYPE_ABSENT) {
			if ($RecordType == PROFILE_TYPE_LATE || $RecordType == PROFILE_TYPE_EARLY)
				$RecordTypeCond = "and prr.RecordType = '" . $this->Get_Safe_Sql_Query($RecordType) . "' ";
			else
				$RecordTypeCond = "and prr.RecordType in ('" . PROFILE_TYPE_LATE . "','" . PROFILE_TYPE_EARLY . "') ";

			// Get Student which don't have any late/ early leave record first
			$sql = "
							SELECT
								u.UserID 
							FROM 
								INTRANET_USER AS u  
								LEFT JOIN 
								CARD_STUDENT_PROFILE_RECORD_REASON  AS prr 
								ON 
									u.UserID = prr.StudentID 
									AND prr.RecordDate between '" . $this->Get_Safe_Sql_Query($StartDate) . "' and '" . $this->Get_Safe_Sql_Query($EndDate) . " 23:59:59' 
									" . $RecordTypeCond . " 
									" . $RecordStatusCond . " 
							Where 
								u.RecordType = 2 
								AND u.RecordStatus IN(0,1,2) AND u.ClassName IS NOT NULL AND u.ClassName<>''  
							group by 
								u.UserID 
							having 
								count(prr.RecordID) = 0 
							ORDER BY 
								u.ClassName,u.ClassNumber+0";
			//debug_r($sql);
			$AwardStudent = $this->returnVector($sql); // student without late/ early leave or BOTH
		}

		if ($RecordType == PROFILE_TYPE_ABSENT || $RecordType == "") { // get absent also
			################# Build array of Attendance Data from Daily Log tables #################
			# CAL target Year months
			$year_month = array ();
			$start_ts = strtotime($StartDate);
			$end_ts = strtotime($EndDate);

			$temp_ts = $start_ts;
			while ($temp_ts <= $end_ts) {
				$year = date('Y', $temp_ts);
				$month = date('m', $temp_ts);
				$this->createTable_Card_Student_Daily_Log($year, $month);
				$year_month[] = array (
					$year,
					$month
				);
				$temp_ts = mktime(0, 0, 0, date('m', $temp_ts) + 1, 1, date('Y', $temp_ts));
			}

			for ($i = 0; $i < sizeof($year_month); $i++) {
				list ($year, $month) = $year_month[$i];
				$date_field = "CONCAT('" . $year . "','-','" . $month . "','-',IF(dailylog.DayNumber<=9,CONCAT('0',dailylog.DayNumber),dailylog.DayNumber)) ";
				
				// count AM absent
				$sql = "select 
													u.UserID 
												from 
													INTRANET_USER as u 
													LEFT JOIN 
													CARD_STUDENT_DAILY_LOG_" . $year . "_" . $month . " as dailylog 
													on 
														u.UserID = dailylog.UserID 
														and 
														" . $date_field . " between '" . $this->Get_Safe_Sql_Query($StartDate) . "' and '" . $this->Get_Safe_Sql_Query($EndDate) . " 23:59:59' 
														and 
														dailylog.AMStatus = '" . CARD_STATUS_ABSENT . "' ";
				if (!$IncludeWaive) {
					$sql .= "
														LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as prr 
														on 
															dailylog.UserID = prr.StudentID 
															and prr.RecordDate = " . $date_field . " 
															and prr.DayType='".PROFILE_DAY_TYPE_AM."' 
															and prr.RecordType = '" . PROFILE_TYPE_ABSENT . "' 
															and dailylog.AMStatus = '" . CARD_STATUS_ABSENT . "'
															" . $RecordStatusCond . " 
														";
				}
				$sql .= "
												where 
													u.RecordType = 2 
													AND u.RecordStatus IN(0,1,2) AND u.ClassName IS NOT NULL AND u.ClassName<>'' ";
				if (sizeof($AwardStudent) > 0) {
					$sql .= "
														AND 
														u.UserID in (" . implode(',', IntegerSafe($AwardStudent)) . ") ";
				}
				$sql .= "					
												group by 
													u.UserID 
												having 
													count(" . ((!$IncludeWaive) ? "prr.RecordID" : "dailylog.RecordID") . ") = 0";
				$AwardStudent = $this->returnVector($sql);

				if (sizeof($AwardStudent) == 0)
					break;
					
				// count PM absent
				$sql = "select 
													u.UserID 
												from 
													INTRANET_USER as u 
													LEFT JOIN 
													CARD_STUDENT_DAILY_LOG_" . $year . "_" . $month . " as dailylog 
													on 
														u.UserID = dailylog.UserID 
														and 
														" . $date_field . " between '" . $this->Get_Safe_Sql_Query($StartDate) . "' and '" . $this->Get_Safe_Sql_Query($EndDate) . " 23:59:59' 
														and 
														dailylog.PMStatus = '" . CARD_STATUS_ABSENT . "' ";
				if (!$IncludeWaive) {
					$sql .= "
														LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as prr 
														on 
															dailylog.UserID = prr.StudentID 
															and prr.RecordDate = " . $date_field . " 
															and prr.DayType='".PROFILE_DAY_TYPE_PM."' 
															and prr.RecordType = '" . PROFILE_TYPE_ABSENT . "' 
															and dailylog.PMStatus = '" . CARD_STATUS_ABSENT . "'
															" . $RecordStatusCond . " 
														";
				}
				$sql .= "
												where 
													u.RecordType = 2 
													AND u.RecordStatus IN(0,1,2) AND u.ClassName IS NOT NULL AND u.ClassName<>''  ";
				if (sizeof($AwardStudent) > 0) {
					$sql .= "
														AND 
														u.UserID in (" . implode(',', IntegerSafe($AwardStudent)) . ") ";
				}
				$sql .= "					
												group by 
													u.UserID 
												having 
													count(" . ((!$IncludeWaive) ? "prr.RecordID" : "dailylog.RecordID") . ") = 0";
				$AwardStudent = $this->returnVector($sql);

				if (sizeof($AwardStudent) == 0)
					break;
			}
		}

		//debug_r($AwardStudent);
		if (sizeof($AwardStudent) > 0) {
			$username_field = getNameFieldByLang();
			$sql = "select 
											" . $username_field . " as Name,
											ClassName,
											ClassNumber 
										from 
											INTRANET_USER 
										where 
											UserID in (" . implode(',', IntegerSafe($AwardStudent)) . ") 
										order by 
											ClassName, ClassNumber+0";
			$AwardStudentDetail = $this->returnArray($sql);
		}

		switch ($RecordType) {
			case PROFILE_TYPE_ABSENT :
				$Status = $Lang['StudentAttendance']['Absent'];
				break;
			case PROFILE_TYPE_EARLY :
				$Status = $Lang['StudentAttendance']['EarlyLeave'];
				break;
			case PROFILE_TYPE_LATE :
				$Status = $Lang['StudentAttendance']['Late'];
				break;
			default :
				$Status = $Lang['General']['All'];
				break;
		}

		$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$display .= "<tr>";
		$display .= "<td>
											<b>" . $i_Attendance_attendance_type . ":</b> " . $Status . "<br> 
											<b>" . $Lang['StudentAttendance']['IncludeWaive'] . ":</b> " . (($IncludeWaive) ? $Lang['General']['Yes'] : $Lang['General']['No']) . "
										</td>";
		$display .= "<td valign=\"bottom\" align=\"right\" class=\"tabletext\">$i_general_report_creation_time: " . date('Y-m-d H:i:s') . "</td>";
		$display .= "</tr>";
		$display .= "</table>";

		$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
								<tr class=\"tablebluetop\">";
		$display .= "<td class=\"tabletoplink\">#</td>";
		$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['StudentName'] . "</td>";
		$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['Class']."</td>";
		$display .= "<td class=\"tabletoplink\">" . $Lang['StudentAttendance']['ClassNumber']."</td>";
		$display .= "</tr>";

		$exportResult = array ();

		$exportColumn = array ();
		$exportColumn[] = " ";
		$exportColumn[] = $Lang['StudentAttendance']['NoMissConductReport'];

		$exportRow = array ();
		$exportRow[] = $i_general_report_creation_time;
		$exportRow[] = date('Y-m-d H:i:s');
		$exportResult[] = $exportRow;

		$exportRow = array ();
		$exportRow[] = $i_Attendance_attendance_type;
		$exportRow[] = $Status;
		$exportResult[] = $exportRow;

		$exportRow = array ();
		$exportRow[] = $Lang['StudentAttendance']['IncludeWaive'];
		$exportRow[] = ($IncludeWaive) ? $Lang['General']['Yes'] : $Lang['General']['No'];
		$exportResult[] = $exportRow;

		$exportRow = array ();
		$exportRow[] = "#";
		$exportRow[] = $Lang['StudentAttendance']['StudentName'];
		$exportRow[] = $Lang['StudentAttendance']['Class'];
		$exportRow[] = $Lang['StudentAttendance']['ClassNumber'];
		$exportResult[] = $exportRow;

		$css = "tablebluerow2";
		if (sizeof($AwardStudentDetail) == 0) {
			$col_span = 4;
			$display .= "<tr class=\"tablebluerow2\">";
			$display .= "<td class=\"tabletext\" colspan=\"$col_span\" align=\"center\">" . $Lang['General']['NoRecordFound'] . "</td>";
			$display .= "</tr>";

			$exportRow = array ();
			$exportRow[] = " ";
			$exportRow[] = $Lang['General']['NoRecordFound'];
			$exportRow[] = " ";
			$exportRow[] = " ";
			$exportRow[] = " ";
			$exportRow[] = " ";
			$exportResult[] = $exportRow;
		}
		$j = 1;
		for ($i = 0; $i < sizeof($AwardStudentDetail); $i++) {
			list ($StudentName, $ClassName, $ClassNumber) = $AwardStudentDetail[$i];
			$css = ($css == "tablebluerow2") ? "tablebluerow1" : "tablebluerow2";
			$row = "<tr class=\"$css\">";
			$row .= "<td class=tableContent$css>" . $j . "</td>";
			$row .= "<td class=tableContent$css>" . $StudentName . "</td>";
			$row .= "<td class=tableContent$css>" . $ClassName . "</td>";
			$row .= "<td class=tableContent$css>" . $ClassNumber . "</td>";
			$row .= "</tr>";
			$display .= $row;

			$exportRow = array ();
			$exportRow[] = $j;
			$exportRow[] = $StudentName;
			$exportRow[] = $ClassName;
			$exportRow[] = $ClassNumber;
			$exportResult[] = $exportRow;

			$j++;
		}
		$display .= "</table>";

		if ($Format == 1) { # Web
			$MODULE_OBJ['title'] = $Lang['StudentAttendance']['NoMissConductReport'] . " ( " . intranet_htmlspecialchars($StartDate) . " " . $Lang['General']['To'] . " " . intranet_htmlspecialchars($EndDate) . " )";
			$linterface = new interface_html("popup.html");
			$linterface->LAYOUT_START();
			echo "<br />";
			echo $display;
			echo "<br />";
			$linterface->LAYOUT_STOP();
		} else
			if ($Format == 2) { # CSV
				include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
				$lexport = new libexporttext();
				$export_content = $lexport->GET_EXPORT_TXT($exportResult, $exportColumn);
				$export_content_final = $export_content;
				$filename = "no_miss_conduct_report_" . $StartDate . "_" . $EndDate . ".csv";
				$lexport->EXPORT_FILE($filename, $export_content_final);
			}
	}

	function Get_Preset_Absence_Form() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import']
		);

		include_once ($PATH_WRT_ROOT . 'includes/libinterface.php');
		$libinterface = new interface_html();

		# step information
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['SelectCSVFile'],
			1
		);
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['CSVConfirmation'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['ImportResult'],
			0
		);
		
		if(get_client_region() == 'zh_TW') {
			$DataColumnTitleArr = $Lang['StudentAttendance']['PresetAbsenceImportDescTW'];
			$DataColumnPropertyArr = array(1,1,1,1,3,3,3,1,3,3);
			$csv_file = "sample_tw.csv";
		} else {
		$DataColumnTitleArr = $Lang['StudentAttendance']['PresetAbsenceImportDesc'];
			$DataColumnPropertyArr = array(1,1,1,1,3,1,1,3);
			$csv_file = "sample.csv";
		}
		$DataColumnTitleNum = count($DataColumnTitleArr);
		$DataColumnRemarksArr = array();
		for ($i = 0 ; $i< $DataColumnTitleNum ; $i++){
			if(get_client_region() == 'zh_TW') {
				$temp = $Lang['StudentAttendance']['PresetAbsenceImportRemarksTW'][$i];
			} else {
				$temp = $Lang['StudentAttendance']['PresetAbsenceImportRemarks'][$i];
			}
			if($temp!="") {
			$DataColumnRemarksArr[$i] = " <span class='tabletextremark'>".$temp."</span>";
			}
		}
		$DataColumn = $libinterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);
		
		$x .= '<br />
						<form name="form1" method="POST" action="import_confirm.php" enctype="multipart/form-data">
						<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>' . $libinterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
							</tr>
							<tr>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td>' . $libinterface->GET_STEPS($STEPS_OBJ) . '</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						<table>
						<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
									' . $Lang['Btn']['Select'] . '
								</td>
								<td width="70%"class="tabletext">
									<input type="file" class="file" name="userfile"><br />
								</td>
							</tr>
							<tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
								<td width="70%" <!--class="tabletextremark"-->
									' . $DataColumn . '<br /><br />
									<a class="tablelink" href="' . GET_CSV($csv_file) . '" target="_blank">' . $Lang['General']['ClickHereToDownloadSample'] . '</a>
								</td>
							</tr>
						</table>
						<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr>
						    	<td class="dotline"><img src="' . $PATH_WRT_ROOT . "images/" . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
						    </tr>
						</table>
						<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr>
							    <td align="center" colspan="2">
									' . $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . '
									' . $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location = 'new.php';", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . '
								</td>
							</tr>
						</table>
						</form>
						<br />';

		return $x;
	}

	function Get_Preset_Absence_Import_Confirm_PageTW($data)
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $i_Attendance_Type, $i_general_startdate, $i_general_enddate, $i_DayTypeWholeDay, $i_DayTypeAM, $i_DayTypePM, $i_DayTypeSession;

		include_once ($PATH_WRT_ROOT . 'includes/libinterface.php');
		$libinterface = new interface_html();

		$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import']
		);

		# step information
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['SelectCSVFile'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['CSVConfirmation'],
			1
		);
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['ImportResult'],
			0
		);

		$x = '<br/>
						<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>' . $libinterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
							</tr>
							<tr>
								<td>' . $libinterface->GET_STEPS($STEPS_OBJ) . '</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
						<div align="center">
						<table width="90%" border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['TotalRecord'] . '</td>
								<td class="tabletext">' . sizeof($data) . '</td>
							</tr>';
		if (sizeof($data) > 0) {
			$ErrorArray = $this->Import_Preset_AbsenceTW($data);
			$x .= '
								<tr>
									<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['SuccessfulRecord'] . '</td>
									<td class="tabletext">' . (sizeof($data) - sizeof($ErrorArray)) . '</td>
								</tr>
								<tr>
									<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['FailureRecord'] . '</td>
									<td class="tabletext">' . sizeof($ErrorArray) . '</td>
								</tr>
							</table>
							';
			$x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0">
									<tr>
										<td class="tablebluetop tabletopnolink" width="10">#</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['ClassName'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['ClassNumber'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $i_Attendance_Type . '</td>
										<td class="tablebluetop tabletopnolink">' . $i_general_startdate . '</td>
										<td class="tablebluetop tabletopnolink">' . $i_general_enddate . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['SessionFrom'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['SessionTo'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['LeaveTypeCode'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['Reason'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['General']['Remark'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['General']['Error'] . '</td>
									</tr>';
			for ($i = 0; $i < sizeof($ErrorArray); $i++) {
				$css_i = ($i % 2) ? "2" : "";
				$attendance_type_str = '';
				switch($ErrorArray[$i]['RecordDetail'][2]) {
					case 0:
						$attendance_type_str = $i_DayTypeWholeDay;
						break;
					case 1:
						$attendance_type_str = $i_DayTypeAM;
						break;
					case 2:
						$attendance_type_str = $i_DayTypePM;
						break;
					case 3:
						$attendance_type_str = $i_DayTypeSession;
						break;
				}

				$x .= '
											<tr>
												<td valign="top" class="tablebluerow' . $css_i . '" width="10">
													' . $ErrorArray[$i]['RowNumber'] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][0] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][1] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $attendance_type_str . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">';
				$x .= $ErrorArray[$i]['RecordDetail'][3];
				$x .= '	</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][4] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">';

				$x .= $ErrorArray[$i]['RecordDetail'][5];

				$x .='</td>
												<td valign="top" class="tablebluerow' . $css_i . '">';

				$x .= $ErrorArray[$i]['RecordDetail'][6];

				$x .='</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][7] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][8] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][9] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													<font style="color:red;">' . $ErrorArray[$i]['ErrorMsg'] . '</font>
												</td>
											</tr>';
			}
			$x .= '</table>';

			$x .= '
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td class="dotline"><img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							</tr>
							<tr>
								<td align="center">';
			if (sizeof($ErrorArray) == 0)
				$x .= $libinterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "window.location='import_confirm_update.php';");

			$x .= $libinterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
			$x .= '</td>
							</tr>
							</table>';
		} else {
			$x .= '</table>';

			$x .= '
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td class="dotline"><img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							</tr>
							<tr>
								<td align="center">';
			$x .= $libinterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
			$x .= '</td>
							</tr>
							</table>
							';
		}
		$x .= '</div>';

		return $x;
	}

	function Get_Preset_Absence_Import_Confirm_Page($data) {
		if(get_client_region() == 'zh_TW') {
			return $this->Get_Preset_Absence_Import_Confirm_PageTW($data);
		}

		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

		include_once ($PATH_WRT_ROOT . 'includes/libinterface.php');
		$libinterface = new interface_html();

		$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import']
		);

		# step information
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['SelectCSVFile'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['CSVConfirmation'],
			1
		);
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['ImportResult'],
			0
		);

		$x = '<br/>
						<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>' . $libinterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
							</tr>
							<tr>
								<td>' . $libinterface->GET_STEPS($STEPS_OBJ) . '</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
						<div align="center">
						<table width="90%" border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['TotalRecord'] . '</td>
								<td class="tabletext">' . sizeof($data) . '</td>
							</tr>';
		if (sizeof($data) > 0) {
			$ErrorArray = $this->Import_Preset_Absence($data);
			$x .= '
								<tr>
									<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['SuccessfulRecord'] . '</td>
									<td class="tabletext">' . (sizeof($data) - sizeof($ErrorArray)) . '</td>
								</tr>
								<tr>
									<td class="formfieldtitle" width="30%" align="left">' . $Lang['General']['FailureRecord'] . '</td>
									<td class="tabletext">' . sizeof($ErrorArray) . '</td>
								</tr>
							</table>
							';
			$x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0">
									<tr>
										<td class="tablebluetop tabletopnolink" width="10">#</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['ClassName'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['ClassNumber'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['General']['Date'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['DayType'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['Reason'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['Waived'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['StudentAttendance']['ProveDocument'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['General']['Remark'] . '</td>
										<td class="tablebluetop tabletopnolink">' . $Lang['General']['Error'] . '</td>
									</tr>';
			for ($i = 0; $i < sizeof($ErrorArray); $i++) {
				$css_i = ($i % 2) ? "2" : "";
				$x .= '
											<tr>
												<td valign="top" class="tablebluerow' . $css_i . '" width="10">
													' . $ErrorArray[$i]['RowNumber'] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][0] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][1] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][2] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">';
				if ($ErrorArray[$i]['RecordDetail'][3] == 2 || $ErrorArray[$i]['RecordDetail'][3] == 3) {
					$x .= ($ErrorArray[$i]['RecordDetail'][3] == 2) ? $Lang['StudentAttendance']['AM'] : $Lang['StudentAttendance']['PM'];
				} else {
					$x .= $ErrorArray[$i]['RecordDetail'][3];
				}
				$x .= '	</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][4] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">';
				if ($ErrorArray[$i]['RecordDetail'][5] == 0 || $ErrorArray[$i]['RecordDetail'][5] == 1){
					$x .= ($ErrorArray[$i]['RecordDetail'][5] == 1 )? $Lang['General']['Yes']:$Lang['General']['No'];
				}
				else{
					$x .= $ErrorArray[$i]['RecordDetail'][5];
				}
				$x .='</td>
												<td valign="top" class="tablebluerow' . $css_i . '">';
				if ($ErrorArray[$i]['RecordDetail'][6] == 0 || $ErrorArray[$i]['RecordDetail'][6] == 1){
					$x .= ($ErrorArray[$i]['RecordDetail'][6] == 1 )? $Lang['General']['Yes']:$Lang['General']['No'];
				}
				else{
					$x .= $ErrorArray[$i]['RecordDetail'][6];
				}
				$x .='</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													' . $ErrorArray[$i]['RecordDetail'][7] . '
												</td>
												<td valign="top" class="tablebluerow' . $css_i . '">
													<font style="color:red;">' . $ErrorArray[$i]['ErrorMsg'] . '</font>
												</td>
											</tr>';
			}
			$x .= '</table>';

			$x .= '
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td class="dotline"><img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							</tr>
							<tr>
								<td align="center">';
			if (sizeof($ErrorArray) == 0)
				$x .= $libinterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "window.location='import_confirm_update.php';");

			$x .= $libinterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
			$x .= '</td>
							</tr>
							</table>';
		} else {
			$x .= '</table>';

			$x .= '
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
							<tr>
								<td class="dotline"><img src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							</tr>
							<tr>
								<td align="center">';
			$x .= $libinterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
			$x .= '</td>
							</tr>
							</table>
							';
		}
		$x .= '</div>';

		return $x;
	}

	function Get_Preset_Absence_Import_Finish_Page() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

		include_once ($PATH_WRT_ROOT . 'includes/libinterface.php');
		$libinterface = new interface_html();

		if(get_client_region() == 'zh_TW') {
			$Result = $this->Finalize_Import_Preset_AbsenceTW();
		} else {
		$Result = $this->Finalize_Import_Preset_Absence();
		}

		$PAGE_NAVIGATION[] = array (
			$Lang['Btn']['Import']
		);

		# step information
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['SelectCSVFile'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['CSVConfirmation'],
			0
		);
		$STEPS_OBJ[] = array (
			$Lang['StaffAttendance']['ImportResult'],
			1
		);

		$x .= '<br />
							<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>' . $libinterface->GET_NAVIGATION($PAGE_NAVIGATION) . '</td>
								</tr>
								<tr>
									<td>' . $libinterface->GET_STEPS($STEPS_OBJ) . '</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
									' . $Result . ' ' . $Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'] . '
									</td>
								</tr>
								<tr>
									<td class="dotline">
										<img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" />
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td align="center">
										' . $libinterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='new.php';") . '
									</td>
								</tr>
							</table>';

		return $x;
	}

	function Get_Reason_Form($RecordID = "") {
		global $Lang, $PATH_WRT_ROOT;

		if ($RecordID != "") {
			$Temp = $this->Get_Outing_Reason($RecordID);
			$ReasonText = $Temp[0];
		}
		$x .= '<div class="edit_pop_board" style="height:400px;">
							<div class="edit_pop_board_write" style="height:374px;">
							<form name="LeaveReasonForm" id="LeaveReasonForm" onsubmit="return false;">
							<input type="hidden" name="RecordID" id="RecordID" value="' . $RecordID . '">
							<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
									<table class="form_table">
										<tr>
											<td>' . $Lang['StudentAttendance']['Reason'] . '</td>
											<td>:</td>
											<td>
												<input type="text" value="' . htmlspecialchars($ReasonText) . '" name="ReasonText" id="ReasonText" maxlength="255" size="30" onkeyup="Check_Reason();"/>
											</td> 
										</tr>
										<tr id="ReasonTextWarningRow" name="ReasonTextWarningRow" style="display:none;">
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td><div id="ReasonTextWarningLayer" style="color:red;"></div></td> 
										</tr>
										<col class="field_title" />
										<col class="field_c" />
									</table>
							</div>
							</form>
							</div>
							<div class="edit_bottom" style="height:10px;">
								<p class="spacer"></p>
								<input name="submit1" type="button" class="formbutton" onclick="Check_Reason(true);" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Save'] . '" />
								<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="' . $Lang['Btn']['Cancel'] . '" />
								<p class="spacer"></p>
							</div>
						</div>';
		return $x;
	}

	function Get_Absent_Present_Sex_Distribution_By_Class($TargetDate, $DayType, $ClassNames, $Format) {
		global $Lang, $PATH_WRT_ROOT, $i_general_report_creation_time, $MODULE_OBJ, $i_Attendance_DayType;

		$Data = $this->Get_Absent_Present_Sex_Distribution_By_Class_Data($TargetDate, $DayType, $ClassNames);

		$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
		$display .= "<tr>";
		$display .= "<td>
											<b>" . $i_Attendance_DayType . ":</b> " . (($DayType == PROFILE_DAY_TYPE_AM) ? $Lang['StudentAttendance']['AM'] : $Lang['StudentAttendance']['PM']) . "<br> 
										</td>";
		$display .= "<td valign=\"bottom\" align=\"right\" class=\"tabletext\">$i_general_report_creation_time: " . date('Y-m-d H:i:s') . "</td>";
		$display .= "</tr>";
		$display .= "</table>";

		$display .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">
										<tr class=\"tablebluetop\">";
		$display .= "<td class=\"tabletoplink\" rowspan=\"2\">" . $Lang['StudentAttendance']['ClassName'] . "</td>";
		$display .= "<td class=\"tabletoplink\" colspan=\"3\" style=\"text-align:center;\">" . $Lang['StudentAttendance']['NumberOfStudent'] . "</td>";
		$display .= "<td class=\"tabletoplink\" colspan=\"3\" style=\"text-align:center;\">" . $Lang['StudentAttendance']['NumberOfPresent'] . "</td>";
		$display .= "<td class=\"tabletoplink\" colspan=\"3\" style=\"text-align:center;\">" . $Lang['StudentAttendance']['NumberOfAbsent'] . "</td>";
		$display .= "<td class=\"tabletoplink\" rowspan=\"2\">" . $Lang['General']['Remark'] . "</td>";
		$display .= "</tr>";
		$display .= '<tr class="tablebluetop"> 
											<td class="tabletoplink" style="text-align:center;">' . $Lang['StudentAttendance']['Male'] . '</td>
											<td class="tabletoplink" style="text-align:center;">' . $Lang['StudentAttendance']['Female'] . '</td>
											<td class="tabletoplink" style="text-align:center;">' . $Lang['General']['Total'] . '</td>
											<td class="tabletoplink" style="text-align:center;">' . $Lang['StudentAttendance']['Male'] . '</td>
											<td class="tabletoplink" style="text-align:center;">' . $Lang['StudentAttendance']['Female'] . '</td>
											<td class="tabletoplink" style="text-align:center;">' . $Lang['General']['Total'] . '</td>
											<td class="tabletoplink" style="text-align:center;">' . $Lang['StudentAttendance']['Male'] . '</td>
											<td class="tabletoplink" style="text-align:center;">' . $Lang['StudentAttendance']['Female'] . '</td>
											<td class="tabletoplink" style="text-align:center;">' . $Lang['General']['Total'] . '</td>
										</tr>
										';

		$exportResult = array ();

		$exportColumn = array ();
		$exportColumn[] = " ";
		$exportColumn[] = $Lang['StudentAttendance']['ClassSexRatioReport'];

		$exportRow = array ();
		$exportRow[] = $i_general_report_creation_time;
		$exportRow[] = date('Y-m-d H:i:s');
		$exportResult[] = $exportRow;

		$exportRow = array ();
		$exportRow[] = $i_Attendance_DayType;
		$exportRow[] = (($DayType == PROFILE_DAY_TYPE_AM) ? $Lang['StudentAttendance']['AM'] : $Lang['StudentAttendance']['PM']);
		$exportResult[] = $exportRow;

		$exportRow = array ();
		$exportRow[] = $Lang['StudentAttendance']['ClassName'];
		$exportRow[] = $Lang['StudentAttendance']['NumberOfStudent'];
		$exportRow[] = "";
		$exportRow[] = "";
		$exportRow[] = $Lang['StudentAttendance']['NumberOfPresent'];
		$exportRow[] = "";
		$exportRow[] = "";
		$exportRow[] = $Lang['StudentAttendance']['NumberOfAbsent'];
		$exportRow[] = "";
		$exportRow[] = "";
		$exportRow[] = $Lang['General']['Remark'];
		$exportResult[] = $exportRow;

		$exportRow = array ();
		$exportRow[] = "";
		$exportRow[] = $Lang['StudentAttendance']['Male'];
		$exportRow[] = $Lang['StudentAttendance']['Female'];
		$exportRow[] = $Lang['General']['Total'];
		$exportRow[] = $Lang['StudentAttendance']['Male'];
		$exportRow[] = $Lang['StudentAttendance']['Female'];
		$exportRow[] = $Lang['General']['Total'];
		$exportRow[] = $Lang['StudentAttendance']['Male'];
		$exportRow[] = $Lang['StudentAttendance']['Female'];
		$exportRow[] = $Lang['General']['Total'];
		$exportResult[] = $exportRow;

		if (sizeof($Data) > 0) {
			foreach ($Data as $ClassName => $Detail) {
				$TotalM += $Detail["M"]["Total"];
				$TotalF += $Detail["F"]["Total"];
				$PresentM += $Detail["M"]["Present"];
				$PresentF += $Detail["F"]["Present"];
				$AbsentM += $Detail["M"]["Absent"];
				$AbsentF += $Detail["F"]["Absent"];

				$css = ($css == "tablebluerow2") ? "tablebluerow1" : "tablebluerow2";
				$row = "<tr class=\"$css\">";
				$row .= "<td class=tableContent$css>" . $ClassName . "</td>";
				$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($Detail["M"]["Total"] + 0) . "</td>";
				$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($Detail["F"]["Total"] + 0) . "</td>";
				$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($Detail["M"]["Total"] + $Detail["F"]["Total"]) . "</td>";
				$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($Detail["M"]["Present"] + 0) . "</td>";
				$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($Detail["F"]["Present"] + 0) . "</td>";
				$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($Detail["M"]["Present"] + $Detail["F"]["Present"]) . "</td>";
				$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($Detail["M"]["Absent"] + 0) . "</td>";
				$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($Detail["F"]["Absent"] + 0) . "</td>";
				$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($Detail["M"]["Absent"] + $Detail["F"]["Absent"]) . "</td>";
				$row .= "<td class=tableContent$css>&nbsp;</td>";
				$row .= "</tr>";
				$display .= $row;

				$exportRow = array ();
				$exportRow[] = $ClassName;
				$exportRow[] = ($Detail["M"]["Total"] + 0);
				$exportRow[] = ($Detail["F"]["Total"] + 0);
				$exportRow[] = ($Detail["M"]["Total"] + $Detail["F"]["Total"]);
				$exportRow[] = ($Detail["M"]["Present"] + 0);
				$exportRow[] = ($Detail["F"]["Present"] + 0);
				$exportRow[] = ($Detail["M"]["Present"] + $Detail["F"]["Present"]);
				$exportRow[] = ($Detail["M"]["Absent"] + 0);
				$exportRow[] = ($Detail["F"]["Absent"] + 0);
				$exportRow[] = ($Detail["M"]["Absent"] + $Detail["F"]["Absent"]);
				$exportRow[] = "";
				$exportResult[] = $exportRow;
			}
		}

		// Grand Total
		$css = "tablebluerow1";
		$row = "<tr class=\"$css\">";
		$row .= "<td class=tableContent$css>" . $Lang['General']['Total'] . "</td>";
		$row .= "<td class=tableContent$css style=\"text-align:center;\">" . $TotalM . "</td>";
		$row .= "<td class=tableContent$css style=\"text-align:center;\">" . $TotalF . "</td>";
		$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($TotalM + $TotalF) . "</td>";
		$row .= "<td class=tableContent$css style=\"text-align:center;\">" . $PresentM . "</td>";
		$row .= "<td class=tableContent$css style=\"text-align:center;\">" . $PresentF . "</td>";
		$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($PresentM + $PresentF) . "</td>";
		$row .= "<td class=tableContent$css style=\"text-align:center;\">" . $AbsentM . "</td>";
		$row .= "<td class=tableContent$css style=\"text-align:center;\">" . $AbsentF . "</td>";
		$row .= "<td class=tableContent$css style=\"text-align:center;\">" . ($AbsentM + $AbsentF) . "</td>";
		$row .= "<td class=tableContent$css>&nbsp;</td>";
		$row .= "</tr>";
		$display .= $row;
		$display .= '</table>';

		$exportRow = array ();
		$exportRow[] = $Lang['General']['Total'];
		$exportRow[] = $TotalM;
		$exportRow[] = $TotalF;
		$exportRow[] = ($TotalM + $TotalF);
		$exportRow[] = $PresentM;
		$exportRow[] = $PresentF;
		$exportRow[] = ($PresentM + $PresentF);
		$exportRow[] = $AbsentM;
		$exportRow[] = $AbsentF;
		$exportRow[] = ($AbsentM + $AbsentF);
		$exportRow[] = "";
		$exportResult[] = $exportRow;

		if ($Format == 1) { # Web
			$MODULE_OBJ['title'] = $Lang['StudentAttendance']['ClassSexRatioReport'] . " (" . intranet_htmlspecialchars($TargetDate) . ")";
			$linterface = new interface_html("popup.html");
			$linterface->LAYOUT_START();
			echo "<br />";
			echo $display;
			echo "<br />";
			$linterface->LAYOUT_STOP();
		} else
			if ($Format == 2) { # CSV
				include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");
				$lexport = new libexporttext();
				$export_content = $lexport->GET_EXPORT_TXT($exportResult, $exportColumn);
				$export_content_final = $export_content;
				$filename = "sex_ratio_report_" . $TargetDate . ".csv";
				$lexport->EXPORT_FILE($filename, $export_content_final);
			}
	}

	function Get_Academic_Year_Selection_List($Name = "", $ID = "", $OnChange = "", $Others = "") {
		$AcademicYearList = $this->Get_Academic_Year_List();

		$Return = '<select name="' . $Name . '" id="' . $ID . '" onchange="' . $OnChange . '" ' . $Others . '>';
		for ($i = 0; $i < sizeof($AcademicYearList); $i++) {
			list ($AcademicYearID, $AcadmicYearName, $IsCurrent) = $AcademicYearList[$i];

			$Return .= '<option value="' . $AcademicYearID . '" ' . (($IsCurrent == 1) ? "selected" : "") . '>' . $AcadmicYearName . '</option>';
		}
		$Return .= '</select>';

		return $Return;
	}

	function Get_Class_Selection_List_By_Academic_Year($AcademicYear, $Name = "", $ID = "", $OnChange = "", $Others = "") {
		global $Lang;

		$ClassList = $this->Get_Class_List_By_Academic_Year($AcademicYear);

		$Return = '<select name="' . $Name . '" id="' . $ID . '" onchange="' . $OnChange . '" ' . $Others . '>';
		$Return .= '<option value="">-- ' . $Lang['General']['All'] . ' --</option>';
		for ($i = 0; $i < sizeof($ClassList); $i++) {
			list ($ClassID, $ClassName) = $ClassList[$i];

			$Return .= '<option value="' . $ClassID . '">' . $ClassName . '</option>';
		}
		$Return .= '</select>';

		return $Return;
	}

	function Get_Search_Sessions_Report_Form($FromSmartCard = false) {
		global $Lang, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT, $i_general_Format, $i_general_startdate, $i_general_enddate;

		include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
		$linterface = new interface_html();

		# class list
		if ($FromSmartCard) {
			if ($this->ClassTeacherTakeOwnClassOnly == 1) {
				;
				include_once ($PATH_WRT_ROOT . "includes/libteaching.php");
				$lteaching = new libteaching();
				$class = $lteaching->returnTeacherClass($_SESSION['UserID']);
				for ($i = 0; $i < sizeof($class); $i++) {
					$ClassList[] = array (
						$class[$i][1],
						$class[$i][1]
					);
				}
				$select_class = $lteaching->getSelect($ClassList, ' name="class_name[]" id="class_name[]" multiple class="class_list" style="min-width:150px; width:200px;" onchange="Get_Student_List()"', $class_name, 1);
			} else {
				$select_class = $this->getSelectClass('name="class_name[]" id="class_name[]" multiple class="class_list" size="10" style="min-width:150px; width:200px;" onchange="Get_Student_List()"', $class_name, 1);
			}
		} else {
			$select_class = $this->getSelectClass('name="class_name[]" id="class_name[]" multiple class="class_list" size="10" style="min-width:150px; width:200px;"', $class_name, 1);
		}

		# report type
		$select_report_type = "<SELECT name=\"report_type\" onchange=\"if (this.value == '1') $('#DisplayAllStudentRow').css('display',''); else { $('#DisplayAllStudentRow').css('display','none'); $('#DisplayAllStudent').attr('checked',false); }\">\n";
		$select_report_type .= "<OPTION value=\"1\">" . $Lang['StudentAttendance']['Summary'] . "</OPTION>";
		$select_report_type .= "<OPTION value=\"2\">" . $Lang['StudentAttendance']['Detail'] . "</OPTION>\n";
		$select_report_type .= "</SELECT>\n";

		# date range
		$current_month = date('n');
		$current_year = date('Y');
		if ($current_month >= 9) {
			$startDate = date('Y-m-d', mktime(0, 0, 0, 9, 1, $current_year));
		} else {
			$startDate = date('Y-m-d', mktime(0, 0, 0, 9, 1, $current_year -1));
		}
		$endDate = date('Y-m-d');

		# day type
		$select_day_type = "<SELECT name=\"day_type\">\n";
		$select_day_type .= "<OPTION value=\"\">" . $Lang['Btn']['All'] . "</OPTION>\n";
		$select_day_type .= "<OPTION value=\"" . PROFILE_DAY_TYPE_AM . "\">" . $Lang['StudentAttendance']['AM'] . "</OPTION>\n";
		$select_day_type .= "<OPTION value=\"" . PROFILE_DAY_TYPE_PM . "\">" . $Lang['StudentAttendance']['PM'] . "</OPTION>\n";
		$select_day_type .= "</SELECT>\n";

		# format
		$select_format = "<SELECT name=\"format\">\n";
		$select_format .= "<OPTION value=\"1\">Web</OPTION>\n";
		$select_format .= "<OPTION VALUE=\"2\">CSV</OPTION>\n";
		$select_format .= "</SELECT>\n";

		$x = '<br />
							<form name="form1" action="search_absent_session_result.php" method="POST">
							<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $i_general_startdate . '</td>
									<td width="70%"class="tabletext">
										' . $linterface->GET_DATE_PICKER("startStr", $startDate) . '
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $i_general_enddate . '</td>
									<td width="70%"class="tabletext">
										' . $linterface->GET_DATE_PICKER("endStr", $endDate) . '
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['Btn']['Select'] . ' ' . $Lang['StudentAttendance']['ClassName'] . '</td>
									<td width="70%"class="tabletext">
										' . $select_class . '
										<span>' . $linterface->GET_SMALL_BTN($Lang['Btn']['All'], "button", "SelectAll(document.form1.elements['class_name[]']);return false;") . '</span>
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['DayType'] . '</td>
									<td width="70%"class="tabletext">
										' . $select_day_type . '
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentRegistry']['ReportType'] . '</td>
									<td width="70%"class="tabletext">
										' . $select_report_type . '
									</td>
								</tr>
								<tr id="DisplayAllStudentRow">
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['AbsenceSessionReport']['ShowAllStudent'] . '</td>
									<td width="70%"class="tabletext">
										<input id="DisplayAllStudent" name="DisplayAllStudent" type="checkbox" value="1">
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['StudentAttendance']['AbsentSessionType'] . '</td>
									<td width="70%"class="tabletext">
										<input id="se_late" name="se_late" type="checkbox" value="1" ><label for="se_late">' . $Lang['StudentAttendance']['Late'] . '</label></input>&nbsp;
										<input id="se_request_leave" name="se_request_leave" type="checkbox" value="1" ><label for="se_request_leave">' . $Lang['StudentAttendance']['RequestLeave'] . '</label></input>&nbsp;
										<input id="se_play_truant" name="se_play_truant" type="checkbox" value="1" ><label for="se_play_truant">' . $Lang['StudentAttendance']['PlayTruant'] . '</label></input>&nbsp;
										<input id="se_official_leave" name="se_official_leave" type="checkbox" value="1" ><label for="se_official_leave">' . $Lang['StudentAttendance']['OfficalLeave'] . '</label></input>
									</td>
								</tr>
								<tr>
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $i_general_Format . '</td>
									<td width="70%"class="tabletext">
										' . $select_format . '
									</td>
								</tr>
							</table>
							<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
								<tr>
							    	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							    </tr>
							</table>
							<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
								<tr>
								    <td align="center" colspan="2">
										' . $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm(document.form1);") . '
										' . $linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset", "", "reset2", " class='formbutton' onclick=\"document.getElementById('OptionsLayer').style.display='none';\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . '
									</td>
								</tr>
							</table>
							<input type="hidden" name="selected_classes" value="">
							</form>
							<br />';

		return $x;
	}

	function Get_Remove_Past_Data_Form($TargetDate = '', $TargetClass = '', $TargetDayType='') {
		global $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $i_status_all;
		include_once ("libinterface.php");

		if ($TargetDate == '')
			$TargetDate = date("Y-m-d");
		$linterface = new interface_html();
		$x = $this->Include_JS_CSS();

		$x .= '<form id="form1" name="form1" method="post" action="remove_update.php" onSubmit="CheckSubmit(this);return false;">' . "\n";
		$x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">' . "\n";
		$x .= '<tr><td>' . "\n";
		
		$x .= $linterface->GET_DATE_PICKER("TargetDate", $TargetDate);
		
		# Get Classes List
		$select_class = $this->getSelectClassID("name='ClassID' id='ClassID'",$TargetClass ,1,'', $i_status_all);
		$x .= $select_class . " ";
		
		# Day Type selection
		$x .= "<select name='TargetDayType' id='TargetDayType'>\n";
		$x .= "<option value='0'>". $Lang['StudentAttendance']['DayTypeWD'] ."</option>\n";
		$x .= "<option value='2' ". ($TargetDayType==2?"selected":"") .">". $Lang['StudentAttendance']['DayTypeAM'] ."</option>\n";
		$x .= "<option value='3' ". ($TargetDayType==3?"selected":"") .">". $Lang['StudentAttendance']['DayTypePM'] ."</option>\n";
		$x .= "</select>\n";
		
		$x .= $linterface->GET_SMALL_BTN($Lang['Btn']['View'], "button", "Get_Remove_Past_Data_Calendar()", "btnGo", "");
		$x .= '</td></tr>' . "\n";
		$x .= '<tr><td>' . "\n";
		$x .= '<div id="CalendarDiv">';
		$x .= $this->Get_Remove_Past_Data_Calendar($TargetDate, $TargetClass, $TargetDayType);
		$x .= '</div>' . "\n";
		$x .= '</td></tr>' . "\n";
		$x .= '</table>' . "\n";
		$x .= '<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr>
						    	<td class="dotline"><img src="' . "{$image_path}/{$LAYOUT_SKIN}/10x10.gif" . '" width="10" height="1" /></td>
						    </tr>
						    <tr>
							    <td align="center">
									' . $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "submit2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . '
								</td>
							</tr>
						</table>' . "\n";
		$x .= '</form>' . "\n";

		return $x;
	}

	function Get_Remove_Past_Data_Calendar($TargetDate = '', $TargetClass = '', $TargetDayType='') {
		global $Lang, $PATH_WRT_ROOT;
		include_once ("libcal.php");
		include_once ("libcalevent.php");

		$libcal = new libcal();
		if ($TargetDate == '')
			$TargetDate = date("Y-m-d");
		$date_arr = explode("-", $TargetDate);
		$TargetYear = $date_arr[0];
		$TargetMonth = $date_arr[1];

		$row = $libcal->returnCalendarArray($TargetMonth, $TargetYear);
		$cardlog_table = "CARD_STUDENT_DAILY_LOG_" . $TargetYear . "_" . $TargetMonth;

		##### Selected class?
		if($TargetClass)
		{
			$sql1 = "Select UserID From YEAR_CLASS_USER where YearClassID='". $TargetClass ."'";
			$sql1_result = $libcal->returnVector($sql1);
			$studentIdCond2 = " AND UserID IN ('".implode("','",$sql1_result)."') ";
		}
		
		##### Selected DayType?
		if(!empty($TargetDayType))
		{
			if($TargetDayType==2)	#AM
				$DayTypeCond = " AND AMStatus is not NULL";
			if($TargetDayType==3)	#PM
				$DayTypeCond = " AND PMStatus is not NULL";
		}
		
		$sql = "SELECT DISTINCT DayNumber FROM " . $cardlog_table . " WHERE 1 ". $studentIdCond2. $DayTypeCond. " ORDER BY DayNumber";
		/////debug_pr($sql);
		$DataDays = $this->returnVector($sql);

		$x .= '<div style="text-align:center;"><b>' . $TargetYear . "/" . $TargetMonth . '</b></div>' . "\n";
		# Month and Day Title
		$x .= '<table class="calendar_board" width="95%">
				          <tr class="week_title">
							<th><input type="checkbox" id="CheckAll" name="CheckAll" value="1" onClick="CheckAllDates(this,\'class_checkable\');"></th>
				            <td align="center" class="icalendar_weektitle " ><font color="red">&nbsp;' . $Lang['General']['DayType4'][0] . '</font></td>
				            <td align="center" class="icalendar_weektitle " >&nbsp;' . $Lang['General']['DayType4'][1] . '</td>
				            <td align="center" class="icalendar_weektitle " >&nbsp;' . $Lang['General']['DayType4'][2] . '</td>
				            <td align="center" class="icalendar_weektitle " >&nbsp;' . $Lang['General']['DayType4'][3] . '</td>
				            <td align="center" class="icalendar_weektitle " >&nbsp;' . $Lang['General']['DayType4'][4] . '</td>
				            <td align="center" class="icalendar_weektitle " >&nbsp;' . $Lang['General']['DayType4'][5] . '</td>
				            <td align="center" class="icalendar_weektitle " >&nbsp;' . $Lang['General']['DayType4'][6] . '</td>
				          </tr>';

		# Show Dates
		for ($j = 0; $j < sizeof($row); $j++) {
			$nth_row = $j / 7;
			if ($j % 7 == 0) {
				$x .= "<tr id=\"row_" . $nth_row . "\">\n";
				$x .= '<td><input type="checkbox" value="1" onClick="CheckRowDates(this,\'row_' . $nth_row . '\');" /></td>';
			}

			if (!empty ($row[$j])) {
				$DayOfMonth = date("d", $row[$j]);
				$DayOfMonthNozero = date("j", $row[$j]);
				$Date = date("Y-m-d", $row[$j]);
				$span_class = 'class="day_no"';
				$checkbox = '&nbsp;';
				if (in_array($DayOfMonthNozero, $DataDays)) {
					$span_class = 'class="roster_day_no roster_day_no_special"';
					$checkbox = '<input type="checkbox" name="dates[]" value="' . $Date . '" class="class_checkable" />';
				}

				$x .= '<td valign="top">
					        		 	<span ' . $span_class . '>' . $DayOfMonth;
				$x .= $checkbox;
				$x .= '</span>';
				$x .= '</td>';

			} else {
				$x .= '<td class="day_sunday day_dummy">&nbsp;</td>';
			}

			$x .= ($j % 7 == 6) ? "</tr>\n" : "";
		}

		$x .= '</table><br />' . "\n";

		$x .= '<input type="hidden" name="TargetYear" value="' . $TargetYear . '" >' . "\n";
		$x .= '<input type="hidden" name="TargetMonth" value="' . $TargetMonth . '" >' . "\n";

		$x .= '<div class="event_list_title title_roster_special">
		    				<span class="event_type_icon">&nbsp;</span>
		    				<span>' . $Lang['StudentAttendance']['DataExist'] . '</span>
		    			</div><br /><br />' . "\n";

		$x .= '<table width="95%" cellspacing="0" cellpadding="3" border="0" align="left">' . "\n";
		$x .= '<tr><td class="tabletextremark"><span style="color:red;">' . $Lang['StudentAttendance']['Warning']['RemoveWholeDayDataRemark'] . '</span></td></tr>';
		$x .= '</table>' . "\n";

		return $x;
	}

	#Rita add 17-07-2012
	function Get_Class_Monthly_Attendance_Report_By_Date_Range($Classes, $Year, $Month, $Columns, $HideNoData, $IsGroup = false, $AcademicYear = "", $StudentStatus = "0,1,2", $startDate, $endDate, $PrintVersion = 0, $ContinuousAbsentDay = 0) {
		global $PATH_WRT_ROOT, $Lang, $sys_custom;
		global $i_SmartCard_GroupName, $intranet_session_language;

		include_once ($PATH_WRT_ROOT . "includes/global.php");
		include_once ($PATH_WRT_ROOT . "includes/libdb.php");

		//$attendance_mode = $this->attendance_mode;
		$count_value = $this->ProfileAttendCount == 1 ? 0.5 : 1;
		$absent_count_value = $count_value;
		$present_count_value = $count_value;
		$late_count_value = 1;
		$early_leave_count_value = 1;

		$reason_count_with_absent_session = isset($sys_custom['SmartCardAttendance_ReasonCountAbsentSession']) && $sys_custom['SmartCardAttendance_ReasonCountAbsentSession'];

		$Start = explode('-', $startDate);
		$StartYear = $Start[0];
		$StartMonth = $Start[1];
		$Date = $Start[2];
		$Date = intval($Date);
		

		#set colWidth for attandance data
		if ($intranet_session_language=='en') {
			$colWidth = 40;
		}
		else {
			$colWidth = 30;
		}
		
		#for for loop  use
		$iniVal = strtotime($startDate);
		$target = strtotime($endDate);

		$a = date('d', $iniVal);
		$a = intval($a) - 1;

		# get user customized status symbols
		$sql = "SELECT StatusID, StatusSymbol FROM CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL";
		$SymbolsArr = $this->returnArray($sql);
		$notation_symbol[0] = ($SymbolsArr[0]['StatusSymbol'] == NULL || $SymbolsArr[0]['StatusSymbol'] == "") ? $Lang['StudentAttendance']['SymbolPresent'] : $SymbolsArr[0]['StatusSymbol'];
		$notation_symbol[1] = ($SymbolsArr[1]['StatusSymbol'] == NULL || $SymbolsArr[1]['StatusSymbol'] == "") ? $Lang['StudentAttendance']['SymbolAbsent'] : $SymbolsArr[1]['StatusSymbol'];
		$notation_symbol[2] = ($SymbolsArr[2]['StatusSymbol'] == NULL || $SymbolsArr[2]['StatusSymbol'] == "") ? $this->DefaultAttendanceSymbol[CARD_STATUS_LATE] : $SymbolsArr[2]['StatusSymbol'];
		$notation_symbol[3] = $Lang['StudentAttendance']['SymbolOuting'];
		$notation_symbol[4] = ($SymbolsArr[3]['StatusSymbol'] == NULL || $SymbolsArr[3]['StatusSymbol'] == "") ? $Lang['StudentAttendance']['SymbolEarlyLeave'] : $SymbolsArr[3]['StatusSymbol'];
		$notation_symbol[5] = $Lang['StudentAttendance']['SymbolSL'];
		$notation_symbol[6] = $Lang['StudentAttendance']['SymbolAR'];
		$notation_symbol[7] = $Lang['StudentAttendance']['SymbolLE'];
		$notation_symbol[8] = $Lang['StudentAttendance']['SymbolTruancy'];
		$notation_symbol[9] = ($SymbolsArr[4]['StatusSymbol'] == NULL || $SymbolsArr[4]['StatusSymbol'] == "") ? $Lang['StudentAttendance']['SymbolWaived'] : $SymbolsArr[4]['StatusSymbol'];

		# get reason symbols
		$absent_reason_symbol = $this->Get_Reason_Symbol(PROFILE_TYPE_ABSENT);
		$late_reason_symbol = $this->Get_Reason_Symbol(PROFILE_TYPE_LATE);
		$earlyleave_reason_symbol = $this->Get_Reason_Symbol(PROFILE_TYPE_EARLY);

		# set table name for sql 

		if (strtotime($startDate) == strtotime($endDate)) {

			$year = date('Y', strtotime($startDate));
			$month = date('m', strtotime($startDate));
			$this->createTable_Card_Student_Daily_Log($year, $month);
			$table[] = "CARD_STUDENT_DAILY_LOG_" . $year . _ . $month;
		} else {
			$table = array ();
			$montharr = $this->Get_Class_Monthly_Attendance_Report_By_Date_Range_Get_Month($startDate, $endDate);
			foreach (array_keys($montharr) as $year) {
				foreach ($montharr[$year] as $month) {
					$this->createTable_Card_Student_Daily_Log($year, $month);
					$temp_table = "CARD_STUDENT_DAILY_LOG_" . $year . _ . $month;
					$table[] = $temp_table;
				}
			}
		}

		$this->retrieveSettings();
		$months = $Lang['StudentAttendance']['MonthShortForm'];
		if ($Year == "") {
			$Year = date('Y');
		}
		if ($Month == "") {
			$Month = date('m');
		}
		if (strlen($Month) == 1) {
			$Month = "0" . $Month;
		}

		$IncludeLeftArchivedStudents = strstr($StudentStatus, "3") !== FALSE;

		$content = "";
		for ($z = 0; $z < sizeof($Classes); $z++) {
			$attendance_mode = $this->attendance_mode;
			#improvement hide AM/PM header in AM/PM mode
			if ($attendance_mode == '0' || $attendance_mode == '1' ){
				$Set_rowspan = 1;
				$Set_rowspan2 = 2;
			}
			else{
				$Set_rowspan = 2;
				$Set_rowspan2 = 3;
			}

			if($sys_custom['StudentAttendance']['KIS_AttendanceUseClassGroup']) {
				$YearClass = new year_class($Classes[$z]['ClassID'], true, true, true);
				$ClassGroupInfo = $YearClass->Get_Class_Group_Info();
				$class_attendance_mode = '';
				if(is_array($ClassGroupInfo)) {
					$class_attendance_type = $ClassGroupInfo[0]['AttendanceType'];
				}

				if($class_attendance_type == '1') {
					$attendance_mode = 0;
					$Set_rowspan = 2;
					$Set_rowspan2 = 3;
				} else if($class_attendance_type == '2') {
					$attendance_mode = 1;
					$Set_rowspan = 2;
					$Set_rowspan2 = 3;;
				} else if($class_attendance_type == '3') {
					$attendance_mode = 2;
					$Set_rowspan = 2;
					$Set_rowspan2 = 3;
				} else if($class_attendance_type == '') {
					$Set_rowspan = 2;
					$Set_rowspan2 = 3;
				}
			}
			# Get Student List
			if ($IsGroup) {
				$sql = "SELECT a.UserID, a.ClassNumber, a.ChineseName, a.EnglishName, a.Gender, a.Remark, a.RecordStatus, '0' as IsArchivedUser FROM INTRANET_USER a, INTRANET_GROUP b, INTRANET_USERGROUP c WHERE a.RecordStatus IN (" . IntegerSafe($StudentStatus) . ") AND a.RecordType = 2 AND b.groupid = '" . IntegerSafe($Classes[$z]['ClassID']) . "' AND b.groupid = c.groupid AND c.userid = a.userid ORDER BY a.ClassName, a.ClassNumber+0, a.EnglishName";
				$students = $this->returnArray($sql, 8);
			} else {
			//	if ($AcademicYear == "" || ($lstudentattendance_ui->Platform == "IP" && $AcademicYear == Get_Current_Academic_Year_ID())) {
			//		$sql = "SELECT UserID, ClassNumber, ChineseName, EnglishName, Gender, Remark, RecordStatus, '0' as IsArchivedUser FROM INTRANET_USER WHERE ClassName = '" . $Classes[$z]['ClassName'] . "' AND RecordStatus IN (0,1,2) AND RecordType = 2 ORDER BY ClassName, ClassNumber+0, EnglishName";
			//		$students = $this->returnArray($sql, 8);
			//	} else {
					if ($this->Platform == "IP") {
						$students = $this->Get_Student_List_By_Class_Academic_Year($Classes[$z]['ClassID'], $AcademicYear);
					} else { // EJ
						$students = $this->Get_Student_List_By_Class_Academic_Year($Classes[$z]['ClassName'], $AcademicYear);
					}
			//	}
			}

			unset ($StudentIDs);
			for ($i = 0; $i < sizeof($students); $i++) {
				$StudentIDs[] = $students[$i]['UserID'];
			}

			#Get array from sql query.
			$tmp_total_day = array ();
			$total_day = array ();

			for ($i = 0; $i < sizeof($table); $i++) {
				$parts = explode("_", $table[$i]);
				$tmp_year = $parts[4];
				$tmp_month = $parts[5];

				$sql = "SELECT 
											DISTINCT DATE_FORMAT(CONCAT('$tmp_year-$tmp_month-',a.DayNumber),'%Y-%m-%d') as DayNumber  
										FROM 
											" . $table[$i] . " AS a 
											LEFT OUTER JOIN 
											INTRANET_USER AS b ON (a.UserID=b.UserID) 
											LEFT OUTER JOIN 
											INTRANET_ARCHIVE_USER AS au ON au.UserID = a.UserID 
										WHERE 
											(DATE_FORMAT(CONCAT('$tmp_year-$tmp_month-',a.DayNumber),'%Y-%m-%d') BETWEEN '".$this->Get_Safe_Sql_Query($startDate)."' AND '".$this->Get_Safe_Sql_Query($endDate)."') 
											AND (b.RecordType=2 OR au.RecordType = 2)  
											AND 
											a.UserID in (" . ((sizeof($StudentIDs) > 0) ? implode(',', IntegerSafe($StudentIDs)) : '-1') . ")
											AND (b.RecordStatus IN (" . IntegerSafe($StudentStatus) . ") OR au.UserID IS NOT NULL)";

				$tmp_total_day = $this->returnVector($sql, 1);
				
				for ($j = 0; $j < sizeof($tmp_total_day); $j++) {
					$total_day[] = array (
						'DayNumber' => $tmp_total_day[$j]
					);
				}
			}
			$day_numbers_ary = Get_Array_By_Key($total_day,'DayNumber'); // school days array

			// ###### Prepare preset absence records that after today ######
			$tomorrow_date = date("Y-m-d", time() + 86400);
			$tmp_preset_absent_records = $this->Get_Preset_Absent_Student_Records($StudentIDs, $tomorrow_date, $endDate);
			$preset_absent_data = array ();
			for ($i = 0; $i < count($tmp_preset_absent_records); $i++) {
				$tmp_student_id = $tmp_preset_absent_records[$i]['StudentID'];
				$tmp_day = $tmp_preset_absent_records[$i]['RecordDate'];
				$tmp_day_type = $tmp_preset_absent_records[$i]['DayPeriod'];
				$tmp_waive = $tmp_preset_absent_records[$i]['Waive'];
				$tmp_reason = $tmp_preset_absent_records[$i]['Reason'];
				if (in_array_col($tmp_day, $total_day, 'DayNumber') === false) {
					$total_day[] = array (
						'DayNumber' => $tmp_day
					);
				}
				if (!isset ($preset_absent_data[$tmp_student_id])) {
					$preset_absent_data[$tmp_student_id] = array ();
				}
				if (!isset ($preset_absent_data[$tmp_student_id][$tmp_day])) {
					$preset_absent_data[$tmp_student_id][$tmp_day] = array ();
				}
				$preset_absent_data[$tmp_student_id][$tmp_day][0] = ($tmp_day_type == PROFILE_DAY_TYPE_AM ? CARD_STATUS_ABSENT : $preset_absent_data[$tmp_student_id][$tmp_day][0]); // AM status
				$preset_absent_data[$tmp_student_id][$tmp_day][1] = ($tmp_day_type == PROFILE_DAY_TYPE_PM ? CARD_STATUS_ABSENT : $preset_absent_data[$tmp_student_id][$tmp_day][1]); // PM status
				$preset_absent_data[$tmp_student_id][$tmp_day][2] = ''; // leave status
				$preset_absent_data[$tmp_student_id][$tmp_day][3] = ''; // AM late waive
				$preset_absent_data[$tmp_student_id][$tmp_day][4] = ''; // PM late waive
				$preset_absent_data[$tmp_student_id][$tmp_day][5] = ($tmp_day_type == PROFILE_DAY_TYPE_AM ? $tmp_waive : $preset_absent_data[$tmp_student_id][$tmp_day][5]); // AM absent waive
				$preset_absent_data[$tmp_student_id][$tmp_day][6] = ($tmp_day_type == PROFILE_DAY_TYPE_PM ? $tmp_waive : $preset_absent_data[$tmp_student_id][$tmp_day][6]); // PM absent waive
				$preset_absent_data[$tmp_student_id][$tmp_day][7] = ''; // AM early leave waive
				$preset_absent_data[$tmp_student_id][$tmp_day][8] = ''; // PM early leave waive
				$preset_absent_data[$tmp_student_id][$tmp_day][9] = ''; // AM late reason
				$preset_absent_data[$tmp_student_id][$tmp_day][10] = ''; // PM late reason
				$preset_absent_data[$tmp_student_id][$tmp_day][11] = ($tmp_day_type == PROFILE_DAY_TYPE_AM ? $tmp_reason : $preset_absent_data[$tmp_student_id][$tmp_day][11]); // AM absent reason
				$preset_absent_data[$tmp_student_id][$tmp_day][12] = ($tmp_day_type == PROFILE_DAY_TYPE_PM ? $tmp_reason : $preset_absent_data[$tmp_student_id][$tmp_day][12]); // PM absent reason
				$preset_absent_data[$tmp_student_id][$tmp_day][13] = ''; // AM early leave reason
				$preset_absent_data[$tmp_student_id][$tmp_day][14] = ''; // PM early leave reason
			}
			// ###### end of preparing preset absence records that after today ######

			// ###### Get holidays ######
			$holiday_css = 'background-color:#CCCCCC;';
			$tmp_holiday_arr = $this->Get_School_Holiday_Events($startDate, $endDate);
			$holiday_data = array ();
			for ($i = 0; $i < count($tmp_holiday_arr); $i++) {
				$tmp_day = $tmp_holiday_arr[$i]['EventDate'];
				if (!isset ($holiday_data[$tmp_day])) {
					$holiday_data[$tmp_day] = array ();
				}
				$holiday_data[$tmp_day][] = $tmp_holiday_arr[$i]['Title'];
			}
			// ###### end of getting holidays #####
			$continuous_absent_css = 'color:red;';

			$count_total_student = 0;
			/*
			#Show no records if totalday don't have value
			if (sizeof($total_day) <= 0) { # No records
				$content .= "<br />";
				$content .= "<table width=\"95%\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";

				$content .= "<tr class=\"tablebluetop\"><td class=\"tabletoplink\">" . $Lang['StudentAttendance']['ClassName'] . ": " . $Classes[$z]['ClassName'] . " (" . $startDate . " To " . $endDate . ")</td></tr>";
				$content .= "<tr class=\"tablebluerow2\"><td align=\"center\">" . $Lang['General']['NoRecordFound'] . "</td></tr>";
				$content .= "</table>";
				// 				$content .= "<br />"; 

			}
			#Show  records if totalday havs value
			else */
			{
				if ($HideNoData == 1) {
					$DayWithData = array();
					for ($i = 0; $i < sizeof($total_day); $i++) {
						$DayWithData[] = $total_day[$i]['DayNumber'];
					}
				}

				$col_span = 3;
				$toprow_colspan = 2;
				if($Columns['Remark']) $toprow_colspan += 1;
				$content .= "<table id=\"contentTable1\" width=\"95%\" border=\"1\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\" style=\"border-collapse:collapse;\">";
				$content .= "<tr class=\"tablebluetop\"><td class=\"tabletoplink\" colspan=\"" . $toprow_colspan . "\">
														" . (($IsGroup) ? $i_SmartCard_GroupName : $Lang['StudentAttendance']['ClassName']) . ": " . $Classes[$z]['ClassName'] . " (" . $startDate . " To " . $endDate . ")</td></tr>";
				$content .= "<tr>";
				$content .= "<td valign=\"top\">";

				# main content
				$content .= "<table id=\"contentTable2\" width=\"100%\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" style=\"border-collapse:collapse;\">";

				# menu header
				$non_schoolday_reason_colspan = 0;
				$non_schoolday_reason_colspan_end = 0;
				$content .= "<tr class=\"tablebluetop\" align=\"center\">";
				$content .= "<td class=\"tabletoplink\" rowspan=\"3\">" . $Lang['StudentAttendance']['ClassNumber'] . "</td>";
				$non_schoolday_reason_colspan++;
				switch ($this->ReportDisplayStudentNameFormat()) {
					case 1 :
						if ($Columns['ChineseName'] == "1") {
							$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['General']['ChineseName'] . "</td>";
							$non_schoolday_reason_colspan++;
						}
						break;
					case 2 :
						if ($Columns['EnglishName'] == "1") {
							$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['General']['EnglishName'] . "</td>";
							$non_schoolday_reason_colspan++;
						}
						break;
					default :
						if ($Columns['ChineseName'] == "1") {
							$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['General']['ChineseName'] . "</td>";
							$non_schoolday_reason_colspan++;
						}
						if ($Columns['EnglishName'] == "1") {
							$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['General']['EnglishName'] . "</td>";
							$non_schoolday_reason_colspan++;
						}
						break;
				}

				if ($Columns['Gender'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['StudentAttendance']['Gender'] . "</td>";
					$non_schoolday_reason_colspan++;
				}
#improvement for KIS showing birth and entry date
				if ($Columns['BirthDate'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['StudentRegistry']['BirthDate'] . "</td>";
					$non_schoolday_reason_colspan++;
				}
				if ($Columns['EntryDate'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"" . $Set_rowspan2 . "\">" . $Lang['StudentRegistry']['EntryDate'] . "</td>";
					$non_schoolday_reason_colspan++;
				}
				if ($Columns['Data'] == "1") {
					$content .= "<td class=\"tabletoplink\">" . $Lang['General']['Date'] . "</td>";
					$non_schoolday_reason_colspan++;
					for ($i = $iniVal; $i <= $target; $i += 86400) {
						$a = '';
						$a = date('Y-m-d', $i);

						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						$displayDate = date('d-M', $i);
						$content .= "<td class=\"tabletoplink\" colspan=\"2\">" . $displayDate . "</td>";
					}
				}

				# Number of School Days: 1Day
				if ($Columns['Schooldays'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">";
					$non_schoolday_reason_colspan_end++;
					$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
					$content .= "<tr>";
					$content .= "<td colspan=\"5\">" . $Lang['StudentAttendance']['NumberOfSchoolDays'] . ":" . count($total_day) . $Lang['StudentAttendance']['Day'] . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['Attend'] . "</td>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['Absent'] . "</td>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['Late'] . "</td>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['EarlyLeave'] . "</td>";
					$content .= "<td rowspan=\"2\" width='25%'>" . $Lang['StudentAttendance']['Present'] . "</td>";
					$content .= "</tr>";
					$content .= "</table>";

					$content .= "</td>";
				}

				if ($Columns['ReasonStat'] == "1") {
					# Reason Stat Header
					# Absent Reasons
					if (sizeof($absent_reason_symbol) > 0) {
						$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">";
						$non_schoolday_reason_colspan_end++;
						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						$absent_colspan = sizeof($absent_reason_symbol);
						$content .= "<td width=\"100%\" align=\"center\" valign=\"middle\" nowrap colspan=\"$absent_colspan\">" . $Lang['StudentAttendance']['Absent'] . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$width_percent = 100 / $absent_colspan;
						for ($n = 0; $n < $absent_colspan; $n++) {
							$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $absent_reason_symbol[$n]['Reason'] . "</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";
						$content .= "</td>";
					}

					# Late Reasons
					if (sizeof($late_reason_symbol) > 0) {
						$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">";
						$non_schoolday_reason_colspan_end++;
						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						$late_colspan = sizeof($late_reason_symbol);
						$content .= "<td width=\"100%\" align=\"center\" valign=\"middle\" nowrap colspan=\"$late_colspan\">" . $Lang['StudentAttendance']['Late'] . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$width_percent = 100 / $late_colspan;
						for ($n = 0; $n < $late_colspan; $n++) {
							$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $late_reason_symbol[$n]['Reason'] . "</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";
						$content .= "</td>";
					}

					# EarlyLeave Reasons
					if (sizeof($earlyleave_reason_symbol)) {
						$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">";
						$non_schoolday_reason_colspan_end++;
						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						$earlyleave_colspan = sizeof($earlyleave_reason_symbol);
						$content .= "<td width=\"100%\" align=\"center\" valign=\"middle\" nowrap colspan=\"$earlyleave_colspan\">" . $Lang['StudentAttendance']['EarlyLeave'] . "</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$width_percent = 100 / $earlyleave_colspan;
						for ($n = 0; $n < $earlyleave_colspan; $n++) {
							$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $earlyleave_reason_symbol[$n]['Reason'] . "</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";
						$content .= "</td>";
					}
					# End of Reason Stat Header
				}

				if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan2."\">";
					$non_schoolday_reason_colspan_end++;
					$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
					$content .= "<tr>";
					$content .= "<td width=\"100%\" align=\"center\" valign=\"middle\" nowrap colspan=\"".($sys_custom['SmartCardAttendance_StudentAbsentSession2']?5:4)."\">" . $Lang['StudentAttendance']['AbsentSessions'] . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					$width_percent = 100 / ($sys_custom['SmartCardAttendance_StudentAbsentSession2']? 5 : 4);
					$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $Lang['StudentAttendance']['Late'] . "</td>";
					$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $Lang['StudentAttendance']['RequestLeave'] . "</td>";
					$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $Lang['StudentAttendance']['PlayTruant'] . "</td>";
					$content .= "<td rowspan=\"2\" width='$width_percent%'>" . $Lang['StudentAttendance']['OfficalLeave'] . "</td>";
					if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
						$content .= "<td rowspan=\"2\" width='$width_percent%'>".$Lang['StudentAttendance']['Absent']."</td>";
					}
					$content .= "</tr>";
					$content .= "</table>";
					$content .= "</td>";
				}

				$content .= "</tr>";
				$content .= "<tr class=\"tablebluetop\">";
				if ($Columns['Data'] == "1") {
					$content .= "<td class=\"tabletoplink\" rowspan=\"".$Set_rowspan."\" style=\"text-align:center;\">" . $Lang['StudentAttendance']['Weekday'] . "</td>";
					for ($i = $iniVal; $i <= $target; $i += 86400) {
						$b = date('Y-m-d', $i);
						//$b = intval($b);
						if ($HideNoData == 1 && !in_array($b, $DayWithData)) {
							continue;
						}
						$parts = explode("_", $table[$i]);
						$year = $parts[4];
						$month = $parts[5];
						//$timestamp = mktime(0,0,0,intval($month),intval($a),$year);				
						$content .= "<td class=\"tabletoplink\" colspan=\"2\" align=\"center\" style=\"width:$colWidth\">" . $Lang['General']['DayType4'][date("w", $i)] . "</td>";
					}
				}
				$content .= "</tr>";
				$content .= "<tr class=\"tablebluetop\" align=\"center\">";
				if ($Columns['Data'] == "1") {

					for ($lii = $iniVal; $lii <= $target; $lii += 86400) {
						$a = '';
						$a = date('Y-m-d', $lii);

						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
				#AM/PM header
						if ($attendance_mode == "0" || $attendance_mode == "1" ) {
							if($sys_custom['StudentAttendance']['KIS_AttendanceUseClassGroup']) {
								$content .= "<td class=\"tabletoplink\" colspan=\"2\"><font size=\"-2\">";
								if ($attendance_mode == "0") {
									$content .= $Lang['StudentAttendance']['AM'];
								} else if($attendance_mode == "1") {
									$content .= $Lang['StudentAttendance']['PM'];
								}
								$content .= "</font></td>";
							}
							#not display AM/PM
						} else {
							$content .= "<td class=\"tabletoplink\"><font size=\"-2\">" . $Lang['StudentAttendance']['AM'] . "</font></td>";
							$content .= "<td class=\"tabletoplink\"><font size=\"-2\">" . $Lang['StudentAttendance']['PM'] . "</font></td>";
						}
					}

				}
				$content .= "</tr>";

				if ($Columns['NonSchoolDayReason'] == "1" && $Columns['Data'] == "1") {
					$content .= "<tr class=\"tablebluetop\" align=\"center\">";
					$content .= "<td class=\"tabletoplink\" colspan=\"".$non_schoolday_reason_colspan."\">".$Lang['StudentAttendance']['NonSchoolDayReason']."</td>";
					for ($liii = $iniVal; $liii <= $target; $liii += 86400) {
						$a = date('Y-m-d', $liii);
						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						$content .= "<td class=\"tabletoplink\" colspan=\"2\">";
						if (isset ($holiday_data[$a]) && count($holiday_data[$a]) > 0) {
							$content .= implode('/', $holiday_data[$a]);
						}
						$content .= "</td>";
					}
					if($non_schoolday_reason_colspan_end > 0) {
						$content .= "<td class=\"tabletoplink\" colspan=\"" . $non_schoolday_reason_colspan_end . "\"></td>";
					}
					$content .= "</tr>";
				}


				for ($liii = $iniVal; $liii <= $target; $liii += 86400) {
					$a = '';
					$a = date('Y-m-d', $liii);

					if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
						continue;
					}

					$new_monthly_report[$a]['0']['am'] = 0;
					$new_monthly_report[$a]['0']['pm'] = 0;
					$new_monthly_report[$a]['1']['am'] = 0;
					$new_monthly_report[$a]['1']['pm'] = 0;
					$new_monthly_report[$a]['2']['am'] = 0;
					$new_monthly_report[$a]['2']['pm'] = 0;
					$new_monthly_report[$a]['4']['am'] = 0;
					$new_monthly_report[$a]['4']['pm'] = 0;
					$new_monthly_report[$a]['5']['am'] = 0;
					$new_monthly_report[$a]['5']['pm'] = 0;

					### init reasons count statistic ###
					for ($k = 0; $k < sizeof($absent_reason_symbol); $k++) {
						### [day index][reason index][am], [day index][reason index][pm]
						$absent_reasons_stat[$a][$k]['am'] = 0;
						$absent_reasons_stat[$a][$k]['pm'] = 0;
					}
					for ($k = 0; $k < sizeof($late_reason_symbol); $k++) {
						### [day index][reason index][am], [day index][reason index][pm]
						$late_reasons_stat[$a][$k]['am'] = 0;
						$late_reasons_stat[$a][$k]['pm'] = 0;
					}
					for ($k = 0; $k < sizeof($earlyleave_reason_symbol); $k++) {
						### [day index][reason index][am], [day index][reason index][pm]
						$earlyleave_reasons_stat[$a][$k]['am'] = 0;
						$earlyleave_reasons_stat[$a][$k]['pm'] = 0;
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
						// session stat init 
						$absent_sessions_stat[$a]['Late']['am'] = 0;
						$absent_sessions_stat[$a]['Late']['pm'] = 0;
						$absent_sessions_stat[$a]['RequestLeave']['am'] = 0;
						$absent_sessions_stat[$a]['RequestLeave']['pm'] = 0;
						$absent_sessions_stat[$a]['OfficalLeave']['am'] = 0;
						$absent_sessions_stat[$a]['OfficalLeave']['pm'] = 0;
						$absent_sessions_stat[$a]['PlayTruant']['am'] = 0;
						$absent_sessions_stat[$a]['PlayTruant']['pm'] = 0;
						$absent_sessions_stat[$a]['Absenteeism']['am'] = 0;
						$absent_sessions_stat[$a]['Absenteeism']['pm'] = 0;
					}
				}

				#Student List 

				if ($AcademicYear == "" || ($lstudentattendance_ui->Platform == "IP" && $AcademicYear == Get_Current_Academic_Year_ID())) {
					if ($IsGroup) {
						$result = $this->retrieveGroupMonthDataByDateRange($Classes[$z]['ClassID'], $table);
					} else {
						$result = $this->retrieveClassMonthDataByDateRange($Classes[$z]['ClassName'], $table);
					}
				} else {
					$result = $this->retrieveCustomizedIndividualMonthDataByDateRange($StudentIDs, $table);
				}

				// append preset absent data to result set
				if (count($preset_absent_data) > 0) {
					foreach ($preset_absent_data as $tmp_uid => $tmp_data) {
						foreach ($tmp_data as $tmp_day => $tmp_row) {
							if (!isset ($result[$tmp_uid][$tmp_day])) {
								$result[$tmp_uid][$tmp_day] = $tmp_row;
							}
						}
					}
				}

				// ###### prepare continuous absent data ######
				$ContinuousAbsentData = array ();
				$ts_startdate = strtotime($startDate);
				$ts_enddate = strtotime($endDate);
				if (count($result) > 0 && $ContinuousAbsentDay > 0) {
					foreach ($result as $tmp_uid => $tmp_data) {
						if (!isset ($ContinuousAbsentData[$tmp_uid])) {
							$ContinuousAbsentData[$tmp_uid] = array ();
						}
						$tmp_days = array ();
						for ($i = $ts_startdate; $i <= $ts_enddate; $i += 86400) {
							$tmp_date = date("Y-m-d", $i);
							if (isset ($result[$tmp_uid][$tmp_date]) && (($attendance_mode != "1" && $result[$tmp_uid][$tmp_date][0] == CARD_STATUS_ABSENT) || ($attendance_mode == "1" && $result[$tmp_uid][$tmp_date][0] == "")) && (($attendance_mode != "0" && $result[$tmp_uid][$tmp_date][1] == CARD_STATUS_ABSENT) || ($attendance_mode == "0" && $result[$tmp_uid][$tmp_date][1] == ""))) {
								$tmp_days[$tmp_date] = 1;
							} else if (count($tmp_days) >= 1 && ((!isset($tmp_days[date("Y-m-d", $i-86400)]) && in_array(date("Y-m-d", $i-86400), $day_numbers_ary)) || in_array(date("Y-m-d",$i),$day_numbers_ary))) { // if accumulated cont absence >=1, previous day is school day but not absent, or this day is not absent, reset continuous absent array
								$tmp_days = array ();
							}
							if (count($tmp_days) >= $ContinuousAbsentDay) {
								foreach ($tmp_days as $tmp_key => $tmp_val) {
									$ContinuousAbsentData[$tmp_uid][$tmp_key] = $tmp_val;
								}
							}
						}
					}
				}
				// ###### end of preparing continuous absent data ######

				for ($i = 0; $i < sizeof($students); $i++) {

					list ($sid, $class_num, $cname, $ename, $gender, $remark,$recordstatus, $isArchieved, $classname ,$dob, $doe) = $students[$i];
					$IsLeftOrArchived = $students[$i]['RecordStatus'] == 3 || $students[$i]['IsArchivedUser'] == 1;
					$red_asteriok = $IsLeftOrArchived || $students[$i]['RecordStatus']=='0' ? '<font color="red">*</font>' : '';
					if ($IsLeftOrArchived && !$IncludeLeftArchivedStudents)
						continue;
					$user_remark = ($IsLeftOrArchived && $remark != '') ? "&nbsp;[" . intranet_htmlspecialchars($remark) . "]" : "";

					$count_total_student++;
					$s_stat[0] = 0;
					$s_stat[1] = 0;
					$s_stat[2] = 0;
					$s_stat[3] = 0;
					$s_stat[4] = 0;
					$s_stat[5] = 0;

					# Init individual student reason stat
					$s_stat_late = array ();
					$s_stat_absent = array ();
					$s_stat_earlyleave = array ();
					for ($k = 0; $k < sizeof($late_reason_symbol); $k++) {
						$s_stat_late[$k] = 0;
					}
					for ($k = 0; $k < sizeof($absent_reason_symbol); $k++) {
						$s_stat_absent[$k] = 0;
					}
					for ($k = 0; $k < sizeof($earlyleave_reason_symbol); $k++) {
						$s_stat_earlyleave[$k] = 0;
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$s_stat_absent_sessions['Late'] = 0;
						$s_stat_absent_sessions['RequestLeave'] = 0;
						$s_stat_absent_sessions['OfficalLeave'] = 0;
						$s_stat_absent_sessions['PlayTruant'] = 0;
						$s_stat_absent_sessions['Absenteeism'] = 0;
					}

					$css = ($i % 2 ? "2" : "");
					$content .= "<tr align=\"center\" class='tablebluerow$css'>";
					$content .= "<td class=\"tabletext\">" . $class_num . "</td>";
					switch ($this->ReportDisplayStudentNameFormat()) {
						case 1 :
							if ($Columns['ChineseName'] == "1")
								$content .= "<td class=\"tabletext\">" . $red_asteriok . $cname . $user_remark . "</td>";
							break;
						case 2 :
							if ($Columns['EnglishName'] == "1")
								$content .= "<td class=\"tabletext\">" . $red_asteriok . $ename . $user_remark . "</td>";
							break;
						default :
							if ($Columns['ChineseName'] == "1")
								$content .= "<td class=\"tabletext\">" . $red_asteriok . $cname . $user_remark . "</td>";
							if ($Columns['EnglishName'] == "1")
								$content .= "<td class=\"tabletext\">" . $red_asteriok . $ename . $user_remark . "</td>";
							break;
					}

#to give empty space if the date is empty					
					$dob_display = substr(trim($dob),0,10);
					if(is_date_empty($dob_display)) {
						$dob_display = "&nbsp;";
					}else{
						$dob_display = $dob_display;
					}
					$doe_display = $doe;
					if(is_date_empty($doe_display)){
						$doe_display = "&nbsp;"; 
					}else{
						$doe_display = $doe_display;
					}
										

					if ($Columns['Gender'] == "1")
						$content .= "<td class=\"tabletext\">" . $gender . "</td>";
#improvement for KIS showing birth and entry date
					if ($Columns['BirthDate'] == "1")
						$content .= "<td class=\"tabletext\">" . $dob_display . "</td>";
					if ($Columns['EntryDate'] == "1")
						$content .= "<td class=\"tabletext\">" . $doe_display . "</td>";
					if ($Columns['Data'] == "1")
						$content .= "<td class=\"tabletext\">&nbsp;</td>";

					for ($li = $iniVal; $li <= $target; $li += 86400) {
						$j = '';
						$j = date('Y-m-d', $li);

						if ($HideNoData == 1 && !in_array($j, $DayWithData)) {
							continue;
						}

						if (sizeof($result[$sid][$j]) != 0) {
							list ($am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive, $am_late_reason, $pm_late_reason, $am_absent_reason, $pm_absent_reason, $am_early_leave_reason, $pm_early_leave_reason, $am_late_session, $am_request_leave_session, $am_play_truant_session, $am_offical_leave_session, $am_absent_session, $pm_late_session, $pm_request_leave_session, $pm_play_truant_session, $pm_offical_leave_session, $pm_absent_session) = $result[$sid][$j];
							if ($am == 3) // outing = > present
								$am = '0';

							if ($pm == 3) // outing = > present
								$pm = '0';

							$c = "";
							if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
								$s_stat_absent_sessions['Late'] += ($am_late_session + $pm_late_session);
								$s_stat_absent_sessions['RequestLeave'] += ($am_request_leave_session + $pm_request_leave_session);
								$s_stat_absent_sessions['OfficalLeave'] += ($am_offical_leave_session + $pm_offical_leave_session);
								$s_stat_absent_sessions['PlayTruant'] += ($am_play_truant_session + $pm_play_truant_session);
								$s_stat_absent_sessions['Absenteeism'] += $am_absent_session + $pm_absent_session;
								$absent_sessions_stat[$j]['Late']['am'] += $am_late_session;
								$absent_sessions_stat[$j]['Late']['pm'] += $pm_late_session;
								$absent_sessions_stat[$j]['RequestLeave']['am'] += $am_request_leave_session;
								$absent_sessions_stat[$j]['RequestLeave']['pm'] += $pm_request_leave_session;
								$absent_sessions_stat[$j]['OfficalLeave']['am'] += $am_offical_leave_session;
								$absent_sessions_stat[$j]['OfficalLeave']['pm'] += $pm_offical_leave_session;
								$absent_sessions_stat[$j]['PlayTruant']['am'] += $am_play_truant_session;
								$absent_sessions_stat[$j]['PlayTruant']['pm'] += $pm_play_truant_session;
								$absent_sessions_stat[$j]['Absenteeism']['am'] += $am_absent_session;
								$absent_sessions_stat[$j]['Absenteeism']['pm'] += $pm_absent_session;
							}

							if ($am != "" && $attendance_mode != "1") {
								$c .= $notation_symbol[$am];

								if ($am == '0') {

									$s_stat[$am] += $present_count_value;
									$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + 1;
									$s_stat[5] += $present_count_value; // on time stat
									$new_monthly_report[$j]['5']['am'] += 1;
								} else
									if ($am == '1') {
										$s_stat[$am] += $absent_count_value;
										$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + 1;
										# Find reason symbol
										$symbol_index = -1;
										for ($p = 0; $p < sizeof($absent_reason_symbol); $p++) {
											if ($am_absent_reason == $absent_reason_symbol[$p]['Reason']) {
												$symbol_index = $p;
												break;
											}
										}
										if ($symbol_index != -1) {
											$c = $absent_reason_symbol[$symbol_index]['StatusSymbol'];
											$temp_count_value = 1; // count number of times
											if($reason_count_with_absent_session){ // count number of sessions
												$temp_count_value = max($am_request_leave_session,$am_offical_leave_session,$am_absent_session);
											}
											$absent_reasons_stat[$j][$symbol_index]['am'] += $temp_count_value;
											$s_stat_absent[$symbol_index] += $reason_count_with_absent_session? $temp_count_value : $absent_count_value;
										}
										# End of find reason symbol
										if ($am_absent_waive == 1) {
											if ($c != "")
												$c .= "<br>";
											$c .= $notation_symbol[9];
											$s_stat[$am] -= $absent_count_value;
											$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - 1;
											$s_stat['0'] += $present_count_value;
											$new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + 1;
										}
									} else
										if ($am == '2') {
											$s_stat['0'] += $present_count_value;
											$s_stat[$am] += $late_count_value;
											$new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + 1;
											$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + 1;
											$symbol_index = -1;

											for ($p = 0; $p < sizeof($late_reason_symbol); $p++) {
												if ($am_late_reason == $late_reason_symbol[$p]['Reason']) {
													$symbol_index = $p;
													break;
												}
											}
											if ($symbol_index != -1) {
												$c = $late_reason_symbol[$symbol_index]['StatusSymbol'];
												$late_reasons_stat[$j][$symbol_index]['am'] += 1;
												$s_stat_late[$symbol_index] += $late_count_value;
											}
											# End of find reason symbol
											if ($am_late_waive == 1) {
												if ($c != "")
													$c .= "<br>";
												$c .= $notation_symbol[9];
												$s_stat[$am] -= $late_count_value;
												$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - 1;
											}
										}
							}

							// eary leave info, added on 2008-03-13
							if ($leave == 1 && $attendance_mode != "1") {
								if ($c != "")
									$c .= "<br>";
								# Find reason symbol
								$symbol_index = -1;
								for ($p = 0; $p < sizeof($earlyleave_reason_symbol); $p++) {
									if ($am_early_leave_reason == $earlyleave_reason_symbol[$p]['Reason']) {
										$symbol_index = $p;
										break;
									}
								}
								if ($symbol_index != -1) {
									$c .= $earlyleave_reason_symbol[$symbol_index]['StatusSymbol'];
									$earlyleave_reasons_stat[$j][$symbol_index]['am'] += 1;
									$s_stat_earlyleave[$symbol_index] += $early_leave_count_value;
								} else {
									$c .= $notation_symbol[4];
								}

								$s_stat[4] += $early_leave_count_value;

								$new_monthly_report[$j][4]['am'] = $new_monthly_report[$j][4]['am'] + 1;

								if ($am_early_waive == 1) {
									$c .= $notation_symbol[9];
									$s_stat[4] -= $early_leave_count_value;
									$new_monthly_report[$j][4]['am'] = $new_monthly_report[$j][4]['am'] - 1;
								}
							}
							if ($Columns['Data'] == "1") {
								$td_css = "";
								$td_css_arr = array ();
								if (isset ($holiday_data[$j]) && count($holiday_data[$j]) > 0) {
									$td_css_arr[] = $holiday_css;
								}
								if (isset ($ContinuousAbsentData[$sid][$j])) {
									$td_css_arr[] = $continuous_absent_css;
								}
								if (count($td_css_arr) > 0) {
									$td_css = 'style="' . implode("", $td_css_arr) . '"';
								}
								if ($attendance_mode == "0") {
									if ($c != "")
										$content .= "<td class=\"tabletext\" colspan=\"2\" " .
										$td_css . ">$c</td>";
									else
										$content .= "<td class=\"tabletext\" colspan=\"2\" " . $td_css . ">&nbsp;</td>";
								} else
									if ($attendance_mode != "1") {
										if ($c != "")
											$content .= "<td class=\"tabletext\" " .
											$td_css . ">$c</td>";
										else
											$content .= "<td class=\"tabletext\" " . $td_css . ">&nbsp;</td>";
									}
							}

							$d = "";
							if ($pm != "" && $attendance_mode != "0") {
								//$content .= "<td>".$notation_symbol[$pm]."</td>";
								$d .= $notation_symbol[$pm];

								if ($pm == '0') {
									$s_stat[$pm] += $present_count_value;
									$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + 1;
									$s_stat[5] += $present_count_value; // on time stat
									$new_monthly_report[$j]['5']['pm'] += 1;
								} else
									if ($pm == '1') {
										$s_stat[$pm] += $absent_count_value;
										$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + 1;
										# Find reason symbol
										$symbol_index = -1;
										for ($p = 0; $p < sizeof($absent_reason_symbol); $p++) {
											if ($pm_absent_reason == $absent_reason_symbol[$p]['Reason']) {
												$symbol_index = $p;
												break;
											}
										}
										if ($symbol_index != -1) {
											$d = $absent_reason_symbol[$symbol_index]['StatusSymbol'];
											$temp_count_value = 1; // count number of times
											if($reason_count_with_absent_session){ // count sessions
												$temp_count_value = max($pm_request_leave_session,$pm_offical_leave_session,$pm_absent_session);
											}
											$absent_reasons_stat[$j][$symbol_index]['pm'] += $temp_count_value;
											$s_stat_absent[$symbol_index] += $reason_count_with_absent_session? $temp_count_value : $absent_count_value;
										}
										# End of find reason symbol
										if ($pm_absent_waive == 1) {
											if ($d != "")
												$d .= "<br>";
											$d .= $notation_symbol[9];
											$s_stat[$pm] -= $absent_count_value;

											$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - 1;
											$s_stat['0'] += $present_count_value;

											$new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + 1;
										}
									} else
										if ($pm == '2') {
											$s_stat['0'] += $present_count_value;
											$s_stat[$pm] += $late_count_value;

											$new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + 1;
											$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + 1;
											# Find reason symbol
											$symbol_index = -1;
											for ($p = 0; $p < sizeof($late_reason_symbol); $p++) {
												if ($pm_late_reason == $late_reason_symbol[$p]['Reason']) {
													$symbol_index = $p;
													break;
												}
											}

											if ($symbol_index != -1) {
												$d = $late_reason_symbol[$symbol_index]['StatusSymbol'];
												$late_reasons_stat[$j][$symbol_index]['pm'] += 1;
												$s_stat_late[$symbol_index] += $late_count_value;
											}
											# End of find reason symbol
											if ($pm_late_waive == 1) {
												if ($d != "")
													$d .= "<br>";
												$d .= $notation_symbol[9];
												$s_stat[$pm] -= $late_count_value;

												$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - 1;
											}

										}
							}

							// eary leave info, added on 2008-03-13
							if ($leave == 2 && $attendance_mode != "0") {
								if ($d != "")
									$d .= "<Br>";
								# Find reason symbol
								$symbol_index = -1;
								for ($p = 0; $p < sizeof($earlyleave_reason_symbol); $p++) {
									if ($pm_early_leave_reason == $earlyleave_reason_symbol[$p]['Reason']) {
										$symbol_index = $p;
										break;
									}
								}
								if ($symbol_index != -1) {
									$d .= $earlyleave_reason_symbol[$symbol_index]['StatusSymbol'];
									$earlyleave_reasons_stat[$j][$symbol_index]['pm'] += 1;
									$s_stat_earlyleave[$symbol_index] += $early_leave_count_value;
								} else {
									$d .= $notation_symbol[4];
								}
								# End of find reason symbol
								# $d .= $notation_symbol[4];
								$s_stat[4] += $early_leave_count_value;

								$new_monthly_report[$j][4]['pm'] = $new_monthly_report[$j][4]['pm'] + 1;

								if ($pm_early_waive == 1) {
									$d .= $notation_symbol[9];
									$s_stat[4] -= $early_leave_count_value;
									$new_monthly_report[$j][4]['pm'] = $new_monthly_report[$j][4]['pm'] - 1;
								}
							}
							if ($Columns['Data'] == "1") {
								$td_css = "";
								$td_css_arr = array ();
								if (isset ($holiday_data[$j]) && count($holiday_data[$j]) > 0) {
									$td_css_arr[] = $holiday_css;
								}
								if (isset ($ContinuousAbsentData[$sid][$j])) {
									$td_css_arr[] = $continuous_absent_css;
								}
								if (count($td_css_arr) > 0) {
									$td_css = 'style="' . implode("", $td_css_arr) . '"';
								}
								if ($attendance_mode == "1") {
									if ($d != "")
										$content .= "<td class=\"tabletext\" colspan=\"2\" " . $td_css . ">$d</td>";
									else
										$content .= "<td class=\"tabletext\" colspan=\"2\" " . $td_css . ">&nbsp;</td>";
								} else
									if ($attendance_mode != "0") {
										if ($d != "")
											$content .= "<td class=\"tabletext\" " . $td_css . ">$d</td>";
										else
											$content .= "<td class=\"tabletext\" " . $td_css . ">&nbsp;</td>";
									}
							}
						} else {
							$td_css = "";
							$td_css_arr = array ();
							if (isset ($holiday_data[$j]) && count($holiday_data[$j]) > 0) {
								$td_css_arr[] = $holiday_css;
							}
							if (isset ($ContinuousAbsentData[$sid][$j])) {
								$td_css_arr[] = $continuous_absent_css;
							}
							if (count($td_css_arr) > 0) {
								$td_css = 'style="' . implode("", $td_css_arr) . '"';
							}
							if ($Columns['Data'] == "1") {
								if ($attendance_mode == "0" || $attendance_mode == "1")
									$content .= "<td class=\"tabletext\" colspan=\"2\" " . $td_css . ">&nbsp;</td>";
								else
									$content .= "<td class=\"tabletext\" " . $td_css . ">&nbsp;</td><td class=\"tabletext\" " . $td_css . ">&nbsp;</td>";
							}
						}
					}

					if ($Columns['Schooldays'] == "1") {
						$content .= "<td>";

						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						for ($k = 0; $k < sizeof($s_stat); $k++) {
							if ($k == 3)
								continue;
							$percent_width = sizeof($s_stat) - 1 > 0 ? (100 / sizeof($s_stat) - 1) . "%" : '100%';
							$content .= "<td class=\"tabletext\" width='$percent_width'>" . $s_stat[$k] . "</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";

						$content .= "</td>";
					}

					if ($Columns['ReasonStat'] == "1") {
						# Reason Stat
						# Absent Reasons
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td>";
							$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
							$content .= "<tr>";
							$width_percent = 100 / sizeof($s_stat_absent);
							for ($k = 0; $k < sizeof($s_stat_absent); $k++) {
								$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent[$k] . "</td>";
							}
							$content .= "</tr>";
							$content .= "</table>";
							$content .= "</td>";
						}
						# Late Reasons
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td>";
							$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
							$content .= "<tr>";
							$width_percent = 100 / sizeof($s_stat_late);
							for ($k = 0; $k < sizeof($s_stat_late); $k++) {
								$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_late[$k] . "</td>";
							}
							$content .= "</tr>";
							$content .= "</table>";
							$content .= "</td>";
						}
						# EarlyLeave Reasons
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td>";
							$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
							$content .= "<tr>";
							$width_percent = 100 / sizeof($s_stat_earlyleave);
							for ($k = 0; $k < sizeof($s_stat_earlyleave); $k++) {
								$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_earlyleave[$k] . "</td>";
							}
							$content .= "</tr>";
							$content .= "</table>";
							$content .= "</td>";
						}
						# End of Reason Stat
					}

					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
						# Show Absent Sessions Stat for each student
						$content .= "<td>";
						$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
						$content .= "<tr>";
						$width_percent = 100 / ($sys_custom['SmartCardAttendance_StudentAbsentSession2']?5:4);
						$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent_sessions['Late'] . "</td>";
						$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent_sessions['RequestLeave'] . "</td>";
						$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent_sessions['PlayTruant'] . "</td>";
						$content .= "<td class=\"tabletext\" width='$width_percent%'>" . $s_stat_absent_sessions['OfficalLeave'] . "</td>";
						if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
							$content .= "<td class=\"tabletext\" width='$width_percent%'>".$s_stat_absent_sessions['Absenteeism']."</td>";
						}
						$content .= "</tr>";
						$content .= "</table>";
						$content .= "</td>";

					}

					$content .= "</tr>";
				}

				### Daily Statistic ###
				if ($Columns['DailyStat'] == "1" && $Columns['Data'] == "1") {
					$col_span = 6;
					if ($Columns['ChineseName'] != "1")
						$col_span -= 1;
					if ($Columns['EnglishName'] != "1")
						$col_span -= 1;
					if ($Columns['Gender'] != "1")
						$col_span -= 1;
					if ($Columns['BirthDate'] != "1")
						$col_span -= 1;
					if ($Columns['EntryDate'] != "1")
						$col_span -= 1;

					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"5\" colspan=\"$col_span\" align=\"right\" valign=\"top\">" . $Lang['StudentAttendance']['DailyStatistic'] . "</td>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfPresent'] . "</td>";
					$totalPresent = 0;

					for ($il = $iniVal; $il <= $target; $il += 86400) {
						$a = date('Y-m-d', $il);
						//$a = $b;

						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['0']."</td>";
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['0']['am'] . "</td>";
							$totalPresent = $totalPresent + $new_monthly_report[$a]['0']['am'];
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['0']['pm'] . "</td>";
								$totalPresent = $totalPresent + $new_monthly_report[$a]['0']['pm'];
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['0']['am'] == '' ? 0 : $new_monthly_report[$a]['0']['am']) . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['0']['pm'] == '' ? 0 : $new_monthly_report[$a]['0']['pm']) . "</td>";
								$totalPresent = $totalPresent + $new_monthly_report[$a]['0']['am'] + $new_monthly_report[$a]['0']['pm'];
							}

					}

					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";

					// Row of On Time Daily Stat
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfOnTime'] . "</td>";
					$totalOnTime = 0;

					for ($lm = $iniVal; $lm <= $target; $lm += 86400) {
						$a = date('Y-m-d', $lm);

						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['1']."</td>";
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['5']['am'] . "</td>";
							$totalOnTime = $totalOnTime + $new_monthly_report[$a]['5']['am'];
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['5']['pm'] . "</td>";
								$totalOnTime = $totalOnTime + $new_monthly_report[$a]['5']['pm'];
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['5']['am'] == '' ? 0 : $new_monthly_report[$a]['5']['am']) . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['5']['pm'] == '' ? 0 : $new_monthly_report[$a]['5']['pm']) . "</td>";
								$totalOnTime = $totalOnTime + $new_monthly_report[$a]['5']['am'] + $new_monthly_report[$a]['5']['pm'];
							}
					}

					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					// End row of On Time Daily Stat

					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfAbsent'] . "</td>";
					$totalAbsent = 0;

					for ($ln = $iniVal; $ln <= $target; $ln += 86400) {
						$a = date('Y-m-d', $ln);

						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['1']."</td>";
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['1']['am'] . "</td>";
							$totalAbsent = $totalAbsent + $new_monthly_report[$a]['1']['am'];
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['1']['pm'] . "</td>";
								$totalAbsent = $totalAbsent + $new_monthly_report[$a]['1']['pm'];
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['1']['am'] == '' ? 0 : $new_monthly_report[$a]['1']['am']) . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['1']['pm'] == '' ? 0 : $new_monthly_report[$a]['1']['pm']) . "</td>";
								$totalAbsent = $totalAbsent + $new_monthly_report[$a]['1']['am'] + $new_monthly_report[$a]['1']['pm'];
							}

					}

					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";

					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfLate'] . "</td>";

					for ($lo = $iniVal; $lo <= $target; $lo += 86400) {
						$a = date('Y-m-d', $lo);
						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['2']."</td>";
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['2']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['2']['pm'] . "</td>";
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['2']['am'] == '' ? 0 : $new_monthly_report[$a]['2']['am']) . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['2']['pm'] == '' ? 0 : $new_monthly_report[$a]['2']['pm']) . "</td>";
							}
					}

					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";

					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['NumberOfEarlyLeave'] . "</td>";

					for ($lp = $iniVal; $lp <= $target; $lp += 86400) {
						$a = date('Y-m-d', $lp);
						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['4']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\" colspan=\"2\">" . $new_monthly_report[$a]['4']['pm'] . "</td>";
							} else {
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['4']['am'] == '' ? 0 : $new_monthly_report[$a]['4']['am']) . "</td>";
								$content .= "<td class=\"tabletext\" align=\"center\">" . ($new_monthly_report[$a]['4']['pm'] == '' ? 0 : $new_monthly_report[$a]['4']['pm']) . "</td>";
							}

					}

					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == '1') {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
				}

				### End of Daily Statistic ###
				### Reasons Daily Statistic ###
				if ($Columns['ReasonStat'] == "1" && $Columns['Data'] == "1") {
					$col_span = 6;
					if ($Columns['ChineseName'] != "1")
						$col_span -= 1;
					if ($Columns['EnglishName'] != "1")
						$col_span -= 1;
					if ($Columns['Gender'] != "1")
						$col_span -= 1;
					if ($Columns['BirthDate'] != "1")
						$col_span -= 1;
					if ($Columns['EntryDate'] != "1")
						$col_span -= 1;
					$row_span = sizeof($absent_reason_symbol);
					if ($row_span > 0) {
						$temp = $row_span +1;
						$content .= "<tr>";
						$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"$temp\" colspan=\"$col_span\" align=\"right\" valign=\"top\">" . $Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat'] . "(" . $Lang['StudentAttendance']['Absent'] . ")</td>";
						for ($k = 0; $k < $row_span; $k++) {
							$content .= "<tr>";
							$content .= "<td class=\"tablebluetop tabletoplink\">" . $absent_reason_symbol[$k]['Reason'] . "</td>";

							for ($lii = $iniVal; $lii <= $target; $lii += 86400) {
								$a = date('Y-m-d', $lii);
								if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
									continue;
								}
								if ($attendance_mode == "0") {
									$content .= "<td align=\"center\" colspan=\"2\">" . $absent_reasons_stat[$a][$k]['am'] . "</td>";
								} else
									if ($attendance_mode == "1") {
										$content .= "<td align=\"center\" colspan=\"2\">" . $absent_reasons_stat[$a][$k]['pm'] . "</td>";
									} else {
										$content .= "<td align=\"center\">" . ($absent_reasons_stat[$a][$k]['am'] == '' ? 0 : $absent_reasons_stat[$a][$k]['am']) . "</td>";
										$content .= "<td align=\"center\">" . ($absent_reasons_stat[$a][$k]['pm'] == '' ? 0 : $absent_reasons_stat[$a][$k]['pm']) . "</td>";
									}

							}

							// dummy column insert
							if ($Columns['Schooldays'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if ($Columns['ReasonStat'] == "1") {
								if (sizeof($absent_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($late_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($earlyleave_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
							}
							if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							$content .= "</tr>";
						}
						$content .= "</tr>";
					}

					$row_span = sizeof($late_reason_symbol);
					if ($row_span > 0) {
						$temp = $row_span +1;
						$content .= "<tr>";
						$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"$temp\" colspan=\"$col_span\" align=\"right\" valign=\"top\">" . $Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat'] . "(" . $Lang['StudentAttendance']['Late'] . ")</td>";
						for ($k = 0; $k < $row_span; $k++) {
							$content .= "<tr>";
							$content .= "<td class=\"tablebluetop tabletoplink\">" . $late_reason_symbol[$k]['Reason'] . "</td>";

							for ($ii = $iniVal; $ii <= $target; $ii += 86400) {
								$a = date('Y-m-d', $ii);

								if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
									continue;
								}
								if ($attendance_mode == "0") {
									$content .= "<td align=\"center\" colspan=\"2\">" . $late_reasons_stat[$a][$k]['am'] . "</td>";
								} else
									if ($attendance_mode == "1") {
										$content .= "<td align=\"center\" colspan=\"2\">" . $late_reasons_stat[$a][$k]['pm'] . "</td>";
									} else {
										$content .= "<td align=\"center\">" . ($late_reasons_stat[$a][$k]['am'] == '' ? 0 : $late_reasons_stat[$a][$k]['am']) . "</td>";
										$content .= "<td align=\"center\">" . ($late_reasons_stat[$a][$k]['pm'] == '' ? 0 : $late_reasons_stat[$a][$k]['pm']) . "</td>";
									}

							}

							// dummy column insert
							if ($Columns['Schooldays'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if ($Columns['ReasonStat'] == "1") {
								if (sizeof($absent_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($late_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($earlyleave_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
							}
							if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							$content .= "</tr>";
						}

						$content .= "</tr>";
					}

					$row_span = sizeof($earlyleave_reason_symbol);
					if ($row_span > 0) {
						$temp = $row_span +1;
						$content .= "<tr>";
						$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"$temp\" colspan=\"$col_span\" align=\"right\" valign=\"top\">" . $Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat'] . "(" . $Lang['StudentAttendance']['EarlyLeave'] . ")</td>";
						for ($k = 0; $k < $row_span; $k++) {
							$content .= "<tr>";
							$content .= "<td class=\"tablebluetop tabletoplink\">" . $earlyleave_reason_symbol[$k]['Reason'] . "</td>";

							for ($ii = $iniVal; $ii <= $target; $ii += 86400) {
								$a = date('Y-m-d', $ii);
								if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
									continue;
								}
								if ($attendance_mode == "0") {
									$content .= "<td align=\"center\" colspan=\"2\">" . $earlyleave_reasons_stat[$a][$k]['am'] . "</td>";
								} else
									if ($attendance_mode == "1") {
										$content .= "<td align=\"center\" colspan=\"2\">" . $earlyleave_reasons_stat[$a][$k]['pm'] . "</td>";
									} else {
										$content .= "<td align=\"center\">" . ($earlyleave_reasons_stat[$a][$k]['am'] == '' ? 0 : $earlyleave_reasons_stat[$a][$k]['am']) . "</td>";
										$content .= "<td align=\"center\">" . ($earlyleave_reasons_stat[$a][$k]['pm'] == '' ? 0 : $earlyleave_reasons_stat[$a][$k]['pm']) . "</td>";
									}

							}

							// dummy column insert
							if ($Columns['Schooldays'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if ($Columns['ReasonStat'] == "1") {
								if (sizeof($absent_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($late_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
								if (sizeof($earlyleave_reason_symbol) > 0) {
									$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
								}
							}
							if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1") {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							$content .= "</tr>";
						}

						$content .= "</tr>";
					}
				}
				### End of Reasons Daily Statistic ###

				if ($sys_custom['SmartCardAttendance_StudentAbsentSession'] && $Columns['Session'] == "1" && $Columns['Data'] == "1") {
					### Absent sessions Daily Statistic ###
					$col_span = 6;
					if ($Columns['ChineseName'] != "1")
						$col_span -= 1;
					if ($Columns['EnglishName'] != "1")
						$col_span -= 1;
					if ($Columns['Gender'] != "1")
						$col_span -= 1;
					if ($Columns['BirthDate'] != "1")
						$col_span -= 1;
					if ($Columns['EntryDate'] != "1")
						$col_span -= 1;

					$row_span = 4;
					if($sys_custom['SmartCardAttendance_StudentAbsentSession2']){
						$row_span += 1;
					}
					$temp = $row_span +1;
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\" rowspan=\"$temp\" colspan=\"$col_span\" align=\"right\">" . $Lang['StudentAttendance']['AbsentSessions'] . "</td>";
					// late session
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['Late'] . "</td>";

					for ($ii = $iniVal; $ii <= $target; $ii += 86400) {
						$a = date('Y-m-d', $ii);

						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['Late']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['Late']['pm'] . "</td>";
							} else {
								$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['Late']['am'] == '' ? 0 : $absent_sessions_stat[$a]['Late']['am']) . "</td>";
								$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['Late']['pm'] == '' ? 0 : $absent_sessions_stat[$a]['Late']['pm']) . "</td>";
							}

					}

					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					// RequestLeave
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['RequestLeave'] . "</td>";

					for ($ii = $iniVal; $ii <= $target; $ii += 86400) {
						$a = date('Y-m-d', $ii);

						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['RequestLeave']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['RequestLeave']['pm'] . "</td>";
							} else {
								$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['RequestLeave']['am'] == '' ? 0 : $absent_sessions_stat[$a]['RequestLeave']['am']) . "</td>";
								$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['RequestLeave']['pm'] == '' ? 0 : $absent_sessions_stat[$a]['RequestLeave']['pm']) . "</td>";
							}

					}

					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					// PlayTruant
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['PlayTruant'] . "</td>";

					for ($ii = $iniVal; $ii <= $target; $ii += 86400) {
						$a = date('Y-m-d', $ii);
						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['PlayTruant']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['PlayTruant']['pm'] . "</td>";
							} else {
								$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['PlayTruant']['am'] == '' ? 0 : $absent_sessions_stat[$a]['PlayTruant']['am']) . "</td>";
								$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['PlayTruant']['pm'] == '' ? 0 : $absent_sessions_stat[$a]['PlayTruant']['pm']) . "</td>";
							}

					}
					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					// OfficalLeave
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['OfficalLeave'] . "</td>";

					for ($ii = $iniVal; $ii <= $target; $ii += 86400) {
						$a = date('Y-m-d', $ii);
						if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
							continue;
						}
						if ($attendance_mode == "0") {
							$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['OfficalLeave']['am'] . "</td>";
						} else
							if ($attendance_mode == "1") {
								$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['OfficalLeave']['pm'] . "</td>";
							} else {
								$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['OfficalLeave']['am'] == '' ? 0 : $absent_sessions_stat[$a]['OfficalLeave']['am']) . "</td>";
								$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['OfficalLeave']['pm'] == '' ? 0 : $absent_sessions_stat[$a]['OfficalLeave']['pm']) . "</td>";
							}

					}

					// dummy column insert
					if ($Columns['Schooldays'] == "1") {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					if ($Columns['ReasonStat'] == "1") {
						if (sizeof($absent_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($late_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if (sizeof($earlyleave_reason_symbol) > 0) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
					}
					if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
						$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
					}
					$content .= "</tr>";
					
					if($sys_custom['SmartCardAttendance_StudentAbsentSession2'])
					{
						// Absent 
						$content .= "<tr>";
						$content .= "<td class=\"tablebluetop tabletoplink\">" . $Lang['StudentAttendance']['Absent'] . "</td>";
	
						for ($ii = $iniVal; $ii <= $target; $ii += 86400) {
							$a = date('Y-m-d', $ii);
							if ($HideNoData == 1 && !in_array($a, $DayWithData)) {
								continue;
							}
							if ($attendance_mode == "0") {
								$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['Absenteeism']['am'] . "</td>";
							} else
								if ($attendance_mode == "1") {
									$content .= "<td align=\"center\" colspan=\"2\">" . $absent_sessions_stat[$a]['Absenteeism']['pm'] . "</td>";
								} else {
									$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['Absenteeism']['am'] == '' ? 0 : $absent_sessions_stat[$a]['Absenteeism']['am']) . "</td>";
									$content .= "<td align=\"center\">" . ($absent_sessions_stat[$a]['Absenteeism']['pm'] == '' ? 0 : $absent_sessions_stat[$a]['Absenteeism']['pm']) . "</td>";
								}
	
						}
	
						// dummy column insert
						if ($Columns['Schooldays'] == "1") {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						if ($Columns['ReasonStat'] == "1") {
							if (sizeof($absent_reason_symbol) > 0) {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if (sizeof($late_reason_symbol) > 0) {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
							if (sizeof($earlyleave_reason_symbol) > 0) {
								$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
							}
						}
						if ($sys_custom['SmartCardAttendance_StudentAbsentSession']) {
							$content .= "<td class=\"tabletext\" align=\"center\">&nbsp;</td>";
						}
						$content .= "</tr>";
					}
				}
				$content .= "</tr>";
				### End of Absent sessions Daily Statistic ###

				$content .= "</table>";

				$content .= "</td>";
				if ($Columns['Remark'] == "1") {
					$content .= "<td valign=\"top\">";

					$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\" height=\"75\" align=\"center\">";
					//$content .= "<td rowspan=\"3\" algin=\"center\">";
					$content .= $Lang['General']['Remark'];
					$content .= "</td>";
					$content .= "</tr>";
					$content .= "<tr></tr>";
					$content .= "<tr></tr>";
					$content .= "</table>";

					$content .= "</td>";
				}
				$content .= "<td valign=\"top\">";
				if ($Columns['MonthlyStat'] == "1") {
					$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=0 cellspacing=0>";
					$content .= "<tr>";
					$content .= "<td class=\"tablebluetop tabletoplink\" height=\"75\" align=\"center\">" . $Lang['StudentAttendance']['MonthlyStatistic'] . "</td>";
					$content .= "</tr>";
					$content .= "</table>
													<table id=\"contentTable3\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
					$content .= "<tr>";
					$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['NumberOfAttendants'] . ": " . $count_total_student . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['AveragePresentNumber'] . ": " . round(count($total_day)==0? '0' : ($totalPresent / count($total_day)), 2) . "</td>";
					$content .= "</tr>";
					$content .= "<tr>";
					$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['AverageAbsentNumber'] . ": " . round(count($total_day)==0? '0' : ($totalAbsent / count($total_day)), 2) . "</td>";
					$content .= "</tr>";
					$content .= "</table>";
				}
				$content .= "<br>";

				$content .= "<table id=\"contentTable4\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['NotationSymbols'] . ": </td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[0] . " - " . $Lang['StudentAttendance']['Present'] . "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[1] . " - " . $Lang['StudentAttendance']['Absent'] . "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[2] . " - " . $Lang['StudentAttendance']['Late'] . "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[4] . " - " . $Lang['StudentAttendance']['EarlyLeave'] . "</td>";
				$content .= "</tr>";
				$content .= "<tr>";
				$content .= "<td class=\"tabletext\">" . $notation_symbol[9] . " - " . $Lang['StudentAttendance']['Waived'] . "</td>";
				$content .= "</tr>";
				$content .= "</table>";
				### Customize Reason Symbols ###
				if (sizeof($absent_reason_symbol) > 0 || sizeof($late_reason_symbol) > 0 || sizeof($earlyleave_reason_symbol) > 0) {
					$content .= "<br>";
					$content .= "<table id=\"contentTable4\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
					$content .= "<tr>";
					$content .= "<td nowrap class=\"tabletext\">" . $Lang['SmartCard']['StudentAttendance']['CustomizeReasonSymbols'] . ": </td>";
					$content .= "</tr>";
					if (sizeof($absent_reason_symbol) > 0) {
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">&nbsp;</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['Absent'] . ": </td>";
						$content .= "</tr>";
						for ($k = 0; $k < sizeof($absent_reason_symbol); $k++) {
							$content .= "<tr>";
							$content .= "<td nowrap class=\"tabletext\">" . $absent_reason_symbol[$k]['StatusSymbol'] . " - " . $absent_reason_symbol[$k]['Reason'] . "</td>";
							$content .= "</tr>";
						}
					}
					if (sizeof($late_reason_symbol) > 0) {
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">&nbsp;</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['Late'] . ": </td>";
						$content .= "</tr>";
						for ($k = 0; $k < sizeof($late_reason_symbol); $k++) {
							$content .= "<tr>";
							$content .= "<td nowrap class=\"tabletext\">" . $late_reason_symbol[$k]['StatusSymbol'] . " - " . $late_reason_symbol[$k]['Reason'] . "</td>";
							$content .= "</tr>";
						}
					}
					if (sizeof($earlyleave_reason_symbol) > 0) {
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">&nbsp;</td>";
						$content .= "</tr>";
						$content .= "<tr>";
						$content .= "<td class=\"tabletext\">" . $Lang['StudentAttendance']['EarlyLeave'] . ": </td>";
						$content .= "</tr>";
						for ($k = 0; $k < sizeof($earlyleave_reason_symbol); $k++) {
							$content .= "<tr>";
							$content .= "<td nowrap class=\"tabletext\">" . $earlyleave_reason_symbol[$k]['StatusSymbol'] . " - " . $earlyleave_reason_symbol[$k]['Reason'] . "</td>";
							$content .= "</tr>";
						}
					}
					$content .= "</table>";
				}
				### End of Customize Reason Symbols ###

				$content .= "<br />";
				$content .= "<table id=\"contentTable4\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
				$content .= "<tr>";
				$content .= "<td><span style=\"border:1px solid black;background-color:#CCCCCC;width:16px;height:16px;display:block;float:left;\"></span>:<br style=\"clear:both;\"/>" . $Lang['EventType']['PublicHoliday'] . " / " . $Lang['EventType']['SchoolHoliday'] . "</td>";
				$content .= "</tr>";
				$content .= "</table>";

				$content .= "</td>";
				$content .= "</tr>";
				$content .= "</table>";
				// 				$content .= "<br/>";
			}

			if ($PrintVersion) {
				$breakStyle = $z < sizeof($Classes) - 1 ? "style='page-break-after:always'" : "";
			}

			$content .= "<div " . $breakStyle . ">&nbsp;</div>";
		}
		return $content;
	}

	function Get_Class_Monthly_Attendance_Report_By_Date_Range_Get_Month($startstring, $endstring)
	//function get_months($startstring, $endstring)
	{
		$time1 = strtotime($startstring);
		$time2 = strtotime($endstring);
		/*
		$my1     = date('mY', $time1); 
		$my2    = date('mY', $time2);
		$year1 = date('Y', $time1);
		$year2 = date('Y', $time2);
		$years = range($year1, $year2);	 
		foreach($years as $year)
		{
			$months[$year] = array();		
			while($time1 < $time2)
			{
				if(date('Y',$time1) == $year)
				{
					$months[$year][] = date('m', $time1);
					$time1 = strtotime(date('Y-m-d', $time1).' +1 month');
				}
				else
				{
				break;
				}
			}		
		continue;
		}
		*/
		$months = array ();
		while ($time1 <= $time2) {
			$y = date("Y", $time1);
			$m = date("m", $time1);
			//$d = date("d",$time1);
			if (!isset ($months[$y])) {
				$months[$y] = array ();
			}
			$months[$y][] = $m;
			$time1 = mktime(0, 0, 0, intval($m) + 1, 1, intval($y));
		}

		return $months;
	}

	function Get_Lesson_Non_Confirmed_Report_Form() {
		global $linterface, $Lang, $image_path, $LAYOUT_SKIN;

		$FromDate = date('Y-m-d');
		$ToDate = date('Y-m-d');

		$x = $this->Include_JS_CSS();

		$x .= '<form name="form1" method="post" action="">
							<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
								<tr>
									<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
										' . $Lang['LessonAttendance']['FromDate'] . '
									</td>
									<td class="tabletext" width="70%">
										' . $linterface->GET_DATE_PICKER("FromDate", $FromDate) . '
										<span style="color:red;display:none" id="FromDateWarningLayer"></span>
									</td>
								</tr>
								<tr>
									<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
										' . $Lang['LessonAttendance']['ToDate'] . '
									</td>
									<td class="tabletext" width="70%">
										' . $linterface->GET_DATE_PICKER("ToDate", $ToDate) . '
										<span style="color:red;display:none;" id="ToDateWarningLayer"></span>
									</td>
								</tr>
							</table>
							<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
								<tr>
							    	<td class="dotline"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1" /></td>
							    </tr>
							    <tr>
								    <td align="center">
										' . $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "Get_Lesson_Non_Confirmed_Report_Table();") . '
									</td>
								</tr>
							</table>
						</form>' . "\n";

		$x .= '<div id="TableDiv">';
		$x .= $linterface->Get_Ajax_Loading_Image();
		$x .= '</div>' . "\n";

		return $x;
	}

	function Get_Lesson_Non_Confirmed_Report_Table($FromDate, $ToDate, $Format = "web") {
		global $linterface, $Lang, $image_path, $LAYOUT_SKIN, $intranet_root;

		$records = $this->Get_Lesson_Non_Confirmed_Report_Records($FromDate, $ToDate);
		$record_count = count($records);

		$x = '';

		if ($Format == 'web') {
			$x .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
			$x .= '<tr>' . "\n";
			$x .= '<td>' . "\n";
			$x .= '<div class="content_top_tool">' . "\n";
			$x .= '<div class="Conntent_tool">' . "\n";
			$x .= $linterface->GET_LNK_PRINT("javascript:PrintPage();", "", "", "", "", 1);
			$x .= $linterface->GET_LNK_EXPORT("lesson_non_confirmed_report_export.php?FromDate=" . $FromDate . "&ToDate=" . $ToDate, "", "", "", "", 1);
			$x .= '</div>' . "\n";
			$x .= '<br style="clear: both;">' . "\n";
			$x .= '</div>' . "\n";
			$x .= '</td>' . "\n";
			$x .= '</tr>' . "\n";
			$x .= '</table>' . "\n";
		} else
			if ($Format == "print") {
				$x .= '<table width="96%" align="center" class="print_hide" border=0>
									<tr>
										<td align="right">' . $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submitPrint") . '</td>
									</tr>
								   </table>' . "\n";
			}

		if ($Format == 'export') {
			include_once ($intranet_root . "/includes/libexporttext.php");
			$lexport = new libexporttext();

			$Rows = array ();
			$Row = array ();
			$ExportColumn = array ();

			$ExportColumn[] = $Lang['General']['Date'];
			$ExportColumn[] = $Lang['SysMgr']['FormClassMapping']['Class'];
			$ExportColumn[] = $Lang['SysMgr']['Timetable']['Period'];
			$ExportColumn[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'];
			$ExportColumn[] = $Lang['SysMgr']['Timetable']['TeacherLoginID'];
			$ExportColumn[] = $Lang['SysMgr']['Timetable']['TeacherName'];

			if ($record_count > 0) {

				for ($i = 0; $i < $record_count; $i++) {
					$Row = array ();
					$Row[] = $records[$i]['RecordDate'];
					$Row[] = $records[$i]['ClassTitle'];
					$Row[] = $records[$i]['TimeSlotName'] . '(' . $records[$i]['StartTime'] . ' - ' . $records[$i]['EndTime'] . ')';
					$Row[] = $records[$i]['SubjectGroupTitle'];
					$Row[] = $records[$i]['UserLogin'];
					$Row[] = $records[$i]['TeacherName'];
					$Rows[] = $Row;
				}
			} else {
				$Row[] = $Lang['General']['NoRecordFound'];
				for ($i = 1; $i <= 5; $i++) {
					$Row[] = '';
				}
				$Rows[] = $Row;
			}

			$exportContent = $lexport->GET_EXPORT_TXT($Rows, $ExportColumn);
			$lexport->EXPORT_FILE('lesson_non-confirmed_report_' . $FromDate . '_to_' . $ToDate . '.csv', $exportContent);

			return;
		} else
			if ($Format == 'print') {
				$x .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
				$x .= '<tr>' . "\n";
				$x .= '<td>' . $Lang['LessonAttendance']['LessonNonConfirmedReport'] . ' (' . $Lang['General']['From'] . ' ' . $FromDate . ' ' . $Lang['General']['To'] . ' ' . $ToDate . ')</td>' . "\n";
				$x .= '</tr>' . "\n";
				$x .= '</table>' . "\n";

				$x .= '<table class="eSporttableborder" width="96%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
				$x .= '<tr>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['General']['Date'] . '</td>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['SysMgr']['Timetable']['Period'] . '</td>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'] . '</td>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['SysMgr']['Timetable']['TeacherLoginID'] . '</td>' . "\n";
				$x .= '<td class="eSporttdborder eSportprinttabletitle">' . $Lang['SysMgr']['Timetable']['TeacherName'] . '</td>' . "\n";
				$x .= '</tr>' . "\n";

				if ($record_count > 0) {
					for ($i = 0; $i < $record_count; $i++) {
						$x .= '<tr>';
						$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($records[$i]['RecordDate']) . '</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($records[$i]['ClassTitle']) . '</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($records[$i]['TimeSlotName'] . '(' . $records[$i]['StartTime'] . ' - ' . $records[$i]['EndTime'] . ')') . '</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($records[$i]['SubjectGroupTitle']) . '</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($records[$i]['UserLogin']) . '</td>';
						$x .= '<td class="eSporttdborder eSportprinttext">' . Get_String_Display($records[$i]['TeacherName']) . '</td>';
						$x .= '</tr>' . "\n";
					}
				} else {
					$x .= '<tr>' . "\n";
					$x .= '<td class="eSporttdborder eSportprinttext" colspan="6" align="center">' . $Lang['General']['NoRecordFound'] . '</td>' . "\n";
					$x .= '</tr>' . "\n";
				}
				$x .= '</table><br />' . "\n";
			} else {

				$x .= '<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">' . "\n";
				$x .= '<tr class="tabletop">' . "\n";
				$x .= '<td>' . $Lang['General']['Date'] . '</td>' . "\n";
				$x .= '<td>' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
				$x .= '<td>' . $Lang['SysMgr']['Timetable']['Period'] . '</td>' . "\n";
				$x .= '<td>' . $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'] . '</td>' . "\n";
				$x .= '<td>' . $Lang['SysMgr']['Timetable']['TeacherLoginID'] . '</td>' . "\n";
				$x .= '<td>' . $Lang['SysMgr']['Timetable']['TeacherName'] . '</td>' . "\n";
				$x .= '</tr>' . "\n";

				if ($record_count > 0) {
					for ($i = 0; $i < $record_count; $i++) {
						$css_num = ($i % 2) == 0 ? '2' : '';

						$x .= '<tr class="tablerow' . $css_num . '">';
						$x .= '<td class="tabletext">' . Get_String_Display($records[$i]['RecordDate']) . '</td>';
						$x .= '<td class="tabletext">' . Get_String_Display($records[$i]['ClassTitle']) . '</td>';
						$x .= '<td class="tabletext">' . Get_String_Display($records[$i]['TimeSlotName'] . '(' . $records[$i]['StartTime'] . ' - ' . $records[$i]['EndTime'] . ')') . '</td>';
						$x .= '<td class="tabletext">' . Get_String_Display($records[$i]['SubjectGroupTitle']) . '</td>';
						$x .= '<td class="tabletext">' . Get_String_Display($records[$i]['UserLogin']) . '</td>';
						$x .= '<td class="tabletext">' . Get_String_Display($records[$i]['TeacherName']) . '</td>';
						$x .= '</tr>' . "\n";
					}
				} else {
					$x .= '<tr class="tablerow2">' . "\n";
					$x .= '<td class="tabletext" colspan="6" align="center">' . $Lang['General']['NoRecordFound'] . '</td>' . "\n";
					$x .= '</tr>' . "\n";
				}
				$x .= '</table>' . "\n";

				if ($Format == "web") {
					$x .= '<script type="text/JavaScript" language="JavaScript">' . "\n";
					$x .= 'function PrintPage()
										   {
												url="lesson_non_confirmed_report_print.php?FromDate=' . $FromDate . '&ToDate=' . $ToDate . '"; 
												newWindow(url,10);
										   }';
					$x .= '</script>' . "\n";
				}
			}
		return $x;
	}
	
	function Get_Waive_Absence_Table($options)
	{
		global $Lang, $linterface, $sys_custom, $i_Attendance_DayType, $i_StudentAttendance_Slot_AM, $i_StudentAttendance_Slot_PM;
		
		$rankTarget = $options['rankTarget'];
		$rankTargetDetail = $options['rankTargetDetail'];
		$studentID = $options['studentID'];
		$StartDate = $options['StartDate'];
		$EndDate = $options['EndDate'];
		$DayType = $options['DayType'];
		$RecordType = $options['RecordType'];
		
		$records = $this->getWaiveAbsenceRecords($options);
		$record_count = count($records);
		
		$options_data = 'rankTarget='.$rankTarget.'&StartDate='.$StartDate.'&EndDate='.$EndDate.'&DayType='.$DayType.'&RecordType='.$RecordType;
		for($i=0;$i<count($rankTargetDetail);$i++){
			$options_data .= '&rankTargetDetail[]='.$rankTargetDetail[$i];
		}
		for($i=0;$i<count($studentID);$i++){
			$options_data .= '&studentID[]='.$studentID[$i];
		}
		
		$x = '';
		//$x .= $linterface->GET_LNK_PRINT_IP25('javascript:newWindow(\'overdue_notice.php?StartDate='.$StartDate.'&EndDate='.$EndDate.'&DayType='.$DayType.'&RecordType='.$RecordType.'\',10);',$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['OverdueNotice']);
		//$x .= $linterface->GET_LNK_PRINT_IP25('javascript:newWindow(\'demerit_table.php?StartDate='.$StartDate.'&EndDate='.$EndDate.'&DayType='.$DayType.'&RecordType='.$RecordType.'\',10);',$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DemeritForm']);
		$x .= $linterface->GET_LNK_PRINT_IP25('javascript:printReport(\'overdue_notice.php\');',$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['OverdueNotice']);
		$x .= $linterface->GET_LNK_PRINT_IP25('javascript:printReport(\'demerit_table.php\');',$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DemeritForm']);
		
		$x .= '<br />';
		$x .= '<table border="0" align="left" width="96%">
					<tr>
						<td class="attendance_norecord" width="15px">
							&nbsp;
						</td>
						<td class="tabletext">
							'.$Lang['StudentAttendance']['AbsenceWaived'].'
						</td>
					</tr>
				</table>';
		
		$absent_session_step = $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] > 0 ? $sys_custom['SmartCardAttendance_StudentAbsentSessionStep'] : 0.5;
		$absent_session_data = array();
		for($k=0.0;$k<=CARD_STUDENT_MAX_SESSION;$k+=$absent_session_step){
			$absent_session_data[] = array($k,$k);
		}
		
		$x.= '<table class="common_table_list_v30">'."\n";
			$x.='<thead>';
			$x.= '<tr>';
				$x.='<th class="num_check">#</th>';
				$x.='<th style="width:11%;">'.$Lang['StudentAttendance']['Class'].'</th>';
				$x.='<th style="width:5%;">'.$Lang['StudentAttendance']['ClassNumber'].'</th>';
				$x.='<th style="width:11%;">'.$Lang['StudentAttendance']['StudentName'].'</th>';
				$x.='<th style="width:10%;">'.$Lang['General']['RecordDate'].'</th>';
				$x.='<th style="width:5%;">'.$i_Attendance_DayType .'</th>';
				$x.='<th style="width:5%;">'.$Lang['StudentAttendance']['AbsentSessions'] .'</th>';
				$x.='<th style="width:5%;text-align:center;">'.$Lang['StudentAttendance']['WaiveAbsence'].'<br><input type="checkbox" id="checkmaster" name="checkmaster" onclick="Set_Checkbox_Value(\'WaiveAbsent[]\',this.checked);" /></th>';
				$x.='<th style="width:11%;">'.$Lang['StudentAttendance']['Reason'].'</th>';
				$x.='<th style="width:11%;">'.$Lang['StudentAttendance']['iSmartCardRemark'].'</th>';
				$x.='<th style="width:11%;">'.$Lang['StudentAttendance']['Notes'].'</th>'; 
				$x.='<th style="width:15%;">'.$Lang['General']['LastUpdatedTime'].'</th>';
				
				//$x.='<th class="num_check"><input type="checkbox" id="checkmaster" name="checkmaster" onclick="$(\'input[name="RecordID[]"]\').attr(\'checked\',this.checked);" /></th>';
			$x.= '</tr>'."\n";
			$x.='</thead>';
			$x.='<tbody>';
			
		if($record_count == 0){
			$x.='<tr>';
				$x.='<td colspan="12" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>';
			$x.='</tr>'."\n";
		}
		
		for($i=0;$i<$record_count;$i++) {
			$record_id = $records[$i]['RecordID'];
			$style = $records[$i]['WaiveAbsent']==1? ' style="background-color:#FAC5C6" ': '';
			$css = $records[$i]['WaiveAbsent']==1? 'background-color:#FAC5C6";': '';
			
			$absent_session_selection = $linterface->GET_SELECTION_BOX($absent_session_data, ' name="AbsentSession[]" ', '', $records[$i]['AbsentSession']);
			
			$x .= '<tr>';
				$x.='<td'.$style.'>'.($i+1).'</td>';
				$x.='<td'.$style.'>'.Get_String_Display($records[$i]['ClassName'],1).'</td>';
				$x.='<td style="text-align:center;'.$css.'">'.Get_String_Display($records[$i]['ClassNumber'],1).'</td>';
				$x.='<td'.$style.'>'.Get_String_Display($records[$i]['StudentName'],1).'</td>';
				$x.='<td'.$style.'>'.$records[$i]['RecordDate'].'</td>';
				$x.='<td'.$style.'>'.($records[$i]['DayType']==PROFILE_DAY_TYPE_AM?$i_StudentAttendance_Slot_AM:$i_StudentAttendance_Slot_PM).'</td>';
				$x.='<td style="text-align:center;'.$css.'">'.$absent_session_selection.'</td>';
				$x.='<td style="text-align:center;'.$css.'"><input type="checkbox" id="WaiveAbsent['.$i.']" name="WaiveAbsent[]" value="1" '.($records[$i]['WaiveAbsent']==1?'checked':'').' />
						<input type="hidden" name="RecordID[]" value="'.$records[$i]['RecordID'].'" /></td>';
				//$x.='<td>'.($records[$i]['WaiveAbsent']=='1'?$Lang['General']['Yes']:$Lang['General']['No']).'</td>';
				$x.='<td'.$style.'>'.Get_String_Display($records[$i]['Reason'],1).'</td>';
				$x.='<td'.$style.'>'.Get_String_Display($records[$i]['Remark'],1).'</td>';
				$x.='<td'.$style.'>'.Get_String_Display($records[$i]['AbsenceRemark'],1).'</td>';
				$x.='<td'.$style.'>'.Get_String_Display($records[$i]['DateModified'],1).'</td>';
				
			$x .= '</tr>'."\n";
		}
		
		$x .= '</tbody>';
		$x .= '</table>'."\n";
		
		$x.='<div class="edit_bottom_v30">'."\n";
			$x.= '<p class="spacer"></p>'."\n";
			if($record_count > 0){
				$x.= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitUpdate();", "updateBtn");
			}
			$x.= '<p class="spacer"></p>'."\n";
		$x.='</div>'."\n";
		$x .= '<input type="hidden" name="generated_options" id="generated_options" value="'.$options_data.'" />';
		
		return $x;
	}
	
	function getOverdueNotice($dataAry)
	{
		global $Lang, $sys_custom, $linterface;
		
		$fcm = new form_class_manage();
		$record_count = count($dataAry);
		
		$x = '';
		$x .= '<table width="96%" align="center" class="print_hide" border="0">
				<tbody>
					<tr>
						<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
					</tr>
				</tbody>
				</table>'."\n";
		
		for($i=0;$i<$record_count;$i++)
		{
			$year_class_id = $fcm->Get_Student_Studying_Class($dataAry[$i]['UserID']);
			$year_class = new year_class($year_class_id);
			$year_class->Get_Class_Teacher_List(false);
			$class_teacher_list = $year_class->ClassTeacherList;
			$class_teacher_name = $class_teacher_list[0]['TeacherName'];
			for($j=1;$j<count($class_teacher_list);$j++){
				$class_teacher_name .= ', '.$class_teacher_list[$j]['TeacherName'];
			}
			
			//$class_teacher_name = $dataAry[$i]['ClassTeacherName'];
			$class_name = $dataAry[$i]['ClassName'];
			$class_number = $dataAry[$i]['ClassNumber'];
			$student_name = $dataAry[$i]['StudentName'];
			$record_detail = $dataAry[$i]['RecordDetail'];
			$generated_date = $dataAry[$i]['GeneratedDate'];
			$x .= '<div style="page-break-inside:avoid; page-break-after:always; display:block; margin:1em;">';
				$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center" border-collapse="collapse" style="border:1px solid black;">';
				$x .= '<tbody>';
				$x .= '<tr>';
				$x .=  '<td valign="top" width="60%">';
					$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" valign="top" border-collapse="collapse">';
						$x .= '<tbody>';
							$x .= '<tr>';
								$x .= '<td style="text-align:left;vertical-align:top;" colspan="2">'.$class_teacher_name.':</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td style="text-align:center;" colspan="2">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['BelowStudentRecordOneDemerit'].'</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td colspan="2">';
									$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" border-collapse="collapse">';
										$x .= '<tbody>';
											$x .= '<tr>';
												$x .= '<td>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ClassName'].'</td><td>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ClassNumber'].'</td>
														<td>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['Name'].'</td><td>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['OverdueRecords'].'</td>';
											$x .= '</tr>';
											$x .= '<tr>';
												$x .= '<td style="border-bottom:5px double black;">&nbsp;</td><td style="border-bottom:5px double black;">&nbsp;</td>
														<td style="border-bottom:5px double black;">&nbsp;</td><td style="border-bottom:5px double black;">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DayMonthYearSession'].'</td>';
											$x .= '</tr>';
											$x .= '<tr>';
												$x .= '<td style="border-bottom:5px double black;">'.$class_name.'</td><td style="border-bottom:5px double black;">'.$class_number.'</td>
														<td style="border-bottom:5px double black;">'.$student_name.'</td><td style="border-bottom:5px double black;">'.$record_detail.'</td>';
											$x .= '</tr>';
										$x .= '</tbody>';
									$x .= '</table>';
								$x .= '</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['PleaseNoticeParent'].'</td>';
								$x .= '<td align="right">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['GrowthGroup'].'<br>'.$generated_date.'</td>';
							$x .= '</tr>';
						$x .= '</tbody>';
					$x .= '</table>';
				$x .=  '</td>';
				$x .=  '<td style="border-left:1px solid black;" width="40%">';
					$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" border-collapse="collapse">';
						$x .= '<tbody>';
							$x .= '<tr>';
								$x .= '<td colspan="3">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ToMoralGuidanceDepartment'].'</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td colspan="3">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['YourRef'].'('.$class_name.' / '.$class_number.', '.$generated_date.')</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td colspan="2" nowrap>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['AbsenteeismOnThatDay'].'</td>';
								$x .= '<td align="right"><input type="checkbox" value="" /></td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td colspan="2" nowrap>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['NotWaiveRecordOneDemerit'].'</td>';
								$x .= '<td align="right"><input type="checkbox" value="" /></td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td colspan="2" nowrap>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['WaivedNoDemerit'].'</td>';
								$x .= '<td align="right"><input type="checkbox" value="" /></td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td colspan="3">&nbsp;</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td width="50%">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ClassTeacherSignature'].'</td>';
								$x .= '<td colspan="2" width="50%" style="border-bottom:1px solid black;">&nbsp;</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['StudentSignature'].'</td>';
								$x .= '<td colspan="2" style="border-bottom:1px solid black;">&nbsp;</td>';
							$x .= '</tr>';
							$x .= '<tr>';
								$x .= '<td>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DateColon'].'</td>';
								$x .= '<td colspan="2" style="border-bottom:1px solid black;">&nbsp;</td>';
							$x .= '</tr>';
						$x .= '</tbody>';
					$x .= '</table>';
				$x .=  '</td>';
				$x .= '</tr>';
				$x .= '</tbody>';
				$x .= '</table>';
			
			$x .= '</div>';
			$x .= '<br>';
		}
		
		return $x;
	}
	
	function getNotWaiveAbsenceReport($dataAry)
	{
		global $Lang, $sys_custom, $linterface;
		
		$print_date = date("d/m/Y");
		$print_time = date("H:i:s");
		
		$record_count = count($dataAry);
		
		$x = '';
		$x .= '<table width="96%" align="center" class="print_hide" border="0">
					<tr>
						<td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</td>
					</tr>
				</table>
			   <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
				 <tr>
					<td align="left">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['PrintReportDate'].' '.$print_date.' &nbsp;&nbsp;'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['PrintReportTime'].' '.$print_time.'</td>
				 </tr>
			   </table>';
		$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
				 <tr>
					<td align="center"><h2>'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DemeritForm'].'</h2></td>
				 </tr>
			   </table>';	   
		$x .= '<table align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="96%">
			  	<tbody>
			  	<tr class="tabletop">
				    <th class="eSporttdborder eSportprinttabletitle" width="10%">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ClassName'].'</th>
				    <th class="eSporttdborder eSportprinttabletitle">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ClassNumber'].'</th>
					<th class="eSporttdborder eSportprinttabletitle">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['Name'].'</th>
					<th class="eSporttdborder eSportprinttabletitle">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['NotWaiveAbsenceMoreThanSevenDays'].'<br />'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DayMonthYearSession'].'</th>
					<th class="eSporttdborder eSportprinttabletitle">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['Truancy'].'</th>
					<th class="eSporttdborder eSportprinttabletitle">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['NoDemerit'].'</th>
					<th class="eSporttdborder eSportprinttabletitle">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['Demerit'].'</th>
					<th class="eSporttdborder eSportprinttabletitle">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['DateLong'].'</th>
			    </tr>';
			
		for($i=0;$i<$record_count;$i++){
			
			$class_name = $dataAry[$i]['ClassName'];
			$class_number = $dataAry[$i]['ClassNumber'];
			$student_name = $dataAry[$i]['StudentName'];
			$record_detail = $dataAry[$i]['RecordDetail'];
			
			$x .= '<tr>';
				$x .= '<td class="eSporttdborder eSportprinttext">'.$class_name.'</td>';
				$x .= '<td class="eSporttdborder eSportprinttext">'.$class_number.'</td>';
				$x .= '<td class="eSporttdborder eSportprinttext">'.$student_name.'</td>';
				$x .= '<td class="eSporttdborder eSportprinttext">'.$record_detail.'</td>';
				$x .= '<td class="eSporttdborder eSportprinttext">&nbsp;</td>';
				$x .= '<td class="eSporttdborder eSportprinttext">&nbsp;</td>';
				$x .= '<td class="eSporttdborder eSportprinttext">&nbsp;</td>';
				$x .= '<td class="eSporttdborder eSportprinttext">&nbsp;</td>';
			$x .= '</tr>';
		}	
			
			$x .= '</tbody>';
		$x .= '</table>';
		$x .= '<br />';
		$x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
				 <tr>
					<td align="right" width="80%">'.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['RecorderSignature'].'</td>
					<td align="right" width="20%" style="border-bottom:1px solid black;">&nbsp;</td>
				 </tr>
				 <tr>
					<td align="center" colspan="2">&lt;&lt; '.$Lang['StudentAttendance']['StudentAttendance_WaiveAbsent']['ReportCompleted'].' &gt;&gt;</td>
				 </tr>
			   </table>';
		
		
		return $x;
	}
	
}
?>