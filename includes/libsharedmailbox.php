<?php
// Editing by 
/******************************** Modification Log *****************************************
 * Create Date: 2010-12-03
 * 2014-07-03 (Carlos) : Use new approach to store encrypted password 
 * 2011-12-09 (Carlos) : Added a function to get raw member list Get_MailBox_Members()
 * 2011-04-13 (Carlos) : Fix Create_Shared_MailBox() should call open_account(), not base class's openAccount()
 *******************************************************************************************/
 
include_once("libdb.php");
include_once("imap_gamma.php");

class libsharedmailbox extends libdb
{
	function libsharedmailbox()
	{
		$this->libdb();
	}
	
	function GET_MODULE_OBJ_ARR()
	{
		global $PATH_WRT_ROOT, $plugin, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $intranet_session_language, $intranet_root;
        global $Lang, $special_feature;
        
        # Current Page Information init
		$PageManagement	= 0;
		$PageMgmt_SharedMailBox = 0;
		
		switch ($CurrentPage) {
	        case "Mgmt_SharedMailBox":
	        	$PageManagement = 1;
	        	$PageMgmt_SharedMailBox = 1;
	        	break;
	    }
	    
	    $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/SharedMailBox/";
	    
	    # Management 
		$MenuArr["Management"] = array($Lang['Menu']['AccountMgmt']['Management'], "#", $PageManagement);
		$MenuArr["Management"]["Child"]["MgmtSharedMailBox"] = array($Lang['SharedMailBox']['SharedMailBox'], $PATH_WRT_ROOT."home/eAdmin/AccountMgmt/SharedMailBox/", $PageMgmt_SharedMailBox);
	
		### module information
        $MODULE_OBJ['title'] = $Lang['SharedMailBox']['SharedMailBox'];
        $MODULE_OBJ['title_css'] = "menu_opened";
        $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_iAccount.gif";
        
        $MODULE_OBJ['menu'] = $MenuArr;
        
        return $MODULE_OBJ;
	}
	
	function Get_Shared_MailBox($MailBoxID="",$Keyword="")
	{
		$sql = "Select 
					MailBoxID,
					MailBoxName,
					MailBoxPassword,
					CreateDate,
					CreateBy,
					LastModified,
					ModifiedBy 
				From 
					MAIL_SHARED_MAILBOX 
				Where 1=1 ";
		if($MailBoxID!=""){
			if(is_array($MailBoxID) && sizeof($MailBoxID)>0)
				$sql .= "AND MailBoxID IN (".implode(",",$MailBoxID).")";
			else
				$sql .= "AND MailBoxID = '".$MailBoxID."' ";
		}
		if ($Keyword != "") {
			$sql .= "AND MailBoxName like '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' ";
		}
		$sql .= "Order by 
						  MailBoxName
				";
		
		$Result = $this->returnArray($sql,7);
		
		return $Result;
	}
	
	function Get_Shared_MailBox_By_Name($MailBoxName)
	{
		$sql = "Select 
					MailBoxID,
					MailBoxName,
					MailBoxPassword,
					CreateDate,
					CreateBy,
					LastModified,
					ModifiedBy 
				From 
					MAIL_SHARED_MAILBOX 
				Where MailBoxName = '".$this->Get_Safe_Sql_Query($MailBoxName)."' ";
		$Result = $this->returnArray($sql,4);
		
		return $Result;
	}
	
	function Add_User_To_MailBox($MailBoxID,$UID) {
		$sql = "Select 
					count(1) 
				From 
					MAIL_SHARED_MAILBOX_MEMBER 
				Where 
					MailBoxID = '".$MailBoxID."' 
					And 
					UserID = '".$UID."' ";
		$Exist = $this->returnVector($sql);
		
		if ($Exist[0] != 0) {
			return true;
		}
		else {
			$sql = "Insert Into MAIL_SHARED_MAILBOX_MEMBER 
					(
						MailBoxID, 
						UserID, 
						CreateDate, 
						CreateBy,
						LastModified,
						ModifiedBy 							 
					) 
					Values 
					(
						'".$MailBoxID."', 
						'".$UID."', 
						NOW(), 
						'".$_SESSION['UserID']."',
						NOW(),
						'".$_SESSION['UserID']."' 
					) ";
			$Result = $this->db_db_query($sql);
			
			return $Result;
		}
	}
	
	function Remove_User_From_MailBox($MailBoxID,$UID) {
		$sql = "Delete From MAIL_SHARED_MAILBOX_MEMBER 
						Where 
							MailBoxID = '".$MailBoxID."' 
							AND 
							UserID = '".$UID."' ";
		$Result = $this->db_db_query($sql);
		
		return $Result;
	}
	
	function Update_MailBox_Last_Update($MailBox) {
		$sql = "Update MAIL_SHARED_MAILBOX 
					Set 
						ModifiedBy = '".$_SESSION['UserID']."', 
						LastModified = NOW() 
					Where 
						MailBoxName = '".$this->Get_Safe_Sql_Query($MailBox)."' ";
		$Result = $this->db_db_query($sql);
		
		return $Result;
	}
	
	function Create_Shared_MailBox($MailBoxName,$MailBoxPassword) {
		global $SYS_CONFIG;
		
		include_once("libpwm.php");
		
		$IMap = new imap_gamma(true);
		$libpwm = new libpwm();
		
		$Result['CreateMailBox'] = $IMap->open_account($MailBoxName,$MailBoxPassword);
		
		if ($Result['CreateMailBox']) {
			$sql = "Insert Into MAIL_SHARED_MAILBOX 
							(
							MailBoxName, 
							MailBoxPassword, 
							CreateDate, 
							CreateBy,
							LastModified,
							ModifiedBy 
							) 
						Values 
							(
							'".$this->Get_Safe_Sql_Query($MailBoxName)."', 
							'',
							NOW(), 
							'".$_SESSION['UserID']."',
							NOW(),
							'".$_SESSION['UserID']."' 
							)
					 ";
			$Result['CreateDbRecord'] = $this->db_db_query($sql);
			if($Result['CreateDbRecord']){
				$mailboxId = $this->db_insert_id();
				$mid_ary = array();
				$mid_ary[$mailboxId] = $MailBoxPassword;
				$libpwm->setShareMailboxData($mid_ary);
			}
		}
		//debug_r($Result);
		return $mailboxId > 0? $mailboxId : false;
	}
	
	function Get_MailBox_Password($MailBoxName) {
		global $SYS_CONFIG;
		include_once("libpwm.php");
		
		// Check if is personal email
		$sql = "Select 
							count(1) 
						From 
							INTRANET_USER 
						Where 
							ImapUserEmail = '".$this->Get_Safe_Sql_Query($MailBoxName)."'
						AND UserID = '".$_SESSION["UserID"]."' ";
		
		$Result = $this->returnVector($sql);
		
		if ($Result[0] > 0 ) {
			return $_SESSION['SSV_LOGIN_PASSWORD'];
		}
		
		// Check if is shared mailbox
		$sql = "Select 
					 MailBoxID 
				From 
					MAIL_SHARED_MAILBOX 
				Where 
					MailBoxName = '".$this->Get_Safe_Sql_Query($MailBoxName)."' ";
		$Result = $this->returnVector($sql);
		
		if ($Result[0] != "" && $Result[0]>0) {
			$libpwm = new libpwm();
			$midToPw = $libpwm->getShareMailboxData(array($Result[0]));
			return $midToPw[$Result[0]];
		}
		
		return false;
	}
	
	function Get_MailBox_UserList($MailBoxID) {
		$NameField = getNameFieldByLang2("u.");
		$sql = "Select 
					u.UserID,
					$NameField as UserName 
				From 
					INTRANET_USER as u 
					INNER JOIN MAIL_SHARED_MAILBOX_MEMBER as m ON m.UserID = u.UserID 
				Where 
					MailBoxID = '".$MailBoxID."' 
					AND u.RecordStatus = '1' ";
		$Result = $this->returnArray($sql,2);
		
		return $Result;
	}
	
	function Get_MailBox_Members($MailBoxID) {
		$sql = "select 
					UserID 
				from MAIL_SHARED_MAILBOX_MEMBER 
				where MailBoxID = '".$MailBoxID."' ";
				
		$Result = $this->returnArray($sql,2);
		
		return $Result;
	}
	
	function Change_Shared_Mailbox_Password($MailBoxName,$Password) {
		global $SYS_CONFIG;
		include_once("libpwm.php");
		
		$libpwm = new libpwm();
		$IMap = new imap_gamma(true);
		$Result['ResetPassword'] = $IMap->change_password($MailBoxName,$Password);
		
		if ($Result['ResetPassword']) {
			$sql = "Select 
						 MailBoxID 
					From 
						MAIL_SHARED_MAILBOX 
					Where 
						MailBoxName = '".$this->Get_Safe_Sql_Query($MailBoxName)."' ";
			$rs = $this->returnVector($sql);
			
			if(count($rs)>0 && $rs[0]>0){
				$midToPw = array();
				$midToPw[$rs[0]] = $Password;
				$Result['UpdateDbRecord'] = $libpwm->setShareMailboxData($midToPw);
			}else{
				$Result['UpdateDbRecord'] = false;
			}
			/*
			$sql = "Update MAIL_SHARED_MAILBOX 
							Set 
								MailBoxPassword = '".$this->Get_Safe_Sql_Query($Password)."',
								LastModified = NOW(),
								ModifiedBy = '".$_SESSION['UserID']."'  
							Where 
								MailBoxName = '".$this->Get_Safe_Sql_Query($MailBoxName)."' ";
			$Result['UpdateDbRecord'] = $this->db_db_query($sql);
			*/
		}
		
		return !in_array(false,$Result);
	}
	
	function Remove_MailBox($MailBoxID,$MailBoxName)
	{
		$IMap = new imap_gamma(true);
		
		$Result['DeleteOSMailBox'] = $IMap->delete_account($MailBoxName);
		if($Result['DeleteOSMailBox']){
			$sql = "Delete From MAIL_SHARED_MAILBOX Where MailBoxID = '".$MailBoxID."' ";
			$Result['DeleteDBMailBox'] = $this->db_db_query($sql);
			if($Result['RemoveDBMailBox']){
				$sql = "Delete From MAIL_SHARED_MAILBOX_MEMBER Where MailBoxID = '".$MailBoxID."' ";
				$Result['RemoveDBMembers'] = $this->db_db_query($sql);
				$sql = "Delete From MAIL_PREFERENCE Where MailBoxName = '".$this->Get_Safe_Sql_Query($MailBoxName)."' ";
				$Result['DeletePreference'] = $this->db_db_query($sql);
			}
		}
		
		return !in_array(false,$Result);
	}
	
	// Exist - true ; Not exist - false
	function Check_MailBox_Exist($MailBoxName)
	{
		$IMap = new imap_gamma(true);
		$Result = $IMap->is_user_exist($MailBoxName);
		return $Result;
	}
	
	function Get_MailBox_Total_Quota($MailBoxName)
	{
		$IMap = new imap_gamma(true);
		$quota = $IMap->getTotalQuota($MailBoxName,"iMail");
		return $quota;
	}
	
	function Get_MailBox_Used_Quota($MailBoxName)
	{
		$IMap = new imap_gamma(true);
		$quota = $IMap->getUsedQuota($MailBoxName,"iMail");
		return $quota;
	}
	
	function Set_MailBox_Total_Quota($MailBoxName,$Quota)
	{
		$IMap = new imap_gamma(true);
		return $IMap->setTotalQuota($MailBoxName, $Quota, "iMail");
	}
}

?>