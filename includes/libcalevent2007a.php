<?php
## Using By :  

##########################################################
## Modification Log
##
## 2019-05-21 Cameron
## - modify Display_Calendar_Event(), check if $SpecialTimetableSettingsArr is empty or not before using it as array
##
## 2018-09-05 Anna
## - modified returnAllEvents(), returnAllEvents_Portal(), display titleEng according to UI
##
## 2018-09-04 Anna
## - modified displayCalandar_MonthlyEventTable() , display titleEng according to UI
##
## 2015-07-31 Evan
## - Modified displayCalandar_MonthlyEventTable() getEventInfo(), event start in Sept end in Oct with be shown in the event table of both Sept and Oct
##
## 2015-01-14 Omas
## - Modified displayCalendar_eServiceeBookingSelectDate(), add a div to the Date title as a button
##
## 2014-05-13 Carlos
## - Modified displayCalandar_CyclePeriodProductionView(), displayCalandar_CyclePeriodPrintingView() to batch init event info in one month
##
## 2013-10-04 Carlos
## - Modified displayCalandar_CyclePeriodProductionView(), Display_Calendar_Event(), displayCalandar_CyclePeriodPrintingView(),
##	 getEventInfo(), displayCalandar_MonthlyEventTablePrintingView(), add parameter $ClassGroupID
##
## 2012-08-01 Ivan
## -	added function synEventToModules() to syn holidays to library system (can be applied to different modules later)
##
## 2010-07-11 Marcus
## -	modified displayCalandar_CyclePeriodPreview, changed getPreviewCycleInfo to getCycleInfoByYearMonth
##
## 2010-10-11 Ronald
## -	new : function displayEventType_Portal() & returnAllEvents_Portal() & displayCalendarEvent_Portal() which used in /home/index.php
##		aim : reduce no. of query executed in the portal page.
##
## 2010-04-22 Marcus
## - Add displayCalendar_eServiceeBookingSelectDate for eBooking -> eService
##
## 2009-12-15: Max (200912141001)
## - Add function [displayCalandar_CyclePeriodPrintingView(), displayCalandar_MonthlyEventTablePrintingView(), getEventInfo()]
##########################################################
  class libcalevent2007 extends libcalevent 
  {  
    var $image_path2;
    
      function libcalevent2007($ts="", $v="")
      {
        global $intranet_session_language, $image_path, $LAYOUT_SKIN;
        
        $this->libcalevent($ts,$v);     
        $this->image_path2 = $image_path."/".$LAYOUT_SKIN;
      }

       function displayCalendar()
       {
         global $image_path,$intranet_httppath,$i_EventShowAll,$i_EventCloseImage;
         $rb_image_path = "$image_path/resourcesbooking";
         $lower_bound = mktime(0,0,0,$this->m,1,$this->y);
         $upper_bound = mktime(0,0,0,$this->m+2,1,$this->y) - 1;
         //$upper_bound = mktime(0,0,0,$this->m+1,1,$this->y) - 1;
         
               
         //$event_list = $this->returnAllEvents($lower_bound,$upper_bound);
         $event_list = $this->returnAllEvents_Portal($lower_bound,$upper_bound);
         $x .= "<script language='javascript' >\n";
         $x .= "var table_head = '<table width=\'260\' border=\'1\' bordercolorlight=\'#FBFDEA\' bordercolordark=\'#B3B692\' cellpadding=\'2\' cellspacing=\'0\' bgcolor=\'#FBFDEA\' >';";
         $x .= "table_head += '<tr><td align=\'center\' ><table width=\'250\' border=\'0\' cellpadding=\'3\' cellspacing=\'0\' class=\'body\' >';";
         $x .= "table_head += '<tr><td>&nbsp;</td><td align=\'right\'><a href=\'javascript:hideLayer(\"ToolMenu\")\' >$i_EventCloseImage</a></td></tr>';";
         $x .= "table_head += '<tr><td colspan=\'2\' align=\'center\' ><img src=\'$image_path/index/el_2lines.gif\' width=\'223\' height=\'8\' ></td></tr>';";
         
         $x .= "var table_tail = '<tr align=center><td colspan=\'2\' valign=top><img src=$image_path/index/el_1lines.gif width=223 height=8></td></tr><tr><td>&nbsp;</td>';";
         $x .= "table_tail += '<td align=right><a href=javascript:showAllEventList()>$i_EventShowAll</a></td></tr>';";
         $x .= "table_tail += '<tr align=center><td colspan=\'2\' valign=top><img src=$image_path/index/el_1lines.gif width=223 height=8></td></tr><tr><td>&nbsp;</td><td align=right><a href=javascript:hideLayer(\"ToolMenu\")>$i_EventCloseImage</a></td></tr></table>';";
         $x .= "var event_list_text = '';\n";
         
         for ($i=0; $i<sizeof($event_list); $i++)
         {
           list($eventID, $eventDate, $eventTitle, $eventType) = $event_list[$i];
           $eventTitle = addslashes($eventTitle);
           $x .= "event_list_text += '<tr><td width=90>$eventDate</td><td width=160><a href=javascript:fe_view_event($eventID)>$eventTitle</a></td></tr>';\n";
           $arrEventDate[] = $eventDate;
           $arrTempEventType[$eventDate][] = $eventType;
         }
         // remove duplicate data
         
         if(sizeof($arrTempEventType)>0) {
         	foreach($arrEventDate as $key=>$date){
         		$arrEventType[$date] = array_unique($arrTempEventType[$date]);
         	}
         } else {
         	$arrEventType = array();
         }
         
         
         $x .= "var event_layer_text = table_head+event_list_text+table_tail;\r\n";
         $x .= "function showEventList()
         {
           writeToLayer('ToolMenu',event_layer_text)
           showLayer('ToolMenu');
         }
         function showAllEventList()
         {		
                newWindow('$intranet_httppath/home/allevent.php', 3);
         }
         \n";
         $x .= "</script>\n";
         $x .= $this->displayCalendarArray($this->m, $this->y, "", $arrEventDate, $arrEventType);
         return $x;
       }
    
    
       function displayCalendarArray($month, $year, $order="", $arrEventDate="", $arrEventType="")
       {
            global $i_frontpage_day,$i_EventList,$image_path, $intranet_session_language;
            
            $row = $this->returnCalendarArray($month, $year);
            $a = new libcycleperiods();
            $cycles_array = $a->getCycleInfoByYearMonth($year,$month);
  			
            $x .= "
		            <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"indexcalendartable\">
		            <tr>
		              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweeks\" >&nbsp;".$i_frontpage_day[0]."</td>
		              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[1]."</td>
		              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[2]."</td>
		              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[3]."</td>
		              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[4]."</td>
		              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[5]."</td>
		              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[6]."</td>
                 ";
			for($i=0; $i<sizeof($row); $i++)
			{
				$txtTargetDateEventType = "";		// unset variable
				
				$x .= ($i%7==0) ? "</tr>\n<tr>\n" : "";
				if(!empty($row[$i]))
				$date_string = date("Y-m-d",$row[$i]);
				$cycle = $cycles_array[$date_string];
				$display = $cycle[2];
				
				if(sizeof($arrEventType[$date_string])>0){
					$txtTargetDateEventType = implode(",",$arrEventType[$date_string])."<BR>";
				}
				$x .= $this->displayCalendarEvent_Portal($row[$i],$display,$txtTargetDateEventType);
				//$x .= $this->displayCalendarEvent($row[$i],$display);
			}
			$x .= "</tr></table>\n";
			         
			return $x;
        }
        
         function displayCalendarEvent($ts, $cycle)
         {
         		if($ts=="")
         		{
					$x = "<td class=\"indexcalendartabletd\" ><br></td>";
              	}
              	else
              	{
              		$this->initEventRecordByTimestamp($ts);
              		
              		if ($this->IsSchoolEvent($ts)) $ClassType = " class='school_event' ";
              		
					if ($this->IsAcademicEvent($ts)) $ClassType = " class='academic_event' ";
					
					if ($this->IsAcademicEvent($ts) && $this->IsSchoolEvent($ts)) 
                   	{
						$ClassType = " class='indexcalendarbothevent' ";
            		}
                   
                   	if($this->IsHolidayEvent($ts)){
                   		$Event = "<font class='public_holiday'>".date("j",$ts)."</font>";
                	} else if($this->IsSchoolHolidayEvent($ts)){
                		$Event = "<font class='school_holiday'>".date("j",$ts)."</font>";
                	} else {
                		if(date("w",$ts) == 0){
                			$Event = "<font class='public_holiday'>".date("j",$ts)."</font>";
                		}else{
                			$Event = date("j",$ts);
                		}
                	}
                   
                   if ($cycle=="") $cycle = "&nbsp;";
                   
                   if ($this->IsGroupEvent($ts))
                   {
                     $EventImage = "<img src=\"".$this->image_path2."/index/calendar/icon_groupevent.gif\"  >";
                   }
                   else {
                     $EventImage = "<img src=\"".$this->image_path2."/10x10.gif\" width=\"3\" height=\"7\" />";
                   }
                                      
                   $x  = "<td class=\"indexcalendartabletd\" >";
                   if($this->IsResource)
                   {
                        $x .= "<a href=?ts=$ts>".$Event."</a>";
                   }
                   else
                   {
                     if ($this->IsHolidayEvent($ts))
                     {
                          $Event = ($this->IsEvent($ts)) ? "<a href='javascript:fe_view_event_by_date($ts)' class='indexcalendarholidaylink' >".$Event."</a>" : $Event;
                     } else {
                          $Event = ($this->IsEvent($ts)) ? "<a href='javascript:fe_view_event_by_date($ts)' class='indexcalendardaylink' >".$Event."</a>" : $Event;
                     }                       
    
		              $x .= "
		                  <table width=\"25\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" {$ClassType} >
		                  <tr>
		                    <td align=\"left\" valign=\"top\"  class=\"indexcalendarremark\">$cycle</td>
		                    <td align=\"right\" valign=\"top\" >{$EventImage}</td>
		                  </tr>
		                  <tr>
		                    <td colspan=\"2\" align=\"center\" class=\"indexcalendarday\" >$Event</td>
		                  </tr>
		                  </table>";
    
                   }
                   $x .= "</td>\n";
              }
              return $x;
         }
         
         function displayCalendarEvent_Portal($ts, $cycle, $txtTargetDateEventType="")
         {
         		if($ts=="")
         		{
					$x = "<td class=\"indexcalendartabletd\" ><br></td>";
              	}
              	else
              	{
              		if($txtTargetDateEventType != "")
              		{
              			$arrTargetDateEventType = explode(",",$txtTargetDateEventType);
              			if( (in_array(0,$arrTargetDateEventType)) && (in_array(1,$arrTargetDateEventType)) ) {
              				$ClassType = " class='indexcalendarbothevent' ";
              			}else if(in_array(0,$arrTargetDateEventType)) {
							$ClassType = " class='school_event' ";
              			}else if(in_array(1,$arrTargetDateEventType)) {
              				$ClassType = " class='academic_event' ";
              			}
              			
              			if(in_array(3,$arrTargetDateEventType)){
                   			$Event = "<font class='public_holiday'>".date("j",$ts)."</font>";
	                	} else if(in_array(4,$arrTargetDateEventType)){
	                		$Event = "<font class='school_holiday'>".date("j",$ts)."</font>";
	                	} else {
	                		if(date("w",$ts) == 0){
	                			$Event = "<font class='public_holiday'>".date("j",$ts)."</font>";
	                		}else{
	                			$Event = date("j",$ts);
	                		}
	                	}

						if ($cycle=="") $cycle = "&nbsp;";
						
						if (in_array(2,$arrTargetDateEventType))
						{
							$EventImage = "<img src=\"".$this->image_path2."/index/calendar/icon_groupevent.gif\"  >";
						}
						else
						{
							$EventImage = "<img src=\"".$this->image_path2."/10x10.gif\" width=\"3\" height=\"7\" />";
						}
						
						
						if($this->IsResource)
						{
							$x .= "<a href=?ts=$ts>".$Event."</a>";
						}
						else
						{
							if ((in_array(3,$arrTargetDateEventType)) || (in_array(4,$arrTargetDateEventType)))
							{
								//$Event = ($this->IsEvent($ts)) ? "<a href='javascript:fe_view_event_by_date($ts)' class='indexcalendarholidaylink' >".$Event."</a>" : $Event;
								$Event = "<a href='javascript:fe_view_event_by_date($ts)' class='indexcalendarholidaylink' >".$Event."</a>";
							} else {
								//$Event = ($this->IsEvent($ts)) ? "<a href='javascript:fe_view_event_by_date($ts)' class='indexcalendardaylink' >".$Event."</a>" : $Event;
								$Event = "<a href='javascript:fe_view_event_by_date($ts)' class='indexcalendardaylink' >".$Event."</a>";
							}	
			            }
              		}
              		else
              		{
              			if ($cycle=="") $cycle = "&nbsp;";
              			$ClassType = "&nbsp;";
              			$EventImage = "<img src=\"".$this->image_path2."/10x10.gif\" width=\"3\" height=\"7\" />";
              			
              			if(date("w",$ts) == 0){
                			$Event = "<font class='public_holiday'>".date("j",$ts)."</font>";
                		}else{
                			$Event = date("j",$ts);
                		}
              		}
              		$x  = "<td class=\"indexcalendartabletd\" >";
              		$x .= "
			                  <table width=\"25\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" {$ClassType} >
			                  <tr>
			                    <td align=\"left\" valign=\"top\"  class=\"indexcalendarremark\">$cycle</td>
			                    <td align=\"right\" valign=\"top\" >{$EventImage}</td>
			                  </tr>
			                  <tr>
			                    <td colspan=\"2\" align=\"center\" class=\"indexcalendarday\" >$Event</td>
			                  </tr>
			                  </table>";
			        $x .= "</td>\n";
              }
              return $x;
         }

        function displayPrevMonth()
        {
              //global $intranet_session_language, $image_path, $LAYOUT_SKIN, $ListType ;
                
              global $Lang;
                                        
              //$x  = "<a href=\"?v=".$this->view."&ts=".$this->go(-1,"m")."&ListType={$ListType}\" >";             
              $x  = "<a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_CALENDER(document.CalForm, ".$this->view.", ".$this->go(-1,"m")." );jAJAX_GO_EVENT(document.EventForm,0,".$this->go(-1,"m").");\">";
              $x .= "<img title=\"". $Lang['Calendar']['PrevMonth'] ."\" src=\"".$this->image_path2."/index/calendar/btn_prev_month.gif\" name=\"pmonth\" border=\"0\" id=\"pmonth\" onMouseOver=\"MM_swapImage('pmonth','','/images/2009a/index/calendar/btn_prev_month_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" ></a>\n";
              
              return $x;
        }

		function displayNextMonth()
		{
			//global $intranet_session_language, $ListType ;
			global $Lang;
      
			//$x = "<a href=\"?v=".$this->view."&ts=".$this->go(+1,"m")."&ListType={$ListType}\" >";              
			$x  = "<a href=\"javascript:void(0)\" onclick=\"jAJAX_GO_CALENDER(document.CalForm, ".$this->view.", ".$this->go(+1,"m")." );jAJAX_GO_EVENT(document.EventForm,0,".$this->go(+1,"m").");\" >";
			$x .= "<img title=\"". $Lang['Calendar']['NextMonth'] ."\" src=\"".$this->image_path2."/index/calendar/btn_next_month.gif\" name=\"nmonth\" border=\"0\" id=\"nmonth\" onMouseOver=\"MM_swapImage('nmonth','','/images/2009a/index/calendar/btn_next_month_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" ></a>\n";
      
			return $x;
      	}

      	function displayMonthText()
		{
			$x = date("F Y", $this->ts);
          
			return $x;
        }
            
        
		function returnAllEvents($lower_bound="",$upper_bound="")
        {
			if ($lower_bound == "" || $upper_bound == "")
			{
			}
	        else
	        {
				$conds = " AND UNIX_TIMESTAMP(EventDate) BETWEEN $lower_bound AND $upper_bound";
	        }
			//$sql = "SELECT a.EventID FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = ".$this->UserID." AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' AND a.RecordType = '3' $conds";
			$sql = "SELECT a.EventID FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b, INTRANET_USERGROUP AS c WHERE c.UserID = ".$this->UserID." AND c.GroupID = b.GroupID AND b.EventID = a.EventID AND a.RecordStatus = '1' $conds";
			$row1 = $this->returnVector($sql);
	        //$sql = "SELECT EventID FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'3' $conds";
	        $sql = "SELECT EventID FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType <>'2' $conds";
	        $row2 = $this->returnVector($sql);
	        //$sql = "SELECT a.EventID FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '3' AND a.RecordStatus = 1 $conds";
	        $sql = "SELECT a.EventID FROM INTRANET_EVENT AS a LEFT OUTER JOIN INTRANET_GROUPEVENT as b ON a.EventID = b.EventID WHERE b.EventID IS NULL AND a.RecordType = '2' AND a.RecordStatus = '1' $conds";
	        $row3 = $this->returnVector($sql);
	        $all = array_merge($row1, $row2);
	        $all = array_merge($all, $row3);
        	if (sizeof($all) == 0) return array();
	        $sql = "SELECT 
						EventID, DATE_FORMAT(EventDate,'%Y-%m-%d') AS EventDate, 
                        If (TitleEng IS NULL Or TitleEng = '',
                        Title, 
                        ".Get_Lang_Selection('Title','TitleEng').") 
                        as Title,
                        RecordType
					FROM 
						INTRANET_EVENT
					WHERE 
						EventID IN (".implode(",",$all).") 
					ORDER BY 
						EventDate ASC , Title 
					";
              
			return $this->returnArray($sql,4);
		}
		
		function returnAllEvents_Portal($lower_bound="",$upper_bound="")
        {
			if ($lower_bound == "" || $upper_bound == "")
			{
			}
	        else
	        {
				$conds = " AND (UNIX_TIMESTAMP(event.EventDate) BETWEEN $lower_bound AND $upper_bound) ";
	        }
	        //event.EventID, DATE_FORMAT(event.EventDate,'%Y-%m-%d') AS EventDate, event.Title, event.RecordType
	        ## Get All Events Except Group Event
	        
	        $Title = Get_Lang_Selection('event.Title','event.TitleEng');
	        
            $sql_1 = "SELECT 
						event.EventID, DATE_FORMAT(event.EventDate,'%Y-%m-%d') AS EventDate, 
                    If (event.TitleEng IS NULL Or event.TitleEng = '',
                    event.Title, 
                    $Title) 
                    as Title,  
                    event.RecordType
					FROM 
						INTRANET_EVENT as event 
					WHERE 
						event.RecordType != 2
						$conds 
					ORDER BY 
						event.EventDate ASC , event.Title 
					";
			## Get All Group Events Belonged to User
            $sql_2 = "SELECT 
						event.EventID, DATE_FORMAT(event.EventDate,'%Y-%m-%d') AS EventDate, 
                        If (event.TitleEng IS NULL Or event.TitleEng = '',
                        event.Title, 
                        $Title) 
                        as Title, 
                        event.RecordType
					FROM 
						INTRANET_EVENT as event LEFT OUTER JOIN
						INTRANET_GROUPEVENT as group_event ON (event.EventID = group_event.EventID) LEFT OUTER JOIN
						INTRANET_USERGROUP as user_group ON (group_event.GroupID = user_group.GroupID)
					WHERE 
						event.RecordType = 2
						AND user_group.UserID = ".$_SESSION['UserID']."
						$conds 
					ORDER BY 
						event.EventDate ASC , event.Title 
					";
			$table = "(".$sql_1.") UNION (".$sql_2.")";
			
			$sql = "SELECT
						final_event.EventID, DATE_FORMAT(final_event.EventDate,'%Y-%m-%d') AS EventDate, 
                        final_event.Title, 
                        final_event.RecordType
					FROM
						($table) as final_event
					Order By 
						final_event.EventDate ASC , final_event.Title";
// 			DEBUG_PR($sql);
            return $this->returnArray($sql,4);
		}
        
        function displayEventType($type="")
        {
	        global $image_path,$intranet_httppath,$i_EventShowAll,$i_EventCloseImage, $i_no_record_exists_msg, $LAYOUT_SKIN, $Lang;
	        
	        $rb_image_path = "$image_path/resourcesbooking";
	                
	        if ($type=="1")
	        {
	        // Monthly  
	          $lower_bound = mktime(0,0,0,$this->m,1,$this->y);
	          $upper_bound = mktime(0,0,0,$this->m+1,1,$this->y) - 1;       
	        } 
	        else {
	        //Daily 
	          $lower_bound = mktime(0,0,0,$this->m,$this->d,$this->y);
	          $upper_bound = mktime(0,0,0,$this->m,$this->d+1,$this->y) - 1;                  
	        }
	        
	        $event_list = $this->returnAllEvents($lower_bound,$upper_bound);
	          
	        $event_list2 = array();
	        for ($i=0;$i<count($event_list);$i++)
	        {
	          $eventdate = $event_list[$i][1];          
	          $event_list2[$eventdate][] = $event_list[$i]; 
	        }
	                        
	        if(sizeof($event_list2)==0)
	        {
	          $x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"> ";
	          $x .= "     
	              <tr>
	                <td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
	                <td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
	                <td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
	              </tr>
	              <tr>
	                <td colspan='3'>&nbsp;</td>
	              </tr>
	              ";      
	          $x .= "</table>";   
	        }
	        else
	        {
	          $x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"> ";
	          foreach($event_list2 as $DateID => $DateArr)          
	          {
		         $weekday = $Lang['General']['DayType4'][(date("w",strtotime($DateID)))];
		        $x .= "
	                <tr id=\"Event_".$DateID."\">
	                  <td width=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/icon_arrow.gif\" ></td>
	                  <td class=\"indexeventlist\">{$DateID} ({$weekday})</td>
	                </tr>               
	                ";
	            $x .= "
	                <tr>
	                  <td>&nbsp;</td>
	                  <td>
	                  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
	                ";

	            for ($i=0;$i<count($DateArr);$i++)
	            {
		            switch($DateArr[$i][3])
		            {
			         	case 0: $this_css = "indexeventlink2"; break;   # school event  - green
			         	case 1: $this_css = "indexeventlink3"; break;   # academic event -  blue 
			         	case 2: $this_css = "indexeventlink1"; break;   # group event - orange
			         	case 3: $this_css = "indexeventlink4"; break;   # public holiday - red 
			         	case 4: $this_css = "indexeventlink5"; break;   # school holiday- purple 
			         	default:$this_css = "indexeventlink2"; break;   
		            }
		            $x .= "
	                    <tr>
	                      <td width=\"5\" valign=\"top\" ><a class=\"". $this_css ."\">-</a></td>
	                      <td><a href=\"javascript:void(0)\" onclick=\"fe_view_event(".$DateArr[$i][0].", 1)\" class=\"". $this_css ."\"> ".$DateArr[$i][2]." </a></td>
	                    </tr>
	                    ";      
	            }   
	            
	            $x .= "
	                  </table>
	                  </td>
	                </tr>
	                ";
	                                
	            
	          }
	          $x .= "</table>";
	        }
	        
	        
	        return $x;
        }
        
        function displayEventType_Portal($type="")
        {
	        global $image_path,$intranet_httppath,$i_EventShowAll,$i_EventCloseImage, $i_no_record_exists_msg, $LAYOUT_SKIN, $Lang;

	        //$rb_image_path = "$image_path/resourcesbooking";
	                
	        if ($type=="1")
	        {
	        // Monthly  
	          $lower_bound = mktime(0,0,0,$this->m,1,$this->y);
	          $upper_bound = mktime(0,0,0,$this->m+1,1,$this->y) - 1;       
	        } 
	        else {
	        //Daily 
	          $lower_bound = mktime(0,0,0,$this->m,$this->d,$this->y);
	          $upper_bound = mktime(0,0,0,$this->m,$this->d+1,$this->y) - 1;                  
	        }
	        
	        $event_list = $this->returnAllEvents_Portal($lower_bound,$upper_bound);
	          
	        $event_list2 = array();
	        for ($i=0;$i<count($event_list);$i++)
	        {
	          $eventdate = $event_list[$i][1];          
	          $event_list2[$eventdate][] = $event_list[$i]; 
	        }
	                        
	        if(sizeof($event_list2)==0)
	        {
	          $x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"> ";
	          $x .= "     
	              <tr>
	                <td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
	                <td width=\"90%\" class=\"indextabclassiconoff\" >$i_no_record_exists_msg</td>
	                <td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td>
	              </tr>
	              <tr>
	                <td colspan='3'>&nbsp;</td>
	              </tr>
	              ";      
	          $x .= "</table>";   
	        }
	        else
	        {
	          $x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\"> ";
	          foreach($event_list2 as $DateID => $DateArr)          
	          {
		        $weekday = $Lang['General']['DayType4'][(date("w",strtotime($DateID)))];
		        $x .= "
	                <tr id=\"Event_".$DateID."\">
	                  <td width=\"10\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/event/icon_arrow.gif\" ></td>
	                  <td class=\"indexeventlist\">{$DateID} ({$weekday})</td>
	                </tr>               
	                ";
	            $x .= "
	                <tr>
	                  <td>&nbsp;</td>
	                  <td>
	                  <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\">
	                ";

	            for ($i=0;$i<count($DateArr);$i++)
	            {
		            switch($DateArr[$i][3])
		            {
			         	case 0: $this_css = "indexeventlink2"; break;   # school event  - green
			         	case 1: $this_css = "indexeventlink3"; break;   # academic event -  blue 
			         	case 2: $this_css = "indexeventlink1"; break;   # group event - orange
			         	case 3: $this_css = "indexeventlink4"; break;   # public holiday - red 
			         	case 4: $this_css = "indexeventlink5"; break;   # school holiday- purple 
			         	default:$this_css = "indexeventlink2"; break;   
		            }
		            $x .= "
	                    <tr>
	                      <td width=\"5\" valign=\"top\" ><a class=\"". $this_css ."\">-</a></td>
	                      <td><a href=\"javascript:void(0)\" onclick=\"fe_view_event(".$DateArr[$i][0].", 1)\" class=\"". $this_css ."\"> ".$DateArr[$i][2]." </a></td>
	                    </tr>
	                    ";      
	            }   
	            
	            $x .= "
	                  </table>
	                  </td>
	                </tr>
	                ";
	                                
	            
	          }
	          $x .= "</table>";
	        }
	        
	        
	        return $x;
        }
                
         function displayEventByDate($ts)
         {
              global $i_EventTypeSchool, $i_EventTypeAcademic, $i_EventTypeHoliday, $i_EventTypeGroup;
              $row = $this->returnEventByDate($ts);
              $x .= "<table width=400 border=0 cellpadding=2 cellspacing=1>\n";
              if(sizeof($row)==0){
                   global $i_no_record_exists_msg;
                   $x .= "<tr><td class='indextabclassiconoff'>$i_no_record_exists_msg</td></tr>\n";
              }else{
                   for($i=0; $i<sizeof($row); $i++) {
                        $EventID = $row[$i][0];
                        $EventDate = $row[$i][1];
                        $EventType = $row[$i][2];
                        //$EventTitle = $row[$i][3];
                        $EventTitle = intranet_wordwrap($row[$i][3],30,"\n",1);
                        switch($EventType){
                             case 0: $i_EventType = $i_EventTypeSchool; break;
                             case 1: $i_EventType = $i_EventTypeAcademic; break;
                             case 2: $i_EventType = $i_EventTypeHoliday; break;
                             case 3: $i_EventType = $i_EventTypeGroup; break;
                        }
                        $x .= "<tr>\n";
                        $x .= "<td width=30%>$i_EventType</td>\n";
                        $x .= "<td width=70%><a href=javascript:fe_view_event($EventID)>$EventTitle</a></td>\n";
                        $x .= "</tr>\n";
                        $x .= "<tr><td colspan=2 align=center><img src=/images/line_520.gif width=425></td></tr>\n";
                   }
              }
              $x .= "</table>\n";
              return $x;
         }
         
    
    ####################################################################
    ## eCommunity Calendar
    ####################################################################
    /*
       function displayeCommCalendar($CalID)
    {
      global $image_path,$intranet_httppath,$i_EventShowAll,$i_EventCloseImage;
      //$rb_image_path = "$image_path/resourcesbooking";
      
      $lower_bound = mktime(0,0,0,$this->m,1,$this->y);
      $upper_bound = mktime(0,0,0,$this->m+2,1,$this->y) - 1;
      
      
      $event_list = $this->returnAllEvents($lower_bound,$upper_bound);
      
      $x .= "<script language='javascript' >\n";
      $x .= "var table_head = '<table width=\'260\' border=\'1\' bordercolorlight=\'#FBFDEA\' bordercolordark=\'#B3B692\' cellpadding=\'2\' cellspacing=\'0\' bgcolor=\'#FBFDEA\' >';";
      $x .= "table_head += '<tr><td align=\'center\' ><table width=\'250\' border=\'0\' cellpadding=\'3\' cellspacing=\'0\' class=\'body\' >';";
      $x .= "table_head += '<tr><td>&nbsp;</td><td align=\'right\'><a href=\'javascript:hideLayer(\"ToolMenu\")\' >$i_EventCloseImage</a></td></tr>';";
      $x .= "table_head += '<tr><td colspan=\'2\' align=\'center\' ><img src=\'$image_path/index/el_2lines.gif\' width=\'223\' height=\'8\' ></td></tr>';";
      
      $x .= "var table_tail = '<tr align=center><td colspan=\'2\' valign=top><img src=$image_path/index/el_1lines.gif width=223 height=8></td></tr><tr><td>&nbsp;</td>';";
      $x .= "table_tail += '<td align=right><a href=javascript:showAllEventList()>$i_EventShowAll</a></td></tr>';";
      $x .= "table_tail += '<tr align=center><td colspan=\'2\' valign=top><img src=$image_path/index/el_1lines.gif width=223 height=8></td></tr><tr><td>&nbsp;</td><td align=right><a href=javascript:hideLayer(\"ToolMenu\")>$i_EventCloseImage</a></td></tr></table>';";

      
      $x .= "var event_list_text = '';\n";
      for ($i=0; $i<sizeof($event_list); $i++)
      {
        list($eventID, $eventDate,$eventTitle,$eventType) = $event_list[$i];
        $eventTitle = addslashes($eventTitle);
        $x .= "event_list_text += '<tr><td width=90>$eventDate</td><td width=160><a href=javascript:fe_view_event($eventID)>$eventTitle</a></td></tr>';\n";
      }
      $x .= "var event_layer_text = table_head+event_list_text+table_tail;\r\n";
      $x .= "function showEventList()
      {
        writeToLayer('ToolMenu',event_layer_text)
        showLayer('ToolMenu');
      }
      
      function showAllEventList()
      {
             newWindow('$intranet_httppath/home/allevent.php', 6);
      }
      
      \n";
      
      
      $x .= "</script>\n";
      
      $x .= $this->displayeCommCalendarArray($this->m, $this->y);
      
      return $x;
    }
    */
    
       function displayeCommCalendar($month, $year, $CalID)
       {
            global $i_frontpage_day,$i_EventList,$image_path, $intranet_session_language;
            
            $row = $this->returnCalendarArray($month, $year);
  
            $x .= "
            <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"indexcalendartable\">
            <tr>
              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweeks\" >&nbsp;".$i_frontpage_day[0]."</td>
              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[1]."</td>
              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[2]."</td>
              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[3]."</td>
              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[4]."</td>
              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[5]."</td>
              <td align=\"center\" class=\"indexcalendartabletd indexcalendarweek\" >&nbsp;".$i_frontpage_day[6]."</td>           
                 ";
                 $r = 0;
            for($i=0; $i<sizeof($row); $i++){
                 $x .= ($i%7==0) ? "</tr>\n<tr>\n" : "";
                 if($i%7==0)  $r++;
                 $x .= $this->displayeCommCalendarEvent($row[$i]);
            }
            
            for($j=$r;$j<6;$j++)
            {
              $x .="</tr>\n<tr>";
              for($i=0;$i<7;$i++)
              $x .= "<td class=\"indexcalendartabletd\" height=\"25\">&nbsp;</td>";    
            }
            
            $x .= "</tr></table>\n";
            
            return $x;
        }
        
        function displayeCommCalendarEvent($ts)
         {
           global $GroupID;
           
           list($usec, $sec) = explode(" ", microtime());   
           
              if($ts=="")
              {
                   $x = "<td class=\"indexcalendartabletd\" ><br></td>";
              }
              else
              {
            $Event = ($this->IsHolidayEvent($ts)) ? "<font color='red'>".date("j",$ts)."</font>" : date("j",$ts);
            
            
            $link = "<a href=\"javascript:void(0)\" onclick=\"javascript:view_event($ts)\">";
            
                      $Event = $this->IseCommCalEvent($GroupID, $ts) ? "<div id=\"ecomm_calendar_day_event\">{$link}$Event</a></div>" : $Event;
                      
            $Event = (date("Y-m-d", $sec) == date("Y-m-d", $ts)) ? "<div id=\"ecomm_calendar_today\">$Event</div>" : $Event;
            $x  = "<td class=\"indexcalendartabletd\" >";
                  $x .= "
                <table width=\"25\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" >
                <tr>
                  <td height=\"25\" align=\"left\" valign=\"top\"><div id=\"ecomm_calendar_day\">$Event</div></td>
                </tr>
                </table>                        
            
                          ";                            

            $x .= "</td>\n";
              }
              return $x;
         }
         
      function IseCommCalEvent($GroupID, $ts)
      {
        
        $sql = "select CalID from INTRANET_GROUP where GroupID=$GroupID";
        $row = $this->returnVector($sql);
        $CalID = $row[0];
        
        $sql = "select EventID from CALENDAR_EVENT_ENTRY where CalID=$CalID and left(EventDate,10)='". date("Y-m-d", $ts) ."'";
        $row = $this->returnVector($sql);
        $EventID = $row[0];
        
        return $EventID=="" ? "0" : "1";
      }
        
      ### Below is for IP2.5 ###
        function displayCalandar_CyclePeriodPreview($month, $year, $ShowArchiveRecord, $order="", $SpecialTimetableSettingsArr='')   ### School Calendar - Cycle day Setting 
      {
        global $i_DayType4, $i_EventList, $image_path, $intranet_session_language;
        global $eEnrollment, $LAYOUT_SKIN, $Lang;
        
        $row = $this->returnCalendarArray($month, $year);
        $a = new libcycleperiods();
        $cycles_array = $a->getCycleInfoByYearMonth($year,$month);
//        $cycles_array = $a->getPreviewCycleInfo($year,$month);
        
        $x = "";
        $x .= "<div id=\"CalendarDiv\">\n";
        # Month and Day Title
        $x .= "
            <table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\" bgcolor=\"#D2D2D2\">\n
              <tr><td colspan=\"7\" align=\"center\" class=\"tabletop\">".$Lang['General']['month'][$month]." ".$year."</td></tr>\n
              <tr>\n
                <td align=\"center\" class=\"icalendar_weektitle \" ><font color='red'>&nbsp;".$Lang['General']['DayType4'][0]."</font></td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][1]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][2]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][3]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][4]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][5]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][6]."</td>\n
              </tr>\n
        ";
        
        # Show Dates
        for($i=0; $i<sizeof($row); $i++)
        {
          $x .= ($i % 7 == 0) ? "<tr>\n" : "";
          
          if(!empty($row[$i]))
            $date_string = date("Y-m-d",$row[$i]);
            
          $cycle = $cycles_array[$date_string];
          $display = $cycle[2];
          $x .= $this->Display_Calendar_Event($row[$i],$display,1,$ShowArchiveRecord,"",$ParIsColorPrint=false, $SpecialTimetableSettingsArr);
          $x .= ($i % 7 == 6) ? "</tr>\n" : "";
        }
        $x .= "</table>\n";
        $x .= "</div>\n";
        
        return $x;
      }
      
      function displayCalandar_CyclePeriodProductionView($month, $year, $ShowArchiveRecord, $order="", $SpecialTimetableSettingsArr='', $ClassGroupID='', $viewOnly=false)  ### School Calendar - Event/Holiday Setting
      {
      	include_once('libcycleperiods.php');
      	include_once("libcycleperiods_ui.php");
        global $i_DayType4, $i_EventList, $image_path, $intranet_session_language;
        global $eEnrollment, $LAYOUT_SKIN, $Lang;

        $tableYearMonth = "ymTable".$year.substr('0'.$month,-2);
        $row = $this->returnCalendarArray($month, $year);
        $a = new libcycleperiods();
        //$cycles_array = $a->getCycleInfoByYearMonth($year,$month);
        $cycles_array = $a->getProductionCycleInfo($year,$month);
        
        $start_timestamp = strtotime(sprintf("%4d-%02d-01 00:00:00",$year,$month));
        $end_timestamp = strtotime(sprintf("%4d-%02d-%02d 00:00:00",$year,$month,date("t",$start_timestamp)));
        $this->initAllEventRecordByTimestampRange($start_timestamp,$end_timestamp,$ClassGroupID); // batch get events info in one month
        
        $x = "";
        $x .= "<div id=\"CalendarDiv\">\n";
        # Month and Day Title
        $x .= "
            <table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\" bgcolor=\"#D2D2D2\" id={$tableYearMonth}>\n
              <tr><td colspan=\"7\" align=\"center\" class=\"tabletop\">".$Lang['General']['month'][$month]." ".$year."</td></tr>\n
              <tr>\n
                <td align=\"center\" class=\"icalendar_weektitle \" ><font color='red'>&nbsp;".$Lang['General']['DayType4'][0]."</font></td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][1]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][2]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][3]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][4]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][5]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][6]."</td>\n
              </tr>\n
        ";
		# Show Dates
		for($i=0; $i<sizeof($row); $i++)
		{
			$x .= ($i % 7 == 0) ? "<tr>\n" : "";
			if(!empty($row[$i]))
				$date_string = date("Y-m-d",$row[$i]);
			$cycle = $cycles_array[$date_string];
			$display = $cycle[2];
			/***/
			$x .= $this->Display_Calendar_Event($row[$i],$display,0,$ShowArchiveRecord, $ParPrintingVersion=false, $ParIsColorPrint=false, $SpecialTimetableSettingsArr, $ClassGroupID, $viewOnly);
			$x .= ($i % 7 == 6) ? "</tr>\n" : "";
		}
        $x .= "</table>\n";
        $x .= "</div>\n";
        
        return $x;
      }
      
      function displayCalandar_PeriodView($month, $year, $order="") ## Period Setting 
      {
        global $i_DayType4, $i_EventList, $image_path, $intranet_session_language;
        global $eEnrollment, $LAYOUT_SKIN, $Lang;
        
        $row = $this->returnCalendarArray($month, $year);
        $a = new libcycleperiods();
        //$cycles_array = $a->getCycleInfoByYearMonth($year,$month);
        $cycles_array = $a->getProductionCycleInfo($year,$month);
        
        $x = "";
        $x .= "<div id=\"CalendarDiv\">\n";
        # Month and Day Title
        $x .= "
            <table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\" bgcolor=\"#D2D2D2\">\n
              <tr><td colspan=\"7\" align=\"center\" class=\"tabletop\">".$Lang['General']['month'][$month]." ".$year."</td></tr>\n
              <tr>\n
                <td align=\"center\" class=\"icalendar_weektitle \" ><font color='red'>&nbsp;".$Lang['General']['DayType4'][0]."</font></td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][1]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][2]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][3]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][4]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][5]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][6]."</td>\n
              </tr>\n
        ";
        
        # Show Dates
        for($i=0; $i<sizeof($row); $i++)
        {
          $x .= ($i % 7 == 0) ? "<tr>\n" : "";
          
          if(!empty($row[$i]))
            $date_string = date("Y-m-d",$row[$i]);
          
          $cycle = $cycles_array[$date_string];
          $display = $cycle[2];
          
          $x .= $this->Display_Calendar_Period($row[$i],$display);
          $x .= ($i % 7 == 6) ? "</tr>\n" : "";
        }
        $x .= "</table>\n";
        $x .= "</div>\n";
        
        return $x;
      }
    
      	# $ts means "timestamp"
      	## used in holiday / event
      	function Display_Calendar_Event($ts, $cycle, $edit_mode, $ShowArchiveRecord, $ParPrintingVersion=false, $ParIsColorPrint=false, $SpecialTimetableSettingsArr='',$ClassGroupID='', $viewOnly=false)
      	{
			global $image_path,$LAYOUT_SKIN,$Lang;
        	
			//$this->initAllEventRecordByTimestamp($ts, $ClassGroupID);

			if ($ts == '') {
				$thisDateString = '';
			}
			else {
				$thisDateString = date("Y-m-d",$ts);
			}

			$cycle = ($cycle)? $cycle : "&nbsp;";

			if($ts=="") {
  				$x = "<td class=\"non_bookable\" ><br></td>";
			}
			else {
  				# Temp condition: not Holiday => bookable
  				if ($this->IsHolidayEvent($ts,1)) {
  					if($edit_mode!=1) {
    					$Event = "<font class='public_holiday'>".date("j",$ts)."</font>";
  					}
					else {
						$Event = date("j",$ts);
					}
					
					$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
					if(date("w",$ts)==0) {  ## is Sunday?
  						$ClassType = "non_bookable sunday_holiday";
					}
					else {
  						$ClassType = "bookable";
					}

					if($this->IsAcademicEvent($ts,1)) {
  						$css_academic_event = "academic_event";
					}
					if($this->IsSchoolEvent($ts,1)) {
  						$css_school_event = "school_event";
					}
					if($this->IsGroupEvent($ts,1)){
						$css_group_event = "group_event";
						$EventImage = "<img src=\"".$this->image_path2."/index/calendar/icon_groupevent.gif\"  border=\"0\">";
					}
  				}
  				else if($this->IsSchoolHolidayEvent($ts,1)) {
  					if($edit_mode!=1) {
    					$Event = "<font class='school_holiday'>".date("j",$ts)."</font>";
  					}
					else {
						$Event = date("j",$ts);	
					}
	
					$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
					if(date("w",$ts)==0) {  ## is Sunday?
  						$ClassType = "non_bookable";
					}
					else {
  						$ClassType = "bookable";
					}
  
					if($this->IsAcademicEvent($ts,1)) {
						//$bg_color_academic_event = "#D0E5FF";
						$css_academic_event = "academic_event";
					}
					if($this->IsSchoolEvent($ts,1)) {
  						//$bg_color_school_event = "#D2F384";
  						$css_school_event = "school_event";
					}
					if($this->IsGroupEvent($ts,1)){
						$css_group_event = "group_event";
						$EventImage = "<img src=\"".$this->image_path2."/index/calendar/icon_groupevent.gif\"  border=\"0\">";
    				}
  				}
  				else if($this->IsAcademicEvent($ts,1)) {
    				$Event = "<font color='#000000'>".date("j",$ts)."</font>";
					$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
					$ClassType = "bookable";

					//$bg_color_academic_event = "#D0E5FF";
					$css_academic_event = "academic_event";
					if($this->IsSchoolEvent($ts,1)) {
  						//$bg_color_school_event = "#D2F384";
  						$css_school_event = "school_event";
					}
					if($this->IsGroupEvent($ts,1)){
						$css_group_event = "group_event";
						$EventImage = "<img src=\"".$this->image_path2."/index/calendar/icon_groupevent.gif\"  border=\"0\">";
    				}
  				}
  				else if($this->IsSchoolEvent($ts,1)) {
    				$Event = "<font color='#000000'>".date("j",$ts)."</font>";
					$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
					$ClassType = "bookable";

					$css_school_event = "school_event";
					if($this->IsAcademicEvent($ts,1)) {
  						$css_academic_event = "academic_event";
					}
					if($this->IsGroupEvent($ts,1)){
						$css_group_event = "group_event";
						$EventImage = "<img src=\"".$this->image_path2."/index/calendar/icon_groupevent.gif\"  border=\"0\">";
    				}
  				}
  				else if($this->IsGroupEvent($ts,1)) {
  					$css_group_event = "group_event";
					$Event = date("j",$ts);
					$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
					$ClassType = "bookable";
					$EventImage = "<img src=\"".$this->image_path2."/index/calendar/icon_groupevent.gif\"  border=\"0\">";
  				}
  				else {
    				$Event = date("j",$ts);
					$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
					$ClassType = "bookable";
					$EventImage = "<img src=\"".$this->image_path2."/10x10.gif\" width=\"3\" height=\"7\" border=\"0\" />";
  				}
  
				# Printing in B/W mode no need to have img
				if ($ParPrintingVersion && !$ParIsColorPrint) {
					$EventImage = "";
				}
  				if ($edit_mode) {     ## ($edit_mode == 1)?
					### School Calender - TimeZone Edit/Add ###
					$sql = "SELECT PeriodID, PeriodStart, PeriodEnd, ColorCode, RecordStatus FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE '$thisDateString' BETWEEN PeriodStart AND PeriodEnd";
					$result = $this->returnArray($sql,4);
// 					debug_pr($result);

					if(sizeof($result)>0) {
  						list($period_id, $period_start, $period_end, $bg_color_code, $published) = $result[0];
  
  						$bg_color = (!$published ? "yellow" : "$bg_color_code" );
  						$thisCellBorder = '';
  						
  						// Apply Special Timetable background color if there are any
  						if ($SpecialTimetableSettingsArr && $SpecialTimetableSettingsArr[$thisDateString]['BgColor'] != '') {
							$bg_color = strtoupper($SpecialTimetableSettingsArr[$thisDateString]['BgColor']);
							//$thisCellBorder = 'border:1px solid red;';
  						}
    
  						if($period_start == $thisDateString) {
    						if(strtoupper($bg_color) != "#FFFFFF") {
	  							$font_color = "#FFFFFF";
							}

							## within a period & show edit icon ($thisDateString == StartPeriod)
							if ($ShowArchiveRecord) {
								$link = "";
								$editable_class = "";
								$EditIcon = $EventImage;
							}
							else {
								//$link = "<a href='#TB_inline?height=400&width=600&inlineId=FakeLayer' title='".$Lang['SysMgr']['CycleDay']['EditThisTimeZone']."' class='thickbox add_dim' onClick='editPeriodRange(".$period_id.");'>";
								$linkID = 'TimezoneEditLink_'.$thisDateString;
								$link = '<a id="'.$linkID.'" href="javascript:void(0);" title="'.$Lang['SysMgr']['CycleDay']['EditThisTimeZone'].'" onclick="js_Show_Option_Layer(\''.$thisDateString.'\', \''.$period_id.'\', \''.$linkID.'\', \'EditOptionDiv\');">';
								$editable_class = "editable";
	
								//$EditIcon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit.gif" style="border:0;" >';
								if ($SpecialTimetableSettingsArr && $SpecialTimetableSettingsArr[$thisDateString]['FirstDateInSpecialSettings'] == 1) {
// 								    debug_pr($SpecialTimetableSettingsArr);
									// Both First Day of Timezone and Special Timetable
									$EditIcon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_both_special.gif" style="border:0;" >';
								}
								else {
									// First Day of Timezone only
									$EditIcon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit.gif" style="border:0;" >';
								}	
							}
	
							# Printing mode does not need links
							$closeLink = "</a></td>\n";
							if ($ParPrintingVersion) {
								$link = "";
								$closeLink = "</td>\n";
							}
							
							$x  = "<td class=\"$ClassType $editable_class\" width=\"14%\" style=\"background-color:$bg_color; $thisCellBorder\">$link";
	  						$x .= "
									<table id=\"Day_Table_$thisDateString\" width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"$ClassType\" style=\"background-color:$bg_color;\" >
	  									<tr>
	    									<td class='$editable_class' align=\"center\" width=\"50%\" style=\"background-color:$bg_color\" ><div class=\"booking_calendar_date\">$Event</div></td>
	    									<td class='$editable_class' align=\"center\" style=\"background-color:$bg_color\" align=\"right\" vlaign=\"top\">".$EditIcon."</td>
	  									</tr> 
	  									<tr>
	    									<td class=\"indexcalendarremark $editable_class\" align=\"left\" valign=\"top\" style=\"background-color:$bg_color\" ><font color='$font_color'>$cycle</font></td>
	    									<td class=\"indexcalendarremark $editable_class\" align=\"left\" valign=\"top\" style=\"background-color:$bg_color\" ><font color='$font_color'>&nbsp;</font></td>
	  									</tr>
									</table>";
							//$x .= "</a></td>\n";
	    					$x .= $closeLink;
	  					}
	  					else {
	    					if(strtoupper($bg_color) != "#FFFFFF") {
	  							$font_color = "#FFFFFF";
	    					}
	    					
	    					// First Date of Special Timetable Settings => Show Icon
	    					$link = '';
	    					$EditIcon = '';
	    					if ($SpecialTimetableSettingsArr && $SpecialTimetableSettingsArr[$thisDateString]['FirstDateInSpecialSettings'] == 1) {
	    						$linkID = 'TimezoneEditLink_'.$thisDateString;
	    						$link = '<a id="'.$linkID.'" href="javascript:void(0);" title="'.$Lang['SysMgr']['CycleDay']['EditThisTimeZone'].'" onclick="js_Show_Option_Layer(\''.$thisDateString.'\', \''.$period_id.'\', \''.$linkID.'\', \'EditOptionDiv\');">';
								$EditIcon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_special.gif" style="border:0;" >';
	    					}
	    					
	    					# Printing mode does not need links
							$closeLink = "</a></td>\n";
							if ($ParPrintingVersion) {
								$link = "";
								$closeLink = "</td>\n";
							}
	  
							## within a period but do not show edit icon ($thisDateString != StartPeriod)
							$x  = "<td class=\"$ClassType\" width=\"14%\" style=\"background-color:$bg_color; $thisCellBorder\">$link";
	  						$x .= "
									<table id=\"Day_Table_$thisDateString\" width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"$ClassType\" style=\"background-color:$bg_color\" >
	  									<tr>
	    									<td align=\"center\" width=\"50%\" style=\"background-color:$bg_color\" ><div class=\"booking_calendar_date\">$Event</div></td>
	    									<td align=\"center\" style=\"background-color:$bg_color\" align=\"right\" valign=\"top\">$EditIcon</td>
	  									</tr> 
	  									<tr>
	    									<td class=\"indexcalendarremark\" align=\"left\" valign=\"top\" colspan=\"2\" style=\"background-color:$bg_color\" ><font color='$font_color'>$cycle</font></td>
	  									</tr>
									</table>";
							//$x .= "</td>\n";
							$x .= $closeLink;
						}
					}
					else {
	  					if ($ShowArchiveRecord){
							$link = "";
							$editable_class = "";
						}
						else {
							$link = "<a href=\"#TB_inline?height=400&width=600&inlineId=FakeLayer\" class=\"thickbox add_dim\" title='".$Lang['SysMgr']['CycleDay']['NewTimeZoneHere']."' onClick=\"newDefaultPeriodForm2('$thisDateString')\">";
							$editable_class = "editable";
						}
	
						# Printing mode does not need links
						$closeLink = "</a></td>\n";
						if ($ParPrintingVersion) {
							$link = "";
							$closeLink = "</td>\n";
	    				}
	  					
	  					## no cycle period set
	  					$x  = "<td class=\"$ClassType $editable_class\" width=\"14%\" >$link";
						$x .= "
	  							<table id=\"Day_Table_$thisDateString\" width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"$ClassType\" >
	    							<tr>
	      								<td class='$editable_class' align=\"center\" width=\"50%\"><div class=\"booking_calendar_date\">$Event</div></td>
	      								<td class='$editable_class' align=\"right\" valign=\"top\"></td>
	    							</tr>
	    							<tr>
	      								<td class=\"indexcalendarremark $editable_class\" align=\"left\" valign=\"top\" >$cycle</td>
	      								<td class=\"indexcalendarremark $editable_class\" align=\"left\" valign=\"top\" >&nbsp;</td>
	    							</tr>
	  							</table>";
						//$x .= "</a></td>\n";
	        			$x .= $closeLink;
	    			}
	  			}
	  			else {
	    			### School Calender - Holiday & Event Edit/Add ###
					if ($css_academic_event != "" && $css_school_event != "") {
	  					// continuous
					}
					else if ($css_academic_event != "" && $css_school_event == "") {
	  					$css_school_event = $css_academic_event;
					}
					else if ($css_academic_event == "" && $css_school_event != "") {
	  					$css_academic_event = $css_school_event;
					}
					else {
	  					$css_academic_event = "";
	  					$css_school_event = "";
					}
	
					if ($ShowArchiveRecord || $viewOnly) {
						$link = "";
						$editable_class = "";
					}
					else {
						$link = "<a href=\"#TB_inline?height=470&width=600&inlineId=FakeLayer\" class=\"thickbox add_dim\" title='".$Lang['SysMgr']['SchoolCalendar']['NewHolidaysOrEvents']."' onClick=\"newHolidayEventForm('$thisDateString'); return false;\">";
						$editable_class = "editable";
					}
					
					# Printing mode does not need links
					$closeLink = "</a></td>\n";
					if ($ParPrintingVersion) {
						$link = "";
						$closeLink = "</td>\n";
	    			}
					
					$x  = "<td class=\"$ClassType $editable_class\" width=\"14%\" >$link";
					$x .= "	<table id=\"Day_Table_$thisDateString\" width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"$ClassType\" >
	  							<tr>
	    							<td class=\"$editable_class $css_academic_event $css_group_event\" align=\"center\" width=\"50%\" ><div class=\"booking_calendar_date\">$Event</div></td>
	    							<td class=\"$editable_class $css_school_event\" align=\"right\" valign=\"top\">{$EventImage}</td>
	  							</tr>
	  							<tr>
	    							<td class=\"indexcalendarremark $editable_class $css_academic_event\" align=\"left\" valign=\"top\" width=\"50%\" >$cycle</td>
	    							<td class=\"indexcalendarremark $editable_class $css_school_event\" align=\"left\" valign=\"top\" >&nbsp;</td>
	  							</tr>
							</table>";
					//$x .= "</a></td>\n";
	            	$x .= $closeLink;
	      		}
    		}
    		return $x;
		}
      
      # $ts means "timestamp"
      function Display_Calendar_Period($ts, $cycle)
      {
        global $image_path,$LAYOUT_SKIN,$Lang;
        
        if($ts == '')
          $thisDateString = '';
        else
          $thisDateString = date("Y-m-d",$ts);
        $cycle = ($cycle)? $cycle : "&nbsp;";
        
        if($ts=="")
        {
          $x = "<td class=\"non_bookable\" ><br></td>";
        }
        else
        {
          # Temp condition: not Holiday => bookable
          if ($this->IsHolidayEvent($ts))
          {
            $Event = "<font color='red'>".date("j",$ts)."</font>";
            ### Old method - chagen to hyperlink when mouseover the day
            //$Event = ($this->IsEvent($ts)) ? "<a href='javascript:fe_view_event_by_date($ts)' class='indexcalendarholidaylink' >".$Event."</a>" : $Event;
            ### New method - nothing will chagned
            $Event = ($this->IsEvent($ts)) ? $Event : $Event;
            if(date("w",$ts)==0)  ## is Sunday?
              $ClassType = "non_bookable";
            else
              $ClassType = "bookable";
          }
          else
          {
            $Event = date("j",$ts);
            ### Old method - chagen to hyperlink when mouseover the day
            //$Event = ($this->IsEvent($ts)) ? "<a href='javascript:fe_view_event_by_date($ts)' class='indexcalendardaylink' >".$Event."</a>" : $Event;
            ### New method - nothing will chagned
            $Event = ($this->IsEvent($ts)) ? $Event : $Event;
            $ClassType = "bookable";
          }
          
          ### Period - Add / Edit ###
          //$sql = "SELECT PeriodID, PeriodStart, PeriodEnd, ColorCode, RecordStatus FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE '$thisDateString' BETWEEN PeriodStart AND PeriodEnd";
          $sql = "SELECT DateRangeID, StartDate, EndDate FROM INTRANET_PERIOD_DATERANGE WHERE '$thisDateString' BETWEEN StartDate AND EndDate";
          $result = $this->returnArray($sql,3);
          
          if(sizeof($result)>0){
            ## have cycle period
            
            list($period_id, $period_start, $period_end) = $result[0];
            
            //$bg_color = (!$published ? "yellow" : "$bg_color_code" );
            
            if($period_start == $thisDateString)
            {
              ## within a period & show edit icon ($thisDateString == StartPeriod)
              $x  = "<td class=\"$ClassType\" width=\"14%\" style=\"background-color:$bg_color\">";
                $x .= "
                  <table id=\"Day_Table_$thisDateString\" width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"$ClassType\" style=\"background-color:$bg_color;\" >
                    <tr>
                      <td align=\"center\" width=\"50%\" style=\"background-color:$bg_color\" ><div class=\"booking_calendar_date\">$Event</div></td>
                      <td align=\"center\" style=\"background-color:$bg_color\" ><a href='#TB_inline?height=400&width=600&inlineId=FakeLayer' title='".$Lang['SysMgr']['CycleDay']['EditThisTimeZone']."' class='thickbox add_dim' onClick='editPeriodRange(".$period_id.");'><img src=\"$image_path/".$LAYOUT_SKIN."/icon_edit.gif\" style='border:0'></a></td>
                    </tr> 
                    <tr>
                      <td class=\"indexcalendarremark\" align=\"left\" valign=\"top\" colspan=\"2\" style=\"background-color:$bg_color\" >$cycle</td>
                    </tr>
                  </table>";
              $x .= "</td>\n";
            }else{
              ## within a period but do not show edit icon ($thisDateString != StartPeriod)
              $x  = "<td class=\"$ClassType\" width=\"14%\" style=\"background-color:$bg_color\">";
                $x .= "
                  <table id=\"Day_Table_$thisDateString\" width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"$ClassType\" style=\"background-color:$bg_color\" >
                    <tr>
                      <td align=\"center\" width=\"50%\" style=\"background-color:$bg_color\" ><div class=\"booking_calendar_date\">$Event</div></td>
                      <td align=\"center\" style=\"background-color:$bg_color\" ></td>
                    </tr> 
                    <tr>
                      <td class=\"indexcalendarremark\" align=\"left\" valign=\"top\" colspan=\"2\" style=\"background-color:$bg_color\" >$cycle</td>
                    </tr>
                  </table>";
              $x .= "</td>\n";
            }
          }else{
            ## no cycle period set
            $x  = "<td class=\"$ClassType\" width=\"14%\" ><a href=\"#TB_inline?height=500&width=700&inlineId=FakeLayer\" class=\"thickbox add_dim\" title='".$Lang['SysMgr']['Periods']['FieldTitle']['PeriodSetting']."' onClick=\"loadPeriodsForm('$thisDateString')\">";
              $x .= "
                <table id=\"Day_Table_$thisDateString\" width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"$ClassType\" >
                  <tr>
                    <td align=\"center\" width=\"50%\"><div class=\"booking_calendar_date\">$Event</div></td>
                    <td align=\"center\"></td>
                  </tr>
                  <tr>
                    <td class=\"indexcalendarremark\" align=\"left\" valign=\"top\" colspan=\"2\">$cycle</td>
                  </tr>
                </table>";
            $x .= " </a></td>\n";
          }
        }
        return $x;
      }
      
      function Return_Previous_Month_Btn($id="")
      {
        if ($id=="")
        {
          $idHTML = "";
        }
        else
        {
          $idHTML = "id=\"$id\"";
        }
        $prevBtn = "<a href=\"javascript:void(0)\"><img $idHTML src=\"".$this->image_path2."/index/calendar/btn_prev_month.gif\" border=\"0\" /></a>";
      
        return $prevBtn;
      }
      
      function Return_Next_Month_Btn($id="")
      {
        if ($id=="")
        {
          $idHTML = "";
        }
        else
        {
          $idHTML = "id=\"$id\"";
        }
        $nextBtn = "<a href=\"javascript:void(0)\"><img $idHTML src=\"".$this->image_path2."/index/calendar/btn_next_month.gif\" border=\"0\" /></a>";
      
        return $nextBtn;
      }
      
      function displayCalandar_MonthlyEventTable($month, $year, $ShowArchiveRecord, $ClassGroupID='', $viewOnly=false)
      {
        global $image_path, $LAYOUT_SKIN, $Lang, $sys_custom;
        
        include_once("libinterface.php");
        $linterface = new interface_html();
        
        include_once("libcycleperiods.php");
        $lcycleperiods = new libcycleperiods();
    	
    	$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
    	if($isKIS && $ClassGroupID != '') { 
    		//if($_SESSION['UserType'] != USERTYPE_STAFF){
    			$classGroupCond = " AND (a.ClassGroupID IN (".(is_array($ClassGroupID)?implode(",",$ClassGroupID):$ClassGroupID).") OR a.ClassGroupID IS NULL OR a.ClassGroupID='') ";
    		//}else{
    		//	$classGroupCond = " AND a.ClassGroupID IN (".(is_array($ClassGroupID)?implode(",",$ClassGroupID):$ClassGroupID).") ";
    		//}
    	}
    	
    	$DefaultNumOfRowShow = 5;
    
        $x .= "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
        $x .= "<tr>";
        $x .= "<td><h1 class='event_list_month'><span> - ".$Lang['General']['month'][$month]." - </span></h1></td>";
        $x .= "</tr>";
        $x .= "<tr><td>";
        $x .= "<table class='common_table_list'>";
        ## Start Here 
        $StartDateOfMonth = date('Y-m-d',mktime(0,0,0,$month,1,$year));
        
        $LastDayOfMonth = date('t',mktime(0,0,0,$month,1,$year));
        $EndDateOfMonth = date('Y-m-d',mktime(0,0,0,$month,$LastDayOfMonth,$year));
        $Title = Get_Lang_Selection('a.Title','a.TitleEng');
        
        $sql = "SELECT 
	                a.EventID, 
                    If (a.TitleEng IS NULL Or a.TitleEng = '',
                    a.Title, 
                    $Title) 
                    as Title,                                      
                    a.EventDate As StartDate, c.EndDate AS EndDate, a.IsSkipCycle, a.RecordType
	              FROM 
	                INTRANET_EVENT AS a INNER JOIN (SELECT b.EventID, b.RelatedTo, MAX(b.EventDate) AS EndDate FROM INTRANET_EVENT AS b WHERE b.RelatedTo IS NOT NULL OR b.RelatedTo != '' GROUP BY b.RelatedTo) AS c ON (a.EventID = c.RelatedTo) 
	              WHERE
	                a.EventDate BETWEEN '$StartDateOfMonth' AND '$EndDateOfMonth' 
	              OR
	              	c.EndDate BETWEEN '$StartDateOfMonth' AND '$EndDateOfMonth'
	              	$classGroupCond
	              GROUP BY 
	                a.RelatedTo
	              ORDER By
	                a.EventDate, a.RecordType DESC";
        $arrResult = $this->returnArray($sql,5);
        $arrDate = array();
        if(sizeof($arrResult)>0) {      
          for($i=0; $i<sizeof($arrResult); $i++) {
            list($EventID, $EventTitle, $StartDate, $EndDate, $IsSkipCycle, $EventType) = $arrResult[$i];
           
            $StartDate = substr($StartDate,0,10);
            $EndDate = substr($EndDate,0,10);
            $NumOfDay = round( abs(strtotime($EndDate)-strtotime($StartDate)) / 86400, 0 );
            
            if($NumOfDay>0) {
				## Have Related Event
				$arrDate[] = $StartDate;
				$arrDateNoEvent[$StartDate]['NoEvent'] = 0;
				$arrDateEvent[$StartDate]['EventID'][] = $EventID;
				$arrDateEvent[$StartDate][$EventID]['AnyRelated'] = 1;
				$arrDateRelatedEvent[$StartDate][$EventID]['EndDate'][] = $EndDate;
				$arrDateRelatedEvent[$StartDate][$EventID]['EventTitle'][] = $EventTitle;
				$arrDateRelatedEvent[$StartDate][$EventID]['IsSkipCycle'][] = $IsSkipCycle;
				$arrDateRelatedEvent[$StartDate][$EventID]['EventType'][] = $EventType;
            } else {
				## Single Event
				$arrDate[] = $StartDate;
				$arrDateNoEvent[$StartDate]['NoEvent'] = 0;
				$arrDateEvent[$StartDate]['EventID'][] = $EventID;
				$arrDateEvent[$StartDate][$EventID]['AnyRelated'] = 0;
				$arrDateSingleEvent[$StartDate][$EventID]['EventTitle'][] = $EventTitle;
				$arrDateSingleEvent[$StartDate][$EventID]['IsSkipCycle'][] = $IsSkipCycle;
				$arrDateSingleEvent[$StartDate][$EventID]['EventType'][] = $EventType;
            }
          }
        }else{
          $arrDate[] = $StartDate;
          $arrDateNoEvent[$StartDate]['NoEvent'] = 1;
        }
        $arrDate = array_unique($arrDate);

        foreach($arrDate as $key=>$StartDate)
        {
          if($arrDateNoEvent[$StartDate]['NoEvent'] == 0)
          {
				$haveTitle = 0;
				for($i=0; $i<sizeof($arrDateEvent[$StartDate]['EventID']); $i++) {
					$NumOfRow++;
					$RowDisplay = '';
					$RowClass = '';
					if($NumOfRow > $DefaultNumOfRowShow){
						$RowDisplay = 'none';
						$RowClass = $month.'DetailEventRow';
					}
					
					$EventID = $arrDateEvent[$StartDate]['EventID'][$i];
                 
                 if($arrDateEvent[$StartDate][$EventID]['AnyRelated'] == 0) {
					$EventTitle = $arrDateSingleEvent[$StartDate][$EventID]['EventTitle'][0];
					$IsSkipCycle = $arrDateSingleEvent[$StartDate][$EventID]['IsSkipCycle'][0];
					$EventType = $arrDateSingleEvent[$StartDate][$EventID]['EventType'][0];
                   
					switch ($EventType){
						case 0: 
								$title_div_class = "title_school_event";
								$span_icon = "<span class='event_type_icon'>&nbsp;</span>";
								break;
                    	case 1: 
								$title_div_class = "title_academic";
								$span_icon = "<span class='event_type_icon'>&nbsp;</span>";
								break;
						case 2: 
								$title_div_class = "title_group_event";
								$span_icon = "<span class='event_type_icon'><img src=\"".$this->image_path2."/index/calendar/icon_groupevent.gif\"  align=\"right\"></span>";
								break;
						case 3: 
								$title_div_class = "title_public_holiday";
								$span_icon = "<span class='event_type_icon'>1</span>";
								break;
						case 4: 
								$title_div_class = "title_school_holiday";
								$span_icon = "<span class='event_type_icon'>1</span>";
								break;
						default:
								break;
                   	}
                   
                   if($IsSkipCycle)
                   	$iconSkipCycleDay = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_cycle_day_off.gif\" Title=\"".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['NotCountInCycleDay']."\">";
                   else
                    $iconSkipCycleDay = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_cycle_day_on.gif\" Title=\"".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CountInCycleDay']."\">";
					
                   if(!$ShowArchiveRecord && !$viewOnly)
                   {
                   		$thisDateHaveSpecialTimetable = 0;
                   		$thisTimezoneInfoArr = $lcycleperiods->returnPeriods_NEW('', 0, '', $StartDate, $StartDate);
                   		$thisTimezoneIDArr = Get_Array_By_Key($thisTimezoneInfoArr, 'PeriodID');
                   		$thisTimezoneDateRangeArr = $lcycleperiods->Get_Timezone_Date_Range($thisTimezoneIDArr);
                   		if ($IsSkipCycle && $lcycleperiods->Has_Special_Timetable_Settings_With_Cycle_Day($thisTimezoneDateRangeArr[0]['StartDate'], $thisTimezoneDateRangeArr[0]['EndDate'])) {
                   			$thisDateHaveSpecialTimetable = 1;
                   		}
                   	
                   		$x .= "<tr class='sub_row ".$RowClass."' id='EventLayer_$i' style='display:$RowDisplay'>
                   				<td width='20%' nowrap>".date("j",strtotime($StartDate))."</td>
                    	       	<td width='58%'><div class='event_list_title $title_div_class' style='width:100%;'>$span_icon<span>$EventTitle</span></div></td>
                           		<td width='22%'><div class='table_row_tool row_content_tool'>".
                           		$linterface->Get_Thickbox_Link(450, 600, "edit_dim", $Lang['SysMgr']['SchoolCalendar']['Event']['Edit'], "editHolidayEventForm('$EventID',0,'MonthlyEvent'); return false;")."".
                           		$linterface->GET_LNK_DELETE("#", $Lang['Btn']['Delete'], "deleteHolidayEvent('$EventID','$StartDate',0,'MonthlyEvent','$thisDateHaveSpecialTimetable'); return false;")
                           		."</div></td>
                         		</tr>";
                   }else{
                   		$x .= "<tr class='sub_row ".$RowClass."' id='EventLayer_$i' style='display:$RowDisplay'>
                   				<td width='20%' nowrap>".date("j",strtotime($StartDate))."</td>
								<td width='80%' colspan='2'><div class='event_list_title $title_div_class' style='width:100%;'>$span_icon<span>$EventTitle</span></div></td>
                         		</tr>";
                   }
                   $haveTitle = 1;
                 } else {
                   $EndDate = $arrDateRelatedEvent[$StartDate][$EventID]['EndDate'][0];
                   $EventTitle = $arrDateRelatedEvent[$StartDate][$EventID]['EventTitle'][0];
                   $IsSkipCycle = $arrDateRelatedEvent[$StartDate][$EventID]['IsSkipCycle'][0];
                   $EventType = $arrDateRelatedEvent[$StartDate][$EventID]['EventType'][0];
                   
                   switch ($EventType){
                    case 0: 
                                   $title_div_class = "title_school_event";
                                   $span_icon = "<span class='event_type_icon'>&nbsp;</span>";
                                   break;
                    case 1: 
                                   $title_div_class = "title_academic";
                                   $span_icon = "<span class='event_type_icon'>&nbsp;</span>";
                                   break;
                    case 2: 
                                   $title_div_class = "title_group_event";
                                   $span_icon = "<span class='event_type_icon'>1</span>";
                                   break;
                    case 3: 
                                   $title_div_class = "title_public_holiday";
                                   $span_icon = "<span class='event_type_icon'>1</span>";
                                   break;
                    case 4: 
                                   $title_div_class = "title_school_holiday";
                                   $span_icon = "<span class='event_type_icon'>1</span>";
                                   break;
                    default:
                                   break;
                   }
                   
                   if($IsSkipCycle)
                    $iconSkipCycleDay = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_cycle_day_off.gif\" Title=\"".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['NotCountInCycleDay']."\">";
                   else
                    $iconSkipCycleDay = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_cycle_day_on.gif\" Title=\"".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CountInCycleDay']."\">";
                  
                   if(!$ShowArchiveRecord && !$viewOnly)
                   {
                   		# Check every dates in this event to see if there are any special timetable settings
                   		$thisDateHaveSpecialTimetable = 0;
                   		$thisTimezoneInfoArr = $lcycleperiods->returnPeriods_NEW('', 0, '', $StartDate, $EndDate);
                   		$thisTimezoneIDArr = Get_Array_By_Key($thisTimezoneInfoArr, 'PeriodID');
                   		$thisTimezoneDateRangeArr = $lcycleperiods->Get_Timezone_Date_Range($thisTimezoneIDArr);
                   		if ($IsSkipCycle && $lcycleperiods->Has_Special_Timetable_Settings_With_Cycle_Day($thisTimezoneDateRangeArr[0]['StartDate'], $thisTimezoneDateRangeArr[0]['EndDate'])) {
                   			$thisDateHaveSpecialTimetable = 1;
                   		}
                   		
		                   $x .= "<tr class='sub_row ".$RowClass."' id='EventLayer_$i' style='display:$RowDisplay'>
		                   			<td width='20%' nowrap>".date("j",strtotime($StartDate))." ~ ".date("j M Y",strtotime($EndDate))."</td>
		                            <td width='58%'><div class='event_list_title $title_div_class' style='width:100%;'>$span_icon<span>$EventTitle</span></div></td>
		                            <td width='22%'><div class='table_row_tool row_content_tool'>".
									$linterface->Get_Thickbox_Link(450, 600, "edit_dim", $Lang['SysMgr']['SchoolCalendar']['Event']['Edit'], "editHolidayEventForm('$EventID',1,'MonthlyEvent'); return false;")."".
									//$linterface->GET_LNK_DELETE("#", $Lang['Btn']['Delete'], "deleteHolidayEvent('$EventID','$StartDate',0,'MonthlyEvent'); return false;")."".
									$linterface->GET_ACTION_LNK("#", $Lang['SysMgr']['SchoolCalendar']['ToolTip']['DeleteRelatedHolidaysEvents'], "deleteHolidayEvent('$EventID','$StartDate',1,'MonthlyEvent','$thisDateHaveSpecialTimetable'); return false;","delete_period_dim")
									."</div></td>
		                         </tr>";
		           }else{
		           			$x .= "<tr class='sub_row ".$RowClass."' id='EventLayer_$i' style='display:$RowDisplay'>
		           					<td width='20%' nowrap>".date("j",strtotime($StartDate))." ~ ".date("j M Y",strtotime($EndDate))."</td>
		                           	<td width='80%' colspan='2'><div class='event_list_title $title_div_class' style='width:100%;'>$span_icon<span>$EventTitle</span></div></td>
		                         </tr>";
		           }
                 }
               }
          }else{
               $x .= "<tr class='sub_row'><td align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
          }
        }
        if($NumOfRow > $DefaultNumOfRowShow && $ReadMoreShow!= 1){
                 	$TotalNumOfHiddenRow = $NumOfRow-$DefaultNumOfRowShow;
                 	
                 	$x .= "<tr id='MoreEventLayer".$month."' ><td colspan=3 align='right'><a href='#' onClick='jsShowMoreEvent(1,$month); return false;'>".$Lang['SysMgr']['SchoolCalendar']['ToolTip']['Expand']."</a></td></tr>";
                 	$x .= "<tr id='HideEventLayer".$month."' style='display: none'><td colspan=3 align='right'><a href='#' onClick='jsShowMoreEvent(0,$month); return false;'>".$Lang['SysMgr']['SchoolCalendar']['ToolTip']['Hide']."</a></td></tr>";
                 	$ReadMoreShow = 1;
        }
        $x .= "</table>";
        $x .= "</td></tr>";
        $x .= "</table>";
        
        return $x;
      }
      
      function displayBookingCalendar($month, $year, $FacilityType='', $FacilityID='', $DateTimeZone='', $AvailableBookingDate='')  ### eBooking Calendar - show booking period 
      {
        global $i_DayType4, $i_EventList, $image_path, $intranet_session_language;
        global $eEnrollment, $LAYOUT_SKIN, $Lang, $a;
        
        if(!$a)
        {
	        include_once('libcycleperiods.php');
	        $a = new libcycleperiods();
        }
        
        $row = $this->returnCalendarArray($month, $year);
        $cycles_array = $a->getProductionCycleInfo($year,$month);
        
        
        $x = "";
        $x .= "<div id=\"CalendarDiv\">\n";
        # Month and Day Title
        $x .= "
            <table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\" bgcolor=\"#D2D2D2\">\n
              <tr><td colspan=\"7\" align=\"center\" class=\"tabletop\">".$Lang['General']['month'][$month]." ".$year."</td></tr>\n
              <tr>\n
                <td align=\"center\" class=\"icalendar_weektitle \" ><font color='red'>&nbsp;".$Lang['General']['DayType4'][0]."</font></td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][1]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][2]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][3]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][4]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][5]."</td>\n
                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][6]."</td>\n
              </tr>\n
        ";
        
        # Show Dates
        for($i=0; $i<sizeof($row); $i++)
        {
          $x .= ($i % 7 == 0) ? "<tr>\n" : "";
          
          if(!empty($row[$i]))
            $date_string = date("Y-m-d",$row[$i]);
          
          $cycle = $cycles_array[$date_string];
          $display = $cycle[2];
          $SpecialTimeTableID = $cycle[3];
          $ColorCode = $DateTimeZone[$date_string]['ColorCode'];
          $TimeZoneID = $DateTimeZone[$date_string]['PeriodID'];
          
          //$x .= $this->Display_Calendar_Event($row[$i],$display,0,$ShowArchiveRecord);
          $x .= $this->DisplayCalendar_BookingPeriod($row[$i],$display,$FacilityType,$FacilityID,$ColorCode,$AvailableBookingDate);
          $x .= ($i % 7 == 6) ? "</tr>\n" : "";
        }
        $x .= "</table>\n";
        $x .= "</div>\n";
        
        return $x;
      }
      
      	function DisplayCalendar_BookingPeriod($ts, $cycle, $FacilityType='', $FacilityID='', $ColorCode='', $AvailableBookingDate='')
      	{
      		
			global $image_path,$LAYOUT_SKIN,$Lang;
		
			if($ts == '')
		  		$thisDateString = '';
			else
		  		$thisDateString = date("Y-m-d",$ts);
			$cycle = ($cycle)? $cycle : "&nbsp;";
		
			if($ts=="")
			{
				$x = "<td class=\"non_bookable\" ><br></td>";
			}
			else
			{
				# Temp condition: 
				#		1. Any record in avaliable period setting => bookable
				# 		2. Holiday => non-bookable
				#		3. School Holiday => non-bookable
				#		4. non of the above => non-bookable 
//				if ($this->IsHolidayEvent($ts,1))
//				{
//					$Event = "<font class='public_holiday'>".date("j",$ts)."</font>";
//					$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
//					
//					if($lebooking->isAvaliableBookingPeriod($thisDateString, $FacilityType, $FacilityID))
//					{
//						$ClassType = "bookable";
//					}
//					else
//					{
//						$ClassType = "non_bookable";
//					}
//			  	}
//				else if($this->IsSchoolHolidayEvent($ts,1))
//				{
//					$Event = "<font class='school_holiday'>".date("j",$ts)."</font>";
//					$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
//					
//					if($lebooking->isAvaliableBookingPeriod($thisDateString, $FacilityType, $FacilityID))
//					{
//						$ClassType = "bookable";
//					}
//					else
//					{
//						$ClassType = "non_bookable";
//					}
//				}
//			  	else
//			  	{
//			  		if($lebooking->isAvaliableBookingPeriod($thisDateString, $FacilityType, $FacilityID))
//					{
//						$ClassType = "bookable";
//					}
//					else
//					{
//						$ClassType = "non_bookable";
//					}
//					$Event = date("j",$ts);
//		    		$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
//			  	}

//		  		if($lebooking->isAvaliableBookingPeriod($thisDateString, $FacilityType, $FacilityID))
				if(in_array($thisDateString, (array)$AvailableBookingDate))
				{
					$ClassType = "bookable";
				}
				else
				{
					$ClassType = "non_bookable";
				}
				
				if ($this->IsHolidayEvent($ts,1))
				{
					$Event = "<font class='public_holiday'>".date("j",$ts)."</font>";
			  	}
				else if($this->IsSchoolHolidayEvent($ts,1))
				{
					$Event = "<font class='school_holiday'>".date("j",$ts)."</font>";
				}
			  	else
			  	{
					$Event = date("j",$ts);
			  	}				
//	    		$Event = ($this->IsEvent($ts,1)) ? $Event : $Event;
	    		
			  	//$EventImage = "<img src=\"".$this->image_path2."/10x10.gif\" width=\"3\" height=\"7\" border=\"0\" />";
			  	    
			    ### School Calender - Holiday & Event Edit/Add ###
			    if($css_academic_event != "" && $css_school_event != ""){
			      // continuous
			    }else if($css_academic_event != "" && $css_school_event == ""){
			      $css_school_event = $css_academic_event;
			    }else if($css_academic_event == "" && $css_school_event != ""){
			      $css_academic_event = $css_school_event;
			    }else{
			      $css_academic_event = "";
			      $css_school_event = "";
			    }
			    
//	    		$x  = "<td class=\"$ClassType $editable_class\" width=\"14%\" >$link";
//	    		$x .= "<table id=\"Day_Table_$thisDateString\" width=\"100%\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"$ClassType\" >
//			              <tr>
//			                <td class=\"$editable_class $css_academic_event\" align=\"center\" width=\"50%\" ><div class=\"booking_calendar_date\">$Event</div></td>
//			                <td class=\"$editable_class $css_school_event\" align=\"right\" valign=\"top\">{$EventImage}</td>
//			              </tr>
//			              <tr>
//			                <td class=\"indexcalendarremark $editable_class $css_academic_event\" align=\"left\" valign=\"top\" width=\"50%\" >$cycle</td>
//			                <td class=\"indexcalendarremark $editable_class $css_school_event\" align=\"left\" valign=\"top\" bgcolor=\"$ColorCode\">&nbsp;</td>
//			              </tr>
//			            </table>";
//	    		$x .= "</a></td>\n";
	    		$x  = "<td class=\"$ClassType\" width=\"14%\" >";
		    		$x  .= '<div class="booking_calendar_date">'.$Event.'</div>'."\n";
		    		if($ColorCode)
		    			$x  .= '<div class="booking_calendar_tool" ><div class="icon_timezone" style="background-color: '.$ColorCode.';">&nbsp;</div></div>'."\n";
		    		$x  .= '<br style="clear: both;">'."\n";
		    		$x  .= '<em>'.$cycle.'</em>'."\n";
                $x .= "</td>\n";
			}
			return $x;
		}
		
		##########PrintingViewForCalendar################
		function displayCalandar_CyclePeriodPrintingView($month, $year, $ShowArchiveRecord, $ParIsColorPrint=false, $ClassGroupID='') {
			include_once('libcycleperiods.php');
	      	include_once("libcycleperiods_ui.php");
	        global $i_DayType4, $i_EventList, $image_path, $intranet_session_language;
	        global $eEnrollment, $LAYOUT_SKIN, $Lang;
	        
	        $row = $this->returnCalendarArray($month, $year);
	        $a = new libcycleperiods();
	        //$cycles_array = $a->getCycleInfoByYearMonth($year,$month);
	        $cycles_array = $a->getProductionCycleInfo($year,$month);
	        
	        $start_timestamp = strtotime(sprintf("%4d-%02d-01 00:00:00",$year,$month));
			$end_timestamp = strtotime(sprintf("%4d-%02d-%02d 00:00:00",$year,$month,date("t",$start_timestamp)));
			$this->initAllEventRecordByTimestampRange($start_timestamp,$end_timestamp,$ClassGroupID); // batch get events info in one month
	        
	        $x = "";
	        $x .= "<div id=\"CalendarDiv\">\n";
	        # Month and Day Title
	        $x .= "
	            <table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\" bgcolor=\"#D2D2D2\">\n
	              <tr><td colspan=\"7\" align=\"center\" class=\"tabletop\">".$Lang['General']['month'][$month]." ".$year."</td></tr>\n
	              <tr>\n
	                <td align=\"center\" class=\"icalendar_weektitle \" ><font class='sundayField' color='red'>&nbsp;".$Lang['General']['DayType4'][0]."</font></td>\n
	                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][1]."</td>\n
	                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][2]."</td>\n
	                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][3]."</td>\n
	                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][4]."</td>\n
	                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][5]."</td>\n
	                <td align=\"center\" class=\"icalendar_weektitle \" >&nbsp;".$Lang['General']['DayType4'][6]."</td>\n
	              </tr>\n
	        ";
	        
	        # Show Dates
	        for($i=0; $i<sizeof($row); $i++)
	        {
	          $x .= ($i % 7 == 0) ? "<tr>\n" : "";
	          
	          if(!empty($row[$i]))
	            $date_string = date("Y-m-d",$row[$i]);
	          
	          $cycle = $cycles_array[$date_string];
	          $display = $cycle[2];
	          
	          $IS_PRINTING_VERSION = true;
	          
	          $x .= $this->Display_Calendar_Event($row[$i],$display,0,$ShowArchiveRecord, $IS_PRINTING_VERSION, $ParIsColorPrint,'',$ClassGroupID);
	          $x .= ($i % 7 == 6) ? "</tr>\n" : "";
	        }
	        $x .= "</table>\n";
	        $x .= "</div>\n";
	        
	        return $x;
		}
		function displayCalandar_MonthlyEventTablePrintingView($month, $year, $ShowArchiveRecord, $ParIsColorPrint=false, &$ParEventFlag=false, $ClassGroupID='') {
	        global $image_path, $LAYOUT_SKIN, $Lang;
	        include_once("libinterface.php");
	        $linterface = new interface_html();
	    	
	    	$DefaultNumOfRowShow = 5;
	        ## Start Here 
	        $StartDateOfMonth = date('Y-m-d',mktime(0,0,0,$month,1,$year));
	        
	        $LastDayOfMonth = date('t',mktime(0,0,0,$month,1,$year));
	        $EndDateOfMonth = date('Y-m-d',mktime(0,0,0,$month,$LastDayOfMonth,$year));
	        
	        $arrResult = $this->getEventInfo($StartDateOfMonth, $EndDateOfMonth, $ClassGroupID);
	    
			//if ($arrResult[0]==NULL) {}
			//else {
		        $x .= "<table border='0' width='96%' cellpadding='0' cellspacing='0'>";
		        $x .= "<tr>";
		        $x .= "<td><h1 class='event_list_month'><span> - ".$Lang['General']['month'][$month]." - </span></h1></td>";
		        $x .= "</tr>";
		        $x .= "<tr><td>";
		        $x .= "<table class='common_table_list'>";
		        
		        $arrDate = array();
		        if(sizeof($arrResult)>0) {
		        	$ParEventFlag = true; 
		          for($i=0; $i<sizeof($arrResult); $i++) {
		            list($EventID, $EventTitle, $StartDate, $EndDate, $IsSkipCycle, $EventType) = $arrResult[$i];
		            $StartDate = substr($StartDate,0,10);
		            $EndDate = substr($EndDate,0,10);
		            $NumOfDay = round( abs(strtotime($EndDate)-strtotime($StartDate)) / 86400, 0 );
		            
		            if($NumOfDay>0) {
						## Have Related Event
						$arrDate[] = $StartDate;
						$arrDateNoEvent[$StartDate]['NoEvent'] = 0;
						$arrDateEvent[$StartDate]['EventID'][] = $EventID;
						$arrDateEvent[$StartDate][$EventID]['AnyRelated'] = 1;
						$arrDateRelatedEvent[$StartDate][$EventID]['EndDate'][] = $EndDate;
						$arrDateRelatedEvent[$StartDate][$EventID]['EventTitle'][] = $EventTitle;
						$arrDateRelatedEvent[$StartDate][$EventID]['IsSkipCycle'][] = $IsSkipCycle;
						$arrDateRelatedEvent[$StartDate][$EventID]['EventType'][] = $EventType;
		            } else {
						## Single Event
						$arrDate[] = $StartDate;
						$arrDateNoEvent[$StartDate]['NoEvent'] = 0;
						$arrDateEvent[$StartDate]['EventID'][] = $EventID;
						$arrDateEvent[$StartDate][$EventID]['AnyRelated'] = 0;
						$arrDateSingleEvent[$StartDate][$EventID]['EventTitle'][] = $EventTitle;
						$arrDateSingleEvent[$StartDate][$EventID]['IsSkipCycle'][] = $IsSkipCycle;
						$arrDateSingleEvent[$StartDate][$EventID]['EventType'][] = $EventType;
		            }
		          }
		        }else{
		          $arrDate[] = $StartDate;
		          $arrDateNoEvent[$StartDate]['NoEvent'] = 1;
		        }
		        $arrDate = array_unique($arrDate);
	        
		        foreach($arrDate as $key=>$StartDate)
		        {
		          if($arrDateNoEvent[$StartDate]['NoEvent'] == 0)
		          {
						$haveTitle = 0;
						for($i=0; $i<sizeof($arrDateEvent[$StartDate]['EventID']); $i++) {
							$NumOfRow++;
							$RowDisplay = '';
							$RowClass = '';
							
							$EventID = $arrDateEvent[$StartDate]['EventID'][$i];
		                 
		                 if($arrDateEvent[$StartDate][$EventID]['AnyRelated'] == 0) {
							$EventTitle = $arrDateSingleEvent[$StartDate][$EventID]['EventTitle'][0];
							$IsSkipCycle = $arrDateSingleEvent[$StartDate][$EventID]['IsSkipCycle'][0];
							$EventType = $arrDateSingleEvent[$StartDate][$EventID]['EventType'][0];
		                   
							switch ($EventType){
								case 0: 
										$title_div_class = "title_school_event";
										$span_icon = "<span class='event_type_icon'>&nbsp;</span>";
										break;
		                    	case 1: 
										$title_div_class = "title_academic";
										$span_icon = "<span class='event_type_icon'>&nbsp;</span>";
										break;
								case 2: 
										$title_div_class = "title_group_event";
										$span_icon = "<span class='event_type_icon'><img src=\"".$this->image_path2."/index/calendar/icon_groupevent.gif\"  align=\"right\"></span>";
										break;
								case 3: 
										$title_div_class = "title_public_holiday";
										$span_icon = "<span class='event_type_icon'>1</span>";
										break;
								case 4: 
										$title_div_class = "title_school_holiday";
										$span_icon = "<span class='event_type_icon'>1</span>";
										break;
								default:
										break;
		                   	}
		                   
		                   if($IsSkipCycle)
		                   	$iconSkipCycleDay = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_cycle_day_off.gif\" Title=\"".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['NotCountInCycleDay']."\">";
		                   else
		                    $iconSkipCycleDay = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_cycle_day_on.gif\" Title=\"".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CountInCycleDay']."\">";

							# Print in B/W mode does not need img and span icon
							if($ParIsColorPrint) {}
							else {
								$iconSkipCycleDay="";
								$span_icon="";
							}
	                   		$x .= "<tr class='sub_row ".$RowClass."' id='EventLayer_$i' style='display:$RowDisplay'>
	                   				<td width='20%' nowrap>".date("j",strtotime($StartDate))."</td>
									<td><div class='event_list_title $title_div_class'>$span_icon<span>$EventTitle</span></div></td>
	                         		</tr>";
		                   $haveTitle = 1;
		                 } else {
		                   $EndDate = $arrDateRelatedEvent[$StartDate][$EventID]['EndDate'][0];
		                   $EventTitle = $arrDateRelatedEvent[$StartDate][$EventID]['EventTitle'][0];
		                   $IsSkipCycle = $arrDateRelatedEvent[$StartDate][$EventID]['IsSkipCycle'][0];
		                   $EventType = $arrDateRelatedEvent[$StartDate][$EventID]['EventType'][0];
		                   
		                   switch ($EventType){
		                    case 0: 
		                                   $title_div_class = "title_school_event";
		                                   $span_icon = "<span class='event_type_icon'>&nbsp;</span>";
		                                   break;
		                    case 1: 
		                                   $title_div_class = "title_academic";
		                                   $span_icon = "<span class='event_type_icon'>&nbsp;</span>";
		                                   break;
		                    case 2: 
		                                   $title_div_class = "title_group_event";
		                                   $span_icon = "<span class='event_type_icon'>1</span>";
		                                   break;
		                    case 3: 
		                                   $title_div_class = "title_public_holiday";
		                                   $span_icon = "<span class='event_type_icon'>1</span>";
		                                   break;
		                    case 4: 
		                                   $title_div_class = "title_school_holiday";
		                                   $span_icon = "<span class='event_type_icon'>1</span>";
		                                   break;
		                    default:
		                                   break;
		                   }
		                   
		                   if($IsSkipCycle)
		                    $iconSkipCycleDay = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_cycle_day_off.gif\" Title=\"".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['NotCountInCycleDay']."\">";
		                   else
		                    $iconSkipCycleDay = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_cycle_day_on.gif\" Title=\"".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CountInCycleDay']."\">";
							
							# Print in B/W mode does not need img and span icon
							if($ParIsColorPrint) {}
							else {
								$iconSkipCycleDay="";
								$span_icon="";
							}
							
		           			$x .= "<tr class='sub_row ".$RowClass."' id='EventLayer_$i' style='display:$RowDisplay'>
		           					<td width='20%' nowrap>".date("j",strtotime($StartDate))." ~ ".date("j M Y",strtotime($EndDate))."</td>
		                           	<td><div class='event_list_title $title_div_class'>$span_icon<span>$EventTitle</span></div></td>
		                         </tr>";
		                 }
		               }
		          }else{
		               $x .= "<tr class='sub_row'><td align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
		          }
		        }
	
		        $x .= "</table>";
		        $x .= "</td></tr>";
		        $x .= "</table>";
			//}
	        
	        return $x;
		}
		function getEventInfo($StartDateOfMonth, $EndDateOfMonth, $ClassGroupID='') {
			global $sys_custom;
			
			if($ClassGroupID != "") {
				//if($_SESSION['UserType'] != USERTYPE_STAFF){
					$classGroupIdCond = " AND (a.ClassGroupID IN (".(is_array($ClassGroupID)?implode(",",$ClassGroupID):$ClassGroupID).") OR a.ClassGroupID IS NULL OR a.ClassGroupID='') ";
				//}else{
				//	$classGroupIdCond = " AND a.ClassGroupID IN (".(is_array($ClassGroupID)?implode(",",$ClassGroupID):$ClassGroupID).") ";
				//}
			}
			if($_SESSION["platform"]=="KIS") {
				$canGetAllEvents = ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] || $_SESSION['UserType'] == USERTYPE_STAFF);
				if(!$canGetAllEvents){
					$relatedEvents = $this->returnAllEvents(strtotime($StartDateOfMonth),strtotime($EndDateOfMonth));
					$relatedEventIdAry = Get_Array_By_Key($relatedEvents,'EventID');
					$relatedEventIdCond = " AND a.EventID IN (".implode(",",$relatedEventIdAry).") ";
				}
			}
			$sql = "SELECT 
		                a.EventID, a.Title, a.EventDate As StartDate, c.EndDate AS EndDate, a.IsSkipCycle, a.RecordType
		              FROM 
		                INTRANET_EVENT AS a INNER JOIN (SELECT b.EventID, b.RelatedTo, MAX(b.EventDate) AS EndDate FROM INTRANET_EVENT AS b WHERE b.RelatedTo IS NOT NULL OR b.RelatedTo != '' GROUP BY b.RelatedTo) AS c ON (a.EventID = c.RelatedTo) 
		              WHERE
		                a.EventDate BETWEEN '$StartDateOfMonth' AND '$EndDateOfMonth'
	                  OR
	              		c.EndDate BETWEEN '$StartDateOfMonth' AND '$EndDateOfMonth'
		                $classGroupIdCond $relatedEventIdCond
		              GROUP BY 
		                a.RelatedTo
		              ORDER By
		                a.EventDate, a.RecordType DESC";
	        $arrResult = $this->returnArray($sql,5);
	        return $arrResult;
		}
		
		function displayCalendar_eServiceeBookingSelectDate($month='', $year='', $selectedList=array(), $availableList=array()) {
			include_once('libcycleperiods.php');
			include_once('libinterface.php');

	      	global $Lang,$image_path,$LAYOUT_SKIN;
	        
	        $row = $this->returnCalendarArray($month, $year);
	        $a = new libcycleperiods();

	        $cycles_array = $a->getProductionCycleInfo($year,$month);
	        	        
	        
	        $x = "";
	        # Month and Day Title
	        $x .='<table border="0" align="center" cellpadding="0" cellspacing="0">';
				$x .='<tr>';
					$x .='<td width="21"><a href="javascript:void(0);" onclick="ChangeMonth(-1)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icalendar/icon_prev_off.gif" width="21" height="21" border="0" align="absmiddle" /></a></td>';
					$x .='<td align="center"><div id="CalendarDateTitle" class="tablelink">'.date("F Y",strtotime("$year-$month-01")).'</div></td>';
					$x .='<td width="21"><a href="javascript:void(0);" onclick="ChangeMonth(1)"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icalendar/icon_next_off.gif" width="21" height="21" border="0" align="absmiddle" /></a></td>';
				$x .='</tr>';
			$x .='</table>';
			$x .='<table border="0" cellspacing="0" cellpadding="0" class="booking_table_small_cal">';
				$x .='<tr>';
					$x .='<td class="icalendar_weektitle">S</td>';
					$x .='<td class="icalendar_weektitle">M</td>';
					$x .='<td class="icalendar_weektitle">T</td>';
					$x .='<td class="icalendar_weektitle">W</td>';
					$x .='<td class="icalendar_weektitle">T</td>';
					$x .='<td class="icalendar_weektitle">F</td>';
					$x .='<td class="icalendar_weektitle">S</td>';
				$x .='</tr>';
	        
	        # Show Dates
	        for($i=0; $i<sizeof($row); $i++)
	        {
	          $x .= ($i % 7 == 0) ? "<tr>\n" : "";
	          
	          if(!empty($row[$i]))
	            $date_string = date("Y-m-d",$row[$i]);
	          
	          $cycle = $cycles_array[$date_string];
	          $display = $cycle[2];
	          
	          $selected = in_array($date_string,$selectedList)?1:0;
	          $available = in_array($date_string,$availableList)?1:0;

	          $x .= $this->Display_eBooking_Calendar_Date($row[$i], $display, $selected, $available);
	          $x .= ($i % 7 == 6) ? "</tr>\n" : "";
	        }
	        $x .= "</table>\n";
	        
			# Calender Reference
//	        $x .= '<table border="0" cellpadding="0" cellspacing="3">';
//				$x .= '<tr>';
//					//$x .= '<td> '.$Lang['eBooking']['eService']['Ref'].' :</td>';
//					$x .= '<td  class="non_bookable" style="border:1px dashed #CCCCCC; font-size:11px; padding:2px;">'.$Lang['eBooking']['eService']['NotAvailable'].'</td>';
//					$x .= '<td  class="bookable" style="border:1px dashed #CCCCCC; font-size:11px; padding:2px;">'.$Lang['eBooking']['eService']['Available'].'</td>';
//					$x .= '<td style="border:1px dashed #CCCCCC; font-size:11px; padding:2px;"><em>'.$Lang['eBooking']['eService']['CycleDay'].'</em></td>';
//				$x .= '</tr>';
//			$x .= '</table>';

			$linterface = new interface_html();
			$IndicationArr[]= array("normal",$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['AvaliablePeriod']);		
			$IndicationArr[]= array("drafted",$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['NonAvaliablePeriod']);
			$x .= $linterface->GET_RECORD_INDICATION_IP25($IndicationArr);

	        
	        return $x;
		}
		
		function Display_eBooking_Calendar_Date($ts='', $cycle='', $selected=0, $available=0)
		{

			if($ts=="")
	        {
	        	$x = "<td class=\"small_cal_blank\">&nbsp;</td>";
	        }
	        else if($available==1)
	        {
				$datestr = date("Y-m-d",$ts);
				$day = date("D",$ts); 
	        	$x = "<td class=\"small_cal_normal_day bookable\" ><a href=\"javascript:void(0);\" class=\"ebooking_choose\" onclick=\"addDate('$datestr','$cycle','$day');\"><span>".date("j",$ts)."<br><em>".$cycle."</em></span></a></td>";
			}
			else
			{
				$x = "<td class=\"small_cal_normal_day non_bookable\" ><span>".date("j",$ts)."<br><em>".$cycle."</em></span></td>";
			}
			
			return $x;
		}		
		
		function synEventToModules($eventIdAry='', $moduleAry='') {
			global $PATH_WRT_ROOT, $plugin, $intranet_db;
			
			$successAry = array();
			
			### Syn data to LMS
			if ($plugin['library_management_system'] && ($moduleAry=='' || in_array('library_management_system', $moduleAry))) {
				include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
				$liblms = new liblms();
				
				$INTRANET_EVENT = $intranet_db.'.INTRANET_EVENT';
				$LIBMS_HOLIDAY = $liblms->db.'.LIBMS_HOLIDAY';
				
				if ($eventIdAry != '') {
					$conds_ie_eventId = " And ie.EventID In ('".implode("','", (array)$eventIdAry)."') ";
					$conds_lh_intranetEventId = " And lh.IntranetEventID  In ('".implode("','", (array)$eventIdAry)."') ";
				}
				
				/*
				 * RecordType:	3 => Public Holiday
				 * 				4 => School Holiday
				 */ 
				$sql = "Select 
								ie.EventID,
								ie.Title, 
								ie.EventDate,
								ie.DateModified,
								ie.ModifyBy
						From 
								$INTRANET_EVENT as ie
						Where 
								ie.RecordType In (3, 4)
								$conds_ie_eventId
				";
				$intranetEventDataAry = $liblms->returnResultSet($sql);
				$intranetEventIdAry = Get_Array_By_Key($intranetEventDataAry, 'EventID');
				$numOfEvent = count($intranetEventDataAry);
				
				$sql = "Select lh.IntranetEventID From $LIBMS_HOLIDAY as lh Where 1 $conds_lh_intranetEventId";
				$lmsHolidayDataAry = $liblms->returnResultSet($sql);
				$lmsIntranetEventIdAry = Get_Array_By_Key($lmsHolidayDataAry, 'IntranetEventID');
				unset($lmsHolidayDataAry);
				
				$insertAry = array();
				for ($i=0; $i<$numOfEvent; $i++) {
					$_eventId = $intranetEventDataAry[$i]['EventID'];
					$_eventTitle = trim($intranetEventDataAry[$i]['Title']);
					$_eventDate = $intranetEventDataAry[$i]['EventDate'];
					$_dateModified = $intranetEventDataAry[$i]['DateModified'];
					$_modifiedBy = $intranetEventDataAry[$i]['ModifyBy'];
						
					$_eventIdDb = $liblms->pack_value($_eventId, 'int');
					$_eventTitleDb = $liblms->pack_value($_eventTitle, 'str');
					$_eventDateDb = $liblms->pack_value($_eventDate, 'date');
					$_dateModifiedDb = $liblms->pack_value($_dateModified, 'date');
					$_modifiedByDb = $liblms->pack_value($_modifiedBy, 'int');
					
					if (in_array($_eventId, (array)$lmsIntranetEventIdAry)) {
						// update
						$sql = "Update 
										$LIBMS_HOLIDAY
								Set 
										Event = ".$_eventTitleDb.",
										DateFrom = ".$_eventDateDb.",
										DateEnd = ".$_eventDateDb.",
										DateModified = ".$_dateModifiedDb.",
										LastModifiedBy = ".$_modifiedByDb."
								Where 
										IntranetEventID = '".$_eventIdDb."'
								";
						$successAry['library_management_system']['update'][$_eventId] = $liblms->db_db_query($sql);
					}
					else {
						// insert
						$insertAry[] = " (	".$_eventTitleDb.", 
											".$_eventDateDb.", 
											".$_eventDateDb.",
											".$_eventIdDb.",
											".$_dateModifiedDb.",
											".$_modifiedByDb."
										) ";
					}
				}
				
				### Handle deleted user in ITRANET_EVENT
				$deletedEventIdAry = array_values(array_diff($lmsIntranetEventIdAry, $intranetEventIdAry));
				$sql = "Delete From $LIBMS_HOLIDAY Where  IntranetEventID In ('".implode("','", (array)$deletedEventIdAry)."')";
				$successAry['library_management_system']['deleteHoliday'] = $liblms->db_db_query($sql);
				
				
				### Process insert statement by batch
				$numOfInsert = count($insertAry);
				if ($numOfInsert > 0) {
					$numOfDataPerChunck = 500;
					$numOfChunk = ceil($numOfInsert / $numOfDataPerChunck);
					$insertChunkAry = array_chunk($insertAry, $numOfChunk);
					foreach((array)$insertChunkAry as $_insertValueAry) {
						$_insertValueText = implode(", ", $_insertValueAry);
						$sql = "INSERT INTO $LIBMS_HOLIDAY
												(Event, DateFrom, DateEnd, IntranetEventID, DateModified, LastModifiedBy)
										VALUES
												$_insertValueText
										";
						$successAry['library_management_system']['insert'][] = $liblms->db_db_query($sql);
					}
				}
			}
			
			return in_multi_array(false, (array)$successAry)? false : true;
		}
  }
?>