<?php 
//modifying by: 

/*
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Modification of this file may affect different modules.                          !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Please check carefully and inform team leaders after modifying it.               !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

class libeclassapiauth
{
	var $salt = "xShY3sb1"; //hardcoded, cannot change!
	var $apiKeyList;
	
	function libeclassapiauth()
	{
		$this->apiKeyList = $this->GetAPIKeyList();
	}
	
	function GetAPIKeyList()
	{
		//temp hardcode the data here first, later should read from a central DB
		return array(
					array(
						"Company" => "ASL", 
						"Project" => "eClass Parent App", 
						"CreateDate" => "2011/8/29", 
						"Version" => "1", 
						"APIKey" => "4cd8e7d329a2b967e0569d7b8d4cec69",
						"Status" => "A"
					),
					array(
						"Company" => "AVRIO", 
						"Project" => "eSchoolPad", 
						"CreateDate" => "2014/03/07", 
						"Version" => "1", 
						"APIKey" => "ab1b44b68f319962f6f2009fd941900b",
						"Status" => "A"
					)
					,
					array(
						"Company" => "BL", 
						"Project" => "eClass App", 
						"CreateDate" => "2014/09/01", 
						"Version" => "1", 
						"APIKey" => "01c6f164a501d4609b3bddad2044961d",
						"Status" => "A"
					),
					array(
						"Company" => "BL", 
						"Project" => "eClass App (Teacher)", 
						"CreateDate" => "2014/09/01", 
						"Version" => "1", 
						"APIKey" => "2e7feb4a406efc0c5da8a83ff8be4f26",
						"Status" => "A"
					),
					array(
						"Company" => "BL", 
						"Project" => "CEES", 
						"CreateDate" => "2016/02/15", 
						"Version" => "1", 
						"APIKey" => "3a144f92e477774197e10892fd802cd2",
						"Status" => "A"
					),
					array(
						"Company" => "BL", 
						"Project" => "eClass App (Student)", 
						"CreateDate" => "2016/03/08", 
						"Version" => "1", 
						"APIKey" => "6b7e7ed26ec4b9fa416c89b85c1ae25c",
						"Status" => "A"
					),
					array(
						"Company" => "BL", 
						"Project" => "SFOC App", 
						"CreateDate" => "2016/07/05", 
						"Version" => "1", 
						"APIKey" => "9840a617985469c9ce80f1d360e6a583",
						"Status" => "A"
					),
					array(
						"Company" => "BL", 
						"Project" => "TNG dev", 
						"CreateDate" => "2016/12/15", 
						"Version" => "1", 
						"APIKey" => "7af437d8637568264d7f787c8f190031",
						"Status" => "A"
					),
					array(
						"Company" => "BL", 
						"Project" => "TNG production", 
						"CreateDate" => "2016/12/15", 
						"Version" => "1", 
						"APIKey" => "8182e726e64b422d5bee02dd8bbe92b1",
						"Status" => "A"
					),
					array(
						"Company" => "BL", 
						"Project" => "IP/EJ platform", 
						"CreateDate" => "2017/02/07", 
						"Version" => "1", 
						"APIKey" => "987fdce7de218bd688b19d7fac2a1fc3",
						"Status" => "A"
					),
					array(
						"Company" => "BL", 
						"Project" => "PL2 App", 
						"CreateDate" => "2017/02/17", 
						"Version" => "1", 
						"APIKey" => "4faa86df47b877ee958c6a3dd1c7aa81",
						"Status" => "A"
					),
					array(
						"Company" => "BL", 
						"Project" => "App Central Server", 
						"CreateDate" => "2017/11/01", 
						"Version" => "1", 
						"APIKey" => "27796e2b74b6f576b97990451b7c518b",
						"Status" => "A"
					),
        		    array(
        		        "Company" => "BL",
        		        "Project" => "Reprint Card System",
        		        "CreateDate" => "2018/10/31",
        		        "Version" => "1",
        		        "APIKey" => "5f2245fa26c18f212bb61f8339b8a6bb",
        		        "Status" => "A"
        		    ),
					array(
						"Company" => "BL",
						"Project" => "eClass App",
						"CreateDate" => "2020/11/06",
						"Version" => "2",
						"APIKey" => "1f87eb62fde85fff83e0aaf51f4e57ec",
						"Status" => "A"
					),
					array(
						"Company" => "BL",
						"Project" => "eClass App (Teacher)",
						"CreateDate" => "2020/11/06",
						"Version" => "2",
						"APIKey" => "626d5a50c05d9cc6ddd9e91af5350d92",
						"Status" => "A"
					),
					array(
						"Company" => "BL",
						"Project" => "eClass App (Student)",
						"CreateDate" => "2020/11/06",
						"Version" => "2",
						"APIKey" => "9b7e167fffd24e48473245c8cc46ea68",
						"Status" => "A"
					)
		);
	}
	
	function GenerateAPIKey($company, $project, $createDate, $version)
	{
		return MD5($company."||".$project."||".$createDate."||".$version."_".$this->salt);
	}
	
	function VerifyAPIKey($key)
	{
		foreach($this->apiKeyList as $apiKey)
		{
			if (strcmp($apiKey["APIKey"], $key) == 0 && $apiKey["Status"] == "A")
			{
				return true;
			}
		}
		
		return false;
	}
	
	function GetAPIKeyInfoOfCompany($companyName) {
		$keyAry = BuildMultiKeyAssoc($this->GetAPIKeyList(), 'Company');
		return $keyAry[$companyName];
	}
	
	function GetProjectByApiKey($apiKey) {
		$infoAry = BuildMultiKeyAssoc($this->GetAPIKeyList(), 'APIKey');
		return $infoAry[$apiKey]['Project'];
	}
	
	function GetAPIKeyByProject($project) {
		$infoAry = BuildMultiKeyAssoc($this->GetAPIKeyList(), 'Project');
		return $infoAry[$project]['APIKey'];
	}

	function GetDataByApiKey($apiKey) {
		$infoAry = BuildMultiKeyAssoc($this->GetAPIKeyList(), 'APIKey');
		return $infoAry[$apiKey];
	}
}

/*
 * Sample Usage:
 
 	$company = "ASL";
	$project = "eClass Parent App";
	$createDate = "2011/8/29";
	$version = "1";
	
	$auth = new libeclassapiauth();
	echo $auth->GenerateAPIKey($company, $project, $createDate, $version);
	echo "<br>";
	var_dump($auth->VerifyAPIKey("4cd8e7d329a2b967e0569d7b8d4cec69"));
*/
?>