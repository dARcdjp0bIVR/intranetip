<?php
// page editing by:
/********************************* Modification Log ********************************************
 * 2020-08-21 (Henry) : Modified Build_Mime_Text(), added para $isFromApp to use html content
 * 2020-05-22 (Henry) : Modified Get_Message_Text(), rollback remove '../' in inline image url path
 * 2020-02-27 (Sam)   : Modified Get_Message_Text(), remove '../' in inline image url path to avoid WAF blocking [G180466]
 * 2019-09-03 (Ray)   : Added Get_Message_Text, calendar type
 * 2019-05-31 (Carlos): modified MIME_Decode($str) use alternative charset CP949 for charset ks_c_5601-1987.
 * 2019-05-30 (Carlos): Added Detect_Convert_FileName_To_UTF8($filename) to convert non-utf-8 file name to utf-8 file name. Applied it to Extract_Tnef_Attachment($PartNumber,$Folder,$UID). 
 * 2019-03-06 [Carlos]: Modified Get_Message_Text(), fixed display of structure {multipart/alternative >>> text/plain | multipart/related}.
 * 2017-09-22 [Carlos]: Modified Get_Message_Text(), for plain text part, if htmlspecialchars() with its charset return empty string, do not specify the charset.
 * 2017-06-09 [Carlos]: $sys_custom['iMailPlus']['CKEditor'] Modified Build_Mime_Text(), changed to use CKEditor and build html content.
 * 2017-03-09 [Carlos]: Modified Build_Mime_Text(), avoid AttachPartNumber attachments duplicated added to the email if inline list already has same name file. 
 * 2016-05-23 [Carlos]: Modified Get_Attach_Message_Detail(), apply MIME_Decode() with imap_mime_header_decode() to decode the subject.
 * 2016-03-30 [Carlos]: Modified Get_Attachment_List(), comment out the exclusion of applefile attachment type.
 * 2016-03-07 [Carlos]: Modified Build_Mime_Text(), remove backward slash, double quotes and single quotes from recipient names.
 * 2016-01-29 [Carlos]: Added getImapMessagePartAttachmentFileName($part) to get filename from a php mime part object. 
 * 						Modified Get_Attachment() and  Get_Attachment_List() to use getImapMessagePartAttachmentFileName(). 
 * 2015-12-15 [Carlos]: Added Get_File_Extension_From_Mime_Type($mime_type) for determining the file extension of inline files without filename. 
 * 						Modified Get_Attachment() and Get_Message_Text(), for inline files without filename, to makeup a filename according to its mime type and content id.
 * 2015-12-01 [Carlos]: Modified Get_Message_Text(), handle the nested multipart structure {multipart/alternative -> multipart/related}.
 * 2015-10-30 [Carlos]: Modified Build_Mime_Text(), add display name to Reply-to header.
 * 2015-04-20 [Carlos]: Modified Build_Mime_Text(), split name and email with imap_gamma->splitNameEmailEntry()
 * 2014-08-19 [Carlos]: Modified Build_Mime_Text(), added $sys_custom['iMailPlus']['CcHeaderNoLineBreak'] for Cc header do not add <CR><LF><SPACE>
 * 2014-02-25 [Carlos]: Modified Get_Message_Text() - use iconv() to convert text from BIG5-HKSCS to UTF-8
 * 2014-02-21 [Carlos]: Modified Get_Attachment() - added paramter $Parts; 
 * 						Modified Get_Message_Text() and Get_Related_Inline_Info() - Get_Attachment() reuse $StructureArray
 * 2013-08-01 [Carlos]: Modified Build_Mime_Text(), use <CR><LF><SPACE> to separate recipients
 * 2013-03-25 [Carlos]: Fix Get_Message_Text() use GBK to do convertion for gb18030
 * 2013-02-21 [Carlos]: Fix Build_Mime_Text() fail to replace inline image file path with content id
 * 2012-05-07 [Carlos]: Modified Get_Message_Text() and Build_Mime_Text() fix inline image same filename conflict problem 
 * 2011-08-19 [Yuen] : support iPad/Andriod and send using text/plain in Build_Mime_Text()
 * 2011-05-31 [Carlos] : Fix Get_Attachment_List() calculate base64 encoded file size to approximate binary file size
 * 2011-04-15 [Carlos] : Modified Build_Mime_Text() get X-eClass-MailID from global var, not construct locally.
 * 2011-04-04 [Carlos] : Added default folder $SYS_CONFIG['Mail']['SystemMailBox']['HiddenFolder'] at constructor
 * 2011-02-09 [Carlos] : Fixed Get_Mail_Format() to base64 encode correctly 
 * 2010-12-16 [Carlos] : Convert tnef filesize from byte to kb in Get_Attachment_List()
 * 2010-12-15 [Carlos] : Remove code to delete /related folder recursively which will affects 
 * 						 uploaded inline images when composing mail 
 * 2010-12-09 [Carlos] : Changed to use email to get preference
 ***********************************************************************************************/
include_once("imap_socket.php");
include_once("imap.php");
//include_once("database.php");
include_once("libfilesystem.php");
//include_once("file_table.php");

class imap_cache_agent extends imap_socket {
	var $MailAddress;
	var $MailPassword;
	var $CurUserName;
	var $DefaultFolderList;
	var $ConnectionStatus;
	var $MimeLineBreak;
	var $MaxHeaderSize=32768;
	var $MaxRecipientSize=500;
	var $ReplaceAttachmentFlag;
	var $ExtMailFlag;
	
	function imap_cache_agent($MailAddress="",$MailPassword="",$UserType="") {
		global $SYS_CONFIG;
		
		########### Add Default Folder #########
		if(!isset($SYS_CONFIG['Mail']['SystemMailBox']['HiddenFolder']))
		{
			$SYS_CONFIG['Mail']['SystemMailBox']['HiddenFolder'] = "Hidden";
		}
		##### End adding Default Folder #######
		
		//$this->MimeLineBreak = $SYS_CONFIG['Mail']['MimeLineBreak'];
		// for create mime message
		$this->MimeLineBreak = chr(10);
		
		if ($MailAddress == "") {
			parent::imap_socket();
			$this->CurUserName = $_SESSION['SSV_EMAIL_LOGIN'];
			$this->MailAddress = ($_SESSION['SSV_LOGIN_EMAIL']==""?($this->CurUserName."@".$SYS_CONFIG['Mail']['UserNameSubfix']):$_SESSION['SSV_LOGIN_EMAIL']);
  		    $this->MailPassword = $_SESSION['SSV_EMAIL_PASSWORD'];
		}
		else {
			parent::imap_socket($MailAddress,$MailPassword,$UserType);
			$this->MailAddress = $MailAddress;
			$this->MailPassword = $MailPassword;
		}
		//ad hoc flow to handle exchange server type 2
		//if ($SYS_CONFIG['Mail']['ServerType'] == "exchange" && $SYS_CONFIG['Mail']['ExchangeUsernameLogin'] && $this->CurUserAddress != "tgcestest1@esfcentre.edu.hk")
		if($SYS_CONFIG['Mail']['UsernameLoginWithoutDomain'] === true)
		{
			if(stristr($this->CurUserAddress,"@")){
	    		$this->CurUserName = substr($this->CurUserAddress, 0, strpos($this->CurUserAddress, "@")); //get the username before '@' as the login to exchange server
	    		$this->CurUserAddress = substr($this->CurUserAddress, 0, strpos($this->CurUserAddress, "@"));
			}else{
				$this->CurUserName = $this->CurUserAddress;
			}
		}
		
		$temp = explode('@',$this->CurUserAddress);
		$Domain = $temp[1];
		//$this->ExtMailFlag = ($Domain != $this->UserNameSuffix) && ($this->CurUserAddress != 'tgcestest1@esfcentre.edu.hk') && ($this->CurUserAddress != 'tgcestest2@esfcentre.edu.hk') && ($this->CurUserAddress != 'tgcestest3@esfcentre.edu.hk');
		//var_dump($this->ExtMailFlag);die;

		if (!$this->ExtMailFlag) {
			if ($this->Connect()) {
//				if ($this->UserType == 'S') {
//					$DefaultFolderList = $SYS_CONFIG['StudMail']['SystemMailBox'];
//					
//					foreach ($DefaultFolderList as $Key=> $Val) {
//						if ($Val != $SYS_CONFIG['StudMail']['FolderPrefix']) {
//					  	if (!$_SESSION['MailFolderChecked']) {
//					  		if ($this->Go_To_Folder(IMap_Encode_Folder_Name($this->RootPrefix.$Val)) === false) 
//					  			$this->Create_Folder(IMap_Encode_Folder_Name($this->RootPrefix.$Val));
//					  	}
//					  		
//					    $DefaultFolderList[$Key] = $this->RootPrefix.$Val;
//					 	}
//					 	else {
//					 		$DefaultFolderList[$Key] = $Val;
//					 	}
//					}
//				}
//				else {
					$DefaultFolderList = $SYS_CONFIG['Mail']['SystemMailBox'];
					
					foreach ($DefaultFolderList as $Key=> $Val) {
						if ($Val != $SYS_CONFIG['Mail']['FolderPrefix']) {
					  	if (!$_SESSION['MailFolderChecked']) {
					  		if ($this->Go_To_Folder(IMap_Encode_Folder_Name($this->RootPrefix.$Val)) === false) 
					  			$this->Create_Folder(IMap_Encode_Folder_Name($this->RootPrefix.$Val));
					  	}
					  		
					    $DefaultFolderList[$Key] = $this->RootPrefix.$Val;
					 	}
					 	else {
					 		$DefaultFolderList[$Key] = $Val;
					 	}
					}
//				}
				
				
				$_SESSION['MailFolderChecked'] = true;
				$this->DefaultFolderList = $DefaultFolderList;
				/*echo '<pre>';
				var_dump($this->DefaultFolderList);
				echo '</pre>';*/
				$this->ConnectionStatus = true;
			}
			else {
				$this->ConnectionStatus = false;
			}
		}
		else {
			$this->ConnectionStatus = false;
		}
		
		$this->ReplaceAttachmentFlag = true;
		//var_dump($this->DefaultFolderList);
	}
	
	// $Mode = 0 -> fast mode, 1 -> slow mode with house keeping process
	function Syn_Mail_List_By_Batch($Mode=0, $Folder="") {
		// $Mode = 1; // Debug
		
		// echo '.'.$Mode;
		if ($this->ShowTime && $this->ShowLevel == 1) 
			$this->Start_Timer();
			
		global $Lang, $SYS_CONFIG; 
		//$this->Start_Timer();
		$db = new database(true);
		$TotalMsgNumber = 0;
		$Finished = 0;
		$BatchSize = $SYS_CONFIG['Mail']['BatchSize'];
		
		if ($Folder == "")
		{
			$FolderList = $this->Get_Mail_Folders();
		}
		else
		{
			$FolderList = array
						( 0 => Array
							(
								"FolderAttribute" => "\\Marked \\HasChildren",
            					"FolderFullPath" => $Folder
            				)
						);
		}
		
		for ($i=0; $i< sizeof($FolderList); $i++) {
			
			// Array to store UIDs just got
			$CacheJustGot = array();
			
			// Detect Newest mail syn
			$Folder = $this->Go_To_Folder(IMap_Encode_Folder_Name($FolderList[$i]['FolderFullPath']));
			$MaxUID = $this->Get_Cached_UID($FolderList[$i]['FolderFullPath'], $Flag="MAX", $this->CurUserAddress);
			
			if (!is_null($MaxUID)) {
				While ($this->Get_MsgNo($MaxUID) == false) {
					$Result['DeleteMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MaxUID] = $this->Delete_Cached_Message($FolderList[$i]['FolderFullPath'], $MaxUID, $this->MailAddress);
					
					$MaxUID = $this->Get_Cached_UID($FolderList[$i]['FolderFullPath'], $Flag="MAX", $this->CurUserAddress);
					if (is_null($MaxUID)) break;
				}
			}
			// End Detect Newest mail syn
			
			$NumberOfMsg = (trim($Folder['EXISTS']) != "")? $Folder['EXISTS']:$this->Get_Check_Mail_Count_In_Folder($FolderList[$i]['FolderFullPath']);
			if (is_null($MaxUID)) {	
				$FetchStartNumber = 1;
				$FetchEndNumber = $NumberOfMsg;
			}
			else if ($MaxUID < $this->Get_UID($NumberOfMsg)) {
				$FetchStartNumber = $this->Get_MsgNo($MaxUID) + 1;
				$FetchEndNumber = $NumberOfMsg;
			}
			else {
				$FetchStartNumber = 0;
				$FetchEndNumber = -1;
			}
			$TotalMsgNumber += ($FetchEndNumber - $FetchStartNumber)+1;
			
			for($j=$FetchEndNumber; $j >= $FetchStartNumber; $j=$j-$BatchSize) {
				if (($j-$BatchSize) < $FetchStartNumber) {
					$StartMsgNo = $FetchStartNumber;
				}
				else {
					$StartMsgNo = ($j-$BatchSize)+1;
				}
				
				$MsgCache = $this->Get_Message_Cache_By_Batch($StartMsgNo,$j);
				for ($k=0;$k < sizeof($MsgCache); $k++) {
					$CacheJustGot[] = $MsgCache[$k]["UID"];
	
					$Result['GetMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MsgCache[$k]["UID"]] = $this->Insert_Cache($MsgCache[$k],$FolderList[$i]['FolderFullPath'],$this->CurUserAddress);
					
					$Finished++;
					
					if (($Finished%10) == 0) {
					echo $this->Get_Syn_Mail_Status_Msg($Finished,$TotalMsgNumber);
					flush();
					ob_flush();
					}
				}
			}
			// end syn from top
			
			// syn from bottom
			$MinUID = $this->Get_Cached_UID($FolderList[$i]['FolderFullPath'], $Flag="MIN", $this->CurUserAddress);
			
			if (!is_null($MinUID)) {
				While ($this->Get_MsgNo($MinUID) == false) {
					$Result['DeleteMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MinUID] = $this->Delete_Cached_Message($FolderList[$i]['FolderFullPath'], $MinUID, $this->MailAddress);
					
					$MinUID = $this->Get_Cached_UID($FolderList[$i]['FolderFullPath'], $Flag="MIN", $this->CurUserAddress);
					if (is_null($MinUID)) break;
				}
			}
			
			if (is_null($MinUID)) {	
				$FetchStartNumber = 1;
				$FetchEndNumber = $NumberOfMsg;
			}
			else if ($MinUID > $this->Get_UID(1)) {
				$FetchStartNumber = 1;
				$FetchEndNumber = $this->Get_MsgNo($MinUID) - 1;
			}
			else {
				$FetchStartNumber = 0;
				$FetchEndNumber = -1;
			}
			$TotalMsgNumber += ($FetchEndNumber - $FetchStartNumber)+1;
			
			
			for($j=$FetchEndNumber; $j >= $FetchStartNumber; $j=$j-$BatchSize) {
				if (($j-$BatchSize) < $FetchStartNumber) {
					$StartMsgNo = $FetchStartNumber;
				}
				else {
					$StartMsgNo = ($j-$BatchSize)+1;
				}
				
				$MsgCache = $this->Get_Message_Cache_By_Batch($StartMsgNo,$j);
				for ($k=0;$k < sizeof($MsgCache); $k++) {
					$CacheJustGot[] = $MsgCache[$k]["UID"];
					
					$Result['GetMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MsgCache[$k]["UID"]] = $this->Insert_Cache($MsgCache[$k],$FolderList[$i]['FolderFullPath'],$this->CurUserAddress);
					
					$Finished++;
					
					if (($Finished%10) == 0) {
						echo $this->Get_Syn_Mail_Status_Msg($Finished,$TotalMsgNumber);
						flush();
						ob_flush();
					}
				}
			}
			// end syn from bottom
			
			echo '| Getting mail, please wait...';
			flush();
			ob_flush();
			
			if ($Mode == "1") {
				echo '| Synchronizing Data...';
				
				flush();
				ob_flush();
				// syn mail from middle
				/*
				do {
					$this->Add_Syn_Status('true',$this->CurUserAddress);
					$CacheGap = $this->Get_Cache_Message_Gap($FolderList[$i]['FolderFullPath']);
				} while($CacheGap == false);
				
				if (sizeof($CacheGap) > 0) {
					$TotalMsgNumber += $CacheGap['TotalNumber'];
					for ($k=0; $k< sizeof($CacheGap['Pairs']); $k++) {
						for ($l=$CacheGap['Pairs'][$k][0];$l > $CacheGap['Pairs'][$k][1]; $l=$l-$BatchSize) {
							if (($l-$BatchSize) <= $CacheGap['Pairs'][$k][1]) {
								$StartMsgNo = $CacheGap['Pairs'][$k][1]+1;
							}
							else {
								$StartMsgNo = ($l-$BatchSize)+1;
							}
							
							$MsgCache = $this->Get_Message_Cache_By_Batch($StartMsgNo,$l);
							for ($z=0; $z< sizeof($MsgCache); $z++) {
								$CacheJustGot[] = $MsgCache[$z]["UID"];
								
								$Result['GetMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MsgCache[$z]["UID"]] = $this->Insert_Cache($MsgCache[$z],$FolderList[$i]['FolderFullPath'],$this->CurUserAddress);
							
								$Finished++;
							}
						}
					}
				}
				*/
				$CacheGap = $this->Get_Unchain_UID($FolderList[$i]['FolderFullPath']);
				// debug_r($CacheGap); // die;
				
				if (sizeof($CacheGap) > 0) {
					$TotalMsgNumber += $CacheGap['TotalNumber'];
					for ($k=0; $k< sizeof($CacheGap['Pairs']); $k++) {
						for ($l=$CacheGap['Pairs'][$k][0];$l <= $CacheGap['Pairs'][$k][1]; $l=$l+$BatchSize) {
							if (($l+$BatchSize) > $CacheGap['Pairs'][$k][1]) {
								$EndMsgNo = $CacheGap['Pairs'][$k][1];
							}
							else {
								$EndMsgNo = ($l+$BatchSize)-1;
							}
							
							$MsgCache = $this->Get_Message_Cache_By_Batch($l,$EndMsgNo);
							for ($z=0; $z< sizeof($MsgCache); $z++) {
								$CacheJustGot[] = $MsgCache[$z]["UID"];
								
								$Result['GetMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MsgCache[$z]["UID"]] = $this->Insert_Cache($MsgCache[$z],$FolderList[$i]['FolderFullPath'],$this->CurUserAddress);
							
								$Finished++;
							}
						}
					}
				}
				// end syn from middle
			}
			
			// clear duplicate mail cache record
			// disabled on 28/10/2008, 
			//coz the duplicate mail problem is fixed by adding primary key to cach table
			/*echo '| Clearing unwanted data...';
			flush();
			ob_flush();
			$this->Delete_Duplicate_MailRecord($FolderList[$i]['FolderFullPath']);*/
	
			if ($Mode == "1") {
				// check if some old mail in cache that deleted out side
				$CacheMailCount = $this->Get_Mail_Count($FolderList[$i]['FolderFullPath']);
				$RemoteMailCount = $NumberOfMsg;
				// echo 'CacheMailCount : '.$CacheMailCount."\n";
				// echo 'RemoteMailCount : '.$RemoteMailCount."\n";
				if ($CacheMailCount > $RemoteMailCount) {
					
					$ExpiredUID = $this->Get_Expired_UID($MinUID,$MaxUID);
					$Result['DeleteMail'.$FolderList[$i]['FolderFullPath'].'-ExpiredUIDs'] = $this->Delete_Cached_Message($FolderList[$i]['FolderFullPath'], $ExpiredUID, $this->MailAddress);
					// comment on 25/9/2008 as it will slow down performance
					/*$Difference = $CacheMailCount - $RemoteMailCount;
					$Size = 500;
					$k=1;
					for($l=1; ($k <= $Difference && $l < $CacheMailCount); $l+=$Size) {
						$this->Add_Syn_Status('true',$this->CurUserAddress);
						$UIDList = $this->Get_Cache_For_Syn($FolderList[$i]['FolderFullPath'],$l,$Size);
						for ($m=0; $m < sizeof($UIDList); $m++) {
							if (!in_array($UIDList[$m]['UID'],$CacheJustGot)) {
								if ($this->Get_MsgNo($UIDList[$m]['UID']) == false) {
									$Result['DeleteMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$UIDList[$m]['UID']] = $this->Delete_Cached_Message($FolderList[$i]['FolderFullPath'], $UIDList[$m]['UID'], $this->MailAddress);
									$k++;
								}
							}
						}
					}*/
				}
				// end syn in middle
				
				// record last full syn time
				$this->Add_Syn_Status('true',$this->CurUserAddress,$FolderList[$i]['FolderFullPath']);
			}
			
			echo '| Getting mail, please wait...';
			flush();
			ob_flush();
		}	
		//echo $this->Stop_Timer();
		
		if ($this->ShowTime && $this->ShowLevel == 1) { 
			if ($this->Silent)
				Write_Message_Log("Log",'Cache: Time Different (Sync Mail): '.$this->Stop_Timer());
			else
				echo 'Time Different (Sync Mail): '.$this->Stop_Timer().'<br>';
		}
	}
	
	// $Mode = 0 -> fast mode, 1 -> slow mode
	/*function Syn_Mail_List($Mode=0, $Folder="") {
		global $Lang; 

		$db = new database(true);
		$TotalMsgNumber = 0;
		$Finished = 0;

		if ($Folder == "")
		{
			$FolderList = $this->Get_Mail_Folders();
		}
		else
		{
			$FolderList = array
						( 0 => Array
							(
								"FolderAttribute" => "\\Marked \\HasChildren",
            					"FolderFullPath" => $Folder
            				)
						);
		}

		$this->Add_Syn_Status('true',$this->CurUserAddress);
		for ($i=0; $i< sizeof($FolderList); $i++) {
			// Array to store UIDs just got
			$CacheJustGot = array();
			
			// Detect Newest mail syn
			$Folder = $this->Go_To_Folder(IMap_Encode_Folder_Name($FolderList[$i]['FolderFullPath']));
			$MaxUID = $this->Get_Cached_UID($FolderList[$i]['FolderFullPath'], $Flag="MAX", $this->CurUserAddress);
			
			if (!is_null($MaxUID)) {
				While ($this->Get_MsgNo($MaxUID) == false) {
					$this->Add_Syn_Status('true',$this->CurUserAddress);
					$Result['DeleteMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MaxUID] = $this->Delete_Cached_Message($FolderList[$i]['FolderFullPath'], $MaxUID, $this->MailAddress);
					
					$MaxUID = $this->Get_Cached_UID($FolderList[$i]['FolderFullPath'], $Flag="MAX", $this->CurUserAddress);
					if (is_null($MaxUID)) break;
				}
			}
			// End Detect Newest mail syn
			
			$NumberOfMsg = $Folder['EXISTS'];
			if (is_null($MaxUID)) {	
				$FetchStartNumber = 1;
				$FetchEndNumber = $NumberOfMsg;
			}
			else if ($MaxUID < $this->Get_UID($NumberOfMsg)) {
				$FetchStartNumber = $this->Get_MsgNo($MaxUID) + 1;
				$FetchEndNumber = $NumberOfMsg;
			}
			else {
				$FetchStartNumber = 0;
				$FetchEndNumber = -1;
			}
			$TotalMsgNumber += ($FetchEndNumber - $FetchStartNumber)+1;
			
			for($j=$FetchEndNumber; $j >= $FetchStartNumber; $j--) {
				$MsgCache = $this->Get_Message_Cache($j);
				$CacheJustGot[] = $MsgCache["UID"];

				$this->Add_Syn_Status('true',$this->CurUserAddress);
				$Result['GetMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MsgCache["UID"]] = $this->Insert_Cache($MsgCache,$FolderList[$i]['FolderFullPath'],$this->CurUserAddress);
				
				$Finished++;
				
				if (($Finished%10) == 0) {
					echo $this->Get_Syn_Mail_Status_Msg($Finished,$TotalMsgNumber);
					flush();
					ob_flush();
				}
			}
			// end syn from top
			
			// syn from bottom
			$MinUID = $this->Get_Cached_UID($FolderList[$i]['FolderFullPath'], $Flag="MIN", $this->CurUserAddress);
			
			if (!is_null($MinUID)) {
				While ($this->Get_MsgNo($MinUID) == false) {
					$Result['DeleteMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MinUID] = $this->Delete_Cached_Message($FolderList[$i]['FolderFullPath'], $MinUID, $this->MailAddress);
					
					$MinUID = $this->Get_Cached_UID($FolderList[$i]['FolderFullPath'], $Flag="MIN", $this->CurUserAddress);
					if (is_null($MinUID)) break;
				}
			}
			
			if (is_null($MinUID)) {	
				$FetchStartNumber = 1;
				$FetchEndNumber = $NumberOfMsg;
			}
			else if ($MinUID > $this->Get_UID(1)) {
				$FetchStartNumber = 1;
				$FetchEndNumber = $this->Get_MsgNo($MinUID) - 1;
			}
			else {
				$FetchStartNumber = 0;
				$FetchEndNumber = -1;
			}
			$TotalMsgNumber += ($FetchEndNumber - $FetchStartNumber)+1;
			
			
			for($j=$FetchEndNumber; $j >= $FetchStartNumber; $j--) {
				$MsgCache = $this->Get_Message_Cache($j);
				$CacheJustGot[] = $MsgCache["UID"];
				
				$this->Add_Syn_Status('true',$this->CurUserAddress);
				$Result['GetMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MsgCache["UID"]] = $this->Insert_Cache($MsgCache,$FolderList[$i]['FolderFullPath'],$this->CurUserAddress);
				
				$Finished++;
				
				if (($Finished%10) == 0) {
					echo $this->Get_Syn_Mail_Status_Msg($Finished,$TotalMsgNumber);
					flush();
					ob_flush();
				}
			}
			// end syn from bottom
			
			echo '| ';
			flush();
			ob_flush();
			
			if ($Mode == 1) {
				// syn mail from middle
				do {
					$this->Add_Syn_Status('true',$this->CurUserAddress);
					$CacheGap = $this->Get_Cache_Message_Gap($FolderList[$i]['FolderFullPath']);
				} while($CacheGap == false);
				
				//var_dump($CacheGap); die;
				if (sizeof($CacheGap) > 0) {
					$TotalMsgNumber += $CacheGap['TotalNumber'];
					for ($k=0; $k< sizeof($CacheGap['Pairs']); $k++) {
						for ($l=$CacheGap['Pairs'][$k][0];$l > $CacheGap['Pairs'][$k][1]; $l--) {
							$MsgCache = $this->Get_Message_Cache($l);
							$CacheJustGot[] = $MsgCache["UID"];
							
							$Result['GetMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$MsgCache["UID"]] = $this->Insert_Cache($MsgCache,$FolderList[$i]['FolderFullPath'],$this->CurUserAddress);
							
							$Finished++;
						}
					}
				}
	
				// check if some old mail in cache that deleted out side
				$CacheMailCount = $this->Get_Mail_Count($FolderList[$i]['FolderFullPath']);
				$RemoteMailCount = $NumberOfMsg;
				
				if ($CacheMailCount > $RemoteMailCount) {
					$Difference = $CacheMailCount - $RemoteMailCount;
					$Size = 500;
					$k=1;
					for($l=1; ($k <= $Difference && $l < $CacheMailCount); $l+=$Size) {
						$this->Add_Syn_Status('true',$this->CurUserAddress);
						$UIDList = $this->Get_Cache_For_Syn($FolderList[$i]['FolderFullPath'],$l,$Size);
						for ($m=0; $m < sizeof($UIDList); $m++) {
							if (!in_array($UIDList[$m]['UID'],$CacheJustGot)) {
								if ($this->Get_MsgNo($UIDList[$m]['UID']) == false) {
									$Result['DeleteMail'.$FolderList[$i]['FolderFullPath'].'-UID:'.$UIDList[$m]['UID']] = $this->Delete_Cached_Message($FolderList[$i]['FolderFullPath'], $UIDList[$m]['UID'], $this->MailAddress);
									$k++;
								}
							}
						}
					}
				}
				// end syn in middle
			}
		}	

		$this->Add_Syn_Status('false',$this->CurUserAddress);
	}*/
	
	function Get_Cached_UID($Folder, $Flag="MAX", $MailAddress) {
		// if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
		$ldb = new database(true);
		
		$sql = 'Select 
							 	'.$Flag.'(MessageUID) 
						From 
							MAIL_LOCAL_LIST 
						With 
							(NOLOCK)
						Where 
							CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Folder).' 
							And 
							EmailAddress = \''.$this->MailAddress.'\'
					 ';
		// echo $sql;
		$Result = $ldb->returnVector($sql);
		
		if ($this->ShowTime && $this->ShowLevel == 0) { 
			if ($this->Silent)
				Write_Message_Log("Log",'Cache: Time Different (Get Min/Max UID in Cache): '.$this->Stop_Timer());
			else
				echo 'Time Different (Get Min/Max UID in Cache): '.$this->Stop_Timer().'<br>';
		} else {
			Write_Message_Log("Log",'Cache: Time Different (Get Min/Max UID in Cache): '.$this->Stop_Timer());
		}
    
		return $Result[0];
	}
	
	function Delete_Cached_Message($Folder, $UID, $MailAddress) {
		//if ($this->ShowTime && $this->ShowLevel == 0) 
		//	$this->Start_Timer();
			
		$ldb = new database(true);
		
		if ($UID !== "") {
			$UIDArr = explode(',',$UID);
			
				$BlockUIDArr = array();
				$BlockUIDIdx = 0;
				foreach($UIDArr as $Key => $Value) {
					$BlockUIDArr[$BlockUIDIdx][] = $Value;
					if ($Key > 0 && $Key %1000 == 0) {
						$BlockUIDIdx ++;
					}
				}
			
			
			for ($i=0; $i <= $BlockUIDIdx; $i++) {
				$BlockUID = implode(',',$BlockUIDArr[$i]);
				$this->Start_Timer();
				$sql = 'Delete From MAIL_LOCAL_LIST 
								Where 
									EmailAddress = \''.$this->MailAddress.'\' 
									AND 
									CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Folder).' 
									AND 
									MessageUID in ('.$BlockUID.') 
							 ';
							 
				// echo $sql;
				Write_Message_Log("Log","Delete cache record of UID \"$BlockUID\" in folder $Folder - Triggered By Delete_Cache_Message");
					
				if ($this->ShowTime && $this->ShowLevel == 0) { 
					if ($this->Silent)
						Write_Message_Log("Log",'Cache: Time Different (Delete Cached Msg): '.$this->Stop_Timer());
					else
						echo 'Time Different (Delete Cached Msg): '.$this->Stop_Timer().'<br>';
				} else {
					Write_Message_Log("Log",'Cache: Time Different (Delete Cached Msg): '.$this->Stop_Timer());
				}
				return $ldb->db_db_query($sql);
			}
			
		}	
		else {
			if ($this->ShowTime && $this->ShowLevel == 0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Cache: Time Different (Delete Cached Msg): '.$this->Stop_Timer());
	    	else
	    		echo 'Time Different (Delete Cached Msg): '.$this->Stop_Timer().'<br>';
	    }
			return true;
		}
	}
	
	function Insert_Cache($MsgCache,$Folder,$MailAddress="") {
		if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
		if ($MailAddress == "") $MailAddress = $this->MailAddress;
		$ldb = new database(true);
		
		if ($MsgCache["UID"] != "" && $MsgCache["UID"] != 0) {
			$ldb = new database(true);
						
			$sql = 'if not exists (select 1 from MAIL_LOCAL_LIST where MessageUID = '.$MsgCache["UID"].'
					and MailFolder = '.UTF8_To_DB_Handler($Folder).'
					and EmailAddress = \''.$this->MailAddress.'\') 
					Insert Into MAIL_LOCAL_LIST 
								(
								MessageUID,
								MailFolder,
								EmailAddress,
								Size,
								Date,
								HeaderFrom,
								HeaderTo,
								HeaderCc,
								HeaderBcc,
								IsImportant,
								HasAttachment,
								Subject,
								CreateDate,
								ForwardFlag, 
								MessageBoundary,
								SortFrom,
								SortTo,
								MessageStructure
								)
							Values 
								(
								'.$MsgCache["UID"].', 
								'.UTF8_To_DB_Handler($Folder).', 
								\''.$this->MailAddress.'\', 
								\''.$MsgCache["RFC822.SIZE"].'\', 
								\''.$MsgCache["Date"].'\', 
								'.UTF8_To_DB_Handler($MsgCache["From"]).', 
								'.UTF8_To_DB_Handler($MsgCache["To"]).', 
								'.UTF8_To_DB_Handler($MsgCache["Cc"]).', 
								'.UTF8_To_DB_Handler($MsgCache["Bcc"]).', 
								\''.$MsgCache['ImportantFlag'].'\', 
								\''.$MsgCache['HaveAttachment'].'\', 
								'.UTF8_To_DB_Handler($MsgCache["Subject"]).', 
								GetDate(), 
								\'false\', 
								\''.$ldb->Get_Safe_Sql_Query($MsgCache['Boundary']).'\',
								'.UTF8_To_DB_Handler($MsgCache["SortFrom"]).',
								'.UTF8_To_DB_Handler($MsgCache["SortTo"]).',
								\''.$ldb->Get_Safe_Sql_Query($MsgCache['MessageStructure']).'\'
								)';
			//echo $sql.'<br>'; die;
			$Result = $ldb->db_db_query($sql);
			Write_Message_Log("Log","Cache \"".$MsgCache["UID"]."\" of \"".$Folder."\" insert to db");
		}
		
		if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (insert Cache): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (insert Cache): '.$this->Stop_Timer().'<br>';
    }
		return $Result;
	}
	
	function Get_Mail_Table_Data($folderName, $PageNumber, $PageSize, $sort="DateSort", $reverse=0, $Keyword="") {
		global $Lang, $SYS_CONFIG;
		
		//$Socket = new imap_socket();
		//$Socket->Connect();
		$Folder = $this->Go_To_Folder(IMap_Encode_Folder_Name($folderName));
		
		if ($MailAddress == "") $MailAddress = $this->MailAddress;
		
		$ldb = new database(true);
			
		if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
		$sql = 'exec Get_Mail_Cache '.$PageSize.', '.$PageNumber.', \''.$MailAddress.'\', '.UTF8_To_DB_Handler($folderName).', \''.$sort.'\', '.$reverse.', '.UTF8_To_DB_Handler($Keyword);
		// echo $sql.'<br>';
		$Result = $ldb->returnArray($sql);
		if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Get Mailtable Cache): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Get Mailtable Cache): '.$this->Stop_Timer().'<br>';
    }
		
		// not searching
		if (trim($Keyword) == "") {
			for ($i=0; $i< sizeof($Result); $i++) {
				if ($i==0) 
					$UIDList = $Result[$i]['MessageUID'];
				else 
					$UIDList .= ",".$Result[$i]['MessageUID'];
			}
			
			$Folder = UTF8_From_DB_Handler($Result[0]['MailFolder']);
			$Flags = $this->Get_Message_Status($UIDList,IMap_Encode_Folder_Name($Folder),false);
		}
		
		for ($i=0; $i< sizeof($Result); $i++) {
			$Result[$i]['MailFolder'] = UTF8_From_DB_Handler($Result[$i]['MailFolder']);
			// in search
			if (trim($Keyword) != "") {
				if ($Result[$i]['MailFolder'] != $LastFolder) {
					$Flags = $this->Get_Message_Status($Result[$i]['MessageUID'],IMap_Encode_Folder_Name($Result[$i]['MailFolder']),true);
				}
				else {
					$Flags = $this->Get_Message_Status($Result[$i]['MessageUID'],IMap_Encode_Folder_Name($Result[$i]['MailFolder']),false);
				}
				//var_dump($Flags);
				if (!is_array($Flags)) {
					$this->Delete_Cached_Message($Result[$i]['MailFolder'], $Result[$i]['MessageUID'], $MailAddress);
					$Result[$i]['deleted'] = true;
				}
				else {
					$Result[$i] = array_merge($Result[$i],$Flags);
					if ($Result[$i]['HasAttachment'] && !$Result[$i]['ReadFlag'] && !$Result[$i]['Checked']) {
						$Temp = $this->Get_Message_Structure($Result[$i]['MessageUID'],IMap_Encode_Folder_Name($Result[$i]['MailFolder']),$Result[$i]['MessageStructure'],false);
						$Result[$i]['HasAttachment'] = ($this->Check_Attach_Exists($Temp));
						$TempInt = ($Result[$i]['HasAttachment'])? 1:0;
						$sql = 'Update MAIL_LOCAL_LIST Set 
											HasAttachment = '.$TempInt.', 
											LastModified = GetDate()
										where 
											EmailAddress = \''.$MailAddress.'\' 
											AND 
											CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Result[$i]['MailFolder']).' 
											AND 
											MessageUID = '.$Result[$i]['MessageUID'];
						$ldb->db_db_query($sql);
					}
				}
			}
			// not search
			else {
				if (!is_array($Flags[$Result[$i]['MessageUID']])) {
					$this->Delete_Cached_Message($Result[$i]['MailFolder'], $Result[$i]['MessageUID'], $MailAddress);
					$Result[$i]['deleted'] = true;
				}
				else {
					$Result[$i] = array_merge($Result[$i],$Flags[$Result[$i]['MessageUID']]);
					if ($Result[$i]['HasAttachment'] && !$Result[$i]['ReadFlag'] && !$Result[$i]['Checked']) {
						$Temp = $this->Get_Message_Structure($Result[$i]['MessageUID'],IMap_Encode_Folder_Name($Result[$i]['MailFolder']),$Result[$i]['MessageStructure'],false);
						$Result[$i]['HasAttachment'] = ($this->Check_Attach_Exists($Temp));
						$TempInt = ($Result[$i]['HasAttachment'])? 1:0;
						$sql = 'Update MAIL_LOCAL_LIST Set 
											HasAttachment = '.$TempInt.', 
											LastModified = GetDate()
										where 
											EmailAddress = \''.$MailAddress.'\' 
											AND 
											CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Result[$i]['MailFolder']).' 
											AND 
											MessageUID = '.$Result[$i]['MessageUID'];
						$ldb->db_db_query($sql);
					}
				}
			}
			
			$FromList = UTF8_From_DB_Handler($Result[$i]['HeaderFrom']);
			/*preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", trim($FromList), $matches, PREG_OFFSET_CAPTURE);
			$Result[$i]['HeaderFrom'] = ($matches[0][0][1] == 0)? $matches[0][0][0]:trim(substr($FromList,0,$matches[0][0][1]-1));*/
			$FromList = imap_rfc822_parse_adrlist($FromList,"");
			if (trim($FromList[0]->personal) != "") {
				$Result[$i]['HeaderFrom'] = $FromList[0]->personal; 
			}
			else {
				$Host = ('.SYNTAX-ERROR.' != $FromList[0]->host)? "@".$FromList[0]->host:"";
				
				$Result[$i]['HeaderFrom'] = $FromList[0]->mailbox.$Host;
			}
			if (sizeof($FromList) > 1) $Result[$i]['HeaderFrom'] .= '...';
			
			//unset($matches);
			$ToList = UTF8_From_DB_Handler($Result[$i]['HeaderTo']);
			/*preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", trim($ToList), $matches, PREG_OFFSET_CAPTURE);
			
			if (sizeof($matches[0]) > 1) {
				$Result[$i]['HeaderTo'] = ($matches[0][0][1] == 0)? $matches[0][0][0]:trim(substr($ToList,0,$matches[0][0][1]-1));
				$Result[$i]['HeaderTo'] .= '...';
			}
			else if (sizeof($matches[0]) == 0) 
				$Result[$i]['HeaderTo'] = $Lang['email']['NoReceiver'];
			else
				$Result[$i]['HeaderTo'] = ($matches[0][0][1] == 0)? $matches[0][0][0]:trim(substr($ToList,0,$matches[0][0][1]-1));*/
			$ToList = imap_rfc822_parse_adrlist($ToList,"");
			if (sizeof($ToList) > 0) {
				if (trim($ToList[0]->personal) != "") {
					$Result[$i]['HeaderTo'] = $ToList[0]->personal; 
				}
				else {
					$Host = ('.SYNTAX-ERROR.' != $ToList[0]->host)? "@".$ToList[0]->host:"";
					
					$Result[$i]['HeaderTo'] = $ToList[0]->mailbox.$Host;
				}
				if (sizeof($ToList) > 1) $Result[$i]['HeaderTo'] .= '...';
			}
			else $Result[$i]['HeaderTo'] = $Lang['email']['NoReceiver'];

			$Result[$i]['HeaderCc'] = UTF8_From_DB_Handler($Result[$i]['HeaderCc']);
			$Result[$i]['HeaderBcc'] = UTF8_From_DB_Handler($Result[$i]['HeaderBcc']);
			$Result[$i]['Subject'] = UTF8_From_DB_Handler($Result[$i]['Subject']);
			$LastFolder = $Result[$i]['MailFolder'];
		}
		/*echo '<pre>';
		var_dump($Result);
		echo '</pre>'; */
		//$Socket->Close_Connect();
		return $Result;
	}
	
	function Get_Mail_Count($FolderName,$Keyword="") {
		if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
		$ldb = new database(true);
			
		$sql = 'exec Get_Mail_Cache_Count \''.$this->MailAddress.'\', '.UTF8_To_DB_Handler($FolderName).', '.UTF8_To_DB_Handler($Keyword);
		//echo $sql.'<br>';
		$Result = $ldb->returnVector($sql);
		
		if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Get cache count): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Get cache count): '.$this->Stop_Timer().'<br>';
    }
		return $Result[0];
	}
	
	// disabled function: on 22/10/2008 by Kenneth Chung
	function Get_Cache_For_Syn($FolderName,$StartFrom=1,$SearchSize=500) {
		/*$ldb = new database(true);
		
		$sql = 'exec Get_Cache_To_Check '.$StartFrom.', '.$SearchSize.', \''.$this->MailAddress.'\', '.UTF8_To_DB_Handler($FolderName);
		echo $sql.'<br>';
		$Result = $ldb->returnArray($sql);
		
		return $Result;*/
	}
  
  function Get_Mail_Display_Folder($FullPath) {
  	global $SYS_CONFIG;
    if (strrpos($FullPath,$this->FolderDelimiter) !== false) {
    	$FolderDisplayName = substr($FullPath,strrpos($FullPath,$this->FolderDelimiter)+1, strlen($FullPath)-1);
    }
    else {
      $FolderDisplayName = $FullPath;
    }
    return $FolderDisplayName;
	}
  
  function Add_Syn_Status($Status,$MailAddress,$MailFolder) {
  	if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
  	$ldb = new database(true);
  	
  	$sql = 'Select 
  						count(1) 
  					From 
  						MAIL_SYN_STATUS 
  					With 
  						(NOLOCK) 
  					Where 
  						MailAddress = \''.$MailAddress.'\'
  						And 
  						CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($MailFolder);

  	$result = $ldb->returnVector($sql);
  	
  	if ($result[0] > 0) {
  		$sql = 'Update MAIL_SYN_STATUS 
  						Set 
  							SynStatus = \''.$Status.'\', 
  							LastSynDate = '.time().'
  						Where 
  							MailAddress = \''.$MailAddress.'\'
  							And 
  							CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($MailFolder);
  	}
  	else {
  		$sql = 'Insert into MAIL_SYN_STATUS 
  						(MailAddress, SynStatus, LastSynDate, MailFolder) 
  						Values 
  						(\''.$MailAddress.'\',\''.$Status.'\','.time().','.UTF8_To_DB_Handler($MailFolder).')';
  	}
  	$result = $ldb->db_db_query($sql);
  	
  	if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Set Sync Status): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Set Sync Status): '.$this->Stop_Timer().'<br>';
    }
  	//Write_Message_Log("Log","Full s set to ".$Status);
  	return $result;
  }
  
  function Check_Syn_Status($MailAddress,$MailFolder) {
  	global $SYS_CONFIG;
  	
  	if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
  	
  	$ldb = new database(true);
  	
  	$sql = 'Select 
  						count(1) 
  					From 
  						MAIL_SYN_STATUS 
  					With 
  						(NOLOCK)
  					Where 
  						MailAddress = \''.$MailAddress.'\' 
  						And 
  						CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($MailFolder);
  	$result = $ldb->returnVector($sql);
  	
  	if ($result[0] == 0) {
  		$sql = 'Insert into MAIL_SYN_STATUS 
  						(MailAddress, SynStatus, LastSynDate, MailFolder) 
  						Values 
  						(\''.$MailAddress.'\',\'false\','.time().','.UTF8_To_DB_Handler($MailFolder).')';
  						
  		$result = $ldb->db_db_query($sql);
  	}
		
		$sql = 'Select 
  						LastSynDate
  					From 
  						MAIL_SYN_STATUS 
  					With 
  						(NOLOCK) 
  					Where 
  						MailAddress = \''.$MailAddress.'\'
  						And 
  						CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($MailFolder);
  	$result = $ldb->returnArray($sql);

		Write_Message_Log("Log","Check Sync Status");
		if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Check Sync Status): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Check Sync Status): '.$this->Stop_Timer().'<br>';
    }
    
		if ((time() - $result[0][1]) > 60*$SYS_CONFIG['Mail']['SynMailTimeChecker']) {
			return "1"; // Full sync
		}
		else {
			return "0"; // Fast sync
		}
  }
  
  function Move_Mail_Between_Folder($FromFolder,$UID,$ToFolder,$NeedGoTo=true) {
  	//$Socket = new imap_socket();
  	//$Socket->Connect();
  	
  	$EncodeFrom = IMap_Encode_Folder_Name($FromFolder);
  	$EncodeTo = IMap_Encode_Folder_Name($ToFolder);
  	//echo '11';die;
  	$Result['MoveInIMap'] = $this->Move_Mail($EncodeFrom,$EncodeTo,$UID,$NeedGoTo);
  	//var_dump($Result['MoveInIMap']); die;
  	if ($Result['MoveInIMap'] !== false) {
  		$db = new database(true);
	  	
	  	if ($this->ServerType == 'gmail') {
		  	$sql = 'delete from MAIL_LOCAL_LIST where 
		  							MessageUID in ('.$UID.')  
			  						AND 
			  						CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($FromFolder).' 
			  						AND 
			  						EmailAddress = \''.$this->MailAddress.'\'';
			  $result["CacheDelete"] = $db->db_db_query($sql);
			}
			else {
				if (stristr($Result['MoveInIMap'],":") !== FALSE) {
		  		list($StartUID,$EndUID) = explode(":",$Result['MoveInIMap']);
		  	}
		  	else {
		  		$StartUID = $EndUID = $Result['MoveInIMap'];
		  	}
		  	
		  	$k = 0;
		  	$UID = explode(",",$UID);
		  	
		  	if ($this->ServerType == 'exchange')
		  		sort($UID,SORT_NUMERIC);
		  		
		  	if ($this->ShowTime && $this->ShowLevel == 0) 
					$this->Start_Timer();
		  	for ($i=$StartUID; $i<= $EndUID; $i++) { 
			  	$sql = 'Update MAIL_LOCAL_LIST 
			  					Set 
			  						MailFolder = '.UTF8_To_DB_Handler($ToFolder).', 
			  						MessageUID = \''.$i.'\' 
			  					Where 
			  						MessageUID = \''.$UID[$k].'\' 
			  						AND 
			  						CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($FromFolder).' 
			  						AND 
			  						EmailAddress = \''.$this->MailAddress.'\'';
			  	//echo $sql.'<br>';
			  	$result["CacheDelete$i"] = $db->db_db_query($sql);
			  	$k++;
			  }
			}

			if ($this->ShowTime && $this->ShowLevel == 0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Cache: Time Different (Move Mail): '.$this->Stop_Timer());
	    	else
	    		echo 'Time Different (Move Mail): '.$this->Stop_Timer().'<br>';
	    }
	  	//$Socket->Close_Connect();
	  	return $result;
	  }
	  else {
	  	//$Socket->Close_Connect();
	  	return false;
	  }
	}
  
  function Save_Draft_Copy($Message,$ToCache) {
  	$db = new database(true);
  	//$Socket = new imap_socket();
  	//$Socket->Connect();
  	$Folder = IMap_Encode_Folder_Name($this->DefaultFolderList["DraftFolder"]);
  	$Result = $this->Append_Msg_List($Message,$Folder,'\Draft');
  	
  	if ($Result !== false) {
  		/*$ToCache["UID"] = $Result;
  		$ToCache["RFC822.SIZE"] = strlen($Message);
  		$ToCache["Date"] = time();
  		$ToCache["Seen"] = '0';
  		
  		$ReturnStatus = $this->Insert_Cache($ToCache,$this->DefaultFolderList["DraftFolder"],$this->MailAddress);*/
  		
  		/*if ($ReturnStatus) return $ToCache["UID"];
  		else  return false;*/
  		//return $ToCache["UID"];
  	}
  	//$Socket->Close_Connect();
  	
  	return $Result;
  }
  
  function Save_Sent_Copy($Message,$ToCache) {
  	$db = new database(true);
  	//$Socket = new imap_socket();
  	//$Socket->Connect();
  	$Folder = IMap_Encode_Folder_Name($this->DefaultFolderList["SentFolder"]);
  	$Result = $this->Append_Msg_List($Message,$Folder,'\Seen');
  	
  	if ($Result !== false) {
  		/*$ToCache["UID"] = $Result;
  		$ToCache["RFC822.SIZE"] = strlen($Message);
  		$ToCache["Date"] = time();
  		$ToCache["Seen"] = '1';
  		
  		$ReturnStatus = $this->Insert_Cache($ToCache,$this->DefaultFolderList["SentFolder"],$this->MailAddress);*/
  		
  		return true;
  	}
  	//$Socket->Close_Connect();
  	
  	return $Result;
  }
  
  function Set_Answered_Flag($Folder,$UID) {
  	//$Socket = new imap_socket();
  	//$Socket->Connect();
  	$EncodedFolder = IMap_Encode_Folder_Name($Folder);
  	$Result = $this->Set_Msg_Flags('\Answered',$UID,$EncodedFolder);
  	//$Socket->Close_Connect();
  	
  	return $Result;
  }
  
   function Set_Fowarded_Flag($Folder,$UID) {
   	
  	$EncodedFolder = IMap_Encode_Folder_Name($Folder);
  	$Result = $this->Set_Msg_Flags('Forwarded',$UID,$EncodedFolder);
  	
  	return $Result;
  }
  
  function Delete_Mail($Folder, $UID, $NeedGoTo=true, $HardDelete=false) {
    //$Socket = new imap_socket();
  	//$Socket->Connect();
	$EncodedFolder = IMap_Encode_Folder_Name($Folder);

	if ($Folder == $this->DefaultFolderList['TrashFolder'] || $Folder == $this->DefaultFolderList['SpamFolder'] || $HardDelete) {
		$Result['DeleteIMap'] = $this->Delete_Server_Mail($EncodedFolder, $UID, $NeedGoTo);
		
		if ($Result['DeleteIMap']) {
			$Result['DeleteCache'] = $this->Delete_Cached_Message($Folder, $UID, $this->MailAddress);
		}
		$ReturnVal = (!in_array(false,$Result));
	}
	else 
		$ReturnVal = $this->Move_Mail_Between_Folder($Folder,$UID,$this->DefaultFolderList['TrashFolder'],$NeedGoTo);

		//$Socket->Close_Connect();
	return $ReturnVal;
	}
	
	function Get_Folder_List_In_Path($MailFolder='') {
		global $SYS_CONFIG;
		if ($MailFolder == "") $MailFolder = $this->FolderPrefix;
		
		if ($MailFolder != "") 
			$MotherLevel = substr_count($MailFolder,$this->FolderDelimiter);
		else
			$MotherLevel = -1;

		if ($MotherLevel > 0) {
			$MailFolder = substr($MailFolder,0,strripos($MailFolder,$this->FolderDelimiter));
			$MotherLevel = substr_count($MailFolder,$this->FolderDelimiter);
		}
		else if ($MotherLevel == 0) {
			$MailFolder = $this->FolderPrefix;
		}
		$FolderList = $this->Get_Mail_Folders();
		$result = array();
		
		/*echo '<div style="visibility:hidden; display:none;">';
		echo '<pre>';
		var_dump($FolderList);
		echo '</pre>';
		echo '</div>';*/
		//echo $MotherLevel.'<br>';
		
		if ($MailFolder == "") {
			for ($i=0; $i< sizeof($FolderList); $i++) {
				if (!in_array($FolderList[$i]["FolderFullPath"],$this->DefaultFolderList) 
						&& $FolderList[$i]['FolderFullPath'] != $MailFolder 
						&& (substr_count($FolderList[$i]['FolderFullPath'],$this->FolderDelimiter) - $MotherLevel) <= 2) {
					$result[] = $FolderList[$i];
				}
			}
		}
		else {
			for ($i=0; $i< sizeof($FolderList); $i++) {
				if (stristr($FolderList[$i]['FolderFullPath'],$MailFolder.$this->FolderDelimiter) !== false 
						&& !in_array($FolderList[$i]["FolderFullPath"],$this->DefaultFolderList) 
						&& $FolderList[$i]['FolderFullPath'] != $MailFolder 
						&& (substr_count($FolderList[$i]['FolderFullPath'],$this->FolderDelimiter) - $MotherLevel) <= 2) {
					$result[] = $FolderList[$i];
				}
			}
		}
		
		//sort($result,SORT_STRING);
		//usort($result,"To_Lower_Case_Folder_Compare");
		
		/*echo '<div style="visibility:hidden; display:none;">';
		echo $MotherLevel.'<br>';
		echo '<pre>';
		var_dump($result);
		echo '</pre>';
		echo '</div>';*/
		
		return $result;
	}
	
	// disabled function, on 22/10/2008 by kenneth chung
	function Get_All_Unseen_Mail_Number()
	{
		/*$SelectSql = "SELECT CAST(CAST(MailFolder AS  varbinary(8000)) AS image) as MailFolder, Count(*) FROM MAIL_LOCAL_LIST With (NoLock) ";
		$SelectSql .= "WHERE EmailAddress = '" . $this->MailAddress . "' AND ReadFlag = 0 ";
		$SelectSql .= "Group By CAST(MailFolder AS  varbinary(8000))";
		$db = new database(true);*/
		//echo $SelectSql;
		//$result = $db->returnArray($SelectSql);
		/*echo '<pre>';
		var_dump($result);
		echo '</pre>';*/
		/*return $result;*/
	}
	
	function Get_Unseen_Mail_Number($Folder) {
		//read from DB instead of read from mail server
		/*$SelectSql = "SELECT Count(*) FROM MAIL_LOCAL_LIST With (NoLock) ";
		$SelectSql .= "WHERE EmailAddress = '" . $this->MailAddress . "' AND ";
		$SelectSql .= "CAST(MailFolder AS varbinary(8000)) = " . UTF8_To_DB_Handler($Folder) . " AND ";
		$SelectSql .= "ReadFlag = 0";
		$db = new database(true);
		$result = $db->returnVector($SelectSql);
		//echo $result.'<br>';
		return $result[0];*/
		$EncodedFolder = IMap_Encode_Folder_Name($Folder);
		
		return $this->Get_Unseen_In_Folder($EncodedFolder);
	}
	
	function Get_Prev_Next_UID($FolderName, $Sort="", $Reverse=0, $CurUID) {
		if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
		$db = new database(true);
		
		if (trim($Sort) == "") $Sort = "Date";
		if (trim($Sort) == "MailFolder") $Sort = "Date";
		
		if (in_array($Sort, array("Subject","SortFrom","SortTo"))) 
			$CurColumn = 'CAST(CAST('.$Sort.' AS  varbinary(8000)) AS image) as '.$Sort;
		else {
			$CurColumn = $Sort;
		}
			
		$GetCurValueSql = 'Select 
												'.$CurColumn.' 
											From 
												MAIL_LOCAL_LIST 
											With 
												(NOLOCK)
											Where 
												EmailAddress = \''.$this->MailAddress.'\' 
												AND 
												CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($FolderName).' 
												AND 
												MessageUID = \''.$CurUID.'\'';
		$CurSortValue = $db->returnVector($GetCurValueSql);
		if (in_array($Sort, array("Subject","SortFrom","SortTo"))) 
			$CurSortValue[0] = UTF8_To_DB_Handler(UTF8_From_DB_Handler($CurSortValue[0]));
		
		$sql1 = 'Select Top 1
							MessageUID 
						From 
							MAIL_LOCAL_LIST
						With 
							(NOLOCK)
						Where 
							EmailAddress = \''.$this->MailAddress.'\'
							AND CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($FolderName).' 
							AND '.$Sort.' < '.$CurSortValue[0].'
						Order by 
							'.$Sort.' desc';
		//echo $sql1.'<br>';
		$sql2 = 'Select Top 1
							MessageUID 
						From 
							MAIL_LOCAL_LIST 
						With 
							(Nolock) 
						Where 
							EmailAddress = \''.$this->MailAddress.'\'
							AND CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($FolderName).' 
							AND '.$Sort.' > '.$CurSortValue[0].'
						Order by 
							'.$Sort.' asc';
		//echo $sql2.'<br>';
		$Order = ($Reverse == 0)? "asc":"desc";	
		$FetchUpSql = ($Reverse == 0)? $sql1:$sql2;	
		$FetchDownSql = ($Reverse == 1)? $sql1:$sql2;	
		
		$NeedFetchUp = false;
		$NeedFetchDown = false;
		$sql = 'Select 
							MessageUID
						From 
							MAIL_LOCAL_LIST
						With 
							(NOLOCK)
						Where 
							EmailAddress = \''.$this->MailAddress.'\'
							AND CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($FolderName).' 
							AND '.$Sort.' = '.$CurSortValue[0].'
						Order by 
							'.$Sort.' '.$Order;
		/*echo $sql.'<br>';					
		echo $sql1.'<br>';
		echo $sql2.'<br>';*/
		$Result = $db->returnArray($sql);
		
		//var_dump($Result);
		for ($i=0; $i < sizeof($Result); $i++) {
			//echo $i.'<br>';
			if ($Result[$i][0] == $CurUID) {
				if ($i == 0) $NeedFetchUp = true;
				else $PrevUID = $Result[$i-1][0];
				
				if ($i == (sizeof($Result)-1)) $NeedFetchDown = true;
				else $NextUID = $Result[$i+1][0];
			}
		}
		
		/*var_dump($NeedFetchUp);
		var_dump($NeedFetchDown);*/
		
		if ($NeedFetchUp) {
			$Result = $db->returnVector($FetchUpSql);
			/*echo '<pre>';
			var_dump($Result);
			echo '</pre>';*/
			$PrevUID = (trim($Result[0]) == "" || is_null($Result[0]))? false:$Result[0];
		}
		
		if ($NeedFetchDown) {
			$Result = $db->returnVector($FetchDownSql);
			/*echo '<pre>';
			var_dump($Result);
			echo '</pre>';*/
			$NextUID = (trim($Result[0]) == "" || is_null($Result[0]))? false:$Result[0];
		}
		
    $ReturnVal['PrevUID'] = $PrevUID;
    $ReturnVal['NextUID'] = $NextUID;
    
    if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Get Prev/ Next Msg UID): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Get Prev/ Next Msg UID): '.$this->Stop_Timer().'<br>';
    }
            
    return $ReturnVal;
  }
  
  function Clear_Cache_Folder($Folder) {
  	$db = new database(true);
  	
  	$EncodedFolder = IMap_Encode_Folder_Name($Folder);
  	
  	$Result = $this->Clear_Folder($EncodedFolder,true);
  	if ($Result) {
  		if ($this->ShowTime && $this->ShowLevel == 0) 
				$this->Start_Timer();
				
  		$sql = 'Delete From MAIL_LOCAL_LIST 
							Where 
								EmailAddress = \''.$this->MailAddress.'\' 
								AND 
								CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Folder).'
						 ';
			//echo $sql; die;
			$Result = $db->db_db_query($sql);
			
			if ($this->ShowTime && $this->ShowLevel == 0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Cache: Time Different (Clear Folder): '.$this->Stop_Timer());
	    	else
	    		echo 'Time Different (Clear Folder): '.$this->Stop_Timer().'<br>';
	    }
			return $Result;
			
  	}
  	else 
  		return false;
  }
  
  function Get_Mail_Record($FolderName, $UID) {
  	//disabled function, will go on to further investiage the message body structure
    /*$db = new database(true);
    
		$sql = 'select 
							Date,
							IsImportant,
							HasAttachment,
							CAST(CAST(HeaderFrom AS varbinary(8000)) AS image) as HeaderFrom,
							CAST(CAST(HeaderTo AS varbinary(8000)) AS image) as HeaderTo,
							CAST(CAST(HeaderCc AS varbinary(8000)) AS image) as HeaderCc,
							CAST(CAST(HeaderBcc AS varbinary(8000)) AS image) as HeaderBcc,
							CAST(CAST(Subject AS varbinary(8000)) AS image) as Subject, 
							MessageBoundary 
						From 
							MAIL_LOCAL_LIST 
						Where 
							MessageUID = \''.$UID.'\' 
							AND 
							MailFolder = '.UTF8_To_DB_Handler($FolderName).' 
							AND 
							EmailAddress = \''.$this->MailAddress.'\'
					 ';
		$Header = $db->returnArray($sql);
		
		$MailParts = $this->Get_Mail_Content(Imap_Encode_Folder_Name($FolderName),$UID,$Header[0]['MessageBoundary']);					
    
    $fromDetail = UTF8_From_DB_Handler($Header[0]['HeaderFrom']);
    $toDetail = UTF8_From_DB_Handler($Header[0]['HeaderTo']);
    $ccDetail = UTF8_From_DB_Handler($Header[0]['HeaderCc']);
    $bccDetail = UTF8_From_DB_Handler($Header[0]['HeaderBcc']);
    $subject = UTF8_From_DB_Handler($Header[0]['Subject']);
    $dateReceive = date('Y-m-d H:i:s',$Header[0]['Date']);
    $attachments = ($MailParts["HaveAttachment"])? $MailParts["attachment"]:array();

    # Handle attachments
    unset($attach_parts);
    unset($embed_cids);
    $total_attach_size = 0;
    for ($k=0; $k<sizeof($attachments); $k++)
    {
      $attach_filename = $attachments[$k]["FileName"];
      $attach_type = $attachments[$k]["ContentType"];
      $attach_size = $attachments[$k]["Size"];
      

      $attach_entry['filename'] = $attach_filename;
      $attach_entry['type'] = $attach_type;
      $attach_entry['size'] = $attach_size;
      $attach_entry['content'] = $attachments[$k]["FileContent"];
      $attach_parts[] = $attach_entry;
    }

    $message = $MailParts["message"]["html"];
    $text_message = $MailParts["message"]["text"];

    $returnArray = array();
    //$returnArray[0]["msgno"] = $mail_msgno;
    $returnArray[0]["uid"] = $UID;
    $returnArray[0]["Priority"] = $Header[0]['IsImportant'];
    $returnArray[0]["subject"] = UTF8_From_DB_Handler($Header[0]['Subject']);
    $returnArray[0]["message"] = $message;
    $returnArray[0]["text_message"] = $text_message;
    $returnArray[0]["total_attach_size"] = $total_attach_size;
    $returnArray[0]["fromname"] = imap_rfc822_parse_adrlist(UTF8_From_DB_Handler($Header[0]['HeaderFrom']),"");
    $returnArray[0]["showto"] = imap_rfc822_parse_adrlist(UTF8_From_DB_Handler($Header[0]['HeaderTo']),"");
    $returnArray[0]["showcc"] = imap_rfc822_parse_adrlist(UTF8_From_DB_Handler($Header[0]['HeaderCc']),"");
    $returnArray[0]["showbcc"] = imap_rfc822_parse_adrlist(UTF8_From_DB_Handler($Header[0]['HeaderBcc']),"");
    $returnArray[0]["toDetail"] = imap_rfc822_parse_adrlist(UTF8_From_DB_Handler($Header[0]['HeaderTo']),"");
    $returnArray[0]["fromDetail"] = imap_rfc822_parse_adrlist(UTF8_From_DB_Handler($Header[0]['HeaderFrom']),"");
    $returnArray[0]["ccDetail"] = imap_rfc822_parse_adrlist(UTF8_From_DB_Handler($Header[0]['HeaderCc']),"");
    $returnArray[0]["bccDetail"] = imap_rfc822_parse_adrlist(UTF8_From_DB_Handler($Header[0]['HeaderBcc']),"");
    $returnArray[0]["dateReceive"] = date('Y-m-d H:i:s',$Header[0]['Date']);
    $returnArray[0]["ReceiveTimeStamp"] = $Header[0]['Date'];
    $returnArray[0]["attach_parts"] = $attach_parts;
    $returnArray[0]["is_html_message"] = $MailParts["message"]["IsHtml"];

    return $returnArray;*/
  }
  
  function Get_Cache_Message_Gap($FolderName) {
  	if ($this->ShowTime && $this->ShowLevel == 0) 
				$this->Start_Timer();
  	$db = new database(true);
  	
  	$sql = 'exec Get_Unchain_Cache '.UTF8_To_DB_Handler($FolderName).', \''.$this->MailAddress.'\'';
  	$CacheGap = $db->returnArray($sql);

  	$ErrFlag = false;
		for ($k=0; $k< sizeof($CacheGap); $k++) {
			$FromMsgNo[$k] = $this->Get_MsgNo($CacheGap[$k]['FromID']);
			$ToMsgNo[$k] = $this->Get_MsgNo($CacheGap[$k]['ToID']);
			
			if ($FromMsgNo[$k] == false) {
				$this->Delete_Cached_Message($FolderName, $CacheGap[$k]['FromID'], $this->MailAddress);
				$ErrFlag = true;
			}
			
			if ($ToMsgNo[$k] == false) {
				$this->Delete_Cached_Message($FolderName, $CacheGap[$k]['ToID'], $this->MailAddress);
				$ErrFlag = true;
			}
		}	
		
		if ($ErrFlag) return false;
		
		$Result['TotalNumber'] = 0;
		for ($k=0; $k< sizeof($CacheGap); $k++) {
			if (($FromMsgNo[$k]-1) != $ToMsgNo[$k]) {
				$Temp = $FromMsgNo[$k]-$ToMsgNo[$k]-1;
				$Result['Pairs'][] = array($FromMsgNo[$k]-1,$ToMsgNo[$k]);
				$Result['TotalNumber'] += $Temp;
			}
		}
		/*echo '<pre>';
		var_dump($Result);
		echo '</pre>';
		die;*/
		
		if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Get cache gap for sync): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Get cache gap for sync): '.$this->Stop_Timer().'<br>';
    }
  	return (is_null($Result))? array():$Result;
  }
  
  function Get_Syn_Mail_Status_Msg($Finished,$Total) {
  	$x = '|Getting new email(s) '.$Finished.'/'.$Total.', please wait...';
  	
  	return $x;
  }
  
  // disabled function on 22/10/2008, kenneth Chung
  function Update_ReadFlag($Folder, $MessageID)
  {
	/*$UpdateSql = "UPDATE MAIL_LOCAL_LIST SET ReadFlag = 1 WHERE ";
	$UpdateSql .= "EmailAddress = '" . $this->MailAddress . "' AND ";
	$UpdateSql .= "CAST(MailFolder AS varbinary(8000)) = ".UTF8_To_DB_Handler($Folder)." AND ";
	$UpdateSql .= "MessageUID = " . $MessageID;
	
	$db = new database(true);	  
	return $db->db_db_query($UpdateSql);*/
  }
  
  // disabled function on 22/10/2008, kenneth Chung
  function Get_Cached_Mail_Header($UID, $MailFolder)
  {
  /*
	$SelectSql = "Select ReadFlag From Mail_Local_List With (NOLOCK)";
	$SelectSql .= "WHERE EmailAddress = '" . $this->MailAddress . "' ";
	$SelectSql .= "AND MessageUID = " . $UID . " ";
	$SelectSql .= "AND CAST(MailFolder AS varbinary(8000)) = ". UTF8_To_DB_Handler($MailFolder)." ";

	$db = new database(true);	  
	return $db->returnArray($SelectSql);*/
  }
  
  function Rename_Cache_Folder($OldName,$NewName) {
  	if ($this->ShowTime && $this->ShowLevel == 0) 
				$this->Start_Timer();
				
  	global $SYS_CONFIG;
  	$db = new database(true);
  	
  	$TempPath = explode($this->FolderDelimiter, $OldName);
    $FolderPath = "";
    for ($i=0; $i< (sizeof($TempPath)-1); $i++)
    	$FolderPath .= $TempPath[$i].$this->FolderDelimiter;
    $NewName = $FolderPath.$NewName;
  	
  	$FolderList = $this->Get_Mail_Folders();	
  	for ($i=0; $i< sizeof($FolderList); $i++) {
  		if (strpos($FolderList[$i]["FolderFullPath"],$OldName) === 0) {
  			$OldFolderName = $FolderList[$i]["FolderFullPath"];
  			$NewFolderName = preg_replace('/'.$OldName.'/',$NewName,$FolderList[$i]["FolderFullPath"],1);
	  		$sql = 'Update MAIL_LOCAL_LIST 
	  					Set 
	  						MailFolder = '.UTF8_To_DB_Handler($NewFolderName).'
	  					Where 
	  						EmailAddress = \''.$this->MailAddress.'\' 
	  						AND 
	  						CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($OldFolderName).'
	  				 ';
	  		$db->db_db_query($sql);
  		}
  	}
  	
  	if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Rename folder): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Rename folder): '.$this->Stop_Timer().'<br>';
    }
  }
  
  function Delete_Cache_Folder($FolderName) {
  	if ($this->ShowTime && $this->ShowLevel == 0) 
				$this->Start_Timer();
				
  	$db = new database(true);
  	
  	$FolderList = $this->Get_Mail_Folders();
  	for ($i=0; $i< sizeof($FolderList); $i++) {
  		if (strpos($FolderList[$i]["FolderFullPath"],$FolderName) === 0) {
		  	$sql = 'Delete From MAIL_LOCAL_LIST 
		  					Where 
		  						EmailAddress = \''.$this->MailAddress.'\' 
		  						AND 
		  						CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($FolderList[$i]["FolderFullPath"]).'
		  				 ';
		  	$db->db_db_query($sql);
  		}
  	}
  	
  	if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Delete folder): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Delete folder): '.$this->Stop_Timer().'<br>';
    }
  }
  
  // for message/rfc822 sub message
  function Get_Attach_Message_Detail($Folder,$UID,$PartNumber) {
  	$EncodedFolder = IMap_Encode_Folder_Name($Folder);
  	
  	$StructureArray = $this->Get_Message_Structure($UID,$EncodedFolder);
  	
  	if (trim($PartNumber) != "") 
  		$MessagePart = $this->Get_Target_PartNumber($PartNumber,$StructureArray);
  	
  	/*$NarrowParts = $MessagePart->parts;
  	$AttachmentCount = 0;
  	for ($i=0; $i< sizeof($NarrowParts); $i++) {
  		if (strtolower($NarrowParts[$i]->disposition) == 'attachment' || (strtolower($StructureParts[$i]->disposition) == 'inline' && strtolower($StructureParts[$i]->ctype_primary) != 'text')) {
  			if (trim($NarrowParts[$i]->filename) != "") {
	  			$AttachList[$AttachmentCount]['FileName'] = $NarrowParts[$i]->filename;
	  			$AttachList[$AttachmentCount]['FileSize'] = round($NarrowParts[$i]->size/1024,2);
	  			$AttachList[$AttachmentCount]['PartID'] = $NarrowParts[$i]->mime_id;
	  			$AttachList[$AttachmentCount]['AttachType'] = ($NarrowParts[$i]->ctype_primary == "message")? "Message":"File";
	  			$AttachmentCount++;
  			}
  		}
  	}*/
  	
  	$MessageHeaders = $MessagePart->headers;
  	$MailInfo['To'] = $MessageHeaders['to'];
  	$MailInfo['From'] = $MessageHeaders['from'];
  	$MailInfo['Cc'] = $MessageHeaders['cc'];
  	$MailInfo['Bcc'] = $MessageHeaders['bcc'];
  	$MailInfo['Subject'] = $this->MIME_Decode($MessageHeaders['subject']);
  	$MailInfo['IsImportant'] = ($MessageHeaders['x-priority'] == 1 || $MsgCache[$z]['x-msmail-priority'] == 'High')? 'true':'false';
  	$MailInfo['Date'] = Parse_Date_Time(strtotime($MessageHeaders['date']),"",false,true,"normal",false);
  	$MailInfo['AttachList'] = $this->Get_Attachment_List($MessagePart,$Folder,$UID);
  	
//		if (sizeof($MailInfo['AttachList']) > 0 && $MessageHeader[0]['HasAttachment'] == 0) {
//			$ldb = new database(true);
//			$sql = 'Update Mail_Local_List Set 
//								HasAttachment = 1,
//								LastModified = GetDate()
//							Where 
//								EmailAddress = \''.$this->MailAddress.'\' 
//								And 
//								CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Folder).' 
//								And 
//								MessageUID = '.$UID;
//			$ldb->db_db_query($sql);
//		}
  	
  	return $MailInfo;
  }
  
  function Get_Message_Detail($Folder,$UID,$PartNumber="") {
  	global $SYS_CONFIG;
  	
  	if ($PartNumber != "") 
  		return $this->Get_Attach_Message_Detail($Folder,$UID,$PartNumber);
  	
  	if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
  	$db = new database(true);
  	
  	$sql = 'Get_Single_Cache \''.$this->MailAddress.'\', '.UTF8_To_DB_Handler($Folder).', '.$UID;
  	//echo $sql;
  	$MessageHeader = $db->returnArray($sql);
  	if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Get Single Cache): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Get Single Cache): '.$this->Stop_Timer().'<br>';
    }
  	
  	$EncodedFolder = IMap_Encode_Folder_Name($Folder);
  	$StructureArray = $this->Get_Message_Structure($UID,$EncodedFolder,$MessageHeader[0]['MessageStructure']);
  	/*$StructureParts = $StructureArray->parts;
  	$AttachmentCount = 0;
  	for ($i=0; $i< sizeof($StructureParts); $i++) {
  		if (strtolower($StructureParts[$i]->disposition) == 'attachment' || (strtolower($StructureParts[$i]->disposition) == 'inline' && strtolower($StructureParts[$i]->ctype_primary) != 'text')) {
  			if (trim($StructureParts[$i]->filename) != "") {
	  			$AttachList[$AttachmentCount]['FileName'] = $StructureParts[$i]->filename;
	  			$AttachList[$AttachmentCount]['FileSize'] = round($StructureParts[$i]->size/1024,2);
	  			$AttachList[$AttachmentCount]['PartID'] = $StructureParts[$i]->mime_id;
	  			$AttachList[$AttachmentCount]['AttachType'] = ($StructureParts[$i]->ctype_primary == "message")? "Message":"File";
	  			$AttachmentCount++;
  			}
  		}
  	}*/
  	
  	$MailInfo['To'] = UTF8_From_DB_Handler($MessageHeader[0]['HeaderTo']);
  	$MailInfo['From'] = UTF8_From_DB_Handler($MessageHeader[0]['HeaderFrom']);
  	$MailInfo['Cc'] = UTF8_From_DB_Handler($MessageHeader[0]['HeaderCc']);
  	$MailInfo['Bcc'] = UTF8_From_DB_Handler($MessageHeader[0]['HeaderBcc']);
  	$MailInfo['Subject'] = UTF8_From_DB_Handler($MessageHeader[0]['Subject']);
  	//$MailInfo['Date'] = Parse_Date_Time($MessageHeader[0]['Date'],"",false,true,"normal",false);
  	// display month and day as well even though the mail date is today
  	if(date("dm", $MessageHeader[0]['Date'])!=date("dm")){
  		$MailInfo['Date'] = Parse_Date_Time($MessageHeader[0]['Date'],"",false,true,"normal",false);		
  	}
  	else{
  		$DateForm = $SYS_CONFIG['DateFormat']['DayMonth']. ", ". "H:i";
  		$MailInfo['Date'] = date($DateForm, $MessageHeader[0]['Date']);
  	}
  	$MailInfo['IsImportant'] = $MessageHeader[0]['IsImportant'];
  	$MailInfo['AttachList'] = $this->Get_Attachment_List($StructureArray,$Folder,$UID);
  	
  	if (sizeof($MailInfo['AttachList']) > 0 && $MessageHeader[0]['HasAttachment'] == 0) {
			$ldb = new database(true);
			$sql = 'Update Mail_Local_List Set 
								HasAttachment = 1,
								LastModified = GetDate()
							Where 
								EmailAddress = \''.$this->MailAddress.'\' 
								And 
								CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Folder).' 
								And 
								MessageUID = '.$UID;
			$ldb->db_db_query($sql);
		}
		
  	return $MailInfo;
  }
  
  // to get message body message from mail server, if PartNumber parameter is defined, 
  // means to get the body message within one part <-- which will applied to message/rfc sub_message
  function Get_Message_Text($Folder,$UID,$Parts="",$PartNumber="",$PreviewMode=0) {
  	global $SYS_CONFIG, $PATH_WRT_ROOT,$file_path,$UserID,$IMap;
  	
  	$EncodedFolder = IMap_Encode_Folder_Name($Folder);
  	
  	if ($Parts == "")
  		$StructureArray = $this->Get_Message_Structure($UID,$EncodedFolder);
  	else 
  		$StructureArray = $Parts;
	
  	if (trim($PartNumber) != "") {
  		$StructureArray = $this->Get_Target_PartNumber($PartNumber,$StructureArray);
  		//echo '<pre>';
  		//debug_pr($StructureArray);
  		//echo '</pre>';
  		
  		/*$StructureParts = $StructureArray->parts;
	  	for ($i=0; $i< sizeof($StructureParts); $i++) {
	  		if ($StructureParts[$i]->mime_id == $PartNumber) 
	  			$MessagePart = $StructureParts[$i];
	  	}*/
	  	$StructureArray->ctype_primary = $StructureArray->headers['c_primary'];
	  	$StructureArray->ctype_secondary = $StructureArray->headers['c_secondary'];
	  	$StructureArray->mimetype = substr($StructureArray->headers['content-type'],0,strlen($MessagePart->headers['content-type'])-1);
	  	
	  	//$StructureArray = $MessagePart;
	  }
	//debug_pr($StructureArray);
  	if (strtolower($StructureArray->ctype_primary) == 'multipart') {
  		// alternative
  		if (strtolower($StructureArray->ctype_secondary) == 'alternative') {
	  		for ($i=0; $i< sizeof($StructureArray->parts); $i++) {
	  			
	  			
	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'html' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				$RawContent = $this->Get_Message_Part($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder);

						$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->parts[$i]->encoding);
						//try_coding($RawContent);
						$lc_charset = trim(strtolower($StructureArray->parts[$i]->charset));
						if($lc_charset=="gb2312" || $lc_charset=="gb18030"){
							$ResultHTML = @mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK");
						}else if($lc_charset == "big5-hkscs"){
							$ResultHTML = @iconv($StructureArray->parts[$i]->charset,"UTF-8",$RawContent);
						}else if ($StructureArray->parts[$i]->charset != "utf-8"){
							if($StructureArray->parts[$i]->charset=="")
								$ResultHTML = @mb_convert_encoding($RawContent,"UTF-8","BIG5");
							else
								$ResultHTML = @mb_convert_encoding($RawContent,"UTF-8",$StructureArray->parts[$i]->charset);
						}else 
							$ResultHTML = $RawContent;
						
	  			}
	  			
	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'plain' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				$RawContent = $this->Get_Message_Part($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder);
					$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->parts[$i]->encoding);
					
					$lc_charset = trim(strtolower($StructureArray->parts[$i]->charset));
  					if($lc_charset=="gb2312" || $lc_charset=="gb18030") {
						$ResultText = nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK"),ENT_COMPAT));
  					}else if($lc_charset == "big5-hkscs"){
							$ResultText = nl2br(@htmlspecialchars(@iconv($StructureArray->parts[$i]->charset,"UTF-8",$RawContent),ENT_COMPAT));
					}else if ($StructureArray->parts[$i]->charset != "utf-8"){
						if($StructureArray->parts[$i]->charset=="")
							$ResultText = nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","BIG5"),ENT_COMPAT));
						else
  							$ResultText = nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8",$StructureArray->parts[$i]->charset),ENT_COMPAT));
					}else {
						$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT,$StructureArray->parts[$i]->charset));
						if($tmp_content == ''){
							$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT));
						}
						$ResultText = $tmp_content;
  					}
	  			}

	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'calendar' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				include_once("icalendar.php");
	  				$iCal = new icalendar();

	  				$RawContent = $this->Get_Message_Part($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder);
	  				$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->parts[$i]->encoding);
	  				$events = $iCal->extract_icalEvent($RawContent);

					$ResultHTML = $iCal->Get_IMap_Calendar_Content($events).$ResultHTML;
	  			}

	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'alternative' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
					foreach($StructureArray->parts[$i]->parts as $Key =>$Arr) {
						$lc_charset = trim(strtolower($Arr->charset));
						if (strtolower($Arr->ctype_secondary) == 'plain' && strtolower($Arr->disposition) != 'attachment') {
							$RawContent = $this->Get_Message_Part($Arr->mime_id,$UID,$EncodedFolder);
							$RawContent = $this->Mime_Decode_Msg($RawContent,$Arr->encoding);
							
							if ($lc_charset == "gb2312" || $lc_charset=="gb18030") {
								$ResultText = nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK"),ENT_COMPAT));
							}else if($lc_charset == "big5-hkscs"){
								$ResultText = nl2br(@htmlspecialchars(@iconv($Arr->charset,"UTF-8",$RawContent),ENT_COMPAT));
							}else if ($Arr->charset != "utf-8"){
								if($Arr->charset=="")
									$ResultText = nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","BIG5"),ENT_COMPAT));
								else
									$ResultText = nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8",$Arr->charset),ENT_COMPAT));
							}else{
								$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT,$Arr->charset));
								if($tmp_content == ''){
									$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT));
								}
								$ResultText = $tmp_content;
							}
						}
						
						if (strtolower($Arr->ctype_secondary) == 'html' && strtolower($Arr->disposition) != 'attachment') {
							$RawContent = $this->Get_Message_Part($Arr->mime_id,$UID,$EncodedFolder);
							$RawContent = $this->Mime_Decode_Msg($RawContent,$Arr->encoding);
						
						if ($lc_charset == "gb2312" || $lc_charset == "gb18030") {
							$ResultHTML = @mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK");
						}else if($lc_charset == "big5-hkscs"){
							$ResultHTML = @iconv($Arr->charset,"UTF-8",$RawContent);
						}else if ($Arr->charset != "utf-8"){
							if($Arr->charset=="")
								$ResultHTML = @mb_convert_encoding($RawContent,"UTF-8","BIG5");
							else
								$ResultHTML = @mb_convert_encoding($RawContent,"UTF-8",$Arr->charset);
						}else
							$ResultHTML = $RawContent;
						}
					}
				}
					  			
	  			// if ctype_secondary is related (added on 2015-12-01)
	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'related' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				$ResultHTML .= $this->Get_Message_Text($Folder,$UID,$StructureArray->parts[$i]);
	  			}
	  			
	  		}
	  		/*
	  		if (trim(strip_tags($ResultHTML)) != '') {
	  			$Result .= $ResultHTML;
	  		}
	  		else {
	  			$Result .= $ResultText;
	  		}
	  		*/
	  		if (trim($ResultHTML) != '') {
	  			$Result .= $ResultHTML;
	  		}
	  		else {
	  			$Result .= $ResultText;
	  		}
  		}
  		// related
  		else if (strtolower($StructureArray->ctype_secondary) == 'related') {
  			$lfs = new libfilesystem();
			//$personal_path = $PATH_WRT_ROOT."src/module/email/view_mail_folder/".$SYS_CONFIG['SchoolCode']."/u".$_SESSION['SSV_USERID']."/related/";
			$personal_path = $IMap->user_file_path."/related/";
			
			if (!is_dir($personal_path)) {
				$lfs->folder_new($personal_path, 0755, true);
			}
			//else {
			//	$lfs->folder_remove_recursive($personal_path);
			//	$lfs->folder_new($personal_path, 0755, true);
			//}
			
	  		for ($i=0; $i< sizeof($StructureArray->parts); $i++) {
	  			if (strtolower($StructureArray->parts[$i]->ctype_primary) == 'multipart')
	  				$Result .= $this->Get_Message_Text($Folder,$UID,$StructureArray->parts[$i]);
	  			
	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'html' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				$RawContent = $this->Get_Message_Part($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder);
						$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->parts[$i]->encoding);
						
						$lc_charset = trim(strtolower($StructureArray->parts[$i]->charset));
						if ($lc_charset == "gb2312" || $lc_charset == "gb18030") {
							$Result .= @mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK");
						}else if($lc_charset == "big5-hkscs"){
							$Result .= @iconv($StructureArray->parts[$i]->charset,"UTF-8",$RawContent);
						}else if ($StructureArray->parts[$i]->charset != "utf-8"){
							if($StructureArray->parts[$i]->charset=="")
								$Result .= @mb_convert_encoding($RawContent,"UTF-8","BIG5");
							else
								$Result .= @mb_convert_encoding($RawContent,"UTF-8",$StructureArray->parts[$i]->charset);
						}else 
							$Result .= $RawContent;
	  			}
	  			
	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'plain' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				$RawContent = $this->Get_Message_Part($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder);
						$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->parts[$i]->encoding);
					
					$lc_charset = trim(strtolower($StructureArray->parts[$i]->charset));	
  					if ($lc_charset == "gb2312" || $lc_charset == "gb18030") {
						$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK"),ENT_COMPAT));
					}else if($lc_charset == "big5-hkscs"){
						$Result .= nl2br(@htmlspecialchars(@iconv($StructureArray->parts[$i]->charset,"UTF-8",$RawContent),ENT_COMPAT));
					}else if ($StructureArray->parts[$i]->charset != "utf-8") {
						if($StructureArray->parts[$i]->charset=="")
							$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","BIG5"),ENT_COMPAT));
						else
							$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8",$StructureArray->parts[$i]->charset),ENT_COMPAT));
					}
					else {
						$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT,$StructureArray->parts[$i]->charset));
						if($tmp_content == ''){
							$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT));
						}
						$Result .= $tmp_content;
					}
	  			}
	  			
	  			if (strtolower($StructureArray->parts[$i]->disposition) == 'inline') {
						$Attachment = $this->Get_Attachment($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder,$StructureArray->parts[$i]);
						$tmp_content_id = trim(str_replace(array("<",">"),array("",""),$StructureArray->parts[$i]->content_id));
						if(trim($Attachment['FileName']) == '' && $tmp_content_id!="")
						{
						 	$file_ext = $this->Get_File_Extension_From_Mime_Type($Attachment['MimeType']);
						 	$Attachment['FileName'] = $tmp_content_id.($file_ext != ''? ".".$file_ext:"");
						}
						if (trim($Attachment['FileName']) != "" && $tmp_content_id!="") {
							if(!is_dir($personal_path.$tmp_content_id)){
								$lfs->folder_new($personal_path.$tmp_content_id, 0755, true);
							}
							$lfs->file_write($Attachment['Content'], $personal_path.$tmp_content_id."/".$Attachment['FileName']);
							$Temp['FileContentID'] = (stristr($StructureArray->parts[$i]->content_id,"cid:")?"":"cid:").str_replace(array("<",">"),array("",""),$StructureArray->parts[$i]->content_id);
							//$Temp['FileContentID'] = str_replace(array("<",">"),array("",""),$StructureArray->parts[$i]->content_id);
							$Temp['FilePath'] = ($personal_path[0]!="/"?"/":"").$personal_path.$tmp_content_id."/".$Attachment['FileName'];
							//if($Temp['FilePath'][0]=="/") $Temp['FilePath'] = str_replace('../', '', $Temp['FilePath']); //[G180466]
							$InlineFileList[] = $Temp;
						}
	  			}
	  		}
	  		
	  		/*echo '<textarea cols=250 rows=100>';
	  		var_dump($Result);
	  		echo '</textarea>';*/
	  			  		
	  		for ($i=0; $i< sizeof($InlineFileList); $i++) {
	  			$temp_file_contentid = (stristr($InlineFileList[$i]['FileContentID'],"cid:"))?$InlineFileList[$i]['FileContentID']:("cid:".$InlineFileList[$i]['FileContentID']);
	  			$Result = str_replace($temp_file_contentid,$InlineFileList[$i]['FilePath'],$Result);
	  			//$Result = str_replace($InlineFileList[$i]['FileContentID'],$InlineFileList[$i]['FilePath'],$Result);
	  		}
	  		/*echo '<textarea cols=250 rows=100>';
	  		var_dump($Result);
	  		echo '</textarea>';
	  		die;*/
  		} /*
  		else if (strtolower($StructureArray->ctype_secondary) == 'report') {
  			for ($i=0; $i< sizeof($StructureArray->parts); $i++) {
  				
	  			if (strtolower($StructureArray->parts[$i]->ctype_primary) == 'multipart')
	  				$Result .= nl2br(@htmlspecialchars($this->Get_Message_Text($Folder,$UID,$StructureArray->parts[$i]),ENT_COMPAT));
	  			
	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'html' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				$RawContent = $this->Get_Message_Part($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder);
						$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->parts[$i]->encoding);
						
						$lc_charset = trim(strtolower($StructureArray->parts[$i]->charset));
						if ($lc_charset == "gb2312" || $lc_charset == "gb18030") {
							$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK"),ENT_COMPAT));
						}else if ($StructureArray->parts[$i]->charset != "utf-8"){
							if($StructureArray->parts[$i]->charset=="")
								$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","BIG5"),ENT_COMPAT));
							else
								$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8",$StructureArray->parts[$i]->charset),ENT_COMPAT));
						}else
							$Result .= nl2br(@htmlspecialchars($RawContent,ENT_COMPAT));
	  			}
	  			
	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'plain' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				
	  				$RawContent = $this->Get_Message_Part($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder);
					
					$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->parts[$i]->encoding);
					
					$lc_charset = trim(strtolower($StructureArray->parts[$i]->charset ));
	  				if ($lc_charset== "gb2312" || $lc_charset == "gb18030") {
						$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK"),ENT_COMPAT));
	  				}else if ($StructureArray->parts[$i]->charset != "utf-8"){
						if($StructureArray->parts[$i]->charset=="")
							$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","BIG5"),ENT_COMPAT));
						else
							$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8",$StructureArray->parts[$i]->charset),ENT_COMPAT));
					}else 
						$Result .= nl2br(@htmlspecialchars($RawContent,ENT_COMPAT,$StructureArray->parts[$i]->charset));
	  			}
	  		}
  		} */
  		// message, to be added later
  		else {
  			
  			for ($i=0; $i< sizeof($StructureArray->parts); $i++) {
  				//echo $i.'<br>';
	  			if (strtolower($StructureArray->parts[$i]->ctype_primary) == 'multipart')
	  				$Result .= $this->Get_Message_Text($Folder,$UID,$StructureArray->parts[$i]);
	  			
	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'html' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				$RawContent = $this->Get_Message_Part($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder);
						$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->parts[$i]->encoding);
						
						$lc_charset = trim(strtolower($StructureArray->parts[$i]->charset));
						if ($lc_charset == "gb2312" || $lc_charset == "gb18030") {
							$Result .= @mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK");
						}else if($lc_charset == "big5-hkscs"){
							$Result .= @iconv($StructureArray->parts[$i]->charset,"UTF-8",$RawContent);
						}else if ($StructureArray->parts[$i]->charset != "utf-8"){
							if($StructureArray->parts[$i]->charset=="")
								$Result .= @mb_convert_encoding($RawContent,"UTF-8","BIG5");
							else
								$Result .= @mb_convert_encoding($RawContent,"UTF-8",$StructureArray->parts[$i]->charset);
						}else
							$Result .= $RawContent;
	  			}
	  			
	  			if (strtolower($StructureArray->parts[$i]->ctype_secondary) == 'plain' && strtolower($StructureArray->parts[$i]->disposition) != 'attachment') {
	  				
	  				$RawContent = $this->Get_Message_Part($StructureArray->parts[$i]->mime_id,$UID,$EncodedFolder);
					
					$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->parts[$i]->encoding);
					
					$lc_charset = trim(strtolower($StructureArray->parts[$i]->charset ));
	  				if ($lc_charset== "gb2312" || $lc_charset == "gb18030") {
						$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK"),ENT_COMPAT));
	  				}else if($lc_charset == "big5-hkscs"){
						$Result .= nl2br(@htmlspecialchars(@iconv($StructureArray->parts[$i]->charset,"UTF-8",$RawContent),ENT_COMPAT));
					}else if ($StructureArray->parts[$i]->charset != "utf-8"){
						if($StructureArray->parts[$i]->charset=="")
							$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","BIG5"),ENT_COMPAT));
						else
							$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8",$StructureArray->parts[$i]->charset),ENT_COMPAT));
					}else{ 
						$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT,$StructureArray->parts[$i]->charset));
						if($tmp_content == ''){
							$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT));
						}
						$Result .= $tmp_content;
	  				}
	  			}
	  		}
  		}
  	}
  	else {
  		
  		if (strtolower($StructureArray->ctype_secondary) == 'html') {
				$RawContent = $this->Get_Message_Part($StructureArray->mime_id,$UID,$EncodedFolder);
				$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->encoding);
				
				$lc_charset = trim(strtolower($StructureArray->charset));
				if ($lc_charset == "gb2312" || $lc_charset == "gb18030") {
					$Result .= @mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK");
				}else if($lc_charset == "big5-hkscs"){
					$Result .= @iconv($StructureArray->charset,"UTF-8",$RawContent);
				}else if ($StructureArray->charset != "utf-8"){
					if($StructureArray->charset=="")
						$Result .= @mb_convert_encoding($RawContent,"UTF-8","BIG5");
					else
						$Result .= @mb_convert_encoding($RawContent,"UTF-8",$StructureArray->charset);
				}else 
					$Result .= $RawContent;
			}
	  			
			if (strtolower($StructureArray->ctype_secondary) == 'plain') {
				
				$RawContent = $this->Get_Message_Part($StructureArray->mime_id,$UID,$EncodedFolder);
				$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->encoding);
				
				$lc_charset = trim(strtolower($StructureArray->charset ));
				if ($lc_charset== "gb2312" || $lc_charset == "gb18030") {
					$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","GB2312,GBK"),ENT_COMPAT));
				}else if($lc_charset == "big5-hkscs"){
					$Result .= nl2br(@htmlspecialchars(@iconv($StructureArray->charset,"UTF-8",$RawContent),ENT_COMPAT));
				}else if ($lc_charset != "utf-8" && $lc_charset != ''){
					if($StructureArray->charset=="")
						$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8","BIG5"),ENT_COMPAT));
					else
						$Result .= nl2br(@htmlspecialchars(@mb_convert_encoding($RawContent,"UTF-8",$StructureArray->charset),ENT_COMPAT));
				}else {
					$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT,$StructureArray->charset));
					if($tmp_content == ''){
						$tmp_content = nl2br(@htmlspecialchars($RawContent,ENT_COMPAT));
					}
					$Result .= $tmp_content;
				}					
			}

			if (strtolower($StructureArray->ctype_secondary) == 'calendar' && strtolower($StructureArray->disposition) != 'attachment') {
				include_once("icalendar.php");
				$iCal = new icalendar();

				$RawContent = $this->Get_Message_Part($StructureArray->mime_id,$UID,$EncodedFolder);
				$RawContent = $this->Mime_Decode_Msg($RawContent,$StructureArray->encoding);
				$events = $iCal->extract_icalEvent($RawContent);

				$Result = $iCal->Get_IMap_Calendar_Content($events).$Result;
			}
			//debug_pr($StructureArray->ctype_secondary);
  	}
	
  	return HTML_Transform($Result);
  }
  
  function Mime_Decode_Msg($msg, $encoding) {
		//Decode Msg Body if needed....
		if ($encoding == "base64")
		   $msg = base64_decode($msg);
		else if ($encoding == "quoted-printable")
		    $msg = quoted_printable_decode($msg);
		else if ($encoding == "8bit")
		    $msg = quoted_printable_decode(imap_8bit($msg));
		else { }
		
		return $msg;
  }
  
  function Get_Target_PartNumber($PartNumber,$StructureArray) {
  	$PartLevel = explode(".",$PartNumber);

		for ($i=0; $i< sizeof($PartLevel); $i++) {
			$StructureParts = $StructureArray->parts;
			
			if ($i == 0) 
				$FullPart = $PartLevel[$i];
			else
				$FullPart .= ".".$PartLevel[$i];
			for ($j=0; $j< sizeof($StructureParts); $j++) {
	  		if ($StructureParts[$j]->mime_id == $FullPart) {
	  			$StructureArray = $StructureParts[$j];
	  		}
  		}
		}
		
		return $StructureArray;
  }
  
  function Get_Attachment($PartNumber,$UID,$Folder,$Parts="") {
  	$EncodedFolder = IMap_Encode_Folder_Name($Folder);
  	
  	if($Parts == ""){
  		$StructureArray = $this->Get_Message_Structure($UID,$EncodedFolder);
  	}else{
  		$StructureArray = $Parts;
  	}
  	if (trim($PartNumber) != "") 
  		$StructureArray = $this->Get_Target_PartNumber($PartNumber,$StructureArray);
  	
  	$RawContent = $this->Get_Message_Part($PartNumber,$UID,$EncodedFolder);
  	//$RawContent = $this->Get_Message_Part($PartNumber,$UID,$Folder);
	
  	$Result['Content'] = $this->Mime_Decode_Msg($RawContent,$StructureArray->encoding);
  	$Result['MimeType'] = $StructureArray->mimetype;
  	//$Result['FileName'] = trim($StructureArray->filename);
  	$Result['FileName'] = $this->getImapMessagePartAttachmentFileName($StructureArray);
  	// cater inline file without filename 
  	if($Result['FileName'] == '' && $StructureArray->disposition == 'inline' && $StructureArray->content_id != ''){
  		$tmp_content_id = str_replace(array("<",">","/"),array("","","_"),$StructureArray->content_id);
  		$file_ext = $this->Get_File_Extension_From_Mime_Type($StructureArray->mimetype);
		$Result['FileName'] = $tmp_content_id.($file_ext != ''? ".".$file_ext:"");
  	}
  	
  	return $Result;
  }
  
  function Build_Mime_Text($subject,$message,$from,$receiver_to=array(),$receiver_cc=array(),$receiver_bcc=array(),$AttachList=array(),$IsImportant="",$DraftFlag=false, $Preference="", $RelatedFileList=array(), $isFromApp=false) {
  	global $PartNumber, $AttachPartNumber, $UID, $TNEFFile, $PathRelative;
  	global $Folder, $SYS_CONFIG, $UserID, $XeClassMailID, $IMap, $sys_custom;
		
		$TextMessage = html_entity_decode(html2txt(str_replace(array("<br>","<hr>"),array(chr(10),"______________________".chr(10)),$message))); 
		$message = Remove_Html_Comment($message); 
		$message = stripslashes($message);

		if ($UID != "") {
			$InlineFileList = $this->Get_Related_Inline_Info($Folder,$UID,"",$PartNumber);
		}
		else {
			$InlineFileList = false;
		}
		//debug_r($InlineFileList);
		if(sizeof($RelatedFileList)>0)
		{
			if($InlineFileList != false)
				$InlineFileList = array_merge($InlineFileList,$RelatedFileList);
			else
				$InlineFileList = $RelatedFileList;
		}
    # Content-type of attachment
    $type="application/octet-stream";

    $priority = ($IsImportant==1)? 1: 3;
   	//debug_r($InlineFileList);
    # receiving email addresses
    for ($i=0; $i< sizeof($receiver_to); $i++) {
      if (trim($receiver_to[$i]) != "") {
      	if(strstr($receiver_to[$i],"<"))
      	{
	      	//$splitTo = explode("<",$receiver_to[$i]);
	      	//$encodedTo = $this->Get_Mail_Format(trim(stripslashes($splitTo[0])))."<".$splitTo[1];
	      	$splitTo = $IMap->splitNameEmailEntry($receiver_to[$i]);
	      	$encodedTo = $splitTo[0]==""? $splitTo[1] : $this->Get_Mail_Format('"'.$IMap->replaceQuotesForDisplayName($splitTo[0]).'"')."<".$splitTo[1].">";
	      	$FinalReceiverTo[] = trim($encodedTo);
      	}
      	else
      		$FinalReceiverTo[] = trim($receiver_to[$i]);
      }
    }
    for ($i=0; $i< sizeof($receiver_cc); $i++) {
      if (trim($receiver_cc[$i]) != "") {
      	if(strstr($receiver_cc[$i],"<"))
      	{
	      	//$splitCc = explode("<",$receiver_cc[$i]);
	      	//$encodedCc = $this->Get_Mail_Format(trim(stripslashes($splitCc[0])))."<".$splitCc[1];
	      	$splitCc = $IMap->splitNameEmailEntry($receiver_cc[$i]);
	      	$encodedCc = $splitCc[0]==""? $splitCc[1] : $this->Get_Mail_Format('"'.$IMap->replaceQuotesForDisplayName($splitCc[0]).'"')."<".$splitCc[1].">";
			$FinalReceiverCc[] = trim($encodedCc);
      	}
      	else
			$FinalReceiverCc[] = trim($receiver_cc[$i]);
      }
    }
    for ($i=0; $i< sizeof($receiver_bcc); $i++) {
      if (trim($receiver_bcc[$i]) != "") {
      	if(strstr($receiver_bcc[$i],"<"))
      	{
	      	//$splitBcc = explode("<",$receiver_bcc[$i]);
	      	//$encodedBcc = $this->Get_Mail_Format(trim(stripslashes($splitBcc[0])))."<".$splitBcc[1];
	      	$splitBcc = $IMap->splitNameEmailEntry($receiver_bcc[$i]);
	      	$encodedBcc = $splitBcc[0]==""? $splitBcc[1] : $this->Get_Mail_Format('"'.$IMap->replaceQuotesForDisplayName($splitBcc[0]).'"')."<".$splitBcc[1].">";
			$FinalReceiverBcc[] = trim($encodedBcc);
      	}
      	else
      		$FinalReceiverBcc[] = trim($receiver_bcc[$i]);
      }
    }
    $numTo = sizeof($FinalReceiverTo);
    $numCC = sizeof($FinalReceiverCc);
    $numBCC = sizeof($FinalReceiverBcc);
    
    if (!$DraftFlag) {
    	if ($numTo + $numCC + $numBCC == 0) return false;
    }

    $to = ($numTo != 0)? implode(",".chr(13).chr(10)." ",$FinalReceiverTo) : "";
    if($sys_custom['iMailPlus']['CcHeaderNoLineBreak']){
		// Special handling for case [N64897 - The Spastics Association of Hong Kong Head Office - Cannot see the own mail address in cc]
		$cc = ($numCC != 0)? implode(",",$FinalReceiverCc) : "";
	}else{
    	$cc = ($numCC != 0)? implode(",".chr(13).chr(10)." ",$FinalReceiverCc) : "";
	}
    $bcc = ($numBCC != 0)? implode(",".chr(13).chr(10)." ",$FinalReceiverBcc) : "";
	
    # Mail Header
    $headers='';
    // have peference
    include_once("libuser.php");
    $luser = new libuser($UserID);
    if ($Preference!="") {
    	if($luser->ImapUserEmail == $from){
    		if(trim($Preference["DisplayName"]) == "")
    		{
	    		if($luser->RecordType == 2)
	         	{
					$sql = "SELECT ".getNameFieldWithClassNumberByLang()." FROM INTRANET_USER WHERE UserID = '$UserID'";
	         		$StudentClassInfo = $luser->returnVector($sql);
	         		if(sizeof($StudentClassInfo)>0){
		         		$fromName = $StudentClassInfo[0];
	    	     	}else{
	        	 		$fromName = $luser->UserName();
	         		}	
	         	}else{
	         		$fromName = $luser->UserName();
	         	}
    		}else
	    	{
	    		$fromName = $Preference["DisplayName"];
	    	}
	    }else{// Shared Mail Box
			/*$sql = "SELECT s.DisplayName 
					FROM MAIL_SHARED_MAILBOX as s 
					INNER JOIN MAIL_SHARED_MAILBOX_MEMBER as m ON m.MailBoxID = s.MailBoxID AND m.UserID = '$UserID' 
					WHERE s.MailBoxName = '$from' ";*/
			$sql = "SELECT DisplayName FROM MAIL_PREFERENCE WHERE MailBoxName='$from' ";
			$nameInfo = $luser->returnVector($sql);
			if(trim($nameInfo[0])!=""){
				$fromName = $nameInfo[0];
			}else{
				$fromName = $from;
			}
	    }
      $ReplyAddress = "\"".$this->Get_Mail_Format($fromName)."\" <".((trim($Preference["ReplyEmail"]) != "" && intranet_validateEmail($Preference["ReplyEmail"]))? $Preference["ReplyEmail"]:$from).">";
      $HeaderFrom .= "From: \"".$this->Get_Mail_Format($fromName)."\" <$from>".$this->MimeLineBreak;

      $ReturnMsg['From'] = trim('"'.$fromName.'" <'.$from.'>');
			$ReturnMsg['SortFrom'] = str_replace('"','',$ReturnMsg['From']);
    }
    // for auto mail, e.g password reset
    else {
    	$HeaderFrom .= 'From: '.$from.$this->MimeLineBreak;
      $ReplyAddress = $from;
    }

    for ($i=0; $i< sizeof($AttachList); $i++) {
      $files[] = $AttachList[$i][2];
      $fileRealName[] = $AttachList[$i][1];
    }

    $charset = "UTF-8";
	global $userBrowser;
    $isHTMLMessage = (!$isFromApp && !$sys_custom['iMailPlus']['CKEditor'] && ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")) ? 0 : 1;

		// check is multipart message
    $isMulti = (sizeof($files) > 0 || sizeof($AttachPartNumber) > 0 || $InlineFileList != false || sizeof($InlineFileList)>0 || sizeof($TNEFFile) > 0);
    // check is mixed message
    $isMixed = (sizeof($files) > 0 || sizeof($AttachPartNumber) > 0 || sizeof($TNEFFile) > 0);
    // check is related message
    $isRelated = (sizeof($InlineFileList)>0);

    $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
    $mime_boundary2 = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for multi-part
		
		$headers .= "MIME-Version: 1.0".$this->MimeLineBreak;	
    if ($isMulti) {
      $headers .= "X-Priority: $priority".$this->MimeLineBreak;
      if ($isMixed) 
      	$headers .= "Content-Type: multipart/mixed; ";
      else
      	$headers .= "Content-Type: multipart/related; ";
      $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
    else if ($isHTMLMessage)         # HTML message w/o attachments
    {
      $headers .= "X-Priority: $priority".$this->MimeLineBreak;
      $headers .= "Content-Type: multipart/alternative; ";
      $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
//    if ($Preference == "")
//			$headers .= $HeaderFrom;

    $headers .= "Date: " . date("r") .$this->MimeLineBreak;


    if ($ReplyAddress != "") {
    	$headers .= "Reply-To: $ReplyAddress".$this->MimeLineBreak;
    }
    
    //testing 20100326 
    if(!isset($XeClassMailID) || $XeClassMailID=='') $XeClassMailID = "u$UserID-".time();
    //$headers .= "X-eClass-MailID: u$UserID-".time().$this->MimeLineBreak;
	$headers .= "X-eClass-MailID: ".$XeClassMailID.$this->MimeLineBreak;
	
    if ($isMulti || $isHTMLMessage) {
	    $mime = "This is a multi-part message in MIME format.".$this->MimeLineBreak;
	    $mime .= $this->MimeLineBreak;
	    $mime .= "--" . $mime_boundary . $this->MimeLineBreak;
    }

    if (!$isMulti) { # No attachments/ related
    	if ($isHTMLMessage) {
    		
        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$encoded_text_message = QuotedPrintableEncode($message);
        $encoded_text_message = chunk_split(base64_encode($TextMessage));
        $mime .= $encoded_text_message. $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--". $mime_boundary . $this->MimeLineBreak;
        # HTML part
        $mime .= "Content-Type: text/html; ".$this->MimeLineBreak;
        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$mime .= "<HTML><BODY>\n".QuotedPrintableEncode($message)."</BODY></HTML>\n";
        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary . "--".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        
      }
      else {
      	$mime .= $message;
      }
    }
    else {

    	$filename_encoding = "=?".strtoupper($charset)."?B?";
    	if ($isMixed && $isRelated) {
    		$mime_boundary_related = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for Related
    		$mime_boundary_alternative = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for Alternative
    		$mime .= "Content-Type: multipart/related; boundary=\"$mime_boundary_related\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary_related . $this->MimeLineBreak;
        
        /*echo '<textarea cols=300 rows=150>';
				echo $message;
				echo '</textarea>';*/
        
        for ($i=0; $i < sizeof($InlineFileList); $i++) {
        	$temp_file_path = ($InlineFileList[$i]['FilePath'][0]!="/"?"/".$InlineFileList[$i]['FilePath']:$InlineFileList[$i]['FilePath']);
        	$temp_file_path_e = str_replace("%2F","/",rawurlencode($temp_file_path));
        	if(trim($InlineFileList[$i]['FileContentID'])=="") continue;
        	$temp_file_contentid = stristr($InlineFileList[$i]['FileContentID'],"cid:")?$InlineFileList[$i]['FileContentID']:"cid:".$InlineFileList[$i]['FileContentID'];
        	//if (trim($InlineFileList[$i]['FileContentID'])!='' && stripos($message,$InlineFileList[$i]['FileContentID'],0)!=FALSE){
	      	if(trim($temp_file_path)!='' && (stripos($message,$temp_file_path,0)!=FALSE || stripos($message,$temp_file_path_e,0)!=FALSE) ){
	      		//$message = str_replace(($InlineFileList[$i]['FilePath'][0]!="/"?"/":"").$InlineFileList[$i]['FilePath'],"cid:".$InlineFileList[$i]['FileContentID'],$message);
	      		$message = str_replace($temp_file_path,$temp_file_contentid,$message);
	      		$message = str_replace($temp_file_path_e,$temp_file_contentid,$message);
	      		$FinalInlineList[] = $InlineFileList[$i];
	      	}
	      }
		
				/*echo '<textarea cols=300 rows=150>';
				echo $message;
				echo '</textarea>';
				die;*/
	      if ($isHTMLMessage) {
	        $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary_alternative\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_alternative . $this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= chunk_split(base64_encode($TextMessage)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--". $mime_boundary_alternative . $this->MimeLineBreak;
	        # HTML part
	        $mime .= "Content-Type: text/html; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode($message);
	        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_alternative. "--".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	      }
	      else {
	        # Message Body
	        $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	
	        $mime .= $message;
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	      }
	      
	      if (sizeof($FinalInlineList) > 0) {
	      	$lfs = new libfilesystem();
	      	$mime .= "--" . $mime_boundary_related. $this->MimeLineBreak;
		      for ($i=0; $i < sizeof($FinalInlineList); $i++) {
		      	if(trim($FinalInlineList[$i]['PartNumber'])!="")
		      	{
		      	//$AttachmentContent = $IMapCache->Get_Attachment($FinalInlineList[$i]['PartNumber'],$UID,$Folder);
				$AttachmentContent = $this->Get_Attachment($FinalInlineList[$i]['PartNumber'],$UID,$Folder);
		     		$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].';'.$this->MimeLineBreak;
			     	$mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak;
						$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
						$mime .= 'Content-ID: '.$FinalInlineList[$i]['RealContentID'].$this->MimeLineBreak;
						$mime .= $this->MimeLineBreak;
			        
			      $mime .= chunk_split(base64_encode($AttachmentContent['Content']));
			     	$mime .= $this->MimeLineBreak;
			     	$mime .= $this->MimeLineBreak;
			      if ($i != (sizeof($FinalInlineList)-1)) {
			      	$mime .= "--" . $mime_boundary_related. $this->MimeLineBreak;
			      }
			      else {
			      	$mime .= "--" . $mime_boundary_related. "--" .$this->MimeLineBreak;
			      	$mime .= $this->MimeLineBreak;
			      }
			      
		      	}else # build related inline images
		      	{
		      		$mime .= 'Content-Type: '.$type.';'.$this->MimeLineBreak;
				    $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($lfs->get_file_basename($FinalInlineList[$i]['FilePath'])))."?=\"".$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
					$mime .= 'Content-ID: <'.$FinalInlineList[$i]['FileContentID'].">".$this->MimeLineBreak;
					$mime .= "Content-Disposition: inline;".$this->MimeLineBreak;
					$mime .= $this->MimeLineBreak;
				    
				    $mime .= chunk_split(base64_encode($lfs->file_read($FinalInlineList[$i]['FilePath'])));
				    $mime .= $this->MimeLineBreak;
				    $mime .= $this->MimeLineBreak;
				    if ($i != (sizeof($FinalInlineList)-1)) {
				      	$mime .= "--" . $mime_boundary. $this->MimeLineBreak;
				    }
				    else {
				      	$mime .= "--" . $mime_boundary_related. "--" .$this->MimeLineBreak;
				      	$mime .= $this->MimeLineBreak;
				    }
		      	}
		      }
		    }
		    else {
		    	$mime .= "--" . $mime_boundary_related. "--" .$this->MimeLineBreak;
			    $mime .= $this->MimeLineBreak;
		    }
	      $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
	    }
    	else if ($isRelated) {
    		//$mime_boundary_related = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for Related
    		$mime_boundary_alternative = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for Alternative
    		/*$mime .= "Content-Type: multipart/related; boundary=\"$mime_boundary_related\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary_related . $this->MimeLineBreak;*/
        
        /*echo '<textarea cols=300 rows=150>';
				echo $message;
				echo '</textarea>';*/
        
        for ($i=0; $i < sizeof($InlineFileList); $i++) {
        	$temp_file_path = ($InlineFileList[$i]['FilePath'][0]!="/"?"/".$InlineFileList[$i]['FilePath']:$InlineFileList[$i]['FilePath']);
        	$temp_file_path_e = str_replace("%2F","/",rawurlencode($temp_file_path));
        	if(trim($InlineFileList[$i]['FileContentID'])=="") continue;
        	$temp_file_contentid = stristr($InlineFileList[$i]['FileContentID'],"cid:")?$InlineFileList[$i]['FileContentID']:"cid:".$InlineFileList[$i]['FileContentID'];
        	//if (trim($InlineFileList[$i]['FileContentID'])!='' && stripos($message,$InlineFileList[$i]['FileContentID'],0)!=FALSE){
	      	if(trim($temp_file_path)!='' && (stripos($message,$temp_file_path,0)!=FALSE || stripos($message,$temp_file_path_e,0)!=FALSE) ){
	      		//$message = str_replace(($InlineFileList[$i]['FilePath'][0]!="/"?"/":"").$InlineFileList[$i]['FilePath'],"cid:".$InlineFileList[$i]['FileContentID'],$message);
	      		$message = str_replace($temp_file_path,$temp_file_contentid,$message);
	      		$message = str_replace($temp_file_path_e,$temp_file_contentid,$message);
	      		$FinalInlineList[] = $InlineFileList[$i];
	      	}
	      }
		
				/*echo '<textarea cols=300 rows=150>';
				echo $message;
				echo '</textarea>';
				die;*/
	      if ($isHTMLMessage) {
	        $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary_alternative\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_alternative . $this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= chunk_split(base64_encode($TextMessage)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--". $mime_boundary_alternative . $this->MimeLineBreak;
	        # HTML part
	        $mime .= "Content-Type: text/html; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode($message);
	        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary_alternative. "--".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	      }
	      else {
	        # Message Body
	        $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	
	        $mime .= $message;
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	      }
	      
	      if (sizeof($FinalInlineList) > 0) {
	      	$lfs = new libfilesystem();
	      	$mime .= "--" . $mime_boundary. $this->MimeLineBreak;
		      for ($i=0; $i < sizeof($FinalInlineList); $i++) {
		      	if(trim($FinalInlineList[$i]['PartNumber'])!="")
		      	{
			      	//$AttachmentContent = $IMapCache->Get_Attachment($FinalInlineList[$i]['PartNumber'],$UID,$Folder);
					$AttachmentContent = $this->Get_Attachment($FinalInlineList[$i]['PartNumber'],$UID,$Folder);
			     		$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].';'.$this->MimeLineBreak;
				     	$mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak;
							$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
							$mime .= 'Content-ID: '.$FinalInlineList[$i]['RealContentID'].$this->MimeLineBreak;
							$mime .= $this->MimeLineBreak;
				        
				      $mime .= chunk_split(base64_encode($AttachmentContent['Content']));
				     	$mime .= $this->MimeLineBreak;
				     	$mime .= $this->MimeLineBreak;
				      if ($i != (sizeof($FinalInlineList)-1)) {
				      	$mime .= "--" . $mime_boundary. $this->MimeLineBreak;
				      }
				      else {
				      	$mime .= "--" . $mime_boundary. "--" .$this->MimeLineBreak;
				      	$mime .= $this->MimeLineBreak;
				      }
		      	}else # build related inline images
		      	{
		      		$mime .= 'Content-Type: '.$type.';'.$this->MimeLineBreak;
				    $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($lfs->get_file_basename($FinalInlineList[$i]['FilePath'])))."?=\"".$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
					$mime .= 'Content-ID: <'.$FinalInlineList[$i]['FileContentID'].">".$this->MimeLineBreak;
					$mime .= "Content-Disposition: inline;".$this->MimeLineBreak;
					$mime .= $this->MimeLineBreak;
				    
				    $mime .= chunk_split(base64_encode($lfs->file_read($FinalInlineList[$i]['FilePath'])));
				    $mime .= $this->MimeLineBreak;
				    $mime .= $this->MimeLineBreak;
				    if ($i != (sizeof($FinalInlineList)-1)) {
				      	$mime .= "--" . $mime_boundary. $this->MimeLineBreak;
				    }
				    else {
				      	$mime .= "--" . $mime_boundary. "--" .$this->MimeLineBreak;
				      	$mime .= $this->MimeLineBreak;
				    }
		      	}
		      }
		    }
		    else {
		    	$mime .= "--" . $mime_boundary. "--" .$this->MimeLineBreak;
			    $mime .= $this->MimeLineBreak;
		    }
	    }  
	    else {
	    	if ($isHTMLMessage) {
	        $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary2\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary2 . $this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= chunk_split(base64_encode($TextMessage)). $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--". $mime_boundary2 . $this->MimeLineBreak;
	        # HTML part
	        $mime .= "Content-Type: text/html; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
	        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        #$mime .= QuotedPrintableEncode($message);
	        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary2. "--".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary. "".$this->MimeLineBreak;
	      }
	      else {
	        # Message Body
	        $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
	        $mime .= "Content-Type: text/plain; ".$this->MimeLineBreak;
	        $mime .= "\tcharset=\"$charset\"".$this->MimeLineBreak;
	        $mime .= $this->MimeLineBreak;
	
	        $mime .= $message;
	        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
	        $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
	      }
	    }

      // handle user's upload attachment
      for ($i=0; $i<sizeof($files); $i++) {
        //$target = $files[$i];
        $data = get_file_content($files[$i]);
        $fname = addslashes($fileRealName[$i]);

        $mime .= "Content-Type: $type;".$this->MimeLineBreak;
        $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= "Content-Disposition: attachment;".$this->MimeLineBreak;
        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= chunk_split(base64_encode($data));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary;

        # Different if end of email
        if ($i==sizeof($files)-1 && sizeof($AttachPartNumber) == 0 && sizeof($TNEFFile) == 0)
        	$mime .= "--".$this->MimeLineBreak;
        else
          $mime .= $this->MimeLineBreak;
     	}
     	
     	// handle forward message attachment
     	for ($i=0; $i< sizeof($AttachPartNumber); $i++) {
     		//$AttachmentContent = $IMapCache->Get_Attachment($AttachPartNumber[$i],$UID,$Folder);
			$AttachmentContent = $this->Get_Attachment($AttachPartNumber[$i],$UID,$Folder);
			//debug_pr($AttachmentContent);
				//die;
			// Find out if the same attachment is already included in the inline attachment list, avoid duplicated
			if(isset($FinalInlineList) && is_array($FinalInlineList))
			{
				$is_duplicated = false;
				for($j=0;$j<count($FinalInlineList);$j++){
					$last_slash_pos = strrpos($FinalInlineList[$j]['FilePath'],'/');
					$tmp_file_name = substr($FinalInlineList[$j]['FilePath'],$last_slash_pos+1);
					if(strcmp($AttachmentContent['FileName'],$tmp_file_name)==0){ // same file name then skip the file
						$is_duplicated = true;
					}
				}
				if($is_duplicated) continue;
			}
     		if ($AttachmentContent['MimeType'] == "message/rfc822") {
		     	$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: 7bit'.$this->MimeLineBreak;
					$mime .= 'Content-Disposition: attachment'.$this->MimeLineBreak.$this->MimeLineBreak;
					
					$mime .= $AttachmentContent['Content'];
				}
				else {
					$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].';'.$this->MimeLineBreak;
	     		$mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
					$mime .= 'Content-Disposition: attachment;'.$this->MimeLineBreak;
	        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        
	        $mime .= chunk_split(base64_encode($AttachmentContent['Content']));
				}     		
				
				$mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary;
        
        if ($i==sizeof($AttachPartNumber)-1 && sizeof($TNEFFile) == 0)
        	$mime .= "--".$this->MimeLineBreak;
        else
          $mime .= $this->MimeLineBreak; 
     	}
     	
     	// handle tnef attachment
     	$lfs = new libfilesystem();
			//$personal_path = $PathRelative."src/module/email/view_mail_folder/".$SYS_CONFIG['SchoolCode']."/u".$_SESSION['SSV_USERID']."/tnef/";
			$personal_path = $IMap->user_file_path."/tnef/";
     	for ($i=0; $i< sizeof($TNEFFile); $i++) {
     		$FilePath = $personal_path.$TNEFFile[$i];
     		$FileName = $TNEFFile[$i];
     		$Content = $lfs->file_read($FilePath);
     		$MimeType = $lfs->getMIMEType($FilePath);
				
				$mime .= 'Content-Type: '.$MimeType.';'.$this->MimeLineBreak;
     		$mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($FileName))."?=\"".$this->MimeLineBreak;
				$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
				$mime .= 'Content-Disposition: attachment;'.$this->MimeLineBreak;
        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($FileName))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
        
        $mime .= chunk_split(base64_encode($Content));
				
				$mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary;
        
        if ($i==sizeof($TNEFFile)-1)
        	$mime .= "--".$this->MimeLineBreak;
        else
          $mime .= $this->MimeLineBreak; 
     	}
		}

    $ReturnMsg['to'] = str_replace('\"','"',$to);
    $ReturnMsg['cc'] = str_replace('\"','"',$cc);
    $ReturnMsg['bcc'] = str_replace('\"','"',$bcc);
    $ReturnMsg['subject'] = (trim($subject) == "")? "No Subject":$this->Get_Mail_Format(stripslashes(trim($subject)),"UTF-8");
    $ReturnMsg['headers'] = $HeaderFrom .$headers;
    $ReturnMsg['body'] = $mime;

    //echo $mime; die;

    // Kenneth Wong (20080612) Added element headers2 for mail()
    $ReturnMsg['headers2'] = $headers;
    if ($cc != "")
    {
        $ReturnMsg['headers2'] .= "cc: $cc".$this->MimeLineBreak;
    }
    if ($bcc != "")
    {
        $ReturnMsg['headers2'] .= "bcc: $bcc".$this->MimeLineBreak;
    }
    $ReturnMsg['headers2'] .= $HeaderFrom . $this->MimeLineBreak;

    $headers .= $HeaderFrom;
    $headers .= "To: $to".$this->MimeLineBreak;
    $headers .= "cc: $cc".$this->MimeLineBreak;
    $headers .= "bcc: $bcc".$this->MimeLineBreak;
    $headers .= "Subject: ".$this->Get_Mail_Format(stripslashes(trim($subject)),"UTF-8").$this->MimeLineBreak;

    $ReturnMsg['FullMimeMsg'] = $headers.$mime;
    
    $ReturnMsg['SortTo'] = trim(str_replace('"','',$ReturnMsg['to']));
    $ReturnMsg['Boundary'] = $mime_boundary;

    return $ReturnMsg;
  }
  /* commented by Marcus, use imap_8bit/quote_printable_encode/=?UTF-8?Q?.....?= to encode chinese characters seems to be more difficult to decode, 
 * changed to encode by base64 method below
  function Get_Mail_Format1($string, $encoding='UTF-8') {
	  $string = str_replace(" ", "_", trim($string)) ;
	  // We need to delete "=\r\n" produced by imap_8bit() and replace '?'
	  $string = str_replace("?", "=3F", str_replace("=\r\n", "", imap_8bit($string))) ;
	
	  // Now we split by \r\n - i'm not sure about how many chars (header name counts or not?)
	  $string = chunk_split($string, 73);
	  // We also have to remove last unneeded \r\n :
	  $string = substr($string, 0, strlen($string)-2);
	  // replace newlines with encoding text "=?UTF ..."
	  $string = str_replace("\r\n", "?=".chr(13).chr(10)." =?".$encoding."?Q?", $string) ;
	
	  return '=?'.$encoding.'?Q?'.$string.'?=';
	}
	*/
	/* 2011-02-09
	function Get_Mail_Format($string, $encoding='UTF-8') {
	  //$string = str_replace(" ", "_", trim($string)) ;
	  // We need to delete "=\r\n" produced by imap_8bit() and replace '?'
	 // $string = str_replace("?", "=3F", str_replace("=\r\n", "", imap_8bit($string))) ;
		$string = base64_encode($string);
	
	  // Now we split by \r\n - i'm not sure about how many chars (header name counts or not?)
	  $string = chunk_split($string, 72);
	  // We also have to remove last unneeded \r\n :
	  $string = substr($string, 0, strlen($string)-2);
	  // replace newlines with encoding text "=?UTF ..."
	  $string = str_replace("\r\n", "?=".chr(13).chr(10)." =?".$encoding."?B?", $string) ;
	
	$encoded_string = '=?'.$encoding.'?B?'.$string.'?=';
	  return $encoded_string;
	}
	*/
	function Get_Mail_Format($string, $encoding='UTF-8') {
	  //$string = str_replace(" ", "_", trim($string)) ;
	  // We need to delete "=\r\n" produced by imap_8bit() and replace '?'
	 // $string = str_replace("?", "=3F", str_replace("=\r\n", "", imap_8bit($string))) ;
		$string = base64_encode($string);
	  // Now we split by \r\n - i'm not sure about how many chars (header name counts or not?)
	//  $string = chunk_split($string, 72);
	//  debug_pr($string);
	  // We also have to remove last unneeded \r\n :
	//  $string = substr($string, 0, strlen($string)-2);
	//  debug_pr($string);
	  // replace newlines with encoding text "=?UTF ..."
	  	$string = str_replace("\r\n", "?=".chr(13).chr(10)." =?".$encoding."?B?", $string) ;
		$encoded_string = '=?'.$encoding.'?B?'.$string.'?=';
		
	  	return $encoded_string;
	}
	
	// test function for bulk mail
	/*function Build_Mime_Text_Bulk($subject,$message,$from,$receiver_to=array(),$AttachList=array(),$IsImportant="",$DraftFlag=false, $Preference="") {
  	global $PartNumber, $AttachPartNumber, $DraftUID;
  	global $Folder, $ActionUID, $IMapCache;
		
		$UID = ($DraftFlag)? $DraftUID:$ActionUID;
    # Content-type of attachment
    $type="application/octet-stream";

    $priority = ($IsImportant==1)? 1: 3;
   	
    # receiving email addresses
    for ($i=0; $i< sizeof($receiver_to); $i++) {
      if (trim($receiver_to[$i]) != "") {
      	$FinalReceiverTo[] = $receiver_to[$i];
      }
    }
    for ($i=0; $i< sizeof($receiver_cc); $i++) {
      if (trim($receiver_cc[$i]) != "") {
        $FinalReceiverCc[] = $receiver_cc[$i];
      }
    }
    for ($i=0; $i< sizeof($receiver_bcc); $i++) {
      if (trim($receiver_bcc[$i]) != "") {
        $FinalReceiverBcc[] = $receiver_bcc[$i];
      }
    }
    $numTo = sizeof($FinalReceiverTo);
    $numCC = sizeof($FinalReceiverCc);
    $numBCC = sizeof($FinalReceiverBcc);
    
    if (($numTo+$numCC+$numBCC) > $this->MaxRecipientSize)
    
    if (!$DraftFlag) {
    	if ($numTo + $numCC + $numBCC == 0) return false;
    }

    $to = ($numTo != 0)? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverTo))) : "";
    $cc = ($numCC != 0)? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverCc))) : "";
    $bcc = ($numBCC != 0)? str_replace("\'","'",str_replace('\"','"',implode(", ",$FinalReceiverBcc))) : "";
    
    # Mail Header
    $headers='';
    // have peference
    if ($Preference!="") {
    	$fromName = (trim($Preference[0]["DisplayName"]) == "")? substr($from,0,stripos($from,"@")):$Preference[0]["DisplayName"];
      $ReplyAddress = (trim($Preference[0]["ReplyEmail"]) != "")? $Preference[0]["ReplyEmail"]:$from;
      $HeaderFrom .= "From: \"$fromName\" <$from>".$this->MimeLineBreak;

      $ReturnMsg['From'] = trim('"'.$fromName.'" <'.$from.'>');
			$ReturnMsg['SortFrom'] = str_replace('"','',$ReturnMsg['From']);
    }
    // for auto mail, e.g password reset
    else {
    	$HeaderFrom .= 'From: '.$from.$this->MimeLineBreak;
      $ReplyAddress = $from;
    }

    for ($i=0; $i< sizeof($AttachList); $i++) {
      $files[] = $AttachList[$i][2];
      $fileRealName[] = $AttachList[$i][1];
    }

    $charset = "UTF-8";

    $isHTMLMessage = 1;

    $isMulti = (sizeof($files) > 0 || sizeof($AttachPartNumber) > 0);

    $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
    $mime_boundary2 = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for multi-part

    if ($isMulti) {
      $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
      $headers .= "X-Priority: $priority".$this->MimeLineBreak;
      $headers .= "Content-Type: multipart/mixed; ";
      $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
    else if ($isHTMLMessage)         # HTML message w/o attachments
    {
      $headers .= "MIME-Version: 1.0".$this->MimeLineBreak;
      $headers .= "X-Priority: $priority".$this->MimeLineBreak;
      $headers .= "Content-Type: multipart/alternative; ";
      $headers .= " boundary=\"" . $mime_boundary . "\"".$this->MimeLineBreak;
    }
    if ($Preference == "")
			$headers .= $HeaderFrom;

    $headers .= "Date: " . date("r") .$this->MimeLineBreak;


    if ($ReplyAddress != "") {
    	$headers .= "Reply-To: $ReplyAddress".$this->MimeLineBreak;
    }

    if ($isMulti || $isHTMLMessage) {
	    $mime = "This is a multi-part message in MIME format.".$this->MimeLineBreak;
	    $mime .= $this->MimeLineBreak;
	    $mime .= "--" . $mime_boundary . $this->MimeLineBreak;
    }

    if (!$isMulti) { # No attachments
    	if ($isHTMLMessage) {
        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        $text_message = Remove_HTML_Tags($message);
        #$encoded_text_message = QuotedPrintableEncode($message);
        $encoded_text_message = chunk_split(base64_encode($text_message));
        $mime .= $encoded_text_message. $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--". $mime_boundary . $this->MimeLineBreak;
        # HTML part
        $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$mime .= "<HTML><BODY>\n".QuotedPrintableEncode($message)."</BODY></HTML>\n";
        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary . "--".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
      }
      else {
      	$mime .= $message;
      }
    }
    else {
      if ($isHTMLMessage) {
        $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary2\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary2 . $this->MimeLineBreak;
        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$mime .= QuotedPrintableEncode(Remove_HTML_Tags($message)). $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= chunk_split(base64_encode(Remove_HTML_Tags($message))). $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--". $mime_boundary2 . $this->MimeLineBreak;
        # HTML part
        $mime .= "Content-Type: text/html; charset=\"$charset\"".$this->MimeLineBreak;
        #$mime .= "Content-Transfer-Encoding: quoted-printable".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        #$mime .= QuotedPrintableEncode($message);
        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".$message."</BODY></HTML>\n"));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary2. "--".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;
        $mime .= "--" . $mime_boundary. "".$this->MimeLineBreak;
      }
      else {
        # Message Body
        $mime .= "Content-Transfer-Encoding: 7bit".$this->MimeLineBreak;
        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$this->MimeLineBreak;
        $mime .= $this->MimeLineBreak;

        $mime .= $message;
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary. $this->MimeLineBreak;
      }
      $filename_encoding = "=?".strtoupper($charset)."?B?";

      # Embed attachment files
      for ($i=0; $i<sizeof($files); $i++) {
        //$target = $files[$i];
        $data = Get_File_Content($files[$i]);
        $fname = addslashes($fileRealName[$i]);

        $mime .= "Content-Type: $type;".$this->MimeLineBreak;
        $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak;
        $mime .= "Content-Transfer-Encoding: base64".$this->MimeLineBreak;
        $mime .= "Content-Disposition: attachment;".$this->MimeLineBreak;
        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= chunk_split(base64_encode($data));
        $mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary;

        # Different if end of email
        if ($i==sizeof($files)-1 && sizeof($AttachPartNumber) == 0)
        	$mime .= "--".$this->MimeLineBreak;
        else
          $mime .= $this->MimeLineBreak;
     	}
     	
     	for ($i=0; $i< sizeof($AttachPartNumber); $i++) {
     		$AttachmentContent = $IMapCache->Get_Attachment($AttachPartNumber[$i],$UID,$Folder);
     		
     		if ($AttachmentContent['MimeType'] == "message/rfc822") {
		     	$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: 7bit'.$this->MimeLineBreak;
					$mime .= 'Content-Disposition: attachment'.$this->MimeLineBreak.$this->MimeLineBreak;
					
					$mime .= $AttachmentContent['Content'];
				}
				else {
					
					$mime .= 'Content-Type: '.$AttachmentContent['MimeType'].';'.$this->MimeLineBreak;
	     		$mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak;
					$mime .= 'Content-Transfer-Encoding: base64'.$this->MimeLineBreak;
					$mime .= 'Content-Disposition: attachment;'.$this->MimeLineBreak;
	        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($AttachmentContent['FileName']))."?=\"".$this->MimeLineBreak.$this->MimeLineBreak;
	        
	        $mime .= chunk_split(base64_encode($AttachmentContent['Content']));
				}     		
				
				$mime .= $this->MimeLineBreak.$this->MimeLineBreak;
        $mime .= "--" . $mime_boundary;
        
        if ($i==sizeof($AttachPartNumber)-1)
        	$mime .= "--".$this->MimeLineBreak;
        else
          $mime .= $this->MimeLineBreak;
     	}
		}

    $ReturnMsg['to'] = str_replace('\"','"',$to);
    $ReturnMsg['cc'] = str_replace('\"','"',$cc);
    $ReturnMsg['bcc'] = str_replace('\"','"',$bcc);
    $ReturnMsg['subject'] = (trim($subject) == "")? "No Subject":$this->Get_Mail_Format($subject,"UTF-8");
    $ReturnMsg['headers'] = $headers;
    $ReturnMsg['body'] = $mime;

    // Kenneth Wong (20080612) Added element headers2 for mail()
    $ReturnMsg['headers2'] = $headers;
    if ($cc != "")
    {
        $ReturnMsg['headers2'] .= "cc: $cc".$this->MimeLineBreak;
    }
    if ($bcc != "")
    {
        $ReturnMsg['headers2'] .= "bcc: $bcc".$this->MimeLineBreak;
    }
    $ReturnMsg['headers2'] .= $HeaderFrom . $this->MimeLineBreak;

    $headers .= $HeaderFrom;
    $headers .= "To: $to".$this->MimeLineBreak;
    $headers .= "cc: $cc".$this->MimeLineBreak;
    $headers .= "bcc: $bcc".$this->MimeLineBreak;
    $headers .= "Subject: ".$this->Get_Mail_Format($subject,"UTF-8").$this->MimeLineBreak;

    $ReturnMsg['FullMimeMsg'] = $headers.$mime;
    
    $ReturnMsg['SortTo'] = trim(str_replace('"','',$ReturnMsg['to']));
    $ReturnMsg['Boundary'] = $mime_boundary;

    return $ReturnMsg;
  }*/
  
  function Check_Duplicate_Mail_Record($Folder) {
  	if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
  	$ldb = new database(true);
  	$sql = "exec Check_Mail_Duplicate '".$this->MailAddress."', ".UTF8_To_DB_Handler($Folder);
  	$Result = $ldb->returnArray($sql);
  	Write_Message_Log("Log","Check for duplicate cache record in folder \"$Folder\"");
  	if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Check duplicate record): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Check duplicate record): '.$this->Stop_Timer().'<br>';
    }
    
  	return $Result;
  }
  
  function Delete_Duplicate_MailRecord($Folder) {
  	if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
  	$ldb = new database(true);
  	$DuplicateList = $this->Check_Duplicate_Mail_Record($Folder);
  	
  	for ($i=0; $i< sizeof($DuplicateList); $i++) {
  		$sql = "exec Process_Mail_Duplicate 
  							'".$this->MailAddress."', 
  							".UTF8_To_DB_Handler($Folder).", 
  							".$DuplicateList[$i][0].",
  							".$DuplicateList[$i][1];
  		$ldb->db_db_query($sql);
  		Write_Message_Log("Log","Delete duplicate cache record of UID: \"$DuplicateList[$i][0]\" in folder \"$Folder\"");
  	}
  	
  	if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Delete duplicate record): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Delete duplicate record): '.$this->Stop_Timer().'<br>';
    }
  	
  	return true;
  }
  
  function Get_Unchain_UID($Folder,$UIDList="") {
  	if ($UIDList == "") {
	  	$ldb = new database(true);
	  	$sql = 'Select 
	  						MessageUID 
	  					From 
	  						Mail_Local_List 
	  					With (Nolock)
	  					Where 
	  						EmailAddress = \''.$this->MailAddress.'\'
	  						And 
	  						CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Folder).'
	  					Order By
	  						MessageUID';
	  	$UIDList = $ldb->returnArray($sql);
		}
		
		for ($i=0; $i< sizeof($UIDList); $i++) {
			if ($i > 0) {
				//echo ($UIDList[$i-1][0]+1).'-'.$UIDList[$i][0].'<br>';
				if ($UIDList[$i][0] > ($UIDList[$i-1][0]+1)) {
					$Temp['FromID'] = $UIDList[$i-1][0];
					$Temp['ToID'] = $UIDList[$i][0];
					
					$CacheGap[] = $Temp;
				}
			}
		}
		
  	$ErrFlag = false;
		for ($k=0; $k< sizeof($CacheGap); $k++) {
			$FromMsgNo[$k] = $this->Get_MsgNo($CacheGap[$k]['FromID']);
			$ToMsgNo[$k] = $this->Get_MsgNo($CacheGap[$k]['ToID']);
			
			if ($FromMsgNo[$k] == false) {
				$this->Delete_Cached_Message($FolderName, $CacheGap[$k]['FromID'], $this->MailAddress);
				$RemoveArray[] = $CacheGap[$k]['FromID'];
				$ErrFlag = true;
			}
			
			if ($ToMsgNo[$k] == false) {
				$this->Delete_Cached_Message($FolderName, $CacheGap[$k]['ToID'], $this->MailAddress);
				$RemoveArray[] = $CacheGap[$k]['ToID'];
				$ErrFlag = true;
			}
		}	
		
		if ($ErrFlag) {
			for ($i=0; $i< sizeof($UIDList); $i++) {
				if (in_array($UIDList[$i][0],$RemoveArray)) {
					$UIDList = Array_Remove($UIDList,$i);
					$i--;
				}
			}
			
			return $this->Get_Unchain_UID($Folder,$UIDList);
		}
		else {
			$Result['TotalNumber'] = 0;
			for ($k=0; $k< sizeof($CacheGap); $k++) {
				if ($FromMsgNo[$k] != ($ToMsgNo[$k]-1)) {
					$Temp = $ToMsgNo[$k]-$FromMsgNo[$k]-1;
					$Result['Pairs'][] = array($FromMsgNo[$k]+1,$ToMsgNo[$k]-1);
					$Result['TotalNumber'] += $Temp;
				}
			}
			
			return (is_null($Result))? array():$Result;
		}
  }
  
  function Get_Related_Inline_Info($Folder,$UID,$Parts="",$PartNumber="") {
  	global $SYS_CONFIG, $PathRelative, $PATH_WRT_ROOT;
  	
  	//$personal_path = $PathRelative."src/module/email/view_mail_folder/".$SYS_CONFIG['SchoolCode']."/u".$_SESSION['SSV_USERID']."/related/";
  	//$personal_path = $IMap->user_file_path."/related/";
  	$personal_path = $PATH_WRT_ROOT."file/gamma_mail/u".$_SESSION['UserID'].""."/related/";
  	
  	$EncodedFolder = IMap_Encode_Folder_Name($Folder);
  	
  	if (trim($PartNumber) != "") {
  		//echo trim($PartNumber); die;
  		$StructureArray = $this->Get_Target_PartNumber($PartNumber,$StructureArray);
  		$StructureArray->ctype_primary = $StructureArray->headers['c_primary'];
	  	$StructureArray->ctype_secondary = $StructureArray->headers['c_secondary'];
	  	$StructureArray->mimetype = substr($StructureArray->headers['content-type'],0,strlen($MessagePart->headers['content-type'])-1);
  	}
	  else {
	  	if ($Parts == "")
	  		$StructureArray = $this->Get_Message_Structure($UID,$EncodedFolder);
	  	else 
	  		$StructureArray = $Parts;
  		//echo '2222'; die;
  	}
  	//debug_r($Folder);
  	//debug_r($StructureArray);
  	if (strtolower($StructureArray->ctype_primary) == 'multipart') {
  		// alternative
  		if (strtolower($StructureArray->ctype_secondary) == 'alternative') {
  			for ($i=0; $i< sizeof($StructureArray->parts); $i++) {
	  			if (strtolower($StructureArray->parts[$i]->ctype_primary) == 'multipart') {
	  				$Temp = $this->Get_Related_Inline_Info($Folder,$UID,$StructureArray->parts[$i]);
	  				if (is_array($Temp) && is_array($InlineFileList)) {
	  					$InlineFileList = array_merge($InlineFileList,$Temp);
	  				}
	  				else if (is_array($Temp)) {
	  					$InlineFileList = $Temp;
	  				}
	  			}
	  		}
  		}
  		// related
  		else if (strtolower($StructureArray->ctype_secondary) == 'related') {	
	  		for ($i=0; $i< sizeof($StructureArray->parts); $i++) {
	  			if (strtolower($StructureArray->parts[$i]->ctype_primary) == 'multipart') {
	  				$Temp = $this->Get_Related_Inline_Info($Folder,$UID,$StructureArray->parts[$i]);
	  				if (is_array($Temp) && is_array($InlineFileList)) {
	  					$InlineFileList = array_merge($InlineFileList,$Temp);
	  				}
	  				else if (is_array($Temp)) {
	  					$InlineFileList = $Temp;
	  				}
	  			}
	  			
	  			if (strtolower($StructureArray->parts[$i]->disposition) == 'inline') {
						$Attachment = $this->Get_Attachment($StructureArray->parts[$i]->mime_id,$UID,$Folder,$StructureArray->parts[$i]);
						$tmp_content_id = trim(str_replace(array("<",">"),array("",""),$StructureArray->parts[$i]->content_id));
						if (trim($Attachment['FileName']) != "" && $tmp_content_id!="") {
							$Temp['FileContentID'] = "cid:".str_replace(array("<",">"),array("",""),$StructureArray->parts[$i]->content_id);
							//$Temp['FileContentID'] = str_replace(array("<",">"),array("",""),$StructureArray->parts[$i]->content_id);
							$Temp['RealContentID'] = $StructureArray->parts[$i]->content_id;
							$Temp['FilePath'] = $personal_path.$tmp_content_id."/".$Attachment['FileName'];
							//$Temp['FilePath'] = $Attachment['FileName'];
							$Temp['PartNumber'] = $StructureArray->parts[$i]->mime_id;
							$InlineFileList[] = $Temp;
						}
	  			}
	  		}
  		}
  		else {
  			for ($i=0; $i< sizeof($StructureArray->parts); $i++) {
	  			if (strtolower($StructureArray->parts[$i]->ctype_primary) == 'multipart') {
	  				$Temp = $this->Get_Related_Inline_Info($Folder,$UID,$StructureArray->parts[$i]);
	  				if (is_array($Temp) && is_array($InlineFileList)) {
	  					$InlineFileList = array_merge($InlineFileList,$Temp);
	  				}
	  				else if (is_array($Temp)) {
	  					$InlineFileList = $Temp;
	  				}
	  			}
	  		}
  		}
  		
  		if (is_null($InlineFileList)) 
  			return false;
  		else
  			return $InlineFileList;
  	}
		else {
			return false;
		}
  }
  
  function Get_Attachment_List($StructureArray,$Folder,$UID) {
  	if ($StructureArray->parts) {
  		$StructureParts = $StructureArray->parts;
  	}
  	else {
  		if (trim($StructureArray->mime_id) == "0")
  			$StructureParts = array($StructureArray);
  		else 
  			$StructureParts = array();
  	}

  	$AttachmentCount = 0;
  	$AttachList = array();
  	for ($i=0; $i< sizeof($StructureParts); $i++) {
  		
  		if (strtolower($StructureParts[$i]->ctype_primary) == 'multipart' && $StructureArray->parts) {
  			$Temp = $this->Get_Attachment_List($StructureParts[$i],$Folder,$UID);
  			if (is_array($Temp)) 
  				$AttachList = array_merge($AttachList,$Temp);
  		}
  		
  		if (strtolower($StructureParts[$i]->ctype_primary) == "application" && strtolower($StructureParts[$i]->ctype_secondary) == "ms-tnef") {
    		
    		$Temp = $this->Extract_Tnef_Attachment($StructureParts[$i]->mime_id,$Folder,$UID);
    		
    		if (sizeof($Temp) > 0){
    			for($k=0;$k<sizeof($Temp);$k++) $Temp[$k]['FileSize'] = round($Temp[$k]['FileSize']/1024,2);
    			$AttachList = array_merge($AttachList,$Temp);
    		}
    	}
  		
  		if (strtolower($StructureParts[$i]->disposition) == 'attachment' ||  
  				(strtolower($StructureParts[$i]->disposition) == 'inline' && strtolower($StructureParts[$i]->ctype_primary) != 'text') || 
  				trim($StructureParts[$i]->filename) != "") {
  			// check if the file is not tnef file or apple single/double, applefile
  			if (!(strtolower($StructureParts[$i]->ctype_primary) == "application" && strtolower($StructureParts[$i]->ctype_secondary) == "ms-tnef") 
  			/* && !(strtolower($StructureParts[$i]->ctype_primary) == "application" && strtolower($StructureParts[$i]->ctype_secondary) == 'applefile') */ ) {
	  			if (trim($StructureParts[$i]->filename) != "" || count($StructureParts[$i]->d_parameters)>0) {
		  			//$Temp['FileName'] = $StructureParts[$i]->filename;
		  			$Temp['FileName'] = $this->getImapMessagePartAttachmentFileName($StructureParts[$i]);
		  			$Temp['FileSize'] = round(($StructureParts[$i]->size / 1.37)/1024,2); // base64 data is 1.37 times larger
		  			$Temp['PartID'] = $StructureParts[$i]->mime_id;
		  			$Temp['AttachType'] = ($StructureParts[$i]->ctype_primary == "message" && $StructureParts[$i]->ctype_secondary == "rfc822")? "Message":"File";
		  			$Temp['IsTNEF'] = false;
		  			$Temp['Disposition'] = strtolower($StructureParts[$i]->disposition);
		  			$AttachList[] = $Temp;
	  			}
	  		}
  		}
  	}
  	
  	return $AttachList;
  }
	
	function Get_Empty_Structure_Cache($Folder) {
		$db = new database(true);
		
		$sql = 'select 
							MessageUID 
						from 
							Mail_Local_List with (nolock) 
						where 	
							EmailAddress = \''.$this->MailAddress.'\'
							And 
							CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Folder).'
							And
							HasAttachment = \'true\'
							And 
							(MessageStructure is NULL
							 Or
							 MessageStructure = \'\')';
		$Result = $db->returnVector($sql);
		
		return (sizeof($Result) > 0)? $Result:false;
	}
	
	function Insert_Cache_Structure($UID,$Folder,$Structure) {
		$ldb = new database(true);
		
		$sql = 'update Mail_Local_List set 
							MessageStructure = \''.$ldb->Get_Safe_Sql_Query($Structure).'\'
						Where 
							EmailAddress = \''.$this->MailAddress.'\' 
							And 
							CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Folder).'
							And 
							MessageUID = '.$UID;
		$ldb->db_db_query($sql);
	}
	
	function Syn_Cache_Structure($Folder="") {
		global $Lang, $SYS_CONFIG; 
		
		if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
		$db = new database(true);
		$TotalMsgNumber = 0;
		$Finished = 0;
		$BatchSize = $SYS_CONFIG['Mail']['BatchSize'];
		
		if ($Folder == "")
		{
			$FolderList = $this->Get_Mail_Folders();
		}
		else
		{
			$FolderList = array
						( 0 => Array
							(
								"FolderAttribute" => "\\Marked \\HasChildren",
            					"FolderFullPath" => $Folder
            				)
						);
		}
		
		for($i=0; $i< sizeof($FolderList); $i++) {	
			$Folder = $this->Go_To_Folder(IMap_Encode_Folder_Name($FolderList[$i]['FolderFullPath']));
			
			// fetch message structure to cache record
			$EmptyList = $this->Get_Empty_Structure_Cache($FolderList[$i]['FolderFullPath']);
			
			for($k=0; $k< sizeof($EmptyList); $k++) {
				$UIDList .= $EmptyList[$k].",";
				if ((($k%$BatchSize) == 0 && $k > 0) || $k == (sizeof($EmptyList)-1)) {
					$UIDList = substr($UIDList,0,-1);
					//echo $UIDList; die;
					$Temp = $this->Get_Message_Raw_Structure($UIDList);
					for ($z=0;$z< sizeof($Temp);$z++) {
						$this->Insert_Cache_Structure($Temp[$z]['UID'],$FolderList[$i]['FolderFullPath'],$Temp[$z]['Structure']);
					}
					
					$UIDList = "";
				}
			}
		}
		
		if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Get Body Struct Cache): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Get Body Struct Cache): '.$this->Stop_Timer().'<br>';
    }
	}
	
	function Extract_Tnef_Attachment($PartNumber,$Folder,$UID) {
		global $PathRelative, $SYS_CONFIG, $IMap;
		
		$lfs = new libfilesystem();
		//$personal_path = $PathRelative."src/module/email/view_mail_folder/".$SYS_CONFIG['SchoolCode']."/u".$_SESSION['SSV_USERID']."/tnef/";
		$personal_path = $IMap->user_file_path."/tnef/";
		
		$CMD = "/usr/local/bin/tnef --overwrite --number-backups";
//		$db = new database(true);
//		$sql = 'Select 
//							CAST(CAST(MailFolder AS varbinary(max)) AS image) as MailFolder,
//							UID,
//							PartNumber 
//						from 
//							Mail_Last_View_Info with (nolock) 
//						where EmailAddress = \''.$this->MailAddress.'\'';
//		$LastViewInfo = $db->returnArray($sql);
//		if (sizeof($LastViewInfo) == 0) {
//			$sql = 'Insert into Mail_Last_View_Info 
//							(EmailAddress,MailFolder,UID,PartNumber,LastViewDate) 
//							Values 
//							(\''.$this->MailAddress.'\','.UTF8_To_DB_Handler($Folder).','.$UID.',\''.$PartNumber.'\',GETDATE())';
//			$db->db_db_query($sql);
//			$SameDAT = false;
//		}
//		else {			
//			$SameDAT = (is_file($personal_path."winmail.dat") &&
//									UTF8_From_DB_Handler($LastViewInfo[0][0]) == $Folder &&
//									$LastViewInfo[0][1] == $UID && 
//									$LastViewInfo[0][2] == $PartNumber);
//				
//			if (!$SameDAT) {
//				$sql = 'Update Mail_Last_View_Info Set
//									MailFolder = '.UTF8_To_DB_Handler($Folder).',
//									UID = '.$UID.',
//									PartNumber = \''.$PartNumber.'\',
//									LastViewDate = GETDATE()
//								Where 
//									EmailAddress = \''.$this->MailAddress.'\'
//								';
//				$db->db_db_query($sql);
//			}
//		}
		
		
		if (!is_dir($personal_path)) {
			$lfs->folder_new($personal_path, 0755, true);
		}
		else {
			if (!$SameDAT) {
				$lfs->folder_remove_recursive($personal_path);
				$lfs->folder_new($personal_path, 0755, true);
			}
		}
		
		if (!$SameDAT) {
			$EncodedFolder = IMap_Encode_Folder_Name($Folder);
			
			$DATFile = $this->Get_Attachment($PartNumber,$UID,$Folder);
			
			if (trim($DATFile['FileName']) != "") {
				$lfs->file_write($DATFile['Content'], $personal_path.$DATFile['FileName']);
				// change to the temp folder to process the .dat file
				chdir($personal_path);
				// exe the tnef command and remove the .dat file after the process
				exec($CMD." ".OsCommandSafe($DATFile['FileName']));
			}
		}
		else {
			chdir($personal_path);
		}

		$AttachList = array();

		$dir = opendir(".");
		//loop the directory to get the extracted files
		while (false!==($file = readdir($dir))) { 
			// do not need the "." and ".." entry
			if ($file != "." && $file != ".." && $file != "winmail.dat" && is_file($file)) { 
			//This filename is not an unwanted directory entry and this is really a file
				$converted_file = $this->Detect_Convert_FileName_To_UTF8($file);
				if($converted_file != $file){ // if it is not utf-8 file name, copy to utf-8 named file
					$lfs->file_copy($file,$converted_file);
					$Temp['FilePath'] = $personal_path.$converted_file;
					$Temp['FileName'] = $converted_file;
					$Temp['MimeType'] = $lfs->getMIMEType($converted_file);
					$Temp['FileSize'] = filesize($converted_file);
					$Temp['IsTNEF'] = true;
					$AttachList[] = $Temp;
				}else{
					$Temp['FilePath'] = $personal_path.$file;
					$Temp['FileName'] = $file;
					$Temp['MimeType'] = $lfs->getMIMEType($file);
					$Temp['FileSize'] = filesize($file);
					$Temp['IsTNEF'] = true;
					$AttachList[] = $Temp;
				}
			} 
		}
		closedir($dir);
		// change back to currently running page path so that the page footer can be include
		chdir(dirname($_SERVER['SCRIPT_FILENAME']));
		
		return $AttachList;
	}
	
	function Get_All_Folder_List() {
	  Global $Lang, $SYS_CONFIG;
	  
	  $FolderList = $this->Get_Mail_Folders();
	  
		$Result = array();
		for ($i=0; $i< sizeof($FolderList); $i++) {
			$Result[] = $FolderList[$i]['FolderFullPath'];
		}
	  	
	  return $Result;
	}
	
	function Get_Search_Table_Data($PageNumber, $PageSize, $sort="Date", $reverse=0, $KeywordFrom="", $KeywordTo="", $KeywordCc="", $KeywordBcc="", $KeywordSubject="", $FromDate, $ToDate) {
		global $Lang, $SYS_CONFIG;
		
		if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
		
		$ldb = new database(true);
		$sql = 'exec Search_Mail_Cache \''.$sort.'\', '.$reverse.', '.$PageSize.', 
							'.$PageNumber.', \''.$this->MailAddress.'\', '.$KeywordFrom.', 
							'.$KeywordTo.', '.$KeywordCc.', '.$KeywordBcc.', 
							'.$KeywordSubject.', '.$FromDate.', '.$ToDate;
		$Result = $ldb->returnArray($sql);
		
		if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Get Adv Search Cache): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Get Adv Search Cache): '.$this->Stop_Timer().'<br>';
    }
				
		for ($i=0; $i< sizeof($Result); $i++) {
			$Result[$i]['MailFolder'] = UTF8_From_DB_Handler($Result[$i]['MailFolder']);
				
			if ($Result[$i]['MailFolder'] != $LastFolder) {
				$Flags = $this->Get_Message_Status($Result[$i]['MessageUID'],IMap_Encode_Folder_Name($Result[$i]['MailFolder']),true);
			}
			else {
				$Flags = $this->Get_Message_Status($Result[$i]['MessageUID'],IMap_Encode_Folder_Name($Result[$i]['MailFolder']),false);
			}
			//var_dump($Flags);
			if (!is_array($Flags)) {
				$this->Delete_Cached_Message($Result[$i]['MailFolder'], $Result[$i]['MessageUID'], $MailAddress);
				$Result[$i]['deleted'] = true;
			}
			else {
				$Result[$i] = array_merge($Result[$i],$Flags);
				if ($Result[$i]['HasAttachment'] && !$Result[$i]['ReadFlag'] && !$Result[$i]['Checked']) {
					$Temp = $this->Get_Message_Structure($Result[$i]['MessageUID'],IMap_Encode_Folder_Name($Result[$i]['MailFolder']),$Result[$i]['MessageStructure'],false);
					$Result[$i]['HasAttachment'] = ($this->Check_Attach_Exists($Temp));
					$TempInt = ($Result[$i]['HasAttachment'])? 1:0;
					$sql = 'Update MAIL_LOCAL_LIST Set 
										HasAttachment = '.$TempInt.', 
										LastModified = GetDate()
									where 
										EmailAddress = \''.$MailAddress.'\' 
										AND 
										CAST(MailFolder AS varbinary(8000)) = '.UTF8_To_DB_Handler($Result[$i]['MailFolder']).' 
										AND 
										MessageUID = '.$Result[$i]['MessageUID'];
					$ldb->db_db_query($sql);
				}
			}

			$FromList = UTF8_From_DB_Handler($Result[$i]['HeaderFrom']);
			/*preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", trim($FromList), $matches, PREG_OFFSET_CAPTURE);
			$Result[$i]['HeaderFrom'] = ($matches[0][0][1] == 0)? $matches[0][0][0]:trim(substr($FromList,0,$matches[0][0][1]-1));*/
			$FromList = imap_rfc822_parse_adrlist($FromList,"");
			if (trim($FromList[0]->personal) != "") {
				$Result[$i]['HeaderFrom'] = $FromList[0]->personal; 
			}
			else {
				$Host = ('.SYNTAX-ERROR.' != $FromList[0]->host)? "@".$FromList[0]->host:"";
				
				$Result[$i]['HeaderFrom'] = $FromList[0]->mailbox.$Host;
			}
			if (sizeof($FromList) > 1) $Result[$i]['HeaderFrom'] .= '...';
			
			//unset($matches);
			$ToList = UTF8_From_DB_Handler($Result[$i]['HeaderTo']);
			/*preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", trim($ToList), $matches, PREG_OFFSET_CAPTURE);
			
			if (sizeof($matches[0]) > 1) {
				$Result[$i]['HeaderTo'] = ($matches[0][0][1] == 0)? $matches[0][0][0]:trim(substr($ToList,0,$matches[0][0][1]-1));
				$Result[$i]['HeaderTo'] .= '...';
			}
			else if (sizeof($matches[0]) == 0) 
				$Result[$i]['HeaderTo'] = $Lang['email']['NoReceiver'];
			else
				$Result[$i]['HeaderTo'] = ($matches[0][0][1] == 0)? $matches[0][0][0]:trim(substr($ToList,0,$matches[0][0][1]-1));*/
			$ToList = imap_rfc822_parse_adrlist($ToList,"");
			if (sizeof($ToList) > 0) {
				if (trim($ToList[0]->personal) != "") {
					$Result[$i]['HeaderTo'] = $ToList[0]->personal; 
				}
				else {
					$Host = ('.SYNTAX-ERROR.' != $ToList[0]->host)? "@".$ToList[0]->host:"";
					
					$Result[$i]['HeaderTo'] = $ToList[0]->mailbox.$Host;
				}
				if (sizeof($ToList) > 1) $Result[$i]['HeaderTo'] .= '...';
			}
			else $Result[$i]['HeaderTo'] = $Lang['email']['NoReceiver'];

			$Result[$i]['HeaderCc'] = UTF8_From_DB_Handler($Result[$i]['HeaderCc']);
			$Result[$i]['HeaderBcc'] = UTF8_From_DB_Handler($Result[$i]['HeaderBcc']);
			$Result[$i]['Subject'] = UTF8_From_DB_Handler($Result[$i]['Subject']);
			$LastFolder = $Result[$i]['MailFolder'];
		}
		/*echo '<pre>';
		var_dump($Result);
		echo '</pre>'; */
		//$Socket->Close_Connect();

		return $Result;
	}
	
	function Get_Mail_Search_Count($KeywordFrom,$KeywordTo,$KeywordCc,$KeywordBcc,$KeywordSubject,$FromDate,$ToDate) {
		if ($this->ShowTime && $this->ShowLevel == 0) 
			$this->Start_Timer();
			
		$ldb = new database(true);
			
		$sql = 'exec Search_Mail_Cache_Count \''.$this->MailAddress.'\', '.$KeywordFrom.', 
							'.$KeywordTo.', '.$KeywordCc.', '.$KeywordBcc.', '.$KeywordSubject.', '.$FromDate.', '.$ToDate;
		//echo $sql.'<br>';
		$Result = $ldb->returnVector($sql);
		
		if ($this->ShowTime && $this->ShowLevel == 0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Cache: Time Different (Get search cache count): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Get search cache count): '.$this->Stop_Timer().'<br>';
    }
		return $Result[0];
	}
	
	// get folder Structure for UI use
	function Get_Folder_Structure ($Shift=false, $IncludeDefaultFolder=true) {
		Global $Lang, $SYS_CONFIG;

		$FolderList = $this->Get_Mail_Folders();

		$FolderArray[] = array($Lang['email']['MoveToFolder'],'');
		if ($IncludeDefaultFolder) {
			  // please default folder list at top
			  $FolderArray[] = array($Lang['email']['DefaultFolderList'],'Default','LabelGroupStart');
			  foreach ($this->DefaultFolderList as $Key => $Val) {
				$FolderArray[] = array($this->Get_Mail_Display_Folder($Val),$Val);
			  }
			  $FolderArray[] = array('','','LabelGroupEnd');
			  $FolderArray[] = array($Lang['email']['PersonalFolderList'],'Personal','LabelGroupStart');
			}

		for ($i=0; $i< sizeof($FolderList); $i++) {
			foreach ($this->DefaultFolderList as $Key => $Val) {
				if ($FolderList[$i]['FolderFullPath'] == $this->FolderPrefix) {
						continue 2;
				}
				//if (stristr($FolderList[$i],$defaultFolderList[$j]) !== FALSE && $defaultFolderList[$j] != $this->FolderPrefix) {
				if ($FolderList[$i]['FolderFullPath'] == $Val && $Val != $this->FolderPrefix) {
						continue 2;
				}
			}
		
			$Level = substr_count($FolderList[$i]['FolderFullPath'],$this->FolderDelimiter);
			$LevelSpaces = "";
			for ($j=0; $j< $Level; $j++) {
					$LevelSpaces .= '&nbsp;&nbsp;';
			}
			$FolderDisplayName = $this->Get_Mail_Display_Folder($FolderList[$i]['FolderFullPath']);
		
			$FolderArray[] = array($LevelSpaces.$FolderDisplayName,$FolderList[$i]['FolderFullPath']);
		}
	
		if ($IncludeDefaultFolder) {
			$FolderArray[] = array('','','LabelGroupEnd');
		}
		if ($Shift) {
			array_shift($FolderArray);
		}
	
		return $FolderArray;
	}
	
	function Get_File_Extension_From_Mime_Type($mime_type)
	{
		$mapping = array(

             'text/plain' => 'txt',
             'text/html' => 'htm',
             'text/html' => 'html',
            'text/html' => 'php',
            'text/css' => 'css',
              'application/javascript'=>'js',
             'application/json' =>'json',
              'application/xml'=>'xml',
            'application/x-shockwave-flash' => 'swf',
              'video/x-flv'=>'flv',

            // images
           'image/png'  => 'png',
           'image/jpeg'  => 'jpe',
            'image/jpeg' => 'jpeg',
           'image/jpeg'  => 'jpg',
             'image/gif' =>'gif',
         'image/bmp'  =>  'bmp' ,
           'image/vnd.microsoft.icon'  => 'ico',
           'image/tiff' => 'tiff' ,
            'image/tiff'  =>'tif',
           'image/svg+xml'  => 'svg',
            'image/svg+xml' => 'svgz',

            // archives
            'application/zip' => 'zip',
            'application/x-rar-compressed' => 'rar',
            'application/x-msdownload' =>'exe' ,
           'application/x-msdownload' => 'msi' ,
            'application/vnd.ms-cab-compressed' => 'cab',

            // audio/video
            'audio/mpeg' => 'mp3',
           'video/quicktime' => 'qt' ,
          'video/quicktime'  => 'mov' ,

            // adobe
            'application/pdf'  =>'pdf',
           'image/vnd.adobe.photoshop' =>  'psd',
          'application/postscript' =>  'ai' ,
          'application/postscript'  =>  'eps',
           'application/postscript'  => 'ps',

            // ms office
            'application/msword' =>'doc' ,
           'application/rtf' => 'rtf' ,
             'application/vnd.ms-excel'=> 'xls',
           'application/vnd.ms-powerpoint'  => 'ppt',

            // open office
          'application/vnd.oasis.opendocument.text'  =>  'odt',
           'application/vnd.oasis.opendocument.spreadsheet'  => 'ods',
           
           'application/octet-stream' => ''
        );
        
        $file_ext = '';
        $mime_type = strtolower($mime_type);
        if(isset($mapping[$mime_type]) && $mapping[$mime_type]!=''){
        	$file_ext = $mapping[$mime_type];
        }
        
        return $file_ext;
	}
	
	/*
	 * Get attachment file name from either 
	 * 1) part_obj->filename
	 * 2) part_obj->d_parameters
	 *    2.1) filename or
	 * 	  2.2) [filename*0*,filename*1*,...] 
	 */
	function getImapMessagePartAttachmentFileName($part)
	{
		$return_filename = $part->filename;
		if($part->filename == '' && isset($part->d_parameters) && sizeof($part->d_parameters)>0)
		{
			global $IMap;
			if(!($IMap instanceof imap_gamma)){
				$IMap = new imap_gamma();
			}
			
			$filename_keys = array_keys($part->d_parameters);
			if(preg_match('/^filename\\*\d+\\*$/',$filename_keys[0])){
				$whole_filename = '';
				foreach($part->d_parameters as $key => $val){
					$whole_filename .= $val;
				}
				if(preg_match('/^(.+)\'(.*)\'(.+)$/',$whole_filename, $matches)){
					$return_filename = rawurldecode($matches[3]);
				}else{
					$return_filename = rawurldecode($whole_filename);
				}
			}else if($part->d_parameters['filename']!=''){
				$return_filename = $IMap->MIME_Decode($part->d_parameters['filename']);
			}
		}
		
		return $return_filename;
	}
	
	function MIME_Decode($str)
	{
		$elements = imap_mime_header_decode($str);
		for($k=0 ; $k<count($elements) ; $k++) {
			if(trim($elements[$k]->text) != '')
			{
				$lc_charset = strtolower($elements[$k]->charset);
				if($lc_charset=="utf-8"){
					$tmp .= $elements[$k]->text;
				}else if(in_array($lc_charset, array("utf-8","default","x-unknown","unknown")) || stristr($elements[$k]->charset,"unknown")){
					$charset = mb_detect_encoding($elements[$k]->text, 'UTF-8, BIG5, GB2312, GBK, ASCII');
					if($charset != "" && strtolower($charset) != 'utf-8')
						$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8",$charset);
					else
						$tmp .= $elements[$k]->text;
				}else if($lc_charset=="gb2312" || $lc_charset=="gb18030"){
					$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8","GB2312,GBK");
				}else if($lc_charset == "big5-hkscs"){
					$tmp .= @iconv($lc_charset,"UTF-8",$elements[$k]->text);
				}else if($lc_charset == "ks_c_5601-1987"){
					$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8","CP949");
				}
				else{ 
					$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8",$elements[$k]->charset);
				}
			}
		}	
		
		return $tmp;
	}
	
	function Detect_Convert_FileName_To_UTF8($filename)
	{
		$output_filename = $filename;
		$charset = mb_detect_encoding($filename, 'UTF-8, BIG5, GB2312, GBK,  ASCII');
		if($charset != "" && strtolower($charset) != 'utf-8'){
			if(in_array(strtolower($charset),array("big5-hkscs","big5","big-5"))){
				$output_filename = @iconv($charset,"UTF-8",$filename);
			}else{
				$output_filename = @mb_convert_encoding($filename,"UTF-8",$charset);
			}
		}
		
		return $output_filename;
	}
}
?>