<?php
## Using By :             
/*
###########################################
## Modification Log:
##
## 2018-12-11 Cameron
##      - add SubHeading to print in getClassWeeklyScheduleLayout()
##      - remove hyper link of the lesson when print in getClassWeeklyScheduleTable()
##
## 2018-10-22 Cameron
##      - modify getClassTrainingReportData() to cater course-class and lesson-class structure
##
## 2018-10-19 Cameron
##      - retrieve InstructorID in getClassWeeklyScheduleData(), getClassWeeklyScheduleTable() so that it can restrict instructor to edit other's record
##
## 2018-10-15 Cameron
##      - modify getClassWeeklyScheduleData() to suit new lesson-class structure
##      - modify getClassWeeklyScheduleTable() to give link to edit lesson schedule
##
## 2018-09-19 Cameron
##      - add addLesson class to available timeslot div so that it can be used for setting full td cell to show hyperlink in getClassWeeklyScheduleTable()
##      - apply intake period restriction (out of range checking) to addLesson in getClassWeeklyScheduleTable()
##
## 2018-09-12 Cameron
##      - add function getTimeslotSettingLayout()
##      - revised getClassWeeklyScheduleTable() and getMappingOfEventDateIDtoTimeslot() to show add new lesson button in availabe timeslot
##
## 2018-09-06 Cameron [ip.2.5.9.10.1]:
##      - add time interval ($minuteStep=1) to getTimeSel() in getClassWeeklyScheduleTable() 
##      - add parameter $className to getClassWeeklyScheduleTable(), hide interval setting and show className if it's not empty
##
## 2018-09-03 Cameron [ip.2.5.9.10.1]: 
##      - show all lessons row by row even they are overlapped in the same date and time in getClassWeeklyScheduleTable()
##      - fix: include INTRANET_ENROL_EVENT_DATE.RecordStatus=null in getClassWeeklyScheduleData() 
##
## 2018-08-29 Cameron [ip.2.5.9.10.1]: add function getDisplayWeekByCal(), getClassWeeklyScheduleLayout(), getClassWeeklyScheduleData(), getMappingOfEventDateIDtoTimeslot(), getClassWeeklyScheduleTable()
##
## 2018-08-27 Ivan [X145096] [ip.2.5.9.10.1]: added getClassMembershipReportHeaderAry(), getClassMembershipReportResultAry(), getClassMembershipReportResultTableHtml(), getClassMembershipReportResultExportAry() to add class teacher report customization ($sys_custom['eEnrolment']['ClassTeacherView'])
##
## 2018-08-22 Cameron [ip.2.5.9.10.1] fix: remove duplicate record in getClassTrainingReportData()
##
## 2018-08-15 Cameron [ip.2.5.9.10.1] add function getClassTrainingReportData(), getClassTrainingReport(), getClassTrainingDetailReport()
##
## 2018-08-14 Cameron [ip.2.5.9.10.1] add function getHiddenTableForExport()
##
## 2018-08-13 Cameron [ip.2.5.9.10.1] add function getStaffTeachingReportData(), getStaffTeachingReport(), getStaffTeachingDetailReport()
##
## 2018-07-03 Pun [121822] [ip.2.5.9.7.1] Added NCS cust
## 2017-09-26 (Anna) : modified getActivitySummaryReport() - delete AcademicYearID condition when generate by date range  
## 2017-09-18 (Anna) : modified getDailyActivityRecords() - comment 
## 2016-11-28 (Omas) : modified getDailyActivityRecords() - add join condition to sql "ieed.RecordStatus = 1"
## 2016-02-29 (Kenneth): added getGeneralEnrolmentReportTableHtml() - add getTable function for general uses - by header array and content array
## 2015-12-04 (Kenneth):modify getStudentWithoutEnrolmentReportResultTableHtml(), getStudentWithoutEnrolmentReportResultAry, add CategoryID as filter
## 2015-01-22 (Carlos): [ip2.5.6.3.1] modified getActivitySummaryReport() and getGroupActivityStatistics(), redefine display data according to the cust requirement
## 2015-01-02 (Omas):	modified SQL in getActivitySummaryReport() YearTermID join on iegi instead of ieei
## 2014-08-04 (Bill):	Added getStudentWithoutEnrolmentReportResultTableHtml(), getStudentWithoutEnrolmentReportResultHeaderAry(), getStudentWithoutEnrolmentReportResultAry()
##						for student without enrolment report
## 2014-07-23 (Carlos): $sys_custom['eEnrolment']['TWGHCYMA'] - Modified getActivitySummaryReport(), added joint activity fields
##						modified getGroupActivityStatistics() - display events that are non-club related but has related intranet groups 
## 2014-04-16 (Carlos): TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
##						Added getActivitySummaryGroupReport(), getClassLevelActivitySummaryReport(), getClubRolesAndCommentsReport(),
##						 getActivityNatureAndOleTypeStatistics(), getActivityAwardsReport(), getClubActivityStatistics(), getActivitySummaryReport(),
##						getDailyActivityRecords(), getGroupActivityStatistics()
###########################################
*/
if (!defined("LIBCLUBSENROL_REPORT_DEFINED"))                     // Preprocessor directive
{

	class libclubsenrol_report extends libclubsenrol {
		function libclubsenrol_report() {
			$this->libdb();
		}
		
		function getAttendanceStatusReportResultTableHtml($startDate, $endDate, $studentSource, $studentIdAry, $studentSource_individualStudentSource, $studentName, $studentLogin, $studentClassId, $studentClassNumber, 
															$attendanceSourceAry, $attendanceSource_clubIdAry, $attendanceSource_activirtIdAry, $attendanceStatusAry, $showMainGuardianInfo) {
			global $Lang, $PATH_WRT_ROOT;
			include_once $PATH_WRT_ROOT.'includes/table/table_commonTable.php';
			
			$headerAry = $this->getAttendanceStatusReportResultHeaderAry($showMainGuardianInfo);
			$numOfHeader = count($headerAry);
			
			$resultAry = $this->getAttendanceStatusReportResultAry($startDate, $endDate, $studentSource, $studentIdAry, $studentSource_individualStudentSource, $studentName, $studentLogin, $studentClassId, $studentClassNumber, 
																	$attendanceSourceAry, $attendanceSource_clubIdAry, $attendanceSource_activirtIdAry, $attendanceStatusAry, $showMainGuardianInfo);
			$numOfRow = count($resultAry);
			
			
			$tableObj = new table_commonTable();
			$tableObj->setTableType('view');
			
			$tableObj->addHeaderTr(new tableTr());
			for ($i=0; $i<$numOfHeader; $i++) {
				$tableObj->addHeaderCell(new tableTh($headerAry[$i]));
			}
			
			for ($i=0; $i<$numOfRow; $i++) {
				$_rowContentAry = $resultAry[$i];
				$_numOfCol = count($_rowContentAry);
				
				$tableObj->addBodyTr(new tableTr());
				
				for ($j=0; $j<$_numOfCol; $j++) {
					$tableObj->addBodyCell(new tableTd($_rowContentAry[$j]));
				}
			}
			
			return $tableObj->returnHtml();
		}
		
		function getAttendanceStatusReportResultHeaderAry($showMainGuardianInfo) {
			global $Lang;
			
			$headerAry = array();
			$headerAry[] = $Lang['General']['Class'];
			$headerAry[] = $Lang['General']['ClassNumber'];
			$headerAry[] = $Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['StudentName'];
			$headerAry[] = $Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['AttendanceSource'];
			$headerAry[] = $Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['Club/ActivityName'];
			$headerAry[] = $Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['AttendanceStatus'];
			if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
    			$headerAry[] = $Lang['General']['Remark'];
			}
			
			if ($showMainGuardianInfo) {
				$headerAry[] = $Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['GuardianName'];
				$headerAry[] = $Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['GuardianRelationship'];
				$headerAry[] = $Lang['eEnrolment']['ReportArr']['AttendanceStatusReportArr']['EmergencyPhoneNo'];
			}
			
			return $headerAry;
		}
		
		function getAttendanceStatusReportResultAry($startDate, $endDate, $studentSource, $studentIdAry, $studentSource_individualStudentSource, $studentName, $studentLogin, $studentClassId, $studentClassNumber, 
													$attendanceSourceAry, $attendanceSource_clubIdAry, $attendanceSource_activirtIdAry, $attendanceStatusAry, $showMainGuardianInfo) {
			
			global $PATH_WRT_ROOT, $ec_guardian, $eEnrollment;
			
			include_once($PATH_WRT_ROOT.'includes/libuser.php');
			include_once($PATH_WRT_ROOT.'includes/libclubsenrol_ui.php');
			$libuser = new libuser();
			$libenroll_ui = new libclubsenrol_ui();
			
			### Get Student Info
			if ($studentSource == 'club' || $studentSource == 'activity' || $studentSource == 'formClass') {
				// use the $studentIdAry in the parameter => do nth 
			}
			else if ($studentSource == 'individualStudent') {
				// get the student info by the selected criteria
				if ($studentSource_individualStudentSource == 'studentName') {
					$studentIdAry = Get_Array_By_Key($libuser->getUserInfoByName($studentName), 'UserID');
				}
				else if ($studentSource_individualStudentSource == 'studentLogin') {
					$studentIdAry = Get_Array_By_Key($libuser->returnUser('', $studentLogin), 'UserID');
				}
				else if ($studentSource_individualStudentSource == 'studentClassNameClassNumber') {
					include_once($PATH_WRT_ROOT.'includes/libclass.php');
					$libclass = new libclass();
					
					$studentClassName = $libclass->getClassName($studentClassId);
					$studentIdAry = array($libuser->returnUserID($studentClassName, $studentClassNumber));
				}
			}
			$studentInfoAry = $this->Get_Student_Info($studentIdAry);
			$numOfStudent = count($studentInfoAry);
			
			
			### Get Club and Attendance Info
			$clubInfoAssoAry = array();
			$AssoAry = array();
			if (in_array('club', (array)$attendanceSourceAry)) {
				$clubInfoAssoAry = BuildMultiKeyAssoc($this->Get_All_Club_Info($attendanceSource_clubIdAry), 'EnrolGroupID');
				// $clubAttendanceAssoAry[$thisStudentID][$thisEnrolGroupID][$thisGroupDateID][key] = data
				$clubAttendanceAssoAry = $this->Get_Student_Club_Attendance_Info($studentIdAry, $attendanceSource_clubIdAry);
			}
			### Get Activity and Attendance Info
			$activityInfoAssoAry = array();
			if (in_array('activity', (array)$attendanceSourceAry)) {
				$activityInfoAssoAry = BuildMultiKeyAssoc($this->Get_Activity_Info_By_Id($attendanceSource_activirtIdAry), 'EnrolEventID');
				// $activityAttendanceAssoAry[$thisStudentID][$thisEnrolEventID][$thisEventDateID][key] = data
				$activityAttendanceAssoAry = $this->Get_Student_Activity_Attendance_Info($studentIdAry, $attendanceSource_activirtIdAry);
			}
			
			### Main Guardian Info
			if ($showMainGuardianInfo) {
				$mainGuardianAssoAry = BuildMultiKeyAssoc($libuser->getGuardianInfoAry($studentIdAry, $isMainOnly=true), 'UserID');
			}
			
			$returnAry = array();
			$iCount = 0;
			$startDateTimeTs = strtotime($startDate.' 00:00:00');
			$endDateTimeTs = strtotime($endDate.' 23:59:59');
			### Loop each student
			for ($i=0; $i<$numOfStudent; $i++) {
				$_studentId = $studentInfoAry[$i]['UserID'];
				$_studentName = Get_Lang_Selection($studentInfoAry[$i]['ChineseName'], $studentInfoAry[$i]['EnglishName']);
				$_className = $studentInfoAry[$i]['ClassName'];
				$_classNumber = $studentInfoAry[$i]['ClassNumber'];
				
				### Loop each Club
				foreach ((array)$clubInfoAssoAry as $__enrolGroupId => $__clubInfoAry) {
					### Loop each meeting date
					foreach ((array)$clubAttendanceAssoAry[$_studentId][$__enrolGroupId] as $___dateId => $___dateInfoAry) {
						if(!is_array($___dateInfoAry)){
							continue;
						}
						$___startDateTimeTs = strtotime($___dateInfoAry['ActivityDateStart']);
						$___endDateTimeTs = strtotime($___dateInfoAry['ActivityDateEnd']);
						$___recordStatus = $___dateInfoAry['RecordStatus'];
						$___remark = $___dateInfoAry['Remark'];
						
						$___isWithinDateRange = false;
						$___isIncludedStatus = false;
						
						// compare date time
						if ($startDateTimeTs <= $___startDateTimeTs && $___endDateTimeTs <= $endDateTimeTs) {
							$___isWithinDateRange = true;
						}
						
						// filter attendance status
						if (in_array($___recordStatus, (array)$attendanceStatusAry)) {
							$___isIncludedStatus = true;
						}
						
						if ($___isWithinDateRange && $___isIncludedStatus) {
							$returnAry[$iCount][] = $_className;
							$returnAry[$iCount][] = $_classNumber;
							$returnAry[$iCount][] = $_studentName;
							$returnAry[$iCount][] = $eEnrollment['club'];
							$returnAry[$iCount][] = $__clubInfoAry['TitleWithSemester'];
							$returnAry[$iCount][] = $libenroll_ui->Get_Attendance_Display($___recordStatus);
        					if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
        					    $returnAry[$iCount][] = $___remark;
        					}
							
							if ($showMainGuardianInfo) {
								$returnAry[$iCount][] = Get_Lang_Selection($mainGuardianAssoAry[$_studentId]['ChName'], $mainGuardianAssoAry[$_studentId]['EnName']);
								$returnAry[$iCount][] = $ec_guardian[str_pad($mainGuardianAssoAry[$_studentId]['Relation'], 2, 0, STR_PAD_LEFT)];
								$returnAry[$iCount][] = $mainGuardianAssoAry[$_studentId]['EmPhone'];
							}
							
							$iCount++;
						}
					}
				}
				
				### Loop each Activity
				foreach ((array)$activityInfoAssoAry as $__enrolEventId => $__activityInfoAry) {
					### Loop each meeting date
					foreach ((array)$activityAttendanceAssoAry[$_studentId][$__enrolEventId] as $___dateId => $___dateInfoAry) {
						if(!is_array($___dateInfoAry)){
							continue;
						}

						$___startDateTimeTs = strtotime($___dateInfoAry['ActivityDateStart']);
						$___endDateTimeTs = strtotime($___dateInfoAry['ActivityDateEnd']);
						$___recordStatus = $___dateInfoAry['RecordStatus'];
						$___remark = $___dateInfoAry['Remark'];
						
						$___isWithinDateRange = false;
						$___isIncludedStatus = false;
						
						// compare date time
						if ($startDateTimeTs <= $___startDateTimeTs && $___endDateTimeTs <= $endDateTimeTs) {
							$___isWithinDateRange = true;
						}
						
						// filter attendance status
						if (in_array($___recordStatus, (array)$attendanceStatusAry)) {
							$___isIncludedStatus = true;
						}
						
						if ($___isWithinDateRange && $___isIncludedStatus) {
							$returnAry[$iCount][] = $_className;
							$returnAry[$iCount][] = $_classNumber;
							$returnAry[$iCount][] = $_studentName;
							$returnAry[$iCount][] = $eEnrollment['activity'];
							$returnAry[$iCount][] = $__activityInfoAry['EventTitle'];
							$returnAry[$iCount][] = $libenroll_ui->Get_Attendance_Display($___recordStatus);
        					if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
        					    $returnAry[$iCount][] = $___remark;
        					}
							
							if ($showMainGuardianInfo) {
								$returnAry[$iCount][] = Get_Lang_Selection($mainGuardianAssoAry[$_studentId]['ChName'], $mainGuardianAssoAry[$_studentId]['EnName']);
								$returnAry[$iCount][] = $ec_guardian[str_pad($mainGuardianAssoAry[$_studentId]['Relation'], 2, 0, STR_PAD_LEFT)];
								$returnAry[$iCount][] = $mainGuardianAssoAry[$_studentId]['EmPhone'];
							}
							
							$iCount++;
						}
					}
				}
			}
			
			return $returnAry;
		}
		
		function getGeneralEnrolmentReportTableHtml($headerArr, $contentArr,$widthArr=''){
			global $Lang, $PATH_WRT_ROOT;
			include_once $PATH_WRT_ROOT.'/includes/table/table_commonTable.php';
			
//			$headerArr = array('Header 1','Header 2', 'Header 3');
//			$contentArr='';
//			$contentArr[] = array('Content 1','Content 2', 'Content 3');
//			$contentArr[] = array('Content 1','Content 2', 'Content 3');
//			$contentArr[] = array('Content 1','Content 2<br>Content2b', 'Content 3');

			
			$tableObj = new table_commonTable();
			$tableObj->setTableType('view');
			$tableObj->setApplyConvertSpecialChars(false);
			$tableObj->addHeaderTr(new tableTr());
			$tableObj->addHeaderCell(new tableTh('#','3%'));
			$numOfHeader=count($headerArr);
			for ($i=0; $i<$numOfHeader; $i++) {
				$tableObj->addHeaderCell(new tableTh($headerArr[$i],$widthArr[$i]));
			}
			$numOfRow = count($contentArr);
			for ($i=0; $i<$numOfRow; $i++) {
				$_rowContentAry = $contentArr[$i];
				$_numOfCol = count($_rowContentAry);
				
				$tableObj->addBodyTr(new tableTr());
				$tableObj->addBodyCell(new tableTd($i+1));
				
				for ($j=0; $j<$_numOfCol; $j++) {
					$tableObj->addBodyCell(new tableTd($_rowContentAry[$j]));
				}
			}
			$x = $tableObj->returnHtml();

			return $x;
		}
		
		
		// Student Without Enrolment Report
		
		function getStudentWithoutEnrolmentReportResultTableHtml($enrolSourceAry, $clubTermIdAry, $AcademicYearID, $CategoryID='') {
			global $Lang, $PATH_WRT_ROOT;
			include_once $PATH_WRT_ROOT.'/includes/table/table_commonTable.php';
			
			$headerAry = $this->getStudentWithoutEnrolmentReportResultHeaderAry();
			$numOfHeader = count($headerAry);
			//debug_pr($CategoryID);
			$resultAry = $this->getStudentWithoutEnrolmentReportResultAry($enrolSourceAry, $clubTermIdAry, $AcademicYearID, $CategoryID);
			//debug_pr($resultAry);
			$numOfRow = count($resultAry);
			
			
			$tableObj = new table_commonTable();
			$tableObj->setTableType('view');
			
			$tableObj->addHeaderTr(new tableTr());
			$tableObj->addHeaderCell(new tableTh('#'));
			for ($i=0; $i<$numOfHeader; $i++) {
				$tableObj->addHeaderCell(new tableTh($headerAry[$i]));
			}
			
			for ($i=0; $i<$numOfRow; $i++) {
				$_rowContentAry = $resultAry[$i];
				$_numOfCol = count($_rowContentAry);
				
				$tableObj->addBodyTr(new tableTr());
				$tableObj->addBodyCell(new tableTd($i+1));
				
				for ($j=0; $j<$_numOfCol; $j++) {
					$tableObj->addBodyCell(new tableTd($_rowContentAry[$j]));
				}
			}
			
			return $tableObj->returnHtml();
		}
		
		// Student Without Enrolment Report Header
		function getStudentWithoutEnrolmentReportResultHeaderAry() {
			global $Lang;
			
			$headerAry = array();
			$headerAry[] = $Lang['General']['Class'];
			$headerAry[] = $Lang['General']['ClassNumber'];
			$headerAry[] = $Lang['eEnrolment']['ReportArr']['StudentWithoutEnrolmentReportArr']['StudentName'];
			
			return $headerAry;
		}
		
		// Student Without Enrolment Content
		function getStudentWithoutEnrolmentReportResultAry($enrolSourceAry, $clubTermIdAry, $AcademicYearID, $CategoryID='') {
			
			global $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT.'includes/libuser.php');
			
			$libuser = new libuser();
			$studentInfoAry = $libuser->returnUsersByIdentity(USERTYPE_STUDENT);
			$allStudentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
			
//			debug_pr($studentInfoAry);

			# Club (with terms)
			$enrolledClubStudentIdAry = array();
			if (in_array('club', (array)$enrolSourceAry)) {
				// Club in target year terms
				$clubInfoAry = $this->Get_All_Club_Info($enrolGroupIdAry='', $AcademicYearID, $CategoryID, $Keyword='', $clubTermIdAry);
				$enrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
//				debug_pr($AcademicYearID);
//				debug_pr($clubInfoAry);
				// Students in enrol group
				$studentEnrolmentInfoAry = $this->Get_Club_Student_Enrollment_Info($enrolGroupIdAry, $AcademicYearID, $ShowUserRegOnly=0, $clubTermIdAry = '', $ClubKeyword='', $WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=false);
				
				//Get All studentId that enrolled in at least one club
				$enrolledClubStudentIdAry = array_values(array_unique(Get_Array_By_Key($studentEnrolmentInfoAry, 'StudentID')));
				//debug_pr($studentEnrolmentInfoAry);
				//debug_pr($enrolledClubStudentIdAry);
			}
			
			# Activity
			$enrolledActivityStudentIdAry = array();
			if (in_array('activity', (array)$enrolSourceAry)) {
				
				$studentEnrolmentInfoAry = $this->Get_Activity_Student_Enrollment_Info($EnrolEventIDArr='', $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ActiveMemberOnly=0, $FormIDArr='', $enrollStatusAry='', $ReturnAsso=false,'',$CategoryID);
				//debug_pr($studentEnrolmentInfoAry);
				//Get All studentId that enrolled in at least one activity
				$enrolledActivityStudentIdAry = array_values(array_unique(Get_Array_By_Key($studentEnrolmentInfoAry, 'StudentID')));
				//debug_pr($enrolledActivityStudentIdAry);
			}
			
			//Combine and unique both studentID array
			$enrolledStudentIdAry = array_values(array_unique(array_merge((array)$enrolledClubStudentIdAry, (array)$enrolledActivityStudentIdAry)));
			//debug_pr($enrolledStudentIdAry);
			
			$notEnrolledStudentIdAry = array_values(array_unique(array_diff((array)$allStudentIdAry, (array)$enrolledStudentIdAry)));
//			debug_pr($allStudentIdAry);debug_pr($enrolledStudentIdAry);
			// for sorting
			$studentInfoAry = $this->Get_Student_Info($notEnrolledStudentIdAry, true);
			$numOfStudent = count($studentInfoAry);
			
			$returnAry = array();
			for ($i=0; $i<$numOfStudent; $i++) {
				$_className = $studentInfoAry[$i]['ClassName'];
				$_classNumber = $studentInfoAry[$i]['ClassNumber'];
				$_studentName = Get_Lang_Selection($studentInfoAry[$i]['ChineseName'], $studentInfoAry[$i]['EnglishName']);
				
				$returnAry[$i][] = $_className;
				$returnAry[$i][] = $_classNumber;
				$returnAry[$i][] = $_studentName;
			}
			
			return $returnAry;
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getActivitySummaryGroupReport($options)
		{
			global $intranet_root, $Lang, $eEnrollment, $linterface, $PATH_WRT_ROOT, $i_no_record_searched_msg, $button_print;
			
			$format = $options['format'];
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			
			if ($radioPeriod == "YEAR") 
			{
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
				$selectYear = Get_Current_Academic_Year_ID();
				$selectSemester = 0;
			}
			$libenroll_cust = new libclubsenrol_cust();
			$records = $libenroll_cust->getActivitySummaryGroupReportRecords($options);
			$record_count = count($records);
			
			if($format == 'web'){
				$x = '<div class="content_top_tool">
						<div class="Conntent_tool">
							<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
							<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
						</div>
						<br style="clear:both" />
					  </div>';
			}
			
			if($format == 'print'){
				$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2").'</td>
						</tr>
						</table>';
			}
			
			if($format == 'csv'){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				
				$header = array(
							$Lang['eEnrolment']['Activity'],
							$Lang['eEnrolment']['Club'],
							$eEnrollment['add_activity']['act_pic'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['OLEType'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['Target'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['ActivityTotalHour']);
				$rows = array();
				
				if($record_count == 0){
					$row = array($i_no_record_searched_msg,'','','','','');
					$rows[] = $row;
				}
			}
			
			if($format == 'web' && $record_count == 0){
				$x .= "<div class='no_record_find_v30'>". $i_no_record_searched_msg ."</div>";
				
				return $x;
			}
			
			if($format == 'print'){
				$school_name = GET_SCHOOL_NAME();
				$academic_year_name = GET_ACADEMIC_YEAR3($SQL_startdate);
				if($selectSemester != 0 && $selectSemester != ''){
					$semester_name = retrieveSemester($SQL_startdate);
					$academic_year_name .= ' ('.$semester_name.')';
				}
				
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td align="center"><h2>'.$school_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$academic_year_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eEnrolment']['ActivitySummaryGroup']['ActivitySummaryGroup'].'</h2></td>
							</tr>
						</table>';
			}
			
			$x .= '<table class="common_table_list_v30 view_table_list_v30">
					<thead>
						<tr>
							<th width="20%">'.$Lang['eEnrolment']['Activity'].'</th>
							<th width="20%">'.$Lang['eEnrolment']['Club'].'</th>
							<th width="15%">'.$eEnrollment['add_activity']['act_pic'].'</th>
							<th width="15%">'.$Lang['eEnrolment']['ActivitySummaryGroup']['OLEType'].'</th>
							<th width="20%">'.$Lang['eEnrolment']['ActivitySummaryGroup']['Target'].'</th>
							<th width="10%">'.$Lang['eEnrolment']['ActivitySummaryGroup']['ActivityTotalHour'].'</th>
						</tr>
					</thead>';
			
			if($format=='print' && $record_count == 0){
				$x .= '<tr><td colspan="6" style="text-align:center;">'.$i_no_record_searched_msg.'</td></tr>';
			}
			
			//$cat_total_hours = array();
			$grand_total = 0;
			if($record_count > 0)
			{
				foreach($records as $ole_type => $recordAry)
				{
					$cat_total_hours = 0;
					$recordAry_count = count($recordAry);
					for($i=0;$i<$recordAry_count;$i++){
						
						$cat_total_hours += $recordAry[$i]['ActivityTotalTime'];
						$grand_total += $recordAry[$i]['ActivityTotalTime'];
						$x .= '<tr>';
							$x .= '<td>'.Get_String_Display($recordAry[$i]['EventTitle'],1).'</td>';
							$x .= '<td>'.Get_String_Display($recordAry[$i]['GroupTitle'],1).'</td>';
							$x .= '<td>'.Get_String_Display($recordAry[$i]['PIC'],1).'</td>';
							$x .= '<td>'.Get_String_Display($recordAry[$i]['CategoryName'],1).'</td>';
							$x .= '<td>'.Get_String_Display($recordAry[$i]['Target'],1).'</td>';
							$x .= '<td>'.Get_String_Display($recordAry[$i]['ActivityTotalTime'],1).'</td>';
						$x .= '</tr>';
						
						if($format == 'csv'){
							$row = array(Get_String_Display($recordAry[$i]['EventTitle'],1),
										Get_String_Display($recordAry[$i]['GroupTitle'],1),
										Get_String_Display($recordAry[$i]['PIC'],1),
										Get_String_Display($recordAry[$i]['CategoryName'],1),
										Get_String_Display($recordAry[$i]['Target'],1),
										Get_String_Display($recordAry[$i]['ActivityTotalTime'],1));
							$rows[] = $row;
						}
					}
					$x .= '<tr class="total_row">
							<td colspan="3">&nbsp;</td>
							<th>'.$recordAry[0]['CategoryName'].'&nbsp;'.$Lang['eEnrolment']['ActivitySummaryGroup']['Total'].'</th>
							<th>&nbsp;</th>
							<th>'.sprintf("%.2f",$cat_total_hours).'</th>
						</tr>';
					if($format == 'csv'){
						$row = array('','','',$recordAry[0]['CategoryName'].' '.$Lang['eEnrolment']['ActivitySummaryGroup']['Total'],'',sprintf("%.2f",$cat_total_hours));
						$rows[] = $row;
					}
				}
			}
			if($record_count > 0){
				$x .= '<tr>
							<td colspan="3">&nbsp;</td>
							<td>'.$Lang['eEnrolment']['ActivitySummaryGroup']['GrandTotal'].'</td>
							<td>&nbsp;</td>
							<td>'.sprintf("%.2f",$grand_total).'</td>
						</tr>';
				if($format == 'csv'){
					$row = array('','','',$Lang['eEnrolment']['ActivitySummaryGroup']['GrandTotal'],'',sprintf("%.2f",$grand_total));
					$rows[] = $row;
				}
			}
			
			$x .= '	</tbody>
					</table><br />';
			
			if($format == 'csv'){
				$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
				$lexport->EXPORT_FILE($Lang['eEnrolment']['ActivitySummaryGroup']['ActivitySummaryGroup'].".csv", $exportContent);
			}else{
				return $x;
			}
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getClassLevelActivitySummaryReport($options)
		{
			global $intranet_root, $Lang, $eEnrollment, $linterface, $PATH_WRT_ROOT, $i_no_record_searched_msg, $button_print;
			
			include_once($intranet_root."/includes/form_class_manage.php");
			
			$options['ReportType'] = 'ClassLevel';
			$year_class_id_ary = $options['rankTargetDetail'];
			$year_class_count = count($year_class_id_ary);
			$format = $options['format'];
			$libenroll_cust = new libclubsenrol_cust();
			
			$x = '';
			if($format == 'web'){
				$x = '<div class="content_top_tool">
						<div class="Conntent_tool">
							<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
							<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
						</div>
						<br style="clear:both" />
					  </div>';
			}
			
			if($format == 'print'){
				$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
						</tr>
						</table>';
			}
			
			if($format == 'csv'){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				
				$header = array(
							$Lang['eEnrolment']['Activity'],
							$Lang['eEnrolment']['Club'],
							$eEnrollment['add_activity']['act_pic'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['OLEType'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['Target'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['ActivityTotalHour']);
							
				$csv_delim = "";
			}
			
			for($j=0;$j<$year_class_count;$j++)
			{
				$this_options = $options;
				$this_options['TargetYearID'] = $year_class_id_ary[$j];
				$yearObj = new Year($year_class_id_ary[$j]);
				$year_name = $yearObj->Get_Year_Name();
				
				$records = $libenroll_cust->getActivitySummaryGroupReportRecords($this_options);
				$record_count = count($records);
				
				if($format == 'csv'){
					$rows = array();
				}
				
				if($record_count == 0){
					$row = array($i_no_record_searched_msg,'','','','','');
					$rows[] = $row;
				}
				/*
				if($format == 'web' && $record_count == 0){
					$x .= "<div class='no_record_find_v30'>". $i_no_record_searched_msg ."</div>";
					
					continue;
				}
				*/
				if($format == 'web' || $format == 'print'){
					$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td align="center"><h2>'.$Lang['eEnrolment']['ActivitySummaryForm']['ActivitySummaryForm'].' - '.$year_name.'</h2></td>
								</tr>
							</table>';
				}else if($format == 'csv'){
					$header = array($Lang['eEnrolment']['ActivitySummaryForm']['ActivitySummaryForm'].' - '.$year_name,'','','','','');
					$rows[] = array(
							$Lang['eEnrolment']['Activity'],
							$Lang['eEnrolment']['Club'],
							$eEnrollment['add_activity']['act_pic'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['OLEType'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['Target'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['ActivityTotalHour']);
					
				}
				
				$x .= '<table class="common_table_list_v30 view_table_list_v30">
						<thead>
							<tr>
								<th width="20%">'.$Lang['eEnrolment']['Activity'].'</th>
								<th width="20%">'.$Lang['eEnrolment']['Club'].'</th>
								<th width="15%">'.$eEnrollment['add_activity']['act_pic'].'</th>
								<th width="15%">'.$Lang['eEnrolment']['ActivitySummaryGroup']['OLEType'].'</th>
								<th width="20%">'.$Lang['eEnrolment']['ActivitySummaryGroup']['Target'].'</th>
								<th width="10%">'.$Lang['eEnrolment']['ActivitySummaryGroup']['ActivityTotalHour'].'</th>
							</tr>
						</thead>';
				
				if($format=='print' && $record_count == 0){
					$x .= '<tr><td colspan="6" style="text-align:center;">'.$i_no_record_searched_msg.'</td></tr>';
				}
				
				//$cat_total_hours = array();
				$grand_total = 0;
				if($record_count > 0)
				{
					foreach($records as $ole_type => $recordsAry)
					{
						$cat_total_hours = 0;
						$recordsAry_count = count($recordsAry);
						for($i=0;$i<$recordsAry_count;$i++){
							
							$cat_total_hours += $recordsAry[$i]['ActivityTotalTime'];
							$grand_total += $recordsAry[$i]['ActivityTotalTime'];
							$x .= '<tr>';
								$x .= '<td>'.Get_String_Display($recordsAry[$i]['EventTitle'],1).'</td>';
								$x .= '<td>'.Get_String_Display($recordsAry[$i]['GroupTitle'],1).'</td>';
								$x .= '<td>'.Get_String_Display($recordsAry[$i]['PIC'],1).'</td>';
								$x .= '<td>'.Get_String_Display($recordsAry[$i]['CategoryName'],1).'</td>';
								$x .= '<td>'.Get_String_Display($recordsAry[$i]['Target'],1).'</td>';
								$x .= '<td>'.Get_String_Display($recordsAry[$i]['ActivityTotalTime'],1).'</td>';
							$x .= '</tr>';
							
							if($format == 'csv'){
								$row = array(Get_String_Display($recordsAry[$i]['EventTitle'],1),
											Get_String_Display($recordsAry[$i]['GroupTitle'],1),
											Get_String_Display($recordsAry[$i]['PIC'],1),
											Get_String_Display($recordsAry[$i]['CategoryName'],1),
											Get_String_Display($recordsAry[$i]['Target'],1),
											Get_String_Display($recordsAry[$i]['ActivityTotalTime'],1));
								$rows[] = $row;
							}
						}
						$x .= '<tr class="total_row">
									<td colspan="3">&nbsp;</td>
									<th>'.$recordsAry[0]['CategoryName'].'&nbsp;'.$Lang['eEnrolment']['ActivitySummaryGroup']['Total'].'</th>
									<th>&nbsp;</th>
									<th>'.sprintf("%.2f",$cat_total_hours).'</th>
								</tr>';
						if($format == 'csv'){
							$row = array('','','',$recordsAry[0]['CategoryName'].' '.$Lang['eEnrolment']['ActivitySummaryGroup']['Total'],'',sprintf("%.2f",$cat_total_hours));
							$rows[] = $row;
						}
					}
					
					$x .= '<tr>
								<td colspan="3">&nbsp;</td>
								<td>'.$Lang['eEnrolment']['ActivitySummaryGroup']['GrandTotal'].'</td>
								<td>&nbsp;</td>
								<td>'.sprintf("%.2f",$grand_total).'</td>
							</tr>';
					if($format == 'csv'){
						$row = array('','','',$Lang['eEnrolment']['ActivitySummaryGroup']['GrandTotal'],'',sprintf("%.2f",$grand_total));
						$rows[] = $row;
					}
					
				}
				
				$x .= '	</tbody>
						</table><br />';
				
				if($format == 'print'){
					$x .= '<div style="page-break-after:always">&nbsp;</div>';
				}
					
				if($format == 'csv'){
					$rows[] = array(' ',' ',' ',' ',' ',' ');
					$exportContent .= $csv_delim.$lexport->GET_EXPORT_TXT($rows, $header);
					$csv_delim = "\r\n";
				}
			}
			
			if($format == 'csv'){
				//$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
				$lexport->EXPORT_FILE($Lang['eEnrolment']['ActivitySummaryForm']['ActivitySummaryForm'].".csv", $exportContent);
			}else{
				return $x;
			}
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getClubRolesAndCommentsReport($options)
		{
			global $intranet_root, $Lang, $eEnrollment, $linterface, $PATH_WRT_ROOT, $i_no_record_searched_msg, $button_print;
				
			$format = $options['format'];
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			if ($radioPeriod == "YEAR") 
			{
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
				$selectYear = Get_Current_Academic_Year_ID();
				$selectSemester = 0;
			}
			
			$libenroll_cust = new libclubsenrol_cust();
			$libenroll_ui = new libclubsenrol_ui();
			$records = $libenroll_cust->getClubRolesAndCommentsRecords($options);
			$record_count = count($records);
			
			if($format == 'web'){
				$x = '<div class="content_top_tool">
						<div class="Conntent_tool">
							<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
							<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
						</div>
						<br style="clear:both" />
					  </div>';
			}
			
			if($format == 'print'){
				$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
						</tr>
						</table>';
			}
			
			if($format == 'csv'){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				
				$header = array(
							$Lang['General']['AcademicYear'],
							$Lang['General']['Semester'],
							$Lang['SysMgr']['FormClassMapping']['Class'],
							$Lang['General']['ClassNumber'],
							$Lang['SysMgr']['FormClassMapping']['StudentName'],
							$Lang['eEnrolment']['Activity'],
							$Lang['eEnrolment']['Role'],
							$eEnrollment['performance'],
							$eEnrollment['comment']);
				$rows = array();
				
				if($record_count == 0){
					$row = array($i_no_record_searched_msg,'','','','','');
					$rows[] = $row;
				}
			}
			
			if($format == 'web' && $record_count == 0){
				$x .= "<div class='no_record_find_v30'>". $i_no_record_searched_msg ."</div>";
				
				return $x;
			}
			
			if($format == 'print'){
				$school_name = GET_SCHOOL_NAME();
				$academic_year_name = GET_ACADEMIC_YEAR3($SQL_startdate);
				if($selectSemester != 0 && $selectSemester != ''){
					$semester_name = retrieveSemester($SQL_startdate);
					$academic_year_name .= ' ('.$semester_name.')';
				}
				
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td align="center"><h2>'.$school_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$academic_year_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eEnrolment']['ClubRolesAndComments']['ClubRolesAndComments'].'</h2></td>
							</tr>
						</table>';
			}
			
			$x .= '<table class="common_table_list_v30 view_table_list_v30">
					<thead>
						<tr>
							<th width="10%">'.$Lang['General']['AcademicYear'].'</th>
							<th width="10%">'.$Lang['General']['Semester'].'</th>
							<th width="5%">'.$Lang['SysMgr']['FormClassMapping']['Class'].'</th>
							<th width="5%">'.$Lang['General']['ClassNumber'].'</th>
							<th width="10%">'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</th>
							<th width="20%">'.$Lang['eEnrolment']['Activity'].'</th>
							<th width="10%">'.$Lang['eEnrolment']['Role'].'</th>
							<th width="15%">'.$eEnrollment['performance'].'</th>
							<th width="15%">'.$eEnrollment['comment'].'</th>
						</tr>
					</thead>';
			
			if($format=='print' && $record_count == 0){
				$x .= '<tr><td colspan="9" style="text-align:center;">'.$i_no_record_searched_msg.'</td></tr>';
			}
			
			
			for($i=0;$i<$record_count;$i++){
	
				$x .= '<tr>';
					$x .= '<td>'.Get_String_Display($records[$i]['YearName'],1).'</td>';
					$x .= '<td>'.Get_String_Display($records[$i]['Semester'],1).'</td>';
					$x .= '<td>'.Get_String_Display($records[$i]['ClassName'],1).'</td>';
					$x .= '<td>'.Get_String_Display($records[$i]['ClassNumber'],1).'</td>';
					$x .= '<td>'.Get_String_Display($records[$i]['StudentName'],1).'</td>';
					$x .= '<td>'.Get_String_Display($records[$i]['Activity'],1).'</td>';
					$x .= '<td>'.Get_String_Display($records[$i]['Role'],1).'</td>';
					$x .= '<td>'.Get_String_Display(nl2br($records[$i]['Performance']),1).'</td>';
					$x .= '<td>'.Get_String_Display(nl2br($records[$i]['Comment']),1).'</td>';
				$x .= '</tr>';
				
				if($format == 'csv'){
					$row = array(Get_String_Display($records[$i]['YearName'],1),
								Get_String_Display($records[$i]['Semester'],1),
								Get_String_Display($records[$i]['ClassName'],1),
								Get_String_Display($records[$i]['ClassNumber'],1),
								Get_String_Display($records[$i]['StudentName'],1),
								Get_String_Display($records[$i]['Activity'],1),
								Get_String_Display($records[$i]['Role'],1),
								Get_String_Display($records[$i]['Performance'],1),
								Get_String_Display($records[$i]['Comment'],1));
					$rows[] = $row;
				}
			}
			
			$x .= '	</tbody>
					</table><br />';
			
			//$x .= $libenroll_ui->Get_Student_Role_And_Status_Remarks();
			
			if($format == 'csv'){
				$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
				$lexport->EXPORT_FILE($Lang['eEnrolment']['ClubRolesAndComments']['ClubRolesAndComments'].".csv", $exportContent);
			}else{
				return $x;
			}
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getActivityNatureAndOleTypeStatistics($options)
		{
			global $Lang, $button_print, $PATH_WRT_ROOT, $linterface;
			
			$LibPortfolio = new libpf_slp();
			
			$format = $options['format'];
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			$sel_category = $options['sel_category']; // array expected
			$sel_nature = $options['sel_nature']; 
			
			if ($radioPeriod == "YEAR") 
			{
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
				$selectYear = Get_Current_Academic_Year_ID();
				//$selectSemester = getCurrentSemesterID();
				$selectSemester = 0;
				$join_date_table = " INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID ";
				$date_cond = " AND ieed.ActivityDateStart>='$SQL_startdate 00:00:00' AND ieed.ActivityDateStart<='$SQL_enddate 23:59:59' ";
			}
			
			$YearTermIDArr = $selectSemester;
			if ($YearTermIDArr != '')
			{
				if (!is_array($YearTermIDArr))
					$YearTermIDArr = array($YearTermIDArr);
					
				if (in_array(0, $YearTermIDArr))
					$conds_YearTermIDArr = " And (ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') Or ieei.YearTermID Is Null Or ieei.YearTermID = '') ";
				else
					$conds_YearTermIDArr = " And ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') ";
			}
			
			$DefaultELEArray = $LibPortfolio->GET_ELE();
			
			// Get events with OLE category
			$sql = "SELECT 
						ieei.EnrolEventID,
						etos.OLE_Component 
					FROM INTRANET_ENROL_EVENTINFO as ieei $join_date_table 
					LEFT JOIN ENROLMENT_TO_OLE_SETTING as etos ON etos.RelatedEnrolID=ieei.EnrolEventID AND etos.RecordType='activity' 
					WHERE ieei.AcademicYearID='$selectYear' $conds_YearTermIDArr $date_cond";
			$ole_records = $this->returnArray($sql);
			$ole_record_count = count($ole_records);
			
			$OleToAmount = array();
			for($i=0;$i<$ole_record_count;$i++){
				$ole_components = explode(",",$ole_records[$i]['OLE_Component']);
				$ole_components_count = count($ole_components);
				
				for($j=0;$j<$ole_components_count;$j++){
					if(in_array($ole_components[$j],$sel_category)){
						$OleToAmount[$ole_components[$j]] += 1;
					}
				}
			}
			
			$activity_nature_list = $this->Get_Activity_Nature_List();
			
			if(count($sel_nature)>0){
				$nature_cond = " AND iean.NatureID IN ('".implode("','",(array)$sel_nature)."') ";
			}
			// Get events with activity nature category
			$sql = "SELECT 
						ieei.EnrolEventID,
						iean.NatureID,
						iean.Nature 
					FROM INTRANET_ENROL_EVENTINFO as ieei $join_date_table 
					LEFT JOIN INTRANET_ENROL_EVENT_NATURE as ieen ON ieen.EnrolEventID=ieei.EnrolEventID 
					LEFT JOIN INTRANET_ENROL_ACTIVITY_NATURE as iean ON iean.NatureID=ieen.NatureID 
					WHERE ieei.AcademicYearID='$selectYear' $conds_YearTermIDArr $date_cond $nature_cond";
			$nature_records = $this->returnArray($sql);
			$nature_record_count = count($nature_records);
			
			$NatureToAmount = array();
			for($i=0;$i<$nature_record_count;$i++){
				$NatureToAmount[$nature_records[$i]['NatureID']] += 1;
			}
			
			// Count total of activity hours
			$sql = "SELECT 
						SUM(TIME_TO_SEC(IF(TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart) < TIME('00:00:00'), '00:00:00', TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart))))/3600 as TotalHours 
					FROM INTRANET_ENROL_EVENT_DATE as e 
					WHERE e.EnrolEventID IN (".implode(",",Get_Array_By_Key($ole_records,'EnrolEventID')).") AND (e.RecordStatus IS NULL OR e.RecordStatus = 1)";
			$hours_record = $this->returnVector($sql);
			$total_hours = sprintf("%.2f",$hours_record[0]);
			//debug_r($sql);
			
			// Count the number of activities that have awards 
			$sql = "SELECT COUNT(DISTINCT EnrolEventID) FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID IN (".implode(",",Get_Array_By_Key($ole_records,'EnrolEventID')).") AND TRIM(Achievement)<>''";
			$awards_record = $this->returnVector($sql);
			$total_awarded = sprintf("%d", $awards_record[0]);
			//debug_r($sql);
			
			if($format == 'web'){
				$x = '<div class="content_top_tool">
						<div class="Conntent_tool">
							<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
							<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
						</div>
						<br style="clear:both" />
					  </div>';
			}
			
			if($format == 'print'){
				$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
						</tr>
						</table>';
				$school_name = GET_SCHOOL_NAME();
				$academic_year_name = GET_ACADEMIC_YEAR3($SQL_startdate);
				if($selectSemester != 0 && $selectSemester != ''){
					$semester_name = retrieveSemester($SQL_startdate);
					$academic_year_name .= ' ('.$semester_name.')';
				}
				
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td align="center"><h2>'.$school_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$academic_year_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['ActivityNatureOleTypeStatistics'].'</h2></td>
							</tr>
						</table>';
			}
			
			if($format == 'csv'){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				
				$header = array($Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['ActivityTotal'],$ole_record_count);
				$rows = array();
				$row = array($Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['ActivityTotalHours'],$total_hours);
				$rows[] = $row;
				$row = array($Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['AwardedActvityTotal'],$total_awarded);
				$rows[] = $row;
				$rows[] = array('','');
			}
			
			$x .= '<table cellspacing="0" cellpadding="5" border="0" width="50%" align="left">
						<tr>
							<td align="left" width="30%">'.$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['ActivityTotal'].'</td><td align="center" width="70%" style="border-bottom:1px solid black;">'.$ole_record_count.'</td>
						</tr>
						<tr>
							<td align="left" width="30%">'.$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['ActivityTotalHours'].'</td><td align="center" width="70%" style="border-bottom:1px solid black;">'.$total_hours.'</td>
						</tr>
						<tr>
							<td align="left" width="30%">'.$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['AwardedActvityTotal'].'</td><td align="center" width="70%" style="border-bottom:1px solid black;">'.$total_awarded.'</td>
						</tr>
						<tr><td colspan="3">&nbsp;</td></tr>
					</table>';
			
			
			$x .= '<table class="common_table_list_v30 view_table_list_v30">
					<thead>
						<tr>
							<th width="50%">'.$Lang['eEnrolment']['Nature'].'</th>
							<th width="50%">'.$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['NumberOfActivity'].'</th>
						</tr>
					</thead>';
			$x .= '<tbody>';
			
			if($format == 'csv'){
				$rows[] = array($Lang['eEnrolment']['Nature'],$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['NumberOfActivity']);
			}
			
			$nature_list_count = count($activity_nature_list);
			for($i=0;$i<$nature_list_count;$i++){
				
				if(in_array($activity_nature_list[$i]['NatureID'],$sel_nature)){
					$x .= '<tr>
								<td>'.$activity_nature_list[$i]['Nature'].'</td>
								<td>'.(isset($NatureToAmount[$activity_nature_list[$i]['NatureID']])?$NatureToAmount[$activity_nature_list[$i]['NatureID']]:'0').'</td>
							</tr>';
					if($format == 'csv'){
						$rows[] = array($activity_nature_list[$i]['Nature'],(isset($NatureToAmount[$activity_nature_list[$i]['NatureID']])?$NatureToAmount[$activity_nature_list[$i]['NatureID']]:'0'));
					}
				}
			}
			
			$x .= '	</tbody>
					</table><br />';
			
			
			$ole_list = $DefaultELEArray;
			$x .= '<table class="common_table_list_v30 view_table_list_v30">
					<thead>
						<tr>
							<th width="50%">'.$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['OLEType'].'</th>
							<th width="50%">'.$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['NumberOfActivity'].'</th>
						</tr>
					</thead>';
			$x .= '<tbody>';
			
			if($format == 'csv'){
				$rows[] = array('','');
				$rows[] = array($Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['OLEType'],$Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['NumberOfActivity']);
			}
			
			if(count($ole_list)>0){
				foreach($ole_list as $ole_code => $ole_name)
				{
					if(in_array($ole_code,$sel_category)){
						$x .= '<tr>
									<td>'.$ole_name.'</td>
									<td>'.(isset($OleToAmount[$ole_code])?$OleToAmount[$ole_code]:'0').'</td>
								</tr>';
								
						if($format == 'csv'){
							$rows[] = array($ole_name,(isset($OleToAmount[$ole_code])?$OleToAmount[$ole_code]:'0'));
						}
					}
				}
			}
			$x .= '	</tbody>
					</table><br />';
			
			if($format == 'csv'){
				$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
				$lexport->EXPORT_FILE($Lang['eEnrolment']['ActivityNatureOleTypeStatistics']['ActivityNatureOleTypeStatistics'].".csv", $exportContent);
			}else{
				return $x;
			}
				
			return $x;
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getActivityAwardsReport($options)
		{
			global $intranet_root, $Lang, $eEnrollment, $linterface, $PATH_WRT_ROOT, $sys_custom, $i_no_record_searched_msg, $button_print;
				
			$format = $options['format'];
			$rankTarget = $options['rankTarget'];
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			
			if ($radioPeriod == "YEAR") 
			{
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
				$selectYear = Get_Current_Academic_Year_ID();
				//$selectSemester = getCurrentSemesterID();
				$selectSemester = 0;
				$join_date_table = " INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID ";
				$date_cond = " AND ieed.ActivityDateStart>='$SQL_startdate 00:00:00' AND ieed.ActivityDateStart<='$SQL_enddate 23:59:59' ";
			}
			$YearTermIDArr = $selectSemester;
			
			if($rankTarget == 'form'){
				$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			}else if($rankTarget == 'class'){
				$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			}else if($rankTarget == 'student'){
				$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
				$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$options['studentID'])."')";
			}
			
			if ($YearTermIDArr != '')
			{
				if (!is_array($YearTermIDArr))
					$YearTermIDArr = array($YearTermIDArr);
					
				if (in_array(0, $YearTermIDArr))
					$conds_YearTermIDArr = " And (ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') Or ieei.YearTermID Is Null Or ieei.YearTermID = '') ";
				else
					$conds_YearTermIDArr = " And ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') ";
			}
			
			if($format == 'csv'){
				$EmptySymbol = '';
				$StylePrefix = '';
				$StyleSuffix = '';
			}else{
				$EmptySymbol = $Lang['General']['EmptySymbol'];
				$StylePrefix = '<span class="tabletextrequire">';
				$StyleSuffix = '</span>';
			}
			
			$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5','ay.YearNameEN');
			$SemesterNameField = Get_Lang_Selection('ayt.YearTermNameB5' ,'ayt.YearTermNameEN');
			$YearClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
			$StudentNameField = getNameFieldByLang("iu.");
			
			$clsName = $YearClassNameField;
			$sql = "SELECT 
						ieei.EnrolEventID,
						ies.StudentID,
						$AcademicYearNameField as YearName,
						IF(ayt.YearTermID IS NULL,'".$Lang['eEnrolment']['WholeYear']."',$SemesterNameField) as Semester,
						$YearClassNameField as ClassName,
						ycu.ClassNumber,
						CONCAT(
							IF($clsName IS NULL OR $clsName='' Or ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
							$StudentNameField
						) as StudentName,
						ieei.EventTitle,
						GROUP_CONCAT(DISTINCT iec.CategoryName SEPARATOR ', ') as CategoryName,
		                ies.Achievement 
					FROM INTRANET_ENROL_EVENTINFO as ieei ";
			if($sys_custom['eEnrolment']['TWGHCYMA']){
				$sql.= " LEFT JOIN INTRANET_ENROL_EVENT_CATEGORY as ec ON ec.EnrolEventID=ieei.EnrolEventID 
						LEFT JOIN INTRANET_ENROL_CATEGORY as iec ON ec.CategoryID=iec.CategoryID ";
			}else{
				$sql .= " LEFT JOIN INTRANET_ENROL_CATEGORY as iec ON ieei.EventCategory=iec.CategoryID ";
			}
			$sql.= $join_date_table; 
			$sql.= " INNER Join INTRANET_ENROL_EVENTSTUDENT as ies On (ieei.EnrolEventID = ies.EnrolEventID)
					INNER Join INTRANET_USER as iu On (ies.StudentID = iu.UserID And iu.RecordType = 2) 
					Left Join ACADEMIC_YEAR_TERM as ayt On (ieei.YearTermID = ayt.YearTermID) 
					LEFT JOIN ACADEMIC_YEAR as ay ON ay.AcademicYearID=ieei.AcademicYearID  
					LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID 
					LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=ieei.AcademicYearID 
					LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
					WHERE ieei.AcademicYearID = '".$selectYear."' AND TRIM(ies.Achievement)<>'' 
					$yearIdCond $classIdCond $studentIdCond $conds_YearTermIDArr $date_cond 
					GROUP BY ieei.EnrolEventID,ies.EventStudentID 
					ORDER BY ieei.EventTitle";
			$records = $this->returnArray($sql);
			//debug_r($sql);
			//debug_r($records);
			$record_count = count($records);
			
			if($format == 'web'){
				$x = '<div class="content_top_tool">
						<div class="Conntent_tool">
							<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
							<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
						</div>
						<br style="clear:both" />
					  </div>';
			}
			
			if($format == 'print'){
				$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
						</tr>
						</table>';
			}
			
			if($format == 'csv'){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				
				$header = array(
							$Lang['SysMgr']['FormClassMapping']['Class'],
							$Lang['General']['ClassNumber'],
							$Lang['SysMgr']['FormClassMapping']['StudentName'],
							$Lang['eEnrolment']['Activity'],
							$Lang['eEnrolment']['ActivitySummaryGroup']['ActivityTotalHour'],
							$eEnrollment['add_activity']['act_category'],
							$Lang['eEnrolment']['ActivityAwardsReport']['AwardOrAchievement']);
				$rows = array();
				
				if($record_count == 0){
					$row = array($i_no_record_searched_msg,'','','','','','');
					$rows[] = $row;
				}
			}
			
			if($format == 'web' && $record_count == 0){
				$x .= "<div class='no_record_find_v30'>". $i_no_record_searched_msg ."</div>";
				
				return $x;
			}
			
			if($format == 'print'){
				$school_name = GET_SCHOOL_NAME();
				$academic_year_name = GET_ACADEMIC_YEAR3($SQL_startdate);
				if($selectSemester != 0 && $selectSemester != ''){
					$semester_name = retrieveSemester($SQL_startdate);
					$academic_year_name .= ' ('.$semester_name.')';
				}
				
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td align="center"><h2>'.$school_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$academic_year_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eEnrolment']['ActivityAwardsReport']['ActivityAwardsReport'].'</h2></td>
							</tr>
						</table>';
			}
			
			$x .= '<table class="common_table_list_v30 view_table_list_v30">
					<thead>
						<tr>
							<th width="10%" nowrap>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</th>
							<th width="5%" nowrap>'.$Lang['General']['ClassNumber'].'</th>
							<th width="10%" nowrap>'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</th>
							<th width="20%" nowrap>'.$Lang['eEnrolment']['Activity'].'</th>
							<th width="10%" nowrap>'.$Lang['eEnrolment']['ActivitySummaryGroup']['ActivityTotalHour'].'</th>
							<th width="20%" nowrap>'.$eEnrollment['add_activity']['act_category'].'</th>
							<th width="25%" nowrap>'.$Lang['eEnrolment']['ActivityAwardsReport']['AwardOrAchievement'].'</th>
						</tr>
					</thead>';
			
			if($format=='print' && $record_count == 0){
				$x .= '<tr><td colspan="7" style="text-align:center;">'.$i_no_record_searched_msg.'</td></tr>';
			}
			
			$EventColumnSpan = array();
			for($i=0;$i<$record_count;$i++){
				$EventColumnSpan[$records[$i]['EnrolEventID']] += 1;
			}
			
			$today = date("Y-m-d H:i:s");
			for($i=0;$i<$record_count;$i++){
				
				// count the attended event hours for current student 
				$sql = "SELECT 
							SUM(TIME_TO_SEC(IF(TIMEDIFF(ieed.ActivityDateEnd,ieed.ActivityDateStart) < TIME('00:00:00'), '00:00:00', TIMEDIFF(ieed.ActivityDateEnd, ieed.ActivityDateStart))))/3600 as TotalHours 
						FROM 
						INTRANET_ENROL_EVENT_ATTENDANCE as ieea 
						INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EventDateID=ieea.EventDateID AND ieed.EnrolEventID=ieea.EnrolEventID 
						WHERE ieea.EnrolEventID='".$records[$i]['EnrolEventID']."' AND ieea.StudentID='".$records[$i]['StudentID']."' 
							AND ieed.ActivityDateStart <= '$today' AND ieed.ActivityDateStart>='$SQL_startdate' AND ieed.ActivityDateStart<='$SQL_enddate' 
							AND (ieed.RecordStatus IS NULL OR ieed.RecordStatus = 1)
							And (ieea.RecordStatus = '".ENROL_ATTENDANCE_PRESENT."' Or ieea.RecordStatus = '".ENROL_ATTENDANCE_LATE."')";
				$total_hours_record = $this->returnVector($sql);
				$records[$i]['ActivityTotalTime'] = sprintf("%.2f",$total_hours_record[0]);
				
				$x .= '<tr>';
					$x .= '<td>'.Get_String_Display($records[$i]['ClassName'],1).'</td>';
					$x .= '<td>'.Get_String_Display($records[$i]['ClassNumber'],1).'</td>';
					$x .= '<td>'.Get_String_Display($records[$i]['StudentName'],1).'</td>';
					if($records[$i]['EnrolEventID'] != $records[$i-1]['EnrolEventID']){
						$x .= '<td rowspan="'.$EventColumnSpan[$records[$i]['EnrolEventID']].'" style="vertical-align:middle;text-align:center;">'.Get_String_Display($records[$i]['EventTitle'],1).'</td>';
					}
					$x .= '<td>'.Get_String_Display($records[$i]['ActivityTotalTime'],1).'</td>';
					$x .= '<td>'.Get_String_Display($records[$i]['CategoryName'],1).'</td>';
					$x .= '<td>'.Get_String_Display(nl2br($records[$i]['Achievement']),1).'</td>';
				$x .= '</tr>';
				
				if($format == 'csv'){
					$row = array(
								Get_String_Display($records[$i]['ClassName'],1),
								Get_String_Display($records[$i]['ClassNumber'],1),
								Get_String_Display($records[$i]['StudentName'],1),
								Get_String_Display($records[$i]['EventTitle'],1),
								Get_String_Display($records[$i]['ActivityTotalTime'],1),
								Get_String_Display($records[$i]['CategoryName'],1),
								Get_String_Display($records[$i]['Achievement'],1));
					$rows[] = $row;
				}
			}
			
			$x .= '	</tbody>
					</table><br />';
			
			//$x .= $libenroll_ui->Get_Student_Role_And_Status_Remarks();
			
			if($format == 'csv'){
				$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
				$lexport->EXPORT_FILE($Lang['eEnrolment']['ActivityAwardsReport']['ActivityAwardsReport'].".csv", $exportContent);
			}else{
				return $x;
			}
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getClubActivityStatistics($options)
		{
			global $Lang, $button_print, $PATH_WRT_ROOT, $linterface, $i_no_record_searched_msg;
			
			$format = $options['format'];
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			$sel_club = $options['sel_club']; // array expected
			
			if ($radioPeriod == "YEAR") 
			{
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
				$selectYear = Get_Current_Academic_Year_ID();
				//$selectSemester = getCurrentSemesterID();
				$selectSemester = 0;
				$join_date_table = " INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID ";
				$date_cond = " AND ieed.ActivityDateStart>='$SQL_startdate 00:00:00' AND ieed.ActivityDateStart<='$SQL_enddate 23:59:59' ";
			}
			
			$YearTermIDArr = $selectSemester;
			if ($YearTermIDArr != '')
			{
				if (!is_array($YearTermIDArr))
					$YearTermIDArr = array($YearTermIDArr);
					
				if (in_array(0, $YearTermIDArr))
					$conds_YearTermIDArr = " And (iegi.Semester In ('".implode("','", $YearTermIDArr)."') Or iegi.Semester Is Null Or iegi.Semester = '') ";
				else
					$conds_YearTermIDArr = " And iegi.Semester In ('".implode("','", $YearTermIDArr)."') ";
			}
			
			$SemesterFieldName = Get_Lang_Selection('ayt.YearTermNameB5' ,'ayt.YearTermNameEN');
			
			// Get events with OLE category
			$sql = "SELECT 
						ig.GroupCode,
						IF (iegi.Semester Is Null Or iegi.Semester = '',
							Concat(ig.TitleChinese, ' (".$Lang['eEnrolment']['WholeYear'].")'),
							Concat(ig.TitleChinese, ' (', $SemesterFieldName, ')') 
						) as GroupTitle,
						COUNT(ieei.EnrolEventID) as NumberOfActivity 
					FROM INTRANET_ENROL_GROUPINFO as iegi
					INNER Join INTRANET_GROUP as ig On (ig.GroupID = iegi.GroupID)
					LEFT JOIN INTRANET_ENROL_EVENTINFO as ieei ON ieei.EnrolGroupID=iegi.EnrolGroupID $join_date_table 
					LEFT JOIN ACADEMIC_YEAR_TERM as ayt On (iegi.Semester = ayt.YearTermID) 
					WHERE (
							(iegi.Semester IS NULL Or iegi.Semester = '')
							OR 
							ayt.YearTermID IS NOT NULL
						)
						AND ig.RecordType=5 AND ig.AcademicYearID='$selectYear' AND iegi.EnrolGroupID IN ('".implode("','",$sel_club)."') $conds_YearTermIDArr $date_cond
					GROUP BY iegi.EnrolGroupID 
					ORDER BY ig.GroupCode";
			$records = $this->returnArray($sql);
			$record_count = count($records);
			
			if($format == 'web'){
				$x = '<div class="content_top_tool">
						<div class="Conntent_tool">
							<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
							<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
						</div>
						<br style="clear:both" />
					  </div>';
			}
			
			if($format == 'print'){
				$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
						</tr>
						</table>';
			}
			
			if($format == 'csv'){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				
				$header = array($Lang['eEnrolment']['ClubCode'],
							$Lang['eEnrolment']['ClubName'],
							$Lang['eEnrolment']['ClubActivityStatistics']['NumberOfClubActivity']);
				$rows = array();
				
				if($record_count == 0){
					$row = array($i_no_record_searched_msg,'','','','','','');
					$rows[] = $row;
				}
			}
			
			if($format == 'web' && $record_count == 0){
				$x .= "<div class='no_record_find_v30'>". $i_no_record_searched_msg ."</div>";
				
				return $x;
			}
			
			if($format == 'print'){
				$school_name = GET_SCHOOL_NAME();
				$academic_year_name = GET_ACADEMIC_YEAR3($SQL_startdate);
				if($selectSemester != 0 && $selectSemester != ''){
					$semester_name = retrieveSemester($SQL_startdate);
					$academic_year_name .= ' ('.$semester_name.')';
				}
				
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td align="center"><h2>'.$school_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$academic_year_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eEnrolment']['ClubActivityStatistics']['ClubActivityStatistics'].'</h2></td>
							</tr>
						</table>';
			}
			
			$x .= '<table class="common_table_list_v30 view_table_list_v30">
					<thead>
						<tr>
							<th width="20%" nowrap>'.$Lang['eEnrolment']['ClubCode'].'</th>
							<th width="50%" nowrap>'.$Lang['eEnrolment']['ClubName'].'</th>
							<th width="30%" nowrap>'.$Lang['eEnrolment']['ClubActivityStatistics']['NumberOfClubActivity'].'</th>
						</tr>
					</thead>';
			
			if($format=='print' && $record_count == 0){
				$x .= '<tr><td colspan="3" style="text-align:center;">'.$i_no_record_searched_msg.'</td></tr>';
			}
			
			for($i=0;$i<$record_count;$i++)
			{
				if($format == 'csv'){
					$row = array(Get_String_Display($records[$i]['GroupCode'],1),$records[$i]['GroupTitle'],$records[$i]['NumberOfActivity']);
					$rows[] = $row;
				}else{
					$x .= '<tr>';
						$x .= '<td>'.Get_String_Display($records[$i]['GroupCode'],1).'</td>';
						$x .= '<td>'.Get_String_Display($records[$i]['GroupTitle'],1).'</td>';
						$x .= '<td>'.Get_String_Display($records[$i]['NumberOfActivity'],1).'</td>';
					$x .= '</tr>';
				}
			}
			
			$x .= '	</tbody>
					</table><br />';
			
			if($format == 'csv'){
				$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
				$lexport->EXPORT_FILE($Lang['eEnrolment']['ClubActivityStatistics']['ClubActivityStatistics'].".csv", $exportContent);
			}else{
				return $x;
			}
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getActivitySummaryReport($options)
		{
			global $intranet_root, $Lang, $eEnrollment, $sys_custom, $linterface, $PATH_WRT_ROOT, $i_no_record_searched_msg, $button_print;
			
			$libenroll_cust = new libclubsenrol_cust();
			$LibPortfolio = new libpf_slp();
			$DefaultELEArray = $LibPortfolio->GET_ELE();
			$column_list = $libenroll_cust->getActivitySummaryReportColumnArray();
			$column_fields = Get_Array_By_Key($column_list,0);
			$column_list_count = count($column_list);
			
			$format = $options['format'];
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			$sel_column = (array)$options['sel_column'];
			
			if ($radioPeriod == "YEAR") 
			{
				$selectYear_conds = "AND ieei.AcademicYearID = '".$selectYear."'";
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
			//	$selectYear = Get_Current_Academic_Year_ID();
				$selectYear_conds = "";
				$selectSemester = 0;
				$join_date_table = " INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID ";
				$date_cond = " AND ieed.ActivityDateStart>='$SQL_startdate 00:00:00' AND ieed.ActivityDateStart<='$SQL_enddate 23:59:59' ";
			}
			$YearTermIDArr = $selectSemester;
			
			if ($YearTermIDArr != '')
			{
				if (!is_array($YearTermIDArr))
					$YearTermIDArr = array($YearTermIDArr);
					
				if (in_array(0, $YearTermIDArr)){
					//$conds_YearTermIDArr = " And (ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') Or ieei.YearTermID Is Null Or ieei.YearTermID = '') ";
					//$conds_YearTermIDArr = " And (iegi.Semester In ('".implode("','", $YearTermIDArr)."') Or iegi.Semester Is Null Or iegi.Semester = '') ";
					$conds_YearTermIDArr = '';
				}
				else{
					//$conds_YearTermIDArr = " And ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') ";
					$conds_YearTermIDArr = " And iegi.Semester In ('".implode("','", $YearTermIDArr)."')  ";
				}
			}
			
			if($format == 'csv'){
				$EmptySymbol = '';
				$StylePrefix = '';
				$StyleSuffix = '';
			}else{
				$EmptySymbol = $Lang['General']['EmptySymbol'];
				$StylePrefix = '<span class="tabletextrequire">';
				$StyleSuffix = '</span>';
			}
			
			if($format == 'csv'){
				$line_break = ', ';
			}else{
				$line_break = '<br>';
			}
			$nowrap = " nowrap ";		
			$name_separator = "; ";
			
			//$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5','ay.YearNameEN');
			$SemesterNameField = Get_Lang_Selection('ayt.YearTermNameB5' ,'ayt.YearTermNameEN');
			//$YearClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
			//$StudentNameField = getNameFieldByLang("iu.");
			$groupNameField = Get_Lang_Selection('rg.TitleChinese','rg.Title');
			
			$sql = "SELECT 
						ieei.EnrolEventID,
						ieei.ActivityCode,
						ieei.EventTitle,
						IF (iegi.Semester Is Null Or iegi.Semester = '',
							Concat(ig.TitleChinese, ' (".$Lang['eEnrolment']['WholeYear'].")'),
							Concat(ig.TitleChinese, ' (', ayt.YearTermNameB5, ')') 
						) as GroupTitle,
						IF(rg.GroupID IS NOT NULL,$groupNameField,'') as RelatedGroupName,
						GROUP_CONCAT(DISTINCT iean.Nature SEPARATOR '$line_break') as ActivityNature,
		             	IF(ieei.IsVolunteerService='1','Y','N') as IsVolunteerService,  
						IF(ieei.SchoolActivity='1','".$Lang['eEnrolment']['SchoolActivity']."',
							IF(ieei.SchoolActivity='0','".$Lang['eEnrolment']['NonSchoolActivity']."','".$Lang['eEnrolment']['MixedActivity']."')
						) as InternalOrExternal,
						ieei.ActivityExtLocation,
						ieei.Description as ActivityGoal,
						etos.OLE_Component,
						COUNT(DISTINCT iu.UserID) as NumberOfParticipants,
						IF(ieei.JointAct='1','Y','N') as JointAct,
						IF(ieei.JointAct='1','".$Lang['eEnrolment']['ActivitySummaryReport']['ExternalOrganizationOrganizedActivity']."','".$Lang['eEnrolment']['ActivitySummaryReport']['SchoolOrganizedActivity']."') as JointActType,
						IF(ieei.JointAct='1',ieei.JointActOrganiser,'".$Lang['eEnrolment']['ActivitySummaryReport']['School']."') as JointActOrganiser,
						ieei.JointActOrganisedGroup,
						IF(ieei.JointActAwarded='1','Y','N') as JointActAwarded,
						ieei.JointActAwardType,
						ieei.JointActChiAuthority,
						ieei.JointActEngAuthority,
						ieei.JointActChiAwardName,
						ieei.JointActEngAwardName,
						IF(ieei.PaymentAmount IS NOT NULL,CONCAT('$',ieei.PaymentAmount),'') as ActivityFee 
					FROM INTRANET_ENROL_EVENTINFO as ieei 
					LEFT JOIN INTRANET_ENROL_GROUPINFO as iegi ON (iegi.EnrolGroupID=ieei.EnrolGroupID) 
					LEFT JOIN INTRANET_GROUP as ig On (ig.GroupID = iegi.GroupID) 
					LEFT JOIN INTRANET_GROUP as rg ON rg.GroupID = iegi.RelatedGroupID 
					LEFT JOIN ENROLMENT_TO_OLE_SETTING as etos ON (etos.RelatedEnrolID=ieei.EnrolEventID AND etos.RecordType='activity') 
					LEFT JOIN INTRANET_ENROL_EVENT_NATURE as ieen ON (ieen.EnrolEventID=ieei.EnrolEventID) 
					LEFT JOIN INTRANET_ENROL_ACTIVITY_NATURE as iean ON (iean.NatureID=ieen.NatureID) 
					$join_date_table 
					LEFT Join INTRANET_ENROL_EVENTSTUDENT as ies On (ieei.EnrolEventID = ies.EnrolEventID)
					LEFT Join INTRANET_USER as iu On (ies.StudentID = iu.UserID And iu.RecordType = 2) 
					Left Join ACADEMIC_YEAR_TERM as ayt On (iegi.Semester = ayt.YearTermID) 
					LEFT JOIN ACADEMIC_YEAR as ay ON (ay.AcademicYearID=ieei.AcademicYearID)  
					LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID=iu.UserID) 
					LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=ieei.AcademicYearID) 
					LEFT JOIN YEAR as y ON (y.YearID=yc.YearID) 
					WHERE 1 $selectYear_conds  $conds_YearTermIDArr $date_cond 
					GROUP BY ieei.EnrolEventID
					ORDER BY ieei.ActivityCode";
					
			$records = $this->returnArray($sql);
			//debug_r($sql);
			//debug_r($records);
			$record_count = count($records);
			
			$PersonInChargeNameField = getNameFieldByLang("iu.");
			$StudentNameField = getNameFieldWithClassNumberByLang("iu.");
			
			for($i=0;$i<$record_count;$i++){
				$enrol_event_id = $records[$i]['EnrolEventID'];
				if($records[$i]['RelatedGroupName'] != ''){
					$records[$i]['GroupTitle'] .= ' ('.$records[$i]['RelatedGroupName'].')';
				}
				
				if(in_array('OLEType',$sel_column)){
					$ole_type = '';
					$delim = '';
					$ole_components = explode(",",$records[$i]['OLE_Component']);
					$ole_components_count = count($ole_components);
					for($j=0;$j<$ole_components_count;$j++){
						$ole_type .= $delim.$DefaultELEArray[$ole_components[$j]];
						$delim = $line_break;
					}
					$records[$i]['OLEType'] = $ole_type;
				}
				
				if(in_array('PersonInCharge',$sel_column) || in_array('SupportTeachers',$sel_column)){
					$sql = "SELECT $PersonInChargeNameField as StaffName,ies.StaffType FROM INTRANET_ENROL_EVENTSTAFF as ies INNER JOIN INTRANET_USER as iu ON iu.UserID=ies.UserID WHERE ies.EnrolEventID='$enrol_event_id'";
					$staff_records = $this->returnResultSet($sql);
					$staff_record_count = count($staff_records);
					$person_in_charge = '';
					$support_teachers = '';
					$person_in_charge_delim = '';
					$support_teachers_delim = '';
					for($j=0;$j<$staff_record_count;$j++){
						if($staff_records[$j]['StaffType'] == 'PIC'){
							$person_in_charge .= $person_in_charge_delim.$staff_records[$j]['StaffName'];
							$person_in_charge_delim = $line_break;
						}else if($staff_records[$j]['StaffType'] == 'HELPER'){
							$support_teachers .= $support_teachers_delim.$staff_records[$j]['StaffName'];
							$support_teachers_delim = $line_break;
						}
					}
					$records[$i]['PersonInCharge'] = $person_in_charge;
					$records[$i]['SupportTeachers'] = $support_teachers;
				}
				
				if(in_array('Target',$sel_column)){
					$sql = "SELECT 
								y.YearName 
							FROM INTRANET_ENROL_EVENTCLASSLEVEL as ieec 
							INNER JOIN YEAR as y ON y.YearID=ieec.ClassLevelID 
							WHERE ieec.EnrolEventID='$enrol_event_id' 
							ORDER BY y.Sequence";
					$targets = $this->returnVector($sql);
					$records[$i]['Target'] = implode($line_break,$targets);
				}
				
				if(in_array('ActivityDates',$sel_column) || in_array('ActivityStartDate',$sel_column) || in_array('ActivityEndDate',$sel_column) || in_array('NumbrOfActivity',$sel_column) || in_array('ActivityTotalTime',$sel_column)){
					$sql = "SELECT 
								ActivityDateStart,
								ActivityDateEnd 
							FROM INTRANET_ENROL_EVENT_DATE 
							WHERE EnrolEventID='$enrol_event_id' AND (RecordStatus IS NULL OR RecordStatus = 1) 
							ORDER BY ActivityDateStart";
					$date_records = $this->returnResultSet($sql);
					$start_date = '';
					$end_date = '';
					$number_of_activity = count($date_records);
					$activity_total_time = 0.0;
					$activity_dates = array();
					if($date_records[0]['ActivityDateStart'] != ''){
						$start_date = date("Y-m-d",strtotime($date_records[0]['ActivityDateStart']));
					}
					if($date_records[$number_of_activity-1]['ActivityDateEnd'] != ''){
						$end_date  = date("Y-m-d",strtotime($date_records[$number_of_activity-1]['ActivityDateEnd']));
					}
					
					for($j=0;$j<$number_of_activity;$j++){
						$start_ts = strtotime(substr($date_records[$j]['ActivityDateStart'],0,10));
						$end_ts = strtotime(substr($date_records[$j]['ActivityDateEnd'],0,10));
						for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts+=86400){
							$activity_dates[] = date("Y-m-d",$cur_ts);
						}	
						$activity_total_time += strtotime($date_records[$j]['ActivityDateEnd']) - strtotime($date_records[$j]['ActivityDateStart']); //count in seconds
					}
					$activity_dates = array_unique($activity_dates);
					sort($activity_dates);	
					$activity_total_time = sprintf("%.1f",$activity_total_time / 3600); // seconds to hours
					$records[$i]['ActivityStartDate'] = $start_date;
					$records[$i]['ActivityEndDate'] = $end_date;
					$records[$i]['ActivityDates'] = '';
					//$records[$i]['ActivityDates'] = implode(', ',$activity_dates);
					$sep = '';
					$cnt = 0;
					for($j=0;$j<count($activity_dates);$j++){
						$cnt++;
						$records[$i]['ActivityDates'] .= $sep.$activity_dates[$j];
						if($format != 'csv' && $cnt >= 3){
							$sep = ', <br />';
							$cnt = 0;
						}else{
							$sep = ', ';
						}
					}
					$records[$i]['NumberOfActivity'] = $number_of_activity;
					$records[$i]['ActivityTotalTime'] = $activity_total_time;
				}
				/*
				if(in_array('JointActivity',$sel_column) || in_array('OrganizeType',$sel_column) 
						|| in_array('Organiser',$sel_column) || in_array('OrganisedGroupOrganizationName',$sel_column))
				{
					$sql = "SELECT Organization FROM ENROLMENT_TO_OLE_SETTING WHERE RecordType='activity' AND RelatedEnrolID='$enrol_event_id' AND TRIM(Organization)<>''";
					$organizations = $this->returnVector($sql);
					$records[$i]['JointActivity'] = count($organizations)>0? 'Y':'N';
					$records[$i]['OrganizeType'] = count($organizations)>0? $Lang['eEnrolment']['ActivitySummaryReport']['ExternalOrganizationOrganizedActivity'] : $Lang['eEnrolment']['ActivitySummaryReport']['SchoolOrganizedActivity'];
					$records[$i]['Organiser'] = count($organizations)>0? implode($line_break,$organizations) : $Lang['eEnrolment']['ActivitySummaryReport']['School'];
					$records[$i]['OrganisedGroupOrganizationName'] = count($organizations)>0? implode($line_break,$organizations) : $Lang['eEnrolment']['ActivitySummaryReport']['School'];
				}
				
				if(in_array('ChineseAwardName',$sel_column) || in_array('EnglishAwardName',$sel_column) || in_array('AwardName',$sel_column)){
					$sql = "SELECT 
								DISTINCT iees.Achievement 
							FROM INTRANET_ENROL_EVENTSTUDENT as iees 
							WHERE iees.EnrolEventID='$enrol_event_id' AND TRIM(iees.Achievement)<>''";
					$award_names = $this->returnVector($sql);
					$records[$i]['ChineseAwardName'] = implode($line_break,$award_names);
					$records[$i]['EnglishAwardName'] = implode($line_break,$award_names);
					$records[$i]['AwardName'] = implode($line_break,$award_names);
				}
				*/
				if(in_array('AwardWinners',$sel_column)){
					$sql = "SELECT 
								$StudentNameField as StudentName 
							FROM INTRANET_ENROL_EVENTSTUDENT as iees 
							INNER JOIN INTRANET_USER as iu On iu.UserID=iees.StudentID 
							WHERE iees.EnrolEventID='$enrol_event_id' AND TRIM(iees.Achievement)<>''
							ORDER BY iu.ClassName,iu.ClassNumber+0";
					$award_students = $this->returnVector($sql);
					$award_winners = implode($name_separator,$award_students);		
					$records[$i]['AwardWinners'] = $award_winners;
				}
				
			}
			
			
			if($format == 'web'){
				$x = '<div class="content_top_tool">
						<div class="Conntent_tool">
							<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
							<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
						</div>
						<br style="clear:both" />
					  </div>';
			}
			
			if($format == 'print'){
				$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
						</tr>
						</table>';
			}
			
			if($format == 'csv'){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				
				$header = array();
				for($i=0;$i<$column_list_count;$i++){
					if(in_array($column_list[$i][0],$sel_column)){
						$header[] = $column_list[$i][1];
					}
				}	
				
				$rows = array();
				
				if($record_count == 0){
					$row = array($i_no_record_searched_msg);
					for($j=1;$j<count($sel_column);$j++){
						$row[] = '';
					}
					$rows[] = $row;
				}
			}
			
			if($format == 'web' && $record_count == 0){
				$x .= "<div class='no_record_find_v30'>". $i_no_record_searched_msg ."</div>";
				
				return $x;
			}
			
			if($format == 'print'){
				$school_name = GET_SCHOOL_NAME();
				$academic_year_name = GET_ACADEMIC_YEAR3($SQL_startdate);
				if($selectSemester != 0 && $selectSemester != ''){
					$semester_name = retrieveSemester($SQL_startdate);
					$academic_year_name .= ' ('.$semester_name.')';
				}
				
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td align="center"><h2>'.$school_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$academic_year_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eEnrolment']['ActivitySummaryReport']['ActivitySummaryReport'].'</h2></td>
							</tr>
						</table>';
			}
			
			$x .= '<table class="common_table_list_v30 view_table_list_v30">
					<thead>
						<tr>';
			for($i=0;$i<$column_list_count;$i++){
				if(in_array($column_list[$i][0],$sel_column)){
					$x .= '<th'.$nowrap.'>'.$column_list[$i][1].'</th>';
				}
			}			
				$x .= '</tr>
					</thead>';
			
			if($format=='print' && $record_count == 0){
				$x .= '<tr><td colspan="'.count($sel_column).'" style="text-align:center;">'.$i_no_record_searched_msg.'</td></tr>';
			}
			
			for($i=0;$i<$record_count;$i++){
				$x .= '<tr>';
					for($j=0;$j<$column_list_count;$j++){
						
						if(in_array($column_list[$j][0],$sel_column)){
							$x .= '<td'.$nowrap.'>'.Get_String_Display($records[$i][$column_list[$j][0]],1,$column_list[$j][2]).'</td>';
						}
					}
				$x .= '</tr>';
				
				if($format == 'csv'){
					$row = array();
					for($j=0;$j<$column_list_count;$j++){
						
						if(in_array($column_list[$j][0],$sel_column)){
							$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($records[$i][$column_list[$j][0]],1)));
						}
					}
					$rows[] = $row;
				}
			}
			
			$x .= '	</tbody>
					</table><br />';
			
			if($format == 'csv'){
				$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
				$lexport->EXPORT_FILE($Lang['eEnrolment']['ActivitySummaryReport']['ActivitySummaryReport'].".csv", $exportContent);
			}else{
				return $x;
			}
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getDailyActivityRecords($options)
		{
			global $intranet_root, $Lang, $eEnrollment, $sys_custom, $linterface, $PATH_WRT_ROOT, $i_no_record_searched_msg, $button_print;
			
			$format = $options['format'];
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			$rankTarget = $options['rankTarget'];
			$sel_pic = $options['sel_pic'];
			
			if($rankTarget == 'form'){
				$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			}else if($rankTarget == 'class'){
				$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
			}else if($rankTarget == 'student'){
				$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$options['rankTargetDetail'])."') ";
				$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$options['studentID'])."')";
			}
			
			if ($radioPeriod == "YEAR") 
			{
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
				$selectYear = Get_Current_Academic_Year_ID();
				$selectSemester = 0;
				$join_date_table = " INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID ";
				$date_cond = " AND ieed.ActivityDateStart>='$SQL_startdate 00:00:00' AND ieed.ActivityDateStart<='$SQL_enddate 23:59:59' ";
			}
			$YearTermIDArr = $selectSemester;
			
			if ($YearTermIDArr != '')
			{
				if (!is_array($YearTermIDArr))
					$YearTermIDArr = array($YearTermIDArr);
					
				if (in_array(0, $YearTermIDArr))
					$conds_YearTermIDArr = " And (ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') Or ieei.YearTermID Is Null Or ieei.YearTermID = '') ";
				else
					$conds_YearTermIDArr = " And ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') ";
			}
			
			$cond_PIC = " AND iees.UserID IN ('".implode("','",(array)$sel_pic)."') ";
			
			if($format == 'csv'){
				$EmptySymbol = '';
				$StylePrefix = '';
				$StyleSuffix = '';
			}else{
				$EmptySymbol = $Lang['General']['EmptySymbol'];
				$StylePrefix = '<span class="tabletextrequire">';
				$StyleSuffix = '</span>';
			}
			
			if($format == 'csv'){
				$line_break = ', ';
			}else{
				$line_break = '<br>';
			}
			if($format != 'print'){
				$nowrap = " nowrap ";
			}
			$name_separator = "; ";
			
			//$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5','ay.YearNameEN');
			$SemesterNameField = Get_Lang_Selection('ayt.YearTermNameB5' ,'ayt.YearTermNameEN');
			//$YearClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
			//$StudentNameField = getNameFieldByLang("iu.");
			//$PicNameField = getNameFieldByLang("iu.");
			
			$sql = "SELECT 
						ieei.EnrolEventID,
						ieei.ActivityCode,
						ieei.EventTitle as ActivityName,
						IF (iegi.Semester Is Null Or iegi.Semester = '',
							Concat(ig.TitleChinese, ' (".$Lang['eEnrolment']['WholeYear'].")'),
							Concat(ig.TitleChinese, ' (', $SemesterNameField, ')') 
						) as ActivityGroup,
						GROUP_CONCAT(DISTINCT iec.CategoryName SEPARATOR '$line_break') as ActivityCategory,
						GROUP_CONCAT(DISTINCT iean.Nature SEPARATOR '$line_break') as ActivityNature,
		             	ieed.ActivityDateStart,
						ieed.ActivityDateEnd,
						ieei.SchoolActivity,
						ieei.ActivityInLocation,
						ieei.ActivityExtLocation 
					FROM INTRANET_ENROL_EVENTINFO as ieei 
					LEFT JOIN INTRANET_ENROL_GROUPINFO as iegi ON iegi.EnrolGroupID=ieei.EnrolGroupID 
					LEFT Join INTRANET_GROUP as ig On (ig.GroupID = iegi.GroupID) ";
			if($sys_custom['eEnrolment']['TWGHCYMA']){
				$sql.= " LEFT JOIN INTRANET_ENROL_EVENT_CATEGORY as ieec ON ieec.EnrolEventID=ieei.EnrolEventID 
						LEFT JOIN INTRANET_ENROL_CATEGORY as iec ON iec.CategoryID=ieec.CategoryID ";
			}else{
				$sql.= " LEFT JOIN INTRANET_ENROL_CATEGORY as iec ON iec.CategoryID=ieei.EventCategory ";
			}
			$sql.= " LEFT JOIN INTRANET_ENROL_EVENT_NATURE as ieen ON ieen.EnrolEventID=ieei.EnrolEventID 
					LEFT JOIN INTRANET_ENROL_ACTIVITY_NATURE as iean ON iean.NatureID=ieen.NatureID 
					INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID AND ieed.RecordStatus = 1 
					LEFT Join INTRANET_ENROL_EVENTSTAFF as iees On (ieei.EnrolEventID = iees.EnrolEventID)
					LEFT Join INTRANET_USER as iu On (iees.UserID = iu.UserID And iu.RecordType = 2) 
					LEFT JOIN INTRANET_ENROL_EVENTSTUDENT as ieet ON ieet.EnrolEventID=ieei.EnrolEventID 
					Left Join ACADEMIC_YEAR_TERM as ayt On (ieei.YearTermID = ayt.YearTermID) 
					LEFT JOIN ACADEMIC_YEAR as ay ON ay.AcademicYearID=ieei.AcademicYearID 
					LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=ieet.StudentID 
					LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=ieei.AcademicYearID 
					LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
					WHERE /*ieei.AcademicYearID = '".$selectYear."' $yearIdCond */ 1$classIdCond $studentIdCond $conds_YearTermIDArr $date_cond $cond_PIC 
					GROUP BY ieei.EnrolEventID,ieed.EventDateID 
					ORDER BY ieed.ActivityDateStart,ieei.ActivityCode";
		
			$records = $this->returnArray($sql);
			//debug_r($sql);
			//debug_r($records);
			$record_count = count($records);
			
			$PersonInChargeNameField = getNameFieldByLang("iu.");
			$StudentNameField = getNameFieldWithClassNumberByLang("iu.");
			
			$DateToRecords = array();
			
			for($i=0;$i<$record_count;$i++){
				$enrol_event_id = $records[$i]['EnrolEventID'];
				$start_date = date("Y-m-d",strtotime($records[$i]['ActivityDateStart']));
				
				$sql = "SELECT $PersonInChargeNameField as StaffName,ies.StaffType FROM INTRANET_ENROL_EVENTSTAFF as ies INNER JOIN INTRANET_USER as iu ON iu.UserID=ies.UserID WHERE ies.EnrolEventID='$enrol_event_id'";
				$staff_records = $this->returnResultSet($sql);
				$staff_record_count = count($staff_records);
				$person_in_charge = '';
				$support_teachers = '';
				$person_in_charge_delim = '';
				$support_teachers_delim = '';
				for($j=0;$j<$staff_record_count;$j++){
					if($staff_records[$j]['StaffType'] == 'PIC'){
						$person_in_charge .= $person_in_charge_delim.$staff_records[$j]['StaffName'];
						$person_in_charge_delim = $line_break;
					}else if($staff_records[$j]['StaffType'] == 'HELPER'){
						$support_teachers .= $support_teachers_delim.$staff_records[$j]['StaffName'];
						$support_teachers_delim = $line_break;
					}
				}
				$records[$i]['PersonInCharge'] = $person_in_charge;
				$records[$i]['Staff'] = $support_teachers;
				
				$sql = "SELECT $StudentNameField as StudentName FROM INTRANET_ENROL_EVENTSTUDENT as iees INNER JOIN INTRANET_USER as iu ON iu.UserID=iees.StudentID WHERE iees.EnrolEventID='$enrol_event_id' and iu.RecordType='2'";
				$students = $this->returnVector($sql);
				$records[$i]['StudentList'] = implode($line_break, $students);
				
				if(!isset($DateToRecords[$start_date])){
					$DateToRecords[$start_date] = array();
				}
				$DateToRecords[$start_date][] = $records[$i];
			}
			
			$record_count = count($DateToRecords);
			
			if($format == 'web'){
				$x = '<div class="content_top_tool">
						<div class="Conntent_tool">
							<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
							<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
						</div>
						<br style="clear:both" />
					  </div>';
			}
			
			if($format == 'print'){
				$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
						</tr>
						</table>';
			}
			
			if($format == 'csv'){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				
				$header_row = array($Lang['eEnrolment']['DailyActivityRecords']['ActivityCode'],
								$Lang['eEnrolment']['DailyActivityRecords']['ActivityName'],
								$Lang['eEnrolment']['DailyActivityRecords']['ActivityCategory'],
								$Lang['eEnrolment']['DailyActivityRecords']['ActivityGroup'],
								$Lang['eEnrolment']['DailyActivityRecords']['StartDate'],
								$Lang['eEnrolment']['DailyActivityRecords']['Time'],
								$Lang['eEnrolment']['DailyActivityRecords']['EndDate'],
								$Lang['eEnrolment']['DailyActivityRecords']['Time'],
								$Lang['eEnrolment']['DailyActivityRecords']['ActivityLocation'],
								$Lang['eEnrolment']['DailyActivityRecords']['PersonInCharge'],
								$Lang['eEnrolment']['DailyActivityRecords']['Staff'],
								$Lang['eEnrolment']['DailyActivityRecords']['StudentList'],
								$Lang['eEnrolment']['DailyActivityRecords']['Remark']);
				
				$rows = array();
				
				if($record_count == 0){
					$row = array($i_no_record_searched_msg);
					for($j=1;$j<13;$j++){
						$row[] = '';
					}
					$rows[] = $row;
					
					$exportContent = $lexport->GET_EXPORT_TXT($rows, $header_row);
					$lexport->EXPORT_FILE($Lang['eEnrolment']['DailyActivityRecords']['DailyActivityRecords'].".csv", $exportContent);
					
					return;
				}
			}
			
			if($format == 'web' && $record_count == 0){
				$x .= "<div class='no_record_find_v30'>". $i_no_record_searched_msg ."</div>";
				
				return $x;
			}
			
			$exportContent = "";
			if($format == 'print'){
				$school_name = GET_SCHOOL_NAME();
				$academic_year_name = GET_ACADEMIC_YEAR3($SQL_startdate);
				if($selectSemester != 0 && $selectSemester != ''){
					$semester_name = retrieveSemester($SQL_startdate);
					$academic_year_name .= ' ('.$semester_name.')';
				}
				
				$report_title = '<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td align="center"><h2>'.$school_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$academic_year_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eEnrolment']['DailyActivityRecords']['DailyActivityRecords'].'</h2></td>
							</tr>
						</table>';
				//$x .= $report_title;
			}
			
			foreach($DateToRecords as $date => $records)
			{
				$records_count = count($records);
				
				if($format == 'print'){
					$x .= $report_title;
				}
				
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
							<tr>
								<td align="left">'.$Lang['General']['Date'].': '.$date.'</td>
							</tr>
						</table>';
				
				$x .= '<table class="common_table_list_v30 view_table_list_v30">
						<thead>
							<tr>';
						$x .= '<th'.$nowrap.' width="5%">'.$Lang['eEnrolment']['DailyActivityRecords']['ActivityCode'].'</th>';
						$x .= '<th'.$nowrap.' width="10%">'.$Lang['eEnrolment']['DailyActivityRecords']['ActivityName'].'</th>';
						$x .= '<th'.$nowrap.' width="10%">'.$Lang['eEnrolment']['DailyActivityRecords']['ActivityCategory'].'</th>';
						$x .= '<th'.$nowrap.' width="10%">'.$Lang['eEnrolment']['DailyActivityRecords']['ActivityGroup'].'</th>';
						$x .= '<th'.$nowrap.' width="5%">'.$Lang['eEnrolment']['DailyActivityRecords']['StartDate'].'</th>';
						$x .= '<th'.$nowrap.' width="5%">'.$Lang['eEnrolment']['DailyActivityRecords']['Time'].'</th>';
						$x .= '<th'.$nowrap.' width="5%">'.$Lang['eEnrolment']['DailyActivityRecords']['EndDate'].'</th>';
						$x .= '<th'.$nowrap.' width="5%">'.$Lang['eEnrolment']['DailyActivityRecords']['Time'].'</th>';
						$x .= '<th'.$nowrap.' width="10%">'.$Lang['eEnrolment']['DailyActivityRecords']['ActivityLocation'].'</th>';
						$x .= '<th'.$nowrap.' width="10%">'.$Lang['eEnrolment']['DailyActivityRecords']['PersonInCharge'].'</th>';
						$x .= '<th'.$nowrap.' width="10%">'.$Lang['eEnrolment']['DailyActivityRecords']['Staff'].'</th>';
						$x .= '<th'.$nowrap.' width="10%">'.$Lang['eEnrolment']['DailyActivityRecords']['StudentList'].'</th>';
						$x .= '<th'.$nowrap.' width="5%">'.$Lang['eEnrolment']['DailyActivityRecords']['Remark'].'</th>';
					$x .= '</tr>
						</thead>';
				
				if($format == 'csv'){
					$header = array($Lang['General']['Date'].': '.$date,'','','','','','','','','','','','','',);
					$rows = array();
					$rows[] = $header_row;
				}
				
				if($format=='print' && $records_count == 0){
					$x .= '<tr><td colspan="13" style="text-align:center;">'.$i_no_record_searched_msg.'</td></tr>';
				}
				
				for($i=0;$i<$records_count;$i++){
					$ts_startdate = strtotime($records[$i]['ActivityDateStart']);
					$ts_enddate = strtotime($records[$i]['ActivityDateEnd']);
					$start_date = date("Y-m-d",$ts_startdate);
					$start_time = date("H:i:s",$ts_startdate);
					$end_date = date("Y-m-d",$ts_enddate);
					$end_time = date("H:i:s",$ts_enddate);
					
					$activity_location = '';
					if(trim($records[$i]['ActivityInLocation'])!=''){
						$activity_location .= $records[$i]['ActivityInLocation'].$line_break;
					}
					if(trim($records[$i]['ActivityExtLocation']!='')){
						$activity_location .= $records[$i]['ActivityExtLocation'];
					}
					
					$x .= '<tr>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($records[$i]['ActivityCode'],1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($records[$i]['ActivityName'],1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($records[$i]['ActivityCategory'],1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($records[$i]['ActivityGroup'],1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($start_date,1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($start_time,1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($end_date,1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($end_time,1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($activity_location,1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($records[$i]['PersonInCharge'],1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($records[$i]['Staff'],1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($records[$i]['StudentList'],1).'</td>';
						$x .= '<td'.$nowrap.'>'.Get_String_Display($records[$i]['Remark'],1).'</td>';
					$x .= '</tr>';
					
					if($format == 'csv'){
						$row = array();
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($records[$i]['ActivityCode'],1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($records[$i]['ActivityName'],1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($records[$i]['ActivityCategory'],1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($records[$i]['ActivityGroup'],1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($start_date,1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($start_time,1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($end_date,1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($end_time,1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($activity_location,1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($records[$i]['PersonInCharge'],1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($records[$i]['Staff'],1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($records[$i]['StudentList'],1)));
						$row[] = strip_tags(str_replace(array('<br>','<br />','<br/>'),' ', Get_String_Display($records[$i]['Remark'],1)));
						$rows[] = $row;
					}
				}
				
				$x .= '	</tbody>
						</table><br />';
				
				if($format == 'print'){
					$x .= '<div style="page-break-after:always">&nbsp;</div>';
				}
				
				if($format == 'csv'){
					$rows[] = array('','','','','','','','','','','','','');
					if($exportContent != ""){
						$exportContent .= "\r\n";
					}
					$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
				}
				
			}
			
			if($format == 'csv'){
				//$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
				$lexport->EXPORT_FILE($Lang['eEnrolment']['DailyActivityRecords']['DailyActivityRecords'].".csv", $exportContent);
			}else{
				return $x;
			}
		}
		
		/*
		 * TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA']
		 */
		function getGroupActivityStatistics($options)
		{
			global $Lang, $button_print, $PATH_WRT_ROOT, $linterface, $i_no_record_searched_msg;
			
			$format = $options['format'];
			$radioPeriod = $options['radioPeriod']; // YEAR || DATE
			$selectYear = $options['selectYear']; 
			$selectSemester = $options['selectSemester'];
			$textFromDate = $options['textFromDate'];
			$textToDate = $options['textToDate'];
			$sel_group = $options['sel_group']; // array expected
			
			if ($radioPeriod == "YEAR") 
			{
				$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
				$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
			}
			else
			{
				$SQL_startdate = $textFromDate;
				$SQL_enddate = $textToDate;
				$selectYear = Get_Current_Academic_Year_ID();
				//$selectSemester = getCurrentSemesterID();
				$selectSemester = 0;
				//$event_join_date_table = " INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID ";
				$event_date_cond = " AND ieed.ActivityDateStart>='$SQL_startdate 00:00:00' AND ieed.ActivityDateStart<='$SQL_enddate 23:59:59' ";
			}
			
			$YearTermIDArr = $selectSemester;
			if ($YearTermIDArr != '')
			{
				if (!is_array($YearTermIDArr))
					$YearTermIDArr = array($YearTermIDArr);
					
				if (in_array(0, $YearTermIDArr)){
					$conds_YearTermIDArr = " And (iegi.Semester In ('".implode("','", $YearTermIDArr)."') Or iegi.Semester Is Null Or iegi.Semester = '') ";
					$conds_eventYearTermIDArr = " And (ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') Or ieei.YearTermID Is Null Or ieei.YearTermID = '') ";
				}else{
					$conds_YearTermIDArr = " And iegi.Semester In ('".implode("','", $YearTermIDArr)."') ";
					$conds_eventYearTermIDArr = " And ieei.YearTermID In ('".implode("','", $YearTermIDArr)."') ";
				}
			}
			
			if($format == 'web'){
				$x = '<div class="content_top_tool">
						<div class="Conntent_tool">
							<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>
							<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
						</div>
						<br style="clear:both" />
					  </div>';
			}
			
			if($format == 'print'){
				$x .= '<table width="100%" align="center" class="print_hide" border="0">
						<tr>
							<td align="right">'.$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit_print").'</td>
						</tr>
						</table>';
			}
			
			if($format == 'csv'){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				
				$exportContent = '';
			}
			
			$school_name = GET_SCHOOL_NAME();
			$academic_year_name = GET_ACADEMIC_YEAR3($SQL_startdate);
			$academic_year_display_name = $academic_year_name;
			if($selectSemester != 0 && $selectSemester != ''){
				$semester_name = retrieveSemester($SQL_startdate);
				$academic_year_display_name .= ' ('.$semester_name.')';
			}
			if($format == 'print'){
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">
							<tr>
								<td align="center"><h2>'.$school_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$academic_year_display_name.'</h2></td>
							</tr>
							<tr>
								<td align="center"><h2>'.$Lang['eEnrolment']['GroupActivityStatistics']['GroupActivityStatistics'].'</h2></td>
							</tr>
						</table>';
			}
			
			$group_title = Get_Lang_Selection("ig.TitleChinese","ig.Title");
			$sql = "SELECT ig.GroupID, $group_title as Title, igc.CategoryName, ig.RecordType 
					FROM INTRANET_GROUP as ig 
					LEFT JOIN INTRANET_GROUP_CATEGORY as igc ON igc.GroupCategoryID=ig.RecordType 
					WHERE ig.AcademicYearID='$selectYear' AND ig.GroupID IN ('".implode("','",$sel_group)."') 
					ORDER BY Title";
			$group_list = $this->returnArray($sql);
			$group_list_count = count($group_list);
			
			// get group related clubs
			$sql = "SELECT 
						iegi.EnrolGroupID, 
						ig.GroupCode,
						ig.Title,
						iegi.RelatedGroupID,
						COUNT(iegd.GroupDateID) as NumberOfActivity 
					FROM 
						INTRANET_GROUP as ig 
						INNER JOIN INTRANET_ENROL_GROUPINFO as iegi ON ig.GroupID = iegi.GroupID
						LEFT JOIN INTRANET_ENROL_GROUP_DATE as iegd ON iegd.EnrolGroupID=iegi.EnrolGroupID 
					WHERE 
						ig.RecordType=5
						AND ig.AcademicYearID = '$selectYear' 
						AND iegi.RelatedGroupID IN ('".implode("','",$sel_group)."') $conds_YearTermIDArr 
					GROUP BY iegi.EnrolGroupID 
					ORDER BY 
						ig.GroupCode,ig.Title";
			$enrol_group_list = $this->returnResultSet($sql);
			$groupIdToEnrolGroupAry = array();
			$enrol_group_id_ary = array();
			for($i=0;$i<count($enrol_group_list);$i++){
				$enrol_group_id_ary[] = $enrol_group_list[$i]['EnrolGroupID'];
				if(!isset($groupIdToEnrolGroupAry[$enrol_group_list[$i]['RelatedGroupID']])){
					$groupIdToEnrolGroupAry[$enrol_group_list[$i]['RelatedGroupID']] = array();
				}
				$groupIdToEnrolGroupAry[$enrol_group_list[$i]['RelatedGroupID']][] = $enrol_group_list[$i];
			}
			$group_id_ary = array_keys($groupIdToEnrolGroupAry);
			$enrol_group_id_ary = array_values(array_unique($enrol_group_id_ary));
			
			// get club event dates 
			$sql = "SELECT iegi.EnrolGroupID, iegd.ActivityDateStart, iegd.ActivityDateEnd 
					FROM INTRANET_ENROL_GROUPINFO as iegi 
					INNER JOIN INTRANET_ENROL_GROUP_DATE as iegd ON iegd.EnrolGroupID=iegi.EnrolGroupID 
					WHERE iegi.EnrolGroupID IN (".implode(",",$enrol_group_id_ary).") 
					ORDER BY iegd.EnrolGroupID,iegd.ActivityDateStart";
			$enrol_group_dates = $this->returnResultSet($sql);
			$enrolGroupIdToHourAry = array();
			for($i=0;$i<count($enrol_group_dates);$i++){
				if(!isset($enrolGroupIdToHourAry[$enrol_group_dates[$i]['EnrolGroupID']])){
					$enrolGroupIdToHourAry[$enrol_group_dates[$i]['EnrolGroupID']] = 0;
				}
				$hour = (strtotime($enrol_group_dates[$i]['ActivityDateEnd']) - strtotime($enrol_group_dates[$i]['ActivityDateStart'])) / 3600;
				$enrolGroupIdToHourAry[$enrol_group_dates[$i]['EnrolGroupID']] += $hour;
			}
			
			// get club event participant count
			$sql = "SELECT iegi.EnrolGroupID, COUNT(iug.UserID) as NumberOfParticipants 
					FROM INTRANET_ENROL_GROUPINFO as iegi 
					INNER JOIN INTRANET_GROUP as ig ON ig.GroupID=iegi.GroupID 
					INNER JOIN INTRANET_USERGROUP as iug ON iug.EnrolGroupID=iegi.EnrolGroupID 
					INNER JOIN INTRANET_USER as u ON u.UserID=iug.UserID 
					WHERE u.RecordType='".USERTYPE_STUDENT."' AND iegi.EnrolGroupID IN (".implode(",",$enrol_group_id_ary).") 
					GROUP BY iegi.EnrolGroupID";
			$enrol_group_participants = $this->returnResultSet($sql);
			$enrolGroupIdToParticipantAry = array();
			for($i=0;$i<count($enrol_group_participants);$i++){
				//if(!isset($enrolGroupIdToParticipantAry[$enrol_group_participants[$i]['EnrolFroupID']])){
				//	$enrolGroupIdToParticipantAry[$enrol_group_participants[$i]['EnrolFroupID']] = 0;
				//}
				$enrolGroupIdToParticipantAry[$enrol_group_participants[$i]['EnrolGroupID']] = $enrol_group_participants[$i]['NumberOfParticipants'];
			}
			
			if(count($groupIdToEnrolGroupAry)>0){
				foreach($groupIdToEnrolGroupAry as $related_group_id => $ary){
					$ary_count = count($ary);
					for($i=0;$i<$ary_count;$i++){
						 $t_enrol_group_id = $ary[$i]['EnrolGroupID'];
						 $groupIdToEnrolGroupAry[$related_group_id][$i]['TotalHours'] = isset($enrolGroupIdToHourAry[$t_enrol_group_id])? sprintf("%.1f",$enrolGroupIdToHourAry[$t_enrol_group_id]) : 0;
						 $groupIdToEnrolGroupAry[$related_group_id][$i]['TotalParticipants'] = isset($enrolGroupIdToParticipantAry[$t_enrol_group_id])? $enrolGroupIdToParticipantAry[$t_enrol_group_id]: 0;
					}
				}
			}
			//debug_r($sql);
			//debug_r($groupIdToEnrolGroupAry);
			
			// get clubs related events
			$SemesterNameField = Get_Lang_Selection('ayt.YearTermNameB5' ,'ayt.YearTermNameEN');
			$sql = "SELECT 
						ieei.EnrolEventID,
						ieei.ActivityCode,
						ieei.EventTitle as ActivityName,
						IF (iegi.Semester Is Null Or iegi.Semester = '',
							Concat(ig.TitleChinese, ' (".$Lang['eEnrolment']['WholeYear'].")'),
							Concat(ig.TitleChinese, ' (', $SemesterNameField, ')') 
						) as ActivityGroup,
						ieei.EnrolGroupID,
						COUNT(ieed.EventDateID) as NumberOfActivity,
						ieei.RelatedGroupID  
					FROM INTRANET_ENROL_EVENTINFO as ieei 
					LEFT JOIN INTRANET_ENROL_GROUPINFO as iegi ON iegi.EnrolGroupID=ieei.EnrolGroupID 
					LEFT Join INTRANET_GROUP as ig On (ig.GroupID = iegi.GroupID OR ieei.RelatedGroupID=ig.GroupID) 
					LEFT JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID 
					LEFT Join ACADEMIC_YEAR_TERM as ayt On (ieei.YearTermID = ayt.YearTermID) 
					LEFT JOIN ACADEMIC_YEAR as ay ON ay.AcademicYearID=ieei.AcademicYearID 
					WHERE ieei.AcademicYearID = '".$selectYear."' AND (ieei.EnrolGroupID IN ('".implode("','",$enrol_group_id_ary)."') OR ieei.RelatedGroupID IN ('".implode("','",$sel_group)."')) $conds_eventYearTermIDArr $event_date_cond 
					GROUP BY ieei.EnrolEventID 
					ORDER BY ieei.ActivityCode,ieei.EventTitle";
			$event_list = $this->returnResultSet($sql);
			
			$enrolGroupIdToEventAry = array();
			$intranetGroupIdToEventAry = array();
			$event_id_ary = array();
			for($i=0;$i<count($event_list);$i++){
				$event_id_ary[] = $event_list[$i]['EnrolEventID'];
				if($event_list[$i]['EnrolGroupID'] != '' && $event_list[$i]['EnrolGroupID'] > 0){
					if(!isset($enrolGroupIdToEventAry[$event_list[$i]['EnrolGroupID']])){
						$enrolGroupIdToEventAry[$event_list[$i]['EnrolGroupID']] = array();
					}
					$enrolGroupIdToEventAry[$event_list[$i]['EnrolGroupID']][] = $event_list[$i];
				}else if($event_list[$i]['RelatedGroupID'] != ''){
					if(!isset($intranetGroupIdToEventAry[$event_list[$i]['RelatedGroupID']])){
						$intranetGroupIdToEventAry[$event_list[$i]['RelatedGroupID']] = array();
					}
					$intranetGroupIdToEventAry[$event_list[$i]['RelatedGroupID']][] = $event_list[$i];
				}
			}
			
			// get activity event dates 
			$sql = "SELECT 
						ieei.EnrolEventID,
						ieed.ActivityDateStart,
						ieed.ActivityDateEnd 
					FROM INTRANET_ENROL_EVENTINFO as ieei 
					INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID 
					WHERE ieei.EnrolEventID IN (".implode(",",$event_id_ary).")";
			$activity_event_dates = $this->returnResultSet($sql);
			$activityEventIdToHourAry = array();
			for($i=0;$i<count($activity_event_dates);$i++){
				if(!isset($activityEventIdToHourAry[$activity_event_dates[$i]['EnrolEventID']])){
					$activityEventIdToHourAry[$activity_event_dates[$i]['EnrolEventID']] = 0;
				}
				$hour = (strtotime($activity_event_dates[$i]['ActivityDateEnd']) - strtotime($activity_event_dates[$i]['ActivityDateStart'])) / 3600;
				$activityEventIdToHourAry[$activity_event_dates[$i]['EnrolEventID']] += $hour;
			}
			
			// get activity event participants
			$sql = "SELECT ieei.EnrolEventID,COUNT(iees.StudentID) as NumberOfParticipants
					FROM INTRANET_ENROL_EVENTINFO as ieei 
					INNER JOIN INTRANET_ENROL_EVENTSTUDENT as iees ON iees.EnrolEventID=ieei.EnrolEventID 
					WHERE ieei.EnrolEventID IN (".implode(",",$event_id_ary).")
					GROUP BY ieei.EnrolEventID";
			$activity_event_participants = $this->returnResultSet($sql);
			$activityEventIdToParticipantAry = array();
			for($i=0;$i<count($activity_event_participants);$i++){
				//if(!isset($enrolGroupIdToParticipantAry[$enrol_group_participants[$i]['EnrolFroupID']])){
				//	$enrolGroupIdToParticipantAry[$enrol_group_participants[$i]['EnrolFroupID']] = 0;
				//}
				$activityEventIdToParticipantAry[$activity_event_participants[$i]['EnrolEventID']] = $activity_event_participants[$i]['NumberOfParticipants'];
			}
			
			if(count($enrolGroupIdToEventAry)>0){
				foreach($enrolGroupIdToEventAry as $t_enrol_group_id => $ary){
					$ary_count = count($ary);
					for($i=0;$i<$ary_count;$i++){
						 $t_enrol_event_id = $ary[$i]['EnrolEventID'];
						 $enrolGroupIdToEventAry[$t_enrol_group_id][$i]['TotalHours'] = isset($activityEventIdToHourAry[$t_enrol_event_id])? sprintf("%.1f",$activityEventIdToHourAry[$t_enrol_event_id]):0;
						 $enrolGroupIdToEventAry[$t_enrol_group_id][$i]['TotalParticipants'] = isset($activityEventIdToParticipantAry[$t_enrol_event_id])? $activityEventIdToParticipantAry[$t_enrol_event_id]: 0;
					}
				}
			}
			
			if(count($intranetGroupIdToEventAry)>0){
				foreach($intranetGroupIdToEventAry as $t_related_group_id => $ary){
					$ary_count = count($ary);
					for($i=0;$i<$ary_count;$i++){
						 $t_enrol_event_id = $ary[$i]['EnrolEventID'];
						 $intranetGroupIdToEventAry[$t_related_group_id][$i]['TotalHours'] = isset($activityEventIdToHourAry[$t_enrol_event_id])? sprintf("%.1f",$activityEventIdToHourAry[$t_enrol_event_id]) : 0;
						 $intranetGroupIdToEventAry[$t_related_group_id][$i]['TotalParticipants'] = isset($activityEventIdToParticipantAry[$t_enrol_event_id])? $activityEventIdToParticipantAry[$t_enrol_event_id]: 0;
					}
				}
			}
			
			for($g=0;$g<$group_list_count;$g++)
			{
				$group_id = $group_list[$g]['GroupID'];
				$group_title = $group_list[$g]['Title'];
				$number_of_enrolgroup = count($groupIdToEnrolGroupAry[$group_id]);
				
				if($format == 'csv'){
					$header = array($school_name.' - '.$group_title.' ('.$academic_year_name.')','','','');
					$rows = array();
					$row = array($Lang['eEnrolment']['GroupActivityStatistics']['Code'],
								$Lang['eEnrolment']['GroupActivityStatistics']['RelatedGroupName'],
								$Lang['eEnrolment']['GroupActivityStatistics']['NumberOfActivity'],
								$Lang['eEnrolment']['GroupActivityStatistics']['RelatedGroupActivityHours'],
								$Lang['eEnrolment']['GroupActivityStatistics']['RelatedGroupActivityParticipants'],
								$Lang['eEnrolment']['GroupActivityStatistics']['TotalNumberOfActivity'],
								$Lang['eEnrolment']['GroupActivityStatistics']['ClubOrGroupActivityHours']);
					$rows[] = $row;
				}
				
				$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%" align="center">';
					$x .= '<tr><td align="left">'.$school_name.' - '.$group_title.' ('.$academic_year_name.')'.'</td></tr>';
				$x .= '</table>';
				
				$x .= '<table class="common_table_list_v30 view_table_list_v30">
						<thead>
							<tr>
								<th width="20%" nowrap>'.$Lang['eEnrolment']['GroupActivityStatistics']['Code'].'</th>
								<th width="30%" nowrap>'.$Lang['eEnrolment']['GroupActivityStatistics']['RelatedGroupName'].'</th>
								<th width="10%" nowrap>'.$Lang['eEnrolment']['GroupActivityStatistics']['NumberOfActivity'].'</th>
								<th width="10%" nowrap>'.$Lang['eEnrolment']['GroupActivityStatistics']['RelatedGroupActivityHours'].'</th>
								<th width="10%" nowrap>'.$Lang['eEnrolment']['GroupActivityStatistics']['RelatedGroupActivityParticipants'].'</th>
								<th width="10%" nowrap>'.$Lang['eEnrolment']['GroupActivityStatistics']['TotalNumberOfActivity'].'</th>
								<th width="10%" nowrap>'.$Lang['eEnrolment']['GroupActivityStatistics']['ClubOrGroupActivityHours'].'</th>
							</tr>
						</thead>';
				 $x .= '<tbody>';
				
				if($number_of_enrolgroup == 0 && count($intranetGroupIdToEventAry[$group_id]) == 0){
					if($format == 'csv'){
						$row = array($i_no_record_searched_msg,'','','','','','');
						$rows[] = $row;
						//$rows[] = array('','','','');
					}
					$x .= '<tr><td colspan="7" style="text-align:center;">'.$i_no_record_searched_msg.'</td></tr>';
				}
				
				$tmp_enrol_group_ary = $groupIdToEnrolGroupAry[$group_id];
				$tmp_enrol_group_ary_count = count($tmp_enrol_group_ary);
				for($k=0;$k<$tmp_enrol_group_ary_count;$k++){
					$tmp_enrol_group_id = $tmp_enrol_group_ary[$k]['EnrolGroupID'];
					
					$number_of_event = count($enrolGroupIdToEventAry[$tmp_enrol_group_id]);
					
					$total_number_of_event = $tmp_enrol_group_ary[$k]['NumberOfActivity'];
					$total_number_of_hour = $tmp_enrol_group_ary[$k]['TotalHours'];
					$y = '';
					$tmp_rows = array();
					for($i=0;$i<$number_of_event;$i++){
						$total_number_of_event += $enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['NumberOfActivity'];
						$total_number_of_hour += $enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['TotalHours'];
						$y .= '<tr>
									<td>'.Get_String_Display($enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['ActivityCode'],1).'</td>
									<td>'.Get_String_Display($enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['ActivityName'],1).'</td>
									<td style="vertical-align:middle;text-align:center;">'.$enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['NumberOfActivity'].'</td>
									<td style="vertical-align:middle;text-align:center;">'.$enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['TotalHours'].'</td>
									<td style="vertical-align:middle;text-align:center;">'.$enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['TotalParticipants'].'</td>
								</tr>';
						if($format == 'csv'){
							$tmp_rows[] = array(Get_String_Display($enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['ActivityCode'],1),
												Get_String_Display($enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['ActivityName'],1),
												$enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['NumberOfActivity'],
												$enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['TotalHours'],
												$enrolGroupIdToEventAry[$tmp_enrol_group_id][$i]['TotalParticipants'],
												'');
						}
					}
					
					if($format == 'csv'){
						$row = array(Get_String_Display($tmp_enrol_group_ary[$k]['GroupCode'],1),
									Get_String_Display($tmp_enrol_group_ary[$k]['Title'],1),
									Get_String_Display($tmp_enrol_group_ary[$k]['NumberOfActivity'],1),
									$tmp_enrol_group_ary[$k]['TotalHours'],
									$tmp_enrol_group_ary[$k]['TotalParticipants'],
									$total_number_of_event,
									$total_number_of_hour);
						$rows[] = $row;
						$rows = array_merge($rows,$tmp_rows);
					}
					
					$x .= '<tr>
								<td style="font-weight:bold;">'.Get_String_Display($tmp_enrol_group_ary[$k]['GroupCode'],1).'</td>
								<td style="font-weight:bold;">'.Get_String_Display($tmp_enrol_group_ary[$k]['Title'],1).'</td>
								<td style="vertical-align:middle;text-align:center;">'.Get_String_Display($tmp_enrol_group_ary[$k]['NumberOfActivity'],1).'</td>
								<td style="vertical-align:middle;text-align:center;">'.$tmp_enrol_group_ary[$k]['TotalHours'].'</td>
								<td style="vertical-align:middle;text-align:center;">'.$tmp_enrol_group_ary[$k]['TotalParticipants'].'</td>
								<td rowspan="'.($number_of_event+1).'" style="vertical-align:middle;text-align:center;">'.$total_number_of_event.'</td>
								<td rowspan="'.($number_of_event+1).'" style="vertical-align:middle;text-align:center;">'.$total_number_of_hour.'</td>
							</tr>';
					$x .= $y;
					if($k < ($tmp_enrol_group_ary_count-1)){
						$x .= '<tr><td colspan="7">&nbsp;</td></tr>';
						if($format == 'csv'){
							$rows[] = array('','','','','','','');
						}
					}
				}
				
				// other intranet_group related events but non-club related events
				$tmp_intranet_group_event_ary = $intranetGroupIdToEventAry[$group_id];
				$tmp_intranet_group_event_ary_count = count($tmp_intranet_group_event_ary);
				$number_of_event = $tmp_intranet_group_event_ary_count;
				$total_number_of_event = 0;
				$total_number_of_hour = 0;
				$y = '';
				$tmp_rows = array();
				if($tmp_enrol_group_ary_count > 0 && $tmp_intranet_group_event_ary_count>0){
					$y .= '<tr><td colspan="7">&nbsp;</td></tr>';
					if($format == 'csv'){
						$tmp_rows[] = array('','','','','','','');
					}
				}
				for($i=0;$i<$number_of_event;$i++){
					$total_number_of_event += $tmp_intranet_group_event_ary[$i]['NumberOfActivity'];
					$total_number_of_hour += $tmp_intranet_group_event_ary[$i]['TotalHours'];
				}
				
				for($i=0;$i<$number_of_event;$i++){
					//$total_number_of_event += $tmp_intranet_group_event_ary[$i]['NumberOfActivity'];
					$y .= '<tr>
								<td>'.Get_String_Display($tmp_intranet_group_event_ary[$i]['ActivityCode'],1).'</td>
								<td>'.Get_String_Display($tmp_intranet_group_event_ary[$i]['ActivityName'],1).'</td>
								<td style="vertical-align:middle;text-align:center;">'.$tmp_intranet_group_event_ary[$i]['NumberOfActivity'].'</td>
								<td style="vertical-align:middle;text-align:center;">'.$tmp_intranet_group_event_ary[$i]['TotalHours'].'</td>
								<td style="vertical-align:middle;text-align:center;">'.$tmp_intranet_group_event_ary[$i]['TotalParticipants'].'</td>';
					if($i==0){
						$y .= '<td rowspan="'.$number_of_event.'" style="vertical-align:middle;text-align:center;">'.$total_number_of_event.'</td>';
						$y .= '<td rowspan="'.$number_of_event.'" style="vertical-align:middle;text-align:center;">'.$total_number_of_hour.'</td>';
					}
					$y .= '</tr>';
					if($format == 'csv'){
						$tmp_rows[] = array(Get_String_Display($tmp_intranet_group_event_ary[$i]['ActivityCode'],1),
											Get_String_Display($tmp_intranet_group_event_ary[$i]['ActivityName'],1),
											$tmp_intranet_group_event_ary[$i]['NumberOfActivity'],
											$tmp_intranet_group_event_ary[$i]['TotalHours'],
											$tmp_intranet_group_event_ary[$i]['TotalParticipants'],
											$i==0? $total_number_of_event : '',
											$i==0? $total_number_of_hour : '');
					}
				}
				
				$x .= $y;
				if($format == 'csv'){
					$rows = array_merge($rows,$tmp_rows);
				}
				// end of non-club related events
				
				
				$x .= '	</tbody>
						</table><br />';
				
				if($format == 'print'){
					$x .= '<div style="page-break-after:always">&nbsp;</div>';
				}
				
				if($format == 'csv'){
					$rows[] = array('','','','');
					if($g > 0){
						$exportContent .= "\r\n";
					}
					$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
				}
			}
			
			if($format == 'csv'){
				$lexport->EXPORT_FILE($Lang['eEnrolment']['GroupActivityStatistics']['GroupActivityStatistics'].".csv", $exportContent);
			}else{
				return $x;
			}
		}
	
	
		function getStaffTeachingReportData($fromDate, $toDate, $displayType, $instructor, $sortBy='')
		{
		    global $sys_custom;
		    
		    $fromDate = $fromDate." 00:00:00";
		    $toDate = $toDate." 23:59:59";
		    $userName = getNameFieldWithClassNumberByLang("u.", $sys_custom['hideTeacherTitle']);

		    if ($displayType == 'summary') {
		        
		        $sql = "SELECT
                                    ".$userName." AS Trainer,
                                    COUNT(DISTINCT e.EnrolEventID) AS NrCourse,
                                    COUNT(DISTINCT d.EventDateID) AS NrLesson,
                                    t.TrainerID
        		        FROM
        		                    INTRANET_ENROL_EVENTINFO e
        		        INNER JOIN
        		                    INTRANET_ENROL_EVENT_DATE d ON d.EnrolEventID=e.EnrolEventID
        		        INNER JOIN
        		                    INTRANET_ENROL_EVENT_DATE_TRAINER t ON t.EventDateID=d.EventDateID
                        LEFT JOIN
                                    INTRANET_USER u ON u.UserID=t.TrainerID
        		        WHERE
        		                    d.ActivityDateStart>='".$fromDate."'
                        AND
                                    d.ActivityDateEnd<='".$toDate."'
                        AND
        		                    t.TrainerID IN ('".implode("','",(array)$instructor)."')
                        GROUP BY
                                    t.TrainerID
                        ORDER BY    u.EnglishName";
		        
		        $summaryAry =  $this->returnResultSet($sql);
		        $returnAry= BuildMultiKeyAssoc($summaryAry, array('TrainerID'));

		    }
		    else {    // detail
		        switch ($sortBy) {
		            case 'course':
		                $orderBy = 'u.EnglishName, e.EventTitle, d.ActivityDateStart, d.Lesson';
		                break;
		            case 'lesson':
		                $orderBy = 'u.EnglishName, d.Lesson';
		                break;
		            case 'date':
		                $orderBy = 'u.EnglishName, d.ActivityDateStart, d.Lesson';
		                break;
		            default:
		                $orderBy = 'u.EnglishName';
		                break;
		        }
		        
		        $sql = "SELECT
                                    t.TrainerID,
                                    ".$userName." AS Trainer,
                                    e.EnrolEventID,
                                    e.EventTitle,
                                    d.EventDateID,
                                    d.Lesson,
                                    DATE(d.ActivityDateStart) AS ActivityDate,
                                    CONCAT(DATE_FORMAT(d.ActivityDateStart,'%H:%i'),' - ',DATE_FORMAT(d.ActivityDateEnd,'%H:%i')) AS TimeSlot
        		        FROM
        		                    INTRANET_ENROL_EVENTINFO e
        		        INNER JOIN
        		                    INTRANET_ENROL_EVENT_DATE d ON d.EnrolEventID=e.EnrolEventID
        		        INNER JOIN
        		                    INTRANET_ENROL_EVENT_DATE_TRAINER t ON t.EventDateID=d.EventDateID
                        LEFT JOIN
                                    INTRANET_USER u ON u.UserID=t.TrainerID
        		        WHERE
        		                    d.ActivityDateStart>='".$fromDate."'
                        AND
                                    d.ActivityDateEnd<='".$toDate."'
                        AND
        		                    t.TrainerID IN ('".implode("','",(array)$instructor)."')
                        ORDER BY ".$orderBy;
		        $detailAry =  $this->returnResultSet($sql);
		        $trainerAry = array();
		        $courseAry = array();
		        $lessonAry = array();
		        for($i=0,$iMax=count($detailAry);$i<$iMax;$i++) {
		            $_colAry = $detailAry[$i];
		            $_trainerID = $_colAry['TrainerID'];
		            $_trainer = $_colAry['Trainer'];
		            $_enrolEventID= $_colAry['EnrolEventID'];
		            $_eventTitle= $_colAry['EventTitle'];
 		            $_eventDateID= $_colAry['EventDateID'];

		            $lessonInfoAry = array();
		            $lessonInfoAry['EventDateID'] = $_colAry['EventDateID'];
		            $lessonInfoAry['Lesson'] = $_colAry['Lesson'];
		            $lessonInfoAry['ActivityDate'] = $_colAry['ActivityDate'];
		            $lessonInfoAry['TimeSlot'] = $_colAry['TimeSlot'];
		            
		            if (!isset($trainerAry[$_trainerID])) {
		                $trainerAry[$_trainerID] = $_trainer;
		            }
		            if (!isset($courseAry[$_trainerID][$_enrolEventID])) {
		                $courseAry[$_trainerID][$_enrolEventID] = $_eventTitle;
		            }
		            if (!isset($lessonAry[$_trainerID][$_enrolEventID][$_eventDateID])) {
		                $lessonAry[$_trainerID][$_enrolEventID][$_eventDateID] = $lessonInfoAry;
		            }
		        }
		        $returnAry['trainer'] = $trainerAry;  
		        $returnAry['course'] = $courseAry;
		        $returnAry['lesson'] = $lessonAry;
		    }
		    return $returnAry;
		}
		
		function getHiddenTableForExport($headerAry,$contentAry,$widthAry,$reportTitle='')
		{
		    $seperator = "<br />";
		    
		    $x = '<form id="form2" name="form2" method="post">';
		    $x .= '<input type="hidden" name="reportTitle" value="'.$reportTitle.'">';
		    
		    for($i=0,$iMax=count($headerAry);$i<$iMax;$i++){
		        $x .= '<input type="hidden" name="headerAry[]" value="'.$headerAry[$i].'">';
		    }
		    
		    for($i=0,$iMax=count($contentAry);$i<$iMax;$i++){
		        for($j=0,$jMax=count($contentAry[$i]);$j<$jMax;$j++){
		            $x .= '<input type="hidden" name="contentAry['.$i.'][]" value="'.$contentAry[$i][$j].'">';
		        }
		    }
		    $x .= '<input type="hidden" name="seperator" value="'.$seperator.'" >';

		    for($i=0,$iMax=count($widthAry);$i<$iMax;$i++){
		        $x .= '<input type="hidden" name="widthAry[]" value="'.$widthAry[$i].'" >';
		    }
		    
		    $x .= '</form>';
		    
		    return $x;
		}
		
		function getStaffTeachingDetailReport($dataAry, $returnHidden=true, $reportTitle='')
		{
		    global $Lang;
		    
		    $x = '<br>';
		    $hiddenField = '<form id="form2" name="form2" method="post">';
		    $hiddenField .= '<input type="hidden" name="displayType" value="detail">';
		    $hiddenField .= '<input type="hidden" name="reportTitle" value="'.$reportTitle.'">';
		    
		    $tableHeader = '<table class="common_table_list_v30 view_table_list_v30 ">'."\n";
    		    $tableHeader .= '<thead>'."\n";
        		    $tableHeader .= '<tr>'."\n";
            		    $tableHeader .= '<th width="3%">#</th>'."\n";
            		    $tableHeader .= '<th width="30%">'.$Lang['eEnrolment']['curriculumTemplate']['course']['course'].'</th>'."\n";
            		    $tableHeader .= '<th width="30%">'.$Lang['eEnrolment']['curriculumTemplate']['course']['lesson'].'</th>'."\n";
            		    $tableHeader .= '<th width="15%">'.$Lang['General']['Date'].'</th>'."\n";
            		    $tableHeader .= '<th width="20%">'.$Lang['General']['Time'].'</th>'."\n";
        		    $tableHeader .= '</tr>'."\n";
    		    $tableHeader .= '</thead>'."\n";
		    
		    if (count($dataAry['trainer']) == 0) {
		        $x .= $tableHeader;
    		        $x .= '<tbody>'."\n";
        		        $x .= '<tr>'."\n";
            		        $x .= '<td colspan="5" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>'."\n";
        		        $x .= '</tr>'."\n";
    		        $x .= '</tbody>'."\n";
		        $x .= '</table>'."\n";
		    }
		    else {
		        foreach((array)$dataAry['trainer'] as $_trainerID => $_trainer) {
		            
		            $courseCount = 0;
		            $hiddenField .= '<input type="hidden" name="trainerAry['.$_trainerID.']" value="'.intranet_htmlspecialchars($_trainer).'">';
		            
		            $x .= '<br><div style="font-weight:bold">'.$_trainer.'</div><br>'."\n";
		                $x .= $tableHeader;
		            $x .= '<tbody>'."\n";
		            
		            foreach((array)$dataAry['course'][$_trainerID] as $__courseID => $__course) {
		                $lessonCount = 0;
		                $hiddenField .= '<input type="hidden" name="courseAry['.$_trainerID.']['.$__courseID.']" value="'.intranet_htmlspecialchars($__course).'">';
		                
		                foreach((array)$dataAry['lesson'][$_trainerID][$__courseID] as $___lessonID => $___lessonAry) {
		                    $___lesson = $___lessonAry['Lesson'];
		                    $___date = $___lessonAry['ActivityDate'];
		                    $___time = $___lessonAry['TimeSlot'];
		                    
		                    $hiddenField .= '<input type="hidden" name="lessonAry['.$_trainerID.']['.$__courseID.']['.$___lessonID.'][Lesson]" value="'.intranet_htmlspecialchars($___lesson).'">';
		                    $hiddenField .= '<input type="hidden" name="lessonAry['.$_trainerID.']['.$__courseID.']['.$___lessonID.'][ActivityDate]" value="'.$___date.'">';
		                    $hiddenField .= '<input type="hidden" name="lessonAry['.$_trainerID.']['.$__courseID.']['.$___lessonID.'][TimeSlot]" value="'.$___time.'">';
		                    
		                    $x .= '<tr>'."\n";
		                    if ($lessonCount == 0) {
		                        $x .= '<td>'.($courseCount+1).'</td>'."\n";
		                        $x .= '<td>'.$__course.'</td>'."\n";
		                    }
		                    else {
		                        $x .= '<td>&nbsp;</td>'."\n";
		                        $x .= '<td>&nbsp;</td>'."\n";
		                    }
    		                    $x .= '<td>'.$___lesson.'</td>'."\n";
    		                    $x .= '<td>'.$___date.'</td>'."\n";
    		                    $x .= '<td>'.$___time.'</td>'."\n";
		                    $x .= '</tr>'."\n";
		                    
		                    $lessonCount++;
		                    
		                } // lesson
		                
		                $courseCount++;
		                
		            }     // course
    		            $x .= '</tbody>'."\n";
		            $x .= '</table>'."\n";
		            
		            $x .= '<div>&nbsp;</div>'."\n";   // break
		            
		        }     // trainer
		    }         // not empty
		    
		    return $returnHidden ? $x.$hiddenField : $x;
		    
		}
		
		function getStaffTeachingReport($fromDate, $toDate, $displayType, $instructor, $sortBy)
		{
		    global $Lang;
		    
		    $dataAry = $this->getStaffTeachingReportData($fromDate, $toDate, $displayType, $instructor, $sortBy);

		    if ($displayType == 'summary') {
	           $headerAry = array();
	           $headerAry[] = $Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'];
	           $headerAry[] = $Lang['eEnrolment']['report']['staffTeachingSummary']['nrCourse'];
	           $headerAry[] = $Lang['eEnrolment']['report']['staffTeachingSummary']['nrLesson'];
	           
	           $contentAry = array();
	           foreach((array)$dataAry as $_row=>$_col) {
	               $colAry = array();
	               $colAry[] = $_col['Trainer'];
	               $colAry[] = $_col['NrCourse'];
	               $colAry[] = $_col['NrLesson'];
	               $contentAry[] = $colAry;
	           }
	           
	           $widthAry = array();
	           $widthAry[] = '50%';
	           $widthAry[] = '20%';
	           $widthAry[] = '20%';
	           
	           
	           $reportTitle =  $Lang['eEnrolment']['StaffTeachingSummary']['ReportName'].' ( ' . $fromDate .' to '. $toDate . ')';
	           $resultTable = $this->getGeneralEnrolmentReportTableHtml($headerAry,$contentAry,$widthAry);
	           $hiddenTable = $this->getHiddenTableForExport($headerAry,$contentAry,$widthAry, $reportTitle);
	           return $resultTable.$hiddenTable;
		    }
		    else {        // detail
		        $reportTitle = $Lang['eEnrolment']['StaffTeachingSummary']['ReportDetail'].' ( ' . $fromDate .' to '. $toDate . ')';
		        return $this->getStaffTeachingDetailReport($dataAry, $returnHidden=true, $reportTitle);
		    }
		}

		function getClassTrainingReportData($intakeID, $class, $displayType, $sortBy)
		{
		    if ($displayType == 'summary') {
		        
		        $sql = "SELECT
                                    c.ClassName,
                                    COUNT(DISTINCT e.EnrolEventID) AS NrCourse,
                                    COUNT(DISTINCT d.EventDateID) AS NrLesson,
                                    c.ClassID
        		        FROM
                                    INTRANET_ENROL_GROUPINFO_MAIN m
                        INNER JOIN
                                    INTRANET_ENROL_INTAKE_CLASS c ON c.IntakeID=m.IntakeID 
                        INNER JOIN
                                    INTRANET_ENROL_GROUPINFO g ON g.GroupID=m.GroupID
                        INNER JOIN
        		                    INTRANET_ENROL_EVENTINFO e ON e.EnrolGroupID=g.EnrolGroupID 
        		        INNER JOIN
        		                    INTRANET_ENROL_EVENT_DATE d ON d.EnrolEventID=e.EnrolEventID
                        INNER JOIN
        		                    INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS edic ON edic.EventDateID=d.EventDateID AND edic.IntakeClassID=c.ClassID
        		        WHERE
                                    m.ClubType='Y'
                        AND 
                                    m.IntakeID='".$intakeID."'
                        AND
                                    edic.IntakeClassID IN ('".implode("','",$class)."')
                        GROUP BY    c.ClassID
                        ORDER BY    c.ClassName";
		        
		        $summaryAry =  $this->returnResultSet($sql);
		        $returnAry= BuildMultiKeyAssoc($summaryAry, array('ClassID'));
//  debug_pr($sql);
//  debug_pr($summaryAry);
		    }
		    else {    // detail
		        switch ($sortBy) {
		            case 'course':
		                $orderBy = 'c.ClassName, e.EventTitle, d.ActivityDateStart, d.Lesson';
		                break;
		            case 'lesson':
		                $orderBy = 'c.ClassName, d.Lesson';
		                break;
		            case 'date':
		                $orderBy = 'c.ClassName, d.ActivityDateStart, d.Lesson';
		                break;		                
		            default:
		                $orderBy = 'c.ClassName';
		                break;
		        }
		        
		        $sql = "SELECT
                                    c.ClassName,
                        			e.EnrolEventID,
                        			e.EventTitle,
                        			e.IntakeClassID,
                        			d.EventDateID,
                        			d.Lesson,
                                    DATE(d.ActivityDateStart) AS ActivityDate,
                                    CONCAT(DATE_FORMAT(d.ActivityDateStart,'%H:%i'),' - ',DATE_FORMAT(d.ActivityDateEnd,'%H:%i')) AS TimeSlot,
                        			c.ClassID
        		        FROM
                                    INTRANET_ENROL_GROUPINFO_MAIN m
                        INNER JOIN
                                    INTRANET_ENROL_INTAKE_CLASS c ON c.IntakeID=m.IntakeID
                        INNER JOIN
                                    INTRANET_ENROL_GROUPINFO g ON g.GroupID=m.GroupID
                        INNER JOIN
        		                    INTRANET_ENROL_EVENTINFO e ON e.EnrolGroupID=g.EnrolGroupID
        		        INNER JOIN
        		                    INTRANET_ENROL_EVENT_DATE d ON d.EnrolEventID=e.EnrolEventID
                        INNER JOIN
        		                    INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS edic ON edic.EventDateID=d.EventDateID AND edic.IntakeClassID=c.ClassID
        		        WHERE
                                    m.ClubType='Y'
                        AND
                                    m.IntakeID='".$intakeID."'
                        AND
                                    edic.IntakeClassID IN ('".implode("','",$class)."')
                        ORDER BY ".$orderBy;

		        $detailAry = $this->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($detailAry);
		        $classAry = array();
		        $courseAry = array();
		        $lessonAry = array();
		        for($i=0,$iMax=count($detailAry);$i<$iMax;$i++) {
		            $_colAry = $detailAry[$i];
		            $_classID = $_colAry['ClassID'];
		            $_className = $_colAry['ClassName'];
		            $_enrolEventID= $_colAry['EnrolEventID'];
		            $_eventTitle= $_colAry['EventTitle'];
		            $_eventDateID= $_colAry['EventDateID'];
		            
		            $lessonInfoAry = array();
		            $lessonInfoAry['EventDateID'] = $_colAry['EventDateID'];
		            $lessonInfoAry['Lesson'] = $_colAry['Lesson'];
		            $lessonInfoAry['ActivityDate'] = $_colAry['ActivityDate'];
		            $lessonInfoAry['TimeSlot'] = $_colAry['TimeSlot'];
		            
		            if (!isset($classAry[$_classID])) {
		                $classAry[$_classID] = $_className;
		            }
		            if (!isset($courseAry[$_classID][$_enrolEventID])) {
		                $courseAry[$_classID][$_enrolEventID] = $_eventTitle;
		            }
		            if (!isset($lessonAry[$_classID][$_enrolEventID][$_eventDateID])) {
		                $lessonAry[$_classID][$_enrolEventID][$_eventDateID] = $lessonInfoAry;
		            }
		        }
		        $returnAry['class'] = $classAry;
		        $returnAry['course'] = $courseAry;
		        $returnAry['lesson'] = $lessonAry;
		    }
		    return $returnAry;
		    
		}
		
		function getClassTrainingReport($intakeID, $class, $displayType, $sortBy)
		{
		    global $Lang;
		    
		    $dataAry = $this->getClassTrainingReportData($intakeID, $class, $displayType, $sortBy);
		    $intakeList = $this->getIntakeList($intakeID);
		    
		    if ($displayType == 'summary') {
		        $headerAry = array();
		        $headerAry[] = $Lang['eEnrolment']['curriculumTemplate']['intake']['class'];
		        $headerAry[] = $Lang['eEnrolment']['report']['staffTeachingSummary']['nrCourse'];
		        $headerAry[] = $Lang['eEnrolment']['report']['staffTeachingSummary']['nrLesson'];
		        
		        $contentAry = array();
		        foreach((array)$dataAry as $_row=>$_col) {
		            $colAry = array();
		            $colAry[] = $_col['ClassName'];
		            $colAry[] = $_col['NrCourse'];
		            $colAry[] = $_col['NrLesson'];
		            $contentAry[] = $colAry;
		        }
		        
		        $widthAry = array();
		        $widthAry[] = '50%';
		        $widthAry[] = '20%';
		        $widthAry[] = '20%';
		        
//		        $reportTitleDiv = '<br><br><div align="center">'.$reportTitle.'</div>'."\n";
		        $resultTable = $this->getGeneralEnrolmentReportTableHtml($headerAry,$contentAry,$widthAry);
		        
		        $reportTitle = $Lang['eEnrolment']['ClassTrainingSummary']['ReportName'] .' - ' . $intakeList[0]['Title'];
		        $hiddenTable = $this->getHiddenTableForExport($headerAry,$contentAry,$widthAry,$reportTitle);
		        
		        return $resultTable.$hiddenTable;
		    }
		    else {        // detail
		        $reportTitle = $Lang['eEnrolment']['ClassTrainingSummary']['ReportDetail'] .' - ' . $intakeList[0]['Title'];
		        return $this->getClassTrainingDetailReport($dataAry, true, $reportTitle);
		    }
		}
		
		function getClassTrainingDetailReport($dataAry, $returnHidden=true, $reportTitle='')
		{
		    global $Lang;
		    
		    $x = '<br>';
		    $hiddenField = '<form id="form2" name="form2" method="post">';
		    $hiddenField .= '<input type="hidden" name="displayType" value="detail">';
		    $hiddenField .= '<input type="hidden" name="reportTitle" value="'.$reportTitle.'">';
		    
// 		    $x .= '<div align="center">'.$reportTitle.'</div><br>'."\n";
// 		    if ($reportTitle) {		        
// 		        $hiddenField .= '<input type="hidden" name="reportTitle" value="'.$reportTitle.'">';
// 		    }

		    $tableHeader = '<table class="common_table_list_v30 view_table_list_v30 ">'."\n";
		    $tableHeader .= '<thead>'."\n";
		    $tableHeader .= '<tr>'."\n";
		    $tableHeader .= '<th width="3%">#</th>'."\n";
		    $tableHeader .= '<th width="30%">'.$Lang['eEnrolment']['curriculumTemplate']['course']['course'].'</th>'."\n";
		    $tableHeader .= '<th width="30%">'.$Lang['eEnrolment']['curriculumTemplate']['course']['lesson'].'</th>'."\n";
		    $tableHeader .= '<th width="15%">'.$Lang['General']['Date'].'</th>'."\n";
		    $tableHeader .= '<th width="20%">'.$Lang['General']['Time'].'</th>'."\n";
		    $tableHeader .= '</tr>'."\n";
		    $tableHeader .= '</thead>'."\n";
		    
		    if (count($dataAry['class']) == 0) {
		        $x .= $tableHeader;
		        $x .= '<tbody>'."\n";
		        $x .= '<tr>'."\n";
		        $x .= '<td colspan="5" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td>'."\n";
		        $x .= '</tr>'."\n";
		        $x .= '</tbody>'."\n";
		        $x .= '</table>'."\n";
		    }
		    else {
		        foreach((array)$dataAry['class'] as $_classID => $_class) {
		            
		            $courseCount = 0;
		            $hiddenField .= '<input type="hidden" name="classAry['.$_classID.']" value="'.intranet_htmlspecialchars($_class).'">';
		            
		            $x .= '<br><div style="font-weight:bold">'.$_class.'</div><br>'."\n";
		            $x .= $tableHeader;
		            $x .= '<tbody>'."\n";
		            
		            foreach((array)$dataAry['course'][$_classID] as $__courseID => $__course) {
		                $lessonCount = 0;
		                $hiddenField .= '<input type="hidden" name="courseAry['.$_classID.']['.$__courseID.']" value="'.intranet_htmlspecialchars($__course).'">';
		                
		                foreach((array)$dataAry['lesson'][$_classID][$__courseID] as $___lessonID => $___lessonAry) {
		                    $___lesson = $___lessonAry['Lesson'];
		                    $___date = $___lessonAry['ActivityDate'];
		                    $___time = $___lessonAry['TimeSlot'];
		                    
		                    $hiddenField .= '<input type="hidden" name="lessonAry['.$_classID.']['.$__courseID.']['.$___lessonID.'][Lesson]" value="'.intranet_htmlspecialchars($___lesson).'">';
		                    $hiddenField .= '<input type="hidden" name="lessonAry['.$_classID.']['.$__courseID.']['.$___lessonID.'][ActivityDate]" value="'.$___date.'">';
		                    $hiddenField .= '<input type="hidden" name="lessonAry['.$_classID.']['.$__courseID.']['.$___lessonID.'][TimeSlot]" value="'.$___time.'">';
		                    
		                    $x .= '<tr>'."\n";
		                    if ($lessonCount == 0) {
		                        $x .= '<td>'.($courseCount+1).'</td>'."\n";
		                        $x .= '<td>'.$__course.'</td>'."\n";
		                    }
		                    else {
		                        $x .= '<td>&nbsp;</td>'."\n";
		                        $x .= '<td>&nbsp;</td>'."\n";
		                    }
		                    $x .= '<td>'.$___lesson.'</td>'."\n";
		                    $x .= '<td>'.$___date.'</td>'."\n";
		                    $x .= '<td>'.$___time.'</td>'."\n";
		                    $x .= '</tr>'."\n";
		                    
		                    $lessonCount++;
		                    
		                } // lesson
		                
		                $courseCount++;
		                
		            }     // course
		            $x .= '</tbody>'."\n";
		            $x .= '</table>'."\n";
		            
		            $x .= '<div>&nbsp;</div>'."\n";   // break
		            
		        }     // class
		    }         // not empty
		    
		    $hiddenField .= '</form>';

		    return $returnHidden ? $x.$hiddenField : $x;
		    
		}
		
		function getClassMembershipReportHeaderAry() {
		    global $Lang, $eEnrollment, $sys_custom;
		    
		    $headerAry = array();
		    $headerAry['title'][] = $Lang['General']['UserLogin'];
		    $headerAry['width'][] = 10;
		    $headerAry['title'][] = $eEnrollment['student_name'];
		    $headerAry['width'][] = 15;
		    $headerAry['title'][] = $Lang['General']['Class'];
		    $headerAry['width'][] = 10;
		    $headerAry['title'][] = $Lang['General']['ClassNumber'];
		    $headerAry['width'][] = 8;
		    $headerAry['title'][] = $Lang['eEnrolment']['ClubCode'];
		    $headerAry['width'][] = 8;
		    $headerAry['title'][] = $Lang['eEnrolment']['ClubName'];
		    $headerAry['width'][] = 20;
		    $headerAry['title'][] = $eEnrollment['performance'];
		    $headerAry['width'][] = 10;
		    
		    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
		        $termNameLang = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
		        
		        $sqlTemp = "SELECT YearTermID, $termNameLang as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
		        $termAry = $this->returnResultSet($sqlTemp);
		        $numOfTerm = count($termAry);
		        
		        for ($i=0; $i<$numOfTerm; $i++) {
		            $_yearTermName = $termAry[$i]['TermName'];
		            
		            $headerAry['title'][] = $_yearTermName.' '.$eEnrollment['performance'];
		            $headerAry['width'][] = 10;
		        }
		    }
		    return $headerAry;
		}
		
		function getClassMembershipReportResultAry($parUserId) {
		    global $PATH_WRT_ROOT, $sys_custom;
		    
		    include_once($PATH_WRT_ROOT."includes/libclass.php");
		    include_once($PATH_WRT_ROOT."includes/libuser.php");
		    
		    $lclass = new libclass();
		    
		    $classAry = $lclass->GetClassOfClassTeacher($_SESSION['UserID']);
		    $numOfClass = count($classAry);
		    
		    $memberInfoAry = $this->Get_Club_Member_Info($EnrolGroupIDArr='', $PersonTypeArr='', Get_Current_Academic_Year_ID(), $IndicatorArr=0, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0,
		        $YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='', $ActiveMemberOnly=0, $FormIDArr='');
		    $memberInfoAssoAry = BuildMultiKeyAssoc($memberInfoAry, 'StudentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		    
		    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
		        $termNameLang = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
		        
		        $sqlTemp = "SELECT YearTermID, $termNameLang as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
		        $termAry = $this->returnResultSet($sqlTemp);
		        $numOfTerm = count($termAry);
		        
		        $involvedStudentIdAry = Get_Array_By_Key($memberInfoAry, 'StudentID');
		        $termBasedPerformanceAssoAry = BuildMultiKeyAssoc($this->getClubTermBasedStudentPerformance('', $involvedStudentIdAry), array('StudentID', 'EnrolGroupID', 'YearTermID'));
		    }
		    
		    
		    $resultAry = array();
		    for ($i=0; $i<$numOfClass; $i++) {
		        $_classId = $classAry[$i]['ClassID'];
		        $_className = $classAry[$i]['ClassName'];
		        
		        $_studentAry = $lclass->getStudentByClassId($_classId);
		        $_studentIdAry = Get_Array_By_Key($_studentAry, 'UserID');
		        $_numOfStudent = count($_studentIdAry);
		        
		        if ($_numOfStudent == 0) {
		            continue;
		        }
		        
		        $_userObj = new libuser('', '', $_studentIdAry);
		        
		        for ($j=0; $j<$_numOfStudent; $j++) {
		            $__studentId = $_studentIdAry[$j];
		            $_userObj->LoadUserData($__studentId);
		            
		            $__studentMembershipAry = $memberInfoAssoAry[$__studentId];
		            $__numOfStudentClub = (is_array($__studentMembershipAry))? count($__studentMembershipAry) : 0;
		            for ($k=0; $k<$__numOfStudentClub; $k++) {
		                $___enrolGroupId = $__studentMembershipAry[$k]['EnrolGroupID'];
		                $___groupCode = $__studentMembershipAry[$k]['GroupCode'];
		                $___groupTitle = $__studentMembershipAry[$k]['GroupTitle'];
		                $___performance = $__studentMembershipAry[$k]['Performance'];
		                
		                $___tmpAry = array();
		                $___tmpAry[] = $_userObj->UserLogin;
		                $___tmpAry[] = Get_Lang_Selection($_userObj->ChineseName, $_userObj->EnglishName);
		                $___tmpAry[] = $_userObj->ClassName;
		                $___tmpAry[] = $_userObj->ClassNumber;
		                $___tmpAry[] = $___groupCode;
		                $___tmpAry[] = $___groupTitle;
		                $___tmpAry[] = $___performance;
		                
		                if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
		                    for ($l=0; $l<$numOfTerm; $l++) {
		                        $____yearTermId = $termAry[$l]['YearTermID'];
		                        
		                        $____performance = $termBasedPerformanceAssoAry[$__studentId][$___enrolGroupId][$____yearTermId]['Performance'];
		                        $___tmpAry[] = $____performance;
		                    }
		                }
		                
		                $resultAry[$_classId]['studentMembershipAry'][] = $___tmpAry;
		            }
		        }
		        
		        if (count($resultAry[$_classId]['studentMembershipAry']) > 0) {
		            $resultAry[$_classId]['classInfo']['className'] = $_className;
		        }
		    }
		    
		    return $resultAry;
		}
		
		function getClassMembershipReportResultTableHtml($parUserId) {
		    global $PATH_WRT_ROOT;
		    
		    include_once($PATH_WRT_ROOT."includes/libinterface.php");
		    include_once($PATH_WRT_ROOT."includes/table/table_commonTable.php");
		    $linterface = new interface_html();
		    
		    $headerArr = $this->getClassMembershipReportHeaderAry();
		    $numOfHeader = count($headerArr['title']);
		    
		    $reportDataAry = $this->getClassMembershipReportResultAry($parUserId);
		    
		    $x = '';
		    foreach ((array)$reportDataAry as $_classId => $_classDataAry) {
		        $_className = $_classDataAry['classInfo']['className'];
		        $_dataAry = $_classDataAry['studentMembershipAry'];
		        
		        $x .= '<span style="float:left;">'.$linterface->GET_NAVIGATION2($_className).'</span>'."\n\r";
		        $x .= '<br style="clear:both;" />'."\n\r";
		        
		        $_tableObj = new table_commonTable();
		        $_tableObj->setTableType('view');
		        $_tableObj->setApplyConvertSpecialChars(false);
		        $_tableObj->addHeaderTr(new tableTr());
		        $_tableObj->addHeaderCell(new tableTh('#','3%'));
		        
		        for ($i=0; $i<$numOfHeader; $i++) {
		            $_tableObj->addHeaderCell(new tableTh($headerArr['title'][$i], $headerArr['width'][$i].'%'));
		        }
		        $_numOfRow = count($_dataAry);
		        for ($i=0; $i<$_numOfRow; $i++) {
		            $__rowContentAry = $_dataAry[$i];
		            $__numOfCol = count($__rowContentAry);
		            
		            $_tableObj->addBodyTr(new tableTr());
		            $_tableObj->addBodyCell(new tableTd($i+1));
		            
		            for ($j=0; $j<$__numOfCol; $j++) {
		                $_tableObj->addBodyCell(new tableTd($__rowContentAry[$j]));
		            }
		        }
		        $x .= $_tableObj->returnHtml();
		        $x .= '<br />';
		    }
		    
		    return $x;
		}
		
		function getClassMembershipReportResultExportAry($parUserId) {
		    $reportDataAry = $this->getClassMembershipReportResultAry($parUserId);
		    
		    $exportDataAry = array();
		    foreach ((array)$reportDataAry as $_classId => $_classDataAry) {
		        $_className = $_classDataAry['classInfo']['className'];
		        $_dataAry = $_classDataAry['studentMembershipAry'];
		        
		        $exportDataAry = array_merge((array)$exportDataAry, (array)$_dataAry);
		    }
		    
		    return $exportDataAry;
		}

		function getDisplayWeekByCal($startDate)
		{
		    global $Lang;
		    
		    $startDate = $this->getMondayOfTheWeek($startDate,true);
		    
		    $curr_year = substr($startDate, 0, 4);
		    $curr_month = substr($startDate, 5, 2);
		    $curr_day = substr($startDate, 8, 2);
		    
		    $startOfTheWeek = $startDate;
		    $endOfTheWeek = date("Y-m-d", mktime(0, 0, 0, $curr_month, $curr_day + 6, $curr_year));
		    
		    $startYearOfTheWeek = substr($startOfTheWeek, 0, 4);
		    $startMonthOfTheWeek = substr($startOfTheWeek, 5, 2);
		    $startDayOfTheWeek = substr($startOfTheWeek, 8, 2);
		    
		    $endYearOfTheWeek = substr($endOfTheWeek, 0, 4);
		    $endMonthOfTheWeek = substr($endOfTheWeek, 5, 2);
		    $endDayOfTheWeek = substr($endOfTheWeek, 8, 2);
		    
		    $displayStartOfTheWeek = $startDayOfTheWeek . " " . $Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int) $startMonthOfTheWeek] . "," . $startYearOfTheWeek;
		    $displayEndOfTheWeek = $endDayOfTheWeek . " " . $Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int) $endMonthOfTheWeek] . "," . $endYearOfTheWeek;
		    $weekStartTimeStamp = strtotime($startOfTheWeek);
		    
		    $ret = array();
		    $ret['TimeStamp'] = $weekStartTimeStamp;
		    $ret['Display'] = $displayStartOfTheWeek . " - " . $displayEndOfTheWeek;
		    
		    return $ret;
		}
		
		function getClassWeeklyScheduleLayout($intakeID, $classID='', $startDate='')
		{
		    global $Lang, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		    include_once($PATH_WRT_ROOT."includes/libinterface.php");
		    include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
		    
		    $linterface = new interface_html();
		    $libenroll_ui= new libclubsenrol_ui();
		    
		    // # print btn
		    $toolbar = $linterface->GET_LNK_PRINT("#", "", "jsPrintClassWeeklySchedule();", "", "", 0);
		    // # export btn
		    $toolbar .= $linterface->GET_LNK_EXPORT("#", "", "jsExportClassWeeklySchedule();", "", "", 0);

		    $firstTitle = $Lang['eEnrolment']['report']['selectIntake'];
		    $intakeSelection = $libenroll_ui->getIntakeSelection('IntakeID', $intakeID, $firstTitle);
		    $intakeList = $this->getIntakeList($intakeID);
		    $today = date('Y-m-d');
		    if ($startDate == '') {
		        $startDate = $today;
		    }
		    
		    if (count($intakeList)){
		        $currentIntake = $intakeList[0];
		        $startDate = $currentIntake['StartDate'] > $today ? $currentIntake['StartDate'] : $today;
		        $intakeID = $intakeID ? $intakeID : $currentIntake['IntakeID'];
		        $firstTitle = $Lang['SysMgr']['FormClassMapping']['AllClass'];
		        $classSelection = $libenroll_ui->getClassSelection($intakeID, $classID, $name='IntakeClassID', $firstTitle);
		    }
		    else {
		        $classSelection = '';
		    }
		    
		    $displayWeekInfoAry = $this->getDisplayWeekByCal($startDate);
		    
		    $filter_bar .= '<table border="0">' . "\n";
    		    $filter_bar .= '<tr>' . "\n";
        		    $filter_bar .= '<td>' . $intakeSelection. '</td>' . "\n";
        		    $filter_bar .= '<td id="tdIntakeClassSelection">' . $classSelection . '</td>' . "\n";
    		    $filter_bar .= '</tr>' . "\n";
		    $filter_bar .= '</table>' . "\n";
		    
		    $sub_heading = '<table border="0" class="form_table_v30">' . "\n";
		        $sub_heading .= '<tr>' . "\n";
		        $sub_heading .= '<td class="field_title" style="width:10%">' . $Lang['eEnrolment']['report']['subHeading']. '</td>' . "\n";
		        $sub_heading .= '<td><input type="text" name="SubHeading" id="SubHeading" class="textboxtext" value="" maxlength="100"></td>' . "\n";
		        $sub_heading .= '</tr>' . "\n";
		    $sub_heading .= '</table>' . "\n";
		    
		    
		    $prev_week_link = "<a href='#' onClick='jsPrevWeek()'><img height='21' border='0' align='absmiddle' width='21' src='" . $image_path . "/" . $LAYOUT_SKIN . "/icalendar/icon_prev_off.gif'></a>";
		    $next_week_link = "<a href='#' onClick='jsNextWeek()'><img height='21' border='0' align='absmiddle' width='21' src='" . $image_path . "/" . $LAYOUT_SKIN . "/icalendar/icon_next_off.gif'></a>";
		    $week_by_calendar = "<input type='hidden' name='WeekStartDate' id='WeekStartDate' value='' onChange='jsChangeWeek();'>";
		    
		    $week_selection = $prev_week_link . "<span id='DIV_WeeklyDisplay'>" . $displayWeekInfoAry['Display'] . "</span>" . $next_week_link . $week_by_calendar;
		    
		    $tab_content .= "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
    		    $tab_content .= "<tr>";
        		    $tab_content .= "<td width='30%' align='center'></td>";
        		    $tab_content .= "<td width='40%' align='center' valign='bottom'><span class='icalendar_title'>" . $week_selection . "</span></td>";
        		    $tab_content .= "<td width='30%' align='center' valign='bottom'></td>";
    		    $tab_content .= "</tr>";
		    $tab_content .= "</table>";
		    
		    $content = "<form name='form2' id='form2' action='' method='POST'>";
		    $content .= "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
		        $content .= "<tr><td>" . $toolbar . "</td></tr>";
		    $content .= "</table>";
		    
		    $content .= "<div id='BlockTable'>";
    		    $content .= "<table border='0' width='100%' cellpadding='0' cellspacing='0' style='border-bottom: 1px solid rgb(219,237,255)'>";
        		    $content .= "<tr><td align='left'>" . $filter_bar . "</td></tr>";
        		    $content .= "<tr><td align='left'>" . $sub_heading. "</td></tr>";
        		    $content .= "<tr><td align='center' valign='bottom'>" . $tab_content . "</td></tr>";
    		    $content .= "</table>";
		    
    		    $content .= "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
    		        $content .= "<tr><td style='padding: 5px; background-color:rgb(219,237,255)'><div id='reportResultDiv'></div></td></tr>";
    		    $content .= "</table>";
		    $content .= "</div>";

		    $content .= "<input type='hidden' name='WeekStartTimeStamp' id='WeekStartTimeStamp' value='" . $displayWeekInfoAry['TimeStamp']. "'>";
		    $content .= "</form>";
		    
		    return $content;
		    
		}
	
		function getClassWeeklyScheduleData($intakeID, $classID, $startDate)
		{
		    global $sys_custom;
		    
		    $startTimeStamp = strtotime($startDate);
		    $endDate = date('Y-m-d',strtotime('7 day',$startTimeStamp));
		    
		    $userName = getNameFieldWithClassNumberByLang("u.", $sys_custom['hideTeacherTitle']);
		    $buildingName = Get_Lang_Selection("b.NameChi","b.NameEng");
		    $floorName = Get_Lang_Selection("f.NameChi","f.NameEng");
		    $roomName = Get_Lang_Selection("r.NameChi","r.NameEng");
		    
	        $sql = "SELECT
                        			d.EventDateID,
                        			d.Lesson,
                                    DATE(d.ActivityDateStart) AS ActivityDate,
                                    d.ActivityDateStart AS StartTime,
                                    d.ActivityDateEnd AS EndTime,
                                    CONCAT(DATE_FORMAT(d.ActivityDateStart,'%H:%i'),'-',DATE_FORMAT(d.ActivityDateEnd,'%H:%i')) AS TimeSlot,
                                    (UNIX_TIMESTAMP(d.ActivityDateEnd) - UNIX_TIMESTAMP(d.ActivityDateStart)) AS Duration,
                                    t.Instructor,
                                    t.InstructorID,
                                    CONCAT({$buildingName},' > ',{$floorName}, ' > ',{$roomName}) AS Location
        		        FROM
      		                        INTRANET_ENROL_EVENTINFO e 
	                    INNER JOIN
    		                        INTRANET_ENROL_EVENT_DATE d ON d.EnrolEventID=e.EnrolEventID
                        INNER JOIN 
                                    INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS dic ON dic.EventDateID=d.EventDateID
	                    INNER JOIN
                                    INTRANET_ENROL_GROUPINFO g ON g.EnrolGroupID=e.EnrolGroupID
                        INNER JOIN
                                    INTRANET_ENROL_GROUPINFO_MAIN m ON m.GroupID=g.GroupID
                        LEFT JOIN   (SELECT t.EventDateID,
                                            GROUP_CONCAT(DISTINCT $userName ORDER BY $userName SEPARATOR ',') AS Instructor,
                                            GROUP_CONCAT(DISTINCT t.TrainerID SEPARATOR ',') AS InstructorID
                                     FROM
                                            INTRANET_ENROL_EVENT_DATE_TRAINER t
                                     INNER JOIN 
                                            INTRANET_USER u ON u.UserID=t.TrainerID
                                     GROUP BY t.EventDateID) AS t ON t.EventDateID=d.EventDateID
         		        LEFT JOIN
                                INVENTORY_LOCATION r ON r.LocationID=d.LocationID AND r.RecordStatus='1'
                        LEFT JOIN 	
                        	   INVENTORY_LOCATION_LEVEL f ON f.LocationLevelID=r.LocationLevelID AND f.RecordStatus='1'
                        LEFT JOIN 
                               INVENTORY_LOCATION_BUILDING b ON b.BuildingID=f.BuildingID AND b.RecordStatus='1'
        		        WHERE
                                    d.ActivityDateStart>='".$startDate." 00:00:00'
                        AND         d.ActivityDateEnd<='".$endDate." 23:59:59'
                        AND         (d.RecordStatus='1' OR d.RecordStatus IS NULL)
                        AND         m.IntakeID='".$intakeID."'
                        AND         dic.IntakeClassID='".$classID."'
                        ORDER BY d.ActivityDateStart";
		    
	        $detailAry =  $this->returnResultSet($sql);
	        
	        $lessonAry = array();
	        for($i=0,$iMax=count($detailAry);$i<$iMax;$i++) {
	            $_colAry = $detailAry[$i];
	            
	            $_eventDateID = $_colAry['EventDateID'];
	            $_activityDate = $_colAry['ActivityDate'];
	            
	            $lessonInfoAry = array();
	            $lessonInfoAry['Lesson'] = $_colAry['Lesson'];
	            $lessonInfoAry['ActivityDate'] = $_activityDate;
	            $lessonInfoAry['StartTime'] = $_colAry['StartTime'];
	            $lessonInfoAry['EndTime'] = $_colAry['EndTime'];
	            $lessonInfoAry['TimeSlot'] = $_colAry['TimeSlot'];
	            $lessonInfoAry['Duration'] = $_colAry['Duration'];
	            $lessonInfoAry['Instructor'] = $_colAry['Instructor'];
	            $lessonInfoAry['InstructorID'] = $_colAry['InstructorID'];
	            $lessonInfoAry['Location'] = $_colAry['Location'];
	            
	            if (!isset($lessonAry[$_activityDate][$_eventDateID])) {
	                $lessonAry[$_activityDate][$_eventDateID] = $lessonInfoAry;
	            }
	        }

	        return $lessonAry;
		    
		}
		
		
		function getMappingOfEventDateIDtoTimeslot($date, $scheduleInfoAry, $startTime, $timeRange, $timeslotInterval)
		{
		    $mappingIntervalSecond = 60;      // time interval = 1 min
		    $timeIntervalArray = array();
		    $numTimeslot = $timeRange / $mappingIntervalSecond;
		    
		    // ######################### Get All the Start to End timeslot (Start) ##########################
		    $_tempStartEndTimeslotAry = array();

		    $maxRow = 1;
		    $checkRowAry = array();
		    if (isset($scheduleInfoAry[$date])) {
		        $_oneDayScheduleAry = $scheduleInfoAry[$date];
		        
		        foreach((array)$_oneDayScheduleAry as $_eventDateID=>$_oneScheduleAry) {
		            
		            $_startTimeTimestamp = strtotime($_oneScheduleAry['StartTime']);
		            $_endTimeTimestamp = strtotime($_oneScheduleAry['EndTime']) - $mappingIntervalSecond; // end time minus 1 time
    	            $_duration = $_oneScheduleAry['Duration'] - $mappingIntervalSecond;
    	            
    	            $i = 0;
    	            while ($i < $maxRow) {
    	                $isTimeSlotSet = false;
    	                for ($j = $_startTimeTimestamp; $j < $_endTimeTimestamp; $j += $mappingIntervalSecond) {
    	                    if (!empty($_tempStartEndTimeslotAry[$j][$i])) {
    	                        $isTimeSlotSet = true;
    	                        break;
    	                    }
    	                }

                        if ($isTimeSlotSet == false) {
        	                
        	                $_tempStartEndTimeslotAry[$_startTimeTimestamp][$i]['EventDateID'] = $_eventDateID;
        	                $_tempStartEndTimeslotAry[$_startTimeTimestamp][$i]['TimeType'] = 'Start';
                            
                            for ($j = $mappingIntervalSecond; $j < $_duration; $j += $mappingIntervalSecond) {
                                $_middleTimeTimestamp = $_startTimeTimestamp + $j;
                                $_tempStartEndTimeslotAry[$_middleTimeTimestamp][$i]['EventDateID'] = $_eventDateID;
                                $_tempStartEndTimeslotAry[$_middleTimeTimestamp][$i]['TimeType'] = 'Start';
                            }
                            $_tempStartEndTimeslotAry[$_endTimeTimestamp][$i]['EventDateID'] = $_eventDateID;
                            $_tempStartEndTimeslotAry[$_endTimeTimestamp][$i]['TimeType'] = 'End';
                            
                            break;
                        }
                        else {
                            if (!in_array($i,$checkRowAry)) {
                                $maxRow++;
                            }
                            $checkRowAry[] = $i;
                            $i++;
                        }
    	            }
		        }
		    }
		    // ########################## Get All the Start to End timeslot (End) ###########################

		    // ########################## Get Empty Timeslot (Start) ###########################
		    $timeIntervalArray = array();

            $eventAry = array();
            
            $maxEndTime = date('H:i:s',strtotime($date.' '. $startTime) + $timeRange);
            
            for ($row=0; $row<$maxRow; $row++) {
                $eventCounter = 0;      // count number of EventDateID and empty timeslot in a row
                $prevEventDateID = 0;
                $_colSpan = 1;
                $emptyTimeSlotStarter = null;
                
    		    for ($i = 0; $i < $numTimeslot; $i++) {
    		        $_timestamp = strtotime($date.' '. $startTime) + $mappingIntervalSecond * $i;
    		        $isMajorStartTimeslot = $timeslotInterval ? ($mappingIntervalSecond * $i % $timeslotInterval == 0) : false;

    		        // ################ Set previous cell target end time (Start) #################
    		        if ($isMajorStartTimeslot) {
    		            if ($prevEventDateID == 0 && $i != 0) {
    		                $eventAry[$row][$eventCounter]['TargetEndTime'] = date('H:i:s',$_timestamp);
    		            }
    		        }
    		        else {
    		            if ($prevEventDateID == 0 && $_tempStartEndTimeslotAry[$_timestamp][$row]['EventDateID']) {
    		                $eventAry[$row][$eventCounter]['TargetEndTime'] = date('H:i:s',$_timestamp);
    		            }
    		        }
    		        // ################ Set previous cell target end time (End)   #################
    		        
    		        if (!empty($_tempStartEndTimeslotAry[$_timestamp][$row])) {

    		            if (!isset($eventAry[$row][$eventCounter])) {
    		                $_colSpan = 1;
    		                $eventAry[$row][$eventCounter]['EventDateID'] = $_tempStartEndTimeslotAry[$_timestamp][$row]['EventDateID'];
    		                $prevEventDateID = $_tempStartEndTimeslotAry[$_timestamp][$row]['EventDateID'];
    		                $reassignEndTime = true;
    		            }
    		            else if ($_tempStartEndTimeslotAry[$_timestamp][$row]['EventDateID'] != $prevEventDateID) {
    		                $eventCounter++;
    		                $_colSpan = 1;    		                
    		                $eventAry[$row][$eventCounter]['EventDateID'] = $_tempStartEndTimeslotAry[$_timestamp][$row]['EventDateID'];
    		                $prevEventDateID = $_tempStartEndTimeslotAry[$_timestamp][$row]['EventDateID'];
    		                $reassignEndTime = true;
    		            }
    		            else {
    		                $reassignEndTime = false;
    		            }
    		            
    		            $eventAry[$row][$eventCounter]['ColSpan'] = $_colSpan++;
    		            
    		            if ($reassignEndTime) {
        		            $prevEventCounter = $eventCounter - 1;
        		            if ($prevEventCounter >= 0 && $eventAry[$row][$prevEventCounter]['EventDateID'] == 0) {
        		                $_endTime = date('H:i:s',$_timestamp);
        		                for ($j = $emptyTimeSlotStarter; $j < $eventCounter; $j++) {
        		                    $eventAry[$row][$j]['MaxEndTime'] = $_endTime;
        		                }
        		            }
        		            $emptyTimeSlotStarter = null;
    		            }
    		            
    		        } else {
    		            
    		            $_endTime = date('H:i:s',$_timestamp);
    		            
    		            $initializeTimeSlot = false;
    		            if ($prevEventDateID > 0 || $isMajorStartTimeslot) {    // previous timeslot is non-empty 
    		                $eventCounter++;          // new counter
    		                $initializeTimeSlot = true;
    		            }
    		            
    		            if ($i == 0) {    // first timeslot is empty
    		                $initializeTimeSlot = true;
    		            }
    		            
                        if ($initializeTimeSlot) {
    		                $_colSpan = 1;
    		                $_startTime = $_endTime;
    		                $eventAry[$row][$eventCounter]['StartTime'] = $_startTime;
    		                $eventAry[$row][$eventCounter]['EventDateID'] = 0;
    		                if ($emptyTimeSlotStarter == null) {
    		                    $emptyTimeSlotStarter = $eventCounter;
    		                }
    		            }
    		            $prevEventDateID = 0;
    		            
		                $eventAry[$row][$eventCounter]['MaxEndTime'] = $maxEndTime;
		                $eventAry[$row][$eventCounter]['ColSpan'] = $_colSpan++;
    		            
    		        }
    		    }
            }
            $resultAssoc[$date] = $eventAry;

		    // ########################### Get Empty Timeslot (End) ############################

            /*
               [2018-09-10] => Array
                    (
                        [0] => Array
                            (
                                [0] => Array
                                    (
                                        [EventDateID] => 1994
                                        [ColSpan] => 90
                                    )
            
                                [1] => Array
                                    (
                                        [StartTime] => 08:00:00
                                        [EventDateID] => 0
                                        [MaxEndTime] => 08:24:00
                                        [ColSpan] => 24
                                        [TargetEndTime] => 08:24:00
                                    )
                            )
                    )
             */
            
		    //debug_pr($resultAssoc);
		    
		    return $resultAssoc;
		}
		
		function getClassWeeklyScheduleTable($intakeID, $classID, $startDate='', $fromPrint=0, $className='')
		{
		    global $Lang, $image_path, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		    include_once($PATH_WRT_ROOT."includes/libinterface.php");
		    
		    $isEnrolAdmin = $this->IS_ENROL_ADMIN($_SESSION['UserID']);
		    $isInstructor = $this->isInstructor();
		    
		    $linterface = new interface_html();
		    
		    if ($startDate == '') {
		        $startDate = $this->getMondayOfTheWeek(date('Y-m-d'),true);
		    }
		    
		    $scheduleAry = $this->getClassWeeklyScheduleData($intakeID, $classID, $startDate);
		    
		    if ($intakeID) {
		        $intakeAry = $this->getIntake($intakeID);
		        $intakeStartDate = $intakeAry[$intakeID]['StartDate'];
		        $intakeEndDate = $intakeAry[$intakeID]['EndDate'];
		    }
		    else {
		        $intakeStartDate = '';
		        $intakeEndDate = '';
		    }
		    
			// ################## Setting of the table (Start) ##################
		    include_once ($PATH_WRT_ROOT."includes/libgeneralsettings.php");
			$libgs = new libgeneralsettings();
			
			$settingAry['EnrolmentClassWeeklyScheduleStartTime'] = '';
			$settingAry['EnrolmentClassWeeklyScheduleEndTime'] = '';
			$settingAry['EnrolmentClassWeeklyScheduleTimeInterval'] = '';
			
			$settingAry = $libgs->Get_General_Setting('eEnrolment');
			
			$startTime = $settingAry['EnrolmentClassWeeklyScheduleStartTime'];
			$endTime = $settingAry['EnrolmentClassWeeklyScheduleEndTime'];
			$timeslotInterval = $settingAry['EnrolmentClassWeeklyScheduleTimeInterval'] * 60;        // in seconds

			// Default value
			$startTime = ($startTime == 0) ? '06:00:00' : $startTime;
			$endTime = ($endTime == 0) ? '18:00:00' : $endTime;
			$timeslotInterval = ($timeslotInterval == 0) ? 1800 : $timeslotInterval;
			
			$timeRange = strtotime($endTime) - strtotime($startTime);
			$numTimeslot = $timeRange / $timeslotInterval;
			$numTimeslotWithDate = $numTimeslot + 1;         // with date column
			$numSmallTimeslot = $timeslotInterval / 60;
            $numberTimeslotForDate = 30;
            $numberSmallestTimeslot = $numSmallTimeslot * $numTimeslot + $numberTimeslotForDate;
			
			// ################### Setting of the table (End) ###################
			
			// Build Timeslot Array
			$microTimeSlotAry = array();
			$displayTimeslotAry = array();
			for ($i = 0; $i < $numTimeslot; $i ++) {
			    $_newTime = strtotime($startTime) + $timeslotInterval * $i;
			    $_newTimeDisplay = date("H:i", $_newTime);
			    $displayTimeslotAry[] = $_newTimeDisplay;
			}
			
			// ##### Table Start ###### 
			$x .= '<table id="AjaxContentTable" border="0" cellpadding="0" cellspacing="0">';
			$x .= '<thead>';
			
			$x .= '<tr>';
		    for ($i=0; $i<$numberSmallestTimeslot;$i++) {
		        $x .= '<th style="width:1px; max-width:2px;">&nbsp;</th>';
		    }
			$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
			$x .= '<tr>';
			$x .= '<td class="tabletop" align="center" style="border-right: 1px solid #FFFFFF;" colspan="' . $numberTimeslotForDate. '">';
			$x .= '<span class="table_row_tool row_content_tool" style="float:right">' . "\n";
			if ($className == '') {
    			if ($fromPrint == 0) {
    			    $x .= $linterface->Get_Thickbox_Link(200, 300, "setting_row", $Lang['General']['Settings'], "", "divThickBox", "", "thickboxButton");
    			}
			}
			else {
			    $x .= $className;
			}
			$x .= '</span>' . "\n";
			$x .= '</td>';
			foreach ($displayTimeslotAry as $timeslotName) {
			    $x .= '<td colspan="' . $numSmallTimeslot. '" class="tabletop" style="border-right: 1px solid #FFFFFF;">' . $timeslotName . '</td>';
			}
			$x .= '</tr>';
			
			$_startTimeStamp = strtotime($startDate);
			
			// loop weekday array
			for ($i=0; $i<7; $i++) {			    
			    $_thisDateTimeStamp = strtotime('+'.$i.' day',$_startTimeStamp);
			    $_thisDate = date('Y-m-d',$_thisDateTimeStamp);
			    $_weekDay = date('w',$_thisDateTimeStamp);
			    $_disWeekDay = ($_weekDay>=0 && $_weekDay <=6) ? $Lang['eEnrolment']['report']['weekday'][$_weekDay] : '';
			    
			    $_mappingAssoAry = $this->getMappingOfEventDateIDtoTimeslot($_thisDate, $scheduleAry, $startTime, $timeRange, $timeslotInterval);
			    $_mappingOfEventDateIDtoTimeslot = $_mappingAssoAry[$_thisDate];
			    $_numOfRow = count($_mappingOfEventDateIDtoTimeslot);
                unset($_mappingAssoAry);

                for ($__row = 0; $__row < $_numOfRow; $__row++) {
                    $x .= '<tr>';
                    if ($__row == 0) {
                        $x .= '<td rowspan="'.$_numOfRow.'" valign="top" align="center" valign="top" class="eventDate" colspan="' . $numberTimeslotForDate. '">' . $_disWeekDay . '<br>'. $_thisDate. '</td>';
                    }
                    
                    $__rowTimeslot = $_mappingOfEventDateIDtoTimeslot[$__row];
                    
                    foreach ((array)$__rowTimeslot as $___index => $___timeslotAry) {
                        
                        $___eventDateID = $___timeslotAry['EventDateID'];
                        if ($___eventDateID) {

                            $___borderClass = ($__row == 0) ? 'lessonBorder_normal' : 'lessonBorder_conflict';
                            $x .= '<td cellpadding="0" cellspacing="0" colspan="' . $___timeslotAry['ColSpan']. '" class="eventDateItem ' . $___borderClass. '" id="EventDateID_' . $___eventDateID . '">';
                            
                             
                            $___scheduleAry = $scheduleAry[$_thisDate][$___eventDateID];
                            
                            $isAllowEdit = false;
                            if ($isEnrolAdmin) {
                                $isAllowEdit = true;
                            }
                            else if ($isInstructor) {
                                if ($___scheduleAry['InstructorID']) {
                                    $___instructorAry = explode(",",$___scheduleAry['InstructorID']);
                                    if (in_array($_SESSION['UserID'], (array)$___instructorAry)) {
                                        $isAllowEdit = true;
                                    }
                                }
                            }
                            
                            if ($isAllowEdit && !$fromPrint) {
                                $___lessonInfo = '<a href="#TB_inline?height=400&width=600&inlineId=FakeLayer" onclick="editLesson(\''.$___eventDateID.'\', \''.$classID.'\'); return false;"  title="' . $Lang['eEnrolment']['course']['editLesson']. '">' . $___scheduleAry['Lesson'];
                            }
                            else {
                                $___lessonInfo = $___scheduleAry['Lesson'];
                            }
                            
                            $___lessonInfo .= "<br>".$___scheduleAry['TimeSlot'];
                            if ($___scheduleAry['Instructor']) {
                                $___lessonInfo .= "<br>".$Lang['eEnrolment']['report']['staff'].": ".$___scheduleAry['Instructor'];
                            }
                            if ($___scheduleAry['Location']) {
                                $___lessonInfo .= "<br>".$Lang['eEnrolment']['report']['location'].": ".$___scheduleAry['Location'];
                            }
                            $x .= $___lessonInfo;
                            $x .= ($isAllowEdit && !$fromPrint) ? "</a>" : '';
                            $x .= '</td>';
                            
                        }
                        else {  // empty cell, allow to add new lesson
                            $x .= '<td class="empty" colspan="'.$___timeslotAry['ColSpan'].'">';
                            if ($__row == 0 && $fromPrint == 0 && $_thisDate >= $intakeStartDate && $_thisDate <= $intakeEndDate) {
                                $x .= '<a href="#TB_inline?height=400&width=600&inlineId=FakeLayer" onclick="addLesson(\''.$classID.'\', \''.$_thisDate.'\', \'' . $___timeslotAry['StartTime']. '\', \'' . $___timeslotAry['TargetEndTime']. '\', \'' . $___timeslotAry['MaxEndTime']. '\'); return false;" title="' . $Lang['eEnrolment']['course']['addLesson']. '">' . "\n";
                                $x .= '<div class="addLesson" onmouseover="Show_Edit_Background(this);" onmouseout="Hide_Edit_Background(this);">&nbsp;</div></a>' . "\n";
                            }
                            $x .= '</td>';
                        }
                        
                    }   // end loop for each timeslot
                    
                }       // end loop for each row
                
                $x .= '</tr>';
			}    // end loop of a week
			
			$x .= '</tbody>';
			
            $x .= '</table>';
		    // ##### Table End ###### 
		    
		    // ThickBoxContent
            $x .= $this->getTimeslotSettingLayout($startTime, $endTime, $settingAry['EnrolmentClassWeeklyScheduleTimeInterval']);
		
			return $x;
		}
		
		
		function getTimeslotSettingLayout($startTime, $endTime, $timeslotInterval) 
		{
		    global $Lang, $PATH_WRT_ROOT;
		    include_once($PATH_WRT_ROOT."includes/libinterface.php");
		    
		    $linterface = new interface_html();

		    $explodeSTime = explode(':', $startTime);
		    $explodeETime = explode(':', $endTime);
		    $timeIntervalAry[15] = 15;
		    $timeIntervalAry[30] = 30;
		    $timeIntervalAry[60] = 60;
		    $x  = '<div id="divThickBox" style="display:none">';
		    $x .= '<form>';
		    $x .= '<br>';
		    $x .= '<table width="90%" class="form_table_v30">';
    		    $x .= '<tr>';
        		    $x .= '<td class="field_title" width="50%">' . $Lang['SysMgr']['Timetable']['StartTime'] . '</td>';
        		    $x .= '<td width="50%">' . getTimeSel("Start", $explodeSTime[0], $explodeSTime[1], $OnChange="", $minuteStep=1) . '</td>';
    		    $x .= '</tr>';
    		    $x .= '<tr>';
        		    $x .= '<td class="field_title">' . $Lang['SysMgr']['Timetable']['EndTime'] . '</td>';
        		    $x .= '<td>' . getTimeSel("End", $explodeETime[0], $explodeETime[1], $OnChange="", $minuteStep=1) . '</td>';
    		    $x .= '</tr>';
    		    $x .= '<tr>';
        		    $x .= '<td class="field_title">' . $Lang['eBooking']['Management']['DayView']['Settings']['TimeInterval'] . '</td>';
        		    $x .= '<td>' . getSelectByAssoArray($timeIntervalAry, 'id="timeInterval"', $timeslotInterval, 0, 1) . '</td>';
    		    $x .= '</tr>';
		    $x .= '</table>';
		    $x .= '<br/>';
		    $x .= '<div align="center">';
		    $x .= $linterface->GET_ACTION_BTN($Lang['Button']['SaveApply'], "button", "checkTime();", "SaveSetting");
		    $x .= "&nbsp;&nbsp;";
		    $x .= '</div>';
		    $x .= '</form></div>';
		 
		    return $x;
		}
		
	} // End of class
} // end of define
?>