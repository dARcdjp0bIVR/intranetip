<?php
# using: 

####################################
#
#   Date:   2019-05-13 Cameron
#           fix potential sql injection problem by enclosing var with apostrophe
#
#   Date:   2019-03-29  Isaac
#           change split to explode in manipulateFile() to support php5.4+
#
#	Date:	2011-02-25	YatWoon
#			update getSelectCats(), getSelectItems(), add ording in query

####################################

$rb_image_path = "$image_path/resourcesbooking";
class libresource extends libdb{

     var $Resource;
     var $ResourceID;
     var $ResourceCode;
     var $ResourceCategory;
     var $Title;
     var $Description;
     var $Remark;
     var $Attachment;
     var $RecordType;
     var $RecordStatus;
     var $DateInput;
     var $DateModified;
     var $DaysBefore;
     var $TimeSlotBatchID;
     var $ItemsRelated;


     ######################################################################################

     function libresource($ResourceID=""){
          $ResourceID += 0;
          $this->libdb();
          if($ResourceID<>""){
               $this->Resource = $this->returnResource($ResourceID);
               $this->ResourceID = $this->Resource[0][0];
               $this->ResourceCode = $this->Resource[0][1];
               $this->ResourceCategory = $this->Resource[0][2];
               $this->Title = $this->Resource[0][3];
               $this->Description = $this->Resource[0][4];
               $this->Remark = $this->Resource[0][5];
               $this->Attachment = $this->Resource[0][6];
               $this->RecordType = $this->Resource[0][7];
               $this->RecordStatus = $this->Resource[0][8];
               $this->DateInput = $this->Resource[0][9];
               $this->DateModified = $this->Resource[0][10];
               $this->DaysBefore = $this->Resource[0][11];
               $this->TimeSlotBatchID = $this->Resource[0][12];
          }
     }

     function returnResource($ResourceID){
          $sql = "SELECT ResourceID, ResourceCode, ResourceCategory, Title, Description, Remark, Attachment, RecordType, RecordStatus, DateInput, DateModified, DaysBefore, TimeSlotBatchID FROM INTRANET_RESOURCE WHERE ResourceID = '$ResourceID'";
          return $this->returnArray($sql,13);
     }

     function returnResourceCategory(){
          $sql = "SELECT ResourceCategory FROM INTRANET_RESOURCE GROUP BY ResourceCategory ORDER BY ResourceCategory";
          return $this->returnArray($sql,1);
     }

     ######################################################################################

     function displayResourceCategory($title){
          global $button_select;
          $row = $this->returnResourceCategory();
          if(sizeof($row)<>0){
               $x .= "<select onChange='this.form.$title.value=this.value;this.options[0].selected=true;'>\n";
               $x .= "<option value=\"\">- $button_select -</option>\n";
               for($i=0;$i<sizeof($row); $i++)
               $x .= "<option value=\"".$row[$i][0]."\">".$row[$i][0]."</option>\n";
               $x .= "</select>\n";
          }
          return $x;
     }

     function display(){
          global $i_ResourceCode, $i_ResourceCategory, $i_ResourceTitle, $i_ResourceDescription, $i_ResourceRemark, $i_ResourceAttachment, $i_ResourceDateModified;
          $x .= "<table width=100% border=0 cellpadding=2 cellspacing=1>\n";
          $x .= "<tr><td colspan=2><b>".$this->ResourceCode." - ".$this->Title."</b></td></tr>\n";
          $x .= "<tr><td colspan=2><img src=/images/frontpage/myinfo_yellowbar.gif border=0 width=350 height=2 vspace=5></td></tr>\n";
          $x .= "<tr><td width=30%>$i_ResourceCategory:</td><td>".$this->ResourceCategory."</td></tr>\n";
          $x .= ($this->Description=="") ? "" : "<tr><td>$i_ResourceDescription:</td><td>".nl2br($this->convertAllLinks2($this->Description))."</td></tr>\n";
          $x .= ($this->Remark=="") ? "" : "<tr><td>$i_ResourceRemark:</td><td>".nl2br($this->convertAllLinks2($this->Remark))."</td></tr>\n";
          $x .= ($this->Attachment=="") ? "" : "<tr><td>$i_ResourceAttachment:</td><td>".nl2br($this->manipulateFile($this->Attachment))."</td></tr>\n";
          $x .= "<tr><td colspan=2><img src=/images/frontpage/myinfo_yellowbar.gif border=0 width=350 height=2 vspace=5></td></tr>\n";
          $x .= "</table>\n";
          return $x;
     }

     ######################################################################################

     function manipulateFile($str){
         $files = explode(":", stripslashes($str));
          for($i=0; $i<count($files); $i++){
               $x .= $this->processAttachmentString(trim($files[$i]));
          }
          return $x;
     }

     function processAttachmentString($filename){
          global $intranet_httppath;
          $path = "/file/resource/";
          $url = $path.str_replace(" ","%20",$filename);
          $ext = substr($filename,strrpos($filename,"."));
//          $x .= "<a href=javascript:fs_view(\"$url\") onMouseOver=\"window.status='$filename';return true;\" onMouseOut=\"window.status='';return true;\">$filename</a>\n";
          $x .= "<a href=\"$intranet_httppath$url\" target=_blank onMouseOver=\"window.status='$filename';return true;\" onMouseOut=\"window.status='';return true;\">$filename</a>\n";
          return $x;
     }

     ######################################################################################

/*
     function returnBookingPeriod(){
          $sql = "SELECT BookingPeriodID, TimeStart, TimeEnd, Title FROM INTRANET_BOOKING_PERIOD ORDER BY TimeStart";
          return $this->returnArray($sql,4);
     }

     function returnBookablePeriod($ts, $uid=0){
          $sql = "SELECT (HOUR(DateStart)*3600+MINUTE(DateStart)*60) FROM INTRANET_BOOKING WHERE ( RecordStatus IN (0,2,3) OR (RecordStatus=4 AND UserID=$uid) ) AND UNIX_TIMESTAMP(DATE_FORMAT(DateStart,'%Y-%m-%d')) = $ts AND ResourceID = ".$this->ResourceID;
          $sql = "SELECT BookingPeriodID, TimeStart, TimeEnd, Title FROM INTRANET_BOOKING_PERIOD WHERE TimeStart NOT IN (".$this->db_sub_select($sql).") ORDER BY TimeStart";
          return $this->returnArray($sql,4);
     }

     function checkBookingPeriod($TS, $TE){
          $sql = "SELECT BookingPeriodID FROM INTRANET_BOOKING_PERIOD WHERE (TimeStart BETWEEN $TS AND ".($TE-1).") OR (TimeEnd BETWEEN ".($TS+1)." AND $TE)";
          return sizeof($this->returnArray($sql,1));
     }

     function updateBookingPeriod($BookingPeriodID, $Title){
          $sql = "UPDATE INTRANET_BOOKING_PERIOD SET Title = '$Title' WHERE BookingPeriodID = $BookingPeriodID";
          $this->db_db_query($sql);
     }

     function insertBookingPeriod($TimeStart, $TimeEnd, $Title){
          if($this->checkBookingPeriod($TimeStart, $TimeEnd)==0){
               $sql = "INSERT INTO INTRANET_BOOKING_PERIOD (TimeStart, TimeEnd, Title) VALUES ($TimeStart, $TimeEnd, '$Title')";
               $this->db_db_query($sql);
               return 1;
          }
     }

     function deleteBookingPeriod($BookingPeriodID){
          $sql = "SELECT TimeStart FROM INTRANET_BOOKING_PERIOD WHERE BookingPeriodID = $BookingPeriodID";
          $row = $this->returnArray($sql,1);
          $TimeStart = $row[0][0];
          $sql = "DELETE FROM INTRANET_BOOKING WHERE (HOUR(DateStart)*3600+MINUTE(DateStart)*60) = $TimeStart";
          $this->db_db_query($sql);
          $sql = "DELETE FROM INTRANET_BOOKING_PERIOD WHERE BookingPeriodID = $BookingPeriodID";
          $this->db_db_query($sql);
     }

     function displayBookingPeriod(){
          global $button_select;
          $row = $this->returnBookingPeriod();
          $x .= "<select name=BookingPeriodID>\n";
          $x .= "<option value=''>- $button_select -</option>\n";
          for($i=0; $i<sizeof($row); $i++){
               $BookingPeriodID = $row[$i][0];
               $TimeStart = date("H:i", mktime(0, 0, $row[$i][1], date("m"), date("d"), date("Y")));
               $TimeEnd  = date("H:i", mktime(0, 0, $row[$i][2], date("m"), date("d"), date("Y")));
               $Title = $row[$i][3];
               # $x .= "<option value=$BookingPeriodID>$TimeStart - $TimeEnd $Title</option>\n";
               $x .= "<option value=$BookingPeriodID>$Title</option>\n";
          }
          $x .= "</select>\n";
          return $x;
     }

     function displayBookingOption(){
          for($i=0; $i<288; $i++){
               $p = 300*$i;
               $q = date("H:i", mktime(0, 0, $p, date("m"), date("d"), date("Y")));
               $x .= "<option value=$p>$q</option>\n";
          }
          return $x;
     }

     function displayBookingCheck($ts, $uid=0){
          $row = $this->returnBookablePeriod($ts,$uid);
          if(sizeof($row)==0){
               $x .= "-";
          }else{
               for($i=0; $i<sizeof($row); $i++){
                    $BookingPeriodID = $row[$i][0];
                    $TimeStart = $row[$i][1];
                    $TimeEnd = $row[$i][2];
                    $TS = date("H:i", mktime(0, 0, $TimeStart, date("m"), date("d"), date("Y")));
                    $TE = date("H:i", mktime(0, 0, $TimeEnd, date("m"), date("d"), date("Y")));
                    $Title = $row[$i][3];
                    $x .= "<input type=checkbox name=BookingPeriod[] value=$TimeStart:$TimeEnd> $TS - $TE $Title<br>\n";
               }
          }
          return $x;
     }
*/
     ######################################################################################

     # Intranet 1.2

     function displayItem ()
     {
              global $i_ResourceCode, $i_ResourceCategory, $i_ResourceTitle, $i_ResourceDescription, $i_ResourceRemark, $i_ResourceAttachment, $i_ResourceDateModified;
              global $rb_image_path,$i_BookingItem;
              $cat = intranet_wordwrap($this->ResourceCategory,50," ",1);
              $item = intranet_wordwrap($this->Title,50," ",1);
              $code = intranet_wordwrap($this->ResourceCode,50," ",1);
              $x = "<table width=729 border=0 cellspacing=0 cellpadding=2 background='$rb_image_path/framebglightblue.gif' class=body>
                     <tr><td width=150 align=right valign=top>$i_ResourceCategory :</td>
                     <td width=529 align=left valign=top>$cat</td><td width=50>&nbsp;</td></tr>
                     <tr><td>&nbsp;</td><td>&nbsp;</td><td width=20>&nbsp;</td></tr>
                     <tr><td align=right>$i_BookingItem :</td>
                     <td align=left> $item ($code)</td><td width=20>&nbsp;</td></tr>
                     <tr><td>&nbsp;</td><td>&nbsp; </td><td width=20>&nbsp;</td></tr>";
          $x .= ($this->Description=="") ? "" : "<tr><td align=right>$i_ResourceDescription :</td><td>".nl2br($this->convertAllLinks2($this->Description,50))."</td><td width=20>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp; </td><td width=20>&nbsp;</td></tr>\n";
          $x .= ($this->Remark=="") ? "" : "<tr><td align=right>$i_ResourceRemark:</td><td>".nl2br($this->convertAllLinks2($this->Remark,50))."</td><td width=20>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp; </td><td width=20>&nbsp;</td></tr>\n";
          $x .= ($this->Attachment=="") ? "" : "<tr><td align=right>$i_ResourceAttachment:</td><td>".nl2br($this->manipulateFile($this->Attachment))."</td><td width=20>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp; </td><td width=20>&nbsp;</td></tr>\n";
              $x .= "</table>";
              return $x;
     }

     function getSelectItems($attrb, $matched="", $cat="", $bid="", $included=true)
     {
              global $i_ResourceSelectAllItem;
              $conds = "";
              if ($bid != "")
              {
                  $conds .= " TimeSlotBatchID". ($included?"=":"!=")."'$bid'";
              }
              if ($cat != "")
              {
                  if ($conds != "") $conds .= " AND ";
                  $conds .= " ResourceCategory = '$cat'";

              }
              if ($conds .= "") $conds = " WHERE $conds";
              $sql = "SELECT ResourceID, Title FROM INTRANET_RESOURCE $conds";
              $sql .= " order by Title";
              $result = $this->returnArray($sql,2);
              $x = "<SELECT $attrb>";
              $x .= "<OPTION value=''> -- $i_ResourceSelectAllItem -- </OPTION>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   list($id, $title) = $result[$i];
                   $checked = ($matched == $id?  "SELECTED":"");
                   $x .= "<OPTION value=$id $checked> $title</option>\n";
              }
              $x .= "</SELECT>\n";
              return $x;
     }

     function getSelectCats($attrb, $matched="", $bid="", $included=true)
     {
              global $i_ResourceSelectAllCat;
              if ($bid != "")
              {
                  $conds = "WHERE TimeSlotBatchID". ($included?"=":"!=")."'$bid'";
              }
              else $conds = "";
              $sql = "SELECT DISTINCT ResourceCategory FROM INTRANET_RESOURCE $conds";
              $sql .= " order by ResourceCategory";
              $result = $this->returnVector($sql);
              $x = "<SELECT $attrb>";
              $x .= "<OPTION value=''> -- $i_ResourceSelectAllCat -- </OPTION>\n";
              for ($i=0; $i<sizeof($result); $i++)
              {
                   $title = $result[$i];
                   $checked = ($matched == $title?  "SELECTED":"");
                   $x .= "<OPTION value='$title' $checked> $title</option>\n";
              }
              $x .= "</SELECT>\n";
              return $x;
     }

}
?>