<?php
/*
 * Modifying by 
 * 2019-08-23 (Ray)
 * - added "plupload_process.php" in exclude
 * 2016-06-16 (Thomas) [ip.2.5.8.4.1]
 * 	- added "elearningapi" and "webserviceapi" in the exclude array
 * 2016-06-16 (Ivan) [ip.2.5.7.7.1]
 * 	- added "eclassappapi" in the exclude array
 * 2016-06-15 (Paul) [ip.2.5.7.7.1]
 * 	- created this page
 * 
 */
class abuse{
	var $user = NULL;
	
	var $seconds = '60';
	
	var $refresh = '50';
	
	function abuse($second='60', $refresh='50', $user=NULL){
		$this->user = $user;
		$this->seconds = $second;
		$this->refresh = $refresh;
		$this->checkAbuse();
		$this->removeDummySession();
	}
	
	function checkAbuse(){
		global $intranet_root, $Lang;
		session_start();
		//setup the checking
		$cur_time = time();
		$cur_uri = $_SERVER["REQUEST_URI"];
		$doChecking = false;
		$notInExclude = true;
		$wordOfPageToExclude = array('stool', 'long_polling', 'record_abuse_ajax', 'eclassappapi', 'elearningapi', 'webserviceapi', '.css' ,'aj', 'portfolio/teacher/management/others/upload.php','plupload_process.php');
		foreach($wordOfPageToExclude as $page){
			if((strpos($cur_uri, $page) !== false)){
				$notInExclude = false;
			}
		}
		
		if($notInExclude){
			$_SESSION['refresh_checking'][$cur_uri]['refresh_times'] = isset($_SESSION['refresh_checking'][$cur_uri]['refresh_times'] )?$_SESSION['refresh_checking'][$cur_uri]['refresh_times'] :0;
			if(isset($_SESSION['refresh_checking'][$cur_uri]['last_time'])){
			    $_SESSION['refresh_checking'][$cur_uri]['refresh_times']  += 1;
			}else{
			    $_SESSION['refresh_checking'][$cur_uri]['refresh_times']  = 1;
			    $_SESSION['refresh_checking'][$cur_uri]['last_time'] = $cur_time;
			}
			// pop out alert and record into log
			if($cur_time - $_SESSION['refresh_checking'][$cur_uri]['last_time'] < $this->seconds){
			    if($_SESSION['refresh_checking'][$cur_uri]['refresh_times'] >= $this->refresh){
			    	include_once($intranet_root."/includes/libdb.php");
			        $db = new libdb();
			        intranet_opendb();
			        $module = explode("/",$cur_uri);
			        $attempt = ($_SESSION['refresh_checking'][$cur_uri]['refresh_times'] > $this->refresh)?1:$_SESSION['refresh_checking'][$cur_uri]['refresh_times'];
			        $sql = "INSERT INTO INTRANET_ABUSE_LOG (LoginSessionID, UserID, Module, CurrentPath, AttemptNo, InputDate) VALUES ('".$_SESSION['iSession_LoginSessionID']."', '".$this->user."', '".$module[count($module)-2]."', '".$cur_uri."', '".$attempt."', NOW())";
			        $log[] = $db->db_db_query($sql);
			        intranet_closedb();
			        //$_SESSION['refresh_checking'][$cur_uri]['refresh_times'] = 0;
			        //$_SESSION['refresh_checking'][$cur_uri]['last_time'] = $cur_time;
			        $redirectURL = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			        //die("RefreshOverload");
			        if($_SESSION['intranet_session_language']  == "en"){
			        	$Lang['ErrorMsg']["RefreshOverload"] = "The Malicious Refresh action is being detected. Please do not refresh the same page again.";
			        }else{
			        	$Lang['ErrorMsg']['RefreshOverload'] = "惡意刷新行為已被記錄。請不要再次刷新同一頁面。";
			        }
			        
			        echo "<script language='javascript'>
			        	alert('". $Lang['ErrorMsg']['RefreshOverload'] ."');		        
			        </script>";
			        die();
			    }
			}else{
			    $_SESSION['refresh_checking'][$cur_uri]['refresh_times'] = 0;
			    $_SESSION['refresh_checking'][$cur_uri]['last_time'] = $cur_time;
			}	
		}
	}
	
	function removeDummySession(){
		session_start();
		$cur_time = time();
		foreach((array) $_SESSION['time_checking'] as $uri=>$item){
			if($cur_time - $_SESSION['time_checking'][$uri]['last_time'] > 3600){
				unset($_SESSION['time_checking'][$uri]);
			}
		}
	}
} 

?>