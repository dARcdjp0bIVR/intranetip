<?php
#  Editing by 
/* ***************************************************
 * Modification Log
 *  20190502 Bill
 *  - modified saveSpecificMarksheetSubmissionSubjectBased(), prevent SQL Injection
 */
if (!defined("LIBREPORTCARD_SCHEDULE"))         // Preprocessor directives
{
	define("LIBREPORTCARD_SCHEDULE", true);
	
	class libreportcard_schedule extends libreportcard {
		
		function libreportcard_schedule() {
			$this->libreportcard();
		}
		
		function getSpecificMarksheetSubmissionPeriodData($ReportIdAry, $TeacherIdAry='', $UnixTs=true, $returnAppliedReportOnly=false, $parSubjectIdAry='', $parSubjectGroupIdAry='') {
			if ($TeacherIdAry != '') {
				$condsUserId = " And msusp.UserID In ('".implode("','", (array)$TeacherIdAry)."') ";
			}
			
			if ($returnAppliedReportOnly) {
				$condsAppliedSpecialPeriodReportOnly = " And rt.ApplySpecificMarksheetSubmission = 1 ";
			}
			
			if ($UnixTs) {
				$subStartField = "If (UNIX_TIMESTAMP(msusp.MarksheetSubmissionStart) = 0, '', UNIX_TIMESTAMP(msusp.MarksheetSubmissionStart))";
				$subEndField = "If (UNIX_TIMESTAMP(msusp.MarksheetSubmissionEnd) = 0, '', UNIX_TIMESTAMP(msusp.MarksheetSubmissionEnd))";
			} else {
				$subStartField = "If (msusp.MarksheetSubmissionStart = '0000-00-00 00:00:00', '', msusp.MarksheetSubmissionStart)";
				$subEndField = "If (msusp.MarksheetSubmissionEnd = '0000-00-00 00:00:00', '', msusp.MarksheetSubmissionEnd)";
			}
			
			if ($parSubjectIdAry !== '') {
				$condsSubjectId = " AND msusp.SubjectID IN ('".implode("','", (array)$parSubjectIdAry)."') ";
			}
			
			if ($parSubjectGroupIdAry !== '') {
				$condsSubjectGroupId = " AND msusp.SubjectGroupID IN ('".implode("','", (array)$parSubjectGroupIdAry)."') ";
			}
			
			$RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD = $this->DBName.".RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD";
			$RC_REPORT_TEMPLATE = $this->DBName.".RC_REPORT_TEMPLATE";
			$sql = "Select
							msusp.RecordID, msusp.ReportID, msusp.UserID, msusp.SubjectID, $subStartField as SubStart, $subEndField as SubEnd,
							IF (msusp.SubjectGroupID is null Or msusp.SubjectGroupID = '', 0, msusp.SubjectGroupID) as SubjectGroupID
					From
							$RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD as msusp
							Inner Join $RC_REPORT_TEMPLATE as rt On (msusp.ReportID = rt.ReportID)
					Where
							msusp.ReportID In ('".implode("','", (array)$ReportIdAry)."')
							$condsUserId
							$condsAppliedSpecialPeriodReportOnly
							$condsSubjectId
							$condsSubjectGroupId
					";
			return $this->returnResultSet($sql);
		}
		
		function saveSpecificMarksheetSubmission($ReportId, $StartDateTime, $EndDateTime, $TargetTeacherIdAry) {
			$RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD = $this->DBName.".RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD";
			$successAry = array();
			
			$originalDataAry = $this->getSpecificMarksheetSubmissionPeriodData($ReportId);
			$orignalTeacherIdAry = Get_Array_By_Key($originalDataAry, 'UserID');
			$originalDataAssoAry = BuildMultiKeyAssoc($originalDataAry, 'UserID');
			
			// delete record for the removed teachers
			$deletedUserId = array_values(array_diff((array)$orignalTeacherIdAry, (array)$TargetTeacherIdAry));
			$sql = "Delete From $RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD Where ReportID = '".$ReportId."' And UserID In ('".implode("','", (array)$deletedUserId)."')";
			$successAry['deleteRemovedData'] = $this->db_db_query($sql);
			
			// insert or update records for the selected teachers
			$insertAry = array();
			$numOfTargetTeacher = count((array)$TargetTeacherIdAry);
			for ($i=0; $i<$numOfTargetTeacher; $i++) {
				$_teacherId = $TargetTeacherIdAry[$i];
				$_recordId = $originalDataAssoAry[$_teacherId]['RecordID'];
				
				if ($_recordId == '') {
					// insert
					$insertAry[] = "('".$ReportId."', '".$_teacherId."', ".$StartDateTime.", ".$EndDateTime.", now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')";
				}
				else {
					// update
					$sql = "Update $RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD 
							Set 
									MarksheetSubmissionStart = $StartDateTime,
									MarksheetSubmissionEnd = $EndDateTime,
									DateModified = now(),
									ModifiedBy = '".$_SESSION['UserID']."'
							Where RecordID = '".$_recordId."'";
					$successAry['updateData'][$_recordId] = $this->db_db_query($sql);
				}
			}
			
			if (count($insertAry) > 0) {
				$sql = "Insert Into $RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD
								(ReportID, UserID, MarksheetSubmissionStart, MarksheetSubmissionEnd, DateInput, InputBy, DateModified, ModifiedBy)
						Values
								".implode(',', (array)$insertAry)."
						";
				$successAry['insertData'] = $this->db_db_query($sql);
			}
			
			return !in_array(false, (array)$successAry);
		}
		
		function saveSpecificMarksheetSubmissionSubjectBased($parReportId, $parStartDateTime, $parEndDateTime, $parTeacherAssoAry)
		{
			$RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD = $this->DBName.".RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD";
			$successAry = array();
			
			// delete record first
			$sql = "Delete From $RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD Where ReportID = '".$parReportId."'";
			$successAry['deleteRemovedData'] = $this->db_db_query($sql);
			
			// insert or update records for the selected teachers
			$insertAry = array();
			foreach ((array)$parTeacherAssoAry as $_teacherId => $_subjectIdAry) {
			    $_teacherId = IntegerSafe($_teacherId);
			    
				$_numOfSubject = count($_subjectIdAry);
				for ($i=0; $i<$_numOfSubject; $i++) {
					$__subjectInfoText = $_subjectIdAry[$i];
					$__subjectInfoAry = explode('_', $__subjectInfoText);
					
					$__subjectId = $__subjectInfoAry[0];
					$__subjectId = IntegerSafe($__subjectId);
					$__subjectGroupId = ($__subjectInfoAry[1]=='')? 0 : $__subjectInfoAry[1];
					$__subjectGroupId = IntegerSafe($__subjectGroupId);
					
					$insertAry[] = " ('".$parReportId."', '".$_teacherId."', '".$__subjectId."', '".$__subjectGroupId."', ".$parStartDateTime.", ".$parEndDateTime.", now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
				}
			}
			
			if (count($insertAry) > 0) {
				$sql = "INSERT INTO $RC_MARKSHEET_SPECIFIC_USER_SUBMISSION_PERIOD
								(ReportID, UserID, SubjectID, SubjectGroupID, MarksheetSubmissionStart, MarksheetSubmissionEnd, DateInput, InputBy, DateModified, ModifiedBy)
						VALUES
								".implode(',', (array)$insertAry)." ";
				$successAry['insertData'] = $this->db_db_query($sql);
			}
			
			return !in_array(false, (array)$successAry);
		}
		
		function getTeacherSpecialMarksheetSubmissionPeriodDisplay($ReportId, $TeacherId) {
			global $eReportCard, $linterface;
			
			$specificSubmissionDisplay = '';
			$specificSubmissionAry = $this->getSpecificMarksheetSubmissionPeriodData($ReportId, $TeacherId, $UnixTs=false, $returnAppliedReportOnly=true);
			if (count($specificSubmissionAry) > 0) {
				$specificSubmissionDisplay .= '<br />';
				$specificSubmissionDisplay .= $linterface->OptionalSymbol().'<b>'.$eReportCard['SubmissionStartDate'].':</b> '.$specificSubmissionAry[0]['SubStart'].' &nbsp;&nbsp; <b>'.$eReportCard['SubmissionEndDate'].':</b> '.$specificSubmissionAry[0]['SubEnd'];
			}
			
			return $specificSubmissionDisplay;
		}
		
		function getTeacherSpecialMarksheetSubmissionPeriodRemarks() {
			global $eReportCard, $linterface;
			return '<span class="tabletextremark">'.$linterface->OptionalSymbol().' '.$eReportCard['ManagementArr']['SchdeuleArr']['SpecificSubmissionPeriod'].'</span>';
		}
		
		function compareSpecialMarksheetSubmissionPeriodForTeacher($ReportId, $TeacherId, $parSubjectId='', $parSubjectGroupId='') {
			global $eRCTemplateSetting;
			
			$reportInfoAry = $this->returnReportTemplateBasicInfo($ReportId);
			
			if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectBased']) {
				$targetSubjectId = $parSubjectId;
			}
			else {
				$targetSubjectId = 0; 
			}
			if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectGroupBased']) {
				$targetSubjectGroupId = $parSubjectGroupId;
			}
			else {
				$targetSubjectGroupId = 0; 
			}
			$specificSubmissionAry = $this->getSpecificMarksheetSubmissionPeriodData($ReportId, $TeacherId, $UnixTs=true, $returnAppliedReportOnly=true, $targetSubjectId, $targetSubjectGroupId);
			
			if ($reportInfoAry['ApplySpecificMarksheetSubmission']==0 || count($specificSubmissionAry) == 0) {
				return -1;
			}
			else {
				return $this->COMPARE_REPORT_PERIOD($ReportId, '', $specificSubmissionAry[0]['SubStart'], $specificSubmissionAry[0]['SubEnd']);
			}
		}
		
	} // end of class libreportcard_schedule
} // end of Preprocessor directives
?>