<?php
class libemail
{
 var $email_regular_expression='/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i';
 var $mailer="";
 //var $default_charset="ISO-8859-1";
 var $default_charset="utf-8";
 var $headers=array("To"=>"","Subject"=>"");
 var $body=array();
 var $delivery=array("State"=>"");
 var $debug="";

 function libemail()
 {
          global $intranet_default_lang_set;
          
          $this_default_charset = "";
          if(isset($intranet_default_lang_set) && is_array($intranet_default_lang_set))
          {
          	// modifed by Ronald (20090725)
          	/*
				switch($intranet_default_lang_set[0])
				{
					case "b5":
							$this_default_charset = "big5";
							break;
					case "gb":
							$this_default_charset = "gb2312";
							break;
					case "en":
							$this_default_charset = "ISO-8859-1";
							break;
				}
			*/
			$this_default_charset = "utf-8";
          }
          	
		if($this_default_charset)
			$this->default_charset = $this_default_charset;
 }
     
 Function ValidateEmailAddress($address)
 {
  return(preg_match($this->email_regular_expression,QuoteMeta($address)));
 }

 Function QuotedPrintableEncode($text,$header_charset="",$break_lines=1)
 {
  $length=strlen($text);
  if(strcmp($header_charset,""))
  {
   $break_lines=0;
   for($index=0;$index<$length;$index++)
   {
    $code=Ord($text[$index]);
    if($code<32
    || $code>127)
     break;
   }
   if($index>0)
    return(substr($text,0,$index).$this->QuotedPrintableEncode(substr($text,$index),$header_charset,0));
  }
  for($whitespace=$encoded="",$line=0,$index=0;$index<$length;$index++)
  {
   $character=$text[$index];
   $order=Ord($character);
   $encode=0;
   switch($order)
   {
    case 9:
    case 32:
     if(!strcmp($header_charset,""))
     {
      $previous_whitespace=$whitespace;
      $whitespace=$character;
      $character="";
     }
     else
     {
      if(!strcmp($order,32))
       $character="_";
      else
       $encode=1;
     }
     break;
    case 10:
    case 13:
     if(strcmp($whitespace,""))
     {
      if($break_lines
      && $line+3>75)
      {
       $encoded.="=\n";
       $line=0;
      }
      $encoded.=sprintf("=%02X",Ord($whitespace));
      $line+=3;
      $whitespace="";
     }
     $encoded.=$character;
     $line=0;
     continue 2;
    default:
     if($order>127
     || $order<32
     || !strcmp($character,"=")
     || (strcmp($header_charset,"")
     && (!strcmp($character,"?")
     || !strcmp($character,"_")
     || !strcmp($character,"(")
     || !strcmp($character,")"))))
      $encode=1;
     break;
   }
   if(strcmp($whitespace,""))
   {
    if($break_lines
    && $line+1>75)
    {
     $encoded.="=\n";
     $line=0;
    }
    $encoded.=$whitespace;
    $line++;
    $whitespace="";
   }
   if(strcmp($character,""))
   {
    if($encode)
    {
     $character=sprintf("=%02X",$order);
     $encoded_length=3;
    }
    else
     $encoded_length=1;
    if($break_lines
    && $line+$encoded_length>75)
    {
     $encoded.="=\n";
     $line=0;
    }
    $encoded.=$character;
    $line+=$encoded_length;
   }
  }
  if(strcmp($whitespace,""))
  {
   if($break_lines
   && $line+3>75)
    $encoded.="=\n";
   $encoded.=sprintf("=%02X",Ord($whitespace));
  }
  if(strcmp($header_charset,"")
  && strcmp($text,$encoded))
   return("=?$header_charset?q?$encoded?=");
  else
   return($encoded);
 }

 Function WrapText($text,$line_length=75,$line_break="\n")
 {
  for($wrapped="",$length=strlen($text),$line_start=0;$length-$line_start>$line_length;)
  {
   $line=substr($text,$line_start,$line_length);
   if(($position=strpos($line,$line_break))
   || !strcmp(substr($line,0,strlen($line_break)),$line_break))
   {
    $append=substr($line,0,$position).$line_break;
    $wrapped.=$append;
    $line_start+=strlen($append);
    continue;
   }
   if(!($position=strrpos($line," "))
   && strcmp($text[$line_start]," "))
   {
    $wrapped.=$line.$line_break;
    $line_start+=$line_length;
   }
   else
   {
    $wrapped.=substr($line,0,$position).$line_break;
    $line_start+=$position+1;
   }
  }
  return($wrapped.substr($text,$line_start));
 }

 Function OutputError($error)
 {
  if(strcmp($function=$this->debug,"")
  && strcmp($error,""))
   $function($error);
  return($error);
 }

 Function SendMail($to,$subject,$body,$headers)
 {
  return(mail($to,$subject,$body,$headers) ? "" : "it was not possible to send e-mail message");
 }

 Function StartSendingMessage()
 {
  if(strcmp($this->delivery["State"],""))
   return($this->OutputError("the message was already started to be sent"));
  $this->delivery=array("State"=>"SendingHeaders");
  return("");
 }

 Function SendMessageHeaders($headers)
 {
  if(strcmp($this->delivery["State"],"SendingHeaders"))
  {
   if(!strcmp($this->delivery["State"],""))
    return($this->OutputError("the message was not yet started to be sent"));
   else
    return($this->OutputError("the message headers were already sent"));
  }
  $this->delivery["Headers"]=$headers;
  $this->delivery["State"]="SendingBody";
  return("");
 }

 Function SendMessageBody($data)
 {
  if(strcmp($this->delivery["State"],"SendingBody"))
   return($this->OutputError("the message headers were not yet sent"));
  if(IsSet($this->delivery["Body"]))
   $this->delivery["Body"].=$data;
  else
   $this->delivery["Body"]=$data;
  return("");
 }

 Function EndSendingMessage()
 {
  if(strcmp($this->delivery["State"],"SendingBody"))
   return($this->OutputError("the message body data was not yet sent"));
  if(!IsSet($this->delivery["Headers"])
  || count($this->delivery["Headers"])==0)
   return($this->OutputError("message has no headers"));
  $headers=$this->delivery["Headers"];
  for($headers_text=$to=$subject="",$header=0,Reset($headers);$header<count($headers);Next($headers),$header++)
  {
   switch(strtolower(Key($headers)))
   {
    case "to":
     $to=$headers[Key($headers)];
     break;
    case "subject":
     $subject=$headers[Key($headers)];
     break;
    default:
     $headers_text.=Key($headers).": ".$headers[Key($headers)]."\n";
   }
  }
  if(!strcmp($to,""))
   return($this->OutputError("it was not specified a valid To: header"));
  if(!strcmp($subject,""))
   return($this->OutputError("it was not specified a valid Subject: header"));
  if(strcmp($error=$this->SendMail($to,$subject,$this->delivery["Body"],$headers_text),""))
   return($this->OutputError($error));
  $this->delivery=array("State"=>"");
  return("");
 }

 Function StopSendingMessage()
 {
  $this->delivery=array("State"=>"");
  return("");
 }

 Function SetHeader($header,$value,$encoding_charset="")
 {
  $this->headers["$header"]=(!strcmp($encoding_charset,"") ? "$value" : $this->QuotedPrintableEncode($value,$encoding_charset));
  return("");
 }

 Function SetEncodedHeader($header,$value,$charset="")
 {
	 # updated on 20090312 yatwoon
	 if(!strcmp($charset,""))
   		$charset=$this->default_charset;
  	return($this->SetHeader($header,$value,$charset));
  	
  	//return($this->SetHeader($header,$value,$this->default_charset));
 }

 Function SetEncodedEmailHeader($header,$address,$name)
 {
  if($name=="")
    return($this->SetHeader($header, $address));
  else
    return($this->SetHeader($header, $this->QuotedPrintableEncode($name,$this->default_charset)." <".$address.">"));
 }

 Function AddPlainTextPart($text)
 {
  $this->body[]=array(
   "Content-Type"=>"text/plain",
   "PART"=>$text
  );
  return("");
 }

 Function AddEncodedQuotedPrintableTextPart($text,$charset="")
 {
  if(!strcmp($charset,""))
   $charset=$this->default_charset;
  $this->body[]=array(
   "Content-Type"=>"text/plain; charset=$charset",
   "Content-Transfer-Encoding"=>"quoted-printable",
   "PART"=>$text
  );
  return("");
 }

 Function AddQuotedPrintableTextPart($text,$charset="")
 {
  return($this->AddEncodedQuotedPrintableTextPart($this->QuotedPrintableEncode($text),$charset));
 }

 Function Send()
 {
  if(count($this->body)==0)
   return($this->OutputError("message has no body parts"));
  if(strcmp($error=$this->StartSendingMessage(),""))
   return($error);
  $headers=$this->headers;
  if(strcmp($this->mailer,""))
   $headers["X-Mailer"]=$this->mailer;
  for($part=0;$part<count($this->body);$part++)
  {
   if($part>0)
    return("multipart mails are not yet supported");
   if(!IsSet($this->body[$part]["PART"]))
    return("it was added a part without a body PART");
   if(!IsSet($this->body[$part]["Content-Type"]))
    return("it was added a part without Content-Type: defined");
   switch(strtok($this->body[$part]["Content-Type"],";"))
   {
    case "text/plain":
     $headers["MIME-Version"]="1.0";
     $headers["Content-Type"]=$this->body[$part]["Content-Type"];
     if(IsSet($this->body[$part]["Content-Transfer-Encoding"]))
      $headers["Content-Transfer-Encoding"]=$this->body[$part]["Content-Transfer-Encoding"];
     break;
    default:
     return("Content-Type: ".$this->body[$part]["Content-Type"]." not yet supported");
   }
  }
  if(!strcmp($error=$this->SendMessageHeaders($headers),""))
  {
   for($part=0;$part<count($this->body);$part++)
   {
    if(strcmp($error=$this->SendMessageBody($this->body[$part]["PART"]),""))
     break;
   }
   if(!strcmp($error,""))
    $error=$this->EndSendingMessage();
  }
  if(strcmp($error,""))
   $this->StopSendingMessage();
  return($error);
 }

};

?>