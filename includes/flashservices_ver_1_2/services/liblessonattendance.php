<?php
// using by Kenneth Chung
class liblessonattendance 
{
	var $CurAcademicYearID;
	var $methodTable;
	var $sql;
  var $db;
  var $rs;
  var $conn;
	
	// constructor function
	function liblessonattendance()
	{
		// -----------------------------------------
		// the method table describes all the
		// available methods for this class to flash
		// and define the roles of these methods
		// -----------------------------------------
		
		$this->methodTable = array(
			// name of the function
			"Test_DB_Connection" => array(
				"description" => "Testing DB Connection",
				//"arguments" => array("SubjectGroupID - ID of subject group"),
				"access" => "remote"
				//"returns" => "string"
			),
			"Test_Connection" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			)/*,
			"Get_Current_Academic_Year_ID" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"returnVector" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"returnArray" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"db_free_result" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"db_db_query" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"db_num_rows" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"db_fetch_array" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"db_insert_id" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"db_affected_rows" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"Get_Safe_Sql_Query" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"Start_Trans" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"Commit_Trans" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"RollBack_Trans" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"libdb" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"intranet_opendb" => array(
				"description" => "Another Test DB Connection but without parameter",
				"access" => "remote"
				//"returns" => "string"
			),
			"Build_Subject_Group_Attend_Overview" => array(
				"description" => "Build the lesson attendance overview table in year base",
				"access" => "remote"
			),
			"Build_Subject_Group_Student_Attendance" => array(
				"description" => "Build the lesson attendance student data table in year base",
				"access" => "remote"
			)*/,
			"Get_Default_Plan" => array(
				"description" => "Get Sitting Plans from ATTEND_DEFAULT_PLAN",
				"access" => "remote",
				"arguments" => array(
					"RecordID" => array("required" => "true", "description" => "ID of subject group/ year class"),
					"RecordType" => array("required" => "true", "description" => "identifier of recordID")
				),
				"returns" => "array"
			),
			"Get_Student_List" => array(
				"description" => "Get Subject Group student list from SUBJECT_TERM_CLASS_USER",
				"access" => "remote",
				"arguments" => array(
					"RecordID" => array("required" => "true", "description" => "ID of subject group/ year class"),
					"RecordType" => array("required" => "true", "description" => "identifier of recordID")
				)/*,
				"returns" => "array"*/
			),
			"Get_Lesson_Saved_Plan" => array(
				"description" => "Get Sitting Plan from SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}",
				"access" => "remote",
				"arguments" => array(
					"SubjectGroupID" => array("required" => "true", "description" => "ID of subject group"),
					"LessonDate" => array("required" => "true", "description" => "Date of lesson"),
					"RoomAllocationID" => array("required" => "true", "description" => "Room allocation ID of the lesson")
				),
				"returns" => "array"
			),
			"Get_Lesson_Attendance_Record" => array(
				"description" => "Get Attendance data from SUBJECT_GROUP_STUDENT_ATTENDANCE_{Current Academic Year ID}",
				"access" => "remote",
				"arguments" => array(
					"AttendOverviewID" => array("required" => "true", "description" => "Record ID of SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}")
				),
				"returns" => "array"
			),
			"Save_Default_Plan" => array(
				"description" => "Save Sitting Plans to ATTEND_DEFAULT_PLAN",
				"access" => "remote",
				"arguments" => array(
					"RecordID" => array("required" => "true", "description" => "ID of subject group/ year class"),
					"PlanDetailArray" => array("required" => "true", "description" => "Plan details array that storing sitting plan information"),
					"RecordType" => array("required" => "true", "description" => "identifier of recordID")
				)/*,
				"returns" => "boolean"*/
			),
			"Save_Lesson_Plan" => array(
				"description" => "Save Sitting Plan to SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}",
				"access" => "remote",
				"arguments" => array(
					"SubjectGroupID" => array("required" => "true", "description" => "ID of subject group"),
					"LessonDate" => array("required" => "true", "description" => "Date of lesson"),
					"RoomAllocationID" => array("required" => "true", "description" => "Room allocation ID of the lesson"),
					"StartTime" => array("required" => "true", "description" => "Lesson start time"),	
					"EndTime" => array("required" => "true", "description" => "Lesson end time"),				
					"PlanID" => array("required" => "true", "description" => "PlanID"),
					"PlanName" => array("required" => "true", "description" => "Plan Name"),
					"StudentPositionAMF" => array("required" => "true", "description" => "StudentPositionAMF"),
					"PlanDetailAMF" => array("required" => "true", "description" => "PlanLayoutAMF")
				)
			),
			"Save_Lesson_Attendance_Data" => array(
				"description" => "Save Attendance data to SUBJECT_GROUP_STUDENT_ATTENDANCE_{Current Academic Year ID}",
				"access" => "remote",
				"arguments" => array(
					"AttendOverviewID" => array("required" => "true", "description" => "Record ID of SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}"),
					"StudentAttendArray" => array("required" => "true", "description" => "Student information: Student ID, AttendStatus, Remarks")
				),
				"returns" => "boolean"
			),
			"Check_Record_Exist_Subject_Group_Attend_Overview" => array(
				"description" => "Check Existence of Record SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}",
				"access" => "remote",
				"arguments" => array(
					"SubjectGroupID" => array("required" => "true", "description" => "ID of subject group"),
					"LessonDate" => array("required" => "true", "description" => "Lesson date"),
					"RoomAllocationID" => array("required" => "true", "description" => "ID of allocated room")
				)
			),
			"Check_Record_Exist_Subject_Group_Attend_Default_Plan" => array(
				"description" => "Check Existence of Record ATTEND_DEFAULT_PLAN",
				"access" => "remote",
				"arguments" => array(
					"SubjectGroupID" => array("required" => "true", "description" => "ID of subject group")
				),
				"returns" => "boolean"
			)
		);
		

		$this->intranet_opendb();
		$this->CurAcademicYearID = $this->Get_Current_Academic_Year_ID();
	}



	##########################* REAL METHOD LOGIC *#########################

	function Test_DB_Connection($SubjectGroupID)
	{
		$sql = "SELECT COUNT(*) FROM SUBJECT_GROUP_ATTEND_OVERVIEW WHERE SubjectGroupID = '$SubjectGroupID'";
		$result=$this->returnVector($sql);
		return $result[0];
	}
	
	function Test_Connection()
	{
		$sql="SELECT count(*) FROM SUBJECT_TERM_CLASS";
		$result=$this->returnVector($sql);
		return $result[0];
	}
	
	// Build the lesson attendance overview table in year base
	function Build_Subject_Group_Attend_Overview()
	{
		$sql = 'CREATE TABLE IF NOT EXISTS SUBJECT_GROUP_ATTEND_OVERVIEW_'.$this->CurAcademicYearID.' (
						  AttendOverviewID int(8) NOT NULL auto_increment,
						  SubjectGroupID int(8) NOT NULL,
						  LessonDate date NOT NULL,
						  RoomAllocationID int(8) NOT NULL,
						  StartTime time default NULL,
						  EndTime time default NULL,
						  PlanID varchar(20) default NULL, 
						  PlanName varchar(255),
						  StudentPositionAMF TEXT, 
						  PlanLayoutAMF TEXT,
						  CreateBy int(8) default NULL,
						  CreateDate datetime default NULL,
						  LastModifiedBy int(8) default NULL,
						  LastModifiedDate datetime default NULL,
						  PRIMARY KEY  (AttendOverviewID),
						  UNIQUE KEY SubjectGroupID (SubjectGroupID,LessonDate,RoomAllocationID)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8';
		return $this->db_db_query($sql) or die(mysql_error());
	}
	
	// Build the lesson attendance student data table in year base
	function Build_Subject_Group_Student_Attendance()
	{
		$sql = 'CREATE TABLE IF NOT EXISTS SUBJECT_GROUP_STUDENT_ATTENDANCE_'.$this->CurAcademicYearID.' (
					  AttendOverviewID int(8) NOT NULL,
					  StudentID int(8) NOT NULL,
					  AttendStatus int(2) default NULL,
					  InTime time default NULL,
					  OutTime time default NULL,
					  LateFor int(8) default 0,
					  Remarks text,
					  CreateBy int(8) default NULL,
					  CreateDate datetime default NULL,
					  LastModifiedBy int(8) default NULL,
					  LastModifiedDate datetime default NULL,
					  PRIMARY KEY  (AttendOverviewID,StudentID),
					  KEY StudentID (StudentID,AttendOverviewID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
		return $this->db_db_query($sql) or die(mysql_error());
	}
	
	// Get Sitting Plans from ATTEND_DEFAULT_PLAN
	function Get_Default_Plan($RecordID,$RecordType="SubjectGroup")
	{
		$NameField = $this->getNameFieldByLang2("u.");
		
		if ($RecordType=="SubjectGroup") {
			$SubjectGroupFloorPlan = array();
			$YearClassFloorPlan = array();
			
			$sql = "SELECT 
								adp.FlashPlanID as PlanID, 
								adp.PlanName, 
								adp.StudentPositionAMF, 
								adp.PlanLayoutAMF,
								adp.LastModifiedDate, 
								".$NameField." as LastModifiedBy 
							FROM 
								ATTEND_DEFAULT_PLAN as adp 
								LEFT JOIN 
								INTRANET_USER as u 
								on adp.LastModifiedBy = u.UserID 
							WHERE 
								adp.SubjectGroupID = '$RecordID' ";
			$SubjectGroupFloorPlan = $this->returnArray($sql);
								
			// check if subject group is class based
			$sql = "select UserID from SUBJECT_TERM_CLASS_USER where SubjectGroupID = '$RecordID'";
			$SubjectGroupUserList = $this->returnVector($sql);
			
			// get subject group's year(form) relation and get all classID related to those year
			$sql = "select 
								YearClassID 
							From 
								SUBJECT_TERM_CLASS_YEAR_RELATION as stcyr 
								INNER JOIN 
								YEAR_CLASS as yc 
								on 
									stcyr.SubjectGroupID = '".$RecordID."' 
									and stcyr.YearID = yc.YearID 
									and yc.AcademicYearID = '".$this->CurAcademicYearID."'";
			$YearClassList = $this->returnVector($sql);
			
			// get year class student from the YearClassList and compare the student list to subject group student list
			for ($i=0; $i< sizeof($YearClassList); $i++) {
				$sql = "select 
									ycu.UserID 
								From 
									YEAR_CLASS_USER as ycu 
									INNER JOIN 
									INTRANET_USER as u 
									on ycu.UserId = u.UserID 
								where 
									YearClassID = '".$YearClassList[$i]."'";
				$YearClassUserList = $this->returnVector($sql);
				
				if (sizeof($SubjectGroupUserList) == sizeof($YearClassUserList)) {
					$Match = true;
					for ($j=0; $j< sizeof($SubjectGroupUserList); $j++) {
						if (!in_array($SubjectGroupUserList[$j],$YearClassUserList)) {
							$Match = false;
							break;
						}
					}
					
					if ($Match) {
						$MatchedYearClassID[] = $YearClassList[$i];
						break;
					}
				}
			}
			
			if (sizeof($MatchedYearClassID) > 0) {
				$Cond = implode(',',$MatchedYearClassID);
				
				$sql = "Select 
									adp.FlashPlanID as PlanID, 
									adp.PlanName, 
									adp.StudentPositionAMF, 
									adp.PlanLayoutAMF,
									adp.LastModifiedDate, 
									".$NameField." as LastModifiedBy 
								FROM 
									ATTEND_DEFAULT_PLAN as adp 
									LEFT JOIN 
									INTRANET_USER as u 
									on adp.LastModifiedBy = u.UserID 
								WHERE 
									adp.YearClassID in (".$Cond.") 
							 ";
				$YearClassFloorPlan = $this->returnArray($sql);
			}
			
			// combine FloorPlan from Year Class and Subject group		
			$Result = array_merge($SubjectGroupFloorPlan,$YearClassFloorPlan);
		}
		else {
			$sql = "SELECT 
								adp.FlashPlanID as PlanID, 
								adp.PlanName, 
								adp.StudentPositionAMF, 
								adp.PlanLayoutAMF,
								adp.LastModifiedDate, 
								".$NameField." as LastModifiedBy 
							FROM 
								ATTEND_DEFAULT_PLAN as adp 
								LEFT JOIN 
								INTRANET_USER as u 
								on adp.LastModifiedBy = u.UserID 
							WHERE 
								adp.YearClassID = '$RecordID' ";
			
			$Result = $this->returnArray($sql);
		}
		
		return $Result;
	}
	
	// Get Subject Group student list from SUBJECT_TERM_CLASS_USER
	function Get_Student_List($RecordID,$RecordType="SubjectGroup",$LessonDate="")
	{
		include("../../../includes/global.php");
		
		$TableName = ($RecordType == "SubjectGroup")? "SUBJECT_TERM_CLASS_USER" : "YEAR_CLASS_USER";
		
		$sql = "SELECT 
							i.UserID AS StudentID,
							i.EnglishName, 
							i.ChineseName,
							i.ClassNumber,
							yc.ClassTitleEN,
							yc.ClassTitleB5,
							CONCAT('0') as AttendStatus, 
							CONCAT('') as Remarks, 
							i.UserLogin, 
							CONCAT('0') as LateFor
					 FROM 
					 		".$TableName." a 
					 		INNER JOIN 
					 		INTRANET_USER i 
					 		ON 
					 			a.UserID=i.UserID 
					 			AND 
								i.RecordType = 2 
								AND 
								i.RecordStatus IN (0,1,2) 
					 			AND ";
		if ($RecordType == "SubjectGroup") {
			$sql .= " a.SubjectGroupID='$RecordID' ";
		}
		else {
			$sql .= " a.YearClassID='$RecordID' ";
		}
		$sql .= "	INNER JOIN 
							YEAR_CLASS yc
							on 
								yc.AcademicYearID = '".$this->CurAcademicYearID."' 
								AND 
								yc.ClassTitleEN = i.ClassName ";
		//echo $sql;
		$StudentList = $this->returnArray($sql);
		
		for ($i=0; $i< sizeof($StudentList); $i++) {
			if ($RecordType == "SubjectGroup" && $LessonDate != "") {
				$Month = date('m',strtotime($LessonDate));
				$Year = date('Y',strtotime($LessonDate));
				$Day = date('d',strtotime($LessonDate));
				$this->createTable_Card_Student_Daily_Log($Year,$Month);
				
				$sql = 'Select 
									AMStatus 
								From 
									CARD_STUDENT_DAILY_LOG_'.$Year.'_'.$Month .'
								where 
									UserID = \''.$StudentList[$i]['StudentID'].'\'
									and 
									DayNumber = \''.$Day.'\'
							 ';
				$DailyAttendStatus = $this->returnVector($sql);
				
				switch ($DailyAttendStatus[0]) {
					case '1': // absent
						$StudentList[$i]['AttendStatus'] = '3';
						break;
					case '2': // late
						$StudentList[$i]['AttendStatus'] = '0';
						break;
					case '0': // normal
						$StudentList[$i]['AttendStatus'] = '0';
						break;
					case '3': // outing
						$StudentList[$i]['AttendStatus'] = '1';
						break;
					default:
						$StudentList[$i]['AttendStatus'] = '3';
						break;
				}
				
			}
			
			$PhotoLink = (is_file($intranet_root."/file/user_photo/".$StudentList[$i]['UserLogin'].".jpg"))? "/file/user_photo/".$StudentList[$i]['UserLogin'].".jpg":"";
			$Return[] = array($StudentList[$i]['StudentID'],
												$StudentList[$i]['EnglishName'],
												$StudentList[$i]['ChineseName'],
												$StudentList[$i]['ClassNumber'],
												$StudentList[$i]['ClassTitleEN'],
												$StudentList[$i]['AttendStatus'],
												$StudentList[$i]['Remarks'],
												$PhotoLink,
												$StudentList[$i]['LateFor'],
												$StudentList[$i]['ClassTitleB5']);
		}
		return (sizeof($Return)>0)? $Return:-1;
	}
	
	// Get Sitting Plan from SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}
	function Get_Lesson_Saved_Plan($SubjectGroupID, $LessonDate, $RoomAllocationID)
	{
		$this->Build_Subject_Group_Attend_Overview();
		$NameField = $this->getNameFieldByLang2("u.");
		
		$sql = "SELECT 
							sgao.PlanID, 
							sgao.PlanName, 
							sgao.StudentPositionAMF, 
							sgao.PlanLayoutAMF, 
							".$NameField." as LastModifiedBy,
							sgao.LastModifiedDate
						FROM 
							SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID." as sgao 
							LEFT JOIN 
							INTRANET_USER as u 
							on sgao.LastModifiedBy = u.UserID 
						WHERE 
							sgao.SubjectGroupID = '$SubjectGroupID' 
							AND 
							sgao.LessonDate = '$LessonDate' 
							AND 
							sgao.RoomAllocationID = '$RoomAllocationID' ";
		$Result = $this->returnArray($sql,6);
		
		return array($Result[0]['PlanID'],$Result[0]['PlanName'],$Result[0]['StudentPositionAMF'],$Result[0]['PlanLayoutAMF'],$Result[0]['LastModifiedDate'],$Result[0]['LastModifiedBy']);
	}
	
	// Get Attendance data from SUBJECT_GROUP_STUDENT_ATTENDANCE_{Current Academic Year ID}
	function Get_Lesson_Attendance_Record($AttendOverviewID) 
	{
		include("../../../includes/global.php");
		$this->Build_Subject_Group_Student_Attendance();
		
		$sql = "SELECT 
							s.StudentID, 
							u.EnglishName, 
							u.ChineseName, 
							u.ClassNumber, 
							yc.ClassTitleEN, 
							yc.ClassTitleB5, 
							s.AttendStatus, 
							s.Remarks, 
							u.UserLogin, 
							s.LateFor 
						FROM 
							SUBJECT_GROUP_STUDENT_ATTENDANCE_".$this->CurAcademicYearID." s 
							INNER JOIN 
						 	INTRANET_USER u 
						 	ON 
						 		AttendOverviewID = '$AttendOverviewID' 
						 		and 
						 		u.UserID = s.StudentID 
						 	INNER JOIN 
						 	YEAR_CLASS yc 
						 	ON 
						 		yc.AcademicYearID = '".$this->CurAcademicYearID."' 
						 		and 
						 		yc.ClassTitleEN = u.ClassName";
		/*echo '<pre>';
		var_dump($sql);
		echo '</pre>';*/
		$AttendRecord = $this->returnArray($sql);
		
		for ($i=0; $i< sizeof($AttendRecord); $i++) {
			$PhotoLink = (is_file($intranet_root."/file/user_photo/".$AttendRecord[$i]['UserLogin'].".jpg"))? "/file/user_photo/".$AttendRecord[$i]['UserLogin'].".jpg":"";
			$Return[] = array($AttendRecord[$i]['StudentID'],
												$AttendRecord[$i]['EnglishName'],
												$AttendRecord[$i]['ChineseName'],
												$AttendRecord[$i]['ClassNumber'],
												$AttendRecord[$i]['ClassTitleEN'],
												$AttendRecord[$i]['AttendStatus'],
												$AttendRecord[$i]['Remarks'],
												$PhotoLink,
												$AttendRecord[$i]['LateFor'],
												$AttendRecord[$i]['ClassTitleB5']);
		}
		return (sizeof($Return)>0)? $Return:-1;
	}
	
	// Save Sitting Plans to ATTEND_DEFAULT_PLAN
	function Save_Default_Plan($RecordID,$PlanDetailArray,$RecordType="SubjectGroup")
	{
		$this->Start_Trans();
		$result = array();
		
		$FieldName = ($RecordType=="SubjectGroup")? "SubjectGroupID":"YearClassID";
		$sql = 'delete from ATTEND_DEFAULT_PLAN where '.$FieldName.' = \''.$RecordID.'\'';
		$result['delete'] = $this->db_db_query($sql);
		if(is_array($PlanDetailArray))
		{
			for($i=0;$i<count($PlanDetailArray);$i++)
			{
				//$TestReturn .= $PlanDetailArray[$i][0].'='.$PlanDetailArray[$i][1].'='.$PlanDetailArray[$i][2].'='.$PlanDetailArray[$i][3].'||||||||';
				$PlanDetailPlanID = $PlanDetailArray[$i][0];
				if ($PlanDetailPlanID[0] != "y" || $RecordType == "YearClass") {
					$PlanDetailPlanName = $PlanDetailArray[$i][1];
					$PlanDetailStudentPosition = $PlanDetailArray[$i][2];
					$PlanDetailLayoutPlan = $PlanDetailArray[$i][3];
					
					$sql = "INSERT INTO ATTEND_DEFAULT_PLAN 
										(".$FieldName.", 
										FlashPlanID, 
										PlanName, 
										StudentPositionAMF, 
										PlanLayoutAMF, 
										LocationID, 
										CreateBy, 
										CreateDate, 
										LastModifiedBy, 
										LastModifiedDate
										)
									VALUES 
										('$RecordID',
										'$PlanDetailPlanID',
										'".$this->Get_Safe_Sql_Query($PlanDetailPlanName)."',
										'".$this->Get_Safe_Sql_Query($PlanDetailStudentPosition)."',
										'".$this->Get_Safe_Sql_Query($PlanDetailLayoutPlan)."',
										NULL,
										'".$_SESSION['UserID']."',
										NOW(),
										'".$_SESSION['UserID']."',
										NOW()
										) 
								";
					$result[$i]=$this->db_db_query($sql);
				}
			}
		}
		//return "11111";
		/*echo '<pre>';
		var_dump($result);
		echo '</pre>';*/
		if(in_array(false,$result))
		{
			$this->RollBack_Trans();
			return false;
		}
		else {
			$this->Commit_Trans();
			return true;
		}
	}
	
	// Save Sitting Plan to SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}
	function Save_Lesson_Plan($SubjectGroupID, $LessonDate, $RoomAllocationID, $StartTime, $EndTime, $PlanDetailPlanID, $PlanDetailPlanName, $StudentPositionAMF, $PlanDetailAMF)
	{	
		$Result['1'] = $this->Build_Subject_Group_Attend_Overview();
			
		$AttendOverviewID = -1;
		
		$sqlCheckExist = "SELECT 
												AttendOverviewID 
											FROM 
												SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID." 
											WHERE 
												SubjectGroupID='$SubjectGroupID' 
												AND 
												LessonDate='$LessonDate' 
												AND 
												RoomAllocationID='$RoomAllocationID' ";
		$resultCheckExist = $this->returnVector($sqlCheckExist);
		if(count($resultCheckExist)>0)
		{
			$AttendOverviewID = $resultCheckExist[0];
			$sql = "UPDATE SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID;
			$sql.= " SET 
								PlanID='$PlanDetailPlanID', 
								PlanName='".$this->Get_Safe_Sql_Query($PlanDetailPlanName)."', 
								StudentPositionAMF='".$this->Get_Safe_Sql_Query($StudentPositionAMF)."', 
								PlanLayoutAMF='".$this->Get_Safe_Sql_Query($PlanDetailAMF)."', ";
			$sql.= " LastModifiedBy='".$_SESSION['UserID']."', LastModifiedDate=NOW() ";
			$sql.= " WHERE AttendOverviewID='$AttendOverviewID' ";
			$Result['2'] = $this->db_db_query($sql);
		}else
		{
			$sql = "INSERT INTO SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID;
			$sql.= " (SubjectGroupID, 
								LessonDate, 
								RoomAllocationID, 
								StartTime, 
								EndTime, 
								PlanID, 
								PlanName, 
								StudentPositionAMF, 
								PlanLayoutAMF, 
								CreateBy, 
								CreateDate, 
								LastModifiedBy, 
								LastModifiedDate) ";
			$sql.= " VALUES 
							 ('$SubjectGroupID',
							 	'$LessonDate',
							 	'$RoomAllocationID',
							 	'$StartTime',
							 	'$EndTime',
							 	'$PlanDetailPlanID',
							 	'".$this->Get_Safe_Sql_Query($PlanDetailPlanName)."',
							 	'".$this->Get_Safe_Sql_Query($StudentPositionAMF)."',
							 	'".$this->Get_Safe_Sql_Query($PlanDetailAMF)."', 
							 	'".$_SESSION['UserID']."', 
							 	NOW(), 
							 	'".$_SESSION['UserID']."', 
							 	NOW()) ";
			$Result['3'] = $this->db_db_query($sql);
			$AttendOverviewID = $this->db_insert_id();
		}
		
		/*echo '<pre>';
		var_dump($Result);
		echo '</pre>';*/
		
		return $AttendOverviewID;
	}
	
	// Save Attendance data to SUBJECT_GROUP_STUDENT_ATTENDANCE_{Current Academic Year ID}
	function Save_Lesson_Attendance_Data($AttendOverviewID, $StudentAttendArray)
	{
		$this->Build_Subject_Group_Student_Attendance();
		
		$result = false;
		if(is_array($StudentAttendArray))
		{
			$this->Start_Trans();
			for($i=0;$i<count($StudentAttendArray);$i++)
			{
				$StudentID = $StudentAttendArray[$i][0];
				$AttendStatus = $StudentAttendArray[$i][1];
				$Remarks = $StudentAttendArray[$i][2];
				$LateFor = $StudentAttendArray[$i][3];
				//$Remarks = HTMLtoDB($Remarks);
				
				$sql = "INSERT INTO SUBJECT_GROUP_STUDENT_ATTENDANCE_".$this->CurAcademicYearID;
				$sql.= " (AttendOverviewID, 
									StudentID, 
									AttendStatus, 
									InTime, 
									OutTime, 
									LateFor, 
									Remarks, 
									CreateBy, 
									CreateDate, 
									LastModifiedBy, 
									LastModifiedDate) ";
				$sql.= " VALUES
									('$AttendOverviewID',
									'$StudentID',
									'$AttendStatus',
									NULL,
									NULL,
									'".$this->Get_Safe_Sql_Query($LateFor)."',
									'".$this->Get_Safe_Sql_Query($Remarks)."',
									'".$_SESSION['UserID']."',
									NOW(),
									'".$_SESSION['UserID']."',
									NOW()) 
								ON DUPLICATE KEY UPDATE 
									AttendStatus='$AttendStatus', 
									LateFor = '".$this->Get_Safe_Sql_Query($LateFor)."', 
									Remarks='".$this->Get_Safe_Sql_Query($Remarks)."', 
									LastModifiedBy='".$_SESSION['UserID']."', 
									LastModifiedDate=NOW()";
				
				$result[$i] = $this->db_db_query($sql);
			}
			if(in_array(false,$result))
			{
				$this->RollBack_Trans();
				return false;
			}
			else {
				$this->Commit_Trans();
				return true;
			}
		}
		
		return false;
	}
	
	//Check Existence of Record SUBJECT_GROUP_ATTEND_OVERVIEW_{Current Academic Year ID}
	function Check_Record_Exist_Subject_Group_Attend_Overview($SubjectGroupID,$LessonDate,$RoomAllocationID)
	{
		$this->Build_Subject_Group_Attend_Overview();
		
		$sql = "SELECT 
							AttendOverviewID 
						FROM 
							SUBJECT_GROUP_ATTEND_OVERVIEW_".$this->CurAcademicYearID." 
						WHERE 
							SubjectGroupID = '$SubjectGroupID'
							and 
							LessonDate = '$LessonDate' 
							and 
							RoomAllocationID = '$RoomAllocationID'";
		
		$result = $this->returnVector($sql);
		if(count($result)>0)
		{
			return $result[0];
		}else
		{
			return -1;
		}
	}
	
	//Check Existence of Record ATTEND_DEFAULT_PLAN
	function Check_Record_Exist_Subject_Group_Attend_Default_Plan($SubjectGroupID)
	{
		$sql = "SELECT COUNT(*) FROM ATTEND_DEFAULT_PLAN ";
		$sql.= " WHERE SubjectGroupID = '$SubjectGroupID'";
		
		$result = $this->returnVector($sql);
		if(count($result)>0)
		{
			if($result[0]>0)
			{
				return true;
			}else
			{
				return false;
			}
		}else
		{
			return false;
		}
	}
	
	function Get_Current_Academic_Year_ID(){
		$this->intranet_opendb();
		$sql = "select
					AcademicYearID
				from
					ACADEMIC_YEAR_TERM
				where
					NOW() between TermStart and TermEnd";
				$arrResult = $this->returnVector($sql);
	
		if(sizeof($arrResult)>0)
			return $arrResult[0];
		else
			return 0;
	}
	
	function returnVector($sql)
	{
		$i = 0;
		$x = array();
		$this->rs = $this->db_db_query($sql);
		
		if($this->rs && $this->db_num_rows()!=0)
		{
			while($row = $this->db_fetch_array())
			{
				$x[$i] = $row[0];
				$i++;
			}
		}
		
		if ($this->rs)
		$this->db_free_result();
		
		return $x;
	}
	
	function returnArray($sql, $field_no=null){
		$i = 0;
		$this->rs = $this->db_db_query($sql);
		$x = array();
		if ($this->rs && $this->db_num_rows()!=0)
		{
			while ($row = $this->db_fetch_array())
			{
				$x[] = $row;
			}
			$this->db_free_result();
		}
		return $x;
	}
	
	function db_free_result()
	{
		return mysql_free_result($this->rs);
	}

	function db_db_query($query)
	{
		mysql_select_db($this->db) or exit(mysql_error());
		return mysql_query($query);
	}
  
	function db_num_rows()
	{
		return mysql_num_rows($this->rs);
	}
	
	function db_fetch_array()
	{
		return mysql_fetch_array($this->rs);
	}
	
	function db_insert_id(){
		return mysql_insert_id();
	}
	
	function db_affected_rows(){
		return mysql_affected_rows();
	}
	
	function Get_Safe_Sql_Query($value) {
    return ereg_replace("'", "\\'", $value);
  }
  
  function Start_Trans() {
  	mysql_query("START TRANSACTION");
  }
  
	function Commit_Trans() {
		mysql_query("COMMIT");
	}
	
	function RollBack_Trans() {
		mysql_query("ROLLBACK");
	}
  
  function intranet_opendb()
	{
		include("../../../includes/settings.php");
		
		$db_host = ($sys_custom['MySQL_Server_Host']=="")? "localhost" : $sys_custom['MySQL_Server_Host'];
    $this->db = $intranet_db;
    $this->conn = mysql_pconnect($db_host , $intranet_db_user, $intranet_db_pass);
    mysql_query("set character_set_database='utf8'");
		mysql_query("set names utf8");
	}
	
	function getNameFieldByLang2 ($prefix ="", $displayLang="")
	{
		global $intranet_session_language;
		
		$displayLang = $displayLang ? $displayLang : $intranet_session_language;
		$chi = ($displayLang =="b5" || $displayLang == "gb");
		if ($chi)
		{
			$firstChoice = "ChineseName";
			$altChoice = "EnglishName";
		}
		else
		{
			$firstChoice = "EnglishName";
			$altChoice = "ChineseName";
		}
		$username_field = "IF($prefix$firstChoice IS NULL OR TRIM($prefix$firstChoice) = '',$prefix$altChoice,$prefix$firstChoice)";
		
		return $username_field;
	}
	
	function createTable_Card_Student_Daily_Log($year="",$month="") {
		$year=($year=="")?date("Y"):$year;
		$month=($month=="")?date("m"):$month;
		
		$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
		$sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name(
		        RecordID int(11) NOT NULL auto_increment,
		        UserID int(11) NOT NULL,
		        DayNumber int(11) NOT NULL,
		        InSchoolTime time,
		        InSchoolStation varchar(255),
		        AMStatus int,
		        LunchOutTime time,
		        LunchOutStation varchar(255),
		        LunchBackTime time,
		        LunchBackStation varchar(255),
		        PMStatus int,
		        LeaveSchoolTime time,
		        LeaveSchoolStation varchar(255),
		        LeaveStatus int,
		        ConfirmedUserID int,
		        IsConfirmed int,
		        RecordType int,
		        RecordStatus int,
		        DateInput datetime,
		        DateModified datetime,
		        PRIMARY KEY (RecordID),
		        UNIQUE UserDay (UserID, DayNumber)
		      ) ENGINE=InnoDB charset=utf8";
		$this->db_db_query($sql);
  }
}
?>