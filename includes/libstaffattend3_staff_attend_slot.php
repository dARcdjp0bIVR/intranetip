<?
// using by kenenth chung
include_once("libdb.php");

class staff_attend_slot extends libdb {
	var $SlotID;
	var $GroupID;
	var $TemplateID;
	var $StaffID;
	var $SlotName;
	var $SlotStart;
	var $SlotEnd;
	var $InWavie;
	var $OutWavie;
	var $DutyCount;
	
	var $StartHour;
	var $StartMin;
	var $StartSec;
	var $EndHour;
	var $EndMin;
	var $EndSec;
	
	function staff_attend_slot($SlotID="") {
		parent::libdb();
		if ($SlotID != "") {
			$sql = 'select 
								SlotID,
								GroupID,
								TemplateID,
								UserID,
								SlotName,
								SlotStart,
								SlotEnd,
								InWavie,
								OutWavie,
								DutyCount
							From 
								CARD_STAFF_ATTENDANCE3_SLOT 
							where 
								SlotID = \''.$SlotID.'\'
								';
			$Result = $this->returnArray($sql);
			
			$this->SlotID = $Result[0]['SlotID'];
			$this->GroupID = $Result[0]['GroupID'];
			$this->TemplateID = $Result[0]['TemplateID'];
			$this->StaffID = $Result[0]['UserID'];
			$this->SlotName = $Result[0]['SlotName'];
			$this->SlotStart = $Result[0]['SlotStart'];
			$this->SlotEnd = $Result[0]['SlotEnd'];
			$this->InWavie = $Result[0]['InWavie'];
			$this->OutWavie = $Result[0]['OutWavie'];
			$this->DutyCount = $Result[0]['DutyCount'];
			
			$StartArray = explode(':',$this->SlotStart);
			$this->StartHour = $StartArray[0];
			$this->StartMin = $StartArray[1];
			$this->StartSec = $StartArray[2];
			
			$EndArray = explode(':',$this->SlotEnd);
			$this->EndHour = $EndArray[0];
			$this->EndMin = $EndArray[1];
			$this->EndSec = $EndArray[2];
		}
	}
}
?>