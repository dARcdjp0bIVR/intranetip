<?php
// Editing by 
/****************************************
 * 
 * 2018-09-14 [Ryan] : K147437 - Ignored REMOTE_ADDR Checking due to Cloudflare like services
 * 
 ******************************************/
if (!defined("LIBHKSSF_DEFINED")) {
	define("LIBHKSSF_DEFINED", true);
	
	include_once("$intranet_root/includes/libdb.php");
	include_once("$intranet_root/includes/json.php");
	include_once("$intranet_root/includes/libgeneralsettings.php");
	include_once($intranet_root.'/includes/libfilesystem.php');
	
	class libhkssf extends libgeneralsettings
	{
		private $avaliable; 
		private $access_token; 
		private $identity; 
		
		function libhkssf()
		{
			$this->libgeneralsettings();
			$hkssf_settings = $this->Get_General_Setting('HKSSF');
			$this->avaliable = $hkssf_settings['avaliable'];
			$this->access_token = $hkssf_settings['access_token'];
			
			$jsonObj = new JSON_obj();
			$r['HTTP_CLIENT_IP'] = $_SERVER['HTTP_CLIENT_IP'];
			$r['HTTP_X_FORWARDED_FOR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
			$r['HTTP_X_FORWARDED'] = $_SERVER['HTTP_X_FORWARDED'];
			$r['HTTP_X_CLUSTER_CLIENT_IP'] = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
			$r['HTTP_FORWARDED_FOR'] = $_SERVER['HTTP_FORWARDED_FOR'];
			$r['HTTP_FORWARDED'] = $_SERVER['HTTP_FORWARDED'];
// 			$r['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
			$r['HTTP_VIA'] = $_SERVER['HTTP_VIA'];
			$this->identity = base64_encode($jsonObj->encode($r));
			
		}
		
		function saveRequest($r)
		{
			$s = time();
			$rs = $s - $r;
			$rs = $this->put($rs);
			$sql = "Insert into HKSSF_REQUESTS_MAP
					(s,rs , r) Values
					('$s', '$rs', '$this->identity')
					";
			$result = $this->db_db_query($sql);
			return $result ? $rs : false;
		}
		
		function signRequest($s,$rs)
		{
			$sql = "Select id 
					From HKSSF_REQUESTS_MAP
					Where s = ".$this->pack_value($s,'str')."
					And rs = ". $this->pack_value($rs,'str')."
					And e is null";
			
			$result = $this->returnArray($sql);
			
			
			if(count($result) === 1  )
			{
				$sql = "update
					HKSSF_REQUESTS_MAP
					set e = '".date('Y-m-d H:m:s')."'
					Where id = ".$this->pack_value($result[0]['id'],'int');
				
				return $this->db_db_query($sql);
			}
			
			return false;
		}
		
		function closeRequest($id)
		{
			$sql = "update
				HKSSF_REQUESTS_MAP
				set e = '".date('Y-m-d H:m:s')."'
				Where id = ".$this->pack_value($id,'int');
	
			return $this->db_db_query($sql);
		}
		
		function matchRequest($s,$rs)
		{
			$sql = "Select id
					From HKSSF_REQUESTS_MAP
					Where s = ".$this->pack_value($s,'str')."
					And rs = ". $this->pack_value($rs,'str')."
					And r = ". $this->pack_value($this->identity, 'str')."
					And e is null";
				
			$result = $this->returnArray($sql);

			if(count($result) === 1  )
			{
				return $result[0]['id'];
			}
			
			header("HTTP/1.1 403");
			exit();
				
		}
		
		function isAvaliable()
		{
			return $this->avaliable;
		}
		
		
		/** 
		 * Use put/getData if params canbe a long string
		 * 
		 * ( more than 50 chars in raw data) 
		 **/
		function put($r,$s='')
		{
			$s = $s ? $s : $this->access_token; 
			return  trim(shell_exec('echo ' . $r . ' | openssl aes-256-cbc -a -salt -k ' . $s));
		}
		
		function get($r)
		{
			return  trim(shell_exec('echo ' . $r . ' | openssl aes-256-cbc -d -a -k ' . $this->access_token));
		}
		
		function getData($filepath)
		{
			return  trim(shell_exec('openssl aes-256-cbc -d -a -in '.$filepath.' -k ' . $this->access_token));
		}
		
		function putData($filepath,$s='')
		{
			$s = $s ? $s : $this->access_token;
			return  trim(shell_exec('openssl aes-256-cbc -a -in '.$filepath.' -salt -k ' . $s));
		}
	
		
		function checkParams($request, $requirements=array())
		{
			if(!is_array($request))
			{
				header("HTTP/1.1 403");
				exit();
			}
			
			foreach($requirements as $requirement)
			{
				$matched = isset($request[$requirement]);
				if(!$matched)
				{
					header("HTTP/1.1 403");
					exit();
				}
			}
			return true;
		}
		
		
		/* Helper functions */
		function CreateDataInput($parPlainText)
		{
			global $intranet_root;
			$lfs = new libfilesystem();
		
			$tmpFilePath =  $intranet_root.'/file/hkssf/' ;
			if (!file_exists($tmpFilePath)) {
				$successAry['createRootFolder'] = $lfs->folder_new($tmpFilePath);
			}
		
			$tmpFilePath .= 'd_'.date('Ymd_His').rand().'_.tmp';
			$lfs->file_write($parPlainText, $tmpFilePath);
		
			return $tmpFilePath;
		}
		
		function DeleteDataInput($tmpFilePath)
		{
			$lfs = new libfilesystem();
			if (file_exists($tmpFilePath)) {
				return $lfs->file_remove($tmpFilePath);
			}
			return true;
		}
	}
}
?>