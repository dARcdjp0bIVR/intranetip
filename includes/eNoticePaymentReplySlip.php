<?php
// Editing by 

include_once($intranet_root.'/includes/libdb.php');
include_once($intranet_root.'/includes/libpayment.php');

if (!defined("ENOTICEPAYMENTREPLYSLIP_DEFINED"))                     // Preprocessor directive
{

	define("ENOTICEPAYMENTREPLYSLIP_DEFINED", true);
	
	define("QTYPE_WHETHER_TO_PAY","1");
	define("QTYPE_MUST_PAY","12");
	define("QTYPE_ONE_PAY_ONE_CAT","2");
	define("QTYPE_ONE_PAY_FEW_CAT","9");
	define("QTYPE_FEW_PAY_FEW_CAT","3");
	define("QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY","17");
	define("QTYPE_TEXT_LONG","10");
	define("QTYPE_TEXT_SHORT","11");
	define("QTYPE_TRUE_OR_FALSE","13");
	define("QTYPE_MC_SINGLE","14");
	define("QTYPE_MC_MULTIPLE","15");
	define("QTYPE_NOT_APPLICABLE","16");
	
	define("REPLYSLIP_EDIT_MODE","0");
	define("REPLYSLIP_FILLIN_ANSWER_MODE","1");
	define("REPLYSLIP_VIEW_ONLY_MODE","2");
	
	class eNoticePaymentReplySlip extends libdb
	{
		var $QUE_SEP = '#QUE#';
		var $ITEM_SEP = '||';
		var $OPT_SEP = '#OPT#';
		var $TAR_SEP = '#TAR#';
		var $MSUB_SEP = '#MSUB#';
		var $CATEGORYID_SEP = '#CATEGORYID#';
		var $PAYMENTITEMID_SEP = '#PAYMENTITEMID#';
		var $AMT_SEP = '#AMT#';
		var $ANS_SEP = '#ANS#';
		var $SUBSIDY_SEP = '#SUBSIDY#';
		var $IDENTITIES_SEP = '#IDENTITIES#'; // subsidy identities
		var $IDENTITY_SEP = '#IDENTITY#';
		var $STUDENTS_SEP = '#STUDENTS#'; // individual subsidy students amounts
		var $STUDENT_SEP = '#STUDENT#';
		var $SUBSIDY_PART_SEP = '_'; // separate subsidy enable status, identities and students
		var $QUANTITY_SEP = '#QUANTITY#';
		
		var $is_ej = false;
		
		function eNoticePaymentReplySlip()
		{
			global $intranet_root, $intranet_version, $eclass_version;
			
			$this->libdb();
			
			$this->is_ej = $intranet_version == "2.0" || $eclass_version==3.0;
		}
		
		function isEJ()
		{
			return $this->is_ej;
		}
		
		function isPaymentTypeQuestion($type)
		{
			$payment_types = array(QTYPE_WHETHER_TO_PAY,QTYPE_MUST_PAY,QTYPE_ONE_PAY_ONE_CAT,QTYPE_ONE_PAY_FEW_CAT,QTYPE_FEW_PAY_FEW_CAT,QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY);
			return in_array($type, $payment_types);
		}
		
		function convertToFloat($value)
		{
			$converted_value = floatval($value);
			if($converted_value == null){
				return 0.00;
			}else{
				return $converted_value;
			}
		}
		
		function convertToInteger($value)
		{
			$converted_value = intval($value);
			if($converted_value == null){
				return 0;
			}else{
				return $converted_value;
			}
		}
		
		function convertToString($value)
		{
			return strval($value);
		}
		
		function convertToEmptyStringOrInteger($value)
		{
			if(trim($value) == ''){
				return '';
			}
			$converted_value = intval($value);
			if($converted_value == null){
				return '';
			}else{
				return $converted_value;
			}
		}
		
		function formatDisplayNumber($value)
		{
			return floatval(sprintf('%.2f',$value));
		}
		
		function extractNumberText($value)
		{
			if(preg_match('/^(\d+\.?\d*)(.*)$/',$value,$matches)){
				return array($this->formatDisplayNumber($matches[1]),trim($matches[2]));
			}else{
				return array(0,'');
			}
		}
		
		function replaceSpecialChars($text)
		{
			if($this->isEJ()){
				// EJ big5 encoding has special handling of | which is ascii code may be part of Big5 characters, cater with double ||
				$text = str_replace(array('>','<','\'','"','||','#',"\n"),array('&gt;','&lt;','&apos;','&quot;','&vert;&vert;','&num;','<br />'),$text);
				$text = ' '.trim($text).' '; // pad spaces to separate text content with separators as some Big5 characters sequence may break the separators 
				return $text;
			}else{
				// UTF-8 is fine
				return str_replace(array('>','<','\'','"','|','#',"\n"),array('&gt;','&lt;','&apos;','&quot;','&vert;','&num;','<br />'),$text);
			}
		}
		
		function reverseReplaceSpecialChars($text)
		{	
			$text = str_replace(array('<br />','&num;','&vert;','&quot;','&apos;','&gt;','&lt;'),array("\n",'#','|','"','\'','>','<'),$text);
			if($this->isEJ()){
				$text = trim($text);
			}
			
			return $text;
		}
		
		function getRequiredWords()
		{
			global $Lang, $i_Survey_AllRequire2Fill, $i_Notice_ReplySlip, $i_Form_answersheet_template, $i_Form_answersheet_header, $i_general_Format, $i_Form_answersheet_sq2, $i_Form_answersheet_sq1, $i_Form_answersheet_tf, $i_Form_answersheet_mc, $i_Form_answersheet_mo, $i_Form_answersheet_not_applicable, $i_Form_answersheet_option, $i_Form_pls_fill_in_title, $i_Form_pls_specify_type, $i_Form_pls_specify_option_num;
			
			$key_values = array(
				"IsEJ"=>($this->isEJ()?1:0),
				"StrBasicSettings"=>$Lang['eNotice']['BasicSettings'],
				"StrAllRequireToFill"=>$i_Survey_AllRequire2Fill,
				"StrDisplayQuestionNumber"=>$Lang['eNotice']['DisplayQuestionNumber'],
				"StrQuestionSetting"=>$Lang['eNotice']['ReplySlipQuestionSetting'],
				"StrReplySlip"=>$i_Notice_ReplySlip,
				"StrReplySlipQuestions"=>$Lang['eSurvey']['ReplySlipQuestions'],
				"StrTemplates"=>$i_Form_answersheet_template,
				"StrTopicTitle"=>$i_Form_answersheet_header,
				"StrFormat"=>$i_general_Format,
				"StrMustPay"=>$Lang['ePayment']['MustPayItem'],
				"StrWhetherToPay"=>$Lang['ePayment']['WhetherToPay'],
				"StrOnePayOneCat"=>$Lang['ePayment']['OnePaymentFromOneCategory'],
				"StrOnePayFewCat"=>$Lang['ePayment']['OnePaymentFromFewCategories'],
				"StrFewPayFewCat"=>$Lang['ePayment']['SeveralPaymentFromFewCategories'],
				"StrFewPayFewCatWithInputQuantity"=>$Lang['ePayment']['SeveralPaymentFromFewCategoriesWithInputQuantity'],
				"StrTextLong"=>$i_Form_answersheet_sq2,
				"StrTextShort"=>$i_Form_answersheet_sq1,
				"StrTrueOrFalse"=>$i_Form_answersheet_tf,
				"StrMCSingle"=>$i_Form_answersheet_mc,
				"StrMCMultiple"=>$i_Form_answersheet_mo,
				"StrNotApplicable"=>$i_Form_answersheet_not_applicable,
				"StrOptions"=>$i_Form_answersheet_option,
				"StrQuestionNeedToReply"=>$Lang['eNotice']['QuestionNeedToReply'],
				"StrAdd"=>$Lang['Btn']['Add'],
				"StrDelete"=>$Lang['Btn']['Delete'],
				"StrMoveQuestion"=>$Lang['eNotice']['MoveQuestion'],
				"StrPleaseFillInTitle"=>$i_Form_pls_fill_in_title,
				"StrPleaseSpecifyType"=>$i_Form_pls_specify_type,
				"StrPleaseSpecifyOptionNum"=>$i_Form_pls_specify_option_num,
				"StrPaymentItemName"=>$Lang['eNotice']['PaymentItemName'],
				"StrPaymentTitle"=>$Lang['eNotice']['PaymentTitle'],
				"StrPaymentCategory"=>$Lang['eNotice']['PaymentCategory'],
				"StrPaymentAmount"=>$Lang['eNotice']['Amount'],
				"StrPaymentDescription"=>$Lang['eNotice']['Description'],
				"StrPaymentPay"=>$Lang['eNotice']['Pay'],
				"StrPaymentNotPaying"=>$Lang['eNotice']['NotPaying'],
				"StrPaymentAccountBalance"=>$Lang['eNotice']['AccountBalance'],
				"StrNonPaymentItem"=>$Lang['eNotice']['NonPaymentItem'],
				"StrNA"=>$Lang['General']['NA'],
				"StrGoToQuestion"=>$Lang['eNotice']['SkipToQuestion'],
				"StrGoToEndOfReplySlip"=>$Lang['eNotice']['SkipToAllQuestion'],
				"StrPleaseCompleteThisQuestion"=>$Lang['eNotice']['PleaseCompleteThisQuestion'],
				"StrPleaseCompleteAllQuestions"=>$Lang['eNotice']['PleaseCompleteAllQuestions'],
				"StrAccountBalance"=>$Lang['eNotice']['AccountBalance'],
				"StrTotal"=> (isset($Lang['ePayment']['TotalAmount'])? $Lang['ePayment']['TotalAmount'] : $Lang['General']['Total']),
				"StrPleaseAnswerThisQuestion"=>$Lang['eNotice']['PleaseAnswerThisQuestion'],
				"StrPleaseAnswerAllQuestions"=>$Lang['eNotice']['PleaseAnswerAllQuestions'],
				"StrNoNeedToAnswerThisQuestion"=>$Lang['eNotice']['NoNeedToAnswerThisQuestion'],
				"StrApplySubsidySettings"=>$Lang['eNotice']['ApplySubsidySettings'],
				"StrSetSubsidyByIdentity"=>$Lang['eNotice']['SetSubsidyByIdentity'],
				"StrSetSubsidyByIndividualStudent"=>$Lang['eNotice']['SetSubsidyByIndividualStudent'],
				"StrSubsidyIdentity"=>$Lang['ePayment']['SubsidyIdentity'],
				"StrStudent"=>$Lang['Identity']['Student'],
				"StrPleaseCompleteThisSubsidySettings"=>$Lang['eNotice']['PleaseCompleteThisSubsidySettings'],
				"StrClass"=>$Lang['General']['Class'],
				"StrAmountDescription"=>$Lang['eNotice']['AmountDescription'],
				"StrPleaseInputValidQuantity"=>$Lang['eNotice']['PleaseInputValidQuantity'],
				"StrPurchaseQuantity"=>$Lang['eNotice']['PurchaseQuantity']
			);
			
			return $key_values;
		}
		
		function constructSubsidyPartString($subsidy_ary)
		{
			$str = '';
			for($i=0;$i<count($subsidy_ary);$i++)
			{
				$subsidy_obj = $subsidy_ary[$i];
				$str .= $this->SUBSIDY_SEP;
				$str .= $subsidy_obj['Enable'].','.$subsidy_obj['SetIdentity'].','.$subsidy_obj['SetStudent'].$this->SUBSIDY_PART_SEP;
				$str .= $this->IDENTITIES_SEP;
				if(count($subsidy_obj['Identities'])>0)
				{
					foreach($subsidy_obj['Identities'] as $key => $val){
						$str .= $this->IDENTITY_SEP.$key.','.$val;
					}
				}
				
				$str .= $this->SUBSIDY_PART_SEP;
				$str .= $this->STUDENTS_SEP;
				if(count($subsidy_obj['Students'])>0)
				{
					foreach($subsidy_obj['Students'] as $key => $val){
						$str .= $this->STUDENT_SEP.$key.','.$val;
					}
				}
			}
			
			return $str;
		}
		
		function constructQuestionString($questions)
		{
			$qstr = '';
			for($i=0;$i<count($questions);$i++){
				$q = $questions[$i];
				switch($q['Type'])
				{
					case QTYPE_MUST_PAY:
						//q = {"Type":type,"Title":title,"Option":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":1,"Answer":""};
						$qstr .= $this->QUE_SEP . $q['Type'] . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP . $this->OPT_SEP . $this->replaceSpecialChars($q['Option']). $this->TAR_SEP . $q['JumpToQuestion'] . $this->ITEM_SEP . $this->PAYMENTITEMID_SEP . $q['PaymentItemID'] . $this->ITEM_SEP . $this->CATEGORYID_SEP . $q['CategoryID'] . $this->ITEM_SEP . $this->AMT_SEP . $this->formatDisplayNumber($q['Amount']) . $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'] . $this->ITEM_SEP . $this->constructSubsidyPartString($q['Subsidy']);
					break;
					case QTYPE_WHETHER_TO_PAY:
						//q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":mustSubmit,"Answer":""};
						$qstr .= $this->QUE_SEP . $q['Type'] . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP . $this->OPT_SEP . $this->replaceSpecialChars($q['OptionYes']) . $this->TAR_SEP . $q['OptionYesJumpToQuestion'] . $this->OPT_SEP . $this->replaceSpecialChars($q['OptionNo']) . $this->TAR_SEP . $q['OptionNoJumpToQuestion'] . $this->ITEM_SEP . $this->PAYMENTITEMID_SEP . $q['PaymentItemID'] . $this->ITEM_SEP . $this->CATEGORYID_SEP . $q['CategoryID'] . $this->ITEM_SEP . $this->AMT_SEP . $this->formatDisplayNumber($q['Amount']) . $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'] . $this->ITEM_SEP . $this->constructSubsidyPartString($q['Subsidy']);
					break;
					case QTYPE_ONE_PAY_ONE_CAT:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Amount":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"CategoryID":"","PaymentItemID":"","Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
						$qstr .= $this->QUE_SEP . $q['Type'] . ',' . count($q['Options']) . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->OPT_SEP . $this->formatDisplayNumber($q['Options'][$j]['Amount']) . ' ' . $this->replaceSpecialChars($q['Options'][$j]['Option']) . $this->TAR_SEP . $q['Options'][$j]['JumpToQuestion'];
						}
						$qstr .= $this->ITEM_SEP . $this->PAYMENTITEMID_SEP . $q['PaymentItemID'] . $this->ITEM_SEP . $this->CATEGORYID_SEP . $q['CategoryID'] . $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'] . $this->ITEM_SEP . $this->constructSubsidyPartString($q['Subsidy']);
					break;
					case QTYPE_ONE_PAY_FEW_CAT:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
						$qstr .= $this->QUE_SEP . $q['Type'] . ',' . count($q['Options']) . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->OPT_SEP . $this->replaceSpecialChars($q['Options'][$j]['Option']) . $this->TAR_SEP . $q['Options'][$j]['JumpToQuestion'];
						}
						$qstr .= $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->PAYMENTITEMID_SEP . $q['Options'][$j]['PaymentItemID'];
						}
						$qstr .= $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->CATEGORYID_SEP . $q['Options'][$j]['CategoryID'];
						}
						$qstr .= $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->AMT_SEP . $this->formatDisplayNumber($q['Options'][$j]['Amount']);
						}
						$qstr .= $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'] . $this->ITEM_SEP . $this->constructSubsidyPartString($q['Subsidy']);
					break;
					case QTYPE_FEW_PAY_FEW_CAT:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":[]};
						$qstr .= $this->QUE_SEP . $q['Type'] . ',' . count($q['Options']) . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->OPT_SEP . $this->replaceSpecialChars($q['Options'][$j]['Option']) . $this->TAR_SEP . $q['Options'][$j]['JumpToQuestion'];
						}
						$qstr .= $this->ITEM_SEP . $this->PAYMENTITEMID_SEP;
						$comma = '';
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $comma . $q['Options'][$j]['PaymentItemID'];
							$comma = ',';
						}
						$qstr .= $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->CATEGORYID_SEP . $q['Options'][$j]['CategoryID'];
						}
						$qstr .= $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->AMT_SEP . $this->formatDisplayNumber($q['Options'][$j]['Amount']);
						}
						$qstr .= $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'] . $this->ITEM_SEP . $this->constructSubsidyPartString($q['Subsidy']);
					break;
					case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
						$qstr .= $this->QUE_SEP . $q['Type'] . ',' . count($q['Options']) . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->OPT_SEP . $this->replaceSpecialChars($q['Options'][$j]['Option']) . $this->TAR_SEP . $q['Options'][$j]['JumpToQuestion'];
						}
						$qstr .= $this->ITEM_SEP . $this->PAYMENTITEMID_SEP;
						$comma = '';
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $comma . $q['Options'][$j]['PaymentItemID'];
							$comma = ',';
						}
						$qstr .= $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->CATEGORYID_SEP . $q['Options'][$j]['CategoryID'];
						}
						$qstr .= $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->AMT_SEP . $this->formatDisplayNumber($q['Options'][$j]['Amount']);
						}
						$qstr .= $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'] . $this->ITEM_SEP . $this->constructSubsidyPartString($q['Subsidy']);
						$qstr .= $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->QUANTITY_SEP . $q['Options'][$j]['MinQuantity'] . ',' . $q['Options'][$j]['MaxQuantity'];
						}
					break;
					case QTYPE_TEXT_LONG:
						//q = {"Type":type,"Title":title,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
						$qstr .= $this->QUE_SEP . $q['Type'] . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP . $this->TAR_SEP . $q['JumpToQuestion'] . $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'];
					break;
					case QTYPE_TEXT_SHORT:
						//q = {"Type":type,"Title":title,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
						$qstr .= $this->QUE_SEP . $q['Type'] . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP . $this->TAR_SEP . $q['JumpToQuestion'] . $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'];
					break;
					case QTYPE_TRUE_OR_FALSE:
						//q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
						$qstr .= $this->QUE_SEP . $q['Type'] . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP . $this->OPT_SEP . $this->replaceSpecialChars($q['OptionYes']) . $this->TAR_SEP . $q['OptionYesJumpToQuestion'] . $this->OPT_SEP . $this->replaceSpecialChars($q['OptionNo']) . $this->TAR_SEP . $q['OptionNoJumpToQuestion'] . $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'];
					break;
					case QTYPE_MC_SINGLE:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
						$qstr .= $this->QUE_SEP . $q['Type'] . ',' . count($q['Options']) . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->OPT_SEP . $this->replaceSpecialChars($q['Options'][$j]['Option']) . $this->TAR_SEP . $q['Options'][$j]['JumpToQuestion'];
						} 
						$qstr .= $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'];
					break;
					case QTYPE_MC_MULTIPLE:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":""});
						//}
						//q = {"Type":type,"Title":title,"Options":option_ary,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":[]};
						$qstr .= $this->QUE_SEP . $q['Type'] . ',' . count($q['Options']) . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP;
						for($j=0;$j<count($q['Options']);$j++){
							$qstr .= $this->OPT_SEP . $this->replaceSpecialChars($q['Options'][$j]['Option']);
						} 
						$qstr .= $this->ITEM_SEP . $this->TAR_SEP . $q['JumpToQuestion'] . $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'];
					break;
					case QTYPE_NOT_APPLICABLE:
						//q = {"Type":type,"Title":title,"MustSubmit":0};
						$qstr .= $this->QUE_SEP . $q['Type'] . $this->ITEM_SEP . $this->replaceSpecialChars($q['Title']) . $this->ITEM_SEP . $this->MSUB_SEP . $q['MustSubmit'];
					break;
				}
			}
			
			return $qstr;
		}
		
		function parseSubsidyPartString($str)
		{
			$tmp_subsidy_ary = explode($this->SUBSIDY_SEP,$str);
			array_shift($tmp_subsidy_ary);
			
			$subsidy_ary = array();
			for($k=0;$k<count($tmp_subsidy_ary);$k++){
				$subsidy_obj = array("Enable"=>0,"SetIdentity"=>0,"SetStudent"=>0,"Identities"=>array(),"Students"=>array());
				$tmp_subsidy_parts = explode($this->SUBSIDY_PART_SEP, $tmp_subsidy_ary[$k]);
				$tmp_enable_parts = explode(",", $tmp_subsidy_parts[0]);
				$subsidy_obj["Enable"] = $tmp_enable_parts[0];
				$subsidy_obj["SetIdentity"] = $tmp_enable_parts[1];
				$subsidy_obj["SetStudent"] = $tmp_enable_parts[2];
				$identities_parts = explode($this->IDENTITIES_SEP, $tmp_subsidy_parts[1]);
				if(count($identities_parts) > 0){
					array_shift($identities_parts);
				}
				$students_parts = explode($this->STUDENTS_SEP, $tmp_subsidy_parts[2]);
				if(count($students_parts) > 0){
					array_shift($students_parts);
				}
				
				for($i=0;$i<count($identities_parts);$i++){
					$identity_ary = explode($this->IDENTITY_SEP, $identities_parts[$i]);
					if(count($identity_ary) > 0){
						array_shift($identity_ary);
						for($j=0;$j<count($identity_ary);$j++){
							$identity_id_amount = explode(",",$identity_ary[$j]);
							$tmp_id = $this->convertToEmptyStringOrInteger($identity_id_amount[0]);
							$tmp_amount = $this->formatDisplayNumber($identity_id_amount[1]);
							$subsidy_obj["Identities"][$tmp_id] = $tmp_amount;
						}
					}
				}
				
				for($i=0;$i<count($students_parts);$i++){
					$student_ary = explode($this->STUDENT_SEP, $students_parts[$i]);
					if(count($student_ary) > 0){
						array_shift($student_ary);
						for($j=0;$j<count($student_ary);$j++){
							$student_id_amount = explode(",", $student_ary[$j]);
							$tmp_id = $this->convertToEmptyStringOrInteger($student_id_amount[0]);
							$tmp_amount = $this->formatDisplayNumber($student_id_amount[1]);
							$subsidy_obj["Students"][$tmp_id] = $tmp_amount;
						}
					}
				}
				
				$subsidy_ary[] = $subsidy_obj;
			}
			
			return $subsidy_ary;
		}
		
		function parseQuestionString($qstr)
		{
			$questions_ary = array();
			
			$ques = explode($this->QUE_SEP,$qstr);
			if(count($ques) > 0){
				array_shift($ques);
			}
			for($i=0;$i<count($ques);$i++)
			{
				$parts = explode($this->ITEM_SEP,$ques[$i]);
				if(count($parts)==0){
					continue;
				}
				$type_opt_num = explode(',',$parts[0]);
				$type = $type_opt_num[0];
				switch($type)
				{
					case QTYPE_MUST_PAY:
						$q = array("Type"=>$type,"Title"=>"","Option"=>"","JumpToQuestion"=>"","CategoryID"=>"","PaymentItemID"=>"","Amount"=>0,"OriginalAmount"=>0,"MustSubmit"=>1,"Answer"=>"","Subsidy"=>array(array("Enable"=>0,"SetIdentity"=>0,"SetStudent"=>0,"Identities"=>array(),"Students"=>array())));
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						$tmp_option = explode($this->OPT_SEP, $parts[2]);
						array_shift($tmp_option);
						$opt_tar = explode($this->TAR_SEP, $tmp_option[0]);
						$q['Option'] = $this->reverseReplaceSpecialChars($opt_tar[0]);
						if(count($opt_tar) > 1){
							$q['JumpToQuestion'] = $opt_tar[1];
						}
						$tmp_paymentitemid = explode($this->PAYMENTITEMID_SEP, $parts[3]);
						array_shift($tmp_paymentitemid);
						$q['PaymentItemID'] = $this->convertToEmptyStringOrInteger($tmp_paymentitemid[0]);
						$tmp_categoryid = explode($this->CATEGORYID_SEP, $parts[4]);
						array_shift($tmp_categoryid);
						$q['CategoryID'] = $this->convertToEmptyStringOrInteger($tmp_categoryid[0]);
						$tmp_amount = explode($this->AMT_SEP,$parts[5]);
						array_shift($tmp_amount);
						$q['Amount'] = $this->formatDisplayNumber($tmp_amount[0]);
						$q['OriginalAmount'] = $q['Amount'];
						if(count($parts) > 6 && trim($parts[6]) != '')
						{
							$tmp_msub = explode($this->MSUB_SEP, $parts[6]);
							array_shift($tmp_msub);
							$q['MustSubmit'] = $tmp_msub[0];
						}
						if(count($parts) > 7 && trim($parts[7]) != '')
						{
							$q['Subsidy'] = $this->parseSubsidyPartString($parts[7]);						
						}
						$questions_ary[] = $q;
						
					break;
					case QTYPE_WHETHER_TO_PAY:
						$q = array("Type"=>$type,"Title"=>"","OptionYes"=>"","OptionYesJumpToQuestion"=>"","OptionNo"=>"","OptionNoJumpToQuestion"=>"","CategoryID"=>"","PaymentItemID"=>"","Amount"=>0,"OriginalAmount"=>0,"MustSubmit"=>1,"Answer"=>"","Subsidy"=>array(array("Enable"=>0,"SetIdentity"=>0,"SetStudent"=>0,"Identities"=>array(),"Students"=>array())));
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						$tmp_options = explode($this->OPT_SEP, $parts[2]);
						array_shift($tmp_options);
						$yes_opt_tar = explode($this->TAR_SEP, $tmp_options[0]);
						$q['OptionYes'] = $this->reverseReplaceSpecialChars($yes_opt_tar[0]);
						if(count($yes_opt_tar) > 1){
							$q['OptionYesJumpToQuestion'] = $yes_opt_tar[1];
						}
						$no_opt_tar = explode($this->TAR_SEP, $tmp_options[1]);
						$q['OptionNo'] = $this->reverseReplaceSpecialChars($no_opt_tar[0]);
						if(count($no_opt_tar) > 1){
							$q['OptionNoJumpToQuestion'] = $no_opt_tar[1];
						}
						$tmp_paymentitemid = explode($this->PAYMENTITEMID_SEP,$parts[3]);
						array_shift($tmp_paymentitemid);
						$q['PaymentItemID'] = $this->convertToEmptyStringOrInteger($tmp_paymentitemid[0]);
						$tmp_categoryid = explode($this->CATEGORYID_SEP ,$parts[4]);
						array_shift($tmp_categoryid);
						$q['CategoryID'] = $this->convertToEmptyStringOrInteger($tmp_categoryid[0]);
						$tmp_amount = explode($this->AMT_SEP ,$parts[5]);
						array_shift($tmp_amount);
						$q['Amount'] = $this->formatDisplayNumber($tmp_amount[0]);
						$q['OriginalAmount'] = $q['Amount'];
						if(count($parts) > 6 && trim($parts[6]) != '')
						{
							$tmp_msub = explode($this->MSUB_SEP, $parts[6]);
							array_shift($tmp_msub);
							$q['MustSubmit'] = $tmp_msub[0];
						}
						if(count($parts) > 7 && trim($parts[7]) != '')
						{
							$q['Subsidy'] = $this->parseSubsidyPartString($parts[7]);						
						}
						$questions_ary[] = $q;
						
					break;
					case QTYPE_ONE_PAY_ONE_CAT:
						$option_ary = array();
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Amount":"","JumpToQuestion":""});
						//}
						$q = array("Type"=>$type,"Title"=>"","CategoryID"=>"","PaymentItemID"=>"","Options"=>array(),"MustSubmit"=>1,"Answer"=>"","Subsidy"=>array());
						$num_of_opt = $type_opt_num[1];
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						$tmp_options = explode($this->OPT_SEP, $parts[2]);
						array_shift($tmp_options);
						for($j=0;$j<count($tmp_options);$j++){
							$tmp_opt_tar = explode($this->TAR_SEP, $tmp_options[$j]);
							$tmp_number_text = $this->extractNumberText($tmp_opt_tar[0]);
							$tmp_opt_obj = array("Amount"=>$this->formatDisplayNumber($tmp_number_text[0]),"OriginalAmount"=>$this->formatDisplayNumber($tmp_number_text[0]),"Option"=>trim($tmp_number_text[1]),"JumpToQuestion"=>"");
							if(count($tmp_opt_tar) > 1){
								$tmp_opt_obj['JumpToQuestion'] = $tmp_opt_tar[1];
							}
							$option_ary[] = $tmp_opt_obj;
						}
						$q['Options'] = $option_ary;
						$tmp_paymentitemid = explode($this->PAYMENTITEMID_SEP, $parts[3]);
						array_shift($tmp_paymentitemid);
						$q['PaymentItemID'] = $this->convertToEmptyStringOrInteger($tmp_paymentitemid[0]);
						$tmp_categoryid = explode($this->CATEGORYID_SEP, $parts[4]);
						array_shift($tmp_categoryid);
						$q['CategoryID'] = $this->convertToEmptyStringOrInteger($tmp_categoryid[0]);
						if(count($parts) > 5 && trim($parts[5]) != '')
						{
							$tmp_msub = explode($this->MSUB_SEP,  $parts[5]);
							array_shift($tmp_msub);
							$q['MustSubmit'] = $tmp_msub[0];
						}
						if(count($parts) > 6 && trim($parts[6]) != '')
						{
							$q['Subsidy'] = $this->parseSubsidyPartString($parts[6]);						
						}else{
							for($j=0;$j<count($q['Options']);$j++){
								$q['Subsidy'][] = array("Enable"=>0,"SetIdentity"=>0,"SetStudent"=>0,"Identities"=>array(),"Students"=>array());
							}
						}
						$questions_ary[] = $q;
						
					break;
					case QTYPE_ONE_PAY_FEW_CAT:
						$option_ary = array();
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
						//}
						$q = array("Type"=>$type,"Title"=>"","Options"=>array(),"MustSubmit"=>1,"Answer"=>"","Subsidy"=>array());
						$num_of_opt = $type_opt_num[1];
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						$tmp_options = explode($this->OPT_SEP, $parts[2]);
						array_shift($tmp_options);
						$tmp_paymentitemid = explode($this->PAYMENTITEMID_SEP ,$parts[3]);
						array_shift($tmp_paymentitemid);
						$tmp_categoryid = explode($this->CATEGORYID_SEP,$parts[4]);
						array_shift($tmp_categoryid);
						$tmp_amount = explode($this->AMT_SEP, $parts[5]);
						array_shift($tmp_amount);
						for($j=0;$j<count($tmp_options);$j++){
							$tmp_opt_tar = explode($this->TAR_SEP,$tmp_options[$j]);
							$tmp_opt_obj = array("Option"=>$this->reverseReplaceSpecialChars($tmp_opt_tar[0]),"CategoryID"=>"","PaymentItemID"=>"","Amount"=>"","OriginalAmount"=>"","JumpToQuestion"=>"");
							if(count($tmp_opt_tar) > 1){
								$tmp_opt_obj['JumpToQuestion'] = $tmp_opt_tar[1];
							}
							$tmp_opt_obj['PaymentItemID'] = $this->convertToEmptyStringOrInteger($tmp_paymentitemid[$j]);
							$tmp_opt_obj['CategoryID'] = $this->convertToEmptyStringOrInteger($tmp_categoryid[$j]);
							$tmp_opt_obj['Amount'] = $this->formatDisplayNumber($tmp_amount[$j]);
							$tmp_opt_obj['OriginalAmount'] = $tmp_opt_obj['Amount'];
							$option_ary[] = $tmp_opt_obj;
						}
						$q['Options'] = $option_ary;
						if(count($parts) > 6 && trim($parts[6]) != '')
						{
							$tmp_msub = explode($this->MSUB_SEP , $parts[6]);
							array_shift($tmp_msub);
							$q['MustSubmit'] = $tmp_msub[0];
						}
						if(count($parts) > 7 && trim($parts[7]) != '')
						{
							$q['Subsidy'] = $this->parseSubsidyPartString($parts[7]);						
						}else{
							for($j=0;$j<count($q['Options']);$j++){
								$q['Subsidy'][] = array("Enable"=>0,"SetIdentity"=>0,"SetStudent"=>0,"Identities"=>array(),"Students"=>array());
							}
						}
						$questions_ary[] = $q;
						
					break;
					case QTYPE_FEW_PAY_FEW_CAT:
					case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
						$option_ary = array();
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
						//}
						$q = array("Type"=>$type,"Title"=>"","Options"=>array(),"MustSubmit"=>1,"Answer"=>array(),"Subsidy"=>array());
						$num_of_opt = $type_opt_num[1];
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						$tmp_options = explode($this->OPT_SEP,$parts[2]);
						array_shift($tmp_options);
						$tmp_paymentitemid = explode($this->PAYMENTITEMID_SEP, $parts[3]);
						array_shift($tmp_paymentitemid);
						$tmp_paymentitemid_ary = explode(',',$tmp_paymentitemid[0]);
						$tmp_categoryid = explode($this->CATEGORYID_SEP, $parts[4]);
						array_shift($tmp_categoryid);
						$tmp_amount = explode($this->AMT_SEP,$parts[5]);
						array_shift($tmp_amount);
						for($j=0;$j<count($tmp_options);$j++){
							$tmp_opt_tar = explode($this->TAR_SEP, $tmp_options[$j]);
							$tmp_opt_obj = array("Option"=>$this->reverseReplaceSpecialChars($tmp_opt_tar[0]),"CategoryID"=>"","PaymentItemID"=>"","Amount"=>"","OriginalAmount"=>"","JumpToQuestion"=>"");
							if(count($tmp_opt_tar) > 1){
								$tmp_opt_obj['JumpToQuestion'] = $tmp_opt_tar[1];
							}
							$tmp_opt_obj['PaymentItemID'] = $this->convertToEmptyStringOrInteger($tmp_paymentitemid_ary[$j]);
							$tmp_opt_obj['CategoryID'] = $this->convertToEmptyStringOrInteger($tmp_categoryid[$j]);
							$tmp_opt_obj['Amount'] = $this->formatDisplayNumber($tmp_amount[$j]);
							$tmp_opt_obj['OriginalAmount'] = $tmp_opt_obj['Amount'];
							$option_ary[] = $tmp_opt_obj;
						}
						$q['Options'] = $option_ary;
						if(count($parts) > 6 && trim($parts[6]) != '')
						{
							$tmp_msub = explode($this->MSUB_SEP, $parts[6]);
							array_shift($tmp_msub);
							$q['MustSubmit'] = $tmp_msub[0] == '1'? '1' : '0';
						}
						if(count($parts) > 7 && trim($parts[7]) != '')
						{
							$q['Subsidy'] = $this->parseSubsidyPartString($parts[7]);						
						}else{
							for($j=0;$j<count($q['Options']);$j++){
								$q['Subsidy'][] = array("Enable"=>0,"SetIdentity"=>0,"SetStudent"=>0,"Identities"=>array(),"Students"=>array());
							}
						}
						if($type == QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY)
						{ 
							if(count($parts) > 8 && trim($parts[8]) != ''){
								$tmp_quantity = explode($this->QUANTITY_SEP, $parts[8]);
								array_shift($tmp_quantity);
								for($j=0;$j<count($q['Options']);$j++){
									$tmp_quantity_range = explode(',', $tmp_quantity[$j]);
									$q['Options'][$j]['MinQuantity'] = $tmp_quantity_range[0];
									$q['Options'][$j]['MaxQuantity'] = $tmp_quantity_range[1];
								}
							}else{
								for($j=0;$j<count($q['Options']);$j++){
									$q['Options'][$j]['MinQuantity'] = 1;
									$q['Options'][$j]['MaxQuantity'] = 100;
								}
							}
						}
						$questions_ary[] = $q;
						
					break;
					case QTYPE_TEXT_LONG:
					case QTYPE_TEXT_SHORT:
						$q = array("Type"=>$type,"Title"=>"","JumpToQuestion"=>"","MustSubmit"=>1,"Answer"=>"");
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						if(count($parts) > 2){
							$tmp_tar = explode($this->TAR_SEP, $parts[2]);
							array_shift($tmp_tar);
							$q['JumpToQuestion'] = $tmp_tar[0];
						}
						if(count($parts) > 3 && trim($parts[3]) != '')
						{
							$tmp_msub = explode($this->MSUB_SEP, $parts[3]);
							array_shift($tmp_msub);
							$q['MustSubmit'] = $tmp_msub[0];
						}
						$questions_ary[] = $q;
						
					break;
					case QTYPE_TRUE_OR_FALSE:
						$q = array("Type"=>$type,"Title"=>"","OptionYes"=>"","OptionYesJumpToQuestion"=>"","OptionNo"=>"","OptionNoJumpToQuestion"=>"","MustSubmit"=>1,"Answer"=>"");
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						$tmp_options = explode($this->OPT_SEP,$parts[2]);
						array_shift($tmp_options);
						$yes_opt_tar = explode($this->TAR_SEP, $tmp_options[0]);
						$q['OptionYes'] = $this->reverseReplaceSpecialChars($yes_opt_tar[0]);
						$q['OptionYesJumpToQuestion'] = $yes_opt_tar[1];
						$no_opt_tar = explode($this->TAR_SEP, $tmp_options[1]);
						$q['OptionNo'] = $this->reverseReplaceSpecialChars($no_opt_tar[0]);
						$q['OptionNoJumpToQuestion'] = $no_opt_tar[1];
						if(count($parts) > 3 && trim($parts[3]) != '')
						{
							$tmp_msub = explode($this->MSUB_SEP, $parts[3]);
							array_shift($tmp_msub);
							$q['MustSubmit'] = $tmp_msub[0];
						}
						$questions_ary[] = $q;
						
					break;
					case QTYPE_MC_SINGLE:
						$option_ary = array();
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","JumpToQuestion":""});
						//}
						$q = array("Type"=>$type,"Title"=>"","Options"=>array(),"MustSubmit"=>1,"Answer"=>"");
						$num_of_opt = $type_opt_num[1];
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						$tmp_options = explode($this->OPT_SEP, $parts[2]);
						array_shift($tmp_options);
						for($j=0;$j<count($tmp_options);$j++){
							$tmp_opt_tar = explode($this->TAR_SEP, $tmp_options[$j]);
							$tmp_opt_obj = array("Option"=>$this->reverseReplaceSpecialChars($tmp_opt_tar[0]),"JumpToQuestion"=>"");
							if(count($tmp_opt_tar) > 1){
								$tmp_opt_obj['JumpToQuestion'] = $tmp_opt_tar[1];
							}
							$option_ary[] = $tmp_opt_obj;
						}
						$q['Options'] = $option_ary;
						if(count($parts) > 3 && trim($parts[3]) != '')
						{
							$tmp_msub = explode($this->MSUB_SEP, $parts[3]);
							array_shift($tmp_msub);
							$q['MustSubmit'] = $tmp_msub[0];
						}
						$questions_ary[] = $q;
						
					break;
					case QTYPE_MC_MULTIPLE:
						$option_ary = array();
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":""});
						//}
						$q = array("Type"=>$type,"Title"=>"","Options"=>array(),"JumpToQuestion"=>"","MustSubmit"=>1,"Answer"=>array());
						$num_of_opt = $type_opt_num[1];
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						$tmp_options = explode($this->OPT_SEP, $parts[2]);
						array_shift($tmp_options);
						for($j=0;$j<count($tmp_options);$j++){
							$tmp_opt_obj = array("Option"=>$this->reverseReplaceSpecialChars($tmp_options[$j]));
							$option_ary[] = $tmp_opt_obj;
						}
						$q['Options'] = $option_ary;
						if(count($parts) > 3){
							$tmp_tar = explode($this->TAR_SEP, $parts[3]);
							array_shift($tmp_tar);
							$q['JumpToQuestion'] = $tmp_tar[0];
						}
						if(count($parts) > 4 && trim($parts[4]) != '')
						{
							$tmp_msub = explode($this->MSUB_SEP, $parts[4]);
							array_shift($tmp_msub);
							$q['MustSubmit'] = $tmp_msub[0];
						}
						$questions_ary[] = $q;
						
					break;
					case QTYPE_NOT_APPLICABLE:
						$q = array("Type"=>$type,"Title"=>"","MustSubmit"=>0);
						$q['Title'] = $this->reverseReplaceSpecialChars($parts[1]);
						
						$questions_ary[] = $q;
						
					break;				
				}
			}
			
			return $questions_ary;
			
		}
		
		function preparePaymentItemsForQuestions($questions, $noticeId, $startDate, $endDate, $extraKeyValues)
		{
			$lpayment = new libpayment();
			$final_questions = array();
			for($i=0;$i<count($questions);$i++){
				$q = $questions[$i];
				switch($q['Type'])
				{
					case QTYPE_MUST_PAY:
						//q = {"Type":type,"Title":title,"Option":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":1,"Answer":""};
					case QTYPE_WHETHER_TO_PAY:
						//q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":mustSubmit,"Answer":""};
					case QTYPE_ONE_PAY_ONE_CAT:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Amount":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"CategoryID":"","PaymentItemID":"","Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
						$missing_record = false;
						if($q['PaymentItemID'] != ''){
							$sql = "SELECT * FROM PAYMENT_PAYMENT_ITEM WHERE ItemID='".$q['PaymentItemID']."'";
							$records = $lpayment->returnResultSet($sql);
							$missing_record = count($records)==0;
						}
						if($q['PaymentItemID'] == '' || $missing_record){
							$item_name = $q['Title'];
							$category_id = $q['CategoryID'];
							$new_payment_item_id = $lpayment->Create_Notice_Payment_Item($item_name, $startDate, $endDate, $noticeId, $category_id, $extraKeyValues);
							if($new_payment_item_id){
								$q['PaymentItemID'] = $new_payment_item_id;
							}
						}else if($q['PaymentItemID'] != ''){
							$item_name = $q['Title'];
							$category_id = $q['CategoryID'];
							$lpayment->Edit_Notice_Payment_Item($q['PaymentItemID'], $item_name, $startDate, $endDate, $category_id, $extraKeyValues);
						}
						$final_questions[] = $q;
					break;
					case QTYPE_ONE_PAY_FEW_CAT:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
					case QTYPE_FEW_PAY_FEW_CAT:
					case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":[]};
						for($j=0;$j<count($q['Options']);$j++){
							$missing_record = false;
							if($q['Options'][$j]['PaymentItemID'] != ''){
								$sql = "SELECT * FROM PAYMENT_PAYMENT_ITEM WHERE ItemID='".$q['Options'][$j]['PaymentItemID']."'";
								$records = $lpayment->returnResultSet($sql);
								$missing_record = count($records)==0;
							}
							if($q['Options'][$j]['PaymentItemID'] == '' || $missing_record){
								$item_name = $q['Options'][$j]['Option'];
								$category_id = $q['Options'][$j]['CategoryID'];
								$new_payment_item_id = $lpayment->Create_Notice_Payment_Item($item_name, $startDate, $endDate, $noticeId, $category_id, $extraKeyValues);
								if($new_payment_item_id){
									$q['Options'][$j]['PaymentItemID'] = $new_payment_item_id;
								}
							}else if($q['Options'][$j]['PaymentItemID'] != ''){
								$item_name = $q['Options'][$j]['Option'];
								$category_id = $q['Options'][$j]['CategoryID'];
								$lpayment->Edit_Notice_Payment_Item($q['Options'][$j]['PaymentItemID'], $item_name, $startDate, $endDate, $category_id, $extraKeyValues);
							}
						}
						$final_questions[] = $q;
					break;
					default:
						$final_questions[] = $q;
					break;
				}
			}
			
			return $final_questions;
		}
		
		function removePaymentItemsForQuestion($questions)
		{
			$lpayment = new libpayment();
			$final_questions = array();
			for($i=0;$i<count($questions);$i++){
				$q = $questions[$i];
				switch($q['Type'])
				{
					case QTYPE_MUST_PAY:
						//q = {"Type":type,"Title":title,"Option":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":1,"Answer":""};
					case QTYPE_WHETHER_TO_PAY:
						//q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":mustSubmit,"Answer":""};
					case QTYPE_ONE_PAY_ONE_CAT:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Amount":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"CategoryID":"","PaymentItemID":"","Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
						if($q['PaymentItemID'] != ''){
							$lpayment->Remove_Notice_Payment_Item($q['PaymentItemID']);
							$q['PaymentItemID'] = '';
						}
						
						$final_questions[] = $q;
					break;
					case QTYPE_ONE_PAY_FEW_CAT:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
					case QTYPE_FEW_PAY_FEW_CAT:
					case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
						//var option_ary = [];
						//for(var i=0;i<optionNo;i++){
						//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
						//}
						//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":[]};
						for($j=0;$j<count($q['Options']);$j++){
							if($q['Options'][$j]['PaymentItemID'] != ''){
								$lpayment->Remove_Notice_Payment_Item($q['Options'][$j]['PaymentItemID']);
								$q['Options'][$j]['PaymentItemID'] = '';
							}
						}
						$final_questions[] = $q;
					break;
					default:
						$final_questions[] = $q;
					break;
				}
			}
			
			return $final_questions;
		}
		
		function applySubsidySettingsToQuestions($questions,$subsidy_student_id,$subsidy_identity_id)
		{
			$final_questions = array();
			if($subsidy_student_id != '' || $subsidy_identity_id != '')
			{
				for($i=0;$i<count($questions);$i++){
					$q = $questions[$i];
					if(isset($q['Subsidy'])){
						switch($q['Type'])
						{
							case QTYPE_MUST_PAY:
							case QTYPE_WHETHER_TO_PAY:
								if($q['Subsidy'][0]['Enable'] == 1){
									$applied = false;
									if($q['Subsidy'][0]['SetStudent'] == 1){
										if($subsidy_student_id != '' && isset($q['Subsidy'][0]['Students'][$subsidy_student_id]))
										{
											$q['Amount'] = $q['Subsidy'][0]['Students'][$subsidy_student_id];
											$applied = true;
										}
									}
									if(!$applied && $q['Subsidy'][0]['SetIdentity'] == 1){
										if($subsidy_identity_id != '' && isset($q['Subsidy'][0]['Identities'][$subsidy_identity_id]))
										{
											$q['Amount'] = $q['Subsidy'][0]['Identities'][$subsidy_identity_id];
											$applied = true;
										}
									}
								}
							break;
							case QTYPE_ONE_PAY_ONE_CAT:
							case QTYPE_ONE_PAY_FEW_CAT:
							case QTYPE_FEW_PAY_FEW_CAT:
							case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
								for($j=0;$j<count($q['Options']);$j++){
									if($q['Subsidy'][$j]['Enable'] == 1){
										$applied = false;
										if($q['Subsidy'][$j]['SetStudent'] == 1){
											if($subsidy_student_id != '' && isset($q['Subsidy'][$j]['Students'][$subsidy_student_id])){
												$q['Options'][$j]['Amount'] = $q['Subsidy'][$j]['Students'][$subsidy_student_id];
												$applied = true;
											}
										}
										if(!$applied && $q['Subsidy'][$j]['SetIdentity'] == 1){
											if($subsidy_identity_id != '' && isset($q['Subsidy'][$j]['Identities'][$subsidy_identity_id])){
												$q['Options'][$j]['Amount'] = $q['Subsidy'][$j]['Identities'][$subsidy_identity_id];
												$applied = true;
											}
										}
									}
								}
							break;
						}
					}
					$final_questions[] = $q;
				}
			}else{
				$final_questions = $questions;
			}
			
			return $final_questions;
		}
		
		function constructAnswerString($questions)
		{
			$astr = '';
			for($i=0;$i<count($questions);$i++){
				$q = $questions[$i];
				switch($q['Type'])
				{
					case QTYPE_MUST_PAY:
					case QTYPE_WHETHER_TO_PAY:
					case QTYPE_ONE_PAY_ONE_CAT:
					case QTYPE_ONE_PAY_FEW_CAT:
					case QTYPE_TEXT_LONG:
					case QTYPE_TEXT_SHORT:
					case QTYPE_TRUE_OR_FALSE:
					case QTYPE_MC_SINGLE:
					case QTYPE_NOT_APPLICABLE:
						$astr .= $this->ANS_SEP.trim($this->replaceSpecialChars($q['Answer']));
					break;
					case QTYPE_FEW_PAY_FEW_CAT:
					case QTYPE_MC_MULTIPLE:
						$astr .= $this->ANS_SEP.trim($this->replaceSpecialChars(implode(',',$q['Answer'])));
					break;
					case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
						$astr .= $this->ANS_SEP;
						$ans_opt_sep = '';
						for($j=0;$j<count($q['Answer']);$j++){
							$astr .= $ans_opt_sep.$q['Answer'][$j]['Option'].'_'.$q['Answer'][$j]['Quantity'];
							$ans_opt_sep = ',';
						}
					break;
				}
			}
			
			return $astr;
		}
		
		function parseAnswerString($astr, $questions)
		{
			$return_questions = array();
			$answers = explode($this->ANS_SEP, $astr);
			if(count($answers) > 0){
				array_shift($answers);
			}
			for($i=0;$i<count($questions);$i++){
				$q = $questions[$i];
				
				switch($q['Type'])
				{
					case QTYPE_MUST_PAY:
					case QTYPE_WHETHER_TO_PAY:
					case QTYPE_ONE_PAY_ONE_CAT:
					case QTYPE_ONE_PAY_FEW_CAT:
					case QTYPE_TEXT_LONG:
					case QTYPE_TEXT_SHORT:
					case QTYPE_TRUE_OR_FALSE:
					case QTYPE_MC_SINGLE:
					case QTYPE_NOT_APPLICABLE:
						if(isset($answers[$i])){
							$q['Answer'] = trim($this->reverseReplaceSpecialChars($answers[$i]));
						}
					break;
					case QTYPE_FEW_PAY_FEW_CAT:
					case QTYPE_MC_MULTIPLE:
						if(trim($answers[$i])!=''){
							$q['Answer'] = explode(',',$answers[$i]);
						}
					break;
					case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
						$q['Answer'] = array();
						$ans_opt_ary = explode(',',$answers[$i]);
						for($j=0;$j<count($ans_opt_ary);$j++){
							$ans_opt_ary2 = explode('_', $ans_opt_ary[$j]);
							$q['Answer'][] = array("Option"=>$ans_opt_ary2[0],"Quantity"=>$this->convertToInteger($ans_opt_ary2[1]));
						}
					break;
				}
				$return_questions[] = $q;
			}
			
			return $return_questions;
		}
		
		function getAllPaymentItemID($questionString)
		{
			$questions = $this->parseQuestionString($questionString);
			$payment_item_ids = array();
			for($i=0;$i<count($questions);$i++){
				
				if(isset($questions[$i]['PaymentItemID']) && $questions[$i]['PaymentItemID'] != ''){
					$payment_item_ids[] = $questions[$i]['PaymentItemID'];
				}else if(isset($questions[$i]['Options']) && is_array($questions[$i]['Options'])){
					for($j=0;$j<count($questions[$i]['Options']);$j++){
						if(isset($questions[$i]['Options'][$j]['PaymentItemID']) &&  $questions[$i]['Options'][$j]['PaymentItemID']!=''){
							$payment_item_ids[] = $questions[$i]['Options'][$j]['PaymentItemID'];
						}
					}
				}
			}
			return $payment_item_ids;
		}
		
		function printStatisticsView($questionString, $answerStrings, $displayQuestionNumber=true, $isPreview=false)
		{
			global $i_Notice_NumberOfSigned;
			
			$questions = $this->parseQuestionString($questionString);
			$no_of_questions = count($questions);
			$no_of_signed_replies = count($answerStrings);
			
			$stat = array();
			
			// initialize the statistics array
			for($j=0;$j<$no_of_questions;$j++)
			{
				$q = $questions[$j];
				$stat[$j] = array('Type'=>$q['Type'],'Title'=>$q['Title'],'Options'=>array(),'OptionsCount'=>array());
				switch($q['Type'])
				{
					case QTYPE_MUST_PAY:
						$stat[$j]['Options'][] = $q['Option'];
						$stat[$j]['OptionsCount'][] = 0;
						break;
					case QTYPE_WHETHER_TO_PAY:
					case QTYPE_TRUE_OR_FALSE:
						$stat[$j]['Options'][] = $q['OptionYes'];
						$stat[$j]['Options'][] = $q['OptionNo'];
						$stat[$j]['OptionsCount'][] = 0;
						$stat[$j]['OptionsCount'][] = 0;
						break;
					case QTYPE_ONE_PAY_ONE_CAT:
						foreach($q['Options'] as $opt){
							$stat[$j]['Options'][] = '$'.$opt['Amount'];
							$stat[$j]['OptionsCount'][] = 0;
						}
						break;
					case QTYPE_ONE_PAY_FEW_CAT:
					case QTYPE_FEW_PAY_FEW_CAT:
					case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
					case QTYPE_MC_SINGLE:
					case QTYPE_MC_MULTIPLE:
						foreach($q['Options'] as $opt){
							$stat[$j]['Options'][] = $opt['Option'];
							$stat[$j]['OptionsCount'][] = 0;
						}
						break;
					case QTYPE_TEXT_LONG:
					case QTYPE_TEXT_SHORT:
						break;
					case QTYPE_NOT_APPLICABLE:
						
						break;
				}
			}
			//debug_pr($stat);
			// count answers
			for($i=0;$i<$no_of_signed_replies;$i++)
			{
				$questions_with_answer = $this->parseAnswerString($answerStrings[$i],$questions);
				for($j=0;$j<$no_of_questions;$j++)
				{
					$q = $questions_with_answer[$j];
					switch($q['Type'])
					{
						case QTYPE_MUST_PAY:
						case QTYPE_WHETHER_TO_PAY:
						case QTYPE_ONE_PAY_ONE_CAT:
						case QTYPE_ONE_PAY_FEW_CAT:
						case QTYPE_TRUE_OR_FALSE:
						case QTYPE_MC_SINGLE:
							if($q['Answer'] != ''){
								$ans = strval($q['Answer']);
								$stat[$j]['OptionsCount'][$ans] += 1;
							}
						break;
						case QTYPE_FEW_PAY_FEW_CAT:
						case QTYPE_MC_MULTIPLE:
							foreach($q['Answer'] as $ans){
								$ans = strval($ans);
								$stat[$j]['OptionsCount'][$ans] += 1;
							}
						break;
						case QTYPE_TEXT_LONG:
						case QTYPE_TEXT_SHORT:
							$ans = trim($q['Answer']);
							if($ans != '')
							{
								if(!isset($stat[$j]['Options'][$ans])){
									$stat[$j]['Options'][$ans] = $ans;
								}
								if(!isset($stat[$j]['OptionsCount'][$ans])){
									$stat[$j]['OptionsCount'][$ans] = 0;
								}
								$stat[$j]['OptionsCount'][$ans] += 1;
							}
						break;
						case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
							foreach($q['Answer'] as $ans){
								$ans_opt = strval($ans['Option']);
								$stat[$j]['OptionsCount'][$ans_opt] += 1;
							}
						break;
						case QTYPE_NOT_APPLICABLE:
							
						break;		
					}
				}
			}
			
			//debug_pr($stat);
			$table_width = $isPreview? '90%' : '96%';
			$cellspacing = $isPreview? '0' : '2';
			$print_title_tags = $isPreview? ' class="eSportprinttitle" ': ' class="eNoticereplytitle" ';
			$table_border_tags = $isPreview? ' class="eSporttableborder" ' : '';
			$title_tags = $isPreview? ' class="eSporttdborder eSportprinttabletitle" ' : ' class="tabletext" ';
			$text_tags = $isPreview? ' class="eSporttdborder eSportprinttext" ' : ' bgcolor="#FFFFFF" class="tabletext" ';
			$td_bgcolor = $isPreview? '' : ' bgcolor="#EFEFEF" ';
			
			// print out the questions and stat
			$x = '<table width="'.$table_width.'" border="0" cellspacing="0" cellpadding="5" align="center">'."\n";
			$x.= 	'<tbody>'."\n";
			$x.= 		'<tr>'."\n";
			$x.=			'<td align="left" '.$print_title_tags.'>'.$i_Notice_NumberOfSigned.' : '.$no_of_signed_replies.'</td>'."\n";
			$x.=		'</tr>'."\n";
			$x.=	'</tbody>'."\n";
			$x.= '</table>'."\n";
			
			$x .= '<table width="'.$table_width.'" border="0" cellpadding="0" cellspacing="5" align="center">'."\n";
			$x .= 	'<tbody>'."\n";
			for($i=0;$i<count($stat);$i++){
				$type = $stat[$i]['Type'];
				
				$x .= 	'<tr>'."\n";
				$x .=		'<td align="center" '.$td_bgcolor.'>'."\n";
				$x .=			'<table width="100%" border="0" cellspacing="'.$cellspacing.'" cellpadding="2" '.$table_border_tags.'>'."\n";
				$x .= 				'<tbody>'."\n";
				$x .=					'<tr>'."\n";
				$x .=						'<td '.$title_tags.' colspan="2">'.($displayQuestionNumber?($i+1).'. ':'').intranet_htmlspecialchars($stat[$i]['Title']).'</td>'."\n";
				$x .=					'</tr>'."\n";
				if(!$isPreview){
					$x .=				'<tr>'."\n";
					$x .= 					'<td align="center" colspan="2" >'."\n";
					$x .=						'<table width="100%" border="0" cellspacing="2" cellpadding="2" '.$title_tags.'>'."\n";
					$x .=							'<tbody>'."\n";
				}
				if(in_array($type,array(QTYPE_TEXT_LONG,QTYPE_TEXT_SHORT))){
					foreach($stat[$i]['Options'] as $ans){
						$percent = sprintf("%d", ($stat[$i]['OptionsCount'][$ans] / $no_of_signed_replies) * 100);
						$x .=							'<tr>'."\n";
						$x .=								'<td width="80%" '.$text_tags.'>'.intranet_htmlspecialchars($stat[$i]['Options'][$ans]).'</td>'."\n";
						$x .=								'<td nowrap="" '.$text_tags.'>'.$stat[$i]['OptionsCount'][$ans].' ('.$percent.'%)</td>'."\n";
						$x .=							'</tr>'."\n";
					}
				}else{
					for($j=0;$j<count($stat[$i]['Options']);$j++){
						$percent = sprintf("%d", ($stat[$i]['OptionsCount'][$j] / $no_of_signed_replies) * 100);
						$x .=							'<tr>'."\n";
						$x .=								'<td width="80%" '.$text_tags.'>'.$stat[$i]['Options'][$j].'</td>'."\n";
						$x .=								'<td nowrap="" '.$text_tags.'>'.$stat[$i]['OptionsCount'][$j].' ('.$percent.'%)</td>'."\n";
						$x .=							'</tr>'."\n";
					}
				}
				if(!$isPreview){
					$x .=							'</tbody>'."\n";
					$x .=						'</table>'."\n";
					$x .=					'</td>'."\n";
					$x .=				'</tr>'."\n";
				}
				$x .=				'</tbody>'."\n";
				$x .=			'</table>'."\n";
				$x .=		'</td>'."\n";
				$x .=	'</tr>'."\n";
			}
			$x .= 	'</tbody>'."\n";
			$x .= '</table>'."\n";
			
			return $x;
		}
	}

}
?>