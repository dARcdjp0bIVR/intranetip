<?php
// Using: 
/*
##### Modify at 20040908 to support qmail
##### Combined at 20050112 to support both sendmail and qmail
##### Modification Log
## 2020-04-27 (Tommy) - modified sendModuleMail(), add (') to "filter only send to active users" sql
## 2018-06-26 (Carlos) - modified sendMail() only send plain text message for iMail on mobile environment.
## 2017-09-27 (Carlos) - modified retrieve_message(), for plain text message replace < and > to &lt; and &gt; in order to avoid content text wrapped in html tags.
##						 added flag $sys_custom['iMail_DisableUrlTransformForPlainTextMessage'] to do not transform url into <a></a> tag with regular expression.
## 2017-03-06 (Carlos) - modified GET_MODULE_OBJ_ARR(), use new cached counting new mail methods to get number of new/unread emails for each folder.
##					   - modified sendModuleMail(), sendModuleMail2(), insertCampusMail(), update new mail counter for recipients.
## 2017-01-13 (Carlos) - added getPartedAttachmentFileName($part) for create_partArray($obj, $partno) to get attachment parted filename.
## 2016-04-14 (Henry)  - modified function sendMail() support $isHTMLMessage on iPad and Android for sending kis eAdmission Email
## 2016-02-29 (Cameron)- add parameter $showSenderAsAdmin to function sendMail() and sendModuleMail()
## 2016-02-03 (Carlos) - added getNextFreeUserFolder($user_id, $return_full_path=false), modified SaveOriginal(), SaveRawBody(), getNewAttachmentPath(),
##						 to check and create next usable user folder when current user folder has reach the maximum number of dir and file limitation.
## 2015-12-03 (Cameron) - set sender_id to 0 if $UseAdminEmailAsSender is set in function sendModuleMail() (case #E89634)
## 2015-12-01 (Carlos) - modified grab_alt(), added multipart subtype related handling.
## 2015-11-30 (Carlos) - modified get_raw_message() and retrieve_message(), replace ereg_replace() with preg_replace().
## 2015-11-19 (Carlos) - modified sendModuleMail(), filter only send to active users.
## 2015-06-03 (Carlos) - modified sendModuleMail(), added global flag $globalDoNotSendToAlternativeEmail to disable sending to user alternative emails.
## 2015-04-30 (Carlos) - modified sendModuleMail(), log recipients.
## 2015-04-17 (Omas) - modified sendMail() to log mime
## 2015-03-17 (Omas) - modified sendModuleMail(), sendModuleMail2() parameter $addDoNotReply add DoNotReply at the end of the message
## 2015-02-02 (Carlos) - added osapi moveMails() to move webmail Junk emails to INBOX
## 2015-01-30 (Carlos) - added changeDomainSpamScore() for postfix email type
## 2015-01-15 (Carlos) - added goToFolder(), getInboxName(), getJunkBoxName() for /home/imail/checkmail_process2.php to get INBOX.Junk emails
## 2014-05-20 (Carlos) - modified retrieve_message(), replace <html> and </html> tags, work around the problem of converting Outlook CP936 charset/encoding to UTF-8
## 2014-03-13 (Carlos) - modified sendModuleMail() and sendModuleMail2(), added custom flag $sys_custom['SendModuleMailUseAlternativeEmail'] which will only send to users alternative emails
## 2014-01-07 (Carlos) - added removeMailByUID($uid)
##					   - modified checkMail2() added UID to return array
## 2013-07-30 (Carlos)
## - modified grab_alt() - added mixed subtype
## 2013-06-27 (Carlos)
## - add logModuleMail(), modified sendModuleMail() and sendModuleMail2() to do log when $sys_custom['SendModuleMailTraceLog'] is on
## 2013-05-24 (Carlos)
## - modified sendMail(), add parameter $nl2br for convert new line to <br> in html message. Send module mail generally needs it. iMail does not need it.
## 2013-05-13 (Yuen)
## - add function set_charset() to preset the charset in some cases like eLibrary plus
## 2013-04-23 (Carlos)
##	- modified retrieve_message(), convert charset BIG5-HKSCS to UTF-8 by iconv()
## 2013-04-18 (Carlos)
##	- modified checkMail2() and places call imap_mime_header_decode(), do not convert text for default charset
##	- modified retrieve_message(), use iconv() to conert Windows-1252 to UTF-8
## 2013-03-11 (YatWoon)
##	- update GetUserEmailAddress(), add $returnEmailOnly parameter, only return sender email [Case#2013-0304-1518-38093]
## 2013-03-01 (Carlos)
##	- added formatDatetimeString() to format datetime string to ensure that strtotime() can handle it
## 2012-09-27 (Carlos)
##	- modified sendModuleMail(), added parameter $SendTo for CC to parent
## 2012-09-17 (Carlos)
##	- added SaveRawBody() for saving complete raw mail message source to file, RemoveRawBodyLogFile() for removing it
## 2012-07-16 (Carlos)
## - modified sendMail() text/html message part added nl2br($message) to preserve line break
## 2011-10-20 (Carlos)
## - added returnNumOfAllTeachingStaff() - count number of alumni for select recipient
## 2011-07-11 (Carlos)
## - modified checkMail2(), retrieve_message() improved charset conversion for Subject and Message body
## 2011-06-14 (Carlos)
## - modified checkOneMail(),checkMail2() replace datetime string with timezone UT to UTC cuz strtotime() cannot convert it correctly
## 2011-06-02 Carlos
## - modified sendMail() - change mime line break from \r\n to chr(10) (use \r\n sometimes Outlook fail to read mail)
## 2011-06-01 Carlos
## - modified sendMail() add header Date
## 2011-05-20 Carlos
## - added sendModuleMail2() and insertCampusMail()
##
## 2011-05-17 Carlos
## - modified sendModuleMail() to enable (1)send internet mail with attachment;(2)direct insert campusmail with attachment.
##
## 2011-04-27 Carlos
## - modified GET_MODULE_OBJ_ARR() change module title to iMail Archive if iMail 1.2 is in archive mode.
## 2011-04-07 Carlos
## - added Get_Archive_Mail_Message_Layer()
## 2011-04-04 Carlos
## - modified sendModuleMail(), if using iMail Gamma, users not set INTRANET_SYSTEM_ACCESS IN (1,3) will send to internal personal email address
##
## 2011-03-21 Carlos
## - open Search menu for iMail Archive mode in GET_MODULE_OBJ_ARR()
##
## 2011-03-15 Carlos
## - added setDBTotalQuota() for fast update total quota in DB
##
## 2011-02-11 Carlos
## - function sendModuleMail(), fix SendMailByBcc
##
## 2011-02-01 Carlos
## - function retrieve_message() added checking on message charset MS950 to convert it as compatible charset BIG5 to UTF-8
##
## 2010-12-07 Ronald
## - modified function checkMail2(), disable imap_rfc822_parse_adrlist() for the Sender email address
##
## 2010-11-18 Ronald
## - modified create_partArray(), now will use "dparameters" to get the attachment first if both "ifdparameters" & "ifparameters" are true
##
## 2010-10-12 Ronald
## - modified GET_MODULE_OBJ_ARR(), modified the Query used to count no. of mail in draft folder, now will also count those marked a read
##
## 2010-10-12 Ronald
## - modified create_partArray(), add a code to handle the attachment if it only have "ifdparameters"
##
## 2010-09-28 Ronald
## - Modified sendModuleMail, if there are too many reipients(compare the size of recipients with $iMail_NumOfRecipientPerTime),
##		then system will break the recipients array in to serveral array and then send out the mail.
##
## 2010-09-06 Marcus
## - Modified sendModuleMail, check UserType permission, if user type was not allowed to use gamma, email will be sent to UserEmail.
##
## 2010-08-27 Carlos
## - Modified sendModuleMail, check if $SYS_CONFIG['Mail']['use_old_sendModuleMail'] on, send mail to UserMail
##
## 2010-08-18 Max (201008111611)
## - Modified sendMail to accept parameter $isMulti to set multi-part message
##
## 2010-07-15 Marcus
## - modified sendModuleMail , also check $SYS_CONFIG['Mail']['use_old_sendModuleMail'] for dev
##
## 2010-06-22 Marcus
## - modified GET_MODULE_OBJ_ARR(), check $SYS_CONFIG['Mail']['hide_imail'] , if true , hide Compose Mail, Draft, Settings
##
## 2010-05-27 (Michael Cheung)
## - retrieve_message() $myBod = iconv with '//TRANSLIT' added
##
## 2010-05-24 (Ronald Yeung)
## - create_partArray() : modified this function, use imap_mime_header_decode to decode the filename
##
## 2010-01-25 (Michael Cheung)
## - checkMail2(), retrieve_message() : charset probe
##
## 2010-01-08 (Ronald Yeung):
## - sendModuleMail() : a new function, used to send mail to user's external mail address(XXX@SchoolDomain),
##						will be used by eclass module, e.g eNotice, SchoolNews, etc.
##
## 2009-12-11 (Ronald Yeung):
## - isExternalMailAvaliable(): New function to check if the external mail is avaliable
##
## 2009-07-25 (Ronald Yeung):
## - sendMail(): Use "utf-8" as default charset, $charset = "utf-8";
## - sendMail(): Modify the $subject so that the subject can send out in utf-8 format, $subject="=?UTF-8?B?".base64_encode($subject)."?=\n";
##
## 2008-08-01 (Ronald Yeung):
## - sendMail(): add str_replace() to remove "\r" in the mail header as some mail server cannot read "\r\n"
##
## 2008-04-25 (Kenneth Wong):
## - Work on new email server with SPAM controls
##
## 2007-08-27 (Peter Ho):
## - checkMail2(): check quota
##
## 2007-03-30 (Kenneth Wong):
## - checkMail2(): use imap_fetchheader() & imap_rfc822_parse_headers(), rather than imap_header()
##
## 2005-10-06 (Kenneth Wong):
## - Checked compliant to qmail (in option settings)
##
## 2005-08-19 (Kenneth Wong):
## - rewrite mail retrieve functions checkMail2()
##
## 2005-07-18 (Kenneth Wong):
## - sendmail(): to enable sending of HTML mails
##
*/
if (!defined("LIBWEBMAIL_DEFINED"))                     // Preprocessor directive
{
define("LIBWEBMAIL_DEFINED", true);
define("DEFAULT_INBOX_SUBJECT_FIELD_WIDTH", 50);

include_once("$intranet_root/includes/libosaccount.php");
//include_once("$intranet_root/includes/libwebmail.php");

//For checking sendmail
include_once("$intranet_root/includes/libdb.php");
include_once("$intranet_root/includes/libaccess.php");

class libwebmail extends libosaccount
{

      var $has_webmail;
      var $type;
      var $host;
      var $mailaddr_domain;
      var $mailSystemAction;
      var $message_format;
      var $mail_server_port;

      var $inbox;

      var $embed_part_ids;

      var $times;


      var $parsed_part;          # Variable for parse_message
      var $message_charset;      # Variable for retrieve_message
      var $is_html_message;      # Variable for storing the flag of HTML message
      var $mime_type;
      var $mime_encoding;

      var $mail_api_url;         # Separated mail server API

      var $charset;


      function libwebmail()
      {
               $this->libosaccount();

               $this->charset = "";

               global $plugin,$webmail_SystemType,$webmail_info;

               if (isset($plugin['webmail']) && $plugin['webmail'])
               {
                   $this->has_webmail = $plugin['webmail'];
                   $this->type = ($webmail_SystemType!=""? $webmail_SystemType:1);

                   if ($this->type == 3)
                   {
                       if ($webmail_info['host'] == "")
                       {
                           $this->host = $HTTP_SERVER_VARS['SERVER_ADDR'];
                       }
                       else
                       {
                           $this->host = $webmail_info['host'];
                       }
                       $this->setRemoteAPIHost($this->host,$webmail_info['api_port']);
                       $this->mail_server_port = $webmail_info['mail_port'];
                       $this->actype = $webmail_info['actype'];
                       //type array
                       $this->mime_type = array("text", "multipart", "message", "application", "audio", "image", "video", "other");
                       // message encodings
                       $this->mime_encoding = array("7bit", "8bit", "binary", "base64", "quoted-printable", "other", "uuencode");
                       $this->mailaddr_domain = $webmail_info['mailhost'];

                       $this->login_type = ($webmail_info['login_type']!=""?$webmail_info['login_type']:"");  // Parent attribute

                       if ($this->login_type == "email")
                       {
                           $this->login_domain = $this->mailaddr_domain;
                       }
                       	if($this->actype == "postfix")
						{
							$this->mail_api_url['GetAliasInfo'] = "/osapi/getAliasInfo.php";
                       		$this->mail_api_url['GetAliasList'] = "/osapi/getAliasList.php";
                       		$this->mail_api_url['AddAlias'] = "/osapi/addAlias.php";
                       		$this->mail_api_url['RemoveAlias'] = "/osapi/removeAlias.php";

                       		$this->mail_api_url['AddAutoReply'] = "/osapi/addAutoreply.php";
                       		$this->mail_api_url['ReadAutoReply'] = "/osapi/readAutoreply.php";
                           	$this->mail_api_url['RemoveAutoReply'] = "/osapi/removeAutoreply.php";

                       		if ($webmail_info['bl_spam'])
                       		{
								$this->mail_api_url['AddBlackList'] = "/osapi/addBlackList.php";
								$this->mail_api_url['ReadBlackList'] = "/osapi/readBlackList.php";
								$this->mail_api_url['RemoveBlackListRecord'] = "/osapi/removeBlackListRecord.php";

								$this->mail_api_url['AddWhiteList'] = "/osapi/addWhiteList.php";
								$this->mail_api_url['ReadWhiteList'] = "/osapi/readWhiteList.php";
								$this->mail_api_url['RemoveWhiteList'] = "/osapi/removeWhiteListRecord.php";

								$this->mail_api_url['BypassSpamCheck'] = "/osapi/bypassSpamCheck.php";
								$this->mail_api_url['BypassVirusCheck'] = "/osapi/bypassVirusCheck.php";
								$this->mail_api_url['ReadSpamCheck'] = "/osapi/readSpamStatus.php";
								$this->mail_api_url['ReadVirusCheck'] = "/osapi/readVirusStatus.php";

								$this->mail_api_url['ChangeCustomPolicyScore'] = "/osapi/changeCustomPolicyScore.php";
								$this->mail_api_url['ReadSpamScore'] = "/osapi/getSpamScore.php";

								$this->mail_api_url['changeAllUserPolicy'] = "/osapi/changeAllUserPolicy.php";
								$this->mail_api_url['ChangeDomainSpamScore'] = "/osapi/changeDomainSpamScore.php";
								$this->mail_api_url['moveMails'] = "/osapi/moveMails.php";
							}
                       }

                   }
                   else
                   {
                       $this->mailSystemAction = $webmail_info['url'];
                   }
                   $this->message_format = 'html';
               }
               else
               {
                   $this->has_webmail = false;
               }
      }

      function set_charset($charset)
      {
			$this->charset = $charset;
      }

      function open_account($UserLogin, $UserPassword, $quota="")
      {
               global $webmail_info;
               if ($this->type==1)
               {
                   # Open webmail add user code
                   $s1 = exec("sudo /etc/api/api 1 ".OsCommandSafe($UserLogin));
                   $s2 = exec("sudo /etc/api/api 3 ".OsCommandSafe($UserLogin)." ".escapeshellarg($UserPassword));
                   $s3 = exec("sudo /etc/api/550makemap.pl");
                   return true;
               }
               else if ($this->type==3)
               {
                    return $this->openAccount($UserLogin,$UserPassword,"iMail");       # Call parent method
               }
               else return true;
      }

      function delete_account ($UserLogin)
      {
               if ($this->type==1)
               {
                   # Open webmail remove user code
                   exec("sudo /etc/api/api 2 ".OsCommandSafe($UserLogin));
                   $virtual_user = $UserLogin ."@" . $this->mailaddr_domain;
                   exec("sudo /etc/api/api 6 ".OsCommandSafe($UserLogin)." ".OsCommandSafe($virtual_user));
                   exec("sudo /usr/bin/xtrmakemap");
                   return true;
               }
               else if ($this->type==3)
               {
                    return $this->removeAccount($UserLogin,"iMail");            # Call parent method
               }
               return true;
      }

      function change_password ($UserLogin, $NewPassword)
      {
               if ($this->type==1)
               {
                   # Open webmail change password code
                   exec("sudo /etc/api/api 3 ".OsCommandSafe($UserLogin)." ".OsCommandSafe($NewPassword));
                   return true;
               }
               else if ($this->type==3)
               {
                    return $this->changePassword($UserLogin,$NewPassword,"iMail");  # Call parent method
               }
               else return true;
      }

      function is_user_exist($UserLogin)
      {
               if ($this->type==1)
               {
                   # Open webmail check user code
                   $result = exec("sudo /etc/api/api 4 ".OsCommandSafe($UserLogin));
                   return ($result==1? true: false);
               }
               else if ($this->type==2)
               {
                    return true;
               }
               else
               {
                   return $this->isAccountExist($UserLogin,"iMail");  # Call parent method
               }
      }

        function hasWebmailAccess($targetUserID="")
        {
                global $UserID, $intranet_version;

                if ($intranet_version == "2.5")
                {
                        if ( (isset($_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"])) && ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"] != "") )
                        {
                                return $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"];
                        }
                        else
                        {
                           if (!$this->has_webmail) return false;
                           if ($targetUserID == "") $targetUserID = $UserID;
                           if ($this->type == 2)
                           {
                                        return true;       # No method in checking remote mail system (Left for customization)
                           }

                           $a = new libdb();
                                $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '$targetUserID'";
                           $result = $a->returnVector($sql);
                           $login = $result[0];
                           if ($this->type == 1)    # Open webmail just check OS account
                           {
                                        if (!$this->is_user_exist($login)) return false;
                               else return true;
                           }

                           /* Skip bcoz of performance
                                $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '$targetUserID'";
                           $result = $a->returnVector($sql);
                           $login = $result[0];

                           if (!$this->is_user_exist($login)) return false;
                           else return true;
                           */

                           $sql = "SELECT REVERSE(BIN(ACL)) FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '$targetUserID'";
                           $result = $a->returnVector($sql);
                           $c = substr($result[0],0,1);

                           /* seems useless [20090408]
                           $sql = "SELECT ACL FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '$targetUserID'";
                           $result = $a->returnVector($sql);
                           */

                           $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"] = ($c==1);

                           return ($c==1);
                        }
                }
                else
                {
                   if (!$this->has_webmail) return false;
                   if ($targetUserID == "") $targetUserID = $UserID;
                   if ($this->type == 2)
                   {
                                return true;       # No method in checking remote mail system (Left for customization)
                   }

                   $a = new libdb();
                        $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '$targetUserID'";
                   $result = $a->returnVector($sql);
                   $login = $result[0];
                   if ($this->type == 1)    # Open webmail just check OS account
                   {
                                if (!$this->is_user_exist($login)) return false;
                       else return true;
                   }

                   /* Skip bcoz of performance
                        $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '$targetUserID'";
                   $result = $a->returnVector($sql);
                   $login = $result[0];

                   if (!$this->is_user_exist($login)) return false;
                   else return true;
                   */

                   $sql = "SELECT REVERSE(BIN(ACL)) FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '$targetUserID' ";
                   $result = $a->returnVector($sql);
                   $c = substr($result[0],0,1);

                   return ($c==1);
           }
      }

      function goWebmailPage($login,$password)
      {
               if ($this->mailSystemAction != "")
               {
                   $x .= "
                      <HTML>
                        <BODY ONLOAD=this.autoform.submit()>
                          <FORM NAME=autoform METHOD=POST ACTION=".$this->mailSystemAction.">
                          <INPUT TYPE=HIDDEN NAME=action VALUE=\"login\">
                          <INPUT TYPE=HIDDEN NAME=loginname VALUE=\"$login\">
                          <INPUT TYPE=HIDDEN NAME=password VALUE=\"$password\">
                          </FORM>
                        </BODY>
                      </HTML>";
               }
               return $x;
      }

      #### New functions for ASAV by BL
      function readAutoReply($login)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               if ($this->login_type=="email" && $this->login_domain != "")
               {
                   $login = $login . "@" . $this->login_domain;
               }
               $path = $this->mail_api_url['ReadAutoReply']."?login=".urlencode($login)."&actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               return $response;
      }
      function addAutoReply($login, $text)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               if ($this->login_type=="email" && $this->login_domain != "")
               {
                   $login = $login . "@" . $this->login_domain;
               }

               $path = $this->mail_api_url['AddAutoReply']."?actype=".$this->actype."&login=".urlencode($login)."&text=".urlencode($text)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function removeAutoReply($login)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               if ($this->login_type=="email" && $this->login_domain != "")
               {
                   $login = $login . "@" . $this->login_domain;
               }
               $path = $this->mail_api_url['RemoveAutoReply']."?actype=".$this->actype."&login=".urlencode($login)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function readBlackList()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['ReadBlackList']."?actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response == "")
               {
                   return "";
               }
               else
               {
                   $array = explode(";",$response);
                   return $array;
               }
      }
      function addBlackList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['AddBlackList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function removeBlackList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['RemoveBlackListRecord']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function readWhiteList()
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['ReadWhiteList']."?actype=".$this->actype."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response == "")
               {
                   return "";
               }
               else
               {
                   $array = explode(";",$response);
                   return $array;
               }
      }
      function addWhiteList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['AddWhiteList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function removeWhiteList($target_address)
      {
               if (!$this->checkSecret())
               {
                    return false;
               }
               $path = $this->mail_api_url['RemoveWhiteList']."?actype=".$this->actype."&list=".urlencode($target_address)."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);

               if ($response[0] == "1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }

      function readSpamCheck()
      {
	      ###############################
	      # return value meaning 		#
	      # Y - Do not use spam check	#
	      # N - Use spam check 			#
	      ###############################
	      global $webmail_info;

               if (!$this->checkSecret())
               {
                    return false;
               }

               if($this->retriveSpamControlSetting() == 1){
               		$policy = $webmail_info['custom_spam_control_level'];
               } else {
	                $policy = $webmail_info['bl_spam_control_level'];
               }

               $path = $this->mail_api_url['ReadSpamCheck']."?actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               return $response;
      }
      function turnOnSpamCheck()
      {
	      global $webmail_info;

               if (!$this->checkSecret())
               {
                    return false;
               }

               if($this->retriveSpamControlSetting() == 1){
               		$policy = $webmail_info['custom_spam_control_level'];
               } else {
	                $policy = $webmail_info['bl_spam_control_level'];
               }

               $path = $this->mail_api_url['BypassSpamCheck']."?onOff=N&actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);

               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function turnOffSpamCheck()
      {
	      global $webmail_info;

               if (!$this->checkSecret())
               {
                    return false;
               }

               if($this->retriveSpamControlSetting() == 1){
               		$policy = $webmail_info['custom_spam_control_level'];
               } else {
	                $policy = $webmail_info['bl_spam_control_level'];
               }

               $path = $this->mail_api_url['BypassSpamCheck']."?onOff=Y&actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function readVirusCheck()
      {
	      ###############################
	      # return value meaning 		#
	      # Y - Do not use virus check	#
	      # N - Use virus check 		#
	      ###############################
	      global $webmail_info;

               if (!$this->checkSecret())
               {
                    return false;
               }

               if($this->retriveSpamControlSetting() == 1){
               		$policy = $webmail_info['custom_spam_control_level'];
               } else {
	                $policy = $webmail_info['bl_spam_control_level'];
               }

               $path = $this->mail_api_url['ReadVirusCheck']."?actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               return $response;
      }
      function turnOnVirusCheck()
      {
	      global $webmail_info;

               if (!$this->checkSecret())
               {
                    return false;
               }

               if($this->retriveSpamControlSetting() == 1){
               		$policy = $webmail_info['custom_spam_control_level'];
               } else {
	                $policy = $webmail_info['bl_spam_control_level'];
               }

               $path = $this->mail_api_url['BypassVirusCheck']."?onOff=N&actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);

               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }
      function turnOffVirusCheck()
      {
	      global $webmail_info;

               if (!$this->checkSecret())
               {
                    return false;
               }

               if($this->retriveSpamControlSetting() == 1){
               		$policy = $webmail_info['custom_spam_control_level'];
               } else {
	                $policy = $webmail_info['bl_spam_control_level'];
               }

               $path = $this->mail_api_url['BypassVirusCheck']."?onOff=Y&actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);
               if ($response[0]=="1")
               {
                   return true;
               }
               else
               {
                   return false;
               }
      }

      function setCustomeSpamScore($custom_spam_score)
      {
	      ###############################
	      # return value meaning 		#
	      # Y - Do not use virus check	#
	      # N - Use virus check 		#
	      ###############################
	      global $webmail_info;

               if (!$this->checkSecret())
               {
                    return false;
               }
               $policy = $webmail_info['custom_spam_control_level'];

               $path = $this->mail_api_url['ChangeCustomPolicyScore']."?actype=".$this->actype."&policy=".$policy."&score=".$custom_spam_score."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);

               return $response;
      }

      function readSpamScore()
      {
	      global $webmail_info;

               if (!$this->checkSecret())
               {
                    return false;
               }

               if($this->retriveSpamControlSetting() == 1){
               		$policy = $webmail_info['custom_spam_control_level'];
               } else {
	                $policy = $webmail_info['bl_spam_control_level'];
               }

               $path = $this->mail_api_url['ReadSpamScore']."?actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
               $response = $this->getWebPage($path);

               return $response;
      }

      function changeAllUserPolicy()
      {
	      global $webmail_info;

	      		if (!$this->checkSecret())
				{
				    return false;
				}

				if($this->retriveSpamControlSetting() == 1){
					$policy = $webmail_info['custom_spam_control_level'];
				} else {
				    $policy = $webmail_info['bl_spam_control_level'];
				}

				$path = $this->mail_api_url['changeAllUserPolicy']."?actype=".$this->actype."&policy=".$policy."&secret=".urlencode($this->secret);
				$response = $this->getWebPage($path);

				return $response;
      }

	  function changeDomainSpamScore($score)
      {
      		if (!$this->checkSecret())
           	{
                return false;
           	}
           	$path = $this->mail_api_url['ChangeDomainSpamScore']."?domain=".$this->mailaddr_domain."&actype=".$this->actype."&score=".$score."&secret=".urlencode($this->secret);
      		$response = $this->getWebPage($path);
      		if($response[0]=="1"){
      			return true;
      		}else{
      			return false;
      		}
      }

	  // move INBOX.Junk emails to INBOX - cater POP3 cannot access other folders emails
	  function moveMails($email)
	  {
	  		if (!$this->checkSecret())
           	{
                return false;
           	}
           	if(strpos($email,'@')===false){
           		$email = $email.'@'.$this->mailaddr_domain;
           	}
           	$path = $this->mail_api_url['moveMails']."?email=".urlencode($email)."&actype=".$this->actype."&module=imail&secret=".urlencode($this->secret);
      		$response = $this->getWebPage($path);
      		if($response[0]=="1"){
      			return true;
      		}else{
      			return false;
      		}
	  }

      ## Unused functions
      /*
      function addAlias($loginname, $recipient)
      {
      }
      function removeAlias($loginname)
      {
      }
      function getAliasInfo($loginname)
      {
      }
      function getAliasList()
      {
      }
      */


      ###### End of new functions



      # Functions for Type 3 Only
      # ------------------------------------

      # Connect to mail server
      # Store the connection resource variable in $this->connection
      function openInbox($UserLogin, $UserPassword)
      {
               global $webmail_info;
               global $intranet_inbox_connect_failed;
               $UserLogin = strtolower($UserLogin);
               if ($webmail_info['mail_server_type'] == 'qmail')
               {
                   $login_string = "$UserLogin@".$this->mailaddr_domain;
               }
               else if ($this->login_type == "email")
               {
                   $login_string = "$UserLogin@".$this->mailaddr_domain;
               }
               else $login_string = $UserLogin;
               $inbox = @imap_open("{".$this->host.":".$this->mail_server_port."}",$login_string,$UserPassword);

               if (!$inbox)
               {
                    $intranet_inbox_connect_failed = true;
                    return false;
               }
               $this->inbox = $inbox;
               return $this->inbox;
      }

	  // avoid typo of folder name for goToFolder()
	  function getInboxName()
	  {
	  	 return "INBOX";
	  }
	  // avoid type of folder name for goToFolder()
	  function getJunkBoxName()
	  {
	  	return "INBOX.Junk";
	  }
	  // caution: folder name is not UTF7-IMAP encoded, works for English named folder only
	  function goToFolder($Folder)
	  {
	  		return imap_reopen($this->inbox, "{".$this->host.":".$this->mail_server_port."}".$Folder);
	  }

      # Close the stream
      function close()
      {
               if ($this->inbox)
                   @imap_close($this->inbox,CL_EXPUNGE);
      }

      # Remove Mail in msgno
      function removeMail($msgno)
      {
               if ($this->inbox)
               {
                   return imap_delete($this->inbox,$msgno);
               }
               else return false;
      }

	   function removeMailByUID($uid)
      {
               if ($this->inbox)
               {
                   return imap_delete($this->inbox,$uid, FT_UID);
               }
               else return false;
      }

      # return number of mails in the inbox
      function checkMailCount()
      {
               if (!$this->inbox) return 0;
               return imap_num_msg($this->inbox);
      }

      # Return mail information if the inbox is not empty
      # Attachment needs to copy to path if any
      # Remove the mail immediately
      #
      # Param:
      # $quota - Try to get most mails if quota is enough. (You can refer space with mails or attachment only, just depends which is easy to get)
      #          If no limit , $quota == "UNLIMIT";
      #
      # Return : false if quota not enough
      #          "empty" if the inbox is empty
      #          array of (subject,message,sender address, receiver addresses(To),receiver addresses (cc),receiver addresses (bcc)  , attachment path, date of receive)
      #
      # Attachment path can be get by $this->getNewAttachmentPath()
      # You can write the files using function write_file_content($content,$filepath)
      # Not used as use checkmailOne
      function checkMail($quota)
      {
               $this->embed_part_ids = array();
               $lf = new libfilesystem();
               global $file_path;
               $mailCount = $this->checkMailCount();
               if ($mailCount==0) return;

               $used_quota = 0;                     # Calculate in Kbytes

               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);

               if (!is_array($overview)) return;

               reset($overview);
               while (list($key,$val) = each($overview))
               {
                      $this->embed_part_ids = array();
                      $msg_no = imap_msgno($this->inbox, $val->uid);      # Messge Sequence Number

                      # Grab Structure of the mail
                      $structure = imap_fetchstructure($this->inbox, $val->uid, FT_UID);
                      $i = 1;
                      while (imap_fetchbody( $this->inbox, $val->uid, $i , FT_UID) )
                      {
                             $structure->parts[$i-1] = imap_bodystruct( $this->inbox, $msg_no, $i );
                             $i++;
                      }

                      # Subject
                      $subject_decoded = imap_mime_header_decode($val->subject);
                      $subject = trim($subject_decoded[0]->text);
                      if ($subject == "")
                      {
                          $subject = "[Unknown]";
                      }

                      # Size (in Kbytes)
                      $size = number_format ($val->size / 1024, 1);
                      # Check Quota enough

                      $used += $size;
                      if ($quota != 0 && $quota < $used)
                      {
                          return $mail;
                      }

                      # Grab information
                      $headerinfo = imap_headerinfo($this->inbox, $msg_no);
                      $temp = imap_mime_header_decode($headerinfo->fromaddress);
                      $fromaddress = "";
                      $delim = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $fromaddress .= $delim.$temp[$i]->text;
                           $delim = " ";
                      }
                      $temp = imap_mime_header_decode($headerinfo->toaddress);
                      $delim = "";
                      $toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      $temp = imap_mime_header_decode($headerinfo->reply_toaddress);
                      $delim = "";
                      $reply_toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $reply_toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      if (isset($headerinfo->ccaddress))
                      {
                          $temp = imap_mime_header_decode($headerinfo->ccaddress);
                          $delim = "";
                          $ccaddress = "";
                          for ($i=0; $i<sizeof($temp); $i++)
                          {
                               $ccaddress .= $delim.$temp[$i]->text;
                               $delim = ",";
                          }
                      }
                      $dateReceive = date('Y-m-d H:i:s',strtotime($val->date));

                      # Attachment Path
                      $attachment_path = $this->getNewAttachmentPath();
                      $actual_file_path = $file_path."/file/mail/".$attachment_path;

                      # Message Content
                      $message = $this->parseMessageBody($this->inbox, $val->uid, $structure,'',$actual_file_path);
                      $filtered_message = $this->removeHTMLHeader($message);
                      $isHTML = ($filtered_message != $message);

                      # Get Attachment Size
                      if (is_dir($actual_file_path))
                      {
                          $temp = $lf->folder_size($actual_file_path);
                          $attachment_size = $temp[0]/1024;
                      }
                      else $attachment_size = 0;
                      $mail[] = array($val->uid,$subject,$filtered_message,$isHTML,$attachment_size,$fromaddress,$toaddress,$ccaddress,$dateReceive,$attachment_path);

               }
               return $mail;
      }
      # Return ONE and ONLY ONE mail information if the inbox is not empty
      # Same as checkMail()
      function checkOneMail($quota)
      {
               $this->embed_part_ids = array();
               $lf = new libfilesystem();
               global $file_path;
               $mailCount = $this->checkMailCount();
               if ($mailCount==0) return false;

               $used_quota = 0;                     # Calculate in Kbytes

               $overview = imap_fetch_overview($this->inbox, "1:".$mailCount);

               if (!is_array($overview)) return;

               reset($overview);
               while (list($key,$val) = each($overview))
               {
                      $msg_no = imap_msgno($this->inbox, $val->uid);      # Messge Sequence Number

                      # Grab Structure of the mail
                      $structure = imap_fetchstructure($this->inbox, $msg_no, FT_UID);
                      $i = 1;
                      while (imap_fetchbody( $this->inbox, $msg_no, $i , FT_UID) )
                      {
                             $structure->parts[$i-1] = imap_bodystruct( $this->inbox, $msg_no, $i );
                             /*
                             $j = 1;
                             while( imap_fetchbody( $this->inbox, $msg_no, "$i.$j" ) ){
                                    $structure->parts[$i-1]->parts[$j-1] = imap_bodystruct( $this->inbox, $msg_no, "$i.$j" );
                                    $j++;
                             }
                             */


                             $i++;
                      }
                      /*
                      $i = 1;
                      while (imap_fetchbody( $this->inbox, $val->uid, $i , FT_UID) )
                      {
                             $structure->parts[$i-1] = imap_bodystruct( $this->inbox, $msg_no, $i );
                             $i++;
                      }
*/
                      # Subject
                      $subject_decoded = imap_mime_header_decode($val->subject);
                      $subject = trim($subject_decoded[0]->text);
                      if ($subject == "")
                      {
                          $subject = "[Unknown]";
                      }

                      # Size (in Kbytes)
                      $size = number_format ($val->size / 1024, 1);
                      # Check Quota enough

                      $used += $size;
                      if ($quota != 0 && $quota < $used)
                      {
                          return $mail;
                      }

                      # Grab information
                      $headerinfo = imap_headerinfo($this->inbox, $msg_no);
                      $temp = imap_mime_header_decode($headerinfo->fromaddress);
                      $fromaddress = "";
                      $delim = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $fromaddress .= $delim.$temp[$i]->text;
                           $delim = " ";
                      }
                      $temp = imap_mime_header_decode($headerinfo->toaddress);
                      $delim = "";
                      $toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      $temp = imap_mime_header_decode($headerinfo->reply_toaddress);
                      $delim = "";
                      $reply_toaddress = "";
                      for ($i=0; $i<sizeof($temp); $i++)
                      {
                           $reply_toaddress .= $delim.$temp[$i]->text;
                           $delim = ",";
                      }
                      if (isset($headerinfo->ccaddress))
                      {
                          $temp = imap_mime_header_decode($headerinfo->ccaddress);
                          $delim = "";
                          $ccaddress = "";
                          for ($i=0; $i<sizeof($temp); $i++)
                          {
                               $ccaddress .= $delim.$temp[$i]->text;
                               $delim = ",";
                          }
                      }
                      //$dateReceive = date('Y-m-d H:i:s',strtotime(str_ireplace("UT","UTC",$val->date)));
                      $formattedDatetime = $this->formatDatetimeString($val->date);
                      $dateReceive = date('Y-m-d H:i:s',strtotime($formattedDatetime));

                      # Attachment Path
                      $attachment_path = $this->getNewAttachmentPath();
                      $actual_file_path = $file_path."/file/mail/".$attachment_path;

                      # Message Content
                      $message = $this->parseMessageBody($this->inbox, $val->uid, $structure,'',$actual_file_path);
                      $filtered_message = $this->removeHTMLHeader($message);
                      $isHTML = ($filtered_message != $message);

                      # Get Attachment Size
                      if (is_dir($actual_file_path))
                      {
                          $temp = $lf->folder_size($actual_file_path);
                          $attachment_size = $temp[0]/1024;
                      }
                      else $attachment_size = 0;
                      $mail = array($val->uid,$subject,$filtered_message,$isHTML,$attachment_size,$fromaddress,$toaddress,$ccaddress,$dateReceive,$attachment_path);
                      return $mail;

               }
               return false;
      }
      # Send external mails
      # Mail Subject: $subject
      # Mail Content: $message
      # To: $receiver_to (array)
      # CC: $receiver_cc (array)
      # Bcc: $receiver_bcc (array)
      # Attachment: All files under $attachment_path (no sub-directory)
      # Return true if send is successful
      function sendMail_old($subject,$message,$from,$receiver_to,$receiver_cc,$receiver_bcc,$attachment_path,$IsImportant="")
      {
               # Content-type of attachment
               $type="application/octet-stream";

               $priority = ($IsImportant==1? 1: 3);

               # receiving email addresses
               $numTo = sizeof($receiver_to);
               $numCC = sizeof($receiver_cc);
               $numBCC = sizeof($receiver_bcc);
               if ($numTo + $numCC + $numBCC == 0) return false;

               $to = ($numTo != 0)? implode(", ",$receiver_to) : "";
               $cc = ($numCC != 0)? implode(", ",$receiver_cc) : "";
               $bcc = ($numBCC != 0)? implode(", ",$receiver_bcc) : "";

               # Mail Header
               $headers='';
               $files = $this->listfiles($attachment_path);
               $isMulti = (sizeof($files)!=0);

               $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
               if ($isMulti)
               {
                   $headers .= "MIME-Version: 1.0\r\n";
                   $headers .= "X-Priority: $priority\r\n";
                   $headers .= "Content-Type: multipart/mixed; ";
                   $headers .= " boundary=\"" . $mime_boundary . "\"\r\n";
               }
               $headers .= "From: $from\r\n";
               if ($cc != "")
                   $headers .= "cc: $cc\r\n"; // CC to
               if ($bcc != "")
                   $headers .= "bcc: $bcc\r\n"; // BCCs to, separete multiple with commas mail@mail.com, mail2@mail.com

               if ($isMulti)
               {
                   $mime = "This is a multi-part message in MIME format.\r\n";
                   $mime .= "\r\n";
                   $mime .= "--" . $mime_boundary . "\r\n";

                   # Message Body
                   $mime .= "Content-Transfer-Encoding: 7bit\r\n";
                   $mime .= "Content-Type: text/plain; charset=\"iso-8859-1\"\r\n";
                   $mime .= "\r\n";
               }
               $mime .= $message . "\r\n\r\n";
               if ($isMulti)
               {
                   $mime .= "--" . $mime_boundary . "\r\n";
               }

               # Embed attachment files
               for ($i=0; $i<sizeof($files); $i++)
               {
                    $target = $files[$i];
                    $data = get_file_content("$attachment_path/$target");
                    $fname = addslashes($target);

                    $mime .= "Content-Type: $type;\r\n";
                    $mime .= "\tname=\"$fname\"\r\n";
                    $mime .= "Content-Transfer-Encoding: base64\r\n";
                    $mime .= "Content-Disposition: attachment;\r\n ";
                    $mime .= "\tfilename=\"$fname\"\r\n\r\n";
                    $mime .= base64_encode($data);
                    $mime .= "\r\n\r\n";
                    $mime .= "--" . $mime_boundary;

                    # Different if end of email
                    if ($i==sizeof($files)-1)
                        $mime .= "--\r\n";
                    else
                        $mime .= "\r\n";
               }
               $result = mail($to, $subject, $mime, $headers);
               return $result;
      }

      # New Method
      # Send external mails
      # Mail Subject: $subject
      # Mail Content: $message
      # To: $receiver_to (array)
      # CC: $receiver_cc (array)
      # Bcc: $receiver_bcc (array)
      # Attachment: All files under $attachment_path (no sub-directory)
      # Return true if send is successful
      function sendMail($subject,$message,$from,$receiver_to,$receiver_cc,$receiver_bcc,$attachment_path,$IsImportant="",$mail_return_path="",$reply_address="",$isMulti=null,$nl2br=1,$showSenderAsAdmin=false)
      {
               global $intranet_session_language,$intranet_default_lang_set, $Lang;

				$MimeLineBreak = chr(10);

               # Content-type of attachment
               $type="application/octet-stream";

               $priority = ($IsImportant==1? 1: 3);

               # receiving email addresses
               $numTo = sizeof($receiver_to);
               $numCC = sizeof($receiver_cc);
               $numBCC = sizeof($receiver_bcc);
               if ($numTo + $numCC + $numBCC == 0) return false;

               $to = ($numTo != 0)? implode(", ",$receiver_to) : "";
               $cc = ($numCC != 0)? implode(", ",$receiver_cc) : "";
               $bcc = ($numBCC != 0)? implode(", ",$receiver_bcc) : "";

               # Mail Header
               $headers='';
               $files = $this->listfiles($attachment_path);
               global $intranet_session_language;
               //$charset = (($intranet_session_language=="gb")?"gb2312":"big5");
               // Modified by Ronald on 20090725 - Use "utf-8"
               if ($this->charset!="")
               {
               		$charset = $this->charset;
               } else
               {
               		$charset = "utf-8";
               }

               #$charset = (isBig5($message)?"big5":"iso-8859-1");
               #$isHTMLMessage = $this->isHTMLContent($message);
               global $special_feature, $userBrowser;
               $is_send_from_imail = strpos($_SERVER['REQUEST_URI'],'/home/imail')!==false; /* applicable to iMail and iMail+ */
               $is_mobile_environment = $userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod";
               //$isHTMLMessage = ($special_feature['imail_richtext'] && !($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod"));
               $isHTMLMessage = $special_feature['imail_richtext'] && !($is_send_from_imail && $is_mobile_environment);

			   # For sending kis eAdmission Email
			   if(preg_match('/(\/kis\/admission|\/kis\/apps\/admission\/)/',$_SERVER['SCRIPT_FILENAME'])){
			   		$isHTMLMessage = $special_feature['imail_richtext'];
			   }

			   if (isset($isMulti)) {
					// do nothing
			   } else {
			   		$isMulti = (sizeof($files)!=0);
			   }


               $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
               $mime_boundary2 = "<<<:" . md5(uniqid(mt_rand(), 1));       # Inside Boundary for multi-part
               if ($isMulti)
               {
                   $headers .= "MIME-Version: 1.0".$MimeLineBreak;
                   $headers .= "X-Priority: $priority".$MimeLineBreak;
                   $headers .= "Content-Type: multipart/mixed; ";
                   $headers .= " boundary=\"" . $mime_boundary . "\"".$MimeLineBreak;
               }
               else if ($isHTMLMessage)         # HTML message w/o attachments
               {
                   $headers .= "MIME-Version: 1.0".$MimeLineBreak;
                   $headers .= "X-Priority: $priority".$MimeLineBreak;
                   $headers .= "Content-Type: multipart/alternative; ";
                   $headers .= " boundary=\"" . $mime_boundary . "\"".$MimeLineBreak;
               }

               	//$headers .= "From: $from\r\n";
               	$from = explode("<",$from );

       			if ($showSenderAsAdmin) {
       				$from[0] = $Lang['Email']['SystemAdmin'];
       			}
       			else {
	               	// remove double quote show in sender
	               	if( (substr($from[0],0,1) === "\"") && (substr(rtrim($from[0]),strlen(rtrim($from[0]))-1,1) === "\"") )
	               	{
	               		$from[0] = substr($from[0],1,strlen($from[0]));
	               		$from[0] = substr(rtrim($from[0]),0,strlen(rtrim($from[0]))-1);
	               	}
       			}

				$headers .= "From: =?UTF-8?B?".base64_encode($from[0])."?= <". $from[1] . $MimeLineBreak;

				if ($cc != "")
					$headers .= "cc: $cc".$MimeLineBreak; // CC to
				if ($bcc != "")
					$headers .= "bcc: $bcc".$MimeLineBreak; // BCCs to, separete multiple with commas mail@mail.com, mail2@mail.com
               if ($reply_address != "")
               {
                   $headers .= "Reply-To: $reply_address".$MimeLineBreak;
               }

               $headers .= "Date: ".date("r").$MimeLineBreak;

               # Added by ronald on 20080801
               //$headers = str_replace("\r\n","\n",$headers);

               if ($isMulti || $isHTMLMessage)
               {
                   $mime = "This is a multi-part message in MIME format.".$MimeLineBreak;
                   $mime .= $MimeLineBreak;
                   $mime .= "--" . $mime_boundary . $MimeLineBreak;

               }
               if (!$isMulti)  # No attachments
               {
                    if ($isHTMLMessage)
                    {
                        $mime .= "Content-Type: text/plain; charset=\"$charset\"".$MimeLineBreak;
                        #$mime .= "Content-Transfer-Encoding: quoted-printable\r\n";
                        $mime .= "Content-Transfer-Encoding: base64".$MimeLineBreak;
                        $mime .= $MimeLineBreak;
                        $text_message = removeHTMLtags($message);
                        #$encoded_text_message = QuotedPrintableEncode($message);
                        $encoded_text_message = chunk_split(base64_encode($text_message));
                        $mime .= $encoded_text_message. $MimeLineBreak.$MimeLineBreak;
                        $mime .= "--". $mime_boundary . $MimeLineBreak;
                        # HTML part
                        $mime .= "Content-Type: text/html; charset=\"$charset\"".$MimeLineBreak;
                        #$mime .= "Content-Transfer-Encoding: quoted-printable\r\n";
                        $mime .= "Content-Transfer-Encoding: base64".$MimeLineBreak;
                        $mime .= $MimeLineBreak;
                        #$mime .= "<HTML><BODY>\n".QuotedPrintableEncode($message)."</BODY></HTML>\n";
                        $mime .= chunk_split(base64_encode("<HTML><BODY>\n".($nl2br ? nl2br($message) : $message)."</BODY></HTML>\n"));
                        $mime .= $MimeLineBreak.$MimeLineBreak;
                        $mime .= "--" . $mime_boundary . "--".$MimeLineBreak;
                        $mime .= $MimeLineBreak;
                    }
                    else
                    {
                        $mime .= $message;
                    }

                    //$location_log = '-------------1--------------';//#V77419 log
                    $mime_log = $mime;
               }
               else
               {
                   if ($isHTMLMessage)
                   {
                       $mime .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary2\"".$MimeLineBreak.$MimeLineBreak;
                       $mime .= "--" . $mime_boundary2 . $MimeLineBreak;
                       $mime .= "Content-Type: text/plain; charset=\"$charset\"".$MimeLineBreak;
                       #$mime .= "Content-Transfer-Encoding: quoted-printable\r\n";
                       $mime .= "Content-Transfer-Encoding: base64".$MimeLineBreak;
                       $mime .= $MimeLineBreak;
                       #$mime .= QuotedPrintableEncode(removeHTMLtags($message)). "\r\n\r\n";
                       $mime .= chunk_split(base64_encode(removeHTMLtags($message))). $MimeLineBreak.$MimeLineBreak;
                       $mime .= "--". $mime_boundary2 . $MimeLineBreak;
                       # HTML part
                       $mime .= "Content-Type: text/html; charset=\"$charset\"".$MimeLineBreak;
                       #$mime .= "Content-Transfer-Encoding: quoted-printable\r\n";
                       $mime .= "Content-Transfer-Encoding: base64".$MimeLineBreak;
                       $mime .= $MimeLineBreak;
                       #$mime .= QuotedPrintableEncode($message);
                       $mime .= chunk_split(base64_encode("<HTML><BODY>\n".($nl2br ? nl2br($message) : $message)."</BODY></HTML>\n"));
                       $mime .= $MimeLineBreak.$MimeLineBreak;
                       $mime .= "--" . $mime_boundary2. "--".$MimeLineBreak;
                       $mime .= $MimeLineBreak;
                       $mime .= "--" . $mime_boundary. $MimeLineBreak;

                       //$location_log = '-------------2--------------';//#V77419 log
                       $mime_log = $mime;
                   }
                   else
                   {
                       # Message Body
                       $mime .= "Content-Transfer-Encoding: 7bit".$MimeLineBreak;
                       $mime .= "Content-Type: text/plain; charset=\"$charset\"".$MimeLineBreak;
                       $mime .= $MimeLineBreak;

                       $mime .= $message;
                       $mime .= $MimeLineBreak.$MimeLineBreak;
                       $mime .= "--" . $mime_boundary. $MimeLineBreak;
                   }

                   //$location_log = '-------------3--------------';//#V77419 log
                   $mime_log = $mime;

                   $filename_encoding = "=?".strtoupper($charset)."?B?";

                   # Embed attachment files
                   for ($i=0; $i<sizeof($files); $i++)
                   {
                        $target = $files[$i];
                        $data = get_file_content("$attachment_path/$target");
                        $fname = addslashes($target);

                        $mime .= "Content-Type: $type;".$MimeLineBreak;
                        //$mime .= "\tname=\"$fname\"\r\n";
                        $mime .= "\tname=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$MimeLineBreak;
                        $mime .= "Content-Transfer-Encoding: base64".$MimeLineBreak;
                        $mime .= "Content-Disposition: attachment;".$MimeLineBreak." ";
                        //$mime .= "\tfilename=\"$fname\"\r\n\r\n";
                        $mime .= "\tfilename=\"".$filename_encoding.base64_encode(stripslashes($fname))."?=\"".$MimeLineBreak.$MimeLineBreak;
                        $mime .= chunk_split(base64_encode($data));
                        $mime .= $MimeLineBreak.$MimeLineBreak;
                        $mime .= "--" . $mime_boundary;

                        # Different if end of email
                        if ($i==sizeof($files)-1)
                            $mime .= "--".$MimeLineBreak;
                        else
                            $mime .= $MimeLineBreak;
                   }

               }

               //for log to debug 2015-04-17 Omas
               global $sys_custom;
               if($sys_custom['SendMailMIMELog']) {
					$log = "--------MIME Log Start-------\n";
					$log.= "SourceFrom:".var_export(debug_backtrace(),true)."\n";
					//$log.= "Location:".$location_log."\n\n";//#V77419 log
					$log.= "To:".$to."\n";
					$log.= "Subject:".$subject."\n";
					$log.= $headers."\n";
					$log.= $mime_log."\n";
					$log.= "--------MIME Log End-------\n";
					$this->logModuleMail($log);
				}

               // Added by ronald on 20090725
               $subject="=?UTF-8?B?".base64_encode($subject)."?=".$MimeLineBreak;

               #global $intranet_root;
               #write_file_content($mime, "$intranet_root/file/outmail.txt");

               if ($mail_return_path!="")
               {
               		$result = mail($to, $subject, $mime, $headers, "-f $mail_return_path");
               }
               else
               {
               		$result = mail($to, $subject, $mime, $headers);
               }

               return $result;
      }

      # ---------------------------------------
      # Supporting functions

      # get a random path
      function getNewAttachmentPath()
      {
               $path = session_id().".".substr( md5(uniqid(rand(),1)),10 );
               global $UserID;
               if ($UserID == "")
               {
                   return $path;
               }
               else
               {
                   global $intranet_root;
                   if (!is_dir("$intranet_root/file/mail/u$UserID"))
                   {
                        mkdir("$intranet_root/file/mail/u$UserID",0777);
                   }
                   $user_folder = $this->getNextFreeUserFolder($UserID, false);
                   //$path = "u$UserID/$path";
                   $path = $user_folder."/$path";
                   return $path;
               }
      }

      # Return all filenames under directory $path (sub-directories will be ignored)
      # For Debian version, is_dir will not return correct value if the directory is just removed
      # therefore, need to add @ to suppress error message and return null array if error occurs
      function listfiles($path)
      {
               clearstatcache();
               if (!is_dir($path)) return array();
               $d = @dir($path);
               if (!$d) return array();
               while($entry = $d->read())
               {
                     $filepath = $path."/".$entry;
                     if (is_file($filepath))
                     {
                         $files[] = $entry;
                     }
               }
               $d->close();
               return $files;
      }
      # Modify significant HTML tag that will affect the whole document outlook
      function removeHTMLHeader($str)
      {
               $pos = strpos($str,"<body");
               if ($pos===false)
               {
                   $pos = strpos($str,"<BODY");
               }
               if ($pos!==false)
               {
                   $str = substr($str,$pos);
               }
               $epos = strpos($str,"</body>");
               if ($epos===false)
               {
                   $epos = strpos($str,"</BODY>");
               }
               if ($epos!==false)
               {
                   $str = substr($str,0,$epos+7);
               }

               $str = str_replace("<BODY","<xbody",$str);
               $str = str_replace("<body","<xbody",$str);
               $str = str_replace("</BODY>","</xbody>",$str);
               $str = str_replace("</body>","</xbody>",$str);
               $str = str_replace("<HTML","<xhtml",$str);
               $str = str_replace("<html","<xhtml",$str);
               $str = str_replace("</HTML>","</xhtml>",$str);
               $str = str_replace("</html>","</xhtml>",$str);
               $str = str_replace("<META","<xmeta",$str);
               $str = str_replace("<meta","<xmeta",$str);

               return $str;

      }

      # Convert Mail message
      function convertMessage($str)
      {
               $coded = str_replace("=\r\n","",$str);
               if ($coded == $str)
               {
                   return $str;
               }
               else
               {
                  $result = quoted_printable_decode($coded);
                  return $result;
               }
      }

      // This function recursively analyze and output the contents of the mail message.
      function parseMessageBody($imap_stream, $msg_number, $structure, $part_number, $attachment_path)
      {
               #echo "part: $part_number";
               #print_r($structure);
               // Split the $part_number. For example, 1.2.3 will be split into 1.2.(major) and 3(minor)
               if (strlen($part_number) == 0)
               {
                   $major_part_number = '';
                   $minor_part_number = 1;
               }
               elseif (strlen($part_number) == 1)
               {
                   $major_part_number = "$part_number.";        // be aware of the 'dot' at the end.
                   $minor_part_number = 0;
               }
               else
               {
                   $major_part_number = substr($part_number, 0, strlen($part_number)-1);
                   $minor_part_number = intval(substr($part_number, -1));
               }

               // Determine whether it is a leaf node. Perform recursion if not a leaf node.
               // NOTE: For "subtype = alternative", it is most likely generated by email clients
               // such as outlook express for sending rich text message and plain text message
               // at the same time. So only in this case no recursion is nessary.
               // NOTE: For "type = message",
               $parts = isset($structure->parts)?$structure->parts:'';

               if (is_array($parts) && ($structure->ifsubtype) && strtolower($structure->subtype) != 'alternative')
               {
                   foreach ($parts as $part)
                   {
                            $body = $this->parseMessageBody($imap_stream, $msg_number, $part, "$major_part_number".$minor_part_number,$attachment_path);
                            $minor_part_number++;

                            if ($part->encoding==0 && (strtolower($part->disposition)=="attachment" || $part->ifdisposition==1))
                            {
                                $part->encoding = 4;
                            }

                            $body = $this->decode_part($body, $part->encoding);
                            $message_body .= $this->manage_part($body,$part,$attachment_path);
                            if ($part->id != "")
                            {
                                $temp = imap_mime_header_decode($part->parameters[0]->value);
                                $t_filename = $temp[0]->text;
                                $temp_str = substr($part->id,1,strlen($part->id)-2);
                                $this->embed_part_ids[] = array($temp_str,$t_filename);
                            }
                   }
                   return $message_body;
               }


               // See whether it is an 'alternative' message. Output the content and return if so.
               #
               if (($structure->ifsubtype) && strtolower($structure->subtype) == 'alternative' && is_array($structure->parts))
               {
                    // Load user preferences
                    $message_format = $this->message_format;
                    if ($message_format != 'html') $message_format = 'plain';

                    // Read the message according to the preferences of the user
                    foreach ($structure->parts as $part)
                    {
                             if (strtolower($part->subtype) == $message_format)
                             {
                                 $body = $this->readMessageBody($imap_stream, $msg_number, $major_part_number.$minor_part_number);
                                 if ($part->encoding==0 && (strtolower($part->disposition)=="attachment" || $part->ifdisposition==1))
                                 {
                                     $part->encoding = 4;
                                 }
                                 $body = $this->decode_part($body, $part->encoding, $part->subtype);
                                 $message_body .= $this->manage_part($body,$part,$attachment_path);
                             }
                             $minor_part_number++;
                    }

                    return $message_body;
               }
               else if (($structure->ifsubtype) && (strtolower($structure->subtype) == 'alternative'))
               {
                    // Load user preferences
                    $message_format = $this->message_format;
                    if ($message_format != 'html') $message_format = 'plain';
                    if (true || strtolower($structure->subtype) == $message_format)
                    {
                        #$body = $this->readMessageBody($imap_stream, $msg_number,$structure, $major_part_number.$minor_part_number, $attachment_path);
                        $body = $this->readMessageBody($imap_stream, $msg_number, $part_number);
                        if ($structure->encoding==0 && (strtolower($structure->disposition)=="attachment" || $structure->ifdisposition==1))
                        {
                            $structure->encoding = 4;
                        }
                        $body = $this->decode_part($body, $structure->encoding,$structure->subtype);
                        $message_body .= $this->manage_part($body,$structure,$attachment_path);
                        $message_body = $this->handle_nested_message($message_body);
                    }
                    $minor_part_number++;
                    return $message_body;
               }
               else
               {
                   if ( strtolower($structure->subtype)=="related")
                   {
                   }
               }


               // Something other than those mentioned above at the leaf node.
               $body = $this->readMessageBody($imap_stream, $msg_number, $part_number);
               #$body = $this->handle_mixed_message($body);

               # Perform encoding
               $body = $this->decode_part($body, $structure->encoding, $structure->subtype);
               $message_body .= $this->manage_part($body,$structure,$attachment_path);
               return $message_body;
      }

      // This function opens the message. This function is a helper of parseMessageBody().
      function readMessageBody ($imap_stream, $msg_number, $part_number)
      {
               // If $part_number is empty, it assume the message does not have multiple parts
               // and just throw them to screen.
               //echo "Read body $part_number<br>\n";
               if (empty($part_number) || $part_number === "1.0")
               {
                   $temp_msg  =imap_body($imap_stream, $msg_number, FT_UID);
                   //echo $temp_msg;
                   return $temp_msg;
               }
               else
               {
                   $temp_msg = imap_fetchbody($imap_stream, $msg_number, $part_number, FT_UID);
                   //echo $temp_msg;
                   return $temp_msg;
               }
      }

      # Decode message part
      function decode_part($body, $encoding, $subtype="")
      {
               if ($encoding=="")
               {
                   $lower_subtype = trim(strtolower($subtype));
                   if ($lower_subtype=='plain' || $lower_subtype=='html' || $lower_subtype=='')
                   {
                       return $body;
                   }
                   else
                   {
                       return quoted_printable_decode($body);
                   }
               }
               switch ($encoding)
               {
                       case 0:
                            #$body = $this->convertMessage($body);
                            break;
                       case 1:
                            break;
                       case 2:
                       case 3:
                            $body = base64_decode ($body); break;
                       case 4:
                            $body = quoted_printable_decode ($body); break;
                       default:
                            $body = quoted_printable_decode ($body); break;
               }

               return $body;
      }

      # Copy the content to file if this part is attachment
      function manage_part($body, $structure, $attachment_path)
      {
               if ($body == "") {
               return ""; }
               #print_r($structure);
               if (strtolower($structure->disposition)=="attachment" || $structure->ifdisposition==1 || $structure->ifid==1 || ($structure->type!=0 && $structure->type!=1 &&$structure->type!=2))
               {
                   $file_content = $body;

                   if (!is_dir($attachment_path))
                   {
                        mkdir($attachment_path,0777);
                   }
                   # Get Filename

                   if ($structure->ifdparameters==1)
                   {
                       $temp = imap_mime_header_decode($structure->dparameters[0]->value);
                   }
                   else if ($structure->ifparameters==1)
                   {
                       $temp = imap_mime_header_decode($structure->parameters[0]->value);
                   }
                   $filename = $temp[0]->text;
                   if ($filename == "")
                   {
                       $filename = "unknown";
                   }
                   $target_file = $attachment_path."/".$filename; #$structure->parameters[0]->value;
                   write_file_content($file_content,$target_file);
                   return "";                       # Empty String (not affect message body)
               }
               else
               {
                   return $body;
               }
      }
      function handle_nested_message($msg)
      {

               $is_quoted_printable = false;
               $msg = trim($msg);
               if (strpos($msg,"Content-Transfer-Encoding: quoted-printable\r\n")!==false)
               {
                   $is_quoted_printable = true;
               }
               else
               {
               }

               $firstnl = strpos($msg,"\r\n");
               if ($firstnl === false) return $msg;
               $bound_str = trim(substr($msg,0,$firstnl));

               if ($bound_str == "") return $msg;
               if (strlen($bound_str)<=70)    # RFC 1521 standard
               {
                   $left_str = substr($msg,$firstnl+2);
                   $alt_bpos = strpos($left_str,$bound_str);     # begin of alternative part boundary
                   $alt_bendpos = $alt_bpos + strlen($bound_str);  # end of alternative part boundary
                   #$temp = substr($left_str, $alt_bpos, strlen($bound_str));

                   if ($alt_bpos===false || ($left_str[$alt_bendpos]!="\r" && $left_str[$alt_bendpos]!="\n" ) )
                   {
                       return $msg;
                   }
                   else
                   {
                       $left_str2 = substr($left_str,$alt_bendpos);
                       $pos = strpos($left_str2,"\r\n\r\n");
                       if ($pos === false) return $msg;
                       $left_str3 = trim(substr($left_str2,$pos));
                       $end_boundary = substr($left_str3,0-strlen($bound_str)-2);
                       if ($end_boundary == "$bound_str--")
                       {
                           $left_str3 = trim(substr($left_str3,0,strlen($left_str3)-strlen($bound_str)-2));
                       }
                       if ($is_quoted_printable)
                       {
                           $pos_3d = strpos($left_str3,"=3D");
                           if ($pos_3d === false)
                           {
                           }
                           else
                           {
                               $left_str3 = quoted_printable_decode($left_str3);
                           }
                       }

                       return $left_str3;
                   }
               }
               else
               {
                   return $msg;
               }
      }

      # Supporting function
      function isHTMLContent($content)
      {
               $filtered_content = removeHTMLtags($content);
               return ($filtered_content != $content);
      }

      /*
          Get X-Spam Header
          Param: email header from imap_fetchheader()
          Return: Array(flag, score, level, status)
      */
      function getSpamInfo($mail_header)
      {
               //echo $mail_header;
               $xheader_flag = "X-Spam-Flag: ";
               $xheader_score = "X-Spam-Score: ";
               $xheader_level = "X-Spam-Level: ";
               $xheader_status = "X-Spam-Status: ";
               $len_xheader_flag = strlen($xheader_flag);
               $len_xheader_score = strlen($xheader_score);
               $len_xheader_level = strlen($xheader_level);
               $len_xheader_status = strlen($xheader_status);

               $header = explode("\r\n", $mail_header);
               if (is_array($header) && sizeof($header))
               {
                   $spam_flag = false;
                   $spam_score = 0;
                   $spam_level = "";
                   $spam_status = "";
                   foreach ($header as $line)
                   {
                       if (substr($line,0,$len_xheader_flag)==$xheader_flag)
                       {
                           $spam_flag = substr($line,$len_xheader_flag);
                       }
                       else if (substr($line,0,$len_xheader_score)==$xheader_score)
                       {
                           $spam_score = substr($line,$len_xheader_score);
                       }
                       else if (substr($line,0,$len_xheader_level)==$xheader_level)
                       {
                           $spam_level = substr($line,$len_xheader_level);
                       }
                       else if (substr($line,0,$len_xheader_status)==$xheader_status)
                       {
                           $spam_status = substr($line,$len_xheader_status);
                       }
                   }
                   return array($spam_flag, $spam_score, $spam_level, $spam_status);
               }
               else return false;
      }


      ##########################################################################################
      # New Mail retrieving function
      # Param: Quota : quota in Mbytes, num : number of emails to be retrieved
      function checkMail2($quota, $num=0)
      {
               global $webmail_info, $iMail_insertDB_debug;

               $mailCount = $this->checkMailCount();

			   #Defining MbString Detect Order
			   mb_detect_order("ASCII, UTF-8, Big5, GB2312, GBK");

               if ($mailCount==0) return;

               $used_quota = 0;                     # Calculate in Kbytes
               //$overview = imap_fetch_overview($this->inbox, "1:".$mailCount);
			   $overview = imap_fetch_overview($this->inbox, "1:".($num>0? min($num,$mailCount): $mailCount));
			   //debug_pr($overview);
               if (!is_array($overview)) return;
               if ($num==0)
               {
                   $num = sizeof($overview);
               }
               unset($result_mail);

               for ($i=0; $i < sizeof($overview) && $i < $num; $i++)
               {
					$aggregatedTextHeader = null;
                    $mail_msgno = $overview[$i]->msgno;
                    $uid = $overview[$i]->uid;
                    /* Fix error of retriving multiple address in To: */
                    $text_header = imap_fetchheader($this->inbox, $mail_msgno);
                    $raw_header = imap_rfc822_parse_headers($text_header);

					if ($aggregatedTextHeader != '')
						$aggregatedTextHeader .= "\n";

					$aggregatedTextHeader .= $text_header;

                    /* Read SPAM header */
                    if ($webmail_info['bl_spam'])
                    {
                        $spam_info = $this->getSpamInfo($text_header);
                        list($spamFlag, $spamScore, $spamLevel, $spamStatus) = $spam_info;
                    }
                    /* End of read SPAM header */

                    $header = imap_header($this->inbox, $mail_msgno);

                    ## From address
                    //$from = $header->fromaddress;
                    $fromElements= imap_mime_header_decode($header->fromaddress);
					unset($tmp_from);
                    for($k=0;$k<count($fromElements);$k++) {
                    	if(strtolower($fromElements[$k]->charset) != "default")
                        	$tmp_from .= @iconv($fromElements[$k]->charset,"UTF-8//TRANSLIT",$fromElements[$k]->text);
                        else
                        	$tmp_from .= $fromElements[$k]->text;
                    }
                    $from = $tmp_from;

                    ## To address
                    //$to = $raw_header->toaddress;
                    $toElements= imap_mime_header_decode($raw_header->toaddress);
					unset($tmp_to);
                    for($k=0;$k<count($toElements);$k++) {
                    	if(strtolower($toElements[$k]->charset) != "default")
                        	$tmp_to .= @iconv($toElements[$k]->charset,"UTF-8//TRANSLIT",$toElements[$k]->text);
                        else
                        	$tmp_to .= $toElements[$k]->text;
                    }
                    $to = $tmp_to;

                    ## Subject
                    //$subject = $this->MIME_Decode($header->Subject);
                    //$subject = $header->Subject;
                    $subjectElements = imap_mime_header_decode($header->Subject);

					unset($tmp_subject);
                    for($k=0;$k<count($subjectElements);$k++)
                    {
                    	if(strtolower($subjectElements[$k]->charset) != "default")
                    	{
                        	//$tmp_subject .= @iconv($subjectElements[$k]->charset,"UTF-8//TRANSLIT",$subjectElements[$k]->text);

                        	if((strtoupper($subjectElements[$k]->charset) == "GB2312") || (strtoupper($subjectElements[$k]->charset) == "GB18030"))
							{
								$tmp_subject .= @iconv("GBK","UTF-8//TRANSLIT",$subjectElements[$k]->text);
							}
							else if((strtoupper($subjectElements[$k]->charset) == "X-GBK") || (strtoupper($subjectElements[$k]->charset) == "GBK"))
							{
								$tmp_subject .= @iconv("GBK","UTF-8//TRANSLIT",$subjectElements[$k]->text);
							}
							else
							{
								//$tmp_subject .= $subjectElements[$k]->text;
								$tmp_subject .= @iconv($subjectElements[$k]->charset,"UTF-8//TRANSLIT",$subjectElements[$k]->text);
							}
                        }else if(trim($subjectElements[$k]->charset)==''){
                        	$tmp_subject .= @mb_convert_encoding($subjectElements[$k]->text,"UTF-8","ASCII,BIG5,MS950,GBK,GB2312,UTF-8");
                        }
                        else // strtolower($subjectElements[$k]->charset) == "default"
                        {
                        	$tmp_subject .= $subjectElements[$k]->text;
                        }
                    }
                    //debug_r($subjectElements);
                    $subject = $tmp_subject;
                    //echo "<BR>".$subject."<BR>";
                    //die;

					unset($Temp);
					/*
					 * Disabled by Ronald on (20101206)
				    $elements = imap_rfc822_parse_adrlist(imap_utf8($from),"");
				    $Temp = $this->Get_To_Cache_Address_List($elements);
				    $fromname = $Temp['Actual'];
				    */
				    $fromname = imap_utf8($from);

                    ## As imap_mime_header_decode cannot convert GB2321 to UTF8 correctly.
					## So changed to the following logic, if MIME Header Charset = GB2312,
					## then use mb_converty_concoding to convert the subject into UTF8 format.
					/*
					$tempSubjectArray = imap_mime_header_decode($subject);

					$preProbe = mb_detect_encoding($tempSubjectArray[0]->text);

					if($tempSubjectArray[0]->charset == 'gb2312')
					{
						unset($subtmp);
						foreach($tempSubjectArray as $Key=>$tmpArr) {
							$subtmp .= mb_convert_encoding($tmpArr->text, 'UTF-8', 'GBK');
						}
						$subject = $subtmp; # Subject to return
					}
					else
					{
                    	$elements=imap_mime_header_decode(imap_utf8($subject));

                    	for($k=0;$k<count($elements);$k++) {
                        	$subtmp .= $elements[$k]->text;
                    	}
                    	$subject = $subtmp; # Subject to return

                    	if(strtoupper($tempSubjectArray[0]->charset) == "GB18030")
                    	{
                    		$subject = mb_convert_encoding($subject, 'UTF-8', 'GBK');
                    	}
                    	else if (strtoupper($tempSubjectArray[0]->charset) == "GBK")
                    	{
                    		$subject = mb_convert_encoding($subject, 'UTF-8', 'GBK');
                    	}
                    }
                    */

					$showto = imap_utf8($to);
					unset($Temp);
				    //$elements = imap_rfc822_parse_adrlist(imap_utf8($to),"");
				    //$Temp = $this->Get_To_Cache_Address_List($elements);
				    //$showto = $Temp['Actual'];

					$showcc = imap_utf8($header->ccaddress);
					unset($Temp);
				    //$elements = imap_rfc822_parse_adrlist(imap_utf8($header->ccaddress),"");
				    //$Temp = $this->Get_To_Cache_Address_List($elements);
				    //$showcc = $Temp['Actual'];

                    /*
                    $this_offset = $tzoffset * 3600;
                    $date = @Date("r", $header->udate+$this_offset);      # Date of email
                    */

                    ### Modified By Ronald (20091021)
                    ### New method : prevent the mail which sent from gateway
                    //$dateReceive = date('Y-m-d H:i:s',strtotime($header->date));
                    //$TimeString = str_ireplace("UT","UTC",$header->date);
                    $TimeString = $this->formatDatetimeString($header->date);
					do {
						$TempTime = strtotime(trim($TimeString));
						if ($TempTime === false)
							$TimeString = substr($TimeString,0,strlen($TimeString)-1);
					} while ($TempTime === false && $TimeString != "");
					$dateReceive = date('Y-m-d H:i:s',$TempTime);

                    $structure = imap_fetchstructure($this->inbox, $mail_msgno);
					//debug_r($structure);
					$this->iMail_mailStructure_log($iMail_insertDB_debug, $header->message_id, $structure);

					unset($this->parsed_part);
                    $this->parse_message($structure);
                    $attachments = $this->grab_attach($this->parsed_part);
                    //debug_pr($this->parsed_part);
                    # Handle attachments
                    unset($attach_parts);
                    unset($embed_cids);
                    $total_attach_size = 0;

                    for ($k=0; $k<sizeof($attachments); $k++)
                    {
						$pid = $attachments[$k]["partnum"];
						$attach_filename = $attachments[$k]["filename"];
						$attach_type = $attachments[$k]["type"];
						$attach_size = $attachments[$k]["size"];
						$attach_encoding = $attachments[$k]["encoding"];
						$attach_cid = $attachments[$k]["cid"];

						$elements=imap_mime_header_decode(imap_utf8($attach_filename));

						unset($subtmp);
						for($f_i=0;$f_i<count($elements);$f_i++) {
							//if(strtoupper($elements[$f_i]->charset) == "DEFAULT"){
							//	if( strtoupper(mb_detect_encoding($elements[$f_i]->text)) != "UTF-8" ) {
							//		$subtmp .= iconv(mb_detect_encoding($elements[$f_i]->text),"UTF-8//TRANSLIT//IGNORE",$elements[$f_i]->text);
							//	}else{
							//		$subtmp .= $elements[$f_i]->text;
							//	}
							//}else{
								$subtmp .= $elements[$f_i]->text;
							//}
						}
						$attach_filename = $subtmp;

						$attach_content = $this->retrieve_attachment_content($this->inbox, $mail_msgno, $pid);
						$decoded_content = $this->mime_decode_msg($attach_content,$this->mime_encoding[$attach_encoding]);

						unset($attach_entry);

						/*
						mb_detect_order("ASCII, UTF-8, Big5, GB2312, GBK");
						$preProbe = mb_detect_encoding($attach_filename);

						if ($preProbe != 'UTF-8') {
								$probedFilenameCharset = mb_detect_encoding($attach_filename,"ASCII, Big5, GB2312, GBK, UTF-8");
						} else {
								$probedFilenameCharset = $preProbe;
						}

						if (in_array(strtoupper($probedFilenameCharset),array('BIG5, BIG-5','GBK','CP936'))) {
								if (strtoupper($probedFilenameCharset) == 'BIG-5' || strtoupper($probedFilenameCharset) == 'BIG5') {
										$attach_entry['filename'] = @iconv('Big5', 'UTF-8//TRANSLIT', $attach_filename);
								} else if (strtolower($probedFilenameCharset) == 'gbk' || strtolower($probedFilenameCharset) == 'cp936') {
										$attach_entry['filename'] = @mb_convert_encoding($attach_filename, 'UTF-8',$probedFilenameCharset);
								}
						} else {
								$attach_entry['filename'] = @mb_convert_encoding($attach_filename,'UTF-8',$probedFilenameCharset);
						}
						*/
						$attach_entry['filename'] = $attach_filename;

						$attach_entry['type'] = $attach_type;
						$attach_entry['size'] = $attach_size;
						$attach_entry['cid'] = $attach_cid;
						$attach_entry['content'] = $decoded_content;
						$attach_parts[] = $attach_entry;
						$embed_cids[] = array($attach_entry['cid'],$attach_entry['filename']);
						$total_attach_size += $attach_size;

                    }

                    # Check Quota enough
                    $size = round($total_attach_size/1024,1);
                    $used_quota += $size;

                    # updated on 2007-08-27
                    if ($quota < $used_quota)
                    {
                        return $result_mail;
                    }

					unset($tmpParsedPart);
					$tmpParsedPart = $this->parsed_part;
					//debug_r($tmpParsedPart);
					$raw_mail_source = imap_fetchbody($this->inbox, $mail_msgno, '');

                    # Retrieve Message Content
					unset($message);
                    $message = $this->retrieve_message($this->inbox, $mail_msgno, $tmpParsedPart);

                    # Retrieve Raw Message Content
					unset($rawMessage);
					$rawMessage = $this->get_raw_message($this->inbox, $mail_msgno, $tmpParsedPart);

					if (strtolower($this->message_charset) != 'utf-8') {
						if (strtolower(mb_detect_encoding($rawMessage, "ASCII, UTF-8, Big5, GB2312, GBK, windows-1252", true)) != strtolower($this->message_charset)) {
							if (strtolower($this->message_charset == 'gb2312')) {
								$messageCharSet = 'gb2312';
							} else {
								$messageCharSet = mb_detect_encoding($rawMessage, "BIG5, GBK");
							}
						} else {
							$messageCharSet = $this->message_charset;
						}
					} else {
						$messageCharSet = $this->message_charset;
					}

                    $result_mail[] = array($mail_msgno, $subject,$message,$total_attach_size,
                                           $fromname, $showto, $showcc, $dateReceive, $attach_parts,
                                           $embed_cids, $this->message_charset, $this->is_html_message,
                                           $spamFlag, $spamScore, $spamLevel, $spamStatus, $rawMessage, $messageCharSet,
										   $aggregatedTextHeader, $raw_mail_source, $uid
                                           );
               }
               return $result_mail;
      }

      function retrieve_message($inbox, $uid, $partArr)
      {
      		global $sys_custom;
               $this->is_html_message = -1;
               $viewpart = 0;
               $totalviewed = 0;
               $result_message = "";

               while ($partArr[$viewpart] != "")
               {
                      $p = $partArr[$viewpart];

                      #if ((($p["type"] == 0 && strtolower($p["subtype"]) == ("plain" || "html")) || (strtolower($p["disposition"]) == "inline")) && strtolower($p["disposition"])!='attachment')
                      if ((($p["type"] == 0 && strtolower($p["subtype"]) == ("plain" || "html")) ) && strtolower($p["disposition"])!='attachment')
                      {
                           $htmltransform = false;
                           $this->is_html_message = -1;
                           $this->message_charset = $p["charset"];

                           if ($p["type"] == 0 && strtolower($p["subtype"]) == "plain")
                           {
                               $transform = true;
                           }
                           if (strtolower($p["subtype"]) == "html")
                           {
                               $htmltransform = true;
                               $this->is_html_message = 1;
                           }

                           /*Fix for Inaccurate Part number finding*/
                           	$tmp = $p["partnum"];
							while (strlen($new = str_replace("..",".",$tmp)) < strlen($tmp))
								$tmp = $new;

							$myBod = imap_fetchbody($inbox, $uid, (string)($tmp));

							$myBod = $this->mime_decode_msg($myBod, $this->mime_encoding[$partArr[$viewpart]["encoding"]]);
							//debug_r(htmlspecialchars($myBod,ENT_QUOTES));
							//mb_detect_order("ASCII, UTF-8, Big5, GB2312, GBK");

							if((strtoupper($this->message_charset) == "X-GBK") || (strtoupper($this->message_charset) == "GBK"))
							{
								$myBod = @iconv("GBK",'UTF-8//TRANSLIT',$myBod);
							}
							else if((strtoupper($this->message_charset) == "GB2312"))
							{
								$myBod = @iconv("GBK",'UTF-8//TRANSLIT',$myBod);
							}
							else if((strtoupper($this->message_charset) == "MS950"))
							{
								$myBod = @iconv("BIG5",'UTF-8//TRANSLIT',$myBod);
							}else if((strtoupper($this->message_charset) == "CP936") || strtoupper($this->message_charset) == "BIG5-HKSCS")
							{
								$myBod = @iconv("BIG5-HKSCS",'UTF-8//TRANSLIT',$myBod);
							}else if(strtoupper($this->message_charset) == "BIG5")
							{
								$detect_charset = mb_detect_encoding($myBod, "ASCII, UTF-8, Big5, GB2312, GBK", true);
								if(strtoupper($detect_charset) == "CP936"){ // MS Outlook use CP936 that is similar to BIG5
									$tmp_myBod = @iconv("BIG5-HKSCS",'UTF-8//TRANSLIT',$myBod);
									if(strlen($tmp_myBod) < strlen($myBod)){ // loss some text implies convertion has failed
										$myBod = @mb_convert_encoding($myBod,"UTF-8",$this->message_charset);
									}else{
										$myBod = $tmp_myBod;
									}
								}else{
									$myBod = @iconv("BIG5",'UTF-8//TRANSLIT',$myBod);
								}
							}else if(strtoupper($this->message_charset) == "WINDOWS-1252"){
								$myBod = @iconv("Windows-1252",'UTF-8//TRANSLIT',$myBod);
							}
							else
							{
								if(strtoupper($this->message_charset) != "UNKNOWN_PARAMETER_VALUE" && strtoupper($this->message_charset) != "DEFAULT")
								{
									//$myBod = @iconv($this->message_charset,'UTF-8//TRANSLIT',$myBod);
									$target_charset = (trim($this->message_charset)!=''?$this->message_charset.',':'')."ASCII,BIG5,GBK,GB2312,windows-1252,UTF-8";
									$myBod = @mb_convert_encoding($myBod,"UTF-8",$target_charset);
								}
								else
								{
									$myBod = $myBod;
								}
							}

							// echo '<br/>';
							// debug_r(mb_detect_order());
							// echo '<pre>detect encoding : '.mb_detect_encoding($myBod, mb_list_encodings()).'</pre>';
                           	// echo '<pre>$partArr[$viewpart]["charset"] : '.$partArr[$viewpart]["charset"].'</pre>';
							### Modified By Michael Cheung (201001251654) - Maliculous Charset probing logic.
							// debug_r($partArr[$viewpart]);
							// echo '<pre>$partArr[$viewpart][charset] : '.$partArr[$viewpart]['charset'].'</pre>';

							/*
							$preProbe = mb_detect_encoding($myBod);

							// echo '<pre>$preProbe : '.$preProbe.'</pre>';
							if ($preProbe != 'UTF-8') {
									$probedCharset = mb_detect_encoding($myBod,"ASCII, UTF-8, Big5, GB2312, GBK");
							} else {
									$probedCharset = $preProbe;
							}
							// echo '<pre>ProbedCharset : '.$probedCharset.'</pre>';
							if (in_array(strtoupper($probedCharset),array('BIG5, BIG-5','GBK','CP936'))) {
									if (strtoupper($probedCharset) == 'BIG-5' || strtoupper($probedCharset) == 'BIG5') {
											$myBod = @iconv('Big5', 'UTF-8', $myBod);
									} else if (strtoupper($probedCharset) == 'GBK' || strtoupper($probedCharset) == 'CP936') {
											$myBod = @iconv('GBK', 'UTF-8', $myBod);
									}
							} else {
									$myBod = @mb_convert_encoding($myBod,'UTF-8',$probedCharset);
							}
							*/

                           if ($transform)
                           {
                               $htmltransform = true;
                               $myBod = str_replace(array('<','>'),array('&lt;','&gt;'),$myBod); // for plain text message, change < and > to &lt; and &gt; to avoid content becomes real html tags
                               if (!$htmltransform)
                               {
                                    if ($fwd == false)
                                    {
                                        $myBod = "<code class='dispmsg'>".$myBod."</code>";
                                        $myBod = str_replace("\n","<br>",$myBod);
                                    }
                               }
                               if(!$sys_custom['iMail_DisableUrlTransformForPlainTextMessage'])
                               {
                               		$myBod = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-]+)/i","$1http://$2",    $myBod);
                               		$myBod = preg_replace("/([\w]+:\/\/[\w-?%:&;#~=\.\/\@]+[\w\/])/i","<A TARGET=\"_blank\" HREF=\"$1\">$1</A>", $myBod);
                               }
                               //Old Pattern Finder
                               //$myBod = preg_replace("/([\w-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3}))/i","<A    HREF=\"compose.php?to=$1\">$1</A>",$myBod);
                               //$myBod= eregi_replace('(<)?([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})(>)?','\\1<a href="compose.php?to=\\2">\\2</a>\\4', $myBod);
                               $myBod= preg_replace('/(<)?([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})(>)?/i','\\1<a href="compose.php?to=\\2">\\2</a>\\4', $myBod);

                           }

                           if ($htmltransform)
                           {
                               /*** GET RID OF EXCESS ITEMS IN HTML EMAILS ***/
                               #$myBod = preg_replace ("/(<\/?)(\w+)([^>]*>)/e", "'\\1'.strtoupper('\\2').'\\3'", $myBod);
                               $myBod = str_replace ("<HTML>", "", $myBod);
                               $myBod = str_replace ("</HTML>", "", $myBod);
                               $myBod = preg_replace("'<HTML([^>]*?>)'si", "", $myBod);
                               $myBod = preg_replace ("'<HEAD[^>]*?>.*?</HEAD>'si", "", $myBod);
                               $myBod = preg_replace ("'<SCRIPT[^>]*?>.*?</SCRIPT>'si", "", $myBod);
                               $myBod = preg_replace ("'<STYLE[^>]*?>.*?</STYLE>'si", "", $myBod);
                               $myBod = preg_replace("'<BODY([^>]*?>)'si", "<DIV$1 ", $myBod);
                               $myBod = str_replace ("</BODY>", "</DIV>", $myBod);
                               #$myBod = preg_replace ('/cid:(.*?)("|;|\s|\\\|\')/e', "'view_img.php?mailbox=$mailbox&id=$id&cid='.urlencode('\\1').'\\2'", $myBod);
                               #echo "<!-- checkbody\n";
                               #echo $myBod;
                               #echo "--->";
                               $myBod = preg_replace ('/background=(.*?)(\'|"|;|\s|\\\)/e', "'style=\"background-image: url('.'\\1'.');\"'", $myBod);
                               $myBod= str_replace("mailto:", "compose.php?targetemail=",$myBod);
                               #$myBod = stripslashes($myBod);
                               $myBod = str_replace("\\\"","\"",$myBod);
                           }

                           /* View Attachment part
                           if ($totalviewed > 0 && $fwd == false)
                           {
                                echo "<hr><br>\n";
                                echo "<table width=100% cellpadding=3 cellspacing=0 border=0><tr bgcolor='#cccccc'><td>";
                                //echo "<a href='view_img?mailbox=".$mailbox."&id=".$id."&pid=".$p["partnum"]."' target='_blank'><b>View Part: ".$tmp."</b></a></td>";
                                echo "<b>Viewing Part: ".$tmp."</b></td>";
                                echo "<td align=right><small>Content-Type: <b>".strtolower($type[$p["type"]]."/".$p["subtype"])."</b></small></td></tr></table>";

                           }
                        if ($totalviewed > 0 && $fwd == 'true')
                        {
                                echo "\n";
                                return;
                        }
                        echo $myBod;
                        if ($fwd == 'true')
                                echo "\n";
                        else echo "<br>";
                        */
                        $result_message .= $myBod;
                        $transform = false;
                        $totalviewed++;
                      }

                      $viewpart++;
               }
               return $result_message;
      }

	  function get_raw_message($inbox, $uid, $partArr){

		$this->is_html_message = -1;
               $viewpart = 0;
               $totalviewed = 0;
               $result_message = "";
               while ($partArr[$viewpart] != "")
               {
                      $p = $partArr[$viewpart];

                      #if ((($p["type"] == 0 && strtolower($p["subtype"]) == ("plain" || "html")) || (strtolower($p["disposition"]) == "inline")) && strtolower($p["disposition"])!='attachment')
                      if ((($p["type"] == 0 && strtolower($p["subtype"]) == ("plain" || "html")) ) && strtolower($p["disposition"])!='attachment')
                      {
                           $htmltransform = false;
                           $this->is_html_message = -1;
                           $this->message_charset = $p["charset"];

                           if ($p["type"] == 0 && strtolower($p["subtype"]) == "plain")
                           {
                               $transform = true;
                           }
                           if (strtolower($p["subtype"]) == "html")
                           {
                               $htmltransform = true;
                               $this->is_html_message = 1;
                           }

                           /*Fix for Inaccurate Part number finding*/
                           $tmp = $p["partnum"];
                           while (strlen($new = str_replace("..",".",$tmp)) < strlen($tmp))
                                $tmp = $new;

                           $myBod = imap_fetchbody($inbox, $uid, (string)($tmp));

                           $myBod = $this->mime_decode_msg($myBod, $this->mime_encoding[$partArr[$viewpart]["encoding"]]);

                           /*
                           	// debug_r($myBod);
                           	// debug_r($partArr[$viewpart]["charset"]);
							### Modified By Michael Cheung (20091117) - cater Sinamail incorrect charset
                           	if (strtolower($partArr[$viewpart]["charset"]) != "utf-8") {
								// var_dump($myBod);
								// var_dump(mb_detect_encoding($myBod, "BIG-5, JIS, eucjp-win, sjis-win"));

								if (mb_detect_encoding($myBod, "BIG-5, JIS, eucjp-win, sjis-win")) {
									$myBod = @mb_convert_encoding($myBod,"UTF-8",mb_detect_encoding($myBod, "BIG-5, JIS, eucjp-win, sjis-win"));
								} else {
									$myBod = @mb_convert_encoding($myBod,"UTF-8",$partArr[$viewpart]["charset"]);
								}

							} else {
								$myBod = $myBod;
							}
							*/

							if ($transform)
							{
								$htmltransform = true;
								if (!$htmltransform)
								{
                                    if ($fwd == false)
                                    {
                                        $myBod = "<code class='dispmsg'>".$myBod."</code>";
                                        $myBod = str_replace("\n","<br>",$myBod);
                                    }
								}
								$myBod = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-]+)/i","$1http://$2",    $myBod);
								$myBod = preg_replace("/([\w]+:\/\/[\w-?%:&;#~=\.\/\@]+[\w\/])/i","<A TARGET=\"_blank\" HREF=\"$1\">$1</A>", $myBod);

								//Old Pattern Finder
								//$myBod = preg_replace("/([\w-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3}))/i","<A    HREF=\"compose.php?to=$1\">$1</A>",$myBod);
								//$myBod= eregi_replace('(<)?([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})(>)?','\\1<a href="compose.php?to=\\2">\\2</a>\\4', $myBod);
								$myBod= preg_replace('/(<)?([_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3})(>)?/i','\\1<a href="compose.php?to=\\2">\\2</a>\\4', $myBod);
							}

							if ($htmltransform)
							{
								/*** GET RID OF EXCESS ITEMS IN HTML EMAILS ***/
								#$myBod = preg_replace ("/(<\/?)(\w+)([^>]*>)/e", "'\\1'.strtoupper('\\2').'\\3'", $myBod);
								$myBod = str_replace ("<HTML>", "", $myBod);
								$myBod = str_replace ("</HTML>", "", $myBod);
								$myBod = preg_replace("'<HTML([^>]*?>)'si", "", $myBod);
								$myBod = preg_replace ("'<HEAD[^>]*?>.*?</HEAD>'si", "", $myBod);
								$myBod = preg_replace ("'<SCRIPT[^>]*?>.*?</SCRIPT>'si", "", $myBod);
								$myBod = preg_replace ("'<STYLE[^>]*?>.*?</STYLE>'si", "", $myBod);
								$myBod = preg_replace("'<BODY([^>]*?>)'si", "<DIV$1", $myBod);
								$myBod = str_replace ("</BODY>", "</DIV>", $myBod);
								#$myBod = preg_replace ('/cid:(.*?)("|;|\s|\\\|\')/e', "'view_img.php?mailbox=$mailbox&id=$id&cid='.urlencode('\\1').'\\2'", $myBod);
								#echo "<!-- checkbody\n";
								#echo $myBod;
								#echo "--->";
								$myBod = preg_replace ('/background=(.*?)(\'|"|;|\s|\\\)/e', "'style=\"background-image: url('.'\\1'.');\"'", $myBod);
								$myBod= str_replace("mailto:", "compose.php?targetemail=",$myBod);
								#$myBod = stripslashes($myBod);
								$myBod = str_replace("\\\"","\"",$myBod);
							}
							// debug_r($myBod);
                           	// echo "<br>================<br>$uid:<br>";
                           	// echo "encoding: ".$partArr[$viewpart]["encoding"];
                           	// print_r($partArr);
                           	// echo $myBod;


                        $result_message .= $myBod;
						// echo 'myBod : '.$myBod."\n";
                        $transform = false;
                        $totalviewed++;
                      }

                      $viewpart++;
               }
               return $result_message;
	  }
      function mime_decode_msg($msg, $encoding)
      {
				//Decode Msg Body if needed....

				if ($encoding == "base64")
				   $msg = base64_decode($msg);
				else if ($encoding == "quoted-printable")
					$msg = quoted_printable_decode($msg);
				else if ($encoding == "8bit")
					$msg = quoted_printable_decode(imap_8bit($msg));
				else {

				}
				/*
				else {
					// $msg = quoted_printable_decode($msg);
					// $msg = quoted_printable_decode(imap_8bit(iconv("UTF8","big5",$msg)));
					// $msg = quoted_printable_decode(mb_convert_encoding($msg,'UTF-8',mb_detect_encoding($msg,mb_list_encodings())));

					// $msg = quoted_printable_decode(mb_convert_encoding( $msg, "UTF-8", "UTF7-IMAP" )); #Converting Sinamail case
					// $msg = quoted_printable_decode(imap_8bit($msg));
					$msg = mb_convert_encoding($msg,'UTF-8', $encoding);
					$msg = mb_convert_encoding($msg, 'HTML-ENTITIES', 'UTF-8');
					$pattern = '/charset=' . $encoding . '/i';
					$msg = preg_replace($pattern, 'charset=utf-8' , $msg);
				}
				*/
               return $msg;
      }
      function retrieve_attachment_content($inbox, $uid, $pid)
      {
               /*Fix for Inaccurate Part number finding*/
               while (strlen($new = str_replace("..",".",$pid)) < strlen($pid))
                      $pid = $new;
               $attachment = imap_fetchbody($inbox, $uid, $pid);
               return $attachment;
      }
	function parse_message($obj,$prefix="")
	{
		if (strtolower($obj->subtype) == "alternative")
		{
			$objArr = $this->grab_alt($obj, $onlyText);
			$obj = $objArr['part'];

			if ($prefix =="") {
				$prefix = substr($objArr['pid'], 1);
			}
			else
				$prefix = $prefix.$objArr['pid'];
		}
		else
		{
			if (sizeof($obj->parts) > 0)
				foreach ($obj->parts as $count=>$p)
					$this->parse_part($p, $prefix.($count+1));
		}

		if ($prefix[strlen($prefix)-1] == ".") {

			$prefix = substr($prefix, 0, strlen($prefix)-1);
		}
		if ($prefix[0] == ".") {
			$prefix = substr($prefix, 1);

		}

		if (sizeof($obj->parts) == 0 && is_integer($obj->type)) {
			$this->create_partArray($obj, $prefix);
		}
	}
      function grab_alt($obj, $text=false)
      {
               for ($i = sizeof($obj->parts) - 1; $i > -1 ; $i--)
               {
               		if((strtolower($obj->parts[$i]->subtype) == "mixed" || strtolower($obj->parts[$i]->subtype) == "related") && $text == false)
               		{
               			$ret['pid'] = "." . ($i+1);
               			$ret['part'] = $this->parse_message($obj->parts[$i], $ret['pid'].".");
               			return $ret;
               		}

                    if (strtolower($obj->parts[$i]->subtype) == "html" && $text == false)
                    {
                        $ret['pid'] = "." . ($i+1);
                        $ret['part'] = $obj->parts[$i];
                        return $ret;
                    }
                    if (strtolower($obj->parts[$i]->subtype) == "plain")
                    {
                        $ret['pid'] = "." . ($i+1);
                        $ret['part'] = $obj->parts[$i];
                        return $ret;
                    }
               }
      }
		function parse_part($obj, $partno)
		{
			if ($obj->subtype == "ALTERNATIVE")
			{
				$objArr = $this->grab_alt($obj);
				$obj = $objArr['part'];
				$partno = $partno.$objArr['pid'];
			}
			else
			{
				if ($obj->type == TYPEMESSAGE)
					$this->parse_message($obj->parts[0], $partno.".");
				else if (sizeof($obj->parts) > 0)
					foreach ($obj->parts as $count=>$p)
						$this->parse_part($p, $partno.".".($count+1));
			}
			if ($partno[strlen($partno)-1] == ".")
				$partno = substr($partno, 0, strlen($prefix)-1);
			if ($partno[0] == ".")
				$partno = substr($partno, 1);

			if (sizeof($obj->parts) == 0 && is_integer($obj->type))
				$this->create_partArray($obj, $partno);
		}

	function create_partArray($obj, $partno)
	{
		$k = sizeof($this->parsed_part);

		if ($partno == "")
			$partno = 1;

		$this->parsed_part[$k]["disposition"] = $obj->disposition;
		$this->parsed_part[$k]["partnum"] = $partno;
		$this->parsed_part[$k]["description"] = $obj->description;
		$this->parsed_part[$k]["type"] = $obj->type;
		$this->parsed_part[$k]["subtype"] = $obj->subtype;
		$this->parsed_part[$k]["encoding"] = $obj->encoding;
		$this->parsed_part[$k]["size"] = $obj->bytes;

		if ($obj->ifid == 1)
		{
			$tmp = str_replace ("<", "", $obj->id);
			$this->parsed_part[$k]["cid"] = str_replace (">", "", $tmp);
		}
		if($obj->ifparameters == 1 && $obj->ifdparameters == 1)
		{
			//$params = $obj->parameters;
			$params = $obj->dparameters;

			foreach ($params as $p)
			{
				if(strtolower($p->attribute) == "charset")
				{
					$elements = imap_mime_header_decode(imap_utf8($p->value));
					for ($i=0; $i<count($elements); $i++)
					{
					    $this->parsed_part[$k]["charset"] = $elements[$i]->text;
					}
				}

				if((strtolower($p->attribute) == "name") || (strtolower($p->attribute) == "filename"))
				{
					$elements = imap_mime_header_decode($p->value);

					$thisFilename = "";

					for ($i=0; $i<count($elements); $i++)
					{
						if($this->parsed_part[$k]["charset"] == "")
						{
							$this->parsed_part[$k]["charset"] = $elements[$i]->charset;

							if((strtoupper($this->parsed_part[$k]["charset"]) == "GB2312") || (strtoupper($this->parsed_part[$k]["charset"]) == "GB18030"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if((strtoupper($this->parsed_part[$k]["charset"]) == "X-GBK") || (strtoupper($this->parsed_part[$k]["charset"]) == "GBK"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if(strtolower($this->parsed_part[$k]["charset"]) == "DEFAULT") {
								$this->parsed_part[$k]['filename'] .= trim($elements[$i]->text);
							}
							else
							{
								$this->parsed_part[$k]['filename'] .= @iconv($this->parsed_part[$k]["charset"],"UTF-8//TRANSLIT",trim($elements[$i]->text));
								if(!$this->parsed_part[$k]['filename'])
								{
									$this->parsed_part[$k]['filename'] = trim($elements[$i]->text);
								}
							}
						}
						else
						{
							if((strtoupper($this->parsed_part[$k]["charset"]) == "GB2312") || (strtoupper($this->parsed_part[$k]["charset"]) == "GB18030"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if((strtoupper($this->parsed_part[$k]["charset"]) == "X-GBK") || (strtoupper($this->parsed_part[$k]["charset"]) == "GBK"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if(strtoupper($this->parsed_part[$k]["charset"]) == "DEFAULT") {
								$this->parsed_part[$k]['filename'] .= trim($elements[$i]->text);
							}
							else
							{
								$this->parsed_part[$k]['filename'] .= @iconv($this->parsed_part[$k]["charset"],"UTF-8//TRANSLIT",trim($elements[$i]->text));
								if(!$this->parsed_part[$k]['filename'])
								{
									$this->parsed_part[$k]['filename'] = trim($elements[$i]->text);
								}
							}
						}
					}
				}
			}

			if(($this->parsed_part[$k]['filename'] == "") || ($this->parsed_part[$k]['filename'] == "UNKNOWN_PARAMETER_VALUE"))
			{
				//$params = $obj->dparameters;
				$params = $obj->parameters;
				$this->parsed_part[$k]["filename"] = "";

				foreach ($params as $p)
				{
					if(strtolower($p->attribute) == "charset")
					{
						$elements = imap_mime_header_decode(imap_utf8($p->value));
						for ($i=0; $i<count($elements); $i++)
						{
						    $this->parsed_part[$k]["charset"] = $elements[$i]->text;
						}
					}

					if((strtolower($p->attribute) == "name") || (strtolower($p->attribute) == "filename"))
					{
						$elements = imap_mime_header_decode($p->value);
						$thisFilename = "";

						for ($i=0; $i<count($elements); $i++)
						{
							if($this->parsed_part[$k]["charset"] == "")
							{
								$this->parsed_part[$k]["charset"] = $elements[$i]->charset;

								if((strtoupper($this->parsed_part[$k]["charset"]) == "GB2312") || (strtoupper($this->parsed_part[$k]["charset"]) == "GB18030"))
								{
									$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
								}
								else if((strtoupper($this->parsed_part[$k]["charset"]) == "X-GBK") || (strtoupper($this->parsed_part[$k]["charset"]) == "GBK"))
								{
									$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
								}
								else if (strtoupper($this->parsed_part[$k]["charset"]) == "DEFAULT"){
									$this->parsed_part[$k]['filename'] .= trim($elements[$i]->text);
								}
								else
								{
									$this->parsed_part[$k]['filename'] .= @iconv($this->parsed_part[$k]["charset"],"UTF-8//TRANSLIT",trim($elements[$i]->text));
									if(!$this->parsed_part[$k]['filename'])
									{
										$this->parsed_part[$k]['filename'] = trim($elements[$i]->text);
									}
								}
							}
							else
							{
								if((strtoupper($this->parsed_part[$k]["charset"]) == "GB2312") || (strtoupper($this->parsed_part[$k]["charset"]) == "GB18030"))
								{
									$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
								}
								else if((strtoupper($this->parsed_part[$k]["charset"]) == "X-GBK") || (strtoupper($this->parsed_part[$k]["charset"]) == "GBK"))
								{
									$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
								}
								else if(strtoupper($this->parsed_part[$k]["charset"]) == "DEFAULT") {
									$this->parsed_part[$k]['filename'] .= trim($elements[$i]->text);
								}
								else
								{
									$this->parsed_part[$k]['filename'] .= @iconv($this->parsed_part[$k]["charset"],"UTF-8//TRANSLIT",trim($elements[$i]->text));
									if(!$this->parsed_part[$k]['filename'])
									{
										$this->parsed_part[$k]['filename'] = trim($elements[$i]->text);
									}
								}
							}
							/*
							if($elements[$i]->charset != "")
							{
								$this->parsed_part[$k]["charset"] = $elements[$i]->charset;

								if((strtoupper($this->parsed_part[$k]["charset"]) == "GB2312") || (strtoupper($this->parsed_part[$k]["charset"]) == "GB18030"))
								{
									$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
								}
								else if((strtoupper($this->parsed_part[$k]["charset"]) == "X-GBK") || (strtoupper($this->parsed_part[$k]["charset"]) == "GBK"))
								{
									$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
								}
								else
								{
									$this->parsed_part[$k]['filename'] .= @iconv($this->parsed_part[$k]["charset"],"UTF-8//TRANSLIT",trim($elements[$i]->text));
									if(!$this->parsed_part[$k]['filename'])
									{
										$this->parsed_part[$k]['filename'] = trim($elements[$i]->text);
									}
								}
							}
							else
							{
								$this->parsed_part[$k]["filename"] = $p->value;
							}
							*/
						}
					}
				}
			}

			if($this->parsed_part[$k]['filename'] == ''){
				$this->parsed_part[$k]['filename'] = $this->getPartedAttachmentFileName($obj);
			}
		}
		else if ($obj->ifparameters == 1 && $obj->ifdparameters == 0)
		{
			$params = $obj->parameters;
			foreach ($params as $p)
			{
				if(strtolower($p->attribute) == "charset")
				{
					$elements = imap_mime_header_decode(imap_utf8($p->value));
					for ($i=0; $i<count($elements); $i++)
					{
					    $this->parsed_part[$k]["charset"] = $elements[$i]->text;
					}
				}
				if((strtolower($p->attribute) == "name") || (strtolower($p->attribute) == "filename"))
				{
					$elements = imap_mime_header_decode($p->value);

					$thisFilename = "";
					for ($i=0; $i<count($elements); $i++)
					{
						if($this->parsed_part[$k]["charset"] == "")
						{
							$this->parsed_part[$k]["charset"] = $elements[$i]->charset;
							if((strtoupper($this->parsed_part[$k]["charset"]) == "GB2312") || (strtoupper($this->parsed_part[$k]["charset"]) == "GB18030"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if((strtoupper($this->parsed_part[$k]["charset"]) == "X-GBK") || (strtoupper($this->parsed_part[$k]["charset"]) == "GBK"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if(strtoupper($this->parsed_part[$k]["charset"]) == "DEFAULT") {
								$this->parsed_part[$k]['filename'] .= trim($elements[$i]->text);
							}
							else
							{
								$this->parsed_part[$k]['filename'] .= @iconv($this->parsed_part[$k]["charset"],"UTF-8//TRANSLIT",trim($elements[$i]->text));
								if(!$this->parsed_part[$k]['filename'])
								{
									$this->parsed_part[$k]['filename'] = trim($elements[$i]->text);
								}
							}
						}
						else
						{
							if((strtoupper($this->parsed_part[$k]["charset"]) == "GB2312") || (strtoupper($this->parsed_part[$k]["charset"]) == "GB18030"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if((strtoupper($this->parsed_part[$k]["charset"]) == "X-GBK") || (strtoupper($this->parsed_part[$k]["charset"]) == "GBK"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if(strtoupper($this->parsed_part[$k]["charset"]) == "DEFAULT") {
								$this->parsed_part[$k]['filename'] .= trim($elements[$i]->text);
							}
							else
							{
								$this->parsed_part[$k]['filename'] .= @iconv($this->parsed_part[$k]["charset"],"UTF-8//TRANSLIT",trim($elements[$i]->text));
								if(!$this->parsed_part[$k]['filename'])
								{
									$this->parsed_part[$k]['filename'] = trim($elements[$i]->text);
								}
							}
						}
					}
				}
			}

			if($this->parsed_part[$k]['filename'] == ''){
				$this->parsed_part[$k]['filename'] = $this->getPartedAttachmentFileName($obj);
			}
		}
		else if ($obj->ifdparameters == 1)
		{
			$params = $obj->dparameters;
			foreach ($params as $p)
			{
				if(strtolower($p->attribute) == "charset")
				{
					$elements = imap_mime_header_decode(imap_utf8($p->value));
					for ($i=0; $i<count($elements); $i++)
					{
					    $this->parsed_part[$k]["charset"] = $elements[$i]->text;
					}
				}
				if((strtolower($p->attribute) == "name") || (strtolower($p->attribute) == "filename"))
				{
					$elements = imap_mime_header_decode($p->value);

					$thisFilename = "";
					for ($i=0; $i<count($elements); $i++)
					{
						if($this->parsed_part[$k]["charset"] == "")
						{
							$this->parsed_part[$k]["charset"] = $elements[$i]->charset;
							if((strtoupper($this->parsed_part[$k]["charset"]) == "GB2312") || (strtoupper($this->parsed_part[$k]["charset"]) == "GB18030"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if((strtoupper($this->parsed_part[$k]["charset"]) == "X-GBK") || (strtoupper($this->parsed_part[$k]["charset"]) == "GBK"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if(strtoupper($this->parsed_part[$k]["charset"]) == "DEFAULT") {
								$this->parsed_part[$k]['filename'] .= trim($elements[$i]->text);
							}
							else
							{
								$this->parsed_part[$k]['filename'] .= @iconv($this->parsed_part[$k]["charset"],"UTF-8//TRANSLIT",trim($elements[$i]->text));
								if(!$this->parsed_part[$k]['filename'])
								{
									$this->parsed_part[$k]['filename'] = trim($elements[$i]->text);
								}
							}
						}
						else
						{
							if((strtoupper($this->parsed_part[$k]["charset"]) == "GB2312") || (strtoupper($this->parsed_part[$k]["charset"]) == "GB18030"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}
							else if((strtoupper($this->parsed_part[$k]["charset"]) == "X-GBK") || (strtoupper($this->parsed_part[$k]["charset"]) == "GBK"))
							{
								$this->parsed_part[$k]["filename"] .= @iconv("GBK","UTF-8//TRANSLIT",trim($elements[$i]->text));
							}else if(strtoupper($this->parsed_part[$k]["charset"]) == "DEFAULT"){
								$this->parsed_part[$k]['filename'] .= trim($elements[$i]->text);
							}
							else
							{
								$this->parsed_part[$k]['filename'] .= @iconv($this->parsed_part[$k]["charset"],"UTF-8//TRANSLIT",trim($elements[$i]->text));
								if(!$this->parsed_part[$k]['filename'])
								{
									$this->parsed_part[$k]['filename'] = trim($elements[$i]->text);
								}
							}
						}
					}
				}
			}

			if($this->parsed_part[$k]['filename'] == ''){
				$this->parsed_part[$k]['filename'] = $this->getPartedAttachmentFileName($obj);
			}
		}
		else
		{
			if($this->parsed_part[$k]['filename'] == ''){
				$this->parsed_part[$k]['filename'] = $this->getPartedAttachmentFileName($obj);
			}
		}
	}

      function grab_attach($partArr)
      {
               $k = 0;
               for ($i=0; $i < sizeof($partArr); $i++)
               {
                    $p = $partArr[$i];
                    if (strtoupper($p["disposition"]) == "ATTACHMENT" || $p["type"] >= 3)
                    {
                        if ($p["filename"] != "")
                        {
                                $retArr[$k] = $this->mime_decode_msg($p,$this->mime_encoding[$attach_encoding]);
                                $k++;
                        }
                    }

				}
				return $retArr;

      }

      # Added by Ronald on 20080730
      # set use default spam level($webmail_info['bl_spam_control_level']) or custom spam level ##
      function setSpamControlSetting($enable_custom)
      {
	      global $webmail_info, $intranet_root;
	      include_once("$intranet_root/includes/libfilesystem.php");

	      $lf = new libfilesystem();
	      $location = $intranet_root."/file/";
	      $file = $location."setting_enable_custom_spam_control.txt";
	      $success = $lf->file_write($enable_custom, $file);
      }

      # Added by Ronald on 20080730
      ## retrive spam level is using default spam level($webmail_info['bl_spam_control_level']) or custom spam level ##
      function retriveSpamControlSetting()
      {
	      global $intranet_root;
	      include_once("$intranet_root/includes/libfilesystem.php");

	      $lf = new libfilesystem();
	      $SpamControlSetting = trim($lf->file_read($intranet_root."/file/setting_enable_custom_spam_control.txt"));

	      return $SpamControlSetting;
      }

      # Added by Ronald on 20080730
      ## set the custom spam level ##
      function setSpamControlLevel($spam_level)
      {
	      global $webmail_info, $intranet_root;
	      include_once("$intranet_root/includes/libfilesystem.php");

	      $lf = new libfilesystem();
	      $location = $intranet_root."/file/";
	      $file = $location."setting_spam_control_level.txt";
	      $success = $lf->file_write($spam_level, $file);
      }

      # Added by Ronald on 20080730
      function retriveSpamControlLevel()
      {
	      global $webmail_info, $intranet_root;
	      include_once("$intranet_root/includes/libfilesystem.php");

	      $lf = new libfilesystem();

	      //if($this->readSpamCheck() == "Y"){
		      //echo $this->retriveSpamControlSetting();
		      if($this->retriveSpamControlSetting() == 1)
		      {
			      $SpamControlLevel = trim($lf->file_read($intranet_root."/file/setting_spam_control_level.txt"));
		      }
		      else
		      {
			      $SpamControlLevel = $webmail_info['bl_spam_control_level'];
		      }
	      //}
	      return $SpamControlLevel;
      }

		function retriveDayInTrashByAdmin(){
	      	global $webmail_info, $intranet_root;

	      	include_once("$intranet_root/includes/libfilesystem.php");

	      	$lf = new libfilesystem();

	      	$days_in_trash = trim($lf->file_read($intranet_root."/file/iMail/days_in_trash.txt"));

	      	if($days_in_trash == ""){
		      	$days_in_trash = -1;
	      	}

	      	return $days_in_trash;
      	}


        /*
        * Get MODULE_OBJ array
        */
        function GET_MODULE_OBJ_ARR()
        {

                global $intranet_root, $PATH_WRT_ROOT, $i_Discipline_System, $CurrentPage, $iDiscipline, $LAYOUT_SKIN, $i_CampusMail_New_iMail;
                global $image_path;

                global $i_CampusMail_New_CheckMail, $i_CampusMail_New_Inbox, $i_CampusMail_New_AddressBook;
                global $i_CampusMail_Internal_Recipient_Group, $i_CampusMail_External_Recipient, $i_CampusMail_External_Recipient_Group;
                global $i_CampusMail_New_Outbox, $i_CampusMail_New_Draft, $i_CampusMail_New_Trash;
                global $i_CampusMail_New_Settings, $i_CampusMail_New_FolderManager, $i_CampusMail_New_ComposeMail;
                global $i_CampusMail_New_Mail_Search, $i_CampusMail_New_Settings_PersonalSetting;
                global $i_CampusMail_New_Settings_Signature;
                global $unreadInboxNo, $folders, $FolderID, $UserID, $i_CampusMail_New_Settings_EmailForwarding;
                global $special_option,  $iNewCampusMail;
                global $i_CampusMail_New_Settings_AutoReply, $i_CampusMail_New_Settings_POP3, $i_CampusMail_New_Settings_EmailRules;
                global $i_spam, $webmail_info, $sys_custom,$SYS_CONFIG, $Lang;
				global $libcampusmail;

                include_once($intranet_root."/includes/libcampusmail.php");

                if(!is_object($libcampusmail)){
                	$libcampusmail = new libcampusmail();
                }
                $anc = $special_option['no_anchor'] ?"":"#anc";

                //Check has webmail or not
                $lwebmail = new libwebmail();
                if ($lwebmail->has_webmail && $lwebmail->hasWebmailAccess($UserID) &&  $lwebmail->type == 3)
                {
                    include_once("../../includes/libsystemaccess.php");
                    $lsysaccess = new libsystemaccess($UserID);
                    if ($lsysaccess->hasMailRight())
                    {
                        $noWebmail = false;
                    }
                    else
                    {
                        $noWebmail = true;
                    }
                }
                else
                {
                    $noWebmail = true;
                }

                # Current Page Information init
                $PageComposeMail = 0;

                $PageCheckMail = 0;
                $PageCheckMail_Inbox = 0;
                $PageCheckMail_Outbox = 0;
                $PageCheckMail_Draft = 0;
                $PageCheckMail_Trash = 0;
                $PageCheckMail_FolderManager = 0;

                $PageAddressBook = 0;
                $PageAddressBook_InternalReceipientGroup = 0;
                $PageAddressBook_ExternalReceipientGroup = 0;
                $PageAddressBook_ExternalReceipient = 0;

                $PageSearch = 0;

                $PageSettings = 0;
                $PageSettings_PersonalSetting = 0;
                $PageSettings_Signature = 0;

                switch ($CurrentPage) {
                        case "PageComposeMail":
                                $PageComposeMail = 1;
                                break;

                        case "PageCheckMail_Inbox":
                                $PageCheckMail = 1;
                                $PageCheckMail_Inbox = 1;
                                break;
                        case "PageCheckMail_Outbox":
                                $PageCheckMail = 1;
                                $PageCheckMail_Outbox = 1;
                                break;
                        case "PageCheckMail_Draft":
                                $PageCheckMail = 1;
                                $PageCheckMail_Draft = 1;
                                break;
                        case "PageCheckMail_Trash":
                                $PageCheckMail = 1;
                                $PageCheckMail_Trash = 1;
                                break;
                        case "PageCheckMail_Spam":
                                $PageCheckMail = 1;
                                $PageCheckMail_Spam = 1;
                                break;
                        case "PageCheckMail_FolderManager":
                                $PageCheckMail = 1;
                                $PageCheckMail_FolderManager = 1;
                                break;

                        case "PageAddressBook_InternalReceipientGroup":
                                $PageAddressBook = 1;
                                $PageAddressBook_InternalReceipientGroup = 1;
                                break;

                        case "PageAddressBook_ExternalReceipientGroup":
                                $PageAddressBook = 1;
                                $PageAddressBook_ExternalReceipientGroup = 1;
                                break;

                        case "PageAddressBook_ExternalReceipient":
                                $PageAddressBook = 1;
                                $PageAddressBook_ExternalReceipient = 1;
                                break;

                        case "PageSearch":
                                $PageSearch = 1;
                                break;

                        case "PageSettings_PersonalSetting":
                                $PageSettings = 1;
                                $PageSettings_PersonalSetting = 1;
                                break;
                        case "PageSettings_Signature":
                                $PageSettings = 1;
                                $PageSettings_Signature = 1;
                                break;
                        case "PageSettings_EmailForwarding":
                                $PageSettings = 1;
                                $PageSettings_EmailForwarding = 1;
                                break;
						case "PageSettings_POP3":
                                $PageSettings = 1;
                                $PageSettings_POP3 = 1;
                                break;
						case "PageSettings_AutoReply":
                                $PageSettings = 1;
                                $PageSettings_AutoReply = 1;
                                break;
						case "PageSettings_EmailRules":
								$PageSettings = 1;
                                $PageSettings_EmailRules = 1;
                                break;
                }

                /*
                structure:
                $MenuArr["Section"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
                $MenuArr["Section"]["Child"]["name"] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)", "haveSubMenu");
                */

                /*

                */

                ### No of new email (to be set)
                /*
                ### old version - only show the new mail no.
                $unreadInboxNo = $iNewCampusMail;
                if ($unreadInboxNo != "")
                {
                        $NoOfNewMail = $unreadInboxNo;
                } else {
                        $NoOfNewMail = 0;
                }
                if ($NoOfNewMail > 0) $NewMailText = " (".$NoOfNewMail.")";
                */

//                $li = new libdb();
                ### New version - show no of unread mail in Inbox
                $NewMailText = "";

//                $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '2' AND RECORDSTATUS IS NULL AND DELETED = 0";
//                $arr_result = $li->returnVector($sql);

//                if(sizeof($arr_result)>0)
//                {
//	                $NewMailNo = $arr_result[0];
//                }else{
//	                $NewMailNo = 0;
//                }
				$NewMailNo = $libcampusmail->GetCampusMailNewMailCount($_SESSION['UserID'], 2); // count new mail in Inbox
                if ($NewMailNo > 0) $NewMailText = " (".$NewMailNo.")";

                $libcampusmail->GetCampusMailNewMailCount($_SESSION['UserID'], 0); // count new/unread mail in Outbox

                ### New version - show no of unread mail in Draft
                $NewDraftMailText = "";

                //$sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '1' AND Deleted <> 1 AND RECORDSTATUS IS NULL";
//                $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '1' AND Deleted <> 1";
//                $arr_result = $li->returnVector($sql);

//                if(sizeof($arr_result)>0)
//                {
//	                $NewDraftMailNo = $arr_result[0];
//                }else{
//	                $NewDraftMailNo = 0;
//                }

				$NewDraftMailNo = $libcampusmail->GetCampusMailNewMailCount($_SESSION['UserID'], 1); // count new mail in Draft
                if ($NewDraftMailNo > 0) $NewDraftMailText  = " (".$NewDraftMailNo.")";

                ### New version - show no of unread mail in Trash
                $NewTrashMailText = "";

//                $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND DELETED = 1 AND RECORDSTATUS IS NULL";
//                $arr_result = $li->returnVector($sql);

//                if(sizeof($arr_result)>0)
//                {
//	                $NewTrashMailNo = $arr_result[0];
//                }else{
//	                $NewTrashMailNo = 0;
//                }
				$NewTrashMailNo = $libcampusmail->GetCampusMailNewMailCount($_SESSION['UserID'], -1); // count new/unread mail in Trash
                if ($NewTrashMailNo > 0) $NewTrashMailText  = " (".$NewTrashMailNo.")";

                ### Show unread mail no. in SPAM folder (if $webmail_info['bl_spam'] = ture)
                $NewSpamMailNo = "";
                if($webmail_info['bl_spam'])
                {
//	                $li = new libdb();

//	                $sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserID = '$UserID' AND UserFolderID = '-2' AND Deleted <> 1 AND RECORDSTATUS IS NULL";
//	                $arr_result = $li->returnVector($sql);

//	                if(sizeof($arr_result)>0)
//	                {
//		                $NewSpamMailNo = $arr_result[0];
//	                }else{
//		                $NewSpamMailNo = 0;
//	                }
	                $NewSpamMailNo = $libcampusmail->GetCampusMailNewMailCount($_SESSION['UserID'], -2); // count new/unread mail in Spam
	                if($NewSpamMailNo > 0) $NewSpamMailText = " (".$NewSpamMailNo.")";
                }

                # Menu information
                if($SYS_CONFIG['Mail']['hide_imail']!==true)
                {
	                if (auth_sendmail())
	                {
	                        $MenuArr["ComposeMail"] = array($i_CampusMail_New_ComposeMail, $PATH_WRT_ROOT."home/imail/compose.php", $PageComposeMail, "$image_path/{$LAYOUT_SKIN}/iMail/icon_compose.gif");
	                }
                }

				$CheckMailPath = $SYS_CONFIG['Mail']['hide_imail']===true?"": $PATH_WRT_ROOT."home/imail/checkmail.php";

                $MenuArr["CheckMail"] = array($i_CampusMail_New_CheckMail, $CheckMailPath, $PageCheckMail, "$image_path/{$LAYOUT_SKIN}/iMail/icon_check_mail.gif");
                $MenuArr["CheckMail"]["Child"]["Inbox"] = array($i_CampusMail_New_Inbox.$NewMailText, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=2", $PageCheckMail_Inbox, "$image_path/{$LAYOUT_SKIN}/iMail/icon_inbox.gif");
                $MenuArr["CheckMail"]["Child"]["Outbox"] = array($i_CampusMail_New_Outbox, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=0", $PageCheckMail_Outbox, "$image_path/{$LAYOUT_SKIN}/iMail/icon_outbox.gif");
                if($SYS_CONFIG['Mail']['hide_imail']!==true)
				{
                	$MenuArr["CheckMail"]["Child"]["Draft"] = array($i_CampusMail_New_Draft.$NewDraftMailText, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=1", $PageCheckMail_Draft, "$image_path/{$LAYOUT_SKIN}/iMail/icon_draft.gif");
				}
                $MenuArr["CheckMail"]["Child"]["Trash"] = array($i_CampusMail_New_Trash.$NewTrashMailText, $PATH_WRT_ROOT."home/imail/trash.php", $PageCheckMail_Trash, "$image_path/{$LAYOUT_SKIN}/iMail/btn_trash.gif");
                if($webmail_info['bl_spam']){
                	$MenuArr["CheckMail"]["Child"]["Spam"] = array($i_spam['FolderSpam'].$NewSpamMailText, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=-2", $PageCheckMail_Spam, "$image_path/$LAYOUT_SKIN/iMail/icon_spam.gif");
            	}
                $MenuArr["CheckMail"]["Child"]["FolderManager"] = array($i_CampusMail_New_FolderManager, $PATH_WRT_ROOT."home/imail/folder.php", $PageCheckMail_FolderManager, "$image_path/{$LAYOUT_SKIN}/iMail/icon_folder_open.gif");

                ### Child Folder Array (to be set)
                /*
                structure:
                $ChildFolderArr[] = array("PageTitle", "PageLink", "isCurrentPage", "SelfDefineImg ('' for default img)");
                */

                //$ChildFolderArr[] = array($i_CampusMail_New_Draft, $PATH_WRT_ROOT."home/imail/", 0, "$image_path/{$LAYOUT_SKIN}/iMail/icon_folder_close.gif");
                //$ChildFolderArr[] = array($i_CampusMail_New_Trash, $PATH_WRT_ROOT."home/imail/", 1, "$image_path/{$LAYOUT_SKIN}/iMail/icon_folder_close.gif");

                if (is_array($folders) && count($folders) >0)
                {
                        for ($i=0;$i<count($folders);$i++)
                        {
                                if (($folders[$i][0] != 0) && ($folders[$i][0] != 1) && ($folders[$i][0] != 2) && ($folders[$i][0] != -1))
                                {
	                                	### New/Unread Mail in user define folders
	                                	$NewFolderMailText = "";
//	                                	$sql = "SELECT COUNT(*) FROM INTRANET_CAMPUSMAIL WHERE UserFolderID = '".$folders[$i][0]."' AND RecordStatus IS NULL AND DELETED = 0";
//	                                	$arr_result = $li->returnVector($sql);

//                						if(sizeof($arr_result)>0)
//                						{
//							                $NewFolderMailNo = $arr_result[0];
//						                }else{
//							                $NewFolderMailNo = 0;
//						                }
										$NewFolderMailNo = $libcampusmail->GetCampusMailNewMailCount($_SESSION['UserID'], $folders[$i][0]); // count new/unread mail in this user folder
						                if ($NewFolderMailNo > 0) $NewFolderMailText  = " (".$NewFolderMailNo.")";

                                        if ($FolderID == $folders[$i][0] && $FolderID != "")
                                        {
                                                //$ChildFolderArr[] = array($folders[$i][1].$NewFolderMailText, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=".$folders[$i][0], 1, "$image_path/{$LAYOUT_SKIN}/iMail/icon_folder_close.gif");
                                                $temp = get_wrap_content($folders[$i][1],10," ",1);
                                                /*
                                                $temp = "";
                                                $temp_arr = array();
                                                if(mb_strlen($folders[$i][1],"utf8")>5){
                                                	$str_len = mb_strlen($folders[$i][1],"utf8");
                                                	for($a=0; $a<ceil($str_len/5); $a++)
                                                	{
                                                		$start_pos = 5*($a);
                                                		$end_pos = 5;

                                                		$temp_arr[] = mb_substr($folders[$i][1],$start_pos,$end_pos,"utf8");
                                                	}
                                                	$temp = implode("<br>",$temp_arr);
                                                }else{
                                                	$temp = $folders[$i][1];
                                                }
                                                */
                                                $ChildFolderArr[] = array($temp.$NewFolderMailText, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=".$folders[$i][0], 1, "$image_path/{$LAYOUT_SKIN}/iMail/icon_folder_close.gif");
                                        } else {
                                                //$ChildFolderArr[] = array($folders[$i][1].$NewFolderMailText, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=".$folders[$i][0], 0, "$image_path/{$LAYOUT_SKIN}/iMail/icon_folder_close.gif");
                                                $temp = get_wrap_content($folders[$i][1],10," ",1);
                                                /*
                                                $temp = "";
                                                $temp_arr = array();
                                                if(mb_strlen($folders[$i][1],"utf8")>5){
                                                	$str_len = mb_strlen($folders[$i][1],"utf8");
                                                	for($a=0; $a<ceil($str_len/5); $a++)
                                                	{
                                                		$start_pos = 5*($a);
                                                		$end_pos = 5;

                                                		$temp_arr[] = mb_substr($folders[$i][1],$start_pos,$end_pos,"utf8");
                                                	}
                                                	$temp = implode("<br>",$temp_arr);
                                                }else{
                                                	$temp = $folders[$i][1];
                                                }
                                                */
                                                $ChildFolderArr[] = array($temp.$NewFolderMailText, $PATH_WRT_ROOT."home/imail/viewfolder.php?FolderID=".$folders[$i][0], 0, "$image_path/{$LAYOUT_SKIN}/iMail/icon_folder_close.gif");
                                        }
                                }
                        }
                }

                $MenuArr["CheckMail"]["Child"]["FolderManager"]["child"] = $ChildFolderArr;

				if($SYS_CONFIG['Mail']['hide_imail']!==true)
				{
	                $MenuArr["AddressBook"] = array($i_CampusMail_New_AddressBook, "", $PageAddressBook, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_book.gif");
	                $MenuArr["AddressBook"]["Child"]["InternalRecipientGroup"] = array($i_CampusMail_Internal_Recipient_Group, $PATH_WRT_ROOT."home/imail/groupalias.php?aliastype=0&TabID=1$anc", $PageAddressBook_InternalReceipientGroup, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_ingroup.gif");
	                if(!$noWebmail)
	                {
	                        $MenuArr["AddressBook"]["Child"]["ExternalRecipientGroup"] = array($i_CampusMail_External_Recipient_Group, $PATH_WRT_ROOT."home/imail/groupalias.php?aliastype=1&TabID=3$anc", $PageAddressBook_ExternalReceipientGroup, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_exgroup.gif");
	                        $MenuArr["AddressBook"]["Child"]["ExternalRecipient"] = array($i_CampusMail_External_Recipient, $PATH_WRT_ROOT."home/imail/addressbook.php", $PageAddressBook_ExternalReceipient, "$image_path/{$LAYOUT_SKIN}/iMail/icon_address_ex.gif");
	                }
				}

	                $MenuArr["Search"] = array($i_CampusMail_New_Mail_Search, $PATH_WRT_ROOT."home/imail/search.php", $PageSearch, "$image_path/{$LAYOUT_SKIN}/iMail/btn_view_source.gif");

				if($SYS_CONFIG['Mail']['hide_imail']!==true)
				{
	                $MenuArr["Settings"] = array($i_CampusMail_New_Settings, "", $PageSettings, "$image_path/{$LAYOUT_SKIN}/iMail/icon_preference.gif");

	                $MenuArr["Settings"]["Child"]["PersonalSetting"] = array($i_CampusMail_New_Settings_PersonalSetting, $PATH_WRT_ROOT."home/imail/pref.php", $PageSettings_PersonalSetting);
	                $MenuArr["Settings"]["Child"]["Signature"] = array($i_CampusMail_New_Settings_Signature, $PATH_WRT_ROOT."home/imail/pref_signature.php", $PageSettings_Signature);
	                if (!$noWebmail)
	                {
	                        $MenuArr["Settings"]["Child"]["EmailForwarding"] = array($i_CampusMail_New_Settings_EmailForwarding, $PATH_WRT_ROOT."home/imail/pref_email_forwarding.php", $PageSettings_EmailForwarding);
	                        $MenuArr["Settings"]["Child"]["POP3"] = array($i_CampusMail_New_Settings_POP3, $PATH_WRT_ROOT."home/imail/pref_pop3.php", $PageSettings_POP3);
		                    if($webmail_info['actype'] == "postfix")
			                {
		                        $MenuArr["Settings"]["Child"]["AutoReply"] = array($i_CampusMail_New_Settings_AutoReply, $PATH_WRT_ROOT."home/imail/pref_auto_reply.php", $PageSettings_AutoReply);
		                    }
		                    if($sys_custom['iMail_MailRule']){
		                    	$MenuArr["Settings"]["Child"]["EmailRules"] = array($i_CampusMail_New_Settings_EmailRules, $PATH_WRT_ROOT."home/imail/pref_email_rules.php", $PageSettings_EmailRules);
	                    	}
	                }
				}

                # Display number of unseen mails at document title
                $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
                $js.= 'document.title="eClass iMail";'."\n";
                $js.= '</script>'."\n";

                # module information
                if($SYS_CONFIG['Mail']['hide_imail']!==true)
                	$MODULE_OBJ['title'] = $i_CampusMail_New_iMail.$js;
                else
                	$MODULE_OBJ['title'] = $Lang['Gamma']['iMailArchive'].$js;
                $MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_iMail.gif";
                if($SYS_CONFIG['Mail']['hide_imail']!==true)
                	$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/imail/index.php";
                else
                	$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/imail/viewfolder.php";
                //$MODULE_OBJ['menu'] = array();
                $MODULE_OBJ['menu'] = $MenuArr;

                return $MODULE_OBJ;
        }

        /* tempory disable by ronald @ 20090811
        function Get_Mail_Format($string, $encoding='UTF-8') {
        	$string = str_replace(" ", "_", trim($string)) ;
			// We need to delete "=\r\n" produced by imap_8bit() and replace '?'
			$string = str_replace("?", "=3F", str_replace("=\r\n", "", imap_8bit($string))) ;

			// Now we split by \r\n - i'm not sure about how many chars (header name counts or not?)
			$string = chunk_split($string, 73);
			// We also have to remove last unneeded \r\n :
			$string = substr($string, 0, strlen($string)-2);
			// replace newlines with encoding text "=?UTF ..."
			$string = str_replace("\r\n", "?=".chr(13).chr(10)." =?".$encoding."?Q?", $string) ;

			return '=?'.$encoding.'?Q?'.$string.'?=';
		}
		*/

	function Get_To_Cache_Address_List($elements)
	{
		for ($i=0; $i< sizeof($elements); $i++)
		{
    		// Display field
    		$DisplayName = (trim($elements[$i]->personal) == "")? "": '"'.$elements[$i]->personal.'" ';
    		$DisplayName = imap_utf8($DisplayName);
			$Subfix = (trim($elements[$i]->host) == "")? "": '@'.$elements[$i]->host;
			$MailBox = (trim($elements[$i]->mailbox) == "")? "": $elements[$i]->mailbox;

			if ($Subfix == "" && $DisplayName == "" && $MailBox == "")
				$Address = "";
			else {
				if($DisplayName != "")
					$Address = $DisplayName." <".$elements[$i]->mailbox."@".$elements[$i]->host.">";
				else
					$Address = $elements[$i]->mailbox."@".$elements[$i]->host." <".$elements[$i]->mailbox."@".$elements[$i]->host.">";
			}

			if ($Address != "") {
				if (trim($ReturnVal['Actual']) == "")
					$ReturnVal['Actual'] = $Address;
				else
					$ReturnVal['Actual'] .= ', '.$Address;
			}

			// sorting field
			$DisplayName = (trim($elements[$i]->personal) == "")? "": $elements[$i]->personal;
			if ($DisplayName == "")
				$Address = $elements[$i]->mailbox."@".$elements[$i]->host;
			else
				$Address = $DisplayName." <".$elements[$i]->mailbox."@".$elements[$i]->host.">";

			if ($i == 0)
				$ReturnVal['Sort'] = $Address;
			else
				$ReturnVal['Sort'] .= ', '.$Address;
		}
		return $ReturnVal;
	}

	### Below is start to get all email address (similar to group alias) ###
	function GetAllTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$all_teacher_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", CONCAT(all_user.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
			}else{
				# Staff
				$all_teacher_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", CONCAT(all_user.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
			}
		} else if($TargetUserType == 2) {
			# Student
			$all_teacher_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", CONCAT(all_user.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
		} else if($TargetUserType == 3) {
			# Parent
			$all_teacher_sql = "(SELECT all_user.UserID, ".getNameFieldWithClassNumberByLang("all_user.").", CONCAT(all_user.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
		} else{
			## Unknown identity
			$js_all_teacher = "";
		}

		$result = $li->returnArray($all_teacher_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_teacher = $js_all_teacher."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_teacher = $js_all_teacher.$final_email."; ";
			}
		}else{
			$js_all_teacher = "";
		}

		return $js_all_teacher;
	}

	function GetAllSameFormTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				$sub_sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
				$arrTargetYearClassID = $li->returnVector($sub_sql);
				$targetYearClassID = implode(",",$arrTargetYearClassID);
				# Teacher
				$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
			}else{
				# Staff
				$js_all_form_teacher = "";
			}
		}else if($TargetUserType == 2){
			# Student
			$sub_sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sub_sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);
			$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else if($TargetUserType == 3){

			# Parent
			$sub_sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sub_sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);
			$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else{
			## Unknown identity
			$js_all_form_teacher = "";
		}

		$result = $li->returnArray($form_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_form_teacher = $js_all_form_teacher."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_form_teacher = $js_all_form_teacher.$final_email."; ";
			}
		}else{
			$js_all_form_teacher = "";
		}

		return $js_all_form_teacher;
	}

	function GetAllSameClassTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$sub_sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID'";
				$arrTargetYearClassID = $li->returnVector($sub_sql);
				$targetYearClassID = implode(",",$arrTargetYearClassID);
				$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
			}else{
				#Staff
				$class_sql = "";
			}
		}else if($TargetUserType == 2){
			# Student
			$sub_sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sub_sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);
			$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else if($TargetUserType == 3){
			# Parent
			$sub_sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sub_sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);
			$class_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else{
			# Identity Unknown
			$class_sql = "";
		}

		$result = $li->returnArray($class_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_class_teacher = $js_all_class_teacher."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_class_teacher = $js_all_class_teacher."$final_email"."; ";
			}
		}else{
			$js_all_class_teacher = "";
		}

		return $js_all_class_teacher;
	}

	function GetAllSameSubjectTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				$sub_sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
				$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
				$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
				# Teacher
				$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
			}else{
				# Staff
				$subject_sql = "";
			}
		}else if($TargetUserType == 2){
			$sub_sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
			# Student
			$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else if($TargetUserType == 3){
			$sub_sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
			# Parent
			$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else{
			# Identity Unknown
			$subject_sql = "";
		}

		$result = $li->returnArray($subject_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_subject_teacher = $js_all_subject_teacher."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_subject_teacher = $js_all_subject_teacher."$final_email"."; ";
			}
		}else{
			$js_all_subject_teacher = "";
		}

		return $js_all_subject_teacher;
	}

	function GetAllSameSubjectGroupTeacherEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$sub_sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $uid AND b.YearTermID = $CurrentTermID";
				$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
				$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
				$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
			}else{
				# Staff
				$subject_group_sql = "";
			}
		} else if($TargetUserType == 2) {
			# Student
			$sub_sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = $uid AND b.YearTermID = $CurrentTermID";
			$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
			$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		} else if($TargetUserType == 3) {
			# Parent
			$sub_sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
			$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		} else {
			# identity unknown
			$subject_group_sql = "";
		}

		$result = $li->returnArray($subject_group_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_subject_group_teacher = $js_all_subject_group_teacher."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_subject_group_teacher = $js_all_subject_group_teacher."$final_email"."; ";
			}
		}else{
			$js_all_subject_group_teacher = "";
		}

		return $js_all_subject_group_teacher;
	}

	function GetAllStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

		$all_student_sql = "(SELECT DISTINCT UserID, ".getNameFieldWithClassNumberByLang().", CONCAT(UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND (ClassName != '' OR ClassName != NULL) AND (ClassNumber != '' OR ClassNumber != NULL) ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName)";
		$result = $li->returnArray($all_student_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_student = $js_all_student."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_student = $js_all_student."$final_email"."; ";
			}
		}else{
			$js_all_student = "";
		}

		return $js_all_student;
	}

	function GetAllSameFormStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# Teacher
				$sub_sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
				$arrTargetYearClassID = $li->returnVector($sub_sql);
				$targetYearClassID = implode(",",$arrTargetYearClassID);
				$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
			} else {
				# Staff
				$form_sql = "";
			}
		}else if($TargetUserType == 2){
			$sub_sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sub_sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);
			$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}else if($TargetUserType == 3){
			$sub_sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$uid' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sub_sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);
			$form_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}else{
			$form_sql = "";
		}

		$result = $li->returnArray($form_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_form_student = $js_all_same_form_student."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_same_form_student = $js_all_same_form_student."$final_email"."; ";
			}
		}else{
			$js_all_same_form_student = "";
		}

		return $js_all_same_form_student;
	}

	function GetAllSameClassStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$sub_sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
				$arrTargetYearClassID = $li->returnVector($sub_sql);
				$targetYearClassID = implode(",",$arrTargetYearClassID);
				$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
			}else{
				# staff
				$class_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$sub_sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sub_sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);
			$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}else if($TargetUserType == 3){
			# parent
			$sub_sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sub_sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);
			$class_sql = "(SELECT DISTINCT a.UserID ,".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}else{
			# identity unknown
			$class_sql = "";
		}

		$result = $li->returnArray($class_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_class_student = $js_all_same_class_student."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_same_class_student = $js_all_same_class_student."$final_email"."; ";
			}
		}else{
			$js_all_same_class_student = "";
		}

		return $js_all_same_class_student;
	}

	function GetAllSameSubjectStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];
		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$sub_sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
				$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
				$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
				$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
			}else{
				# staff
				$subject_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$sub_sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
			$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}else if($TargetUserType == 3){
			# parent
			$sub_sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
			$subject_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}else{
			# identity unknown
			$subject_sql = "";
		}

		$result = $li->returnArray($subject_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_subject_student = $js_all_same_subject_student."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_same_subject_student = $js_all_same_subject_student."$final_email"."; ";
			}
		}else{
			$js_all_same_subject_student = "";
		}

		return $js_all_same_subject_student;
	}

	function GetAllSameSubjectGroupStudentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$sub_sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'";
				$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
				$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
				$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
			}else{
				# staff
				$subject_group_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$sub_sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
			$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}else if($TargetUserType == 3){
			# parent
			$sub_sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sub_sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);
			$subject_group_sql = "(SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("b.").", CONCAT(b.UserLogin,'@','".$this->mailaddr_domain."') FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}else{
			# identity unknown
			$subject_group_sql = "";
		}

		$result = $li->returnArray($subject_group_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_subject_group_student = $js_all_same_subject_group_student."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_same_subject_group_student = $js_all_same_subject_group_student."$final_email"."; ";
			}
		}else{
			$js_all_same_subject_group_student = "";
		}

		return $js_all_same_subject_group_student;
	}

	function GetAllParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

		$name_field = getParentNameWithStudentInfo("a.","c.");
		$sub_sql = "SELECT DISTINCT StudentID FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) WHERE a.RecordType = 2 AND a.RecordStatus = 1";
		$arrTargetStudentID = $li->returnVector($sub_sql);
		$targetStudentID = implode(",",$arrTargetStudentID);
		$all_sql = "(SELECT b.ParentID, $name_field , CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetStudentID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";

		$result = $li->returnArray($all_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_parent = $js_all_parent."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_parent = $js_all_parent."$final_email"."; ";
			}
		}else{
			$js_all_parent = "";
		}

		return $js_all_parent;
	}

	function GetAllSameFormParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$sql_year_id = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
				$arrTargetYearID = $li->returnVector($sql_year_id);
				$targetYearID = implode(",",$arrTargetYearID);

				$sql_year_class = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '$CurrentAcademicYearID'";
				$arrTargetYearClassID = $li->returnVector($sql_year_class);
				$targetYearClassID = implode(",",$arrTargetYearClassID);

				$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
				$arrTargetUserID = $li->returnVector($sql_user);
				$targetUserID = implode(",",$arrTargetUserID);

				$name_field = getParentNameWithStudentInfo("a.","c.");
				$form_sql = "(SELECT DISTINCT a.UserID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
			}else{
				# staff
				$form_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$sql_year_id = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearID = $li->returnVector($sql_year_id);
			$targetYearID = implode(",",$arrTargetYearID);

			$sql_year_class = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql_year_class);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql_user);
			$targetUserID = implode(",",$arrTargetUserID);

			$form_sql = "(SELECT DISTINCT b.ParentID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else if($TargetUserType == 3){
			# parent
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$sql_year_id = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$uid' AND a.AcademicYearID = $CurrentAcademicYearID";
			$arrTargetYearID = $li->returnVector($sql_year_id);
			$targetYearID = implode(",",$arrTargetYearID);

			$sql_year_class = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = $CurrentAcademicYearID";
			$arrTargetYearClassID = $li->returnVector($sql_year_class);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql_user);
			$targetUserID = implode(",",$arrTargetUserID);

			$form_sql = "(SELECT DISTINCT b.ParentID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else{
			# identity unknown
			$form_sql = "";
		}

		$result = $li->returnArray($form_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_form_parent = $js_all_same_form_parent."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_same_form_parent = $js_all_same_form_parent."$final_email"."; ";
			}
		}else{
			$js_all_same_form_parent = "";
		}

		return $js_all_same_form_parent;
	}

	function GetAllSameClassParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$name_field = getParentNameWithStudentInfo("a.","c.");
				$sql_year_class = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
				$arrTargetYearClassID = $li->returnVector($sql_year_class);
				$targetYearClassID = implode(",",$arrTargetYearClassID);

				$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
				$arrTargetUserID = $li->returnVector($sql_user);
				$targetUserID = implode(",",$arrTargetUserID);

				$class_sql = "(SELECT DISTINCT a.UserID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
			}else{
				# staff
				$class_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$sql_year_class = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql_year_class);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql_user);
			$targetUserID = implode(",",$arrTargetUserID);

			$class_sql = "(SELECT DISTINCT b.ParentID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else if($TargetUserType == 3){
			# parent
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$sql_year_class = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$uid' AND a.AcademicYearID = '$CurrentAcademicYearI'D";
			$arrTargetYearClassID = $li->returnVector($sql_year_class);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql_user = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql_user);
			$targetUserID = implode(",",$arrTargetUserID);

			$class_sql = "(SELECT DISTINCT b.ParentID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else{
			# identity unknown
			$class_sql = "";
		}

		$result = $li->returnArray($class_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_class_parent = $js_all_same_class_parent."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_same_class_parent = $js_all_same_class_parent."$final_email"."; ";
			}
		}else{
			$js_all_same_class_parent = "";
		}

		return $js_all_same_class_parent;
	}

	function GetAllSameSubjectParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$name_field = getParentNameWithStudentInfo("a.","c.");
				$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
				$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
				$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

				$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
				$arrTargetUserID = $li->returnVector($sql_user);
				$targetUserID = implode(",",$arrTargetUserID);

				$subject_sql = "(SELECT DISTINCT a.UserID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
			}else{
				# staff
				$subject_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql_user);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else if($TargetUserType == 3){
			# parnet
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$sql_subject_group = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql_user);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_sql = "(SELECT DISTINCT b.ParentID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else{
			# identity unknown
			$subject_sql = "";
		}

		$result = $li->returnArray($subject_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_subject_parent = $js_all_same_subject_parent."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_same_subject_parent = $js_all_same_subject_parent."$final_email"."; ";
			}
		}else{
			$js_all_same_subject_parent = "";
		}

		return $js_all_same_subject_parent;
	}

	function GetAllSameSubjectGroupParentEmailByUserIdentity($uid)
	{
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($uid);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($TargetUserType == 1)
		{
			if($lusertype->isTeacher($uid))
			{
				# teacher
				$name_field = getParentNameWithStudentInfo("a.","c.");
				$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'";
				$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
				$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

				$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
				$arrTargetUserID = $li->returnVector($sql_user);
				$targetUserID = implode(",",$arrTargetUserID);

				$subject_group_sql = "(SELECT DISTINCT a.UserID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
			}else{
				# staff
				$subject_group_sql = "";
			}
		}else if($TargetUserType == 2){
			# student
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$uid' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql_user);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else if($TargetUserType == 3){
			# parent
			$name_field = getParentNameWithStudentInfo("a.","c.");
			$sql_subject_group = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$uid' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql_subject_group);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql_user = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql_user);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_group_sql = "(SELECT DISTINCT b.ParentID, $name_field, CONCAT(c.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}else{
			# identity unknown
			$subject_group_sql = "";
		}

		$result = $li->returnArray($subject_group_sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_same_subject_group_parent = $js_all_same_subject_group_parent."$uemail"."; ";\
				$final_email = $uname." <".$uemail.">";
				$js_all_same_subject_group_parent = $js_all_same_subject_group_parent."$final_email"."; ";
			}
		}else{
			$js_all_same_subject_group_parent = "";
		}

		return $js_all_same_subject_group_parent;
	}

	function GetAllGroupEmail($TargetGroupID,$ToTeacher,$ToStaff,$ToStudent,$ToParent)
	{
		global $UserID;
		include_once("libusertype.php");
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$lusertype = new libusertype();
		$fcm = new form_class_manage();
		$TargetUserType = $lusertype->returnIdentity($UserID);
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($ToTeacher == 1){
			if($cond != "")
				$cond .= " OR ";
			$cond .= " (a.RecordType = 1 AND (a.Teaching = 1)) ";
		}
		if($ToStaff == 1){
			if($cond != "")
				$cond .= " OR ";
			$cond .= " (a.RecordType = 1 AND (TEACHING = 0 OR a.Teaching IS NULL)) ";
		}
		if($ToStudent == 1){
			if($cond != "")
				$cond .= " OR ";
			$cond .= " (a.RecordType = 2) ";
		}
		if($ToParent == 1){
			if($cond != "")
				$cond .= " OR ";
			$cond .= " (a.RecordType = 3) ";
		}

		if($cond != "")
			$final_cond = " AND ( $cond ) ";


		$sql = "SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.").", CONCAT(a.UserLogin,'@','".$this->mailaddr_domain."') FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.GroupID IN ($TargetGroupID) $final_cond ORDER BY a.RecordType, IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName";
		$result = $li->returnArray($sql,3);
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($uid, $uname, $uemail) = $result[$i];
				//$js_all_group_email = $js_all_group_email."$uemail"."; ";
				$final_email = $uname." <".$uemail.">";
				$js_all_group_email = $js_all_group_email."$final_email"."; ";
			}
		}else{
			$js_all_group_email = "";
		}

		return $js_all_group_email;
	}

	function returnNumOfAllTeachingStaff($UserIdentity)
    {
    	include_once("libdb.php");
    	$li = new libdb();

    	if(($UserIdentity == "Teaching") || ($UserIdentity == "NonTeaching") || ($UserIdentity == "Student") || ($UserIdentity == "Parent") || $UserIdentity == "Alumni")
    	{
    		$all_sql = "(SELECT Count(*) FROM INTRANET_USER as all_user WHERE all_user.RecordType = 1 AND all_user.RecordStatus = 1 AND all_user.Teaching = 1 ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
    	}
    	$no_of_all_teacher = $li->returnVector($all_sql);
    	return $no_of_all_teacher[0];
    }

    function returnNumOfFormTeacher($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

    	if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$form_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## Non teaching suppose cannnot send to form teacher
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$form_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$form_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}

		$no_of_form_teacher = $li->returnVector($form_sql);
    	return $no_of_form_teacher[0];
    }

    function returnNumOfClassTeacher($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();

    	if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$class_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## Non teaching suppose cannnot send to class teacher
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$class_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT b.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$class_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.YearClassID IN ($targetYearClassID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		$num_of_class_teacher = $li->returnVector($class_sql);
		return $num_of_class_teacher[0];
    }

    function returnNumOfSubjectTeacher($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## Non teaching suppose cannnot send to subject teacher
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		$num_of_subject_teacher = $li->returnVector($subject_sql);
		return $num_of_subject_teacher[0];
    }

    function returnNumOfSubjectGroupTeacher($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_group_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## Non teaching suppose cannnot send to subject group teacher
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_group_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_group_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a INNER JOIN SUBJECT_TERM_CLASS_TEACHER AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.SubjectGroupID IN ($targetSubjectGroupID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}

		$num_of_subject_group_teacher = $li->returnVector($subject_group_sql);
		return $num_of_subject_group_teacher[0];
    }

    function returnNumOfAllStudent($UserIdentity)
    {
    	include_once("libdb.php");
    	$li = new libdb();

    	if(($UserIdentity == "Teaching") || ($UserIdentity == "NonTeaching") || ($UserIdentity == "Student") || ($UserIdentity == "Parent") || $UserIdentity == "Alumni")
		{
			$all_sql = "(SELECT Count(DISTINCT UserID) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName)";
		}

		$num_of_all_student = $li->returnVector($all_sql);
		return $num_of_all_student[0];
    }

    function returnNumOfFormStudent($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_TEACHER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$form_sql = "(SELECT Count(DISTINCT a.UserID) FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## support staff suppose cannot send to form student
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE a.UserID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$form_sql = "(SELECT Count(DISTINCT a.UserID) FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT c.YearClassID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN YEAR_CLASS AS b ON (a.YearClassID = b.YearClassID) INNER JOIN YEAR_CLASS AS c ON (b.YearID = c.YearID) WHERE relation.ParentID = '$UserID' AND b.AcademicYearID = '$CurrentAcademicYearID' AND c.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$form_sql = "(SELECT Count(DISTINCT a.UserID) FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}

		$num_of_form_student = $li->returnVector($form_sql);
		return $num_of_form_student[0];
    }

    function returnNumOfClassStudent($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$class_sql = "(SELECT Count(DISTINCT a.UserID) FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## support staff suppose cannot send to class student
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$class_sql = "(SELECT Count(DISTINCT a.UserID) FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$class_sql = "(SELECT Count(DISTINCT a.UserID) FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.YearClassID IN ($targetYearClassID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}

		$num_of_class_student = $li->returnVector($class_sql);
		return $num_of_class_student[0];
    }

    function returnNumOfSubjectStudent($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_sql = "(SELECT Count(DISTINCT a.UserID) FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## support staff suppose cannot send to Subject student
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_sql = "(SELECT Count(DISTINCT a.UserID) FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_sql = "(SELECT Count(DISTINCT a.UserID) FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}

		$num_of_subject_student = $li->returnVector($subject_sql);
		return $num_of_subject_student[0];
    }

    function returnNumOfSubjectGroupStudent($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_group_sql = "(SELECT Count(DISTINCT a.UserID) FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## support staff suppose cannot send to subject group student
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_group_sql = "(SELECT Count(DISTINCT a.UserID) FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$subject_group_sql = "(SELECT Count(DISTINCT a.UserID) FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE b.RecordStatus = 1 AND a.SubjectGroupID IN ($targetSubjectGroupID) AND (b.ClassName != '' OR b.ClassName != NULL) AND (b.ClassNumber != '' OR b.ClassNumber != NULL) ORDER BY IFNULL(b.ClassName,''), IFNULL(b.ClassNumber,0), b.EnglishName)";
		}

		$num_of_subject_group_student = $li->returnVector($subject_group_sql);
		return $num_of_subject_group_student[0];
    }

    function returnNumOfAllParent($UserIdentity)
    {
    	include_once("libdb.php");
    	$li = new libdb();

    	if(($UserIdentity == "Teaching") || ($UserIdentity == "NonTeaching") || ($UserIdentity == "Student") || ($UserIdentity == "Parent") || $UserIdentity == "Alumni")
		{
			$all_sql = "(SELECT Count(DISTINCT a.UserID) FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = 3 AND a.RecordStatus = 1 ORDER BY IFNULL(c.ClassName,''), IFNULL(c.ClassNumber,0), c.EnglishName)";
		}

		$num_of_all_parent = $li->returnVector($all_sql);
		return $num_of_all_parent;
    }

    function returnNumOfFormParent($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearID = $li->returnVector($sql);
			$targetYearID = implode(",",$arrTargetYearID);

			$sql = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$form_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE a.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## Support Staff suppose cannot send to form parent
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearID = $li->returnVector($sql);
			$targetYearID = implode(",",$arrTargetYearID);

			$sql = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$form_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT a.YearID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearID = $li->returnVector($sql);
			$targetYearID = implode(",",$arrTargetYearID);

			$sql = "SELECT a.YearClassID FROM YEAR_CLASS AS a WHERE a.YearID IN ($targetYearID) AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$form_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}

		$num_of_form_parent = $li->returnVector($form_sql);
		return $num_of_form_parent[0];
    }

    function returnNumOfClassParent($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_TEACHER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$class_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## Support Staff suppose cannot send to class parent
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) WHERE b.UserID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$class_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT a.YearClassID FROM YEAR_CLASS AS a INNER JOIN YEAR_CLASS_USER AS b ON (a.YearClassID = b.YearClassID) INNER JOIN INTRANET_PARENTRELATION AS relation ON (b.UserID = relation.StudentID) WHERE relation.ParentID = '$UserID' AND a.AcademicYearID = '$CurrentAcademicYearID'";
			$arrTargetYearClassID = $li->returnVector($sql);
			$targetYearClassID = implode(",",$arrTargetYearClassID);

			$sql = "SELECT a.UserID FROM YEAR_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.YearClassID IN ($targetYearClassID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$class_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}

		$num_of_class_parent = $li->returnVector($class_sql);
		return $num_of_class_parent[0];
    }

    function returnNumOfSubjectParent($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## Support Staff suppose cannot send to subject parent
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT c.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) INNER JOIN SUBJECT_TERM AS c ON (b.SubjectID = c.SubjectID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID' AND c.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}

		$num_of_subject_parent = $li->returnVector($subject_sql);
		return $num_of_subject_parent[0];
    }

    function returnNumOfSubjectGroupParent($UserIdentity)
    {
    	global $UserID;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		$li = new libdb();
		$fcm = new form_class_manage();
		$CurrentAcademicYearID = $fcm->getCurrentAcademicaYearID();
		$arrCurrentInfo = $fcm->Get_Current_Academic_Year_And_Year_Term();
		$CurrentTermID = $arrCurrentInfo[0]['YearTermID'];

		if($UserIdentity == "Teaching")
		{
			$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_group_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "NonTeaching")
		{
			## Support Staff suppose cannot send to subject group parent
		}
		if($UserIdentity == "Student")
		{
			$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE a.UserID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_group_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}
		if($UserIdentity == "Parent")
		{
			$sql = "SELECT DISTINCT b.SubjectGroupID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_PARENTRELATION AS relation ON (a.UserID = relation.StudentID) INNER JOIN SUBJECT_TERM AS b ON (a.SubjectGroupID = b.SubjectGroupID) WHERE relation.ParentID = '$UserID' AND b.YearTermID = '$CurrentTermID'";
			$arrTargetSubjectGroupID = $li->returnVector($sql);
			$targetSubjectGroupID = implode(",",$arrTargetSubjectGroupID);

			$sql = "SELECT a.UserID FROM SUBJECT_TERM_CLASS_USER AS a INNER JOIN INTRANET_USER AS b ON (a.UserID = b.UserID) WHERE a.SubjectGroupID IN ($targetSubjectGroupID)";
			$arrTargetUserID = $li->returnVector($sql);
			$targetUserID = implode(",",$arrTargetUserID);

			$subject_group_sql = "(SELECT Count(DISTINCT b.ParentID) FROM INTRANET_USER AS a INNER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.StudentID) INNER JOIN INTRANET_USER AS c ON (b.ParentID = c.UserID) WHERE c.RecordStatus = 1 AND b.StudentID IN ($targetUserID) ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName)";
		}

		$num_of_subject_group_parent = $li->returnVector($subject_group_sql);
		return $num_of_subject_group_parent[0];
    }

	function returnNumOfAllAlumni($UserIdentity)
    {
    	include_once("libdb.php");
    	$li = new libdb();

    	if(($UserIdentity == "Teaching") || ($UserIdentity == "NonTeaching") || ($UserIdentity == "Student") || ($UserIdentity == "Parent") || $UserIdentity == "Alumni")
    	{
    		$all_sql = "(SELECT Count(*) FROM INTRANET_USER as all_user WHERE all_user.RecordType = 4 AND all_user.RecordStatus = 1 ORDER BY IFNULL(all_user.ClassName,''), IFNULL(all_user.ClassNumber,0), all_user.EnglishName)";
    	}
    	$no_of_all_alumni = $li->returnVector($all_sql);
    	return $no_of_all_alumni[0];
    }

	function SaveOriginal($CampusMailID='',$PartType='', $Content='') {
		global $intranet_root, $UserID;
		include_once("$intranet_root/includes/libfilesystem.php");
		$fs = new libfilesystem();
	    if (!is_dir("$intranet_root/file/mail/u$UserID"))
		{
			mkdir("$intranet_root/file/mail/u$UserID",0777);
		}
		$user_folder = $this->getNextFreeUserFolder($UserID, false);
		//$path = "$intranet_root/file/mail/u$UserID";
		$path = "$intranet_root/file/mail/".$user_folder;
		$fs = new libfilesystem();
		if ($CampusMailID != '') {
			$subfilepath = md5($CampusMailID).'.'.$PartType.'.'.time();
			$filename = $path.'/'.$subfilepath;
			$SaveState = $fs->file_write($Content,$filename);
			//$SaveFilePath = "u$UserID/".$subfilepath;
			$SaveFilePath = $user_folder."/".$subfilepath;
		}

		// echo '<pre>CampusMailID : '.$CampusMailID.'</pre>';
		// echo '<pre>PartType : '.$PartType.'</pre>';
		// echo '<pre>Content : '.$Content.'</pre>';
		// echo '<pre>filename : '.$filename.'</pre>';
		// echo '<pre>======================================</pre>';
		$SaveState = ($SaveFilePath != '') ? $SaveFilePath : false;
		return $SaveState;
	}

	function ReadOrignialMail($filepath='') {
		global $intranet_root, $UserID;
		include_once("$intranet_root/includes/libfilesystem.php");
		$fs = new libfilesystem();
		if ($filepath != '') {
			$path = "$intranet_root/file/mail/";
			$fullpath = $path.$filepath;
			$file = $fs->file_read($fullpath);
		}
		return $file;
	}

	function isExternalMailAvaliable(){

		if($this->has_webmail){
			if($this->type == 3){
				return true;
			}
		}
		return false;
	}

	function retrieveExternalMailAddressByUserID($uid){
		global $intranet_root;

		include_once("$intranet_root/includes/libdb.php");
		$li = new libdb();

		if($this->isExternalMailAvaliable()) {
			if($uid != ""){
				$sql = "SELECT a.UserLogin FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.UserID = '$uid'";
				$arrUserLogin = $li->returnArray($sql);

				if(sizeof($arrUserLogin)>0){
					list($UserLogin) = $arrUserLogin[0];
					$external_mail_address = $UserLogin."@".$this->login_domain;

					return $external_mail_address;
				}
			}
		}
		return "";
	}

	function retrieveExternalMailAddressByUserLogin($login){
		global $intranet_root;

		include_once("$intranet_root/includes/libdb.php");
		$li = new libdb();

		if($this->isExternalMailAvaliable()) {
			if($login != ""){
				$sql = "SELECT a.UserLogin FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.UserLogin = '$login'";
				$arrUserLogin = $li->returnArray($sql);

				if(sizeof($arrUserLogin)>0){
					list($UserLogin) = $arrUserLogin[0];
					$external_mail_address = $UserLogin."@".$this->login_domain;

					return $external_mail_address;
				}
			}
		}
		return "";
	}

	function GetWebmasterMailAddress()
	{
		global $intranet_root;
      	include_once("$intranet_root/includes/libfilesystem.php");
      	$lf = new libfilesystem();
      	$webmaster_mail = trim($lf->file_read($intranet_root."/file/email_webmaster.txt"));
      	$webmaster_name = substr($webmaster_mail,0,strpos($webmaster_mail,"@"));
      	$webmaster_full_mail = $webmaster_name." <".$webmaster_mail.">";
      	return $webmaster_full_mail;
	}

	function GetUserEmailAddress($uid, $returnEmailOnly=0)
	{
		global $intranet_root, $plugin;

		include_once("$intranet_root/includes/libdb.php");
		$li = new libdb();

		if($plugin['imail_gamma']) {
			$sql = "SELECT ".getNameFieldByLang().", IMapUserEmail FROM INTRANET_USER WHERE UserID = '$uid'";
			$SenderArray = $li->returnArray($sql,2);
			if(sizeof($SenderArray)>0) {
				list($sender_name, $sender_MailAddress) = $SenderArray[0];
				if($sender_MailAddress != "") {
					$sender_mail = $returnEmailOnly ? $sender_MailAddress : $sender_name." <".$sender_MailAddress.">";
				} else {
					$sender_mail = "";
				}
			}
		} else {
			$sql = "SELECT UserLogin, ".getNameFieldByLang()." FROM INTRANET_USER WHERE UserID = '$uid'";
			$SenderArray = $li->returnArray($sql,2);
			if(sizeof($SenderArray)>0) {
				list($sender_login, $sender_name) = $SenderArray[0];
				$sender_mail = $returnEmailOnly ? $sender_login."@".$this->login_domain : $sender_name." <".$sender_login."@".$this->login_domain.">";
			}
		}
		return $sender_mail;
	}

	/*
	 * @param string $SendTo : default 'User' - to particular user (student, parent, teacher)
	 * 						   'CCParent' - send to student and cc to parent
	 * 						   'ParentOnly' - send to parent of the student
	 */
	function sendModuleMail($ToArray,$Subject,$Message,$UseAdminEmailAsSender=1,$AttachmentPath='',$SendTo='User',$addDoNotReply=false,$showSenderAsAdmin=false)
	{
		global $intranet_root, $UserID, $SYS_CONFIG, $sys_custom, $plugin,$Lang, $globalDoNotSendToAlternativeEmail;
		global $iMail_DummyAddress, $iMail_NumOfRecipientPerTime;
		//$use_new_sendModuleMail= $SYS_CONFIG['Mail']['hide_imail']===true && $SYS_CONFIG['Mail']['use_old_sendModuleMail']!== true;

		$use_new_sendModuleMail = $plugin['imail_gamma'];

		include_once("$intranet_root/includes/libdb.php");
		include_once($intranet_root."/includes/libfilesystem.php");
		include_once($intranet_root."/includes/libcampusmail.php");

		$libcampusmail = new libcampusmail();
		$li = new libdb();
		$lfs = new libfilesystem();
		$log_text = '';

		// add Do not reply at the end of the message
		if($addDoNotReply){
			$Message .= '<br>';
			//$Message .= '<i>*'.Get_Lang_Selection("此郵件由系統發出，請勿回覆。","This is an automated email. Please DO NOT respond").'</i>';
			$Message .= '<i>*'.$Lang['EmailNotification']['DoNotReply'].'</i>';
		}

		## generate sender email ##
		if($sys_custom['GetModuleMailSenderFrom'] != "")
		{
			if( strtoupper(trim($sys_custom['GetModuleMailSenderFrom'])) === "ADMIN" )
			{
		      	$sender_mail = $this->GetWebmasterMailAddress();
		      	$UseAdminEmailAsSender = 1;
		    }
		    else if( strtoupper(trim($sys_custom['GetModuleMailSenderFrom'])) === "USER" )
		    {
		    	$UseAdminEmailAsSender = 0;
		    	if(!$use_new_sendModuleMail) {
		    		if($UserID != ""){
		    			$sender_mail = $this->GetUserEmailAddress($UserID);

						if($sender_mail == "")
			    		{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
		    		} else {
		    			$sender_mail = $this->GetWebmasterMailAddress();
		    		}
		    	} else {
		    		if($UserID != ""){
		    			$sender_mail = $this->GetUserEmailAddress($UserID);

			    		if($sender_mail == "")
			    		{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
		    		} else {
		    			$sender_mail = $this->GetWebmasterMailAddress();
		    		}
		    	}
		    }
		    else
		    {
		    	if($UseAdminEmailAsSender == 1)
				{
					$sender_mail = $this->GetWebmasterMailAddress();
				}
				else
				{
					if(!$use_new_sendModuleMail) {
						if($UserID != ""){
							$sender_mail = $this->GetUserEmailAddress($UserID);
							if($sender_mail == "")
				    		{
								$sender_mail = $this->GetWebmasterMailAddress();
							}
						}else{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
			    	} else {
			    		if($UserID != "") {
			    			$sender_mail = $this->GetUserEmailAddress($UserID);
				    		if($sender_mail == "")
				    		{
								$sender_mail = $this->GetWebmasterMailAddress();
							}
			    		} else {
			    			$sender_mail = $this->GetWebmasterMailAddress();
			    		}
			    	}
				}
		    }
		}
		else
		{
			if($UseAdminEmailAsSender == 1)
			{
		      	$sender_mail = $this->GetWebmasterMailAddress();
			}
			else
			{
				if(!$use_new_sendModuleMail) {
					if($UserID != "") {
						$sender_mail = $this->GetUserEmailAddress($UserID);
						if($sender_mail == "")
			    		{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
					} else {
						$sender_mail = $this->GetWebmasterMailAddress();
					}
		    	} else {
		    		if($UserID != "") {
		    			$sender_mail = $this->GetUserEmailAddress($UserID);
			    		if($sender_mail == "")
			    		{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
		    		} else {
		    			$sender_mail = $this->GetWebmasterMailAddress();
		    		}
		    	}
			}
		}
		if($sys_custom['SendModuleMailTraceLog']) $log_text .= 'Sender: '.$sender_mail."\n";

		// filter only send to active users

		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus='1' AND UserID IN ('".implode("','",$ToArray)."')";
		$ToArray = $li->returnVector($sql);

		$CcArray = array();
		if($SendTo=='CCParent' || $SendTo=='ParentOnly') {
			// Find parent UserIDs
			$sql = "SELECT
						DISTINCT a.ParentID
					FROM INTRANET_PARENTRELATION as a
	                INNER JOIN INTRANET_USER as b on (b.UserID=a.ParentID) AND b.RecordStatus=1
					INNER JOIN INTRANET_USER as c on (c.UserID=a.StudentID) AND c.RecordStatus=1
					WHERE c.UserID IN (".implode(",",$ToArray).")";
			$parentStudentUserID = $li->returnVector($sql);
			if($SendTo == 'CCParent') {
				$CcArray = $parentStudentUserID;
			}else if($SendTo=='ParentOnly'){
				$oldToArray = $ToArray;
				$ToArray = $parentStudentUserID;
			}
		}

		## handle receiver array
		$arr_send_internal = array();// send to intranet iMail
		$arr_external_mail = array();// send to webmail or imail plus
		if(sizeof($ToArray)>0) {
			$targetUserID = implode(",",$ToArray);

			## check external mail is avaliable, i.e. webmail or iMail plus is available
			if(!$sys_custom['SendModuleMailUseAlternativeEmail'] && ($this->isExternalMailAvaliable() || $use_new_sendModuleMail))
			{
				if($use_new_sendModuleMail) // iMail plus
				{
					# filter User whose UserType was not permitted to use gamma
					$gamma_access_right = Get_Gamma_Identity_Access_Rights();
					$user_type_arr = array(1,2,3,4);
					$permittedUserType = array();
					for($k=0; $k<count($gamma_access_right);$k++)
					{
						if($gamma_access_right[$k]==1)
							$permittedUserType[] = $user_type_arr[$k];
					}
					$permittedUserType = implode(",",(array)$permittedUserType);

					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.").", a.IMapUserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail <> '' AND a.UserID IN ($targetUserID) AND a.RecordType IN ($permittedUserType)";
					$arrUserDetails = $li->returnArray($sql,4);
				}
				else // webmail
				{
					## check user have imail account or not
					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.UserID IN ($targetUserID)";
					$arrUserDetails = $li->returnArray($sql,3);
				}

				if(sizeof($arrUserDetails)>0) {
					for($i=0; $i<sizeof($arrUserDetails); $i++) {
						list($user_id, $user_login, $user_name, $IMapEmail) = $arrUserDetails[$i];
						$external_mail_address = $user_name." <".(!empty($IMapEmail)?$IMapEmail:$user_login."@".$this->login_domain).">";
						$arr_external_mail[] = $external_mail_address;
						$arr_have_external[] = $user_id;
					}
					$arr_send_internal = array_diff($ToArray, $arr_have_external);
				}else{
					$arr_send_internal = $ToArray;
				}
			}else{
				$arr_send_internal = $ToArray;
			}
		}

		## handle CC array
		$arr_cc_send_internal = array(); // send to intranet iMail
		$arr_cc_external_mail = array(); // send to webmail or iMail plus
		if(sizeof($CcArray)>0)
		{
			$ccTargetUserID = implode(",",$CcArray);

			## check external mail is avaliable, i.e. webmail or iMail plus is available
			if(!$sys_custom['SendModuleMailUseAlternativeEmail'] && ($this->isExternalMailAvaliable() || $use_new_sendModuleMail))
			{
				if($use_new_sendModuleMail) // iMail plus
				{
					# filter User whose UserType was not permitted to use gamma
					$gamma_access_right = Get_Gamma_Identity_Access_Rights();
					$user_type_arr = array(1,2,3,4);
					$permittedUserType = array();
					for($k=0; $k<count($gamma_access_right);$k++)
					{
						if($gamma_access_right[$k]==1)
							$permittedUserType[] = $user_type_arr[$k];
					}
					$permittedUserType = implode(",",(array)$permittedUserType);

					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.").", a.IMapUserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail <> '' AND a.UserID IN ($ccTargetUserID) AND a.RecordType IN ($permittedUserType)";
					$arrCcUserDetails = $li->returnArray($sql,4);
				}
				else // webmail
				{
					## check user have imail account or not
					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.UserID IN ($ccTargetUserID)";
					$arrCcUserDetails = $li->returnArray($sql,3);
				}

				if(sizeof($arrCcUserDetails)>0) {
					for($i=0; $i<sizeof($arrCcUserDetails); $i++) {
						list($user_id, $user_login, $user_name, $IMapEmail) = $arrCcUserDetails[$i];
						$external_mail_address = $user_name." <".(!empty($IMapEmail)?$IMapEmail:$user_login."@".$this->login_domain).">";
						$arr_cc_external_mail[] = $external_mail_address;
						$arr_cc_have_external[] = $user_id;
					}
					$arr_cc_send_internal = array_diff($CcArray, $arr_cc_have_external);
				}else{
					$arr_cc_send_internal = $CcArray;
				}
			}else{
				$arr_cc_send_internal = $CcArray;
			}
		}

		## TO: Get other email address for those internal recipient
		$arrSendInternalOption2 = array(); // send to user's UserEmail
		if(sizeof($arr_send_internal)>0){
			$targetInternalUserID = implode(",",$arr_send_internal);
			$sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserID IN ($targetInternalUserID) AND UserEmail != ''";
			$arrSendInternalOption2 = $li->returnVector($sql);
		}

		## CC: Get other email address for those internal recipient
		$arrCcSendInternalOption2 = array(); // cc to user's UserEmail
		if(sizeof($arr_cc_send_internal)>0){
			$ccTargetInternalUserID = implode(",",$arr_cc_send_internal);
			$sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserID IN ($ccTargetInternalUserID) AND UserEmail != ''";
			$arrCcSendInternalOption2 = $li->returnVector($sql);
		}

		if(!$sys_custom['SendModuleMailUseAlternativeEmail'] && !$use_new_sendModuleMail) // intranet iMail
		{
			$tmp_arr_to_send_internal = array();
			$tmp_arr_cc_send_internal = array();
			$tmp_arr_bcc_send_internal = array();
			for($j=0;$j<sizeof($arr_send_internal);$j++){
				$tmp_arr_to_send_internal[] = "U".$arr_send_internal[$j];
			}
			$internal_recipient_id = implode(",",$tmp_arr_to_send_internal);
			for($j=0;$j<sizeof($arr_cc_send_internal);$j++){
				$tmp_arr_cc_send_internal[] = "U".$arr_cc_send_internal[$j];
			}
			$internal_cc_recipient_id = implode(",",$tmp_arr_cc_send_internal);
			if(!$sys_custom['SendModuleMailByBcc'])
			{
				$final_internal_recipient_id = $internal_recipient_id;
				$final_internal_cc_recipient_id = $internal_cc_recipient_id;
				$final_internal_bcc_recipient_id = '';
			}
			else
			{
				$final_internal_recipient_id = "";
				$final_internal_cc_recipient_id = "";
				$tmp_arr_bcc_send_internal = array_merge($tmp_arr_bcc_send_internal,$tmp_arr_to_send_internal,$tmp_arr_cc_send_internal);
				//$final_internal_bcc_recipient_id = implode(",",$tmp_arr_bcc_send_internal);
				$final_internal_bcc_recipient_id = '';
			}

			if($UseAdminEmailAsSender) {
				$sender_id = 0;
			} else {
				$sender_id = $UserID;
			}

			## - for intranal mail, directly insert the mail into INTRANET_CAMPUSMAIL
			if(sizeof($arr_send_internal)>0) {
				## To recipient
				foreach($arr_send_internal as $key=>$user_id) {
					$internal_result[] = $this->insertCampusMail($user_id, $sender_id, $final_internal_recipient_id, $final_internal_cc_recipient_id, $final_internal_bcc_recipient_id, '', '', '', $Subject, $Message, 'utf-8', '', '', 1, 2, $AttachmentPath);
				}
				if($sys_custom['SendModuleMailTraceLog']) $log_text .= "Campus Mail To: ".implode(",",(array)$arr_send_internal)."\n";
			}else{
				$internal_result[] = true;
			}

			if(sizeof($arr_cc_send_internal)>0) {
				## CC
				foreach($arr_cc_send_internal as $key =>$user_id){
					$internal_result[] = $this->insertCampusMail($user_id, $sender_id, $final_internal_recipient_id, $final_internal_cc_recipient_id, $final_internal_bcc_recipient_id, '', '', '', $Subject, $Message, 'utf-8', '', '', 1, 2, $AttachmentPath);
				}
				if($sys_custom['SendModuleMailTraceLog']) $log_text .= "Campus Mail CC: ".implode(",",(array)$arr_cc_send_internal)."\n";
			}
		}
		else
		{
			$internal_result[] = true;
		}


		## - Send to other email address for those internal recipient, i.e. UserEmail
		if(sizeof($arrSendInternalOption2)>0 && !$globalDoNotSendToAlternativeEmail){
			if($iMail_NumOfRecipientPerTime == "")
         		$iMail_NumOfRecipientPerTime = 150;

			if(!$sys_custom['SendModuleMailByBcc'])
			{
				//$iMail_DummyAddress
				$arrOtherInternalMail = $arrSendInternalOption2;
				$arrayCC = array();
				$arrayBCC = array();

				if($SendTo == 'CCParent') {
					$arrayCC = $arrCcSendInternalOption2;
					if((count($arrOtherInternalMail)+count($arrayCC)) > $iMail_NumOfRecipientPerTime){
						$toSend = $this->split_array($arrOtherInternalMail, $iMail_NumOfRecipientPerTime-count($arrayCC));
			         	for($i=0; $i<sizeof($toSend); $i++){
			         		if($i>0){
			         			// CC emails are sent in first batch, do not repeatly send afterwards
			         			$arrayCC = array();
			         		}
			         		$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$toSend[$i],$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
			         		if($sys_custom['SendModuleMailTraceLog']) $log_text .= "CCParent UserEmail SendMail To: ".implode(",",(array)$toSend[$i])." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
			         	}
					}else{
						$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$arrOtherInternalMail,$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
						if($sys_custom['SendModuleMailTraceLog']) $log_text .= "CCParent UserEmail SendMail To: ".implode(",",(array)$arrOtherInternalMail)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
					}
				}else{
					if(sizeof($arrOtherInternalMail) > $iMail_NumOfRecipientPerTime)
					{
			         	$toSend = $this->split_array($arrOtherInternalMail, $iMail_NumOfRecipientPerTime);
			         	for($i=0; $i<sizeof($toSend); $i++){
			         		$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$toSend[$i],$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
			         		if($sys_custom['SendModuleMailTraceLog']) $log_text .= "UserEmail SendMail To: ".implode(",",(array)$toSend[$i])." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
			         	}
					}
					else
					{
						$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$arrOtherInternalMail,$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
						if($sys_custom['SendModuleMailTraceLog']) $log_text .= "UserEmail SendMail To: ".implode(",",(array)$arrOtherInternalMail)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
					}
				}
			}
			else //by Bcc
			{
				$arrOtherInternalMail = array();
				$arrayCC = array();
				$arrayBCC = $arrSendInternalOption2;
				if(count($arrCcSendInternalOption2)>0){
					// append CC mail addresses to BCC
					foreach($arrCcSendInternalOption2 as $key => $val){
						$arrayBCC[] = $val;
					}
				}

				if(sizeof($arrayBCC) > $iMail_NumOfRecipientPerTime)
				{
		         	$toSend = $this->split_array($arrayBCC, $iMail_NumOfRecipientPerTime);
		         	for($i=0; $i<sizeof($toSend); $i++){
		         		$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$arrOtherInternalMail,$arrayCC,$toSend[$i],$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
		         		if($sys_custom['SendModuleMailTraceLog']) $log_text .= "BCC UserEmail SendMail To: ".implode(",",(array)$arrOtherInternalMail)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$toSend[$i])."\n";
		         	}
				}
				else
				{
					$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$arrOtherInternalMail,$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
					if($sys_custom['SendModuleMailTraceLog']) $log_text .= "BCC UserEmail SendMail To: ".implode(",",(array)$arrOtherInternalMail)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
				}
			}
			//$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$arrOtherInternalMail,$arrayCC,$arrayBCC,'');
		}else{
			$internal_result2[] = true;
		}

		## - for external mail, pass data to function SendMail() to sendout
		if(sizeof($arr_external_mail)>0){
			include_once("libcampusmail.php");
			$lcampusmail = new libcampusmail();

			//$tt = $lcampusmail->externalRecipientFormatting($arr_external_mail,1);

			if(!$sys_custom['SendModuleMailByBcc'])
			{
				if($iMail_NumOfRecipientPerTime == "")
         			$iMail_NumOfRecipientPerTime = 150;

         		if($SendTo == 'CCParent'){
         			if((count($arr_external_mail)+count($arr_cc_external_mail)) > $iMail_NumOfRecipientPerTime){
         				$toSend = $this->split_array($arr_external_mail, $iMail_NumOfRecipientPerTime-count($arr_cc_external_mail));
	         			for($i=0; $i<sizeof($toSend); $i++)
	         			{
         					$tt = $lcampusmail->externalRecipientFormatting($toSend[$i],1);
	         				$arr_to_address = $tt[0];
	         				if($i == 0){
	         					// CC in first batch only
	         					$tt_cc = $lcampusmail->externalRecipientFormatting($arr_cc_external_mail,1);;
	         					$arrayCC = $tt_cc[0];
	         				}else{
								$arrayCC = array();
	         				}
							$arrayBCC = array();
							$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
							if($sys_custom['SendModuleMailTraceLog']) $log_text .= "CCParent Webmail/iMail+ SendMail To: ".implode(",",(array)$arr_to_address)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
	         			}
         			}else{
         				$tt = $lcampusmail->externalRecipientFormatting($arr_external_mail,1);
	         			$arr_to_address = $tt[0];
	         			if(count($arr_cc_external_mail)>0){
	         				$tt_cc = $lcampusmail->externalRecipientFormatting($arr_cc_external_mail,1);
	         				$arrayCC = $tt_cc[0];
	         			}else{
							$arrayCC = array();
	         			}
						$arrayBCC = array();
						$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
						if($sys_custom['SendModuleMailTraceLog']) $log_text .= "CCParent Webmail/iMail+ SendMail To: ".implode(",",(array)$arr_to_address)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
         			}
         		}else if(sizeof($arr_external_mail) > $iMail_NumOfRecipientPerTime)
         		{
         			$toSend = $this->split_array($arr_external_mail, $iMail_NumOfRecipientPerTime);
         			for($i=0; $i<sizeof($toSend); $i++)
         			{
         				$tt = $lcampusmail->externalRecipientFormatting($toSend[$i],1);
         				$arr_to_address = $tt[0];
						$arrayCC = array();
						$arrayBCC = array();
						$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
						if($sys_custom['SendModuleMailTraceLog']) $log_text .= "Webmail/iMail+ SendMail To: ".implode(",",(array)$arr_to_address)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
         			}
         		}
         		else
         		{
         			$tt = $lcampusmail->externalRecipientFormatting($arr_external_mail,1);
         			$arr_to_address = $tt[0];
         			$arrayCC = array();
					$arrayBCC = array();
					$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
					if($sys_custom['SendModuleMailTraceLog']) $log_text .= "Webmail/iMail+ SendMail To: ".implode(",",(array)$arr_to_address)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
         		}
			}
			else/// by BCC
			{
     			$tt = $lcampusmail->externalRecipientFormatting($arr_external_mail,1);
     			if(count($arr_cc_external_mail)>0){
     				$tt_cc = $lcampusmail->externalRecipientFormatting($arr_cc_external_mail,1);
     			}
     			$arr_to_address = array();
				$arrayCC = array();
				$arrayBCC = $tt[0];
				if(count($tt_cc[0])>0){
					$arrayBCC = array_merge($arrayBCC,$tt_cc[0]);
				}
				if(count($arrayBCC) > $iMail_NumOfRecipientPerTime){
					$toSend = $this->split_array($arrayBCC, $iMail_NumOfRecipientPerTime);
         			for($i=0; $i<sizeof($toSend); $i++)
         			{
         				$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$toSend[$i],$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
         				if($sys_custom['SendModuleMailTraceLog']) $log_text .= "BCC Webmail/iMail+ SendMail To: ".implode(",",(array)$arr_to_address)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$toSend[$i])."\n";
         			}
				}else{
					$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$arrayBCC,$AttachmentPath,"","","",null,1,$showSenderAsAdmin);
					if($sys_custom['SendModuleMailTraceLog']) $log_text .= "BCC Webmail/iMail+ SendMail To: ".implode(",",(array)$arr_to_address)." CC: ".implode(",",(array)$arrayCC)." BCC: ".implode(",",(array)$arrayBCC)."\n";
				}
			}
		}else{
			$external_result[] = true;
		}

		if($sys_custom['SendModuleMailTraceLog']) {
			$log = "Page: ".$_SERVER['SCRIPT_FILENAME']."\n";
			$log.= "Time: ".date("Y-m-d H:i:s")."\n";
			$log.= "SenderID: ".$_SESSION['UserID']."\n";
			$log.= "RecipientUserID: ".implode(",",(array)$ToArray)."\n";
			$log.= "Subject: ".$Subject."\n";
			$log.= "Message: \n".$Message."\n";
			$log.= "internal_result: ".implode(",",$internal_result)."\n";
			$log.= "internal_result2: ".implode(",",$internal_result2)."\n";
			$log.= "external_result: ".implode(",",$external_result)."\n";
			$log.= "arr_send_internal: ".implode(",",$arr_send_internal)."\n";
			$log.= $log_text;
			$log.= "\n";
			$this->logModuleMail($log);
		}

		if((in_array(true,$internal_result)) && (in_array(true,$internal_result2)) && (in_array(true,$external_result))) {
			return true;
		}else{
			return false;
		}
	}

	function iMail_insertDB_debug_log($Mode=false,$sql='') {
		global $intranet_root, $UserID;
		include_once("$intranet_root/includes/libfilesystem.php");
		$fs = new libfilesystem();
		if (!is_dir("$intranet_root/file/maillog"))
		{
			mkdir("$intranet_root/file/maillog",0777);
		}
		if (!is_dir("$intranet_root/file/maillog/u".$UserID))
		{
			mkdir("$intranet_root/file/maillog/u$UserID",0777);
		}
		$maillogPath = $intranet_root.'/file/maillog/u'.$UserID;
		// echo $maillogPath;

		if ($Mode && trim($sql) != '') {
			$logFilename = date('Y-m-d').'.log';
			// echo $maillogPath.'/'.$logFilename;
			$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
			$logData = 'Unix Time : '.time()."\t".'SQL : '.$sql."\n";
			$logData .= '================================================================================'."\n";
			fwrite($logFileHandle, $logData);
			fclose($logFileHandle);
		}


	}

	function iMail_mailStructure_log($Mode=false, $Msg_ID="", $MailStructure=array()) {
		global $intranet_root, $UserID;
		include_once("$intranet_root/includes/libfilesystem.php");
		$fs = new libfilesystem();

		if (!is_dir("$intranet_root/file/mailStructurelog"))
		{
			mkdir("$intranet_root/file/mailStructurelog",0777);
		}
		if (!is_dir("$intranet_root/file/mailStructurelog/u".$UserID))
		{
			mkdir("$intranet_root/file/mailStructurelog/u$UserID",0777);
		}

		$mailStrucutureLogPath = $intranet_root.'/file/mailStructurelog/u'.$UserID;
		if ($Mode && count($MailStructure) > 0) {
			// debug_r($MailStructure);
			$logFilename = date('Y-m-d').'.log';
			$logFileHandle = (file_exists($mailStrucutureLogPath.'/'.$logFilename)) ? fopen($mailStrucutureLogPath.'/'.$logFilename,'a') : fopen($mailStrucutureLogPath.'/'.$logFilename,'w');
			$logHeader = 'Message ID : '.$Msg_ID."\t";
			$logHeader .= 'Unix Time : '.time()."\n";

			$logContent = print_r($MailStructure,true);

			$logContent .= "\n";
			$logFooter = '================================================================================'."\n";
			fwrite($logFileHandle, $logHeader);
			fwrite($logFileHandle, $logContent);
			fwrite($logFileHandle, $logFooter);
			fclose($logFileHandle);

			// echo $logContent;
		}
	}

	function iMail_InternalMailSQL_log($Mode=false, $CampusMailFromID="", $sql='') {
		global $intranet_root, $UserID;
		include_once("$intranet_root/includes/libfilesystem.php");
		$fs = new libfilesystem();
		if (!is_dir("$intranet_root/file/InternalMailQueryLog"))
		{
			mkdir("$intranet_root/file/InternalMailQueryLog",0777);
		}
		if (!is_dir("$intranet_root/file/InternalMailQueryLog/u".$UserID))
		{
			mkdir("$intranet_root/file/InternalMailQueryLog/u$UserID",0777);
		}
		$maillogPath = $intranet_root.'/file/InternalMailQueryLog/u'.$UserID;
		// echo $maillogPath;

		if ($Mode && trim($sql) != '') {
			$logFilename = date('Y-m-d').'.log';
			// echo $maillogPath.'/'.$logFilename;
			$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
			$logData = 'Unix Time : '.time()."\t".'CampusMailFromID : '.$CampusMailFromID."\n";
			$logData .= 'SQL : '.$sql."\n";
			$logData .= '================================================================================'."\n";
			fwrite($logFileHandle, $logData);
			fclose($logFileHandle);
		}
	}

	function split_array($a, $count)
	{
	         if ($count == 0) return array($a);

	         $i = 0;
	         while (list ($key, $val) = each ($a))
	         {
	                $temp[] = $val;
	                $i++;
	                if ($i == $count)
	                {
	                    $result[] = $temp;
	                    unset($temp);
	                    $i = 0;
	                }
	         }
	         if (sizeof($temp)!=0) $result[] = $temp;
	         return $result;
	}

	function setDBTotalQuota($UID,$totalQuota)
	{
		$li = new libdb();
		$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USERQUOTA (UserID,Quota)
				VALUES('$UID','$totalQuota') ON DUPLICATE KEY UPDATE Quota = '$totalQuota' ";

		return $li->db_db_query($sql);
	}

	function Get_Archive_Mail_Message_Layer($id,$msg,$width='500px',$height='280px')
	{
		global $Lang, $intranet_root, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;

		$x = "<div id=\"$id\" style=\"padding:20px;width:$width;height:$height;visibility: visible; z-index:9999999999;\" class=\"selectbox_layer selectbox_layer_show_info\">
                <p class=\"spacer\"></p>\n
				$msg
				<p class=\"spacer\"></p>\n
				<div class=\"edit_bottom\" align=\"center\">\n
					<input type=\"button\" class=\"formsmallbutton\" onClick=\"javascript:goProceed();\" name=\"proceedBtn\" id=\"proceedBtn\" value=\"".$Lang['General']['Yes']."\" onMouseOver=\"this.className='formsmallbuttonon'\" onMouseOut=\"this.className='formsmallbutton'\"/>\n
					<input type=\"button\" class=\"formsmallbutton\" onClick=\"javascript:$('div#$id').remove();\" name=\"closeBtn\" id=\"closeBtn\" value=\"".$Lang['General']['Close']."\" onMouseOver=\"this.className='formsmallbuttonon'\" onMouseOut=\"this.className='formsmallbutton'\"/>\n
					<input type=\"button\" class=\"formsmallbutton\" onClick=\"javascript:setRemindLater();\" name=\"remindBtn\" id=\"remindBtn\" value=\"".$Lang['General']['RemindMeLater']."\" onMouseOver=\"this.className='formsmallbuttonon'\" onMouseOut=\"this.className='formsmallbutton'\"/>\n
				</div>\n
			  </div>";
		return $x;
	}

	/* Extended version of sendModuleMail() - adedd CC adn BCC recipients */
	function sendModuleMail2($ToArray,$CcArray,$BccArray,$Subject,$Message,$UseAdminEmailAsSender=1,$AttachmentPath='',$addDoNotReply=false)
	{
		global $intranet_root, $UserID, $SYS_CONFIG, $sys_custom, $plugin,$Lang;
		global $iMail_DummyAddress, $iMail_NumOfRecipientPerTime;

		$use_new_sendModuleMail = $plugin['imail_gamma'];

		include_once("$intranet_root/includes/libdb.php");
		include_once($intranet_root."/includes/libfilesystem.php");
		include_once($intranet_root."/includes/libcampusmail.php");

		$libcampusmail = new libcampusmail();
		$li = new libdb();
		$lfs = new libfilesystem();

		// add Do not reply at the end of the message
		if($addDoNotReply){
			$Message .= '<br>';
			//$Message .= '<i>*'.Get_Lang_Selection("此郵件由系統發出，請勿回覆。","This is an automated email. Please DO NOT respond").'</i>';
			$Message .= '<i>*'.$Lang['EmailNotification']['DoNotReply'].'</i>';
		}

		## generate sender email ##
		if($sys_custom['GetModuleMailSenderFrom'] != "")
		{
			if( strtoupper(trim($sys_custom['GetModuleMailSenderFrom'])) === "ADMIN" )
			{
				$UseAdminEmailAsSender = 1;
		      	$sender_mail = $this->GetWebmasterMailAddress();
		    }
		    else if( strtoupper(trim($sys_custom['GetModuleMailSenderFrom'])) === "USER" )
		    {
		    	$UseAdminEmailAsSender = 0;
		    	if(!$use_new_sendModuleMail) {
		    		if($UserID != ""){
		    			$sender_mail = $this->GetUserEmailAddress($UserID);

						if($sender_mail == "")
			    		{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
		    		} else {
		    			$sender_mail = $this->GetWebmasterMailAddress();
		    		}
		    	} else {
		    		if($UserID != ""){
		    			$sender_mail = $this->GetUserEmailAddress($UserID);

			    		if($sender_mail == "")
			    		{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
		    		} else {
		    			$sender_mail = $this->GetWebmasterMailAddress();
		    		}
		    	}
		    }
		    else
		    {
		    	if($UseAdminEmailAsSender == 1)
				{
					$sender_mail = $this->GetWebmasterMailAddress();
				}
				else
				{
					if(!$use_new_sendModuleMail) {
						if($UserID != ""){
							$sender_mail = $this->GetUserEmailAddress($UserID);
							if($sender_mail == "")
				    		{
								$sender_mail = $this->GetWebmasterMailAddress();
							}
						}else{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
			    	} else {
			    		if($UserID != "") {
			    			$sender_mail = $this->GetUserEmailAddress($UserID);
				    		if($sender_mail == "")
				    		{
								$sender_mail = $this->GetWebmasterMailAddress();
							}
			    		} else {
			    			$sender_mail = $this->GetWebmasterMailAddress();
			    		}
			    	}
				}
		    }
		}
		else
		{
			if($UseAdminEmailAsSender == 1)
			{
		      	$sender_mail = $this->GetWebmasterMailAddress();
			}
			else
			{
				if(!$use_new_sendModuleMail) {
					if($UserID != "") {
						$sender_mail = $this->GetUserEmailAddress($UserID);
						if($sender_mail == "")
			    		{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
					} else {
						$sender_mail = $this->GetWebmasterMailAddress();
					}
		    	} else {
		    		if($UserID != "") {
		    			$sender_mail = $this->GetUserEmailAddress($UserID);
			    		if($sender_mail == "")
			    		{
							$sender_mail = $this->GetWebmasterMailAddress();
						}
		    		} else {
		    			$sender_mail = $this->GetWebmasterMailAddress();
		    		}
		    	}
			}
		}
		## end of generate sender email

		## handle receiver array
		$arr_send_internal = array();
		$arr_external_mail = array();
		if(sizeof($ToArray)>0) {
			$targetUserID = implode(",",$ToArray);

			## check external mail is avaliable
			if(!$sys_custom['SendModuleMailUseAlternativeEmail'] && ($this->isExternalMailAvaliable() || $use_new_sendModuleMail))
			{
				if($use_new_sendModuleMail)
				{
					# filter User whose UserType was not permitted to use gamma
					$gamma_access_right = Get_Gamma_Identity_Access_Rights();
					$user_type_arr = array(1,2,3,4);
					$permittedUserType = array();
					for($k=0; $k<count($gamma_access_right);$k++)
					{
						if($gamma_access_right[$k]==1)
							$permittedUserType[] = $user_type_arr[$k];
					}
					$permittedUserType = implode(",",(array)$permittedUserType);

					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.").", a.IMapUserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail <> '' AND a.UserID IN ($targetUserID) AND a.RecordType IN ($permittedUserType)";
					$arrUserDetails = $li->returnArray($sql,4);
				}
				else
				{
					## check user have imail account or not
					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.UserID IN ($targetUserID)";
					$arrUserDetails = $li->returnArray($sql,3);
				}

				if(sizeof($arrUserDetails)>0) {
					for($i=0; $i<sizeof($arrUserDetails); $i++) {
						list($user_id, $user_login, $user_name, $IMapEmail) = $arrUserDetails[$i];
						$external_mail_address = $user_name." <".(!empty($IMapEmail)?$IMapEmail:$user_login."@".$this->login_domain).">";
						$arr_external_mail[] = $external_mail_address;
						$arr_have_external[] = $user_id;
					}
					$arr_send_internal = array_diff($ToArray, $arr_have_external);
				}else{
					$arr_send_internal = $ToArray;
				}
			}else{
				$arr_send_internal = $ToArray;
			}
		}

		## handle CC array
		$arr_cc_send_internal = array();
		$arr_cc_external_mail = array();
		if(sizeof($CcArray)>0)
		{
			$ccTargetUserID = implode(",",$CcArray);

			## check external mail is avaliable
			if(!$sys_custom['SendModuleMailUseAlternativeEmail'] && ($this->isExternalMailAvaliable() || $use_new_sendModuleMail))
			{
				if($use_new_sendModuleMail)
				{
					# filter User whose UserType was not permitted to use gamma
					$gamma_access_right = Get_Gamma_Identity_Access_Rights();
					$user_type_arr = array(1,2,3,4);
					$permittedUserType = array();
					for($k=0; $k<count($gamma_access_right);$k++)
					{
						if($gamma_access_right[$k]==1)
							$permittedUserType[] = $user_type_arr[$k];
					}
					$permittedUserType = implode(",",(array)$permittedUserType);

					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.").", a.IMapUserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail <> '' AND a.UserID IN ($ccTargetUserID) AND a.RecordType IN ($permittedUserType)";
					$arrCcUserDetails = $li->returnArray($sql,4);
				}
				else
				{
					## check user have imail account or not
					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.UserID IN ($ccTargetUserID)";
					$arrCcUserDetails = $li->returnArray($sql,3);
				}

				if(sizeof($arrCcUserDetails)>0) {
					for($i=0; $i<sizeof($arrCcUserDetails); $i++) {
						list($user_id, $user_login, $user_name, $IMapEmail) = $arrCcUserDetails[$i];
						$external_mail_address = $user_name." <".(!empty($IMapEmail)?$IMapEmail:$user_login."@".$this->login_domain).">";
						$arr_cc_external_mail[] = $external_mail_address;
						$arr_cc_have_external[] = $user_id;
					}
					$arr_cc_send_internal = array_diff($CcArray, $arr_cc_have_external);
				}else{
					$arr_cc_send_internal = $CcArray;
				}
			}else{
				$arr_cc_send_internal = $CcArray;
			}
		}

		## handle BCC array
		$arr_bcc_send_internal = array();
		$arr_bcc_external_mail = array();
		if(sizeof($BccArray)>0)
		{
			$bccTargetUserID = implode(",",$BccArray);

			## check external mail is avaliable
			if(!$sys_custom['SendModuleMailUseAlternativeEmail'] && ($this->isExternalMailAvaliable() || $use_new_sendModuleMail))
			{
				if($use_new_sendModuleMail)
				{
					# filter User whose UserType was not permitted to use gamma
					$gamma_access_right = Get_Gamma_Identity_Access_Rights();
					$user_type_arr = array(1,2,3,4);
					$permittedUserType = array();
					for($k=0; $k<count($gamma_access_right);$k++)
					{
						if($gamma_access_right[$k]==1)
							$permittedUserType[] = $user_type_arr[$k];
					}
					$permittedUserType = implode(",",(array)$permittedUserType);

					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.").", a.IMapUserEmail FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.IMapUserEmail IS NOT NULL AND a.IMapUserEmail <> '' AND a.UserID IN ($bccTargetUserID) AND a.RecordType IN ($permittedUserType)";
					$arrBccUserDetails = $li->returnArray($sql,4);
				}
				else
				{
					## check user have imail account or not
					$sql = "SELECT a.UserID, a.UserLogin, ".getNameFieldByLang("a.")." FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE b.ACL IN (1,3) AND a.UserID IN ($bccTargetUserID)";
					$arrBccUserDetails = $li->returnArray($sql,3);
				}

				if(sizeof($arrBccUserDetails)>0) {
					for($i=0; $i<sizeof($arrBccUserDetails); $i++) {
						list($user_id, $user_login, $user_name, $IMapEmail) = $arrBccUserDetails[$i];
						$external_mail_address = $user_name." <".(!empty($IMapEmail)?$IMapEmail:$user_login."@".$this->login_domain).">";
						$arr_bcc_external_mail[] = $external_mail_address;
						$arr_bcc_have_external[] = $user_id;
					}
					$arr_bcc_send_internal = array_diff($BccArray, $arr_bcc_have_external);
				}else{
					$arr_bcc_send_internal = $BccArray;
				}
			}else{
				$arr_bcc_send_internal = $BccArray;
			}
		}

		## TO: Get other email address for those internal recipient
		$arrSendInternalOption2 = array();
		if(sizeof($arr_send_internal)>0){
			$targetInternalUserID = implode(",",$arr_send_internal);
			$sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserID IN ($targetInternalUserID) AND UserEmail != ''";
			$arrSendInternalOption2 = $li->returnVector($sql);
		}

		## CC: Get other email address for those internal recipient
		$arrCcSendInternalOption2 = array();
		if(sizeof($arr_cc_send_internal)>0){
			$ccTargetInternalUserID = implode(",",$arr_cc_send_internal);
			$sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserID IN ($ccTargetInternalUserID) AND UserEmail != ''";
			$arrCcSendInternalOption2 = $li->returnVector($sql);
		}

		## BCC: Get other email address for those internal recipient
		$arrBccSendInternalOption2 = array();
		if(sizeof($arr_bcc_send_internal)>0){
			$bccTargetInternalUserID = implode(",",$arr_bcc_send_internal);
			$sql = "SELECT UserEmail FROM INTRANET_USER WHERE UserID IN ($bccTargetInternalUserID) AND UserEmail != ''";
			$arrBccSendInternalOption2 = $li->returnVector($sql);
		}

		if(!$sys_custom['SendModuleMailUseAlternativeEmail'] && !$use_new_sendModuleMail)
		{
			$tmp_arr_to_send_internal = array();
			$tmp_arr_cc_send_internal = array();
			$tmp_arr_bcc_send_internal = array();
			for($j=0;$j<sizeof($arr_send_internal);$j++){
				$tmp_arr_to_send_internal[] = "U".$arr_send_internal[$j];
			}
			$internal_recipient_id = implode(",",$tmp_arr_to_send_internal);
			for($j=0;$j<sizeof($arr_cc_send_internal);$j++){
				$tmp_arr_cc_send_internal[] = "U".$arr_cc_send_internal[$j];
			}
			$internal_cc_recipient_id = implode(",",$tmp_arr_cc_send_internal);
			for($j=0;$j<sizeof($arr_bcc_send_internal);$j++){
				$tmp_arr_bcc_send_internal[] = "U".$arr_bcc_send_internal[$j];
			}
			$internal_bcc_recipient_id = implode(",",$tmp_arr_bcc_send_internal);
			if(!$sys_custom['SendModuleMailByBcc'])
			{
				$final_internal_recipient_id = $internal_recipient_id;
				$final_internal_cc_recipient_id = $internal_cc_recipient_id;
				//$final_internal_bcc_recipient_id = $internal_bcc_recipient_id;
				$final_internal_bcc_recipient_id = '';
			}
			else
			{
				$final_internal_recipient_id = "";
				$final_internal_cc_recipient_id = "";
				$tmp_arr_bcc_send_internal = array_merge($tmp_arr_bcc_send_internal,$tmp_arr_to_send_internal,$tmp_arr_cc_send_internal);
				//$final_internal_bcc_recipient_id = implode(",",$tmp_arr_bcc_send_internal);
				$final_internal_bcc_recipient_id = '';
			}
			## - for intranal mail, directly insert the mail into INTRANET_CAMPUSMAIL
			if(sizeof($arr_send_internal)>0) {
				## To recipient
				foreach($arr_send_internal as $key=>$user_id) {
					$sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE UserID = '$user_id'";
					$isUserIDExist = $li->returnVector($sql);
					if($isUserIDExist[0] > 0)
					{
						$internal_result[] = $this->insertCampusMail($user_id, $UserID, $final_internal_recipient_id, $final_internal_cc_recipient_id, $final_internal_bcc_recipient_id, '', '', '', $Subject, $Message, 'utf-8', '', '', 1, 2, $AttachmentPath);
					}else{
						$internal_result[] = false;
					}
				}
			}else{
				$internal_result[] = true;
			}

			if(sizeof($arr_cc_send_internal)>0){
				## CC recipient
				foreach($arr_cc_send_internal as $key=>$user_id){
					$sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE UserID = '$user_id'";
					$isUserIDExist = $li->returnVector($sql);
					if($isUserIDExist[0] > 0)
					{
						$internal_result[] = $this->insertCampusMail($user_id, $UserID, $final_internal_recipient_id, $final_internal_cc_recipient_id, $final_internal_bcc_recipient_id, '', '', '', $Subject, $Message, 'utf-8', '', '', 1, 2, $AttachmentPath);
					}else{
						$internal_result[] = false;
					}
				}
			}else{
				$internal_result[] = true;
			}

			if(sizeof($arr_bcc_send_internal)>0){
				## BCC recipient
				foreach($arr_bcc_send_internal as $key=>$user_id){
					$sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE UserID = '$user_id'";
					$isUserIDExist = $li->returnVector($sql);
					if($isUserIDExist[0] > 0)
					{
						$internal_result[] = $this->insertCampusMail($user_id, $UserID, $final_internal_recipient_id, $final_internal_cc_recipient_id, $final_internal_bcc_recipient_id, '', '', '', $Subject, $Message, 'utf-8', '', '', 1, 2, $AttachmentPath);
					}else{
						$internal_result[] = false;
					}
				}
			}else{
				$internal_result[] = true;
			}
		}
		else
		{
			$internal_result[] = true;
		}


		## - Send to other email address for those internal recipient
		if(sizeof($arrSendInternalOption2)>0){
			if($iMail_NumOfRecipientPerTime == "")
         		$iMail_NumOfRecipientPerTime = 150;

			if(!$sys_custom['SendModuleMailByBcc'])
			{
				$arrOtherInternalMail = $arrSendInternalOption2;
				$arrayCC = $arrCcSendInternalOption2;
				$arrayBCC = $arrBccSendInternalOption2;

				if(sizeof($arrOtherInternalMail) > $iMail_NumOfRecipientPerTime)
				{
		         	$toSend = $this->split_array($arrOtherInternalMail, $iMail_NumOfRecipientPerTime);
		         	for($i=0; $i<sizeof($toSend); $i++){
		         		$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$toSend[$i],$arrayCC,$arrayBCC,$AttachmentPath);
		         	}
				}
				else
				{
					$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$arrOtherInternalMail,$arrayCC,$arrayBCC,$AttachmentPath);
				}
			}
			else //by Bcc
			{
				$arrOtherInternalMail = array();
				$arrayCC = array();
				$arrayBCC = array_merge($arrSendInternalOption2,$arrCcSendInternalOption2,$arrBccSendInternalOption2);

				if(sizeof($arrayBCC) > $iMail_NumOfRecipientPerTime)
				{
		         	$toSend = $this->split_array($arrayBCC, $iMail_NumOfRecipientPerTime);
		         	for($i=0; $i<sizeof($toSend); $i++){
		         		$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$arrOtherInternalMail,$arrayCC,$toSend[$i],$AttachmentPath);
		         	}
				}
				else
				{
					$internal_result2[] = $this->SendMail($Subject,$Message,$sender_mail,$arrOtherInternalMail,$arrayCC,$arrayBCC,$AttachmentPath);
				}
			}
		}else{
			$internal_result2[] = true;
		}

		## - for external mail, pass data to function SendMail() to sendout
		if(sizeof($arr_external_mail)>0){
			include_once("libcampusmail.php");
			$lcampusmail = new libcampusmail();

			if(!$sys_custom['SendModuleMailByBcc'])
			{
				if($iMail_NumOfRecipientPerTime == "")
         			$iMail_NumOfRecipientPerTime = 150;

         		if(sizeof($arr_external_mail) > $iMail_NumOfRecipientPerTime)
         		{
         			$toSend = $this->split_array($arr_external_mail, $iMail_NumOfRecipientPerTime);
         			for($i=0; $i<sizeof($toSend); $i++)
         			{
         				$tt = $lcampusmail->externalRecipientFormatting($toSend[$i],1);
         				$tt_cc = $lcampusmail->externalRecipientFormatting($arr_cc_external_mail,1);
         				$tt_bcc = $lcampusmail->externalRecipientFormatting($arr_bcc_external_mail,1);
         				$arr_to_address = $tt[0];
						$arrayCC = $tt_cc[0];
						$arrayBCC = $tt_bcc[0];
						$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$arrayBCC,$AttachmentPath);
         			}
         		}
         		else
         		{
         			$tt = $lcampusmail->externalRecipientFormatting($arr_external_mail,1);
         			$tt_cc = $lcampusmail->externalRecipientFormatting($arr_cc_external_mail,1);
         			$tt_bcc = $lcampusmail->externalRecipientFormatting($arr_bcc_external_mail,1);
         			$arr_to_address = $tt[0];
					$arrayCC = $tt_cc[0];
					$arrayBCC = $tt_bcc[0];
					$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$arrayBCC,$AttachmentPath);
         		}
			}
			else/// by BCC
			{
				if($iMail_NumOfRecipientPerTime == "")
         			$iMail_NumOfRecipientPerTime = 150;

         		if(sizeof($arr_external_mail) > $iMail_NumOfRecipientPerTime)
         		{
         			$toSend = $this->split_array($arr_external_mail, $iMail_NumOfRecipientPerTime);
         			for($i=0; $i<sizeof($toSend); $i++)
         			{
         				$tt = $lcampusmail->externalRecipientFormatting($toSend[$i],1);
         				$tt_cc = $lcampusmail->externalRecipientFormatting($arr_cc_external_mail,1);
         				$tt_bcc = $lcampusmail->externalRecipientFormatting($arr_bcc_external_mail,1);
         				$arr_to_address = array();
						$arrayCC = array();
						$arrayBCC = array_merge($tt[0],$tt_cc[0],$tt_bcc[0]);
						$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$arrayBCC,$AttachmentPath);
         			}
         		}
				else
				{
         			$tt = $lcampusmail->externalRecipientFormatting($arr_external_mail,1);
         			$tt_cc = $lcampusmail->externalRecipientFormatting($arr_cc_external_mail,1);
         			$tt_bcc = $lcampusmail->externalRecipientFormatting($arr_bcc_external_mail,1);
         			$arr_to_address = array();
					$arrayCC = array();
					$arrayBCC = array_merge($tt[0],$tt_cc[0],$tt_bcc[0]);
					$external_result[] = $this->SendMail($Subject,$Message,$sender_mail,$arr_to_address,$arrayCC,$arrayBCC,$AttachmentPath);
         		}
			}
		}

		if($sys_custom['SendModuleMailTraceLog']) {
			$log = "Page: ".$_SERVER['SCRIPT_FILENAME']."\n";
			$log.= "Time: ".date("Y-m-d H:i:s")."\n";
			$log.= "SenderID: ".$_SESSION['UserID']."\n";
			$log.= "RecipientUserID: ".implode(",",(array)$ToArray)."\n";
			$log.= "Subject: ".$Subject."\n";
			$log.= "Message: \n".$Message."\n";
			$log.= "\n";
			$this->logModuleMail($log);
		}

		if((in_array(true,$internal_result)) && (in_array(true,$internal_result2)) && (in_array(true,$internal_result))) {
			return true;
		}else{
			return false;
		}
	}

	function insertCampusMail($ParUserID, $ParSenderID, $ParRecipientID, $ParInternalCC, $ParInternalBCC, $ParExternalTo, $ParExternalCC, $ParExternalBCC, $ParSubject, $ParMessage, $ParMessageEncoding='utf-8', $ParIsImportant='', $ParIsNotification='', $ParMailType=1, $ParUserFolderID=2, $ParAttachmentPath='')
	{
		global $intranet_root, $UserID, $SYS_CONFIG, $sys_custom, $plugin;
		global $iMail_DummyAddress, $iMail_NumOfRecipientPerTime;
		global $li, $lfs, $libcampusmail;
		if(!($li instanceof libdb)){
			include_once($intranet_root."/includes/libdb.php");
			$li = new libdb();
		}
		if(!($lfs instanceof libfilesystem)){
			include_once($intranet_root."/includes/libfilesystem.php");
			$lfs = new libfilesystem();
		}
		if(!($libcampusmail instanceof libcampusmail)){
			include_once($intranet_root."/includes/libcampusmail.php");
			$libcampusmail = new libcampusmail();
		}

		## Handle attachment files if any
		$Attachment = "";
		$IsAttachment = "";
		$AttachmentSize = "";
		$folder_copy_success = false;
		if(trim($ParAttachmentPath)!=''){
			$tmp_total_size = 0; // KB
			if(!isset($AttachmentList)){
				$tmp_attachment_list = $lfs->return_folderlist($ParAttachmentPath);
				$AttachmentList = array();
				for($i=0;$i<sizeof($tmp_attachment_list);$i++){
					$tmp_path = $tmp_attachment_list[$i];
					if(file_exists($tmp_path)){
						$tmp_file_size = ceil(filesize($tmp_path)/1024); // byte to kb
						$tmp_total_size += $tmp_file_size;
						$AttachmentList[] = array($tmp_path, $tmp_file_size);
					}
				}
			}
			if(sizeof($AttachmentList)>0)
			{
				$composeFolder = "u$ParUserID/".session_id().".".time();
				$composePath = $intranet_root."/file/mail";
				if(!is_dir($composePath)){
					$lfs->folder_new($composePath);
				}
				$composePath = $composePath."/u".$ParUserID;
				if(!is_dir($composePath)){
					$lfs->folder_new($composePath);
				}
				$composePath = $composePath."/".session_id().".".time();
				if(!is_dir($composePath)){
					$lfs->folder_new($composePath);
				}

				if(is_dir($composePath)){
					$folder_copy_success = $lfs->folder_content_copy($ParAttachmentPath,$composePath);
				}
				if($folder_copy_success){
					$Attachment = $composeFolder;
					$IsAttachment = "1";
					$AttachmentSize = $tmp_total_size;
				}
			}
		}
		## End of handle attachment files

		$sql = "INSERT INTO INTRANET_CAMPUSMAIL (
			          UserID, SenderID, RecipientID, InternalCC, InternalBCC,
			          ExternalTo, ExternalCC, ExternalBCC,
			          Subject, Message, MessageEncoding, Attachment,
			          IsAttachment, IsImportant, IsNotification,
			          MailType, UserFolderID,
			          RecordType, DateInput, DateModified, DateInFolder, AttachmentSize, isHTML
			     )
			     VALUES
			     (
			          '$ParUserID', '$ParSenderID', '$ParRecipientID','$ParInternalCC','$ParInternalBCC',
			          '$ParExternalTo','$ParExternalCC','$ParExternalBCC',
			          '".$li->Get_Safe_Sql_Query($ParSubject)."', '".$li->Get_Safe_Sql_Query($ParMessage)."','$ParMessageEncoding','".$Attachment."',
			          '".$IsAttachment."', '$ParIsImportant', '$ParIsNotification',
			          '$ParMailType','$ParUserFolderID',
			          '2', now(), now(), now(), '".$AttachmentSize."','1'
			     )";

		$insert_campusmail_success = $li->db_db_query($sql);
		$internal_result[] = $insert_campusmail_success;

		## handle campusmail attachment part db mappings and quota
		if($insert_campusmail_success && trim($ParAttachmentPath)!='' && $Attachment!='' && sizeof($AttachmentList)>0)
		{
			$campus_mail_id = $li->db_insert_id();
			$delim = "";
			$campus_mail_part_values = "";
			for($i=0;$i<sizeof($AttachmentList);$i++){
				$tmp_file_name = $li->Get_Safe_Sql_Query(basename($AttachmentList[$i][0]));
				$tmp_file_size = $AttachmentList[$i][1];
				$campus_mail_part_values .= $delim."('$campus_mail_id','$Attachment','$tmp_file_name','$tmp_file_size')";
				$delim = ",";
			}
			$sql = "INSERT IGNORE INTO INTRANET_IMAIL_ATTACHMENT_PART (CampusMailID, AttachmentPath, FileName, FileSize) VALUES $campus_mail_part_values";
			$insert_part_success = $li->db_db_query($sql);
			if($insert_part_success){
				$sql = "UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET QuotaUsed = QuotaUsed + $tmp_total_size WHERE UserID = $ParUserID";
				$li->db_db_query($sql);
			}
		}
		## end of handle campusmail attachment part db mappings

		$libcampusmail->UpdateCampusMailNewMailCount($ParUserID, 2, 1);

		return $insert_campusmail_success;
	}

	function MIME_Decode($str)
	{
		$elements = imap_mime_header_decode($str);
		for($k=0 ; $k<count($elements) ; $k++) {
			if(trim($elements[$k]->text) != '')
			{
				if(strtolower($elements[$k]->charset)=="utf-8"){
					$tmp .= $elements[$k]->text;
				}else if(strtolower($elements[$k]->charset) == "default"){
					$tmp .= $elements[$k]->text;
				}else if(in_array(strtolower($elements[$k]->charset), array("utf-8","default")) || stristr($elements[$k]->charset,"unknown")){
					$charset = mb_detect_encoding($elements[$k]->text, 'UTF-8, BIG5, GB2312, GBK, ASCII');
					if($charset != "")
						$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8",$charset);
					else
						$tmp .= $elements[$k]->text;
				}else if(strtolower($elements[$k]->charset)=="gb2312" || strtolower($elements[$k]->charset)=="gb18030"){
					$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8","GB2312,GBK");
				}else{
					$tmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8",$elements[$k]->charset);
				}
			}
		}

		return $tmp;
	}

	function SaveRawBody($CampusMailID, $Content) {
		global $intranet_root, $UserID;
		include_once("$intranet_root/includes/libfilesystem.php");
		$fs = new libfilesystem();
	    if (!is_dir("$intranet_root/file/mail/u$UserID"))
		{
			mkdir("$intranet_root/file/mail/u$UserID",0777);
		}

		$user_folder = $this->getNextFreeUserFolder($UserID, false);
		//$path = "$intranet_root/file/mail/u$UserID";
		$path = "$intranet_root/file/mail/".$user_folder;
		if ($CampusMailID != '') {
			$subfilepath = 'body_'.$CampusMailID.'.txt';
			$filename = $path.'/'.$subfilepath;
			$SaveState = $fs->file_write($Content,$filename);
		}
		return $SaveState;
	}

	function RemoveRawBodyLogFile($CampusMailID)
	{
		global $intranet_root, $UserID;

		$path = "$intranet_root/file/mail/u$UserID";
		if ($CampusMailID != '') {
			$subfilepath = 'body_'.$CampusMailID.'.txt';
			$filepath = $path.'/'.$subfilepath;
			if(file_exists($filepath)){
				unlink($filepath);
			}
		}
	}

	// format a datetime string to follow RFC822 standard and more php friendly
	function formatDatetimeString($datetime)
	{
		$return_datetime = str_replace("UT","UTC",$datetime); // UT would make strtotime() fail
		// remove ( and ) which would make strtotime() fail
		$matches = array();
		if(preg_match('/^.+(\([^\(\)]+\)).*$/',$return_datetime,$matches)){
			$numOfMatch = count($matches);
			if($numOfMatch>1) {
				for($i=0;$i<$numOfMatch;$i++) {
					$match = $matches[$i];
					$replace = str_replace(array('(',')'),'',$match);
					$return_datetime = str_replace($match,$replace,$return_datetime);
				}
			}
		}
		return $return_datetime;
	}

	function logModuleMail($logContent)
	{
		global $intranet_root;

	    if (!is_dir("$intranet_root/file/moduleMailLog"))
		{
			mkdir("$intranet_root/file/moduleMailLog",0777);
		}
		$date = date("Y-m-d");
		$path = "$intranet_root/file/moduleMailLog/".$date.".log";
		$logFileHandle = (file_exists($path)) ? fopen($path,'a') : fopen($path,'w');
		$bytesWritten = fwrite($logFileHandle, $logContent);
		fclose($logFileHandle);

		return $bytesWritten;
	}

	/*
	 * This function would get the user folder that has not exceed ext3 maximum number of files and dirs limit (i.e. 32000, this function use limit 30000).
	 * If uXXXX folder has reach limit, check and create a subfolder uXXXX/1, if uXXXX/1 also reach limit, use uXXXX/2, etc.
	 */
	function getNextFreeUserFolder($user_id, $return_full_path=false)
	{
		global $file_path;

		$LIMIT = 30000;
		//$LIMIT = 10; // test value
		$absolute_target_folder = $file_path."/file/mail/u".$user_id;
		$target_folder = "u".$user_id;

		$folder_to_find = $absolute_target_folder;
		$rel_folder_to_find = $target_folder;
		$subfolder_number = 0;
		$file_count = 0;
		do
		{
			$cmd = "find ".OsCommandSafe($folder_to_find)." -maxdepth 1 | wc -l";
			//debug_pr($cmd." : ".$file_count);
			$file_count = intval(shell_exec($cmd));
			if($file_count >= $LIMIT)
			{
				$subfolder_number += 1;
				$folder_to_find = $absolute_target_folder."/".$subfolder_number;
				$rel_folder_to_find = $target_folder."/".$subfolder_number;
				if(!is_dir($folder_to_find)){
					mkdir($folder_to_find,0777);
				}
			}else{
				break;
			}
		}while($file_count >= $LIMIT);

		if($return_full_path){
			return $folder_to_find;
		}else{
			return $rel_folder_to_find;
		}
	}

	function getPartedAttachmentFileName($part)
	{
		$return_filename = uniqid().'.'.strtolower($part->subtype);
		if(isset($part->dparameters) && sizeof($part->dparameters)>0)
		{
			$objs = $part->dparameters;
			$obj_size = count($objs);
			$whole_filename = '';
			for($i=0;$i<$obj_size;$i++)
			{
				if(preg_match('/^filename\\*\d+\\*$/i',$objs[$i]->attribute)){
					$whole_filename .= $objs[$i]->value;
				}else if($objs[$i]->attribute == 'filename'){
					$whole_filename .= $objs[$i]->value;
				}
			}
			if(preg_match('/^(.+)\'(.*)\'(.+)$/',$whole_filename, $matches)){
				$return_filename = rawurldecode($matches[3]);
			}else{
				$return_filename = rawurldecode($whole_filename);
			}
			$return_filename = $this->MIME_Decode($return_filename);
		}
	/*	else if(isset($part->parameters) && sizeof($part->parameters)>0)
		{
			$objs = $part->parameters;
			$obj_size = count($objs);
			$whole_filename = '';
			for($i=0;$i<$obj_size;$i++)
			{
				if(preg_match('/^name\\*\d+\\*$/',$objs[$i]->attribute)){
					$whole_filename .= $objs[$i]->value;
				}else if($objs[$i]->attribute == 'name'){
					$whole_filename .= $objs[$i]->value;
				}
			}
			if(preg_match('/^(.+)\'(.*)\'(.+)$/',$whole_filename, $matches)){
				$return_filename = rawurldecode($matches[3]);
			}else{
				$return_filename = rawurldecode($whole_filename);
			}
			$return_filename = $this->MIME_Decode($return_filename);
		}
		*/
		return $return_filename;
	}
}
}        // End of directive
?>