<?php
// editing by Roy
 
/********************
 * change log:
 * 2016-03-29 Roy: [ip.2.5.7.4.1]: [P94232] fix cannot send back message status bug by changing parameter's name
 * 2015-09-23 Roy: [ip.2.5.6.10.1]: consolidate message ary cannot get fromModule & moduleRecordID
 * 2015-07-03 Roy: [ip.2.5.6.7.1.0]: add parameter fromModule and moduleRecordID for link up push message to other module
 * 2015-04-01 Roy: [ip.2.5.6.5.1.0]: create file, for GeTui push message service
 ********************/

include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
if (!defined("LIBGETUI_DEFINED")) {
	define("LIGETUI_DEFINED", true);
	
	class libgetui extends libdb {
		function libgetui() {
			$this->libdb();
		}
//		
//		function sendRequestToCentralServer($schoolCode, $messageInfoAry) {
//			global $intranet_root, $config_school_code, $plugin, $eclassAppConfig;
//			
//			include_once($intranet_root.'/includes/json.php');
//			//include_once($intranet_root.'/includes/eClassApp/libAES.php');
//			
//			$jsonObj = new JSON_obj();
//			//$libaes = new libAES($eclassAppConfig['aesKey']);
//			$successAry = array();
//			
//			$postParamAry = array();
//			$postParamAry['RequestMethod'] = 'sendPushMessageByGcm';
////			$postParamAry['SchoolCode'] = $schoolCode;
//			$postParamAry['ApiVersion'] = $eclassAppConfig['pushMessage']['apiVersion']['gcm'];
//			$postParamAry['MessageInfoAry'] = $messageInfoAry;
//			$postJsonString = $jsonObj->encode($postParamAry);
//			
////			$postEncryptedJsonAry = array();
////			$postEncryptedJsonAry['eClassRequestEncrypted'] = $libaes->encrypt($postJsonString);
////			$postEncryptedJsonString = $jsonObj->encode($postEncryptedJsonAry);
//			
//			$headers = array('Content-Type: application/json');
//			
//			session_write_close();
//			$ch = curl_init();
//			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
//			curl_setopt($ch, CURLOPT_URL, $eclassAppConfig['pushMessage']['apiPath']);
//			curl_setopt($ch, CURLOPT_HEADER, false);
//			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//			curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
//			//curl_setopt($ch, CURLOPT_POSTFIELDS, $postEncryptedJsonString);
//			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
//			$responseJson = curl_exec($ch);
//			curl_close($ch);
//			
//			$responseJson = standardizeFormPostValue($responseJson);
//			$responseJsonAry = $jsonObj->decode($responseJson);
//						
//			return $responseJsonAry;
//		}
//		
		function getMaxDevicePerMessage() {
			global $eclassAppConfig;
			return $eclassAppConfig['pushMessage']['maxDevicePerMessage']['getui'];
		}
		
		function getMaxMessagePayloadByte() {
			global $eclassAppConfig;
			return $eclassAppConfig['pushMessage']['maxMessagePayloadByte']['getui'];
		}
		
		function buildMessageJson($notifyMessageId, $messageAry) {
			global $intranet_root, $config_school_code, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();

			$maxNumOfDevicePerMessage = $this->getMaxDevicePerMessage();
			$maxMessagePayloadByte = $this->getMaxMessagePayloadByte();
			
			
			foreach ((array)$messageAry as $_appType => $_messageAry) {
				
				### consolidate message info
				$consolidatedMessageAry = array();
				$deviceCount = 0; 
				$messageCount = -1;
				$numOfMessage = count($_messageAry);
				for ($i=0; $i<$numOfMessage; $i++) {
					$_messageTitle = $_messageAry[$i]['messageTitle'];
					$_messageContent = $_messageAry[$i]['messageContent'];
					$_userAssoAry = $_messageAry[$i]['userAssoAry'];
					$_fromModule = $_messageAry[$i]['fromModule'];
					$_moduleRecordID = $_messageAry[$i]['moduleRecordID'];
					
					$_newMessage = true;
					foreach ((array)$_userAssoAry as $__recipientUserId => $__userInfoAry) {
						$__relatedUserIdAry = $__userInfoAry['relatedUserIdAry'];
						$__deviceIdAry = $__userInfoAry['deviceIdAry'];
						$__numOfDeviceId = count((array)$__deviceIdAry);
						
						for ($j=0; $j<$__numOfDeviceId; $j++) {
							$___deviceId = $__deviceIdAry[$j];
							
							$___openNewMessage = false;
							if ($_newMessage || $deviceCount == $maxNumOfDevicePerMessage) {
								$___openNewMessage = true;
							}
							else {
								// simulate "data" json to see if excess 4096 bytes
								// http://developer.android.com/google/gcm/adv.html
								// => It has a payload limit of 4096 bytes.
								
								//$___tmpRelatedUserIdAssoAry = array_merge((array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'], (array)$__relatedUserIdAry);
								$___tmpRelatedUserIdAssoAry = (array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] + (array)$__relatedUserIdAry;
								$___tmpMessagePayloadAry = $this->buildMessagePayloadAry($notifyMessageId, $_messageTitle, $_messageContent, $___tmpRelatedUserIdAssoAry, $_fromModule, $_moduleRecordID);
								$___tmpMessagePayloadJsonString = $jsonObj->encode($___tmpMessagePayloadAry);
								
								if (strlen($___tmpMessagePayloadJsonString) > $maxMessagePayloadByte) {
									$___openNewMessage = true;
								}
							}
							
							if ($___openNewMessage) {
								$messageCount++;
								$deviceCount = 0;
								$_newMessage = false;
								
								$consolidatedMessageAry[$messageCount]['messageTitle'] = $_messageTitle;
								$consolidatedMessageAry[$messageCount]['messageContent'] = $_messageContent;
							}
							
							//$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] = array_merge((array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'], (array)$__relatedUserIdAry);
							$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] = (array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] + (array)$__relatedUserIdAry;
							$consolidatedMessageAry[$messageCount]['deviceIdAry'][] = $___deviceId;
							
							// 20150923 Roy - [B85981] set fromModule & moduleRecordID to consolidatedMessageAry
							$consolidatedMessageAry[$messageCount]['fromModule'] = $_fromModule;
							$consolidatedMessageAry[$messageCount]['moduleRecordID'] = $_moduleRecordID;
							$deviceCount++;
						}
					}
				}
				
				$numOfConsolidatedMessage = count($consolidatedMessageAry);
				for ($i=0; $i<$numOfConsolidatedMessage; $i++) {
					$_messageTitle = $consolidatedMessageAry[$i]['messageTitle'];
					$_messageContent = $consolidatedMessageAry[$i]['messageContent'];
					$_relatedUserIdAssoAry = $consolidatedMessageAry[$i]['relatedUserIdAssoAry'];
					$_deviceIdAry = $consolidatedMessageAry[$i]['deviceIdAry'];
					$_fromModule = $consolidatedMessageAry[$i]['fromModule'];
					$_moduleRecordID = $consolidatedMessageAry[$i]['moduleRecordID'];
					
					$_messageDataAry = $this->buildMessagePayloadAry($notifyMessageId, $_messageTitle, $_messageContent, $_relatedUserIdAssoAry, $_fromModule, $_moduleRecordID);
					
					// 'dry_run' => true, => will not send out, just for simulation
	//				$_pushMessageDataAry = array(
	//					'dry_run' => true,
	//			        'data' => $_messageDataAry,
	//			        'registration_ids' => $_deviceIdAry
	//			    );
				    $_pushMessageDataAry = array(
				    	'data' => $jsonObj->encode($_messageDataAry),
				        'registration_ids' => $_deviceIdAry
				    );
					
					$messageJsonAry[$_appType][$i] = $_pushMessageDataAry;
					$messageCount++;
				}
				
			}
			
			
			

//			debug_pr($messageJsonAry);
			return $messageJsonAry;
		}
		
		function buildMessagePayloadAry($notifyMessageId, $messageTitle, $messageContent, $userMappingAry, $fromModule='', $moduleRecordID='') {
			global $config_school_code, $intranet_root, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();
			
			$extraJsonAry = array();
			$extraJsonAry['IntranetReferenceID'] = $notifyMessageId;
			$extraJsonAry['SchoolCode'] = $config_school_code;
			$extraJsonAry['UserMapping'] = $userMappingAry;
			$extraJsonAry['FromModule'] = $fromModule;
			$extraJsonAry['ModuleRecordID'] = $moduleRecordID;
			$extraJsonString = $jsonObj->encode($extraJsonAry);
			
			$payloadAry = array();
			$payloadAry['MessageTitle'] = $messageTitle;
			$payloadAry['MessageContent'] = $messageContent;
			$payloadAry['ApiVersion'] = $eclassAppConfig['pushMessage']['apiVersion']['getui'];
			$payloadAry['ExtraJson'] = $extraJsonAry;
			
			return $payloadAry;
		}
		
		function saveMessageResult($notifyMessageId, $resultAry, $parNotifyMessageTargetIdAry) {
			global $eclassAppConfig, $intranet_root;
			
			include_once($intranet_root.'/includes/eClassApp/libeClassApp.php');
			$leClassApp = new libeClassApp();
			
			if ($parNotifyMessageTargetIdAry != '') {
				$conds_notifyMessageTargetId = " AND NotifyMessageTargetID IN ('".implode("','", (array)$parNotifyMessageTargetIdAry)."') ";
			}
			
			$successAry = array();
			if ($resultAry['ErrorCode']) {
				$sql = "Update 
								INTRANET_APP_NOTIFY_MESSAGE_REFERENCE 
						Set 
								ErrorCode = '".$this->Get_Safe_Sql_Query($resultAry['ErrorCode'])."', 
								MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']."',
								DateModified = now()
						Where
								NotifyMessageID = '".$notifyMessageId."'
								And ServiceProvider = 'getui'
								$conds_notifyMessageTargetId
						";
				$successAry['updateErrorCode'] = $this->db_db_query($sql);
			}
			else {
				$messageResultAry = $resultAry['result'];
				
				foreach ((array)$messageResultAry as $_appType => $_resultAry) {
					$numOfMessage = count($_resultAry);
					for ($i=0; $i<$numOfMessage; $i++) {
						$_messageResult = $_resultAry[$i]['result'];
						$_messageContentId = $_resultAry[$i]['contentId'];
						$_messageResultDetailsAry = $_resultAry[$i]['details'];
						
						if ($_messageResult == 'ok') {
							foreach ((array)$_messageResultDetailsAry as $__deviceId => $__deviceStatus) {
								if ($__deviceStatus == 'successed_online' || $__deviceStatus == 'successed_offline') {
									$messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'];
									$errorCode = "";
								} else {
									$messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
									$errorCode = $__deviceStatus;
								}
								
								$sql = 
								"Select RecordID
								From APP_USER_DEVICE As aud
								Inner Join INTRANET_APP_NOTIFY_MESSAGE_TARGET As ianmt On aud.UserID = ianmt.TargetID
								Where 1
								And aud.DeviceID = '$__deviceId'
								And aud.RecordStatus = 1
								And ianmt.NotifyMessageID = $notifyMessageId";
								$tempRecordIDAry = $this->returnResultSet($sql);
								$deviceRecordIDAry = array();
								// P94232: 20160329 Roy, fix cannot send back message status bug by changing parameter's name
								foreach ((array)$tempRecordIDAry as $_tempRecordIDAry) {
									$deviceRecordIDAry[] = $_tempRecordIDAry['RecordID'];
								}

								$_sql = 
								"Update 
								INTRANET_APP_NOTIFY_MESSAGE_REFERENCE 
								Set 
								MulticastID = '$_messageContentId',
								MessageStatus = $messageStatus,
								ErrorCode = '$errorCode',
								DateModified = now()
								Where
								NotifyMessageID = $notifyMessageId
								And ServiceProvider = 'getui'
								And DeviceRecordID in (".implode(", ", (array)$deviceRecordIDAry).")
								And MessageStatus != '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']."'
								";
								
								$successAry['updateMessageStatus'][] = $this->db_db_query($_sql);
							}
						} else {
							// fail to send message
							$_sql = 
							"Update 
							INTRANET_APP_NOTIFY_MESSAGE_REFERENCE
							Set 
							ErrorCode = '".$_messageResult."',
							DateModified = now()
							Where 1
							And NotifyMessageID = '".$notifyMessageId."'
							And ServiceProvider = 'getui'
							$conds_notifyMessageTargetId";
							
							$successAry['updateMessageStatus'][] = $this->db_db_query($_sql);
						}
					}
				}
			}
			return !in_array(false, $successAry);
		}
	}
}
?>