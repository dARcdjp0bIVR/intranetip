<?php
// editing by
 
/********************
 * change log:

 ********************/

include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
if (!defined("LIBHUAWEI_DEFINED")) {
	define("LIBHUAWEI_DEFINED", true);
	
	class libhuawei extends libdb {
		function libhuawei() {
			$this->libdb();
		}
		
		function sendRequestToCentralServer($schoolCode, $messageInfoAry) {
			/*
			global $intranet_root, $config_school_code, $plugin, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			//include_once($intranet_root.'/includes/eClassApp/libAES.php');
			
			$jsonObj = new JSON_obj();
			//$libaes = new libAES($eclassAppConfig['aesKey']);
			$successAry = array();
			
			$postParamAry = array();
			$postParamAry['RequestMethod'] = 'sendPushMessageByHuawei';
//			$postParamAry['SchoolCode'] = $schoolCode;
			$postParamAry['ApiVersion'] = $eclassAppConfig['pushMessage']['apiVersion']['huawei'];
			$postParamAry['MessageInfoAry'] = $messageInfoAry;
			$postJsonString = $jsonObj->encode($postParamAry);
			
//			$postEncryptedJsonAry = array();
//			$postEncryptedJsonAry['eClassRequestEncrypted'] = $libaes->encrypt($postJsonString);
//			$postEncryptedJsonString = $jsonObj->encode($postEncryptedJsonAry);
			
			$headers = array('Content-Type: application/json');
			
			session_write_close();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
			curl_setopt($ch, CURLOPT_URL, $eclassAppConfig['pushMessage']['apiPath']);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $postEncryptedJsonString);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			$responseJson = curl_exec($ch);
			curl_close($ch);
			
			$responseJson = standardizeFormPostValue($responseJson);
			$responseJsonAry = $jsonObj->decode($responseJson);
						
			return $responseJsonAry;
			*/
		}
		
		function getMaxDevicePerMessage() {
			global $eclassAppConfig;
			return $eclassAppConfig['pushMessage']['maxDevicePerMessage']['huawei'];
		}
		
		function getMaxMessagePayloadByte() {
			global $eclassAppConfig;
			return $eclassAppConfig['pushMessage']['maxMessagePayloadByte']['huawei'];
		}
		
		function buildMessageJson($notifyMessageId, $messageAry) {
			global $intranet_root, $config_school_code, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();

			$numOfMessage = count($messageAry);
			$maxNumOfDevicePerMessage = $this->getMaxDevicePerMessage();
			$maxMessagePayloadByte = $this->getMaxMessagePayloadByte();
			
			### consolidate message info
			$consolidatedMessageAry = array();
			$deviceCount = 0; 
			$messageCount = -1;

			foreach ((array)$messageAry as $_apnsType => $__appMessageAry) {
				$__numOfMessage = count($__appMessageAry);
				for ($i=0; $i<$__numOfMessage; $i++) {
					$_messageTitle = $__appMessageAry[$i]['messageTitle'];
					$_messageContent = $__appMessageAry[$i]['messageContent'];
					$_userAssoAry = $__appMessageAry[$i]['userAssoAry'];
					$_fromModule = $__appMessageAry[$i]['fromModule'];
					$_moduleRecordID = $__appMessageAry[$i]['moduleRecordID'];
					$_attachmentPath = $__appMessageAry[$i]['attachmentPath'];

					$_newMessage = true;
					foreach ((array)$_userAssoAry as $__recipientUserId => $__userInfoAry) {
						$__relatedUserIdAry = $__userInfoAry['relatedUserIdAry'];
						$__deviceIdAry = $__userInfoAry['deviceIdAry'];
						$__numOfDeviceId = count((array)$__deviceIdAry);

						for ($j=0; $j<$__numOfDeviceId; $j++) {
							$___deviceId = $__deviceIdAry[$j];

							$___openNewMessage = false;
							if ($_newMessage || $deviceCount == $maxNumOfDevicePerMessage) {
								$___openNewMessage = true;
							}
							else {
								// simulate "data" json to see if excess 4096 bytes
								// https://developer.huawei.com/consumer/en/doc/development/HMS-References/push-sendapi
								// => It has a payload limit of 4096 bytes.

								$___tmpRelatedUserIdAssoAry = (array)$consolidatedMessageAry[$_apnsType][$messageCount]['relatedUserIdAssoAry'] + (array)$__relatedUserIdAry;
								$___tmpMessagePayloadAry = $this->buildMessagePayloadAry($notifyMessageId, $_messageTitle, $_messageContent, $___tmpRelatedUserIdAssoAry, $_fromModule, $_moduleRecordID, $_attachmentPath);
								$___tmpMessagePayloadJsonString = $jsonObj->encode($___tmpMessagePayloadAry);

								if (strlen($___tmpMessagePayloadJsonString) > $maxMessagePayloadByte) {
									$___openNewMessage = true;
								}
							}

							if ($___openNewMessage) {
								$messageCount++;
								$deviceCount = 0;
								$_newMessage = false;

								$consolidatedMessageAry[$_apnsType][$messageCount]['messageTitle'] = $_messageTitle;
								$consolidatedMessageAry[$_apnsType][$messageCount]['messageContent'] = $_messageContent;
							}

							$consolidatedMessageAry[$_apnsType][$messageCount]['relatedUserIdAssoAry'] = (array)$consolidatedMessageAry[$_apnsType][$messageCount]['relatedUserIdAssoAry'] + (array)$__relatedUserIdAry;
							$consolidatedMessageAry[$_apnsType][$messageCount]['deviceIdAry'][] = $___deviceId;


							$consolidatedMessageAry[$_apnsType][$messageCount]['fromModule'] = $_fromModule;
							$consolidatedMessageAry[$_apnsType][$messageCount]['moduleRecordID'] = $_moduleRecordID;

							$consolidatedMessageAry[$_apnsType][$messageCount]['attachmentPath'] = $_attachmentPath;

							$deviceCount++;
						}
					}
				}
			}

			$$messageJsonAry = array();
			foreach($consolidatedMessageAry as $_apnsType => $_consolidatedMessageAry) {
				$numOfConsolidatedMessage = count($_consolidatedMessageAry);
				for ($i=0; $i<$numOfConsolidatedMessage; $i++) {
					$_messageTitle = $_consolidatedMessageAry[$i]['messageTitle'];
					$_messageContent = $_consolidatedMessageAry[$i]['messageContent'];
					$_relatedUserIdAssoAry = $_consolidatedMessageAry[$i]['relatedUserIdAssoAry'];
					$_deviceIdAry = $_consolidatedMessageAry[$i]['deviceIdAry'];
					$_fromModule = $_consolidatedMessageAry[$i]['fromModule'];
					$_moduleRecordID = $_consolidatedMessageAry[$i]['moduleRecordID'];
					$_attachmentPath = $_consolidatedMessageAry[$i]['attachmentPath'];

					$_messageDataAry = $this->buildMessagePayloadAry($notifyMessageId, $_messageTitle, $_messageContent, $_relatedUserIdAssoAry, $_fromModule, $_moduleRecordID, $_attachmentPath, $_deviceIdAry);


					$_messageDataAry['token'] = $_deviceIdAry;

					$data = array();
					$data['message'] = $_messageDataAry;

					$messageJsonAry[$_apnsType][$i] = $jsonObj->encode($data);
					$messageCount++;
				}
			}

			return $messageJsonAry;
		}
		
		function buildMessagePayloadAry($notifyMessageId, $messageTitle, $messageContent, $userMappingAry, $fromModule='', $moduleRecordID='', $attachmentPath='') {
			global $config_school_code, $intranet_root, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();

			$extraJsonAry = array();
			$extraJsonAry['IntranetReferenceID'] = $notifyMessageId;
			$extraJsonAry['SchoolCode'] = $config_school_code;
			$extraJsonAry['UserMapping'] = $userMappingAry;
			$extraJsonAry['FromModule'] = $fromModule;
			$extraJsonAry['ModuleRecordID'] = $moduleRecordID;
            $extraJsonAry['AttachmentUrl'] = $attachmentPath;

			$payloadAry = array();
			$payloadAry['MessageTitle'] = $messageTitle;
			$payloadAry['MessageContent'] = $messageContent;
			$payloadAry['ApiVersion'] = $eclassAppConfig['pushMessage']['apiVersion']['huawei'];
			$payloadAry['ExtraJson'] = $extraJsonAry;

			$message = array();
			$message['data'] = $jsonObj->encode($payloadAry);

			return $message;
		}
		
		function saveMessageResult($notifyMessageId, $resultAry, $parNotifyMessageTargetIdAry) {
			global $eclassAppConfig, $intranet_root;
			
			include_once($intranet_root.'/includes/eClassApp/libeClassApp.php');
			$leClassApp = new libeClassApp();
			
			if ($parNotifyMessageTargetIdAry != '') {
				$conds_notifyMessageTargetId = " AND NotifyMessageTargetID IN ('".implode("','", (array)$parNotifyMessageTargetIdAry)."') ";
			}
			
			$successAry = array();
			if ($resultAry['ErrorCode']) {
				$sql = "Update 
								INTRANET_APP_NOTIFY_MESSAGE_REFERENCE 
						Set 
								ErrorCode = '".$this->Get_Safe_Sql_Query($resultAry['ErrorCode'])."', 
								MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']."',
								DateModified = now()
						Where
								NotifyMessageID = '".$notifyMessageId."'
								And ServiceProvider = 'huawei'
								$conds_notifyMessageTargetId
						";
				$successAry['updateErrorCode'] = $this->db_db_query($sql);
			}
			else {
				$messageResultAry = $resultAry['result'];

				
				// order by recordid for safety
				$sql = "Select RecordID, DeviceRecordID, NotifyMessageTargetID From INTRANET_APP_NOTIFY_MESSAGE_REFERENCE Where NotifyMessageID = '".$notifyMessageId."' And ServiceProvider = 'huawei' $conds_notifyMessageTargetId Order By RecordID";
				$referenceDataAry = $this->returnResultSet($sql);
				
				$messageCount = 0;
				$dbDataAssoAry = array();
				foreach($messageResultAry as $_appType => $_messageResultAry) {
					$numOfMessage = count($_messageResultAry);
					for($i=0;$i<$numOfMessage;$i++) {
						$__code = $_messageResultAry[$i]['code'];
						$__requestId = $_messageResultAry[$i]['requestId'];

						$__referenceId = $referenceDataAry[$messageCount]['RecordID'];
						$messageCount++;

						if ($__referenceId == '') {
							// for safety only, prevent broken SQL
							continue;
						}

						$__messageStatus = '';
						$__errorCode = $__code;
						$__returnMessage = '';
						if ($__code == '80000000' || $__code == '80100000') {
							$__messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'];
						} else {
							$__messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
							$__returnMessage = $_messageResultAry[$i]['msg'];
						}

						$__tmpAry = array();
						$__tmpAry['MessageID'] = $__requestId;
						$__tmpAry['ErrorCode'] = $__errorCode;
						$__tmpAry['MessageStatus'] = $__messageStatus;
						$__tmpAry['ReturnMessage'] = $__returnMessage;
						$dbDataAssoAry[$__referenceId] = $__tmpAry;
					}
				}
			}
			
			$fieldAry = array('MessageID', 'ErrorCode', 'MessageStatus','ReturnMessage');
			$numOfField = count($fieldAry);
			
			$dbDataChunkAry = array_chunk($dbDataAssoAry, 500, true);
			$numOfChunk = count((array)$dbDataChunkAry);
			for ($i=0; $i<$numOfChunk; $i++) {
				$_dbDataAssoAry = $dbDataChunkAry[$i];
				$_recordIdAry = array_keys($_dbDataAssoAry);
				
				$_setFieldSqlAry = array();
				for ($j=0; $j<$numOfField; $j++) {
					$__dbField = $fieldAry[$j];
					
					$__dbFieldWhenAry = array();
					foreach ((array)$_dbDataAssoAry as $___recordId => $___recordDataAry) {
						$___fieldData = $___recordDataAry[$__dbField];
						$__dbFieldWhenAry[] = " WHEN $___recordId THEN '".$this->Get_Safe_Sql_Query($___fieldData)."' ";
					}
					$_setFieldSqlAry[] = " $__dbField = (CASE RecordID ".implode("\n", (array)$__dbFieldWhenAry)." END) ";
				}
				
				$_sql = "Update INTRANET_APP_NOTIFY_MESSAGE_REFERENCE SET ".implode(', ', (array)$_setFieldSqlAry).", DateModified = now() Where RecordID IN ('".implode("','", (array)$_recordIdAry)."') And MessageStatus != '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']."'";
				$successAry['updateMessageStatus'][] = $this->db_db_query($_sql);
			}
			return !in_array(false, $successAry);
		}
	}
}
?>