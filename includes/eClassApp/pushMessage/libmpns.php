<?php
// editing by 
/**
 * Change Log:
 * 2016-09-08 Pun
 *  - Modified saveMessageResult(), fix does not update INTRANET_APP_NOTIFY_MESSAGE_REFERENCE field
 */

include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
if (!defined("LIBMPNS_DEFINED")) {
	define("LIBMPNS_DEFINED", true);
	
	class libmpns extends libdb {
		function libmpns() {
			$this->libdb();
		}
		
		function sendRequestToCentralServer($schoolCode, $messageInfoAry) {
			global $intranet_root, $config_school_code, $plugin, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			//include_once($intranet_root.'/includes/eClassApp/libAES.php');
			
			$jsonObj = new JSON_obj();
			//$libaes = new libAES($eclassAppConfig['aesKey']);
			$successAry = array();
			
			$postParamAry = array();
			$postParamAry['RequestMethod'] = 'sendPushMessageByMpns';
//			$postParamAry['SchoolCode'] = $schoolCode;
			$postParamAry['ApiVersion'] = $eclassAppConfig['pushMessage']['apiVersion']['mpns'];
			$postParamAry['MessageInfoAry'] = $messageInfoAry;
			$postJsonString = $jsonObj->encode($postParamAry);
			
//			$postEncryptedJsonAry = array();
//			$postEncryptedJsonAry['eClassRequestEncrypted'] = $libaes->encrypt($postJsonString);
//			$postEncryptedJsonString = $jsonObj->encode($postEncryptedJsonAry);
			
			$headers = array('Content-Type: application/json');
			
			session_write_close();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
			curl_setopt($ch, CURLOPT_URL, $eclassAppConfig['pushMessage']['apiPath']);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $postEncryptedJsonString);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			$responseJson = curl_exec($ch);
			curl_close($ch);
			
			$responseJson = standardizeFormPostValue($responseJson);
			$responseJsonAry = $jsonObj->decode($responseJson);
						
			return $responseJsonAry;
		}
		
		function getMaxDevicePerMessage() {
			global $eclassAppConfig;
			return $eclassAppConfig['pushMessage']['maxDevicePerMessage']['mpns'];
		}
		
		function getMaxMessagePayloadByte() {
			global $eclassAppConfig;
			return $eclassAppConfig['pushMessage']['maxMessagePayloadByte']['mpns'];
		}
		
		function buildMessageJson($notifyMessageId, $messageAry) {
			global $intranet_root, $config_school_code, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();

			$numOfMessage = count($messageAry);
			$maxNumOfDevicePerMessage = $this->getMaxDevicePerMessage();
			$maxMessagePayloadByte = $this->getMaxMessagePayloadByte();
			
			### consolidate message info
			$consolidatedMessageAry = array();
			$deviceCount = 0; 
			$messageCount = -1;
			for ($i=0; $i<$numOfMessage; $i++) {
				$_messageTitle = $messageAry[$i]['messageTitle'];
				$_messageContent = $messageAry[$i]['messageContent'];
				$_userAssoAry = $messageAry[$i]['userAssoAry'];
		//debug_r($messageAry);exit;		
				$_newMessage = true;
				foreach ((array)$_userAssoAry as $__recipientUserId => $__userInfoAry) {
					$__relatedUserIdAry = $__userInfoAry['relatedUserIdAry'];
					$__deviceIdAry = $__userInfoAry['deviceIdAry'];
					$__numOfDeviceId = count((array)$__deviceIdAry);
					
					for ($j=0; $j<$__numOfDeviceId; $j++) {
						$___deviceId = $__deviceIdAry[$j];
						
						$___openNewMessage = false;
						if ($_newMessage || $deviceCount == $maxNumOfDevicePerMessage) {
							$___openNewMessage = true;
						}
						else {
							// simulate "data" json to see if excess 4096 bytes
							// http://developer.android.com/google/gcm/adv.html
							// => It has a payload limit of 4096 bytes.
							
							//$___tmpRelatedUserIdAssoAry = array_merge((array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'], (array)$__relatedUserIdAry);
							$___tmpRelatedUserIdAssoAry = (array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] + (array)$__relatedUserIdAry;
							$___tmpMessagePayloadAry = $this->buildMessagePayloadAry($notifyMessageId, $_messageTitle, $_messageContent, $___tmpRelatedUserIdAssoAry);
							$___tmpMessagePayloadJsonString = $jsonObj->encode($___tmpMessagePayloadAry);
							
							if (strlen($___tmpMessagePayloadJsonString) > $maxMessagePayloadByte) {
								$___openNewMessage = true;
							}
						}
						
						if ($___openNewMessage) {
							$messageCount++;
							$deviceCount = 0;
							$_newMessage = false;
							
							$consolidatedMessageAry[$messageCount]['messageTitle'] = $_messageTitle;
							$consolidatedMessageAry[$messageCount]['messageContent'] = $_messageContent;
						}
						
						//$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] = array_merge((array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'], (array)$__relatedUserIdAry);
						$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] = (array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] + (array)$__relatedUserIdAry;
						$consolidatedMessageAry[$messageCount]['deviceIdAry'][] = $___deviceId;
						$deviceCount++;
					}
				}
			}
			
			$numOfConsolidatedMessage = count($consolidatedMessageAry);
			for ($i=0; $i<$numOfConsolidatedMessage; $i++) {
				$_messageTitle = $consolidatedMessageAry[$i]['messageTitle'];
				$_messageContent = $consolidatedMessageAry[$i]['messageContent'];
				$_relatedUserIdAssoAry = $consolidatedMessageAry[$i]['relatedUserIdAssoAry'];
				$_deviceIdAry = $consolidatedMessageAry[$i]['deviceIdAry'];
				
				$_messageDataAry = $this->buildMessagePayloadAry($notifyMessageId, $_messageTitle, $_messageContent, $_relatedUserIdAssoAry);
				
				// 'dry_run' => true, => will not send out, just for simulation
//				$_pushMessageDataAry = array(
//					'dry_run' => true,
//			        'data' => $_messageDataAry,
//			        'registration_ids' => $_deviceIdAry
//			    );
			    $_pushMessageDataAry = array(
			    	'data' => $_messageDataAry,
			        'registration_ids' => $_deviceIdAry
			    );
				//bug_r($_pushMessageDataAry);
				
				$messageJsonAry[$i] = $jsonObj->encode($_pushMessageDataAry);
				$messageCount++;
			}
			return $messageJsonAry;
		}
		
		function buildMessagePayloadAry($notifyMessageId, $messageTitle, $messageContent, $userMappingAry) {
			global $config_school_code, $intranet_root, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();
			
			/* Old Method */
			$extraJsonAry = array();
			$extraJsonAry['IntranetReferenceID'] = $notifyMessageId;
			$extraJsonAry['SchoolCode'] = $config_school_code;
			
			$extraJsonAry['UserMapping'] = array();
			foreach($userMappingAry as $id=>$d1){
				if(ctype_digit($id)){
					$extraJsonAry['UserMapping'][] = array(
						'parent'=>$id,
						'student'=>$d1
					);
				}
			}
			
			$extraJsonString = $jsonObj->encode($extraJsonAry);
			/**/
			
			/* New Method * /
			foreach($userMappingAry as $id=>$d1){
				if(ctype_digit($id)){
					$intranetParentID = $id;
					$intranetStudentID = $d1[0];
					break;
				}
			}
			$extraJsonString = "type=Toast&intranetParentID={$intranetParentID}&intranetStudentID={$intranetStudentID}&schoolCode={$config_school_code}";
			/**/
			
			$payloadAry = array();
			$payloadAry['MessageTitle'] = $messageTitle;
			$payloadAry['MessageContent'] = $messageContent;
			$payloadAry['ApiVersion'] = $eclassAppConfig['pushMessage']['apiVersion']['mpns'];
			$payloadAry['ExtraJson'] = $extraJsonString;
			
			return $payloadAry;
		}
		
		function saveMessageResult($notifyMessageId, $resultAry, $parNotifyMessageTargetIdAry) {
			global $eclassAppConfig, $intranet_root;
			
			include_once($intranet_root.'/includes/eClassApp/libeClassApp.php');
			$leClassApp = new libeClassApp();
			
			if ($parNotifyMessageTargetIdAry != '') {
				$conds_notifyMessageTargetId = " AND NotifyMessageTargetID IN ('".implode("','", (array)$parNotifyMessageTargetIdAry)."') ";
			}
			
			$successAry = array();
			$result = $resultAry['result'][0];
			if ($result['ErrorCode']) {
				// ErrorCode Reference: https://msdn.microsoft.com/en-us/library/windows/apps/hh465435.aspx#WNSResponseCodes
				$sql = "Update 
					INTRANET_APP_NOTIFY_MESSAGE_REFERENCE 
				Set 
					ErrorCode = '".$this->Get_Safe_Sql_Query($result['ErrorCode'])."', 
					MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']."',
					DateModified = now()
				Where
					NotifyMessageID = '".$notifyMessageId."'
				And 
				    ServiceProvider = 'mpns'
					$conds_notifyMessageTargetId
				";
				$successAry['updateErrorCode'] = $this->db_db_query($sql);
				//debug_r($sql);
				return false;
			}else{
			    $sql = "Update
					INTRANET_APP_NOTIFY_MESSAGE_REFERENCE
				Set
					MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess']."',
					DateModified = now()
				Where
					NotifyMessageID = '".$notifyMessageId."'
				And 
					ServiceProvider = 'mpns'
					$conds_notifyMessageTargetId
				";
			    $this->db_db_query($sql);
			}
			//debug_r($sql);
			return true;
		}
	}
}
?>