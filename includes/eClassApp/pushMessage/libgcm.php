<?php
// editing by Roy
 
/********************
 * change log:
 * 2015-09-23 Roy: [ip.2.5.6.10.1]: consolidate message ary cannot get fromModule & moduleRecordID
 * 2015-07-03 Roy: [ip.2.5.6.7.1.0]: add parameter fromModule and moduleRecordID for link up push message to other module
 ********************/

include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
if (!defined("LIBGCM_DEFINED")) {
	define("LIBGCM_DEFINED", true);
	
	class libgcm extends libdb {
		function libgcm() {
			$this->libdb();
		}
		
		function sendRequestToCentralServer($schoolCode, $messageInfoAry) {
			global $intranet_root, $config_school_code, $plugin, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			//include_once($intranet_root.'/includes/eClassApp/libAES.php');
			
			$jsonObj = new JSON_obj();
			//$libaes = new libAES($eclassAppConfig['aesKey']);
			$successAry = array();
			
			$postParamAry = array();
			$postParamAry['RequestMethod'] = 'sendPushMessageByGcm';
//			$postParamAry['SchoolCode'] = $schoolCode;
			$postParamAry['ApiVersion'] = $eclassAppConfig['pushMessage']['apiVersion']['gcm'];
			$postParamAry['MessageInfoAry'] = $messageInfoAry;
			$postJsonString = $jsonObj->encode($postParamAry);
			
//			$postEncryptedJsonAry = array();
//			$postEncryptedJsonAry['eClassRequestEncrypted'] = $libaes->encrypt($postJsonString);
//			$postEncryptedJsonString = $jsonObj->encode($postEncryptedJsonAry);
			
			$headers = array('Content-Type: application/json');
			
			session_write_close();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
			curl_setopt($ch, CURLOPT_URL, $eclassAppConfig['pushMessage']['apiPath']);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $postEncryptedJsonString);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			$responseJson = curl_exec($ch);
			curl_close($ch);
			
			$responseJson = standardizeFormPostValue($responseJson);
			$responseJsonAry = $jsonObj->decode($responseJson);
						
			return $responseJsonAry;
		}
		
		function getMaxDevicePerMessage() {
			global $eclassAppConfig;
			return $eclassAppConfig['pushMessage']['maxDevicePerMessage']['gcm'];
		}
		
		function getMaxMessagePayloadByte() {
			global $eclassAppConfig;
			return $eclassAppConfig['pushMessage']['maxMessagePayloadByte']['gcm'];
		}
		
		function buildMessageJson($notifyMessageId, $messageAry) {
			global $intranet_root, $config_school_code, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();

			$numOfMessage = count($messageAry);
			$maxNumOfDevicePerMessage = $this->getMaxDevicePerMessage();
			$maxMessagePayloadByte = $this->getMaxMessagePayloadByte();
			
			### consolidate message info
			$consolidatedMessageAry = array();
			$deviceCount = 0; 
			$messageCount = -1;
			for ($i=0; $i<$numOfMessage; $i++) {
				$_messageTitle = $messageAry[$i]['messageTitle'];
				$_messageContent = $messageAry[$i]['messageContent'];
				$_userAssoAry = $messageAry[$i]['userAssoAry'];
				$_fromModule = $messageAry[$i]['fromModule'];
				$_moduleRecordID = $messageAry[$i]['moduleRecordID'];
                $_attachmentPath = $messageAry[$i]['attachmentPath'];

				$_newMessage = true;
				foreach ((array)$_userAssoAry as $__recipientUserId => $__userInfoAry) {
					$__relatedUserIdAry = $__userInfoAry['relatedUserIdAry'];
					$__deviceIdAry = $__userInfoAry['deviceIdAry'];
					$__numOfDeviceId = count((array)$__deviceIdAry);
					
					for ($j=0; $j<$__numOfDeviceId; $j++) {
						$___deviceId = $__deviceIdAry[$j];
						
						$___openNewMessage = false;
						if ($_newMessage || $deviceCount == $maxNumOfDevicePerMessage) {
							$___openNewMessage = true;
						}
						else {
							// simulate "data" json to see if excess 4096 bytes
							// http://developer.android.com/google/gcm/adv.html
							// => It has a payload limit of 4096 bytes.
							
							//$___tmpRelatedUserIdAssoAry = array_merge((array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'], (array)$__relatedUserIdAry);
							$___tmpRelatedUserIdAssoAry = (array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] + (array)$__relatedUserIdAry;
							$___tmpMessagePayloadAry = $this->buildMessagePayloadAry($notifyMessageId, $_messageTitle, $_messageContent, $___tmpRelatedUserIdAssoAry, $_fromModule, $_moduleRecordID, $_attachmentPath);
							$___tmpMessagePayloadJsonString = $jsonObj->encode($___tmpMessagePayloadAry);
							
							if (strlen($___tmpMessagePayloadJsonString) > $maxMessagePayloadByte) {
								$___openNewMessage = true;
							}
						}
						
						if ($___openNewMessage) {
							$messageCount++;
							$deviceCount = 0;
							$_newMessage = false;
							
							$consolidatedMessageAry[$messageCount]['messageTitle'] = $_messageTitle;
							$consolidatedMessageAry[$messageCount]['messageContent'] = $_messageContent;
						}
						
						//$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] = array_merge((array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'], (array)$__relatedUserIdAry);
						$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] = (array)$consolidatedMessageAry[$messageCount]['relatedUserIdAssoAry'] + (array)$__relatedUserIdAry;
						$consolidatedMessageAry[$messageCount]['deviceIdAry'][] = $___deviceId;
						
						// 20150923 Roy - [B85981] set fromModule & moduleRecordID to consolidatedMessageAry
						$consolidatedMessageAry[$messageCount]['fromModule'] = $_fromModule;
						$consolidatedMessageAry[$messageCount]['moduleRecordID'] = $_moduleRecordID;

                        $consolidatedMessageAry[$messageCount]['attachmentPath'] = $_attachmentPath;

						$deviceCount++;
					}
				}
			}
			
			$numOfConsolidatedMessage = count($consolidatedMessageAry);
			for ($i=0; $i<$numOfConsolidatedMessage; $i++) {
				$_messageTitle = $consolidatedMessageAry[$i]['messageTitle'];
				$_messageContent = $consolidatedMessageAry[$i]['messageContent'];
				$_relatedUserIdAssoAry = $consolidatedMessageAry[$i]['relatedUserIdAssoAry'];
				$_deviceIdAry = $consolidatedMessageAry[$i]['deviceIdAry'];
				$_fromModule = $consolidatedMessageAry[$i]['fromModule'];
				$_moduleRecordID = $consolidatedMessageAry[$i]['moduleRecordID'];
                $_attachmentPath = $consolidatedMessageAry[$i]['attachmentPath'];

				$_messageDataAry = $this->buildMessagePayloadAry($notifyMessageId, $_messageTitle, $_messageContent, $_relatedUserIdAssoAry, $_fromModule, $_moduleRecordID, $_attachmentPath);
				
				// 'dry_run' => true, => will not send out, just for simulation
//				$_pushMessageDataAry = array(
//					'dry_run' => true,
//			        'data' => $_messageDataAry,
//			        'registration_ids' => $_deviceIdAry
//			    );
			    $_pushMessageDataAry = array(
			    	'data' => $_messageDataAry,
			        'registration_ids' => $_deviceIdAry
			    );
				
				$messageJsonAry[$i] = $jsonObj->encode($_pushMessageDataAry);
				$messageCount++;
			}
			
			return $messageJsonAry;
		}
		
		function buildMessagePayloadAry($notifyMessageId, $messageTitle, $messageContent, $userMappingAry, $fromModule='', $moduleRecordID='', $attachmentPath='') {
			global $config_school_code, $intranet_root, $eclassAppConfig;
			
			include_once($intranet_root.'/includes/json.php');
			$jsonObj = new JSON_obj();
			
//			$originalContent = $messageContent;
//			$messageContent = substr($messageContent, 0, 400);
//			if ($originalContent != $messageContent) {
//				$messageContent .= '...';
//			}
			
			$extraJsonAry = array();
			$extraJsonAry['IntranetReferenceID'] = $notifyMessageId;
			$extraJsonAry['SchoolCode'] = $config_school_code;
			$extraJsonAry['UserMapping'] = $userMappingAry;
			$extraJsonAry['FromModule'] = $fromModule;
			$extraJsonAry['ModuleRecordID'] = $moduleRecordID;
            $extraJsonAry['AttachmentUrl'] = $attachmentPath;

			$extraJsonString = $jsonObj->encode($extraJsonAry);
			
			$payloadAry = array();
			$payloadAry['MessageTitle'] = $messageTitle;
			$payloadAry['MessageContent'] = $messageContent;
			$payloadAry['ApiVersion'] = $eclassAppConfig['pushMessage']['apiVersion']['gcm'];
			$payloadAry['ExtraJson'] = $extraJsonString;
			
			return $payloadAry;
		}
		
		function saveMessageResult($notifyMessageId, $resultAry, $parNotifyMessageTargetIdAry) {
			global $eclassAppConfig, $intranet_root;
			
			include_once($intranet_root.'/includes/eClassApp/libeClassApp.php');
			$leClassApp = new libeClassApp();
			
			if ($parNotifyMessageTargetIdAry != '') {
				$conds_notifyMessageTargetId = " AND NotifyMessageTargetID IN ('".implode("','", (array)$parNotifyMessageTargetIdAry)."') ";
			}
			
			$successAry = array();
			if ($resultAry['ErrorCode']) {
				$sql = "Update 
								INTRANET_APP_NOTIFY_MESSAGE_REFERENCE 
						Set 
								ErrorCode = '".$this->Get_Safe_Sql_Query($resultAry['ErrorCode'])."', 
								MessageStatus = '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']."',
								DateModified = now()
						Where
								NotifyMessageID = '".$notifyMessageId."'
								And ServiceProvider = 'gcm'
								$conds_notifyMessageTargetId
						";
				$successAry['updateErrorCode'] = $this->db_db_query($sql);
			}
			else {
				$messageResultAry = $resultAry['result'];
				$numOfMessage = count($messageResultAry);
				
				// order by recordid for safety
				$sql = "Select RecordID, DeviceRecordID, NotifyMessageTargetID From INTRANET_APP_NOTIFY_MESSAGE_REFERENCE Where NotifyMessageID = '".$notifyMessageId."' And ServiceProvider = 'gcm' $conds_notifyMessageTargetId Order By RecordID";
				$referenceDataAry = $this->returnResultSet($sql);
				
				$messageCount = 0;
				$dbDataAssoAry = array();
				for ($i=0; $i<$numOfMessage; $i++) {
					$_multicastId = $messageResultAry[$i]['multicast_id'];
					$_resultAry = $messageResultAry[$i]['results'];
					
					$_numOfResult = count((array)$_resultAry);
					for ($j=0; $j<$_numOfResult; $j++) {
						$__messageId = $_resultAry[$j]['message_id'];
						$__errorCode = $_resultAry[$j]['error'];
						$__registrationId = $_resultAry[$j]['registration_id'];
						
						$__referenceId = $referenceDataAry[$messageCount]['RecordID'];
						$__deviceRecordId = $referenceDataAry[$messageCount]['DeviceRecordID'];
						$__messageTargetId = $referenceDataAry[$messageCount]['NotifyMessageTargetID'];
						$messageCount++;
						
						// process canonical id if there are any
						if ($__registrationId != '') {
							// get device info 
							$__deviceInfoAry = $leClassApp->getUserDevice('', $__deviceRecordId);
							$__userId = $__deviceInfoAry[0]['UserID'];
							$__deviceOs = $__deviceInfoAry[0]['DeviceOS'];
							$__deviceModel = $__deviceInfoAry[0]['DeviceModel'];
							
							$__disableReason = $eclassAppConfig['APP_USER_DEVICE']['DisableReason']['canonical_ids'].'_'.$__referenceId;
							$successAry['disableOldDeviceId'][$__referenceId] = $leClassApp->inactivateUserDevice('', $parUserId='', $parDeviceOS='', $__disableReason, $__deviceRecordId);
							$successAry['registerNewDeviceId'][$__referenceId] = $leClassApp->saveUserDevice('', $__registrationId, $__deviceOs, $__deviceModel, $parApnsType='', $__userId);
						}
						
						// disable device if have following error returned
						if ($__errorCode != '') {
							$__disableDevice = false;
							$__disableReason = '';
							switch ($__errorCode) {
								case 'NotRegistered':
									$__disableDevice = true;
									$__disableReason = $eclassAppConfig['APP_USER_DEVICE']['DisableReason']['notRegisteredId'];
									break;
								case 'MismatchSenderId':
									$__disableDevice = true;
									$__disableReason = $eclassAppConfig['APP_USER_DEVICE']['DisableReason']['wrongSenderId'];
									break;
							}
							
							if ($__disableDevice) {
								$__disableReason .= '_'.$__referenceId;
								$successAry['disableOldDeviceId'][$__referenceId] = $leClassApp->inactivateUserDevice('', $parUserId='', $parDeviceOS='', $__disableReason, $__deviceRecordId);
							}
						}
						
						$__messageStatus = '';
						if ($__errorCode != '' || $__messageId == '') {
							$__messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
						}
						else {
							$__messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'];
						}
						
						$__tmpAry = array();
						$__tmpAry['MulticastID'] = $_multicastId;
						$__tmpAry['MessageID'] = $__messageId;
						$__tmpAry['ErrorCode'] = $__errorCode;
						$__tmpAry['MessageStatus'] = $__messageStatus;
						$__tmpAry['CanonicalID'] = $__registrationId;
						$dbDataAssoAry[$__referenceId] = $__tmpAry; 
					}
				}
			}
			
			$fieldAry = array('MulticastID', 'MessageID', 'ErrorCode', 'MessageStatus', 'CanonicalID');
			$numOfField = count($fieldAry);
			
			$dbDataChunkAry = array_chunk($dbDataAssoAry, 500, true);
			$numOfChunk = count((array)$dbDataChunkAry);
			for ($i=0; $i<$numOfChunk; $i++) {
				$_dbDataAssoAry = $dbDataChunkAry[$i];
				$_recordIdAry = array_keys($_dbDataAssoAry);
				
				$_setFieldSqlAry = array();
				for ($j=0; $j<$numOfField; $j++) {
					$__dbField = $fieldAry[$j];
					
					$__dbFieldWhenAry = array();
					foreach ((array)$_dbDataAssoAry as $___recordId => $___recordDataAry) {
						$___fieldData = $___recordDataAry[$__dbField];
						$__dbFieldWhenAry[] = " WHEN $___recordId THEN '".$this->Get_Safe_Sql_Query($___fieldData)."' ";
					}
					$_setFieldSqlAry[] = " $__dbField = (CASE RecordID ".implode("\n", (array)$__dbFieldWhenAry)." END) ";
				}
				
				$_sql = "Update INTRANET_APP_NOTIFY_MESSAGE_REFERENCE SET ".implode(', ', (array)$_setFieldSqlAry).", DateModified = now() Where RecordID IN ('".implode("','", (array)$_recordIdAry)."') And MessageStatus != '".$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead']."'";
				$successAry['updateMessageStatus'][] = $this->db_db_query($_sql);
			}
			return !in_array(false, $successAry);
		}
	}
}
?>