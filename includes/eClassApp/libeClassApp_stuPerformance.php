<?php
// editing by Philips
 
/********************
 * 
 * Date : 2018-09-13 Philips
 *          modified getClassViewReportHeaderAry(), getClassViewReportDataAry()
 *          added getSubjectGroupTeacherByClass()
 * 
 * 
 ********************/
if (!defined("LIBECLASSAPP_STUPERFORMACE_DEFINED")) {
	define("LIBECLASSAPP_STUPERFORMACE_DEFINED", true);
	
	class libeClassApp_stuPerformance extends libdb {
		
		function libeClassApp_stuPerformance() {
			$this->libdb();
		}
		
		function insertRecord($parStudentId, $parSubjectGroupId, $parScore, $parComment, $parUserId) {
			$sql = "INSERT INTO INTRANET_APP_PERFORMANCE_STUDENT 
						(StudentID, TeacherID,SubjectGroupID,Score, Comment,DateInput,InputBy,DateModified,ModifiedBy)
	        		VALUES 
						('$parStudentId','$parUserId','$parSubjectGroupId','".$this->Get_Safe_Sql_Query($parScore)."','".$this->Get_Safe_Sql_Query($parComment)."',now(),'$parUserId',now(),'$parUserId')
					";
			return $this->db_db_query($sql);
		}
		
		function updateRecord($parRecordId, $parScore, $parComment, $parUserId) {
			$sql = "UPDATE INTRANET_APP_PERFORMANCE_STUDENT 
                   SET 
                        Score='".$this->Get_Safe_Sql_Query($parScore)."', Comment='".$this->Get_Safe_Sql_Query($parComment)."',
                        DateModified=now(), ModifiedBy = '".$parUserId."' 
                   Where 
                        PerformanceRecordID='".$parRecordId."' limit 1";
			return $this->db_db_query($sql);
		}
		
		function getRecord($studentIdAry, $subjectGroupIdAry, $startDate, $endDate) {
			$conds_studentId = '';
			if ($studentIdAry !== '') {
				$conds_studentId = " AND StudentID IN ('".implode("','", (array)$studentIdAry)."') ";
			}
			
			$conds_subjectGroupId = '';
			if ($subjectGroupIdAry !== '') {
				$conds_subjectGroupId = " AND SubjectGroupID IN ('".implode("','", (array)$subjectGroupIdAry)."') ";
			}
			
			$conds_startDate = '';
			if ($startDate !== '') {
				$conds_startDate = " AND DATE(DateInput) >= '$startDate' ";
			}
			
			$conds_endDate = '';
			if ($endDate !== '') {
				$conds_endDate = " AND DATE(DateInput) <= '$endDate' ";
			}
			
			$sql = "SELECT 
							StudentID, SubjectGroupID, Score, Comment, DateInput 
					FROM 
							INTRANET_APP_PERFORMANCE_STUDENT
					WHERE
							1
							$conds_studentId
							$conds_subjectGroupId
							$conds_startDate
							$conds_endDate
					ORDER BY
							DateInput
					";
			return $this->returnResultSet($sql);
		}
		
		function getReportTopTabMenuAry($parCurView) {
			global $PATH_WRT_ROOT, $Lang;
			
			include_once($PATH_WRT_ROOT."includes/libteaching.php");
			$libteaching = new libteaching();
			$isClassTeacher = $libteaching->Is_Class_Teacher($_SESSION['UserID']);
			$isSubjectTeacher = $libteaching->Is_Subject_Teacher($_SESSION['UserID']);
			$isTeacherAppAdmin = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"];
			
			$TabsArr = array();
			
			if ($isClassTeacher || $isTeacherAppAdmin) {
				$TabsArr[] = array($Lang['General']['Class'], 'class_view.php', ($parCurView=='class'));
			}
			if ($isSubjectTeacher) {
				$TabsArr[] = array($Lang['SysMgr']['SubjectClassMapping']['Subject'], 'subject_view.php', ($parCurView=='subject'));
			}
			
			return $TabsArr;
		}
		
		function getClassViewReportHeaderAry($viewType) {
			global $Lang;
			
			$headerAry = array();
			
			$headerAry[] = $Lang['General']['Class'];
			$headerAry[] = $Lang['General']['ClassNumber'];
			$headerAry[] = $Lang['SysMgr']['FormClassMapping']['StudentName'];
			
			if ($viewType == 'raw') {
			    $headerAry[] = $Lang['SysMgr']['SubjectClassMapping']['Subject'];
			    $headerAry[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
				$headerAry[] = $Lang['General']['Date'];
				$headerAry[] = $Lang['eClassApp']['StudentPerformance']['ActScore'];
				$headerAry[] = $Lang['eClassApp']['StudentPerformance']['Comment'];
			}
			else if ($viewType == 'statistics') {
			    $headerAry[] = $Lang['SysMgr']['SubjectClassMapping']['Subject'];
			    $headerAry[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
				$headerAry[] = $Lang['eClassApp']['StudentPerformance']['TotalActScore'];
				$headerAry[] = $Lang['eClassApp']['StudentPerformance']['NumOfComment'];
			}
			else if ($viewType == 'STSummary'){
			    
			}
			
			return $headerAry;
		}
		
		function getClassViewReportDataAry($viewType, $startDate, $endDate, $classId) {
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/libuser.php');
			include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			$fcm = new form_class_manage();
			$scm = new subject_class_mapping();
			$lsubject = new subject();
			
			// get students
			$studentAry = $fcm->Get_Student_By_Class($classId);
			$studentIdAry = Get_Array_By_Key($studentAry, 'UserID');
			$userObj = new libuser('', '', $studentIdAry);
			$numOfStudent = count($studentAry);
			
			// get subjects
			$subjectAry = $scm->Get_Subject_List_With_Component();
			$numOfSubject = count($subjectAry);
			
			// get terms
			$termIdAry = Get_Array_By_Key($fcm->Get_Term_By_Date_Range($startDate, $endDate), 'YearTermID');
			
			// get subject groups
			$subjectGroupAry = $lsubject->Get_Subject_Group_List($termIdAry, '', '', '', $returnAsso=0, $classId, '', $InYearClassOnly=true);
			$subjectGroupAssoAry = BuildMultiKeyAssoc($subjectGroupAry, 'RecordID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			$subjectGroupIdAry = Get_Array_By_Key($subjectGroupAry, 'SubjectGroupID');
			
			// get performance records
			$performanceAssoAry = BuildMultiKeyAssoc($this->getRecord($studentIdAry, $subjectGroupIdAry, $startDate, $endDate), array('StudentID', 'SubjectGroupID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$dataAry = array();
			$counter = 0;
			// STSummary Counter
			$STcounter = array();
			for ($i=0; $i<$numOfStudent; $i++) {
				$_studentId = $studentAry[$i]['UserID'];
				$_studentName = $studentAry[$i]['StudentName'];
				
				$userObj->loadUserData($_studentId);
				$_className = $userObj->ClassName;
				$_classNumber = $userObj->ClassNumber;
				
				for ($j=0; $j<$numOfSubject; $j++) {
					$__subjectId = $subjectAry[$j]['SubjectID'];
					$__subjectName = Get_Lang_Selection($subjectAry[$j]['SubjectDescB5'], $studentAry[$j]['SubjectDescEN']);
					
					$__subjectGroupAry = (array)$subjectGroupAssoAry[$__subjectId];
					$__numOfSubjectGroup = count($__subjectGroupAry);
					for ($k=0; $k<$__numOfSubjectGroup; $k++) {
						$___subjectGroupId = $__subjectGroupAry[$k]['SubjectGroupID']; 
						$___subjectGroupName = Get_Lang_Selection($__subjectGroupAry[$k]['ClassTitleB5'], $__subjectGroupAry[$k]['ClassTitleEN']);
						
						$___performanceAry = (array)$performanceAssoAry[$_studentId][$___subjectGroupId];
						$___numOfPerformance = count($___performanceAry);
						
						$___subjectScoreOfStudent = 0;
						$___numOfCommentOfStudent = 0;
						for ($l=0; $l<$___numOfPerformance; $l++) {
							$____dateInput = $___performanceAry[$l]['DateInput'];
							$____score = $___performanceAry[$l]['Score'];
							$____comment = trim($___performanceAry[$l]['Comment']);
							
							if ($viewType == 'raw') {
								$dataAry[$counter]['className'] = $_className;
								$dataAry[$counter]['classNumber'] = $_classNumber;
								$dataAry[$counter]['studentName'] = $_studentName;
								$dataAry[$counter]['subjectName'] = $__subjectName;
								$dataAry[$counter]['subjectGroupName'] = $___subjectGroupName;
								$dataAry[$counter]['dateInput'] = $____dateInput;
								$dataAry[$counter]['score'] = $____score;
								$dataAry[$counter]['comment'] = $____comment;
								$counter++;
							}
							else if ($viewType == 'statistics') {
								$___subjectScoreOfStudent += $____score;
								
								if ($____comment != '') {
									$___numOfCommentOfStudent++;
								}
							}
						}
						
						if ($viewType == 'statistics') {
							$dataAry[$counter]['className'] = $_className;
							$dataAry[$counter]['classNumber'] = $_classNumber;
							$dataAry[$counter]['studentName'] = $_studentName;
							$dataAry[$counter]['subjectName'] = $__subjectName;
							$dataAry[$counter]['subjectGroupName'] = $___subjectGroupName;
							$dataAry[$counter]['totalScore'] = $___subjectScoreOfStudent;
							$dataAry[$counter]['numOfComment'] = $___numOfCommentOfStudent;
							$counter++;
						}
						else if($viewType == 'STSummary'){
						    if(!in_array($_studentId, $STcounter)){
						        $dataAry[$counter]['studentId'] = $_studentId;
						        $dataAry[$counter]['className'] = $_className;
						        $dataAry[$counter]['classNumber'] = $_classNumber;
						        $dataAry[$counter]['studentName'] = $_studentName;
						        $STcounter[] = $_studentId;
						        $counter++;
						    }
						}
					}
				}
			}
			
			return $dataAry;
		}
		
		function getSubjectViewReportHeaderAry($viewType) {
			global $Lang;
			
			$headerAry = array();
			
			$headerAry[] = $Lang['SysMgr']['SubjectClassMapping']['Subject'];
			$headerAry[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
			$headerAry[] = $Lang['General']['Class'];
			$headerAry[] = $Lang['General']['ClassNumber'];
			$headerAry[] = $Lang['SysMgr']['FormClassMapping']['StudentName'];
			
			if ($viewType == 'raw') {
				$headerAry[] = $Lang['General']['Date'];
				$headerAry[] = $Lang['eClassApp']['StudentPerformance']['ActScore'];
				$headerAry[] = $Lang['eClassApp']['StudentPerformance']['Comment'];
			}
			else if ($viewType == 'statistics') {
				$headerAry[] = $Lang['eClassApp']['StudentPerformance']['TotalActScore'];
				$headerAry[] = $Lang['eClassApp']['StudentPerformance']['NumOfComment'];
			}
			
			return $headerAry;
		}
		
		function getSubjectViewReportDataAry($viewType, $startDate, $endDate, $subjectGroupIdAry) {
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
			include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
			$fcm = new form_class_manage();
			$scm = new subject_class_mapping();
			$lsubject = new subject();
			
			// get students
			$studentAry = array();
			$numOfSubjectGroup = count($subjectGroupIdAry);
			for ($i=0; $i<$numOfSubjectGroup; $i++) {
				$_subjectGroupId = $subjectGroupIdAry[$i];
				$_subjectGroupObj = new subject_term_class($_subjectGroupId);
				
				$studentAry = array_merge($studentAry, $_subjectGroupObj->Get_Subject_Group_Student_List());
// 				debug_pr($studentAry);
			}
			$subjectGroupStudentAssoAry = BuildMultiKeyAssoc($studentAry, 'SubjectGroupID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			$studentIdAry = Get_Array_By_Key($studentAry, 'UserID');
			$userObj = new libuser('', '', $studentIdAry);
			unset($studentAry);
			
			// get subjects
			$subjectAry = $scm->Get_Subject_List_With_Component();
			$numOfSubject = count($subjectAry);
			
			// get subject groups
			$subjectGroupAssoAry = BuildMultiKeyAssoc($lsubject->Get_Subject_Group_List($YearTermID='', $ClassLevelID='', $subjectGroupIdAry), 'RecordID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
						
			// get performance records
			$performanceAssoAry = BuildMultiKeyAssoc($this->getRecord($studentIdAry, $subjectGroupIdAry, $startDate, $endDate), array('StudentID', 'SubjectGroupID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			
			$dataAry = array();
			$counter = 0;
			for ($i=0; $i<$numOfSubject; $i++) {
				$_subjectId = $subjectAry[$i]['SubjectID'];
				$_subjectName = Get_Lang_Selection($subjectAry[$i]['SubjectDescB5'], $studentAry[$i]['SubjectDescEN']);
				
				$_subjectGroupAry = (array)$subjectGroupAssoAry[$_subjectId];
				$_numOfSubjectGroup = count($_subjectGroupAry);
				for ($j=0; $j<$_numOfSubjectGroup; $j++) {
					$__subjectGroupId = $_subjectGroupAry[$j]['SubjectGroupID'];
					$__subjectGroupName = Get_Lang_Selection($_subjectGroupAry[$j]['ClassTitleB5'], $_subjectGroupAry[$j]['ClassTitleEN']);
					
					$__studentAry = (array)$subjectGroupStudentAssoAry[$__subjectGroupId];
					$__numOfStudent = count($__studentAry);
					for ($k=0; $k<$__numOfStudent; $k++) {
						$___studentId = $__studentAry[$k]['UserID'];
						$___studentName = $__studentAry[$k]['StudentName'];
						
						$userObj->loadUserData($___studentId);
						$___className = $userObj->ClassName; 
						$___classNumber = $userObj->ClassNumber;
						
						$___performanceAry = (array)$performanceAssoAry[$___studentId][$__subjectGroupId];
						$___numOfPerformance = count($___performanceAry);
						
						$___subjectScoreOfStudent = 0;
						$___numOfCommentOfStudent = 0;
						for ($l=0; $l<$___numOfPerformance; $l++) {
							$____dateInput = $___performanceAry[$l]['DateInput'];
							$____score = $___performanceAry[$l]['Score'];
							$____comment = trim($___performanceAry[$l]['Comment']);
							
							if ($viewType == 'raw') {
								$dataAry[$counter]['subjectName'] = $_subjectName;
								$dataAry[$counter]['subjectGroupName'] = $__subjectGroupName;
								$dataAry[$counter]['className'] = $___className;
								$dataAry[$counter]['classNumber'] = $___classNumber;
								$dataAry[$counter]['studentName'] = $___studentName;
								$dataAry[$counter]['dateInput'] = $____dateInput;
								$dataAry[$counter]['score'] = $____score;
								$dataAry[$counter]['comment'] = $____comment;
								$counter++;
							}
							else if ($viewType == 'statistics') {
								$___subjectScoreOfStudent += $____score;
								
								if ($____comment != '') {
									$___numOfCommentOfStudent++;
								}
							}
						}
						
						if ($viewType == 'statistics') {
							$dataAry[$counter]['subjectName'] = $_subjectName;
							$dataAry[$counter]['subjectGroupName'] = $__subjectGroupName;
							$dataAry[$counter]['className'] = $___className;
							$dataAry[$counter]['classNumber'] = $___classNumber;
							$dataAry[$counter]['studentName'] = $___studentName;
							$dataAry[$counter]['totalScore'] = $___subjectScoreOfStudent;
							$dataAry[$counter]['numOfComment'] = $___numOfCommentOfStudent;
							$counter++;
						}
					}
				}
			}
			
			return $dataAry;
		}
		
		function getSubjectGroupTeacherByClass($startDate, $endDate,$classId){
		    global $PATH_WRT_ROOT;
		    include_once($PATH_WRT_ROOT.'includes/libuser.php');
		    include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		    include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
		    $fcm = new form_class_manage();
		    $scm = new subject_class_mapping();
		    $lsubject = new subject();
		    
		    // get terms
		    $termIdAry = Get_Array_By_Key($fcm->Get_Term_By_Date_Range($startDate, $endDate), 'YearTermID');
		    
		    // get subject groups
		    $subjectGroupAry = $lsubject->Get_Subject_Group_List($termIdAry, '', '', '', $returnAsso=0, $classId, '', $InYearClassOnly=true);
		    $subjectGroupAssoAry = BuildMultiKeyAssoc($subjectGroupAry, 'RecordID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
		    $subjectGroupIdAry = Get_Array_By_Key($subjectGroupAry, 'SubjectGroupID');
		    
		    $cols = "iu.EnglishName, iu.ChineseName, iaps.StudentID, iaps.TeacherID, iaps.Score, iaps.DateModified";
		    
		    $conds = "WHERE stct.SubjectGroupID IN ('" . implode("','", $subjectGroupIdAry) . "') ";
		    $conds .= "AND (iaps.DateModified >= '$startDate' AND iaps.DateModified <= '$endDate') ";
		    $table_stct = "SUBJECT_TERM_CLASS_TEACHER";
		    $table_iu = 'INTRANET_USER';
		    $table_iaps = 'INTRANET_APP_PERFORMANCE_STUDENT';
		    $table_iapss = "SELECT StudentID, TeacherID, MAX(DateModified) as DateModified
                       FROM INTRANET_APP_PERFORMANCE_STUDENT
                       GROUP BY StudentID, TeacherID";
		    
		    $grouping = "GROUP BY iaps.StudentID, iaps.TeacherID";
		    
		    $sql = "SELECT $cols
		            FROM $table_stct stct
                    INNER JOIN $table_iaps iaps
                    ON (iaps.TeacherID = stct.UserID)
                    INNER JOIN ($table_iapss) iapss
                    ON (iapss.StudentID = iaps.StudentID AND iapss.TeacherID = iaps.TeacherID AND iapss.DateModified = iaps.DateModified)
                    INNER JOIN $table_iu iu
                    ON (iu.UserID = iaps.TeacherID)
                    $conds
		            $grouping";
		    $result = $this->returnArray($sql);
		    $finalResult = array();
		    foreach($result as $rs){
		        $finalResult[$rs['TeacherID']]['Score'][$rs['StudentID']] = $rs['Score'];
		        $finalResult[$rs['TeacherID']]['EnglishName'] = $rs['EnglishName'];
		        $finalResult[$rs['TeacherID']]['ChineseName'] = $rs['ChineseName'];
		    }
		    
		    return $finalResult;
		}
	}
}
?>