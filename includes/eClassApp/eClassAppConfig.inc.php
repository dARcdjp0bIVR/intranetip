<?php
#####################################################################################################
###################### !!!!!!!!!!!!!!!!!!!!!! Important !!!!!!!!!!!!!!!!!!!!!! ######################
## Remember to set
## $eclassAppConfig['pushMessage']['apiPath'] = 'http://eclassapps2.eclass.com.hk/webserviceapi/index.php';
## $eclassAppConfig['groupMessage']['apiPath'] = 'http://eclassapps4.eclass.com.hk/webserviceapi/index.php';
## if upload this file to 149
###################### !!!!!!!!!!!!!!!!!!!!!! Important !!!!!!!!!!!!!!!!!!!!!! ######################
#####################################################################################################
// Editing by  
$eclassAppConfig = array();

$eclassAppConfig['eAdminPath'] = 'home/eAdmin/GeneralMgmt/eClassApp/';
if(function_exists('isKIS')){
	if (isKIS()) {
		$eclassAppConfig['filePath'] = $PATH_WRT_ROOT.'../'.$intranet_db.'data/eClassApp/';
	}
	else {
		$eclassAppConfig['filePath'] = $PATH_WRT_ROOT.'../intranetdata/eClassApp/';
	}
}else{
	$eclassAppConfig['filePath'] = $PATH_WRT_ROOT.'../intranetdata/eClassApp/';
}
$eclassAppConfig['urlFilePath'] = $intranet_root.'/file/app/';
$eclassAppConfig['urlBrowsePath'] = 'file/app/';
$eclassAppConfig['bannerPath'] = 'file/app/banner.jpg';
$eclassAppConfig['maxDevicePerUser'] = 10;
$eclassAppConfig['aesKey'] = 'AESKEY';
$eclassAppConfig['aesKey_reprintCard'] = 'rekf@i4GerK';
$eclassAppConfig['urlSalt'] = '1dfg5_^%&3g3f24s';
$eclassAppConfig['ssoSalt'] = 'Yt453jfS'.$config_school_code;
$eclassAppConfig['delimiter'] = 'S45deli'.$config_school_code.'TymterS';
$eclassAppConfig['separator'] = 'ghF4d'.$config_school_code.'4tFe5';
$eclassAppConfig['refreshLicenseInterval'] = 3600;	// in second

$eclassAppConfig['appType']['Parent'] = 'P';
$eclassAppConfig['appType']['Teacher'] = 'T';
$eclassAppConfig['appType']['Student'] = 'S';
$eclassAppConfig['appType']['Common'] = 'C';

$eclassAppConfig['appLicenseModuleName']['P'] = 'ParentApp';
$eclassAppConfig['appLicenseModuleName']['T'] = 'TeacherApp';

$eclassAppConfig['appApiProjectName']['P'] = 'eClass App';
$eclassAppConfig['appApiProjectName']['T'] = 'eClass App (Teacher)';
$eclassAppConfig['appApiProjectName']['S'] = 'eClass App (Student)';

$eclassAppConfig['moduleCode']['pushMessage'] = 'pushMessage';
$eclassAppConfig['moduleCode']['applyLeave'] = 'applyLeave';
$eclassAppConfig['moduleCode']['eAttendance'] = 'eAtt';
$eclassAppConfig['moduleCode']['eHomework'] = 'eHW';
$eclassAppConfig['moduleCode']['eNotice'] = 'eNotice';
$eclassAppConfig['moduleCode']['ePayment'] = 'ePayment';
$eclassAppConfig['moduleCode']['ePaymentTng'] = 'ePaymentTng';
$eclassAppConfig['moduleCode']['ePaymentAlipay'] = 'ePaymentAlipay';
$eclassAppConfig['moduleCode']['ePaymentTapAndGo'] = 'ePaymentTapAndGo';
$eclassAppConfig['moduleCode']['ePaymentFps'] = 'ePaymentFps';
$eclassAppConfig['moduleCode']['ePaymentVisaMaster'] = 'ePaymentVisaMaster';
$eclassAppConfig['moduleCode']['ePaymentWeChat'] = 'ePaymentWeChat';
$eclassAppConfig['moduleCode']['ePaymentAlipayCN'] = 'ePaymentAlipayCN';
$eclassAppConfig['moduleCode']['ePaymentMultiple'] = 'ePaymentMultiple';
$eclassAppConfig['moduleCode']['ePaymentMultipleTopUp'] = 'ePaymentMultipleTopUp';
$eclassAppConfig['moduleCode']['schoolCalendar'] = 'schoolCal';
$eclassAppConfig['moduleCode']['schoolNews'] = 'schoolNews';
$eclassAppConfig['moduleCode']['iMail'] = 'iMail';
$eclassAppConfig['moduleCode']['takeAttendance'] = 'takeAttendance';
$eclassAppConfig['moduleCode']['eCircular'] = 'eCircular';
$eclassAppConfig['moduleCode']['crossBoundarySchoolCoaches'] = 'crossBoundarySchoolCoaches';
$eclassAppConfig['moduleCode']['eNoticeS'] = 'eNoticeS';
$eclassAppConfig['moduleCode']['teacherStatus'] = 'teacherStatus';
$eclassAppConfig['moduleCode']['eEnrolment'] = 'eEnrolment';
$eclassAppConfig['moduleCode']['studentStatus'] = 'studentStatus';
$eclassAppConfig['moduleCode']['groupMessage'] = 'groupMessage';
$eclassAppConfig['moduleCode']['staffAttendance'] = 'staffAtt';
$eclassAppConfig['moduleCode']['studentPerformance'] = 'studentPerformance';
$eclassAppConfig['moduleCode']['schoolInfo'] = 'schoolInfo';
$eclassAppConfig['moduleCode']['flippedchannel'] = 'flippedchannel';
$eclassAppConfig['moduleCode']['digitalChannels'] = 'digitalChannels';
$eclassAppConfig['moduleCode']['medicalCaring'] = 'medicalCaring';
$eclassAppConfig['moduleCode']['medicalCaringBowel'] = 'medicalCaringBowel';
$eclassAppConfig['moduleCode']['medicalCaringBowel_v2'] = 'medicalCaringBowel_v2';
$eclassAppConfig['moduleCode']['medicalCaringBowelRecordDetails_v2'] = 'medicalCaringBowelRecordDetails_v2';
$eclassAppConfig['moduleCode']['medicalCaringLog'] = 'medicalCaringLog';
$eclassAppConfig['moduleCode']['medicalCaringLog_v2'] = 'medicalCaringLog_v2';
$eclassAppConfig['moduleCode']['medicalCaringLogRecordDetails_v2'] = 'medicalCaringLogRecordDetails_v2';
$eclassAppConfig['moduleCode']['medicalCaringSleep'] = 'medicalCaringSleep';
$eclassAppConfig['moduleCode']['medicalCaringSleep_v2'] = 'medicalCaringSleep_v2';
$eclassAppConfig['moduleCode']['medicalCaringSleepRecordDetails_v2'] = 'medicalCaringSleepRecordDetails_v2';
$eclassAppConfig['moduleCode']['iCalendar'] = 'iCalendar';
$eclassAppConfig['moduleCode']['weeklyDiary'] = 'weeklyDiary';
$eclassAppConfig['moduleCode']['eLibPlus'] = 'eLibPlus';
$eclassAppConfig['moduleCode']['timetable'] = 'timetable';
$eclassAppConfig['moduleCode']['hkuFlu'] = 'hkuFlu';
$eclassAppConfig['moduleCode']['classroom'] = 'classroom';
$eclassAppConfig['moduleCode']['eBooking'] = 'eBooking';
$eclassAppConfig['moduleCode']['eDiscipline'] = 'eDiscipline';
$eclassAppConfig['moduleCode']['hostelTakeAttendance'] = 'HostelTakeAtt';
$eclassAppConfig['moduleCode']['iPortfolio'] = 'iPf';
$eclassAppConfig['moduleCode']['eSchoolBus'] = 'eSchoolBus';
$eclassAppConfig['moduleCode']['eEnrolmentUserView'] = 'eEnrolmentUserView';
$eclassAppConfig['moduleCode']['reprintCard'] = 'reprintCard';
$eclassAppConfig['moduleCode']['eSchoolBusTeacher'] = 'eSchoolBusTeacher';
$eclassAppConfig['moduleCode']['eEnrolmentStudentView'] = 'eEnrolmentStudentView';
$eclassAppConfig['moduleCode']['SchoolInfoPDF'] = 'SchoolInfoPDF';
$eclassAppConfig['moduleCode']['ePOS'] = 'ePOS';
//$eclassAppConfig['moduleCode']['ePosStudent'] = 'ePosStudent';
$eclassAppConfig['moduleCode']['eInventory'] = 'eInventory';
$eclassAppConfig['moduleCode']['eInventoryStocktake'] = 'eInventoryStocktake';
$eclassAppConfig['moduleCode']['eLearningTimetable'] = 'eLearningTimetable';
$eclassAppConfig['moduleCode']['bodyTemperature'] = 'bodyTemperature';

$eclassAppConfig['pushMessageSendRight']['classTeacher'] = 'pushMessageSendRight_classTeacher';
$eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC'] = 'pushMessageSendRight_clubAndActivityPIC';

$eclassAppConfig['appImage']['status']['deleted'] = 0;
$eclassAppConfig['appImage']['status']['upload'] = 1;
$eclassAppConfig['appImage']['status']['defaultImage'] = 2;

$eclassAppConfig['appIamge']['defaultImage']['defaultSelection'] = 2;

$eclassAppConfig['imageType']['schoolBadge'] = 'badge';
$eclassAppConfig['imageType']['backgroundImage'] = 'background';
$eclassAppConfig['imageType']['accountPageBanner'] = 'banner';

$eclassAppConfig['suggestedResolution']['schoolBadge']['width'] = 300;
$eclassAppConfig['suggestedResolution']['schoolBadge']['height'] = 300;
$eclassAppConfig['suggestedResolution']['backgroundImage']['width'] = 1080;
$eclassAppConfig['suggestedResolution']['backgroundImage']['height'] = 246;
$eclassAppConfig['suggestedResolution']['accountPageBanner']['width'] = 1080;
$eclassAppConfig['suggestedResolution']['accountPageBanner']['height'] = 82;

$eclassAppConfig['deviceOS']['Android'] = 'Android';
$eclassAppConfig['deviceOS']['Android_CN'] = 'Android_CN';
$eclassAppConfig['deviceOS']['iOS'] = 'iOS';
$eclassAppConfig['deviceOS']['Win'] = 'Windows';
$eclassAppConfig['deviceOS']['Huawei'] = 'Huawei';

$eclassAppConfig['apnsType']['sandbox'] = 'sandbox';
$eclassAppConfig['apnsType']['production'] = 'production';

$eclassAppConfig['pushMessageSendMode']['now'] = 'now';
$eclassAppConfig['pushMessageSendMode']['scheduled'] = 'scheduled';

if ($sys_custom['eClassApp']['env'] == "dev") {
	$eclassAppConfig['pushMessage']['apiPath'] = 'https://eclassapps.eclass.com.hk/webserviceapi/index.php';
	$eclassAppConfig['groupMessage']['apiPath'] = 'https://eclassapps.eclass.com.hk/webserviceapi/index.php';
	// 2015/08/26 Roy: group message WS path on server side
	$eclassAppConfig['groupMessage']['wsPath'] = 'ws://eclassgm5-dev.eclass.com.hk:8080';
	//$eclassAppConfig['groupMessage']['wsPath'] = 'http://eclassgm5-dev.eclass.com.hk/ws';

	//$eclassAppConfig['paymentGateway']['apiPath'] = 'https://tng-dev.broadlearning.com/webserviceapi/index.php';
  	//$eclassAppConfig['paymentGateway']['apiPath'] = 'https://blp-dev.broadlearning.com/webserviceapi/index.php';
 	$eclassAppConfig['paymentGateway']['apiPath'] = 'https://blpaygw.broadlearning.com/webserviceapi/index.php';
	$eclassAppConfig['reprintCard']['apiPath'] = 'http://192.168.0.146:31010/webserviceapi/index.php';
} else {
	if(get_client_region() == "zh_TW"){
		$eclassAppConfig['pushMessage']['apiPath'] = 'https://eclassapps3.eclass.com.hk/webserviceapi/index.php';
		//$eclassAppConfig['pushMessage']['apiPath'] = 'http://eclassapps2.eclass.com.hk:3000/webserviceapi/index.php';
		//$eclassAppConfig['pushMessage']['apiPath'] = 'http://eclassapps3.eclass.com.hk/webserviceapi/index.php';
		//$eclassAppConfig['pushMessage']['apiPath'] = 'http://eclassapps4.eclass.com.hk/webserviceapi/index.php';

		$eclassAppConfig['groupMessage']['apiPath'] = 'https://tweclassapps5.eclass.com.hk/webserviceapi/index.php';
		// 2015/08/26 Roy: group message WS path on server side
		$eclassAppConfig['groupMessage']['wsPath'] = 'ws://tweclassgm5.eclass.com.hk:8080';

		$eclassAppConfig['paymentGateway']['apiPath'] = 'https://blpaygw.broadlearning.com/webserviceapi/index.php';
		$eclassAppConfig['reprintCard']['apiPath'] = 'http://store.eclass.com.hk/webserviceapi/index.php';
    }else{
        $eclassAppConfig['pushMessage']['apiPath'] = 'https://eclassapps3.eclass.com.hk/webserviceapi/index.php';
        //$eclassAppConfig['pushMessage']['apiPath'] = 'http://eclassapps2.eclass.com.hk:3000/webserviceapi/index.php';
        //$eclassAppConfig['pushMessage']['apiPath'] = 'http://eclassapps3.eclass.com.hk/webserviceapi/index.php';
        //$eclassAppConfig['pushMessage']['apiPath'] = 'http://eclassapps4.eclass.com.hk/webserviceapi/index.php';

        $eclassAppConfig['groupMessage']['apiPath'] = 'https://eclassapps5.eclass.com.hk/webserviceapi/index.php';
        // 2015/08/26 Roy: group message WS path on server side
        $eclassAppConfig['groupMessage']['wsPath'] = 'ws://eclassgm5.eclass.com.hk:8080';

        if($sys_custom['ePayment']['MultiPaymentGateway'] || $sys_custom['ePayment']['Use_blpgw2_PaymentGateway']) {
			$eclassAppConfig['paymentGateway']['apiPath'] = 'https://blpgw2.broadlearning.com/webserviceapi/index.php';
			if($sys_custom['ePayment']['Use_blpgw3_PaymentGateway']) {
				$eclassAppConfig['paymentGateway']['apiPath'] = 'https://blpgw2-testing.broadlearning.com/webserviceapi/index.php';
			}
		} else {
			$eclassAppConfig['paymentGateway']['apiPath'] = 'https://blpaygw.broadlearning.com/webserviceapi/index.php';
		}
        $eclassAppConfig['reprintCard']['apiPath'] = 'http://store.eclass.com.hk/webserviceapi/index.php';
	}
}

$eclassAppConfig['pushMessage']['apiVersion']['overall'] = 2;
$eclassAppConfig['pushMessage']['serviceProvider']['Android'] = 'gcm';
$eclassAppConfig['pushMessage']['apiVersion']['gcm'] = 2;
$eclassAppConfig['pushMessage']['maxDevicePerMessage']['gcm'] = 900;
$eclassAppConfig['pushMessage']['maxMessagePayloadByte']['gcm'] = 4000;
$eclassAppConfig['pushMessage']['serviceProvider']['iOS'] = 'apns';
$eclassAppConfig['pushMessage']['apiVersion']['apns'] = 1;
//$eclassAppConfig['pushMessage']['maxMessagePayloadByte']['apns'] = 220;
$eclassAppConfig['pushMessage']['maxMessagePayloadByte']['apns'] = 20000;
$eclassAppConfig['pushMessage']['serviceProvider']['Windows'] = 'mpns';
$eclassAppConfig['pushMessage']['apiVersion']['mpns'] = 1;
$eclassAppConfig['pushMessage']['maxDevicePerMessage']['mpns'] = 900;
$eclassAppConfig['pushMessage']['maxMessagePayloadByte']['mpns'] = 4000;
$eclassAppConfig['pushMessage']['serviceProvider']['Android_CN'] = 'getui';
$eclassAppConfig['pushMessage']['apiVersion']['getui'] = 1;
$eclassAppConfig['pushMessage']['maxDevicePerMessage']['getui'] = 900;
$eclassAppConfig['pushMessage']['maxMessagePayloadByte']['getui'] = 4000;
$eclassAppConfig['pushMessage']['serviceProvider']['Huawei'] = 'huawei';
$eclassAppConfig['pushMessage']['apiVersion']['huawei'] = 1;
$eclassAppConfig['pushMessage']['maxDevicePerMessage']['huawei'] = 900;
$eclassAppConfig['pushMessage']['maxMessagePayloadByte']['huawei'] = 4000;

$eclassAppConfig['pushMessage']['fromModule']['eNotice'] = 'eNotice';
$eclassAppConfig['pushMessage']['fromModule']['changePwd'] = 'changePwd';

$eclassAppConfig['pushMessage']['sendBulkPushMessageInBatchesRecipientPerBatch'] = 60;
$eclassAppConfig['pushMessage']['sendBulkPushMessageInBatchesFrequency'] = 10;	// in seconds

$eclassAppConfig['schoolBus']['webViewUrlPrefix'] = 'http://sbus.eclass.hk/home/cust/bus/?t=API.parentView.viewStudentStatus';

$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['active'] = 1;
$eclassAppConfig['APP_USER_DEVICE']['RecordStatus']['inactive'] = 0;
$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedIn'] = "in";
$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['loggedOut'] = "out";
$eclassAppConfig['APP_USER_DEVICE']['LoginStatus']['deleted'] = "del";
$eclassAppConfig['APP_USER_DEVICE']['DisableReason']['deleteAccount'] = 'deleteAccount';
$eclassAppConfig['APP_USER_DEVICE']['DisableReason']['logoutAccount'] = 'logoutAccount';
$eclassAppConfig['APP_USER_DEVICE']['DisableReason']['tooMuchDevice'] = 'tooMuchDevice';
$eclassAppConfig['APP_USER_DEVICE']['DisableReason']['canonical_ids'] = 'canonical_ids';
$eclassAppConfig['APP_USER_DEVICE']['DisableReason']['notRegisteredId'] = 'notRegisteredId';
$eclassAppConfig['APP_USER_DEVICE']['DisableReason']['wrongSenderId'] = 'wrongSenderId';
$eclassAppConfig['APP_USER_DEVICE']['DisableReason']['didNotLoginForLong'] = 'didNotLoginForLong';

$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'] = 0;
$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'] = 1;
$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead'] = 2;
$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend'] = 3;
$eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice'] = 4;

$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['centralServerUrl'] = 'centralServerUrl';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['appInLicense_P'] = 'appInLicense_P';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['appInLicense_T'] = 'appInLicense_T';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['appInLicense_S'] = 'appInLicense_S';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherStatus'] = 'intranetAPPTeacherStatus_User';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherList_Group'] = 'intranetAPPTeacherList_Group';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable'] = 'attendancePushMessageEnable';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable'] = 'attendanceStatusEnable';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable'] = 'attendanceTimeEnable';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable'] = 'attendanceLeaveSectionEnable';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable'] = 'eAttendanceEnable';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceTimeEnable'] = 'eAttendanceTimeEnable';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable'] = 'eAttendanceStatusStatisticsEnable';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord'] = 'nonAdminUserCanDeleteOwnPushMessageRecord';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival'] = 'enableTapCardPushMessageType_Arrival';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave'] = 'enableTapCardPushMessageType_Leave';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay'] = 'enableTapCardPushMessageOnNonSchoolDay';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival'] = 'enableNonSchoolDayTapCardPushMessageType_Arrival';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave'] = 'enableNonSchoolDayTapCardPushMessageType_Leave';

$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewAllStudentList'] = 'intranetAPPStudentList_ClassTeacherCanViewAllStudent';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContact'] = 'intranetAPPStudentList_ClassTeacherCanViewContact';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewAllStudentList'] = 'intranetAPPStudentList_NonClassTeacherCanViewAllStudent';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewStudentContact'] = 'intranetAPPStudentList_NonClassTeacherCanViewContact';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudent'] = 'intranetAPPStudentList_ClassTeacherCanSendPushMessageToClassStudent';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudent'] = 'intranetAPPStudentList_ClassTeacherCanSendPushMessageToAllStudent';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudent'] = 'intranetAPPStudentList_NonClassTeacherCanSendPushMessageToAllStudent';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContactClass'] = 'intranetAPPStudentList_ClassTeacherCanViewContactClass';

$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudentApp'] = 'intranetAPPStudentList_ClassTeacherCanSendPushMessageToClassStudentApp';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudentApp'] = 'intranetAPPStudentList_ClassTeacherCanSendPushMessageToAllStudentApp';
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudentApp'] = 'intranetAPPStudentList_NonClassTeacherCanSendPushMessageToAllStudentApp';

// 20150723 Roy: teacher app can receive push message while logged out
$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut'] = 'teacherApp_canReceivePushMessageWhileLoggedOut';

$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableStudentApp'] = 'enableStudentApp';

$eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active'] = 1;
$eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['inactive'] = 2;
$eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['deleted'] = 0;

$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['active'] = 1;
$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['deleted'] = 2;

$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['Normal'] = 0;
$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['DisableParentToParent'] = 1;

$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['Normal'] = 0;
$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['UserChatroom'] = 1;

$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['member'] = 'M';
$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['admin'] = 'A';

$eclassAppConfig['INTRANET_APP_BLACKLIST_USER']['RecordStatus']['active'] = 1;
$eclassAppConfig['INTRANET_APP_BLACKLIST_USER']['RecordStatus']['deleted'] = 0;

//$eclassAppConfig['intranetAPPTeacherStatus']['User'] = 'intranetAPPTeacherStatus_User';

$eclassAppConfig['groupMessage']['maxNumOfGroup'] = 80;
$eclassAppConfig['groupMessage']['maxNumOfGroupMember'] = 100;

$eclassAppConfig['errorCode']['general']['requestLogFailed'] = 'G101';
$eclassAppConfig['errorCode']['general']['serverBusy'] = 'G102';
$eclassAppConfig['errorCode']['general']['noRequestMethod'] = 'G103';

$eclassAppConfig['errorCode']['pushMessage']['noUserLogin'] = 'PM101';
$eclassAppConfig['errorCode']['pushMessage']['noJsonData'] = 'PM102';
$eclassAppConfig['errorCode']['pushMessage']['cannotConnentToSchool'] = 'PM103';
$eclassAppConfig['errorCode']['pushMessage']['failedToSaveDbInSchoolSite'] = 'PM104';
$eclassAppConfig['errorCode']['pushMessage']['invalidDeviceId'] = 'PM105';
$eclassAppConfig['errorCode']['pushMessage']['userLoginNotFound'] = 'PM106';
$eclassAppConfig['errorCode']['pushMessage']['schoolCodeNotFound'] = 'PM107';
$eclassAppConfig['errorCode']['pushMessage']['serviceProviderNotFound'] = 'PM108';
$eclassAppConfig['errorCode']['pushMessage']['cannotConnectToServiceProvider'] = 'PM109';
$eclassAppConfig['errorCode']['pushMessage']['messageTooLong'] = 'PM110';
$eclassAppConfig['errorCode']['pushMessage']['cannotConnectToCentralServer'] = 'PM111';

$eclassAppConfig['eClassApp']['home']['attendanceView']['displayStyle']['hide'] = 'hide';
$eclassAppConfig['eClassApp']['home']['attendanceView']['displayStyle']['amOnly'] = 'am';
$eclassAppConfig['eClassApp']['home']['attendanceView']['displayStyle']['full'] = 'full';
?>
