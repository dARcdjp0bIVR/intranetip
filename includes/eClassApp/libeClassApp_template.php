<?php
// editing by 
 
/*******
 * 2019-07-24 Ray: add tag payment start date, end date, add CanDelete in push message template
 * 2017-07-17 Carlos: Modified GetMessageTagAry() - added $this->excludedTags to hide unwanted tag items, $this->adhocTags to add extra tag items.
 * 2017-06-21 Anna: Modified GetMessageTagAry()  - add ePayment moudle
 * 2016-09-15 Villa: Modified GetMessageTagAry()- add absent late early leave tag
 *  
 * 2016-02-01 Kenneth: added GetMessageTagAry()/ modified MessageTagAry(), ajaxLoadTemplate()
 * 						Modified access right, add section
 * 						add paras $getItselfAndEmptySection into getMessageTemplateSQL(), getMessageTemplate() and getMessageTemplateSelectionBox()
 * 2015-10-08 Omas [ip.2.5.6.10.1]: eNotice notice summary report MessageTagAry reverse the tag 
 * 2015-07-24 Omas [ip.2.5.6.7.1]:	add new module Message Center, modified getMessageTagSelectionBox() if no tagAry return ''
 * 2014-12-10 Omas [ip.2.5.5.12.1]: Create this file
 *  
 *******/
if (!defined("LIBECLASSAPP_TEMPLATE_DEFINED")) {
	define("LIBECLASSAPP_TEMPLATE_DEFINED", true);
	
	class libeClassApp_template extends libdb {
		var $module;
		var $section;
		
		var $excludedTags = array(); // array of tag values to be excluded, one dimensional array
		var $adhocTags = array(); // array of array(value,display_text), two dimensional array
		
		function libeClassApp_template() {
			$this->libdb();
		}
		
		function setModule($text) {
			$this->module = $text;
		}
		function getModule() {
			return $this->module;
		}
		function setSection($text) {
			$this->section = $text;
		}
		function getSection() {
			return $this->section;
		}
		
		function setExcludedTags($tagsAry){
			$this->excludedTags = $tagsAry;
		}
		
		function setAdhocTags($tagsAry){
			$this->adhocTags = $tagsAry;
		}
		
		function canAccess($module, $section) {
			
			$canAccess = false;
			if ($module=='eNotice') {
				include_once("../../../../includes/libnotice.php");
				$lib = new libnotice;
				$sectionAry = array('NoticeSummaryReport');
				
				if($lib->hasFullRight() /*&& in_array($section,$sectionAry)*/ ){
					$canAccess = true;	
				}
			}
			else if ($module=='StudentAttendance') {

				$sectionAry = array('NotSubmitDocument','AbsentLateEarlyLeave','','all');
				
				if( $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent'] /*&& in_array($section,$sectionAry)*/){
					$canAccess = true;	
				}
			}
			else if($module == 'MessageCenter' ){
				if( $_SESSION["SSV_USER_ACCESS"]['eAdmin-ParentAppNotify'] 
					|| $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"] 
					|| $_SESSION["SSV_USER_ACCESS"]['eAdmin-TeacherAppNotify'] ){
					$canAccess = true;	
				}
			}else if($module == 'ePayment'){
				if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || $_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
					$canAccess = true;
				}
			}
			
			if (!$canAccess) {
			No_Access_Right_Pop_Up();
			die();
			}
			
			return true;
		}
		
		function GetMessageTagAry($module,$section=''){
			global $Lang, $i_SMS_Personalized_Tag;
			
			# Default Tag from SMS  $ldbsms->replace_content
			$TagAry		= array();
			$TagAry[] 	= array("user_name_eng",		$Lang['General']['EnglishName']);
			$TagAry[]	= array("user_name_chi",		$Lang['General']['ChineseName']);
			$TagAry[]	= array("user_class_name",		$Lang['General']['Class']);
			$TagAry[]	= array("user_class_number",	$Lang['General']['ClassNumber']);
			$TagAry[]	= array("user_login",			$Lang['AccountMgmt']['LoginID']);
			
			# str_replace in your own module 
			if($module == 'eNotice'){
				if($section == 'NoticeSummaryReport' ||$section=='all'){
					$TagAry[]	= array("Notice_Num",				$Lang['eNotice']['NotSignedNoticeNum'],'NoticeSummaryReport');
					$TagAry[]	= array("Num_Of_Unsigned_Notice",	$Lang['eNotice']['NumberOfNotSigned'],'NoticeSummaryReport');
				}
			}
			
			if($module == 'StudentAttendance' ){
			
				if($section == 'NotSubmitDocument'||$section=='all'){
					$TagAry[]	= array("Num_Of_Not_Submitted",		$Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedProveDocNum'],'NotSubmitDocument');
					$TagAry[]	= array("Dates",		$Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedProveDocDates'],'NotSubmitDocument');
				}if($section == 'AbsentLateEarlyLeave'||$section=='all'){
					$TagAry[]	= array("Date_Of_Absent_Late_Early_Leave",$Lang['StudentAttendance']['AbsentProveReport']['DateOfAbsentLateEarlyLeave'] ,'AbsentLateEarlyLeave');
				}
			}
			
			if($module == 'MessageCenter' ){
				$TagAry = array();
			}
			if($module == 'ePayment' ){
				$TagAry[]	= array("Payment_Amount",$Lang['ePayment']['Amount']);
				//$TagAry[]	= array("Current_Balance",	$Lang['ePayment']['CurrentAccountBalance'] );
				$TagAry[]	= array("payment_balance",	$i_SMS_Personalized_Tag['payment_balance'] );
				$TagAry[]	= array("Payment_Item",$Lang['ePayment']['PaymentItem']);
				//$TagAry[]	= array("Outstanding_Amount",$Lang['ePayment']['OutstandingAmount']);
				$TagAry[]	= array("payment_outstanding_amount",$i_SMS_Personalized_Tag['payment_outstanding_amount']);
				$TagAry[]	= array("Inadequate_Amount",$Lang['ePayment']['InadequateAmount']);
				$TagAry[]	= array("Payment_StartDate",$Lang['ePayment']['PaymentStartDate']);
				$TagAry[]	= array("Payment_EndDate",$Lang['ePayment']['PaymentEndDate']);
			}
			
			if(count($this->excludedTags)>0)
			{
				for($i=count($TagAry)-1;$i>0;$i--){
					if(in_array($TagAry[$i][0],$this->excludedTags)){
						unset($TagAry[$i]);
					}
				}
				$TagAry = array_values($TagAry);
			}
			if(count($this->adhocTags)>0){
				foreach($this->adhocTags as $ary_value)
				{
					$TagAry[] = $ary_value;
				}
			}
			
			return $TagAry;
		}
		
		function MessageTagAry(){
			##############################################
			##	2016-02-01	Kenneth
			##		- TagArray move to GetMessageTagAry(), 
			##		
			##############################################
			/*			
			# Default Tag from SMS  $ldbsms->replace_content
			$TagAry		= array();
			$TagAry[] 	= array("user_name_eng",		$Lang['General']['EnglishName']);
			$TagAry[]	= array("user_name_chi",		$Lang['General']['ChineseName']);
			$TagAry[]	= array("user_class_name",		$Lang['General']['Class']);
			$TagAry[]	= array("user_class_number",	$Lang['General']['ClassNumber']);
			$TagAry[]	= array("user_login",			$Lang['AccountMgmt']['LoginID']);
			
			# str_replace in your own module 
			if($this->getModule() == 'eNotice'){
				if($this->getSection() == 'NoticeSummaryReport' ){
					$TagAry[]	= array("Notice_Num",				$Lang['eNotice']['NotSignedNoticeNum']);
					$TagAry[]	= array("Num_Of_Unsigned_Notice",	$Lang['eNotice']['NumberOfNotSigned']);
				}
			}
			
			if($this->getModule() == 'StudentAttendance' ){
				if($this->getSection() == 'NotSubmitDocument'){
					$TagAry[]	= array("Num_Of_Not_Submitted",		$Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedProveDocNum']);
					$TagAry[]	= array("Dates",		$Lang['StudentAttendance']['AbsentProveReport']['UnsubmmitedProveDocDates']);
				}else if($this->getSection() == 'AbsentLateEarlyLeave'){
					
				}else{
					
				}
			}
			
			if($this->getModule() == 'MessageCenter' ){
				$TagAry = array();
			}
			*/
			//debug_pr($this->getSection());
			return $this->GetMessageTagAry($this->getModule(),$this->getSection());
		}
		
		function getMessageRemarks(){
		    global $Lang;
		    
		    if($this->getModule() == 'eNotice'){
				if($this->getSection() == 'NoticeSummaryReport' ){
					$remarkStr = $Lang['eNotice']['NoticeSummaryReport']['PushMessageremarks'];
				}
		    }
			if($this->getModule() == 'StudentAttendance' ){
				if($this->getSection() == 'NotSubmitDocument'){
					$remarkStr = $Lang['StudentAttendance']['AbsentProveReport']['PushMessageremarks'];
				}
			}
			return $remarkStr;	
		}
		
		function getMessageTagSelectionBox(){
			global $linterface,$Lang;
			
			$TagAry = $this->MessageTagAry();
			
			if(!empty($TagAry)){
				$MessageTagSelect .= '<select id="messageTag">';
					foreach ($TagAry as $Tag){
						$MessageTagSelect.= '<option value="'.$Tag[0].'">'.$Tag[1].'</option>';
					}
				$MessageTagSelect .= '</select>&nbsp;&nbsp;';
				$MessageTagSelect .= $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "AppMessageReminder.addAutoFillTag();");
				
				return $MessageTagSelect;
			}
			else{
				return '';
			}
		}
		
	    function getMessageTemplateSQL($getItselfAndEmptySection=false){
	   		if($this->getSection()==''||$this->getSection()=='all'){
	   				$selectionStatment ='';
	   		}
	   		else{
	   			if($getItselfAndEmptySection){
	   				$selectionStatment = " AND ( ModuleSection = '".$this->getSection()."' OR ModuleSection ='')";
	   			}else{
	   				$selectionStatment = " AND ModuleSection = '".$this->getSection()."'";
	   			}
	   		}
	   		//debug_pr($selectionStatment);
	    		$sql = "SELECT
				            CONCAT('<a href=\"template_new.php?Module=','".$this->getModule()."','&Section=','".$this->getSection()."','&TemplateID=',TemplateID,'\" >', Title ,'</a>') as TitleLink,
				            Content, 
				            DateModified,
				            IF(CanDelete=0, '', CONCAT('<input type=\"checkbox\" name=\"TemplateID[]\" onclick=\"unset_checkall(this', ',' ,'document.getElementById(\'form1\'));\" value=', TemplateID ,'>')),
							TemplateID,
							Title
        				FROM 
							INTRANET_APP_PUSH_MESSAGE_TEMPLATE
        				WHERE 
							RecordStatus = 1
							AND FromModule = '".$this->getModule()."'".$selectionStatment;
//         		debug_pr($sql);
        		return $sql;	
	    }
	    
	    function getMessageTemplate($orderSQL='',$getItselfAndEmptySection=false){
	    	//debug_pr($getItselfAndEmptySection);
	    	if($orderSQL == ''){
		    	$sql = $this->getMessageTemplateSQL($getItselfAndEmptySection);
	    	}
	    	else{
	    		$sql = $orderSQL;
	    	}
		    $resultAry = $this->returnResultSet($sql);
		    $resultAssoAry = BuildMultiKeyAssoc($resultAry , 'TemplateID', array('TemplateID','Title','Content'));
	    	
	    	return $resultAssoAry;
	    }
	    
	    function getMessageTemplateSelectionBox($getItselfAndEmptySection=false){
			# Sort template
			$sql = $this->getMessageTemplateSQL($getItselfAndEmptySection);
			
			$sql .= ' ORDER BY Title asc ';
			
			$templateAry = $this->getMessageTemplate($sql);
			
			$templateAssoAry = BuildMultiKeyAssoc($templateAry,'TemplateID', array('Title'),1);
			$id = "id=\"templateSelect\" onChange=\"AppMessageReminder.loadMessageTemplate();\" ";
			$templateSelect = getSelectByAssoArray($templateAssoAry,$id);
			
			return $templateSelect;
		}
	    
	    function insertUpdatMessageTemplate($parAction, $parUserID, $parTitle='', $parContent='', $parTemplateID='', $parSection=''){
			$SafeParTitle = $this->Get_Safe_Sql_Query($parTitle);
			$SafeParContent = $this->Get_Safe_Sql_Query($parContent);
			if ($parAction == 'INSERT'){
				$sql = "INSERT INTO 
							INTRANET_APP_PUSH_MESSAGE_TEMPLATE 
							(Title, Content, 
							FromModule, ModuleSection, 
							DateInput, InputBy, 
							DateModified, ModifiedBy) 
						Values
							('".$SafeParTitle."', '".$SafeParContent."', 
								'".$this->getModule()."', '".$this->getSection()."',
								Now(), '".$parUserID."', 
								Now(),'".$parUserID."')
						";
				$result = $this->db_db_query($sql);
			}
			else if ($parAction == 'UPDATE'){
				$sql = "UPDATE 
							INTRANET_APP_PUSH_MESSAGE_TEMPLATE 
						SET 
							Title = '".$SafeParTitle."' ,
 							Content = '".$SafeParContent."' , 
							DateModified = Now() ,
							ModifiedBy = '".$parUserID."' ,
							ModuleSection = '".$parSection."'		
						WHERE 
							TemplateID = '".$parTemplateID."' 
						";
				$result = $this->db_db_query($sql);
			}
			else if ($parAction == 'DELETE'){
				$sql = "UPDATE 
							INTRANET_APP_PUSH_MESSAGE_TEMPLATE 
						SET 
							RecordStatus = '0' , 
							DateModified = Now() ,
							ModifiedBy = '".$parUserID."' 
						WHERE 
							TemplateID IN ( '".$parTemplateID."' ) 
						";
				$result = $this->db_db_query($sql);
			}
			return $result;
	    }
	    
	    function ajaxLoadTemplate($TemplateIDStr){
	    	
			$TemplateAssoAry = $this->getMessageTemplate('',true);
	//		debug_pr($TemplateIDStr);
			$TitleValue = $TemplateAssoAry[$TemplateIDStr]['Title'];
			$ContentValue = $TemplateAssoAry[$TemplateIDStr]['Content'];

			$result = '';
			
			# Message Title
			$result .= $TitleValue;
			
			#########This is a delimiter(Start)###########
			$result .= '<!--eClassAppMessageTemplate-->';
			#########This is a delimiter( End )###########
			
			# Content
			$result .= $ContentValue;
			
			return $result;
	    }
 
	    function getPushMessageThickBox($hiddenFieldAry){
			
			global $linterface,$Lang;
			
			$thickboxContent = '';
			
			$thickBoxHeight = 500;
			$thickBoxWidth = 640;
			$thickboxContent .= $linterface->Get_Thickbox_Link($thickBoxHeight, $thickBoxWidth, "", $Lang['AppNotifyMessage']['button'], "", 'divPushMessage', $Content='', 'pushMessageButton');
			
			
			# thickbox layout
			$thickboxContent .= '<div id="divPushMessage" style="display:none">';
			# thickbox header
				$WarningMessageBoxContent = $this->getMessageRemarks();
				if(!empty($WarningMessageBoxContent)){
					$thickboxContent .= $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $WarningMessageBoxContent);
				}
				# thickbox form
				$thickboxContent .= '<form id="pushMessageForm" action="#">';
					
					$thickboxContent .= '<table class="form_table_v30">';
						# Message Template selection
						$thickboxContent .= '<tr class="push_message_layer">';
							$thickboxContent .= '<td valign="top" nowrap="nowrap" class="field_title">';
								$thickboxContent .= $Lang['SMS']['MessageTemplates'];
							$thickboxContent .= '</td>';
							$thickboxContent .= '<td class="tabletext" width="70%">';
								$thickboxContent .= $this->getMessageTemplateSelectionBox(true);
								$thickboxContent .= '<span id="AjaxStatus"></span>';
							$thickboxContent .= '</td>';
						$thickboxContent .= '</tr>';
			
						# Push message title
						$thickboxContent .= '<tr class="push_message_layer">';
							$thickboxContent .= '<td valign="top" nowrap="nowrap" class="field_title">';
								$thickboxContent .= $Lang['AppNotifyMessage']['Title'];
								$thickboxContent .= '&nbsp;<span class="tabletextrequire">*</span>';
							$thickboxContent .= '</td>';
							$thickboxContent .= '<td class="tabletext" width="70%">';
								$thickboxContent .= '<input type="text" class="messageInfo" id="PushMessageTitle" name="PushMessageTitle" value="" size="50" />';
							$thickboxContent .= '</td>';
						$thickboxContent .= '</tr>';
		
						# Push message content
						$thickboxContent .= '<tr class="push_message_layer">';
						   	$thickboxContent .= '<td valign="top" nowrap="nowrap" class="field_title">';
						    		$thickboxContent .= $Lang['eClassApp']['MessageContent'];
						    		$thickboxContent .= '&nbsp;<span class="tabletextrequire">*</span>';
							$thickboxContent .= '</td>';
							$thickboxContent .= '<td class="tabletext" width="70%">';
									$thickboxContent .= $rx = "<textarea  class=\"messageInfo\" id=\"MessageContent\" name=\"MessageContent\" cols=\"50\" rows=\"8\" wrap=\"virtual\" onFocus=\"this.rows=8\"></textarea>";
									//$thickboxContent .= $linterface->GET_TEXTAREA("MessageContent", '', 50,8);
							$thickboxContent .= '</td>';
						$thickboxContent .= '</tr>';
			
						# message tag drop down menu
						$thickboxContent .= '<tr class="push_message_layer">';
							$thickboxContent .= '<td valign="top" nowrap="nowrap" class="field_title">';
								$thickboxContent .= $Lang['eClassApp']['AutoFillItem'];
							$thickboxContent .= '</td>';
							$thickboxContent .= '<td class="tabletext" width="70%">';
								$thickboxContent .= $this->getMessageTagSelectionBox();
							$thickboxContent .= '</td>';
						$thickboxContent .= '</tr>';
			
					$thickboxContent .= '</table>';
				$thickboxContent .= '<br/>';
				
				# form button
				$thickboxContent .= '<div align="center">';
					$thickboxContent .= $linterface->GET_ACTION_BTN($Lang['Btn']['Send'], "button", "AppMessageReminder.sendAppMessage();" ,"SendAppMessage");
					$thickboxContent .= "&nbsp;&nbsp;";
					$thickboxContent .= $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();");
				$thickboxContent .= '</div>';
				
				# Hidden field for submit to ajax
				//$hiddenFieldAry = array ();
				//$hiddenFieldAry[] = array('iput_field_id', 'value'' );
				foreach($hiddenFieldAry as $hiddenField){
					$thickboxContent .= '<input type="hidden" class="messageInfo" id ="'.$hiddenField[0].'" name="'.$hiddenField[0].'" value="'.$hiddenField[1].'">';
				}
					$thickboxContent .= '<input type="hidden" name="Module" id="Module" value="'.$this->getModule().'" >';
					$thickboxContent .= '<input type="hidden" name="Section" id="Section" value="'.$this->getSection().'" >';
				$thickboxContent .= '</form>';
			
			$thickboxContent .= '</div>';
			
			$thickboxAssoAry['Content'] = $thickboxContent;
			$thickboxAssoAry['Button'] = $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['button'], "button", "AppMessageReminder.showThickbox();");;
			return $thickboxAssoAry;
	    }

	}
}