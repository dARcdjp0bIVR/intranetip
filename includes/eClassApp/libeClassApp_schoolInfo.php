<?php
// editing by 
###################################################### Change Log ######################################################
# Date      : 2019-01-11 (Tiffany)
# Detail    : Support Description for Eng
# Deploy    :
#
# Date      : 2017-07-20 (Roy)
# Detail    : improve getItemDetail(), return pdf path and name
# Deploy    :
#
# Date      : 2017-07-19 (Tiffany)
# Detail    : add saveSchInfoUploadPdf (), deleteSchInfoPdf() function
# Deploy    :
########################################################################################################################
if (!defined("LIBECLASSAPP_SCHOOLINFO_DEFINED")) {
	define("LIBECLASSAPP_SCHOOLINFO_DEFINED", true);
	
	class libeClassApp_schoolInfo extends libdb {
		function libeClassApp_schoolInfo() {
			$this->libdb();
			$this->ModuleTitle = 'eClassApp_schoolInfo';
		}
		
		//school_info_list.php
		function getParentTitle($MenuNameField,$ParentMenuID) {			
			$sql = "select $MenuNameField as Title,ParentMenuID
			        from SCHOOL_INFO
			        where MenuID='".$ParentMenuID."' and RecordStatus != 0";
            return $this->returnResultSet($sql);
					
		}
		
		function getTitles($MenuNameField,$ParentMenuID) {
			$sql = "select MenuID,$MenuNameField as Title , IsItem, RecordStatus
			        from SCHOOL_INFO
			        where ParentMenuID='".$ParentMenuID."' and RecordStatus != 0
			        order by DisplayOrder asc";
            return $this->returnResultSet($sql);
		}
		
		function getSubTitlesNum($ParentMenuID) {
			$sql ="select MenuID from SCHOOL_INFO where ParentMenuID='".$ParentMenuID."' and RecordStatus != 0";			
            return $this->returnVector($sql);
		}
		
		//school_info_new.php
		function getEditDetail($MenuID) {
			$sql = "select * from SCHOOL_INFO where MenuID = '".$MenuID."'";
			return $this->returnResultSet($sql);
		}
		
		function saveSchInfoCustomIcon($id,$parImageInfoAry) {
			global $eclassAppConfig, $PATH_WRT_ROOT;
		
			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
			$lfs = new libfilesystem();
		
			$successAry = array();
			if ($parImageInfoAry['name'] != ''  && $parImageInfoAry['error'] > 0) {
				$successAry['imageDataValid'] = false;
			}
			else if ($parImageInfoAry['name'] == '') {
				$successAry['emptyImage'] = true;
			}
			else {
				// create root folder
				$subfolder = ceil($id/30000);//1-30000 insert into 1,then 30001-60000 into 2...
				$tmpFilePath = $eclassAppConfig['urlFilePath'].'schInfoCustomIcon/'.$subfolder.'/';
				if (!file_exists($tmpFilePath)) {
					$successAry['createRootFolder'] = $lfs->folder_new($tmpFilePath);
					chmod($tmpFilePath, 0777);
				}
				// upload new files
				$orgFileName = $parImageInfoAry['name'];
				$tmpName = $parImageInfoAry['tmp_name'];
				$fileExt = get_file_ext($orgFileName);
				$targetFileName = $id.$fileExt;
				$targetFilePath = $tmpFilePath.$targetFileName;
				$successAry['uploadFile'] = $lfs->file_copy($tmpName, $targetFilePath);
				if ($successAry['uploadFile']) {
					$dbFilePath = str_replace($eclassAppConfig['urlFilePath'], '', $targetFilePath);
						
					// update
					$sql = "Update SCHOOL_INFO Set Icon = '".$this->Get_Safe_Sql_Query($dbFilePath)."', DateModified = now(), ModifyBy = '".$_SESSION['UserID']."' Where MenuID = '".$id."'";
					$successAry['updateDbRecord'] = $this->db_db_query($sql);
		
				}
			}
		
			return !in_array(false, (array)$successAry);
		}
		
		//school_info_new_update.php
		function NewSchoolInfo($groupTitleCh,$groupTitleEn,$iconType,$icon_sql,$type,$Description,$DescriptionEng,$url,$ParentMenuID,$Level,$status,$displayOrder) {
			$sql = "INSERT INTO SCHOOL_INFO (TitleChi, TitleEng, IconType, Icon, IsItem, Description,DescriptionEng,Url,ParentMenuID,Level, RecordStatus,DisplayOrder,DateInput, InputBy, DateModified,ModifyBy)
		            VALUES ('".$this->Get_Safe_Sql_Query($groupTitleCh)."' , '".$this->Get_Safe_Sql_Query($groupTitleEn)."', '".$iconType."', '".$this->Get_Safe_Sql_Query($icon_sql)."', '".$type."' ,'".$this->Get_Safe_Sql_Query($Description)."','".$this->Get_Safe_Sql_Query($DescriptionEng)."','".$this->Get_Safe_Sql_Query($url)."','".$ParentMenuID."','".$Level."','".$status."','".$displayOrder."',now(),'".$_SESSION['UserID']."',now(),'".$_SESSION['UserID']."')";
			return $this->db_db_query($sql);
		}
		
		function UpdateSchoolInfo($groupTitleCh,$groupTitleEn,$iconType,$icon_sql,$type,$Description,$DescriptionEng,$url,$MenuID,$status) {
			$sql = "Update SCHOOL_INFO SET TitleChi = '".$this->Get_Safe_Sql_Query($groupTitleCh)."', TitleEng = '".$this->Get_Safe_Sql_Query($groupTitleEn)."', IconType = '".$iconType."', Icon = '".$this->Get_Safe_Sql_Query($icon_sql)."', IsItem = '".$type."' ,Description = '".$this->Get_Safe_Sql_Query($Description)."', DescriptionEng = '".$this->Get_Safe_Sql_Query($DescriptionEng)."', Url = '".$this->Get_Safe_Sql_Query($url)."', RecordStatus = '".$status."',DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'
			where MenuID = '".$MenuID."'";
			return $this->db_db_query($sql);
		}
		
		function getMaxSchoolInfo(){
			$sql = "select MAX(MenuID) FROM SCHOOL_INFO";
			return $this->returnVector($sql);
		}
		
		function updateSchoolInfoFlashUploadPath($MenuID,$Content,$action='new'){
			global $cfg, $PATH_WRT_ROOT,$file_path;
		
			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
			$lfs = new libfilesystem();
		
			$Content = stripslashes(htmlspecialchars_decode($Content));
			if($action=='copy'){
				$Content = $lfs->copy_fck_flash_image_upload_to_new_id_loc($MenuID, $MenuID, $Content, $file_path, $cfg['fck_image']['SchoolInfo']);
			}else{
				$Content = ($lfs->copy_fck_flash_image_upload($MenuID, $Content, $file_path, $cfg['fck_image']['SchoolInfo']));
			}
			$Content = trim(intranet_htmlspecialchars($Content));
			$sql = "UPDATE SCHOOL_INFO SET Description = '".$this->Get_Safe_Sql_Query($Content)."' WHERE MenuID = '$MenuID'";
			$this->db_db_query($sql);
		}

        function updateSchoolInfoFlashUploadPathEng($MenuID,$Content,$action='new'){
            global $cfg, $PATH_WRT_ROOT,$file_path;

            include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
            $lfs = new libfilesystem();

            $Content = stripslashes(htmlspecialchars_decode($Content));
            if($action=='copy'){
                $Content = $lfs->copy_fck_flash_image_upload_to_new_id_loc($MenuID."_Eng", $MenuID."_Eng", $Content, $file_path, $cfg['fck_image']['SchoolInfo']);
            }else{
                $Content = ($lfs->copy_fck_flash_image_upload($MenuID."_Eng", $Content, $file_path, $cfg['fck_image']['SchoolInfo']));
            }
            $Content = trim(intranet_htmlspecialchars($Content));
            $sql = "UPDATE SCHOOL_INFO SET DescriptionEng = '".$this->Get_Safe_Sql_Query($Content)."' WHERE MenuID = '$MenuID'";
            $this->db_db_query($sql);
        }

		function getMAXDisplayOrder($ParentMenuID){
			$sql = "select MAX(DisplayOrder) FROM SCHOOL_INFO where ParentMenuID='".$ParentMenuID."' and RecordStatus !=0";
			return $this->returnVector($sql);			
		}
		
		//menu.php(APP)		
		function getSchoolInfoDetail($ParentMenuID) {
			$sql = "select TitleChi, TitleEng, IsItem,Description,DescriptionEng,Url from SCHOOL_INFO where MenuID = '".$ParentMenuID."'";
			return $this->returnResultSet($sql);
		}

		function getItemDetail($ParentMenuID) {
			$sql = "select MenuID, TitleChi, TitleEng, Icon, IconType, IsItem,Url, PdfPath, PdfName
					from SCHOOL_INFO 
					where ParentMenuID = '".$ParentMenuID."' and RecordStatus = 1 
					order by DisplayOrder asc ";
			return $this->returnResultSet($sql);
		}
		
		//custom_icon_delete.php
		function deleteSchInfoCustomIcon($id) {
			global $eclassAppConfig, $PATH_WRT_ROOT;
		
			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
			$lfs = new libfilesystem();
				
			$successAry = array();
		
			### get image data
			$sql = "Select Icon from SCHOOL_INFO Where MenuID = '".$id."'";
			$result = $this->returnVector($sql);
				
			$imagePath = $eclassAppConfig['urlFilePath'].$result[0];
				
			### delete image
			unlink($imagePath);
				
			### update db record
			$sql = "Update SCHOOL_INFO Set Icon = '', IconType = '2', DateModified = now(), ModifyBy = '".$_SESSION['UserID']."' Where MenuID = '".$id."'";
			$successAry['changeRecordStatus'] = $this->db_db_query($sql);
		
			return $successAry;
		}
		
		//school_info_delete.php
		function deleteSchoolInfo($MenuIDAry=array())
		{
			 
		
			for($i=0, $i_max=sizeof($MenuIDAry); $i<$i_max; $i++) {
				$menuID = $MenuIDAry[$i];
				 
				if($menuID=="") continue;
				 
				$sql = "SELECT IsItem FROM SCHOOL_INFO WHERE MenuID='".$menuID."'";
				$result = $this->returnVector($sql);
		
				if(sizeof($result)>0) {
					if($result[0]!=0) {
						$result[] = $this->deleteSchInfoItem($menuID);
		
					} else {
						$result[] = $this->deleteSchInfoMenu($menuID);
					}
				}
			}
			return $result;
		}
		
		function deleteSchInfoItem($menuID)
		{
			global $UserID;
		
			$sql = "UPDATE SCHOOL_INFO SET RecordStatus=0, DateModified=NOW(), ModifyBy='".$UserID."' WHERE MenuID='".$menuID."'";
		
			$this->db_db_query($sql);
		
			return $result;
		}
		
		function deleteSchInfoMenu($menuID)
		{
			global $UserID;
		
			$sql = "SELECT MenuID FROM SCHOOL_INFO WHERE ParentMenuID='$menuID'";
			$childMenu = $this->returnVector($sql);
		
			if (sizeof($childMenu)>0)
			{
				$this->deleteSchoolInfo($childMenu);
			}
		
			$sql = "UPDATE SCHOOL_INFO SET RecordStatus = 0, DateModified=NOW(), ModifyBy='".$UserID."' WHERE MenuID='".$menuID."'";
			$result = $this->db_db_query($sql);
		
			return $result;
		}
		
		//ajax_update.php
		
		function Get_Update_Display_Order_Arr($OrderText, $Separator=",")
		{
			if ($OrderText == "")
				return false;
			$orderArr = explode($Separator, $OrderText);
			$orderArr = array_remove_empty($orderArr);
			$orderArr = Array_Trim($orderArr);
			$numOfOrder = count($orderArr);
			# display order starts from 1
			$counter = 1;
			$newOrderArr = array();
			for ($i=0; $i<$numOfOrder; $i++)
			{
				$thisID = $orderArr[$i];
				if (is_numeric($thisID))
				{
					$newOrderArr[$counter] = $thisID;
					$counter++;
				}
			}
			return $newOrderArr;
		}
		
		function Update_Category_Record_DisplayOrder($DisplayOrderArr=array())
		{
			if (count($DisplayOrderArr) == 0)
				return false;
			
				$this->Start_Trans();
				for ($i=1; $i<=sizeof($DisplayOrderArr); $i++) {
					$thisObjectID = $DisplayOrderArr[$i];
					
					$sql = 'UPDATE SCHOOL_INFO SET
					DisplayOrder = \''.$i.'\'
					WHERE
										MenuID = \''.$thisObjectID.'\'';
					
							$Result['ReorderResult'.$i] = $this->db_db_query($sql);
				}
			
			if (in_array(false,$Result))
				{
					$this->RollBack_Trans();
					return false;
			}
			else
			{
				$this->Commit_Trans();
				return true;
			}
		}
		
		//ajax_update_status.php
		function Update_Status($MenuID,$status)
		{
			$MenuIDArr = explode(",", $MenuID);
			if (count($MenuIDArr) == 0)
				return false;
				
			$this->Start_Trans();
			for ($i=0; $i<sizeof($MenuIDArr); $i++) {
				$thisMenuID = $MenuIDArr[$i];
					
				$sql = 'UPDATE SCHOOL_INFO SET
					RecordStatus = \''.$status.'\'
					WHERE
										MenuID = \''.$thisMenuID.'\'';
				
				$Result['Result'.$i] = $this->db_db_query($sql);
			}
				
			if (in_array(false,$Result))
			{
				$this->RollBack_Trans();
				return false;
			}
			else
			{
				$this->Commit_Trans();
				return true;
			}
		}

		//test
		function Get_Menu_Hierarchy_Count_Array(){		
			$returnAry = array();
			$sql = "select MenuID,TitleChi,TitleEng,IsItem,ParentMenuID,RecordStatus,DisplayOrder from SCHOOL_INFO where RecordStatus !=0";
			$result = $this->returnResultSet($sql);
			$returnAry = $this->getMenuHierarchyCountArray($result);
			return $returnAry;
		}
		
		function getMenuHierarchyCountArray($dataAry,$curFolderID=0)
		{
			$returnAry = array();
			$numOfRecord = count($dataAry);
			$MenuNameField = Get_Lang_Selection('TitleChi', 'TitleEng');
				
			for($i=0;$i<$numOfRecord;$i++) {
				$id = $dataAry[$i]['MenuID'];
				$title = $dataAry[$i][$MenuNameField];
				$parent_folder_id = $dataAry[$i]['ParentMenuID'];
					
				if($id == $curFolderID) {
					if(!isset($returnAry[$id])){
						$returnAry[$id] = array();
					}
					$returnAry[$id]['MenuID'] = $id;
					$returnAry[$id]['ParentMenuID'] = $parent_folder_id;
					$returnAry[$id]['Title'] = $title;
					$returnAry[$id]['SubMenu'] = array();
				}
			}
		
			for($i=0;$i<$numOfRecord;$i++) {
				$id = $dataAry[$i]['MenuID'];
				$title = $dataAry[$i][$MenuNameField];
				$parent_folder_id = $dataAry[$i]['ParentMenuID'];
					
				if($parent_folder_id == $curFolderID) {
					$returnAry[$curFolderID]['SubMenu'][] = $this->getMenuHierarchyCountArray($dataAry, $id);
				}
			}
			return $returnAry;
		}
		
		
		function Get_Folder_Hierarchy_File_Count_Array($GroupID, $FolderID=0)
		{
		
			$returnAry = array();
		
			$sql = "SELECT '0' as DocumentID, '/' as Title,'-1' as ParentFolderID, COUNT(r1.DocumentID) as FileCount
			FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD as r1
			WHERE r1.IsFolder=0 AND r1.RecordStatus=1 AND r1.IsLatestFile=1 AND r1.GroupID='$GroupID' AND r1.ParentFolderID=0 $conds
			GROUP BY r1.ParentFolderID";
		 
			$rootResultAry = $this->returnResultSet($sql);
		
			$sql = "SELECT r1.DocumentID, r1.Title, r1.ParentFolderID, COUNT(r2.DocumentID) as FileCount
			FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r1
			LEFT JOIN DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD r2 ON r2.IsFolder=0 AND r2.RecordStatus=1 AND r2.IsLatestFile=1 AND r2.ParentFolderID=r1.DocumentID AND r2.GroupID='$GroupID' $conds2
			WHERE r1.IsFolder=1 AND r1.RecordStatus=1 AND r1.GroupID='$GroupID' $conds
			GROUP BY r1.DocumentID";
		
			$result = $this->returnResultSet($sql);
			return $result;
				
			if(count($rootResultAry)>0) {
				array_unshift($result,$rootResultAry[0]);
			}
			$returnAry = $this->getFolderHierarchyFileCountArray($result, $FolderID);
		
			return $returnAry;
			//return $rootResultAry;
		}
		
		function getFolderHierarchyFileCountArray($dataAry,$curFolderID)
		{
			$returnAry = array();
			$numOfRecord = count($dataAry);
		
			for($i=0;$i<$numOfRecord;$i++) {
				$id = $dataAry[$i]['DocumentID'];
				$title = $dataAry[$i]['Title'];
				$parent_folder_id = $dataAry[$i]['ParentFolderID'];
				$file_count = $dataAry[$i]['FileCount'];
					
				if($id == $curFolderID) {
					if(!isset($returnAry[$id])){
						$returnAry[$id] = array();
					}
					$returnAry[$id]['FolderID'] = $id;
					$returnAry[$id]['ParentFolderID'] = $parent_folder_id;
					$returnAry[$id]['Title'] = $title;
					$returnAry[$id]['FileCount'] = $file_count;
					$returnAry[$id]['Subfolders'] = array();
				}
			}
		
			for($i=0;$i<$numOfRecord;$i++) {
				$id = $dataAry[$i]['DocumentID'];
				$title = $dataAry[$i]['Title'];
				$parent_folder_id = $dataAry[$i]['ParentFolderID'];
				$file_count = $dataAry[$i]['FileCount'];
					
				if($parent_folder_id == $curFolderID) {
					$returnAry[$curFolderID]['Subfolders'][] = $this->getFolderHierarchyFileCountArray($dataAry, $id);
				}
			}
			return $returnAry;
		}
		
		
		function Get_Quickview_Folder_Div($array = array (), $levelCount = 0, $FolderID = "", $NoReload = "", $moveTo = "", $GroupID = "", $selectedGroupID = "", $showTitle=true) {
			global $Lang;
		
			if (sizeof($array) > 0) {
				$returnText .= '<div class="folder_list">' . "\n";
		
				if($this-> Has_Folder_In_Group($GroupID, 0) == FALSE)
					$returnText .= "* ".$Lang['DigitalArchive']['NoSubfolder'];
		
				$returnText .= $this->Get_Quickview_Folder_Tree($array, $levelCount +1, $FolderID, $NoReload, $moveTo, $GroupID, $selectedGroupID,$showTitle);
		
				$returnText .= '</div>' . "\n";
			}
		
			return $returnText;
		}
		
		function Get_Quickview_Folder_Tree($array = array (), $levelCount = 0, $FolderID = "", $NoReload = "", $moveTo = "", $GroupID = "", $selectedGroupID = "", $showTitle=true) {
			global $Lang;
		
			if (count($array) > 0) {
				if ($levelCount == 1) {
					$returnText .= '<ul ' . ($levelCount == 1 ? 'class="folder_tree"' : '') . '>' . "\n";
				}
				foreach ($array as $folder_id => $folder_ary) {
		
					$parent_folder_id = $folder_ary['ParentFolderID'];
					$title = $folder_ary['Title'];
					$file_count = $folder_ary['FileCount'];
					$subfolders = $folder_ary['Subfolders'];
					$subfolder_count = count($subfolders);
		
					$onclick = (($GroupID == $selectedGroupID) ? "document.getElementById('status_option_value').value=0;document.form1.GroupID.value=" . $GroupID . ";document.form1.FolderID.value=" . $folder_id . ";Get_Directory_List_Table();" : "self.location.href='".(($showTitle==TRUE)?"fileList_Icon.php": $_SERVER["PATH_INFO"])."?GroupID=$GroupID&FolderID=$folder_id&folderTreeSelect=".(($showTitle==TRUE)?"":"selected")."'");
					$is_last_folder = false;
		
					if ($folder_id != 0) {
						$returnText .= '<li ' . ($is_last_folder ? 'class="last"' : '') . '>' . "\n";
						if ($subfolder_count > 0) {
							$returnText .= '<a class="btn_folder_control_collapse" href="javascript:void(0);" onclick="expandCollapseFolder(this);"></a>' . "\n";
						}
						if($_POST['FolderID'] == $folder_id){
							$returnText .= '<a class="' . ($subfolder_count == 0 ? 'folder_close folder_selected' : 'folder_open folder_selected') . '" href="javascript:void(0);" onclick="' . $onclick . '">' . $title . '<span>&nbsp;(' . $file_count . ')</span></a>' . "\n";
						}
						else
							$returnText .= '<a class="' . ($subfolder_count == 0 ? 'folder_close' : 'folder_open') . '" href="javascript:void(0);" onclick="' . $onclick . '">' . $title . '<span>&nbsp;(' . $file_count . ')</span></a>' . "\n";
					}
					if ($subfolder_count > 0) {
						if ($folder_id != 0) {
							$returnText .= '<ul>' . "\n";
						}
						for ($i = 0; $i < $subfolder_count; $i++) {
							$returnText .= $this->Get_Quickview_Folder_Tree($subfolders[$i], $levelCount +1, $FolderID, $NoReload, $moveTo, $GroupID, $selectedGroupID, $showTitle);
						}
						if ($folder_id != 0) {
							$returnText .= '</ul>' . "\n";
						}
					}
					if ($folder_id != 0) {
						$returnText .= '</li>' . "\n";
					}
		
				}
				if ($levelCount == 1) {
					$returnText .= '</ul>' . "\n";
				}
			}
		
			return $returnText;
		}
		
		function Has_Folder_In_Group($GroupID, $FolderID=0){
			global $_SESSION, $UserID;
								
			$returnAry = array();
			$sql = "SELECT '0' as DocumentID, '/' as Title,'-1' as ParentFolderID, COUNT(r1.DocumentID) as FileCount
			FROM DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD as r1
			WHERE r1.IsFolder=1 AND r1.RecordStatus=1 AND r1.IsLatestFile=1 AND r1.GroupID='$GroupID' AND r1.ParentFolderID=0 $conds
			GROUP BY r1.ParentFolderID";
			$rootResultAry = $this->returnResultSet($sql);
				
			if($rootResultAry != NULL)
				return TRUE;
				else
				return FALSE;
		}
		
		function Include_JS_CSS() {
			global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
			//include_once($PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/common_js_lang.php');
			$x = '
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.blockUI.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.core.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.draggable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.droppable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/ui.selectable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.jeditable.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.pack.js"></script>
					<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/script.js"></script>
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/fancybox-1.3.4/jquery.fancybox-1.3.4.css"  type="text/css" media="screen">
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />
					<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
					';
			return $x;
		}
		
		function saveSchInfoUploadPdf($id,$parPdfAry) {
			global $eclassAppConfig, $PATH_WRT_ROOT;
		
			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
			$lfs = new libfilesystem();
		
			$successAry = array();
			if ($parPdfAry['name'] != ''  && $parPdfAry['error'] > 0) {
				$successAry['pdfDataValid'] = false;
			}
			else if ($parPdfAry['name'] == '') {
				$successAry['emptyPdf'] = true;
			}
			else {
				// create root folder
				$subfolder = ceil($id/30000);//1-30000 insert into 1,then 30001-60000 into 2...
				$tmpFilePath = $eclassAppConfig['urlFilePath'].'schInfoPdf/'.$subfolder.'/';
				if (!file_exists($tmpFilePath)) {
					$successAry['createRootFolder'] = $lfs->folder_new($tmpFilePath);
					chmod($tmpFilePath, 0777);
				}
				// upload new files
				$orgFileName = $parPdfAry['name'];
				$tmpName = $parPdfAry['tmp_name'];
				$fileExt = get_file_ext($orgFileName);
				$targetFileName = $id.$fileExt;
				$targetFilePath = $tmpFilePath.$targetFileName;
				$successAry['uploadFile'] = $lfs->file_copy($tmpName, $targetFilePath);
				if ($successAry['uploadFile']) {
					$dbFilePath = str_replace($eclassAppConfig['urlFilePath'], '', $targetFilePath);
						
					// update
					$sql = "Update SCHOOL_INFO Set PdfPath = '".$this->Get_Safe_Sql_Query($dbFilePath)."', PdfName = '".$this->Get_Safe_Sql_Query($orgFileName)."', DateModified = now(), ModifyBy = '".$_SESSION['UserID']."' Where MenuID = '".$id."'";
					$successAry['updateDbRecord'] = $this->db_db_query($sql);
		
				}
			}
		
			return !in_array(false, (array)$successAry);
		}
		
	    //pdf_delete.php
		function deleteSchInfoPdf($id) {
			global $eclassAppConfig, $PATH_WRT_ROOT;
		
			include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
			$lfs = new libfilesystem();
				
			$successAry = array();
		
			### get image data
			$sql = "Select PdfPath from SCHOOL_INFO Where MenuID = '".$id."'";
			$result = $this->returnVector($sql);
				
			$imagePath = $eclassAppConfig['urlFilePath'].$result[0];
				
			### delete image
			unlink($imagePath);
				
			### update db record
			$sql = "Update SCHOOL_INFO Set PdfPath = '', PdfName = '', DateModified = now(), ModifyBy = '".$_SESSION['UserID']."' Where MenuID = '".$id."'";
			$successAry['changeRecordStatus'] = $this->db_db_query($sql);
		
			return $successAry;
		}		
	}
}
?>