<?php
$elibplus_cfg['book_reviews_limit'] = 10;				// number of reviews show in book details
$elibplus_cfg['book_reviews_more'] = 10;				// number of reviews append for each 'more' click in book details
$elibplus_cfg['category_limit'] = 500;					// max number of categories show in portal catalog
$elibplus_cfg['carousel_active_books'] = 6;				// number of active books per page in portal book shelf
//$elibplus_cfg['carousel_ebook_accumulated_days'] = 60;	// number of accumulated days to count most hit books shown in portal eBook shelf
$elibplus_cfg['carousel_ebook_best_rated'] = 3;			// number of best rated books shown in portal eBook shelf
$elibplus_cfg['carousel_ebook_limit'] = 50;				// number of books loop in portal eBook shelf
$elibplus_cfg['carousel_ebook_most_hit'] = 3;			// number of most hit books shown in portal eBook shelf
//$elibplus_cfg['carousel_pbook_accumulated_days'] = 60;	// number of accumulated days to count most loan books shown in portal physical book shelf
$elibplus_cfg['carousel_pbook_best_rated'] = 3;			// number of best rated books shown in portal physical book shelf
$elibplus_cfg['carousel_pbook_limit'] = 50;				// number of books loop in portal physical book shelf
$elibplus_cfg['carousel_pbook_most_loan'] = 3;			// number of most loan books shown in portal physical book shelf
$elibplus_cfg['eLearning_path'] = "/eLearning";			// set this in IP & EJ
$elibplus_cfg['my_favourite_books_per_row'] = 6;		// number of favourite books per row in my record book shelf
$elibplus_cfg['navigation_cover_per_page'] = 10;		// number of book cover per page in cover view of search result
$elibplus_cfg['navigation_item_per_page'] = 20;			// number of rows per page to show in table list view of search result
$elibplus_cfg['portal_news_limit'] = 5;					// number of news show in portal announcement
$elibplus_cfg['portal_news_more'] = 10;					// number of news append for each click in portal announcement
$elibplus_cfg['similarity_percentage_1'] = 50;			// 1st round similarity percentage to find related books in book detail page
$elibplus_cfg['similarity_percentage_2'] = 80;			// 2nd round similarity percentage to find related books in book detail page
$elibplus_cfg['user_reviews_more'] = 5;					// number of reviews append for each 'more' click in ranking user reviews
$elibplus_cfg['more_new_ebooks_limit'] = 100;			// number of ebooks regarded as new when click 'more' in portal 
$elibplus_cfg['more_new_pbooks_limit'] = 100;			// number of physical books regarded as new when click 'more' in portal
?>