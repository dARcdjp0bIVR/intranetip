<?php
// editing by 

/********************
 * 2017-12-08 (Carlos): Added retrieveMailUserList() to get user token list for mail index server sync job.
 * 						Modified retrieveMailUserInfoByToken($parToken) added "isAdmin" to return array.
 ********************/
if (!defined("LIBECLASSAPP_API_DEFINED")) {
	define("LIBECLASSAPP_API_DEFINED", true);
	
	class libeClassAppApi {
		var $aesObjAry = array();
		var $dbObj = null;
		var $delimiter = '###';
		var $pk = 'Fe4RE&4rM';
		var $salt = 'SDbg5!hj';
		
		function libeClassAppApi() {
			
		}
		
		function getDelimiter() {
			return $this->delimiter;
		}
		function getPk() {
			return $this->pk;
		}
		function getSalt() {
			return $this->salt;
		}
		
		function getAesInstance($parKey) {
			if ($this->aesObjAry[$parKey] === null) {
				global $intranet_root;
				include_once($intranet_root.'/includes/eClassApp/libAES.php');
				
				$this->aesObjAry[$parKey] = new libAES($parKey);
			}
			
			return $this->aesObjAry[$parKey];
		}
		function getDbObjInstance() {
			if ($this->dbObj=== null) {
				global $intranet_root;
				include_once($intranet_root.'/includes/libdb.php');
				
				$this->dbObj = new libdb();
			}
			
			return $this->dbObj;
		}
		
		
		function getApiToken($parUserId, $parUserLogin) {
			$delimiter = $this->getDelimiter();
			$timePk = $this->getTimePk();
			$salt = $this->getSalt();
			
			$keyStr = $salt.$delimiter.$parUserId.$delimiter.$parUserLogin.$delimiter.$salt;
			
			$aesObj = $this->getAesInstance($timePk);
			return safe_b64encode($aesObj->encrypt($keyStr));
		}
		
		function getTimePk($parTime=0) {
			$pk = $this->getPk();
			
			return $pk;
			
// 			$curTs = time();
// 			if ($parTime == -1) {
// 				$targetTs = $curTs - 3600;
// 			}
// 			else if ($parTime == 1) {
// 				$targetTs = $curTs + 3600;
// 			}
// 			else {
// 				$targetTs = $curTs;
// 			}
			
// 			return $pk.date('YmdH', $targetTs);
		}
		
		function decryptToken($parToken) {
			$timeUnitAry = array(0, 1, -1);
			$numOfTry = count($timeUnitAry);
			
			$decryptedToken = '';
			for ($i=0; $i<$numOfTry; $i++) {
				$_timeUnit = $timeUnitAry[$i];
				
				$_timePk = $this->getTimePk($_timeUnit);
				$_aesObj = $this->getAesInstance($_timePk);
				$_decryptedToken = $_aesObj->decrypt(safe_b64decode($parToken));
				
				if ($_decryptedToken === false) {
					// try another time unit
				}
				else {
					$decryptedToken = $_decryptedToken;
					break;
				}
			}
			
			return $decryptedToken;
		}
		
		function retrieveMailUserInfoByToken($parToken) {
		    global $intranet_root, $eclassAppConfig;
		    
		    include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
			
			$decryptedToken = $this->decryptToken($parToken);
			if ($decryptedToken === false) {
				// invalid token
				return '-999';
			}
			
			$delimiter = $this->getDelimiter();
			$tokenPieces = explode($delimiter, $decryptedToken);
			$tokenUserId = $tokenPieces[1];
			$tokenUserLogin = $tokenPieces[2];
			
			$dbObj = $this->getDbObjInstance();
			$sql = "SELECT UserID, UserLogin, ImapUserEmail FROM INTRANET_USER WHERE UserID = '".$tokenUserId."'";
			$userAry = $dbObj->returnResultSet($sql);
			$dbUserId = $userAry[0]['UserID'];
			$dbUserLogin = $userAry[0]['UserLogin'];
			$dbImapUserEmail = $userAry[0]['ImapUserEmail'];
			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SchoolSettings' AND SettingName='admin_user'";
			$temp_ary = $dbObj->returnVector($sql);
			$admin_user_ids = array();
			if(count($temp_ary)>0){
				$admin_user_ids = explode(",",trim($temp_ary[0]));
			}
			$dbIsAdmin = in_array($dbUserId,$admin_user_ids) ? '1' : '0';
			if ($dbUserId == '' || $dbUserId != $tokenUserId || $dbUserLogin == '' || $dbUserLogin != $tokenUserLogin) {
				// invalid token => data not matched
				return '-999';
			}
			
			
			// retrieve $_SESSION for mail targeting
			if (!isset($_SESSION['SSV_USER_TARGET'])) {
				include_once($intranet_root.'/includes/user_right_target.php');
				$urtObj = new user_right_target();
				$_SESSION['SSV_USER_TARGET'] = $urtObj->Load_User_Target($dbUserId);
			}
			
			// retrieve user data
			global $intranet_root;
			include_once($intranet_root.'/includes/libpwm.php');
			//include_once($intranet_root.'/includes/imap_gamma.php');
			
			$lpwm = new libpwm();
			$infoAry = $lpwm->getData($dbUserId);
			$userPw = $infoAry[$dbUserId];
			
			$returnAry = array();
			$returnAry['userId'] = $dbUserId;
			$returnAry['userLogin'] = $dbUserLogin;
			$returnAry['userPw'] = $userPw;
			$returnAry['email'] = $dbImapUserEmail;
			$returnAry['isAdmin'] = $dbIsAdmin;
			//$returnAry['imapObj'] = new imap_gamma($skipAutoLogin=false, $dbUserLogin, $userPw, $isHalfOpen=false);
			
			return $returnAry;
		}
		
		// smaller number means higher priority
		function retrieveMailUserList()
		{
			global $intranet_root, $eclassAppConfig;
		    
		    include_once($intranet_root.'/includes/eClassApp/eClassAppConfig.inc.php');
		    
		    $dbObj = $this->getDbObjInstance();
		    
		    // uncomment RecordType,RecordStatus,TitleEnglish for debug display
		    $sql = "SELECT UserID,UserLogin,ImapUserEmail as Email,
						-- RecordType,RecordStatus,TitleEnglish,
						'' as Token,
						0 + IFNULL(RecordType,0) - IF(RecordStatus=1,1,0) - 
						IF(TitleEnglish LIKE '%assistant%',
							20,
							IF(TitleEnglish LIKE '%manager%',
								50,
								IF(TitleEnglish LIKE '%vice%' OR TitleEnglish LIKE '%VP%',
									80,
									IF(TitleEnglish LIKE '%Principal%',100,0) 
								)
							)
						) as Priority 
					FROM INTRANET_USER WHERE ImapUserEmail IS NOT NULL AND ImapUserEmail<>'' ORDER BY Priority";
			
			$records = $dbObj->returnResultSet($sql);
			$record_size = count($records);
			$return_records = array();
			$min_priority = min(0, $records[0]['Priority']);
			for($i=0;$i<$record_size;$i++){
				$token = $this->getApiToken($records[$i]['UserID'], $records[$i]['UserLogin']);
				$return_records[] = array('Token'=>$token,'Priority'=>$records[$i]['Priority']-$min_priority+1);
			}
			
			return $return_records;
		}
	}
}
?>