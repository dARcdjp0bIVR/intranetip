<?php
// editing by Roy
 
/********************
 * 2020-09-28 Ray:
 * - modified addGroupMember, createOrUpdateGroupAndSyncToCloud, add YearClassID
 *
 * 20170830 Roy:
 * - modified getGroupData() and getGroupListPageSql(), add $excludeGroupTypeAry condition;
 * 
 * 20170728 Roy:
 * - added createOrUpdateGroup(), use to add or create group and sync to cloud;
 * 
 * 20170515 Roy:
 * - modified getGroupData(), add CommunicationMode to SQL statement;
 * 
 ********************/
if (!defined("LIBECLASSAPP_GROUPMESSAGE_DEFINED")) {
	define("LIBECLASSAPP_GROUPMESSAGE_DEFINED", true);
	
	class libeClassApp_groupMessage extends libdb {
		
		function libeClassApp_groupMessage() {
			$this->libdb();
		}
		
		function getMaxNumOfGroup() {
			global $sys_custom, $eclassAppConfig;
			
			$max = $eclassAppConfig['groupMessage']['maxNumOfGroup'];
			if (isset($sys_custom['eClassApp']['maxNumOfGroup'])) {
				$max = max($max, $sys_custom['eClassApp']['maxNumOfGroup']);
			}
			
			return $max;
		}
		
		function getMaxNumOfGroupMember() {
			global $sys_custom, $eclassAppConfig;
			
			$max = $eclassAppConfig['groupMessage']['maxNumOfGroupMember'];
			if (isset($sys_custom['eClassApp']['maxNumOfGroupMember'])) {
				$max = max($max, $sys_custom['eClassApp']['maxNumOfGroupMember']);
			}
			
			return $max;
		}
		
		function getGroupListPageSql($isAdminUser, $setCodeForGroup, $excludeGroupTypeAry='') {
			global $eclassAppConfig;
			
			$conds_groupId = '';
			if (!$isAdminUser) {
				$groupIdAry = Get_Array_By_Key($this->getUserAdminGroup($_SESSION['UserID']), 'GroupID');
				$conds_groupId = " AND g.GroupID IN ('".implode("','", (array)$groupIdAry)."') ";
			}
			
			$groupNameField = Get_Lang_Selection('g.NameCh', 'g.NameEn');
			if($setCodeForGroup){
				$addCodeShow = "g.Code, ";				
			}
			
			if ($excludeGroupTypeAry !== '') {
				$conds_excludeGroupType = " AND g.GroupType NOT IN ('".implode("','", (array)$excludeGroupTypeAry)."') ";
			}
			
			$sql = "Select UserID From INTRANET_USER Where RecordStatus = 1";
			$activeUserAry = Get_Array_By_Key($this->returnResultSet($sql), 'UserID');
			
			$sql = "Select 
							$addCodeShow
							$groupNameField as GroupName, 
							CONCAT('<a href=\"javascript:void(0);\" onclick=\"goMemberList(', g.GroupID, ');\">', IF(gm.GroupID is null, 0, COUNT(*)), '</a>') as MemberListLink,
							CONCAT('<input type=\"checkbox\" class=\"groupIdChk\" name=\"groupIdAry[]\" id=\"groupIdChk_Global\" value=\"', g.GroupID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />'),
							IF(gm.GroupID is null, 0, COUNT(*)) as NumOfMember  
					From 
							INTRANET_APP_MESSAGE_GROUP as g
							LEFT OUTER JOIN INTRANET_APP_MESSAGE_GROUP_MEMBER as gm ON (g.GroupID = gm.GroupID AND gm.UserID IN ('".implode("', '", (array)$activeUserAry)."'))
					Where
							g.RecordStatus = '".$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['active']."'
							$conds_groupId
							$conds_excludeGroupType
					Group By
							g.GroupID 
					";
			return $sql;
		}
		
		function getGroupData($groupIdAry='', $parCode='', $excludeGroupIdAry='', $excludeGroupTypeAry='') {
			if ($groupIdAry !== '') {
				$conds_groupId = " AND GroupID In ('".implode("','", (array)$groupIdAry)."') ";
			}
			
			if ($parCode !== '') {
				$conds_code = " AND Code = '".$this->Get_Safe_Sql_Query($parCode)."' ";
			}
			
			if ($excludeGroupIdAry !== '') {
				$conds_excludeGroupId = " AND GroupID NOT IN ('".implode("','", (array)$excludeGroupIdAry)."') ";
			}
			
			if ($excludeGroupTypeAry !== '') {
				$conds_excludeGroupType = " AND GroupType NOT IN ('".implode("','", (array)$excludeGroupTypeAry)."') ";
			}
			
			$sql = "Select GroupID, Code, NameEn, NameCh, CommunicationMode From INTRANET_APP_MESSAGE_GROUP Where RecordStatus = 1 $conds_groupId $conds_code $conds_excludeGroupId $conds_excludeGroupType";
			return $this->returnResultSet($sql);
		}
		
		function addGroupMember($groupId, $userIdAry, $memberType='', $yearClassID='') {
			global $eclassAppConfig;
			
			if ($memberType == '') {
				$memberType = $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['member'];
			}

			$extra_fields = '';
			$extra_values = '';
			if($yearClassID != '') {
				$extra_fields .= ', YearClassID';
				$extra_values .= ", '$yearClassID'";
			}

			$numOfUser = count($userIdAry);
			$insertAry = array();
			for ($i=0; $i<$numOfUser; $i++) {
				$_userId = $userIdAry[$i];
				$insertAry[] = " ('".$groupId."', '".$_userId."', '".$memberType."', now(), '".$_SESSION['UserID']."' $extra_values) ";
			}
			
			$sql = "INSERT INTO INTRANET_APP_MESSAGE_GROUP_MEMBER
						(GroupID, UserID, MemberType, DateInput, InputBy $extra_fields)
					VALUES
						".implode(', ', (array)$insertAry)."
					";
			return $this->db_db_query($sql);
		}
		
		function editGroupMember($groupId, $userIdAry, $memberType='') {
			$sql = "UPDATE INTRANET_APP_MESSAGE_GROUP_MEMBER SET MemberType = '".$memberType."' WHERE GroupID = '".$groupId."' AND UserID IN ('".implode("','", (array)$userIdAry)."')";
			return $this->db_db_query($sql);
		}
		
		function deleteGroupMember($groupId, $userIdAry) {
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT.'includes/liblog.php');
			$liblog = new liblog();
			
			$sql = "DELETE FROM INTRANET_APP_MESSAGE_GROUP_MEMBER WHERE GroupID = '".$groupId."' AND UserID IN ('".implode("','", (array)$userIdAry)."')";
			$success = $this->db_db_query($sql);
			
			if ($success) {
				$tmpAry = array();
			    $tmpAry['GroupID'] = $groupId;
			    $tmpAry['UserIDAry'] = implode(',', (array)$userIdAry);
			    $SuccessArr['Log_Delete'] = $liblog->INSERT_LOG('eClassApp', 'Delete_MeesageGropuMember', $liblog->BUILD_DETAIL($tmpAry), 'INTRANET_APP_MESSAGE_GROUP_MEMBER', '');
			}
			
			return $success;
		}
		
		function saveGroupMemberInCloud($groupId) {
			global $intranet_root, $config_school_code;
			
			include_once($intranet_root."/includes/json.php");
			include_once($intranet_root."/includes/eClassApp/libeClassApp.php");
			$jsonObj = new JSON_obj();
			$leClassApp = new libeClassApp();
			
			include_once($intranet_root.'/includes/services_json.php');
			$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
			
			$memberAry = $this->getGroupMemberList($groupId);
			$memberAssoAry = BuildMultiKeyAssoc($memberAry, 'GroupID', $IncludedDBField=array('UserID', 'MemberType'), $SingleValue=0, $BuildNumericArray=1);
			$memberJsonString = $jsonObj->encode($memberAssoAry);
			
			$memberUserIdAry = array_values(array_unique(Get_Array_By_Key($memberAry, 'UserID')));
			$userAssoAry = BuildMultiKeyAssoc($leClassApp->getUserInfo($memberUserIdAry, true), 'UserID');
			$userJsonString = $json->encode($userAssoAry);		// services_json can maintain array key as string. Otherwise, central server cannot decode the json
			
			$userDeviceAry = $leClassApp->getUserDevice($memberUserIdAry, '', true);
			$userDeviceJsonString = $jsonObj->encode($userDeviceAry);
			
			$postParamAry = array();
			$postParamAry['RequestMethod'] = 'saveGroupMessage_groupMember';
			$postParamAry['SchoolCode'] = $config_school_code;
			$postParamAry['MemberJsonString'] = $memberJsonString;
			$postParamAry['UserJsonString'] = $userJsonString;
			$postParamAry['UserDeviceJsonString'] = $userDeviceJsonString;
			$postJsonString = $jsonObj->encode($postParamAry);
			
			$headers = array('Content-Type: application/json; charset=utf-8', 'Expect:');
			$centralServerUrl = $leClassApp->getCurCentralServerUrl('groupMessage');
						
			session_write_close();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
			curl_setopt($ch, CURLOPT_URL, $centralServerUrl);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
			$responseJson = curl_exec($ch);
			curl_close($ch);
			
			$responseJson = standardizeFormPostValue($responseJson);
			$responseJsonAry = $jsonObj->decode($responseJson);
			$saveSuccess = ($responseJsonAry['MethodResult']=='1')? true : false;
			
			return $saveSuccess;
		}
		
		function getGroupMemberList($groupIdAry='', $userIdAry='', $activeUserOnly=true) {
			global $eclassAppConfig;
			
			if ($groupIdAry != '') {
				$conds_groupId = " AND gm.GroupID IN ('".implode("','", (array)$groupIdAry)."') ";
			}
			if ($userIdAry != '') {
				$conds_userId = " AND gm.UserID IN ('".implode("','", (array)$userIdAry)."') ";
			}
			if ($activeUserOnly) {
			    $conds_userRecordStatus = " AND iu.RecordStatus = 1 ";
			}
			
			$sql = "Select 
							gm.GroupID, gm.UserID, gm.MemberType 
					From 
							INTRANET_APP_MESSAGE_GROUP_MEMBER as gm 
							Inner Join INTRANET_APP_MESSAGE_GROUP as iamg ON (gm.GroupID = iamg.GroupID)
							Inner Join INTRANET_USER as iu ON (gm.UserID = iu.UserID)
					Where 
							iamg.RecordStatus = '".$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['active']."'
							$conds_groupId
							$conds_userId
							$conds_userRecordStatus
					Order By
							iu.EnglishName
					";
			return $this->returnResultSet($sql);
		}
		
		function getMemberTypeDisplay($parMemberType) {
			global $eclassAppConfig, $Lang;
			
			$memberTypeDisplay = '';
			switch ($parMemberType) {
				case $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['admin']:
					$memberTypeDisplay = $Lang['General']['Admin'];
					break;
				case $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['member']:
					$memberTypeDisplay = $Lang['General']['Member'];
					break;
			}
			
			return $memberTypeDisplay;
		}
		
		function getUserAdminGroup($parUserId) {
			global $eclassAppConfig;
			
			$sql = "Select 
							g.GroupID
					From
							INTRANET_APP_MESSAGE_GROUP as g
							INNER JOIN INTRANET_APP_MESSAGE_GROUP_MEMBER as gm ON (g.GroupID = gm.GroupID)
					Where
							g.RecordStatus = '".$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['active']."'
							AND gm.MemberType = '".$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['admin']."'
							AND gm.UserID = '".$parUserId."'
					";
			return $this->returnResultSet($sql);
		}
		
		function createOrUpdateGroupAndSyncToCloud($groupId, $groupCode, $groupNameEn, $groupNameCh, $communicationMode, $groupType, $yearClassID='') {
			global $PATH_WRT_ROOT, $eclassAppConfig, $sys_custom;
			include_once($PATH_WRT_ROOT."includes/eClassApp/groupMessage/libeClassApp_groupMessage_group.php");
			include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
			$leClassApp = new libeClassApp();
						
			$fromNew = ($groupId == '')? 1 : 0;
			
			$groupObj = new libeClassApp_groupMessage_group($groupId);
			$groupObj->setCode($groupCode);
			$groupObj->setNameEn($groupNameEn);
			$groupObj->setNameCh($groupNameCh);
			$groupObj->setCommunicationMode($communicationMode);
			$groupObj->setGroupType($groupType);
			$groupObj->setRecordStatus($eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['active']);
			if($yearClassID != '') {
				$groupObj->setYearClassID($yearClassID);
			}

			$successAry = array();
			$leClassApp->Start_Trans();
			$groupId = $groupObj->save();
			
			if(!$sys_custom['eClassApp']['GroupMessage']['setCodeForGroup']){
				$groupObj = new libeClassApp_groupMessage_group($groupId);
				$groupObj->setCode($groupId);
				$groupId = $groupObj->save();
			}
			
			$successAry['saveLocal'] = ($groupId > 0)? true : false;
			
			if ($successAry['saveLocal']) {
				$successAry['saveCloud'] = $groupObj->saveCloud();
				
				if (!$fromNew) {
					$successAry['saveCloudMember'] = $this->saveGroupMemberInCloud($groupId);
				}
			}
			
			$returnAry = array();
			$returnAry['groupId'] = $groupId;
			$returnAry['fromNew'] = $fromNew;
			if (in_array(false, $successAry)) {
				// fail
				$returnAry['success'] = false;
				$returnAry['returnMsgKey'] = 'UpdateUnsuccess';
				$leClassApp->RollBack_Trans();
			}
			else {
				// success
				$returnAry['success'] = true;
				$returnAry['returnMsgKey'] = 'UpdateSuccess';
				$leClassApp->Commit_Trans();
			}
			
			return $returnAry;
		}
	}
}
?>