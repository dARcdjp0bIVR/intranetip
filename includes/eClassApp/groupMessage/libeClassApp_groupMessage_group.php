<?php
// editing by Roy
/*
 * 2020-09-28 (Ray): add $yearClassID
 *
 * 2017-07-28 (Roy): add field $groupType
 * 
 * 2017-05-11 (Roy): add field $communicationMode
 * 
 * 2013-01-08 (Carlos): add field $sizeInBytes
 */

if (!defined("LIBECLASSAPP_GROUPMESSAGE_GROUP_DEFINED")) {
	define("LIBECLASSAPP_GROUPMESSAGE_GROUP_DEFINED", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	
	class libeClassApp_groupMessage_group extends libdbobject {
		var $groupId;
		var $code;
		var $nameEn;
		var $nameCh;
		var $communicationMode;
		var $groupType;
		var $recordStatus;
		var $dateInput;
		var $inputBy;
		var $dateModified;
		var $modifiedBy;
		var $yearClassID;

		function libeClassApp_groupMessage_group($objectId='') {
			parent::__construct('INTRANET_APP_MESSAGE_GROUP', 'GroupID', $this->returnFieldMappingAry(), $objectId);
		}
		
		function setGroupId($val) {
			$this->groupId = $val;
		}
		function getGroupId() {
			return $this->groupId;
		}
		
		function setCode($val) {
			$this->code = $val;
		}
		function getCode() {
			return $this->code;
		}
		
		function setNameEn($val) {
			$this->nameEn = $val;
		}
		function getNameEn() {
			return $this->nameEn;
		}
		
		function setNameCh($val) {
			$this->nameCh = $val;
		}
		function getNameCh() {
			return $this->nameCh;
		}
		
		function setCommunicationMode($val) {
			$this->communicationMode = $val;
		}
		function getCommunicationMode() {
			return $this->communicationMode;
		}
		
		function setGroupType($val) {
			$this->groupType = $val;
		}
		function getGroupType() {
			return $this->groupType;
		}
		
		function setRecordStatus($val) {
			$this->recordStatus = $val;
		}
		function getRecordStatus() {
			return $this->recordStatus;
		}
		
		function setDateInput($val) {
			$this->dateInput = $val;
		}
		function getDateInput() {
			return $this->dateInput;
		}
		
		function setInputBy($val) {
			$this->inputBy = $val;
		}
		function getInputBy() {
			return $this->inputBy;
		}
		
		function setDateModified($val) {
			$this->dateModified = $val;
		}
		function getDateModified() {
			return $this->dateModified;
		}
		
		function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		function getModifiedBy() {
			return $this->modifiedBy;
		}

		function setYearClassID($val) {
			$this->yearClassID = $val;
		}
		function getYearClassID() {
			return $this->yearClassID;
		}

		function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('GroupID', 'int', 'setGroupId', 'getGroupId');
			$fieldMappingAry[] = array('Code', 'str', 'setCode', 'getCode');
			$fieldMappingAry[] = array('NameEn', 'str', 'setNameEn', 'getNameEn');
			$fieldMappingAry[] = array('NameCh', 'str', 'setNameCh', 'getNameCh');
			$fieldMappingAry[] = array('CommunicationMode', 'int', 'setCommunicationMode', 'getCommunicationMode');
			$fieldMappingAry[] = array('GroupType', 'int', 'setGroupType', 'getGroupType');
			$fieldMappingAry[] = array('RecordStatus', 'int', 'setRecordStatus', 'getRecordStatus');
			$fieldMappingAry[] = array('DateInput', 'date', 'setDateInput', 'getDateInput');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('DateModified', 'date', 'setDateModified', 'getDateModified');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			$fieldMappingAry[] = array('YearClassID', 'int', 'setYearClassID', 'getYearClassID');
			return $fieldMappingAry;
		}

		function newRecordBeforeHandling() {
			global $indexVar;
			
			$this->setDateInput('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setDateModified('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			return true;
		}
		
		function updateRecordBeforeHandling() {
			global $indexVar;
			
			$this->setDateModified('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			return true;
		}
		
		function saveCloud() {
			global $intranet_root, $config_school_code;
			
			include_once($intranet_root."/includes/json.php");
			include_once($intranet_root."/includes/eClassApp/libeClassApp.php");
			$jsonObj = new JSON_obj();
			$leClassApp = new libeClassApp();
			
			$postParamAry = array();
			$postParamAry['RequestMethod'] = 'saveGroupMessage_group';
			$postParamAry['SchoolCode'] = $config_school_code;
			$postParamAry['GroupID'] = $this->getGroupId();
			$postParamAry['Code'] = $this->getCode();
			$postParamAry['NameEn'] = $this->getNameEn();
			$postParamAry['NameCh'] = $this->getNameCh();
			$postParamAry['CommunicationMode'] = $this->getCommunicationMode();
			$postParamAry['GroupType'] = $this->getGroupType();
			$postParamAry['RecordStatus'] = $this->getRecordStatus();
			$postJsonString = $jsonObj->encode($postParamAry);
			
			$headers = array('Content-Type: application/json; charset=utf-8', 'Expect:');
			$centralServerUrl = $leClassApp->getCurCentralServerUrl('groupMessage');
			
			session_write_close();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
			curl_setopt($ch, CURLOPT_URL, $centralServerUrl);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postJsonString);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
			$responseJson = curl_exec($ch);
			curl_close($ch);
			
//			debug_pr($responseJson);
//			die();
			
			$responseJson = standardizeFormPostValue($responseJson);
			$responseJsonAry = $jsonObj->decode($responseJson);
			$saveSuccess = ($responseJsonAry['MethodResult']=='1')? true : false;
			
			return $saveSuccess;
		}
	}
}
?>