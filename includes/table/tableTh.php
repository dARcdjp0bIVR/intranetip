<?php
if (!defined("TABLETH_DEFINED")) {

	class tableTh extends tableCellBase {
		
		function tableTh($content, $width='', $colspan='', $rowspan='') {
			$this->setTagName('th');
			$this->setContent($content);
			$this->setWidth($width);
			$this->setColspan($colspan);
			$this->setRowspan($rowspan);
		}
	}
}
?>