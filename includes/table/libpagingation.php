<?
# modifying : ivan

if (!defined("LIBPAGINATION"))         // Preprocessor directives
{
	define("LIBPAGINATION",true);

	class libpagination{
		private $totalNumOfResult;
		private $numPerPage;
		private $curPage;
		
		private $submitMode;
		private $submitFormID;
		private $submitHref;
		private $submitFunctionName;
		private $submitExtraPara;
		
		public function libpagination() {
			$this->setTotalNumOfResult(0);
			$this->setNumPerPage(100);
			$this->setCurPage(1);
			$this->setJsFunctionName('');
		}
		
		public function getTotalNumOfResult() {
			return $this->totalNumOfResult;
		}
		public function setTotalNumOfResult($val) {
			$this->totalNumOfResult = $val;
		}
		
		public function getNumPerPage() {
			return $this->numPerPage;
		}
		public function setNumPerPage($val) {
			$this->numPerPage = $val;
		}
		
		public function getCurPage() {
			return $this->curPage;
		}
		public function setCurPage($val) {
			$this->curPage = $val;
		}
		
		public function getJsFunctionName() {
			return $this->submitFunctionName;
		}
		public function setJsFunctionName($val) {
			$this->submitFunctionName = $val;
		}
		
		public function setSubmitMode($str) {
    		$this->submitMode = $str;
    	}
    	public function getSubmitMode() {
    		return $this->submitMode;
    	}
    	
    	public function setSubmitFormID($str) {
    		$this->submitFormID = $str;
    	}
    	private function getSubmitFormID() {
    		return $this->submitFormID;
    	}
    	
    	public function setSubmitHref($str) {
    		$this->submitHref = $str;
    	}
    	public function getSubmitHref() {
    		return $this->submitHref;
    	}
    	
    	public function setSubmitFunctionName($str) {
    		$this->submitFunctionName = $str;
    	}
    	public function getSubmitFunctionName() {
    		return $this->submitFunctionName;
    	}
    	
    	public function setSubmitExtraPara($str) {
    		$this->submitExtraPara = $str;
    	}
    	public function getSubmitExtraPara() {
    		return $this->submitExtraPara;
    	}
		
		
		
		
		private function getMaxPageNum() {
			$totalNumOfResult = $this->getTotalNumOfResult();
			$numPerPage = $this->getNumPerPage();
			
			$maxPageNum = 1;
			if ($numPerPage > 0) {
				$maxPageNum = ceil($totalNumOfResult / $numPerPage);
			}
			
			return $maxPageNum;
		}
		
		private function getJsAction($action, $curPage='', $numPerPage='') {
			$curPage = ($curPage=='')? $this->getCurPage() : $curPage;
    		$numPerPage = ($numPerPage=='')? $this->getNumPerPage() : $numPerPage;
    		$submitMode = $this->getSubmitMode();
    		$submitFormID = $this->getSubmitFormID();
    		$submitHref = $this->getSubmitHref();
    		$submitFunctionName = $this->getSubmitFunctionName();
    		$submitExtraPara = $this->getSubmitExtraPara();
    		
    		$x = '';
    		$x .= " $action=\"reloadResultTable($curPage, $numPerPage, '', '', '$submitMode', '$submitFormID', '$submitHref', '$submitFunctionName', '$submitExtraPara');\" "; 
    					
			return $x;
		}
		
		public function getPaginationHtml() {
			global $image_path;
			
			$x = '';
			
			if ($this->getTotalNumOfResult() > 0) {
				$x .= '<div class="common_table_bottom">'."\r\n";
					$x .= '<div class="record_no">'.$this->getRecordRange().'</div>'."\r\n";
					$x .= '<div class="page_no">'."\r\n";
						$x .= '<span>'.$this->getGoFirstPageButton().'</span>'."\r\n";
		                $x .= '<span>'.$this->getGoPreviousPageButton().'</span>'."\r\n";
						$x .= '<span>'.$this->getGoPageSelection().'</span>'."\r\n";
						$x .= '<span>'.$this->getGoNextPageButton().'</span>'."\r\n";
						$x .= '<span>'.$this->getGoLastPageButton().'</span>'."\r\n";
						$x .= '<span>'."\r\n";
							$x .= '|'."\r\n";
							$x .= '<img src="'.$image_path.'/space.gif" width="2" height="10" border="0">'."\r\n";
	                    	$x .= $this->getNumPerPageSelection();
						$x .= '</span>'."\r\n";
					 $x .= '</div>'."\r\n";
	        	$x .= '</div>'."\r\n";
			}
        	
        	return $x;
		}
		
		private function getRecordRange() {
			global $Lang;
			
			$curPage = $this->getCurPage();
			$numPerPage = $this->getNumPerPage();
			$totalNumOfResult = $this->getTotalNumOfResult();
			
			$startIndex = (($curPage - 1) * $numPerPage) + 1;
			$endIndex = min($totalNumOfResult, ($startIndex + $numPerPage) - 1);
	        
	        $x = '';
	        $x = $Lang['General']['Record'].' '.$startIndex.' - '.$endIndex.', '.$Lang['StudentRegistry']['TotalRecord'].' '.$totalNumOfResult."\r\n";
	        
            return $x;
		}
		
		private function getGoFirstPageButton() {
			global $image_path, $image_pre;
			
			$curPage = $this->getCurPage();
			$numPerPage = $this->getNumPerPage();
			$onclickJsFunctionName = $this->getJsFunctionName();
			
			$x = '';
			if ($curPage == 1) {
				$x .= '<a class="first">&nbsp;</a>'."\r\n";
			}
			else {
				$targetPage = 1;
				$onclick = $this->getJsAction('onclick', $targetPage);
				
				$x .= '<a class="first" href="javascript:void(0);" '.$onclick.'></a>'."\r\n";
			}
								
			return $x;
		}
		
		private function getGoPreviousPageButton() {
			global $image_path, $image_pre;
			
			$curPage = $this->getCurPage();
			$numPerPage = $this->getNumPerPage();
			$onclickJsFunctionName = $this->getJsFunctionName();
			
			$x = '';
			if ($curPage == 1) {
				$x .= '<a class="prev">&nbsp;</a>'."\r\n";
			}
			else {
				$targetPage = $curPage - 1;
				$onclick = $this->getJsAction('onclick', $targetPage);
				
				$x .= '<a class="prev" href="javascript:void(0);" '.$onclick.'></a>'."\r\n";
			}
								
			return $x;
		}
		
		private function getGoNextPageButton() {
			global $image_path, $image_pre;
			
			$curPage = $this->getCurPage();
			$numPerPage = $this->getNumPerPage();
			$totalNumOfPages = $this->getMaxPageNum();
			
			$x = '';
			if ($curPage == $totalNumOfPages) {
				// Last Page
				$x .= '<a class="next">&nbsp;</a>'."\r\n";
			}
			else {
				$targetPage = $curPage + 1;
				$onclick = $this->getJsAction('onclick', $targetPage);
				
				$x .= '<a class="next" href="javascript:void(0);" '.$onclick.'></a>'."\r\n";
			}
								
			return $x;
		}
		
		private function getGoLastPageButton() {
			global $image_path, $image_pre;
			
			$curPage = $this->getCurPage();
			$numPerPage = $this->getNumPerPage();
			$totalNumOfPages = $this->getMaxPageNum();
			
			$x = '';
			if ($curPage == $totalNumOfPages) {
				// Last Page
				$x .= '<a class="last">&nbsp;</a>'."\r\n";
			}
			else {
				$targetPage = $totalNumOfPages;
				$onclick = $this->getJsAction('onclick', $targetPage);
				
				$x .= '<a class="last" href="javascript:void(0);" '.$onclick.'></a>'."\r\n";
			}
								
			return $x;
		}
		
		private function getGoPageSelection() {
			global $Lang;
			
			$curPage = $this->getCurPage();
			$totalNumOfPages = $this->getMaxPageNum();
			$onchange = $this->getJsAction('onchange', 'this.value');
			
			$x = '';
			$x .= $Lang['StudentRegistry']['PageName1'];
			$x .= " ";
			$x .= '<select '.$onchange.'>'."\r\n";
				for ($i=1; $i<=$totalNumOfPages; $i++) {
					$thisSelected = ($i==$curPage)? 'selected' : '';
					$x .= '<option value="'.$i.'" '.$thisSelected.'>'.$i.'</option>'."\r\n";
				}
			$x .= '</select>'."\r\n";
			$x .= $Lang['General']['PageNumber'];
			
			return $x;
		}
		
		private function getNumPerPageSelection() {
			global $i_general_EachDisplay, $i_general_PerPage;
			
			$numPerPage = $this->getNumPerPage();
			$onchange = $this->getJsAction('onchange', $CurPage='1', 'this.value');
			
			$x = '';
			$x .= $i_general_EachDisplay;
			$x .= ' ';
			$x .= '<select '.$onchange.'>'."\r\n";
				$x .= '<option value="10" '.($numPerPage==10? "SELECTED":"").'>10</option>'."\r\n";
				$x .= '<option value="20" '.($numPerPage==20? "SELECTED":"").'>20</option>'."\r\n";
				$x .= '<option value="30" '.($numPerPage==30? "SELECTED":"").'>30</option>'."\r\n";
				$x .= '<option value="40" '.($numPerPage==40? "SELECTED":"").'>40</option>'."\r\n";
				$x .= '<option value="50" '.($numPerPage==50? "SELECTED":"").'>50</option>'."\r\n";
				$x .= '<option value="60" '.($numPerPage==60? "SELECTED":"").'>60</option>'."\r\n";
				$x .= '<option value="70" '.($numPerPage==70? "SELECTED":"").'>70</option>'."\r\n";
				$x .= '<option value="80" '.($numPerPage==80? "SELECTED":"").'>80</option>'."\r\n";
				$x .= '<option value="90" '.($numPerPage==90? "SELECTED":"").'>90</option>'."\r\n";
				$x .= '<option value="100" '.($numPerPage==100? "SELECTED":"").'>100</option>'."\r\n";
			$x .= '</select>'."\r\n";
			$x .= ' ';
			$x .= $i_general_PerPage;
			
			return $x;
		}
	}
}
?>