<?php
if (!defined("TABLEBASE_DEFINED")) {
	
	include_once $PATH_WRT_ROOT.'includes/table/tableTr.php';
	include_once $PATH_WRT_ROOT.'includes/table/tableCellBase.php';
	include_once $PATH_WRT_ROOT.'includes/table/tableTh.php';
	include_once $PATH_WRT_ROOT.'includes/table/tableTd.php';

	class tableBase {
		var $headerTrAry;
		var $bodyTrAry;
		var $applyConvertSpecialChars;
		
		function tableBase() {
			$this->headerTrAry = array();
			$this->bodyTrAry = array();
			$this->setApplyConvertSpecialChars(true);
		}
		
		function getHeaderTrAry() {
			return $this->headerTrAry;
		}
		function getBodyTrAry() {
			return $this->bodyTrAry;
		}
		
		function addHeaderTr($trObj) {
			$this->headerTrAry[] = $trObj;
		}
		function addBodyTr($trObj) {
			$this->bodyTrAry[] = $trObj;
		}
		
		function setApplyConvertSpecialChars($val) {
			$this->applyConvertSpecialChars = $val;
		}
		function getApplyConvertSpecialChars() {
			return $this->applyConvertSpecialChars;
		}
		
		function returnCurHeaderTrObjIndex() {
			return count($this->getHeaderTrAry()) - 1;
		}
		function returnCurBodyTrObjIndex() {
			return count($this->getBodyTrAry()) - 1;
		}
		
		function addHeaderCell($cellObj) {
			$index = $this->returnCurHeaderTrObjIndex();
			$cellObj->setApplyConvertSpecialChars($this->getApplyConvertSpecialChars());
			
			$this->headerTrAry[$index]->addCell($cellObj);
		}
		function addBodyCell($cellObj) {
			$index = $this->returnCurBodyTrObjIndex();
			$cellObj->setApplyConvertSpecialChars($this->getApplyConvertSpecialChars());
			
			$this->bodyTrAry[$index]->addCell($cellObj);
		}
		
		function returnTotalNumOfCol() {
			$headerTrAry = $this->getHeaderTrAry();
			$numOfTr = count($headerTrAry);
			
			$colNumAry = array(0);
			for ($i=0; $i<$numOfTr; $i++) {
				$colNumAry[] = $headerTrAry[$i]->returnTotalNumOfCol();
			}
			
			return max($colNumAry);
		}
	}
}
?>