<?php
if (!defined("TABLE_COMMONTABLE_DEFINED")) {
	
	include_once $PATH_WRT_ROOT.'includes/table/tableBase.php';
	
	class table_commonTable extends tableBase {
		var $tableType;			// '', 'edit', 'view'
		var $tableExtraClass;
		var $tableId;
		
		function table_commonTable() {
			$this->tableBase();
		}
		
		function setTableType($val) {
			$this->tableType = $val;
		}
		function getTableType() {
			return $this->tableType;
		}
		function setTableExtraClass($val) {
			$this->tableExtraClass = $val;
		}
		function getTableExtraClass() {
			return $this->tableExtraClass;
		}
		function setTableId($val) {
			$this->tableId = $val;
		}
		function getTableId() {
			return $this->tableId;
		}
		
		function returnTableTypeCssClass() {
			switch ($this->getTableType()) {
				case 'edit':
					$cssClass = 'edit_table_list_v30';
					break;
				case 'view':
					$cssClass = 'view_table_list_v30';
					break;
				default:
					$cssClass = '';
			}
			
			return $cssClass;
		}
		
		function returnHtml() {
			global $Lang;
			
			$tableId = $this->getTableId();
			if ($tableId != '') {
				$idAttr = ' id="'.$tableId.'" ';
			}
			
			$tableTypeCss = $this->returnTableTypeCssClass();
			$tableExtraClass = $this->getTableExtraClass();
						
			$headerTrAry = $this->getHeaderTrAry();
			$numOfHeaderTr = count($headerTrAry);
			$bodyTrAry = $this->getBodyTrAry();
			$numOfBodyTr = count($bodyTrAry);
			
			$x = '';
			$x .= '<table '.$idAttr.' class="common_table_list_v30 '.$tableTypeCss.' '.$tableExtraClass.'">'."\n";
				$x .= '<thead>'."\n";
					for ($i=0; $i<$numOfHeaderTr; $i++) {
						$x .= $headerTrAry[$i]->returnHtml();
					}
				$x .= '</thead>'."\n";
				$x .= '<tbody>'."\n";
					if ($numOfBodyTr == 0) {
						$x .= '<tr><td colspan="'.$this->returnTotalNumOfCol().'" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
					}
					else {
						for ($i=0; $i<$numOfBodyTr; $i++) {
							$x .= $bodyTrAry[$i]->returnHtml();
						}
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";	
			return $x;
		}
	}
}
?>