<?php
if (!defined("TABLETR_DEFINED")) {

	class tableTr {
		var $cellAry;
		
		function tableTr() {
			$this->cellAry = array();
		}
		
		function getCellAry() {
			return $this->cellAry;
		}
		
		function addCell($cellObj) {
			$this->cellAry[] = $cellObj;
		}
		
		function returnTotalNumOfCol() {
			// may need to cater colspan later
			return count($this->getCellAry());
		}
		
		function returnHtml() {
			$cellAry = $this->getCellAry();
			$numOfCell = count($cellAry);
			
			$x = '';
			$x .= '<tr>'."\n";
				for ($i=0; $i<$numOfCell; $i++) {
					$x .= $cellAry[$i]->returnHtml();
				}
			$x .= '</tr>'."\n";
			
			return $x;
		}
	}
}
?>