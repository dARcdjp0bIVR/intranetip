<?php
# modifying : ivan
if (!defined("LIBTABLE_MGR"))         // Preprocessor directives
{
	define("LIBTABLE_MGR", true);

	class libtable_mgr{
		private $objDb;
		private $objSqlGenerator;
		private $objLayout;
		private $objPagination;
		
		private $curPage;
		private $numPerPage;
		private $columnInfoAry;
		private $sortColumnIndex;
		private $sortOrder;			// 0: asc, 1: desc
		private $extraSortInfoAry;	// $extraSortInfoAry[0]['dbField'] = 'xxx'; $extraSortInfoAry[0]['sortOrder'] = 0;
		private $applyLimit;
		private $showResultNumber;
		private $resultNumberColumnExtraAttr;
		
		private $divTemplate;
		private $dbTableReplaceCode;
		private $paginationReplaceCode;
		
		private $resultSet;
		private $headerTrClassAry;
		private $dataTrClassAry;
		
		private $submitMode;			// 1:document sumbit mode, 2: href mode, 3: function mode
		private $submitFormID;			// for document sumbit mode
		private $submitHref;			// for href mode
		private $submitFunctionName;	// for function mode
		private $submitExtraPara;
		
		public function __construct() {
			$this->objDb = new libdb();
			$this->objSqlGenerator = new libtable_sqlGenerator();
			$this->objLayout = null;
			$this->objPagination = null;
			
			$this->setApplyLimit(false);
			$this->setDbTableReplaceCode('{{{dbTable}}}');
			$this->setPaginationReplaceCode('{{{pagination}}}');
			$this->resetColumnInfoAry();
			$this->setShowResultNumber(true);
    	}
    	
    	
    	#######################################################################
    	################# [Start] Get / Set fucntions #########################
    	#######################################################################
    	public function setObjLayout($obj) {
    		$this->objLayout = $obj;
    		
    		$this->objLayout->setSortColumnIndex($this->getSortColumnIndex());
    		$this->objLayout->setSortOrder($this->getSortOrder());
    		$this->objLayout->setShowResultNumber($this->getShowResultNumber());
    		$this->objLayout->setResultNumberColumnExtraAttr($this->getResultNumberColumnExtraAttr());
    		$this->objLayout->setColumnInfoAry($this->getColumnInfoAry());
    		$this->objLayout->setHeaderTrClassAry($this->getHeaderTrClassAry());
    		$this->objLayout->setDataTrClassAry($this->getDataTrClassAry());
    		$this->objLayout->setCurPage($this->getCurPage());
    		$this->objLayout->setNumPerPage($this->getNumPerPage());
    		
    		$this->objLayout->setSubmitMode($this->getSubmitMode());
    		$this->objLayout->setSubmitFormID($this->getSubmitFormID());
    		$this->objLayout->setSubmitHref($this->getSubmitHref());
			$this->objLayout->setSubmitFunctionName($this->getSubmitFunctionName());
			$this->objLayout->setSubmitExtraPara($this->getSubmitExtraPara());
    	}
    	
    	public function setObjPagination($obj) {
    		$this->setApplyLimit(true);
    		
    		$this->objPagination = $obj;
    		$this->objPagination->setCurPage($this->getCurPage());
    		$this->objPagination->setNumPerPage($this->getNumPerPage());
    		
    		$this->objPagination->setSubmitMode($this->getSubmitMode());
    		$this->objPagination->setSubmitFormID($this->getSubmitFormID());
			$this->objPagination->setSubmitHref($this->getSubmitHref());
			$this->objPagination->setSubmitFunctionName($this->getSubmitFunctionName());
			$this->objPagination->setSubmitExtraPara($this->getSubmitExtraPara());
    	}
    	
    	public function setCurPage($int) {
    		$this->curPage = $int;
    		$this->objSqlGenerator->setCurPage($int);
    		
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setCurPage($int);
    		}
    		
    		if ($this->isPaginationObjectExist()) {
    			$this->objPagination->setCurPage($int);
    		}
    	}
    	public function getCurPage() {
    		return $this->curPage;
    	}
    	
    	public function setNumPerPage($int) {
    		$this->numPerPage = $int;
    		$this->objSqlGenerator->setNumPerPage($int);
    		
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setNumPerPage($int);
    		}
    		
    		if ($this->isPaginationObjectExist()) {
    			$this->objPagination->setNumPerPage($int);
    		}
    	}
    	public function getNumPerPage() {
    		return $this->numPerPage;
    	}
    	
    	public function setSortColumnIndex($int) {
    		$this->sortColumnIndex = $int;
    		$this->objSqlGenerator->setSortColumnIndex($int);
    		
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setSortColumnIndex($int);
    		}
    	}
    	public function getSortColumnIndex() {
    		return $this->sortColumnIndex;
    	}
    	
    	public function setSortOrder($int) {
    		$this->sortOrder = $int;
    		$this->objSqlGenerator->setSortOrder($int);
    		
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setSortOrder($int);
    		}
    	}
    	public function getSortOrder() {
    		return $this->sortOrder;
    	}
    	
    	private function setApplyLimit($bool) {
    		$this->applyLimit = $bool;
    	}
    	private function getApplyLimit() {
    		return $this->applyLimit;
    	}
    	
    	private function addColumnInfoAry($ary) {
    		$this->columnInfoAry[] = $ary;
    	}
    	private function resetColumnInfoAry() {
    		$this->columnInfoAry = array();
    	}
    	private function getColumnInfoAry() {
    		
    		return $this->columnInfoAry;
    	}
    	
    	private function addExtraSortInfoAry($ary) {
    		$this->extraSortInfoAry[] = $ary;
    	}
    	private function resetExtraSortInfoAry() {
    		$this->extraSortInfoAry = array();
    	}
    	private function getExtraSortInfoAry() {
    		return $this->extraSortInfoAry;
    	}
    	
    	public function setShowResultNumber($bool) {
    		$this->showResultNumber = $bool;
    		
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->showResultNumber($bool);
    		}
    	}
    	public function getShowResultNumber() {
    		return $this->showResultNumber;
    	}
    	
    	public function setResultNumberColumnExtraAttr($str) {
    		$this->resultNumberColumnExtraAttr = $str;
    		
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setResultNumberColumnExtraAttr($str);
    		}
    	}
    	public function getResultNumberColumnExtraAttr() {
    		return $this->resultNumberColumnExtraAttr;
    	}
    	
    	public function setDivTemplate($str) {
    		$this->divTemplate = $str;
    	}
    	public function getDivTemplate() {
    		return $this->divTemplate;
    	}
    	
    	private function setDbTableReplaceCode($str) {
    		$this->dbTableReplaceCode = $str;
    	}
    	public function getDbTableReplaceCode() {
    		return $this->dbTableReplaceCode;
    	}
    	
    	private function setPaginationReplaceCode($str) {
    		$this->paginationReplaceCode = $str;
    	}
    	public function getPaginationReplaceCode() {
    		return $this->paginationReplaceCode;
    	}
    	
    	public function setHeaderTrClassAry($ary) {
    		$this->headerTrClassAry = $ary;
    		
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setHeaderTrClassAry($ary);
    		}
    	}
    	private function getHeaderTrClassAry() {
    		return $this->headerTrClassAry;
    	}
    	
    	public function setDataTrClassAry($ary) {
    		$this->dataTrClassAry = $ary;
    		
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setDataTrClassAry($ary);
    		}
    	}
    	private function getDataTrClassAry() {
    		return $this->dataTrClassAry;
    	}
    	
    	public function setSubmitMode($mode, $target='') {
    		$this->submitMode = $mode;
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setSubmitMode($mode);
    		}
    		
    		switch ($mode) {
    			case 1:		// document submit mode
    				$this->setSubmitFormID = $target;
    				if ($this->isLayoutObjectExist()) {
    					$this->objLayout->setSubmitFormID($target);
    				}
    				break;
    			case 2:		// href mode
    				$target = ($target=='')? curPageURL($withQueryString=0) : $target;	// if there are no specific url, use the current page
    				$this->setSubmitHref($target);
    				if ($this->isLayoutObjectExist()) {
    					$this->objLayout->setSubmitHref($target);
    				}
    				break;
    			case 3:		// function mode
    				$this->setSubmitFunctionName = $target;
    				if ($this->isLayoutObjectExist()) {
    					$this->objLayout->setSubmitFunctionName($target);
    				}
    				break;
    			
    		}
    	}
    	public function getSubmitMode() {
    		return $this->submitMode;
    	}
    	
    	public function setSubmitFormID($str) {
    		$this->submitFormID = $str;
    	}
    	public function getSubmitFormID() {
    		return $this->submitFormID;
    	}
    	
    	public function setSubmitHref($str) {
    		$this->submitHref = $str;
    	}
    	public function getSubmitHref() {
    		return $this->submitHref;
    	}
    	
    	public function setSubmitFunctionName($str) {
    		$this->submitFunctionName = $str;
    	}
    	public function getSubmitFunctionName() {
    		return $this->submitFunctionName;
    	}
    	
    	public function setSubmitExtraPara($str) {
    		$this->submitExtraPara = $str;
    	}
    	public function getSubmitExtraPara() {
    		return $this->submitExtraPara;
    	}
    	#######################################################################
    	################## [End] Get / Set fucntions ##########################
    	#######################################################################
    	
    	private function isLayoutObjectExist() {
    		return ($this->objLayout==null)? false : true;
    	}
    	
    	private function isPaginationObjectExist() {
    		return ($this->objPagination==null)? false : true;
    	}
    	
    	public function addColumn($dbField, $isSortable, $uiTitle, $width='', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='', $rowspan=1, $colspan=1, $rowLevel=1) {
    		$infoArr = get_defined_vars();
    		$this->addColumnInfoAry($infoArr);
    		
    		$this->objSqlGenerator->setColumnInfoAry($this->getColumnInfoAry());
    		
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setColumnInfoAry($this->getColumnInfoAry());
    		}
    	}
    	public function resetColumn() {
    		$this->resetColumnInfoAry();
    		
    		$this->objSqlGenerator->setColumnInfoAry($this->getColumnInfoAry());
    		$this->objLayout->setColumnInfoAry($this->getColumnInfoAry());
    	}
    	
    	public function addExtraSort($dbField, $sortOrder) {
    		$infoArr = get_defined_vars();
    		$this->addExtraSortInfoAry($infoArr);
    		
    		$this->objSqlGenerator->setExtraSortInfoAry($this->getExtraSortInfoAry());
    	}    	
    	public function restExtraSort() {
    		$this->resetExtraSortInfoAry();
    		
    		$this->objSqlGenerator->setExtraSortInfoAry($this->getExtraSortInfoAry());
    	}
    	
    	public function display() {
    		$divTemplate = $this->getDivTemplate();
    		$dbTableHtml = $this->generateDbTable();
    		$paginationHtml = $this->generatePagination();
    		
    		$html = '';
    		if ($divTemplate== '') {
    			// If no template, samply output table and pagination
    			$html = $dbTableHtml.$paginationHtml;
    		}
    		else {
    			// If have template, place the table and pagination in the specific position
    			$html = $divTemplate;
    			$html = str_replace($this->getDbTableReplaceCode(), $dbTableHtml, $html);
    			$html = str_replace($this->getPaginationReplaceCode(), $paginationHtml, $html);
    		}
    		
    		$html .= $this->generateHiddenFormField();
    		
    		return $html;
    	}
    	
    	private function generateHiddenFormField() {
    		$x = '';
    		
    		$x .= '<input type="hidden" id="curPage" name="curPage" value="'.$this->getCurPage().'" />'."\r\n";
    		$x .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$this->getNumPerPage().'" />'."\r\n";
    		$x .= '<input type="hidden" id="sortColumnIndex" name="sortColumnIndex" value="'.$this->getSortColumnIndex().'" />'."\r\n";
    		$x .= '<input type="hidden" id="sortOrder" name="sortOrder" value="'.$this->getSortOrder().'" />'."\r\n";
    		
    		return $x;
    	}
    	
    	#######################################################################
    	############ [Start] SqlGenerator-related fucntions ###################
    	#######################################################################
    	public function setSql($sql) {
    		$this->objSqlGenerator->setSql($sql);
    	}
    	
    	public function getGeneratedSql() {
    		$this->objSqlGenerator->setApplyLimit($this->getApplyLimit());
    		return $this->objSqlGenerator->generateSql();
    	}
    	
    	public function getResultSet() {
    		$this->resultSet = $this->objDb->returnResultSet($this->getGeneratedSql());
    		return $this->resultSet;
    	}
    	
    	public function getFullResultSetSize() {
    		$originalApplyLimit = $this->getApplyLimit();
    		
    		$this->setApplyLimit(false);
    		$fullResultSet = $this->objDb->returnResultSet($this->getGeneratedSql());
    		$this->setApplyLimit($originalApplyLimit);
    		
    		return count($fullResultSet);
    	}
    	#######################################################################
    	############ [End] SqlGenerator-related fucntions ###################
    	#######################################################################
    	
    	
    	#######################################################################
    	############### [Start] Layout-related fucntions ######################
    	#######################################################################
    	private function generateDbTable() {
    		$resultSet = $this->getResultSet();
    		$html = '';
    		if ($this->isLayoutObjectExist()) {
    			$this->objLayout->setResultSet($resultSet);
    			$html .= $this->objLayout->display();
    		}
    		
    		return $html;
    	}
    	#######################################################################
    	############# [End] Layout-related fucntions ####################
    	#######################################################################
    	
    	
    	#######################################################################
    	############# [Start] Pagination-related fucntions ####################
    	#######################################################################
    	private function setResultSetSize() {
    		if ($this->isPaginationObjectExist()) {
    			$this->objPagination->setTotalNumOfResult(count($this->getResultSet()));
    		}
    	}
    	
    	private function generatePagination() {
    		$html = '';
    		if ($this->isPaginationObjectExist()) {
    			$this->objPagination->setTotalNumOfResult($this->getFullResultSetSize());
    			$html .= $this->objPagination->getPaginationHtml();
    		}
    		
    		return $html;
    	}
    	#######################################################################
    	############# [End] Pagination-related fucntions ####################
    	#######################################################################
	}
}
?>