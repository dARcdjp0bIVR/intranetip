<?php
if (!defined("TABLETD_DEFINED")) {

	class tableTd extends tableCellBase {
		
		function tableTd($content, $width='', $colspan='', $rowspan='') {
			$this->setTagName('td');
			$this->setContent($content);
			$this->setWidth($width);
			$this->setColspan($colspan);
			$this->setRowspan($rowspan);
		}
	}
}
?>