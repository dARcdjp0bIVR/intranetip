<?php
// Editing by

/**
 * ************************************************************************
 * Modification log
 * 20201030 Bill:   [2020-0708-0950-12308]
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI(), display cust wordings in table header     ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher'])
 * 20200512 Bill:
 * - modified Get_Master_Report(), to support Print Date display & export   ($eRCTemplateSetting['Report']['MasterReport']['DisplayPrintReportDateTime'])
 * 20200428 Bill:   [2019-0923-1726-31289]
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI(), Get_Add_Student_Existing_Comment_Layer_UI(), Get_Comment_Div_Selection(), for manual adjust comment + conduct grade filtering
 * - added Get_Conduct_Grade_Calculation_Overview_UI(), Get_Conduct_Grade_Calculation_Overview_Table(), Get_Conduct_Grade_Modify_Edit_UI(), Get_Conduct_Grade_Modify_Table(), for student conduct grade
 * 20200407 Philips: [2019-1120-1355-05289]
 * - modified Get_Mgmt_DataHandling_Tab_Array() add transfer to websams
 * 20200226 Philips: [2020-0122-1154-15073]
 * - modified Get_SubjectAcademicResultReport_right_UI() to use Highchart instead of flash chart
 * 20200221 Bill:
 * - modified Get_SubjectAcademicResultReport(), to fix page break problem
 * 20191101 Bill:   [2019-1030-0914-49066]
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI(), to set up main / competition subject from settings
 * 20190711 Bill:   [2019-0709-1158-26235]
 * - modified Get_SubjectAcademicResultReport(), to update layout of Subject Academic Result
 * 20190708 Bill:   [2019-0705-1034-09066]
 * - added Get_Comment_Div_Selection(), to return comment selection html
 * - modified Get_Add_Student_Existing_Comment_Layer_UI()
 * 20190628 Bill:   [2019-0628-1016-01207]
 * - modified Get_Management_ClassTeacherComment_CommentView_ChooseStudent_UI(), to fix cannot display search div
 * 20190502 Bill:
 * - modified Get_Management_OtherInfo_Import_Table(), Class Selection > Change 'YearClassID' to 'YearClassSelect' to prevent IntegerSafe()
 * 20190301 Bill:	[2018-1130-1440-05164]
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI
 * - added Get_Add_Student_Existing_Comment_Layer_UI(), Get_Student_New_Comment_Div_UI()
 * - for Handling for Comment Selection ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
 * - for Previous Term Report Comment Display ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
 * 20190225 Bill [2019-0221-1032-56207]
 * - modified Get_HKUGA_Grade_List_TableGet_HKUGA_Grade_List_Table(), to display Achievement of case subject
 * 20190204 Bill [2018-0327-1517-24164]
 * - modified Get_SubjectAcademicResultReport_left_UI(), fix report layout display in IE
 * 20180723 Bill [2017-0901-1525-31265]
 * - modified Get_HKUGA_Grade_List_Index_UI(), allow View Group to select all options
 * 20180628 Bill [2017-1204-1601-38164]
 * - modified Get_Master_Report_Index_UI(), added active year selection
 * - added Get_eRC_Active_Year_Selection()
 * 20180306 Isaac [2017-1127-1218-28164]
 * - modified Get_Management_ClassTeacherComment_CommentView_ChooseStudent_UI() to apply the new user selection plugin.
 * 20171129 Bill [2017-1127-1218-28164]
 * - modified Get_Master_Report_Index_UI(), to add Option - Position in Stream ($eRCTemplateSetting['MasterReport']['AlwaysApplyOrderMeritStream'])
 * 20171006 Bill [2017-0907-1704-15258]
 * - modified Get_Import_Class_Teacher_Comment_Step1_UI(), Get_Import_Class_Teacher_Comment_Step2_UI(), Get_Import_Class_Teacher_Comment_Step3_UI(), to support new import mode - Append to original comment
 * 20170606 Bill [2017-0605-1627-50066]
 * - modified Get_Master_Report(), to return correct Class Name / Subject Group Name for customized Master Report
 * 20170519 Bill
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI(), Get_Other_Info_Overview_UI(), Get_Other_Info_Edit_UI(), Get_Other_Info_Edit_Table(), to support View Group access (view only)
 * 20170331 Bill [2017-0109-1818-40164]
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI(), to fix demerit checking for conduct grade selection limit
 * 20170308 Villa [2016-1130-1238-56240]
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI() - Add Show/ Hide Student ProPic function
 * 20170217 Bill
 * - modified GenReportList(), to display export button in Reports > Generate & Print Report Card ($eRCTemplateSetting['Report']['BatchExportReport'])
 * 20170214 Bill [2017-0210-1150-46236]
 * - modified Get_Master_Report(), to remove unwanted slashes in Subject Teacher's Comment
 * 20170207 Villa
 * - modified Get_HKUGA_Grade_List_Table(), add check to check how many YG/EG/CA need to print replacing handcode last 3
 * 20161208 Bill
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI(), fixed cannot perform fail subject checking for pui ching cust
 * 20161205 Bill
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI(), fixed js error by changing onkeypress to onkeydown for auto fill-in textbox
 * 20161124 Bill [2016-1031-1053-34096]
 * - modified Get_Master_Report(), display customize report title ($eRCTemplateSetting['MasterReport']['DisplayCustReportTitle'])
 * 20161123 Bill [2015-1104-1130-08164]
 * - modified GenReportList(), always show generate button and data input reminder ($eRCTemplateSetting['AllowGenerateAtAnyTimeWithMsg'])
 * 20160810 Bill [2015-1104-1130-08164]
 * - modified Get_Edit_Award_Student_Table_By_Generated_Result(), to exclude students that conduct grade received not high enough (Student & Award View)
 * - modified Get_Setting_Conduct_DBTable(), to customize conduct display order
 * 20160630 Bill [2015-1104-1130-08164]
 * Pui Ching (Macau) eRC Cust
 * - added Get_Import_Awards_Table(), display table for all awards (not involved in award generation process)
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI(), added Get_ExtraInfo_Conduct_Selection_Disable_Option() to disable some conduct option (except eReportCard admin)
 * 20160519 Bill [2016-0406-1633-32096]
 * - modified Get_Master_Report_Index_UI(), Get_Master_Report(), Get_Master_Report_CSS()
 * General
 * - Support display chinese title of Other Info Options
 * Sao Jose (Macau) Master Report Cust
 * - added Unit to Grand Mark Options
 * - display empty cell after Student Display Options for left students
 * - added footer to report
 * 20160427 Bill [2016-0330-1524-42164]
 * - modified GenReportList(), to add loading message to UI
 * 20160415 Ivan [2016-0415-1139-22206] [ip.2.5.7.4.1]
 * - modified Get_Import_Student_AwardText_Step1_UI() to add import remarks for award code
 * 20160128 Bill [2016-0128-1008-07066]
 * - modified Get_Master_Report(), to display multiple Other Info data
 * 20160120 Bill [2015-0421-1144-05164]
 * - added Get_Settings_Form_Main_Subject_UI(), Get_Form_Main_Subject_Table() ($eRCTemplateSetting['Settings']['FormMainSubject'])
 * - modified Get_Management_PersonalChar_Edit_UI() ($eRCTemplateSetting['PersonalChar']['ShowStudentDetails'])
 * 20151015 Bill [2015-1013-1434-16164]
 * - modified GenReportList(), Get_Report_Card_Type_Display() - for BIBA KG report type display
 * 20150820 Bill [2014-0918-1608-33164]
 * - modified Get_Other_Info_Edit_Table(), to add new input type - char
 * 20150722 Bill [2015-0413-1359-48164]
 * - modified GenReportList(), Get_Report_Card_Type_Display() - for BIBA report type display
 * 20150717 Bill [2014-1014-1144-21164]
 * - modified Get_Master_Report_Index_UI(), Master Report - input passing mark one time only
 * 20150313 Bill [2014-1121-1759-15164]
 * - modified ExportExcelFile_To_WebSAMAS(), Get_Mgmt_DataHandling_Tab_Array() - for data export to WebSAMS
 * 20150130 Bill [2014-1014-1144-21164]
 * - modified Get_Master_Report_Index_UI(), Get_Management_PersonalChar_Edit_UI() - for Cust
 * 20150102 Ivan [2014-1211-0922-37073]:
 * - modified Get_SubjectAcademicResultReport_right_UI() to add subject group teacher display
 * - deploy: ip.2.5.6.1.1
 * 20141103 Siuwan [2014-0912-1716-13164]:
 * - pt3 : modified Get_Other_Info_Edit_Table() to add apply all feature
 * 20141006 Ryan:
 * - modified : Get_Settings_PersonalCharacteristics_Scale_Step1_UI()
 * disable check box only when scaleID != default
 * 20140801 Ryan:
 * - Merged SIS cust, keyword # SIS # 201407
 * - Hide DataHandling->TransferToiPortfolio
 * - Other Info Cust Export -> Export ALL
 * - Hide Manual Adjustment, Promotion
 * 20130722 Roy:
 * - added function Get_Import_Class_Teacher_Comment_Step1_UI()
 * - added function Get_Import_Class_Teacher_Comment_Step2_UI()
 * - added function Get_Import_Class_Teacher_Comment_Step3_UI()
 * 20130708 Roy:
 * - modified function Get_Other_Info_Overview_UI() to add $ExportBtn
 * 20130705 Roy:
 * - modified function Get_Other_Info_Overview_UI() to add $FormSelection
 * - modified function Get_Other_Info_Overview_table() to add form filter in for-loop
 * 20130219 Ivan [2013-0218-1002-16071]:
 * - updated function Get_Other_Info_Overview_UI() to show remarks for specific tab in Other Info
 * 20120919 Ivan:
 * - updated function Get_Settings_ReportCardTemplate_ReportCardInfo_Table() added param $ExtraRowAry to show more rows
 * 20120620 Rita:
 * - added function Get_Settings_ReportCardTemplate_ReportCardInfo_Table - Get Report Card Info for report card templates
 * - added function Get_Settings_ReportCardTemplate_Nav() - Navigation for report card templates
 * 20120405 Marcus:
 * - modified Get_Reportcard_DB_Selection (added Param $IncludedYear), Get_Print_Archived_Report_Select_Report_UI, show academic year with archived report only
 * - 2012-0227-1124-07072 - 匯基書院 (東九龍) - 4 Problems of eRC
 * 20120326 Marcus:
 * - modified Get_Management_PersonalChar_Edit_UI, Cater allow to set time for submission, verification
 * - 2012-0112-1622-04073 - 迦密愛禮信中學 - Schedule of Report Card Management
 * 20120316 Marcus:
 * - modified Get_Ranking_Display_Table, Cater GreaterThanMark for $eRCTemplateSetting['DisplayPosition']['SubjectPositionDisplaySettings']
 * - 2011-1130-1655-19066 - 蘇浙公學 - Show position setting in report card template is not working
 * 20120308 Marcus:
 * -improved Master Report to display archived student name with star // 2011-1111-1552-56042 - 聖若瑟教區中學(第二、三校) Master Report
 * -improved Master Report to display Class Teacher // 2011-1111-1552-56042 - 聖若瑟教區中學(第二、三校) Master Report
 * 20120229 Ivan:
 * - added Get_Report_Common_Index()
 * 20120224 Ivan:
 * - added Get_Subject_Teacher_Comment_MaxLength_Settings_Table()
 * - modified Get_Management_Class_Teacher_Comment_Edit_UI() to support $eRCTemplateSetting['ClassTeacherComment']['MaxLengthSettings']
 * 20120221 Marcus: [2012-0220-1119-33132 - 港大同學會書院 - eRC - Change report card subject status]
 * - modified Get_HKUGA_Grade_List_Table, Improved to display special case instead of N.A.
 * 20120216 Ivan:
 * modified Get_HKUGA_Grade_List_Table, added stripslashes() before display the comment
 * 20120214 Marcus:
 * modified Get_Other_Info_Edit_Table, prepend \n to the value in textarea.
 * 20120209 Marcus:
 * modified functions Get_Master_Report_Index_UI, Get_Master_Report, to cater Overall Personal Characteristic
 * 20120109 Ivan:
 * modified function Get_Management_PersonalChar_Edit_UI() to add print function
 * 20111125 Marcus:
 * added functions to cater OtherInfo in DB
 * 20111115 Ivan:
 * added Get_Settings_PersonalCharacteristics_Scale_UI(), Get_Settings_PersonalCharacteristics_Scale_Table()
 * 20111114 Marcus:
 * modified Get_Master_Report, Get_Master_Report_Index_UI, Cater display Personal Characteristic, Class Teacher Comment in Master Report.
 * 20111012 Ivan:
 * added / modified: Get_Edit_Award_Student_Table_By_Generated_Result, Get_Add_Student_Award_Layer_UI, Get_Add_Student_Award_Layer_Table, Get_Add_Award_Student_Layer_UI, Get_Add_Award_Student_Layer_Table
 * 20111012 Marcus:
 * modified Get_Master_Report, Get_Master_Report_Index_UI, Cater show coresponding subject only for subject group
 * 20111007 Connie:
 * modified Get_Management_Class_Teacher_Comment_Edit_UI
 * 20110921 Connie:
 * Added Get_Trial_Promotion_Report
 * 20110919 Connie:
 * Added Get_Trial_Promotion_Index_UI
 * 20110829 Ivan:
 * Added Include_eReportCard_Common_JS
 * 20110613 Marcus:
 * modified Get_HKUGA_Grade_List_Index_UI, Get_Class_Selection, Get_Form_Selection, improved teaching checking, cater subject teacher permission
 * 20110530 Ivan:
 * Added Award Generation related functions
 * 20110520 Marcus:
 * Added Get_HKUGA_Grade_List_Index_UI, Get_HKUGA_Grade_List_Table HKUGA Cust
 *
 *
 * **************************************************************************
 */
class libreportcard_ui extends libreportcard
{
    
    function libreportcard_ui($academicYearId = '')
    {
        $this->libreportcard($academicYearId);
    }
    
    function Include_JS_CSS()
    {
        global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
        
        $x = '
			<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.js"></script>
			    
			<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.autocomplete.css" type="text/css" />
			';
        
        return $x;
    }
    
    function Include_eReportCard_Common_JS()
    {
        global $PATH_WRT_ROOT, $LAYOUT_SKIN;
        
        return '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/' . $LAYOUT_SKIN . '/js/reportcard.js"></script>';
    }
    
    function GenReportList($Semester = '', $ClassLevelID = '', $DisplayOption = '')
    {
        global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $linterface, $button_view, $eReportCard, $button_print, $button_promotion, $button_archive, $button_export, $sys_custom, $eRCTemplateSetting, $Lang;
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        $lclass = new libclass();
        
        // Default: show generate, adjust, print columns in Reports > Generate & Print Report Card
        if ($eRCTemplateSetting['Report']['BatchExportReport'] && $DisplayOption == '')
            $DisplayOption = array(
                'Generate',
                'Adjust',
                'Print',
                'Export'
            );
            else if ($DisplayOption == '')
                $DisplayOption = array(
                    'Generate',
                    'Adjust',
                    'Print'
                );
                
                $con = array();
                if ($Semester)
                    $con[] = "template.Semester = '" . $Semester . "'";
                    if ($ClassLevelID != - 1)
                        $con[] = "template.ClassLevelID = '" . $ClassLevelID . "'";
                        $conStr = implode(" and ", $con);
                        $conStr = $conStr ? " where " . $conStr : "";
                        
                        $table = $this->DBName . ".RC_REPORT_TEMPLATE";
                        $RC_REPORT_TEMPLATE_COLUMN = $this->DBName . ".RC_REPORT_TEMPLATE_COLUMN";
                        
                        $ReportTypeWithSemesterNameField = $this->Get_Template_Type_With_Semester_NameField('template.', 'ayt.', 'ayt_c.');
                        
                        // [2015-0413-1359-48164] fields for BIBA preset report type
                        if ($eRCTemplateSetting['Settings']['AutoSelectReportType']) {
                            $fields = " , template.isMainReport, template.ReportSequence";
                        }
                        
                        $sql = "
                        select
                        template.ReportID,
                        template.ReportTitle,
                        template.Semester,
                        template.ClassLevelID,
                        template.Issued,
                        template.LastPrinted,
                        template.LastAdjusted,
                        template.LastGenerated,
                        template.LastMarksheetInput,
                        template.LastArchived,
                        template.LastGeneratedAward,
                        y.YearName as FormName,
                        $ReportTypeWithSemesterNameField as ReportTypeWithSemester,
                        If (template.Semester = 'F', 2, 1) as ReportDisplayOrder
                        $fields
                        from
                        $table as template
                        Inner Join $RC_REPORT_TEMPLATE_COLUMN as rtc On (template.ReportID = rtc.ReportID)
                        Inner Join YEAR as y On (template.ClassLevelID = y.YearID)
                        Left Outer Join ACADEMIC_YEAR_TERM as ayt On (template.Semester = ayt.YearTermID)
                        Left Outer Join ACADEMIC_YEAR_TERM as ayt_c On (rtc.SemesterNum = ayt_c.YearTermID)
                        $conStr
                        Group By
                        template.ReportID
                        order by
                        y.Sequence, ReportDisplayOrder, /* template.Semester + 0, */ ayt.TermStart, template.isMainReport Desc, template.ReportTitle
                        ";
                        $x = $this->returnArray($sql);
                        
                        $xi = 0;
                        // $LevelNameArr = array();
                        foreach ($x as $key => $data) {
                            $css = "tablerow" . (($xi % 2) + 1);
                            $ReportID = $data['ReportID'];
                            $SemID = $data['Semester'];
                            $IsMainReport = $data['isMainReport'];
                            $ClassLevelID = $data['ClassLevelID'];
                            $report_title = str_replace(":_:", "<br>", $data['ReportTitle']);
                            $lv_name = $data['FormName'];
                            $ReportType = $SemID == "F" ? "W" : "T";
                            
                            $issue_date = (is_date_empty($data['Issued'])) ? $Lang['General']['EmptySymbol'] : $data['Issued'];
                            $archive_btn = (is_date_empty($data['LastGenerated'])) ? $Lang['General']['EmptySymbol'] : $linterface->GET_ACTION_BTN($button_archive, "button", "clickArchiveReport($ReportID)");
                            
                            // ## Report Type Display
                            // $ReportTypeDisplay = $data['ReportTypeWithSemester'];
                            if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment'] && $data['Semester'] == 'F') {
                                $ReportTypeDisplay = $this->Get_Report_Type_With_Term_Info($ReportID, ' ');
                            } else {
                                $ReportTypeDisplay = $data['ReportTypeWithSemester'];
                            }
                            
                            // [2015-0413-1359-48164] display for BIBA preset report type
                            if ($eRCTemplateSetting['Settings']['AutoSelectReportType']) {
                                // form and semester
                                // [2015-1013-1434-16164]
                                // $cur_form_num = intval($this->GET_FORM_NUMBER($ClassLevelID));
                                $cur_form_num = $this->GET_FORM_NUMBER($ClassLevelID);
                                $sem_num = intval($this->Get_Semester_Seq_Number($SemID));
                                
                                $ReportTypeDisplay = $this->getBIBAReportName($cur_form_num, $sem_num, $IsMainReport, $SemID == "F", $data['ReportSequence']);
                            }
                            
                            // ## Preview Button
                            $preview_btn = "<a href=\"javascript:newWindow('../../settings/reportcard_templates/preview.php?ReportID=$ReportID', '10');\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
                            
                            // ## Archive
                            if (in_array('Archive', $DisplayOption)) {
                                $LastGenerateDate = $data['LastArchived'];
                                $LastModifiedLang = $eReportCard['ReportCardGeneratedOn'];
                                $LastModifiedDate = $data['LastGenerated'];
                                $archive_date = (is_date_empty($data['LastArchived'])) ? "&nbsp;" : $eReportCard['LastArchive'] . " : " . $data['LastArchived'];
                                
                                $print_btn = (is_date_empty($data['LastArchived'])) ? $Lang['General']['EmptySymbol'] : $linterface->GET_ACTION_BTN($button_print, "button", "clickPrintMenu($ReportID)");
                            }
                            
                            // ## Generate
                            if (in_array('Generate', $DisplayOption)) {
                                $LastGenerateDate = $data['LastGenerated'];
                                $LastModifiedLang = $eReportCard['MarkModifiedOn'];
                                $LastModifiedDate = $data['LastMarksheetInput'];
                                
                                $generate_date = (is_date_empty($LastGenerateDate)) ? "&nbsp;" : $eReportCard['LastGenerate'] . " : " . $LastGenerateDate;
                                $print_btn = (is_date_empty($LastGenerateDate)) ? $Lang['General']['EmptySymbol'] : $linterface->GET_ACTION_BTN($button_print, "button", "clickPrintMenu($ReportID)");
                                if ($eRCTemplateSetting['Report']['BatchExportReport'])
                                    $export_btn = (is_date_empty($LastGenerateDate)) ? $Lang['General']['EmptySymbol'] : $linterface->GET_ACTION_BTN($button_export, "button", "clickExportMenu($ReportID)");
                                    $generate_award_btn = (is_date_empty($LastGenerateDate)) ? $Lang['General']['EmptySymbol'] : $linterface->GET_ACTION_BTN($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GenerateAward'], "button", "clickGenerateAwardMenu($ReportID)");
                                    
                                    $CanGen = $this->checkCanGenernateReport($ReportID);
                                    $CanGenStr = implode("<br>", $CanGen);
                                    
                                    // [2016-0330-1524-42164] added loading message div
                                    $loading_div = "<div id='sendingDiv_$ReportID' style='width:100%; display:none;'>" . $linterface->Get_Ajax_Loading_Image($noLang = 1) . "<span style='color:red; font-weight:bold;'>" . $Lang['General']['Generating...'] . "</span></div>";
                                    $gen_btn = empty($CanGen) || $eRCTemplateSetting['AllowGenerateAtAnyTime'] ? $linterface->GET_ACTION_BTN($eReportCard['Generate'], "button", "clickGenReport($ReportID)", "GenBtn_$ReportID") . $loading_div : $CanGenStr;
                                    
                                    // [2015-1104-1130-08164] display generate button with data input reminder
                                    if ($eRCTemplateSetting['AllowGenerateAtAnyTimeWithMsg']) {
                                        $gen_btn = empty($CanGen) ? "" : $CanGenStr . "<br><br>";
                                        $gen_btn .= $linterface->GET_ACTION_BTN($eReportCard['Generate'], "button", "clickGenReport($ReportID)", "GenBtn_$ReportID") . $loading_div;
                                    }
                            }
                            
                            // ## MS / Report Generated Alert
                            $modify_alert = ($LastModifiedDate > $LastGenerateDate && ! is_date_empty($LastGenerateDate)) ? "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"systemmsg\"><tr><td>" . $LastModifiedLang . "<br>" . $LastModifiedDate . "</td></tr></table>" : "";
                            
                            // ## Promotion
                            if ($ReportType == "W" && ! is_date_empty($data['LastGenerated']))
                                $promote_btn = $linterface->GET_ACTION_BTN($button_promotion, "button", "clickPromotion($ReportID, $ClassLevelID)");
                                else
                                    $promote_btn = "--";
                                    
                                    if (in_array('Adjust', $DisplayOption)) {
                                        $adj_btn = (is_date_empty($data['LastGenerated'])) ? $Lang['General']['EmptySymbol'] : $linterface->GET_ACTION_BTN($eReportCard['Adjust'], "button", "clickAdjust($ReportID)");
                                        $adj_date = (is_date_empty($data['LastGenerated'])) ? "&nbsp;" : $eReportCard['LastAdjust'] . " : " . $data['LastAdjusted'];
                                    }
                                    
                                    if (in_array('ToDigitalArchive', $DisplayOption)) {
                                        $toDA_btn = (is_date_empty($data['LastArchived'])) ? $Lang['General']['EmptySymbol'] : $linterface->GET_ACTION_BTN($Lang['eReportCard']['GeneralArr']['ToDigitalArchive'], "button", "clickToDigitalArchive($ReportID)");
                                    }
                                    
                                    $row .= "<tr class=\"resubtabletop\" style=\"vertical-align:top;\">";
                                    $row .= "<td class=\"$css tabletext\"><strong>" . $report_title . "</strong></td>";
                                    $row .= "<td width=\"150\" class=\"$css tabletext\">" . $ReportTypeDisplay . "</td>";
                                    $row .= "<td width=\"80\" class=\"$css tabletext\">" . $lv_name . "</td>";
                                    $row .= "<td class=\"$css\">" . $preview_btn . "</td>";
                                    $row .= "<td class=\"$css tabletext\" nowrap>" . $issue_date . "</td>";
                                    
                                    // Generate
                                    $row .= (in_array('Generate', $DisplayOption)) ? "<td class=\"$css tabletext\">" . $gen_btn . "<br></td>" : '';
                                    
                                    // Generate Award
                                    if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration'] && in_array('Generate', $DisplayOption)) {
                                        $LastGenerateReportDate = $data['LastGenerated'];
                                        $LastGeneratedAwardDate = $data['LastGeneratedAward'];
                                        
                                        $generate_award_date = (is_date_empty($LastGeneratedAwardDate)) ? "&nbsp;" : $eReportCard['LastGenerate'] . " : " . $LastGeneratedAwardDate;
                                        $generate_award_warning = ($LastGenerateDate > $LastGeneratedAwardDate && ! is_date_empty($LastGenerateDate)) ? "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"systemmsg\"><tr><td>" . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardNotYetGeneratedAfterReportGeneration'] . "</td></tr></table>" : "";
                                        
                                        $row .= "<td class=\"$css\">" . $generate_award_btn . "<br></td>";
                                    }
                                    
                                    // Archive
                                    $row .= (in_array('Archive', $DisplayOption)) ? "<td class=\"$css tabletext\">" . $archive_btn . "<br></td>" : '';
                                    
                                    // updated by marcus 20100518 - reopen for holy trinity
                                    // 20140730 $eRCTemplateSetting['ArchiveReportCard']['HidePromotion'] for Hiding promotion for SIS in Archive report
                                    if ($sys_custom['eRC']['Report']['ReportGeneration']['Promotion'] && $eRCTemplateSetting['ArchiveReportCard']['HidePromotion'] == false) {
                                        $row .= "<td class=\"$css\">" . $promote_btn . "</td>";
                                    }
                                    // 20140731 SIS HideManualAdjustment
                                    if ($eRCTemplateSetting['Report']['ReportGeneration']['HideManualAdjustment']) {
                                        // Skip output Adjust button
                                    } else {
                                        $row .= (in_array('Adjust', $DisplayOption)) ? "<td class=\"$css\">" . $adj_btn . "</td>" : '';
                                    }
                                    $row .= ($eRCTemplateSetting['Report']['BatchExportReport'] && in_array('Export', $DisplayOption)) ? "<td class=\"$css\">" . $export_btn . "<br></td>" : '';
                                    $row .= (in_array('Print', $DisplayOption)) ? "<td class=\"$css\">" . $print_btn . "<br></td>" : '';
                                    $row .= (in_array('ToDigitalArchive', $DisplayOption)) ? "<td class=\"$css\">" . $toDA_btn . "<br></td>" : '';
                                    $row .= "<td class=\"$css\">&nbsp;</td>";
                                    $row .= "</tr>";
                                    $row .= "<tr class=\"$css\" style=\"vertical-align:top;\">";
                                    $row .= "<td align=\"right\" class=\"tabletext\"></td>";
                                    $row .= "<td class=\"tabletext\">&nbsp;</td>";
                                    $row .= "<td class=\"tabletext\">&nbsp;</td>";
                                    $row .= "<td class=\"tabletext\">&nbsp;</td>";
                                    $row .= "<td class=\"tabletext \">&nbsp;</td>";
                                    $row .= (in_array('Generate', $DisplayOption)) ? "<td class=\"tabletextremark \">" . $generate_date . "<br>" . $modify_alert . "</td>" : '';
                                    
                                    if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration'] && in_array('Generate', $DisplayOption)) {
                                        $row .= "<td class=\"tabletextremark \" valign=\"top\">" . $generate_award_date . "<br>" . $generate_award_warning . "</td>";
                                    }
                                    
                                    $row .= (in_array('Archive', $DisplayOption)) ? "<td class=\"tabletextremark \">" . $archive_date . "<br>" . $modify_alert . "</td>" : '';
                                    
                                    // 20140730 $eRCTemplateSetting['ArchiveReportCard']['HidePromotion'] for Hiding promotion for SIS in Archive report
                                    if ($sys_custom['eRC']['Report']['ReportGeneration']['Promotion'] && $eRCTemplateSetting['ArchiveReportCard']['HidePromotion'] == false) {
                                        $row .= "<td class=\"tabletext \">&nbsp;</td>";
                                    }
                                    // 20140731 SIS HideManualAdjustment
                                    if ($eRCTemplateSetting['Report']['ReportGeneration']['HideManualAdjustment']) {
                                        // Skip output Adjust button
                                    } else {
                                        $row .= (in_array('Adjust', $DisplayOption)) ? "<td class=\"tabletext \"><span class=\"tabletextremark\">" . $adjust_date . "<br>" . $adj_list . "</span></td>" : '';
                                    }
                                    // $row .= "<td class=\"tabletext\"><span class=\"tabletextremark\">". $adj_date ."</span></td>";
                                    $row .= ($eRCTemplateSetting['Report']['BatchExportReport'] && in_array('Export', $DisplayOption)) ? "<td class=\"tabletext \"><span class=\"tabletextremark\">" . $export_date . "</span></td>" : '';
                                    $row .= (in_array('Print', $DisplayOption)) ? "<td class=\"tabletext \"><span class=\"tabletextremark\">" . $print_date . "</span></td>" : '';
                                    $row .= (in_array('ToDigitalArchive', $DisplayOption)) ? "<td class=\"tabletext \"><span class=\"tabletextremark\">&nbsp;</span></td>" : '';
                                    $row .= "<td class=\"tabletext\">&nbsp;</td>";
                                    $row .= "</tr>";
                                    
                                    $xi ++;
                        }
                        
                        return $row;
    }
    
    function GenReportList_To_iPortfolio($Semester = '', $ClassLevelID = '')
    {
        global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $linterface, $button_view, $eReportCard, $button_print, $button_promotion;
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        $lclass = new libclass();
        
        $con = array();
        if ($Semester != '')
            $con[] = "template.Semester = '" . $Semester . "'";
            if ($ClassLevelID != - 1)
                $con[] = "template.ClassLevelID = '" . $ClassLevelID . "'";
                $conStr = implode(" and ", $con);
                $conStr = $conStr ? " where " . $conStr : "";
                
                $table = $this->DBName . ".RC_REPORT_TEMPLATE";
                /*
                 * $sql = "
                 * SELECT
                 * ReportID,
                 * ReportTitle,
                 * Semester,
                 * ClassLevelID,
                 * LastGenerated,
                 * LastDateToiPortfolio
                 * FROM
                 * $table
                 * $conStr
                 * order by
                 * ClassLevelID
                 * ";
                 */
                $sql = "
                select
                template.ReportID,
                template.ReportTitle,
                template.Semester,
                template.ClassLevelID,
                template.LastGenerated,
                template.LastDateToiPortfolio,
                If (template.Semester = 'F', 2, 1) as ReportDisplayOrder
                from
                $table as template
                Inner Join YEAR as y On (template.ClassLevelID = y.YearID)
                Left Outer Join ACADEMIC_YEAR_TERM as ayt On (template.Semester = ayt.YearTermID)
                $conStr
                order by
                y.Sequence, ReportDisplayOrder, /* template.Semester + 0, */ ayt.TermStart, template.isMainReport Desc, template.ReportTitle
                ";
                $x = $this->returnArray($sql);
                $xi = 0;
                foreach ($x as $key => $data) {
                    $css = "tablerow" . (($xi % 2) + 1);
                    $ReportID = $data['ReportID'];
                    $ClassLevelID = $data['ClassLevelID'];
                    $ClassLevelName = $this->returnClassLevel($ClassLevelID);
                    $report_title = str_replace(":_:", "<br>", $data[ReportTitle]);
                    $lv_name = $lclass->getLevelName($data[ClassLevelID]);
                    $generate_date = ($data[LastGenerated] == "0000-00-00 00:00:00" || $data[LastGenerated] == "") ? "--" : $data['LastGenerated'];
                    $transfer_date = ($data[LastDateToiPortfolio] == "0000-00-00 00:00:00" || $data[LastDateToiPortfolio] == "") ? "--" : $data['LastDateToiPortfolio'];
                    $SemID = $data['Semester'];
                    // $SemName = $this->returnSemesters($SemID);
                    $ClassLevelName = $this->returnClassLevel($ClassLevelID);
                    
                    // ## Report Type Display
                    $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID);
                    
                    if ($generate_date != "--") {
                        $transferBtn = $linterface->GET_ACTION_BTN($eReportCard['Transfer'], "button", "js_Go_Transfer($ReportID)");
                    } else {
                        $transferBtn = "--";
                    }
                    
                    $row .= "<tr class=\"$css tabletext\" valign='top'>";
                    $row .= "<td ><strong>" . $report_title . "</strong></td>";
                    $row .= "<td width=\"150\" >" . $ReportTypeDisplay . "</td>";
                    $row .= "<td width=\"80\" >" . $lv_name . "</td>";
                    // $row .= "<td >". $SemName ."</td>";
                    $row .= "<td >" . $generate_date . "</td>";
                    $row .= "<td >" . $transfer_date . "</td>";
                    $row .= "<td >" . $transferBtn . "</td>";
                    $row .= "<td >&nbsp;</td>";
                    $row .= "</tr>";
                    
                    $xi ++;
                }
                
                return $row;
    }
    
    function ExportExcelFile_To_WebSAMAS($Semester = '', $ClassLevelID = '')
    {
        global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $linterface, $button_view, $eReportCard, $button_print, $button_promotion;
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        $lclass = new libclass();
        
        $conds = array();
        if ($Semester != '')
            $conds[] = "template.Semester = '" . $Semester . "'";
            if ($ClassLevelID != - 1)
                $conds[] = "template.ClassLevelID = '" . $ClassLevelID . "'";
                $condStr = implode(" and ", $conds);
                $condStr = $condStr ? " where " . $condStr : "";
                
                $table = $this->DBName . ".RC_REPORT_TEMPLATE";
                $sql = "select
                template.ReportID,
                template.ReportTitle,
                template.Semester,
                template.ClassLevelID,
                template.LastGenerated,
                template.LastDateExportToWebSAMS,
                If (template.Semester = 'F', 2, 1) as ReportDisplayOrder
                from
                $table as template
                Inner Join YEAR as y On (template.ClassLevelID = y.YearID)
                Left Outer Join ACADEMIC_YEAR_TERM as ayt On (template.Semester = ayt.YearTermID)
                $condStr
                order by
                y.Sequence, ReportDisplayOrder, template.Semester + 0, ayt.TermStart, template.isMainReport Desc, template.ReportTitle
                ";
                $x = $this->returnArray($sql);
                
                $xi = 0;
                foreach ($x as $key => $data) {
                    $alertDiv = "";
                    $css = "tablerow" . (($xi % 2) + 1);
                    $ReportID = $data['ReportID'];
                    $ClassLevelID = $data['ClassLevelID'];
                    $ClassLevelName = $this->returnClassLevel($ClassLevelID);
                    $lv_name = $lclass->getLevelName($ClassLevelID);
                    $report_title = str_replace(":_:", "<br>", $data['ReportTitle']);
                    $generate_date = ($data['LastGenerated'] == "0000-00-00 00:00:00" || $data['LastGenerated'] == "") ? "--" : $data['LastGenerated'];
                    $export_date = ($data['LastDateExportToWebSAMS'] == "0000-00-00 00:00:00" || $data['LastDateExportToWebSAMS'] == "") ? "--" : $data['LastDateExportToWebSAMS'];
                    $SemID = $data['Semester'];
                    
                    // ## Report Type Display
                    $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID);
                    
                    if ($generate_date != "--") {
                        $transferBtn = $linterface->GET_ACTION_BTN($eReportCard['Export'], "button", "js_Go_Export_WebSAMS($ReportID)");
                        if ($export_date != "--" && (strtotime($generate_date) > strtotime($export_date))) {
                            $alertDiv = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"systemmsg\"><tr><td>" . $eReportCard['LastGenerate'] . "<br>" . $generate_date . "</td></tr></table>";
                        }
                    } else {
                        $transferBtn = "--";
                    }
                    
                    $row .= "<tr class=\"$css tabletext\" valign='top'>";
                    $row .= "<td ><strong>" . $report_title . "</strong></td>";
                    $row .= "<td width=\"150\" >" . $ReportTypeDisplay . "</td>";
                    $row .= "<td width=\"80\" >" . $lv_name . "</td>";
                    $row .= "<td >" . $generate_date . "</td>";
                    $row .= "<td >" . $export_date . $alertDiv . "</td>";
                    $row .= "<td >" . $transferBtn . "</td>";
                    $row .= "<td >&nbsp;</td>";
                    $row .= "</tr>";
                    
                    $xi ++;
                }
                
                return $row;
    }
    
    function GenSubMarksheetReportList($Semester = '', $ClassLevelID = '', $DisplayOption = '')
    {
        global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $linterface, $button_view, $eReportCard, $button_print, $button_promotion, $button_archive;
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        $lclass = new libclass();
        
        // Default: show generate, adjust, print columns in Reports > Generate & Print Report Card
        // if ($DisplayOption == '')
        // $DisplayOption = array('Generate', 'Adjust', 'Print');
        
        $con = array();
        if ($Semester)
            $con[] = "template.Semester = '" . $Semester . "'";
            if ($ClassLevelID != - 1)
                $con[] = "template.ClassLevelID = '" . $ClassLevelID . "'";
                $conStr = implode(" and ", $con);
                $conStr = $conStr ? " where " . $conStr : "";
                
                $tables = $this->DBName . ".RC_REPORT_TEMPLATE as template ";
                
                /*
                 * $tables .= " LEFT JOIN ".$this->DBName.".RC_REPORT_TEMPLATE_COLUMN b ON a.ReportID = b.ReportID";
                 * $tables .= " LEFT JOIN ".$this->DBName.".RC_SUB_MARKSHEET_COLUMN c ON c.ReportColumnID = b.ReportColumnID";
                 * $sql = "
                 * select
                 * a.ReportID,
                 * a.ReportTitle,
                 * a.Semester,
                 * a.ClassLevelID,
                 * COUNT(c.ColumnID) as NumOfSubMS
                 * from
                 * $tables
                 * $conStr
                 * GROUP BY
                 * a.ReportID
                 * order by
                 * a.ClassLevelID, a.Semester
                 * ";
                 */
                
                $tables .= " LEFT JOIN " . $this->DBName . ".RC_REPORT_TEMPLATE_COLUMN templateCol ON template.ReportID = templateCol.ReportID";
                $tables .= " LEFT JOIN " . $this->DBName . ".RC_SUB_MARKSHEET_COLUMN subMSCol ON subMSCol.ReportColumnID = templateCol.ReportColumnID";
                $sql = "
                select
                template.ReportID,
                template.ReportTitle,
                template.Semester,
                template.ClassLevelID,
                COUNT(subMSCol.ColumnID) as NumOfSubMS,
                If (template.Semester = 'F', 2, 1) as ReportDisplayOrder
                from
                $tables
                Left Outer Join
                YEAR as y
                On (template.ClassLevelID = y.YearID)
                Left Outer Join
                ACADEMIC_YEAR_TERM as ayt
                On (template.Semester = ayt.YearTermID)
                $conStr
                GROUP BY
                template.ReportID
                order by
                y.Sequence, ReportDisplayOrder, template.Semester + 0, ayt.TermStart, template.isMainReport Desc, template.ReportTitle
                ";
                $x = $this->returnArray($sql);
                
                $xi = 0;
                foreach ($x as $key => $data) {
                    $css = "tablerow" . (($xi % 2) + 1);
                    $ReportID = $data['ReportID'];
                    $ClassLevelID = $data['ClassLevelID'];
                    $report_title = str_replace(":_:", "<br>", $data['ReportTitle']);
                    $lv_name = $lclass->getLevelName($data['ClassLevelID']);
                    $haveSubMarksheet = ($data["NumOfSubMS"] > 0);
                    
                    $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
                    $SemID = $ReportSetting['Semester'];
                    $ReportType = $SemID == "F" ? "W" : "T";
                    $ClassLevel = $this->returnClassLevel($ClassLevelID);
                    // $ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
                    
                    // ## Report Type Display
                    $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID);
                    
                    $gen_btn = ! empty($haveSubMarksheet) ? $linterface->GET_ACTION_BTN($eReportCard['Generate'], "button", "clickGenReport($ReportID)") : "--";
                    
                    $row .= "<tr class=\"resubtabletop\">";
                    $row .= "<td class=\"$css tabletext\"><strong>" . $report_title . "</strong></td>";
                    $row .= "<td width=\"150\" class=\"$css tabletext\">" . $ReportTypeDisplay . "</td>";
                    $row .= "<td width=\"80\" class=\"$css tabletext\">" . $lv_name . "</td>";
                    // $row .= "<td class=\"$css\"><a href=\"javascript:newWindow('../../settings/reportcard_templates/preview.php?ReportID=$ReportID', '10');\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">{$button_view}</a></td>";
                    $row .= "<td class=\"$css tabletext\">" . $gen_btn . "<br></td>";
                    $row .= "<td class=\"$css\">&nbsp;</td>";
                    $row .= "</tr>";
                    
                    $xi ++;
                }
                
                return $row;
    }
    
    function GenAssessmentReportList($Semester = '', $ClassLevelID = '')
    {
        global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $linterface, $button_view, $eReportCard, $button_print, $button_promotion, $button_archive;
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        $lclass = new libclass();
        
        // $con = array();
        $conStr = '';
        if ($Semester)
            $conStr .= " and template.Semester = '" . $Semester . "'";
            if ($ClassLevelID != - 1)
                $conStr .= " and template.ClassLevelID = '" . $ClassLevelID . "'";
                // $conStr = implode(" and ", $con);
                // $conStr = $conStr ? " where " . $conStr : "";
                
                $tables = $this->DBName . ".RC_REPORT_TEMPLATE as template ";
                
                /*
                 * $tables .= " INNER JOIN ".$this->DBName.".RC_REPORT_TEMPLATE_COLUMN b ON a.ReportID = b.ReportID";
                 * $sql = "
                 * select
                 * a.ReportID,
                 * a.ReportTitle,
                 * a.Semester,
                 * a.ClassLevelID,
                 * COUNT(b.ReportColumnID) as NumOfReportColumn
                 * from
                 * $tables
                 * where
                 * a.Semester != '' && a.Semester != 'F'
                 * $conStr
                 * GROUP BY
                 * a.ReportID
                 * order by
                 * a.ClassLevelID, a.Semester
                 * ";
                 */
                
                $tables .= " INNER JOIN " . $this->DBName . ".RC_REPORT_TEMPLATE_COLUMN col ON template.ReportID = col.ReportID";
                $sql = "
                select
                template.ReportID,
                template.ReportTitle,
                template.Semester,
                template.ClassLevelID,
                COUNT(col.ReportColumnID) as NumOfReportColumn,
                If (template.Semester = 'F', 2, 1) as ReportDisplayOrder
                from
                $tables
                Left Outer Join
                YEAR as y
                On (template.ClassLevelID = y.YearID)
                Left Outer Join
                ACADEMIC_YEAR_TERM as ayt
                On (template.Semester = ayt.YearTermID)
                WHERE
                1
                $conStr
                GROUP BY
                template.ReportID
                order by
                y.Sequence, ReportDisplayOrder, template.Semester + 0, ayt.TermStart, template.isMainReport Desc, template.ReportTitle
                ";
                $x = $this->returnArray($sql);
                
                $xi = 0;
                foreach ($x as $key => $data) {
                    $css = "tablerow" . (($xi % 2) + 1);
                    $ReportID = $data['ReportID'];
                    $ClassLevelID = $data['ClassLevelID'];
                    $report_title = str_replace(":_:", "<br>", $data['ReportTitle']);
                    $lv_name = $lclass->getLevelName($data['ClassLevelID']);
                    $haveSubMarksheet = ($data["NumOfReportColumn"] > 0);
                    
                    $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
                    $SemID = $ReportSetting['Semester'];
                    $ReportType = $SemID == "F" ? "W" : "T";
                    $ClassLevel = $this->returnClassLevel($ClassLevelID);
                    // $ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
                    
                    // ## Report Type Display
                    $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID);
                    
                    $gen_btn = ! empty($haveSubMarksheet) ? $linterface->GET_ACTION_BTN($eReportCard['Generate'], "button", "clickGenReport($ReportID)") : "--";
                    
                    $row .= "<tr class=\"resubtabletop\">";
                    $row .= "<td class=\"$css tabletext\"><strong>" . $report_title . "</strong></td>";
                    $row .= "<td width=\"150\" class=\"$css tabletext\">" . $ReportTypeDisplay . "</td>";
                    $row .= "<td width=\"80\" class=\"$css tabletext\">" . $lv_name . "</td>";
                    // $row .= "<td class=\"$css\"><a href=\"javascript:newWindow('../../settings/reportcard_templates/preview.php?ReportID=$ReportID', '10');\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">{$button_view}</a></td>";
                    $row .= "<td class=\"$css tabletext\">" . $gen_btn . "<br></td>";
                    $row .= "<td class=\"$css\">&nbsp;</td>";
                    $row .= "</tr>";
                    
                    $xi ++;
                }
                
                return $row;
    }
    
    function GenReportList_Mangement_AcademicProgress($Semester = '', $ClassLevelID = '')
    {
        global $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $linterface, $eReportCard, $Lang;
        
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        $lclass = new libclass();
        
        $con = array();
        if ($Semester)
            $con[] = "template.Semester = '" . $Semester . "'";
            if ($ClassLevelID != - 1)
                $con[] = "template.ClassLevelID = '" . $ClassLevelID . "'";
                $conStr = implode(" and ", $con);
                $conStr = $conStr ? " where " . $conStr : "";
                
                $table = $this->DBName . ".RC_REPORT_TEMPLATE";
                $sql = "select
                template.ReportID,
                template.ReportTitle,
                template.Semester,
                template.ClassLevelID,
                template.Issued,
                template.LastGenerated,
                template.LastGeneratedAcademicProgress,
                If (template.Semester = 'F', 2, 1) as ReportDisplayOrder
                from
                $table as template
                Inner Join YEAR as y On (template.ClassLevelID = y.YearID)
                Left Outer Join ACADEMIC_YEAR_TERM as ayt On (template.Semester = ayt.YearTermID)
                $conStr
                order by
                y.Sequence, ReportDisplayOrder, template.Semester + 0, ayt.TermStart, template.isMainReport Desc, template.ReportTitle
                ";
                $x = $this->returnArray($sql);
                
                $xi = 0;
                foreach ($x as $key => $data) {
                    $css = "tablerow" . (($xi % 2) + 1);
                    $ReportID = $data['ReportID'];
                    $ClassLevelID = $data['ClassLevelID'];
                    $ClassLevelID = $data['ClassLevelID'];
                    $report_title = str_replace(":_:", "<br>", $data['ReportTitle']);
                    $lv_name = $lclass->getLevelName($data['ClassLevelID']);
                    
                    $LastReportGenerated = $data['LastGenerated'];
                    $LastReportGenerated = ($LastReportGenerated == '0000-00-00 00:00:00') ? '' : $LastReportGenerated;
                    $LastGeneratedAcademicProgress = $data['LastGeneratedAcademicProgress'];
                    $LastGeneratedAcademicProgress = ($LastGeneratedAcademicProgress == '0000-00-00 00:00:00') ? '' : $LastGeneratedAcademicProgress;
                    
                    // ## Report Type Display
                    $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID);
                    
                    // ## Btn Display
                    $GenerateBtn = ($LastReportGenerated != '') ? $linterface->GET_ACTION_BTN($eReportCard['Generate'], "button", "js_Go_Generate_Academic_Progress($ReportID)") : '---';
                    $ViewBtn = ($LastGeneratedAcademicProgress != '') ? $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "js_View_Academic_Progress($ReportID)") : '---';
                    
                    // ## Modified Alert
                    $ModifiedAlertTable = '';
                    $thisAlertMsgArr = array();
                    
                    if ($LastGeneratedAcademicProgress != '' && ($LastGeneratedAcademicProgress < $LastReportGenerated))
                        $thisAlertMsgArr[] = $eReportCard["ReportGeneratedOn"] . ": " . $LastReportGenerated;
                        
                        if ($LastReportGenerated == '')
                            $thisAlertMsgArr[] = $eReportCard["ReportNotGenerated"];
                            
                            if (count($thisAlertMsgArr) > 0) {
                                $ModifiedAlertTable .= "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"systemmsg\">";
                                $ModifiedAlertTable .= "<tr>";
                                $ModifiedAlertTable .= "<td>";
                                $ModifiedAlertTable .= implode('<br />', $thisAlertMsgArr);
                                $ModifiedAlertTable .= "</td>";
                                $ModifiedAlertTable .= "</tr>";
                                $ModifiedAlertTable .= "</table>";
                            }
                            
                            // ## Last Generated Info
                            $LastGeneratedInfo = ($LastGeneratedAcademicProgress != '') ? $eReportCard['LastGenerate'] . ': ' . $LastGeneratedAcademicProgress : '&nbsp;';
                            
                            $row .= "<tr class=\"resubtabletop\">";
                            $row .= "<td class=\"$css tabletext\"><strong>" . $report_title . "</strong></td>";
                            $row .= "<td width=\"150\" class=\"$css tabletext\">" . $ReportTypeDisplay . "</td>";
                            $row .= "<td width=\"80\" class=\"$css tabletext\">" . $lv_name . "</td>";
                            $row .= "<td class=\"$css tabletext\">" . $GenerateBtn . "<br></td>";
                            $row .= "<td class=\"$css\">" . $ViewBtn . "<br></td>";
                            $row .= "<td class=\"$css\">&nbsp;</td>";
                            $row .= "</tr>";
                            $row .= "<tr class=\"$css\">";
                            $row .= "<td align=\"right\" class=\"tabletext\"></td>";
                            $row .= "<td class=\"tabletext\">&nbsp;</td>";
                            $row .= "<td class=\"tabletext\">&nbsp;</td>";
                            $row .= "<td class=\"tabletextremark \">" . $LastGeneratedInfo . "<br>" . $ModifiedAlertTable . "</td>";
                            $row .= "<td class=\"tabletext \">&nbsp;</td>";
                            $row .= "<td class=\"$css\">&nbsp;</td>";
                            $row .= "</tr>";
                            
                            $xi ++;
                }
                
                return $row;
    }
    
    // function Get_Form_Selection($ID_Name, $SelectedYearID='', $Onchange='', $noFirst=0, $isAll=0, $hasTemplateOnly=1, $checkClassTeacher=0, $IsMultiple=0)
    // {
    // global $Lang;
    // $FormArr = $this->GET_ALL_FORMS($hasTemplateOnly);
    // $numOfForm = count($FormArr);
    //
    // $selectArr = array();
    //
    // if ($isAll)
    // $selectArr['-1'] = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['All']['Form']);
    //
    // if ($checkClassTeacher==1)
    // {
    // $TeachingClassInfoArr = $this->GET_TEACHING_CLASS();
    // $TeachingYearIDArr = Get_Array_By_Key($TeachingClassInfoArr, 'YearID');
    // }
    //
    // for ($i=0; $i<$numOfForm; $i++)
    // {
    // $thisYearID = $FormArr[$i]['ClassLevelID'];
    // $thisYearName = $FormArr[$i]['LevelName'];
    //
    // // can select Form which the user is class teacher only
    // if ($checkClassTeacher==1 && !in_array($thisYearID, $TeachingYearIDArr))
    // continue;
    //
    // $selectArr[$thisYearID] = $thisYearName;
    // }
    //
    // $onchange = '';
    // if ($Onchange != "")
    // $onchange = 'onchange="'.$Onchange.'"';
    //
    // $Multiple = $IsMultiple? 'multiple size="10"' : "";
    //
    // $selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$Multiple;
    // $firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Form']);
    //
    // $formSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedYearID, $all=0, $noFirst, $firstTitle);
    //
    // return $formSelection;
    // }
    
    // $checkPermission == 0, do not check permission
    // $checkPermission == 1, show class teacher class only
    // $checkPermission == 2, show subject teacher class only
    // $checkPermission == 3, show subject teacher class and class teacher class
    function Get_Form_Selection($ID_Name, $SelectedYearID = '', $Onchange = '', $noFirst = 0, $isAll = 0, $hasTemplateOnly = 1, $checkPermission = 0, $IsMultiple = 0, $excludeWithoutClass = false)
    {
        global $Lang;
        $FormArr = $this->GET_ALL_FORMS($hasTemplateOnly, $excludeWithoutClass);
        $numOfForm = count($FormArr);
        
        $selectArr = array();
        
        if ($isAll)
            $selectArr['-1'] = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['All']['Form']);
            
            if ($checkPermission != 0) {
                $TeachingClassInfoArr = $this->GET_TEACHING_CLASS();
                $ClassTeacherYearID = Get_Array_By_Key($TeachingClassInfoArr, 'YearID');
                
                $SubjectTeacherClass = $this->returnSubjectTeacherClass($_SESSION['UserID'], '');
                $SubjectTeacherYearID = Get_Array_By_Key($SubjectTeacherClass, 'YearID');
                
                $subjectPanelYearID = Get_Array_By_Key($this->returnSubjectPanelInfo($_SESSION['UserID']), 'YearID');
                
                if ($checkPermission == 1)
                    $TeachingYearIDArr = $ClassTeacherYearID;
                    else if ($checkPermission == 2)
                        $TeachingYearIDArr = $SubjectTeacherYearID;
                        else if ($checkPermission == 3)
                            $TeachingYearIDArr = array_unique((array) array_merge((array) $ClassTeacherYearID, (array) $SubjectTeacherYearID, (array) $subjectPanelYearID));
            }
            
            // returnSunjectTeacherForm
            
            for ($i = 0; $i < $numOfForm; $i ++) {
                $thisYearID = $FormArr[$i]['ClassLevelID'];
                $thisYearName = $FormArr[$i]['LevelName'];
                
                // can select Form which the user is class teacher only
                if ($checkPermission != 0 && ! in_array($thisYearID, $TeachingYearIDArr))
                    continue;
                    
                    $selectArr[$thisYearID] = $thisYearName;
            }
            
            $onchange = '';
            if ($Onchange != "")
                $onchange = 'onchange="' . $Onchange . '"';
                
                $Multiple = $IsMultiple ? 'multiple size="10"' : "";
                
                $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $Multiple;
                $firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Form']);
                
                return getSelectByAssoArray($selectArr, $selectionTags, $SelectedYearID, $all = 0, $noFirst, $firstTitle);
    }
    
    // function Get_Class_Selection($ID_Name, $YearID, $SelectYearClassID='', $Onchange='', $noFirst=0, $isAll=0, $checkClassTeacher=0, $IsMultiple=0)
    // {
    // global $Lang, $PATH_WRT_ROOT, $eReportCard;
    //
    // include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
    // $ObjYear = new Year($YearID);
    // $YearClassInfoArr = $ObjYear->Get_All_Classes($checkClassTeacher, $this->schoolYearID);
    // $numOfClass = count($YearClassInfoArr);
    //
    // $selectArr = array();
    //
    // for ($i=0; $i<$numOfClass; $i++)
    // {
    // $thisYearClassID = $YearClassInfoArr[$i]['YearClassID'];
    // $thisYearClassName = Get_Lang_Selection($YearClassInfoArr[$i]['ClassTitleB5'], $YearClassInfoArr[$i]['ClassTitleEN']);
    //
    // $selectArr[$thisYearClassID] = $thisYearClassName;
    // }
    //
    // $onchange = '';
    // if ($Onchange != "")
    // $onchange = 'onchange="'.$Onchange.'"';
    //
    // $Multiple = $IsMultiple? 'multiple size="10"' : "";
    //
    // $selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$Multiple;
    // $firstTitle = ($isAll)? $eReportCard['AllClasses'] : Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Form']);
    //
    // $formSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectYearClassID, $isAll, $noFirst, $firstTitle);
    //
    // return $formSelection;
    // }
    
    // $checkPermission == 0, do not check permission
    // $checkPermission == 1, show class teacher class only
    // $checkPermission == 2, show subject teacher class only
    // $checkPermission == 3, show subject teacher class and class teacher class
    function Get_Class_Selection($ID_Name, $YearID, $SelectYearClassID = '', $Onchange = '', $noFirst = 0, $isAll = 0, $checkPermission = 0, $IsMultiple = 0)
    {
        global $Lang, $PATH_WRT_ROOT, $eReportCard;
        
        include_once ($PATH_WRT_ROOT . 'includes/form_class_manage.php');
        $ObjYear = new Year($YearID);
        $YearClassInfoArr = $ObjYear->Get_All_Classes(0, $this->schoolYearID);
        $numOfClass = count($YearClassInfoArr);
        
        $selectArr = array();
        
        if ($checkPermission != 0) {
            $TeachingClassInfoArr = $this->GET_TEACHING_CLASS();
            $ClassTeacherClassID = Get_Array_By_Key($TeachingClassInfoArr, 'YearClassID');
            $SubjectTeacherClass = $this->returnSubjectTeacherClass($_SESSION['UserID'], '');
            $SubjectTeacherClassID = Get_Array_By_Key($SubjectTeacherClass, 'YearClassID');
            
            if ($checkPermission == 1)
                $TeachingClassIDArr = $ClassTeacherClassID;
                else if ($checkPermission == 2)
                    $TeachingClassIDArr = $SubjectTeacherClassID;
                    else if ($checkPermission == 3)
                        $TeachingClassIDArr = array_unique((array) array_merge((array) $ClassTeacherClassID, (array) $SubjectTeacherClassID));
        }
        
        for ($i = 0; $i < $numOfClass; $i ++) {
            $thisYearClassID = $YearClassInfoArr[$i]['YearClassID'];
            
            if ($checkPermission != 0 && ! in_array($thisYearClassID, $TeachingClassIDArr))
                continue;
                
                $thisYearClassName = Get_Lang_Selection($YearClassInfoArr[$i]['ClassTitleB5'], $YearClassInfoArr[$i]['ClassTitleEN']);
                
                $selectArr[$thisYearClassID] = $thisYearClassName;
        }
        
        $onchange = '';
        if ($Onchange != "")
            $onchange = 'onchange="' . $Onchange . '"';
            
            $Multiple = $IsMultiple ? 'multiple size="10"' : "";
            
            $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $Multiple;
            $firstTitle = $IsMultiple ? "" : ($isAll) ? $eReportCard['AllClasses'] : Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Form']);
            
            $formSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectYearClassID, $isAll, $noFirst, $firstTitle);
            
            return $formSelection;
    }
    
    function Get_Position_Display_Type_Selection($ID_Name, $SelectedType = '', $OnChange = '', $Class = '')
    {
        global $eReportCard;
        
        $SelectArr = array();
        $SelectArr[1] = $eReportCard['PositionRange'];
        $SelectArr[2] = $eReportCard['PercentageRangeForAllStudents'];
        $SelectArr[3] = $eReportCard['PercentageRangeForPassedStudents'];
        
        $onchange = '';
        if ($OnChange != "")
            $onchange = 'onchange="' . $OnChange . '"';
            
            $class = '';
            if ($Class != "")
                $class = 'class="' . $Class . '"';
                
                $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $class;
                $selection = getSelectByAssoArray($SelectArr, $selectionTags, $SelectedType, $all = 0, $noFirst = 1);
                
                return $selection;
    }
    
    /*
     * @param string $PositionType: "Class" or "Form"
     */
    function Get_Ranking_Display_Table($ReportID, $ClassLevelID, $PositionType)
    {
        global $intranet_session_language, $eReportCard;
        
        // ## Get Subjects
        $SubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection = 0, $TeacherID = '', $intranet_session_language);
        // Add Grand Mark Row to the array
        $SubjectArr['-1'] = array(
            0 => $eReportCard['GrandPosition']
        );
        
        // Get All Position Display Setting
        $SettingArr = $this->Get_Position_Display_Setting($ReportID, '', $PositionType);
        
        // ## Prepare "Apply all" buttons
        $ApplyAllImg = $this->Get_ApplyToAll_Image();
        
        // ## Add a common class of all elements for enable / disable the whole table
        $thisElementClass = 'PositionDisplayElement_' . $PositionType;
        
        // Range Type Global Settings
        $thisID = 'RangeType_Global_' . $PositionType;
        $thisClass = $thisElementClass;
        $Global_RangeTypeSelection = $this->Get_Position_Display_Type_Selection($thisID, '', '', $thisClass);
        $Global_RangeTypeBtn = '<a href="javascript:jsApplyToAll(\'' . $thisID . '\', \'RangeType\', \'' . $PositionType . '\');">' . $ApplyAllImg . '</a>';
        
        // Upper Limit Input Global Settings
        $thisID = 'UpperLimit_Global_' . $PositionType;
        $thisClass = 'ratebox ' . $thisElementClass;
        $Global_UpperLimitTb = '<input id="' . $thisID . '" class="' . $thisClass . '" maxlength="3" value="0">';
        $Global_UpperLimitBtn = '<a href="javascript:jsApplyToAll(\'' . $thisID . '\', \'UpperLimit\', \'' . $PositionType . '\');">' . $ApplyAllImg . '</a>';
        
        // Greater than Mark Input Global Settings
        $thisID = 'GreaterThanMark_Global_' . $PositionType;
        $thisClass = 'ratebox ' . $thisElementClass;
        $Global_GreaterThanMarkTb = '<input id="' . $thisID . '" class="' . $thisClass . '" maxlength="3" value="0">';
        $Global_GreaterThanMarkBtn = '<a href="javascript:jsApplyToAll(\'' . $thisID . '\', \'GreaterThanMark\', \'' . $PositionType . '\');">' . $ApplyAllImg . '</a>';
        
        // Greater than Average Mark Input Global Settings
        $thisID = 'GreaterThanAverage_Global_' . $PositionType;
        $thisClass = 'ratebox ' . $thisElementClass;
        $Global_GreaterThanAverageTb = '<input id="' . $thisID . '" class="' . $thisClass . '" maxlength="3" value="0">';
        $Global_GreaterThanAverageBtn = '<a href="javascript:jsApplyToAll(\'' . $thisID . '\', \'GreaterThanAverage\', \'' . $PositionType . '\');">' . $ApplyAllImg . '</a>';
        
        // Display CheckBox Global Setting
        $thisID = 'DisplayPosition_Global_' . $PositionType;
        $Global_DisplayCheckBox = '<input type="checkbox" id="' . $thisID . '" name="' . $thisID . '" class="' . $thisElementClass . '" value="1" onclick="SetAll(this.checked,\'' . $PositionType . '\')">';
        
        // ## Build table
        $table = '';
        $table .= '<table cellpadding="5" cellspacing="0" width="100%" style="border:1px solid #CCCCCC;">';
        $table .= '<tr class="tabletop tabletopnolink term_break_link">';
        $table .= '<td width="25%">' . $eReportCard['Subject'] . '</td>';
        $table .= '<td width="25%" align="center">' . $eReportCard['Type'] . '</td>';
        $table .= '<td width="10%" align="center">' . $eReportCard['UpperLimit'] . '</td>';
        $table .= '<td width="10%" align="center">' . $eReportCard['AboveMark'] . '</td>';
        $table .= '<td width="15%" align="center">' . $eReportCard['AboveAverageMark'] . '</td>';
        $table .= '<td width="15%" align="center">' . $eReportCard['ShowPosition'] . '</td>';
        $table .= '</tr>';
        
        // ## "Apply all" buttons row
        $table .= '<tr class="tablebottom tabletopnolink term_break_link">' . "\n";
        $table .= '<td>&nbsp;</td>' . "\n";
        $table .= '<td align="center">' . $Global_RangeTypeSelection . ' ' . $Global_RangeTypeBtn . '</td>' . "\n";
        $table .= '<td align="center">' . $Global_UpperLimitTb . ' ' . $Global_UpperLimitBtn . '</td>' . "\n";
        $table .= '<td align="center">' . $Global_GreaterThanMarkTb . ' ' . $Global_GreaterThanMarkBtn . '</td>' . "\n";
        $table .= '<td align="center">' . $Global_GreaterThanAverageTb . ' ' . $Global_GreaterThanAverageBtn . '</td>' . "\n";
        $table .= '<td align="center">' . $Global_DisplayCheckBox . '</td>' . "\n";
        $table .= '</tr>' . "\n";
        
        $displayCounter = 0;
        foreach ((array) $SubjectArr as $MainSubjectID => $SubjectComponentArr) {
            foreach ((array) $SubjectComponentArr as $thisSubjectID => $SubjectName) {
                if ($thisSubjectID == 0) // Main Subject
                {
                    $thisSubjectID = $MainSubjectID;
                    $isSub = false;
                    $prefix = '';
                } else // Subject Component
                {
                    $isSub = true;
                    $prefix = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                }
                
                // ## Bold for Grand Mark
                $thisSubjectPrefix = ($thisSubjectID < 0) ? '<b>' : '';
                $thisSubjectSuffix = ($thisSubjectID < 0) ? '</b>' : '';
                
                // ## Get Setting Value
                $thisRangeType = $SettingArr[$thisSubjectID][$PositionType]['RangeType'];
                $thisRangeType = ($thisRangeType == '') ? 1 : $thisRangeType;
                $thisUpperLimit = $SettingArr[$thisSubjectID][$PositionType]['UpperLimit'];
                $thisUpperLimit = ($thisUpperLimit == '') ? 0 : $thisUpperLimit;
                $thisGreaterThanMark = $SettingArr[$thisSubjectID][$PositionType]['GreaterThanMark'];
                $thisGreaterThanMark = ($thisGreaterThanMark == '') ? 0 : $thisGreaterThanMark;
                $thisGreaterThanAverage = $SettingArr[$thisSubjectID][$PositionType]['GreaterThanAverage'];
                $thisGreaterThanAverage = ($thisGreaterThanAverage == '') ? 0 : $thisGreaterThanAverage;
                $thisDisplayPosition = $SettingArr[$thisSubjectID][$PositionType]['ShowPosition'];
                $thisDisplayPositionChecked = ($thisDisplayPosition == 1 || $thisDisplayPosition == '') ? "checked" : "";
                
                // Range Type Selection
                $thisID = 'RangeType[' . $thisSubjectID . '][' . $PositionType . ']';
                $thisClass = $thisElementClass . ' RangeType_' . $PositionType;
                $thisOnchange = 'js_ShowHide_Percentage_Symbol(this.value, ' . $thisSubjectID . ', \'' . $PositionType . '\')';
                $thisTypeSelection = $this->Get_Position_Display_Type_Selection($thisID, $thisRangeType, $thisOnchange, $thisClass);
                
                // Upper Limit Input
                $thisID = 'UpperLimit[' . $thisSubjectID . '][' . $PositionType . ']';
                $thisClass = $thisElementClass . ' ratebox UpperLimitTb UpperLimit_' . $PositionType;
                $thisLimitTb = '<input id="' . $thisID . '" name="' . $thisID . '" class="' . $thisClass . '" maxlength="3" value="' . $thisUpperLimit . '">';
                // '%' symbol
                $PercentageDisplay = 'style="display:none"';
                if ($thisRangeType == 2 || $thisRangeType == 3)
                    $PercentageDisplay = '';
                    $thisID = 'PercentageSymbolSpan_' . $PositionType . '_' . $thisSubjectID;
                    $thisClass = 'PercentageSymbolSpan_' . $PositionType;
                    $thisPercentageSymbol = '<span id="' . $thisID . '" ' . $PercentageDisplay . ' class="' . $thisClass . '"> %</span>';
                    
                    // Greater than Mark Input
                    $thisID = 'GreaterThanMark[' . $thisSubjectID . '][' . $PositionType . ']';
                    $thisClass = $thisElementClass . ' ratebox GreaterThanMarkTb GreaterThanMark_' . $PositionType;
                    $thisGreaterThanMarkTb = '<input id="' . $thisID . '" name="' . $thisID . '" class="' . $thisClass . '" maxlength="3" value="' . $thisGreaterThanMark . '">';
                    
                    // Greater than Average Mark Input
                    $thisID = 'GreaterThanAverage[' . $thisSubjectID . '][' . $PositionType . ']';
                    $thisClass = $thisElementClass . ' ratebox GreaterThanAverageTb GreaterThanAverage_' . $PositionType;
                    $thisGreaterThanAverageTb = '<input id="' . $thisID . '" name="' . $thisID . '" class="' . $thisClass . '" maxlength="3" value="' . $thisGreaterThanAverage . '">';
                    
                    // Display CheckBox
                    $thisID = 'DisplayPosition[' . $thisSubjectID . '][' . $PositionType . ']';
                    $thisClass = $thisElementClass . ' DisplayPositionCheckBox' . $PositionType . ' ' . $thisElementClass;
                    $thisDisplayCheckBox = '<input type="checkbox" id="' . $thisID . '" name="' . $thisID . '" class="' . $thisClass . '" value="1" ' . $thisDisplayPositionChecked . ' onclick="DisableRow(this)">';
                    
                    // Build Row
                    $css = ($displayCounter % 2 == 0) ? "tablerow1" : "tablerow2";
                    
                    $table .= '<tr class="tabletext ' . $css . '">';
                    $table .= '<td>' . $prefix . $thisSubjectPrefix . $SubjectName . $thisSubjectSuffix . '</td>';
                    $table .= '<td align="center">' . $thisTypeSelection . '</td>';
                    $table .= '<td align="center">' . $thisLimitTb . $thisPercentageSymbol . '</td>';
                    $table .= '<td align="center">' . $thisGreaterThanMarkTb . '</td>';
                    $table .= '<td align="center">' . $thisGreaterThanAverageTb . '</td>';
                    $table .= '<td align="center">' . $thisDisplayCheckBox . '</td>';
                    $table .= '</tr>';
            }
            $displayCounter ++;
        }
        $table .= '</table>';
        
        return $table;
    }
    
    function Get_Subject_Teacher_Comment_MaxLength_Settings_Table($ReportID)
    {
        global $intranet_session_language, $eReportCard, $Lang, $lreportcard_comment;
        
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        
        // ## Get Subjects
        $SubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection = 0, $TeacherID = '', $intranet_session_language, $ParDisplayType = 'Desc', $ExcludeCmpSubject = 0, $ReportID);
        
        // Get All Position Display Setting
        $MaxLengthInfoArr = $lreportcard_comment->Get_Comment_MaxLength_Info($ReportID);
        
        $x = '';
        $x .= '<table class="common_table_list_v30">' . "\r\n";
        $x .= '<thead>' . "\r\n";
        $x .= '<th class="sub_row_top" style="width:50%;">' . $eReportCard['Subject'] . '</th>' . "\r\n";
        $x .= '<th class="sub_row_top" style="width:50%;">' . $Lang['eReportCard']['CommentArr']['MaxNumberOfChar'] . '</th>' . "\r\n";
        $x .= '</thead>' . "\r\n";
        
        $x .= '<tbody>' . "\r\n";
        $displayCounter = 0;
        foreach ((array) $SubjectArr as $MainSubjectID => $SubjectComponentArr) {
            foreach ((array) $SubjectComponentArr as $thisSubjectID => $SubjectName) {
                if ($thisSubjectID == 0) {
                    // Main Subject
                    $thisSubjectID = $MainSubjectID;
                    $isSub = false;
                } else {
                    // Subject Component
                    $isSub = true;
                    continue;
                }
                
                $_maxLength = ($MaxLengthInfoArr[$thisSubjectID]['MaxLength'] == '') ? $this->Get_Subject_Teacher_Comment_MaxLength() : $MaxLengthInfoArr[$thisSubjectID]['MaxLength'];
                
                $x .= '<tr>';
                $x .= '<td>' . $SubjectName . '</td>';
                $x .= '<td><input id="stcMaxLength_' . $thisSubjectID . '" name="stcMaxLengthArr[' . $thisSubjectID . ']" class="textboxnum" value="' . $_maxLength . '" /></td>';
                $x .= '</tr>';
            }
        }
        $x .= '</tbody>' . "\r\n";
        $x .= '</table>' . "\r\n";
        
        return $x;
    }
    
    function Get_ApplyToAll_Image()
    {
        global $image_path, $LAYOUT_SKIN;
        
        $img = '<img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_assign.gif" width="12" height="12" border="0">';
        
        return $img;
    }
    
    function Get_Report_Card_Type_Display($ReportID, $ForSelection = 0, $Semester = '', $IsMainReport = '')
    {
        global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
        include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
        
        if ($Semester == '' || $IsMainReport == '') {
            $data = $this->returnReportTemplateBasicInfo($ReportID);
            $Semester = $data['Semester'];
            $IsMainReport = $data['isMainReport'];
        }
        
        // ## Report Type Display
        $ReportType = '';
        if ($Semester == 'F') {
            $ReportType = $eReportCard['WholeYearReport'];
            
            $thisColumnData = $this->returnReportTemplateColumnData($ReportID);
            $numOfColumn = count($thisColumnData);
            if ($numOfColumn > 0) {
                $TermTitleArr = array();
                $TermIDAry = array();
                for ($i = 0; $i < $numOfColumn; $i ++) {
                    $thisYearTermID = $thisColumnData[$i]['SemesterNum'];
                    
                    if (! in_array($thisYearTermID, (array) $TermIDAry)) {
                        // $ObjYearTerm = new academic_year_term($thisYearTermID);
                        // $TermTitleArr[] = $ObjYearTerm->Get_Year_Term_Name();
                        
                        $TermTitleArr[] = $this->returnSemesters($thisYearTermID);
                    }
                    
                    $TermIDAry[] = $thisYearTermID;
                }
                
                if ($ForSelection == 0)
                    $ReportType .= '<br />(' . implode(', ', $TermTitleArr) . ')';
                    else
                        $ReportType .= ' [' . implode(', ', $TermTitleArr) . ']';
                        
                        // [2014-1121-1759-15164] display BIBA report card type
                        if ($eRCTemplateSetting['Settings']['AutoSelectReportType']) {
                            $ReportType = $this->getBIBAReportName("", "", true, true);
                        }
            }
        } else {
            $TermReportTypeText = ($IsMainReport == 1) ? $eReportCard['Main'] : $eReportCard['Extra'];
            $ReportType = $TermReportTypeText . ' ' . $eReportCard['TermReport'];
            
            // $ObjYearTerm = new academic_year_term($Semester);
            $thisTermName = $this->returnSemesters($Semester);
            
            if ($ForSelection == 0)
                $ReportType .= '<br />(' . $thisTermName . ')';
                else
                    $ReportType .= ' [' . $thisTermName . ']';
                    
                    // [2014-1121-1759-15164] display BIBA report card type
                    if ($eRCTemplateSetting['Settings']['AutoSelectReportType']) {
                        $report_data = $this->returnReportTemplateBasicInfo($ReportID);
                        // [2015-1013-1434-16164]
                        // $form_num = intval($this->GET_FORM_NUMBER($report_data['ClassLevelID']));
                        $form_num = $this->GET_FORM_NUMBER($report_data['ClassLevelID']);
                        $sem_num = $this->Get_Semester_Seq_Number($Semester);
                        
                        $ReportType = $this->getBIBAReportName($form_num, $sem_num, $IsMainReport, false, $report_data['ReportSequence']);
                    }
        }
        
        return $ReportType;
    }
    
    // Modified by Marcus 20101011 (for template based grading scheme) (param changed)
    function Get_Subject_Name_Display($SubjectID, $ClassLevelID, $ReportID = '')
    {
        $SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
        $LangDisplay = $SubjectFormGradingSettings['LangDisplay'];
        
        $SubjectNameEn = $this->GET_SUBJECT_NAME_LANG($SubjectID, 'EN');
        $SubjectNameCh = $this->GET_SUBJECT_NAME_LANG($SubjectID, 'CH');
        
        $SubjectDisplay = '';
        if ($LangDisplay == 'en' || $LangDisplay == '')
            $SubjectDisplay = $SubjectNameEn;
            else if ($LangDisplay == 'ch')
                $SubjectDisplay = $SubjectNameCh;
                else if ($LangDisplay == 'both')
                    $SubjectDisplay = $SubjectNameCh . ' (' . $SubjectNameEn . ')';
                    
                    return $SubjectDisplay;
    }
    
    function Get_Grading_Scheme_Selection($ID_Name, $SelectedScheme, $OnChange = '', $Class = '')
    {
        global $eReportCard;
        
        $AllSchemeData = $this->GET_GRADING_SCHEME_MAIN_INFO();
        $numOfScheme = count($AllSchemeData);
        
        $SelectArr = array();
        for ($k = 0; $k < $numOfScheme; $k ++) {
            $thisSchemeID = $AllSchemeData[$k]['SchemeID'];
            $thisSchemeTitle = $AllSchemeData[$k]['SchemeTitle'];
            
            $SelectArr[$thisSchemeID] = $thisSchemeTitle;
        }
        
        $onchange = '';
        if ($OnChange != "")
            $onchange = 'onchange="' . $OnChange . '"';
            
            $class = '';
            if ($Class != "")
                $class = 'class="' . $Class . '"';
                
                $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $class;
                $selection = getSelectByAssoArray($SelectArr, $selectionTags, $SelectedScheme, $all = 0, $noFirst = 0, " --" . $eReportCard['SelectScheme'] . "-- ");
                
                return $selection;
    }
    
    function GetPromotionStatusSelection($ID_Name = '', $OnChange = '', $selected = '', $isAll = 1, $noFirst = 0, $isMultiple = 0, $OtherTagInfo = '')
    {
        global $eReportCard;
        
        for ($i=1; $i<=sizeof($eReportCard["PromotionStatus"]); $i++)
        {
            if(!isset($eReportCard["PromotionStatus"][$i])) {
                continue;
            }
            
            $ary[] = array($i, $eReportCard["PromotionStatus"][$i]);
        }
        
        $Multiple = $isMultiple ? "multiple" : "";
        $tags = " id=\"$ID_Name\" name=\"$ID_Name\" onchange=\"$OnChange\" $Multiple $OtherTagInfo ";
        $FirstTitle = $eReportCard["AllPromotionStatus"];
        $Selection = getSelectByArray($ary, $tags, $selected, $isAll, $noFirst, $FirstTitle);
        
        return $Selection;
    }
    
    function Get_School_Name($Bilingual = 0)
    {
        global $eReportCard, $intranet_session_language;
        
        if ($Bilingual)
            $SchoolName = $eReportCard['Template']['SchoolNameEn'] . ' ' . $eReportCard['Template']['SchoolNameCh'];
            else {
                if ($intranet_session_language == 'en')
                    $SchoolName = $eReportCard['Template']['SchoolNameEn'];
                    else
                        $SchoolName = $eReportCard['Template']['SchoolNameCh'];
            }
            
            return $SchoolName;
    }
    
    function Get_Number_Selection($ID_Name, $MinValue, $MaxValue, $SelectedValue = '', $Onchange = '', $noFirst = 0, $isAll = 0, $FirstTitle = '', $Name = '', $Class = '')
    {
        global $Lang;
        
        $onchange = '';
        if ($Onchange != "")
            $onchange = ' onchange="' . $Onchange . '" ';
            
            $class = '';
            if ($Class != "")
                $class = ' class="' . $Class . '" ';
                
                $Name = ($Name == '') ? $ID_Name : $Name;
                $selectionTags = ' id="' . $ID_Name . '" name="' . $Name . '" ' . $onchange . ' ' . $class;
                
                for ($i = $MinValue; $i <= $MaxValue; $i ++)
                    $selectArr[$i] = $i;
                    
                    if ($FirstTitle == '')
                        $FirstTitle = ($isAll) ? $Lang['Btn']['All'] : $Lang['Btn']['Select'];
                        
                        $FirstTitle = Get_Selection_First_Title($FirstTitle);
                        $NumberSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedValue, $isAll, $noFirst, $FirstTitle);
                        
                        return $NumberSelection;
    }
    
    function Get_Management_ClassTeacherComment_CommentView_UI($Keyword = '')
    {
        global $eReportCard, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;
        
        include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
        $linterface = new interface_html();
        
        // SearchBox
        $SearchBox = $linterface->Get_Search_Box_Div('Keyword', $Keyword);
        
        // Table Action Button
        $TableActionArr[] = array(
            "javascript:js_Form_Check();",
            $image_path . "/" . $LAYOUT_SKIN . "/icon_new.gif",
            "imgAdd",
            $eReportCard['ManagementArr']['ClassTeacherCommentArr']['AddToStudentComment']
        );
        $ActionBtnTable = $linterface->Get_Table_Action_Button($TableActionArr);
        
        $x = '';
        $x .= '<br />' . "\n";
        $x .= '<form id="CommentViewForm" name="CommentViewForm" method="post" onsubmit="return false;">' . "\n";
        $x .= '<table width="95%" border="0" cellpadding="0" cellspacing="0">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td align="right">' . $SearchBox . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        
        $x .= '<tr><td>&nbsp;</td></tr>';
        
        // ## Category List Table
        $x .= '<tr class="table-action-bar"><td align="right" valign="bottom">' . $ActionBtnTable . '</td></tr>';
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<div id="CommentTableDiv">' . "\n";
        $x .= $this->Get_Management_ClassTeacherComment_CommentView_Table($Keyword);
        $x .= '</div>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</form>' . "\n";
        
        return $x;
    }
    
    function Get_Management_ClassTeacherComment_CommentView_Table($Keyword = '', $ViewMode = 0, $CommentIDArr = '')
    {
        global $eReportCard, $Lang, $PATH_WRT_ROOT;
        
        include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
        $linterface = new interface_html();
        
        // ## Get Comments from Comment Bank
        $CommentArr = $this->Get_Comment_In_Comment_Bank($SubjectID = '', $Keyword, $CommentIDArr);
        $numOfComment = count($CommentArr);
        
        // ## Get Global Checkbox
        $CheckAllChk = $linterface->Get_Checkbox('CheckAll', 'CheckAll', '', $isChecked = 0, $Class = '', $Display = '', $Onclick = 'js_CheckAll(this.checked);', $Disabled = '');
        
        $x = '';
        $x .= '<table class="common_table_list" id="CommentContentTable">' . "\n";
        // # Header
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th style="width:20">#</th>' . "\n";
        $x .= '<th style="width:10%">' . $eReportCard['Code'] . '</th>' . "\n";
        $x .= '<th style="width:85%">' . $eReportCard['CommentContent'] . '</th>' . "\n";
        if (! $ViewMode)
            $x .= '<th style="width:20; text-align:center;">' . $CheckAllChk . '</th>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</thead>' . "\n";
            
            if ($numOfComment == 0) {
                $x .= '<tr><td colspan="4" align="center">' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>' . "\n";
            } else {
                $x .= '<tbody>' . "\n";
                for ($i = 0; $i < $numOfComment; $i ++) {
                    $thisCommentID = $CommentArr[$i]['CommentID'];
                    $thisCommentCode = $CommentArr[$i]['CommentCode'];
                    $thisCommentEn = $CommentArr[$i]['CommentEng'];
                    $thisCommentCh = $CommentArr[$i]['Comment'];
                    
                    $thisChkboxk = $linterface->Get_Checkbox('Comment_' . $thisCommentID, 'CommentIDArr[]', $thisCommentID, $isChecked = 0, $Class = 'CommentChk', $Display = '', $Onclick = 'js_Clicked_Comment(this.checked);', $Disabled = '');
                    
                    $x .= '<tr>' . "\n";
                    $x .= '<td valign="top">' . ($i + 1) . '</td>' . "\n";
                    $x .= '<td valign="top">' . $thisCommentCode . '</td>' . "\n";
                    $x .= '<td valign="top">' . nl2br(Get_Lang_Selection($thisCommentCh, $thisCommentEn)) . '</td>' . "\n";
                    if (! $ViewMode)
                        $x .= '<td align="center" valign="top">' . $thisChkboxk . '</td>' . "\n";
                        $x .= '</tr>' . "\n";
                }
                $x .= '</tbody>' . "\n";
            }
            $x .= '</table>' . "\n";
            
            return $x;
    }
    
    function Get_Management_ClassTeacherComment_CommentView_ChooseStudent_UI($CommentIDArr, $Keyword = '')
    {
        global $eReportCard, $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $i_general_from_class_group, $i_general_search_by_loginid, $i_general_search_by_inputformat;
        
        include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
        $linterface = new interface_html();
        
        // ## Navigation
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Management_ClassTeacherComment'],
            "javascript:js_Go_Back_To_Comment_List()"
        );
        $PAGE_NAVIGATION[] = array(
            $eReportCard['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent'],
            ""
        );
        $PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);
        
        // ## Get Form selection (show form which has report card only)
        $FormArr = $this->GET_ALL_FORMS($hasTemplateOnly = 1);
        $YearID = $FormArr[0]['ClassLevelID'];
        $CheckClassTeacher = ($this->IS_ADMIN_USER($_SESSION['UserID'])) ? 0 : 1;
        $FormSelection = $this->Get_Form_Selection('YearID', $YearID, 'js_Reload_Selection(this.value)', $noFirst = 1, $isAll = 0, $hasTemplateOnly = 1, $CheckClassTeacher);
        
        // ## Get Report selection
        // $ReportSelection = $this->Get_Report_Selection($YearID, '', 'ReportID');
        
        // ## Remarks Area
        $StudentSelectionRemarks = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $eReportCard['ManagementArr']['ClassTeacherCommentArr']['OnlyStudentInTheSelectedFormWillBeShown'], $others = "");
        
        // ## Choose Member Btn
        $btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:js_Select_Student_Pop_up();");
        
        // ## Auto-complete ClassName ClassNumber StudentName search
        $UserClassNameClassNumberInput = '';
        $UserClassNameClassNumberInput .= '<div style="float:left;">';
        $UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
        $UserClassNameClassNumberInput .= '</div>';
        
        // ## Auto-complete login search
        $UserLoginInput = '';
        $UserLoginInput .= '<div style="float:left;">';
        $UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
        $UserLoginInput .= '</div>';
        
        // ## Student Selection Box & Remove all Btn
        $MemberSelectionBox = $linterface->GET_SELECTION_BOX(array(), "name='SelectedUserIDArr[]' id='SelectedUserIDArr[]' class='select_studentlist' size='15' multiple='multiple'", "");
        $btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected_Student();");
        
        // ## Continue Button
        $btn_Submit = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick = "js_Add_Comment_To_Student();", $id = "Btn_Submit");
        
        // ## Cancel Button
        $btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick = "js_Go_Back_To_Comment_List()", $id = "Btn_Cancel");
        
        $x = '';
        $x .= '<br />' . "\n";
        $x .= '<form id="form1" name="form1" method="post" onsubmit="return false;">' . "\n";
        $x .= '<table width="100%" border="0" cellpadding"0" cellspacing="0">' . "\n";
        $x .= '<tr><td colspan="2" class="navigation">' . $PageNavigation . '</td></tr>' . "\n";
        $x .= '<table>' . "\n";
        
        $x .= '<br style="clear:both"/>' . "\n";
        
        $x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0">' . "\n";
        // ## Selected Comment(s)
        $x .= '<tr>' . "\n";
        $x .= '<td colspan="2">' . $linterface->GET_NAVIGATION2($eReportCard['ManagementArr']['ClassTeacherCommentArr']['SelectedComment']) . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td colspan="2">' . "\n";
        $x .= $this->Get_Management_ClassTeacherComment_CommentView_Table('', $ViewMode = 1, $CommentIDArr);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        
        $x .= '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";
        
        // ## Form and Report Card Selection
        $x .= '<tr>' . "\n";
        $x .= '<td colspan="2">' . $linterface->GET_NAVIGATION2($eReportCard['ManagementArr']['ClassTeacherCommentArr']['ChooseReportCard']) . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $eReportCard['FormName'] . '</td>' . "\n";
        $x .= '<td >' . $FormSelection . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $eReportCard['ReportCard'] . '</td>' . "\n";
        $x .= '<td class="tabletext"><div id="ReportSelectionDiv">' . $ReportSelection . '</div></td>' . "\n";
        $x .= '</tr>' . "\n";
        
        $x .= '<tr><td colspan="2">&nbsp;</td></tr>' . "\n";
        
        $x .= '<tr>' . "\n";
        $x .= '<td colspan="2">' . "\n";
        $x .= $linterface->GET_NAVIGATION2($eReportCard['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent']);
        $x .= '<br />' . "\n";
        $x .= $StudentSelectionRemarks;
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        
        // Group Info
        $x .= '<tr>' . "\n";
        $x .= '<td colspan="2">' . "\n";
        $x .= '<table width="90%" border="0" cellpadding"0" cellspacing="0" align="center">' . "\n";
//         $x .= '<tr>' . "\n";
//         $x .= '<td class="tabletext" >';
//         $x .= '<td class="tabletext">'. $eReportCard['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent'] . '</td>' . "\n";
//         $x .= '<td class="tabletext"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1"></td>' . "\n";
//         $x .= '<td class="tabletext" width="60%">' . $Lang['eBooking']['Settings']['ManagementGroup']['SelectedUser'] . '</td>' . "\n";
//         $x .= '</tr>' . "\n";
        $x .= '<tr>';
        $x .= '<td class="tablerow2" valign="top"><div id="divSelectStudent"></div></td>';
        $x .= '</tr>
                <tr>
				<td class="tablerow2" valign="top">
					<table width="100%" border="0" cellpadding="3" cellspacing="0">
					<tr>
						<td class="tabletext">' . $i_general_from_class_group . '</td>
					</tr>
					<tr>
						<td class="tabletext">' . $btn_ChooseMember . '</td>
					</tr>
					<tr>
						<td class="tabletext"><i>' . $Lang['General']['Or'] . '</i></td>
					</tr>
					<tr>
						<td class="tabletext">
							' . $i_general_search_by_inputformat . '
							<br />
							' . $UserClassNameClassNumberInput . '
						</td>
					</tr>
					<tr>
						<td class="tabletext"><i>' . $Lang['General']['Or'] . '</i></td>
					</tr>
					<tr>
						<td class="tabletext">
							' . $i_general_search_by_loginid . '
							<br />
							' . $UserLoginInput . '
						</td>
					</tr>
					</table>
				</td>
				<td class="tabletext" ><img src="' . $image_path . '/' . $LAYOUT_SKIN . '"/10x10.gif" width="10" height="1"></td>
				<td align="left" valign="top" width="70%">
					<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td align="left">
							' . $MemberSelectionBox . '
							' . $btn_RemoveSelected . '
						</td>
					</tr>
					<tr>
						<td>
							' . $linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* ' . $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']) . '
						</td>
					</tr>
					</table>
				</td>
                </tr>' . "\n";
        $x .= '<tr><td>&nbsp;</td></tr>' . "\n";
        $x .= '<tr><td colspan="4"><div class="edit_bottom"><br />' . $btn_Submit . '&nbsp;' . $btn_Cancel . '</div></td></tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</td>' . "\n";
//         $X .= '</td>'. "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        
        $x .= '<input type="hidden" id="Keyword" name="Keyword" value="' . intranet_htmlspecialchars($Keyword) . '" />' . "\n";
        $x .= '<input type="hidden" name="CommentIDArr" id="CommentIDArr" value="' . rawurlencode(serialize($CommentIDArr)) . '">' . "\n";
        $x .= '</form>' . "\n";
        
        return $x;
    }
    
    // Trial Promotion Report
    function Get_Trial_Promotion_Index_UI()
    {
        global $eReportCard, $linterface, $image_path, $LAYOUT_SKIN, $Lang, $eRCTemplateSetting, $sys_custom, $lreportcard;
        
        $x .= '<form id="form1" name="form1" method="POST">';
        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">' . "\n";
        // preset was here before <tr></tr>
        $x .= '<tr>' . "\n";
        $x .= '<td  colspan="2" >' . "\n";
        
        // Get Form selection (show form which has report card only)
        $libForm = new Year();
        $FormArr = $libForm->Get_All_Year_List();
        $ClassLevelID = $FormArr[0]['YearID'];
        $FormSelection = $this->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'js_Reload_Selection(this.value)', $noFirst = 1);
        
        // Select All Class Btn
        $SelectAllClassBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearClassIDArr[]')");
        
        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
        
        // form selection
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['Form'] . '</td>' . "\n";
        $x .= '<td >' . $FormSelection . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // class selection
        $x .= '<tr class="ClassSelectOption">' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['Class'] . '</td>' . "\n";
        $x .= '<td id="ClassSelectionTD">' . "\n";
        $x .= '<span id="ClassSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!-- load on ajax --></span>' . $SelectAllClassBtn . "\n";
        $x .= '<br>' . "\n";
        $x .= $linterface->MultiSelectionRemark() . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        
        // Report Selection
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['Reports'] . '</td>' . "\n";
        $x .= '<td ><span id="ReportSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax--></span></td>' . "\n";
        $x .= '</tr>' . "\n";
        
        // CSV / Html
        $x .= '<tr>' . "\n";
        $x .= '<td  class="field_title">' . $eReportCard['ViewFormat'] . '</td>' . "\n";
        $x .= '<td class="tabletext">';
        $x .= '<input type="radio" id="ViewFormatHTML" name="ViewFormat" value="1" checked ><label for="ViewFormatHTML">' . $eReportCard['HTML'] . '</label>&nbsp;';
        $x .= '<input type="radio" id="ViewFormatCSV" name="ViewFormat" value="0"><label for="ViewFormatCSV">' . $eReportCard['CSV'] . '</label>';
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table><br><br>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        
        $x .= '<div class="edit_bottom_v30">';
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Check_Form();", "submitBtn");
        $x .= '</div>';
        $x .= '</form>' . "\n";
        
        return $x;
    }
    
    function Get_Trial_Promotion_Report($ParReportID, $ParYearClassIDArr, $ParClassLevelID, $ParViewFormat)
    {
        global $eReportCard, $linterface, $PATH_WRT_ROOT;
        
        $StudentInfoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ParReportID, $ParClassLevelID, $ParYearClassIDArr, $ParStudentID = "", $ReturnAsso = 0, $isShowStyle = 0);
        $StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
        $StudentInfoAssoArr = BuildMultiKeyAssoc($StudentInfoArr, 'UserID');
        
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ParReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $FormNumber = $this->GET_FORM_NUMBER($ClassLevelID);
        $FormName = $this->returnClassLevel($ClassLevelID);
        
        $YearTermID = $ReportInfoArr['Semester'];
        $YearTermID = ($YearTermID == 'F') ? 0 : $YearTermID;
        
        // $TrialPromotionStudentArr[$StudentID][Key] = Data
        $TrialPromotionStudentArr = $this->Get_Student_Trial_Promotion_Status($ParReportID, $StudentIDArr);
        
        if ($ParViewFormat == 'html') {
            $ReportTitle = str_replace(':_:', '<br />', $ReportInfoArr['ReportTitle']);
            
            $table = '';
            $table .= "<table class=\"font_12pt\" style=\"width:100%; text-align:left; font-family:'arial, \"lucida console\", sans-serif';\">\n";
            $table .= "<tr>\n";
            $table .= "<td style=\"width:10%;\">" . $eReportCard['Form'] . "</td>\n";
            $table .= "<td style=\"width:3%;\"> : </td>\n";
            $table .= "<td>" . $FormName . "</td>\n";
            $table .= "</tr>\n";
            $table .= "<tr>\n";
            $table .= "<td style=\"vertical-align:text-top;\">" . $eReportCard['TrialPromotion']['Report'] . "</td>\n";
            $table .= "<td style=\"vertical-align:text-top;\"> : </td>\n";
            $table .= "<td style=\"vertical-align:text-top;\">" . $ReportTitle . "</td>\n";
            $table .= "</tr>\n";
            $table .= "<table>\n";
            
            $table .= "<table id='ResultTable' class='GrandMSContentTable border_table' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
            $table .= "<thead>\n";
            $table .= "<tr>\n";
            $table .= "<th style=\"width:5%\">" . '#' . "</th>\n";
            $table .= "<th style=\"width:8%\">" . $eReportCard['Class'] . "</th>\n";
            $table .= "<th style=\"width:5%\">" . $eReportCard['ClassNo'] . "</th>\n";
            $table .= "<th style=\"width:10%\" align='left'>" . $eReportCard['Student'] . "</th>\n";
            $table .= "<th style=\"width:22%\" align='left'>" . $eReportCard['TrialPromotion']['BelowOneSubject'] . "</th>\n";
            $table .= "<th style=\"width:7%\">" . $eReportCard['TrialPromotion']['Conduct'] . "</th>\n";
            $table .= "<th style=\"width:7%\">" . $eReportCard['TrialPromotion']['Truant'] . "</th>\n";
            $table .= "<th style=\"width:7%\">" . $eReportCard['TrialPromotion']['Late'] . "</th>\n";
            $table .= "<th style=\"width:7%\">" . $eReportCard['TrialPromotion']['SickLeave'] . "</th>\n";
            $table .= "<th style=\"width:7%\">" . $eReportCard['TrialPromotion']['DisapprovedLeave'] . "</th>\n";
            $table .= "<th style=\"width:7%\">" . $eReportCard['TrialPromotion']['EarlyLeave'] . "</th>\n";
            $table .= "<th style=\"width:7%\">" . $eReportCard['TrialPromotion']['LeaveWithReason'] . "</th>\n";
            $table .= "</tr>\n";
            $table .= "</thead>\n";
            
            $table .= "<tbody>\n";
            
            $counter = 1;
            foreach ((array) $TrialPromotionStudentArr as $thisStudentID => $thisStudentTrialInfoArr) {
                $thisStudentInfoArr = $StudentInfoAssoArr[$thisStudentID];
                // debug_r($thisStudentTrialInfoArr['Late']);
                
                $table .= "<tr>\n";
                $table .= "<td align='center'>" . $counter . "</td>\n";
                $table .= "<td align='center'>" . Get_Table_Display_Content($thisStudentInfoArr['ClassTitleEn']) . "</td>\n";
                $table .= "<td align='center'>" . Get_Table_Display_Content($thisStudentInfoArr['ClassNumber']) . "</td>\n";
                $table .= "<td align='left'>" . Get_Table_Display_Content($thisStudentInfoArr['StudentName']) . "</td>\n";
                $table .= "<td align='left'>" . Get_Table_Display_Content($thisStudentTrialInfoArr['Below1Subject']) . "</td>\n";
                $table .= "<td align='center'>" . Get_Table_Display_Content($thisStudentTrialInfoArr['Conduct']) . "</td>\n";
                $table .= "<td align='center'>" . Get_Table_Display_Content($thisStudentTrialInfoArr['Truant']) . "</td>\n";
                $table .= "<td align='center'>" . Get_Table_Display_Content($thisStudentTrialInfoArr['Late']) . "</td>\n";
                $table .= "<td align='center'>" . Get_Table_Display_Content($thisStudentTrialInfoArr['SickLeave']) . "</td>\n";
                $table .= "<td align='center'>" . Get_Table_Display_Content($thisStudentTrialInfoArr['DisapprovedLeave']) . "</td>\n";
                $table .= "<td align='center'>" . Get_Table_Display_Content($thisStudentTrialInfoArr['EarlyLeave']) . "</td>\n";
                $table .= "<td align='center'>" . Get_Table_Display_Content($thisStudentTrialInfoArr['LeaveWithReason']) . "</td>\n";
                $table .= "</tr>\n";
                
                $counter ++;
            }
            $table .= "</tbody>\n";
            $table .= "</table>\n";
            
            $allTable = '';
            $allTable .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
							<tr>
								<td align="center"><h1>' . $eReportCard['TrialPromotion']['ReportTitle'] . '</h1></td>
							</tr>
							<tr>
								<td align="center"><div style="width:720px;">' . $table . '</div></td>
							</tr>
						</table>';
            
            $css = $this->Get_GrandMS_CSS();
            echo $allTable . $css;
        } else if ($ParViewFormat == 'csv') {
            $ExportHeaderArr = array();
            $ExportContentArr = array();
            $lexport = new libexporttext();
            
            $trialCSVheaderArr = $this->Get_Trial_Promotion_Report_Header();
            
            $ColumnTitleArr = array();
            $ColumnTitleArr['En'] = Get_Array_By_Key($trialCSVheaderArr, 'En');
            $ColumnTitleArr['Ch'] = Get_Array_By_Key($trialCSVheaderArr, 'Ch');
            
            $PropertyArr = Get_Array_By_Key($trialCSVheaderArr, 'Property');
            
            $ExportHeaderArr = $lexport->GET_EXPORT_HEADER_COLUMN($ColumnTitleArr, $PropertyArr);
            
            // Content
            $i_counter = 0;
            foreach ((array) $TrialPromotionStudentArr as $thisStudentID => $thisStudentTrialInfoArr) {
                $thisStudentInfoArr = $StudentInfoAssoArr[$thisStudentID];
                
                $j_counter = 0;
                
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentInfoArr['ClassTitleEn'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentInfoArr['ClassNumber'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentInfoArr['StudentName'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentTrialInfoArr['Below1Subject'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentTrialInfoArr['Conduct'];
                // $ExportContentArr[$i_counter][$j_counter++] = $thisStudentTrialInfoArr['Late'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentTrialInfoArr['Truant'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentTrialInfoArr['Late'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentTrialInfoArr['SickLeave'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentTrialInfoArr['DisapprovedLeave'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentTrialInfoArr['EarlyLeave'];
                $ExportContentArr[$i_counter][$j_counter ++] = $thisStudentTrialInfoArr['LeaveWithReason'];
                $i_counter ++;
            }
            
            $ReportTitle = str_replace(" ", "_", $ReportTitle);
            $ReportTitle = str_replace(":_:", "_", $ReportTitle);
            $filename = $ReportTitle . ".csv";
            
            $export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportContentArr, $ExportHeaderArr);
            
            // Output the file to user browser
            $lexport->EXPORT_FILE($filename, $export_content);
        }
    }
    
    // /////end//////////
    function Get_Master_Report_Index_UI($PresetID = '')
    {
        global $eReportCard, $linterface, $image_path, $LAYOUT_SKIN, $Lang, $eRCTemplateSetting, $sys_custom, $lreportcard;
        
        $display_Active_Year_Options = false;
        if ($eRCTemplateSetting['Report']['SupportAllActiveYear']){
            $display_Active_Year_Options = true;
        }
        
        $Display_SD_Options = false;
        if ($eRCTemplateSetting['OrderPositionMethod'] == "WeightedSD" || $eRCTemplateSetting['OrderPositionMethod'] == "SD") {
            $Display_SD_Options = true;
        }
        
        // [2014-1014-1144-21164]
        // Added: 2015-01-30 - Option: Marksheet Score
        $Display_MarksheetScore_Option = false;
        if ($eRCTemplateSetting['Report']['MasterReport']['showMarksheetScoreOption']) {
            $Display_MarksheetScore_Option = true;
        }
        
        $Display_Stream_Options = false;
        if ($sys_custom['Class']['ClassGroupSettings'] == true && $eRCTemplateSetting['MasterReport']['AlwaysApplyOrderMeritStream']) {
            $Display_Stream_Options = true;
        }
        // $Display_Stream_Options = false; // function not ready => set to false now
        
        $Display_Effort_Options = false;
        if ($lreportcard->IsEnableMarksheetExtraInfo) {
            $Display_Effort_Options = true;
        }
        
        $Display_ActualAverage_Options = false;
        if ($eRCTemplateSetting['Calculation']['ActualAverage']) {
            $Display_ActualAverage_Options = true;
        }
        
        $OrderOption = array();
        
        $ctr = 0;
        // wrapper
        $x .= '<form id="form1" name="form1" action="master_report.php" method="POST" target="_blank">';
        
        // Preset Layer
        // prepare Preset list
        $PresetArr = $this->Get_Master_Report_Preset_List();
        
        $layervisible = $PresetID != '' ? "block" : "none";
        $x .= '<div style="width: 250px; display:' . $layervisible . '; visibility:visible; position:absolute; right:30px; top:183px;"  class="selectbox_layer" id="PresetLayer">';
        $x .= '<table width="250px" border="0" cellspacing="0" cellpadding="2">' . "\n";
        // data display
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext" >' . $eReportCard['MasterReport']['ReportPreset'] . '</td>' . "\n";
        $x .= '<td class="tabletext" align="right"><a href="javascript:toggleLayer();" class="contenttool">' . $Lang['Btn']['Close'] . '</a></td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext" nowrap>';
        $defaultVal = $eReportCard['MasterReport']['DefaultPresetName'];
        $checked = $PresetID == '' ? "checked" : "";
        $x .= '<input type="radio" id="Preset_new" name="PresetID" value="new" ' . $checked . '>&nbsp;';
        $x .= '<label for="Preset_new"></label><input name="PresetName" id="PresetName" value="' . $defaultVal . '" onclick="if(this.value==\'' . $defaultVal . '\') this.value=\'\';"><br>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '<td class="tabletext" nowrap>';
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        foreach ((array) $PresetArr as $val => $label) {
            $x .= '<tr>' . "\n";
            $x .= '<td class="tabletext" >';
            $checked = ($PresetID == $val) ? "checked" : "";
            $x .= '<input type="radio" id="Preset' . $val . '" name="PresetID" value="' . $val . '" ' . $checked . '>&nbsp;';
            $x .= '<label for="Preset' . $val . '">' . $label . '</label><br>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '<td class="tabletext" nowrap align=right>';
            $x .= $linterface->GET_LNK_DELETE("javascript:delete_preset($val)");
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
        }
        $x .= '<tr><td class="dotline" colspan=2></td></tr>' . "\n";
        
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext" nowrap align="center" colspan=2>' . "\n";
        $x .= $linterface->GET_SMALL_BTN($Lang['Btn']['Apply'], "button", "preset();") . "\n";
        $x .= '<img src="' . $image_path . '/' . $LAYOUT_SKIN . '"/10x10.gif" width="10" height="1">' . "\n";
        $x .= $linterface->GET_SMALL_BTN($Lang['Btn']['Save'], "button", "save_preset();", "submitBtn");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</div>' . "\n";
        
        $x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5">' . "\n";
        if ($this->IS_ADMIN_USER($_SESSION['UserID']) || $this->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) {
            $x .= '<tr>' . "\n";
            $x .= '<td colspan="2" align="right"><a href="javascript:toggleLayer();" class="contenttool">' . $eReportCard['MasterReport']['ReportPreset'] . '</a></td>' . "\n";
            $x .= '</tr>' . "\n";
        }
        $x .= '<tr>' . "\n";
        $x .= '<td  colspan="2" >' . "\n";
        // #### Main Content Start

        // ## Report Settting
        // Get Form selection (show form which has report card only)
        $libForm = new Year();
        $FormArr = $libForm->Get_All_Year_List();
        $ClassLevelID = $FormArr[0]['YearID'];
        $checkPermission = ($this->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $this->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) ? 0 : 3;
        $FormSelection = $this->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'js_Reload_Selection(this.value)', $noFirst = 1, $isAll = 0, $hasTemplateOnly = 1, $checkPermission);

        // Get Active Year selection
        if($display_Active_Year_Options){
            $ActiveYearSelection = $this->Get_eRC_Active_Year_Selection('targetActiveYear', $targetActiveYear, ' js_Reload_Active_Year_Setting() ');
        }

        $x .= $linterface->GET_NAVIGATION2($eReportCard['MasterReport']['ReportOption']) . "&nbsp;&nbsp;";
        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
        // active year selection
        if($display_Active_Year_Options){
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['SchoolYear']. '</td>' . "\n";
            $x .= '<td >' . $ActiveYearSelection . '</td>' . "\n";
            $x .= '</tr>' . "\n";
        }
        // form selection
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Form'] . '</td>' . "\n";
        $x .= '<td id="FormSelectionTD">' . $FormSelection . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Report Selection
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Reports'] . '</td>' . "\n";
        $x .= '<td ><span id="ReportSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax--></span></td>' . "\n";
        $x .= '</tr>' . "\n";
        // Report Column Selection
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['ReportColumn'] . '</td>' . "\n";
        $x .= '<td ><span id="ReportColumnSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax--></span></td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table><br><br>' . "\n";

        // ## Student Display Options Start
        // Get Term Selection
        $TermSelection = $this->Get_Term_Selection("TermID", $lreportcard->schoolYearID, '', 'js_Reload_Subject_Group_Selection()', 1, 0, 1);

        // Select All Class Btn
        $SelectAllClassBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearClassIDArr[]')");
        $SelectAllTermBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Term()");
        $SelectAllSubjBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Subject_Group_Subject()");
        $SelectAllSubjectGroupBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('SubjectGroupIDArr[]')");

        // prepare data display options
        $DataDisplayOptions = array();
        $DataDisplayOptions["UserLogin"] = $Lang["General"]["UserLogin"];
        $DataDisplayOptions["ClassName"] = $eReportCard["Class"];
        $DataDisplayOptions["ClassNumber"] = $eReportCard["ClassNumber"];
        $DataDisplayOptions["EnglishName"] = $eReportCard["EnglishName"];
        $DataDisplayOptions["ChineseName"] = $eReportCard["ChineseName"];
        $DataDisplayOptions["Gender"] = $eReportCard["Gender"];

        if ($eRCTemplateSetting['MasterReport']['ShowRegNoForStudent']) {
            $DataDisplayOptions["RegNo"] = $eReportCard['MasterReport']['RegistrationNo'];
        }

        $OrderOption = array_merge($OrderOption, $DataDisplayOptions);

        $x .= $linterface->GET_NAVIGATION2($eReportCard['MasterReport']['StudentDisplayOption']) . "&nbsp;&nbsp;";
        $x .= '<span id="spanShowOption' . $ctr . '" style="display:none"><a href="javascript:showOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['ShowOption'] . '</a></span>';
        $x .= '<span id="spanHideOption' . $ctr . '"><a href="javascript:hideOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['HideOption'] . '</a></span>';
        $x .= '<table id="table' . $ctr . '" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
        // select from
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['SelectFrom'] . '</td>' . "\n";
        $x .= '<td >';
        $x .= $linterface->Get_Radio_Button('SelectFromClass', 'SelectFrom', 'class', 1, "SelectFrom", $eReportCard['Class'], "js_Toggle_Student_Selection()") . "\n";
        $x .= $linterface->Get_Radio_Button('SelectFromSubjectGroup', 'SelectFrom', 'subjectgroup', 0, "SelectFrom", $eReportCard['SubjectGroup'], "js_Toggle_Student_Selection()") . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // class selection
        $x .= '<tr class="ClassSelectOption">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Class'] . '</td>' . "\n";
        // $x .= '<td ><span id="ClassSelectionSpan">'.$linterface->Get_Ajax_Loading_Image().'<!-- load on ajax --></span>'.$SelectAllClassBtn.'</td>'."\n";
        $x .= '<td id="ClassSelectionTD">' . "\n";
        $x .= '<span id="ClassSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!-- load on ajax --></span>' . $SelectAllClassBtn . "\n";
        $x .= '<br>' . "\n";
        $x .= $linterface->MultiSelectionRemark() . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Term selection
        $x .= '<tr class="SubjectGroupSelectOption">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Term'] . '</td>' . "\n";
        $x .= '<td id="TermSelectionTD">' . "\n";
        $x .= $TermSelection . $SelectAllTermBtn . "\n";
        $x .= '<br>' . "\n";
        $x .= $linterface->MultiSelectionRemark() . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Subject Selection
        $x .= '<tr class="SubjectGroupSelectOption">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard["Subject"] . '</td>' . "\n";
        $x .= '<td >' . "\n";
        $x .= '<span id="StdSubjectSelectionSpan">' . "\n";
        $x .= $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax-->' . "\n";
        $x .= '</span>' . "\n";
        $x .= $SelectAllSubjBtn . '' . "\n";
        $x .= '<br>' . "\n";
        $x .= $linterface->MultiSelectionRemark() . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Subject Group Selection
        $x .= '<tr class="SubjectGroupSelectOption">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['SubjectGroup'] . '</td>' . "\n";
        $x .= '<td id="SubjectGroupSelectionTD">' . "\n";
        $x .= '<span id="SubjectGroupSelectionSpan">' . "\n";
        $x .= $linterface->Get_Ajax_Loading_Image() . '<!-- load on ajax -->' . "\n";
        $x .= '</span>' . "\n";
        $x .= $SelectAllSubjectGroupBtn . "\n";
        $x .= '<br>' . "\n";
        $x .= $linterface->MultiSelectionRemark() . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // display format
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['DisplayFormat'] . '</td>' . "\n";
        $x .= '<td >';
        // $x .= '<input type="radio" id="FormSummaryRadio" name="DisplayFormat" value="FormSummary" checked>'."\n";
        $x .= $linterface->Get_Radio_Button('FormSummaryRadio', 'DisplayFormat', 'FormSummary', 1, "DisplayFormat", "", "js_Toggle_Display_Class_Teacher()") . "\n";
        $x .= '<label for="FormSummaryRadio" class="ClassSelectOption">' . $eReportCard['MasterReport']['ClassDisplayArr']['ShowFormSummary'] . '</label>' . "\n";
        $x .= '<label for="FormSummaryRadio" class="SubjectGroupSelectOption">' . $eReportCard['MasterReport']['SubjectGroupDisplayArr']['ShowFormSummary'] . '</label>' . "\n";
        $x .= '<br>';
        // $x .= '<input type="radio" id="ClassSummaryRadio" name="DisplayFormat" value="ClassSummary">'."\n";
        $x .= $linterface->Get_Radio_Button('ClassSummaryRadio', 'DisplayFormat', 'ClassSummary', 0, "DisplayFormat", "", "js_Toggle_Display_Class_Teacher()") . "\n";
        $x .= '<label for="ClassSummaryRadio" class="ClassSelectOption">' . $eReportCard['MasterReport']['ClassDisplayArr']['ShowClassSummary'] . '</label>' . "\n";
        $x .= '<label for="ClassSummaryRadio" class="SubjectGroupSelectOption">' . $eReportCard['MasterReport']['SubjectGroupDisplayArr']['ShowClassSummary'] . '</label>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // data display
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['DataDisplay'] . '</td>' . "\n";
        $x .= '<td >';
        $x .= $linterface->Get_Checkbox('Std_All', 'Std_All', '', $isChecked = 1, $Class = '', $Lang['Btn']['SelectAll'], "jsSelectAllCheckbox(this.checked, 'StudentDataDisplayChk'); refreshSortingTable();", $Disabled = '');
        $x .= '<br>' . "\n";
        $i = 0;
        $x .= '<ul>' . "\n";
        foreach ((array) $DataDisplayOptions as $val => $label) {

            $x .= '<li>' . "\n";
            $x .= $linterface->Get_Checkbox('Std' . $val, 'StudentData[]', $val, $isChecked = 1, 'StudentDataDisplayChk', $label, "jsCheckUnSelectAll(this.checked, 'Std_All'); refreshSortingTable();", $Disabled = '');

            // add option "force the name to be displayed in one line" after the English and Chinese Name Selection
            if ($val == 'EnglishName' || $val == 'ChineseName') {
                $val = 'Std' . trim($val) . 'NoWrap';
                $label = $eReportCard['MasterReport']['DisplayNameInOneLine'];
                $x .= '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                $x .= $linterface->Get_Checkbox($val, $val, '1', $isChecked = 0, '', $label, $onclick = "", $Disabled = '');
            }
            $x .= '</li>' . "\n";
            $i ++;
        }
        $x .= '</ul>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table><br><br>' . "\n";
        $ctr ++;
        // ## Student Display Options End

        // ## Subject Display Options Start
        // Select All Subject Btn
        $SelectAllSubjBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "SelectAll(document.form1.elements['SubjectIDArr[]'])");

        // Name Display Selection
        $NameSelectArr = array(
            "Desc",
            "Abbr",
            "ShortName"
        );

        // prepare data display options
        $DataDisplayOptions = array();
        $DataDisplayOptions["Mark"] = $eReportCard["Mark"];
        $DataDisplayOptions["RawMark"] = $eReportCard["RawMarks"];
        if ($Display_SD_Options) {
            $DataDisplayOptions["SDScore"] = $eReportCard["StandardScore"];
            $DataDisplayOptions["RawSDScore"] = $eReportCard["RawStandardScore"];
        }
        $DataDisplayOptions["Grade"] = $eReportCard["Grade"];
        // Added: 2015-01-30 - Option: Marksheet Score [2014-1014-1144-21164]
        if ($Display_MarksheetScore_Option) {
            $DataDisplayOptions["MSScore"] = $eReportCard["MSScore"];
        }
        $DataDisplayOptions["OrderMeritClass"] = $eReportCard["ClassPosition"];
        $DataDisplayOptions["OrderMeritForm"] = $eReportCard["FormPosition"];
        $DataDisplayOptions["OrderMeritSubjectGroup"] = $eReportCard['Template']["SubjGroupPosition"];
        if ($Display_Stream_Options) {
            $DataDisplayOptions["OrderMeritStream"] = $eReportCard["StreamPosition"];
        }
        $OrderOption = array_merge($OrderOption, $DataDisplayOptions);

        // prepare other display options
        $OtherDataDisplayOptions = array();
        if ($Display_Effort_Options) {
            $OtherDataDisplayOptions["Effort"] = $eReportCard['ExtraInfoLabel'];
        }
        $OtherDataDisplayOptions["SubjectTeacherComment"] = $eReportCard['Template']['SubjectTeacherComment'];
        if ($eRCTemplateSetting['MasterReport']['DisplayPersonalCharacteristicsInMasterReport']) {
            $OtherDataDisplayOptions["PersonalCharacteristics"] = $eReportCard['PersonalCharacteristics'];
        }

        // No Wrap Option (will add an check for avoid word wrapping for th field)
        $NoWrapOptionField = array(
            "SubjectTeacherComment"
        );

        $x .= $linterface->GET_NAVIGATION2($eReportCard['MasterReport']['SubjectDisplayOption']) . "&nbsp;&nbsp;";
        $x .= '<span id="spanShowOption' . $ctr . '" style="display:none"><a href="javascript:showOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['ShowOption'] . '</a></span>';
        $x .= '<span id="spanHideOption' . $ctr . '"><a href="javascript:hideOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['HideOption'] . '</a></span>';
        $x .= '<table id="table' . $ctr . '" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
        // Subject Selection
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard["Subject"] . '</td>' . "\n";
        $x .= '<td >' . "\n";
        $x .= '<div class="SubjectGroupSelectOption">' . $linterface->Get_Checkbox('ShowCorrespondingSubjectOnly', 'ShowCorrespondingSubjectOnly', 1, $isChecked = 0, $Class = '', $eReportCard['MasterReport']['ShowSubjectGroupSubjectOnly'], "js_Toggle_Subject_Selection()", $Disabled = '') . '</div>' . "\n";
        $x .= '<span id="SubjectSelectionSpan">' . "\n";
        $x .= $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax-->' . "\n";
        $x .= '</span>' . "\n";
        $x .= $SelectAllSubjBtn . "\n";
        $x .= '<br>' . "\n";
        $x .= $linterface->MultiSelectionRemark() . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Subject Name Display
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['SubjectDisplay'] . '</td>' . "\n";
        $x .= '<td >' . "\n";
        $i = 0;
        $checked = "CHECKED";
        foreach ((array) $NameSelectArr as $val) {
            $x .= '<input type="radio" id="SubjectDisplay' . $i . '" name="SubjectDisplay" value="' . $val . '" ' . $checked . '>';
            $x .= '<label for="SubjectDisplay' . $i . '">' . $eReportCard[$val] . '</label>&nbsp;' . "\n";
            $checked = '';
            $i ++;
        }
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Display Subject with All NA
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['DisplaySubjectWithAllNA'] . '</td>' . "\n";
        $x .= '<td >';
        $x .= $linterface->Get_Checkbox('DisplayAllNASubject', 'DisplayAllNASubject', '1', $isChecked = 1, $Class = '', '', "", $Disabled = '');
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // # Display Empty for NA Subject
        // $x .= '<tr>'."\n";
        // $x .= '<td class="field_title" width="30%">'.$eReportCard['MasterReport']['EmptySubjectDataForNA'].'</td>'."\n";
        // $x .= '<td><input type="checkbox" id="EmptySubjectDataForNA" name="EmptySubjectDataForNA" value=1 CHECKED></td>'."\n";
        // $x .= '</tr>'."\n";
        // data display
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['ColumnDataDisplay'] . '</td>' . "\n";
        $x .= '<td >';
        $x .= $linterface->Get_Checkbox('SD_All', 'SD_All', '', $isChecked = 1, $Class = '', $Lang['Btn']['SelectAll'], "jsSelectAllCheckbox(this.checked, 'SubjectDataChk')", $Disabled = '');
        $x .= '<br>' . "\n";
        $x .= '<ul>' . "\n";
        foreach ((array) $DataDisplayOptions as $val => $label) {
            // Added MSScore - ShowStyle [2014-1014-1144-21164]
            if (in_array($val, array(
                "Mark",
                "RawMark",
                "Grade",
                "MSScore"
            ))) {
                $StyleChk = '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                $StyleChk .= $linterface->Get_Checkbox('SD' . $val . 'Style', 'ShowStyle[]', $val, $isChecked = 0, 'ShowStyleChk', $eReportCard['MasterReport']['FormatWithStyle'], "", $Disabled = '');
                // $js_Disable_Style = "js_Disable_Style('SD{$val}Style', 'SD{$val}')";
            } else {
                $StyleChk = $js_Disable_Style = '';
            }
            $x .= '<li>' . "\n";
            $x .= '<span id="' . $val . 'Span">' . "\n";
            $x .= $linterface->Get_Checkbox('SD' . $val, 'SubjectData[]', $val, $isChecked = 1, 'SubjectDataChk', $label, "jsCheckUnSelectAll(this.checked, 'SD_All'); $js_Disable_Style", $Disabled = '');
            $x .= '</span>' . "\n";
            $x .= $StyleChk . "\n";
            $x .= '</li>' . "\n";
        }
        $x .= '</ul>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Other Data
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['SubjectDataDisplay'] . '</td>' . "\n";
        $x .= '<td >';
        if (count($OtherDataDisplayOptions) > 1) // show select all only if there are more than 1 option
        {
            $x .= $linterface->Get_Checkbox('SOD_All', 'SOD_All', '', $isChecked = 1, $Class = '', $Lang['Btn']['SelectAll'], "jsSelectAllCheckbox(this.checked, 'SubjectOtherDataChk')", $Disabled = '');
            $x .= '<br>' . "\n";
        }

        $x .= '<ul>' . "\n";
        foreach ((array) $OtherDataDisplayOptions as $val => $label) {
            $StyleChk = '';
            if (in_array($val, $NoWrapOptionField)) {
                $ID_Name = 'SOD' . $val . 'NoWrap';
                $StyleChk = '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                $StyleChk .= $linterface->Get_Checkbox($ID_Name, $ID_Name, 1, $isChecked = 0, '', $eReportCard['MasterReport']['NoWrap'], "", $Disabled = '');
            }

            $x .= '<li>' . "\n";
            $x .= '<span id="' . $val . 'Span">' . "\n";
            $x .= $linterface->Get_Checkbox('SOD' . $val, 'SubjectOtherData[]', $val, $isChecked = 1, 'SubjectOtherDataChk', $label, "jsCheckUnSelectAll(this.checked, 'SOD_All'); $js_Disable_Style", $Disabled = '');
            $x .= '</span>' . "\n";
            $x .= $StyleChk . "\n";
            $x .= '</li>' . "\n";
        }
        $x .= '</ul>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table><br><br>' . "\n";
        $ctr ++;
        // ## Subject Display Options End

        // ## Grand Mark Display Options Start
        // prepare data display options
        $DataDisplayOptions = array();
        $DataDisplayOptions["Promotion"] = $eReportCard["Promotion"];
        $DataDisplayOptions["GrandTotal"] = $eReportCard["GrandTotal"];
        $DataDisplayOptions["GrandAverage"] = $eReportCard["GrandAverage"];
        if ($Display_SD_Options) {
            $DataDisplayOptions["GrandSDScore"] = $eReportCard["GrandStandardScore"];
        }
        if ($Display_ActualAverage_Options) {
            $DataDisplayOptions["ActualAverage"] = $eReportCard['Template']['ActualAverage'];
        }
        $DataDisplayOptions["GPA"] = $eReportCard["GPA"];
        $DataDisplayOptions["GrandGrade"] = $eReportCard['GrandGrade'];
        $DataDisplayOptions["OrderMeritClass"] = $eReportCard["ClassPosition"];
        $DataDisplayOptions["OrderMeritForm"] = $eReportCard["FormPosition"];
        if ($Display_Stream_Options) {
            $DataDisplayOptions["OrderMeritStream"] = $eReportCard["StreamPosition"];
        }

        $DataDisplayOptions["NoOfPassSubject"] = $eReportCard['NumOfPassSubject'];
        $DataDisplayOptions["NoOfFailSubject"] = $eReportCard['NumOfFailSubject'];

        // [2016-0406-1633-32096] Fail Subject Unit
        if ($eRCTemplateSetting['MasterReport']['DisplayFailSubjetUnitInReport']) {
            $DataDisplayOptions["FailSubjectUnit"] = $eReportCard['MasterReport']['SubjectUnit'];
        }

        $DataDisplayOptions["ClassTeacherComment"] = $eReportCard["ClassTeacherComment"];

        if ($eRCTemplateSetting['MasterReport']['DisplayOverallPersonalCharacteristicsInMasterReport']) {
            $DataDisplayOptions["PersonalCharacteristics"] = $eReportCard['PersonalCharacteristics'];
        }

        // No Wrap Option (will add an check for avoid word wrapping for th field)
        $NoWrapOptionField = array(
            "ClassTeacherComment"
        );
        $ExcludeComponentSubjectOptionField = array(
            "NoOfPassSubject",
            "NoOfFailSubject"
        );

        $OrderOption = array_merge($OrderOption, $DataDisplayOptions);

        $x .= $linterface->GET_NAVIGATION2($eReportCard['MasterReport']['GrandMarkDisplayOption']) . "&nbsp;&nbsp;";
        $x .= '<span id="spanShowOption' . $ctr . '" style="display:none"><a href="javascript:showOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['ShowOption'] . '</a></span>';
        $x .= '<span id="spanHideOption' . $ctr . '"><a href="javascript:hideOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['HideOption'] . '</a></span>';
        $x .= '<table id="table' . $ctr . '" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
        // data display
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['DataDisplay'] . '</td>' . "\n";
        $x .= '<td >';
        $x .= $linterface->Get_Checkbox('GM_All', 'GM_All', '', $isChecked = 1, $Class = '', $Lang['Btn']['SelectAll'], "jsSelectAllCheckbox(this.checked, 'GrandMarkDataChk'); refreshSortingTable()", $Disabled = '');
        $x .= '<br>' . "\n";
        $i = 0;
        $x .= '<ul>' . "\n";
        foreach ((array) $DataDisplayOptions as $val => $label) {
            if (in_array($val, array(
                "GrandTotal",
                "GrandAverage",
                "ActualAverage",
                "GPA"
            ))) {
                $StyleChk = '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                $StyleChk .= $linterface->Get_Checkbox('GM' . $val . 'Style', 'ShowStyle[]', $val, $isChecked = 0, 'ShowStyleChk', $eReportCard['MasterReport']['FormatWithStyle'], "", $Disabled = '');
                // $js_Disable_Style = "js_Disable_Style('GD{$val}Style', 'GD{$val}')";
            } else if (in_array($val, $NoWrapOptionField)) {
                $ID_Name = 'GM' . $val . 'NoWrap';
                $StyleChk = '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                $StyleChk .= $linterface->Get_Checkbox($ID_Name, $ID_Name, 1, $isChecked = 0, '', $eReportCard['MasterReport']['NoWrap'], "", $Disabled = '');
            } else if (in_array($val, $ExcludeComponentSubjectOptionField)) {
                $StyleChk = '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                $StyleChk .= $linterface->Get_Checkbox('GM' . $val . 'ExcludeSubjectComponent', 'ExcludeSubjectComponentFieldAry[]', 'GM' . $val, $isChecked = 0, 'ExcludeSubjectComponentChk', $eReportCard['MasterReport']['ExcludeComponentSubject'], "", $Disabled = '');
            } else {
                $StyleChk = $js_Disable_Style = '';
            }

            $x .= '<li>' . "\n";
            $x .= $linterface->Get_Checkbox('GM' . $val, 'GrandMarkDataAry[]', $val, $isChecked = 1, 'GrandMarkDataChk', $label, "jsCheckUnSelectAll(this.checked, 'GM_All'); refreshSortingTable(); $js_Disable_Style ", $Disabled = '');
            $x .= $StyleChk . "\n";
            $x .= '</li>' . "\n";
            $i ++;
        }
        $x .= '</ul>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table><br><br>';
        $ctr ++;
        // ## Grand Mark Display Options End

        // ## Subject statistic Display Options Start
        // prepare data display options
        $DataDisplayOptions = array();
        $DataDisplayOptions['Mean'] = $eReportCard['Average'];
        $DataDisplayOptions['SD'] = $eReportCard['SD'];
        $DataDisplayOptions['HighestMark'] = $eReportCard['HighestMark'];
        $DataDisplayOptions['LowestMark'] = $eReportCard['LowestMark'];
        $DataDisplayOptions['SubjectWeight'] = $eReportCard['SubjectWeight'];
        $DataDisplayOptions['NumOfStudentStudyingInClass'] = $eReportCard['NumOfStudentStudyingInClass'];
        $DataDisplayOptions['NumOfStudentStudyingInForm'] = $eReportCard['NumOfStudentStudyingInForm'];
        $DataDisplayOptions['PassingNumber'] = $eReportCard['PassingNumber'];
        $DataDisplayOptions['PassingRate'] = $eReportCard['PassingRate'];
        $DataDisplayOptions['FailingNumber'] = $eReportCard['FailingNumber'];
        $DataDisplayOptions['FailingRate'] = $eReportCard['FailingRate'];
        $DataDisplayOptions['LowerQuartile'] = $eReportCard['LowerQuartile'];
        $DataDisplayOptions['Median'] = $eReportCard['Median'];
        $DataDisplayOptions['UpperQuartile'] = $eReportCard['UpperQuartile'];

        $CustDataDisplayOptions = array();
        if ($eRCTemplateSetting['MasterReport']['PassingNumberDetermineByPercentage']) {
            $CustDataDisplayOptions['CustPassingNumber'] = $eReportCard['PassingNumber'];
            $CustDataDisplayOptions['CustPassingRate'] = $eReportCard['PassingRate'];
            $CustDataDisplayOptions['CustFailingNumber'] = $eReportCard['FailingNumber'];
            $CustDataDisplayOptions['CustFailingRate'] = $eReportCard['FailingRate'];
        }

        $x .= $linterface->GET_NAVIGATION2($eReportCard['MasterReport']['StatisticDisplayOption']) . "&nbsp;&nbsp;";
        $x .= '<span id="spanShowOption' . $ctr . '" style="display:none"><a href="javascript:showOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['ShowOption'] . '</a></span>';
        $x .= '<span id="spanHideOption' . $ctr . '"><a href="javascript:hideOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['HideOption'] . '</a></span>';
        $x .= '<table id="table' . $ctr . '" width="100%" border="0" cellspacing="0" cellpadding="5" id="" class="form_table_v30">' . "\n";
        // data display
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['DataDisplay'] . '</td>' . "\n";
        $x .= '<td >';
        $x .= $linterface->Get_Checkbox('Stat_All', 'Stat_All', '', $isChecked = 1, $Class = '', $Lang['Btn']['SelectAll'], "jsSelectAllCheckbox(this.checked, 'StatisticDataChk'); js_Enable_Disable_Passing_Percentage();", $Disabled = '');
        $x .= '<ul class="sortable">' . "\n";
        $i = 0;
        foreach ((array) $DataDisplayOptions as $val => $label) {
            $x .= '<li>';
            $x .= $linterface->Get_Checkbox('Stat' . $val, 'StatisticData[]', $val, $isChecked = 1, 'StatisticDataChk', $label, "jsCheckUnSelectAll(this.checked, 'Stat_All')", $Disabled = '');
            $x .= '</li>' . "\n";
            $i ++;
        }

        foreach ((array) $CustDataDisplayOptions as $val => $label) {
            $x .= '<li>';
            $x .= $linterface->Get_Checkbox('Stat' . $val, 'StatisticData[]', $val, $isChecked = 1, 'StatisticDataChk Cust_PassingNumberDetermineByPercentage_Option', $label, "jsCheckUnSelectAll(this.checked, 'Stat_All'); js_Enable_Disable_Passing_Percentage();", $Disabled = '');

            $x .= ' (' . $eReportCard['MasterReport']['WithPassingMarkEqualsTo'] . $eReportCard['MasterReport']['PercentageOfCh'];

            // [2014-1014-1144-21164] Master Report - input passing mark one time
            $x .= '<input type="text" id="Stat' . $val . '_val" name="Stat' . $val . '_val" class="textboxnum fullMarkPercentageTb" onchange="changedFullMarkPercentageTb(this.value);">%';
            $x .= $eReportCard['MasterReport']['PercentageOfEn'] . ')';
            $x .= '</li>' . "\n";
            $i ++;
        }
        $x .= '</ul>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table><br><br>';
        $ctr ++;
        // ## Subject statistic Display Options End

        // ## Sorting Options Start (show selection option of ScaleInput == M only, as Grade input dont have ranking)
        // $ClassSubjectSelection = $this->Get_Subject_Selection($ClassLevelID, '', 'SortSubjectIDClass', '', $isMultiple=0, $IncludeComponent=1, $InputMarkOnly=1,$IncludeGrandMarks=1," class='SortByClassSubjectSelection'");
        // $FormSubjectSelection = $this->Get_Subject_Selection($ClassLevelID, '', 'SortSubjectIDForm', '', $isMultiple=0, $IncludeComponent=1, $InputMarkOnly=1,$IncludeGrandMarks=1," class='SortByFormSubjectSelection'");
        $ClassSubjectSelection = $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax-->';
        $FormSubjectSelection = $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax-->';
        $radioOnClick = "onclick='ChangeSortBy();'";

        $x .= $linterface->GET_NAVIGATION2($eReportCard['MasterReport']['SortingOption']) . "&nbsp;&nbsp;";
        $x .= '<span id="spanShowOption' . $ctr . '" style="display:none"><a href="javascript:showOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['ShowOption'] . '</a></span>';
        $x .= '<span id="spanHideOption' . $ctr . '"><a href="javascript:hideOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['HideOption'] . '</a></span>';
        $x .= '<table id="table' . $ctr . '" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['SortBy'] . '</td>' . "\n";
        $x .= '<td >';
        // Sort By Class & Class Number
        $x .= '<input type="radio" id="SortByClassNumber" name="SortBy" value="0" CHECKED ' . $radioOnClick . '>';
        $x .= '<label for="SortByClassNumber">' . $eReportCard['ClassAndClassNo'] . '</label><br>' . "\n";
        // Sort By Class Position
        $x .= '<input type="radio" id="SortByClassPosition" name="SortBy" value="1" ' . $radioOnClick . '>';
        $x .= '<span id="SortByClassSelectionSpan">' . $ClassSubjectSelection . '</span><label for="SortByClassPosition">' . $eReportCard['ClassPosition'] . '</label><br>' . "\n";
        // Sort By Form Position
        $x .= '<input type="radio" id="SortByFormPosition" name="SortBy" value="2" ' . $radioOnClick . '>';
        $x .= '<span id="SortByFormSelectionSpan">' . $FormSubjectSelection . '</span><label for="SortByFormPosition">' . $eReportCard['FormPosition'] . '</label><br>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table><br><br>';
        $ctr ++;
        // ## Sorting Options End

        // ## Other Info Options Start
        // prepare data display options
        global $lreportcard;
        $OtherInfoArr = $lreportcard->OtherInfoInGrandMS;
        $DataDisplayOptions = array();
        foreach ((array) $OtherInfoArr as $OtherInfo)
            $DataDisplayOptions[$OtherInfo] = $OtherInfo;

            $OrderOption = array_merge($OrderOption, $DataDisplayOptions);

            // [2016-0406-1633-32096] Build lang array for Other Info Title display
            // loop Other Info Type
            $OtherInfoLang = array();
            $OtherInfoTypeArr = $lreportcard->configFilesType;
            foreach ((array) $OtherInfoTypeArr as $OtherInfoType) {
                // loop Other Info Title
                $OtherInfoConfig = $this->getOtherInfoConfig($OtherInfoType);
                $NumOfConfig = sizeof($OtherInfoConfig);
                for ($numCon = 5; $numCon < $NumOfConfig; $numCon ++) {
                    // Other Info Details
                    $CurrentOtherInfo = $OtherInfoConfig[$numCon];
                    $CurrentOtherInfo["ChineseTitle"] = str_replace(array(
                        "(",
                        ")"
                    ), "", $CurrentOtherInfo["ChineseTitle"]);

                    $ConTitle = Get_Lang_Selection("ChineseTitle", "EnglishTitle");

                    $ConKey = $CurrentOtherInfo["EnglishTitle"];
                    $ConName = $CurrentOtherInfo[$ConTitle];

                    $OtherInfoLang[$ConKey] = $ConName;
                }
            }

            // Data Source Selection
            $DataSourceArr["LastestTerm"] = $eReportCard['MasterReport']['LastestTerm'];
            $DataSourceArr["WholeYear"] = $eReportCard['MasterReport']['WholeYear'];

            $x .= $linterface->GET_NAVIGATION2($eReportCard['MasterReport']['OtherInfoDisplayOption']) . "&nbsp;&nbsp;";
            $x .= '<span id="spanShowOption' . $ctr . '" style="display:none"><a href="javascript:showOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['ShowOption'] . '</a></span>';
            $x .= '<span id="spanHideOption' . $ctr . '"><a href="javascript:hideOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['HideOption'] . '</a></span>';
            $x .= '<table id="table' . $ctr . '" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
            // data display
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['DataDisplay'] . '</td>' . "\n";
            $x .= '<td >';
            $x .= '<ul>' . "\n";
            $i = 0;
            foreach ((array) $DataDisplayOptions as $val => $label) {
                // [2016-0406-1633-32096] Get Info Title from lang array
                $label = $OtherInfoLang[$label] ? $OtherInfoLang[$label] : $label;

                $x .= '<li>' . "\n";
                $x .= $linterface->Get_Checkbox('Other' . $val, 'OtherInfoData[]', $val, $isChecked = 1, 'OtherInfoDataChk', $label, "refreshSortingTable();", $Disabled = '') . "<br>";
                $x .= '</li>' . "\n";
                $i ++;
            }
            $x .= '</ul>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            // data source
            $x .= '<tr id="DataSourceRow">' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['DataSource'] . '</td>' . "\n";
            $x .= '<td >';
            $checked = "CHECKED";
            foreach ((array) $DataSourceArr as $val => $label) {
                $x .= '<input type="radio" id="' . $val . '" name="DataSource" value="' . $val . '" ' . $checked . '>&nbsp;';
                $x .= '<label for="' . $val . '">' . $label . '</label>' . "\n";
                $checked = '';
            }
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table><br><br>';

            $ctr ++;
            // ## Other Info Options End

            // ## Display Style
            $x .= $linterface->GET_NAVIGATION2($eReportCard['MasterReport']['DisplayStyle']) . "&nbsp;&nbsp;";
            $x .= '<span id="spanShowOption' . $ctr . '" style="display:none"><a href="javascript:showOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['ShowOption'] . '</a></span>';
            $x .= '<span id="spanHideOption' . $ctr . '"><a href="javascript:hideOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['MasterReport']['HideOption'] . '</a></span><br><br>';
            $x .= '<table id="table' . $ctr . '" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
            // CSV / Html
            $x .= '<tr>' . "\n";
            $x .= '<td  class="field_title" width="30%">' . $eReportCard['ViewFormat'] . '</td>' . "\n";
            $x .= '<td class="tabletext">';
            $x .= '<input type="radio" id="ViewFormatHTML" name="ViewFormat" value=0 checked onclick="refreshFormatOptions(0);"><label for="ViewFormatHTML">' . $eReportCard['HTML'] . '</label>&nbsp;';
            $x .= '<input type="radio" id="ViewFormatCSV" name="ViewFormat" value=1  onclick="refreshFormatOptions(1);"><label for="ViewFormatCSV">' . $eReportCard['CSV'] . '</label>';
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            // Report Title
            $x .= '<tr>' . "\n";
            $x .= '<td  class="field_title" width="30%">' . $eReportCard['MasterReport']['ReportTitle'] . '</td>' . "\n";
            $x .= '<td><input class="textboxtext" id="ReportTitle" name="ReportTitle"></td>' . "\n";
            $x .= '</tr>' . "\n";
            // Show Class Teacher
            $x .= '<tr id="ShowClassTeacherTR">' . "\n";
            $x .= '<td  class="field_title" width="30%">' . $eReportCard['MasterReport']['ShowClassTeacher'] . '</td>' . "\n";
            $x .= '<td>' . $linterface->Get_Checkbox("ShowClassTeacher", "ShowClassTeacher", 1, 1) . '</td>' . "\n";
            $x .= '</tr>' . "\n";
            // Subject Font Size
            $x .= '<tr class="HTMLSetting">' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['FontSize'] . '</td>' . "\n";
            $x .= '<td><input id="SubjectFontSize" name="SubjectFontSize" size=2 value=12>px</td>' . "\n";
            $x .= '</tr>' . "\n";
            // Subject Component Font Size
            $x .= '<tr class="HTMLSetting">' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['SubjectComponentFontSize'] . '</td>' . "\n";
            $x .= '<td><input id="SubjectComponentFontSize" name="SubjectComponentFontSize" size=2 value=12 >px</td>' . "\n";
            $x .= '</tr>' . "\n";
            // Student Bottom Line
            $x .= '<tr class="HTMLSetting">' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['StudentBottomLine'] . '</td>' . "\n";
            $x .= '<td>' . $eReportCard['MasterReport']['Every'] . ' <input id="BottomLineNumber" name="BottomLineNumber" size=2 value=10 > ' . $eReportCard['MasterReport']['Row'] . '</td>' . "\n";
            $x .= '</tr>' . "\n";

            if ($eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplay']) {
                // Apply fail style to mark display
                $x .= '<tr class="HTMLSetting">' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['ApplyFailStyleToScore'] . '<br><span class="tabletextremark">' . $eReportCard['MasterReport']['ApplyFailStyleToScoreRemarks'] . '</span></td>' . "\n";
                $x .= '<td><input type="checkbox" id="ApplyStyleToFailOnly" name="ApplyStyleToFailOnly" value=1 CHECKED></td>' . "\n";
                $x .= '</tr>' . "\n";
            }

            // Empty Symbol
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['EmptySymbol'] . '</td>' . "\n";
            $x .= '<td><input id="EmptySymbol" name="EmptySymbol" size=5 value="--" ></td>' . "\n";
            $x .= '</tr>' . "\n";
            // Show Column Label Option
            $x .= '<tr id="ColumnLabelOptionRow">' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['DisplayColumnLabel'] . '</td>' . "\n";
            $x .= '<td><input type="checkbox" id="DisplayColumnLabel" name="DisplayColumnLabel" value=1 CHECKED></td>' . "\n";
            $x .= '</tr>' . "\n";
            // Show SubjectData Label Option
            $x .= '<tr id="SubjectDataLabelOptionRow">' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['DisplaySubjectDataLabel'] . '</td>' . "\n";
            $x .= '<td><input type="checkbox" id="DisplaySubjDataLabel" name="DisplaySubjDataLabel" value=1 CHECKED></td>' . "\n";
            $x .= '</tr>' . "\n";
            // Column Display Order
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title" width="30%">' . $eReportCard['MasterReport']['ColumnDisplayOrder'] . '</td>' . "\n";
            $x .= '<td class="tabletext">';
            $x .= '<input type="radio" id="ColumnOrderDefault" class="ColumnOrderType" name="ColumnOrderType" value=0 checked onclick="refreshSortingTable();"><label for="ColumnOrderDefault">' . $Lang['General']['Default'] . '</label>&nbsp;';
            $x .= '<input type="radio" id="ColumnOrderCustom" class="ColumnOrderType" name="ColumnOrderType" value=1  onclick="refreshSortingTable();"><label for="ColumnOrderCustom">' . $Lang['General']['Custom'] . '</label>';
            $x .= '<br><span id="ColumnOrderSpan"></span>';
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            // Subject Separator
            $x .= '<tr class="HTMLSetting">' . "\n";
            $x .= '<td  class="field_title" width="30%">' . $eReportCard['MasterReport']['DisplayStyle'] . '</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<table cellspacing=0 cellpadding=2 border=0 id="ReportStyleTable">' . "\n";
            $x .= '<tr valign="top">' . "\n";
            // style 1
            $x .= '<td>' . "\n";
            $x .= '<div style="padding-bottom:5px"><input type="radio" class="ReportStyle" name="ReportStyle" id="ReportStyle1" onclick="SelectStyle()" value="0" checked><label for="ReportStyle1">' . $eReportCard['MasterReport']['ReportStyle1'] . '<br></label></div>' . "\n";
            $x .= '<label for="ReportStyle1"><img id="ReportStyle1Pic" class="checkpic" src="' . $image_path . '/' . $LAYOUT_SKIN . '/ereportcard/reportcard_master_report_01.gif" onclick="$(\'input#ReportStyle1\').click()"></label>' . "\n";
            $x .= $eReportCard['MasterReport']['SubjectSeparator'] . ' <input class="SeparatorReportStyle1" name="SeparatorSize" size=2 value=20 >px<br>';
            $x .= $eReportCard['MasterReport']['SubjectComponentSeparator'] . ' <input class="SeparatorReportStyle1" name="ComponentSeparatorSize1" size=2 value=20 >px';
            $x .= '</td>' . "\n";
            // style 2
            $x .= '<td>' . "\n";
            $x .= '<div style="padding-bottom:5px"><input type="radio" class="ReportStyle" name="ReportStyle" id="ReportStyle2" onclick="SelectStyle()" value="1"><label for="ReportStyle2">' . $eReportCard['MasterReport']['ReportStyle2'] . '</label></div>' . "\n";
            $x .= '<label for="ReportStyle2"><img id="ReportStyle2Pic" class="uncheckpic" src="' . $image_path . '/' . $LAYOUT_SKIN . '/ereportcard/reportcard_master_report_02.gif" onclick="$(\'input#ReportStyle2\').click()"></label><br>' . "\n";
            $x .= $eReportCard['MasterReport']['SubjectComponentSeparator'] . ' <input class="SeparatorReportStyle2" name="ComponentSeparatorSize2" size=2 value=20 >px';
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
            // style 3
            $x .= '<td>' . "\n";
            $x .= '<br><div style="padding-bottom:5px"><input type="radio" class="ReportStyle" name="ReportStyle" id="ReportStyle3" onclick="SelectStyle()" value="2"><label for="ReportStyle3">' . $eReportCard['MasterReport']['ReportStyle3'] . '<br></label></div>' . "\n";
            $x .= '<label for="ReportStyle3"><img id="ReportStyle3Pic" class="uncheckpic" src="' . $image_path . '/' . $LAYOUT_SKIN . '/ereportcard/reportcard_master_report_03.gif" onclick="$(\'input#ReportStyle3\').click()"></label>' . "\n";
            $x .= '</td>' . "\n";
            // style 4
            $x .= '<td>' . "\n";
            $x .= '<br><div style="padding-bottom:5px"><input type="radio" class="ReportStyle" name="ReportStyle" id="ReportStyle4" onclick="SelectStyle()" value="3"><label for="ReportStyle4">' . $eReportCard['MasterReport']['ReportStyle4'] . '<br></label></div>' . "\n";
            $x .= '<label for="ReportStyle4"><img id="ReportStyle4Pic" class="uncheckpic" src="' . $image_path . '/' . $LAYOUT_SKIN . '/ereportcard/reportcard_master_report_04.gif" onclick="$(\'input#ReportStyle4\').click()"></label>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table><br><br>';
            // ## Display Style

            // #### Main Content End
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";

            $x .= '<tr>' . "\n";
            $x .= '<td colspan="2" class="dotline">' . "\n";
            $x .= '<img src="' . $image_path . '/' . $LAYOUT_SKIN . '"/10x10.gif" width="10" height="1">' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td colspan="2" align="center">' . "\n";
            $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Check_Form();", "submitBtn");
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table><br><br>' . "\n";
            $x .= '</form>' . "\n";

            return $x;
    }

    function Get_All_Master_Report($StudentDisplayArr, $StatisticDisplayAry, $MasterReportSetting, $isSubjectAllNA, $CustValue = '', $ShowSubjectTeacherComment = false)
    {
        for ($i = 0; $i < 4; $i ++) {
            $MasterReportSetting["ReportStyle"] = $i;
            $tmp = $this->Get_Master_Report($StudentDisplayArr, $StatisticDisplayAry, $MasterReportSetting, $isSubjectAllNA, $CustValue, $ShowSubjectTeacherComment);
            $x .= $tmp[0];
        }

        return array(
            $x
        );
    }

    function Get_Master_Report($StudentDisplayArr, $StatisticDisplayAry, $MasterReportSetting, $isSubjectAllNA, $CustValue = '', $DisplayFormat = '', $SelectFrom = '')
    {
        global $eReportCard, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $eRCTemplateSetting;

        // separator
        // $MasterReportSetting["ReportStyle"] = 0;
        switch ($MasterReportSetting["ReportStyle"]) {
            case 0:
                // Space between Main Subject, Space between Component, Space between SubjectData
                $SeparatorSize = $MasterReportSetting["SeparatorSize"] ? $MasterReportSetting["SeparatorSize"] : "20px";
                $ComponentSeparatorSize = $MasterReportSetting["ComponentSeparatorSize1"] ? $MasterReportSetting["ComponentSeparatorSize1"] : "20px";
                $SpaceBetweenSubject = "<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='$SeparatorSize' height='1'>";
                $SpaceBetweenComponent = "<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='$ComponentSeparatorSize' height='1'>";
                $HeaderBorder = "";
                break;
            case 1:
                // border between Main Subject, Space between Component, no border between SubjectData
                $BorderBetweenSubject = "border_right";
                $BorderBetweenComponent = "";
                $SeparatorSize = $MasterReportSetting["SeparatorSize"] ? $MasterReportSetting["SeparatorSize"] : "20px";
                $ComponentSeparatorSize = $MasterReportSetting["ComponentSeparatorSize2"] ? $MasterReportSetting["ComponentSeparatorSize2"] : "20px";
                $SpaceBetweenSubject = "";
                $SpaceBetweenComponent = "<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='$ComponentSeparatorSize' height='1'>";
                $HeaderBorder = "border_bottom";
                break;
            case 2:
                // border between Main Subject, border between Component, no border between SubjectData
                $BorderBetweenSubject = "border_right";
                $BorderBetweenComponent = "border_right";
                $BorderBetweenColumn = "border_right";
                $HeaderBorder = "border_bottom";
                break;
            case 3:
                // border between Main Subject, border between Component, border between SubjectData
                $SubjectDataBorder = true;
                $BorderBetweenSubject = "border_right";
                $BorderBetweenComponent = "border_right";
                $BorderBetweenColumn = "border_right";
                $HeaderBorder = "border_bottom";
                break;
        }

        // Get Form Name and Class Name
        $ClassLevelName = $this->returnClassLevel($MasterReportSetting["ClassLevelID"]);
        $ClassInfo = $this->GET_CLASSES_BY_FORM($MasterReportSetting["ClassLevelID"]);
        $ClassInfo = BuildMultiKeyAssoc($ClassInfo, "ClassID");

        // [2016-0406-1633-32096] Get left student list
        if ($eRCTemplateSetting['MasterReport']['ShowEmptyRowForLeftStudentInReport']) {
            include_once ($PATH_WRT_ROOT . 'includes/libuser.php');
            $lu = new libuser();
            $leftStudentIDList = $lu->returnLeftStudentIDList();
        }

        // Get Class Teacher
        if ($MasterReportSetting['ShowClassTeacher']) {
            include_once ('form_class_manage.php');
        }

        // Show /hide row 2
        $HideReportColumn = ! $MasterReportSetting["DisplayColumnLabel"];
        $HideSubjectMarksLabel = ! $MasterReportSetting["DisplaySubjDataLabel"];
        $nonSubjectDataRowSpan = 3 - $HideReportColumn - $HideSubjectMarksLabel;

        // prepare colspan
        $SubjectMarksDataCount = count($MasterReportSetting["SubjectData"]);
        $ReportColumnCount = count($MasterReportSetting["ReportColumnID"]);
        $SubjectColspan = ($SubjectMarksDataCount * $ReportColumnCount) + ($ReportColumnCount - 1); // no of columns*displaydata + no. of separator (no. of Column - 1)
        $ReportColumnColspan = $SubjectMarksDataCount;

        if (in_array("PersonalCharacteristics", (array) $MasterReportSetting['SubjectOtherData']) || in_array("PersonalCharacteristics", (array) $MasterReportSetting['GrandMarkDataAry'])) {
            $PCDataArr = $this->returnPersonalCharSettingData($MasterReportSetting["ClassLevelID"]);
            $PCDataArr = BuildMultiKeyAssoc($PCDataArr, "SubjectID", '', 0, 1);

            $PCTitle = Get_Lang_Selection("Title_CH", "Title_EN");
        }

        // [2016-0406-1633-32096] Build lang array for Other Info Title display
        // loop Other Info Type
        $OtherInfoLang = array();
        $OtherInfoTypeArr = $this->configFilesType;
        foreach ((array) $OtherInfoTypeArr as $OtherInfoType) {
            // loop Other Info Title
            $OtherInfoConfig = $this->getOtherInfoConfig($OtherInfoType);
            $NumOfConfig = sizeof($OtherInfoConfig);
            for ($numCon = 5; $numCon < $NumOfConfig; $numCon ++) {
                // Other Info Details
                $CurrentOtherInfo = $OtherInfoConfig[$numCon];
                $CurrentOtherInfo["ChineseTitle"] = str_replace(array(
                    "(",
                    ")"
                ), "", $CurrentOtherInfo["ChineseTitle"]);

                $ConTitle = Get_Lang_Selection("ChineseTitle", "EnglishTitle");

                $ConKey = $CurrentOtherInfo["EnglishTitle"];
                $ConName = $CurrentOtherInfo[$ConTitle];

                $OtherInfoLang[$ConKey] = $ConName;
            }
        }

        $valignTop = "top";
        $page_break = '';
        $ExportRowArr = array();
        foreach ((array) $StudentDisplayArr as $StudentGroupingID => $StudentList) {
            if ($MasterReportSetting["SelectFrom"] == "subjectgroup" && $MasterReportSetting["ShowCorrespondingSubjectOnly"] == 1 && $MasterReportSetting["DisplayFormat"] == "ClassSummary") {
                // $MasterReportSetting["SubjectGroupSubjectArr"] this variable was init in master_report.php
                $thisDisplaySubjectArr = $MasterReportSetting["SubjectGroupSubjectArr"][$StudentGroupingID];
            } else {
                $thisDisplaySubjectArr = array_values((array) $MasterReportSetting["SubjectIDArr"]);
            }

            // prepare SubjectIDArr to find next SubjectID in foreach loop $NextSubjIDArr[$thisSubjID] = $NextSubjID;
            for ($i = 0; $NextSubjIDArr[$thisDisplaySubjectArr[$i]] = $thisDisplaySubjectArr[$i + 1]; $i ++);

            // for counting Student row
            $row = 0;

            // table title
            if ($MasterReportSetting["DisplayFormat"] == "FormSummary")
                $DisplayClassFormName = $ClassLevelName;
                else if ($MasterReportSetting['SelectFrom'] == "class")
                    $DisplayClassFormName = $ClassInfo[$StudentGroupingID]["ClassName"];
                    else {
                        $SubjectGroupObj = new subject_term_class($StudentGroupingID);
                        $DisplayClassFormName = $SubjectGroupObj->{Get_Lang_Selection("ClassTitleB5", "ClassTitleEN")};
                    }

                    if ($MasterReportSetting['ShowClassTeacher']) {
                        $TeacherArr = array();
                        if ($DisplayFormat == "ClassSummary") {
                            if ($SelectFrom == "class") {
                                // group by Class
                                $ClassObj = new year_class($StudentGroupingID, 0, 1);
                                $TeacherArr = Get_Array_By_Key($ClassObj->ClassTeacherList, "TeacherName");
                            } else {
                                // $StudentGroupingIDArr = $SubjectGroupIDArr; // group by Subject Group
                            }
                        } else {
                            // Group By form
                            $ClassInfoAry = $this->GET_CLASSES_BY_FORM($StudentGroupingID);
                            $ClassIdAry = Get_Array_By_Key($ClassInfoAry, 'ClassID');
                            $numOfClass = count($ClassIdAry);
                            for ($i = 0; $i < $numOfClass; $i ++) {
                                $_classId = $ClassIdAry[$i];
                                $ClassObj = new year_class($_classId, 0, 1);
                                $TeacherArr = array_merge($TeacherArr, Get_Array_By_Key($ClassObj->ClassTeacherList, "TeacherName"));
                            }
                        }

                        $TeacherArr = array_values(array_unique($TeacherArr));
                        $TeacherList = implode(", ", $TeacherArr);
                        $ClassTeacherDisplay = $eReportCard['ClassTeacher'] . " : " . $TeacherList;
                    }

                    // [2016-1031-1053-34096] Customize Master Report Title
                    if (trim($eRCTemplateSetting['MasterReport']['DisplayCustReportTitle']) != "") {
                        // Year & Semester
                        $TargetReportInfo = $this->returnReportTemplateBasicInfo($MasterReportSetting["ReportID"]);
                        $TargetReportYearName = $this->GET_ACTIVE_YEAR("/");
                        $TargetReportSemester = $TargetReportInfo["Semester"];
                        $TargetReportSemester = $TargetReportSemester ? $this->Get_Semester_Seq_Number($TargetReportSemester) : 0;
                        $TargetReportSemester = $TargetReportSemester ? "第" . $TargetReportSemester . "段" : "全年";

                        // Form / Class
                        if ($MasterReportSetting["DisplayFormat"] == "FormSummary")
                            $targetReportClassName = $ClassLevelName . "級";
                            else if ($MasterReportSetting["SelectFrom"] == "class" && $MasterReportSetting["DisplayFormat"] == "ClassSummary")
                                $targetReportClassName = $ClassInfo[$StudentGroupingID]["ClassTitleB5"] . "班";
                                else
                                    $targetReportClassName = $SubjectGroupObj->ClassTitleB5;

                                    // Report Title
                                    $MasterReportDisplayTitle = trim($eRCTemplateSetting['MasterReport']['DisplayCustReportTitle']);
                                    $MasterReportDisplayTitle = str_replace("<!--SchoolYear-->", $TargetReportYearName, $MasterReportDisplayTitle);
                                    $MasterReportDisplayTitle = str_replace("<!--TargetName-->", $targetReportClassName, $MasterReportDisplayTitle);
                                    $MasterReportDisplayTitle = str_replace("<!--SemesterDisplay-->", $TargetReportSemester, $MasterReportDisplayTitle);

                                    $MasterReportSetting["ReportTitle"] = $MasterReportDisplayTitle;
                    }

                    // [2016-0406-1633-32096] Display Class Teacher Name in next line
                    if ($eRCTemplateSetting['MasterReport']['ClassTeacherDisplayNextLineInReport']) {
                        $html .= "	<div class='report_title' $page_break>
                        <table width='100%'>
                        <!--<tr><td>" . stripslashes($MasterReportSetting["ReportTitle"]) . " - " . $DisplayClassFormName . "</td></tr>-->
									<tr><td>" . stripslashes($MasterReportSetting["ReportTitle"]) . "</td></tr>
									<tr><td style='font-size:14px;'>" . $ClassTeacherDisplay . "</td></tr>
								</table>
							</div>";
                    } else {
                        $html .= "<div class='report_title' $page_break><table width='100%'><tr><td>" . stripslashes($MasterReportSetting["ReportTitle"]) . " - " . $DisplayClassFormName . "</td><td align='right'>" . $ClassTeacherDisplay . "</td></div>";
                    }
                    $html .= "<table id='MasterReport" . $StudentGroupingID . "' cellpadding=1 cellspacing=0 border=0 class='report_border MasterReport' style='border-collapse:collapse;width:1px;' >"; // add 1 px width to minimize the table width

                    $ExportRowArr[] = array(
                        stripslashes($MasterReportSetting["ReportTitle"]) . " - " . $DisplayClassFormName,
                        $ClassTeacherDisplay
                    );

                    // prepare array
                    $trow1Arr = array();
                    $trow2Arr = array();
                    $trow3Arr = array();

                    $csvRow1Arr = array();

                    // [2016-0406-1633-32096]
                    $totalDataSpan = 0;

                    // ################## Header > Student Info Start ###################
                    foreach ((array) $MasterReportSetting["StudentData"] as $StudentData) {
                        $DisplayStudentData = $eReportCard['MasterReport']['DataShortName'][$StudentData];
                        $trow1Arr[$StudentData] .= "<td rowspan=" . $nonSubjectDataRowSpan . " valign=bottom class='border_bottom'>$DisplayStudentData</td>";
                        // separator
                        $trow1Arr[$StudentData] .= "<td rowspan=" . $nonSubjectDataRowSpan . " class='$BorderBetweenSubject border_bottom'>$SubjectSpacer</td>\n";

                        // ExportData
                        $csvRow1Arr[$StudentData] = $DisplayStudentData;
                    }
                    // ################## Header > Student Info End ###################

                    // ################## Header > Subject Marks Start ###################

                    // get column title
                    $ColumnTitleArr = $this->returnReportColoumnTitle($MasterReportSetting["ReportID"]);
                    $ColumnTitleArr[0] = $eReportCard['Overall'];

                    // loop subject
                    foreach ((array) $thisDisplaySubjectArr as $SubjectID) {
                        // skip subject with all NA
                        if (! $MasterReportSetting["DisplayAllNASubject"] && $isSubjectAllNA[$StudentGroupingID][$SubjectID])
                            continue;

                            // check whether this subject is component subject
                            if (! isset($isComponent[$SubjectID]))
                                $isComponent[$SubjectID] = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);

                                // check whether the next subject is component subject , if true
                                $NextSubjID = $NextSubjIDArr[$SubjectID];
                                if (! empty($NextSubjID) && ! isset($isComponent[$NextSubjID]))
                                    $isComponent[$NextSubjID] = $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($NextSubjID);

                                    // Separator setup
                                    $subjectcss = $isComponent[$SubjectID] ? "subjectcomponent" : "subject";
                                    // $ColumnBorder = $isComponent[$SubjectID]||$isComponent[$NextSubjID]?$BorderBetweenComponent:$BorderBetweenSubject;
                                    $ColumnBorder = $BorderBetweenColumn;
                                    $SubjectBorder = $isComponent[$NextSubjID] ? $BorderBetweenComponent : $BorderBetweenSubject;
                                    // $ColumnSpacer = $isComponent[$SubjectID]||$isComponent[$NextSubjID]?$SpaceBetweenComponent:$SpaceBetweenSubject;
                                    $SubjectSpacer = $isComponent[$NextSubjID] ? $SpaceBetweenComponent : $SpaceBetweenSubject;

                                    $AdditionalColspan = 0;
                                    // prepare Personal Characteristic Data Additional ColSpan
                                    if (in_array('PersonalCharacteristics', (array) $MasterReportSetting['SubjectOtherData'])) {
                                        $AdditionalColspan += count($PCDataArr[$SubjectID]) * 2; // include separator
                                    }

                                    // prepare SubjectTeacherComment Additional Colspan
                                    if (in_array('SubjectTeacherComment', (array) $MasterReportSetting['SubjectOtherData'])) {
                                        $AdditionalColspan += 2; // include separator
                                    }

                                    // prepare Effort Additional Colspan
                                    if (in_array('Effort', (array) $MasterReportSetting['SubjectOtherData'])) {
                                        $AdditionalColspan += 2; // include separator
                                    }

                                    $SubjectName = $this->GET_SUBJECT_NAME_LANG($SubjectID, $Lang = '', $MasterReportSetting["SubjectDisplay"] == "ShortName", $MasterReportSetting["SubjectDisplay"] == "Abbr");

                                    $trow1Arr[$SubjectID] .= "<td colspan='" . ($SubjectColspan + $AdditionalColspan) . "' class='$borderleft $HeaderBorder $subjectcss'  valign=bottom>$SubjectName</td>";

                                    // [2016-0406-1633-32096]
                                    $totalDataSpan += ($SubjectColspan + $AdditionalColspan);

                                    // loop column
                                    $column_ctr = 1;

                                    foreach ((array) $MasterReportSetting["ReportColumnID"] as $ColumnID) {
                                        $ColumnTitle = $ColumnTitleArr[$ColumnID];

                                        $ColumnAdditionalColSpan = 0;
                                        $trow2Arr[$SubjectID] .= "<td colspan='" . ($ReportColumnColspan + $ColumnAdditionalColSpan) . "' class='$borderleft $HeaderBorder $subjectcss' valign=bottom>$ColumnTitle</td>";

                                        // loop subject marks data
                                        foreach ((array) $MasterReportSetting["SubjectData"] as $SubjectData) { // aaaa

                                            $DisplaySubjectData = $eReportCard['MasterReport']['DataShortName'][$SubjectData];
                                            $borderleft = $SubjectDataBorder ? "border_left" : "";
                                            $trow3Arr[$SubjectID] .= "<td class='$borderleft $HeaderBorder $subjectcss '  valign=bottom>$DisplaySubjectData</td>";

                                            // if(!$SubjectDataBorder)
                                            // $borderleft="";

                                            // ExportData
                                            $CSVColumnTitle = ($HideReportColumn ? "" : '(' . $ColumnTitle . ')');
                                            $CSVSubjectData = ($HideSubjectMarksLabel ? "" : '[' . $DisplaySubjectData . ']');
                                            $csvRow1Arr[$SubjectID][] = $SubjectName . $CSVColumnTitle . $CSVSubjectData;
                                        }

                                        // separator (add separator to the end of Column except the last Column)
                                        if ($column_ctr < count($MasterReportSetting["ReportColumnID"])) {
                                            $trow2Arr[$SubjectID] .= "<td class='$ColumnBorder $HeaderBorder'>$ColumnSpacer</td>";
                                            $trow3Arr[$SubjectID] .= "<td class='$ColumnBorder $HeaderBorder'>$ColumnSpacer</td>";
                                        }
                                        $column_ctr ++;
                                    }

                                    $SizeOfSubjectOtherData = count($MasterReportSetting['SubjectOtherData']);
                                    if ($SizeOfSubjectOtherData) {
                                        $LastColumn = $SizeOfSubjectOtherData - 1;
                                        if (in_array("PersonalCharacteristics", $MasterReportSetting['SubjectOtherData'])) {
                                            $SizeOfPC = count($PCDataArr[$SubjectID]);

                                            if ($SizeOfPC == 0)
                                                $LastColumn -= 1;
                                        }

                                        if ($LastColumn >= 0) {
                                            // ################## Subject Other Data Start ######################
                                            $trow2Arr[$SubjectID] .= "<td class='$ColumnBorder $HeaderBorder'>$ColumnSpacer</td>";
                                            $trow3Arr[$SubjectID] .= "<td class='$ColumnBorder $HeaderBorder'>$ColumnSpacer</td>";
                                        }

                                        // foreach((array)$MasterReportSetting['SubjectOtherData'] as $DisplaySubjectOtherData)
                                        for ($i = 0; $i < $SizeOfSubjectOtherData; $i ++) {
                                            $DisplaySubjectOtherData = $MasterReportSetting['SubjectOtherData'][$i];
                                            switch ($DisplaySubjectOtherData) {
                                                case "SubjectTeacherComment":
                                                    if ($HideReportColumn && ! $HideSubjectMarksLabel) // special handling for hide 2nd row only (only Display Subject Marks Label was checked)
                                                    {
                                                        $trow3Arr[$SubjectID] .= "<td class='$borderleft $HeaderBorder $subjectcss'>" . $eReportCard['Template']['SubjectTeacherComment'] . "</td>";
                                                    } else {
                                                        $trow2Arr[$SubjectID] .= "<td class='$borderleft $HeaderBorder $subjectcss' rowspan='" . ($nonSubjectDataRowSpan - 1) . "'>" . $eReportCard['Template']['SubjectTeacherComment'] . "</td>";
                                                    }

                                                    // ExportData
                                                    $csvRow1Arr[$SubjectID][] = $SubjectName . " [" . $eReportCard['Template']['SubjectTeacherComment'] . "]";
                                                    break;
                                                case "PersonalCharacteristics":
                                                    $thisPCArr = array();

                                                    // foreach((array)$PCDataArr[$SubjectID] as $PCData)
                                                    $SizeOfPC = count($PCDataArr[$SubjectID]);
                                                    for ($j = 0; $j < $SizeOfPC; $j ++) {
                                                        $PCData = $PCDataArr[$SubjectID][$j];

                                                        if ($HideReportColumn && ! $HideSubjectMarksLabel) // special handling for hide 2nd row only (only Display Subject Marks Label was checked)
                                                        {
                                                            $trow3Arr[$SubjectID] .= "<td class='$borderleft $HeaderBorder $subjectcss'>" . $PCData[$PCTitle] . "</td>";
                                                        } else {
                                                            $trow2Arr[$SubjectID] .= "<td class='$borderleft $HeaderBorder $subjectcss' rowspan='" . ($nonSubjectDataRowSpan - 1) . "'>" . $PCData[$PCTitle] . "</td>";
                                                        }

                                                        if ($j < $SizeOfPC - 1) {
                                                            if ($HideReportColumn && ! $HideSubjectMarksLabel) // special handling for hide 2nd row only (only Display Subject Marks Label was checked)
                                                            {
                                                                $trow3Arr[$SubjectID] .= "<td class='$ColumnBorder $HeaderBorder'>" . $ColumnSpacer . "</td>";
                                                            } else {
                                                                $trow2Arr[$SubjectID] .= "<td class='$ColumnBorder $HeaderBorder' rowspan='" . ($nonSubjectDataRowSpan - 1) . "'>" . $ColumnSpacer . "</td>";
                                                            }
                                                        }

                                                        // ExportData
                                                        $csvRow1Arr[$SubjectID][] = $SubjectName . " [" . $PCData[$PCTitle] . "]";
                                                    }
                                                    break;
                                                case "Effort":
                                                    if ($HideReportColumn && ! $HideSubjectMarksLabel) // special handling for hide 2nd row only (only Display Subject Marks Label was checked)
                                                    {
                                                        $trow3Arr[$SubjectID] .= "<td class='$borderleft $HeaderBorder $subjectcss'>" . $eReportCard['ExtraInfoLabel'] . "</td>";
                                                    } else {
                                                        $trow2Arr[$SubjectID] .= "<td class='$borderleft $HeaderBorder $subjectcss' rowspan='" . ($nonSubjectDataRowSpan - 1) . "'>" . $eReportCard['ExtraInfoLabel'] . "</td>";
                                                    }

                                                    // ExportData
                                                    $csvRow1Arr[$SubjectID][] = $SubjectName . " [" . $eReportCard['ExtraInfoLabel'] . "]";
                                                    break;
                                            }

                                            if ($i < $LastColumn) {
                                                if ($HideReportColumn && ! $HideSubjectMarksLabel) // special handling for hide 2nd row only (only Display Subject Marks Label was checked)
                                                {
                                                    $trow3Arr[$SubjectID] .= "<td class='$ColumnBorder $HeaderBorder'>$ColumnSpacer</td>";
                                                } else {
                                                    $trow2Arr[$SubjectID] .= "<td class='$ColumnBorder $HeaderBorder' rowspan='" . ($nonSubjectDataRowSpan - 1) . "'>$ColumnSpacer</td>";
                                                }
                                            }
                                        }
                                        // ################## Subject Other Data End ######################

                                        // separator (add separator to the end of columns 2nd & 3rd rows)
                                        if (($HideReportColumn && ! $HideSubjectMarksLabel) || $LastColumn < 0) // special handling for hide 2nd row only (only Display Subject Marks Label was checked)
                                        {
                                            $trow2Arr[$SubjectID] .= "<td class='$SubjectBorder $HeaderBorder'>$SubjectSpacer</td>";
                                            $trow3Arr[$SubjectID] .= "<td class='$SubjectBorder $HeaderBorder'>$SubjectSpacer</td>";
                                        } else {
                                            $trow2Arr[$SubjectID] .= "<td class='$SubjectBorder $HeaderBorder' rowspan='" . ($nonSubjectDataRowSpan - 1) . "'>$SubjectSpacer</td>";
                                        }
                                    } else {
                                        $trow2Arr[$SubjectID] .= "<td class='$SubjectBorder $HeaderBorder'>$SubjectSpacer</td>";
                                        $trow3Arr[$SubjectID] .= "<td class='$SubjectBorder $HeaderBorder'>$SubjectSpacer</td>";
                                    }

                                    // separator (add separator to the end of subject title 1st row)
                                    $trow1Arr[$SubjectID] .= "<td class='$SubjectBorder $HeaderBorder'>$SubjectSpacer</td>";

                                    // [2016-0406-1633-32096]
                                    $totalDataSpan ++;
                    }

                    // ################## Header > Subject Marks End ###################

                    //

                    // ################## Header > Other Info Start ###################
                    // if($MasterReportSetting["DisplayColumnLabel"]||$MasterReportSetting["DisplaySubjDataLabel"])
                    // $borderleft = "border_left";
                        foreach ((array) $MasterReportSetting["OtherInfoData"] as $DisplayOtherData) {
                            // [2016-0406-1633-32096]
                            $DisplayName = $eReportCard['MasterReport']['OtherInfo'][$DisplayOtherData];
                            $DisplayName = $DisplayName ? $DisplayName : $OtherInfoLang[$DisplayOtherData];
                            $DisplayName = $DisplayName ? $DisplayName : $DisplayOtherData;

                            $trow1Arr[$DisplayOtherData] .= "<td rowspan=" . $nonSubjectDataRowSpan . " valign=bottom class='$borderleft border_bottom'>$DisplayName</td>";
                            // separator
                            $trow1Arr[$DisplayOtherData] .= "<td rowspan=" . $nonSubjectDataRowSpan . " class='$BorderBetweenSubject border_bottom'>$SubjectSpacer</td>\n";
                            $borderleft = "";

                            // [2016-0406-1633-32096]
                            $totalDataSpan += 2;

                            // ExportData
                            $csvRow1Arr[$DisplayOtherData] = $DisplayName;
                        }
                    // ################## Header > Other Info End ###################

                    // ################## Header > Grand Marks Start ###################
                    foreach ((array) $MasterReportSetting["GrandMarkDataAry"] as $GrandMarkData) {
                        $DisplayGrandMarkData = $eReportCard['MasterReport']['DataShortName'][$GrandMarkData];

                        if ($GrandMarkData == "PersonalCharacteristics") {
                            $thisPCArr = array();
                            $SizeOfPC = count($PCDataArr[0]);
                            for ($j = 0; $j < $SizeOfPC; $j ++) {
                                $PCData = $PCDataArr[0][$j];

                                $trow1Arr[$GrandMarkData] .= "<td rowspan=" . $nonSubjectDataRowSpan . " valign=bottom class='border_bottom '>" . $PCData[$PCTitle] . "</td>";

                                // separator
                                $trow1Arr[$GrandMarkData] .= "<td rowspan=" . $nonSubjectDataRowSpan . " class='$BorderBetweenSubject border_bottom'>$SubjectSpacer</td>\n";

                                // ExportData
                                $csvRow1Arr[$GrandMarkData][] = $PCData[$PCTitle];

                                // [2016-0406-1633-32096]
                                $totalDataSpan += 2;
                            }
                        } else {
                            $trow1Arr[$GrandMarkData] .= "<td rowspan=" . $nonSubjectDataRowSpan . " valign=bottom class='border_bottom '>$DisplayGrandMarkData</td>";
                            // separator
                            $trow1Arr[$GrandMarkData] .= "<td rowspan=" . $nonSubjectDataRowSpan . " class='$BorderBetweenSubject border_bottom'>$SubjectSpacer</td>\n";

                            // ExportData
                            $csvRow1Arr[$GrandMarkData] = $DisplayGrandMarkData;

                            // [2016-0406-1633-32096]
                            $totalDataSpan += 2;
                        }
                    }
                    // ################## Header > Grand Marks End ###################

                    $trow1 = "<tr>"; // Subject
                    $trow2 = "<tr>"; // Column
                    $trow3 = "<tr>"; // Display Data

                    $ExportColArr = array();
                    foreach ($MasterReportSetting["ColumnOrder"] as $idx) {
                        $trow1 .= $trow1Arr[$idx];
                        $trow2 .= $trow2Arr[$idx];
                        $trow3 .= $trow3Arr[$idx];

                        // ExportData
                        foreach ((array) $csvRow1Arr[$idx] as $csvCol)
                            $ExportColArr[] = $csvCol;
                    }
                    $trow1 .= "</tr>\n";
                    $trow2 .= "</tr>\n";
                    $trow3 .= "</tr>\n";

                    // ExportData
                    $ExportRowArr[] = $ExportColArr;

                    $html .= $trow1 . ($HideReportColumn ? "" : $trow2) . ($HideSubjectMarksLabel ? "" : $trow3);

                    // table body
                    $border_top = "border_top";
                    foreach ((array) $StudentList as $StudentID => $DataArr) {
                        $row ++;
                        $htmlArr = array();

                        if ($row == count($StudentList))
                            $border_bottom = "border_bottom";
                            else if ($MasterReportSetting["BottomLineNumber"] == 0)
                                $border_bottom = "";
                                else
                                    $border_bottom = $row % $MasterReportSetting["BottomLineNumber"] == 0 ? "border_bottom" : "";

                                    $html .= "<tr>";
                                    $ExportColArr = array();

                                    // ################## Body > Student Info Start ###################
                                    foreach ((array) $DataArr["StudentInfoData"] as $DisplayFieldName => $StudentInfo) {
                                        $nowrap = $MasterReportSetting["Std" . $DisplayFieldName . "NoWrap"] ? "nowrap" : "";

                                        $htmlArr[$DisplayFieldName] .= "<td $nowrap valign='$valignTop' class='$border_bottom'>$StudentInfo</td>\n";
                                        // Separator
                                        $htmlArr[$DisplayFieldName] .= "<td class='$BorderBetweenSubject $border_bottom'>$SubjectSpacer</td>\n";

                                        // ExportData
                                        $ExportColArr[$DisplayFieldName] = ($StudentInfo == '') ? ' ' : $StudentInfo;
                                    }
                                    // ################## Body > Student Info End ###################

                                    // [2016-0406-1633-32096] Show empty row for left students
                                    if ($eRCTemplateSetting['MasterReport']['ShowEmptyRowForLeftStudentInReport'] && ! empty($leftStudentIDList) && in_array($StudentID, (array) $leftStudentIDList)) {
                                        $htmlArr[$DisplayFieldName] .= "<td colspan='$totalDataSpan' class='border_top border_bottom' align='center'>" . $eReportCard['MasterReport']['LeftStudent'] . "</td>";
                                    }                // Normal case
                                    else {

                                        // ################## Body > Subject Marks Start ###################
                                        $SubjectMarksDisplay = "";
                                        if ($MasterReportSetting["DisplayColumnLabel"] || $MasterReportSetting["DisplaySubjDataLabel"])
                                            $borderleft = "border_left";

                                            foreach ((array) $thisDisplaySubjectArr as $SubjectID) {
                                                $ColumnArr = $DataArr["SubjectMarksData"][$SubjectID];
                                                // skip subject with all NA
                                                if (! $MasterReportSetting["DisplayAllNASubject"] && $isSubjectAllNA[$StudentGroupingID][$SubjectID])
                                                    continue;

                                                    $subjectcss = $isComponent[$SubjectID] ? "subjectcomponent" : "subject";
                                                    $NextSubjID = $NextSubjIDArr[$SubjectID];
                                                    // $ColumnBorder = $isComponent[$SubjectID]||$isComponent[$NextSubjID]?$BorderBetweenComponent:$BorderBetweenSubject;
                                                    $ColumnBorder = $BorderBetweenColumn;
                                                    $SubjectBorder = $isComponent[$NextSubjID] ? $BorderBetweenComponent : $BorderBetweenSubject;
                                                    // $ColumnSpacer = $isComponent[$SubjectID]||$isComponent[$NextSubjID]?$SpaceBetweenComponent:$SpaceBetweenSubject;
                                                    $SubjectSpacer = $isComponent[$NextSubjID] ? $SpaceBetweenComponent : $SpaceBetweenSubject;
                                                    // bbbb

                                                    $column_ctr = 1;
                                                    foreach ((array) $ColumnArr as $ReportColumnID => $SubjectMarksArr) {
                                                        $ColumnTitle = $ColumnTitleArr[$ReportColumnID];

                                                        foreach ((array) $SubjectMarksArr as $SubjectMarks) {
                                                            $borderleft = $SubjectDataBorder ? "border_left" : "";
                                                            $htmlArr[$SubjectID] .= "<td class='$borderleft $border_bottom $border_top $subjectcss' valign='$valignTop' nowrap>$SubjectMarks</td>\n";

                                                            // ExportData
                                                            $ExportColArr[$SubjectID][] = $SubjectMarks;
                                                        }

                                                        // separator (add separator to the end of Column except the last Column)
                                                        if ($column_ctr < count($ColumnArr)) {
                                                            $htmlArr[$SubjectID] .= "<td class='$ColumnBorder $border_bottom $border_top'>$ColumnSpacer</td>\n";
                                                        }
                                                        $column_ctr ++;
                                                    }
                                                    // # separator (add separator to the end of Subject )
                                                    // $htmlArr[$SubjectID] .= "<td class='$SubjectBorder $border_bottom $border_top'>$SubjectSpacer</td>\n";

                                                    // ################## Subject Other Data Start ######################
                                                    $SizeOfSubjectOtherData = count($MasterReportSetting['SubjectOtherData']);
                                                    if ($SizeOfSubjectOtherData) {
                                                        $SubjectOtherData = $DataArr["SubjectOtherData"][$SubjectID];
                                                        $LastColumn = $SizeOfSubjectOtherData - 1;
                                                        if (in_array("PersonalCharacteristics", $MasterReportSetting['SubjectOtherData'])) {
                                                            $SizeOfPC = count($SubjectOtherData["PersonalCharacteristics"]);

                                                            if ($SizeOfPC == 0)
                                                                $LastColumn -= 1;
                                                        }

                                                        if ($LastColumn >= 0) { // separator (add separator back to the Last Column)
                                                            $htmlArr[$SubjectID] .= "<td class='$ColumnBorder $border_bottom $border_top'>$ColumnSpacer</td>\n";
                                                        }

                                                        // foreach((array)$MasterReportSetting['SubjectOtherData'] as $DisplaySubjectOtherData)
                                                        for ($i = 0; $i < $SizeOfSubjectOtherData; $i ++) {
                                                            $DisplaySubjectOtherData = $MasterReportSetting['SubjectOtherData'][$i];

                                                            $nowrap = $MasterReportSetting["SOD" . $DisplaySubjectOtherData . "NoWrap"] ? "nowrap" : "";

                                                            switch ($DisplaySubjectOtherData) {
                                                                case "SubjectTeacherComment":
                                                                    $Comment = Get_String_Display($SubjectOtherData[$DisplaySubjectOtherData], NULL, $MasterReportSetting['EmptySymbol']);
                                                                    // [2017-0210-1150-46236] remove unwanted slashes
                                                                    $htmlArr[$SubjectID] .= "<td $nowrap class='$borderleft $border_bottom $border_top $subjectcss' valign='$valignTop'>" . stripslashes($Comment) . "</td>\n";

                                                                    // ExportData
                                                                    // [2017-0210-1150-46236] remove unwanted slashes
                                                                    $ExportColArr[$SubjectID][] = stripslashes($SubjectOtherData[$DisplaySubjectOtherData]);
                                                                    break;
                                                                case "PersonalCharacteristics":
                                                                    $thisPCArr = array();
                                                                    //
                                                                    $SizeOfPC = count($SubjectOtherData[$DisplaySubjectOtherData]);
                                                                    for ($j = 0; $j < $SizeOfPC; $j ++) {
                                                                        $thisPCData = $SubjectOtherData[$DisplaySubjectOtherData][$j];
                                                                        $thisPCDisplay = Get_String_Display($thisPCData, NULL, $MasterReportSetting['EmptySymbol']);
                                                                        $htmlArr[$SubjectID] .= "<td class='$borderleft $border_bottom $border_top $subjectcss' valign='$valignTop'>" . $thisPCDisplay . "</td>\n";

                                                                        // separator
                                                                        if ($j < $SizeOfPC - 1) {
                                                                            $htmlArr[$SubjectID] .= "<td class='$ColumnBorder $border_bottom $border_top'>$ColumnSpacer</td>\n";
                                                                        }

                                                                        // ExportData
                                                                        $ExportColArr[$SubjectID][] = $thisPCData;
                                                                    }
                                                                    break;
                                                                case "Effort":
                                                                    $thisEffort = Get_String_Display($SubjectOtherData[$DisplaySubjectOtherData], NULL, $MasterReportSetting['EmptySymbol']);
                                                                    $htmlArr[$SubjectID] .= "<td $nowrap class='$borderleft $border_bottom $border_top $subjectcss' valign='$valignTop'>" . $thisEffort . "</td>\n";

                                                                    // ExportData
                                                                    $ExportColArr[$SubjectID][] = $SubjectOtherData[$DisplaySubjectOtherData];
                                                                    break;
                                                            }

                                                            // separator (add separator to the end of Column except the last Column) # Last Column
                                                            if ($i < $LastColumn) {
                                                                $htmlArr[$SubjectID] .= "<td class='$ColumnBorder $border_bottom $border_top'>$ColumnSpacer</td>\n";
                                                            }
                                                        }
                                                    }
                                                    // ################## Subject Other Data End ######################

                                                    // separator (add separator to the end of Subject )
                                                    $htmlArr[$SubjectID] .= "<td class='$SubjectBorder $border_bottom $border_top'>$SubjectSpacer</td>\n";
                                            }
                                            // ################## Body > Subject Marks End ###################

                                            // ################## Body > Other Info Start ###################
                                            $OtherInfoDisplay = "";
                                            foreach ((array) $DataArr["OtherInfoData"] as $OrderInfoField => $OtherInfo) {
                                                // [2016-0128-1008-07066] display multiple Other Info data
                                                $OtherInfo = isset($OtherInfo) && is_array($OtherInfo) ? implode("<br/>", $OtherInfo) : $OtherInfo;

                                                $borderleft = $SubjectDataBorder ? "border_left" : "";
                                                $htmlArr[$OrderInfoField] .= "<td valign='$valignTop' class='$borderleft $border_bottom'>$OtherInfo</td>\n";
                                                // Separator
                                                $htmlArr[$OrderInfoField] .= "<td class='$BorderBetweenSubject $border_bottom'>$SubjectSpacer</td>\n";

                                                // [2016-0128-1008-07066] display multiple Other Info data in CSV
                                                $OtherInfo = str_replace("<br/>", "\r\n", $OtherInfo);

                                                // ExportData
                                                $ExportColArr[$OrderInfoField] = trim($OtherInfo) != '' ? trim($OtherInfo) : '';

                                                $borderleft = '';
                                            }
                                            // ################## Body > Other Info End ###################

                                            // ################## Body > Grand Mark Start ###################
                                            $GrandMarkDisplay = "";
                                            foreach ((array) $DataArr["GrandMarkData"] as $GrandMarkField => $GrandMark) {
                                                $nowrap = $MasterReportSetting["GM" . $GrandMarkField . "NoWrap"] ? "nowrap" : "";

                                                if ($GrandMarkField == "PersonalCharacteristics") // print overall personal characteristic
                                                {
                                                    $thisPCArr = array();

                                                    $SizeOfPC = count($GrandMark);

                                                    for ($j = 0; $j < $SizeOfPC; $j ++) {
                                                        $thisPCData = $GrandMark[$j];
                                                        $thisPCDisplay = Get_String_Display($thisPCData, NULL, $MasterReportSetting['EmptySymbol']);

                                                        $htmlArr[$GrandMarkField] .= "<td $nowrap valign='$valignTop' class=' $border_bottom'>" . $thisPCDisplay . "</td>\n";

                                                        $htmlArr[$GrandMarkField] .= "<td class='$BorderBetweenSubject $border_bottom'>$SubjectSpacer</td>\n";

                                                        // ExportData
                                                        $ExportColArr[$GrandMarkField][] = $thisPCData;
                                                    }
                                                } else {
                                                    $htmlArr[$GrandMarkField] .= "<td $nowrap valign='$valignTop' class=' $border_bottom'>$GrandMark</td>\n";
                                                    // Separator
                                                    $htmlArr[$GrandMarkField] .= "<td class='$BorderBetweenSubject $border_bottom'>$SubjectSpacer</td>\n";

                                                    // ExportData
                                                    $ExportColArr[$GrandMarkField] = $GrandMark;
                                                }
                                            }
                                            // ################## Body > Grand Mark End ###################
                                    }

                                    $SortedExportColArr = array();

                                    foreach ($MasterReportSetting["ColumnOrder"] as $idx) {
                                        $html .= $htmlArr[$idx];

                                        if (is_array($ExportColArr[$idx])) {
                                            foreach ($ExportColArr[$idx] as $csvCol)
                                                $SortedExportColArr[] = $csvCol;
                                        } else if (isset($ExportColArr[$idx])) {
                                            if (trim($ExportColArr[$idx]) != '')
                                                $SortedExportColArr[] = $ExportColArr[$idx];
                                                else
                                                    $SortedExportColArr[] = "";
                                        }
                                    }

                                    $html .= "</tr>";
                                    $ExportRowArr[] = $SortedExportColArr;

                                    $border_top = '';
                    }

                    // ############### Statistic Start #################
                    foreach ((array) $StatisticDisplayAry[$StudentGroupingID] as $DisplayStatisticType => $StatisticDataArr) {
                        $htmlArr = array();
                        $ExportColArr = array();

                        $StatValueColspan = count($MasterReportSetting["SubjectData"]);
                        $StatisticTitle = $eReportCard['MasterReport']['DataShortName'][$DisplayStatisticType];

                        if ($eRCTemplateSetting['MasterReport']['PassingNumberDetermineByPercentage'] && empty($StatisticTitle)) {
                            $CustDisplayStatisticType = str_replace("Cust", "", $DisplayStatisticType);
                            $StatisticTitle = $eReportCard['MasterReport']['DataShortName'][$CustDisplayStatisticType];
                            if ($CustValue["Stat" . $DisplayStatisticType . "_val"])
                                $StatisticTitle .= ' ( ' . $eReportCard['MasterReport']['Marks'] . ' >= ' . $eReportCard['MasterReport']['PercentageOfCh'] . $CustValue["Stat" . $DisplayStatisticType . "_val"] . '% ' . $eReportCard['MasterReport']['PercentageOfEn'] . ')';
                        }

                        // Subject Marks Statistic
                        foreach ((array) $thisDisplaySubjectArr as $SubjectID) {
                            // skip subject with all NA
                            if (! $MasterReportSetting["DisplayAllNASubject"] && $isSubjectAllNA[$StudentGroupingID][$SubjectID])
                                continue;

                                $subjectcss = $isComponent[$SubjectID] ? "subjectcomponent" : "subject";
                                $NextSubjID = $NextSubjIDArr[$SubjectID];
                                // $ColumnBorder = $isComponent[$SubjectID]||$isComponent[$NextSubjID]?$BorderBetweenComponent:$BorderBetweenSubject;
                                $ColumnBorder = $BorderBetweenColumn;
                                $SubjectBorder = $isComponent[$NextSubjID] ? $BorderBetweenComponent : $BorderBetweenSubject;
                                // $ColumnSpacer = $isComponent[$SubjectID]||$isComponent[$NextSubjID]?$SpaceBetweenComponent:$SpaceBetweenSubject;
                                $SubjectSpacer = $isComponent[$NextSubjID] ? $SpaceBetweenComponent : $SpaceBetweenSubject;

                                $column_ctr = 1;
                                foreach ((array) $MasterReportSetting["ReportColumnID"] as $ReportColumnID) {
                                    $thisColumnTitle = $ColumnTitleArr[$ReportColumnID];
                                    $thisColStatistic = $StatisticDataArr["SubjectMark"][$SubjectID][$ReportColumnID];
                                    // eeee
                                    $htmlArr[$SubjectID] .= "<td class='$borderleft border_bottom $subjectcss' colspan='$StatValueColspan'>$thisColStatistic</td>\n";

                                    $borderleft = '';
                                    // separator (add separator to the end of Column except the last Column)
                                    if ($column_ctr < count($MasterReportSetting["ReportColumnID"])) {
                                        $htmlArr[$SubjectID] .= "<td class='$ColumnBorder border_bottom'>$ColumnSpacer</td>\n";
                                    }

                                    // ExportData
                                    for ($i = 1; $i < $StatValueColspan; $i ++)
                                        $ExportColArr[$SubjectID][] = '';
                                        $ExportColArr[$SubjectID][] = $thisColStatistic;
                                        $column_ctr ++;
                                }

                                $SizeOfSubjectOtherData = count($MasterReportSetting['SubjectOtherData']);
                                if ($SizeOfSubjectOtherData) {
                                    $SubjectOtherData = $DataArr["SubjectOtherData"][$SubjectID];
                                    $LastColumn = $SizeOfSubjectOtherData - 1;
                                    if (in_array("PersonalCharacteristics", $MasterReportSetting['SubjectOtherData'])) {
                                        $SizeOfPC = count($SubjectOtherData["PersonalCharacteristics"]);

                                        if ($SizeOfPC == 0)
                                            $LastColumn -= 1;
                                    }

                                    if ($LastColumn >= 0) { // separator (add separator back to the Last Column)
                                        $htmlArr[$SubjectID] .= "<td class='$ColumnBorder border_bottom'>$ColumnSpacer</td>\n";
                                    }

                                    // foreach((array)$MasterReportSetting['SubjectOtherData'] as $DisplaySubjectOtherData)
                                    for ($i = 0; $i < $SizeOfSubjectOtherData; $i ++) {
                                        $DisplaySubjectOtherData = $MasterReportSetting['SubjectOtherData'][$i];

                                        switch ($DisplaySubjectOtherData) {
                                            case "SubjectTeacherComment":
                                                $htmlArr[$SubjectID] .= "<td class='$borderleft border_bottom $subjectcss'>" . $MasterReportSetting['EmptySymbol'] . "</td>\n";

                                                // ExportData
                                                $ExportColArr[$SubjectID][] = $MasterReportSetting['EmptySymbol'];
                                                break;
                                            case "PersonalCharacteristics":
                                                $thisPCArr = array();
                                                //
                                                $SizeOfPC = count($SubjectOtherData[$DisplaySubjectOtherData]);
                                                for ($j = 0; $j < $SizeOfPC; $j ++) {
                                                    $htmlArr[$SubjectID] .= "<td class='$borderleft $border_bottom $border_top $subjectcss' valign='$valignTop'>" . $MasterReportSetting['EmptySymbol'] . "</td>\n";

                                                    // separator
                                                    if ($j < $SizeOfPC - 1) {
                                                        $htmlArr[$SubjectID] .= "<td class='$ColumnBorder border_bottom'>$ColumnSpacer</td>\n";
                                                    }

                                                    // ExportData
                                                    $ExportColArr[$SubjectID][] = $MasterReportSetting['EmptySymbol'];
                                                }
                                                break;
                                            case "Effort":
                                                $htmlArr[$SubjectID] .= "<td class='$borderleft border_bottom $subjectcss'>" . $MasterReportSetting['EmptySymbol'] . "</td>\n";

                                                // ExportData
                                                $ExportColArr[$SubjectID][] = $MasterReportSetting['EmptySymbol'];
                                                break;
                                        }

                                        // separator (add separator to the end of Column)
                                        if ($i < $LastColumn) {
                                            $htmlArr[$SubjectID] .= "<td class='$ColumnBorder border_bottom'>$ColumnSpacer</td>\n";
                                        }
                                    }
                                }

                                // separator (add separator)
                                $htmlArr[$SubjectID] .= "<td class='$SubjectBorder border_bottom'>$SubjectSpacer</td>\n";
                        }

                        // OtherInfo (print empty td)
                        $OtherInfoDisplay = "";
                        foreach ((array) $MasterReportSetting["OtherInfoData"] as $OtherInfo) {
                            $htmlArr[$OtherInfo] .= "<td class='$borderleft border_bottom'></td>\n";
                            // Separator
                            $htmlArr[$OtherInfo] .= "<td class='$BorderBetweenSubject border_bottom'>$SpaceBetweenSubject</td>\n";

                            // ExportData
                            $ExportColArr[$OtherInfo][] = '';

                            $borderleft = '';
                        }

                        // Grand Mark Statistic
                        $GrandMarkDisplay = "";
                        foreach ((array) $MasterReportSetting["GrandMarkDataAry"] as $GrandMarkDisplay) {
                            if ($GrandMarkDisplay == "PersonalCharacteristics") // print empty symbol for Personal characteristic
                            {
                                $thisPCArr = array();

                                $SizeOfPC = count($PCDataArr[0]);
                                for ($j = 0; $j < $SizeOfPC; $j ++) {

                                    $htmlArr[$GrandMarkDisplay] .= "<td class='$borderleft border_bottom'>" . $MasterReportSetting['EmptySymbol'] . "</td>\n";
                                    // Separator
                                    $htmlArr[$GrandMarkDisplay] .= "<td class='$BorderBetweenSubject border_bottom'>$SpaceBetweenSubject</td>\n";

                                    // ExportData
                                    $ExportColArr[$GrandMarkDisplay][] = $MasterReportSetting['EmptySymbol'];
                                }
                            } else {
                                $thisGrandMarkStat = $StatisticDataArr["GrandMark"][$GrandMarkDisplay];

                                $htmlArr[$GrandMarkDisplay] .= "<td class='$borderleft border_bottom'> $thisGrandMarkStat</td>\n";
                                // Separator
                                $htmlArr[$GrandMarkDisplay] .= "<td class='$BorderBetweenSubject border_bottom'>$SpaceBetweenSubject</td>\n";

                                // ExportData
                                $ExportColArr[$GrandMarkDisplay][] = $thisGrandMarkStat;
                            }
                        }

                        $SubjectMarksStart = false;
                        $ColumnHtml = '';
                        $StatTitleColspan = 0;
                        $CSVEmptyField = 0;
                        $SortedExportColArr = array();
                        foreach ($MasterReportSetting["ColumnOrder"] as $idx) {
                            // special handling for Student Data in Statistic rows
                            if (in_array($idx, (array) array_values((array) $MasterReportSetting["StudentData"]))) {
                                if (! $SubjectMarksStart) // do not need to add colspan for studentData among SubjectMarks
                                {
                                    $StatTitleColspan += 2;
                                    $CSVEmptyField += 1;
                                    continue;
                                } else // special handle for placing student info among subject marks, add empty td for them
                                {
                                    $ColumnHtml .= "<td class='$borderleft border_bottom'> </td>\n";
                                    // Separator
                                    $ColumnHtml .= "<td class='$BorderBetweenSubject border_bottom'></td>\n";

                                    $SortedExportColArr[] = "";
                                }
                            }

                            $SubjectMarksStart = true;
                            $ColumnHtml .= $htmlArr[$idx];

                            foreach ((array) $ExportColArr[$idx] as $csvCol) {
                                $SortedExportColArr[] = $csvCol;
                            }
                        }

                        $StatTitleColspan -= 1; // exclude the final separator of Student Data

                        $html .= "<tr>\n";
                        // Statistic Title
                        $html .= "<td colspan='$StatTitleColspan' class='border_bottom'>$StatisticTitle</td>\n";
                        // Separator
                        $html .= "<td class='$BorderBetweenSubject border_bottom'>$SpaceBetweenSubject</td>\n";
                        $html .= $ColumnHtml;
                        $html .= "</tr>";

                        // exportData
                        $StatTitleCSVArr = array();
                        $StatTitleCSVArr[] = $StatisticTitle;
                        for ($i = 0; $i < $CSVEmptyField - 1; $i ++)
                            $StatTitleCSVArr[] = "";
                            $ExportRowArr[] = array_merge($StatTitleCSVArr, $SortedExportColArr);
                    } // end looping form/class

                    // ############### Statistic End #################

                    $html .= "</table><br>";

                    // [2016-0406-1633-32096] Added footer to display Print Date and Signature
                    if ($eRCTemplateSetting['MasterReport']['DisplayReportFooterInReport']) {
                        $html .= "	<table width='95%' align='center' class='MasterReportFooter'>
                                        <tr height='60px'>
                                            <td width='25%' align='center'>&nbsp;</td>
                                            <td width='50%'>&nbsp;</td>
                                            <td align='right'>" . $eReportCard['MasterReport']['Signature'] . ":&nbsp;</td>
                                            <td width='20%' class='border_bottom'>&nbsp;</td>
                                        </tr>
                                        <tr height='55px'>
                                            <td>" . $eReportCard['MasterReport']['PrintDate'] . ":&nbsp;" . date('j/n/Y H:i') . "</td>
                                            <td>&nbsp;</td>
                                            <td align='right'>" . $eReportCard['MasterReport']['SignatureDate'] . ":&nbsp;</td>
                                            <td class='border_bottom'>&nbsp;</td>
                                        </tr>
                                    </table>";
                    }
                    // Added footer to display Print Date
                    else if ($eRCTemplateSetting['Report']['MasterReport']['DisplayPrintReportDateTime'])
                    {
                        $html .= "	<table width='100%' class='MasterReportFooter'>
                                        <tr>
                                            <td align='right'>" . $eReportCard['MasterReport']['PrintDate'] . ":&nbsp;" . date('j/n/Y H:i') . "</td>
                                        </tr>
                                    </table>";
                        $html .= "<br><br>";
                    }

                    // Added to export Print Date
                    if ($eRCTemplateSetting['Report']['MasterReport']['DisplayPrintReportDateTime'])
                    {
                        $ExportRowArr[] = array(
                            ""
                        );
                        $ExportRowArr[] = array(
                            $eReportCard['MasterReport']['PrintDate'] . ":&nbsp;" . date('j/n/Y H:i')
                        );
                    }
                    $ExportRowArr[] = array(
                        ""
                    );
                    $ExportRowArr[] = array(
                        ""
                    );

                    // page break before report title
                    $page_break = "style='page-break-before:always;' ";
        }

        // css
        $html .= $this->Get_Master_Report_CSS($MasterReportSetting);

        return array(
            $html,
            $ExportRowArr
        );
    }

    function Get_Master_Report_CSS($MasterReportSetting)
    {
        $FontSize = $MasterReportSetting["SubjectFontSize"] ? $MasterReportSetting["SubjectFontSize"] . "px" : "12px";
        $SubjectComponentFontSize = $MasterReportSetting["SubjectComponentFontSize"] ? $MasterReportSetting["SubjectComponentFontSize"] . "px" : "10px";

        $css .= "<style>\n";
        $css .= ".report_title  {font-size:16px; font-weight:bold;}\n";
        $css .= ".report_border { border:1px solid #000; }\n";
        $css .= ".border_right { border-right:1px solid #000; }\n";
        $css .= ".border_left { border-left:1px solid #000; }\n";
        $css .= ".border_bottom { border-bottom:1px solid #000; }\n";
        $css .= ".border_top { border-top:1px solid #000; }\n";
        $css .= ".MasterReport { font-size:$FontSize; }\n";
        $css .= ".subjectcomponent { font-size:$SubjectComponentFontSize; }\n";
        // [2016-0406-1633-32096]
        $css .= ".MasterReportFooter { font-size:14px; }\n";
        $css .= "</style>";

        return $css;
    }

    function Get_SubjectAcademicResultReport($Post_Arr, $StudentPersonalCharInfoArr, $lang = '', &$GetChartArr)
    {
        global $eReportCard, $linterface, $image_path, $LAYOUT_SKIN, $Lang, $eRCTemplateSetting, $sys_custom, $lreportcard;

        $ClassLevelID = $Post_Arr['ClassLevelID'];
        $ReportID = $Post_Arr['ReportID'];
        $ReportColumnID = $Post_Arr['ReportColumnID'];
        $SelectFrom = $Post_Arr['SelectFrom']; // subjectgroup or class
        $YearClassIDArr = $Post_Arr['YearClassIDArr'];
        $TermID = $Post_Arr['TermID'];
        $SubjectGroupIDArr = $Post_Arr['SubjectGroupIDArr'];
        // $StudentIDArr= $Post_Arr['StudentIDArr'];
        $SubjectIDArr = $Post_Arr['SubjectIDArr'];

        $thisLang = ($lang == 'en') ? 'en' : 'ch';

        // ## Get Subject Title
        $MainSubjectArr = $this->returnSubjectwOrder($ClassLevelID, $ParForSelection = 0, $TeacherID = '', $thisLang, $ParDisplayType = 'Desc', $ExcludeCmpSubject = 0, $ReportID);

        // ## Get Report Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);

        // ## Get Report Info
        $YearTermID = $ReportInfoArr['Semester'];
        $TermTitle = $this->returnSemesters($YearTermID, $thisLang);
        $ReportType = ($YearTermID == 'F') ? 'W' : 'T';

        // ## Get Report Column Info
        if ($ReportType == 'T')
        {
            $ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
            $numOfReportColumn = count($ReportColumnInfoArr);
            $ReportColumnInfoArr[$numOfReportColumn]['ReportColumnID'] = 0;
            $ReportColumnInfoArr[$numOfReportColumn]['ColumnTitle'] = $eReportCard['SubjectAcademicResult']['Overall'];

            $PersonalCharSourceReportID = $ReportID;
            $MarksArr[$ReportID] = $this->getMarks($ReportID);
        }
        else if ($ReportType == 'W')
        {
            // $TermReportInfoArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
            $TermReportInfoArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly = 1, "ALL");
            $TermReportIDArr = Get_Array_By_Key($TermReportInfoArr, 'ReportID');
            $numOfTermReport = count($TermReportInfoArr);

            $LastTermReportID = $TermReportIDArr[$numOfTermReport - 1];
            $ReportColumnInfoAssoArray = $this->returnReportColoumnTitle($ReportID);

            $ReportColumnInfoArr = array();
            $j = 0;
            foreach ((array) $ReportColumnInfoAssoArray as $_colID => $_colTitle) {
                $ReportColumnInfoArr[$j]['ReportColumnID'] = $_colID;
                $ReportColumnInfoArr[$j]['ColumnTitle'] = $_colTitle;
                $j ++;
            }

            $numOfReportColumn = count($ReportColumnInfoArr);
            $ReportColumnInfoArr[$numOfReportColumn]['ReportColumnID'] = 0;
            $ReportColumnInfoArr[$numOfReportColumn]['ColumnTitle'] = $eReportCard['SubjectAcademicResult']['Overall'];

            $PersonalCharSourceReportID = $LastTermReportID;
            $MarksArr[$PersonalCharSourceReportID] = $this->getMarks($PersonalCharSourceReportID);
            // $MarksArr[$ReportID] = $this->getMarks($ReportID, '', '', 0, 1, '', '', 0);
            $MarksArr[$ReportID] = $this->getMarks($ReportID);

            $ReportInfoArr_SemTwo = $this->returnReportTemplateBasicInfo($LastTermReportID);
            $YearTermID_SemTwo = $ReportInfoArr_SemTwo['Semester'];
            $TermTitle = $this->returnSemesters($YearTermID_SemTwo, $thisLang);
        }

        $numOfReportColumn = count($ReportColumnInfoArr);
        for ($i=0; $i < $numOfReportColumn; $i++)
        {
            $thisTitle = $ReportColumnInfoArr[$i]['ColumnTitle'];

            $thisPosCA = stripos($thisTitle, 'Continuous Assessment');
            $thisPosJT = stripos($thisTitle, 'Joint Test');
            $thisPosSBA = stripos($thisTitle, 'SBA');
            $thisPosEm = stripos($thisTitle, 'Exam');
            if (is_numeric($thisPosCA)) {
                $ReportColumnInfoArr[$i]['ColumnTitle'] = 'CA';
            }
            else if (is_numeric($thisPosJT)) {
                $ReportColumnInfoArr[$i]['ColumnTitle'] = 'JT';
            }
            else if (is_numeric($thisPosSBA)) {
                $ReportColumnInfoArr[$i]['ColumnTitle'] = 'SBA';
            }
            else if (is_numeric($thisPosEm)) {
                $ReportColumnInfoArr[$i]['ColumnTitle'] = 'Em';
            }
        }

        // ## Subject Grading Scheme Info
        $GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectIDArr, $withGrandResult = 0, $returnAsso = 1, $ReportID);

        // ## Subject Weight Info
        $AllSubjectWeightInfoArr = $this->returnReportTemplateSubjectWeightData($ReportID);
        $AllSubjectWeightInfoAssoArr = BuildMultiKeyAssoc($AllSubjectWeightInfoArr, array(
            "ReportColumnID",
            "SubjectID"
        ));

        // ## Get Subject Grading Scheme Info
        $GradingSchemeInfoArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult = 0, $returnAsso = 1, $ReportID);

        // ## Get Mark Stat (Simple Statistics)
        // $Mark_Statistics_Info_Arr[$ReportColumnID][$SubjectID][$YearClassID / $SubjectGroupID][Field] = data
        if ($SelectFrom == 'class') {
            $forSubjectGroup = 0;
        }
        else if ($SelectFrom == 'subjectgroup') {
            $forSubjectGroup = 1;
        }
        $Mark_Statistics_Info_Arr = $this->Get_Mark_Statistics_Info($ReportID, $SubjectIDArr, $Average_Rounding = 2, $SD_Rounding = 2, $Percentage_Rounding = 1, $SubjectMarkRounding = 0, $IncludeClassStat = 1, $ReportColumnID, $forSubjectGroup);

        // ## Get Personal Characteristics Info
        $PersonalCharInfoArr = $this->returnPersonalCharSettingData($ClassLevelID, $ParSubjectID = '', $GroupByCharID = 1);
        // debug_r($PersonalCharInfoArr);

        $InfoArray = array();
        $InfoArray['ReportInfoArr'] = $ReportInfoArr;
        $InfoArray['MainSubjectArr'] = $MainSubjectArr;
        $InfoArray['StudentPersonalCharInfoArr'] = $StudentPersonalCharInfoArr;
        $InfoArray['GradingSchemeInfoArr'] = $GradingSchemeInfoArr;
        $InfoArray['MarksArr'] = $MarksArr;
        $InfoArray['PersonalCharSourceReportID'] = $PersonalCharSourceReportID;
        $InfoArray['TermTitle'] = $TermTitle;
        $InfoArray['ReportColumnInfoArr'] = $ReportColumnInfoArr;
        $InfoArray['PersonalCharInfoArr'] = $PersonalCharInfoArr;
        $InfoArray['AllSubjectWeightInfoAssoArr'] = $AllSubjectWeightInfoAssoArr;
        $InfoArray['Mark_Statistics_Info_Arr'] = $Mark_Statistics_Info_Arr;

        $html = '';

        $myChartIDArr = array();
        $countChartID = 0;

        for ($i=0, $i_MAX=count($SubjectIDArr); $i < $i_MAX; $i++)
        {
            if ($SelectFrom == 'class') {
                $count_jMax = count($YearClassIDArr);
            }
            else if ($SelectFrom == 'subjectgroup') {
                $count_jMax = count($SubjectGroupIDArr);
            }

            for ($j=0, $j_MAX=$count_jMax; $j < $j_MAX; $j++) {
                $myChartIDArr[] = $countChartID;
                $countChartID ++;
            }
        }

        $myChartID = 0;
        $outChartArr = array();
        for ($i=0, $i_MAX=count($SubjectIDArr); $i < $i_MAX; $i++)
        {
            $thisSubjectID = $SubjectIDArr[$i];
            $thisSchemeID = $GradingSchemeInfoArr[$thisSubjectID]['SchemeID'];

            // ## Get Grade Stat (Simple Statistics)
            // $Grade_Statistics_Info_Arr[$SchemeGrade][$ReportColumnID][$YearClassID / $SubjectGroupID][Field] = data;

            if ($SelectFrom == 'class') {
                $count_jMax = count($YearClassIDArr);
                $forSubjectGroup = 0;
                // $Grade_Statistics_Info_Arr = $this->Get_Scheme_Grade_Statistics_Info($ReportID, $thisSubjectID, $forSubjectGroup=0, $ReportColumnID);
            }
            else if ($SelectFrom == 'subjectgroup') {
                $count_jMax = count($SubjectGroupIDArr);
                $forSubjectGroup = 1;
                // $Grade_Statistics_Info_Arr = $this->Get_Scheme_Grade_Statistics_Info($ReportID, $thisSubjectID, $forSubjectGroup=1, $ReportColumnID);
            }
            // $InfoArray['Grade_Statistics_Info_Arr'] = $Grade_Statistics_Info_Arr;
            $InfoArray['Grade_Statistics_Info_Arr'] = $this->Get_Scheme_Grade_Statistics_Info($ReportID, $thisSubjectID, $forSubjectGroup, '', $Rounding = 1, $returnWithReportColumnID = 1);

            for ($j=0, $j_MAX=$count_jMax; $j < $j_MAX; $j++)
            {
                if ($SelectFrom == 'class') {
                    $thisClassID = $YearClassIDArr[$j];
                    $InfoArray['ClassID'] = $thisClassID;
                    $StudentInfoArr = $this->GET_STUDENT_BY_CLASS($thisClassID, $ParStudentIDList = "", $isShowBothLangs = 0, $withClassNumber = 0, $isShowStyle = 0, $ReturnAsso = 0);
                }
                else if ($SelectFrom == 'subjectgroup') {
                    $thisSubjectGroupID = $SubjectGroupIDArr[$j];
                    $InfoArray['SubjectGroupID'] = $thisSubjectGroupID;
                    $StudentInfoArr = $this->GET_STUDENT_BY_SUBJECT_GROUP($thisSubjectGroupID, '', '', 0, 0, 0);
                }

                $StudentIDArr = array();
                foreach ((array) $StudentInfoArr as $key => $thisStdInfoArr) {
                    $StudentIDArr[] = $thisStdInfoArr['UserID'];
                }

                $Post_Arr['StudentIDArr'] = $StudentIDArr;

                //$PageBreak_html = 'page-break-after: always;';
                //if ($j == $j_MAX - 1) {
                //    $PageBreak_html = '';
                //}
                if($html != '') {
                    $PageBreak_html = 'page-break-before: always;';
                }

                // $html .='<table class="border_table" style="page-break-after: always;">';
                // $html .= '<div style="' . $PageBreak_html . 'width:1100px; height:770px;">';
                // $html .= '<div style="' . $PageBreak_html . 'width:1075px; height:750px;">';
                $html .= '<div style="' . $PageBreak_html . 'width:1155px; height:750px;">';
                $html .= '<table class="" style="width:100%;">';
                //$html .= '<col width="50%" />';
                //$html .= '<col width="50%" />';
                $html .= '<col width="55%" />';
                $html .= '<col width="45%" />';

                $html .= '<tr>';
                $html .= '<td colspan="2" style="text-align:center" >' . $eReportCard['SubjectAcademicResult']['En']['SchoolName'] . '</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td style="text-align:right" >&nbsp;</td>';
                $html .= '<td style="text-align:right" >' . $eReportCard['SubjectAcademicResult']['En']['Subject'] . ': ' . $MainSubjectArr[$thisSubjectID][0] . '</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td align="right" style="vertical-align:top;">';
                $html .= $this->Get_SubjectAcademicResultReport_left_UI($Post_Arr, $InfoArray, $thisSubjectID, $lang);
                $html .= '</td>';
                $html .= '<td align="left" style="vertical-align:top;">';
                $html .= $this->Get_SubjectAcademicResultReport_right_UI($Post_Arr, $InfoArray, $thisSubjectID, $lang, $thisSchemeID, $myChartIDArr[$myChartID], $outChartArr[$myChartID]);
                $html .= '</td>';
                $html .= '</tr>';
                $html .= '</table>';
                $html .= '</div>';

                $myChartID ++;
            }
        }
        $GetChartArr = $outChartArr;
        return $html;
    }

    function Get_SubjectAcademicResultReport_left_UI($Post_Arr, $InfoArray, $SubjectID, $lang = '')
    {
        global $eReportCard, $PATH_WRT_ROOT;

        $Table_Height = 700;

        $ClassLevelID = $Post_Arr['ClassLevelID'];
        $ReportID = $Post_Arr['ReportID'];
        $ReportColumnIDArr = $Post_Arr['ReportColumnID'];
        $SelectFrom = $Post_Arr['SelectFrom'];          // Subject group or Class
        $YearClassIDArr = $Post_Arr['YearClassIDArr'];
        $TermID = $Post_Arr['TermID'];
        $SubjectGroupIDArr = $Post_Arr['SubjectGroupIDArr'];
        $StudentIDArr = $Post_Arr['StudentIDArr'];

        $ReportInfoArr = $InfoArray['ReportInfoArr'];
        $MainSubjectArr = $InfoArray['MainSubjectArr'];
        $StudentPersonalCharInfoArr = $InfoArray['StudentPersonalCharInfoArr'];
        $GradingSchemeInfoArr = $InfoArray['GradingSchemeInfoArr'];
        $MarksArr = $InfoArray['MarksArr'];
        $PersonalCharSourceReportID = $InfoArray['PersonalCharSourceReportID'];
        $TermTitle = $InfoArray['TermTitle'];
        $ReportColumnInfoArr = $InfoArray['ReportColumnInfoArr'];
        $PersonalCharInfoArr = $InfoArray['PersonalCharInfoArr'];
        $AllSubjectWeightInfoAssoArr = $InfoArray['AllSubjectWeightInfoAssoArr'];

        $lang = ($lang == 'en') ? 'En' : 'Ch';

        // ## Get Report Info
        $YearTermID = $ReportInfoArr['Semester'];
        $ReportType = ($YearTermID == 'F') ? 'W' : 'T';

        // ## Count total number of subjects
        $Count_Subjects = count($MainSubjectArr[$SubjectID]);

        // ## Count total number of component subjects
        $Count_SubSubjects = count($MainSubjectArr[$SubjectID]) - 1;

        // ## Count total number of students
        $count_Student = count($StudentIDArr);

        // ## Count total number of Report Column
        $numOfReportColumn = count($ReportColumnInfoArr);

        // ## Get Personal Characteristics Info
        $numOfPersonalChar = count($PersonalCharInfoArr);

        // ## Get total of Student
        $numOfStudent = count($StudentIDArr);

        // ## Get Height
        $MainTitle_Height = ($Table_Height * 0.01);
        $SubTitle_Height = ($Table_Height * 0.14);
        $EachStd_Height = ($Table_Height * 0.85) / 40;
        $RemainingPlace_Height = $Table_Height - ($EachStd_Height * $numOfStudent);

        // ## Get Number of <td> for each students
        $NumberOfTD = 2 + $numOfPersonalChar + 1 + $Count_SubSubjects;
        for ($i = 0; $i < $numOfReportColumn; $i ++) {
            $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
            if (in_array($thisReportColumnID, $ReportColumnIDArr)) {
                $NumberOfTD += 2;
            } else {
                continue;
            }
        }

        // ## Get column width
        $tdCol_Width = my_round((55 / ($NumberOfTD - 3)), 2);

        $table_font = 'font_6pt';
        $html = '';
        $html .= '<table cellpadding="2" class="border_collapse ' . $table_font . ' " style="height:100%; width:100%;">';

        $html .= '<colgroup>';
            $html .= '<col style="width:10%;">';
            $html .= '<col style="width:4%;">';
            $html .= '<col style="width:10%;">';
            for($i=0; $i<$numOfPersonalChar; $i++) {
                $html .= '<col style="width:'.$tdCol_Width.'%;">';
            }
            $html .= '<col style="width:21%;">';
            for($i=0; $i<($NumberOfTD - 3 - $numOfPersonalChar); $i++) {
                $html .= '<col style="width:'.$tdCol_Width.'%;">';
            }
        $html .= '</colgroup>';

        $html .= '<tr>' . "\r\n";
        $html .= '<td style="width:10%;">&nbsp;</td>';      // Class Name
        $html .= '<td style="width:4%;">&nbsp;</td>';       // Class Number
        $html .= '<td style="width:10%;" class="border_right">&nbsp;</td>';     // Student Name
        $html .= '<td colspan="' . $numOfPersonalChar . '" class="border_right border_bottom border_top" style="height:' . $MainTitle_Height . 'px">' . $eReportCard['SubjectAcademicResult']['StudentReport'] . ' (' . $TermTitle . ')' . '</td>';
        $html .= '<td class="border_right border_bottom border_top">' . $eReportCard['SubjectAcademicResult']['En']['LA'] . '</td>';

        if ($Count_SubSubjects > 0) {
            $html .= '<td colspan="' . $Count_SubSubjects . '"  class="border_right border_bottom border_top">' . $eReportCard['SubjectAcademicResult']['StudentReport'] . ' (' . $TermTitle . ')' . '</td>';
        }

        // ## test, Exam....etc
        for ($i = 0; $i < $numOfReportColumn; $i ++) {
            $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
            if (in_array($thisReportColumnID, $ReportColumnIDArr)) {} else {
                continue;
            }

            $thisReportColumnTitle = $ReportColumnInfoArr[$i]['ColumnTitle'];
            $html .= '<td colspan="2" class="border_top border_right border_bottom">' . $thisReportColumnTitle . '</td>' . "\r\n";
        }
        $html .= '</tr>' . "\r\n";

        // ## Overall
        $html .= '<tr>' . "\r\n";
        $html .= '<td class="border_right border_bottom" colspan="3" style="vertical-align:bottom;">' . $eReportCard['SubjectAcademicResult']['En']['Entry'] . ' ' . $count_Student . '</td>';

        $thisLang = ($lang == 'En') ? 'EN' : 'CH';
        for ($i = 0; $i < $numOfPersonalChar; $i ++) {
            $thisPersonalCharTitle = $PersonalCharInfoArr[$i]['Title_' . $thisLang];
            $thisPersonalCharTitle = $thisPersonalCharTitle;

            // $html .= '<td class="vertical_text_ie border_right border_bottom " style="padding-left:1px; padding-right:1px;height:'.$SubTitle_Height.'px">'.$thisPersonalCharTitle.'</td>'."\r\n";
            $html .= '<td class="border_right border_bottom" style="height:'.$SubTitle_Height.'px; width:'.$tdCol_Width.'%; text-align:right;">';
                $html .= '<span class="vertical_text_ie">'.$thisPersonalCharTitle.'</span>';
            $html .= '</td>'."\r\n";
        }
        // $html .='<td class="vertical_text_ie border_right border_bottom">'.$eReportCard['SubjectAcademicResult']['En']['AvgGradeArr']['Word'].'</td>';
        // $html .='<td class="vertical_text_ie border_right border_bottom">'.$eReportCard['SubjectAcademicResult']['En']['GradePtArr']['Word'].'</td>';
        // $html .= '<td class="vertical_text_ie border_right border_bottom">'.$eReportCard['SubjectAcademicResult']['En']['ReasonArr']['Word'].'</td>';
        $html .= '<td class="border_right border_bottom" style="height:'.$SubTitle_Height.'px; width:21%; text-align:right;">';
            $html .= '<span class="vertical_text_ie">'.$eReportCard['SubjectAcademicResult']['En']['ReasonArr']['Word'].'</span>';
        $html .= '</td>';

        foreach ((array) $MainSubjectArr[$SubjectID] as $thisColumnID => $thisSubjectName) {
            if ($thisColumnID == 0) {
                continue;
            }

            // $html .= '<td class="vertical_text_ie border_right border_bottom">'.$thisSubjectName.'</td>';
            $html .= '<td class="border_right border_bottom" style="height:'.$SubTitle_Height.'px; width:'.$tdCol_Width.'%; text-align:right;">';
                $html .= '<span class="vertical_text_ie">'.$thisSubjectName.'</span>';
            $html .= '</td>';
        }

        for ($i = 0; $i < $numOfReportColumn; $i ++) {
            $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
            if (in_array($thisReportColumnID, $ReportColumnIDArr)) {
                // do nothing
            } else {
                continue;
            }

            // $html .= '<td class="vertical_text_ie border_right border_bottom">'.$eReportCard['SubjectAcademicResult']['En']['MarksArr']['Word'].'</td>' . "\r\n";
            // $html .= '<td class="vertical_text_ie border_right border_bottom">'.$eReportCard['SubjectAcademicResult']['En']['GradeArr']['Word'].'</td>' . "\r\n";
            $html .= '<td class="border_right border_bottom" style="height:'.$SubTitle_Height.'px; width:'.$tdCol_Width.'%; text-align:right;">';
                $html .= '<span class="vertical_text_ie">'.$eReportCard['SubjectAcademicResult']['En']['MarksArr']['Word'].'</span>';
            $html .= '</td>';
            $html .= '<td class="border_right border_bottom" style="height:'.$SubTitle_Height.'px; width:'.$tdCol_Width.'%; text-align:right;">';
                $html .= '<span class="vertical_text_ie">'.$eReportCard['SubjectAcademicResult']['En']['GradeArr']['Word'].'</span>';
            $html .= '</td>';
        }

        // $eReportCard['SubjectAcademicResult']['MarksArr']['Word']
        $html .= '</tr>' . "\r\n";

        foreach ((array) $StudentIDArr as $key => $thisStudentID) {
            // debug_r($thisStudentID);
            $StudentInfoArr = current($this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ParClassID = "", $thisStudentID, $ReturnAsso = 0, $isShowStyle = 0));

            $thisClassTitle = $StudentInfoArr['ClassTitle' . $lang];
            $thisClassNumber = $StudentInfoArr['ClassNumber'];
            $thisStudentName = $StudentInfoArr['StudentName'];

            $html .= '<tr>
						<td class="border_right border_bottom border_left" style="height:' . $EachStd_Height . 'px">' . $thisClassTitle . '</td>
						<td class="border_right border_bottom border_left">' . $thisClassNumber . '</td>
						<td class="border_right border_bottom border_left" style="font-size:7px">' . $thisStudentName . '</td>';

            for ($i = 0; $i < $numOfPersonalChar; $i ++) {
                $thisPersonalCharID = $PersonalCharInfoArr[$i]['CharID'];
                $thisPersonalCharGrade = $StudentPersonalCharInfoArr[$PersonalCharSourceReportID][$SubjectID][$thisStudentID][$thisPersonalCharID];
                $thisPersonalCharGrade = ($thisPersonalCharGrade == '') ? '&nbsp;' : $thisPersonalCharGrade;
                $html .= '<td class="border_right border_bottom" style="padding-left:1px; padding-right:1px;">' . $thisPersonalCharGrade . '</td>' . "\r\n";
            }

            // ## LA (Reason)
            $ReasonStr = $StudentPersonalCharInfoArr[$PersonalCharSourceReportID][$SubjectID][$thisStudentID]['comment'];
            $html .= '<td class="border_right border_bottom">' . $ReasonStr . '</td>';

            if ($Count_SubSubjects > 0) {
                foreach ((array) $MainSubjectArr[$SubjectID] as $thisSubjectID => $thisSubjectName) {
                    if ($thisSubjectID == 0) {
                        $thisIsComponentSubject = false;
                        $thisSubjectID = $SubjectID;
                        continue;
                    } else {
                        $thisIsComponentSubject = true;
                    }

                    // Grading Scheme Info
                    $thisScaleInput = $GradingSchemeInfoArr[$thisSubjectID]['ScaleInput'];
                    $thisScaleDisplay = $GradingSchemeInfoArr[$thisSubjectID]['ScaleDisplay'];

                    // Report Column Marks
                    $thisIsAllNA = true;
                    $thisMarkDisplayArr = array();

                    $thisTargetReportID = $ReportID;

                    $thisMSMark = $MarksArr[$thisTargetReportID][$thisStudentID][$thisSubjectID][0]['Mark'];
                    $thisMSGrade = $MarksArr[$thisTargetReportID][$thisStudentID][$thisSubjectID][0]['Grade'];

                    $thisGrade = ($thisScaleDisplay == "G" || $thisScaleDisplay == "" || $thisMSGrade != '') ? $thisMSGrade : "";
                    $thisMark = ($thisScaleDisplay == "M" && $thisGrade == '') ? $thisMSMark : "";
                    $thisMark = ($thisScaleDisplay == "M" && strlen($thisMark)) ? $thisMark : $thisGrade;

                    // check special case
                    list ($thisMark, $needStyle) = $this->checkSpCase($ReportID, $thisSubjectID, $thisMark, $thisMSGrade);

                    if ($needStyle) {
                        $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $thisSubjectID, $thisMark);
                    } else {
                        $thisMarkDisplay = $thisMark;
                    }

                    $html .= '<td class="border_right border_bottom">' . $thisMarkDisplay . '</td>' . "\r\n";
                }
            }

            for ($i = 0; $i < $numOfReportColumn; $i ++) {
                $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];

                if (in_array($thisReportColumnID, $ReportColumnIDArr)) {} else {
                    continue;
                }

                // Grading Scheme Info
                $thisScaleInput = $GradingSchemeInfoArr[$SubjectID]['ScaleInput'];
                $thisScaleDisplay = $GradingSchemeInfoArr[$SubjectID]['ScaleDisplay'];

                // Report Column Marks
                $thisTargetReportID = $ReportID;

                $thisMark = $MarksArr[$thisTargetReportID][$thisStudentID][$SubjectID][$thisReportColumnID]['Mark'];
                $thisGrade = $MarksArr[$thisTargetReportID][$thisStudentID][$SubjectID][$thisReportColumnID]['Grade'];

                // check special case
                list ($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);

                if ($needStyle) {
                    $thisMarkDisplay = $this->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisGrade);
                    $thisGradeDisplay = $this->Get_Score_Display_HTML($thisGrade, $ReportID, $ClassLevelID, $SubjectID, $thisGrade);
                } else {
                    $thisMarkDisplay = $thisMark;
                    $thisGradeDisplay = $thisGrade;
                }

                $thisMarkDisplay = ($thisMarkDisplay == '') ? '&nbsp;' : $thisMarkDisplay;
                $thisGradeDisplay = ($thisGradeDisplay == '') ? '&nbsp;' : $thisGradeDisplay;

                $html .= '<td class="border_right border_bottom">' . $thisMarkDisplay . '</td>' . "\r\n";
                $html .= '<td class="border_right border_bottom">' . $thisGradeDisplay . '</td>' . "\r\n";
            }

            $html .= '</tr>';
        }
        $RemainingRow = 32 - $numOfStudent;

        for ($j = 0; $j < $RemainingRow; $j ++) {
            $html .= '<tr>';
            $html .= '<td class="border_left border_right border_bottom" style="height:' . $EachStd_Height . 'px"></td>';
            for ($k = 0; $k < $NumberOfTD; $k ++) {
                $html .= '<td class="border_right border_bottom" style="height:' . $EachStd_Height . 'px"></td>';
            }
            $html .= '</tr>';
        }

        $html .= '</table>';

        return $html;
    }

    function Get_SubjectAcademicResultReport_right_UI($Post_Arr, $InfoArray, $SubjectID, $lang = '', $SchemeID, $myChartID, &$outChart)
    {
        global $eReportCard, $PATH_WRT_ROOT;

        $ClassLevelID = $Post_Arr['ClassLevelID'];
        $ReportID = $Post_Arr['ReportID'];
        $ReportColumnIDArr = $Post_Arr['ReportColumnID'];
        $SelectFrom = $Post_Arr['SelectFrom']; // subjectgroup or class
        $YearClassIDArr = $Post_Arr['YearClassIDArr'];
        $TermID = $Post_Arr['TermID'];
        $SubjectGroupIDArr = $Post_Arr['SubjectGroupIDArr'];
        $StudentIDArr = $Post_Arr['StudentIDArr'];

        $ReportInfoArr = $InfoArray['ReportInfoArr'];
        $MainSubjectArr = $InfoArray['MainSubjectArr'];
        $StudentPersonalCharInfoArr = $InfoArray['StudentPersonalCharInfoArr'];
        $GradingSchemeInfoArr = $InfoArray['GradingSchemeInfoArr'];
        $MarksArr = $InfoArray['MarksArr'];
        $PersonalCharSourceReportID = $InfoArray['PersonalCharSourceReportID'];
        $TermTitle = $InfoArray['TermTitle'];
        $ReportColumnInfoArr = $InfoArray['ReportColumnInfoArr'];
        $PersonalCharInfoArr = $InfoArray['PersonalCharInfoArr'];
        $AllSubjectWeightInfoAssoArr = $InfoArray['AllSubjectWeightInfoAssoArr'];
        $Grade_Statistics_Info_Arr = $InfoArray['Grade_Statistics_Info_Arr'];
        $Mark_Statistics_Info_Arr = $InfoArray['Mark_Statistics_Info_Arr'];
        $ClassID = $InfoArray['ClassID'];
        $SubjectGroupID = $InfoArray['SubjectGroupID'];

        $lang = ($lang == 'en') ? 'En' : 'Ch';

        $EmptySymbol = '-';

        // ## Part 1 : Learning Attitude
        // $LA_html='<table class="border_table font_7pt" style="width:100%;">';
        // $LA_html='<tr><td></td></tr>';
        // $LA_html=' <col width = "14%"/>';
        // <col width = "14%"/>
        // <tr>
        // <td rowspan="2">'.$eReportCard['SubjectAcademicResult']['En']['LearningAtt'].'<br/>('.$eReportCard['SubjectAcademicResult']['En']['LA'].')</td>
        // <td>'.$eReportCard['SubjectAcademicResult']['En']['AvgGradeArr']['Word'].'</td>';
        //
        // for($i=0;$i<5;$i++)
        // {
        // $LA_html.=' <td>'.'&nbsp;'.'</td>';
        // }
        // $LA_html.=' </tr>';
        //
        // $LA_html.=' <tr>
        // <td>'.$eReportCard['SubjectAcademicResult']['En']['GradePtArr']['Word'].'</td> ';
        // for($i=0;$i<5;$i++)
        // {
        // $LA_html.=' <td>'.'&nbsp;'.'</td>';
        // }
        // $LA_html.=' </tr>';
        // $LA_html.=' </table>';

        // ## End Part 1 : Learning Attitude

        // ## Part 2 : Grade Scale
        $Grading_Scheme_Range_InfoArray = $this->GET_GRADING_SCHEME_RANGE_INFO($SchemeID, $Nature = "ALL");

        $GradeArr = array();
        $MarksRangeArr = array();
        $Max_Mark = 100;
        foreach ((array) $Grading_Scheme_Range_InfoArray as $key => $thisGrading_Scheme_RangeArr) {
            $GradeArr[] = $thisGrading_Scheme_RangeArr['Grade'];
            $thisLowerLimit = $thisGrading_Scheme_RangeArr['LowerLimit'];
            $MarksRangeArr[] = $Max_Mark . '-' . $thisLowerLimit;
            $Max_Mark = $thisLowerLimit - 1;
        }

        $GradeScale_html = '';
        $GradeScale_html = '<table cellpadding="2" style="text-align:center;width:100%;"  class="font_7pt border_table" >';
        $GradeScale_html .= '<tr>';
        $GradeScale_html .= '<td rowspan="2">' . $eReportCard['SubjectAcademicResult']['En']['GradeScale'] . '</td>';
        $GradeScale_html .= '<td>' . $eReportCard['SubjectAcademicResult']['En']['GradeArr']['Word'] . '</td>';
        for ($i = 0, $i_MAX = count($GradeArr); $i < $i_MAX; $i ++) {
            $GradeScale_html .= '<td>' . $GradeArr[$i] . '</td>';
        }
        $GradeScale_html .= '<td>' . $eReportCard['SubjectAcademicResult']['En']['Abs'] . '</td>';
        $GradeScale_html .= '</tr>';
        $GradeScale_html .= '<tr>';
        $GradeScale_html .= '<td>' . $eReportCard['SubjectAcademicResult']['En']['MarksArr']['Word'] . '</td>';
        for ($i = 0, $i_MAX = count($MarksRangeArr); $i < $i_MAX; $i ++) {
            $GradeScale_html .= '<td>' . $MarksRangeArr[$i] . '</td>';
        }
        $GradeScale_html .= '<td>' . $eReportCard['SubjectAcademicResult']['En']['Abs'] . '</td>';
        $GradeScale_html .= '</tr>';
        $GradeScale_html .= '</table>';

        // ## End Part 2 : Grade Scale

        // ## Part 3 : For amendment only
        $longLine = '_____________________';
        $AmendOnly_html = '';
        $AmendOnly_html = '<table cellpadding="10" style="text-align:left;width:100%;"  class="font_8pt" >
							<tr>
								<td colspan="2">' . $eReportCard['SubjectAcademicResult']['En']['ForAmendOnly'] . ':' . '</td>
							</tr>
							<tr>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['Name'] . ' (' . $eReportCard['SubjectAcademicResult']['En']['SDO'] . '):' . $longLine . '</td>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['ReceivedDate'] . ' (' . $eReportCard['SubjectAcademicResult']['En']['SDO'] . '):' . $longLine . '</td>
							</tr>
							<tr>
								<td colspan="2">' . $eReportCard['SubjectAcademicResult']['En']['Signature'] . ' (' . $eReportCard['SubjectAcademicResult']['En']['SDO'] . '):' . $longLine . '</td>
							</tr>
							<tr>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['Name'] . ' (' . $eReportCard['SubjectAcademicResult']['En']['Office'] . '):' . $longLine . '</td>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['ReceivedDate'] . ' (' . $eReportCard['SubjectAcademicResult']['En']['Office'] . '):' . $longLine . '</td>
							</tr>
							<tr>
								<td colspan="2">' . $eReportCard['SubjectAcademicResult']['En']['Signature'] . ' (' . $eReportCard['SubjectAcademicResult']['En']['Office'] . '):' . $longLine . '</td>
							</tr>
								    
						 </table>';
        // ## End Part 3 : For amendment only

        // ## Part 4 : Simple Statistics

        $numOfReportColumn = count($ReportColumnInfoArr);
        $parStatsFieldTextArr = array();
        $parByFieldArr = array();
        $chartDataArr = array();

        $showBarCharColumnNameAry = array(
            'Em'
        );
        $colourArr = array(
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#92d24f',
            '#eaa325',
            '#f0e414',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A',
            '#72a9db',
            '#eb593b',
            '#aaaaaa',
            '#B40431',
            '#04B486',
            '#0A0A2A'
        );

        $Stat_html = '<table class="font_7pt" cellpadding="0" style="text-align:left;">';
        $Stat_html .= '<tr>';

        $colorCount = 0;
        for ($i = 0; $i < $numOfReportColumn; $i ++) {
            $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
            $thisReportColumnTitle = $ReportColumnInfoArr[$i]['ColumnTitle'];

            if (in_array($thisReportColumnID, $ReportColumnIDArr)) {} else {
                continue;
            }

            if (in_array($thisReportColumnTitle, $showBarCharColumnNameAry) || $thisReportColumnID == 0) {
                $bgcolor = $colourArr[$colorCount ++];
            } else {
                $bgcolor = '';
            }

            $Stat_html .= '<td style="vertical-align:top;"><table class="border_collapse font_7pt" cellpadding="2" style="text-align:left;">';
            $parStatsFieldTextArr[$thisReportColumnID] = $thisReportColumnTitle;
            $Stat_html .= '<tr>
								<td class="border_left_bold border_top_bold border_right_bold border_bottom_bold" style="text-align:center"><b>' . $thisReportColumnTitle . '</b></td>
								<td class="border_top_bold border_bottom_bold" style="text-align:center" bgcolor="' . $bgcolor . '">&nbsp;</td>
								<td class="border_top_bold border_right_bold border_bottom_bold"  bgcolor="' . $bgcolor . '" style="text-align:center">&nbsp;</td>
						   </tr>' . "\r\n";

            $TotalStudent = 0;

            if ($SelectFrom == 'class') {
                $ClassID_SubjGroupID = $ClassID;
            } else if ($SelectFrom == 'subjectgroup') {
                $ClassID_SubjGroupID = $SubjectGroupID;
            }

            $numOfGradeInfo = count($Grade_Statistics_Info_Arr);
            foreach ((array) $Grade_Statistics_Info_Arr as $_grade => $_gradeInfoArr) {
                $_stdNum = $_gradeInfoArr[$thisReportColumnID][$ClassID_SubjGroupID]['NumOfStudent'];
                $thisStdNum = ($_stdNum == '') ? 0 : $_stdNum;
                $thisGradePercent = ($_gradeInfoArr[$thisReportColumnID][$ClassID_SubjGroupID]['Percentage'] == '') ? $EmptySymbol : $_gradeInfoArr[$thisReportColumnID][$ClassID_SubjGroupID]['Percentage'] . '%';

                $Stat_html .= '<tr>
									 <td class="border_left_bold  border_right_bold" style="text-align:center">' . $_grade . '</td>
								     <td class="" style="text-align:center">' . $thisStdNum . '</td>
									 <td class="border_right_bold" style="text-align:center">' . $thisGradePercent . '</td>
							   	   </tr>';
                $TotalStudent += $_stdNum;
            }

            $AverageDisplay = $Mark_Statistics_Info_Arr[$thisReportColumnID][$SubjectID][$ClassID_SubjGroupID]['Average_Rounded'];
            $SD_Display = $Mark_Statistics_Info_Arr[$thisReportColumnID][$SubjectID][$ClassID_SubjGroupID]['SD_Rounded'];
            $MaxDisplay = $Mark_Statistics_Info_Arr[$thisReportColumnID][$SubjectID][$ClassID_SubjGroupID]['MaxMark'];
            $MinDisplay = $Mark_Statistics_Info_Arr[$thisReportColumnID][$SubjectID][$ClassID_SubjGroupID]['MinMark'];

            $Stat_html .= '<tr>
									<td class="border_left_bold border_top_bold border_right_bold border_bottom_bold" style="text-align:center"><b>' . $eReportCard['SubjectAcademicResult']['En']['Tot'] . '</b></td>
								    <td class="border_top_bold border_right_bold border_bottom_bold" style="text-align:center">' . $TotalStudent . '</td>
									<td class="border_top_bold border_right_bold" style="text-align:center">&nbsp;</td>
							   </tr>';
            $Stat_html .= '<tr>
									<td class="border_left_bold border_top_bold border_bottom_bold">&nbsp;</td>
								    <td colspan="2" class=" border_right_bold border_bottom_bold" style="text-align:center">' . '</td>
							   </tr>';

            $Stat_html .= '<tr>
									<td class="border_left_bold border_top_bold border_right_bold border_bottom_bold"><b>' . $eReportCard['SubjectAcademicResult']['En']['Mean'] . '</b></td>
								    <td colspan="2" class="border_top_bold border_right_bold border_bottom_bold" style="text-align:center">' . $AverageDisplay . '</td>
							   </tr>
							   <tr>
									<td class="border_left_bold border_top_bold border_right_bold border_bottom_bold"><b>' . $eReportCard['SubjectAcademicResult']['En']['SD'] . '</b></td>
								    <td colspan="2" class="border_top_bold border_right_bold border_bottom_bold" style="text-align:center">' . $SD_Display . '</td>
							   </tr>
							   <tr>
									<td class="border_left_bold border_top_bold border_right_bold border_bottom_bold"><b>' . $eReportCard['SubjectAcademicResult']['En']['Max'] . '</b></td>
								    <td colspan="2" class="border_top_bold border_right_bold border_bottom_bold" style="text-align:center">' . $MaxDisplay . '</td>
							   </tr>
							   <tr>
									<td class="border_left_bold border_top_bold border_right_bold border_bottom_bold"><b>' . $eReportCard['SubjectAcademicResult']['En']['Min'] . '</b></td>
								    <td colspan="2" class="border_top_bold border_right_bold border_bottom_bold" style="text-align:center">' . $MinDisplay . '</td>
							   </tr>';

            $Stat_html .= '</table></td>';
        }
        $Stat_html .= '</tr>';
        $Stat_html .= '</table>';

        $parByFieldArr = array();
        $x_axis_titleArr = array();
        foreach ((array) $Grade_Statistics_Info_Arr as $_grade => $_gradeInfoArr) {
            $x_axis_titleArr[] = (string) $_grade;

            for ($i = 0; $i < $numOfReportColumn; $i ++) {
                $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
                if (in_array($thisReportColumnID, $ReportColumnIDArr)) {} else {
                    continue;
                }
                $_stdNum = $_gradeInfoArr[$thisReportColumnID][$ClassID_SubjGroupID]['NumOfStudent'];
                $mainParByFieldArr[$thisReportColumnID][] = ($_stdNum == '') ? 0 : $_stdNum;
            }
        }

        /**
         * ************************ Flash Chart **********************************
         */
        $Chart_html = '';
        // $colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa', '#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa', '#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa', '#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa', '#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa', '#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa','#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa', '#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa','#B40431', '#04B486', '#0A0A2A', '#72a9db', '#eb593b', '#aaaaaa', '#B40431', '#04B486', '#0A0A2A',);

        $chart = new open_flash_chart();
        $i = 0;
        $all_item = $ReportColumnIDArr;
        foreach ($parStatsFieldTextArr as $thisReportColumnID => $statsValue) { // selected item name
            if (in_array($statsValue, $showBarCharColumnNameAry) || $thisReportColumnID == 0) {} else {
                continue;
            }

            $statsUnicodeValue = intranet_undo_htmlspecialchars(stripslashes($statsValue));
            $statsValue = stripslashes($statsValue);
            $valueArr = array();

            $bar = new bar_glass();

            $bar->set_expandable(false);
            $bar->set_colour("$colourArr[$i]");

            // remember the column colour for printing
            $itemNameColourMappingArr[$statsValue] = $colourArr[$i];

            $j = 0;
            $valueArr = array();
            // $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];

            if (in_array($thisReportColumnID, $ReportColumnIDArr)) {

                $parByFieldArr = $mainParByFieldArr[$thisReportColumnID];
                foreach ((array)$parByFieldArr as $byValue) {
                    $byValue = stripslashes($byValue);
                    // $tmpValue = $chartDataArr[$byValue][$statsValue];

                    $tmpValue = $byValue;
                    $valueArr[] = (int) $tmpValue;

                    // $bar->set_values( $valueArr );
                }
                $bar->set_values($valueArr);
            }

            // $bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#val#"); # x-axis item
            // $bar->set_key("$statsUnicodeValue", "12"); //here is the selection box <-- if the name is too name will cause the image cannot be generated

            $barID = $myChartID . $i;
            $bar->set_id($barID);

            $flag = 0;
            foreach ($parStatsFieldTextArr as $statsValue2) {
                $statsValue2 = stripslashes($statsValue2);
                if ($flag == 0) {
                    if ($statsValue == $statsValue2) { // for selected AP
                        $bar->set_visible(true);
                        $flag = 1;
                    } else {
                        $bar->set_visible(false);
                    }
                }
            }

            // y axis
            $y_legend = new y_legend($eReportCard['SubjectAcademicResult']['En']['Frequency']);
            $y_legend->set_style('{font-size: 12px; font-weight: bold; color: #1e9dff}');

            $y = new y_axis();
            $y->set_stroke(2);
            $y->set_tick_length(2);
            $y->set_colour('#999999');
            $y->set_grid_colour('#CCCCCC');
            $y->set_range(0, count($StudentIDArr), 2);
            $y->set_offset(true);

            // x axis
            // $x_axis_title []='connie';
            $x_legend = new x_legend($eReportCard['SubjectAcademicResult']['En']['GradeArr']['Word']);
            $x_legend->set_style('{font-size: 12px; font-weight: bold; color: #1e9dff}');

            $x = new x_axis();
            $x->set_stroke(2);
            $x->set_tick_height(2);
            $x->set_colour('#999999');
            $x->set_grid_colour('#CCCCCC');
            $x->set_labels_from_array($x_axis_titleArr);

            $chart->set_y_legend($y_legend);
            $chart->set_y_axis($y);
            $chart->set_x_legend($x_legend);
            $chart->set_x_axis($x);

            // $chart->set_key_legend('chart_'.$myChartID);
            $chart->add_element($bar);
            $i ++;
        }
//         $Chart_html = '<div id="div' . $myChartID . 'parent"><div id="div' . $myChartID . '">no flash?</div></div>';
        $Chart_html = '<div id="div' . $myChartID . 'parent"><div id="div' . $myChartID . '"></div></div>';

        # 20200226 (Philips) [2020-0122-1154-15073] - Use Highcharts instead of flash chart
        $xAxisCategories = implode("','", $x_axis_titleArr);
        $seriesData = implode(",", $valueArr);
        $generateScript = <<< EOD
Highcharts.chart('div{$myChartID}',{
	chart: {backgroundColor: '#ffdab9'	,type: 'column', width: 250, height: 280, style: {'font-size' : '9px'}},
	title:{style: {'display':'none'}},
	xAxis:{
		labels: {padding: '2', autoRotation: '[0]', style: {'font-size': '7px'}},
		categories:['{$xAxisCategories}'],
		title: {text: '{$eReportCard['SubjectAcademicResult']['En']['GradeArr']['Word']}'}
	},
	yAxis:{
		min: 0,
		max: 28,
		tickInterval: 2,
		tickLength: 3,
		plotBands: {thickness: "3"},
		title:{text: '{$eReportCard['SubjectAcademicResult']['En']['Frequency']}'}
	},
	series: [{colorByPoint: true, name:'', data:[{$seriesData}]}],
	credits: {
		enabled: false
	},
	tooltip: {enabled: false},
	legend: {enabled: false},
	exporting: {enabled: false}
});
EOD;

//         $outChart = $chart;
        $outChart = $generateScript;

 //         debug_r($outChart);
        /**
         * ******************** Main Simple Statistics ******************************
         */
        $SimpleStat_html = '';
        $SimpleStat_html = '<table class="" cellpadding="0" style="text-align:left;"  >
							   <tr><td colspan="2" style="padding-left:10px" class="font_9pt"><b>' . $eReportCard['SubjectAcademicResult']['En']['SimpleStat'] . ':' . '</b></td></tr>
							   <tr>
									<td style="width:50%;vertical-align:top;">' . $Stat_html . '</td>
									<td style="width:50%;vertical-align:top;">' . $Chart_html . '</td>
							   </tr>
						   </table>';

        // ## End Part 4 : Simple Statistics

        // ## Part 5 : Signature
        // 2014-1211-0922-37073
        $lastTermId = $this->Get_Last_Semester_Of_Report($ReportID);
        $subjectTeacherInfoAry = $this->Get_Student_Subject_Teacher($lastTermId, $StudentIDArr, $SubjectID);
        $targetNameField = Get_Lang_Selection('ChineseName', 'EnglishName');
        $subjectTeacherNameAry = Get_Array_By_Key($subjectTeacherInfoAry, $targetNameField);
        $subjectTeacherNameDisplay = implode(', ', $subjectTeacherNameAry);
        $subjectTeacherNameDisplay = ($subjectTeacherNameDisplay == '') ? '&nbsp;' : $subjectTeacherNameDisplay;

        $XSLine = '______________';
        $shortLine = '__________________';
        $middleLine = '_______________________';
        $longLine = '___________________________';
        $Signature_html = '';
        $Signature_html = '<table cellpadding="8" style="text-align:left;width:100%;"  class="font_7pt" >
							<tr>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['SubjectTeacher'] . ': <span style="border-bottom:1px solid #000000;">' . $subjectTeacherNameDisplay . '</span></td>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['Checker'] . ': ' . $middleLine . '</td>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['SubjCoordinator'] . ': ' . $XSLine . '</td>
							</tr>
							<tr>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['Signature'] . ': ' . $middleLine . '</td>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['Signature'] . ': ' . $middleLine . '</td>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['Signature'] . ': ' . $middleLine . '</td>
							</tr>
							<tr>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['Date'] . ': ' . $longLine . '</td>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['Date'] . ': ' . $longLine . '</td>
								<td>' . $eReportCard['SubjectAcademicResult']['En']['Date'] . ': ' . $longLine . '</td>
							</tr>
								    
						 </table>';
        // ## End Part 5 : Signature

        /**
         * ************ Main Table ******************
         */

        $table_font = 'font_7pt';
        $html = '';
        $html .= '<table cellpadding="2" class=" " style="height:100%; width:100%; vertical-align:top;">
				     <tr style="height:5%;"><td style="vertical-align:top;">' . $LA_html . '</td></tr>
					 <tr style="height:5%;"><td style="vertical-align:top;">' . $GradeScale_html . '</td> </tr>
			  		 <tr style="height:20%;"><td style="vertical-align:top;">' . $AmendOnly_html . '</td></tr>
					 <tr style="height:60%;"><td style="vertical-align:top;">' . $SimpleStat_html . '</td> </tr>
					 <tr style="height:10%;"><td style="vertical-align:top;">' . $Signature_html . '</td> </tr>';
        $html .= '</table>';
        return $html;
    }

    function Get_SubjectAcademicResult_UI()
    {
        global $eReportCard, $linterface, $image_path, $LAYOUT_SKIN, $Lang, $eRCTemplateSetting, $sys_custom, $lreportcard;

        $Display_SD_Options = false;
        if ($eRCTemplateSetting['OrderPositionMethod'] == "WeightedSD" || $eRCTemplateSetting['OrderPositionMethod'] == "SD")
            $Display_SD_Options = true;

            $Display_Stream_Options = false;
            if ($sys_custom['Class']['ClassGroupSettings'] == true)
                $Display_Stream_Options = true;
                $Display_Stream_Options = false; // function not ready => set to false now

                $OrderOption = array();

                $ctr = 0;
                // wrapper
                $x .= '<form id="form1" name="form1" action="subject_academic_result.php" method="POST" target="_blank">';

                $x .= '<table width="96%" border="0" cellspacing="0" cellpadding="5">' . "\n";
                $x .= '<tr>' . "\n";
                $x .= '<td  colspan="2" >' . "\n";
                // #### Main Content Start

                // ## Report Settting
                // Get Form selection (show form which has report card only)
                $libForm = new Year();
                $FormArr = $libForm->Get_All_Year_List();
                $ClassLevelID = $FormArr[0]['YearID'];
                $FormSelection = $this->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'js_Reload_Selection(this.value)', $noFirst = 1);

                $x .= $linterface->GET_NAVIGATION2($eReportCard['SubjectAcademicResult']['ReportOption']) . "&nbsp;&nbsp;";
                $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
                // form selection
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard['Form'] . '</td>' . "\n";
                $x .= '<td >' . $FormSelection . '</td>' . "\n";
                $x .= '</tr>' . "\n";
                // Report Selection
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard['Reports'] . '</td>' . "\n";
                $x .= '<td ><span id="ReportSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax--></span></td>' . "\n";
                $x .= '</tr>' . "\n";
                // Report Column Selection
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard['ReportColumn'] . '</td>' . "\n";
                $x .= '<td ><span id="ReportColumnSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax--></span></td>' . "\n";
                $x .= '</tr>' . "\n";
                $x .= '</table><br><br>' . "\n";

                // ## Subject Display Options Start

                // Select All Subject Btn
                $SelectAllSubjBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Subject()");

                $x .= $linterface->GET_NAVIGATION2($eReportCard['SubjectAcademicResult']['SubjectDisplayOption']) . "&nbsp;&nbsp;";
                $x .= '<span id="spanShowOption' . $ctr . '" style="display:none"><a href="javascript:showOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['SubjectAcademicResult']['ShowOption'] . '</a></span>';
                $x .= '<span id="spanHideOption' . $ctr . '"><a href="javascript:hideOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['SubjectAcademicResult']['HideOption'] . '</a></span>';
                $x .= '<table id="table' . $ctr . '" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
                // Subject Selection
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard["Subject"] . '</td>' . "\n";
                $x .= '<td >' . "\n";
                $x .= '<span id="SubjectSelectionSpan">' . "\n";
                $x .= $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax-->' . "\n";
                $x .= '</span>' . "\n";
                $x .= $SelectAllSubjBtn . "\n";
                $x .= '<br>' . "\n";
                $x .= $linterface->MultiSelectionRemark() . "\n";
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";

                $x .= '</table><br><br>' . "\n";
                $ctr ++;
                // ## Subject Display Options End

                // ## Student Display Options Start
                // Get Term Selection
                // $TermSelection = $this->Get_Term_Selection("TermID", $lreportcard->schoolYearID, '', 'js_Reload_Subject_Group_Selection()', 1, 0, 1);

                // Select All Class Btn
                $SelectAllClassBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Class()");
                $SelectAllTermBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Term()");
                $SelectAllSubjBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Subject_Group_Subject()");
                $SelectAllSubjectGroupBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_SubjectGroup()");
                $SelectAllStudentBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentIDArr[]')");

                $x .= $linterface->GET_NAVIGATION2($eReportCard['SubjectAcademicResult']['StudentDisplayOption']) . "&nbsp;&nbsp;";
                $x .= '<span id="spanShowOption' . $ctr . '" style="display:none"><a href="javascript:showOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['SubjectAcademicResult']['ShowOption'] . '</a></span>';
                $x .= '<span id="spanHideOption' . $ctr . '"><a href="javascript:hideOption(\'' . $ctr . '\');" class="contenttool"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle">' . $eReportCard['SubjectAcademicResult']['HideOption'] . '</a></span>';
                $x .= '<table id="table' . $ctr . '" width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
                // select from
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard['SelectFrom'] . '</td>' . "\n";
                $x .= '<td >';
                $x .= $linterface->Get_Radio_Button('SelectFromClass', 'SelectFrom', 'class', 1, "SelectFrom", $eReportCard['Class'], "js_Toggle_Student_Selection()") . "\n";
                $x .= $linterface->Get_Radio_Button('SelectFromSubjectGroup', 'SelectFrom', 'subjectgroup', 0, "SelectFrom", $eReportCard['SubjectGroup'], "js_Toggle_Student_Selection()") . "\n";
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";
                // class selection
                $x .= '<tr class="ClassSelectOption">' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard['Class'] . '</td>' . "\n";
                $x .= '<td id="ClassSelectionTD">' . "\n";
                $x .= '<span id="ClassSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!-- load on ajax --></span>' . $SelectAllClassBtn . "\n";
                $x .= '<br>' . "\n";
                $x .= $linterface->MultiSelectionRemark() . "\n";
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";
                // Term selection
                $x .= '<tr class="SubjectGroupSelectOption">' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard['Term'] . '</td>' . "\n";
                $x .= '<td id="TermSelectionTD">' . "\n";
                $x .= '<span id="TermSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '</span>' . $SelectAllTermBtn . "\n";
                $x .= '<br>' . "\n";
                $x .= $linterface->MultiSelectionRemark() . "\n";
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";

                // Subject Group Selection
                $x .= '<tr class="SubjectGroupSelectOption">' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard['SubjectGroup'] . '</td>' . "\n";
                $x .= '<td id="SubjectGroupSelectionTD">' . "\n";
                $x .= '<span id="SubjectGroupSelectionSpan">' . "\n";
                $x .= $linterface->Get_Ajax_Loading_Image() . '<!-- load on ajax -->' . "\n";
                $x .= '</span>' . "\n";
                $x .= $SelectAllSubjectGroupBtn . "\n";
                $x .= '<br>' . "\n";
                $x .= $linterface->MultiSelectionRemark() . "\n";
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";

                // Student Selection
                // $x .= '<tr class="StudentSelectOption">'."\n";
                // $x .= '<td class="field_title" width="30%">'.$eReportCard['Student'].'</td>'."\n";
                // $x .= '<td id="StudentSelectionTD">'."\n";
                // $x .= '<span id="StudentSelectionSpan">'."\n";
                // $x .= $linterface->Get_Ajax_Loading_Image().'<!-- load on ajax -->'."\n";
                // $x .= '</span>'."\n";
                // $x .= $SelectAllStudentBtn."\n";
                // $x .= '<br>'."\n";
                // // $x .= $linterface->MultiSelectionRemark()."\n";
                // $x .= '</td>'."\n";
                // $x .= '</tr>'."\n";

                $x .= '</table><br><br>' . "\n";
                $ctr ++;
                // ## Student Display Options End

                // #### Main Content End
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";

                $x .= '<tr>' . "\n";
                $x .= '<td colspan="2" class="dotline">' . "\n";
                $x .= '<img src="' . $image_path . '/' . $LAYOUT_SKIN . '"/10x10.gif" width="10" height="1">' . "\n";
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";
                $x .= '<tr>' . "\n";
                $x .= '<td colspan="2" align="center">' . "\n";
                $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Check_Form();", "submitBtn");
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";
                $x .= '</table><br><br>' . "\n";
                $x .= '</form>' . "\n";

                return $x;
    }

    function Get_Term_Selection($ID_Name, $AcademicYearID = '', $SelectedYearTermID = '', $OnChange = '', $NoFirst = 0, $WithWholeYear = 0, $IsMultiple = 0)
    {
        // global $Lang, $eReportCard, $PATH_WRT_ROOT;
        // SIS 20140731
        global $Lang, $eReportCard, $PATH_WRT_ROOT, $eRCTemplateSetting;

        include_once ($PATH_WRT_ROOT . 'includes/form_class_manage.php');
        $fcm = new form_class_manage();
        $CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();

        // Set to currect Academic Year if there is no target Academic Year
        if ($AcademicYearID == '')
            $AcademicYearID = $CurrentYearTermArr['AcademicYearID'];

            // Set to currect Semester if there is no target semester
            if ($SelectedYearTermID == '')
                $SelectedYearTermID = $CurrentYearTermArr['YearTermID'];

                $YearTermArr = $fcm->Get_Academic_Year_Term_List($AcademicYearID);

                // 20140731 SIS Special Remarks input in Term 2 (SA1), Term 4 (SA2) only
                if ($eRCTemplateSetting['SpecialRemarks']['LimitTermSelection']) {
                    $tmp = array();
                    foreach ($YearTermArr as $yearTerm) {
                        if (strpos($yearTerm['YearTermNameEN'], 'SA') !== false) {
                            $tmp[] = $yearTerm;
                        }
                    }
                    $YearTermArr = $tmp;
                    // count term 2 term 4 only
                    $numOfYearTerm = count($YearTermArr);

                    $selectArr = array();
                    // Whole Year is not needed
                    // if ($WithWholeYear==1)
                    // $selectArr[0] = $eReportCard['WholeYear'];
                }        // Normal Case
                else {
                    $numOfYearTerm = count($YearTermArr);

                    $selectArr = array();

                    if ($WithWholeYear == 1)
                        $selectArr[0] = $eReportCard['WholeYear'];
                }

                for ($i = 0; $i < $numOfYearTerm; $i ++) {
                    $thisYearTermID = $YearTermArr[$i]['YearTermID'];
                    $thisYearTermName = Get_Lang_Selection($YearTermArr[$i]['YearTermNameB5'], $YearTermArr[$i]['YearTermNameEN']);

                    $selectArr[$thisYearTermID] = $thisYearTermName;
                }

                $onchange = '';
                if ($OnChange != "")
                    $onchange = 'onchange="' . $OnChange . '"';

                    if ($IsMultiple == 1)
                        $Multiple = ' multiple size=10 ';
                        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $Multiple;

                        $firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Term']);

                        $semesterSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedYearTermID, $IsAll = 0, $NoFirst, $firstTitle);

                        return $semesterSelection;
    }

    function Get_HKUGA_Grade_List_Index_UI()
    {
        global $eReportCard, $linterface, $Lang;

        $CheckClassTeacher = ($this->IS_ADMIN_USER($_SESSION['UserID']) || $this->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) ? 0 : 3;

        // Form Selection
        $FormSelection = $this->Get_Form_Selection("YearID", "", "js_Form_Selection_Changed()", 1, 0, 1, $CheckClassTeacher, 0);

        // select all btn
        $SelectAllSubjectBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('SubjectIDArr[]'); js_Reload_SubjectGroup_Selection();");
        $SelectAllClassBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ClassIDArr[]'); ");
        $SelectAllSubjectGroupBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('SubjectGroupIDArr[]'); ");
        // $SelectAllStudentBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('StudentIDArr[]')");

        $x .= '<form action="generate.php" method="post" target="_blank" onsubmit="return checkForm();">';
        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
        // Form
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Form'] . '</td>' . "\n";
        $x .= '<td >' . $FormSelection . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Report
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['ReportCard'] . '</td>' . "\n";
        $x .= '<td ><span class="ReportSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '</span></td>' . "\n";
        $x .= '</tr>' . "\n";
        // Subject
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Subject'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<span class="SubjectSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '</span>' . "\n";
        $x .= $SelectAllSubjectBtn . "\n";
        $x .= '<div>' . $linterface->MultiSelectionRemark() . '</div>' . "\n";
        $x .= $linterface->Get_Form_Warning_Msg("WarnEmptySubject", $Lang['eReportCard']['WarningArr']['Select']['Subject'], "WarnMsg");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // View By
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['ViewBy'] . '</td>' . "\n";
        $x .= '<td >' . "\n";
        $x .= $linterface->Get_Radio_Button("ViewByClass", "ViewBy", 1, 1, "ViewBy", $eReportCard['Class'], "js_Toggle_View();", 0) . "\n";
        $x .= $linterface->Get_Radio_Button("ViewBySubjectGroup", "ViewBy", 2, 0, "ViewBy", $eReportCard['SubjectGroup'], "js_Toggle_View();", 0) . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Class
        $x .= '<tr id="ClassSelectionRow">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Class'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<span class="ClassSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '</span>' . "\n";
        $x .= $SelectAllClassBtn . "\n";
        $x .= '<div>' . $linterface->MultiSelectionRemark() . '</div>' . "\n";
        $x .= $linterface->Get_Form_Warning_Msg("WarnEmptyClass", $Lang['eReportCard']['WarningArr']['Select']['Class'], "WarnMsg");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Subject Group
        $x .= '<tr id="SubjectGroupSelectionRow">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['SubjectGroup'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<span class="SubjectGroupSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '</span>' . "\n";
        $x .= $SelectAllSubjectGroupBtn . "\n";
        $x .= '<div>' . $linterface->MultiSelectionRemark() . '</div>' . "\n";
        $x .= $linterface->Get_Form_Warning_Msg("WarnEmptySubjectGroup", $Lang['eReportCard']['WarningArr']['Select']['SubjectGroup'], "WarnMsg");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Student
        // $x .= '<tr>'."\n";
        // $x .= '<td class="field_title" width="30%">'.$eReportCard['Student'].'</td>'."\n";
        // $x .= '<td>'."\n";
        // $x .= '<span class="StudentSelectionSpan">'.$linterface->Get_Ajax_Loading_Image().'</span>'."\n";
        // $x .= $SelectAllStudentBtn."\n";
        // $x .= '<div>'.$linterface->MultiSelectionRemark().'</div>'."\n";
        // $x .= '</td>'."\n";
        // $x .= '</tr>'."\n";

        $x .= '</table>' . "\n";
        $x .= $linterface->MandatoryField() . "\n";
        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "Submit") . "\n";
        $x .= '' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</form>';
        return $x;
    }

    function Get_HKUGA_Grade_List_Table($MarksAry, $SubjectID, $StudentArr, $CmpSubjectIDArr, $PCDataArr, $PCArr, $CommentArr, $ReportID = '')
    {
        global $eReportCard;

        $GradeWidth = "16";

        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="grade_list_table full_border">' . "\n";

        // Col
        // Student Info
        $x .= '<col width="55px">' . "\n";
        $x .= '<col width="140px">' . "\n";

        // Achievement
        $numOfCmp = count($CmpSubjectIDArr);

        // Count EGCAYG Start
        $numOfEGCGYG = 0;
        $HasEG = false;
        $HasCA = false;
        $HasYG = false;
        foreach ((array) $CmpSubjectIDArr as $_CmpSubjectIDArr) {
            if ($_CmpSubjectIDArr['SubjectName'] == 'Examination') {
                $HasEG = true;
                $numOfEGCGYG ++;
            }
            if ($_CmpSubjectIDArr['SubjectName'] == 'Continuous Assessment') {
                $HasCA = true;
                $numOfEGCGYG ++;
            }
            if ($_CmpSubjectIDArr['SubjectName'] == 'Year Grade') {
                $HasYG = true;
                $numOfEGCGYG ++;
            }
        }
        // Count EGCAYG END
        for ($i = 0; $i < $numOfCmp - $numOfEGCGYG; $i ++) {
            $x .= '<col width="' . $GradeWidth . 'px">' . "\n";
        }
        // for($i=0; $i<$numOfCmp-3; $i++)
        // {
        // $x .= '<col width="'.$GradeWidth.'px">'."\n";
        // }

        // Attitude
        $numOfPC = count($PCDataArr);
        for ($i = 0; $i < $numOfPC; $i ++) {
            $x .= '<col width="' . $GradeWidth . 'px">' . "\n";
        }

        // EG,CG,YG
        $x .= '<col width="' . $GradeWidth . 'px">' . "\n";
        $x .= '<col width="' . $GradeWidth . 'px">' . "\n";
        $x .= '<col width="' . $GradeWidth . 'px">' . "\n";

        $x .= '<col>' . "\n";

        // thead
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th colspan="2">' . $eReportCard['HKUGAGradeList']['Student'] . '</th>' . "\n";
        $x .= '<th colspan="' . ($numOfCmp - $numOfEGCGYG) . '">' . $eReportCard['HKUGAGradeList']['Achievement'] . '</th>' . "\n";
        $x .= '<th colspan="' . $numOfPC . '">' . $eReportCard['HKUGAGradeList']['Attitude'] . '</th>' . "\n";
        if ($HasEG) {
            $x .= '<th>' . $eReportCard['HKUGAGradeList']['EG'] . '</th>' . "\n";
        }
        if ($HasCA) {
            $x .= '<th>' . $eReportCard['HKUGAGradeList']['CG'] . '</th>' . "\n";
        }
        if ($HasYG) {
            $x .= '<th>' . $eReportCard['HKUGAGradeList']['YG'] . '</th>' . "\n";
        }
        $x .= '<th>' . $eReportCard['HKUGAGradeList']['Comments'] . '</th>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";
        // $x .= '<thead>'."\n";
        // $x .= '<tr>'."\n";
        // $x .= '<th colspan="2">'.$eReportCard['HKUGAGradeList']['Student'].'</th>'."\n";
        // $x .= '<th colspan="'.($numOfCmp-3).'">'.$eReportCard['HKUGAGradeList']['Achievement'].'</th>'."\n";
        // $x .= '<th colspan="'.$numOfPC.'">'.$eReportCard['HKUGAGradeList']['Attitude'].'</th>'."\n";
        // $x .= '<th>'.$eReportCard['HKUGAGradeList']['EG'].'</th>'."\n";
        // $x .= '<th>'.$eReportCard['HKUGAGradeList']['CG'].'</th>'."\n";
        // $x .= '<th>'.$eReportCard['HKUGAGradeList']['YG'].'</th>'."\n";
        // $x .= '<th>'.$eReportCard['HKUGAGradeList']['Comments'].'</th>'."\n";
        // $x .= '</tr>'."\n";
        // $x .= '</thead>'."\n";

        // tbody
        $x .= '<tbody>' . "\n";

        foreach ($StudentArr as $StudentInfo) // loop student
        {
            $thisStudentID = $StudentInfo['UserID'];
            $x .= '<tr>' . "\n";

            $ClassNameLang = Get_Lang_Selection("ClassTitleCh", "ClassTitleEn");
            // student info
            $x .= '<td>' . $StudentInfo[$ClassNameLang] . "-" . $StudentInfo['ClassNumber'] . '</td>' . "\n";
            $x .= '<td>' . $StudentInfo['StudentName'] . '</td>' . "\n";

            // Achievement$numOfEGCGYG
            // for($i=0; $i<$numOfCmp-3; $i++)
            $lreportcardcust = new libreportcardcustom();
            if ($lreportcardcust->isModularStudySubject($SubjectID) || $lreportcardcust->isCaseSubject($SubjectID)) {
                $x .= '<td align="center">' . "\n";
                if ($MarksAry[$thisStudentID][$SubjectID][0]['Mark']) {
                    $x .= $MarksAry[$thisStudentID][$SubjectID][0]['Mark'];
                    $x .= '<br>' . "\n";
                }
                $x .= $MarksAry[$thisStudentID][$SubjectID][0]['Grade'];
                $x .= '</td>' . "\n";
            } else {
                for ($i = 0; $i < $numOfCmp - $numOfEGCGYG; $i ++) {
                    $thisCmpSubjID = $CmpSubjectIDArr[$i]['SubjectID'];
                    $x .= '<td align="center">' . "\n";
                    if ($MarksAry[$thisStudentID][$thisCmpSubjID][0]['Mark']) {
                        $x .= $MarksAry[$thisStudentID][$thisCmpSubjID][0]['Mark'];
                        $x .= '<br>' . "\n";
                    }
                    $x .= $MarksAry[$thisStudentID][$thisCmpSubjID][0]['Grade'];
                    $x .= '</td>' . "\n";
                }
            }

            // Attitude
            foreach ((array) $PCDataArr as $PCData) {
                $x .= '<td align="center">' . "\n";
                $x .= $PCArr[$thisStudentID][$PCData['CharID']];
                $x .= '</td>' . "\n";
            }

            // EG, CG, YG
            // for($i=$numOfCmp-3; $i<$numOfCmp; $i++)
            for ($i = $numOfCmp - $numOfEGCGYG; $i < $numOfCmp; $i ++) {
                $thisCmpSubjID = $CmpSubjectIDArr[$i]['SubjectID'];
                $x .= '<td align="center">' . "\n";
                if ($MarksAry[$thisStudentID][$thisCmpSubjID][0]['Mark']) {
                    $x .= $MarksAry[$thisStudentID][$thisCmpSubjID][0]['Mark'];
                    $x .= '<br>' . "\n";
                }

                list ($thisGrade, $needStyle) = $this->checkSpCase($ReportID, $thisCmpSubjID, $MarksAry[$thisStudentID][$thisCmpSubjID][0]['Grade'], $MarksAry[$thisStudentID][$thisCmpSubjID][0]['Grade']);
                $x .= $thisGrade;

                $x .= '</td>' . "\n";
            }

            // Comment
            $x .= '<td class="comment">' . "\n";
            $x .= nl2br(stripslashes(trim($CommentArr[$thisStudentID]['Comment'])));
            $x .= '</td>' . "\n";

            $x .= '</tr>' . "\n";
        }
        $x .= '</tbody>' . "\n";

        $x .= '</table>' . "\n";
        return $x;
    }

    public function Get_Generate_Award_SubTag($ReportID, $CurrentStep)
    {
        global $linterface, $Lang, $eRCTemplateSetting;

        $StepCount = 1;
        $SUBTAGS_OBJ = array();
        $SUBTAGS_OBJ[] = array(
            $Lang['Btn']['Generate'],
            'generate_award.php?ReportID=' . $ReportID,
            ($CurrentStep == $StepCount ++) ? true : false
        );
        $SUBTAGS_OBJ[] = array(
            $Lang['Btn']['View'],
            'view_award_class_mode.php?ReportID=' . $ReportID,
            ($CurrentStep == $StepCount ++) ? true : false
        );
        if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_PrintCertificate']) {
            $SUBTAGS_OBJ[] = array(
                $Lang['Btn']['Print'],
                'print_award.php?ReportID=' . $ReportID,
                ($CurrentStep == $StepCount ++) ? true : false
            );
        }

        return $linterface->GET_SUBTAGS($SUBTAGS_OBJ);
    }

    public function Get_Generate_Award_UI($ReportID)
    {
        global $Lang, $eReportCard;
        global $linterface, $lreportcard_award, $eRCTemplateSetting;

        // ## Get Report Card Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);
        $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID, 1);
        $FormName = $this->returnClassLevel($ClassLevelID);

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Reports_GenerateReport'],
            "javascript:js_Go_Back_To_Report_Generation();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GenerateAward'],
            ""
        );

        // ## Remarks
        $RemarksBox = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DoNotIncludeAdjustedMarkRemarks'], $others = "");

        // ## Buttons
        $GenerateBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "js_Generate_Award();");
        $BackBtn = $linterface->GET_ACTION_BTN($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['BackToReportGeneration'], "button", "js_Go_Back_To_Report_Generation();");

        $x = '';
        $x .= '<form id="form1" name="form1" method="post" action="generate_award_update.php">' . "\n";
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr><td align="center">' . $RemarksBox . '</td></tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportTitle'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTitle . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportType'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTypeDisplay . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
        $x .= '<td>' . $FormName . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<br />' . "\n";

        if (! $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_CustomizedLogic']) {
            $x .= $this->Get_Generate_Award_Settings_Table($ReportID);
            $x .= '<br />' . "\n";
        }

        $x .= '</div>' . "\n";

        // Buttons
        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= '<span class="tabletextremark" id="LastGeneratedSpan"></span>' . "\n";
        $x .= '<br />' . "\n";
        $x .= $GenerateBtn;
        $x .= "&nbsp;";
        $x .= $BackBtn;
        $x .= '</div>' . "\n";

        // Hidden Field
        $x .= '<input type="hidden" id="ReportID" name="ReportID" value="' . $ReportID . '">' . "\n";

        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        return $x;
    }

    private function Get_Generate_Award_Settings_Table($ReportID)
    {
        global $PATH_WRT_ROOT, $Lang;
        global $linterface, $lreportcard_award;

        include_once ($PATH_WRT_ROOT . 'includes/libreportcard2008_award.php');
        $lreportcard_award = new libreportcard_award();

        // ## Get Report Card Info
        $ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);

        $AwardInfoAssoArr = $lreportcard_award->Get_Report_Award_Info($ReportID);
        $numOfAward = count((array) $AwardInfoAssoArr);

        $x = '';
        $x .= $linterface->GET_NAVIGATION2_IP25($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardSettings']);
        $x .= '<table id="ContentTable" class="common_table_list_v30">' . "\n";
        // col group
        $x .= '<col align="left" style="width:3%;">' . "\n";
        $x .= '<col align="left" style="width:27%;">' . "\n";
        $x .= '<col align="left" style="width:70%;">' . "\n";

        // table head
        $x .= '<thead>';
        $x .= '<tr>';
        $x .= '<th>#</th>';
        $x .= '<th>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardName'] . '</th>';
        $x .= '<th>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardCriteria'] . '</th>';
        $x .= '</tr>';
        $x .= '</thead>';

        $x .= '<tbody>';
        if ($numOfAward == 0) {
            $x .= '<tr><td colspan="3" style="text-align:center;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['NoAwardSettingsForThisReportAndForm'] . '</td></tr>' . "\n";
        } else {
            $AwardCount = 0;
            foreach ((array) $AwardInfoAssoArr as $thisAwardID => $thisAwardInfoArr) {
                $thisAwardType = $thisAwardInfoArr['BasicInfo']['AwardType'];
                $thisAwardName = Get_Lang_Selection($thisAwardInfoArr['BasicInfo']['AwardNameCh'], $thisAwardInfoArr['BasicInfo']['AwardNameEn']);
                $thisAwardName = ($thisAwardType == 'SUBJECT') ? $thisAwardName . ' ' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['SubjectPrizeSuffix'] : $thisAwardName;

                $x .= '<tr>' . "\n";
                $x .= '<td>' . (++ $AwardCount) . '</td>' . "\n";
                $x .= '<td>' . $thisAwardName . '</td>' . "\n";
                $x .= '<td>' . $this->Get_Generate_Award_Criteria_Settings_Table($ReportID, $thisAwardID, $thisAwardInfoArr) . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
        }
        $x .= '</tbody>';
        $x .= '</table>';

        return $x;
    }

    private function Get_Generate_Award_Criteria_Settings_Table($ReportID, $AwardID, $AwardInfoArr)
    {
        global $Lang, $eReportCard;

        $x = '';

        $CriteriaCount = 0;
        foreach ((array) $AwardInfoArr['CriteriaInfo'] as $thisCriteriaID => $thisCriteriaInfoArr) {
            $thisCriteriaType = $thisCriteriaInfoArr['CriteriaType'];

            if ($CriteriaCount > 0) {
                $thisPreviousCriteriaRelation = $thisCriteriaInfoArr['PreviousCriteriaRelation'];
                $thisRelationDisplay = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['PreviousCriteriaRelationArr'][$thisPreviousCriteriaRelation];

                $x .= '<div><b>' . $thisRelationDisplay . '</b></div>' . "\n";

                $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $thisCriteriaID . '][PreviousCriteriaRelation]';
                $x .= '<input type="hidden" id="' . $ID_Name . '" name="' . $ID_Name . '" value="' . $thisPreviousCriteriaRelation . '" />' . "\n";
            }

            if ($thisCriteriaType == 'RANK') {
                $x .= $this->Get_Generate_Award_Criteria_Rank_Settings_Display($ReportID, $AwardID, $thisCriteriaID, $thisCriteriaInfoArr);
            } else if ($thisCriteriaType == 'PERSONAL_CHAR') {
                $x .= $this->Get_Generate_Award_Criteria_PersonalChar_Settings_Display($ReportID, $AwardID, $thisCriteriaID, $thisCriteriaInfoArr);
            } else if ($thisCriteriaType == 'IMPROVEMENT') {
                $x .= $this->Get_Generate_Award_Criteria_Improvement_Settings_Display($ReportID, $AwardID, $thisCriteriaID, $AwardInfoArr, $thisCriteriaInfoArr);
            } else if ($thisCriteriaType == 'AWARD_LINKAGE') {
                $x .= $this->Get_Generate_Award_Criteria_AwardLinkage_Settings_Display($ReportID, $AwardID, $thisCriteriaID, $AwardInfoArr, $thisCriteriaInfoArr);
            }

            $CriteriaCount ++;
        }

        if ($CriteriaCount == 0) {
            $awardCode = $AwardInfoArr['BasicInfo']['AwardCode'];
            if ($awardCode == 'first_class_honour' || $awardCode == 'second_class_honour_1' || $awardCode == 'second_class_honour_2') {
                $x .= '<span class="tabletextremark"><i>' . str_replace('<!--reportName-->', $eReportCard['Reports_ClassHonourReport'], $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GeneratedFromReport']) . '</i></span>';
            } else if ($awardCode == 'conduct_award') {
                $x .= '<span class="tabletextremark"><i>' . str_replace('<!--reportName-->', $eReportCard['Reports_ConductAwardReport'], $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GeneratedFromReport']) . '</i></span>';
            } else if ($awardCode == 'conduct_progress_award') {
                $x .= '<span class="tabletextremark"><i>' . str_replace('<!--reportName-->', $eReportCard['Reports_ConductProgressReport'], $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GeneratedFromReport']) . '</i></span>';
            } else if ($awardCode == 'pta_progress' || $awardCode == 'academic_progress') {
                $x .= '<span class="tabletextremark"><i>' . str_replace('<!--reportName-->', $eReportCard['Management_AcademicProgress'], $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GeneratedFromReport']) . '</i></span>';
            } else {
                $x .= '<span class="tabletextremark"><i>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ManualInput'] . '</i></span>';
            }
        }

        return $x;
    }

    private function Get_Generate_Award_Criteria_Rank_Settings_Display($ReportID, $AwardID, $CriteriaID, $CriteriaArr)
    {
        global $Lang, $lreportcard_award, $linterface;

        $ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);

        $RankField = $CriteriaArr['RankField'];
        $FromRank = $CriteriaArr['FromRank'];
        $ToRank = $CriteriaArr['ToRank'];

        // ## Rank Field Selection
        $IncludeSubjectGroupOption = ($ReportType == 'T') ? true : false;
        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][RankField]';
        $RankFieldSelection = $this->Get_Ranking_Field_Selection($ID_Name, $RankField, $IncludeSubjectGroupOption);

        // ## From Rank Selection
        $ID = 'FromRankSel_' . $AwardID . '_' . $CriteriaID;
        $Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][FromRank]';
        $Class = 'FromRank';
        $OnChange = "js_Changed_FromRank_Selection('" . $AwardID . "', '" . $CriteriaID . "');";
        $FromRankSelection = $this->Get_Number_Selection($ID, $MinValue = 1, $MaxValue = 99, $FromRank, $OnChange, $noFirst = 1, $isAll = 0, $FirstTitle = '', $Name, $Class);

        // ## To Rank Selection
        $ID = 'ToRankSel_' . $AwardID . '_' . $CriteriaID;
        $Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][ToRank]';
        $Class = 'ToRank';
        $OnChange = "js_Changed_ToRank_Selection('" . $AwardID . "', '" . $CriteriaID . "');";
        $ToRankSelection = $this->Get_Number_Selection($ID, $MinValue = 1, $MaxValue = 99, $ToRank, $OnChange, $noFirst = 1, $isAll = 0, $FirstTitle = '', $Name, $Class);

        // ## Build Display
        $Display = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankRangeDisplay'];
        $Display = str_replace('<!--RankFieldSelection-->', $RankFieldSelection, $Display);
        $Display = str_replace('<!--FromRankSelection-->', $FromRankSelection, $Display);
        $Display = str_replace('<!--ToRankSelection-->', $ToRankSelection, $Display);

        // ## Warning Message
        $WarningMsg = $linterface->Get_Form_Warning_Msg($DivID = 'RankWarning_' . $AwardID . '_' . $CriteriaID, $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['FromRankCannotLargerThenToRank'], $Class = 'WarningMsg');

        $x = '';
        $x .= '<div>' . $Display . $WarningMsg . '</div>' . "\n";

        return $x;
    }

    private function Get_Generate_Award_Criteria_PersonalChar_Settings_Display($ReportID, $AwardID, $CriteriaID, $CriteriaArr)
    {
        global $Lang, $intranet_session_language;

        $MinPersonalCharCode = $CriteriaArr['MinPersonalCharCode'];
        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][MinPersonalCharCode]';
        $ScaleSelection = $this->Get_Personal_Characteristics_Scale_Selection($ID_Name, $MinPersonalCharCode);

        $Display = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['PersonalCharDisplay'];
        $Display = str_replace('<!--PersonalCharCodeSelection-->', $ScaleSelection, $Display);

        $x = '';
        $x .= '<div>' . $Display . '</div>' . "\n";

        return $x;
    }

    private function Get_Generate_Award_Criteria_Improvement_Settings_Display($ReportID, $AwardID, $CriteriaID, $AwardInfoArr, $CriteriaArr)
    {
        global $Lang, $linterface, $lreportcard_award, $eRCTemplateSetting;

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $Semester = $ReportInfoArr['Semester'];
        $ReportType = ($Semester == 'F') ? 'W' : 'T';

        $GenerateAwardReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);
        $AwardType = $AwardInfoArr['BasicInfo']['AwardType'];

        // ## From and To Report
        if ($ReportType == 'T') {
            // term report
            // set the report selection to the first term report and this report by default
            $excludeSemesterArr = array(
                'F'
            );
            $TermReportInfoArr = $this->returnReportTemplateBasicInfo('', $others = '', $ClassLevelID, '', 1, $excludeSemesterArr);

            $FromReportID = $TermReportInfoArr['ReportID'];
            $ToReportID = $ReportID;

            if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_UseLastTermReportAsConsolidatedReport'] && $GenerateAwardReportType == 'W') {
                $FromReportColumnID = 0;
                $ToReportColumnID = 0;
            } else {
                // First Report Column for 1st Report and Last Report Column for the 2nd Report
                $FromReportColumnInfoArr = $this->returnReportTemplateColumnData($FromReportID);
                $FromReportColumnID = $FromReportColumnInfoArr[0]['ReportColumnID'];
                $ToReportColumnInfoArr = $this->returnReportTemplateColumnData($ToReportID);
                $ToReportColumnID = $ToReportColumnInfoArr[count($ToReportColumnInfoArr) - 1]['ReportColumnID'];
            }
        } else {
            // consolidated report
            // set the report selection to the first term and the last term included in the consolidated report by default
            $TermReportInfoArr = $this->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly = 1);
            $numOfTermReport = count($TermReportInfoArr);

            $FromReportID = $TermReportInfoArr[0]['ReportID'];
            $ToReportID = $TermReportInfoArr[$numOfTermReport - 1]['ReportID'];

            $FromReportColumnID = 0;
            $ToReportColumnID = 0;
        }
        // From Report and Report Column Selection
        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][FromReportID]';
        $OnChange = "js_Changed_Report_Selection(this.value, '" . $AwardID . "', '" . $CriteriaID . "', 'From');";
        $FromReportSelection = $this->Get_Report_Selection($ClassLevelID, $FromReportID, $ID_Name, $OnChange, $ForVerification = 0, $ForSubmission = 0, $HideNonGenerated = 1, $OtherTags = '');

        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][FromReportColumnID]';
        $FromReportColumnSelection = $this->Get_ReportColumn_Selection($FromReportID, $ID_Name, $FromReportColumnID);

        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][FromMustBePass]';
        $Checked = ($CriteriaArr['FromMustBePass'] == 1) ? 'checked' : '';
        $FromMustBePassChk = $linterface->Get_Checkbox($ID_Name, $ID_Name, $Value = 1, $Checked, $Class = '', $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['MustBePass'], $Onclick = '', $Disabled = '');

        // To Report and Report Column Selection
        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][ToReportID]';
        $OnChange = "js_Changed_Report_Selection(this.value, '" . $AwardID . "', '" . $CriteriaID . "', 'To');";
        $ToReportSelection = $this->Get_Report_Selection($ClassLevelID, $ToReportID, $ID_Name, $OnChange, $ForVerification = 0, $ForSubmission = 0, $HideNonGenerated = 1, $OtherTags = '');

        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][ToReportColumnID]';
        $ToReportColumnSelection = $this->Get_ReportColumn_Selection($ToReportID, $ID_Name, $ToReportColumnID);

        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][ToMustBePass]';
        $Checked = ($CriteriaArr['ToMustBePass'] == 1) ? 'checked' : '';
        $ToMustBePassChk = $linterface->Get_Checkbox($ID_Name, $ID_Name, $Value = 1, $Checked, $Class = '', $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['MustBePass'], $Onclick = '', $Disabled = '');

        // ## Improvement Field Selection
        $ImprovementField = $CriteriaArr['ImprovementField'];
        $ID = 'ImprovementFieldSel_' . $AwardID . '_' . $CriteriaID;
        $Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][ImprovementField]';
        $OnChange = "js_Changed_Improvement_Field_Selection('" . $AwardID . "', '" . $CriteriaID . "', this.value);";
        $ImprovementFieldSelection = $this->Get_Improvement_Determination_Field_Selection($AwardType, $ID, $Name, $ImprovementField, $OnChange);

        // ## Minimun Improvement Textbox
        $MinImprovement = $CriteriaArr['MinImprovement'];
        $ID = 'MinImprovementTb_' . $AwardID . '_' . $CriteriaID;
        $Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][MinImprovement]';
        $Class = 'MinImprovementTb';
        $MinImprovementTb = $linterface->GET_TEXTBOX_NUMBER($ID, $Name, $MinImprovement, $Class);

        // ## Quota
        $Quota = $AwardInfoArr['ApplicableFormInfo'][$ClassLevelID]['Quota'];
        $Quota = ($Quota == '') ? 0 : $Quota;
        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][Quota]';
        $QuotaSelection = $this->Get_Number_Selection($ID_Name, $MinValue = 0, $MaxValue = 99, $Quota, $Onchange = '', $noFirst = 1, $isAll = 0, $FirstTitle = '');
        $QuotaRemarks = '<span class="tabletextremark">(' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['QuotaRemarks'] . ')</span>';

        $Padding = 'padding-top:3px;padding-bottom:3px;';
        $x = '';
        $x .= '<div>' . "\n";
        $x .= '<table style="width:100%;border:0px;padding:0px;">' . "\n";
        // From Report
        $SpanID = "FromReportColumnSelSpan_" . $AwardID . '_' . $CriteriaID;
        $x .= '<tr>' . "\n";
        $x .= '<td style="width:18%;border:0px;' . $Padding . '">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['FromReport'] . '</td>' . "\n";
        $x .= '<td style="width:1%;border:0px;' . $Padding . '"> : </td>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '">' . "\n";
        $x .= $FromReportSelection . "\n";
        $x .= '<span id="' . $SpanID . '">' . $FromReportColumnSelection . '</span>' . "\n";
        $x .= '<br />' . "\n";
        $x .= $FromMustBePassChk . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // To Report
        $SpanID = "ToReportColumnSelSpan_" . $AwardID . '_' . $CriteriaID;
        $x .= '<tr>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ToReport'] . '</td>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '"> : </td>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '">' . "\n";
        $x .= $ToReportSelection . "\n";
        $x .= '<span id="' . $SpanID . '">' . $ToReportColumnSelection . '</span>' . "\n";
        $x .= '<br />' . "\n";
        $x .= $ToMustBePassChk . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Improvement Field
        $x .= '<tr>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DetermineBy'] . '</td>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '"> : </td>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '">' . $ImprovementFieldSelection . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Min Improvement
        $SpanID = 'ImprovementUnitSpan_' . $AwardID . '_' . $CriteriaID;
        $x .= '<tr>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['MinimumImprovement'] . '</td>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '"> : </td>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '">' . "\n";
        $x .= $MinImprovementTb . ' <span id="' . $SpanID . '" class="ImprovementUnitSpan"></span>' . "\n";
        $x .= $linterface->Get_Form_Warning_Msg($DivID = 'MinImprovement_Mark_' . $AwardID . '_' . $CriteriaID, $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['MinImprovement_Mark'], $Class = 'WarningMsg');
        $x .= $linterface->Get_Form_Warning_Msg($DivID = 'MinImprovement_Rank_' . $AwardID . '_' . $CriteriaID, $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['MinImprovement_Rank'], $Class = 'WarningMsg');
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Quota
        $x .= '<tr>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Quota'] . '</td>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '"> : </td>' . "\n";
        $x .= '<td style="border:0px;' . $Padding . '">' . $QuotaSelection . ' ' . $QuotaRemarks . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</div>' . "\n";

        return $x;
    }

    private function Get_Generate_Award_Criteria_AwardLinkage_Settings_Display($ReportID, $AwardID, $CriteriaID, $AwardInfoArr, $CriteriaArr)
    {
        global $Lang, $lreportcard_award, $linterface;

        $AwardType = $AwardInfoArr['BasicInfo']['AwardType'];
        $LinkedAwardID = $CriteriaArr['LinkedAwardID'];
        $FromRank = $CriteriaArr['FromRank'];
        $ToRank = $CriteriaArr['ToRank'];
        $ExcludeOriginalAward = $CriteriaArr['ExcludeOriginalAward'];

        // ## Award Selection
        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][LinkedAwardID]';
        $AwardSelection = $this->Get_Award_Selection($ReportID, $ID_Name, $LinkedAwardID, $ShowHaveRankingAwardOnly = 1, $AwardID, $AwardType);

        // ## From Rank Selection
        $ID = 'FromRankSel_' . $AwardID . '_' . $CriteriaID;
        $Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][FromRank]';
        $Class = 'FromRank';
        $OnChange = "js_Changed_FromRank_Selection('" . $AwardID . "', '" . $CriteriaID . "');";
        $FromRankSelection = $this->Get_Number_Selection($ID, $MinValue = 1, $MaxValue = 99, $FromRank, $OnChange, $noFirst = 1, $isAll = 0, $FirstTitle = '', $Name, $Class);

        // ## To Rank Selection
        $ID = 'ToRankSel_' . $AwardID . '_' . $CriteriaID;
        $Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][ToRank]';
        $Class = 'ToRank';
        $OnChange = "js_Changed_ToRank_Selection('" . $AwardID . "', '" . $CriteriaID . "');";
        $ToRankSelection = $this->Get_Number_Selection($ID, $MinValue = 1, $MaxValue = 99, $ToRank, $OnChange, $noFirst = 1, $isAll = 0, $FirstTitle = '', $Name, $Class);

        // ## Build Display
        $Display = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['LinkedAwardSettingsDisplay'];
        $Display = str_replace('<!--AwardSelection-->', $AwardSelection, $Display);
        $Display = str_replace('<!--FromRankSelection-->', $FromRankSelection, $Display);
        $Display = str_replace('<!--ToRankSelection-->', $ToRankSelection, $Display);

        // ## Exclude original award Checkbox
        $ID_Name = 'AwardPageSettingsArr[' . $AwardID . '][' . $CriteriaID . '][ExcludeOriginalAward]';
        $Checked = ($ExcludeOriginalAward == 1) ? 'checked' : '';
        $ExcludeOriginalAwardChk = $linterface->Get_Checkbox($ID_Name, $ID_Name, $Value = 1, $Checked, $Class = '', $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ExcludeStudentFromOriginalAward'], $Onclick = '', $Disabled = '');

        // ## Warning Message
        $WarningMsg = $linterface->Get_Form_Warning_Msg($DivID = 'RankWarning_' . $AwardID . '_' . $CriteriaID, $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['FromRankCannotLargerThenToRank'], $Class = 'WarningMsg');

        $x = '';
        $x .= '<div>' . "\n";
        $x .= $Display . $WarningMsg;
        $x .= '<br />' . "\n";
        $x .= $ExcludeOriginalAwardChk;
        $x .= '</div>' . "\n";

        return $x;
    }

    public function Get_Import_Awards_Table()
    {
        global $PATH_WRT_ROOT, $Lang, $linterface, $lreportcard_award;

        // Get Awards
        $AwardInfoAssoArr = $lreportcard_award->Load_All_School_Award_Info();
        $numOfAward = count((array) $AwardInfoAssoArr);

        $x = '';
        $x .= '<table id="DataTable" class="common_table_list_v30">' . "\n";

        // Column Group
        $x .= '<col align="left" style="width:3%;">' . "\n";
        $x .= '<col align="left" style="width:8%;">' . "\n";
        $x .= '<col align="left" style="width:48%;">' . "\n";
        $x .= '<col align="left" style="width:35%;">' . "\n";
        $x .= '<col align="left" style="width:3%;">' . "\n";
        $x .= '<col align="left" style="width:3%;">' . "\n";

        // Table Header
        $x .= '<thead>';
        $x .= '<tr>';
        $x .= '<th>#</th>';
        $x .= '<th>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardCode'] . '</th>';
        $x .= '<th>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardName'] . '</th>';
        $x .= '<th>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardForm'] . '</th>';
        $x .= '<th>&nbsp;</th>';
        $x .= '<th>' . "<input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'awardIDAry[]'):setChecked(0,this.form,'awardIDAry[]')>" . '</th>';
        $x .= '</tr>';
        $x .= '</thead>';

        // Table Content
        $x .= '<tbody>';
        if ($numOfAward == 0) {
            $x .= '<tr><td colspan="6" style="text-align:center;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardSettings'] . '</td></tr>' . "\n";
        } else {
            // loop awards
            $AwardCount = 0;
            foreach ((array) $AwardInfoAssoArr as $thisAwardID => $thisAwardInfoArr) {
                // Get Award Info
                $thisAwardCode = $thisAwardInfoArr['BasicInfo']['AwardCode'];
                $thisAwardName = Get_Lang_Selection($thisAwardInfoArr['BasicInfo']['AwardNameCh'], $thisAwardInfoArr['BasicInfo']['AwardNameEn']);
                $thisAwardCanDelete = $thisAwardInfoArr['BasicInfo']['NotGenerateAward'] == "1";

                // Get Award related Form
                $thisAwardFormList = array();
                $thisAwardForm = $thisAwardInfoArr['ApplicableFormInfo'];

                // loop related forms
                foreach ((array) $thisAwardForm as $yearid => $thisForm)
                    $thisAwardFormList[] = $thisForm["YearName"];

                    $thisAwardFormList = array_filter((array) $thisAwardFormList);
                    $thisAwardFormDisplay = implode(", ", (array) $thisAwardFormList);

                    // Award Row
                    $x .= '<tr id="tr_' . $thisAwardID . '">' . "\n";
                    $x .= '<td>' . (++ $AwardCount) . '</td>' . "\n";
                    $x .= '<td>' . $thisAwardCode . '</td>' . "\n";
                    $x .= '<td>' . $thisAwardName . '</td>' . "\n";
                    $x .= '<td>' . $thisAwardFormDisplay . '</td>' . "\n";
                    $x .= '<td class="Dragable">' . "\n";
                    $x .= $linterface->GET_LNK_MOVE("#", $Lang['Btn']['Move']) . "\n";
                    $x .= '</td>' . "\n";
                    $x .= '<td>' . "\n";
                    $x .= ($thisAwardCanDelete ? '<input type="checkbox" class="ScaleChk" name="awardIDAry[]" value="' . $thisAwardID . '">' : '') . "\n";
                    $x .= '</td>' . "\n";
                    $x .= '</tr>' . "\n";
            }
        }
        $x .= '</tbody>';
        $x .= '</table>';

        return $x;
    }

    public function Get_Award_Selection($ReportID, $ID_Name, $SelectedValue, $ShowHaveRankingAwardOnly = 0, $ExcludeAwardIDArr = '', $TargetAwardType = '', $OnChange = '', $WithAwardType = 1, $isMultiple = 0)
    {
        global $lreportcard_award, $PATH_WRT_ROOT;

        if ($lreportcard_award == null) {
            include_once ($PATH_WRT_ROOT . 'includes/libreportcard2008_award.php');
            $lreportcard_award = new libreportcard_award();
        }

        $AwardInfoArr = $lreportcard_award->Get_Report_Award_Info($ReportID);
        $SelectArr = array();
        foreach ((array) $AwardInfoArr as $thisAwardID => $thisAwardInfoArr) {
            $thisAwardValid = true;

            if ($ExcludeAwardIDArr != '' && in_array($thisAwardID, (array) $ExcludeAwardIDArr)) {
                $thisAwardValid = false;
            }

            if ($ShowHaveRankingAwardOnly) {
                foreach ((array) $thisAwardInfoArr['CriteriaInfo'] as $thisCriteriaID => $thisCriteriaInfoArr) {
                    if ($thisCriteriaInfoArr['CriteriaType'] == 'PERSONAL_CHAR') {
                        // Awards which has Personal Char as Criteria has no Ranking and therefore exclude from the Award Selection for some cases
                        $thisAwardValid = false;
                        break;
                    }
                }
            }

            $thisAwardType = $AwardInfoArr[$thisAwardID]['BasicInfo']['AwardType'];
            if ($TargetAwardType != '' && $thisAwardType != $TargetAwardType) {
                $thisAwardValid = false;
            }

            if ($thisAwardValid) {
                $SelectArr[$thisAwardID] = $lreportcard_award->Get_Award_Name_Display($thisAwardID, $WithAwardType);
            }
        }

        if ($OnChange != '') {
            $onchange = ' onchange="' . $OnChange . '" ';
        }

        if ($isMultiple) {
            $multipleTag = ' multiple size="10" ';
        }

        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange . ' ' . $multipleTag;
        return getSelectByAssoArray($SelectArr, $selectionTags, $SelectedValue, $all = 0, $noFirst = 1);
    }

    private function Get_AwardText_Selection($ReportID, $ID_Name, $SelectedValue, $OnChange = '')
    {
        global $lreportcard_award;

        $AwardNameInfoArr = $lreportcard_award->Get_Report_Award_Full_List($ReportID);
        $numOfAwardName = count($AwardNameInfoArr);

        $SelectArr = array();
        for ($i = 0; $i < $numOfAwardName; $i ++) {
            $thisAwardNameKey = urlencode(intranet_htmlspecialchars($AwardNameInfoArr[$i]['AwardName']));
            $thisAwardNameDisplay = $AwardNameInfoArr[$i]['AwardNameDisplay'];

            $SelectArr[$thisAwardNameKey] = $thisAwardNameDisplay;
        }

        if ($OnChange != '') {
            $onchange = ' onchange="' . $OnChange . '" ';
        }

        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange;
        $SelectedValue = urlencode(intranet_htmlspecialchars($SelectedValue));
        return getSelectByAssoArray($SelectArr, $selectionTags, $SelectedValue, $all = 0, $noFirst = 1);
    }

    private function Get_Improvement_Determination_Field_Selection($AwardType, $ID, $Name, $SelectedValue, $Onchange = '')
    {
        global $Lang, $eRCTemplateSetting;

        $FieldArr = array();

        if ($AwardType == 'OVERALL') {
            $FieldArr['GrandAverage'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandAverage'];
            $FieldArr['GrandTotal'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandTotal'];
            $FieldArr['GPA'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandGPA'];

            if ($eRCTemplateSetting['OrderPositionMethod'] == "WeightedSD") {
                $FieldArr['GrandSDScore'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandSDScore'];
            }
        }

        if ($AwardType == 'SUBJECT') {
            $FieldArr['Mark'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['Mark'];

            if ($eRCTemplateSetting['OrderPositionMethod'] == "WeightedSD") {
                $FieldArr['SDScore'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['SDScore'];
            }
        }

        $FieldArr['OrderMeritClass'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['OrderMeritClass'];
        $FieldArr['OrderMeritForm'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['OrderMeritForm'];

        if ($AwardType == 'SUBJECT') {
            $FieldArr['OrderMeritSubjectGroup'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['OrderMeritSubjectGroup'];
        }

        if ($Onchange != '') {
            $onchange = ' onchange="' . $Onchange . '" ';
        }
        $selectionTags = ' id="' . $ID . '" name="' . $Name . '" ' . $onchange;
        $selection = getSelectByAssoArray($FieldArr, $selectionTags, $SelectedValue, $all = 0, $noFirst = 1);

        return $selection;
    }

    private function Get_Personal_Characteristics_Scale_Selection($ID_Name, $SelectedValue, $Name = '', $Disabled = false, $ApplicableScaleOnly = false, $noFirst = 1, $OtherParArr = '')
    {
        global $lreportcard; // use $lreportcard instead of $this as $lreportcard is the libreportcardcustom object so as to get the client customized Personal Char Scale
        global $PATH_WRT_ROOT;

        $ScaleInfoArr = $this->returnPersonalCharOptions();

        if ($ApplicableScaleOnly) {
            include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_pc.php");
            $lreportcard_pc = new libreportcard_pc();

            $ApplicableScaleInfoArr = $lreportcard_pc->Get_User_Applicable_Personal_Characteristics_Scale($_SESSION['UserID']);
            $ApplicableScaleIDArr = Get_Array_By_Key($ApplicableScaleInfoArr, 'ScaleID');
            foreach ((array) $ScaleInfoArr as $thisScaleID => $thisScaleText) {
                if (! in_array($thisScaleID, (array) $ApplicableScaleIDArr)) {
                    unset($ScaleInfoArr[$thisScaleID]);
                }
            }
        }

        $Name = ($Name == '') ? $ID_Name : $Name;
        $DisabledTag = ($Disabled) ? 'disabled' : '';

        $OtherPar = '';
        if (is_array($OtherParArr) && count($OtherParArr) > 0) {
            foreach ($OtherParArr as $parKey => $parVal) {
                $OtherPar .= ' ' . $parKey . '="' . $parVal . '" ';
            }
        }

        $selectionTags = ' id="' . $ID_Name . '" name="' . $Name . '" ' . $DisabledTag . ' ' . $OtherPar;
        $selection = getSelectByAssoArray($ScaleInfoArr, $selectionTags, $SelectedValue, $all = 0, $noFirst);

        return $selection;
    }

    private function Get_Ranking_Field_Selection($ID_Name, $SelectedValue, $IncludeSubjectGroupOption = 0)
    {
        global $Lang;

        $TypeArr = array();
        $TypeArr['OrderMeritClass'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankTypeArr']['Class'];
        $TypeArr['OrderMeritForm'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankTypeArr']['Form'];
        if ($IncludeSubjectGroupOption) {
            $TypeArr['OrderMeritSubjectGroup'] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankTypeArr']['SubjectGroup'];
        }

        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ';
        $selection = getSelectByAssoArray($TypeArr, $selectionTags, $SelectedValue, $all = 0, $noFirst = 1);

        return $selection;
    }

    public function Get_ReportColumn_Selection($ReportID, $ID_Name, $SelectedValue = '', $IncludeOverallOption = true, $IsAll = 0, $NoFirst = 1, $FirstTitle = '', $OverallOptionValue = 0)
    {
        global $Lang;

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $Semester = $ReportInfoArr['Semester'];
        $ReportType = ($Semester == 'F') ? 'W' : 'T';

        $ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
        $numOfColumn = count($ReportColumnInfoArr);

        $ColumnArr = array();
        for ($i = 0; $i < $numOfColumn; $i ++) {
            $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];

            if ($ReportType == 'T') {
                $thisReportColumnName = $ReportColumnInfoArr[$i]['ColumnTitle'];
            } else {
                $thisYearTermID = $ReportColumnInfoArr[$i]['SemesterNum'];
                $thisReportColumnName = $this->returnSemesters($thisYearTermID);
            }

            $ColumnArr[$thisReportColumnID] = $thisReportColumnName;
        }

        if ($IncludeOverallOption) {
            $ColumnArr[$OverallOptionValue] = $Lang['eReportCard']['OverallResult'];
        }

        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ';
        $selection = getSelectByAssoArray($ColumnArr, $selectionTags, $SelectedValue, $IsAll, $NoFirst, $FirstTitle);

        return $selection;
    }

    public function Get_Report_Teacher_Selection($parReportIdAry, $parId, $parName, $parOnChange = '')
    {
        $teacherAssoAry = BuildMultiKeyAssoc($this->Get_Report_Related_Teacher($parReportIdAry), 'UserID', Get_Lang_Selection('ChineseName', 'EnglishName'), $SingleValue = 1);

        $selectionAttr = ' id="' . $parId . '" name="' . $parName . '" onchange="' . $parOnChange . '"';
        return getSelectByAssoArray($teacherAssoAry, $selectionAttr, '', $IsAll = false, $NoFirst = true);
    }

    public function Get_View_Award_Class_Mode_UI($ReportID)
    {
        global $Lang, $eReportCard;
        global $linterface, $lreportcard_award;

        // ## Get Report Card Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);
        $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID, 1);
        $FormName = $this->returnClassLevel($ClassLevelID);

        // ## Toolbar
        $ImportBtn = $linterface->Get_Content_Tool_v30('import', $href = "javascript:js_Import();", $text = "", '', $other = "", $divID = '');
        $ExportBtn = $linterface->Get_Content_Tool_v30('export', $href = "javascript:js_Export();", $text = "", '', $other = "", $divID = '');

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Reports_GenerateReport'],
            "javascript:js_Go_Back_To_Report_Generation();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ViewAward'],
            ""
        );

        // ## View Mode
        $ViewModeBtn = $this->Get_View_Award_View_Mode_Button($ReportID, 'Class');

        // ## Buttons
        $BackBtn = $linterface->GET_ACTION_BTN($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['BackToReportGeneration'], "button", "js_Go_Back_To_Report_Generation();");

        $x = '';
        $x .= '<form id="form1" name="form1" method="post">' . "\n";
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $ImportBtn . "\n";
        $x .= $ExportBtn . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= $ViewModeBtn . "\n";
        $x .= '</div>' . "\n";
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportTitle'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTitle . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportType'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTypeDisplay . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
        $x .= '<td>' . $FormName . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<br />' . "\n";

        $x .= $this->Get_View_Award_Class_Mode_Table($ReportID);
        $x .= '<br />' . "\n";
        $x .= '</div>' . "\n";

        // Buttons
        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= $BackBtn;
        $x .= '</div>' . "\n";

        // Hidden Field
        $x .= '<input type="hidden" id="ReportID" name="ReportID" value="' . $ReportID . '">' . "\n";

        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        return $x;
    }

    private function Get_View_Award_Class_Mode_Table($ReportID)
    {
        global $Lang, $lreportcard_award;

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];

        $ClassInfoArr = $this->GET_CLASSES_BY_FORM($ClassLevelID);
        $numOfClass = count($ClassInfoArr);

        $LastModifiedInfoArr = $lreportcard_award->Get_Report_Award_Class_Last_Modified_Info($ReportID);

        $x = '';
        $x .= '<table class="common_table_list_v30">' . "\n";
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th style="width:50%;">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</th>' . "\n";
        $x .= '<th style="width:25%;">' . $Lang['General']['LastModified'] . '</th>' . "\n";
        $x .= '<th style="width:25%;">' . $Lang['General']['LastUpdatedBy'] . '</th>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";
        $x .= '<tbody>' . "\n";
        if ($numOfClass == 0) {
            $x .= '<tr><td>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoClassSettings'] . '</td></tr>' . "\n";
        } else {
            for ($i = 0; $i < $numOfClass; $i ++) {
                $thisClassID = $ClassInfoArr[$i]['ClassID'];
                $thisClassName = Get_Lang_Selection($ClassInfoArr[$i]['ClassTitleB5'], $ClassInfoArr[$i]['ClassTitleEN']);
                $thisClassLink = '<a class="tablelink" href="javascript:js_View_Class_Student_Award(\'' . $thisClassID . '\');">' . $thisClassName . '</a>';
                $thisLastModifiedDate = Get_Standardized_Table_Data_Display($LastModifiedInfoArr[$thisClassID]['LastModifiedDate']);
                $thisLastModifiedUserName = Get_Standardized_Table_Data_Display($LastModifiedInfoArr[$thisClassID]['LastModifiedUserName']);

                $x .= '<tr>' . "\n";
                $x .= '<td>' . $thisClassLink . '</td>' . "\n";
                $x .= '<td>' . $thisLastModifiedDate . '</td>' . "\n";
                $x .= '<td>' . $thisLastModifiedUserName . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        return $x;
    }

    function Get_Edit_Award_Student_UI($ReportID, $ViewMode, $YearClassID, $AwardName, $AwardID, $SubjectID)
    {
        global $Lang, $eReportCard, $eRCTemplateSetting;
        global $linterface, $lreportcard_award;

        // ## Get Report Card Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);
        $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID, 1);
        $FormName = $this->returnClassLevel($ClassLevelID);

        $SelectionOnChange = 'js_Reload_Student_Table();';
        if ($ViewMode == 'Class') {
            $LastNavigationDisplay = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ClassView'];

            $ImportBtn = $linterface->Get_Content_Tool_v30('import', $href = "javascript:js_Import();", $text = "", array(), $other = "", $divID = '');

            $SelectionTitle = $Lang['SysMgr']['FormClassMapping']['Class'];
            $Selection = $this->Get_Class_Selection('YearClassID', $ClassLevelID, $YearClassID, $SelectionOnChange, $noFirst = 1, $isAll = 0, $checkClassTeacher = 0, $IsMultiple = 0);
        } else if ($ViewMode == 'Award') {
            $LastNavigationDisplay = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardView'];

            $ImportBtn = '';
            $SelectionTitle = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'];

            if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
                $AwardInfoArr = $lreportcard_award->Get_Award_Info($AwardID);
                $AwardType = $AwardInfoArr['AwardType'];
                $AwardName = Get_Lang_Selection($AwardInfoArr['AwardNameCh'], $AwardInfoArr['AwardNameEn']);

                if ($AwardType == 'OVERALL') {
                    $Selection = $AwardName;
                } else if ($AwardType == 'SUBJECT') {
                    $SubjectName = $this->GET_SUBJECT_NAME_LANG($SubjectID);
                    $Selection = $lreportcard_award->Get_Subject_Prize_Display($AwardName, $SubjectName);
                }
            } else {
                $Selection = $this->Get_AwardText_Selection($ReportID, 'AwardName', $AwardName, $SelectionOnChange);
            }
        }

        // ## Toolbar
        $ExportBtn = $linterface->Get_Content_Tool_v30('export', $href = "javascript:js_Export();", $text = "", $ExportSubBtn = array(), $other = "", $divID = '');

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Reports_GenerateReport'],
            "javascript:js_Go_Back_To_Report_Generation();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ViewAward'],
            "javascript:js_Go_Back();"
        );
        $PAGE_NAVIGATION[] = array(
            $LastNavigationDisplay,
            ""
        );

        // ## Buttons
        $SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Save_Student_Award();");
        $BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Back();");
        $BackToGenerateBtn = $linterface->GET_ACTION_BTN($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['BackToReportGeneration'], "button", "js_Go_Back_To_Report_Generation();");

        $x = '';
        $x .= '<form id="form1" name="form1" method="post">' . "\n";
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $ImportBtn . $ExportBtn . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportTitle'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTitle . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportType'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTypeDisplay . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
        $x .= '<td>' . $FormName . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $SelectionTitle . '</td>' . "\n";
        $x .= '<td>' . $Selection . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<br />' . "\n";

        $x .= '<div id="StudentTableDiv"></div>' . "\n";
        $x .= '<br />' . "\n";
        $x .= '</div>' . "\n";

        // Buttons
        $x .= '<div class="edit_bottom_v30">' . "\n";
        if (! $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
            $x .= $SaveBtn;
            $x .= '&nbsp;' . "\n";
        }
        $x .= $BackBtn;
        $x .= '</div>' . "\n";

        // Hidden Field
        $x .= '<input type="hidden" id="ReportID" name="ReportID" value="' . $ReportID . '">' . "\n";

        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        return $x;
    }

    public function Get_Edit_Award_Student_Table($ReportID, $ViewMode, $YearClassID, $AwardName)
    {
        global $Lang, $lreportcard_award, $linterface;

        // ## Get Form Student Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $StudentInfoAssoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $YearClassID, $ParStudentIDList = "", $ReturnAsso = 1, $isShowStyle = 1);

        // ## Get the Display Student
        $StudentIDArr = array();
        if ($ViewMode == 'Class') {
            $StudentIDArr = array_keys($StudentInfoAssoArr);
            $NoRecordWarning = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoStudentInClass'];
        } else if ($ViewMode == 'Award') {
            $StudentAwardInfoArr = $lreportcard_award->Get_Student_With_AwardText($ReportID, $AwardName);
            $StudentIDArr = Get_Array_By_Key($StudentAwardInfoArr, 'StudentID');

            $NoRecordWarning = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardedStudentSettings'];
        }
        $numOfStudent = count($StudentIDArr);

        // ## Get Display Student Award Info
        $StudentAwardInfoAssoArr = $lreportcard_award->Get_Award_Student_Record($ReportID, $StudentIDArr);

        $x = '';
        $x .= '<table class="common_table_list_v30 edit_table_list_v30">' . "\n";
        $x .= '<col style="width:5%;">' . "\n";
        $x .= '<col style="width:10%;">' . "\n";
        $x .= '<col style="width:10%;">' . "\n";
        $x .= '<col style="width:25%;">' . "\n";
        $x .= '<col style="width:50%;">' . "\n";

        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th>#</th>' . "\n";
        $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</th>' . "\n";
        $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</th>' . "\n";
        $x .= '<th>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['StudentName'] . '</th>' . "\n";
        $x .= '<th>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'] . '</th>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";
        $x .= '<tbody>' . "\n";
        if ($numOfStudent == 0) {
            $x .= '<tr><td colspan="5" style="text-align:center;">' . $NoRecordWarning . '</td></tr>' . "\n";
        } else {
            $StudentDisplayCount = 0;
            foreach ((array) $StudentInfoAssoArr as $thisStudentID => $thisStudentInfoArr) { // Loop $StudentInfoAssoArr to maintain Class and Class Number ordering
                if (! in_array($thisStudentID, $StudentIDArr)) {
                    continue;
                }

                $thisClassName = Get_Lang_Selection($thisStudentInfoArr['ClassTitleCh'], $thisStudentInfoArr['ClassTitleEn']);
                $thisClassNumber = $thisStudentInfoArr['ClassNumber'];
                $thisStudentName = $thisStudentInfoArr['StudentName'];

                $thisRecordID = $StudentAwardInfoAssoArr[$ReportID][$thisStudentID]['RecordID'];
                $thisRecordID = ($thisRecordID == '') ? 'new' : $thisRecordID;
                $thisAwardText = $StudentAwardInfoAssoArr[$ReportID][$thisStudentID]['AwardText'];

                $thisID = 'AwardTextArr[' . $thisStudentID . '][' . $thisRecordID . ']';
                $thisAwardTextArea = $linterface->GET_TEXTAREA($thisID, $thisAwardText, $taCols = 70, $taRows = 5, $OnFocus = "", $readonly = '', $other = 'width="100%"', $class = '');

                $x .= '<tr>' . "\n";
                $x .= '<td>' . (++ $StudentDisplayCount) . '</td>' . "\n";
                $x .= '<td>' . $thisClassName . '</td>' . "\n";
                $x .= '<td>' . $thisClassNumber . '</td>' . "\n";
                $x .= '<td>' . $thisStudentName . '</td>' . "\n";
                $x .= '<td>' . $thisAwardTextArea . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<input type="hidden" id="AwardedStudentIDList" name="AwardedStudentIDList" value="' . implode(',', (array) $StudentIDArr) . '">' . "\n";

        return $x;
    }

    function Get_View_Award_Award_Mode_UI($ReportID)
    {
        global $Lang, $eReportCard, $eRCTemplateSetting;
        global $linterface, $lreportcard_award;

        // ## Get Report Card Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);
        $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID, 1);
        $FormName = $this->returnClassLevel($ClassLevelID);

        // ## Toolbar
        $ExportBtn = $linterface->Get_Content_Tool_v30('export', $href = "javascript:js_Export();", $text = "", $ExportSubBtn, $other = "", $divID = '');

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Reports_GenerateReport'],
            "javascript:js_Go_Back_To_Report_Generation();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ViewAward'],
            ""
        );

        // ## View Mode
        $ViewModeBtn = $this->Get_View_Award_View_Mode_Button($ReportID, 'Award');

        // ## Buttons
        $BackBtn = $linterface->GET_ACTION_BTN($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['BackToReportGeneration'], "button", "js_Go_Back_To_Report_Generation();");

        $x = '';
        $x .= '<form id="form1" name="form1" method="post">' . "\n";
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $ExportBtn . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= $ViewModeBtn . "\n";
        $x .= '</div>' . "\n";
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportTitle'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTitle . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportType'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTypeDisplay . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
        $x .= '<td>' . $FormName . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<br />' . "\n";

        if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
            $x .= $this->Get_View_Award_Award_Mode_Table_By_Generated_Result($ReportID);
        } else {
            $x .= $this->Get_View_Award_Award_Mode_Table($ReportID);
        }

        $x .= '<br />' . "\n";
        $x .= '</div>' . "\n";

        // Buttons
        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= $BackBtn;
        $x .= '</div>' . "\n";

        // Hidden Field
        $x .= '<input type="hidden" id="ReportID" name="ReportID" value="' . $ReportID . '">' . "\n";

        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        return $x;
    }

    private function Get_View_Award_Award_Mode_Table($ReportID)
    {
        global $Lang, $lreportcard_award;

        $AwardInfoArr = $lreportcard_award->Get_Report_Award_Full_List($ReportID);
        $numOfAward = count($AwardInfoArr);

        $x = '';
        $x .= '<table class="common_table_list_v30">' . "\n";
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th style="width:5%;">#</th>' . "\n";
        $x .= '<th style="width:50%;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'] . '</th>' . "\n";
        $x .= '<th style="width:45%;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['NoOfAwardedStudent'] . '</th>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";
        $x .= '<tbody>' . "\n";
        if ($numOfAward == 0) {
            $x .= '<tr><td colspan="100%" style="text-align:center;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardSettings'] . '</td></tr>' . "\n";
        } else {
            for ($i = 0; $i < $numOfAward; $i ++) {
                $thisAwardID = $AwardInfoArr[$i]['AwardID'];
                $thisAwardName = $AwardInfoArr[$i]['AwardName'];
                $thisAwardNameDisplay = $AwardInfoArr[$i]['AwardNameDisplay'];
                $thisAwardType = $AwardInfoArr[$i]['AwardType'];

                $thisAwardStudentInfoArr = $lreportcard_award->Get_Student_With_AwardText($ReportID, $thisAwardName);
                $thisNumOfAwardStudent = count($thisAwardStudentInfoArr);
                if ($thisNumOfAwardStudent > 0) {
                    $thisNumOfStudentDisplay = '<a class="tablelink" href="javascript:js_View_Award_Student(\'' . urlencode(intranet_htmlspecialchars($thisAwardName)) . '\');">' . $thisNumOfAwardStudent . '</a>';
                } else {
                    $thisNumOfStudentDisplay = $thisNumOfAwardStudent;
                }

                $x .= '<tr>' . "\n";
                $x .= '<td>' . ($i + 1) . '</td>' . "\n";
                $x .= '<td>' . $thisAwardNameDisplay . '</td>' . "\n";
                $x .= '<td>' . $thisNumOfStudentDisplay . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        return $x;
    }

    private function Get_View_Award_Award_Mode_Table_By_Generated_Result($ReportID)
    {
        global $Lang, $lreportcard_award, $intranet_session_language, $eRCTemplateSetting, $linterface;

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];

        // ## Get Subject List
        $TargetLang = ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_SubjectNameAlwaysInEnglish']) ? 'en' : $intranet_session_language;
        $SubjectInfoAssoArr = $SubjectInfoArr = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection = 0, $MainSubjectOnly = 1, $ReportID, $TargetLang);

        // ## Get Award List
        $AwardInfoAssoArr = $lreportcard_award->Get_Report_Award_Info($ReportID, $MapByCode = 0);
        $numOfAward = count($AwardInfoAssoArr);

        // ## Get Student Award Info
        $StudentAwardInfoArr = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr = '', $ReturnAsso = 0, $AwardNameWithSubject = 1);
        $StudentAwardAssoArr = BuildMultiKeyAssoc($StudentAwardInfoArr, array(
            'AwardID',
            'SubjectID',
            'StudentID'
        ), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 0);

        $x = '';
        $x .= '<table class="common_table_list_v30">' . "\n";
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th style="width:5%;">#</th>' . "\n";
        $x .= '<th style="width:50%;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'] . '</th>' . "\n";
        $x .= '<th style="width:45%;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['NoOfAwardedStudent'] . '</th>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";
        $x .= '<tbody>' . "\n";
        if ($numOfAward == 0) {
            $x .= '<tr><td colspan="100%" style="text-align:center;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardSettings'] . '</td></tr>' . "\n";
        } else {
            $DisplayedAwardCount = 0;
            foreach ((array) $AwardInfoAssoArr as $thisAwardID => $thisAwardInfoArr) {
                $thisAwardType = $thisAwardInfoArr['BasicInfo']['AwardType'];
                $thisAwardName = Get_Lang_Selection($thisAwardInfoArr['BasicInfo']['AwardNameCh'], $thisAwardInfoArr['BasicInfo']['AwardNameEn']);

                if ($thisAwardType == 'SUBJECT') {
                    foreach ((array) $SubjectInfoAssoArr as $thisSubjectID => $thisSubjectName) {
                        $thisAwardNameDisplay = $lreportcard_award->Get_Subject_Prize_Display($thisAwardName, $thisSubjectName);
                        $thisNumOfAwardStudent = count($StudentAwardAssoArr[$thisAwardID][$thisSubjectID]);
                        $thisNumOfStudentDisplay = '<a class="tablelink" href="javascript:js_View_Award_Student_By_Generated_Result(\'' . $thisAwardID . '\', \'' . $thisSubjectID . '\');">' . $thisNumOfAwardStudent . '</a>';

                        $x .= '<tr>' . "\n";
                        $x .= '<td>' . (++ $DisplayedAwardCount) . '</td>' . "\n";
                        $x .= '<td>' . $thisAwardNameDisplay . '</td>' . "\n";
                        $x .= '<td>' . $thisNumOfStudentDisplay . '</td>' . "\n";
                        $x .= '</tr>' . "\n";
                    }
                } else {
                    $thisSubjectID = 0;
                    $thisAwardNameDisplay = $thisAwardName;
                    $thisNumOfAwardStudent = count($StudentAwardAssoArr[$thisAwardID][$thisSubjectID]);
                    $thisNumOfStudentDisplay = '<a class="tablelink" href="javascript:js_View_Award_Student_By_Generated_Result(\'' . $thisAwardID . '\', \'' . $thisSubjectID . '\');">' . $thisNumOfAwardStudent . '</a>';

                    $x .= '<tr>' . "\n";
                    $x .= '<td>' . (++ $DisplayedAwardCount) . '</td>' . "\n";
                    $x .= '<td>' . $thisAwardNameDisplay . '</td>' . "\n";
                    $x .= '<td>' . $thisNumOfStudentDisplay . '</td>' . "\n";
                    $x .= '</tr>' . "\n";
                }
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        return $x;
    }

    public function Get_Print_Award_Table($ReportID)
    {
        global $Lang, $lreportcard_award, $intranet_session_language, $eRCTemplateSetting, $linterface;

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];

        // ## Get Subject List
        $TargetLang = ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_SubjectNameAlwaysInEnglish']) ? 'en' : $intranet_session_language;
        $SubjectInfoAssoArr = $SubjectInfoArr = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection = 0, $MainSubjectOnly = 1, $ReportID, $TargetLang);

        // ## Get Award List
        $AwardInfoAssoArr = $lreportcard_award->Get_Report_Award_Info($ReportID, $MapByCode = 0);
        $numOfAward = count($AwardInfoAssoArr);

        // ## Get Student Award Info
        $StudentAwardInfoArr = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr = '', $ReturnAsso = 0, $AwardNameWithSubject = 1);
        $StudentAwardAssoArr = BuildMultiKeyAssoc($StudentAwardInfoArr, array(
            'AwardID',
            'SubjectID',
            'StudentID'
        ), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 0);

        $GlobalAwardCheckboxTd = '';
        $dbTableActionBtn = '';
        if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_PrintCertificate']) {
            $GlobalAwardCheckboxTd = '<th><input type="checkbox" id="globalAwardChk" onclick="Check_All_Options_By_Class(\'awardChk\', this.checked);"></th>';

            // ## DB table action buttons
            // $btnAry[] = array($btnClass, $btnHref, $displayLang);
            $btnAry[] = array(
                'other',
                'javascript: goPrint();',
                $Lang['Btn']['Print']
            );
            $dbTableActionBtn = $linterface->Get_DBTable_Action_Button_IP25($btnAry);
        }

        $x = '';
        $x .= $dbTableActionBtn;
        $x .= '<table class="common_table_list_v30">' . "\n";
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th style="width:5%;">#</th>' . "\n";
        $x .= '<th style="width:50%;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'] . '</th>' . "\n";
        $x .= '<th style="width:45%;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['NoOfAwardedStudent'] . '</th>' . "\n";
        $x .= $GlobalAwardCheckboxTd . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";
        $x .= '<tbody>' . "\n";
        if ($numOfAward == 0) {
            $x .= '<tr><td colspan="100%" style="text-align:center;">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardSettings'] . '</td></tr>' . "\n";
        } else {
            $DisplayedAwardCount = 0;
            foreach ((array) $AwardInfoAssoArr as $thisAwardID => $thisAwardInfoArr) {
                $thisAwardType = $thisAwardInfoArr['BasicInfo']['AwardType'];
                $thisAwardName = Get_Lang_Selection($thisAwardInfoArr['BasicInfo']['AwardNameCh'], $thisAwardInfoArr['BasicInfo']['AwardNameEn']);

                if ($thisAwardType == 'SUBJECT') {
                    foreach ((array) $SubjectInfoAssoArr as $thisSubjectID => $thisSubjectName) {
                        $thisAwardNameDisplay = $lreportcard_award->Get_Subject_Prize_Display($thisAwardName, $thisSubjectName);
                        $thisNumOfAwardStudent = count($StudentAwardAssoArr[$thisAwardID][$thisSubjectID]);

                        $thisAwardCheckboxTd = '';
                        if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_PrintCertificate']) {
                            $thisAwardCheckboxTd = '<td><input type="checkbox" class="awardChk" id="awardChk_' . $thisAwardID . '_' . $thisSubjectID . '" name="awardAry[' . $thisAwardID . '][]" value="' . $thisSubjectID . '" onclick="Uncheck_SelectAll(\'globalAwardChk\', this.checked);"></td>';
                        }

                        $x .= '<tr>' . "\n";
                        $x .= '<td>' . (++ $DisplayedAwardCount) . '</td>' . "\n";
                        $x .= '<td>' . $thisAwardNameDisplay . '</td>' . "\n";
                        $x .= '<td>' . $thisNumOfAwardStudent . '</td>' . "\n";
                        $x .= $thisAwardCheckboxTd . "\n";
                        $x .= '</tr>' . "\n";
                    }
                } else {
                    $thisSubjectID = 0;
                    $thisAwardNameDisplay = $thisAwardName;
                    $thisNumOfAwardStudent = count($StudentAwardAssoArr[$thisAwardID][$thisSubjectID]);

                    $thisAwardCheckboxTd = '';
                    if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_PrintCertificate']) {
                        $thisAwardCheckboxTd = '<td><input type="checkbox" class="awardChk" id="awardChk_' . $thisAwardID . '_' . $thisSubjectID . '" name="awardAry[' . $thisAwardID . '][]" value="' . $thisSubjectID . '" onclick="Uncheck_SelectAll(\'globalAwardChk\', this.checked);"></td>';
                    }

                    $x .= '<tr>' . "\n";
                    $x .= '<td>' . (++ $DisplayedAwardCount) . '</td>' . "\n";
                    $x .= '<td>' . $thisAwardNameDisplay . '</td>' . "\n";
                    $x .= '<td>' . $thisNumOfAwardStudent . '</td>' . "\n";
                    $x .= $thisAwardCheckboxTd . "\n";
                    $x .= '</tr>' . "\n";
                }
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        return $x;
    }

    public function Get_Student_View_Selection($ReportID, $ID_Name, $SelectedValue, $OnChange = '')
    {
        global $Lang;

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = ($Semseter == 'F') ? 'W' : 'T';

        $SelectArr = array();
        $SelectArr['Class'] = $Lang['SysMgr']['FormClassMapping']['Class'];

        if ($ReportType == 'T') {
            $SelectArr['SubjectGroup'] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
        }

        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ';
        return getSelectByAssoArray($SelectArr, $selectionTags, $SelectedValue, $all = 0, $noFirst = 1);
    }

    private function Get_View_Award_View_Mode_Button($ReportID, $CurrentMode)
    {
        global $Lang, $linterface;

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = ($Semseter == 'F') ? 'W' : 'T';

        $pages_arr = array();

        // Award View
        $pages_arr[] = array(
            $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'],
            "javascript:js_Change_View_Mode('" . $ReportID . "', 'Award');",
            $img_src,
            $CurrentMode == 'Award'
        );

        // Subject Group View
        if ($ReportType == 'T') {
            // $pages_arr[] = array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'], "javascript:js_Change_View_Mode('".$ReportID."', 'SubjectGroup');", $img_src, $CurrentMode=='SubjectGroup');
        }

        // Class View
        $pages_arr[] = array(
            $Lang['SysMgr']['FormClassMapping']['Class'],
            "javascript:js_Change_View_Mode('" . $ReportID . "', 'Class');",
            $img_src,
            $CurrentMode == 'Class'
        );

        return $linterface->GET_CONTENT_TOP_BTN($pages_arr);
    }

    public function Get_Import_Student_AwardText_Step1_UI($ReportID, $YearClassID = '')
    {
        global $linterface, $lreportcard_award;
        global $eReportCard, $Lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Reports_GenerateReport'],
            "javascript:js_Go_Back_To_Report_Generation();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ViewAward'],
            "javascript:js_Change_View_Mode('" . $ReportID . "', 'Class');"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['Btn']['Import'],
            ""
        );

        // ## Import Remarks
        $RemarksTable = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eReportCard']['GeneralArr']['ImportStudentRemarks'], $others = "");

        // ## Get Report Card Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);
        $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID, 1);
        $FormName = $this->returnClassLevel($ClassLevelID);

        if ($YearClassID != '') {
            $ClassInfoArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, $YearClassID, $returnAssoName = 0, $returnAssoInfo = 1);
            $ClassName = Get_Lang_Selection($ClassInfoArr[$YearClassID]['ClassTitleB5'], $ClassInfoArr[$YearClassID]['ClassTitleEN']);
        }

        // ## Column Description
        $ColumnTitleArr = $lreportcard_award->Get_Report_Award_Csv_Header_Title();
        $ColumnTitleArr = Get_Lang_Selection($ColumnTitleArr['Ch'], $ColumnTitleArr['En']);
        $ColumnPropertyArr = $lreportcard_award->Get_Report_Award_Csv_Header_Property();
        $RemarksArr = array();
        $RemarksArr[6] = '<a id="remarkBtn_awardCode" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'awardCode\');">[' . $Lang['General']['Remark'] . ']</a>';
        $ColumnDisplay = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);

        // ## Buttons
        $ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();");
        $CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_View_Index();");

        $x = '';
        $x .= '<form id="form1" name="form1" method="post" action="import_award_step2.php" enctype="multipart/form-data">' . "\n";
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        $x .= '<br style="clear:both;" />' . "\n";
        $x .= $linterface->GET_IMPORT_STEPS($CurrStep = 1);
        $x .= '<br style="clear:both;" />' . "\n";
        $x .= '<div align="center" style="width:100%;">' . $RemarksTable . '</div>' . "\n";
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportTitle'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTitle . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportType'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTypeDisplay . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
        $x .= '<td>' . $FormName . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        if ($YearClassID != '') {
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
            $x .= '<td>' . $ClassName . '</td>' . "\n";
            $x .= '</tr>' . "\n";
        }
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['SourceFile'] . ' <span class="tabletextremark">' . $Lang['General']['CSVFileFormat'] . '</span></td>' . "\n";
        $x .= '<td class="tabletext">' . "\n";
        $x .= '<input class="file" type="file" name="csvfile" id="csvfile">' . "\n";
        $x .= '<br />' . "\n";
        $x .= '<span class="tabletextrequire">' . $Lang['General']['ImportArr']['SecondRowWarn'] . '</span>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['CSVSample'] . '</td>' . "\n";
        $x .= '<td>' . $linterface->Get_CSV_Sample_Download_Link('javascript:js_Go_Export();') . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . "\n";
        $x .= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn'] . "\n";
        $x .= '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $ColumnDisplay . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletextremark" colspan="2">' . "\n";
        $x .= $linterface->MandatoryField();
        $x .= $linterface->ReferenceField();
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</div>' . "\n";

        // Buttons
        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= $ContinueBtn;
        $x .= "&nbsp;";
        $x .= $CancelBtn;
        $x .= '</div>' . "\n";

        // Hidden Field
        $x .= '<input type="hidden" id="ReportID" name="ReportID" value="' . $ReportID . '">' . "\n";
        $x .= '<input type="hidden" id="YearClassID" name="YearClassID" value="' . $YearClassID . '">' . "\n";

        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        // awardCode remarks layer
        $awardAry = $lreportcard_award->Get_Report_Award_Info($ReportID);
        $numOfAward = count($awardAry);
        $thisRemarksType = 'awardCode';
        $x .= '<div id="remarkDiv_' . $thisRemarksType . '" class="selectbox_layer" style="width:500px;">' . "\r\n";
        $x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">' . "\r\n";
        $x .= '<tbody>' . "\r\n";
        $x .= '<tr>' . "\r\n";
        $x .= '<td align="right" style="border-bottom: medium none;">' . "\r\n";
        $x .= '<a href="javascript:hideRemarkLayer(\'' . $thisRemarksType . '\');"><img border="0" src="' . $PATH_WRT_ROOT . $image_path . '/' . $LAYOUT_SKIN . '/ecomm/btn_mini_off.gif"></a>' . "\r\n";
        $x .= '</td>' . "\r\n";
        $x .= '</tr>' . "\r\n";
        $x .= '<tr>' . "\r\n";
        $x .= '<td align="left" style="border-bottom: medium none;">' . "\r\n";
        $x .= '<table class="common_table_list_v30 view_table_list_v30">' . "\r\n";
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th>' . $ColumnTitleArr[6] . '</th>' . "\n";
        $x .= '<th>' . $ColumnTitleArr[5] . '</th>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";
        $x .= '<tbody>' . "\n";
        if ($numOfAward == 0) {
            $x .= '<tr><td colspan="2" align="center">' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>' . "\n";
        } else {
            foreach ((array) $awardAry as $_awardId => $_awardAry) {
                $_awardCode = $_awardAry['BasicInfo']['AwardCode'];
                $_awardNameEn = $_awardAry['BasicInfo']['AwardNameEn'];
                $_awardNameCh = $_awardAry['BasicInfo']['AwardNameCh'];
                $_awardName = Get_Lang_Selection($_awardNameCh, $_awardNameEn);

                $x .= '<tr>' . "\n";
                $x .= '<td>' . $_awardCode . '</td>' . "\n";
                $x .= '<td>' . $_awardName . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\r\n";
        $x .= '</td>' . "\r\n";
        $x .= '</tr>' . "\r\n";
        $x .= '</tbody>' . "\r\n";
        $x .= '</table>' . "\r\n";
        $x .= '</div>' . "\r\n";

        return $x;
    }

    function Get_Import_Student_AwardText_Step2_UI($ReportID, $YearClassID, $TargetFilePath, $numOfCsvData)
    {
        global $Lang, $eReportCard;
        global $lreportcard_award, $linterface;

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Reports_GenerateReport'],
            "javascript:js_Go_Back_To_Report_Generation();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ViewAward'],
            "javascript:js_Change_View_Mode('" . $ReportID . "', 'Class');"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['Btn']['Import'],
            ""
        );

        // ## Get Report Card Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);
        $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID, 1);
        $FormName = $this->returnClassLevel($ClassLevelID);

        if ($YearClassID != '') {
            $ClassInfoArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, $YearClassID, $returnAssoName = 0, $returnAssoInfo = 1);
            $ClassName = Get_Lang_Selection($ClassInfoArr[$YearClassID]['ClassTitleB5'], $ClassInfoArr[$YearClassID]['ClassTitleEN']);
        }

        // ## Buttons
        $ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();", 'ContinueBtn', '', $Disabled = 1);
        $BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Back_To_Import_Step1();");
        $CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_View_Index();");

        $x = '';
        $x .= '<form id="form1" name="form1" method="post" action="import_award_step3.php">' . "\n";
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        $x .= '<br style="clear:both;" />' . "\n";
        $x .= $linterface->GET_IMPORT_STEPS($CurrStep = 2);
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportTitle'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTitle . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportType'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTypeDisplay . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
        $x .= '<td>' . $FormName . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        if ($YearClassID != '') {
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
            $x .= '<td>' . $ClassName . '</td>' . "\n";
            $x .= '</tr>' . "\n";
        }
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['SuccessfulRecord'] . '</td>' . "\n";
        $x .= '<td><div id="SuccessCountDiv">' . $Lang['General']['EmptySymbol'] . '</div></td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['FailureRecord'] . '</td>' . "\n";
        $x .= '<td><div id="FailCountDiv">' . $Lang['General']['EmptySymbol'] . '</div></td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<br style="clear:both;" />' . "\n";

        $x .= '<div id="ErrorTableDiv"></div>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</div>' . "\n";

        // Buttons
        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= $ContinueBtn;
        $x .= '&nbsp;' . "\n";
        $x .= $BackBtn;
        $x .= '&nbsp;' . "\n";
        $x .= $CancelBtn;
        $x .= '</div>' . "\n";

        // iFrame for validation
        $thisSrc = "ajax_validate.php?Action=Import_Student_AwardText&ReportID=" . $ReportID . "&YearClassID=" . $YearClassID . "&TargetFilePath=" . $TargetFilePath;
        $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="' . $thisSrc . '" style="width:100%;height:300px;display:none;"></iframe>' . "\n";
        // $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;"></iframe>'."\n";

        // Hidden Field for Step 3 use
        $x .= '<input type="hidden" id="ReportID" name="ReportID" value="' . $ReportID . '" />' . "\n";
        $x .= '<input type="hidden" id="YearClassID" name="YearClassID" value="' . $YearClassID . '" />' . "\n";
        $x .= '<input type="hidden" id="numOfCsvData" name="numOfCsvData" value="' . $numOfCsvData . '" />' . "\n";

        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        return $x;
    }

    function Get_Import_Student_AwardText_Step3_UI($ReportID, $YearClassID = '')
    {
        global $Lang, $eReportCard;
        global $lreportcard_award, $linterface;

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Reports_GenerateReport'],
            "javascript:js_Go_Back_To_Report_Generation();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ViewAward'],
            "javascript:js_Change_View_Mode('" . $ReportID . "', 'Class');"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['Btn']['Import'],
            ""
        );

        // ## Get Report Card Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);
        $ReportTypeDisplay = $this->Get_Report_Card_Type_Display($ReportID, 1);
        $FormName = $this->returnClassLevel($ClassLevelID);

        // ## Buttons
        $ImportOtherBtn = $linterface->GET_ACTION_BTN($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImportOtherAwards'], "button", "js_Back_To_Import_Step1();");
        $BackBtn = $linterface->GET_ACTION_BTN($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GoBackToViewAwards'], "button", "js_Go_View_Index();");

        $x = '';
        $x .= '<form id="form1" name="form1" method="post">' . "\n";
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        $x .= '<br style="clear:both;" />' . "\n";
        $x .= $linterface->GET_IMPORT_STEPS($CurrStep = 3);
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext" style="text-align:center;">' . "\n";
        $x .= '<span id="ImportStatusSpan"></span>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</div>' . "\n";

        // Buttons
        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= $ImportOtherBtn;
        $x .= '&nbsp;' . "\n";
        $x .= $BackBtn;
        $x .= '</div>' . "\n";

        // iFrame for importing data
        $thisSrc = "ajax_update.php?Action=Import_Student_AwardText&ReportID=$ReportID";
        $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="' . $thisSrc . '" style="width:100%;height:300px;display:none;"></iframe>' . "\n";

        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        return $x;
    }

    // Rita added 20120620 - Get Report Card Info for ReportCard Templates
    public function Get_Settings_ReportCardTemplate_ReportCardInfo_Table($ReportID, $YearClassID = '', $ExtraRowAry = '')
    {
        global $eReportCard, $Lang;

        // ## Get Report Card Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
        $ReportType = $this->Get_Report_Card_Type_Display($ReportID, 1);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $FormName = $this->returnClassLevel($ClassLevelID);
        if ($YearClassID != '') {
            $ClassInfoArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, $YearClassID, $returnAssoName = 0, $returnAssoInfo = 1);
            $ClassName = Get_Lang_Selection($ClassInfoArr[$YearClassID]['ClassTitleB5'], $ClassInfoArr[$YearClassID]['ClassTitleEN']);
        }

        $numOfExtraRow = count((array) $ExtraRowAry);

        $x = '';
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportTitle'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTitle . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportType'] . '</td>' . "\n";
        $x .= '<td>' . $ReportType . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
        $x .= '<td>' . $FormName . '</td>' . "\n";
        $x .= '</tr>' . "\n";

        if ($YearClassID != '') {
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
            $x .= '<td>' . $ClassName . '</td>' . "\n";
            $x .= '</tr>' . "\n";
        }

        if ($ExtraRowAry != '' && $numOfExtraRow > 0) {
            for ($i = 0; $i < $numOfExtraRow; $i ++) {
                $_title = $ExtraRowAry[$i]['title'];
                $_content = $ExtraRowAry[$i]['content'];

                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title">' . $_title . '</td>' . "\n";
                $x .= '<td>' . $_content . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
        }
        $x .= '</table>' . "\n";
        return $x;
    }

    // Rita added 20120620 - Navigation for ReportCard Templates
    public function Get_Settings_ReportCardTemplate_Nav()
    {
        global $eReportCard, $Lang;
        global $linterface;

        // ## Navigation
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Settings_ReportCardTemplates'],
            "javascript:js_Go_Back();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['Btn']['Edit']
        );

        $x = '';
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        return $x;
    }

    public function Get_Management_Class_Teacher_Comment_Edit_UI($ReportID, $YearID, $YearClassID, $Result, $SuccessCount, $cut_row, $isPrintMode, $WrongConductRow)
    {
        global $eReportCard, $Lang, $i_con_msg_num_records_updated;
        global $linterface;
        global $eRCTemplateSetting;
        global $image_path, $LAYOUT_SKIN, $button_view;
        global $intranet_root, $PATH_WRT_ROOT, $UserID, $ck_ReportCard_UserType;

        // ## Get Report Card Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = $ReportInfoArr['ReportTitle'];
        $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
        $ReportType = $this->Get_Report_Card_Type_Display($ReportID, 1);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $FormName = $this->returnClassLevel($ClassLevelID);
        if ($YearClassID != '') {
            $ClassInfoArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, $YearClassID, $returnAssoName = 0, $returnAssoInfo = 1);
            $ClassName = Get_Lang_Selection($ClassInfoArr[$YearClassID]['ClassTitleB5'], $ClassInfoArr[$YearClassID]['ClassTitleEN']);
        }
        $SemID = $ReportInfoArr['Semester'];
        $SemNum = $this->Get_Semester_Seq_Number($SemID);

        // ## Toolbar Button
        if ($ck_ReportCard_UserType != "VIEW_GROUP") {
            $btn_Import = $linterface->Get_Content_Tool_v30("import", "javascript:js_Import();");
        }
        $btn_Export = $linterface->Get_Content_Tool_v30('export', "javascript:js_Export();");
        if ($eRCTemplateSetting['ClassTeacherComment']['PrintPage']) {
            $btn_Print = $linterface->Get_Content_Tool_v30('print', 'javascript:js_Go_Print();');
        }

        // ## Navigation
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Management_ClassTeacherComment'],
            "javascript:js_Go_Back();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['Btn']['Edit']
        );

        // ## Buttons
        if ($ck_ReportCard_UserType != "VIEW_GROUP") {
            $BtnSave = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "document.form1.submit()") . "&nbsp";
            $BtnReset = $linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset", "") . "&nbsp";
        }
        $BtnCancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();");

        // ## Column Width
        $ClassNoWidth = 6;
        // $StudentNameWidth = 20;
        $StudentNameWidth = 25;
        $StudentReportIconWidth = 6;
        if ($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
            $ConductWidth = 20;
        }
        if($eRCTemplateSetting['ClassTeacherComment_Conduct'] && $eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'] && $SemNum == 3) {
            $ConductWidth2 = 10;
        }

        $NumOfCommentCol = 1;
        // [2019-0923-1726-31289]
        if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) {
            if($this->IS_ADMIN_USER($UserID)) {
                $NumOfCommentCol++;
            }
        }
        else if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
            $NumOfCommentCol++;
        }
        $CommentWidth = floor((100 - $ClassNoWidth - $StudentNameWidth - $ConductWidth - $ConductWidth2) / $NumOfCommentCol);

        // ## Student Info
        $StudentInfoArr = $this->GET_STUDENT_BY_CLASS($YearClassID, '', $isShowBothLangs = 1, $withClassNumber = 0, $isShowStyle = 1);
        $StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
        $numOfStudent = count($StudentIDArr);

        // ## Report Generated (boolean)
        $LastGenerated = $ReportInfoArr['LastGenerated'];
        $ReportIsGenerated_bool = (is_date_empty($LastGenerated)) ? false : true;

        // ## Get Comment Info
        $CommentInfoArr = $this->GET_TEACHER_COMMENT($StudentIDArr, "", $ReportID);
        $CommentMaxLength = $this->Get_Class_Teacher_Comment_MaxLength($ReportID);

        // ## Get Conduct
        if ($eRCTemplateSetting['ClassTeacherComment_Conduct'])
        {
            include_once ($PATH_WRT_ROOT . 'includes/libreportcard2008_extrainfo.php');
            $lreportcard_extrainfo = new libreportcard_extrainfo();

            // Get Conduct List
            $ConductArr = $lreportcard_extrainfo->Get_Conduct();
            $ConductArr = BuildMultiKeyAssoc((array) $ConductArr, "ConductID", "Conduct", 1);

            $StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($ReportID, $StudentIDArr);
            $StudentConductID = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);

            // Pui Ching Conduct handling
            if($eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'])
            {
                global $ReportCardCustomSchoolName;

                include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
                $lreportcard_cust = new libreportcardcustom();

                // [2015-1104-1130-08164] Pui Ching Conduct display limit (except eReportCard admin)
                if(!$this->IS_ADMIN_USER($UserID) && $SemID != "F")
                {
                    $failAry = array();
                    $disciplineAry = array();

                    // Get Report Column
                    $relatedReportColumn = $this->returnReportTemplateColumnData($ReportID);
                    $relatedReportColumn = Get_Array_By_Key($relatedReportColumn, "ReportColumnID");

                    // Get Main Subject IDs
                    $MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID, 0, '', '', 'Desc', 1, $ReportID);
                    $MainSubjectIDArray = array();
                    if (sizeof($MainSubjectArray) > 0) {
                        foreach ($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
                    }

                    // [2019-1030-0914-49066] Set Up Main / Competition Subject
                    $lreportcard_cust->Set_Main_Competition_Subject_List($ClassLevelID);

                    // Get Completition Subjects
                    $excludedSubjectAry = $lreportcard_cust->CompletitionSubjectList;

                    // Get Subject Code Mapping
                    $SubjectCodeMapping = $lreportcard_cust->GET_SUBJECTS_CODEID_MAP(0, 0);

                    // Get Marksheet Input Score
                    $data = $this->GET_MARKSHEET_INPUT_SCORE($ReportID, $relatedReportColumn, $StudentIDArr, $MainSubjectIDArray, 0);

                    // Marksheet Input Score
                    // loop students (to store students with fail subjects)
                    foreach ((array) $data as $dataStudentID => $dataStudent) {
                        if (empty($dataStudent)) {
                            continue;
                        }

                        // loop Subject
                        foreach ((array) $dataStudent as $dataSubjectID => $dataSubject) {
                            // Get Subject Code
                            $dataSubjectCode = $SubjectCodeMapping[$dataSubjectID];

                            // already with fail subject => Stop checking
                            if ($failAry[$dataStudentID] == true) {
                                break;
                            }

                            // skip this subject if this is component => Check another subjects
                            if (empty($dataSubject) || $this->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($dataSubjectID)) {
                                continue;
                            }

                            // skip this subject if this is not main subject / is training subject => Check another subjects
                            if (empty($MainSubjectIDArray) || ! in_array($dataSubjectID, $MainSubjectIDArray) || in_array($dataSubjectCode, $excludedSubjectAry)) {
                                continue;
                            }

                            // loop data column
                            foreach ((array) $dataSubject as $dataColumnID => $dataColumn) {
                                // Check input score is fail or not
                                if ($dataColumn["MarkType"] == "M" && $dataColumn["MarkRaw"] < 60 || $dataColumn["MarkType"] == "SC" && $dataColumn["MarkNonNum"] == "+") {
                                    $failAry[$dataStudentID] = true;
                                    break;
                                }
                            }
                        }
                    }

                    // Discipline Records
                    // loop student (to store students with punishment records)
                    $disciplineTypeName = $eRCTemplateSetting['MSTableFooter']['Fields'];
                    foreach ((array) $StudentIDArr as $thisStudentID) {
                        // Get discipline record for checking
                        $thisOtherInfo = $this->getReportOtherInfoData($ReportID, $thisStudentID);

                        // Get data from eDiscipline
                        $edisData = $this->Get_Student_Profile_Data($ReportID, $thisStudentID, true, true, true, $SemID, false);

                        // Merge discipline data
                        $edisData = $lreportcard_cust->MergeDisciplineData($thisOtherInfo, $edisData, $thisStudentID, $SemID);

                        // Check if student receive any punishment
                        if ($edisData[$thisStudentID][$SemID][$disciplineTypeName[5]] > 0 || $edisData[$thisStudentID][$SemID][$disciplineTypeName[6]] > 0) {
                            $disciplineAry[$thisStudentID] = 1;
                        }
                        if ($edisData[$thisStudentID][$SemID][$disciplineTypeName[7]] > 0) {
                            $disciplineAry[$thisStudentID] = 2;
                        }
                        if ($edisData[$thisStudentID][$SemID][$disciplineTypeName[8]] > 0) {
                            $disciplineAry[$thisStudentID] = 3;
                        }
                    }
                }

                // [2018-0621-1453-45066] Calculate Overall Grade
                if($SemNum == 3)
                {
                    // Related Term Reports
                    $relatedTermReports = $this->Get_Report_List($ClassLevelID, "T");
                    $relatedTermReports = BuildMultiKeyAssoc($relatedTermReports, array("isMainReport", "Semester"));
                    $relatedTermReports = $relatedTermReports[1];

                    // loop Term Reports
                    $termNumber = 0;
                    $studentTermConductIDArr = array();
                    foreach((array)$relatedTermReports as $termReportInfo)
                    {
                        // Get Term Conduct
                        $termStudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($termReportInfo["ReportID"], $StudentIDArr);
                        $termStudentConductIDArr = BuildMultiKeyAssoc($termStudentExtraInfo, "StudentID", "ConductID", 1);
                        foreach($termStudentConductIDArr as $_studentID => $termStudentConductID) {
                            $studentTermConductIDArr[$_studentID][$termNumber] = $ConductArr[$termStudentConductID];
                        }
                        $termNumber++;
                    }

                    $studentOverallConductArr = array();
                    foreach($studentTermConductIDArr as $_studentID => $thisTermConductArr) {
                        $studentOverallConductArr[$_studentID] = $lreportcard_cust->calculateOverallConduct($thisTermConductArr, $ConductArr);
                    }
                    unset($studentTermConductIDArr);
                }
            }
        }

        # [2018-1130-1440-05164] Get Previous Term Report Comment
        if($eRCTemplateSetting['Management']['ClassTeacherComment']['DisplayPreviousComment'] && ($SemID == 'F' || $SemNum > 1))
        {
            # Retrieve Term Reports
            if(!isset($lreportcard_cust))
            {
                global $ReportCardCustomSchoolName;
                include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

                $lreportcard_cust = new libreportcardcustom();
            }
            $TermReportIDArr = $lreportcard_cust->GET_ALL_RELATED_TERM_REPORT_LIST($ReportID);

            // loop Term Reports
            $TermCommentInfoArr = array();
            foreach((array)$TermReportIDArr as $thisIndex => $thisTermReportID)
            {
                // No need to display next term report
                if(($SemID != 'F' && $thisTermReportID == '') || $thisTermReportID == $ReportID)
                {
                    continue;
                }

                $TermCommentInfoArr[$thisIndex] = $this->GET_TEACHER_COMMENT($StudentIDArr, "", $thisTermReportID);
            }
        }

        if ($Result == "num_records_updated") {
            $msg = "$SuccessCount $i_con_msg_num_records_updated";
            if ($cut_row) {
                $warnmsg .= " " . sprintf($eReportCard['CommentTooLong'], $cut_row);
            }
            if ($WrongConductRow) {
                $warnmsg .= " " . sprintf($eReportCard['ConductNotFound'], $WrongConductRow);
            }

            if ($warnmsg) {
                $color = 'red';
            }
            else {
                $color = 'green';
            }

            $SysMsg = $linterface->GET_SYS_MSG("", "<font color='$color'>$msg $warnmsg</font>");
        } else {
            $SysMsg = $linterface->GET_SYS_MSG($Result);
        }

        $x = '';
        $x .= '<br />' . "\n";
        $x .= '<form id="form1" name="form1" method="post" action="edit_update_new.php">' . "\n";
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";

        // [2018-1130-1440-05164] Hide import function
        if(!$eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) {
            $x .= $btn_Import . "\n";
        }

        $x .= $btn_Export . "\n";
        $x .= $btn_Print . "\n";
        $x .= '</div>' . "\n";
        $x .= '<div style="float:right;">' . "\n";
        $x .= $SysMsg;
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<br style="clear:both;" />' . "\n";

        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) . "\n";
        $x .= '<br style="clear:both;" />' . "\n";

        $x .= '<!--PrintStart-->' . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportTitle'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTitle . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['ReportType'] . '</td>' . "\n";
        $x .= '<td>' . $ReportType . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
        $x .= '<td>' . $FormName . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        if ($YearClassID != '') {
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
            $x .= '<td>' . $ClassName . '</td>' . "\n";
            $x .= '</tr>' . "\n";
        }
        $x .= '</table>' . "\n";
        $x .= '<br style="clear:both;" />' . "\n";

        $x .= '<table id="ContentTable" class="common_table_list_v30 edit_table_list_v30">' . "\n";
        $x .= '<col style="width:' . $ClassNoWidth . '%;" />' . "\n";
        $x .= '<col style="width:' . $StudentNameWidth . '%;" />' . "\n";
        if ($eRCTemplateSetting['Management']['ClassTeacherComment']['ShowGeneratedReportCard']) {
            $x .= '<col style="width:' . $StudentReportIconWidth . '%;" />' . "\n";
        }
        $x .= '<col style="width:' . $CommentWidth . '%;" />' . "\n";
        // [2019-0923-1726-31289]
        if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) {
            if($this->IS_ADMIN_USER($UserID)) {
                $x .= '<col style="width:' . $CommentWidth . '%;" />' . "\n";
            }
        }
        else if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
            $x .= '<col style="width:' . $CommentWidth . '%;" />' . "\n";
        }
        if ($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
            $x .= '<col style="width:' . $ConductWidth . '%;" />' . "\n";
        }
        if($eRCTemplateSetting['ClassTeacherComment_Conduct'] && $eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'] && $SemNum == 3) {
            $x .= '<col style="width:' . $ConductWidth2 . '%;" />' . "\n";
        }

        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</th>' . "\n";
        // $x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</th>'."\n";
        $x .= '<th>';
        $x .= $Lang['SysMgr']['FormClassMapping']['StudentName'];
        $x .= '&nbsp;';
        $x .= '<a href="javascript: void(0)" onclick="js_show_all_propic()" class="ProPicShow_ALL">' . $eReportCard['ClassTeacherCommentTemp']['ViewAll'] . '</a>';
        $x .= '<a href="javascript: void(0)" onclick="js_hide_all_propic()" class="ProPicHide_ALL" style="display: none;">' . $eReportCard['ClassTeacherCommentTemp']['HideAll'] . '</a>';
        $x .= '</th>' . "\n";
        if ($eRCTemplateSetting['Management']['ClassTeacherComment']['ShowGeneratedReportCard']) {
            $x .= '<th>' . $eReportCard['ManagementArr']['ClassTeacherCommentArr']['GenerateReport'] . '</th>' . "\n";
        }
        // [2020-0708-0950-12308]
        if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher']) {
            $x .= '<th>' . $eReportCard['ManagementArr']['ClassTeacherCommentArr']['CommentCustDisplay'] . '</th>' . "\n";
        } else {
            $x .= '<th>' . $eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'] . '</th>' . "\n";
        }
        // [2019-0923-1726-31289]
        if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) {
            if($this->IS_ADMIN_USER($UserID)) {
                $x .= '<th>' . $eReportCard['ManagementArr']['ClassTeacherCommentArr']['ManualAdjustComment'] . '</th>' . "\n";
            }
        }
        else if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
            $x .= '<th>' . $eReportCard['ManagementArr']['ClassTeacherCommentArr']['AdditionalComment'] . '</th>' . "\n";
        }
        if ($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
            $x .= '<th>' . $eReportCard['Conduct'] . '</th>' . "\n";
        }
        if($eRCTemplateSetting['ClassTeacherComment_Conduct'] && $eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'] && $SemNum == 3) {
            $x .= '<th>' . $eReportCard['WholeYear'] . '</th>' . "\n";
        }

        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";

        $x .= '<tbody>' . "\n";
        if ($numOfStudent == 0)
        {
            $x .= '<tr><td col="100%">' . $Lang['General']['EmptyTableMsg']['NoClass'] . '</td></tr>' . "\n";
        }
        else
        {
            for ($i = 0; $i < $numOfStudent; $i ++)
            {
                $thisStudentID = $StudentInfoArr[$i]['UserID'];
                $thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
                $thisStudentName = $StudentInfoArr[$i]['StudentName'];
                $thisComment = $CommentInfoArr[$thisStudentID]['Comment'];

                if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
                    $thisAdditionalComment = $CommentInfoArr[$thisStudentID]['AdditionalComment'];
                }

                if ($eRCTemplateSetting['Management']['ClassTeacherComment']['ShowGeneratedReportCard']) {
                    // ## Generate Student Report Icon
                    if ($ReportIsGenerated_bool) {
                        $thisStudentReportIcon = "<a href=\"javascript:newWindow('" . $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/reports/generate_reports/print_preview.php?ClassID=$YearClassID&TargetStudentID[]=$thisStudentID&ReportID=$ReportID&hp=1', '10');\" class=\"tablelink\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
                    } else {
                        $thisStudentReportIcon = $Lang['General']['EmptySymbol'];
                    }
                }

                # [2018-1130-1440-05164] Comment Selection Handling     (similar layout in Get_Student_New_Comment_Div_UI())
                if($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
                {
                    $thisCommentDiv = '';
                    $thisCommentDiv .= '<div id="StudentDiv_' . $thisStudentID . '">' . "\n";

                    // loop selected comments
                    $thisCommentIDArr = $CommentInfoArr[$thisStudentID]['CommentIDList'];
                    $thisCommentIDArr = explode(',', $thisCommentIDArr);
                    $thisCommentIDArr = array_values(array_filter(array_unique($thisCommentIDArr)));
                    foreach((array)$thisCommentIDArr as $thisCommentID)
                    {
                        // Delete button for Comment Div
                        $thisDeleteButton = $linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['Btn']['Delete'], "js_Delete_Comment('".$thisStudentID."', '".$thisCommentID."')", $ParClass = "", $WithSpan = 1);

                        $thisCommentInfo = $this->GET_MARKSHEET_COMMENT_INFO_BY_ID($thisCommentID);
                        $thisCommentTextCh = $thisCommentInfo['Comment'];
                        $thisCommentTextEn = $thisCommentInfo['CommentEng'];
                        $thisCommentTextDisplay = nl2br($thisCommentTextCh).'<br/>'.nl2br($thisCommentTextEn);

                        // Comment Div  (Display Comment + Delete button + Hidden fields for CommentID)
                        $thisCommentDiv .= '<div id="StudentCommentDiv_'.$thisStudentID.'_'.$thisCommentID.'" style="border:1px dotted grey; margin:5px; padding:2px">' . "\n";
                            $thisCommentDiv .= '<span style="float:left; min-width:200px">' . $thisCommentTextDisplay. '&nbsp;&nbsp;</span>' . $thisDeleteButton . "\n";
                            $thisCommentDiv .= '<br style="clear:both;" />' . "\n";
                            $thisCommentDiv .= '<input type="hidden" name="CommentIDList['.$thisStudentID.'][]" value="'.$thisCommentID.'"/>' . "\n";
                        $thisCommentDiv .= '</div>' . "\n";
                    }
                    $thisCommentDiv .= '</div>' . "\n";

                    // Add Comment button
                    $thisCommentDiv .= '<div style="margin:5px; padding:2px">' . "\n";
                        $thisCommentDiv .= '<span class="table_row_tool row_content_tool">' . "\n";
                            $thisCommentDiv .= $linterface->Get_Thickbox_Link(450, 750, $ExtraClass = 'add_dim', $eReportCard['AddComment'], "js_Show_Add_Comment_Layer('" . $thisStudentID . "'); return false;", $InlineID = "FakeLayer", $Content = "", $LinkID = '');
                        $thisCommentDiv .= '</span>' . "\n";
                    $thisCommentDiv .= '</div>' . "\n";

                    // [2019-0923-1726-31289] Manual Adjust Comment
                    if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment'] && $this->IS_ADMIN_USER($UserID))
                    {
                        // Textarea
                        $thisTextareaID = 'AdditionalCommentTextarea_' . $thisStudentID;
                        $thisTextareaName = 'AdditionalCommentArr[' . $thisStudentID . ']';
                        if ($CommentMaxLength != - 1) {
                            // 2013-0409-1636-35156
                            // $thisOthersPara = 'onkeyup="limitText(this, '.$CommentMaxLength.');" onpaste="limitText(this, '.$CommentMaxLength.');"';
                            $thisOthersPara = ' onkeyup="limitText(this, ' . $CommentMaxLength . ');" onpaste="setTimeout(function() { limitText(document.getElementById(\'' . $thisTextareaID . '\'), ' . $CommentMaxLength . '); }, 10);" ondrop="setTimeout(function() { limitText(document.getElementById(\'' . $thisTextareaID . '\'), ' . $CommentMaxLength . '); }, 10);" ';
                        }
                        $thisAdditionalCommentTextarea = $linterface->GET_TEXTAREA($thisTextareaName, $thisAdditionalComment, $taCols = 70, $taRows = 5, $OnFocus = "", $readonly = "0", $thisOthersPara, $class = '', $thisTextareaID);

                        // auto complete textbox
                        $thisTbID = 'AdditionalCommentSearchTb_' . $thisStudentID;
                        $thisTbName = 'AdditionalCommentSearchTb_' . $thisStudentID;
                        $thisTbClass = 'AdditionalCommentSearchTb';
                        $thisTbOnKeyPress = 'js_OnKeyPress_AutoComplete_Textbox(\'' . $thisStudentID . '\', 0);';
                        $thisAdditionalCommentSearchTb = '<input type="text" id="' . $thisTbID . '" name="' . $thisTbName . '" class="' . $thisTbClass . '" onkeydown="' . $thisTbOnKeyPress . '" value="" />';

                        // serach button
                        $thisAdditionalSearchBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", 'js_Clicked_Select_Comment(\'' . $thisStudentID . '\', \'AdditionalComment\');');
                    }
                }
                else
                {
                    // Textarea
                    $thisTextareaID = 'CommentTextarea_' . $thisStudentID;
                    $thisTextareaName = 'CommentArr[' . $thisStudentID . ']';

                    if ($CommentMaxLength != - 1) {
                        // 2013-0409-1636-35156
                        // $thisOthersPara = 'onkeyup="limitText(this, '.$CommentMaxLength.');" onpaste="limitText(this, '.$CommentMaxLength.');"';
                        $thisOthersPara = ' onkeyup="limitText(this, ' . $CommentMaxLength . ', 0, 1, \'alertCommentMaximum()\');" onpaste="setTimeout(function() { limitText(document.getElementById(\'' . $thisTextareaID . '\'), ' . $CommentMaxLength . ', 0, 1, \'alertCommentMaximum()\'); }, 10);" ondrop="setTimeout(function() { limitText(document.getElementById(\'' . $thisTextareaID . '\'), ' . $CommentMaxLength . ', 0, 1, \'alertCommentMaximum()\'); }, 10);" ';
                    }

                    if ($ck_ReportCard_UserType == "VIEW_GROUP") {
                        $thisOthersPara .= " disabled  ";
                    }

                    $thisCommentTextarea = '';
                    $thisCommentTextarea = $linterface->GET_TEXTAREA($thisTextareaName, $thisComment, $taCols = 70, $taRows = 5, $OnFocus = "", $readonly = "0", $thisOthersPara, $class = '', $thisTextareaID);

                    // $thisCommentTextarea='';
                    // Auto complete textbox
                    $thisTbID = 'CommentSearchTb_' . $thisStudentID;
                    $thisTbName = 'CommentSearchTb_' . $thisStudentID;
                    $thisTbClass = 'CommentSearchTb';
                    $thisTbOnKeyPress = 'js_OnKeyPress_AutoComplete_Textbox(\'' . $thisStudentID . '\', 0);';
                    // $thisCommentSearchTb = '<input type="text" id="'.$thisTbID.'" name="'.$thisTbName.'" class="'.$thisTbClass.'" onkeypress="'.$thisTbOnKeyPress.'" value="" />';
                    $thisCommentSearchTb = '<input type="text" id="' . $thisTbID . '" name="' . $thisTbName . '" class="' . $thisTbClass . '" onkeydown="' . $thisTbOnKeyPress . '" value="" />';

                    // Serach button
                    $thisSearchBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", 'js_Clicked_Select_Comment(\'' . $thisStudentID . '\', \'Comment\');');

                    if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment'])
                    {
                        // Textarea
                        $thisTextareaID = 'AdditionalCommentTextarea_' . $thisStudentID;
                        $thisTextareaName = 'AdditionalCommentArr[' . $thisStudentID . ']';
                        if ($CommentMaxLength != - 1) {
                            // 2013-0409-1636-35156
                            // $thisOthersPara = 'onkeyup="limitText(this, '.$CommentMaxLength.');" onpaste="limitText(this, '.$CommentMaxLength.');"';
                            $thisOthersPara = ' onkeyup="limitText(this, ' . $CommentMaxLength . ');" onpaste="setTimeout(function() { limitText(document.getElementById(\'' . $thisTextareaID . '\'), ' . $CommentMaxLength . '); }, 10);"  ondrop="setTimeout(function() { limitText(document.getElementById(\'' . $thisTextareaID . '\'), ' . $CommentMaxLength . '); }, 10);" ';
                        }
                        $thisAdditionalCommentTextarea = $linterface->GET_TEXTAREA($thisTextareaName, $thisAdditionalComment, $taCols = 70, $taRows = 5, $OnFocus = "", $readonly = "0", $thisOthersPara, $class = '', $thisTextareaID);

                        // Auto complete textbox
                        $thisTbID = 'AdditionalCommentSearchTb_' . $thisStudentID;
                        $thisTbName = 'AdditionalCommentSearchTb_' . $thisStudentID;
                        $thisTbClass = 'AdditionalCommentSearchTb';
                        $thisTbOnKeyPress = 'js_OnKeyPress_AutoComplete_Textbox(\'' . $thisStudentID . '\', 1);';
                        // $thisAdditionalCommentSearchTb = '<input type="text" id="'.$thisTbID.'" name="'.$thisTbName.'" class="'.$thisTbClass.'" onkeypress="'.$thisTbOnKeyPress.'" value="" />';
                        $thisAdditionalCommentSearchTb = '<input type="text" id="' . $thisTbID . '" name="' . $thisTbName . '" class="' . $thisTbClass . '" onkeydown="' . $thisTbOnKeyPress . '" value="" />';

                        // Serach button
                        $thisAdditionalSearchBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", 'js_Clicked_Select_Comment(\'' . $thisStudentID . '\', \'AdditionalComment\');');
                    }
                }

                # [2018-1130-1440-05164] Display previous term report comments
                $previousTermComment = '';
                if($eRCTemplateSetting['Management']['ClassTeacherComment']['DisplayPreviousComment'] && ($SemID == 'F' || $SemNum > 1))
                {
                    $previousTermCommentTr = '';
                    foreach((array)$TermCommentInfoArr as $thisSemNum => $thisTermCommentInfo)
                    {
                        $thisTermComment = $thisTermCommentInfo[$thisStudentID]['Comment'];

                        // [2019-0923-1726-31289] manual adjust comment > selected comment
                        $thisTermAdditionalComment = $thisTermCommentInfo[$thisStudentID]['AdditionalComment'];
                        if(!empty($thisTermAdditionalComment) && trim($thisTermAdditionalComment) != '') {
                            $previousTermCommentTr .= '<tr>';
                                $previousTermCommentTr .= '<td>'.Get_Lang_Selection($eReportCard['Template']['Term'.($thisSemNum + 1).'Ch'], $eReportCard['Template']['Term'.($thisSemNum + 1).'En']).'</td>';
                                $previousTermCommentTr .= '<td>'.nl2br($thisTermAdditionalComment).'</td>';
                            $previousTermCommentTr .= '<tr>';
                        }
                        else if(!empty($thisTermComment) && trim($thisTermComment) != '') {
                            $previousTermCommentTr .= '<tr>';
                                $previousTermCommentTr .= '<td>'.Get_Lang_Selection($eReportCard['Template']['Term'.($thisSemNum + 1).'Ch'], $eReportCard['Template']['Term'.($thisSemNum + 1).'En']).'</td>';
                                $previousTermCommentTr .= '<td>'.nl2br($thisTermComment).'</td>';
                            $previousTermCommentTr .= '<tr>';
                        }
                    }

                    if($previousTermCommentTr != '')
                    {
                        $previousTermComment .= '<br/>';
                        $previousTermComment .= '<br/>';
                        $previousTermComment .= '<a href="javascript:void(0);" onclick="js_show_comment_table('.$thisStudentID.')" id="CommentView_'.$thisStudentID.'" class="ProPicView">[' . $eReportCard['CommentViewPreviousTerm']. ']</a>';
                        $previousTermComment .= '<a href="javascript:void(0);" onclick="js_hide_comment_table('.$thisStudentID.')" id="CommentHide_'.$thisStudentID.'" class="ProPicHide" style="display: none;">' . $eReportCard['ClassTeacherCommentTemp']['Hide'] . '</a>';

                        $previousTermComment .= '<table class="common_table_list_v30" id="previousComment_'.$thisStudentID.'" style="display: none">';
                            $previousTermComment .= '<tr>';
                                $previousTermComment .= '<th class="sub_row_top" width="90px">'.$eReportCard['Term'].'</td>';
                                $previousTermComment .= '<th class="sub_row_top">'.$eReportCard['Comment'].'</td>';
                            $previousTermComment .= '</tr>';
                            $previousTermComment .= $previousTermCommentTr;
                        $previousTermComment .= '</table>';
                    }
                }

                $x .= '<tr>' . "\n";
                $x .= '<td>' . $thisClassNumber . '</td>' . "\n";
                // $x .= '<td>'.$thisStudentName.'</td>'."\n";
                $x .= '<td>';
                $x .= '<table width="100%" class="inside_form_table_v30 inside_form_table">';
                $x .= '<tr>';
                $x .= '<td>';
                $x .= $thisStudentName;
                $x .= '</td>';
                $x .= '<td width="35px">';
                $x .= '<a href="javascript:void(0);" onclick="js_show_propic(' . $thisStudentID . ')" id="ProPicView_' . $thisStudentID . '" class="ProPicView ProPicView">' . $eReportCard['ClassTeacherCommentTemp']['View'] . '</a>';
                $x .= '<a href="javascript:void(0);" onclick="js_hide_propic(' . $thisStudentID . ')" id="ProPicHide_' . $thisStudentID . '" class="ProPicHide" style="display: none;">' . $eReportCard['ClassTeacherCommentTemp']['Hide'] . '</a>';
                $x .= '</td>';
                $x .= '<td width="100px">';
                $x .= '<div id="profile_pic_' . $thisStudentID . '">';
                $x .= '</td>';
                $x .= '</tr>';
                $x .= '</table>';
                $x .= '</td>' . "\n";

                if ($eRCTemplateSetting['Management']['ClassTeacherComment']['ShowGeneratedReportCard']) {
                    $x .= '<td>' . $thisStudentReportIcon . '</td>' . "\n";
                }

                $x .= '<td>' . "\n";
                if($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
                {
                    $x .= $thisCommentDiv;
                }
                else
                {
                    if (!$isPrintMode && $ck_ReportCard_UserType != "VIEW_GROUP") {
                        $x .= $Lang['Btn']['Search'] . ' ' . $thisCommentSearchTb . ' ' . $Lang['General']['Or'] . ' ' . $thisSearchBtn;
                        $x .= '<br />';
                    }
                    $x .= $thisCommentTextarea;
                }
                if($eRCTemplateSetting['Management']['ClassTeacherComment']['DisplayPreviousComment'] && ($SemID == 'F' || $SemNum > 1)) {
                    $x .= $previousTermComment;
                }
                $x .= '</td>' . "\n";

                // [2019-0923-1726-31289]
                if ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly']) {
                    if($this->IS_ADMIN_USER($UserID)) {
                        $x .= '<td>' . "\n";
                        if (!$isPrintMode && $ck_ReportCard_UserType != "VIEW_GROUP") {
                            $x .= $Lang['Btn']['Search'] . ' ' . $thisAdditionalCommentSearchTb . ' ' . $Lang['General']['Or'] . ' ' . $thisAdditionalSearchBtn;
                            $x .= '<br />';
                        }
                        $x .= $thisAdditionalCommentTextarea;
                        $x .= '</td>' . "\n";
                    }
                }
                else if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
                    $x .= '<td>' . "\n";
                    if (!$isPrintMode && $ck_ReportCard_UserType != "VIEW_GROUP") {
                        $x .= $Lang['Btn']['Search'] . ' ' . $thisAdditionalCommentSearchTb . ' ' . $Lang['General']['Or'] . ' ' . $thisAdditionalSearchBtn;
                        $x .= '<br />';
                    }
                    $x .= $thisAdditionalCommentTextarea;
                    $x .= '</td>' . "\n";
                }

                if ($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
                    $x .= '<td>' . "\n";

                    if ($ck_ReportCard_UserType == "VIEW_GROUP") {
                        if ($StudentConductID[$thisStudentID] > 0) {
                            $x .= $ConductArr[$StudentConductID[$thisStudentID]] . "\n";
                        }
                        else {
                            $x .= "&nbsp;\n";
                        }
                    }
                    // [2015-1104-1130-08164] Disable some conduct grade according to marksheet input score & discipline records of student (except eReportCard Admin)
                    else if ($eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'] && !$this->IS_ADMIN_USER($UserID) && $SemID != "F" && (isset($failAry[$thisStudentID]) || isset($disciplineAry[$thisStudentID]))) {
                        $x .= $this->Get_ExtraInfo_Conduct_Selection_Disable_Option('StudentConductIDArr[' . $thisStudentID . ']', $StudentConductID[$thisStudentID], $failAry[$thisStudentID], $disciplineAry[$thisStudentID]) . "\n";
                    }
                    // Normal Case
                    else {
                        $x .= $this->Get_ExtraInfo_Conduct_Selection('StudentConductIDArr[' . $thisStudentID . ']', $StudentConductID[$thisStudentID], "", "") . "\n";
                    }

                    $x .= '</td>' . "\n";
                }

                if($eRCTemplateSetting['ClassTeacherComment_Conduct'] && $eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'] && $SemNum == 3) {
                    $x .= '<td>' . "\n";
                        $x .= $studentOverallConductArr[$thisStudentID] . "\n";
                    $x .= '</td>' . "\n";
                }

                $x .= '</tr>' . "\n";
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<!--PrintEnd-->' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</div>' . "\n";

        // Buttons
        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= '<div class="tabletextremark" style="float:left;">' . $eReportCard['DeletedStudentLegend'] . '</div>';
        $x .= '<br style="clear:both;" />' . "\n";
        $x .= $BtnSave;
        $x .= '&nbsp;' . "\n";
        $x .= $BtnReset;
        $x .= '&nbsp;' . "\n";
        $x .= $BtnCancel;
        $x .= '</div>' . "\n";

        // Hidden Field for Step 3 use
        $x .= '<input type="hidden" id="ReportID" name="ReportID" value="' . $ReportID . '" />' . "\n";
        $x .= '<input type="hidden" id="ClassLevelID" name="ClassLevelID" value="' . $YearID . '" />' . "\n";
        $x .= '<input type="hidden" id="ClassID" name="ClassID" value="' . $YearClassID . '" />' . "\n";

        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        return $x;
    }

    function Get_Add_Student_Existing_Comment_Layer_UI($ReportID, $StudentID, $excludeCommentIDArr)
    {
        global $Lang, $eReportCard, $linterface;

        /*
        // Get Grading Scheme for Comment Grade checking
        if(!$this->IS_ADMIN_USER($_SESSION['UserID']))
        {
            # Retrieve Display Settings
            $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
            $ClassLevelID = $ReportSetting['ClassLevelID'];

            # Retrieve Grading Scheme
            $GrandGradingSchemeArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID=-1, $withGrandResult = 1, $returnAsso = 0, $ReportID);
            $SchemeID = $GrandGradingSchemeArr['SchemeID'];
        }

        $dataArr = array();
        $CommentArr = $this->Get_Comment_In_Comment_Bank('');
        foreach((array)$CommentArr as $thisComment)
        {
            // SKIP - selected comments
            $thisCommentID = $thisComment['CommentID'];
            if(!empty($excludeCommentIDArr) && in_array($thisCommentID, (array)$excludeCommentIDArr)) {
                continue;
            }

            // Perform Comment Grade checking if not eReportCard admin
            $thisCommentGrade = $thisComment['RelatedGrade'];
            if(!$this->IS_ADMIN_USER($_SESSION['UserID']) && $thisCommentGrade != '' && $SchemeID > 0)
            {
                // Get Student Overall Result
                $thisStudentOverallResult = $this->getReportResultScore($ReportID, 0, $StudentID);
                $thisStudentGrandAverage = $thisStudentOverallResult['GrandAverage'];

                // Convert Grand Average to Grade for checking
                $thisStudentGrandAverageGrade = '';
                if($thisStudentGrandAverage != -1) {
                    $thisStudentGrandAverageGrade = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisStudentGrandAverage, $ReportID, $StudentID, $SubjectID=-1, $ClassLevelID);
                }

                // SKIP - not matched comments
                if($thisStudentGrandAverageGrade != $thisCommentGrade) {
                    continue;
                }
            }

            // Coment Info
            $thisCommentCode = $thisComment['CommentCode'];
            $thisCommentCH = $thisComment['Comment'];
            $thisCommentEN = $thisComment['CommentEng'];

            $dataArr[] = array($thisCommentID, '('.$thisCommentCode.') ');

            $thisCommentCH = explode("\n", $thisCommentCH);
            foreach((array)$thisCommentCH as $this_index => $thisDisplay) {
                if(trim($thisDisplay) != '') {
                    $dataArr[] = array($thisCommentID, $thisDisplay);
                }
            }

            $thisCommentEN = explode("\n", $thisCommentEN);
            foreach((array)$thisCommentEN as $this_index => $thisDisplay) {
                if(trim($thisDisplay) != '') {
                    $dataArr[] = array($thisCommentID, $thisDisplay);
                }
            }
        }
        */

        $x = '';
        $x .= '<br />';
        $x .= '<table class="form_table_v30">' . "\n";
            $x .= '<tr>' . "\n";
                $x .= '<td class="field_title">' . $Lang['Btn']['Search'] . '</td>' . "\n";
                $x .= '<td>' . "\n";
                    $x .= '<input type="text" id="commentSearchInput" name="commentSearchInput" class="CommentSearchTb" onkeydown="js_OnKeyPress_AutoFilter()" value="" />';
                $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
                $x .= '<td class="field_title">' . $eReportCard['CommentList']. '</td>' . "\n";
                $x .= '<td id="commentDivTd">' . "\n";
                    // $x .= $linterface->GET_SELECTION_BOX($dataArr, 'name="newCommentID[]" id="newCommentID" size="21" multiple="multiple" onchange="" style="display:block"', '');
                    // $x .= $linterface->Get_Form_Warning_Msg('CommentEmptyWarningDiv', $eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectAtLeastOneComment'], $Class = 'WarningDiv');
                    $x .= $this->Get_Comment_Div_Selection($ReportID, $StudentID, $excludeCommentIDArr);
                $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= $linterface->MultiSelectionRemark();

        $x .= '<br />';
        $x .= '<div class="edit_bottom_v30">';
            $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Add_Student_Comment();") . "\n";
            $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();") . "\n";
        $x .= '</div>';

        $x .= '<input type="hidden" id="StudentID" name="StudentID" value="' . $StudentID . '">' . "\n";
        $x .= '<input type="hidden" id="curReportID" name="curReportID" value="' . $ReportID . '">' . "\n";
        $x .= '<input type="hidden" id="excludeCommentIDArr" name="excludeCommentIDArr" value="' . implode(',', (array)$excludeCommentIDArr) . '">' . "\n";

        return $x;
    }

    public function Get_Comment_Div_Selection($ReportID, $StudentID, $excludeCommentIDArr, $SearchText='')
    {
        global $eReportCard, $linterface, $eRCTemplateSetting;

        // Get Grading Scheme for Comment Grade checking
        if(!$this->IS_ADMIN_USER($_SESSION['UserID']))
        {
            /*
            # Retrieve Display Settings
            $ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
            $ClassLevelID = $ReportSetting['ClassLevelID'];

            # Retrieve Grading Scheme
            $GrandGradingSchemeArr = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID=-1, $withGrandResult = 1, $returnAsso = 0, $ReportID);
            $SchemeID = $GrandGradingSchemeArr['SchemeID'];
            */

            # [2019-0923-1726-31289] Retrieve Conduct Grade
            $thisConductGradeInfo = $this->Get_Student_Conduct_Grade($ReportID, $StudentID);
            if(!empty($thisConductGradeInfo)) {
                $thisConductGrade = $studentConductGrade[0]['FinalGrade'];
            }
        }

        $dataArr = array();
        $CommentArr = $this->Get_Comment_In_Comment_Bank('', $SearchText);
        foreach((array)$CommentArr as $thisComment)
        {
            // SKIP - selected comments
            $thisCommentID = $thisComment['CommentID'];
            if(!empty($excludeCommentIDArr) && in_array($thisCommentID, (array)$excludeCommentIDArr)) {
                continue;
            }

            // Perform Comment Grade checking if not eReportCard admin
            $thisCommentGrade = $thisComment['RelatedGrade'];

            /*
             *  temp disabled
            // [2019-0923-1726-31289] Filter Comments by Conduct Grade if not eReportCard admin
            if(!$this->IS_ADMIN_USER($_SESSION['UserID']))
            {
                // e.g. Conduct Grade - B+ / B / B- > Comment Grade Setting - B
                if(in_array($thisCommentGrade, (array)$eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeList']) && isset($thisConductGrade) && strpos($thisConductGrade, $thisCommentGrade) !== false) {
                    // do nothing
                }
                else {
                    continue;
                }
            }
            */

            // Coment Info
            $thisCommentCode = $thisComment['CommentCode'];
            $thisCommentCH = $thisComment['Comment'];
            $thisCommentEN = $thisComment['CommentEng'];

            $dataArr[] = array($thisCommentID, '('.$thisCommentCode.') ');

            $thisCommentCH = explode("\n", $thisCommentCH);
            foreach((array)$thisCommentCH as $this_index => $thisDisplay) {
                if(trim($thisDisplay) != '') {
                    $dataArr[] = array($thisCommentID, $thisDisplay);
                }
            }

            $thisCommentEN = explode("\n", $thisCommentEN);
            foreach((array)$thisCommentEN as $this_index => $thisDisplay) {
                if(trim($thisDisplay) != '') {
                    $dataArr[] = array($thisCommentID, $thisDisplay);
                }
            }
        }

        $commentDiv = '';
        $commentDiv .= $linterface->GET_SELECTION_BOX($dataArr, 'name="newCommentID[]" id="newCommentID" size="21" multiple="multiple" onchange="" style="display:block"', '');
        $commentDiv .= $linterface->Get_Form_Warning_Msg('CommentEmptyWarningDiv', $eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectAtLeastOneComment'], $Class = 'WarningDiv');
        return $commentDiv;
    }

    public function Get_Student_New_Comment_Div_UI($StudentID, $CommentIDArr)
    {
        global $Lang, $eReportCard, $linterface;

        $returnStr = '';
        foreach((array)$CommentIDArr as $thisCommentID)
        {
            // Delete button for Comment Div
            $thisDeleteButton = $linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['Btn']['Delete'], "js_Delete_Comment('".$StudentID."', '".$thisCommentID."')", $ParClass = "", $WithSpan = 1);

            $thisCommentInfo = $this->GET_MARKSHEET_COMMENT_INFO_BY_ID($thisCommentID);
            $thisCommentTextCh = $thisCommentInfo['Comment'];
            $thisCommentTextEn = $thisCommentInfo['CommentEng'];
            $thisCommentTextDisplay = nl2br($thisCommentTextCh).'<br/>'.nl2br($thisCommentTextEn);

            // Comment Div  (Display Comment + Delete button + Hidden fields for CommentID)
            $returnStr .= '<div id="StudentCommentDiv_'.$StudentID.'_'.$thisCommentID.'" style="border:1px dotted grey; margin:5px; padding:2px">' . "\n";
                $returnStr .= '<span style="float:left; min-width:200px">' . $thisCommentTextDisplay. '&nbsp;&nbsp;</span>' . $thisDeleteButton . "\n";
                $returnStr .= '<br style="clear:both;" />' . "\n";
                $returnStr .= '<input type="hidden" name="CommentIDList['.$StudentID.'][]" value="'.$thisCommentID.'"/>' . "\n";
            $returnStr.= '</div>' . "\n";
        }

        return $returnStr;
    }

    public function Get_Edit_Award_Student_Table_By_Generated_Result($ReportID, $ViewMode, $YearClassID, $AwardName, $AwardID, $SubjectID)
    {
        global $Lang, $lreportcard_award, $linterface, $eRCTemplateSetting, $eReportCard;
        global $PATH_WRT_ROOT, $ReportCardCustomSchoolName;

        $AwardInfoArr = $lreportcard_award->Get_Award_Info($AwardID);
        $AwardType = $AwardInfoArr['AwardType'];

        // ## Get Form Student Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $SemID = $ReportInfoArr['Semester'];
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];
        $StudentInfoAssoArr = $this->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $YearClassID, $ParStudentIDList = "", $ReturnAsso = 1, $isShowStyle = 1);
        $StudentIDArr = array_keys($StudentInfoAssoArr);

        // [2016-0526-1018-39096]
        if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade']) {
            include_once ($PATH_WRT_ROOT . 'includes/libreportcard2008_extrainfo.php');
            $lreportcard_extrainfo = new libreportcard_extrainfo();

            // Conduct List
            $ConductArr = $lreportcard_extrainfo->Get_Conduct();
            $ConductArr = BuildMultiKeyAssoc($ConductArr, "ConductID", "Conduct", 1);

            // Get Conduct
            if ($SemID == "F") {
                include_once ($PATH_WRT_ROOT . "includes/reportcard_custom/" . $ReportCardCustomSchoolName . ".php");
                $lreportcard_cust = new libreportcardcustom();

                // loop semester
                $SemesterList = getSemesters($this->schoolYearID, 0);
                for ($j = 0; $j < 3; $j ++) {
                    // Get Semester Report Info
                    $thisSemester = $SemesterList[$j]["YearTermID"];
                    $thisReport = $this->returnReportTemplateBasicInfo("", "Semester='" . $thisSemester . "' and ClassLevelID = '" . $ClassLevelID . "' ");
                    $thisReportID = $thisReport["ReportID"];

                    // Get Conduct
                    $StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($thisReportID, $StudentIDArr);
                    $StudentSemConduct = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
                    $StudentConduct[$j] = $StudentSemConduct;
                }
            } else {
                $StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($ReportID, $StudentIDArr);
                $StudentConduct = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
            }

            // for Award View, check if can add award for student
            if ($ViewMode == 'Award') {
                $failConductStudentAry = array();
                $numOfTotalStudent = count($StudentIDArr);
                for ($i = 0; $i < $numOfTotalStudent; $i ++) {
                    $thisStudentID = $StudentIDArr[$i];

                    // Get Conduct
                    $thisStudentConductGrade = "";
                    if ($SemID == "F") {
                        // loop semester
                        $ConductDataAry = array();
                        for ($j = 0; $j < 3; $j ++) {
                            $thisStudentConductID = $StudentConduct[$j][$thisStudentID];
                            $ConductDataAry[$j] = $ConductArr[$thisStudentConductID];
                        }
                        $ConductDataAry = array_filter((array) $ConductDataAry);

                        // get overall conduct grade
                        if (! empty($ConductDataAry) && count($ConductDataAry) == 3)
                            $thisStudentConductGrade = $lreportcard_cust->calculateOverallConduct($ConductDataAry, $ConductArr);
                    } else {
                        $thisStudentConductID = $StudentConduct[$thisStudentID];
                        $thisStudentConductGrade = $ConductArr[$thisStudentConductID];
                    }
                    $allowStudentAddAward = $thisStudentConductGrade != "" && (strpos($thisStudentConductGrade, "A") !== false || (strpos($thisStudentConductGrade, "B") !== false && $thisStudentConductGrade != "B-"));

                    if (! $allowStudentAddAward)
                        $failConductStudentAry[] = $thisStudentID;
                }
            }
        }

        // ## Get the Display Student
        $IncludeStudentIDArr = array();
        if ($ViewMode == 'Class') {
            $IncludeStudentIDArr = array_keys($StudentInfoAssoArr);
            $NoRecordWarning = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoStudentInClass'];
        } else if ($ViewMode == 'Award') {
            if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
                $StudentAwardInfoArr = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr = '', $ReturnAsso = 0, $AwardNameWithSubject = 1, $AwardID, $SubjectID);
                $IncludeStudentIDArr = Get_Array_By_Key($StudentAwardInfoArr, 'StudentID');
                $StudentIDArr = $IncludeStudentIDArr;
            } else {
                $StudentAwardInfoArr = $lreportcard_award->Get_Student_With_AwardText($ReportID, $AwardName);
                $IncludeStudentIDArr = Get_Array_By_Key($StudentAwardInfoArr, 'StudentID');
            }

            // Count number of the estimated score
            $EstimatedMarkSubjectIDArr = array();
            $ComponentSubjectInfoArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang = '', $ParDisplayType = 'Desc', $ReportID, $ReturnAsso = 0);
            $numOfComponentSubject = count($ComponentSubjectInfoArr);
            if ($numOfComponentSubject > 0) {
                $EstimatedMarkSubjectIDArr = Get_Array_By_Key($ComponentSubjectInfoArr, 'SubjectID');
            } else {
                $EstimatedMarkSubjectIDArr[] = $SubjectID;
            }
            $numOfEsitmatedMarkSubject = count($EstimatedMarkSubjectIDArr);

            $ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
            $numOfReportColumn = count($ReportColumnInfoArr);
            for ($i = 0; $i < $numOfReportColumn; $i ++) {
                $thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];

                for ($j = 0; $j < $numOfEsitmatedMarkSubject; $j ++) {
                    $thisSubjectID = $EstimatedMarkSubjectIDArr[$j];

                    $SubjectMsScoreInfoArr[$thisReportColumnID][$thisSubjectID] = $this->GET_MARKSHEET_SCORE($StudentIDArr, $thisSubjectID, $thisReportColumnID);
                }
            }

            $StudentNumOfEstimatedScoreArr = array();
            $numOfStudent = count($IncludeStudentIDArr);
            for ($i = 0; $i < $numOfStudent; $i ++) {
                $thisStudentID = $IncludeStudentIDArr[$i];

                // Check Estimated Score
                for ($j = 0; $j < $numOfReportColumn; $j ++) {
                    $thisReportColumnID = $ReportColumnInfoArr[$j]['ReportColumnID'];

                    for ($k = 0; $k < $numOfEsitmatedMarkSubject; $k ++) {
                        $thisSubjectID = $EstimatedMarkSubjectIDArr[$k];

                        $thisIsEstimatedScore = $SubjectMsScoreInfoArr[$thisReportColumnID][$thisSubjectID][$thisStudentID]['IsEstimated'];
                        if ($thisIsEstimatedScore) {
                            $StudentNumOfEstimatedScoreArr[$thisStudentID] ++;
                        }
                    }
                }
            }

            $NoRecordWarning = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardedStudentSettings'];
        }
        $numOfTotalStudent = count($StudentIDArr);
        $numOfDisplayStudent = count($IncludeStudentIDArr);

        // ## Get Display Student Award Info
        // $StudentAwardInfoAssoArr[$StudentID][$AwardID][$SubjectID][Key] = Value;
        $StudentAwardInfoAssoArr = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $IncludeStudentIDArr);

        $x = '';
        $x .= '<table id="AwardStudentTable" class="common_table_list_v30 edit_table_list_v30">' . "\n";
        $x .= '<col style="width:5%;">' . "\n";
        $x .= '<col style="width:10%;">' . "\n";
        $x .= '<col style="width:10%;">' . "\n";
        $x .= '<col style="width:25%;">' . "\n";
        if ($ViewMode == 'Class') {
            $x .= '<col style="width:50%;">' . "\n";
        } else if ($ViewMode == 'Award') {
            if ($AwardType == 'SUBJECT') {
                $x .= '<col style="width:10%;">' . "\n";
            }
            $x .= '<col style="width:3%;">' . "\n";
            $x .= '<col style="width:3%;">' . "\n";
        }

        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        if ($ViewMode == 'Award') {
            $x .= '<th>' . $eReportCard['GeneralArr']['Ranking'] . '</th>' . "\n";
        } else if ($ViewMode == 'Class') {
            $x .= '<th>#</th>' . "\n";
        }
        $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</th>' . "\n";
        $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</th>' . "\n";
        $x .= '<th>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['StudentName'] . '</th>' . "\n";
        if ($ViewMode == 'Class') {
            $x .= '<th>' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'] . '</th>' . "\n";
        }
        if ($ViewMode == 'Award') {
            if ($AwardType == 'SUBJECT') {
                $x .= '<th>' . $Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['NoOfEstimatedScore'] . '</th>' . "\n";
            }
            $x .= '<th>&nbsp;</th>' . "\n";
            $x .= '<th>&nbsp;</th>' . "\n";
        }
        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";
        $x .= '<tbody>' . "\n";
        if ($numOfDisplayStudent == 0) {
            $x .= '<tr><td colspan="100%" style="text-align:center;">' . $NoRecordWarning . '</td></tr>' . "\n";
        } else {
            $StudentDisplayCount = 0;
            // foreach((array)$StudentInfoAssoArr as $thisStudentID => $thisStudentInfoArr) { // Loop $StudentInfoAssoArr to maintain Class and Class Number ordering
            for ($i = 0; $i < $numOfTotalStudent; $i ++) {
                $thisStudentID = $StudentIDArr[$i];

                if (! in_array($thisStudentID, $IncludeStudentIDArr)) {
                    continue;
                }

                $thisStudentInfoArr = $StudentInfoAssoArr[$thisStudentID];
                $thisClassName = Get_Lang_Selection($thisStudentInfoArr['ClassTitleCh'], $thisStudentInfoArr['ClassTitleEn']);
                $thisClassNumber = $thisStudentInfoArr['ClassNumber'];
                $thisStudentName = $thisStudentInfoArr['StudentName'];

                if ($ViewMode == 'Class') {
                    $thisNumberDisplay = ++ $StudentDisplayCount;
                } else if ($ViewMode == 'Award') {
                    $thisRecordID = $StudentAwardInfoAssoArr[$thisStudentID][$AwardID][$SubjectID]['RecordID'];
                    $thisNumberDisplay = $StudentAwardInfoAssoArr[$thisStudentID][$AwardID][$SubjectID]['AwardRank'];
                }

                // [2016-0526-1018-39096] for Class View, check if can add award for student
                $allowStudentAddAward = true;
                if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade'] && $ViewMode == 'Class') {
                    // Get Conduct
                    $thisStudentConductGrade = "";
                    if ($SemID == "F") {
                        // loop semester
                        $ConductDataAry = array();
                        for ($j = 0; $j < 3; $j ++) {
                            $thisStudentConductID = $StudentConduct[$j][$thisStudentID];
                            $ConductDataAry[$j] = $ConductArr[$thisStudentConductID];
                        }
                        $ConductDataAry = array_filter((array) $ConductDataAry);

                        // get overall conduct grade
                        if (! empty($ConductDataAry) && count($ConductDataAry) == 3)
                            $thisStudentConductGrade = $lreportcard_cust->calculateOverallConduct($ConductDataAry, $ConductArr);
                    } else {
                        $thisStudentConductID = $StudentConduct[$thisStudentID];
                        $thisStudentConductGrade = $ConductArr[$thisStudentConductID];
                    }
                    $allowStudentAddAward = $thisStudentConductGrade != "" && (strpos($thisStudentConductGrade, "A") !== false || (strpos($thisStudentConductGrade, "B") !== false && $thisStudentConductGrade != "B-"));
                }

                $x .= '<tr id="' . $thisRecordID . '">' . "\n";
                $x .= '<td><span class="RankingSpan">' . $thisNumberDisplay . '</span></td>' . "\n";
                $x .= '<td>' . $thisClassName . '</td>' . "\n";
                $x .= '<td>' . $thisClassNumber . '</td>' . "\n";
                $x .= '<td>' . $thisStudentName . '</td>' . "\n";
                if ($ViewMode == 'Class') {
                    $x .= '<td>' . "\n";
                    $x .= '<div id="StudentAwardDiv_' . $thisStudentID . '">' . "\n";
                    foreach ((array) $StudentAwardInfoAssoArr[$thisStudentID] as $thisAwardID => $thisAwardInfoArr) {
                        foreach ((array) $thisAwardInfoArr as $thisSubjectID => $thisSubjectAwardInfoArr) {
                            $thisRecordID = $thisSubjectAwardInfoArr['RecordID'];
                            $thisAwardName = $thisSubjectAwardInfoArr['AwardName'];

                            $thisDeleteButton = $linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DeleteAward'], "js_Delete_Award('" . $thisStudentID . "', '" . $thisRecordID . "')", $ParClass = "", $WithSpan = 1);

                            $x .= '<div id="GeneratedAwardDiv_' . $thisRecordID . '">' . "\n";
                            $x .= '<span style="float:left;">' . $thisAwardName . '&nbsp;&nbsp;</span>' . $thisDeleteButton . "\n";
                            $x .= '<br style="clear:both;" />' . "\n";
                            $x .= '</div>' . "\n";
                        }
                    }

                    // [2015-1104-1130-08164] hide add award icon if conduct grade received not high enough
                    if (! $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade'] || $allowStudentAddAward) {
                        $x .= '<span class="table_row_tool row_content_tool">' . "\n";
                        $x .= $linterface->Get_Thickbox_Link(450, 750, $ExtraClass = 'add_dim', $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AddAward'], "js_Show_Add_Student_Award_Layer('" . $thisStudentID . "');", $InlineID = "FakeLayer", $Content = "", $LinkID = '');
                        $x .= '</span>' . "\n";
                    }                    // Display warning message if conduct grade received not high enough
                    else if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade'] && ! $allowStudentAddAward) {
                        $x .= '<b><span  style="color:red;">' . "\n";
                        $x .= "- " . $Lang['eReportCard']['WarningArr']['ConductDisplay']['RejectAwardWhenFailConduct'];
                        $x .= '</span></b>' . "\n";
                    }

                    $x .= '</div>' . "\n";
                    $x .= '</td>' . "\n";
                }

                if ($ViewMode == 'Award') {
                    if ($AwardType == 'SUBJECT') {
                        // Number of estimated score
                        $thisNumOfEstimatedScore = $StudentNumOfEstimatedScoreArr[$thisStudentID];
                        $thisNumOfEstimatedScore = ($thisNumOfEstimatedScore == '') ? 0 : $thisNumOfEstimatedScore;
                        $x .= '<td>' . "\n";
                        $x .= $thisNumOfEstimatedScore;
                        $x .= '</td>' . "\n";
                    }

                    // Move Icon
                    $x .= '<td class="Dragable" align="center">' . "\n";
                    $x .= $linterface->GET_LNK_MOVE("javascript:void(0);", $Lang['Btn']['Move']) . "\n";
                    $x .= '</td>' . "\n";
                    // Remove Icon
                    $x .= '<td align="right">' . "\n";
                    $x .= $linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DeleteAward'], "js_Delete_Award('" . $thisStudentID . "', '" . $thisRecordID . "')", $ParClass = "", $WithSpan = 1);
                    $x .= '</td>' . "\n";
                }
                $x .= '</tr>' . "\n";
            }
        }

        if ($ViewMode == 'Award') {
            $x .= '<tr>' . "\n";
            $x .= '<td colspan="100%">' . "\n";
            $x .= '<span class="table_row_tool row_content_tool" style="float:right;">' . "\n";
            $x .= $linterface->Get_Thickbox_Link(450, 750, $ExtraClass = 'add', $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AddStudent'], "js_Show_Add_Award_Student_Layer('" . $AwardID . "', '" . $SubjectID . "');", $InlineID = "FakeLayer", $Content = "", $LinkID = '');
            $x .= '</span>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
        }

        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<input type="hidden" id="AwardedStudentIDList" name="AwardedStudentIDList" value="' . implode(',', (array) $StudentIDArr) . '" />' . "\n";

        // [2015-1104-1130-08164] store StudentID that conduct grade received not enough
        if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade'] && $ViewMode == 'Award') {
            $x .= '<input type="hidden" id="failCountStudentIDList" name="failCountStudentIDList" value="' . implode(',', (array) $failConductStudentAry) . '" />' . "\n";
        }

        return $x;
    }

    public function Get_Add_Student_Award_Layer_UI($StudentID, $ReportID)
    {
        global $linterface;

        $x = "";
        $x .= '<div id="debugArea"></div>';

        $x .= '<div class="edit_pop_board" style="height:410px;">';
        $x .= $linterface->Get_Thickbox_Return_Message_Layer();
        $x .= '<div id="LevelSettingLayer" class="edit_pop_board_write" style="height:370px;">';
        $x .= $this->Get_Add_Student_Award_Layer_Table($StudentID, $ReportID);
        $x .= '</div>';
        $x .= '</div>';

        return $x;
    }

    public function Get_Add_Student_Award_Layer_Table($StudentID, $ReportID)
    {
        global $Lang, $linterface, $lreportcard_award;

        $StudentInfoArr = $this->Get_Student_Class_ClassLevel_Info($StudentID);
        $ClassName = Get_Lang_Selection($StudentInfoArr[0]['ClassNameCh'], $StudentInfoArr[0]['ClassName']);
        $ClassNumber = $StudentInfoArr[0]['ClassNumber'];
        $StudentName = Get_Lang_Selection($StudentInfoArr[0]['ChineseName'], $StudentInfoArr[0]['EnglishName']);

        $SubjectAwardArr = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $StudentID, $ReturnAsso = 0);
        $numOfStudentAward = count($SubjectAwardArr);

        $ExcludeAwardIDArr = array();
        for ($i = 0; $i < $numOfStudentAward; $i ++) {
            $thisAwardID = $SubjectAwardArr[$i]['AwardID'];
            $thisAwardType = $SubjectAwardArr[$i]['AwardType'];

            if ($thisAwardType == 'OVERALL') {
                $ExcludeAwardIDArr[] = $thisAwardID;
            }
        }

        $x = '';
        $x .= '<table class="form_table_v30">' . "\n";
        // Class Name
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
        $x .= '<td>' . $ClassName . '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // Class No.
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</td>' . "\n";
        $x .= '<td>' . $ClassNumber . '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // Student Name
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['StudentName'] . '</td>' . "\n";
        $x .= '<td>' . $StudentName . '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // Award
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $this->Get_Award_Selection($ReportID, 'AwardID', $SelectedValue = '', $ShowHaveRankingAwardOnly = 0, $ExcludeAwardIDArr, $TargetAwardType = '', $OnChange = 'js_Changed_Award_Selection();', $WithAwardType = 1);
        $x .= $linterface->Get_Form_Warning_Msg('AwardEmptyWarningDiv', $Lang['eReportCard']['WarningArr']['Select']['Award'], $Class = 'WarningDiv');
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // Subject
        $x .= '<tr id="SubjectTr">' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['SubjectClassMapping']['Subject'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<div id="SubjectSelDiv"></div>' . "\n";
        $x .= $linterface->Get_Form_Warning_Msg('SubjectEmptyWarningDiv', $Lang['eReportCard']['WarningArr']['Select']['Subject'], $Class = 'WarningDiv');
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<br />';
        $x .= '<div class="edit_bottom_v30">';
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Add_Student_Award();") . "\n";
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'], "button", "js_Add_Student_Award(1);") . "\n";
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();") . "\n";
        $x .= '</div>';

        $x .= '<input type="hidden" id="StudentID" name="StudentID" value="' . $StudentID . '">' . "\n";

        return $x;
    }

    public function Get_Add_Award_Student_Layer_UI($ReportID, $AwardID, $SubjectID)
    {
        global $linterface;

        $x = "";
        $x .= '<div id="debugArea"></div>';

        $x .= '<div class="edit_pop_board" style="height:410px;">';
        $x .= $linterface->Get_Thickbox_Return_Message_Layer();
        $x .= '<div id="LevelSettingLayer" class="edit_pop_board_write" style="height:370px;">';
        $x .= $this->Get_Add_Award_Student_Layer_Table($ReportID, $AwardID, $SubjectID);
        $x .= '</div>';
        $x .= '</div>';

        return $x;
    }

    public function Get_Add_Award_Student_Layer_Table($ReportID, $AwardID, $SubjectID)
    {
        global $Lang, $linterface, $lreportcard_award;

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];

        $AwardInfoArr = $lreportcard_award->Get_Award_Info($AwardID);
        $AwardType = $AwardInfoArr['AwardType'];
        $AwardName = Get_Lang_Selection($AwardInfoArr['AwardNameCh'], $AwardInfoArr['AwardNameEn']);
        if ($AwardType == 'OVERALL') {
            $AwardNameDisplay = $AwardName;
        } else if ($AwardType == 'SUBJECT') {
            $SubjectName = $this->GET_SUBJECT_NAME_LANG($SubjectID);
            $AwardNameDisplay = $lreportcard_award->Get_Subject_Prize_Display($AwardName, $SubjectName);
        }

        $x = '';
        $x .= '<table class="form_table_v30">' . "\n";
        // Award
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'] . '</td>' . "\n";
        $x .= '<td>' . $AwardNameDisplay . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['Class'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $this->Get_Class_Selection('YearClassIDInThickBox', $ClassLevelID, $SelectYearClassID = '', 'js_Changed_Class_Selection(this.value);', $noFirst = 0, $isAll = 0, $checkPermission = 0, $IsMultiple = 0) . "\n";
        $x .= $linterface->Get_Form_Warning_Msg('ClassEmptyWarningDiv', $Lang['eReportCard']['WarningArr']['Select']['Class'], $Class = 'WarningDiv');
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr id="StudentSelTr" style="display:none;">' . "\n";
        $x .= '<td class="field_title">' . $Lang['SysMgr']['SubjectClassMapping']['ClassStudent'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<div id="StudentSelDiv"></div>' . "\n";
        $x .= $linterface->Get_Form_Warning_Msg('StudentEmptyWarningDiv', $Lang['eReportCard']['WarningArr']['Select']['AtLeastOneStudent'], $Class = 'WarningDiv');
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<br />';
        $x .= '<div class="edit_bottom_v30">';
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Add_Award_Student();") . "\n";
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit&AddMore'], "button", "js_Add_Award_Student(1);") . "\n";
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();") . "\n";
        $x .= '</div>';

        $x .= '<input type="hidden" id="AwardID" name="AwardID" value="' . $AwardID . '">' . "\n";
        $x .= '<input type="hidden" id="SubjectID" name="SubjectID" value="' . $SubjectID . '">' . "\n";

        return $x;
    }

    public function Get_Management_Progress_View_Mode_Button($ReportID, $CurrentMode)
    {
        global $Lang, $linterface;

        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $Semseter = $ReportInfoArr['Semester'];
        $ReportType = ($Semseter == 'F') ? 'W' : 'T';

        $pages_arr = array();

        // Subject Group View
        if ($ReportType == 'T') {
            $pages_arr[] = array(
                $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'],
                "javascript:js_Change_View_Mode('" . $ReportID . "', 'SubjectGroup');",
                '',
                $CurrentMode == 'SubjectGroup'
            );
        }

        // Class View
        $pages_arr[] = array(
            $Lang['SysMgr']['FormClassMapping']['Class'],
            "javascript:js_Change_View_Mode('" . $ReportID . "', 'Class');",
            '',
            $CurrentMode == 'Class'
        );

        return $linterface->GET_CONTENT_TOP_BTN($pages_arr);
    }

    public function Get_Management_Progress_Subject_Group_View_UI($ReportID, $ClassLevelID, $SubjectID)
    {
        global $linterface, $eReportCard, $Lang;

        // ## View Mode info
        $ViewModeBtn = $this->Get_Management_Progress_View_Mode_Button($ReportID, 'SubjectGroup');

        // ## Filtering
        $checkPermission = ($this->IS_ADMIN_USER($_SESSION['UserID'])) ? 0 : 1;
        $FormSelection = $this->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'document.form1.submit();', $noFirst = 1, $isAll = 0, $hasTemplateOnly = 1, $checkPermission);
        $ReportSelection = $this->Get_Report_Selection($ClassLevelID, $ReportID, 'ReportID', 'document.form1.submit();', $ForVerification = 0, $ForSubmission = 1);

        $TeachingOnly = ($this->IS_ADMIN_USER($_SESSION['UserID'])) ? 0 : 1;
        $SubjectSelection = $this->Get_Subject_Selection($ClassLevelID, $SubjectID, 'SubjectID', 'document.form1.submit();', $isMultiple = 0, $includeComponent = 0, $InputMarkOnly = 0, $includeGrandMarks = 0, $otherTagInfo = '', $ReportID, $ParentSubjectAsOptGroup = 0, $ExcludeWithoutSubjectGroup = 0, $TeachingOnly, $ExcludeSubjectIDArr = '', Get_Selection_First_Title($eReportCard['AllSubjects']));

        $x = '';
        $x .= '<br />' . "\n";
        $x .= '<form id="form1" name="form1" method="post" action="submission_sg.php">' . "\n";
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<div style="float:right;">' . "\n";
        $x .= $ViewModeBtn;
        $x .= '</div>' . "\n";
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div style="float:left;">' . "\n";
        $x .= $FormSelection . "\n";
        $x .= $ReportSelection . "\n";
        $x .= $SubjectSelection . "\n";
        $x .= '<br />' . "\n";
        $x .= $this->Get_Report_Submission_Period_Display($ReportID);
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<br />' . "\n";
        $x .= $this->Get_Management_Progress_Subject_Group_View_Table($ReportID, $ClassLevelID, $SubjectID);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<div id="FloatLayer" class="selectbox_layer" style="visibility:hidden; width:200px;">' . "\n";
        $x .= '<div style="float:right;"><a href="javascript:void(0);" class="tablelink" onclick="js_Hide_Layer(\'FloatLayer\');"> ' . $Lang['Btn']['Close'] . '</a></div>' . "\n";
        $x .= '<br style="clear:both;" />' . "\n";
        $x .= '<div id="FloatLayerContent"></div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        return $x;
    }

    private function Get_Management_Progress_Subject_Group_View_Table($ReportID, $ClassLevelID, $SubjectID)
    {
        global $PATH_WRT_ROOT, $Lang, $eReportCard;

        include_once ($PATH_WRT_ROOT . 'includes/subject_class_mapping.php');
        $libscm = new subject_class_mapping();

        // ## Get Report
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $YearTermID = $ReportInfoArr['YearTermID'];

        // ## Get Report Column
        $ReportColumnInfoArr = $this->returnReportTemplateColumnData($ReportID);
        $ReportColumnIDArr = Get_Array_By_Key($ReportColumnInfoArr, 'ReportColumnID');
        $ReportColumnIDList = implode(',', $ReportColumnIDArr);

        // ## Get Subject
        $SubjectInfoArr = $this->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection = 0, $MainSubjectOnly = 0, $ReportID);
        $SubjectIDArr = array_keys($SubjectInfoArr);
        $CmpSubjectInfoArr = $this->GET_COMPONENT_SUBJECT($SubjectIDArr, $ClassLevelID, $ParLang = '', $ParDisplayType = 'Desc', $ReportID, $ReturnAsso = 1);

        // ## Get Subject Group Info
        $SubjectGroupInfoArr = $this->Get_Subject_Group_List_Of_Report($ReportID, $ClassID = '', $SubjectID, $IncludeSubject = true);
        $SubjectGroupIDArr = $this->Get_Subject_Group_List_Of_Report($ReportID, $ClassID = '', $SubjectID, $IncludeSubject = false);
        $SubjectGroupInfoAssoArr = $libscm->Get_Subject_Group_Info_By_SubjectGroupID($SubjectGroupIDArr);
        $numOfSubjectGroup = count($SubjectGroupInfoAssoArr);

        // ## Get Marksheet Progress
        $MarksheetProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS('', $ReportID, '', $SubjectGroupIDArr, $ParReturnAsso = 1);

        // ## Get Marksheet Last Modified Info
        $LastModifiedArr = $this->GET_SUBJECT_GROUP_MARKSHEET_LAST_MODIFIED($ParSubjectID = '', $ReportColumnIDList, '', $YearTermID); // Must set $ParSubjectID='' otherwise will miss the $SubjectID key in the array

        $x = '';
        $x .= '<table id="ContentTable" class="common_table_list_v30">' . "\n";
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th style="width:3%;">#</th>' . "\n";
        $x .= '<th style="width:50%;">' . $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'] . '</th>' . "\n";
        $x .= '<th style="width:22%;">' . $Lang['General']['Status'] . '</th>' . "\n";
        $x .= '<th style="width:25%;">' . $eReportCard['LastModifiedBy'] . '</th>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '</thead>' . "\n";

        $x .= '<tbody>' . "\n";
        if ($numOfSubjectGroup == 0) {
            $x .= '<tr><td colspan="100%" style="text-align:center;">' . $eReportCard['NoSubjectGroup'] . '</td></tr>' . "\n";
        } else {
            $DisplayCounter = 0;
            foreach ((array) $SubjectInfoArr as $thisSubjectID => $thisSubjectName) {
                $thisSubjectGroupArr = $SubjectGroupInfoArr[$thisSubjectID];
                $thisNumOfSubjectGroup = count((array) $thisSubjectGroupArr);

                $thisCmpSubjectArr = $CmpSubjectInfoArr[$thisSubjectID];
                $thisCmpSubjectIDArr = Get_Array_By_Key($thisCmpSubjectArr, 'SubjectID');
                $thisIsParentSubject = (count((array) $thisCmpSubjectArr) > 0) ? true : false;

                for ($i = 0; $i < $thisNumOfSubjectGroup; $i ++) {
                    $thisSubjectGroupID = $thisSubjectGroupArr[$i]['SubjectGroupID'];
                    $thisSubjectGroupName = Get_Lang_Selection($SubjectGroupInfoAssoArr[$thisSubjectGroupID]['SubjectGroupNameCh'], $SubjectGroupInfoAssoArr[$thisSubjectGroupID]['SubjectGroupNameEn']);
                    $thisProgressStatus = $MarksheetProgressArr[$thisSubjectGroupID]['IsCompleted'];

                    $thisParentLastModifiedArr = array();
                    if ($thisIsParentSubject) {
                        $thisParentLastModifiedArr = $this->GET_SUBJECT_GROUP_PARENT_MARKSHEET_LAST_MODIFIED($thisSubjectID, $thisCmpSubjectIDArr, $ReportColumnIDList, $thisSubjectGroupID, $YearTermID);
                    }
                    $thisCmpSubjectModified = (count((array) $thisParentLastModifiedArr) > 0) ? true : false;

                    // ## Last Modified
                    $thisLastModifiedDisplay = '';
                    if ($thisIsParentSubject && $thisCmpSubjectModified) {
                        // Get Component Subject Last Modified for Parent Subject
                        $thisLastModifiedDate = $thisParentLastModifiedArr[$thisSubjectGroupID][0];
                        $thisLastModifiedBy = $thisParentLastModifiedArr[$thisSubjectGroupID][1];
                        $thisLastModifiedDisplay = $thisLastModifiedBy . '<br />' . $thisLastModifiedDate;
                    } else if (! empty($LastModifiedArr[$thisSubjectID][$thisSubjectGroupID])) {
                        $thisLastModifiedDate = $LastModifiedArr[$thisSubjectID][$thisSubjectGroupID][0];
                        $thisLastModifiedBy = $LastModifiedArr[$thisSubjectID][$thisSubjectGroupID][1];
                        $thisLastModifiedDisplay = $thisLastModifiedBy . '<br />' . $thisLastModifiedDate;
                    }
                    $thisLastModifiedDisplay = (trim($thisLastModifiedDisplay) == '') ? $Lang['General']['EmptySymbol'] : trim($thisLastModifiedDisplay);

                    if ($thisProgressStatus == 1) { // completed
                        $thisProgressDisplay = $Lang['General']['Completed'];
                        $thisProgressStyle = 'background-color:#AADC74;';
                    } else if ($thisLastModifiedDate) { // in progress
                        $thisProgressDisplay = $eReportCard['InProgress'];
                        $thisProgressStyle = 'background-color:#FFF475;';
                    } else { // not started
                        $thisProgressDisplay = $eReportCard['NotStartedYet'];
                        $thisProgressStyle = '';
                    }

                    $thisLinkID = 'SubjectGroupLink_' . $thisSubjectGroupID;
                    $x .= '<tr>' . "\n";
                    $x .= '<td style="' . $thisProgressStyle . '">' . (++ $DisplayCounter) . '</td>' . "\n";
                    $x .= '<td style="' . $thisProgressStyle . '"><a id="' . $thisLinkID . '" class="tablelink" href="javascript:void(0);" onclick="js_Show_Layer(\'' . $thisSubjectGroupID . '\', \'' . $thisLinkID . '\', \'FloatLayer\', \'FloatLayerContent\');">' . $thisSubjectGroupName . '</a></td>' . "\n";
                    $x .= '<td style="' . $thisProgressStyle . '">' . $thisProgressDisplay . '</td>' . "\n";
                    $x .= '<td style="' . $thisProgressStyle . '">' . $thisLastModifiedDisplay . '</td>' . "\n";
                    $x .= '<tr>' . "\n";
                }
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        return $x;
    }

    public function Get_Management_Progress_Subject_Group_Shortcut_Layer($ReportID, $SubjectGroupID)
    {
        global $PATH_WRT_ROOT, $eReportCard, $LAYOUT_SKIN;

        include_once ($PATH_WRT_ROOT . 'includes/subject_class_mapping.php');

        // ## Get Report Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ClassLevelID = $ReportInfoArr['ClassLevelID'];

        // ## Get Subject Info
        $objSubjectGroup = new subject_term_class($SubjectGroupID);
        $SubjectID = $objSubjectGroup->SubjectID;
        $CmpSubjectInfoArr = $this->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, $ParLang = '', $ParDisplayType = 'Desc', $ReportID, $ReturnAsso = 1);
        $IsParentSubject = (count($CmpSubjectInfoArr) > 0) ? true : false;

        // ## Get Marksheet Progress
        $MarksheetProgressArr = $this->GET_MARKSHEET_SUBMISSION_PROGRESS('', $ReportID, '', $SubjectGroupID, $ParReturnAsso = 1);
        $IsCompleted = $MarksheetProgressArr[$SubjectGroupID]['IsCompleted'];

        // ## Build the link
        $MarksheetUrl = ($IsParentSubject) ? 'marksheet_cmp_edit.php' : 'marksheet_edit.php';

        // ## Build the parameter
        $LinkParaArr = array();
        $LinkParaAssoArr = array();
        $LinkParaAssoArr['ClassLevelID'] = $ClassLevelID;
        $LinkParaAssoArr['ReportID'] = $ReportID;
        $LinkParaAssoArr['SubjectID'] = $SubjectID;
        $LinkParaAssoArr['SubjectGroupID'] = $SubjectGroupID;
        $LinkParaAssoArr['isProgressSG'] = 1;
        $LinkParaAssoArr['redirectTo'] = 'submissionSG';
        $LinkPara = getUrlParaByAssoAry($LinkParaAssoArr);

        $x = '';
        $x .= '<table cellspacing="0" cellpadding="3" border="0">' . "\r\n";
        $x .= '<tbody>' . "\r\n";
        $x .= '<tr id="editMarksheetTr">' . "\r\n";
        $x .= '<td>' . "\r\n";
        // marksheet
        $x .= '<a href="../marksheet_revision/' . $MarksheetUrl . '?' . $LinkPara . '" class="contenttool">' . "\r\n";
        $x .= '<img width="20" height="20" border="0" align="absmiddle" src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/icon_edit_b.gif" /> ' . $eReportCard['Marksheet'] . "\r\n";
        $x .= '</a>' . "\r\n";
        $x .= '<br />' . "\r\n";

        // subject teacher comment
        $x .= '<a href="../marksheet_revision/teacher_comment.php?' . $LinkPara . '" class="contenttool">' . "\r\n";
        $x .= '<img width="20" height="20" border="0" align="absmiddle" src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/icon_edit_b.gif" /> ' . $eReportCard['TeacherComment'] . "\r\n";
        $x .= '</a>' . "\r\n";
        $x .= '<br />' . "\r\n";

        if ($IsCompleted) {
            // set as incomplete
            $LinkParaAssoArr['complete_' . $SubjectID . '_' . $SubjectGroupID] = 0;
            $LinkParaAssoArr['SubjectGroupIDList[]'] = $SubjectGroupID;
            $LinkParaAssoArr['SubjectIDList[]'] = $SubjectID;
            $LinkPara = getUrlParaByAssoAry($LinkParaAssoArr);

            $x .= '<a href="../marksheet_revision/marksheet_complete_confirm_subject_group.php?' . $LinkPara . '" class="contenttool">' . "\r\n";
            $x .= '<img width="20" height="20" border="0" align="absmiddle" src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/icon_undo_b.gif"> ' . $eReportCard['SetToNotComplete'] . "\r\n";
            $x .= '</a>' . "\r\n";
        } else {
            // set as complete
            $LinkParaAssoArr['complete_' . $SubjectID . '_' . $SubjectGroupID] = 1;
            $LinkParaAssoArr['SubjectGroupIDList[]'] = $SubjectGroupID;
            $LinkParaAssoArr['SubjectIDList[]'] = $SubjectID;
            $LinkPara = getUrlParaByAssoAry($LinkParaAssoArr);

            $x .= '<a href="../marksheet_revision/marksheet_complete_confirm_subject_group.php?' . $LinkPara . '" class="contenttool">' . "\r\n";
            $x .= '<img width="20" height="20" border="0" align="absmiddle" src="' . $PATH_WRT_ROOT . 'images/' . $LAYOUT_SKIN . '/icon_tick_green.gif"> ' . $eReportCard['SetToComplete'] . "\r\n";
            $x .= '</a>' . "\r\n";
        }

        $x .= '</td>' . "\r\n";
        $x .= '</tr>' . "\r\n";
        $x .= '</tbody>' . "\r\n";
        $x .= '</table>' . "\r\n";

        return $x;
    }

    public function Get_Report_Submission_Period_Display($ReportID)
    {
        global $eReportCard, $Lang;

        $scheduleEntries = $this->GET_REPORT_DATE_PERIOD("", $ReportID);
        $subStart = (is_date_empty($scheduleEntries[0]["SubStart"])) ? $Lang['General']['Empty'] : date("Y-m-d H:i:s", $scheduleEntries[0]["SubStart"]);
        $subEnd = (is_date_empty($scheduleEntries[0]["SubEnd"])) ? $Lang['General']['Empty'] : date("Y-m-d H:i:s", $scheduleEntries[0]["SubEnd"]);

        return '<b>' . $eReportCard['SubmissionStartDate'] . ':</b> ' . $subStart . ' &nbsp;&nbsp; <b>' . $eReportCard['SubmissionEndDate'] . ':</b> ' . $subEnd;
    }

    public function Get_Settings_PersonalCharacteristics_Tag_Array($CurrentTab)
    {
        global $eReportCard, $eRCTemplateSetting;

        $tagArr = array();
        $tagArr[] = array(
            $eReportCard['PersonalCharacteristicsPool'],
            "index.php",
            ($CurrentTab == 'pool')
        );
        $tagArr[] = array(
            $eReportCard['PersonalCharacteristicsFormSettings'],
            "assign.php",
            ($CurrentTab == 'formSettings')
        );

        if ($eRCTemplateSetting['PersonalCharacteristicScaleSettings']) {
            $tagArr[] = array(
                $eReportCard['Scale'],
                "scale.php",
                ($CurrentTab == 'scale')
            );
        }

        return $tagArr;
    }

    public function Get_Settings_PersonalCharacteristics_Scale_UI()
    {
        global $linterface, $Lang;

        // ## Warning Table
        $UserSelectionRemarks = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['eReportCard']['PersonalCharArr']['WarningArr']['NoUserSelectionMeansAllCanEdit'], $others = "");

        $ActionBtnArr = array();
        $ActionBtnArr[] = array(
            'edit',
            'javascript:checkEdit(document.form1,\'ScaleIDArr[]\',\'scale_new_step1.php\')'
        );
        $ActionBtnArr[] = array(
            'delete',
            'javascript:checkRemove2(document.form1,\'ScaleIDArr[]\',\'goDeleteScale();\')'
        );

        $x = '';
        $x .= '<br />' . "\n";
        $x .= '<form id="form1" name="form1" method="get" action="scale.php">' . "\n";
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table id="html_body_frame" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $linterface->Get_Content_Tool_v30("new", "javascript:js_Go_New_Scale();") . "\n";
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr><td align="center">' . $UserSelectionRemarks . '</td></tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);
        $x .= $this->Get_Settings_PersonalCharacteristics_Scale_Table();
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</form>' . "\n";
        $x .= '<br />' . "\n";

        return $x;
    }

    public function Get_Settings_PersonalCharacteristics_Scale_Table()
    {
        global $Lang, $PATH_WRT_ROOT, $linterface;

        include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_pc.php");
        $lreportcard_pc = new libreportcard_pc();

        $ScaleInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale();
        $numOfScale = count($ScaleInfoArr);

        $ScaleMemberInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale_Mgmt_Group_Member();
        $ScaleMemberAssoArr = BuildMultiKeyAssoc($ScaleMemberInfoArr, 'ScaleID', array(), $SingleValue = 0, $BuildNumericArray = 1);

        $x = '';
        $x .= '<table id="ContentTable" class="common_table_list_v30">' . "\n";
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th style="width:3%;">#</th>' . "\n";
        $x .= '<th style="width:17%;">' . $Lang['General']['Code'] . '</th>' . "\n";
        $x .= '<th style="width:37%;">' . $Lang['General']['Name'] . '</th>' . "\n";
        $x .= '<th style="width:20%;">' . $Lang['eReportCard']['PersonalCharArr']['NoOfManagementUser'] . '</th>' . "\n";
        $x .= '<th style="width:15%;">' . $Lang['General']['Default'] . '</th>' . "\n";
        $x .= '<th style="width:5%;">&nbsp;</th>' . "\n";
        $x .= '<th style="width:3%;"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,\'ScaleIDArr[]\'):setChecked(0,this.form,\'ScaleIDArr[]\')" name="checkmaster"></th>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '</thead>' . "\n";

        $x .= '<tbody>' . "\n";
        if ($numOfScale == 0) {
            $x .= '<tr><td colspan="100%" style="text-align:center;">' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>' . "\n";
        } else {
            for ($i = 0; $i < $numOfScale; $i ++) {
                $thisScaleID = $ScaleInfoArr[$i]['ScaleID'];
                $thisCode = $ScaleInfoArr[$i]['Code'];
                $thisName = Get_Lang_Selection($ScaleInfoArr[$i]['NameCh'], $ScaleInfoArr[$i]['NameEn']);
                $thisDefault = ($ScaleInfoArr[$i]['IsDefault']) ? $Lang['General']['Yes'] : $Lang['General']['No'];

                $thisNumOfMgmtStaff = count((array) $ScaleMemberAssoArr[$thisScaleID]);
                $thisNumOfMgmtStaffLink = '<a href="scale_member_list.php?ScaleID=' . $thisScaleID . '" class="tablelink">' . $thisNumOfMgmtStaff . '</a>' . "\n";

                $x .= '<tr id="tr_' . $thisScaleID . '">' . "\n";
                $x .= '<td><span class="rowNumSpan">' . ($i + 1) . '</td>' . "\n";
                $x .= '<td>' . $thisCode . '</td>' . "\n";
                $x .= '<td>' . $thisName . '</td>' . "\n";
                $x .= '<td>' . $thisNumOfMgmtStaffLink . '</td>' . "\n";
                $x .= '<td>' . $thisDefault . '</td>' . "\n";
                $x .= '<td class="Dragable">' . "\n";
                $x .= $linterface->GET_LNK_MOVE("#", $Lang['Btn']['Move']) . "\n";
                $x .= '</td>' . "\n";
                $x .= '<td>' . "\n";
                $x .= '<input type="checkbox" class="ScaleChk" name="ScaleIDArr[]" scaleID="' . $thisScaleID . '" value="' . $thisScaleID . '">' . "\n";
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        return $x;
    }

    public function Get_Settings_PersonalCharacteristics_Scale_Step1_UI($ScaleID = '')
    {
        global $PATH_WRT_ROOT, $Lang, $eReportCard, $linterface;

        include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_pc.php");
        $lreportcard_pc = new libreportcard_pc();

        $isEdit = ($ScaleID == '') ? false : true;

        $PAGE_NAVIGATION[] = array(
            $eReportCard['Scale'],
            "javascript:js_Back_To_Scale_List()"
        );
        if ($isEdit) {
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['Edit'],
                ""
            );
            $SubmitBtnText = $Lang['Btn']['Submit'];

            $InfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale($ScaleID);
        } else {
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['New'],
                ""
            );
            $SubmitBtnText = $Lang['Btn']['Continue'];

            // ## Step Table
            $STEPS_OBJ[] = array(
                $Lang['eReportCard']['PersonalCharArr']['NewScaleStepArr'][1],
                1
            );
            $STEPS_OBJ[] = array(
                $Lang['eReportCard']['PersonalCharArr']['NewScaleStepArr'][2],
                0
            );
            $StepTable = $linterface->GET_STEPS_IP25($STEPS_OBJ);
        }

        // ## Navigation
        $PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

        // ## Get Default Scale Info
        $DefaultScaleInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale($ScaleIDArr = '', $CodeArr = '', $IsDefault = 1, $ScaleID);
        $HaveDefaultScale = (count($DefaultScaleInfoArr) > 0) ? true : false;

        $x = '';
        $x .= '<br />' . "\n";
        $x .= '<form id="form1" name="form1" method="post">' . "\n";

        // Navigation & Steps
        $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td align="left" class="navigation">' . $PageNavigation . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . $StepTable . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<table width="100%" cellpadding="2" class="form_table_v30">' . "\n";
        $x .= '<col class="field_title">';
        $x .= '<col class="field_c">';

        // Code
        $thisValue = intranet_htmlspecialchars($InfoArr[0]['Code']);
        $MaxLength = 64;
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['General']['Code'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<input name="Code" id="Code" class="textbox_name required" value="' . $thisValue . '" maxlength="' . $MaxLength . '">' . "\n";
        $x .= $linterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // English Name
        $thisValue = intranet_htmlspecialchars($InfoArr[0]['NameEn']);
        $MaxLength = 255;
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" rowspan="2"><span class="tabletextrequire">*</span>' . $Lang['General']['Name'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<span class="sub_row_content_v30">' . $Lang['General']['FormFieldLabel']['En'] . '</span>';
        $x .= '<span class="row_content_v30"><input name="NameEn" id="NameEn" class="textbox_name required" value="' . $thisValue . '"  maxlength="' . $MaxLength . '"></span>' . "\n";
        $x .= $linterface->Spacer();
        $x .= $linterface->Get_Thickbox_Warning_Msg_Div("NameEnWarningDiv");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // Chinese Name
        $thisValue = intranet_htmlspecialchars($InfoArr[0]['NameCh']);
        $MaxLength = 255;
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<span class="sub_row_content_v30">' . $Lang['General']['FormFieldLabel']['Ch'] . '</span>';
        $x .= '<span class="row_content_v30"><input name="NameCh" id="NameCh" class="textbox_name required" value="' . $thisValue . '"  maxlength="' . $MaxLength . '"></span>' . "\n";
        $x .= $linterface->Spacer();
        $x .= $linterface->Get_Thickbox_Warning_Msg_Div("NameChWarningDiv");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // Default
        $thisChecked = ($InfoArr[0]['IsDefault']) ? 'checked' : '';
        $thisDisabled = ($HaveDefaultScale && ! $InfoArr[0]['IsDefault']) ? 'disabled' : '';
        $thisRemarks = ($HaveDefaultScale) ? '<span class="tabletextremark">(' . $Lang['eReportCard']['GeneralArr']['WarningArr']['HaveScaleDefaultAlready'] . ')</span>' : '';
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['General']['SetAsDefault'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<input type="checkbox" name="IsDefault" id="IsDefault" value="1" ' . $thisChecked . ' ' . $thisDisabled . ' />' . "\n";
        $x .= $thisRemarks . "\n";
        $x .= $linterface->Get_Thickbox_Warning_Msg_Div("IsDefaultWarningDiv") . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<br style="clear:both;" />' . "\n";

        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= $linterface->GET_ACTION_BTN($SubmitBtnText, "button", $onclick = "js_Update_Scale_Info();", $id = "Btn_Submit") . "\n";
        $x .= '&nbsp;' . "\n";
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick = "js_Back_To_Scale_List()", $id = "Btn_Cancel") . "\n";
        $x .= '</div>' . "\n";

        $x .= '<input type="hidden" id="isEdit" name="isEdit" value="' . $isEdit . '" />' . "\n";
        $x .= '<input type="hidden" id="ScaleID" name="ScaleID" value="' . $ScaleID . '" />' . "\n";
        $x .= '</form>' . "\n";

        return $x;
    }

    function Get_Settings_PersonalCharacteristics_Scale_Step2_UI($ScaleID = '', $FromNew = 0)
    {
        global $Lang, $eReportCard, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN, $linterface;

        $PAGE_NAVIGATION[] = array(
            $eReportCard['Scale'],
            "javascript:js_Back_To_Scale_List()"
        );
        if ($FromNew == 1) {
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['New'],
                ""
            );

            // ## Step Table
            $STEPS_OBJ[] = array(
                $Lang['eReportCard']['PersonalCharArr']['NewScaleStepArr'][1],
                0
            );
            $STEPS_OBJ[] = array(
                $Lang['eReportCard']['PersonalCharArr']['NewScaleStepArr'][2],
                1
            );
            $StepTable = $linterface->GET_STEPS_IP25($STEPS_OBJ);

            // ## Cancel Button
            $btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick = "js_Back_To_Scale_List()", $id = "Btn_Cancel");
        } else {
            $PAGE_NAVIGATION[] = array(
                $Lang['eReportCard']['PersonalCharArr']['MemberList'],
                "javascript:js_Back_To_Member_List();"
            );
            $PAGE_NAVIGATION[] = array(
                $Lang['eReportCard']['PersonalCharArr']['AddMember'],
                ""
            );

            // ## Cancel Button
            $btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick = "js_Back_To_Member_List()", $id = "Btn_Cancel");
        }

        // ## Navigation
        $PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

        // ## Warning Table
        $UserSelectionRemarks = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['eReportCard']['PersonalCharArr']['WarningArr']['NoUserSelectionMeansAllCanEdit'], $others = "");

        // ## Choose Member Btn
        $permitted_type = USERTYPE_STAFF;
        $btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('" . $PATH_WRT_ROOT . "home/common_choose/index.php?fieldname=SelectedUserIDArr[]&page_title=SelectMembers&permitted_type=$permitted_type&DisplayGroupCategory=1', 9)");

        // ## Auto-complete login search
        $UserLoginInput = '';
        $UserLoginInput .= '<div style="float:left;">';
        $UserLoginInput .= '<input type="text" id="UserSearchTb" name="UserSearchTb" value=""/>';
        $UserLoginInput .= '</div>';

        // ## Member Selection Box & Remove all Btn
        $MemberSelectionBox = $linterface->GET_SELECTION_BOX(array(), 'name="SelectedUserIDArr[]" id="SelectedUserIDArr[]" class="select_studentlist" size="15" multiple="multiple"', "");
        $btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "javascript:checkOptionRemove(document.getElementById('SelectedUserIDArr[]'))");

        // ## Submit Button
        $btn_Submit = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick = "js_Add_Member();", $id = "Btn_Submit");

        $x = '';
        $x .= '<br />' . "\n";
        $x .= '<form id="form1" name="form1" method="post">' . "\n";

        // Navigation & Steps
        $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td align="left" class="navigation">' . $PageNavigation . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . $StepTable . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<div style="width:100%;" align="center">' . $UserSelectionRemarks . '</div>' . "\n";
        $x .= $this->Get_Settings_PersonalCharacteristics_Scale_Info_Table($ScaleID);
        $x .= '<br style="clear:both;" />' . "\n";

        // Add Member Info
        $x .= '<table width="99%" border="0" cellpadding"0" cellspacing="0" align="center">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext" width="40%">' . $Lang['General']['ChooseUser'] . '</td>' . "\n";
        $x .= '<td class="tabletext"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/10x10.gif" width="10" height="1"></td>' . "\n";
        $x .= '<td class="tabletext" width="60%">' . $Lang['General']['SelectedUser'] . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tablerow2" valign="top">' . "\n";
        $x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext">' . $Lang['General']['FromGroup'] . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext">' . $Lang['General']['ChooseMember'] . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext">' . $btn_ChooseMember . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext"><i>' . strtolower($Lang['General']['Or']) . '</i></td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="tabletext">' . "\n";
        $x .= $Lang['General']['SearchByLoginID'];
        $x .= '<br />' . "\n";
        $x .= $UserLoginInput;
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '<td class="tabletext" ><img src="' . $image_path . '/' . $LAYOUT_SKIN . '"/10x10.gif" width="10" height="1"></td>' . "\n";
        $x .= '<td align="left" valign="top">' . "\n";
        $x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td align="left">' . "\n";
        $x .= $MemberSelectionBox;
        $x .= $btn_RemoveSelected;
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", $Lang['eReportCard']['PersonalCharArr']['WarningArr']['SelectionContainsMember']);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr><td>&nbsp;</td></tr>' . "\n";
        $x .= '<tr><td colspan="4"><div class="edit_bottom"><br />' . $btn_Submit . '&nbsp;' . $btn_Cancel . '</div></td></tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<input type="hidden" id="ScaleID" name="ScaleID" value="' . $ScaleID . '" />' . "\n";
        $x .= '<input type="hidden" id="FromNew" name="FromNew" value="' . $FromNew . '" />' . "\n";
        $x .= '</form>' . "\n";

        return $x;
    }

    private function Get_Settings_PersonalCharacteristics_Scale_Info_Table($ScaleID)
    {
        global $Lang, $PATH_WRT_ROOT;

        include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_pc.php");
        $lreportcard_pc = new libreportcard_pc();

        $InfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale($ScaleID);

        $x = '';
        $x .= '<table width="100%" cellpadding="2" class="form_table_v30">' . "\n";
        $x .= '<col class="field_title">';
        $x .= '<col class="field_c">';
        // Code
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['Code'] . '</td>' . "\n";
        $x .= '<td>' . $InfoArr[0]['Code'] . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Name
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['Name'] . '</td>' . "\n";
        $x .= '<td>' . Get_Lang_Selection($InfoArr[0]['NameCh'], $InfoArr[0]['NameEn']) . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        return $x;
    }

    function Get_Settings_PersonalCharacteristics_Scale_Mgmt_User_UI($ScaleID, $page = '', $order = '', $field = '', $Keyword = '')
    {
        global $Lang, $eReportCard, $linterface;

        // Navigation
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Scale'],
            "javascript:js_Back_To_Scale_List();"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['eReportCard']['PersonalCharArr']['MemberList'],
            ""
        );
        $PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

        // ## Warning Table
        $UserSelectionRemarks = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['eReportCard']['PersonalCharArr']['WarningArr']['NoUserSelectionMeansAllCanEdit'], $others = "");

        // SearchBox
        // $SearchBox = $linterface->Get_Search_Box_Div('Keyword_Member_List', $Keyword);

        // Back Button
        $btn_Back = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick = "js_Back_To_Scale_List()", $id = "Btn_Back");

        $x = '';
        $x .= '<br />' . "\n";
        $x .= '<form id="form1" name="form1" method="post" action="scale_member_list.php">' . "\n";
        // Navigation & Steps
        $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td align="left" class="navigation">' . $PageNavigation . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= $this->Get_Settings_PersonalCharacteristics_Scale_Info_Table($ScaleID);
        $x .= '<br style="clear:both;" />' . "\n";

        $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";

        // Main Table
        $x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $linterface->GET_LNK_ADD("scale_new_step2.php?ScaleID=$ScaleID") . "\n";
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '<td align="right">' . $SearchBox . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr><td colspan="2" align="center">' . $UserSelectionRemarks . '</td></tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td colspan="2" align="right">' . "\n";
        $x .= '<div class="common_table_tool">' . "\n";
        $x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'UserIDArr[]\',\'scale_member_delete.php\')">' . $Lang['Btn']['Delete'] . '</a>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td colspan="2">' . "\n";
        $x .= '<div id="MemberDiv">' . $this->Get_Settings_PersonalCharacteristics_Scale_Mgmt_User_DBTable($ScaleID, $page, $order, $field, $Keyword) . '</div>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<div class="edit_bottom_v30">';
        $x .= $btn_Back;
        $x .= '</div>';

        $x .= '<input type="hidden" id="ScaleID" name="ScaleID" value="' . $ScaleID . '" />' . "\n";
        $x .= '</form>' . "\n";

        return $x;
    }

    public function Get_Settings_PersonalCharacteristics_Scale_Mgmt_User_DBTable($ScaleID, $page = '', $order = '', $field = '', $Keyword = '')
    {
        global $PATH_WRT_ROOT, $Lang, $page_size, $ck_settings_personal_char_mgmt_user_page_size;

        include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_pc.php");
        include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
        include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
        $lreportcard_pc = new libreportcard_pc();

        $field = ($field == '') ? 0 : $field;
        $order = ($order == '') ? 1 : $order;
        $page = ($page == '') ? 1 : $page;

        if (isset($ck_settings_personal_char_mgmt_user_page_size) && $ck_settings_personal_char_mgmt_user_page_size != "")
            $page_size = $ck_settings_personal_char_mgmt_user_page_size;
            $li = new libdbtable2007($field, $order, $page);

            $sql = $lreportcard_pc->Get_Personal_Characteristics_Scale_Mgmt_User_DBTable_Sql($ScaleID, $Keyword);

            $li->sql = $sql;
            $li->IsColOff = "IP25_table";
            $li->field_array = array(
                "iu.EnglishName",
                "iu.ChineseName"
            );
            $li->column_array = array(
                0,
                0,
                0,
                0
            );
            $li->wrap_array = array(
                0,
                0,
                0,
                0
            );

            $pos = 0;
            $li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
            $li->column_list .= "<th width='47%' >" . $li->column_IP25($pos ++, $Lang['General']['EnglishName']) . "</th>\n";
            $li->column_list .= "<th width='47%'>" . $li->column_IP25($pos ++, $Lang['General']['ChineseName']) . "</th>\n";
            $li->column_list .= "<th width='1'>" . $li->check("UserIDArr[]") . "</td>\n";
            $li->no_col = $pos + 2;

            $x .= $li->display();
            // debug_pr($li->built_sql());
            // debug_pr(mysql_error());
            $x .= '<input type="hidden" name="pageNo" value="' . $li->pageNo . '" />';
            $x .= '<input type="hidden" name="order" value="' . $li->order . '" />';
            $x .= '<input type="hidden" name="field" value="' . $li->field . '" />';
            $x .= '<input type="hidden" name="page_size_change" value="" />';
            $x .= '<input type="hidden" name="numPerPage" value="' . $li->page_size . '" />';

            return $x;
    }

    public function Get_Management_PersonalChar_Edit_UI($ReportID, $SubjectID, $SubjectGroupID, $ClassLevelID, $ClassID, $FromMS)
    {
        global $PATH_WRT_ROOT, $Lang, $eReportCard, $linterface, $eRCTemplateSetting, $ck_ReportCard_UserType, $ReportCardCustomSchoolName, $image_path, $LAYOUT_SKIN;

        include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_pc.php");
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        $lreportcard_pc = new libreportcard_pc();
        $lclass = new libclass();

        // ## Get Class Info
        $className = $lclass->getClassName($ClassID);
        $className = ($className == '') ? $Lang['General']['EmptySymbol'] : $className;

        // ## Get Subject Info
        if ($SubjectID == 0) {
            $SubjectName = $eReportCard['Template']['OverallCombined'];
        } else {
            $SubjectName = $this->GET_SUBJECT_NAME_LANG($SubjectID);
        }

        // ## Get Report Info
        $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
        $ReportTitle = str_replace(":_:", "<br>", $ReportInfoArr['ReportTitle']);

        // ## Get Student List
        if ($SubjectGroupID != '') {
            // Subject Group View
            $StudentInfoArr = $this->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1, 1);
            $showClassName = true;

            include_once ($PATH_WRT_ROOT . "includes/subject_class_mapping.php");
            $obj_SubjectGroup = new subject_term_class($SubjectGroupID);
            $SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
        } else {
            // Class View
            // Get all students from the class
            $StudentInfoArr = $this->GET_STUDENT_BY_CLASS($ClassID);

            $SubjectGroupName = $Lang['General']['EmptySymbol'];
        }
        $StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
        $numOfStudent = count($StudentInfoArr);

        // ## Get Personal Char Item Info
        $Data = array();
        $Data['SubjectID'] = $SubjectID;
        $Data['SubjectGroupID'] = $SubjectGroupID;
        $Data['ReportID'] = $ReportID;
        $Data['ClassLevelID'] = $ClassLevelID;
        $Data['ClassID'] = $ClassID;
        $CharInfoArr = $this->GET_CLASSLEVEL_CHARACTERISTICS($Data);
        $numOfChar = count($CharInfoArr);

        // ## Get Student Personal Char Info
        // $StudentPersonalCharAssoArr[$ReportID][$SubjectID][$StudentID]['comment' or $scaleID] = data
        $StudentPersonalCharAssoArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentIDArr, $ReportID, $SubjectID, $ReturnScaleID = 1, $ReturnCharID = 1, $ReturnComment = 1);

        // Added for Sha Tin Methodist College - Get Personal Char Info of previous term if empty
        if ($eRCTemplateSetting['Personal Characteristics']['DefaultFirstTermSettings'] && $StudentPersonalCharAssoArr == "") {
            // Get current term number
            $semester = $ReportInfoArr['Semester'];
            $term_num = $this->Get_Semester_Seq_Number($semester);

            // Get previous term ReportID if second term
            if ($term_num == 2) {
                include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
                $ay = new academic_year_term($semester, true);

                // Get first term YearTermID
                $sql = "Select
								YearTermID
						From
								ACADEMIC_YEAR_TERM
						Where
								TermEnd < '" . $ay->TermStart . "' AND AcademicYearID = '" . $ay->AcademicYearID . "'
						Order By
								TermEnd Desc";
                $firstTerm = $this->returnVector($sql);
                $firstTermID = $firstTerm[0];

                // Get first term ReportID
                if ($firstTermID) {
                    $FirstTermReportInfoArr = $this->returnReportTemplateBasicInfo('', '', $ClassLevelID, $firstTermID);
                    $firstTermReportID = $FirstTermReportInfoArr["ReportID"];

                    // Get Student Personal Char Info of first term
                    if ($firstTermReportID) {
                        $ScaleFistTermInfoArr = $this->getPersonalCharacteristicsProcessedDataByBatch($StudentIDArr, $firstTermReportID, $SubjectID, $ReturnScaleID = 1, $ReturnCharID = 1, $ReturnComment = 1);
                    }
                }
            }
        }

        // ## Get Scale Info
        $ScaleInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale();
        $ScaleIDArr = Get_Array_By_Key($ScaleInfoArr, 'ScaleID');
        $ScaleInfoAssoArr = BuildMultiKeyAssoc($ScaleInfoArr, 'ScaleID');
        $ScaleDefaultInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale('', '', $IsDefault = 1);

        // ## Get User Applicable Personal Char Scale
        $ApplicableScaleInfoArr = $lreportcard_pc->Get_User_Applicable_Personal_Characteristics_Scale($_SESSION['UserID']);
        $ApplicableScaleIDArr = Get_Array_By_Key($ApplicableScaleInfoArr, 'ScaleID');
        $numOfApplicableScale = count($ApplicableScaleIDArr);

        // ## Get Page View Mode
        $ViewOnly = 0;
        if ($numOfApplicableScale == 0) {
            $ViewOnly = 1;
        } else if ($eRCTemplateSetting['PersonalCharacteristicSubmissionSchedule'] && $ck_ReportCard_UserType != "ADMIN") {

            // $reportDateArr = $this->GET_REPORT_DATE_PERIOD($Condition="", $ReportID);
            // $PCStart = $reportDateArr[0]['PCStart'];
            // $PCEnd = $reportDateArr[0]['PCEnd']+(60*60*24); //inclusive
            // $PCEnd = $reportDateArr[0]['PCEnd']; //change to time limit
            $PeriodAction = "PCSubmission";
            $PeriodType = $this->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);

            // $ViewOnly = ($PCStart && $PCEnd && ($PCStart <= time() && $PCEnd >= time()))? 0 : 1;
            $ViewOnly = $PeriodType == 1 ? 0 : 1;
        }

        // ## Toolbar
        $Toolbar = '';
        $Para = "ClassLevelID=$ClassLevelID&ClassID=$ClassID&ReportID=$ReportID&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID&FromMS=$FromMS";
        if ($ViewOnly == 0) {
            // $Toolbar .= $linterface->GET_LNK_IMPORT("import.php?$import_parameters", '', '', '', '', 0);
            $Toolbar .= $linterface->Get_Content_Tool_v30("import", "import.php?$Para");
        }
        $ExportPara = $Para . "&FromExport=1";
        // $Toolbar .= $linterface->GET_LNK_EXPORT("export.php?$exportparameters", '', '', '', '', 0);
        $Toolbar .= $linterface->Get_Content_Tool_v30("export", "export.php?$ExportPara");

        if ($eRCTemplateSetting['PersonalChar']['PrintPage']) {
            $Toolbar .= $linterface->Get_Content_Tool_v30('print', 'javascript:js_Go_Print();');
        }

        // ## Get Redirection page
        if ($FromMS == 1) {
            $CancelPara = 'ReportID=' . $ReportID . '&ClassLevelID=' . $ClassLevelID . '&ClassID=' . $ClassID . '&SubjectID=' . $SubjectID . '&SubjectGroupID=' . $SubjectGroupID;
            if ($SubjectGroupID != '') {
                $thisCancelPath = '../marksheet_revision/marksheet_revision_subject_group.php?' . $CancelPara;
            } else {
                $thisCancelPath = '../marksheet_revision/marksheet_revision.php?' . $CancelPara;
            }
        } else {
            $thisCancelPath = 'index.php?ClassLevelID=' . $ClassLevelID . '&ReportID=' . $ReportID;
        }

        // ## Get Warning Msg
        if ($ReportCardCustomSchoolName == 'hkuga_college') {
            $Warningbox = $linterface->Get_Warning_Message_Box('<font color="red">' . strtoupper($Lang['General']['Important']) . '</font>', $eReportCard['Template']['PersonalCharacteristicsRemarks']);
        }

        $x = '';
        $x .= '<br />' . "\n";
        $x .= '<form id="form1" name="form1" method="post" action="edit_update.php">' . "\n";

        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $Toolbar . "\n";
        $x .= '</div>' . "\n";
        $x .= '<div class="Conntent_search">' . "\n";
        $x .= $SearchBox . "\n";
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<br style="clear: both;">' . "\n";

        $x .= '<div style="width:100%;" align="center">' . "\n";
        $x .= $Warningbox;
        $x .= '</div>' . "\n";
        $x .= '<br style="clear: both;">' . "\n";

        // ## Info Data
        $x .= '<!--PrintStart-->' . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<col class="field_title">';
        $x .= '<col class="field_c">';

        if ($SubjectGroupID == '') {
            // Class
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['General']['Class'] . '</td>' . "\n";
            $x .= '<td>' . $className . '</td>' . "\n";
            $x .= '</tr>' . "\n";
        }

        // Subject
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['Subject'] . '</td>' . "\n";
        $x .= '<td>' . $SubjectName . '</td>' . "\n";
        $x .= '</tr>' . "\n";

        if ($SubjectGroupID != '') {
            // Subject Group
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'] . '</td>' . "\n";
            $x .= '<td>' . $SubjectGroupName . '</td>' . "\n";
            $x .= '</tr>' . "\n";
        }

        // Report
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['Reports'] . '</td>' . "\n";
        $x .= '<td>' . $ReportTitle . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<br style="clear:both;" />' . "\n";

        // ## Edit Data Table
        $ClassWidth = 5;
        $ClassNoWidth = 5;
        $StudentNameWidth = 15;
        $CommentWidth = ($eRCTemplateSetting['PersonalCharacteristicComment']) ? 15 : 0;
        $PCWidth = ($numOfChar > 0) ? floor((100 - $ClassWidth - $ClassNoWidth - $StudentNameWidth - $CommentWidth) / $numOfChar) : 0;

        $x .= '<table id="DataTable" class="common_table_list_v30 edit_table_list_v30">' . "\n";
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th style="width:' . $ClassWidth . '%;">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</th>' . "\n";
        $x .= '<th style="width:' . $ClassNoWidth . '%;">' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</th>' . "\n";
        $x .= '<th style="width:' . $StudentNameWidth . '%;">' . $Lang['SysMgr']['FormClassMapping']['StudentName'] . '</th>' . "\n";
        for ($i = 0; $i < $numOfChar; $i ++) {
            $thisCharID = $CharInfoArr[$i]['CharID'];
            $thisCharName = Get_Lang_Selection($CharInfoArr[$i]['CharTitleCh'], $CharInfoArr[$i]['CharTitleEn']);

            $x .= '<th style="width:' . $PCWidth . '%;">' . $thisCharName . '</th>' . "\r\n";
            $x .= '<input type="hidden" name="CharID[]" value="' . $thisCharID . '" />' . "\r\n";
        }
        if ($eRCTemplateSetting['PersonalCharacteristicComment']) {
            $x .= '<th style="width:' . $CommentWidth . '%;">' . $Lang['eReportCard']['PersonalCharArr']['Comment'] . '</th>' . "\r\n";
        }
        // [2015-0421-1144-05164]
        if ($eRCTemplateSetting['PersonalChar']['ShowStudentDetails'] && count($numOfChar) > 0) {
            $x .= "<th width='1'>&nbsp;</td>";
        }
        $x .= '</tr>' . "\n";
        $x .= '</thead>' . "\n";
        $x .= '<tbody>' . "\n";

        $studentIdHiddenField = '';
        if ($numOfStudent == 0) {
            $x .= '<tr><td colspan="100%" style="text-align:center;">' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>' . "\n";
        } else {
            for ($i = 0; $i < $numOfStudent; $i ++) {
                $thisStudentID = $StudentInfoArr[$i]['UserID'];
                $thisClassName = $StudentInfoArr[$i]['ClassName'];
                $thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
                $thisStudentName = $StudentInfoArr[$i]['StudentName'];

                $studentIdHiddenField .= '<input type="hidden" name="StudentID[]" value="' . $thisStudentID . '" />' . "\n";

                $x .= '<tr>' . "\n";
                $x .= '<td>' . $thisClassName . '</td>' . "\n";
                $x .= '<td>' . $thisClassNumber . '</td>' . "\n";
                $x .= '<td>' . $thisStudentName . '</td>' . "\n";
                for ($j = 0; $j < $numOfChar; $j ++) {
                    $thisCharID = $CharInfoArr[$j]['CharID'];
                    $thisScaleID = $StudentPersonalCharAssoArr[$ReportID][$SubjectID][$thisStudentID][$thisCharID];
                    // $thisScaleID = ($thisScaleID=='' || !in_array($thisScaleID, (array)$ScaleIDArr))? $ScaleDefaultInfoArr[0]['ScaleID'] : $thisScaleID;

                    // first term scale
                    $fistTermScaleID = $ScaleFistTermInfoArr[$firstTermReportID][$SubjectID][$thisStudentID][$thisCharID];
                    // $previousScaleID = ($previousScaleID=='' || !in_array($previousScaleID, (array)$ScaleIDArr))? $ScaleDefaultInfoArr[0]['ScaleID'] : $previousScaleID;
                    // $thisScaleID = ($thisScaleID=='' || !in_array($thisScaleID, (array)$ScaleIDArr))? $previousScaleID : $thisScaleID;

                    // Set default scale if no existing data
                    if ($thisScaleID == '' || ! in_array($thisScaleID, (array) $ScaleIDArr)) {
                        // if no existing data => check have first term data or not
                        if ($fistTermScaleID == '' || ! in_array($fistTermScaleID, (array) $ScaleIDArr)) {
                            // if no first term data => use default data
                            $thisScaleID = $ScaleDefaultInfoArr[0]['ScaleID'];
                        }                        // if have first term data => use first term data
                        else {
                            $thisScaleID = $fistTermScaleID;
                        }
                    }

                    $thisScaleName = Get_Lang_Selection($ScaleInfoAssoArr[$thisScaleID]['NameCh'], $ScaleInfoAssoArr[$thisScaleID]['NameEn']);
                    $thisScaleName = ($thisScaleName == '') ? $Lang['General']['EmptySymbol'] : $thisScaleName;

                    // view only or the user has no permission for this scale => cannot edit
                    $thisViewOnly = ($ViewOnly || ($thisScaleID != '' && ! in_array($thisScaleID, (array) $ApplicableScaleIDArr))) ? true : false;
                    $thisSelectionName = 'char_' . $thisStudentID . '_' . $thisCharID;

                    $thisDisplay = '';
                    if ($thisViewOnly) {
                        $thisDisplay .= $thisScaleName;
                        $thisDisplay .= '<input type="hidden" id="' . $thisSelectionName . '" name="' . $thisSelectionName . '" value="' . $thisScaleID . '" />';
                    } else {
                        $thisSelectionID = 'mark[' . $i . '][' . $j . ']';
                        $thisOtherParArr = array(
                            'onpaste' => 'pasteContent(\'' . $i . '\', \'' . $j . '\');',
                            'onkeyup' => 'isPasteContent(event, \'' . $i . '\', \'' . $j . '\');',
                            'onkeypress' => 'isPasteContent(event, \'' . $i . '\', \'' . $j . '\');',
                            'onkeydown' => 'isPasteContent(event, \'' . $i . '\', \'' . $j . '\');'
                        );
                        $thisDisplay = $this->Get_Personal_Characteristics_Scale_Selection($thisSelectionID, $thisScaleID, $thisSelectionName, $thisDisabled = false, $ApplicableScaleOnly = true, $noFirst = false, $thisOtherParArr);
                    }
                    $x .= '<td>' . $thisDisplay . '</td>' . "\n";
                }

                if ($eRCTemplateSetting['PersonalCharacteristicComment']) {
                    $thisComment = $StudentPersonalCharAssoArr[$ReportID][$SubjectID][$thisStudentID]['comment'];

                    $thisTextareaID = 'comment_' . $thisStudentID;
                    $thisLimitTextAreaEvent = "onKeyDown='limitText(document.getElementById(\"$thisTextareaID\"), " . $this->Get_Personal_Characteristics_Comment_MaxLength() . ");return noEsc();'";
                    $thisLimitTextAreaEvent .= "onKeyUp='limitText(document.getElementById(\"$thisTextareaID\"), " . $this->Get_Personal_Characteristics_Comment_MaxLength() . ");'";

                    $x .= '<td>';
                    $x .= $linterface->GET_TEXTAREA($thisTextareaID, $thisComment, 40, 5, '', 0, $thisLimitTextAreaEvent);
                    $x .= '</td>';
                }

                // [2015-0421-1144-05164]
                if ($eRCTemplateSetting['PersonalChar']['ShowStudentDetails'] && count($numOfChar) > 0) {
                    $x .= "<td><span class='row_link_tool'>
                    <a href='#' id='personal_$thisStudentID' onclick='Toggle_Display_Student_Detail(\"$thisStudentID\"); return false;' class='zoom_in' title='" . $Lang['SysMgr']['SubjectClassMapping']['ViewGroup'] . "'></a>
												</span></td>";
                }
                $x .= '</tr>' . "\n";

                // [2015-0421-1144-05164]
                // display Student eAttendance & eDiscipline Infomation
                if ($eRCTemplateSetting['PersonalChar']['ShowStudentDetails'] && count($numOfChar) > 0) {
                    // Get Student Info
                    $studentData = $this->Get_Student_Profile_Data($ReportID, $thisStudentID, true, true, true, "", true);
                    $studentAttendanceData = $studentData['AttendanceAry'][$thisStudentID];

                    // eAttendance Infomation
                    $x .= '<tr class="sub_row personalDetail_' . $thisStudentID . '" style="display:none;">';
                    $x .= '<td colspan="3">' . $Lang['eReportCard']['AdditionalInfo']['Attendance'] . '</td>';
                    $x .= '<td colspan="' . ($numOfChar + 1) . '">';
                    $x .= '<table style="width:60%;">';
                    $x .= '<tr>';
                    $x .= '<td width="35%">' . $Lang['eReportCard']['AdditionalInfo']['Absent'] . ' ( ' . $Lang['eReportCard']['AdditionalInfo']['Days'] . ' )</td>';
                    $x .= '<td>' . ($studentAttendanceData['Days Absent'] > 0 ? $studentAttendanceData['Days Absent'] : 0) . '</td>';
                    $x .= '</tr>';
                    $x .= '<tr>';
                    $x .= '<td>' . $Lang['eReportCard']['AdditionalInfo']['Absent'] . ' ( ' . $Lang['eReportCard']['AdditionalInfo']['Period'] . ' )</td>';
                    $x .= '<td>0</td>';
                    $x .= '</tr>';
                    $x .= '<tr>';
                    $x .= '<td>' . $Lang['eReportCard']['AdditionalInfo']['Late'] . '</td>';
                    $x .= '<td>' . ($studentAttendanceData['Time Late'] > 0 ? $studentAttendanceData['Time Late'] : 0) . '</td>';
                    $x .= '</tr>';
                    $x .= '</table>';
                    $x .= '</td>';
                    $x .= '</tr>';

                    // eDiscipline Information
                    $x .= '<tr class="sub_row personalDetail_' . $thisStudentID . '" style="display:none;">';
                    $x .= '<td colspan="3">' . $Lang['eReportCard']['AdditionalInfo']['MeritsDemerits'] . '</td>';
                    $x .= '<td colspan="' . ($numOfChar + 1) . '">';
                    $x .= '<table style="width:60%;">';
                    $x .= '<tr>';
                    $x .= '<td width="35%">' . $Lang['eReportCard']['AdditionalInfo']['Merits'] . '</td>';
                    $x .= '<td>' . (intval($studentData[1]) > 0 ? intval($studentData[1]) : 0) . '</td>';
                    $x .= '</tr>';
                    $x .= '<tr>';
                    $x .= '<td>' . $Lang['eReportCard']['AdditionalInfo']['MinorMerit'] . '</td>';
                    $x .= '<td>' . (intval($studentData[2]) > 0 ? intval($studentData[2]) : 0) . '</td>';
                    $x .= '</tr>';
                    $x .= '<tr>';
                    $x .= '<td>' . $Lang['eReportCard']['AdditionalInfo']['MajorMerit'] . '</td>';
                    $x .= '<td>' . (intval($studentData[3]) > 0 ? intval($studentData[3]) : 0) . '</td>';
                    $x .= '</tr>';
                    $x .= '<tr>';
                    $x .= '<td>' . $Lang['eReportCard']['AdditionalInfo']['Demerits'] . '</td>';
                    $x .= '<td>' . (intval($studentData[- 1]) > 0 ? intval($studentData[- 1]) : 0) . '</td>';
                    $x .= '</tr>';
                    $x .= '<tr>';
                    $x .= '<td>' . $Lang['eReportCard']['AdditionalInfo']['MinorDemerit'] . '</td>';
                    $x .= '<td>' . (intval($studentData[- 2]) > 0 ? intval($studentData[- 2]) : 0) . '</td>';
                    $x .= '</tr>';
                    $x .= '<tr>';
                    $x .= '<td>' . $Lang['eReportCard']['AdditionalInfo']['MajorDemerit'] . '</td>';
                    $x .= '<td>' . (intval($studentData[- 3]) > 0 ? intval($studentData[- 3]) : 0) . '</td>';
                    $x .= '</tr>';
                    $x .= '</table>';
                    $x .= '</td>';
                    $x .= '</tr>';
                }
            }
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<br style="clear:both;" />' . "\n";
        $x .= '<!--PrintEnd-->' . "\n";

        $x .= '<div class="edit_bottom_v30">' . "\n";
        $thisDisabled = ($ViewOnly) ? true : false;
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit();", $id = "Btn_Submit", $ParOtherAttribute = '', $thisDisabled) . "\n";
        $x .= '&nbsp;' . "\n";
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location='$thisCancelPath'", $id = "Btn_Cancel") . "\n";
        $x .= '</div>' . "\n";

        // ## Textarea for copy and paste
        $x .= '<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>' . "\n";

        // ## Javascript for copy and paste
        $x .= '	<script language="javascript">
						var rowx = 0, coly = 0;			//init
						var xno = ' . $numOfChar . '; yno = ' . $numOfStudent . ';		// set table size
						var startContent = "";
						var setStart = 1;
					</script>
					';

        $x .= $studentIdHiddenField;
        $x .= '<input type="hidden" id="ClassLevelID" name="ClassLevelID" value="' . $ClassLevelID . '" />' . "\n";
        $x .= '<input type="hidden" id="ReportID" name="ReportID" value="' . $ReportID . '" />' . "\n";
        $x .= '<input type="hidden" id="SubjectID" name="SubjectID" value="' . $SubjectID . '" />' . "\n";
        $x .= '<input type="hidden" id="ClassID" name="ClassID" value="' . $ClassID . '" />' . "\n";
        $x .= '<input type="hidden" id="SubjectGroupID" name="SubjectGroupID" value="' . $SubjectGroupID . '" />' . "\n";
        $x .= '<input type="hidden" id="FromMS" name="FromMS" value="' . $FromMS . '" />' . "\n";

        $x .= '</form>' . "\n";

        return $x;
    }

    // ######################################################################################################
    // Management > OtherInfo Start
    //
    function Get_Other_Info_Overview_UI($UploadType, $TermID, $FormID = '')
    {
        global $linterface, $eRCTemplateSetting, $eReportCard, $Lang, $ck_ReportCard_UserType;

        switch ($ck_ReportCard_UserType) {
            case "ADMIN":
                $checkPermission = 0;
                break;
            case "TEACHER":
                $checkPermission = 1;
                break;
            case "VIEW_GROUP":
                $checkPermission = 0;
        }
        $FormSelection = $this->Get_Form_Selection("FormID", $FormID, "js_Reload_Other_Info_Table()", $noFirst = 0, $isAll = 0, $hasTemplateOnly = 0, $checkPermission, $IsMultiple = 0, $excludeWithoutClass = 1);
        $TermSelection = $this->Get_Term_Selection("TermID", $this->schoolYearID, $TermID, "js_Reload_Other_Info_Table()", $NoFirst = 1, $WithWholeYear = 1);

        $remarkTable = '';
        if (isset($eRCTemplateSetting['OtherInfo']['CustRemarks'])) {
            $remarksLang = $eReportCard['Template']['OtherInfoRemarks'][$UploadType];

            if ($remarksLang != '') {
                $remarkTable = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $remarksLang);
            }
        }

        // ## Toolbar
        // SIS Export all data 20140731
        if ($eRCTemplateSetting['SpecialRemarks']['ExportAllData']) {
            // Export -> Export ALL Data
            global $intranet_session_language;
            if ($intranet_session_language == 'en') {
                $Lang['Btn']['Export'] = 'Export All Data';
            } else
                $Lang['Btn']['Export'] = '匯出所有資料';
        }

        if ($ck_ReportCard_UserType != "VIEW_GROUP") {
            $ImportBtn = $linterface->Get_Content_Tool_v30('import', $href = "javascript:js_Import();", $text = "", '', $other = "", $divID = '');
        }
        $ExportBtn = $linterface->Get_Content_Tool_v30('export', $href = "javascript:js_Export();", $text = "", $options = "", $other = "", $divID = '');

        if ($remarkTable != '') {
            $x .= $remarkTable . "\n";
            $x .= '<br style="clear: both;">' . "\n";
        }

        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $ImportBtn . "\n";
        $x .= $ExportBtn . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<br style="clear: both;">' . "\n";

        $x .= '<div class="table_board">' . "\n";
        $x .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">' . "\n";
        $x .= '<tbody>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td valign="bottom">' . "\n";
        $x .= '<div class="table_filter">' . "\n";
        $x .= $FormSelection;
        $x .= $TermSelection;
        $x .= '</div>' . "\n";
        $x .= '<br style="clear: both;">' . "\n";
        $x .= '</td>' . "\n";
        $x .= '<td valign="bottom">&nbsp;</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<div id="OtherInfoTableDiv"></div>';
        $x .= '</div>' . "\n";

        return $x;
    }

    function Get_Other_Info_Overview_Table($UploadType, $TermID, $FormID = '')
    {
        global $Lang;

        include_once ("libinterface.php");
        $linterface = new interface_html();

        $FormArr = BuildMultiKeyAssoc($this->Get_User_Accessible_Form(), "ClassLevelID");
        $ClassInfoArr = $this->Get_User_Accessible_Class();
        $ClassArr = BuildMultiKeyAssoc($ClassInfoArr, array(
            "ClassLevelID",
            "ClassID"
        ));
        $ClassIDArr = Get_Array_By_Key($ClassInfoArr, "ClassID");

        $LastModifiedInfo = $this->Get_OtherInfo_Last_Modified_Info($UploadType, $TermID, $ClassIDArr);
        $LastModifiedInfo = BuildMultiKeyAssoc($LastModifiedInfo, "ClassID");

        $x .= '<table id="OtherInfoTable" class="common_table_list_v30">' . "\n";
        // col group
        $x .= '<col align="left" style="width: 20%;">' . "\n";
        $x .= '<col align="left">' . "\n";
        $x .= '<col align="left">' . "\n";
        $x .= '<col align="left">' . "\n";
        $x .= '<col align="left" style="width: 100px;">' . "\n";

        // table head
        $ColSpan = "4";
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th>' . $Lang['General']['Form'] . '</th>' . "\n";
        $x .= '<th colspan="' . $ColSpan . '" class="sub_row_top">' . $Lang['General']['Class'] . '</th>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th >' . $Lang['General']['Name'] . '</th>' . "\n";
        $x .= '<th class="sub_row_top">' . $Lang['General']['Name'] . '</th>' . "\n";
        $x .= '<th class="sub_row_top">' . $Lang['General']['LastModified'] . '</th>' . "\n";
        $x .= '<th class="sub_row_top">' . $Lang['General']['LastModifiedBy'] . '</th>' . "\n";
        $x .= '<th class="sub_row_top">&nbsp;</th>' . "\n";
        $x .= '</tr>' . "\n";

        $x .= '</thead>' . "\n";

        // table body
        $x .= '<tbody>' . "\n";

        // loop term
        foreach ((array) $ClassArr as $ClassLevelID => $ClassInfoArr) {
            if (! ($FormInfo = $FormArr[$ClassLevelID]))
                continue;

                // Filter by Form
                if ($FormID != $FormInfo["ClassLevelID"] && $FormID != '') {
                    continue;
                }

                $NumOfClass = count($ClassInfoArr) + 1;

                $x .= '<tr>' . "\n";
                $x .= '<td rowspan="' . $NumOfClass . '">' . $FormInfo['LevelName'] . '</td>' . "\n";
                $x .= '</tr>' . "\n";

                // loop module
                foreach ((array) $ClassInfoArr as $thisClassID => $thisClassInfo) {
                    $LastModifiedDate = Get_String_Display($LastModifiedInfo[$thisClassID]['DateInput']);
                    $LastModifiedBy = Get_String_Display($LastModifiedInfo[$thisClassID]['NameField']);

                    $x .= '<tr class="sub_row">' . "\n";
                    $x .= '<td>' . $thisClassInfo['ClassName'] . '</td>' . "\n";
                    $x .= '<td>' . $LastModifiedDate . '</td>' . "\n";
                    $x .= '<td>' . $LastModifiedBy . '</td>' . "\n";
                    $x .= '<td>' . "\n";
                    $x .= $linterface->Get_Action_Lnk('edit.php?UploadType=' . $UploadType . '&TermID=' . $TermID . '&ClassID=' . $thisClassID, "Edit Other Info", NULL, "edit_dim");
                    $x .= '</td>' . "\n";
                    $x .= '</tr>' . "\n";
                } // end loop module
        } // end loop term

        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n" . "\n";

        return $x;
    }

    function Get_Other_Info_Edit_UI($UploadType, $TermID, $ClassID, $SelectClassID = '')
    {
        global $linterface, $PATH_WRT_ROOT, $eReportCard, $Lang, $eRCTemplateSetting;

        // Get Term
        // if($TermID == 0)
        // {
        // $TermTitle = $eReportCard['WholeYear'];
        // }
        // else
        // {
        // include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
        // $ObjYearTerm = new academic_year_term($TermID);
        // $TermTitle = $ObjYearTerm->Get_Year_Term_Name();
        // }
        $TermSelection = $this->Get_Term_Selection("TermID", $this->schoolYearID, $TermID, "js_Reload_Page();", $NoFirst = 1, $WithWholeYear = 1);

        // Get Class Name
        include_once ("form_class_manage.php");
        $YearClassObj = new year_class($ClassID, 1);
        $ClassName = $YearClassObj->ClassTitleEN;
        $YearName = $YearClassObj->YearName;
        $YearID = $YearClassObj->YearID;
        $checkPermission = ($this->IS_ADMIN_USER($_SESSION['UserID']) || $this->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) ? 0 : 1;
        $ClassSelection = $this->Get_Class_Selection("ClassID", $YearID, $ClassID, "js_Reload_Page();", $noFirst = 1, $isAll = 0, $checkPermission);

        // Page Navigation
        $PAGE_NAVIGATION[] = array(
            $eReportCard['OtherInfoArr'][$UploadType],
            "javascript:js_Go_Back_To_Other_Info()"
        );
        // $PAGE_NAVIGATION[] = array($TermTitle, "");
        // $PAGE_NAVIGATION[] = array($ClassName, "");
        $PAGE_NAVIGATION[] = array(
            $TermSelection,
            ""
        );
        $PAGE_NAVIGATION[] = array(
            $ClassSelection,
            ""
        );
        $PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

        // ## Toolbar
        if (! $this->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) {
            $ImportBtn = $linterface->Get_Content_Tool_v30('import', $href = "javascript:js_Import();", $text = "", '', $other = "", $divID = '');
        }

        $options = array();
        $options[] = array(
            "export.php?TermID=$TermID&UploadType=$UploadType&YearClassID=" . $YearClassObj->YearClassID,
            $Lang['eReportCard']['PersonalCharArr']['ExportClassData'] . " ($ClassName)"
        );
        $options[] = array(
            "export.php?TermID=$TermID&UploadType=$UploadType&YearID=" . $YearClassObj->YearID,
            $Lang['eReportCard']['PersonalCharArr']['ExportFormData'] . " ($YearName)"
        );
        $ExportBtn = $linterface->Get_Content_Tool_v30('export', "javascript:void(0)", $text = "", $options, $other = "", $divID = '');

        // export past years data
        if ($eRCTemplateSetting['OtherInfo']['ExportPastYearData']) {
            $ExportPastDataBtn = $linterface->Get_Content_Tool_v30('export', "javascript:goExportPastData();", $eReportCard['ManagementArr']['OtherInfoArr']['ExportPastYearData']);
        }

        $x .= $PageNavigation;
        $x .= '<br style="clear: both;">' . "\n";
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $ImportBtn . "\n";
        $x .= $ExportBtn . "\n";
        $x .= $ExportPastDataBtn . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<br style="clear: both;">' . "\n";

        if ($UploadType == 'foreignStudent') {
            $x .= $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $eReportCard['foreignStudentArr']['Remark']);
        }
        $x .= '<br style="clear:both;" />' . "\n";
        $x .= '<div class="table_board">' . "\n";
        // $x .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">'."\n";
        // $x .= '<tbody>'."\n";
        // $x .= '<tr>'."\n";
        // $x .= '<td valign="bottom">'."\n";
        // $x .= '<div class="table_filter">'."\n";
        // $x .= $ClassSelection;
        // $x .= '</div>'."\n";
        // $x .= '<br style="clear: both;">'."\n";
        // $x .= '</td>'."\n";
        // $x .= '<td valign="bottom">&nbsp;</td>'."\n";
        // $x .= '</tr>'."\n";
        // $x .= '</tbody>'."\n";
        // $x .= '</table>'."\n";
        $x .= $this->Get_Other_Info_Edit_Table($UploadType, $TermID, $ClassID);
        $x .= '</div>' . "\n";

        return $x;
    }

    function Get_Other_Info_Edit_Table($UploadType, $TermID, $ClassID)
    {
        global $Lang, $eReportCard, $eRCTemplateSetting;

        include_once ("libinterface.php");
        $linterface = new interface_html();

        // Get Other Info Config
        $OtherInfoConfig = $this->getOtherInfoConfig($UploadType);
        $NumOfConfig = sizeof($OtherInfoConfig);

        // Get Student By Class
        $StudentArr = $this->GET_STUDENT_BY_CLASS($ClassID);

        // Get Existing Other Info
        $OtherInfoData = $this->Get_Student_OtherInfo_Data($UploadType, $TermID, $ClassID);
        $OtherInfoData = BuildMultiKeyAssoc($OtherInfoData, array(
            "StudentID",
            "ItemCode"
        ), "Information", 1);

        // Get Btn
        if (! $this->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) {
            $SubmitBtn = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "submit");
        }
        $CancelBtn = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Go_Back_To_Other_Info()");

        $x .= '<form id="form1" name="form1" action="edit_update.php" method="POST" onsubmit="return js_Check_Form();">' . "\n";
        $x .= '<table id="OtherInfoTable" class="common_table_list_v30 edit_table_list_v30">' . "\n";
        // col group
        $x .= '<col align="left">' . "\n";
        $x .= '<col align="left">' . "\n";
        $x .= '<col align="left">' . "\n";
        for ($i = 5; $i < $NumOfConfig; $i ++) {
            $x .= '<col align="left" width="' . ceil(70 / ($NumOfConfig - 5)) . '%">' . "\n";
        }
        // ## Prepare "Apply all" buttons
        $ApplyAllImg = $this->Get_ApplyToAll_Image();

        // table head
        $x .= '<thead>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</th>' . "\n";
        $x .= '<th>' . $Lang['SysMgr']['FormClassMapping']['ClassNo'] . '</th>' . "\n";
        $x .= '<th>' . $eReportCard['Student'] . '</th>' . "\n";
        $applyAllStr = "<tr class=\"edit_table_head_bulk\">";
        $applyAllStr .= "<th>&nbsp;</th>";
        $applyAllStr .= "<th>&nbsp;</th>";
        $applyAllStr .= "<th>&nbsp;</th>";
        for ($i = 5; $i < $NumOfConfig; $i ++) {
            $NameEn = $OtherInfoConfig[$i]['EnglishTitle'];
            $NameCh = str_replace(array(
                "(",
                ")"
            ), "", $OtherInfoConfig[$i]['ChineseTitle']);

            if ($OtherInfoConfig[$i]['Type'] == "str") {
                $applyAllStr .= "<th>";
                $applyAllStr .= $linterface->GET_TEXTAREA('remarkTextInputAll_' . $i, "", '', 5, NULL, NULL, "style='width:99%' ", "", 'remarkTextInputAll_' . $i, $OtherInfoConfig[$i]['Length']);
                $applyAllStr .= "<br />";
                $applyAllStr .= '<a href="javascript:jsApplyToAll(\'remarkTextInputAll_' . $i . '\',\'remarkTextInput_' . $i . '\');">' . $ApplyAllImg . '</a>';
                $applyAllStr .= "</th>";
            }
            $x .= '<th>' . Get_Lang_Selection($NameCh, $NameEn) . '</th>' . "\n";
        }
        $applyAllStr .= "</tr>";
        $x .= '</tr>' . "\n";
        $x .= $eRCTemplateSetting['OtherInfo']['RemarkApplyToAll'] ? $applyAllStr : ""; // 2014-0912-1716-13164
        $x .= '</thead>' . "\n";

        // table body
        $x .= '<tbody>' . "\n";
        $NumOfStudent = sizeof($StudentArr);
        for ($i = 0; $i < $NumOfStudent; $i ++) {
            // $ClassNameNum = $StudentArr[$i]['ClassName']." (".$StudentArr[$i]['ClassNumber'].")";
            $thisStudentID = $StudentArr[$i]['UserID'];

            $x .= '<tr>' . "\n";
            // $x .= '<td>'.$ClassNameNum.'</td>'."\n";
            $x .= '<td>' . $StudentArr[$i]['ClassName'] . '</td>' . "\n";
            $x .= '<td>' . $StudentArr[$i]['ClassNumber'] . '</td>' . "\n";
            $x .= '<td>' . Get_Lang_Selection($StudentArr[$i]['StudentNameCh'], $StudentArr[$i]['StudentNameEn']) . '</td>' . "\n";
            for ($j = 5; $j < $NumOfConfig; $j ++) {
                $Code = $OtherInfoConfig[$j]['EnglishTitle'];
                $inputPar['maxlength'] = $OtherInfoConfig[$j]['Length'];
                $ID = 'OtherInfoArrIdx' . "_" . $i . "_" . $j;
                $Name = 'OtherInfoArr[' . $ClassID . '][' . $thisStudentID . '][' . $Code . ']';
                $Value = $OtherInfoData[$thisStudentID][$Code];
                $isDisabled = '';
                if ($this->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) {
                    $inputPar['disabled'] = 'true';
                    $isDisabled = ' disabled ';
                }

                $x .= '<td>' . "\n";

                // Input Type: Number
                if ($OtherInfoConfig[$j]['Type'] == "num") {
                    $x .= $linterface->GET_TEXTBOX_NUMBER($ID, $Name, $Value, $OtherInfoConfig[$j]['Type'], $inputPar);
                    $x .= $linterface->Get_Form_Warning_Msg("WarnDiv" . $ID, $eReportCard['ImportWarningArr']['InputNumber'], "WarnMsgDiv");
                }                // Input Type: Content
                else if ($OtherInfoConfig[$j]['Type'] == "str") {
                    // $x .= $linterface->GET_TEXTAREA($Name, "\n".$Value, '', 5, NULL, NULL, "style='width:99%' ", $OtherInfoConfig[$j]['Type'], $ID, $OtherInfoConfig[$j]['Length']);
                    $x .= $linterface->GET_TEXTAREA($Name, "\n" . $Value, '', 5, NULL, NULL, "style='width:99%' " . $isDisabled, $OtherInfoConfig[$j]['Type'] . " remarkTextInput_" . $j, $ID, $OtherInfoConfig[$j]['Length']); // 2014-0912-1716-13164
                }                // Input Type: Characters
                else if ($OtherInfoConfig[$j]['Type'] == "char") {
                    $x .= $linterface->GET_TEXTBOX($ID, $Name, $Value, $OtherInfoConfig[$j]['Type'], $inputPar);
                }                // Input Type: Grade
                else {
                    $x .= $linterface->GET_TEXTBOX($ID, $Name, $Value, $OtherInfoConfig[$j]['Type'], $inputPar);
                    $x .= $linterface->Get_Form_Warning_Msg("WarnDiv" . $ID, $eReportCard['ImportWarningArr']['InputGrade'], "WarnMsgDiv");
                }

                $x .= '</td>' . "\n";
            }
            $x .= '</tr>' . "\n";
        }
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= $SubmitBtn . "\n";
        $x .= $CancelBtn . "\n";
        $x .= '</div>' . "\n";
        $x .= '<input type="hidden" name="UploadType" value="' . $UploadType . '">' . "\n";
        $x .= '<input type="hidden" name="TermID" value="' . $TermID . '">' . "\n";
        $x .= '<input type="hidden" name="ClassID" value="' . $ClassID . '">' . "\n";
        $x .= '</form>' . "\n";

        return $x;
    }

    function Get_Management_OtherInfo_Import_UI($UploadType, $TermID, $ClassID)
    {
        global $Lang, $linterface, $eReportCard;

        // page navigation (leave the array empty if no need)
        $PAGE_NAVIGATION[] = array(
            $eReportCard['OtherInfoArr'][$UploadType],
            "javascript:js_Go_Back_To_Other_Info()"
        );
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Import'],
            ""
        );

        $x .= '<form id="form1" name="form1" method="post" action="file_upload_confirm.php" onsubmit="return checkForm();" enctype="multipart/form-data">' . "\n";
        $x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $linterface->GET_IMPORT_STEPS(1);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $linterface->Get_Warning_Message_Box('', $eReportCard['ManagementArr']['OtherInfoArr']['ImportDataInstruction']);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $this->Get_Management_OtherInfo_Import_Table($UploadType, $TermID, $ClassID);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<br><br>' . "\n";

        $SubmitBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "checkForm()");
        $CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back_To_Other_Info();");
        $x .= '<div class="edit_bottom_v30">';
        // ## Close Buttom
        $x .= $SubmitBtn . "\n";
        $x .= $CancelBtn . "\n";
        $x .= '</div>';
        $x .= '<input type="hidden" id="UploadType" name="UploadType" value="' . $UploadType . '">' . "\n";
        $x .= '</form>' . "\n";

        return $x;
    }

    function Get_Management_OtherInfo_Import_Table($UploadType, $TermID, $ClassID)
    {
        global $lreportcard, $Lang, $eReportCard, $PATH_WRT_ROOT, $LAYOUT_SKIN, $ck_ReportCard_UserType;

        include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
        $ObjYear = new academic_year($lreportcard->schoolYearID);
        $yearDisplay = $ObjYear->Get_Academic_Year_Name();

        // ## Semester Selection
        if ($TermID == '') {
            $semester_data = getSemesters($lreportcard->schoolYearID, 0);
            $curSemInfoArr = getCurrentAcademicYearAndYearTerm();
            $TermID = $curSemInfoArr['YearTermID'];
        }

        $semesterSelect = $this->Get_Term_Selection('YearTermID', $lreportcard->schoolYearID, $TermID, $OnChange = '', $NoFirst = 1, $WithWholeYear = 1);

        // class selection
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        $lclass = new libclass();
        $checkClassTeacher = ($ck_ReportCard_UserType == "ADMIN") ? 0 : 1;
        $ClassSelection = $lclass->getSelectClassWithWholeForm("id='YearClassSelect' name='YearClassSelect' ", "::" . $ClassID, $firstValue = "", $pleaseSelect = "", $lreportcard->schoolYearID, $FormIDArr = '', $checkClassTeacher, $HideNoClassForm = 1);

        $SampleCSV = "get_sample_csv.php?UploadType=" . $UploadType;

        $x .= '<table width="100%" cellpadding="2" class="form_table_v30">' . "\n";
        $x .= '<col class="field_title">';
        $x .= '<col class="field_c">';

        // Report
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['SchoolYear'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $yearDisplay . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // Form
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['Semester'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $semesterSelect . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // Class Name
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['Class'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<span id="ClassSelection">' . $ClassSelection . '</span>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // File
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['File'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<input class="file" type="file" name="userfile" id="userfile">' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<a target="_blank" href="' . $SampleCSV . '" class="tablelink">' . "\n";
        $x .= '<img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">' . "\n";
        $x .= $eReportCard['DownloadCSVFile'] . "\n";
        $x .= '</a>' . "\n";

        return $x;
    }

    //
    // Management > OtherInfo End
    // ######################################################################################################

    // ########## Report > Print Archived Report
    function Get_Print_Archived_Report_Index()
    {
        global $eReportCard, $linterface, $Lang;

        $StepsObj = $this->Get_Print_Archived_Report_Step_Obj(1);

        // get existing academic year in class history
        $YearList = $this->Get_Student_From_Class_History();
        $YearList = array_unique(Get_Array_By_Key($YearList, "AcademicYearID"));

        $x .= '<br />';
        $x .= $StepsObj;
        $x .= '<br />';
        $x .= '<form id="form1" name="form1" action="select_report.php" method="POST" onsubmit="return js_Check_Form()">';
        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
        // class selection
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['SelectStudentMethod'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $linterface->Get_Radio_Button("Method1", "Method", 1, 1, $Class = "", $eReportCard['UserLogin'], "js_Toggle_Select_Method()") . "&nbsp;";
        $x .= $linterface->Get_Radio_Button("Method2", "Method", 2, 0, $Class = "", $eReportCard['ClassHistory'], "js_Toggle_Select_Method()");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // User Login
        $x .= '<tr class="UserLoginTr" style="display:none;">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $linterface->RequiredSymbol() . $eReportCard['UserLogin'] . '</td>' . "\n";
        $x .= '<td>';
        $x .= '<input id="UserLogin" name="UserLogin" class="textboxnum">';
        $x .= $linterface->Get_Form_Warning_Msg("WarnEmptyUserLogin", $eReportCard['jsWarningArr']['PleaseInputUserLogin'], "WarnMsg");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Class History
        $x .= '<tr class="ClassHistoryTr" style="display:none;">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['AcademicYear'] . '</td>' . "\n";
        $x .= '<td>';
        $x .= $this->Get_Class_History_Academic_Year_Selection("AcademicYear", "", "js_Reload_Class_Selection()", 1, 0);
        // $x .= getSelectAcademicYear("AcademicYear", " onchange='js_Reload_Class_Selection()' ", 1, 0, '', 0, 1);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr class="ClassHistoryTr" style="display:none;">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Class'] . '</td>' . "\n";
        $x .= '<td >';
        $x .= '<div id="ClassSelectionDiv">' . $linterface->Get_Ajax_Loading_Image() . '</div>';
        // $x .= $this->Get_Class_History_Class_Selection("ClassName", "", "", "js_Reload_Student_Selection()", 1, 0);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '<tr class="ClassHistoryTr" style="display:none;">' . "\n";
        $x .= '<td class="field_title" width="30%">' . $linterface->RequiredSymbol() . $eReportCard['ClassNumber'] . '</td>' . "\n";
        $x .= '<td >';
        $x .= '<div id="StudentSelectionDiv">' . $linterface->Get_Ajax_Loading_Image() . '</div>';
        $x .= $linterface->Get_Form_Warning_Msg("WarnEmptyStudentID", $eReportCard['jsWarningArr']['PleaseSelectStudent'], "WarnMsg");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= $linterface->MandatoryField();
        $x .= '<div class="edit_bottom_v30">';
        $x .= $linterface->Get_Action_Btn($Lang['Btn']['Continue'], "submit");
        $x .= '</div>' . "\n";
        return $x;
    }

    function Get_Print_Archived_Report_Step_Obj($PageNo)
    {
        global $eReportCard, $linterface;

        ${"Page" . $PageNo} = 1;
        $StepObj[] = array(
            $eReportCard['SelectStudent'],
            $Page1
        );
        $StepObj[] = array(
            $eReportCard['SelectReport'],
            $Page2
        );

        return $linterface->GET_STEPS($StepObj);
    }

    function Get_eRC_Active_Year_Selection($id_name, $selectedYear='', $onchange='', $noFirst=0, $isAll=0, $firstTitle='')
    {
        global $intranet_db, $linterface, $lreportcard;

        $selectedYearDisplay = $selectedYear? $lreportcard->Get_Academic_Year_Long_Name($selectedYear) : $lreportcard->GET_ACTIVE_YEAR("-");

        $sql = "SHOW DATABASES LIKE '".$intranet_db."_DB_REPORT_CARD_%'";
        $reportcardDB = $lreportcard->returnVector($sql);

        $selectedDBYear = '';
        $reportcardDBArr = array();
        if (sizeof($reportcardDB) > 1) {
            for($i=0; $i<sizeof($reportcardDB); $i++) {
                $yearDisplay = substr($reportcardDB[$i], -4);
                $correctDbNameSubStr = '_DB_REPORT_CARD_'.$yearDisplay;
                if (is_numeric($yearDisplay) && substr($reportcardDB[$i], -20) == $correctDbNameSubStr) {
                    $yearDisplayLong = $lreportcard->Get_Academic_Year_Long_Name($yearDisplay);
                    $reportcardDBArr[] = array($yearDisplay, $yearDisplayLong);

                    if($yearDisplayLong == $selectedYearDisplay){
                        $selectedDBYear = $yearDisplay;
                    }
                }
            }
        }

        if (count($reportcardDBArr) > 0) {
            # have other years of eRC => can let the user to switch between years
            $disableOtherYearSelect = '';
        }
        else {
            # no other years of eRC => disable the year selection
            $disableOtherYearSelect = 'disabled';
        }

        $selectionTag = ' id="'.$id_name.'" name="'.$id_name.'" onchange="'.$onchange.'" '.$disableOtherYearSelect;
        $activeYearSelection = $linterface->GET_SELECTION_BOX($reportcardDBArr, $selectionTag, '', $selectedDBYear);

        return $activeYearSelection;
    }

    function Get_Academic_Year_Selection($ID_Name, $Selected = '', $Onchange = '', $noFirst = 0, $isAll = 0, $firstTitle = '')
    {
        global $PATH_WRT_ROOT;

        include_once ($PATH_WRT_ROOT . 'includes/form_class_manage.php');
        $fcm = new form_class_manage();

        $academicYearAry = $fcm->Get_Academic_Year_List();
        $optionArr = BuildMultiKeyAssoc($academicYearAry, "AcademicYearID", Get_Lang_Selection('YearNameB5', 'YearNameEN'), 1);

        $onchange = '';
        if ($Onchange != "")
            $onchange = 'onchange="' . $Onchange . '"';

            $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange;
            return getSelectByAssoArray($optionArr, $selectionTags, $Selected, $isAll, $noFirst, $firstTitle);
    }

    function Get_Class_History_Academic_Year_Selection($ID_Name, $Selected = '', $Onchange = '', $noFirst = 0, $isAll = 0, $firstTitle = '')
    {
        $AcademicYearArr = $this->Get_Academic_Year_From_Class_History();
        foreach ($AcademicYearArr as $Rec) {
            $OptionArr[$Rec['AcademicYear']] = $Rec['AcademicYear'];
        }

        $onchange = '';
        if ($Onchange != "")
            $onchange = 'onchange="' . $Onchange . '"';

            $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange;
            return getSelectByAssoArray($OptionArr, $selectionTags, $Selected, $isAll, $noFirst, $firstTitle);
    }

    function Get_Class_History_Class_Selection($ID_Name, $AcademicYear = '', $Selected = '', $Onchange = '', $noFirst = 0, $isAll = 0, $firstTitle = '')
    {
        $ClassStudentArr = $this->Get_Student_From_Class_History($AcademicYear);
        $OptionArr = BuildMultiKeyAssoc($ClassStudentArr, array(
            "AcademicYear",
            "ClassName"
        ), "ClassName", 1);

        $onchange = '';
        if ($Onchange != "")
            $onchange = 'onchange="' . $Onchange . '"';

            $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange;
            return getSelectByAssoArray($OptionArr, $selectionTags, $Selected, $isAll, $noFirst, $firstTitle);
    }

    function Get_Class_History_Student_Selection($ID_Name, $AcademicYear = '', $ClassName = '', $Selected = '', $Onchange = '', $noFirst = 0, $isAll = 0, $firstTitle = '')
    {
        $ClassStudentArr = $this->Get_Student_From_Class_History($AcademicYear, $ClassName);
        $numOfStudent = count($ClassStudentArr);
        // $OptionArr = BuildMultiKeyAssoc($ClassStudentArr, "UserID", "ClassNumber", 1);

        $OptionArr = array();
        for ($i = 0; $i < $numOfStudent; $i ++) {
            $_studentId = $ClassStudentArr[$i]['UserID'];
            $_classNumber = $ClassStudentArr[$i]['ClassNumber'];

            $_studentInfoAry = $this->Get_Student_Info_For_Print_Archived_Report('', $_studentId);
            $_studentName = $_studentInfoAry['UserName'];

            $OptionArr[$_studentId] = $_classNumber;
            if ($_studentName != '') {
                $OptionArr[$_studentId] .= ' (' . $_studentName . ')';
            }
        }

        $onchange = '';
        if ($Onchange != "")
            $onchange = 'onchange="' . $Onchange . '"';

            $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange;
            return getSelectByAssoArray($OptionArr, $selectionTags, $Selected, $isAll, $noFirst, $firstTitle);
    }

    function Get_Print_Archived_Report_Select_Report_UI($UserLogin = '', $StudentID = '')
    {
        global $eReportCard, $Lang, $linterface;

        $StudentInfo = $this->Get_Student_Info_For_Print_Archived_Report($UserLogin, $StudentID);
        $StepsObj = $this->Get_Print_Archived_Report_Step_Obj(2);

        $x .= '<br />';
        $x .= $StepsObj;
        $x .= '<br />';
        $x .= '<form id="form1" name="form1" action="select_report.php" method="POST">';

        if (empty($StudentInfo)) {
            $x .= '<table cellpadding="20" cellspacing="20" border="0" width="100%">';
            $x .= '<tr><td  align="center">' . $eReportCard['StudentNotFound'] . '</td></tr>';
            $x .= '</table>';
            $x .= '<div class="edit_bottom_v30">';
            $x .= $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "history.back()");
            $x .= '</div>' . "\n";
        } else {
            $ArchiveReport = $this->Get_Student_Archived_Report($StudentInfo['UserID'], '', 1);
            if (empty($ArchiveReport)) {
                $x .= '<table cellpadding="20" cellspacing="20" border="0" width="100%">';
                $x .= '<tr><td  align="center">' . $eReportCard['WarningArr']['NoArchivedReport'] . '</td></tr>';
                $x .= '</table>';
                $x .= '<div class="edit_bottom_v30">';
                $x .= $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "history.back()");
                $x .= '</div>' . "\n";
            } else {
                $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
                // Student
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title" width="30%">' . $eReportCard['Student'] . '</td>' . "\n";
                $x .= '<td>' . "\n";
                $x .= $StudentInfo['UserName'] . " (" . $StudentInfo['UserLogin'] . ")";
                $x .= '<input type="hidden" name="StudentID" id="StudentID" value="' . $StudentInfo['UserID'] . '">' . "\n";
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";
                // Academic Year
                $YearWithArchiveReport = array_keys($ArchiveReport);
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title" width="30%">' . $linterface->RequiredSymbol() . $eReportCard['AcademicYear'] . '</td>' . "\n";
                $x .= '<td>';
                $x .= $this->Get_Reportcard_DB_Selection("AcademicYear", "", "js_Reload_Archive_Report_Selection()", 1, 0, '', $YearWithArchiveReport);
                $x .= $linterface->Get_Form_Warning_Msg("WarnEmptyAcademicYear", $eReportCard['jsWarningArr']['PleaseSelectAcademicYear'], "WarnMsg");
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";
                // Report
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title" width="30%">' . $linterface->RequiredSymbol() . $eReportCard['Reports'] . '</td>' . "\n";
                $x .= '<td >';
                $x .= '<div id="ReportSelectionDiv">' . $linterface->Get_Ajax_Loading_Image() . '</div>';
                $x .= $linterface->Get_Form_Warning_Msg("WarnEmptyReportID", $eReportCard['jsWarningArr']['PleaseSelectReport'], "WarnMsg");
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";
                $x .= '</table>' . "\n";

                $GenerateBtn = $linterface->Get_Action_Btn($Lang['Btn']['Generate'], "button", "js_Print_Archive_Report()") . "\n";
                $x .= $linterface->MandatoryField();

                $x .= '<div class="edit_bottom_v30">';
                $x .= $GenerateBtn . "&nbsp;";
                $x .= $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "history.back()");
                $x .= '</div>' . "\n";
            }
        }
        $x .= '</form>';
        return $x;
    }

    function Get_Reportcard_DB_Selection($ID_Name, $Selected = '', $Onchange = '', $noFirst = 0, $isAll = 0, $firstTitle = '', $IncludedYear = array())
    {
        global $intranet_db;

        $DBArr = $this->Get_Reportcard_DB_Academic_Year();

        $SizeOfRec = sizeof($DBArr);
        for ($i = 0; $i < $SizeOfRec; $i ++) {
            $thisDB = $DBArr[$i];
            $thisYear = substr($thisDB, - 4);

            if (! empty($IncludedYear) && ! in_array($thisYear, (array) $IncludedYear)) {
                continue;
            }

            $OptionArr[$thisYear] = $this->Get_Academic_Year_Long_Name($thisYear);
        }

        $onchange = '';
        if ($Onchange != "")
            $onchange = 'onchange="' . $Onchange . '"';

            $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange;
            return getSelectByAssoArray($OptionArr, $selectionTags, $Selected, $isAll, $noFirst, $firstTitle);
    }

    function Get_Archive_Report_Selection($ID_Name, $StudentID, $AcademicYear = '', $Selected = '', $Onchange = '', $noFirst = 0, $isAll = 0, $firstTitle = '')
    {
        global $intranet_db;

        $ReportArr = $this->Get_Student_Archived_Report($StudentID, $AcademicYear);
        $SizeOfRec = sizeof($ReportArr);
        for ($i = 0; $i < $SizeOfRec; $i ++) {
            $OptionArr[$ReportArr[$i]['ReportID']] = str_replace(":_:", "", $ReportArr[$i]['ReportTitle']);
        }

        $onchange = '';
        if ($Onchange != "")
            $onchange = 'onchange="' . $Onchange . '"';

            $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange;
            return getSelectByAssoArray($OptionArr, $selectionTags, $Selected, $isAll, $noFirst, $firstTitle);
    }

    function Get_Print_Archived_Report_Report_Select_UI($StudentInfo)
    {
        global $eReportCard, $linterface, $Lang;

        $x .= '<form id="form1" name="form1" action="select_report.php" method="POST">';
        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";
        // Student
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Student'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= $StudentInfo['UserName'] . " (" . $StudentInfo['UserLogin'] . ")";
        $x .= '<input type="hidden" name="StudentID" id="StudentID" value="' . $StudentInfo['UserID'] . '">' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Academic Year
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['AcademicYear'] . '</td>' . "\n";
        $x .= '<td>';
        $x .= $this->Get_Reportcard_DB_Selection("AcademicYear", "", "js_Reload_Archive_Report_Selection()", 1, 0);
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // Report
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title" width="30%">' . $eReportCard['Reports'] . '</td>' . "\n";
        $x .= '<td >';
        $x .= '<div id="ReportSelectionDiv">' . $linterface->Get_Ajax_Loading_Image() . '</div>';
        $x .= $linterface->Get_Form_Warning_Msg("WarnEmptyReportID", $eReportCard['jsWarningArr']['PleaseSelectReport'], "WarnMsg");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<div class="edit_bottom_v30">';
        $x .= $linterface->Get_Action_Btn($Lang['Btn']['Generate'], "button", "js_Print_Archive_Report()") . "\n";
        $x .= $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "history.back()");
        $x .= '</div>' . "\n";
        $x .= '</form>';
        return $x;
    }

    public function Get_Setting_Conduct_Index_UI($page = '', $order = '', $field = '')
    {
        global $linterface;

        // ## Toolbar
        $NewBtn = $linterface->Get_Content_Tool_v30('new', $href = "javascript:js_New();", $text = "", '', $other = "", $divID = '');
        $ImportBtn = $linterface->Get_Content_Tool_v30('import', $href = "javascript:js_Import();", $text = "", '', $other = "", $divID = '');
        $ExportBtn = $linterface->Get_Content_Tool_v30('export', $href = "javascript:js_Export();", $text = "", '', $other = "", $divID = '');

        $DBTableBtnArr[] = array(
            "edit",
            "javascript:checkEdit(document.form1, 'ConductID[]', 'edit.php')"
        );
        $DBTableBtnArr[] = array(
            "delete",
            "javascript:checkRemove(document.form1, 'ConductID[]', 'remove.php')"
        );
        $DBTableBtn = $linterface->Get_DBTable_Action_Button_IP25($DBTableBtnArr);

        $x .= '<form name="form1" id="form1" method="post" >';
        $x .= '<div class="content_top_tool">' . "\n";
        $x .= '<div class="Conntent_tool">' . "\n";
        $x .= $NewBtn . "\n";
        $x .= $ImportBtn . "\n";
        $x .= $ExportBtn . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= '</div>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<br style="clear:both" />' . "\n";
        $x .= '<div class="table_board">' . "\n";
        $x .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">' . "\n";
        $x .= '<tbody>' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td valign="bottom">' . "\n";
        $x .= $DBTableBtn;
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</tbody>' . "\n";
        $x .= '</table>' . "\n";

        $x .= $this->Get_Setting_Conduct_DBTable($page, $order, $field);
        $x .= '</div>' . "\n";
        $x .= '</form>';

        return $x;
    }

    public function Get_Setting_Conduct_DBTable($page = '', $order = '', $field = '')
    {
        global $PATH_WRT_ROOT, $Lang, $eReportCard, $eRCTemplateSetting, $page_size;

        include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_extrainfo.php");
        include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
        include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
        $lreportcard_extrainfo = new libreportcard_extrainfo();

        $field = ($field == '') ? 0 : $field;
        $order = ($order == '') ? 1 : $order;
        $page = ($page == '') ? 1 : $page;

        if (isset($ck_settings_page_size) && $ck_settings_page_size != "")
            $page_size = $ck_settings_page_size;
            $li = new libdbtable2007($field, $order, $page);

            $sql = $lreportcard_extrainfo->Get_Setting_Conduct_DBTable_Sql();

            $li->sql = $sql;
            $li->IsColOff = "IP25_table";

            // [2015-1104-1130-08164] Customize conduct display order
            if ($eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'])
                $li->field_array = array(
                    "MID(Conduct, 1, 1), CASE MID(Conduct, 2, 1) WHEN '+' THEN 2 WHEN '' THEN 3 WHEN '-' THEN 4 ELSE 1 END"
                );
                else
                    $li->field_array = array(
                        "Conduct"
                    );

                    $li->column_array = array(
                        0
                    );
                    $li->wrap_array = array(
                        0
                    );

                    $pos = 0;
                    $li->column_list .= "<th width='1' class='num_check'>#</th>\n";
                    $li->column_list .= "<th>" . $li->column_IP25($pos ++, $eReportCard['Conduct']) . "</th>\n";
                    $li->column_list .= "<th width='1'>" . $li->check("ConductID[]") . "</th>\n";
                    $li->no_col = $pos + 2;

                    $x .= $li->display();
                    // debug_pr($li->built_sql());
                    // debug_pr(mysql_error());
                    $x .= '<input type="hidden" name="pageNo" value="' . $li->pageNo . '" />';
                    $x .= '<input type="hidden" name="order" value="' . $li->order . '" />';
                    $x .= '<input type="hidden" name="field" value="' . $li->field . '" />';
                    $x .= '<input type="hidden" name="page_size_change" value="" />';
                    $x .= '<input type="hidden" name="numPerPage" value="' . $li->page_size . '" />';

                    return $x;
    }

    public function Get_Setting_Conduct_Edit_UI($ConductID = '')
    {
        global $eReportCard, $Lang, $linterface, $PATH_WRT_ROOT, $eRCTemplateSetting;

        // page navigation (leave the array empty if no need)
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Conduct'],
            "index.php"
        );
        if (trim($ConductID) != '') {
            include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_extrainfo.php");
            $lreportcard_extrainfo = new libreportcard_extrainfo();
            $ConductArr = $lreportcard_extrainfo->Get_Conduct($ConductID);
            $Conduct = $ConductArr[0]['Conduct'];
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['Edit']
            );
        } else
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['New']
            );

            // Btn
            $SubmitBtn = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "submit");
            $CancelBtn = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "window.location='index.php' ");

            $x .= '<form name="form1" id="form1" method="post" onsubmit="return js_Check_Form()" action="edit_update.php">';
            $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) . "\n";
            $x .= '<table class="form_table_v30">' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $eReportCard['Conduct'] . '</td>' . "\n";
            $x .= '<td>' . "\n";
            $OtherPar['maxlength'] = $eRCTemplateSetting['MaxLength']['ExtraInfo']['Conduct'];
            $OtherPar['onkeyup'] = "js_Delay_Action('js_Check_Conduct_Exist();')";
            $OtherPar['onkeydown'] = "AjaxChecking = true;";

            $x .= $linterface->GET_TEXTBOX_NAME("Conduct", "Conduct", $Conduct, '', $OtherPar) . "\n";
            $x .= $linterface->Get_Form_Warning_Msg("WarnEmptyConduct", $eReportCard['jsWarningArr']['PleaseInputConduct'], "WarnMsg");
            $x .= $linterface->Get_Form_Warning_Msg("WarnExistConduct", $eReportCard['jsWarningArr']['ConductExist'], "WarnMsg");
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table>' . "\n";
            $x .= $linterface->GET_HIDDEN_INPUT("ConductID", "ConductID", $ConductID) . "\n";
            $x .= '<div class="edit_bottom_v30">' . "\n";
            $x .= $SubmitBtn . "\n";
            $x .= $CancelBtn . "\n";
            $x .= '</div>' . "\n";
            $x .= '</form>';
            return $x;
    }

    public function Get_Setting_Conduct_Import_UI()
    {
        global $eReportCard, $Lang, $linterface, $PATH_WRT_ROOT, $LAYOUT_SKIN, $i_general_clickheredownloadsample;

        // page navigation (leave the array empty if no need)
        $PAGE_NAVIGATION[] = array(
            $eReportCard['Conduct'],
            "index.php"
        );
        $PAGE_NAVIGATION[] = array(
            $Lang['Btn']['Import']
        );

        // Btn
        $SubmitBtn = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "submit");
        $CancelBtn = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "window.location='index.php' ");

        $x .= '<form name="form1" action="import_update.php" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data">';
        $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) . "\n";
        $x .= '<table class="form_table_v30">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $Lang['General']['File'] . '</td>' . "\n";
        $x .= '<td>' . "\n";
        $x .= '<input class="file" type="file" id="userfile" name="userfile" >' . "\n";
        $x .= $linterface->Get_Form_Warning_Msg("WarnEmptyUserFile", $eReportCard['AlertSelectFile'], "WarnMsg");
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";
        $x .= '<div>' . "\n";
        $x .= '<a id="SampleCsvLink" class="tablelink" href="' . GET_CSV("sample.csv") . '" target="_blank">' . "\n";
        $x .= '<img src="' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">' . "\n";
        $x .= $i_general_clickheredownloadsample . "\n";
        $x .= '</a>' . "\n";
        $x .= '</div>' . "\n";
        $x .= '<div class="edit_bottom_v30">' . "\n";
        $x .= $SubmitBtn . "\n";
        $x .= $CancelBtn . "\n";
        $x .= '</div>' . "\n";
        $x .= '</form>';

        return $x;
    }

    public function Get_ExtraInfo_Conduct_Selection($ID_Name, $selected = '', $Onchange = '', $noFirst = 0)
    {
        global $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_extrainfo.php");
        $lreportcard_extrainfo = new libreportcard_extrainfo();
        $ConductArr = $lreportcard_extrainfo->Get_Conduct();
        $data = BuildMultiKeyAssoc($ConductArr, "ConductID", "Conduct", 1);

        $onchange = '';
        if ($Onchange != "")
            $onchange = 'onchange="' . $Onchange . '"';

            $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ' . $onchange;

            return getSelectByAssoArray($data, $selectionTags, $selected, $all = 0, $noFirst);
    }

    public function Get_ExtraInfo_Conduct_Selection_Disable_Option($ID_Name, $selected = '', $isFail = false, $disciplineType = false)
    {
        global $PATH_WRT_ROOT, $Lang;
        global $button_select, $i_status_all, $i_general_NotSet;

        $reasonStr = "";
        $reason2Str = "";

        include_once ($PATH_WRT_ROOT . "includes/libreportcard2008_extrainfo.php");
        $lreportcard_extrainfo = new libreportcard_extrainfo();

        // Tag for drop down list
        $empty_selected = ($selected == '' && trim(strlen($selected) == 0)) ? "SELECTED" : "";
        $selectionTags = ' id="' . $ID_Name . '" name="' . $ID_Name . '" ';

        // Build drop down list
        $x = "<SELECT $selectionTags>\n";
        $x .= "<OPTION value='' $empty_selected> " . "-- $button_select --" . " </OPTION>\n";

        // Get Conduct
        $ConductArr = $lreportcard_extrainfo->Get_Conduct();
        $ConductArr = BuildMultiKeyAssoc((array) $ConductArr, "ConductID", "Conduct", 1);

        // loop conduct
        while ($Conduct = each($ConductArr)) {
            $tempKey = $Conduct['key'];
            $tempValue = $Conduct['value'];

            $disabled = "";

            // Disable Condition 1
            // Any subject fail (at most B+)
            if ($isFail == 1 && strpos($tempValue, "A") !== false) {
                $disabled = " disabled";
                $reasonStr = $Lang['eReportCard']['WarningArr']['ConductDisplay']['FailSubject'];
            }

            // Disable Condition 2
            // Major Demerit (at most C-)
            if ($disciplineType == 3 && (strpos($tempValue, "A") !== false || strpos($tempValue, "B") !== false || (strpos($tempValue, "C") !== false && $tempValue != "C-"))) {
                $disabled = " disabled";
                $reason2Str = $Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline' . $disciplineType];
            }            // Minor Demerit (at most C+)
            else if ($disciplineType == 2 && (strpos($tempValue, "A") !== false || strpos($tempValue, "B") !== false)) {
                $disabled = " disabled";
                $reason2Str = $Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline' . $disciplineType];
            }
            // Misdemeanor / Light Demerit (at most B-)
            if ($disciplineType == 1 && (strpos($tempValue, "A") !== false || (strpos($tempValue, "B") !== false && $tempValue != "B-"))) {
                $disabled = " disabled";
                $reason2Str = $Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline' . $disciplineType];
            }

            if (is_array($selected)) {
                $sel_str = (isset($selected) && in_array($tempKey, $selected) ? "SELECTED" : "");
            } else {
                $sel_str = ($selected == $tempKey && $selected !== "" ? "SELECTED" : "");
            }

            $x .= "<OPTION value='" . htmlspecialchars($tempKey, ENT_QUOTES) . "' $sel_str $disabled>$tempValue</OPTION>\n";
        }

        $x .= "</SELECT>\n";

        // Disable Reasons
        $reasonStr = trim($reasonStr);
        $reason2Str = trim($reason2Str);
        if ($reasonStr != "" || $reason2Str != "") {
            $x .= "<br><br>" . $Lang['General']['Reason'] . ":\n";
            $x .= $reasonStr != "" ? "<br>&nbsp;<b><span style='color:red;'>- " . $reasonStr . "</span></b>" : "";
            $x .= $reason2Str != "" ? "<br>&nbsp;<b><span style='color:red;'>- " . $reason2Str . "</span></b>" : "";
        }

        return $x;
    }

    // Common Index with Form, Class, Report and View Format
    function Get_Report_Common_Index()
    {
        global $eReportCard, $linterface, $Lang;

        $x .= '<form id="form1" name="form1" method="POST">';
        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">' . "\n";
        $x .= '<tr>' . "\n";
        $x .= '<td  colspan="2" >' . "\n";

        // Get Form selection
        $libForm = new Year();
        $FormArr = $libForm->Get_All_Year_List();
        $ClassLevelID = $FormArr[0]['YearID'];
        $FormSelection = $this->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'js_Reload_Selection()', $noFirst = 1);

        // Select All Class Btn
        $SelectAllClassBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('YearClassIDArr[]')");

        $x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5" class="form_table_v30">' . "\n";

        // form selection
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['Form'] . '</td>' . "\n";
        $x .= '<td >' . $FormSelection . '</td>' . "\n";
        $x .= '</tr>' . "\n";
        // class selection
        $x .= '<tr class="ClassSelectOption">' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['Class'] . '</td>' . "\n";
        $x .= '<td id="ClassSelectionTD">' . "\n";
        $x .= '<span id="ClassSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!-- load on ajax --></span>' . $SelectAllClassBtn . "\n";
        $x .= '<br>' . "\n";
        $x .= $linterface->MultiSelectionRemark() . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";

        // Report Selection
        $x .= '<tr>' . "\n";
        $x .= '<td class="field_title">' . $eReportCard['Reports'] . '</td>' . "\n";
        $x .= '<td ><span id="ReportSelectionSpan">' . $linterface->Get_Ajax_Loading_Image() . '<!--load on ajax--></span></td>' . "\n";
        $x .= '</tr>' . "\n";

        // CSV / Html
        $x .= '<tr>' . "\n";
        $x .= '<td  class="field_title">' . $eReportCard['ViewFormat'] . '</td>' . "\n";
        $x .= '<td class="tabletext">';
        $x .= '<input type="radio" id="ViewFormatHTML" name="ViewFormat" value="html" checked ><label for="ViewFormatHTML">' . $eReportCard['HTML'] . '</label>&nbsp;';
        $x .= '<input type="radio" id="ViewFormatCSV" name="ViewFormat" value="csv"><label for="ViewFormatCSV">' . $eReportCard['CSV'] . '</label>';
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table><br><br>' . "\n";
        $x .= '</td>' . "\n";
        $x .= '</tr>' . "\n";
        $x .= '</table>' . "\n";

        $x .= '<div class="edit_bottom_v30">';
        $x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Check_Form();", "submitBtn");
        $x .= '</div>';
        $x .= '</form>' . "\n";

        return $x;
    }

    // $border, e.g. "left,right,top"
    function Get_Double_Line_Td($border = "", $width = 1)
    {
        global $image_path, $LAYOUT_SKIN;

        $borderArr = explode(",", $border);
        $left = $right = $top = $bottom = 0;

        if (in_array("left", $borderArr))
            $left = $width;
            if (in_array("right", $borderArr))
                $right = $width;
                if (in_array("top", $borderArr))
                    $top = $width;
                    if (in_array("bottom", $borderArr))
                        $bottom = $width;

                        $style = " style='border-style:solid; border-color:#000; border-width:{$top}px {$right}px {$bottom}px {$left}px ' ";

                        $td = "<td width='0%' height='0%' $style><img src='" . $image_path . "/" . $LAYOUT_SKIN . "/10x10.gif' width='$width' height='$width'/></td>";
                        return $td;
    }

    function Get_Mgmt_DataHandling_Tab_Array($curTab)
    {
    	global $PATH_WRT_ROOT, $plugin, $eReportCard, $eRCTemplateSetting, $Lang, $sys_custom;

        $ary = array();
        $ary[] = array(
            $eReportCard['DataTransition'],
            "transition.php",
            ($curTab == 'dataTransition')
        );

        if ($plugin['iPortfolio'] || $plugin['StudentDataAnalysisSystem']) {
            // SIS Data handling Hide transfer to iPortfolio
            $showToiPortfolio = true;
            if ($eRCTemplateSetting['DataHandling']['HideToPortfolio']) {
                $showToiPortfolio = false;
            }
            if ($showToiPortfolio) {
                $ary[] = array(
                    $eReportCard['DataTransfer_To_iPortfolio'],
                    "transfer_to_iportfolio.php?clearCoo=1",
                    ($curTab == 'to_iPf')
                );
            }
        }

        $ary[] = array(
            $eReportCard['DataDeletion'],
            "deletion.php",
            ($curTab == 'deletion')
        );

        if ($eRCTemplateSetting['Management']['DataHandling']['CopyMarksheet']) {
            $ary[] = array(
                $Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyMarksheet'],
                "copy_marksheet.php?clearCoo=1",
                ($curTab == 'copyMarksheet')
            );
        }

        // Add Tab - "Data Export (to WebSAMS)" [2014-1121-1759-15164]
        if ($eRCTemplateSetting['Management']['DataHandling']['ExportToWebSAMS']) {
            $ary[] = array(
                $eReportCard['DataExport_To_WebSAMS'],
                "export_to_websams.php?clearCoo=1",
                ($curTab == 'exporttoWebSAMS')
            );
        }

        // 2020-04-07 Add Tab - Data Transfer to WebSAMS
        if($sys_custom['eRC']['Management']['TransferToWebSAMS']) {
        	$ary[] = array(
        			$eReportCard['DataTransfer_To_WebSAMS'],
        			"transfer_to_websams.php?clearCoo=1",
        			($curTab == 'transfertoWebSAMS')
        	);
        }
        return $ary;
    }

    function Get_Settings_BasicSettings_Tab_Array($curTab)
    {
        global $eReportCard, $PATH_WRT_ROOT;

        $ary = array();
        $ary[] = array(
            $eReportCard['StyleSettings'],
            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/highlight.php",
            ($curTab == 'StyleSettings')
        );
        $ary[] = array(
            $eReportCard['MarkStorageAndDisplay'],
            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/mark_storage_display.php",
            ($curTab == 'MarkStorageAndDisplay')
        );
        $ary[] = array(
            $eReportCard['AbsentAndExemptSettings'],
            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/absent_exempt.php",
            ($curTab == 'AbsentAndExemptSettings')
        );
        $ary[] = array(
            $eReportCard['CalculationSettings'],
            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/calculation.php",
            ($curTab == 'CalculationSettings')
        );
        $ary[] = array(
            $eReportCard['AccessSettings'],
            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/access_setting.php",
            ($curTab == 'AccessSettings')
        );
        $ary[] = array(
            $eReportCard['SpecificIP'],
            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/access_setting.php",
            ($curTab == 'SpecificIP')
        );

        return $ary;
    }

    // Settings > Main Subject
    function Get_Settings_Form_Main_Subject_UI($ClassLevelID)
    {
        global $eReportCard, $Lang, $linterface;

        // Form Drop Down List
        $FormSelection = $this->Get_Form_Selection("ClassLevelID", $ClassLevelID, "js_Reload_Subject_Class_Table()", 1);

        // Table
        $x = "";
        $x .= '<br>';
        $x .= '<div class="table_filter">' . "\n";
        $x .= $FormSelection;
        $x .= '</div>' . "\n";
        $x .= '<br>';
        $x .= '<br>';
        $x .= '<div id="SubjectClassTable"></div>' . "\n";

        return $x;
    }

    function Get_Form_Main_Subject_Table($ClassLevelID)
    {
        global $eReportCard, $Lang, $linterface, $lreportcard, $LAYOUT_SKIN, $image_path;

        // Save Button
        $btn_Save = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Save_Subject_Class()", "Btn_Save");

        // Class info
        $ClassInfoArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, "", 1, 0);
        $numOfClass = count($ClassInfoArr);

        // Subject info
        $SubjectArr = $this->returnSubjectwOrder($ClassLevelID, 0, '', '', 'Desc', 1);
        $numOfSubject = count($SubjectArr);

        // Subject Related Info
        $SubjectIDArr = array_keys((array) $SubjectArr);
        $SubjectCodeMapping = $this->GET_SUBJECTS_CODEID_MAP();
        $SubjectClassInfoArr = $this->Get_Form_Main_Subject($ClassLevelID, 1);

        $x = "";
        $x .= '<form id="form1" name="form1" method="post">' . "\n";
        $x .= '<table id="ContentTable" class="common_table_list_v30 edit_table_list_v30">' . "\n";

        // $x .= '<colgroup>'."\n";
        // $x .= '<col style="width:20%;" />'."\n";
        // $x .= '<col style="width:10%;" />'."\n";
        // $classWidth = ($numOfClass != 0) ? floor(70 / $numOfClass) : 0;
        // foreach ((array)$ClassInfoArr as $thisClassID => $thisClassName) {
        // $x .= '<col style="text-align:center; width:'.$classWidth.'%;" />'."\n";
        // }
        // $x .= '</colgroup>'."\n";

        $x .= '<tr>' . "\n";
        $x .= '<th style="text-align:center;" width="40%" valign="top">' . $eReportCard['Subject'] . '</th>' . "\n";
        $x .= '<th style="text-align:center;" width="15%" valign="top">' . $eReportCard['Code'] . '</th>' . "\n";
        // $x .= '<th class="apply_all_top" style="text-align:center;">'.$Lang['Btn']['ApplyToAll'].'</th>'."\n";
        $x .= '<th class="apply_all_top_bulk" style="text-align:center;" width="15%">';
        $x .= $Lang['eReportCard']['SettingsArr']['MainSubjectsArr']['MainSubject'];
        $x .= '&nbsp;<a href="javascript:jsApplyToAllRadio(\'0\');"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_assign.gif" width="12" height="12" border="0"><br />';
        $x .= $linterface->Get_Radio_Button("Check_Main_All", "Check_All", 0, 0);
        $x .= '</th>' . "\n";
        $x .= '<th class="apply_all_top_bulk" style="text-align:center;" width="15%">';
        $x .= $Lang['eReportCard']['SettingsArr']['MainSubjectsArr']['Subject'];
        $x .= '&nbsp;<a href="javascript:jsApplyToAllRadio(\'1\');"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_assign.gif" width="12" height="12" border="0"><br />';
        $x .= $linterface->Get_Radio_Button("Check_Subject_All", "Check_All", 1, 0);
        $x .= '</th>' . "\n";
        $x .= '<th class="apply_all_top_bulk" style="text-align:center;" width="15%">';
        $x .= $Lang['eReportCard']['SettingsArr']['MainSubjectsArr']['TechnicalSubject'];
        $x .= '&nbsp;<a href="javascript:jsApplyToAllRadio(\'2\');"><img src="' . $image_path . '/' . $LAYOUT_SKIN . '/icon_assign.gif" width="12" height="12" border="0"><br />';
        $x .= $linterface->Get_Radio_Button("Check_TSubject_All", "Check_All", 2, 0);
        $x .= '</th>' . "\n";
        // // Check All
        // $thisChkID = 'AllCheck';
        // $thisChkClass = 'AllCheck';
        // $thisChkOnClick = "js_Select_All(this.checked);";
        // $x .= $linterface->Get_Checkbox($thisChkID, $thisChkName='', $Value=1, $thisChecked=0, $thisChkClass, $Display='', $thisChkOnClick)."\n";
        $x .= '</tr>' . "\n";

        // loop Subject
        foreach ((array) $SubjectArr as $thisSubjectID => $_cmpSubjectArr) {
            $formSubjectInfo = $SubjectClassInfoArr[$thisSubjectID];
            $SubjectName = $_cmpSubjectArr[0];

            $x .= '<tr>' . "\n";
            $x .= '<td style="text-align:center;">' . $SubjectName . '</td>' . "\n";
            $x .= '<td style="text-align:center;">' . $SubjectCodeMapping[$thisSubjectID] . '</td>' . "\n";
            // $x .= '<td id="td_checksubject['.$thisSubjectID.']" style="text-align:center;">'."\n";
            // // Check All Class Of Subject
            // $thisChkID = 'SubjectAllCheck_'.$thisSubjectID;
            // $thisChkClass = 'SubjectAllCheck SubjectAllCheck_SubjectID_'.$thisSubjectID;
            // $thisChkOnClick = "js_Select_All_Class_Of_Subject(this.checked, $thisSubjectID);";
            // $x .= $linterface->Get_Checkbox($thisChkID, $thisChkName='', $Value=1, $thisChecked=0, $thisChkClass, $Display='', $thisChkOnClick)."\n";
            // $x .= '</td>'."\n";
            // foreach ((array)$ClassInfoArr as $thisClassID => $thisClassName) {

            // Radio box
            for ($count = 0; $count < 3; $count ++) {
                // Info
                $thisChkID = 'Check_' . $thisSubjectID . '_' . $count;
                $thisChkName = 'SubjectClassArr[' . $thisSubjectID . ']';
                $thisChecked = (isset($formSubjectInfo) && $formSubjectInfo["DisplayStatus"] == $count) ? 1 : 0;

                // $thisChkClass = 'Checkbox Checkbox_SubjectID_'.$thisSubjectID;
                // $thisChkOnClick = "js_Select(this.checked, $thisSubjectID);";
                // $tdBackgroundColor = ($thisChecked) ? '#E1FCCC' : '#FFFFFF';

                // $x .= '<td id="td_checkbox_'.$thisSubjectID.'" class="td_checkbox td_checkbox_SubjectID_'.$thisSubjectID.'" style="text-align:center; background-color:'.$tdBackgroundColor.'">'."\n";
                $x .= '<td style="text-align:center;">' . "\n";
                // $x .= $linterface->Get_Checkbox($thisChkID, $thisChkName, $Value=1, $thisChecked, $thisChkClass, $Display='', $thisChkOnClick)."\n";
                $x .= $linterface->Get_Radio_Button($thisChkID, $thisChkName, $count, $thisChecked);
                $x .= '</td>' . "\n";
            }

            $x .= '</tr>' . "\n";
        }
        $x .= '</table>' . "\n";
        $x .= '<input type="hidden" name="SubjectIDList" value="' . implode(",", (array) $SubjectIDArr) . '" />';
        $x .= '<input type="hidden" name="ClassLevelID" value="' . $ClassLevelID . '" />';

        $x .= '<div class="edit_bottom_v30">';
        $x .= $btn_Save;
        $x .= '<p class="spacer" />';
        $x .= '</div>';
        $x .= '</form>' . "\n";
        return $x;
    }

    // #############################################################################################
    // Management > Class Teacher Comment [Start]
    public function Get_Import_Class_Teacher_Comment_Step1_UI($ClassLevelID, $ClassID = '', $ReportID)
    {
        global $linterface, $lreportcard_comment;
        global $eReportCard, $Lang;

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['ClassTeacherComment'],
            "javascript:js_Go_Class_Teacher_Comment();"
        );
        if ($ClassID != '')
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['Edit'],
                "javascript:js_Go_Edit();"
            );
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['Import'],
                ""
            );

            // ## Import Remarks
            $RemarksTable = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportStudentRemarks'], $others = "");

            // ## Get Report Card Info
            $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
            $ReportTitle = $ReportInfoArr['ReportTitle'];
            $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
            $FormName = $this->returnClassLevel($ClassLevelID);
            if ($ClassID != '') {
                $ClassNameArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID, $returnAssoName = 1);
                $ClassName = $ClassNameArr[$ClassID];
            }

            // ## Column Description
            $ColumnTitleArr = $lreportcard_comment->Get_Class_Teacher_Comment_Csv_Header_Title();
            $ColumnTitleArr = Get_Lang_Selection($ColumnTitleArr['Ch'], $ColumnTitleArr['En']);
            $ColumnPropertyArr = $lreportcard_comment->Get_Class_Teacher_Comment_Csv_Header_Property();
            $ColumnDisplay = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr);

            // ## Buttons
            $ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();");
            $CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");

            $x = '';
            $x .= '<form id="form1" name="form1" action="import_step2.php" method="POST" enctype="multipart/form-data">' . "\n";
            $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
            $x .= '<br style="clear:both;" />' . "\n";
            $x .= $linterface->GET_IMPORT_STEPS($CurrStep = 1);
            $x .= '<br style="clear:both;" />' . "\n";
            $x .= '<div align="center" style="width:100%;">' . $RemarksTable . '</div>' . "\n";
            $x .= '<div class="table_board">' . "\n";
            $x .= '<table id="html_body_frame" width="100%">' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<table class="form_table_v30">' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $eReportCard['Period'] . '</td>' . "\n";
            $x .= '<td>' . $ReportTitle . '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $eReportCard['Form'] . '</td>' . "\n";
            $x .= '<td>' . $FormName . '</td>' . "\n";
            $x .= '</tr>' . "\n";
            if ($ClassID != "") {
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title">' . $eReportCard['Class'] . '</td>' . "\n";
                $x .= '<td>' . $ClassName . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
            // [2017-0907-1704-15258]
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $eReportCard['ManagementArr']['ClassTeacherCommentArr']['ImportMode'] . '</td>' . "\n";
            $x .= '<td class="tabletext">' . "\n";
            $x .= $linterface->Get_Radio_Button('insertNew', 'importMode', 'new', $isChecked = 1, $Class = "", $eReportCard['ManagementArr']['ClassTeacherCommentArr']['ImportNew'], $Onclick = "onClickImportMode(this);", $isDisabled = 0);
            $x .= $linterface->Get_Radio_Button('appendExist', 'importMode', 'append', $isChecked = 0, $Class = "", $eReportCard['ManagementArr']['ClassTeacherCommentArr']['AppendExisting'], $Onclick = "onClickImportMode(this);", $isDisabled = 0);
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['General']['SourceFile'] . ' <span class="tabletextremark">' . $Lang['General']['CSVFileFormat'] . '</span></td>' . "\n";
            $x .= '<td class="tabletext">' . "\n";
            $x .= '<input class="file" type="file" name="csvfile" id="csvfile">' . "\n";
            $x .= '<br />' . "\n";
            $x .= '<span class="tabletextrequire">' . $Lang['General']['ImportArr']['FirstRowWarn'] . '</span>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['General']['CSVSample'] . '</td>' . "\n";
            $x .= '<td>' . $linterface->Get_CSV_Sample_Download_Link('javascript:js_Go_Export();') . '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . "\n";
            $x .= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn'] . "\n";
            $x .= '</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= $ColumnDisplay . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="tabletextremark" colspan="2">' . "\n";
            $x .= $linterface->MandatoryField();
            $x .= $linterface->ReferenceField();
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table>' . "\n";
            $x .= '</div>' . "\n";

            // Buttons
            $x .= '<div class="edit_bottom_v30">' . "\n";
            $x .= $ContinueBtn;
            $x .= "&nbsp;";
            $x .= $CancelBtn;
            $x .= '</div>' . "\n";

            // Hidden Field
            $x .= '<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="' . $ClassLevelID . '"/>' . "\n";
            $x .= '<input type="hidden" name="ClassID" id="ClassID" value="' . $ClassID . '"/>' . "\n";
            $x .= '<input type="hidden" name="ReportID" id="ReportID" value="' . $ReportID . '"/>' . "\n";

            $x .= '</form>' . "\n";
            $x .= '<br />' . "\n";

            return $x;
    }

    function Get_Import_Class_Teacher_Comment_Step2_UI($ClassLevelID, $ClassID = '', $ReportID, $TargetFilePath, $numOfCsvData, $importMode = "")
    {
        global $linterface, $lreportcard_comment;
        global $eReportCard, $Lang;

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['ClassTeacherComment'],
            "javascript:js_Go_Class_Teacher_Comment();"
        );
        if ($ClassID != '')
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['Edit'],
                "javascript:js_Go_Edit();"
            );
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['Import'],
                ""
            );

            // ## Get Report Card Info
            $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
            $ReportTitle = $ReportInfoArr['ReportTitle'];
            $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
            $FormName = $this->returnClassLevel($ClassLevelID);
            if ($ClassID != '') {
                $ClassNameArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID, $returnAssoName = 1);
                $ClassName = $ClassNameArr[$ClassID];
            }

            // ## Buttons
            $ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();", 'ContinueBtn', '', $Disabled = 1);
            $BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Back_To_Import_Step1();");
            $CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Cancel();");

            $x = '';
            $x .= '<form id="form1" name="form1" method="post" action="import_step3.php">' . "\n";
            $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
            $x .= '<br style="clear:both;" />' . "\n";
            $x .= $linterface->GET_IMPORT_STEPS($CurrStep = 2);
            $x .= '<div class="table_board">' . "\n";
            $x .= '<table id="html_body_frame" width="100%">' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<table class="form_table_v30">' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $eReportCard['ReportTitle'] . '</td>' . "\n";
            $x .= '<td>' . $ReportTitle . '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Form'] . '</td>' . "\n";
            $x .= '<td>' . $FormName . '</td>' . "\n";
            $x .= '</tr>' . "\n";
            if ($ClassID != '') {
                $x .= '<tr>' . "\n";
                $x .= '<td class="field_title">' . $Lang['SysMgr']['FormClassMapping']['Class'] . '</td>' . "\n";
                $x .= '<td>' . $ClassName . '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['General']['SuccessfulRecord'] . '</td>' . "\n";
            $x .= '<td><div id="SuccessCountDiv">' . $Lang['General']['EmptySymbol'] . '</div></td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="field_title">' . $Lang['General']['FailureRecord'] . '</td>' . "\n";
            $x .= '<td><div id="FailCountDiv">' . $Lang['General']['EmptySymbol'] . '</div></td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table>' . "\n";
            $x .= '<br style="clear:both;" />' . "\n";
            $x .= '<div id="ErrorTableDiv"></div>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table>' . "\n";
            $x .= '</div>' . "\n";

            // Buttons
            $x .= '<div class="edit_bottom_v30">' . "\n";
            $x .= $ContinueBtn;
            $x .= '&nbsp;' . "\n";
            $x .= $BackBtn;
            $x .= '&nbsp;' . "\n";
            $x .= $CancelBtn;
            $x .= '</div>' . "\n";

            // iFrame for Validation
            $thisSrc = "ajax_validate.php?Action=Import_Class_Teacher_Comment&ReportID=" . $ReportID . "&ClassID=" . $ClassID . "&TargetFilePath=" . $TargetFilePath;
            $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="' . $thisSrc . '" style="width:100%;height:300px;display:none;"></iframe>' . "\n";
            // $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;"></iframe>'."\n";

            // Hidden Field for Step 3
            $x .= '<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="' . $ClassLevelID . '"/>' . "\n";
            $x .= '<input type="hidden" id="ReportID" name="ReportID" value="' . $ReportID . '" />' . "\n";
            $x .= '<input type="hidden" id="ClassID" name="ClassID" value="' . $ClassID . '" />' . "\n";
            $x .= '<input type="hidden" id="numOfCsvData" name="numOfCsvData" value="' . $numOfCsvData . '" />' . "\n";
            $x .= '<input type="hidden" id="importMode" name="importMode" value="' . $importMode . '" />' . "\n";

            $x .= '</form>' . "\n";
            $x .= '<br />' . "\n";

            return $x;
    }

    function Get_Import_Class_Teacher_Comment_Step3_UI($ReportID, $ClassLevelID, $ClassID = '', $importMode = "")
    {
        global $Lang, $eReportCard;
        global $lreportcard_award, $linterface;

        // ## Navaigtion
        $PAGE_NAVIGATION[] = array(
            $eReportCard['ClassTeacherComment'],
            "javascript:js_Go_Class_Teacher_Comment();"
        );
        if ($ClassID != '')
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['Edit'],
                "javascript:js_Go_Edit();"
            );
            $PAGE_NAVIGATION[] = array(
                $Lang['Btn']['Import'],
                ""
            );

            // ## Get Report Card Info
            $ReportInfoArr = $this->returnReportTemplateBasicInfo($ReportID);
            $ReportTitle = $ReportInfoArr['ReportTitle'];
            $ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
            $FormName = $this->returnClassLevel($ClassLevelID);
            if ($ClassID != '') {
                $ClassNameArr = $this->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID, $returnAssoName = 1);
                $ClassName = $ClassNameArr[$ClassID];
            }

            // ## Buttons
            $ImportOtherBtn = $linterface->GET_ACTION_BTN($Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportOtherComments'], "button", "js_Back_To_Import_Step1();");
            $BackBtn = $linterface->GET_ACTION_BTN($Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['GoBackToCommentView'], "button", "js_Back();");

            $x = '';
            $x .= '<form id="form1" name="form1" method="post">' . "\n";
            $x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
            $x .= '<br style="clear:both;" />' . "\n";
            $x .= $linterface->GET_IMPORT_STEPS($CurrStep = 3);
            $x .= '<div class="table_board">' . "\n";
            $x .= '<table id="html_body_frame" width="100%">' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<table class="form_table_v30">' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td class="tabletext" style="text-align:center;">' . "\n";
            $x .= '<span id="ImportStatusSpan"></span>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table>' . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            $x .= '</table>' . "\n";
            $x .= '</div>' . "\n";

            // Buttons
            $x .= '<div class="edit_bottom_v30">' . "\n";
            $x .= $ImportOtherBtn;
            $x .= '&nbsp;' . "\n";
            $x .= $BackBtn;
            $x .= '</div>' . "\n";

            // iFrame for Import Data
            $thisSrc = "ajax_update.php?Action=Import_Class_Teacher_Comment&ReportID=$ReportID&ImportMode=$importMode";
            $x .= '<iframe id="ImportIFrame" name="ImportIFrame" src="' . $thisSrc . '" style="width:100%;height:300px;display:none;"></iframe>' . "\n";

            $x .= '</form>' . "\n";
            $x .= '<br />' . "\n";

            return $x;
    }

    // Management > Class Teacher Comment [End]
    // ######################################################################################################    // ######################################################################################################
    // Management > Conduct Grade Start
    //
    function Get_Conduct_Grade_Calculation_Overview_UI($TermID, $FormID = '')
    {
        global $linterface, $eRCTemplateSetting, $eReportCard, $Lang, $ck_ReportCard_UserType;

        $checkPermission = 0;
        $FormSelection = $this->Get_Form_Selection("FormID", $FormID, "js_Reload_Other_Info_Table()", $noFirst = 0, $isAll = 0, $hasTemplateOnly = 0, $checkPermission, $IsMultiple = 0, $excludeWithoutClass = 1);
        $TermSelection = $this->Get_Term_Selection("TermID", $this->schoolYearID, $TermID, "js_Reload_Other_Info_Table()", $NoFirst = 1, $WithWholeYear = 1);

        $generateBtn = $linterface->Get_Content_Tool_v30('generate', $href = "javascript:js_Generate();", $text = "", $options = "", $other = "", $divID = '');

        $x .= '<div class="content_top_tool">'."\n";
            $x .= '<div class="Conntent_tool">'."\n";
                $x .= $generateBtn."\n";
                $x .= '<br style="clear:both" />'."\n";
            $x .= '</div>'."\n";
        $x .= '</div>'."\n";
        $x .= '<br style="clear: both;">'."\n";

        $x .= '<div class="table_board">'."\n";
            $x .= '<table width="100%" cellspacing="0" cellpadding="0" border="0">'."\n";
                $x .= '<tbody>'."\n";
                    $x .= '<tr>'."\n";
                        $x .= '<td valign="bottom">'."\n";
                            $x .= '<div class="table_filter">'."\n";
                                $x .= $FormSelection;
                                $x .= $TermSelection;
                            $x .= '</div>'."\n";
                            $x .= '<br style="clear: both;">'."\n";
                        $x .= '</td>'."\n";
                        $x .= '<td valign="bottom">&nbsp;</td>'."\n";
                    $x .= '</tr>'."\n";
                $x .= '</tbody>'."\n";
            $x .= '</table>'."\n";

            $x .= '<div id="OtherInfoTableDiv">';
                $x .= $this->Get_Conduct_Grade_Calculation_Overview_Table($TermID, $FormID);
            $x .= '</div>';
        $x .= '</div>'."\n";

        return $x;
    }

    function Get_Conduct_Grade_Calculation_Overview_Table($TermID, $FormID = '')
    {
        global $eReportCard, $Lang;

        include_once("libinterface.php");
        $linterface = new interface_html();

        $FormArr = BuildMultiKeyAssoc($this->Get_User_Accessible_Form(), "ClassLevelID");
        $ClassInfoArr = $this->Get_User_Accessible_Class();
        $ClassArr = BuildMultiKeyAssoc($ClassInfoArr, array("ClassLevelID", "ClassID"));
        $ClassIDArr = Get_Array_By_Key($ClassInfoArr, "ClassID");

        $reportIDArr = array();
        $reportYearIDIncluded = array();

        $reportTermID = ($TermID == '' || $TermID == 0) ? 'F' : $TermID;
        $reportList = $this->Get_Report_List($FormID, '', 1);
        foreach($reportList as $thisReportInfo) {
            if($thisReportInfo['Semester'] == $reportTermID) {
                if(in_array($thisReportInfo['ClassLevelID'], $reportYearIDIncluded)) {
                    continue;
                }

                $reportIDArr[] = $thisReportInfo['ReportID'];
                $reportYearIDIncluded[] = $thisReportInfo['ClassLevelID'];
            }
        }

        $LastModifiedInfo = $this->Get_Generated_Conduct_Grade_Last_Modified_Info($reportIDArr, $ClassIDArr);
        $LastModifiedInfo = BuildMultiKeyAssoc($LastModifiedInfo, "YearClassID");

        $x .= '<table id="OtherInfoTable" class="common_table_list_v30">'."\n";

        // Col Group
        $x .= '<col align="left" style="width: 20%;">'."\n";
        $x .= '<col align="left">'."\n";
        $x .= '<col align="left">'."\n";
        $x .= '<col align="left">'."\n";
        $x .= '<col align="left" style="width: 100px;">'."\n";

        // Table Head
        $ColSpan = "4";
        $x .= '<thead>'."\n";
            $x .= '<tr>'."\n";
                $x .= '<th>'.$Lang['General']['Form'].'</th>'."\n";
                $x .= '<th colspan="'.$ColSpan.'" class="sub_row_top">'.$Lang['General']['Class'].'</th>'."\n";
            $x .= '</tr>'."\n";
            $x .= '<tr>'."\n";
                $x .= '<th >'.$Lang['General']['Name'].'</th>'."\n";
                $x .= '<th class="sub_row_top">'.$Lang['General']['Name'].'</th>'."\n";
                $x .= '<th class="sub_row_top">'.$Lang['General']['LastModified'].'</th>'."\n";
                $x .= '<th class="sub_row_top">'.$Lang['General']['LastModifiedBy'].'</th>'."\n";
                $x .= '<th class="sub_row_top">&nbsp;</th>'."\n";
            $x .= '</tr>'."\n";
        $x .= '</thead>'."\n";

        // Table Body
        $x .= '<tbody>'."\n";

        // loop terms
        foreach ((array)$ClassArr as $ClassLevelID => $ClassInfoArr) {
            if (!($FormInfo = $FormArr[$ClassLevelID])) {
                continue;
            }

            // filter by Form
            if ($FormID != $FormInfo["ClassLevelID"] && $FormID != '' && $FormID != 0) {
                continue;
            }
            $NumOfClass = count($ClassInfoArr) + 1;

            // No report settings
            $noReportSettings = !in_array($ClassLevelID, $reportYearIDIncluded);

            $x .= '<tr>'."\n";
                $x .= '<td rowspan="'.$NumOfClass.'">'.$FormInfo['LevelName'].'</td>'."\n";
            $x .= '</tr>'."\n";

            // loop classes
            foreach ((array) $ClassInfoArr as $thisClassID => $thisClassInfo) {
                // Get Class Student
                $StudentArr = $this->GET_STUDENT_BY_CLASS($thisClassID);
                $StudentIDArr = Get_Array_By_Key($StudentArr, 'UserID');

                $LastModifiedDate = Get_String_Display($LastModifiedInfo[$thisClassID]['DateModified']);
                $LastModifiedBy = Get_String_Display($LastModifiedInfo[$thisClassID]['NameField']);
                $noGradeGeneration = empty($LastModifiedInfo[$thisClassID]);

                $x .= '<tr class="sub_row">'."\n";
                    $x .= '<td>'.$thisClassInfo['ClassName'].'</td>'."\n";
                    if (empty($StudentIDArr)) {
                        $x .= '<td colspan="3" align="center">'.$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoStudentInClass'].'</td>';
                    }
                    else if($noReportSettings) {
                        $x .= '<td colspan="3" align="center">'.$eReportCard['NoReportAvailable'].'</td>';
                    }
                    else if ($noGradeGeneration) {
                        $x .= '<td colspan="3" align="center">'.$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['WarningMsgArr']['NotGenerated'].'</td>';
                    }
                    else {
                        $x .= '<td>'.$LastModifiedDate.'</td>'."\n";
                        $x .= '<td>'.$LastModifiedBy.'</td>'."\n";
                        $x .= '<td>'."\n";
                            $x .= $linterface->Get_Action_Lnk('edit.php?TermID='.$TermID.'&ClassID='.$thisClassID, "Edit Conduct", NULL, "edit_dim");
                        $x .= '</td>'."\n";
                    }
                $x .= '</tr>'."\n";
            }
        }

        $x .= '</tbody>'."\n";
        $x .= '</table>'."\n"."\n";

        return $x;
    }

    function Get_Conduct_Grade_Modify_Edit_UI($TermID, $ClassID)
    {
        global $linterface, $PATH_WRT_ROOT, $eReportCard, $Lang;
        global $eRCTemplateSetting;

        // Get Class Name
        include_once ("form_class_manage.php");
        $YearClassObj = new year_class($ClassID, 1);
        $ClassName = $YearClassObj->ClassTitleEN;
        $YearName = $YearClassObj->YearName;
        $YearID = $YearClassObj->YearID;
        $checkPermission = ($this->IS_ADMIN_USER($_SESSION['UserID']) || $this->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) ? 0 : 1;
        $ClassSelection = $this->Get_Class_Selection("ClassID", $YearID, $ClassID, "js_Reload_Page();", $noFirst = 1, $isAll = 0, $checkPermission);

        // Get Term / Year Report
        $reportTermID = ($TermID == '' || $TermID == 0) ? 'F' : $TermID;
        $targetReportInfo = $this->returnReportTemplateBasicInfo('', '', $YearID, $reportTermID, 1);
        $TermSelection = $this->Get_Term_Selection("TermID", $this->schoolYearID, $TermID, "js_Reload_Page();", $NoFirst = 1, $WithWholeYear = 1);

        // Page Navigation
        $PAGE_NAVIGATION[] = array($eReportCard['Management_ConductGradeCalculation'], "javascript:js_Go_Back_To_Other_Info()");
        $PAGE_NAVIGATION[] = array($TermSelection, "");
        $PAGE_NAVIGATION[] = array($ClassSelection, "");
        $PageNavigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

        //$generateBtn = $linterface->Get_Content_Tool_v30('generate', $href = "javascript:js_Generate();", $text = "", $options = "", $other = "", $divID = '');

        $x .= $PageNavigation;
        $x .= '<br style="clear: both;">' . "\n";

        //$x .= '<div class="content_top_tool">' . "\n";
        //    $x .= '<div class="Conntent_tool">' . "\n";
        //        $x .= $generateBtn . "\n";
        //        $x .= '<br style="clear:both" />' . "\n";
        //    $x .= '</div>' . "\n";
        //$x .= '</div>' . "\n";
        //$x .= '<br style="clear: both;">' . "\n";
        //$x .= '<br style="clear:both;" />' . "\n";

        $x .= '<div class="table_board">' . "\n";
            $x .= $this->Get_Conduct_Grade_Modify_Table($targetReportInfo['ReportID'], $TermID, $ClassID);
        $x .= '</div>' . "\n";

        return $x;
    }

    function Get_Conduct_Grade_Modify_Table($ReportID, $TermID, $ClassID)
    {
        global $Lang, $eReportCard;
        global $eRCTemplateSetting;

        include_once ("libinterface.php");
        $linterface = new interface_html();

        // Get Class Student
        $StudentArr = $this->GET_STUDENT_BY_CLASS($ClassID);
        $StudentIDArr = Get_Array_By_Key($StudentArr, 'UserID');

        // Get Student Conduct
        $studentConductInfo = array();
        if($ReportID && !empty($StudentIDArr)) {
            $studentConductInfo = $this->Get_Student_Conduct_Grade($ReportID, $StudentIDArr);
            $studentConductInfo = BuildMultiKeyAssoc($studentConductInfo, 'StudentID', array('ConductScore', 'ConductGrade', 'ModifiedGrade'));
        }
        //$noGradeGeneration = empty($thisStudentConductInfo);

        $SubmitBtn = '';
        $CancelBtn = '';
        $warningMsg = '';
        if (empty($StudentIDArr)) {
            $warningMsg = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoStudentInClass'];
        }
        else if ($ReportID == '') {
            $warningMsg = $eReportCard['NoReportAvailable'];
        }
        else if (empty($studentConductInfo)) {
            $warningMsg = $Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['WarningMsgArr']['NotGenerated'];
        }
        else {
            // Get Btn
            $SubmitBtn = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "submit", "", "submitBtn");
            $CancelBtn = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Go_Back_To_Other_Info()");
        }

        $x .= '<form id="form1" name="form1" action="update.php" method="POST" onsubmit="return js_Check_Form();">'."\n";
            $x .= '<table id="OtherInfoTable" class="common_table_list_v30 edit_table_list_v30">'."\n";

            // Col group
            $x .= '<col align="left">'."\n";
            $x .= '<col align="left">'."\n";
            $x .= '<col align="left">'."\n";
            $x .= '<col align="left" width="20%">'."\n";
            $x .= '<col align="left" width="20%">'."\n";
            $x .= '<col align="left" width="20%">'."\n";

            // Table Head
            $x .= '<thead>'."\n";
            $x .= '<tr>'."\n";
                $x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['Class'].'</th>'."\n";
                $x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>'."\n";
                $x .= '<th>'.$eReportCard['Student'].'</th>'."\n";
                $x .= '<th>'.$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['ConductScore'].'</th>'."\n";
                $x .= '<th>'.$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['ConductGrade'].'</th>'."\n";
                $x .= '<th>'.$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['ModifyGrade'].'</th>'."\n";
            $x .= '</tr>'."\n";
            $x .= '</thead>'."\n";

            // Table Body
            $x .= '<tbody>'."\n";
            if($warningMsg != '')
            {
                $x .= '<tr>'."\n";
                    $x .= '<td colspan="6" align="center">'.$warningMsg.'</td>'."\n";
                $x .= '</tr>'."\n";
            }
            else
            {
                $NumOfStudent = sizeof($StudentArr);
                for ($i=0; $i<$NumOfStudent; $i++)
                {
                    //$thisStudentConductInfo = $this->Get_Student_Conduct_Grade($ReportID, array($StudentArr[$i]['UserID']));
                    //$thisStudentConductInfo = $thisStudentConductInfo[0];
                    $thisStudentConductInfo = $studentConductInfo[$StudentArr[$i]['UserID']];

                    $x .= '<tr>'."\n";
                        $x .= '<td>'.$StudentArr[$i]['ClassName'].'</td>'."\n";
                        $x .= '<td>'.$StudentArr[$i]['ClassNumber'].'</td>'."\n";
                        $x .= '<td>'.Get_Lang_Selection($StudentArr[$i]['StudentNameCh'], $StudentArr[$i]['StudentNameEn']).'</td>'."\n";
                        $x .= '<td>'.$thisStudentConductInfo['ConductScore'].'</td>'."\n";
                        $x .= '<td>'.$thisStudentConductInfo['ConductGrade'].'</td>'."\n";

                        $ID = 'modifyGradeArrIdx'."_".$i."_".$j;
                        $Name = 'modifyGradeArr['.$StudentArr[$i]['UserID'].']';
                        $Value = $thisStudentConductInfo['ModifiedGrade'];
                        $inputPar['maxlength'] = 3;

                        $x .= '<td>'."\n";
                            $x .= $linterface->GET_TEXTBOX($ID, $Name, $Value, '', $inputPar);
                        $x .= '</td>'."\n";
                    $x .= '</tr>'."\n";
                }
            }
            $x .= '</tbody>'."\n";
            $x .= '</table>'."\n";

            $x .= '<div class="edit_bottom_v30">'."\n";
                $x .= $SubmitBtn."\n";
                $x .= $CancelBtn."\n";
            $x .= '</div>'."\n";

            $x .= '<input type="hidden" name="ReportID" value="'.$ReportID.'">'."\n";
            $x .= '<input type="hidden" name="TermID" value="'.$TermID.'">'."\n";
            $x .= '<input type="hidden" name="ClassID" value="'.$ClassID.'">'."\n";
        $x .= '</form>'."\n";

        return $x;
    }
}
?>