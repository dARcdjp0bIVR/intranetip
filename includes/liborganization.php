<?php
if (!defined("LIBORGANIZATION_DEFINED"))                     // Preprocessor directive
{
define("LIBORGANIZATION_DEFINED", true);


class liborganization extends libdb {

        function liborganization ()
        {
                 $this->libdb();
        }

        function returnHideList ()
        {
                 $sql = "SELECT GroupID FROM INTRANET_ORPAGE_GROUP WHERE RecordStatus = 1";
                 return $this->returnVector($sql);
        }
        function isGroupHidden($GroupID)
        {
                 $groups = $this->returnHideList();
                 return in_array($GroupID,$groups);
        }
        function setGroupHidden($GroupID)
        {
                 $sql = "DELETE FROM INTRANET_ORPAGE_GROUP WHERE GroupID = $GroupID";
                 $this->db_db_query($sql);
                 $sql = "INSERT INTO INTRANET_ORPAGE_GROUP (GroupID,RecordStatus,DateInput,DateModified)
                         VALUES ($GroupID,'1',now(),now())";
                 $this->db_db_query($sql);
        }
        function setGroupDisplay($GroupID)
        {
                 $sql = "DELETE FROM INTRANET_ORPAGE_GROUP WHERE GroupID = $GroupID";
                 $this->db_db_query($sql);
        }
        function setCategoryHidden($catID)
        {
                 $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = $catID";
                 $groups = $this->returnVector($sql);
                 if (sizeof($groups)==0) return;

                 $list = implode(",",$groups);
                 $sql = "DELETE FROM INTRANET_ORPAGE_GROUP WHERE GroupID IN ($list)";
                 $this->db_db_query($sql);

                 $delimiter = "";
                 $values = "";
                 for ($i=0; $i<sizeof($groups); $i++)
                 {
                      $values .= "$delimiter (".$groups[$i].",'1',now(),now())";
                      $delimiter = ",";
                 }
                 $sql = "INSERT INTO INTRANET_ORPAGE_GROUP (GroupID,RecordStatus,DateInput,DateModified)
                         VALUES $values";
                 $this->db_db_query($sql);
        }
        function setCategoryDisplay($catID)
        {
                 $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = $catID";
                 $groups = $this->returnVector($sql);
                 if (sizeof($groups)==0) return;

                 $list = implode(",",$groups);
                 $sql = "DELETE FROM INTRANET_ORPAGE_GROUP WHERE GroupID IN ($list)";
                 $this->db_db_query($sql);
        }

}


}        // End of directive
?>