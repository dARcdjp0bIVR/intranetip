<?
# ivan
class libreportcardSIS extends libreportcardcustom
{
	function getReportHeader($ReportID)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		$TitleTable = "";
		
		if($ReportID)
		{
			# Retieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$HeaderHeight = $ReportSetting['HeaderHeight'];
			$ReportTitle =  str_replace(":_:", "<br>", $ReportSetting['ReportTitle']);
			
			# get school name
// 			$SchoolName = GET_SCHOOL_NAME();	
		
			$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
			$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
			if(!empty($ReportTitle) || !empty($SchoolName))
			{
				$TitleTable .= "<td>";
				if ($HeaderHeight == -1) {
					$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					$TitleTable .= "<tr><td nowrap='nowrap' align='center'>\n";
					if(!empty($SchoolName))
						$TitleTable .= "<h1>".$SchoolName."</h1>\n";
					if(!empty($ReportTitle))
						$TitleTable .= "<h2><br>".$ReportTitle."</h2>\n";
					$TitleTable .= "</td></tr>\n";
					$TitleTable .= "</table>\n";
				} else {
					for ($i = 0; $i < $HeaderHeight; $i++) {
						$TitleTable .= "<br/>";
					}
				}
				$TitleTable .= "</td>";
			}
			
			$emptyImagePath = $PATH_WRT_ROOT."images/2007a/10x10.gif";
			$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
			//$TitleTable .= "<tr><td colspan='3'><img src='".$emptyImagePath."' height='8'></td></tr>";
			$TitleTable .= "</table>";
		}
		
		return $TitleTable;
	}
	
	function getReportHeaderByCSV($ParInfoArr)
	{
		global $eReportCard, $PATH_WRT_ROOT;
		$TitleTable = "";
		$ReportTitle = "";
		$HeaderHeight = -1;
				
		$ReportTitle = $ParInfoArr['ReportTitle1']."<br />".$ParInfoArr['ReportTitle2'];
		
		# get school name
// 		$SchoolName = GET_SCHOOL_NAME();	
	
		$TitleTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
		$TitleTable .= "<tr><td width='120' align='center'>".$TempLogo."</td>";
		if(!empty($ReportTitle) || !empty($SchoolName))
		{
			$TitleTable .= "<td>";
			if ($HeaderHeight == -1) {
				$TitleTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
				$TitleTable .= "<tr><td nowrap='nowrap' align='center'>\n";
				if(!empty($SchoolName))
					$TitleTable .= "<h1>".$SchoolName."</h1><br>\n";
				if(!empty($ReportTitle))
					$TitleTable .= "<h2>".$ReportTitle."</h2>\n";
				$TitleTable .= "</td></tr>\n";
				$TitleTable .= "</table>\n";
			} else {
				for ($i = 0; $i < $HeaderHeight; $i++) {
					$TitleTable .= "<br/>";
				}
			}
			$TitleTable .= "</td>";
		}
		$TitleTable .= "<td width='120' align='center'>&nbsp;</td></tr>";
		
		$emptyImagePath = $PATH_WRT_ROOT."images/2007a/10x10.gif";
		$TitleTable .= "<tr><td colspan='3'><img src='".$emptyImagePath."' height='8'></td></tr>";
		$TitleTable .= "</table>";
		
		return $TitleTable;
	}
	
	function getReportStudentInfo($ReportID, $StudentID='')
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		$StudentInfoTable = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
			$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$SettingStudentInfo = unserialize($ReportSetting['DisplaySettings']);
			
			# retrieve required variables
			$defaultVal		= "XXX";
			$data['AcademicYear'] = getCurrentAcademicYear();
			if($StudentID)		# retrieve Student Info
			{
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libclass.php");
				$lu = new libuser($StudentID);
				$lclass = new libclass();
				
				$data['Name'] 		= $lu->UserName2Lang();
				$data['ClassNo'] 	= $lu->ClassNumber; 
				$data['ClassIndexNo'] 	= $lu->ClassNumber; 
				$data['Class'] 		= $lu->ClassName;
				$data['StudentNo'] 	= $lu->ClassNumber;
				$data['DateOfBirth'] = $lu->DateOfBirth;
				$data['Gender'] 	= $lu->Gender;
				$data['StudentAdmNo'] = $lu->AdmissionNo;
								
				$ClassTeacherAry = $lclass->returnClassTeacher($lu->ClassName,$includeSecond=true);
				foreach($ClassTeacherAry as $key=>$val)
				{
					$CTeacher[] = $val['CTeacher'];
				}
				//$data['ClassTeacher'] = !empty($CTeacher) ? implode(", ", $CTeacher) : "--";
				$data['ClassTeacher'] = !empty($CTeacher) ? implode("<br />", $CTeacher) : "--";

			}
			
			if(!empty($SettingStudentInfo))
			{
				$count = 0;
				$StudentInfoTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
				
				# set css (colgroup)
				for($i=0;$i<$StudentInfoTableCol;$i++)
				{
					$StudentInfoTable .= "<colgroup span='1' id='info_label'></colgroup>";
					$StudentInfoTable .= "<colgroup span='1' id='colon_col'></colgroup>";
					$StudentInfoTable .= "<colgroup span='1' id='info_value_wide'></colgroup>";
				}
				
				for($i=0; $i<sizeof($StudentTitleArray); $i++)
				{
					$SettingID = trim($StudentTitleArray[$i]);
					if(in_array($SettingID, $SettingStudentInfo)===true)
					{
						$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
						
						if($count%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "<tr>";
						}
						
						$StudentInfoTable .= "<td class='result_col_label' nowrap>".$Title."</td>";
						$StudentInfoTable .= "<td>:&nbsp;</td>";
						
						$width = ($count%$StudentInfoTableCol==0) ? "130" : "50";
						
						if($Title=="Name" && ($count%$StudentInfoTableCol==0))
						{
							$StudentInfoTable .= "<td colspan='4' class='result_mark' width='100%'>". ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							$count++;
						}
						else
							$StudentInfoTable .= "<td class='result_mark' width='$width'>". ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
							
						if(($count+1)%$StudentInfoTableCol==0) {
							$StudentInfoTable .= "</tr>";
						}
						$count++;
					}
				}
				$StudentInfoTable .= "<tr class='bottom_dot_border'><td colspan='". ($StudentInfoTableCol*3)."' style='font-size:2px'>&nbsp;</td></tr>";
				$StudentInfoTable .= "</table>";
			}
		}
		
		return $StudentInfoTable;
	}
	
	function getReportStudentInfoByCSV($ParInfoArr)
	{
		global $PATH_WRT_ROOT, $eReportCard, $eRCTemplateSetting;
		$StudentInfoTable = "";
		
		# Retrieve Display Settings
		$StudentInfoTableCol = $eRCTemplateSetting['StudentInfo']['Col'];
		$StudentTitleArray = $eRCTemplateSetting['StudentInfo']['Selection'];
			
		$SettingStudentInfo = array("Name", "Class", "ClassIndexNo", "ClassTeacher", "StudentAdmNo");
		
		$data['Name'] 			= $ParInfoArr['Name'];
		$data['ClassNo'] 		= $ParInfoArr['ClassNo']; 
		$data['ClassIndexNo'] 	= $ParInfoArr['ClassIndexNo']; 
		$data['Class'] 			= $ParInfoArr['Class'];
		$data['StudentNo'] 		= $ParInfoArr['StudentNo'];
		$data['DateOfBirth'] 	= $ParInfoArr['DateOfBirth'];
		$data['Gender'] 		= $ParInfoArr['Gender'];
		$data['StudentAdmNo'] 	= $ParInfoArr['StudentAdmNo'];
		$data['ClassTeacher'] 	= $ParInfoArr['ClassTeacher'];
		$data['AcademicYear'] 	= getCurrentAcademicYear();
		
		if(!empty($SettingStudentInfo))
		{
			$count = 0;
			$StudentInfoTable = "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'>";
			
			# set css (colgroup)
			for($i=0;$i<$StudentInfoTableCol;$i++)
			{
				$StudentInfoTable .= "<colgroup span='1' id='info_label'></colgroup>";
				$StudentInfoTable .= "<colgroup span='1' id='colon_col'></colgroup>";
				$StudentInfoTable .= "<colgroup span='1' id='info_value_wide'></colgroup>";
			}
			
			for($i=0; $i<sizeof($StudentTitleArray); $i++)
			{
				$SettingID = trim($StudentTitleArray[$i]);
				if(in_array($SettingID, $SettingStudentInfo)===true)
				{
					$Title = $eReportCard['Template']['StudentInfo'][$SettingID];
					
					if($count%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "<tr>";
					}
					
					$StudentInfoTable .= "<td class='result_col_label' nowrap>".$Title."</td>";
					$StudentInfoTable .= "<td>:&nbsp;</td>";
					
					$width = ($count%$StudentInfoTableCol==0) ? "130" : "50";
					
					if($Title=="Name" && ($count%$StudentInfoTableCol==0))
					{
						$StudentInfoTable .= "<td colspan='4' class='result_mark' width='100%'>". ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
						$count++;
					}
					else
						$StudentInfoTable .= "<td class='result_mark' width='$width'>". ($data[$SettingID] ? $data[$SettingID] : $defaultVal ) ."</td>";
						
					if(($count+1)%$StudentInfoTableCol==0) {
						$StudentInfoTable .= "</tr>";
					}
					$count++;
				}
			}
			$StudentInfoTable .= "<tr class='bottom_dot_border'><td colspan='". ($StudentInfoTableCol*3)."' style='font-size:2px'>&nbsp;</td></tr>";
			$StudentInfoTable .= "</table>";
		}
		
		return $StudentInfoTable;
	}
	
	function getMSTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		//$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		//$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeader($ReportID);
		list($ColHeader, $ColNum) = $ColHeaderAry;
				
		$MarksAry = $this->getMarks($ReportID, $StudentID);
		# retrieve SubjectID Array
		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
		foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
		foreach($SubjectArray as $SubjectIDArray[] => $SubjectNameArray[]);
		
		$SubjectCol	= $this->returnTemplateSubjectCol($ReportID, $ClassLevelID);
		$sizeofSubjectCol = sizeof($SubjectCol);
		
		/*
		# Show Subject Full Mark?
		if($ShowSubjectFullMark)
		{
			$FullMarkCol = $this->returnSubjectFullMark($ClassLevelID);
		}
		# Show Subject Overall?
		if($ShowSubjectOverall)
		{
			$SubjectOverall = $this->returnSubjectOverall($ReportID, $StudentID);
		}
		
		# Allow Subject Teacher Comment?
		if($AllowSubjectTeacherComment)
		{
			$SubjectTeacherCommentCol = $this->returnSubjectTeacherComment($ReportID, $StudentID);
		}
		*/
		
		/*
		# Display MS Table Footer?
		if($eRCTemplateSetting['MSTableFooter']['Display'])
		{
			$MSTableFooter = $this->genMSTableFooter($ReportID, $MarksAry);
			
		}
		*/
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$isFirst = 1;
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$isSub = 0;
			$thisSubjectID = $SubjectIDArray[$i];
			
			if($StudentID)
			{
				# check weight=0 (continue)
				$this_subject_weight = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID=".$thisSubjectID ." and ReportColumnID <>''");
				$sw = 0;
				foreach($this_subject_weight as $key => $w)
					$sw += $w['Weight']+0;
				if(!$sw)	continue;
				//if(empty($MarksAry[$thisSubjectID]))	continue;
			}
			if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
			if($isSub)	continue;
			$css_border_top = ($isFirst or $isSub)? "" : "border_top";
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectID]==false)
			{
				$DetailsTable .= "<tr>";
			
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				$DetailsTable .= "<td class='colon_col'>:</td>";
				
	/*			
				# Full Mark
				if($ShowSubjectFullMark)	$DetailsTable .= "<td class='tabletext border_left {$css_border_top}' align='center'>". $FullMarkCol[$thisSubjectID] ."</td>";
	*/			
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectID];
				
				/*
				# Subject Overall 
				if($ShowSubjectOverall)
				{
					$SOverall = $StudentID ? $SubjectOverall[$thisSubjectID] : "xxx";
					$DetailsTable .= "<td class='result_mark' align='center'>". $SOverall ."</td>";
					$DetailsTable .= "<td style='width: 1px;'>&nbsp;</td>";
				}
			
				# Subject Teacher Comment
				if($AllowSubjectTeacherComment)
				{
					if($StudentID)
						$SubjectTeacherComment = $SubjectTeacherCommentCol[$thisSubjectID] ? $SubjectTeacherCommentCol[$thisSubjectID] : "&nbsp;";
					else
						$SubjectTeacherComment = "XXX";
					$DetailsTable .= "<td class='tabletext border_left {$css_border_top}' align='center'>". $SubjectTeacherComment ."</td>";
				}
				*/
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		/*
		# MS Table Footer
		if($eRCTemplateSetting['MSTableFooter']['Display'])
		{
			$DetailsTable .= $MSTableFooter;
		}
		*/
		//$this->getOtherInfoConfig($UploadType);
		
		$DetailsTable .= "<tr class='bottom_dot_border'><td colspan='{$ColNum}' style='font-size:2px'>&nbsp;</td></tr>";
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
	
	function getMSTableByCSV($ParInfoArr)
	{
		global $eRCTemplateSetting, $eReportCard;
		
		# define 	
		$ColHeaderAry = $this->genMSTableColHeaderByCSV($ParInfoArr);
		list($ColHeader, $ColNum) = $ColHeaderAry;
		
		$SubjectCol	= $this->returnTemplateSubjectColByCSV($ParInfoArr);
		$sizeofSubjectCol = sizeof($SubjectCol);		
		
		# retrieve Marks Array
		$MSTableReturned = $this->genMSTableMarksByCSV($ParInfoArr);
		$MarksDisplayAry = $MSTableReturned['HTML'];
		$isAllNAAry = $MSTableReturned['isAllNA'];
		
		##########################################
		# Start Generate Table
		##########################################
		$DetailsTable = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
		
		# ColHeader
		$DetailsTable .= $ColHeader;
		
		$subjectArr = $ParInfoArr["Subject"];
		for($i=0;$i<$sizeofSubjectCol;$i++)
		{
			$thisSubjectName = $subjectArr[$i];
			
			# check if displaying subject row with all marks equal "N.A."
			if ($eRCTemplateSetting['DisplayNA'] || $isAllNAAry[$thisSubjectName]==false)
			{
				$DetailsTable .= "<tr>";
			
				# Subject 
				$DetailsTable .= $SubjectCol[$i];
				$DetailsTable .= "<td class='colon_col'>:</td>";
				
				# Marks
				$DetailsTable .= $MarksDisplayAry[$thisSubjectName];
				
				$DetailsTable .= "</tr>";
				$isFirst = 0;
			}
		}
		
		$DetailsTable .= "<tr class='bottom_dot_border'><td colspan='{$ColNum}' style='font-size:2px'>&nbsp;</td></tr>";
		$DetailsTable .= "</table>";
		##########################################
		# End Generate Table
		##########################################				
		
		return $DetailsTable;
	}
			
	function genMSTableColHeader($ReportID)
	{
		global $eReportCard, $eRCTemplateSetting;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		$AllowSubjectTeacherComment = $ReportSetting['AllowSubjectTeacherComment'];
		$SemID = $ReportSetting['Semester'];
		$ReportType = $SemID == "F" ? "W" : "T";
// 		$ShowSubjectFullMark = $ReportSetting['ShowSubjectFullMark'];
// 		$ShowSubjectOverall = $ReportSetting['ShowSubjectOverall'];
		$PercentageOnColumnWeight = $ReportSetting['PercentageOnColumnWeight'];
		$n = 0;

		#########################################################
		############## Marks START
		$subtitleRow2 = "";
		if($ReportType=="T")	# Temrs Report Type
		{
			switch(substr($ClassLevel, 0, 1))
			{
				case "P":
					$SubTitle = $eReportCard['Template']['1SemestralAssessment'];
					# Retrieve Invloved Assesment
					$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					$AchievementStr = substr($ClassLevel, 1, 1)<5 ? $eReportCard['Template']['AchievementBand'] : $eReportCard['Template']['AchievementGrade'];
					
					foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
					for($i=0;$i<sizeof($ColumnTitle);$i++)
					{
						$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $ColumnTitle[$i] . "</td>";
						$n++;
						$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
						$n++;
					}
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $AchievementStr . "</td>";
					$n++;
					
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
					$n++;
					
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['Template']['Grade'] . "</td>";
					$n++;
					break;
			}
		}
		else					# Whole Year Report Type
		{
			switch(substr($ClassLevel, 0, 1))
			{
				case "P":
					$SubTitle = $eReportCard['Template']['Overall'];
					$ColumnData = $this->returnReportTemplateColumnData($ReportID);
					$AchievementStr = substr($ClassLevel, 1, 1)<5 ? $eReportCard['Template']['AchievementBand'] : $eReportCard['Template']['AchievementGrade'];
					
					# P1 & P2 only have 1 semester
					if(substr($ClassLevel, 1, 1) == 1 || substr($ClassLevel, 1, 1)==2)
					{
						$subtitleRow2 .= "<td valign='bottom' class='result_col_label' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $eReportCard['Template']['2Sem'] . "</td>";
						$subtitleRow2 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
							 
						$this_weight = $this->returnReportTemplateColumnData($ReportID, "ReportColumnID=".$ColumnData[0]['ReportColumnID']);
						$this_weight = ($this_weight[0]['DefaultWeight']*100)."%";
						$row1 .= "<td valign='bottom' class='result_col_label dot_bottom_border' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $eReportCard['Template']['Assessment'] . "<br>(". $this_weight .")</td>";
						$n++;
						$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
						$n++;
					
						$Rowspan = "";
					}
					else
					{
						$sa = 0;
						for($i=0;$i<sizeof($ColumnData);$i++)
						{
							$sa++;
							$subtitleRow2 .= "<td valign='bottom' class='result_col_label' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $eReportCard['Template'][$sa.'Sem'] . "</td>";
							$subtitleRow2 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
								 
							$this_weight = $this->returnReportTemplateColumnData($ReportID, "ReportColumnID=".$ColumnData[$i]['ReportColumnID']);
							//$this_weight = $PercentageOnColumnWeight ? ($this_weight[0]['DefaultWeight']*100)."%" : $this_weight[0]['DefaultWeight'];
							$this_weight = ($this_weight[0]['DefaultWeight']*100)."%";
							$row1 .= "<td valign='bottom' class='result_col_label dot_bottom_border' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $eReportCard['Template']['Assessment'] . "<br>(". $this_weight .")</td>";
							$n++;
							$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
							$n++;
						
							$Rowspan = "";
				
						}
					}
					
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['Template']['Mark'] . "<br>(100%)</td>";
					$n++;
					
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
					$n++;
					
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $AchievementStr . "</td>";
					$n++;
					
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
					$n++;
					
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['Template']['Grade'] . "</td>";
					$n++;
					break;
					
				case "S":
					# Retrieve Invloved Assesment
					$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
					if(sizeof($ColoumnTitle)==2)	
						$SubTitle = $eReportCard['Template']['1SemestralAssessment'];	# 1st Semestral Assessment
					else
						$SubTitle = $eReportCard['Template']['2SemestralAssessment'];	# 2nd Semestral Assessment
						
					$ColumnData = $this->returnReportTemplateColumnData($ReportID);
					$this_weight2 = 0;
					for($i=0;$i<sizeof($ColumnData);$i++)
					{
						if(sizeof($ColoumnTitle)==4 && $i<2)	continue;
						
						//$SemName = $this->returnSemesters($ColumnData[$i]['SemesterNum']);
						if($i==0)	$SemName = "CA1";
						if($i==1)	$SemName = "SA1";
						if($i==2)	$SemName = "CA2";
						if($i==3)	$SemName = "SA2";
						
						$this_weight = $this->returnReportTemplateColumnData($ReportID, "ReportColumnID=".$ColumnData[$i]['ReportColumnID']);
						//$this_weight = $PercentageOnColumnWeight ? ($this_weight[0]['DefaultWeight']*100)."%" : $this_weight[0]['DefaultWeight'];
						$this_weight2 += $this_weight[0]['DefaultWeight']*100;
						$this_weight = ($this_weight[0]['DefaultWeight']*100)."%";

						$row1 .= "<td valign='bottom' class='result_col_label dot_bottom_border' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $SemName ."<br>(". $this_weight .")</td>";
						$n++;
						$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
						$n++;
					
						$Rowspan = "";
					}
					
					if(sizeof($ColoumnTitle)==2)
						$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['Template']['FirstCombined'] . "</td>";
					else
						$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['Template']['SecondCombined'] . " (". $this_weight2."%)</td>";
					$n++;
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
					$n++;
					
					if(sizeof($ColoumnTitle)==4)
					{
						$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['Template']['OverallCombined'] . " (100%)</td>";
						$n++;
						$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
						$n++;
					}
					
					$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['Template']['Grade'] . "</td>";
					$n++;
					
					break;
			}
		}
		############## Marks END
		#########################################################
		
		$Rowspan = "";
		$x .= "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = $eReportCard['Template']['SubjectEng'];
			$SubjectChn = $eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan} width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."' class='result_col_label dot_bottom_border' valign='bottom' >". $SubjectTitle . "</td>";
			$n++;
		}
		
		$x .= "<td>&nbsp;</td>";
		
		/*
		# Show Subject Full Mark
		if($ShowSubjectFullMark)
		{
			$x .= "<td {$Rowspan} class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['SchemesFullMark'] . "</td>";
			$n++;
		}
		*/
		
		# Marks
		$x .= $row1;
		
		/*
		# Subject Overall 
		if($ShowSubjectOverall)
		{
			$x .= "<td {$Rowspan} class='result_col_label dot_bottom_border' align='center' valign='bottom' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $eReportCard['Template']['FirstCombined'] ."</td>";
			$n++;
			$x .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
			$n++;
			
			if($eRCTemplateSetting['OverallGrade']['Display'])
			{
				$x .= "<td {$Rowspan} class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['Template']['Grade'] ."</td>";
				$n++;		
			}
		}
		
		
		# Subject Teacher Comment
		if($AllowSubjectTeacherComment)
		{
			$x .= "<td {$Rowspan} class='result_col_label dot_bottom_border' align='center' valign='bottom'>". $eReportCard['Template']['SubjectTeacherComment'] ."</td>";
			$n++;
		}
		*/
		
		$x .= "</tr>";
		//if($row2)	$x .= "<tr>". $row2 ."</tr>";
		$n++;		//Colon after subject
		if(substr($ClassLevel, 0, 1)=="P")
			$subtitleRow = "<tr><td colspan='". ($n-5-($subtitleRow2?4:0) + ((substr($ClassLevel, 1, 1)==1 || substr($ClassLevel, 1, 1)==2) ? 2:0) ) ."'></td>". $subtitleRow2 ."<td class='result_col_label' colspan='5' align='center'>". $SubTitle."</td></tr>";
		else
		{
			# 24 Jun 2009 move the <2nd Semestral Assessment> above the column of "CA2, SA2, and Second Combined"
			$subtitleRow = "<tr>";
				//$subtitleRow .= "<td colspan='". ($n-5-($subtitleRow2?4:0)) ."'></td>";
				$subtitleRow .= "<td colspan='". ($n-5-($subtitleRow2?4:4)) ."'></td>";
				$subtitleRow .= $subtitleRow2;
				$subtitleRow .= "<td class='result_col_label' colspan='5' align='center' nowrap>". $SubTitle."</td>";
			$subtitleRow .= "</tr>";
		}
		return array($subtitleRow.$x, $n);
	}
	
	function genMSTableColHeaderByCSV($ParInfoArr)
	{
		global $eReportCard, $eRCTemplateSetting;
		$n = 0;
		$row1 = "";
		$SubTitle = "";
		$subtitleRow2 = "";
		$x = "";

		#########################################################
		############## Marks START
		
		$SubTitle = "<".$ParInfoArr['AssessmentName'].">";
		$columnTitleArr = $ParInfoArr['ColumnTitle'];
		$columnSize = count($columnTitleArr);
		for ($i=0; $i<$columnSize; $i++)
		{
			$thisTitle = trim($columnTitleArr[$i]);
			$thisTitle = str_replace(" (", "<br />(", $thisTitle);
			$row1 .= "<td valign='bottom' class='result_col_label dot_bottom_border' align='center' width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."'>". $thisTitle ."</td>";
			$n++;
			
			if ($i != $columnSize-1)
			{
				$row1 .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
				$n++;
			}
		}
		
		$Rowspan = "";
		$x .= "<tr>";
		# Subject 
		$SubjectColAry = $eRCTemplateSetting['ColumnHeader']['Subject'];
		for($i=0;$i<sizeof($SubjectColAry);$i++)
		{
			$SubjectEng = $eReportCard['Template']['SubjectEng'];
			$SubjectChn = $eReportCard['Template']['SubjectChn'];
			$SubjectTitle = $SubjectColAry[$i];
			$SubjectTitle = str_replace("SubjectEng", $SubjectEng, $SubjectTitle);
			$SubjectTitle = str_replace("SubjectChn", $SubjectChn, $SubjectTitle);
			$x .= "<td {$Rowspan} width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."' class='result_col_label dot_bottom_border' valign='bottom' >". $SubjectTitle . "</td>";
			$n++;
		}		
		$x .= "<td>&nbsp;</td>";
		
		# Marks
		$x .= $row1;
		
		$x .= "</tr>";
		//if($row2)	$x .= "<tr>". $row2 ."</tr>";
		$n++;		//Colon after subject
		
		$subtitleRow = "<tr><td colspan='". ($n-5) . "'>&nbsp;</td><td class='result_col_label' colspan='5' align='center'>". $SubTitle."</td></tr>";
		
		return array($subtitleRow.$x, $n);
	}
	
	function genMSTableMarks($ReportID, $MarksAry=array(), $StudentID='')
	{
		global $eReportCard, $PATH_WRT_ROOT;
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
 		$ReportType = $SemID == "F" ? "W" : "T";		
 		$ClassLevelID = $ReportSetting['ClassLevelID'];
 		$ClassLevel = $this->returnClassLevel($ClassLevelID);
 		
 		$activeYear = $this->GET_ACTIVE_YEAR();
		
		$SubjectArray = $this->returnSubjectwOrderNoL($ClassLevelID);
 		$MainSubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		
		$x = array();
		switch(substr($ClassLevel, 0, 1))
		{
			case "P": 
				if($ReportType=="T")	# Temrs Report Type
				{	
					$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
					$ColumnID = array();
					$ColumnTitle = array();
					foreach ($ColoumnTitle as $ColumnID[] => $ColumnTitle[]);
					
					foreach($SubjectArray as $SubjectID => $SubjectName)
					{
						# Retrieve Subject Scheme ID
						$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
						$SchemeID = $SubjectFormGradingSettings[SchemeID];
						$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
						
						$isAllNA = true;
						
						for($i=0;$i<sizeof($ColumnID);$i++)
						{
							$thisColumnID = $ColumnID[$i];
							$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
							$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
							$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
							if ($columnSubjectWeightTemp == 0)
							{
								$showEmpty = true;
							}
							else
							{
								$showEmpty = false;
							}
							
// 							$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID[$i]][Mark] : "";
// 							$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnID[$i]][Grade] : ""; 
							$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
							$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0][Grade] : ""; 
														
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark = $ScaleDisplay=="M" ? "100.0" : "";
								$thisGrade = $ScaleDisplay=="G" ? "A" : "";
							}
							
							# Mark
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? number_format($thisMark, 1) : "";
							
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][0]['Grade']);
							if($needStyle)
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							}
							else
								$thisMarkDisplay = $thisMark;
								
							if ($thisMarkDisplay != "N.A.")
							{
								$isAllNA = false;
							}
							
							$thisMarkDisplay = $ScaleDisplay=="M" ? $thisMarkDisplay : "";
							if ($showEmpty) 
								$thisMarkDisplay = "&nbsp;";
							
// 							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
// 							$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='bottom'>". $thisMarkDisplay ."</td>";
							$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
							
							# Achieve't Band
							$thisBand = $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMark);
							$thisBandDisplay = $this->ReturnTextwithStyle($thisBand, 'HighLight', $thisNature);
							$thisBandDisplay = $ScaleDisplay=="M" ? $thisBandDisplay : "";
							$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='bottom'>". $thisBandDisplay ."</td>";
							$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
							
							# Grade
							# check special case
							list($thisGrade, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisGrade, $MarksAry[$SubjectID][0]['Grade']);
							if($needStyle)
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisGrade);
								$thisGradeDisplay = $this->ReturnTextwithStyle($thisGrade, 'HighLight', $thisNature);
							}
							else
								$thisGradeDisplay = $thisGrade;
									
							if($ScaleDisplay=="G")
							{
// 								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisGrade);
// 								$thisGradeDisplay = $this->ReturnTextwithStyle($thisGrade, 'HighLight', $thisNature);
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='bottom'>". $thisGradeDisplay ."</td>";
							}							
						}
						# construct an array to return
						$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
						$returnArr['isAllNA'][$SubjectID] = $isAllNA;
					}
				}
				else				# Whole Year Report Type
				{
					# Retrieve Invloved Temrs
					$ColumnData = $this->returnReportTemplateColumnData($ReportID);
					foreach($SubjectArray as $SubjectID => $SubjectName)
					{
						if(in_array($SubjectID, $MainSubjectIDArray)===true)
						{
							# Retrieve Subject Scheme ID
							$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
							$SchemeID = $SubjectFormGradingSettings[SchemeID];
							$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
							$isAllNA = true;
							
							for($i=0;$i<sizeof($ColumnData);$i++)
							{
								$ColumnID = $ColumnData[$i]['ReportColumnID'];
								
								$columnWeightConds = " ReportColumnID = '$ColumnID' AND SubjectID = '$SubjectID' " ;
								$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
								$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
								if ($columnSubjectWeightTemp == 0)
								{
									$showEmpty = true;
								}
								else
								{
									$showEmpty = false;
								}
								
								$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID][Mark] : "";
								$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnID][Grade] : ""; 
																
								# for preview purpose
								if(!$StudentID)
								{
									$thisMark = $ScaleDisplay=="M" ? "100.0" : "";
									$thisGrade = $ScaleDisplay=="G" ? "A" : "";
								}
								
								# check the mark is distinction/pass/fail
								$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? number_format($thisMark, 1) : "";
								
								if($ScaleDisplay=="M")
								{
									# check the subjec weight is zero or not
									$t = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=$ColumnID and SubjectID=$SubjectID ");
									# check new student
									$thisMark = strlen($thisMark) ? $thisMark : "--";
									if(!$t[0]['Weight']) $thisMark = "";
								}		
								
								# check special case
								list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID]['Grade']);
								
								if ($thisMark != "--" && $thisMark != "N.A.")
								{
									$isAllNA = false;
								}
								
								if($needStyle)
								{
									$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
									$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
								}
								else
									$thisMarkDisplay = $thisMark;								
									
								if ($showEmpty) $thisMarkDisplay = "&nbsp;";
// 								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
// 								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
								
								# Mark
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='bottom'>". $thisMarkDisplay ."</td>";
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
							}
							
							# Overall
							$thisOverall = $ScaleDisplay=="M" ? number_format($MarksAry[$SubjectID][0][Mark],0) : "";
							if(!$StudentID)
							{
								$thisOverall = $ScaleDisplay=="M" ? "100" : "";
							}
							//$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisOverall);
							//$thisOverallDisplay = $this->ReturnTextwithStyle($thisOverall, 'HighLight', $thisNature);
							# check special case
							list($thisOverall, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisOverall, $MarksAry[$SubjectID][0]['Grade']);
							if($needStyle)
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisOverall);
								$thisOverallDisplay = $this->ReturnTextwithStyle($thisOverall, 'HighLight', $thisNature);
							}
							else
								$thisOverallDisplay = $thisOverall;	
							
								
							# special handling for  ( do not show subject mark for not examed subject)
							$excludeSubjectIDArr = $eRCCalculationSetting['ExcludeSubjectInGrandMark'][$activeYear]['SubjectID'];
							$excludeClassLevelNameArr = $eRCCalculationSetting['ExcludeSubjectInGrandMark'][$activeYear]['ClassLevelName'];
							$haveExcludeSubject = (is_array($excludeSubjectIDArr))? 1 : 0;
							
							if ($haveExcludeSubject)
							{
								if (in_array($ClassLevel, $excludeClassLevelNameArr) && in_array($SubjectID, $excludeSubjectIDArr))
								{
									$thisOverallDisplay = "--";
								}
							}
							
							$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='bottom'>". $thisOverallDisplay ."</td>";
							$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
							
							# Achieve't Band
							$thisBand = $ScaleDisplay=="M" ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $MarksAry[$SubjectID][0][Mark]) : "";
							if(!$StudentID)
							{
								$thisBand = $ScaleDisplay=="M" ? "1" : "";
							}
							//$thisBandDisplay = $this->ReturnTextwithStyle($thisBand, 'HighLight', $thisNature);
							$thisBand = ($ScaleDisplay=="M" && !strlen($thisBand)) ? "--" : $thisBand;
							$thisBandDisplay = $thisBand ? $this->ReturnTextwithStyle($thisBand, 'HighLight', $thisNature) : $thisBand;
							$tempMark = $MarksAry[$SubjectID][0]['Mark'];
							list($thisBandTemp, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $tempMark, $MarksAry[$SubjectID][0]['Grade']);
							$thisBandDisplay = ($tempMark != $thisBandTemp)	? "" : $thisBandDisplay;
							
							# special handling for  ( do not show subject mark for not examed subject)
							if ($haveExcludeSubject)
							{
								if (in_array($ClassLevel, $excludeClassLevelNameArr) && in_array($SubjectID, $excludeSubjectIDArr))
								{
									$thisBandDisplay = "--";
								}
							}
							
							$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='bottom'>". $thisBandDisplay ."</td>";
							$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
							
							# Grade
							if($ScaleDisplay=="G") 
							{
								$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][0][Grade] : "";
								if(!$StudentID)
								{
									$thisGrade = $ScaleDisplay=="G" ? "A" : "";
								}
	// 							# check special case
								list($thisGrade, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisGrade, $MarksAry[$SubjectID][0]['Grade']);
								if($needStyle)
								{
									$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisGrade);
									$thisGradeDisplay = $this->ReturnTextwithStyle($thisGrade, 'HighLight', $thisNature);
								}
								else
									$thisGradeDisplay = $thisGrade;	
									
	// 							$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisGrade);
	// 							$thisGradeDisplay = $this->ReturnTextwithStyle($thisGrade, 'HighLight', $thisNature);
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='bottom'>". $thisGradeDisplay ."</td>";
							}
						}
						# construct an array to return
						$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
						$returnArr['isAllNA'][$SubjectID] = $isAllNA;
					}
				}
				break;
				
			case "S":
				# Whole Year Report Only
					# Retrieve Invloved Temrs
					$ColumnData = $this->returnReportTemplateColumnData($ReportID);
					if(sizeof($ColumnData)==4)	
						$SecondComOverallMarksAry = $this->returnSecondCombinedOverallMarks($ReportID, $StudentID);
						
					$GrandTotal = 0;
					foreach($SubjectArray as $SubjectID => $SubjectName)
					{
						if(in_array($SubjectID, $MainSubjectIDArray)===true)
						{
						
						# Retrieve Subject Scheme ID
						$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
						$SchemeID = $SubjectFormGradingSettings[SchemeID];
						$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
						$isAllNA = true;
						
						for($i=0;$i<sizeof($ColumnData);$i++)
						{
							if(sizeof($ColumnData)==4 && $i<2)	continue;
							
							$thisColumnID = $ColumnData[$i]['ReportColumnID'];
							$columnWeightConds = " ReportColumnID = '$thisColumnID' AND SubjectID = '$SubjectID' " ;
							$columnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $columnWeightConds);
							$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
							if ($columnSubjectWeightTemp == 0)
							{
								$showEmpty = true;
							}
							else
							{
								$showEmpty = false;
							}
							
							$ColumnID = $ColumnData[$i]['ReportColumnID'];
							$thisMark = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][$ColumnID][Mark] : "";
							$thisGrade = $ScaleDisplay=="G" ? $MarksAry[$SubjectID][$ColumnID][Grade] : ""; 
							
							# for preview purpose
							if(!$StudentID)
							{
								$thisMark = $ScaleDisplay=="M" ? "100.0" : "";
								$thisGrade = $ScaleDisplay=="G" ? "A" : "";
							}
							
							# check the mark is distinction/pass/fail
							$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? number_format($thisMark, 1) : $thisGrade;
														
							# check the subjec weight is zero or not
							$t = $this->returnReportTemplateSubjectWeightData($ReportID, "ReportColumnID=$ColumnID and SubjectID=$SubjectID ");
							# check new student
							$thisMark = strlen($thisMark) ? $thisMark : "--";
							if(!$t[0]['Weight']) $thisMark = "";
							
							if ($thisMark != "--" && $thisMark != "N.A." && $thisMark!="0.0")
							{
								$isAllNA = false;
							}
							
							# check special case
							list($thisMark, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisMark, $MarksAry[$SubjectID][$ColumnID]['Grade']);
							if($needStyle)
							{
								$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisMark);
								$thisMarkDisplay = $this->ReturnTextwithStyle($thisMark, 'HighLight', $thisNature);
							}
							else
								$thisMarkDisplay = $thisMark;
							
							if ($showEmpty) $thisMarkDisplay = "&nbsp;";
							
							# For the 2nd consolidated report, move the grade to the rightest column
							if ($ScaleDisplay=="G" && ($thisMarkDisplay!="&nbsp;" && $thisMarkDisplay!=""))
							{
								if (sizeof($ColumnData)==2)		# First Semester
								{
									$x[$SubjectID] .= "<td>&nbsp;</td>";
									$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
									$x[$SubjectID] .= "<td>&nbsp;</td>";
									$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
									# Mark
									$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='top'>". $thisMarkDisplay ."</td>";								
								}
								else
								{
									$x[$SubjectID] .= "<td>&nbsp;</td>";
									$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
									$x[$SubjectID] .= "<td>&nbsp;</td>";
									$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
									$x[$SubjectID] .= "<td>&nbsp;</td>";
									$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
									# Mark
									$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='top'>". $thisMarkDisplay ."</td>";								
								}
								
								$skipOverall = true;
								break;
							}
							else
							{
								# Mark
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='top'>". $thisMarkDisplay ."</td>";
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
								
								$skipOverall = false;
							}
						}
						
						if (!$skipOverall)
						{
							if(sizeof($ColumnData)==2)	# First Semester
							{
								# First Combined
								$thisOverall = $ScaleDisplay=="M" ? $MarksAry[$SubjectID][0][Mark] : "";
								if(!$StudentID)
								{
									$thisOverall = $ScaleDisplay=="M" ? "100.0" : "";
								}
								$thisOverall = ($ScaleDisplay=="M" && !strlen($thisOverall)) ? "--" : $thisOverall;
								//$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisOverall);
								//$thisOverallDisplay = $thisOverall ? $this->ReturnTextwithStyle($thisOverall, 'HighLight', $thisNature) : $thisOverall;
								list($thisOverall, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisOverall, $MarksAry[$SubjectID][0]['Grade']);
								if($needStyle)
								{
									$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisOverall);
									$thisOverallDisplay = $thisOverall ? $this->ReturnTextwithStyle($thisOverall, 'HighLight', $thisNature) : $thisOverall;
								}
								else
									$thisOverallDisplay = $thisOverall;
								
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='top'>". $thisOverallDisplay ."</td>";
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
							}
							else						# Second Semester
							{
								# need retrive the second combined overall from 2nd Report 
								# Second Combined
								$thisOverall = $ScaleDisplay=="M" ? $SecondComOverallMarksAry[$SubjectID][0][Mark] : "";
								if(!$StudentID)
								{
									$thisOverall = $ScaleDisplay=="M" ? "100.0" : "";
								}
								$thisOverall = ($ScaleDisplay=="M" && !strlen($thisOverall)) ? "--" : $thisOverall;
								//$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisOverall);
								//$thisOverallDisplay = $thisOverall ? $this->ReturnTextwithStyle($thisOverall, 'HighLight', $thisNature) : $thisOverall;
								list($thisOverall, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisOverall, $SecondComOverallMarksAry[$SubjectID][0][Grade]);
								
								if($needStyle)
								{
									$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisOverall);
									$thisOverallDisplay = $thisOverall ? $this->ReturnTextwithStyle($thisOverall, 'HighLight', $thisNature) : $thisOverall;
								}
								else
									$thisOverallDisplay = $thisOverall;
									
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='top'>". $thisOverallDisplay ."</td>";
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
								
								
								# Overall
								$thisOverall = $ScaleDisplay=="M" ? number_format(ceil($MarksAry[$SubjectID][0][Mark]),0) : "";
								if(!$StudentID)
								{
									$thisOverall = $ScaleDisplay=="M" ? "100" : "";
								}
								$thisOverall = ($ScaleDisplay=="M" && !strlen($thisOverall)) ? "--" : $thisOverall;
								//$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisOverall);
								//$thisOverallDisplay = $thisOverall ? $this->ReturnTextwithStyle($thisOverall, 'HighLight', $thisNature) : $thisOverall;
								list($thisOverall, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $thisOverall, $MarksAry[$SubjectID][0][Grade]);
								
								# add the rounded subject overall for grand total display
								if ($ScaleDisplay=="M" && is_numeric($thisOverall))
								{
									$thisColumnWeightConds = " ReportColumnID IS NULL AND SubjectID = '$SubjectID' " ;
									$thisColumnSubjectWeightArr = $this->returnReportTemplateSubjectWeightData($ReportID, $thisColumnWeightConds);
									$thisColumnSubjectWeightTemp = $thisColumnSubjectWeightArr[0]['Weight'];
							
									$GrandTotal += $thisOverall * $thisColumnSubjectWeightTemp;
								}
								
								if($needStyle)
								{
									$thisNature = $this->returnMarkNature($ClassLevelID, $SubjectID, $thisOverall);
									$thisOverallDisplay = $thisOverall ? $this->ReturnTextwithStyle($thisOverall, 'HighLight', $thisNature) : $thisOverall;
								}
								else
									$thisOverallDisplay = $thisOverall;
									
								$thisOverallDisplay = $ScaleDisplay=="M" ? $thisOverallDisplay : "";
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='top'>". $thisOverallDisplay ."</td>";
								$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
								////////////////////////////////////////////
							}
							
							# Grade
							$thisBand = $ScaleDisplay=="M" ? $this->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $MarksAry[$SubjectID][0][Mark]) : "";
							if(!$StudentID)
							{
								$thisBand = $ScaleDisplay=="M" ? "1" : "";
							}
							$thisBand = ($ScaleDisplay=="M" && !strlen($thisBand)) ? "--" : $thisBand;
							$thisBandDisplay = $thisBand ? $this->ReturnTextwithStyle($thisBand, 'HighLight', $thisNature) : $thisBand;
							$tempMark = $MarksAry[$SubjectID][0]['Mark'];
							list($thisBandTemp, $needStyle) = $this->checkSpCase($ReportID, $SubjectID, $tempMark, $MarksAry[$SubjectID][0]['Grade']);
							$thisBandDisplay = ($tempMark != $thisBandTemp)	? "" : $thisBandDisplay;
							$x[$SubjectID] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='top'>". $thisBandDisplay ."</td>";
						}
						
					}
					# construct an array to return
					$returnArr['HTML'][$SubjectID] = $x[$SubjectID];
					$returnArr['isAllNA'][$SubjectID] = $isAllNA;
					$returnArr['GrandTotal'] = $GrandTotal;
				}
				break;
		}
		return $returnArr;
	}
	
	function genMSTableMarksByCSV($ParInfoArr)
	{
		global $eReportCard;
		$x = array();
		
		$subjectArr = $ParInfoArr['Subject'];
		$subjectSize = count($subjectArr);
		
		# Do not round number for "Achieve't Grade" and "First Combined" Column
		$columnTitleArr = $ParInfoArr['ColumnTitle'];
		for ($i=0; $i<count($columnTitleArr); $i++)
		{
			$thisTitle = $columnTitleArr[$i];
			if (substr($thisTitle,0,7) == "Achieve" || $thisTitle == "First Combined")
			{
				$AchGradePosition = $i;
				break;
			}
		}
		
		for ($i=0; $i<$subjectSize; $i++)
		{
			$thisSubjectName = $subjectArr[$i];
			
			$thisMarksArr = $ParInfoArr[$thisSubjectName];
			$markSize = count($thisMarksArr);
			
			for ($j=0; $j<$markSize; $j++)
			{
				$thisMark = $thisMarksArr[$j];
				if (is_numeric($thisMark) && $j!=$AchGradePosition)
				{
					$thisMark = $this->getDisplayMarkdp("SubjectScore", $thisMark);
				}
				
				$x[$thisSubjectName] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['Mark'] ."' class='result_col_label' align='center' valign='bottom'>". $thisMark ."</td>";
				
				if ($j != ($markSize-1))
				{
					$x[$thisSubjectName] .= "<td width='". $eRCTemplateSetting['ColumnWidth']['MarkSpace'] ."'>&nbsp;</td>";
				}
			}
			
			# construct an array to return
			$returnArr['HTML'][$thisSubjectName] = $x[$thisSubjectName];
			$returnArr['isAllNA'][$thisSubjectName] = false;
		}
		
		return $returnArr;
	}

	function returnTemplateSubjectCol($ReportID, $ClassLevelID)
	{
		global $eRCTemplateSetting, $PATH_WRT_ROOT;
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
 		$ReportType = $SemID == "F" ? "W" : "T";		
 		$ClassLevelID = $ReportSetting['ClassLevelID'];
 		$ClassLevel = $this->returnClassLevel($ClassLevelID);
 		
		$SubjectArray = $this->returnSubjectwOrder($ClassLevelID);
 		$SubjectDisplay = $eRCTemplateSetting['ColumnHeader']['Subject'];
 		
 		# 24 Jun 2009 special handling for class suspension
 		# show "*" after the subject name
 		$activeYear = $this->GET_ACTIVE_YEAR();
 		
		$excludeSubjectIDArr = $eRCCalculationSetting['ExcludeSubjectInGrandMark'][$activeYear]['SubjectID'];
		$excludeClassLevelNameArr = $eRCCalculationSetting['ExcludeSubjectInGrandMark'][$activeYear]['ClassLevelName'];
		$haveExcludeSubject = (is_array($excludeSubjectIDArr))? 1 : 0;
		
 		$x = array(); 
		$isFirst = 1;
 		foreach($SubjectArray as $SubjectID=>$Ary)
 		{
	 		foreach($Ary as $SubSubjectID=>$Subjs)
	 		{
		 		$t = "";
		 		$Prefix = "&nbsp;&nbsp;&nbsp;&nbsp;";
		 		
		 		if($SubSubjectID==0)		# Main Subject
		 		{
			 		$SubSubjectID=$SubjectID;
			 		$Prefix = "";
		 		}
// 		 		else
// 		 		{
// 			 		debug_r($SubSubjectID);
// 			 		if(!$eRCTemplateSetting['DisplaySubSubject'])	continue;
// 		 		}

				# check subject weight
				$sw = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID=$SubSubjectID and ReportColumnID is NULL");
				$thisWeightStr = $sw[0]['Weight'] < 1 ? " *" : "";
		 		
		 		$css_border_top = ($isFirst or $Prefix)? "" : "border_top";
		 		foreach($SubjectDisplay as $k=>$v)
		 		{
			 		$SubjectEng = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "EN");
	 				$SubjectChn = $this->GET_SUBJECT_NAME_LANG($SubSubjectID, "CH");
	 				
	 				if ($ReportType=="W" && $haveExcludeSubject)
					{
						if (in_array($ClassLevel, $excludeClassLevelNameArr) && in_array($SubjectID, $excludeSubjectIDArr))
						{
							$SubjectEng .= " *";
							$SubjectChi .= " *";
						}
					}
	 				
	 				$v = str_replace("SubjectEng", $SubjectEng, $v);
	 				$v = str_replace("SubjectChn", $SubjectChn, $v);
	 				
			 		$t	.= "<td width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."' class='result_col_label' align='left' nowrap>". $Prefix. $v.$thisWeightStr."</td>";
		 		}
				$x[] = $t;
				$isFirst = 0;
			}
	 	}
 		return $x;
	}
	
	function returnTemplateSubjectColByCSV($ParInfoArr)
	{
		global $eRCTemplateSetting;
		$x = array();
		
	 	$subjectArr = $ParInfoArr['Subject'];
	 	$subjectSize = count($subjectArr);
	 	for ($i=0; $i<$subjectSize; $i++)
	 	{
		 	$thisSubjectName = $subjectArr[$i];
		 	$t = "";
		 	
		 	$t = "<td width='". $eRCTemplateSetting['ColumnWidth']['Subject'] ."' class='result_col_label' align='left'>". $thisSubjectName ."</td>";
		 	$x[] = $t;
	 	}
	 	
 		return $x;
	}
	
	function getMiscTable($ReportID, $StudentID='')
	{
		global $eRCTemplateSetting, $eReportCard, $PATH_WRT_ROOT;
		include($PATH_WRT_ROOT."includes/eRCConfig.php");
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		$AllowClassTeacherComment = $ReportSetting['AllowClassTeacherComment'];
		$SemID = $ReportSetting['Semester'];
 		$ReportType = $SemID == "F" ? "W" : "T";		
 		$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
 		
 		# Retrieve Misc. table's data
 		$MiscData = $this->getMiscTableData($ReportID, $StudentID);
 		
 		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
 		# Get the GrandTotal from the overall column if required
		if ((substr($ClassLevel, 0, 1) == "S") && ($eRCCalculationSetting['GrandTotalDisplay'] == "FromOverall") && (sizeof($ColumnData)!=2))
 		{
	 		# retrieve Marks Array
	 		$MarksAry = $this->getMarks($ReportID, $StudentID);
			$MSTableReturned = $this->genMSTableMarks($ReportID, $MarksAry, $StudentID);
			$GrandTotalFromOverallColumn = $MSTableReturned['GrandTotal'];
			
			$MiscData['Marks'] =  my_round($MSTableReturned['GrandTotal'], 0);
			$MiscData['Percentage'] = ($MiscData['Marks'] / $MiscData['TotalMarks']) * 100;
			$MiscData['Percentage'] = my_round($MiscData['Percentage'], 1);
 		}
 		
		##########################################
		# Start Misc. Table
		##########################################				
		$MiscTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
		//$MiscTable = "<table width='100%' border='0' cellpadding='2' cellspacing='0' style=\"page-break-before:always\">";

		# Attendance / Conduct / Total / Percentage / Passed Failed / Promoted..
		$MiscTable .= "<tr>";
		#Attendance
		$MiscTable .= "<td align='left' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Attendance'] ."</td>";
		$MiscTable .= "<td align='left' class='colon_col' width='5'>:&nbsp;</td>";
		$MiscTable .= "<td align='left' class='result_mark'>". $MiscData['Attendance'] ."</td>";
		# Total
		$MiscTable .= "<td align='left' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Total'] ."</td>";
		$MiscTable .= "<td align='left' class='colon_col' width='5'>:&nbsp;</td>";
		$MiscTable .= "<td align='right' class='result_mark' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $MiscData['Marks'] ." /<b>". $MiscData['TotalMarks'] ."</b></td>";
		$MiscTable .= "</tr>";
		$MiscTable .= "<tr>";
		# Conduct
		$MiscTable .= "<td align='left' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Conduct'] ."</td>";
		$MiscTable .= "<td align='left' class='colon_col' width='". $eRCTemplateSetting['ColumnWidth']['MiscColColon'] ."'>:&nbsp;</td>";
		$MiscTable .= "<td align='left' class='result_mark' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth2'] ."'>". $MiscData['Conduct'] ."</td>";
		# Percentage
		$MiscTable .= "<td align='left' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Percentage'] ."</td>";
		$MiscTable .= "<td align='left' class='colon_col' width='". $eRCTemplateSetting['ColumnWidth']['MiscColColon'] ."'>:&nbsp;</td>";
		$MiscTable .= "<td align='right' class='result_mark' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $MiscData['Percentage'] ."</td>";
		$MiscTable .= "</tr>";
		
		if((substr($ClassLevel, 0, 1)=="P" && $ReportType=="W") or (substr($ClassLevel, 0, 1)=="S" && sizeof($ColoumnTitle)==4))
		{
			$MiscTable .= "<tr>";
			# Promoted / Retained / Advanced
			$PromoteStr = ($MiscData['Promotion']!=1 ? "<s>":"") . $eReportCard['Template']['Promoted'] . ($MiscData['Promotion']!=1 ? "</s>":"") . " / " . ($MiscData['Promotion']!=0 ? "<s>":"") . $eReportCard['Template']['Retained'] . ($MiscData['Promotion']!=0 ? "</s>":"") . " / " . ($MiscData['Promotion']!=2 ? "<s>":"") . $eReportCard['Template']['Advanced'] . ($MiscData['Promotion']!=2 ? "</s>":"");
			$MiscTable .= "<td align='left' class='result_col_label' colspan='3' width='". ($eRCTemplateSetting['ColumnWidth']['MiscColWidth']+$eRCTemplateSetting['ColumnWidth']['MiscColColon']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth2'])."'>". $PromoteStr . ": ". $MiscData['NewClassName'] ."</td>";
			# Passed / Failed
			$Pass = $MiscData['Passed']  ? $eReportCard['Template']['Passed'] : "<s>".$eReportCard['Template']['Passed']."</s>";
 			$Fail = !$MiscData['Passed'] ? $eReportCard['Template']['Failed'] : "<s>".$eReportCard['Template']['Failed']."</s>";
			$PassStr = $Pass . " / " . $Fail;
			$MiscTable .= "<td align='left' class='result_col_label' colspan='3'>". $PassStr ."</td>";
			$MiscTable .= "</tr>";
		}
		
		# Class Teacher Comment		
		if($AllowClassTeacherComment)
		{
			$MiscTable .= "<tr>";
			$MiscTable .= "<td align='left' valign='top' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Remarks'] ."</td>";
			$MiscTable .= "<td align='left' valign='top' class='colon_col' width='".$eRCTemplateSetting['ColumnWidth']['MiscColColon']."'>:&nbsp;</td>";
			$MiscTable .= "<td colspan='4' align='left' valign='top' class='result_mark' width='". ($eRCTemplateSetting['ColumnWidth']['MiscColWidth2']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']+$eRCTemplateSetting['ColumnWidth']['MiscColColon']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']) ."'>". nl2br($MiscData['TComment']) ."</td>";
			$MiscTable .= "</tr>";
		}
		
                         
		# CCA Remarks
		/*
		$MiscTable .= "<tr>";
		$MiscTable .= "<td align='left' valign='top' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['CCA'] ."</td>";
		$MiscTable .= "<td align='left' valign='top' class='colon_col' width='".$eRCTemplateSetting['ColumnWidth']['MiscColColon']."'>:&nbsp;</td>";
		$MiscTable .= "<td colspan='4' align='left' valign='top' class='result_mark' width='". ($eRCTemplateSetting['ColumnWidth']['MiscColWidth2']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']+$eRCTemplateSetting['ColumnWidth']['MiscColColon']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']) ."'>". nl2br($MiscData['CCA']) ."</td>";
		$MiscTable .= "</tr>";
		*/

		/* Hidden on 03 Jun 2009
		if((substr($ClassLevel, 0, 1)=="P" && $ReportType=="W") or (substr($ClassLevel, 0, 1)=="S" && sizeof($ColoumnTitle)==4))
		{
			# CIP
			$MiscTable .= "<tr>";
	        $MiscTable .= "<td colspan='5' align='left'><span class='result_col_label'>Community Involvement Programme (CIP) Hours: </span></td>";
	        $MiscTable .= "<td align='left' valign='top' class='result_mark'>". $MiscData['CIP'] ."</td>";
			$MiscTable .= "</tr>";
			
			# NAPFA only display P4+ (Last Report)
			if(!(substr($ClassLevel, 0, 1)=="P" && substr($ClassLevel, 1, 1) < "4"))
			{
				# NAPFA
				$MiscTable .= "<tr>";
				$MiscTable .= "<td align='left' valign='top' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['NAPFA'] ."</td>";
				$MiscTable .= "<td align='left' valign='top' class='colon_col' width='".$eRCTemplateSetting['ColumnWidth']['MiscColColon']."'>:&nbsp;</td>";
				$MiscTable .= "<td colspan='4' align='left' valign='top' class='result_mark' width='". ($eRCTemplateSetting['ColumnWidth']['MiscColWidth2']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']+$eRCTemplateSetting['ColumnWidth']['MiscColColon']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']) ."'>". nl2br($MiscData['NAPFA']) ."</td>";
				$MiscTable .= "</tr>";
			}
		}
		*/
		
		# Class Suspension special remarks
		$activeYear = $this->GET_ACTIVE_YEAR();
		$specialRemarks = $eReportCard['Template']['ClassSuspension']['Remarks'][$activeYear][$ClassLevel];
		if ($ReportType=='W' && $specialRemarks != '')
		{
			$specialRemarks = "* (".$specialRemarks.")";
			$MiscTable .= "<tr>";
				$MiscTable .= "<td colspan='6' align='left' valign='top' class='result_mark'><div align='justify'>". $specialRemarks ."</div></td>";
			$MiscTable .= "</tr>";
		}
		
		$MiscTable .= "</table>";
		##########################################
		# End Misc. Table
		##########################################		
		
		return $MiscTable;
	}
	
	function getMiscTableByCSV($ParInfoArr)
	{
		global $eRCTemplateSetting, $eReportCard;
		
		##########################################
		# Start Misc. Table
		##########################################				
		$MiscTable = "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";

		# Attendance / Conduct / Total / Percentage / Passed Failed / Promoted..
		$MiscTable .= "<tr>";
		# Attendance
		$MiscTable .= "<td align='left' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Attendance'] ."</td>";
		$MiscTable .= "<td align='left' class='colon_col' width='5'>:&nbsp;</td>";
		$MiscTable .= "<td align='left' class='result_mark'>". $ParInfoArr['Attendance'] ."</td>";
		# Total
		$thisMark = $ParInfoArr['Total'];
		$thisMarkArr = explode("/", $thisMark);
		$thisGrandTotal = $this->getDisplayMarkdp("SubjectTotal", trim($thisMarkArr[0]));
		$thisFullMark = trim($thisMarkArr[1]);
		
		$MiscTable .= "<td align='left' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Total'] ."</td>";
		$MiscTable .= "<td align='left' class='colon_col' width='5'>:&nbsp;</td>";
		$MiscTable .= "<td align='right' class='result_mark' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $thisGrandTotal ." /<b>". $thisFullMark ."</b></td>";
		$MiscTable .= "</tr>";
		$MiscTable .= "<tr>";
		# Conduct
		$MiscTable .= "<td align='left' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Conduct'] ."</td>";
		$MiscTable .= "<td align='left' class='colon_col' width='". $eRCTemplateSetting['ColumnWidth']['MiscColColon'] ."'>:&nbsp;</td>";
		$MiscTable .= "<td align='left' class='result_mark' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth2'] ."'>". $ParInfoArr['Conduct'] ."</td>";
		# Percentage
		$thisPercentage = $this->getDisplayMarkdp("GrandAverage", $ParInfoArr['Percentage']);
		$MiscTable .= "<td align='left' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Percentage'] ."</td>";
		$MiscTable .= "<td align='left' class='colon_col' width='". $eRCTemplateSetting['ColumnWidth']['MiscColColon'] ."'>:&nbsp;</td>";
		$MiscTable .= "<td align='right' class='result_mark' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $thisPercentage ."</td>";
		$MiscTable .= "</tr>";
		
		# Class Teacher Comment		
		if($ParInfoArr['Remarks'])
		{
			$MiscTable .= "<tr>";
			$MiscTable .= "<td align='left' valign='top' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['Remarks'] ."</td>";
			$MiscTable .= "<td align='left' valign='top' class='colon_col' width='".$eRCTemplateSetting['ColumnWidth']['MiscColColon']."'>:&nbsp;</td>";
			$MiscTable .= "<td colspan='4' align='left' valign='top' class='result_mark' width='". ($eRCTemplateSetting['ColumnWidth']['MiscColWidth2']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']+$eRCTemplateSetting['ColumnWidth']['MiscColColon']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']) ."'>". nl2br($ParInfoArr['Remarks']) ."</td>";
			$MiscTable .= "</tr>";
		}
		
		# Community Involvement Programme (CIP) Hours
		if($ParInfoArr["CIPHours"])
		{
			$MiscTable .= "<tr>";
			$MiscTable .= "<td align='left' colspan='4' valign='top' class='result_col_label' >". $eReportCard['Template']['CIPHours'] ."</td>";
			$MiscTable .= "<td align='left' valign='top' class='colon_col' >:&nbsp;</td>";
			$MiscTable .= "<td align='left' valign='top' class='result_mark' >". nl2br($ParInfoArr['CIPHours']) ."</td>";
			$MiscTable .= "</tr>";
		}
		
		# NAPFA
		if($ParInfoArr["NAPFA"])
		{
			$MiscTable .= "<tr>";
			$MiscTable .= "<td align='left' valign='top' class='result_col_label' width='". $eRCTemplateSetting['ColumnWidth']['MiscColWidth'] ."'>". $eReportCard['Template']['NAPFA'] ."</td>";
			$MiscTable .= "<td align='left' valign='top' class='colon_col' width='".$eRCTemplateSetting['ColumnWidth']['MiscColColon']."'>:&nbsp;</td>";
			$MiscTable .= "<td colspan='4' align='left' valign='top' class='result_mark' width='". ($eRCTemplateSetting['ColumnWidth']['MiscColWidth2']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']+$eRCTemplateSetting['ColumnWidth']['MiscColColon']+$eRCTemplateSetting['ColumnWidth']['MiscColWidth']) ."'>". nl2br($ParInfoArr['NAPFA']) ."</td>";
			$MiscTable .= "</tr>";
		}
		
		/*
		# CCA Membership
		if($ParInfoArr["CCAMembership"])
		{
			$MiscTable .= "<tr>";
				$MiscTable .= "<td colspan='6'>";
					$MiscTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						$MiscTable .= "<tr>";
							$MiscTable .= "<td align='left' valign='top' class='result_col_label' width='".$eRCTemplateSetting['ColumnWidth']['Subject']."'>". $eReportCard['Template']['CCAMembership'] ."</td>";
							$MiscTable .= "<td align='left' valign='top' class='colon_col' width='".$eRCTemplateSetting['ColumnWidth']['MiscColColon']."'>:&nbsp;</td>";
							$MiscTable .= "<td align='left' valign='top' class='result_mark' >". nl2br($ParInfoArr['CCAMembership']) ."</td>";
						$MiscTable .= "</tr>";
					$MiscTable .= "</table>";
				$MiscTable .= "</td>";
			$MiscTable .= "</tr>";
		}
		
		# CCA Achievement(s)
		if($ParInfoArr["CCAAchievements"])
		{
			$thisDisplay = implode("<br />", $ParInfoArr['CCAAchievements']);
			$MiscTable .= "<tr>";
				$MiscTable .= "<td colspan='6'>";
					$MiscTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						$MiscTable .= "<tr>";
							$MiscTable .= "<td align='left' valign='top' class='result_col_label' width='".$eRCTemplateSetting['ColumnWidth']['Subject']."'>". $eReportCard['Template']['CCAAchievements'] ."</td>";
							$MiscTable .= "<td align='left' valign='top' class='colon_col' width='".$eRCTemplateSetting['ColumnWidth']['MiscColColon']."'>:&nbsp;</td>";
							$MiscTable .= "<td align='left' valign='top' class='result_mark' >". $thisDisplay ."</td>";
						$MiscTable .= "</tr>";
					$MiscTable .= "</table>";
				$MiscTable .= "</td>";
			$MiscTable .= "</tr>";
		}
		*/
		
		$numOfOtherInfo = count($ParInfoArr["OtherInfoTitle"]);
		for ($i=0; $i<$numOfOtherInfo; $i++)
		{
			$thisTitle = $ParInfoArr["OtherInfoTitle"][$i];
			$thisContent = $ParInfoArr[$thisTitle];
			
			if (is_array($thisContent))
			{
				$thisDisplay = implode("<br />", $ParInfoArr[$thisTitle]);
			}
			else
			{
				$thisDisplay = $thisContent;
			}
			
			$MiscTable .= "<tr>";
				$MiscTable .= "<td colspan='6'>";
					$MiscTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						$MiscTable .= "<tr>";
							$MiscTable .= "<td align='left' valign='top' class='result_col_label' width='".$eRCTemplateSetting['ColumnWidth']['Subject']."'>". $thisTitle ."</td>";
							$MiscTable .= "<td align='left' valign='top' class='colon_col' width='".$eRCTemplateSetting['ColumnWidth']['MiscColColon']."'>:&nbsp;</td>";
							$MiscTable .= "<td align='left' valign='top' class='result_mark' >". $thisDisplay ."</td>";
						$MiscTable .= "</tr>";
					$MiscTable .= "</table>";
				$MiscTable .= "</td>";
			$MiscTable .= "</tr>";
		}
		
		$MiscTable .= "</table>";
		##########################################
		# End Misc. Table
		##########################################		
		
		return $MiscTable;
	}
	
	function getSignatureTable()
	{
 		global $eReportCard, $eRCTemplateSetting;
 		
		$SignatureTitleArray = $eRCTemplateSetting['Signature'];

		$SignatureTable = "";
		
		$SignatureTable = "<table width='100%' border='0' cellpadding='1' cellspacing='0' align='center' valign='bottom'>";
		
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SignatureTable .= "<td class='dot_bottom_border'>&nbsp;</td>";
			if($k<sizeof($SignatureTitleArray)-1)	
				$SignatureTable .= "<td class='signature_space'>&nbsp;</td>";
		}
		$SignatureTable .= "</tr>";
		
		$SignatureTable .= "<tr>";
		for($k=0; $k<sizeof($SignatureTitleArray); $k++)
		{
			$SettingID = trim($SignatureTitleArray[$k]);
			$Title = $eReportCard['Template'][$SettingID];

			$SignatureTable .= "<td class='signature_label'>".$Title."</td>";
			if($k<sizeof($SignatureTitleArray)-1)	
				$SignatureTable .= "<td>&nbsp;</td>";
		}
		$SignatureTable .= "</tr>";
		$SignatureTable .= "</table>";
		
		return $SignatureTable;
	}
	
	function getFooter($ReportID)
	{
		global $eReportCard;
		$FooterRow = "";
		
		if($ReportID)
		{
			# Retrieve Display Settings
			$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
			$Footer = $ReportSetting['Footer'];
			$FooterRow = "<tr><td class='result_mark'>".str_replace("\n", "<br />", intranet_undo_htmlspecialchars($Footer))."</td></tr>\n";
		}
		
		return $FooterRow;
	}
	
	function getMiscTableData($ReportID, $StudentID='', $ReportColumnID=0)
	{
		global $eReportCard;
		
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		
		$x = array();
		
		# Marks
		$ReportResultScoreAry = $this->getReportResultScore($ReportID, $ReportColumnID, $StudentID);
		$x['Marks'] = $ReportResultScoreAry['GrandTotal'];
		$x['Promotion'] = $ReportResultScoreAry['Promotion'];
		$x['NewClassName'] = $ReportResultScoreAry['NewClassName'];
		$x['Passed'] = $ReportResultScoreAry['Passed'];
						
		# Totax Marks
		$x['TotalMarks'] = $this->returnGrandTotalFullMark($ReportID, $StudentID);
		if(!$StudentID) 	$x['Marks'] = $x['TotalMarks'];
		

		# Percentage
		# Modified by Andy on 29/4/2008, to prevent divide by Zero
		 if ($x['TotalMarks'] != 0)
			//$x['Percentage'] = number_format(($x['Marks'] / $x['TotalMarks'])*100,1);
			$x['Percentage'] = number_format($this->getDisplayMarkdp("GrandAverage", ($x['Marks'] / $x['TotalMarks'])*100), 1);
    	else
    		$x['Percentage'] = "-";
	
    	# if Secondary School, data is retrieve from 2nd report
    	if(substr($ClassLevel, 0, 1)=="S")
    	{
	    	$ColoumnTitle = $this->returnReportColoumnTitle($ReportID);
	    	if(sizeof($ColoumnTitle)==4)
	    	{
		    	$table = $this->DBName.".RC_REPORT_TEMPLATE";
		    	$sql = "	
					SELECT 
						a.ReportID
					FROM 
						$table as a
						left join INTRANET_CLASSLEVEL as b on a.ClassLevelID = b.ClassLevelID
					WHERE 
						a.ClassLevelID = $ClassLevelID 
						and a.Semester='F' and ReportID<> ". $ReportID ."
					ORDER BY a.ReportID ";
				$temp = $this->returnArray($sql);
				$ReportID = $temp[1]['ReportID'];
	    	}
    	}
    	
		# Teacher Comment
		$TeacherCommentAry = $this->GET_TEACHER_COMMENT(array($StudentID), '', $ReportID);
		foreach($TeacherCommentAry as $k => $ary)
			$x['TComment'] .= $ary[Comment];

		# Attendance / Conduct / CIP / CCA Remark
		$StuOtherInfoAry = $this->GET_OTHER_STUDENT_INFO(array($StudentID), $ReportID);
		$x['Attendance'] = $eReportCard['OtherStudentInfo_Attendance'][$StuOtherInfoAry[$StudentID]['Attendance']];
		$x['Conduct'] = $eReportCard['OtherStudentInfo_Conduct'][$StuOtherInfoAry[$StudentID]['Conduct']];
		$x['CIP'] = $StuOtherInfoAry[$StudentID]['CIPHours'];
		$x['CCA'] = $StuOtherInfoAry[$StudentID]['CCA'];
		//$x['NAPFA'] = $eReportCard['OtherStudentInfo_NAPFA'][$StuOtherInfoAry[$StudentID]['NAPFA']];
		$x['NAPFA'] = $StuOtherInfoAry[$StudentID]['NAPFA'];
		
		return $x;
	}
	
	function getReportResultScore($ReportID, $ReportColumnID=0, $StudentID) 
	{
		$table = $this->DBName.".RC_REPORT_RESULT";
		$fields = "StudentID, SubjectID, ReportID, ReportColumnID, Mark, Grade, IsOverall, Semester, OrderMeritClass, OrderMeritForm, DateInput, DateModified";
		$sql = "
			select
				GrandTotal,
				Promotion,
				NewClassName,
				Passed,
				GrandAverage,
				OrderMeritClass,
				OrderMeritForm,
				RawGrandAverage
			from 
				$table
			where
				ReportID = $ReportID 
				and ReportColumnID = $ReportColumnID
				and StudentID = $StudentID
		";
		$result = $this->returnArray($sql);
		return $result[0];
	}
	
	############################################
	# cus for SIS SEND (replace function in 2008)
	############################################
	function returnGrandTotalFullMark($ReportID, $StudentID='')
	{
		# Retrieve Display Settings
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
	
		# check if the grand total is abnormal
		$gt = $this->returnStudentSubjectSPFullMark($ReportID, 0, $StudentID, 0);
		if($gt)	return $gt[0];
	
		$MarksAry = $this->getMarks($ReportID, $StudentID);						# Student Marks
		$SubjectFullMarkAry = $this->returnSubjectFullMark($ClassLevelID, 0);
		//debug_r($SubjectFullMarkAry);
		$x = 0;
		if(sizeof($SubjectFullMarkAry) > 0 && is_array($SubjectFullMarkAry)){
			foreach($SubjectFullMarkAry as $SubjectID=>$v)
			{
				$thisWeightAry = $this->returnReportTemplateSubjectWeightData($ReportID, "SubjectID=$SubjectID and ReportColumnID is NULL");
				$thisWeight = $thisWeightAry[0]['Weight'];
				if(!empty($MarksAry[$SubjectID]) or !$StudentID)	
						$x += $v * $thisWeight;
				
		/*
				$SPFull = $this->returnStudentSubjectSPFullMark($ReportID, $SubjectID, $StudentID);
				
				if(!empty($SPFull))		
					$x += $SPFull[0];
				else
					if(!empty($MarksAry[$SubjectID]) or !$StudentID)	
						$x += $v;	
	*/
			}
		}
		return $x;
	}
	
	############################################
	# cus for SIS END
	############################################
	
	# For Other Student Info use in SIS
	# Get other student info (CIP hours, Attendance, Conduct, CCA Remarks, NAPFA)
	function GET_OTHER_STUDENT_INFO($StudentIDList, $ReportID) {
		global $PATH_WRT_ROOT;
		
		$returnArr = array();
		if (sizeof($StudentIDList) < 1) {
			return $returnArr;
		}
		
		$StudentIDArr = $StudentIDList;
		
		$StudentIDList = implode(",", $StudentIDList);
		$table = $this->DBName.".RC_OTHER_STUDENT_INFO";
		$sql = "SELECT 
					OtherStudentInfoID, StudentID, CIPHours, 
					Attendance, Conduct, CCA, NAPFA
				FROM 
					$table 
				WHERE 
					ReportID = '$ReportID' AND 
					StudentID IN ($StudentIDList)";

		$result = $this->returnArray($sql);
		
		if(sizeof($result) > 0) {
			for($i=0; $i<sizeof($result); $i++) {
				$thisStudentID = $result[$i]["StudentID"];
				$returnArr[$thisStudentID]["OtherStudentInfoID"] = $result[$i]["OtherStudentInfoID"];
				$returnArr[$thisStudentID]["CIPHours"] = $result[$i]["CIPHours"];
				$returnArr[$thisStudentID]["Attendance"] = $result[$i]["Attendance"];
				$returnArr[$thisStudentID]["Conduct"] = $result[$i]["Conduct"];
				$returnArr[$thisStudentID]["CCA"] = $result[$i]["CCA"];
				$returnArr[$thisStudentID]["NAPFA"] = $result[$i]["NAPFA"];
			}
		}
		
		/*
		# For getting NAPFA Award
		include_once($PATH_WRT_ROOT."includes/libPMS.php");
		include_once($PATH_WRT_ROOT."includes/config.inc.php");
		include_once($PATH_WRT_ROOT."includes/libdb.php");
		
		for($i=0; $i<sizeof($StudentIDArr); $i++) {
			$thisStudentID = $StudentIDArr[$i];
			$returnArr[$thisStudentID]["NAPFA"] = getStudentFitnessAward($thisStudentID);
		}
		*/
		
		return $returnArr;
	}
	

	function returnSecondCombinedOverallMarks($ReportID, $StudentID='')
	{
		$ReportSetting = $this->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		$sql = "	
			SELECT 
				a.ReportID
			FROM 
				$table as a
				left join INTRANET_CLASSLEVEL as b on a.ClassLevelID = b.ClassLevelID
			WHERE 
				a.ClassLevelID = $ClassLevelID 
				and a.Semester='F' and ReportID<> ". $ReportID;
				
		$sql .= "
			ORDER BY a.ReportID
		";
		$x = $this->returnArray($sql);
		$ReportID2 = $x[1]['ReportID'];

		return $this->getMarks($ReportID2, $StudentID);
	}


	function returnGrandMSBand($ClassLevelID, $Marks='-1')
	{
		$ClassLevel = $this->returnClassLevel($ClassLevelID);
		$PS = substr($ClassLevel, 0, 1);
		$L = substr($ClassLevel, 1, 1);
		
		# SP CASE
		if(!(ereg ("[0-9.]{1,}", $Marks)))	return "";
						
		if($PS=="P")
		{
			if($L==5 || $L==6)	
			{
				if($Marks==-1)	return array("A*","A","B","C","D","E","U");
			
				if($Marks >= 91)	return "A*";
				if($Marks >= 75)	return "A";
				if($Marks >= 60)	return "B";
				if($Marks >= 50)	return "C";
				if($Marks >= 35)	return "D";
				if($Marks >= 20)	return "E";
				return "U";
			}
			else
			{
				if($Marks==-1)	return array(1,2,3,4);
			
				if($Marks >= 85)	return "1";
				if($Marks >= 70)	return "2";
				if($Marks >= 50)	return "3";
				return "4";
			}
		}
		else
		{
			if($Marks==-1)	return array("A1", "A2", "B3", "B4", "C5", "C6", "D7", "E8", "F9");
			
			if($Marks >= 75)	return "A1";
			if($Marks >= 70)	return "A2";
			if($Marks >= 65)	return "B3";
			if($Marks >= 60)	return "B4";
			if($Marks >= 55)	return "C5";
			if($Marks >= 50)	return "C6";
			if($Marks >= 45)	return "D7";
			if($Marks >= 40)	return "E8";
			return "F9";
		}
	}
	
	/*
	function ReportTemplatesInfo($ClassLevelID='', $cons='')
	{
		$table = $this->DBName.".RC_REPORT_TEMPLATE";
		
		$sql = "	
			SELECT 
				a.ReportID
			FROM 
				$table as a
				left join INTRANET_CLASSLEVEL as b on a.ClassLevelID = b.ClassLevelID
			WHERE 
				a.ClassLevelID = $ClassLevelID 
		";
		$sql .= $cons ? " and ". $cons : "";
		$sql .= "
			ORDER BY a.ReportID
		";
		$result = $this->returnArray($sql);
		
		return $result;	
	}
	*/
	
	
	function Get_Student_Transcript_HTML($StudentID, $isTemp=0)
	{
		$ClassHistoryArr = $this->Get_Student_Class_History($StudentID, $ExcludePY=1, $AcademicYearArr='', $WithReportOnly=1);
		$numClassHistory = count($ClassHistoryArr);
		
		$x = '';
		$CurPageNum = 0;
		$TotalNumOfPage = ceil($numClassHistory / 3);
		for ($i=0; $i<$numClassHistory; $i++)
		{
			$thisAcademicYear = $ClassHistoryArr[$i]['AcademicYear'];
			$thisClassName = $ClassHistoryArr[$i]['ClassName'];
			
			if (($i % 3) == 0)
			{
				$CurPageNum++;
				
				if ($CurPageNum > 1)
					$x .= '<br />'."\n";
				
				$x .= '<table valign="top" align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="page-break-after:always">';
					$x .= '<tr>';
						$x .= '<td valign="top" height="1020px">';
							$x .= $this->Get_Transcript_Header($StudentID);
						
			}
			
						$x .= $this->Get_Transcript_Year_Content($StudentID, $thisAcademicYear, $thisClassName);
			
			if (($i % 3) == 2 || $i==$numClassHistory-1)
			{
							$x .= $this->Get_Separator($Height=2);
						$x .= '</td>';
					$x .= '</tr>';
					$x .= '<tr><td>'.$this->Get_Transcript_Footer($CurPageNum, $TotalNumOfPage).'</td></tr>';
				$x .= '</table>';
			}
		}
		
		return $x;
	}
	
	function Get_Transcript_Header($StudentID)
	{
		global $eReportCard;
		include_once('libuser.php');
		
		$lu = new libuser($StudentID);
		
		$x = '';
		$x .= '<table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td class="transcript_title" style="text-align:center;">'.implode('<br />', (array)$eReportCard['ReportsArr']['TranscriptArr']['TitleArr']).'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= $this->Get_Empty_Space(10)."\n";
		$x .= '<table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= $this->Get_Transcript_Left_Empty_Cell();
				$x .= '<td class="transcript_text_bold" style="width:85px;">'.$eReportCard['ReportsArr']['TranscriptArr']['StudentName'].'</td>'."\n";
				$x .= '<td class="transcript_text" style="width:200px;">'.$lu->EnglishName.'</td>'."\n";
				$x .= '<td class="transcript_text_bold" style="width:110px;">'.$eReportCard['ReportsArr']['TranscriptArr']['StudentNumber'].'</td>'."\n";
				$x .= '<td class="transcript_text">'.$lu->AdmissionNo.'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Transcript_Footer($CurPageNum, $TotalNumOfPage)
	{
		global $eReportCard;
		
		$x = '';
		$x .= $this->Get_Separator($Height=4, $Color='#CCCCCC');
		
		### Date and Page Number
		$CurrentDateTime = $this->GET_CURRENT_DATETIME();
		$x .= '<table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td class="GMS_text14bi">'.$CurrentDateTime.'</td>'."\n";
				$x .= '<td class="GMS_text14bi" style="text-align:right">'.'Page '.$CurPageNum.' of '.$TotalNumOfPage.'</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	// Old function hardcoded Chi, Eng, Math, Sci as the Top Subjects
	// New function - display all mark-display subjects as the Top Subjects  
//	function Get_Transcript_Year_Content_Old($StudentID, $AcademicYear, $ClassName)
//	{
//		global $eReportCard, $eRCCalculationSetting;
//		
//		$thisLibRC = new libreportcardSIS($AcademicYear);
//		
//		### Get Main Subject Info
//		$SubjectCodeMappingArr = $thisLibRC->GET_SUBJECTS_CODEID_MAP($CodeAsKey=1);
//		$MainSubjectIDArr = array();
//		foreach ($this->SubjectCodeID as $SubjectName => $SubjectCode)
//		{
//			$MainSubjectInfoArr[$SubjectCode]['SubjectID'] = $SubjectCodeMappingArr[$SubjectCode];
//			$MainSubjectInfoArr[$SubjectCode]['Name'] = $thisLibRC->GET_SUBJECT_NAME($MainSubjectInfoArr[$SubjectCode]['SubjectID'], $ShortName=0, $Bilingual=0, $AbbrName=1);
//			
//			$MainSubjectIDArr[] = $MainSubjectInfoArr[$SubjectCode]['SubjectID'];
//		}
//		
//		### Get Non Main Subject List
//		//$ClassInfoArr = $thisLibRC->Get_ClassInfo_By_ClassName($ClassName);
//		$ClassInfoArr = $thisLibRC->Get_FormInfo_By_ClassName($ClassName);
//		$ClassLevelID = $ClassInfoArr[0]['ClassLevelID'];
//		$ClassLevelName = $thisLibRC->returnClassLevel($ClassLevelID);
//		
//		$SubjectInfoArr = $thisLibRC->returnSubjectwOrderNoL($ClassLevelID);
//		// filter component subject
//		$NonMainSubjectInfoArr = array();
//		foreach ($SubjectInfoArr as $thisSubjectID => $thisSubjectFullName)
//		{
//			if ($thisLibRC->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID) || in_array($thisSubjectID, $MainSubjectIDArr))
//				continue;
//				
//			$NonMainSubjectInfoArr[$thisSubjectID] = $thisLibRC->GET_SUBJECT_NAME($thisSubjectID, $ShortName=0, $Bilingual=0, $AbbrName=1);
//		}
//		$numOfNonMainSubject = count($NonMainSubjectInfoArr);
//		
//		### Get all Semester Info
//		$SemesterInfoMappingArr = $thisLibRC->GET_ALL_SEMESTERS($SemID='-1', $haveReportTemplate=false, $SemesterShortName=1);
//		
//		$SemesterArr = array();
//		$PSLevel = $thisLibRC->Get_PSClassLevel($ClassLevelID);
//		if ($PSLevel=='P' || $PSLevel=='PY')
//			$SemesterArr = array(1, 3);
//		else if ($PSLevel=='S')
//			$SemesterArr = array(2, 3);
//		$numOfSemester = count($SemesterArr);
//			
//		### Get all Report Info
//		$ConsolidateReportInfo = $thisLibRC->returnReportTemplateBasicInfo('', $others="ClassLevelID = '$ClassLevelID' And Semester = 'F'");
//		$ReportID = $ConsolidateReportInfo['ReportID'];
//		$ReportColumnArr = $thisLibRC->returnReportTemplateColumnData($ReportID);
//		$ReportColumnID_SemesterMapping = array();
//		$numOfColumn = count($ReportColumnArr);
//		for ($i=0; $i<$numOfColumn; $i++)
//		{
//			$thisReportColumnID = $ReportColumnArr[$i]['ReportColumnID'];
//			$thisSemesterID = $ReportColumnArr[$i]['SemesterNum'];
//			
//			$ReportColumnID_SemesterMapping[$thisSemesterID] = $thisReportColumnID;
//		}
//		
//		### Get Marks
//		//$MarkArr[$SubjectID][$ReportColumnID][field] = value;
//		$MarkArr = $thisLibRC->getMarks($ReportID, $StudentID);
//		
//		### Get Grading Scheme Info
//		$GradingSchemArr = $thisLibRC->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
//		
//		
//		$numOfColumn = 14;
//		$x = '';
//		$x .= $this->Get_Separator();
//		$x .= '<table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0">'."\n";
//			$x .= '<tr valign="top">'."\n";
//				$x .= $this->Get_Transcript_Left_Empty_Cell($numOfColumn);
//				# School Year & Class Name
//				$x .= '<td rowspan="'.$numOfColumn.'" class="transcript_text_bold" style="width:120px">'."\n";
//					$x .= '<table width="100%" valign="top" border="0" cellpadding="0" cellspacing="0">'."\n";
//						$x .= '<tr>'."\n";
//							$x .= '<td class="transcript_text_bold" nowrap>'.$eReportCard['ReportsArr']['TranscriptArr']['SchoolYear'].'</td>'."\n";
//							$x .= '<td class="transcript_text_bold" style="width:15px"> : </td>'."\n";
//							$x .= '<td class="transcript_text" style="width:50px">'.$AcademicYear.'</td>'."\n";
//						$x .= '</tr>'."\n";	
//						$x .= '<tr>'."\n";
//							$x .= '<td class="transcript_text_bold">'.$eReportCard['ReportsArr']['TranscriptArr']['ClassName'].'</td>'."\n";
//							$x .= '<td class="transcript_text_bold"> : </td>'."\n";
//							$x .= '<td class="transcript_text">'.$ClassName.'</td>'."\n";
//						$x .= '</tr>'."\n";	
//					$x .= '</table>'."\n";
//				$x .= '</td>'."\n";	
//				
//				# Empty Column
//				$x .= '<td rowspan="'.$numOfColumn.'" style="width:10px">&nbsp;</td>'."\n";
//				
//				# Semester Title
//				$x .= '<td class="transcript_semester_text" style="width:30px">&nbsp;</td>'."\n";
//				
//				# Empty Column
//				$x .= '<td style="width:10px">&nbsp;</td>'."\n";
//				
//				# 1st subject name of each term
//				$x .= '<td class="transcript_text_bold" style="width:90px">&nbsp;</td>'."\n";
//				
//				# 1st subject grade of each term
//				$x .= '<td class="transcript_text_bold" style="width:35px">&nbsp;</td>'."\n";
//				
//				# 1. Main subject columns
//				# 2. Subject name and subject grade of each term
//				foreach ($this->SubjectCodeID as $SubjectName => $SubjectCode)
//				{
//					$x .= '<td class="transcript_text_bold" style="width:50px;text-align:right;">'.$MainSubjectInfoArr[$SubjectCode]['Name'].'</td>'."\n";
//				}
//				$x .= '<td class="transcript_text_bold" style="width:50px;text-align:right;">'.$eReportCard['ReportsArr']['TranscriptArr']['Total'].'</td>'."\n";
//				//$x .= '<td class="transcript_text_bold" style="width:50px;text-align:right;">'.$eReportCard['ReportsArr']['TranscriptArr']['Grade'].'</td>'."\n";
//				$x .= '<td class="transcript_text_bold" style="width:50px;text-align:right;">&nbsp;</td>'."\n";
//				
//				# Last subject grade of each term
//				$x .= '<td class="transcript_text_bold" style="width:50px;">&nbsp;</td>'."\n";
//				$x .= $this->Get_Transcript_Right_Empty_Cell();
//			$x .= '</tr>'."\n";
//			
//			
//			### Main Subject Marks Display for each terms
//			for ($i=0; $i<$numOfSemester; $i++)
//			{
//				$thisSemesterID = $SemesterArr[$i];
//				$thisSemesterTitle = $SemesterInfoMappingArr[$thisSemesterID];
//				$thisReportColumnID = $ReportColumnID_SemesterMapping[$thisSemesterID];
//				
//				$x .= '<tr>'."\n";
//					$x .= '<td>&nbsp;</td>'."\n";
//					$x .= '<td>&nbsp;</td>'."\n";
//					$x .= '<td>&nbsp;</td>'."\n";
//					$x .= '<td class="transcript_text_bold">'.$thisSemesterTitle.'</td>'."\n";
//					
//					# Display Marks of each main subject
//					$thisMainSubjectMarkArr = array();
//					$thisMainSubjectWeightArr = array();
//					$thisTotal = 0;
//					foreach ($this->SubjectCodeID as $SubjectName => $SubjectCode)
//					{
//						$thisSubjectID = $MainSubjectInfoArr[$SubjectCode]['SubjectID'];
//						$thisMark = $MarkArr[$thisSubjectID][$thisReportColumnID]['Mark'];
//						
//						if ($thisReportColumnID=='' || $thisMark=='')
//						{
//							$thisMarkDisplay = '---';
//						}
//						else
//						{
//							if (!isset($SubjectWeightArr[$thisSubjectID]))
//							{
//								$thisWeightAry = $thisLibRC->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = '$thisSubjectID' and ReportColumnID Is Null");
//								$SubjectWeightArr[$thisSubjectID] = $thisWeightAry[0]['Weight'];
//							}
//							
//							$thisWeight = $SubjectWeightArr[$thisSubjectID];
//							$thisMarkDisplay = $thisMark;
//							
//							if ($thisMark !== '' && is_numeric($thisMark))
//							{
//								$thisMainSubjectMarkArr[] = $thisMark * $thisWeight;
//								$thisMainSubjectWeightArr[] = $thisWeight;
//							}
//						}
//						
//						$x .= '<td class="transcript_text" style="width:50px;text-align:right;">'.$thisMarkDisplay.'</td>'."\n";
//					}
//					
//					
//					# Total
//					if ($thisReportColumnID=='' || count($thisMainSubjectMarkArr)==0)
//					{
//						$thisTotalDisplay = '---';
//					}
//					else
//					{
//						### Get the GrandAverage From DB if there are any, if not, on-the-fly calculation
//						$thisTermReportInfoArr = $thisLibRC->returnReportTemplateBasicInfo('', $others="ClassLevelID = '$ClassLevelID' And Semester = '$thisSemesterID'");
//						$thisTermReportID = $thisTermReportInfoArr['ReportID'];
//						$thisMiscData = $thisLibRC->getMiscTableData($thisTermReportID, $StudentID, 0);
//						$thisTotalFromDB = $thisMiscData['Percentage'];
//						
//						if ($thisTotalFromDB !== '' && is_numeric($thisTotalFromDB))
//						{
//							$thisTotalDisplay = $thisTotalFromDB;
//						}
//						else
//						{
//							$thisTotalWeight = array_sum($thisMainSubjectWeightArr);
//							if ($thisTotalWeight > 0)
//								$thisTotalDisplay = my_round(array_sum($thisMainSubjectMarkArr) / $thisTotalWeight, 1);
//							else
//								$thisTotalDisplay = 0;
//								
//							
//						}
//						
//					}
//					$x .= '<td class="transcript_text" style="width:50px;text-align:right;">'.$thisTotalDisplay.'</td>'."\n";
//					
//					# Grade
//					$x .= '<td class="transcript_text" style="width:50px;text-align:right;">&nbsp;</td>'."\n";
//				$x .= '</tr>'."\n";
//			}
//			
//			# Overall Row
//			$thisReportColumnID = 0;
//			
//			$x .= '<tr>'."\n";
//				$x .= '<td>&nbsp;</td>'."\n";
//				$x .= '<td>&nbsp;</td>'."\n";
//				$x .= '<td>&nbsp;</td>'."\n";
//				$x .= '<td class="transcript_text_bold">'.$eReportCard['ReportsArr']['TranscriptArr']['Overall'].'</td>'."\n";
//				
//				# Display Marks of each main subject
//				$thisMainSubjectMarkArr = array();
//				$thisMainSubjectWeightArr = array();
//				$thisTotal = 0;
//				foreach ($this->SubjectCodeID as $SubjectName => $SubjectCode)
//				{
//					$thisSubjectID = $MainSubjectInfoArr[$SubjectCode]['SubjectID'];
//					$thisMark = $MarkArr[$thisSubjectID][$thisReportColumnID]['Mark'];
//					
//					if (in_array($thisSubjectID, (array)$eRCCalculationSetting['ExcludeSubjectInGrandMark'][$AcademicYear]['SubjectID']) && in_array($ClassLevelName, (array)$eRCCalculationSetting['ExcludeSubjectInGrandMark'][$AcademicYear]['ClassLevelName']))
//					{
//						$thisMarkDisplay = '---';
//					}
//					else
//					{
//						if (!isset($SubjectWeightArr[$thisSubjectID]))
//						{
//							$thisWeightAry = $thisLibRC->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = '$thisSubjectID' and ReportColumnID Is Null");
//							$SubjectWeightArr[$thisSubjectID] = $thisWeightAry[0]['Weight'];
//						}
//						
//						$thisWeight = $SubjectWeightArr[$thisSubjectID];
//						$thisMarkDisplay = ($thisMark=='')? '---' : $thisMark;
//						
//						if ($thisMark !== '' && is_numeric($thisMark))
//						{
//							$thisMainSubjectMarkArr[] = $thisMark * $thisWeight;
//							$thisMainSubjectWeightArr[] = $thisWeight;
//						}
//					}
//					$x .= '<td class="transcript_text" style="width:50px;text-align:right;">'.$thisMarkDisplay.'</td>'."\n";
//				}
//				
//				# Total
//				if (count($thisMainSubjectMarkArr)==0)
//				{
//					$thisTotalDisplay = '---';
//				}
//				else
//				{
//					### Get the GrandAverage From DB if there are any, if not, on-the-fly calculation
//					$thisMiscData = $thisLibRC->getMiscTableData($ReportID, $StudentID, $thisReportColumnID);
//					$thisTotalFromDB = $thisMiscData['Percentage'];
//			
//					if ($thisTotalFromDB !== '' && is_numeric($thisTotalFromDB))
//					{
//						$thisTotalDisplay = $thisTotalFromDB;
//					}
//					else
//					{
//						$thisTotalWeight = array_sum($thisMainSubjectWeightArr);
//						if ($thisTotalWeight > 0)
//							$thisTotalDisplay = my_round(array_sum($thisMainSubjectMarkArr) / $thisTotalWeight, 1);
//						else
//							$thisTotalDisplay = 0;
//					}
//				}
//				//$thisTotal = (count($thisMainSubjectMarkArr)==0)? '---' : $this->getAverage($thisMainSubjectMarkArr, $decimalPlace=0);
//				$x .= '<td class="transcript_text" style="width:50px;text-align:right;">'.$thisTotalDisplay.'</td>'."\n";
//				
//				# Grade
//				$x .= '<td class="transcript_text" style="width:50px;text-align:right;">&nbsp;</td>'."\n";
//				
//				$x .= '<td>&nbsp;</td>'."\n";
//				$x .= '<td>&nbsp;</td>'."\n";
//			$x .= '</tr>'."\n";
//			
//			### Term Subject Info
//			for ($i=0; $i<$numOfSemester; $i++)
//			{
//				$thisSemesterID = $SemesterArr[$i];
//				$thisSemesterTitle = $SemesterInfoMappingArr[$thisSemesterID];
//				$thisReportColumnID = $ReportColumnID_SemesterMapping[$thisSemesterID];
//				
//				### Misc Data
//				$thisReportInfoArr = $thisLibRC->returnReportTemplateBasicInfo('', $others=" ClassLevelID = '$ClassLevelID' And Semester = '$thisSemesterID' ");
//				$thisReportID = $thisReportInfoArr['ReportID'];
//				
//				// If no term report of the term, use the consolidate report comment
//				if ($thisReportID == '')
//					$thisReportID = $ReportID;
//				$MiscData = $thisLibRC->getMiscTableData($thisReportID, $StudentID);
//
//				# Separator
//				$x .= '<tr>'."\n";
//					$x .= '<td>&nbsp;</td>'."\n";
//					$x .= '<td>&nbsp;</td>'."\n";
//					$x .= '<td colspan="9">'.$this->Get_Separator($Height=2).'</td>'."\n";
//				$x .= '</tr>'."\n";
//								
//								
//				### Term Marks Display
//				$NonMainSubjectInfoArr['-1'] = $eReportCard['ReportsArr']['TranscriptArr']['Attendance'];
//				$NonMainSubjectInfoArr['-2'] = $eReportCard['ReportsArr']['TranscriptArr']['Conduct'];
//				$TotalDisplayItem = $numOfNonMainSubject + 2;	// Attendance and Conduct
//				$TotalDisplayRow = ceil($TotalDisplayItem / 4) + 1;	// 4 items per row + 1 remarks row
//				$RowNumber = 0;
//				$ItemCounter = 0;
//				foreach ($NonMainSubjectInfoArr as $thisSubjectID => $thisSubjectName)
//				{
//					$ItemCounter++;
//					
//					// start of a row
//					if ($ItemCounter % 4 == 1)
//					{
//						$RowNumber++;
//						$x .= '<tr>'."\n";
//						if ($RowNumber == 1)
//						{
//							$x .= '<td rowspan="'.$TotalDisplayRow.'" class="transcript_semester_text" style="vertical-align:top">'.$thisSemesterTitle.': </td>'."\n";
//							$x .= '<td rowspan="'.$TotalDisplayRow.'" >&nbsp;</td>'."\n";
//						}
//					}
//					
//					
//					if ($thisSubjectID > 0 )
//					{
//						# Subject Name
//						$thisSubjectName = $thisLibRC->GET_SUBJECT_NAME($thisSubjectID, $ShortName=0, $Bilingual=0, $AbbrName=1);
//						
//						# Mark
//						$thisScaleDisplay = $GradingSchemArr[$thisSubjectID]['scaleDisplay'];
//						$thisMark = ($thisScaleDisplay=='M')? $MarkArr[$thisSubjectID][$thisReportColumnID]['Mark'] : $MarkArr[$thisSubjectID][$thisReportColumnID]['Grade'];
//					}
//					else if ($thisSubjectID == '-1')
//					{
//						$thisSubjectName = $eReportCard['ReportsArr']['TranscriptArr']['Attendance'];
//						$thisMark = $MiscData['Attendance'];
//					}
//					else if ($thisSubjectID == '-2')
//					{
//						$thisSubjectName = $eReportCard['ReportsArr']['TranscriptArr']['Conduct'];
//						$thisMark = $MiscData['Conduct'];
//					}
//					
//					$x .= '<td class="transcript_text_bold">'.$thisSubjectName.'</td>'."\n";
//					$x .= '<td class="transcript_text" style="text-align:center">'.$thisMark.'</td>'."\n";
//					
//					// end of a row
//					if ($ItemCounter % 4 == 0 || $ItemCounter == $TotalDisplayItem)	
//						$x .= '</tr>'."\n";
//				}
//				
//				### Term Remarks
//				$Remarks = $MiscData['TComment'];
//				
//				$x .= '<tr style="vertical-align:top;">'."\n";
//					$x .= '<td class="transcript_text_bold">'.$eReportCard['ReportsArr']['TranscriptArr']['Remarks'].':</td>'."\n";
//					$x .= '<td colspan="8" class="transcript_text" style="height:40px;">'.$Remarks.'</td>'."\n";
//				$x .= '</tr>'."\n";
//				
//			}
//		$x .= '</table>'."\n";
//		
//		return $x;
//	}
	
	function Get_Transcript_Year_Content($StudentID, $AcademicYear, $ClassName)
	{
		global $eReportCard, $eRCCalculationSetting;
		
		$HideMarkSpecialCaseArr = array('-', 'abs', '/', 'N.A.');
		
		### Create library of that academic year
		$thisLibRC = new libreportcardSIS($AcademicYear);
		
		### Get Student Class Info of that year
		$ClassInfoArr = $thisLibRC->Get_FormInfo_By_ClassName($ClassName);
		$ClassLevelID = $ClassInfoArr[0]['ClassLevelID'];
		$ClassLevelName = $thisLibRC->returnClassLevel($ClassLevelID);
		$SchoolLevelName = substr($ClassLevelName, 0, 1);
		
		### Get Grading Scheme Info
		$GradingSchemArr = $thisLibRC->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		
		### Get Main Subject Info
		// $SubjectInfoArr[$SubjectID] = $SubjectName
		$SubjectInfoArr = $thisLibRC->returnSubjectwOrderNoL($ClassLevelID);
		$SubjectIDMappingArr = $thisLibRC->GET_SUBJECTS_CODEID_MAP($CodeAsKey=0);
		$SubjectCodeMappingArr = $thisLibRC->GET_SUBJECTS_CODEID_MAP($CodeAsKey=1);
		
		
		$MainSubjectInfoArr = array();
		if (count($SubjectInfoArr)==0 || $SchoolLevelName != 'S')
		{
			// Hardcode Eng, Chi, Maths and Science if there are no record
			foreach ((array)$this->SubjectCodeID as $thisSubjectName => $thisSubjectCode)
			{
				$thisSubjectID = $SubjectCodeMappingArr[$thisSubjectCode];
				$MainSubjectInfoArr[$thisSubjectID] = $thisLibRC->GET_SUBJECT_NAME($thisSubjectID, $ShortName=0, $Bilingual=0, $AbbrName=1);
			}
		}
		else
		{
			// Get all Mark input Subject
			foreach ((array)$SubjectInfoArr as $thisSubjectID => $thisSubjectName)
			{
				$thisScaleDisplay = $GradingSchemArr[$thisSubjectID]['scaleDisplay'];
				
				# Get Mark Input Subject Only
				if ($thisScaleDisplay != 'M')
					continue;
					
				# Skip Component Subject
				if ($thisLibRC->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID) == true)
					continue;
				
				$MainSubjectInfoArr[$thisSubjectID] = $thisLibRC->GET_SUBJECT_NAME($thisSubjectID, $ShortName=0, $Bilingual=0, $AbbrName=1);
			}
		}
		$MainSubjectIDArr = array_keys($MainSubjectInfoArr);
		
		
		### Get Non Main Subject List
		// filter component subject
		$NonMainSubjectInfoArr = array();
		foreach ($SubjectInfoArr as $thisSubjectID => $thisSubjectFullName)
		{
			if ($thisLibRC->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID) || in_array($thisSubjectID, $MainSubjectIDArr))
				continue;
				
			$NonMainSubjectInfoArr[$thisSubjectID] = $thisLibRC->GET_SUBJECT_NAME($thisSubjectID, $ShortName=0, $Bilingual=0, $AbbrName=1);
		}
		$numOfNonMainSubject = count($NonMainSubjectInfoArr);
		
		
		### Get all Semester Info
		$SemesterInfoMappingArr = $thisLibRC->GET_ALL_SEMESTERS($SemID='-1', $haveReportTemplate=false, $SemesterShortName=1);
		
		$SemesterArr = array();
		$PSLevel = $thisLibRC->Get_PSClassLevel($ClassLevelID);
		if ($PSLevel=='P' || $PSLevel=='PY')
		{
			$SemesterArr = array(1, 3);
			$GradeSubjectInfoSemesterArr = array(1, 3);
		}
		else if ($PSLevel=='S')
		{
			
			$SemesterArr = array(0, 1, 2, 3);
			$GradeSubjectInfoSemesterArr = array(1, 3);
			
			$ConsolidatedReportInfoArr = $thisLibRC->Get_Form_Report_List($ClassLevelID);
		}
		$numOfSemester = count($SemesterArr);
		$numOfGradeSubjectInfoSemester = count($GradeSubjectInfoSemesterArr);
			
			
		### Get all Report Info
		$ConsolidateReportInfo = $thisLibRC->Get_Form_Last_Consolidated_Report($ClassLevelID);
		$ReportID = $ConsolidateReportInfo[0]['ReportID'];
		$ReportColumnArr = $thisLibRC->returnReportTemplateColumnData($ReportID);
		$ReportColumnID_SemesterMapping = array();
		$numOfColumn = count($ReportColumnArr);
		for ($i=0; $i<$numOfColumn; $i++)
		{
			$thisReportColumnID = $ReportColumnArr[$i]['ReportColumnID'];
			$thisSemesterID = $ReportColumnArr[$i]['SemesterNum'];
			
			$ReportColumnID_SemesterMapping[$thisSemesterID] = $thisReportColumnID;
		}
		
		
		### Get Marks
		//$MarkArr[$SubjectID][$ReportColumnID][field] = value;
		$MarkArr = $thisLibRC->getMarks($ReportID, $StudentID);
		
		
		$numOfColumn = 11;
		$x = '';
		$x .= $this->Get_Separator();
		$x .= '<table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr valign="top">'."\n";
				$x .= $this->Get_Transcript_Left_Empty_Cell($numOfColumn);
				# School Year & Class Name
				$x .= '<td rowspan="'.$numOfColumn.'" class="transcript_text_bold" style="width:120px">'."\n";
					$x .= '<table width="100%" valign="top" border="0" cellpadding="0" cellspacing="0">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="transcript_text_bold" nowrap>'.$eReportCard['ReportsArr']['TranscriptArr']['SchoolYear'].'</td>'."\n";
							$x .= '<td class="transcript_text_bold" style="width:15px"> : </td>'."\n";
							$x .= '<td class="transcript_text" style="width:50px">'.$AcademicYear.'</td>'."\n";
						$x .= '</tr>'."\n";	
						$x .= '<tr>'."\n";
							$x .= '<td class="transcript_text_bold">'.$eReportCard['ReportsArr']['TranscriptArr']['ClassName'].'</td>'."\n";
							$x .= '<td class="transcript_text_bold"> : </td>'."\n";
							$x .= '<td class="transcript_text">'.$ClassName.'</td>'."\n";
						$x .= '</tr>'."\n";	
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";	
				
				# Empty Column
				$x .= '<td rowspan="'.$numOfColumn.'" style="width:10px">&nbsp;</td>'."\n";
				
				# Semester Title
				$x .= '<td class="transcript_semester_text" style="width:30px">&nbsp;</td>'."\n";
				
				# Empty Column
				$x .= '<td style="width:10px">&nbsp;</td>'."\n";
				
				# 1st subject name of each term
				$x .= '<td class="transcript_text_bold" style="width:50px">&nbsp;</td>'."\n";
				
				
				### Mark Displayed Subject Mark Table
				$x .= '<td colspan="7" class="transcript_text_bold">'."\n";
					$x .= '<table width="100%" align="center" valign="top" border="0" cellpadding="2" cellspacing="0">'."\n";
						$x .= '<tr>'."\n";
							// Term Title
							$x .= '<td style="width:35px">&nbsp;</td>'."\n";
							
							// Mark input Subject Name
							foreach ($MainSubjectInfoArr as $thisSubjectID => $thisSubjectName)
							{
								$x .= '<td class="transcript_text_bold" style="width:60px;text-align:right;">'.$thisSubjectName.'</td>'."\n";
							}
							$x .= '<td class="transcript_text_bold" style="width:50px;text-align:right;">'.$eReportCard['ReportsArr']['TranscriptArr']['Total'].'</td>'."\n";
						$x .= '</tr>'."\n";
						
						### Main Subject Marks Display for each terms
						for ($i=0; $i<$numOfSemester; $i++)
						{
							$thisSemesterID = $SemesterArr[$i];
							$thisSemesterTitle = $SemesterInfoMappingArr[$thisSemesterID];
							$thisReportColumnID = $ReportColumnID_SemesterMapping[$thisSemesterID];
							
							$x .= '<tr>'."\n";
								$x .= '<td class="transcript_text_bold">'.$thisSemesterTitle.'</td>'."\n";
								
								# Display Marks of each main subject
								$thisMainSubjectMarkArr = array();
								$thisMainSubjectWeightArr = array();
								$thisTotal = 0;
								foreach ((array)$MainSubjectInfoArr as $thisSubjectID => $thisSubjectName)
								{
									$thisMark = $MarkArr[$thisSubjectID][$thisReportColumnID]['Mark'];
									$thisGrade = $MarkArr[$thisSubjectID][$thisReportColumnID]['Grade'];
									
									if ($thisReportColumnID=='' || $thisMark=='' || in_array($thisGrade, $HideMarkSpecialCaseArr))
									{
										$thisMarkDisplay = '---';
									}
									else
									{
										if (!isset($SubjectWeightArr[$thisSubjectID]))
										{
											$thisWeightAry = $thisLibRC->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = '$thisSubjectID' and ReportColumnID Is Null");
											$SubjectWeightArr[$thisSubjectID] = $thisWeightAry[0]['Weight'];
										}
										
										$thisWeight = $SubjectWeightArr[$thisSubjectID];
										$thisMarkDisplay = $thisMark;
										
										if ($thisMark !== '' && is_numeric($thisMark))
										{
											$thisMainSubjectMarkArr[] = $thisMark * $thisWeight;
											$thisMainSubjectWeightArr[] = $thisWeight;
										}
									}
									
									$x .= '<td class="transcript_text" style="width:50px;text-align:right;">'.$thisMarkDisplay.'</td>'."\n";
								}
								
								
								# Total
								if ($thisReportColumnID=='' || count($thisMainSubjectMarkArr)==0)
								{
									$thisTotalDisplay = '---';
								}
								else
								{
									### Get the GrandAverage From DB if there are any, if not, on-the-fly calculation
									$thisTermReportInfoArr = $thisLibRC->returnReportTemplateBasicInfo('', $others="ClassLevelID = '$ClassLevelID' And Semester = '$thisSemesterID'");
									$thisTermReportID = $thisTermReportInfoArr['ReportID'];
									$thisMiscData = $thisLibRC->getMiscTableData($thisTermReportID, $StudentID, 0);
									$thisTotalFromDB = $thisMiscData['Percentage'];
									
									if ($thisTotalFromDB !== '' && is_numeric($thisTotalFromDB))
									{
										$thisTotalDisplay = $thisTotalFromDB;
									}
									else
									{
										$thisTotalWeight = array_sum($thisMainSubjectWeightArr);
										if ($thisTotalWeight > 0)
											$thisTotalDisplay = my_round(array_sum($thisMainSubjectMarkArr) / $thisTotalWeight, 1);
										else
											$thisTotalDisplay = 0;
											
										
									}
									
								}
								$x .= '<td class="transcript_text" style="width:50px;text-align:right;">'.$thisTotalDisplay.'</td>'."\n";
								
							$x .= '</tr>'."\n";
						}
						
						### Overall Row
						$thisReportColumnID = 0;
						
						$x .= '<tr>'."\n";
							$x .= '<td class="transcript_text_bold">'.$eReportCard['ReportsArr']['TranscriptArr']['Overall'].'</td>'."\n";
							
							# Display Marks of each main subject
							$thisMainSubjectMarkArr = array();
							$thisMainSubjectWeightArr = array();
							$thisTotal = 0;
							foreach ((array)$MainSubjectInfoArr as $thisSubjectID => $thisSubjectName)
							{
								$thisMark = $MarkArr[$thisSubjectID][$thisReportColumnID]['Mark'];
								$thisGrade = $MarkArr[$thisSubjectID][$thisReportColumnID]['Grade'];
								
								# Grade is a Special Case or 2008 non-examed Subject => Display '---'
								if (in_array($thisGrade, $HideMarkSpecialCaseArr) || in_array($thisSubjectID, (array)$eRCCalculationSetting['ExcludeSubjectInGrandMark'][$AcademicYear]['SubjectID']) && in_array($ClassLevelName, (array)$eRCCalculationSetting['ExcludeSubjectInGrandMark'][$AcademicYear]['ClassLevelName']))
								{
									$thisMarkDisplay = '---';
								}
								else
								{
									if (!isset($SubjectWeightArr[$thisSubjectID]))
									{
										$thisWeightAry = $thisLibRC->returnReportTemplateSubjectWeightData($ReportID, "SubjectID = '$thisSubjectID' and ReportColumnID Is Null");
										$SubjectWeightArr[$thisSubjectID] = $thisWeightAry[0]['Weight'];
									}
									
									$thisWeight = $SubjectWeightArr[$thisSubjectID];
									$thisMarkDisplay = ($thisMark=='')? '---' : $thisMark;
									
									if ($thisMark !== '' && is_numeric($thisMark))
									{
										$thisMainSubjectMarkArr[] = $thisMark * $thisWeight;
										$thisMainSubjectWeightArr[] = $thisWeight;
									}
								}
								$x .= '<td class="transcript_text" style="width:50px;text-align:right;">'.$thisMarkDisplay.'</td>'."\n";
							}
							
							# Total
							if (count($thisMainSubjectMarkArr)==0)
							{
								$thisTotalDisplay = '---';
							}
							else
							{
								### Get the GrandAverage From DB if there are any, if not, on-the-fly calculation
								$thisMiscData = $thisLibRC->getMiscTableData($ReportID, $StudentID, $thisReportColumnID);
								$thisTotalFromDB = $thisMiscData['Percentage'];
						
								if ($thisTotalFromDB !== '' && is_numeric($thisTotalFromDB))
								{
									$thisTotalDisplay = $thisTotalFromDB;
								}
								else
								{
									$thisTotalWeight = array_sum($thisMainSubjectWeightArr);
									if ($thisTotalWeight > 0)
										$thisTotalDisplay = my_round(array_sum($thisMainSubjectMarkArr) / $thisTotalWeight, 1);
									else
										$thisTotalDisplay = 0;
								}
							}
							$x .= '<td class="transcript_text" style="width:50px;text-align:right;">'.$thisTotalDisplay.'</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";				
				$x .= $this->Get_Transcript_Right_Empty_Cell();
			$x .= '</tr>'."\n";
			
			
			
			### Term Subject Info
			for ($i=0; $i<$numOfGradeSubjectInfoSemester; $i++)
			{
				$thisSemesterID = $GradeSubjectInfoSemesterArr[$i];
				$thisSemesterTitle = $SemesterInfoMappingArr[$thisSemesterID];
				$thisReportColumnID = $ReportColumnID_SemesterMapping[$thisSemesterID];
				
				### Misc Data
				if ($PSLevel=='P' || $PSLevel=='PY')
				{
					$thisReportInfoArr = $thisLibRC->returnReportTemplateBasicInfo('', $others=" ClassLevelID = '$ClassLevelID' And Semester = '$thisSemesterID' ");
					$thisReportID = $thisReportInfoArr['ReportID'];
				}
				else if ($PSLevel=='S')
				{
					$thisReportID = $ConsolidatedReportInfoArr[$i]['ReportID'];
				}
				
				
				// If no term report of the term, use the consolidate report comment
				if ($thisReportID == '')
					$thisReportID = $ReportID;
				$MiscData = $thisLibRC->getMiscTableData($thisReportID, $StudentID);

				# Separator
				$x .= '<tr>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td colspan="8">'.$this->Get_Separator($Height=2).'</td>'."\n";
				$x .= '</tr>'."\n";
								
								
				### Term Marks Display
				$NonMainSubjectInfoArr['-1'] = $eReportCard['ReportsArr']['TranscriptArr']['Attendance'];
				$NonMainSubjectInfoArr['-2'] = $eReportCard['ReportsArr']['TranscriptArr']['Conduct'];
				$TotalDisplayItem = $numOfNonMainSubject + 2;	// Attendance and Conduct
				$TotalDisplayRow = ceil($TotalDisplayItem / 4) + 1;	// 4 items per row + 1 remarks row
				$RowNumber = 0;
				$ItemCounter = 0;
				foreach ($NonMainSubjectInfoArr as $thisSubjectID => $thisSubjectName)
				{
					$ItemCounter++;
					
					// start of a row
					if ($ItemCounter % 4 == 1)
					{
						$RowNumber++;
						$x .= '<tr>'."\n";
						if ($RowNumber == 1)
						{
							$x .= '<td rowspan="'.$TotalDisplayRow.'" class="transcript_semester_text" style="vertical-align:top">'.$thisSemesterTitle.': </td>'."\n";
							$x .= '<td rowspan="'.$TotalDisplayRow.'" >&nbsp;</td>'."\n";
						}
					}
					
					
					if ($thisSubjectID > 0 )
					{
						# Subject Name
						$thisSubjectName = $thisLibRC->GET_SUBJECT_NAME($thisSubjectID, $ShortName=0, $Bilingual=0, $AbbrName=1);
						
						# Mark
						$thisScaleDisplay = $GradingSchemArr[$thisSubjectID]['scaleDisplay'];
						$thisMark = ($thisScaleDisplay=='M')? $MarkArr[$thisSubjectID][$thisReportColumnID]['Mark'] : $MarkArr[$thisSubjectID][$thisReportColumnID]['Grade'];
					}
					else if ($thisSubjectID == '-1')
					{
						$thisSubjectName = $eReportCard['ReportsArr']['TranscriptArr']['Attendance'];
						$thisMark = $MiscData['Attendance'];
					}
					else if ($thisSubjectID == '-2')
					{
						$thisSubjectName = $eReportCard['ReportsArr']['TranscriptArr']['Conduct'];
						$thisMark = $MiscData['Conduct'];
					}
					
					$thisWidth = ($ItemCounter % 4 == 1)? 50 : 50;
					$x .= '<td class="transcript_text_bold" style="width:'.$thisWidth.'px;">'.$thisSubjectName.'</td>'."\n";
					$x .= '<td class="transcript_text" style="text-align:center;width:50px;">'.$thisMark.'</td>'."\n";
					
					// end of a row
					if ($ItemCounter % 4 == 0 || $ItemCounter == $TotalDisplayItem)	
						$x .= '</tr>'."\n";
				}
				
				### Term Remarks
				$Remarks = $MiscData['TComment'];
				
				$x .= '<tr style="vertical-align:top;">'."\n";
					$x .= '<td class="transcript_text_bold">'.$eReportCard['ReportsArr']['TranscriptArr']['Remarks'].':</td>'."\n";
					$x .= '<td colspan="7" class="transcript_text" style="height:40px;">'.$Remarks.'</td>'."\n";
				$x .= '</tr>'."\n";
				
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Separator($Height=4, $Color='')
	{
		global $image_path;
		
		if ($Color=='')
			$Color = '#000000';
		
		$x = '';
		$x .= '<table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr><td style="border-bottom:'.$Height.'px solid '.$Color.';"><img src="'.$image_path.'/2007a/10x10.gif" height="'.$Height.'px" /></td></tr>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Get_Empty_Space($Space)
	{
		global $image_path;
		$x = '';
		$x .= '<table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr><td><img src="'.$image_path.'/2007a/10x10.gif" height="'.$Space.'px" /></td></tr>'."\n";
		$x .= '</table>'."\n";
		return $x;
	}
	
	function Get_Transcript_Left_Empty_Cell($Rowspan='')
	{
		if ($Rowspan != '')
			$rowspan = 'rowspan="'.$Rowspan.'"';
		return '<td '.$rowspan.' width="15px">&nbsp;</td>'."\n";
	}
	
	function Get_Transcript_Right_Empty_Cell()
	{
		return '<td width="30px">&nbsp;</td>'."\n";
	}
}	
?>
