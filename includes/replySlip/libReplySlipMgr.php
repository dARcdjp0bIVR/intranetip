<?php
// editing by 
/*
 * 2013-09-18 (Carlos): added updateShowUserInfoInResult()
 * 2013-02-15 (Carlos): added exportReplySlipStatisticsCSV() and returnGetExportCsvButton()
 * 						added export csv button in returnReplySlipStatisticsHtml()
 */
if (!defined("LIBREPLYSLIPMGR_DEFINED")) {
	define("LIBREPLYSLIPMGR_DEFINED", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	include_once($PATH_WRT_ROOT.'includes/libinterface.php');
	include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
	include_once($PATH_WRT_ROOT.'includes/replySlip/replySlipConfig.inc.php');
	include_once($PATH_WRT_ROOT.'includes/replySlip/libReplySlip.php');
	include_once($PATH_WRT_ROOT.'includes/replySlip/libReplySlipQuestion.php');
	include_once($PATH_WRT_ROOT.'includes/replySlip/libReplySlipQuestionOption.php');
	include_once($PATH_WRT_ROOT."includes/table/table_commonTable.php");
	
	class libreplySlipMgr {
		
		private $libdb;
		private $linterface;
		private $limporttext;
		private $csvFilePath;
		private $csvDataAry;
		private $csvErrorAry;
		private $curUserId;
		private $replySlipId;
		private $linkToModule;
		private $linkToType;
		private $linkToId;
		private $showQuestionNum;
		private $ansAllQuestion;
		private $showUserInfoInResult;
		private $recordType;
		private $recordStatus;
		
		public function __construct() {
			$this->libdb = new libdb();
			$this->linterface = new interface_html();
			$this->csvDataAry = array();
			$this->resetCsvErrorAry();
		}
		
		private function getInstanceLibImportText() {
			if ($this->limporttext === null) {
				$this->limporttext = new libimporttext();
			}
			
			return $this->limporttext;
		}
		
		public function setCsvFilePath($val) {
			$this->csvFilePath = $val;
			$this->loadCsvFileData();
		}
		public function getCsvFilePath() {
			return $this->csvFilePath;
		}
		
		public function setCsvDataAry($val) {
			$this->csvDataAry = $val;
		}
		public function getCsvDataAry() {
			return $this->csvDataAry;
		}
		
		public function setCurUserId($val) {
			$this->curUserId = $val;
		}
		public function getCurUserId() {
			return $this->curUserId;
		}
		
		public function setReplySlipId($val) {
			$this->replySlipId = $val;
		}
		public function getReplySlipId() {
			return $this->replySlipId;
		}
		
		public function setLinkToModule($val) {
			$this->linkToModule = $val;
		}
		public function getLinkToModule() {
			return $this->linkToModule;
		}
		
		public function setLinkToType($val) {
			$this->linkToType = $val;
		}
		public function getLinkToType() {
			return $this->linkToType;
		}
		
		public function setLinkToId($val) {
			$this->linkToId = $val;
		}
		public function getLinkToId() {
			return $this->linkToId;
		}
		
		public function setShowQuestionNum($val) {
			$this->showQuestionNum = $val;
		}
		public function getShowQuestionNum() {
			global $replySlipConfig;
			
			if ($this->showQuestionNum == '') {
				return $replySlipConfig['INTRANET_REPLY_SLIP']['ShowQuestionNum']['hide'];
			}
			else {
				return $this->showQuestionNum;
			}
		}
		
		public function setAnsAllQuestion($val) {
			$this->ansAllQuestion = $val;
		}
		public function getAnsAllQuestion() {
			global $replySlipConfig;
			
			if ($this->ansAllQuestion == '') {
				return $replySlipConfig['INTRANET_REPLY_SLIP']['AnsAllQuestion']['no'];
			}
			else {
				return $this->ansAllQuestion;
			}
		}
		
		public function setShowUserInfoInResult($val) {
			$this->showUserInfoInResult = $val;
		}
		public function getShowUserInfoInResult() {
			global $replySlipConfig;
			
			if ($this->showUserInfoInResult == '') {
				return $replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['hide'];
			}
			else {
				return $this->showUserInfoInResult;
			}
		}
		
		
		public function setRecordType($val) {
			$this->recordType = $val;
		}
		public function getRecordType() {
			global $replySlipConfig;
			
			if ($this->recordType == '') {
				return $replySlipConfig['INTRANET_REPLY_SLIP']['RecordType']['realForm'];
			}
			else {
				return $this->recordType;
			}
		}
		
		public function setRecordStatus($val) {
			$this->recordStatus = $val;
		}
		public function getRecordStatus() {
			global $replySlipConfig;
			
			if ($this->recordStatus == '') {
				return $replySlipConfig['INTRANET_REPLY_SLIP']['RecordStatus']['active'];
			}
			else {
				return $this->recordStatus;
			}
		}
		
		private function resetCsvErrorAry() {
			$this->csvErrorAry = array();
		}
		private function addCsvError($lineNumber, $errorCode) {
			$this->csvErrorAry[$lineNumber][] = $errorCode;
		}
		public function getCsvErrorAry() {
			return $this->csvErrorAry;
		}
		
		public function returnReplySlipHtmlByCsvData() {
			$successAry = array();
			
			if ($this->isCsvFileValid()) {
				$replySlipHtml = $this->returnReplySlipHtml($forCsvPreview=true);
				return $replySlipHtml;
			}
			else {
				return $this->returnCsvWarningTable();
			}
		}
		
		private function loadCsvFileData() {
			global $replySlipConfig;
			
			$libimport = $this->getInstanceLibImportText();
			$filePath = $this->getCsvFilePath();
			
			$this->setCsvDataAry($libimport->GET_IMPORT_TXT($filePath, $incluedEmptyRow=0, $replySlipConfig['importDataLineBreakReplacement']));
		}
		
		public function isCsvFileValid() {
			global $replySlipConfig;
			
			$libimport = $this->getInstanceLibImportText();
			$isValid = true;
			
			$headerAry = $this->getImportHeaderAry();
			$coreHeaderAry = $headerAry['En'];
			$libimport->SET_CORE_HEADER($coreHeaderAry);
			
			$csvDataAry = $this->getCsvDataAry();
			
			
			### Check header
			$isHeaderValid = $libimport->VALIDATE_HEADER($csvDataAry, $ParExactlyCore=false);
			if (!$isHeaderValid) {
				$isValid = false;
				$this->addCsvError(0, 'invalidHeader');
			}
			
			### Check data
			array_shift($csvDataAry);
			array_shift($csvDataAry);
			$numOfData = count($csvDataAry);
			for ($i=0; $i<$numOfData; $i++) {
				$_lineNumber = $i + 2;
				$_questionType = strtoupper(trim($csvDataAry[$i][0]));
				$_questionTitle = trim($csvDataAry[$i][1]);
				
				// check if valid question type
				if ($_questionType == '') {
					$isValid = false;
					$this->addCsvError($_lineNumber, 'missingQuestionType');
				}
				else {
					$_questionTypeDbValue = $this->returnQuestionTypeDbValueByImportCode($_questionType);
					if ($_questionTypeDbValue == '') {
						$isValid = false;
						$this->addCsvError($_lineNumber, 'invalidQuestionType');
					}
				}
				
				// check if title exist
				if ($_questionTitle == '') {
					$isValid = false;
					$this->addCsvError($_lineNumber, 'missingTitle');
				}
				
				// get options
				$_optionAry = $this->getImportOptionAryFromRowData($csvDataAry[$i]);
				$_numOfOption = count($_optionAry);
				
				if ($_questionType=='MC' || $_questionType=='MMC') {
					// MC, MMC must have options
					if ($_numOfOption == 0) {
						$isValid = false;
						$this->addCsvError($_lineNumber, 'missingOptions');
					}
				}
				else if ($_questionType=='MC' || $_questionType=='MMC' || $_questionType=='NA') {
					// SQ, LQ, NA must not have options
					if ($_numOfOption > 0) {
						$isValid = false;
						$this->addCsvError($_lineNumber, 'cannotHaveOption');
					}
				}
			}
			
			return $isValid;
		}
		
		private function returnCsvWarningTable() {
			global $Lang;
			
			$errorAry = $this->getCsvErrorAry();
			$csvDataAry = $this->getCsvDataAry();
			
			$importHeaderAry = $this->getImportHeaderAry();
			$importHeaderAry = Get_Lang_Selection($importHeaderAry['Ch'], $importHeaderAry['En']);
			$numOfHeader = count($importHeaderAry);
			
			$numOfMaxOption = $this->getMaxNumOfOptionInCsvData();
			$numOfEmptyRow = $numOfMaxOption - 1;
			
			
			### Show error result
			$x = '';
			if (isset($errorAry[0])) {
				// Have general error => show error message instead of error in each row
				$numOfError = count($errorAry[0]);
				$errorDisplayAry = array();
				for ($i=0; $i<$numOfError; $i++) {
					$errorDisplayAry[] = $this->returnImportErrorMsgByErrorCode($errorAry[0][$i]);
				}
				$x .= $this->linterface->Get_Warning_Message_Box('<span class="tabletextrequire">'.$Lang['General']['Error'].'</span>', $errorDisplayAry);
			}
			else {
				// Create table
				$tableObj = new table_commonTable();
				$tableObj->setTableType('view');
				$tableObj->setApplyConvertSpecialChars(false);
				
				// table header
				$tableObj->addHeaderTr(new tableTr());
				$tableObj->addHeaderCell(new tableTh($Lang['General']['ImportArr']['Row']));
				for ($i=0; $i<$numOfHeader; $i++) {
					$_headerTitle = $importHeaderAry[$i];
					$tableObj->addHeaderCell(new tableTh($_headerTitle));
				}
				for ($i=0; $i<$numOfEmptyRow; $i++) {
					$tableObj->addHeaderCell(new tableTh('&nbsp;'));
				}
				$tableObj->addHeaderCell(new tableTh($Lang['General']['Error']));
				
				// table body
				foreach ((array)$errorAry as $_rowNum => $_errorCodeAry) {
					$_csvDataAry = $csvDataAry[$_rowNum];
					$_numOfCol = count((array)$_csvDataAry);
					
					$_numOfError = count($_errorCodeAry);
					$_errorDisplayAry = array();
					for ($i=0; $i<$_numOfError; $i++) {
						$_errorDisplayAry[] = '- '.$this->returnImportErrorMsgByErrorCode($_errorCodeAry[$i]);
					}
					$_errorDisplay = implode('<br />', $_errorDisplayAry);
					
					$tableObj->addBodyTr(new tableTr());
					$tableObj->addBodyCell(new tableTd($_rowNum));
					for ($i=0; $i<$_numOfCol; $i++) {
						$__displayContent = $_csvDataAry[$i];
						
						if (in_array('missingQuestionType', $_errorCodeAry) && $i==0) {
							$__displayContent = '<span class="tabletextrequire">###</span>';
						}
						if (in_array('invalidQuestionType', $_errorCodeAry) && $i==0) {
							$__displayContent = '<span class="tabletextrequire">'.$__displayContent.'</span>';
						}
						if (in_array('missingTitle', $_errorCodeAry) && $i==1) {
							$__displayContent = '<span class="tabletextrequire">###</span>';
						}
						if (in_array('missingOptions', $_errorCodeAry) && $i==2) {
							$__displayContent = '<span class="tabletextrequire">###</span>';
						}
						if (in_array('cannotHaveOption', $_errorCodeAry) && $i>=2) {
							$__displayContent = '<span class="tabletextrequire">'.$__displayContent.'</span>';
						}
						
						$tableObj->addBodyCell(new tableTd($__displayContent));
					}
					$tableObj->addBodyCell(new tableTd($_errorDisplay));
				}
				
				$x = $tableObj->returnHtml();
			}
			
			return $x;
		}
		
		private function returnImportErrorMsgByErrorCode($code) {
			global $Lang;
			return $Lang['replySlip']['WarningArr'][$code];
		}
		
		public function saveCsvDataToDb() {
			global $replySlipConfig;
			
			$successAry = array();
			
			$replySlipId = $this->getReplySlipId();
			
			$csvDataAry = $this->getCsvDataAry();
			array_shift($csvDataAry);
			array_shift($csvDataAry);
			
			### Create Reply Slip
			$replySlipObj = new libReplySlip($replySlipId);
			$replySlipObj->setLinkToModule($this->getLinkToModule());
			$replySlipObj->setLinkToType($this->getLinkToType());
			$replySlipObj->setLinkToId($this->getLinkToId());
			$replySlipObj->setShowQuestionNum($this->getShowQuestionNum());
			$replySlipObj->setAnsAllQuestion($this->getAnsAllQuestion());
			$replySlipObj->setShowUserInfoInResult($this->getShowUserInfoInResult());
			$replySlipObj->setRecordType($this->getRecordType());
			$replySlipObj->setRecordStatus($this->getRecordStatus());
			$replySlipObj->setInputBy($this->getCurUserId());
			$replySlipObj->setModifiedBy($this->getCurUserId());
			$replySlipId = $replySlipObj->save();
			$successAry['insertReportSlip'] = ($replySlipId > 0)? true : false;
			
			
			### Create Questions & Options
			$numOfQuestion = count($csvDataAry);
			for ($i=0; $i<$numOfQuestion; $i++) {
				$_questionType = $csvDataAry[$i][0];
				$_questionTitle = $csvDataAry[$i][1];
				$_questionTitle = str_replace($replySlipConfig['importDataLineBreakReplacement'], "\n", $_questionTitle);
				
				$_questionTypeDbValue = $this->returnQuestionTypeDbValueByImportCode($_questionType);
				
				// Create Question
				$_questionObj = new libReplySlipQuestion();
				$_questionObj->setReplySlipId($replySlipId);
				$_questionObj->setRecordType($_questionTypeDbValue);
				$_questionObj->setContent($_questionTitle);
				$_questionObj->setDisplayOrder($i + 1);
				$_questionObj->setInputBy($this->getCurUserId());
				$_questionObj->setModifiedBy($this->getCurUserId());
				$_questionId = $_questionObj->save();
				$successAry[$replySlipId]['insertQuestion'][] = ($_questionId > 0)? true : false;
				
				
				// Create Options -> consolidate all options in $_optionAry first
				$_optionAry = $this->getImportOptionAryFromRowData($csvDataAry[$i]);
				$_numOfOptions = count($_optionAry);
				for ($j=0; $j<$_numOfOptions; $j++) {
					$__content = $_optionAry[$j]['content'];
					$__content = str_replace($replySlipConfig['importDataLineBreakReplacement'], "\n", $__content);
					
					$__optionObj = new libReplySlipQuestionOption();
					$__optionObj->setQuestionId($_questionId);
					$__optionObj->setContent($__content);
					$__optionObj->setDisplayOrder($j + 1);
					$__optionObj->setInputBy($this->getCurUserId());
					$__optionObj->setModifiedBy($this->getCurUserId());
					$__optionId = $__optionObj->save();
					
					$successAry[$replySlipId][$_questionId]['insertOption'][] = ($__optionId > 0)? true : false;
				}
			}
			
			if (in_multi_array(false, $successAry)) {
				return false;
			}
			else {
				$this->setReplySlipId($replySlipId);
				return true;
			}
		}
		
		public function deleteReplySlipData() {
			$replySlipId = $this->getReplySlipId();
			if ($replySlipId == '') {
				return false;
			}
			
			### Get reply slip info
			$replySlipObj = new libReplySlip($replySlipId);
			return $replySlipObj->deleteRecord();
		}
		
		public function returnReplySlipHtml($forCsvPreview=false, $showUserAnswer=false, $disabledAnswer=false, $showSelfAnswerOnly=false) {
			global $replySlipConfig, $Lang;
			
			### Get reply slip info
			$questionAry = array();
			$optionAssoAry = array();	
			if ($forCsvPreview) {
				$replySlipObj = new libReplySlip();
				$replySlipObj->setShowQuestionNum($this->getShowQuestionNum());
				$replySlipObj->setAnsAllQuestion($this->getAnsAllQuestion());
				$replySlipObj->setRecordStatus($replySlipConfig['INTRANET_REPLY_SLIP']['RecordStatus']['importTemp']);
				
				### Create Questions & Options
				$csvDataAry = $this->getCsvDataAry();
				array_shift($csvDataAry);
				array_shift($csvDataAry);
				$numOfQuestion = count($csvDataAry);
				for ($i=0; $i<$numOfQuestion; $i++) {
					$_questionId = $i + 1;
					$_questionType = $csvDataAry[$i][0];
					$_questionTitle = str_replace($replySlipConfig['importDataLineBreakReplacement'], "\n", $csvDataAry[$i][1]);
					
					$_questionTypeDbValue = $this->returnQuestionTypeDbValueByImportCode($_questionType);
					
					// simulate "$questionAry = $this->returnQuestionData($replySlipId);"
					$questionAry[$i]['QuestionID'] = $_questionId;
					$questionAry[$i]['RecordType'] = $_questionTypeDbValue;
					$questionAry[$i]['Content'] = str_replace($replySlipConfig['importDataLineBreakReplacement'], "\n", $_questionTitle);
					
					
					// simulate "$optionAssoAry = BuildMultiKeyAssoc($this->returnOptionData($replySlipId), 'QuestionID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);"
					$_optionAry = $this->getImportOptionAryFromRowData($csvDataAry[$i]);
					$_numOfOptions = count($_optionAry);
					for ($j=0; $j<$_numOfOptions; $j++) {
						$__content = $_optionAry[$j]['content'];
						
						$optionAssoAry[$_questionId][$j]['Content'] = $__content;
					}
				}
			}
			else {
				$replySlipId = $this->getReplySlipId();
				if ($replySlipId == '') {
					return false;
				}
				
				### Get Reply Slip object
				$replySlipObj = new libReplySlip($replySlipId);
				
				### Get reply slip question
				$questionAry = $this->returnQuestionData($replySlipId);
				
				### Get options of each questions
				$optionAssoAry = BuildMultiKeyAssoc($this->returnOptionData($replySlipId), 'QuestionID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			}
			$showQuestionNum = ($replySlipObj->getShowQuestionNum()==$replySlipConfig['INTRANET_REPLY_SLIP']['ShowQuestionNum']['show'])? true : false; 
			$recordStatus = $replySlipObj->getRecordStatus();
			$numOfQuestion = count($questionAry);
			
			if ($disabledAnswer) {
				$disableOption = true;
			}
			else {
				$disableOption = ($recordStatus == $replySlipConfig['INTRANET_REPLY_SLIP']['RecordStatus']['importTemp'])? true : false;
			}
			
			
			$curUserId = $this->getCurUserId();
			if ($showUserAnswer && $curUserId != '') {
				$answerAssoAry = BuildMultiKeyAssoc($this->returnUserAnswer($replySlipId, $curUserId), 'QuestionID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			}
			
			
			### Create table
//			$tableObj = new table_commonTable();
//			$tableObj->setTableType('view');
//			$tableObj->setApplyConvertSpecialChars(false);
			
			$questionHtml = '';
			for ($i=0; $i<$numOfQuestion; $i++) {
				$_questionId = $questionAry[$i]['QuestionID'];
				$_questionType = $questionAry[$i]['RecordType'];
				
				$_questionContent = nl2br(strip_tags($questionAry[$i]['Content'], '<br>'));
				$_questionNum = $i + 1;
				$_questionOptionAry = $optionAssoAry[$_questionId];
				$_numOfOption = count((array)$_questionOptionAry);
				$_answerAry = $answerAssoAry[$_questionId];
				
				### Build the display content
				$_elementName = 'answerAry['.$replySlipId.']['.$_questionId.']';
				
				$_optionHtml = '';
				$_optionHtml .= '<ul>';
					if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['singleMC']) {
						$_selectedOptionIdAry = Get_Array_By_Key($_answerAry, 'OptionID');
						
						$_radioAry = array();
						for ($j=0; $j<$_numOfOption; $j++) {
							$__optionId = $_questionOptionAry[$j]['OptionID'];
							$__optionContent = nl2br(strip_tags($_questionOptionAry[$j]['Content'], '<br>'));
							
							$__isChecked = (in_array($__optionId, $_selectedOptionIdAry))? true : false;
							$__elementId = 'replySlipRadio_'.$replySlipId.'_'.$_questionId.'_'.$__optionId;
							
							if ($showSelfAnswerOnly) {
								if ($__isChecked) {
									$_optionHtml .= '<li>'.$__optionContent.'</li>';
								}
							}
							else {
								$_optionHtml .= '<li>';
									$_optionHtml .= '<em>'.$this->linterface->Get_Radio_Button($__elementId, $_elementName, $__optionId, $__isChecked, $class='replySlipAnswer_'.$replySlipId.'_'.$_questionId, '', $Onclick="", $disableOption).'</em>';
									$_optionHtml .= '<span><label for="'.$__elementId.'">'.$__optionContent.'</label></span>';
								$_optionHtml .= '</li>';
							}
						}
					}
					else if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['multipleMC']) {
						$_selectedOptionIdAry = Get_Array_By_Key($_answerAry, 'OptionID');
						
						$_checkboxAry = array();
						for ($j=0; $j<$_numOfOption; $j++) {
							$__optionId = $_questionOptionAry[$j]['OptionID'];
							$__optionContent = nl2br(strip_tags($_questionOptionAry[$j]['Content'], '<br>'));
							
							$__isChecked = (in_array($__optionId, $_selectedOptionIdAry))? true : false;
							$__elementId = 'replySlipChk_'.$replySlipId.'_'.$_questionId.'_'.$__optionId;
							
							if ($showSelfAnswerOnly) {
								if ($__isChecked) {
									$_optionHtml .= '<li>'.$__optionContent.'</li>';
								}
							}
							else {
								$_optionHtml .= '<li>';
									$_optionHtml .= '<em>'.$this->linterface->Get_Checkbox($__elementId, $_elementName, $__optionId, $__isChecked, $class='replySlipAnswer_'.$replySlipId.'_'.$_questionId, '', $Onclick='', $disableOption).'</em>';
									$_optionHtml .= '<span><label for="'.$__elementId.'">'.$__optionContent.'</label></span>';
								$_optionHtml .= '</li>';	
							}
						}
					}
					else if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['shortQuestion']) {
						$__answeredContent = nl2br(strip_tags($_answerAry[0]['Content'], '<br>'));
						
						$__paramAry = array();
						if ($disableOption) {
							$__paramAry['disabled'] = 'disabled';
						}
						$__elementId = 'replySlipTb_'.$replySlipId.'_'.$_questionId;
						
						if ($showSelfAnswerOnly) {
							$_optionHtml .= ($__answeredContent=='')? $Lang['General']['EmptySymbol'] : $__answeredContent;
						}
						else {
							$_optionHtml .= $this->linterface->GET_TEXTBOX($__elementId, $_elementName, $__answeredContent, $OtherClass='replySlipAnswer_'.$replySlipId.'_'.$_questionId, $__paramAry);
						}
					}
					else if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['longQuestion']) {
						//$__answeredContent = nl2br(strip_tags($_answerAry[0]['Content'], '<br>'));
						$__answeredContent = strip_tags($_answerAry[0]['Content'], '<br>');
						$__elementId = 'replySlipTa_'.$replySlipId.'_'.$_questionId;
						
						if ($showSelfAnswerOnly) {
							$_optionHtml .= ($__answeredContent=='')? $Lang['General']['EmptySymbol'] : nl2br($__answeredContent);
						}
						else {
							$_optionHtml .= $this->linterface->GET_TEXTAREA($_elementName, $__answeredContent, $taCols=70, $taRows=5, $OnFocus="", $disableOption, $other='', $class='replySlipAnswer_'.$replySlipId.'_'.$_questionId, $__elementId, $CommentMaxLength='');
						}
					}
					else if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['na']) {
						// display nth
					}
					
					if ($_questionType != $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['na']) {
						// add "please answer question" warning span
						$_optionHtml .= $this->linterface->Get_Form_Warning_Msg('replySlipAnsQuestionWarningDiv_'.$replySlipId.'_'.$_questionId, $Lang['replySlip']['WarningArr']['mustAnswerQuestion'], $Class='replySlipWarningDiv_'.$replySlipId);
					}
				$_optionHtml .= '</ul>';
				
//				### Add tr and td to the table
//				$tableObj->addBodyTr(new tableTr());
//				
//				if ($showQuestionNum) {
//					$tableObj->addBodyCell(new tableTd($_questionNum.'.'));
//				}
//				$tableObj->addBodyCell(new tableTd($_display));

				$questionHtml .= '<div class="reply_slip_content" style="text-align:left;">'."\r\n";
					$questionHtml .= $_questionContent."\r\n";
					if ($_questionType != $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['na']) {
						$questionHtml .= $_optionHtml."\r\n";
					}
				$questionHtml .= '</div>'."\r\n";
			}
			
			$x = '';
			//$x .= $tableObj->returnHtml();
			$x .= '<div>';
				$x .= $questionHtml;
				$x .= $this->linterface->GET_HIDDEN_INPUT('replySlipQuestionIdListTb_'.$replySlipId, 'replySlipQuestionIdList_'.$replySlipId, implode($replySlipConfig['questionIdHiddenFieldSeparator'], Get_Array_By_Key($questionAry, 'QuestionID')));
				$x .= $this->linterface->GET_HIDDEN_INPUT('replySlipUserIdTb_'.$replySlipId, 'replySlipUserId_'.$replySlipId, $this->getCurUserId());
			$x .= '</div>';
			
			return $x;
		}
		
		public function returnQuestionTypeDbValueByImportCode($importCode) {
			global $replySlipConfig;
			
			$importCode = strtoupper($importCode);
			$internalCode = $replySlipConfig['importReplySlip']['questionTypeCode'][$importCode]; 
			$dbValue = $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType'][$internalCode];
			
			return $dbValue;
		}
		
		private function returnQuestionData($replySlipIdAry) {
			$sql = "Select * From INTRANET_REPLY_SLIP_QUESTION Where ReplySlipID In ('".implode("','", (array)$replySlipIdAry)."') Order By DisplayOrder";
			return $this->libdb->returnResultSet($sql);
		}
		
		private function returnOptionData($replySlipIdAry) {
			$sql = "Select
							o.*
					From
							INTRANET_REPLY_SLIP_QUESTION_OPTION as o
							Inner Join INTRANET_REPLY_SLIP_QUESTION as q On (o.QuestionID = q.QuestionID)
					Where
							q.ReplySlipID In ('".implode("','", (array)$replySlipIdAry)."')
					Order By
							q.DisplayOrder, o.DisplayOrder
					";
			return $this->libdb->returnResultSet($sql);
		}
		
		private function getImportHeaderAry() {
			global $Lang;
			
			$headerAry = array();
			$headerAry['En'][] = $Lang['replySlip']['QuestionType']['En'];
			$headerAry['En'][] = $Lang['replySlip']['QuestionTitle']['En'].' / '.$Lang['replySlip']['Topic']['En'];
			$headerAry['En'][] = $Lang['replySlip']['Options']['En'].' ('.$Lang['replySlip']['StartingFromThisColumn']['En'].')';
			$headerAry['Ch'][] = $Lang['replySlip']['QuestionType']['Ch'];
			$headerAry['Ch'][] = $Lang['replySlip']['QuestionTitle']['Ch'].' / '.$Lang['replySlip']['Topic']['Ch'];
			$headerAry['Ch'][] = $Lang['replySlip']['Options']['Ch'].' ('.$Lang['replySlip']['StartingFromThisColumn']['Ch'].')';
			
			return $headerAry;
		}
		
		private function getImportOptionAryFromRowData($rowDataAry) {
			$_optionAry = array();
			$_optionCount = 0;
			$_numOfOption = count($rowDataAry) - 2;
			for ($j=2; $j<($_numOfOption+2); $j++) {
				$__optionContent = trim($rowDataAry[$j]);
				
				if ($__optionContent != '') {
					$_optionAry[$_optionCount]['content'] = $__optionContent;
					$_optionCount++;
				}
			}
			
			return $_optionAry;
		}
		
		private function getMaxNumOfOptionInCsvData() {
			$csvDataAry = $this->getCsvDataAry();
			array_shift($csvDataAry);
			array_shift($csvDataAry);
			
			$numOfData = count($csvDataAry);
			if ($numOfData == 0) {
				return 0;
			}
			else {
				$optionNumAry = array();
				for ($i=0; $i<$numOfData; $i++) {
					$_optionAry = $this->getImportOptionAryFromRowData($csvDataAry[$i]);
					$optionNumAry[] = count($_optionAry);
				}
				return max($optionNumAry);
			}
		}
		
		// $answerAssoAry[$questionId] = $answerValue
		public function saveUserAnswer($replySlipId, $replySlipUserId, $answerAssoAry) {
			global $replySlipConfig;
			
			$successAry = array();
			$this->libdb->Start_Trans();
			
			if ($replySlipUserId == '') {
				$replySlipUserId = $_SESSION['UserID'];
			}
			
			$questionAssoAry = BuildMultiKeyAssoc($this->returnQuestionData($replySlipId), 'QuestionID');
			$originalAnswerAssoAry = BuildMultiKeyAssoc($this->returnUserAnswer($replySlipId, $replySlipUserId), 'QuestionID');
			
			
			### Delete old question answers
			$originalQuestionIdAry = array_keys($originalAnswerAssoAry);
			$sql = "Delete From INTRANET_REPLY_SLIP_USER_ANSWER Where ReplySlipID = '".$replySlipId."' And UserID = '".$replySlipUserId."' And QuestionID In ('".implode("','", (array)$originalQuestionIdAry)."')";
			$successAry['removeOldAnswer'] = $this->libdb->db_db_query($sql);
			
			
			// loop each questions
			$insertAry = array();
			foreach((array)$answerAssoAry as $_questionId => $_answerValue) {
				$_questionType = $questionAssoAry[$_questionId]['RecordType'];
				
				$_answerAry = array();
				$_storeInOptionId = false;
				$_storeInContent = false;
				if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['singleMC']) {
					if ($_answerValue == '') {
						continue;
					}
					$_answerAry[] = $_answerValue;
					$_storeInOptionId = true;
				}
				else if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['multipleMC']) {
					if ($_answerValue == '') {
						continue;
					}
					$_answerAry = explode(',', $_answerValue);
					$_storeInOptionId = true;
				}
				else if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['shortQuestion'] || $_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['longQuestion']) {
					$_answerAry[] = $_answerValue;
					$_storeInContent = true;
				}
				else if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['na']) {
					// NA type => no answer
					continue;
				}
				else {
					// unknow type => don't save the answer
					continue;
				}
				$_numOfAnswer = count($_answerAry);
				
				for ($i=0; $i<$_numOfAnswer; $i++) {
					$__answerValue = $_answerAry[$i];
					 
					$__optionId = 'null';
					$__content = 'null';
					if ($_storeInOptionId) {
					 	$__optionId = "'".$__answerValue."'";
					}
					else if ($_storeInContent) {
					 	$__content = "'".$this->libdb->Get_Safe_Sql_Query($__answerValue)."'";
					}
						 
					$insertAry[] = " ('".$replySlipUserId."', '".$replySlipId."', '".$_questionId."', ".$__optionId.", ".$__content.", now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') "; 
				}
			}
			
			$numOfInsert = count($insertAry);
			if ($numOfInsert > 0) {
				$insertChunkedAry = array_chunk($insertAry, 1000);
				$numOfChunk = count($insertChunkedAry);
				
				for ($i=0; $i<$numOfChunk; $i++) {
					$sql = "Insert Into INTRANET_REPLY_SLIP_USER_ANSWER
								(UserID, ReplySlipID, QuestionID, OptionID, Content, InputDate, InputBy, ModifiedDate, ModifiedBy)
							Values ".implode(',', $insertChunkedAry[$i]);
					$successAry['insertAnswer'][] = $this->libdb->db_db_query($sql);
				}
			}
			
			if (in_multi_array(false, $successAry)) {
				$this->libdb->RollBack_Trans();
				return false;
			}
			else {
				$this->libdb->Commit_Trans();
				return true;
			}
		}
		
		public function returnUserAnswer($replySlipId, $replySlipUserIdAry='', $optionIdNullToZero=false) {
			if ($replySlipUserIdAry !== '') {
				$condsUserId = " And ans.UserID In ('".implode("','", (array)$replySlipUserIdAry)."') ";
			}
			
			$optionIdField = ($optionIdNullToZero)? 'IFNULL(ans.OptionID, 0)' : 'ans.OptionID';
			
			$userNameField = getNameFieldByLang('iu.');
			$sql = "Select 
							ans.UserAnswerID, ans.UserID, ans.ReplySlipID, ans.QuestionID, $optionIdField as OptionID, ans.Content, $userNameField as answerUserName
					From 
							INTRANET_REPLY_SLIP_USER_ANSWER as ans
							Inner Join INTRANET_USER as iu On (ans.UserID = iu.UserID)
					Where 
							ReplySlipID = '".$replySlipId."' 
							$condsUserId
					Order By
							answerUserName
					";
			return $this->libdb->returnResultSet($sql);
		}
		
		
		
		public function returnReplySlipStatisticsHtml($showDiv=false) {
			global $replySlipConfig, $Lang;
			
			$replySlipId = $this->getReplySlipId();
			
			// get reply slip data
			$replySlipObj = new libReplySlip($replySlipId);
			$showQuestionNum = ($replySlipObj->getShowQuestionNum()==$replySlipConfig['INTRANET_REPLY_SLIP']['ShowQuestionNum']['show'])? true : false;
			$showUserInfoInResult = ($replySlipObj->getShowUserInfoInResult()==$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['show'])? true : false;
			
			// get question data
			$questionAry = $this->returnQuestionData($replySlipId);
			$numOfQuestion = count($questionAry);
			
			// get option data
			$optionAssoAry = BuildMultiKeyAssoc($this->returnOptionData($replySlipId), array('QuestionID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			// get answer data
			$answerAssoAry = BuildMultiKeyAssoc($this->returnUserAnswer($replySlipId, '', $optionIdNullToZero=true), array('QuestionID', 'OptionID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
							
			$mcQuestionTypeAry = $this->returnAnswerOptionQuestionTypeAry();
			$textQuestionTypeAry = $this->returnAnswerTextQuestionTypeAry();
			
			if ($showDiv) {
				$divStyle = 'style="display: block;"';
			}
			
			$x = '';
			$x .= $this->returnGetExportCsvButton($replySlipId).'<br />';
			$x .= '<div class="reply_slip_stat triangle-border top" '.$divStyle.'>'."\r\n";
				for ($i=0; $i<$numOfQuestion; $i++) {
					$_questionId = $questionAry[$i]['QuestionID'];
					$_questionContent = $questionAry[$i]['Content'];
					$_questionType = $questionAry[$i]['RecordType'];
					$_questionNum = $i + 1;
					
					$x .= $_questionContent;
					$x .= '<ul>'."\r\n";
					if (in_array($_questionType, $mcQuestionTypeAry)) {
						$_optionAry = $optionAssoAry[$_questionId];
						$_numOfOption = count($_optionAry);
						
						if ($showUserInfoInResult) {
							$_tableObj = new table_commonTable();
							$_tableObj->setTableType('view');
							$_tableObj->setApplyConvertSpecialChars(false);
							
							$_tableObj->addHeaderTr(new tableTr());
							$_tableObj->addHeaderCell(new tableTh($Lang['replySlip']['Answer'], '30%'));
							$_tableObj->addHeaderCell(new tableTh($Lang['replySlip']['User'], '70%'));
						}
						
						for ($j=0; $j<$_numOfOption; $j++) {
							$__optionId = $_optionAry[$j]['OptionID'];
							$__optionContent = $_optionAry[$j]['Content'];
							$__answerAry = (array)$answerAssoAry[$_questionId][$__optionId];
							$__numOfAnswer = count($__answerAry);
							
							
							
							if ($showUserInfoInResult) {
								$__optionDisplay = '';
								$__optionDisplay .= '<li>'."\r\n";
									$__optionDisplay .= '<em class="reply_stat_result">'.$__numOfAnswer.'</em>';
									$__optionDisplay .= '<span> '.$Lang['DocRouting']['Selected'].' '.$__optionContent.'</span>';
								$__optionDisplay .= '</li>'."\r\n";
								
								$__userNameDisplay = implode(', ', Get_Array_By_Key($__answerAry, 'answerUserName'));
								
								$_tableObj->addBodyTr(new tableTr());
								$_tableObj->addBodyCell(new tableTd($__optionDisplay));
								$_tableObj->addBodyCell(new tableTd($__userNameDisplay));
							}
							else {
								$x .= '<li>'."\r\n";
									$x .= '<em class="reply_stat_result">'.$__numOfAnswer.'</em>';
									$x .= '<span>'.$__optionContent.'</span>';
								$x .= '</li>'."\r\n";
							}
						}
						
						if ($showUserInfoInResult) {
							$x .= '<span>'.$_tableObj->returnHtml().'</span>';
						}
					}
					else if (in_array($_questionType, $textQuestionTypeAry)) {
						$__optionId = 0;
						$_answerAry = (array)$answerAssoAry[$_questionId][$__optionId];
						$_numOfAnswer = count($_answerAry);
						
						$_tableObj = new table_commonTable();
						$_tableObj->setTableType('view');
						$_tableObj->setApplyConvertSpecialChars(false);
						
						if ($showUserInfoInResult) {
							$_tableObj->addHeaderTr(new tableTr());
							$_tableObj->addHeaderCell(new tableTh($Lang['replySlip']['Answer'], '70%'));
							$_tableObj->addHeaderCell(new tableTh($Lang['replySlip']['User'], '30%'));
						}
						
						for ($j=0; $j<$_numOfAnswer; $j++) {
							$_answerUserName = $_answerAry[$j]['answerUserName'];
							$_answerContent = nl2br($_answerAry[$j]['Content']);
							
							$_tableObj->addBodyTr(new tableTr());
							
							$_tableObj->addBodyCell(new tableTd($_answerContent));
							if ($showUserInfoInResult) {
								$_tableObj->addBodyCell(new tableTd($_answerUserName));
							}
						}
						
						
						$x .= '<span>'.$_tableObj->returnHtml().'</span>';
					}
					else if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['na']) {
						// do nth
					}
					$x .= '</ul>'."\r\n";
				}
			$x .= '</div>'."\r\n";
			return $x;
		}
		
		private function returnAnswerOptionQuestionTypeAry() {
			global $replySlipConfig;
			return array($replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['singleMC'], $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['multipleMC']);
		}
		
		private function returnAnswerTextQuestionTypeAry() {
			global $replySlipConfig;
			return array($replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['shortQuestion'], $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['longQuestion']);
		}
		
		public function returnGetSampleCsvLink() {
			global $intranet_root, $PATH_WRT_ROOT, $image_path, $Lang, $LAYOUT_SKIN;
			
			$encryptedPath = getEncryptedText($intranet_root.'/home/common_reply_slip/reply_slip_sample.csv');
			$attachmentLink = $PATH_WRT_ROOT."home/download_attachment.php?target_e=".$encryptedPath;
			
			return '<a class="tablelink" href="'.$attachmentLink.'\" target="_blank" title="'.$Lang['General']['ClickHereToDownloadSample'].'"><img src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/icon_attachment.gif" border="0" align="absmiddle" valign="2">['.$Lang['General']['ClickHereToDownloadSample'].']</a>';
		}
		
		public function exportReplySlipStatisticsCSV($title="") {
			global $replySlipConfig, $Lang, $PATH_WRT_ROOT;
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$libexport = new libexporttext();
			$replySlipId = $this->getReplySlipId();
			
			$columns = array();
			$rows = array();
			$row = array();
			
			// get reply slip data
			$replySlipObj = new libReplySlip($replySlipId);
			$showQuestionNum = ($replySlipObj->getShowQuestionNum()==$replySlipConfig['INTRANET_REPLY_SLIP']['ShowQuestionNum']['show'])? true : false;
			$showUserInfoInResult = ($replySlipObj->getShowUserInfoInResult()==$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['show'])? true : false;
			
			// get question data
			$questionAry = $this->returnQuestionData($replySlipId);
			$numOfQuestion = count($questionAry);
			
			// get option data
			$optionAssoAry = BuildMultiKeyAssoc($this->returnOptionData($replySlipId), array('QuestionID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			// get answer data
			$answerAssoAry = BuildMultiKeyAssoc($this->returnUserAnswer($replySlipId, '', $optionIdNullToZero=true), array('QuestionID', 'OptionID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
							
			$mcQuestionTypeAry = $this->returnAnswerOptionQuestionTypeAry();
			$textQuestionTypeAry = $this->returnAnswerTextQuestionTypeAry();
			
			for ($i=0; $i<$numOfQuestion; $i++) {
				$_questionId = $questionAry[$i]['QuestionID'];
				$_questionContent = $questionAry[$i]['Content'];
				$_questionType = $questionAry[$i]['RecordType'];
				$_questionNum = $i + 1;
				
				$row = array();
				$row[] = $_questionContent;
				$row[] = " ";
				$rows[] = $row;
				
				if (in_array($_questionType, $mcQuestionTypeAry)) {
					$_optionAry = $optionAssoAry[$_questionId];
					$_numOfOption = count($_optionAry);
					
					for ($j=0; $j<$_numOfOption; $j++) {
						$__optionId = $_optionAry[$j]['OptionID'];
						$__optionContent = $_optionAry[$j]['Content'];
						$__answerAry = (array)$answerAssoAry[$_questionId][$__optionId];
						$__numOfAnswer = count($__answerAry);
						
						$__userNameDisplay = implode(', ', Get_Array_By_Key($__answerAry, 'answerUserName'));
						
						$row = array();
						$row[] = $__optionContent;
						$row[] = $__numOfAnswer;
						
						if ($showUserInfoInResult) {
							$row[] = $__userNameDisplay;
						}else {
							$row[] = " ";
						}
						
						$rows[] = $row;
					}
				}
				else if (in_array($_questionType, $textQuestionTypeAry)) {
					$__optionId = 0;
					$_answerAry = (array)$answerAssoAry[$_questionId][$__optionId];
					$_numOfAnswer = count($_answerAry);

					for ($j=0; $j<$_numOfAnswer; $j++) {
						$row = array();
						$col = "";
						$_answerUserName = $_answerAry[$j]['answerUserName'];
						$_answerContent = $_answerAry[$j]['Content'];
						
						//if ($showUserInfoInResult) {
						//	$col .= "[".$_answerUserName."] ";
						//}
						$col .= $_answerContent;
						$row[] = $col;
						if ($showUserInfoInResult) {
							$row[] = $_answerUserName;
						}else {
							$row[] = " ";
						}
						$rows[] = $row;
					}
				}
				else if ($_questionType == $replySlipConfig['INTRANET_REPLY_SLIP_QUESTION']['RecordType']['na']) {
					// do nth
				}
			}
			
			$filename = "reply_slip.csv";
			if($title != ""){
				if(strtolower(substr($title,strrpos($title,"."))) != ".csv") {
					$filename = $title.".csv";
				}else {
					$filename = $title;
				}
			}
			
			$export_content = $libexport->GET_EXPORT_TXT($rows, $columns,"","\r\n","",0,"11");
			$libexport->EXPORT_FILE($filename, $export_content);	
		}
		
		public function returnGetExportCsvButton($replySlipId, $title="") {
			global $intranet_root, $PATH_WRT_ROOT, $image_path, $Lang, $LAYOUT_SKIN;
			
			$link = '/home/common_reply_slip/export_reply_slip_csv.php?ReplySlipID='.$replySlipId.'&Title='.rawurlencode($title);
			
			return $this->linterface->GET_LNK_EXPORT_IP25($link);
		}
		
		function updateShowUserInfoInResult($val)
		{
			$replySlipId = $this->getReplySlipId();
			$replySlipObj = new libReplySlip($replySlipId);
			
			return $replySlipObj->updateShowUserInfoInResult($val);
		}
	}
}
?>