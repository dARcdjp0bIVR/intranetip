<?php
// editing by ivan

if (!defined("LIBREPLYSLIPQUESTION_DEFINED")) {
	define("LIBREPLYSLIPQUESTION_DEFINED", true);
	
	class libReplySlipQuestion extends libdbobject {
		private $questionId;
		private $replySlipId;
		private $recordType;
		private $content;
		private $displayOrder;
		private $inputDate;
		private $inputBy;
		private $modifiedDate;
		private $modifiedBy;
		
		public function __construct($objectId='') {
			parent::__construct('INTRANET_REPLY_SLIP_QUESTION', 'QuestionID', $this->returnFieldMappingAry(), $objectId);
		}
		
		public function setQuestionId($val) {
			$this->questionId = $val;
		}
		public function getQuestionId() {
			return $this->questionId;
		}
		
		public function setReplySlipId($val) {
			$this->replySlipId = $val;
		}
		public function getReplySlipId() {
			return $this->replySlipId;
		}
		
		public function setRecordType($val) {
			$this->recordType = $val;
		}
		public function getRecordType() {
			return $this->recordType;
		}
		
		public function setContent($val) {
			$this->content = $val;
		}
		public function getContent() {
			return $this->content;
		}
		
		public function setDisplayOrder($val) {
			$this->displayOrder = $val;
		}
		public function getDisplayOrder() {
			return $this->displayOrder;
		}
		
		public function setInputDate($val) {
			$this->inputDate = $val;
		}
		public function getInputDate() {
			return $this->inputDate;
		}
		
		public function setInputBy($val) {
			$this->inputBy = $val;
		}
		public function getInputBy() {
			return $this->inputBy;
		}
		
		public function setModifiedDate($val) {
			$this->modifiedDate = $val;
		}
		public function getModifiedDate() {
			return $this->modifiedDate;
		}
		
		public function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		public function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		private function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('QuestionID', 'int', 'setQuestionId', 'getQuestionId');
			$fieldMappingAry[] = array('ReplySlipID', 'int', 'setReplySlipId', 'getReplySlipId');
			$fieldMappingAry[] = array('RecordType', 'int', 'setRecordType', 'getRecordType');
			$fieldMappingAry[] = array('Content', 'str', 'setContent', 'getContent');
			$fieldMappingAry[] = array('DisplayOrder', 'int', 'setDisplayOrder', 'getDisplayOrder');
			$fieldMappingAry[] = array('InputDate', 'date', 'setInputDate', 'getInputDate');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('ModifiedDate', 'date', 'setModifiedDate', 'getModifiedDate');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
		
		protected function newRecordBeforeHandling() {
			$this->setInputDate('now()');
			$this->setInputBy($this->getInputBy());
			$this->setModifiedDate('now()');
			$this->setModifiedBy($this->getModifiedBy());
			
			return true;
		}
		
		protected function deleteRecordAfterHandling() {
			$optionIdAry = Get_Array_By_Key($this->returnOptionData(), 'OptionID');
			$numOfOption = count($optionIdAry);
			
			$successAry = array();
			for ($i=0; $i<$numOfOption; $i++) {
				$_optionId = $optionIdAry[$i];
				$_optionObj = new libReplySlipQuestionOption($_optionId);
				$successAry['deleteOption'][$_optionId] = $_optionObj->deleteRecord();
			}
			
			return !in_multi_array(false, $successAry);
		}
		
		private function returnOptionData() {
			$questionId = $this->getQuestionId();
			$sql = "Select OptionID From INTRANET_REPLY_SLIP_QUESTION_OPTION Where QuestionID = '".$questionId."'";
			return $this->objDb->returnResultSet($sql);
		}
	}
}
?>