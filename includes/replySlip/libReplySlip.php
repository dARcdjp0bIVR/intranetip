<?php
// editing by 
/*
 * 2013-09-17 (Carlos): added updateShowUserInfoInResult()
 */
if (!defined("LIBREPLYSLIP_DEFINED")) {
	define("LIBREPLYSLIP_DEFINED", true);
	
	class libReplySlip extends libdbobject {
		private $replySlipId;
		private $linkToModule;
		private $linkToType;
		private $linkToId;
		private $title;
		private $description;
		private $showQuestionNum;
		private $ansAllQuestion;
		private $showUserInfoInResult;
		private $recordType;
		private $recordStatus;
		private $inputDate;
		private $inputBy;
		private $modifiedDate;
		private $modifiedBy;
		
		public function __construct($objectId='') {
			parent::__construct('INTRANET_REPLY_SLIP', 'ReplySlipID', $this->returnFieldMappingAry(), $objectId);
		}
		
		public function setReplySlipId($val) {
			$this->replySlipId = $val;
		}
		public function getReplySlipId() {
			return $this->replySlipId;
		}
		
		public function setLinkToModule($val) {
			$this->linkToModule = $val;
		}
		public function getLinkToModule() {
			return $this->linkToModule;
		}
		
		public function setLinkToType($val) {
			$this->linkToType = $val;
		}
		public function getLinkToType() {
			return $this->linkToType;
		}
		
		public function setLinkToId($val) {
			$this->linkToId = $val;
		}
		public function getLinkToId() {
			return $this->linkToId;
		}
		
		public function setTitle($val) {
			$this->title = $val;
		}
		public function getTitle() {
			return $this->title;
		}
		
		public function setDescription($val) {
			$this->description = $val;
		}
		public function getDescription() {
			return $this->description;
		}
		
		public function setShowQuestionNum($val) {
			$this->showQuestionNum = $val;
		}
		public function getShowQuestionNum() {
			return $this->showQuestionNum;
		}
		
		public function setAnsAllQuestion($val) {
			$this->ansAllQuestion = $val;
		}
		public function getAnsAllQuestion() {
			return $this->ansAllQuestion;
		}
		
		public function setShowUserInfoInResult($val) {
			$this->showUserInfoInResult = $val;
		}
		public function getShowUserInfoInResult() {
			return $this->showUserInfoInResult;
		}
		
		public function setRecordType($val) {
			$this->recordType = $val;
		}
		public function getRecordType() {
			return $this->recordType;
		}
		
		public function setRecordStatus($val) {
			$this->recordStatus = $val;
		}
		public function getRecordStatus() {
			return $this->recordStatus;
		}
		
		public function setInputDate($val) {
			$this->inputDate = $val;
		}
		public function getInputDate() {
			return $this->inputDate;
		}
		
		public function setInputBy($val) {
			$this->inputBy = $val;
		}
		public function getInputBy() {
			return $this->inputBy;
		}
		
		public function setModifiedDate($val) {
			$this->modifiedDate = $val;
		}
		public function getModifiedDate() {
			return $this->modifiedDate;
		}
		
		public function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		public function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		private function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('ReplySlipID', 'int', 'setReplySlipId', 'getReplySlipId');
			$fieldMappingAry[] = array('LinkToModule', 'str', 'setLinkToModule', 'getLinkToModule');
			$fieldMappingAry[] = array('LinkToType', 'str', 'setLinkToType', 'getLinkToType');
			$fieldMappingAry[] = array('LinkToID', 'int', 'setLinkToId', 'getLinkToId');
			$fieldMappingAry[] = array('Title', 'str', 'setTitle', 'getTitle');
			$fieldMappingAry[] = array('Description', 'str', 'setDescription', 'getDescription');
			$fieldMappingAry[] = array('ShowQuestionNum', 'int', 'setShowQuestionNum', 'getShowQuestionNum');
			$fieldMappingAry[] = array('AnsAllQuestion', 'int', 'setAnsAllQuestion', 'getAnsAllQuestion');
			$fieldMappingAry[] = array('ShowUserInfoInResult', 'int', 'setShowUserInfoInResult', 'getShowUserInfoInResult');
			$fieldMappingAry[] = array('RecordType', 'int', 'setRecordType', 'getRecordType');
			$fieldMappingAry[] = array('RecordStatus', 'int', 'setRecordStatus', 'getRecordStatus');
			$fieldMappingAry[] = array('InputDate', 'date', 'setInputDate', 'getInputDate');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('ModifiedDate', 'date', 'setModifiedDate', 'getModifiedDate');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
		
		protected function newRecordBeforeHandling() {
			$this->setInputDate('now()');
			$this->setInputBy($this->getInputBy());
			$this->setModifiedDate('now()');
			$this->setModifiedBy($this->getModifiedBy());
			
			return true;
		}
		
//		protected function deleteRecordAfterHandling() {
//			$questionIdAry = Get_Array_By_Key($this->returnQuestionData(), 'QuestionID');
//			$numOfQuestion = count($questionIdAry);
//			
//			$successAry = array();
//			for ($i=0; $i<$numOfQuestion; $i++) {
//				$_questionId = $questionIdAry[$i];
//				$_questionObj = new libReplySlipQuestion($_questionId);
//				$successAry['deleteQuestion'][$_questionId] = $_questionObj->deleteRecord();
//			}
//			
//			return !in_multi_array(false, $successAry);
//		}

		public function deleteRecord() {
			global $replySlipConfig;
			
			$this->setRecordStatus($replySlipConfig['INTRANET_REPLY_SLIP']['RecordStatus']['deleted']);
			$objectId = $this->save();
			
			return ($objectId > 0)? true : false; 
		}
		
		private function returnQuestionData() {
			$replySlipId = $this->getReplySlipId();
			$sql = "Select QuestionID From INTRANET_REPLY_SLIP_QUESTION Where ReplySlipID = '".$replySlipId."'";
			return $this->objDb->returnResultSet($sql);
		}
		
		public function updateShowUserInfoInResult($val)
		{
			$this->setShowUserInfoInResult($val);
			$objectId = $this->save();
			
			return ($objectId > 0)? true : false;
		}
	}
}
?>