<?php
// editing by yuen
include_once('libinterface.php');
include_once('libuser.php');
include_once('form_class_manage_ui.php');
include_once('subject_class_mapping_ui.php');
//include_once('libclass.php');
include_once("$intranet_root/lang/classdiary_lang.$intranet_session_language.php");

class libclassdiary_ui extends interface_html
{
	var $libclassdiary = null;
	var $thickBoxWidth;
	var $thickBoxHeight;
	var $toolCellWidth;
	
	function libclassdiary_ui() 
	{
		parent::interface_html();
		
		$this->thickBoxWidth = 750;
		$this->thickBoxHeight = 480;
		$this->toolCellWidth = "120px";
	}
	
	function Get_libclassdiary()
	{
		if(!$this->libclassdiary){
			$this->libclassdiary = new libclassdiary();
		}
		
		return $this->libclassdiary;
	}
	
	function GET_MODULE_OBJ_ARR() 
	{
		global $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $Lang, $sys_custom;
		
		$libclassdiary = $this->Get_libclassdiary();
		
		# Menu information
		
		$ClassDiaryPath = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/classdiary/";
		
		$MenuArr["Management"] = array($Lang['ClassDiary']['Management'], "", $CurrentPage['ClassDiary']);
		$MenuArr["Management"]["Child"]["ClassDiary"] = array($Lang['ClassDiary']['ClassDiary'], $ClassDiaryPath."management/classdiary/", $CurrentPage['ClassDiary']);
		
		$MenuArr["Report"] = array($Lang['ClassDiary']['Report'], "", $CurrentPage['Statistics']);
		$MenuArr["Report"]["Child"]["Statistics"] = array($Lang['ClassDiary']['Statistics'], $ClassDiaryPath."report/statistics/", $CurrentPage['Statistics']);
		$MenuArr["Report"]["Child"]["ExportMailMerge"] = array($Lang['ClassDiary']['ExportMailMerge'], $ClassDiaryPath."report/statistics/export_mail_merge.php", $CurrentPage['ExportMailMerge']);
		
		# module information
	    $MODULE_OBJ['title'] = $Lang['ClassDiary']['ClassDiary'];
	    $MODULE_OBJ['title_css'] = "menu_opened";
	    $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_ediscipline.gif";
	    $MODULE_OBJ['root_path'] = $ClassDiaryPath;
	    $MODULE_OBJ['menu'] = $MenuArr;
	
	    return $MODULE_OBJ;
	}
	
	function Include_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		include_once($PATH_WRT_ROOT.'home/eAdmin/StudentMgmt/classdiary/js_lang.php');
		/*
		$x .= '
			<link href="'.$PATH_WRT_ROOT.'templates/2007a/css/content.css" rel="stylesheet" type="text/css"/>
			<link href="'.$PATH_WRT_ROOT.'templates/2009a/css/content.css" rel="stylesheet" type="text/css"/>
			<link href="'.$PATH_WRT_ROOT.'templates/2009a/css/content_25.css" rel="stylesheet" type="text/css"/>';
		*/	
		$x .= '
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.alerts.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.core.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.draggable.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.droppable.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.selectable.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.scrollTo-min.js"></script>
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.alerts.css" type="text/css" media="screen" />
			';
		$x .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'/home/eAdmin/StudentMgmt/classdiary/classdiary_script.js"></script>';
		
		return $x;
	}
	
	function Get_Class_Diary_Index()
	{
		global $Lang;
		
		$from_date = date("Y-m-d",getStartOfAcademicYear());
		$to_date = date("Y-m-d",getEndOfAcademicYear());
		
		# Add Btn
		$NewBtn = $this->Get_Content_Tool_v30('new','edit_classdiary.php',$Lang['Btn']['New']);
		
		# Date Range
		$DateRange = $Lang['General']['From'].'&nbsp;'.$this->GET_DATE_PICKER("FromDate",$from_date);
		$DateRange.= $Lang['General']['To'].'&nbsp;'.$this->GET_DATE_PICKER("ToDate",$to_date); 
		
		# View Btn
		$ViewBtn = $this->GET_SMALL_BTN($Lang['Btn']['View'],"button","Get_Class_Diary_Table();","ViewButton");
		
		$x = $this->Include_JS_CSS();
		$x .= '<form id="form1" name="form1" method="post" action="index.php">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
					
						$x .= '<div class="content_top_tool">'."\n";
							$x .= '<div class="Conntent_tool">'."\n";
								$x .= $NewBtn."\n";
							$x .= '</div>'."\n";
							//$x .= '<div class="Conntent_search"><input type="text" id="Keyword" name="Keyword" value="'.htmlspecialchars($Keyword,ENT_QUOTES).'"></div>'."\n";
							$x .= '<br style="clear: both;">'."\n";
						$x .= '</div>'."\n";
						$x .= '<br />';
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="table_filter">'."\n";
										$x .= $DateRange.'&nbsp;'.$ViewBtn."\n";
										$x .= '<span id="DateWarningLayer" style="color:red;display:none;"></span>';
									$x .= '</div>'."\n";
									$x .= '<p class="spacer"></p>';
								$x .= '</td>';
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="common_table_tool">'."\n";
										$x .= '<a class="tool_edit" href="javascript:checkEdit(document.form1,\'DiaryID[]\',\'edit_classdiary.php\')">'.$Lang['Btn']['Edit'].'</a>'."\n";
										$x .= '<a class="tool_delete" href="javascript:checkRemove(document.form1,\'DiaryID[]\',\'delete_classdiary.php\')">'.$Lang['Btn']['Delete'].'</a>'."\n";
									$x .= '</div>'."\n";		
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n"; 
						# Main Table 
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="ClassDiaryTableDiv">'."\n";
										//$x .= $this->Get_Class_Diary_Table($from_date,$to_date);
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Class_Diary_Table($FromDate='',$ToDate='')
	{
		global $Lang;
		
		$libclassdiary = $this->Get_libclassdiary();
		
		$ClassDiarys = $libclassdiary->Get_Class_Diarys("","",$FromDate,$ToDate);
		
		$x .= '<table class="common_table_list_v30" id="ClassDiaryTable">'."\n";
		# col group
			$x .= '<col align="left" style="width: 10px">'."\n";
			$x .= '<col align="left" style="width: 10%;">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="left" style="width: 15%;">'."\n";
			$x .= '<col align="left" style="width: 15%;">'."\n";
			$x .= '<col align="left" style="width: 15%;">'."\n";
			$x .= '<col align="left" style="width: 30px">'."\n";
	
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Lesson_Session'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['RecordDate'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Class'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Creator'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['LastModifiedBy'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['LastModifiedDate'].'</th>'."\n";
					$x .= '<th>'.$this->Get_Checkbox("CheckMaster", "", "", 0, '', '', 'Check_All(\'DiaryID[]\',this.checked)').'</th>';
				$x .= '</tr>'."\n";	
			$x .= '</thead>'."\n";
		
			$x .= '<tbody>'."\n";
			if(sizeof($ClassDiarys)==0){
				$x .= '<tr><td colspan="7" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			}
			for($i=0;$i<sizeof($ClassDiarys);$i++)
			{
				list($diary_id, $class_id, $record_date, $class_name, $creator, $last_modified_by, $last_modified_date) = $ClassDiarys[$i];
				$x .= '<tr>'."\n";
				$x .= '<td>'.($i+1).'</td>'."\n";
				$x .= '<td>'.$record_date.'</td>'."\n";
				$x .= '<td><a href="classdiary_detail.php?DiaryID='.$diary_id.'" title="'.$Lang['ClassDiary']['ViewRecordDetail'].'">'.$libclassdiary->html_special_chars($class_name).'</a></td>'."\n";
				$x .= '<td>'.$libclassdiary->html_special_chars($creator).'</td>'."\n";
				$x .= '<td>'.$libclassdiary->html_special_chars($last_modified_by).'</td>'."\n";
				$x .= '<td>'.$libclassdiary->html_special_chars($last_modified_date).'</td>'."\n";
				$x .= '<td>'.$this->Get_Checkbox("", "DiaryID[]", $diary_id, 0, '', '', '').'</td>'."\n";
				$x .= '</tr>'."\n";
			}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br />';
		
		return $x;	
	}
	
	function Get_Class_Diary_View_Form($DiaryID)
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		
		$libclassdiary = $this->Get_libclassdiary();
		$DiaryDetail = $libclassdiary->Get_Class_Diary_Lessons($DiaryID, $IsAll=true);
		$LessonNumberToDetail = $DiaryDetail['LessonNumberToDetail'];
		//debug_r($LessonNumberToDetail);
		$LessonIDToUser = $DiaryDetail['LessonIDToUser'];
		$AbsentAMStudents = $DiaryDetail['AbsentAMStudents'];
		$AbsentPMStudents = $DiaryDetail['AbsentPMStudents'];
		$LateStudents = $DiaryDetail['LateStudents'];
		$EarlyleaveStudents = $DiaryDetail['EarlyleaveStudents'];
		//debug_pr($DiaryDetail);
		# Navigation
		$NavigationObj[] = array($Lang['ClassDiary']['ClassDiary'],"index.php");
		$NavigationObj[] = array($Lang['ClassDiary']['ViewRecordDetail'],"");
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj,"navigation_2");
		
		$x = $this->Include_JS_CSS();
		$x .= $Navigation;
		$x.= '<br />';
		$x.= '<div class="content_top_tool">';
			$x.= '<div class="Conntent_tool">';
				$x .= $this->GET_LNK_PRINT("javascript:newWindow('print_classdiary.php?DiaryID=".$DiaryID."',32);","","","","",1);
			$x.= '</div>';
		$x.= '<br style="clear: both;">';
		$x.= '</div>';
		
		$x.= '<table class="form_table_v30">'."\n";
		$x.= '<col class="field_title">
			  <col class="field_c">
			  <tbody>'."\n";
		$x.= '<tr>'."\n";
			$x.='<td class="field_title">'.$Lang['ClassDiary']['Class'].'</td>';
			$x.='<td>'.$libclassdiary->html_special_chars($DiaryDetail['ClassName']).'</td>';
		$x.= '</tr>'."\n";
		$x.= '<tr>'."\n";
			$x.= '<td class="field_title">'.$Lang['ClassDiary']['RecordDate'].'</td>';
			$x.= '<td>'.$DiaryDetail['DiaryDate'].'</td>';
		$x.= '</tr>'."\n";
		$x.='</tbody>
			</table>';
		
		$x.='<br />';
		
		$x.='<table class="common_table_list_v30">';
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th rowspan="2" style="width: 10px;">'.$Lang['ClassDiary']['Lesson_Session'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 10%;">'.$Lang['ClassDiary']['Subject'].'</th>'."\n";
					$x .= '<th colspan="4" style="width: 15%;">'.$Lang['ClassDiary']['MediumOfInstruction'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 20%;">'.$Lang['ClassDiary']['Assignment'].'</th>'."\n";
					//$x .= '<th rowspan="2" style="width: 15%;">'.$Lang['ClassDiary']['DateOfSubmission'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 13%;">'.$Lang['ClassDiary']['ForgotToBringBooks'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 13%;">'.$Lang['ClassDiary']['StudentsExcuseLessons'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 13%;">'.$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 15%;">'.$Lang['ClassDiary']['Others'].'</th>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>';
				foreach($Lang['ClassDiary']['MediumOfInstructionMapping'] as $key => $val){
					$x .= '<th>'.$key.'</th>';
				}
				$x .= '</tr>';	
			$x .= '</thead>'."\n";
		$x.='</tr>';
		
		$lessonListed = array();
		for($i=1;$i<=$libclassdiary->MAX_NUMBER_OF_LESSON;$i++) {
			if(isset($LessonNumberToDetail[$i])){
				//debug_r($LessonNumberToDetail[$i]);
				for ($j=0; $j<sizeof($LessonNumberToDetail[$i]); $j++)
				{
					$ThisLessonNumberToDetail = $LessonNumberToDetail[$i][$j];
					$libuser = new libuser($ThisLessonNumberToDetail['InputBy']);
					$TeacherName = $libuser->UserName(0);
					$lesson_id = $ThisLessonNumberToDetail['LessonID'];
					$x .= '<tr>';
					$x .= '<td>'.(($j>0)?"&nbsp;":$i).'</td>';
					$x .= '<td>'.$libclassdiary->html_special_chars($ThisLessonNumberToDetail['Subject']).' <br />('.$TeacherName.')</td>';
					$x .= '<td>'.($ThisLessonNumberToDetail['InstructionMediumP']==1? '<img width="20" height="20" border="0" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_tick_green.gif">' :'&nbsp;').'</td>';
					$x .= '<td>'.($ThisLessonNumberToDetail['InstructionMediumE']==1? '<img width="20" height="20" border="0" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_tick_green.gif">' :'&nbsp;').'</td>';
					$x .= '<td>'.($ThisLessonNumberToDetail['InstructionMediumC']==1? '<img width="20" height="20" border="0" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_tick_green.gif">' :'&nbsp;').'</td>';
					$x .= '<td>'.($ThisLessonNumberToDetail['InstructionMediumB']==1? '<img width="20" height="20" border="0" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_tick_green.gif">' :'&nbsp;').'</td>';
					$x .= '<td>'.nl2br($libclassdiary->html_special_chars($ThisLessonNumberToDetail['Assignment'])).'</td>';
					//$x .= '<td>'.$libclassdiary->html_special_chars($ThisLessonNumberToDetail['SubmissionDate']).'</td>';
					$x .= '<td>';
					if(isset($LessonIDToUser[$lesson_id]) && count($LessonIDToUser[$lesson_id][RECORD_TYPE_FORGOT_BOOK])>0){
						$students = $LessonIDToUser[$lesson_id][RECORD_TYPE_FORGOT_BOOK];
						$x .= $this->Get_Student_Names_Block($students);
					}else{
						$x .= '-';
	 				}
					$x .= '</td>';
					
					$x .= '<td>';
					if(isset($LessonIDToUser[$lesson_id]) && count($LessonIDToUser[$lesson_id][RECORD_TYPE_EXCUSE_LESSON])>0){
						$students = $LessonIDToUser[$lesson_id][RECORD_TYPE_EXCUSE_LESSON];
						$x .= $this->Get_Student_Names_Block($students);
					}else{
						$x .= '-';
	 				}
					$x .= '</td>';
					
					$x .= '<td>';
					if(isset($LessonIDToUser[$lesson_id]) && count($LessonIDToUser[$lesson_id][RECORD_TYPE_NO_MEDIUM_INSTRUCTION])>0){
						$students = $LessonIDToUser[$lesson_id][RECORD_TYPE_NO_MEDIUM_INSTRUCTION];
						$x .= $this->Get_Student_Names_Block($students);
					}else{
						$x .= '-';
	 				}
					$x .= '</td>';
					
					$x .= '<td>'.$libclassdiary->html_special_chars($ThisLessonNumberToDetail['Others']).'</td>';
					$x .= '</tr>';
				}
			}else{
				
				$x .= '<tr>';
				$x .= '<td>'.$i.'</td>';
				$x .= '<td>-</td>';
				$x .= '<td>-</td>';
				$x .= '<td>-</td>';
				$x .= '<td>-</td>';
				$x .= '<td>-</td>';
				//$x .= '<td>-</td>';
				$x .= '<td>-</td>';
				$x .= '<td>-</td>';
				$x .= '<td>-</td>';
				$x .= '<td>-</td>';
				$x .= '<td>-</td>';
				$x .= '</tr>';
			}
		}
		
		$x.='</table>';
		
		$x.='<div>';
		foreach($Lang['ClassDiary']['MediumOfInstructionMapping'] as $key => $val){
			$x.=$key.': '.$val;
			for($k=0;$k<10;$k++){
				$x.='&nbsp;';
			}
		}
		$x.='</div>';
		
		$x.= '<br />';
		
		$x.= '<table class="form_table_v30">'."\n";
		$x.= '<col class="field_title">
			  <col class="field_c">
			  <tbody>'."\n";
		$x.= '<tr>'."\n";
			$x.='<td class="field_title">'.$Lang['ClassDiary']['Absentee'].'&nbsp;'.$Lang['ClassDiary']['AM'].'</td>';
			$x.='<td>';
			if(count($AbsentAMStudents)>0){
				$x .= $this->Get_Student_Names_Block($AbsentAMStudents);
			}else{
				$x .= '-';
			}
			$x.='</td>';
		$x.= '</tr>'."\n";
		
		$x.= '<tr>'."\n";
			$x.='<td class="field_title">'.$Lang['ClassDiary']['Absentee'].'&nbsp;'.$Lang['ClassDiary']['PM'].'</td>';
			$x.='<td>';
			if(count($AbsentPMStudents)>0){
				$x .= $this->Get_Student_Names_Block($AbsentPMStudents);
			}else{
				$x .= '-';
			}
			$x.='</td>';
		$x.= '</tr>'."\n";
		
		$x.= '<tr>'."\n";
			$x.='<td class="field_title">'.$Lang['ClassDiary']['Late'].'</td>';
			$x.='<td>';
			if(count($LateStudents)>0){
				$x .= $this->Get_Student_Names_Block($LateStudents);
			}else{
				$x .= '-';
			}
			$x.='</td>';
		$x.= '</tr>'."\n";
		
		$x.= '<tr>'."\n";
			$x.='<td class="field_title">'.$Lang['ClassDiary']['EarlyLeave'].'</td>';
			$x.='<td>';
			if(count($EarlyleaveStudents)>0){
				$x .= $this->Get_Student_Names_Block($EarlyleaveStudents);
			}else{
				$x .= '-';
			}
			$x.='</td>';
		$x.= '</tr>'."\n";
		
		$x.='</tbody>
			</table>';
		
		# edit bottom
			$EditBtn = $this->GET_ACTION_BTN($Lang['Btn']['Edit'],"button","window.location='edit_classdiary.php?DiaryID=".$DiaryID."';");
			$DeleteBtn = $this->GET_ACTION_BTN($Lang['Btn']['Delete'],"button","DeleteClassDiary(".$DiaryID.");");
			$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Back'],"button","window.location='index.php'");
		
		$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $EditBtn;
				$x .= '&nbsp;'.$DeleteBtn;
				$x .= "&nbsp;".$BackBtn;
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Class_Diary_Edit_Form($DiaryID='')
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		
		$is_edit = (is_numeric($DiaryID) && $DiaryID > 0);
		
		//$form_class_manage_ui = new form_class_manage_ui();
		//$libclass = new libclass();
		$subject_class_mapping_ui = new subject_class_mapping_ui();
		$libclassdiary = $this->Get_libclassdiary();
		$DiaryDetail = array();
		if($is_edit){
			$DiaryDetail = $libclassdiary->Get_Class_Diary_Lessons($DiaryID);
		}
		$ClassID = $DiaryDetail['ClassID'];
		$DiaryDate = $DiaryDetail['DiaryDate'];
		$LessonNumberToDetail = $DiaryDetail['LessonNumberToDetail'];
		$LessonIDToUser = $DiaryDetail['LessonIDToUser'];
		$AbsentAMStudents = $DiaryDetail['AbsentAMStudents'];
		$AbsentPMStudents = $DiaryDetail['AbsentPMStudents'];
		$LateStudents = $DiaryDetail['LateStudents'];
		$EarlyleaveStudents = $DiaryDetail['EarlyleaveStudents'];
		
		if(!$is_edit){
			//$class_selection = $form_class_manage_ui->Get_Class_Selection($libclassdiary->Get_Current_Academic_Year_ID(),'','ClassID',$ClassID,'ResetClassStudents();');
			$class_selection = $this->Get_Class_Selection('id="ClassID" name="ClassID" onchange="ResetClassStudents();" ', $ClassID);
			$class_selection.= $this->Get_Form_Warning_Msg("ClassIDWarningDiv",$Lang['ClassDiary']['WarningMsg']['SelectClass'],"");
			$date_selection = $this->GET_DATE_PICKER("DiaryDate",date("Y-m-d"));
			$date_selection.= $this->Get_Form_Warning_Msg("DiaryDateWarningDiv",$Lang['ClassDiary']['WarningMsg']['InvalidDate'],"");
		}else{
			$class_selection = $libclassdiary->html_special_chars($DiaryDetail['ClassName']).'<input type="hidden" id="ClassID" name="ClassID" value="'.$DiaryDetail['ClassID'].'" />';
			$date_selection = $DiaryDate.'<input type="hidden" id="DiaryDate" name="DiaryDate" value="'.$DiaryDate.'" />'."\n";
		}
		
		# Navigation
		$NavigationObj[] = array($Lang['ClassDiary']['ClassDiary'],"index.php");
		$NavigationObj[] = array($is_edit?$Lang['Btn']['Edit']:$Lang['Btn']['New'],"");
		$Navigation = $this->GET_NAVIGATION_IP25($NavigationObj,"navigation_2");
		
		$x = $this->Include_JS_CSS();
		$x.= $Navigation;
		$x.= '<br />';
		$x.= '<form id="EditForm" name="EditForm" method="post" action="" >'; 
		$x.= '<table class="form_table_v30">'."\n";
		$x.= '<col class="field_title">
			  <col class="field_c">
			  <tbody>'."\n";
		$x.= '<tr>'."\n";
			$x.='<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ClassDiary']['Class'].'</td>';
			$x.='<td>'.$class_selection.'</td>';
		$x.= '</tr>'."\n";
		$x.= '<tr>'."\n";
			$x.= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['ClassDiary']['RecordDate'].'</td>';
			$x.= '<td>'.$date_selection.'</td>';
		$x.= '</tr>'."\n";
		$x.='</tbody>
			</table>'."\n";
		
		$x.='<br />'."\n";
		
		$x.='<table class="common_table_list_v30">'."\n";
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th rowspan="2" style="width: 5%">'.$Lang['ClassDiary']['Lesson_Session'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 10%;"><span class="tabletextrequire">*</span>'.$Lang['ClassDiary']['Subject'].'</th>'."\n";
					$x .= '<th colspan="4" style="width: 10%;"><span class="tabletextrequire">*</span>'.$Lang['ClassDiary']['MediumOfInstruction'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 20%;">'.$Lang['ClassDiary']['Assignment'].'</th>'."\n";
					//$x .= '<th rowspan="2" style="width: 15%;">'.$Lang['ClassDiary']['DateOfSubmission'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 10%;">'.$Lang['ClassDiary']['ForgotToBringBooks'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 10%;">'.$Lang['ClassDiary']['StudentsExcuseLessons'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 10%;">'.$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width: 15%;">'.$Lang['ClassDiary']['Others'].'</th>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>';
				foreach($Lang['ClassDiary']['MediumOfInstructionMapping'] as $key => $val){
					$x .= '<th>'.$key.'<input type="checkbox" name="checkmaster'.$key.'" onClick="(this.checked)?setChecked(1,this.form,\'InstructionMedium'.$key.'[]\'):setChecked(0,this.form,\'InstructionMedium'.$key.'[]\')" /></th>';
				}
				$x .= '</tr>';	
			$x .= '</thead>'."\n";
		$x.='</tr>'."\n";
		$text_css = array('style'=>'width:96%;');
		for($i=1;$i<=$libclassdiary->MAX_NUMBER_OF_LESSON;$i++) {
			$x .= '<tr>';
			
			$lesson_id = $LessonNumberToDetail[$i][0]['LessonID'];
			$subject_id = $LessonNumberToDetail[$i][0]['SubjectID'];
			
			//$submission_date = trim($LessonNumberToDetail[$i][0]['SubmissionDate']);
			
			$x .= '<td>'.$i.'<input type="hidden" id="LessonID'.$i.'" name="LessonID[]" value="'.$lesson_id.'" /></td>';
			//$x .= '<td>'.$subject_class_mapping_ui->Get_Subject_Selection("SubjectID".$i,$subject_id).$this->Get_Form_Warning_Msg("SubjectIDWarningDiv".$i,$Lang['ClassDiary']['WarningMsg']['SelectSubject'],"").'</td>';
			$x .= '<td>'.$this->Get_Class_Subject_Selection($ClassID,"SubjectID".$i,$subject_id,"",0,$Lang['General']['PleaseSelect']).$this->Get_Form_Warning_Msg("SubjectIDWarningDiv".$i,$Lang['ClassDiary']['WarningMsg']['SelectSubject'],"").'</td>';
			$x .= '<td>'.$this->Get_Checkbox("InstructionMediumP".$i,"InstructionMediumP[]","1",$LessonNumberToDetail[$i][0]['InstructionMediumP']==1, "", "", "checkmasterP.checked=false").$this->Get_Form_Warning_Msg("MediumWarningDiv".$i,$Lang['ClassDiary']['WarningMsg']['SelectAtLeastOneMediumOfInstruction'],"").'</td>';
			$x .= '<td>'.$this->Get_Checkbox("InstructionMediumE".$i,"InstructionMediumE[]","1",$LessonNumberToDetail[$i][0]['InstructionMediumE']==1, "", "", "checkmasterE.checked=false").'</td>';
			$x .= '<td>'.$this->Get_Checkbox("InstructionMediumC".$i,"InstructionMediumC[]","1",$LessonNumberToDetail[$i][0]['InstructionMediumC']==1, "", "", "checkmasterC.checked=false").'</td>';
			$x .= '<td>'.$this->Get_Checkbox("InstructionMediumB".$i,"InstructionMediumB[]","1",$LessonNumberToDetail[$i][0]['InstructionMediumB']==1, "", "", "checkmasterB.checked=false").'</td>';
			//$x .= '<td>'.$this->GET_TEXTBOX("Assignment".$i,"Assignment[]",$LessonNumberToDetail[$i][0]['Assignment'],"",$text_css).$this->Get_Form_Warning_Msg("AssignmentWarningDiv".$i,$Lang['ClassDiary']['WarningMsg']['InputAssignment'],"").'</td>';
			$x .= '<td>'.$this->GET_TEXTAREA("Assignment".$i,$LessonNumberToDetail[$i][0]['Assignment'],"50",$text_css).$this->Get_Form_Warning_Msg("AssignmentWarningDiv".$i,$Lang['ClassDiary']['WarningMsg']['InputAssignment'],"").'</td>';
			//$x .= '<td>'.$this->GET_DATE_PICKER("SubmissionDate".$i,$submission_date,"","yy-mm-dd","","","","","",0,1).$this->Get_Form_Warning_Msg("SubmissionDateWarningDiv".$i,$Lang['ClassDiary']['WarningMsg']['InvalidDate'],"").'</td>';
			$x .= '<td>';
			$field_id = "Student".RECORD_TYPE_FORGOT_BOOK.$i;
			$x .= '<a href="javascript:void(0);" onclick="ShowHideStudentPanel(this,1,\''.$field_id.'\');"><img width="20" height="20" border="0" align="absmiddle" alt="'.$Lang['ClassDiary']['SelectStudents'].'" title="'.$Lang['ClassDiary']['SelectStudents'].'" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif"></a>';
			$x .= '<div>'."\n";
			$students_id = array();
			if(isset($LessonIDToUser[$lesson_id]) && count($LessonIDToUser[$lesson_id][RECORD_TYPE_FORGOT_BOOK])>0){
				$students = $LessonIDToUser[$lesson_id][RECORD_TYPE_FORGOT_BOOK];
				$students_id = Get_Array_By_Key($students,'UserID');
				$x .= $this->Get_Student_Names_Block($students);
			}
			$x .= '</div>'."\n";
			$x .= '<input type="hidden" id="'.$field_id.'" name="Student'.RECORD_TYPE_FORGOT_BOOK.'[]" value="'.implode(",",$students_id).'" />';
			$x .= '</td>';
			
			$x .= '<td>';
			$field_id = 'Student'.RECORD_TYPE_EXCUSE_LESSON.$i;
			$x .= '<a href="javascript:void(0);" onclick="ShowHideStudentPanel(this,1,\''.$field_id.'\');"><img width="20" height="20" border="0" align="absmiddle" alt="'.$Lang['ClassDiary']['SelectStudents'].'" title="'.$Lang['ClassDiary']['SelectStudents'].'" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif"></a>';
			$x .= '<div>';
			$students_id = array();
			if(isset($LessonIDToUser[$lesson_id]) && count($LessonIDToUser[$lesson_id][RECORD_TYPE_EXCUSE_LESSON])>0){
				$students = $LessonIDToUser[$lesson_id][RECORD_TYPE_EXCUSE_LESSON];
				$students_id = Get_Array_By_Key($students,'UserID');
				$x .= $this->Get_Student_Names_Block($students);
			}
			$x .= '</div>';
			$x .= '<input type="hidden" id="'.$field_id.'" name="Student'.RECORD_TYPE_EXCUSE_LESSON.'[]" value="'.implode(",",$students_id).'" />';
			$x .= '</td>';
			
			$x .= '<td>';
			$field_id = 'Student'.RECORD_TYPE_NO_MEDIUM_INSTRUCTION.$i;
			$x .= '<a href="javascript:void(0);" onclick="ShowHideStudentPanel(this,1,\''.$field_id.'\');"><img width="20" height="20" border="0" align="absmiddle" alt="'.$Lang['ClassDiary']['SelectStudents'].'" title="'.$Lang['ClassDiary']['SelectStudents'].'" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif"></a>';
			$x .= '<div>';
			$students_id = array();
			if(isset($LessonIDToUser[$lesson_id]) && count($LessonIDToUser[$lesson_id][RECORD_TYPE_NO_MEDIUM_INSTRUCTION])>0){
				$students = $LessonIDToUser[$lesson_id][RECORD_TYPE_NO_MEDIUM_INSTRUCTION];
				$students_id = Get_Array_By_Key($students,'UserID');
				$x .= $this->Get_Student_Names_Block($students);
			}
			$x .= '</div>';
			$x .= '<input type="hidden" id="'.$field_id.'" name="Student'.RECORD_TYPE_NO_MEDIUM_INSTRUCTION.'[]" value="'.implode(",",$students_id).'" />';
			$x .= '</td>';
			
			$x .= '<td>'.$this->GET_TEXTBOX("Others".$i,"Others[]",$LessonNumberToDetail[$i][0]['Others'],"",$text_css).'</td>';
		
			$x .= '</tr>'."\n";
		}
		
		$x.='</table>'."\n";
		
		$x.='<div>';
		foreach($Lang['ClassDiary']['MediumOfInstructionMapping'] as $key => $val){
			$x.=$key.': '.$val;
			for($k=0;$k<10;$k++){
				$x.='&nbsp;';
			}
		}
		$x.='</div>';
		
		$x.= '<br />';
		
		$x.= '<div id="edit_class_attendance_btn" align="center">'.$this->GET_SMALL_BTN($Lang['ClassDiary']['Edit_Class_Attendance'],"button","jsShowEditAttendance(1)")."</div>";
		$x.= '<div id="discard_edit_class_attendance_btn" align="center" style="display:none">'.$this->GET_SMALL_BTN($Lang['ClassDiary']['Discard_Edit_Class_Attendance'],"button","jsShowEditAttendance(0)")."</div>";

		$x.= '<br /><table class="form_table_v30" id="edit_attendance_table" style="display:none">'."\n";
		$x.= '<col class="field_title">
			  <col class="field_c">
			  <tbody>'."\n";
		$x.= '<tr>'."\n";
			$x.='<td class="field_title">'.$Lang['ClassDiary']['Absentee'].'&nbsp;'.$Lang['ClassDiary']['AM'].'</td>';
			$x.='<td width="95%">';
			$x .= '<a href="javascript:void(0);" onclick="ShowHideStudentPanel(this,1,\'AbsentAMStudent\');"><img width="20" height="20" border="0" align="absmiddle" alt="'.$Lang['ClassDiary']['SelectStudents'].'" title="'.$Lang['ClassDiary']['SelectStudents'].'" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif"></a>';
			$x .= '<span>';
			$students_id = array();
			if(count($AbsentAMStudents)>0){
				$students_id = Get_Array_By_Key($AbsentAMStudents,'UserID');
				$x .= $this->Get_Student_Names_Block($AbsentAMStudents);
			}
			$x.= '</span>';
			$x.='<input type="hidden" id="AbsentAMStudent" name="AbsentAMStudent" value="'.implode(",",$students_id).'" />';
			$x.='</td>';
		$x.= '</tr>'."\n";
		
		$x.= '<tr>'."\n";
			$x.='<td class="field_title">'.$Lang['ClassDiary']['Absentee'].'&nbsp;'.$Lang['ClassDiary']['PM'].'</td>';
			$x.='<td width="95%">';
			$x .= '<a href="javascript:void(0);" onclick="ShowHideStudentPanel(this,1,\'AbsentPMStudent\');"><img width="20" height="20" border="0" align="absmiddle" alt="'.$Lang['ClassDiary']['SelectStudents'].'" title="'.$Lang['ClassDiary']['SelectStudents'].'" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif"></a>';
			$x .= '<span>';
			$students_id = array();
			if(count($AbsentPMStudents)>0){
				$students_id = Get_Array_By_Key($AbsentPMStudents,'UserID');
				$x .= $this->Get_Student_Names_Block($AbsentPMStudents);
			}
			$x.= '</span>';
			$x.='<input type="hidden" id="AbsentPMStudent" name="AbsentPMStudent" value="'.implode(",",$students_id).'" />';
			$x.='</td>';
		$x.= '</tr>'."\n";
		
		$x.= '<tr>'."\n";
			$x.='<td class="field_title">'.$Lang['ClassDiary']['Late'].'</td>';
			$x.='<td width="95%">';
			$x .= '<a href="javascript:void(0);" onclick="ShowHideStudentPanel(this,1,\'LateStudent\');"><img width="20" height="20" border="0" align="absmiddle" alt="'.$Lang['ClassDiary']['SelectStudents'].'" title="'.$Lang['ClassDiary']['SelectStudents'].'" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif"></a>';
			$x .= '<span>';
			$students_id = array();
			if(count($LateStudents)>0){
				$students_id = Get_Array_By_Key($LateStudents,'UserID');
				$x .= $this->Get_Student_Names_Block($LateStudents);
			}
			$x.='</span>';
			$x.='<input type="hidden" id="LateStudent" name="LateStudent" value="'.implode(",",$students_id).'" />';
			$x.='</td>';
		$x.= '</tr>'."\n";
		
		$x.= '<tr>'."\n";
			$x.='<td class="field_title">'.$Lang['ClassDiary']['EarlyLeave'].'</td>';
			$x.='<td width="95%">';
			$x .= '<a href="javascript:void(0);" onclick="ShowHideStudentPanel(this,1,\'EarlyleaveStudent\');"><img width="20" height="20" border="0" align="absmiddle" alt="'.$Lang['ClassDiary']['SelectStudents'].'" title="'.$Lang['ClassDiary']['SelectStudents'].'" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/iMail/btn_alias_group.gif"></a>';
			$x .= '<span>';
			$students_id = array();
			if(count($EarlyleaveStudents)>0){
				$students_id = Get_Array_By_Key($EarlyleaveStudents,'UserID');
				$x .= $this->Get_Student_Names_Block($EarlyleaveStudents);
			}
			$x.='</span>';
			$x.='<input type="hidden" id="EarlyleaveStudent" name="EarlyleaveStudent" value="'.implode(",",$students_id).'" />';
			$x.='</td>';
		$x.= '</tr>'."\n";
		
		$x.='</tbody>
			</table>'."\n";
		$x .= $this->MandatoryField();
		
		$x.='<input type="hidden" id="DiaryID" name="DiaryID" value="'.$DiaryID.'" />'."\n";
		
		
		$x .= $this->Get_Student_Selection_Panel("StudentPanel");
		
		# edit bottom
			$SubmitBtn = $this->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","CheckSubmitClassDiary();");
			if($is_edit){
				$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='classdiary_detail.php?DiaryID=".$DiaryID."'");
			}else{
				$BackBtn = $this->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location='index.php'");
			}
		$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SubmitBtn;
				$x .= "&nbsp;".$BackBtn;
		$x .= '</div>'."\n";
		
		$x.='<input type="hidden" name="edit_class_attendance" id="edit_class_attendance" value="0" />'."\n";
		$x.='</form>'."\n";
		
		return $x;
	}
	
	function Get_Student_Names_Block($StudentArray,$separator=', ')
	{
		$x = '';
		$delimiter = "";
		for($j=0;$j<count($StudentArray);$j++){
			$students_id[] = $StudentArray[$j]['UserID'];
			$x.= $delimiter.$StudentArray[$j]['StudentName'];
			if(trim($StudentArray[$j]['ClassNumber'])!=''){
				$x.= " (#".$StudentArray[$j]['ClassNumber'].")";
			}
			$delimiter = $separator;
		}
		return $x;
	}
	
	function Get_Student_Selection_Panel($id)
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		
		$x = '<div style="display:none;visibility:visible;width:400px;overflow:auto;position:absolute;z-index:900;" class="selectbox_layer" id="'.$id.'">'."\n";
		$x.= '<table width="100%" cellspacing="0" cellpadding="3" border="0">
				<tbody>
					<tr>
						<td align="right">
							<a href="javascript:ShowHideStudentPanel(this,0,\'\')" title="'.$Lang['Btn']['Close'].'">
								<img border="0" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif">
							</a>
						</td>
					</tr>
					<tr>
						<td align="left" style="border-bottom: medium none;">
							<div id="'.$id.'Detail" style="z-index: 999; overflow: auto; max-height:480px;"></div>
						</td>
					</tr>
				</tbody>
			  </table>'."\n";
		$x.= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Student_Selection_Table($classID,$fieldID,$dataArray=array(),$selectedIDArray=array())
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		
		$libclassdiary = $this->Get_libclassdiary();
		if(count($dataArray)==0){
			$students = $libclassdiary->Get_Students_By_ClassID($classID);
		}else{
			$students = $dataArray;
		}
		
		$x = '<table width="100%" cellspacing="0" cellpadding="0" border="0">'."\n";
			$x.= '<tbody>';
			$x.= '<tr class="tabletop">'."\n";
				$x.= '<td>'.$Lang['ClassDiary']['ClassNumber'].'</td>';
				$x.= '<td>'.$Lang['ClassDiary']['StudentName'].'</td>';
				$x.= '<td>'.$this->Get_Checkbox("CheckAllStudent","CheckAllStudent","1",false,'','','Check_All(\'Students[]\',this.checked);').'</td>';
			$x.= '</tr>'."\n";
		
		for($i=0;$i<count($students);$i++){
			$checked = in_array($students[$i]['UserID'],$selectedIDArray);
			
			$x.= '<tr class="row_approved">';
				$x .= '<td>'.$students[$i]['ClassNumber'].'</td>'."\n";
				$x .= '<td>'.$libclassdiary->html_special_chars($students[$i]['StudentName']).'</td>'."\n";
				$x .= '<td>'.$this->Get_Checkbox("Students[".$students[$i]['UserID']."]","Students[]",$students[$i]['UserID'],$checked).'</td>'."\n";
			$x.= '</tr>'."\n";
		}
		$x.= '<tr height="10px"><td nowrap="" style="border-bottom: medium none;" colspan="3"></td></tr>';
		$x.= '<tr>
				<td nowrap="" align="center" style="border-bottom: medium none;" colspan="3">';
		$x .= $this->GET_SMALL_BTN($Lang['Btn']['Apply'],"button","ApplyStudents('".$fieldID."');","ApplyBtn").'&nbsp;';
		$x .= $this->GET_SMALL_BTN($Lang['Btn']['Cancel'],"button","ShowHideStudentPanel(this,0,'');","CancelBtn");
		$x  .= '</td>
			  </tr>'."\n";
		
			$x.='</tbody>'."\n";
		$x.= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Class_Selection($attrib,$selected="",$optionSelect="", $optionFirst="", $AcademicYearID='')
	{
		global $button_select;
		
		if ($AcademicYearID == '')
			$AcademicYearID = Get_Current_Academic_Year_ID();
		
		$fcm = new form_class_manage();
		$ClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
		
		$x .= '<select '.$attrib.'>';
		if($optionSelect != 1)
		{
			$x .= "<OPTION value='' ".((sizeof($ClassList) > 0)? "":"selected").">".(($optionFirst=="")? "-- ".$button_select." --":$optionFirst)."</OPTION>\n";
		}
	  	for ($i=0; $i< sizeof($ClassList); $i++) {
	  		if ($ClassList[$i]['YearID'] != $ClassList[$i-1]['YearID']) {
	  			if ($i == 0) 
		  			$x .= '<optgroup label="'.htmlspecialchars($ClassList[$i]['YearName'],ENT_QUOTES).'">';
		  		else {
		  			$x .= '</optgroup>';
		  			$x .= '<optgroup label="'.htmlspecialchars($ClassList[$i]['YearName'],ENT_QUOTES).'">';
		  		}
	  		}
	  	
	  		unset($Selected);
	  		$Selected = ($ClassList[$i]['YearClassID'] == $selected)? 'selected':'';
	  		$x .= '<option value="'.$ClassList[$i]['YearClassID'].'" '.$Selected.'>'.Get_Lang_Selection($ClassList[$i]['ClassTitleB5'],$ClassList[$i]['ClassTitleEN']).'</option>';
	  	
	  		if ($i == (sizeof($ClassList)-1)) 
	  			$x .= '</optgroup>';
	    }
	   	$x .= '</select>';
	                  
	  	return $x;
	}
	
	function Get_Class_Subject_Selection($YearClassID, $ID_Name, $Selected='', $OnChange='', $noFirst='0', $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=1, $IsMultiple=0, $IncludeSubjectIDArr='')
	{
		global $Lang;
		
		if ($OnFocus != '')
			$onfocus = 'onfocus="'.$OnFocus.'"';
		
		if ($OnChange != '')
			$onchange = 'onchange="'.$OnChange.'"';
			
		if ($IsMultiple == 1)
			$multiple = 'multiple="1" size="10"';
			
		# Use current term if not specified
		if ($YearTermID == '')
		{
			$YearTermArr = getCurrentAcademicYearAndYearTerm();
			$YearTermID = $YearTermArr['YearTermID'];
		}
		
		$libclassdiary = $this->Get_libclassdiary();
		$currentAcademicYearID = $libclassdiary->Get_Current_Academic_Year_ID();
		
		$libLC = new Learning_Category();
		$LC_Arr = $libLC->Get_All_Learning_Category();
		$numOfLC = count($LC_Arr);
		//debug_pr($LC_Arr);
		
		$libSubject = new subject();
		$SubjectArr = $libSubject->Get_Subject_List(1);
		//debug_pr($SubjectArr);
		
		$subject_class_mapping = new subject_class_mapping();
		$subject_id_and_count = $subject_class_mapping->Get_Number_Of_Subject_Taken($YearClassID,$currentAcademicYearID,true);
		$subject_id_list = Get_Array_By_Key($subject_id_and_count,'SubjectID');
		
		//debug_r($subject_id_and_count);
		//debug_r($subject_id_list);
		
		$select = '';
		$select .= '<select name="'.$ID_Name.'" id="'.$ID_Name.'" class="formtextbox" '.$onchange.' '.$onfocus.' '.$multiple.'>';
			
		if (!$noFirst)
			$select .= '<option value="">'.Get_Selection_First_Title($firstTitle).'</option>';
			
		$scm = new subject_class_mapping();
		$SubjectGroupInfoArr = $scm->Get_Subject_Group_Info();

		if($FilterSubjectWithoutSG==1)
		{
			$AllSubjectGroupInfoArr = $scm->Get_Subject_Group_List($YearTermID);
			$AllSubjectGroupInfoArr = BuildMultiKeyAssoc($AllSubjectGroupInfoArr['SubjectGroupList'], array("SubjectID","SubjectGroupID"));
		}
		
		$subjectCounter = 1;
		if($YearClassID!=''){
			for ($i=0; $i<$numOfLC; $i++)
			{
				$thisLC_ID = $LC_Arr[$i]['LearningCategoryID'];
				$thisLC_Name = Get_Lang_Selection($LC_Arr[$i]['NameChi'], $LC_Arr[$i]['NameEng']);
				$thisSubjectArr = $SubjectArr[$thisLC_ID];
		
				if (!is_array($thisSubjectArr) || count($thisSubjectArr) == 0) continue;
				
	//			$select .= '<optgroup label="'.$thisLC_Name.'">';
	
				$options = '';
				foreach($thisSubjectArr as $thisSubjectID => $thisSubjectInfoArr)
				{
					if(!in_array($thisSubjectID,$subject_id_list)){
						continue;
					}
					if (is_array($IncludeSubjectIDArr) && !in_array($thisSubjectID, $IncludeSubjectIDArr))
						continue;
					
	//				$SubjectGroupInfoArr = $scm->Get_Subject_Group_List($YearTermID,$thisSubjectID);
	//				$SubjectGroupArr = $SubjectGroupInfoArr['SubjectGroupList'];
					
					# cannot select subject which do not have subject group
					if ($FilterSubjectWithoutSG==1)
					{
						$SubjectGroupArr = $AllSubjectGroupInfoArr[$thisSubjectID];
						if(count($SubjectGroupArr) == 0)
							continue;
					}	
					
					$thisNameChi = intranet_htmlspecialchars($thisSubjectInfoArr['CH_DES']);
					$thisNameEng = intranet_htmlspecialchars($thisSubjectInfoArr['EN_DES']);
					$thisSubjectName = Get_Lang_Selection($thisNameChi, $thisNameEng);
					
					$select_tag = "";
					if ($thisSubjectID == $Selected || ($subjectCounter==1 && $noFirst))
						$select_tag = "selected";
						
					$options .= '<option value="'.$thisSubjectID.'" '.$select_tag.'>'.$thisSubjectName.'</option>';
					$subjectCounter++;
				}
				if(trim($options)!='')
				{
					$select .= '<optgroup label="'.$thisLC_Name.'">';
						$select .= $options;
					$select .= '</optgroup>';
				}
			}
		}
		$select .= '</select>';
		
		return $select;
	}
	
	function Get_Class_Diary_Print_Form($DiaryID)
	{
		global $Lang, $LAYOUT_SKIN, $PATH_WRT_ROOT;
		include_once("libcycleperiods.php");
		
		$libcycleperiods = new libcycleperiods();
		$libclassdiary = $this->Get_libclassdiary();
		$DiaryDetail = $libclassdiary->Get_Class_Diary_Lessons($DiaryID, $IsAll=true);
		$LessonNumberToDetail = $DiaryDetail['LessonNumberToDetail'];
		$LessonIDToUser = $DiaryDetail['LessonIDToUser'];
		$AbsentAMStudents = $DiaryDetail['AbsentAMStudents'];
		$AbsentPMStudents = $DiaryDetail['AbsentPMStudents'];
		$LateStudents = $DiaryDetail['LateStudents'];
		$EarlyleaveStudents = $DiaryDetail['EarlyleaveStudents'];
		$CycleDay = $libcycleperiods->getCycleDayStringByDate($DiaryDetail['DiaryDate']);
		
		$x = $this->Include_JS_CSS();
		
		$x .= '<table width="98%" align="center" class="print_hide" border="0">
					<tr>
						<td align="right">'.$this->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</td>
					</tr>
				</table>
			   <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td align="center" style="text-align:center;font-weight:bold;">'.$libclassdiary->html_special_chars(GET_SCHOOL_NAME()).'</td>
					</tr>
				 	<tr>
						<td align="center" style="text-align:center;font-weight:bold;">'.$Lang['ClassDiary']['ClassDiary'].'</td>
				 	</tr>
			   </table>'."\n";
		
		$x.= '<table width="98%" align="center" border="0">'."\n";
		$x.= '<tbody>'."\n";
		$x.= '<tr>'."\n";
			$x.= '<td align="left">'.$Lang['ClassDiary']['Class'].':&nbsp;<u>'.$libclassdiary->html_special_chars($DiaryDetail['ClassName']).'</u></td>';
			$x.= '<td align="center">'.$Lang['ClassDiary']['Cycle'].':&nbsp;<u>'.$CycleDay.'</u></td>'."\n"; 
			$x.= '<td align="right">'.$Lang['ClassDiary']['Date'].':&nbsp;<u>'.$DiaryDetail['DiaryDate'].'</u></td>';
		$x.= '</tr>'."\n";
		$x.='</tbody>
			</table>';
		
		$x.='<br style="clear:both;"/>';
		
		$x.='<table width="98%" cellspacing="0" cellpadding="5" border="0" align="center" class="eSporttableborder">';
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th rowspan="2" style="width:5%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['Lesson'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width:10%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['Subject'].'</th>'."\n";
					$x .= '<th colspan="4" style="width:10%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['MediumOfInstruction'].'*</th>'."\n";
					$x .= '<th rowspan="2" style="width:15%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['Assignment'].'</th>'."\n";
					//$x .= '<th rowspan="2" style="width:10%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['DateOfSubmission'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width:10%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['ForgotToBringBooks'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width:10%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['StudentsExcuseLessons'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width:10%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width:10%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['Others'].'</th>'."\n";
					$x .= '<th rowspan="2" style="width:10%;text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$Lang['ClassDiary']['TeachersSignature'].'</th>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>';
				foreach($Lang['ClassDiary']['MediumOfInstructionMapping'] as $key => $val){
					$x .= '<th style="text-align:center;" class="eSporttdborder eSportprinttabletitle">'.$key.'</th>';
				}
				$x .= '</tr>';	
			$x .= '</thead>'."\n";
		$x.='</tr>';
		
		for($i=1;$i<=$libclassdiary->MAX_NUMBER_OF_LESSON;$i++) {			
			if(isset($LessonNumberToDetail[$i])){
				for ($j=0; $j<sizeof($LessonNumberToDetail[$i]); $j++)
				{
					$ThisLessonNumberToDetail = $LessonNumberToDetail[$i][$j];
					$libuser = new libuser($ThisLessonNumberToDetail['InputBy']);
					$TeacherName = $libuser->UserName(0);
					$lesson_id = $ThisLessonNumberToDetail['LessonID'];
					$x .= '<tr>';
					$x .= '<td class="eSporttdborder eSportprinttext">'.(($j>0)?"&nbsp;":$i).'</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">'.$libclassdiary->html_special_chars($ThisLessonNumberToDetail['Subject']).' <br />('.$TeacherName.')</td>';
				//$lesson_id = $ThisLessonNumberToDetail['LessonID'];
				//$x .= '<td class="eSporttdborder eSportprinttext">'.$libclassdiary->html_special_chars($ThisLessonNumberToDetail['Subject']).'</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">'.($ThisLessonNumberToDetail['InstructionMediumP']==1? '<img width="20" height="20" border="0" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_tick_green.gif">' :'&nbsp;').'</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">'.($ThisLessonNumberToDetail['InstructionMediumE']==1? '<img width="20" height="20" border="0" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_tick_green.gif">' :'&nbsp;').'</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">'.($ThisLessonNumberToDetail['InstructionMediumC']==1? '<img width="20" height="20" border="0" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_tick_green.gif">' :'&nbsp;').'</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">'.($ThisLessonNumberToDetail['InstructionMediumB']==1? '<img width="20" height="20" border="0" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_tick_green.gif">' :'&nbsp;').'</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">'.nl2br($libclassdiary->html_special_chars($ThisLessonNumberToDetail['Assignment'])).'</td>';
					//$x .= '<td class="eSporttdborder eSportprinttext">'.$libclassdiary->html_special_chars($ThisLessonNumberToDetail['SubmissionDate']).'</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">';
					if(isset($LessonIDToUser[$lesson_id]) && count($LessonIDToUser[$lesson_id][RECORD_TYPE_FORGOT_BOOK])>0){
						$students = $LessonIDToUser[$lesson_id][RECORD_TYPE_FORGOT_BOOK];
						$x .= $this->Get_Student_Names_Block($students,"<br />");
					}else{
						$x .= '&nbsp;';
	 				}
					$x .= '</td>';
					
					$x .= '<td class="eSporttdborder eSportprinttext">';
					if(isset($LessonIDToUser[$lesson_id]) && count($LessonIDToUser[$lesson_id][RECORD_TYPE_EXCUSE_LESSON])>0){
						$students = $LessonIDToUser[$lesson_id][RECORD_TYPE_EXCUSE_LESSON];
						$x .= $this->Get_Student_Names_Block($students,"<br />");
					}else{
						$x .= '&nbsp;';
	 				}
					$x .= '</td>';
					
					$x .= '<td class="eSporttdborder eSportprinttext">';
					if(isset($LessonIDToUser[$lesson_id]) && count($LessonIDToUser[$lesson_id][RECORD_TYPE_NO_MEDIUM_INSTRUCTION])>0){
						$students = $LessonIDToUser[$lesson_id][RECORD_TYPE_NO_MEDIUM_INSTRUCTION];
						$x .= $this->Get_Student_Names_Block($students,"<br />");
					}else{
						$x .= '&nbsp;';
	 				}
					$x .= '</td>';
					
					$x .= '<td class="eSporttdborder eSportprinttext">'.$libclassdiary->html_special_chars($ThisLessonNumberToDetail['Others'],true,'&nbsp;').'</td>';
					$x .= '<td class="eSporttdborder eSportprinttext">&nbsp;</td>';
					$x .= '</tr>';
				}
			}else{
				$x .= '<tr>';
				$x .= '<td class="eSporttdborder eSportprinttext">'.$i.'</td>';
				for($j=1;$j<=11;$j++){
					$x .= '<td class="eSporttdborder eSportprinttext">-</td>';
				}
				$x .= '</tr>';
			}
		}
		
		$x.='</table>';
		
		$x.='<table width="98%" align="center">';
		$x.='<tbody>
				<tr><td>';
		$x.='* ';
		foreach($Lang['ClassDiary']['MediumOfInstructionMapping'] as $key => $val){
			$x.=$key.': '.$val;
			for($k=0;$k<10;$k++){
				$x.='&nbsp;';
			}
		}
		$x.='</td></tr>';
		$x.='</table>';
		
		$x.= '<br />';
		
		$x.= '<table width="98%" align="center">'."\n";
		$x.= '<tbody>'."\n";
		$x.= '<tr>'."\n";
			$x.='<td width="1%" nowrap style="vertical-align:top;">'.$Lang['ClassDiary']['Absentee'].'&nbsp;'.$Lang['ClassDiary']['AM'].':</td>';
			$x.='<td width="24%" style="text-align:left;vertical-align:top;border-bottom:solid 1px black;">';
			if(count($AbsentAMStudents)>0){
				$x .= $this->Get_Student_Names_Block($AbsentAMStudents,"<br />");
			}else{
				$x .= '&nbsp;';
			}
			$x.='</td>';
			$x.='<td width="1%" style="text-align:right;vertical-align:top;" nowrap>'.$Lang['ClassDiary']['PM'].':</td>';
			$x.='<td width="24%" style="text-align:left;vertical-align:top;;border-bottom:solid 1px black;">';
			if(count($AbsentPMStudents)>0){
				$x .= $this->Get_Student_Names_Block($AbsentPMStudents,"<br />");
			}else{
				$x .= '&nbsp;';
			}
			$x.='</td>';
			$x.='<td width="1%" style="text-align:right;vertical-align:top;" nowrap>'.$Lang['ClassDiary']['Late'].':</td>';
			$x.='<td width="24%" style="text-align:left;vertical-align:top;border-bottom:solid 1px black;">';
			if(count($LateStudents)>0){
				$x .= $this->Get_Student_Names_Block($LateStudents,"<br />");
			}else{
				$x .= '&nbsp;';
			}
			$x.='</td>';
			$x.='<td width="1%" style="text-align:right;vertical-align:top;" nowrap>'.$Lang['ClassDiary']['EarlyLeave'].':</td>';
			$x.='<td width="24%" style="text-align:left;vertical-align:top;border-bottom:solid 1px black;">';
			if(count($EarlyleaveStudents)>0){
				$x .= $this->Get_Student_Names_Block($EarlyleaveStudents,"<br />");
			}else{
				$x .= '&nbsp;';
			}
			$x.='</td>';
		$x.= '</tr>'."\n";
		
		$x .= '<tr>'."\n";
		$x .= '<td width="10%">'.$Lang['ClassDiary']['Remarks'].':</td>'."\n";
		$x .= '<td width="90%" colspan="7" style="text-align:left;border-bottom:solid 1px black;">&nbsp;</td>';
		
		$x .= '</tr>'."\n";
		
		$x .= '<tr>'."\n";
		$x .= '<td width="10%" style="text-align:left;" nowrap>'.$Lang['ClassDiary']['SignatureOfClassMonitor'].':</td>'."\n";
		$x .= '<td width="40%" colspan="3" style="text-align:left;border-bottom:solid 1px black;">&nbsp;</td>'."\n";
		$x .= '<td width="10%" style="text-align:right;" nowrap>'.$Lang['ClassDiary']['SignatureOfClassTeacher'].':</td>'."\n";
		$x .= '<td width="40%" colspan="3" style="text-align:left;border-bottom:solid 1px black;">&nbsp;</td>'."\n";
		$x .= '</tr>'."\n";
		
		$x .= '<tr>'."\n";
		$x .= '<td style="vertical-align:top;text-align:left;">'.$Lang['ClassDiary']['Remarks'].':</td>'."\n";
		$x .= '<td colspan="7" style="text-align:left;">';
		$x .= '<ol>';
		foreach($Lang['ClassDiary']['RemarksDescription'] as $key=>$val){
			$x.='<li>'.$val.'</li>';
		}
		$x .= '</ol>'."\n";
		$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		$x.='</tbody>
			</table>';
		
		return $x;
	}
	
	function Get_Report_Statistics_Index()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$x .= $this->Include_JS_CSS();
		
		$form_class_manage_ui = new form_class_manage_ui();
		$libclassdiary = $this->Get_libclassdiary();
		$class_selection = $form_class_manage_ui->Get_Class_Selection($libclassdiary->Get_Current_Academic_Year_ID(),'','ClassID[]','','',1,1);
		
		$select_all_btn = '&nbsp;'.$this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID[]',true);return false;");
		$start_date = date("Y-m-d",getStartOfAcademicYear());
		$end_date = date("Y-m-d",getEndOfAcademicYear());
		
		$record_type_select = '<input type="checkbox" id="RecordTypeF" name="RecordType[]" value="'.RECORD_TYPE_FORGOT_BOOK.'" checked="checked" /><label for="RecordTypeF">'.$Lang['ClassDiary']['ForgotToBringBooks'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeE" name="RecordType[]" value="'.RECORD_TYPE_EXCUSE_LESSON.'" checked="checked" /><label for="RecordTypeE">'.$Lang['ClassDiary']['StudentsExcuseLessons'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeM" name="RecordType[]" value="'.RECORD_TYPE_NO_MEDIUM_INSTRUCTION.'" checked="checked" /><label for="RecordTypeM">'.$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeAbsentAM" name="RecordType[]" value="'.RECORD_TYPE_ABSENT_AM.'" checked="checked" /><label for="RecordTypeAbsentAM">'.$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['AM'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeAbsentPM" name="RecordType[]" value="'.RECORD_TYPE_ABSENT_PM.'" checked="checked" /><label for="RecordTypeAbsentPM">'.$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['PM'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeLate" name="RecordType[]" value="'.RECORD_TYPE_LATE.'" checked="checked" /><label for="RecordTypeLate">'.$Lang['ClassDiary']['Late'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeEarlyleave" name="RecordType[]" value="'.RECORD_TYPE_EARLYLEAVE.'" checked="checked" /><label for="RecordTypeEarlyleave">'.$Lang['ClassDiary']['EarlyLeave'].'</label><br />';
		
		$x .= '<div id="ReportDiv">';
			$x .= '<div class="table_board">
					<table class="form_table">
						<tbody>
							<tr>
								<td class="field_title">'.$Lang['ClassDiary']['Class'].' <span class="tabletextrequire">*</span></td>
								<td>'.$class_selection.$select_all_btn.'<div id="ClassIDWarningDiv" style="color:red;display:none;"></div></td>
							</tr>
							<tr>
						        <td class="field_title">'.$Lang['ClassDiary']['Date'].'</td>
						        <td>
									<span>'.$Lang['General']['From'].' '.$this->GET_DATE_PICKER("StartDate", $start_date).'</span>
									<span>'.$Lang['General']['To'].' '.$this->GET_DATE_PICKER("EndDate", $end_date).'</span>
									<div id="DateWarningLayer" style="color:red;display:none;"></div>
						        </td>
						      </tr>
							  <tr>
								<td class="field_title">'.$Lang['ClassDiary']['CountTimesOfRecordType'].'</td>
								<td>'.$record_type_select.'<div id="RecordTypeWarningDiv" style="color:red;display:none;"></div></td>
							  </tr>
							  <tr>
								<td class="field_title">'.$Lang['ClassDiary']['ShowDates'].'</td>
								<td><input type="checkbox" id="ShowDates" name="ShowDates" value="1" /><label for="ShowDates">&nbsp;</label></td>
							  </tr>
						    </tbody>
							<col class="field_title"/>
						    <col class="field_c"/>
						</table>
						<p class="spacer"/>
					</div>'."\n";
						
			$x .= '<div class="tabletextremark">&nbsp; '.$Lang['General']['RequiredField'].'</div><div class="edit_bottom_v30" style="width:95%;">
						<p class="spacer"/>
							<input type="button" value="'.$Lang['Btn']['Generate'].'" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" onclick="Generate_Report();" class="formbutton" name="submit2"/>
							<input type="button" value="'.$Lang['Btn']['Cancel'].'" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" onclick="window.location=\'index.php\'" class="formbutton" name="submit3"/>
						<p class="spacer"/>
					</div>';
			
			$x .= '<div id="ReportLayer">';
				
			$x .= '</div>';		
		$x .= '</div>';
		
		//$libclassdiary->Get_Class_Diary_Student_Lessons('1711',$start_date,$end_date);
		
		return $x;
	}
	
	
	function Get_Report_Export_Mail_Merge()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		$x .= $this->Include_JS_CSS();
		
		$form_class_manage_ui = new form_class_manage_ui();
		$libclassdiary = $this->Get_libclassdiary();
		$class_selection = $form_class_manage_ui->Get_Class_Selection($libclassdiary->Get_Current_Academic_Year_ID(),'','ClassID[]','','',1,1);
		
		$select_all_btn = '&nbsp;'.$this->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('ClassID[]',true);return false;");
		//$start_date = date("Y-m-d",getStartOfAcademicYear());
		$start_date = date("Y-m-d", mktime(0, 0, 0, date("m") , 1, date("Y")));
		//$end_date = date("Y-m-d",getEndOfAcademicYear());
		$end_date = date("Y-m-d", mktime(0, 0, 0, date("m")+1 , 0, date("Y")));
		
		/*
		$record_type_select = '<input type="checkbox" id="RecordTypeF" name="RecordType[]" value="'.RECORD_TYPE_FORGOT_BOOK.'" checked="checked" /><label for="RecordTypeF">'.$Lang['ClassDiary']['ForgotToBringBooks'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeE" name="RecordType[]" value="'.RECORD_TYPE_EXCUSE_LESSON.'" checked="checked" /><label for="RecordTypeE">'.$Lang['ClassDiary']['StudentsExcuseLessons'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeM" name="RecordType[]" value="'.RECORD_TYPE_NO_MEDIUM_INSTRUCTION.'" checked="checked" /><label for="RecordTypeM">'.$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeAbsentAM" name="RecordType[]" value="'.RECORD_TYPE_ABSENT_AM.'" checked="checked" /><label for="RecordTypeAbsentAM">'.$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['AM'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeAbsentPM" name="RecordType[]" value="'.RECORD_TYPE_ABSENT_PM.'" checked="checked" /><label for="RecordTypeAbsentPM">'.$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['PM'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeLate" name="RecordType[]" value="'.RECORD_TYPE_LATE.'" checked="checked" /><label for="RecordTypeLate">'.$Lang['ClassDiary']['Late'].'</label><br />';
		$record_type_select.= '<input type="checkbox" id="RecordTypeEarlyleave" name="RecordType[]" value="'.RECORD_TYPE_EARLYLEAVE.'" checked="checked" /><label for="RecordTypeEarlyleave">'.$Lang['ClassDiary']['EarlyLeave'].'</label><br />';
		*/
		$x .= '<form id="form1" name="form1" method="post" action="export_mail_merge_csv.php"><div id="ReportDiv">';
			$x .= '<div class="table_board">
					<table class="form_table">
						<tbody>
							<tr>
								<td class="field_title">'.$Lang['ClassDiary']['Class'].' <span class="tabletextrequire">*</span></td>
								<td>'.$class_selection.$select_all_btn.'<div id="ClassIDWarningDiv" style="color:red;display:none;"></div></td>
							</tr>
							<tr>
						        <td class="field_title">'.$Lang['ClassDiary']['Date'].'</td>
						        <td>
									<span>'.$Lang['General']['From'].' '.$this->GET_DATE_PICKER("StartDate", $start_date).'</span>
									<span>'.$Lang['General']['To'].' '.$this->GET_DATE_PICKER("EndDate", $end_date).'</span>
									<div class="tabletextremark">'.$Lang['ClassDiary']['WarningMsg']['FirstMonth'].'</div>
									<div id="DateWarningLayer" style="color:red;display:none;"></div>
						        </td>
						      </tr>
							  <!--<tr>
								<td class="field_title">'.$Lang['ClassDiary']['CountTimesOfRecordType'].'</td>
								<td>'.$record_type_select.'<div id="RecordTypeWarningDiv" style="color:red;display:none;"></div></td>
							  </tr>
							  <tr>
								<td class="field_title">'.$Lang['ClassDiary']['ShowDates'].'</td>
								<td><input type="checkbox" id="ShowDates" name="ShowDates" value="1" /><label for="ShowDates">&nbsp;</label></td>
							  </tr>-->
						    </tbody>
							<col class="field_title"/>
						    <col class="field_c"/>
						</table>
						<p class="spacer"/>
					</div>'."\n";
						
			$x .= '<div class="tabletextremark">&nbsp; '.$Lang['General']['RequiredField'].'</div><div class="edit_bottom_v30" style="width:95%;">
						<p class="spacer"/>
							<input type="button" value="'.$Lang['Btn']['Generate'].'" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" onclick="Generate_Report();" class="formbutton" name="submit2"/>
							<input type="button" value="'.$Lang['Btn']['Cancel'].'" onmouseout="this.className=\'formbutton\'" onmouseover="this.className=\'formbuttonon\'" onclick="window.location=\'index.php\'" class="formbutton" name="submit3"/>
						<p class="spacer"/>
					</div>';
			
			$x .= '<div id="ReportLayer">';
				
			$x .= '</div>';		
		$x .= '</div></form>';
		
		//$libclassdiary->Get_Class_Diary_Student_Lessons('1711',$start_date,$end_date);
		
		return $x;
	}
	
	function Get_Report_Statistics_Table($StartDate,$EndDate,$ClassIDArray,$ClassNameArray,$RecordTypeArray,$ShowDates=false, $ShowEmpty=false)
	{
		global $Lang;
		
		$libclassdiary = $this->Get_libclassdiary();
		$column_cnt = count($RecordTypeArray);
		$column_width = sprintf("%d", 90 / $column_cnt);
		$TypeToRecordName = array(RECORD_TYPE_FORGOT_BOOK=>$Lang['ClassDiary']['ForgotToBringBooks'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_EXCUSE_LESSON=>$Lang['ClassDiary']['StudentsExcuseLessons'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_NO_MEDIUM_INSTRUCTION=>$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_ABSENT_AM=>$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['AM'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_ABSENT_PM=>$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['PM'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_LATE=>$Lang['ClassDiary']['Late'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_EARLYLEAVE=>$Lang['ClassDiary']['EarlyLeave'].'('.$Lang['ClassDiary']['Times'].')'
							);
		
		$x = '';
		for($i=0;$i<count($ClassIDArray);$i++){
			$class_name = rawurldecode(stripslashes($ClassNameArray[$i]));
			$students = $libclassdiary->Get_Students_By_ClassID($ClassIDArray[$i]);
			$studentIDArray = Get_Array_By_Key($students,'UserID');
			$result = $libclassdiary->Count_Student_Records($studentIDArray,$StartDate,$EndDate,$RecordTypeArray);
			
			$x .= '<div>';
				$x .= '<span style="float:left;">'.$Lang['ClassDiary']['Class'].':&nbsp;'.$class_name.'</span>';
				$x .= '<span style="float:right;">'.$Lang['ClassDiary']['Date'].':&nbsp;'.$Lang['General']['From'].'&nbsp;'.$StartDate.'&nbsp;'.$Lang['General']['To'].'&nbsp;'.$EndDate.'</span>';
			$x .= '</div>';
			$x .= '<table class="common_table_list_v30">'."\n";
				$x .= '<col align="left" style="width: 10px">'."\n";
				$x .= '<col align="left" style="width: 9%;">'."\n";
				foreach($RecordTypeArray as $record_type){
					$x .= '<col align="left" style="width: '.$column_width.'%">'."\n";
				}
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['ClassNumber'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['StudentName'].'</th>'."\n";
					foreach($RecordTypeArray as $record_type){
						$x .= '<th>'.$TypeToRecordName[$record_type].'</th>'."\n";
					}
				$x .= '</tr>'."\n";	
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
			
			if(count($students)==0){
				$x .= '<tr><td colspan="'.($column_cnt+2).'" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
			}
			
			for($j=0;$j<count($students);$j++) {
				if (!$ShowEmpty)
				{
					$IsRowShow = false;
					foreach($RecordTypeArray as $record_type){
						if(isset($result[$students[$j]['UserID']][$record_type])){
							$cnt = count($result[$students[$j]['UserID']][$record_type]);
							if ($cnt>0)
							{
								$IsRowShow = true;
							}
						}
					}
				} else
				{
					$IsRowShow = true;
				}
				if ($IsRowShow)
				{
					$x .= '<tr>';
					$x .= '<td>'.$students[$j]['ClassNumber'].'</td>'."\n";
					$x .= '<td>'.$students[$j]['StudentName'].'</td>'."\n";
					foreach($RecordTypeArray as $record_type){
						if(isset($result[$students[$j]['UserID']][$record_type])){
							$x .= '<td>';
							$cnt = count($result[$students[$j]['UserID']][$record_type]);
							$x .= $cnt;
							if($cnt>0 && $ShowDates){
								$x .= '<br />('.implode(",<br />",$result[$students[$j]['UserID']][$record_type]).')';
							}
							$x .= '</td>';
						}else{
							$x .= '<td>0</td>';
						}
					}
					$x .= '</tr>'."\n";
				}
			}
			
			$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br />';
		}
		
		return $x;
	}
	
	
	function Get_Report_Export_Mail_Merge_CSV($StartDate,$EndDate,$ClassIDArray,$ClassNameArray,$RecordTypeArray,$ShowDates=false, $ShowEmpty=false)
	{
		global $Lang, $PATH_WRT_ROOT;
		
		
		$libclassdiary = $this->Get_libclassdiary();
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		
		// determine start date from academic_year regarding to given startdate 
		$sql = "select 
							AcademicYearID
						From 
							ACADEMIC_YEAR_TERM 
						Where 
							UNIX_TIMESTAMP(TermStart) <= UNIX_TIMESTAMP('".$StartDate."') AND UNIX_TIMESTAMP(TermEnd) >= UNIX_TIMESTAMP('".$StartDate."') ";
		$rows = $libclassdiary->returnArray($sql);
		if (sizeof($rows)>0)
		{
			$academicYearObj = new academic_year($rows[0][0]);
			$StartDateOfYear = $academicYearObj->AcademicYearStart;
		} else
		{
			echo "There is no corresponding academic year and term set in the period from {$StartDate} to {$EndDate}. Please set them in school settings and then try again.";
			die();
		}

		$month_str = $Lang['ClassDiary']['Month'][date("n", strtotime($StartDate))];
		$column_cnt = count($RecordTypeArray);
		$column_width = sprintf("%d", 90 / $column_cnt);
		$TypeToRecordName = array(RECORD_TYPE_FORGOT_BOOK=>$Lang['ClassDiary']['ForgotToBringBooks'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_EXCUSE_LESSON=>$Lang['ClassDiary']['StudentsExcuseLessons'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_NO_MEDIUM_INSTRUCTION=>$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_ABSENT_AM=>$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['AM'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_ABSENT_PM=>$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['PM'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_LATE=>$Lang['ClassDiary']['Late'].'('.$Lang['ClassDiary']['Times'].')',
							RECORD_TYPE_EARLYLEAVE=>$Lang['ClassDiary']['EarlyLeave'].'('.$Lang['ClassDiary']['Times'].')'
							);
		
		$return_arr[] = array("class","classnumber", "name",
							"month", 
							"nil_bring", "nil_bring_date", 
							"nil_submit", "nil_submit_date",
							"allow_abs", "allow_abs_date",
							"improper_lang", "improper_lang_date",
							"am_abs", "am_abs_date",
							"pm_abs", "pm_abs_date",
							"late", "late_date",
							"early_leave", "early_leave_date",
							"total_nil_submit", "total_nil_bring", "total_late", "total_abs");
										
		for($i=0;$i<count($ClassIDArray);$i++)
		{
			//$class_name = rawurldecode(stripslashes($ClassNameArray[$i]));
			$yearClassObj = new year_class();
			$yearClassObj->Get_Year_Class_Info($ClassIDArray[$i]);
			$class_name = $yearClassObj->ClassTitleEN;
			$students = $libclassdiary->Get_Students_By_ClassID($ClassIDArray[$i]);
			$studentIDArray = Get_Array_By_Key($students,'UserID');
			$result = $libclassdiary->Count_Student_Records($studentIDArray,$StartDate,$EndDate,$RecordTypeArray, "F", True);
			
			# to do: count for accumulated records starting from first day of school year and given end date			
			$result_accumulated =  $libclassdiary->Count_Student_Records($studentIDArray,$StartDateOfYear,$EndDate,$RecordTypeArray, "", True);
			
			for($j=0;$j<count($students);$j++) {
				if (!$ShowEmpty)
				{
					$IsRowShow = false;
					foreach($RecordTypeArray as $record_type){
						if(isset($result[$students[$j]['UserID']][$record_type])){
							$cnt = count($result[$students[$j]['UserID']][$record_type]);
							if ($cnt>0)
							{
								$IsRowShow = true;
							}
						}
					}
				} else
				{
					$IsRowShow = true;
				}

				if (true || $IsRowShow)
				{
					//debug_r($result[$students[$j]['UserID']]);
					$cnt["nil_bring"] = count($result[$students[$j]['UserID']]["F"]);
					$record_arr["nil_bring"] = ($cnt["nil_bring"]>0) ? implode(",<br /> ",$result[$students[$j]['UserID']]["F"]) : "N/A";
					$cnt["nil_submit"] = count($result[$students[$j]['UserID']]["EH"]);
					$record_arr["nil_submit"] = ($cnt["nil_submit"]>0) ? implode(",<br /> ",$result[$students[$j]['UserID']]["EH"]) : "N/A";
					$cnt["allow_abs"] = count($result[$students[$j]['UserID']]["E"]);
					$record_arr["allow_abs"] = ($cnt["allow_abs"]>0) ? implode(", ",$result[$students[$j]['UserID']]["E"]) : "N/A";
					$cnt["improper_lang"] = count($result[$students[$j]['UserID']]["M"]);
					$record_arr["improper_lang"] = ($cnt["improper_lang"]>0) ? implode(", ",$result[$students[$j]['UserID']]["M"]) : "N/A";
					$cnt["am_abs"] = count($result[$students[$j]['UserID']][1]);
					$record_arr["am_abs"] = ($cnt["am_abs"]>0) ? implode(", ",$result[$students[$j]['UserID']][1]) : "N/A";
					$cnt["pm_abs"] = count($result[$students[$j]['UserID']][2]);
					$record_arr["pm_abs"] = ($cnt["pm_abs"]>0) ? implode(", ",$result[$students[$j]['UserID']][2]) : "N/A";
					$cnt["late"] = count($result[$students[$j]['UserID']][3]);
					$record_arr["late"] = ($cnt["late"]>0) ? implode(", ",$result[$students[$j]['UserID']][3]) : "N/A";
					$cnt["early_leave"] = count($result[$students[$j]['UserID']][4]);
					$record_arr["early_leave"] = ($cnt["early_leave"]>0) ? implode(", ",$result[$students[$j]['UserID']][4]) : "N/A";
					
					$cnt["total_nil_bring"] = count($result_accumulated[$students[$j]['UserID']]["F"]);
					$cnt["total_nil_submit"] = count($result_accumulated[$students[$j]['UserID']]["EH"]);
					$cnt["total_late"] = count($result_accumulated[$students[$j]['UserID']][3]);
					$cnt["total_abs"] = count($result_accumulated[$students[$j]['UserID']][1]) + count($result_accumulated[$students[$j]['UserID']][2]);
					if ($students[$j]['ClassNumber']<10)
					{
						$students[$j]['ClassNumber'] = "0".trim($students[$j]['ClassNumber']);
					}
					$return_arr[] = array(
										$class_name, $students[$j]['ClassNumber'], $students[$j]['StudentName'], 
										$month_str, 
										$cnt["nil_bring"], $record_arr["nil_bring"], 
										$cnt["nil_submit"], $record_arr["nil_submit"],
										$cnt["allow_abs"], $record_arr["allow_abs"],
										$cnt["improper_lang"], $record_arr["improper_lang"],
										$cnt["am_abs"], $record_arr["am_abs"],
										$cnt["pm_abs"], $record_arr["pm_abs"],
										$cnt["late"], $record_arr["late"],
										$cnt["early_leave"], $record_arr["early_leave"],
										$cnt["total_nil_submit"], $cnt["total_nil_bring"], $cnt["total_late"], $cnt["total_abs"]);
				}
			}
		}

		return $return_arr;
	}
	
	function Get_Class_Diary_Student_Parent_View_Index()
	{
		global $Lang;
		include_once("libuser.php");
		
		$libclassdiary = $this->Get_libclassdiary();
		
		if($_SESSION['UserType']==USERTYPE_PARENT){
			$libuser = new libuser($_SESSION['UserID']);
			$children_list = $libuser->getChildrenList();
			$children_selection = '<select id="StudentID" name="StudentID" onchange="Get_Class_Diary_Table();">';
			for($i=0;$i<count($children_list);$i++){
				$student_id = $children_list[$i]['StudentID'];
				$student_name = $children_list[$i]['StudentName'];
				$class_name = $children_list[$i]['ClassName'];
				$class_number = $children_list[$i]['ClassNumber'];
				$display_name = $student_name;
				//if($class_name != '' && $class_number !=''){
				//	$display_name .= '('.$class_name.'#'.$class_number.')';
				//}
				$children_selection .= '<option value="'.$student_id.'">'.$libclassdiary->html_special_chars($display_name).'</option>';
			}
			$children_selection.= '</select>';
			$user_field = $Lang['ClassDiary']['Children'].':&nbsp;'.$children_selection.'&nbsp;|&nbsp;';
		}else if($_SESSION['UserType']==USERTYPE_STUDENT){
			$user_field = '<input type="hidden" id="StudentID" name="StudentID" value="'.$_SESSION['UserID'].'" />';
		}
		
		$from_date = date("Y-m-d",getStartOfAcademicYear());
		$to_date = date("Y-m-d",getEndOfAcademicYear());
		
		# Date Range
		$DateRange = $Lang['General']['From'].'&nbsp;'.$this->GET_DATE_PICKER("FromDate",$from_date);
		$DateRange.= $Lang['General']['To'].'&nbsp;'.$this->GET_DATE_PICKER("ToDate",$to_date); 
		
		# View Btn
		$ViewBtn = $this->GET_SMALL_BTN($Lang['Btn']['View'],"button","Get_Class_Diary_Table();","ViewButton");
		
		$x = $this->Include_JS_CSS();
		$x .= '<br />';
		$x .= '<form id="form1" name="form1" method="post" action="index.php">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td valign="bottom">'."\n";
									$x .= '<div class="table_filter">'."\n";
										$x .= $user_field;
										$x .= $DateRange.'&nbsp;'.$ViewBtn."\n";
										$x .= '<span id="DateWarningLayer" style="color:red;display:none;"></span>';
									$x .= '</div>'."\n";
									$x .= '<p class="spacer"></p>';
								$x .= '</td>';
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n"; 
						# Main Table 
						$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="ClassDiaryTableDiv">'."\n";
										
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";		
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
						
					$x .= '</td>'."\n";		
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</form>'."\n";		
		$x .= '<br><br>'."\n";
		
		return $x;
	}
	
	function Get_Class_Diary_Student_View_Table($StudentID,$FromDate,$ToDate)
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$libclassdiary = $this->Get_libclassdiary();
		
		$records = $libclassdiary->Get_Class_Diary_Student_Lessons($StudentID,$FromDate,$ToDate);
		
		//debug_r($records);
		
		$x .= '<table class="common_table_list_v30" id="ClassDiaryTable">'."\n";
		# col group
			//$x .= '<col align="left" style="width: 10px">'."\n"; // #
			$x .= '<col align="left" style="width:10%;">'."\n"; // Date
			$x .= '<col align="left" style="width:10px;">'."\n"; // Lesson Number
			$x .= '<col align="left" style="width:15%;">'."\n"; // Subject
			$x .= '<col align="left" style="width:15%;">'."\n"; // Assignment
			$x .= '<col align="left" style="width:10%;">'."\n"; // Submission date
			$x .= '<col align="left" style="width:8%;">'."\n"; // Forgot book or stationary
			$x .= '<col align="left" style="width:8%;">'."\n"; // Excuss from lesson
			$x .= '<col align="left" style="width:8%;">'."\n"; // Not adapt apropriate medium instruction
			$x .= '<col align="left" style="width:8%;">'."\n"; // Absent AM
			$x .= '<col align="left" style="width:8%;">'."\n"; // Absent PM
			$x .= '<col align="left" style="width:%;">'."\n"; // Late
			$x .= '<col align="left" style="width:8%;">'."\n"; // Early leave
			
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Date'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Lesson'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Subject'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Assignment'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['DateOfSubmission'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['ForgotToBringBooks'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['StudentsExcuseLessons'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['StudentsNotAdoptMediumInstruction'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['AM'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Absentee'].$Lang['ClassDiary']['PM'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['Late'].'</th>'."\n";
					$x .= '<th>'.$Lang['ClassDiary']['EarlyLeave'].'</th>'."\n";
				$x .= '</tr>'."\n";	
			$x .= '</thead>'."\n";
		
			$x .= '<tbody>'."\n";
			if(sizeof($records)==0){
				$x .= '<tr><td colspan="10" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			}
			$prev_diary_id = -1;
			$red_tick_icon = '<img width="20" height="20" border="0" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/icon_tick.gif">';
			
			$diaryIDToRowCount = array();
			for($i=0;$i<count($records);$i++){
				if(!isset($diaryIDToRowCount[$records[$i]['DiaryID']])){
					$diaryIDToRowCount[$records[$i]['DiaryID']] = 0;
				}
				$diaryIDToRowCount[$records[$i]['DiaryID']]++;
			}
			
			for($i=0;$i<sizeof($records);$i++)
			{
				list($diary_id, $class_id, $diary_date, $lesson_id, $lesson_number, $subject, $subject_short,$assignment,$submission_date,$lesson_record_type, $profile_record_type) = $records[$i];
				$lesson_types = explode(",",$lesson_record_type);
				$profile_types = explode(",",$profile_record_type);
				
				$isF = in_array(RECORD_TYPE_FORGOT_BOOK,$lesson_types);
				$isE = in_array(RECORD_TYPE_EXCUSE_LESSON,$lesson_types);
				$isM = in_array(RECORD_TYPE_NO_MEDIUM_INSTRUCTION,$lesson_types);
				$isAbsentAM = in_array(RECORD_TYPE_ABSENT_AM,$profile_types);
				$isAbsentPM = in_array(RECORD_TYPE_ABSENT_PM,$profile_types);
				$isLate = in_array(RECORD_TYPE_LATE,$profile_types);
				$isEarlyleave = in_array(RECORD_TYPE_EARLYLEAVE,$profile_types);
				
				$row_span = $diaryIDToRowCount[$diary_id];
				
				$x .= '<tr>'."\n";
				if($prev_diary_id != $diary_id){
					$x .= '<td rowspan="'.$row_span.'">'.$diary_date.'</td>'."\n";
				}
					$x .= '<td>'.$lesson_number.'</td>'."\n";
					$x .= '<td>'.$libclassdiary->html_special_chars($subject).'</td>'."\n";
					$x .= '<td>'.$libclassdiary->html_special_chars($assignment,true,'-').'</td>'."\n";
					$x .= '<td>'.$libclassdiary->html_special_chars($submission_date,true,'-').'</td>'."\n";
					$x .= '<td>'.($isF?$red_tick_icon:'-').'</td>'."\n";
					$x .= '<td>'.($isE?$red_tick_icon:'-').'</td>'."\n";
					$x .= '<td>'.($isM?$red_tick_icon:'-').'</td>'."\n";
				if($prev_diary_id != $diary_id){
					$x .= '<td rowspan="'.$row_span.'">'.($isAbsentAM?$red_tick_icon:'-').'</td>'."\n";
					$x .= '<td rowspan="'.$row_span.'">'.($isAbsentPM?$red_tick_icon:'-').'</td>'."\n";
					$x .= '<td rowspan="'.$row_span.'">'.($isLate?$red_tick_icon:'-').'</td>'."\n";
					$x .= '<td rowspan="'.$row_span.'">'.($isEarlyleave?$red_tick_icon:'-').'</td>'."\n";
				}
				$x .= '</tr>'."\n";
				$prev_diary_id = $diary_id;
			}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br />'."\n";
		
		return $x;
	}
	
}

?>