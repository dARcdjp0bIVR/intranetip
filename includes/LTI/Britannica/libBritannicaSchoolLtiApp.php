<?php


class libBritannicaSchoolLtiApp extends libLtiApp
{
    const MODULE = 'BritannicaSchool';

    /**
     * libBritannicaSchoolLtiApp constructor.
     *
     * @param string $resourceLinkId
     * @param string $contextId
     * @param string $consumerKey
     * @param string $consumerSecret
     */
    public function __construct($resourceLinkId, $contextId, $consumerKey, $consumerSecret)
    {
        $launchUrl = 'https://lti.eb.com.au/lti';
        parent::__construct($resourceLinkId, $contextId, $launchUrl, $consumerKey, $consumerSecret);
    }

    /**
     * @return array
     */
    protected function getLaunchData()
    {
        return array_merge(
            parent::getLaunchData(),
            array(
                'custom_resource_url' => 'https://school.eb.com.au'
            )
        );
    }
}