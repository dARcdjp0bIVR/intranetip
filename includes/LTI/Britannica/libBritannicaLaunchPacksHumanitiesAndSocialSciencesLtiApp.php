<?php


class libBritannicaLaunchPacksHumanitiesAndSocialSciencesLtiApp extends libLtiApp
{
    const MODULE = 'BritannicaLaunchPacksHumanitiesAndSocialSciences';

    /**
     * libBritannicaLaunchPacksScienceLtiApp constructor.
     *
     * @param $resourceLinkId
     * @param $contextId
     * @param $consumerKey
     * @param $consumerSecret
     */
    public function __construct($resourceLinkId, $contextId, $consumerKey, $consumerSecret)
    {
        $launchUrl = 'https://lti.eb.com.au/lti';
        parent::__construct($resourceLinkId, $contextId, $launchUrl, $consumerKey, $consumerSecret);
    }

    /**
     * @return array
     */
    protected function getLaunchData()
    {
        return array_merge(
            parent::getLaunchData(),
            array(
                'custom_resource_url' => 'https://packs.eb.com.au/hss'
            )
        );
    }
}