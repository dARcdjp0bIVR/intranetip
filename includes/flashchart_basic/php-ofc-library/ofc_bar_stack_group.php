<?php

include_once 'ofc_bar_base.php';

class bar_stack_group_value
{
	function bar_value( $top, $bottom=null )
	{
		$this->top = $top;
		
		if( isset( $bottom ) )
			$this->bottom = $bottom;
	}
	
	function set_colour( $colour )
	{
		$this->colour = $colour;
	}
	
	function set_tooltip( $tip )
	{
		$this->tip = $tip;
	}
}

class bar_stack_group extends bar_base
{
	function bar_stack_group()
	{
		$this->type      = "bar_stack_group";
		parent::bar_base();
	}
}

