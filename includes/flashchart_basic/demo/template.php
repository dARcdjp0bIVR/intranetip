<?php
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");


# flash chart
//$title = new title( "2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title = new title( "中文測試 2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$x_legend = new x_legend( 'class' );
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$xLab = new x_axis_labels();
$xLab->set_labels( array('1A','1B','1C','1D','1E','1F') );
$xLab->set_vertical();

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
//$x->set_labels_from_array( array('1A','1B','1C','1D','1E','1F') );
$x->set_labels( $xLab );

$y_legend = new y_legend( 'Quantity 數量' );
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );
$y->set_range( 0, 20, 5 );
$y->set_offset(true);

$testChi = "記大過";
//$testChi = iconv("UTF-8", "BIG5", $testChi);

$bar0 = new bar();
$bar0->set_values( array(12,16,7,10,5,18) );
$bar0->set_colour( '#ab45ff' );
$bar0->set_tooltip( $testChi.':#val#' );
$bar0->set_key( $testChi, '12' );
$bar0->set_id( 0 );
$bar0->set_visible( true );

$bar1 = new bar();
$bar1->set_values( array(5,19,12,16,9,3) );
$bar1->set_colour( '-2' );
$bar1->set_tooltip( 'Minar Merit:#val#' );
$bar1->set_key( 'Minar Merit', '12' );
$bar1->set_id( 1 );
$bar1->set_visible( true );

$bar2 = new bar();
$bar2->set_values( array(18,4,16,6,16,5) );
$bar2->set_colour( '-3' );
$bar2->set_tooltip( 'Major Merit:#val#' );
$bar2->set_key( 'Major Merit', '12' );
$bar2->set_id( 2 );
$bar2->set_visible( false );

$bar3 = new bar();
$bar3->set_values( array(14,8,11,13,7,14) );
$bar3->set_colour( '-4' );
$bar3->set_tooltip( 'Black Point:#val#' );
$bar3->set_key( 'Black Point', '12' );
$bar3->set_id( 3 );
$bar3->set_visible( true );

$bar4 = new bar();
$bar4->set_values( array(16,7,5,11,17,12) );
$bar4->set_colour( '-5' );
$bar4->set_tooltip( 'Minar Demerit:#val#' );
$bar4->set_key( 'Minar Demerit', '12' );
$bar4->set_id( 4 );
$bar4->set_visible( false );

$bar5 = new bar();
$bar5->set_values( array(11,8,13,11,16,5) );
$bar5->set_colour( '-6' );
$bar5->set_tooltip( 'Major Demerit:#val#' );
$bar5->set_key( 'Major Demerit', '12' );
$bar5->set_id( 5 );
$bar5->set_visible( true );

$bar6 = new bar();
$bar6->set_values( array(18,4,16,6,16,5) );
$bar6->set_colour( '-7' );
$bar6->set_tooltip( 'Major Merit:#val#' );
$bar6->set_key( 'Major Merit', '12' );
$bar6->set_id( 6 );
$bar6->set_visible( false );

$bar7 = new bar();
$bar7->set_values( array(14,8,11,13,7,14) );
$bar7->set_colour( '-8' );
$bar7->set_tooltip( 'Black Point:#val#' );
$bar7->set_key( 'Black Point', '12' );
$bar7->set_id( 7 );
$bar7->set_visible( true );

$bar8 = new bar();
$bar8->set_values( array(16,7,5,11,17,12) );
$bar8->set_colour( '-9' );
$bar8->set_tooltip( 'Minar Demerit:#val#' );
$bar8->set_key( 'Minar Demerit', '12' );
$bar8->set_id( 8 );
$bar8->set_visible( false );

$bar9 = new bar();
$bar9->set_values( array(11,8,13,11,16,5) );
$bar9->set_colour( '-10' );
$bar9->set_tooltip( 'Major Demerit:#val#' );
$bar9->set_key( 'Major Demerit', '12' );
$bar9->set_id( 9 );
$bar9->set_visible( true );

$tooltip = new tooltip();
//$tooltip->set_proximity();
$tooltip->set_hover();
$tooltip->set_stroke( 2 );
//$tooltip->set_colour( "#ff0000" );
//$tooltip->set_background_colour( "#ff00ff" ); 

# show/hide checkbox panel
//$key = new key_legend();
//$key->set_visible(true);		

$chart = new open_flash_chart();
$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->add_element( $bar0 );
$chart->add_element( $bar1 );
$chart->add_element( $bar2 );
$chart->add_element( $bar3 );
$chart->add_element( $bar4 );
$chart->add_element( $bar5 );
$chart->add_element( $bar6 );
$chart->add_element( $bar7 );
$chart->add_element( $bar8 );
$chart->add_element( $bar9 );
$chart->set_tooltip( $tooltip );
//$chart->set_key_legend( $key );

?>

<html>
<head>
	<title>flash demo</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
	<script type="text/javascript">
		function ofc_ready(){
			//alert('ofc_ready');
		}

		function open_flash_chart_data(){
			//alert( 'reading data' );
			return JSON.stringify(data);
	
			//return "{\"elements\":[{\"type\":\"line_dot\",\"values\":[4,3,4,1,0,1],\"width\":2,\"colour\":\"#00FF21\",\"tip\":\"#val#\",\"id\":\"component\"}],\"bg_colour\":\"#ffffff\",\"title\":{\"text\":\"adam\",\"style\":\"{font-size: 12px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}\"},\"radar_axis\":{\"max\":5,\"colour\":\"#C1CBFF\",\"grid-colour\":\"#C1CBFF\",\"spoke-labels\":{\"labels\":[\"Like Green Color?\",\"Like Pink Color?\",\"Like Boy?\",\"Like Girl?\",\"Like Old Man?\",\"Like Young Woman?\"],\"colour\":\"#9F819F\"}},\"tooltip\":{\"mouse\":1}}";

			

		}

		function findSWF(movieName) {
		  if (navigator.appName.indexOf("Microsoft")!= -1) {
			return window[movieName];
		  } else {
			return document[movieName];
		  }
		}
		
		function setChart(id, display){
			//for testing
			var heading = document.getElementById("testHead");
			while(heading.hasChildNodes()){
				heading.removeChild(heading.firstChild);
			}
			var h = document.createTextNode(String(id));
			heading.appendChild(h);
			var content = document.getElementById("testContent");
			while(content.hasChildNodes()){
				content.removeChild(content.firstChild);
			}			
			var c = document.createTextNode(String(display));
			content.appendChild(c);
		}
			
		var data = <?php echo $chart->toPrettyString(); ?>;
		
	</script>
	<script type="text/javascript">
		swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart", "850", "350", "9.0.0");
	</script>
		
</head>
<body>	
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center">
					
						<div id="my_chart">no flash?</div>

							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<span id="testHead"></span> : <span id="testContent"></span>
		<td>
	</tr>
</table>
</body>
</html>

