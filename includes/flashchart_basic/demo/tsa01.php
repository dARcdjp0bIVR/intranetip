<?php
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");


# flash chart
$title = new title( "Paper Statistics – No. of Participant (P3, English, Listening Paper 1)" );
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$x_legend = new x_legend( 'Class' );
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$x->set_labels_from_array( array('3A','3B','3C','3D') );

$y_legend = new y_legend( 'No. of students' );
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );
$y->set_range( 0, 25, 5 );
$y->set_offset(false);

$bar0 = new bar_stack_group();
$bar0->set_values( array(20,10,5,5) );
$bar0->set_colour( '#72a9db' );
$bar0->set_tooltip( 'Participant:#val#' );
$bar0->set_key( 'Participant', '12' );
$bar0->set_id( 0 );
$bar0->set_visible( true );

$bar1 = new bar_stack_group();
$bar1->set_values( array(0,10,15,15) );
$bar1->set_colour( '#eb593b' );
$bar1->set_tooltip( 'Non-Participant:#val#' );
$bar1->set_key( 'Non-Participant', '12' );
$bar1->set_id( 1 );
$bar1->set_visible( true );

$tooltip = new tooltip();
$tooltip->set_hover();
//$tooltip->set_static();
//$tooltip->set_stroke( 2 );
//$tooltip->set_colour( "#000000" );
//$tooltip->set_background_colour( "#ffffff" ); 

# show/hide checkbox panel
//$key = new key_legend();
//$key->set_visible(true);		

$chart = new open_flash_chart();
$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->add_element( $bar0 );
$chart->add_element( $bar1 );
$chart->set_tooltip( $tooltip );
//$chart->set_key_legend( $key );

?>

<html>
<head>
	<title>flash demo</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
	<script type="text/javascript">
		function ofc_ready(){
			//alert('ofc_ready');
		}

		function open_flash_chart_data(){
			//alert( 'reading data' );
			return JSON.stringify(data);
		}

		function findSWF(movieName) {
		  if (navigator.appName.indexOf("Microsoft")!= -1) {
			return window[movieName];
		  } else {
			return document[movieName];
		  }
		}
		
		function setChart(id, display){
			//for testing
			var heading = document.getElementById("testHead");
			while(heading.hasChildNodes()){
				heading.removeChild(heading.firstChild);
			}
			var h = document.createTextNode(String(id));
			heading.appendChild(h);
			var content = document.getElementById("testContent");
			while(content.hasChildNodes()){
				content.removeChild(content.firstChild);
			}			
			var c = document.createTextNode(String(display));
			//content.appendChild(c);
		}
			
		var data = <?php echo $chart->toPrettyString(); ?>;
		
	</script>
	<script type="text/javascript">
		swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart-develop.swf", "my_chart", "690", "415", "9.0.0");
	</script>
		
</head>
<body>	
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center">
					
						<div id="my_chart">no flash?</div>

							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<span id="testHead"></span> : <span id="testContent"></span>
		<td>
	</tr>
</table>
</body>
</html>

