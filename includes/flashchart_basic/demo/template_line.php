<?php
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");


# flash chart
//$title = new title( "2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title = new title( "中文測試 2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$x_legend = new x_legend( 'class' );
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$x->set_labels_from_array( array('1A','1B','1C','1D','1E','1F') );

$y_legend = new y_legend( 'Quantity 數量' );
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );
$y->set_range( 0, 20, 5 );
$y->set_offset(true);

$testChi = "記大過";
//$testChi = iconv("UTF-8", "BIG5", $testChi);

$line0 = new line_dot();
$line0->set_dot_size(5);
$line0->set_values( array(12,16,7,10,5,18) );
$line0->set_colour( '#72a9db' );
$line0->set_tooltip( $testChi.':#val#' );
$line0->set_key( $testChi, '12' );
$line0->set_id( 0 );
$line0->set_visible( true );

$line1 = new line_dot();
$line1->set_dot_size(5);
$line1->set_values( array(5,19,12,16,9,3) );
$line1->set_colour( '#eb593b' );
$line1->set_tooltip( 'Minar Merit:#val#' );
$line1->set_key( 'Minar Merit', '12' );
$line1->set_id( 1 );
$line1->set_visible( true );

$tooltip = new tooltip();
//$tooltip->set_proximity();
$tooltip->set_hover();
$tooltip->set_stroke( 2 );
//$tooltip->set_colour( "#ff0000" );
//$tooltip->set_background_colour( "#ff00ff" ); 

# show/hide checkbox panel
//$key = new key_legend();
//$key->set_visible(true);		

$chart = new open_flash_chart();
$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->add_element( $line0 );
$chart->add_element( $line1 );
$chart->set_tooltip( $tooltip );
//$chart->set_key_legend( $key );

?>

<html>
<head>
	<title>flash demo</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
	<script type="text/javascript">
		function ofc_ready(){
			//alert('ofc_ready');
		}

		function open_flash_chart_data(){
			//alert( 'reading data' );
			return JSON.stringify(data);
	
			//return "{\"elements\":[{\"type\":\"line_dot\",\"values\":[4,3,4,1,0,1],\"width\":2,\"colour\":\"#00FF21\",\"tip\":\"#val#\",\"id\":\"component\"}],\"bg_colour\":\"#ffffff\",\"title\":{\"text\":\"adam\",\"style\":\"{font-size: 12px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}\"},\"radar_axis\":{\"max\":5,\"colour\":\"#C1CBFF\",\"grid-colour\":\"#C1CBFF\",\"spoke-labels\":{\"labels\":[\"Like Green Color?\",\"Like Pink Color?\",\"Like Boy?\",\"Like Girl?\",\"Like Old Man?\",\"Like Young Woman?\"],\"colour\":\"#9F819F\"}},\"tooltip\":{\"mouse\":1}}";

			

		}

		function findSWF(movieName) {
		  if (navigator.appName.indexOf("Microsoft")!= -1) {
			return window[movieName];
		  } else {
			return document[movieName];
		  }
		}
		
		function setChart(id, display){
			//for testing
			var heading = document.getElementById("testHead");
			while(heading.hasChildNodes()){
				heading.removeChild(heading.firstChild);
			}
			var h = document.createTextNode(String(id));
			heading.appendChild(h);
			var content = document.getElementById("testContent");
			while(content.hasChildNodes()){
				content.removeChild(content.firstChild);
			}			
			var c = document.createTextNode(String(display));
			content.appendChild(c);
		}
			
		var data = <?php echo $chart->toPrettyString(); ?>;
		
	</script>
	<script type="text/javascript">
		swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart", "850", "350", "9.0.0");
	</script>
		
</head>
<body>	
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center">
					
						<div id="my_chart">no flash?</div>

							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<span id="testHead"></span> : <span id="testContent"></span>
		<td>
	</tr>
</table>
</body>
</html>

