<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");


# flash chart
//$title = new title( "2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title = new title( "中文測試 2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$x_legend = new x_legend( 'class' );
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$x->set_labels_from_array( array('1A','1B','1C','1D','1E','1F') ); //set class name or other x-axis label

$y_legend = new y_legend( 'Quantity' ); //y-axis label
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );
$y->set_range( 0, 30, 5 ); //set scale of y-axis, based on the maximum values of data 
$y->set_offset(true);

##########################################################################################

$bar_stack0 = new bar_stack();
$bar_stack0->set_oneColour( true ); //whether the bar is draw in one colour instead of stack of colours
$bar_stack0->set_expandable( true ); //can click to call popup of sub-level chart
$bar_stack0->set_colour( '#72a9db' ); //colour of whole bar i.e. set_oneColour( true )
// set a cycle of 5 colours for stacked bars i.e. set_oneColour( false ):
//$bar_stack0->set_colours( array( '#72a900', '#72a944', '#72a9aa',  '#72a9cc', '#72a9ff' ) );

//append bar for every x-axis values, in here, is classes array('1A','1B','1C','1D','1E','1F')
$bar_stack0->append_stack( array( 2.5, 5, 2.5 ) );
$bar_stack0->append_stack( array( 2.5, 5, 1.25, 1.25 ) );
$bar_stack0->append_stack( array( 5, 7 ) );
$bar_stack0->append_stack( array( 2, 2, 2, 2, new bar_stack_value(2, '#72a9cc') ) ); //special colour
$bar_stack0->append_stack( array( 4, 1.9 ) );
$bar_stack0->append_stack( array( 3.8, 2, 3.7, 4 ) );
$bar_stack0->set_key( '記大過', '12' );

/*
//keys for individual stack
$bar_stack0->set_keys(
    array(
        new bar_stack_key( '#72a900', 'late', 12 ),
        new bar_stack_key( '#72a944', 'fighting', 12 ),
        new bar_stack_key( '#72a9aa', 'drunk', 12 ),
        new bar_stack_key( '#72a9cc', 'sleepy', 12 ),
        new bar_stack_key( '#72a9ff', 'fail', 12 ),
        new bar_stack_key( '#72a9db', 'unknown', 12 )
        )
    );
*/

$bar_stack0->set_tooltip( '記大過: #total#' ); //tool tip when mouse-over the bar    
//$bar_stack0->set_tooltip( 'X label [#x_label#], Value [#val#]<br>Total [#total#]' );

$bar_stack0->set_id( 0 ); //id to recognize this bar_stack during calls between flash and javascript
$bar_stack0->set_visible( true ); //visible? in the beginning


##########################################################################################

$bar_stack1 = new bar_stack();
$bar_stack1->set_oneColour( true );
$bar_stack1->set_expandable( true );
$bar_stack1->set_colour( '#eb593b' );
// set a cycle of 3 colours for stacked:
//$bar_stack1->set_colours( array( '#eb5900', '#eb59aa', '#eb59ff' ) );
$bar_stack1->append_stack( array( 6, 5) );
$bar_stack1->append_stack( array( 2, 1, 1 ) );
$bar_stack1->append_stack( array( 5, 0.2, 7 ) );
$bar_stack1->append_stack( array( 2, 0.6 ) );
$bar_stack1->append_stack( array( 3, 5.6, 7, 1.2, 8 ) );
$bar_stack1->append_stack( array( 2, 0.6, 4, 2 ) );
$bar_stack1->set_key( '欠交功課三次', '12' );
/*
$bar_stack1->set_keys(
    array(
        new bar_stack_key( '#eb5900', 'aaa', 12 ),
        new bar_stack_key( '#eb59aa', 'bbb', 12 ),
        new bar_stack_key( '#eb59ff', 'ccc', 12 )
        )
    );
*/    
$bar_stack1->set_tooltip( '欠交功課三次: #total#' );
//$bar_stack1->set_tooltip( 'X label [#x_label#], Value [#val#]<br>Total [#total#]' );
$bar_stack1->set_id( 1 );
$bar_stack1->set_visible( true );


##########################################################################################

$bar_stack2 = new bar_stack();
$bar_stack2->set_oneColour( true );
$bar_stack2->set_expandable( false );
$bar_stack2->set_colour( '#aaaaaa' );
// set a cycle of 6 colours for stacked:
//$bar_stack2->set_colours( array( '#aaaa00', '#aaaa44', '#aaaa99', '#aaaaaa', '#aaaacc', '#aaaaff' ) );
$bar_stack2->append_stack( array( 2.3, 1, 4.5 ) );
$bar_stack2->append_stack( array( 4.6, 2, 3.5, 1 ) );
$bar_stack2->append_stack( array( 5, 0.2, 7, 6.9, 4.1 ) );
$bar_stack2->append_stack( array( 2.9, 3, 4.6 ) ); //special color
$bar_stack2->append_stack( array( 7.1, 1, 5, 6.3, 2.3, 0.2 ) );
$bar_stack2->append_stack( array( 3.4, 7.6, 8 ) );
$bar_stack2->set_key( 'item 05', '12' );
/*
$bar_stack2->set_keys(
    array(
        new bar_stack_key( '#aaaa00', '123', 12 ),
        new bar_stack_key( '#aaaa44', '456', 12 ),
        new bar_stack_key( '#aaaa99', '789', 12 ),
        new bar_stack_key( '#aaaaaa', '256', 12 ),
        new bar_stack_key( '#aaaacc', '476', 12 ),
        new bar_stack_key( '#aaaaff', '875', 12 ),
        new bar_stack_key( '#ffaaaa', '021', 12 )        
        )
    );
*/ 
$bar_stack2->set_tooltip( '&quot;xxx &#039;yyy &lt;b&gt;許&lt;/b&gt; s1 (MC) item 05: #total#' );
//$bar_stack2->set_tooltip( 'X label [#x_label#], Value [#val#]<br>Total [#total#]' );
$bar_stack2->set_id( 2 );
$bar_stack2->set_visible( true );

##########################################################################################

$bar_stack3 = new bar_stack();
$bar_stack3->set_oneColour( false );
$bar_stack3->set_expandable( true );
$bar_stack3->set_colour( '#92d24f' );
// set a cycle of 4 colours for stacked:
$bar_stack3->set_colours( array( '#00d24f', '#44d24f', '#bbd24f', '#ffd24f' ) );
$bar_stack3->append_stack( array( 1.5, 4.5 ) );
$bar_stack3->append_stack( array( 6.8, 2.6, 1.9 ) );
$bar_stack3->append_stack( array( 3.9, 2.2, 3.8 ) );
$bar_stack3->append_stack( array( 2.9, 3 ) ); //special color
$bar_stack3->append_stack( array( 2.6, 3.3, 8.3, 2.2 ) );
$bar_stack3->append_stack( array( 4.5, 2.6, 2.1 ) );
$bar_stack3->set_key( 'Black Point', '12' );
$bar_stack3->set_keys(
    array(
        new bar_stack_key( '#00d24f', '123', 12 ),
        new bar_stack_key( '#44d24f', '456', 12 ),
        new bar_stack_key( '#bbd24f', '789', 12 ),
        new bar_stack_key( '#ffd24f', '256', 12 )
        )
    );
$bar_stack3->set_tooltip( 'Black Point: #total#' );
//$bar_stack3->set_tooltip( 'X label [#x_label#], Value [#val#]<br>Total [#total#]' );
$bar_stack3->set_id( 3 );
$bar_stack3->set_visible( true );

##########################################################################################

$tooltip = new tooltip();
$tooltip->set_hover();
//$tooltip->set_stroke( 2 );
//$tooltip->set_colour( "#000000" );
//$tooltip->set_background_colour( "#ffffff" ); 

//uncomment this in print version (i.e. non-selectable) and $chart->set_key_legend( $key )
//$key = new key_legend();
//$key->set_selectable(false);

$chart = new open_flash_chart();
$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
//add how many bar_stack i.e. categories
$chart->add_element( $bar_stack0 );
$chart->add_element( $bar_stack1 );
$chart->add_element( $bar_stack2 );
$chart->add_element( $bar_stack3 );

$chart->set_tooltip( $tooltip );
//$chart->set_key_legend( $key );

?>

<html>
<head>
	<title>flash demo</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
	<script type="text/javascript">
		function ofc_ready(){
			//alert('ofc_ready');
		}

		function open_flash_chart_data(){
			//alert( 'reading data' );
			return JSON.stringify(data);
		}

		function findSWF(movieName) {
		  if (navigator.appName.indexOf("Microsoft")!= -1) {
			return window[movieName];
		  } else {
			return document[movieName];
		  }
		}
		
		function setChart(id, display){
			//for testing
			var heading = document.getElementById("testHead");
			while(heading.hasChildNodes()){
				heading.removeChild(heading.firstChild);
			}
			var h = document.createTextNode(String(id));
			heading.appendChild(h);
			var content = document.getElementById("testContent");
			while(content.hasChildNodes()){
				content.removeChild(content.firstChild);
			}			
			var c = document.createTextNode(String(display));
			content.appendChild(c);
		}
		
		function expandSubLevel(id){
			//for testing
			var popup = document.getElementById("testPopup");
			while(popup.hasChildNodes()){
				popup.removeChild(popup.firstChild);
			}
			var p = document.createTextNode(String(id));
			popup.appendChild(p);	
		}	
			
		var data = <?php echo $chart->toPrettyString(); ?>;
		
	</script>
	<script type="text/javascript">
		swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart-stephen.swf", "my_chart", "850", "350", "9.0.0");
	</script>
		
</head>
<body>	
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center">
					
						<div id="my_chart">no flash?</div>

							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			Call back the visibility: category id: <span id="testHead"></span> - visible? : <span id="testContent"></span>
		<td>
	</tr>
	<tr>
		<td>
			Call pop-up to generate sub-level bar chart: categroy id: <span id="testPopup"></span>
		<td>
	</tr>	
</table>
</body>
</html>

