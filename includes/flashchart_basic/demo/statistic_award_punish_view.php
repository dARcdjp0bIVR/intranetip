<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

##############################################################################################################################
############################################### flash chart ##################################################################
##############################################################################################################################
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
##############################################################################################################################
############################################ END - flash chart ###############################################################
##############################################################################################################################

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();


if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") && !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-View") && !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
		
$TAGS_OBJ[] = array($eDiscipline['Overview'], "");

# menu highlight setting
$CurrentPage = "Overview";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$PATH_WRT_ROOT = "../../../../";

##############################################################################################################################
############################################### flash chart ##################################################################
##############################################################################################################################
# flash chart
//$title = new title( "2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title = new title( "中文測試 2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$x_legend = new x_legend( 'class' );
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$x->set_labels_from_array( array('1A','1B','1C','1D','1E','1F') );

$y_legend = new y_legend( 'Quantity' );
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );
$y->set_range( 0, 20, 5 );
$y->set_offset(true);

$bar0 = new bar();
$bar0->set_values( array(12,16,7,10,5,18) );
$bar0->set_colour( '#72a9db' );
$bar0->set_tooltip( '記大過:#val#' );
$bar0->set_key( '記大過', '12' );
$bar1 = new bar();
$bar1->set_values( array(5,19,12,16,9,3) );
$bar1->set_colour( '#eb593b' );
$bar1->set_tooltip( 'Minar Merit:#val#' );
$bar1->set_key( 'Minar Merit', '12' );
$bar2 = new bar();
$bar2->set_values( array(18,4,16,6,16,5) );
$bar2->set_colour( '#aaaaaa' );
$bar2->set_tooltip( 'Major Merit:#val#' );
$bar2->set_key( 'Major Merit', '12' );
$bar3 = new bar();
$bar3->set_values( array(14,8,11,13,7,14) );
$bar3->set_colour( '#92d24f' );
$bar3->set_tooltip( 'Black Point:#val#' );
$bar3->set_key( 'Black Point', '12' );
$bar4 = new bar();
$bar4->set_values( array(16,7,5,11,17,12) );
$bar4->set_colour( '#eaa325' );
$bar4->set_tooltip( 'Minar Demerit:#val#' );
$bar4->set_key( 'Minar Demerit', '12' );
$bar5 = new bar();
$bar5->set_values( array(11,8,13,11,16,5) );
$bar5->set_colour( '#f0e414' );
$bar5->set_tooltip( 'Major Demerit:#val#' );
$bar5->set_key( 'Major Demerit', '12' );

$tooltip = new tooltip();
$tooltip->set_hover();
//$tooltip->set_stroke( 2 );
//$tooltip->set_colour( "#000000" );
//$tooltip->set_background_colour( "#ffffff" ); 

$chart = new open_flash_chart();
$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->add_element( $bar0 );
$chart->add_element( $bar1 );
$chart->add_element( $bar2 );
$chart->add_element( $bar3 );
$chart->add_element( $bar4 );
$chart->add_element( $bar5 );
$chart->set_tooltip( $tooltip );

##############################################################################################################################
############################################ END - flash chart ###############################################################
##############################################################################################################################

?>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center">
						<!--
						##############################################################################################################################
						############################################### flash chart ##################################################################
						##############################################################################################################################
						-->
						<div id="my_chart">no flash?</div>
						
						<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
						<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
						<script type="text/javascript">
							swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart-develop.swf", "my_chart", "850", "350", "9.0.0", "expressInstall.swf");
						</script>
						
						<script type="text/javascript">
						function ofc_ready(){
							//alert('ofc_ready');
						}

						function open_flash_chart_data(){
							//alert( 'reading data' );
							return JSON.stringify(data);
						}

						function findSWF(movieName) {
						  if (navigator.appName.indexOf("Microsoft")!= -1) {
							return window[movieName];
						  } else {
							return document[movieName];
						  }
						}
							
						var data = <?php echo $chart->toPrettyString(); ?>;
						</script>
						<!--
						##############################################################################################################################
						############################################ END - flash chart ###############################################################
						##############################################################################################################################
						-->
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
