<?php
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");


# flash chart
//$title = new title( "2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title = new title( "分數調整機制" );
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$x_legend = new x_legend( '課業' );
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #666666}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$x->set_labels_from_array( array('A','B','C','D','E','F') );

$y_legend = new y_legend( '分數' );
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #666666}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );
$y->set_range( 0, 20, 2 );
$y->set_offset(true);

$line0 = new line_dot();
$line0->set_dot_size(2);
$line0->set_values( array(15,15,12,10,13,16) );
$line0->set_colour( '#72a9db' );
$line0->set_tooltip( 'Lee Ka Wai: #val#' );
$line0->set_key( 'Lee Ka Wai', '12' );
$line0->set_id( 0 );
$line0->set_visible( true );

$line1 = new line_dot();
$line1->set_dot_size(2);
$line1->set_values( array(10,11,9,11,10,11) );
$line1->set_colour( '#eb593b' );
$line1->set_tooltip( 'Lam Kei Hai: #val#' );
$line1->set_key( 'Lam Kei Hai', '12' );
$line1->set_id( 1 );
$line1->set_visible( true );

$line2 = new line_dot();
$line2->set_dot_size(2);
$line2->set_values( array(13,13,11,8,12,15) );
$line2->set_colour( '#a32afa' );
$line2->set_tooltip( 'Keung Siu Wai: #val#' );
$line2->set_key( 'Keung Siu Wai', '12' );
$line2->set_id( 2 );
$line2->set_visible( true );

$line3 = new line_dot();
$line3->set_dot_size(2);
$line3->set_values( array(12,15,12,12,13,15) );
$line3->set_colour( '#92d24f' );
$line3->set_tooltip( 'Cheung Ka Wai: #val#' );
$line3->set_key( 'Cheung Ka Wai', '12' );
$line3->set_id( 3 );
$line3->set_visible( true );

$line4 = new line_dot();
$line4->set_dot_size(2);
$line4->set_values( array(14,14,13,11,11,13) );
$line4->set_colour( '#eaa325' );
$line4->set_tooltip( 'Ho Mei Wing: #val#' );
$line4->set_key( 'Ho Mei Wing', '12' );
$line4->set_id( 4 );
$line4->set_visible( true );

$line5 = new line_dot();
$line5->set_dot_size(2);
$line5->set_values( array(14,14,14,12,12,14) );
$line5->set_colour( '#f0e414' );
$line5->set_tooltip( 'Lee Ka Fai: #val#' );
$line5->set_key( 'Lee Ka Fai', '12' );
$line5->set_id( 5 );
$line5->set_visible( true );

$line6 = new line_dot();
$line6->set_dot_size(2);
$line6->set_values( array(13,13,13,9,12,13) );
$line6->set_colour( '#a35d1e' );
$line6->set_tooltip( 'Lo Mei Wai: #val#' );
$line6->set_key( 'Lo Mei Wai', '12' );
$line6->set_id( 6 );
$line6->set_visible( true );

$line7 = new line_dot();
$line7->set_dot_size(2);
$line7->set_values( array(14,12,13,11,13,15) );
$line7->set_colour( '#0000ff' );
$line7->set_tooltip( 'Li Ka Wing: #val#' );
$line7->set_key( 'Li Ka Wing', '12' );
$line7->set_id( 7 );
$line7->set_visible( true );

$line8 = new line_dot();
$line8->set_dot_size(2);
$line8->set_values( array(15,15,11,9,11,13) );
$line8->set_colour( '#00ff00' );
$line8->set_tooltip( 'Lee Ka Fai: #val#' );
$line8->set_key( 'Lee Ka Fai', '12' );
$line8->set_id( 8 );
$line8->set_visible( true );

$line9 = new line_dot();
$line9->set_dot_size(2);
$line9->set_values( array(16,16,13,13,14,17) );
$line9->set_colour( '#bc236a' );
$line9->set_tooltip( 'Lai Ming Wai: #val#' );
$line9->set_key( 'Lai Ming Wai', '12' );
$line9->set_id( 9 );
$line9->set_visible( true );

$line10 = new line_dot();
$line10->set_dot_size(2);
$line10->set_values( array(13.6,13.8,12.1,10.6,12.1,14.2) );
$line10->set_colour( '#333333' );
$line10->set_tooltip( '評分老師之平均數: #val#' );
$line10->set_key( '評分老師之平均數', '12' );
$line10->set_id( 10 );
$line10->set_visible( true );

$line11 = new line_dot();
$line11->set_dot_size(2);
$line11->set_values( array(14,13,11,11,12,14) );
$line11->set_colour( '#bcbcbc' );
$line11->set_tooltip( '標準評分: #val#' );
$line11->set_key( '標準評分', '12' );
$line11->set_id( 11 );
$line11->set_visible( true );

$tooltip = new tooltip();
//$tooltip->set_proximity();
$tooltip->set_hover();
$tooltip->set_stroke( 2 );
//$tooltip->set_colour( "#ff0000" );
//$tooltip->set_background_colour( "#ff00ff" ); 

# show/hide checkbox panel
//$key = new key_legend();
//$key->set_visible(true);		

$chart = new open_flash_chart();
$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->add_element( $line0 );
$chart->add_element( $line1 );
$chart->add_element( $line2 );
$chart->add_element( $line3 );
$chart->add_element( $line4 );
$chart->add_element( $line5 );
$chart->add_element( $line6 );
$chart->add_element( $line7 );
$chart->add_element( $line8 );
$chart->add_element( $line9 );
$chart->add_element( $line10 );
$chart->add_element( $line11 );
$chart->set_tooltip( $tooltip );
//$chart->set_key_legend( $key );

?>

<html>
<head>
	<title>flash demo</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/json/json2.js"></script>
	<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>	
	<script type="text/javascript">
		function ofc_ready(){
			//alert('ofc_ready');
		}

		function open_flash_chart_data(){
			//alert( 'reading data' );
			return JSON.stringify(data);
	
			//return "{\"elements\":[{\"type\":\"line_dot\",\"values\":[4,3,4,1,0,1],\"width\":2,\"colour\":\"#00FF21\",\"tip\":\"#val#\",\"id\":\"component\"}],\"bg_colour\":\"#ffffff\",\"title\":{\"text\":\"adam\",\"style\":\"{font-size: 12px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}\"},\"radar_axis\":{\"max\":5,\"colour\":\"#C1CBFF\",\"grid-colour\":\"#C1CBFF\",\"spoke-labels\":{\"labels\":[\"Like Green Color?\",\"Like Pink Color?\",\"Like Boy?\",\"Like Girl?\",\"Like Old Man?\",\"Like Young Woman?\"],\"colour\":\"#9F819F\"}},\"tooltip\":{\"mouse\":1}}";

			

		}

		function findSWF(movieName) {
		  if (navigator.appName.indexOf("Microsoft")!= -1) {
			return window[movieName];
		  } else {
			return document[movieName];
		  }
		}
		
		function setChart(id, display){
			//for testing
			/*
			var heading = document.getElementById("testHead");
			while(heading.hasChildNodes()){
				heading.removeChild(heading.firstChild);
			}
			var h = document.createTextNode(String(id));
			heading.appendChild(h);
			var content = document.getElementById("testContent");
			while(content.hasChildNodes()){
				content.removeChild(content.firstChild);
			}			
			var c = document.createTextNode(String(display));
			content.appendChild(c);
			*/
		}
			
		var data = <?php echo $chart->toPrettyString(); ?>;
		
	</script>
	<script type="text/javascript">
		swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart-develop.swf", "my_chart", "850", "700", "9.0.0");
	</script>
		
</head>
<body>	
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align="center">
					
						<div id="my_chart">no flash?</div>

							
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<span id="testHead"></span> : <span id="testContent"></span>
		<td>
	</tr>
</table>
</body>
</html>

