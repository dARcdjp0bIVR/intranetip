<?php
# using : Siuwan

/* Modification Log:
 * 
 */

if (!defined("LIBEPOSTCOURSEWARE_DEFINED"))         // Preprocessor directives
{
	define("LIBEPOSTCOURSEWARE_DEFINED",true);
	include_once("config/config_courseware.php");
	
	class libepost_courseware extends libepost
	{	
		var $RecordID;
		var $ModuleType;
		var $ModuleCode;
		var $ModuleDetails = array();
		var $isFrom;
		
		function libepost_courseware($RecordID){
			# Initialize ePost Object
			$this->libepost();
			$this->RecordID = $RecordID;
			$this->isFrom = 'article_shelf';
			# Check if $ArticleShelfID is inputed or not
			$CoursewareAry = $this->get_courseware_ary($RecordID);
			
			# Initialize Newspaper Page Article Object
			$this->initialize_courseware_obj($CoursewareAry);
		}
		function get_courseware_ary($RecordID){
			$sql = "
				SELECT
					ModuleType,
					ModuleCode,
					ThemeCode,
					TopicCode
				FROM
					EPOST_WRITING
				WHERE
					LevelCode = 'C'
				AND
					CoursewareRecordID = '$RecordID'
				
			";
			$result_ary = current($this->returnArray($sql));
			return $result_ary;
		}
		function initialize_courseware_obj($CoursewareAry){
			$this->ModuleType = $CoursewareAry['ModuleType'];
			$this->ModuleCode = $CoursewareAry['ModuleCode'];
			list($key,$value) = explode('_',$CoursewareAry['ThemeCode']);
			$this->ModuleDetails[$key] = $value;
			list($key,$value) = explode('_',$CoursewareAry['TopicCode']);
			$this->ModuleDetails[$key] = $value;
		}
		function retrieve_writing_with_style_in_div($content){
			global $cfg_ePost;
			$x = '';
			$style_arr = $cfg_ePost[$this->ModuleType][$this->ModuleCode][$this->ModuleDetails['unit']];
			switch($this->ModuleCode){
				case 'poemsandsongs':
					$writing_arr = explode('|=|',$content);
					$writing_num = count($writing_arr);
					if($writing_num>0){
						$x .= ($this->isFrom=='article_shelf')?'<div '.(!empty($style_arr['css'])?'style="'.$style_arr['css'].'"':'').'>':'';
						if($style_arr['use_default']){
							$x .= ($this->isFrom=='article_shelf')?nl2br(intranet_undo_htmlspecialchars($writing_arr[1])):intranet_undo_htmlspecialchars($writing_arr[1]);
						}else{
							for($i=2;$i<$writing_num;$i++){ //first 2 fields are title n content string 
								$x .= ($this->isFrom=='article_shelf')?$style_arr['insert_before'][$i-2]:'';
								if(!empty($style_arr['line_style'][$i-2])){
									$x .= ($this->isFrom=='article_shelf')?'<span style="'.$style_arr['line_style'][$i-2].'">':'';
									$x .= $writing_arr[$i];
									$x .= ($this->isFrom=='issue')?'\n':'';
									$x .= ($this->isFrom=='article_shelf')?'</span>':'';
								}else{
									$x .= $writing_arr[$i];
									$x .= ($this->isFrom=='issue')?'\n':'';
								}
								$x .= ($this->isFrom=='article_shelf')?$style_arr['insert_after'][$i-2]:'';
							}
						}
						$x .= ($this->isFrom=='article_shelf')?'</div>':'';
						
					}
				break;
			}
			return $x;
		}
		
	}
}