<?php
# using : Paul

/* Modification Log:
 * 2019-05-13 (Paul):
 * 	- add single quote for variables in SQL query
 */

if (!defined("LIBEPOSTSTUDENTEDITOR_DEFINED"))         // Preprocessor directives
{
	define("LIBEPOSTSTUDENTEDITOR_DEFINED",true);
	include_once($intranet_root."/includes/ePost/config/config.php");
	
	class libepost_studenteditors extends libepost
	{
		var $StudentEditors;
		
		function libepost_studenteditors(){
			# Initialize ePost Object
			$this->libepost();
			
			$studenteditors_raw_ary = $this->get_studenteditors_raw_ary();
			
			for($i=0;$i<count($studenteditors_raw_ary);$i++){
				$this->StudentEditors[$i] = new libepost_studenteditor(0, $studenteditors_raw_ary[$i]);
			}
		}
		
		function get_studenteditors_raw_ary(){
			global $cfg_ePost;
			
			$sql = "SELECT
						ese.EditorID,
						ese.UserID,
						ese.Status,
						ese.InputDate,
						ese.ModifiedBy,
						ese.ModifiedDate,
						".getNameFieldByLang('eiu.')." AS UserName,
						eiu.EnglishName,
						eiu.ClassName,
						eiu.ClassNumber,
						".getNameFieldByLang('miu.')." AS Modified_User,
						miu.EnglishName AS Modified_EnglishName,
						miu.ClassName AS Modified_ClassName,
						miu.ClassNumber AS Modified_ClassNumber
					FROM
						EPOST_STUDENT_EDITOR AS ese INNER JOIN
						INTRANET_USER AS eiu ON ese.UserID = eiu.UserID INNER JOIN
						INTRANET_USER AS miu ON ese.ModifiedBy = miu.UserID
					WHERE
						ese.Status = '".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']."'
					ORDER BY
						eiu.ClassName , eiu.ClassNumber, eiu.EnglishName";
			$editor_ary = $this->returnArray($sql);
			
			if(count($editor_ary)>0){
				#
				$sql = "SELECT
							esem.EditorID, encr.RequestID, encr.Topic
						FROM
							EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
							EPOST_NONCOURSEWARE_REQUEST AS encr On esem.ManageItemID = encr.RequestID
						WHERE
							esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Request']."' AND
							esem.Status = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."' AND
							encr.Status = '".$cfg_ePost['BigNewspaper']['NonCoursewareRequest_status']['exist']."'";
				$request_raw_ary = $this->returnArray($sql);
				
				# 
				$sql = "SELECT
							esem.EditorID, en.NewspaperID, en.Title, en.Name
						FROM
							EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
							EPOST_NEWSPAPER AS en On esem.ManageItemID = en.NewspaperID
						WHERE
							esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."' AND
							esem.Status = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."' AND
							en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."'";
				$newspaper_raw_ary = $this->returnArray($sql);
				
				for($i=0;$i<count($editor_ary);$i++){
					$EditorID 	   = $editor_ary[$i]['EditorID'];
					$request_ary   = array();
					$newspaper_ary = array();
					
					for($j=0;$j<count($request_raw_ary);$j++){
						list($tmp_EditorID, $tmp_RequestID, $tmp_Topic) = $request_raw_ary[$j];
						if($EditorID==$tmp_EditorID){
							$request_ary[$tmp_RequestID] = $tmp_Topic;
						}
					}
					$editor_ary[$i]['ManagingRequest'] = $request_ary;
					
					for($j=0;$j<count($newspaper_raw_ary);$j++){
						list($tmp_EditorID, $tmp_NewspaperID, $tmp_Title, $tmp_Name) = $newspaper_raw_ary[$j];
						if($EditorID==$tmp_EditorID){
							$newspaper_ary[$tmp_NewspaperID] = $tmp_Title.' - '.$tmp_Name;
						}
					}
					$editor_ary[$i]['ManagingNewspaper'] = $newspaper_ary;
				}
			}
			
			return $editor_ary;
		}
	}
	
	class libepost_studenteditor extends libepost_studenteditors
	{
		var $EditorID;
		var $UserID;
		var $ManagingRequest;
		var $ManagingNewspaper;
		var $Status;
		var $InputDate;
		var $ModifiedBy;
		var $ModifiedDate;
		var $UserName;
		var $EnglishName;
		var $ClassName;
		var $ClassNumber;
		var $Modified_User;
		var $Modified_EnglishName;
		var $Modified_ClassName;
		var $Modified_ClassNumber;
		
		function libepost_studenteditor($EditorID=0, $Studenteditor_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $EditorID is inputed or not
			$Studenteditor_Data_ary = $EditorID? $this->get_studenteditor_raw_ary($EditorID) : $Studenteditor_Data_ary;
			
			# Initialize Student Editor Object
			$this->initialize_studenteditor_obj($Studenteditor_Data_ary);
		}
		
		function initialize_studenteditor_obj($Studenteditor_Data_ary){
			$this->EditorID 			= $Studenteditor_Data_ary['EditorID'];
			$this->UserID 				= $Studenteditor_Data_ary['UserID'];
			$this->ManagingRequest		= $Studenteditor_Data_ary['ManagingRequest'];
			$this->ManagingNewspaper	= $Studenteditor_Data_ary['ManagingNewspaper'];
			$this->Status 				= $Studenteditor_Data_ary['Status'];
			$this->InputDate 			= $Studenteditor_Data_ary['InputDate'];
			$this->ModifiedBy 			= $Studenteditor_Data_ary['ModifiedBy'];
			$this->ModifiedDate 		= $Studenteditor_Data_ary['ModifiedDate'];
			$this->UserName 			= $Studenteditor_Data_ary['UserName'];
			$this->EnglishName 			= $Studenteditor_Data_ary['EnglishName'];
			$this->ClassName 			= $Studenteditor_Data_ary['ClassName'];
			$this->ClassNumber 			= $Studenteditor_Data_ary['ClassNumber'];
			$this->Modified_User		= $Studenteditor_Data_ary['Modified_User'];
			$this->Modified_EnglishName = $Studenteditor_Data_ary['Modified_EnglishName'];
			$this->Modified_ClassName 	= $Studenteditor_Data_ary['Modified_ClassName'];
			$this->Modified_ClassNumber = $Studenteditor_Data_ary['Modified_ClassNumber'];
		}
		
		function get_studenteditor_raw_ary($EditorID){
			global $cfg_ePost;
			
			$sql = "SELECT
						ese.EditorID,
						ese.UserID,
						ese.Status,
						ese.InputDate,
						ese.ModifiedBy,
						ese.ModifiedDate,
						".getNameFieldByLang('eiu.')." AS UserName,
						eiu.EnglishName,
						eiu.ClassName,
						eiu.ClassNumber,
						".getNameFieldByLang('miu.')." AS Modified_User,
						miu.EnglishName AS Modified_EnglishName,
						miu.ClassName AS Modified_ClassName,
						miu.ClassNumber AS Modified_ClassNumber
					FROM
						EPOST_STUDENT_EDITOR AS ese INNER JOIN
						INTRANET_USER AS eiu ON ese.UserID = eiu.UserID INNER JOIN
						INTRANET_USER AS miu ON ese.ModifiedBy = miu.UserID
					WHERE
						EditorID = '$EditorID'";
			$editor_ary = current($this->returnArray($sql));
			
			#
			$sql = "SELECT
						esem.EditorID, encr.RequestID, encr.Topic,
					FROM
						EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
						EPOST_NONCOURSEWARE_REQUEST AS encr On esem.ManageItemID = encr.RequestID
					WHERE
						esem.EditorID = '$EditorID' AND
						esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Request']."' AND
						esem.Status = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."'
						encr.Status = '".$cfg_ePost['BigNewspaper']['NonCoursewareRequest_status']['exist']."'";
			$request_raw_ary = $this->returnArray($sql);
				
			# 
			$sql = "SELECT
						esem.EditorID, en.NewspaperID, en.Title, en.Name
					FROM
						EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
						EPOST_NEWSPAPER AS en On esem.ManageItemID = en.NewspaperID
					WHERE
						esem.EditorID = '$EditorID' AND
						esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."' AND
						esem.Status = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."'
						en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."'";
			$newspaper_raw_ary = $this->returnArray($sql);
			
			$EditorID 	   = $editor_ary['EditorID'];
			$request_ary   = array();
			$newspaper_ary = array();
					
			for($j=0;$j<count($request_raw_ary);$j++){
				list($tmp_EditorID, $tmp_RequestID, $tmp_Topic) = $request_raw_ary[$j];
				if($EditorID==$tmp_EditorID){
					$request_ary[$tmp_RequestID] = $tmp_Topic;
				}
			}
			$editor_ary['ManagingRequest'] = $request_ary;
					
			for($j=0;$j<count($newspaper_raw_ary);$j++){
				list($tmp_EditorID, $tmp_NewspaperID, $tmp_Title, $tmp_Name) = $newspaper_raw_ary[$j];
				if($EditorID==$tmp_EditorID){
					$newspaper_ary[$tmp_NewspaperID] = $tmp_Title.' - '.$tmp_Name;
				}
			}
			$editor_ary['ManagingNewspaper'] = $request_ary;
			
			return $editor_ary;
		}
		
		function update_studenteditor_db(){
			global $UserID, $cfg_ePost;
			
			if($this->EditorID){
				$EditorID 	 = $this->EditorID;
				$InputDate   = $this->InputDate;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_STUDENT_EDITOR
						SET
							UserID = '".$this->UserID."',
							Status = '".$this->Status."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							EditorID = '".$this->EditorID."'";
				$result = $this->db_db_query($sql);
			}
			else{
				$InputDate   = date('Y-m-d H:i:s');
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_STUDENT_EDITOR
							(
								UserID, Status, InputDate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								'".$this->UserID."', '".$this->Status."', '$InputDate', '$UserID', '$ModifedDate'
							)";
				$result = $this->db_db_query($sql);
				$EditorID = $this->db_insert_id();
			}
			
			if($result){
				$sql = "SELECT ".getNameFieldByLang('iu.')." AS UserName, iu.EnglishName, iu.ClassName, iu.ClassNumber FROM INTRANET_USER AS iu WHERE iu.UserID = '".$this->UserID."'";
				$Editor_ary = current($this->returnArray($sql));
				
				$sql = "SELECT ".getNameFieldByLang('iu.')." AS Modified_User, iu.EnglishName, iu.ClassName, iu.ClassNumber FROM INTRANET_USER AS iu WHERE iu.UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				
				$this->EditorID	    		 = $EditorID;
				$this->UserName				 = $Editor_ary['UserName'];
				$this->EnglishName			 = $Editor_ary['EnglishName'];
				$this->ClassName			 = $Editor_ary['ClassName'];
				$this->ClassNumber			 = $Editor_ary['ClassNumber'];
				$this->InputDate			 = $InputDate;
				$this->ModifiedBy   		 = $UserID;
				$this->ModifiedDate 		 = $ModifedDate;
				$this->Modified_User		 = $Editor_ary['Modified_User'];
				$this->Modified_EnglishName  = $ModifiedBy_ary['EnglishName'];
				$this->Modified_ClassName 	 = $ModifiedBy_ary['ClassName'];
				$this->Modified_ClassNumber  = $ModifiedBy_ary['ClassNumber'];
			}
			
			return $result;
		}
		
		function delete_studenteditor(){
			global $cfg_ePost, $UserID;
			
			$sql = "UPDATE
						EPOST_STUDENT_EDITOR
					SET
						Status = '".$cfg_ePost['BigNewspaper']['StudentEditor_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						EditorID = '".$this->EditorID."'";
			$result = $this->db_db_query($sql);
			
			$sql = "UPDATE
						EPOST_STUDENT_EDITOR_MANAGING
					SET
						Status = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						EditorID = '".$this->EditorID."'";
			$result = $this->db_db_query($sql);
			
			return $result;
		}
	}
}