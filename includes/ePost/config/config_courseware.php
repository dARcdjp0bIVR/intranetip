<?php
/*-----------------Display Content-----------------*/		
$cfg_ePost['content']['poemsandsongs']['1'] = array(
                                        "use_default"=>true
                                ); 
$cfg_ePost['content']['poemsandsongs']['2'] = array(
                                        "use_default"=>true
                                ); 								
$cfg_ePost['content']['poemsandsongs']['3'] = array(
										"use_default"=>true,
                                        "css"=>"text-align:center"
                                ); 
$cfg_ePost['content']['poemsandsongs']['5'] = array(
										"use_default"=>true,
                                        "css"=>"text-align:center"
                                ); 
$cfg_ePost['content']['poemsandsongs']['6'] = array(
										"use_default"=>true,
                                        "css"=>"text-align:center"
                                ); 
$cfg_ePost['content']['poemsandsongs']['8'] = array(
										"use_default"=>false,
                                        "css"=>"text-align:center",
										"line_style"=>array(
											"2"=>"padding-left: 50px;",
											"3"=>"padding-left: 50px;"
										),
										"insert_after"=>array(
											"0"=>"<br/>",
											"1"=>"<br/>",
											"2"=>"<br/>",
											"3"=>"<br/>",
											"4"=>"<br/>"
										),
                                ); 
$cfg_ePost['content']['poemsandsongs']['9'] = array(
										"use_default"=>false,
                                        "css"=>"text-align:center",
										"insert_before"=>array(
											"4"=>"<br/>"
										),
										"insert_after"=>array(
											"0"=>"<br/>",
											"1"=>"<br/>",
											"2"=>"<br/>",
											"3"=>"<br/>",
											"4"=>"<br/>",
											"5"=>"<br/>",
											"6"=>"<br/>",
										),
										"line_style"=>array(
											"4"=>"font-style: italic;",
											"5"=>"font-style: italic;",
											"6"=>"font-style: italic;",
											"7"=>"font-style: italic;"
										)										
                                ); 	
$cfg_ePost['content']['poemsandsongs']['10'] = array(
                                        "use_default"=>true
                                ); 								
/*-----------------Display Content End-----------------*/
?>