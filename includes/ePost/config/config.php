<?php
$cfg_ePost['show_newspaper_demo'] = true;

$cfg_ePost['cert_image'] = array('3_1','4_1','4_3','4_4','4_5','5_1','5_2','5_3','5_3_1_1','5_3_1_2','5_3_2_1','5_3_2_2','5_3_3_1','5_3_3_2','5_3_4_1','5_3_4_2','5_3_5_1','5_3_5_2','5_3_5_3','5_3_6_1','5_3_6_2','5_3_7_1','5_3_7_2','5_3_7_3');
$cfg_ePost['card_image'] = array('4_2');

$cfg_ePost['school_name'] = 'C.C.C. Kei Wa Primary School (Kowloon Tong)';

$cfg_ePost['head_title']['default'] = $cfg_ePost['school_name']." ::";
$cfg_ePost['head_title']['P3'] 		= "I am Creative ::".$cfg_ePost['school_name']." ::";
$cfg_ePost['head_title']['P4'] 		= "I am a News Writer ::".$cfg_ePost['school_name']." ::";
$cfg_ePost['head_title']['P5'] 		= "I am a News Anchor ::".$cfg_ePost['school_name']." ::";

$cfg_ePost['title']['P3'] = "I am Creative";
$cfg_ePost['title']['P4'] = "I am a News Writer";
$cfg_ePost['title']['P5'] = "I am a News Anchor";

$cfg_ePost['module_type']['training'] = 'training';
$cfg_ePost['module_type']['content']  = 'content';

$cfg_ePost['module_type_display'][$cfg_ePost['module_type']['training']] = 'Training Modules';
$cfg_ePost['module_type_display'][$cfg_ePost['module_type']['content']]  = 'Exercise Modules';

$cfg_ePost['training_flash'][] = '3_1';
$cfg_ePost['training_flash'][] = '3_2';
$cfg_ePost['training_flash'][] = '4_1';
$cfg_ePost['training_flash'][] = '4_2';
$cfg_ePost['training_flash'][] = '4_3';
$cfg_ePost['training_flash'][] = '4_4';
$cfg_ePost['training_flash'][] = '4_5';
$cfg_ePost['training_flash'][] = '5_1';
$cfg_ePost['training_flash'][] = '5_2';
$cfg_ePost['training_flash'][] = '5_3';

$cfg_ePost['sql_config']['topic_code_delimiter']= '|=|';
$cfg_ePost['sql_config']['ReadFlag_delimiter']  = '|=|';
$cfg_ePost['sql_config']['Content_delimiter_1'] = '|=|';
$cfg_ePost['sql_config']['Content_delimiter_2'] = '|+|';
$cfg_ePost['sql_config']['Attachment_delimiter_1'] = '|=|';
$cfg_ePost['sql_config']['Status']['deleted'] = 2;
$cfg_ePost['sql_config']['Status']['submitted'] = 1;
$cfg_ePost['sql_config']['Status']['drafted'] 	= 0;
$cfg_ePost['sql_config']['Status']['redo'] 		= -1;
$cfg_ePost['sql_config']['IsRedo']['true'] 		= 1;
$cfg_ePost['sql_config']['IsRedo']['false'] 	= 0;

/* InShowBoard field is used to store the date that the teacher post the article in show board now
$cfg_ePost['sql_config']['InShowBoard']['Yes'] 	= 1;
$cfg_ePost['sql_config']['InShowBoard']['No']  	= 0;
*/

$cfg_ePost['sql_config']['Content_headline']  	= 'headline';
$cfg_ePost['sql_config']['Content_lead']	  	= 'lead';
$cfg_ePost['sql_config']['Content_nut_graph']  	= 'nut_graph';
$cfg_ePost['sql_config']['Content_less_important_info'] = 'less_important_info';

# For P3 > Poster Writing
$cfg_ePost['sql_config']['Content_event_date'] = 'event_date';
$cfg_ePost['sql_config']['Content_event_time'] = 'event_time';
$cfg_ePost['sql_config']['Content_event_venue'] = 'event_venue';
$cfg_ePost['sql_config']['Content_programmes'] = 'programmes';
$cfg_ePost['sql_config']['Content_organiser'] = 'organiser';
$cfg_ePost['sql_config']['Content_contact_person'] = 'contact_person';

# For P4 > Comics Writing
$cfg_ePost['sql_config']['Content_part1_A'] = 'part1_A';
$cfg_ePost['sql_config']['Content_part1_B'] = 'part1_B';
$cfg_ePost['sql_config']['Content_part2_A'] = 'part2_A';
$cfg_ePost['sql_config']['Content_part2_B'] = 'part2_B';
$cfg_ePost['sql_config']['Content_part3_A'] = 'part3_A';
$cfg_ePost['sql_config']['Content_part3_B'] = 'part3_B';
$cfg_ePost['sql_config']['Content_part4_A'] = 'part4_A';
$cfg_ePost['sql_config']['Content_part4_B'] = 'part4_B';


$cfg_ePost['showboard']['NoOfCharsInArticle'] = 92;

$cfg_ePost['writer_photo']['max_width']  = 260;
$cfg_ePost['writer_photo']['max_height'] = 250;

$cfg_ePost['writer_photo_big']['max_width'] = 490;
$cfg_ePost['writer_photo_big']['max_height'] = 300;

$cfg_ePost['skip_update_title'] = "[=No Title=]";

$cfg_ePost['comment_rubrics'] = $Lang['ePost']['CommentRubrics'];

# Noncourseware request config
$cfg_ePost['Noncourseware_Request']['Status']['All'] 			   = 0;
$cfg_ePost['Noncourseware_Request']['Status']['DeadlinePassed']    = 1;
$cfg_ePost['Noncourseware_Request']['Status']['DeadlineNotPassed'] = 2;

$cfg_ePost['max_upload']['module_1'] = 2;
$cfg_ePost['max_upload']['module_2'] = 1;
$cfg_ePost['max_upload']['poemsandsongs'] = 2;
# Include Newspaper Config
include('config_newspaper.php');

?>