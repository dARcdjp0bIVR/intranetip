<?php
# using : Paul

/* Modification Log:
 * 2019-05-13 (Paul):
 * 	- add single quote for variables in SQL query
 * 
 */

if (!defined("LIBEPOSTARTICLESHELF_DEFINED"))         // Preprocessor directives
{
	define("LIBEPOSTARTICLESHELF_DEFINED",true);
	include_once($intranet_root."/includes/ePost/config/config.php");
	
	class libepost_articleshelf extends libepost
	{
		var $ArticleShelfID;
		var $WritingID;
		var $Title;
		var $Content;
		var $Attachment;
		var $Caption;
		var $IsAppendix;
		var $Status;
		var $InputDate;
		var $InputBy;
		var $InputByUser;
		var $InputEnglishName;
		var $InputClassName;
		var $InputClassNumber;
		var $ModifiedDate;
		var $ModifiedBy;
		var $ModifiedByUser;
		var $ModifiedEnglishName;
		var $ModifiedClassName;
		var $ModifiedClassNumber;
		var $Enjoy;
		var $isFrom;
				
		function libepost_articleshelf($ArticleShelfID=0, $ArticleShelf_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $ArticleShelfID is inputed or not
			$ArticleShelf_Data_ary = $ArticleShelfID? $this->get_articleshelf_raw_ary($ArticleShelfID) : $ArticleShelf_Data_ary;
			
			# Initialize Newspaper Page Article Object
			$this->initialize_articleshelf_obj($ArticleShelf_Data_ary);
			$this->isFrom = 'article_shelf';
		}
		
		function initialize_articleshelf_obj($ArticleShelf_Data_ary){
			global $cfg_ePost;
			
			$this->ArticleShelfID      	= $ArticleShelf_Data_ary['ArticleShelfID'];
			$this->WritingID 		   	= $ArticleShelf_Data_ary['WritingID'];
			$this->Title 			   	= $ArticleShelf_Data_ary['Title'];
			$this->Content 				= $ArticleShelf_Data_ary['Content'];
			###HANDLE POEMS AND SONGS DATA###
			if($ArticleShelf_Data_ary['CoursewareRecordID']){
				include_once("libepost_courseware.php");
				$libcourseware = new libepost_courseware($ArticleShelf_Data_ary['CoursewareRecordID']);
				$libcourseware->isFrom = $this->isFrom;
				$this->Content = $libcourseware->retrieve_writing_with_style_in_div($this->Content);
			}
			###HANDLE POEMS AND SONGS DATA###
			$this->Attachment 		   = $ArticleShelf_Data_ary['Attachment'];
			$this->Caption 			   = $ArticleShelf_Data_ary['Caption'];
			$this->IsAppendix		   = $ArticleShelf_Data_ary['IsAppendix'];
			$this->Status 			   = $ArticleShelf_Data_ary['Status'];
			$this->InputDate 		   = $ArticleShelf_Data_ary['InputDate'];
			$this->InputBy 			   = $ArticleShelf_Data_ary['InputBy'];
			$this->InputByUser		   = $ArticleShelf_Data_ary['InputByUser'];
			$this->InputEnglishName    = $ArticleShelf_Data_ary['InputEnglishName'];
			$this->InputClassName 	   = $ArticleShelf_Data_ary['InputClassName'];
			$this->InputClassNumber    = $ArticleShelf_Data_ary['InputClassNumber'];
			$this->ModifiedDate 	   = $ArticleShelf_Data_ary['ModifiedDate'];
			$this->ModifiedBy 		   = $ArticleShelf_Data_ary['ModifiedBy'];
			$this->ModifiedByUser	   = $ArticleShelf_Data_ary['ModifiedByUser'];
			$this->ModifiedEnglishName = $ArticleShelf_Data_ary['ModifiedEnglishName'];
			$this->ModifiedClassName   = $ArticleShelf_Data_ary['ModifiedClassName'];
			$this->ModifiedClassNumber = $ArticleShelf_Data_ary['ModifiedClassNumber'];
			$this->Enjoy		   = $ArticleShelf_Data_ary['Enjoy'];			
		}
		
		function get_articleshelf_raw_ary($ArticleShelfID){
			global $cfg_ePost;
			$sql = "SELECT
						ea.ArticleShelfID,
						ea.WritingID,
						ea.Title,
						ea.Content,
						ea.Attachment,
						ea.Caption,
						ea.IsAppendix,
						ea.Status,
						ea.InputDate,
						ea.InputBy,
						ea.Enjoy,
						".getNameFieldByLang('iiu.')." AS InputByUser,
						iiu.EnglishName AS InputEnglishName,
						iiu.ClassName AS InputClassName,
						iiu.ClassNumber AS InputClassNumber,
						ea.ModifiedDate,
						ea.ModifiedBy,
						".getNameFieldByLang('miu.')." AS ModifiedByUser,
						miu.EnglishName AS ModifiedEnglishName,
						miu.ClassName AS ModifiedClassName,
						miu.ClassNumber AS ModifiedClassNumber,
						ea.CoursewareRecordID
					FROM
						EPOST_ARTICLE_SHELF AS ea INNER JOIN
						INTRANET_USER AS iiu ON ea.InputBy = iiu.UserID INNER JOIN
						INTRANET_USER AS miu ON ea.ModifiedBy = miu.UserID
					WHERE
						ea.ArticleShelfID = '$ArticleShelfID'";
			$result_ary = current($this->returnArray($sql));
			
			$sql = "
				SELECT 
					WritingID,
					AttachmentID,
					Attachment,
					ArticleShelfCaption as Caption
				FROM
					EPOST_WRITING_ATTACHMENT 
				WHERE ArticleShelfID = '".$ArticleShelfID."'
			"; 
			$attachmentResultAry = $this->returnArray($sql);
			$attachmentCnt = count($attachmentResultAry);
			$attachmentAry = array();
			for($i=0;$i<$attachmentCnt;$i++){
				list($_writingId,$_attachmentId,$_attachment,$_caption) = $attachmentResultAry[$i];
				$attachmentAry[$_attachmentId] = array('writingId'=>$_writingId,'articleShelfId'=>$ArticleShelfID,'attachmentId'=>$_attachmentId,'attachment'=>$_attachment,'caption'=>$_caption);
			}
			$result_ary['Attachment'] = $attachmentAry;	
			return $result_ary;
		}
		
		function format_attachment_path($attachment,$Is_http_path=false,$isWritingAttachment=true){
			global $PATH_WRT_ROOT, $cfg_ePost;
	    	if(!empty($attachment)){
		    	if($isWritingAttachment)
		    		$path = ($Is_http_path? "/" : $PATH_WRT_ROOT)."file/ePost/writing_attachment/".$this->WritingID."/".($Is_http_path? rawurlencode(addslashes($attachment)) : $attachment);
		    	else
		    		$path = ($Is_http_path? "/" : $PATH_WRT_ROOT)."file/ePost/appendix_attachment/".$this->ArticleShelfID."/".($Is_http_path? rawurlencode(addslashes($attachment)) : $attachment);
	    	}
	    	return $path;
		}
		function remove_article_shelf_attachment($_attachmentId){
			global $PATH_WRT_ROOT;
			
			$dir = $PATH_WRT_ROOT.'file/ePost/appendix_attachment/'.$this->ArticleShelfID.'/';	
			if(is_dir($dir)){
				$cur_dir = opendir($dir);
				$file = $this->Attachment[$_attachmentId]['attachment'];
				if($file!='.' && $file!='..'){
					chmod($dir.$file, 0777);
					if(!is_dir($dir.$file)){
						$ext = getFileExtention($file);
						if($ext=='FLV'||$ext=='MOV'||$ext=='MP4'){
							list($filename,$filetype)=explode(".",$file);
							if(file_exists($dir.$filename.".flv"))	unlink($dir.$filename.".flv");
							if(file_exists($dir.$filename.".mov"))	unlink($dir.$filename.".mov");
							if(file_exists($dir.$filename.".mp4"))	unlink($dir.$filename.".mp4");
						}else{
							unlink($dir.$file);
						}
						$sql = "DELETE FROM EPOST_WRITING_ATTACHMENT WHERE AttachmentID = '".$_attachmentId."'";
						$this->db_db_query($sql);
						unset($this->Attachment[$_attachmentId]);
					}	
				}
			}		
		}
		function update_article_shelf_db(){
			global $UserID, $cfg_ePost;
			
			if($this->ArticleShelfID){
				$ArticleShelfID = $this->ArticleShelfID;
				$InputDate   	= $this->InputDate;
				$InputBy	 	= $this->InputBy;
				$ModifedDate 	= date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_ARTICLE_SHELF
						SET
							WritingID = '".$this->WritingID."',
							Title = '".$this->Title."',
							Content = '".$this->Content."',
							IsAppendix = '".$this->IsAppendix."',
							Status = '".$this->Status."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							ArticleShelfID = '".$this->ArticleShelfID."'";
				$result = $this->db_db_query($sql);
				if(count($this->Attachment)>0){
					foreach((array)$this->Attachment as $_attachmentId => $_attachmentPathAry){
						$sql = "UPDATE EPOST_WRITING_ATTACHMENT SET 
									ArticleShelfID='".$_attachmentPathAry['articleShelfId']."' ,
									ArticleShelfCaption='".$_attachmentPathAry['caption']."' ,
									ModifiedDate = NOW()
								WHERE AttachmentID = '".$_attachmentId."'";
						$this->db_db_query($sql);
					}
				}
			}
			else{
				$InputDate   = date('Y-m-d H:i:s');
				$InputBy	 = $UserID;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_ARTICLE_SHELF
							(
								WritingID, Title, Content, Attachment, Caption, IsAppendix,
								Status, Inputdate, InputBy, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								'".$this->WritingID."', '".$this->Title."', '".$this->Content."', '', '".$this->Caption."', '".$this->IsAppendix."',
								'".$this->Status."', '$InputDate', '$UserID', '$UserID', '$ModifedDate'
							)";
				$result = $this->db_db_query($sql);
				$ArticleShelfID = $this->db_insert_id();
			}
			
			
			if($result){
				$sql = "SELECT ".getNameFieldByLang('iu.')." AS InputByUser, iu.EnglishName, iu.ClassName, iu.ClassNumber FROM INTRANET_USER AS iu WHERE iu.UserID = '$InputBy'";
				$InputBy_ary = current($this->returnArray($sql));
				
				$sql = "SELECT ".getNameFieldByLang('iu.')." AS ModifiedByUser, iu.EnglishName, iu.ClassName, iu.ClassNumber FROM INTRANET_USER AS iu WHERE iu.UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				
				$this->ArticleShelfID	   = $ArticleShelfID;
				$this->InputDate	 	   = $InputDate;
				$this->InputBy			   = $InputBy;
				$this->InputByUser		   = $InputBy_ary['InputByUser'];
				$this->InputEnglishName	   = $InputBy_ary['EnglishName'];
				$this->InputClassName	   = $InputBy_ary['ClassName'];
				$this->InputClassNumber	   = $InputBy_ary['ClassNumber'];
				$this->ModifiedBy   	   = $UserID;
				$this->ModifiedByUser	   = $ModifiedBy_ary['ModifiedByUser'];
				$this->ModifiedDate 	   = $ModifedDate;
				$this->ModifiedEnglishName = $ModifiedBy_ary['EnglishName'];
				$this->ModifiedClassName   = $ModifiedBy_ary['ClassName'];
				$this->ModifiedClassNumber = $ModifiedBy_ary['ClassNumber'];
			}
			
			return $result;
		}
		
		function delete_article_shelf(){
			global $cfg_ePost, $UserID;
			if($this->IsAppendix==$cfg_ePost['BigNewspaper']['ArticleShelf_IsAppendix']['No']){
				$sql = "
						SELECT
							COUNT(*)
						FROM
							EPOST_NEWSPAPER_PAGE_ARTICLE
						WHERE
							Status = '".$cfg_ePost['BigNewspaper']['Article_status']['exist']."'
						AND
							ArticleShelfID = '".$this->ArticleShelfID."'";
				$result = current($this->returnVector($sql));
				
				if(count($this->Attachment)>0&&$this->isAppendix==$cfg_ePost['BigNewspaper']['ArticleShelf_IsAppendix']['No']){
					$sql = "
							UPDATE
								EPOST_WRITING_ATTACHMENT 
							SET
								ArticleShelfID = NULL,
								ArticleShelfCaption = NULL,
								ModifiedBy = '$UserID',
								ModifiedDate = NOW()
							WHERE
								ArticleShelfID = '".$this->ArticleShelfID."'
						";
					$result = $this->db_db_query($sql);				
				}
				$sql = "UPDATE
						EPOST_WRITING
					SET
						InNewspaper = NULL,
						modified = NOW(),
						ModifiedBy = '$UserID'
					WHERE
						RecordID = '".$this->WritingID."'";
				$result = $this->db_db_query($sql);					
				$sql = "
						DELETE FROM
							EPOST_ARTICLE_SHELF 
						WHERE
							ArticleShelfID = '".$this->ArticleShelfID."'
					";
				$result = $this->db_db_query($sql);	
							
			}else{
				$sql = "UPDATE
						EPOST_ARTICLE_SHELF
					SET
						Status = '".$cfg_ePost['BigNewspaper']['ArticleShelf_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						ArticleShelfID = '".$this->ArticleShelfID."'";
				$result = $this->db_db_query($sql);
			}
			return $result;
		}
			
	}
	
}