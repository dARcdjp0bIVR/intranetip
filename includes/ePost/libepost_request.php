<?php
# using : Siuwan 

/* Modification Log:
 * 
 */

if (!defined("LIBEPOSTREQUEST_DEFINED"))         // Preprocessor directives
{
	define("LIBEPOSTREQUEST_DEFINED",true);
	include_once($intranet_root."/includes/ePost/config/config.php");
	
	class libepost_requests extends libepost
	{
		var $Requests = array();
		
		function libepost_requests($ignore_status=false){
			# Initialize ePost Object
			$this->libepost();
			
			$requests_raw_ary = $this->get_requests_raw_ary($ignore_status);
			
			for($i=0;$i<count($requests_raw_ary);$i++){
				$this->Requests[$i] = new libepost_request(0, $requests_raw_ary[$i]);
			}
		}
		
		function get_requests_raw_ary($ignore_status=false){
			global $cfg_ePost;
			
			# Get Request Raw Data
			$sql = "SELECT
						encr.RequestID,
						encr.Topic,
						encr.Type,
						encr.Deadline,
						encr.TargetLevel,
						encr.Description,
						encr.Status,
						encr.InputDate,
						encr.ModifiedBy,
						encr.ModifiedDate,
						".getNameFieldByLang("iu.")." as ModifiedByUser
					FROM
						EPOST_NONCOURSEWARE_REQUEST AS encr INNER JOIN
						INTRANET_USER AS iu ON encr.ModifiedBy = iu.UserID ";
			if(!$ignore_status){
			    $sql.= " WHERE
							encr.Status = ".$cfg_ePost['BigNewspaper']['NonCoursewareRequest_status']['exist']."
						";
			}
			$sql .= "
					ORDER BY
						encr.Deadline ASC";
			
			$result_ary = $this->returnArray($sql);
			
			$sql = "SELECT
						RequestID,
						COUNT(RequestID) AS ArticleSubmitted
					FROM
						EPOST_WRITING
        		    WHERE
        				RequestID != 0 AND Status = ".$cfg_ePost['sql_config']['Status']['submitted']." 
        			GROUP BY
        				RequestID";
			$writingAry = $this->returnArray($sql);
			
			$articleSubmittedOfRequestMapping = array();
			for($i = 0; $i < count($writingAry); $i++){
			    $articleSubmittedOfRequestMapping[$writingAry[$i]['RequestID']] = $writingAry[$i]['ArticleSubmitted'];
			}
			
			$sql = "SELECT
						encr.RequestID,
						COUNT(eas.ArticleShelfID) AS ArticleAddedToShelf
					FROM
			            EPOST_ARTICLE_SHELF AS eas INNER JOIN
            			EPOST_WRITING AS ew ON ew.Status = ".$cfg_ePost['sql_config']['Status']['submitted']." AND ew.RecordID = eas.WritingID INNER JOIN
            			EPOST_NONCOURSEWARE_REQUEST AS encr ON encr.RequestID = ew.RequestID " . 
            			(!$ignore_status ? "AND encr.Status = " . $cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist'] : "" ) . " 
            		GROUP BY
			            encr.RequestID
					";
			$articleShelfAry = $this->returnArray($sql);
			
			$articleAddedToShelfOfRequestMapping = array();
			for($i = 0; $i < count($articleShelfAry); $i++){
			    $articleAddedToShelfOfRequestMapping[$articleShelfAry[$i]['RequestID']] = $articleShelfAry[$i]['ArticleAddedToShelf'];
			}
			
			for($i = 0; $i < count($result_ary); $i++){
			    $result_ary[$i]['ArticleSubmitted'] = isset($articleSubmittedOfRequestMapping[$result_ary[$i]['RequestID']]) ? $articleSubmittedOfRequestMapping[$result_ary[$i]['RequestID']] : 0;
			    $result_ary[$i]['ArticleAddedToShelf'] = isset($articleAddedToShelfOfRequestMapping[$result_ary[$i]['RequestID']]) ? $articleAddedToShelfOfRequestMapping[$result_ary[$i]['RequestID']] : 0;
			}
			
			# Get Request Student Editor
			$sql = "SELECT
						esem.ManageItemID,
						iu.UserID,
						".getNameFieldByLang("iu.")." as UserName,
						iu.EnglishName,
						iu.ClassName,
						iu.ClassNumber
					FROM
						EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
						EPOST_STUDENT_EDITOR AS ese ON esem.EditorID = ese.EditorID INNER JOIN
						INTRANET_USER AS iu ON ese.UserID = iu.UserID
					WHERE
						ese.Status = ".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']." AND
						esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Request']."' AND
						esem.Status = ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."
					ORDER BY
						iu.ClassName, iu.ClassNumber, iu.EnglishName";
			$StudentEditor_ary = $this->returnArray($sql);
			
			for($i=0;$i<count($result_ary);$i++){
				$result_ary[$i]['TargetLevel'] 			= $result_ary[$i]['TargetLevel']? explode($cfg_ePost['sql_config']['NonCoursewareRequest_TargetLevel_delimiter'], $result_ary[$i]['TargetLevel']) : array();
				$result_ary[$i]['StudentEditors'] 		= array();
				$result_ary[$i]['StudentEditorUserIDs'] = array();
				
				for($j=0;$j<count($StudentEditor_ary);$j++){
					if($result_ary[$i]['RequestID']==$StudentEditor_ary[$j]['ManageItemID']){
						$result_ary[$i]['StudentEditorUserIDs'][] = $StudentEditor_ary[$j]['UserID'];
						$result_ary[$i]['StudentEditors'][] 	  = array("UserID"	    => $StudentEditor_ary[$j]['UserID'],
																		  "UserName"    => $StudentEditor_ary[$j]['UserName'],
																		  "EnglishName" => $StudentEditor_ary[$j]['EnglishName'],
																		  "ClassName"   => $StudentEditor_ary[$j]['ClassName'],
																		  "ClassNumber" => $StudentEditor_ary[$j]['ClassNumber']);
					}
				}
			}
			
			return $result_ary;
		}
		
		/* function get_user_assigned_requests input parameter : $type
		 * $type = 0 -> All user assigned requests
		 * $type = 1 -> user assigned requests with deadline not yet passed
		 * $type = 2 -> user assigned requests with deadline passed
		 * */
		function get_user_assigned_requests($type=0){
			global $cfg_ePost;
			
			$requests_ary = array();
			
			for($i=0;$i<count($this->Requests);$i++){
				$RequestObj = $this->Requests[$i];
				
				$DeadlinePassed = strtotime($RequestObj->Deadline) <= time();
				
				switch($type){
					case $cfg_ePost['Noncourseware_Request']['Status']['All']: 
						$IsSkipped = false; 
						break;
					case $cfg_ePost['Noncourseware_Request']['Status']['DeadlineNotPassed']: 
						$IsSkipped = $DeadlinePassed; 
						break;
					case $cfg_ePost['Noncourseware_Request']['Status']['DeadlinePassed']: 
						$IsSkipped = !$DeadlinePassed; 
						break;
				}
				
				if(!$IsSkipped){
					if(is_array($RequestObj->TargetLevel) && count($RequestObj->TargetLevel)>0){
						
					}
					else{
						$requests_ary[] = $RequestObj;
					}
				}
			}
			
			return $requests_ary;
		}
		
		function get_managing_requests(){
			global $cfg_ePost, $UserID;
			
			$requests_ary = array();
			
			if($this->is_editor){
				if($this->user_obj->isTeacherStaff()){
					$requests_ary = $this->Requests;
				}
				else{
					for($i=0;$i<count($this->Requests);$i++){
						$RequestObj = $this->Requests[$i];
						if(in_array($UserID, $RequestObj->StudentEditorUserIDs)){
							$requests_ary[] = $RequestObj;
						}
					}
				}
			}
			
			return $requests_ary;
		}
	}
	
	class libepost_request extends libepost_requests
	{
		var $RequestID;
		var $Topic;
		var $Type;
		var $LevelCode;
		var $ModuleType;
		var $ModuleCode;
		var $Deadline;
		var $TargetLevel;
		var $Description;
		var $ArticleSubmitted;
		var $ArticleAddedToShelf;
		var $Status;
		var $InputDate;
		var $ModifiedBy;
		var $ModifiedDate;
		var $ModifiedByUser;
		var $StudentEditorUserIDs;
		var $StudentEditors;
		
		function libepost_request($RequestID=0, $Request_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $ArticleID is inputed or not
			$Request_Data_ary = $RequestID? $this->get_request_raw_ary($RequestID) : $Request_Data_ary;
			
			# Initialize Newspaper Page Article Object
			$this->initialize_request_obj($Request_Data_ary);
		}
		
		function initialize_request_obj($Request_Data_ary){
			global $cfg_ePost;
			
			$this->RequestID   			= $Request_Data_ary['RequestID'];
			$this->Topic	   			= $Request_Data_ary['Topic'];
			$this->Type		   			= $Request_Data_ary['Type'];
			$this->LevelCode			= $cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types'][$this->Type]['search_attr']['level'];
			$this->ModuleType			= 'content';
			$this->ModuleCode			= $cfg_ePost['BigNewspaper']['ArticleType']['non_courseware']['types'][$this->Type]['search_attr']['content'];
			$this->ThemeCode			= 'theme_1';
			$this->TopicCode			= 'topic_1';
			$this->Deadline    			= $Request_Data_ary['Deadline'];
			$this->TargetLevel 			= $Request_Data_ary['TargetLevel'];
			$this->Description 			= $Request_Data_ary['Description'];
			$this->ArticleSubmitted 	= $Request_Data_ary['ArticleSubmitted'];
			$this->ArticleAddedToShelf 	= $Request_Data_ary['ArticleAddedToShelf'];
			$this->Status				= $Request_Data_ary['Status'];
			$this->InputDate   			= $Request_Data_ary['InputDate'];
			$this->ModifiedBy   		= $Request_Data_ary['ModifiedBy'];
			$this->ModifiedDate 		= $Request_Data_ary['ModifiedDate'];
			$this->ModifiedByUser  		= $Request_Data_ary['ModifiedByUser'];
			$this->StudentEditorUserIDs	= $Request_Data_ary['StudentEditorUserIDs'];
			$this->StudentEditors 		= $Request_Data_ary['StudentEditors'];
		}
		
		function get_request_raw_ary($RequestID){
			global $cfg_ePost;
			
			$sql = "SELECT
						encr.RequestID,
						encr.Topic,
						encr.Type,
						encr.Deadline,
						encr.TargetLevel,
						encr.Description,
						COUNT(ew.RequestID) AS ArticleSubmitted,
						COUNT(eas.ArticleShelfID) AS ArticleAddedToShelf,
						encr.Status,
						encr.InputDate,
						encr.ModifiedBy,
						encr.ModifiedDate,
						".getNameFieldByLang("iu.")." as ModifiedByUser
					FROM
						EPOST_NONCOURSEWARE_REQUEST AS encr INNER JOIN
						INTRANET_USER AS iu ON encr.ModifiedBy = iu.UserID LEFT JOIN
						EPOST_WRITING AS ew ON ew.RequestID != 0 AND ew.Status = ".$cfg_ePost['sql_config']['Status']['submitted']." AND encr.RequestID = ew.RequestID LEFT JOIN
						EPOST_ARTICLE_SHELF AS eas ON eas.Status = ".$cfg_ePost['BigNewspaper']['ArticleShelf_status']['exist']." AND ew.RecordID = eas.WritingID
					WHERE
						encr.RequestID = $RequestID
					GROUP BY
						encr.RequestID
					ORDER BY
						encr.Deadline ASC";
			$result_ary = current($this->returnArray($sql));
			
			$result_ary['TargetLevel'] = $result_ary['TargetLevel']? explode($cfg_ePost['sql_config']['NonCoursewareRequest_TargetLevel_delimiter'], $result_ary['TargetLevel']) : array();
			
			# Get Request Student Editor
			$sql = "SELECT
						esem.ManageItemID,
						iu.UserID,
						".getNameFieldByLang("iu.")." as UserName,
						iu.EnglishName,
						iu.ClassName,
						iu.ClassNumber
					FROM
						EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
						EPOST_STUDENT_EDITOR AS ese ON esem.EditorID = ese.EditorID INNER JOIN
						INTRANET_USER AS iu ON ese.UserID = iu.UserID
					WHERE
						ese.Status = ".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']." AND
						esem.ManageItemID = ".$result_ary['RequestID']." AND
						esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Request']."' AND
						esem.Status = ".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."
					ORDER BY
						iu.ClassName, iu.ClassNumber, iu.EnglishName";
			$StudentEditor_ary = $this->returnArray($sql);
			
			$result_ary['StudentEditors'] 		= array();
			$result_ary['StudentEditorUserIDs'] = array();
				
			for($i=0;$i<count($StudentEditor_ary);$i++){
				$result_ary['StudentEditorUserIDs'][] = $StudentEditor_ary[$i]['UserID'];
				$result_ary['StudentEditors'][] 	  = array("UserID"	  	=> $StudentEditor_ary[$i]['UserID'],
															  "UserName" 	=> $StudentEditor_ary[$i]['UserName'],
															  "EnglishName" => $StudentEditor_ary[$i]['EnglishName'],
															  "ClassName"	=> $StudentEditor_ary[$i]['ClassName'],
															  "ClassNumber" => $StudentEditor_ary[$i]['ClassNumber']);
			}
			
			return $result_ary;
		}
	}
}
?>