<?php
# using : thomas

/* Modification Log:
 * 
 */

if (!defined("LIBEPOSTWRITER_DEFINED"))         // Preprocessor directives
{
	define("LIBEPOSTWRITER_DEFINED",true);
	include_once($intranet_root."/includes/ePost/config/config.php");
	
	class libepost_writers extends libepost
	{
		var $Writers = array();
		
		function libepost_writers(){
			# Initialize ePost Object
			$this->libepost();
			
			$writers_raw_ary = $this->get_writers_raw_ary();
			
			for($i=0;$i<count($writers_raw_ary);$i++){
				$this->Writers[$i] = new libepost_writer(0, $writers_raw_ary[$i]);
			}
		}
		
		function get_writers_raw_ary(){
			global $cfg_ePost;
			$sql = "SELECT
						ew.RecordID, ew.RequestID, ew.UserID, ew.Title, ew.Content,
						ew.Attachment, ew.Caption, ew.LevelCode, ew.ModuleType, ew.ModuleCode,
						ew.ThemeCode, ew.TopicCode, ew.Status, ew.IsRedo, 
						ew.TeacherComment, ew.InShowBoard, ew.InNewspaper, ew.inputdate, ew.modified, ew.ModifiedBy,
						ew.markdate, ".getNameFieldByLang('iu.')." AS UserName, iu.EnglishName, iu.ClassName, iu.ClassNumber
					FROM
						EPOST_WRITING AS ew INNER JOIN
						INTRANET_USER AS iu ON ew.UserID = iu.UserID 
					GROUP BY
						ew.RecordID";
			$result_ary = $this->returnArray($sql);
			
			
				
			return $result_ary;
		}
	}
	
	class libepost_writer extends libepost_writers
	{
		var $RecordID;
		var $RequestID;
		var $UserID;
		var $Title;
		var $Content;
		var $Attachment;
		var $Caption;
		var $LevelCode;
		var $ModuleType;
		var $ModuleCode;
		var $ThemeCode;
		var $TopicCode;
		var $Status;
		var $IsRedo;
		var $ReadFlag;
		var $TeacherComment;
		var $InShowBoard;
		var $InNewspaper;
		var $inputdate;
		var $modified;
		var $markdate;
		var $UserName;
		var $EnglishName;
		var $ClassName;
		var $ClassNumber;
		var $ModifiedBy;

		
		function libepost_writer($RecordID=0, $Writer_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $RecordID is inputed or not
			$Writer_Data_ary = $RecordID? $this->get_writer_raw_ary($RecordID) : $Writer_Data_ary;

			# Initialize Newspaper Page Article Object
			$this->initialize_writer_obj($Writer_Data_ary);
		}
		
		function initialize_writer_obj($Writer_Data_ary){
			global $cfg_ePost;
			
			$this->RecordID 		= $Writer_Data_ary['RecordID'];
			$this->RequestID		= $Writer_Data_ary['RequestID'];
			$this->UserID			= $Writer_Data_ary['UserID'];
			$this->Title 			= $Writer_Data_ary['Title'];
			$this->Content 			= $Writer_Data_ary['Content'];
			$this->Attachment 		= $Writer_Data_ary['Attachment'];
			$this->Caption			= $Writer_Data_ary['Caption'];
			$this->LevelCode 		= $Writer_Data_ary['LevelCode'];
			$this->ModuleType 		= $Writer_Data_ary['ModuleType'];
			$this->ModuleCode 		= $Writer_Data_ary['ModuleCode'];
			$this->ThemeCode 		= $Writer_Data_ary['ThemeCode'];
			$this->TopicCode 		= $Writer_Data_ary['TopicCode']? explode($cfg_ePost['sql_config']['topic_code_delimiter'], $Writer_Data_ary['TopicCode']) : array();
			$this->Status			= $Writer_Data_ary['Status'];
			$this->IsRedo			= $Writer_Data_ary['IsRedo'];
			$this->ReadFlag 		= $Writer_Data_ary['ReadFlag']? explode($cfg_ePost['sql_config']['ReadFlag_delimiter'], $Writer_Data_ary['ReadFlag']) : array();
			$this->TeacherComment 	= $Writer_Data_ary['TeacherComment'];
			$this->InShowBoard 		= $Writer_Data_ary['InShowBoard'];
			$this->InNewspaper 		= $Writer_Data_ary['InNewspaper'];
			$this->inputdate 		= $Writer_Data_ary['inputdate'];
			$this->modified 		= $Writer_Data_ary['modified'];
			$this->markdate			= $Writer_Data_ary['markdate'];
			$this->UserName			= $Writer_Data_ary['UserName'];
			$this->EnglishName		= $Writer_Data_ary['EnglishName'];
			$this->ClassName		= $Writer_Data_ary['ClassName'];
			$this->ClassNumber		= $Writer_Data_ary['ClassNumber'];
			$this->ModifiedBy		= $Writer_Data_ary['ModifiedBy'];
		}
		
		function get_writer_raw_ary($RecordID){
			global $cfg_ePost;
			
			$sql = "SELECT
						ew.RecordID, ew.RequestID, ew.UserID, ew.Title, ew.Content,
						ew.Attachment, ew.Caption, ew.LevelCode, ew.ModuleType, ew.ModuleCode,
						ew.ThemeCode, ew.TopicCode, ew.Status, ew.IsRedo,
						ew.TeacherComment, ew.InShowBoard, ew.InNewspaper, ew.inputdate, ew.modified, ew.ModifiedBy,
						ew.markdate, ".getNameFieldByLang('iu.')." AS UserName, iu.EnglishName, iu.ClassName, iu.ClassNumber
					FROM
						EPOST_WRITING AS ew INNER JOIN
						INTRANET_USER AS iu ON ew.UserID = iu.UserID
					WHERE
						ew.RecordID = $RecordID
					GROUP BY
						ew.RecordID";
			$result_ary = current($this->returnArray($sql));
			
			$sql = "
				SELECT 
					AttachmentID,Attachment,Caption
				FROM
					EPOST_WRITING_ATTACHMENT 
				WHERE WritingID = '".$RecordID."'
			"; 
			$attachmentResultAry = $this->returnArray($sql);
			$attachmentCnt = count($attachmentResultAry);
			$attachmentAry = array();
			for($i=0;$i<$attachmentCnt;$i++){
				list($_attachmentId,$_attachment,$_caption) = $attachmentResultAry[$i];
				$attachmentAry[$_attachmentId] = array('attachment'=>$_attachment,'caption'=>$_caption);
			}
			$result_ary['Attachment'] = $attachmentAry;
			return $result_ary;
		}
		
		
		function format_attachment_path($attachment,$Is_http_path=false){
			global $PATH_WRT_ROOT;
			if(!empty($attachment)){
	    		$path = ($Is_http_path? "/" : $PATH_WRT_ROOT)."file/ePost/writing_attachment/".$this->RecordID."/".($Is_http_path? rawurlencode(addslashes($attachment)) : $attachment);
			}
	    	return $path;
		}
	}
	
	
}