<?php
# using : 

/* Modification Log:
 * 
 * 	2016-01-06 [Cameron]
 * 		- fix bug: cast $Folder_Data_ary as array for array_merge in get_newspaper_raw_ary() 
 * 
 */

if (!defined("LIBEPOSTNEWSPAPER_DEFINED"))         // Preprocessor directives
{
	define("LIBEPOSTNEWSPAPER_DEFINED",true);
	include_once($intranet_root."/includes/ePost/config/config.php");
	
	class libepost_folders extends libepost
	{
		var $Folders;
		function libepost_folders($folderId=""){
			# Initialize ePost Object
			$this->libepost();
			
			$folders_raw_ary = $this->get_folders_raw_ary($folderId);
			
			for($i=0;$i<count($folders_raw_ary);$i++){
				$this->Folders[$i] = new libepost_folder(0, $folders_raw_ary[$i]);
			}
		}
		
		function get_folders_raw_ary($folderId=""){
			global $cfg_ePost;
			
			$NewspaperIDSeparator = '|=|';
			
			$sql = "SELECT
						ef.FolderID,
						ef.Title AS Folder_Title,
						ef.Status AS Folder_Status,
						ef.IsSystemPublic AS Folder_IsSystemPublic,
						ef.InputDate AS Folder_InputDate,
						ef.ModifiedBy AS Folder_ModifiedBy,
						ef.ModifiedDate AS Folder_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Folder_ModifiedByUser,
						iu.EnglishName AS Folder_EnglishName,
						iu.ClassName AS Folder_ClassName,
						iu.ClassNumber AS Folder_ClassNumber,
						GROUP_CONCAT(DISTINCT en.NewspaperID ORDER BY en.NewspaperID SEPARATOR '$NewspaperIDSeparator') AS Folder_NewspaperIDs,
						SUM(en.NoOfView) AS Folder_TotalNoOfView
					FROM
						EPOST_FOLDER AS ef LEFT JOIN
						EPOST_NEWSPAPER AS en ON ef.FolderID = en.FolderID AND en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."' LEFT JOIN
						INTRANET_USER AS iu ON ef.ModifiedBy = iu.UserID
					WHERE
						ef.Status = '".$cfg_ePost['BigNewspaper']['Folder_status']['exist']."'
					".(!empty($folderId)?" AND ef.FolderID=\"".$folderId."\"":"")."
					GROUP BY
						ef.FolderID
					ORDER BY
						ef.IsSystemPublic DESC, ef.Title ASC";
			$result_ary = $this->returnArray($sql);
			
			for($i=0;$i<count($result_ary);$i++){
				$result_ary[$i]['Folder_NewspaperIDs'] = $result_ary[$i]['Folder_NewspaperIDs']? explode($NewspaperIDSeparator, $result_ary[$i]['Folder_NewspaperIDs']) : array();
			}
			
			return $result_ary;
		}
		
		function get_default_folder(){
			global $cfg_ePost;
			
			if(count($this->Folders) > 0){
				for($i=0;$i<count($this->Folders);$i++){
					if($this->Folders[$i]->Folder_IsSystemPublic == $cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']){
						$default_folder = $this->Folders[$i];
					}
				}
			}
			else{
				$NewspaperIDSeparator = '|=|';
				
				$sql = "SELECT
							ef.FolderID,
							ef.Title AS Folder_Title,
							ef.Status AS Folder_Status,
							ef.IsSystemPublic AS Folder_IsSystemPublic,
							ef.InputDate AS Folder_InputDate,
							ef.ModifiedBy AS Folder_ModifiedBy,
							ef.ModifiedDate AS Folder_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Folder_ModifiedByUser,
							iu.EnglishName AS Folder_EnglishName,
							iu.ClassName AS Folder_ClassName,
							iu.ClassNumber AS Folder_ClassNumber,
							GROUP_CONCAT(DISTINCT en.NewspaperID ORDER BY en.NewspaperID SEPARATOR '$NewspaperIDSeparator') AS Folder_NewspaperIDs,
							SUM(en.NoOfView) AS Folder_TotalNoOfView
						FROM
							EPOST_FOLDER AS ef LEFT JOIN
							EPOST_NEWSPAPER AS en ON ef.FolderID = en.FolderID AND en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."' LEFT JOIN
							INTRANET_USER AS iu ON ef.ModifiedBy = iu.UserID
						WHERE
							ef.IsSystemPublic = '".$cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']."'
						GROUP BY
							ef.FolderID";$this->returnArray($sql) or die(mysql_error());
				$result_ary = current($this->returnArray($sql));
				
				if(is_array($result_ary) && count($result_ary)>0)
					$result_ary['Folder_TotalNoOfView'] = $result_ary['Folder_TotalNoOfView']? explode($NewspaperIDSeparator, $result_ary['Folder_TotalNoOfView']) : array(); 
				
				$default_folder = new libepost_folder(0, $result_ary);
			}
			
			return $default_folder;
		}

	}
	
	class libepost_folder extends libepost_folders
	{
		var $FolderID;
		var $Folder_Title;
		var $Folder_Status;
		var $Folder_IsSystemPublic;
		var $Folder_InputDate;
		var $Folder_ModifiedBy;
		var $Folder_ModifiedDate;
		var $Folder_ModifiedByUser;
		var $Folder_EnglishName;
		var $Folder_ClassName;
		var $Folder_ClassNumber;
		var $Folder_NewspaperIDs;
		var $Folder_Newspapers;
		var $Folder_TotalNoOfView;
		var $sortBy;
		var $orderBy;
		var $keyword;

		
		function libepost_folder($FolderID=0, $Folder_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $FolderID is inputed or not
			$Folder_Data_ary = $FolderID? $this->get_folder_raw_ary($FolderID) : $Folder_Data_ary;
			
			# Initialize Folder Object
			$this->initialize_folder_obj($Folder_Data_ary);
		}
		
		function initialize_folder_obj($Folder_Data_ary){
			$this->FolderID		 		 = $Folder_Data_ary['FolderID'];
			$this->Folder_Title		 	 = $Folder_Data_ary['Folder_Title'];
			$this->Folder_Status		 = $Folder_Data_ary['Folder_Status'];
			$this->Folder_IsSystemPublic = $Folder_Data_ary['Folder_IsSystemPublic'];
			$this->Folder_InputDate	 	 = $Folder_Data_ary['Folder_InputDate'];
			$this->Folder_ModifiedBy	 = $Folder_Data_ary['Folder_ModifiedBy'];
			$this->Folder_ModifiedDate   = $Folder_Data_ary['Folder_ModifiedDate'];
			$this->Folder_ModifiedByUser = $Folder_Data_ary['Folder_ModifiedByUser'];
			$this->Folder_EnglishName	 = $Folder_Data_ary['Folder_EnglishName'];
			$this->Folder_ClassName	 	 = $Folder_Data_ary['Folder_ClassName'];
			$this->Folder_ClassNumber	 = $Folder_Data_ary['Folder_ClassNumber'];
			$this->Folder_NewspaperIDs   = $Folder_Data_ary['Folder_NewspaperIDs'];
			$this->Folder_TotalNoOfView  = $Folder_Data_ary['Folder_TotalNoOfView'];
		}
		
		function folder_obj_to_array(){
			$Folder_Data_ary['FolderID'] 			  = $this->FolderID;
			$Folder_Data_ary['Folder_Title'] 		  = $this->Folder_Title;
			$Folder_Data_ary['Folder_Status'] 		  = $this->Folder_Status;
			$Folder_Data_ary['Folder_IsSystemPublic'] = $this->Folder_IsSystemPublic;
			$Folder_Data_ary['Folder_InputDate'] 	  = $this->Folder_InputDate;
			$Folder_Data_ary['Folder_ModifiedBy'] 	  = $this->Folder_ModifiedBy;
			$Folder_Data_ary['Folder_ModifiedDate']   = $this->Folder_ModifiedDate;
			$Folder_Data_ary['Folder_ModifiedByUser'] = $this->Folder_ModifiedByUser;
			$Folder_Data_ary['Folder_EnglishName'] 	  = $this->Folder_EnglishName;
			$Folder_Data_ary['Folder_ClassName'] 	  = $this->Folder_ClassName;
			$Folder_Data_ary['Folder_ClassNumber'] 	  = $this->Folder_ClassNumber;
			$Folder_Data_ary['Folder_NewspaperIDs']   = $this->Folder_NewspaperIDs;
			$Folder_Data_ary['Folder_TotalNoOfView']  = $this->Folder_TotalNoOfView;
			
			return $Folder_Data_ary;
		}
		
		function get_folder_raw_ary($FolderID){
			global $cfg_ePost;
			
			$NewspaperIDSeparator = '|=|';
			
			$sql = "SELECT
						ef.FolderID,
						ef.Title AS Folder_Title,
						ef.Status AS Folder_Status,
						ef.IsSystemPublic AS Folder_IsSystemPublic,
						ef.InputDate AS Folder_InputDate,
						ef.ModifiedBy AS Folder_ModifiedBy,
						ef.ModifiedDate AS Folder_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Folder_ModifiedByUser,
						iu.EnglishName AS Folder_EnglishName,
						iu.ClassName AS Folder_ClassName,
						iu.ClassNumber AS Folder_ClassNumber,
						GROUP_CONCAT(DISTINCT en.NewspaperID ORDER BY en.NewspaperID SEPARATOR '$NewspaperIDSeparator') AS Folder_NewspaperIDs,
						SUM(en.NoOfView) AS Folder_TotalNoOfView
					FROM
						EPOST_FOLDER AS ef LEFT JOIN
						EPOST_NEWSPAPER AS en ON ef.FolderID = en.FolderID AND en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."' LEFT JOIN
						INTRANET_USER AS iu ON ef.ModifiedBy = iu.UserID
					WHERE
						ef.FolderID = '$FolderID'
					GROUP BY
						ef.FolderID";
			$result_ary = current($this->returnArray($sql));
			
			if(is_array($result_ary) && count($result_ary)>0)
				$result_ary['Folder_NewspaperIDs'] = $result_ary['Folder_NewspaperIDs']? explode($NewspaperIDSeparator, $result_ary['Folder_NewspaperIDs']) : array(); 
			
			return $result_ary;
		}
		
		function get_newspapers_raw_ary(){
			global $cfg_ePost;
			$result_ary = array();
			if(count($this->Folder_NewspaperIDs) > 0){
				$PageIDSeparator = '|=|';
				switch($this->sortBy){
					case 'title':$cond=' ORDER BY Newspaper_Title';break;
					case 'name':$cond=' ORDER BY Newspaper_Name';break;
					case 'date':$cond=' ORDER BY Newspaper_IssueDate';break;
					default:$cond='';
				}
				$cond .= ($this->orderBy==1)?' DESC':'';
				if(!empty($this->keyword)){
					$keyword_search = "
						AND(
								en.Title LIKE '%".$this->keyword."%'
							OR en.Name LIKE '%".$this->keyword."%'
							OR en.IssueDate LIKE '%".$this->keyword."%'
						)
					";
				}
				$sql = "SELECT
							en.NewspaperID,
							en.FolderID,
							en.Title AS Newspaper_Title,
							en.Name AS Newspaper_Name,
							en.IssueDate AS Newspaper_IssueDate,
							en.OpenToPublic AS Newspaper_OpenToPublic,
							en.Skin AS Newspaper_Skin,
							en.SchoolLogo AS Newspaper_SchoolLogo,
							en.CoverBanner AS Newspaper_CoverBanner,
							en.CoverBannerHideTitle AS Newspaper_CoverBannerHideTitle,
							en.CoverBannerHideName AS Newspaper_CoverBannerHideName,
							en.InsideBanner AS Newspaper_InsideBanner,
							en.InsideBannerHideTitle AS Newspaper_InsideBannerHideTitle,
							en.InsideBannerHideName AS Newspaper_InsideBannerHideName,
							en.InsideBannerHidePageCat AS Newspaper_InsideBannerHidePageCat,
							en.NoOfView AS Newspaper_NoOfView,
							en.PublishStatus AS Newspaper_PublishStatus,
							en.Status AS Newspaper_Status,
							en.InputDate AS Newspaper_InputDate,
							en.ModifiedBy AS Newspaper_ModifiedBy,
							en.ModifiedDate AS Newspaper_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Newspaper_ModifiedByUser,
							iu.EnglishName AS Newspaper_EnglishName,
							iu.ClassName AS Newspaper_ClassName,
							iu.ClassNumber AS Newspaper_ClassNumber,
							GROUP_CONCAT(DISTINCT ep.PageID ORDER BY ep.PageOrder SEPARATOR '$PageIDSeparator') AS Newspaper_PageIDs
						FROM
							EPOST_NEWSPAPER AS en INNER JOIN
							INTRANET_USER AS iu ON en.ModifiedBy = iu.UserID LEFT JOIN
							EPOST_NEWSPAPER_PAGE AS ep ON en.NewspaperID = ep.NewspaperID AND ep.Status = '".$cfg_ePost['BigNewspaper']['Page_status']['exist']."'
						WHERE
							en.NewspaperID IN ('".implode("','", $this->Folder_NewspaperIDs)."') AND
							en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."'
							".$keyword_search."
						GROUP BY
							en.NewspaperID
							".$cond."
						";
				$result_ary = $this->returnArray($sql); 
				
				# Get Newspaper Student Editor
				$sql = "SELECT
							esem.ManageItemID,
							iu.UserID,
							iu.EnglishName,
							iu.ClassName,
							iu.ClassNumber
						FROM
							EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
							EPOST_STUDENT_EDITOR AS ese ON esem.EditorID = ese.EditorID INNER JOIN
							INTRANET_USER AS iu ON ese.UserID = iu.UserID
						WHERE
							ese.Status = '".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']."' AND
							esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."' AND
							esem.Status = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."' AND
							esem.ManageItemID IN ('".implode("','", $this->Folder_NewspaperIDs)."')
						ORDER BY
							iu.ClassName, iu.ClassNumber, iu.EnglishName";
				$StudentEditor_ary = $this->returnArray($sql);
				
				for($i=0;$i<count($result_ary);$i++){
					$result_ary[$i]['Newspaper_PageIDs'] 	= $result_ary[$i]['Newspaper_PageIDs']? explode($PageIDSeparator, $result_ary[$i]['Newspaper_PageIDs']) : array();
					$result_ary[$i]['Newspaper_StudentEditors'] 		= array();
					$result_ary[$i]['Newspaper_StudentEditorUserIDs'] = array();
					
					for($j=0;$j<count($StudentEditor_ary);$j++){
						if($result_ary[$i]['NewspaperID']==$StudentEditor_ary[$j]['ManageItemID']){
							$result_ary[$i]['Newspaper_StudentEditorUserIDs'][] = $StudentEditor_ary[$j]['UserID'];
							$result_ary[$i]['Newspaper_StudentEditors'][] 	    = array("UserID"	  => $StudentEditor_ary[$j]['UserID'],
																			  			"EnglishName" => $StudentEditor_ary[$j]['EnglishName'],
																			  			"ClassName"	  => $StudentEditor_ary[$j]['ClassName'],
																			  			"ClassNumber" => $StudentEditor_ary[$j]['ClassNumber']);
						}
					}
				}
			}
			
			return $result_ary;
		}
		
		function get_newspapers(){
			if(count($this->Folder_Newspapers)==0){
				$Folder_Data_ary    = $this->folder_obj_to_array();
				$newspapers_raw_ary = $this->get_newspapers_raw_ary();
				
				for($i=0;$i<count($newspapers_raw_ary);$i++){
					$Newspaper_Data_ary = array_merge($Folder_Data_ary, $newspapers_raw_ary[$i]);
					$this->Folder_Newspapers[$i] = new libepost_newspaper(0, $Newspaper_Data_ary);
				}
			}
			
			return $this->Folder_Newspapers;
		}
		
		function get_managing_newspapers(){
			global $UserID;
			
			$newspapers_ary = array();
			
			if($this->is_editor){
				$all_newspapers_ary = $this->get_newspapers();
				if($this->user_obj->isTeacherStaff()){
					$newspapers_ary = $all_newspapers_ary;
				}
				else{
					for($i=0;$i<count($all_newspapers_ary);$i++){
						$NewspaperObj = $all_newspapers_ary[$i];
						if(in_array($UserID, $NewspaperObj->Newspaper_StudentEditorUserIDs)){
							$newspapers_ary[] = $NewspaperObj;
						}
					}
				}
			}
			
			return $newspapers_ary;
		}
		
		function update_folder_db(){
			
		}
		
		function delete_folder(){
			global $cfg_ePost, $UserID;
			
			if($this->Folder_IsSystemPublic != $cfg_ePost['BigNewspaper']['Folder_IsSystemPublic']['true']){
				$sql = "UPDATE
							EPOST_FOLDER AS ef
						SET
							ef.Status = '".$cfg_ePost['BigNewspaper']['Folder_status']['deleted']."',
							ef.ModifiedBy = '$UserID',
							ef.ModifiedDate = NOW()
						WHERE
							ef.FolderID = '".$this->FolderID."'";
				$result = $this->db_db_query($sql);
				
				if(count($this->Folder_NewspaperIDs)>0){
					$default_folder = $this->get_default_folder();
					
					$sql = "UPDATE
								EPOST_NEWSPAPER AS en
							SET
								en.FolderID = '".$default_folder->FolderID."'
							WHERE
								en.NewspaperID IN ('".implode("','", $this->Folder_NewspaperIDs)."')";
					$result = $this->db_db_query($sql);
				}
			}
			else{
				$result = false;
			}
			
			return $result;
		}
	}
	
	class libepost_newspaper extends libepost_folder
	{
		var $NewspaperID;
		var $Newspaper_Title;
		var $Newspaper_Name;
		var $Newspaper_IssueDate;
		var $Newspaper_OpenToPublic;
		var $Newspaper_Skin;
		var $Newspaper_SchoolLogo;
		var $Newspaper_CoverBanner;
		var $Newspaper_CoverBannerHideTitle;
		var $Newspaper_CoverBannerHideName;
		var $Newspaper_InsideBanner;
		var $Newspaper_InsideBannerHideTitle;
		var $Newspaper_InsideBannerHideName;
		var $Newspaper_InsideBannerHidePageCat;
		var $Newspaper_NoOfView;
		var $Newspaper_PublishStatus;
		var $Newspaper_Status;
		var $Newspaper_InputDate;
		var $Newspaper_ModifiedBy;
		var $Newspaper_ModifiedDate;
		var $Newspaper_ModifiedByUser;
		var $Newspaper_EnglishName;
		var $Newspaper_ClassName;
		var $Newspaper_ClassNumber;
		var $Newspaper_PageIDs;
		var $Newspaper_Pages;
		var $Newspaper_StudentEditorUserIDs;
		var $Newspaper_StudentEditors;
		
		function libepost_newspaper($NewspaperID=0, $Newspaper_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $NewspaperID is inputed or not
			$Newspaper_Data_ary = $NewspaperID? $this->get_newspaper_raw_ary($NewspaperID) : $Newspaper_Data_ary;
			
			# Initialize Newspaper Object
			$this->initialize_newspaper_obj($Newspaper_Data_ary);
		}
		
		function initialize_newspaper_obj($Newspaper_Data_ary){
			# Initialize Folder Object
			$this->initialize_folder_obj($Newspaper_Data_ary);
			
			$this->NewspaperID 			 		  	 = $Newspaper_Data_ary['NewspaperID'];
			$this->Newspaper_Title 		 		  	 = stripslashes($Newspaper_Data_ary['Newspaper_Title']);
			$this->Newspaper_Name 		 		  	 = stripslashes($Newspaper_Data_ary['Newspaper_Name']);
			$this->Newspaper_IssueDate 	  		  	 = $Newspaper_Data_ary['Newspaper_IssueDate'];
			$this->Newspaper_OpenToPublic 	  	  	 = $Newspaper_Data_ary['Newspaper_OpenToPublic'];
			$this->Newspaper_Skin 		  		  	 = $Newspaper_Data_ary['Newspaper_Skin'];
			$this->Newspaper_SchoolLogo			  	 = $Newspaper_Data_ary['Newspaper_SchoolLogo'];
			$this->Newspaper_CoverBanner		  	 = $Newspaper_Data_ary['Newspaper_CoverBanner'];
			$this->Newspaper_CoverBannerHideTitle 	 = $Newspaper_Data_ary['Newspaper_CoverBannerHideTitle'];
			$this->Newspaper_CoverBannerHideName  	 = $Newspaper_Data_ary['Newspaper_CoverBannerHideName'];
			$this->Newspaper_InsideBanner		  	 = $Newspaper_Data_ary['Newspaper_InsideBanner'];
			$this->Newspaper_InsideBannerHideTitle 	 = $Newspaper_Data_ary['Newspaper_InsideBannerHideTitle'];
			$this->Newspaper_InsideBannerHideName  	 = $Newspaper_Data_ary['Newspaper_InsideBannerHideName'];
			$this->Newspaper_InsideBannerHidePageCat = $Newspaper_Data_ary['Newspaper_InsideBannerHidePageCat'];
			$this->Newspaper_NoOfView 	  		  	 = $Newspaper_Data_ary['Newspaper_NoOfView'];
			$this->Newspaper_PublishStatus		  	 = $Newspaper_Data_ary['Newspaper_PublishStatus'];
			$this->Newspaper_Status 	  		  	 = $Newspaper_Data_ary['Newspaper_Status'];
			$this->Newspaper_InputDate 	  		  	 = $Newspaper_Data_ary['Newspaper_InputDate'];
			$this->Newspaper_ModifiedBy   		  	 = $Newspaper_Data_ary['Newspaper_ModifiedBy'];
			$this->Newspaper_ModifiedDate 		  	 = $Newspaper_Data_ary['Newspaper_ModifiedDate'];
			$this->Newspaper_ModifiedByUser		  	 = $Newspaper_Data_ary['Newspaper_ModifiedByUser'];
			$this->Newspaper_EnglishName  		  	 = $Newspaper_Data_ary['Newspaper_EnglishName'];
			$this->Newspaper_ClassName 	  		  	 = $Newspaper_Data_ary['Newspaper_ClassName'];
			$this->Newspaper_ClassNumber  		  	 = $Newspaper_Data_ary['Newspaper_ClassNumber'];
			$this->Newspaper_PageIDs	  		  	 = $Newspaper_Data_ary['Newspaper_PageIDs'];
			$this->Newspaper_StudentEditorUserIDs 	 = $Newspaper_Data_ary['Newspaper_StudentEditorUserIDs'];
			$this->Newspaper_StudentEditors 	  	 = $Newspaper_Data_ary['Newspaper_StudentEditors'];
		}
		
		function newspaper_obj_to_array(){
			# Get Folder Onject Array
			$Folder_Data_ary = $this->folder_obj_to_array();
			
			$Newspaper_Data_ary['NewspaperID'] 			 		  	 = $this->NewspaperID;
			$Newspaper_Data_ary['Newspaper_Title'] 				  	 = stripslashes($this->Newspaper_Title);
			$Newspaper_Data_ary['Newspaper_Name'] 				  	 =stripslashes( $this->Newspaper_Name);
			$Newspaper_Data_ary['Newspaper_IssueDate'] 			  	 = $this->Newspaper_IssueDate;
			$Newspaper_Data_ary['Newspaper_OpenToPublic'] 		  	 = $this->Newspaper_OpenToPublic;
			$Newspaper_Data_ary['Newspaper_Skin'] 				  	 = $this->Newspaper_Skin;
			$Newspaper_Data_ary['Newspaper_SchoolLogo']			  	 = $this->Newspaper_SchoolLogo;
			$Newspaper_Data_ary['Newspaper_CoverBanner'] 			 = $this->Newspaper_CoverBanner;
			$Newspaper_Data_ary['Newspaper_CoverBannerHideTitle'] 	 = $this->Newspaper_CoverBannerHideTitle;
			$Newspaper_Data_ary['Newspaper_CoverBannerHideName'] 	 = $this->Newspaper_CoverBannerHideName;
			$Newspaper_Data_ary['Newspaper_InsideBanner'] 			 = $this->Newspaper_InsideBanner;
			$Newspaper_Data_ary['Newspaper_InsideBannerHideTitle'] 	 = $this->Newspaper_InsideBannerHideTitle;
			$Newspaper_Data_ary['Newspaper_InsideBannerHideName'] 	 = $this->Newspaper_InsideBannerHideName;
			$Newspaper_Data_ary['Newspaper_InsideBannerHidePageCat'] = $this->Newspaper_InsideBannerHidePageCat;
			$Newspaper_Data_ary['Newspaper_NoOfView'] 	  		  	 = $this->Newspaper_NoOfView;
			$Newspaper_Data_ary['Newspaper_PublishStatus'] 		  	 = $this->Newspaper_PublishStatus;
			$Newspaper_Data_ary['Newspaper_Status'] 	  		  	 = $this->Newspaper_Status;
			$Newspaper_Data_ary['Newspaper_InputDate'] 	  		  	 = $this->Newspaper_InputDate;
			$Newspaper_Data_ary['Newspaper_ModifiedBy']   		   	 = $this->Newspaper_ModifiedBy;
			$Newspaper_Data_ary['Newspaper_ModifiedDate'] 		  	 = $this->Newspaper_ModifiedDate;
			$Newspaper_Data_ary['Newspaper_ModifiedByUser']   	  	 = $this->Newspaper_ModifiedByUser;
			$Newspaper_Data_ary['Newspaper_EnglishName']  		  	 = $this->Newspaper_EnglishName;
			$Newspaper_Data_ary['Newspaper_ClassName']	  		  	 = $this->Newspaper_ClassName;
			$Newspaper_Data_ary['Newspaper_ClassNumber'] 		  	 = $this->Newspaper_ClassNumber;
			$Newspaper_Data_ary['Newspaper_PageIDs']	  		  	 = $this->Newspaper_PageIDs;
			$Newspaper_Data_ary['Newspaper_StudentEditorUserIDs'] 	 = $this->Newspaper_StudentEditorUserIDs;
			$Newspaper_Data_ary['Newspaper_StudentEditors'] 	  	 = $this->Newspaper_StudentEditors;
			
			$Newspaper_Data_ary = array_merge($Folder_Data_ary, $Newspaper_Data_ary);
			
			return $Newspaper_Data_ary;
		}
		
		function get_newspaper_raw_ary($NewspaperID){
			global $cfg_ePost;
			
			# Newspaper Raw Array
			$PageIDSeparator = '|=|';
			
			$sql = "SELECT
						en.NewspaperID,
						en.FolderID,
						en.Title AS Newspaper_Title,
						en.Name AS Newspaper_Name,
						en.IssueDate AS Newspaper_IssueDate,
						en.OpenToPublic AS Newspaper_OpenToPublic,
						en.Skin AS Newspaper_Skin,
						en.SchoolLogo AS Newspaper_SchoolLogo,
						en.CoverBanner AS Newspaper_CoverBanner,
						en.CoverBannerHideTitle AS Newspaper_CoverBannerHideTitle,
						en.CoverBannerHideName AS Newspaper_CoverBannerHideName,
						en.InsideBanner AS Newspaper_InsideBanner,
						en.InsideBannerHideTitle AS Newspaper_InsideBannerHideTitle,
						en.InsideBannerHideName AS Newspaper_InsideBannerHideName,
						en.InsideBannerHidePageCat AS Newspaper_InsideBannerHidePageCat,
						en.NoOfView AS Newspaper_NoOfView,
						en.PublishStatus AS Newspaper_PublishStatus,
						en.Status AS Newspaper_Status,
						en.InputDate AS Newspaper_InputDate,
						en.ModifiedBy AS Newspaper_ModifiedBy,
						en.ModifiedDate AS Newspaper_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Newspaper_ModifiedByUser,
						iu.EnglishName AS Newspaper_EnglishName,
						iu.ClassName AS Newspaper_ClassName,
						iu.ClassNumber AS Newspaper_ClassNumber,
						GROUP_CONCAT(DISTINCT ep.PageID ORDER BY ep.PageOrder SEPARATOR '$PageIDSeparator') AS Newspaper_PageIDs
					FROM
						EPOST_NEWSPAPER AS en INNER JOIN
						INTRANET_USER AS iu ON en.ModifiedBy = iu.UserID LEFT JOIN
						EPOST_NEWSPAPER_PAGE AS ep ON en.NewspaperID = ep.NewspaperID AND ep.Status = '".$cfg_ePost['BigNewspaper']['Page_status']['exist']."'
					WHERE
						en.NewspaperID = '$NewspaperID'
					GROUP By
						en.NewspaperID";
			$result_ary = current($this->returnArray($sql)); 
			
			# Get Newspaper Student Editor
			$sql = "SELECT
						esem.ManageItemID,
						iu.UserID,
						iu.EnglishName,
						iu.ClassName,
						iu.ClassNumber
					FROM
						EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
						EPOST_STUDENT_EDITOR AS ese ON esem.EditorID = ese.EditorID INNER JOIN
						INTRANET_USER AS iu ON ese.UserID = iu.UserID
					WHERE
						ese.Status = '".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']."' AND
						esem.ManageItemID = '".$result_ary['NewspaperID']."' AND
						esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."' AND
						esem.Status = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."' AND
						esem.ManageItemID = '".$NewspaperID."'
					ORDER BY
						iu.ClassName, iu.ClassNumber, iu.EnglishName";
			$StudentEditor_ary = $this->returnArray($sql);
			
			$result_ary['Newspaper_PageIDs'] 			  = $result_ary['Newspaper_PageIDs']? explode($PageIDSeparator, $result_ary['Newspaper_PageIDs']) : array();
			$result_ary['Newspaper_StudentEditors'] 	  = array();
			$result_ary['Newspaper_StudentEditorUserIDs'] = array();
					
			for($i=0;$i<count($StudentEditor_ary);$i++){
				$result_ary['Newspaper_StudentEditorUserIDs'][] = $StudentEditor_ary[$i]['UserID'];
				$result_ary['Newspaper_StudentEditors'][] 	    = array("UserID"	  => $StudentEditor_ary[$i]['UserID'],
																  		"EnglishName" => $StudentEditor_ary[$i]['EnglishName'],
																  		"ClassName"	  => $StudentEditor_ary[$i]['ClassName'],
																  		"ClassNumber" => $StudentEditor_ary[$i]['ClassNumber']);
			}
			
			# Folder Raw Array
			$Folder_Data_ary = $this->get_folder_raw_ary($result_ary['FolderID']);
			
			$result_ary = array_merge((array)$Folder_Data_ary, (array)$result_ary);
			
			return $result_ary;
		}
		
		function get_pages_raw_ary(){
			global $cfg_ePost;
			
			$result_ary = array();
			
			if(count($this->Newspaper_PageIDs) > 0){
				$ArticleIDSeparator = '|=|';
				
				$sql = "SELECT
							ep.PageID,
							ep.NewspaperID,
							ep.Type AS Page_Type,
							ep.Layout AS Page_Layout,
							ep.Theme AS Page_Theme,
							ep.ThemeName AS Page_ThemeName,
							ep.Name AS Page_Name,
							ep.PageOrder AS Page_PageOrder,
							ep.Status AS Page_Status,
							ep.InputDate AS Page_InputDate,
							ep.ModifiedBy AS Page_ModifiedeBy,
							ep.ModifiedDate AS Page_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Page_ModifiedByUser,
							iu.EnglishName AS Page_EnglishName,
							iu.ClassName AS Page_ClassName,
							iu.ClassNumber AS Page_ClassNumber,
							GROUP_CONCAT(DISTINCT ea.ArticleID ORDER BY ea.Position SEPARATOR '$ArticleIDSeparator') AS Page_ArticleIDs
						FROM
							EPOST_NEWSPAPER_PAGE AS ep INNER JOIN
							INTRANET_USER AS iu ON ep.ModifiedBy = iu.UserID LEFT JOIN
							EPOST_NEWSPAPER_PAGE_ARTICLE AS ea ON ep.PageID = ea.PageID AND ea.Status = '".$cfg_ePost['BigNewspaper']['Article_status']['exist']."'
						WHERE
							ep.PageID IN ('".implode("','", $this->Newspaper_PageIDs)."') AND
							ep.Status = '".$cfg_ePost['BigNewspaper']['Page_status']['exist']."'
						GROUP BY
							ep.PageID
						ORDER BY
							ep.PageOrder";
				$result_ary = $this->returnArray($sql); 
				
				# Process Page ArticleID array
				for($i=0;$i<count($result_ary);$i++){
					$result_ary[$i]['Page_ArticleIDs'] = $result_ary[$i]['Page_ArticleIDs']? explode($ArticleIDSeparator, $result_ary[$i]['Page_ArticleIDs']) : array();
				}
			}
			
			return $result_ary;
		}
		
		function get_pages(){
			if(count($this->Newspaper_Pages)==0){
				$Newspaper_Data_ary = $this->newspaper_obj_to_array();
				$pages_raw_ary 		= $this->get_pages_raw_ary();
				
				for($i=0;$i<count($pages_raw_ary);$i++){
					$Page_Data_ary = array_merge($Newspaper_Data_ary, $pages_raw_ary[$i]);
					$this->Newspaper_Pages[$i] = new libepost_page(0, $Page_Data_ary);
				}
			}
			
			return $this->Newspaper_Pages;
		}
		
		function update_NoOfView(){
			$this->Newspaper_NoOfView = $this->Newspaper_NoOfView + 1;
			$this->update_newspaper_db();
		}
		
		function update_PublishStatus($PublishStatus){
			$this->Newspaper_PublishStatus = $PublishStatus;
			return $this->update_newspaper_db();
		}
		
		function update_newspaper_db(){
			global $PATH_WRT_ROOT, $cfg_ePost, $UserID;
			
			if($this->NewspaperID){
				$NewspaperID = $this->NewspaperID;
				$InputDate   = $this->Newspaper_InputDate;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_NEWSPAPER
						SET
							FolderID = '".$this->FolderID."',
							Title = '".addslashes($this->Newspaper_Title)."',
							Name = '".addslashes($this->Newspaper_Name)."',
							IssueDate = '".date('Y-m-d', strtotime($this->Newspaper_IssueDate))." 00:00:00',
							OpenToPublic = '".$this->Newspaper_OpenToPublic."',
							Skin = '".$this->Newspaper_Skin."',
							SchoolLogo = '".$this->Newspaper_SchoolLogo."',
							CoverBanner = '".$this->Newspaper_CoverBanner."',
							CoverBannerHideTitle = '".$this->Newspaper_CoverBannerHideTitle."',
							CoverBannerHideName = '".$this->Newspaper_CoverBannerHideName."',
							InsideBanner = '".$this->Newspaper_InsideBanner."',
							InsideBannerHideTitle = '".$this->Newspaper_InsideBannerHideTitle."',
							InsideBannerHideName = '".$this->Newspaper_InsideBannerHideName."',
							InsideBannerHidePageCat = '".$this->Newspaper_InsideBannerHidePageCat."',
							NoOfView = '".$this->Newspaper_NoOfView."',
							PublishStatus = '".$this->Newspaper_PublishStatus."',
							Status = '".$this->Newspaper_Status."',
							".((!empty($UserID))?"ModifiedBy = '$UserID',":"")."
							ModifiedDate = '$ModifedDate'
						WHERE
							NewspaperID = '".$this->NewspaperID."'";
				
				$result = $this->db_db_query($sql);
			}
			else{
				$InputDate   = date('Y-m-d H:i:s');
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_NEWSPAPER
							(
								FolderID, Title, Name, IssueDate, OpenToPublic,
								Skin, SchoolLogo, CoverBanner, CoverBannerHideTitle, CoverBannerHideName,
								InsideBanner, InsideBannerHideTitle, InsideBannerHideName, InsideBannerHidePageCat, NoOfView,
								PublishStatus, Status, Inputdate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								'".$this->FolderID."', '".addslashes($this->Newspaper_Title)."', '".addslashes($this->Newspaper_Name)."', '".date('Y-m-d', strtotime($this->Newspaper_IssueDate))." 00:00:00', '".$this->Newspaper_OpenToPublic."',
								'".$this->Newspaper_Skin."', '".$this->Newspaper_SchoolLogo."', '".$this->Newspaper_CoverBanner."', '".$this->Newspaper_CoverBannerHideTitle."', '".$this->Newspaper_CoverBannerHideName."',
								'".$this->Newspaper_InsideBanner."', '".$this->Newspaper_InsideBannerHideTitle."', '".$this->Newspaper_InsideBannerHideName."', '".$this->Newspaper_InsideBannerHidePageCat."', 0,
								'".$this->Newspaper_PublishStatus."', '".$this->Newspaper_Status."', '$InputDate', '$UserID', '$ModifedDate'
							)";
				$result 	 = $this->db_db_query($sql);
				$NewspaperID = $this->db_insert_id();
			}
			
			if($result){
				$this->NewspaperID			  = $NewspaperID;
				$this->Newspaper_InputDate	  = $InputDate;
				$this->Newspaper_ModifiedBy   = $UserID;
				$this->Newspaper_ModifiedDate = $ModifedDate;
				
				# Handle SchoolLogo, CoverBanner and InsideBanner
				$this->Newspaper_SchoolLogo   = $this->update_newspaper_file($cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']);
				$this->Newspaper_CoverBanner  = $this->update_newspaper_file($cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']);
				$this->Newspaper_InsideBanner = $this->update_newspaper_file($cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']);
				
				# Update the DB for the real location
				$sql = "UPDATE
							EPOST_NEWSPAPER
						SET
							SchoolLogo = '".$this->Newspaper_SchoolLogo."',
							CoverBanner = '".$this->Newspaper_CoverBanner."',
							InsideBanner = '".$this->Newspaper_InsideBanner."'
						WHERE
							NewspaperID = '$NewspaperID'";
				
				if(!$this->db_db_query($sql)){
					$this->Newspaper_SchoolLogo   = '';
					$this->Newspaper_CoverBanner  = '';
					$this->Newspaper_InsideBanner = '';
				}
				
				$sql = "SELECT EnglishName, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				
				$this->Newspaper_EnglishName  = $ModifiedBy_ary['EnglishName'];
				$this->Newspaper_ClassName 	  = $ModifiedBy_ary['ClassName'];
				$this->Newspaper_ClassNumber  = $ModifiedBy_ary['ClassNumber'];
			}
			
			return $result;
		}
		
		function update_newspaper_file($FileType){
			global $PATH_WRT_ROOT, $cfg_ePost;
			
			switch($FileType){
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']: 
	    			$FileName = $this->Newspaper_SchoolLogo;
	    			break;
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']:
	    			$FileName = $this->Newspaper_CoverBanner;
	    			break;
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']:
	    			$FileName = $this->Newspaper_InsideBanner;
	    			break;
	    	}
	    	
			$result_file_name = '';
			
			# Check if upload attachment is image or not
			if(preg_match('/^\/tmp\/'.$FileType.'\//', $FileName) && !IsImage($PATH_WRT_ROOT.'file/ePost/newspaper_attachment'.$FileName)){
				return '';
			}
			
			if(!$FileName || preg_match('/^\/tmp\/'.$FileType.'\//', $FileName)){
				$dir = $PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID.'/'.$FileType.'/';
				
				# Clear All Previous Uploaded Files
				if(is_dir($dir)){
					$cur_dir = opendir($dir);
				
					while(($file = readdir($cur_dir))!==false){
						if($file!='.' && $file!='..'){
							chmod($dir.$file, 0777);
							if(!is_dir($dir.$file))
								unlink($dir.$file);
						}
					}
				}
					
				if($FileName){
					# Create corresponding folders if not exist
					if(!is_dir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID))
						 mkdir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID, 0777);
					if(!is_dir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID.'/'.$FileType))
						 mkdir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID.'/'.$FileType, 0777);
					
					# Move the school logo from temp location to real location
					$source_file 	  = $PATH_WRT_ROOT.'file/ePost/newspaper_attachment'.$FileName;
					$destination_file = $PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID.'/'.$FileType.'/'.get_file_basename($FileName); 
						
					if(is_file($source_file)){
						rename($source_file, $destination_file);
						
						# Update the DB for the real location
						if(is_file($destination_file)){
							rmdir(dirname($source_file));
							$result_file_name = get_file_basename($FileName);
						}
					}
				}
				else{
					if(is_dir($dir)) rmdir($dir);
				}
			}
			else
				$result_file_name = $FileName;
					
			return $result_file_name;
		}
		
		function return_newspaper_attachment_http_path($FileType){
			global $cfg_ePost;
	    	
	    	switch($FileType){
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']: 
	    			$FileName = $this->Newspaper_SchoolLogo;
	    			break;
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']:
	    			$FileName = $this->Newspaper_CoverBanner;
	    			break;
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']:
	    			$FileName = $this->Newspaper_InsideBanner;
	    			break;
	    	}
	    	
	    	if(preg_match('/^\/tmp\/'.$FileType.'\//', $FileName))
	    		$path = "/file/ePost/newspaper_attachment/".rawurlencode(addslashes($FileName));
	    	else
	    		$path = "/file/ePost/newspaper_attachment/".$this->NewspaperID."/".$FileType."/".rawurlencode(addslashes($FileName));
	    	
	    	return $path;
		}
		
		function delete_newspaper(){
			global $cfg_ePost, $UserID;
			
			# Delete This Newspaper
			$sql = "UPDATE
						EPOST_NEWSPAPER
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						NewspaperID = '".$this->NewspaperID."'";
			$result = $this->db_db_query($sql);
			
			# Delete All Pages in this newspaper
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Page_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						PageID IN ('".implode("','", $this->Newspaper_PageIDs)."')";
			$result = $this->db_db_query($sql);
			
			# Delete All Articles in this newspaper
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE_ARTICLE
					SET
						Status = ".$cfg_ePost['BigNewspaper']['Article_status']['deleted'].",
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						PageID IN ('".implode("','", $this->Newspaper_PageIDs)."')";
			$result = $this->db_db_query($sql);
			
			return $result;
		}
		
		function update_pages_PageOrder(){
			$result = array();
			
			for($i=0;$i<count($this->Newspaper_PageIDs);$i++){
				$sql = "UPDATE EPOST_NEWSPAPER_PAGE SET PageOrder = '$i' WHERE PageID = '".$this->Newspaper_PageIDs[$i]."'";
				$result[] = $this->db_db_query($sql);
			}
			
			return !in_array(false, $result);
		}
		
		function move_target_page_after_ref_page($TargetPageID, $RefPageID){
			$tmp_PageIDs = array();
			$result 	 = array();
			$IsMoved 	 = false;
		
			# Rearranging the Pages
			if($RefPageID==0){
				$tmp_PageIDs[] = $TargetPageID;
				$IsMoved = true;
			}

			for($i=0;$i<count($this->Newspaper_PageIDs);$i++){
				if($this->Newspaper_PageIDs[$i] != $TargetPageID){
					$tmp_PageIDs[] = $this->Newspaper_PageIDs[$i];
				}
				
				if($this->Newspaper_PageIDs[$i] == $RefPageID){
					$tmp_PageIDs[] = $TargetPageID;
					$IsMoved 	   = true;
				}
			}
			
			if(!$IsMoved){
				$tmp_PageIDs[] = $TargetPageID;
			}
		
			# Update the instance of the object
			$this->Newspaper_PageIDs = $tmp_PageIDs;
			$this->Newspaper_Pages 	 = array();
	
			return $this->update_pages_PageOrder();
		}
		
		function get_page_class_suffix(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['page_class_suffix']:"";
			
			return $suffix;
		}
		
		function get_page_big_class_suffix(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['page_big_class_suffix']:"";
			
			return $suffix;
		}
		
		function get_page_body_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['body_class']:"";
			
			return $suffix;
		}
		
		function get_page_top_left_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['top_left_class']:"";
			
			return $suffix;
		}
		
		function get_page_top_right_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['top_right_class']:"";
			
			return $suffix;
		}
		
		function get_page_top_bg_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['top_bg_class']:"";
			
			return $suffix;
		}
		
		function get_page_bg_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['bg_class']:"";
			
			return $suffix;
		}
		
		function is_published(){
			global $cfg_ePost;
			
			$result = (strtotime($this->Newspaper_IssueDate) <= time()) && 
					  (count($this->Newspaper_PageIDs)>0) && 
					  ($this->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published']);
			
			return $result;
		}
		
		function is_open_to_public(){
			$result = $this->is_published() && $this->Newspaper_OpenToPublic;
			
			return $result;
		}
	}
	
	class libepost_page extends libepost_newspaper
	{
		var $PageID;
		var $Page_Type;
		var $Page_Layout;
		var $Page_Theme;
		var $Page_ThemeName;		
		var $Page_Name;
		var $Page_PageOrder;
		var $Page_Status;
		var $Page_InputDate;
		var $Page_ModifiedBy;
		var $Page_ModifiedDate;
		var $Page_ModifiedByUser;
		var $Page_EnglishName;
		var $Page_ClassName;
		var $Page_ClassNumber;
		var $Page_ArticleIDs;
		var $Page_Articles;
		
		function libepost_page($PageID=0, $Page_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $PageID is inputed or not
			$Page_Data_ary = $PageID? $this->get_page_raw_ary($PageID) : $Page_Data_ary;
			
			# Initialize Page Object
			$this->initialize_page_obj($Page_Data_ary);
		}
		
		function initialize_page_obj($Page_Data_ary){
			# Initialize Newspaper Object
			$this->initialize_newspaper_obj($Page_Data_ary);
			
			$this->PageID 			   	= $Page_Data_ary['PageID'];
			$this->Page_Type 		  	= $Page_Data_ary['Page_Type'];
			$this->Page_Layout 		   	= $Page_Data_ary['Page_Layout'];
			$this->Page_Theme 		   	= $Page_Data_ary['Page_Theme'];
			$this->Page_ThemeName		= $Page_Data_ary['Page_ThemeName'];			
			$this->Page_Name 		   	= $Page_Data_ary['Page_Name'];
			$this->Page_PageOrder 	   	= $Page_Data_ary['Page_PageOrder'];
			$this->Page_Status 		   	= $Page_Data_ary['Page_Status'];
			$this->Page_InputDate 	   	= $Page_Data_ary['Page_InputDate'];
			$this->Page_ModifiedBy 	   	= $Page_Data_ary['Page_ModifiedBy'];
			$this->Page_ModifiedDate   	= $Page_Data_ary['Page_ModifiedDate'];
			$this->Page_ModifiedByUser 	= $Page_Data_ary['Page_ModifiedByUser'];
			$this->Page_EnglishName    	= $Page_Data_ary['Page_EnglishName'];
			$this->Page_ClassName 	   	= $Page_Data_ary['Page_ClassName'];
			$this->Page_ClassNumber    	= $Page_Data_ary['Page_ClassNumber'];
			$this->Page_ArticleIDs	   	= $Page_Data_ary['Page_ArticleIDs'];
		}
		
		function page_obj_to_array(){
			# Get Newspaper Onject Array
			$Newspaper_Data_ary = $this->newspaper_obj_to_array();
			
			$Page_Data_ary['PageID'] 				  	= $this->PageID;
			$Page_Data_ary['Page_Type'] 			  	= $this->Page_Type;
			$Page_Data_ary['Page_Layout'] 			  	= $this->Page_Layout;
			$Page_Data_ary['Page_Theme'] 			  	= $this->Page_Theme;
			$Page_Data_ary['Page_ThemeName']			= $this->Page_ThemeName;			
			$Page_Data_ary['Page_Name'] 			  	= $this->Page_Name;
			$Page_Data_ary['Page_PageOrder'] 		  	= $this->Page_PageOrder;
			$Page_Data_ary['Page_Status'] 			  	= $this->Page_Status;
			$Page_Data_ary['Page_InputDate'] 		  	= $this->Page_InputDate;
			$Page_Data_ary['Page_ModifiedBy'] 		  	= $this->Page_ModifiedBy;
			$Page_Data_ary['Page_ModifiedDate'] 	  	= $this->Page_ModifiedDate;
			$Page_Data_ary['Page_ModifiedByUser'] 	  	= $this->Page_ModifiedByUser;
			$Page_Data_ary['Page_EnglishName'] 		  	= $this->Page_EnglishName;
			$Page_Data_ary['Page_ClassName'] 		  	= $this->Page_ClassName;
			$Page_Data_ary['Page_ClassNumber'] 		  	= $this->Page_ClassNumber;
			$Page_Data_ary['Page_ArticleIDs']		  	= $this->Page_ArticleIDs;
			$Page_Data_ary['Page_ArticlePositionIDs'] 	= $this->Page_ArticlePositionIDs;
			
			$Page_Data_ary = array_merge($Newspaper_Data_ary, $Page_Data_ary);
			
			return $Page_Data_ary;
		}
		
		function get_page_raw_ary($PageID){
			global $cfg_ePost;
			
			$ArticleIDSeparator = '|=|';
				
			$sql = "SELECT
						ep.PageID,
						ep.NewspaperID,
						ep.Type AS Page_Type,
						ep.Layout AS Page_Layout,
						ep.Theme AS Page_Theme,
						ep.ThemeName AS Page_ThemeName,
						ep.Name AS Page_Name,
						ep.PageOrder AS Page_PageOrder,
						ep.Status AS Page_Status,
						ep.InputDate AS Page_InputDate,
						ep.ModifiedBy AS Page_ModifiedBy,
						ep.ModifiedDate AS Page_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Page_ModifiedByUser,
						iu.EnglishName AS Page_EnglishName,
						iu.ClassName AS Page_ClassName,
						iu.ClassNumber AS Page_ClassNumber,
						GROUP_CONCAT(DISTINCT ea.ArticleID ORDER BY ea.Position SEPARATOR '$ArticleIDSeparator') AS Page_ArticleIDs
					FROM
						EPOST_NEWSPAPER_PAGE AS ep INNER JOIN
						INTRANET_USER AS iu ON ep.ModifiedBy = iu.UserID LEFT JOIN
						EPOST_NEWSPAPER_PAGE_ARTICLE AS ea ON ep.PageID = ea.PageID AND ea.Status = '".$cfg_ePost['BigNewspaper']['Article_status']['exist']."'
					WHERE
						ep.PageID = '$PageID'
					GROUP BY
						ep.PageID";
			$result_ary = current($this->returnArray($sql)); 
			
			# Process Page ArticleID array
			$result_ary['Page_ArticleIDs'] = $result_ary['Page_ArticleIDs']? explode($ArticleIDSeparator, $result_ary['Page_ArticleIDs']) : array();
			
			# Newspaper Raw Array
			$Newspaper_Data_ary = $this->get_newspaper_raw_ary($result_ary['NewspaperID']);
			
			$result_ary = array_merge($Newspaper_Data_ary, $result_ary);
			
			return $result_ary;
		}
		
		function get_articles_raw_ary(){
			global $cfg_ePost;
			
			$result_ary = array();
			
			if(count($this->Page_ArticleIDs) > 0){
				$sql = "SELECT
							ea.ArticleID,
							ea.ArticleShelfID,
							ea.PageID,
							ea.Title AS Article_Title,
							ea.Content AS Article_Content,
							ea.Position AS Article_Position,
							ea.FontSize as Article_FontSize,
							ea.FontStyle as Article_FontStyle,
							ea.LineHeight as Article_LineHeight,
							ea.DisplayAuthor AS Article_DisplayAuthor,
							ea.DisplayTitle AS Article_DisplayTitle,
							ea.DisplayContent AS Article_DisplayContent,
							ea.Alignment AS Article_Alignment,
							ea.Size AS Article_Size,
							ea.Type AS Article_Type,
							ea.Theme AS Article_Theme,
							ea.Color AS Article_Color,
							ea.Status AS Article_Status,
							ea.Enjoy as Article_Enjoy,
							ea.InputDate AS Article_InputDate,
							ea.ModifiedBy AS Article_ModifiedeBy,
							ea.ModifiedDate AS Article_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Article_ModifiedByUser,
							iu.EnglishName AS Article_EnglishName,
							iu.ClassName AS Article_ClassName,
							iu.ClassNumber AS Article_ClassNumber
						FROM
							EPOST_NEWSPAPER_PAGE_ARTICLE AS ea INNER JOIN
							INTRANET_USER AS iu ON ea.ModifiedBy = iu.UserID
						WHERE
							ea.ArticleID IN ('".implode("','", $this->Page_ArticleIDs)."') AND
							ea.Status = '".$cfg_ePost['BigNewspaper']['Article_status']['exist']."'
						ORDER BY
							ea.Position";
				
				$result_ary = $this->returnArray($sql);
			}
			return $result_ary;
		}
	
		function get_articles(){
			if(count($this->Pages_Articles)==0){
				$Page_Data_ary = $this->page_obj_to_array();
				$articles_raw_ary = $this->get_articles_raw_ary();
				
				for($i=1;$i<=$this->return_page_max_article();$i++){
					$ArticleObj = null;
					
					for($j=0;$j<count($articles_raw_ary);$j++){
						if($i==$articles_raw_ary[$j]['Article_Position'])
						{
							$Article_Data_ary = array_merge($Page_Data_ary, $articles_raw_ary[$j]);
							$ArticleObj = new libepost_article(0, $Article_Data_ary);
							$ArticleObj->Article_CommentIDs = $ArticleObj->get_commentIds_by_articleId($ArticleObj->ArticleID);
							$ArticleObj->Article_Attachment = $ArticleObj->get_article_attachment($ArticleObj->ArticleID);
							
							break;
						}
					}
					
					$this->Pages_Articles[$i] = $ArticleObj;
				}
			}
			
			return $this->Pages_Articles;
		}
		function return_page_file_name($FileType){
			global $cfg_ePost;
			$FileName = '';
			switch($FileType){
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']: 
	    			$FileName = $this->Page_ThemeName;
	    			break;
	    	}
	    	return $FileName;
		}
		function return_page_attachment_http_path($FileType){
			$FileName = $this->return_page_file_name($FileType);
	    	
	    	if(preg_match('/^\/tmp\/'.$FileType.'\//', $FileName))
	    		$path = "/file/ePost/newspaper_attachment/".rawurlencode(addslashes($FileName));
	    	else
	    		$path = "/file/ePost/newspaper_attachment/".$this->NewspaperID."/".$FileType."/".$this->PageID."/".rawurlencode(addslashes($FileName));
	    	return $path;
		}	
		function get_page_attachment_folder($FileType){
			global $PATH_WRT_ROOT;
			$toDir = $PATH_WRT_ROOT.'file/ePost';
			if(!is_dir($toDir))
				 mkdir($toDir, 0777);
			$toDir .= '/newspaper_attachment';	 
			if(!is_dir($toDir))
				 mkdir($toDir, 0777);
			if(!empty($this->NewspaperID)){
				$toDir .= '/'.$this->NewspaperID;
				if(!is_dir($toDir))
					mkdir($toDir, 0777);
			}
			$toDir .= '/'.$FileType;
				if(!is_dir($toDir))
					mkdir($toDir, 0777);
			if(!empty($this->PageID)){
				$toDir .= '/'.$this->PageID;
				if(!is_dir($toDir))
					mkdir($toDir, 0777);
			}						
			return $toDir;	 
		}		
		function update_page_file($FileType){
			global $PATH_WRT_ROOT, $cfg_ePost;
			$FileName = $this->return_page_file_name($FileType);
	    	
			$result_file_name = '';
			
			# Check if upload attachment is image or not
			if(preg_match('/^\/tmp\/'.$FileType.'\//', $FileName) && !IsImage($PATH_WRT_ROOT.'file/ePost/newspaper_attachment'.$FileName)){
				return '';
			}
			
			if(!$FileName || preg_match('/^\/tmp\/'.$FileType.'\//', $FileName)){
				$dir = $PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID.'/'.$FileType.'/'.$this->PageID.'/';
				
				# Clear All Previous Uploaded Files
				if(is_dir($dir)){
					$cur_dir = opendir($dir);
				
					while(($file = readdir($cur_dir))!==false){
						if($file!='.' && $file!='..'){
							chmod($dir.$file, 0777);
							if(!is_dir($dir.$file))
								unlink($dir.$file);
						}
					}
				}
					
				if($FileName){
					# Create corresponding folders if not exist
					
					# Move the school logo from temp location to real location
					$source_file 	  = $PATH_WRT_ROOT.'file/ePost/newspaper_attachment'.$FileName;
					$destination_file = $this->get_page_attachment_folder($FileType);
					$destination_file .= '/'.get_file_basename($FileName); 
						
					if(is_file($source_file)){
						rename($source_file, $destination_file);
						
						# Update the DB for the real location
						if(is_file($destination_file)){
							rmdir(dirname($source_file));
							$result_file_name = get_file_basename($FileName);
						}
					}
				}
				else{
					if(is_dir($dir)) rmdir($dir);
				}
			}
			else
				$result_file_name = $FileName;
					
			return $result_file_name;
		}				
		function update_page_db($PageRefPage=0, $IsLayoutChanged=false){
			global $UserID, $cfg_ePost;
			
			if($this->PageID){
				$PageID 	 = $this->PageID;
				$InputDate   = $this->Page_InputDate;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_NEWSPAPER_PAGE
						SET
							NewspaperID = '".$this->NewspaperID."',
							Type = '".$this->Page_Type."',
							Layout = '".$this->Page_Layout."',
							Theme = '".$this->Page_Theme."',
							ThemeName = '".$this->Page_ThemeName."',
							Name = '".$this->Page_Name."',
							PageOrder = '".$this->Page_PageOrder."',
							Status = '".$this->Page_Status."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							PageID = '".$this->PageID."'";
				$result = $this->db_db_query($sql);
				
				# Remove All Articles if the Page Layout is changed
				$result = $result && $IsLayoutChanged? $this->delete_all_articles():$result;
			}
			else{
				$InputDate   = date('Y-m-d H:i:s');
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_NEWSPAPER_PAGE
							(
								NewspaperID, Type, Layout, Theme, ThemeName, Name,
								PageOrder, Status, Inputdate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								'".$this->NewspaperID."', '".$this->Page_Type."', '".$this->Page_Layout."', '".$this->Page_Theme."', '".$this->Page_ThemeName."', '".$this->Page_Name."',
								'".$this->Page_PageOrder."', '".$this->Page_Status."', '$InputDate', '$UserID', '$ModifedDate'
							)";
							
				$result = $this->db_db_query($sql);
				$PageID = $this->db_insert_id();
			}
			
			$result = $result? $this->move_target_page_after_ref_page($PageID, $PageRefPage) : $result;
			
			if($result){
				$sql = "SELECT EnglishName, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				
				$this->PageID			 = $PageID;
				$this->Page_InputDate	 = $InputDate;
				$this->Page_ModifiedBy   = $UserID;
				$this->Page_ModifiedDate = $ModifedDate;
				$this->Page_EnglishName  = $ModifiedBy_ary['EnglishName'];
				$this->Page_ClassName 	 = $ModifiedBy_ary['ClassName'];
				$this->Page_ClassNumber  = $ModifiedBy_ary['ClassNumber'];
				
				$this->Page_ThemeName   = $this->update_page_file($cfg_ePost['BigNewspaper']['AttachmentType']['ThemeIcon']);
				$sql = "UPDATE
							EPOST_NEWSPAPER_PAGE
						SET
							ThemeName = '".$this->Page_ThemeName."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							PageID = '".$this->PageID."'";
				$result = $this->db_db_query($sql);
			}
			
			return $result;
		}
		
		function delete_page(){
			global $cfg_ePost, $UserID;
			
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Page_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						PageID = '".$this->PageID."'";
			$result = $this->db_db_query($sql);
			
			$result = $this->delete_all_articles();
			
			# Update Pages PageOrder
			$PageIDs_ary_key = array_search($this->PageID, $this->Newspaper_PageIDs);
			
			if($PageIDs_ary_key!==false){
				array_splice($this->Newspaper_PageIDs, $PageIDs_ary_key, 1);
				
				if(count($this->Newspaper_Pages)){
					array_splice($this->Newspaper_Pages, $PageIDs_ary_key, 1);
				}
				
				if(count($this->Newspaper_PageIDs)){
					$result = $this->update_pages_PageOrder();
				}
			}
			return $result;
		}
		
		function delete_all_articles(){
			global $cfg_ePost, $UserID;
			
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE_ARTICLE
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Article_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						PageID = '".$this->PageID."'";
			$result = $this->db_db_query($sql);
			
			return $result;
		}
		
		function return_page_type(){
			return $this->Page_Type[0];
		}
		
		function return_page_max_article(){
			global $cfg_ePost;
			
			return count($cfg_ePost['BigNewspaper']['TypeLayout'][$this->Page_Type][$this->Page_Layout]['articles']);
		}
		
		function return_layout_image_path(){
			global $intranet_httppath, $cfg_ePost;
			
			$img_path = $cfg_ePost['BigNewspaper']['TypeLayout'][$this->Page_Type][$this->Page_Layout]['img']? $intranet_httppath."/images/ePost/issue_layout/".$cfg_ePost['BigNewspaper']['TypeLayout'][$this->Page_Type][$this->Page_Layout]['img'] : "";
			
			return $img_path;
		}
		
		function return_theme_image_path(){
			global $intranet_httppath, $cfg_ePost;
			
			$img_path = $cfg_ePost['BigNewspaper']['Theme'][$this->Page_Theme]['big_img']? $intranet_httppath."/images/ePost/newspaper/page_cat/".$cfg_ePost['BigNewspaper']['Theme'][$this->Page_Theme]['big_img'] : "";
			
			return $img_path;
		}
		
		function get_page_format(){
			global $cfg_ePost;
			
			if($this->return_page_type()==$cfg_ePost['BigNewspaper']['PageType']['Cover']){
				$page_format = 'C';
			}
			else{
				if(($this->Page_PageOrder % 2)==1)
					$page_format = 'L';
				else
					$page_format = 'R';
			}
			
			return $page_format;
		}
		
		function get_page_TypeLayout_articles_ary(){
			global $cfg_ePost;
			
			return $cfg_ePost['BigNewspaper']['TypeLayout'][$this->Page_Type][$this->Page_Layout]['articles'];
		}
		
		function get_article_restriction_name_ary($position){
			global $cfg_ePost;
			
			$article_cfg_ary = $this->get_page_TypeLayout_articles_ary();
			$article_cfg 	 = $article_cfg_ary[$position];
			$name_ary		 = array();
			
			$all_article_type_ary = $cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types'];
			
			foreach($all_article_type_ary as $article_tpye=>$article_tpye_ary){
				if(!in_array($article_tpye, $article_cfg['restriction']))
					$name_ary[] = $article_tpye_ary['name'];
			}
			
			return $name_ary;
		}
	}
	
	class libepost_article extends libepost_page
	{
		var $ArticleID;
		var $ArticleShelfID;
		var $Article_Position;
		var $Article_DisplayAuthor;
		var $Article_DisplayTitle;
		var $Article_DisplayContent;				
		var $Article_Alignment;
		var $Article_Size;
		var $Article_Type;
		var $Article_Theme;
		var $Article_Color;
		var $Article_Status;
		var $Article_InputDate;
		var $Article_ModifiedBy;
		var $Article_ModifiedDate;
		var $Article_ModifiedByUser;
		var $Article_EnglishName;
		var $Article_ClassName;
		var $Article_ClassNumber;	
		var $Article_Attachment;
		var $Article_Title;
		var $Article_Content;		
		var $Article_CommentIDs;		
		var $Article_Enjoy;
		var $Article_FontSize;
		var $Article_FontStyle;
		var $Article_LineHeight;
		var $Article_Comments;
				
		function libepost_article($ArticleID=0, $Article_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $ArticleID is inputed or not
			$Article_Data_ary = $ArticleID? $this->get_article_raw_ary($ArticleID) : $Article_Data_ary;
			
			# Initialize Article Object
			$this->initialize_article_obj($Article_Data_ary);
		}
		
		function initialize_article_obj($Article_Data_ary){
			# Initialize Page Object
			$this->initialize_page_obj($Article_Data_ary);
			
			$this->ArticleID 			  = $Article_Data_ary['ArticleID'];
			$this->ArticleShelfID		  = $Article_Data_ary['ArticleShelfID'];
			$this->Article_Position 	  = $Article_Data_ary['Article_Position'];
			$this->Article_DisplayAuthor  = $Article_Data_ary['Article_DisplayAuthor'];
			$this->Article_DisplayTitle  = $Article_Data_ary['Article_DisplayTitle'];
			$this->Article_DisplayContent  = $Article_Data_ary['Article_DisplayContent'];						
			$this->Article_Alignment 	  = $Article_Data_ary['Article_Alignment'];
			$this->Article_Size 		  = $Article_Data_ary['Article_Size'];					
			$this->Article_Type 		  = $Article_Data_ary['Article_Type'];
			$this->Article_Theme 		  = $Article_Data_ary['Article_Theme'];
			$this->Article_Color 		  = $Article_Data_ary['Article_Color'];
			$this->Article_Status 		  = $Article_Data_ary['Article_Status'];
			$this->Article_InputDate 	  = $Article_Data_ary['Article_InputDate'];
			$this->Article_ModifiedBy 	  = $Article_Data_ary['Article_ModifiedBy'];
			$this->Article_ModifiedDate   = $Article_Data_ary['Article_ModifiedDate'];
			$this->Article_ModifiedByUser = $Article_Data_ary['Article_ModifiedByUser'];
			$this->Article_EnglishName 	  = $Article_Data_ary['Article_EnglishName'];
			$this->Article_ClassName 	  = $Article_Data_ary['Article_ClassName'];
			$this->Article_ClassNumber    = $Article_Data_ary['Article_ClassNumber'];
			$this->Article_Attachment    = $Article_Data_ary['Article_Attachment'];		
			$this->Article_Title 	 	 = $Article_Data_ary['Article_Title'];
			$this->Article_Content   	 = $Article_Data_ary['Article_Content'];				
			$this->Article_CommentIDs    = $Article_Data_ary['Article_CommentIDs'];	
			$this->Article_Enjoy   		 = $Article_Data_ary['Article_Enjoy'];		
			$this->Article_FontSize   	 = $Article_Data_ary['Article_FontSize'];				
			$this->Article_FontStyle    = $Article_Data_ary['Article_FontStyle'];	
			$this->Article_LineHeight   = $Article_Data_ary['Article_LineHeight'];							
		}
			
		function article_obj_to_array(){
			# Get Page Object Array
			$Page_Data_ary = $this->page_obj_to_array();
			
			$Article_Data_ary['ArticleID'] 			    = $this->ArticleID;
			$Article_Data_ary['ArticleShelfID'] 	    = $this->ArticleShelfID;
			$Article_Data_ary['Article_Position'] 	    = $this->Article_Position;
			$Article_Data_ary['Article_DisplayAuthor']  = $this->Article_DisplayAuthor;
			$Article_Data_ary['Article_DisplayTitle']  = $this->Article_DisplayTitle;
			$Article_Data_ary['Article_DisplayContent']  = $this->Article_DisplayContent;						
			$Article_Data_ary['Article_Alignment'] 	    = $this->Article_Alignment;
			$Article_Data_ary['Article_Size'] 		    = $this->Article_Size;
			$Article_Data_ary['Article_Type'] 		    = $this->Article_Type;
			$Article_Data_ary['Article_Theme'] 		    = $this->Article_Theme;
			$Article_Data_ary['Article_Color'] 		    = $this->Article_Color;
			$Article_Data_ary['Article_Status'] 	    = $this->Article_Status;
			$Article_Data_ary['Article_InputDate'] 	    = $this->Article_InputDate;
			$Article_Data_ary['Article_ModifiedBy']     = $this->Article_ModifiedBy;
			$Article_Data_ary['Article_ModifiedDate']   = $this->Article_ModifiedDate;
			$Article_Data_ary['Article_ModifiedByUser'] = $this->Article_ModifiedByUser;
			$Article_Data_ary['Article_EnglishName']    = $this->Article_EnglishName;
			$Article_Data_ary['Article_ClassName'] 	    = $this->Article_ClassName;
			$Article_Data_ary['Article_ClassNumber']    = $this->Article_ClassNumber;
			$Article_Data_ary['Article_Attachment']    	= $this->Article_Attachment;	
			$Article_Data_ary['Article_Title'] 	    	= $this->Article_Title;
			$Article_Data_ary['Article_Content']   		= $this->Article_Content;
			$Article_Data_ary['Article_CommentIDs']   	= $this->Article_CommentIDs;
			$Article_Data_ary['Article_Enjoy']   		= $this->Article_Enjoy;		
			$Article_Data_ary['Article_FontSize']   	= $this->Article_FontSize;
			$Article_Data_ary['Article_FontStyle']   	= $this->Article_FontStyle;
			$Article_Data_ary['Article_LineHeight']   	= $this->Article_LineHeight;	
						
			$Article_Data_ary = array_merge($Page_Data_ary, $Article_Data_ary);

			return $Article_Data_ary;
		}
		
		function get_article_raw_ary($ArticleID){
			
			global $cfg_ePost;
			
			$sql = "SELECT
						ea.ArticleID,
						ea.ArticleShelfID,
						ea.PageID,
						ea.Title as Article_Title,
						ea.Content as Article_Content,
						ea.FontSize as Article_FontSize,
						ea.FontStyle as Article_FontStyle,
						ea.LineHeight as Article_LineHeight,
						ea.Position AS Article_Position,
						ea.DisplayAuthor AS Article_DisplayAuthor,
						ea.DisplayTitle AS Article_DisplayTitle,
						ea.DisplayContent AS Article_DisplayContent,
						ea.Alignment AS Article_Alignment,
						ea.Size AS Article_Size,
						ea.Type AS Article_Type,
						ea.Theme AS Article_Theme,
						ea.Color AS Article_Color,
						ea.Status AS Article_Status,
						ea.Enjoy AS Article_Enjoy,
						ea.InputDate AS Article_InputDate,
						ea.ModifiedBy AS Article_ModifiedeBy,
						ea.ModifiedDate AS Article_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Article_ModifiedByUser,
						iu.EnglishName AS Article_EnglishName,
						iu.ClassName AS Article_ClassName,
						iu.ClassNumber AS Article_ClassNumber
					FROM
						EPOST_NEWSPAPER_PAGE_ARTICLE AS ea INNER JOIN
						INTRANET_USER AS iu ON ea.ModifiedBy = iu.UserID
					WHERE
						ea.ArticleID = '$ArticleID'
					ORDER BY
						ea.Position";
			$result_ary = current($this->returnArray($sql)); 
		
			# Page Raw Array
			$Page_Data_ary = $this->get_page_raw_ary($result_ary['PageID']);
			
			$result_ary = array_merge($Page_Data_ary, $result_ary);
			# Process Article CommentID array
			$sql = "
				SELECT 
					CommentID 
				FROM 
					EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT 
				WHERE 
					ArticleID = '$ArticleID'
				AND
					Status = '".$cfg_ePost['BigNewspaper']['Comment_status']['exist']."' 
				ORDER BY InputDate
			";
			$comment_ary = $this->returnVector($sql); 
			$result_ary['Article_CommentIDs'] = $this->get_commentIds_by_articleId($ArticleID);
			$result_ary['Article_Attachment'] = $this->get_article_attachment($ArticleID);
			
			return $result_ary;
		}
		function get_commentIds_by_articleId($ArticleID){
			global $cfg_ePost;
			# Process Article CommentID array
			$sql = "
				SELECT 
					CommentID 
				FROM 
					EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT 
				WHERE 
					ArticleID = '".$ArticleID."'
				AND
					Status = '".$cfg_ePost['BigNewspaper']['Comment_status']['exist']."' 
				ORDER BY InputDate
			";
			$comment_ary = $this->returnVector($sql); 
			return count($comment_ary)>0? $comment_ary : array();
		}
		function save_article_attachment($updateAttachmentAry){
			global $UserID;
			foreach($updateAttachmentAry as $_attachmentAry){
				if(empty($_attachmentAry['status'])){
					$_attachmentAry['alignment']="";
					$_attachmentAry['size']="";
				}
				$sql = "";
				if($_attachmentAry['attachmentId']=='NEW'){
					$sql .= "INSERT INTO EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT (ArticleID,Attachment,Caption,Alignment,Size,Status,InputBy,InputDate,ModifiedBy,ModifiedDate) VALUES ";
					$sql .= "('".$this->ArticleID."','".$_attachmentAry['attachment']."','".intranet_htmlspecialchars($_attachmentAry['caption'])."','".$_attachmentAry['alignment']."','".$_attachmentAry['size']."','".$_attachmentAry['status']."','".$UserID."',NOW(),'".$UserID."',NOW())";
				}else{
					$sql .= "UPDATE EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT SET ";
					$sql .= "Attachment = '".$_attachmentAry['attachment']."',";
					$sql .= "Caption = '".intranet_htmlspecialchars($_attachmentAry['caption'])."',";
					$sql .= "Alignment = '".$_attachmentAry['alignment']."',";
					$sql .= "Size = '".$_attachmentAry['size']."',";
					$sql .= "Status = '".$_attachmentAry['status']."',";
					$sql .= "ModifiedBy = '".$UserID."',";
					$sql .= "ModifiedDate = NOW()";
					$sql .= " WHERE RecordID = '".$_attachmentAry['attachmentId']."'";
				}
				$this->db_db_query($sql);	
			}
			
			
		}
		
		function get_article_attachment($ArticleID){
			global $cfg_ePost;
			$sql = "
				SELECT 
					RecordID as attachmentId,
					ArticleID as articleId,
					Attachment as attachment,
					Alignment as alignment,
					Size as size,
					Status as status,
					Caption as caption
				FROM 
					EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT
				WHERE 
					ArticleID = '".$ArticleID."'
			";
			$result = $this->returnArray($sql);
			return BuildMultiKeyAssoc($result, 'attachmentId');
		}
		function get_article_attachment_folder(){
			global $PATH_WRT_ROOT;
			$toDir = $PATH_WRT_ROOT.'file/ePost';
			if(!is_dir($toDir))
				 mkdir($toDir, 0777);
			$toDir .= '/article_attachment';	 
			if(!is_dir($toDir))
				 mkdir($toDir, 0777);
			if(!empty($this->ArticleID)){
				$toDir .= '/'.$this->ArticleID;
				if(!is_dir($toDir))
					mkdir($toDir, 0777);
			}
			return $toDir;	 
		}
		function format_attachment_path($attachment,$Is_http_path=false){
			global $PATH_WRT_ROOT, $cfg_ePost;
	    	if(!empty($attachment)){
		    	$path = ($Is_http_path? "/" : $PATH_WRT_ROOT)."file/ePost/article_attachment/".$this->ArticleID."/".($Is_http_path? rawurlencode(addslashes($attachment)) : $attachment);
		    }
	    	return $path;
		}		
		function update_article_db(){
			global $UserID, $cfg_ePost;
			
			if($this->ArticleID){
				$ArticleID 	 = $this->ArticleID;
				$InputDate   = $this->Article_InputDate;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_NEWSPAPER_PAGE_ARTICLE
						SET
							ArticleShelfID = '".$this->ArticleShelfID."',
							PageID = '".$this->PageID."',
							Position = '".$this->Article_Position."',
							Title = '".intranet_htmlspecialchars($this->Article_Title)."',
							Content = '".intranet_htmlspecialchars($this->Article_Content)."',
							FontSize = '".$this->Article_FontSize."',
							FontStyle = '".$this->Article_FontStyle."',
							LineHeight = '".$this->Article_LineHeight."',
							DisplayAuthor = '".$this->Article_DisplayAuthor."',
							DisplayTitle = '".$this->Article_DisplayTitle."',
							DisplayContent = '".$this->Article_DisplayContent."',
							Alignment = '".$this->Article_Alignment."',
							Size = ".(is_null($this->Article_Size)? "NULL":"'".$this->Article_Size."'").",
							Type = '".$this->Article_Type."',
							Theme = '".$this->Article_Theme."',
							Color = '".$this->Article_Color."',
							Status = '".$this->Article_Status."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							ArticleID = '".$this->ArticleID."'";
				$result = $this->db_db_query($sql);
			}
			else{
				$InputDate   = date('Y-m-d H:i:s');
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_NEWSPAPER_PAGE_ARTICLE
							(
								ArticleShelfID, PageID, Title, Content, FontSize, FontStyle, LineHeight, 
								DisplayAuthor, DisplayTitle, DisplayContent, Position, 
								Type, Theme, Color, Status,
								Inputdate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								'".$this->ArticleShelfID."', '".$this->PageID."', '".intranet_htmlspecialchars($this->Article_Title)."', '".intranet_htmlspecialchars($this->Article_Content)."', '".$this->Article_FontSize."', '".$this->Article_FontStyle."', '".$this->Article_LineHeight."', 
								'".$this->Article_DisplayAuthor."', '".$this->Article_DisplayTitle."', '".$this->Article_DisplayContent."', '".$this->Article_Position."',
								'".$this->Article_Type."', '".$this->Article_Theme."', '".$this->Article_Color."', '".$this->Article_Status."',
								'$InputDate', '$UserID', '$ModifedDate'
							)";
				$result = $this->db_db_query($sql);
				$ArticleID = $this->db_insert_id();
			}	
			if($result){
				$sql = "SELECT EnglishName, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				
				$this->ArticleID		    = $ArticleID;
				$this->Article_InputDate	= $InputDate;
				$this->Article_ModifiedBy   = $UserID;
				$this->Article_ModifiedDate = $ModifedDate;
				$this->Article_EnglishName  = $ModifiedBy_ary['EnglishName'];
				$this->Article_ClassName 	= $ModifiedBy_ary['ClassName'];
				$this->Article_ClassNumber  = $ModifiedBy_ary['ClassNumber'];
			}
		
			return $result;
		}
		
		function delete_article(){
			global $cfg_ePost, $UserID;
			
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE_ARTICLE
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Article_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						ArticleID = '".$this->ArticleID."'";
			$result = $this->db_db_query($sql);
			$commentObjs = $this->get_comments();
			foreach((array)$commentObjs as $_commentObj){
				$_commentObj->delete_comment();
			}
			$this->delete_article_attachments();
			return $result;
		}
		function delete_article_attachments(){
			global $PATH_WRT_ROOT;
			$dir = $PATH_WRT_ROOT.'file/ePost/writing_attachment/'.$this->ArticleID.'/';
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$fs = new libfilesystem();
			$fs->deleteDirectory($dir);
			$sql = "
					DELETE FROM
						EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT 
					WHERE
						ArticleID = '".$this->ArticleID."'";
			$this->db_db_query($sql);
		}
		function delete_article_attachment($attachmentId){
			global $PATH_WRT_ROOT;
			
			$dir = $PATH_WRT_ROOT.'file/ePost/article_attachment/'.$this->ArticleID.'/';	
			if(is_dir($dir)){
				$cur_dir = opendir($dir);
				$file = $this->Article_Attachment[$attachmentId]['attachment'];
				if($file!='.' && $file!='..'){
					chmod($dir.$file, 0777);
					if(!is_dir($dir.$file)){
						$ext = getFileExtention($file);
						if($ext=='FLV'||$ext=='MOV'||$ext=='MP4'){
							list($filename,$filetype)=explode(".",$file);
							if(file_exists($dir.$filename.".flv"))	unlink($dir.$filename.".flv");
							if(file_exists($dir.$filename.".mov"))	unlink($dir.$filename.".mov");
							if(file_exists($dir.$filename.".mp4"))	unlink($dir.$filename.".mp4");
						}else{
							unlink($dir.$file);
						}
						$sql = "DELETE FROM EPOST_NEWSPAPER_PAGE_ARTICLE_ATTACHMENT WHERE RecordID = '".$attachmentId."'";
						$this->db_db_query($sql);
						unset($this->Article_Attachment[$attachmentId]);
					}	
				}
			}		
		}	
		### ARTICLE COMMENT ###
		function get_comments_raw_ary(){
			global $cfg_ePost;
			
			$result_ary = array();
			
			if(count($this->Article_CommentIDs) > 0){
				$sql = "SELECT
						ec.CommentID,
						ec.ArticleID AS ArticleID,
						ec.Content AS Content,
						ec.Status AS Status,
						ec.InputBy AS InputBy,
						ec.InputDate AS InputDate,
						ec.ModifiedBy AS ModifiedBy,
						ec.ModifiedDate AS ModifiedDate,
						".getNameFieldByLang('iiu.')." AS InputByUser,
						iiu.RecordType AS InputByMemberType,
						iiu.EnglishName AS InputByEnglishName,
						iiu.ClassName AS InputByClassName,
						iiu.ClassNumber AS InputByClassNumber,
						".getNameFieldByLang('miu.')." AS ModifiedByUser,
						miu.EnglishName AS ModifiedByEnglishName,
						miu.ClassName AS ModifiedByClassName,
						miu.ClassNumber AS ModifiedByClassNumber,
						CONCAT(
							   IF(
									iiu.ClassName IS NOT NULL AND iiu.ClassName !='',
									CONCAT(
											' (',
											iiu.ClassName,
											IF(
												iiu.ClassNumber IS NOT NULL AND iiu.ClassNumber !='',
												CONCAT(' - ', iiu.ClassNumber, ')'),
												')'
											)
									),
									''
								 ),iiu.EnglishName
						) AS DisplayInputUserName
					FROM
						EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT AS ec INNER JOIN
						INTRANET_USER AS iiu ON ec.InputBy = iiu.UserID INNER JOIN
						INTRANET_USER AS miu ON ec.ModifiedBy = miu.UserID
					WHERE
						ec.CommentID IN ('".implode("','", $this->Article_CommentIDs)."') AND
						ec.Status = '".$cfg_ePost['BigNewspaper']['Comment_status']['exist']."'
					ORDER BY
						ec.InputDate ASC";
				$result_ary = $this->returnArray($sql);
			}
			
			return $result_ary;
		}
		
		function get_comments(){
			if(count($this->Article_CommentIDs) > 0){
				$this->Article_Comments = array();
				
				$comments_raw_ary = $this->get_comments_raw_ary();
			
				for($i=0;$i<count($this->Article_CommentIDs);$i++){
					$this->Article_Comments[$i] = new libepost_article_comment(0, $comments_raw_ary[$i]);
				}
			}
		
			return $this->Article_Comments;
		}
		
		function add_comment($Content){
			global $cfg_ePost;
			
			$this->get_comments();
			$Comment_Data_ary['ArticleID'] = $this->ArticleID;			
			$Comment_Data_ary['Content']   = $Content;
			$Comment_Data_ary['Status']    = $cfg_ePost['BigNewspaper']['Comment_status']['exist'];
			
			$CommentObj = new libepost_article_comment(0, $Comment_Data_ary);
			$result = $CommentObj->update_comment_db(); 
			if($result){
				$this->Article_CommentIDs[] = $CommentObj->CommentID;
				$this->Article_Comments[]   = $CommentObj;
			}
			
			return $result;
		}
		
		function edit_comment($CommentID, $Content){
			$this->get_comments();
			
			for($i=0;$i<count($this->Article_Comments);$i++){
				if($this->Article_Comments[$i]->CommentID==$CommentID){
					$this->Article_Comments[$i]->Content = $Content;
					return $this->Article_Comments[$i]->update_comment_db();
				}
			}
			
			return false;
		}
		
		function delete_comment($CommentID){
			global $cfg_ePost;
			
			$this->get_comments();
			
			$result = true;
			$tmp_comment_id_ary = array();
			$tmp_comment_ary 	= array();
			
			for($i=0;$i<count($this->Article_Comments);$i++){
				if($this->Article_Comments[$i]->CommentID==$CommentID){
					$this->Article_Comments[$i]->Status = $cfg_ePost['BigNewspaper']['Comment_status']['deleted'];
					$result = $this->Article_Comments[$i]->update_comment_db();
					
					if(!$result){ // If fail to update, put it back to original position
						$tmp_comment_id_ary[] = $this->Article_Comments[$i]->CommentID;
						$tmp_comment_ary[] 	  = $this->Article_Comments[$i];
					}
				}
				else{
					$tmp_comment_id_ary[] = $this->Article_Comments[$i]->CommentID;
					$tmp_comment_ary[] 	  = $this->Article_Comments[$i];
				}
			}
			
			$this->Article_CommentIDs = $tmp_comment_id_ary;
			$this->Article_Comments   = $tmp_comment_ary;
			
			return $result;
		}
		function gen_student_comment_list(){
			global $Lang,$UserID;
	    	$studentCommentCnt = count($this->Article_Comments);
			$content = '';
			for($i=0;$i<$studentCommentCnt;$i++){
	        	$libepost_article_comment = $this->Article_Comments[$i];
	        	$content .= "   	<li id=\"li_comment_".$libepost_article_comment->CommentID."\"><div style=\"float:left;\"><em> ".$libepost_article_comment->ModifiedByUser."</em> ".$libepost_article_comment->convertBad($libepost_article_comment->Content)."</div>";
	        	$content .= "		<div class=\"table_row_tool\" style=\"float:right;\">";
	        	if($this->is_super_admin || (($libepost_article_comment->InputBy==$UserID||$libepost_article_comment->InputByMemberType==USERTYPE_STUDENT)&&$this->user_obj->isTeacherStaff())){
					$content .= "		<a href=\"javascript:go_delete_comment(".$libepost_article_comment->ArticleID.",".$libepost_article_comment->CommentID.")\" class=\"tool_delete\" title=\"".$Lang['ePost']['Delete']."\">&nbsp;</a>";
				}
				$content .= "		</div>";
	        	$content .= "        	<p class=\"spacer\"></p><span class=\"date_time\">".parseDateTime($libepost_article_comment->ModifiedDate)."</span>";
	        	$content .= "<p class=\"spacer\"></p>";
	       		$content .= "       </li>";        
	        }
	        return $content;
			
	    }			
	}
	class libepost_article_comment extends libepost{
		var $CommentID;
		var $ArticleID;	
		var $Content;
		var $Status;
		var $InputBy;
		var $InputDate;
		var $InputByMemberType;
		var $ModifiedBy;
		var $ModifiedDate;
		var $InputByUser;
		var $InputByEnglishName;
		var $InputByClassName;
		var $InputByClassNumber;
		var $DisplayInputUserName;
		var $ModifiedByUser;
		var $ModifiedByEnglishName;
		var $ModifiedByClassName;
		var $ModifiedByClassNumber;
		var $bad_words;
		
		function libepost_article_comment($CommentID=0, $Comment_Data_ary=array()){
			# Initialize ePost Object
			
			$this->libepost();
			
			# Check if $CommentID is inputed or not
			$Comment_Data_ary = $CommentID? $this->get_comment_raw_ary($CommentID) : $Comment_Data_ary;
			
			# Initialize Comment Object
			$this->initialize_comment_obj($Comment_Data_ary);
		}
		
		function initialize_comment_obj($Comment_Data_ary){
			$this->CommentID			 = $Comment_Data_ary['CommentID'];
			$this->ArticleID		 = $Comment_Data_ary['ArticleID'];			
			$this->Content				 = $Comment_Data_ary['Content'];
			$this->Status				 = $Comment_Data_ary['Status'];
			$this->InputBy				 = $Comment_Data_ary['InputBy'];
			$this->InputDate			 = $Comment_Data_ary['InputDate'];
			$this->InputByMemberType	 = $Comment_Data_ary['InputByMemberType'];
			$this->ModifiedBy			 = $Comment_Data_ary['ModifiedBy'];
			$this->ModifiedDate			 = $Comment_Data_ary['ModifiedDate'];
			$this->InputByUser			 = $Comment_Data_ary['InputByUser'];
			$this->InputByEnglishName	 = $Comment_Data_ary['InputByEnglishName'];
			$this->InputByClassName		 = $Comment_Data_ary['InputByClassName'];
			$this->InputByClassNumber	 = $Comment_Data_ary['InputByClassNumber'];
			$this->ModifiedByUser		 = $Comment_Data_ary['ModifiedByUser'];
			$this->ModifiedByEnglishName = $Comment_Data_ary['ModifiedByEnglishName'];
			$this->ModifiedByClassName	 = $Comment_Data_ary['ModifiedByClassName'];
			$this->ModifiedByClassNumber = $Comment_Data_ary['ModifiedByClassNumber'];
			$this->DisplayInputUserName = $Comment_Data_ary['DisplayInputUserName']; 
		}
		
		function comment_obj_to_array(){
			$Comment_Data_ary['CommentID'] 				= $this->CommentID;
			$Comment_Data_ary['ArticleID']			= $this->ArticleID;			
			$Comment_Data_ary['Content'] 				= $this->Content;
			$Comment_Data_ary['Status'] 				= $this->Status;
			$Comment_Data_ary['InputBy'] 				= $this->InputBy;
			$Comment_Data_ary['InputDate'] 				= $this->InputDate;
			$Comment_Data_ary['InputByMemberType'] 		= $this->InputByMemberType;
			$Comment_Data_ary['ModifiedBy'] 			= $this->ModifiedBy;
			$Comment_Data_ary['ModifiedDate'] 			= $this->ModifiedDate;
			$Comment_Data_ary['InputByUser'] 			= $this->InputByUser;
			$Comment_Data_ary['InputByEnglishName'] 	= $this->InputByEnglishName;
			$Comment_Data_ary['InputByClassName'] 		= $this->InputByClassName;
			$Comment_Data_ary['InputByClassNumber'] 	= $this->InputByClassNumber;
			$Comment_Data_ary['ModifiedByUser'] 		= $this->ModifiedByUser;
			$Comment_Data_ary['ModifiedByEnglishName'] 	= $this->ModifiedByEnglishName;
			$Comment_Data_ary['ModifiedByClassName']   	= $this->ModifiedByClassName;
			$Comment_Data_ary['ModifiedByClassNumber'] 	= $this->ModifiedByClassNumber;
			
			return $Comment_Data_ary;
		}
		
		function get_comment_raw_ary($CommentID){
			$sql = "SELECT
						ec.CommentID,
						ec.ArticleID AS ArticleID,
						ec.Content AS Content,
						ec.Status AS Status,
						ec.InputBy AS InputBy,
						ec.InputDate AS InputDate,
						ec.ModifiedBy AS ModifiedBy,
						ec.ModifiedDate AS ModifiedDate,
						".getNameFieldByLang('iiu.')." AS InputByUser,
						iiu.RecordType AS InputByMemberType,
						iiu.EnglishName AS InputByEnglishName,
						iiu.ClassName AS InputByClassName,
						iiu.ClassNumber AS InputByClassNumber,
						".getNameFieldByLang('miu.')." AS ModifiedByUser,
						miu.EnglishName AS ModifiedByEnglishName,
						miu.ClassName AS ModifiedByClassName,
						miu.ClassNumber AS ModifiedByClassNumber
					FROM
						EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT AS ec INNER JOIN
						INTRANET_USER AS iiu ON ec.InputBy = iiu.UserID INNER JOIN
						INTRANET_USER AS miu ON ec.ModifiedBy = miu.UserID
					WHERE
						ec.CommentID = '$CommentID'";
					
			$result_ary = current($this->returnArray($sql));
			return $result_ary;
		}
		# badword - copied from eclass40/src/includes/php/lib-bulletin.php#
		function convertBad($ParWord)
	    {
			global $intranet_root,$imail_bad_words_replacement;
	
			if ($imail_bad_words_replacement=="")
			{
				$imail_bad_words_replacement = "***";
			}
	
			if ($this->bad_words==null)
			{
				# load the bad words
				$base_dir = "$intranet_root/file/templates/";
				$target_file = "$base_dir"."bulletin_badwords.txt";
				$data = trim(get_file_content($target_file));
				$this->bad_words = $data;
			} else
			{
				# already loaded the bad words
				$data = $this->bad_words;
			}
	
			# no need to filter if no bad words set
			if ($data=="")
			{
				return $ParWord;
			}
	
			$mb_string_enabled = function_exists("mb_ereg_replace");
	
			$bad_words = explode("\n",$data);
	
			$itr_bad_word = '(';
			for ($i=0; $i<sizeof($bad_words); $i++)
			{
				if (trim($bad_words[$i]) != "") $itr_bad_word .= '('.(trim($bad_words[$i])).')|';
			}
			$itr_bad_word = substr($itr_bad_word, 0, -1).')(?=[^>]*(<|$))';
	
			# should handle using mb_string function if available
			# otherwise, convert both into utf-8 to replace and then convert the result back to GB/BIG5
			if ($mb_string_enabled)
			{
				$ReturnStr = mb_ereg_replace($itr_bad_word, $imail_bad_words_replacement, $ParWord);
			} 
			else
			{
				$itr_bad_word = convert2unicode($itr_bad_word, 1, 1);
				$ParWord = convert2unicode($ParWord, 1, 1);
	
				$ReturnStr = preg_replace($itr_bad_word, $imail_bad_words_replacement, $ParWord);
				$ReturnStr = convert2unicode($ReturnStr, 1, -1);
			}
	
			return $ReturnStr;
	     }
	
		# badword #		
		
		function delete_comment(){
			global $cfg_ePost, $UserID;
			if($this->is_editor&&$this->user_obj->isTeacherStaff()){
				$ModifedDate = date('Y-m-d H:i:s');
				$sql = "UPDATE
							EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT
						SET
							Status = '".$cfg_ePost['BigNewspaper']['Comment_status']['deleted']."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							CommentID = '".$this->CommentID."'";
				$result = $this->db_db_query($sql);
			}
			return $result;
		}
		function update_comment_db(){
			global $cfg_ePost, $UserID;
			
			if($this->CommentID){
				$CommentID   = $this->CommentID;
				$InputBy     = $this->InputBy;
				$InputDate   = $this->InputDate;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT
						SET
							ArticleID = '".$this->ArticleID."',
							Content = '".$this->Content."',
							Status = '".$this->Status."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							CommentID = '".$this->CommentID."'";
				$result = $this->db_db_query($sql);
			}
			else{
				$InputBy     = $UserID;
				$InputDate   = date('Y-m-d H:i:s');
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_NEWSPAPER_PAGE_ARTICLE_COMMENT
							(
								ArticleID, Content, Status, InputBy, InputDate,
								ModifiedBy, ModifiedDate	
							)
						VALUES
							(
								'".$this->ArticleID."', '".$this->Content."', '".$this->Status."', '$UserID', '$InputDate',
								'$UserID', '$ModifedDate'
							)"; 
				$result    = $this->db_db_query($sql);
				$CommentID = $this->db_insert_id();
			}
			if($result){
				$this->CommentID 	= $CommentID;
				$this->InputBy 		= $InputBy;
				$this->InputDate 	= $InputDate;
				$this->ModifiedBy 	= $UserID;
				$this->ModifiedDate = $ModifedDate;
		
				$sql = "SELECT EnglishName, ".getNameFieldByLang()." AS InputByUser, ClassName, ClassNumber, RecordType AS InputByMemberType FROM INTRANET_USER WHERE UserID = '$InputBy'";
				$InputBy_ary = current($this->returnArray($sql));
				$this->InputByUser = $InputBy_ary['InputByUser'];
				$this->InputByEnglishName = $InputBy_ary['EnglishName'];
				$this->InputByClassName   = $InputBy_ary['ClassName'];
				$this->InputByClassNumber = $InputBy_ary['ClassNumber'];
				$this->InputByMemberType = $InputBy_ary['InputByMemberType'];
				
				$sql = "SELECT EnglishName, ".getNameFieldByLang()." AS ModifiedByUser, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				$this->ModifiedByUser = $ModifiedBy_ary['ModifiedByUser'];
				$this->ModifiedByEnglishName = $ModifiedBy_ary['EnglishName'];
				$this->ModifiedByClassName 	 = $ModifiedBy_ary['ClassName'];
				$this->ModifiedByClassNumber = $ModifiedBy_ary['ClassNumber'];
			}
			
			return $result;
		}
	}		
}