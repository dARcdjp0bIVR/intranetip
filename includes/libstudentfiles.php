<?php
# using: 

if (!defined("LIBSTUDENTFILES_DEFINED"))         // Preprocessor directives
{
 define("LIBSTUDENTFILES_DEFINED",true);

 class libstudentfiles extends libclass{
       var $FileID;
       var $UserID;
       var $year;
       var $semester;
       var $Title;
       var $Description;
       var $FileType;
       var $FileSize;
       var $FileName;
       var $path;
       var $RecordType;
       var $RecordStatus;
       var $DateInput;
       var $DateModified;

       function libstudentfiles($FileID="")
       {
                $this->libclass();
                if ($FileID != "")
                {
                    $this->FileID = $FileID;
                    $this->retrieveRecord($this->FileID);
                }
       }
       function retrieveRecord($id)
       {
                $fields = "UserID, Year, Semester, Title,Description,FileType,FileSize,FileName,Path, RecordType, RecordStatus,DateInput,DateModified";
                $conds = "FileID = $id";
                $sql = "SELECT $fields FROM PROFILE_STUDENT_FILES WHERE $conds";
                $result = $this->returnArray($sql,13);
                list(
                $this->UserID,$this->year,$this->semester, $this->Title,
                $this->Description,$this->FileType,$this->FileSize,$this->FileName,
                $this->path, $this->RecordType,
                $this->RecordStatus, $this->DateInput,$this->DateModified) = $result[0];
                return $result;
       }
       function returnYears($studentid)
       {
           $sql = "SELECT DISTINCT Year FROM PROFILE_STUDENT_FILES WHERE UserID = '".IntegerSafe($studentid)."' ORDER BY Year DESC";
                return $this->returnVector($sql);
       }
       function returnArchiveYears($studentid)
       {
           $sql = "SELECT DISTINCT Year FROM PROFILE_ARCHIVE_FILES WHERE UserID = '".IntegerSafe($studentid)."' ORDER BY Year DESC";
                return $this->returnVector($sql);
       }       
       function getClassFilesList()
       {
                $sql = "SELECT c.YearClassID, COUNT(a.FileID)
                        FROM PROFILE_STUDENT_FILES as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        LEFT OUTER JOIN YEAR_CLASS as c ON b.ClassName = c.ClassTitleEN
                        GROUP BY c.YearClassID";
                $result = $this->returnArray($sql,2);
                $counts = array();
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($ClassID,$fileCount) = $result[$i];
                     $counts[$ClassID] = $fileCount;
                }

                $classList = $this->getClassList();
                $returnResult = array();
                for ($i=0; $i<sizeof($classList); $i++)
                {
                     list($ClassID, $ClassName, $LvlID) = $classList[$i];
                     $fileCount = $counts[$ClassID]+0;
                     $returnResult[] = array($ClassID, $ClassName, $fileCount, $LvlID);
                }
                return $returnResult;
       }
       function getClassFilesListByClass($classid)
       {
                $sql = "SELECT b.UserID, COUNT(a.FileID)
                        FROM 
                        PROFILE_STUDENT_FILES as a 
                        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        LEFT OUTER JOIN YEAR_CLASS as c ON c.ClassTitleEN = b.ClassName
                        WHERE c.YearClassID = '".IntegerSafe($classid)."'
                        GROUP BY b.UserID";
                $result = $this->returnArray($sql,2);
                $counts = array();
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($StudentID,$count) = $result[$i];
                     $counts[$StudentID] = $count;
                }

                $studentList = $this->getClassStudentNameList($classid);
                $returnResult = array();
                for ($i=0; $i<sizeof($studentList); $i++)
                {
                     list($StudentID, $StudentName, $ClassNumber) = $studentList[$i];
                     $count = $counts[$StudentID]+0;
                     $returnResult[] = array($StudentID, $StudentName,$ClassNumber, $count);
                }
                return $returnResult;
       }

       function getFileListByStudent($studentid, $year,$sem="")
       {
                $conds = ($year != ""? " AND a.Year = '$year'":"");
                $conds = ($sem != ""? " AND a.Semester = '$sem'":"");
                $sql = "SELECT a.FileID, a.Year, a.Semester, a.Title,a.FileType,a.DateModified
                               FROM PROFILE_STUDENT_FILES as a
                               WHERE a.UserID = '".IntegerSafe($studentid)."' $conds
                               ORDER BY a.Year DESC, a.Semester, a.Title";
                return $this->returnArray($sql,6);
       }
       function displayFileListAdmin($studentid, $year="",$sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_Profile_Year,$i_Profile_Semester,$i_ReferenceFiles_Title,$i_ReferenceFiles_FileType,$i_LastModified;
                global $i_no_record_exists_msg;
                $result = $this->getFileListByStudent($studentid, $year,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_Profile_Year</td>
                      <td width=110 align=center class=tableTitle_new>$i_Profile_Semester</td>
                      <td width=90 align=center class=tableTitle_new>$i_ReferenceFiles_Title</td>
                      <td width=170 align=center class=tableTitle_new>$i_ReferenceFiles_FileType</td>
                      <td width=120 align=center class=tableTitle_new>$i_LastModified</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($FileID, $year,$sem,$title,$filetype,$lastMod) = $result[$i];
                     $title = "<a href=javascript:viewFile($FileID)>$title</a>";
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$title</td>
                              <td align=center>$filetype</td>
                              <td align=center>$lastMod</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       

        // ++++++ for archive student ++++++ \\         
       
       function displayArchiveFileListAdmin($studentid, $year="",$sem="")
       {
                global $image_path, $intranet_session_language;
                global $i_Profile_Year,$i_Profile_Semester,$i_ReferenceFiles_Title,$i_ReferenceFiles_FileType,$i_LastModified;
                global $i_no_record_exists_msg;
                $result = $this->getArchiveFileListByStudent($studentid, $year,$sem);
                $x = "
                  <table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
                    <tr>
                      <td width=70 align=center class=tableTitle_new>$i_Profile_Year</td>
                      <td width=110 align=center class=tableTitle_new>$i_Profile_Semester</td>
                      <td width=90 align=center class=tableTitle_new>$i_ReferenceFiles_Title</td>
                      <td width=170 align=center class=tableTitle_new>$i_ReferenceFiles_FileType</td>
                      <td width=120 align=center class=tableTitle_new>$i_LastModified</td>
                    </tr>\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($FileID, $year,$sem,$title,$filetype,$lastMod) = $result[$i];
                     $title = "<a href=javascript:viewFile($FileID)>$title</a>";
                     $x .= "
                      <tr>
                              <td align=center>$year</td>
                              <td align=center>$sem</td>
                              <td align=center>$title</td>
                              <td align=center>$filetype</td>
                              <td align=center>$lastMod</td>
                            </tr>\n";
                }
                if (sizeof($result)==0)
                {
                    $x .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       
       function getArchiveFileListByStudent($studentid, $year,$sem="")
       {
                $conds = ($year != ""? " AND a.Year = '$year'":"");
                $conds = ($sem != ""? " AND a.Semester = '$sem'":"");
                $sql = "SELECT RecordID, Year, Semester, Title, FileType, DateModified
                               FROM PROFILE_ARCHIVE_FILES as a
                               WHERE UserID = '$studentid' $conds
                               ORDER BY Year DESC, Semester, Title";                               
                return $this->returnArray($sql,6);
       }       
       
       function setArchiveFileID($RecordID){
	            $this->libclass();
	            
                if ($RecordID != "")
                {
                    $this->FileID = $RecordID;
                    $this->retrieveArchiveRecord($this->FileID);
                }
              
       }
       
       function retrieveArchiveRecord($id)
       {
                $fields = "UserID, Year, Semester, Title,Description,FileType,FileSize,FileName,Path,DateInput,DateModified";
                $conds = "RecordID = '$id'";
                $sql = "SELECT $fields FROM PROFILE_ARCHIVE_FILES WHERE $conds";
                $result = $this->returnArray($sql,11);   
                list(
                $this->UserID,$this->year,$this->semester, $this->Title,
                $this->Description,$this->FileType,$this->FileSize,$this->FileName,
                $this->path, $this->DateInput,$this->DateModified) = $result[0];               
                return $result;
       }
              
       // +++++ end of archive student +++++ \\                          
       
 }


} // End of directives
?>