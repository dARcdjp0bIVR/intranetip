<?php
/**
 * Using: 
 *
 * 2020-10-07 Cameron
 *      - hide checkout button for student in getShoppingCart(), show remark to inform parent instead
 *
 * 2020-06-08 Cameron
 *      - add function invalidChildForTheParent()
 *      - fix to use ascii colon instead of the utf-8 counterpart in getShoppingCart()
 *
 * 2020-04-22 Cameron
 *      - get photo from outer photo directory instead of original if original photo path does not exist in getZoomInItemPhotoDialog()
 *      - retrieve active items only in getCategoryItem()
 *      
 * 2020-04-16 Cameron
 *      - modify getOrderDetails() and getReceiptDetails() to handle case of all items are void
 *      
 * 2020-04-15 Cameron
 *      - also show records which grand total is zero and items are void in getOrderDetails() as picked up record
 *      
 * 2020-04-03 Cameron
 *      - modify getOrderDetails() to retrieve totalQty and grandTotal by calling Get_POS_Transaction_Log_Data
 *       
 * 2020-02-06 Cameron
 *      - change PhotoPath to OriginalPhotoPath in getZoomInItemPhotoDialog()
 *      
 * 2019-12-17 Cameron
 *      - hide getPowerByFooter()
 *      - show qrcode only if php verison >= 5.4
 *
 * 2019-11-29 Cameron
 *      - allow negative balance to process if $sys_custom['ePOS']['AllowNegativeBalance'] = true is set in getBeforePayByDeposit()
 *
 * 2019-11-20 Cameron
 *      - show qrcode in getOrderDetails()
 *
 * 2019-11-12 Ray
 *      - Add PaymentMethod & Remark getBeforePayByDeposit
 *
 * 2019-11-04 Cameron
 *      - apply number_format without thousand separator to number fields for money in getItemInShoppingCart(), getShoppingCart(), getOrderDetails(), getReceiptDetails()
 *
 * 2019-10-21 Cameron
 *      - show active category only in getAllCategories()
 *
 * 2019-10-15 Cameron
 *      - pass argument $receiveAmount to getLastReceiveTime() in getOrderDetails()
 *
 * 2019-08-20 Cameron
 *      - show Class Selection and Student List Selection for ePOS admin in getTopBarMenu()
 *
 * 2019-07-09 Cameron
 *      - create this file
 */

class libpos_app_ui extends libpos_ui
{
    // get Power By footer
    function getPowerByFooter()
    {
//        $x = "<div id=\"blkFooter\">
//                <span id=\"lbleClass\"><span>Powered by</span><a href=\"http://eclass.com.hk\" title=\"eClass\" target=\"_blank\"><img src=\"../web/images/eClassLogo.png\"></a></span>
//              </div>";
        $x = '';
        return $x;
    }

    // $className and $studentID are pased from teacher purchase on behalf of student
    function getTopBarMenu($className='', $studentID='', $enableSelectStudent=false, $children=array())
    {
        global $Lang, $indexVar;

        $libpos = $indexVar['libpos'];

        $studentName = $libpos->getUserToDisplay($_SESSION['ePosStudentID']);
        $unpickupDetailAry = $libpos->getUnpickupDetailByStudent($_SESSION['ePosStudentID']);
        $nrUnpickup = count($unpickupDetailAry);
        if ($nrUnpickup) {
            $unpickupStyle = '';
        }
        else {
            $unpickupStyle = 'none';
        }

        $pickupDetailAry = $libpos->getPickupDetailByStudent($_SESSION['ePosStudentID']);
        $nrPickup = count($pickupDetailAry);
        if ($nrPickup) {
            $pickupStyle = '';
        }
        else {
            $pickupStyle = 'none';
        }

        ob_start();
?>
    <form name="form2" id="form2" method="post" action="?task=teacherApp">
        <div id="blkTopBar" class="transition">

            <div id="blkUser">
                <right>
                    <right id="btnHistory" class="topBarButtons" style="display:"<?php echo $pickupStyle;?>">
                        <div class="fa fa-history"></div>
                    </right>
                    <right id="btnPending" class="topBarButtons" style="display:"<?php echo $unpickupStyle;?>">
                        <div class="fa fa-gift"></div>
                        <span id="lblPending" class="topBarButtons-number"><?php echo $nrUnpickup;?></span>
                    </right>
                    <right id="btnSearch" class="topBarButtons" onclick="">
                        <div class="fa fa-search"></div>
                    </right>
                </right>

    <?php
        if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] && $_SESSION['OnBehalfOfStdent'] && $enableSelectStudent) {
            $studentName = '';      // not need to show student name as it's in option list
            if ($className) {
                $studentList = $libpos->getStudentListByClass($className);
                $studentList = $this->GET_SELECTION_BOX($studentList, 'name="StudentID" id="StudentID"', "-- ".$Lang['Btn']['Select']." --", $studentID);
            }
            else {
                $studentList = '';
            }

            echo '<left style="top: 20px; right:10px; line-height:70px;">'.$this->getClassSelection($className).'</left>';
            echo '<left style="top: 20px; right:10px; line-height:70px;" id="StudentList">'.$studentList.'</left>';
        }
        elseif (($_SESSION['UserType'] == USERTYPE_PARENT) && (count($children)>1)) {
            $studentList = $this->GET_SELECTION_BOX($children, 'name="StudentID" id="StudentID"', "-- ".$Lang['Btn']['Select']." --", $studentID);
            echo '<left style="top: 20px; right:10px; line-height:70px;" id="StudentList">'.$studentList.'</left>';
        }
        else {
            echo '<input type="hidden" id="StudentID" value="'.$_SESSION['ePosStudentID'].'">';
        }
?>
                <left id="lblUsername"><?php echo $studentName;?></left>
                <div id="blkHistory" class="hidden notificationBox">
            <?php
                echo $this->getPickupOrder($pickupDetailAry);
            ?>
                </div>
                <div id="blkPending" class="hidden notificationBox">
            <?php
                echo $this->getUnpickupOrder($unpickupDetailAry);
            ?>
                </div>

                <div id="blkTopSearch" class="hidden notificationBox">
                    <div class="textbox-floatlabel">
                        <input type="text" id="tbxTopSearch">
                        <div class="textboxLabel">
                            <span class="icon fa-search"></span>&nbsp;<span><?php echo $Lang['Btn']['Search'];?></span>
                        </div>
                    </div>
                </div>
            </div>

            <div id="blkTitle">
                <span class="bold"><?php echo $Lang['ePOS']['eClassApp']['Title'];?></span>
            </div>

        </div>
    </form>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getTopBarMenuWithoutFunction()
    {
        global $Lang, $indexVar;

        $libpos = $indexVar['libpos'];

        $studentName = $libpos->getUserToDisplay($_SESSION['ePosStudentID']);

        ob_start();
?>
        <div id="blkTopBar" class="transition">

            <div id="blkUser">
                <left id="lblUsername"><?php echo $studentName;?></left>
            </div>

            <div id="blkTitle">
                <span class="bold"><?php echo $Lang['ePOS']['eClassApp']['Title'];?></span>
            </div>

        </div>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getPickupOrder($pickupDetailAry)
    {
        global $Lang, $intranet_root;
        include_once($intranet_root."/includes/libpos_category.php");
        include_once($intranet_root."/includes/libpos_item.php");

        ob_start();
?>

            <div class="card-header"><?php echo $Lang['ePOS']['eClassApp']['CollectedOrder'];?></div>
            <div class="notificationItems">
    <?php
        foreach((array)$pickupDetailAry as $_logID=>$_pickupDetailAry):
            $_itemID = $_pickupDetailAry['ItemID'];
            $_itemQty = $_pickupDetailAry['ItemQty'];
            $_grandTotal = $_pickupDetailAry['GrandTotal'];
            $_dateInput = $_pickupDetailAry['DateInput'];
            $_displayQty = sprintf($Lang['ePOS']['eClassApp']['NumberOfItems'], $_itemQty);
            $_obj = new POS_Item($_itemID);
    ?>
                <div class="product-item-row" data-TransactionLogID="<?php echo $_logID;?>">
                    <left class="product-item-thumbnail">
                        <img src="<?php echo $_obj->PhotoPath;?>">
                    </left>
                    <right>
                        <div class="bold"><?php echo ($this->CurrencySign).$_grandTotal;?></div>
                    </right>
                    <div>
                        <div>
                            <span><?php echo $_displayQty;?></span>
                        </div>
                        <div class="remark"><?php echo $_dateInput;?></div>
                    </div>
                </div>
    <?php
        endforeach;
    ?>
            </div>
<?
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getUnpickupOrder($unpickupDetailAry)
    {
        global $Lang, $intranet_root;
        include_once($intranet_root."/includes/libpos_category.php");
        include_once($intranet_root."/includes/libpos_item.php");

        ob_start();
?>
        <div class="card-header"><?php echo $Lang['ePOS']['eClassApp']['PendingOrder'];?></div>
        <div class="notificationItems">
            <?php
            foreach((array)$unpickupDetailAry as $_logID=>$_unpickupDetailAry):
                $_itemID = $_unpickupDetailAry['ItemID'];
                $_itemQty = $_unpickupDetailAry['ItemQty'];
                $_grandTotal = $_unpickupDetailAry['GrandTotal'];
                $_dateInput = $_unpickupDetailAry['DateInput'];
                $_displayQty = sprintf($Lang['ePOS']['eClassApp']['NumberOfItems'], $_itemQty);
                $_obj = new POS_Item($_itemID);
                ?>

                <div class="product-item-row" data-TransactionLogID="<?php echo $_logID;?>">
                    <left class="product-item-thumbnail">
                        <img src="<?php echo $_obj->PhotoPath;?>">
                    </left>
                    <right>
                        <div class="bold"><?php echo ($this->CurrencySign).$_grandTotal;?></div>
                    </right>
                    <div>
                        <div>
                            <span><?php echo $_displayQty;?></span>
                        </div>
                        <div class="remark"><?php echo $_dateInput;?></div>
                    </div>
                </div>
            <?php
            endforeach;
            ?>
        </div>
<?
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getAllCategories()
    {
        global $indexVar, $intranet_root;
        include_once($intranet_root."/includes/libpos_category.php");

        $x = '';
        $lcategory = $indexVar['posCategory'];
        $allCategoryAry = $lcategory->Get_All_Category($Keyword='', $ActiveOnly=1);
        if (count($allCategoryAry)) {
            foreach ((array)$allCategoryAry as $_allCategoryAry) {
                $_categoryID = $_allCategoryAry['CategoryID'];
                $_categoryName = $_allCategoryAry['CategoryName'];
                $_obj = new POS_Category($_categoryID);

                $x .= '<a href="#" class="tabScrollable-link" data-CategoryID="'.$_categoryID.'" aria-selected="false">
						    <div class="tabScrollable-thumbnail"><div class="product-item-thumbnail"><img src="'.$_obj->PhotoPath.'" alt=""></div></div>
						    <div>'.$_categoryName.'</div>
					    </a>';
            }
        }
        return $x;
    }

    // show item list with ItemName, Description, Image, UnitPrice and tray for shopping
    function getShowItems($itemAry, $itemInCart=array())
    {
        global $intranet_root, $Lang;
        include_once($intranet_root."/includes/libpos_category.php");
        include_once($intranet_root."/includes/libpos_item.php");
        foreach ((array)$itemAry as $_itemAry) {
            $_itemID = $_itemAry['ItemID'];
            $_itemName = $_itemAry['ItemName'];
            $_unitPrice = $_itemAry['UnitPrice'];
            $_itemCount = $_itemAry['ItemCount'];
            $_description = $_itemAry['Description'];
            $_obj = new POS_Item($_itemID);
            $_class = in_array($_itemID,(array)$itemInCart) ? ' addedToCart' : '';
            if ($_itemCount == 0) {
                $_class .= ' disabled';
                $_cart = $Lang['ePOS']['eClassApp']['OutOfStock'];
            }
            else {
                $_cart = '<span class="product-item-addToCart fa fa-cart-plus" data-ItemID="'.$_itemID.'"></span>';
            }

            $x .= '
                <div class="product-item-block'.$_class.'">
                    <div class="product-item-thumbnail">
                        <img src="'.$_obj->PhotoPath.'" alt="" data-ItemID="'.$_itemID.'">
                    </div>
                    <div class="product-name">'.$_itemName.'</div>
                    <div class="remark">'.$_description.'</div>
                    <right>
                        '.$_cart.'
                    </right>
                    <div class="product-price">'.($this->CurrencySign).$_unitPrice.'</div>
                </div>';
        }
        return $x;
    }
    
    function getCategoryItem($categoryID, $itemInCart=array())
    {
        global $intranet_root;
        include_once($intranet_root."/includes/libpos_category.php");
        include_once($intranet_root."/includes/libpos_item.php");

        $lcategory = new POS_Category($categoryID);
        $categoryItemAry = $lcategory->Get_All_Items($ActiveOnly=1);
        $x = $this->getShowItems($categoryItemAry,$itemInCart);
        return $x;
    }

    // $itemInfo - item details of one product only
    function getItemInShoppingCart($itemInfo)
    {
        global $Lang;

        $itemID = $itemInfo['ItemID'];
        $itemName = $itemInfo['ItemName'];
        $unitPrice = $itemInfo['UnitPrice'];
        $purchaseQty = $itemInfo['PurchaseQty'];
        $remainQty = $itemInfo['RemainQty'];
        if ($remainQty - $purchaseQty <= 0) {
            $addButtonClass = ' disabled';
            $remark = sprintf($Lang['ePOS']['eClassApp']['MaxNumberOfItems'],$remainQty);
            $purchaseQty = $remainQty;
        }
        else {
            $addButtonClass = '';
            $remark = '';
        }
        $deductButtonClass = $purchaseQty == 1 ? 'fa-trash' : 'fa-minus';
        $subTotal = number_format($unitPrice * $purchaseQty,2,'.','');

        $x = '
            <div class="cart-item" id="CartItemID_'.$itemID.'" data-ItemID="'.$itemID.'">
                <div class="product-name">
                    '.$itemName.'
                </div>';

        // note: please don't leave space betwee span tab to suit the alignment
        $x .= '<span>
                    <div class="product-price">'.($this->CurrencySign).$unitPrice.'</div>
                    <div class="remark">'.$remark.'</div>
                </span>';
        $x .= '<span class="cart-item-control">';
			$x .= '<span class="fa '.$deductButtonClass.' cart-item-minus"></span>';
            $x .= '<span class="cart-item-amount">'.$purchaseQty.'</span>';
            $x .= '<span class="fa fa-plus cart-item-plus'.$addButtonClass.'"></span>';
        $x .= '</span>';
        $x .= '<span class="cart-item-price">
				    '.($this->CurrencySign).$subTotal.'
                </span>
            </div>';

        return $x;
    }

    function getShoppingCart($currentShoppingCart=array())
    {
        global $Lang;
        $nrItems = count($currentShoppingCart);
        if ($nrItems) {
            $blkCartPriceDisplay = '';
            $blkBlankCartDisplay = 'none';
        }
        else {
            $blkCartPriceDisplay = 'none';
            $blkBlankCartDisplay = '';
        }

        ob_start();
?>
        <div id="btnTrayToggle">
            <left>
                <span class="fa fa-shopping-cart"></span>
                <span><?php echo $Lang['ePOS']['eClassApp']['SelectedItems'];?></span>
                <span id="lblNoOfSelectedItems1"></span>
            </left>
            <right class="fa fa-chevron-up"></right>
            <div id="lblPrice1">
            </div>
        </div>

        <div id="blkCart">
            <div class="card-header">
				<span id="btnClearCart" style="display:<?php echo $blkCartPriceDisplay;?>">
					<span class="fa fa-trash"></span><span><?php echo $Lang['ePOS']['eClassApp']['CancelThisOrder'];?></span>
				</span>
            </div>

            <!--Cart product item-->
        <?php
            $totalQty = 0;
            $totalAmount = 0;
            for($i=0;$i<$nrItems;$i++) {
                $_thisCart = $currentShoppingCart[$i];
                $totalQty += $_thisCart['PurchaseQty'];
                $totalAmount += $_thisCart['UnitPrice'] * $_thisCart['PurchaseQty'];
                echo $this->getItemInShoppingCart($_thisCart);
            }
            $totalAmount = number_format($totalAmount,2,'.','');
        ?>

        </div>

        <div id="blkCartPrice" style="display:<?php echo $blkCartPriceDisplay;?>">
            <left>
                <span><?php echo $Lang['ePOS']['eClassApp']['TotalQty'];?>:</span><span id="lblNoOfSelectedItems2"><?php echo $totalQty;?></span><span><?php echo $Lang['ePOS']['NumberOfItems'];?></span>
            </left>
            <right id="lblPrice2">
                <?php echo ($this->CurrencySign).$totalAmount;?>
            </right>
            <?php if ($_SESSION['UserType'] == USERTYPE_STUDENT):?>
                <br><br>
                <div class="center textColor1"><?php echo $Lang['ePOS']['eClassApp']['InformParentToCheckout'];?></div>
            <?php else:?>
            <div id="btnCheckout" class="button"><?php echo $Lang['ePOS']['eClassApp']['Checkout'];?></div>
            <?php endif;?>
        </div>

        <div id="blkBlankCart" style="display:<?php echo $blkBlankCartDisplay;?>">
			<span>
				<img src="../web/images/blankCart.png">
				<div><?php echo $Lang['ePOS']['eClassApp']['NoSelectedItem'];?></div>
			</span>
        </div>

<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getConfirmRemoveItemDialog()
    {
        global $Lang;
        $x = '
            <span class="dialog dialog-yesno" id="blkDelItem">
                <div>
                    '.$Lang['ePOS']['eClassApp']['js']['RemoveItem'].'
                </div>
                <div class="dialog-buttons">
                    <span id="btnDelItem" class="button">'.$Lang['Btn']['Remove'].'</span>
                    <span class="closeDialog-button button button-secondary">'.$Lang['Btn']['Cancel'].'</span>
                </div>
            </span>';
        return $x;
    }

    function getConfirmCancelOrderDialog()
    {
        global $Lang;
        $x = '
            <span class="dialog dialog-yesno" id="blkClearCart">
                <div>
                    '.$Lang['ePOS']['eClassApp']['js']['CancelOrder'].'
                </div>
                <div class="dialog-buttons">
                    <span class="button confirm-button">'.$Lang['Btn']['Confirm'].'</span>
                    <span class="closeDialog-button button button-secondary">'.$Lang['Btn']['Back'].'</span>
                </div>
            </span>';
        return $x;
    }

    function getZoomInItemPhotoDialog($itemID='')
    {
        global $intranet_root, $Lang;
        include_once($intranet_root."/includes/libpos_category.php");
        include_once($intranet_root."/includes/libpos_item.php");
        if ($itemID) {
            $obj = new POS_Item($itemID);
            $itemName = $obj->ItemName;
            $description = $obj->Description;
            $photoPath = $obj->OriginalPhotoPath;
            if(!file_exists($intranet_root.$photoPath)) {
                $photoPath = $obj->PhotoPath;
            }
        }
        else {
            $itemName = '';
            $description = '';
            $photoPath = '';
        }

        $x = '
            <span class="dialog dialog-productPhoto">
                <img src="'.$photoPath.'">
                <div class="dialog-productPhoto-content">
                    <span>
                        <div class="dialog-productPhoto-subject">'.$itemName.'</div>
                        <div class="dialog-productPhoto-description">'.$description.'</div>
                        <div class="center">
                            <span class="fa fa-angle-up pointer dialog-productPhoto-expand"></span>
                        </div>
                    </span>
                </div>
                <div class="closeDialog-button">
                    <img src="../web/images/icon-closeDialog.png">
                </div>
            </span>';
        return $x;
    }

    function getSearchResult($keyword, $itemInCart=array())
    {
        global $Lang, $intranet_root;
        include_once($intranet_root."/includes/libpos_category.php");
        include_once($intranet_root."/includes/libpos_item.php");

        $libItem = new POS_Item();
        $searchResult = $libItem->Get_All_Items($keyword,$isAssoCategory=0,$isAssoc=true);
        $itemList = $this->getShowItems($searchResult,$itemInCart);
        $keyword = intranet_htmlspecialchars($keyword);
        ob_start();
?>
        <nav class="page-topBar">
            <right class="page-close-btn"></right>
            <div class="page-topBar-subject">
                <?php echo $Lang['ePOS']['eClassApp']['SearchResult'];?>
            </div>
        </nav>

        <div id="blkSearchBox">
			<span class="textbox-withIcon">
				<input type="text" id="tbxItemSearch" placeholder="<?php echo $keyword;?>">
				<div class="icon fa-search"></div>
			</span>
        </div>

        <div id="blkSearchResult" class="product-item-container">
            <?php echo $itemList; ?>
        </div>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getBeforePayByDeposit($studentID)
    {
        global $Lang, $indexVar, $intranet_root, $sys_custom;
        include_once($intranet_root."/includes/libuser.php");

        $libpos = $indexVar['libpos'];
        $pendingCartSum = $libpos->getPendingCartSum($studentID);
        if (count($pendingCartSum)) {
            $totalQty = $pendingCartSum['TotalQty'];
            $totalPrice = $pendingCartSum['TotalPrice'];
        }
        else {
            $totalQty = 0;
            $totalPrice = 0;
        }

        $userBalance = $libpos->getUserBalance($studentID);

        $lui = new libuser($studentID);
        $userPhoto = $lui->GET_OFFICIAL_PHOTO_BY_USER_ID($studentID);
        if (strstr($userPhoto[1], "images/myaccount_personalinfo/samplephoto.gif"))
        {
            if ($lui->PersonalPhotoLink !="" && is_file($intranet_root.$lui->PersonalPhotoLink))
            {
                $userPhoto[1] = $lui->PersonalPhotoLink;
            }
        }

        $userName = $lui->UserNameLang();
        $userPhoto = count($userPhoto) ? $userPhoto[1] : '';

ob_start();
?>
        <nav class="page-topBar">
            <left class="page-back-btn"></left>
            <div class="page-topBar-subject">
                <?php echo $Lang['ePOS']['eClassApp']['Checkout'];?>
            </div>
        </nav>

        <article class="clearfix">

            <section class="page-section page-order-header">
                <right class="page-order-header-price">
                    <?php echo ($this->CurrencySign).$totalPrice;?>
                </right>
                <div>
                    <span><?php echo $totalQty;?></span>&nbsp;<span><?php echo $Lang['ePOS']['NumberOfItems'];?></span>
                </div>
            </section>

            <section class="page-section card marginB20" id="blkDeposit">
                <div class="card-header"><?php echo $Lang['ePOS']['eClassApp']['PayByFollowingAcct'];?></div>
                <div>
				<span>
					<div class="userBox">
						<img src="<?php echo $userPhoto;?>" alt="">
						<div>
							<?php echo $userName;?>
						</div>
					</div>
					<div id="lblBalance"><?php echo $Lang['ePOS']['eClassApp']['Balance'];?></div>
					<div id="lblToBePaid"><?php echo ($this->CurrencySign).$userBalance;?></div>
				</span>
                </div>
            </section>

<?php if (($userBalance >= $totalPrice) || $sys_custom['ePOS']['AllowNegativeBalance']): ?>
            <section class="page-section page-buttons">
                <div class="button floatR" id="btnPay"><?php echo $Lang['ePOS']['eClassApp']['Pay'];?></div>
                <?php if($sys_custom['ePOS']['PaymentMethod']) { ?>
                    <?php if($_SESSION['fromApp'] == true) { ?>
                    <input type="hidden" id="PaymentMethod" name="PaymentMethod" value="<?=$sys_custom['ePOS']['DefaultPaymentMethod']?>" />
                    <input type="hidden" id="Remark" name="Remark" value="" />
                    <?php } else { ?>
                    <div class="floatR">
                        <input type="text" id="Remark" name="Remark" value="" placeholder="<?=$Lang['General']['Remark']?>"/>
                        <select name="PaymentMethod" id="PaymentMethod">
                            <?php foreach($sys_custom['ePayment']['PaymentMethodItems'] as $k=>$v) {
                                if(isset($sys_custom['ePayment_NotAllowEditPaymentMethodItems']) && is_array($sys_custom['ePayment_NotAllowEditPaymentMethodItems'])) {
                                    if(in_array($k, $sys_custom['ePayment_NotAllowEditPaymentMethodItems'])) {
										continue;
									}
								}
                                ?>
                            <option value="<?=$k?>"><?=$v?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <?php } ?>
                <?php } ?>
            </section>
<?php endif;?>
        </article>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getPayFinishLayout($transactionDetail)
    {
        global $Lang;

        ob_start();
?>
        <article class="clearfix">

            <section class="page-section" id="blkFinishPayment">
                <span class="fa fa-check-circle"></span><span><?php echo $Lang['ePOS']['eClassApp']['PaymentComplete'];?></span>
            </section>

            <section class="page-section marginB20 card center">
			<span class="page-QRCode left">

				<div class="card-header"><?php echo $Lang['ePOS']['RefCode'];?></div>
				<div class="page-QRCode-no"><?php echo $transactionDetail['OrderNumber'];?></div>
<!--				<div class="page-QRCode-img">
					<img src="">
				</div>
-->
			</span>
            </section>

            <section class="page-section page-buttons">
                <div class="button floatR" id="btnHome"><?php echo $Lang['ePOS']['eClassApp']['BackToMain'];?></div>
                <div class="button button-secondary floatR" id="btnOrder" data-TransactionLogID="<?php echo $transactionDetail['TransactionLogID'];?>"><?php echo $Lang['ePOS']['eClassApp']['ViewOrderDetail'];?></div>
            </section>

        </article>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getErrorLayout($error, $getBack=true)
    {
        global $Lang, $PATH_WRT_ROOT;

        if (!$getBack) {
            if ($_SESSION['UserType'] == USERTYPE_STUDENT) {
                $bodyClass = "studentApp";
            } elseif ($_SESSION['UserType'] == USERTYPE_STAFF) {
                $bodyClass = "teacherApp";
            } else {
                $bodyClass = "";  // default parentApp
            }
        }

        ob_start();
?>
    <?php if (!$getBack):?>
        <?php include($PATH_WRT_ROOT."home/eClassApp/common/ePOS/header.php");?>
        <body class="<?php echo $bodyClass;?>">
        <div class="mainBody">
    <?php endif;?>

        <article class="clearfix">

            <section class="page-section" id="blkFinishPayment">
                <span class="fa fa-times-circle"></span><span><?php echo $Lang['General']['Error'];?></span>
            </section>

            <section class="page-section marginB20 card center">
				<div><?php echo $error;?></div>
			</span>
            </section>

            <section class="page-section page-buttons">
        <?php if ($getBack):?>
                <div class="button floatR" id="btnHome"><?php echo $Lang['ePOS']['eClassApp']['BackToMain'];?></div>
        <?php else:?>
            <div class="button floatR" id="btnClose"><a href="javascript: window.close();"><?php echo $Lang['Btn']['Close'];?></a></div>
        <?php endif;?>
            </section>

        </article>

    <?php if (!$getBack):?>
        </div>
        </body>

        <?php echo $this->getPowerByFooter();?>
        <?php echo $this->getTopBarMenuWithoutFunction();?>

    <?php endif;?>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }


    // show order details of a transaction
    function getOrderDetails($transactionLogID)
    {
        global $Lang, $indexVar;
        $libpos = $indexVar['libpos'];

        $nrChangeRecord = 0;
        $nrVoidRecord = 0;
        $lastPickupTime = '';
        $lastPickupTitle = '';
        $downloadReceipt = '';
        $pageTitle = $Lang['ePOS']['eClassApp']['CollectedOrder'];  // default
        $transaction = $libpos->Get_POS_Transaction_Log_Data($transactionLogID);
        if (count($transaction) == 0) {
            $transaction = $libpos->Get_POS_Transaction_Log_Data($transactionLogID,$void='',$allVoid=true);       // get void data
        }
        
        if (count($transaction)) {
            $refCode = $transaction[0]['RefCode'];
            $orderTime = $transaction[0]['DateInput'];

            $totalQty = 0;
            $grandTotal = 0;
            
            $transactionDetailAry = $libpos->Get_POS_Transaction_Log_Data($transactionLogID);
            foreach((array)$transactionDetailAry as $_transactionDetailAry) {
                $totalQty += $_transactionDetailAry['ItemQty'];
                $grandTotal += $_transactionDetailAry['GrandTotal'];
            }
            
            $lastReceiveItem = $libpos->getLastReceiveTime($transactionLogID, $receiveAmount=true);
            if (count($lastReceiveItem)) {
                $lastPickupTime = $lastReceiveItem[0]['ReceiveAmountDateModify'];
            }

            $sql = $libpos->Get_Manage_Pickup_Detail_Sql($transactionLogID,'NotComplete',$ShowCheckbox=false);
            $unPickupItemAry = $libpos->returnResultSet($sql);
            $totalUnPickupQty = 0;
            if (count($unPickupItemAry)) {
                foreach((array)$unPickupItemAry as $_unPickupItemAry) {
                    $totalUnPickupQty += $_unPickupItemAry['ItemQty'];
                }
                $pageTitle = $Lang['ePOS']['eClassApp']['PendingOrder'];        // unpick if there's at least one
            }

            // $totalQty and $grandTotal can't rely on this $pickupItemAry because pick up item can be void later
            $sql = $libpos->Get_Manage_Pickup_Detail_Sql($transactionLogID,'Complete',$ShowCheckbox=false);
            $pickupItemAry = $libpos->returnResultSet($sql);
            $totalPickupQty = 0;
            if (count($pickupItemAry)) {
                foreach((array)$pickupItemAry as $_pickupItemAry) {
                    $totalPickupQty += $_pickupItemAry['ItemQty'];
                }
            }

            $grandTotal = number_format($grandTotal,$libpos->DecimalPlace,'.','');

            if (($totalPickupQty > 0) && ($totalUnPickupQty > 0)) {
                $lastPickupTitle = $Lang['ePOS']['eClassApp']['LastPickupTime'];
            }
            //else if (($totalPickupQty > 0) && ($totalUnPickupQty == 0)) {
            else if ($totalUnPickupQty == 0) {
                $lastPickupTitle = $Lang['ePOS']['eClassApp']['CollectTime'];

                $downloadReceipt = '<section class="page-floatL page-buttons"><form name="form1" id="form1" method="post" action="?task=management.print_receipt">
			            <div class="button button-secondary floatR" id="DownloadReceipt"><span class="icon fa-file-pdf-o"></span>'.$Lang['ePOS']['eClassApp']['DownloadReceipt'].'</div>
			            <input type="hidden" name="TransactionLogID" value="'.$transactionLogID.'">
			            '.csrfTokenHtml(generateCsrfToken()).'
			        </form>
		        </section>';

                // show change items and void items
                $changeItemsAry = $libpos->getReplaceItemsByTransaction($transactionLogID);
                $voidItemsAry = $libpos->getVoidItemsByTransaction($transactionLogID);
                $nrChangeRecord = count($changeItemsAry);
                $nrVoidRecord = count($voidItemsAry);
            }
        }
        else {
            $refCode = '';
            $orderTime = '';
            $pageTitle = $Lang['General']['NoRecordAtThisMoment'];
        }

        ob_start();
?>
    <nav class="page-topBar">
        <right id="orderDetalCloseBtn" class="page-close-btn"></right>
        <div class="page-topBar-subject">
            <?php echo $pageTitle;?>
        </div>
    </nav>

<?php if ($refCode):?>
    <article class="clearfix">

        <section class="page-floatL page-order-header">
            <right class="page-order-header-price">
                <?php echo ($this->CurrencySign).$grandTotal;?>
            </right>
            <div>
                <span><?php echo $totalQty;?></span>&nbsp;<span><?php echo $Lang['ePOS']['NumberOfItems'];?></span>
            </div>
        </section>

        <section class="page-floatR card marginB20">

            <div class="page-QRCode">
                <div class="card-header"><?php echo $Lang['ePOS']['RefCode'];?></div>
                <div class="page-QRCode-no"><?php echo $refCode;?></div>
<?php if (intranet_phpversion_compare('5.4')=='SAME' || intranet_phpversion_compare('5.4')=='LATER'):?>
                <div class="page-QRCode-img">
                   <img src="http://<?php echo $_SERVER['HTTP_HOST'];?>/home/eClassApp/common/ePOS/management/qrcode.php?refCode=<?php echo $refCode;?>" style="width:200px; height:200px;">
                </div>
<?php endif;?>
            </div>

            <div class="card-footer">
                <span><?php echo $Lang['ePOS']['eClassApp']['OrderTime'];?></span>
                <right><?php echo $orderTime;?></right>
            </div>

        <?php if ($lastPickupTime):?>
            <div class="card-footer">
                <span><?php echo $lastPickupTitle;?></span>
                <right><?php echo $lastPickupTime;?></right>
            </div>
        <?php endif;?>

        </section>

<?php if ($totalUnPickupQty):?>
        <section class="page-floatL card marginB20">

            <div class="card-header"><?php echo $Lang['ePOS']['ItemDoNotTake'];?><span class="card-header-figure"><?php echo $totalUnPickupQty;?></span></div>
        <?php
            foreach((array)$unPickupItemAry as $_unPickupItemAry):
                $_itemID = $_unPickupItemAry['ItemID'];
                $_itemName = $_unPickupItemAry['ItemName'];
                $_itemQty = $_unPickupItemAry['ItemQty'];
                $_unitPrice = $_unPickupItemAry['ItemSubTotal'];
                $_grandTotal = $_unPickupItemAry['GrandTotal'];
                $_obj = new POS_Item($_itemID);
        ?>
            <div class="product-item-row">
                <left class="product-item-thumbnail">
                    <img src="<?php echo $_obj->PhotoPath;?>">
                </left>
                <right>
                    <div class="product-item-amount"><?php echo $_itemQty;?></div>
                    <div class="bold"><?php echo ($this->CurrencySign).$_grandTotal;?></div>
                </right>
                <div>
                    <div class="product-name">
                        <?php echo $_itemName;?>
                    </div>
                    <div class="remark"><?php echo ($this->CurrencySign).$_unitPrice;?></div>
                </div>
            </div>
        <?php
            endforeach;
        ?>
        </section>
<?php endif;?>

<?php if ($totalPickupQty):?>
        <section class="page-floatL card marginB20">

            <div class="card-header"><?php echo $Lang['ePOS']['ItemTaken'];?><span class="card-header-figure"><?php echo $totalPickupQty;?></span></div>
        <?php
            foreach((array)$pickupItemAry as $_pickupItemAry):
                $_itemID = $_pickupItemAry['ItemID'];
                $_itemName = $_pickupItemAry['ItemName'];
                $_itemQty = $_pickupItemAry['ItemQty'];
                $_unitPrice = $_pickupItemAry['ItemSubTotal'];
                $_grandTotal = $_pickupItemAry['GrandTotal'];
                $_obj = new POS_Item($_itemID);
        ?>
            <div class="product-item-row">
                <left class="product-item-thumbnail">
                    <img src="<?php echo $_obj->PhotoPath;?>">
                </left>
                <right>
                    <div class="product-item-amount"><?php echo $_itemQty;?></div>
                    <div class="bold"><?php echo ($this->CurrencySign).$_grandTotal;?></div>
                </right>
                <div>
                    <div class="product-name">
                        <?php echo $_itemName;?>
                    </div>
                    <div class="remark"><?php echo ($this->CurrencySign).$_unitPrice;?></div>
                </div>
            </div>
        <?php
            endforeach;
        ?>

        </section>
<?php endif;?>

<?php echo $downloadReceipt;?>

<?php if ($nrChangeRecord > 0 || $nrVoidRecord > 0):?>
        <section class="page-floatL card marginB20">

            <div class="card-header"><?php echo $Lang['ePOS']['eClassApp']['ChangeGoods'];?></div>
        <?php
        if ($nrChangeRecord): 
            foreach((array)$changeItemsAry as $_changeItemAry):
                $_itemID = $_changeItemAry['ItemID'];
                $_itemName = $_changeItemAry['ItemName'];
                $_itemQty = $_changeItemAry['ItemQty'];
                $_unitPrice = $_changeItemAry['UnitPrice'];
                $_grandTotal = $_changeItemAry['GrandTotal'];
                $_replaceTo = $_changeItemAry['ReplaceTo'];
                $_obj = new POS_Item($_itemID);
         ?>
                <div class="product-item-row dim">
                    <left class="product-item-thumbnail">
                        <img src="<?php echo $_obj->PhotoPath;?>">
                    </left>
                    <right>
                        <div class="product-item-amount"><?php echo $_itemQty;?></div>
                        <div class="bold"><?php echo ($this->CurrencySign).$_grandTotal;?></div>
                    </right>
                    <div>
                        <div class="product-name">
                            <?php echo $_itemName;?>
                        </div>
                        <div class="remark"><?php echo ($this->CurrencySign).$_unitPrice;?></div>
                    </div>
                    <div class="product-item-status">
                        <span class="product-item-status-change"><?php echo $Lang['ePOS']['eClassApp']['ChangeTo'];?> <span class="icon fa-exchange"></span></span><span><?php echo $_replaceTo;?></span>
                    </div>
                </div>
        <?php
            endforeach;
        endif;
        ?>

        <?php
        if ($nrVoidRecord):
            foreach((array)$voidItemsAry as $_voidItemAry):
                $_itemID = $_voidItemAry['ItemID'];
                $_itemName = $_voidItemAry['ItemName'];
                $_itemQty = $_voidItemAry['ItemQty'];
                $_unitPrice = $_voidItemAry['UnitPrice'];
                $_grandTotal = $_voidItemAry['GrandTotal'];
                $_obj = new POS_Item($_itemID);
                ?>
                <div class="product-item-row dim">
                    <left class="product-item-thumbnail">
                        <img src="<?php echo $_obj->PhotoPath;?>">
                    </left>
                    <right>
                        <div class="product-item-amount"><?php echo $_itemQty;?></div>
                        <div class="bold"><?php echo ($this->CurrencySign).$_grandTotal;?></div>
                    </right>
                    <div>
                        <div class="product-name">
                            <?php echo $_itemName;?>
                        </div>
                        <div class="remark"><?php echo ($this->CurrencySign).$_unitPrice;?></div>
                    </div>
                    <div class="product-item-status">
                        <span class="product-item-status-return"><?php echo $Lang['ePOS']['eClassApp']['ReturnGoods'];?> <span class="icon fa-mail-reply"></span><span>
                    </div>
                </div>
            <?php
            endforeach;
        endif;
        ?>
            
        </section>
<?php endif;?>

    </article>

<?php
    endif;

        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }       // end getOrderDetails


    // header for pdf
    function getReceiptHeader()
    {
        global $junior_mck, $Lang;
        $school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

        ob_start();
?>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=<?=($junior_mck) ? 'big5-hkscs' : 'utf-8'?>" />
            <title></title>
            <style type="text/css">
                body{color: #000;  font-family:  msjh !important; font-size:0.9em; line-height:0.9em; margin:0; padding:0; -webkit-print-color-adjust: exact; font-stretch: condensed; /*font-size-adjust: 0.5;*/}

                .schoolName, .reportTitle{
                    text-align: center;
                }

                table{
                    width: 180mm;
                    border-collapse: collapse;
                    margin-top: 3mm;
                    overflow: visible;
                }

            </style>
        </head>

        <h3 class="schoolName"><?php echo $school_name; ?></h3>
        <h3 class="reportTitle"><?php echo $Lang['ePOS']['eClassApp']['Receipt']; ?></h3>
<?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function getReceiptDetails($transactionLogID='')
    {
        global $Lang, $indexVar;

        $libpos = $indexVar['libpos'];

        $css_table_content = 'tablebluerow';
        $table_content  = '<style type="text/css" media="print">'."\n";
        $table_content .= 'thead {display: table-header-group;}'."\n";
        $table_content .= '</style>'."\n";

        $Total = 0;
        $Result = $libpos->Get_POS_Transaction_Log_Data($transactionLogID);
        if (count($Result) == 0) {
            $allVoid = true;
            $Result = $libpos->Get_POS_Transaction_Log_Data($transactionLogID,$void='',$allVoid);       // get void data
        }
        else {
            $allVoid = false;
        }
        
        $table_content .= "<table width=95% border=0 cellpadding=0 cellspacing=0 align=center>";
        $table_content .= "<tr>";
        $table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['UserName']." : </b>".$Result[0]['Name']."</td>";
        $table_content .= "</tr>";
        $table_content .= "<tr>";
        $table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['TransactionTime']." : </b>".$Result[0]['DateInput']."</td>";
        $table_content .= "</tr>";
        $table_content .= "<tr>";
        $table_content .= "<td class='$css_text'><B>".$Lang['ePOS']['RefCode']." : </b>".$Result[0]['RefCode']."</td>";
        $table_content .= "</tr>";
        $table_content .= "<tr>";
        $table_content .= "<td class='$css_text'><B>".$Lang['ePayment']['InvoiceNumber']." : </b>".$Result[0]['InvoiceNumber']."</td>";
        $table_content .= "</tr>";
        $table_content .="</table><br>";
        $table_content .= "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
        $table_content .= "<thead>";
        $table_content .= "<tr>";
        $table_content .= "<td class='$css_text'><b>".$Lang['ePOS']['Category']."</b></td>";
        $table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['ItemName']."</b></td>";
        $table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['Quantity']."</b></td>";
        $table_content .= "<td class='$css_text'><b>".$Lang['ePayment']['UnitPrice']."</b></td>";
        $table_content .= "<td class='$css_text' align=right><b>".$Lang['ePayment']['GrandTotal']."</b></td>";
        $table_content .= "</tr>";
        $table_content .= "<tr><td colspan='5'><hr style='border:none; height: 1px;'></td></tr>";
        $table_content .= "</thead>";
        $table_content .= "<tbody>";

        if (!$allVoid) {
            for ($i=0; $i< sizeof($Result);$i++) {
                $css =$i%2==0?$css_table_content:$css_table_content."2";
    
                $table_content .= "<tr>";
                $table_content .= "<td class='$css'>".$Result[$i]['CategoryName']."</td>";
                $table_content .= "<td class='$css'>".$Result[$i]['ItemName']."</td>";
                $table_content .= "<td class='$css'>".$Result[$i]['ItemQty']."</td>";
                $table_content .= "<td class='$css'>".($this->CurrencySign).$Result[$i]['ItemSubTotal']."</td>";
                $table_content .= "<td class='$css' align=right>".($this->CurrencySign).$Result[$i]['GrandTotal']."</td>";
                $table_content .= "</tr>";
                $Total += $Result[$i]['GrandTotal'];
            }
        }
        $Total = number_format($Total,2,'.','');
        $table_content .= "<tr><td colspan='5'><hr style='border:none; height: 1px;'></td></tr>";
        $table_content .= "<tr><td colspan='5' align=right>".($this->CurrencySign).$Total."</td></tr>";
        $table_content .= "</tbody></table><br><br><br>";

        return $table_content;
    }

    function getClassSelection($selected="")
    {
        global $PATH_WRT_ROOT;
        include_once ($PATH_WRT_ROOT . "includes/libclass.php");
        // Class List
        $lc = new libclass();
        $studentClasses = $lc->getSelectClass('name="StudentClass" id="StudentClass"', $selected, 0);
        return $studentClasses;
    }

    function getBeforePayByAlipay($studentID)
    {
        global $Lang, $indexVar;

        $libpos = $indexVar['libpos'];
        $pendingCartSum = $libpos->getPendingCartSum($studentID);
        if (count($pendingCartSum)) {
            $totalQty = $pendingCartSum['TotalQty'];
            $totalPrice = $pendingCartSum['TotalPrice'];
        }
        else {
            $totalQty = 0;
            $totalPrice = 0;
        }

        ob_start();
        ?>
        <nav class="page-topBar">
            <left class="page-back-btn"></left>
            <div class="page-topBar-subject">
                <?php echo $Lang['ePOS']['eClassApp']['Checkout'];?>
            </div>
        </nav>

        <article class="clearfix">

            <section class="page-section page-order-header">
                <right class="page-order-header-price">
                    <?php echo ($this->CurrencySign).$totalPrice;?>
                </right>
                <div>
                    <span><?php echo $totalQty;?></span>&nbsp;<span><?php echo $Lang['ePOS']['NumberOfItems'];?></span>
                </div>
            </section>

            <section class="page-section card marginB20" id="blkAlipay">
                <img src="../web/images/alipay.png" alt="">
                <div class="center">
				<span class="left">
					<?php echo $Lang['ePOS']['eClassApp']['PayByAlipay']['Instruction_1'];?>
					<br>
					<?php echo $Lang['ePOS']['eClassApp']['PayByAlipay']['Instruction_2'];?>
				</span>
                </div>
            </section>

            <section class="page-section page-buttons">
                <div class="button floatR" id="btnPay"><?php echo $Lang['ePOS']['eClassApp']['Pay'];?></div>
            </section>

        </article>
        <?php
        $x = ob_get_contents();
        ob_end_clean();
        return $x;
    }

    function invalidChildForTheParent($redirect="/")
    {
        global $Lang;
        $x = '<script language="javascript">';
            $x .= 'alert("'.$Lang['ePOS']['eClassApp']['error']['UserNotFound'].'");'."\n";
            if ($redirect) {
                $x .= 'window.location="' . $redirect . '";' . "\n";
            }
        $x .= '</script>';
        return $x;
    }

}
?>