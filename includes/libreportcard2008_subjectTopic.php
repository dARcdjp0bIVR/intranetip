<?php
#  Editing by 
/* ***************************************************
 * Modification Log
 * 20201103 Bill    [2020-0915-1830-00164]
 * - modified getSubjectTopic(), saveSubjectTopic(), getSubjectTopicTableHtml(), getCsvHeader(), getCsvHeaderProperty(), to support subject topics with term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * - added parm subjectTopicRelatedTermArr
 * 20160427 Bill	[2016-0330-1524-42164]
 *  - modified UPDATE_MARKSHEET_SUBJECT_TOPIC_SCORE(), to set marksheet score to NA if $updateMarksheet = 1
 * 20160201 Bill
 * 	- modified CONVERT_SUBJECT_TOPIC_SCORE(), to support Pass/Fail display
 * 20160129 Bill
 *  - modified GET_MARKSHEET_SUBJECT_TOPIC_SCORE(), CONVERT_SUBJECT_TOPIC_SCORE(), to return special characters
 * 20151030 Bill
 * - modified UPDATE_MARKSHEET_SUBJECT_TOPIC_SCORE(), to update last modified in RC_MARKSHEET_SCORE
 * 20150817 Bill
 * - added CONVERT_SUBJECT_TOPIC_SCORE(), modified GET_MARKSHEET_SUBJECT_TOPIC_SCORE(), to support mark and grade conversion
 * 20150512 Bill
 * - BIBA Cust
 * - added GET_MARKSHEET_SUBJECT_TOPIC_SCORE(), INSERT_MARKSHEET_SUBJECT_TOPIC_SCORE(), UPDATE_MARKSHEET_SUBJECT_TOPIC_SCORE(), to process marksheet score of subject topics
 */
if (!defined("LIBREPORTCARD_SUBJECT_TOPIC"))         // Preprocessor directives
{
	define("LIBREPORTCARD_SUBJECT_TOPIC", true);
	
	class libreportcard_subjectTopic extends libreportcard {
        var $subjectTopicRelatedTermArr;

		function libreportcard_subjectTopic() {
			$this->libreportcard();

            // [2020-0915-1830-00164]
			$this->subjectTopicRelatedTermArr = array('W', 'S1', 'S2');
		}
		
		function getSubjectTopic($parFormIdAry='', $parSubjectIdAry='', $parSubjectTopicIdAry='', $parTargetTermSeq=0) {
		    global $eReportCard;

			if ($parFormIdAry != '') {
				$conds_formId = " AND YearID IN ('".implode("','", (array)$parFormIdAry)."')";
			}
			if ($parSubjectIdAry != '') {
				$conds_subjectId = " AND SubjectID IN ('".implode("','", (array)$parSubjectIdAry)."')";
			}
			if ($parSubjectTopicIdAry != '') {
				$conds_subjectTopicId = " AND SubjectTopicID IN ('".implode("','", (array)$parSubjectTopicIdAry)."')";
			}
            // [2020-0915-1830-00164]
			if ($parTargetTermSeq !== '' && $parTargetTermSeq > 0) {
                $conds_targetTermSeq = " AND (TargetTermSeq = '0' OR TargetTermSeq = '$parTargetTermSeq') ";
            }
			
			$table = $this->DBName.".RC_SUBJECT_TOPIC";
			$sql = "SELECT 
                        SubjectTopicID, 
                        YearID, 
                        SubjectID, 
                        Code, 
                        NameEn, 
                        NameCh,
                        TargetTermSeq,
                        CASE
                            WHEN TargetTermSeq = 1 THEN 'S1'
                            WHEN TargetTermSeq = 2 THEN 'S2'
                            ELSE '".$eReportCard['WholeYear']."'
                        END as TargetTermName
                    FROM 
                        $table 
                    WHERE 
                        RecordStatus = 1 
                        $conds_formId 
                        $conds_subjectId 
                        $conds_subjectTopicId 
                        $conds_targetTermSeq
                    ORDER BY 
                        DisplayOrder";
			return $this->returnResultSet($sql);
		}
		
		function getLastDisplayOrder($parFormId, $parSubjectId) {
			$table = $this->DBName.".RC_SUBJECT_TOPIC";
			$sql = "Select Max(DisplayOrder) as maxDisplayOrder From $table Where RecordStatus = 1 And YearID = '".$parFormId."' And SubjectID = '".$parSubjectId."'";
			$resultAry = $this->returnResultSet($sql);
			
			return $resultAry[0]['maxDisplayOrder'];
		}
		
		function saveSubjectTopic($parFormId, $parSubjectId, $parSubjectTopicId, $parCode, $parNameEn, $parNameCh, $parTargetTermSeq) {
			$dataAry = array();
			$dataAry['Code'] = $parCode;
			$dataAry['NameEn'] = $parNameEn;
			$dataAry['NameCh'] = $parNameCh;
            // [2020-0915-1830-00164]
            $dataAry['TargetTermSeq'] = $parTargetTermSeq;

			if ($parSubjectTopicId == '')
			{
				$dataAry['SubjectID'] = $parSubjectId;
				$dataAry['YearID'] = $parFormId;
				$dataAry['Level'] = 1;
				$dataAry['DisplayOrder'] = $this->getLastDisplayOrder($parFormId, $parSubjectId) + 1;
				$dataAry['RecordStatus'] = 1;
				$subjectTopicId = $this->insertSubjectTopic($dataAry);
				
				$updateAry = array();
				$updateAry['PreviousLevelTopicID'] = $subjectTopicId;
				$updateAry['ParentTopicID'] = $subjectTopicId;
				$subjectTopicId = $this->updateSubjectTopic($subjectTopicId, $updateAry);
			}
			else
            {
				$subjectTopicId = $this->updateSubjectTopic($parSubjectTopicId, $dataAry);
			}
			
			return $subjectTopicId;
		}
		
		function insertSubjectTopic($dataAry) {
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($dataAry as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
			}
			
			## set others fields
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# InputBy
			$fieldArr[] = 'InputBy';
			$valueArr[] = "'".$_SESSION['UserID']."'";
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			# LastModifiedBy
			$fieldArr[] = 'LastModifiedBy';
			$valueArr[] = "'".$_SESSION['UserID']."'";
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$table = $this->DBName.".RC_SUBJECT_TOPIC";
			$sql = "Insert Into $table ($fieldText) Values ($valueText)";
			$success = $this->db_db_query($sql);
			if ($success) {
				$insertedId = $this->db_insert_id();
				$this->Commit_Trans();
			}
			else {
				$insertedId = -1;
				$this->RollBack_Trans();
			}
			
			return $insertedId;
		}
		
		function updateSubjectTopic($parSubjectTopicId, $dataAry, $updateLastModified=true) {
			if (!is_array($dataAry) || count($dataAry) == 0)
				return false;
			
			# Build field update values string
			$valueFieldAry = array();
			foreach ($dataAry as $field => $value) {
				$valueFieldAry[] = " $field = '".$this->Get_Safe_Sql_Query($value)."' ";
			}
			
			if ($updateLastModified) {
				$valueFieldAry[] = " DateModified = now() ";
				$valueFieldAry[] = " LastModifiedBy = '".$_SESSION['UserID']."' ";
			}
			$valueFieldText .= implode(',', $valueFieldAry);
			
			$table = $this->DBName.".RC_SUBJECT_TOPIC";
			$sql = "Update $table Set $valueFieldText Where SubjectTopicID = '$parSubjectTopicId'";
			$success = $this->db_db_query($sql);
			
			return ($success)? $parSubjectTopicId : -1;
		}
		
		function deleteSubjectTopic($subjectTopicIdAry) {
			$table = $this->DBName.".RC_SUBJECT_TOPIC";
			$sql = "Update $table Set RecordStatus = 0 Where SubjectTopicID In ('".implode("','", (array)$subjectTopicIdAry)."')";
			return $this->db_db_query($sql);
		}
		
		function isCodeValid($parFormId, $parSubjectId, $parCode, $parExcludeSubjectTopicId) {
			if ($parExcludeSubjectTopicId > 0) {
				$conds_excludeSubjectTopic = " And SubjectTopicID != '".$parExcludeSubjectTopicId."' ";
			}
			
			$table = $this->DBName.".RC_SUBJECT_TOPIC";
			$sql = "Select SubjectTopicID From $table Where RecordStatus = 1 And YearID = '".$parFormId."' And SubjectID = '".$parSubjectId."' And Code = '".$this->Get_Safe_Sql_Query($parCode)."' $conds_excludeSubjectTopic";
			$resultAry = $this->returnResultSet($sql);
			
			return (count($resultAry) == 0)? true : false;
		}
		
		function getSubjectTopicTableHtml($parFormId, $parSubjectId) {
			global $eRCTemplateSetting, $PATH_WRT_ROOT, $Lang;
			
			include_once($PATH_WRT_ROOT."includes/libinterface.php");
			$linterface = new interface_html();
			
			$subjectTopicAry = $this->getSubjectTopic($parFormId, $parSubjectId);
			$numOfSubjectTopic = count($subjectTopicAry);

			$x = '';
			$x .= '<table id="DataTable" class="common_table_list_v30">'."\n";
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th style="width:3%;">#</th>'."\n";
						$x .= '<th style="width:20%;">'.$Lang['General']['Code'].'</th>'."\n";
                        // [2020-0915-1830-00164]
                        if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
                            $x .= '<th style="width:50%;">'.$Lang['General']['Name'].'</th>'."\n";
                            $x .= '<th style="width:10%;">'.$Lang['General']['Term'].'</th>'."\n";
                        } else {
                            $x .= '<th style="width:60%;">'.$Lang['General']['Name'].'</th>'."\n";
                        }
						$x .= '<th style="width:5%;">&nbsp;</th>'."\n";
						$x .= '<th style="width:3%;"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,\'subjectTopicIdAry[]\'):setChecked(0,this.form,\'subjectTopicIdAry[]\')" name="checkmaster"></th>'."\n";
					$x .= '<tr>'."\n";
				$x .= '</thead>'."\n";
				
				$x .= '<tbody>'."\n";
					if ($numOfSubjectTopic == 0) {
						$x .= '<tr><td colspan="100%" style="text-align:center;">'.	$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
					}
					else {
						for ($i=0; $i<$numOfSubjectTopic; $i++) {
							$_subjectTopicId = $subjectTopicAry[$i]['SubjectTopicID'];
							$_code = $subjectTopicAry[$i]['Code'];
							$_name = Get_Lang_Selection($subjectTopicAry[$i]['NameCh'], $subjectTopicAry[$i]['NameEn']);
                            $_targetTermName = $subjectTopicAry[$i]['TargetTermName'];
							
							$x .= '<tr id="tr_'.$_subjectTopicId.'">'."\n";
								$x .= '<td><span class="rowNumSpan">'.($i + 1).'</td>'."\n";
								$x .= '<td>'.$_code.'</td>'."\n";
								$x .= '<td>'.$_name.'</td>'."\n";
                                // [2020-0915-1830-00164]
                                if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
                                    $x .= '<td>'.$_targetTermName.'</td>'."\n";
                                }
								$x .= '<td class="Dragable">'."\n";
									$x .= $linterface->GET_LNK_MOVE("#", $Lang['Btn']['Move'])."\n";
								$x .= '</td>'."\n";
								$x .= '<td>'."\n";
									$x .= '<input type="checkbox" class="ScaleChk" name="subjectTopicIdAry[]" value="'.$_subjectTopicId.'">'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						}
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			
			return $x;
		}
		
		function getCsvHeader($parTargetPropertyAry='') {
			global $eRCTemplateSetting, $Lang;
			
			$headerAry = array();
			
			$headerAry['En'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['FormNameEn'];
			$headerAry['En'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectCodeEn'];
			$headerAry['En'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectNameEn'];
			$headerAry['En'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentCodeEn'];
			$headerAry['En'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentNameEn'];
			$headerAry['En'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicCodeEn'];
			$headerAry['En'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameEnEn'];
			$headerAry['En'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameChEn'];
            // [2020-0915-1830-00164]
            if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
                $headerAry['En'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicTermEn'];
            }
			
			$headerAry['Ch'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['FormNameCh'];
			$headerAry['Ch'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectCodeCh'];
			$headerAry['Ch'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectNameCh'];
			$headerAry['Ch'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentCodeCh'];
			$headerAry['Ch'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentNameCh'];
			$headerAry['Ch'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicCodeCh'];
			$headerAry['Ch'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameEnCh'];
			$headerAry['Ch'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameChCh'];
            // [2020-0915-1830-00164]
            if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
                $headerAry['Ch'][] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicTermCh'];
            }
			
			$returnAry = array();
			if ($parTargetPropertyAry != '') {
				if (!is_array($parTargetPropertyAry)) {
					$parTargetPropertyAry = array($parTargetPropertyAry);
				}
					
				$numOfColumn = count($headerAry['En']);
				$columnPropertyAry = $this->getCsvHeaderProperty();
				for ($i=0; $i<$numOfColumn; $i++) {
					if (in_array($columnPropertyAry[$i], $parTargetPropertyAry)) {
						$returnAry['En'][] = $headerAry['En'][$i];
						$returnAry['Ch'][] = $headerAry['Ch'][$i];
					}
				}
			}
			else {
				$returnAry = $headerAry;
			}
			
			return $returnAry;
		}
		
		function getCsvHeaderProperty($forValidation=false) {
		    global $eRCTemplateSetting;

			// 1: Required, 2: Reference, 3: Optional
            // [2020-0915-1830-00164]
            if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
            {
                if ($forValidation) {
                    return array(1, 1, 2, 1, 2, 1, 1, 1, 1);
                }
                else {
                    return array(1, 1, 2, 3, 2, 1, 1, 1, 1);
                }
            }
            else
            {
                if ($forValidation) {
                    return array(1, 1, 2, 1, 2, 1, 1, 1);
                }
                else {
                    return array(1, 1, 2, 3, 2, 1, 1, 1);
                }
            }
		}
		
		function getImportTempData() {
			$table = $this->DBName.".TEMP_SUBJECT_TOPIC_IMPORT";
			$sql = "Select * from $table Where UserID = '".$_SESSION["UserID"]."' ";
			return $this->returnResultSet($sql);
		}
		
		function deleteImportTempData() {
			$successAry = array();
			$table = $this->DBName.".TEMP_SUBJECT_TOPIC_IMPORT";
			
			$sql = "Delete from $table Where UserID = '".$_SESSION["UserID"]."' ";
			$successAry['DeleteTempImportCsvData'] = $this->db_db_query($sql);
			
			return !in_array(false, $successAry);
		}
		
		# Get the marksheet subject topic score for students
		function GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $SubjectTopicIDArr, $ReportColumnID, $ConvertToNaIfZeroWeight=0, $ColumnWeight='', $SkipSubjectGroupCheckingForGetMarkSheet=0, $needGradeConversion=0, $reportSchemeSettings=array(), $NADisplayConversion=false) {
			$PreloadArrKey = 'GET_MARKSHEET_SUBJECT_TOPIC_SCORE';
			$FuncArgArr = get_defined_vars();
			$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    	if ($returnArr !== false) {
	    		return $returnArr;
	    	}
	    	
			global $eRCTemplateSetting, $PATH_WRT_ROOT;
			
			$checkStudnetInSG = false;
			if ($eRCTemplateSetting['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup'] && $SkipSubjectGroupCheckingForGetMarkSheet!=1) {
				$checkStudnetInSG = true;
				$ReportID = $this->returnReportIDByReportColumnID($ReportColumnID);
				$SubjectGroupStudentIDArr = $this->Get_Student_In_Current_Subject_Group_Of_Subject($ReportID, $SubjectID);
			}
			$thisStudentNotInSG = false;
			if ($eRCTemplateSetting['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup']==true && $checkStudnetInSG==true && in_array($StudentID, $SubjectGroupStudentIDArr)==false)
				$thisStudentNotInSG = true;
			
			$IsZeroWeightColumn = false;
			if ($ConvertToNaIfZeroWeight == 1) {
				if ($ColumnWeight == 0)
					$IsZeroWeightColumn = true;
			}
			
			$table = $this->DBName.".RC_MARKSHEET_TOPIC_SCORE";
			$fields  = "MarksheetTopicScoreID, SubjectTopicID, SchemeID, MarkType, MarkRaw, MarkNonNum, LastModifiedUserID";
			$sql  = "SELECT $fields FROM $table WHERE StudentID = '$StudentID' AND SubjectID = '$SubjectID' AND ReportColumnID = '$ReportColumnID'";
			$result = $this->returnArray($sql);
			
			$SubjectTopicIDArr = array_keys(BuildMultiKeyAssoc($SubjectTopicIDArr, array("SubjectTopicID")));
			
			$returnArr = array();
			for($i=0; $i<sizeof($result); $i++) {
				$thisSubjectTopicID = $result[$i]["SubjectTopicID"];
				
				$thisSubjectTopicNotExist = in_array($thisSubjectTopicID, (array)$SubjectTopicIDArr)==false;
				
				if ($thisStudentNotInSG || $IsZeroWeightColumn || $thisSubjectTopicNotExist) {
					$returnArr[$thisSubjectTopicID] = array(
														"MarksheetScoreID" =>  $result[$i]["MarksheetScoreID"],
														"SchemeID" =>  $result[$i]["SchemeID"],
														"MarkType" =>  'SC',
														"MarkRaw" =>  '-1',
														"MarkNonNum" =>  'NA',
														"LastModifiedUserID" =>  $result[$i]["LastModifiedUserID"],
														"IsEstimated" =>  '0',
													);
				}
				else {
					$returnArr[$thisSubjectTopicID] = array(
														"MarksheetScoreID" =>  $result[$i]["MarksheetScoreID"],
														"SchemeID" =>  $result[$i]["SchemeID"],
														"MarkType" =>  $result[$i]["MarkType"],
														"MarkRaw" =>  $result[$i]["MarkRaw"],
														"MarkNonNum" =>  $result[$i]["MarkNonNum"],
														"LastModifiedUserID" =>  $result[$i]["LastModifiedUserID"],
														"IsEstimated" => '0',
													);
					// need grade conversion for report display
					if($needGradeConversion){
						$returnArr[$thisSubjectTopicID]["DisplayContent"] = $this->CONVERT_SUBJECT_TOPIC_SCORE($result[$i], $reportSchemeSettings);
					}
				}
				// Convert N.A. to NA
				if($NADisplayConversion && $returnArr[$thisSubjectTopicID]["DisplayContent"]=="N.A."){
					$returnArr[$thisSubjectTopicID]["DisplayContent"] = "NA";
				}
			}
			$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $returnArr);
			return $returnArr;
		}
		
		function CONVERT_SUBJECT_TOPIC_SCORE($content, $reportSchemeSettings){
			// get grading scheme info
			$SchemeInfo = $this->GET_GRADING_SCHEME_MAIN_INFO($content["SchemeID"]);
			$GradingInfo = $this->GET_GRADING_SCHEME_RANGE_INFO($content["SchemeID"]);
			
			$display = "";
			
			// Special Case
			if($content["MarkType"]=="SC"){
				$display = $content["MarkNonNum"];
			}
			// Mark Input -> Mark Display
			else if(($reportSchemeSettings["scaleInput"]=="M" && $reportSchemeSettings["scaleDisplay"]=="M") || ($reportSchemeSettings["ScaleInput"]=="M" && $reportSchemeSettings["ScaleDisplay"]=="M")){
				$display = $content["MarkRaw"];
			}
			// Mark Input -> Grade Display
			else if(($reportSchemeSettings["scaleInput"]=="M" && $reportSchemeSettings["scaleDisplay"]=="G") || ($reportSchemeSettings["ScaleInput"]=="M" && $reportSchemeSettings["ScaleDisplay"]=="G")){
				$cur_Mark = $content["MarkRaw"];
				
				// Grade conversion
				$isStart = 1;
				for($i=0 ; $i<count($GradingInfo) ; $i++){
					$j = $i - 1;
					if($GradingInfo[$i]['LowerLimit'] != ""){
						if( ($isStart == 1 && floatcmp((float)$cur_Mark, ">=", (float)$GradingInfo[$i]['LowerLimit']) && floatcmp((float)$cur_Mark, "<=", (float)$SchemeInfo['FullMark'])) || 
							($isStart == 0 && floatcmp((float)$cur_Mark, ">=", (float)$GradingInfo[$i]['LowerLimit']) && floatcmp((float)$cur_Mark, "<", (float)$GradingInfo[$j]['LowerLimit'])) ){
								if($isStart == 1) $isStart = 0;
								$display = $GradingInfo[$i]["Grade"];
						}
					}
				}
			}
			// Grade Input -> Grade Display
			else if(($reportSchemeSettings["scaleInput"]=="G" && $reportSchemeSettings["scaleDisplay"]=="G") || ($reportSchemeSettings["ScaleInput"]=="G" && $reportSchemeSettings["ScaleDisplay"]=="G")){
				$GradingInfo = BuildMultiKeyAssoc($GradingInfo, array("GradingSchemeRangeID"));
				$display = $GradingInfo[$content["MarkNonNum"]]["Grade"];
			}
			// Pass / Fail Case
			else if ($content["MarkType"]=="PF"){
				$PFType = $content["MarkNonNum"];
				// Pass
				if($PFType=="P") {
					$display = $SchemeInfo["Pass"];
				}
				// Fail
				else if($PFType=="F"){
					$display = $SchemeInfo["Fail"];
				}
			}
			
			return $display;
		}
		
		# INSERT marksheet subject topic scores without raw mark record
		function INSERT_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $SubjectTopicIDArr, $ReportColumnID, $LastModifiedUserID="") {
			if ($LastModifiedUserID == "")
				$LastModifiedUserID = $this->uid;
			
			$existSubjectTopicID = $this->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($StudentID, $SubjectID, $SubjectTopicIDArr, $ReportColumnID);	
			$existSubjectTopicID = array_keys((array)$existSubjectTopicID);
			if(is_array($existSubjectTopicID) && count($existSubjectTopicID)>0){
				$SubjectTopicIDArr = array_diff($SubjectTopicIDArr, (array)$existSubjectTopicID);
				sort($SubjectTopicIDArr);
			}
			
			if (sizeof($SubjectTopicIDArr) > 0) {
				$table = $this->DBName.".RC_MARKSHEET_TOPIC_SCORE";
				$fields = "StudentID, SubjectID, SubjectTopicID, ReportColumnID, LastModifiedUserID, DateInput, DateModified";
				$sql = "INSERT INTO $table ($fields) VALUES ";
				for($i=0; $i<sizeof($SubjectTopicIDArr); $i++) {
					$entries[] = "('$StudentID', '$SubjectID', '".$SubjectTopicIDArr[$i]."', '$ReportColumnID', '$LastModifiedUserID', NOW(), NOW())";
				}
				$sql .= implode(",", $entries);
				$success = $this->db_db_query($sql);
				return $success;
			} else {
				return true;
			}
		}
		
		# UPDATE marksheet scores
		function UPDATE_MARKSHEET_SUBJECT_TOPIC_SCORE($MarksheetScoreArr, $updateMarksheet=0) {
			$table = $this->DBName.".RC_MARKSHEET_TOPIC_SCORE";
			
			$sqlUpdate = "UPDATE $table SET ";
			for($i=0; $i<sizeof($MarksheetScoreArr); $i++) {
				$sql = "SchemeID = '".$MarksheetScoreArr[$i]["SchemeID"]."', ";
				$sql .= "MarkType = '".$MarksheetScoreArr[$i]["MarkType"]."', ";
				$sql .= "MarkRaw = '".$MarksheetScoreArr[$i]["MarkRaw"]."', ";
				$sql .= "MarkNonNum = '".$MarksheetScoreArr[$i]["MarkNonNum"]."', ";
				$sql .= "LastModifiedUserID = '".$MarksheetScoreArr[$i]["LastModifiedUserID"]."', ";
				$sql .= "DateModified = NOW() ";
				$sql .= "WHERE ";
				if (isset($MarksheetScoreArr[$i]["MarksheetScoreID"]) &&  $MarksheetScoreArr[$i]["MarksheetScoreID"] != "") {
					$sql .= "MarksheetScoreID = '".$MarksheetScoreArr[$i]["MarksheetScoreID"]."'";
				} else {
					$sql .= "StudentID = '".$MarksheetScoreArr[$i]["StudentID"]."' AND ";
					$sql .= "SubjectID = '".$MarksheetScoreArr[$i]["SubjectID"]."' AND ";
					$sql .= "SubjectTopicID = '".$MarksheetScoreArr[$i]["SubjectTopicID"]."' AND ";
					$sql .= "ReportColumnID = '".$MarksheetScoreArr[$i]["ReportColumnID"]."'";
				}
				$sql = $sqlUpdate.$sql;
				
				$success[] = $this->db_db_query($sql);
			}
			
			if($updateMarksheet){
				$table = $this->DBName.".RC_MARKSHEET_SCORE";
			
				$sqlUpdate = "UPDATE $table SET ";
				for($i=0; $i<sizeof($MarksheetScoreArr); $i++) {
					// [2016-0330-1524-42164] update marksheet score to NA
					$sql  = "MarkType = 'SC', ";
					$sql .= "MarkNonNum = 'N.A.', ";
					$sql .= "LastModifiedUserID = '".$MarksheetScoreArr[$i]["LastModifiedUserID"]."', ";
					$sql .= "DateModified = NOW() ";
					$sql .= "WHERE ";
					$sql .= "StudentID = '".$MarksheetScoreArr[$i]["StudentID"]."' AND ";
					$sql .= "SubjectID = '".$MarksheetScoreArr[$i]["SubjectID"]."' AND ";
					$sql .= "ReportColumnID = '".$MarksheetScoreArr[$i]["ReportColumnID"]."'";
					$sql = $sqlUpdate.$sql;
					
					$success[] = $this->db_db_query($sql);
				}
			}
			return !in_array(false, $success);
		}
	} // end of class libreportcard_subjectTopic
} // end of Preprocessor directives
?>