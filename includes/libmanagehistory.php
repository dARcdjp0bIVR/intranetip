<?php

// Modifing by yat 

###############################################################
#
#	Date: 	2017-08-02 Cameron
#			add parameter $includeLeftStudent to getStudentListByClass()
#
#	Date:	2011-12-19	YatWoon
#			Fixed: update getClassListByYear(), cater year format with "yyyy - yyyy" [Case#2011-1216-1536-55073]
#
###############################################################

class libmanagehistory extends libstudentpromotion {
	/**
	 * Constructor
	 **/
	function libmanagehistory (){
		$this->libstudentpromotion();
    }    
	
    /**
     * Get list of years existing in table PROFILE_CLASS_HISTORY
     *
     * Year Format - '20xx'
     *     
     * @return array $aryYearList
     */
	function getYearList($formatYear=true){
		$aryYearList = array();
		$sql = "SELECT distinct h.AcademicYear FROM INTRANET_USER as u INNER JOIN PROFILE_CLASS_HISTORY as h ON ".
			   "u.UserID = h.UserID WHERE u.RecordType = 2 AND u.RecordStatus IN (0,1,2)  ORDER BY h.AcademicYear ";
			   
		$result = $this->returnArray($sql);
		
		foreach($result as $key=>$value){
			$aryYear = explode("-", $value[0]);
			$aryYear[0] = trim($aryYear[0]);
			if(!empty($aryYear) && !in_array($aryYear[0], $aryYearList))
				$aryYearList[] = $aryYear[0];
		}			
		return $aryYearList;		
	}
	
	/**
	 * Get Class details (classID, classname) according to year
	 *
	 * @param int $year
	 * @param stirng $class
	 * @param boolean $doCount
	 * @return array ( array $result, int $totalStudent)
	 */
	function getClassListByYear($year,$class='', $doCount=true){
		$totalStudent = 0;		
		
		$nxtYr = $this->do_get_nxtYr($year);		
		
		$academicYear_type2 = $this->do_calc_academic_nxt_yr($year, $nxtYr);
			
		$fieldNames = "class.ClassId,history.ClassName";
		
		if($doCount) $fieldNames .= ", count(distinct history.UserID)";
			
		$sql = "SELECT $fieldNames FROM ".
			   "INTRANET_CLASS as class RIGHT JOIN PROFILE_CLASS_HISTORY as history ".
			   "ON class.ClassName = history.ClassName INNER JOIN INTRANET_USER as user ON ".
			   "history.UserID = user.UserID ".
			   "WHERE ".			   
			   "( history.AcademicYear LIKE '".$year."-%' OR ".
			   "history.AcademicYear in ('".$year."','".$year." - ".$nxtYr."', '".$year."-".$nxtYr."' ".$academicYear_type2.") ) AND ".
			   "user.RecordType = 2 AND user.RecordStatus IN (0,1,2) ".
			   "GROUP BY history.ClassName ".
			   "ORDER BY  class.ClassName, class.ClassLevelID";
			
		$result = $this->returnArray($sql);
		foreach($result as $key=>$value){
			$totalStudent += $value[2];
		}
	
		return array($result, $totalStudent);
	}
	
	
	/**
	 * Get list of student according to class
	 *
	 * @param int $year
	 * @param string $class
	 * @return array $aryStudentList
	 */
	function getStudentListByClass($year, $class, $includeLeftStudent=false){
		$aryStudentList = array();
		$aryFound		= array();
		$formatData 	= "";		
		$defaultColor 	= "#FF0000";
		
		$nxtYr = $this->do_get_nxtYr($year);
		$academicYear_type2 = $this->do_calc_academic_nxt_yr($year, $nxtYr);
		$extraStatus = $includeLeftStudent ? ",3" : "";
		
		$name_field = getNameFieldByLang();
		$sql = "SELECT history.UserID, $name_field, history.ClassName, history.ClassNumber, history.RecordID, history.AcademicYear FROM INTRANET_USER as user RIGHT JOIN PROFILE_CLASS_HISTORY as history ".
			   "ON user.UserId = history.UserId WHERE ".
			   "( history.AcademicYear LIKE '".$year."-%' OR ".
			   "history.AcademicYear in ('".$year."','".$year."-".$nxtYr."', '".$year." - ".$nxtYr."' ".$academicYear_type2.")) AND ".
			   "history.ClassName = '".$class."' AND ".
			   "user.RecordType = 2 AND user.RecordStatus IN (0,1,2{$extraStatus}) ".		
			   "ORDER BY history.ClassNumber+0,  history.AcademicYear";
		
		$result = $this->returnArray($sql);
		
		foreach($result as $key=>$value){			
			$foundYear = array();
			$sameYear  = array();
			
			if(!in_array($value['UserID'], $aryFound)){
			
				
				$data = $this->returnStudentClassHistory($value['UserID']);			
				
				$sql = "SELECT DISTINCT AcademicYear, ClassName, ClassNumber, RecordId FROM PROFILE_CLASS_HISTORY as h WHERE ".
		                "UserID = ".$value[0]."
		                ORDER BY AcademicYear, ClassName, ClassNumber";		                
		                
				$data = $this->returnArray($sql, 3);
				
				$formatData = "<table cellpadding='0' cellspacing='0'>";
				
				foreach($data as $key2=>$value2){
					$aryTempYear = explode("-", $value2['AcademicYear']);
					$currColor = "";	
					
					if($value2['ClassName'] == $class){
						if(!in_array( $aryTempYear[0], $foundYear) && !in_array($aryTempYear[0], $sameYear)){
							array_push($foundYear, $aryTempYear[0]);
							array_push($sameYear, $aryTempYear[0]);
						}else{
							$currColor = $defaultColor;
						}
					}else{
						if(!in_array( $aryTempYear[0], $foundYear) && $aryTempYear[0] != $year)
							array_push($foundYear, $aryTempYear[0]);							
						else
							$currColor = $defaultColor;
					}
					
					$formatData .= "<tr><td><div style='float:left;width:100px;'><font color='".$currColor."'>".$value2['AcademicYear']."</font></div><div style='float:left;'><font color='".$currColor."'> ".$value2['ClassName']."-".str_pad($value2['ClassNumber'], 2, '0', STR_PAD_LEFT)."</font></div></td></tr>";
				}			
				$formatData .= "</table>";
				$aryStudentList[] = array('recordId'=>$value['RecordID'], 'id'=>$value['UserID'],'name'=>$value[1], 'classnumber'=>$value['ClassNumber'],'history'=>$formatData);
				$formatData = "";
				
				array_push($aryFound, $value['UserID']);
			}
		}
		
		
		
		return $aryStudentList;
	}
	
	/**
	 * Get RecordID according to Class
	 * 
	 * @param string $year
	 * @param string $class
	 * @return array $result
	 */
	function getRecordIdListByClassId($year, $class){
		$aryStudentList = array();
		$formatData = "";
		
		$nxtYr = $this->do_get_nxtYr($year);
		$academicYear_type2 = $this->do_calc_academic_nxt_yr($year, $nxtYr);
		
		$name_field = getNameFieldByLang();
		$sql = "SELECT h.RecordID FROM  INTRANET_USER as u RIGHT JOIN  PROFILE_CLASS_HISTORY as h ON ".
			   "u.UserID = h.UserID INNER JOIN INTRANET_CLASS as c ON ".
			   "h.ClassName = c.ClassName ".
			   "WHERE ( h.AcademicYear in ('".$year."','".$year."-".$nxtYr."','".$year." - ".$nxtYr."' ".$academicYear_type2.") OR h.AcademicYear like '".$year."-%') AND ".
			   "c.ClassId = ".$class." AND u.RecordType = 2 AND u.RecordStatus IN (0,1,2)";
			   
		$result = $this->returnVector($sql);
			
		return $result;
	}


	/**
	 * Update history detail according to RecordID
	 *
	 * @param int $rid
	 * @param string $year
	 * @param string $class
	 * @param string $classnum
	 * @return boolean
	 */
	function updateStudentHistoryByRecord($rid, $year, $class, $classnum){			     		
		$sql = "UPDATE PROFILE_CLASS_HISTORY SET AcademicYear = '$year', ClassName = '$class', ClassNumber ='$classnum', DateModified = '".date('Y-m-d h:m:s')."' WHERE RecordID = $rid";		
		$this->db_db_query($sql);
		return true;
	}
		
	
	/**
	 * Build Classs name <Select> 
	 * 
	 * @param string $extra
	 * @param string $class
	 * @return String $x
	 */
	function getSelectLevelByClass($extra, $class){
		
		$x = "";
		$sql = "SELECT l.ClassLevelID FROM INTRANET_CLASSLEVEL as l INNER JOIN INTRANET_CLASS as c ON ".
			   "l.ClassLevelID = c.ClassLevelID WHERE ".
			   "c.ClassName = '$class' LIMIT 1";
			  			  
		$result = $this->returnArray($sql,2);
	
		$aryLvlList = $this->getLevelList();
	
		$x .= "<SELECT $extra >";
		foreach($aryLvlList as $key=>$value){				
			$x .= "<option value='$key' ";
			if($result[0][0] == $key){
				$x .= "SELECTED";
			}
			$x .= ">$value</option>";
		}		
		$x .= "</SELECT>";
		
		return $x;
	}
	
	
	/** 
	 * To retrieve list of recordID that is duplicated 
	 *
	 * @param int $UserID
	 * @param string $AcademicYear
	 * @param string $ClassName
	 * @param string $ClassNumber
	 * @return String $strRecord
	 */
	function getDuplicate($UserID, $AcademicYear, $ClassName="", $ClassNumber=""){
		$aryFoundYear = array();		
		$strRecord = "";
		
		$sql = "
				SELECT 
					RecordID, AcademicYear 
				FROM 
					PROFILE_CLASS_HISTORY 				
				WHERE					
					UserID = $UserID AND
					AcademicYear = '$AcademicYear' ";
					
		if(!empty($ClassName) && !empty($ClassNumber)){
		$sql .= "	AND
					ClassName = '$ClassName' AND 
					LPAD(ClassNumber, 2,'0') = LPAD($ClassNumber, 2,'0')
				";				
		}
				
		
		$aryData = $this->returnArray($sql);
		
		foreach($aryData as $kye=>$data){
			if(!in_array($data['AcademicYear'], $aryFoundYear)){
				array_push($aryFoundYear, $data['AcademicYear']);
			}else{
				(!empty($strRecord))? $strRecord .= ", ".$data['RecordID']:$strRecord = $data['RecordID'];				
			}
		}		
		
		return $strRecord;
	}	
	
	/**
	 * Get list of records that are duplicated
	 	 
	 *
	 * Duplicate fields - UserID, AcademicYear , ClassName, ClassNumber
	 *
	 * NOTE: if calling this function to display data, cannot use with libdbtable to display content
	 *
	 * @param string $search
	 * @return array $aryData
	 */
	function getDuplicateList($search=""){
		$name_field = getNameFieldByLang();
		
		$sql = "
				SELECT 
					 $name_field, h.AcademicYear, h.ClassName, h.ClassNumber, (COUNT(*)-1), CONCAT('<input type=checkbox name=chk[] value=', h.RecordID ,'>'),  h.RecordID, h.UserID, COUNT(*) as rowCount, 
					 LPAD( h.ClassNumber, 2,'0') as cn					 
				FROM 
					PROFILE_CLASS_HISTORY as h LEFT JOIN INTRANET_USER as u 
				ON 
					h.UserID = u.UserID 
				WHERE
					u.RecordType = 2 AND u.RecordStatus IN (0,1,2) 						
				";
				
		if(!empty($search)){
			$sql .= "AND (h.AcademicYear like '%".$search."%' OR u.ChineseName like '%".$search."%' OR u.EnglishName like '%".$search."%' OR h.ClassName like '%".$search."%') "; 
		}	
		
		$sql .=" group by h.UserID, h.AcademicYear, h.ClassName, cn having COUNT(*) > 1 ";			
		$aryData = $this->returnArray($sql);
		
		return $aryData;
	}
	
	
	/** 
	 * Get list of records that are problematic
	 *
	 * Problematic - duplicated ~ UserID , AcademicYear 
	 *
	 * @param string $search
	 * @return String $sql 
	 */
	function getProblemData($search=""){
		$name_field = getNameFieldByLang();
	
		$sql = "
				SELECT 
					 $name_field, h.AcademicYear,COUNT(*), CONCAT('<input type=checkbox name=chk[] value=', h.RecordID ,'>'),  h.RecordID, h.UserID, COUNT(*) as rowCount
				FROM 
					PROFILE_CLASS_HISTORY as h LEFT JOIN INTRANET_USER as u 
				ON 
					h.UserID = u.UserID 
				WHERE
					u.RecordType = 2 AND u.RecordStatus IN (0,1,2) 						
				";
				
		if(!empty($search)){
			$sql .= "AND (h.AcademicYear like '%".$search."%' OR u.ChineseName like '%".$search."%' OR u.EnglishName like '%".$search."%' ) "; 	
		}	

		$sql .=" group by h.UserID, h.AcademicYear having COUNT(*) > 1 ";		
	
		return $sql;
	}
	
	
	/**
	 * Get ALL users with problematic/duplicate Data
	 *
	 * @return Array 
	 */
	function getGenerateProblemList(){
		return $this->returnArray($this->getProblemData());		
	}
	
	
	/**
	 * Get UserID using RecordID
	 * 
	 * @param int $UserID
	 * @return int $row[0] (UserID)
	 */
	function getUserIDByRecordId($recordID){
		$sql = "SELECT UserID FROM PROFILE_CLASS_HISTORY WHERE RecordID = '$recordID' LIMIT 1";
		$row = $this->returnVector($sql);
		return $row[0];
	}
	
	
	/**
	 * Get RecordID using Year and/or UserID
	 *
	 * @param int $year
	 * @param int $UserID
	 * @return Array $row (1D array of RecordID)
	 */
	function getRecordIdByYearAndUserID($year , $UserID){
		
		$nxtYr = $this->do_get_nxtYr($year);
		
		$sql = "SELECT RecordID FROM PROFILE_CLASS_HISTORY WHERE AcademicYear in ('$year', '$year-$nxtYr', '$year - $nxtYr') OR AcademicYear like '$year-%' ";		
		if($UserID != false)
			$sql .= "AND UserID = '$UserID'";

		$row = $this->returnVector($sql);
		return $row;
	}
	
	
	function returnClassHistoryByRecord($recordID){
		global $eclass_db;
		
		$sql = "SELECT AcademicYear, ClassName, ClassNumber, RecordId FROM PROFILE_CLASS_HISTORY
		           WHERE RecordID = '$recordID' LIMIT 1";
		           
		return $this->returnArray($sql, 3);
	}
	
	/**
	 * Remove a history record
	 *
	 * @param string $strRecordID
	 * @return boolean
	 */
	function doRemoveHistoryRecord($strRecordID){
		$sql = "DELETE FROM PROFILE_CLASS_HISTORY WHERE RecordID in ($strRecordID) ";		
		$this->db_db_query($sql);
		return true;		
	}
	
	
	/**
	 * Remove N history records of a User not in list provided
	 *
	 * Usage : Manage history-->Edit page
	 *
	 * @param Array $aryRecord
	 * @param int $UserID
	 * @return boolean
	 */	
	function doRemoveHistoryRecordNotInList($aryRecord, $UserID){
		
		(is_array($aryRecord))? $strRecord = implode(',', $aryRecord): $strRecord = $aryRecord;
		
		$nxtYr = $this->do_get_nxtYr($year);
		$sql = "DELETE FROM PROFILE_CLASS_HISTORY WHERE RecordID NOT IN ($strRecord) AND UserID='$UserID'";				
		$this->db_db_query($sql);
		
		return true;		
	}
	
	/**
	 * Get list acedemic years according to UserID 
	 *
	 * @param int $UserID
	 * @return Array $rs [RecordID, AcademicYear]
	 */
	function doGetAcademicYearByUser($UserID){
		$sql = "SELECT RecordID, AcademicYear FROM PROFILE_CLASS_HISTORY WHERE UserID = '$UserID'";
		$rs = $this->returnArray($sql);
		return $rs;
	}
	
	/**
	 * Get specific history record
	 *
	 * @param int $RecordID
	 * @return Array
	 */
	function doGetHistoryByRecordID($RecordID){
		$sql = "SELECT * FROM PROFILE_CLASS_HISTORY WHERE RecordID = '$RecordID' LIMIT 1";
		$rs = $this->returnArray($sql);
		return $rs[0];
	}
	
	/**
	 * Get student history
	 *
	 * Check whether record is a Duplicate or not
	 *
	 * @param int $studentid
	 * @param string $currClass
	 * @return Array $aryData
	 */	
	function getStudentClassHistory($studentid, $currClass){
		global $eclass_db;
		$foundYear = array();
		$sameYear = array();
		$isDuplicate = "NO";
		
		$sql = "
				SELECT 
					DISTINCT AcademicYear, ClassName, ClassNumber, RecordId 
				FROM 
					PROFILE_CLASS_HISTORY
		       	WHERE 
		       		UserID = '$studentid' 
		       	ORDER BY 
		       		AcademicYear";
		       		
		$aryData = $this->returnArray($sql, 3);
	
		foreach($aryData as $key=>$value){
			$aryTempYear = explode("-", $value['AcademicYear']);
			$isDuplicate = "NO";	
			
			if($value['ClassName'] == $currClass){
				if(!in_array( $aryTempYear[0], $foundYear) && !in_array($aryTempYear[0], $sameYear)){
					array_push($foundYear, $aryTempYear[0]);
					array_push($sameYear, $aryTempYear[0]);
				}else{
					$isDuplicate = "YES";
				}
			}else{
				if(!in_array( $aryTempYear[0], $foundYear) && $aryTempYear[0] != $year)
					array_push($foundYear, $aryTempYear[0]);							
				else
					$isDuplicate = "YES";
			}
			
			$value['isDuplicate'] = $isDuplicate;
			$aryData[$key] = $value;
		}		
		
		return $aryData;
	}
	
	function do_calc_academic_nxt_yr($year="", $nxtYr=""){
		$academicYear_type2 = "";
		$tmp_nxtYr = "";
		$tmp_optYr = "";
		
		$nxtYr_len = strlen($nxtYr);
		$optYr_len = strlen($year);
		
		if($nxtYr_len >= 4){
			$tmp_nxtYr .= substr($nxtYr,2,2);
			$academicYear_type2 = ", '".$year."-".$tmp_nxtYr."'";
		}
		
		if($optYr_len >= 4){
			$tmp_optYr .= substr($year,2,2);
			$academicYear_type2 .= ", '".$tmp_optYr."-".$nxtYr."'";
		}
		
		if(!empty($tmp_optYr) && !empty($tmp_nxtYr)){
			$academicYear_type2 .= ", '".$tmp_optYr."-".$tmp_nxtYr."'";
		}
		return $academicYear_type2;
	}

	function do_get_nxtYr($year){
		$yr_len = strlen($year);
		return ($yr_len >=4)?$year+1: str_pad($year+1, 2, "0", STR_PAD_LEFT);
		
	}
	
	function doRemoveHistoryRecordInList($strDeleteRecordID=""){
		$sql = "DELETE FROM PROFILE_CLASS_HISTORY WHERE RecordID IN ($strDeleteRecordID)";
		$this->db_db_query($sql);
	}
	
}