<?php 
#using: yat
/*************************************************************
 *	20111207 YatWoon:
 *		Update getFolderList(), add onTop ordering (display "onTop" folder first [Case#2011-0830-1331-46071])
 *
 *  20101101 Marcus:
 * 		Add Batch Logic to save query
 * ************************************************************/

class libfolder extends libdb{

	var $FolderID;
	var $GroupID;
	var $FolderType;
	var $UserID;
	var $UserName;
	var $Title;
	var $PublicStatus;
	var $RecordType;
	var $RecordStatus;
	var $DateInput;
	var $onTop;
	var $PhotoResize; // Added by Thomas on 2010-08-04: indicate the photo in this album need resize or not
	var $DemensionX;  // Added by Thomas on 2010-08-04: Max width of the upload photo
	var $DemensionY;  // Added by Thomas on 2010-08-04: Max height of the upload photo

	# ------------------------------------------------------------------------------------

	function libfolder($FolderID=0, $FolderIDArr=''){
		$this->libdb();
		$row = $this->returnFolder($FolderID,$FolderIDArr);
		
		$this->AssignToBatch($row);
		
		$loadFolderID = $FolderID?$FolderID:$row[0][0];
		$this->LoadFolderData($loadFolderID);
//		$this->FolderID = $row[0][0];
//		$this->GroupID = $row[0][1];
//        $this->FolderType = $row[0][2];
//		$this->UserID = $row[0][3];
//		$this->UserName = $row[0][4];
//		$this->Title = $row[0][5];
//		$this->PublicStatus = $row[0][6];
//		$this->RecordType = $row[0][7];
//		$this->RecordStatus = $row[0][8];
//		$this->DateInput = $row[0][9];
//		$this->onTop = $row[0][10];
//		$this->PhotoResize = $row[0][11]; // Added by Thomas on 2010-08-04
//		$this->DemensionX = $row[0][12];  // Added by Thomas on 2010-08-04
//		$this->DemensionY = $row[0][13];  // Added by Thomas on 2010-08-04
	}

	function returnFolder($FolderID,$FolderIDArr=''){
		
		if(empty($FolderID) && empty($FolderIDArr))
			return false;
		
  		$sql = "SELECT FolderID, GroupID, FolderType, UserID, UserName, Title, PublicStatus, RecordType, RecordStatus, DateInput, onTop, PhotoResize, DemensionX, DemensionY FROM INTRANET_FILE_FOLDER WHERE ";
  		if(is_array($FolderIDArr) && count($FolderIDArr)>0)
			$sql .= " FolderID IN ('".implode("','",$FolderIDArr)."') ";
		else
		    $sql .= " FolderID = '".IntegerSafe($FolderID)."' ";
		
		return $this->returnArray($sql,14);
	}

	function AssignToBatch($FolderDataArr)
	{
		foreach((array)$FolderDataArr as $FolderData)
			$this->BatchFolderArr[$FolderData['FolderID']] =  $FolderData;
	}
	
	function LoadFolderData($FolderID)
	{
		if(empty($this->BatchFolderArr[$FolderID]))
		{
			$result = $this->returnFolder($FolderID);
			$this->AssignToBatch($result);
		}
		
		$FolderData = $this->BatchFolderArr[$FolderID];

		$this->FolderID = $FolderData[0];
		$this->GroupID = $FolderData[1];
        $this->FolderType = $FolderData[2];
		$this->UserID = $FolderData[3];
		$this->UserName = $FolderData[4];
		$this->Title = $FolderData[5];
		$this->PublicStatus = $FolderData[6];
		$this->RecordType = $FolderData[7];
		$this->RecordStatus = $FolderData[8];
		$this->DateInput = $FolderData[9];
		$this->onTop = $FolderData[10];
		$this->PhotoResize = $FolderData[11]; // Added by Thomas on 2010-08-04
		$this->DemensionX = $FolderData[12];  // Added by Thomas on 2010-08-04
		$this->DemensionY = $FolderData[13];  // Added by Thomas on 2010-08-04
		
	}
	# ------------------------------------------------------------------------------------

	function StatusImg()
	{
		global $LAYOUT_SKIN, $image_path, $eComm;
		return (!$this->PublicStatus) ? "<img src=$image_path/$LAYOUT_SKIN/ecomm/icon_private.gif border=0 align=absmiddle title='". $eComm['Private'] ."' alt='". $eComm['Private'] ."'>" : "";
	}
	
	function FirstFileID()
	{
	    $sql = "SELECT FileID FROM INTRANET_FILE WHERE FolderID = '".IntegerSafe($this->FolderID) ."' order by Title limit 1";
		$x = $this->returnArray($sql,1);
		return $x[0][0];
	}
        
        function CoverFile()
        {
//        	$sql = "SELECT FileID FROM INTRANET_FILE WHERE FolderID = ".$this->FolderID ." and CoverImg=1 limit 1";
//			$x = $this->returnArray($sql,1);
//			if($x[0][0])
//				return $x[0][0];
//			else
//			{	
//				//set the first file as the cover image
//				return $this->FirstFileID();
//			}
$sql = "SELECT FileID FROM INTRANET_FILE WHERE FolderID = '".IntegerSafe($this->FolderID) ."' ORDER BY CoverImg DESC, Title ASC limit 1";
			$x = $this->returnArray($sql,1);
			return $x[0][0];
        }
        
	function TypeImg($FolderType)
	{
		global $LAYOUT_SKIN, $image_path;
		
		$FolderType = $FolderType ? $FolderType : $this->FolderType;
		switch($FolderType)
		{
			case "P":
				$x = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_photo.gif" width="20" height="20" align="absmiddle" border="0">';
				break;
			case "V":
				$x = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_video.gif" width="20" height="20" align="absmiddle" border="0">';
				break;
			case "W":
				$x = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_website.gif" width="20" height="20" align="absmiddle" border="0">';
				break;				
			case "WA":
				$x = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_file.gif" width="20" height="20" align="absmiddle" border="0">';
				break;				
			case "F":
			default:
				$x = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/icon_file.gif" width="20" height="20" align="absmiddle" border="0">';
				break;
		}
		
		return $x;
	}
	
	function TypeName($FolderType)
	{
		global $eComm;
		$FolderType = $FolderType ? $FolderType : $this->FolderType;
		switch($FolderType)
		{
			case "P":
				$x = $eComm['Photo'];	
				break;
			case "V":
				$x = $eComm['Video'];	
				break;
			case "W":
				$x = $eComm['Website'];	
				break;				
			case "F":
				$x = $eComm['File'];	
				break;	
			case "WA":
				$x = $eComm['WaitingRejectList'];	
				break;
			default:
				$x = $eComm['LatestShare'];	
				break;
		}
		
		return $x;
	}
	
	function FolderTitle($FolderType)
	{
		global $eComm;
		$FolderType = $FolderType ? $FolderType : $this->FolderType;
		switch($FolderType)
		{
			case "P":
				$x = $eComm['Album'];
				break;
			case "V":
				$x = $eComm['Album'];
				break;
			case "W":
				$x = $eComm['Category'];
				break;				
			case "F":
			default:
				$x = $eComm['Folder'];
				break;
		}
		
		return $x;
	}
	
	function getFolderNumber($GroupID, $FType)
	{
	    $sql = "SELECT count(FolderID) FROM INTRANET_FILE_FOLDER where GroupID='".IntegerSafe($GroupID)."' ";
		$sql .= $FType? "and FolderType='$FType' " : "";
		
		$x = $this->returnArray($sql,1);
		return $x[0][0];
	}
	
	function getFolderList($GroupID, $js_event, $selected, $FType='', $includeAll=0, $AllValue='', $AllName='')
	{
		global $UserID;
		$sql = "SELECT FolderID, Title FROM INTRANET_FILE_FOLDER where GroupID='".IntegerSafe($GroupID)."' ";
		$sql .= $FType? "and FolderType='$FType' " : "";
		$sql .= " order by onTop desc, Title";
		
		$folders = $this->returnArray($sql,2);
		return $this->arrayToHTMLSelect($folders,$js_event,$selected, $includeAll, $AllValue, $AllName);
	}
	
	function arrayToHTMLSelect($array,$js_event,$selected, $includeAll=0, $AllValue='', $AllName='')
    {
	    global $i_status_all;
	    
             $x = "<SELECT $js_event>\n";
             if($includeAll)
             {
	             $AllName = $AllName ? $AllName : $i_status_all;
	             $x .= "<OPTION value=$AllValue $op_sel>$AllName</OPTION>\n";
             }
             for ($i=0; $i<sizeof($array); $i++)
             {
                  list ($id, $name) = $array[$i];
                  $op_sel = ($selected==$id? "SELECTED":"");
                  $x .= "<OPTION value=$id $op_sel>$name</OPTION>\n";
             }
             $x .= "</SELECT>\n";
             return $x;
    }
    
    function getFileIDList()
	{
		$sql = "SELECT FileID FROM INTRANET_FILE WHERE FolderID = '".$this->FolderID."'";
		$x = $this->returnArray($sql,1);
		return $x;
	}
	
	function countFile()
	{
		global $PATH_WRT_ROOT, $GroupID;
		
		include_once($PATH_WRT_ROOT."includes/libegroup.php");
		$legroup 		= new libegroup($GroupID);
		if($legroup->needApproval)	$Approved_str = " and Approved=1";
		
		$sql = "SELECT count(FileID) FROM INTRANET_FILE WHERE FolderID = '".$this->FolderID ."' ". $Approved_str;
		$x = $this->returnArray($sql,1);
		return $x[0][0];
	}
	
	function viewFileRight()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $GroupID, $UserID,$lu2007;
		
		if(!isset($lu2007) || $lu2007->UserID!=$UserID)
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			//include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
			include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
			$lu2007 	= new libuser2007($UserID);
		}
		
		if($this->PublicStatus)
			return true;
		else
			return $lu2007->isInGroup($GroupID);
	}
	# ------------------------------------------------------------------------------------
}
?>
