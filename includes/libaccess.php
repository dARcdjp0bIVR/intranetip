<?php
// Modifing by :  

/********************** Change Log ***********************
#
#   Date    :   2019-05-13 (Cameron) fix potential sql injection problem by enclosing var with apostrophe in hasSendRight() and hasSendRight()
#
#	Date	:	2018-09-06 (Carlos) Modified auth_campusmail(), banned to assign UserID in GET/POST variables, must use session UserID.
#	Date	:	2017-10-16 (Carlos) Modified isAccessCampusmail(), checked $sys_custom['iMailDisabledUserID'] to force disable some users accessing iMail.
#	Date:	:	2015-01-29 (Carlos) Add missing exit() after header("Location: /") to prevent cloud server auto redirect	
#
#	Date	:	2012-08-22 [Siuwan]
#	Details : 	add isAccessChildrenEClassMgt(), $is_access_children_eclass for Children's eClass
#				modify retrieveAccessEClass() to retrieve $is_access_children_eclass
#
#	Date	:	2012-02-13 [Carlos]
#	Details : 	modified isAccessCampusmail(), set is_access_campusmail by retrieveAccessCampusmailForType() instead of retrieveAccessCampusmail()
#
#	Date	:	2011-12-02 [Yuen]
#	Details	:	update retrieveAccessCampusmail() to get the Internal/Internet mail setting
#
#	Date	:	2011-01-17 [Carlos]
#	Details	:	add auth checking of Shared Mailbox account in hasSendRight()
#
# 	Date	:	2010-04-16 [Yuen]
#	Details	:	add $eclass_settings_loaded to store other settings
#				retrieveEClassSettings() is modified
#
******************** End Of Change Log *******************/



include_once("$intranet_root/includes/libusertype.php");
if (!defined("LIBACCESS_DEFINED"))                     // Preprocessor directive
{
define("LIBACCESS_DEFINED", true);

function auth_campusmail()
{
	global $UserID;
	global $_SESSION, $intranet_version;

	// User should only view own records and banned to assign UserID in GET/POST variables
	if(isset($_GET['UserID']) || isset($_POST['UserID']) || $UserID != $_SESSION['UserID']){
		header("Location:/");
		exit;
	}

	if ($intranet_version == "2.5")
	{
		if (isset($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"]))
		{
			if (!$_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"])
			{
				header("Location: /");
				exit();
			}
		}
		else
		{
			$laccess = new libaccess($UserID);
			$_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] = $laccess->isAccessCampusmail();
			if (!$_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"])
			{
				header("Location: /");
				exit();
			}
		}
	}
	else
	{
	     $laccess = new libaccess($UserID);
	     if (!$laccess->isAccessCampusmail())
	     {
	         header("Location: /");
	         exit();
	     }
	}
}

function auth_resource()
{
	global $UserID;
	global $_SESSION, $intranet_version;

	if ($intranet_version == "2.5")
	{
		if (isset($_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"]))
		{
			if (!$_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"])
			{
				header("Location: /");
				exit();
			}
		}
		else
		{
	         $laccess = new libaccess($UserID);
	         $_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"] = $laccess->isAccessResource();
	         if (!$_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"])
	         {
	             header("Location: /");
	             exit();
	         }
		}
	}
	else
	{
         $laccess = new libaccess($UserID);
         if (!$laccess->isAccessResource())
         {
             header("Location: /");
             exit();
         }
     }
}

function auth_sendmail()
{
	global $UserID;
	global $_SESSION, $intranet_version;

	if ($intranet_version == "2.5")
	{
		if (isset($_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"]))
		{
			return $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"];
		}
		else
		{
         	$laccess = new libaccess($UserID);
         	$_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"] = $laccess->hasSendRight();

         	return $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"];
		}
	}
	else
	{
         $laccess = new libaccess($UserID);
         return $laccess->hasSendRight();
     }
}

class libaccess extends libusertype
{
        var $UserID;
        #var $groupsRetrieved;
        #var $groupList;
		var $is_access_children_eclass;
        var $is_access_campusmail;
        var $is_access_resource;
        var $mapping;
        var $eclass_content = array();
        var $eclass_settings = array();
        var $eclass_settings_loaded = array();


        function libaccess ($UserID="")
        {
                $this->libdb();
                $this->UserID = $UserID;
                #$this->groupRetrieved = false;
                #$this->groupList = array();
				$this->is_access_children_eclass = "";
                $this->is_access_campusmail = "";
                $this->is_access_resource = "";
                $this->ModuleName = "eClassSettings";
                $this->mapping = array(0,0,1,3,2);         # Map the new user type to original position (N/A, Teacher/Staff, Student, Parent)

        }

        function isAccessCampusmail()
        {
	        global $_SESSION, $intranet_version, $sys_custom;

			if(isset($sys_custom['iMailDisabledUserID']) && $this->UserID !='' && in_array($this->UserID,$sys_custom['iMailDisabledUserID'])){
				$_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] = 0;
				return $_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"];
			}
			
			if ($intranet_version == "2.5")
			{
             	if (isset($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"]))
             	{
	             	return $_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"];
             	}
             	else
             	{
	                 if ($this->is_access_campusmail == "")
	                 {
	                     $this->is_access_campusmail = $this->retrieveAccessCampusmailForType($_SESSION['UserType']);
	                 }
	                 $_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] = $this->is_access_campusmail;

	                 return $this->is_access_campusmail;
             	}
             }
             else
             {
                 if ($this->is_access_campusmail == "")
                 {
                     $this->is_access_campusmail = $this->retrieveAccessCampusmailForType($_SESSION['UserType']);
                 }
                 return $this->is_access_campusmail;
             }
        }

        function retrieveAccessCampusmailForType($usertype)
        {
                 global $intranet_root;
                 $filecontent = trim(get_file_content ("$intranet_root/file/campusmail_set.txt"));
                 if ($filecontent == "") return true;

                 $content = explode("\n",$filecontent);
                 if ($usertype == 0) return false;
                 $settings = explode(":",$content[$this->mapping[$usertype]]);

                 return ($settings[0]=="" || $settings[0] == 1);
        }

        function retrieveAccessCampusmail()
        {
                 if ($this->UserID == "") return false;

                 global $intranet_root;
                 $filecontent = trim(get_file_content ("$intranet_root/file/campusmail_set_usage.txt"));
                 if ($filecontent == "") return true;

                 $content = explode("\n",$filecontent);
                 return ($content[0]=="" || $content[0]=="0");
                 /* disabled on 2011-12-02
                 $usertype = $this->returnIdentity($this->UserID);
                 if ($usertype == 0) return false;
                 $settings = split(":",$content[$this->mapping[$usertype]]);
                 return ($settings[0]=="" || $settings[0] == 1);
                 */
                 /*
                 if (!$this->groupRetrieved)
                 {
                      $this->groupList = $this->returnIdentityGroupIDs($this->UserID);
                      $this->groupRetrieved = true;
                 }
                 $groups = $this->groupList;

                 for ($i=0; $i<sizeof ($groups); $i++)
                 {
                      $settings = split(":",$content[$groups[$i]-1]);
                      if ($settings[0]=="" || $settings[0] == 1)
                          return true;
                 }
                 return false;
                 */
        }

        function isAccessResource()
        {
	        global $_SESSION, $intranet_version;

			if ($intranet_version == "2.5")
			{
             	if (isset($_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"]))
             	{
	             	return $_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"];
             	}
             	else
             	{
	                 if ($this->is_access_resource == "")
	                 {
	                     $this->is_access_resource = $this->retrieveAccessResource();
	                 }
	                 $_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"] = $this->is_access_resource;

	                 return $this->is_access_resource;
             	}
        	}
        	else
        	{
                 if ($this->is_access_resource == "")
                 {
                     $this->is_access_resource = $this->retrieveAccessResource();
                 }
                 return $this->is_access_resource;
             }
        }

        function retrieveAccessResource()
        {
                 global $intranet_root;
                 if ($this->UserID == "") return false;

                 $filecontent = trim(get_file_content ("$intranet_root/file/resource_set.txt"));
                 if ($filecontent == "") return true;

                 $content = explode("\n",$filecontent);
                 $usertype = $this->returnIdentity($this->UserID);
                 if ($usertype == 0) return false;
                 $settings = $content[$this->mapping[$usertype]];

                 return ($settings=="" || $settings == 1);
                 /*
                 if (!$this->groupRetrieved)
                 {
                      $this->groupList = $this->returnIdentityGroupIDs($this->UserID);
                      $this->groupRetrieved = true;
                 }
                 $groups = $this->groupList;

                 for ($i=0; $i<sizeof ($groups); $i++)
                 {
                      $settings = $content[$groups[$i]-1];
                      if ($settings[0]=="" || $settings[0] == 1)
                          return true;
                 }
                 return false;
                 */
        }

        function isAccessChildrenEClassMgt()
        {
			return ($this->is_access_children_eclass == 1);
        }

        function isAccessEClassMgt()
        {
			return ($this->eclass_content[0] == 1);
        }

		function isAccessEClassMgtCourse()
        {
			return ($this->eclass_content[1] == 1);
        }
		
		function isAccessEClassNTMgtCourse()
        {
			return ($this->eclass_content[2] == 1);
        }

		function isAccessEClassEmailTeacher()
        {
			return ($this->eclass_settings[0] != 1);
        }

		function isAccessEClassEmailHelper()
        {
			return ($this->eclass_settings[1] != 1);
        }

		function isAccessEClassEmailStudent()
        {
			return ($this->eclass_settings[2] != 1);
        }

        function retrieveAccessEClass($loadDBData = false)
        {
      			global $intranet_root;
      			global $_SESSION, $intranet_version;
      
      			# get from DB
            include_once("libgeneralsettings.php");
			      $lgeneralsettings = new libgeneralsettings();
            $settings_ary = $lgeneralsettings->Get_General_Setting($this->ModuleName);
            if(sizeof($settings_ary) > 0)
            {
              foreach($settings_ary as $settingName => $settingValue)
              {
                if($settingName == 'isAccessEClassMgt') $this->eclass_content[0] = $settingValue;
                else if($settingName == 'isAccessEClassMgtCourse') $this->eclass_content[1] = $settingValue;
				else if($settingName == 'isAccessEClassNTMgtCourse') $this->eclass_content[2] = $settingValue;
 				else if($settingName == 'isAccessChildrenEClassMgt') $this->is_access_children_eclass = $settingValue;               
              }
            }         
             
            if ($intranet_version == "2.5")
      			{
               	if (isset($_SESSION["SSV_PRIVILEGE"]["eclass"]["is_access"]))
               	{
                    if(!$loadDBData)
                      $this->eclass_content = $_SESSION["SSV_PRIVILEGE"]["eclass"]["is_access"];
               	}
               	else
               	{
        					$_SESSION["SSV_PRIVILEGE"]["eclass"]["is_access"] = $this->eclass_content;
        
        					return;
               	}
           	}

        }

		    function retrieveEClassSettings()
        {
              global $eclass_filepath;
              /*
              $filecontent = trim(get_file_content ("$eclass_filepath/files/settings.txt"));
              if ($filecontent == "") return true;
            
              $this->eclass_settings = explode("\n",$filecontent);
              */
              
              # get from DB
              include_once("libgeneralsettings.php");
				      $lgeneralsettings = new libgeneralsettings();
              
              $settings_ary = $lgeneralsettings->Get_General_Setting($this->ModuleName);              
              
              if(sizeof($settings_ary) > 0)
              {
                foreach($settings_ary as $settingName => $settingValue)
                {
                  if($settingName == 'isAccessEClassEmailTeacher') 
                  	$this->eclass_settings[0] = $settingValue;
                  else if($settingName == 'isAccessEClassEmailHelper') 
                  	$this->eclass_settings[1] = $settingValue;
                  else if($settingName == 'isAccessEClassEmailStudent') 
                  	$this->eclass_settings[2] = $settingValue;
                  else
                  	$this->eclass_settings_loaded[$settingName] = $settingValue;
                }
              }
              
              return;
        }

        function hasSendRight()
        {
			global $intranet_root, $intranet_version, $plugin;

			if ($intranet_version == "2.5")
			{
				if (isset($_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"]))
				{
					return $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"];
				}
				else
				{
	                 if ($this->UserID == "") return false;

	                 $list = trim(get_file_content("$intranet_root/file/campusmail_banlist.txt"));
	                 if ($list == "") return true;

	                 $banusers = explode("\n",$list);
	                 for ($i=0; $i<sizeof($banusers); $i++)
	                 {
	                      $banusers[$i] = trim($banusers[$i]);
	                 }
	                 $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '".$this->UserID."'";
	                 $result = $this->returnVector($sql);
	                 $login = $result[0];
	                 if($plugin['imail_gamma'] === true){
	                 	// When user switch to Shared Mailbox, check the Email Login Name
	                 	if(trim($_SESSION['SSV_LOGIN_EMAIL'])!=''){
		                 	$GammaMailUserLogin = $_SESSION['SSV_LOGIN_EMAIL'];
		                 	if(stristr($GammaMailUserLogin,"@"))
		                 		$GammaMailUserLogin = substr($GammaMailUserLogin, 0, strpos($GammaMailUserLogin, "@"));
		                 	$login = $GammaMailUserLogin;
	                 	}
	                 }
	                 if (in_array($login,$banusers))
	                 {
		                 $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"] = false;
	                     return false;
	                 }
	                 else
	                 {
		                 $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_send_right"] = true;
	                     return true;
	                 }
                 }
             }
             else
             {
                 if ($this->UserID == "") return false;

                 $list = trim(get_file_content("$intranet_root/file/campusmail_banlist.txt"));
                 if ($list == "") return true;

                 $banusers = explode("\n",$list);
                 for ($i=0; $i<sizeof($banusers); $i++)
                 {
                      $banusers[$i] = trim($banusers[$i]);
                 }
                 $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '".$this->UserID."'";
                 $result = $this->returnVector($sql);
                 $login = $result[0];
                 if (in_array($login,$banusers))
                 {

                     return false;
                 }
                 else
                 {
                     return true;
                 }
             }
        }

/*
        function isInGroup ($UserID, $gid)
        {
                 if (!$this->groupRetrieved)
                 {
                     $this->groupList = $this->returnIdentityGroupIDs($UserID);
                     $this->groupRetrieved = true;
                 }
                 $array = $this->groupList;
                 if (in_array($gid, $array))
                     return true;
                 else return false;
        }

        function isTeacher($UserID)
        {
                 return $this->isInGroup($UserID, 1);
        }

        function isStudent($UserID)
        {
                 return $this->isInGroup($UserID, 2);
        }

        function isAdminStaff($UserID)
        {
                 return $this->isInGroup($UserID, 3);
        }

        function isParent($UserID)
        {
                 return $this->isInGroup($UserID, 4);
        }

        function returnIdentityGroupIDs($UserID)
        {
                $sql = "select b.GroupID from INTRANET_USER as a ,INTRANET_USERGROUP as b where a.UserID = b.UserID and a.UserID = $UserID AND b.GroupID IN (1,2,3,4)";
                return $this->returnVector($sql);
        }

*/
}


}        // End of directive
?>