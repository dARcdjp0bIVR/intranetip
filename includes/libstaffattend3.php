<?php
// using by 

/* Change log
 *  2020-11-04 (Ray):    Modified Get_Report_Customize_Monthly_Detail_Report_Data, late & early leave non-waived not count
 *  2020-05-14 (Ray):    Added Get_School_Event_By_Date_Range $allow_past_date
 *  2019-08-13 (Ray):    Added INSERT_UPDATE_LOG, Modified Save_Past_Duty, Save_Shifting_Duty, Save_Duty_Period_User_Time_Slot add log
 *  2019-02-21 (Carlos): Modified Check_Card_ID() and Import_Staff_Info(), cater $sys_custom['SupplementarySmartCard'] to prevent duplicated card number with students. 
 *  2019-02-04 (Carlos): Modified Save_Past_Duty(), manual input times would not create raw tap card records after edited.
 *  2018-11-13 (Carlos): Modified Get_Duty_Period_Slot_Duty_Distribution() to only get active staff users.
 *  2018-11-09 (Carlos): Modified Finalize_Attendance_Offline_Import(), update affected daily log records ModifyBy to current import user.
 *  2018-10-16 (Carlos): Modified Finalize_Attendance_Offline_Import(), improved to merge manual input in/out times in daily log records.
 *  2018-05-08 (Carlos): Modified Get_Report_Date_Range_Summary() and Get_Report_Monthly_Summary_Individual_Data(), added MinLate and MinEarlyLeave in select query. 
 *  2018-04-25 (Carlos): Fixed Save_Profile_Set(), reset MinLate, MinEarlyLeave to null if not late, early leave respectively. 
 *  2018-02-07 (Carlos): Added Get_FullDay_Setting_Records($params) to get leave records more daynamically.
 *  2018-01-23 (Carlos): Modified Get_Full_Date_Setting_By_Date(), Save_Full_Day_Setting(), added Remark field.
 *  2018-01-22 (Carlos): Added Get_Overall_Daily_Attendance_Records() for updating take attendance records by daily log RecordID.
 *  2017-11-30 (Carlos): Modified Get_Real_Time_Staff_Status(), display reason for absent, late, early leave status.
 *  2017-07-27 (Carlos): Fixed Save_Past_Duty() to sort tap card times by datetime order and do not count the manual input times for overnight duty.
 *  2017-06-29 (Carlos): [ip.2.5.8.7.1] Added log() and __destruct() for logging update/remove/delete action pages.
 *  2017-01-03 (Carlos): [ip2.5.8.1.1] modified Save_Past_Duty(), fixed the order issue of tap card times that input to the simulate tap card process.
 *  2016-12-13 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - modified Save_Shifting_Duty(), Save_Past_Duty(), Get_Export_Duty_Records() to apply new field AttendOne. 
 *  2016-11-22 (Carlos): Fixed Get_Real_Time_Staff_Status() no out time bug. 
 *  2016-11-09 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - added Get_Working_Hours_Deduction_Records($params), Add_Working_Hours_Deduction_Record($map), Delete_Working_Hours_Deduction_Record($params), 
 * 						 Get_OT_Records($params), Upsert_OT_Record($map), Delete_OT_Record($params), Get_Full_Date_Leave_Records($params)
 * 						 modified Get_Report_Working_Hour_Data() 
 * 	2016-10-31 (Cameron): - add function Get_Monitoring_Groups() and Get_Monitoring_Members()
 * 		- add monitoring filter to Get_Report_Monthly_Summary_Group_Data(), Get_Report_Daily_Details_Group_Data()
 * 		- add $monitoring parameter to Get_Aval_User_List()
 *  2016-07-07 (Carlos) : [ip2.5.7.7.1] modified Save_Past_Duty(), merge manual input time with raw tap card time to simulate tap card. And remap original absent status to new duty records.
 *  2016-03-24 (Carlos) : [ip2.5.7.4.1] added Get_Export_Duty_Records() for exporting duty records.
 *  2016-03-09 (Carlos) : [ip2.5.7.3.1] modified Get_InOut_Record_List(), added InSchoolStatus and OutSchoolStatus to query.
 *  2015-12-30 (Carlos) : [ip2.5.7.1.1] modified Import_Staff_Working_Periods(), fixed overlap period checking. 
 *  2015-10-08 (Carlos) : [ip2.5.6.10.1] modified Get_Report_DailyLog_Data() and Get_InOut_Record_List(), added parameter IncludeAbsentSuspectedRecord.
 *  2015-10-07 (Carlos) : [ip2.5.6.10.1] added Get_Attendance_Status_Settings(), Update_Attendance_Status_Setting($StatusType, $StatusSymbol, $StatusColor), Check_Attendance_Status_Symbol($StatusType, $StatusSymbol).
 * 										modified Get_Preset_Leave_Reason_List(), Get_Preset_Leave_Reason_Detail(), Save_Preset_Leave_Reason(), Check_Preset_Leave_Reason().
 *  2015-09-01 (Carlos) : [ip2.5.6.10.1] added Get_Preset_Leave_Reason_Active_Count().
 *  2015-08-20 (Carlos) : [ip2.5.6.10.1] added Get_No_Tap_Card_Records().
 *  2015-08-18 (Carlos) : [ip2.5.6.10.1] modified Get_Aval_User_List(), Get_Group_Member_List(), added UserLogin to query result set.
 *  2015-07-20 (Carlos) : [ip2.5.6.7.1] modified Get_Weekly_Duty_Period(), Check_Duty_Period(), Save_Duty_Period(), added day type to regular duty period record. 
 *  2015-06-11 (Carlos) : [ip2.5.6.7.1] modified Get_Report_DailyLog_Data() and Get_InOut_Record_List() to display all attendance records with time slot, no need to tap card or confirmed. 
 *  2015-06-10 (Carlos) : [ip2.5.6.7.1] updated Save_Past_Duty(), sync daily log record in and out waive fields with its profiles.
 *  2015-01-26 (Carlos) : [ip2.5.6.3.1] modified Get_Report_Customize_Monthly_Detail_Report_Data() and Get_Report_Customize_Monthly_Summary_Report_Data(), cater check only late or only early leave or both late and early leave situations.
 *  2015-01-19 (Carlos) : modified Save_Past_Duty() handle overnight duty leave records. Modified with libstaffattend3_api.php Record_Tap_Card().
 *  2015-01-07 (Carlos) : modified Get_Report_Customize_Monthly_Detail_Report_Data() and Get_Report_Customize_Monthly_Summary_Report_Data(), fix not include waived records condition.
 *  2014-12-24 (Carlos) : Modified Save_Past_Duty(), update InputBy and ModifyBy after editted daily log records for tracing
 *  2014-12-10 (Carlos) : improved Get_Real_Time_Staff_Status() to get all users daily log records in one query
 *  2014-11-13 (Carlos) : modified Get_Report_Customize_Monthly_Detail_Report_Data() and Get_Report_Customize_Monthly_Summary_Report_Data(), 
 * 						  if not include waived records, status is late & earlyleave, either late or earlyleave is non-waived is also counted
 *  2014-09-30 (Carlos) : modified constructor, init MaxSlotPerSetting with $sys_custom['StaffAttendance']['MaxSlotPerSetting'] if set
 *  2014-09-02 (Carlos) : modified Save_Duty_Period_User_Time_Slot() and Save_Duty_Period(), do not run Freeze_Duty_By_Date() to avoid performance issue
 *  2014-06-10 (Carlos) : modified Get_Report_Monthly_Details_Data() - fix late records join conditions
 *  2014-06-05 (Carlos) : modified Get_Report_Customize_Monthly_Detail_Report_Data(), do not pad off duty data for option [HaveRecord] (Only show staffs that have records)
 *  2014-05-27 (Carlos) : modified Get_Report_Monthly_Details_Data(), use DateConfirmed for DateModified if it is not null 
 *  2014-03-27 (Carlos) : modified Get_Real_Time_Staff_Status() - display raw log times when request to In/Out Status mode
 *  2014-02-19 (Carlos) : added Get_Customized_Settings() 
 *  2014-02-14 (Carlos) : added global function StaffAttendance_USortCmp() for sorting array by three columns
 *  2014-01-09 (Carlos) : modified Get_Report_Customize_Monthly_Detail_Report_Data(), pad empty data to no duty days when [DisplayAllCalendarDay] is on
 *  2013-11-08 (Carlos) : Display left staff in reports according to setting (except [Roster Report])
 * 						  modified Get_Full_Day_Setting_User_List(), Get_Working_Period_User_List(), Get_Staff_Info(),Get_Report_Monthly_Summary_Group_Data(),
 *							Get_Group_Member_List(), Get_Report_Daily_Details_Group_Data(),Get_Report_Date_Range_Summary(), Get_Aval_User_List(), Get_Report_OT_Data(), Get_InOut_Record_List(),
 *							Get_Absence_Report_Data(), Get_LeaveAbsence_Report_Data(), Get_Report_Working_Hour_Data() 
 *					  		added Get_Archive_Name_Field()
 *  2013-11-07 (Carlos) : modified Save_Past_Duty(), keep original cardlog remark after edited past duty
 *  2013-08-13 (Carlos) : fix Get_Profile_Status_List() - absent suspected get active staff users only
 *  2013-07-22 (Carlos) : modified Save_Duty_Period_User_Time_Slot(), 
 * 						  $sys_custom['StaffAttendance']['CopyRegularDutyAsSpecialDuty'] - for special duty days, copy regular duty as special duty for those no special duty staff 
 *  2013-07-17 (Carlos) : fix Get_Report_Monthly_Details_Data() - add OutSchoolStatus != EARLY LEAVE for non early leave status
 *  2012-12-19 (Carlos) : fix Get_LeaveAbsence_Report_Data() start date of preset leave records
 *  2012-12-17 (Carlos) : modified Get_InOut_Record_List() - match min time, max time and station records
 *  2012-11-06 (Carlos) : modified report related functions to cater setting ReportDisplaySuspendedStaff
 *  2012-08-28 (Carlos) : modified Get_Report_Customize_Monthly_Detail_Report_Data(), get Doctor Certificate file records when option checked
 *  2012-08-27 (Carlos) : added Get_Doctor_Certificate_DBTable_Sql(),
 * 							Get_Doctor_Certificate_By_FileID(),
 * 							Is_Doctor_Certificate_File_Owner(),
 *							Manage_Doctor_Certificate_Files(),
 *							Delete_Doctor_Certificates(),
 *							Delete_Doctor_Certificate_By_FileID()
 *  2012-08-20 (Carlos) : modified Get_Profile_Status_List() and Save_Profile_Set(), added [Absent Suspected] status
 *  2012-08-03 (Carlos) : modified Import_Staff_Working_Periods() and Finalize_Import_Staff_Working_Periods() to allow update single start date or single end date working periods
 *  2012-04-05 (Carlos) : modified Save_Past_Duty() set reason DefaultWavie to PROFILE Waived and DAILYLOG InWaived/OutWaived
 *  2012-04-02 (Carlos) : use reason default waive flag if daily log not confirmed yet. 
 * 						  modified Get_View_Attendance_Record_Group_List() and Get_Attendance_Record_Individual_List_By_UserID()
 *  2011-11-17 (Carlos) : added Get_Report_Working_Hour_Data()
 *  2011-11-03 (Carlos) : modified Import_Staff_Info(), format SmartCardID to 10 digits
 *  2011-10-21 (Carlos) : added Log_Record() to log down special duty removal as request by school
 *  2011-09-06 (Carlos) : modified Save_Duty_Period_User_Time_Slot(), add checking on valid staff id to insert CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY records
 *  2011-09-06 (Carlos) : modified Get_Daily_Log_Entry_Info(), 王錦輝中小學 customize StaffID from INTRANET_USER Remark
 *  2011-07-12 (Carlos) : added Get_Daily_Log_Entry_Info() for 王錦輝中小學  customized entry log csv 
  	2011-05-24 (Carlos) : modified Get_Entry_Log() add parameter DayNumber
 	2011-04-04 (Carlos) : modified Get_Attendance_Record_Group_List_Overview() exclude no duty staff at Off Duty
	2011-02-17 (Carlos) : Save_Profile_Set() added updating InWaived/OutWaived field in CARDLOG
	2010-11-26 (Carlos) : use DateConfirmed and ConfirmBy as the last update date and upadter at Group Slot Overview 
	2010-10-30 (Carlos) : added Get_Report_Date_Range_Summary()

	On: 2010-10-11
	By: Kenneth chung
	Detail: changed Get_Real_Time_Staff_Status, add last tap card time

	On: 2010-10-06
	By: Kenneth chung
	Detail: changed Save_Past_Duty, fix problem that slot outing/ holiday reason cannot be set
	
	On: 2010-10-05
	By: Kenneth chung
	Detail: changed Get_Real_Time_Staff_Status, added group name on reset set
*/
include_once("libdb.php");
include_once("libstaffattend2.php");
include_once("libstaffattend3_api.php");
include_once("libstaffattend3_staff_access_right.php");
include_once("libstaffattend3_staff_attend_group.php");
include_once("libstaffattend3_staff_attend_slot.php");
include_once("libstaffattend3_staff_special_template.php");
include_once("libaccessright.php");

// usort() compare function for sorting an array by three to four columns
function StaffAttendance_USortCmp($a,$b)
{
	global $CmpField, $CmpField2, $CmpField3, $CmpField4;
	
	if(!is_numeric($a[$CmpField])||!is_numeric($b[$CmpField])) {
		if ($CmpField2 != '' && $a[$CmpField] == $b[$CmpField]) {
			if ($a[$CmpField2] == $b[$CmpField2]) {
				if($a[$CmpField3] == $b[$CmpField3]){
					if($CmpField4 != ''){
						return strcasecmp($a[$CmpField4],$b[$CmpField4]);
					}else{
						return 0;
					}
				}else{
					return strcasecmp($a[$CmpField3],$b[$CmpField3]);
				}
			}else{
				return strcasecmp($a[$CmpField2],$b[$CmpField2]);
			}
		}
		else {
			return strcasecmp($a[$CmpField],$b[$CmpField]);
		}
	}
	
	if ($a[$CmpField] == $b[$CmpField]) {
		if ($CmpField2 != '') {
			if ($a[$CmpField2] == $b[$CmpField2]) {
				if($a[$CmpField3] == $b[$CmpField3]){
					if($CmpField4 != ''){
						return strcasecmp($a[$CmpField4],$b[$CmpField4]);
					}else{
						$result = 0;
					}
				}else{
					return strcasecmp($a[$CmpField3],$b[$CmpField3]);
				}
			}
			else {
				$result = strcasecmp($a[$CmpField2], $b[$CmpField2]);
			}
		}
		else {
			$result =  0;
		}
    }
    else
    {
    	$result = strcasecmp($a[$CmpField], $b[$CmpField]);
    }
    return $result;
}

class libstaffattend3 extends libstaffattend3_api {
	var $ModuleName;
	var $MaxSlotPerSetting;
	var $StatusCount;
	var $GeneralSettings = false;
	
	var $LibAccessRight = null;
	var $AdminUserFileContent = null;
	var $IsAdminUserFileReaded = false;
	
	function libstaffattend3() {
		global $sys_custom;
		parent::libstaffattend3_api();
		
		$this->ModuleName = 'StaffAttendance3';
		$this->MaxSlotPerSetting = $sys_custom['StaffAttendance']['MaxSlotPerSetting']!=''?$sys_custom['StaffAttendance']['MaxSlotPerSetting']:10;
		$this->StatusCount = 1;
	}
	
	function __destruct()
	{
		global $___STAFF_ATTENDANCE_LOGGED___; // prevent same page being logged more than one time
		// log down when and who do update or remove action under module path
		if( !$___STAFF_ATTENDANCE_LOGGED___  && (strpos($_SERVER['REQUEST_URI'],'/home/eAdmin/StaffMgmt/attendance/')!==false)
			&& (strpos($_SERVER['REQUEST_URI'],'update')!==false || strpos($_SERVER['REQUEST_URI'],'save')!==false || strpos($_SERVER['REQUEST_URI'],'remove')!==false || strpos($_SERVER['REQUEST_URI'],'delete')!==false))
		{
			$___STAFF_ATTENDANCE_LOGGED___ = 1;
			$log = date("Y-m-d H:i:s").' '.OsCommandSafe($_SERVER['REQUEST_URI']).' '.(isset($_SESSION['UserID'])?$_SESSION['UserID']:0).' '.(count($_POST)>0?base64_encode(serialize($_POST)):'');
			$this->log($log);
		}
	}
	/*
	function log($log_row)
	{
		global $intranet_root, $file_path, $sys_custom;
		
		$num_line_to_keep = isset($sys_custom['StaffAttendanceLogMaxLine'])? $sys_custom['StaffAttendanceLogMaxLine'] : 400000;
		$log_file_path = $file_path."/file/staff_attendance_log";
		$log_file_path_tmp = $file_path."/file/staff_attendance_log_tmp";
		// assume $log_row does not contains single quote that would break the shell command statement
		shell_exec("echo '$log_row' >> $log_file_path"); // append to log file
		$line_count = intval(shell_exec("cat '$log_file_path' | wc -l"));
		if($line_count > $num_line_to_keep){
			shell_exec("tail -n ".($num_line_to_keep/2)." '$log_file_path' > '$log_file_path_tmp'"); // keep the last $num_line_to_keep/2 lines
			shell_exec("rm '$log_file_path'");
			shell_exec("mv '$log_file_path_tmp' '$log_file_path'");
		}
	}
	*/
	function Get_Access_Right_Lib()
	{
		if(!$this->LibAccessRight){
			include_once("libaccessright.php");
			$this->LibAccessRight = new libaccessright($this->ModuleName);
		}
		
		return $this->LibAccessRight;
	}
	
	function Get_Stat_Weekday_Distribution($GroupID,$StartDate,$EndDate,$Status) {				
		$sql = 'select 
							DAYOFWEEK(p.RecordDate) as DayOfWeek,
							p.RecordType,
							p.ProfileCountFor 
						from 
							CARD_STAFF_ATTENDANCE2_PROFILE p 
							INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
						 	ON wp.UserID=p.StaffID 
						 		AND p.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
							';
		if ($GroupID != "") {
			$sql .= 'inner join 
							CARD_STAFF_ATTENDANCE_USERGROUP ug 
							on 
								p.RecordDate Between \''.$StartDate.'\' and \''.$EndDate.'\' 
								and 
								p.RecordType in ('.implode(',',$Status).')
								and 
								p.StaffID = ug.UserID 
								and 
								ug.GroupID = \''.$GroupID.'\' ';
		}
		else {
			$sql .= 'where 
								p.RecordDate Between \''.$StartDate.'\' and \''.$EndDate.'\' 
								and 
								p.RecordType in ('.implode(',',$Status).')';
		}
		$Result = $this->returnArray($sql);
		
		for ($i=1; $i<= 7; $i++) {
			$Return[$i][CARD_STATUS_LATE] = 0;
			$Return[$i][CARD_STATUS_ABSENT] = 0;
			$Return[$i][CARD_STATUS_EARLYLEAVE] = 0;
			$Return[$i][CARD_STATUS_HOLIDAY] = 0;
			$Return[$i][CARD_STATUS_OUTGOING] = 0;
		}
		$Max = 0; // max unit for y axis
		
		for ($i=0; $i< sizeof($Result); $i++) {
			$Return[$Result[$i]['DayOfWeek']][$Result[$i]['RecordType']] += 1;
			
			if ($Return[$Result[$i]['DayOfWeek']][$Result[$i]['RecordType']] > $Max) 
				$Max = $Return[$Result[$i]['DayOfWeek']][$Result[$i]['RecordType']];
		}
		
		for ($i=0; $i< ($Max+10); $i+=10) {
			$Return['Max'] = $i;
		}
		return $Return;
	}
	
	function Get_Module_Access_Right() {
		//include_once("libaccessright.php");
		//$Access = new libaccessright($this->ModuleName);
		$Access = $this->Get_Access_Right_Lib();
		
		$AccessRight = $Access->retrieveUserAccessRight($_SESSION['UserID']);
		if (sizeof($AccessRight[strtoupper($this->ModuleName)]) > 0) {
			$_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"] = true;
			if (is_array($_SESSION['SESSION_ACCESS_RIGHT'])) 
				$_SESSION['SESSION_ACCESS_RIGHT'] = array_merge($_SESSION['SESSION_ACCESS_RIGHT'],$AccessRight);
			else 
				$_SESSION['SESSION_ACCESS_RIGHT'] = $AccessRight;
		}
		else {
			$_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"] = false;
		}
	}
	
	function Check_Access_Right($RightString) {
		//include_once("libaccessright.php");
		//$Access = new libaccessright($this->ModuleName);
		$Access = $this->Get_Access_Right_Lib();
		$RightString = $this->ModuleName.'-'.$RightString;
		
		switch (sizeof(explode('-',$RightString))) {
			case 2:
				$Return = $Access->CHECK_SECTION_ACCESS($RightString);
				break;
			case 3:
				$Return = $Access->CHECK_FUNCTION_ACCESS($RightString);
				break;
			case 4:
				$Return = $Access->CHECK_ACCESS($RightString);
				break;
			default: 
				$Return = false;
				break;
		}
		
		return $Return;
	}
	
	function Save_Past_Duty($ID,$TargetDate,$TimeSlotName,$TimeSlotUser,$HolidayUser,$OutgoingUser,$OutgoingReason,$HolidayReason,$FullHolidayList,$FullOutgoingList,$FullHolidayReason,$FullOutgoingReason,$SlotName,$SlotStart,$SlotEnd,$SlotInWavie,$SlotOutWavie,$SlotDutyCount,$GroupOrIndividual="Group",$AttendOne=0) {
		global $sys_custom, $Lang;

		$CARD_STATUS_LATEEARLYLEAVE = 6;
		$CARD_STATUS_ABSENT_SUSPECTED = 7;
		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;

		$StaffOccupied = array();
		$TargetDateArray = explode('-',$TargetDate);
		$inputBy = $_SESSION['UserID'];
		$RawLogTable = $this->buildStaffRawLogMonthTable($TargetDateArray[0], $TargetDateArray[1]);
    	$DailyLogTable = $this->createTable_Card_Staff_Attendance2_Daily_Log($TargetDateArray[0], $TargetDateArray[1]);
    	if ($GroupOrIndividual == "Group") {
	    	$Obj = new staff_attend_group($ID,true);
	    	$TargetUserList = $Obj->MemberList;
	  	}
	  	else {
	  		$Obj = $ID;
	  		$TargetUserList = array($ID);
	  	}
	  	$OrgDuty = $this->Get_Duty_By_Date($Obj,$TargetDate,false);
      	$overnight_duty_userid = array();
		$overnight_tomorrow_ts = strtotime($TargetDate)+86400;
		$overnight_tomorrow_date = date("Y-m-d",$overnight_tomorrow_ts);
		$overnight_tomorrow_year = date("Y",$overnight_tomorrow_ts);
		$overnight_tomorrow_month = date("m",$overnight_tomorrow_ts);
		$overnight_tomorrow_day = date("d",$overnight_tomorrow_ts);
		$overnight_tomorrow_rawlog_table = "CARD_STAFF_ATTENDANCE2_RAW_LOG_".$overnight_tomorrow_year."_".$overnight_tomorrow_month;
	  
      $OrgCardlogData = array(); // use for keeping original remark
      $OrgCardlogTimes = array(); // will be merge will raw tap card log
      $OrgCardlogUserIdTimeMap = array(); // for faster checking to avoid duplicated times from raw log
      $OrgCardlogAbsent = array(); // [StaffID][TimeSlotName] = StaffID
    if (sizeof($TargetUserList) > 0) {
    		$sql = "SELECT ReasonID, DefaultWavie FROM CARD_STAFF_ATTENDANCE3_REASON WHERE ReasonActive = 1 AND ReasonType IN (".CARD_STATUS_HOLIDAY.",".CARD_STATUS_OUTGOING.")";
			$ReasonIDDefaultWavieArray = $this->returnArray($sql);
			$ReasonIDToDefaultWavieArray = array();
			for($tt=0;$tt<count($ReasonIDDefaultWavieArray);$tt++){
				$ReasonIDToDefaultWavieArray[$ReasonIDDefaultWavieArray[$tt]['ReasonID']] = $ReasonIDDefaultWavieArray[$tt]['DefaultWavie'];
			}
    		
    		$sql = "SELECT d.StaffID,d.SlotName,d.DutyStart,d.DutyEnd,d.Remark,d.InSchoolStatus,d.InTime,d.InSchoolStation,d.OutTime,d.OutSchoolStation   
					FROM $DailyLogTable as d 
					WHERE d.DayNumber='".$TargetDateArray[2]."' AND d.StaffID IN (".implode(",",$TargetUserList).") ORDER BY d.StaffID,d.DutyStart";
    		$tmpCardlogAry = $this->returnResultSet($sql);
    		$tmpCardlogAry_count = count($tmpCardlogAry);
    		for($i=0;$i<$tmpCardlogAry_count;$i++) {
    			if(!isset($OrgCardlogData[$tmpCardlogAry[$i]['StaffID']])){
    				$OrgCardlogData[$tmpCardlogAry[$i]['StaffID']] = array();
    			}
    			if($tmpCardlogAry[$i]['Remark']!=''){
    				$OrgCardlogData[$tmpCardlogAry[$i]['StaffID']][$tmpCardlogAry[$i]['SlotName']] = $tmpCardlogAry[$i]['Remark'];
    			}
    			if($tmpCardlogAry[$i]['InTime']!='' && $tmpCardlogAry[$i]['DutyStart']<$tmpCardlogAry[$i]['DutyEnd']){ // do not add the time for overnight duty, use raw log records instead
    				$datetime = $TargetDate.' '.$tmpCardlogAry[$i]['InTime'];
    				$OrgCardlogTimes[] = array('UserID'=>$tmpCardlogAry[$i]['StaffID'],'RecordTime'=>$datetime,'RecordStation'=>$tmpCardlogAry[$i]['InSchoolStation']);
    				$OrgCardlogUserIdTimeMap[$tmpCardlogAry[$i]['StaffID']][$datetime] = true;
    			}
    			if($tmpCardlogAry[$i]['OutTime']!='' && $tmpCardlogAry[$i]['DutyStart']<$tmpCardlogAry[$i]['DutyEnd']){ // do not add the time for overnight duty, use raw log records instead
    				$datetime = $TargetDate.' '.$tmpCardlogAry[$i]['OutTime'];
    				$OrgCardlogTimes[] = array('UserID'=>$tmpCardlogAry[$i]['StaffID'],'RecordTime'=>$datetime,'RecordStation'=>$tmpCardlogAry[$i]['OutSchoolStation']);
    				$OrgCardlogUserIdTimeMap[$tmpCardlogAry[$i]['StaffID']][$datetime] = true;
    			}
    			if($tmpCardlogAry[$i]['InSchoolStatus'] == CARD_STATUS_ABSENT)
    			{
    				if(!isset($OrgCardlogAbsent[$tmpCardlogAry[$i]['StaffID']]))
    				{
    					$OrgCardlogAbsent[$tmpCardlogAry[$i]['StaffID']] = array();
    				}
    				$OrgCardlogAbsent[$tmpCardlogAry[$i]['StaffID']][$tmpCardlogAry[$i]['SlotName']] = $tmpCardlogAry[$i]['StaffID'];
    			}
    		}

			$StatusList = $this->Get_Attendance_Status_Settings();

			// clear old daily log record in database
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_DAILY_LOG_'.$TargetDateArray[0].'_'.$TargetDateArray[1].' 
							where 
								DayNumber = \''.$TargetDateArray[2].'\' 
								and 
								StaffID in ('.implode(',',$TargetUserList).')';
			$Result['ClearOldDailyLogRecord'] = $this->db_db_query($sql);
			
			// delete profile record in database
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_PROFILE where 
								RecordDate = \''.$TargetDate.'\' 
								and 
								StaffID in ('.implode(',',$TargetUserList).')';
			$Result['ClearProfileRecord'] = $this->db_db_query($sql);
			
			// delete OT record on target date
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_OT_RECORD 
							where 
								RecordDate = \''.$TargetDate.'\' 
								and 
								StaffID in ('.implode(',',$TargetUserList).')';
			$Result['ClearOTRecord'] = $this->db_db_query($sql);
			
			// process full holiday and outgoing first
			// Holiday
			for ($i=0; $i< sizeof($FullHolidayList); $i++) {
				$StaffOccupied[] = $FullHolidayList[$i];
				$ReasonDefaultWavie = $ReasonIDToDefaultWavieArray[$FullHolidayReason[$FullHolidayList[$i]]]==1? "1":"NULL";
				
				$sql = 'insert into CARD_STAFF_ATTENDANCE2_PROFILE (
									StaffID,
									ReasonID,
									RecordDate,
									RecordType,
									Waived,
									DateInput,
									DateModified
									)
								values (
									\''.$FullHolidayList[$i].'\',
									\''.$FullHolidayReason[$FullHolidayList[$i]].'\',
									\''.$TargetDate.'\',
									\''.CARD_STATUS_HOLIDAY.'\',
									'.$ReasonDefaultWavie.',
									NOW(),
									NOW()
								)
							';
				//debug_r($sql);
				$Result['InsertFullHolidayProfile:'.$FullHolidayList[$i]] = $this->db_db_query($sql);
				$ProfileID = $this->db_insert_id();
				
				$sql = 'INSERT INTO '.$DailyLogTable.'
									(
										StaffID, 
										DayNumber,
										Duty,
										InSchoolStatus,
										OutSchoolStatus,
										RecordType,
										InAttendanceRecordID,
										InWaived,
										DateInput,
										DateModified,
										InputBy,
										ModifyBy 
									)
									VALUES
									(
										\''.$FullHolidayList[$i].'\',
										\''.$TargetDateArray[2].'\',
										\'0\',
										\''.CARD_STATUS_HOLIDAY.'\',
										\''.CARD_STATUS_HOLIDAY.'\',
										\''.CARD_STATUS_HOLIDAY.'\',
										\''.$ProfileID.'\',
										'.$ReasonDefaultWavie.',
										NOW(),
										NOW(),
										\''.$inputBy.'\',
										\''.$inputBy.'\' 
									)';
				//debug_r($sql);
				$Result['InsertFullHolidayDailLog:'.$FullHolidayList[$i]] = $this->db_db_query($sql);
			}
			
			// Outgoing
			for ($i=0; $i< sizeof($FullOutgoingList); $i++) {
				$StaffOccupied[] = $FullOutgoingList[$i];
				$ReasonDefaultWavie = $ReasonIDToDefaultWavieArray[$FullOutgoingReason[$FullOutgoingList[$i]]]==1? "1":"NULL";
				
				$sql = 'insert into CARD_STAFF_ATTENDANCE2_PROFILE (
									StaffID,
									ReasonID,
									RecordDate,
									RecordType,
									Waived,
									DateInput,
									DateModified
									)
								values (
									\''.$FullOutgoingList[$i].'\',
									\''.$FullOutgoingReason[$FullOutgoingList[$i]].'\',
									\''.$TargetDate.'\',
									\''.CARD_STATUS_OUTGOING.'\',
									'.$ReasonDefaultWavie.',
									NOW(),
									NOW()
								)
							';
				//debug_r($sql);
				$Result['InsertFullOutgoingProfile:'.$FullOutgoingList[$i]] = $this->db_db_query($sql);
				$ProfileID = $this->db_insert_id();
				
				$sql = 'INSERT INTO '.$DailyLogTable.'
									(
										StaffID, 
										DayNumber,
										Duty,
										InSchoolStatus,
										OutSchoolStatus,
										RecordType,
										InAttendanceRecordID,
										InWaived,
										DateInput,
										DateModified,
										InputBy,
										ModifyBy 
									)
									VALUES
									(
										\''.$FullOutgoingList[$i].'\',
										\''.$TargetDateArray[2].'\',
										\'0\',
										\''.CARD_STATUS_OUTGOING.'\',
										\''.CARD_STATUS_OUTGOING.'\',
										\''.CARD_STATUS_OUTGOING.'\',
										\''.$ProfileID.'\',
										'.$ReasonDefaultWavie.',
										NOW(),
										NOW(),
										\''.$inputBy.'\',
										\''.$inputBy.'\' 
									)';
				//debug_r($sql);
				$Result['InsertFullOutgoingDailLog:'.$FullOutgoingList[$i]] = $this->db_db_query($sql);
			}
			// end process full holiday and outgoing
			
			
			// create new daily log
			for ($i=0; $i< sizeof($TimeSlotName); $i++) {
				// slot onduty
				for ($j=0; $j< sizeof($TimeSlotUser[$TimeSlotName[$i]]); $j++) {
					$StaffID = $TimeSlotUser[$TimeSlotName[$i]][$j];
					$StaffOccupied[] = $StaffID;
					if(in_array($StaffID,$FullHolidayList) || in_array($StaffID,$FullOutgoingList)){
						continue;
					}
					$RemarkValue = 'NULL';
					if(isset($OrgCardlogData[$StaffID][$SlotName[$TimeSlotName[$i]]])){
						$RemarkValue = '\''.$this->Get_Safe_Sql_Query($OrgCardlogData[$StaffID][$SlotName[$TimeSlotName[$i]]]).'\'';
					}
					if($SlotStart[$TimeSlotName[$i]] > $SlotEnd[$TimeSlotName[$i]]){
						$overnight_duty_userid[] = $StaffID;
					}
					$sql = 'INSERT INTO '.$DailyLogTable.'
									(
										StaffID, 
										DayNumber,
										SlotName,
										Duty,
										DutyStart,
										DutyEnd,
										DutyCount,
										InWavie,
										OutWavie,
										Remark,';
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$sql .= 'AttendOne,';
						}
								 $sql.='DateInput,
										DateModified,
										InputBy,
										ModifyBy 
									)
									VALUES
									(
										\''.$StaffID.'\',
										\''.$TargetDateArray[2].'\',
										\''.$this->Get_Safe_Sql_Query($SlotName[$TimeSlotName[$i]]).'\',
										\'1\',
										\''.$SlotStart[$TimeSlotName[$i]].'\',
										\''.$SlotEnd[$TimeSlotName[$i]].'\',
										\''.$SlotDutyCount[$TimeSlotName[$i]].'\',
										\''.$SlotInWavie[$TimeSlotName[$i]].'\',
										\''.$SlotOutWavie[$TimeSlotName[$i]].'\',
										'.$RemarkValue.',';
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$sql .= '\''.$AttendOne.'\',';
						}
								$sql .= 'NOW(),
										NOW(),
										\''.$inputBy.'\',
										\''.$inputBy.'\'
									)';
						//debug_r($sql);
						$Result['InsertNewDailyLog:'.$StaffID.'-'.$SlotName[$TimeSlotName[$i]]] = $this->db_db_query($sql);
				}

				// slot holiday
				for ($j=0; $j< sizeof($HolidayUser[$TimeSlotName[$i]]); $j++) {
					$StaffID = $HolidayUser[$TimeSlotName[$i]][$j];
					if (in_array($StaffID, $FullHolidayList) || in_array($StaffID, $FullOutgoingList)) {
						continue;
					}
					$ReasonDefaultWavie = $ReasonIDToDefaultWavieArray[$HolidayReason[$TimeSlotName[$i]][$StaffID]] == 1 ? "1" : "NULL";
					$RemarkValue = 'NULL';
					if (isset($OrgCardlogData[$StaffID][$SlotName[$TimeSlotName[$i]]])) {
						$RemarkValue = '\'' . $this->Get_Safe_Sql_Query($OrgCardlogData[$StaffID][$SlotName[$TimeSlotName[$i]]]) . '\'';
					}

					$sql = 'insert into CARD_STAFF_ATTENDANCE2_PROFILE (
										StaffID,
										ReasonID,
										RecordDate,
										RecordType,
										Waived,
										DateInput,
										DateModified
										)
									values (
										\'' . $StaffID . '\',
										\'' . $this->Get_Safe_Sql_Query($HolidayReason[$TimeSlotName[$i]][$StaffID]) . '\',
										\'' . $TargetDate . '\',
										\'' . CARD_STATUS_HOLIDAY . '\',
										' . $ReasonDefaultWavie . ',
										NOW(),
										NOW()
									)
								';
					$Result['InsertHolidayProfile:' . $StaffID] = $this->db_db_query($sql);
					$ProfileID = $this->db_insert_id();


					$sql = 'INSERT INTO ' . $DailyLogTable . '
										(
											StaffID, 
											DayNumber,
											SlotName,
											Duty,
											DutyStart,
											DutyEnd,
											DutyCount,
											InWavie,
											OutWavie,
											InSchoolStatus,
											OutSchoolStatus,
											RecordType,
											InAttendanceRecordID,
											InWaived,
											Remark,
											DateInput,
											DateModified,
											InputBy,
											ModifyBy 
										)
										VALUES
										(
											\'' . $StaffID . '\',
											\'' . $TargetDateArray[2] . '\',
											\'' . $this->Get_Safe_Sql_Query($SlotName[$TimeSlotName[$i]]) . '\',
											\'0\',
											\'' . $SlotStart[$TimeSlotName[$i]] . '\',
											\'' . $SlotEnd[$TimeSlotName[$i]] . '\',
											\'' . $SlotDutyCount[$TimeSlotName[$i]] . '\',
											\'' . $SlotInWavie[$TimeSlotName[$i]] . '\',
											\'' . $SlotOutWavie[$TimeSlotName[$i]] . '\',
											\'' . CARD_STATUS_HOLIDAY . '\',
											\'' . CARD_STATUS_HOLIDAY . '\',
											\'' . CARD_STATUS_HOLIDAY . '\',
											\'' . $ProfileID . '\',
											' . $ReasonDefaultWavie . ',
											' . $RemarkValue . ',
											NOW(),
											NOW(),
											\'' . $inputBy . '\',
											\'' . $inputBy . '\'
										)
										On Duplicate Key Update 
											RecordID = LAST_INSERT_ID(RecordID), 
											Duty = \'0\', 
											InSchoolStatus = \'' . CARD_STATUS_HOLIDAY . '\',
											OutSchoolStatus = \'' . CARD_STATUS_HOLIDAY . '\',
											RecordType = \'' . CARD_STATUS_HOLIDAY . '\',
											InAttendanceRecordID = \'' . $ProfileID . '\',
											InWaived = ' . $ReasonDefaultWavie . ',
											Remark=' . $RemarkValue . ',
											DateModified = NOW(),
											ModifyBy=\'' . $inputBy . '\' 
										';
					$Result['InsertHolidayDailyLog:' . $StaffID . '-' . $SlotName[$TimeSlotName[$i]]] = $this->db_db_query($sql);
				}

				// slot outgoing
				for ($j=0; $j< sizeof($OutgoingUser[$TimeSlotName[$i]]); $j++) {
					$StaffID = $OutgoingUser[$TimeSlotName[$i]][$j];
					if(in_array($StaffID,$FullHolidayList) || in_array($StaffID,$FullOutgoingList)){
						continue;
					}
					$ReasonDefaultWavie = $ReasonIDToDefaultWavieArray[$OutgoingReason[$TimeSlotName[$i]][$StaffID]]==1? "1":"NULL";
					$RemarkValue = 'NULL';
					if(isset($OrgCardlogData[$StaffID][$SlotName[$TimeSlotName[$i]]])){
						$RemarkValue = '\''.$this->Get_Safe_Sql_Query($OrgCardlogData[$StaffID][$SlotName[$TimeSlotName[$i]]]).'\'';
					}
					
					$sql = 'insert into CARD_STAFF_ATTENDANCE2_PROFILE (
										StaffID,
										ReasonID,
										RecordDate,
										RecordType,
										Waived,
										DateInput,
										DateModified
										)
									values (
										\''.$StaffID.'\',
										\''.$this->Get_Safe_Sql_Query($OutgoingReason[$TimeSlotName[$i]][$StaffID]).'\',
										\''.$TargetDate.'\',
										\''.CARD_STATUS_OUTGOING.'\',
										'.$ReasonDefaultWavie.',
										NOW(),
										NOW()
									)
								';
					$Result['InsertOutgoingProfile:'.$StaffID.'-'.$SlotName[$TimeSlotName[$i]]] = $this->db_db_query($sql);
					$ProfileID = $this->db_insert_id();


					$sql = 'INSERT INTO '.$DailyLogTable.'
										(
											StaffID, 
											DayNumber,
											SlotName,
											Duty,
											DutyStart,
											DutyEnd,
											DutyCount,
											InWavie,
											OutWavie,
											InSchoolStatus,
											OutSchoolStatus,
											RecordType,
											InAttendanceRecordID,
											InWaived,
											Remark,
											DateInput,
											DateModified,
											InputBy,
											ModifyBy 
										)
										VALUES
										(
											\''.$StaffID.'\',
											\''.$TargetDateArray[2].'\',
											\''.$this->Get_Safe_Sql_Query($SlotName[$TimeSlotName[$i]]).'\',
											\'0\',
											\''.$SlotStart[$TimeSlotName[$i]].'\',
											\''.$SlotEnd[$TimeSlotName[$i]].'\',
											\''.$SlotDutyCount[$TimeSlotName[$i]].'\',
											\''.$SlotInWavie[$TimeSlotName[$i]].'\',
											\''.$SlotOutWavie[$TimeSlotName[$i]].'\',
											\''.CARD_STATUS_OUTGOING.'\',
											\''.CARD_STATUS_OUTGOING.'\',
											\''.CARD_STATUS_OUTGOING.'\',
											\''.$ProfileID.'\',
											'.$ReasonDefaultWavie.',
											'.$RemarkValue.',
											NOW(),
											NOW(),
											\''.$inputBy.'\',
											\''.$inputBy.'\' 
										)
										On Duplicate Key Update 
											RecordID = LAST_INSERT_ID(RecordID), 
											Duty = \'0\', 
											InSchoolStatus = \''.CARD_STATUS_OUTGOING.'\',
											OutSchoolStatus = \''.CARD_STATUS_OUTGOING.'\',
											RecordType = \''.CARD_STATUS_OUTGOING.'\',
											InAttendanceRecordID = \''.$ProfileID.'\',
											InWaived = '.$ReasonDefaultWavie.',
											Remark='.$RemarkValue.', 
											DateModified = NOW(),
											ModifyBy=\''.$inputBy.'\' 
										';
					$Result['InsertOutgoingDailyLog:'.$StaffID.'-'.$SlotName[$TimeSlotName[$i]]] = $this->db_db_query($sql);
				}
			}
			
			// add CARD_STATUS_NOSETTING dummy daily log record for staff that end up without duty
			for ($i=0; $i< sizeof($TargetUserList); $i++) {
				if (!in_array($TargetUserList[$i],$StaffOccupied)) {
					$sql = "insert into ".$DailyLogTable."
									(
										StaffID, 
										DayNumber,
										Duty,
										InSchoolStatus,
										OutSchoolStatus,
										RecordType,
										DateInput,
										DateModified,
										InputBy,
										ModifyBy
									)
									VALUES
									(
										'".$TargetUserList[$i]."',
										'".$TargetDateArray[2]."',
										'0',
										'".CARD_STATUS_NOSETTING."',
										'".CARD_STATUS_NOSETTING."',
										'".CARD_STATUS_NOSETTING."',
										NOW(),
										NOW(),
										'$inputBy',
										'$inputBy' 
									)";
					$this->db_db_query($sql);				
				}
			}
			
			// get all raw log of users on that day
			$sql = 'select 
								UserID,
								CONCAT(\''.$TargetDate.' \', RecordTime) as RecordTime,
								RecordStation 
							from 
								'.$RawLogTable.' 
							where 
								DayNumber = \''.$TargetDateArray[2].'\' 
								AND 
								UserID in ('.implode(',',$TargetUserList).') 
							order by 
								UserID, RecordTime
						';
			$TmpRawLogDetail = $this->returnArray($sql);
			$RawLogDetail = $OrgCardlogTimes;
			$RealRawlogUserIdTimeMap = array();
			for($i=0;$i<count($TmpRawLogDetail);$i++){
				$RealRawlogUserIdTimeMap[$TmpRawLogDetail[$i]['UserID']][$TmpRawLogDetail[$i]['RecordTime']] = true;
				if(!isset($OrgCardlogUserIdTimeMap[$TmpRawLogDetail[$i]['UserID']][$TmpRawLogDetail[$i]['RecordTime']])){
					$RawLogDetail[] = $TmpRawLogDetail[$i];
					$OrgCardlogUserIdTimeMap[$TmpRawLogDetail[$i]['UserID']][$TmpRawLogDetail[$i]['RecordTime']] = true;
				}
			}

			global $CmpField, $CmpField2;
			$CmpField = 'UserID';
			$CmpField2 = 'RecordTime';
			$RawLogDetail = array_merge($RawLogDetail,$OrgCardlogTimes);
			usort($RawLogDetail,'USortCmp');
			
			// mark which time records are real from tap card log
			for($i=0;$i<count($RawLogDetail);$i++){
				if(isset($RealRawlogUserIdTimeMap[$RawLogDetail[$i]['UserID']]) 
					&& isset($RealRawlogUserIdTimeMap[$RawLogDetail[$i]['UserID']][$RawLogDetail[$i]['RecordTime']])){
					$RawLogDetail[$i]['IsRealRawLog'] = true;
				}else{
					$RawLogDetail[$i]['IsRealRawLog'] = false;
				}
			}
			
			// remove the raw log of users
			$sql = 'delete from 
								'.$RawLogTable.' 
							where 
								DayNumber = \''.$TargetDateArray[2].'\' 
								AND 
								UserID in ('.implode(',',$TargetUserList).') 
						 ';
			$Result['RemoveRawLog'] = $this->db_db_query($sql);
			
			$tappedRecords = array();
			// simulate the tap card process again
			for ($i=0; $i< sizeof($RawLogDetail); $i++) {
				if(!isset($tappedRecords[$RawLogDetail[$i]['UserID']][$RawLogDetail[$i]['RecordTime']])){
					$TimeStr = strtotime($RawLogDetail[$i]['RecordTime']);
					$Result['RecordTapCard:'.$RawLogDetail[$i]['UserID'].'-'.$RawLogDetail[$i]['RecordTime']] = $this->Record_Tap_Card($RawLogDetail[$i]['UserID'],$TimeStr,$RawLogDetail[$i]['RecordStation'],$RawLogDetail[$i]['IsRealRawLog'],false,$inputBy);
					$tappedRecords[$RawLogDetail[$i]['UserID']][$RawLogDetail[$i]['RecordTime']] = 1;
				}
			}
			
			// if there are time slots that are overnight, process next day's first tap card time
			if(count($overnight_duty_userid)>0)
			{
				$sql = 'select 
								UserID,
								CONCAT(\''.$overnight_tomorrow_date.' \',RecordTime) as RecordTime,
								RecordStation 
							from 
								'.$overnight_tomorrow_rawlog_table.' 
							where 
								DayNumber = \''.$overnight_tomorrow_day.'\' 
								AND 
								UserID in ('.implode(',',$overnight_duty_userid).') 
							group by UserID 
							order by 
								UserID, RecordTime
						';
				$overnight_rawlog_records = $this->returnResultSet($sql);
				for($i=0;$i<sizeof($overnight_rawlog_records);$i++){
					//$sql = "DELETE FROM $overnight_tomorrow_rawlog_table WHERE RecordID='".$overnight_rawlog_records[$i]['RecordID']."'";
					//$this->db_db_query($sql);
					if(!isset($OrgCardlogUserIdTimeMap[$overnight_rawlog_records[$i]['UserID']][$overnight_rawlog_records[$i]['RecordTime']])){
						$TimeStr = strtotime($overnight_rawlog_records[$i]['RecordTime']);
						$Result['RecordTapCard:'.$overnight_rawlog_records[$i]['UserID'].'-'.$overnight_rawlog_records[$i]['RecordTime']] = $this->Record_Tap_Card($overnight_rawlog_records[$i]['UserID'],$TimeStr,$overnight_rawlog_records[$i]['RecordStation'],false,true,$inputBy);
						$OrgCardlogUserIdTimeMap[$overnight_rawlog_records[$i]['UserID']][$overnight_rawlog_records[$i]['RecordTime']] = true;
					}
				}
			}
			
			// find the absent suspected daily log records to remap absent status to it
			if(count($OrgCardlogAbsent)>0)
			{
				$absent_staff_id_ary = array_keys($OrgCardlogAbsent);
				$sql = "SELECT RecordID,StaffID,SlotName,DutyCount FROM ".$DailyLogTable." WHERE StaffID IN (".implode(",",$absent_staff_id_ary).") AND DayNumber='".$TargetDateArray[2]."' AND Duty='1' AND SlotName IS NOT NULL AND InSchoolStatus IS NULL AND OutSchoolStatus IS NULL";
				$absent_suspected_records = $this->returnResultSet($sql);
				$absent_suspected_records_size = count($absent_suspected_records);
				for($i=0;$i<$absent_suspected_records_size;$i++){
					$tmp_record_id = $absent_suspected_records[$i]['RecordID'];
					$tmp_staff_id = $absent_suspected_records[$i]['StaffID'];
					$tmp_slotname = $absent_suspected_records[$i]['SlotName'];
					if(isset($OrgCardlogAbsent[$tmp_staff_id]) && isset($OrgCardlogAbsent[$tmp_staff_id][$tmp_slotname]))
					{
						$absent_profile_id = $this->Create_Profile($TargetDate,$tmp_staff_id,"",$absent_suspected_records[$i]['DutyCount'],CARD_STATUS_ABSENT);
						if($absent_profile_id > 0)
						{
							$sql = "UPDATE ".$DailyLogTable." 
									SET 
										StaffPresent = '0',
										InSchoolStatus = '".CARD_STATUS_ABSENT."',
										OutSchoolStatus = NULL,
										InAttendanceRecordID = '$absent_profile_id',
										OutAttendanceRecordID = NULL 
									WHERE RecordID = '$tmp_record_id'";
							$this->db_db_query($sql);
						}
					}
				}
			}
			
			// try to recover reason of late/ absent from the old duty settings
			$NewDuty = $this->Get_Duty_By_Date($Obj,$TargetDate,false);
			foreach ($OrgDuty['CardUserToDuty'] as $StaffID => $DutyDetail) {
				for ($i=0; $i< sizeof($DutyDetail); $i++) {
					$MatchedDuty = $NewDuty['CardUserToDutyStartToDuty'][$StaffID][$DutyDetail[$i]['DutyStart']];
					if (is_array($MatchedDuty)) { 
						if ($MatchedDuty['InSchoolStatus'] == $DutyDetail[$i]['InSchoolStatus'] 
								&& ($MatchedDuty['InSchoolStatus'] == CARD_STATUS_LATE || 
								$MatchedDuty['InSchoolStatus'] == CARD_STATUS_ABSENT)) {
							$sql = 'update CARD_STAFF_ATTENDANCE2_PROFILE set 
												ReasonID = \''.$DutyDetail[$i]['InReasonID'].'\', 
												Waived = \''.$DutyDetail[$i]['InWavied'].'\' 
											where 
												RecordID = \''.$MatchedDuty['InAttendanceRecordID'].'\'';
							$Result['RecoverReasonIn:'.$StaffID.'-'.$MatchedDuty['SlotName']] = $this->db_db_query($sql);
							
							$sql = "UPDATE $DailyLogTable SET InWaived='".$DutyDetail[$i]['InWavied']."' WHERE DayNumber='".$TargetDateArray[2]."' AND StaffID='".$StaffID."' AND InAttendanceRecordID='".$MatchedDuty['InAttendanceRecordID']."'";
							$Result['RecoverInWaiveDailylog-'.$StaffID.'-'.$MatchedDuty['SlotName']] = $this->db_db_query($sql);
						}
					}
					
					$MatchedDuty = $NewDuty['CardUserToDutyEndToDuty'][$StaffID][$DutyDetail[$i]['DutyEnd']];
					if (is_array($MatchedDuty)) {
						if ($MatchedDuty['OutSchoolStatus'] == $DutyDetail[$i]['OutSchoolStatus'] 
								&& $MatchedDuty['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE) {
							$sql = 'update CARD_STAFF_ATTENDANCE2_PROFILE set 
												ReasonID = \''.$DutyDetail[$i]['OutReasonID'].'\', 
												Waived = \''.$DutyDetail[$i]['OutWavied'].'\'  
											where 
												RecordID = \''.$MatchedDuty['OutAttendanceRecordID'].'\'';
							$Result['RecoverReasonOut:'.$StaffID.'-'.$MatchedDuty['SlotName']] = $this->db_db_query($sql);
							
							$sql = "UPDATE $DailyLogTable SET OutWaived='".$DutyDetail[$i]['OutWavied']."' WHERE DayNumber='".$TargetDateArray[2]."' AND StaffID='".$StaffID."' AND OutAttendanceRecordID='".$MatchedDuty['OutAttendanceRecordID']."'";
							$Result['RecoverOutWaiveDailylog-'.$StaffID.'-'.$MatchedDuty['SlotName']] = $this->db_db_query($sql);
						}
					}
				}
			}
		}


		if ($this->isRecordChangeLogEnabled()) {
			if(is_array($OrgDuty['CardUserToDuty'])) {
				foreach ($OrgDuty['CardUserToDuty'] as $StaffID => $DutyDetail) {
					for ($i = 0; $i < count($DutyDetail); $i++) {
						$detail = $DutyDetail[$i];
						$new_detail = false;
						if (is_array($NewDuty['CardUserToDuty']) && is_array($NewDuty['CardUserToDuty'][$StaffID])) {
							foreach ($NewDuty['CardUserToDuty'][$StaffID] as $temp) {
								if ($detail['SlotName'] == $temp['SlotName']) {
									$new_detail = $temp;
									break;
								}
							}
						}

						$from_outgoing = (is_array($OrgDuty['CardOutgoingSlotToUser']) && is_array($OrgDuty['CardOutgoingSlotToUser'][$detail['SlotName']]) && in_array($StaffID, $OrgDuty['CardOutgoingSlotToUser'][$detail['SlotName']])) ? true : false;
						$from_holiday = (is_array($OrgDuty['CardHolidaySlotToUser']) && is_array($OrgDuty['CardHolidaySlotToUser'][$detail['SlotName']]) && in_array($StaffID, $OrgDuty['CardHolidaySlotToUser'][$detail['SlotName']])) ? true : false;

						$from_duty = $Lang['StaffAttendance']['OnDuty'];
						if ($from_outgoing) {
							$from_duty = $Lang['StaffAttendance']['Outgoing'];
						} elseif ($from_holiday) {
							$from_duty = $Lang['StaffAttendance']['Leave'];
						}

						$to_outgoing = (is_array($NewDuty['CardOutgoingSlotToUser']) && is_array($NewDuty['CardOutgoingSlotToUser'][$detail['SlotName']]) && in_array($StaffID, $NewDuty['CardOutgoingSlotToUser'][$detail['SlotName']])) ? true : false;
						$to_holiday = (is_array($NewDuty['CardHolidaySlotToUser']) && is_array($NewDuty['CardHolidaySlotToUser'][$detail['SlotName']]) && in_array($StaffID, $NewDuty['CardHolidaySlotToUser'][$detail['SlotName']])) ? true : false;

						$to_duty = $Lang['StaffAttendance']['OnDuty'];
						if ($to_outgoing) {
							$to_duty = $Lang['StaffAttendance']['Outgoing'];
						} elseif ($to_holiday) {
							$to_duty = $Lang['StaffAttendance']['Leave'];
						}

						if ($new_detail == false) {
							$temp_result = $this->getAttendanceRecordStatus($StaffID, $detail['InSchoolStatus'], $detail['OutSchoolStatus']);
							$StaffName = $temp_result[0]['StaffName'];
							$orgStatus = $temp_result[0]['Status'];
							$RecordDetail = array();
							$RecordDetail['Action'] = 'Delete';
							$RecordDetail['Name'] = $StaffName;
							$RecordDetail['Date'] = $TargetDate;
							$RecordDetail['Time_Slot'] = $detail['SlotName'].'('.$detail['DutyStart'].'-'.$detail['DutyEnd'].')';
							$RecordDetail['From_Status'] = $StatusList[$orgStatus]['StatusDisplay'];
							$RecordDetail['From_Duty'] = $from_duty;
							$Result['AttendanceLog:' . $StaffID . $TargetDate . $detail['SlotName']] = $this->INSERT_UPDATE_LOG('Special_Duty', $RecordDetail, $detail['RecordID']);
							continue;
						}


						$temp_result = $this->getAttendanceRecordStatus($StaffID, $detail['InSchoolStatus'], $detail['OutSchoolStatus']);
						$StaffName = $temp_result[0]['StaffName'];
						$orgStatus = $temp_result[0]['Status'];

						$RecordDetail = array();
						$RecordDetail['Action'] = 'Move';
						$RecordDetail['Name'] = $StaffName;
						$RecordDetail['Date'] = $TargetDate;
						$RecordDetail['Time_Slot'] = $detail['SlotName'].'('.$detail['DutyStart'].'-'.$detail['DutyEnd'].')';
						$have_update = false;
						if ($detail['InSchoolStatus'] != $new_detail['InSchoolStatus']
							|| $detail['OutSchoolStatus'] != $new_detail['OutSchoolStatus']) {
							$temp_result = $this->getAttendanceRecordStatus($StaffID, $new_detail['InSchoolStatus'], $new_detail['OutSchoolStatus']);
							$newStatus = $temp_result[0]['Status'];
							if ($orgStatus != $newStatus) {
								$RecordDetail['From_Status'] = $StatusList[$orgStatus]['StatusDisplay'];
								$RecordDetail['To_Status'] = $StatusList[$newStatus]['StatusDisplay'];
								$have_update = true;
							}
						}
						if ($detail['Duty'] != $new_detail['Duty']) {
							$RecordDetail['From_Duty'] = $from_duty;
							$RecordDetail['To_Duty'] = $to_duty;
							$have_update = true;
						}

						if ($have_update) {
							$Result['AttendanceLog:' . $StaffID . $TargetDate . $detail['SlotName']] = $this->INSERT_UPDATE_LOG('Special_Duty', $RecordDetail, $detail['RecordID']);
						}
					}
				}
			}

			if(is_array($NewDuty['CardUserToDuty'])) {
				foreach ($NewDuty['CardUserToDuty'] as $StaffID => $DutyDetail) {
					for ($i = 0; $i < count($DutyDetail); $i++) {
						$detail = $DutyDetail[$i];
						$old_detail = false;
						if (is_array($OrgDuty['CardUserToDuty']) && is_array($OrgDuty['CardUserToDuty'][$StaffID])) {
							foreach ($OrgDuty['CardUserToDuty'][$StaffID] as $temp) {
								if ($detail['SlotName'] == $temp['SlotName']) {
									$old_detail = $temp;
									break;
								}
							}
						}

						$to_outgoing = (is_array($NewDuty['CardOutgoingSlotToUser']) && is_array($NewDuty['CardOutgoingSlotToUser'][$detail['SlotName']]) && in_array($StaffID, $NewDuty['CardOutgoingSlotToUser'][$detail['SlotName']])) ? true : false;
						$to_holiday = (is_array($NewDuty['CardHolidaySlotToUser']) && is_array($NewDuty['CardHolidaySlotToUser'][$detail['SlotName']]) && in_array($StaffID, $NewDuty['CardHolidaySlotToUser'][$detail['SlotName']])) ? true : false;

						$to_duty = $Lang['StaffAttendance']['OnDuty'];
						if ($to_outgoing) {
							$to_duty = $Lang['StaffAttendance']['Outgoing'];
						} elseif ($to_holiday) {
							$to_duty = $Lang['StaffAttendance']['Leave'];
						}

						if ($old_detail == false) {
							$temp_result = $this->getAttendanceRecordStatus($StaffID, $detail['InSchoolStatus'], $detail['OutSchoolStatus']);
							$StaffName = $temp_result[0]['StaffName'];
							$newStatus = $temp_result[0]['Status'];
							$RecordDetail = array();
							$RecordDetail['Action'] = 'Create';
							$RecordDetail['Name'] = $StaffName;
							$RecordDetail['Date'] = $TargetDate;
							$RecordDetail['Time_Slot'] = $detail['SlotName'].'('.$detail['DutyStart'].'-'.$detail['DutyEnd'].')';
							$RecordDetail['To_Status'] = $StatusList[$newStatus]['StatusDisplay'];
							$RecordDetail['To_Duty'] = $to_duty;
							$Result['AttendanceLog:' . $StaffID . $TargetDate . $detail['SlotName']] = $this->INSERT_UPDATE_LOG('Special_Duty', $RecordDetail, $detail['RecordID']);
							continue;
						}
					}
				}
			}
		}


    //debug_r($Result);
    return !in_array(false,$Result);
	}

	function getAttendanceRecordStatus($StaffID, $InSchoolStatus, $OutSchoolStatus)
	{
		global $Lang;
		$CARD_STATUS_LATEEARLYLEAVE = 6;
		$CARD_STATUS_ABSENT_SUSPECTED = 7;
		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;

		$name_field = getNameFieldByLang("a.");
		$sql = "SELECT 
					  a.UserID,
				  $name_field as StaffName,
				(CASE 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_OUTGOING."' 
				  	THEN '".CARD_STATUS_OUTGOING."' 
				  WHEN  h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' 
					THEN '".CARD_STATUS_HOLIDAY."' 
				  WHEN (h.InSchoolStatus = '".CARD_STATUS_LATE."' AND h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."') 
					THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_LATE."' 
					THEN '".CARD_STATUS_LATE."' 
				  WHEN h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' 
					THEN '".CARD_STATUS_EARLYLEAVE."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_ABSENT."' 
					THEN '".CARD_STATUS_ABSENT."'
				  WHEN h.InSchoolStatus = '".CARD_STATUS_NORMAL."' OR h.OutSchoolStatus = '".CARD_STATUS_NORMAL."' 
					THEN '".CARD_STATUS_NORMAL."' 
				  ELSE '".$CARD_STATUS_DEFAULT."' 
				  END) as Status
FROM 
				  INTRANET_USER as a, 
				  (SELECT '$InSchoolStatus' as InSchoolStatus, '$OutSchoolStatus' as OutSchoolStatus) as h
WHERE
				  a.RecordType='1'
				  AND a.RecordStatus = '1'
				  AND a.UserID='$StaffID'";

		return $this->returnResultSet($sql);
	}


	function Save_Shifting_Duty($ID,$TargetDate,$TimeSlotID,$TimeSlotUser,$HolidayUser,$OutgoingUser,$OutgoingReason,$HolidayReason,$GroupOrIndividual="Group",$SetNonSchoolDay=0,$AttendOne=0) {
		global $sys_custom, $Lang;
		$TargetDateCond = '\''.implode("','",$TargetDate).'\'';

		if ($GroupOrIndividual == "Group") {
			$Obj = new staff_attend_group($ID,true);
		}
		else {
			$Obj = $ID;
		}

		if ($this->isRecordChangeLogEnabled()) {
			$OrgDuty = array();
			for ($i = 0; $i < sizeof($TargetDate); $i++) {
				$OrgDuty[$TargetDate[$i]] = $this->Get_Duty_By_Date($Obj, $TargetDate[$i], false);
			}
		}


		if ($GroupOrIndividual == "Group") {
			// delete leave record
			$sql = 'delete lr from 
							CARD_STAFF_ATTENDANCE_USERGROUP ug 
							inner join 
							CARD_STAFF_ATTENDANCE2_LEAVE_RECORD lr 
							on 
								ug.GroupID = \''.$ID.'\' 
								and 
								ug.UserID = lr.StaffID 
								and 
								lr.RecordDate in ('.$TargetDateCond.') 
								and 
								lr.SlotID IS NOT NULL
						 ';
			//debug_r($sql);
			$Result['Delete:CARD_STAFF_ATTENDANCE2_LEAVE_RECORD'] = $this->db_db_query($sql);
			
			// delete shift date duty
			$sql = 'delete sdd from 
							CARD_STAFF_ATTENDANCE_USERGROUP ug 
							inner join 
							CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY sdd 
							on 
								ug.GroupID = \''.$ID.'\' 
								and 
								ug.UserID = sdd.UserID 
								and 
								sdd.DutyDate in ('.$TargetDateCond.')
						 ';
			//debug_r($sql);
			$Result['Delete:CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY'] = $this->db_db_query($sql);
			if($sys_custom['StaffAttendance']['LogDeleteDuty']){
				$this->Log_Record($sql);
			}
			// Prepare variable for NonSchoolDaySetting
			$NonSchoolDayTableName = "CARD_STAFF_ATTENDANCE2_GROUP_DATE_DUTY";
			$IDField = "GroupID";
		}
		else {
			// delete leave record
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
							where 
								StaffID = \''.$ID.'\'
								and 
								RecordDate in ('.$TargetDateCond.') 
								and 
								SlotID IS NOT NULL
						 ';
			//debug_r($sql);
			$Result['Delete:CARD_STAFF_ATTENDANCE2_LEAVE_RECORD'] = $this->db_db_query($sql);
			
			// delete shift date duty
			$sql = 'delete from CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY 
							where 
								UserID = \''.$ID.'\'
								and 
								DutyDate in ('.$TargetDateCond.')
						 ';
			//debug_r($sql);
			$Result['Delete:CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY'] = $this->db_db_query($sql);
			if($sys_custom['StaffAttendance']['LogDeleteDuty']){
				$this->Log_Record($sql);
			}
			// Prepare variable for NonSchoolDaySetting
			$NonSchoolDayTableName = "CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY";
			$IDField = "UserID";
		}
		for ($i=0; $i< sizeof($TargetDate); $i++) {
			// insert duties
			foreach ($TimeSlotUser as $TimeSlotID => $StaffList) {
				for ($j=0; $j < sizeof($StaffList); $j++) {
					$sql = 'insert into CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY (
										UserID,
										DutyDate,
										SlotID,';
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$sql .= 'AttendOne,';
						}
								$sql.='DateInput,
										InputBy,
										DateModify,
										ModifyBy)
									values (
										\''.$StaffList[$j].'\',
										\''.$TargetDate[$i].'\',
										\''.$TimeSlotID.'\',';
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$sql .= '\''.$AttendOne.'\',';
						}
								$sql .='NOW(),
										\''.$_SESSION['UserID'].'\',
										NOW(),
										\''.$_SESSION['UserID'].'\'
										)
									on duplicate key update 
										DateModify = NOW(),
										ModifyBy = \''.$_SESSION['UserID'].'\'';
					//debug_r($sql);
					$Result['Insert:CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY:'.$StaffList[$j].'-'.$TargetDate[$i].'-'.$TimeSlotID] = $this->db_db_query($sql);
				}
			}
			
			// insert holiday
			foreach ($HolidayUser as $TimeSlotID => $StaffList) {
				for ($j=0; $j < sizeof($StaffList); $j++) {
					$sql = 'insert into CARD_STAFF_ATTENDANCE2_LEAVE_RECORD (
										StaffID,
										RecordDate,
										SlotID,
										ReasonID,
										RecordType,
										DateInput,
										InputBy,
										DateModified,
										ModifyBy)
									values (
										\''.$StaffList[$j].'\',
										\''.$TargetDate[$i].'\',
										\''.$TimeSlotID.'\',
										'.(($HolidayReason[$TimeSlotID][$StaffList[$j]] != "")? '\''.$HolidayReason[$TimeSlotID][$StaffList[$j]].'\'':'NULL').',
										\''.CARD_STATUS_HOLIDAY.'\',
										NOW(),
										\''.$_SESSION['UserID'].'\',
										NOW(),
										\''.$_SESSION['UserID'].'\'
										)
									on duplicate key update 
										DateModified = NOW(),
										ModifyBy = \''.$_SESSION['UserID'].'\'';
					//debug_r($sql);
					$Result['Insert:Holiday:'.$StaffList[$j].'-'.$TargetDate[$i].'-'.$TimeSlotID] = $this->db_db_query($sql);
				}
			}
			
			// insert Outgoing
			foreach ($OutgoingUser as $TimeSlotID => $StaffList) {
				for ($j=0; $j < sizeof($StaffList); $j++) {
					$sql = 'insert into CARD_STAFF_ATTENDANCE2_LEAVE_RECORD (
										StaffID,
										RecordDate,
										SlotID,
										ReasonID,
										RecordType,
										DateInput,
										InputBy,
										DateModified,
										ModifyBy)
									values (
										\''.$StaffList[$j].'\',
										\''.$TargetDate[$i].'\',
										\''.$TimeSlotID.'\',
										'.(($OutgoingReason[$TimeSlotID][$StaffList[$j]] != "")? '\''.$OutgoingReason[$TimeSlotID][$StaffList[$j]].'\'':'NULL').',
										\''.CARD_STATUS_OUTGOING.'\',
										NOW(),
										\''.$_SESSION['UserID'].'\',
										NOW(),
										\''.$_SESSION['UserID'].'\'
										)
									on duplicate key update 
										DateModified = NOW(),
										ModifyBy = \''.$_SESSION['UserID'].'\'';
					//debug_r($sql);
					$Result['Insert:Outgoing:'.$StaffList[$j].'-'.$TargetDate[$i].'-'.$TimeSlotID] = $this->db_db_query($sql);
				}
			}
			
			if ($SetNonSchoolDay) {
				$sql = "REPLACE INTO ".$NonSchoolDayTableName." (
									".$IDField.",
									DutyDate,
									Duty,
									DateInput,
									DateModified
									) 
								values (
									'".$ID."',
									'".$TargetDate[$i]."',
									'0',
									NOW(),
									NOW()
									)";	
			}
			else {
				$sql = "DELETE from ".$NonSchoolDayTableName." 
								where 
									".$IDField." = '".$ID."' 
									and 
									DutyDate = '".$TargetDate[$i]."'";
			}
			$Result['SetDeleteNonSchoolDay:'.$TargetDate[$i]] = $this->db_db_query($sql);
		}


		if($this->isRecordChangeLogEnabled()) {
			$NewDuty = array();
			for ($i = 0; $i < sizeof($TargetDate); $i++) {
				$NewDuty[$TargetDate[$i]] = $this->Get_Duty_By_Date($Obj, $TargetDate[$i], false);
			}

			$Obj = new staff_attend_group($ID,true);
			$RecordDetail = array();
			$RecordDetail[] = 'Group Name: '.$Obj->GroupName;
			$RecordDetail[] = "";

			$record_delete = array();
			$record_move = array();
			$record_create = array();

			$slot_ids = array();
			$staff_ids = array();
			foreach($OrgDuty as $date=>$date_duty_records) {
				foreach($date_duty_records['UserToSlotDuty'] as $StaffID=>$slots) {
					$new_duty = $NewDuty[$date];
					foreach($slots as $slotID) {
						$from_outgoing = (is_array($date_duty_records['SlotToUserOutgoing']) && is_array($date_duty_records['SlotToUserOutgoing'][$slotID]) && in_array($StaffID, $date_duty_records['SlotToUserOutgoing'][$slotID])) ? true : false;
						$from_holiday = (is_array($date_duty_records['SlotToUserHoliday']) && is_array($date_duty_records['SlotToUserHoliday'][$slotID]) && in_array($StaffID, $date_duty_records['SlotToUserHoliday'][$slotID])) ? true : false;

						$move = "From: ";
						if($from_outgoing) {
							$move .= $Lang['StaffAttendance']['Outgoing'];
						} elseif($from_holiday) {
							$move .= $Lang['StaffAttendance']['Leave'];
						} else {
							$move .= $Lang['StaffAttendance']['OnDuty'];
						}

						if(is_array($new_duty['UserToSlotDuty'])) {
							if(is_array($new_duty['UserToSlotDuty'][$StaffID])) {
								if(!in_array($slotID, $new_duty['UserToSlotDuty'][$StaffID])) {
									$record_delete[$date][$slotID][] = array("StaffID"=>$StaffID,"delete"=>$move);
									$slot_ids[] = $slotID;
									$staff_ids[] = $StaffID;
									continue;
								}
							}
						}

						if(is_array($new_duty['StaffWithoutSetting'])) {
							if (in_array($StaffID, $new_duty['StaffWithoutSetting'])) {
								$record_delete[$date][$slotID][] = array("StaffID"=>$StaffID,"delete"=>$move);
								$slot_ids[] = $slotID;
								$staff_ids[] = $StaffID;
								continue;
							}
						}


						$to_outgoing = (is_array($new_duty['SlotToUserOutgoing']) && is_array($new_duty['SlotToUserOutgoing'][$slotID]) && in_array($StaffID, $new_duty['SlotToUserOutgoing'][$slotID])) ? true : false;
						$to_holiday = (is_array($new_duty['SlotToUserHoliday']) && is_array($new_duty['SlotToUserHoliday'][$slotID]) && in_array($StaffID, $new_duty['SlotToUserHoliday'][$slotID])) ? true : false;


						if($from_outgoing == $to_outgoing && $from_holiday == $to_holiday) {
							continue;
						}



						$move .= " To: ";
						if($to_outgoing) {
							$move .= $Lang['StaffAttendance']['Outgoing'];
						} elseif($to_holiday) {
							$move .= $Lang['StaffAttendance']['Leave'];
						} else {
							$move .= $Lang['StaffAttendance']['OnDuty'];
						}

						$record_move[$date][$slotID][] = array("StaffID"=>$StaffID,"move"=>$move);
						$slot_ids[] = $slotID;
						$staff_ids[] = $StaffID;
					}
				}
			}

			foreach($NewDuty as $date=>$date_duty_records) {
				foreach ($date_duty_records['UserToSlotDuty'] as $StaffID => $slots) {
					$old_duty = $OrgDuty[$date];
					foreach($slots as $slotID) {
						$is_new_create = (!is_array($old_duty['SlotToUserDuty']) || !is_array($old_duty['SlotToUserDuty'][$slotID]) || !in_array($StaffID, $old_duty['SlotToUserDuty'][$slotID])) ? true : false;
						if(!$is_new_create) {
							continue;
						}

						$to_outgoing = (is_array($date_duty_records['SlotToUserOutgoing']) && is_array($date_duty_records['SlotToUserOutgoing'][$slotID]) && in_array($StaffID, $date_duty_records['SlotToUserOutgoing'][$slotID])) ? true : false;
						$to_holiday = (is_array($date_duty_records['SlotToUserHoliday']) && is_array($date_duty_records['SlotToUserHoliday'][$slotID]) && in_array($StaffID, $date_duty_records['SlotToUserHoliday'][$slotID])) ? true : false;

						$create = "To: ";
						if($to_outgoing) {
							$create .= $Lang['StaffAttendance']['Outgoing'];
						} elseif($to_holiday) {
							$create .= $Lang['StaffAttendance']['Leave'];
						} else {
							$create .= $Lang['StaffAttendance']['OnDuty'];
						}

						$record_create[$date][$slotID][] = array("StaffID"=>$StaffID,"create"=>$create);
						$slot_ids[] = $slotID;
						$staff_ids[] = $StaffID;
					}
				}
			}

			$slot_detail = array();
			if(count($slot_ids) > 0) {
				$slot_details = $this->Get_Slot_List($ID, $GroupOrIndividual, $slot_ids);
				foreach($slot_details as $temp) {
					$slot_detail[$temp['SlotID']] = $temp;
				}
			}

			$staff_detail = array();
			if(count($staff_ids) > 0) {
				$name_field = $this->Get_Name_Field("u.");
				$sql = "SELECT 
					u.UserID,
					$name_field as StaffName
				FROM INTRANET_USER as u 
				WHERE u.UserID IN (".implode(',', $staff_ids).")";
				$staff_details = $this->returnResultSet($sql);
				foreach($staff_details as $temp) {
					$staff_detail[$temp['UserID']] = $temp['StaffName'];
				}
			}


			if(count($record_delete) > 0 || count($record_move) > 0 || count($record_create) > 0) {
				if(count($record_delete) > 0) {
					$RecordDetail[] = "Action: Delete";
					foreach($record_delete as $date=>$records) {
						$RecordDetail[] = "Date: ".$date;
						foreach($records as $slotID=>$record) {
							$RecordDetail[] = "Time Slot: ".$slot_detail[$slotID]['SlotName'].'('.$slot_detail[$slotID]['SlotStart'].'-'.$slot_detail[$slotID]['SlotEnd'].')';
							$user_array = array();
							foreach($record as $temp) {
								$RecordDetail[] = "User: " . $staff_detail[$temp['StaffID']] . ", " . $temp['delete'];
							}
						}
					}
					$RecordDetail[] = "";
				}

				if(count($record_move) > 0) {
					$RecordDetail[] = "Action: Move";
					foreach($record_move as $date=>$records) {
						$RecordDetail[] = "Date: ".$date;
						foreach($records as $slotID=>$record) {
							$RecordDetail[] = "Time Slot: ".$slot_detail[$slotID]['SlotName'].'('.$slot_detail[$slotID]['SlotStart'].'-'.$slot_detail[$slotID]['SlotEnd'].')';
							foreach($record as $temp) {
								$RecordDetail[] = "User: " . $staff_detail[$temp['StaffID']] . ", " . $temp['move'];
							}
						}
					}
					$RecordDetail[] = "";
				}

				if(count($record_create) > 0) {
					$RecordDetail[] = "Action: Create";
					foreach($record_create as $date=>$records) {
						$RecordDetail[] = "Date: ".$date;
						foreach($records as $slotID=>$record) {
							$RecordDetail[] = "Time Slot: ".$slot_detail[$slotID]['SlotName'].'('.$slot_detail[$slotID]['SlotStart'].'-'.$slot_detail[$slotID]['SlotEnd'].')';
							foreach($record as $temp) {
								$RecordDetail[] = "User: " . $staff_detail[$temp['StaffID']] . ", " . $temp['create'];
							}
						}
					}
					$RecordDetail[] = "";
				}

				$Result['AttendanceLog:'] = $this->INSERT_UPDATE_LOG('Special_Duty', implode('<br>', $RecordDetail));
			}
		}

		//debug_r($Result);
		return !in_array(false,$Result);
	}
	
	function Save_Full_Day_Setting($StaffID,$RecordType,$StartDate,$EndDate,$ReasonID,$LeaveNature,$Outgoing,$Holiday,$OutgoingReason,$HolidayReason,$TargetDate,$Remark='',$OutgoingRemark=array(),$HolidayRemark=array()) {
		if ($LeaveNature == 'Full') { // full day leave
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
							Where 
								StaffID = \''.$this->Get_Safe_Sql_Query($StaffID).'\' 
								and 
								RecordDate between \''.$this->Get_Safe_Sql_Query($StartDate).'\' and \''.$this->Get_Safe_Sql_Query($EndDate).' 23:59:59\'';
			$Result['DeleteSetting'] = $this->db_db_query($sql);
			
			if ($RecordType != "") {
				$StartTimeStamp = strtotime($StartDate);
				$EndTimeStamp = strtotime($EndDate);
				for ($i=$StartTimeStamp; $i<= $EndTimeStamp; $i+=86400) {
					$sql = 'insert into CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
										(
										StaffID,
										RecordDate,
										RecordType,
										ReasonID,
										Remark,
										DateInput,
										InputBy,
										DateModified,
										ModifyBy
										)
									values 
										(
										\''.$this->Get_Safe_Sql_Query($StaffID).'\',
										\''.date('Y-m-d',$i).'\',
										\''.$this->Get_Safe_Sql_Query($RecordType).'\',
										\''.$this->Get_Safe_Sql_Query($ReasonID).'\',
										\''.$this->Get_Safe_Sql_Query($Remark).'\',
										NOW(),
										\''.$_SESSION['UserID'].'\',
										NOW(),
										\''.$_SESSION['UserID'].'\'
										)
									on duplicate key update 
										RecordType = \''.$this->Get_Safe_Sql_Query($RecordType).'\',
										DateModified = NOW(),
										ModifyBy = \''.$_SESSION['UserID'].'\'';
					//debug_r($sql);
					$Result['Insert:'.date('Y-m-d',$i)] = $this->db_db_query($sql);
				}
			}
		}
		else { // slot leave
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
							where 
								StaffID = \''.$this->Get_Safe_Sql_Query($StaffID).'\' 
								and 
								RecordDate = \''.$this->Get_Safe_Sql_Query($TargetDate).'\'';
			$Result['DeleteLeaveSetting'] = $this->db_db_query($sql);
			
			for ($i=0; $i < sizeof($Outgoing); $i++) {
				$sql = 'insert into CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
								(
									StaffID,
									RecordDate,
									RecordType,
									ReasonID,
									Remark,
									SlotID,
									DateInput,
									InputBy,
									DateModified,
									ModifyBy
									)
								values  
								(
									\''.$this->Get_Safe_Sql_Query($StaffID).'\',
									\''.$this->Get_Safe_Sql_Query($TargetDate).'\',
									\''.CARD_STATUS_OUTGOING.'\',
									'.(($OutgoingReason[$Outgoing[$i]] != "")? '\''.$this->Get_Safe_Sql_Query($OutgoingReason[$Outgoing[$i]]).'\'':'NULL').',
									'.($OutgoingRemark[$Outgoing[$i]]!=''?'\''.$this->Get_Safe_Sql_Query($OutgoingRemark[$Outgoing[$i]]).'\'':'NULL').',
									\''.$this->Get_Safe_Sql_Query($Outgoing[$i]).'\',
									NOW(),
									\''.$_SESSION['UserID'].'\',
									NOW(),
									\''.$_SESSION['UserID'].'\'
								)';
				$Result['InsertOutgoing:'.$Outgoing[$i]] = $this->db_db_query($sql);
			}
			
			for ($i=0; $i < sizeof($Holiday); $i++) {
				$sql = 'insert into CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
								(
									StaffID,
									RecordDate,
									RecordType,
									ReasonID,
									Remark,
									SlotID,
									DateInput,
									InputBy,
									DateModified,
									ModifyBy
									)
								values  
								(
									\''.$this->Get_Safe_Sql_Query($StaffID).'\',
									\''.$this->Get_Safe_Sql_Query($TargetDate).'\',
									\''.CARD_STATUS_HOLIDAY.'\',
									'.(($HolidayReason[$Holiday[$i]] != "")? '\''.$this->Get_Safe_Sql_Query($HolidayReason[$Holiday[$i]]).'\'':'NULL').',
									'.($HolidayRemark[$Holiday[$i]]!=''?'\''.$this->Get_Safe_Sql_Query($HolidayRemark[$Holiday[$i]]).'\'':'NULL').',
									\''.$this->Get_Safe_Sql_Query($Holiday[$i]).'\',
									NOW(),
									\''.$_SESSION['UserID'].'\',
									NOW(),
									\''.$_SESSION['UserID'].'\'
								)';
				$Result['InsertHoliday:'.$Holiday[$i]] = $this->db_db_query($sql);
			}
		}
		//debug_r($Result);
		return !in_array(false,$Result);
	}
	
	function Get_Full_Date_Leave_Records($params)
	{
		$conds = '';
		if(isset($params['StaffID'])){
			$staff_id = $params['StaffID'];
			if(is_array($staff_id)){
				$conds .= " AND lr.StaffID IN ('".implode("','",$staff_id)."') ";
			}else{
				$conds .= " AND lr.StaffID='".$staff_id."' ";
			}
		}
		if(isset($params['RecordDate'])){
			$record_date = $params['RecordDate'];
			if(is_array($record_date)){
				$conds .= " AND lr.RecordDate IN ('".implode("','",$record_date)."') ";
			}else{
				$conds .= " AND lr.RecordDate='".$record_date."' ";
			}
		}
		if(isset($params['StartDate'])){
			$start_date = $params['StartDate'];
			$conds .= " AND lr.RecordDate>='".$start_date."' ";
		}
		if(isset($params['EndDate'])){
			$end_date = $params['EndDate'];
			$conds .= " AND lr.RecordDate<='".$end_date."' ";
		}
		if(isset($params['RecordType'])){
			$record_type = $params['RecordType'];
			if($record_type == 'NULL'){
				$conds .= " AND lr.RecordType IS NULL ";
			}else if(is_array($record_type)){
				$conds .= " AND lr.RecordType IN (".implode(",",$record_type).") ";
			}else {
				$conds .= " AND lr.RecordType='$record_type' ";
			}
		}
		$sql = "SELECT 
					lr.RecordID,
					lr.StaffID,
					lr.SlotID,
					lr.RecordDate,
					DAYOFMONTH(lr.RecordDate) as DayOfMonth,
					lr.RecordType,
					r.ReasonID,
					r.ReasonText 
				FROM CARD_STAFF_ATTENDANCE2_LEAVE_RECORD as lr 
				LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r ON r.ReasonID=lr.ReasonID 
				WHERE 1 $conds 
				ORDER BY StaffID,RecordDate";
		$records =  $this->returnResultSet($sql);
		if(isset($params['ReturnAssocArray'])){
			$record_size = count($records);
			$ary = array();
			for($i=0;$i<$record_size;$i++){
				if(!isset($ary[$records[$i]['StaffID']])){
					$ary[$records[$i]['StaffID']] = array();
				}
				if(!isset($ary[$records[$i]['StaffID']][$records[$i]['RecordDate']])){
					$ary[$records[$i]['StaffID']][$records[$i]['RecordDate']] = $records[$i];
				}
			}
			return $ary;
		}
		return $records;
	}
	
	function Get_Full_Date_Setting_By_Date($StaffID,$TargetDate) {
		$sql = 'select 
							lr.RecordID,
							lr.StaffID,
							lr.SlotID,
							lr.RecordDate,
							DAYOFMONTH(lr.RecordDate) as \'DayOfMonth\',
							lr.RecordType,
							r.ReasonID,
							lr.Remark  
						From 
							CARD_STAFF_ATTENDANCE2_LEAVE_RECORD lr 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE3_REASON r 
							on lr.ReasonID = r.ReasonID 
						where 
							StaffID = \''.$StaffID.'\' 
							and 
							RecordDate = \''.$TargetDate.'\' 
					 ';
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Get_Full_Day_Setting_By_Year_Month($StaffID,$TargetMonth="",$TargetYear="") {
		$TargetMonth = ($TargetMonth >= 10)? $TargetMonth:'0'.$TargetMonth;
		// get from setting
		$sql = 'select 
							lr.RecordID,
							lr.StaffID,
							lr.SlotID,
							lr.RecordDate,
							DAYOFMONTH(lr.RecordDate) as \'DayOfMonth\',
							lr.RecordType, 
							s.SlotName,
							s.SlotStart,
							s.SlotEnd,
							lr.Remark  
						From 
							CARD_STAFF_ATTENDANCE2_LEAVE_RECORD as lr 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE3_SLOT as s 
							on lr.SlotID = s.SlotID 
						where 
							StaffID = \''.$StaffID.'\' 
							and 
							RecordDate like \''.$TargetYear.'-'.$TargetMonth.'%\' 
							and 
							RecordDate > \''.date('Y-m-d').'\' 
						order by 
							lr.RecordDate, s.SlotStart
					';
		//debug_r($sql);
		$Result = $this->returnArray($sql);
		
		for ($i=0; $i< sizeof($Result); $i++) {
			$Return[$Result[$i]['DayOfMonth']][$Result[$i]['RecordType']][] = $Result[$i];
		}
		
		// get from card log (past record)
		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($TargetYear,$TargetMonth);
		$sql = 'select 
							DayNumber,
							SlotName as SlotID,
							SlotName,
							DutyStart as SlotStart,
							DutyEnd as SlotEnd,
							InSchoolStatus 
						from 
							'.$card_log_table_name.' 
						where 
							StaffID = \''.$StaffID.'\' 
							and 
							InSchoolStatus in ('.CARD_STATUS_HOLIDAY.','.CARD_STATUS_OUTGOING.') 
						order by 
							DayNumber, DutyStart
					 ';
		//debug_r($sql);
		$Result = $this->returnArray($sql);
		$CardLogSelected = array();
		for ($i=0; $i< sizeof($Result); $i++) {
			$CardLogSelected[$Result[$i]['DayNumber']][$Result[$i]['InSchoolStatus']][] = $Result[$i];
		}
		
		foreach ($CardLogSelected as $DayNumber => $Detail) {
			$Return[$DayNumber] = $Detail;
		}
		
		//debug_r($Return);
		return $Return;
	}
		
	function Get_FullDay_Setting_Records($params)
	{
		$conds = "";
		
		if(isset($params['RecordID'])){
			$id = IntegerSafe($params['RecordID']);
			if(is_array($id)){
				$conds .= " AND lr.RecordID IN (".implode(",",$id).") ";
			}else{
				$conds .= " AND lr.RecordID='$id' ";
			}
		}
		
		if(isset($params['StaffID'])){
			$id = IntegerSafe($params['StaffID']);
			if(is_array($id)){
				$conds .= " AND lr.StaffID IN (".implode(",",$id).") ";
			}else{
				$conds .= " AND lr.StaffID='$id' ";
			}
		}
		
		if(isset($params['RecordDate'])){
			$dates = $params['RecordDate'];
			if(is_array($dates)){
				$conds .= " AND lr.RecordDate IN ('".implode("','",$this->Get_Safe_Sql_Query($params['RecordDate']))."') ";
			}else{
				$conds .= " AND lr.RecordDate='".$this->Get_Safe_Sql_Query($dates)."' ";
			}
		}
		
		if(isset($params['StartDate'])){
			$conds .= " AND lr.RecordDate >= '".$this->Get_Safe_Sql_Query($params['StartDate'])."' ";
		}
		if(isset($params['EndDate'])){
			$conds .= " AND lr.RecordDate <= '".$this->Get_Safe_Sql_Query($params['EndDate'])."' ";
		}
		
		if(isset($params['RecordType']) && 
			( (!is_array($params['RecordType']) && $params['RecordType']!='') 
				|| (is_array($params['RecordType']) && count($params['RecordType'])>0) )){
			$types = IntegerSafe($params['RecordType']);
			if(is_array($types)){
				$conds .= " AND lr.RecordType IN ('".implode("','",$this->Get_Safe_Sql_Query($types))."') ";
			}else{
				$conds .= " AND lr.RecordType='".$this->Get_Safe_Sql_Query($types)."' ";
			}
		}
		
		$name_field = $this->Get_Name_Field("u.");
		$sql = "SELECT 
					lr.RecordID,
					lr.RecordDate,
					lr.StaffID,
					$name_field as StaffName,
					lr.RecordType,
					lr.Remark,
					s.SlotID,
					s.SlotName,
					s.SlotStart,
					s.SlotEnd,
					r.ReasonID,
					r.ReasonText 
				FROM CARD_STAFF_ATTENDANCE2_LEAVE_RECORD as lr 
				INNER JOIN INTRANET_USER as u ON u.UserID=lr.StaffID 
				LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as s ON s.SlotID=lr.SlotID 
				LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r ON r.ReasonID=lr.ReasonID 
				WHERE 1 $conds 
				ORDER BY u.EnglishName,lr.RecordDate ";
				
		$records = $this->returnResultSet($sql);
		
		if(isset($params['StaffIDToRecords']) && $params['StaffIDToRecords']){
			$staffIdToRecords = array();
			for($i=0;$i<count($records);$i++){
				if(!isset($staffIdToRecords[$records[$i]['StaffID']])){
					$staffIdToRecords[$records[$i]['StaffID']] = array();
				}
				$staffIdToRecords[$records[$i]['StaffID']][] = $records[$i];
			}
			return $staffIdToRecords;
		}
		
		return $records;
	}
	
	function Get_Shifting_Month_List($TargetMonth,$TargetYear) {
		for ($i=0; $i < 6; $i++) {
			$TargetTimestamp = mktime(1,1,1,($TargetMonth+$i),1,$TargetYear);
			$Month = date('n',$TargetTimestamp);
			$Year = date('Y',$TargetTimestamp);
			$TextMonth = date('M',$TargetTimestamp);
			
			$Return[] = array("TextMonth"=> $TextMonth, "Month" => $Month, "Year" => $Year);
		}
		
		return $Return;
	}

	function Get_Duty_Period_Slot_Detail_By_Users($DayOfWeeks, $GroupID, $GroupSlotPeriodID)
	{
		$result = array();
		for ($i=0; $i< sizeof($DayOfWeeks); $i++) {
			$DetailSlotInfo = $this->Get_Normal_Duty_Period_Slot_Detail($GroupID, $GroupSlotPeriodID, $DayOfWeeks[$i]);
			foreach($DetailSlotInfo['OnDuty'] as $SlotInfo) {
				foreach($SlotInfo as $temp) {
					$result[$DayOfWeeks[$i]][$temp['UserID']][] = $temp;
				}
			}
		}
		return $result;
	}

	function Save_Duty_Period_User_Time_Slot($ID,$GroupSlotPeriodID,$DayOfWeek,$TimeSlotID,$TimeSlotUser) {
		global $sys_custom, $PATH_WRT_ROOT;
		// special flow to deal with the problem of dm list
		/*
			dm0124
			1)Amend time slot in regular duty
			2)Past record with card tapped will not amend
			3)Past recrod time slot without card tapped changed in report>roster
		*/
		$sql = 'select 
							EffectiveStart, 
							EffectiveEnd,
							GroupID,
							UserID
						from 
							CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD 
						where 
							GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\'';
		$EffectiveDate = $this->returnArray($sql);
		if ($EffectiveDate[0]['GroupID'] != "") {
			$Obj = new staff_attend_group($EffectiveDate[0]['GroupID'],true);
			$UserList = $Obj->MemberList;
		}
		else {
			$Obj = $ID;
			$UserList[] = $EffectiveDate[0]['UserID'];
		}

		$OrgDuty = $this->Get_Duty_Period_Slot_Detail_By_Users($DayOfWeek, $EffectiveDate[0]['GroupID'], $GroupSlotPeriodID);

		//$this->Freeze_Duty_By_Date($UserList,$EffectiveDate[0]['EffectiveStart'],$EffectiveDate[0]['EffectiveEnd']);
		// end special flow
		
		$sql = 'Delete uwd, uwds From 
							CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY uwd 
							inner join 
							CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT uwds
							on uwd.GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\' 
								and 
								uwd.DayOfWeek in ('.implode(',',$DayOfWeek).') 
								and 
								uwd.WeekDutyID = uwds.WeekDutyID
							';
		//debug_r($sql);
		$Result['Delete:CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT'] = $this->db_db_query($sql);
		
		if($sys_custom['StaffAttendance']['CopyRegularDutyAsSpecialDuty']) {
			$AllStaffList = array();
		}
		for ($i=0; $i< sizeof($DayOfWeek); $i++) {
			foreach ($TimeSlotUser as $TimeSlotID => $StaffList) {
				if($sys_custom['StaffAttendance']['CopyRegularDutyAsSpecialDuty']) {
					$AllStaffList = array_merge($AllStaffList, $StaffList);
				}
				for ($j=0; $j < sizeof($StaffList); $j++) {
					if(trim($StaffList[$j])=='') continue;
					$sql = 'insert into CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY (
										GroupSlotPeriodID,
										UserID,
										DayOfWeek,
										DateInput,
										InputBy,
										DateModify,
										ModifyBy)
									values (
										\''.$GroupSlotPeriodID.'\',
										\''.$StaffList[$j].'\',
										\''.$DayOfWeek[$i].'\',
										NOW(),
										\''.$_SESSION['UserID'].'\',
										NOW(),
										\''.$_SESSION['UserID'].'\'
										)
									on duplicate key update 
										WeekDutyID = LAST_INSERT_ID(WeekDutyID),
										DateModify = NOW(),
										ModifyBy = \''.$_SESSION['UserID'].'\'';
					//debug_r($sql);
					$Result['Insert:CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY:'.$GroupSlotPeriodID.'-'.$StaffList[$j].'-'.$DayOfWeek[$i]] = $this->db_db_query($sql);
					
					$WeekDutyID = $this->db_insert_id();
					$sql = 'insert into CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT (
										WeekDutyID,
										SlotID,
										DateInput,
										InputBy
										)
									values (
										\''.$WeekDutyID.'\',
										\''.$TimeSlotID.'\',
										NOW(),
										\''.$_SESSION['UserID'].'\'
										)
									';
					//debug_r($sql);
					$Result['Insert:CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT:'.$WeekDutyID.'-'.$TimeSlotID] = $this->db_db_query($sql);
				}
			}
		}
		
		// when applying regular duty, for special duty days, staff that without special duty, copy regular duty as special duty
		// case: 2013-0717-0919-17073
		if($sys_custom['StaffAttendance']['CopyRegularDutyAsSpecialDuty']) {
			$AllStaffList = array_values(array_unique($AllStaffList));
			$AllStaffIdCsv = implode(",",$AllStaffList);
			$Today = date("Y-m-d");
			$sql = "SELECT 
						s.DutyDate, GROUP_CONCAT(s.UserID) as UserIdCsv 
					FROM CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY as s 
					WHERE s.UserID IN ($AllStaffIdCsv) AND s.DutyDate > '$Today' AND s.DutyDate >= '".$EffectiveDate[0]['EffectiveStart']."' 
						AND s.DutyDate <= '".$EffectiveDate[0]['EffectiveEnd']."' 
					GROUP BY s.DutyDate";
				
			$DutyDateUserIdAry = $this->returnResultSet($sql);
			$DutyDateUserIdAry_count = count($DutyDateUserIdAry);
			for($i=0;$i<$DutyDateUserIdAry_count;$i++) {
				$weekday = date("w", strtotime($DutyDateUserIdAry[$i]['DutyDate']));
				if(!in_array($weekday,$DayOfWeek)) {
					continue;
				}
				$WithSpecialDutyUserList = explode(",",str_replace(" ","",$DutyDateUserIdAry[$i]['UserIdCsv']));
				$NoSpecialDutyUserList = array_diff($AllStaffList,$WithSpecialDutyUserList);
				
				if(count($NoSpecialDutyUserList)>0){
					$NoSpecialDutyUserIdCsv = implode(",",$NoSpecialDutyUserList);
					$sql = "SELECT 
								uwd.UserID, uwds.SlotID 
							FROM CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY uwd 
							INNER JOIN CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT uwds 
								ON uwd.DayOfWeek = '".$weekday."' 
								AND uwd.GroupSlotPeriodID = '".$GroupSlotPeriodID."' 
								AND uwd.WeekDutyID = uwds.WeekDutyID 
								AND uwd.UserID IN (".$NoSpecialDutyUserIdCsv.") 
							WHERE uwd.UserID IN (".$NoSpecialDutyUserIdCsv.") ";
					$UserIdSlotIdAry = $this->returnResultSet($sql);
					
					$special_duty_values = array();
					for($j=0;$j<count($UserIdSlotIdAry);$j++) {
						$special_duty_values[] = "('".$UserIdSlotIdAry[$j]['UserID']."','".$DutyDateUserIdAry[$i]['DutyDate']."','".$UserIdSlotIdAry[$j]['SlotID']."',NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')";
					}
					
					if(count($special_duty_values)>0) {
						$sql = "INSERT INTO CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY 
								(UserID,DutyDate,SlotID,DateInput,InputBy,DateModify,ModifyBy)
								VALUES ".implode(",",$special_duty_values);
						$Result['InsertSpecialDuty_'.$DutyDateUserIdAry[$i]['DutyDate']] = $this->db_db_query($sql);
					}
				}
			}
		}

		$NewDuty = $this->Get_Duty_Period_Slot_Detail_By_Users($DayOfWeek, $EffectiveDate[0]['GroupID'], $GroupSlotPeriodID);

		if ($this->isRecordChangeLogEnabled()) {
			$have_record = false;
			$Obj = new staff_attend_group($ID,true);
			$RecordDetail = array();
			$RecordDetail[] = 'Group Name: '.$Obj->GroupName;
			$RecordDetail[] = 'Time Slot: '.$EffectiveDate[0]['EffectiveStart'].' to '.$EffectiveDate[0]['EffectiveEnd'];
			$record_delete = array();
			$record_new = array();
			$slot_ids = array();
			foreach($OrgDuty as $day_of_week => $staff_duty) {
				foreach($staff_duty as $StaffID => $old_duty) {
					foreach($old_duty as $old_slot_info) {
						$new_duty = false;
						for ($j = 0; $j < count($NewDuty[$day_of_week][$StaffID]); $j++) {
							if($NewDuty[$day_of_week][$StaffID][$j]['SlotID'] == $old_slot_info['SlotID']) {
								$new_duty = $NewDuty[$day_of_week][$StaffID][$j];
								break;
							}
						}

						if($new_duty == false) {
							$record_delete[$day_of_week][$old_slot_info['SlotID']][] = $old_slot_info['StaffName'];
							$slot_ids[] = $old_slot_info['SlotID'];
							continue;
						}
					}
				}
			}

			foreach($NewDuty as $day_of_week => $staff_duty) {
				foreach($staff_duty as $StaffID => $new_duty) {
					foreach($new_duty as $new_slot_info) {
						$old_duty = false;
						for ($j = 0; $j < count($OrgDuty[$day_of_week][$StaffID]); $j++) {
							if($OrgDuty[$day_of_week][$StaffID][$j]['SlotID'] == $new_slot_info['SlotID']) {
								$old_duty = $OrgDuty[$day_of_week][$StaffID][$j];
								break;
							}
						}

						if($old_duty == false) {
							$record_new[$day_of_week][$new_slot_info['SlotID']][] = $new_slot_info['SlotID'];
							$slot_ids[] = $new_slot_info['SlotID'];
							continue;
						}
					}
				}
			}

			$slot_details = array();
			if(count($slot_ids) > 0) {
				$slot_list = $this->Get_Slot_List($ID, ($EffectiveDate[0]['GroupID'] != "") ? "Group" : "", $slot_ids);
				foreach ($slot_list as $temp) {
					$slot_details[$temp['SlotID']] = $temp;
				}
			}

			include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
			if (count($record_delete) > 0) {
				foreach($record_delete as $day_of_week=>$record) {
					$libstaffatt_ui = new libstaffattend3_ui();
					$RecordDetail[] = "";
					$RecordDetail[] = 'Action: Delete ('.$libstaffatt_ui->Get_Day_Of_Week_Presentation($day_of_week).')';
					foreach ($record as $SlotID => $temp) {
						$RecordDetail[] = 'Time Slot: ' . $slot_details[$SlotID]['SlotName'].'('.$slot_details[$SlotID]['SlotStart'].'-'.$slot_details[$SlotID]['SlotEnd'].')';
						$RecordDetail[] = 'Users: ' . implode(', ', $temp);
						$have_record = true;
					}
				}
			}

			if (count($record_new) > 0) {
				foreach($record_new as $day_of_week=>$record) {
					$libstaffatt_ui = new libstaffattend3_ui();
					$RecordDetail[] = "";
					$RecordDetail[] = 'Action: Create ('.$libstaffatt_ui->Get_Day_Of_Week_Presentation($day_of_week).')';
					foreach ($record as $SlotID => $temp) {
						$RecordDetail[] = 'Time Slot: ' . $slot_details[$SlotID]['SlotName'].'('.$slot_details[$SlotID]['SlotStart'].'-'.$slot_details[$SlotID]['SlotEnd'].')';
						$RecordDetail[] = 'Users: ' . implode(', ', $temp);
						$have_record = true;
					}
				}
			}

			if($have_record) {
				$Result['AttendanceLog:'] = $this->INSERT_UPDATE_LOG('Regular_Duty', implode('<br>', $RecordDetail));
			}
		}

		//debug_r($Result);
		return !in_array(false,$Result);
	}
	
	function Check_Duty_Period_User_Time_Slot($TimeSlotID,$TimeSlotUser,$SlotStart,$SlotEnd,$EditDuty=false) {
		if ($EditDuty) {
			foreach ($SlotStart as $SlotName => $DutyStart) {
				$TimeSlotDetail[$SlotName] = array('SlotStart' => $DutyStart,'SlotEnd' => $SlotEnd[$SlotName]);
			}
		}
		else {
			$sql = 'select 
							SlotID,
							SlotName,
							SlotStart,
							SlotEnd 
						From 
							CARD_STAFF_ATTENDANCE3_SLOT 
						where 
							SlotID in ('.implode(',',$TimeSlotID).') 
					 ';
			$Result = $this->returnArray($sql);
			for ($i=0; $i< sizeof($Result); $i++) {
				$TimeSlotDetail[$Result[$i]['SlotID']] = $Result[$i];
			}
		}
		
		$StaffDuty = array();
		foreach ($TimeSlotUser as $Key => $Val) {
			for ($i=0; $i< sizeof($Val); $i++) {
				$StaffDuty[$Val[$i]][] = $TimeSlotDetail[$Key];
			}
		}
		
		foreach ($StaffDuty as $Key => $Val) {
			$MinDutyStart = '00:00:00';
			$MaxDutyEnd = '00:00:00';
			$TimeSlotOverlap = false;
			$HaveOverNightDuty = false;
			$DutyOver24Hour = false;
			for ($i=0; $i< sizeof($Val); $i++) {
				// check for overlap duty
				for ($j=0; $j< sizeof($Val); $j++) {
					if ($i != $j) {
						if ($Val[$j]['SlotStart'] < $Val[$j]['SlotEnd']) { 
							if (($Val[$i]['SlotStart'] >= $Val[$j]['SlotStart'] && 
									$Val[$i]['SlotStart'] <= $Val[$j]['SlotEnd']) ||
									($Val[$i]['SlotEnd'] >= $Val[$j]['SlotStart'] && 
									$Val[$i]['SlotEnd'] <= $Val[$j]['SlotEnd'])) {
								$TimeSlotOverlap = true;
							}
						}
						else { 
							if (($Val[$i]['SlotStart'] >= $Val[$j]['SlotStart'] ||
									$Val[$i]['SlotStart'] <= $Val[$j]['SlotEnd']) ||
									($Val[$i]['SlotEnd'] >= $Val[$j]['SlotStart'] || 
									$Val[$i]['SlotEnd'] <= $Val[$j]['SlotEnd'])) {
								$TimeSlotOverlap = true;
							}
						}
					}
				}
				
				if ($TimeSlotOverlap) {
					$Problem['TimeSlotOverlap'][] = $Key;
					break;
				}
				
				// check for duty over 24 hours
				if ($i==0) {
					$MinDutyStart = $TimeSlotDetail[$Val[$i]['SlotID']]['SlotStart'];
					$MaxDutyEnd = $TimeSlotDetail[$Val[$i]['SlotID']]['SlotEnd'];
				}
				
				if ($TimeSlotDetail[$Val[$i]['SlotID']]['SlotEnd'] < $TimeSlotDetail[$Val[$i]['SlotID']]['SlotStart']) { // over night duty
					$HaveOverNightDuty = true;
					if ($TimeSlotDetail[$Val[$i]['SlotID']]['SlotStart'] < $MinDutyStart) {
						$MinDutyStart = $TimeSlotDetail[$Val[$i]['SlotID']]['SlotStart'];
					}
					
					if ($TimeSlotDetail[$Val[$i]['SlotID']]['SlotEnd'] < $MaxDutyEnd) {
						$MaxDutyEnd = $TimeSlotDetail[$Val[$i]['SlotID']]['SlotEnd'];
					}
				}
				else {
					if ($TimeSlotDetail[$Val[$i]['SlotID']]['SlotStart'] < $MinDutyStart) {
						$MinDutyStart = $TimeSlotDetail[$Val[$i]['SlotID']]['SlotStart'];
					}
					
					if ($TimeSlotDetail[$Val[$i]['SlotID']]['SlotEnd'] > $MaxDutyEnd) {
						$MaxDutyEnd = $TimeSlotDetail[$Val[$i]['SlotID']]['SlotEnd'];
					}
				}
			}
			
			if ($HaveOverNightDuty) {
				if ($MaxDutyEnd > $MinDutyStart) {
					$Problem['DutyOver24Hour'][] = $Key;
				}
			}
			else {
				if ($MaxDutyEnd < $MinDutyStart) {
					$Problem['DutyOver24Hour'][] = $Key;
				}
			}
		}
		
		return $Problem;
	}
	
	function Get_Normal_Duty_Period_Slot_Detail($GroupID,$GroupSlotPeriodID,$DayOfWeek) {
		$NameField = getNameFieldByLang('u.');
		
		// get on duty list
		$sql = 'select
						  s.SlotID,
						  s.SlotName,
						  u.UserID,
						  '.$NameField.' as StaffName
						From
						  CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY uwd 
						  inner join 
						  CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT uwds 
						  on uwd.GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\' 
						    and uwd.DayOfWeek = \''.$DayOfWeek.'\' 
						    and uwd.WeekDutyID = uwds.WeekDutyID 
						  inner join 
						  CARD_STAFF_ATTENDANCE3_SLOT s 
						  on uwds.SlotID = s.SlotID 
						  inner join 
						  INTRANET_USER u 
						  on uwd.UserID = u.UserID AND u.RecordStatus = 1
						order by 
							s.SlotStart,u.EnglishName
					 ';
		//debug_r($sql);
		$Result = $this->returnArray($sql);
		
		for ($i=0; $i< sizeof($Result); $i++) {
			$Return['OnDuty'][$Result[$i]['SlotID']][] = $Result[$i];
		}
		
		// get unassigned 
		$sql = 'select 
							u.UserID,
							'.$NameField.' as StaffName 
						from 
							CARD_STAFF_ATTENDANCE_USERGROUP ug 
							inner join 
							INTRANET_USER u 
							on ug.GroupID = \''.$GroupID.'\' 
								and ug.UserID = u.UserID AND u.RecordStatus = 1 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY uwd 
							on u.UserID = uwd.UserID 
								and uwd.GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\' 
						  	and uwd.DayOfWeek = \''.$DayOfWeek.'\' 
						where 
							uwd.UserID IS NULL
						order by 
							u.EnglishName';
		//debug_r($sql);
		$Result = $this->returnArray($sql);
		
		for ($i=0; $i< sizeof($Result); $i++) {
			$Return['UnAssigned'][] = $Result[$i];
		}
		
		return $Return;
	}
	
	function Get_Group_Member_List($GroupID,$Keyword="",$Recordstatus="1",$GetArchiveUser=false) {
		//$NameField = getNameFieldByLang('u.');
		$NameField = $this->Get_Name_Field("u.");
		$sql = 'select 
							u.UserID,
							'.$NameField.' as StaffName,
							u.EnglishName,
							u.UserLogin  
						From 
							CARD_STAFF_ATTENDANCE_USERGROUP ug 
							inner join 
							INTRANET_USER u 
							on ug.GroupID = \''.$GroupID.'\' 
								and ug.UserID = u.UserID 
								and u.RecordStatus IN ('.$Recordstatus.') ';
		if ($Keyword != "") {
			$sql .= 'and (
							u.EnglishName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							or 
							u.ChineseName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							) ';
		}
		if($GetArchiveUser) {
			$ArchiveNameField = $this->Get_Archive_Name_Field("u2.");
			$sql .= 'UNION (
						select u2.UserID, '.$ArchiveNameField.' as StaffName, u2.EnglishName, u2.UserLogin   
						from CARD_STAFF_ATTENDANCE_USERGROUP ug 
						inner join INTRANET_ARCHIVE_USER as u2 ON u2.UserID=ug.UserID and u2.RecordType=1 and ug.GroupID = \''.$GroupID.'\' ';
			if($Keyword != '') {
				$sql .= 'and (
							u2.EnglishName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							or 
							u2.ChineseName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							) ';
			}
			$sql.= ')';
		}
		$sql .= 'order by EnglishName';
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Get_Slot_List_For_Duty_Period($ID,$GroupSlotPeriodID,$DayOfWeek,$GroupOrIndividual="Group") {
		if ($GroupOrIndividual!="Group") {
			$sql = 'select 
								s.SlotID,
							  s.SlotName,
							  s.SlotStart,
							  s.SlotEnd,
							  IF (b.SlotID IS NULL,\'0\',\'1\') as InUse 
							From 
								CARD_STAFF_ATTENDANCE3_SLOT s 
								LEFT JOIN 
								(select distinct 
									uwds.SlotID 
								From 
									CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY uwd 
									inner join 
									CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT uwds 
									on 
										uwd.GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\' 
										and 
										uwd.DayOfWeek = \''.$DayOfWeek.'\' 
										and 
										uwd.WeekDutyID = uwds.WeekDutyID
								) b 
								on s.SlotID = b.SlotID
							where 
								s.UserID = \''.$ID.'\' 
							order by 
								s.SlotStart
						 ';
		}
		else {
			$sql = 'select 
								s.SlotID,
							  s.SlotName,
							  s.SlotStart,
							  s.SlotEnd,
							  IF (b.SlotID IS NULL,\'0\',\'1\') as InUse 
							From 
								CARD_STAFF_ATTENDANCE2_GROUP g 
								inner join 
								CARD_STAFF_ATTENDANCE3_SLOT s 
								on g.GroupID = \''.$ID.'\' and g.GroupID = s.GroupID 
								LEFT JOIN 
								(select distinct 
									uwds.SlotID 
								From 
									CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY uwd 
									inner join 
									CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT uwds 
									on 
										uwd.GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\' 
										and 
										uwd.DayOfWeek = \''.$DayOfWeek.'\' 
										and 
										uwd.WeekDutyID = uwds.WeekDutyID
								) b 
								on s.SlotID = b.SlotID
							order by 
								s.SlotStart
						 ';
		}
		
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Get_Duty_Period_Slot_Duty_Distribution($GroupSlotPeriodID,$DayOfWeek) {
		$sql = 'select
						  s.SlotID,
						  s.SlotName,
						  s.SlotStart,
						  s.SlotEnd,
						  count(1) as NumberOfUser
						from
						  CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY uwd
						  inner join
						  CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT uwds
						  on
						    uwd.GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\'
						    and uwd.DayOfWeek = \''.$DayOfWeek.'\'
						    and uwd.WeekDutyID = uwds.WeekDutyID
						  inner join
						  CARD_STAFF_ATTENDANCE3_SLOT s
						  on uwds.SlotID = s.SlotID 
						  inner join INTRANET_USER as u on u.UserID=uwd.UserID and u.RecordStatus=1 and u.RecordType=1 
						group by
						  s.SlotID 
						Order by 
							s.SlotStart';
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Save_Duty_Period($ID,$GroupSlotPeriodID,$EffectiveStart,$EffectiveEnd,$SkipSchoolHoliday,$SkipPublicHoliday,$SkipSchoolEvent,$SkipAcademicEvent,$GroupOrIndividual="Group",$DayType=1) {
		// special flow to deal with the problem of dm list
		/*
			dm0124
			1)Amend time slot in regular duty
			2)Past record with card tapped will not amend
			3)Past recrod time slot without card tapped changed in report>roster
		*/
		if ($GroupOrIndividual == "Group") {
			$Obj = new staff_attend_group($ID,true);
			$UserList = $Obj->MemberList;
		}
		else {
			$UserList[] = $ID;
		}
		//$this->Freeze_Duty_By_Date($UserList,$EffectiveStart,$EffectiveEnd);
		// end of special flow
		
		if ($GroupOrIndividual == "Group") {
			$IDField = "GroupID";
		}
		else {
			$IDField = "UserID";
		}
		
		if ($GroupSlotPeriodID == "") {
			$sql= 'insert into CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD 
								(
								'.$IDField.',
								EffectiveStart,
								EffectiveEnd,
								SkipSchoolHoliday,
								SkipPublicHoliday,
								SkipSchoolEvent,
								SkipAcademicEvent,
								DayType,
								DateInput,
								InputBy,
								DateModify,
								ModifyBy
								) 
						values 
								(
								\''.$ID.'\',
								\''.$EffectiveStart.'\',
								\''.$EffectiveEnd.'\',
								\''.$SkipSchoolHoliday.'\',
								\''.$SkipPublicHoliday.'\',
								\''.$SkipSchoolEvent.'\',
								\''.$SkipAcademicEvent.'\',
								\''.$DayType.'\',
								Now(),
								\''.$_SESSION['UserID'].'\',
								Now(),
								\''.$_SESSION['UserID'].'\'
								)
						';
		}
		else {
			$sql = 'update CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD set 
								EffectiveStart = \''.$EffectiveStart.'\',
								EffectiveEnd = \''.$EffectiveEnd.'\',
								SkipSchoolHoliday = \''.$SkipSchoolHoliday.'\',
								SkipPublicHoliday = \''.$SkipPublicHoliday.'\',
								SkipSchoolEvent = \''.$SkipSchoolEvent.'\',
								SkipAcademicEvent = \''.$SkipAcademicEvent.'\',
								DayType = \''.$DayType.'\',
								DateModify = Now(),
								ModifyBy = \''.$_SESSION['UserID'].'\' 
							where 
								GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\'
						 ';
		}
		
		//debug_r($sql);
		return $this->db_db_query($sql);
	}
	
	function Delete_Duty_Period($GroupSlotPeriodID) {
		$sql = 'delete ngsp, uwd, uwds from 
							CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD ngsp 
							left join 
							CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY uwd 
							on 
								ngsp.GroupSlotPeriodID = uwd.GroupSlotPeriodID 
							left join 
							CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT uwds 
							on uwd.WeekDutyID = uwds.WeekDutyID 
						where 
							ngsp.GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\' 
						';
		
		//debug_r($sql);
		return $this->db_db_query($sql);
	}
	
	function Check_Duty_Period($ID,$GroupSlotPeriodID,$EffectiveStart,$EffectiveEnd,$GroupOrIndividual="Group",$DayType=1) {
		if ($GroupOrIndividual == "Group") 
			$IDField = "GroupID ";
		else 
			$IDField = "UserID ";
		
		$sql = 'select 
							count(1) 
						from 
							CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD 
						where 
							'.$IDField.' = \''.$ID.'\' 
							and ';
		if ($GroupSlotPeriodID != "") {
			$sql .= 'GroupSlotPeriodID != \''.$GroupSlotPeriodID.'\' 
							and ';
		}
		//$sql .= 'DayType=\''.$DayType.'\' and ';
		$sql .= '	(
							\''.$EffectiveStart.'\' between EffectiveStart and EffectiveEnd 
							OR 
							\''.$EffectiveEnd.'\' between EffectiveStart and EffectiveEnd 
							OR 
							EffectiveStart between \''.$EffectiveStart.'\' and \''.$EffectiveEnd.'\'
							OR 
							EffectiveEnd between \''.$EffectiveStart.'\' and \''.$EffectiveEnd.'\'
							)';
		$Result = $this->returnVector($sql);
		//debug_r($sql);
		return !($Result[0] > 0);
	}
	
	function Get_Default_Normal_Group_Weekly_Duty_Period($ID,$GroupOrIndividual="Group") {
		if ($GroupOrIndividual == "Group") {
			$Cond = 'GroupID = \''.$ID.'\'';
		}
		else {
			$Cond = 'UserID = \''.$ID.'\'';
		}
		$sql = 'select 
							max(EffectiveEnd) as EffectiveEnd 
						From 
							CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD as ngsp 
						where 
							'.$Cond.'
					 ';
		$Result = $this->returnVector($sql);
		
		if ($Result[0] != "") {
			$TimeStamp = strtotime($Result[0]);
			$DefaultPeriodStartYear = Date('Y',$TimeStamp);
			$DefaultPeriodStartMonth = Date('n',$TimeStamp);
			$DefaultPeriodStartDay = Date('j',$TimeStamp);
			
			$DefaultPeriodStart = date('Y-m-d',mktime(0,0,0,$DefaultPeriodStartMonth,($DefaultPeriodStartDay+1), $DefaultPeriodStartYear));
		}
		return array(array("EffectiveStart" => ($Result[0]==""? date('Y-m-d'):$DefaultPeriodStart),"EffectiveEnd" => ($Result[0]==""? date('Y-m-d'):$DefaultPeriodStart)));
	}

	function Get_Weekly_Duty_Period($ID="",$GroupSlotPeriodID="",$GroupOrIndividual="Group") {
		if ($ID != "") {
			if ($GroupOrIndividual == "Group") {
				$cond = 'ngsp.GroupID = \''.$ID.'\' ';
			}
			else {
				$cond = 'ngsp.UserID = \''.$ID.'\' ';
			}
		}
		else 
			$cond = 'ngsp.GroupSlotPeriodID = \''.$GroupSlotPeriodID.'\' ';
			
		$sql = 'select distinct 
							ngsp.GroupSlotPeriodID,
							ngsp.EffectiveStart,
							ngsp.EffectiveEnd,
							ngsp.SkipSchoolHoliday,
							ngsp.SkipSchoolEvent,
							ngsp.SkipPublicHoliday,
							ngsp.SkipAcademicEvent,
							ngsp.DayType 
						From 
							CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD as ngsp 
						where
							'.$cond.'
						order by 
							ngsp.EffectiveStart
							';
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Delete_Working_Period($WorkingPeriodID) {
		$sql = 'delete from CARD_STAFF_ATTENDANCE3_WORKING_PERIOD where WorkingPeriodID = \''.$WorkingPeriodID.'\'';
		
		return $this->db_db_query($sql);
	}
	
	function Save_Working_Period($StaffID,$WorkingPeriodID,$PeriodStart,$PeriodEnd) {
		$PeriodStart = ($PeriodStart == "")? '1970-01-01':$PeriodStart;
		$PeriodEnd = ($PeriodEnd == "")? '2099-12-31':$PeriodEnd;
		if ($WorkingPeriodID == "") {
			$sql = 'insert into CARD_STAFF_ATTENDANCE3_WORKING_PERIOD 
								(
								UserID,
								PeriodStart,
								PeriodEnd,
								DateInput,
								InputBy,
								DateModify,
								ModifyBy
								)
							values 
								(
								\''.$StaffID.'\',
								\''.$PeriodStart.'\',
								\''.$PeriodEnd.'\',
								NOW(),
								\''.$_SESSION['UserID'].'\',
								NOW(),
								\''.$_SESSION['UserID'].'\'
								)';
		}
		else {
			$sql = 'update CARD_STAFF_ATTENDANCE3_WORKING_PERIOD set 
								PeriodStart = \''.$PeriodStart.'\',
								PeriodEnd = \''.$PeriodEnd.'\',
								DateModify = NOW(),
								ModifyBy = \''.$_SESSION['UserID'].'\' 
							where 
								WorkingPeriodID = \''.$WorkingPeriodID.'\'';
		}
		
		//debug_r($sql);
		return $this->db_db_query($sql);
	}
	
	function Check_Working_Period($StaffID,$WorkingPeriodID,$PeriodStart,$PeriodEnd) {
		$PeriodStart = trim($PeriodStart) == ""? '1970-01-01':$PeriodStart;
		$PeriodEnd = trim($PeriodEnd) == ""? '2099-12-31':$PeriodEnd;
		$sql = 'select 
							count(1) 
						from 
							CARD_STAFF_ATTENDANCE3_WORKING_PERIOD 
						where 
							UserID = \''.$StaffID.'\' 
							and ';
		if ($WorkingPeriodID != "") {
			$sql .= 'WorkingPeriodID != \''.$WorkingPeriodID.'\' 
							and ';
		}
		$sql .= '	(
							(\''.$PeriodStart.'\' between PeriodStart and PeriodEnd) 
							OR (\''.$PeriodEnd.'\' between PeriodStart and PeriodEnd)
							OR (PeriodStart between \''.$PeriodStart.'\' and \''.$PeriodEnd.'\')
							OR (PeriodEnd between \''.$PeriodStart.'\' and \''.$PeriodEnd.'\')
							)';
		$Result = $this->returnVector($sql);
		//debug_r($sql);
		return !($Result[0] > 0);
	}
	
	function Get_Working_Period_Default_Detail($StaffID) {
		$sql = 'select 
							MAX(PeriodEnd) as PeriodEnd 
						From 
							CARD_STAFF_ATTENDANCE3_WORKING_PERIOD 
						where 
							UserID = \''.$StaffID.'\'
							and 
							PeriodEnd != \'2099-12-31\' 
						group by 
							UserID';
		
		$Result = $this->returnVector($sql);
		
		if ($Result[0] != "") {
			$TimeStamp = strtotime($Result[0]);
			$DefaultPeriodStartYear = Date('Y',$TimeStamp);
			$DefaultPeriodStartMonth = Date('n',$TimeStamp);
			$DefaultPeriodStartDay = Date('j',$TimeStamp);
			
			$DefaultPeriodStart = date('Y-m-d',mktime(0,0,0,$DefaultPeriodStartMonth,($DefaultPeriodStartDay+1), $DefaultPeriodStartYear));
		}
		return array(array("",($Result[0]==""? " ":$DefaultPeriodStart)," "));
	}
	
	function Get_Working_Period_Detail($WorkingPeriodID) {
		$sql= 'select 
						WorkingPeriodID,
						PeriodStart,
						PeriodEnd 
					From 
						CARD_STAFF_ATTENDANCE3_WORKING_PERIOD 
					where 
						WorkingPeriodID = \''.$WorkingPeriodID.'\'';
		return $this->returnArray($sql,3);
	}
	
	function Get_Full_Day_Setting_User_List($Keyword="",$Recordstatus="1", $GetArchiveUser=false) {
		//$NameField = getNameFieldByLang("u.");
		$NameField = $this->Get_Name_Field("u.");
		$sql = 'select
						  u.UserID, 
						  '.$NameField.' as UserName 
						from
							INTRANET_USER as u 
						where 
							u.RecordType = \'1\' 
							and 
							u.RecordStatus IN ('.$Recordstatus.') 
							and 
							(
							u.EnglishName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							or 
							u.ChineseName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							)
						order by
						  u.EnglishName';
		$records = $this->returnArray($sql);
		//debug_r($sql);
		if($GetArchiveUser) {
			$ArchiveNameField = $this->Get_Archive_Name_Field("");
			$sql = "select 
						UserID,
						$ArchiveNameField as UserName 
					from INTRANET_ARCHIVE_USER 
					where 
						RecordType=1 
						and (EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						or ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%') 
					order by EnglishName";
			$archivedRecords = $this->returnArray($sql);
			$records = array_merge($records, $archivedRecords);
		}
		return $records;
	}
	
	function Get_Working_Period_User_List($Keyword="",$StaffStatus="",$SearchGroupName=false,$Recordstatus="1",$GetArchiveUser=false) {
		if (trim($StaffStatus) != "") {
			$sql = 'select distinct 
								UserID 
							From 
								CARD_STAFF_ATTENDANCE3_WORKING_PERIOD 
							where 
								NOW() between PeriodStart and PeriodEnd';
			$ActiveUser = $this->returnVector($sql);
			
			if (sizeof($ActiveUser) > 0) {
				$ActiveUser = implode(',',$ActiveUser);
			}
			else {
				$ActiveUser = '-1';
			}
			
		}
		
		//$NameField = getNameFieldByLang("u.");
		$NameField = $this->Get_Name_Field("u.");
		$sql = 'select
						  u.UserID, 
						  '.$NameField.' as UserName, 
						  u.CardID,
						  u.RFID,
						  g.GroupName 
						from
							INTRANET_USER as u 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE_USERGROUP as ug 
							on u.UserID = ug.UserID
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE2_GROUP as g 
							on ug.GroupID = g.GroupID
						where 
							u.RecordType = \'1\' 
							and 
							u.RecordStatus IN ('.IntegerSafe($Recordstatus).')
							and 
							(
							u.EnglishName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							or 
							u.ChineseName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' ';
		if ($SearchGroupName) {
			$sql .= '
							OR 
							g.GroupName like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
							';
		}
		$sql .='
							) 
							';
		
		if ($StaffStatus == "1") { // only active 
			$sql .= ' and u.UserID in ('.$ActiveUser.') ';
		}
		else if ($StaffStatus == "0") { // only in active
			$sql .= ' and u.UserID not in ('.$ActiveUser.') ';
		}
		$sql .= 'order by
						  u.EnglishName';
		//debug_r($sql);
		$records = $this->returnArray($sql);
		
		if($GetArchiveUser) {
			$ArchiveNameField = $this->Get_Archive_Name_Field("");
			$sql = 'select
						  u.UserID, 
						  '.$ArchiveNameField.' as UserName, 
						  u.CardID,
						  u.RFID,
						  g.GroupName 
						from
							INTRANET_ARCHIVE_USER as u 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE_USERGROUP as ug 
							on u.UserID = ug.UserID
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE2_GROUP as g 
							on ug.GroupID = g.GroupID
						where 
							u.RecordType = \'1\' 
							and 
							(
							u.EnglishName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							or 
							u.ChineseName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' ';
			if ($SearchGroupName) {
				$sql .= '
								OR 
								g.GroupName like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								';
			}
			$sql .='
								) 
								';
			//$sql .= ' and u.UserID in ('.$ActiveUser.') ';
			if ($StaffStatus == "1") { // only active 
				$sql .= ' and u.UserID in ('.$ActiveUser.') ';
			}
			else if ($StaffStatus == "0") { // only in active
				$sql .= ' and u.UserID not in ('.$ActiveUser.') ';
			}
			$sql .= 'order by
						  u.EnglishName';
			$archivedRecords = $this->returnArray($sql);
			$records = array_merge($records, $archivedRecords);
		}
		
		return $records;
	}
	
	function Get_User_Working_Period($TargetUserID) {
		$sql = 'select 
							WorkingPeriodID,
							PeriodStart,
							PeriodEnd
						From 
							CARD_STAFF_ATTENDANCE3_WORKING_PERIOD  
						where 
							UserID = \''.$TargetUserID.'\'
					 ';
		return $this->returnArray($sql);
	}
	
	function Get_User_List($Keyword="",$GetDutyPeriodInfo=false,$GetSlotInfo=false,$SearchSlotAlso=true,$FilterEmploymentPeriod="") {
		if (trim($Keyword) != "") {
			$sql = 'select distinct 
								u.UserID 
							From 
								INTRANET_USER as u 
								left join 
								CARD_STAFF_ATTENDANCE_USERGROUP as ug 
								on u.UserID = ug.UserID ';
			if ($SearchSlotAlso) {
				$sql .= '
								left join  
								CARD_STAFF_ATTENDANCE3_SLOT as s 
								on u.UserID = s.UserID ';
			}
			$sql .= '
							where 
								u.RecordType = \'1\' 
								and 
								u.RecordStatus = \'1\' 
								and 
								ug.GroupID IS NULL 
								and 
								(
								u.EnglishName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
								or 
								u.ChineseName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' ';
			if ($SearchSlotAlso) {
				$sql .= '
								OR 
								s.SlotName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
								OR 
								s.SlotName like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								OR 
								s.SlotStart like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								OR 
								s.SlotEnd like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' ';
								
			}
			$sql .= ')';
			//debug_r($sql);
			$UserList = '-1,'.implode(',',$this->returnVector($sql));
		}
		
		$NameField = getNameFieldByLang("u.");
		$sql = 'select
						  u.UserID, 
						  '.$NameField.' as UserName ';
		if ($GetDutyPeriodInfo) {
			$sql .= ',
						  CASE 
						  	WHEN c.NumberOfDutyPeriod IS NULL THEN 0 
						  	ELSE c.NumberOfDutyPeriod 
						  END as NumberOfDutyPeriod ';
		}
		if ($GetSlotInfo) {
			$sql .= ',
						  CASE 
						  	WHEN d.NumberOfSlot IS NULL THEN 0 
						  	ELSE d.NumberOfSlot 
						  END as NumberOfSlot ';
		}
		$sql .= 'from
							INTRANET_USER as u ';
		if ($FilterEmploymentPeriod != "") {
			$sql .= "
							INNER JOIN 
							CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
							on u.UserID = wp.UserID 
								and 
								'".$FilterEmploymentPeriod."' between wp.PeriodStart and wp.PeriodEnd ";
		}
		if ($GetDutyPeriodInfo) {
			$sql .= 'LEFT JOIN 
							(select
						    u.UserID,
						    count(1) as NumberOfDutyPeriod
						  From
						    INTRANET_USER as u 
						    INNER JOIN
						    CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD as ngsp
								on
									u.RecordType = \'1\' 
									and 
									u.RecordStatus = \'1\' 
									and  
									u.UserID = ngsp.UserID
						  Group By
						    u.UserID) as c
						  on u.UserID = c.UserID
							';
		}
		if ($GetSlotInfo) {
			$sql .= 'LEFT JOIN 
							(select
						    u.UserID,
						    count(1) as NumberOfSlot
						  From
						    INTRANET_USER as u 
						    INNER JOIN
						    CARD_STAFF_ATTENDANCE3_SLOT as s
								on
									u.RecordType = \'1\' 
									and 
									u.RecordStatus = \'1\' 
									and  
									u.UserID = s.UserID
						  Group By
						    u.UserID) as d
						  on u.UserID = d.UserID
							';
		}
		$sql .= '	left join 
							CARD_STAFF_ATTENDANCE_USERGROUP as ug 
							on u.UserID = ug.UserID 
						where ';
		if (trim($Keyword) != "") {
			$sql .= 'u.UserID in ('.$UserList.') ';
		}
		else {
			$sql .= '	u.RecordType = \'1\' 
								and 
								u.RecordStatus = \'1\' 
								and 
								ug.GroupID IS NULL ';
		}
		$sql .= 'order by
						  u.EnglishName';
		
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	// $GroupType is not in used already - kenneth chung (2010-01-28)
	function Get_Group_List($Keyword="",$GetGroupStat=false,$GroupType="",$GetDutyPeriodStat=false,$GetSlotStat=false,$SearchSlotAlso=true,$Recordstatus="1") {
		if (trim($Keyword) != "") {
			$sql = 'select distinct 
								g.GroupID 
							From 
								CARD_STAFF_ATTENDANCE2_GROUP as g ';
			if ($SearchSlotAlso) {
				$sql .= '
								left join  
								CARD_STAFF_ATTENDANCE3_SLOT as s 
								on g.GroupID = s.GroupID ';
			}
			$sql .= '
							where 
								(
								g.GroupName like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								OR 
								g.GroupName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' ';
			if ($SearchSlotAlso) {
				$sql .= '
								OR 
								s.SlotName like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
								OR 
								s.SlotName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' ';
			}
			$sql .= '
								)';
			//debug_r($sql);
			$GroupIDList = '-1,'.implode(',',$this->returnVector($sql));
		}
		
		$sql = 'select
						  g.GroupID,
						  g.GroupName ';
		if ($GetGroupStat) {
			$sql .= ', 
						  CASE 
						  	WHEN b.NumberOfMember IS NULL THEN 0 
						  	ELSE b.NumberOfMember 
						  END as NumberOfMember ';
		}
		if ($GetDutyPeriodStat) {
			$sql .= ',
						  CASE 
						  	WHEN c.NumberOfDutyPeriod IS NULL THEN 0 
						  	ELSE c.NumberOfDutyPeriod 
						  END as NumberOfDutyPeriod ';
		}
		if ($GetSlotStat) {
			$sql .= ',
						  CASE 
						  	WHEN d.NumberOfSlot IS NULL THEN 0 
						  	ELSE d.NumberOfSlot 
						  END as NumberOfSlot ';
		}
		$sql .= 'from
						  CARD_STAFF_ATTENDANCE2_GROUP as g ';
		if ($GetGroupStat) {
			$sql .= 'LEFT join
						  (select
						    g.GroupID,
						    count(1) as NumberOfMember
						  From
						    CARD_STAFF_ATTENDANCE2_GROUP as g
						    INNER JOIN
						    CARD_STAFF_ATTENDANCE_USERGROUP as ug
							  on g.GroupID = ug.GroupID
							  INNER JOIN 
							  INTRANET_USER as u 
							  on 
							  	ug.UserID = u.UserID 
									and u.RecordStatus in ('.IntegerSafe($Recordstatus).') 
						  Group By
						    g.GroupID) as b
						  on g.GroupID = b.GroupID
							';
		}
		if ($GetDutyPeriodStat) {
			$sql .= 'LEFT JOIN 
							(select
						    g.GroupID,
						    count(1) as NumberOfDutyPeriod
						  From
						    CARD_STAFF_ATTENDANCE2_GROUP as g
						    INNER JOIN
						    CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD as ngsp
								on g.GroupID = ngsp.GroupID
						  Group By
						    g.GroupID) as c
						  on g.GroupID = c.GroupID
							';
		}
		if ($GetDutyPeriodStat) {
			$sql .= 'LEFT JOIN 
							(select
						    g.GroupID,
						    count(1) as NumberOfSlot
						  From
						    CARD_STAFF_ATTENDANCE2_GROUP as g
						    INNER JOIN
						    CARD_STAFF_ATTENDANCE3_SLOT as s
								on g.GroupID = s.GroupID 
						  Group By
						    g.GroupID) as d
						  on g.GroupID = d.GroupID
							';
		}
		if (trim($Keyword)!="") {	
			$sql .= 'where g.GroupID in ('.IntegerSafe($GroupIDList).') ';
		}
		$sql .= 'order by
						  g.GroupName';
		
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Get_Slot_List($ID="",$GroupOrIndividual="Group",$SlotIDAry='') {
		$sql = 'Select 
							SlotID, 
							SlotName,
							SlotStart,
							SlotEnd,
							IF(InWavie=1,\'Y\',\'N\') as InWavie,
							IF(OutWavie=1,\'Y\',\'N\') as OutWavie,
							DutyCount
						from 
							CARD_STAFF_ATTENDANCE3_SLOT 
						where ';
		if ($GroupOrIndividual=="Group") { // Group
			$sql .= 'GroupId = \''.IntegerSafe($ID).'\'';
		}
		else if ($GroupOrIndividual=="Individual") { // Other individual
			$sql .= 'UserID = \''.IntegerSafe($ID).'\'';
		}
		else { // template
			$sql .= 'TemplateID = \''.IntegerSafe($ID).'\'';
		}
		if($SlotIDAry != '' || (is_array($SlotIDAry) && count($SlotIDAry)>0)){
			$sql .= ' and SlotID IN ('.implode(",",IntegerSafe((array)$SlotIDAry)).') ';
		}
		$sql .= 'order by 
							SlotStart';
		return $this->returnArray($sql);
	}
	
	function Check_Group_Name($GroupName,$GroupID) {
		$sql = 'select 
							GroupID 
						from 
							CARD_STAFF_ATTENDANCE2_GROUP 
						where 
							GroupID != \''.$GroupID.'\' 
							and 
							GroupName = \''.$this->Get_Safe_Sql_Query($GroupName).'\'';
		return sizeof($this->returnVector($sql)) > 0? false:true;
	}
	
	function Rename_Group($GroupName,$GroupID) {
		$sql = 'update CARD_STAFF_ATTENDANCE2_GROUP set 
							GroupName = \''.$this->Get_Safe_Sql_Query($GroupName).'\', 
							DateModified = NOW(),
							ModifyBy = \''.$_SESSION['UserID'].'\'
						where 
							GroupID = \''.$GroupID.'\'';
		//debug_r($sql);
		return $this->db_db_query($sql);
	}
	
	function Check_Slot_Name($SlotName,$GroupID="",$TargetUserID="",$SlotID="",$TemplateID="") {
		if ($GroupID != "") {
			$cond = " GroupID = '".$GroupID."' ";
		}
		else if ($TargetUserID != "") {
			$cond = " UserID = '".$TargetUserID."' ";
		}
		else {
			$cond = " TemplateID = '".$TemplateID."' ";
		}
		
		if ($SlotID != "") {
			$cond .= " and SlotID != '".$SlotID."' ";
		}
		$sql = 'select 
							SlotID 
						from 
							CARD_STAFF_ATTENDANCE3_SLOT 
						where 
							'.$cond.' 
							and 
							SlotName = \''.$this->Get_Safe_Sql_Query($SlotName).'\'';
		//debug_r($sql);
		return (sizeof($this->returnVector($sql)) > 0)? false:true;
	}
	
	function Save_Group($GroupName) {
		$sql= 'insert into CARD_STAFF_ATTENDANCE2_GROUP 
						(
						GroupName,
						DateInput,
						InputBy
						)
					values 
						(
						\''.$this->Get_Safe_Sql_Query($GroupName).'\',
						NOW(),
						\''.$_SESSION['UserID'].'\'
						)';
		return $this->db_db_query($sql);
	}
	
	function Save_Slot($GroupID,$TargetUserID,$SlotID,$SlotName,$SlotStart,$SlotEnd,$InWavie,$OutWavie,$DutyCount,$TemplateID) {
		if ($SlotID == "") {
			$sql = 'insert into CARD_STAFF_ATTENDANCE3_SLOT 
								(
								GroupID,
								UserID,
								TemplateID,
								SlotName,
								SlotStart,
								SlotEnd,
								DutyCount,
								InWavie,
								OutWavie,
								DateInput,
								InputBy,
								DateModify,
								ModifyBy
								)
							values 
								(
								\''.$GroupID.'\',
								\''.$TargetUserID.'\',
								\''.$TemplateID.'\',
								\''.$this->Get_Safe_Sql_Query($SlotName).'\',
								\''.$SlotStart.'\',
								\''.$SlotEnd.'\',
								\''.$DutyCount.'\',
								\''.$InWavie.'\',
								\''.$OutWavie.'\',
								NOW(),
								\''.$_SESSION['UserID'].'\',
								NOW(),
								\''.$_SESSION['UserID'].'\'
								)';
		}
		else {
			$sql = 'update CARD_STAFF_ATTENDANCE3_SLOT set 
								SlotName = \''.$this->Get_Safe_Sql_Query($SlotName).'\',
								SlotStart = \''.$SlotStart.'\',
								SlotEnd = \''.$SlotEnd.'\',
								DutyCount = \''.$DutyCount.'\',
								InWavie = \''.$InWavie.'\',
								OutWavie = \''.$OutWavie.'\',
								DateModify = NOW(),
								ModifyBy = \''.$_SESSION['UserID'].'\'
							where 
								SlotID = \''.$SlotID.'\'';
		}
		
		//debug_r($sql);
		return $this->db_db_query($sql);
	}
	
	function Delete_Slot($SlotID) {
		global $sys_custom;
		if (is_array($SlotID)) 
			$SlotID = implode(',',$SlotID);
		
		$Result = array();
		
		$sql = 'Delete from CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY where SlotID in ('.$SlotID.')';
		$Result['delete-CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		$sql = 'Delete from CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT where SlotID in ('.$SlotID.')';
		$Result['delete-CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		$sql = 'Delete from CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY where SlotID in ('.$SlotID.')';
		$Result['delete-CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		$sql = 'Delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD where SlotID in ('.$SlotID.')';
		$Result['delete-CARD_STAFF_ATTENDANCE2_LEAVE_RECORD'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		$sql = 'Delete from CARD_STAFF_ATTENDANCE3_SLOT where SlotID in ('.$SlotID.')';
		$Result['delete-CARD_STAFF_ATTENDANCE3_SLOT'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $this->Log_Record($log_content);
		
		return (!in_array(false,$Result));
	}
	
	function Get_Aval_User_List($Keyword="",$Recordstatus="1",$GetArchiveUser=false,$monitoring=false) {
		//$NameField = getNameFieldByLang("u.");
		$NameField = $this->Get_Name_Field("u.");

		if ($monitoring) {
	  		$Monitoring_Members = $this->Get_Monitoring_Members();
	  		if (count($Monitoring_Members) > 0) {
	  			$monitoring_cond = " AND u.UserID IN (".implode(',',$Monitoring_Members).") ";
	  		}
	  		else {
	  			$monitoring_cond = '';
	  		}
		}
		else {
			$monitoring_cond = '';
		}
		
		$sql = 'select 
							u.UserID,
							'.$NameField.' as StaffName,
							u.EnglishName,
							u.UserLogin  
						From 
							INTRANET_USER u 
							left join 
							CARD_STAFF_ATTENDANCE_USERGROUP ug 
							on u.UserID = ug.UserID 
						where 
							u.RecordType = \'1\' 
							and 
							u.RecordStatus IN ('.$Recordstatus.')
							'.$monitoring_cond.'
							and 
							ug.GroupID IS NULL ';
		if($Keyword != null && trim($Keyword) != ""){
			$sql .= 'and '.$NameField.' LIKE \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' ';
		} 
		if($GetArchiveUser) {
			$ArchiveNameField = $this->Get_Archive_Name_Field("u.");
			$sql .= 'UNION (select 
							u.UserID,
							'.$ArchiveNameField.' as StaffName,
							u.EnglishName,
							u.UserLogin  
						From 
							INTRANET_ARCHIVE_USER u 
							left join 
							CARD_STAFF_ATTENDANCE_USERGROUP ug 
							on u.UserID = ug.UserID 
						where 
							u.RecordType = \'1\'
							'.$monitoring_cond.' 
							and 
							ug.GroupID IS NULL ';
			if($Keyword != null && trim($Keyword) != ""){
				$sql .= 'and '.$ArchiveNameField.' LIKE \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' ';
			}
			$sql .= ')';
		}
				$sql .= 'order by 
							EnglishName';
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Add_Member($GroupID,$AddUserID) {
		global $sys_custom;
		$AddUserID = (is_array($AddUserID))? $AddUserID:array();
		$CurrentMember = $this->Get_Group_Member_List($GroupID);
		$DeleteMember = array();
		$UnChangeMember = array();
		
		for ($i=0; $i< sizeof($CurrentMember); $i++) {
			if (in_array($CurrentMember[$i]['UserID'],$AddUserID)) {
				$UnChangeMember[] = $CurrentMember[$i]['UserID'];
			}
			else {
				$DeleteMember[] = $CurrentMember[$i]['UserID'];
			}
		}
		
		if (sizeof($DeleteMember) > 0) 
			$Result['RemoveMember'] = $this->Remove_Member($GroupID,$DeleteMember);
		
		$ReallyAddUser = array_diff($AddUserID,$UnChangeMember);
		if (sizeof($ReallyAddUser) > 0) {
			$StaffList = implode(',',$ReallyAddUser);
			$sql = 'delete from CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY where UserID in ('.$StaffList.')';
			$Result['delete-CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY'] = $this->db_db_query($sql);
			if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
			
			$sql = 'select WeekDutyID from CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY where UserID in ('.$StaffList.')';
			$WeekDutyIDList = $this->returnVector($sql);
			
			$sql = 'delete from CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY where UserID in ('.$StaffList.')';
			$Result['delete-CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY'] = $this->db_db_query($sql);
			if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
			
			if (sizeof($WeekDutyIDList) > 0) {
				$sql = 'delete from CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT where WeekDutyID in ('.implode(',',$WeekDutyIDList).')';
				$Result['delete-CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT'] = $this->db_db_query($sql);
				if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
			}
			
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD where StaffID in ('.$StaffList.') and SlotID IS NOT NULL and SlotID <> 0';
			$Result['delete-CARD_STAFF_ATTENDANCE2_LEAVE_RECORD'] = $this->db_db_query($sql);
			if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
			
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY where UserID in ('.$StaffList.')';
			$Result['delete-CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY'] = $this->db_db_query($sql);
			if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
			
			$sql = 'delete from CARD_STAFF_ATTENDANCE3_SLOT where UserID in ('.$StaffList.')';
			$Result['delete-CARD_STAFF_ATTENDANCE3_SLOT'] = $this->db_db_query($sql);
			if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
			
			if($sys_custom['StaffAttendance']['LogDeleteDuty']) $this->Log_Record($log_content);
		}
		
		foreach ($ReallyAddUser as $Key => $StaffID) {
			$sql = 'Insert into CARD_STAFF_ATTENDANCE_USERGROUP 
								(GroupID,UserID) 
								values 
								(\''.$GroupID.'\',\''.$StaffID.'\')';
			$Result['Add-'.$StaffID] = $this->db_db_query($sql);
		}
		
		//debug_r($Result);
		return (!in_array(false,$Result));
	}
	
	function Remove_Member($GroupID,$StaffID) {
		global $sys_custom;
		$StaffList = implode(',',$StaffID);
		$sql = 'delete from CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY where UserID in ('.$StaffList.')';
		$Result['delete-CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		$sql = 'select WeekDutyID from CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY where UserID in ('.$StaffList.')';
		$WeekDutyIDList = $this->returnVector($sql);
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY where UserID in ('.$StaffList.')';
		$Result['delete-CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		if (sizeof($WeekDutyIDList) > 0) {
			$sql = 'delete from CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT where WeekDutyID in ('.implode(',',$WeekDutyIDList).')';
			$Result['delete-CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT'] = $this->db_db_query($sql);
			if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		}
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD where StaffID in ('.$StaffList.') and SlotID IS NOT NULL and SlotID <> 0';
		$Result['delete-CARD_STAFF_ATTENDANCE2_LEAVE_RECORD'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY where UserID in ('.$StaffList.')';
		$Result['delete-CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE_USERGROUP where UserID in ('.$StaffList.')';
		$Result['delete-CARD_STAFF_ATTENDANCE_USERGROUP'] = $this->db_db_query($sql);
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $log_content .= $sql;
		
		if($sys_custom['StaffAttendance']['LogDeleteDuty']) $this->Log_Record($log_content);
		//debug_r($Result);
		return (!in_array(false,$Result));
	}
	
	function Delete_Group($GroupID) {
		$sql = 'select UserID from CARD_STAFF_ATTENDANCE_USERGROUP where GroupID = \''.$GroupID.'\'';
		$StaffList = $this->returnVector($sql);
		
		if (sizeof($StaffList) > 0) {
			$Result['Remove-Members'] = $this->Remove_Member($GroupID,$StaffList);
			
			$sql = 'delete from CARD_STAFF_ATTENDANCE_USERGROUP where GroupID = \''.$GroupID.'\'';
			$Result['delete-CARD_STAFF_ATTENDANCE_USERGROUP'] = $this->db_db_query($sql);
		}
		
		$sql = 'select SlotId from CARD_STAFF_ATTENDANCE3_SLOT where GroupID = \''.$GroupID.'\'';
		$SlotList = $this->returnVector($sql);
		
		if (sizeof($SlotList) > 0) {
			$Result['Remove-Slots'] = $this->Delete_Slot($SlotList);
		}
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD where GroupID = \''.$GroupID.'\'';
		$Result['Delete-CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD'] = $this->db_db_query($sql);
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE2_GROUP where GroupID = \''.$GroupID.'\'';
		$Result['Delete-CARD_STAFF_ATTENDANCE2_GROUP'] = $this->db_db_query($sql);
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET where ObjID = \''.$GroupID.'\' and GroupOrIndividual = \'G\'';
		$Result['Delete-CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET'] = $this->db_db_query($sql);
		
		//debug_r($Result);		
		return (!in_array(false,$Result));
	}
	
	function Get_Preset_Leave_Reason_List($TypeID="", $Keyword="", $ActiveOnly=true, $ReturnAssoc=false)
	{
		$cond = (!empty($TypeID)) ? " AND a.ReasonType = '".$this->Get_Safe_Sql_Query($TypeID)."'" : "";
		$cond .= ($ActiveOnly==true)? " AND a.ReasonActive = 1 " : "";
		
		$sql = "SELECT
					a.ReasonID,
					a.ReasonType,
					a.ReasonText,
					a.ReasonDescription,
					a.ReportSymbol,
					a.DefaultWavie,
					a.ReasonActive, 
					a.DefaultFlag,
					a.ReasonColor  
				FROM
					CARD_STAFF_ATTENDANCE3_REASON AS a
				WHERE
					(a.ReasonText LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
					 OR a.ReasonDescription LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%') 
					$cond ";
		$records = $this->returnArray($sql);
		
		if($ReturnAssoc){
			$ary = array();
			$record_count = count($records);
			for($i=0;$i<$record_count;$i++){
				$ary[$records[$i]['ReasonID']] = $records[$i];
			}
			return $ary;
		}
		return $records;
	}
	
	function Get_Preset_Leave_Reason_Detail($ReasonID)
	{
		$sql = "SELECT
					ReasonType,
					ReasonText,
					ReasonDescription,
					ReportSymbol,
					DefaultWavie,
					ReasonActive,
					DefaultFlag,
					ReasonColor   
				FROM
					CARD_STAFF_ATTENDANCE3_REASON
				WHERE
					ReasonID = '$ReasonID' ";
		
		return $this->returnArray($sql);
	}
	
	function Save_Preset_Leave_Reason($TypeID,$ReasonID,$ReasonText,$ReasonDescription, $Waived, $ReportSymbol, $ReasonActive,$SetAsDefault, $ReasonColor='')
	{
		if ($SetAsDefault == "1") {
			$sql = "update CARD_STAFF_ATTENDANCE3_REASON set 
								DefaultFlag = '0' 
							where 
								ReasonType = '".$TypeID."'";
			$Result["ResetDefault"] = $this->db_db_query($sql);
		}
		if($ReasonID != null && $ReasonID != "" && $ReasonID > 0)
		{
			# Update Reason
			$sql = "UPDATE CARD_STAFF_ATTENDANCE3_REASON
					SET 
						ReasonText = '".$this->Get_Safe_Sql_Query($ReasonText)."',
						ReasonDescription = '".$this->Get_Safe_Sql_Query($ReasonDescription)."',
						ReportSymbol = '".$this->Get_Safe_Sql_Query($ReportSymbol)."',
						DefaultWavie = '$Waived',
						ReasonActive = '$ReasonActive',
						DefaultFlag = '".$SetAsDefault."', 
						ReasonColor = '$ReasonColor',
						DateModify = NOW(),
						ModifyBy = '".$_SESSION['UserID']."'
					WHERE ReasonType = $TypeID && ReasonID=$ReasonID ";
		}else
		{
			# Insert New Reason
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE3_REASON
					(ReasonType, ReasonText, ReasonDescription, ReportSymbol, DefaultWavie, ReasonActive, DefaultFlag, ReasonColor, DateInput, DateModify, InputBy, ModifyBy) 
					VALUES('$TypeID',
						   '".$this->Get_Safe_Sql_Query($ReasonText)."',
						   '".$this->Get_Safe_Sql_Query($ReasonDescription)."',
						   '".$this->Get_Safe_Sql_Query($ReportSymbol)."',
						   '$Waived',
						   '$ReasonActive',
						   '".$SetAsDefault."',
						   '$ReasonColor',
						   NOW(),NOW(),
						   '".$_SESSION['UserID']."',
						   '".$_SESSION['UserID']."')";
		}
		$Result["InsertUpdateReason"] = $this->db_db_query($sql);
		
		return !in_array(false,$Result);
	}
	
	function Delete_Preset_Leave_Reason($ReasonID)
	{
		$sql = "DELETE FROM CARD_STAFF_ATTENDANCE3_REASON
				WHERE ReasonID = '$ReasonID' ";
		
		return $this->db_db_query($sql);
	}
	
	function Check_Preset_Leave_Reason($ReasonText, $TypeID, $ReasonID, $ReportSymbol)
	{
		$result = array();
		// Can use same reason symbol among statuses and reasons
		/*
		if(trim($ReportSymbol) != "")
		{
			$sql = "SELECT 
						ReasonID
					FROM 
						CARD_STAFF_ATTENDANCE3_REASON
					WHERE
						ReasonID != '$ReasonID' 
						AND ReportSymbol = '".$this->Get_Safe_Sql_Query($ReportSymbol)."'";
			
			$result[] = sizeof($this->returnVector($sql)) > 0?false:true;
		}
		*/
		$sql = "SELECT 
					ReasonID
				FROM 
					CARD_STAFF_ATTENDANCE3_REASON
				WHERE
					ReasonID != '$ReasonID' 
					AND ReasonType = '$TypeID' 
					AND ReasonText = '".$this->Get_Safe_Sql_Query($ReasonText)."' 
				 ";
		$result[] = sizeof($this->returnVector($sql)) > 0?false:true;
		
		return !in_array(false, $result);
	}

	function Check_Preset_Leave_Reason_Active($ReasonID)
	{
		$found = 0;
		
		$sql = "SELECT 
				  COUNT(ReasonID) as NoOfReason
				FROM
				  CARD_STAFF_ATTENDANCE2_PROFILE
				WHERE
				  ReasonID = '$ReasonID' ";
		$temp = $this->returnVector($sql);
		$found += $temp[0];
		
		$sql = "SELECT
				  COUNT(ReasonID) as NoOfReason
				FROM
				  CARD_STAFF_ATTENDANCE2_LEAVE_RECORD
				WHERE
				  ReasonID = '$ReasonID' ";
		$temp = $this->returnVector($sql);
		$found += $temp[0];		  
		
		return ($found > 0)?true:false;
	}
	
	function Get_Preset_Leave_Reason_Active_Count()
	{
		$ary = array();
		$sql = "SELECT ReasonID, COUNT(ReasonID) as ReasonCount 
				FROM CARD_STAFF_ATTENDANCE2_PROFILE 
				WHERE ReasonID IS NOT NULL AND ReasonID > 0
				GROUP BY ReasonID";
		$list1 = $this->returnResultSet($sql);
		$list1_count = count($list1);
		
		$sql = "SELECT 
				  ReasonID, COUNT(ReasonID) as ReasonCount 
				FROM 
				  CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
				WHERE ReasonID IS NOT NULL AND ReasonID > 0
				GROUP BY ReasonID";
		$list2 = $this->returnResultSet($sql);
		$list2_count = count($list2);
		
		for($i=0;$i<$list1_count;$i++){
			$id = $list1[$i]['ReasonID'];
			$count = $list1[$i]['ReasonCount'];
			
			if(!isset($ary[$id])) $ary[$id] = 0;
			$ary[$id] += $count;
		}
		
		for($i=0;$i<$list2_count;$i++){
			$id = $list2[$i]['ReasonID'];
			$count = $list2[$i]['ReasonCount'];
			
			if(!isset($ary[$id])) $ary[$id] = 0;
			$ary[$id] += $count;
		}
		
		return $ary;
	}
	
	function Get_Attendance_Group_Slot_List($TargetID, $Year, $Month, $Day, $GroupOrIndividual="Group")
	{
  		$Year = ($Year == "") ? date("Y") : $Year;
	  	$Month = ($Month == "") ? date("m") : $Month;
	
	  	$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$Year."_".$Month;
  		//$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$DateStr = $Year."-".$Month."-".$Day; 
  		
  		if($GroupOrIndividual=="Group")
  		{
			$sql = "SELECT
						h.StaffID as UserID,
						h.SlotName,
						h.DutyStart as SlotStart,
						h.DutyEnd as SlotEnd
					FROM
					 	CARD_STAFF_ATTENDANCE2_GROUP as c
					 	INNER JOIN CARD_STAFF_ATTENDANCE_USERGROUP as u ON c.GroupID = u.GroupID AND u.GroupID = '$TargetID' 
					 	INNER JOIN INTRANET_USER as iu ON iu.UserID = u.UserID AND iu.RecordType = 1 AND iu.RecordStatus = 1  
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
					 	ON wp.UserID=u.UserID 
					 		AND '".$DateStr."' BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					 	INNER JOIN $card_log_table_name as h on h.DayNumber = '$Day' AND h.StaffID = u.UserID AND h.SlotName IS NOT NULL 
					WHERE 
						c.GroupID = '$TargetID' 
					GROUP BY
						c.GroupID, h.SlotName
					ORDER BY
						h.DutyStart ";
  		}else
  		{
  			$sql = "SELECT
						h.StaffID as UserID,
						h.SlotName,
						h.DutyStart as SlotStart,
						h.DutyEnd as SlotEnd
					FROM
					 	$card_log_table_name as h 
						INNER JOIN INTRANET_USER as u ON u.UserID = h.StaffID AND u.RecordType = 1 AND u.RecordStatus = 1 
					 	INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
					 	ON 
					 		h.StaffID = '".$TargetID."' 
					 		AND h.DayNumber = '".$Day."' 
							AND h.SlotName IS NOT NULL 
					 		AND wp.UserID=h.StaffID 
					 		AND '".$DateStr."' BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					GROUP BY
						h.SlotName
					ORDER BY
						h.DutyStart ";
  		}
		return $this->returnArray($sql);
	}
	
	function Get_Attendance_Record_Group_List_Overview($GroupID, $Year, $Month, $Day, $Keyword="")
	{
		global $Lang;
  		$CARD_STATUS_LATEEARLYLEAVE = 6;
  		$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
  		
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$DateStr = $Year."-".$Month."-".$Day; 
  		// Get Slots' Details
		$sql = "SELECT
					h.SlotName,
					h.DutyStart as SlotStart,
					h.DutyEnd as SlotEnd
				FROM
				 	CARD_STAFF_ATTENDANCE2_GROUP as c
				 	INNER JOIN CARD_STAFF_ATTENDANCE_USERGROUP as u ON c.GroupID = u.GroupID AND u.GroupID = '".$this->Get_Safe_Sql_Query($GroupID)."'
				 	INNER JOIN INTRANET_USER as iu ON iu.UserID = u.UserID AND iu.RecordType = 1 AND iu.RecordStatus = 1 
					INNER JOIN $card_log_table_name as h on h.DayNumber = '$Day' AND h.StaffID = u.UserID AND h.SlotName IS NOT NULL
				WHERE 
					c.GroupID = '".$this->Get_Safe_Sql_Query($GroupID)."'  
				GROUP BY
					c.GroupID, h.SlotName 
				ORDER BY h.DutyStart";
		$SlotDetails = $this->returnArray($sql);
		$CardLogUserID = array();// store users that have cardlogs
		$UserIDToInfo = array();// UserID as key to store staff name
		$SlotNames = array();// store slot names
		$SlotNamesQuoted = array();// double quoted slot names used in query
		for($i=0;$i<sizeof($SlotDetails);$i++)
		{
			$SlotNames[$i] = $SlotDetails[$i]['SlotName'];
			$SlotNamesQuoted[$i] = "'".$this->Get_Safe_Sql_Query($SlotDetails[$i]['SlotName'])."'";
		}
		
  		$Unassigned = array();// store user ids that do not have duties, holidays and outings
  		for($i=0;$i<sizeof($SlotNames);$i++)
  		{
  			$SlotName = $SlotNames[$i];
  			$ConfirmedUser[$SlotName] = array();// store confirmed user IDs for each slot
  			$UnconfirmedUser[$SlotName] = array();// store unconfirmed user IDs for each slot
  			$HolidayUser[$SlotName] = array();//store holiday user IDs for each slot
  			$OutgoingUser[$SlotName] = array();// store outing user IDs for each slot
  			$FullHolidayUser = array();// store user IDs that are full day holiday 
  			$FullOutgoingUser = array();// store user IDs that are full day outing
  			$UnassignedUser[$SlotName] = array();// store user IDs that do not have duties, holidays and outing duties for each slot
  			$LastUpdateDate[$SlotName] = '';// store latest updated datetime for each slot
  			$LastUpdateBy[$SlotName] = '';// store latest updated user ID for each slot
  			$LateEarlyleaveUser[$SlotName] = array();//store late and early leave user IDs for each slot
  			$PresentUser[$SlotName] = array();// store present user IDs for each slot
  			$AbsentUser[$SlotName] = array();// store absent user IDs for each slot
  			$LateUser[$SlotName] = array();// store late user IDs for each slot
  			$EarlyleaveUser[$SlotName] = array();// store early leave user IDs for each slot
  			$AbsentSuspectedUser[$SlotName] = array();// store absent suspected user IDs for each slot
  			$OffDutyUser[$SlotName] = array(); // store potential off duty users for each time slot
  		}
  		$NameField = getNameFieldByLang("a.");
  		$UpdaterNameField = getNameFieldByLang("u.");
  		// Get users detail for that group
  		$sql = "SELECT
				  a.UserID,
				  $NameField as StaffName,
				  c.GroupID,
				  c.GroupName,
				  h.SlotName,
				  h.RecordStatus,
				  h.RecordType,
				  h.RecordID,
				  IF(h.InSchoolStatus IS NULL,'-1',h.InSchoolStatus) as InSchoolStatus,
				  IF(h.OutSchoolStatus IS NULL,'-1',h.OutSchoolStatus) as OutSchoolStatus,
				  wp.WorkingPeriodID,
				  DATE_FORMAT(h.DateConfirmed,'%Y-%m-%d %H:%i:%s') as DateConfirmed,
				  $UpdaterNameField as ConfirmBy 
				FROM
				  INTRANET_USER as a
				  INNER JOIN CARD_STAFF_ATTENDANCE_USERGROUP as b on a.UserID = b.UserID
				  INNER JOIN CARD_STAFF_ATTENDANCE2_GROUP as c on c.GroupID = b.GroupID
				  LEFT JOIN $card_log_table_name as h on h.DayNumber = '".$this->Get_Safe_Sql_Query($Day)."' AND h.SlotName IN (".((sizeof($SlotNamesQuoted)>0)?implode(",",$SlotNamesQuoted):"").") AND h.StaffID = a.UserID 
				  LEFT JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=a.UserID AND DATE_FORMAT(CONCAT('".$this->Get_Safe_Sql_Query($Year."-".$Month."-','".$Day)."'),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
				  LEFT JOIN INTRANET_USER as u ON u.UserID = h.ConfirmBy
				WHERE 
				  c.GroupID = '".$this->Get_Safe_Sql_Query($GroupID)."' AND a.RecordType = '1' AND a.RecordStatus = '1' 
				GROUP BY
				  h.SlotName, a.UserID 
				ORDER BY a.EnglishName ";
  		//echo "<PRE>".$sql."</PRE>";
  		$UsersDetail = $this->returnArray($sql);
  		$TotalUsers = sizeof($UsersDetail);
  		
  		// Get users with leave settings
  		$sql = "SELECT 
					a.UserID,
					$NameField as StaffName,
					h.InSchoolStatus,
					h.RecordType
				FROM 
					INTRANET_USER as a
				  	INNER JOIN CARD_STAFF_ATTENDANCE_USERGROUP as b on a.UserID = b.UserID
				  	INNER JOIN CARD_STAFF_ATTENDANCE2_GROUP as c on c.GroupID = b.GroupID
				  	INNER JOIN $card_log_table_name as h on h.DayNumber = '".$this->Get_Safe_Sql_Query($Day)."' AND h.StaffID = a.UserID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=a.UserID AND DATE_FORMAT(CONCAT('".$this->Get_Safe_Sql_Query($Year."-".$Month)."-',h.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
				WHERE
					c.GroupID = '".$this->Get_Safe_Sql_Query($GroupID)."'
					AND h.SlotName IS NULL 
					AND (h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' 
						OR h.InSchoolStatus = '".CARD_STATUS_OUTGOING."'
						OR h.RecordType = '".CARD_STATUS_HOLIDAY."'
						OR h.RecordType = '".CARD_STATUS_OUTGOING."') 
					AND a.RecordType = '1' AND a.RecordStatus = '1'
				ORDER BY 
					a.EnglishName ";
		
  		$FullHolidayAndFullOutgoing = $this->returnArray($sql);
  		
  		for($i=0;$i<sizeof($UsersDetail);$i++)
  		{
  			$SlotName = $UsersDetail[$i]['SlotName'];
  			$TargetUserID = $UsersDetail[$i]['UserID'];
  			$StaffName = $UsersDetail[$i]['StaffName'];
  			if(trim($UsersDetail[$i]['WorkingPeriodID'])=='') continue;
  			$UserIDToInfo[$TargetUserID] = $StaffName;
  			if(trim($UsersDetail[$i]['RecordID']) == '' || trim($SlotName) == '')// no cardlog, probable unassigned candidates
  			{
  				//$UnassignedUser[$SlotName][] = $TargetUserID;
  				$Unassigned[] = $TargetUserID;
  			}else
  			{
	  			$CardLogUserID[] = $TargetUserID;
	  			if($UsersDetail[$i]['RecordStatus'] == 1)// cardlog confirmed
	  			{
	  				$ConfirmedUser[$SlotName][] = $TargetUserID;
	  				if(trim($UsersDetail[$i]['DateConfirmed'])!=''){
	  					if(trim($LastUpdateDate[$SlotName])==''){
	  						$LastUpdateDate[$SlotName] = $UsersDetail[$i]['DateConfirmed'];
	  						$LastUpdateBy[$SlotName] = $UsersDetail[$i]['ConfirmBy'];
	  					}else if($UsersDetail[$i]['DateConfirmed'] > $LastUpdateDate[$SlotName]){
	  						$LastUpdateDate[$SlotName] = $UsersDetail[$i]['DateConfirmed'];
	  						$LastUpdateBy[$SlotName] = $UsersDetail[$i]['ConfirmBy'];
	  					}
	  				}
	  			}else // cardlog not confirmed yet
	  			{
	  				$UnconfirmedUser[$SlotName][] = $TargetUserID;
	  			}
	  			
	  			if($UsersDetail[$i]['InSchoolStatus'] == CARD_STATUS_HOLIDAY){
	  				$HolidayUser[$SlotName][] = $TargetUserID;
	  			}else if($UsersDetail[$i]['InSchoolStatus'] == CARD_STATUS_OUTGOING){
	  				$OutgoingUser[$SlotName][] = $TargetUserID;
	  			}else if($UsersDetail[$i]['InSchoolStatus'] == CARD_STATUS_LATE && $UsersDetail[$i]['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE){
	  				$LateEarlyleaveUser[$SlotName][] = $TargetUserID;
	  			}else if($UsersDetail[$i]['InSchoolStatus'] == CARD_STATUS_LATE){
	  				$LateUser[$SlotName][] = $TargetUserID;
	  			}else if($UsersDetail[$i]['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE){
	  				$EarlyleaveUser[$SlotName][] = $TargetUserID;
	  			}else if($UsersDetail[$i]['InSchoolStatus'] == CARD_STATUS_ABSENT){
	  				$AbsentUser[$SlotName][] = $TargetUserID;
	  			}else if($UsersDetail[$i]['InSchoolStatus'] == CARD_STATUS_NORMAL || $UsersDetail[$i]['OutSchoolStatus'] == CARD_STATUS_NORMAL){
	  				$PresentUser[$SlotName][] = $TargetUserID;
	  			}else{
	  				$AbsentSuspectedUser[$SlotName][] = $TargetUserID;
	  			}
  			}
  		}
  		
  		for($i=0;$i<sizeof($FullHolidayAndFullOutgoing);$i++)
  		{
  			$UserIDToInfo[$FullHolidayAndFullOutgoing[$i]['UserID']] = $FullHolidayAndFullOutgoing[$i]['StaffName'];
  			if($FullHolidayAndFullOutgoing[$i]['InSchoolStatus'] == CARD_STATUS_HOLIDAY || $FullHolidayAndFullOutgoing[$i]['RecordType']==CARD_STATUS_HOLIDAY)
  			{
  				$FullHolidayUser[] = $FullHolidayAndFullOutgoing[$i]['UserID'];
  			}else
  			{
  				$FullOutgoingUser[] = $FullHolidayAndFullOutgoing[$i]['UserID'];
  			}
  		}
  		
  		/* comment by kenneth chung on 20101006
  		for($i=0;$i<sizeof($SlotNames);$i++)
  		{
  			$SlotName = $SlotNames[$i];
  			if($i==0)
  			{
  				$FullHolidayUser = $HolidayUser[$SlotName];
  				$FullOutgoingUser = $OutgoingUser[$SlotName];
  			}else
  			{
  				$FullHolidayUser = array_intersect($FullHolidayUser, $HolidayUser[$SlotName]);
  				$FullOutgoingUser = array_intersect($FullOutgoingUser, $OutgoingUser[$SlotName]);
  			}
  		}*/
  		
  		for($i=0;$i<sizeof($SlotNames);$i++){
  			$SlotName = $SlotNames[$i];
  			$OffDutyUser[$SlotName] = array_keys($UserIDToInfo);
  		}
  		
  		if(sizeof($FullHolidayUser)>0)
  		{
  			$FullHolidayUser = array_values(array_unique($FullHolidayUser));
  			$Unassigned = array_diff($Unassigned,$FullHolidayUser);
  		}
  		if(sizeof($FullOutgoingUser)>0)
  		{
  			$FullOutgoingUser = array_values(array_unique($FullOutgoingUser));
  			$Unassigned = array_diff($Unassigned, $FullOutgoingUser);
  		}
  		if(sizeof($CardLogUserID)>0) $Unassigned = array_diff($Unassigned, $CardLogUserID);
  		$Unassigned = array_values(array_unique($Unassigned));
  		
  		for($i=0;$i<sizeof($SlotNames);$i++)
  		{
  			$SlotName = $SlotNames[$i];
  			$UnassignedUser[$SlotName] = array_diff($OffDutyUser[$SlotName],$ConfirmedUser[$SlotName]);
  			$UnassignedUser[$SlotName] = array_diff($UnassignedUser[$SlotName],$UnconfirmedUser[$SlotName]);
  			$UnassignedUser[$SlotName] = array_diff($UnassignedUser[$SlotName],$FullHolidayUser);
  			$UnassignedUser[$SlotName] = array_diff($UnassignedUser[$SlotName],$FullOutgoingUser);
  			$UnassignedUser[$SlotName] = array_values($UnassignedUser[$SlotName]);
  		}
 		
  		$Return = array(
  						'SlotDetail' => $SlotDetails, 
  						'UserInfo' => $UserIDToInfo,
  						'ConfirmedUser' => $ConfirmedUser,
  						'UnconfirmedUser' => $UnconfirmedUser,
  						'HolidayUser' => $HolidayUser,
  						'OutgoingUser' => $OutgoingUser,
  						'PresentUser' => $PresentUser,
  						'AbsentUser' => $AbsentUser,
  						'LateUser' => $LateUser,
  						'LateEarlyleaveUser' => $LateEarlyleaveUser,
  						'EarlyleaveUser' => $EarlyleaveUser,
  						'AbsentSuspectedUser' => $AbsentSuspectedUser,
  						'FullHolidayUser' => $FullHolidayUser,
  						'FullOutgoingUser' => $FullOutgoingUser,
  						'Unassigned' => $Unassigned,
  						'UnassignedUser' => $UnassignedUser,
  						'LastUpdateDate' => $LastUpdateDate,
  						'LastUpdateBy' => $LastUpdateBy
  				);
  		
  		return $Return;
	}
	
  	# Get attendance data of staffs in specific group and slot
  	function Get_View_Attendance_Record_Group_List($GroupID, $SlotName, $Year, $Month, $Day, $Keyword="", $StatusType="")
  	{
  		global $Lang;
  		$CARD_STATUS_LATEEARLYLEAVE = 6;
  		$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
  		
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$DateStr = $Year."-".$Month."-".$Day; 
  		$Weekday = date("w", mktime(0, 0, 0, $Month, $Day, $Year));
  		
  		if($StatusType == CARD_STATUS_NORMAL)
  		{
  			$status_cond = " AND ((h.InSchoolStatus = '".CARD_STATUS_NORMAL."' AND (h.OutSchoolStatus IS NULL OR h.OutSchoolStatus != '".CARD_STATUS_EARLYLEAVE."')) OR (h.OutSchoolStatus = '".CARD_STATUS_NORMAL."' AND (h.InSchoolStatus IS NULL OR h.InSchoolStatus = '-1'))) ";
  		}else if($StatusType == CARD_STATUS_ABSENT)
  		{
  			$status_cond = " AND (h.InSchoolStatus = '".CARD_STATUS_ABSENT."')";
  		}else if($StatusType == CARD_STATUS_LATE)
  		{
  			$status_cond = " AND (h.InSchoolStatus = '".CARD_STATUS_LATE."' AND (h.OutSchoolStatus IS NULL OR h.OutSchoolStatus != '".CARD_STATUS_EARLYLEAVE."')) ";
  		}else if($StatusType == CARD_STATUS_EARLYLEAVE)
  		{
  			$status_cond = " AND (h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' AND (h.InSchoolStatus IS NULL OR h.InSchoolStatus != '".CARD_STATUS_LATE."')) ";
  		}else if($StatusType == CARD_STATUS_OUTGOING)
  		{
  			$status_cond = " AND (h.InSchoolStatus = '".CARD_STATUS_OUTGOING."')";
  		}else if($StatusType == $CARD_STATUS_LATEEARLYLEAVE)
  		{
  			$status_cond = " AND (h.InSchoolStatus = '".CARD_STATUS_LATE."' AND h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."')";
  		}else if($StatusType == CARD_STATUS_HOLIDAY)
  		{
  			$status_cond = " AND ( h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."')";
  		}else if($StatusType == $CARD_STATUS_ABSENT_SUSPECTED)
  		{
  			$status_cond = " AND h.Duty = '1' AND ((h.InSchoolStatus IS NULL AND h.OutSchoolStatus IS NULL) OR (h.InSchoolStatus='-1' AND h.OutSchoolStatus='-1')) ";
  		}else
  		{
  			$status_cond = "";
  		}
  		
  		$NameField = getNameFieldByLang("a.");
  		$UpdaterNameField = getNameFieldByLang("uu.");
  		$sql = "SELECT 
				  a.UserID,
				  $NameField as StaffName,
				  -1 as SlotID,
				  h.SlotName,
				  h.DutyStart as SlotStart,
				  h.DutyEnd as SlotEnd,
				  IF(h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."',
					'Holiday',
					IF(h.InSchoolStatus = '".CARD_STATUS_OUTGOING."',
						'Outgoing',
						IF(h.Duty = '1',
							'On Duty',
							'No Duty'
						)
					)
				  ) as Duty,
				  h.RecordType as OutgoingType,
				  i.ReasonID as LeaveReasonID,
				  h.InTime,
				  h.OutTime,
				  h.InSchoolStation,
				  h.OutSchoolStation,
				  h.InWaived,
				  h.OutWaived,
				  IF(h.DateConfirmed IS NULL, IF(i.Waived IS NULL AND j.ReasonID IS NOT NULL, j.DefaultWavie, i.Waived) ,i.Waived) as Waived,
				  IF(h.DateConfirmed IS NULL, IF(k.Waived IS NULL AND m.ReasonID IS NOT NULL, m.DefaultWavie, k.Waived) ,k.Waived) as ELWaived,
				  (CASE
				  WHEN h.InSchoolStatus = '".CARD_STATUS_OUTGOING."' 
				  	THEN '".CARD_STATUS_OUTGOING."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' 
					THEN '".CARD_STATUS_HOLIDAY."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_LATE."' AND h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."'
					THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_LATE."' 
					THEN '".CARD_STATUS_LATE."' 
				  WHEN h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' 
					THEN '".CARD_STATUS_EARLYLEAVE."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_ABSENT."' 
					THEN '".CARD_STATUS_ABSENT."'
				  WHEN h.InSchoolStatus = '".CARD_STATUS_NORMAL."' OR h.OutSchoolStatus = '".CARD_STATUS_NORMAL."'
					THEN '".CARD_STATUS_NORMAL."' 
				  ELSE '".$CARD_STATUS_DEFAULT."' 
				  END) as Status,
				  i.ReasonID,
				  j.ReasonText as Reason,
				  k.ReasonID as ELReasonID,
				  m.ReasonText as ELReason,
				  h.Remark,
				  IF((h.InSchoolStatus IS NOT NULL OR h.OutSchoolStatus IS NOT NULL) AND (h.DateModified IS NOT NULL OR h.DateConfirmed IS NOT NULL),
					(CASE
					 WHEN h.DateModified IS NOT NULL AND h.DateConfirmed IS NOT NULL 
					   THEN IF(h.DateModified > h.DateConfirmed,h.DateModified,h.DateConfirmed) 
					 WHEN h.DateModified IS NOT NULL AND h.ModifyBy IS NOT NULL 
					   THEN h.DateModified 
					 WHEN h.DateConfirmed IS NOT NULL AND h.ConfirmBy IS NOT NULL 
					   THEN h.DateConfirmed 
					 ELSE '-' 
					END),
					'-') as LastUpdated,
				  $UpdaterNameField as LastUpdater,
				  h.RecordStatus 
				FROM
				  INTRANET_USER as a
				  INNER JOIN CARD_STAFF_ATTENDANCE_USERGROUP as b on a.UserID=b.UserID
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
					ON wp.UserID=b.UserID 
						AND '".$this->Get_Safe_Sql_Query($DateStr)."' BETWEEN wp.PeriodStart AND wp.PeriodEnd 
				  INNER JOIN $card_log_table_name as h on h.DayNumber = '".$this->Get_Safe_Sql_Query($Day)."' and h.StaffID = a.UserID and h.SlotName = '".$this->Get_Safe_Sql_Query($SlotName)."'
				  LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = a.UserID and i.RecordDate = '".$this->Get_Safe_Sql_Query($DateStr)."' and i.RecordID = h.InAttendanceRecordID 
				  LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID
				  LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as k on k.StaffID = a.UserID and k.RecordDate = '".$this->Get_Safe_Sql_Query($DateStr)."' and k.RecordID = h.OutAttendanceRecordID
				  LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as m on m.ReasonID = k.ReasonID 
				  LEFT JOIN INTRANET_USER as uu 
					ON IF(h.DateModified IS NOT NULL AND h.DateConfirmed IS NOT NULL,
						IF(h.DateModified > h.DateConfirmed,h.ModifyBy = uu.UserID,h.ConfirmBy = uu.UserID),
						IF(h.ModifyBy IS NULL,h.ConfirmBy = uu.UserID, h.ModifyBy = uu.UserID)
						)
				WHERE
				  a.RecordType='1'
				  AND a.RecordStatus = '1'
				  AND b.GroupID = '".$this->Get_Safe_Sql_Query($GroupID)."'
				  AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%'
						OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' )
				  $status_cond
				GROUP BY
				  h.RecordID, h.SlotName
				ORDER BY
				  a.EnglishName";
			//debug_r($sql);
  		
  		return $this->returnArray($sql);
  	}
  	
  	# Get attendance data of all slots of specific staff
  	function Get_Attendance_Record_Individual_List_By_UserID($TargetUserID, $Year, $Month, $Day, $Keyword="", $DutyType="", $StatusType="", $SlotName="")
  	{
  		global $Lang;
  		$CARD_STATUS_LATEEARLYLEAVE = 6;
  		$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
  		
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$DateStr = $Year."-".$Month."-".$Day;
  		$Weekday = date("w", mktime(0, 0, 0, $Month, $Day, $Year));
  		
  		if($DutyType == 1) # On Duty
  		{
  			$duty_cond = " AND h.Duty = '1' ";
  		}else if($DutyType == 2) # Leave
  		{
  			$duty_cond = " AND h.InSchoolStatus = '".CARD_STATUS_OUTGOING."' ";
  		}else if($DutyType == 3) # Non-working
  		{
  			$duty_cond = " AND h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' ";
  		}else # All Duty
  		{
  			$duty_cond = "";
  		}
  		
  		if($StatusType == CARD_STATUS_NORMAL)
  		{
  			$status_cond = " AND ((h.InSchoolStatus = '".CARD_STATUS_NORMAL."' AND (h.OutSchoolStatus IS NULL OR h.OutSchoolStatus != '".CARD_STATUS_EARLYLEAVE."')) OR (h.OutSchoolStatus = '".CARD_STATUS_NORMAL."' AND (h.InSchoolStatus IS NULL OR h.InSchoolStatus = '-1'))) ";
  		}else if($StatusType == CARD_STATUS_ABSENT)
  		{
  			$status_cond = " AND ( h.InSchoolStatus = '".CARD_STATUS_ABSENT."')";
  		}else if($StatusType == CARD_STATUS_LATE)
  		{
  			$status_cond = " AND (h.InSchoolStatus = '".CARD_STATUS_LATE."' AND (h.OutSchoolStatus IS NULL OR h.OutSchoolStatus != '".CARD_STATUS_EARLYLEAVE."'))";
  		}else if($StatusType == CARD_STATUS_EARLYLEAVE)
  		{
  			$status_cond = " AND (h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' AND (h.InSchoolStatus IS NULL OR h.InSchoolStatus != '".CARD_STATUS_LATE."'))";
  		}else if($StatusType == CARD_STATUS_OUTGOING)
  		{
  			$status_cond = " AND (h.InSchoolStatus = '".CARD_STATUS_OUTGOING."')";
  		}else if($StatusType == $CARD_STATUS_LATEEARLYLEAVE)
  		{
  			$status_cond = " AND (h.InSchoolStatus = '".CARD_STATUS_LATE."' AND h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."')";
  		}else if($StatusType == CARD_STATUS_HOLIDAY)
  		{
  			$status_cond = " AND (h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."')";
  		}else if($StatusType == $CARD_STATUS_ABSENT_SUSPECTED)
  		{
  			$status_cond = " AND h.Duty = '1' AND ((h.InSchoolStatus IS NULL AND h.OutSchoolStatus IS NULL) OR (h.InSchoolStatus='-1' AND h.OutSchoolStatus='-1')) ";
  		}else
  		{
  			$status_cond = "";
  		}
  		
  		if(is_array($TargetUserID))
  		{
  			$user_cond = " AND a.UserID IN (".((sizeof($TargetUserID) > 0)? implode(',',IntegerSafe($TargetUserID)):'-1').")";
  		}else
  		{
  			$user_cond = " AND a.UserID = '".IntegerSafe($TargetUserID)."' ";
  		}
  		
  		if($SlotName != "" && $SlotName != NULL && $SlotName != "undefined" && is_array($SlotName))
  		{
  			for($i=0;$i<sizeof($SlotName);$i++)
  			{
  				if(trim($SlotName[$i])!='') $FinalSlotName[$i] = "'".$this->Get_Safe_Sql_Query($SlotName[$i])."'";
  			}
  			if(sizeof($FinalSlotName)>0){
  				$slot_cond = " AND h.SlotName IN (".((sizeof($FinalSlotName) > 0)? implode(',',$FinalSlotName):'-1').")";
  				$on_slot_cond = " AND h.SlotName IN (".((sizeof($FinalSlotName) > 0)? implode(',',$FinalSlotName):'-1').")";
  			}else{
  				$slot_cond = "";
  				$on_slot_cond = " AND h.SlotName IS NOT NULL ";
  			}
  		}else
  		{
  			$slot_cond = "";
  			if($SlotName != "" && $SlotName != NULL && $SlotName != "undefined")
  				$on_slot_cond = " AND h.SlotName = '".$this->Get_Safe_Sql_Query($SlotName)."' ";
  			else
  				$on_slot_cond = " AND h.SlotName IS NOT NULL ";
  		}
  		
  		$NameField = getNameFieldByLang("a.");
  		$UpdaterNameField = getNameFieldByLang("uu.");
  		$sql = "SELECT 
				  a.UserID,
				  $NameField as StaffName,
				  -1 as SlotID,
				  h.SlotName,
				  h.DutyStart as SlotStart,
				  h.DutyEnd as SlotEnd,
				  IF(h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."',
					'Holiday',
					IF(h.InSchoolStatus = '".CARD_STATUS_OUTGOING."',
						'Outgoing',
						IF(h.Duty = '1',
							'On Duty',
							'No Duty'
						)
					)
				  ) as Duty,
				  h.RecordType as OutgoingType,
				  i.ReasonID as LeaveReasonID,
				  h.InTime,
				  h.OutTime,
				  h.InSchoolStation,
				  h.OutSchoolStation,
				  h.InWaived,
				  h.OutWaived,
				  IF(h.DateConfirmed IS NULL, IF(i.Waived IS NULL AND j.ReasonID IS NOT NULL, j.DefaultWavie, i.Waived) ,i.Waived) as Waived,
				  IF(h.DateConfirmed IS NULL, IF(k.Waived IS NULL AND m.ReasonID IS NOT NULL, m.DefaultWavie, k.Waived) ,k.Waived) as ELWaived,
				  (CASE
				  WHEN h.InSchoolStatus = '".CARD_STATUS_OUTGOING."' 
				  	THEN '".CARD_STATUS_OUTGOING."' 
				  WHEN  h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' 
					THEN '".CARD_STATUS_HOLIDAY."' 
				  WHEN (h.InSchoolStatus = '".CARD_STATUS_LATE."' AND h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."') 
					THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_LATE."' 
					THEN '".CARD_STATUS_LATE."' 
				  WHEN h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' 
					THEN '".CARD_STATUS_EARLYLEAVE."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_ABSENT."' 
					THEN '".CARD_STATUS_ABSENT."'
				  WHEN h.InSchoolStatus = '".CARD_STATUS_NORMAL."' OR h.OutSchoolStatus = '".CARD_STATUS_NORMAL."' 
					THEN '".CARD_STATUS_NORMAL."' 
				  ELSE '".$CARD_STATUS_DEFAULT."' 
				  END) as Status,
				  i.ReasonID,
				  j.ReasonText as Reason,
				  k.ReasonID as ELReasonID,
				  m.ReasonText as ELReason,
				  h.Remark,
				  IF((h.InSchoolStatus IS NOT NULL OR h.OutSchoolStatus IS NOT NULL) AND (h.DateModified IS NOT NULL OR h.DateConfirmed IS NOT NULL),
					(CASE
					 WHEN h.DateModified IS NOT NULL AND h.DateConfirmed IS NOT NULL 
					   THEN IF(h.DateModified > h.DateConfirmed,h.DateModified,h.DateConfirmed) 
					 WHEN h.DateModified IS NOT NULL AND h.ModifyBy IS NOT NULL 
					   THEN h.DateModified 
					 WHEN h.DateConfirmed IS NOT NULL AND h.ConfirmBy IS NOT NULL 
					   THEN h.DateConfirmed 
					 ELSE '-' 
					END),
					'-') as LastUpdated,
				  $UpdaterNameField as LastUpdater,
				  h.RecordStatus,
				  h.RecordID 
				FROM
				  INTRANET_USER as a 
				  INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
				  ON wp.UserID=a.UserID 
				  	AND '".$this->Get_Safe_Sql_Query($DateStr)."' BETWEEN wp.PeriodStart AND wp.PeriodEnd 
				  INNER JOIN $card_log_table_name as h on h.DayNumber = '".$this->Get_Safe_Sql_Query($Day)."' and h.StaffID = a.UserID $on_slot_cond 
				  LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = a.UserID and i.RecordDate = '".$this->Get_Safe_Sql_Query($DateStr)."' and i.RecordID = h.InAttendanceRecordID 
				  LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID
				  LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as k on k.StaffID = a.UserID and k.RecordDate = '".$this->Get_Safe_Sql_Query($DateStr)."' and k.RecordID = h.OutAttendanceRecordID
				  LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as m on m.ReasonID = k.ReasonID 
				  LEFT JOIN INTRANET_USER as uu 
					ON IF(h.DateModified IS NOT NULL AND h.DateConfirmed IS NOT NULL,
						IF(h.DateModified > h.DateConfirmed,h.ModifyBy = uu.UserID,h.ConfirmBy = uu.UserID),
						IF(h.ModifyBy IS NULL,h.ConfirmBy = uu.UserID, h.ModifyBy = uu.UserID)
						)
				WHERE
				  a.RecordType='1'
				  AND a.RecordStatus = '1'
				  $user_cond
				  AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
					   OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' )
				  $duty_cond
				  $status_cond
				  $slot_cond
				GROUP BY
				  h.RecordID, h.SlotName 
				ORDER BY
				  a.EnglishName";
		//debug_r($sql);
  		return $this->returnArray($sql);
  	}

  	function Get_InOut_Record_List($Keyword="", $Year="", $Month="", $Day="", $GroupID="", $SortFields=array(), $IncludeAbsentSuspectedRecord=false)
  	{
  		global $Lang;
  		$CARD_STATUS_LATEEARLYLEAVE = 6;
  		$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$DateStr = $Year."-".$Month."-".$Day;
  		
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
		
  		$NameField = getNameFieldByLang("a.");
  		
  		
  		if($GroupID != "" && $GroupID != -1 && $GroupID != "undefined")
  		{
  			$group_cond = " AND c.GroupID = '$GroupID' ";
  		}else
  		{
  			$group_cond = "";
  		}
  		
  		$SortField = trim($SortFields["SortField"])==""?"EnglishName":$SortFields["SortField"];
  		$SortOrder = trim($SortFields["SortOrder"])==""?"ASC":$SortFields["SortOrder"];
  		$SortField2 = $SortField == "GroupName"?"EnglishName":"GroupName";
  		
  		$sql = "SELECT
					a.UserID,
				  	$NameField as StaffName,
					c.GroupID,
					IF(c.GroupName IS NULL,'-',c.GroupName) as GroupName,
				  	e.SlotName,
					MIN(e.InTime) as InTime,
					MAX(e.OutTime) as OutTime,
					e.InSchoolStation,
					e.OutSchoolStation,
					e.InSchoolStatus,
					e.OutSchoolStatus,
					a.EnglishName  
				FROM
					INTRANET_USER as a
					LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as b ON b.UserID = a.UserID
					LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as c ON c.GroupID = b.GroupID
					INNER JOIN $card_log_table_name as e ON e.DayNumber = '$Day' AND e.StaffID = a.UserID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
					ON wp.UserID=a.UserID 
						AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',e.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus IN ($recordstatus) AND e.SlotName IS NOT NULL ";
		if(!$IncludeAbsentSuspectedRecord){
			$sql .= " AND (e.RecordStatus = '1' OR e.InTime IS NOT NULL OR e.OutTime IS NOT NULL) ";
		}
		   $sql .= "AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						OR e.SlotName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
						OR c.GroupName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%')
					$group_cond
				GROUP BY
					a.UserID,e.RecordID ";
		if($ReportDisplayLeftStaff){
			$ArchiveNameField = $this->Get_Archive_Name_Field("a.");
			$sql .= "UNION (
					SELECT
						a.UserID,
					  	$ArchiveNameField as StaffName,
						c.GroupID,
						IF(c.GroupName IS NULL,'-',c.GroupName) as GroupName,
					  	e.SlotName,
						MIN(e.InTime) as InTime,
						MAX(e.OutTime) as OutTime,
						e.InSchoolStation,
						e.OutSchoolStation,
						e.InSchoolStatus,
						e.OutSchoolStatus,
						a.EnglishName 
					FROM
						INTRANET_ARCHIVE_USER as a
						LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as b ON b.UserID = a.UserID
						LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as c ON c.GroupID = b.GroupID
						INNER JOIN $card_log_table_name as e ON e.DayNumber = '$Day' AND e.StaffID = a.UserID 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
						ON wp.UserID=a.UserID 
							AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',e.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					WHERE
						a.RecordType='1' AND e.SlotName IS NOT NULL ";
			if(!$IncludeAbsentSuspectedRecord){
				$sql .= " AND (e.RecordStatus = '1' OR e.InTime IS NOT NULL OR e.OutTime IS NOT NULL) ";
			}
			 $sql .= " AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							OR e.SlotName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
							OR c.GroupName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%')
						$group_cond
					GROUP BY 
						a.UserID,e.RecordID )";
		}
		$sql .="ORDER BY
					$SortField $SortOrder, $SortField2 $SortOrder ";
		$result = $this->returnResultSet($sql);
		$record_count = count($result);
		$records = array(); // final result set
		$markedUsers = array();
		// loop through data set to match time and station 
		for($i=0;$i<$record_count;$i++) {
			if(!isset($markedUsers[$result[$i]['UserID']])){
				$records[] = $result[$i];
				$markedUsers[$result[$i]['UserID']] = count($records)-1;
			}
			for($j=0;$j<count($records);$j++) {
				if($records[$j]['UserID'] == $result[$i]['UserID']){
					if($result[$i]['InTime'] != '' && ($records[$j]['InTime']=='' || ($records[$j]['InTime']!='' && $result[$i]['InTime'] < $records[$j]['InTime']))){
						$records[$j]['InTime'] = $result[$i]['InTime'];
						$records[$j]['InSchoolStation'] = $result[$i]['InSchoolStation'];
						$records[$j]['InSchoolStatus'] = $result[$i]['InSchoolStatus'];
					}
					if($result[$i]['OutTime'] != '' && ($records[$j]['OutTime']=='' || ($records[$j]['OutTime']!='' && $result[$i]['OutTime'] > $records[$j]['OutTime']))){
						$records[$j]['OutTime'] = $result[$i]['OutTime'];
						$records[$j]['OutSchoolStation'] = $result[$i]['OutSchoolStation'];
						$records[$j]['OutSchoolStatus'] = $result[$i]['OutSchoolStatus'];
					}
				}
			}
		}
		
		return $records;
  	}
  	
  	function Get_OT_Record_List($Keyword="", $Date="", $StaffType="")
  	{
  		$NameField = getNameFieldByLang("a.");
  		
  		if($Date == "")
  		{
  			$Date = date('Y-m-d');
  		}
  		/*
  		if($StaffType == 1)// with outstanding mins
  		{
  			$having_cond = "HAVING (TotalOTmins - TotalMinsRedeemed) > 0 ";
  		}else
  		{
  			$having_cond = "";
  		}
  		
  		$sql = "SELECT
					a.UserID,
					$NameField as StaffName,
					b.RecordID as OTRecordID,
					SUM(b.OTmins) as TotalOTmins,
					c.RecordID as RedeemRecordID,
					IF(SUM(c.MinsRedeemed) IS NULL, 0, SUM(c.MinsRedeemed)) as TotalMinsRedeemed
				FROM
					INTRANET_USER as a
					INNER JOIN CARD_STAFF_ATTENDANCE2_OT_RECORD as b ON b.StaffID = a.UserID
					LEFT JOIN CARD_STAFF_ATTENDANCE2_OT_REDEEM as c ON c.StaffID = a.UserID AND c.RedeemDate = b.RecordDate
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus = '1'
					AND b.RecordDate = '$Date'
					AND $NameField LIKE '%$Keyword%' 
				GROUP BY
					b.StaffID
				$having_cond
				ORDER BY
					a.EnglishName, b.DateModified
				";
  		
  		return $this->returnArray($sql);
  		*/
  		
  		$sql = "SELECT
					a.UserID,
					$NameField as StaffName,
					b.RecordID as OTRecordID,
					SUM(b.OTmins) as TotalOTmins
				FROM
					INTRANET_USER as a
					INNER JOIN CARD_STAFF_ATTENDANCE2_OT_RECORD as b ON b.StaffID = a.UserID  
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus = '1'
					AND b.RecordDate = '$Date'
					AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
				GROUP BY
					a.UserID 
				ORDER BY
					a.EnglishName, b.DateModified";
		
		$otArr = $this->returnArray($sql);
		$AssocOTArr = array();
		for($i=0;$i<sizeof($otArr);$i++)
		{
			$AssocOTArr[$otArr[$i]['UserID']] = $otArr[$i];
		}
		
		$sql = "SELECT
					a.UserID,
					c.RecordID as RedeemRecordID,
					SUM(c.MinsRedeemed) as TotalMinsRedeemed
				FROM
					INTRANET_USER as a
					INNER JOIN CARD_STAFF_ATTENDANCE2_OT_REDEEM as c ON c.StaffID = a.UserID  
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus = '1'
					AND c.RedeemDate = '$Date'
				GROUP BY
					a.UserID ";
		$redeemArr = $this->returnArray($sql);
		$AssocRedeemArr = array();
		for($i=0;$i<sizeof($redeemArr);$i++)
		{
			$AssocRedeemArr[$redeemArr[$i]['UserID']] = $redeemArr[$i];
		}
		
		$ReturnArr = array();
		$j = 0;
		for($i=0;$i<sizeof($otArr);$i++)
		{
			$TargetUserID = $otArr[$i]['UserID'];
			$TotalMinsRedeemed = empty($AssocRedeemArr[$TargetUserID]['TotalMinsRedeemed'])?0:$AssocRedeemArr[$TargetUserID]['TotalMinsRedeemed'];
			//When SelectType == 1, only select those with outstanding mins
			if($StaffType != 1 || $AssocOTArr[$TargetUserID]['TotalOTmins'] > $TotalMinsRedeemed)
			{
				$ReturnArr[$j]['UserID'] = $TargetUserID;
				$ReturnArr[$j]['StaffName'] = $AssocOTArr[$TargetUserID]['StaffName'];
				$ReturnArr[$j]['OTRecordID'] = $AssocOTArr[$TargetUserID]['OTRecordID'];
				$ReturnArr[$j]['TotalOTmins'] = $AssocOTArr[$TargetUserID]['TotalOTmins'];
				$ReturnArr[$j]['RedeemRecordID'] = $AssocRedeemArr[$TargetUserID]['RedeemRecordID'];
				$ReturnArr[$j]['TotalMinsRedeemed'] = $TotalMinsRedeemed;
				$j+=1;
			}
		}
		
		return $ReturnArr;
  	}
  	
  	function Get_OT_Records($params)
  	{
  		global $Lang;
  		
		$cond = "";
		$is_sql_table_query = $params['IsSqlQuery'];
		if(isset($params['RecordID'])){
			$record_id = $params['RecordID'];
			if(is_array($record_id)){
				$cond .= " AND r.RecordID IN (".implode(",",$record_id).") ";
			}else{
				$cond .= " AND r.RecordID='$record_id' ";
			}
		}
		if(isset($params['NotRecordID'])){
			$not_record_id = $params['NotRecordID'];
			if(is_array($not_record_id)){
				$cond .= " AND r.RecordID NOT IN (".implode(",",$not_record_id).") ";
			}else{
				$cond .= " AND r.RecordID<>'$not_record_id' ";
			}
		}
		if(isset($params['StaffID'])){
			$staff_id = $params['StaffID'];
			if(is_array($staff_id)){
				$cond .= " AND r.StaffID IN (".implode(",",$staff_id).") ";
			}else{
				$cond .= " AND r.StaffID='$staff_id' ";
			}
		}
		if(isset($params['RecordDate'])){
			$record_date = $params['RecordDate'];
			$cond .= " AND r.RecordDate='$record_date' ";
		}
		if(isset($params['StartDate'])){
			$start_date = $params['StartDate'];
			$cond .= " AND r.RecordDate>='$start_date' ";
		}
		if(isset($params['EndDate'])){
			$end_date = $params['EndDate'];
			$cond .= " AND r.RecordDate<='$end_date' ";
		}
		if(isset($params['TargetYear']) && isset($params['TargetMonth'])){
			$like_pattern = $params['TargetYear']."-".$params['TargetMonth']."-%";
			$cond .= " AND r.RecordDate LIKE '$like_pattern' ";
		}

		if(isset($params['Keyword']) && trim($params['Keyword'])!=''){
			$escape_key = $this->Get_Safe_Sql_Like_Query(trim($params['Keyword']));
			$cond .= " AND (u.EnglishName LIKE '%$escape_key%' OR u.ChineseName LIKE '%$escape_key%' OR r.RecordDate LIKE '%$escape_key%') ";
		}
		$name_field = $this->Get_Name_Field("u.");
		
		$sql = "SELECT 
					$name_field as StaffName,
					r.RecordDate,
					".($is_sql_table_query?"CONCAT(r.OTmins,' ".$Lang['StaffAttendance']['Mins']."')":"r.OTmins")." as OTmins,
					r.DateModified ";
		if($is_sql_table_query){
			$sql.= ",CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',r.RecordID,'\">') as CheckBox ";
		}
		$sql .= ",r.RecordID,r.StaffID 
				FROM CARD_STAFF_ATTENDANCE2_OT_RECORD as r  
				INNER JOIN INTRANET_USER as u ON u.UserID=r.StaffID  
				WHERE u.RecordType='".USERTYPE_STAFF."' $cond ";
		if(!$is_sql_table_query){
			$sql .= " ORDER BY u.EnglishName,r.RecordDate ";
		}
		//debug_pr($sql);
		if($is_sql_table_query){
			return $sql;
		}
		$records = $this->returnResultSet($sql);
		$record_size = count($records);
		if(isset($params['ReturnAssocArray'])){
			$ary = array();
			for($i=0;$i<$record_size;$i++){
				if(!isset($ary[$records[$i]['StaffID']])) $ary[$records[$i]['StaffID']] = array();
				$ary[$records[$i]['StaffID']][$records[$i]['RecordDate']] = $records[$i];
			}
			return $ary;
		}
		return $records;
  	}
  	
  	function Upsert_OT_Record($map)
	{
		$fields = array('StaffID','RecordDate','OTmins');
		$user_id = $_SESSION['UserID'];
		
		if(count($map) == 0) return false;
		$success = true;
		
		if(isset($map['RecordID']) && $map['RecordID']!='' && $map['RecordID']>0)
		{
			$sep = "";
			$record_id = $map['RecordID'];
			$sql = "UPDATE CARD_STAFF_ATTENDANCE2_OT_RECORD SET ";
			foreach($map as $key => $val){
				if(!in_array($key, $fields) || $key=='RecordID') continue;
				$sql .= $sep."$key='".$this->Get_Safe_Sql_Query($val)."'";
				$sep = ",";
			}
			$sql.= ",DateModified=NOW() WHERE RecordID='$record_id'";
			$success = $this->db_db_query($sql);
			//debug_pr($sql);
		}else{
			$keys = '';
			$values = '';
			$sep = "";
			foreach($map as $key => $val){
				if(!in_array($key, $fields)) continue;
				$keys .= $sep."$key";
				$values .= $sep."'".$this->Get_Safe_Sql_Query($val)."'";
				$sep = ",";
			}
			$keys .= ",DateInput,DateModified";
			$values .= ",NOW(),NOW()";
			
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_OT_RECORD ($keys) VALUES ($values)";
			$success = $this->db_db_query($sql);
			
			if($success){
				$record_id = $this->db_insert_id();
				return $record_id;
			}
		}
		return $success;
	}
	
	function Delete_OT_Record($params)
	{
		$cond = "";
		if(isset($params['RecordID'])){
			$RecordID = $params['RecordID'];
			if($cond != "") $cond .= " AND ";
			if(is_array($RecordID))
				$cond .= " RecordID IN ('".implode("','",$RecordID)."') ";
			else
				$cond .= " RecordID='$RecordID' ";
		}
		if(isset($params['StaffID'])){
			$StaffID = $params['StaffID'];
			if($cond != "") $cond .= " AND ";
			if(is_array($StaffID)){
				$cond .= " StaffID IN ('".implode("','",$StaffID)."') ";
			}else{
				$cond .= " StaffID='$StaffID' ";
			}
		}
		if(isset($params['RecordDate'])){
			$RecordDate = $params['RecordDate'];
			if($cond != "") $cond .= " AND ";
			if(is_array($RecordDate)){
				$cond .= " RecordDate IN ('".implode("','",$RecordDate)."') ";
			}else{
				$cond .= " RecordDate='$RecordDate' ";
			}
		}
		$sql = "DELETE FROM CARD_STAFF_ATTENDANCE2_OT_RECORD WHERE $cond";
		$success = $this->db_db_query($sql);
		return $success;
	}
  	
  	function Get_OT_Record_List_By_UserID($TargetUserID, $SelectType="", $StartDate="", $EndDate="", $Keyword="", $TargetDate="")
  	{
  		if($SelectType == "2") // Yesterday
  		{
  			$timestamp = time() - (24*60*60);
  			$yesterday = date('Y-m-d', $timestamp);
  			$cond = " AND b.RecordDate = '$yesterday' ";
  		}else if($SelectType == "3") // this month
  		{
  			$year = date('Y');
  			$month = date('m');
  			$cond = " AND b.RecordDate LIKE '$year-$month-%' ";
  		}else if($SelectType == "4") // from StartDate to EndDate
  		{
  			if($StartDate == "" || $StartDate == "undefined" || $StartDate == null)
  				$StartDate = date('Y-m-d');
  			if($EndDate == "" || $EndDate == "undefined" || $EndDate == null)
  				$EndDate = date('Y-m-d');
  			$cond = " AND b.RecordDate >= '$StartDate' AND b.RecordDate <= '$EndDate' ";
  		}else if($SelectType == "1")// today
  		{
	  		$today = date('Y-m-d');
	  		$cond = " AND b.RecordDate = '$today' ";
  		}else if($TargetDate != "" && $TargetDate != null && $TargetDate != "undefined")
  		{
	  		$cond = " AND b.RecordDate = '$TargetDate' ";	
  		}else
  		{
  			$cond = "";
  		}
  		
  		$NameField = getNameFieldByLang("a.");
  		
  		$sql = "SELECT
					a.UserID,
					$NameField as StaffName,
					b.RecordDate,
					SUM(b.OTmins) as OTmins,
					b.DateModified,
					b.RecordID 
				FROM
					INTRANET_USER as a
					INNER JOIN CARD_STAFF_ATTENDANCE2_OT_RECORD as b ON b.StaffID = a.UserID
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus = '1'
					AND b.StaffID = '$TargetUserID'
					AND b.RecordDate LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
					$cond 
				GROUP BY
					b.RecordDate
				ORDER BY
					b.RecordDate";
  		//debug_r($sql);
  		return $this->returnArray($sql);
  	}
  	
  	function Get_Redeem_Record_List_By_UserID($TargetUserID, $SelectType="", $StartDate="", $EndDate="", $Keyword="", $TargetDate="")
  	{
  		if($SelectType == "2") // Yesterday
  		{
  			$timestamp = time() - (24*60*60);
  			$yesterday = date('Y-m-d', $timestamp);
  			$cond = " AND b.RedeemDate= '$yesterday' ";
  		}else if($SelectType == "3") // this month
  		{
  			$year = date('Y');
  			$month = date('m');
  			$cond = " AND b.RedeemDate LIKE '$year-$month-%' ";
  		}else if($SelectType == "4") // from StartDate to EndDate
  		{
  			if($StartDate == "" || $StartDate == "undefined" || $StartDate == null)
  				$StartDate = date('Y-m-d');
  			if($EndDate == "" || $EndDate == "undefined" || $EndDate == null)
  				$EndDate = date('Y-m-d');
  			$cond = " AND b.RedeemDate >= '$StartDate' AND b.RedeemDate <= '$EndDate' ";
  		}else if($SelectType == "1")// today
  		{
	  		$today = date('Y-m-d');
	  		$cond = " AND b.RedeemDate = '$today' ";
  		}else if($TargetDate != "" && $TargetDate != null && $TargetDate != "undefined")
  		{
  			$cond = " AND b.RedeemDate = '$TargetDate' ";
  		}else
  		{
  			$cond = "";
  		}
  		
  		$NameField = getNameFieldByLang("a.");
  		
  		$sql = "SELECT
					a.UserID,
					$NameField as StaffName,
					b.RedeemDate,
					b.MinsRedeemed as MinsRedeemed,
					IF(b.Remark IS NULL, '--', b.Remark) as Remark,
					IF(b.DateModified IS NULL, '-', b.DateModified) as DateModified
				FROM
					INTRANET_USER as a
					INNER JOIN CARD_STAFF_ATTENDANCE2_OT_REDEEM as b ON b.StaffID = a.UserID
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus = '1'
					AND b.StaffID = '$TargetUserID'
					AND (b.RedeemDate LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
						 OR b.Remark LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' )
					$cond 
				ORDER BY
					b.RedeemDate ";
  		
  		return $this->returnArray($sql);
  	}
  	
  	function Get_Staff_Info($StaffID="",$Keyword="",$Recordstatus="1",$GetArchiveUser=false)
  	{
  		//$NameField = getNameFieldByLang("a.");
  		$NameField = $this->Get_Name_Field("a.");
  		if($StaffID == "" || $StaffID == null || $StaffID == "undefined")
  		{
  			// Get all staffs
  			$cond = "";
  		}else if(is_array($StaffID))
  		{
  			// Get some staffs
  			$cond = " AND a.UserID IN ( ".((sizeof($StaffID)>0)?implode(',',$StaffID):"-1")." )";
  		}else
  		{
  			// Get specific staff
  			$cond = " AND a.UserID = '$StaffID' ";
  		}
  		if($Keyword!=""){
  			$cond.=" AND a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
					 AND a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ";
  		}
  		$sql = "SELECT
					a.UserID,
					$NameField as StaffName, 
					g.GroupName,
					a.EnglishName 
				FROM
					INTRANET_USER as a 
					LEFT JOIN 
					CARD_STAFF_ATTENDANCE_USERGROUP ug 
					on a.UserID = ug.UserID 
					LEFT JOIN 
					CARD_STAFF_ATTENDANCE2_GROUP g 
					on ug.GroupID = g.GroupID 
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus IN ($Recordstatus)
					$cond ";
  		if($GetArchiveUser) {
			$ArchiveNameField = $this->Get_Archive_Name_Field("a.");
			$sql .= "UNION (
					SELECT
						a.UserID,
						$ArchiveNameField as StaffName, 
						g.GroupName,
						a.EnglishName  
					FROM
						INTRANET_ARCHIVE_USER as a 
						LEFT JOIN 
						CARD_STAFF_ATTENDANCE_USERGROUP ug 
						on a.UserID = ug.UserID 
						LEFT JOIN 
						CARD_STAFF_ATTENDANCE2_GROUP g 
						on ug.GroupID = g.GroupID 
					WHERE
						a.RecordType='1'
						$cond 
					)";
		}			
		
		$sql .= "order by EnglishName";
		
		return $this->returnArray($sql);
  	}
  	
  	function Check_OT_Record_For_Redeem($StaffID, $Date, $Mins)
  	{
  		$sql = "SELECT
					SUM(b.OTmins) as OTmins
				FROM
					INTRANET_USER as a
					INNER JOIN CARD_STAFF_ATTENDANCE2_OT_RECORD as b ON b.StaffID = a.UserID  
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus = '1'
					AND b.StaffID = '$StaffID'
					AND b.RecordDate = '$Date'
				GROUP BY
					b.RecordDate";
		
		$otArr = $this->returnArray($sql);
		
		$sql = "SELECT
					SUM(b.MinsRedeemed) as RedeemMins
				FROM
					INTRANET_USER as a
					INNER JOIN CARD_STAFF_ATTENDANCE2_OT_REDEEM as b ON b.StaffID = a.UserID  
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus = '1'
					AND b.StaffID = '$StaffID'
					AND b.RedeemDate = '$Date'
				GROUP BY
					b.RedeemDate";
		$redeemArr = $this->returnArray($sql);
		
		if(sizeof($otArr)>0)
		{
			$OTMins = $otArr[0]['OTmins'];
			$RedeemedMins = $redeemArr[0]['RedeemMins'];
			if($Mins - ($OTMins - $RedeemedMins) > 0)
			{
				return '0';
			}else
			{
				return '1';
			}
		}else
		{
			return '0';
		}
  	}
  	
  	function Save_Redeem_Record($StaffID, $RedeemDate, $MinsRedeemed, $RedeemRemark)
  	{
  		$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_OT_REDEEM 
				(StaffID,RedeemDate,MinsRedeemed,Remark, RecordType, RecordStatus, DateInput, DateModified) 
				VALUES(
					'$StaffID',
					'$RedeemDate',
					'$MinsRedeemed',
					'".$this->Get_Safe_Sql_Query($RedeemRemark)."',
					NULL,
					'1',
					NOW(),
					NOW()
				)";
  		
  		return $this->db_db_query($sql);
  	}
  	
  	function Get_Report_Monthly_Summary_Group_Data($GroupID, $Year, $Month, $Keyword="", $Type="", $WithWaived=1, $HaveRecord=0)
  	{
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
  		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
  		
  		$cond = ($Type != "" && $Type != NULL && $Type != "undefined" && $Type != -1 && $Type != -2)?" AND a.GroupID = '$Type' ":"";
  		if($HaveRecord==1)
  			$joinby = " INNER ";
  		else
  			$joinby = " LEFT ";
  		
  		//$NameField = getNameFieldByLang("b.");
  		$NameField = $this->Get_Name_Field("b.");
  		$ArchiveNameField = $this->Get_Archive_Name_Field("b.");
  		$SafeKeyword = $this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES));
  		
  		$Monitoring_Members = $this->Get_Monitoring_Members();
  		if (count($Monitoring_Members) > 0) {
  			$monitoring_cond = " AND b.UserID IN (".implode(',',$Monitoring_Members).") ";
  		}
  		else {
  			$monitoring_cond = '';
  		}
  		
  		$sql = "SELECT DISTINCT b.UserID 
				FROM 
					CARD_STAFF_ATTENDANCE_USERGROUP AS a 
					INNER JOIN INTRANET_USER as b ON a.UserID = b.UserID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON b.UserID = p.UserID AND ('$Year-$Month' BETWEEN SUBSTRING(p.PeriodStart,1,7) AND SUBSTRING(p.PeriodEnd,1,7)) 
				WHERE
					a.GroupID = '$GroupID' 
					AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus)  
					AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 ) 
					$cond $monitoring_cond ";
		if($ReportDisplayLeftStaff){
			$sql .= "UNION (
					SELECT DISTINCT a.UserID 
					FROM CARD_STAFF_ATTENDANCE_USERGROUP AS a 
					INNER JOIN INTRANET_ARCHIVE_USER as b ON a.UserID = b.UserID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON a.UserID=p.UserID AND ('$Year-$Month' BETWEEN SUBSTRING(p.PeriodStart,1,7) AND SUBSTRING(p.PeriodEnd,1,7)) 
					WHERE
						a.GroupID = '$GroupID' 
						AND b.RecordType = '1' 
							AND (b.EnglishName LIKE '%".$SafeKeyword."%' 
								 OR b.ChineseName LIKE '%".$SafeKeyword."%' 
								 ) 
							$cond $monitoring_cond 
					)";
		}
		
  		$ValidPeiordUsers = $this->returnVector($sql);
  		
  		$sql = "SELECT
					b.UserID,
					$NameField as StaffName,
					c.DayNumber,
					c.InSchoolStatus,
					c.OutSchoolStatus,
					c.InWaived,
					c.OutWaived,
					IF(c.DutyCount IS NULL, 1, c.DutyCount) as DutyCount,
					wp.WorkingPeriodID,
					b.EnglishName,
					c.MinLate,
					c.MinEarlyLeave   
				FROM 
					CARD_STAFF_ATTENDANCE_USERGROUP AS a
			    	INNER JOIN INTRANET_USER AS b on a.UserID = b.UserID 
			    	$joinby JOIN $card_log_table_name AS c on a.UserID = c.StaffID 
					AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1')) 
					LEFT JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
				WHERE
					a.GroupID = '$GroupID' 
					AND b.RecordType = '1' 
					AND b.RecordStatus IN ($recordstatus) 
					AND (b.EnglishName LIKE '%".$SafeKeyword."%' 
						 OR b.ChineseName LIKE '%".$SafeKeyword."%' 
						 ) 
					$cond 
					AND b.UserID IN (".(sizeof($ValidPeiordUsers)>0?implode(",",$ValidPeiordUsers):"-1").") ";
  		if($ReportDisplayLeftStaff) {
			$sql .= "UNION (
					SELECT
						a.UserID,
						$ArchiveNameField as StaffName,
						c.DayNumber,
						c.InSchoolStatus,
						c.OutSchoolStatus,
						c.InWaived,
						c.OutWaived,
						IF(c.DutyCount IS NULL, 1, c.DutyCount) as DutyCount,
						wp.WorkingPeriodID,
						b.EnglishName,
						c.MinLate,
						c.MinEarlyLeave 
					FROM 
						CARD_STAFF_ATTENDANCE_USERGROUP AS a
					    INNER JOIN INTRANET_ARCHIVE_USER AS b on a.UserID = b.UserID 
					    $joinby JOIN $card_log_table_name AS c on a.UserID = c.StaffID 
							AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1'))
						LEFT JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					WHERE 
						a.GroupID = '$GroupID' 
						AND b.RecordType = '1' 
						AND (b.EnglishName LIKE '%".$SafeKeyword."%' 
							 OR b.ChineseName LIKE '%".$SafeKeyword."%' 
							 ) 
						$cond 
						AND b.UserID IN (".(sizeof($ValidPeiordUsers)>0?implode(",",$ValidPeiordUsers):"-1").") 
					)";
		}
		$sql .="ORDER BY 
					EnglishName ";
  		$resultArr = $this->returnArray($sql);
  		$AssocMonthlyData=array();
  		$UserIDList = array();
  		for($i=0;$i<sizeof($resultArr);$i++)
  		{
  			$TargetUserID = $resultArr[$i]['UserID'];
  			$AssocMonthlyData[$TargetUserID]=array();
  			$AssocMonthlyData[$TargetUserID]['UserID'] = $TargetUserID;
  			$AssocMonthlyData[$TargetUserID]['StaffName']= $resultArr[$i]['StaffName'];
  			$AssocMonthlyData[$TargetUserID]['present']=0;
  			$AssocMonthlyData[$TargetUserID]['absent']=0;
  			$AssocMonthlyData[$TargetUserID]['late']=0;
  			$AssocMonthlyData[$TargetUserID]['earlyleave']=0;
  			$AssocMonthlyData[$TargetUserID]['outing']=0;
  			$AssocMonthlyData[$TargetUserID]['holiday']=0;
  			$AssocMonthlyData[$TargetUserID]['present_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['absent_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['late_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['earlyleave_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['outing_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['holiday_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['minlate']=0;
  			$AssocMonthlyData[$TargetUserID]['minearlyleave']=0;
  			
  			$UserIDList[] = $TargetUserID;
  		}
  		for($i=0;$i<sizeof($resultArr);$i++)
  		{
  			if(trim($resultArr[$i]['WorkingPeriodID'])=='') continue; // Not within working period, skip it
  			
  			$TargetUserID = $resultArr[$i]['UserID'];
  			$DutyCount = is_numeric($resultArr[$i]['DutyCount'])?$resultArr[$i]['DutyCount']:0.0;
  			$outwaived = false;
  			if($resultArr[$i]['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE)
  			{
  				if($WithWaived==1){// With Waived
  					$AssocMonthlyData[$TargetUserID]['earlyleave']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['earlyleave_cnt']+=$this->StatusCount;
  					$AssocMonthlyData[$TargetUserID]['minearlyleave']+=$resultArr[$i]['MinEarlyLeave'];
  				}else{ // Without Waived
	  				if($resultArr[$i]['OutWaived']==1)
	  				{
	  					//$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  					$outwaived = true;
	  				}else{
	  					$AssocMonthlyData[$TargetUserID]['earlyleave']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['earlyleave_cnt']+=$this->StatusCount;
	  					$AssocMonthlyData[$TargetUserID]['minearlyleave']+=$resultArr[$i]['MinEarlyLeave'];
	  				}
  				}
  			}
  			
  			if($resultArr[$i]['InSchoolStatus'] != NULL && $resultArr[$i]['InSchoolStatus'] == CARD_STATUS_NORMAL && $resultArr[$i]['OutSchoolStatus'] != CARD_STATUS_EARLYLEAVE)
  			{
  				$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
  				$AssocMonthlyData[$TargetUserID]['present_cnt']+=$this->StatusCount;
  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_ABSENT)
  			{
  				if($WithWaived==1){// With Waived
  					$AssocMonthlyData[$TargetUserID]['absent']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['absent_cnt']+=$this->StatusCount;
  				}else{
	  				if($resultArr[$i]['InWaived']==1)
	  				{
	  					//$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  				}else
	  				{
	  					$AssocMonthlyData[$TargetUserID]['absent']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['absent_cnt']+=$this->StatusCount;
	  				}
  				}
  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_LATE)
  			{
  				if($WithWaived==1){
  					$AssocMonthlyData[$TargetUserID]['late']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['late_cnt']+=$this->StatusCount;
  					$AssocMonthlyData[$TargetUserID]['minlate']+=$resultArr[$i]['MinLate'];
  				}else{
	  				if($resultArr[$i]['InWaived']==1)
	  				{
	  					//if($outwaived==false) $AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  				}else{
	  					$AssocMonthlyData[$TargetUserID]['late']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['late_cnt']+=$this->StatusCount;
	  					$AssocMonthlyData[$TargetUserID]['minlate']+=$resultArr[$i]['MinLate'];
	  				}
  				}
  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_OUTGOING)
  			{
  				if($WithWaived==1){
  					$AssocMonthlyData[$TargetUserID]['outing']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
  				}else{
	  				if($resultArr[$i]['InWaived']==1)
	  				{
	  					//$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  				}else
	  				{
	  					$AssocMonthlyData[$TargetUserID]['outing']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
	  				}
  				}
  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_HOLIDAY)
  			{
  				if($WithWaived==1){
  					$AssocMonthlyData[$TargetUserID]['holiday']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
  				}else{
	  				if($resultArr[$i]['InWaived']==1)
	  				{
	  					//$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  				}else
	  				{
	  					$AssocMonthlyData[$TargetUserID]['holiday']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
	  				}
  				}
  			}else if($resultArr[$i]['OutSchoolStatus'] != NULL && $resultArr[$i]['OutSchoolStatus'] == CARD_STATUS_NORMAL)
  			{
  				$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
  				$AssocMonthlyData[$TargetUserID]['present_cnt']+=$this->StatusCount;
  			}
  		}
  		
  		// Find Preset Leave Records after today
  		$today = date('Y-m-d');
  		$sql = "SELECT
					a.StaffID as UserID,
					a.SlotID,
					a.RecordType,
					IF(b.DutyCount IS NULL, 1.0, b.DutyCount) as DutyCount 
				FROM
					CARD_STAFF_ATTENDANCE2_LEAVE_RECORD as a
					INNER JOIN INTRANET_USER as u ON u.UserID = a.StaffID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND a.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as b ON b.SlotID = a.SlotID 
				WHERE
					a.StaffID in (".((sizeof($UserIDList) > 0)? implode(",",$UserIDList):"-1").")
					AND a.RecordDate LIKE '$Year-$Month-%'
					AND a.RecordDate > '$today' 
					AND u.RecordType = '1' AND u.RecordStatus IN ($recordstatus)
					AND 
					(u.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
					 OR u.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
				";
  		$LeaveArr = $this->returnArray($sql);
  		for($i=0;$i<sizeof($LeaveArr);$i++)
  		{
  			$TargetUserID = $LeaveArr[$i]['UserID'];
  			$DutyCount = is_numeric($LeaveArr[$i]['DutyCount'])?$LeaveArr[$i]['DutyCount']:0.0;
  			if (trim($LeaveArr[$i]['SlotID']) == "")
  			{
				// Full Day Leave
				if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
				{ // holiday
					$AssocMonthlyData[$TargetUserID]['holiday']+=1;
					$AssocMonthlyData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
				}else 
				{ // outgoing
					$AssocMonthlyData[$TargetUserID]['outing']+=1;
					$AssocMonthlyData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
				}
			}else
			{
				if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
				{ // holiday
					$AssocMonthlyData[$TargetUserID]['holiday']+=$DutyCount;
					$AssocMonthlyData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
				}else
				{ // outgoing
					$AssocMonthlyData[$TargetUserID]['outing']+=$DutyCount;
					$AssocMonthlyData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
				}
			}
  		}
  		//debug_r($LeaveArr);
  		// The return array of data using number as key
  		$ReturnArr = array();
  		foreach($AssocMonthlyData as $Key => $Value)
  		{
  			$ReturnArr[]=$Value;
  		}
  		//echo "<PRE>";print_r($ReturnArr);echo "</PRE>";
  		return $ReturnArr;
  	}
  	
  	function Get_Report_Monthly_Summary_Individual_Data($StaffID, $Year, $Month, $Keyword="", $Flag="", $WithWaived=1, $HaveRecord=0)
  	{
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
  		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
  		
  		if($HaveRecord==1)
  			$joinby = " INNER ";
  		else
  			$joinby = " LEFT ";
  		
  		if(is_array($StaffID))
  		{
  			$StaffIDStr = (sizeof($StaffID)>0)?implode(',',$StaffID):'-1';
  			$sql = "SELECT DISTINCT b.UserID 
					FROM INTRANET_USER as b 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON b.UserID = p.UserID AND ('$Year-$Month' BETWEEN SUBSTRING(p.PeriodStart,1,7) AND SUBSTRING(p.PeriodEnd,1,7))
					WHERE
						b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) 
						AND b.UserID IN ( $StaffIDStr )
						AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ) ";
			if($ReportDisplayLeftStaff) {
				$sql .= "UNION (
						SELECT DISTINCT b.UserID 
						FROM INTRANET_ARCHIVE_USER as b 
							INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON b.UserID = p.UserID AND ('$Year-$Month' BETWEEN SUBSTRING(p.PeriodStart,1,7) AND SUBSTRING(p.PeriodEnd,1,7))
						WHERE
							b.RecordType = '1' 
							AND b.UserID IN ( $StaffIDStr )
							AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ) 
						)";
			}
			
			$ValidPeriodUsers = $this->returnVector($sql);
			$StaffIDStr = (sizeof($ValidPeriodUsers)>0)?implode(',',$ValidPeriodUsers):'-1';
  		}else
  		{
  			$StaffIDStr = $StaffID;
  		}
  		
  		//$NameField = getNameFieldByLang("b.");
  		$NameField = $this->Get_Name_Field("b.");
  		$ArchiveNameField = $this->Get_Archive_Name_Field("b.");
  		
  		$sql = "SELECT
					b.UserID,
					$NameField as StaffName,
					c.DayNumber,";
		/* if($Flag=="SmartCard") */ $sql .= "c.MinLate, c.MinEarlyLeave, ";
		$sql .= "	c.InSchoolStatus,
					c.OutSchoolStatus,
					c.InWaived,
					c.OutWaived,
					IF(c.DutyCount IS NULL, 1.0, c.DutyCount) as DutyCount,
					wp.WorkingPeriodID,
					b.EnglishName 
				FROM 
				    INTRANET_USER AS b
				    $joinby JOIN $card_log_table_name AS c on b.UserID = c.StaffID 
						AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1')) 
					LEFT JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
				WHERE 
					b.UserID IN ( $StaffIDStr ) 
					AND b.RecordStatus IN ($recordstatus) AND b.RecordType = '1' 
					AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ) ";
		if($ReportDisplayLeftStaff){
			$sql .= "UNION (
					SELECT
						b.UserID,
						$ArchiveNameField as StaffName,
						c.DayNumber,";
			/* if($Flag=="SmartCard") */ $sql .= "c.MinLate, c.MinEarlyLeave, ";
			$sql .= "	c.InSchoolStatus,
						c.OutSchoolStatus,
						c.InWaived,
						c.OutWaived,
						IF(c.DutyCount IS NULL, 1.0, c.DutyCount) as DutyCount,
						wp.WorkingPeriodID,
						b.EnglishName 
					FROM 
					    INTRANET_ARCHIVE_USER AS b 
					    $joinby JOIN $card_log_table_name AS c on b.UserID = c.StaffID 
							AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1')) 
						LEFT JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					WHERE
						b.UserID IN ( $StaffIDStr ) AND b.RecordType = '1' 
						AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ) 
					)";
		}
		$sql.= "ORDER BY 
					EnglishName ";
  		
  		$resultArr = $this->returnArray($sql);
  		$AssocMonthlyData=array();
  		for($i=0;$i<sizeof($resultArr);$i++)
  		{
  			$TargetUserID = $resultArr[$i]['UserID'];
  			$AssocMonthlyData[$TargetUserID]=array();
  			$AssocMonthlyData[$TargetUserID]['UserID'] = $TargetUserID;
  			$AssocMonthlyData[$TargetUserID]['StaffName']= $resultArr[$i]['StaffName'];
  			$AssocMonthlyData[$TargetUserID]['present']=0;
  			$AssocMonthlyData[$TargetUserID]['absent']=0;
  			$AssocMonthlyData[$TargetUserID]['late']=0;
  			$AssocMonthlyData[$TargetUserID]['earlyleave']=0;
  			$AssocMonthlyData[$TargetUserID]['outing']=0;
  			$AssocMonthlyData[$TargetUserID]['holiday']=0;
  			$AssocMonthlyData[$TargetUserID]['lateearlyleave']=0;
  			$AssocMonthlyData[$TargetUserID]['present_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['absent_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['late_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['earlyleave_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['outing_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['holiday_cnt']=0;
  			$AssocMonthlyData[$TargetUserID]['lateearlyleave_cnt']=0;
  			//if($Flag=="SmartCard")
  			//{
  				$AssocMonthlyData[$TargetUserID]['minlate']=0;
  				$AssocMonthlyData[$TargetUserID]['minearlyleave']=0;
  				$AssocMonthlyData[$TargetUserID]['minlateearlyleave']['late']=0;
  				$AssocMonthlyData[$TargetUserID]['minlateearlyleave']['earlyleave']=0;
  			//}
  		}
  		for($i=0;$i<sizeof($resultArr);$i++)
  		{
  			if(trim($resultArr[$i]['WorkingPeriodID'])=='') continue;
  			$TargetUserID = $resultArr[$i]['UserID'];
  			$DutyCount = is_numeric($resultArr[$i]['DutyCount'])?$resultArr[$i]['DutyCount']:0.0;
  			$outwaived = false;
  			
  			if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_LATE && $resultArr[$i]['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE){
  				if($WithWaived==1){
  					$AssocMonthlyData[$TargetUserID]['lateearlyleave']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['lateearlyleave_cnt']+=$this->StatusCount;
  					//if($Flag!="SmartCard"){
  						$AssocMonthlyData[$TargetUserID]['late']+=$DutyCount;
  						$AssocMonthlyData[$TargetUserID]['late_cnt']+=$this->StatusCount;
  						$AssocMonthlyData[$TargetUserID]['earlyleave']+=$DutyCount;
  						$AssocMonthlyData[$TargetUserID]['earlyleave_cnt']+=$this->StatusCount;
  					//}
  					if(/* $Flag=="SmartCard" && */ $resultArr[$i]['MinLate']>0)
		  			{
		  				$AssocMonthlyData[$TargetUserID]['minlateearlyleave']['late']+=$resultArr[$i]['MinLate'];
		  			}
		  			if(/* $Flag=="SmartCard" && */ $resultArr[$i]['MinEarlyLeave']>0)
		  			{
		  				$AssocMonthlyData[$TargetUserID]['minlateearlyleave']['earlyleave']+=$resultArr[$i]['MinEarlyLeave'];
		  			}
  				}else{
	  				if($resultArr[$i]['InWaived']==1 && $resultArr[$i]['OutWaived']==1){
	  					
	  				}else{
	  					$AssocMonthlyData[$TargetUserID]['lateearlyleave']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['lateearlyleave_cnt']+=$this->StatusCount;
	  					//if($Flag!="SmartCard"){
	  						if($resultArr[$i]['InWaived']!=1){
  								$AssocMonthlyData[$TargetUserID]['late']+=$DutyCount;
  								$AssocMonthlyData[$TargetUserID]['late_cnt']+=$this->StatusCount;
	  						}
	  						if($resultArr[$i]['OutWaived']!=1){
  								$AssocMonthlyData[$TargetUserID]['earlyleave']+=$DutyCount;
  								$AssocMonthlyData[$TargetUserID]['earlyleave_cnt']+=$this->StatusCount;
	  						}
  						//}
	  				}
	  				if($resultArr[$i]['InWaived']==1)
	  				{
	  				}else
	  				{
	  					if(/* $Flag=="SmartCard" && */ $resultArr[$i]['MinLate']>0)
			  			{
			  				$AssocMonthlyData[$TargetUserID]['minlateearlyleave']['late']+=$resultArr[$i]['MinLate'];
			  			}
	  				}
	  				if($resultArr[$i]['OutWaived']==1)
	  				{
	  					$outwaived = true;
	  				}else
	  				{
	  					if(/* $Flag=="SmartCard" && */ $resultArr[$i]['MinEarlyLeave']>0)
			  			{
			  				$AssocMonthlyData[$TargetUserID]['minlateearlyleave']['earlyleave']+=$resultArr[$i]['MinEarlyLeave'];
			  			}
	  				}
  				}
  			}
  			
  			if($resultArr[$i]['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE && $resultArr[$i]['InSchoolStatus'] != CARD_STATUS_LATE)
  			{
  				if($WithWaived==1){
  					$AssocMonthlyData[$TargetUserID]['earlyleave']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['earlyleave_cnt']+=$this->StatusCount;
  					if(/* $Flag=="SmartCard" && */ $resultArr[$i]['MinEarlyLeave']>0)
		  			{
		  				$AssocMonthlyData[$TargetUserID]['minearlyleave']+=$resultArr[$i]['MinEarlyLeave'];
		  			}
  				}else{
	  				if($resultArr[$i]['OutWaived']==1)
	  				{
	  					//$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  					$outwaived = true;
	  				}else
	  				{
	  					$AssocMonthlyData[$TargetUserID]['earlyleave']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['earlyleave_cnt']+=$this->StatusCount;
	  					if(/* $Flag=="SmartCard" && */ $resultArr[$i]['MinEarlyLeave']>0)
			  			{
			  				$AssocMonthlyData[$TargetUserID]['minearlyleave']+=$resultArr[$i]['MinEarlyLeave'];
			  			}
	  				}
  				}
  			}
  			
  			if($resultArr[$i]['InSchoolStatus'] != NULL && $resultArr[$i]['InSchoolStatus'] == CARD_STATUS_NORMAL && $resultArr[$i]['OutSchoolStatus'] != CARD_STATUS_EARLYLEAVE)
  			{
  				$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
  				$AssocMonthlyData[$TargetUserID]['present_cnt']+=$this->StatusCount;
  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_ABSENT)
  			{
  				if($WithWaived==1){
  					$AssocMonthlyData[$TargetUserID]['absent']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['absent_cnt']+=$this->StatusCount;
  				}else{
	  				if($resultArr[$i]['InWaived']==1)
	  				{
	  					//$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  				}else
	  				{
	  					$AssocMonthlyData[$TargetUserID]['absent']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['absent_cnt']+=$this->StatusCount;
	  				}
  				}
  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_LATE && $resultArr[$i]['OutSchoolStatus'] != CARD_STATUS_EARLYLEAVE)
  			{
  				if($WithWaived==1){
  					$AssocMonthlyData[$TargetUserID]['late']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['late_cnt']+=$this->StatusCount;
  					if(/* $Flag=="SmartCard" && */ $resultArr[$i]['MinLate']>0)
		  			{
		  				$AssocMonthlyData[$TargetUserID]['minlate']+=$resultArr[$i]['MinLate'];
		  			}
  				}else{
	  				if($resultArr[$i]['InWaived']==1)
	  				{
	  					//if($outwaived==false) $AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  				}else
	  				{
	  					$AssocMonthlyData[$TargetUserID]['late']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['late_cnt']+=$this->StatusCount;
	  					if(/* $Flag=="SmartCard" && */ $resultArr[$i]['MinLate']>0)
			  			{
			  				$AssocMonthlyData[$TargetUserID]['minlate']+=$resultArr[$i]['MinLate'];
			  			}
	  				}
  				}
  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_OUTGOING)
  			{
  				if($WithWaived==1){
  					$AssocMonthlyData[$TargetUserID]['outing']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
  				}else{
	  				if($resultArr[$i]['InWaived']==1)
	  				{
	  					//$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  				}else
	  				{
	  					$AssocMonthlyData[$TargetUserID]['outing']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
	  				}
  				}
  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_HOLIDAY)
  			{
  				if($WithWaived==1){
  					$AssocMonthlyData[$TargetUserID]['holiday']+=$DutyCount;
  					$AssocMonthlyData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
  				}else{
	  				if($resultArr[$i]['InWaived']==1)
	  				{
	  					//$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
	  				}else
	  				{
	  					$AssocMonthlyData[$TargetUserID]['holiday']+=$DutyCount;
	  					$AssocMonthlyData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
	  				}
  				}
  			}else if($resultArr[$i]['OutSchoolStatus'] != NULL && $resultArr[$i]['OutSchoolStatus'] == CARD_STATUS_NORMAL)
  			{
  				$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
  				$AssocMonthlyData[$TargetUserID]['present_cnt']+=$this->StatusCount;
  			}
  		}
  		
  		// Find Preset Leave Records after today
  		$today = date('Y-m-d');
  		$sql = "SELECT
					a.StaffID as UserID,
					a.SlotID,
					a.RecordType,
					IF(b.DutyCount IS NULL, 1.0, b.DutyCount) as DutyCount 
				FROM
					CARD_STAFF_ATTENDANCE2_LEAVE_RECORD as a
					INNER JOIN INTRANET_USER as u ON u.UserID = a.StaffID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND a.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as b ON b.SlotID = a.SlotID  
				WHERE 
					a.StaffID IN ( $StaffIDStr )
					AND a.RecordDate LIKE '$Year-$Month-%'
					AND a.RecordDate > '$today' 
					AND u.RecordType = '1' AND u.RecordStatus IN ($recordstatus) 
					AND 
					(u.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
					 OR u.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
				";
  		$LeaveArr = $this->returnArray($sql);
  		for($i=0;$i<sizeof($LeaveArr);$i++)
  		{
  			$TargetUserID = $LeaveArr[$i]['UserID'];
  			$DutyCount = is_numeric($LeaveArr[$i]['DutyCount'])?$LeaveArr[$i]['DutyCount']:0.0;
  			if (trim($LeaveArr[$i]['SlotID']) == "")
  			{
				// Full Day Leave
				if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
				{ // holiday
					$AssocMonthlyData[$TargetUserID]['holiday']+=1;
					$AssocMonthlyData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
				}else 
				{ // outgoing
					$AssocMonthlyData[$TargetUserID]['outing']+=1;
					$AssocMonthlyData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
				}
			}else
			{
				if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
				{ // holiday
					$AssocMonthlyData[$TargetUserID]['holiday']+=$DutyCount;
					$AssocMonthlyData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
				}else
				{ // outgoing
					$AssocMonthlyData[$TargetUserID]['outing']+=$DutyCount;
					$AssocMonthlyData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
				}
			}
  		}
  		//debug_r($LeaveArr);
  		$ReturnArr = array();
  		foreach($AssocMonthlyData as $Key => $Value)
  		{
  			$ReturnArr[]=$Value;
  		}
  		//echo "<PRE>";print_r($ReturnArr);echo "</PRE>";
  		return $ReturnArr;
  	}
  	
  	function Get_Report_Monthly_Details_Data($GroupID, $TargetUserID, $Year, $Month, $StatusType, $Keyword="", $WithWaived=1,$ByDateRange=false,$StartDate='',$EndDate='')
  	{
  		if($ByDateRange==true){
  			$start_ts = strtotime($StartDate);
  			$end_ts = strtotime($EndDate);
  		}else{
  			$start_ts = mktime(0,0,0,(int)$Month,1,(int)$Year);// first day of a month
  			$end_ts = mktime(0,0,0,(int)$Month,(int)date('t',$start_ts),(int)$Year);// last day of a month
  		}
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
  		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
  		
  		//$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		//$NameField = getNameFieldByLang("a.");
  		$NameField = $this->Get_Name_Field("a.");
  		$ArchiveNameField = $this->Get_Archive_Name_Field("a.");
  		$FinalResult = array();
  		
  		if($StatusType == CARD_STATUS_EARLYLEAVE)
  		{
  			$join_cond = " (b.OutSchoolStatus IS NOT NULL AND b.OutSchoolStatus != '-1' AND b.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."') ";
  			$profile_join_cond = " and c.RecordID = b.OutAttendanceRecordID ";
  			if($WithWaived!=1){// Without waived
  				$join_cond .= " AND IF(b.OutWaived IS NOT NULL,b.OutWaived <> 1,1) ";
  			}
  		}
  		/*else if($StatusType == CARD_STATUS_NORMAL)
  		{
  			$join_cond = " ((b.InSchoolStatus IS NOT NULL AND b.InSchoolStatus = '$StatusType') OR (b.InSchoolStatus != '".CARD_STATUS_OUTGOING."' 
  					AND b.InSchoolStatus != '".CARD_STATUS_HOLIDAY."' AND b.InSchoolStatus != '".CARD_STATUS_LATE."' AND b.InSchoolStatus != '".CARD_STATUS_ABSENT."' AND b.OutSchoolStatus = '$StatusType' )) ";
  			$profile_join_cond = " and c.RecordID = b.InAttendanceRecordID ";
  		}*/
  		else
  		{
  			if($StatusType == CARD_STATUS_LATE){
  				$join_cond = " ((b.InSchoolStatus IS NOT NULL AND b.InSchoolStatus != '-1' AND b.InSchoolStatus = '$StatusType') ";
  			}else{
  				$join_cond = " ((b.InSchoolStatus IS NOT NULL AND b.InSchoolStatus != '-1' AND b.InSchoolStatus = '$StatusType' AND (b.OutSchoolStatus != '".CARD_STATUS_EARLYLEAVE."' OR b.OutSchoolStatus IS NULL)) ";
  			}
  			if($StatusType == CARD_STATUS_NORMAL)
  			{
  				$join_cond .= " OR (b.OutSchoolStatus = '".CARD_STATUS_NORMAL."' AND (b.InSchoolStatus IS NULL OR b.InSchoolStatus = '-1')) ";
  			}else if($WithWaived!=1){
  				$join_cond .= " AND IF(b.InWaived IS NOT NULL,b.InWaived <> 1,1) ";
  			}
  			$join_cond .= ") ";
  			$profile_join_cond = " and c.RecordID = b.InAttendanceRecordID ";
  		}
  		//INNER JOIN $card_log_table_name AS b on a.UserID = b.StaffID AND ((b.InSchoolStatus IS NOT NULL AND b.InSchoolStatus = '$StatusType') OR (b.OutSchoolStatus IS NOT NULL AND b.OutSchoolStatus = '$StatusType'))
  		if($TargetUserID > 0)// Get specific user data
  		{
  			for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts=mktime(0,0,0,((int)date('m',$cur_ts))+1,1,(int)date('Y',$cur_ts)) )
  			{
	  			//$CurDate = date('Y-m-d',$cur_ts);
	  			$CurYear = date('Y',$cur_ts);
	  			$CurMonth = date('m',$cur_ts);
	  			$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$CurYear."_".$CurMonth;
		  		$sql = "SELECT
							a.UserID,
							$NameField as StaffName,
							CONCAT('$CurYear-$CurMonth-', IF(b.DayNumber<10,CONCAT('0',b.DayNumber),b.DayNumber)) as Date,
							b.InTime,
							b.OutTime,
							b.SlotName,
							b.DutyStart,
							b.DutyEnd,
							$StatusType as Status,
							d.ReasonText,
							b.InWaived as InWaived,
							b.OutWaived as OutWaived,
							IF(b.DateConfirmed IS NOT NULL AND b.DateConfirmed > b.DateModified,b.DateConfirmed,b.DateModified) as DateModified,
							a.EnglishName
						FROM 
						    INTRANET_USER AS a
						    INNER JOIN $card_log_table_name AS b on a.UserID = b.StaffID AND $join_cond ";
				if($ByDateRange==true)
					$sql .= " AND (DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',b.DayNumber),'%Y-%m-%d') BETWEEN '$StartDate' AND '$EndDate' ) ";
				$sql .= "	INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=a.UserID AND DATE_FORMAT(CONCAT('".$CurYear."-".$CurMonth."-',b.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd
							LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as c on c.StaffID = a.UserID and c.RecordDate = CONCAT('$CurYear-$CurMonth-', IF(b.DayNumber<10,CONCAT('0',b.DayNumber),b.DayNumber)) $profile_join_cond 
					  		LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as d on d.ReasonID = c.ReasonID
						WHERE
							a.UserID = '$TargetUserID' AND a.RecordType = '1' AND a.RecordStatus IN ($recordstatus) 
							AND (b.RecordStatus IS NOT NULL OR (b.InSchoolStatus IS NOT NULL AND b.InSchoolStatus != '-1') OR (b.OutSchoolStatus IS NOT NULL AND b.OutSchoolStatus != '-1'))
							AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
						GROUP BY
							b.RecordID ";
				//	"ORDER BY 
				//			a.EnglishName, b.DayNumber, b.DutyStart ";
				if($ReportDisplayLeftStaff){
					$sql .= "UNION 
							SELECT
							a.UserID,
							$ArchiveNameField as StaffName,
							CONCAT('$CurYear-$CurMonth-', IF(b.DayNumber<10,CONCAT('0',b.DayNumber),b.DayNumber)) as Date,
							b.InTime,
							b.OutTime,
							b.SlotName,
							b.DutyStart,
							b.DutyEnd,
							$StatusType as Status,
							d.ReasonText,
							b.InWaived as InWaived,
							b.OutWaived as OutWaived,
							IF(b.DateConfirmed IS NOT NULL AND b.DateConfirmed > b.DateModified,b.DateConfirmed,b.DateModified) as DateModified,
							a.EnglishName 
						FROM 
						    INTRANET_ARCHIVE_USER AS a
						    INNER JOIN $card_log_table_name AS b on a.UserID = b.StaffID AND $join_cond ";
				if($ByDateRange==true)
					$sql .= " AND (DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',b.DayNumber),'%Y-%m-%d') BETWEEN '$StartDate' AND '$EndDate' ) ";
				$sql .= "	INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=a.UserID AND DATE_FORMAT(CONCAT('".$CurYear."-".$CurMonth."-',b.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd
							LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as c on c.StaffID = a.UserID and c.RecordDate = CONCAT('$CurYear-$CurMonth-', IF(b.DayNumber<10,CONCAT('0',b.DayNumber),b.DayNumber)) $profile_join_cond 
					  		LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as d on d.ReasonID = c.ReasonID
						WHERE
							a.UserID = '$TargetUserID' AND a.RecordType = '1'  
							AND (b.RecordStatus IS NOT NULL OR (b.InSchoolStatus IS NOT NULL AND b.InSchoolStatus != '-1') OR (b.OutSchoolStatus IS NOT NULL AND b.OutSchoolStatus != '-1'))
							AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
						GROUP BY
							b.RecordID ";
				}
				
				if($StatusType==CARD_STATUS_HOLIDAY || $StatusType==CARD_STATUS_OUTGOING)
				{
					$today = date('Y-m-d');
					$sql .= "UNION 
							 (SELECT
								lr.StaffID as UserID,
								$NameField as StaffName,
								lr.RecordDate as Date,
								NULL as InTime,
								NULL as OutTime,
								b.SlotName,
								b.SlotStart as DutyStart,
								b.SlotEnd as DutyEnd,
								lr.RecordType as Status,
								c.ReasonText,
								NULL as InWaived,
								NULL as OutWaived,
								lr.DateModified,
								a.EnglishName 
							FROM
								CARD_STAFF_ATTENDANCE2_LEAVE_RECORD AS lr 
								INNER JOIN INTRANET_USER as a ON a.UserID = lr.StaffID 
								INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=a.UserID AND lr.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd
								LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as b ON b.SlotID = lr.SlotID  
								LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as c ON c.ReasonID = lr.ReasonID
							WHERE
								lr.StaffID = '$TargetUserID' 
								AND lr.RecordType = '$StatusType' ";
					if($ByDateRange==true){
						$sql .= "   AND (lr.RecordDate BETWEEN '$StartDate' AND '$EndDate') ";
					}else{
						$sql .= "	AND lr.RecordDate LIKE '$CurYear-$CurMonth-%' ";
					}
					$sql .= "	AND lr.RecordDate > '$today' 
								AND a.RecordType = '1' AND a.RecordStatus IN ($recordstatus) 
								AND
								(a.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								 OR a.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
							GROUP BY
								lr.RecordID)";
				}
				
				$sql .= "ORDER BY 
							EnglishName, Date, DutyStart ";
				$resultArr = $this->returnArray($sql);
				//$FinalResult = array_merge($FinalResult, $resultArr);
				for($i=0;$i<sizeof($resultArr);$i++){
					$FinalResult[] = $resultArr[$i];
				}
  			}
  		}else // Get the whole group users data
  		{
  			for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts=mktime(0,0,0,((int)date('m',$cur_ts))+1,1,(int)date('Y',$cur_ts)) )
  			{
  				$CurYear = date('Y',$cur_ts);
	  			$CurMonth = date('m',$cur_ts);
	  			$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$CurYear."_".$CurMonth;
	  			$sql = "SELECT
							a.UserID,
							$NameField as StaffName,
							CONCAT('$CurYear-$CurMonth-', IF(b.DayNumber<10,CONCAT('0',b.DayNumber),b.DayNumber)) as Date,
							b.InTime,
							b.OutTime,
							b.SlotName,
							b.DutyStart,
							b.DutyEnd,
							$StatusType as Status,
							d.ReasonText,
							b.InWaived as InWaived,
							b.OutWaived as OutWaived,
							IF(b.DateConfirmed IS NOT NULL AND b.DateConfirmed > b.DateModified,b.DateConfirmed,b.DateModified) as DateModified,
							a.EnglishName 
						FROM 
							CARD_STAFF_ATTENDANCE_USERGROUP AS u
						    INNER JOIN INTRANET_USER AS a on u.UserID = a.UserID
						    INNER JOIN $card_log_table_name AS b on a.UserID = b.StaffID AND $join_cond ";
				if($ByDateRange==true)
					$sql .= " AND (DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',b.DayNumber),'%Y-%m-%d') BETWEEN '$StartDate' AND '$EndDate' ) ";
				$sql .= "	INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND DATE_FORMAT(CONCAT('".$CurYear."-".$CurMonth."-',b.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
							LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as c on c.StaffID = a.UserID and c.RecordDate = CONCAT('$CurYear-$CurMonth-', IF(b.DayNumber<10,CONCAT('0',b.DayNumber),b.DayNumber)) $profile_join_cond 
					  		LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as d on d.ReasonID = c.ReasonID
						WHERE
							u.GroupID = '$GroupID' AND a.RecordType = '1' AND a.RecordStatus IN ($recordstatus) 
							AND (b.RecordStatus IS NOT NULL OR (b.InSchoolStatus IS NOT NULL AND b.InSchoolStatus != '-1') OR (b.OutSchoolStatus IS NOT NULL AND b.OutSchoolStatus != '-1'))
							AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%'
								 OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
						GROUP BY
							b.RecordID ";
				//	   "ORDER BY 
				//			 b.DayNumber, a.EnglishName, b.DutyStart ";
				if($ReportDisplayLeftStaff) {
					$sql.= "UNION 
							SELECT
							a.UserID,
							$ArchiveNameField as StaffName,
							CONCAT('$CurYear-$CurMonth-', IF(b.DayNumber<10,CONCAT('0',b.DayNumber),b.DayNumber)) as Date,
							b.InTime,
							b.OutTime,
							b.SlotName,
							b.DutyStart,
							b.DutyEnd,
							$StatusType as Status,
							d.ReasonText,
							b.InWaived as InWaived,
							b.OutWaived as OutWaived,
							IF(b.DateConfirmed IS NOT NULL AND b.DateConfirmed > b.DateModified,b.DateConfirmed,b.DateModified) as DateModified,
							a.EnglishName 
						FROM 
							CARD_STAFF_ATTENDANCE_USERGROUP AS u
						    INNER JOIN INTRANET_ARCHIVE_USER AS a on u.UserID = a.UserID
						    INNER JOIN $card_log_table_name AS b on a.UserID = b.StaffID AND $join_cond ";
				if($ByDateRange==true)
					$sql .= " AND (DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',b.DayNumber),'%Y-%m-%d') BETWEEN '$StartDate' AND '$EndDate' ) ";
				$sql .= "	INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND DATE_FORMAT(CONCAT('".$CurYear."-".$CurMonth."-',b.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
							LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as c on c.StaffID = a.UserID and c.RecordDate = CONCAT('$CurYear-$CurMonth-', IF(b.DayNumber<10,CONCAT('0',b.DayNumber),b.DayNumber)) $profile_join_cond 
					  		LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as d on d.ReasonID = c.ReasonID
						WHERE
							u.GroupID = '$GroupID' 
							AND a.RecordType = '1' 
							AND (b.RecordStatus IS NOT NULL OR (b.InSchoolStatus IS NOT NULL AND b.InSchoolStatus != '-1') OR (b.OutSchoolStatus IS NOT NULL AND b.OutSchoolStatus != '-1'))
							AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%'
								 OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
						GROUP BY
							b.RecordID ";
				}
				if($StatusType==CARD_STATUS_HOLIDAY || $StatusType==CARD_STATUS_OUTGOING)
				{
					$today = date('Y-m-d');
					$sql .= "UNION 
							 (SELECT
								lr.StaffID as UserID,
								$NameField as StaffName,
								lr.RecordDate as Date,
								NULL as InTime,
								NULL as OutTime,
								b.SlotName,
								b.SlotStart as DutyStart,
								b.SlotEnd as DutyEnd,
								lr.RecordType as Status,
								c.ReasonText,
								NULL as InWaived,
								NULL as OutWaived,
								lr.DateModified,
								a.EnglishName 
							FROM
								CARD_STAFF_ATTENDANCE2_LEAVE_RECORD AS lr
								INNER JOIN CARD_STAFF_ATTENDANCE_USERGROUP AS g ON g.GroupID = '$GroupID' 
								INNER JOIN INTRANET_USER as a ON a.UserID = lr.StaffID and g.UserID = a.UserID 
								INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=a.UserID AND lr.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
								LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as b ON b.SlotID = lr.SlotID  
								LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as c ON c.ReasonID = lr.ReasonID
							WHERE
								g.GroupID = '$GroupID'
								AND lr.RecordType = '$StatusType' ";
					if($ByDateRange==true){
						$sql .= "AND (lr.RecordDate BETWEEN '$StartDate' AND '$EndDate') ";
					}else{
						$sql .= "AND lr.RecordDate LIKE '$CurYear-$CurMonth-%' ";
					}
					$sql .= "	AND lr.RecordDate > '$today' 
								AND a.RecordType = '1' AND a.RecordStatus IN ($recordstatus)
								AND
								(a.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								 OR a.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
							GROUP BY
								lr.RecordID)";
				}
				
				$sql .= "ORDER BY 
							EnglishName, Date, DutyStart ";
				$resultArr = $this->returnArray($sql);
				//$FinalResult = array_merge($FinalResult, $resultArr);
				for($i=0;$i<sizeof($resultArr);$i++){
					$FinalResult[] = $resultArr[$i];
				}
  			}
  		}
  		//print("<PRE>".$sql."</PRE>");
  		//return $this->returnArray($sql);
  		return $FinalResult;
  	}
  	
  	function Get_Report_Daily_Details_Group_Data($GroupID, $Year, $Month, $Keyword="", $Type="", $WithWaived=1, $HaveRecord=0)
  	{
  		$CARD_STATUS_LATEEARLYLEAVE = 6;
  		$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
  		
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
  		
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$total_day = date("t",mktime(0,0,0,intval($Month),1,intval($Year)));
  		$cond = ($Type != "" && $Type != NULL && $Type != "undefined" && $Type != -1 && $Type != -2)?" AND a.GroupID = '$Type' ":"";
  		//$NameField = getNameFieldByLang("b.");
  		$NameField = $this->Get_Name_Field("b.");
  		$ArchiveNameField = $this->Get_Archive_Name_Field("au.");

  		$Monitoring_Members = $this->Get_Monitoring_Members();
  		if (count($Monitoring_Members) > 0) {
  			$monitoring_cond = " AND b.UserID IN (".implode(',',$Monitoring_Members).") ";
  			$monitoring_cond_ar = " AND au.UserID IN (".implode(',',$Monitoring_Members).") ";
  		}
  		else {
  			$monitoring_cond = '';
  			$monitoring_cond_ar = '';
  		}
  		
  		$sql = "SELECT
					DISTINCT a.UserID,
					".($ReportDisplayLeftStaff?"IF(b.UserID IS NULL,$ArchiveNameField,$NameField)":$NameField)." as StaffName,
					b.EnglishName 
				FROM 
					CARD_STAFF_ATTENDANCE_USERGROUP AS a 
				    ".($ReportDisplayLeftStaff?"LEFT":"INNER")." JOIN INTRANET_USER AS b on a.UserID = b.UserID and b.RecordType=1 and b.RecordStatus IN ($recordstatus) ";
		if($ReportDisplayLeftStaff){
			$sql .= "LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=a.UserID AND au.RecordType=1 ";
		}
		$sql .= "INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON a.UserID = p.UserID AND ('$Year-$Month' BETWEEN SUBSTRING(p.PeriodStart,1,7) AND SUBSTRING(p.PeriodEnd,1,7)) ";
		if($ReportDisplayLeftStaff){
			$sql .="WHERE
						a.GroupID = '$GroupID' AND ((b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) $monitoring_cond) OR (au.RecordType='1' $monitoring_cond_ar)) 
						AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%'
							 OR au.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							 OR au.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
						$cond 
					ORDER BY
						EnglishName ";
		}else{
			$sql .="WHERE
						a.GroupID = '$GroupID' AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) $monitoring_cond 
						AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
						$cond
					ORDER BY
						EnglishName ";
		}
  		$MemberList = $this->returnArray($sql);
  		
  		$UserIDList = array();
  		$AssocMonthlyData=array();
  		for($i=0;$i<sizeof($MemberList);$i++)
  		{
  			$TargetUserID = $MemberList[$i]['UserID'];
  			$AssocMonthlyData[$TargetUserID]=array();
  			$AssocMonthlyData[$TargetUserID]['UserID'] = $TargetUserID;
  			$AssocMonthlyData[$TargetUserID]['StaffName']= $MemberList[$i]['StaffName'];
  			$AssocMonthlyData[$TargetUserID]['RecordCount']=0;
  			for($j=1;$j<=$total_day;$j++)
			{
				$AssocMonthlyData[$TargetUserID][$j]=array();
			}
			$UserIDList[] = $TargetUserID;
  		}
  		
  		$sql = "SELECT
					a.UserID,
					".($ReportDisplayLeftStaff?"IF(b.UserID IS NULL,$ArchiveNameField,$NameField)":$NameField)." as StaffName,
					c.DayNumber,
					(CASE 
						WHEN c.InSchoolStatus = '".CARD_STATUS_OUTGOING."' THEN '".CARD_STATUS_OUTGOING."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' THEN '".CARD_STATUS_HOLIDAY."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' AND c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' 
						WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_ABSENT."' THEN '".CARD_STATUS_ABSENT."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
						WHEN c.OutSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
						ELSE '".$CARD_STATUS_DEFAULT."' 
					END ) as RecordType,
					IF(c.SlotName IS NULL,'-',c.SlotName) as SlotName,
					e.ReasonText,
					e.ReportSymbol,
					g.ReasonText as ELReasonText,
					g.ReportSymbol as ELReportSymbol,
					c.InWaived,
					c.OutWaived,
					b.EnglishName  
				FROM 
					CARD_STAFF_ATTENDANCE_USERGROUP AS a 
				    ".($ReportDisplayLeftStaff?"LEFT":"INNER")." JOIN INTRANET_USER AS b on a.UserID = b.UserID and b.RecordType='1' and b.RecordStatus IN ($recordstatus)";
		if($ReportDisplayLeftStaff){
			$sql .= "LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=a.UserID AND au.RecordType=1 ";
		}	
			$sql .= "INNER JOIN $card_log_table_name AS c on a.UserID = c.StaffID 
						AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1')) 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=a.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS d on d.StaffID = c.StaffID and d.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and d.RecordID = c.InAttendanceRecordID 
				  	LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS e on e.ReasonID = d.ReasonID 
					LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS f on f.StaffID = c.StaffID and f.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and f.RecordID = c.OutAttendanceRecordID 
				  	LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS g on g.ReasonID = f.ReasonID ";
		if($ReportDisplayLeftStaff){
			$sql.= "WHERE
					a.GroupID = '$GroupID' AND ((b.RecordType = '1' AND b.RecordStatus IN ($recordstatus)) OR au.RecordType='1')
					AND a.UserID IN (".((sizeof($UserIDList) > 0)? implode(",",$UserIDList):"-1").") 
					AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%'
						 OR au.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR au.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
					$cond ";
		}else{  
		  $sql.= "WHERE
					a.GroupID = '$GroupID' AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) 
					AND b.UserID IN (".((sizeof($UserIDList) > 0)? implode(",",$UserIDList):"-1").") 
					AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
					$cond ";
		}
		$sql .= "GROUP BY
					c.RecordID
				ORDER BY 
					EnglishName, c.DutyStart ";
  		//echo $sql;
  		$resultArr = $this->returnArray($sql);
  		
  		for($i=0;$i<sizeof($resultArr);$i++)
  		{
  			$TargetUserID = $resultArr[$i]['UserID'];
  			$DayNum = $resultArr[$i]['DayNumber'];
  			$SlotName = $resultArr[$i]['SlotName'];
			//$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
			if($resultArr[$i]['RecordType'] == $CARD_STATUS_LATEEARLYLEAVE)
			{
				if($WithWaived==1){
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'].((trim($resultArr[$i]['ReasonText'])!="" && trim($resultArr[$i]['ELReasonText'])!=="")?'; ':'').$resultArr[$i]['ELReasonText'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'].((trim($resultArr[$i]['ReportSymbol'])!="" && trim($resultArr[$i]['ELReportSymbol'])!="")?'; ':'').$resultArr[$i]['ELReportSymbol'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['InWaived']=$resultArr[$i]['InWaived'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['OutWaived']=$resultArr[$i]['OutWaived'];
					$AssocMonthlyData[$TargetUserID]['RecordCount']++;
				}else{
					if($resultArr[$i]['InWaived']==1 && $resultArr[$i]['OutWaived']==1){
						
					}else{
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'].((trim($resultArr[$i]['ReasonText'])!="" && trim($resultArr[$i]['ELReasonText'])!=="")?'; ':'').$resultArr[$i]['ELReasonText'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'].((trim($resultArr[$i]['ReportSymbol'])!="" && trim($resultArr[$i]['ELReportSymbol'])!="")?'; ':'').$resultArr[$i]['ELReportSymbol'];
						$AssocMonthlyData[$TargetUserID]['RecordCount']++;
					}
				}
			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_EARLYLEAVE)
			{
				if($WithWaived==1){
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ELReasonText'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ELReportSymbol'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['InWaived']=$resultArr[$i]['InWaived'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['OutWaived']=$resultArr[$i]['OutWaived'];
					$AssocMonthlyData[$TargetUserID]['RecordCount']++;
				}else{
					if($resultArr[$i]['OutWaived']==1){
						
					}else{
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ELReasonText'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ELReportSymbol'];
						$AssocMonthlyData[$TargetUserID]['RecordCount']++;
					}
				}
			}
			else
			{
				if($WithWaived==1){
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['InWaived']=$resultArr[$i]['InWaived'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['OutWaived']=$resultArr[$i]['OutWaived'];
					$AssocMonthlyData[$TargetUserID]['RecordCount']++;
				}else{
					if($resultArr[$i]['InWaived']==1){
						
					}else{
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'];
						$AssocMonthlyData[$TargetUserID]['RecordCount']++;
					}
				}
			}
  		}
  		
  		// Find Preset Leave Records after today
  		$today = date('Y-m-d');
  		$sql = "SELECT
					a.StaffID as UserID,
					a.SlotID,
					IF(RIGHT(a.RecordDate, 2) < '10', RIGHT(a.RecordDate, 1), RIGHT(a.RecordDate, 2)) as DayNumber,
					a.RecordType,
					IF(b.SlotName IS NULL, '-', b.SlotName) as SlotName,
					c.ReasonText,
					c.ReportSymbol
				FROM
					CARD_STAFF_ATTENDANCE2_LEAVE_RECORD AS a
					INNER JOIN INTRANET_USER as u ON u.UserID = a.StaffID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND a.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as b ON b.SlotID = a.SlotID  
					LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as c ON c.ReasonID = a.ReasonID
				WHERE
					a.StaffID in (".((sizeof($UserIDList) > 0)? implode(",",$UserIDList):"-1").")
					AND a.RecordDate LIKE '$Year-$Month-%'
					AND a.RecordDate > '$today' 
					AND u.RecordType = '1' AND u.RecordStatus IN ($recordstatus)
					AND
					(u.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
					 OR u.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
				ORDER BY
					a.RecordDate, u.EnglishName ";
  		$LeaveArr = $this->returnArray($sql);
  		for($i=0;$i<sizeof($LeaveArr);$i++)
  		{
  			$TargetUserID = $LeaveArr[$i]['UserID'];
  			$DayNum = $LeaveArr[$i]['DayNumber'];
  			$SlotName = $LeaveArr[$i]['SlotName'];
			$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$LeaveArr[$i]['RecordType'];
			$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$LeaveArr[$i]['ReasonText'];
			$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$LeaveArr[$i]['ReportSymbol'];
			$AssocMonthlyData[$TargetUserID]['RecordCount']++;
  		}
  		//debug_r($LeaveArr);
  		//echo "<PRE>";print_r($AssocMonthlyData);echo "</PRE>";
  		$ReturnArr = array();
  		if(sizeof($AssocMonthlyData)>0)
  		{
	  		foreach($AssocMonthlyData as $Key => $Value)
	  		{
	  			if($HaveRecord!=1 || ($HaveRecord==1 && $Value['RecordCount']>0))
	  				$ReturnArr[]=$Value;
	  		}
  		}
  		//echo "<PRE>";print_r($resultArr);echo "</PRE>";
  		return $ReturnArr;
  	}
  	
  	function Get_Report_Daily_Details_Individual_Data($StaffList, $Year, $Month, $Keyword="", $WithWaived=1, $HaveRecord=0)
  	{
  		$CARD_STATUS_LATEEARLYLEAVE = 6;
  		$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
  		$Year = sprintf("%4d",$Year);
  		$Month = sprintf("%02d",$Month);
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$total_day = date("t",mktime(0,0,0,intval($Month),1,intval($Year)));
  		
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
  		
  		//$NameField = getNameFieldByLang("b.");
  		$NameField = $this->Get_Name_Field("b.");
  		
  		$Staffs = array();
  		for($i=0;$i<sizeof($StaffList);$i++)
  		{
  			$Staffs[$i]=$StaffList[$i]['UserID'];
  		}
  		$sql = "SELECT 
					DISTINCT b.UserID,
					$NameField as StaffName  
				FROM INTRANET_USER AS b 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON b.UserID = p.UserID AND ('$Year-$Month' BETWEEN SUBSTRING(p.PeriodStart,1,7) AND SUBSTRING(p.PeriodEnd,1,7)) 
				WHERE 
					b.UserID IN ( ".((sizeof($Staffs)>0)?implode(',',IntegerSafe($Staffs)):'-1')." ) 
					AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus)
					AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%') 
				ORDER BY b.EnglishName ";
  		$StaffList = $this->returnArray($sql);
  		$Staffs = array();
  		for($i=0;$i<sizeof($StaffList);$i++)
  		{
  			$Staffs[$i]=$StaffList[$i]['UserID'];
  		}
  		
  		$sql = "SELECT
					b.UserID,
					$NameField as StaffName,
					c.DayNumber,
					(CASE 
						WHEN c.InSchoolStatus = '".CARD_STATUS_OUTGOING."' THEN '".CARD_STATUS_OUTGOING."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' THEN '".CARD_STATUS_HOLIDAY."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' AND c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' 
						WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_ABSENT."' THEN '".CARD_STATUS_ABSENT."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
						WHEN c.OutSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
						ELSE '".$CARD_STATUS_DEFAULT."' 
					END ) as RecordType,
					IF(c.SlotName IS NULL,'-',c.SlotName) as SlotName,
					c.DutyStart,
					c.DutyEnd,
					e.ReasonText,
					e.ReportSymbol,
					g.ReasonText as ELReasonText,
					g.ReportSymbol as ELReportSymbol,
					c.InWaived,
					c.OutWaived 
				FROM 
				    INTRANET_USER AS b
				    INNER JOIN $card_log_table_name AS c on b.UserID = c.StaffID 
						AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1')) 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS d on d.StaffID = c.StaffID and d.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and d.RecordID = c.InAttendanceRecordID  
				  	LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS e on e.ReasonID = d.ReasonID 
					LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS f on f.StaffID = c.StaffID and f.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and f.RecordID = c.OutAttendanceRecordID 
				  	LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS g on g.ReasonID = f.ReasonID
				WHERE
					b.UserID IN ( ".((sizeof($Staffs)>0)?implode(',',IntegerSafe($Staffs)):'-1')." ) 
					AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) 
					AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
				GROUP BY
					c.RecordID
				ORDER BY 
					b.EnglishName, c.DutyStart ";
  		//echo $sql;
  		$resultArr = $this->returnArray($sql);
  		
  		$AssocMonthlyData=array();
  		for($i=0;$i<sizeof($StaffList);$i++)
  		{
  			$TargetUserID = $StaffList[$i]['UserID'];
  			$AssocMonthlyData[$TargetUserID]=array();
  			$AssocMonthlyData[$TargetUserID]['UserID'] = $TargetUserID;
  			$AssocMonthlyData[$TargetUserID]['StaffName']= $StaffList[$i]['StaffName'];
  			$AssocMonthlyData[$TargetUserID]['RecordCount']=0;
  			for($j=1;$j<=$total_day;$j++)
			{
				$AssocMonthlyData[$TargetUserID][$j]=array();
			}
  		}
  		
  		for($i=0;$i<sizeof($resultArr);$i++)
  		{
  			$TargetUserID = $resultArr[$i]['UserID'];
  			$DayNum = $resultArr[$i]['DayNumber'];
  			$SlotName = $resultArr[$i]['SlotName'];
			//$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
			//$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotStart']=$resultArr[$i]['DutyStart'];
			//$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotEnd']=$resultArr[$i]['DutyEnd'];
			if($resultArr[$i]['RecordType'] == $CARD_STATUS_LATEEARLYLEAVE)
			{
				if($WithWaived==1){
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotStart']=$resultArr[$i]['DutyStart'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotEnd']=$resultArr[$i]['DutyEnd'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'].((trim($resultArr[$i]['ReasonText'])!="" && trim($resultArr[$i]['ELReasonText'])!=="")?'; ':'').$resultArr[$i]['ELReasonText'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'].((trim($resultArr[$i]['ReportSymbol'])!="" && trim($resultArr[$i]['ELReportSymbol'])!="")?'; ':'').$resultArr[$i]['ELReportSymbol'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['InWaived']=$resultArr[$i]['InWaived'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['OutWaived']=$resultArr[$i]['OutWaived'];
					$AssocMonthlyData[$TargetUserID]['RecordCount']++;
				}else{
					if($resultArr[$i]['InWaived']==1 && $resultArr[$i]['OutWaived']==1){
						
					}else{
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotStart']=$resultArr[$i]['DutyStart'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotEnd']=$resultArr[$i]['DutyEnd'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'].((trim($resultArr[$i]['ReasonText'])!="" && trim($resultArr[$i]['ELReasonText'])!=="")?'; ':'').$resultArr[$i]['ELReasonText'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'].((trim($resultArr[$i]['ReportSymbol'])!="" && trim($resultArr[$i]['ELReportSymbol'])!="")?'; ':'').$resultArr[$i]['ELReportSymbol'];
						$AssocMonthlyData[$TargetUserID]['RecordCount']++;
					}
				}
			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_EARLYLEAVE)
			{
				if($WithWaived==1){
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotStart']=$resultArr[$i]['DutyStart'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotEnd']=$resultArr[$i]['DutyEnd'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ELReasonText'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ELReportSymbol'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['InWaived']=$resultArr[$i]['InWaived'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['OutWaived']=$resultArr[$i]['OutWaived'];
					$AssocMonthlyData[$TargetUserID]['RecordCount']++;
				}else{
					if($resultArr[$i]['OutWaived']==1){
						
					}else{
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotStart']=$resultArr[$i]['DutyStart'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotEnd']=$resultArr[$i]['DutyEnd'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ELReasonText'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ELReportSymbol'];
						$AssocMonthlyData[$TargetUserID]['RecordCount']++;
					}
				}
			}
			else
			{
				if($WithWaived==1){
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotStart']=$resultArr[$i]['DutyStart'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotEnd']=$resultArr[$i]['DutyEnd'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['InWaived']=$resultArr[$i]['InWaived'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['OutWaived']=$resultArr[$i]['OutWaived'];
					$AssocMonthlyData[$TargetUserID]['RecordCount']++;
				}else{
					if($resultArr[$i]['InWaived']==1){
						
					}else{
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotStart']=$resultArr[$i]['DutyStart'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotEnd']=$resultArr[$i]['DutyEnd'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'];
						$AssocMonthlyData[$TargetUserID]['RecordCount']++;
					}
				}
			}
  		}
  		
  		// Find Preset Leave Records after today
  		$today = date('Y-m-d');
  		$sql = "SELECT
					a.StaffID as UserID,
					a.SlotID,
					IF(RIGHT(a.RecordDate, 2) < '10', RIGHT(a.RecordDate, 1), RIGHT(a.RecordDate, 2)) as DayNumber,
					a.RecordType,
					IF(b.SlotName IS NULL, '-', b.SlotName) as SlotName,
					b.SlotStart,
					b.SlotEnd,
					c.ReasonText,
					c.ReportSymbol
				FROM
					CARD_STAFF_ATTENDANCE2_LEAVE_RECORD AS a
					INNER JOIN INTRANET_USER as u ON u.UserID = a.StaffID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND a.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as b ON b.SlotID = a.SlotID  
					LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as c ON c.ReasonID = a.ReasonID
				WHERE
					a.StaffID in (".((sizeof($Staffs) > 0)? implode(",",IntegerSafe($Staffs)):"-1").")
					AND a.RecordDate LIKE '$Year-$Month-%'
					AND a.RecordDate > '$today' 
					AND u.RecordType = '1' AND u.RecordStatus IN ($recordstatus) 
					AND
					(u.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
					 OR u.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
				ORDER BY
					a.RecordDate, u.EnglishName ";
  		$LeaveArr = $this->returnArray($sql);
  		for($i=0;$i<sizeof($LeaveArr);$i++)
  		{
  			$TargetUserID = $LeaveArr[$i]['UserID'];
  			$DayNum = $LeaveArr[$i]['DayNumber'];
  			$SlotName = $LeaveArr[$i]['SlotName'];
			$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$LeaveArr[$i]['RecordType'];
			$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$LeaveArr[$i]['ReasonText'];
			$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$LeaveArr[$i]['ReportSymbol'];
			$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotStart']=$resultArr[$i]['SlotStart'];
			$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['SlotEnd']=$resultArr[$i]['SlotEnd'];
			$AssocMonthlyData[$TargetUserID]['RecordCount']++;
  		}
  		
  		$ReturnArr = array();
  		if(sizeof($AssocMonthlyData)>0)
  		{
	  		foreach($AssocMonthlyData as $Key => $Value)
	  		{
	  			if($HaveRecord!=1 || ($HaveRecord==1 && $Value['RecordCount']>0))
	  			$ReturnArr[]=$Value;
	  		}
  		}
  		//echo "<PRE>";print_r($resultArr);echo "</PRE>";
  		return $ReturnArr;
  	}
  	
  	function Get_Report_Date_Range_Summary($StartDate,$EndDate,$Target,$WithWaived=1,$Keyword='',$HaveRecord=0)
  	{
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
  		
  		$start_ts = strtotime($StartDate);
  		$end_ts = strtotime($EndDate);
  		if($HaveRecord==1)
  			$joinby = " INNER ";
  		else
  			$joinby = " LEFT ";
  		//$NameField = getNameFieldByLang("u.");
  		$NameField = $this->Get_Name_Field("u.");
  		$ArchiveNameField = $this->Get_Archive_Name_Field("u.");
  		
  		if($Target=='-2'){// individual
  			$sql = "SELECT
						u.UserID,
						$NameField as StaffName,
						u.EnglishName  
					FROM 
						INTRANET_USER as u 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON u.UserID = p.UserID 
								AND (('$StartDate' BETWEEN p.PeriodStart AND p.PeriodEnd) 
									OR ('$EndDate' BETWEEN p.PeriodStart AND p.PeriodEnd)
									OR ('$StartDate' < p.PeriodStart AND '$EndDate' > p.PeriodEnd)
									OR ('$StartDate' > p.PeriodStart AND '$EndDate' < p.PeriodEnd)
									)
						LEFT JOIN
						CARD_STAFF_ATTENDANCE_USERGROUP ug 
						ON u.UserID = ug.UserID 
					WHERE 
						u.RecordType = '1' 
						AND u.RecordStatus IN ($recordstatus)
						AND ug.GroupID IS NULL 
						AND (u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							 OR u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ) ";
			if($ReportDisplayLeftStaff){
				$sql .= "UNION (
							SELECT
								u.UserID,
								$ArchiveNameField as StaffName,
								u.EnglishName  
							FROM 
								INTRANET_ARCHIVE_USER as u 
								INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON u.UserID = p.UserID 
										AND (('$StartDate' BETWEEN p.PeriodStart AND p.PeriodEnd) 
											OR ('$EndDate' BETWEEN p.PeriodStart AND p.PeriodEnd)
											OR ('$StartDate' < p.PeriodStart AND '$EndDate' > p.PeriodEnd)
											OR ('$StartDate' > p.PeriodStart AND '$EndDate' < p.PeriodEnd)
											)
								LEFT JOIN
								CARD_STAFF_ATTENDANCE_USERGROUP ug 
								ON u.UserID = ug.UserID 
							WHERE 
								u.RecordType = '1' 
								AND ug.GroupID IS NULL
								AND (u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
									 OR u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' )
						)";
			}
			$sql.= "ORDER BY EnglishName ";
			$UserList = $this->returnArray($sql);
  		}else{// a group
	  		$sql = "SELECT DISTINCT u.UserID, $NameField as StaffName,u.EnglishName  
					FROM 
						CARD_STAFF_ATTENDANCE_USERGROUP AS ug 
						INNER JOIN INTRANET_USER as u ON u.UserID = ug.UserID 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON u.UserID = p.UserID 
								AND (('$StartDate' BETWEEN p.PeriodStart AND p.PeriodEnd) 
									OR ('$EndDate' BETWEEN p.PeriodStart AND p.PeriodEnd)
									OR ('$StartDate' < p.PeriodStart AND '$EndDate' > p.PeriodEnd)
									OR ('$StartDate' > p.PeriodStart AND '$EndDate' < p.PeriodEnd)
									)
					WHERE
						ug.GroupID = '$Target' 
						AND u.RecordType = '1' AND u.RecordStatus IN ($recordstatus) 
						AND (u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							 OR u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ) ";
			if($ReportDisplayLeftStaff){
				$sql .= "UNION (
						SELECT DISTINCT u.UserID, $ArchiveNameField as StaffName, u.EnglishName  
						FROM 
							CARD_STAFF_ATTENDANCE_USERGROUP AS ug 
							INNER JOIN INTRANET_ARCHIVE_USER as u ON u.UserID = ug.UserID 
							INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as p ON u.UserID = p.UserID 
									AND (('$StartDate' BETWEEN p.PeriodStart AND p.PeriodEnd) 
										OR ('$EndDate' BETWEEN p.PeriodStart AND p.PeriodEnd)
										OR ('$StartDate' < p.PeriodStart AND '$EndDate' > p.PeriodEnd)
										OR ('$StartDate' > p.PeriodStart AND '$EndDate' < p.PeriodEnd)
										)
						WHERE
							ug.GroupID = '$Target' 
							AND u.RecordType = '1' 
							AND (u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								 OR u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ) 
						)";
			}
			$sql.= "ORDER BY EnglishName ";
			$UserList = $this->returnArray($sql);
  		}
  		$AssocData=array();
  		$UserIDList = array();
  		for($i=0;$i<sizeof($UserList);$i++)
  		{
  			$TargetUserID = $UserList[$i]['UserID'];
  			$AssocData[$TargetUserID]=array();
  			$AssocData[$TargetUserID]['UserID'] = $TargetUserID;
  			$AssocData[$TargetUserID]['StaffName']= $UserList[$i]['StaffName'];
  			$AssocData[$TargetUserID]['present']=0;
  			$AssocData[$TargetUserID]['absent']=0;
  			$AssocData[$TargetUserID]['late']=0;
  			$AssocData[$TargetUserID]['earlyleave']=0;
  			$AssocData[$TargetUserID]['outing']=0;
  			$AssocData[$TargetUserID]['holiday']=0;
  			$AssocData[$TargetUserID]['present_cnt']=0;
  			$AssocData[$TargetUserID]['absent_cnt']=0;
  			$AssocData[$TargetUserID]['late_cnt']=0;
  			$AssocData[$TargetUserID]['earlyleave_cnt']=0;
  			$AssocData[$TargetUserID]['outing_cnt']=0;
  			$AssocData[$TargetUserID]['holiday_cnt']=0;
  			$AssocData[$TargetUserID]['minlate']=0;
  			$AssocData[$TargetUserID]['minearlyleave']=0;
  			$AssocData[$TargetUserID]['RecordCount']=0;
  			$UserIDList[] = $TargetUserID;
  		}
  		//debug_r($UserList);
  		$user_csv = sizeof($UserIDList)>0?implode(",",$UserIDList):"-1";
  		for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts=mktime(0,0,0,((int)date('m',$cur_ts))+1,1,(int)date('Y',$cur_ts)) )
  		{
  			//$CurDate = date('Y-m-d',$cur_ts);
  			$CurYear = date('Y',$cur_ts);
  			$CurMonth = date('m',$cur_ts);
  			$card_log_table = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$CurYear."_".$CurMonth;
  			$sql = "SELECT 
						u.UserID,
						$NameField as StaffName,
						DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',c.DayNumber),'%Y-%m-%d') as RecordDate,
						c.InSchoolStatus,
						c.OutSchoolStatus,
						c.InWaived,
						c.OutWaived,
						IF(c.DutyCount IS NULL, 1, c.DutyCount) as DutyCount,
						wp.WorkingPeriodID,
						u.EnglishName,
						c.MinLate,
						c.MinEarlyLeave   
					FROM INTRANET_USER as u 
					LEFT JOIN $card_log_table as c ON c.StaffID = u.UserID AND (DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',c.DayNumber),'%Y-%m-%d') BETWEEN '$StartDate' AND '$EndDate' )
					LEFT JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID = u.UserID AND DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					WHERE 
						u.RecordType = '1' AND u.RecordStatus IN ($recordstatus) 
						AND u.UserID IN ($user_csv) 
						AND (u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
							 OR u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ) ";
			if($ReportDisplayLeftStaff){
				$sql .= "UNION (
						SELECT 
							u.UserID,
							$ArchiveNameField as StaffName,
							DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',c.DayNumber),'%Y-%m-%d') as RecordDate,
							c.InSchoolStatus,
							c.OutSchoolStatus,
							c.InWaived,
							c.OutWaived,
							IF(c.DutyCount IS NULL, 1, c.DutyCount) as DutyCount,
							wp.WorkingPeriodID,
							u.EnglishName,
							c.MinLate,
							c.MinEarlyLeave   
						FROM INTRANET_ARCHIVE_USER as u 
						LEFT JOIN $card_log_table as c ON c.StaffID = u.UserID AND (DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',c.DayNumber),'%Y-%m-%d') BETWEEN '$StartDate' AND '$EndDate' )
						LEFT JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID = u.UserID AND DATE_FORMAT(CONCAT('$CurYear-$CurMonth-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
						WHERE 
							u.RecordType = '1' 
							AND u.UserID IN ($user_csv) 
							AND (u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								 OR u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' ) 
						)";
			}
			$sql.= "ORDER BY EnglishName ";
  			$resultArr = $this->returnArray($sql);
  			//echo $sql."<br /><br />";
  			for($i=0;$i<sizeof($resultArr);$i++)
  			{
  				if(trim($resultArr[$i]['WorkingPeriodID'])=='') continue; // Not within working period, skip it
  				$TargetUserID = $resultArr[$i]['UserID'];
	  			$DutyCount = is_numeric($resultArr[$i]['DutyCount'])?$resultArr[$i]['DutyCount']:0.0;
	  			if($resultArr[$i]['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE)
	  			{
	  				if($WithWaived==1){// With Waived
	  					$AssocData[$TargetUserID]['earlyleave']+=$DutyCount;
	  					$AssocData[$TargetUserID]['earlyleave_cnt']+=$this->StatusCount;
	  					$AssocData[$TargetUserID]['minearlyleave']+=$resultArr[$i]['MinEarlyLeave'];
	  					$AssocData[$TargetUserID]['RecordCount']++;
	  				}else{ // Without Waived
		  				if($resultArr[$i]['OutWaived']==1)
		  				{
		  					//$AssocData[$TargetUserID]['present']+=$DutyCount;
		  				}
		  				else{
		  					$AssocData[$TargetUserID]['earlyleave']+=$DutyCount;
		  					$AssocData[$TargetUserID]['earlyleave_cnt']+=$this->StatusCount;
		  					$AssocData[$TargetUserID]['minearlyleave']+=$resultArr[$i]['MinEarlyLeave'];
		  					$AssocData[$TargetUserID]['RecordCount']++;
		  				}
	  				}
	  			}
	  			
	  			if($resultArr[$i]['InSchoolStatus'] != NULL && $resultArr[$i]['InSchoolStatus'] == CARD_STATUS_NORMAL && $resultArr[$i]['OutSchoolStatus'] != CARD_STATUS_EARLYLEAVE)
	  			{
	  				$AssocData[$TargetUserID]['present']+=$DutyCount;
	  				$AssocData[$TargetUserID]['present_cnt']+=$this->StatusCount;
	  				$AssocData[$TargetUserID]['RecordCount']++;
	  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_ABSENT)
	  			{
	  				if($WithWaived==1){// With Waived
	  					$AssocData[$TargetUserID]['absent']+=$DutyCount;
	  					$AssocData[$TargetUserID]['absent_cnt']+=$this->StatusCount;
	  					$AssocData[$TargetUserID]['RecordCount']++;
	  				}else{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					//$AssocData[$TargetUserID]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocData[$TargetUserID]['absent']+=$DutyCount;
		  					$AssocData[$TargetUserID]['absent_cnt']+=$this->StatusCount;
		  					$AssocData[$TargetUserID]['RecordCount']++;
		  				}
	  				}
	  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_LATE)
	  			{
	  				if($WithWaived==1){
	  					$AssocData[$TargetUserID]['late']+=$DutyCount;
	  					$AssocData[$TargetUserID]['late_cnt']+=$this->StatusCount;
	  					$AssocData[$TargetUserID]['minlate']+=$resultArr[$i]['MinLate'];
	  					$AssocData[$TargetUserID]['RecordCount']++;
	  				}else{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					
		  				}
		  				else{
		  					$AssocData[$TargetUserID]['late']+=$DutyCount;
		  					$AssocData[$TargetUserID]['late_cnt']+=$this->StatusCount;
		  					$AssocData[$TargetUserID]['minlate']+=$resultArr[$i]['MinLate'];
		  					$AssocData[$TargetUserID]['RecordCount']++;
		  				}
	  				}
	  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_OUTGOING)
	  			{
	  				if($WithWaived==1){
	  					$AssocData[$TargetUserID]['outing']+=$DutyCount;
	  					$AssocData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
	  					$AssocData[$TargetUserID]['RecordCount']++;
	  				}else{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					//$AssocData[$TargetUserID]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocData[$TargetUserID]['outing']+=$DutyCount;
		  					$AssocData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
		  					$AssocData[$TargetUserID]['RecordCount']++;
		  				}
	  				}
	  			}else if($resultArr[$i]['InSchoolStatus'] == CARD_STATUS_HOLIDAY)
	  			{
	  				if($WithWaived==1){
	  					$AssocData[$TargetUserID]['holiday']+=$DutyCount;
	  					$AssocData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
	  					$AssocData[$TargetUserID]['RecordCount']++;
	  				}else{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					//$AssocData[$TargetUserID]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocData[$TargetUserID]['holiday']+=$DutyCount;
		  					$AssocData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
		  					$AssocData[$TargetUserID]['RecordCount']++;
		  				}
	  				}
	  			}else if($resultArr[$i]['OutSchoolStatus'] != NULL && $resultArr[$i]['OutSchoolStatus'] == CARD_STATUS_NORMAL)
	  			{
	  				$AssocData[$TargetUserID]['present']+=$DutyCount;
	  				$AssocData[$TargetUserID]['present_cnt']+=$this->StatusCount;
	  				$AssocData[$TargetUserID]['RecordCount']++;
	  			}
  			}
  		}
  		
  		// Find Preset Leave Records after today
  		$today = date('Y-m-d');
  		$sql = "SELECT
					a.StaffID as UserID,
					a.SlotID,
					a.RecordType,
					IF(b.DutyCount IS NULL, 1.0, b.DutyCount) as DutyCount,
					wp.WorkingPeriodID  
				FROM
					CARD_STAFF_ATTENDANCE2_LEAVE_RECORD as a
					INNER JOIN INTRANET_USER as u ON u.UserID = a.StaffID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND a.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as b ON b.SlotID = a.SlotID 
				WHERE
					a.StaffID in ($user_csv)
					AND a.RecordDate < '$EndDate'
					AND a.RecordDate > '$today' 
					AND u.RecordType = '1' AND u.RecordStatus IN ($recordstatus)
					AND 
					(u.EnglishName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
					 OR u.ChineseName like '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%')
				ORDER BY u.EnglishName ";
  		$LeaveArr = $this->returnArray($sql);
  		for($i=0;$i<sizeof($LeaveArr);$i++)
  		{
  			$TargetUserID = $LeaveArr[$i]['UserID'];
  			$DutyCount = is_numeric($LeaveArr[$i]['DutyCount'])?$LeaveArr[$i]['DutyCount']:0.0;
  			if(trim($LeaveArr[$i]['WorkingPeriodID'])!="") $AssocData[$TargetUserID]['period']+=1; 
  			if (trim($LeaveArr[$i]['SlotID']) == "")
  			{
				// Full Day Leave
				if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
				{ // holiday
					$AssocData[$TargetUserID]['holiday']+=1;
					$AssocData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
					$AssocData[$TargetUserID]['RecordCount']++;
				}else 
				{ // outgoing
					$AssocData[$TargetUserID]['outing']+=1;
					$AssocData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
					$AssocData[$TargetUserID]['RecordCount']++;
				}
			}else
			{
				if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
				{ // holiday
					$AssocData[$TargetUserID]['holiday']+=$DutyCount;
					$AssocData[$TargetUserID]['holiday_cnt']+=$this->StatusCount;
					$AssocData[$TargetUserID]['RecordCount']++;
				}else
				{ // outgoing
					$AssocData[$TargetUserID]['outing']+=$DutyCount;
					$AssocData[$TargetUserID]['outing_cnt']+=$this->StatusCount;
					$AssocData[$TargetUserID]['RecordCount']++;
				}
			}
  		}
  		
  		$ReturnArr = array();
  		foreach($AssocData as $Key => $Value)
  		{
  			if($HaveRecord!=1 || ($HaveRecord==1 && $Value['RecordCount']>0))
  				$ReturnArr[]=$Value;
  		}
  		
  		return $ReturnArr;
  	}
  	
  	function Get_Report_DailyLog_Data($TargetDate, $SelectType, $SelectStaff, $Keyword="", $Flag="", $SortField="", $SortOrder="", $IncludeAbsentSuspectedRecord=false)
  	{
  		//$CARD_STATUS_LATEEARLYLEAVE = 6;
  		//$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
  		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
  		
  		$TargetDate = date("Y-m-d",strtotime($TargetDate));
  		$DateArr = explode('-',$TargetDate);
  		$Year = $DateArr[0];
  		$Month = $DateArr[1];
  		$Day = $DateArr[2];
  		$SortField=$SortField==""?"EnglishName":$SortField;
  		$SortField2=$SortField=="GroupName"?"EnglishName":"GroupName";
  		$SortOrder=$SortOrder==""?"ASC":$SortOrder;
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		//$NameField = getNameFieldByLang("b.");
  		$NameField = $this->Get_Name_Field("b.");
  		$ArchiveNameField = $this->Get_Archive_Name_Field("b.");
  		
  		if($Flag=="SmartCard")
  		{
  			$StaffID = $SelectStaff;
  			$sql = "SELECT
						b.UserID,
					  	$NameField as StaffName,
						DATE_FORMAT(CONCAT($Year,'-',$Month,'-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)),'%Y-%m-%d') as RecordDate,
						c.DayNumber,
						-1 as GroupID,
						CONCAT('') as GroupName,
					  	c.SlotName,
						c.DutyStart,
						c.DutyEnd,
						c.InTime,
						c.OutTime,
						c.InSchoolStation,
						c.OutSchoolStation,
						c.InWaived,
						c.OutWaived,
						c.MinLate,
						c.MinEarlyLeave
					FROM
						INTRANET_USER as b 
						INNER JOIN $card_log_table_name as c 
						ON c.StaffID = b.UserID AND (c.SlotName IS NOT NULL) 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
						ON wp.UserID=b.UserID 
							AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					WHERE 
						b.RecordType='1'
					  	AND b.RecordStatus IN ($recordstatus) 
						AND b.UserID = '".IntegerSafe($StaffID)."' 
						AND (c.SlotName LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%') 
					ORDER BY c.DayNumber, DutyStart ";
  		}else
  		{
	  		$group_cond =($SelectStaff > 0)? " AND d.GroupID = '".IntegerSafe($SelectStaff)."' ":"";
	  		$FinalIDs=array();
	  		// Select Group Users
	  		if($SelectStaff != -2)// -2 Individual Staff Only
	  		{
		  		$sql = "SELECT 
							DISTINCT b.UserID 
						FROM CARD_STAFF_ATTENDANCE_USERGROUP as a 
							INNER JOIN CARD_STAFF_ATTENDANCE2_GROUP as d ON d.GroupID = a.GroupID
							INNER JOIN INTRANET_USER as b ON b.UserID = a.UserID 
						WHERE 
							b.RecordType='1' 
						  	AND b.RecordStatus IN ($recordstatus) 
							AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								OR d.GroupName LIKE '%".$this->Get_Safe_Sql_Query($Keyword)."%') 
							$group_cond ";
				if($ReportDisplayLeftStaff){
					$sql .= "UNION 
							SELECT 
								DISTINCT b.UserID 
							FROM CARD_STAFF_ATTENDANCE_USERGROUP as a 
								INNER JOIN CARD_STAFF_ATTENDANCE2_GROUP as d ON d.GroupID = a.GroupID
								INNER JOIN INTRANET_ARCHIVE_USER as b ON b.UserID = a.UserID AND b.RecordType='1' 
							WHERE 
								b.RecordType='1' 
								AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
									OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
									OR d.GroupName LIKE '%".$this->Get_Safe_Sql_Query($Keyword)."%') 
								$group_cond ";
				}
				$FinalIDs = $this->returnVector($sql);
	  		}
			//Also Select Individual Users not belongs to any group
			if($SelectStaff <= -1 || $SelectStaff==null)// -1 All Staffs; -2 Individual Staffs
			{
				$IndividualUsersArr = $this->Get_Aval_User_List();
				$IDS = array();
				for($i=0;$i<sizeof($IndividualUsersArr);$i++)
				{
					$IDS[$i] = $IndividualUsersArr[$i]['UserID'];
				}
				for($n=0;$n<sizeof($IDS);$n++)
				{
					$FinalIDs[]=$IDS[$n];
				}
			}
			$sql = "SELECT
							b.UserID,
						  	$NameField as StaffName,
							CONCAT($Year,'-',$Month,'-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) as RecordDate,
							c.DayNumber,
							IF(d.GroupID IS NULL, -1, d.GroupID) as GroupID,
							IF(d.GroupName IS NULL, '-', d.GroupName) as GroupName,
						  	c.SlotName,
							c.DutyStart,
							c.DutyEnd,
							c.InTime,
							c.OutTime,
							c.InSchoolStation,
							c.OutSchoolStation,
							c.InWaived,
							c.OutWaived,
							c.MinLate,
							c.MinEarlyLeave,
							b.EnglishName 
						FROM
							INTRANET_USER as b 
							INNER JOIN $card_log_table_name as c ON c.StaffID = b.UserID AND (c.SlotName IS NOT NULL) ";
			if(!$IncludeAbsentSuspectedRecord){
				$sql .= " AND (c.RecordStatus IS NOT NULL OR c.InTime IS NOT NULL OR c.OutTime IS NOT NULL) ";
			}
				if($Flag=="") 
				  $sql .= " AND c.DayNumber = '$Day' ";
				$sql .= "  		
							INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
							ON wp.UserID=b.UserID 
								AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd ";
				$sql .= "   
							LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as a ON a.UserID = b.UserID 
							LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as d ON d.GroupID = a.GroupID ";
				$sql .=	"WHERE
							b.RecordType='1'
						  	AND b.RecordStatus IN ($recordstatus)
							AND b.UserID IN (".(sizeof($FinalIDs)>0?implode(",",$FinalIDs):"-1").") 
							AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								OR c.SlotName LIKE '%".$this->Get_Safe_Sql_Query($Keyword)."%' ) 
						";
			if($ReportDisplayLeftStaff) {
				$sql .= "UNION 
						SELECT
							b.UserID,
						  	$ArchiveNameField as StaffName,
							CONCAT($Year,'-',$Month,'-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) as RecordDate,
							c.DayNumber,
							IF(d.GroupID IS NULL, -1, d.GroupID) as GroupID,
							IF(d.GroupName IS NULL, '-', d.GroupName) as GroupName,
						  	c.SlotName,
							c.DutyStart,
							c.DutyEnd,
							c.InTime,
							c.OutTime,
							c.InSchoolStation,
							c.OutSchoolStation,
							c.InWaived,
							c.OutWaived,
							c.MinLate,
							c.MinEarlyLeave,
							b.EnglishName 
						FROM
							INTRANET_ARCHIVE_USER as b 
							INNER JOIN $card_log_table_name as c ON c.StaffID = b.UserID AND (c.SlotName IS NOT NULL) ";
				if(!$IncludeAbsentSuspectedRecord){
					$sql .= " AND (c.RecordStatus IS NOT NULL OR c.InTime IS NOT NULL OR c.OutTime IS NOT NULL) ";
				}
				if($Flag=="") 
				  $sql .= " AND c.DayNumber = '$Day' ";
				$sql .= "  		
							INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
							ON wp.UserID=b.UserID 
								AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd ";
				$sql .= "   
							LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as a ON a.UserID = b.UserID 
							LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as d ON d.GroupID = a.GroupID ";
				$sql .=	"WHERE
							b.RecordType='1'
							AND b.UserID IN (".(sizeof($FinalIDs)>0?implode(",",$FinalIDs):"-1").") 
							AND (b.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								OR b.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								OR c.SlotName LIKE '%".$this->Get_Safe_Sql_Query($Keyword)."%' ) 
						";
			}
			if($Flag=="SmartCard")
				$sql .= " ORDER BY RecordDate, DutyStart ";
			else
				$sql .= " ORDER BY $SortField $SortOrder, $SortField2 $SortOrder, DayNumber, DutyStart ";
			
  		}
		//debug_r($sql);
		$resultArr = $this->returnArray($sql);
		$ReturnArray=array();
		$AssocData=array();
  		$StaffIDs = array();
  		for($i=0;$i<sizeof($resultArr);$i++)
  		{
  			$TargetUserID = $resultArr[$i]['UserID'];
  			$DayNumber = $resultArr[$i]['DayNumber'];
  			$SlotName = $resultArr[$i]['SlotName'];
  			$AssocData[$TargetUserID]['UserID'] = $TargetUserID;
  			$AssocData[$TargetUserID]['StaffName'] = $resultArr[$i]['StaffName'];
  			$AssocData[$TargetUserID]['GroupName'] = $resultArr[$i]['GroupName'];
  			$AssocData[$TargetUserID]['Log'][$DayNumber][$SlotName]=$resultArr[$i];
  			if(!in_array($TargetUserID,$StaffIDs)) $StaffIDs[]=$TargetUserID;
  		}
  		for($i=0;$i<sizeof($StaffIDs);$i++)
  		{
  			$ReturnArray[]=$AssocData[$StaffIDs[$i]];
  		}
  		return $ReturnArray;
  	}
  	// Default $TypeOfReport == 1: Monthly; $TypeOfReport == 2: Weekly/Daily;
  	function Get_Report_Customize_Monthly_Detail_Report_Data($Params, $Year, $Month, $TypeOfReport=1)
  	{
  		global $sys_custom;
  		$CARD_STATUS_LATEEARLYLEAVE = 6;
  		$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$total_day = date("t",mktime(0,0,0,intval($Month),1,intval($Year)));
  		
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
  		//$NameField = getNameFieldByLang("b.");
  		$NameField = $this->Get_Name_Field("b.");
  		$ArchiveNameField = $this->Get_Archive_Name_Field("b.");
  		
  		$StartDate = explode('-',$Params["TargetStartDate"]);
  		$StartYear = $StartDate[0];
  		$StartMonth = $StartDate[1];
  		$StartDay = $StartDate[2];
  		$EndDate = explode('-',$Params["TargetEndDate"]);
  		$EndYear = $EndDate[0];
  		$EndMonth = $EndDate[1];
  		$EndDay = $EndDate[2];
  		
  		$StartDayCond = ($Year==$StartYear && $Month==$StartMonth)?" AND c.DayNumber >= '$StartDay' ":"";
  		$EndDayCond = ($Year==$EndYear && $Month==$EndMonth)?" AND c.DayNumber <= '$EndDay' ":"";
  		
  		$StatusArr = array();
  		if($Params["StatusPresent"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_NORMAL;
  		}
  		if($Params["StatusAbsent"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_ABSENT;
  		}
  		if($Params["StatusLate"]==1 && $Params["StatusEarlyLeave"]==1)
  		{
  			$StatusArr[] = $CARD_STATUS_LATEEARLYLEAVE;
  			$StatusArr[] = CARD_STATUS_LATE;
  			$StatusArr[] = CARD_STATUS_EARLYLEAVE;
  		}else
  		{
	  		if($Params["StatusLate"]==1)
	  		{
	  			$StatusArr[] = CARD_STATUS_LATE;
	  		}
	  		if($Params["StatusEarlyLeave"]==1)
	  		{
	  			$StatusArr[] = CARD_STATUS_EARLYLEAVE;
	  		}
  		}
  		if($Params["StatusOuting"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_OUTGOING;
  		}
  		if($Params["StatusHoliday"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_HOLIDAY;
  		}
  		if($Params["StatusAll"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_NORMAL;
  			$StatusArr[] = CARD_STATUS_ABSENT;
  			$StatusArr[] = $CARD_STATUS_LATEEARLYLEAVE;
  			$StatusArr[] = CARD_STATUS_LATE;
  			$StatusArr[] = CARD_STATUS_EARLYLEAVE;
  			$StatusArr[] = CARD_STATUS_OUTGOING;
  			$StatusArr[] = CARD_STATUS_HOLIDAY;
  		}
  		$AbsentSuspectedCond = "";
  		if($Params["StatusAbsentSuspected"]==1 || $Params["StatusAll"]==1){
  			$StatusArr[] = $CARD_STATUS_ABSENT_SUSPECTED;
  			$AbsentSuspectedCond = " OR (c.InSchoolStatus IS NULL AND c.OutSchoolStatus IS NULL) ";
  		}
  		
  		if($Params["SelectWaived"]==1){ // with waived records
  			$waived_cond = "";
  		}else{ // without waived records
  			//$waived_cond = " AND ((IF(c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus <> ".CARD_STATUS_NORMAL.",c.InWaived IS NULL OR c.InWaived <> 1,1))
			//				 OR (IF(c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus <> ".CARD_STATUS_NORMAL.",c.OutWaived IS NULL OR c.OutWaived <> 1,1))) ";
			$waived_cond = " AND (IF(c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus <> ".CARD_STATUS_NORMAL.",IF(c.InSchoolStatus='".CARD_STATUS_LATE."' AND c.OutSchoolStatus='".CARD_STATUS_EARLYLEAVE."',TRIM(c.InWaived)<>'1' OR c.InWaived IS NULL OR TRIM(c.OutWaived)<>'1' OR c.OutWaived IS NULL,c.InWaived IS NULL OR c.InWaived <> 1),1)) 
							  AND (IF(c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus <> ".CARD_STATUS_NORMAL.",IF(c.InSchoolStatus='".CARD_STATUS_LATE."' AND c.OutSchoolStatus='".CARD_STATUS_EARLYLEAVE."',TRIM(c.InWaived)<>'1' OR c.InWaived IS NULL OR TRIM(c.OutWaived)<>'1' OR c.OutWaived IS NULL,c.OutWaived IS NULL OR c.OutWaived <> 1),1)) ";

			if(in_array(CARD_STATUS_EARLYLEAVE, $StatusArr) && !in_array(CARD_STATUS_LATE, $StatusArr)) {
				$waived_cond = " AND (IF(c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus <> ".CARD_STATUS_NORMAL.",c.OutWaived IS NULL OR c.OutWaived <> 1,1)) ";
			}
  		}
  		
  		
			//$StaffID = $Params["StaffID"];
			$StaffSelection = sizeof($Params['SelectStaff'])>0?implode(",",$Params['SelectStaff']):"-1";
			
			// Show all staffs and fill in remaining off duty days 
	  		if($Params["HaveRecord"]!=1 || $Params["DisplayAllCalendarDay"]==1){
	  			$sql = "SELECT 
							b.UserID,
							$NameField as StaffName,
							IF(g.GroupName IS NULL,'-',g.GroupName) as GroupName,
							b.EnglishName  
						FROM 
						INTRANET_USER as b ";
				$sql .= "
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID 
							AND (('".$Params["TargetStartDate"]."' BETWEEN wp.PeriodStart AND wp.PeriodEnd) 
									OR ('".$Params["TargetEndDate"]."' BETWEEN wp.PeriodStart AND wp.PeriodEnd)
									OR ('".$Params["TargetStartDate"]."' < wp.PeriodStart AND '".$Params["TargetEndDate"]."' > wp.PeriodEnd)
									OR ('".$Params["TargetStartDate"]."' > wp.PeriodStart AND '".$Params["TargetEndDate"]."' < wp.PeriodEnd)
									)";
				$sql .= "
						LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as u ON u.UserID = b.UserID 
						LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID 
						WHERE 
							b.UserID IN (".$StaffSelection.") 
							AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) 
						GROUP BY b.UserID ";
				if($ReportDisplayLeftStaff) {
					$sql .= "UNION 
								SELECT 
									b.UserID,
									$ArchiveNameField as StaffName,
									IF(g.GroupName IS NULL,'-',g.GroupName) as GroupName,
									b.EnglishName  
								FROM 
								INTRANET_ARCHIVE_USER as b ";
							$sql .= "
									INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID 
										AND (('".$Params["TargetStartDate"]."' BETWEEN wp.PeriodStart AND wp.PeriodEnd) 
												OR ('".$Params["TargetEndDate"]."' BETWEEN wp.PeriodStart AND wp.PeriodEnd)
												OR ('".$Params["TargetStartDate"]."' < wp.PeriodStart AND '".$Params["TargetEndDate"]."' > wp.PeriodEnd)
												OR ('".$Params["TargetStartDate"]."' > wp.PeriodStart AND '".$Params["TargetEndDate"]."' < wp.PeriodEnd)
												)";
							$sql .= "
									LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as u ON u.UserID = b.UserID 
									LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID 
									WHERE 
										b.UserID IN (".$StaffSelection.") 
										AND b.RecordType = '1' 
									GROUP BY b.UserID 
							";
				}
				$sql.= "ORDER BY EnglishName ";
	  			$staff_info_list = $this->returnArray($sql);
	  			//debug_r($staff_info_list);
	  		}
			
			$sql = "SELECT
						b.UserID,
					  	$NameField as StaffName,
						IF(g.GroupName IS NULL,'-',g.GroupName) as GroupName,
						IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber) as DayNumber,
					  	c.SlotName,
						(CASE 
							WHEN c.InSchoolStatus = '".CARD_STATUS_OUTGOING."' THEN '".CARD_STATUS_OUTGOING."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' THEN '".CARD_STATUS_HOLIDAY."' ";
			if(in_array(CARD_STATUS_LATE,$StatusArr) && !in_array(CARD_STATUS_EARLYLEAVE,$StatusArr)){ // only checked late
				$sql .= " 	WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' ";
			}else if(in_array(CARD_STATUS_EARLYLEAVE,$StatusArr) && !in_array(CARD_STATUS_LATE,$StatusArr)){ // only checked early leave
				$sql .= "	WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' ";
			}else{ // checked both late and early leave
				$sql .= "   WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' AND c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".$CARD_STATUS_LATEEARLYLEAVE."' ";
				$sql .= " 	WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' ";
				$sql .= "	WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' ";
  			}
				$sql .= "	WHEN c.InSchoolStatus = '".CARD_STATUS_ABSENT."' THEN '".CARD_STATUS_ABSENT."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
							WHEN c.OutSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
							ELSE '".$CARD_STATUS_DEFAULT."'
						END ) as Status,
						c.DutyStart,
						c.DutyEnd,
						c.InTime,
						c.OutTime,
						c.InSchoolStatus,
						c.OutSchoolStatus,
						c.InSchoolStation,
						c.OutSchoolStation,
						c.MinLate,
						c.MinEarlyLeave,
						e.ReasonText,
						e.ReasonID,
						h.ReasonText as ELReasonText,
						h.ReasonID as ELReasonID,
						c.InWaived as InWaived,
						c.OutWaived as OutWaived,
						c.Remark,";
			if($sys_custom['StaffAttendance']['DoctorCertificate'] && $Params["ColDoctorCertificate"]==1){
				$sql .= "GROUP_CONCAT(CONCAT(k.FileName,' (',k.FileSize,' KB)') SEPARATOR ', ') as DoctorCertificates,";
			}
				$sql.= "b.EnglishName 
					 FROM
						INTRANET_USER as b
						INNER JOIN $card_log_table_name as c ON c.StaffID = b.UserID  $waived_cond ";
			if($Params["StatusAbsentSuspected"]==1 || $Params["StatusAll"]==1){
				
			}else{
				$sql .= "	AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1')) ";
			}
			$sql .= "	INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
						LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as u ON u.UserID = b.UserID
						LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID
						LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS d on d.StaffID = c.StaffID and d.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and d.RecordID = c.InAttendanceRecordID  
				  		LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS e on e.ReasonID = d.ReasonID
						LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS f on f.StaffID = c.StaffID and f.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and f.RecordID = c.OutAttendanceRecordID
				  		LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS h on h.ReasonID = f.ReasonID ";
			if($sys_custom['StaffAttendance']['DoctorCertificate'] && $Params["ColDoctorCertificate"]==1){
				$sql .= "LEFT JOIN CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE as k ON k.DailyLogID=c.RecordID AND k.Year='".$Year."' AND k.Month='".$Month."' ";
			}
			$sql .= "WHERE
						b.UserID IN (".$StaffSelection.") 
						AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) 
						AND (c.RecordType IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").")
							OR c.InSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").")
							OR c.OutSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").") $AbsentSuspectedCond)
						$StartDayCond 
						$EndDayCond
					 GROUP BY
						c.RecordID ";
			if($ReportDisplayLeftStaff){
				$sql .= "UNION 
						SELECT
							b.UserID,
						  	$ArchiveNameField as StaffName,
							IF(g.GroupName IS NULL,'-',g.GroupName) as GroupName,
							IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber) as DayNumber,
						  	c.SlotName,
							(CASE 
								WHEN c.InSchoolStatus = '".CARD_STATUS_OUTGOING."' THEN '".CARD_STATUS_OUTGOING."' 
								WHEN c.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' THEN '".CARD_STATUS_HOLIDAY."' ";
				if(in_array(CARD_STATUS_LATE,$StatusArr) && !in_array(CARD_STATUS_EARLYLEAVE,$StatusArr)){ // only checked late
					$sql .= " 	WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' ";
				}else if(in_array(CARD_STATUS_EARLYLEAVE,$StatusArr) && !in_array(CARD_STATUS_LATE,$StatusArr)){ // only checked early leave
					$sql .= "	WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' ";
				}else{ // checked both late and early leave
					$sql .= "	WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' AND c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".$CARD_STATUS_LATEEARLYLEAVE."' ";
					$sql .=	"	WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' ";
					$sql .= "	WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' "; 
				}
					$sql .= "	WHEN c.InSchoolStatus = '".CARD_STATUS_ABSENT."' THEN '".CARD_STATUS_ABSENT."' 
								WHEN c.InSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
								WHEN c.OutSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
								ELSE '".$CARD_STATUS_DEFAULT."'
							END ) as Status,
							c.DutyStart,
							c.DutyEnd,
							c.InTime,
							c.OutTime,
							c.InSchoolStatus,
							c.OutSchoolStatus,
							c.InSchoolStation,
							c.OutSchoolStation,
							c.MinLate,
							c.MinEarlyLeave,
							e.ReasonText,
							e.ReasonID,
							h.ReasonText as ELReasonText,
							h.ReasonID as ELReasonID,
							c.InWaived as InWaived,
							c.OutWaived as OutWaived,
							c.Remark,";
				if($sys_custom['StaffAttendance']['DoctorCertificate'] && $Params["ColDoctorCertificate"]==1){
					$sql .= "GROUP_CONCAT(CONCAT(k.FileName,' (',k.FileSize,' KB)') SEPARATOR ', ') as DoctorCertificates,";
				}			
				$sql .= "	b.EnglishName 
						 FROM
							INTRANET_ARCHIVE_USER as b
							INNER JOIN $card_log_table_name as c ON c.StaffID = b.UserID $waived_cond ";
				if($Params["StatusAbsentSuspected"]==1 || $Params["StatusAll"]==1){
					
				}else{
					$sql .= "	AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1')) ";
				}
				$sql .= "	INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
							LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as u ON u.UserID = b.UserID
							LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID
							LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS d on d.StaffID = c.StaffID and d.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and d.RecordID = c.InAttendanceRecordID  
					  		LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS e on e.ReasonID = d.ReasonID
							LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS f on f.StaffID = c.StaffID and f.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and f.RecordID = c.OutAttendanceRecordID
					  		LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS h on h.ReasonID = f.ReasonID ";
			if($sys_custom['StaffAttendance']['DoctorCertificate'] && $Params["ColDoctorCertificate"]==1){
				$sql .= "	LEFT JOIN CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE as k ON k.DailyLogID=c.RecordID AND k.Year='".$Year."' AND k.Month='".$Month."' ";
			}
				$sql .= "WHERE
							b.UserID IN (".$StaffSelection.") 
							AND b.RecordType = '1'  
							AND (c.RecordType IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").")
								OR c.InSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").")
								OR c.OutSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").") $AbsentSuspectedCond)
							$StartDayCond 
							$EndDayCond
						 GROUP BY
							c.RecordID 
					";
			}
			
			//		"ORDER BY
			//			c.DayNumber, b.EnglishName, c.DutyStart ";
			
			$today = date('Y-m-d');
			if(strtotime($Params["TargetEndDate"])>strtotime($today))
			{
				$sql .= "UNION 
						 (SELECT
							lr.StaffID as UserID,
							$NameField as StaffName,
							IF(g.GroupName IS NULL, '-',g.GroupName) as GroupName,
							SUBSTR(lr.RecordDate,9,2) as DayNumber,
							s.SlotName,
							lr.RecordType as Status,
							s.SlotStart as DutyStart,
							s.SlotEnd as DutyEnd,
							NULL as InTime,
							NULL as OutTime,
							lr.RecordType as InSchoolStatus,
							lr.RecordType as OutSchoolStatus,
							NULL as InSchoolStation,
							NULL as OutSchoolStation,
							NULL as MinLate,
							NULL as MinEarlyLeave,
							c.ReasonText,
							c.ReasonID,
							NULL as ELReasonText,
							NULL as ELReasonID,
							NULL as InWaived,
							NULL as OutWaived,
							NULL as Remark,";
				if($sys_custom['StaffAttendance']['DoctorCertificate'] && $Params["ColDoctorCertificate"]==1){
					$sql .= "'' as DoctorCertificates,";	
				}
				$sql .= "	b.EnglishName 
						FROM 
							CARD_STAFF_ATTENDANCE2_LEAVE_RECORD AS lr 
							INNER JOIN INTRANET_USER as b ON b.UserID = lr.StaffID and b.UserID = '$StaffID' 
							INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND lr.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
							LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as ug ON ug.UserID = b.UserID 
							LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = ug.GroupID
							LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as s ON s.SlotID = lr.SlotID  
							LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as c ON c.ReasonID = lr.ReasonID
						WHERE 
							b.UserID IN (".$StaffSelection.") 
							AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) 
							AND lr.RecordType IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").")
							AND lr.RecordDate BETWEEN '".$Params["TargetStartDate"]."' AND '".$Params["TargetEndDate"]."' 
							AND lr.RecordDate > '$today' 
						GROUP BY 
							lr.RecordID)";
			}
			$sql .= " ORDER BY
						DayNumber, EnglishName, DutyStart ";			
			//debug_r($sql);
			$resultArr = $this->returnArray($sql);
			//debug_r($resultArr);
  		//}
		if($TypeOfReport == 2)
		{
			$AssocMonthlyData=array();
  			
	  		for($i=0;$i<sizeof($resultArr);$i++)
	  		{
	  			//if(in_array($resultArr[$i]['InSchoolStatus'],$StatusArr) || in_array($resultArr[$i]['OutSchoolStatus'],$StatusArr))
	  			if(in_array($resultArr[$i]['Status'],$StatusArr))
	  			{
		  			$TargetUserID = $resultArr[$i]['UserID'];
		  			$AssocMonthlyData[$TargetUserID]=array();
		  			for($j=1;$j<=$total_day;$j++)
					{
						$datekey = $Year.'-'.$Month.'-'.(($j<10)?'0'.$j:$j);
						$AssocMonthlyData[$TargetUserID][$datekey]=array();
			  			$AssocMonthlyData[$TargetUserID]['UserID'] = $TargetUserID;
			  			$AssocMonthlyData[$TargetUserID]['StaffName']= $resultArr[$i]['StaffName'];
			  			$AssocMonthlyData[$TargetUserID]['GroupName']= $resultArr[$i]['GroupName'];
			  			$AssocMonthlyData[$TargetUserID]['EnglishName']= $resultArr[$i]['EnglishName'];
					}
	  			}
	  		}
	  		
	  		for($i=0;$i<sizeof($resultArr);$i++)
	  		{
	  			//if(in_array($resultArr[$i]['InSchoolStatus'],$StatusArr) || in_array($resultArr[$i]['OutSchoolStatus'],$StatusArr))
	  			if(in_array($resultArr[$i]['Status'],$StatusArr))
	  			{
		  			$TargetUserID = $resultArr[$i]['UserID'];
		  			$DayNum = $resultArr[$i]['DayNumber'];
		  			$SlotName = $resultArr[$i]['SlotName'];
		  			$datekey = $Year.'-'.$Month.'-'.$DayNum;
		  			$AssocMonthlyData[$TargetUserID][$datekey][]=$resultArr[$i];
	  			}
	  		}
	  		
	  		if($Params["HaveRecord"]!=1 || $Params["DisplayAllCalendarDay"]==1){
	  			$tsTargetStartDate = strtotime($Params["TargetStartDate"]);
	  			$tsTargetEndDate = strtotime($Params["TargetEndDate"]);
	  			$tsFirstDate = strtotime($Year."-".$Month."-01");
	  			$tsLastDate = mktime(0,0,0,(int)$Month,date("t",$tsFirstDate),(int)$Year);
	  			$tsStartDate = $tsTargetStartDate>$tsFirstDate?$tsTargetStartDate:$tsFirstDate;
	  			$tsEndDate = $tsTargetEndDate<$tsLastDate?$tsTargetEndDate:$tsLastDate;
	  			$tsOneDay = 24*60*60;
	  			for($tsCurDate=$tsStartDate;$tsCurDate<=$tsEndDate;$tsCurDate+=$tsOneDay){
	  				for($i=0;$i<sizeof($staff_info_list);$i++){
	  					$TargetUserID = $staff_info_list[$i]['UserID'];
	  					$datekey = date("Y-m-d",$tsCurDate);
	  					if(sizeof($AssocMonthlyData[$TargetUserID][$datekey])==0){
	  						//if($Params["HaveRecord"]!=1){
	  							//$AssocMonthlyData[$TargetUserID][$datekey][]=array("UserID"=>$TargetUserID,"Status"=>"OffDuty","StaffName"=>$staff_info_list[$i]['StaffName'],"GroupName"=>$staff_info_list[$i]['GroupName']);
	  						//}else 
	  						if($Params["DisplayAllCalendarDay"]==1){
	  							$AssocMonthlyData[$TargetUserID][$datekey][]=array("UserID"=>$TargetUserID,"Status"=>"-","StaffName"=>$staff_info_list[$i]['StaffName'],"GroupName"=>$staff_info_list[$i]['GroupName'],"SlotName"=>"-");
	  						}
			  				$AssocMonthlyData[$TargetUserID]['UserID'] = $TargetUserID;
			  				$AssocMonthlyData[$TargetUserID]['StaffName']= $staff_info_list[$i]['StaffName'];
			  				$AssocMonthlyData[$TargetUserID]['GroupName']= $staff_info_list[$i]['GroupName'];
			  				$AssocMonthlyData[$TargetUserID]['EnglishName']= $staff_info_list[$i]['EnglishName'];
	  					}
	  				}
	  			}
	  		}
	  		
	  		return $AssocMonthlyData;
		}
		
		$ReturnArr = array();
		$AssocMonthlyData=array();
		if(sizeof($StatusArr)>0)
		{
			for($i=0;$i<sizeof($resultArr);$i++)
			{
				//if(in_array($resultArr[$i]['InSchoolStatus'],$StatusArr) || in_array($resultArr[$i]['OutSchoolStatus'],$StatusArr))
				if(in_array($resultArr[$i]['Status'],$StatusArr))
				{
					$ReturnArr[] = $resultArr[$i];
					$datekey = $datekey = $Year.'-'.$Month.'-'.$resultArr[$i]['DayNumber'];
					$TargetUserID = $resultArr[$i]['UserID'];
					$AssocMonthlyData[$TargetUserID][$datekey] = $resultArr[$i];
				}
			}
			if($Params["HaveRecord"]!=1 || $Params["DisplayAllCalendarDay"]==1){
	  			$tsTargetStartDate = strtotime($Params["TargetStartDate"]);
	  			$tsTargetEndDate = strtotime($Params["TargetEndDate"]);
	  			$tsFirstDate = strtotime($Year."-".$Month."-01");
	  			$tsLastDate = mktime(0,0,0,(int)$Month,date("t",$tsFirstDate),(int)$Year);
	  			$tsStartDate = $tsTargetStartDate>$tsFirstDate?$tsTargetStartDate:$tsFirstDate;
	  			$tsEndDate = $tsTargetEndDate<$tsLastDate?$tsTargetEndDate:$tsLastDate;
	  			$tsOneDay = 24*60*60;
	  			for($tsCurDate=$tsStartDate;$tsCurDate<=$tsEndDate;$tsCurDate+=$tsOneDay){
	  				for($i=0;$i<sizeof($staff_info_list);$i++){
	  					$TargetUserID = $staff_info_list[$i]['UserID'];
	  					$datekey = date("Y-m-d",$tsCurDate);
	  					if(sizeof($AssocMonthlyData[$TargetUserID][$datekey])==0){
	  						$daynumber = date("d",$tsCurDate);
	  						//if($Params["HaveRecord"]!=1){
	  							//$AssocMonthlyData[$TargetUserID][$datekey][]=array("Status"=>"OffDuty","StaffName"=>$staff_info_list[$i]['StaffName'],"GroupName"=>$staff_info_list[$i]['GroupName']);
	  							//$ResultArr[] = array("UserID"=>$TargetUserID,"Status"=>"OffDuty","StaffName"=>$staff_info_list[$i]['StaffName'],"GroupName"=>$staff_info_list[$i]['GroupName'],"DayNumber"=>$daynumber);
	  						//}else 
	  						if($Params["DisplayAllCalendarDay"]==1){
	  							$AssocMonthlyData[$TargetUserID][$datekey][]=array("Status"=>"-","StaffName"=>$staff_info_list[$i]['StaffName'],"GroupName"=>$staff_info_list[$i]['GroupName'],"EnglishName"=>$staff_info_list[$i]['EnglishName']);
	  							$ReturnArr[] = array("UserID"=>$TargetUserID,"Status"=>"-","StaffName"=>$staff_info_list[$i]['StaffName'],"GroupName"=>$staff_info_list[$i]['GroupName'],"DayNumber"=>$daynumber,"SlotName"=>"-","EnglishName"=>$staff_info_list[$i]['EnglishName']);
	  						}
	  					}
	  				}
	  			}
	  		}
		}
		global $CmpField, $CmpField2, $CmpField3, $CmpField4;
		if($sys_custom['StaffAttendance']['CustomizedReportSortGroupFirst']){
			$CmpField = 'DayNumber';
			$CmpField2 = 'GroupName';
			$CmpField3 = 'EnglishName';
			$CmpField4 = 'DutyStart';
		}else{
			$CmpField = 'DayNumber';
			$CmpField2 = 'EnglishName';
			$CmpField3 = 'DutyStart';
		}
		usort($ReturnArr,'StaffAttendance_USortCmp');
  		return $ReturnArr;
  	}
  	// Default $TypeOfReport == 1: Monthly; $TypeOfReport == 2: Weekly/Daily;
  	function Get_Report_Customize_Monthly_Summary_Report_Data($Params, $Year, $Month, $TypeOfReport=1)
  	{
  		global $sys_custom;
  		$CARD_STATUS_LATEEARLYLEAVE = 6;
  		$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$total_day = date("t",mktime(0,0,0,intval($Month),1,intval($Year)));
  		
  		$StartDate = explode('-',$Params["TargetStartDate"]);
  		$StartYear = $StartDate[0];
  		$StartMonth = $StartDate[1];
  		$StartDay = $StartDate[2];
  		$EndDate = explode('-',$Params["TargetEndDate"]);
  		$EndYear = $EndDate[0];
  		$EndMonth = $EndDate[1];
  		$EndDay = $EndDate[2];
  		
  		$StartDayCond = ($Year==$StartYear && $Month==$StartMonth)?" AND c.DayNumber >= '$StartDay' ":"";
  		$EndDayCond = ($Year==$EndYear && $Month==$EndMonth)?" AND c.DayNumber <= '$EndDay' ":"";
  		
  		$StatusArr = array();
  		if($Params["StatusPresent"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_NORMAL;
  		}
  		if($Params["StatusAbsent"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_ABSENT;
  		}
  		if($Params["StatusLate"]==1 && $Params["StatusEarlyLeave"]==1)
  		{
  			$StatusArr[] = $CARD_STATUS_LATEEARLYLEAVE;
  			$StatusArr[] = CARD_STATUS_LATE;
  			$StatusArr[] = CARD_STATUS_EARLYLEAVE;
  		}else
  		{
	  		if($Params["StatusLate"]==1)
	  		{
	  			$StatusArr[] = CARD_STATUS_LATE;
	  		}
	  		if($Params["StatusEarlyLeave"]==1)
	  		{
	  			$StatusArr[] = CARD_STATUS_EARLYLEAVE;
	  		}
  		}
  		if($Params["StatusOuting"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_OUTGOING;
  		}
  		if($Params["StatusHoliday"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_HOLIDAY;
  		}
  		if($Params["StatusAll"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_NORMAL;
  			$StatusArr[] = CARD_STATUS_ABSENT;
  			$StatusArr[] = $CARD_STATUS_LATEEARLYLEAVE;
  			$StatusArr[] = CARD_STATUS_LATE;
  			$StatusArr[] = CARD_STATUS_EARLYLEAVE;
  			$StatusArr[] = CARD_STATUS_OUTGOING;
  			$StatusArr[] = CARD_STATUS_HOLIDAY;
  		}
  		$AbsentSuspectedCond = "";
  		if($Params["StatusAbsentSuspected"]==1 || $Params["StatusAll"]==1)
  		{
  			$StatusArr[] = $CARD_STATUS_ABSENT_SUSPECTED;
  			$AbsentSuspectedCond = " OR (c.InSchoolStatus IS NULL AND c.OutSchoolStatus IS NULL)";
  		}
  		
  		if($Params["SelectWaived"]==1){ // with waived records
  			$waived_cond = "";
  		}else{ // without waived records 
  			//$waived_cond = " AND ((IF(c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus <> ".CARD_STATUS_NORMAL.",c.InWaived IS NULL OR c.InWaived <> 1,1)) 
			//				 OR (IF(c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus <> ".CARD_STATUS_NORMAL.",c.OutWaived IS NULL OR c.OutWaived <> 1,1))) ";
			$waived_cond = " AND (IF(c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus <> ".CARD_STATUS_NORMAL.",IF(c.InSchoolStatus='".CARD_STATUS_LATE."' AND c.OutSchoolStatus='".CARD_STATUS_EARLYLEAVE."',TRIM(c.InWaived)<>'1' OR c.InWaived IS NULL OR TRIM(c.OutWaived)<>'1' OR c.OutWaived IS NULL,c.InWaived IS NULL OR c.InWaived <> 1),1)) 
							  AND (IF(c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus <> ".CARD_STATUS_NORMAL.",IF(c.InSchoolStatus='".CARD_STATUS_LATE."' AND c.OutSchoolStatus='".CARD_STATUS_EARLYLEAVE."',TRIM(c.InWaived)<>'1' OR c.InWaived IS NULL OR TRIM(c.OutWaived)<>'1' OR c.OutWaived IS NULL,c.OutWaived IS NULL OR c.OutWaived <> 1),1)) ";
  		}
  		
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
  		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
  		
  		//$NameField = getNameFieldByLang("b.");
  		$NameField = $this->Get_Name_Field("b.");
  		$ArchiveNameField = $this->Get_Archive_Name_Field("b.");
  		$StaffSelection = sizeof($Params['SelectStaff'])>0?implode(",",$Params['SelectStaff']):"-1";
  		/*
  		if($Params["ReportType"] == "g")
  		{
	  		$GroupID = $Params["GroupID"];
	  		$sql = "SELECT
						b.UserID,
						$NameField as StaffName,
						g.GroupName
					FROM
						CARD_STAFF_ATTENDANCE_USERGROUP AS a
					    INNER JOIN INTRANET_USER AS b on a.UserID = b.UserID
						INNER JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = a.GroupID
					WHERE
						a.GroupID = '$GroupID'
					ORDER BY
						b.EnglishName ";
			$MemberList = $this->returnArray($sql);
	  	}else if($Params["ReportType"] == "p")*/
  		//{
  			//$StaffID = $Params["StaffID"];
  			$sql = "SELECT
						b.UserID,
						$NameField as StaffName,
						IF(g.GroupName IS NULL,'-',g.GroupName) as GroupName,
						b.EnglishName 
					FROM
					    INTRANET_USER AS b 
						LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP AS u on b.UserID = u.UserID
						LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID
					WHERE
						b.UserID IN (".$StaffSelection.") 
						AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) 
					 ";
			if($ReportDisplayLeftStaff){
				$sql .= "UNION 
						 SELECT
								b.UserID,
								$ArchiveNameField as StaffName,
								IF(g.GroupName IS NULL,'-',g.GroupName) as GroupName,
								b.EnglishName 
							FROM
							    INTRANET_ARCHIVE_USER AS b
								LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP AS u on b.UserID = u.UserID
								LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID
							WHERE
								b.UserID IN (".$StaffSelection.") 
								AND b.RecordType = '1' ";
			}
			$sql.= "ORDER BY EnglishName ";
			$MemberList = $this->returnArray($sql);
			//debug_r($MemberList);
  		//}
		$UserIDList = array();
		for($i=0;$i<sizeof($MemberList);$i++)
		{
			$UserIDList[$i] = $MemberList[$i]['UserID'];
		}
		
  		$sql = "SELECT
					b.UserID,
					$NameField as StaffName,
					IF(g.GroupName IS NULL, '-', g.GroupName) as GroupName,
					c.DayNumber,
					(CASE 
						WHEN c.InSchoolStatus = '".CARD_STATUS_OUTGOING."' THEN '".CARD_STATUS_OUTGOING."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' THEN '".CARD_STATUS_HOLIDAY."' ";
		if(in_array(CARD_STATUS_LATE,$StatusArr) && !in_array(CARD_STATUS_EARLYLEAVE,$StatusArr)){ // only checked late
			$sql .= " 	WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' ";
		}else if(in_array(CARD_STATUS_EARLYLEAVE,$StatusArr) && !in_array(CARD_STATUS_LATE,$StatusArr)){ // only checked early leave
			$sql .= "	WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' ";
		}else{ // checked both late and early leave
			$sql .= "	WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' AND c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' 
						WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' ";
		}
			$sql .= "	WHEN c.InSchoolStatus = '".CARD_STATUS_ABSENT."' THEN '".CARD_STATUS_ABSENT."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
						WHEN c.OutSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
						ELSE '".$CARD_STATUS_DEFAULT."' 
					END ) as RecordType,
					IF(c.SlotName IS NULL,'-',c.SlotName) as SlotName,
					e.ReasonText,
					e.ReportSymbol,
					e.ReasonID,
					h.ReasonText as ELReasonText,
					h.ReportSymbol as ELReportSymbol,
					h.ReasonID as ELReasonID,
					IF(c.DutyCount IS NULL,1,c.DutyCount) as DutyCount,
					c.InWaived,
					c.OutWaived,
					c.InSchoolStatus,
					c.OutSchoolStatus,
					IF(c.InTime IS NULL,'',DATE_FORMAT(c.InTime,'%H:%i:%s')) as InTime,
					IF(c.OutTime IS NULL,'',DATE_FORMAT(c.OutTime,'%H:%i:%s')) as OutTime,
					c.DutyStart,
					b.EnglishName   
				FROM 
					INTRANET_USER AS b
				    INNER JOIN $card_log_table_name AS c on b.UserID = c.StaffID  $waived_cond ";
		if($Params["StatusAbsentSuspected"]==1 || $Params["StatusAll"]==1){
			//$sql .= " AND (c.RecordStatus IS NOT NULL OR c.InSchoolStatus IS NULL OR c.InSchoolStatus != '-1' OR c.OutSchoolStatus IS NULL OR c.OutSchoolStatus != '-1')";
		}else{
			$sql .= "	AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1')) ";
		}
		$sql .= "	INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP AS u ON u.UserID = b.UserID
					LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID
					LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS d on d.StaffID = c.StaffID and d.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and d.RecordID = c.InAttendanceRecordID  
				  	LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS e on e.ReasonID = d.ReasonID 
					LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS f on f.StaffID = c.StaffID and f.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and f.RecordID = c.OutAttendanceRecordID
				  	LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS h on h.ReasonID = f.ReasonID 
				WHERE
					b.UserID IN (".((sizeof($UserIDList)>0)?implode(",",$UserIDList):"-1").") 
					AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) 
					AND (c.InSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-2").")
						OR c.OutSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-2").") $AbsentSuspectedCond)
					$StartDayCond
					$EndDayCond
				GROUP BY
					c.RecordID ";
  		if($ReportDisplayLeftStaff){
			$sql .= "UNION 
					SELECT
						b.UserID,
						$ArchiveNameField as StaffName,
						IF(g.GroupName IS NULL, '-', g.GroupName) as GroupName,
						c.DayNumber,
						(CASE 
							WHEN c.InSchoolStatus = '".CARD_STATUS_OUTGOING."' THEN '".CARD_STATUS_OUTGOING."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' THEN '".CARD_STATUS_HOLIDAY."' ";
			if(in_array(CARD_STATUS_LATE,$StatusArr) && !in_array(CARD_STATUS_EARLYLEAVE,$StatusArr)){ // only checked late
				$sql .= " 	WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' ";
			}else if(in_array(CARD_STATUS_EARLYLEAVE,$StatusArr) && !in_array(CARD_STATUS_LATE,$StatusArr)){ // only checked early leave
				$sql .= "	WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' ";
			}else{ // checked both late and early leave
				$sql .= "	WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' AND c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' 
							WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' ";
			}
				$sql .= "	WHEN c.InSchoolStatus = '".CARD_STATUS_ABSENT."' THEN '".CARD_STATUS_ABSENT."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
							WHEN c.OutSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
							ELSE '".$CARD_STATUS_DEFAULT."' 
						END ) as RecordType,
						IF(c.SlotName IS NULL,'-',c.SlotName) as SlotName,
						e.ReasonText,
						e.ReportSymbol,
						e.ReasonID,
						h.ReasonText as ELReasonText,
						h.ReportSymbol as ELReportSymbol,
						h.ReasonID as ELReasonID,
						IF(c.DutyCount IS NULL,1,c.DutyCount) as DutyCount,
						c.InWaived,
						c.OutWaived,
						c.InSchoolStatus,
						c.OutSchoolStatus,
						IF(c.InTime IS NULL,'',TIME_FORMAT(c.InTime,'%H:%i:%s')) as InTime,
						IF(c.OutTime IS NULL,'',TIME_FORMAT(c.OutTime,'%H:%i:%s')) as OutTime,
						c.DutyStart,
						b.EnglishName    
					FROM 
						INTRANET_ARCHIVE_USER AS b
					    INNER JOIN $card_log_table_name AS c on b.UserID = c.StaffID  $waived_cond ";
			if($Params["StatusAbsentSuspected"]==1 || $Params["StatusAll"]==1){
				//$sql .= " AND (c.RecordStatus IS NOT NULL OR c.InSchoolStatus IS NULL OR c.InSchoolStatus != '-1' OR c.OutSchoolStatus IS NULL OR c.OutSchoolStatus != '-1')";
			}else{
				$sql .= "	AND (c.RecordStatus IS NOT NULL OR (c.InSchoolStatus IS NOT NULL AND c.InSchoolStatus != '-1') OR (c.OutSchoolStatus IS NOT NULL AND c.OutSchoolStatus != '-1')) ";
			}
			$sql .= "	INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
						LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP AS u ON u.UserID = b.UserID
						LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID
						LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS d on d.StaffID = c.StaffID and d.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and d.RecordID = c.InAttendanceRecordID  
					  	LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS e on e.ReasonID = d.ReasonID 
						LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE AS f on f.StaffID = c.StaffID and f.RecordDate = CONCAT('$Year-$Month-',IF(c.DayNumber<10,CONCAT('0',c.DayNumber),c.DayNumber)) and f.RecordID = c.OutAttendanceRecordID
					  	LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON AS h on h.ReasonID = f.ReasonID 
					WHERE
						b.UserID IN (".((sizeof($UserIDList)>0)?implode(",",$UserIDList):"-1").") 
						AND b.RecordType = '1' 
						AND (c.InSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-2").")
							OR c.OutSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-2").") $AbsentSuspectedCond)
						$StartDayCond
						$EndDayCond
					GROUP BY
						c.RecordID ";
		}
		$sql.= "ORDER BY 
					EnglishName, DayNumber, DutyStart ";
  		$resultArr = $this->returnArray($sql);
  		//debug_r($resultArr);
  		// count total working days for each staff
  		$sql = "SELECT
					c.StaffID as UserID,
					COUNT(DISTINCT c.DayNumber) as Workingdays
				FROM
					$card_log_table_name as c
				WHERE
					c.StaffID IN (".((sizeof($UserIDList)>0)?implode(",",$UserIDList):"-1").") ";
		if($Params["StatusAbsentSuspected"]==1 || $Params["StatusAll"]==1){
			
		}else{
			$sql .= "AND (c.RecordStatus IS NOT NULL OR c.InSchoolStatus IS NOT NULL OR c.OutSchoolStatus IS NOT NULL) ";
		}
		$sql .= "	AND (c.RecordType IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").")
						OR c.InSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").")
						OR c.OutSchoolStatus IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").") $AbsentSuspectedCond)
					$StartDayCond 
					$EndDayCond 
				GROUP BY
					c.StaffID";
  		$workingdaysArr = $this->returnArray($sql);
  		
  		if($TypeOfReport == 1)
  		{
	  		$AssocMonthlyData=array();
	  		for($i=0;$i<sizeof($MemberList);$i++)
	  		{
	  			$TargetUserID = $MemberList[$i]['UserID'];
	  			$AssocMonthlyData[$TargetUserID]=array();
	  			$AssocMonthlyData[$TargetUserID]['UserID'] = $TargetUserID;
	  			$AssocMonthlyData[$TargetUserID]['StaffName']= $MemberList[$i]['StaffName'];
	  			$AssocMonthlyData[$TargetUserID]['EnglishName']= $MemberList[$i]['EnglishName'];
	  			$AssocMonthlyData[$TargetUserID]['GroupName']= $MemberList[$i]['GroupName'];
	  			$AssocMonthlyData[$TargetUserID]['workingdays'] = 0; //init total working days
	  			$AssocMonthlyData[$TargetUserID]['present'] = 0;// init total no. days of present
	  			$AssocMonthlyData[$TargetUserID]['late'] = 0;// init total no. days of late
	  			$AssocMonthlyData[$TargetUserID]['absent'] = 0;// init total no. days of absent
	  			$AssocMonthlyData[$TargetUserID]['earlyleave'] = 0;// init total no. days of earlyleave
	  			$AssocMonthlyData[$TargetUserID]['outing'] = 0;// init total no. days of outing
	  			$AssocMonthlyData[$TargetUserID]['holiday'] = 0; // init total no. days of holiday
	  			$AssocMonthlyData[$TargetUserID]['absentsuspected'] = 0; // init total no. of days of absent suspected
	  			for($j=1;$j<=$total_day;$j++)
				{
					$AssocMonthlyData[$TargetUserID][$j]=array();
				}
	  		}
	  		for($i=0;$i<sizeof($workingdaysArr);$i++)
	  		{
	  			$AssocMonthlyData[$workingdaysArr[$i]['UserID']]['workingdays'] = $workingdaysArr[$i]['Workingdays'];
	  		}
	  		
	  		for($i=0;$i<sizeof($resultArr);$i++)
	  		{
	  			if(in_array($resultArr[$i]['RecordType'],$StatusArr))
	  			//if(in_array($resultArr[$i]['InSchoolStatus'],$StatusArr) || in_array($resultArr[$i]['OutSchoolStatus'],$StatusArr))
	  			{
		  			$TargetUserID = $resultArr[$i]['UserID'];
		  			$DayNum = $resultArr[$i]['DayNumber'];
		  			$SlotName = $resultArr[$i]['SlotName'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$resultArr[$i]['RecordType'];
					//$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'];
					//$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['InTime']=$resultArr[$i]['InTime'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['OutTime']=$resultArr[$i]['OutTime'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['InWaived']=$resultArr[$i]['InWaived'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['OutWaived']=$resultArr[$i]['OutWaived'];
					if($resultArr[$i]['RecordType'] == $CARD_STATUS_LATEEARLYLEAVE)
					{
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'].((trim($resultArr[$i]['ReasonText'])!="" && trim($resultArr[$i]['ELReasonText'])!=="")?'; ':'').$resultArr[$i]['ELReasonText'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'].((trim($resultArr[$i]['ReportSymbol'])!="" && trim($resultArr[$i]['ELReportSymbol'])!="")?'; ':'').$resultArr[$i]['ELReportSymbol'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['ReasonID']=$resultArr[$i]['ReasonID'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['ELReasonID']=$resultArr[$i]['ELReasonID'];
					}else if($resultArr[$i]['RecordType'] == CARD_STATUS_EARLYLEAVE)
					{
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ELReasonText'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ELReportSymbol'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['ELReasonID']=$resultArr[$i]['ELReasonID'];
					}
					else
					{
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$resultArr[$i]['ReasonText'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'];
						$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['ReasonID']=$resultArr[$i]['ReasonID'];
					}
					$DutyCount = is_numeric($resultArr[$i]['DutyCount'])?$resultArr[$i]['DutyCount']:0.0;
		  			if($resultArr[$i]['RecordType'] != NULL && $resultArr[$i]['RecordType'] == CARD_STATUS_NORMAL)
		  			{
		  				$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_ABSENT)
		  			{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID]['absent']+=$DutyCount;
		  				}
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_LATE)
		  			{
		  				if($resultArr[$i]['InWaived']==1)
		  					$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
		  				else
		  					$AssocMonthlyData[$TargetUserID]['late']+=$DutyCount;
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_EARLYLEAVE)
		  			{
		  				if($resultArr[$i]['OutWaived']==1)
		  					$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
		  				else
		  					$AssocMonthlyData[$TargetUserID]['earlyleave']+=$DutyCount;
		  			}else if($resultArr[$i]['RecordType'] == $CARD_STATUS_LATEEARLYLEAVE)
		  			{
		  				$waived = false;
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
		  					$waived = true;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID]['late']+=$DutyCount;
		  				}
		  				if($resultArr[$i]['OutWaived']==1)
		  				{
		  					if($waived==false) $AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID]['earlyleave']+=$DutyCount;
		  				}
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_OUTGOING)
		  			{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID]['outing']+=$DutyCount;
		  				}
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
		  			{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					$AssocMonthlyData[$TargetUserID]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID]['holiday']+=$DutyCount;
		  				}
		  			}else{
		  				$AssocMonthlyData[$TargetUserID]['absentsuspected']+=$DutyCount;
		  			}
	  			}
	  		}
	  		if($Params["StatusAll"]==1 || $Params["StatusOuting"]==1 || $Params["StatusHoliday"]==1)
	  		{
		  		// Find Preset Leave Records after today
		  		$today = date('Y-m-d');
		  		$sql = "SELECT
							a.StaffID as UserID,
							a.SlotID,
							IF(RIGHT(a.RecordDate, 2) < '10', RIGHT(a.RecordDate, 1), RIGHT(a.RecordDate, 2)) as DayNumber,
							a.RecordType,
							IF(b.SlotName IS NULL, '-', b.SlotName) as SlotName,
							c.ReasonText,
							c.ReportSymbol,
							c.ReasonID,
							IF(b.DutyCount IS NULL, 1, b.DutyCount) as DutyCount 
						FROM
							CARD_STAFF_ATTENDANCE2_LEAVE_RECORD AS a 
							INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=a.StaffID AND a.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
							LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as b ON b.SlotID = a.SlotID  
							LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as c ON c.ReasonID = a.ReasonID
						WHERE
							a.StaffID in (".((sizeof($UserIDList) > 0)? implode(",",$UserIDList):"-1").")
							AND a.RecordType IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").")
							AND a.RecordDate LIKE '$Year-$Month-%'
							AND a.RecordDate > '$today' 
						";
		  		$LeaveArr = $this->returnArray($sql);
		  		$WorkingdaysForLeaveArr=array();
		  		for($i=0;$i<sizeof($LeaveArr);$i++)
		  		{
		  			$WorkingdaysForLeaveArr[$LeaveArr[$i]['UserID']]=array();
		  		}
		  		for($i=0;$i<sizeof($LeaveArr);$i++)
		  		{
		  			$TargetUserID = $LeaveArr[$i]['UserID'];
		  			$DayNum = $LeaveArr[$i]['DayNumber'];
		  			$SlotName = $LeaveArr[$i]['SlotName'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Status']=$LeaveArr[$i]['RecordType'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Reason']=$LeaveArr[$i]['ReasonText'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['Symbol']=$LeaveArr[$i]['ReportSymbol'];
					$AssocMonthlyData[$TargetUserID][$DayNum][$SlotName]['ReasonID']=$LeaveArr[$i]['ReasonID'];
					$DutyCount = is_numeric($LeaveArr[$i]['DutyCount'])?$LeaveArr[$i]['DutyCount']:0.0;
		  			if ($LeaveArr[$i]['SlotID'] == "")
		  			{
						// Full Day Leave
						if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
						{ // holiday
							$AssocMonthlyData[$TargetUserID]['holiday']+=1;
						}else 
						{ // outgoing
							$AssocMonthlyData[$TargetUserID]['outing']+=1;
						}
					}else
					{
						if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
						{ // holiday
							$AssocMonthlyData[$TargetUserID]['holiday']+=$DutyCount;
						}else
						{ // outgoing
							$AssocMonthlyData[$TargetUserID]['outing']+=$DutyCount;
						}
					}
					
					if(is_array($WorkingdaysForLeaveArr[$TargetUserID]) && !in_array($DayNum, $WorkingdaysForLeaveArr[$TargetUserID]))
					{
						$WorkingdaysForLeaveArr[$TargetUserID][]=$DayNum;
					}
		  		}
		  		if(sizeof($WorkingdaysForLeaveArr)>0)
		  		{
		  			foreach($WorkingdaysForLeaveArr as $Key=>$Value)
		  			{
		  				$AssocMonthlyData[$Key]['workingdays']+=sizeof($WorkingdaysForLeaveArr[$Key]);
		  			}
		  		}
	  		}
  		}else if($TypeOfReport == 2)
  		{
  			$AssocMonthlyData=array();
  			
	  		for($i=0;$i<sizeof($MemberList);$i++)
	  		{
	  			$TargetUserID = $MemberList[$i]['UserID'];
	  			$AssocMonthlyData[$TargetUserID]=array();
	  			for($j=1;$j<=$total_day;$j++)
				{
					$datekey = $Year.'-'.$Month.'-'.(($j<10)?'0'.$j:$j);
					$AssocMonthlyData[$TargetUserID][$datekey]=array();
		  			$AssocMonthlyData[$TargetUserID]['UserID'] = $TargetUserID;
		  			$AssocMonthlyData[$TargetUserID]['StaffName']= $MemberList[$i]['StaffName'];
		  			$AssocMonthlyData[$TargetUserID]['EnglishName']= $MemberList[$i]['EnglishName'];
		  			$AssocMonthlyData[$TargetUserID]['GroupName']= $MemberList[$i]['GroupName'];
		  			$AssocMonthlyData[$TargetUserID][$datekey]['workingdays'] = 0; //init total working days
		  			$AssocMonthlyData[$TargetUserID][$datekey]['present'] = 0;// init total no. days of present
		  			$AssocMonthlyData[$TargetUserID][$datekey]['late'] = 0;// init total no. days of late
		  			$AssocMonthlyData[$TargetUserID][$datekey]['absent'] = 0;// init total no. days of absent
		  			$AssocMonthlyData[$TargetUserID][$datekey]['earlyleave'] = 0;// init total no. days of earlyleave
		  			$AssocMonthlyData[$TargetUserID][$datekey]['outing'] = 0;// init total no. days of outing
		  			$AssocMonthlyData[$TargetUserID][$datekey]['holiday'] = 0; // init total no. days of holiday
		  			$AssocMonthlyData[$TargetUserID][$datekey]['absentsuspected'] = 0; // init total no. days of absent suspected
				}
	  		}
	  		
	  		for($i=0;$i<sizeof($resultArr);$i++)
	  		{
	  			if(in_array($resultArr[$i]['RecordType'],$StatusArr))
	  			//if(in_array($resultArr[$i]['InSchoolStatus'],$StatusArr) || in_array($resultArr[$i]['OutSchoolStatus'],$StatusArr))
	  			{
		  			$TargetUserID = $resultArr[$i]['UserID'];
		  			$DayNum = $resultArr[$i]['DayNumber'];
		  			$SlotName = $resultArr[$i]['SlotName'];
		  			$datekey = $Year.'-'.$Month.'-'.(($DayNum<'10')?'0'.$DayNum:$DayNum);
					$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Status']=$resultArr[$i]['RecordType'];
					$AssocMonthlyData[$TargetUserID][$datekey]['workingdays'] = 1;
					
					if($resultArr[$i]['RecordType'] == $CARD_STATUS_LATEEARLYLEAVE)
					{
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Reason']=$resultArr[$i]['ReasonText'].((trim($resultArr[$i]['ReasonText'])!="" && trim($resultArr[$i]['ELReasonText'])!=="")?'; ':'').$resultArr[$i]['ELReasonText'];
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'].((trim($resultArr[$i]['ReportSymbol'])!="" && trim($resultArr[$i]['ELReportSymbol'])!="")?'; ':'').$resultArr[$i]['ELReportSymbol'];
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['ReasonID']=$resultArr[$i]['ReasonID'];
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['ELReasonID']=$resultArr[$i]['ELReasonID'];
					}else if($resultArr[$i]['RecordType'] == CARD_STATUS_EARLYLEAVE)
					{
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Reason']=$resultArr[$i]['ELReasonText'];
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Symbol']=$resultArr[$i]['ELReportSymbol'];
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['ELReasonID']=$resultArr[$i]['ELReasonID'];
					}
					else
					{
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Reason']=$resultArr[$i]['ReasonText'];
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Symbol']=$resultArr[$i]['ReportSymbol'];
						$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['ReasonID']=$resultArr[$i]['ReasonID'];
					}
					$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['InTime'] = $resultArr[$i]['InTime'];
					$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['OutTime'] = $resultArr[$i]['OutTime'];
					$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['InWaived'] = $resultArr[$i]['InWaived'];
					$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['OutWaived'] = $resultArr[$i]['OutWaived'];
					$DutyCount = is_numeric($resultArr[$i]['DutyCount'])?$resultArr[$i]['DutyCount']:0.0;
		  			if($resultArr[$i]['RecordType'] != NULL && $resultArr[$i]['RecordType'] == CARD_STATUS_NORMAL)
		  			{
		  				$AssocMonthlyData[$TargetUserID][$datekey]['present']+=$DutyCount;
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_ABSENT)
		  			{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					$AssocMonthlyData[$TargetUserID][$datekey]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID][$datekey]['absent']+=$DutyCount;
		  				}
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_LATE)
		  			{
		  				if($resultArr[$i]['InWaived']==1)
		  					$AssocMonthlyData[$TargetUserID][$datekey]['present']+=$DutyCount;
		  				else
		  					$AssocMonthlyData[$TargetUserID][$datekey]['late']+=$DutyCount;
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_EARLYLEAVE)
		  			{
		  				if($resultArr[$i]['OutWaived']==1)
		  					$AssocMonthlyData[$TargetUserID][$datekey]['present']+=$DutyCount;
		  				else
		  					$AssocMonthlyData[$TargetUserID][$datekey]['earlyleave']+=$DutyCount;
		  			}else if($resultArr[$i]['RecordType'] == $CARD_STATUS_LATEEARLYLEAVE)
		  			{
		  				$waived = false;
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					$AssocMonthlyData[$TargetUserID][$datekey]['present']+=$DutyCount;
		  					$waived = true;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID][$datekey]['late']+=$DutyCount;
		  				}
		  				if($resultArr[$i]['OutWaived']==1)
		  				{
		  					if($waived==false) $AssocMonthlyData[$TargetUserID][$datekey]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID][$datekey]['earlyleave']+=$DutyCount;
		  				}
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_OUTGOING)
		  			{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					$AssocMonthlyData[$TargetUserID][$datekey]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID][$datekey]['outing']+=$DutyCount;
		  				}
		  			}else if($resultArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
		  			{
		  				if($resultArr[$i]['InWaived']==1)
		  				{
		  					$AssocMonthlyData[$TargetUserID][$datekey]['present']+=$DutyCount;
		  				}else
		  				{
		  					$AssocMonthlyData[$TargetUserID][$datekey]['holiday']+=$DutyCount;
		  				}
		  			}else{
		  				$AssocMonthlyData[$TargetUserID][$datekey]['absentsuspected']+=$DutyCount;
		  			}
	  			}
	  		}
	  		
	  		if($Params["StatusAll"]==1 || $Params["StatusOuting"]==1 || $Params["StatusHoliday"]==1)
	  		{
		  		// Find Preset Leave Records after today
		  		$today = date('Y-m-d');
		  		$sql = "SELECT
							a.StaffID as UserID,
							a.SlotID,
							IF(RIGHT(a.RecordDate, 2) < '10', RIGHT(a.RecordDate, 1), RIGHT(a.RecordDate, 2)) as DayNumber,
							a.RecordType,
							IF(b.SlotName IS NULL, '-', b.SlotName) as SlotName,
							c.ReasonText,
							c.ReportSymbol,
							c.ReasonID,
							IF(b.DutyCount IS NULL,1,b.DutyCount) as DutyCount 
						FROM
							CARD_STAFF_ATTENDANCE2_LEAVE_RECORD AS a 
							INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=a.StaffID AND a.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
							LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as b ON b.SlotID = a.SlotID  
							LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as c ON c.ReasonID = a.ReasonID
						WHERE
							a.StaffID in (".((sizeof($UserIDList) > 0)? implode(",",$UserIDList):"-1").")
							AND a.RecordType IN (".((sizeof($StatusArr)>0)?implode(",",$StatusArr):"-1").")
							AND a.RecordDate LIKE '$Year-$Month-%'
							AND a.RecordDate > '$today' 
						";
		  		$LeaveArr = $this->returnArray($sql);
		  		$WorkingdaysForLeaveArr=array();
		  		for($i=0;$i<sizeof($LeaveArr);$i++)
		  		{
		  			$datekey = $Year.'-'.$Month.'-'.(($DayNum<'10')?'0'.$DayNum:$DayNum);
		  			$WorkingdaysForLeaveArr[$LeaveArr[$i]['UserID']]=array();
		  		}
		  		for($i=0;$i<sizeof($LeaveArr);$i++)
		  		{
		  			$TargetUserID = $LeaveArr[$i]['UserID'];
		  			$DayNum = $LeaveArr[$i]['DayNumber'];
		  			$SlotName = $LeaveArr[$i]['SlotName'];
		  			$datekey = $Year.'-'.$Month.'-'.(($DayNum<'10')?'0'.$DayNum:$DayNum);
					$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Status']=$LeaveArr[$i]['RecordType'];
					$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Reason']=$LeaveArr[$i]['ReasonText'];
					$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['Symbol']=$LeaveArr[$i]['ReportSymbol'];
					$AssocMonthlyData[$TargetUserID][$datekey]['Slot'][$SlotName]['ReasonID']=$LeaveArr[$i]['ReasonID'];
					$DutyCount = is_numeric($LeaveArr[$i]['DutyCount'])?$LeaveArr[$i]['DutyCount']:0.0;
		  			if (trim($LeaveArr[$i]['SlotID']) == "")
		  			{
						// Full Day Leave
						if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
						{ // holiday
							$AssocMonthlyData[$TargetUserID][$datekey]['holiday']+=1;
						}else 
						{ // outgoing
							$AssocMonthlyData[$TargetUserID][$datekey]['outing']+=1;
						}
					}else
					{
						if ($LeaveArr[$i]['RecordType'] == CARD_STATUS_HOLIDAY)
						{ // holiday
							$AssocMonthlyData[$TargetUserID][$datekey]['holiday']+=$DutyCount;
						}else
						{ // outgoing
							$AssocMonthlyData[$TargetUserID][$datekey]['outing']+=$DutyCount;
						}
					}
					if(is_array($WorkingdaysForLeaveArr[$TargetUserID]) && !in_array($datekey, $WorkingdaysForLeaveArr[$TargetUserID]))
					{
						$WorkingdaysForLeaveArr[$TargetUserID][]=$datekey;
					}
		  		}
		  		if(sizeof($WorkingdaysForLeaveArr)>0)
		  		{
		  			foreach($WorkingdaysForLeaveArr as $Key=>$Value)
		  			{
	  					for($j=0;$j<sizeof($WorkingdaysForLeaveArr[$Key]);$j++)
	  					{
	  						$AssocMonthlyData[$Key][$WorkingdaysForLeaveArr[$Key][$j]]['workingdays']+=1;
	  					}
		  			}
		  		}
	  		}
	  		
	  		return $AssocMonthlyData;
  		}
  		
  		//debug_r($LeaveArr);
  		//echo "<PRE>";print_r($AssocMonthlyData);echo "</PRE>";
  		$ReturnArr = array();
  		if(sizeof($AssocMonthlyData)>0)
  		{
  			$OrderedStaffList = $this->Get_Staff_Info($Params['SelectStaff'],'',$recordstatus,$ReportDisplayLeftStaff);
  			if($sys_custom['StaffAttendance']['CustomizedReportSortGroupFirst']) sortByColumn2($OrderedStaffList,'GroupName',0,0,'EnglishName');
  			for($n=0;$n<count($OrderedStaffList);$n++) {
  				if(isset($AssocMonthlyData[$OrderedStaffList[$n]['UserID']]))
		  		//foreach($AssocMonthlyData as $Key => $Value)
		  		{
		  			$Value = $AssocMonthlyData[$OrderedStaffList[$n]['UserID']];
		  			$ReturnArr[]=$Value;
		  		}
  			}
  		}
  		//echo "<PRE>";print_r($resultArr);echo "</PRE>";
  		return $ReturnArr;
  	}
  	
  	function Get_Report_Working_Hour_Data($Params, $Year, $Month)
  	{
  		$CARD_STATUS_LATEEARLYLEAVE = 6;
  		$CARD_STATUS_ABSENT_SUSPECTED = 7;
  		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
  		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($Year, $Month);
  		$total_day = date("t",mktime(0,0,0,intval($Month),1,intval($Year)));
  		
  		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
  		//$NameField = getNameFieldByLang("b.");
  		$NameField = $this->Get_Name_Field("b.");
  		$ArchiveNameField = $this->Get_Archive_Name_Field("b.");
  		
  		$StartDate = explode('-',$Params["StartDate"]);
  		$StartYear = $StartDate[0];
  		$StartMonth = $StartDate[1];
  		$StartDay = $StartDate[2];
  		$EndDate = explode('-',$Params["EndDate"]);
  		$EndYear = $EndDate[0];
  		$EndMonth = $EndDate[1];
  		$EndDay = $EndDate[2];
  		
  		$StatusArr = array();
  		if($Params["StatusPresent"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_NORMAL;
  		}
  		if($Params["StatusAbsent"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_ABSENT;
  		}
  		if($Params["StatusLate"]==1 && $Params["StatusEarlyLeave"]==1)
  		{
  			$StatusArr[] = $CARD_STATUS_LATEEARLYLEAVE;
  			$StatusArr[] = CARD_STATUS_LATE;
  			$StatusArr[] = CARD_STATUS_EARLYLEAVE;
  		}else
  		{
	  		if($Params["StatusLate"]==1)
	  		{
	  			$StatusArr[] = CARD_STATUS_LATE;
	  		}
	  		if($Params["StatusEarlyLeave"]==1)
	  		{
	  			$StatusArr[] = CARD_STATUS_EARLYLEAVE;
	  		}
  		}
  		if($Params["StatusOuting"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_OUTGOING;
  		}
  		if($Params["StatusHoliday"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_HOLIDAY;
  		}
  		if($Params["StatusAll"]==1)
  		{
  			$StatusArr[] = CARD_STATUS_NORMAL;
  			$StatusArr[] = CARD_STATUS_ABSENT;
  			$StatusArr[] = $CARD_STATUS_LATEEARLYLEAVE;
  			$StatusArr[] = CARD_STATUS_LATE;
  			$StatusArr[] = CARD_STATUS_EARLYLEAVE;
  			$StatusArr[] = CARD_STATUS_OUTGOING;
  			$StatusArr[] = CARD_STATUS_HOLIDAY;
  		}
  		$AbsentSuspectedCond = "";
  		if($Params["StatusAbsentSuspected"]==1 || $Params["StatusAll"]==1){
  			$StatusArr[] = $CARD_STATUS_ABSENT_SUSPECTED;
  			$AbsentSuspectedCond = " OR (c.InSchoolStatus IS NULL AND c.OutSchoolStatus IS NULL) ";
  		}
  		
  		if($Params['CalculationType']==1){ // by tap card time
  			$InOutTimeCond = " AND ((c.InTime IS NOT NULL AND c.InTime <> '') OR (c.OutTime IS NOT NULL AND c.OutTime <> '')) ";
  		}else if($Params['CalculationType']==2){ // by time slot
 			
  		}else{ // by both tap card time and time slot
  			
  		}
  		
  		$StatusCond = sizeof($StatusArr)>0?implode(",",$StatusArr):"-1";
  		$StartDayCond = ($Year==$StartYear && $Month==$StartMonth)?" AND c.DayNumber >= '$StartDay' ":"";
  		$EndDayCond = ($Year==$EndYear && $Month==$EndMonth)?" AND c.DayNumber <= '$EndDay' ":"";
  		
  		$StaffSelection = sizeof($Params['SelectStaff'])>0?implode(",",$Params['SelectStaff']):"-1";
  		
  		$sql = "SELECT
					b.UserID,
				  	$NameField as StaffName,
					IF(g.GroupName IS NULL,'-',g.GroupName) as GroupName,
					DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') as RecordDate,
				  	c.SlotName,
					(CASE 
						WHEN c.InSchoolStatus = '".CARD_STATUS_OUTGOING."' THEN '".CARD_STATUS_OUTGOING."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' THEN '".CARD_STATUS_HOLIDAY."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' AND c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' 
						WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_ABSENT."' THEN '".CARD_STATUS_ABSENT."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
						WHEN c.OutSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
						ELSE '".$CARD_STATUS_DEFAULT."'
					END ) as Status,
					c.DutyStart,
					c.DutyEnd,
					c.InTime,
					c.OutTime,
					b.EnglishName,
					c.InWaived,
					c.OutWaived,
					r1.ReasonText as InReason, 
					r2.ReasonText as OutReason 
				 FROM
					INTRANET_USER as b
					INNER JOIN $card_log_table_name as c ON c.StaffID = b.UserID 
					INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
					LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as u ON u.UserID = b.UserID
					LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID 
					LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p1 ON p1.RecordID=c.InAttendanceRecordID AND p1.StaffID=c.StaffID 
					LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p2 ON p2.RecordID=c.OutAttendanceRecordID AND p2.StaffID=c.StaffID 
					LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r1 On r1.ReasonID=p1.ReasonID 
					LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r2 On r2.ReasonID=p2.ReasonID 
				 WHERE 
					b.UserID IN (".$StaffSelection.") 
					AND b.RecordType = '1' AND b.RecordStatus IN ($recordstatus) 
					AND c.DutyStart IS NOT NULL AND c.DutyStart <> '' 
					AND c.DutyEnd IS NOT NULL AND c.DutyEnd <> '' 
					AND (c.RecordType IN (".$StatusCond.")
							OR c.InSchoolStatus IN (".$StatusCond.")
							OR c.OutSchoolStatus IN (".$StatusCond.") $AbsentSuspectedCond)
					$InOutTimeCond 
					$StartDayCond 
					$EndDayCond 
				 GROUP BY 
					c.RecordID ";		
		if($ReportDisplayLeftStaff){
			$sql .= "UNION (
					SELECT
						b.UserID,
					  	$ArchiveNameField as StaffName,
						IF(g.GroupName IS NULL,'-',g.GroupName) as GroupName,
						DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') as RecordDate,
					  	c.SlotName,
						(CASE 
							WHEN c.InSchoolStatus = '".CARD_STATUS_OUTGOING."' THEN '".CARD_STATUS_OUTGOING."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' THEN '".CARD_STATUS_HOLIDAY."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' AND c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' THEN '".CARD_STATUS_LATE."' 
							WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' THEN '".CARD_STATUS_EARLYLEAVE."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_ABSENT."' THEN '".CARD_STATUS_ABSENT."' 
							WHEN c.InSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
							WHEN c.OutSchoolStatus = '".CARD_STATUS_NORMAL."' THEN '".CARD_STATUS_NORMAL."' 
							ELSE '".$CARD_STATUS_DEFAULT."'
						END ) as Status,
						c.DutyStart,
						c.DutyEnd,
						c.InTime,
						c.OutTime,
						b.EnglishName,
						c.InWaived,
						c.OutWaived,
						r1.ReasonText as InReason, 
						r2.ReasonText as OutReason 
					 FROM 
						INTRANET_ARCHIVE_USER as b
						INNER JOIN $card_log_table_name as c ON c.StaffID = b.UserID 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=b.UserID AND DATE_FORMAT(CONCAT('".$Year."-".$Month."-',c.DayNumber),'%Y-%m-%d') BETWEEN wp.PeriodStart AND wp.PeriodEnd 
						LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as u ON u.UserID = b.UserID
						LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = u.GroupID 
						LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p1 ON p1.RecordID=c.InAttendanceRecordID AND p1.StaffID=c.StaffID 
						LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p2 ON p2.RecordID=c.OutAttendanceRecordID AND p2.StaffID=c.StaffID 
						LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r1 On r1.ReasonID=p1.ReasonID 
						LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r2 On r2.ReasonID=p2.ReasonID 
					 WHERE
						b.UserID IN (".$StaffSelection.") 
						AND b.RecordType = '1' 
						AND c.DutyStart IS NOT NULL AND c.DutyStart <> '' 
						AND c.DutyEnd IS NOT NULL AND c.DutyEnd <> '' 
						AND (c.RecordType IN (".$StatusCond.")
								OR c.InSchoolStatus IN (".$StatusCond.")
								OR c.OutSchoolStatus IN (".$StatusCond.") $AbsentSuspectedCond)
						$InOutTimeCond 
						$StartDayCond 
						$EndDayCond 
					 GROUP BY 
						c.RecordID) ";
		}
		$sql .= "ORDER BY
					EnglishName, RecordDate, DutyStart ";	
		//debug_pr($sql);
		$resultArr = $this->returnResultSet($sql);
		
		return $resultArr;
  	}
  	
  	function Get_Report_OT_Data($SelectStaff, $SelectDate, $OtOrRedeem="OT", $StartDate="", $EndDate="", $Keyword="", $SortFields=array())
	{
		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
		
		$DateField = ($OtOrRedeem=="OT")?"RecordDate":"RedeemDate";
		
		if($SelectDate == "2") // Yesterday
		{
			$timestamp = time() - (24*60*60);
			$yesterday = date('Y-m-d', $timestamp);
			$cond = " AND b.$DateField = '$yesterday' ";
		}else if($SelectDate == "3") // this month
		{
			$year = date('Y');
			$month = date('m');
			$cond = " AND b.$DateField LIKE '$year-$month-%' ";
		}else if($SelectDate == "4") // from StartDate to EndDate
		{
			if($StartDate == "" || $StartDate == "undefined" || $StartDate == null)
				$StartDate = date('Y-m-d');
			if($EndDate == "" || $EndDate == "undefined" || $EndDate == null)
				$EndDate = date('Y-m-d');
			$cond = " AND b.$DateField >= '$StartDate' AND b.$DateField <= '$EndDate' ";
		}else // today or not select
		{
  		$today = date('Y-m-d');
  		$cond = " AND b.$DateField = '$today' ";
		}
		
		if($SelectStaff == "-1") // All Staff
		{
			$StaffList = $this->Get_Staff_Info("","",$recordstatus,$ReportDisplayLeftStaff);
		}else if($SelectStaff == "-2") // All Individual
		{
			$StaffList = $this->Get_Aval_User_List("",$recordstatus,$ReportDisplayLeftStaff);
		}else // Specific group
		{
			$StaffList = $this->Get_Group_Member_List($SelectStaff,"",$recordstatus,$ReportDisplayLeftStaff);
		}
		$IDList = array();
		for($i=0;$i<sizeof($StaffList);$i++)
		{
			$IDList[$i] = $StaffList[$i]['UserID'];
		}
		
		//$NameField = getNameFieldByLang("a.");
		$NameField = $this->Get_Name_Field("a.");
		$ArchiveNameField = $this->Get_Archive_Name_Field("a.");
		
		if($OtOrRedeem=="OT") //Get OT Records
		{
			$SortField = trim($SortFields["SortField"])==""?"RecordDate":$SortFields["SortField"];
			$SortOrder = trim($SortFields["SortOrder"])==""?"ASC":$SortFields["SortOrder"];
			$sql = "SELECT
					a.UserID,
					$NameField as StaffName,
					IF(d.GroupName IS NULL, '-',d.GroupName) as GroupName,
					b.RecordDate as RecordDate,
					b.OTmins as OTmins,
					b.StartTime as StartTime,
					b.EndTime as EndTime,
					b.Waived,
					b.DateModified 
				FROM
					INTRANET_USER as a
					INNER JOIN CARD_STAFF_ATTENDANCE2_OT_RECORD as b ON b.StaffID = a.UserID
					LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as c ON c.UserID = a.UserID
					LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as d ON c.GroupID = d.GroupID
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus IN ($recordstatus)
					AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR b.RecordDate LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%') 
					AND a.UserID IN (".((sizeof($IDList)>0)?implode(",",$IDList):"-1").")
					$cond ";
			if($ReportDisplayLeftStaff) {
				$sql .= "UNION (
						SELECT
							a.UserID,
							$ArchiveNameField as StaffName,
							IF(d.GroupName IS NULL, '-',d.GroupName) as GroupName,
							b.RecordDate as RecordDate,
							b.OTmins as OTmins,
							b.StartTime as StartTime,
							b.EndTime as EndTime,
							b.Waived,
							b.DateModified
						FROM
							INTRANET_ARCHIVE_USER as a
							INNER JOIN CARD_STAFF_ATTENDANCE2_OT_RECORD as b ON b.StaffID = a.UserID
							LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as c ON c.UserID = a.UserID
							LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as d ON c.GroupID = d.GroupID
						WHERE
							a.RecordType='1' 
							AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								 OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								 OR b.RecordDate LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%') 
							AND a.UserID IN (".((sizeof($IDList)>0)?implode(",",$IDList):"-1").")
							$cond )";
			}
			$sql.="ORDER BY $SortField $SortOrder ";
		}else // Get Redeem Records
		{
			$SortField = trim($SortFields["SortField"])==""?"RedeemDate":$SortFields["SortField"];
			$SortOrder = trim($SortFields["SortOrder"])==""?"ASC":$SortFields["SortOrder"];
			$sql = "SELECT
					a.UserID,
					$NameField as StaffName,
					IF(d.GroupName IS NULL, '-',d.GroupName) as GroupName,
					b.RedeemDate as RedeemDate,
					b.MinsRedeemed as MinsRedeemed,
					b.Remark as Remark,
					b.DateModified
				FROM
					INTRANET_USER as a
					INNER JOIN CARD_STAFF_ATTENDANCE2_OT_REDEEM as b ON b.StaffID = a.UserID
					LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as c ON c.UserID = a.UserID
					LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as d ON c.GroupID = d.GroupID
				WHERE
					a.RecordType='1'
				  	AND a.RecordStatus IN ($recordstatus)
					AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
						 OR b.Remark LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
						 OR b.RedeemDate LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%')
					AND a.UserID IN (".((sizeof($IDList)>0)?implode(",",$IDList):"-1").")
					$cond ";
			if($ReportDisplayLeftStaff) {
				$sql .= "UNION (
						SELECT
							a.UserID,
							$ArchiveNameField as StaffName,
							IF(d.GroupName IS NULL, '-',d.GroupName) as GroupName,
							b.RedeemDate as RedeemDate,
							b.MinsRedeemed as MinsRedeemed,
							b.Remark as Remark,
							b.DateModified
						FROM
							INTRANET_ARCHIVE_USER as a
							INNER JOIN CARD_STAFF_ATTENDANCE2_OT_REDEEM as b ON b.StaffID = a.UserID
							LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as c ON c.UserID = a.UserID
							LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as d ON c.GroupID = d.GroupID
						WHERE
							a.RecordType='1'
							AND (a.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								 OR a.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES))."%' 
								 OR b.Remark LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%' 
								 OR b.RedeemDate LIKE '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%')
							AND a.UserID IN (".((sizeof($IDList)>0)?implode(",",$IDList):"-1").")
							$cond )";
			}
			$sql.="ORDER BY $SortField $SortOrder ";
		}
		
		return $this->returnArray($sql);
	}
	
	function Get_User_Setting($ParUserID,$ParSettingName,$ParDisplayName='',$ParSettingID='')
	{
		$sql = "SELECT SettingID,SettingName,SettingValue,DisplayName,UserID,DateModified FROM CARD_STAFF_ATTENDANCE3_USER_SETTING WHERE UserID = '".$ParUserID."' AND SettingName = '$ParSettingName' ";
		if(trim($ParDisplayName)!=''){
			// if DisplayName is specified, can use to check for existence of duplicated setting items
			$sql .= " AND DisplayName = '".$this->Get_Safe_Sql_Query($ParDisplayName)."' ";
		}
		if(trim($ParSettingID)!=''){
			// if SettingID is specified, only get one record
			$sql .= " AND SettingID = '$ParSettingID' ";
		}
		$sql .= " ORDER BY DateModified";
		
		return $this->returnArray($sql);
	}
	
	function Set_User_Setting($ParUserID,$ParSettingName,$ParSettingValue,$ParDisplayName,$ParSettingID='')
	{
		if(trim($ParSettingID)!=''){
			// update
			$sql = "UPDATE CARD_STAFF_ATTENDANCE3_USER_SETTING 
					SET SettingValue = '$ParSettingValue', DisplayName = '".$this->Get_Safe_Sql_Query($ParDisplayName)."', DateModified = NOW() 
					WHERE SettingID = '$ParSettingID' AND UserID = '".$ParUserID."' AND SettingName = '$ParSettingName' ";
			$result = $this->db_db_query($sql);
		}else{
			// insert
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE3_USER_SETTING (UserID, SettingName, SettingValue, DisplayName, DateModified)
					VALUES ('$ParUserID','$ParSettingName','$ParSettingValue','".$this->Get_Safe_Sql_Query($ParDisplayName)."',NOW())";
			$result = $this->db_db_query($sql);
		}
		
		return $result;
	}
	
	function Delete_User_Setting($ParUserID,$ParSettingName,$ParSettingID='')
	{
		$sql = "DELETE FROM CARD_STAFF_ATTENDANCE3_USER_SETTING 
				WHERE UserID = '".$ParUserID."' AND SettingName = '$ParSettingName' ";
		// if SettingID is specified, only delete 1 record, else delete all setting for the user
		if(trim($ParSettingID)!=''){
			$sql .= " AND SettingID = '$ParSettingID' ";
		}
		$result = $this->db_db_query($sql);
		return $result;
	}
	
	function Import_Staff_Working_Periods($Data)
	{
		global $Lang;
		$Error = array();
		$Result = array();
		if(!is_array($Data))
		{
			return $Error;
		}
		
		$sql = 'DROP TABLE TEMP_IMPORT_STAFF_WORKING_PERIOD';
		$this->db_db_query($sql);
		$sql = 'CREATE TABLE TEMP_IMPORT_STAFF_WORKING_PERIOD (
							UserID int(11) default NULL,
						  PeriodStart date NOT NULL default \'1970-01-01\',
						  PeriodEnd date NOT NULL default \'2099-12-31\' 
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					 ';
		$this->db_db_query($sql);
		
		//$this->Start_Trans();
		$NameField = getNameFieldByLang("a.");
		$values_array = array();
		for($i=0;$i<sizeof($Data);$i++)
		{
			list($StaffName, $PeriodStart, $PeriodEnd) = $Data[$i];
			$StaffName = trim($StaffName);
			$PeriodStart = trim($PeriodStart);
			$PeriodEnd = trim($PeriodEnd);
			if($StaffName=="")
			{
				$Error[]=array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>"*".$Lang['StaffAttendance']['StaffNotExists']);
				continue;
			}else
			{
				$StaffNameStr = "'$StaffName'";
			}
			if(trim($PeriodStart)=="" && trim($PeriodEnd)=="")
			{
				$Error[]=array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['WorkingPeriodNullWarning']);
				continue;
			}
			
			//if($PeriodStart=="")
			//{
			//	$Error[]=array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>"PeriodStart is empty");
			//	continue;
			//}
			//else
			if(trim($PeriodStart)!="")
			{
				if(preg_match('/^\d\d\d\d-\d\d-\d\d$/', $PeriodStart)==0)
				{
					$Error[]=array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>"*".$Lang['General']['InvalidDateFormat']);
					continue;
				}
			}
			//if($PeriodEnd=="")
			//{
			//	$Error[]=array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>"PeriodEnd is empty");
			//	continue;
			//}
			//else
			if(trim($PeriodEnd)!="")
			{
				if(preg_match('/^\d\d\d\d-\d\d-\d\d$/', $PeriodEnd)==0)
				{
					$Error[]=array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>"*".$Lang['General']['InvalidDateFormat']);
					continue;
				}
			}
			
			if(trim($PeriodStart)!="" && trim($PeriodEnd)!="" && strtotime($PeriodStart) > strtotime($PeriodEnd))
			{
				$Error[]=array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['WorkingPeriodEndDateLesserThenStartDateWarning']);
				continue;
			}
			
			$sql = "SELECT
						a.UserID
					FROM
						INTRANET_USER as a
					WHERE
						a.RecordType = '1'
				  		AND a.RecordStatus = '1'
						AND a.UserLogin = '".$this->Get_Safe_Sql_Query($StaffName)."' ";
			$UserInfo = $this->returnArray($sql);
			if(sizeof($UserInfo)>0)
			{
				$TargetUserID = $UserInfo[0]['UserID'];
				//if(trim($PeriodStart)=='') $PeriodStart = "1970-01-01";
				//if(trim($PeriodEnd)=='') $PeriodEnd = "2099-12-31";
				// only check overlap when both period start and period end are not empty
				if($PeriodStart != '' && $PeriodEnd != ''){
					$sql = "SELECT
								WorkingPeriodID
							FROM
								CARD_STAFF_ATTENDANCE3_WORKING_PERIOD
							WHERE
								UserID = '$TargetUserID' 
								AND ( ('".$PeriodStart."' BETWEEN PeriodStart AND PeriodEnd) 
								OR ('".$PeriodEnd."' BETWEEN PeriodStart AND PeriodEnd)
								OR (PeriodStart BETWEEN '".$PeriodStart."' AND '".$PeriodEnd."') 
								OR (PeriodEnd BETWEEN '".$PeriodStart."' AND '".$PeriodEnd."') )";
					$WorkingPeriods = $this->returnVector($sql);
					$sql = "SELECT 
								UserID
							FROM 
								TEMP_IMPORT_STAFF_WORKING_PERIOD
							WHERE 
								UserID = '$TargetUserID' 
								AND ( ('".$PeriodStart."' BETWEEN PeriodStart AND PeriodEnd) 
								OR ('".$PeriodEnd."' BETWEEN PeriodStart AND PeriodEnd)
								OR (PeriodStart BETWEEN '".$PeriodStart."' AND '".$PeriodEnd."') 
								OR (PeriodEnd BETWEEN '".$PeriodStart."' AND '".$PeriodEnd."') )";
					$AlreadyImportedPeriods = $this->returnVector($sql);
					if(sizeof($WorkingPeriods)>0 || sizeof($AlreadyImportedPeriods)>0)
					{
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['WorkingPeriodOverlapWarning']);
						continue;
					}else
					{
						//$values_array[] = "('$TargetUserID','$PeriodStart', '$PeriodEnd')";
						$sql = "INSERT INTO TEMP_IMPORT_STAFF_WORKING_PERIOD (UserID, PeriodStart, PeriodEnd)
								VALUES ('$TargetUserID','$PeriodStart', '$PeriodEnd') ";
						$this->db_db_query($sql);
					}
				}else if($PeriodEnd == ''){
					// only have period start, check overlap but ignore default period end
					/*
					$sql = "SELECT WorkingPeriodID 
							FROM CARD_STAFF_ATTENDANCE3_WORKING_PERIOD 
							WHERE UserID='".$TargetUserID."' AND (
									(('".$PeriodStart."' BETWEEN PeriodStart AND PeriodEnd) AND PeriodEnd <> '2099-12-31') 
									OR ('".$PeriodStart."' >= PeriodStart AND PeriodEnd <> '2099-12-31')
								) ";
					*/
					$sql = "SELECT WorkingPeriodID 
							FROM CARD_STAFF_ATTENDANCE3_WORKING_PERIOD 
							WHERE UserID='".$TargetUserID."' AND (
									( ('".$PeriodStart."' BETWEEN PeriodStart AND PeriodEnd) 
									OR ('2099-12-31' BETWEEN PeriodStart AND PeriodEnd) 
									OR (PeriodStart BETWEEN '$PeriodStart' AND '2099-12-31') 
									OR (PeriodEnd BETWEEN '$PeriodStart' AND '2099-12-31') )
								) ";
					$WorkingPeriods = $this->returnVector($sql);
					/*
					$sql = "SELECT UserID
							FROM TEMP_IMPORT_STAFF_WORKING_PERIOD 
							WHERE UserID='".$TargetUserID."' AND (
									(('".$PeriodStart."' BETWEEN PeriodStart AND PeriodEnd) AND PeriodEnd <> '2099-12-31') 
									OR ('".$PeriodStart."' >= PeriodStart AND PeriodEnd <> '2099-12-31') 
								)";
					*/
					$sql = "SELECT UserID
							FROM TEMP_IMPORT_STAFF_WORKING_PERIOD 
							WHERE UserID='".$TargetUserID."' AND (
									( ('".$PeriodStart."' BETWEEN PeriodStart AND PeriodEnd) 
									OR ('2099-12-31' BETWEEN PeriodStart AND PeriodEnd) 
									OR (PeriodStart BETWEEN '$PeriodStart' AND '2099-12-31') 
									OR (PeriodEnd BETWEEN '$PeriodStart' AND '2099-12-31') )
								)";
					$AlreadyImportedPeriods = $this->returnVector($sql);
					if(sizeof($WorkingPeriods)>0 || sizeof($AlreadyImportedPeriods)>0)
					{
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['WorkingPeriodOverlapWarning']);
						continue;
					}else
					{
						$sql = "INSERT INTO TEMP_IMPORT_STAFF_WORKING_PERIOD (UserID, PeriodStart, PeriodEnd)
								VALUES ('$TargetUserID','$PeriodStart', '2099-12-31') ";
						$this->db_db_query($sql);
					}
				}else if($PeriodStart == ''){
					// only have period end, check overlap but ignore default period start
					/*
					$sql = "SELECT WorkingPeriodID 
							FROM CARD_STAFF_ATTENDANCE3_WORKING_PERIOD 
							WHERE UserID='".$TargetUserID."' AND (
									(('".$PeriodEnd."' BETWEEN PeriodStart AND PeriodEnd) AND PeriodStart <> '1970-01-01') 
									OR ('".$PeriodEnd."' <= PeriodStart AND PeriodStart <> '1970-01-01')
								) ";
					*/
					$sql = "SELECT WorkingPeriodID 
							FROM CARD_STAFF_ATTENDANCE3_WORKING_PERIOD 
							WHERE UserID='".$TargetUserID."' AND (
									( ('1970-01-01' BETWEEN PeriodStart AND PeriodEnd) 
									OR ('".$PeriodEnd."' BETWEEN PeriodStart AND PeriodEnd) 
									OR (PeriodStart BETWEEN '1970-01-01' AND '$PeriodEnd')
									OR (PeriodEnd BETWEEN '1970-01-01' AND '$PeriodEnd') )
								) ";
					$WorkingPeriods = $this->returnVector($sql);
					/*
					$sql = "SELECT UserID
							FROM TEMP_IMPORT_STAFF_WORKING_PERIOD 
							WHERE UserID='".$TargetUserID."' AND (
									(('".$PeriodEnd."' BETWEEN PeriodStart AND PeriodEnd) AND PeriodStart <> '1970-01-01') 
									OR ('".$PeriodEnd."' <= PeriodStart AND PeriodStart <> '1970-01-01') 
								)";
					*/
					$sql = "SELECT UserID
							FROM TEMP_IMPORT_STAFF_WORKING_PERIOD 
							WHERE UserID='".$TargetUserID."' AND (
									( ('1970-01-01' BETWEEN PeriodStart AND PeriodEnd) 
									OR ('".$PeriodEnd."' BETWEEN PeriodStart AND PeriodEnd) 
									OR (PeriodStart BETWEEN '1970-01-01' AND '$PeriodEnd')
									OR (PeriodEnd BETWEEN '1970-01-01' AND '$PeriodEnd') )
								)";
					$AlreadyImportedPeriods = $this->returnVector($sql);
					if(sizeof($WorkingPeriods)>0 || sizeof($AlreadyImportedPeriods)>0)
					{
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['WorkingPeriodOverlapWarning']);
						continue;
					}else
					{
						$sql = "INSERT INTO TEMP_IMPORT_STAFF_WORKING_PERIOD (UserID, PeriodStart, PeriodEnd)
								VALUES ('$TargetUserID','1970-01-01', '$PeriodEnd') ";
						$this->db_db_query($sql);
					}
				}
			}else
			{
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>"*".$Lang['StaffAttendance']['StaffNotExists']);
				continue;
			}
		}
		/*
		if(sizeof($values_array)>0)
		{
			$values = implode(",",$values_array);
			$sql = "INSERT INTO TEMP_IMPORT_STAFF_WORKING_PERIOD (UserID, PeriodStart, PeriodEnd)
					VALUES $values ";
			if($this->db_db_query($sql))
			{	
				$this->Commit_Trans();
			}else
			{
				$this->RollBack_Trans();
			}
		}
		*/
		return $Error;
	}
	
	function Finalize_Import_Staff_Working_Periods()
	{
		$sql = "SELECT UserID, PeriodStart, PeriodEnd 
				FROM TEMP_IMPORT_STAFF_WORKING_PERIOD";
		$TempResult = $this->returnArray($sql);
		$values_array=array();
		for ($i=0; $i< sizeof($TempResult); $i++)
		{
			if($TempResult[$i]['PeriodEnd'] == '2099-12-31'){
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE3_WORKING_PERIOD WHERE UserID='".$TempResult[$i]['UserID']."' AND PeriodEnd='2099-12-31' ";
				$this->db_db_query($sql);
			}
			if($TempResult[$i]['PeriodStart'] == '1970-01-01'){
				$sql = "DELETE FROM CARD_STAFF_ATTENDANCE3_WORKING_PERIOD WHERE UserID='".$TempResult[$i]['UserID']."' AND PeriodStart='1970-01-01' ";
				$this->db_db_query($sql);
			}
			$values_array[] = "('".$TempResult[$i]['UserID']."','".$TempResult[$i]['PeriodStart']."', '".$TempResult[$i]['PeriodEnd']."', NOW(), '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
		}
		if(sizeof($values_array)>0)
		{
			$values = implode(",",$values_array);
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE3_WORKING_PERIOD (UserID, PeriodStart, PeriodEnd, DateInput, InputBy, DateModify, ModifyBy)
					VALUES $values ";
			$this->db_db_query($sql);
		}
		return sizeof($TempResult);
	}
	/*
	function Import_Staff_Working_Periods($Data)
	{
		global $Lang;
		$results = array();
		if(!is_array($Data))
		{
			$results[] = "No data";
			return $results;
		}
		
		$this->Start_Trans();
		$NameField = getNameFieldByLang("a.");
		$values_array = array();
		for($i=0;$i<sizeof($Data);$i++)
		{
			list($StaffName, $PeriodStart, $PeriodEnd) = $Data[$i];
			$StaffName = trim($StaffName);
			$PeriodStart = trim($PeriodStart);
			$PeriodEnd = trim($PeriodEnd);
			if($StaffName=="")
			{
				$results[]="Empty Staff Name";
				continue;
				//break;
			}else
			{
				$StaffNameStr = "'$StaffName'";
			}
			if($PeriodStart=="")
			{
				$results[]="PeriodStart is empty";
				continue;
				//break;
			}
			else
			{
				if(preg_match('/^\d\d\d\d-\d\d-\d\d$/', $PeriodStart)==0)
				{
					$results[]="PeriodStart is not in format YYYY-MM-DD";
					continue;
					//break;
				}
			}
			if($PeriodEnd=="")
			{
				$results[]="PeriodEnd is empty";
				continue;
				//break;
			}
			else
			{
				if(preg_match('/^\d\d\d\d-\d\d-\d\d$/', $PeriodEnd)==0)
				{
					$results[]="PeriodEnd is not in format YYYY-MM-DD";
					continue;
					//break;
				}
				if(strtotime($PeriodStart) > strtotime($PeriodEnd))
				{
					$results[]="PeriodStart is greater than PeriodEnd";
					continue;
					//break;
				}
			}
			
			$sql = "SELECT
						a.UserID
					FROM
						INTRANET_USER as a
					WHERE
						a.RecordType = '1'
				  		AND a.RecordStatus = '1'
						AND a.UserLogin = '".$this->Get_Safe_Sql_Query($StaffName)."' ";
			$UserInfo = $this->returnArray($sql);
			if(sizeof($UserInfo)>0)
			{
				$TargetUserID = $UserInfo[0]['UserID'];
				$sql = "SELECT
							WorkingPeriodID
						FROM
							CARD_STAFF_ATTENDANCE3_WORKING_PERIOD
						WHERE
							UserID = '$TargetUserID' 
							AND ('".$PeriodStart."' BETWEEN PeriodStart AND PeriodEnd 
							OR '".$PeriodEnd."' BETWEEN PeriodStart AND PeriodEnd )";
				$WorkingPeriods = $this->returnArray($sql);
				if(sizeof($WorkingPeriods)>0)
				{
					$results[] = "Working Period Overlaps";
				}else
				{
					$values_array[] = "('$TargetUserID','$PeriodStart', '$PeriodEnd', NOW(), '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
				}
			}else
			{
				$results[] = "Staff does not exist";
				continue;
				//break;
			}
		}
		if(sizeof($results)==0)
		{
			$values = implode(",",$values_array);
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE3_WORKING_PERIOD (UserID, PeriodStart, PeriodEnd, DateInput, InputBy, DateModify, ModifyBy)
					VALUES $values ";
			if($this->db_db_query($sql))
			{	
				$this->Commit_Trans();
			}else
			{
				$this->RollBack_Trans();
				$results[] = "Import failed";
			}
		}
		else
		{
			$this->RollBack_Trans();
		}
		return $results;
	}
	*/
	
	function Get_Entry_Log($Year,$Month,$TargetUser,$ShowNoDutyDateOnly,$DayNumber='') {
		$Year = sprintf("%4d",$Year);
		$Month = sprintf("%02d",$Month);
		
		$sql = 'select 
							raw.UserID, 
							CONCAT(\''.$Year.'-'.$Month.'-\',IF(raw.DayNumber >9, raw.DayNumber,CONCAT(\'0\',raw.DayNumber))) as RecordDate,
							raw.DayNumber,
							raw.RecordTime 
						from 
							CARD_STAFF_ATTENDANCE2_RAW_LOG_'.$Year.'_'.$Month.' as raw ';
		if ($ShowNoDutyDateOnly) {
			$sql .= '
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE2_DAILY_LOG_'.$Year.'_'.$Month.' as dailylog 
							on raw.UserID = dailylog.StaffID 
								and 
								raw.DayNumber = dailylog.DayNumber ';
		}
		$sql .= '
						where 
							raw.UserID in (\''.(sizeof($TargetUser)>0?implode('\',\'',$this->Get_Safe_Sql_Query($TargetUser)):'-1').'\') ';
		if ($ShowNoDutyDateOnly) {
			$sql .= '
							and 
							dailylog.RecordType = \''.CARD_STATUS_NOSETTING.'\' ';
		}
		
		if($DayNumber != ''){
			$DayNumber = sprintf("%d",$DayNumber); 
			$sql .= ' and raw.DayNumber = \''.$DayNumber.'\' ';
		}
		$sql .= '
						order by 
							raw.DayNumber, raw.UserID, raw.RecordTime 
				 ';
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Get_Daily_Log_Entry_Info($Year,$Month,$TargetUser,$StartDate='',$EndDate='')
	{
		global $sys_custom;
		$YEAR = sprintf("%04d",$Year);
		$MONTH = sprintf("%02d",$Month);
		$TARGET_USER = implode(",",(array)$TargetUser);
		$date_range_cond = "";
		if($StartDate != '') 
			$date_range_cond .= " AND UNIX_TIMESTAMP(CONCAT('".$YEAR."-".$MONTH."-',d.DayNumber)) >= ".strtotime($StartDate)." ";
		if($EndDate != '')
			$date_range_cond .= " AND UNIX_TIMESTAMP(CONCAT('".$YEAR."-".$MONTH."-',d.DayNumber)) <= ".strtotime($EndDate)." ";
		$StaffIDField = $sys_custom['StaffAttendance']['RemarkAsStaffID']?"TRIM(u.Remark)":"u.UserLogin";
		
		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
		$sql = "SELECT 
					$StaffIDField as StaffID, 
					DATE_FORMAT(CONCAT('$YEAR-$MONTH-',d.DayNumber),'%d/%m/%Y') as RecordDateIn,
					DATE_FORMAT(IF(d.DutyStart > d.DutyEnd,DATE_ADD(CONCAT('$YEAR-$MONTH-',d.DayNumber),INTERVAL 1 DAY),CONCAT('$YEAR-$MONTH-',d.DayNumber)),'%d/%m/%Y') as RecordDateOut,
					d.InTime,d.InSchoolStation,d.OutTime,d.OutSchoolStation   
				FROM INTRANET_USER as u 
				INNER JOIN CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$YEAR."_".$MONTH." as d ON d.StaffID = u.UserID 
				WHERE u.RecordStatus IN ($recordstatus) AND u.RecordType = '1' 
					AND u.UserID IN ($TARGET_USER) 
					AND	((d.InTime IS NOT NULL AND d.InTime <> '') 
						OR (d.OutTime IS NOT NULL AND d.OutTime <> '')) 
					$date_range_cond 
				ORDER BY d.DayNumber,d.InTime,d.StaffID ";
		//debug_pr($sql);
		return $this->returnArray($sql);
	}
	
	function Check_Card_ID($CardID,$TargetUserID,$IsRFID) {
		global $sys_custom;
		
		$CardField = ($IsRFID == "1")? "RFID":"CardID";
		if ($CardID != "") {
			$EscapedCardID = $this->Get_Safe_Sql_Query($CardID);
			$sql = 'select 
								count(1) 
							from 
								INTRANET_USER 
							where 
								UserID != \''.$TargetUserID.'\'
								and 
								('.$CardField.' = \''.$EscapedCardID.'\' '.($CardField=='CardID' && $sys_custom['SupplementarySmartCard']?' OR CardID2=\''.$EscapedCardID.'\' OR CardID3=\''.$EscapedCardID.'\' OR CardID4=\''.$EscapedCardID.'\' ':'').') 
						 ';
			$Result = $this->returnVector($sql);
			
			return !($Result[0] > 0);
		}
		else 
			return true;
	}
	
	function Save_Card_ID($CardID,$TargetUserID,$IsRFID) {
		$CardField = ($IsRFID == "1")? "RFID":"CardID";
		
		$sql = 'update INTRANET_USER set 
							'.$CardField.' = \''.$CardID.'\' 
						where 
							UserID = \''.$TargetUserID.'\'';
		return $this->db_db_query($sql);
	}
	
	function Finalize_Import_Staff_Info() {
		$sql = 'select 
							UserLogin, 
							CardID,
							CardType 
						from 
							TEMP_IMPORT_STAFF_INFO';
		$TempResult = $this->returnArray($sql);
		//debug_r($TempResult);
		for ($i=0; $i< sizeof($TempResult); $i++) {
			$CardField = ($TempResult[$i]['CardType'] == "SmartCard")? "CardID":"RFID";
			$sql = 'update INTRANET_USER set 
								'.$CardField.' = \''.$TempResult[$i]['CardID'].'\' 
							where 
								UserLogin = \''.$TempResult[$i]['UserLogin'].'\'';
			$Result['UpdateCardID:'.$TempResult[$i]['UserLogin']] = $this->db_db_query($sql);
		}
		
		return sizeof($TempResult);
	}
	
	function Import_Staff_Info($Data,$ToTemp=true,$CardType="SmartCard")
	{
		global $Lang, $sys_custom;
		$CardField = ($CardType == "SmartCard")? "CardID":"RFID";
		$Error = array();
		$Result = array();
		if(!is_array($Data))
		{
			return array();
		}
		
		$sql = 'DROP TABLE TEMP_IMPORT_STAFF_INFO';
		$this->db_db_query($sql);
		$sql = 'CREATE TABLE TEMP_IMPORT_STAFF_INFO (
							UserLogin varchar(20) default NULL,
						  CardID varchar(255) default NULL,
						  CardType varchar(10) default NULL,
						  UNIQUE KEY (UserLogin) 
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					 ';
		$this->db_db_query($sql);
		
		$this->Start_Trans();
		$NameField = getNameFieldByLang("a.");
		$values_array = array();
		for($i=0;$i<sizeof($Data);$i++)
		{
			list($UserLogin, $CardID) = $Data[$i];
			$UserLogin = trim($UserLogin);
			$CardID = FormatSmartCardID(trim($CardID));
			
			$sql = "SELECT
						a.UserID
					FROM
						INTRANET_USER as a
					WHERE
						a.RecordType = '1'
				  	AND a.RecordStatus = '1'
						AND a.UserLogin = '".$this->Get_Safe_Sql_Query($UserLogin)."' ";
			$UserInfo = $this->returnArray($sql);
			if(sizeof($UserInfo)>0)
			{
				$TargetUserID = $UserInfo[0]['UserID'];
				$EscapedCardID = $this->Get_Safe_Sql_Query($CardID);
				$sql = "SELECT
							count(1) 
						FROM
							INTRANET_USER as u 
						WHERE 
							UserID != '$TargetUserID' 
							AND (".$CardField." = '".$EscapedCardID."' ".($CardField=='CardID' && $sys_custom['SupplementarySmartCard']?" OR CardID2='".$EscapedCardID."' OR CardID3='".$EscapedCardID."' OR CardID4='".$EscapedCardID."' ":"").") ";
				$CardIDInUse = $this->returnVector($sql);
				if($CardIDInUse[0] > 0)
				{
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['SmartcardWarning']);
				}else
				{	
					$sql = 'select count(1)
									from 
										TEMP_IMPORT_STAFF_INFO 
									where 
										UserLogin = \''.$this->Get_Safe_Sql_Query($UserLogin).'\'';
					$ImportAlready = $this->returnVector($sql);
					
					if ($ImportAlready[0] > 0) {
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['ImportedAlready']);
					}
					else {
						$sql = 'insert into TEMP_IMPORT_STAFF_INFO (
											UserLogin, 
											CardID,
											CardType
											)
										values (
											\''.$this->Get_Safe_Sql_Query($UserLogin).'\',
											\''.$this->Get_Safe_Sql_Query($CardID).'\',
											\''.$CardType.'\'
											)';
						//debug_r($sql);
						$Result['InsertTempRecord:'.$UserLogin] = $this->db_db_query($sql);
					}
				}
			}else
			{
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['StaffNotExists']);
				continue;
				//break;
			}
		}
		
		//debug_r($Result);
		if (in_array(false,$Result)) {
			$this->RollBack_Trans();
		}
		else {
			$this->Commit_Trans();
		}
		
		return $Error;
	}
	
	function Get_Special_Date_Template_List($Keyword="") {
		$sql = 'select 
							sdt.TemplateID 
						from 
							CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE sdt 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE3_SLOT s 
							on sdt.TemplateID = s.TemplateID 
						where 
							sdt.TemplateName like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
							OR 
							s.SlotName like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
							OR 
							SlotStart like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
							OR 
							SlotEnd like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\' 
						group by 
							sdt.TemplateID';
		$TemplateList = $this->returnVector($sql);
		
		if (sizeof($TemplateList) > 0) {
			$sql = 'select 
								sdt.TemplateID,
								sdt.TemplateName,
								count(sddr.TemplateID) as DateApplied
							from 
								CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE sdt 
								LEFT JOIN 
								CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION sddr 
								on sdt.TemplateID = sddr.TemplateID 
							where 
								sdt.TemplateID in ('.implode(',',$TemplateList).') 
							Group By 
								sdt.TemplateID
							';
			//debug_r($sql);
			return $this->returnArray($sql);
		}
		else {
			return array();
		}
	}
	
	function Get_Special_Date_Template_Slot_List($TemplateID) {
		$sql = 'Select
							s.SlotID,
							s.SlotName,
							s.SlotStart,
							s.SlotEnd,
							IF(s.InWavie=1,\'Y\',\'N\') as InWavie,
							IF(s.OutWavie=1,\'Y\',\'N\') as OutWavie,
							s.DutyCount,
              IFNULL(sdst.GroupInvolved,\'0\') as GroupInvolved,
              IFNULL(sdst1.IndividualInvolved,\'0\') as IndividualInvolved
						from
							CARD_STAFF_ATTENDANCE3_SLOT s
							LEFT JOIN
              (
              select
                a.SlotID,
                count(1) as GroupInvolved
              from
							  CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET a
							  inner join 
							  CARD_STAFF_ATTENDANCE2_GROUP g 
							  on 
							  	a.TemplateID = \''.$TemplateID.'\' 
							  	and 
							  	a.GroupOrIndividual = \'G\' 
							  	and 
							  	a.ObjID = g.GroupID 
              group by
                a.SlotID) as sdst
              on s.SlotID = sdst.SlotID
              LEFT JOIN
              (
              select
                b.SlotID,
                count(1) as IndividualInvolved
              from
							  CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET b 
							  inner join 
							  INTRANET_USER u 
							  on 
							  	b.TemplateID = \''.$TemplateID.'\'
	                and
	                b.GroupOrIndividual = \'I\' 
	                and 
	                b.ObjID = u.UserID 
              group by
                b.SlotID) as sdst1
              on s.SlotID = sdst1.SlotID
						where
							s.TemplateID = \''.$TemplateID.'\' 
						order by
							s.SlotStart';
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Check_Template_Name($TemplateName,$TemplateID="") {
		$sql = 'select 
							count(1) 
						from 
							CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE 
						where 
							TemplateName = \''.$this->Get_Safe_Sql_Query($TemplateName).'\' 
							and 
							TemplateID != \''.$TemplateID.'\'';
		//debug_r($sql);
		
		$Temp = $this->returnVector($sql);
		
		return ($Temp[0] > 0)? '0':'1';
	}
	
	function Save_Template($TemplateID,$TemplateName) {
		if ($TemplateID != "") {
			$sql = 'update CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE set 
								TemplateName = \''.$this->Get_Safe_Sql_Query($TemplateName).'\', 
								DateModify = NOW(),
								ModifyBy = \''.$_SESSION['UserID'].'\' 
							where 
								TemplateID = \''.$TemplateID.'\'';
		}
		else {
			$sql = 'insert into CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE (
								TemplateName,
								InputBy,
								DateInput
								)
							values (
								\''.$this->Get_Safe_Sql_Query($TemplateName).'\',
								\''.$_SESSION['UserID'].'\',
								NOW()
								)
							on duplicate key update  
								TemplateID = LAST_INSERT_ID(TemplateID), 
								TemplateName = \''.$this->Get_Safe_Sql_Query($TemplateName).'\', 
								DateModify = NOW(),
								ModifyBy = \''.$_SESSION['UserID'].'\'';
		}
		
		//debug_r($sql);
		return $this->db_db_query($sql);
	}
	
	function Delete_Template($TemplateID) {
		$sql = 'select 
							SlotID 
						from 
							CARD_STAFF_ATTENDANCE3_SLOT 
						where 
							TemplateID = \''.$TemplateID.'\'';
		$SlotList = $this->returnVector($sql);
		if (sizeof($SlotList) > 0) 
			$Result['DeleteSlots'] = $this->Delete_Slot($SlotList);
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET where TemplateID = \''.$TemplateID.'\'';
		$Result['DeleteSlotTarget'] = $this->db_db_query($sql);
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION where TemplateID = \''.$TemplateID.'\'';
		$Result['DeleteDateRelation'] = $this->db_db_query($sql);
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE where TemplateID = \''.$TemplateID.'\'';
		$Result['DeleteTemplate'] = $this->db_db_query($sql);
		
		//debug_r($Result);
		return !in_array(false,$Result);
	}
	
	function Check_Template_Date_Applied($TargetDate) {
		$sql = 'select 
							count(1) 
						from 
							CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION 
						where 
							TargetDate = \''.$TargetDate.'\'';
		$Temp = $this->returnVector($sql);
		return ($Temp[0] > 0)? '0':'1';
	}
	
	function Save_Template_Target_Date($TemplateID,$TargetDate) {
		// get all involved party
		$sql = 'select 
							ObjID,
							GroupOrIndividual 
						from 
							CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET 
						where 
							TemplateID = \''.$TemplateID.'\' 
						group by 
							ObjID, GroupOrIndividual 
						';
		$InvolvedParty = $this->returnArray($sql);
		
		$FinalIndividual = array();
		for ($i=0; $i< sizeof($InvolvedParty); $i++) {
			if ($InvolvedParty[$i]['GroupOrIndividual'] == "G") {
				$sql = 'select 
									UserID 
								from 
									CARD_STAFF_ATTENDANCE_USERGROUP 
								where 
									GroupID = \''.$InvolvedParty[$i]['ObjID'].'\'';
				$FinalIndividual = array_merge($FinalIndividual,$this->returnVector($sql));
			}
			else { // individual
				$FinalIndividual[] = $InvolvedParty[$i]['ObjID'];
			}
		}
		
		for ($i=0; $i< sizeof($TargetDate); $i++) {
			if (sizeof($FinalIndividual) > 0) {
				// remove all non full day leave recird on newly added target date
				$sql = 'delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD where 
									RecordDate = \''.$TargetDate[$i].'\' 
									and 
									StaffID in (\''.implode(',',$FinalIndividual).'\') 
									and 
									(
										SlotID IS NOT NULL 
										OR 
										SlotID = \'\'
									)
									';
				$Result['RemoveInvolvedTargetPrevLeaveRequest:'.$TargetDate[$i]] = $this->db_db_query($sql);
			}
			
			$sql = 'insert into CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION (
								TemplateID,
								TargetDate,
								InputBy,
								DateInput
								)
							values (
								\''.$TemplateID.'\',
								\''.$TargetDate[$i].'\',
								\''.$_SESSION['UserID'].'\',
								NOW()
								)';
			//debug_r($sql);
			$Result['InsertDateRelation:'.$TargetDate[$i]] = $this->db_db_query($sql);
		}
		
		//debug_r($Result);
		return !in_array(false,$Result);
	}
	
	function Get_Template_Target_Obj_List($SlotID,$GroupOrIndividual="Group") {
		if ($GroupOrIndividual == "Group") {
			$FieldName = 't.GroupName';
		}
		else {
			$FieldName = getNameFieldByLang('t.');
		}
		
		$sql = 'select 
							sdst.ObjID,
							'.$FieldName.' as ObjName
						from 
							CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET sdst ';
		if ($GroupOrIndividual == "Group") {
			$sql .= '
							inner join 
							CARD_STAFF_ATTENDANCE2_GROUP t 
							on 
								sdst.SlotID = \''.$SlotID.'\' 
								and 
								sdst.GroupOrIndividual = \'G\'
								and 
								sdst.ObjID = t.GroupID 
						order by 
							t.GroupName';
		}
		else {
			$sql .= '
							inner join 
							INTRANET_USER t 
							on 
								sdst.SlotID = \''.$SlotID.'\' 
								and 
								sdst.GroupOrIndividual = \'I\'
								and 
								sdst.ObjID = t.UserID 
						order by 
							t.EnglishName';
		}
		
		return $this->returnArray($sql);
	}
	
	function Save_Template_Apply_Target($TemplateID,$SlotID,$GroupOrIndividual,$ObjSelected) {
		$CurrentSelection = $this->Get_Template_Target_Obj_List($SlotID,$GroupOrIndividual);
		
		for ($i=0; $i< sizeof($CurrentSelection); $i++) {
			$found = false;
			for ($j=0; $j< sizeof($ObjSelected); $j++) {
				if ($CurrentSelection[$i]['ObjID'] == $ObjSelected[$j]) {
					$found = true;
					break;
				}
			}
			
			if (!$found) {
				$DeleteObj[] = $CurrentSelection[$i]['ObjID'];
			}
		}
		// insert new
		for ($i=0; $i< sizeof($ObjSelected); $i++) {
			$sql = 'insert into CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET (
								TemplateID,
								SlotID,
								GroupOrIndividual,
								ObjID,
								DateInput,
								InputBy
								)
							values (
								\''.$TemplateID.'\',
								\''.$SlotID.'\',
								\''.(($GroupOrIndividual=="Group")? 'G':'I').'\',
								\''.$ObjSelected[$i].'\',
								NOW(),
								\''.$_SESSION['UserID'].'\'
								) 
							on duplicate key update 
								DateModify = NOW(),
								ModifyBy = \''.$_SESSION['UserID'].'\'';
			$Result['InsertSlotTarget:'.$i] = $this->db_db_query($sql);
		}
		
		// remove old
		if (sizeof($DeleteObj) > 0) {
			// get all involved Target date for this template
			$sql = 'select 
								TargetDate
							from 
								CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION 
							where 
								TemplateID = \''.$TemplateID.'\' 
								and 
								TargetDate > CURDATE()';
			//debug_r($sql);
			$TargetDates = $this->returnVector($sql);
			
			// get group member from db if target is group
			if ($GroupOrIndividual == "Group") {
				$sql = 'select 
									UserID 
								from 
									CARD_STAFF_ATTENDANCE_USERGROUP 
								where 
									GroupID in ('.implode(',',$DeleteObj).')';
				$DeleteObjIndividual = $this->returnVector($sql);
			}
			else {
				$DeleteObjIndividual = $DeleteObj;
			}
			
			if (sizeof($TargetDates) > 0 && sizeof($DeleteObjIndividual) > 0) {
				// remove all non full day leave request about this slot on each target date
				$sql = 'delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD where 
									RecordDate in ('.implode(',',$TargetDates).') 
									and 
									StaffID in ('.implode(',',$DeleteObjIndividual).') 
									and 
									SlotID = \''.$SlotID.'\' ';
				//debug_r($sql);
				$Result['RemoveLeaveRecords'] = $this->db_db_query($sql);
			}
			
			
			// the slot target
			$sql = 'delete from CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET where 
								GroupOrIndividual = \''.(($GroupOrIndividual=="Group")? 'G':'I').'\' 
								and 
								ObjID in ('.implode(',',$DeleteObj).') 
								and 
								SlotID = \''.$SlotID.'\'';
			//debug_r($sql);
			$Result['RemoveSlotTarget'] = $this->db_db_query($sql);
		}

		//debug_r($Result);
		return !in_array(false,$Result);
	}
	
	function Delete_Template_Target_Date($TemplateID,$TargetDate) {
		$sql = 'select 
							SlotID 
						from 
							CARD_STAFF_ATTENDANCE3_SLOT 
						where 
							TemplateID = \''.$TemplateID.'\'';
		$SlotList = $this->returnVector($sql);
		
		if (sizeof($SlotList) > 0) {
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD where 
								RecordDate = \''.$TargetDate.'\' 
								and 
								SlotID in ('.implode(',',$SlotList).')';
			$Result['DeleteLeaveRecord'] = $this->db_db_query($sql);
		}
		
		$sql = 'delete from CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION where 
							TemplateID = \''.$TemplateID.'\' 
							and 
							TargetDate = \''.$TargetDate.'\'';
		$Result['DeleteDateRelation'] = $this->db_db_query($sql);
		
		return !in_array(false,$Result);
	}
	
	function Check_Involve_Target_Slot_Setting($TemplateID,$SlotID,$ObjSelected,$GroupOrIndividual) {
		$Slot = new staff_attend_slot($SlotID);
		for ($i=0; $i< sizeof($ObjSelected); $i++) {
			$sql = 'select 
								s.SlotID,
								s.SlotName,
								s.SlotStart,
								s.SlotEnd
							from 
								CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET sdst 
								inner join 
								CARD_STAFF_ATTENDANCE3_SLOT s 
								on sdst.TemplateID = \''.$TemplateID.'\' 
									and 
									sdst.ObjID = \''.$ObjSelected[$i].'\' 
									and 
									sdst.GroupOrIndividual = \''.(($GroupOrIndividual=="Group")? 'G':'I').'\' 
									and 
									sdst.SlotID = s.SlotID 
									and 
									sdst.SlotID != \''.$SlotID.'\' 
							where 
								\''.$Slot->SlotStart.'\' between SlotStart and SlotEnd 
								or 
								\''.$Slot->SlotEnd.'\' between SlotStart and SlotEnd';
			//debug_r($sql);
			$Temp = $this->returnArray($sql);
			
			if (sizeof($Temp) > 0) {
				if ($GroupOrIndividual == "Group") {
					$Group = new staff_attend_group($ObjSelected[$i]);
					$Name = $Group->GroupName;
				}
				else {
					$NameField = getNameFieldByLang('u.');
					
					$sql = 'select 
										'.$NameField.' as UserName 
									from 
										INTRANET_USER u 
									where 
										UserID = \''.$ObjSelected[$i].'\'';
					$UserName = $this->returnVector($sql);
					$Name = $UserName[0];
				}
				$ProblemObj[] = array($ObjSelected[$i],$Name,$Temp[0]['SlotName'],$Temp[0]['SlotStart'],$Temp[0]['SlotEnd']);
			}
		}
		
		//debug_r($ProblemObj);
		if (sizeof($ProblemObj) > 0) {
			return $ProblemObj;
		}
		else {
			return true;
		}
	}
	
	function Get_Absence_Report_Data($Params)
	{
		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
		
		$TargetStartDate = $Params['TargetStartDate'];
		$TargetEndDate = $Params['TargetEndDate'];
		$ts_start = strtotime($TargetStartDate);
		$ts_end = strtotime($TargetEndDate);
		$card_log_table_names=array();
		for($ts_current=$ts_start;$ts_current<=$ts_end;$ts_current)
		{
			$year = date("Y", $ts_current);
			$month = date("m", $ts_current);
			$card_log_table_names[] = $this->createTable_Card_Staff_Attendance2_Daily_Log($year, $month);
			$ts_current = mktime(0,0,0,intval($month)+1,1,intval($year));
		}
		
		$StaffIDList = array();
		if($Params['ReportType'] == "p")//p - Select by Personal
		{
			if($Params['StaffID']=='')//Select all staff
			{
				$StaffList = $this->Get_Staff_Info("","",$recordstatus,$ReportDisplayLeftStaff);
				for($i=0;$i<sizeof($StaffList);$i++)
				{
					$StaffIDList[] = $StaffList[$i]['UserID'];
				}
			}else // Select a specific staff
			{
				$StaffIDList[] = $Params['StaffID'];
			}
		}else // g - Select by Group
		{
			if($Params['GroupID']=='')//Select all staff
			{
				$StaffList = $this->Get_Staff_Info("","",$recordstatus,$ReportDisplayLeftStaff);
				for($i=0;$i<sizeof($StaffList);$i++)
				{
					$StaffIDList[] = $StaffList[$i]['UserID'];
				}
			}else // Select staff from a specific group
			{
				$StaffList = $this->Get_Group_Member_List($Params['GroupID'],"",$recordstatus,$ReportDisplayLeftStaff);
				for($i=0;$i<sizeof($StaffList);$i++)
				{
					$StaffIDList[] = $StaffList[$i]['UserID'];
				}
			}
		}
		
		if($Params['SelectWaived']==1){// with waived
			$waived_cond = "";
		}else{// without waived
			$waived_cond = " AND (d.InWaived IS NULL OR d.InWaived != 1)";
		}
		
		$result = array();
		//$NameField = getNameFieldByLang("u.");
		$NameField = $this->Get_Name_Field("u.");
		$ArchiveNameField = $this->Get_Archive_Name_Field("u.");
		
		for($i=0;$i<sizeof($card_log_table_names);$i++)
		{
			$card_log_table_name = $card_log_table_names[$i];
			
			$sql = "SELECT
						$NameField as StaffName,
						p.RecordDate,
						d.SlotName,
						d.DutyCount,
						d.DutyStart,
						d.DutyEnd,
						IF(r.ReasonText IS NULL, '', r.ReasonText) as Reason,
						IF(d.Remark IS NULL, '', d.Remark) as Remark,
						u.EnglishName 
					FROM
						INTRANET_USER as u
						INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p ON p.StaffID = u.UserID
								   AND (p.RecordDate BETWEEN '$TargetStartDate' AND '$TargetEndDate' )
								   AND p.RecordType = '".CARD_STATUS_ABSENT."' 
						INNER JOIN $card_log_table_name as d ON p.StaffID = d.StaffID AND d.InAttendanceRecordID = p.RecordID AND d.InSchoolStatus = '".CARD_STATUS_ABSENT."' $waived_cond 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND p.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
						LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r ON p.ReasonID = r.ReasonID
					WHERE 
						u.UserID IN (".(sizeof($StaffIDList)?implode(',', $StaffIDList):'-1').") 
						AND u.RecordType = '1' AND u.RecordStatus IN ($recordstatus) ";
			if($ReportDisplayLeftStaff) {
				$sql .= "UNION (
							SELECT
								$ArchiveNameField as StaffName,
								p.RecordDate,
								d.SlotName,
								d.DutyCount,
								d.DutyStart,
								d.DutyEnd,
								IF(r.ReasonText IS NULL, '', r.ReasonText) as Reason,
								IF(d.Remark IS NULL, '', d.Remark) as Remark,
								u.EnglishName 
							FROM
								INTRANET_ARCHIVE_USER as u
								INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p ON p.StaffID = u.UserID
										   AND (p.RecordDate BETWEEN '$TargetStartDate' AND '$TargetEndDate' )
										   AND p.RecordType = '".CARD_STATUS_ABSENT."' 
								INNER JOIN $card_log_table_name as d ON p.StaffID = d.StaffID AND d.InAttendanceRecordID = p.RecordID AND d.InSchoolStatus = '".CARD_STATUS_ABSENT."' $waived_cond 
								INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND p.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
								LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r ON p.ReasonID = r.ReasonID
							WHERE
								u.UserID IN (".(sizeof($StaffIDList)?implode(',', $StaffIDList):'-1').") 
								AND u.RecordType = '1' 
						)";
			}
			
			$sql.= "ORDER BY 
						EnglishName, RecordDate, DutyStart ";
			$temp = $this->returnArray($sql);
			for($j=0;$j<sizeof($temp);$j++)
			{
				$result[] = $temp[$j];
			}
		}
		
		return $result;
	}
	
	function Get_LeaveAbsence_Report_Data($Params, $TargetUserID, $card_log_table_names_arr=null)
	{
		global $Lang;
		
		$TargetStartDate = $Params['TargetStartDate'];
		$TargetEndDate = $Params['TargetEndDate'];
		$ts_start = strtotime($TargetStartDate);
		$ts_end = strtotime($TargetEndDate);
		$card_log_table_names=array();
		if($card_log_table_names_arr == null)
		{
			for($ts_current=$ts_start;$ts_current<=$ts_end;$ts_current)
			{
				$year = date("Y", $ts_current);
				$month = date("m", $ts_current);
				$card_log_table_names[] = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$year."_".$month;
				$ts_current = mktime(0,0,0,intval($month)+1,1,intval($year));
			}
		}else $card_log_table_names = $card_log_table_names_arr;
		
		if($Params["Status"] == '')
		{
			$status_array = array(CARD_STATUS_ABSENT,
								  CARD_STATUS_EARLYLEAVE,
								  CARD_STATUS_HOLIDAY,
								  CARD_STATUS_OUTGOING);
		}else
		{
			$status_array = array($Params["Status"]);
		}
		$status_cond = (sizeof($status_array)>0)?implode(",", $status_array):"-1";
		
		$waive_cond = ($Params['Waived']==1)?"":"  AND p.Waived != '1' ";
		
		$Settings = $this->Get_General_Settings();
		if($Settings['ReportDisplaySuspendedStaff']==1){
			$recordstatus = "0,1,2";
		}else{
			$recordstatus = "1";
		}
		$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
		
		$result = array();
		//$NameField = getNameFieldByLang("u.");
		$NameField = $this->Get_Name_Field("u.");
		$ArchiveNameField = $this->Get_Archive_Name_Field("u.");
		
		for($i=0;$i<sizeof($card_log_table_names);$i++)
		{
			$card_log_table_name = $card_log_table_names[$i];
			$sql = "SELECT
						$NameField as StaffName,
						p.RecordDate,
						p.RecordType,
						IF(d.SlotName IS NULL,'".$Lang['StaffAttendance']['FullDay']."',d.SlotName) as SlotName,
						IF(d.DutyCount IS NULL,1,d.DutyCount) as DutyCount,
						IF(p.ProfileCountFor IS NULL,1,p.ProfileCountFor) as ProfileCountFor,
						p.Waived,
						r.ReasonID,
						IF(r.ReasonText IS NULL, '', r.ReasonText) as Reason,
						d.DutyStart 
					FROM
						INTRANET_USER as u
						INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p ON p.StaffID = u.UserID
								   AND (p.RecordDate BETWEEN '$TargetStartDate' AND '$TargetEndDate')
								   AND p.RecordType IN (".$status_cond.")
								   $waive_cond
						INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID AND (p.RecordID = d.InAttendanceRecordID OR p.RecordID = d.OutAttendanceRecordID) 
								    AND (d.InSchoolStatus IN (".$status_cond.") OR d.OutSchoolStatus IN (".$status_cond.") ) 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND p.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
						LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r ON p.ReasonID = r.ReasonID
					WHERE
						u.UserID = '$TargetUserID' AND u.RecordType = '1' AND u.RecordStatus IN ($recordstatus) ";
			if($ReportDisplayLeftStaff) {
				$sql .= "UNION (
						SELECT
							$ArchiveNameField as StaffName,
							p.RecordDate,
							p.RecordType,
							IF(d.SlotName IS NULL,'".$Lang['StaffAttendance']['FullDay']."',d.SlotName) as SlotName,
							IF(d.DutyCount IS NULL,1,d.DutyCount) as DutyCount,
							IF(p.ProfileCountFor IS NULL,1,p.ProfileCountFor) as ProfileCountFor,
							p.Waived,
							r.ReasonID,
							IF(r.ReasonText IS NULL, '', r.ReasonText) as Reason,
							d.DutyStart 
						FROM
							INTRANET_ARCHIVE_USER as u
							INNER JOIN CARD_STAFF_ATTENDANCE2_PROFILE as p ON p.StaffID = u.UserID
									   AND (p.RecordDate BETWEEN '$TargetStartDate' AND '$TargetEndDate')
									   AND p.RecordType IN (".$status_cond.")
									   $waive_cond
							INNER JOIN $card_log_table_name as d ON d.StaffID = u.UserID AND (p.RecordID = d.InAttendanceRecordID OR p.RecordID = d.OutAttendanceRecordID) 
									    AND (d.InSchoolStatus IN (".$status_cond.") OR d.OutSchoolStatus IN (".$status_cond.") ) 
							INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND p.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
							LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r ON p.ReasonID = r.ReasonID
						WHERE
							u.UserID = '$TargetUserID' AND u.RecordType = '1' ) ";
			}
			$sql.= "ORDER BY 
						RecordDate, DutyStart ";
			$temp = $this->returnArray($sql);
			for($j=0;$j<sizeof($temp);$j++)
			{
				$result[] = $temp[$j];
			}
		}
		
		$today = date("Y-m-d");
		$ts_today = strtotime($today);
		$LeaveStartDate = $ts_start > $ts_today ? $TargetStartDate : $today;
		// Find preset leave record only if end date is greater than date of today
		if($ts_end > $ts_today)
		{
			$sql = "SELECT
						$NameField as StaffName,
						a.RecordDate,
						a.RecordType,
						IF(s.SlotName IS NULL,'".$Lang['StaffAttendance']['FullDay']."',s.SlotName) as SlotName,
						IF(s.DutyCount IS NULL,1,s.DutyCount) as DutyCount,
						IF(s.DutyCount IS NULL,1,s.DutyCount) as ProfileCountFor,
						0 as Waived,
						r.ReasonID,
						IF(r.ReasonText IS NULL, '', r.ReasonText) as Reason
					FROM
						INTRANET_USER as u
						INNER JOIN CARD_STAFF_ATTENDANCE2_LEAVE_RECORD as a ON u.UserID = a.StaffID
								   AND (a.RecordDate BETWEEN '$LeaveStartDate' AND '$TargetEndDate') 
								   AND a.RecordType IN (".$status_cond.") 
								   AND a.RecordDate > '$today' 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp ON wp.UserID=u.UserID AND a.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd 
						LEFT JOIN CARD_STAFF_ATTENDANCE3_SLOT as s ON s.SlotID = a.SlotID 
						LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r ON a.ReasonID = r.ReasonID
					WHERE
						u.UserID = '$TargetUserID' AND u.RecordType = '1' AND u.RecordStatus IN ($recordstatus)  
					ORDER BY 
						a.RecordDate, s.SlotStart ";
			$temp = $this->returnArray($sql);
			for($j=0;$j<sizeof($temp);$j++)
			{
				$result[] = $temp[$j];
			}
		}
		
		return $result;
	}
	
	function Index_Page_Process() {
		$Today = date('Y-m-d');
		
		$sql = 'select 
							MAX(AccessDate) 
						from 
							CARD_STAFF_ATTENDANCE3_LAST_ACCESS';
		$LastAccess = $this->returnVector($sql);
		
		if ($LastAccess[0] != $Today) {
			$sql = 'select 
								UserID 
							from 
								INTRANET_USER 
							where 
								RecordType = \'1\'';
			$UserList = $this->returnVector($sql);
			if ($LastAccess[0] == "") { // on one accessed before
				$this->Freeze_Duty_By_Date($UserList,$Today,$Today);
			}
			else {
				//$this->Freeze_Duty_By_Date($UserList,$LastAccess[0],$Today);
				$end_ts = strtotime($Today);
				$start_ts = strtotime($LastAccess[0]);
				
				for($cur_ts = $start_ts;$cur_ts <= $end_ts;$cur_ts+=86400){
					$date = date("Y-m-d",$cur_ts);
					$this->Freeze_Duty_By_Date($UserList,$date,$date);
					
					$sql = 'insert into CARD_STAFF_ATTENDANCE3_LAST_ACCESS (
								AccessDate,
								UserID,
								DateInput,
								DateModify 
							)
							values (
								\''.$date.'\',
								\''.$_SESSION['UserID'].'\',
								NOW(),
								NOW()
							)on duplicate key update 
								AccessDate = \''.$date.'\',
								DateModify = NOW() ';
					$result = $this->db_db_query($sql);
				}
			}
		}
		
		$sql = 'insert into CARD_STAFF_ATTENDANCE3_LAST_ACCESS (
							AccessDate,
							UserID,
							DateInput,
							DateModify
						)
						values (
							NOW(),
							\''.$_SESSION['UserID'].'\',
							NOW(),
							NOW()
						) 
						on duplicate key update 
							AccessDate = NOW(),
							DateModify = NOW()
							';
		return $this->db_db_query($sql);
	}
	
	function Freeze_Duty_By_Date($UserList,$StartDate,$EndDate) {
		$StartTimeStamp = strtotime($StartDate);
		$EndTimeStamp = strtotime($EndDate);
		$Today = strtotime(date('Y-m-d'));
		$OneDay = 86400;
		while ($StartTimeStamp <= $Today && $StartTimeStamp <= $EndTimeStamp) {
			for ($i=0; $i< sizeof($UserList); $i++) {
				$this->Get_Duty_By_Date($UserList[$i],date('Y-m-d',$StartTimeStamp),true,false,true,true); // actually is save duty by date
			}
			$StartTimeStamp += $OneDay;
		}
	}
	
	function Get_Real_Time_Staff_Status($Keyword="",$ExtTime="",$SortBy="Group", $InSchoolInOutStatus="1") {
		//debug_r($ExtTime);
		$current_time = ($ExtTime != "")? strtotime($ExtTime):time();
		$Month = date('m',$current_time);
    $Year = date('Y',$current_time);
    $Day = date('j',$current_time);
    $Today = date('Y-m-d',$current_time);
    $Yesterday_ts = mktime(1,1,1,($Month+0),($Day-1),intval($Year));
    $Yesterday = date('Y-m-d',$Yesterday_ts);
    $YesterdayYear = date('Y',$Yesterday_ts);
    $YesterdayMonth = date('m',$Yesterday_ts);
    $YesterdayDay = date('j',$Yesterday_ts);
    $Tmr = date('Y-m-d',mktime(1,1,1,($Month+0),($Day+1),$Year));
    
    $ts_now = $current_time - strtotime($Today);
    		
		$NameField = getNameFieldByLang("u.");
		$sql = 'select
						  u.UserID, 
						  g.GroupName, 
						  '.$NameField.' as UserName 
						from
							INTRANET_USER as u 
							INNER JOIN 
							CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
						 	ON 
						 		wp.UserID=u.UserID 
						 		AND 
						 		\''.$this->Get_Safe_Sql_Query($Today).'\' BETWEEN wp.PeriodStart AND wp.PeriodEnd 
						 	LEFT JOIN 
						 	CARD_STAFF_ATTENDANCE_USERGROUP as ug 
						 	on u.UserID = ug.UserID 
						 	LEFT JOIN 
						 	CARD_STAFF_ATTENDANCE2_GROUP as g 
						 	on ug.GroupID = g.GroupID
						where 
							u.RecordType = \'1\' 
							and 
							u.RecordStatus = \'1\' 
							and 
							(
							u.EnglishName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							or 
							u.ChineseName like \'%'.$this->Get_Safe_Sql_Like_Query(htmlspecialchars($Keyword,ENT_QUOTES)).'%\' 
							or 
							IFNULL(GroupName,\'\') like \'%'.$this->Get_Safe_Sql_Like_Query($Keyword).'%\'
							)
						order by ';
		if ($SortBy == "Group") 
			$sql .= 'g.GroupName, u.EnglishName';
		else
			$sql .= 'u.EnglishName';
			
		//debug_r($sql);
		$UserList = $this->returnArray($sql);
	
	// get raw log times
	if($InSchoolInOutStatus == "2") { 
		$raw_log_table = "CARD_STAFF_ATTENDANCE2_RAW_LOG_".$Year."_".$Month;
		//$UserIdList = Get_Array_By_Key($UserList, 'UserID');
		
		$sql = "SELECT UserID,RecordTime FROM ".$raw_log_table." WHERE DayNumber='".$this->Get_Safe_Sql_Query($Day)."' ORDER BY UserID,RecordTime";
		$RawLogAry = $this->returnResultSet($sql);
		$UserIdToRawLogTime = array();
		$num_of_rawlog = count($RawLogAry);
		for($j=0;$j<$num_of_rawlog;$j++) {
			if(!isset($UserIdToRawLogTime[$RawLogAry[$j]['UserID']])) {
				$UserIdToRawLogTime[$RawLogAry[$j]['UserID']] = array();
			}
			$UserIdToRawLogTime[$RawLogAry[$j]['UserID']][] = $RawLogAry[$j]['RecordTime'];
			/*
			if((count($UserIdToRawLogTime[$RawLogAry[$j]['UserID']]) % 2) == 0){
				$UserIdToRawLogTime[$RawLogAry[$j]['UserID']][] = '<span style="color:green">'. $RawLogAry[$j]['RecordTime']. '</span>';
				
			} else {
				$UserIdToRawLogTime[$RawLogAry[$j]['UserID']][] = '<span style="color:red">'. $RawLogAry[$j]['RecordTime']. '</span>';
			}
			*/
		}
	}
	
	$userIdAry = Get_Array_By_Key($UserList,'UserID');
	
	// Get all users today duty time slot records
	$today_daily_log = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$Year."_".$Month;
	$sql = "SELECT 
				log.StaffID,
				log.SlotName,
				log.DutyStart as SlotStart,
				log.DutyEnd as SlotEnd, 
				log.InSchoolStatus,
				log.InTime,
				log.OutSchoolStatus,
				log.OutTime,
				r.ReasonText as InReason, 
				r1.ReasonText as OutReason 
			FROM $today_daily_log as log 
			LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as ip ON log.InAttendanceRecordID = ip.RecordID 
			LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r on ip.ReasonID = r.ReasonID 
            LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as op on log.OutAttendanceRecordID = op.RecordID
			LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r1 on op.ReasonID = r1.ReasonID 
			WHERE log.DayNumber='".$this->Get_Safe_Sql_Query($Day)."' AND log.SlotName IS NOT NULL 
				AND log.StaffID IN (".implode(",",$userIdAry).") 
			ORDER BY log.StaffID,log.DutyStart ";
	$today_records = $this->returnResultSet($sql);
	//debug_r($sql);
	$today_record_count = count($today_records);
	$userIdToTodayDuty = array();
	for($i=0;$i<$today_record_count;$i++){
		if(!isset($userIdToTodayDuty[$today_records[$i]['StaffID']])){
			$userIdToTodayDuty[$today_records[$i]['StaffID']] = array('CardSlotDetail'=>array(),'CardSlotToDuty'=>array());
		}
		$userIdToTodayDuty[$today_records[$i]['StaffID']]['CardSlotDetail'][$today_records[$i]['SlotName']] = $today_records[$i];
		$userIdToTodayDuty[$today_records[$i]['StaffID']]['CardSlotToDuty'][$today_records[$i]['SlotName']][] = $today_records[$i];
	}
	
	// Get all users yesterday duty time slot records
	$yesterday_daily_log = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$YesterdayYear."_".$YesterdayMonth;
	$sql = "SELECT 
				log.StaffID,
				log.SlotName,
				log.DutyStart as SlotStart,
				log.DutyEnd as SlotEnd, 
				log.InSchoolStatus,
				log.InTime,
				log.OutSchoolStatus,
				log.OutTime,
				r.ReasonText as InReason, 
				r1.ReasonText as OutReason 
			FROM $yesterday_daily_log as log  
			LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as ip ON log.InAttendanceRecordID = ip.RecordID 
			LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r on ip.ReasonID = r.ReasonID 
            LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as op on log.OutAttendanceRecordID = op.RecordID
			LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as r1 on op.ReasonID = r1.ReasonID 
			WHERE log.DayNumber='$YesterdayDay' AND log.SlotName IS NOT NULL 
				AND log.StaffID IN (".implode(",",$userIdAry).") 
			ORDER BY log.StaffID,log.DutyStart ";
	$yesterday_records = $this->returnResultSet($sql);
	$yesterday_record_count = count($yesterday_records);
	$userIdToYesterdayDuty = array();
	for($i=0;$i<$yesterday_record_count;$i++){
		if(!isset($userIdToYesterdayDuty[$yesterday_records[$i]['StaffID']])){
			$userIdToYesterdayDuty[$yesterday_records[$i]['StaffID']] = array('CardSlotDetail'=>array(),'CardSlotToDuty'=>array());
		}
		$userIdToYesterdayDuty[$yesterday_records[$i]['StaffID']]['CardSlotDetail'][$yesterday_records[$i]['SlotName']] = $yesterday_records[$i];
		$userIdToYesterdayDuty[$yesterday_records[$i]['StaffID']]['CardSlotToDuty'][$yesterday_records[$i]['SlotName']][] = $yesterday_records[$i];
	}
	
    for ($j=0; $j< sizeof($UserList); $j++) {
			// get Today Duty
	  	//$Duty = $this->Get_Duty_By_Date($UserList[$j]['UserID'],$Today,true); 
	  	//$YesterdayDuty = $this->Get_Duty_By_Date($UserList[$j]['UserID'],$Yesterday,true); 
	  	
	  	$Duty = $userIdToTodayDuty[$UserList[$j]['UserID']];
	  	$YesterdayDuty = $userIdToYesterdayDuty[$UserList[$j]['UserID']];
	  	
	  	unset($SlotTimeArray);
	  	// get yesterday last slot if it is overnight duty
	  	$i = 0;
	  	if(count($YesterdayDuty['CardSlotDetail'])>0){
		  	foreach ($YesterdayDuty['CardSlotDetail'] as $SlotName => $SlotDetail) {
		  		$YesterdaySlot[] = $SlotDetail;
		  		if ($i == 0) 
		  			$FirstStartTime = $SlotDetail['SlotStart'];
		  		
		  		if ($SlotDetail['SlotEnd'] < $FirstStartTime) // yesterday have overnight shift
		  			$SlotTimeArray[] = array('SlotName'=> $SlotName,'Timestamp'=> strtotime($YesterdayDay." ".$SlotDetail['SlotEnd']),'IsYesterDay'=> 1);
		  		$i++;
		  	}
	  	}
	  	// get today duty
	  	$i = 0;
	  	if(count($Duty['CardSlotDetail'])>0){
		  	foreach ($Duty['CardSlotDetail'] as $SlotName => $SlotDetail) {
		  		$TodaySlot[] = $SlotDetail;
		  		if ($i == 0) 
		  			$FirstStartTime = $SlotDetail['SlotStart'];
		  		
		  		$SlotTimeArray[] = array('SlotName'=> $SlotName,'Timestamp'=> strtotime($Today." ".$SlotDetail['SlotStart']));
		  		
		  		if ($SlotDetail['SlotEnd'] < $FirstStartTime) // have overnight shift
		  			$SlotTimeArray[] = array('SlotName'=> $SlotName,'Timestamp'=> strtotime($Tmr." ".$SlotDetail['SlotEnd']));
		  		else 
		  			$SlotTimeArray[] = array('SlotName'=> $SlotName,'Timestamp'=> strtotime($Today." ".$SlotDetail['SlotEnd']));
		  		$i++;
		  	}
	  	}
	  	unset($ReturnArray);
	  	$ReturnArray['StaffID'] = $UserList[$j]['UserID'];
	  	$ReturnArray['GroupName'] = $UserList[$j]['GroupName'];
	  	$ReturnArray['StaffName'] = $UserList[$j]['UserName'];
	  	// if current is before first slot time (yesterday last duty/ today first duty)
	  	if ($SlotTimeArray[0]['Timestamp'] > $current_time) {
	  		if ($SlotTimeArray[0]['IsYesterDay'] == 1) { // yesterday last duty haven't end
	  			$YesterdayLastDuty = $YesterdayDuty['CardSlotToDuty'][$SlotTimeArray[0]['SlotName']][0];
	  			
	  			if ($YesterdayLastDuty['InSchoolStatus'] ==  CARD_STATUS_OUTGOING) { // no duty for that time slot
						// not in school
						$ReturnArray['SlotName'] = $YesterdayLastDuty['SlotName'];
						$ReturnArray['SlotStatus'] = $YesterdayLastDuty['InSchoolStatus'];
						$ReturnArray['StaffStatus'] = "NotInSchool";
						$ReturnArray['Reason'] = $YesterdayLastDuty['InReason'];
					}
					else if ($YesterdayLastDuty['InSchoolStatus'] ==  CARD_STATUS_HOLIDAY) { // no duty for that time slot
						// not in school
						$ReturnArray['SlotName'] = $YesterdayLastDuty['SlotName'];
						$ReturnArray['SlotStatus'] = $YesterdayLastDuty['InSchoolStatus'];
						$ReturnArray['StaffStatus'] = "NotInSchool";
						$ReturnArray['Reason'] = $YesterdayLastDuty['InReason'];
					}
					else {
		  			if ($YesterdayLastDuty['InSchoolStatus'] != CARD_STATUS_ABSENT && trim($YesterdayLastDuty['InSchoolStatus']) != "") {
		  				if ($YesterdayLastDuty['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE || ($YesterdayLastDuty['OutSchoolStatus'] == CARD_STATUS_NORMAL && $YesterdayLastDuty['OutSchoolStatus'] != "")) {
								// not in school
								$ReturnArray['SlotName'] = $YesterdayLastDuty['SlotName'];
								$ReturnArray['SlotStatus'] = $YesterdayLastDuty['OutSchoolStatus'];
								$ReturnArray['StaffStatus'] = "NotInSchool";
								$ReturnArray['Reason'] = $YesterdayLastDuty['OutReason'];
							}
							else {
								// at school
								$ReturnArray['SlotName'] = $YesterdayLastDuty['SlotName'];
								$ReturnArray['SlotStatus'] = $YesterdayLastDuty['InSchoolStatus'];
								$ReturnArray['StaffStatus'] = "InSchool";
								$ReturnArray['Reason'] = $YesterdayLastDuty['InReason'];
							}
		  			}
		  			else {
		  				// not in school
							$ReturnArray['SlotName'] = $YesterdayLastDuty['SlotName'];
							$ReturnArray['SlotStatus'] = $YesterdayLastDuty['OutSchoolStatus'];
							$ReturnArray['StaffStatus'] = "NotInSchool";
							$ReturnArray['Reason'] = $YesterdayLastDuty['OutReason'];
		  			}
		  		}
	  		}
	  		else { // today first duty
	  			$TodayFirstDuty = $Duty['CardSlotToDuty'][$SlotTimeArray[0]['SlotName']][0];
	  			if ($TodayFirstDuty['InSchoolStatus'] ==  CARD_STATUS_OUTGOING) { // no duty for that time slot
						// not in school
						$ReturnArray['SlotName'] = $TodayFirstDuty['SlotName'];
						$ReturnArray['SlotStatus'] = $TodayFirstDuty['InSchoolStatus'];
						$ReturnArray['StaffStatus'] = "NotInSchool";
						$ReturnArray['Reason'] = $TodayFirstDuty['InReason'];
					}
					else if ($TodayFirstDuty['InSchoolStatus'] ==  CARD_STATUS_HOLIDAY) { // no duty for that time slot
						// not in school
						$ReturnArray['SlotName'] = $TodayFirstDuty['SlotName'];
						$ReturnArray['SlotStatus'] = $TodayFirstDuty['InSchoolStatus'];
						$ReturnArray['StaffStatus'] = "NotInSchool";
						$ReturnArray['Reason'] = $TodayFirstDuty['InReason'];
					}
	  			else if ($TodayFirstDuty['InSchoolStatus'] != CARD_STATUS_ABSENT && trim($TodayFirstDuty['InSchoolStatus']) != "") {
	  				// at school
						$ReturnArray['SlotName'] = $TodayFirstDuty['SlotName'];
						$ReturnArray['SlotStatus'] = $TodayFirstDuty['InSchoolStatus'];
						$ReturnArray['StaffStatus'] = "InSchool";
						$ReturnArray['LatestRecordTime'] = $TodayFirstDuty['InTime'];
						$ReturnArray['Reason'] = $TodayFirstDuty['InReason'];
	  			}
	  			else {
	  				// not in school
						$ReturnArray['SlotName'] = $TodayFirstDuty['SlotName'];
						$ReturnArray['SlotStatus'] = $TodayFirstDuty['OutSchoolStatus'];
						$ReturnArray['StaffStatus'] = "NotInSchool";
						$ReturnArray['Reason'] = $TodayFirstDuty['InReason']!=''? $TodayFirstDuty['InReason'] : $TodayFirstDuty['OutReason'];
	  			}
	  		}
	  	}
	  	else {
	  		// loop for suitable time slot for tap card time
	  		for ($i=0; $i< sizeof($SlotTimeArray); $i++) {
	  			if ($SlotTimeArray[$i]['Timestamp'] <= $current_time && $SlotTimeArray[($i+1)]['Timestamp'] >= $current_time) {
	  				if ($SlotTimeArray[$i]['SlotName'] == $SlotTimeArray[($i+1)]['SlotName']) { // inner tap card record within one time slot
	  					$SlotDuty = $Duty['CardSlotToDuty'][$SlotTimeArray[$i]['SlotName']][0];
	  					if ($SlotDuty['InSchoolStatus'] ==  CARD_STATUS_OUTGOING) { // no duty for that time slot
	  						// check raw log
	  						$ReturnArray['SlotName'] = $SlotDuty['SlotName'];
	  						$ReturnArray['SlotStatus'] = CARD_STATUS_OUTGOING;
	  						$ReturnArray['StaffStatus'] = "NotInSchool";
	  						$ReturnArray['Reason'] = $SlotDuty['InReason'];
	  						break;
	  					}
	  					else if ($SlotDuty['InSchoolStatus'] ==  CARD_STATUS_HOLIDAY) { // no duty for that time slot
	  						// check raw log
	  						$ReturnArray['SlotName'] = $SlotDuty['SlotName'];
	  						$ReturnArray['SlotStatus'] = CARD_STATUS_HOLIDAY;
	  						$ReturnArray['StaffStatus'] = "NotInSchool";
	  						$ReturnArray['Reason'] = $SlotDuty['InReason'];
	  						break;
	  					}
	  					else {
	  						if ($SlotDuty['InSchoolStatus'] != CARD_STATUS_ABSENT && trim($SlotDuty['InSchoolStatus']) != "") {
	  							if ($SlotDuty['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE) {
	  								// not in school
	  								$ReturnArray['SlotName'] = $SlotDuty['SlotName'];
			  						$ReturnArray['SlotStatus'] = CARD_STATUS_EARLYLEAVE;
			  						$ReturnArray['StaffStatus'] = "NotInSchool";
			  						$ReturnArray['LatestRecordTime'] = $SlotDuty['OutTime'];
			  						$ReturnArray['Reason'] = $SlotDuty['OutReason'];
	  								break;
	  							}
	  							else {
	  								// at school
	  								$ReturnArray['SlotName'] = $SlotDuty['SlotName'];
			  						$ReturnArray['SlotStatus'] = $SlotDuty['InSchoolStatus'];
			  						$ReturnArray['StaffStatus'] = "InSchool";
			  						$ReturnArray['LatestRecordTime'] = $SlotDuty['InTime'];
			  						$ReturnArray['Reason'] = $SlotDuty['OutReason']!=''? $SlotDuty['OutReason'] : $SlotDuty['InReason'];
	  								break;
	  							}
	  						}
	  						else {
	  							// not in school
	  							$ReturnArray['SlotName'] = $SlotDuty['SlotName'];
		  						$ReturnArray['SlotStatus'] = "";
		  						$ReturnArray['StaffStatus'] = "NotInSchool";
		  						$ReturnArray['LatestRecordTime'] = $SlotDuty['OutTime']!=''? $SlotDuty['OutTime'] : $SlotDuty['InTime'];
		  						$ReturnArray['Reason'] = $SlotDuty['InReason']!=''? $SlotDuty['InReason'] : $SlotDuty['OutReason'];
	  							break;
	  						}
	  					}
	  				}
	  				else { // tap card record between two time slot
	  					if ($SlotTimeArray[$i]['IsYesterDay'] == 1) 
	  						$PrevSlotDuty = $YesterdayDuty['CardSlotToDuty'][$SlotTimeArray[$i]['SlotName']][0];
	  					else
	  						$PrevSlotDuty = $Duty['CardSlotToDuty'][$SlotTimeArray[$i]['SlotName']][0];
	  					$NextSlotDuty = $Duty['CardSlotToDuty'][$SlotTimeArray[($i+1)]['SlotName']][0];
	  					if ($NextSlotDuty['InSchoolStatus'] != CARD_STATUS_ABSENT && trim($NextSlotDuty['InSchoolStatus']) != "") {
	  						// at school
	  						$ReturnArray['SlotName'] = $NextSlotDuty['SlotName'];
	  						$ReturnArray['SlotStatus'] = $NextSlotDuty['InSchoolStatus'];
	  						$ReturnArray['StaffStatus'] = "InSchool";
	  						$ReturnArray['LatestRecordTime'] = $NextSlotDuty['InTime'];
	  						$ReturnArray['Reason'] = $NextSlotDuty['InReason'];
	  						break;
	  					}
	  					else {
	  						// not in school
	  						$ReturnArray['SlotName'] = $NextSlotDuty['SlotName'];
	  						$ReturnArray['SlotStatus'] = $NextSlotDuty['InSchoolStatus'];
	  						$ReturnArray['StaffStatus'] = "NotInSchool";
	  						$ReturnArray['LatestRecordTime'] = $NextSlotDuty['OutTime'];
	  						$ReturnArray['Reason'] = $NextSlotDuty['InReason']!=''? $NextSlotDuty['InReason'] : $NextSlotDuty['OutReason'];
	  						break;
	  					}
	  				}
	  			}
	  		}
	  		
	  		if ($ReturnArray['StaffStatus'] == "") {
		  		// if below code ran, i means that the tap card is after the last out time
		  		// update last time slot out status to NORMAL
		  		// clear out profile record for time slot
		  		$LastDuty = $Duty['CardSlotToDuty'][$SlotTimeArray[(sizeof($SlotTimeArray)-1)]['SlotName']][0];
				  if ($LastDuty['InSchoolStatus'] ==  CARD_STATUS_OUTGOING) { // no duty for that time slot
						// not in school
						$ReturnArray['SlotName'] = $LastDuty['SlotName'];
						$ReturnArray['SlotStatus'] = $LastDuty['InSchoolStatus'];
						$ReturnArray['StaffStatus'] = "NotInSchool";
						$ReturnArray['Reason'] = $LastDuty['InReason'];
					}
					else if ($LastDuty['InSchoolStatus'] ==  CARD_STATUS_HOLIDAY) { // no duty for that time slot
						// not in school
						$ReturnArray['SlotName'] = $LastDuty['SlotName'];
						$ReturnArray['SlotStatus'] = $LastDuty['InSchoolStatus'];
						$ReturnArray['StaffStatus'] = "NotInSchool";
						$ReturnArray['Reason'] = $LastDuty['InReason'];
					}
					else {
						if ($LastDuty['InSchoolStatus'] != CARD_STATUS_ABSENT && trim($LastDuty['InSchoolStatus']) != "") {
							if ($LastDuty['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE || ($LastDuty['OutSchoolStatus'] == CARD_STATUS_NORMAL && $LastDuty['OutSchoolStatus'] != "")) {
								// not in school
								$ReturnArray['SlotName'] = $LastDuty['SlotName'];
								$ReturnArray['SlotStatus'] = $LastDuty['OutSchoolStatus'];
								$ReturnArray['StaffStatus'] = "NotInSchool";
								$ReturnArray['LatestRecordTime'] = $LastDuty['OutTime'];
								$ReturnArray['Reason'] = $LastDuty['OutReason'];
							}
							else {
								// at school
								$ReturnArray['SlotName'] = $LastDuty['SlotName'];
								$ReturnArray['SlotStatus'] = $LastDuty['InSchoolStatus'];
								$ReturnArray['StaffStatus'] = "InSchool";
								$ReturnArray['LatestRecordTime'] = $LastDuty['InTime'];
								$ReturnArray['Reason'] = $LastDuty['InReason'];
							}
						}
						else {
							// not in school
							$ReturnArray['SlotName'] = $LastDuty['SlotName'];
							$ReturnArray['SlotStatus'] = $LastDuty['InSchoolStatus'];
							$ReturnArray['StaffStatus'] = "NotInSchool";
							$ReturnArray['LatestRecordTime'] = $LastDuty['OutTime']!=''?$LastDuty['OutTime']:$LastDuty['InTime'];
							$ReturnArray['Reason'] = $LastDuty['InReason']!=''? $LastDuty['InReason'] : $LastDuty['OutReason'];
						}
					}
				}
			}
			
			if($InSchoolInOutStatus == '2') {
				if(isset($UserIdToRawLogTime[$UserList[$j]['UserID']])) {
					//$ReturnArray['LatestRecordTime'] = implode(" | ",$UserIdToRawLogTime[$UserList[$j]['UserID']]);
					$ReturnArray['LatestRecordTime'] = $UserIdToRawLogTime[$UserList[$j]['UserID']][count($UserIdToRawLogTime[$UserList[$j]['UserID']])-1];
				}
				if(isset($UserIdToRawLogTime[$UserList[$j]['UserID']]) && count($UserIdToRawLogTime[$UserList[$j]['UserID']])>0){ 
					if ((count($UserIdToRawLogTime[$UserList[$j]['UserID']]) % 2)!=0 ){
						$ReturnArray['StaffStatus'] = "InSchool";
					}else {
						$ReturnArray['StaffStatus'] = "NotInSchool";
					}
				}
			}
			
			$StaffStatus[] = $ReturnArray;
			usleep(1000);
	  }
						
		
		return $StaffStatus;
	}
	
	function Finalize_Attendance_Offline_Import() {
		$sql = "SELECT DISTINCT RecordDate FROM TEMP_CARD_STAFF_LOG";
		$temp = $this->returnArray($sql);
		$RecordDate = $temp[0]['RecordDate'];
		$ts = strtotime($RecordDate);
		$year = date('Y',$ts);
		$month = date('m',$ts);
		$day = date('j',$ts);
		//$weekday = date('w', $ts);
		
		$DailyLogTable = $this->createTable_Card_Staff_Attendance2_Daily_Log($year, $month);
		$RawLogTable = $this->buildStaffRawLogMonthTable($year, $month);
		
		$sql = 'Select UserID from TEMP_CARD_STAFF_LOG group by UserID';
		$TempUserIDs = $this->returnVector($sql);
		
		// get current exists RAW log
		$sql = "select 
							UserID,
							RecordTime as RecordedTime,
							RecordStation as SiteName
						from 
							".$RawLogTable." 
						where 
							DayNumber = '".$day."' 
							AND 
							UserID in (".implode(',',$TempUserIDs).")";
		$CurRawLog = $this->returnArray($sql);
		
		// get existing daily log in times and out times
		$sql = "SELECT StaffID,InTime,InSchoolStation,OutTime,OutSchoolStation FROM $DailyLogTable WHERE DayNumber = '".$day."' AND StaffID IN (".implode(',',$TempUserIDs).")";
		$daily_log_records = $this->returnResultSet($sql);
		
		$sql = 'insert into TEMP_CARD_STAFF_LOG (
							UserID,
							RecordDate,
							RecordedTime,
							SiteName
						)
						Values ';
		for ($i=0; $i< sizeof($CurRawLog); $i++) {
			$sql .= "('".$CurRawLog[$i]['UserID']."',
								'".$RecordDate."',
								'".$RecordDate." ".$CurRawLog[$i]['RecordedTime']."',
								'".$this->Get_Safe_Sql_Query($CurRawLog[$i]['SiteName'])."'),";
		}
		// merge manual input times to import times
		for($i=0;$i<count($daily_log_records);$i++){
			$tmp_staff_id = $daily_log_records[$i]['StaffID'];
			$tmp_in_time = $daily_log_records[$i]['InTime'];
			$tmp_out_time = $daily_log_records[$i]['OutTime'];
			$tmp_in_station = $daily_log_records[$i]['InSchoolStation'];
			$tmp_out_station = $daily_log_records[$i]['OutSchoolStation'];
			
			if($tmp_in_time != ''){
				$sql .= "('$tmp_staff_id','$RecordDate','$RecordDate $tmp_in_time','".$this->Get_Safe_Sql_Query($tmp_in_station)."'),";
			}
			if($tmp_out_time != ''){
				$sql .= "('$tmp_staff_id','$RecordDate','$RecordDate $tmp_out_time','".$this->Get_Safe_Sql_Query($tmp_out_station)."'),";
			}
		}
		
		$sql = substr($sql,0,-1);
		$Result['UnionRawLogAndImportTemp'] = $this->db_db_query($sql);		
		
		// remove exists RAW log
		$sql = "delete from ".$RawLogTable." 
						where 
							DayNumber = '".$day."' 
							and 
							UserID in (".implode(',',$TempUserIDs).")";
		$Result['RemoveCurRawLog'] = $this->db_db_query($sql);
		
		// cache old late/absent/ early Profile record
		$sql = "select 
							pro.StaffID,
							dailylog.SlotName,
							pro.RecordType,
							pro.ReasonID,
							pro.Waived  
						From 
							CARD_STAFF_ATTENDANCE2_PROFILE as pro 
							LEFT JOIN 
							".$DailyLogTable." as dailylog 
							on 
								pro.RecordID = dailylog.InAttendanceRecordID 
								or 
								pro.RecordID = dailylog.OutAttendanceRecordID 
						where 
							pro.RecordDate = '".$RecordDate."' 
							and 
							pro.StaffID  in (".implode(',',$TempUserIDs).") 
							and 
							pro.RecordType not in (".CARD_STATUS_NOSETTING.",".CARD_STATUS_HOLIDAY.",".CARD_STATUS_OUTGOING.") 
							and 
							dailylog.SlotName IS NOT NULL
							";
		$CachedProfileRecord = $this->returnArray($sql);
							
		// reset Daily Log Record
		$sql = 'update '.$DailyLogTable.' set 
							InSchoolStatus = NULL,
							OutSchoolStatus = NULL,
							InTime = NULL,
							OutTime = NULL,
							MinLate = NULL,
							MinEarlyLeave = NULL,
							InAttendanceRecordID = NULL,
							OutAttendanceRecordID = NULL,
							InWaived = NULL,
							OutWaived = NULL 
						where 
							InSchoolStatus not in ('.CARD_STATUS_NOSETTING.','.CARD_STATUS_HOLIDAY.','.CARD_STATUS_OUTGOING.') 
							and 
							DayNumber = \''.$day.'\' 
							and 
							StaffID in ('.implode(',',$TempUserIDs).')';
		$Result['ResetDailyLogStatus'] = $this->db_db_query($sql);
		
		// remove Late/ Absent/ Early Leave Profile
		$sql = "delete from CARD_STAFF_ATTENDANCE2_PROFILE 
						where 
							RecordDate = '".$RecordDate."' 
							and 
							RecordType not in (".CARD_STATUS_NOSETTING.",".CARD_STATUS_HOLIDAY.",".CARD_STATUS_OUTGOING.") 
							and 
							StaffID in (".implode(',',$TempUserIDs).")";
		$Result['DeleteOldProfile'] = $this->db_db_query($sql);					
		
		// remove OT records 
		$sql = "delete from CARD_STAFF_ATTENDANCE2_OT_RECORD 
						where 
							StaffID in (".implode(',',$TempUserIDs).") 
							and 
							RecordDate = '".$RecordDate."' ";
		$Result['DeleteOTRecord'] = $this->db_db_query($sql);
		
		// get Records unioned from CSV import file/ Original Raw LOG 
		$sql = "SELECT 
					DISTINCT 
					UserID,
					RecordedTime,
					SiteName 
				FROM
					TEMP_CARD_STAFF_LOG 
				ORDER BY
					UserID, RecordedTime ASC";
		$import_records = $this->returnArray($sql, 3);
		// simulate tap card record again
		for($i=0;$i<sizeof($import_records);$i++)
		{
			list($StaffID, $RecordedTime, $SiteName) = $import_records[$i];
			$this->Record_Tap_Card($StaffID,strtotime($RecordedTime),$SiteName,true);
		}
		
		for ($i=0; $i< sizeof($CachedProfileRecord); $i++) {
			List($StaffID,$SlotName,$RecordType,$ReasonID,$Waived)  = $CachedProfileRecord[$i];
			if ($RecordType == CARD_STATUS_LATE || $RecordType == CARD_STATUS_ABSENT) {
				$WaiveFieldName = "InWaived";
				$StatusField = "InSchoolStatus";
				$ProfileIDFieldName = "InAttendanceRecordID";
			}
			else { // early leave
				$WaiveFieldName = "OutWaived";
				$StatusField = "OutSchoolStatus";
				$ProfileIDFieldName = "OutAttendanceRecordID";
			}
			$sql = "select 
								RecordID as CardLogID, 
								".$ProfileIDFieldName." as ProfileID 
							from 
								".$DailyLogTable." 
							where 
								DayNumber = '".$day."' 
								and 
								SlotName = '".$this->Get_Safe_Sql_Query($SlotName)."' 
								and 
								StaffID = '".$StaffID."' 
								and 
								".$StatusField." = '".$RecordType."'";
			$ProfileFound = $this->returnArray($sql);
			
			if (sizeof($ProfileFound) > 0) { // record reason if same slot found same status
				$sql = "update ".$DailyLogTable." set 
									".$WaiveFieldName." = '".$Waived."' 
								where 
									RecordID = '".$ProfileFound[0]['CardLogID']."'";
				$Result['RecoverDailyLog:StaffID-'.$StaffID.':SlotName-'.$SlotName] = $this->db_db_query($sql);
				
				$sql = "update CARD_STAFF_ATTENDANCE2_PROFILE set 
									ReasonID = '".$ReasonID."' ,
									Waived = '".$Waived."' 
								where 
									RecordID = '".$ProfileFound[0]['ProfileID']."'";
				$Result['RecoverProfileLog:StaffID-'.$StaffID.':SlotName-'.$SlotName] = $this->db_db_query($sql);
			}
		}
		
		$sql = "UPDATE ".$DailyLogTable." SET ModifyBy='".$_SESSION['UserID']."' 
				WHERE InSchoolStatus NOT IN (".CARD_STATUS_NOSETTING.",".CARD_STATUS_HOLIDAY.",".CARD_STATUS_OUTGOING.") AND DayNumber = '$day' AND StaffID IN (".implode(",",$TempUserIDs).") AND (InTime IS NOT NULL OR OutTime IS NOT NULL)";
		$Result['UpdateDailyLogModifyBy'] = $this->db_db_query($sql);
		
		return $Result;
	}
	
	function IS_ADMIN_USER($ParUserID='')
	{
		global $intranet_root, $intranet_version;
		
		if(!$ParUserID) $ParUserID = $_SESSION['UserID'];

		$AdminUser = $this->GET_ADMIN_USER();
		$IsAdmin = 0;
		if(!empty($AdminUser))
		{
			$AdminArray = explode(",", $AdminUser);
			$IsAdmin = (in_array($ParUserID, $AdminArray)) ? 1 : 0;
		}
			
		return ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] || $IsAdmin);		
	}
	
	function Get_All_Repeated_Day($startUTC,$endUTC,$rule){
		$days = array();
		$start = strtotime($startUTC);
		$end = strtotime($endUTC);
		
		//echo date("Y-m-d", $start).' '.date("Y-m-d", $end).'<br>';
		$interval = empty($rule["interval"])?1:$rule["interval"];
		$weekName = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
		switch ($rule["freq"]){
			case "DAILY":
				//echo "start: ".date("Y-m-d H:i:s",$start)." end: ".date("Y-m-d H:i:s",$start);
				while ($end >= $start){
					$days[] = date("Y-m-d",$start);
					$start = strtotime("+".$interval." day",$start);
				}
			break;
			case "WEEKLY":
				$weeks = $rule["byDay"];
				$start = strtotime("-1 day",$start);
				$continue = true;				
				if (empty ($weeks))
					return false;
				while (true){
					foreach($weeks as $week){
						$temp = strtotime("next ".$weekName[$week],$start);
						
						if ($temp <= $end)
							$days[] = date("Y-m-d",$temp);
						else{
							$continue = false;
							break;
						}
					}
					
					if (!$continue)
						break;
					$start = strtotime("+".$interval." week",$start);
				}
			break;
			case "MONTHLY":				
				if ($rule["RepeatBy"] != "dayOfMonth"){
					//week of month
					$prefix = "";
					$suffix = "";
					$match = Array("1"=>"first","2"=>"second","3"=>"third","4"=>"fourth");
					if ($rule["byDay"]{0} == '-'){
						$temp = date("Y-n-j-G-i-s",$start);						
						$timeEle = explode("-",$temp);
						$suffix = $weekName[date('w',$start)];
						$start = mktime($timeEle[3],$timeElee[4]{0}==0?$timeEle[4]{1}:$timeEle[4],$timeEle[5]{0}==0?$timeEle[5]{1}:$timeEle[5],$timeEle[1],1,$timeEle[0]);
						
						$start = strtotime("+1 month",$start);
						
						$prefix = "last";
					}
					else{
						$prefix = $match[$this->Get_Week_Of_Month($start)];
						$suffix = $weekName[date("w",$start)]; 
						$start = mktime(0,0,0,date("n",$start),1,date("Y",$start));
					}
					while(true){
						$temp = strtotime($prefix." ".$suffix,strtotime('-1 day',$start));
						if ($temp > $end)
							break;
						$days[] = date("Y-m-d",$temp);
						$start = strtotime("+".$interval." month",$start);
					}
				}
				else {
					while ($end > $start){
						$days[] = date("Y-m-d",$start);
						$start = strtotime("+".$interval." month",$start);
					}
				}
				
			break;
			case "YEARLY":
				while ($end >= $start){
					$days[] = date("Y-m-d",$start);
					$start = strtotime("+".$interval." year",$start);
				}
			break;
		};
		return $days;
	}
	
	function Get_Week_Of_Month($Stamp) 
	{ 
	    return ceil( date( 'j',$Stamp) / 7 ); 
	} 
	
	function Import_Leave_Record($Data)
	{
		global $Lang;
		$Error = array();
		$Result = array();
		if(!is_array($Data))
		{
			return array();
		}
		
		$sql = 'DROP TABLE TEMP_IMPORT_STAFF_LEAVE_RECORD';
		$this->db_db_query($sql);
		$sql = 'CREATE TABLE TEMP_IMPORT_STAFF_LEAVE_RECORD (
							UserID varchar(20) default NULL,
						  LeaveType int(1) default NULL,
						  LeaveDate Date default NULL,
						  SlotID varchar(150) default NULL,
						  ReasonID varchar(150) default NULL,
						  UNIQUE KEY (UserID,LeaveDate,SlotID) 
						) ENGINE=InnoDB DEFAULT CHARSET=utf8
					 ';
		$this->db_db_query($sql);
		//debug_r($sql);
		
		$this->Start_Trans();
		$TodayTimeStamp = strtotime(date("Y-m-d"));
		$NameField = getNameFieldByLang("a.");
		$values_array = array();
		for($i=0;$i<sizeof($Data);$i++)
		{
			list($UserLogin,$LeaveType,$LeaveDate,$TimeSlot,$Reason) = $Data[$i];
			$UserLogin = trim($UserLogin);
			$TimeSlot = trim($TimeSlot);
			$Reason = trim($Reason);
			
			// is user found
			$sql = "SELECT
						a.UserID
					FROM
						INTRANET_USER as a
					WHERE
						a.RecordType = '1'
				  	AND a.RecordStatus = '1'
						AND a.UserLogin = '".$this->Get_Safe_Sql_Query($UserLogin)."' ";			
			//debug_r($sql);
			$UserInfo = $this->returnVector($sql);
			if(sizeof($UserInfo) == 0)
			{
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['StaffNotExists']);
				continue;
				//break;
			}
			$StaffID = $UserInfo[0];
			
			// is date format valid
			if (!checkDateIsValid($LeaveDate)) {
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['General']['InvalidDateFormat']);
				continue;
			}
			
			// is date is future
			if (strtotime($LeaveDate) <= $TodayTimeStamp) {
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['TargetDateMustBeAfterTodayWarning']);
				continue;
			}
			
			// is leave type correct
			if ($LeaveType != 1 && $LeaveType != 2) {
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['InvalidLeaveType']);
				continue;
			}
			
			// convert to system leave type
			$LeaveType = ($LeaveType == "1")? 4:5;
			
			// check reason if is not empty
			$ReasonID = "";
			if ($Reason != "") {
				$sql = "select 
									ReasonID 
								from 
									CARD_STAFF_ATTENDANCE3_REASON 
								where 
									ReasonType = '".$LeaveType."' 
									and 
									ReasonActive = 1 
									and 
									ReasonText = '".$this->Get_Safe_Sql_Query($Reason)."'";
				$ReasonInfo = $this->returnVector($sql);
				if (sizeof($ReasonInfo) == 0) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['InvalidReason']);
					continue;
				}
				else {
					$ReasonID = $ReasonInfo[0];
				}
			}
			
			// check slot on duty if is not empty
			$SlotID = "";
			if ($TimeSlot != "") {
				// check is staff in group
				$sql = "select 
									GroupID 
								from 
									CARD_STAFF_ATTENDANCE_USERGROUP 
								where 
									UserID = '".$StaffID."'";
				$GroupInfo = $this->returnVector($sql);
				if (sizeof($GroupInfo) > 0) {
					$GroupOrIndividual = "G";
					$ID = $GroupInfo[0];
					$IDField = 'GroupID';
				}
				else {
					$GroupOrIndividual = "I";
					$ID = $StaffID;
					$IDField = 'UserID';
				}
				
				// check if slot exists
				$sql = "select 
									SlotID 
								from 
									CARD_STAFF_ATTENDANCE3_SLOT 
								where 
									(
										".$IDField." = '".$ID."' 
										OR
										TemplateID IS NOT NULL 
									)
									and 
									SlotName = '".$this->Get_Safe_Sql_Query($TimeSlot)."'";
				//debug_r($sql);
				$SlotInfo = $this->returnVector($sql);
				if (sizeof($SlotInfo) == 0) {
					$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['InvalidTimeSlotName']);
					continue;
				}
				else {
					$UserDuty = $this->Get_Duty_By_Date($StaffID,$LeaveDate);
					$SlotIDFound = false;
					foreach ($SlotInfo as $Key => $TempSlotID) {
						if (in_array($TempSlotID,$UserDuty['SlotIDList'])) {
							$SlotIDFound = true;
							$SlotID = $TempSlotID;
						}
					}
					
					// can't found slot on target date's duty
					if (!$SlotIDFound) {
						$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['CannotFoundSlotOnTargetDate']);
						continue;
					}
				}
			}
			
			// check if there is duplicate import
			$sql = "select 
								count(1) 
							from 
								TEMP_IMPORT_STAFF_LEAVE_RECORD 
							where 
								UserID = '".$StaffID."' 
								and 
								LeaveDate = '".$LeaveDate."' 
								";
			if ($SlotID != "") {
				$sql .="and 
								(
									SlotID = '".$SlotID."' 
									OR 
									SlotID IS NULL
								)";
			}
			$ImportedBefore = $this->returnVector($sql);
			if ($ImportedBefore[0] > 0) {
				$Error[] = array("RecordDetail"=>$Data[$i],"RowNumber"=>($i+2),"ErrorMsg"=>$Lang['StaffAttendance']['DuplicateImportLeave']);
				continue;
			}
			
			$sql = "insert into TEMP_IMPORT_STAFF_LEAVE_RECORD (
								UserID,
							  LeaveType,
							  LeaveDate,
							  SlotID,
							  ReasonID)
							values (
								'".$StaffID."',
								'".$LeaveType."',
								'".$LeaveDate."',
								'".$SlotID."',
								'".$ReasonID."'
								)";
			$Result['InsertTempLeave:StaffID-'.$StaffID.':LeaveDate-'.$LeaveDate.':LeaveType-'.$LeaveType.':SlotID-'.$SlotID] = $this->db_db_query($sql);
		}
		
		//debug_r($Result);
		if (in_array(false,$Result)) {
			$this->RollBack_Trans();
		}
		else {
			$this->Commit_Trans();
		}
		
		return $Error;
	}
	
	function Finalize_Import_Leave_Record()
	{
		$sql = "SELECT 
							UserID, 
							LeaveType, 
							LeaveDate, 
							SlotID, 
							ReasonID 
						FROM 
							TEMP_IMPORT_STAFF_LEAVE_RECORD";
		$TempResult = $this->returnArray($sql);
		$values_array=array();
		for ($i=0; $i< sizeof($TempResult); $i++)
		{
			list($StaffID,$LeaveType,$LeaveDate,$SlotID,$ReasonID) = $TempResult[$i];
			
			$Outgoing = array();
			$Holiday = array();
			$HolidayReason = array();
			$OutgoingReason = array();
			if ($SlotID != "") {
				if ($LeaveType == CARD_STATUS_OUTGOING) {
					$Outgoing[] = $SlotID;
					$OutgoingReason[$SlotID] = $ReasonID;
				}
				if ($LeaveType == CARD_STATUS_HOLIDAY) {
					$Holiday[] = $SlotID;
					$HolidayReason[$SlotID] = $ReasonID;
				}
			}
			$Result['InsertLeaveRecord-StaffID:'.$StaffID.'-LeaveDate:'.$LeaveDate.'-SlotID:'.$SlotID] = $this->Save_Full_Day_Setting($StaffID,$LeaveType,$LeaveDate,$LeaveDate,$ReasonID,($SlotID == ""? "Full":"Slot"),$Outgoing,$Holiday,$OutgoingReason,$HolidayReason,$LeaveDate);
			
			/*
			$sql = "select 
								RecordID 
							from 
								CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
							where 
								StaffID = '".$StaffID."' 
								and 
								RecordDate = '".$LeaveDate."' ";
			if ($SlotID != "") {
				$sql .= "
								and 
								SlotID = '".$SlotID."'";
			}
			$Temp = $this->returnVector($sql);
			if (sizeof($Temp) > 0) {
				$sql = "delete from CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
								where 
									RecordID in (".implode(',',$Temp).")
							 ";
				$Result['RemoveRecordToBeReplaced:'.implode(',',$Temp)] = $this->db_db_query($sql);
			}
			$sql = "insert into CARD_STAFF_ATTENDANCE2_LEAVE_RECORD (
								StaffID,
								RecordDate,
								SlotID,
								ReasonID,
								RecordType,
								DateInput,
								InputBy,
								DateModified,
								ModifyBy 
								) 
							values (
								'".$StaffID."',
								'".$LeaveDate."',
								".(($SlotID == "")? "NULL":"'".$SlotID."'").",
								'".$ReasonID."',
								'".$LeaveType."',
								NOW(),
								'".$_SESSION['UserID']."',
								NOW(),
								'".$_SESSION['UserID']."'
								)";
			$Result['InsertLeaveRecord-StaffID:'.$StaffID.'-LeaveDate:'.$LeaveDate.'-SlotID:'.$SlotID] = $this->db_db_query($sql);
			*/
		}
		
		return sizeof($TempResult);
	}
	
	function Get_School_Event_By_Date_Range($StartDate,$EndDate,$Keyword="",$EventType="4", $allow_past_date=false) {
		$sql = "select 
							EventDate,
							Title,
							Description, 
							RecordType 
						from 
							INTRANET_EVENT 
						where ";
		if($allow_past_date == false) {
			$sql .= " EventDate > '" . date('Y-m-d') . "' 
							and ";
		}

		$sql .= " EventDate Between '".$StartDate."' and '".$EndDate."' 
							and 
							RecordType = '".$EventType."'";
		if (trim($Keyword) != "") {
			$sql .= " 
							and 
							Title like '%".$this->Get_Safe_Sql_Like_Query(trim($Keyword))."%'";
		}
		$sql .= " order by 
								EventDate, RecordType, Title";
		//debug_r($sql);
		
		return $this->returnArray($sql);
	}
	
	function Get_Profile_Status_List($TargetDate, $RecordType) {
		global $Lang;
		
		$Temp = explode("-",$TargetDate);
		$TargetYear = $Temp[0];
		$TargetMonth = $Temp[1];
		$TargetDay = $Temp[2] + 0;
		$ProfileJoin = "INNER";
		$WhereCond = "WHERE u.RecordStatus='1' AND u.RecordType='1' AND dl.DayNumber='".$TargetDay."' ";
		if ($RecordType == CARD_STATUS_ABSENT) {
			$JoinCond = "AND dl.InAttendanceRecordID = pro.RecordID ";
			$MinField = "'0' as StatusMin,";
		}
		else if ($RecordType == CARD_STATUS_LATE) {
			$JoinCond = "AND dl.InAttendanceRecordID = pro.RecordID ";
			$MinField = "MinLate as StatusMin,";
		}
		else if ($RecordType == CARD_STATUS_EARLYLEAVE){ // early leave
			$JoinCond = "AND dl.OutAttendanceRecordID = pro.RecordID ";
			$MinField = "MinEarlyLeave as StatusMin,";
		}else {
			$JoinCond = "";
			$MinField = "'0' as StatusMin,";
			$ProfileJoin = "LEFT";
			$WhereCond = "WHERE u.RecordStatus='1' AND u.RecordType='1' AND dl.DayNumber='".$TargetDay."' AND dl.InSchoolStatus IS NULL AND dl.OutSchoolStatus IS NULL ";
		}
		$NameField = getNameFieldByLang('u.');
		$LastUpdateNameField = getNameFieldByLang('uu.');
		$sql = "select 
							dl.RecordID,
							pro.RecordID as ProfileID,
							dl.StaffID,
							".$NameField." as StaffName,
							dl.SlotName,
							dl.DutyStart,
							dl.DutyEnd,
							dl.DutyCount,
							dl.RecordStatus,
							g.GroupID,
							IF(g.GroupID IS NOT NULL,g.GroupName,'".$Lang['StaffAttendance']['Individual']."') as GroupName,
							IF(dl.InTime = '' OR dl.InTime IS NULL,'--',dl.InTime) as InTime,
							IF(dl.InSchoolStation = '' OR dl.InSchoolStation IS NULL,'--',dl.InSchoolStation) as InSchoolStation,
							IF(dl.OutTime = '' OR dl.OutTime IS NULL,'--',dl.OutTime) as OutTime,
							IF(dl.OutSchoolStation = '' OR dl.OutSchoolStation IS NULL,'--',dl.OutSchoolStation) as OutSchoolStation,
							".$MinField."
							pro.ReasonID,
							pro.Waived, 
							r.ReasonText,
							dl.Remark, 
							IF((dl.InSchoolStatus IS NOT NULL OR dl.OutSchoolStatus IS NOT NULL) AND (dl.DateModified IS NOT NULL OR dl.DateConfirmed IS NOT NULL),
							(CASE
							 WHEN dl.DateModified IS NULL
							   THEN dl.DateModified 
							 WHEN dl.DateConfirmed IS NULL 
							   THEN dl.DateModified 
							 ELSE IF(dl.DateModified > dl.DateConfirmed,dl.DateModified,dl.DateConfirmed)
							END),
							'-') as LastUpdated,
						  ".$LastUpdateNameField." as LastUpdater 
						from 
							CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$TargetYear."_".$TargetMonth." as dl 
							INNER JOIN 
							CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
							on dl.StaffID = wp.UserID 
								AND 
								'".$TargetDate."' between PeriodStart and PeriodEnd 
							$ProfileJoin JOIN 
							CARD_STAFF_ATTENDANCE2_PROFILE pro 
							on 
								dl.DayNumber = '".$TargetDay."' 
								AND 
								dl.StaffID = pro.StaffID 
								".$JoinCond." 
								AND 
								pro.RecordType = '".$RecordType."' 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE3_REASON r 
							on 
								pro.ReasonID = r.ReasonID 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE_USERGROUP ug 
							on 
								dl.StaffID = ug.UserID 
							LEFT JOIN 
							CARD_STAFF_ATTENDANCE2_GROUP g 
							on 
								ug.GroupID = g.GroupID 
							LEFT JOIN 
							INTRANET_USER u 
							on 
								dl.StaffID = u.UserID 
							LEFT JOIN 
							INTRANET_USER uu 
							on 
								IF(dl.DateModified IS NOT NULL AND dl.DateConfirmed IS NOT NULL,
									IF(dl.DateModified > dl.DateConfirmed,dl.ModifyBy = uu.UserID,dl.ConfirmBy = uu.UserID),
									IF(dl.ModifyBy IS NULL,dl.ConfirmBy = uu.UserID, dl.ModifyBy = uu.UserID)
									) 
							$WhereCond 
						order by 
							g.GroupName, dl.DutyStart, dl.SlotName, u.EnglishName 
						";
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Save_Profile_Set($RecordID,$Status,$Waived,$ReasonLate,$ReasonAbsent,$ReasonEarlyLeave,$Remark,$ProfileID,$RecordType,$TargetDate,$StaffID,$DutyCount) {
		$Temp = explode('-',$TargetDate);
		$TargetYear = $Temp[0];
		$TargetMonth = $Temp[1];
		$TargetDay = $Temp[2];
		$Result = array();
		for ($i=0; $i< sizeof($RecordID); $i++) {
			$StatusField = "";
			$WaivedField = "";
			$MoreFields = "";
			if($Status[$RecordID[$i]] == ''){
				// Absent Suspected
				continue;
			}else if ($Status[$RecordID[$i]] == CARD_STATUS_NORMAL) {
				if($ProfileID[$RecordID[$i]] != ''){
					$this->Remove_Profile($ProfileID[$RecordID[$i]]);
				}
				if ($RecordType == '' ||  $RecordType == CARD_STATUS_ABSENT || $RecordType == CARD_STATUS_LATE) {
					$StatusField = "InSchoolStatus = '".CARD_STATUS_NORMAL."', 
													InAttendanceRecordID = NULL, ";
				}
				else { // early leave
					$StatusField = "OutSchoolStatus = '".CARD_STATUS_NORMAL."', 
													OutAttendanceRecordID = NULL, ";
				}
			}else if($RecordType == '' && $Status[$RecordID[$i]] == CARD_STATUS_ABSENT){
				$newProfileID = $this->Create_Profile($TargetDate,$StaffID[$RecordID[$i]],$Waived[$RecordID[$i]],$DutyCount[$RecordID[$i]],$Status[$RecordID[$i]]);
				$sql = 'insert into CARD_STAFF_ATTENDANCE2_PROFILE 
							(
							StaffID,
							RecordDate,
							Waived,
							ProfileCountFor,
							ReasonID,
							RecordType,
							DateInput,
							DateModified
							)
						values 
							(
							\''.$this->Get_Safe_Sql_Query($StaffID[$RecordID[$i]]).'\',
							\''.$this->Get_Safe_Sql_Query($TargetDate).'\',
							\''.$this->Get_Safe_Sql_Query($Waived[$RecordID[$i]]).'\',
							\''.$this->Get_Safe_Sql_Query($DutyCount[$RecordID[$i]]).'\',
							\''.$this->Get_Safe_Sql_Query($ReasonAbsent[$RecordID[$i]]).'\',
							\''.$this->Get_Safe_Sql_Query($Status[$RecordID[$i]]).'\',
							NOW(),
							NOW()
							)';
				$this->db_db_query($sql);
				$newProfileID =$this->db_insert_id();
				$StatusField = "InSchoolStatus = '".CARD_STATUS_ABSENT."', 
									InAttendanceRecordID = '".$newProfileID."', ";
				$WaivedField = "InWaived = '".$this->Get_Safe_Sql_Query($Waived[$RecordID[$i]])."', ";
			}
			else {
				$StatusField = "";
				if ($RecordType == CARD_STATUS_ABSENT) 
					$ReasonID = ($ReasonAbsent[$RecordID[$i]] == "0")? "NULL":"'".$this->Get_Safe_Sql_Query($ReasonAbsent[$RecordID[$i]])."'";
				else if ($RecordType == CARD_STATUS_LATE) 
					$ReasonID = ($ReasonLate[$RecordID[$i]] == "0")? "NULL":"'".$this->Get_Safe_Sql_Query($ReasonLate[$RecordID[$i]])."'";
				else // early leave
					$ReasonID = ($ReasonEarlyLeave[$RecordID[$i]] == "0")? "NULL":"'".$this->Get_Safe_Sql_Query($ReasonEarlyLeave[$RecordID[$i]])."'";
				
				if ($RecordType == CARD_STATUS_ABSENT || $RecordType == CARD_STATUS_LATE) {
					$WaivedField = "InWaived = '".$this->Get_Safe_Sql_Query($Waived[$RecordID[$i]])."', ";
				}else{
					$WaivedField = "OutWaived = '".$this->Get_Safe_Sql_Query($Waived[$RecordID[$i]])."', ";
				}
					
				$sql = "update CARD_STAFF_ATTENDANCE2_PROFILE set 
									Waived = '".$this->Get_Safe_Sql_Query($Waived[$RecordID[$i]])."', 
									ReasonID = ".$ReasonID.", 
									DateModified = NOW() 
								where 
									RecordID = '".$this->Get_Safe_Sql_Query($ProfileID[$RecordID[$i]])."'";
				//debug_r($sql);
				$Result['UpdateProfileStatus:UserID-'.$StaffID[$RecordID[$i]].':ProfileID-'.$ProfileID[$RecordID[$i]].':UpdateReason'] = $this->db_db_query($sql);
			}
			
			if($Status[$RecordID[$i]] != CARD_STATUS_LATE){
				$MoreFields .= " MinLate=NULL,";
			}
			if($Status[$RecordID[$i]] != CARD_STATUS_EARLYLEAVE){
				$MoreFields .= " MinEarlyLeave=NULL,";
			}
			
			$sql = "update CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$TargetYear."_".$TargetMonth." set 
								".$StatusField." 
								".$WaivedField."
								".$MoreFields."
								Remark = '".$this->Get_Safe_Sql_Query($Remark[$RecordID[$i]])."', 
								DateModified = NOW(), 
								ModifyBy = '".$_SESSION['UserID']."', 
								RecordStatus = '1', 
								DateConfirmed = NOW(),
								ConfirmBy = '".$_SESSION['UserID']."' 
							where 
								RecordID = '".$this->Get_Safe_Sql_Query($RecordID[$i])."'";
			//debug_r($sql);
			$Result['UpdateDailyLogStatus:UserID-'.$StaffID[$RecordID[$i]].':RecordID-'.$RecordID[$i]] = $this->db_db_query($sql);
		}
		
		//debug_r($Result);
		return !in_array(false,$Result);
	}
	
	function Get_Doctor_Certificate_DBTable_Sql($TargetYear,$TargetMonth,$RecordType='',$StatusType=array(),$Keyword='',$FromiSmartCard=false)
	{
		global $Lang;
		$TargetYear = sprintf("%04d",$TargetYear);
		$TargetMonth = sprintf("%02d",$TargetMonth);
		$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$TargetYear."_".$TargetMonth;
		
		$COND_USERID = "";
		if($FromiSmartCard){
			$COND_USERID = " AND u.UserID='".$_SESSION['UserID']."' ";
		}
		if($RecordType==1){ // daily logs with Doctor Certificate
			$JOIN_BY = "INNER";
			$COND = "";
		}else if($RecordType==2){ // daily logs without Doctor Certificate
			$JOIN_BY = "LEFT";
			$COND = "AND f.FileName IS NULL";
		}else{
			$JOIN_BY = "LEFT";
			$COND = "";
		}
		$name_field = getNameFieldByLang("u.");
		$inputby_name = getNameFieldByLang("u2.");
		
		if($Keyword != ''){
			$SafeKeyword = $this->Get_Safe_Sql_Like_Query($Keyword);
			if($FromiSmartCard){
				$COND_KEYWORD = "AND (DATE_FORMAT(CONCAT('$TargetYear-$TargetMonth-',d.DayNumber),'%Y-%m-%d') LIKE '%".$SafeKeyword."%' 
									OR d.SlotName LIKE '%".$SafeKeyword."%'
									OR f.FileName LIKE '%".$SafeKeyword."%')";
			}else{
				$COND_KEYWORD = "AND (u.EnglishName LIKE '%".$SafeKeyword."%' 
									OR u.ChineseName LIKE '%".$SafeKeyword."%' 
									OR DATE_FORMAT(CONCAT('$TargetYear-$TargetMonth-',d.DayNumber),'%Y-%m-%d') LIKE '%".$SafeKeyword."%' 
									OR d.SlotName LIKE '%".$SafeKeyword."%'
									OR f.FileName LIKE '%".$SafeKeyword."%')";
			}
		}
		
		$COND_STATUS = '';
		$StatusArray = (array)$StatusType;
		$COND_STATUS .= " AND (d.InSchoolStatus IN ('".implode("','",$StatusArray)."') ";
		if(in_array(CARD_STATUS_EARLYLEAVE,$StatusArray)){
			$COND_STATUS .= " OR d.OutSchoolStatus='".CARD_STATUS_EARLYLEAVE."' ";
		}
		if(in_array('',$StatusArray)){
			$COND_STATUS .= " OR d.InSchoolStatus IS NULL ";
		}
		$COND_STATUS .= ") ";
		
		$sql = "SELECT 
					DATE_FORMAT(CONCAT('$TargetYear-$TargetMonth-',d.DayNumber),'%Y-%m-%d') as RecordDate,";
		if(!$FromiSmartCard){	
			$sql.= "$name_field as StaffName,";
		}
			$sql .= "IF(d.SlotName IS NULL,'Full Day',CONCAT(d.SlotName,'(',d.DutyStart,'-',d.DutyEnd,')')) as TimeSlot,
					(CASE
					  WHEN d.InSchoolStatus = '".CARD_STATUS_OUTGOING."' 
					  	THEN '".$Lang['StaffAttendance']['Outing']."' 
					  WHEN d.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' 
						THEN '".$Lang['StaffAttendance']['Holiday']."' 
					  WHEN d.InSchoolStatus = '".CARD_STATUS_LATE."' AND d.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."'
						THEN '".$Lang['StaffAttendance']['Late']." & ".$Lang['StaffAttendance']['EarlyLeave']."' 
					  WHEN d.InSchoolStatus = '".CARD_STATUS_LATE."' 
						THEN '".$Lang['StaffAttendance']['Late']."' 
					  WHEN d.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' 
						THEN '".$Lang['StaffAttendance']['EarlyLeave']."' 
					  WHEN d.InSchoolStatus = '".CARD_STATUS_ABSENT."' 
						THEN '".$Lang['StaffAttendance']['Absent']."'
					  WHEN d.InSchoolStatus = '".CARD_STATUS_NORMAL."' OR d.OutSchoolStatus = '".CARD_STATUS_NORMAL."'
						THEN '".$Lang['StaffAttendance']['Present']."' 
					  ELSE '".$Lang['StaffAttendance']['AbsentSuspected']."' 
					END) as Status,";
		if($FromiSmartCard){
			/*
			$sql .= "GROUP_CONCAT(
						CONCAT('<a href=\"/home/eAdmin/StaffMgmt/attendance/Management/DoctorCertificate/download.php?FileID=',f.FileID,'\" style=\"float:left;\">',
								REPLACE(REPLACE(REPLACE(f.FileName,'\'','&#039;'),'<','&lt;'),'>','&gt;'),' (',f.FileSize,' KB)',
								'</a>') SEPARATOR '<br style=\"clear:both;\">') as FileNames,
					IF(f.FileID IS NOT NULL,CONCAT(f.DateInput,'&nbsp;by&nbsp;',$inputby_name),'') as DateModifyUser ";
			*/
			$sql .= "GROUP_CONCAT(
						CONCAT(f.FileName,' (',f.FileSize,' KB)|',f.FileID) SEPARATOR '/') as FileNames,
					IF(f.FileID IS NOT NULL,CONCAT(f.DateInput,'&nbsp;by&nbsp;',$inputby_name),'') as DateModifyUser ";
		}else{
			/*
			$sql .= "GROUP_CONCAT(
						CONCAT('<a href=\"/home/eAdmin/StaffMgmt/attendance/Management/DoctorCertificate/download.php?FileID=',f.FileID,'\" style=\"float:left;\">',
								REPLACE(REPLACE(REPLACE(f.FileName,'\'','&#039;'),'<','&lt;'),'>','&gt;'),' (',f.FileSize,' KB)',
								'</a>','<span class=\"table_row_tool row_content_tool\" style=\"float:none;\">',
										'<a onclick=\"DeleteFileByID(',f.FileID,')\" title=\"".$Lang['Btn']['Delete']."\" class=\"delete_dim\" href=\"javascript:void(0);\"></a></span>') SEPARATOR '<br style=\"clear:both;\">') as FileNames,
					IF(f.FileID IS NOT NULL,CONCAT(f.DateInput,'&nbsp;by&nbsp;',$inputby_name),'') as DateModifyUser,
					CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',d.RecordID,'\" />') as Checkbox ";
			*/
			$sql .= "GROUP_CONCAT(
						CONCAT(f.FileName,' (',f.FileSize,' KB)|',f.FileID) SEPARATOR '/') as FileNames,
					IF(f.FileID IS NOT NULL,CONCAT(f.DateInput,'&nbsp;by&nbsp;',$inputby_name),'') as DateModifyUser,
					CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',d.RecordID,'\" />') as Checkbox ";
		}
		$sql .="FROM INTRANET_USER as u 
				INNER JOIN $card_log_table_name as d On d.StaffID=u.UserID 
				$JOIN_BY JOIN CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE as f ON f.DailyLogID=d.RecordID AND f.Year='".$TargetYear."' AND f.Month='".$TargetMonth."' 
				LEFT JOIN INTRANET_USER as u2 ON u2.UserID=f.InputBy 
				WHERE u.RecordStatus=1 and u.RecordType='".USERTYPE_STAFF."' $COND_USERID
					$COND 
					$COND_STATUS 
					$COND_KEYWORD 
				GROUP BY d.RecordID ";
		
		return $sql;
	}
	
	function Get_Doctor_Certificate_By_FileID($FileID)
	{
		$sql = "SELECT FileID,DailyLogID,FileName,FilePath,FileSize,Year,Month FROM CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE WHERE FileID='".$FileID."'";
		$result = $this->returnArray($sql);
		return $result[0];
	}
	
	function Is_Doctor_Certificate_File_Owner($FileID,$ParUserID='')
	{
		$record = $this->Get_Doctor_Certificate_By_FileID($FileID);
		$DailyLogID = $record['DailyLogID'];
		$Year = $record['Year'];
		$Month = $record['Month'];
		
		$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$Year."_".$Month;
		
		$sql = "SELECT 
					c.StaffID 
				FROM $card_log_table_name as c 
				INNER JOIN CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE as f ON f.DailyLogID=c.RecordID AND f.Year='".$Year."' AND f.Month='".$Month."' 
				WHERE f.FileID='".$FileID."' ";
		$tmp = $this->returnVector($sql);
		$StaffID = $tmp[0];
		
		if($ParUserID==''){
			$ParUserID = $_SESSION['UserID'];
		}
		return $StaffID == $ParUserID;
	}
	
	function Manage_Doctor_Certificate_Files($PostData,$FileData)
	{
		global $intranet_root;
		include_once("libfilesystem.php");
		
		$lf = new libfilesystem();
		
		$TargetYear = $PostData['TargetYear'];
		$TargetMonth = $PostData['TargetMonth'];
		$TargetYear = sprintf("%04d",$TargetYear);
		$TargetMonth = sprintf("%02d",$TargetMonth);
		//$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$TargetYear."_".$TargetMonth;
		$DailyLogID = $PostData['DailyLogID'];
		//$TmpDeleteFileID = $PostData['TmpDeleteFileID'];
		
		$numOfFiles = count($FileData['UploadFile']['name']);
		
		$staffattend_file_root = $intranet_root."/file/staff_attendance";
		if (!file_exists($staffattend_file_root))
			$lf->folder_new($staffattend_file_root);
		
		$staffattend_file_root .= "/doctor_certificate";
		if (!file_exists($staffattend_file_root))
			$lf->folder_new($staffattend_file_root);
		
		$Result = array();
		$uploadedFiles = array();
		for($i=0;$i<$numOfFiles;$i++){
			if($FileData['UploadFile']['name'][$i]=='')
			{
				continue;
			}
			if($FileData['UploadFile']['error'][$i]>0){ // has error
				$Result[] = false;
				continue;
			}else{
				$file_name = $FileData['UploadFile']['name'][$i];
				$tmp_file = $FileData['UploadFile']['tmp_name'][$i];
				$file_size = sprintf("%.2f",$FileData['UploadFile']['size'][$i] / 1024); // convert to KB
				$unique_id = md5($FileData['UploadFile']['name'][$i].time());
				$file_ext = $lf->file_ext($file_name);
				
				$renamed_filename = $unique_id.$file_ext;
				$full_file_path = $staffattend_file_root."/".$renamed_filename;
				$file_path = "/file/staff_attendance/doctor_certificate/".$renamed_filename;
				$Result['CopyFile'.$i] = $lf->file_copy($tmp_file,$full_file_path);
				if($Result['CopyFile'.$i]){
					$uploadedFiles[] = array('FileName'=>$file_name,
											'FilePath'=>$file_path,
											'UniqueID'=>$unique_id,
											'FileSize'=>$file_size);
				}
			}
		}
		$InputBy = $_SESSION['UserID'];
		$sql = "INSERT INTO CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE (DailyLogID,FileName,FilePath,UniqueID,FileSize,Year,Month,DateInput,InputBy) VALUES ";
		$values = "";
		$delimiter = "";
		for($i=0;$i<count($DailyLogID);$i++){
			for($j=0;$j<count($uploadedFiles);$j++){
				$file_name = $uploadedFiles[$j]['FileName'];
				$file_path = $uploadedFiles[$j]['FilePath'];
				$unique_id = $uploadedFiles[$j]['UniqueID'];
				$file_size = $uploadedFiles[$j]['FileSize'];
				$values .= $delimiter."('".$DailyLogID[$i]."','".$file_name."','".$file_path."','".$unique_id."','".$file_size."','".$TargetYear."','".$TargetMonth."',NOW(),'".$InputBy."')";
				$delimiter = ",";
			}
		}
		$sql .= $values;
		if($values != ''){
			$Result['InsertDB'] = $this->db_db_query($sql);
		}
		
		return !in_array(false,$Result);
	}
	
	function Delete_Doctor_Certificates($DailyLogID,$TargetYear,$TargetMonth)
	{
		global $intranet_root;
		include_once("libfilesystem.php");
		
		$lf = new libfilesystem();
		
		$TargetYear = sprintf("%04d",$TargetYear);
		$TargetMonth = sprintf("%02d",$TargetMonth);
		$success = array();
		if(is_array($DailyLogID) && count($DailyLogID)>0){
			$sql = "SELECT FileID,DailyLogID,UniqueID,FilePath FROM CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE WHERE DailyLogID IN (".implode(",",$DailyLogID).") AND Year='".$TargetYear."' AND Month='".$TargetMonth."'";
			$result = $this->returnArray($sql);
			
			$UniqueIDToFilePath = array();
			for($i=0;$i<count($result);$i++) {
				$UniqueIDToFilePath[$result[$i]['UniqueID']] = $result[$i]['FilePath'];
			}
			
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE WHERE DailyLogID IN (".implode(",",$DailyLogID).") AND Year='".$TargetYear."' AND Month='".$TargetMonth."'";
			$success['DeleteDB'] = $this->db_db_query($sql);
		
			if(count($UniqueIDToFilePath)>0){
				foreach($UniqueIDToFilePath as $unique_id => $file_path){
					$sql = "SELECT COUNT(*) FROM CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE WHERE UniqueID='".$unique_id."'";
					$total = $this->returnVector($sql);
					if($total[0] == 0){
						$full_file_path = $intranet_root.$file_path;
						$success['DeleteFile'.$unique_id] = $lf->file_remove($full_file_path);
					}
				}
			}
		}
		return !in_array(false,$success);
	}
	
	function Delete_Doctor_Certificate_By_FileID($FileID)
	{
		global $intranet_root;
		include_once("libfilesystem.php");
		
		$lf = new libfilesystem();
		$success = array();
		
		$sql = "SELECT UniqueID,FilePath FROM CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE WHERE FileID='".$FileID."'";
		$result = $this->returnArray($sql);
		if(count($result)>0){
			$UniqueID = $result[0]['UniqueID'];
			$FilePath = $result[0]['FilePath'];
		
			$sql = "DELETE FROM CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE WHERE FileID='".$FileID."'";
			$success['DeleteDB'] = $this->db_db_query($sql);
			
			$sql = "SELECT COUNT(*) FROM CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE WHERE UniqueID='".$UniqueID."'";
			$count = $this->returnVector($sql);
			if($count[0]==0){
				$full_file_path = $intranet_root.$FilePath;
				$success['DeleteFile'] = $lf->file_remove($full_file_path);
			}
		}
		
		return !in_array(false,$success);
	}
	
	function Get_General_Settings()
	{
		include_once("libgeneralsettings.php");
		
		if($this->GeneralSettings === false){
			$GeneralSetting = new libgeneralsettings();
			$SettingList[] = "'TerminalIP'";
			$SettingList[] = "'HideAttendStatus'";
			$SettingList[] = "'IgnorePeriod'"; // Ignore Period for card tapping
			$SettingList[] = "'AbsMinsForTimeSlot'"; // Absolute time limit before time slot start in mins to count all tap card as the target time slot's card tapping
			$SettingList[] = "'ReportDisplaySuspendedStaff'"; // Display suspended staff in reports
			$SettingList[] = "'ReportDisplayLeftStaff'"; // Display left staff in reports
			
			$Settings = $GeneralSetting->Get_General_Setting('StaffAttendance',$SettingList);
		}else{
			$Settings = $this->GeneralSettings;
		}
		return $Settings;
	}
	
	function Get_Name_Field($prefix="")
	{
		global $Lang;
		$name_field = getNameFieldByLang($prefix);
		$x = "CONCAT(".$name_field.",IF(".$prefix."RecordStatus<>1,'[".$Lang['Status']['Suspended']."]',''))";
		return $x;
	}
	
	function Get_Archive_Name_Field($prefix="")
	{
		global $Lang;
		$name_field = getNameFieldByLang2($prefix);
		$x = "CONCAT(".$name_field.",'[".$Lang['Status']['Left']."]')";
		return $x;
	}
	
	function Get_Customized_Settings()
	{
		global $sys_custom;
		include_once("libgeneralsettings.php");
		
		$GeneralSetting = new libgeneralsettings();
		$Settings = array();
		if($sys_custom['StaffAttendance']['DailyAttendanceRecord']) {
			$SettingList[] = "'CustomizedEmailTitle'";
			$SettingList[] = "'CustomizedEmailHeader'";
			$SettingList[] = "'CustomizedEmailFooter'";
			$SettingList[] = "'CustomizedEmailRecipient'";
			$SettingList[] = "'CustomizedEmailSendTime'";
			$SettingList[] = "'CustomizedDisableSendEmail'";
			
			$Settings = $GeneralSetting->Get_General_Setting('StaffAttendance',$SettingList);
			if(!isset($Settings['CustomizedEmailTitle'])){
				$Settings['CustomizedEmailTitle'] = '';
			}
			if(!isset($Settings['CustomizedEmailHeader'])){
				$Settings['CustomizedEmailHeader'] = '';
			}
			if(!isset($Settings['CustomizedEmailFooter'])){
				$Settings['CustomizedEmailFooter'] = '';
			}
			if(!isset($Settings['CustomizedEmailRecipient'])){
				$Settings['CustomizedEmailRecipient'] = '';
			}
			if(!isset($Settings['CustomizedEmailSendTime'])){
				$Settings['CustomizedEmailSendTime'] = '';
			}
			if(!isset($Settings['CustomizedDisableSendEmail'])){
				$Settings['CustomizedDisableSendEmail'] = '';
			}
		}
		
		return $Settings;
	}
	
	###########################################################
	# EJ/IP20 specific functions
	###########################################################
	function GET_ADMIN_USER()
	{
		global $intranet_root;

		if(!$this->IsAdminUserFileReaded)
		{
			include_once($intranet_root."/includes/libfilesystem.php");
			$lf = new libfilesystem();
			$this->AdminUserFileContent = trim($lf->file_read($intranet_root."/file/staffattend3/admin_user.txt"));
			$this->IsAdminUserFileReaded = true;
		}

		return $this->AdminUserFileContent;
	}
	
	function Log_Record($content)
	{
		global $intranet_root;
		
		$log_time = date("Y-m-d H:i:s", time());
		$content = $log_time." by UserID ".$_SESSION['UserID'].":\n".$content."\n\n";
		$log_path = $intranet_root."/file/staffattend_recordlog.log";
		$handle = fopen($log_path,"a+");
		fwrite($handle,$content);
		fclose($handle);
	}
	
	function Get_No_Tap_Card_Records($TargetUserId, $StartDate, $EndDate, $NoInTime, $NoOutTime, $IncludeStatus)
	{
		$CARD_STATUS_LATEEARLYLEAVE = 6;
		$CARD_STATUS_ABSENT_SUSPECTED = 7;
		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
		
		$start_ts = strtotime($StartDate);
		$end_ts = strtotime($EndDate);
		
		$name_field = $this->Get_Name_Field("u.");
		$archived_name_field = $this->Get_Archive_Name_Field("u2.");
		
		$user_cond = " AND c.StaffID IN ('".implode("','",(array)$TargetUserId)."') ";
		$time_cond = "";
		if($NoInTime && $NoOutTime){
			$time_cond .= " AND ((c.InTime IS NULL AND c.InWavie='0') OR (c.OutTime IS NULL AND c.OutWavie='0')) ";
		}else if($NoInTime){
			$time_cond .= " AND c.InTime IS NULL AND c.InWavie='0' ";
		}else if($NoOutTime){
			$time_cond .= " AND c.OutTime IS NULL AND c.OutWavie='0' ";
		}
		
		$statusAry = $IncludeStatus;
		if(in_array(CARD_STATUS_LATE,$IncludeStatus) && in_array(CARD_STATUS_EARLYLEAVE, $IncludeStatus)){
			$statusAry[] = $CARD_STATUS_LATEEARLYLEAVE;
		}
		//$status_cond = " AND Status IN ('".implode("','",$statusAry)."') ";
		
		$records = array();
		for($cur_ts=$start_ts, $cur_year=date("Y",$start_ts), $cur_month=date("m",$start_ts);
			$cur_ts<=$end_ts;$cur_ts=mktime(0,0,0,intval($cur_month)+1,1,intval($cur_year)))
		{
			$cur_year = date("Y",$cur_ts);
			$cur_month = date("m",$cur_ts);
			
			$CardLogTable = $this->createTable_Card_Staff_Attendance2_Daily_Log($cur_year,$cur_month);
			$sql = "SELECT * FROM (
					SELECT 
						c.StaffID, 
						IF(u2.UserID IS NOT NULL, $archived_name_field, $name_field) as StaffName,
						DATE_FORMAT(CONCAT('$cur_year-$cur_month-',c.DayNumber),'%Y-%m-%d') as RecordDate,
						c.SlotName,
						c.DutyStart,
						c.DutyEnd,
						(CASE
						WHEN c.InSchoolStatus = '".CARD_STATUS_OUTGOING."' 
						THEN '".CARD_STATUS_OUTGOING."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' 
						THEN '".CARD_STATUS_HOLIDAY."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' AND c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."'
						THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_LATE."' 
						THEN '".CARD_STATUS_LATE."' 
						WHEN c.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' 
						THEN '".CARD_STATUS_EARLYLEAVE."' 
						WHEN c.InSchoolStatus = '".CARD_STATUS_ABSENT."' 
						THEN '".CARD_STATUS_ABSENT."'
						WHEN c.InSchoolStatus = '".CARD_STATUS_NORMAL."' OR c.OutSchoolStatus = '".CARD_STATUS_NORMAL."'
						THEN '".CARD_STATUS_NORMAL."' 
						ELSE '".$CARD_STATUS_DEFAULT."' 
						END) as Status,
						c.InTime,
						c.OutTime,
						c.InWavie,
						c.OutWavie  
					FROM $CardLogTable as c 
					LEFT JOIN INTRANET_USER as u ON u.UserID=c.StaffID 
					LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=c.StaffID 
					WHERE c.SlotName IS NOT NULL AND c.Duty='1' 
					AND (DATE_FORMAT(CONCAT('$cur_year-$cur_month-',c.DayNumber),'%Y-%m-%d') BETWEEN '$StartDate' AND '$EndDate') 
					$user_cond $time_cond 
					ORDER BY c.DayNumber,StaffName,c.DutyStart 
					) as t WHERE t.Status IN ('".implode("','",$statusAry)."')";
			//debug_r($sql);
			$rows = $this->returnResultSet($sql);
			$records = array_merge($records, $rows);
		}
		
		return $records;
	}
	
	function Get_Attendance_Status_Settings()
	{
		global $Lang;
		$CARD_STATUS_LATEEARLYLEAVE = 6;
		$CARD_STATUS_ABSENT_SUSPECTED = 7;
		
		$sql = "SELECT * FROM CARD_STAFF_ATTENDANCE3_STATUS_SETTING";
		$records = $this->returnResultSet($sql);
		$record_count = count($records);
		
		$status_list = array($CARD_STATUS_ABSENT_SUSPECTED,$CARD_STATUS_LATEEARLYLEAVE,CARD_STATUS_NORMAL,CARD_STATUS_ABSENT,CARD_STATUS_LATE,CARD_STATUS_EARLYLEAVE,CARD_STATUS_HOLIDAY,CARD_STATUS_OUTGOING);
		$status_display = array(CARD_STATUS_NORMAL=>$Lang['StaffAttendance']['Present'],
							CARD_STATUS_ABSENT=>$Lang['StaffAttendance']['Absent'],
							CARD_STATUS_LATE=>$Lang['StaffAttendance']['Late'],
							CARD_STATUS_EARLYLEAVE=>$Lang['StaffAttendance']['EarlyLeave'],
							CARD_STATUS_HOLIDAY=>$Lang['StaffAttendance']['Holiday'],
							CARD_STATUS_OUTGOING=>$Lang['StaffAttendance']['Outing'],
							$CARD_STATUS_LATEEARLYLEAVE=>($Lang['StaffAttendance']['Late'].$Lang['StaffAttendance']['And'].$Lang['StaffAttendance']['EarlyLeave']),
							$CARD_STATUS_ABSENT_SUSPECTED=>$Lang['StaffAttendance']['AbsentSuspected']);
		$ary = array();
		for($i=0;$i<$record_count;$i++){
			$ary[$records[$i]['StatusType']] = $records[$i];
		}
		foreach($status_list as $val){
			if(!isset($ary[$val])){
				$ary[$val] = array('StatusType'=>$val,'StatusSymbol'=>'','StatusColor'=>'','StatusDisplay'=>$status_display[$val]);
			}
			$ary[$val]['StatusDisplay'] = $status_display[$val];
		}
		
		return $ary;
	}
	
	function Update_Attendance_Status_Setting($StatusType, $StatusSymbol, $StatusColor)
	{
		$CARD_STATUS_ABSENT_SUSPECTED = 7;
		$status_list = array(CARD_STATUS_NORMAL,CARD_STATUS_ABSENT,CARD_STATUS_LATE,CARD_STATUS_EARLYLEAVE,CARD_STATUS_HOLIDAY,CARD_STATUS_OUTGOING,$CARD_STATUS_ABSENT_SUSPECTED);
		if(!in_array($StatusType,$status_list)){
			return false;
		}
		
		$sql = "UPDATE CARD_STAFF_ATTENDANCE3_STATUS_SETTING SET StatusSymbol='$StatusSymbol',StatusColor='$StatusColor',ModifiedBy='".$_SESSION['UserID']."',ModifiedDate=NOW() WHERE StatusType='$StatusType'";
		$success = $this->db_db_query($sql);
		if($this->db_affected_rows() == 0){
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE3_STATUS_SETTING (StatusType,StatusSymbol,StatusColor,ModifiedBy,ModifiedDate) VALUES ('$StatusType','$StatusSymbol','$StatusColor','".$_SESSION['UserID']."',NOW())";
			$success = $this->db_db_query($sql);
		}
		
		return $success;
	}
	
	function Check_Attendance_Status_Symbol($StatusType, $StatusSymbol)
	{
		$sql = "SELECT COUNT(*) FROM CARD_STAFF_ATTENDANCE3_STATUS_SETTING WHERE StatusType <> '$StatusType' AND StatusSymbol='$StatusSymbol'";
		$count1 = $this->returnVector($sql);
		
		$sql = "SELECT COUNT(*) FROM CARD_STAFF_ATTENDANCE3_REASON WHERE ReportSymbol='$StatusSymbol'";
		$count2 = $this->returnVector($sql);
		
		if($count1[0] > 0 || $count2[0] > 0){
			return false; // not OK
		}else{
			return true; // OK, no duplicated symbol with other records
		}
	}
	
	function Get_Export_Duty_Records($DutyType, $TargetID, $StartDate, $EndDate)
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $sys_custom;
		
		$rows = array();
		$start_ts = strtotime($StartDate);
		$end_ts = strtotime($EndDate);
		$sql = "SELECT UserID,UserLogin FROM INTRANET_USER WHERE RecordType=1";
		$staff_records = $this->returnResultSet($sql);
		$staff_record_count = count($staff_records);
		$staffIdToUserLogin = array();
		$is_group = $DutyType == 1;
		for($i=0;$i<$staff_record_count;$i++){
			$staffIdToUserLogin[$staff_records[$i]['UserID']] = $staff_records[$i]['UserLogin'];
		}
	
		if($is_group){ // group
			$header = array("Group ID","User Logins","Date","Duty Type","Time Slot","Duty Start","Duty End","Duty Count","Waive In","Waive Out","Reason ID");
		}else{ // individual
			$header = array("User Login","Date","Duty Type","Time Slot","Duty Start","Duty End","Duty Count","Waive In","Waive Out","Reason ID");
		}
		if($sys_custom['StaffAttendance']['WongFutNamCollege']){
			$header = array_merge($header, array("Attend One Time Slot"));
		}
		$rows[] = $header;
		$target_size = count($TargetID);
		for($i=0;$i<$target_size;$i++){
			if($is_group){ // group
				$obj = new staff_attend_group($TargetID[$i],true,false);
			}else{
				$obj = $TargetID[$i];
			}
			
			for($cur_ts=$start_ts;$cur_ts<=$end_ts;
				$year=date("Y",$cur_ts),$month=date("m",$cur_ts),$day=date("d",$cur_ts),$cur_ts=mktime(0,0,0,intval($month),intval($day)+1,intval($year)))
			{
				$date = date("Y-m-d",$cur_ts);
				$duty = $this->Get_Duty_By_Date($obj,$date,false);
				//debug_pr($duty);
				
				// full day on leave
				if(isset($duty['FullHoliday']) && count($duty['FullHoliday'])>0){
					foreach($duty['FullHoliday'] as $user_id){
						$reason_id = isset($duty['HolidayFullReason']) && isset($duty['HolidayFullReason'][$user_id])? $duty['HolidayFullReason'][$user_id] : '';
						//$row = array($TargetID[$i], $staffIdToUserLogin[$user_id], $date, "FL", "", "", "", "", "", "", $reason_id);
						if($is_group){
							$row_col = array($TargetID[$i], $staffIdToUserLogin[$user_id]);
						}else{
							$row_col = array($staffIdToUserLogin[$user_id]);
						}
						$row_cols = array($date, "FL", "", "", "", "", "", "", $reason_id);
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$row_cols = array_merge($row_cols, array("0"));
						}
						$row = array_merge($row_col, $row_cols);
						$rows[] = $row;
					}
				}
				
				// full day outing
				if(isset($duty['FullOutgoing']) && count($duty['FullOutgoing'])>0){
					foreach($duty['FullOutgoing'] as $user_id){
						$reason_id = isset($duty['OutgoingFullReason']) && isset($duty['OutgoingFullReason'][$user_id])? $duty['OutgoingFullReason'][$user_id] : '';
						//$row = array($TargetID[$i], $staffIdToUserLogin[$user_id], $date, "FO", "", "", "", "", "", "", $reason_id);
						if($is_group){
							$row_col = array($TargetID[$i], $staffIdToUserLogin[$user_id]);
						}else{
							$row_col = array($staffIdToUserLogin[$user_id]);
						}
						$row_cols = array($date, "FO", "", "", "", "", "", "", $reason_id);
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$row_cols = array_merge($row_cols, array("0"));
						}
						$row = array_merge($row_col, $row_cols);
						$rows[] = $row;
					}
				}
				
				for($j=0;$j<count($duty['SlotDetail']);$j++)
				{
					$slot_id = $duty['SlotDetail'][$j]['SlotID'];
					$slot_name = $duty['SlotDetail'][$j]['SlotName'];
					$slot_start = $duty['SlotDetail'][$j]['SlotStart'];
					$slot_end = $duty['SlotDetail'][$j]['SlotEnd'];
					$duty_count = $duty['SlotDetail'][$j]['DutyCount'];
					$waive_in = $duty['SlotDetail'][$j]['InWavie'] == '1'?'Y':'N';
					$waive_out = $duty['SlotDetail'][$j]['OutWavie'] == '1'?'Y':'N';
					$slot_user_id = $duty['SlotToUserDuty'][$slot_id];
					
					// on leave for time slot
					if(isset($duty['SlotToUserHoliday'][$slot_id])){
						$onleave_user_id_ary = $duty['SlotToUserHoliday'][$slot_id];
						foreach($onleave_user_id_ary as $onleave_user_id)
						{
							if(isset($duty['HolidaySlotReason'][$slot_id]) && isset($duty['HolidaySlotReason'][$slot_id][$onleave_user_id])){
								$onleave_reason_id = $duty['HolidaySlotReason'][$slot_id][$onleave_user_id];
							}else{
								$onleave_reason_id = '';
							}
							//$row = array($TargetID[$i], $staffIdToUserLogin[$onleave_user_id], $date, "L", $slot_name, $slot_start, $slot_end, $duty_count, $waive_in, $waive_out, $onleave_reason_id);
							if($is_group){
								$row_col = array($TargetID[$i], $staffIdToUserLogin[$onleave_user_id]);
							}else{
								$row_col = array($staffIdToUserLogin[$onleave_user_id]);
							}
							$row_cols = array($date, "L", $slot_name, $slot_start, $slot_end, $duty_count, $waive_in, $waive_out, $onleave_reason_id);
							if($sys_custom['StaffAttendance']['WongFutNamCollege']){
								$row_cols = array_merge($row_cols, array("0"));
							}
							$row = array_merge($row_col, $row_cols);
							$rows[] = $row;
						}
					}
					
					// outing for time slot
					if(isset($duty['SlotToUserOutgoing'][$slot_id])){
						$outing_user_id_ary = $duty['SlotToUserOutgoing'][$slot_id];
						foreach($outing_user_id_ary as $outing_user_id)
						{
							if(isset($duty['OutgoingSlotReason'][$slot_id]) && isset($duty['OutgoingSlotReason'][$slot_id][$outing_user_id])){
								$outing_reason_id = $duty['OutgoingSlotReason'][$slot_id][$outing_user_id];
							}else{
								$outing_reason_id = '';
							}
							//$row = array($TargetID[$i], $staffIdToUserLogin[$outing_user_id], $date, "O", $slot_name, $slot_start, $slot_end, $duty_count, $waive_in, $waive_out, $outing_reason_id);
							if($is_group){
								$row_col = array($TargetID[$i], $staffIdToUserLogin[$outing_user_id]);
							}else{
								$row_col = array($staffIdToUserLogin[$outing_user_id]);
							}
							$row_cols = array($date, "O", $slot_name, $slot_start, $slot_end, $duty_count, $waive_in, $waive_out, $outing_reason_id);
							if($sys_custom['StaffAttendance']['WongFutNamCollege']){
								$row_cols = array_merge($row_cols, array("0"));
							}
							$row = array_merge($row_col, $row_cols);
							$rows[] = $row;
						}
					}
					
					// normal duty
					if(count($slot_user_id)>0){
						$userlogins = '';
						$sep = '';
						foreach($slot_user_id as $user_id){
							if(isset($duty['SlotToUserHoliday']) && isset($duty['SlotToUserHoliday'][$slot_id]) && in_array($user_id,$duty['SlotToUserHoliday'][$slot_id]))
							{
								// on leave for time slot
							}else if(isset($duty['SlotToUserOutgoing']) && isset($duty['SlotToUserOutgoing'][$slot_id]) && in_array($user_id,$duty['SlotToUserOutgoing'][$slot_id]))
							{
								// outing for time slot
							}else{ // normal duty
								$userlogins .= $sep.$staffIdToUserLogin[$user_id];
								$sep = ',';
							}
						}
						//$row = array($TargetID[$i], $userlogins, $date, "D", $slot_name, $slot_start, $slot_end, $duty_count, $waive_in, $waive_out, "");
						if($is_group){
							$row_col = array($TargetID[$i], $userlogins);
						}else{
							$row_col = array($userlogins);
						}
						$row_cols = array($date, "D", $slot_name, $slot_start, $slot_end, $duty_count, $waive_in, $waive_out, "");
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$attend_one = $duty['SpecialDateDutyAttendOneUser'][$user_id]? '1': '0';
							$row_cols = array_merge($row_cols, array($attend_one));
						}
						$row = array_merge($row_col, $row_cols);
						$rows[] = $row;
					}
				} // end for $duty['SlotDetail']
				
				// past full day on leave
				if(isset($duty['CardFullHoliday']) && count($duty['CardFullHoliday'])>0){
					foreach($duty['CardFullHoliday'] as $user_id){
						$reason_id = isset($duty['CardFullHolidayReason']) && isset($duty['CardFullHolidayReason'][$user_id])? $duty['CardFullHolidayReason'][$user_id] : '';
						//$row = array($TargetID[$i], $staffIdToUserLogin[$user_id], $date, "FL", "", "", "", "", "", "", $reason_id);
						if($is_group){
							$row_col = array($TargetID[$i], $staffIdToUserLogin[$user_id]);
						}else{
							$row_col = array($staffIdToUserLogin[$user_id]);
						}
						$row_cols = array($date, "FL", "", "", "", "", "", "", $reason_id);
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$row_cols = array_merge($row_cols, array("0"));
						}
						$row = array_merge($row_col, $row_cols);
						$rows[] = $row;
					}
				}
				
				// past full day outing
				if(isset($duty['CardFullOutgoing']) && count($duty['CardFullOutgoing'])>0){
					foreach($duty['CardFullOutgoing'] as $user_id){
						$reason_id = isset($duty['CardFullOutgoingReason']) && isset($duty['CardFullHOutgoingReason'][$user_id])? $duty['CardFullOutgoingReason'][$user_id] : '';
						//$row = array($TargetID[$i], $staffIdToUserLogin[$user_id], $date, "FO", "", "", "", "", "", "", $reason_id);
						if($is_group){
							$row_col = array($TargetID[$i], $staffIdToUserLogin[$user_id]);
						}else{
							$row_col = array($staffIdToUserLogin[$user_id]);
						}
						$row_cols = array($date, "FO", "", "", "", "", "", "", $reason_id);
						if($sys_custom['StaffAttendance']['WongFutNamCollege']){
							$row_cols = array_merge($row_cols, array("0"));
						}
						$row = array_merge($row_col, $row_cols);
						$rows[] = $row;
					}
				}
				
				// past duty
				if(count($duty['CardSlotDetail'])>0){
					foreach($duty['CardSlotDetail'] as $slot_name => $ary)
					{
						if(isset($duty['CardSlotToDuty']) && isset($duty['CardSlotToDuty'][$slot_name])){
							for($j=0;$j<count($duty['CardSlotToDuty'][$slot_name]);$j++){
								$record_type = $duty['CardSlotToDuty'][$slot_name][$j]['RecordType'];
								$user_id = $duty['CardSlotToDuty'][$slot_name][$j]['StaffID'];
								$slot_start = $duty['CardSlotToDuty'][$slot_name][$j]['DutyStart'];
								$slot_end = $duty['CardSlotToDuty'][$slot_name][$j]['DutyEnd'];
								$duty_count = $duty['CardSlotToDuty'][$slot_name][$j]['DutyCount'];
								$waive_in = $duty['CardSlotToDuty'][$slot_name][$j]['InWavie'] == '1'? 'Y':'N';
								$waive_out = $duty['CardSlotToDuty'][$slot_name][$j]['OutWavie'] == '1' ? 'Y':'N';
								$duty_type = $record_type == 4?"L":($record_type == 5?"O":"D");
								$reason_id = $duty['CardSlotToDuty'][$slot_name][$j]['InReasonID'];
								
								//$row = array($TargetID[$i], $staffIdToUserLogin[$user_id], $date, $duty_type, $slot_name, $slot_start, $slot_end, $duty_count, $waive_in, $waive_out, $reason_id);
								if($is_group){
									$row_col = array($TargetID[$i], $staffIdToUserLogin[$user_id]);
								}else{
									$row_col = array($staffIdToUserLogin[$user_id]);
								}
								$row_cols = array($date, $duty_type, $slot_name, $slot_start, $slot_end, $duty_count, $waive_in, $waive_out, $reason_id);
								if($sys_custom['StaffAttendance']['WongFutNamCollege']){
									$attend_one = $duty['AttendOne']? '1':'0';
									$row_cols = array_merge($row_cols, array($attend_one));
								}
								$row = array_merge($row_col, $row_cols);
								$rows[] = $row;
							}
						}
					}
				}
			} // end for each day
		} // end for each target
		
		//debug_pr($rows);
		return $rows;
	}
	
	// retrieve all monitoring group ids of the login user
	function Get_Monitoring_Groups() {
		// Get Monitoring Groups
		$sql = "SELECT GroupID FROM MONITORING_RIGHT_GROUP_MEMBER WHERE UserID='".$_SESSION['UserID']."' AND UserRole=1";
		$rs = $this->returnVector($sql);
		return $rs;	
	}
	
	// retrieve all monitoring members of the login user
	function Get_Monitoring_Members() {
		$rs = array();
		$groups = $this->Get_Monitoring_Groups();
		// Get Monitoring Members
		if (count($groups) > 0) {
			$sql = "SELECT DISTINCT UserID FROM MONITORING_RIGHT_GROUP_MEMBER WHERE GroupID IN (".implode(',',$groups).")";
			$rs = $this->returnVector($sql);
		}
		return $rs; 	
	}
	
	function Get_Working_Hours_Deduction_Records($params)
	{
		global $Lang;
		$cond = "";
		$group_cond = "";
		$date_field = " w.RecordDate ";
		$is_sql_table_query = $params['IsSqlQuery'];
		if(isset($params['RecordID'])){
			$record_id = $params['RecordID'];
			if(is_array($record_id)){
				$cond .= " AND w.RecordID IN (".implode(",",$record_id).") ";
			}else{
				$cond .= " AND w.RecordID='$record_id' ";
			}
		}
		if(isset($params['StaffID'])){
			$staff_id = $params['StaffID'];
			if(is_array($staff_id)){
				$cond .= " AND w.StaffID IN (".implode(",",$staff_id).") ";
			}else{
				$cond .= " AND w.StaffID='$staff_id' ";
			}
		}
		if(isset($params['StartDate'])){
			$start_date = $params['StartDate'];
			$cond .= " AND w.RecordDate>='$start_date' ";
		}
		if(isset($params['EndDate'])){
			$end_date = $params['EndDate'];
			$cond .= " AND w.RecordDate<='$end_date' ";
		}
		if(isset($params['TargetYear']) && isset($params['TargetMonth'])){
			$like_pattern = $params['TargetYear']."-".$params['TargetMonth']."-%";
			$cond .= " AND w.RecordDate LIKE '$like_pattern' ";
		}
		if(isset($params['GroupByStaff'])){
			if($group_cond == "") $group_cond .= " GROUP BY ";
			$group_cond .= " w.StaffID,DATE_FORMAT(w.RecordDate,'$Y-%m'),w.DeductMin ";
			
			$date_field = " GROUP_CONCAT(w.RecordDate ORDER BY w.RecordDate ASC SEPARATOR '<br />') ";
		}
		if(isset($params['Keyword']) && trim($params['Keyword'])!=''){
			$escape_key = $this->Get_Safe_Sql_Like_Query(trim($params['Keyword']));
			$cond .= " AND (u.EnglishName LIKE '%$escape_key%' OR u.ChineseName LIKE '%$escape_key%' OR w.RecordDate LIKE '%$escape_key%') ";
		}
		$name_field = $this->Get_Name_Field("u.");
		$modifier_name_field = $this->Get_Name_Field("u2.");
		
		$sql = "SELECT 
					$name_field as StaffName,
					$date_field as RecordDate,
					".($is_sql_table_query?"CONCAT(w.DeductMin,' ".$Lang['StaffAttendance']['Mins']."')":"w.DeductMin")." as DeductMin,
					w.Remark,
					w.DateModified,
					$modifier_name_field as ModifiedBy ";
		if($is_sql_table_query){
			$sql.= ",CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',GROUP_CONCAT(w.RecordID),'\">') as CheckBox ";
		}
		$sql .= ",w.RecordID,w.StaffID 
				FROM CARD_STAFF_ATTENDANCE3_DEDUCT_WORKING_TIME as w 
				INNER JOIN INTRANET_USER as u ON u.UserID=w.StaffID 
				LEFT JOIN INTRANET_USER as u2 ON u2.UserID=w.ModifiedBy 
				WHERE u.RecordType='".USERTYPE_STAFF."' $cond $group_cond ";
		if(!$is_sql_table_query){
			$sql .= " ORDER BY u.EnglishName,w.RecordDate ";
		}
		//debug_pr($sql);
		if($is_sql_table_query){
			return $sql;
		}
		$records = $this->returnResultSet($sql);
		$record_size = count($records);
		if(isset($params['ReturnAssocArray'])){
			$ary = array();
			for($i=0;$i<$record_size;$i++){
				if(!isset($ary[$records[$i]['StaffID']])) $ary[$records[$i]['StaffID']] = array();
				$ary[$records[$i]['StaffID']][$records[$i]['RecordDate']] = $records[$i];
			}
			return $ary;
		}
		return $records;
	}
	
	function Add_Working_Hours_Deduction_Record($map)
	{
		$fields = array('StaffID','RecordDate','DeductMin','Remark');
		$user_id = $_SESSION['UserID'];
		
		if(count($map) == 0) return false;
		$success = true;
		
		$keys = '';
		$values = '';
		$sep = "";
		foreach($map as $key => $val){
			if(!in_array($key, $fields)) continue;
			$keys .= $sep."$key";
			$values .= $sep."'".$this->Get_Safe_Sql_Query($val)."'";
			$sep = ",";
		}
		$keys .= ",DateInput,DateModified,InputBy,ModifiedBy";
		$values .= ",NOW(),NOW(),'$user_id','$user_id'";
		
		$sql = "INSERT INTO CARD_STAFF_ATTENDANCE3_DEDUCT_WORKING_TIME ($keys) VALUES ($values)";
		$success = $this->db_db_query($sql);
		
		if($success){
			$record_id = $this->db_insert_id();
			return $record_id;
		}
		return $success;
	}
	
	function Delete_Working_Hours_Deduction_Record($params)
	{
		$cond = "";
		if(isset($params['RecordID'])){
			$RecordID = $params['RecordID'];
			if($cond != "") $cond .= " AND ";
			if(is_array($RecordID))
				$cond .= " RecordID IN ('".implode("','",$RecordID)."') ";
			else
				$cond .= " RecordID='$RecordID' ";
		}
		if(isset($params['StaffID'])){
			$StaffID = $params['StaffID'];
			if($cond != "") $cond .= " AND ";
			if(is_array($StaffID)){
				$cond .= " StaffID IN ('".implode("','",$StaffID)."') ";
			}else{
				$cond .= " StaffID='$StaffID' ";
			}
		}
		if(isset($params['RecordDate'])){
			$RecordDate = $params['RecordDate'];
			if($cond != "") $cond .= " AND ";
			if(is_array($RecordDate)){
				$cond .= " RecordDate IN ('".implode("','",$RecordDate)."') ";
			}else{
				$cond .= " RecordDate='$RecordDate' ";
			}
		}
		$sql = "DELETE FROM CARD_STAFF_ATTENDANCE3_DEDUCT_WORKING_TIME WHERE $cond";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function Get_Overall_Daily_Attendance_Records($params)
	{
		global $Lang;
		$CARD_STATUS_LATEEARLYLEAVE = 6;
		$CARD_STATUS_ABSENT_SUSPECTED = 7;
		$CARD_STATUS_DEFAULT = $CARD_STATUS_ABSENT_SUSPECTED;
		
		if(!isset($params['TargetDate'])){
			$params['TargetDate'] = date('Y-m-d');
		}
		
		$ts = strtotime($params['TargetDate']);
		$target_date = date("Y-m-d",$ts);
		
		$year = date("Y",$ts);
		$month = date("m",$ts);
		$day = date("d",$ts);
		
		$card_log_table_name = $this->createTable_Card_Staff_Attendance2_Daily_Log($year, $month);
		
		$conds = "";
		
		if(isset($params['RecordID'])){
			$record_id = IntegerSafe($params['RecordID']);
			if(is_array($record_id)){
				$conds .= " AND h.RecordID IN (".implode(",",$record_id).") ";
			}else{
				$conds .= " AND h.RecordID='$record_id' ";
			}
		}
		
		if(isset($params['TargetUserID'])){
			$target_user_id = IntegerSafe($params['TargetUserID']);
		  	if(is_array($target_user_id))
			{
				$conds .= " AND a.UserID IN (".((sizeof($target_user_id) > 0)? implode(',',$target_user_id):'-1').")";
			}else
			{
				$conds .= " AND a.UserID = '$target_user_id' ";
			}
		}

		$slot_cond = "";
		if(isset($params['SlotName'])) {
			if (is_array($params['SlotName'])) {
				$FinalSlotName = array();
				for ($i = 0; $i < sizeof($params['SlotName']); $i++) {
					if (trim($params['SlotName'][$i]) != '') $FinalSlotName[$i] = "'" . $this->Get_Safe_Sql_Query($params['SlotName'][$i]) . "'";
				}
				if (sizeof($FinalSlotName) > 0) {
					$slot_cond = " AND h.SlotName IN (" . ((sizeof($FinalSlotName) > 0) ? implode(',', $FinalSlotName) : '-1') . ")";
				} else {
					$slot_cond = " AND h.SlotName IS NOT NULL ";
				}
			} else {
				$slot_cond = "";
				if ($params['SlotName'] != "" && $params['SlotName'] != NULL && $params['SlotName'] != "undefined")
					$slot_cond = " AND h.SlotName = '" . $this->Get_Safe_Sql_Query($params['SlotName']) . "' ";
				else
					$slot_cond = " AND h.SlotName IS NOT NULL ";
			}
		}

		if(isset($params['GroupID'])){
			$group_id = IntegerSafe($params['GroupID']);
			if(is_array($group_id)){
				$conds .= " AND (ug.GroupID IN (".implode(",",$group_id).") ";
				if(in_array('-1',$group_id)){
					$conds .= " OR ug.GroupID IS NULL ";
				}
				$conds .= ") ";
			}else{
				$conds .= " AND (ug.GroupID='$group_id' ";
				if(in_array('-1',$group_id)){
					$conds .= " OR ug.GroupID IS NULL ";
				}
				$conds .= ") ";
			}
		}
		
		$status_cond = "";
		if(isset($params['StatusType']))
		{
			$status_type = (array)$params['StatusType'];
			$status_cond_ary = array();
		  	if(in_array(CARD_STATUS_NORMAL, $status_type))
	  		{
	  			$status_cond_ary[] = " ((h.InSchoolStatus = '".CARD_STATUS_NORMAL."' AND (h.OutSchoolStatus IS NULL OR h.OutSchoolStatus != '".CARD_STATUS_EARLYLEAVE."')) OR (h.OutSchoolStatus = '".CARD_STATUS_NORMAL."' AND (h.InSchoolStatus IS NULL OR h.InSchoolStatus = '-1'))) ";
	  		}
	  		if(in_array(CARD_STATUS_ABSENT,$status_type))
	  		{
	  			$status_cond_ary[] = " (h.InSchoolStatus = '".CARD_STATUS_ABSENT."') ";
	  		}
	  		if(in_array(CARD_STATUS_LATE,$status_type))
	  		{
	  			$status_cond_ary[] = " (h.InSchoolStatus = '".CARD_STATUS_LATE."' AND (h.OutSchoolStatus IS NULL OR h.OutSchoolStatus != '".CARD_STATUS_EARLYLEAVE."')) ";
	  		}
	  		if(in_array(CARD_STATUS_EARLYLEAVE,$status_type))
	  		{
	  			$status_cond_ary[] = " (h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' AND (h.InSchoolStatus IS NULL OR h.InSchoolStatus != '".CARD_STATUS_LATE."')) ";
	  		}
	  		if(in_array(CARD_STATUS_OUTGOING,$status_type))
	  		{
	  			$status_cond_ary[] = " (h.InSchoolStatus = '".CARD_STATUS_OUTGOING."') ";
	  		}
	  		if(in_array($CARD_STATUS_LATEEARLYLEAVE,$status_type))
	  		{
	  			$status_cond_ary[] = " (h.InSchoolStatus = '".CARD_STATUS_LATE."' AND h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."') ";
	  		}
	  		if(in_array(CARD_STATUS_HOLIDAY,$status_type))
	  		{
	  			$status_cond_ary[] = " (h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."') ";
	  		}
	  		if(in_array($CARD_STATUS_ABSENT_SUSPECTED,$status_type))
	  		{
	  			$status_cond_ary[] = " (h.Duty = '1' AND ((h.InSchoolStatus IS NULL AND h.OutSchoolStatus IS NULL) OR (h.InSchoolStatus='-1' AND h.OutSchoolStatus='-1'))) ";
	  		}
	  		if(count($status_cond_ary)>0){
	  			$status_cond .= " AND (".implode(" OR ",$status_cond_ary).") ";
	  		}
		}else{
			$status_cond .= " AND 0 ";
		}
		
		$name_field = getNameFieldByLang("a.");
		$updater_name_field = getNameFieldByLang("uu.");
		$sql = "SELECT 
				  a.UserID,
				  $name_field as StaffName,
				  g.GroupID,
				  IF(g.GroupName IS NULL,'',g.GroupName) as GroupName,
				  h.RecordID,
				  h.SlotName,
				  h.Duty,
				  h.DutyStart as SlotStart,
				  h.DutyEnd as SlotEnd,
				  i.ReasonID as LeaveReasonID,
				  h.InTime,
				  h.OutTime,
				  h.InSchoolStation,
				  h.OutSchoolStation,
				  h.InWaived,
				  h.OutWaived,
				  IF(h.DateConfirmed IS NULL, IF(i.Waived IS NULL AND j.ReasonID IS NOT NULL, j.DefaultWavie, i.Waived) ,i.Waived) as Waived,
				  IF(h.DateConfirmed IS NULL, IF(k.Waived IS NULL AND m.ReasonID IS NOT NULL, m.DefaultWavie, k.Waived) ,k.Waived) as ELWaived,
				  (CASE 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_OUTGOING."' 
				  	THEN '".CARD_STATUS_OUTGOING."' 
				  WHEN  h.InSchoolStatus = '".CARD_STATUS_HOLIDAY."' 
					THEN '".CARD_STATUS_HOLIDAY."' 
				  WHEN (h.InSchoolStatus = '".CARD_STATUS_LATE."' AND h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."') 
					THEN '".$CARD_STATUS_LATEEARLYLEAVE."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_LATE."' 
					THEN '".CARD_STATUS_LATE."' 
				  WHEN h.OutSchoolStatus = '".CARD_STATUS_EARLYLEAVE."' 
					THEN '".CARD_STATUS_EARLYLEAVE."' 
				  WHEN h.InSchoolStatus = '".CARD_STATUS_ABSENT."' 
					THEN '".CARD_STATUS_ABSENT."'
				  WHEN h.InSchoolStatus = '".CARD_STATUS_NORMAL."' OR h.OutSchoolStatus = '".CARD_STATUS_NORMAL."' 
					THEN '".CARD_STATUS_NORMAL."' 
				  ELSE '".$CARD_STATUS_DEFAULT."' 
				  END) as Status,
				  i.ReasonID as InReasonID,
				  j.ReasonText as InReason,
				  j.DefaultWavie as InReasonDefaultWaived,
				  k.ReasonID as OutReasonID,
				  m.ReasonText as OutReason,
				  m.DefaultWavie as OutReasonDefaultWaived,
				  h.Remark,
				  IF((h.InSchoolStatus IS NOT NULL OR h.OutSchoolStatus IS NOT NULL) AND (h.DateModified IS NOT NULL OR h.DateConfirmed IS NOT NULL),
					(CASE 
					 WHEN h.DateModified IS NOT NULL AND h.DateConfirmed IS NOT NULL 
					  THEN IF(h.DateModified > h.DateConfirmed,h.DateModified,h.DateConfirmed)
					 WHEN h.DateModified IS NOT NULL AND h.ModifyBy IS NOT NULL 
					   THEN h.DateModified 
					 WHEN h.DateConfirmed IS NOT NULL AND h.ConfirmBy IS NOT NULL 
					   THEN h.DateConfirmed  
					 ELSE '-' 
					END),
					'-') as LastUpdated,
				  $updater_name_field as LastUpdater,
				  h.RecordStatus,
				  h.RecordType,
				  h.InSchoolStatus,
				  h.OutSchoolStatus   
				FROM 
				  INTRANET_USER as a 
				  INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
				  ON wp.UserID=a.UserID 
				  	AND '".$this->Get_Safe_Sql_Query($target_date)."' BETWEEN wp.PeriodStart AND wp.PeriodEnd 
				  INNER JOIN $card_log_table_name as h on h.DayNumber = '".$this->Get_Safe_Sql_Query($day)."' and h.StaffID = a.UserID 
				  LEFT JOIN CARD_STAFF_ATTENDANCE_USERGROUP as ug ON ug.UserID = a.UserID
				  LEFT JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = ug.GroupID
				  LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as i on i.StaffID = a.UserID and i.RecordDate = '".$this->Get_Safe_Sql_Query($target_date)."' and i.RecordID = h.InAttendanceRecordID 
				  LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as j on j.ReasonID = i.ReasonID
				  LEFT JOIN CARD_STAFF_ATTENDANCE2_PROFILE as k on k.StaffID = a.UserID and k.RecordDate = '".$this->Get_Safe_Sql_Query($target_date)."' and k.RecordID = h.OutAttendanceRecordID
				  LEFT JOIN CARD_STAFF_ATTENDANCE3_REASON as m on m.ReasonID = k.ReasonID
				  LEFT JOIN INTRANET_USER as uu 
					ON IF(h.DateModified IS NOT NULL AND h.DateConfirmed IS NOT NULL,
						IF(h.DateModified > h.DateConfirmed,h.ModifyBy = uu.UserID,h.ConfirmBy = uu.UserID),
						IF(h.ModifyBy IS NULL,h.ConfirmBy = uu.UserID, h.ModifyBy = uu.UserID)
						)
				WHERE
				  a.RecordType='1'
				  AND a.RecordStatus = '1'
				  $conds  
				  AND h.DayNumber='".$this->Get_Safe_Sql_Query($day)."' AND h.SlotName IS NOT NULL ";
		if(isset($params['Keyword']) && trim($params['Keyword'])!=''){
			$keyword = trim($params['Keyword']);
			$safe_keyword = $this->Get_Safe_Sql_Like_Query($keyword);
			$sql.= " AND (a.EnglishName LIKE '%".$safe_keyword."%' 
					   OR a.ChineseName LIKE '%".$safe_keyword."%') ";
		}	
			$sql.= " $duty_cond $status_cond $slot_cond
				ORDER BY a.EnglishName, h.DutyStart ";
		//debug_pr($params);
		//debug_pr($sql);
		$records = $this->returnResultSet($sql);
		if(isset($params['StaffIDToRecords']) && $params['StaffIDToRecords']){
			$ary = array();
			$record_size = count($records);
			for($i=0;$i<$record_size;$i++){
				if(!isset($ary[$records[$i]['UserID']])){
					$ary[$records[$i]['UserID']] = array();
				}
				$ary[$records[$i]['UserID']][] = $records[$i];
			}
			return $ary;
		}
		
		return $records;
	}

	function isRecordChangeLogEnabled()
	{
		global $sys_custom;
		return $sys_custom['StaffAttendance']['RecordChangeLog'];
	}

	function INSERT_UPDATE_LOG($Section='', $RecordDetail='', $RecodID='', $OriginalRecordDetail='')
	{
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT.'includes/liblog.php');
		include_once($PATH_WRT_ROOT."includes/json.php");

		$liblog = new liblog();

		if(empty($RecordDetail))
			$RecordDetail = "";
		else if(is_array($RecordDetail))
			$RecordDetail = $liblog->BUILD_DETAIL($RecordDetail);

		if($OriginalRecordDetail != '') {
			$json = new JSON_obj();
			$OriginalRecordDetail = $json->encode($OriginalRecordDetail);
			$OriginalRecordDetail = $this->Get_Safe_Sql_Query($OriginalRecordDetail);
		}

		$RecodID = $RecodID ? $RecodID : NULL;
		$sql = "insert into CARD_STAFF_ATTENDANCE3_RECORD_CHANGE_LOG 
				(Section, RecordDetail, UpdateRecordID, OriginalRecordDetail, LogDate, LogBy)
				values
				('$Section', '".$this->Get_Safe_Sql_Query($RecordDetail)."', '$RecodID', '$OriginalRecordDetail',now(), '".$_SESSION['UserID']."')
				";
		return $this->db_db_query($sql);
	}
}
?>