<?php
if (!defined("LIBLUNCHBOX_DEFINED"))                     // Preprocessor directive
{
define("LIBLUNCHBOX_DEFINED", true);
define("LIBLUNCHBOX_TICKET_TABLE_PREFIX","CARD_STUDENT_LUNCH_TICKET_");

class liblunchbox extends libdb {


        function liblunchbox ()
        {
                 $this->libdb();
        }
        function getCalendarDates($year, $month)
        {
                 $sql = "SELECT DaysString
                               FROM CARD_STUDENT_LUNCH_CALENDAR WHERE Year = '$year' AND Month = '$month'";
                 $temp = $this->returnVector($sql);
                 if (sizeof($temp)==0) return false;
                 $array = explode(",",$temp[0]);
                 return $array;
        }
        function setCalendarDates($year, $month, $days)
        {
                 $sql = "SELECT RecordID FROM CARD_STUDENT_LUNCH_CALENDAR WHERE Year = '$year' AND Month = '$month'";
                 $temp = $this->returnVector($sql);
                 $id = $temp[0];
                 $day_list = (sizeof($days)!=0 ? implode(",",$days) : "");

                 if ($id > 0) # Record exists
                 {
                     $sql = "UPDATE CARD_STUDENT_LUNCH_CALENDAR
                                    SET DaysString = '$day_list', DateModified = now() WHERE RecordID = $id";
                 }
                 else
                 {
                     $sql = "INSERT INTO CARD_STUDENT_LUNCH_CALENDAR
                                    (Year, Month, DaysString, DateInput, DateModified)
                                    VALUES
                                    ('$year','$month','$day_list',now(), now())";
                 }
                 $this->db_db_query($sql);
        }
        function addCalendarDate($year, $month, $day)
        {
                 $sql = "SELECT RecordID, DaysString FROM CARD_STUDENT_LUNCH_CALENDAR WHERE Year = '$year' AND Month = '$month'";
                 $temp = $this->returnArray($sql, 2);
                 list($id, $DaysString) = $temp[0];

                 $days = (trim($DaysString)!="") ? explode(",", $DaysString) : "";
                 $day = (int) $day;

                 if (is_array($days) && sizeof($days)>0)
                 {
                 	# check if the date is already set
                 	$pos = -1;
                 	for ($i=0; $i<sizeof($days); $i++)
                 	{
                 		if ($days[$i]==$day)
                 		{
                 			return;
                 		} elseif ($days[$i]>$day)
                 		{
                 			$pos = $i;
                 			break;
                 		}
                 	}
                 	if ($pos>-1)
                 	{
	                 	$array2 = array_splice($days, $pos);
					    $days[] = $day;
					    $days = array_merge($days,$array2);
                 	} else
                 	{
                 		$days[] = $day;
                 	}
                 } else
                 {
                 	$days = array($day);
                 }

                 $this->setCalendarDates($year, $month, $days);
        }
        function getMonthlyTicketTableName($year="", $month="")
        {
                 $year=($year=="")?date("Y"):$year;
                 $month=($month=="")?date("m"):$month;

                 if (strlen($month)==1)
                 {
                     $month = "0".$month;
                 }

                 $ticket_table = LIBLUNCHBOX_TICKET_TABLE_PREFIX.$year."_".$month;
                 return $ticket_table;
        }
        function createMonthlyTicketTable($year="", $month="")
        {
                 $ticket_table = $this->getMonthlyTicketTableName($year, $month);

                 $sql = "CREATE TABLE IF NOT EXISTS $ticket_table (
                          RecordID int NOT NULL auto_increment,
                          StudentID int,
                          DayNumber int,
                          RecordType int,
                          RecordStatus int,
                          TakenTime datetime,
                          DateInput datetime,
                          DateModified datetime,
                          PRIMARY KEY (RecordID),
                          UNIQUE StudentDay (StudentID, DayNumber),
                          INDEX RecordType (RecordType),
                          INDEX RecordStatus (RecordStatus)
                        ) ENGINE=InnoDB Charset=utf8";
                 $this->db_db_query($sql);
                 return $ticket_table;
        }

        function getMonthlyTicketNum($year="", $month="")
        {
                 $ticket_table = $this->createMonthlyTicketTable($year, $month);

                 $sql = "SELECT COUNT(*) FROM $ticket_table";
                 $temp = $this->returnVector($sql);
                 return $temp[0];
        }
        function getMonthlyTicketsNumAssocArray($year="", $month="")
        {
                 $ticket_table = $this->createMonthlyTicketTable($year, $month);
                 $sql = "SELECT StudentID, COUNT(RecordID)
                                FROM $ticket_table
                                GROUP BY StudentID
                                ORDER BY StudentID
                                ";
                 $temp = $this->returnArray($sql,2);
                 return build_assoc_array($temp);
        }
        function getDayTicketInfo($StudentID, $target_date = "")
        {
                 if ($target_date == "")
                 {
                     $ts = time();
                 }
                 else
                 {
                     $ts = strtotime($target_date);
                     if ($ts==-1) return FLAG_DATE_ERROR;
                 }
                 $year = date('Y',$ts);
                 $month = date('m',$ts);
                 $day = date('d', $ts);

                 $ticket_table_name = $this->createMonthlyTicketTable($year, $month);
                 $sql = "SELECT RecordID, RecordType, RecordStatus, TakenTime
                                FROM $ticket_table_name
                                WHERE DayNumber = $day AND StudentID = $StudentID";
                 $temp = $this->returnArray($sql,4);

                 if (sizeof($temp)==0 || $temp[0][0]=="")
                 {
                     return FLAG_NO_LUNCH_TICKET;
                 }

                 return $temp[0];
        }
        function setTicketUsed($record_id, $target_date="", $record_type=TYPE_SET_STATUS_TERMINAL)
        {
                 if ($target_date == "")
                 {
                     $ts = time();
                 }
                 else
                 {
                     $ts = strtotime($target_date);
                     if ($ts==-1) return FLAG_DATE_ERROR;
                 }
                 $year = date('Y',$ts);
                 $month = date('m',$ts);
                 $day = date('d', $ts);

                 $ticket_table_name = $this->createMonthlyTicketTable($year, $month);

                 $sql = "UPDATE $ticket_table_name
                                SET RecordStatus = '".STATUS_LUNCH_TICKET_USED."',
                                TakenTime = now(), RecordType=$record_type
                                WHERE RecordID = $record_id AND RecordStatus = ".STATUS_LUNCH_TICKET_AVAILABLE."";
                 $this->db_db_query($sql);

                 if ($this->db_affected_rows()==1)
                 {
                     return true;
                 }
                 else return false;
        }

		# Hong Chi Lions Morninghill School customization
        function setTicketNotUsed($record_id, $target_date="", $record_type=TYPE_SET_STATUS_TERMINAL)
        {
                 if ($target_date == "")
                 {
                     $ts = time();
                 }
                 else
                 {
                     $ts = strtotime($target_date);
                     if ($ts==-1) return FLAG_DATE_ERROR;
                 }
                 $year = date('Y',$ts);
                 $month = date('m',$ts);
                 $day = date('d', $ts);

                 $ticket_table_name = $this->createMonthlyTicketTable($year, $month);

                 $sql = "UPDATE $ticket_table_name
                                SET RecordStatus = '0',
                                TakenTime = now(), RecordType=$record_type
                                WHERE RecordID = $record_id AND RecordStatus = ".STATUS_LUNCH_TICKET_AVAILABLE."";
                 $this->db_db_query($sql);

                 if ($this->db_affected_rows()==1)
                 {
                     return true;
                 }
                 else return false;
        }

        function addBadAction($StudentID, $ActionType)
        {
                 $sql = "INSERT INTO CARD_STUDENT_LUNCH_BAD_LOG
                                (RecordDate, StudentID, ActionTime, RecordType,
                                 RecordStatus, DateInput, DateModified)
                                 VALUES (CURDATE(), $StudentID, now(), '$ActionType',
                                         1, now(), now())";
                 $this->db_db_query($sql);
        }

        function getMonthsTable()
        {
                 unset($result);
                 $sql = "SHOW TABLES LIKE '".LIBLUNCHBOX_TICKET_TABLE_PREFIX."%'";
                 $temp = $this->returnVector($sql);
                 $pos = strlen(LIBLUNCHBOX_TICKET_TABLE_PREFIX);
                 for ($i=0; $i<sizeof($temp); $i++)
                 {
                      $string = substr($temp[$i],$pos);
                      $result[] = $string;
                 }
                 return $result;
        }
        # Param: YearMonth : e.g. 2006_02
        function removeMonthTable($YearMonth)
        {
                 $tablename = LIBLUNCHBOX_TICKET_TABLE_PREFIX."$YearMonth";
                 $sql = "DROP TABLE $tablename";
                 $this->db_db_query($sql);
        }

        # Param: Year, Month
        function getNumStudentsInMonth ($targetYear, $targetMonth)
        {
                 $tablename = $this->createMonthlyTicketTable($targetYear, $targetMonth);
                 $sql = "SELECT COUNT(DISTINCT StudentID) FROM $tablename";
                 $temp = $this->returnVector($sql);
                 return $temp[0]+0;

        }

        # Param: targetYear , targetMonth - new month
        #        lastYear, lastMonth - copy from this month
        function copyMonthData($targetYear, $targetMonth, $lastYear, $lastMonth)
        {
                 $last_tablename = $this->createMonthlyTicketTable($lastYear, $lastMonth);
                 $new_tablename = $this->createMonthlyTicketTable($targetYear, $targetMonth);
                 $array_set_days = $this->getCalendarDates($targetYear, $targetMonth);
                 if ($array_set_days == false || !is_array($array_set_days) || sizeof($array_set_days)==0)
                 {
                     return false;
                 }

                 $sql = "SELECT DISTINCT StudentID FROM $last_tablename";
                 $students = $this->returnVector($sql);
                 if (sizeof($students)==0) return false;

                 $student_list = implode(",", $students);
                 # Remove old records (Status not taken)
                 $sql = "DELETE FROM $new_tablename WHERE StudentID IN ($student_list) AND RecordStatus = 0";
                 $this->db_db_query($sql);

                 $db_day_string = "";
                 $delim = "";
                 for ($j=0; $j<sizeof($array_set_days); $j++)
                 {
                      $db_day_string .= "$delim([StudentID], '".$array_set_days[$j]."', 0, 0, now(), now())";
                      $delim = ",";
                 }
                 # Insert new records
                 $values = "";
                 $delim = "";
                 for ($i=0; $i<sizeof($students); $i++)
                 {
                      $target_string = str_replace("[StudentID]",$students[$i],$db_day_string);
                      $values .= "$delim ".$target_string;
                      $delim = ",";
                 }
                 $sql = "INSERT IGNORE INTO $new_tablename (StudentID, DayNumber, RecordType, RecordStatus, DateInput, DateModified)
                                VALUES $values";
                 if ($this->db_db_query($sql))
                 {
                     return true;
                 }
                 else
                 {
                     return false;
                 }
        }

}       // End of Class liblunchbox

}        // End of directive
?>