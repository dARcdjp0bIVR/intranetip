<?php

# Modifing by 

include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/portfolio25/iportfolio.inc.php");

class libpf_lp
{
	var $course_id;
	var $course_db;
	var $file_path;
	
	function __CONSTRUCT($ParCourseID="")
	{
		global $eclass_prefix, $eclass_filepath;
		
		$t_course_id = (empty($ParCourseID)) ? $_SESSION["iPortfolio"]["course_id"] : $ParCourseID;
		$t_course_db = $eclass_prefix."c".$t_course_id;
		$t_filepath = $eclass_filepath."/files";
				
		$this->SET_CLASS_VARIABLE("course_id", $t_course_id);
		$this->SET_CLASS_VARIABLE("course_db", $t_course_db);
		$this->SET_CLASS_VARIABLE("file_path", $t_filepath);
	}
	
	# Suspend / Activate template
	function SET_TEMPLATE_ACTIVATE($ParActivateStatus, $ParFileIDs)
	{
		global $lpf_gDB;
		$FileIDs = (is_array($ParFileIDs)) ? implode(",", $ParFileIDs) : $ParFileIDs;

		$sql =	"
							UPDATE
								".$this->course_db.".eclass_file
							SET
								IsSuspend = '".$ParActivateStatus."'
							WHERE
								FileID IN (".$FileIDs.")
						";

		return $lpf_gDB->db_db_query($sql);
	}
	
	# Copy template file with a new title
	function COPY_TEMPLATE($ParFileID, $ParNewTitle)
	{
		global $lpf_gDB;
		# Get details of original template
		$sql =	"
							SELECT
								Category,
								ParentDirID
							FROM
								".$this->course_db.".eclass_file
							WHERE
								FileID = '".$ParFileID."'";
		$row = $lpf_gDB->returnArray($sql);
		list($categoryID, $folderID) = $row[0];

		# Create new file name for copying
		$search = array(".html", ".HTML", ".htm", ".HTM");
		$newTitle = str_replace($search, "", $ParNewTitle);
		$newTitle = $newTitle.".html";

		# check whether the name already exist
		$repeated = false;
		$sql =	"
							SELECT
								Title
							FROM
								".$this->course_db.".eclass_file
							WHERE
								ParentDirID = '".$folderID."' AND
								category = '".$categoryID."'";
		$row = $lpf_gDB->returnVector($sql);
		for($i=0; $i<sizeof($row); $i++)
		{
			if(trim($newTitle)==trim($row[$i]))
			{
				$repeated = true;
				break;
			}
		}

		if($repeated)
			$msg = "copy_failed_dup_name";
		else
		{
			# Get more template files detail
			$sql =	"
								SELECT
									FileID,
									Location,
									User_id,
									UserName,
									UserEmail,
									memberType,
									Description,
									Size,
									ParentDirID,
									IsDir,
									LastModifiedBy,
									Permission,
									DateInput,
									DateModified,
									VirPath,
									Title
								FROM
									".$this->course_db.".eclass_file
								WHERE
									FileID = '".$ParFileID."'";
			$row = $lpf_gDB->returnArray($sql);

			if (sizeof($row)>0)
			{
				$FileID = $row[0][0];
				$Location = $row[0][1];
				$User_id = $row[0][2];
				$UserName = addslashes($row[0][3]);
				$UserEmail = addslashes($row[0][4]);
				$memberType = $row[0][5];
				$Description = addslashes($row[0][6]);
				$Size = $row[0][7];
				$ParentDirID = $row[0][8];
				$IsDir = $row[0][9];
				$LastModifiedBy = $row[0][10];
				$Permission = $row[0][11];
				$DateInput = $row[0][12];
				$DateModified = $row[0][13];
				$VirPath = $row[0][14];
				$Title = $row[0][15];

				# Prepare file path for copying
				$myPath = $this->GET_CATEGORY_ROOT($categoryID)."/".$Location;
				$filepath_new = str_replace("\'", "'", ($myPath."/".$newTitle));
				$filepath_old = $myPath."/".$Title;

				if (file_exists($filepath_old))
				{
					echo "overhere";
					$success = copy($filepath_old, $filepath_new);

					if($success==1)
					{
						# Insert new record for copied template
						$sql =	"
											INSERT INTO
												".$this->course_db.".eclass_file
													(Title, Location, VirPath, User_id, UserName, UserEmail, memberType, Description, Size, Category, ParentDirID, IsDir, LastModifiedBy, Permission, DateInput, DateModified)
											VALUES
												('".$newTitle."', '".addslashes($Location)."', '".addslashes($VirPath)."', '$User_id', '$UserName', '$UserEmail', '$memberType', '$Description', '$Size', '".$categoryID."', '$ParentDirID', '$IsDir', '$LastModifiedBy', '$Permission', now(), now())
										";
						$lpf_gDB->db_db_query($sql);

						$this->UPDATE_FOLDER_SIZE($folderID);
					}
				}
			}
			$msg = ($success==1) ? "copy" : "copy_failed";
		}

		return $msg;
	}
	
	# Get category root
	function GET_CATEGORY_ROOT($ParCategoryID){
		
		$coursefolder = $this->course_db;

		switch ($ParCategoryID){
			case 0: $root = $this->file_path."/".$coursefolder."/notes"; break;
			case 1: $root = $this->file_path."/".$coursefolder."/reference"; break;
			case 2: $root = $this->file_path."/".$coursefolder."/glossary"; break;
			case 3: $root = $this->file_path."/".$coursefolder."/assignment"; break;
			case 4: $root = $this->file_path."/".$coursefolder."/handin"; break;
			case 5: $root = $this->file_path."/".$coursefolder."/question"; break;
			case 6: $root = $this->file_path."/".$coursefolder."/public"; break;
			case 7: $root = $this->file_path."/".$coursefolder."/group"; break;
			case 8: $root = $this->file_path."/".$coursefolder."/personal"; break;
			case 9: $root = $this->file_path."/public"; break;
			case 10: $root = $this->file_path."/album"; break;
			default: $root = $this->file_path."/".$coursefolder."/notes"; break;
		}
		return $root;
	}
	
	# Update folder size after modifying files
	function UPDATE_FOLDER_SIZE($ParFileID) {
		global $lpf_gDB;
		# recursively outward
		if ($ParFileID=="")
			return;
		$sql =	"
							SELECT
								SUM(Size)
							FROM
								".$this->course_db.".eclass_file
							WHERE
								ParentDirID = '".$ParFileID."'
						";
		$row = $lpf_gDB->returnArray($sql);
		if (sizeof($row)>0) {
			$size = $row[0][0];
			$sql =	"
								UPDATE
									".$this->course_db.".eclass_file
								SET
									Size = '".$size."'
								WHERE
									FileID = '".$ParFileID."'
							";
			$lpf_gDB->db_db_query($sql);

			$sql =	"
								SELECT
									ParentDirID
								FROM
									".$this->course_db.".eclass_file
								WHERE
									FileID = '".$ParFileID."'
							";
			$result = $lpf_gDB->returnArray($sql);
			if (sizeof($result)>0) {
				# do recursively
				$this->UPDATE_FOLDER_SIZE($result[0][0]);
			}
		}
	}
	
	# Delete template files
	function DELETE_TEMPLATE($ParFileIDs)
	{
		global $lpf_gDB;
		for($i=0; $i<sizeof($ParFileIDs); $i++)
		{
			$fid = $ParFileIDs[$i];

			# check whether the template is being used, if yes, do not allow to delete
			$sql =	"
								SELECT
									COUNT(n.notes_id),
									f.ParentDirID
								FROM
									".$this->course_db.".eclass_file as f
								LEFT JOIN
									".$this->course_db.".notes as n
								ON
									n.url = CONCAT(IFNULL(f.VirPath, '/'), f.Title)
								WHERE
									f.FileID = '".$fid."'
								GROUP BY
									f.FileID
							";
			$check = $lpf_gDB->returnArray($sql);
			list($exist_count, $folderID) = $check[0];

			if($exist_count==0)
			{
				# Get template files detail
				$sql =	"
									SELECT
										FileID,
										Location,
										User_id,
										IsDir,
										Permission,
										Title,
										ifnull(ParentDirID, 'rt')
									FROM
										".$this->course_db.".eclass_file
									WHERE
										FileID = '".$fid."'
								";
				$row = $lpf_gDB->returnArray($sql);

				if (sizeof($row)>0)
				{
					$FileID = $row[0][0];
					$Location = $row[0][1];
					$User_id = $row[0][2];
					$IsDir = $row[0][3];
					$Permission = $row[0][4];
					$Title = $row[0][5];
					$isRoot = ($row[0][6]=='rt');
					$filepath = $this->GET_CATEGORY_ROOT(0)."/".$Location;

					# remove if access right granted
					# delete physical file
					if (file_exists($filepath))
					{
						$filename = $filepath."/".$Title;
						# delete file
						if(file_exists($filename))
							unlink($filename);

						# delete folder if no file remained
						$fileExist = false;
						$dh  = opendir($filepath);
						while (false !== ($file_n = readdir($dh))) {
							if($file_n != "." && $file_n != "..")
							{
								$fileExist = true;
								break;
							}
						}
						closedir($dh);

						if(!$fileExist)
							rmdir($filepath);
					}

					# delete DB row
					$sql =	"
										DELETE FROM
											".$this->course_db.".grouping_function
										WHERE
											function_id = '".$FileID."' AND
											function_type='FM'
									";
					$lpf_gDB->db_db_query($sql);

					$sql =	"
										DELETE FROM
											".$this->course_db.".eclass_file
										WHERE
											FileID = '".$FileID."'
									";
					$lpf_gDB->db_db_query($sql);
				}
				$this->UPDATE_FOLDER_SIZE($folderID);
			}
			else
				$ReturnArr[] = $fid;
		}
		return $ReturnArr;
	}
	
	# Get class list which has published learning portfolios
	function GET_PUBLISH_LEARNING_PORTFOLIO_CLASS_LIST()
	{
		global $eclass_db, $intranet_db, $lpf_gDB;

		$sql =	"
							SELECT DISTINCT
								iu.ClassName
							FROM
								".$this->course_db.".web_portfolio as wp
							LEFT JOIN
								".$this->course_db.".user_config as ucf
							ON
								wp.web_portfolio_id = ucf.web_portfolio_id
							INNER JOIN
								".$eclass_db.".PORTFOLIO_STUDENT as ps
							ON
								ucf.user_id = ps.CourseUserID
							INNER JOIN
								".$intranet_db.".INTRANET_USER as iu
							ON
								iu.UserID = ps.UserID
							WHERE
								ucf.notes_published IS NOT NULL AND
								wp.status = '1' AND
								(iu.ClassName IS NOT NULL AND iu.ClassName != '')
							ORDER BY
								iu.ClassName
						";
		$ReturnArr = $lpf_gDB->returnArray($sql);

		return $ReturnArr;
	}
	
	# Generate learning portfolio table for display
	function GEN_LEARNING_PORTFOLIO_LIST($ParPageInfoArr, $ParUserID)
	{
		global $image_path, $LAYOUT_SKIN, $i_LastModified, $ec_iPortfolio, $button_publish_iPortfolio, $i_status_published, $i_status_shared, $no_record_msg;
		global $PATH_WRT_ROOT, $ck_is_alumni;
	
		$lp_list = $this->GET_LEARNING_PORTFOLIO_LIST($ParUserID);
		
		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}
		
		$ReturnStr =	"<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\n";

		for($i=0; $i<count($lp_list); $i++)
		{
      # Eric Yip (20090916): Crypt parameters
      $key = $this->GET_STUDENT_CRYPTED_STRING($lp_list[$i]['web_portfolio_id'], $ParUserID);
		
			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{
				$lp = $this->GET_LEARNING_PORTFOLIO($lp_list[$i]['web_portfolio_id'], $ParUserID);

				if($lp['UserID'] != "" && $lp['allow_review'] == "1")
					$status_arr[] = "<span class=\"LP_shared\">".$i_status_shared."</span>";
				if($lp['UserID'] != "" && $lp['notes_published'] != "")
					$status_arr[] = "<span class=\"LP_publish\">".$i_status_published."<a href=\"javascript:newWindow('/home/portfolio/learning_portfolio/browse/student_view.php?key=".$key."', 93)\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a></span>";
				else
					$status_arr[] = "<span class=\"LP_draft\">".$ec_iPortfolio['draft']."</span>";

				$status_str = implode(" | ", $status_arr);
				unset($status_arr);
				
				if($lp['comment_count'] > 0)
//					$comment_str = "<span class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"></span> <span class=\"LP_comment\">".$lp['comment_count'].$ec_iPortfolio['new_comments']."</span>";
					$comment_str = "<span class=\"tabletext\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ecomm/icon_chat.gif\" width=\"17\" height=\"17\" border=\"0\" align=\"absmiddle\"></span> <span class=\"tablelink\">".$lp['comment_count']." ".$ec_iPortfolio['comments']."</span>";
				else
					$comment_str = "";
//					$comment_str = "<a href=\"http://".$lp_list[$i]['web_portfolio_id']."\" class=\"LP_comment\">".$lp['comment_count'].$ec_iPortfolio['new_comments']."</a>";
//				else
//					$comment_str = "<a href=\"http://".$lp_list[$i]['web_portfolio_id']."\" class=\"tablelink\">".$ec_iPortfolio['comments']."</a>";
			
				if($i % 2 == 0)
					$ReturnStr .=	"<tr>\n";
			
				if($lp['UserID'] != "" && $lp['notes_published'] != "")
				{
					$ReturnStr .=	"
													<td align=\"center\" valign=\"top\">
														<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
															<tr>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_01.gif\" width=\"5\" height=\"5\"></td>
																<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\" width=\"5\" height=\"5\"></td>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_03.gif\" width=\"5\" height=\"5\"></td>
															</tr>
															<tr>
																<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\" width=\"5\" height=\"5\"></td>
																<td>
																	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".($lp['template_selected'] == ""?"01":$lp['template_selected']).".jpg\" width=\"240\" height=\"172\"></td>
																		</tr>
																		<tr>
																			<td align=\"right\" bgcolor=\"#89bf6c\"><a href=\"javascript:;\"></a>
																				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
																					<tr>
												";
					if(!$ck_is_alumni)
					{
						if($_SESSION['UserType'] == 1 && $this->HAS_RIGHT("manage_student_learning_sharing"))
							$ReturnStr .=	"
																							<td height=\"30\" class=\"tabletext\">
																								<a href=\"javascript:newWindow('/home/portfolio/learning_portfolio/browse/student.php?key=".$key."', 93) \"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\"></a><a href=\"javascript:jTO_LP_SETTING('".$lp_list[$i]['web_portfolio_id']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_preference.gif\" width=\"20\" height=\"20\" border=\"0\"></a>
																							</td>
														";
						else if($_SESSION['UserType'] == 2)
						{
							if($lp['deadline'] == "" || $lp['deadline'] > time())
								$ReturnStr .=	"
																								<td height=\"30\" class=\"tabletext\">
																									<a href=\"javascript:newWindow('/home/portfolio/learning_portfolio/browse/student.php?key=".$key."', 93) \"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\"></a><a href=\"/home/portfolio/learning_portfolio/contents_student/management/notes_setting.php?WebPortfolioID=".$lp_list[$i]['web_portfolio_id']."\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_preference.gif\" width=\"20\" height=\"20\" border=\"0\"></a>
																								</td>
																								<td align=\"right\" class=\"tabletext\">
																									<input type=\"button\" class=\"formbutton\" onClick=\"self.location='./contents_student/management/notes_publish.php?WebPortfolioID=".$lp_list[$i]['web_portfolio_id']."'\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"".$button_publish_iPortfolio."\">
																								</td>
															";
							else
								$ReturnStr .=	"
																								<td height=\"30\" class=\"tabletext\" colspan=\"2\">&nbsp;</td>
															";
						}
					}
					$ReturnStr .=	"
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
																<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\" width=\"5\" height=\"5\"></td>
															</tr>
															<tr>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_07.gif\" width=\"5\" height=\"5\"></td>
																<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\" width=\"5\" height=\"5\"></td>
																<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_09.gif\" width=\"5\" height=\"5\"></td>
															</tr>
														</table>
														<table width=\"250\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
															<tr>
																<td>
																	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td valign=\"top\" class=\"LP_on_title\">".$lp['title']."</td>
																			<td align=\"right\" valign=\"top\" nowrap class=\"tabletext\">
																				".$status_str."
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td class=\"tabletext\">
																	".$i_LastModified."	: ".$lp['modified']."<br>
																	".$ec_iPortfolio['publish_date']."	: ".$lp['notes_published']."
																</td>
															</tr>
															<tr>
																<td>
																	".$comment_str."
																</td>
															</tr>
														</table>
														<br>
													</td>
												";
				}
				else
				{
					$ReturnStr .=	"
													<td align=\"center\" valign=\"top\">
														<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border: solid 1px #cccccc; margin:5px;\">
															<tr>
																<td>
																	<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".($lp['template_selected'] == ""?"01":$lp['template_selected']).".jpg\" width=\"240\" height=\"172\"></td>
																		</tr>
																		<tr>
																			<td align=\"right\" bgcolor=\"#cccccc\"><a href=\"javascript:;\"></a>
																				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
																					<tr>
												";
					if($_SESSION['UserType'] == 1 && $this->HAS_RIGHT("manage_student_learning_sharing"))
						$ReturnStr .=	"
																						<td height=\"30\" class=\"tabletext\"><a href=\"javascript:newWindow('/home/portfolio/learning_portfolio/browse/student.php?key=".$key."', 93) \"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\"></a><a href=\"javascript:jTO_LP_SETTING('".$lp_list[$i]['web_portfolio_id']."')\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_preference.gif\" width=\"20\" height=\"20\" border=\"0\"></a></td>
													";
					else if($_SESSION['UserType'] == 2)
					{
						if($lp['deadline'] == "" || $lp['deadline'] > time())
							$ReturnStr .=	"
																							<td height=\"30\" class=\"tabletext\"><a href=\"javascript:newWindow('/home/portfolio/learning_portfolio/browse/student.php?key=".$key."', 93) \"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\"></a><a href=\"/home/portfolio/learning_portfolio/contents_student/management/notes_setting.php?WebPortfolioID=".$lp_list[$i]['web_portfolio_id']."\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_preference.gif\" width=\"20\" height=\"20\" border=\"0\"></a></td>
																							<td align=\"right\" class=\"tabletext\">
																								<input type=\"button\" class=\"formbutton\" onClick=\"self.location='./contents_student/management/notes_publish.php?WebPortfolioID=".$lp_list[$i]['web_portfolio_id']."'\"  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"".$button_publish_iPortfolio."\">
																							</td>
														";
						else
							$ReturnStr .=	"
																							<td height=\"30\" class=\"tabletext\" colspan=\"2\">&nbsp;</td>
														";
					}
					$ReturnStr .=	"
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<table width=\"250\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
															<tr>
																<td>
																	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td valign=\"top\" class=\"LP_on_title\">".$lp['title']."</td>
																			<td align=\"right\" valign=\"top\" nowrap class=\"tabletext\">
																				".$status_str."
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
															<tr>
																<td class=\"tabletext\">
																	".$i_LastModified."	: ".($lp['UserID'] != ""?$lp['modified']:"--")."<br>
																	".$ec_iPortfolio['publish_date']."	: ".($lp['UserID'] != ""?$lp['notes_published']:"--")."
																</td>
															</tr>
															<tr>
																<td>
																	".$comment_str."
																</td>
															</tr>
														</table>
														<br>
													</td>
												";
				}
											
				if($i % 2 == 1)
					$ReturnStr .=	"</tr>\n";
				else if($i+1 == count($lp_list))
					$ReturnStr .=	"
														<td>&nbsp;</td>\n
													</tr>\n
												";
			}
		}
		
		if(count($lp_list) == 0)
		{
			$ReturnStr .=	"
											<tr>
												<td align=\"center\" class=\"tabletext\">
												".$no_record_msg."
												</td>
											</tr>
										";
		}
		
		$ReturnStr .=	"</table>\n";
		
		return array($ReturnStr, $lp_list);
	}
	
	# Get Learning Portfolio list of a student
	function GET_LEARNING_PORTFOLIO_LIST($ParUserID)
	{
		global $eclass_db, $intranet_db, $ck_is_alumni, $lpf_gDB;

		$lo = new libgroups($this->course_db);
		$exceptionList = $lo->returnFunctionIDs("PORTw",$this->IP_USER_ID_TO_EC_USER_ID($ParUserID));

		if($ck_is_alumni)
		{
			# Get learning portfolio which student has been edited (can be out of editable period)
			$sql =	"
								SELECT DISTINCT
									wp.web_portfolio_id
								FROM
									".$this->course_db.".web_portfolio as wp
								LEFT JOIN
									".$this->course_db.".user_config as ucf
								ON
									wp.web_portfolio_id = ucf.web_portfolio_id
								INNER JOIN
									".$eclass_db.".PORTFOLIO_STUDENT as ps
								ON
									ucf.user_id = ps.CourseUserID
								WHERE
									ps.UserID = '".$ParUserID."' AND
									ucf.notes_published IS NOT NULL AND
									wp.status = 1
								ORDER BY
									ucf.notes_published DESC
							";
			$ReturnArr = $lpf_gDB->returnArray($sql);
		}
		else
		{
			# Get learning portfolio which can be edited (in editable period)
			$sql =	"
								SELECT DISTINCT
									wp.web_portfolio_id
								FROM
									".$this->course_db.".web_portfolio AS wp
								LEFT JOIN
									".$this->course_db.".notes_student as ns
								ON
									(wp.web_portfolio_id = ns.web_portfolio_id AND ns.user_id = '".$this->IP_USER_ID_TO_EC_USER_ID($ParUserID)."')
								WHERE
									wp.web_portfolio_id NOT IN ($exceptionList) AND wp.status='1' AND
									(((wp.starttime < now()) AND (wp.deadline > now())) OR (wp.starttime IS NULL AND wp.deadline IS NULL) OR (wp.starttime IS NULL AND (wp.deadline > now())) OR ((wp.starttime < now()) AND wp.deadline IS NULL))
								GROUP BY
									wp.web_portfolio_id
							";
			$Set1 = $lpf_gDB->returnArray($sql);
	
			# Get learning portfolio which student has been edited (can be out of editable period)
			$sql =	"
								SELECT DISTINCT
									wp.web_portfolio_id
								FROM
									".$this->course_db.".web_portfolio as wp
								LEFT JOIN
									".$this->course_db.".user_config as ucf
								ON
									wp.web_portfolio_id = ucf.web_portfolio_id
								INNER JOIN
									".$eclass_db.".PORTFOLIO_STUDENT as ps
								ON
									ucf.user_id = ps.CourseUserID
								WHERE
									ps.UserID = '".$ParUserID."' AND
									ucf.notes_published IS NOT NULL AND
									wp.status = 1
								ORDER BY
									ucf.notes_published DESC
							";
			$Set2 = $lpf_gDB->returnArray($sql);
	
			# Merge the 2 sets into 1
			foreach($Set2 as $wp_id)
			{
				$wp_id_arr[] = $wp_id[0];
			}
			foreach($Set1 as $wp_id)
			{
				if(!is_array($wp_id_arr) || !in_array($wp_id[0], $wp_id_arr))
					$wp_id_arr[] = $wp_id[0];
			}
	
			# Rearrange data into proper format
			for($i=0; $i<count($wp_id_arr); $i++)
			{
				$ReturnArr[] = array(
															0 => $wp_id_arr[$i],
															"web_portfolio_id" => $wp_id_arr[$i]
														);
			}
		}

		return $ReturnArr;
	}
	
	# Get Learning Portfolio
	function GET_LEARNING_PORTFOLIO($ParWebPortfolioID, $ParUserID)
	{
		global $eclass_db, $lpf_gDB;

		# Get web portfolio title independently
		$sql =	"
							SELECT DISTINCT
								title
							FROM
								".$this->course_db.".web_portfolio
							WHERE
								web_portfolio_id = '".$ParWebPortfolioID."'
						";
		$WP_Title = $lpf_gDB->returnVector($sql);
		
		# Get web portfolio title independently
		$sql =	"
							SELECT DISTINCT
								UNIX_TIMESTAMP(deadline)
							FROM
								".$this->course_db.".web_portfolio
							WHERE
								web_portfolio_id = '".$ParWebPortfolioID."'
						";
		$WP_Deadline = $lpf_gDB->returnVector($sql);
		

		# Get web portfolio details according to user id
		$sql =	"
							SELECT DISTINCT
								date_format(wp.modified,'%Y/%m/%d') as modified,
								date_format(ucf.notes_published,'%Y/%m/%d') as notes_published,
								ucf.allow_review,
								IF(ucf.template_selected IS NULL, '01', ucf.template_selected) as template_selected,
								ps.UserID
							FROM
								".$this->course_db.".web_portfolio as wp
							LEFT JOIN
								".$this->course_db.".user_config as ucf
							ON
								wp.web_portfolio_id = ucf.web_portfolio_id
							INNER JOIN
								".$eclass_db.".PORTFOLIO_STUDENT as ps
							ON
								ucf.user_id = ps.CourseUserID
							WHERE
								wp.web_portfolio_id = '".$ParWebPortfolioID."' AND
								ps.UserID = '".$ParUserID."'
							ORDER BY
								modified DESC, notes_published DESC
						";
		$ReturnArr = $lpf_gDB->returnArray($sql);

		# Add comment count, title and deadline to return array
		$ReturnArr[0][count($ReturnArr[0])] = $ReturnArr[0]['comment_count'] = count($this->GET_LP_COMMENT_LIST($ParWebPortfolioID, $ParUserID));
		$ReturnArr[0][count($ReturnArr[0])] = $ReturnArr[0]['title'] = $WP_Title[0];
		$ReturnArr[0][count($ReturnArr[0])] = $ReturnArr[0]['deadline'] = $WP_Deadline[0];

		return $ReturnArr[0];
	}
	
	# Get comment list in a learning portfolio
	function GET_LP_COMMENT_LIST($ParWebPortfolioID, $ParUserID){

		global $eclass_db, $lpf_gDB;

		$sql =	"
							SELECT
								rc.comment,
								rc.author_user_id,
								date_format(rc.inputdate,'%Y/%m/%d') as inputdate,
								rc.review_comment_id
							FROM
								".$this->course_db.".review_comment as rc
							INNER JOIN
								{$eclass_db}.PORTFOLIO_STUDENT as ps
							ON
								rc.iPortfolio_user_id = ps.CourseUserID
							WHERE
								rc.web_portfolio_id='".$ParWebPortfolioID."' AND
								ps.UserID = '".$ParUserID."'
							ORDER BY
								rc.inputdate DESC
						";
		$ReturnArr = $lpf_gDB->returnArray($sql);

		return $ReturnArr;
	}
	
	# Generate peer learning portfolio table for display
	function GEN_PEER_LEARNING_PORTFOLIO_LIST($ParPageInfoArr, $ParUserID)
	{
		global $image_path, $LAYOUT_SKIN, $i_LastModified, $no_record_msg;
	
		$plp_list = $this->GET_PEER_LEARNING_PORTFOLIO_LIST($ParUserID);
		# sort result set in descending order of time
		if(is_array($plp_list) && count($plp_list) > 0)
		{
			foreach($plp_list as $key => $plp)
				$plp_time[$key] = $plp['notes_published'];
				
			array_multisort($plp_time, SORT_DESC, $plp_list);
		}
		
		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}
		
		$ReturnStr =	"<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";
		
		for($i=0; $i<count($plp_list); $i++)
		{
			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{
				$plp = $this->GET_LEARNING_PORTFOLIO($plp_list[$i]['web_portfolio_id'], $plp_list[$i]['UserID']);
				$lu = new libuser($plp_list[$i]['UserID']);
				
				# Eric Yip (20090916): Crypt parameters
        $key = $this->GET_STUDENT_CRYPTED_STRING($plp_list[$i]['web_portfolio_id'], $plp_list[$i]['UserID']);
				
				$ReturnStr .=	"
												<tr>\n
													<td align=\"center\">
														<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border: solid 1px #cccccc\">
															<tr>
																<td>
																	<a href=\"javascript:newWindow('./browse/student_view.php?key=".$key."', 93)\" ><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".$plp['template_selected'].".jpg\" width=\"160\" height=\"113\" border=\"0\"></a>
																</td>
															</tr>
														</table>
														<table width=\"180\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
															<tr>
																<td>
																	<a href=\"javascript:newWindow('./browse/student_view.php?key=".$key."', 93)\" class=\"tablelink\" >".$plp['title']."<br>
																	".$lu->getNameForRecord()."</a>
																</td>
															</tr>
															<tr>
																<td class=\"tabletext\">".$i_LastModified."	: ".$plp['modified']."<br></td>
															</tr>
														</table>
														<br>
													</td>
												</tr>\n
											";
			}
		}
		
		if(count($plp_list) == 0)
		{
			$ReturnStr .=	"
											<tr>
												<td align=\"center\" class=\"tabletext\">
												".$no_record_msg."
												</td>
											</tr>
										";
		}
		
		$ReturnStr .=	"</table>\n";
		
		return array($ReturnStr, $plp_list);
	}
	
	# Get the peer learning portfolio list of a student
	function GET_PEER_LEARNING_PORTFOLIO_LIST($ParUserID){

		global $eclass_db, $intranet_db, $eclass_filepath, $lpf_gDB;

		$learning_portfolio_config_file = "$eclass_filepath/files/learning_portfolio_config.txt";
		$filecontent = trim(get_file_content($learning_portfolio_config_file));
		list($status, $review_type, $friend_review) = unserialize($filecontent);

		$set1 = array();
		$set2 = array();
		if($status)
		{
			if($friend_review)
			{
				# Get learning portfolio from friends
				$sql  = "
									SELECT DISTINCT
										ps.UserID,
										wp.web_portfolio_id,
										uc.notes_published
									FROM
										{$intranet_db}.INTRANET_USER AS iu
									LEFT JOIN
										{$eclass_db}.PORTFOLIO_STUDENT AS ps
									ON
										iu.UserID = ps.UserID
									LEFT JOIN
										".$this->course_db.".user_friend as uf
									ON
										ps.UserID = uf.UserID
									LEFT JOIN
										".$this->course_db.".user_config as uc
									ON
										(ps.CourseUserID = uc.user_id AND uc.allow_review = 1)
									LEFT JOIN
										".$this->course_db.".web_portfolio as wp
									ON
										uc.web_portfolio_id = wp.web_portfolio_id
									LEFT JOIN
										".$this->course_db.".grouping_function as gf
									ON
										gf.function_id = wp.web_portfolio_id
									LEFT JOIN
										".$this->course_db.".user_group as ug
									ON
										ug.group_id = gf.group_id
									WHERE
										uf.FriendID = '".$ParUserID."' AND
										iu.UserID != '".$ParUserID."' AND
										iu.RecordType = '2' AND
										iu.RecordStatus = '1' AND
										ps.UserKey IS NOT NULL AND
										ps.IsSuspend = 0 AND
										wp.web_portfolio_id IS NOT NULL
								";
				$set1 = $lpf_gDB->returnArray($sql);
			}

			switch($review_type)
			{
				case "SCHOOL":
					break;
				case "LEVEL":
					$stu_obj = $this->GET_STUDENT_DATA($ParUserID);
					$conds =	"
											AND ic.ClassLevelID = '".$stu_obj[0]['ClassLevelID']."'
										";
					break;
				case "CLASS":
					$stu_obj = $this->GET_STUDENT_DATA($ParUserID);
					$conds =	"
											AND iu.ClassName = '".$stu_obj[0]['ClassName']."'
										";
					break;
			}

			# Get learning portfolio from schoolmates
			$sql =	"
								SELECT DISTINCT
									ps.UserID,
									wp.web_portfolio_id,
									uc.notes_published
								FROM
									{$intranet_db}.INTRANET_USER AS iu
								LEFT JOIN
									{$eclass_db}.PORTFOLIO_STUDENT AS ps
								ON
									iu.UserID = ps.UserID
								INNER JOIN
									{$intranet_db}.INTRANET_CLASS AS ic
								ON
									iu.ClassName = ic.ClassName
								LEFT JOIN
									".$this->course_db.".user_config as uc
								ON
									(ps.CourseUserID = uc.user_id AND uc.allow_review = 1)
								LEFT JOIN
									".$this->course_db.".web_portfolio as wp
								ON
									uc.web_portfolio_id = wp.web_portfolio_id
								LEFT JOIN
									".$this->course_db.".grouping_function as gf
								ON
									gf.function_id = wp.web_portfolio_id
								LEFT JOIN
									".$this->course_db.".user_group as ug
								ON
									ug.group_id = gf.group_id
								WHERE
									iu.UserID != '".$ParUserID."' AND
									iu.RecordType = '2' AND
									iu.RecordStatus = '1' AND
									ps.UserKey IS NOT NULL AND
									ps.IsSuspend = 0 AND
									wp.web_portfolio_id IS NOT NULL
									$conds
							";

			$set2 = $lpf_gDB->returnArray($sql);
		}

		return array_merge($set1, $set2);
	}
	
	# Generate crypted string (web portfolio id + user id) for a Student
	function GET_STUDENT_CRYPTED_STRING($ParWebPortfolioID, $ParUserID){
		return base64_encode($ParWebPortfolioID . "join" . $ParUserID);
	}
	
	# Decrypt crypted string to (web portfolio id + user id)
	function GET_STUDENT_DECRYPTED_STRING($ParCryptedStr){
		return split("join", base64_decode($ParCryptedStr));
	}
	
	# Get user_course_id of user in iPortfolio
	function GET_USER_COURSE_ID($ParUserID)
	{
		global $eclass_db, $lpf_gDB;

		$sql =	"
							SELECT
								user_course_id
							FROM
								{$eclass_db}.user_course
							WHERE
								user_id = '".$this->IP_USER_ID_TO_EC_USER_ID($ParUserID)."' AND
								course_id = '".$this->course_id."'
						";
		$ReturnVal = $lpf_gDB->returnVector($sql);

		return $ReturnVal[0];
	}
	
	# Generate learning portfolio comment list for display
	function GEN_LP_COMMENT_LIST($ParPageInfoArr, $ParWebPortfolioID, $ParUserID)
	{
		global $image_path, $intranet_session_language, $LAYOUT_SKIN, $no_record_msg;
	
		$lp_comment_list = $this->GET_LP_COMMENT_LIST($ParWebPortfolioID, $ParUserID);
		$lu = new libuser();
		
		# If not "display all records", calculate the rows to be displayed
		if($ParPageInfoArr[0] != "")
		{
			$FirstRow = ($ParPageInfoArr[1]-1) * $ParPageInfoArr[0];
			$LastRow = $ParPageInfoArr[1] * $ParPageInfoArr[0];
		}
		
		$ReturnStr =	"<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";
		
		for($i=0; $i<count($lp_comment_list); $i++)
		{
			if($ParPageInfoArr[0] == "" || ($i >= $FirstRow && $i < $LastRow))
			{		
				$user_id = $this->EC_USER_ID_TO_IP_USER_ID($lp_comment_list[$i]['author_user_id']);

				$user_obj = $lu->returnUser($user_id);
				switch($user_obj[0]['RecordType'])
				{
					# Teacher
					case 1:
						$photo_link = $image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg";
						$name_str = ($intranet_session_language=="b5"?$user_obj[0]['ChineseName']:$user_obj[0]['EnglishName']);
						break;
					# Student
					case 2:
						list($photo_filepath, $photo_link) = $this->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
						if(!file_exists($photo_filepath))
							$photo_link = $image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg";
						$name_str = ($intranet_session_language=="b5"?$user_obj[0]['ChineseName']:$user_obj[0]['EnglishName'])." (".$user_obj[0]['ClassName']."-".$user_obj[0]['ClassNumber'].")";
						break;
					default:
						$name_str = "&nbsp;";
						$photo_link = $image_path."/".$LAYOUT_SKIN."/iPortfolio/no_photo.jpg";
						break;
				}
				
				# Remove Comment
				//$RemoveCommentStr = ($user_id == $ParUserID || $this->IS_IPF_ADMIN() || $user_obj[0]['RecordType'] == 1) ? "<a href='javascript:checkRemove(".$lp_comment_list[$i]['review_comment_id'].")'><img src=\"".$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif\" border=\"0\" width=\"12\" height=\"12\"></a>" : "&nbsp;";
				$cur_user_obj = $lu->returnUser($_SESSION['ck_intranet_user_id']);
				$RemoveCommentStr = ($user_id == $_SESSION['ck_intranet_user_id'] || $this->IS_IPF_ADMIN() || $cur_user_obj[0]['RecordType'] == 1) ? "<a href='javascript:checkRemove(".$lp_comment_list[$i]['review_comment_id'].")'><img src=\"".$image_path."/".$LAYOUT_SKIN."/management/icon_delete.gif\" border=\"0\" width=\"12\" height=\"12\"></a>" : "&nbsp;";

				$ReturnStr .=	"
												<tr>
													<td>
														<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
															<tr>
																<td width=\"100\">
																	<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">
																		<tr>
																			<td width=\"120\" align=\"center\">
																				<table width=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
																					<tr>
																						<td><div id=\"div\"><span><img src=\"".$photo_link."\" width=\"45\" height=\"60\" border=\"0\"></span> </div></td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																		<tr>
																			<td width=\"120\" align=\"center\" class=\"tabletext\">".$name_str."</td>
																		</tr>
																	</table>
																</td>
																<td align=\"left\" valign=\"top\">
																	<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																		<tr>
																			<td width=\"25\" height=\"7\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_01.gif\" width=\"25\" height=\"7\"></td>
																			<td height=\"7\" background=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_02.gif\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_02.gif\" width=\"12\" height=\"7\"></td>
																			<td width=\"9\" height=\"7\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_03.gif\" width=\"9\" height=\"7\"></td>
																		</tr>
																		<tr>
																			<td width=\"25\" valign=\"top\" background=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_04.gif\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_04b.gif\" width=\"25\" height=\"40\"></td>
																			<td valign=\"top\" bgcolor=\"#FFFFFF\" class=\"tabletext\">
																				<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
																					<tr>
																						<td align=\"right\" class=\"tabletext forumtablerow\">
																							<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
																								<tr>
																									<td align=\"left\">".$RemoveCommentStr."</td>
																									<td align=\"right\" class=\"tabletext\">".$lp_comment_list[$i]['inputdate']."</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr>
																						<td class=\"tabletext\">".$lp_comment_list[$i]['comment']."</td>
																					</tr>
																					<tr>
																						<td height=\"10\" class=\"tabletext\"><img src=\"".$image_path."/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"10\"></td>
																					</tr>
																				</table>
																			</td>
																			<td width=\"9\" background=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_06.gif\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_06.gif\" width=\"9\" height=\"80\"></td>
																		</tr>
																		<tr>
																			<td width=\"25\" height=\"10\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_07.gif\" width=\"25\" height=\"10\"></td>
																			<td height=\"10\" background=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_08.gif\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_08.gif\" width=\"9\" height=\"10\"></td>
																			<td width=\"9\" height=\"10\"><img src=\"".$image_path."/$LAYOUT_SKIN/ecomm/forum_bubble_re_09.gif\" width=\"9\" height=\"10\"></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											";
			}
		}
		
		if(count($lp_comment_list) == 0)
		{
			$ReturnStr .=	"
											<tr>
												<td align=\"center\" class=\"tabletext\">
												".$no_record_msg."
												</td>
											</tr>
										";
		}
		
		$ReturnStr .=	"</table>\n";
		
		return array($ReturnStr, $lp_comment_list);

	}
	
	# Generate template selection table
	function GEN_TEMPLATE_SELECTION_TABLE($ParTemplateSelected)
	{
		global $image_path, $LAYOUT_SKIN, $button_preview, $i_status_activated, $eclass_httppath, $button_apply;
	
		$TemplateArr = $this->GET_LEARNING_PORTFOLIO_TEMPLATE_LIST();

		$ReturnStr =	"<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" bgcolor=\"#FFFFFF\">\n";
	
		for($i=0; $i<count($TemplateArr); $i++)
		{
			if($i%3 == 0)
				$ReturnStr .= "<tr>\n";

			if(substr($TemplateArr[$i], -2) == $ParTemplateSelected)
				$ReturnStr .=	"
												<td align=\"center\">
													<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
														<tr>
															<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_01.gif\" width=\"5\" height=\"5\"></td>
															<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_02.gif\" width=\"5\" height=\"5\"></td>
															<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_03.gif\" width=\"5\" height=\"5\"></td>
														</tr>
														<tr>
															<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_04.gif\" width=\"5\" height=\"5\"></td>
															<td>
																<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
																	<tr>
																		<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".substr($TemplateArr[$i], -2).".jpg\" width=\"210\" height=\"150\" name=\"template[]\" id=\"".$TemplateArr[$i]."\" style=\"\" /></td>
																	</tr>
																	<tr>
																		<td align=\"right\" bgcolor=\"#89bf6c\">
																			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
																				<tr>
																					<td height=\"30\" class=\"tabletext\"><strong>#".substr($TemplateArr[$i], -2)." <span class=\"tabletopnolink\">(".$i_status_activated.")</span></strong></td>
																					<td align=\"right\" class=\"tabletext\">&nbsp;</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
															<td width=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_06.gif\" width=\"5\" height=\"5\"></td>
														</tr>
														<tr>
															<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_07.gif\" width=\"5\" height=\"5\"></td>
															<td height=\"5\" background=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_08.gif\" width=\"5\" height=\"5\"></td>
															<td width=\"5\" height=\"5\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/LP_on_bg_09.gif\" width=\"5\" height=\"5\"></td>
														</tr>
													</table>
												</td>
											";
			else
				$ReturnStr .=	"
												<td align=\"center\">
													<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border: solid 1px #cccccc; margin:5px;\">
														<tr>
															<td><img src=\"".$image_path."/".$LAYOUT_SKIN."/iPortfolio/template_look_".substr($TemplateArr[$i], -2).".jpg\" width=\"210\" height=\"150\" name=\"template[]\" id=\"".$TemplateArr[$i]."\" style=\"opacity:0.4;filter:alpha(opacity=40)\" /></td>
														</tr>
														<tr>
															<td align=\"right\" bgcolor=\"#cccccc\">
																<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">
																	<tr>
																		<td height=\"30\" class=\"tabletext\">#".substr($TemplateArr[$i], -2)."</td>
																		<td align=\"right\" class=\"tabletext\">
																			<input type=\"button\" class=\"formsubbutton\" value=\"".$button_preview."\"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" onClick=\"newWindow('http://".$eclass_httppath."system/portfolio/site_templates/template_".substr($TemplateArr[$i], -2)."/index_main.html', 93)\">
																			<input type=\"button\" class=\"formsubbutton\" value=\"".$button_apply."\"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" onClick=\"jSELECT_TEMPLATE('".substr($TemplateArr[$i], -2)."')\">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											";
			
			if($i%3 == 2 || $i+1 == count($TemplateArr))
				$ReturnStr .= "</tr>\n";
		}
		
		$ReturnStr .= "</table>\n";
		
		return $ReturnStr;
	}
	
	# Get learning portfolio template list
	function GET_LEARNING_PORTFOLIO_TEMPLATE_LIST()
	{
		global $eclass_filepath;

		# Open the folder containing portfolio template files
		if ($handle = opendir($eclass_filepath.'/system/portfolio/site_templates'))
		{
	    while(false !== ($template = readdir($handle)))
			{
				if ($template != "." && $template != "..")
	        $ReturnArr[] = $template;
	    }
	    closedir($handle);
		}

		return $ReturnArr;
	}
	
	# Generate friend multiple selection list
	function GEN_FRIEND_SELECTION_STUDENT($ParUserID)
	{
		# Get friends
		$FriendArr = $this->GET_STUDENT_FRIEND_LIST($ParUserID);

		# Reorganize array for generating selection list
		for($i=0; $i<count($FriendArrTemp); $i++)
		{
			$FriendArr[$i] = array($FriendArr[$i]['FriendID'], $FriendArr[$i]['DisplayName']);
		}
		
		$ReturnStr = getSelectByArray($FriendArr, "size='15' multiple='multiple' name='StudentID[]' id='StudentID[]' class='formtextbox'", "", 0, 1);
			
		return $ReturnStr;
	}
	
	# Get student friend list
	function GET_STUDENT_FRIEND_LIST($ParUserID)
	{
		global $intranet_db, $lpf_gDB;

		$namefield = getNameFieldByLang2("iu.");
		$ClassNumberField = getClassNumberField("iu.");

		$sql =	"
							SELECT
								uf.FriendID,
								CONCAT('(', iu.ClassName, ' - ', $ClassNumberField, ') ', {$namefield}) as DisplayName
							FROM
								".$this->course_db.".user_friend AS uf
							INNER JOIN
								{$intranet_db}.INTRANET_USER as iu
							ON
								uf.FriendID = iu.UserID
							WHERE
								uf.UserID = '".$ParUserID."'
						";
		$ReturnArr = $lpf_gDB->returnArray($sql);

		return $ReturnArr;
	}
	
	# Update learning portfolio chosen
	function UPDATE_LP_TEMPLATE_SELECT($ParWebPortfolioID, $ParUserID, $ParTemplate)
	{
		global $lpf_gDB;
		$CourseUserID = $this->IP_USER_ID_TO_EC_USER_ID($ParUserID);

		# Check if the learning portfolio is initialized by student
		$sql =	"
							SELECT
								count(*)
							FROM
								".$this->course_db.".user_config
							WHERE
								web_portfolio_id = '".$ParWebPortfolioID."' AND
								user_id = '".$CourseUserID."'
						";
		$RowCount = $lpf_gDB->returnVector($sql);

		# Determine whether add or update record
		if($RowCount[0] > 0)
			$sql =	"
								UPDATE
									".$this->course_db.".user_config
								SET
									template_selected = '".$ParTemplate."',
									modified = NOW()
								WHERE
									web_portfolio_id = '".$ParWebPortfolioID."' AND
									user_id = '".$CourseUserID."'
							";
		else
			$sql =	"
								INSERT INTO
									".$this->course_db.".user_config
										(user_id, template_selected, inputdate, modified, web_portfolio_id, allow_review)
								VALUES
									('".$CourseUserID."', '".$ParTemplate."', NOW(), NOW(), '".$ParWebPortfolioID."', '0')
							";

		return $lpf_gDB->db_db_query($sql);
	}
	
	# Add student friends
	function ADD_STUDENT_FRIEND($ParUserID, $ParFriendIDs)
	{
		global $lpf_gDB;
		$FriendIDArr = is_array($ParFriendIDs) ? $ParFriendIDs : array($ParFriendIDs);

		for($i=0; $i<count($FriendIDArr); $i++)
		{
			if($FriendIDArr[$i] != "")
			{
				$sql =	"
									INSERT INTO
										".$this->course_db.".user_friend
											(UserID, FriendID, TimeInput, TimeModified)
									VALUES
										('".$ParUserID."', '".$FriendIDArr[$i]."', NOW(), NOW())
								";

				$lpf_gDB->db_db_query($sql);
			}
		}
	}
	
	# Delete student friends
	function DELETE_STUDENT_FRIEND($ParUserID)
	{
		global $lpf_gDB;
		$sql =	"
							DELETE FROM
								".$this->course_db.".user_friend
							WHERE
								UserID = '".$ParUserID."'
						";

		$lpf_gDB->db_db_query($sql);
	}
	
	# Set sharing status of learning portfolio
	function SET_ALLOW_REVIEW($ParWebPortfolioID, $ParUserID, $ParAllowReview)
	{
		global $lpf_gDB;
		$CourseUserID = $this->IP_USER_ID_TO_EC_USER_ID($ParUserID);

		# Check if the learning portfolio is initialized by student
		$sql =	"
							SELECT
								count(*)
							FROM
								".$this->course_db.".user_config
							WHERE
								web_portfolio_id = '".$ParWebPortfolioID."' AND
								user_id = '".$CourseUserID."'
						";
		$RowCount = $lpf_gDB->returnVector($sql);

		# Determine whether add or update record
		if($RowCount[0] > 0)
			$sql =	"
								UPDATE
									".$this->course_db.".user_config
								SET
									allow_review = '".$ParAllowReview."',
									modified = NOW()
								WHERE
									web_portfolio_id = '".$ParWebPortfolioID."' AND
									user_id = '".$CourseUserID."'
							";
		else
			$sql =	"
								INSERT INTO
									".$this->course_db.".user_config
										(user_id, template_selected, inputdate, modified, web_portfolio_id, allow_review)
								VALUES
									('".$CourseUserID."', '01', NOW(), NOW(), '".$ParWebPortfolioID."', '".$ParAllowReview."')
							";

		return $lpf_gDB->db_db_query($sql);
	}
	
	# Add new comment to learning portfolio
	function ADD_LEARNING_PORTFOLIO_COMMENT($ParLPCommentArr){

		global $lpf_gDB;
		$sql =	"
							INSERT INTO
								".$this->course_db.".review_comment
									(iPortfolio_user_id, comment, author, author_user_id, author_host, web_portfolio_id, inputdate, modified)
							VALUES
								('".$ParLPCommentArr['iPortfolio_user_id']."', '".$ParLPCommentArr['comment']."', '".$ParLPCommentArr['author']."', '".$ParLPCommentArr['author_user_id']."', '".$ParLPCommentArr['author_host']."', '".$ParLPCommentArr['web_portfolio_id']."', NOW(), NOW())
						";

		return $lpf_gDB->db_db_query($sql);
	}
	
	# Translate User ID in eclass to User ID in IP
	function EC_USER_ID_TO_IP_USER_ID($ParCourseUserID){
		global $eclass_db, $intranet_db, $lpf_gDB;

		# Select from user course
		$sql =	"
							SELECT
								iu.UserID
							FROM
								{$eclass_db}.user_course as uc
							LEFT JOIN
								{$intranet_db}.INTRANET_USER as iu
							ON
								iu.UserEmail = uc.user_email
							WHERE
								uc.user_id = '".$ParCourseUserID."' AND
								uc.course_id = '".$this->course_id."'
						";
		$ReturnVal = $lpf_gDB->returnVector($sql);

		return $ReturnVal[0];
	}
	
	# Generate publish notes selection table
	function GEN_NOTES_FOR_PUBLISH($ParWebPortfolioID, $ParUserID){

		global $iPort;

		list($NotesArrL1, $NotesArrL2, $NotesArrL3) = $this->GET_NOTES_FOR_PUBLISH($ParWebPortfolioID, $ParUserID);

		$ReturnStr =	"
										<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
											<tr class=\"tabletop\">
												<td width=\"25\" class=\"tabletopnolink\">&nbsp;</td>
												<td class=\"tabletopnolink\">".$iPort["report_section"]." / ".$iPort["component"]."</td>
											</tr>
									";
		
		for($i=0; $i<count($NotesArrL1); $i++)
		{
			$a_no = $NotesArrL1[$i][1];
			$RowStyleL1 = $NotesArrL1[$i][9] == "1" ? "tablerow1":"tablerow2";
			$CellStyleL1 = $NotesArrL1[$i][9] == "1" ? "row_on":"row_off";
		
			$ReturnStr .= "
											<tr class=\"".$RowStyleL1."\">
												<td align=\"center\" valign=\"top\" class=\"".$CellStyleL1."\"><input name=\"notes_student_id[]\" level=\"1\" a_no=\"".$a_no."\" type=\"checkbox\" value=\"".$NotesArrL1[$i][0]."\" ".($NotesArrL1[$i][9]=="1"?"checked":"")." onClick=\"jTRIGGER_CHILDREN(this, 2, ".$a_no.")\" /></td>
										";
			if(count($NotesArrL2[$a_no]) > 0)
			{
				$ReturnStr .=	"
												<td class=\"".$CellStyleL1." tabletext\">
													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
														<tr>
															<td align=\"left\">".$NotesArrL1[$i][6]."</td>
														</tr>
													</table>
													<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-top: solid 1px #CCCCCC\">
											";
											
				for($j=0; $j<count($NotesArrL2[$a_no]); $j++)
				{
					$b_no = $NotesArrL2[$a_no][$j][2];
					$RowStyleL2 = $NotesArrL2[$a_no][$j][9] == "1" ? "tablerow1":"tablerow2";
					$CellStyleL2 = $NotesArrL2[$a_no][$j][9] == "1" ? "row_on":"row_off";
					
					$ReturnStr .=	"
													<tr class=\"".$RowStyleL2."\">
														<td width=\"25\" align=\"center\" valign=\"top\" class=\"".$CellStyleL2."\"><input name=\"notes_student_id[]\" level=\"2\" a_no=\"".$a_no."\" b_no=\"".$b_no."\" type=\"checkbox\" value=\"".$NotesArrL2[$a_no][$j][0]."\" ".($NotesArrL2[$a_no][$j][9]=="1"?"checked":"")." onClick=\"jTRIGGER_CHILDREN(this, 3, ".$a_no.", ".$b_no."); triggerParents(this.form, '|".$NotesArrL1[$i][0]."|', this.checked)\" /></td>
												";
														
					if(count($NotesArrL3[$a_no][$b_no]) > 0)
					{
						$ReturnStr .=	"
														<td class=\"".$CellStyleL2." tabletext\">
															<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
																<tr>
																	<td align=\"left\">".$NotesArrL2[$a_no][$j][6]."</td>
																</tr>
															</table>
															<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-top: solid 1px #CCCCCC\">
													";
													
						for($k=0; $k<count($NotesArrL3[$a_no][$b_no]); $k++)
						{
							$RowStyleL3 = $NotesArrL3[$a_no][$b_no][$k][9] == "1" ? "tablerow1":"tablerow2";
							$CellStyleL3 = $NotesArrL3[$a_no][$b_no][$k][9] == "1" ? "row_on":"row_off";
							
							$ReturnStr .=	"
															<tr class=\"".$RowStyleL3."\">
																<td width=\"25\" align=\"center\" class=\"".$CellStyleL3."\"><input name=\"notes_student_id[]\" a_no=\"".$a_no."\" b_no=\"".$b_no."\" level=\"3\" type=\"checkbox\" value=\"".$NotesArrL3[$a_no][$b_no][$k][0]."\" ".($NotesArrL3[$a_no][$b_no][$k][9]=="1"?"checked":"")." onClick=\"triggerParents(this.form, '|".$NotesArrL1[$i][0]."||".$NotesArrL2[$a_no][$j][0]."|', this.checked)\" /></td>
																<td class=\"".$CellStyleL3." tabletext\">".$NotesArrL3[$a_no][$b_no][$k][6]."</td>
															</tr>
														";
						}
						
						$ReturnStr .=	"
																</table>
															</td>
														</tr>
													";
					}
					else
						$ReturnStr .=	"			
															<td class=\"".$CellStyleL2." tabletext\">".$NotesArrL2[$a_no][$j][6]."</td>
														</tr>
													";
				}
				$ReturnStr .=	"
														</table>
													</td>
												</tr>
											";
			}
			else
				$ReturnStr .=	"
													<td class=\"".$CellStyleL1." tabletext\">".$NotesArrL1[$i][6]."</td>
												</tr>
											";
		}
		
		$ReturnStr .=	"</table>";
		
		return $ReturnStr;
	}
	
	# Get notes of learning portfolio
	function GET_NOTES_FOR_PUBLISH($ParWebPortfolioID, $ParUserID){
		global $lpf_gDB;
		$CourseUserID = $this->IP_USER_ID_TO_EC_USER_ID($ParUserID);
		# get notes
//		$fieldname = ($ck_memberType=="T") ? "if(status<>1, CONCAT('<font color=".$this->private_color.">',title, '</font>'), title)" : "title";
		$sql =	"
							SELECT
								notes_student_id,
								a_no,
								b_no,
								c_no,
								d_no,
								e_no,
								title,
								url,
								readflag,
								status,
								modified,
								description,
								if(starttime='0000-00-00 00:00:00', '', starttime) as starttime,
								if(endtime='0000-00-00 00:00:00', '', endtime) as endtime,
								RecordType
							FROM
								".$this->course_db.".notes_student
							WHERE
								user_id = '".$CourseUserID."' AND
								web_portfolio_id = '".$ParWebPortfolioID."'
							ORDER BY
								a_no, b_no, c_no, d_no, e_no
						";
		$NotesArr = $lpf_gDB->returnArray($sql);

		# Organize data into an associative array
		for($i=0; $i<count($NotesArr); $i++)
		{
			$note_level = $this->GET_NOTES_LEVEL($NotesArr[$i][1], $NotesArr[$i][2], $NotesArr[$i][3], $NotesArr[$i][4], $NotesArr[$i][5]);

			if($note_level == 1)
				$ReturnArrL1[] = $NotesArr[$i];
			else if($note_level == 2)
				$ReturnArrL2[$NotesArr[$i][1]][] = $NotesArr[$i];
			else if($note_level == 3)
				$ReturnArrL3[$NotesArr[$i][1]][$NotesArr[$i][2]][] = $NotesArr[$i];
		}

		return array($ReturnArrL1, $ReturnArrL2, $ReturnArrL3);
	}
	
	# Get level of notes
	function GET_NOTES_LEVEL($ParA, $ParB, $ParC, $ParD, $ParE){

		# Level 1
		if ($ParA != 0 && $ParB == 0 && $ParC == 0 && $ParD == 0 && $ParE == 0)
			return 1;
		# Level 2
		else if($ParA != 0 && $ParB != 0 && $ParC == 0 && $ParD == 0 && $ParE == 0)
			return 2;
		# Level 3
		else if($ParA != 0 && $ParB != 0 && $ParC != 0 && $ParD == 0 && $ParE == 0)
			return 3;
		# Level 4
		else if($ParA != 0 && $ParB != 0 && $ParC != 0 && $ParD != 0 && $ParE == 0)
			return 4;
		# Level 5
		else if($ParA != 0 && $ParB != 0 && $ParC != 0 && $ParD != 0 && $ParE != 0)
			return 5;
		else
			return 0; // undefined
	}

	function GEN_PUBLISH_LP_CLASS_SELECTION()
	{
		global $i_alert_pleaseselect, $i_general_WholeSchool;
		
		$PublishedLPClass = $this->GET_PUBLISH_LEARNING_PORTFOLIO_CLASS_LIST();
		
		# Reorganize data
		for($i=0; $i<count($PublishedLPClass); $i++)
		{
			$PublishedLPClass[$i] = array($PublishedLPClass[$i][0], $PublishedLPClass[$i][0]);
		}
		
		$ReturnStr = getSelectByArray($PublishedLPClass, "name='ClassName' id='ClassName' class='formtextbox'", "", 0, 1);
		
		return $ReturnStr;
	}
	
	# Copy portfolio files to a folder for burning
	function COPY_PORTFOLIO_FILES($ParClassName, $ParComment)
	{
		global $eclass_filepath, $intranet_session_language;

		$fs = new libfilesystem();

		# To check whether the /eclass/files/portfolio_burnging folder exist and create it if not exist
		$folder_tmp = $this->file_path."/portfolio_2_burn";
		if(!file_exists($folder_tmp))
		{
			$fs->folder_new($folder_tmp);
		}

		# To check whether the /eclass/files/portfolio_burning/$ParClassName folder exist. If exists, delete if and created again.
		$folder_tmp .= "/".trim($ParClassName);
		$return_folder = $folder_tmp;
		if(file_exists($folder_tmp))
		{
			$fs->folder_remove_recursive($folder_tmp);
		}
		$fs->folder_new($folder_tmp);

		$Students = $this->GET_STUDENTS_FOR_COPY_PORTFOLIO($ParClassName);

		for($i=0; $i<sizeof($Students); $i++)
		{
			list($user_id, $WebSAMSRegNo) = $Students[$i];
			$WebSAMSRegNo = str_replace("#", "", $WebSAMSRegNo);
			$folder_target = $folder_tmp."/".$WebSAMSRegNo;

			if(file_exists($folder_target))
			{
				$fs->folder_remove_recursive($folder_target);
			}
			$fs->folder_new($folder_target);

			$wp_id_arr = $this->GET_STUDENT_ALL_PUBLISHED_PORTFOLIO($user_id);

			# Copy published web portfolio files to the destination
			if(sizeof($wp_id_arr)!=0)
			{
				# prepare web portfolio selection
				$web_portfolio_select = "<SELECT onChange='load_page(this.value,".$user_id.");' name='selected_wid'>";

				for($j=0; $j<sizeof($wp_id_arr); $j++)
				{
					list($wp_id, $wp_title) = $wp_id_arr[$j];
					$web_portfolio_select .= "<OPTION value='$wp_id'>".$wp_title."</OPTION>";

					$student_folder = "student_u".$user_id."_wp".$wp_id;
					$folder_source = $this->file_path."/".$this->course_db."/portfolio/".$student_folder;

// 					exec("cp -r $folder_source $folder_target");
					exec("cp -r ".OsCommandSafe($folder_source).' '.OsCommandSafe($folder_target));
					
					
					//$this->replaceImagePath($student_folder, $folder_target."/".$student_folder."/index_main.html");

					# if comment required
					if($ParComment==1)
					{
						# create student_comment.html
						$comment_list = $this->GET_COMMENT_LIST_FOR_CD_BURNING($wp_id, $user_id);
						$comment_content = str_replace("STUDENT_COMMENT_LIST", $comment_list, $this->comment_template);
						$fs->file_write($comment_content, $folder_target."/".$student_folder."/student_comment.html");
					}
				}
				$web_portfolio_select .= "</SELECT>";

				# create the index.php for the student folder
				$first_student_folder = "student_u".$user_id."_wp".$wp_id_arr[0][0];
				$student_url = $first_student_folder."/index_main.html";

				# if comment required
				if($ParComment==1)
				{
					$comment_url = $first_student_folder."/student_comment.html";
					$index_content = str_replace("STUDENT_URL", $student_url, $this->index_template_full);
					$index_content = str_replace("STUDENT_COMMENT", $comment_url, $index_content);
				}
				else
				{
					$index_content = str_replace("STUDENT_URL", $student_url, $this->index_template);
				}
				$fs->file_write($index_content, $folder_target."/index.html");

				# prepare the web_portfolio selection and create the student_comment_menu.html for the student folder
				$menu_content = ($ParComment==1) ? str_replace("WEB_PORTFOLIO_SELECT", $web_portfolio_select, $this->menu_template_full) : str_replace("WEB_PORTFOLIO_SELECT", $web_portfolio_select, $this->menu_template);
				$fs->file_write($menu_content, $folder_target."/student_comment_menu.html");

				############## prepare auto run part ########################
				# prepare the autorun.inf for the student folder
				$autofun_template = "[AutoRun]\n";
				$autofun_template .= "open=autohtml.exe /s\n";
				$autofun_template .= "icon=iportfolio.ico";
				$fs->file_write($autofun_template, $folder_target."/autorun.inf");

				# prepare the showhtml.ini for the student folder
				$showhtml_template = "[Display]\n";
				$showhtml_template .= "htmlfile=\index.html";
				$fs->file_write($showhtml_template, $folder_target."/showhtml.ini");

				# copy the autohtml exe file and the icon
				$fs->file_copy($eclass_filepath."/system/portfolio/cd_burning/autohtml.exe", $folder_target."/autohtml.exe");
				$fs->file_copy($eclass_filepath."/system/portfolio/cd_burning/iportfolio.ico", $folder_target."/iportfolio.ico");
				##############################################################

				$lang = ($intranet_session_language=="en")?"eng":"chib5";
				# copy images files to the student folder
				$source_image_path = $eclass_filepath."/images/portfolio_".$lang."/Icon";
				$source_bg = $source_image_path."/SliverBG.gif";
				$source_comment_icon = $source_image_path."/icon_WriteComment.gif";
				$target_bg_image_path = $folder_target."/images";
				if(!file_exists($target_bg_image_path))
				{
					$fs->folder_new($target_bg_image_path);
				}
				$fs->file_copy($source_bg, $target_bg_image_path);
				$fs->file_copy($source_comment_icon, $target_bg_image_path);
			}
		}

		return $return_folder;
	}
	
	# Get student list of a class which has published learning portfolio
	function GET_STUDENTS_FOR_COPY_PORTFOLIO($ParClassName){
		global $eclass_db, $intranet_db, $lpf_gDB;

		$ClassNumberField = getClassNumberField("iu.");
		$sql =	"
							SELECT DISTINCT
								ps.CourseUserID,
								ps.WebSAMSRegNo
							FROM
								{$intranet_db}.INTRANET_USER AS iu
							LEFT JOIN
								{$eclass_db}.PORTFOLIO_STUDENT AS ps
							ON
								ps.UserID = iu.UserID
							LEFT JOIN
								".$this->course_db.".user_config as uc
							ON
								ps.CourseUserID = uc.user_id
							WHERE
								iu.ClassName = '".$ParClassName."' AND
								ps.WebSAMSRegNo IS NOT NULL AND
								ps.UserKey IS NOT NULL AND
								iu.RecordType = '2' AND
								iu.RecordStatus = '1' AND
								ps.IsSuspend = 0 AND
								uc.notes_published IS NOT NULL
							ORDER BY
								$ClassNumberField,
								iu.EnglishName
						";

		return $lpf_gDB->returnArray($sql);
	}
	
	# Get ID and title of publised Portfolios
	function GET_STUDENT_ALL_PUBLISHED_PORTFOLIO($ParCourseUserID){
		global $lpf_gDB;
		$sql =	"
							SELECT DISTINCT
								wp.web_portfolio_id,
								wp.title
							FROM
								".$this->course_db.".web_portfolio as wp
							LEFT JOIN
								".$this->course_db.".user_config as uc
							ON
								wp.web_portfolio_id = uc.web_portfolio_id
							WHERE
								uc.user_id = '".$ParCourseUserID."' AND
								uc.notes_published IS NOT NULL
							ORDER BY
								uc.notes_published DESC
				";
		$row = $lpf_gDB->returnArray($sql);

		return $row;
	}
	
	# iPortfolio Comment for CD burning
	function GET_COMMENT_LIST_FOR_CD_BURNING($ParWebPortfolioID, $ParCourseUserID){
		global $ec_iPortfolio, $image_path, $lpf_gDB;

		$sql =	"
							SELECT
								comment,
								author,
								modified
							FROM
								".$this->course_db.".review_comment
							WHERE
								iPortfolio_user_id = '".$ParCourseUserID."' AND
								web_portfolio_id = '".$ParWebPortfolioID."'
							ORDER BY
								inputdate DESC
						";
		$row = $lpf_gDB->returnArray($sql);

		if(sizeof($row)!=0)
		{
			for ($i=0; $i<sizeof($row); $i++)
			{
				list($comment, $author, $modified) = $row[$i];
				$comment = nl2br($comment);

				$rx .= "<TR align=middle><TD>\n";
				$rx .= "<TABLE cellSpacing=0 cellPadding=1 width='100%' bgColor='#FFD5C9' border=0>\n";
				$rx .= "<TR>\n";
				$rx .= "<TD><TABLE cellSpacing=0 cellPadding=3 width=100% border=0>\n";
				$rx .= "<TR>\n";
				$rx .= "<TD height=70 bgcolor='#FFFFCC'><TABLE cellSpacing=0 cellPadding=5 width=100% height='100%' border=0><tr><td style=\"filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#FFFFF4', endColorStr='#FFFFCC', gradientType='1')\"><span style='line-height:145%'>$comment </span></td></tr></table></TD>\n";
				$rx .= "</TR>\n";
				$rx .= "<TR>\n";
				$rx .= "<TD width='100%' height=20 style=\"filter:progid:DXImageTransform.Microsoft.Gradient(startColorStr='#FFEFDF', endColorStr='#FFCCCC', gradientType='1')\" ><span style='FONT-SIZE:11px; FONT-FAMILY:Arial; color:#BB4455;'>".$ec_iPortfolio['review_author']."{$author}, $modified</span></TD>\n";
				$rx .= "</TR>\n";
				$rx .= "</TABLE></TD>\n";
				$rx .= "</TR>\n";
				$rx .= "</TABLE>\n";
				$rx .= "</TD></TR>\n";
				$rx .= "<TR align=middle><TD>&nbsp;</TD></TR>\n";
			}
		}
		else
		{

		}

		return $rx;
	}
	
	# Delete portfolio files prepared
	function REMOVE_PORTFOLIO_FILES($ParClassName)
	{
		$folder_tmp = $this->file_path."/portfolio_2_burn/".$ParClassName;
		$folder_tmp = OsCommandSafe($folder_tmp);
		if(file_exists($folder_tmp))
		{
			exec("rm -r \"$folder_tmp\"");
		}

		$x = (!file_exists($folder_tmp)) ? 1 : 0;

		return $x;
	}

/***************************************************************************
 * Eric Yip (20091105): The following functions should not be placed here
 *      since they are not part of LP functionality
 *      Use similar functions in libpf-formclass.php
 ***************************************************************************/

	# Generate selection list for classes, which have activated students
	function GEN_CLASS_SELECTION_STUDENT($ParClassName="")
	{
		global $i_alert_pleaseselect, $i_general_WholeSchool;
	
		# Get classes with activated students
		$ClassActivatedNo = $this->GET_CLASS_STUDENT_ACTIVATED();
		if(is_array($ClassActivatedNo))
		{
			# Classes are stored in keys
			$ClassActivated = array_keys($ClassActivatedNo);
		
			$ClassArr = $this->GET_CLASS_LIST_DATA();
			if(is_array($ClassArr))
			{
				for($i=0; $i<count($ClassArr); $i++)
				{
					# If class includes classes with activated students, add to array
					if(
							in_array($ClassArr[$i]['ClassName'], $ClassActivated) &&
							($ParClassLevelID == "" || $ParClassLevelID == $ClassArr[$i]['ClassLevelID']) &&
							(!isset($ClassNameActivated) || !in_array($ClassArr[$i]['ClassName'], $ClassNameActivated))
						)
					{
						$ClassNameActivated[] = $ClassArr[$i]['ClassName'];
					}
				}
			}
			
			# Reorganize array for generating selection list
			for($i=0; $i<count($ClassNameActivated); $i++)
			{
				$ClassNameActivated[$i] = array($ClassNameActivated[$i], $ClassNameActivated[$i]);
			}
		}
		
		$ReturnStr = getSelectByArrayTitle($ClassNameActivated, "name='ClassName' id='ClassName' class='formtextbox' onChange='jCHANGE_FIELD(\"classname\")'", $i_alert_pleaseselect, $ParClassName);
		
		return $ReturnStr;
	}
	
	# Generate selection list for students, which have activated students
	function GEN_STUDENT_SELECTION_STUDENT($ParClassName)
	{
		global $ck_intranet_user_id;

		# Get classes with activated students
		$StudentArrTemp = $this->GET_STUDENT_LIST_DATA($ParClassName);
		if(is_array($StudentArrTemp))
		{		
			# Reorganize array for generating selection list
			for($i=0; $i<count($StudentArrTemp); $i++)
			{
				if($StudentArrTemp[$i]['UserID'] != $ck_intranet_user_id)
					$StudentArr[] = array($StudentArrTemp[$i]['UserID'], $StudentArrTemp[$i]['DisplayName']);
			}
			
			$ReturnStr = getSelectByArray($StudentArr, "size='21' multiple='multiple' ame='StudentID[]' id='StudentID[]' class='formtextbox'", "", 0, 1);
		}
			
		return $ReturnStr;
	}
	
	# Remove learning portfolio comments
	function DELETE_LP_COMMENT($ParCommentID)
	{
		global $lpf_gDB;
		$sql =	"
					DELETE FROM
						".$this->course_db.".review_comment
					WHERE
						review_comment_id='".$ParCommentID."'
				";
		$lpf_gDB->db_db_query($sql);
	}
	
	# Get student portfolio file path
	function GET_STUDENT_PORTFOLIO_PATH($ParWebPortfolioID, $ParUserID){
		global $eclass_filepath, $eclass_httppath;

		$user_data = $this->GET_USER_FOLDER_ID($ParWebPortfolioID, $ParUserID);
		$file_main = "/files/".$this->course_db."/portfolio/".$user_data["folder_name"]."/index_main.html";

		if (file_exists($eclass_filepath."/".$file_main))
			$file_main = "http://".$eclass_httppath.$file_main;
		else
			$file_main = null;

		return $file_main;
	}
	
	# Get user folder id and name
	function GET_USER_FOLDER_ID($ParWebPortfolioID, $ParUserID){
		global $eclass_db, $lpf_gDB;

		$sql =	"
							SELECT
								ps.CourseUserID
							FROM
								".$this->course_db.".web_portfolio as wp
							LEFT JOIN
								".$this->course_db.".user_config as ucf
							ON
								wp.web_portfolio_id = ucf.web_portfolio_id
							INNER JOIN
								".$eclass_db.".PORTFOLIO_STUDENT as ps
							ON
								ucf.user_id = ps.CourseUserID
							WHERE
								wp.web_portfolio_id = '".$ParWebPortfolioID."' AND
								ps.UserID = '".$ParUserID."'
						";

		$CourseUserID = $lpf_gDB->returnVector($sql);
		$CourseUserID = $CourseUserID[0];

		$folderName = "student_u{$CourseUserID}_wp".$ParWebPortfolioID;

		$sql =	"
							SELECT
								FileID
							FROM
								{$eclass_db}.eclass_file
							WHERE
								Title='$folderName' AND
								Category=7 AND
								IsDir=1 AND
								VirPath IS NULL
						";
		$row = $lpf_gDB->returnVector($sql);
/*
		if (($folderID=$row[0])=="")
		{
			include_once("$eclass_filepath/src/includes/php/lib-user.php");
			include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");

			# create folder if necessary

			$lu = new libuser($user_id, $this->course_db);
			$fm = new fileManager($this->course_id, 7, "");
			$fm->user_name = addslashes($lu->user_name());
			$fm->memberType = "S";
			$fm->permission = "";
			$fm->maxSize = $this->returnPersonalFolderSize($user_id);
			$scheme_obj = $this->getSchemeInfo($ParWebPortfolioID);
			$folder_description = addslashes($scheme_obj[0]["title"]." (User Folder)");
			$folderID = $fm->createFolderDB($folderName, $folder_description, "", 7);
		}
*/
		return Array("folder_id"=>$folderID, "folder_name"=>$folderName);
	}
	
	/*****************************************************************/
	# Generate first row number and last row number 
	function GEN_PAGE_ROW_NUMBER($ParRowCount, $ParPageDivision, $ParCurrentPage)
	{
		# If "show all in a page",
		# first row is 1, last row is number of all rows
		if($ParPageDivision == "")
		{
			$FirstPage = $ParRowCount == 0 ? 0 : 1;
			$LastPage = $ParRowCount;
		}
		# calculate according to page division and current page
		else
		{
			$FirstPage = $ParRowCount == 0 ? 0 : ($ParCurrentPage-1) * $ParPageDivision + 1;
			$LastPage = min($ParRowCount,($ParCurrentPage*$ParPageDivision));
		}
		
		$ReturnStr = $FirstPage." - ".$LastPage;
		return $ReturnStr;
	}
	
	# Generate "page" selection list
	function GEN_PAGE_SELECTION($ParRowCount, $ParPageDivision, $ParCurrentPage, $ParTag)
	{
		# If "show all in a page", one page only
		if($ParPageDivision != "")
		{
			for($i=1; $i<=ceil($ParRowCount/$ParPageDivision); $i++)
			{
				$PageArr[] = array($i, $i);
			}
		}
		else
			$PageArr[] = array(1, 1);
		
		$ReturnStr = getSelectByArray($PageArr, $ParTag, $ParCurrentPage, 0, 1);
		
		return $ReturnStr;
	}
	
	# Generate "records per page" selection list
	function GEN_PAGE_DIVISION_SELECTION($ParPageDivision, $ParTag, $ParStep=1)
	{
		global $i_general_all;
	
		# increment of 10 per step
		# Todo: 1 per step for debug -> 10 per step for actual use
		for($i=1; $i<=10; $i++)
		{
			$DivisionArr[] = array($i*$ParStep, $i*$ParStep);
		}
		
		$ReturnStr = getSelectByArrayTitle($DivisionArr, $ParTag, $i_general_all, $ParPageDivision, false, 2);
		
		return $ReturnStr;
	}
	
		# Get template title
	function GET_TEMPLATE_TITLE($ParTemplateIDs)
	{
		global $lpf_gDB;
		$sql =	"
							SELECT
								Title
							FROM
								".$this->course_db.".eclass_file
							WHERE
								FileID IN (".$ParTemplateIDs.")
						";
		$ReturnArr = $lpf_gDB->returnVector($sql);

		return $ReturnArr;
	}
	
  function GET_LP_INFO($my_web_portfolio_id){
    global $lpf_gDB;
  
    $course_db = $this->GET_CLASS_VARIABLE("course_db");
  
    $sql = "SELECT
        			web_portfolio_id, title, instruction, status, starttime, deadline, folder_size
        		FROM
        			{$course_db}.web_portfolio
        		WHERE
        			web_portfolio_id='$my_web_portfolio_id'
           ";
    $ra = $lpf_gDB->returnArray($sql);
    
    return $ra;
  }
	
	
	
  function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    $this->{$ParVariableName} = $ParValue;
  }
  
  function GET_CLASS_VARIABLE($ParVariableName)
  {
    return $this->{$ParVariableName};
  }
  
}

?>
