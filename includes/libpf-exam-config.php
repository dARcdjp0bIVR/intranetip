<?php 

// EXAM ID
define("EXAMID_HKAT", 1);
define("EXAMID_HKDSE", 2);
define("EXAMID_MOCK", 3);
define("EXAMID_YEAREND", 4);
define("EXAMID_YEAREND_OVERALL", 5);
define("EXAMID_JUPAS", 6);
define("EXAMID_HKDSE_APPEAL", 7);
define("EXAMID_EXITTO", 8);

define("EXAM_REPORT_TYPE_CUSTOM", 0);
define("EXAM_REPORT_TYPE_MOCK", 1);
define("EXAM_REPORT_TYPE_SCHOOL_EXAM", 2);

define("EXAM_REPORT_PREDICT_METHOD_PERCENTILE", 0);
define("EXAM_REPORT_PREDICT_METHOD_CONFIDENCE_INTERVAL", 1);

$DSE_GRADE = array('U','1','2','3','4','5','5*','5**');

// DSE code Map to Websams code
$DSE_MAP_WEBSAMS = array(
		'080' => 'A010',
		'090' => 'A050',
		'165' => 'A020',
		'170' => 'A060',
		'22S' => 'A030',
		'23S' => 'A031',
		'24S' => 'A032',
		'045' => 'A130',
		'070' => 'A140',
		'21N' => 'A165',
		'315' => 'A150',
		'85N' => 'A160',
		'72S' => 'A210',
		'73S' => 'A211',
		'11N' => 'A170',
		'12N' => 'A171',
		'13N' => 'A172',
		'31N' => 'A180',
		'71N' => 'A190',
		'81N' => 'A200',
		'075' => 'A070',
		'135' => 'A080',
		'210' => 'A100',
		'235' => 'A110',
		'41N' => 'A090',
		'71S' => 'A120',
		'21S' => 'A220',
		'83S' => 'A230',
		'41S' => 'A240',
		'265' => 'A040',
		'200' => 'C8682',
		'215' => 'C8683',
		'216' => 'C8281',
		'230' => 'C8687',
		'398' => 'C8685',
		'425' => 'C8686',
		'590' => 'B590',
		'592' => 'B592',
		'595' => 'B595',
		'596' => 'B596',
		'597' => 'B597',
		'599' => 'B599',
		'609' => 'B609',
		'610' => 'B610',
		'611' => 'B611',
		'612' => 'B612',
		'615' => 'B615',
		'616' => 'B616',
		'617' => 'B617',
		'618' => 'B618',
		'624' => 'B624',
		'627' => 'B627',
		'628' => 'B628',
		'631' => 'B631',
		'639' => 'B639',
		'640' => 'B640',
		'643' => 'B643',
		'646' => 'B646',
		'649' => 'B649',
		'650' => 'B650',
		'651' => 'B651',
		'652' => 'B652',
		'653' => 'B653',
		'654' => 'B654',
		'655' => 'B655',
		'656' => 'B656',
		'657' => 'B657',
		'658' => 'B658',
		'659' => 'B659',
		'660' => 'B660',
		'661' => 'B661',
		'662' => 'B662',
		'663' => 'B663',
		'664' => 'B664',
		'665' => 'B665',
		'666' => 'B666',
		'667' => 'B667',
		'668' => 'B668',
		'669' => 'B669',
		'670' => 'B670',
		'671' => 'B671',
		'672' => 'B672',
		'673' => 'B673',
		'674' => 'B674',
		'675' => 'B675',
		'676' => 'B676',
		'677' => 'B677',
		'678' => 'B678',
		'679' => 'B679',
		'680' => 'B680',
		'681' => 'B681',
		'683' => 'B683',
		'684' => 'B684',
		'685' => 'B685',
		'686' => 'B686',
        '687' => 'B687',
        '688' => 'B688',
        '689' => 'B689',
        '690' => 'B690',
        '691' => 'B691',
		'801' => 'B801',
		'802' => 'B802',
		'803' => 'B803',
		'805' => 'B805',
		'806' => 'B806',
		'807' => 'B807',
		'808' => 'B808',
		'809' => 'B809',
		'810' => 'B810',
		'811' => 'B811',
		'812' => 'B812',
		'813' => 'B813',
		'814' => 'B814',
		'815' => 'B815',
		'816' => 'B816',
		'817' => 'B817',
		'818' => 'B818',
		'819' => 'B819',
		'820' => 'B820',
		'821' => 'B821',
		'822' => 'B822',
		'823' => 'B823',
        '824' => 'B824',
        '825' => 'B825',
        '826' => 'B826',
        '692' => 'B692',
        '693' => 'B693',
        '694' => 'B694',
        '695' => 'B695',        
        '697' => 'B697',
        '698' => 'B698',
        '699' => 'B699',
        '700' => 'B700',
        '701' => 'B701',
        '702' => 'B702',
        '703' => 'B703',
        '704' => 'B704',
        '705' => 'B705',
        '706' => 'B706',
        '827' => 'B827',
        '828' => 'B828',
        '829' => 'B829',
);

$DSE_MAP_WEBSAMS_CMP = array(
		"A010" => array(
				"1" => "080-P1",
				"2" => "080-P2",
				"3" => "080-P3",
				"4" => "080-P4",
				"5" => "080-P5"
		),
		"A020" => array(
				"1" => "165-P1",
				"2" => "165-P2",
				"3" => "165-P3",
				"4" => "165-P4"
		),
		"A165" => array(
				"A161" => "A161",
				"A162" => "A162",
				"A163" => "A163"
		),
		"C8682-4" => "C8682-4",
		"C8687-4" => "C8687-4",
		"C8281-4" => "C8281-4",
		"C8685-4" => "C8685-4",
		"C8686-4" => "C8686-4"
);

// JUPAS
$JUPAS_CONFIG['Offer']['No offer'] = '-999';
$JUPAS_CONFIG['Offer']['NO OFFER'] = '-999';
$JUPAS_CONFIG['Funding']['SELF-FINANCING'] = 'SELF';
$JUPAS_CONFIG['Funding']['UGC-FUNDED'] = 'UGC';
$JUPAS_CONFIG['Funding']['SSSDP'] = 'SSSDP';
$JUPAS_CONFIG['Round']['SUBSEQUENT ROUND'] = 'SR';
$JUPAS_CONFIG['Round']['MAIN ROUND'] = 'MR';
$JUPAS_CONFIG['Round']['CLEARING ROUND'] = 'CR';
$JUPAS_CONFIG['Status']['PENDING'] = 'P';
$JUPAS_CONFIG['Status']['ACCEPTED'] = 'A';
$JUPAS_CONFIG['Status']['DECLINED'] = 'D';
$JUPAS_CONFIG['Status']['UNKNOWN'] = 'U';
$JUPAS_CONFIG['Institution']['HKBU'] = 'BU';
$JUPAS_CONFIG['Institution']['POLYU'] = 'PU';
$JUPAS_CONFIG['Institution']['OUHK'] = 'OU';
$JUPAS_CONFIG['Institution']['CUHK'] = 'CU';
$JUPAS_CONFIG['Institution']['HKUST'] = 'UST';
$JUPAS_CONFIG['Institution']['CITYU'] = 'CITY';
$JUPAS_CONFIG['Institution']['HKU'] = 'HKU';
$JUPAS_CONFIG['Institution']['HKIED'] = 'IED';
$JUPAS_CONFIG['Institution']['SSSDP'] = 'SSSDP';
$JUPAS_CONFIG['Institution']['LINGNANU'] = 'LU';
$JUPAS_CONFIG['Institution']['EDUHK'] = 'IED';
$JUPAS_CONFIG['ProgType']['DEGREE'] = 'D';
$JUPAS_CONFIG['ProgType']['SUBDEGREE'] = 'SUB-D';
$JUPAS_CONFIG['ProgType']['UNCLASSIFIED'] = 'U';


$JUPAS_CONFIG['ExitToInstitution']['BU'] = 'BU';
$JUPAS_CONFIG['ExitToInstitution']['PU'] = 'PU';
$JUPAS_CONFIG['ExitToInstitution']['OU'] = 'OU';
$JUPAS_CONFIG['ExitToInstitution']['CU'] = 'CU';
$JUPAS_CONFIG['ExitToInstitution']['UST'] = 'UST';
$JUPAS_CONFIG['ExitToInstitution']['CITY'] = 'CITY';
$JUPAS_CONFIG['ExitToInstitution']['HKU'] = 'HKU';
$JUPAS_CONFIG['ExitToInstitution']['IED'] = 'IED';
$JUPAS_CONFIG['ExitToInstitution']['SSSDP'] = 'SSSDP';
$JUPAS_CONFIG['ExitToInstitution']['LU'] = 'LU';


/*
$JUPAS_CONFIG['Funding']['Self-financing'] = 'SELF';
$JUPAS_CONFIG['Funding']['UGC-funded'] = 'UGC';
$JUPAS_CONFIG['Funding']['SSSDP'] = 'SSSDP';
$JUPAS_CONFIG['Round']['Subsequent Round'] = 'SR';
$JUPAS_CONFIG['Round']['Main Round'] = 'MR';
$JUPAS_CONFIG['Round']['Clearing Round'] = 'CR';
$JUPAS_CONFIG['Status']['pending'] = 'P';
$JUPAS_CONFIG['Status']['accepted'] = 'A';
$JUPAS_CONFIG['Status']['declined'] = 'D';
$JUPAS_CONFIG['Status']['unknown'] = 'U';
$JUPAS_CONFIG['Institution']['HKBU'] = 'BU';
$JUPAS_CONFIG['Institution']['PolyU'] = 'PU';
$JUPAS_CONFIG['Institution']['OUHK'] = 'OU';
$JUPAS_CONFIG['Institution']['CUHK'] = 'CU';
$JUPAS_CONFIG['Institution']['HKUST'] = 'UST';
$JUPAS_CONFIG['Institution']['CityU'] = 'CITY';
$JUPAS_CONFIG['Institution']['HKU'] = 'HKU';
$JUPAS_CONFIG['Institution']['HKIEd'] = 'IED';
$JUPAS_CONFIG['Institution']['SSSDP'] = 'SSSDP';
$JUPAS_CONFIG['Institution']['LingnanU'] = 'LU';
$JUPAS_CONFIG['Institution']['EdUHK'] = 'IED';
 */

// Table of Distribution
$TABLE_OF_DISTRIBUTION['0.9'] = array(
    '120' => 1.282,
    '110' => 1.284,
    '100' => 1.287,
    '90' => 1.289,
    '80' => 1.291,
    '70' => 1.294,
    '60' => 1.296,
    '50' => 1.307,
    '40' => 1.303,
    '30' => 1.31,
    '29' => 1.311,
    '28' => 1.313,
    '27' => 1.314,
    '26' => 1.315,
    '25' => 1.316,
    '24' => 1.318,
    '23' => 1.319,
    '22' => 1.321,
    '21' => 1.323,
    '20' => 1.325,
    '19' => 1.328,
    '18' => 1.33,
    '17' => 1.333,
    '16' => 1.337,
    '15' => 1.341,
    '14' => 1.345,
    '13' => 1.35,
    '12' => 1.356,
    '11' => 1.363,
    '10' => 1.372,
    '9' => 1.383,
    '8' => 1.397,
    '7' => 1.415,
    '6' => 1.44,
    '5' => 1.476,
    '4' => 1.533,
    '3' => 1.638,
    '2' => 1.886,
    '1' => 3.078,
);
$TABLE_OF_DISTRIBUTION['0.95'] = array(
    '120' => 1.645,
    '110' => 1.649,
    '100' => 1.654,
    '90' => 1.658,
    '80' => 1.662,
    '70' => 1.667,
    '60' => 1.671,
    '50' => 1.691,
    '40' => 1.684,
    '30' => 1.697,
    '29' => 1.699,
    '28' => 1.701,
    '27' => 1.703,
    '26' => 1.706,
    '25' => 1.708,
    '24' => 1.711,
    '23' => 1.714,
    '22' => 1.717,
    '21' => 1.721,
    '20' => 1.725,
    '19' => 1.729,
    '18' => 1.734,
    '17' => 1.74,
    '16' => 1.746,
    '15' => 1.753,
    '14' => 1.761,
    '13' => 1.771,
    '12' => 1.782,
    '11' => 1.796,
    '10' => 1.812,
    '9' => 1.833,
    '8' => 1.86,
    '7' => 1.895,
    '6' => 1.943,
    '5' => 2.015,
    '4' => 2.132,
    '3' => 2.353,
    '2' => 2.92,
    '1' => 6.314,
);
$TABLE_OF_DISTRIBUTION['0.975'] = array(
    '120' => 1.96,
    '110' => 1.967,
    '100' => 1.973,
    '90' => 1.98,
    '80' => 1.987,
    '70' => 1.993,
    '60' => 2,
    '50' => 2.01,
    '40' => 2.021,
    '30' => 2.042,
    '29' => 2.045,
    '28' => 2.048,
    '27' => 2.052,
    '26' => 2.056,
    '25' => 2.06,
    '24' => 2.064,
    '23' => 2.069,
    '22' => 2.074,
    '21' => 2.08,
    '20' => 2.086,
    '19' => 2.093,
    '18' => 2.101,
    '17' => 2.11,
    '16' => 2.12,
    '15' => 2.131,
    '14' => 2.145,
    '13' => 2.16,
    '12' => 2.179,
    '11' => 2.201,
    '10' => 2.228,
    '9' => 2.262,
    '8' => 2.306,
    '7' => 2.365,
    '6' => 2.447,
    '5' => 2.571,
    '4' => 2.776,
    '3' => 3.182,
    '2' => 4.303,
    '1' => 12.706,
);
?>