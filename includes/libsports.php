<?php
# using:

/* *******************************************
 * Modification log
 *  2019-10-11 Philips
 *  	modified getAthleticNumDeatil() - when the interface language is english, display Group1 Lane1 Order1 
 * 	2019-09-25 Philips [2019-0503-1912-35164]
 * 		added Get_Student_Profile_Attendance_Data()
 * 	2019-09-23 Philips [2019-0503-1912-35164]
 * 		added insertEventGroupSchedule(), updateEventGroupSchedule(), deleteEventGroupSchedule(), deleteAllEventGroupSchedule, initEventGroupSchedule() for event group schedule
 * 		modified retrieveStudentIDAndName(), retrieveStudentIDAndNameByClassNameAndAgeGroup(), retrieveStudentIDAndNameByHouseAndAgeGroup() to allow customize class and class number display
 * 
 * 	2019-09-19 Philips [2019-0503-1912-35164]
 * 		modified getAthleticNumDeatil(), added FixedPos, event_font_family, event_font_size to customize eventdetail field
 * 
 *  2019-05-30 Bill [2019-0522-0932-16066]
 *      modified Get_Enroled_Participant_List2(), to return students only participated class relay events
 *  
 *  2019-05-21 Bill [2018-1019-1043-03066]
 *      modified retrieveClassRelayName(), to return relay name with target lang
 *  
 *  2019-04-15 Bill [2019-0301-1144-56289]
 *      modified retrieveEventGroupExtInfo()
 *      modified getClassRelayGroupSelection(), retrieveClassRelayClassGroup(), retrieveClassRelayGroupStudent(), updateClassRelayGroupStudent(), insertClassRelayGroupStudent(), deleteClassRelayGroupStudent()
 *      added retrieveClassRelayLaneArrangementByRound(), retrieveClassRelayGroupRankingByRound(), returnClassRelayHeatNumberByEventGroupID(), Get_Obtained_Bonus_Score_Class_Relay(), returnClassRelayLaneArrangeDetailByEventGroupID(), returnBestRecordResultOfClassRelayEvent()
 *      removed retrieveCRLaneArrangementByRound(), returnBestRecordResultOfClassRelayEventType(), 
 *  
 *  2018-10-29 Bill [2018-0628-1634-01096]
 *      modified getClassRelayGroupSelection(), to support Class Group filtering in selection
 *      added retrieveCRLaneArrangementByResult(), returnBestRecordResultOfClassRelayEventType(), to support Class Group handling in Class Relay event 
 *  
 *  2018-10-15 Bill [2018-0628-1634-01096]
 *      modified retrieveTrackFieldDetail(), returnCRLaneArrangeDetailByEventGroupID(), return Class Relay related info
 *      added returnRelatedClassRelayEvent(), returnBestRecordResultOfClassRelayEventType(), returnOldBestResultOfClassRelayEventType()
 *  
 *  2018-09-26 Bill [2018-0628-1634-01096]
 *      modified retrieveClassRelayLaneArrangement()
 *      added retrieveClassRelayType(), retrieveClassRelayClassGroup()
 *      added retrieveClassRelayGroupStudent(), updateClassRelayGroupStudent(), insertClassRelayGroupStudent(), deleteClassRelayGroupStudent()
 *      added getClassRelayGroupSelection()
 *  
 *  2018-09-05 Bill
 *      added isYearClassTeacher(), teachingYearClassList()
 * 
 *  2018-08-10 Philips
 * 		update getAthleticNumDeatil()
 * 		added retrieveClassStudent
 * 		added retrieveStudentIDAndName()
 * 		added retrieveClassStudent()
 *      retrieveClassRelayLaneMapping CRUD:
 * 		added retrieveClassRelayLaneMapping()
 * 		added updateClassRelayLaneMapping()
 * 		added insertClassRelayLaneMapping()
 * 		added deleteClassRelayLaneMapping()
 * 
 * 	2017-09-19 Bill	[2017-0918-1146-09236]
 * 		update autoLanesArrange(), to ensure all althelets arranged in middle for first round track event
 * 
 * 	2017-09-06 Bill	[2017-0405-1457-30236]
 * 		added Is_All_Trial_Included(), modified retrieveTrackFieldDetail(), to support Settings for Fields Event: Best Attempt > Best records in both First Round & Final Round
 * 
 * 	2017-02-28 Bill	[2016-0627-1013-19066]
 * 		modified Get_Student_Basic_Info(), returnStudentAgeGroup(), retrieveAgeGroupByStudentID(), retrieveStudentEnroledEvent(), to handle student with multiple age groups
 * 
 * 	2016-10-12 Bill	[2016-0927-1445-23073]
 * 		update retrieveRestrictQuotaEventGroupInfo, retrieveUnrestrictQuotaEventGroupInfo(), to add condition to skip online enrol checking if $skipOnlineEnrolChecking = true
 * 
 *  2016-06-16 Cara
 *  	modified retrieveEventAndGroupNameByEventGroupID()
 * 
 * 	2016-03-16 Bill [2016-0301-1022-11066]
 * 		update Assign_Student_To_EventGroup(), to auto assign line for students in field event
 * 
 * 	2015-12-22 Ivan [L90682]
 * 		update retrieveTFRanking() add param $finalOnly=false
 * 
 * 	2015-08-31 Bill	[2015-0828-0910-24073]
 * 		update retrieveHouseScoreSumArr() to support $sys_custom['eSports_count_open_event_in_grade_score_default_group'] for Open House Relay
 * 
 *	2013-11-14 YatWoon
 *		update Cache_House_Info(), display group name according to lang [Case#2013-1113-1441-27066]
 *
 *	2013-10-11 YatWoon
 *		update retrieveTrackFieldEventName1(), retrieveTrackFieldEventNameByCondition(), order by age group DisplayOrder [Case#2013-1009-1402-33156]
 * 
 *	2013-08-27 Roy:
 *		update retrieveGroupTopTenScore() to get current house info
 *
 *	2013-08-06 YatWoon:
 *		update Get_Enroled_Participant_List(), add "return_method"
 *
 *	2013-01-16 YatWoon:
 *		update returnTFLaneArrangeDetailByEventGroupID(), add classname and classnumber
 *
 * 	2012-11-21 Ivan:
 * 		update returnEnrolmentRecordByYearID(), changed from "LEFT JOIN" to "INNER JOIN" for table SPORTS_EVENTGROUP to hide the record if the event has been deleted
 * 
 * 	2012-10-18 Ivan [2012-1018-1013-21073]
 * 		update retrieveStudentHouseInfo(), retrieveStudentIDAndNameByClassNameAndAgeGroup() to return records of current academic year house group only
 * 
 * 	2012-10-16 Ivan [2012-1009-1503-52066]
 * 		update returnEnrolmentRecordByYearID() to return records of current academic year classes only
 * 
 *  2012-10-08 Ivan [2012-1003-0920-51073]
 * 		update retrieveHouseStudentCount() to select "House" type group only
 * 
 *	2012-09-26	YatWoon
 *		update Get_Student_Basic_Info(), order by Year and YearClass "Sequence" rather than ClassName+ClassNumber
 *
 *	2012-09-20	YatWoon
 *		update returnTFLaneArrangeDetailByEventGroupID(), add student English name anmd CHinese name [Case#2012-0918-1832-44147]
 *
 *	2012-08-01	YatWoon
 *		update retrieveEventGroupID(), 
 *		retrieveTFResult(), return recordstatus also (for abs / abs-waive...)
 *
 *	2012-06-26	YatWoon
 *		add returnEventIsCompleted(), using in /participation/tf_record.php
 *
 *	2012-06-14	YatWoon 
 *		update searchEnrolmentRecords(), missing to check retrieve House group only; 
 *
 *	2012-06-08	YatWoon
 *		update retrieveEventGroupDetail(), retrieveRestrictQuotaEventGroupInfo(), add "EventQuota"
 *		add returnEventEnroledNo(), returnEventQuota(), retrieveUnrestrictQuotaEventGroupInfo()
 *
 *	2011-09-19	Marcus
 *		rewrite searchEnrolmentRecords, 
 *
 *	2011-01-04	YatWoon
 *		add retrieveParticipantSummary_StudentDetails(), retrieve problemed student for Genearte Athlete Number function
 *
 * 	2010-12-01 Marcus
 * 		modified Cache_House_Info(), Add return values HouseName
 * 		modified Get_Student_Basic_Info(), Order By ClassName, Number
 * 		add Get_Student_EventGroup_Attendance, return Student Attendance in all event enroled
 *
 * 	2010-11-24 Marcus
 * 		modified Get_Track_Result,Get_Field_Result, add return values,
 * 		add Get_EventGroup_Involved_Round
 * 
 * 	2010-11-17 Marcus
 * 		modified retrieveClassNumberOfStudentEnrolment, retrieveClassLevelNumberOfStudentEnrolment, 
 * 		retrieveClassLevelNumberOfEnrolment, retrieveClassStudentCount,
 * 		retrieveClassNumberOfEnrolmentAndScore, retrieveClassNumberOfStudentEnrolment,
 * 		Excluded non-exist student(no classname and number), Included Students enroled in open event.
 * 
 *	2010-11-17 YatWoon
 * 		modified FIELD_NUMBER to 6 (event arrangement > export event record > event result paper print out, display 6 columns for fields event (expect high jump) [CRM#2010-1110-1943-56071])
 *
 *	2010-11-05 YatWoon
 *		add retrieveClassLevelNumberOfStudentEnrolment(), retrieve number of student has enrolled event
 *		add retrieveClassNumberOfStudentEnrolment()
 *
 *	2010-11-04 Marcus
 *		modified Get_Track_Result_Display_Sql, retrieveStudentEventResult
 *           
 * 	2010-11-03 Marcus
 *		modified returnEventInfo retrieveHouseInfo
 *		Added Cache_House_Info(),Get_EventGroup_Enroled_Arranged_Number, Assign_Student_To_EventGroup, Get_Student_Basic_Info, Get_Event_UnAssigned_Student
 * 
 * 	2010-10-19 Marcus
 *		modified retrieveEventGroupExtInfo, Get_Pad_Track_Result_Sql
 *		lpad 0 to the time
 * 
 * 	2010-10-08 Marcus
 *		Added function Get_Admin_Helper_List
 *		modified returnTFLaneArrangeDetailByEventGroupID , Add Return Value ResultTrial1,ResultTrial2,ResultTrial3
 * 		modified FIELD_NUMBER to 3
 * 	2010-09-24 Marcus
 *		modified retrieveClassNumberOfEnrolmentAndScore, improved sql, can also get number of enrolment before lane generation.
 * 
 * 	2010-09-10 Marcus
 *		added Get_Lang_Lin, Get_Lang_Heat, Get_Event_Selection, Get_Lane_Arrangement_Setting_Table, Get_Event_Special_Lane_Arrangement 
 * 
 * 	2010-09-01 Marcus
 * 		fixed retrieveStudentHouse wrongly select duplicate record and return the first empty record...
 *
 * 	2010-08-20 Marcus
 * 		added Get_Max_Heat_Of_Round
 * 		Format_TimeStr, Format_Millisecond, Convert_Millisecond_To_Time, Get_Track_Result_To_Millisecond_Sql, Get_Track_Result_Display_Sql,
 * 	2010-07-22 Marcus
 * 		modified retrieveStudentEnroledEvent (add param to select present student only.)
 * 		modified searchEnrolmentRecords(fixed error displayed if no student in group), 
 *    	added retrieveHouseScoreSumArr to cater $sys_custom['eSports_count_open_event_in_grade_score'] and improve performance
 * 
 *	2010-07-21 Marcus
 *		modified retrieveStudentEnroledEvent (add param to exclude open event),retrieveStudentInfo (add return value Gender )
 *
 *	2010-05-31 YatWoon
 *		update searchEnrolmentRecords(), add currect academic year checking for House Group
 *
 *	20100528 YatWoon:
 *		update retrieveParticipantSummary (check no DOB with DateOfBirth='0000-00-00 00:00:00')
 * 
 * 	20091209 Marcus:
 * 		modified retrieveTFResult (add return value Result Metre and Result Time) 
 * 
 * *******************************************/

###################################################################
# Creator : Kenneth Wong
# Creation Date : 20050729
if (!defined("LIBSPORTS_DEFINED"))         // Preprocessor directives
{
//include_once($PATH_WRT_ROOT."lang/bak/lang.marcus.$intranet_session_language.php");
 define("LIBSPORTS_DEFINED",true);
 define("GROUP_TYPE_HOUSE",4);
 define("JUMP_NUMBER", 13);
 define("FIELD_NUMBER", 6);
 define("EMPTY_ARRAY_NAME","eclass_sports_null");
 
 define("RESULT_STATUS_ABSENT",1);
 define("RESULT_STATUS_PRESENT",2);
 define("RESULT_STATUS_QUALIFY",3);
 define("RESULT_STATUS_RECORDBROKEN",4);
 define("RESULT_STATUS_FOUL",5);
 define("RESULT_STATUS_WAIVEDABSENT",6);
 
 define("EVENT_BOYS_OPEN",-1);
 define("EVENT_GIRLS_OPEN",-2);
 define("EVENT_MIXED_OPEN",-4);
 
 define("EVENT_TYPE_TRACK",1);
 define("EVENT_TYPE_FIELD",2);
 define("EVENT_TYPE_HOUSERELAY",3);
 define("EVENT_TYPE_CLASSRELAY",4);
 
 define("ROUND_TYPE_FIRSTROUND",1);
 define("ROUND_TYPE_SECONDROUND",2);
 define("ROUND_TYPE_FINALROUND",0);
 
 define("DELIMITER","|==|");
 
 class libsports extends libdb{
       var $uid;
       var $access_level;
       var $function_acl;

       # Settings
       var $numberOfLanes;     # Number of lanes in sports court
       var $scoreEnrol;        # Score to house in enrolment
       var $scorePresent;      # Score to house in Present to Event
       var $scoreAbsent;       # Score to be deducted from house in Absent to Event
       var $enrolDateStart;    # Start date of enrollment
       var $enrolDateEnd;      # End date of enrollment
       var $enrolDetails;      # Text for enrollment
       var $numberGenerationType; # Type 1/2
       var $numberGenRule1;     #
       var $numberGenRule2;     #
       var $numberGenRule3;     #
       var $numberGenAutoLength;
       var $numDays;
       var $enrolMinTotal;
       var $enrolMaxTotal;
       var $enrolMinTrack;
       var $enrolMaxTrack;
       var $enrolMinField;
       var $enrolMaxField;
       var $defaultLaneArrangement;
       var $RankPattern;
       var $HiddenAutoArrangeButton;



       function libsports()
       {
                $this->libdb();
                $this->uid = $_SESSION['UserID'];
                $this->access_level = $_SESSION['intranet_sports_right'];
                $this->function_acl = $_SESSION['intranet_sports_acl'];
                $this->retrieveSettings();
       }
       function retrieveSettings()
       {
                $sql = "SELECT NumberOfLanes, EnrolScore, PresentScore, AbsentScore,
                        EnrolDateStart, EnrolDateEnd, EnrolDetails, NumberGenerationType, Rule1,
                        Rule2, Rule3, AutoNumLength, NumOfDays, EnrolMinTotal, EnrolMaxTotal,
                        EnrolMinTrack, EnrolMaxTrack, EnrolMinField, EnrolMaxField,
                        DefaultLaneArrangement,RankPattern,HiddenAutoArrangeButton
                        FROM SPORTS_SYSTEM_SETTING";
                $temp = $this->returnArray($sql,21);
                if (sizeof($temp)==0 || $temp[0][0]=="")
                {
                    # Use default settings
                    $this->numberOfLanes = 8;
                    $this->scoreEnrol = 0;
                    $this->scorePresent = 0;
                    $this->scoreAbsent = 0;
                    $this->enrolDateStart = "";
                    $this->enrolDateEnd = "";
                    $this->enrolDetails = "";
                    $this->numberGenerationType = 1;
                    $this->numberGenRule1 = "";
                    $this->numberGenRule2 = "";
                    $this->numberGenRule3 = "";
                    $this->numberGenAutoLength = 2;
                    $this->numDays = 2;
                    $this->enrolMinTotal = 0;
                    $this->enrolMaxTotal = 3;
                    $this->enrolMinTrack = 0;
                    $this->enrolMaxTrack = 3;
                    $this->enrolMinField = 0;
                    $this->enrolMaxField = 3;
                    $this->defaultLaneArrangement = "7,5,3,1,2,4,6,8";
                    $this->RankPattern = "1223";
                    $this->HiddenAutoArrangeButton = 0;
                    $this->saveSettings();
                }
                else
                {
                    list($this->numberOfLanes, $this->scoreEnrol, $this->scorePresent, $this->scoreAbsent,
                     $this->enrolDateStart, $this->enrolDateEnd, $this->enrolDetails, $this->numberGenerationType,
                     $this->numberGenRule1, $this->numberGenRule2, $this->numberGenRule3, $this->numberGenAutoLength,
                     $this->numDays, $this->enrolMinTotal, $this->enrolMaxTotal, $this->enrolMinTrack,
                     $this->enrolMaxTrack, $this->enrolMinField, $this->enrolMaxField,
                     $this->defaultLaneArrangement,$this->RankPattern, $this->HiddenAutoArrangeButton) = $temp[0];
                }
       }
       function saveSettings()
       {
                $sql = "SELECT COUNT(*) FROM SPORTS_SYSTEM_SETTING";
                $temp = $this->returnVector($sql);
                if ($temp[0]==0)
                {
                    $sql = "INSERT INTO SPORTS_SYSTEM_SETTING () VALUES ()";
                    $this->db_db_query($sql);
                }
                $sql = "UPDATE SPORTS_SYSTEM_SETTING SET ";
                $sql .= "NumberOfLanes = '".$this->numberOfLanes."'";
                $sql .= ",EnrolScore = '".$this->scoreEnrol."'";
                $sql .= ",PresentScore = '". $this->scorePresent."'";
                $sql .= ",AbsentScore = '".$this->scoreAbsent."'";
                $sql .= ",EnrolDateStart = '". $this->enrolDateStart."'";
                $sql .= ",EnrolDateEnd = '". $this->enrolDateEnd ."'";
                $sql .= ",EnrolDetails = '". $this->enrolDetails . "'";
                $sql .= ",NumberGenerationType = '". $this->numberGenerationType."'";
                $sql .= ",Rule1 = '". $this->numberGenRule1 . "'";
                $sql .= ",Rule2 = '". $this->numberGenRule2 . "'";
                $sql .= ",Rule3 = '". $this->numberGenRule3 . "'";
                $sql .= ",AutoNumLength = '". $this->numberGenAutoLength . "'";
                $sql .= ",NumOfDays = '". $this->numDays . "'";
                $sql .= ",EnrolMinTotal = '". $this->enrolMinTotal."'";
                $sql .= ",EnrolMaxTotal = '". $this->enrolMaxTotal."'";
                $sql .= ",EnrolMinTrack = '". $this->enrolMinTrack ."'";
                $sql .= ",EnrolMaxTrack = '". $this->enrolMaxTrack ."'";
                $sql .= ",EnrolMinField = '". $this->enrolMinField . "'";
                $sql .= ",EnrolMaxField = '". $this->enrolMaxField . "'";
                $sql .= ",DefaultLaneArrangement = '". $this->defaultLaneArrangement . "'";
                $sql .= ",RankPattern = '". $this->RankPattern . "'";
                $sql .= ",HiddenAutoArrangeButton = '". $this->HiddenAutoArrangeButton . "'";
                
                $this->db_db_query($sql);
                
       }
       
      function Cache_House_Info()
       { 
       		if(empty($this->Batch_House_Info))
       		{
       			$HouseNameLang = Get_Lang_Selection("b.ChineseName","b.EnglishName"); 
       			$GroupNameLang = Get_Lang_Selection("a.TitleChinese","a.Title"); 
       			
       			$CurrentAcademicYear = Get_Current_Academic_Year_ID();
       			$sql = 	"	SELECT 
								b.HouseID,  
								a.GroupID,  
								b.EnglishName,  
								b.ChineseName,  
								b.ColorCode,  
								b.HouseCode,
								$GroupNameLang,
								$HouseNameLang HouseName
							FROM 
								INTRANET_GROUP a 
								LEFT JOIN INTRANET_HOUSE b ON a.GroupID = b.GroupID 
							WHERE 
								a.RecordType = 4
								AND  AcademicYearID = $CurrentAcademicYear
				";

				$houses = $this->returnArray($sql,6);
				$this->Batch_House_Info = $houses;
       		}
       }
       
       function retrieveEventTypeName() 
       {
                global $intranet_session_language;
                $db_field = ($intranet_session_language=="en"?"EnglishName":"ChineseName");
                $sql = "SELECT $db_field FROM SPORTS_EVENT_TYPE_NAME ORDER BY EventTypeID";
                $temp = $this->returnArray($sql,4);
                for ($i=0; $i<sizeof($temp); $i++)
                {
                     $result[] = $temp[$i][0];
                }
                return $result;
       }
       # get EventTypeName By EventTypeID
       function retrieveEventTypeNameByID($eventTypeID){
	       if($eventTypeID!=""){
                global $intranet_session_language;
                $db_field = ($intranet_session_language=="en"?"EnglishName":"ChineseName");
                $sql = "SELECT $db_field FROM SPORTS_EVENT_TYPE_NAME WHERE EventTypeID=$eventTypeID";
                $temp = $this->returnVector($sql,1);
                return $temp[0];
       	   }
       	   return "";
	   }

       function returnAccessLevel()
       {
                return $this->access_level;
       }
       function hasAccessRight()
       {
                return (sizeof($this->function_acl)>0);
       }
       function allowAccess($lvl=0)
       {
                return ($this->access_level >= $lvl);
       }
       function authAccess($lvl=0)
       {
                if ($this->allowAccess($lvl))
                {
                    return true;
                }
                else
                {
                    echo "You have no priviledge to access this page.";
                    exit();
                }
       }
       function authSportsSystem()
       {
                return $this->authAccess(1);
       }
       function convertHouseFromGroup()
       {
	       $CurrentAcademicYear = Get_Current_Academic_Year_ID();
	       
                $sql = "DELETE FROM INTRANET_HOUSE";
                $this->db_db_query($sql);
                $sql = "INSERT INTO INTRANET_HOUSE (GroupID, EnglishName, ChineseName)
                               SELECT GroupID, Title, Title
                                      FROM INTRANET_GROUP WHERE RecordType = ". GROUP_TYPE_HOUSE ." and AcademicYearID=$CurrentAcademicYear";
                $this->db_db_query($sql);
                if ($this->db_affected_rows()==0)
                {
                    return false;
                }
                else return true;
       }

       function retrieveHouseInfo($HouseID='')
       {
       			$this->Cache_House_Info();
       	
       			if(trim($HouseID)=='')
       				return 	$this->Batch_House_Info;	
       			else
       			{
       				$HouseInfoAssoc = BuildMultiKeyAssoc($this->Batch_House_Info, "HouseID");
       				return $HouseInfoAssoc[$HouseID];
       			}
//                //$sql = "SELECT HouseID, GroupID, EnglishName, ChineseName, ColorCode, HouseCode FROM INTRANET_HOUSE ORDER BY DisplayOrder";
//				$sql = 	"	SELECT 
//								b.HouseID,  
//								a.GroupID,  
//								b.EnglishName,  
//								b.ChineseName,  
//								b.ColorCode,  
//								b.HouseCode,
//								a.Title
//							FROM 
//								INTRANET_GROUP a 
//								LEFT JOIN INTRANET_HOUSE b ON a.GroupID = b.GroupID 
//							WHERE 
//								a.RecordType = 4
//				";
//				# IP25 need only display current year
//				$CurrentAcademicYear = Get_Current_Academic_Year_ID();
//				$sql .= " and  AcademicYearID = $CurrentAcademicYear";
//				
//				$houses = $this->returnArray($sql,6);
//				return $houses;
/*                if (sizeof($houses)==0)
                {
                    # Insert info from INTRANET_GROUP
                    if ($this->convertHouseFromGroup())
                    {
                        return $this->retrieveHouseInfo();
                    }
                    else return;
                }
                else return $houses;
*/
       }
       # Param : House ID in INTRANET_HOUSE
       function getHouseDetail($hid)
       {
                $sql = "SELECT GroupID, EnglishName, ChineseName, DisplayOrder, ColorCode, HouseCode
                               FROM INTRANET_HOUSE WHERE HouseID = $hid";
                $temp = $this->returnArray($sql,6);
                return $temp[0];
       }
	   
       # Param : GroupID in INTRANET_HOUSE
       function getHouseDetailByGroupID($gid)
       {
                $sql = "SELECT GroupID, EnglishName, ChineseName, DisplayOrder, ColorCode, HouseCode
                               FROM INTRANET_HOUSE WHERE GroupID = $gid";
                $temp = $this->returnArray($sql,6);
                return $temp[0];
       }
	   
       function retrieveScoreStandards()
       {
                $sql = "SELECT StandardID, Name, Position1, Position2, Position3, Position4,
                               Position5, Position6, Position7, Position8, RecordBroken,
                               Qualified FROM SPORTS_SCORE_STANDARD ORDER BY DateInput";
				return $this->returnArray($sql,12);
       }

       function retrieveScoreStandardDetail($sid)
       {
                $sql = "SELECT Name, Position1, Position2, Position3, Position4, Position5,
                               Position6, Position7, Position8, RecordBroken, Qualified
                               FROM SPORTS_SCORE_STANDARD WHERE StandardID = $sid";
                $temp = $this->returnArray($sql,11);
                return $temp[0];
       }
       function retrieveAgeGroupInfo()
       {
                $sql = "SELECT AgeGroupID, GradeChar, EnglishName, ChineseName, Gender, GroupCode,
                               DOBUpLimit, DOBLowLimit, DisplayOrder
                               FROM SPORTS_AGE_GROUP
                               ORDER BY DisplayOrder";
                               //ORDER BY Gender DESC, GradeChar ASC";
                return $this->returnArray($sql,9);
       }
       function retrieveAgeGroupIDNames()
       {
                global $intranet_session_language;
                $db_field = ($intranet_session_language=="en"?"EnglishName":"ChineseName");
                $sql = "SELECT AgeGroupID, $db_field
                               FROM SPORTS_AGE_GROUP
                               ORDER BY DisplayOrder";
                               //ORDER BY Gender DESC, GradeChar ASC";
                return $this->returnArray($sql,2);
       }
       function retrieveAgeGroupDetail($gid)
       {
                $sql = "SELECT GradeChar, EnglishName, ChineseName, Gender, GroupCode,
                               DOBUpLimit, DOBLowLimit,DisplayOrder
                               FROM SPORTS_AGE_GROUP
                               WHERE AgeGroupID = $gid";
                $temp = $this->returnArray($sql,8);
                return $temp[0];
       }
       
       function retrieveTrackFieldDetail($eventID)
       {
            $sql = "SELECT EventType, EnglishName, ChineseName, DisplayOrder, IsJump, IsAllTrialIncluded, RelayRoundType, RelayEventType
                    FROM SPORTS_EVENT WHERE EventID = '$eventID'";
            $temp = $this->returnArray($sql,5);
            return $temp[0];
       }

          # Retrieve Event Group ID by EventID and GroupID
          function retrieveEventGroupID($eventID, $groupID, $typeIDs="")
          {
                  if(is_array($typeIDs)) {
                      $types = implode($typeIDs, ",");
                  } else {
                      $types = $typeIDs;
                  }
                  
                  $type_str = $types ? " b.EventType IN ($types) AND " : "";
                  $sql = "SELECT a.EventGroupID, b.EventType
                            FROM SPORTS_EVENTGROUP as a 
                            LEFT OUTER JOIN SPORTS_EVENT as b ON a.EventID = b.EventID 
                        WHERE $type_str a.EventID = '$eventID' AND a.GroupID = '$groupID'";
                 $temp = $this->returnArray($sql, 2);

                 return $temp[0];
          }

                function retrieveEventGroupDetail($eventGroupID)
                {
                    $sql = "SELECT
                                EventGroupID, EventID, GroupID,
                                IsOnlineEnrol, CountPersonalQuota, CountHouseScore,
                                CountClassScore, CountIndividualScore, ScoreStandardID, EventCode, EventQuota
                            FROM 
                                SPORTS_EVENTGROUP 
                            WHERE 
                                EventGroupID = '$eventGroupID'";
                     $temp = $this->returnArray($sql);
                     return $temp[0];
                }

				# Retrieve Top Ten Scores of Each Age Group
				# Param: AgeGroupID
				function retrieveGroupTopTenScore($ageGroupID)
                {
				 global $intranet_session_language;

				$house_name = ($intranet_session_language=="en"?"d.EnglishName":"d.ChineseName");
				$name_field = getNameFieldWithClassNumberByLang ("b.");
				
                $CurrentAcademicYear = Get_Current_Academic_Year_ID();
					
				$sql = "SELECT 
							DISTINCT a.StudentID, 
							$name_field, 
							$house_name, 
							d.ColorCode, 
							f.AthleticNum, 
							a.RecordStatus,         
							ROUND(SUM(a.Score),1) as total
						FROM 
							SPORTS_LANE_ARRANGEMENT as a
							INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
							INNER JOIN INTRANET_USERGROUP as c ON b.UserID = c.UserID
							INNER JOIN INTRANET_GROUP as h ON c.GroupID = h.GroupID
							INNER JOIN INTRANET_HOUSE as d ON d.GroupID = h.GroupID
							INNER JOIN SPORTS_AGE_GROUP as e ON
								(e.DOBLowLimit IS NULL || b.DateOfBirth <= e.DOBLowLimit)
								AND (e.DOBUpLimit IS NULL || b.DateOfBirth >= e.DOBUpLimit)
								AND b.Gender = e.Gender
							INNER JOIN SPORTS_STUDENT_ENROL_INFO as f ON a.StudentID = f.StudentID
							INNER JOIN SPORTS_EVENTGROUP as g ON a.EventGroupID = g.EventGroupID
						WHERE 
							a.Score IS NOT NULL
							AND e.AgeGroupID = '$ageGroupID'
							AND g.CountIndividualScore = '1'
							AND h.AcademicYearID = $CurrentAcademicYear
						GROUP BY 
							a.StudentID
						ORDER BY 
							total DESC
						LIMIT 0, 10
						";
						
                     $temp = $this->returnArray($sql,7);

					 return $temp;
                }

                # Retrieve Score Summary By House ID and Age Group ID
                # Param: HouseID, AgeGroupID
                function retrieveHouseScoreSum($houseID, $ageGroupID, $IncludesOpenEvent = 0)
                {
	                global $sys_custom;
	                
					$total = 0;
	
					/*
                    $sql = "SELECT SUM(a.Score)
							FROM SPORTS_LANE_ARRANGEMENT as a,
                            INTRANET_USER as b,
                            INTRANET_USERGROUP as c,
                            INTRANET_HOUSE as d,
                            SPORTS_AGE_GROUP as e
                            WHERE a.StudentID = b.UserID
                            AND b.UserID = c.UserID
                            AND c.GroupID = d.GroupID
                            AND a.Score IS NOT NULL
                            AND (e.DOBLowLimit IS NULL || b.DateOfBirth <= e.DOBLowLimit)
                            AND (e.DOBUpLimit IS NULL || b.DateOfBirth >= e.DOBUpLimit)
                            AND b.Gender = e.Gender
                            AND d.HouseID = '$houseID'
                            AND e.AgeGroupID = '$ageGroupID'
                           ";
					*/

					 $sql = "SELECT SUM(IF(e.CountHouseScore=1,a.Score,0))
							FROM SPORTS_LANE_ARRANGEMENT as a,
                            INTRANET_USER as b,
                            INTRANET_USERGROUP as c,
                            INTRANET_HOUSE as d,
                            SPORTS_EVENTGROUP as e
                            WHERE a.StudentID = b.UserID
                            AND b.UserID = c.UserID
                            AND c.GroupID = d.GroupID
                            AND a.Score IS NOT NULL
							AND a.EventGroupID = e.EventGroupID
                            AND d.HouseID = '$houseID'
                            AND e.GroupID = '$ageGroupID'
                           ";
                    $temp = $this->returnVector($sql);
					$total_score = $total_score + $temp[0];

					$sql = "SELECT SUM(IF(b.CountHouseScore=1,a.Score,0))
							FROM SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT as a,
                            SPORTS_EVENTGROUP as b
                            WHERE a.EventGroupID = b.EventGroupID
							AND a.Score IS NOT NULL
                            AND a.HouseID = '$houseID'
                            AND b.GroupID = '$ageGroupID'
                           ";
					$temp = $this->returnVector($sql);
					$total_score = $total_score + $temp[0];

					return $total_score;
                }
                
                function retrieveHouseScoreSumArr($houseID='', $ageGroupID='', $IncludesOpenEvent = 0)
                {
                	if($houseID!='')
                	{	
                        $cond .= "AND d.HouseID = '$houseID'";
                	    $cond2 .= "AND a.HouseID = '$houseID'";
                	}
                	if($ageGroupID!='')	
                    {
                    	$cond .= "AND e.GroupID = '$ageGroupID'";        
                    	$cond2 .= "AND b.GroupID = '$ageGroupID'";        
                    }
                	
                	$HouseGroupField = "b.GroupID";
                	if($IncludesOpenEvent == 1)
                	{
                		global $sys_custom;
                		
                		//2011-1027-1506-10071 - Request score of OPEN events added to GRADE A 
                		// $sys_custom['eSports_count_open_event_in_grade_score_default_group'] was hard coded in settings.php
                		if($DefaultGroup = $sys_custom['eSports_count_open_event_in_grade_score_default_group']['SportDay'])
                		{
                			$GroupField = " IF(e.GroupID = ".EVENT_GIRLS_OPEN.", ".$DefaultGroup['F'].", IF(e.GroupID = ".EVENT_BOYS_OPEN.", ".$DefaultGroup['M'].", e.GroupID))";
                			// [2015-0828-0910-24073] SKH Tang Shiu Kiu Cust - assign score of Open House Relay to Grade A
                			$HouseGroupField = " IF(b.GroupID = ".EVENT_GIRLS_OPEN.", ".$DefaultGroup['F'].", IF(b.GroupID = ".EVENT_BOYS_OPEN.", ".$DefaultGroup['M'].", b.GroupID))";
                		}
                		else
                		{
	                		$JOINSAG .= "INNER JOIN SPORTS_AGE_GROUP SAG ON SAG.Gender = b.Gender"."\n";
								$JOINSAG .= "AND (TRIM(SAG.DOBUpLimit) = '' OR SAG.DOBUpLimit IS NULL OR DATE(b.DateOfBirth) >= SAG.DOBUpLimit )"."\n";
								$JOINSAG .= "AND (TRIM(SAG.DOBLowLimit) = '' OR SAG.DOBLowLimit IS NULL OR DATE(b.DateOfBirth) <= SAG.DOBLowLimit)"."\n";
								
							$GroupField = "SAG.AgeGroupID";
                		}	
                	}
                	else
                	{
                		$GroupField = "e.GroupID";
                	}
                	
                	$sql = "
						SELECT
							d.HouseID,
							$GroupField,
							SUM(IF(e.CountHouseScore=1,a.Score,0))
						FROM 
							SPORTS_LANE_ARRANGEMENT as a
							INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
							INNER JOIN INTRANET_USERGROUP as c ON b.UserID = c.UserID
							INNER JOIN INTRANET_HOUSE as d ON c.GroupID = d.GroupID
							INNER JOIN SPORTS_EVENTGROUP as e ON a.EventGroupID = e.EventGroupID
							$JOINSAG
						WHERE
							a.Score IS NOT NULL
							$cond
						Group By 
							d.HouseID, $GroupField
						";
					$result = $this->returnArray($sql);
					
					foreach((array)$result as $rec)
					{
						list($houseID, $GroupID, $Score) = $rec;
						$returnAry[$houseID][$GroupID] += $Score;
					}
					
					$sql = "
						SELECT 
							a.HouseID,
							$HouseGroupField,
							SUM(IF(b.CountHouseScore=1,a.Score,0))
						FROM 
							SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT as a 
							INNER JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
						WHERE
							a.Score IS NOT NULL
							$cond2
						GROUP BY
							a.HouseID, b.GroupID
						";
					$relayresult = $this->returnArray($sql);
					foreach((array)$relayresult as $rec2)
					{
						list($houseID, $GroupID, $Score) = $rec2;
						$returnAry[$houseID][$GroupID] += $Score;
					}
					
					return $returnAry;
					
                }

                function returnAgeGroupName($groupID)
                {
                    global $intranet_session_language;
                    
                    $group_name = $intranet_session_language=="en"? "EnglishName" : "ChineseName";
                    $sql = "SELECT 
                                $group_name
                            FROM 
                                SPORTS_AGE_GROUP
                            WHERE 
                                AgeGroupID = '$groupID'";
                     $temp = $this->returnVector($sql);
                     
                     return $temp[0];
                }
                
                function returnEventInfo($eventID)
                {
                                         global $intranet_session_language;
                                        $event_name = ($intranet_session_language=="en"?"EnglishName":"ChineseName");

                    $sql = "SELECT EventType, $event_name EventName, IsJump
                                                                FROM SPORTS_EVENT
                                                                WHERE EventID = '$eventID'
                                                                ";
                         $temp = $this->returnArray($sql, 3);

                     return $temp[0];
                }

                function returnStudentAthleticNum($studentID)
                {
                    $sql = "SELECT AthleticNum
                                                                        FROM SPORTS_STUDENT_ENROL_INFO
                                                                        WHERE StudentID = '$studentID'";

                         $temp = $this->returnVector($sql);

                     return $temp[0];
                }

                                function returnStudentClassName($studentID)
                {
                    $sql = "SELECT CLassName FROM INTRANET_USER WHERE UserID = '$studentID'";
                    $temp = $this->returnVector($sql);

                     return $temp[0];
                }

				// there is not any field called ClassID in INTRANET_USER , consider revise
                function returnStudentClassID($studentID)
                {
                    $sql = "SELECT ClassID FROM INTRANET_USER WHERE UserID = '$studentID'";
                    $temp = $this->returnVector($sql);

                     return $temp[0];
                }
				
				function returnStudentClassNum($studentID)
                {
                    $sql = "SELECT ClassNumber FROM INTRANET_USER WHERE UserID = '$studentID'";
                    $temp = $this->returnVector($sql);

                     return $temp[0];
                }
				
                function retrieveEventAndGroupNameByEventGroupID($eventGroupID)
                {
                        global $intranet_session_language;

                        $db_field1 = ($intranet_session_language=="en"?"b.EnglishName":"b.ChineseName");
                        $db_field2 = ($intranet_session_language=="en"?"c.EnglishName":"c.ChineseName");

/*
                    $sql = "SELECT $db_field1, $db_field2, b.EventType
                            FROM SPORTS_EVENTGROUP as a,
                            SPORTS_EVENT as b,
                            SPORTS_AGE_GROUP as c
                            WHERE a.EventGroupID = '$eventGroupID'
                            AND b.EventID = a.EventID
                            AND c.AgeGroupID = a.GroupID";
                            */
     
					$sql = "SELECT $db_field1, $db_field2, b.EventType, a.GroupID
                            FROM 
                            SPORTS_EVENTGROUP as a 
                            left join SPORTS_EVENT as b on (b.EventID = a.EventID)
                            left join SPORTS_AGE_GROUP as c on (c.AgeGroupID = a.GroupID)
                            WHERE a.EventGroupID = '$eventGroupID'
                            AND b.EventID = a.EventID
                            ";
                            
					$result1 = $this->returnArray($sql);

                     return $result1[0];
                }

                function retrieveAgeGroupName($ageGroupID)
                {
                        global $intranet_session_language, $i_Sports_Event_Boys_Open, $i_Sports_Event_Girls_Open, $i_Sports_Event_Mixed_Open;

                        if($ageGroupID=='-1')
                           return $i_Sports_Event_Boys_Open;
                        else if($ageGroupID=='-2')
                            return $i_Sports_Event_Girls_Open;
                        else if($ageGroupID=='-4')
                            return $i_Sports_Event_Mixed_Open;


                        $namefield = ($intranet_session_language=="en") ? "EnglishName" : "ChineseName";

                        $sql = "SELECT $namefield FROM SPORTS_AGE_GROUP WHERE AgeGroupID = '$ageGroupID'";
                         $temp = $this->returnVector($sql);

                     return $temp[0];
                }

                function retrieveHouseRelayLaneArrangement($eventGroupID)
                {
                        $sql = "SELECT HouseID, ArrangeOrder
                                                FROM SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT
                                                WHERE EventGroupID = '$eventGroupID'";
                         $temp = $this->returnArray($sql,2);

                     return $temp;
                }

                function retrieveClassRelayLaneArrangement($eventGroupID)
                {
                    $sql = "SELECT
                                ClassID, ArrangeOrder, ClassGroupID
                            FROM
                                SPORTS_CLASS_RELAY_LANE_ARRANGEMENT
                            WHERE
                                EventGroupID = '$eventGroupID'";
                    $temp = $this->returnArray($sql,2);
                    
                    return $temp;
                }

                # Retrieve Track Event Names
                function retrieveTrackEventIDAndName()
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "a.EnglishName" : "a.ChineseName";

                        $sql = "SELECT DISTINCT a.EventID, $namefield FROM SPORTS_EVENT as a
                                                LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventID = b.EventID
                                                WHERE a.EventType = 1
                                                ORDER BY a.DisplayOrder";
                         $temp = $this->returnArray($sql,2);

                     return $temp;
                }

                                # Retrieve Field Event Names
                function retrieveFieldEventIDAndName()
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "a.EnglishName" : "a.ChineseName";

                        $sql = "SELECT DISTINCT a.EventID, $namefield FROM SPORTS_EVENT as a
                                                LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventID = b.EventID
                                                WHERE a.EventType = 2
                                                ORDER BY a.DisplayOrder";
                         $temp = $this->returnArray($sql,2);

                     return $temp;
                }

                # Retrieve Distinct Track and Field Event Names
                function retrieveTrackFieldEventName($target_lang='')
                {
                        global $intranet_session_language;
                        
                        $target_lang = $target_lang != ''? $target_lang : $intranet_session_language;
                        $namefield = $target_lang == "en"? "a.EnglishName" : "a.ChineseName";
                        
                        $sql = "SELECT
                                    DISTINCT a.EventID, $namefield EventName
                                FROM 
                                    SPORTS_EVENT as a
                                    LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventID = b.EventID
                                WHERE 
                                    a.EventType IN (1,2)
                                ORDER BY 
                                    a.EventType, a.DisplayOrder";
                        $temp = $this->returnArray($sql,2);
                        
                        return $temp;
                }

                 # Retrieve All Groups Track and Field Event Names
                function retrieveTrackFieldEventName1()
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "a.EnglishName" : "a.ChineseName";

                        $sql = "SELECT DISTINCT b.EventID, b.GroupID, $namefield FROM SPORTS_EVENT as a
                                                LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventID = b.EventID
                                                                                                LEFT JOIN SPORTS_AGE_GROUP as c ON b.GroupID = c.AgeGroupID
                                                WHERE a.EventType IN (1,2)
                                                ORDER BY a.EventType, a.DisplayOrder, c.DisplayOrder, b.GroupID";
                         $temp = $this->returnArray($sql,3);

                     return $temp;
                }

                 # Retrieve Groups Track and Field Event Names By Condition
                function retrieveTrackFieldEventNameByCondition($con)
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "b.EnglishName" : "b.ChineseName";

                        $sql = "SELECT DISTINCT a.EventID, a.GroupID, $namefield FROM SPORTS_EVENT as b
                                                LEFT JOIN SPORTS_EVENTGROUP as a ON b.EventID = a.EventID
                                                LEFT JOIN SPORTS_AGE_GROUP as c ON a.GroupID = c.AgeGroupID
                                                LEFT JOIN SPORTS_EVENTGROUP_EXT_TRACK as e ON a.EventGroupID = e.EventGroupID
                                                LEFT JOIN SPORTS_EVENTGROUP_EXT_FIELD as f ON a.EventGroupID = f.EventGroupID
												WHERE b.EventType IN (1,2)
												$con
                                                ORDER BY b.EventType, b.DisplayOrder, c.DisplayOrder, a.GroupID";
						 $temp = $this->returnArray($sql,3);

                     return $temp;
                }
				
				
                # Retrieve All Groups House Relay
                function retrieveHouseRelayName()
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "b.EnglishName" : "b.ChineseName";

                        $sql = "SELECT DISTINCT a.EventID, a.GroupID, $namefield FROM SPORTS_EVENTGROUP as a
                                                LEFT JOIN SPORTS_EVENT as b ON a.EventID = b.EventID,
                                                                                                SPORTS_EVENTGROUP_EXT_RELAY as c
                                                WHERE b.EventType = 3
                                                                                                AND c.EventGroupID = a.EventGroupID
                                                ORDER BY b.DisplayOrder, a.GroupID";
                         $temp = $this->returnArray($sql,3);
 
                                         return $temp;
                }

				# Retrieve Groups House Relay By Condition
				#PARAM $condition
                function retrieveHouseRelayNameByCondition($con)
                {
                        global $intranet_session_language;
						
                        $namefield = ($intranet_session_language=="en") ? "b.EnglishName" : "b.ChineseName";
												
                        $sql = "SELECT DISTINCT a.EventID, a.GroupID, $namefield FROM SPORTS_EVENTGROUP as a
                                                LEFT JOIN SPORTS_EVENT as b ON a.EventID = b.EventID
												LEFT JOIN SPORTS_AGE_GROUP as c ON a.GroupID = c.AgeGroupID,
                                                    SPORTS_EVENTGROUP_EXT_RELAY as d
												WHERE b.EventType = 3
                                                    AND d.EventGroupID = a.EventGroupID
													$con
                                                ORDER BY b.DisplayOrder, a.GroupID";
                         $temp = $this->returnArray($sql,3);

                                         return $temp;
                }
				
                # Retrieve All Class Relay
                function retrieveClassRelayName($target_lang='')
                {
                        global $intranet_session_language;
                        
                        $target_lang = $target_lang != ''? $target_lang : $intranet_session_language;
                        $namefield = $target_lang == "en"? "b.EnglishName" : "b.ChineseName";
                        
                        $sql = "SELECT
                                    DISTINCT a.EventID, a.GroupID, $namefield 
                                FROM 
                                    SPORTS_EVENTGROUP as a
                                    LEFT JOIN SPORTS_EVENT as b ON a.EventID = b.EventID,
                                    SPORTS_EVENTGROUP_EXT_RELAY as c
                                WHERE 
                                    b.EventType = 4
                                    AND c.EventGroupID = a.EventGroupID
                                ORDER BY 
                                    b.DisplayOrder, a.GroupID";
                        $temp = $this->returnArray($sql,3);
                        
                        return $temp;
                }
				
				# Retrieve Class Relay By Condition
                function retrieveClassRelayNameByCondition($con)
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "b.EnglishName" : "b.ChineseName";

                        $sql = "SELECT DISTINCT a.EventID, a.GroupID, $namefield 
                        		FROM 
                        		SPORTS_EVENTGROUP as a
                                LEFT JOIN SPORTS_EVENT as b ON a.EventID = b.EventID
								LEFT JOIN SPORTS_AGE_GROUP as c ON a.GroupID = c.AgeGroupID,
								SPORTS_EVENTGROUP_EXT_RELAY as d
                                WHERE 
                                b.EventType = 4
                                AND d.EventGroupID = a.EventGroupID
								$con
                                ORDER BY b.DisplayOrder, a.GroupID";
                         $temp = $this->returnArray($sql,3);

                                         return $temp;
                }
				

                # Retrieve Distint House Relay Name
                 function retrieveDistinctHouseRelayName()
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "b.EnglishName" : "b.ChineseName";

                        $sql = "SELECT DISTINCT a.EventID, $namefield FROM SPORTS_EVENTGROUP as a
                                     LEFT JOIN SPORTS_EVENT as b ON a.EventID = b.EventID,
                                         SPORTS_EVENTGROUP_EXT_RELAY as c
                                     WHERE b.EventType = 3
                                         AND c.EventGroupID = a.EventGroupID
                                     ORDER BY b.DisplayOrder";
                         $temp = $this->returnArray($sql,2);

                     return $temp;
                }
                
                # Retrieve Distint Class Relay Name
                 function retrieveDistinctClassRelayName()
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "b.EnglishName" : "b.ChineseName";

                        $sql = "SELECT DISTINCT a.EventID, $namefield FROM SPORTS_EVENTGROUP as a
                                     LEFT JOIN SPORTS_EVENT as b ON a.EventID = b.EventID,
                                         SPORTS_EVENTGROUP_EXT_RELAY as c
                                     WHERE b.EventType = 4
                                         AND c.EventGroupID = a.EventGroupID
                                     ORDER BY b.DisplayOrder";
                         $temp = $this->returnArray($sql,2);

                     return $temp;
                }

				function returnEventTypeByEventID($eventID)
				 {

					$sql = "SELECT EventType FROM SPORTS_EVENT WHERE EventID = '$eventID'";
					$row = $this->returnVector($sql);

					return $row[0];
				 }

				 function checkFinalRoundReq($eventType, $eventGroupID)
				 {
					if($eventType==1)
					{
						$event_table = "SPORTS_EVENTGROUP_EXT_TRACK";
					}
					else if($eventType==2)
					{
						$event_table = "SPORTS_EVENTGROUP_EXT_FIELD";
					}

					$sql = "SELECT FinalRoundReq FROM $event_table WHERE EventGroupID = '$eventGroupID'";
					$row = $this->returnVector($sql);
					$final_req = $row[0];

					if($final_req==1)
						return 1;
					else
						return 0;
				 }

                # Retrieve Track and Field Event Ranking
                function retrieveTFRanking($eventID, $groupID, $finalOnly=false)
                {
                     $name_field = getNameFieldWithClassNumberByLang("c.");
                     $username_field = getNameFieldByLang("c.");
                     $field = "CONCAT(IF(c.ClassNumber IS NULL OR c.ClassNumber = '','',CONCAT(' (',c.ClassName,'-',c.ClassNumber,')')) )";
					/*
					$eventType = $this->returnEventTypeByEventID($eventID);

					$conds = "";
					if($eventType==1 || $eventType==2)
					{
						$row = $this->retrieveEventGroupID($eventID, $groupID, $eventType);
						$eventGroupID = $row[0];

						$final_req = $this->checkFinalRoundReq($eventType, $eventGroupID);
						$total_heat_num = $this->returnHeatNumberByEventGroupID($eventGroupID);

						if($final_req==1 && $total_heat_num!=1)
							$conds = "AND a.RoundType=0";
					}
					*/
					
					//added by marcus 20090713
					// Show only final round record of Field Event
					$eventType = $this->returnEventTypeByEventID($eventID);
					$row = $this->retrieveEventGroupID($eventID, $groupID, $eventType);
					$eventGroupID = $row[0];
					
					$FinalReq = $this->checkFinalRoundReq($eventType,$eventGroupID);
					//L90682 - show final result only for some cases of track events
					//$conds = ($FinalReq==1&&$eventType==2?" AND roundType = 0":"");
					$conds = ($FinalReq==1&&(($eventType==1 && $finalOnly)||$eventType==2))?" AND roundType = 0":"";
					
					# 20081013 add result in "Event Rankings" list
					$result_sql = $this->Get_Pad_Track_Result_Sql('a.').", a.ResultMetre, d.EventType, a.roundType";
                    $sql = "SELECT $name_field, a.StudentID, a.Rank, a.RecordStatus, $username_field, $field, $result_sql
                                                                FROM SPORTS_LANE_ARRANGEMENT as a
                                LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
								LEFT JOIN SPORTS_EVENT as d ON b.EventID = d.EventID,
                                                                INTRANET_USER as c
                                WHERE a.StudentID = c.UserID
                                                                AND b.EventID = '$eventID'
                                                                AND b.GroupID = '$groupID'
                                                                AND a.Rank IS NOT NULL
								AND a.Rank > 0
				$conds
                                ORDER BY a.Rank
                                                                LIMIT 0, 16";
                    $temp = $this->returnArray($sql);
					 return $temp;
                  }

                # Retrieve House Relay Event Ranking
				function retrieveHRRanking($eventID, $groupID)
				{
					global $intranet_session_language;
					
					$namefield = ($intranet_session_language=="en") ? "c.EnglishName" : "c.ChineseName";
					
					# 20081014 add result in "Event Rankings" list
					$result_sql = $this->Get_Pad_Track_Result_Sql('a.');
					
					$sql = "SELECT 
								$namefield, a.HouseID, a.Rank, a.RecordStatus, $result_sql
							FROM 
								SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT as a
								LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID,
								INTRANET_HOUSE as c
							WHERE 
								a.HouseID = c.HouseID
								AND b.EventID = '$eventID'
								AND b.GroupID = '$groupID'
								AND a.Rank IS NOT NULL
								ORDER BY a.Rank
								LIMIT 0, 8
							";
					$temp = $this->returnArray($sql,7);
					
					return $temp;
				}
				
				# Retrieve Class Relay Event Ranking
				function retrieveCRRanking($eventID, $groupID)
				{
					global $intranet_session_language;
					
					# 20081014 add result in "Event Rankings" list
					$result_sql = $this->Get_Pad_Track_Result_Sql('a.');
					$ClassTitle = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
					
					$sql = "SELECT 
								c.$ClassTitle, a.ClassID, a.Rank, a.RecordStatus, $result_sql
							FROM 
								SPORTS_CLASS_RELAY_LANE_ARRANGEMENT as a
								LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
								INNER JOIN YEAR_CLASS as c ON a.ClassID = c.YearClassID
							WHERE 
								b.EventID = '$eventID'
								AND b.GroupID = '$groupID'
								AND a.Rank IS NOT NULL
								ORDER BY a.Rank
								LIMIT 0, 8
							";
					$temp = $this->returnArray($sql,7);
					
					return $temp;
				}

                # Retrieve Student Name, Class Name and Class Number of students who have enroled the Event
                function retrieveEnroledStudentList($eventGroupID)
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "b.EnglishName" : "b.ChineseName";
                        $sql = "SELECT b.UserID, $namefield,
                                                CONCAT(b.ClassName, if(length(b.ClassNumber) = 1, CONCAT('0', b.ClassNumber), b.ClassNumber))
                                                FROM SPORTS_STUDENT_ENROL_EVENT as a
                                                LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                                                WHERE a.StudentID = b.UserID AND a.EventGroupID = '$eventGroupID' ORDER BY b.UserID";
                         $temp = $this->returnArray($sql, 3);

                         return $temp;
                }

                # Retrieve Student ID of students who have enroled the Event 
                function retrieveEnroledStudentIDList($eventGroupID)
                {
                        $sql = "SELECT StudentID
                                                FROM SPORTS_STUDENT_ENROL_EVENT
                                                WHERE EventGroupID = '$eventGroupID' ORDER BY StudentID";
                         $temp = $this->returnVector($sql);

                         return $temp;
                }

				// added by marcus 18/6
                # Retrieve Student ID of students who have enroled the Event (exclude those who are not in intranet user)
                function retrieveEnroledStudentIDListInIntranet($eventGroupID)
                {
                        $sql = "SELECT StudentID 
                                                FROM SPORTS_STUDENT_ENROL_EVENT a, INTRANET_USER b
                                                WHERE EventGroupID = '$eventGroupID' AND StudentID=UserID ORDER BY StudentID";
                         $temp = $this->returnVector($sql);

                         return $temp;
                }
				
                # Retrieve event enroled count by EventGroupID
                function retrieveEventEnroledCount($eventGroupID)
                {
                        $sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_EVENT as a
                                                LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
                                                WHERE a.EventGroupID = b.EventGroupID AND b.EventGroupID = '$eventGroupID'";
                         $temp = $this->returnVector($sql);

                     return $temp[0];
                }
                
				#added by marcus 18/6
                # Retrieve event enroled count by EventGroupID (exclude those not in INTRANET_USER)
                function retrieveEventEnroledCountInIntranet($eventGroupID)
                {
                        $sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_EVENT as a
                                                LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
												LEFT JOIN INTRANET_USER as c ON a.StudentID = c.UserID
                                                WHERE a.EventGroupID = b.EventGroupID AND a.StudentID = c.UserID AND b.EventGroupID = '$eventGroupID'";
                         $temp = $this->returnVector($sql);

                     return $temp[0];
                }

				
                # Retrieve house relay enroled count by EventGroupID
                function retrieveHREnroledCount($eventGroupID)
                {
                        $sql = "SELECT COUNT(*) FROM SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT 
                                WHERE EventGroupID = '$eventGroupID'";
                         $temp = $this->returnVector($sql);

                     return $temp[0];
                }
                
                # Retrieve class relay enroled count by EventGroupID
                function retrieveCREnroledCount($eventGroupID)
                {
                        $sql = "SELECT COUNT(*) FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
                                WHERE EventGroupID = '$eventGroupID'";
                         $temp = $this->returnVector($sql);

                     return $temp[0];
                }

                # Retrieve arranged event count by EventGroupID (for track and field events)
                function retrieveTFLaneArrangedEventCount($eventGroupID)
                {
                        $sql = "SELECT COUNT(a.EventGroupID) FROM SPORTS_LANE_ARRANGEMENT as a
                                                WHERE a.EventGroupID = '$eventGroupID' AND a.RoundType=1";
                         $temp = $this->returnVector($sql);

                     return $temp[0];
                }

                # Check whether lanes have been arranged for the house relay event
                function returnHRLaneArrangedFlag($eventGroupID)
                {
                                        $sql = "SELECT COUNT(*) FROM SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT
                                                        WHERE EventGroupID = '$eventGroupID'";
                    $temp = $this->returnVector($sql);

                                        if($temp[0]==0)
                                                return 0;
                                        else
                                                return 1;
               }
               
				#uncommented by marcus 6/7/2009			   
                # Check whether lanes have been arranged for the class relay event
                 function returnCRLaneArrangedFlag($eventGroupID)
                 {
                     $sql = "SELECT COUNT(*) FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT
                                                         WHERE EventGroupID = '$eventGroupID'";
                     $temp = $this->returnVector($sql);
                                         if($temp[0]==0)
                                                 return 0;
                                         else
                                                 return 1;
                }

                # Retrieve Track and Field Event Lange Arrangement Detail
                # Return Array with: StudentID, Heat, ArrangeOrder
                function retrieveTFLaneArrangeDetail($eventGroupID, $roundType)
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language=="en") ? "b.EnglishName" : "b.ChineseName";

                        $sql = "SELECT a.StudentID, $namefield,
                                        CONCAT(b.ClassName, if(length(b.ClassNumber) = 1, CONCAT('0', b.ClassNumber), b.ClassNumber)), a.Heat, a.ArrangeOrder
                                        FROM SPORTS_LANE_ARRANGEMENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID WHERE a.EventGroupID='$eventGroupID' AND a.RoundType='$roundType' ORDER BY a.Heat, a.ArrangeOrder";
                        $temp = $this->returnArray($sql, 5);
                        return $temp; 
                }

                # Retrieve EventGroupID enroled by EventID and GroupID
                function retrieveLaneArrangedEventCount($eventGroupID)
                {
                        $sql = "SELECT COUNT(*) FROM SPORTS_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID'";
                        $temp = $this->returnVector($sql);

                     return $temp[0];
                }

                # Retrieve House Info For Selection
                function retrieveHouseSelectionInfo()
                {
                        global $intranet_session_language;

                        $namefield = ($intranet_session_language == "b5") ? "ChineseName" : "EnglishName";
						$CurrentAcademicYear = Get_Current_Academic_Year_ID();
						$sql = "
							SELECT 
								HouseID, 
								$namefield 
							FROM 
								INTRANET_HOUSE a 
								INNER JOIN INTRANET_GROUP b ON a.GroupID = b.GroupID 
							WHERE
								 b.RecordType = 4
								 AND b.AcademicYearID = '$CurrentAcademicYear' ";
                        $temp = $this->returnArray($sql,2);
            			return $temp;
                }

                # Retrieve House Info from StudentID
                # Return Array width HouseID, Name, ColorCode and HouseCode
                function retrieveStudentHouseInfo($studentID)
                {
                        global $intranet_session_language;
                        
                        $CurrentAcademicYear = Get_Current_Academic_Year_ID();
                        
                        $house_namefield = ($intranet_session_language=="en") ? "c.EnglishName" : "c.ChineseName";

                        $sql = "SELECT $house_namefield, c.HouseID, c.ColorCode, c.HouseCode FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID, INTRANET_HOUSE as c WHERE b.GroupID = c.GroupID AND a.UserID = '$studentID' and b.AcademicYearID=$CurrentAcademicYear and b.RecordType = 4";

                        return $this->returnArray($sql,4);
                }

                # Retrieve Student Age Group by StudentID
                function retrieveAgeGroupByStudentID($studentID, $returnMultipleGroupID=false)
                {
		                $sql = "SELECT a.AgeGroupID
		                      	FROM SPORTS_AGE_GROUP as a,
		                            INTRANET_USER as b
		                        WHERE b.RecordStatus IN (0,1,2) AND b.RecordType = 2
		                            AND (a.DOBLowLimit IS NULL || b.DateOfBirth <= a.DOBLowLimit)
		                            AND (a.DOBUpLimit IS NULL || b.DateOfBirth >= a.DOBUpLimit)
		                            AND b.UserID = '$studentID'
		                            AND a.Gender = b.Gender ";
		                $temp = $this->returnVector($sql);
						
						// [2016-0627-1013-19066]
						if($returnMultipleGroupID)
							return $temp;
						else
		                	return $temp[0];
                }
                
                # Retrieve Student Attendant Status
                function returnFirstRoundStudentAttendantStatus($studentID, $eventGroupID)
                {
            			$sql = "SELECT RecordStatus
                                FROM SPORTS_LANE_ARRANGEMENT
                        	    WHERE StudentID = '$studentID'
                                    AND EventGroupID = '$eventGroupID'
                                    AND RoundType = '1' ";
                		$temp = $this->returnVector($sql);
                		return $temp;
                }
				
				function autoLanesArrange($EventGroupIDArr, $EventGroupTypeArr, $roundType)
				{
					global $sys_custom;
					
					for($f=0; $f<sizeof($EventGroupIDArr); $f++)
					{
						$eventGroupID = $EventGroupIDArr[$f];
						$eventType = $EventGroupTypeArr[$f];
				
						# Retrieve the Ext Info of the Event
						$EventGroupInfo = $this->retrieveEventGroupExtInfo($eventGroupID, $eventType);
						
						# Retrieve student list that have enroled the Event
						#$EnroledStudents = $this->retrieveEnroledStudentIDList($eventGroupID);
						
						//modified by marcus 18/6 
						# Retrieve student list that have enroled the Event (exclude those not in intranet user)
						$EnroledStudents = $this->retrieveEnroledStudentIDListInIntranet($eventGroupID);
						
						$studentCount = sizeof($EnroledStudents);
						$numberOfLane = $this->numberOfLanes;
						$invalid = 0;
				
						# Random Student List
						if(sizeof($EnroledStudents) != 1)
							$randArr = array_rand($EnroledStudents, $studentCount);
						else
							$randArr = array(0);
				
						//modified by marcus 18/6
						//if($EventGroupInfo["SecondRoundReq"] == 1 || $EventGroupInfo["FinalRoundReq"] == 1)
						//{
							if($EventGroupInfo["FirstRoundType"] == 0)                # Arranged By Number of People Per Group
							{
								$num_per_group = ($EventGroupInfo["FirstRoundGroupCount"] == 0) ? $numberOfLane : $EventGroupInfo["FirstRoundGroupCount"];
								$group_num = ceil($studentCount/$num_per_group);
							}
							else                # Arranged By Number of Groups
							{
								if(($EventGroupInfo["FirstRoundGroupCount"]*3)<=$studentCount)
								{
									$group_num = $EventGroupInfo["FirstRoundGroupCount"];
								}
								else
								{
									$temp_num = $EventGroupInfo["FirstRoundGroupCount"];
									while(($temp_num*3)>$studentCount)
									{
										$temp_num--;
									}
									$group_num = $temp_num;
								}
							}
				
							if($group_num == 0)
								$group_num = 1;
				
							$average_min = floor($studentCount/$group_num);
							$ext = $studentCount%$group_num;
							
							for($i=0; $i<$group_num; $i++)
							{
								$lanesArrange[$i] = ($ext > 0) ? $average_min + 1 : $average_min;
								if($ext > 0)
									$ext--;
							}
						//}
						//else
						//{
							/*
							## added by Ivan on 12 Feb 2009 - arrange groups even if the event only has first round
							if($EventGroupInfo["FirstRoundType"] == 0)                # Arranged By Number of People Per Group
							{
								$num_per_group = ($EventGroupInfo["FirstRoundGroupCount"] == 0) ? $numberOfLane : $EventGroupInfo["FirstRoundGroupCount"];
								$group_num = ceil($studentCount/$num_per_group);
							}
							else                # Arranged By Number of Groups
							{
								if(($EventGroupInfo["FirstRoundGroupCount"]*3)<=$studentCount)
								{
									$group_num = $EventGroupInfo["FirstRoundGroupCount"];
								}
								else
								{
									$temp_num = $EventGroupInfo["FirstRoundGroupCount"];
									while(($temp_num*3)>$studentCount)
									{
										$temp_num--;
									}
									$group_num = $temp_num;
								}
							}
				
							if($group_num == 0)
								$group_num = 1;
				
							$average_min = floor($studentCount/$group_num);
							$ext = $studentCount%$group_num;
				
							for($i=0; $i<$group_num; $i++)
							{
								$lanesArrange[$i] = ($ext > 0) ? $average_min + 1 : $average_min;
								if($ext > 0)
									$ext--;
							}
							## end of added by Ivan on 12 Feb 2009
							*/
							
						//	$noNext = 1;
						//	$lanesArrange[0] = $studentCount;
						//}
				
				
						$heat = 0;
						$delim = "";
						$values = "";
						$start = 0;
						for($j=0; $j<sizeof($lanesArrange); $j++)
						{
							$heat = $j+1;
							$limit = $start + $lanesArrange[$j];
							$pos = 1;
				
							# Set the start position of this group if it is a track event
							// [2017-0918-1146-09236] Ensure all althelets arranged in middle for first round track event
							if($EventGroupInfo["FirstRoundGroupCount"]==0 || ($sys_custom['eSports']['SportDay_FirstRoundTrackDefaultMiddle'] && $eventType==EVENT_TYPE_TRACK && $roundType == ROUND_TYPE_FIRSTROUND))
							{
								# $noNext is used to indicate If the event have second or final round
								if(($noNext!=1) || (($noNext==1) && ($lanesArrange[0]<=$numberOfLane)))
								{
									$diff = $numberOfLane - $lanesArrange[$j];
									$temp = floor($diff/2);
									if($temp > 0)
										$pos = $pos + $temp;
								}
							}
				
							for($i=$start; $i<$limit; $i++)
							{
								# Arragne Lane Positions Order By Student ID
								if($EventGroupInfo["FirstRoundRandom"] == 0)
								{
									$s_id = $EnroledStudents[$i];
								}
								else                # Arragne Lane Positions Randomly
								{
									$key = $randArr[$i];
									$s_id = $EnroledStudents[$key];
								}
								$values .= $delim."(".$eventGroupID.", ".$roundType.", ".$s_id.", ".$heat.", ".$pos.", now())";
								$delim = ", ";
								$pos++;
							}
							$start = $start + $lanesArrange[$j];
						}
				
						# Clear the records of this event in the SPORTS_LANE_ARRANGEMENT table
						$sql = "DELETE FROM SPORTS_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID'";
						$this->db_db_query($sql);
						
						# Insert lanes arrangement to the SPORT_LANE_ARRANGEMENT table
						$fields = "(EventGroupID, RoundType, StudentID, Heat, ArrangeOrder, DateModified)";
						$sql = "INSERT INTO SPORTS_LANE_ARRANGEMENT $fields VALUES $values";
						$this->db_db_query($sql);
						
						unset($lanesArrange);
						unset($randArr);
					}
				}

				# Get Date of Birth
				function getDateOfBirth($studentID)
				 {
					  $sql = "SELECT DateOfBirth FROM INTRANET_USER WHERE RecordType = '2' AND RecordStatus IN (0,1,2) AND StudentID = $studentID";
 					 $row = $this->returnVector($sql);

					 return $row[0];
				 }
				 
				#Get Student Name
				
				function retrieveStudentName($StudentID)
				{
					global $intranet_session_language;

					$student_name = ($intranet_session_language == "b5") ? "ChineseName" : "EnglishName";
					
					$sql = "	SELECT $student_name
								FROM INTRANET_USER
								WHERE UserID = $StudentID";
					$temp = $this->returnVector($sql);
					return $temp[0];
				}
				
                ######### modified by marcus 21/7/2009 
                # Search Student Enrolment Records by Given Arguments(arrange/enrol_update.php).
                # Arguments: $EnglishName, $ChineseName, $ClassName, $AgeGroupID, $HouseID
                # Return: StudentID, EnglishName, ChineseName, ClassName, DateOfBirth, Gender, AgeGroupName, House, HouseColor#                                Athletic Number
                #
//                function searchEnrolmentRecords($EnglishName="", $ChineseName="", $ClassName = "", $AgeGroupID="", $HouseID="")
//                {
//                        global $intranet_session_language;
//                        
//                        $CurrentAcademicYear = Get_Current_Academic_Year_ID();
//                        
//						/*if($ClassID != "")
//                        {
//                                $lc = new libclass();
//                                $ClassName = $lc->getClassName($ClassID);
//                        }
//                        else
//                                $ClassName = "";
//						*/
//						
//                        $conds1 = ($EnglishName=="")?"":" AND a.EnglishName LIKE '%$EnglishName%'";
//                        $conds2 = ($ChineseName=="")?"":" AND a.ChineseName LIKE '%$ChineseName%'";
//                        $conds3 = ($ClassName=="")?"":" AND a.ClassName = '$ClassName'";
//
//                        if($AgeGroupID != "")
//                        {
//                                $sql = "SELECT Gender, DOBLowLimit, DOBUpLimit FROM SPORTS_AGE_GROUP WHERE AgeGroupID = '$AgeGroupID'";
//                                $ageGrroupInfo = $this->returnArray($sql,3);
//
//                                $t_gender = $ageGrroupInfo[0][0];
//                                $t_low = $ageGrroupInfo[0][1];
//                                $t_up = $ageGrroupInfo[0][2];
//
//                                if($t_up == "")
//                                {
//                                        $conds4 = "AND a.Gender = '$t_gender' AND a.DateOfBirth <= '$t_low' AND a.DateOfBirth <> '0000-00-00 00:00:00'";
//                                }
//                                else if($t_low == "")
//                                {
//                                        $conds4 = "AND a.Gender = '$t_gender' AND a.DateOfBirth >= '$t_up' AND a.DateOfBirth  <> '0000-00-00 00:00:00'";
//                                }
//                                else
//                                        $conds4 = "AND a.Gender = '$t_gender' AND a.DateOfBirth <= '$t_low' AND a.DateOfBirth >= '$t_up' AND a.DateOfBirth <> '0000-00-00 00:00:00'";
//                        }
//                        else
//                                $conds4 = "";
//
//                        if($HouseID != "")
//                        {
//                                $sql = "SELECT GroupID FROM INTRANET_HOUSE WHERE HouseID = '$HouseID'";
//                                $houseInfo = $this->returnArray($sql,1);
//                                $houseGroupID = $houseInfo[0][0];
//
//                                $house_join = "LEFT OUTER JOIN INTRANET_USERGROUP as c ON c.UserID = a.UserID";
//                                $conds5 = "AND c.GroupID = '$houseGroupID'";
//                        }
//                        else
//                        {
//                                $house_join = "";
//                                $conds5 = "";
//                        }
//
////                      $engName = getNameFieldWithClassNumberEng("a.");
//// 						$chiName = getNameFieldWithClassNumberByLang ("a.");
//						
//						$engName = getNameFieldByLang("a.", "en");
//						$chiName = getNameFieldByLang("a.", "b5");
//
//                        # Retrieve Filtered Student Info
//                        $sql = "SELECT a.UserID, $engName, $chiName, a.ClassName,
//                                                a.DateOfBirth, a.Gender, b.AthleticNum
//                                                FROM INTRANET_USER as a LEFT OUTER JOIN SPORTS_STUDENT_ENROL_INFO as b ON a.UserID = b.StudentID $house_join WHERE  a.RecordType = '2' AND a.RecordStatus IN (0,1,2)
//                                                $conds1 $conds2 $conds3 $conds4 $conds5 ORDER BY a.UserID";
//
//						$students = $this->returnArray($sql,7);
//
//                        # Retrieve Age Group Info
//                        $namefield = ($intranet_session_language=="en") ? "EnglishName" : "ChineseName";
//                        $sql = "SELECT AgeGroupID, $namefield, DOBLowLimit, DOBUpLimit, Gender FROM SPORTS_AGE_GROUP";
//                        $ageGroups = $this->returnArray($sql,5);
//
//                        for($i=0; $i<sizeof($students); $i++)
//                        {
//                                $userid = $students[$i][0];
//                                $gender = $students[$i][5];
//
//								if($students[$i][4]=="0000-00-00 00:00:00")
//								{
//									$groupArr[$userid] = "N/A";
//                                    $useridArr[$i] = $userid;
//								}
//								else
//								{
//									$birth = explode(" ", $students[$i][4]);
//									$birth = $birth[0];
//
//									# Filter the students array by AgeGroupID and Create a New Array called groupFilteredArr
//									for($j=0; $j<sizeof($ageGroups); $j++)
//									{
//											list($a_ageGroupID, $a_ageGroupName, $a_lowLimit, $a_upLimit, $a_gender) = $ageGroups[$j];
//
//											if($a_gender == $gender)
//											{
//													if($a_upLimit == "NULL" || $a_upLimit == "")
//													{
//														if($birth <= $a_lowLimit)
//														{
//															$groupArr[$userid] = $a_ageGroupName;
//															$useridArr[$i] = $userid;
//															break;
//														}
//													}
//													else if($a_lowLimit == "NULL" || $a_lowLimit == "")
//													{
//
//														if($birth >= $a_upLimit)
//														{
//															$groupArr[$userid] = $a_ageGroupName;
//															$useridArr[$i] = $userid;
//															break;
//														}
//													}
//													else
//													{
//														if($birth <= $a_lowLimit && $birth >= $a_upLimit)
//														{
//															$groupArr[$userid] = $a_ageGroupName;
//															$useridArr[$i] = $userid;
//															break;
//														}
//													}
//											}
//									}
//								}
//                        }
//                        
//						if(empty($useridArr)) 
//							return array();      
//							                 
//                                if(is_array($useridArr))
//                                        $studentids = implode($useridArr, ",");
//                                else
//                                        $studentids = $useridArr;
//
//                                # Retrieve Student House Info
//                                $house_namefield = ($intranet_session_language=="en") ? "c.EnglishName" : "c.ChineseName";
//                                $sql = "SELECT a.UserID, $house_namefield, c.ColorCode FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID, INTRANET_HOUSE as c WHERE b.GroupID = c.GroupID AND a.UserID IN ($studentids) and b.AcademicYearID = '".$CurrentAcademicYear."'";
//
//                                $houses = $this->returnArray($sql,2);
//                                $houseArr = build_assoc_array($houses);
//
//                                # Retrieve Student House Color
//                                $sql = "SELECT a.UserID, c.ColorCode FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID, INTRANET_HOUSE as c WHERE b.GroupID = c.GroupID AND a.UserID IN ($studentids) and b.AcademicYearID = '".$CurrentAcademicYear."'";
//                                $housesc = $this->returnArray($sql,2);
//                                $housecArr = build_assoc_array($housesc);
//
//                                # Add Age Group Name and House Name to the Array
//                                for($i=0; $i<sizeof($students); $i++)
//                                {
//                                        $uid = $students[$i][0];
//
//                                        $students[$i][] = $groupArr[$uid];
//                                        $students[$i][] = $houseArr[$uid];
//                                        $students[$i][] = $housecArr[$uid];
//                                }
//                                return $students;
//                 }

                function searchEnrolmentRecords($EnglishName="", $ChineseName="", $ClassName = "", $AgeGroupID="", $HouseID="")
                {
                        global $intranet_session_language;
                        
                        $CurrentAcademicYear = Get_Current_Academic_Year_ID();
                        
                        $AgeGroupNAme = Get_Lang_Selection("sag.ChineseName","sag.EnglishName");
 						$engName = getNameFieldByLang("iu.", "en");
						$chiName = getNameFieldByLang("iu.", "b5");
                        
                        if(trim($EnglishName)!='')
                        	$cond_EnglishName = " AND iu.EnglishName LIKE '%$EnglishName%' ";
                        if(trim($ChineseName)!='')
                        	$cond_ChineseName = " AND iu.ChineseName LIKE '%$ChineseName%' ";
                        if(trim($ClassName)!='')
                        	$cond_ClassName = " AND iu.ClassName = '$ClassName' ";
                        if(trim($HouseID)!='')
                        	$cond_HouseID = " AND ih.HouseID = '$HouseID' ";
                    	if(trim($AgeGroupID)!='')
                        	$cond_AgeGroupID = " AND sag.AgeGroupID = '$AgeGroupID' ";
                        	
                        $sql = "
							SELECT 
								iu.UserID, 
								$engName, 
								$chiName, 
								CONCAT(iu.ClassName,'(',iu.ClassNumber,')'),
								iu.DateOfBirth, 
								iu.Gender, 
								ssei.AthleticNum, 
								$AgeGroupNAme,
								ig.Title,
								ih.ColorCode,
								sag.AgeGroupID
							FROM
								INTRANET_USER iu
								LEFT JOIN SPORTS_AGE_GROUP sag 
									ON iu.Gender = sag.Gender
									AND (sag.DOBUpLimit IS NULL OR iu.DateOfBirth >= sag.DOBUpLimit)
									AND (sag.DOBLowLimit IS NULL OR iu.DateOfBirth <= sag.DOBLowLimit) 
									AND iu.DateOfBirth <> '0000-00-00 00:00:00'
								INNER JOIN INTRANET_USERGROUP iug ON iug.UserID  = iu.UserID
								INNER JOIN INTRANET_GROUP ig ON iug.GroupID = ig.GroupID AND ig.AcademicYearID = '$CurrentAcademicYear' and ig.RecordType=4
								LEFT JOIN SPORTS_STUDENT_ENROL_INFO ssei ON ssei.StudentID = iu.UserID 
								LEFT JOIN INTRANET_HOUSE ih ON ih.GroupID = ig.GroupID 
							WHERE
								iu.RecordType = '2' 
								AND iu.RecordStatus IN (0,1,2)
								AND iu.ClassName IS NOT NULL
								AND iu.ClassName <> '' 
								$cond_EnglishName
								$cond_ChineseName
								$cond_ClassName
								$cond_HouseID
								$cond_AgeGroupID
							ORDER BY
								iu.ClassName, iu.ClassNumber
						";
						$students = $this->returnArray($sql); 
                        return $students;
                 }     

                function retrieveEventGroupExtInfo($eventGroupID, $eventTypeID)
                {
                        if($eventTypeID == EVENT_TYPE_TRACK)
                        {
                        	$NewRecordAsMS = $this->Get_Track_Result_To_Millisecond_Sql('','NewRecord');
                        	$RecordAsMS = $this->Get_Track_Result_To_Millisecond_Sql('','Record');
                        	$StandardAsMS = $this->Get_Track_Result_To_Millisecond_Sql('','Standard');
                        	
                            $sql = "SELECT EventGroupID,
                                        RecordHolderName,
                                        ".$this->Get_Pad_Track_Result_Sql('', 'Record').",
                                        RecordYear,
                                        RecordHouseID,
										".$this->Get_Pad_Track_Result_Sql('', 'Standard').",
                                        FirstRoundType,
                                        FirstRoundGroupCount,
                                        FirstRoundRandom,
                                        FirstRoundDay,
                                        SecondRoundReq,
                                        SecondRoundLanes,
                                        SecondRoundGroups,
                                        SecondRoundDay,
                                        FinalRoundReq,
                                        FinalRoundNum,
                                        FinalRoundDay,
                                        ".$this->Get_Pad_Track_Result_Sql('', 'NewRecord').",
                                        NewRecordHolderUserID,
                                        NewRecordHolderName,
                                        NewRecordHouseID,
										$NewRecordAsMS AS NewRecordAsMS,
										$RecordAsMS AS RecordAsMS,
										$StandardAsMS AS StandardAsMS
                                    FROM 
										SPORTS_EVENTGROUP_EXT_TRACK 
									WHERE 
										EventGroupID = '$eventGroupID'";
                            $temp = $this->returnArray($sql, 27);
                            
                            $result["EventGroupID"] = $temp[0][0];
                            $result["RecordHolderName"] = $temp[0][1];
                            $result["RecordMin"] = $temp[0][2];
                            $result["RecordSec"] = $temp[0][3];
                            $result["RecordMs"] = $temp[0][4];
                            $result["RecordYear"] = $temp[0][5];
                            $result["RecordHouseID"] = $temp[0][6];
                            $result["StandardMin"] = $temp[0][7];
                            $result["StandardSec"] = $temp[0][8];
                            $result["StandardMs"] = $temp[0][9];
                            $result["FirstRoundType"] = $temp[0][10];
                            $result["FirstRoundGroupCount"] = $temp[0][11];
                            $result["FirstRoundRandom"] = $temp[0][12];
                            $result["FirstRoundDay"] = $temp[0][13];
                            $result["SecondRoundReq"] = $temp[0][14];
                            $result["SecondRoundLanes"] = $temp[0][15];
                            $result["SecondRoundGroups"] = $temp[0][16];
                            $result["SecondRoundDay"] = $temp[0][17];
                            $result["FinalRoundReq"] = $temp[0][18];
                            $result["FinalRoundNum"] = $temp[0][19];
                            $result["FinalRoundDay"] = $temp[0][20];
                            $result["NewRecordMin"] = $temp[0][21];
                            $result["NewRecordSec"] = $temp[0][22];
                            $result["NewRecordMs"] = $temp[0][23];
                            $result["NewRecordAsMin"] = $temp[0][27];
                            $result["RecordAsMS"] = $temp[0][28];
                            $result["StandardAsMS"] = $temp[0][29];
//                          $result = $temp[0];
                        }
                        else if($eventTypeID == '2')
                        {
                            $sql = "SELECT
                                        EventGroupID,
                                        RecordHolderName,
                                        RecordMetre,
                                        RecordYear,
                                        RecordHouseID,
                                        StandardMetre,
                                        FirstRoundType,
                                        FirstRoundGroupCount,
                                        FirstRoundRandom,
                                        FirstRoundDay,
                                        FinalRoundReq,
                                        FinalRoundNum,
                                        FinalRoundDay,
                                        NewRecordMetre,
                                        NewRecordHolderUserID,
                                        NewRecordHolderName,
                                        NewRecordHouseID
                                    FROM
                                        SPORTS_EVENTGROUP_EXT_FIELD
                                    WHERE
                                        EventGroupID = '$eventGroupID'";
                            $temp = $this->returnArray($sql, 17);
                            
                            $result["EventGroupID"] = $temp[0][0];
                            $result["RecordHolderName"] = $temp[0][1];
                            $result["RecordMetre"] = $temp[0][2];
                            $result["RecordYear"] = $temp[0][3];
                            $result["RecordHouseID"] = $temp[0][4];
                            $result["StandardMetre"] = $temp[0][5];
                            $result["FirstRoundType"] = $temp[0][6];
                            $result["FirstRoundGroupCount"] = $temp[0][7];
                            $result["FirstRoundRandom"] = $temp[0][8];
                            $result["FirstRoundDay"] = $temp[0][9];
                            $result["FinalRoundReq"] = $temp[0][10];
                            $result["FinalRoundNum"] = $temp[0][11];
                            $result["FinalRoundDay"] = $temp[0][12];
                            $result["NewRecordMetre"] = $temp[0][13];
                            $result["NewRecordHolderUserID"] = $temp[0][14];
                            $result["NewRecordHolderName"] = $temp[0][15];
                            $result["NewRecordHouseID"] = $temp[0][16];
                        }
                        else if($eventTypeID == '3' || $eventTypeID == '4')
                        {
                            $sql = "SELECT
                                        EventGroupID,
                                        RecordHolderName,
                                        ".$this->Get_Pad_Track_Result_Sql('', 'Record').",
                                        RecordYear,
                                        RecordHouseID,
                                        ".$this->Get_Pad_Track_Result_Sql('', 'Standard').",
                                        ClassList,
                                        CategoryName,
                                        ".$this->Get_Pad_Track_Result_Sql('', 'NewRecord').",
                                        NewRecordHolderName,
                                        NewRecordHouseID,
                                        FinalRoundReq,
                                        FinalRoundNum,
                                        FinalRoundDay
                                    FROM 
                                        SPORTS_EVENTGROUP_EXT_RELAY 
                                    WHERE 
                                        EventGroupID = '$eventGroupID'";
                            $temp = $this->returnArray($sql, 17);
                            
                            $result["EventGroupID"] = $temp[0][0];
                            $result["RecordHolderName"] = $temp[0][1];
                            $result["RecordMin"] = $temp[0][2];
                            $result["RecordSec"] = $temp[0][3];
                            $result["RecordMs"] = $temp[0][4];
                            $result["RecordYear"] = $temp[0][5];
                            $result["RecordHouseID"] = $temp[0][6];
                            $result["StandardMin"] = $temp[0][7];
                            $result["StandardSec"] = $temp[0][8];
                            $result["StandardMs"] = $temp[0][9];
                            $result["ClassList"] = $temp[0][10];
                            $result["CategoryName"] = $temp[0][11];
                            $result["NewRecordMin"] = $temp[0][12];
                            $result["NewRecordSec"] = $temp[0][13];
                            $result["NewRecordMs"] = $temp[0][14];
                            $result["NewRecordHolderName"] = $temp[0][15];
                            $result["NewRecordHouseID"] = $temp[0][16];
                            
                            // [2019-0301-1144-56289]
                            $result["FinalRoundReq"] = $temp[0][17];
                            $result["FinalRoundNum"] = $temp[0][18];
                            $result["FinalRoundDay"] = $temp[0][19];
                        }
                        
                        return $result;
                }
                
                # Check whether Ext Info exist by EventTypeID and EventGroupID
                # return 0 - do not exist; 1 - exist
                function checkExtInfoExist($eventTypeID, $eventGroupID)
                {
                        if($eventTypeID == '1')
                        {
                                # IF Track Event
                                $sql = "SELECT COUNT(*) FROM SPORTS_EVENTGROUP_EXT_TRACK WHERE EventGroupID = '$eventGroupID'";
                                $temp = $this->returnArray($sql, 1);
                                $is_setup = ($temp[0][0] == 0) ? '0' : '1';
                        }
                        else if($eventTypeID == '2')
                        {
                                # IF Field Event
                                $sql = "SELECT COUNT(*) FROM SPORTS_EVENTGROUP_EXT_FIELD WHERE EventGroupID = '$eventGroupID'";
                                $temp = $this->returnArray($sql, 1);
                                $is_setup = ($temp[0][0] == 0) ? '0' : '1';
                        }
                        else if($eventTypeID == '3' || $eventTypeID=='4')
                        {
                                # IF Relay Event
                                $sql = "SELECT COUNT(*) FROM SPORTS_EVENTGROUP_EXT_RELAY WHERE EventGroupID = '$eventGroupID'";
                                $temp = $this->returnArray($sql, 1);
                                $is_setup = ($temp[0][0] == 0) ? '0' : '1';
                        }
                        return $is_setup;
                }

			# Retrieve All Events that can be enrolled
           	function retrieveRestrictQuotaEventGroupInfo($groupID, $UserID, $skipOnlineEnrolChecking=false)
           	{
            	global $intranet_session_language;

	            $sql = "SELECT Gender From INTRANET_USER Where UserID = '$UserID'";
	            $gender = $this->returnVector($sql);
                $openGroupID = ($gender[0]=="F")?'-2':'-1';
				$onlineEnrolCond = $skipOnlineEnrolChecking? "" : " AND a.IsOnlineEnrol = '1' "; 
				
                $namefield1 = ($intranet_session_language == "b5") ? "b.ChineseName" : "b.EnglishName";
                $namefield2 = ($intranet_session_language == "b5") ? "c.ChineseName" : "c.EnglishName";

                $sql = "SELECT a.EventGroupID, a.EventID, $namefield1, b.EventType, $namefield2, a.GroupID, a.EventQuota
                                                            FROM SPORTS_EVENTGROUP as a
                                                            LEFT OUTER JOIN SPORTS_EVENT as b ON a.EventID = b.EventID,
                                                            SPORTS_EVENT_TYPE_NAME as c
                                                            WHERE b.EventType = c.EventTypeID
                                                            AND (a.GroupID='$groupID' || a.GroupID = '$openGroupID' || a.GroupID = '-4')
                                                            $onlineEnrolCond
                                                            AND CountPersonalQuota = '1'
                                                            ORDER BY b.EventType, b.DisplayOrder";
            	$temp = $this->returnArray($sql);
            	return $temp;
            }


            # Retrieve Unrestrict Quota Events that can be enrolled
            function retrieveUnrestrictQuotaEventGroupInfo($groupID, $UserID, $skipOnlineEnrolChecking=false)
            {
            	global $intranet_session_language;

                $sql = "SELECT Gender From INTRANET_USER Where UserID = '$UserID'";
                $gender = $this->returnVector($sql);
                $openGroupID = ($gender[0]=="F")?'-2':'-1';
				$onlineEnrolCond = $skipOnlineEnrolChecking? "" : " AND a.IsOnlineEnrol = '1' "; 

                $namefield1 = ($intranet_session_language == "b5") ? "b.ChineseName" : "b.EnglishName";

                $sql = "SELECT a.EventGroupID, $namefield1, b.EventType, a.GroupID, a.EventQuota 
                                                            FROM SPORTS_EVENTGROUP as a
                                                            LEFT OUTER JOIN SPORTS_EVENT as b ON a.EventID = b.EventID
                                                            AND (a.GroupID='$groupID' || a.GroupID = '$openGroupID' || a.GroupID = '-4' )
                                                            $onlineEnrolCond
                                                            AND CountPersonalQuota = '0'
                                                            ORDER BY b.EventType, b.DisplayOrder";
                $temp = $this->returnArray($sql,4);
                return $temp;
            }

            function retrieveEnrolmentRules()
            {
                                #Retrieve Enrollment Rules
                 $sql = "SELECT EnrolMaxTotal, EnrolMaxTrack, EnrolMaxField, EnrolDetails FROM SPORTS_SYSTEM_SETTING";
                 $rules = $this->returnArray($sql,4);

                 return $rules;
           }

       # Return: array of Class Name, Num of Students, No ClassNum, No House, Multiple House,
       #               No Gender, No DOB, No Athletic Num
       #         last one is no class name students
       function retrieveParticipantSummary()
       {
				global $PATH_WRT_ROOT;
				
				$CurrentAcademicYear = Get_Current_Academic_Year_ID();
				
                $sql = "UPDATE INTRANET_USER SET ClassName = '' WHERE ClassName IS NULL AND RecordType = 2";
                $this->db_db_query($sql);

                $empty_name = EMPTY_ARRAY_NAME;
                # Get Class list
                //$sql = "SELECT ClassID, ClassName FROM INTRANET_CLASS WHERE RecordStatus IN (0,1,2) ORDER BY ClassName";
                //$classes = $this->returnArray($sql,2);
                include_once($PATH_WRT_ROOT."includes/libclass.php");
                $lc = new libclass();
				$classes = $lc->getClassList();
				
				
                # Num of students
                $sql = "SELECT ClassName, COUNT(UserID) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2)
                               GROUP BY ClassName";
                $temp_array = $this->returnArray($sql,2);
                $a_num_of_students = build_assoc_array($temp_array,$empty_name);


                //start error!!!
                # No ClassNum
                $sql = "SELECT ClassName, COUNT(UserID) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) AND (ClassNumber IS NULL OR ClassNumber = '') GROUP BY ClassName";
                $temp_array = $this->returnArray($sql,2);

                $a_no_classnum = build_assoc_array($temp_array,$empty_name);

                # No House
                # Get house IDs
                $sql = "SELECT GroupID FROM INTRANET_GROUP where RecordType = '".GROUP_TYPE_HOUSE."' and AcademicYearID=$CurrentAcademicYear";
                $group_house = $this->returnVector($sql);
                $house_id_list = implode(",",$group_house);

                $sql = "SELECT a.ClassName, COUNT(a.UserID) FROM INTRANET_USER as a
                               LEFT OUTER JOIN INTRANET_USERGROUP as b ON a.UserID = b.UserID AND b.GroupID IN ($house_id_list)
                               WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                                     AND b.UserID IS NULL
                               GROUP BY a.ClassName"
                               ;
                $temp_array = $this->returnArray($sql,2);
                $a_no_house = build_assoc_array($temp_array,$empty_name);

                # Multiple House
                # Get UserIDs w/ multiple
                $sql = "SELECT b.ClassName, COUNT(b.UserID) FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                             WHERE a.GroupID IN ($house_id_list) AND b.ClassName IS NOT NULL GROUP BY b.UserID";
                $temp_array = $this->returnArray($sql,3);

                                for($i=0; $i<sizeof($temp_array); $i++)
                                {
                                        list($className, $count) = $temp_array[$i];

                                        if($a_mul_house[$className] == "")
                                                $a_mul_house[$className] = 0;

                                        if($count > 1)
                                                $a_mul_house[$className]++;
                                }
                //$a_mul_house = build_assoc_array($temp_array,$empty_name);

                # No Gender
                $sql = "SELECT ClassName, COUNT(UserID) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2)
                                          AND (Gender != 'F' AND Gender != 'M' ) GROUP BY ClassName";
                $temp_array = $this->returnArray($sql,2);
                $a_no_gender = build_assoc_array($temp_array,$empty_name);
                
                # No DOB
                $sql = "SELECT ClassName, COUNT(UserID) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2)
                                          AND (DateOfBirth IS NULL OR DateOfBirth = '' OR DateOfBirth='0000-00-00 00:00:00') GROUP BY ClassName";
                $temp_array = $this->returnArray($sql,2);
                $a_no_dob = build_assoc_array($temp_array,$empty_name);

                # No athletic Num
                                # Retrieve Number of Student With Athletic Number
                $sql = "SELECT a.ClassName, COUNT(b.StudentID)
                               FROM INTRANET_USER as a
                               LEFT OUTER JOIN SPORTS_STUDENT_ENROL_INFO as b ON a.UserID = b.StudentID
                               WHERE (b.AthleticNum IS NOT NULL OR b.AthleticNum != '') AND a.RecordStatus IN (0,1,2)
                               GROUP BY a.ClassName";
                $temp_array = $this->returnArray($sql,2);

                $a_have_athlnum = build_assoc_array($temp_array, $empty_name);

                                $result = array();
                for ($i=0; $i<sizeof($classes); $i++)
                {
                     list($id,$class_name) = $classes[$i];

					if(sizeof($group_house)==0)
						$a_no_house[$class_name] = $a_num_of_students[$class_name]+0;

                     $result[$class_name] =array(
                                            $a_num_of_students[$class_name]+0,
                                            $a_no_classnum[$class_name]+0,
                                            $a_no_house[$class_name]+0,
                                            $a_mul_house[$class_name]+0,
                                            $a_no_gender[$class_name]+0,
                                            $a_no_dob[$class_name]+0,
                                            ($a_num_of_students[$class_name]-$a_have_athlnum[$class_name])+0);
                }

				if(sizeof($group_house)==0)
					$a_no_house[$empty_name] = $a_num_of_students[$empty_name]+0;

                $result[$empty_name] =array(
                                            $a_num_of_students[$empty_name]+0,
                                            $a_no_classnum[$empty_name]+0,
                                            $a_no_house[$empty_name]+0,
                                            $a_mul_house[$empty_name]+0,
                                            $a_no_gender[$empty_name]+0,
                                            $a_no_dob[$empty_name]+0,
                                            ($a_num_of_students[$empty_name]-$a_have_athlnum[$empty_name])+0);

                return $result;
       }
       
       function retrieveParticipantSummary_StudentDetails()
       {
			global $PATH_WRT_ROOT;
			
			$CurrentAcademicYear = Get_Current_Academic_Year_ID();
			
			$temp_result = array();
			
			### No ClassNum
			$name_field = getNameFieldByLang("");
	        $sql = "SELECT UserID, ClassName, ClassNumber, $name_field as studentname, 'NoClassNum' as ErrorField FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) AND (ClassNumber IS NULL OR ClassNumber = '') and ClassNumber is not NULL";
	        $temp_array1 = $this->returnArray($sql);
			if(!empty($temp_array1)) $temp_result = array_merge($temp_result, $temp_array1);
			
			### No House
	        # Get house IDs
	        $sql = "SELECT GroupID FROM INTRANET_GROUP where RecordType = '".GROUP_TYPE_HOUSE."' and AcademicYearID=$CurrentAcademicYear";
	        $group_house = $this->returnVector($sql);
	        $house_id_list = implode(",",$group_house);
	
	        $name_field = getNameFieldByLang("a.");
	        $sql = "SELECT 
	        			a.UserID, a.ClassName, a.ClassNumber, $name_field as studentname, 'NoHouse' as ErrorField
	        		FROM 
		        		INTRANET_USER as a
						LEFT OUTER JOIN INTRANET_USERGROUP as b ON a.UserID = b.UserID AND b.GroupID IN ($house_id_list)
					WHERE 
					a.RecordType = 2 AND 
					a.RecordStatus IN (0,1,2) AND 
					b.UserID IS NULL and
					a.ClassNumber is not NULL
					";
	        $temp_array2 = $this->returnArray($sql);
			if(!empty($temp_array2)) $temp_result = array_merge($temp_result, $temp_array2);
	        
			### Multiple House
	        # Get UserIDs w/ multiple
	        $name_field = getNameFieldByLang("b.");
	        $sql = "SELECT 
	        			b.UserID, b.ClassName, b.ClassNumber, $name_field as studentname, 'MultipleHouse' as ErrorField
					FROM 
						INTRANET_USERGROUP as a 
						LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
					WHERE 
						a.GroupID IN ($house_id_list) AND 
						b.ClassName IS NOT NULL 
					group by 
						b.UserID
					having 
						count(b.UserID) > 1
					"; 
	        $temp_array3 = $this->returnArray($sql);
	        if(!empty($temp_array3)) $temp_result = array_merge($temp_result, $temp_array3);
	        
	        ### No Gender
	        $name_field = getNameFieldByLang("");
            $sql = "SELECT 
            UserID, ClassName, ClassNumber, $name_field as studentname, 'NoGender' as ErrorField
            FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2)
                                      AND (Gender != 'F' AND Gender != 'M' ) and ClassName is not null";
            $temp_array4 = $this->returnArray($sql,2);
	        if(!empty($temp_array4)) $temp_result = array_merge($temp_result, $temp_array4);
	        
	        # No DOB
	        $name_field = getNameFieldByLang("");
            $sql = "SELECT 
						UserID, ClassName, ClassNumber, $name_field as studentname, 'NoDOB' as ErrorField
					FROM 
						INTRANET_USER 
					WHERE 
						RecordType = 2 AND 
						RecordStatus IN (0,1,2) AND 
						(DateOfBirth IS NULL OR DateOfBirth = '' OR DateOfBirth='0000-00-00 00:00:00') and
						ClassNumber is not NULL
					";
            $temp_array5 = $this->returnArray($sql);
			if(!empty($temp_array5)) $temp_result = array_merge($temp_result, $temp_array5);
				        
			# No athletic Num
			# Retrieve Number of Student With Athletic Number
			$name_field = getNameFieldByLang("a.");
			$sql = "SELECT 
			a.UserID, a.ClassName, a.ClassNumber, $name_field as studentname, 'NoAthleticNum' as ErrorField
			FROM 
			INTRANET_USER as a
			LEFT OUTER JOIN SPORTS_STUDENT_ENROL_INFO as b ON (a.UserID = b.StudentID)
			WHERE 
			(b.AthleticNum IS NULL OR b.AthleticNum = '') AND 
			a.RecordStatus IN (0,1,2) and
			a.ClassNumber is not NULL
			";
	         $temp_array6 = $this->returnArray($sql);
	         if(!empty($temp_array6)) $temp_result = array_merge($temp_result, $temp_array6);
	         
	         $result2 = array();
	         
	         ##### build result array
	         if(!empty($temp_result))
	         {
				foreach($temp_result as $k=>$d)
				{
					list($tmp_UserID, $tmp_ClassName, $tmp_ClassNumber, $tmp_studentname, $tmp_ErrorField) = $d;
					$result2[$tmp_ClassName][$tmp_ClassNumber][$tmp_studentname][$tmp_ErrorField] = 1;
				}

	         }
	         ksort($result2);
			return $result2;
       }


                #######################################
                ## For Athletic Number Generation
                ## Argument: Student ID
                function retrieveHouseCode($sid)
                {
	                $CurrentAcademicYear = Get_Current_Academic_Year_ID();
	                
                        $sql = "SELECT b.HouseCode
                                                FROM INTRANET_USERGROUP as a
                                                LEFT OUTER JOIN INTRANET_HOUSE as b ON a.GroupID = b.GroupID
                                                LEFT JOIN INTRANET_GROUP as c ON a.GroupID = c.GroupID
                                                WHERE c.RecordType = '".GROUP_TYPE_HOUSE."' AND a.UserID = '$sid' 
                                                and c.AcademicYearID=$CurrentAcademicYear
                                                ";
                        $houseCode = $this->returnVector($sql);

                        return $houseCode[0];
                }

                function retrieveHouseDisplayOrder($sid)
                {
	                $CurrentAcademicYear = Get_Current_Academic_Year_ID();
	                
                        $sql = "SELECT b.DisplayOrder
                                                FROM INTRANET_USERGROUP as a
                                                LEFT OUTER JOIN INTRANET_HOUSE as b ON a.GroupID = b.GroupID
                                                LEFT JOIN INTRANET_GROUP as c ON a.GroupID = c.GroupID
                                                WHERE c.RecordType = '".GROUP_TYPE_HOUSE."' AND a.UserID = '$sid' 
                                                and c.AcademicYearID=$CurrentAcademicYear";
                        $displayOrder = $this->returnVector($sql);

                        return $displayOrder[0];
            }

                function retrieveGroupCode($sid)
                {
                        $sql = "SELECT b.GroupCode
                                                FROM INTRANET_USER as a, SPORTS_AGE_GROUP as b
                                                WHERE (b.DOBLowLimit IS NULL || Date(a.DateOfBirth) <= b.DOBLowLimit)
                                                AND (b.DOBUpLimit IS NULL || Date(a.DateOfBirth) >= b.DOBUpLimit)
                                                AND a.Gender = b.Gender
                                                AND a.UserID = '$sid'";
                        $gradeCode = $this->returnVector($sql);

                        return  $gradeCode[0];
            }

                function retrieveClassName($sid)
                {
                        $sql = "SELECT ClassName
                                                FROM INTRANET_USER
                                                WHERE UserID = '$sid'";
                        $className = $this->returnVector($sql);

                        return  $className[0];
            }

                function retrieveClassNumber($sid)
                {
                        $sql = "SELECT ClassNumber
                                                FROM INTRANET_USER
                                                WHERE UserID = '$sid'";
                        $classNumber = $this->returnVector($sql);

                        return  $classNumber[0];
            }

                function retrieveClassNameNumber($sid)
                {
                        $sql = "SELECT CONCAT(ClassName, if(length(ClassNumber) = 1, CONCAT('0', ClassNumber),                 ClassNumber))
                                                FROM INTRANET_USER
                                                WHERE UserID = '$sid'";

                        $classNameNum = $this->returnVector($sql,1);

                        return  $classNameNum[0];
            }
        ##################################

       function inEnrolmentPeriod()
       {
                if ($this->enrolDateStart == "") return false;         # Not initialized

                $start = strtotime($this->enrolDateStart);
                $end = strtotime($this->enrolDateEnd);
                $today = mktime(0,0,0,date("m"),date("d"),date("Y"));
                if ($today >= $start && $today <= $end)
                {
                    return true;
                }
                else return false;
       }
######################################################
############## Report Generation Part ##########################
#### START################################################
        function retrieveClassLevelStudentCount()
        {
		/* modified by marcus 21/7/2009	
                $sql = "SELECT COUNT(e.UserID), a.ClassLevelID, a.LevelName FROM INTRANET_CLASSLEVEL as a
                LEFT JOIN INTRANET_CLASS as b ON a.ClassLevelID = b.ClassLevelID
                LEFT JOIN INTRANET_GROUP as c ON b.GroupID = c.GroupID
                LEFT JOIN INTRANET_USERGROUP as d ON c.GroupID = d.GroupID
                LEFT JOIN INTRANET_USER as e ON d.UserID = e.UserID
                WHERE c.RecordType = 3 AND e.RecordStatus IN (0,1,2) AND e.RecordType = 2
                GROUP BY a.ClassLevelID ORDER BY a.ClassLevelID";
         */ 
              $CurrentAcademicYear = Get_Current_Academic_Year_ID();
                $sql = "	SELECT 
								COUNT(e.UserID), a.YearID, a.YearName 
							FROM 
								YEAR as a
								LEFT OUTER JOIN YEAR_CLASS as b ON a.YearID = b.YearID
								LEFT OUTER JOIN YEAR_CLASS_USER as c ON b.YearClassID = c.YearClassID
								LEFT OUTER JOIN INTRANET_USER as e ON c.UserID = e.UserID
							WHERE 
								(e.UserID IS NULL 
								OR(e.RecordStatus IN (0,1,2) AND e.RecordType = 2))
								AND b.AcademicYearID = '$CurrentAcademicYear'
							GROUP BY 
								a.YearID 
							ORDER BY 
								a.YearID";
		  $temp = $this->returnArray($sql, 3);

                return  $temp;
        }

        function retrieveClassLevelNumberOfEnrolment()
        {
			/* modified by marcus 21/7/2009	
                $sql = "SELECT COUNT(e.StudentID), a.ClassLevelID FROM INTRANET_CLASSLEVEL as a
                LEFT JOIN INTRANET_CLASS as b ON a.ClassLevelID = b.ClassLevelID
                LEFT JOIN INTRANET_GROUP as c ON b.GroupID = c.GroupID
                LEFT JOIN INTRANET_USERGROUP as d ON c.GroupID = d.GroupID
                LEFT JOIN SPORTS_STUDENT_ENROL_EVENT as e ON d.UserID = e.StudentID
                WHERE c.RecordType = 3
                GROUP BY a.ClassLevelID ORDER BY a.ClassLevelID";
            */
              $CurrentAcademicYear = Get_Current_Academic_Year_ID();
	            $sql = "	SELECT 
								COUNT(e.StudentID), a.YearID 
							FROM 
								YEAR as a
								LEFT JOIN YEAR_CLASS as b ON a.YearID = b.YearID
								LEFT JOIN YEAR_CLASS_USER as c ON b.YearClassID = c.YearClassID
								LEFT JOIN INTRANET_USER as f ON c.UserID = f.UserID 
								LEFT JOIN SPORTS_STUDENT_ENROL_EVENT as e ON f.UserID = e.StudentID
								INNER JOIN SPORTS_EVENTGROUP as g on (g.EventGroupID = e.EventGroupID)
								INNER JOIN SPORTS_EVENT as h on (h.EventID = g.EventID) 
							WHERE 
								f.RecordStatus IN (0,1,2) 
								AND f.RecordType = 2			
								AND (f.ClassName <> '' AND f.ClassNumber <> ''  AND f.ClassName IS NOT NULL  AND f.ClassNumber IS NOT NULL )
								AND b.AcademicYearID = '$CurrentAcademicYear'
							GROUP BY 
								a.YearID 
							ORDER BY 
								a.YearID";
				$temp = $this->returnArray($sql, 2);

                return  $temp;
        }
        
        function retrieveClassLevelNumberOfStudentEnrolment()
        {
              $CurrentAcademicYear = Get_Current_Academic_Year_ID();
	            $sql = "	SELECT 
								COUNT(distinct(e.StudentID)), a.YearID 
							FROM 
								YEAR as a
								LEFT JOIN YEAR_CLASS as b ON a.YearID = b.YearID
								LEFT JOIN YEAR_CLASS_USER as c ON b.YearClassID = c.YearClassID
								LEFT JOIN INTRANET_USER as f ON c.UserID = f.UserID
								LEFT JOIN SPORTS_STUDENT_ENROL_EVENT as e ON c.UserID = e.StudentID
								INNER JOIN SPORTS_EVENTGROUP as g on (g.EventGroupID = e.EventGroupID)
								INNER JOIN SPORTS_EVENT as h on (h.EventID = g.EventID) 
							WHERE 
								f.RecordStatus IN (0,1,2) 
								AND f.RecordType = 2		
								AND (f.ClassName <> '' AND f.ClassNumber <> ''  AND f.ClassName IS NOT NULL  AND f.ClassNumber IS NOT NULL )	
								AND b.AcademicYearID = '$CurrentAcademicYear'			
							GROUP BY 
								a.YearID 
							ORDER BY 
								a.YearID";
				$temp = $this->returnArray($sql, 2);

                return  $temp;
        }
        
        function retrieveClassNumberOfStudentEnrolment()
        {
              $CurrentAcademicYear = Get_Current_Academic_Year_ID();
	            $sql = "	SELECT 
								COUNT(distinct(e.StudentID)), b.YearClassID 
							FROM 
								YEAR_CLASS as b 
								LEFT JOIN YEAR_CLASS_USER as c ON b.YearClassID = c.YearClassID
								LEFT JOIN SPORTS_STUDENT_ENROL_EVENT as e ON c.UserID = e.StudentID
								LEFT JOIN INTRANET_USER as f ON c.UserID = f.UserID
								INNER JOIN SPORTS_EVENTGROUP as g on (g.EventGroupID = e.EventGroupID)
								INNER JOIN SPORTS_EVENT as h on (h.EventID = g.EventID) 
							WHERE 
								f.RecordStatus IN (0,1,2) 
								AND f.RecordType = 2			
								AND (f.ClassName <> '' AND f.ClassNumber <> ''  AND f.ClassName IS NOT NULL  AND f.ClassNumber IS NOT NULL )	
								AND b.AcademicYearID = '$CurrentAcademicYear'	
							GROUP BY 
								b.YearClassID 
							ORDER BY 
								b.YearClassID";
				$temp = $this->returnArray($sql, 2);

                return  $temp;
        }

        function retrieveClassStudentCount()
        {
			/* modified by marcus 21/7/2009	
                $sql = "SELECT COUNT(d.UserID), a.ClassID, a.ClassName
                                        FROM INTRANET_CLASS as a
                                        LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
                                        LEFT JOIN INTRANET_USERGROUP as c ON b.GroupID = c.GroupID
                                        LEFT JOIN INTRANET_USER as d ON c.UserID = d.UserID
                                        WHERE b.RecordType = 3 AND d.RecordStatus IN (0,1,2) AND d.RecordType = 2
                                        GROUP BY a.ClassID ORDER BY a.ClassID";
			*/
                $CurrentAcademicYear = Get_Current_Academic_Year_ID();
                $sql = "	SELECT 
								COUNT(d.UserID), 
								a.YearClassID, 
								a.ClassTitleEN ,
								a.ClassTitleB5
                            FROM 
								YEAR_CLASS as a
								LEFT JOIN YEAR_CLASS_USER as b ON a.YearClassID = b.YearClassID
								LEFT JOIN INTRANET_USER as d ON b.UserID = d.UserID
							WHERE 
								d.RecordStatus IN (0,1,2) 
								AND d.RecordType = 2
								AND (d.ClassName <> '' AND d.ClassNumber <> ''  AND d.ClassName IS NOT NULL  AND d.ClassNumber IS NOT NULL )
								AND a.AcademicYearID = '$CurrentAcademicYear'					
							GROUP BY a.YearClassID 
							ORDER BY a.YearClassID";
										
                $temp = $this->returnArray($sql, 3);
                return  $temp;
        }

        function retrieveClassNumberOfEnrolmentAndScore()
        {
			/* modified by marcus 21/7/2009	

                $sql = "SELECT COUNT(d.StudentID), ROUND(SUM(e.Score),1), a.ClassID
                                        FROM INTRANET_CLASS as a
                                        LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
                                        LEFT JOIN INTRANET_USERGROUP as c ON b.GroupID = c.GroupID
                                        LEFT JOIN SPORTS_STUDENT_ENROL_EVENT as d ON c.UserID = d.StudentID
                                        LEFT JOIN SPORTS_LANE_ARRANGEMENT as e ON d.EventGroupID = e.EventGroupID AND d.StudentID = e.StudentID
                                        WHERE b.RecordType = 3
                                        GROUP BY a.ClassID ORDER BY a.ClassID";
			*/
			 $CurrentAcademicYear = Get_Current_Academic_Year_ID();
                $sql = "	
						SELECT 
							COUNT(distinct c.UserID, see.EventGroupID), 
							ROUND(SUM(IF(e.CountClassScore = 1,d.Score,0)),1), 
							a.YearClassID
						FROM 
							YEAR_CLASS as a
							LEFT JOIN YEAR_CLASS_USER as b ON a.YearClassID = b.YearClassID
							LEFT JOIN INTRANET_USER as c ON b.UserID = c.UserID
							LEFT JOIN SPORTS_STUDENT_ENROL_EVENT as see ON c.UserID = see.StudentID
							LEFT JOIN SPORTS_LANE_ARRANGEMENT as d ON d.StudentID = c.UserID AND d.EventGroupID = see.EventGroupID
							LEFT JOIN SPORTS_EVENTGROUP e ON d.EventGroupID = e.EventGroupID 
						WHERE 
							c.RecordStatus IN (0,1,2) 
							AND c.RecordType = 2
							AND (c.ClassName <> '' AND c.ClassNumber <> ''  AND c.ClassName IS NOT NULL  AND c.ClassNumber IS NOT NULL )
							AND a.AcademicYearID = '$CurrentAcademicYear'
						GROUP BY 
							a.YearClassID 
						ORDER BY 
							a.YearClassID";
                $temp = $this->returnArray($sql, 3);

                return  $temp;
        }

        function retrieveHouseStudentCount()
        {
                global $intranet_session_language;

                $namefield = ($intranet_session_language == "b5") ? "a.ChineseName" : "a.EnglishName";
				$CurrentAcademicYear = Get_Current_Academic_Year_ID();
                $sql = "SELECT COUNT(d.UserID), a.HouseID, $namefield, a.ColorCode
                                        FROM INTRANET_HOUSE as a
                                        LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
                                        LEFT JOIN INTRANET_USERGROUP as c ON b.GroupID = c.GroupID
                                        LEFT JOIN INTRANET_USER as d ON c.UserID = d.UserID
                                        WHERE d.RecordStatus IN (0,1,2) AND d.RecordType = 2
                                        and d.ClassName<>'' and d.ClassNumber<>''
										AND b.AcademicYearID = '$CurrentAcademicYear'
										AND b.RecordType = 4
                                        GROUP BY a.HouseID ORDER BY a.HouseID";
                $temp = $this->returnArray($sql, 4);

                return  $temp;
        }

		//modified by marcus 20091113
        function retrieveHouseNumberOfEnrolmentAndScore()
        {
        	$CurrentAcademicYear = Get_Current_Academic_Year_ID();
                $sql = "SELECT 
							COUNT(distinct see.StudentID,see.EventGroupID),
							ROUND(SUM(IF(f.CountHouseScore = 1,d.score,0)),1) ,
							a.HouseID
						FROM
							INTRANET_HOUSE a 
							INNER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
							LEFT JOIN INTRANET_USERGROUP as c ON b.GroupID = c.GroupID
							LEFT JOIN SPORTS_STUDENT_ENROL_EVENT as see ON c.UserID = see.StudentID
							LEFT JOIN SPORTS_LANE_ARRANGEMENT as d ON c.UserID = d.StudentID AND d.EventGroupID = see.EventGroupID
							LEFT JOIN INTRANET_USER e ON e.UserID = d.StudentID
							LEFT JOIN SPORTS_EVENTGROUP f ON d.EventGroupID = f.EventGroupID 
						WHERE 
							b.AcademicYearID = '$CurrentAcademicYear'
						GROUP BY a.HouseID";
                $temp = $this->returnArray($sql, 3);
				
				# also from relay events
				$sql = "SELECT ROUND(SUM(a.Score),1), a.HouseID
							FROM SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT as a,
                            SPORTS_EVENTGROUP as b
                            WHERE a.EventGroupID = b.EventGroupID
							AND a.Score IS NOT NULL
							GROUP BY a.HouseID ORDER BY a.HouseID
                           ";
                $temp2 = $this->returnArray($sql, 2);
				
				for ($i=0; $i<sizeof($temp); $i++)
				{
					for ($j=0; $j<sizeof($temp2); $j++)
					{
						if ($temp2[$j][1]==$temp[$i][2] && $temp2[$j][0]>0 && $temp2[$j][0]<>"")
						{
							$temp[$i][1] += $temp2[$j][0];
							break;
						}
					}
				}

                return  $temp;
        }


        function retrieveAgeGroupStudentCountByHouse($houseID)
        {
                $sql = "SELECT COUNT(b.UserID), a.AgeGroupID
                                        FROM SPORTS_AGE_GROUP as a,
                                        INTRANET_USER as b,
                                        INTRANET_USERGROUP as c,
                                        INTRANET_HOUSE as d
                                        WHERE b.RecordStatus IN (0,1,2) AND b.RecordType = 2
                                        AND (a.DOBLowLimit IS NULL || Date(b.DateOfBirth) <= a.DOBLowLimit)
                                        AND (a.DOBUpLimit IS NULL || Date(b.DateOfBirth) >= a.DOBUpLimit)
                                        AND b.UserID = c.UserID
                                        AND c.GroupID = d.GroupID
                                        AND a.Gender = b.Gender
                                        AND d.HouseID = '$houseID'
                                        GROUP BY a.AgeGroupID ORDER BY a.AgeGroupID";
                $temp = $this->returnArray($sql, 3);

                for($i=0; $i<sizeof($temp); $i++)
                {
                        list($count, $group_id) = $temp[$i];
                        $result[$group_id] = $count;
                }

                return  $result;
        }

        # Retrieve Number of Participants in a House
        function retrieveAgeGroupParticipantCountByHouse($houseID)
        {
                $sql = "SELECT COUNT(DISTINCT b.UserID), a.AgeGroupID
                                        FROM SPORTS_AGE_GROUP as a,
                                        INTRANET_USER as b,
                                        INTRANET_USERGROUP as c,
                                        INTRANET_HOUSE as d,
                                        SPORTS_STUDENT_ENROL_EVENT as e
                                        WHERE b.RecordStatus IN (0,1,2) AND b.RecordType = 2
                                        AND (a.DOBLowLimit IS NULL || Date(b.DateOfBirth) <= a.DOBLowLimit)
                                        AND (a.DOBUpLimit IS NULL || Date(b.DateOfBirth) >= a.DOBUpLimit)
                                        AND b.UserID = c.UserID
                                        AND c.GroupID = d.GroupID
                                        AND a.Gender = b.Gender
                                        AND b.UserID = e.StudentID
                                        AND d.HouseID = '$houseID'
                                        GROUP BY a.AgeGroupID ORDER BY a.AgeGroupID";
                $temp = $this->returnArray($sql, 3);

                for($i=0; $i<sizeof($temp); $i++)
                {
                        list($count, $group_id) = $temp[$i];
                        $result[$group_id] = $count;
                }

                return  $result;
        }


        function retrieveAgeGroupNumberOfEnrolmentByHouse($houseID)
        {
		
		/*
		$sql = "SELECT COUNT(d.StudentID), SUM(e.Score), a.HouseID
                                        FROM INTRANET_HOUSE as a
                                        LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
                                        LEFT JOIN INTRANET_USERGROUP as c ON b.GroupID = c.GroupID
                                        LEFT JOIN SPORTS_STUDENT_ENROL_EVENT as d ON c.UserID = d.StudentID
                                        LEFT JOIN SPORTS_LANE_ARRANGEMENT as e ON d.EventGroupID = e.EventGroupID AND d.StudentID = e.StudentID
                                        GROUP BY a.HouseID ORDER BY a.HouseID";
										*/
				$sql = "SELECT COUNT(e.StudentID), a.AgeGroupID
						FROM
							SPORTS_AGE_GROUP as a,
							INTRANET_USER as b,
							INTRANET_USERGROUP as c,
							INTRANET_HOUSE as d,
							SPORTS_STUDENT_ENROL_EVENT as e
						WHERE
							d.HouseID = '$houseID'
							AND c.GroupID = d.GroupID
							AND b.UserID = c.UserID
							AND (a.DOBLowLimit IS NULL || b.DateOfBirth <= a.DOBLowLimit)
							AND (a.DOBUpLimit IS NULL || b.DateOfBirth >= a.DOBUpLimit)
							AND a.Gender = b.Gender
							AND b.RecordStatus IN (0,1,2) AND b.RecordType = 2
							AND e.StudentID = c.UserID
							GROUP BY a.AgeGroupID ORDER BY a.AgeGroupID";
							/*
$sql = "SELECT COUNT(e.StudentID), a.AgeGroupID
						FROM
							SPORTS_AGE_GROUP as a,
							INTRANET_USERGROUP as c,
							INTRANET_HOUSE as d,
							INTRANET_USER as b
							LEFT JOIN SPORTS_STUDENT_ENROL_EVENT as e ON e.StudentID = b.UserID
						WHERE
							d.HouseID = '$houseID'
							AND c.GroupID = d.GroupID
							AND b.UserID = c.UserID
							AND (a.DOBLowLimit IS NULL || UNIX_TIMESTAMP(b.DateOfBirth) <= UNIX_TIMESTAMP(a.DOBLowLimit))
							AND (a.DOBUpLimit IS NULL || UNIX_TIMESTAMP(b.DateOfBirth) >= UNIX_TIMESTAMP(a.DOBUpLimit))
							AND a.Gender = b.Gender
							AND b.RecordStatus IN (0,1,2) AND b.RecordType = 2
							GROUP BY a.AgeGroupID ORDER BY a.AgeGroupID";
							debug($sql);
							*/
							/*
				$sql = "SELECT COUNT(e.StudentID), a.AgeGroupID
						FROM
							SPORTS_AGE_GROUP as a
							LEFT JOIN INTRANET_USER as b ON 
								(a.DOBLowLimit IS NULL || UNIX_TIMESTAMP(b.DateOfBirth) <= UNIX_TIMESTAMP(a.DOBLowLimit))
								AND (a.DOBUpLimit IS NULL || UNIX_TIMESTAMP(b.DateOfBirth) >= UNIX_TIMESTAMP(a.DOBUpLimit))
								AND a.Gender = b.Gender
							LEFT JOIN INTRANET_USERGROUP as c ON
								
							INTRANET_HOUSE as d,
							SPORTS_STUDENT_ENROL_EVENT as e
						WHERE
							d.HouseID = '$houseID'
							AND b.RecordStatus IN (0,1,2) AND b.RecordType = 2
							AND b.UserID = c.UserID
							AND e.StudentID = c.UserID
							AND c.GroupID = d.GroupID
							GROUP BY a.AgeGroupID ORDER BY a.AgeGroupID";
								*/		
                $temp = $this->returnArray($sql, 3);

                for($i=0; $i<sizeof($temp); $i++)
                {
                        list($count, $group_id) = $temp[$i];
                        $result[$group_id] = $count;
                }

                return  $result;
        }

        function retrieveAgeGroupNumberOfEnrolmentByEventID($eventID)
        {
                $sql = "SELECT COUNT(b.UserID), a.AgeGroupID
                                        FROM SPORTS_AGE_GROUP as a,
                                        INTRANET_USER as b,
                                        SPORTS_EVENTGROUP as c,
                                        SPORTS_STUDENT_ENROL_EVENT as d
                                        WHERE b.RecordStatus IN (0,1,2) AND b.RecordType = 2
                                        AND (a.DOBLowLimit IS NULL || Date(b.DateOfBirth) <= a.DOBLowLimit)
                                        AND (a.DOBUpLimit IS NULL || Date(b.DateOfBirth) >= a.DOBUpLimit)
                                        AND b.UserID = d.StudentID
                                                                                AND a.AgeGroupID = c.GroupID
                                        AND a.Gender = b.Gender
                                        AND c.EventGroupID = d.EventGroupID
                                        AND c.EventID = '$eventID'
                                        GROUP BY a.AgeGroupID ORDER BY a.AgeGroupID";
                $temp = $this->returnArray($sql, 2);

                for($i=0; $i<sizeof($temp); $i++)
                {
                        list($count, $group_id) = $temp[$i];
                        $result[$group_id] = $count;
                }

                                #Select Open Group Enrolment Info
                                $sql = "SELECT COUNT(a.StudentID), b.GroupID
                                                        FROM SPORTS_STUDENT_ENROL_EVENT as a
                                                        LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
                                                        WHERE GroupID IN (-1, -2, -4) AND b.EventID = '$eventID'
                                                        GROUP BY b.GroupID ORDER BY b.GroupID
                                                        ";
                                $openGroup= $this->returnArray($sql, 2);

                                for($i=0; $i<sizeof($openGroup); $i++)
                                {
                        list($count, $group_id) = $openGroup[$i];
                        $result[$group_id] = $count;
                }

                return  $result;
        }

        function retrievePersonalEnrolmentRecord($studentID)
        {
                $sql = "SELECT b.EventID FROM SPORTS_STUDENT_ENROL_EVENT as a LEFT JOIN SPORTS_EVENTGROUP as b ON a. EventGroupID = b.EventGroupID WHERE a.StudentID = '$studentID'";
                $temp = $this->returnVector($sql);

                                for($i=0; $i<sizeof($temp); $i++)
                {
                        $result[$temp[$i]] = 1;
                }

                return $result;
        }

        # Retreive Student ID of the Lange Arrangement
        # Param: $Heat, $ArrangeOrder, $EventGroupID
        function returnStudentIDFromLaneArrange($heat, $order, $eventGroupID)
        {
                $sql = "SELECT StudentID from SPORTS_LANE_ARRANGEMENT
                                        WHERE Heat = '$heat'
                                        AND ArrangeOrder = '$order'
                                        AND EventGroupID = '$eventGroupID'";
                $temp = $this->returnVector($sql);

                return $temp[0];
        }

        function returnStudentListByHouse($houseID)
        {
                $name_field = getNameFieldWithClassNumberByLang("a.");
        $sql = "SELECT a.UserID, $name_field FROM INTRANET_USER as a,
                                        INTRANET_USERGROUP as b,
                                        INTRANET_HOUSE as c
                    WHERE a.UserID = b.UserID
                                        AND b.GroupID = c.GroupID
                                        AND a.RecordType = 2
                                        AND a.RecordStatus IN (0,1,2)
                                        AND c.HouseID = '$houseID'
                                        ORDER BY a.UserID";

       return $this->returnArray($sql,2);
   }

   function returnStudentListByHouse2($houseID)
        {
                //$name_field = getNameFieldWithClassNumberByLang("a.");
                $name_field = getNameFieldByLang("a.");
        $sql = "SELECT a.UserID, $name_field, a.ClassName, a.ClassNumber FROM INTRANET_USER as a,
                                        INTRANET_USERGROUP as b,
                                        INTRANET_HOUSE as c
                    WHERE a.UserID = b.UserID
                                        AND b.GroupID = c.GroupID
                                        AND a.RecordType = 2
                                        AND a.RecordStatus IN (0,1,2)
                                        AND c.HouseID = '$houseID'
                                        ORDER BY a.ClassName, a.ClassNumber ";
		# 20081003 order by ClassName, Class number
       return $this->returnArray($sql,2);
   }

        # Retrieve Track and Field Event Lane Arrangement Detail
                # PARAM: $EventGroupID; 0- require name field with class number or not; round type of the arrangement
        function returnTFLaneArrangeDetailByEventGroupID($eventGroupID="", $withClassNum="", $round="")
        {
            $name_field = ($withClassNum == 1) ? getNameFieldWithClassNumberByLang("b."): getNameFieldByLang("b.");
			if(trim($round)!='')
				$cond_RoundType = " AND a.RoundType = '$round' ";
			if(trim($eventGroupID)!='')
				$cond_EventGroupID = " AND a.EventGroupID = '$eventGroupID' ";
			$TrackResult = $this->Get_Track_Result_Display_Sql("a.");
			
            $sql = "SELECT a.Heat, a.ArrangeOrder, $name_field, b.UserID, a.Rank, a.Score, $TrackResult as Result, ResultMetre, ResultTrial1, ResultTrial2, ResultTrial3, 
            		b.EnglishName, b.ChineseName, b.ClassName, b.ClassNumber
                        FROM 	
						SPORTS_LANE_ARRANGEMENT as a
                        INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                WHERE 1
                        $cond_EventGroupID
                        $cond_RoundType
                        ORDER BY a.Heat, a.ArrangeOrder";
            $temp = $this->returnArray($sql);

            return $temp;
         }

        function returnHeatNumberByEventGroupID($eventGroupID)
        {
            $sql = "SELECT DISTINCT a.Heat
                    FROM SPORTS_LANE_ARRANGEMENT as a
                    WHERE a.EventGroupID = '$eventGroupID' AND a.RoundType = '1' ";
            $temp = $this->returnVector($sql);
            
            return sizeof($temp);
        }

        # Retrieve House Relay Event Lane Arrangement
                function returnHRLaneArrangeDetailByEventGroupID($eventGroupID)
        {
                global $intranet_session_language;

                $namefield = ($intranet_session_language == "b5") ? "b.ChineseName" : "b.EnglishName";

				$TrackResult = $this->Get_Track_Result_Display_Sql("a.");
                        $sql = "SELECT $namefield, a.ArrangeOrder, b.HouseID, b.ColorCode, a.Rank, a.Score, $TrackResult as trackresult
                                        FROM SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT as a,
                                        INTRANET_HOUSE as b,
                                        SPORTS_EVENTGROUP as c
                    WHERE a.HouseID = b.HouseID
                                        AND a.EventGroupID = c.EventGroupID
                                        AND a.EventGroupID = '$eventGroupID'
                                        ORDER BY a.ArrangeOrder";
                $temp = $this->returnArray($sql,4);

                return $temp;
                }

	    # Retrieve Class Relay Event Lane Arrangement
	    function returnCRLaneArrangeDetailByEventGroupID($eventGroupID)
        {
			global $intranet_session_language;

			/* modiied by marcus 21/7/2009
	                $sql = "SELECT b.ClassName, a.ArrangeOrder, b.ClassID
	                                FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT as a,
	                                INTRANET_CLASS as b,
	                                SPORTS_EVENTGROUP as c
			            	WHERE 
			            			a.ClassID = b.ClassID
	                                AND a.EventGroupID = c.EventGroupID
	                                AND a.EventGroupID = '$eventGroupID'
	                                ORDER BY a.ArrangeOrder";
			*/
			$TrackResult = $this->Get_Track_Result_Display_Sql("a.");
            $sql = "SELECT
                        b.ClassTitleEN, b.ClassTitleB5, a.ArrangeOrder, b.YearClassID, a.Rank, a.Score, $TrackResult as trackresult, 
                        a.ClassID, a.ClassGroupID
                    FROM 
                        SPORTS_CLASS_RELAY_LANE_ARRANGEMENT as a,
                        YEAR_CLASS as b,
                        SPORTS_EVENTGROUP as c
	            	WHERE 
            			a.ClassID = b.YearClassID AND 
                        a.EventGroupID = c.EventGroupID AND 
                        a.EventGroupID = '$eventGroupID'
                    ORDER BY 
                        a.ArrangeOrder";
	        $temp = $this->returnArray($sql,3);
            
	        return $temp;
        }
        
        # Retrieve Participation Records
        # PARAM: $EventGroupID, $RoundType
        function returnParticipationRecordsByEventGroupID($eventGroupID, $eventType, $roundType)
        {
                        if($eventType==1)
                        {
                                $result_set = "ResultMin, ResultSec, ResultMs";
                                $num = 4;
                        }
                        else
                        {
                                $result_set = "ResultMetre";
                                $num = 2;
                        }

                        $sql = "SELECT StudentID, $result_set
                                                FROM SPORTS_LANE_ARRANGEMENT
                                                WHERE EventGroupID = '$eventGroupID'
                                                AND RoundType = '$roundType'
                                                AND RecordStatus NOT IN (1, 5, 6)
                                                ORDER BY StudentID";
           $temp = $this->returnArray($sql,$num);
           return $temp;
         }

		#Retrieve Number of Participation 
		#PARAM: $EventGroupID, $RoundType
		function returnNumberOfParticipationByEventGroupID($eventGroupID)
		{
			$sql = "SELECT Count(*)
						FROM SPORTS_LANE_ARRANGEMENT
						WHERE EventGroupID = '$eventGroupID'
						AND RoundType = 1
						AND RecordStatus NOT IN (1,6)";
			$temp = $this->returnVector($sql,1);
			
			return $temp[0];

		}
        # To check the value of the FirstRoundGroupCount. If the value is zero which mean Number Of Lane, return -1. Else return the value
        function returnFirstRoundGroupCount($eventGroupID, $eventType)
        {
                if($eventType == 1)
                        $table = "SPORTS_EVENTGROUP_EXT_TRACK";
                else if($eventType == 2)
                        $table = "SPORTS_EVENTGROUP_EXT_FIELD";

                $sql = "SELECT FirstRoundGroupCount FROM $table WHERE EventGroupID = '$eventGroupID'";
                $temp = $this->returnVector($sql);

                if(sizeof($temp) == 0)
                        return -1;
                else
                        return $temp[0];
         }

                 # Retrieve student enrolment detail
        function retrieveEventRaceDay($eventGroupID, $eventType)
        {
                        $qtable = ($eventType == 1)?"SPORTS_EVENTGROUP_EXT_TRACK":"SPORTS_EVENTGROUP_EXT_FIELD";

                        $sql = "SELECT FirstRoundDay
                         FROM $qtable
                         WHERE EventGroupID = '$eventGroupID'";
                        $temp = $this->returnVector($sql);

            return $temp[0];
         }

                # Retrieve student enrolment detail
        function retrieveStudentEnroledEventDetail()
        {
                global $intranet_session_language;

                $event_name = ($intranet_session_language == "b5") ? "c.ChineseName" : "c.EnglishName";

                $sql = "SELECT a.StudentID, a.Heat, a.ArrangeOrder, $event_name, c.EventType, a.EventGroupID
                                        FROM SPORTS_LANE_ARRANGEMENT as a
                                        LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
                                        LEFT JOIN SPORTS_EVENT as c ON b.EventID = c.EventID
                                        WHERE c.EventType IN (1,2) AND RoundType=1
                                        ORDER BY a.StudentID, c.EventType, c.EventID";
                $temp = $this->returnArray($sql, 6);

                return $temp;
         }

                # Retrieve Student ID, Name, Age Group Name, House Name and Athletic Number of Students in a class of a specific Age Group
                # PARAM: $ClassName, $AgeGroupID
				function retrieveStudentIDAndNameByClassNameAndAgeGroup($class_name, $ageGroupID, $withoutClassNumber = false)
				{
					global $intranet_session_language;
					
					$student_name = getNameFieldWithClassNumberByLang("a.");
					if($withoutClassNumber){
						$student_name = getNameFieldByLang("a.");
					}
					$age_group_name = ($intranet_session_language == "b5") ? "b.ChineseName" : "b.EnglishName";
					$house_name = ($intranet_session_language == "b5") ? "d.ChineseName" : "d.EnglishName";

					$sql = "SELECT 
								a.UserID, 
								$student_name, 
								$age_group_name, 
								$house_name, 
								e.AthleticNum
							FROM 
								INTRANET_USER as a,
								SPORTS_AGE_GROUP as b,
								INTRANET_USERGROUP as c,
								INTRANET_HOUSE as d,
								SPORTS_STUDENT_ENROL_INFO as e,
								YEAR_CLASS_USER ycu,
								INTRANET_GROUP as ig
							WHERE 
								a.RecordType = 2
								AND a.RecordStatus IN (0,1,2)
								AND (b.DOBLowLimit IS NULL || a.DateOfBirth <= b.DOBLowLimit)
								AND (b.DOBUpLimit IS NULL || a.DateOfBirth >= b.DOBUpLimit)
								AND a.Gender = b.Gender
								AND a.UserID = c.UserID
								AND c.GroupID = d.GroupID
								AND a.UserID = e.StudentID
								AND a.UserID = ycu.UserID
								AND c.GroupID = ig.GroupID
								AND ig.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
								AND ig.RecordType = 4
							";
								
					if ($ageGroupID != "") {
						$sql .= " AND b.AgeGroupID = '$ageGroupID'";
					}
					if ($class_name!= -3)
					{
						//$sql .= " AND a.ClassName = '$class_name'";
						$sql .= " AND ycu.YearClassID = '$class_name'";
					}
					$sql .= " ORDER BY a.ClassName, a.ClassNumber, a.UserID";

					$temp = $this->returnArray($sql,5);

					return $temp;
				}

				# Retrieve Student ID, Name, Age Group Name, House Name and Athletic Number of Students in a class of a specific Age Group
				# PARAM: $house, $ageGroupID
				function retrieveStudentIDAndNameByHouseAndAgeGroup($house, $ageGroupID, $withoutClassNumber = false)
				{
					global $intranet_session_language;

					$student_name = getNameFieldWithClassNumberByLang("a.");
					if($withoutClassNumber){
						$student_name = getNameFieldByLang("a.");
					}
					$age_group_name = ($intranet_session_language == "b5") ? "b.ChineseName" : "b.EnglishName";
					$house_name = ($intranet_session_language == "b5") ? "d.ChineseName" : "d.EnglishName";

					$sql = "SELECT a.UserID, $student_name, $age_group_name, $house_name, e.AthleticNum
							FROM INTRANET_USER as a,
							SPORTS_AGE_GROUP as b,
							INTRANET_USERGROUP as c,
							INTRANET_HOUSE as d,
							SPORTS_STUDENT_ENROL_INFO as e
							WHERE a.RecordType = 2
							AND a.RecordStatus IN (0,1,2)
							AND (b.DOBLowLimit IS NULL || a.DateOfBirth <= b.DOBLowLimit)
							AND (b.DOBUpLimit IS NULL || a.DateOfBirth >= b.DOBUpLimit)
							AND a.Gender = b.Gender
							AND a.UserID = c.UserID
							AND c.GroupID = d.GroupID
							AND a.UserID = e.StudentID";
					if ($ageGroupID != "") { 
						$sql .= " AND b.AgeGroupID = '$ageGroupID'";
					}
					if ($house != -3)
					{
						$sql .= " AND d.HouseID = '$house'";
					}
					$sql .= " ORDER BY a.ClassName, a.ClassNumber, a.UserID";
 
					$temp = $this->returnArray($sql,5);

					return $temp;
				}

                # Get Athletic Number Detail for Export. Return a Associate Array
                # PARAM: $ClassName, $AgeGroupID
				function getAthleticNumDeatil($groupType, $class_name, $house, $ageGroupID, $raceDay, $displayItems = array(), $fixedPos = 0, $event_font_family = '', $event_font_size = '')
                {
                	global $i_Sports_The, $i_Sports_Line, $i_Sports_Group, $i_Sports_Pos, $sys_custom, $Lang, $intranet_session_language;
                    include_once('libuser.php');
                        # Retrieve Student Info
                        
                        $withoutClassNumber = true;
                        if($sys_custom['eSports']['KaoYipReports']){
                        	$StudentInfo = $this->retrieveStudentIDAndName($ageGroupID, $withoutClassNumber);
                        } else {
                            if ($groupType == "1") {
                            	$StudentInfo = $this->retrieveStudentIDAndNameByClassNameAndAgeGroup($class_name, $ageGroupID, $withoutClassNumber);
                            } else {
                            	$StudentInfo = $this->retrieveStudentIDAndNameByHouseAndAgeGroup($house, $ageGroupID, $withoutClassNumber);
                            }
                        }
                        
                        # Retrieve Enroled Event Detail
                        $eventDetail = $this->retrieveStudentEnroledEventDetail();
                        $event = "";
                        if($fixedPos){
                        	$posCount = 0;
                        }
                        for($i=0; $i<sizeof($eventDetail); $i++)
                        {
                                list($sid, $heat, $order, $eventName, $eventType, $eventGroupID) = $eventDetail[$i];
                                $nextID = $eventDetail[$i+1][0];

                                                                # race day = 0 refer to ALL days
                                                                if($raceDay != 0)
                                                                {
                                                                        # retrieve the race day of the event
                                                                        $theDay = $this->retrieveEventRaceDay($eventGroupID, $eventType);

                                                                        if($theDay == $raceDay)
                                                                        {
                                                                        		if($fixedPos){
                                                                        			if($event == ""){
                                                                        				$event .= "<table width='100%'>";
                                                                        				$event .= "<th width='33%'></th><th width='33%'></th><th width='33%'></th>";
                                                                        			}
                                                                        			if(fmod($posCount, 3) == 0){
                                                                        				$event .= "<tr>";
                                                                        			}
                                                                        			$event .= "<td align='center' style='font-family:". $event_font_family."; font-size:". $event_font_size."'>";
                                                                        		}
                                                                                $event .= $eventName . " ";
                                                                                if(!empty($displayItems)){
                                                                                	if(in_array('Group', $displayItems)||in_array('Line', $displayItems)){
                                                                                		$event .= "(";
                                                                                		if(in_array('Group', $displayItems)){
                                                                                			if($intranet_session_language == 'en'){
                                                                                				$event .= $i_Sports_Group . $heat;
                                                                                			} else {
                                                                                				$event .= $i_Sports_The.$heat.$i_Sports_Group;
                                                                                			}
                                                                                			if(in_array('Line', $displayItems)) $event .= " ";
                                                                                		}
                                                                                		if(in_array('Line', $displayItems)){
                                                                                			if($intranet_session_language == 'en'){
                                                                                				$event .= (($eventType == 1)?$Lang['eSports']['Lane']:$Lang['eSports']['Order']) . $order;
                                                                                			} else {
                                                                                				$event .= $i_Sports_The.$order . (($eventType == 1)?$i_Sports_Line:$i_Sports_Pos);
                                                                                			}
                                                                                		}
                                                                                		$event .= ") ";
                                                                                	}
                                                                                } 
//                                                                                 else {
//                                                                                 	$event .= "(".$i_Sports_The.$heat.$i_Sports_Group;
//                                                                                 	$event .= " ".$i_Sports_The.$order;
//                                                                                 	$event .= ($eventType == 1)?$i_Sports_Line:$i_Sports_Pos;
//                                                                                 	$event .= ") ";
//                                                                                 }
                                                                                if($fixedPos){
                                                                                	$event .= "</td>";
                                                                                	if(fmod($posCount, 3) == 2){
                                                                                		$event .= "</tr>";
                                                                                	}
                                                                                	$posCount++;
                                                                                }
                                                                        }
                                                                }
                                                                else
                                                                {
	                                                                	if($fixedPos){
	                                                                		if($event == ""){
	                                                                			$event .= "<table border='1' width='100%'>";
	                                                                			$event .= "<th width='33%'></th><th width='33%' style='display:none'></th><th width='33%'></th>";
	                                                                		}
	                                                                		if(fmod($posCount, 3) == 0){
	                                                                			$event .= "<tr>";
	                                                                		}
	                                                                		$event .= "<td align='center' style='font-family:". $event_font_family."; font-size:". $event_font_size."'>";
	                                                                	}
                                                                		$event .= $eventName . " ";
                                                                		if(!empty($displayItems)){
                                                                        	if(in_array('Group', $displayItems)||in_array('Line', $displayItems)){
                                                                        		$event .= "(";
                                                                        		if(in_array('Group', $displayItems)){
                                                                        			if($intranet_session_language == 'en'){
                                                                        				$event .= $i_Sports_Group . $heat;
                                                                        			} else {
                                                                        				$event .= $i_Sports_The.$heat.$i_Sports_Group;
                                                                        			}
                                                                        			if(in_array('Line', $displayItems)) $event .= " ";
                                                                        		}
                                                                        		if(in_array('Line', $displayItems)){
                                                                        			if($intranet_session_language == 'en'){
                                                                        				$event .= (($eventType == 1)?$Lang['eSports']['Lane']:$Lang['eSports']['Order']) . $order;
                                                                        			} else {
                                                                        				$event .= $i_Sports_The.$order . (($eventType == 1)?$i_Sports_Line:$i_Sports_Pos);
                                                                        			}
                                                                        		}
                                                                        		$event .= ") ";
                                                                        	}
                                                                        }
//                                                                         else {
// 	                                                                        $event .= "(".$i_Sports_The.$heat.$i_Sports_Group;
// 	                                                                        $event .= " ".$i_Sports_The.$order;
// 	                                                                        $event .= ($eventType == 1)?$i_Sports_Line:$i_Sports_Pos;
// 	                                                                        $event .= ") ";
//                                                                         }
                                                                        if($fixedPos){
                                                                        	$event .= "</td>";
                                                                        	if(fmod($posCount, 3) == 2){
                                                                        		$event .= "</tr>";
                                                                        	}
                                                                        	$posCount++;
                                                                        }
                                                                }

                                                                if($nextID != $sid || $nextID == "")
                                                                {
                                                                        if($event != ""){
                                                                        		if($fixedPos){
                                                                        			$event .= "</table>";
                                                                        			$posCount = 0;
                                                                        		}
                                                                                $detailArr[$sid] = $event;
                                                                        }
                                                                        $event = "";
                                                                }
                                                }
                        if($sys_custom['eSports']['KaoYipReports']) {
                            for($j=0; $j<sizeof($StudentInfo); $j++)
                            {
                                list($sid, $studentName, $ageGroupName, $athleticNum) = $StudentInfo[$j];
                                if($detailArr[$sid] != "")
                                {
                                	if(!empty($displayItems)){
                                		if(in_array('Class',$displayItems)||in_array('ClassNo', $displayItems)){
                                			$lu = new libuser($sid);
                                			$className = $lu->ClassName;
                                			$classNo = $lu->ClassNumber;
                                			$studentName .= " (";
                                			if(in_array('Class',$displayItems)){
                                				$studentName .= $className;
                                				if(in_array('ClassNo', $displayItems)) $studentName .= " - ";
                                			}
                                			if(in_array('ClassNo', $displayItems)) $studentName .= $classNo;
                                			
                                			$studentName .= ")";
                                		}
                                	}
                                    $result[$sid][] = $studentName;
                                    $result[$sid][] = $ageGroupName;
                                    $result[$sid][] = $athleticNum;
                                    $result[$sid][] = $detailArr[$sid];
                                }
                            }
                        } else {
                            for($j=0; $j<sizeof($StudentInfo); $j++)
                            {
                                list($sid, $studentName, $ageGroupName, $houseName, $athleticNum) = $StudentInfo[$j];

                                if($detailArr[$sid] != "")
                                {
                                	if(!empty($displayItems)){
                                		if(in_array('Class',$displayItems)||in_array('ClassNo', $displayItems)){
                                			$lu = new libuser($sid);
                                			$className = $lu->ClassName;
                                			$classNo = $lu->ClassNumber;
                                			$studentName .= " (";
                                			if(in_array('Class',$displayItems)){
                                				$studentName .= $className;
                                				if(in_array('ClassNo', $displayItems)) $studentName .= " - ";
                                			}
                                			if(in_array('ClassNo', $displayItems)) $studentName .= $classNo;
                                			
                                			$studentName .= ")";
                                		}
                                	}
                                    $result[$sid][] = $studentName;
                                    $result[$sid][] = $ageGroupName;
                                    $result[$sid][] = $houseName;
                                    $result[$sid][] = $athleticNum;
                                    $result[$sid][] = $detailArr[$sid];
                                }
                            }
                        }
                        return $result;
       }

####################################################

	   # modified by Marcus 20/7/2009

       /*function addAdminUser($targetLogin, $userType='ADMIN')
       {
                $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$targetLogin'";
                $temp = $this->returnVector($sql);
                $targetID = $temp[0];

                if ($targetID == "")
                {
                    return 0;
                }
                else
                {
                    $sql = "INSERT INTO SPORTS_ADMIN_USER_ACL (AdminUserID, UserType) VALUES ('$targetID', '$userType')";
                    $result = $this->db_db_query($sql);
                    if ($result)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
       }*/
	   
	   function addAdminUser($targetArr, $userType)
       {
			foreach($targetArr as $targetLogin)
			{
                $sql = "SELECT COUNT(*) FROM SPORTS_ADMIN_USER_ACL WHERE AdimnUserID = '$targetLogin'";
                $temp = $this->returnVector($sql);
                $targetID = $temp[0];

                if ($targetID)
                {
                    $sql = "UPDATE SPORTS_ADMIN_USER_ACL SET UserType = '$userType', DateModified = NOW() WHERE AdminUserID = '$targetLogin'";
                }
                else
                {
                    $sql = "INSERT INTO SPORTS_ADMIN_USER_ACL (AdminUserID, UserType, DateInput, DateModified) VALUES ('$targetLogin', '$userType',NOW(),NOW())";
                }
                   $this->db_db_query($sql);				
			}
       }
	   
	   
       function updateAdminUser($targetLogins, $userType)
       {
//                $list = implode(",", $targetLogins);
//                $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin IN ($list)";
//                $temp = $this->returnVector($sql);

				//modified by marcus  21/07/2009
				//$list = implode(",", $targetLogins);
				if(is_array($targetLogins))
					$list = implode(",", $targetLogins);
				else
					$list = $targetLogins;
//
//                if ($targetID == "")
//                {
//                    return 0;
//                }
//                else
//                {
                    $sql = "UPDATE SPORTS_ADMIN_USER_ACL SET UserType = '$userType' WHERE AdminUserID IN ($list)";
                    $result = $this->db_db_query($sql);
                    if ($result)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
//                }
       }
	   
	   
       function removeAdminUser($targetLogins)
       {
				//modified by marcus  21/07/2009
				//$list = implode(",", $targetLogins);
				if(is_array($targetLogins))
					$list = implode(",", $targetLogins);
				else
					$list = $targetLogins;
                $sql = "DELETE FROM SPORTS_ADMIN_USER_ACL WHERE AdminUserID IN ($list)";
                $this->db_db_query($sql);
       }
       function isAdminUser($targetUser)
       {
				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
					return true;
					
                $sql = "SELECT AdminUserID FROM SPORTS_ADMIN_USER_ACL WHERE AdminUserID = '$targetUser' AND UserType = 'ADMIN'";
                $temp = $this->returnVector($sql);
                return ($temp[0]==$targetUser);
       }
       function isAdminOrHelper($targetUser)
       {
   				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
					return true;

                $sql = "SELECT AdminUserID FROM SPORTS_ADMIN_USER_ACL WHERE AdminUserID = '$targetUser' AND UserType IN ('ADMIN', 'HELPER')";
                $temp = $this->returnVector($sql);
                return ($temp[0]==$targetUser);
       }
       function isYearClassTeacher()
       {
                global $sys_custom;
                if($sys_custom['eSports']['KaoYipRelaySettings'] && !$_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["is_class_teacher"]) {
                    return true;
                }
       }
       function teachingYearClassList()
       {
               $sql = "SELECT
        					yc.YearClassID
            			FROM
        					YEAR_CLASS as yc
              				INNER JOIN YEAR_CLASS_TEACHER yct ON (yc.YearClassID = yct.YearClassID)
    			        WHERE
    					    yct.UserID = '".$_SESSION['UserID']."' AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' ";
               return $this->returnVector($sql);
       }

       # Get UserLogin from UserID
       function getUserLoginByUserID($UserID)
       {
                $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserID = '$UserID'";
                $row = $this->returnVector($sql);
                return $row[0];
       }

       # Get UserType from AdminUserID
       function getUserTypeByAdminUserID($AdminUserID)
       {
                $sql = "SELECT UserType FROM SPORTS_ADMIN_USER_ACL WHERE AdminUserID = '$AdminUserID'";
                $row = $this->returnVector($sql);
                return $row[0];
       }

       function GET_MODULE_OBJ_ARR()
        {
                global $plugin, $PATH_WRT_ROOT, $i_Discipline_System, $CurrentPage, $iDiscipline, $LAYOUT_SKIN, $image_path, $CurrentPageArr;
                ### wordings
                global $i_eSports, $i_Sports_menu_Settings, $i_Sports_menu_General_Settings, $i_Sports_menu_Annual_Settings, $i_Sports_menu_Item_Settings;
                global $i_Sports_menu_Arrangement, $i_Sports_menu_Arrangement_EnrolmentUpdate, $i_Sports_menu_Arrangement_Schedule, $i_Sports_menu_Report_ExportRecord;
                global $iDiscipline, $i_Sports_menu_Report_WholeSchool, $i_Sports_menu_Report_Class, $i_Sports_menu_Report_House, $i_Sports_menu_Report_Event;
                global $i_Sports_menu_Report_RaceResult, $i_Sports_menu_Settings_ParticipantRecord, $i_Sports_menu_Report_RaceTopRecord, $i_Sports_Day_Enrolment;
                global $i_Sports_menu_Report_AllArrangement;
				global $Lang, $sys_custom;
				
                global $UserID, $top_menu_mode;
                
				if($CurrentPage=="PageEnroll")
                {
					$CurrentPageArr['eService'] = 1;
					$CurrentPageArr['Home'] = 0;				
                    $CurrentPageArr['eServiceeSports'] = 1;
                    $CurrentPageArr['eServiceSportDay'] = 1;

                }
				else
				{
					$CurrentPageArr['eAdmin'] = 1;
					$CurrentPageArr['Home'] = 0;		
					$CurrentPageArr['StudentManagement'] = 1;
                    $CurrentPageArr['eSports'] = 1;		
                    $CurrentPageArr['SportDay'] = 1;
				}



                # Current Page Information init
                $PageSettings = 0;
                $PageGeneralSettings = 0;
                $PageAnnualSettings = 0;
                $PageItemSettings = 0;

                $PageArrangement = 0;
                $PageArrangement_EnrolmentUpdate = 0;
                $PageArrangement_Schedule = 0;
                $PageReport_ExportRecord = 0;

                $PageReport = 0;
                $PageRaceResult = 0;
                $PageParticipantRecord = 0;
                $PageRaceTopRecord = 0;
                $PageAllArrangement = 0;

                $PageStatistics = 0;
                $PageReport_WholeSchool = 0;
                $PageReport_Class = 0;
                $PageReport_House = 0;
                $PageReport_Event = 0;

                $PageEnroll = 0;

                switch ($CurrentPage) {

                        case "PageGeneralSettings":
                                $PageSettings = 1;
                                $PageGeneralSettings = 1;
                                break;

                        case "PageAnnualSettings":
                                $PageSettings = 1;
                                $PageAnnualSettings = 1;
                                break;

                        case "PageItemSettings":
                                $PageSettings = 1;
                                $PageItemSettings = 1;
                                break;
								
                        case "PageAdminSettings":
                                $PageSettings = 1;
                                $PageAdminSettings = 1;
                                break;
                                
                        case "PageEnrolmentRestriction":
                                $PageSettings = 1;
                                $PageEnrolmentRestrictionSettings = 1;
                                break;        

                        case "PageArrangement_EnrolmentUpdate":
                                $PageArrangement = 1;
                                $PageArrangement_EnrolmentUpdate = 1;
                                break;

                        case "PageArrangement_Schedule":
                                $PageArrangement = 1;
                                $PageArrangement_Schedule = 1;
                                break;
                                
                        case "PageArrangement_ClassRelayEnrol":
                                $PageArrangement = 1;
                                $PageArrangement_ClassRelayEnrol = 1;
                                break;

                        case "PageReport_ExportRecord":
                                $PageArrangement = 1;
                                $PageReport_ExportRecord = 1;
                                break;

                        case "PageRaceResult":
                                $PageReport = 1;
                                $PageRaceResult = 1;
                                break;

                        case "PageParticipantRecord":
                                $PageReport = 1;
                                $PageParticipantRecord = 1;
                                break;

                        case "PageRaceTopRecord":
                                $PageReport = 1;
                                $PageRaceTopRecord = 1;
                                break;
                                
                        case "PageAllArrangement":
                                $PageReport = 1;
                                $PageAllArrangement = 1;
                                break;

                        case "PageReport_WholeSchool":
                                $PageStatistics = 1;
                                $PageReport_WholeSchool = 1;
                                break;

                        case "PageReport_Class":
                                $PageStatistics = 1;
                                $PageReport_Class = 1;
                                break;

                        case "PageReport_House":
                                $PageStatistics = 1;
                                $PageReport_House = 1;
                                break;

                        case "PageReport_Event":
                                $PageStatistics = 1;
                                $PageReport_Event = 1;
                                break;

                        case "PageEnroll":
                                $PageEnroll = 1;
                                break;
                }

                # Menu information
                if($plugin['Sports'])
                {
					if($PageEnroll)
					{
					    include_once($PATH_WRT_ROOT."includes/libuser.php");
                        $lu = new libuser($UserID);

                        if($lu->isStudent())
                        {
                            $MenuArr["Enroll"] = array($i_Sports_Day_Enrolment, "#", $PageEnroll);
                        }
					}
					else if ($sys_custom['eSports']['KaoYipRelaySettings'] && !$_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["is_class_teacher"])
					{
					    $MenuArr["Arrangement"] = array($i_Sports_menu_Arrangement, "#", $PageArrangement);
					    $MenuArr["Arrangement"]["Child"]["Arrangement_ClassRelayEnrol"] = array($Lang['eSports']['ClassRelayEnrol'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/arrange/class_relay_enrol.php", $PageArrangement_ClassRelayEnrol);
					}
					else if ($_SESSION['intranet_sports_right']==1)
                    {
                        $MenuArr["Arrangement"] = array($i_Sports_menu_Arrangement, "", $PageArrangement);
                        $MenuArr["Arrangement"]["Child"]["Arrangement_EnrolmentUpdate"] = array($i_Sports_menu_Arrangement_EnrolmentUpdate, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/arrange/enrol_update.php", $PageArrangement_EnrolmentUpdate);
                        if ($sys_custom['eSports']['KaoYipRelaySettings']) {        // [2018-0628-1634-01096]
                            $MenuArr["Arrangement"]["Child"]["Arrangement_ClassRelayEnrol"] = array($Lang['eSports']['ClassRelayEnrol'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/arrange/class_relay_enrol.php", $PageArrangement_ClassRelayEnrol);
                        }
                        $MenuArr["Arrangement"]["Child"]["Arrangement_Schedule"] = array($i_Sports_menu_Arrangement_Schedule, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/arrange/schedule.php", $PageArrangement_Schedule);
                        $MenuArr["Arrangement"]["Child"]["Report_ExportRecord"] = array($i_Sports_menu_Report_ExportRecord, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/report/export_arrangement.php", $PageReport_ExportRecord);

                        $MenuArr["Report"] = array($iDiscipline['Reports'], "", $PageReport);
                        $MenuArr["Report"]["Child"]["RaceResult"] = array($i_Sports_menu_Report_RaceResult, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/participation/tf_record.php", $PageRaceResult);
                        if(!$sys_custom['eSports']['PuiChi_MultipleGroup']) {       // [2016-0627-1013-19066]
                        	$MenuArr["Report"]["Child"]["ParticipantRecord"] = array($i_Sports_menu_Settings_ParticipantRecord, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/report/class_enrol.php", $PageParticipantRecord);
                        }
                        $MenuArr["Report"]["Child"]["RaceTopRecord"] = array($i_Sports_menu_Report_RaceTopRecord, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/report/race.php", $PageRaceTopRecord);
                        if($sys_custom['eSports']['KaoYipReports']) {               // [2018-0628-1634-01096]
                            $MenuArr["Report"]["Child"]["AllArrangement"] = array($Lang['Sports']['Report']['AllArrangement'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/report/all_arrangement.php", $PageAllArrangement);
                        }

                        $MenuArr["Statistics"] = array($iDiscipline['Statistics'], "", $PageStatistics);
                        $MenuArr["Statistics"]["Child"]["Statistics_WholeSchool"] = array($i_Sports_menu_Report_WholeSchool, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/report/wholesch.php", $PageReport_WholeSchool);
                        $MenuArr["Statistics"]["Child"]["Statistics_Class"] = array($i_Sports_menu_Report_Class, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/report/class.php", $PageReport_Class);
                        $MenuArr["Statistics"]["Child"]["Statistics_House"] = array($i_Sports_menu_Report_House, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/report/house.php", $PageReport_House);
                        $MenuArr["Statistics"]["Child"]["Statistics_Event"] = array($i_Sports_menu_Report_Event, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/report/event.php", $PageReport_Event);

                        if ($this->isAdminUser($UserID))
                        {
                            $MenuArr["Settings"] = array($i_Sports_menu_Settings, "", $PageSettings);
                            $MenuArr["Settings"]["Child"]["GeneralSettings"] = array($i_Sports_menu_General_Settings, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/settings/house.php", $PageGeneralSettings);
                            $MenuArr["Settings"]["Child"]["AnnualSettings"] = array($i_Sports_menu_Annual_Settings, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/settings/agegroup.php", $PageAnnualSettings);
                            $MenuArr["Settings"]["Child"]["ItemSettings"] = array($i_Sports_menu_Item_Settings, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/settings/tf_name.php", $PageItemSettings);
                            if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
								$MenuArr["Settings"]["Child"]["AdminSettings"] = array($Lang['Sports']['AdminSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/settings/admin_setting/?clearCoo=1", $PageAdminSettings);
							$MenuArr["Settings"]["Child"]["EnrolmentRestriction"] = array($Lang['eSports']['EnrolmentRestriction'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eSports/sports/settings/EnrolmentRestriction/", $PageEnrolmentRestrictionSettings);
						}
                    }
                }

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass Sports Day";'."\n";
            $js.= '</script>'."\n";

                # module information
                $MODULE_OBJ['title'] = $Lang['Header']['Menu']['SportDay'].$js;
                $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eSports.gif";
				$MODULE_OBJ['title_css'] = "menu_opened";
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/eSports/sports/index.php";
                $MODULE_OBJ['menu'] = $MenuArr;

                return $MODULE_OBJ;
        }

        function house_flag($color="")
        {
				global $LAYOUT_SKIN, $image_path;

                if($color)
                {
                	$x = "
                        <!--########-HOUSE FLAG START -########-->
                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                        <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                        <td width=\"4\" height=\"11\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/esport/house_flag_01.gif\" width=\"4\" height=\"11\"></td>
                        <td><table width=\"6\" height=\"11\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                        <tr>
                        <!--########-HOUSE FLAG COLOR 1 of 3 -########-->
                        <td width=\"6\" height=\"9\" bgcolor=\"#{$color}\"><!--########-HOUSE FLAG 1 END-########-->
                        <img src=\"{$image_path}/{$LAYOUT_SKIN}/esport/house_flag_02a.gif\" width=\"6\" height=\"9\"></td>
                        </tr>
                        <tr>
                        <td width=\"6\" height=\"2\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/esport/house_flag_02b.gif\" width=\"6\" height=\"2\"></td>
                        </tr>
                        </table></td>
                        <td width=\"4\" height=\"11\" ><table width=\"4\" height=\"11\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                        <td width=\"4\" height=\"3\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/esport/house_flag_03a.gif\" width=\"4\" height=\"3\"></td>
                        </tr>
                        <tr>
                        <!--########-HOUSE FLAG COLOR 2 of 3 -########-->
                        <td width=\"4\" height=\"8\" bgcolor=\"#{$color}\"><!--########-HOUSE FLAG 1 END-########-->
                        <img src=\"{$image_path}/{$LAYOUT_SKIN}/esport/house_flag_03b.gif\" width=\"4\" height=\"8\"></td>
                        </tr>
                        </table></td>
                        <td width=\"3\" height=\"11\"><table width=\"3\" height=\"11\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                        <tr>
                        <td width=\"3\" height=\"2\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/esport/house_flag_04a.gif\" width=\"3\" height=\"2\"></td>
                        </tr>
                        <tr>
                        <!--########-HOUSE FLAG COLOR 3 of 3 -########-->
                        <td width=\"3\" height=\"9\" bgcolor=\"#{$color}\"><!--########-HOUSE FLAG 1 END-########-->
                        <img src=\"{$image_path}/{$LAYOUT_SKIN}/esport/house_flag_04b.gif\" width=\"3\" height=\"9\"></td>
                        </tr>
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td width=\"17\" height=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/esport/house_flag_05.gif\" width=\"17\" height=\"6\"></td>
                        </tr>
                        </table>
                        <!--########-HOUSE FLAG END -########-->
                        ";
		} else {
                	$x = "";
		}

        	return $x;
        }

        function house_flag2($color, $info)
        {
        	$x = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td>";
                $x .= $this->house_flag($color);
                $x .="</td><td valign=\"bottom\" class=\"tabletext\">".$info."</td></tr></table>";

                return $x;
        }

		//modified by marcus 20101104
        function retrieveStudentEventResult($StudentID)
        {
	        global $intranet_session_language;

			$TrackResult = $this->Get_Track_Result_Display_Sql("a.");
			
	        # retrieve student event list
	        $event_name = ($intranet_session_language == "b5") ? "c.ChineseName" : "c.EnglishName";

	         $sql = "	SELECT
	        				a.StudentID, 
							$event_name as event_name, 
							a.Rank, 
							a.RoundType, 
							a.EventGroupID, 
							IF(b.CountIndividualScore = 1,a.Score,0) Score, 
							IF(c.EventType=2, a.ResultMetre , $TrackResult) Result, 
							a.RecordStatus,
							IF(a.RoundType=0,3,a.RoundType) as OrderField
						FROM
							SPORTS_LANE_ARRANGEMENT as a
                            LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
                            LEFT JOIN SPORTS_EVENT as c ON b.EventID = c.EventID
						WHERE
							a.StudentID = $StudentID AND
							c.EventType IN (1,2) 
						ORDER BY
							event_name ASC, OrderField DESC";
                $temp = $this->returnArray($sql);
                
                $ResultArr = BuildMultiKeyAssoc($temp,array("EventGroupID","RoundType"));
                
                foreach($ResultArr as $EventGroupID => $RoundResultArr)
                {
                	$thisScore = 0;
                	foreach($RoundResultArr as $RoundType => $RoundResult)
                	{
//                		list($tempStudentID, $tempEventName, $tempRank, $tmpRoundType, $tmpEventGroupID, $tmpScore, $tmpResult) = $RoundResult;
                		$thisScore +=  $RoundResult['Score'];
                	}
                	if($RoundResultArr[0])
                		$thisFinalRound = $RoundResultArr[0];
                	else if($RoundResultArr[2])
                		$thisFinalRound = $RoundResultArr[2];
                	else
                		$thisFinalRound = $RoundResultArr[1];
                	
                	list($tempStudentID, $tempEventName, $tempRank, $tmpRoundType, $tmpEventGroupID, $tmpScore, $tmpResult) = $thisFinalRound;
                	$thisFinalRound['Score'] = $thisScore;
                	$thisFinalRound[5] = $thisScore;

                	$result[] = $thisFinalRound;
                }
//                debug_pr($ResultArr);
//				for($j=0; $j<sizeof($temp); $j++)
//				{
//					list($tempStudentID, $tempEventName, $tempRank, $tmpRoundType, $tmpEventGroupID) = $temp[$j];
//					if($temp[$j][1]!=$temp[$j-1][1])
//						$result[] = $temp[$j];
//				}
////				debug_t($temp);
////				debug_t($result);
//				
//			# sum the score for each event
//			$sql = "	SELECT
//	        				a.StudentID, a.EventGroupID, IF(b.CountIndividualScore = 1,ROUND(sum(a.Score),1),'-') as Score
//						FROM
//							SPORTS_LANE_ARRANGEMENT as a
//                            LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
//                            LEFT JOIN SPORTS_EVENT as c ON b.EventID = c.EventID
//						WHERE
//							a.StudentID = $StudentID AND
//							c.EventType IN (1,2) 
//						
//						";
//						
//                $temp2 = $this->returnArray($sql);	
//				debug_t($temp2);
//                for($j=0; $j<sizeof($result); $j++)
//				{
//					list($tempStudentID, $tempEventName, $tempRank, $tmpRoundType, $tmpEventGroupID) = $result[$j];
//					
//					for($i=0; $i<sizeof($temp2); $i++)
//					{
//						if($result[$j]['EventGroupID'] == $temp2[$i]['EventGroupID'])
//						{
//							$result[$j][5] = $temp2[$i]['Score'];
//							$result[$j]['Score'] = $temp2[$i]['Score'];
//						}
//					}
//				}
				
				return $result;
				
	        /*
	        $sql = "	SELECT
	        				a.StudentID, $event_name as event_name, a.Rank, a.Score, a.RoundType
						FROM
							SPORTS_LANE_ARRANGEMENT as a
                            LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
                            LEFT JOIN SPORTS_EVENT as c ON b.EventID = c.EventID
						WHERE
							a.StudentID = $StudentID AND
							c.EventType IN (1,2) 
						ORDER BY
							event_name, RoundType";
                $temp = $this->returnArray($sql);
			//and a.RoundType = 0
			
			for($j=0; $j<sizeof($temp); $j++)
			{
				list($tempStudentID, $tempEventName, $tempRank, $tempScore, $tmpRoundType) = $temp[$j];
				if($temp[$j][1]!=$temp[$j-1][1])
					$result[] = $temp[$j];
			}
                
                return $result;
               */
        }
        
        function retrieveEventIDByAgeGroup($ageGroupID)
        {
	        global $intranet_session_language;

	        $event_name = ($intranet_session_language == "b5") ? "b.ChineseName" : "b.EnglishName";
	        
	        $sql = "
		        select 
		        	a.EventID,
					$event_name as EventName,
					b.EventType
				from 
					SPORTS_EVENTGROUP as a
					left join SPORTS_EVENT as b on (a.EventID = b.EventID)
				where 
					a.GroupID=$ageGroupID
				order by
					EventName
			";
			$temp = $this->returnArray($sql);
			return $temp;
			
	        /*
	        global $intranet_session_language;

	        $event_name = ($intranet_session_language == "b5") ? "c.ChineseName" : "c.EnglishName";

	        $sql = "	SELECT
	        				a.StudentID, $event_name as event_name, a.Rank, a.Score
						FROM
							SPORTS_LANE_ARRANGEMENT as a
                            LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
                            LEFT JOIN SPORTS_EVENT as c ON b.EventID = c.EventID
						WHERE
							a.StudentID = $StudentID AND
							c.EventType IN (1,2) and
							a.RoundType = 0
						ORDER BY
							event_name";
                $temp = $this->returnArray($sql);

                return $temp;
                */
        }
        
        # Retrieve Track and Field Event Ranking
        // modified by marcus 20091209 (add result metre and result time)
        function retrieveTFResult($eventID, $groupID)
        {
             $name_field = getNameFieldWithClassNumberByLang("c.");
             $username_field = getNameFieldByLang("c.");
								
            $sql = "SELECT 
						a.StudentID, 
						a.RoundType,
						a.Rank, 
						a.Score,
						a.ResultMetre,
						a.ResultMin,
						a.ResultSec,
						a.ResultMs,
						a.RecordStatus
					FROM 
						SPORTS_LANE_ARRANGEMENT as a
						LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
					WHERE 
						b.EventID = '$eventID'
						AND b.GroupID = '$groupID'
					ORDER BY 
						a.Score
					DESC
                    ";
            $temp = $this->returnArray($sql);

            $data = array();
            foreach($temp as $k=>$d)
            {
	            list($sid, $roundType, $rank, $score,$metre,$min,$sec,$ms,$status) = $d;
	            
				$data[$sid][$roundType]['Rank'] = $rank;
				$data[$sid][$roundType]['Score'] = $score;
				$data[$sid][$roundType]['ResultMetre'] = $metre;
				$data[$sid][$roundType]['ResultMin'] = $min;
				$data[$sid][$roundType]['ResultSec'] = $sec;
				$data[$sid][$roundType]['ResultMs'] = $ms;
				$data[$sid][$roundType]['RecordStatus'] = $status;
            }
            
			 return $data;
          }
          
          # ***did not select current year house info, use retrieveStudentHouseInfo() instead***
          function retrieveStudentHouse($sid)
            {
	            global $intranet_session_language;
	            
	            $house_name = ($intranet_session_language == "b5") ? "b.ChineseName" : "b.EnglishName";

	            /*$sql = "SELECT $house_name
						FROM INTRANET_USERGROUP as a
						LEFT OUTER JOIN INTRANET_HOUSE as b ON a.GroupID = b.GroupID
						LEFT JOIN INTRANET_GROUP as c ON a.GroupID = c.GroupID
						WHERE c.RecordType = '".GROUP_TYPE_HOUSE."' AND a.UserID = '$sid'";*/
				$sql = "
					SELECT 
						$house_name 
					FROM 
						INTRANET_USERGROUP as a   
						INNER JOIN INTRANET_GROUP c ON a.GroupID = c.GroupID 
						INNER JOIN INTRANET_HOUSE as b ON c.GroupID = b.GroupID 
					WHERE
						a.UserID = '$sid' 
						AND c.RecordType = ".GROUP_TYPE_HOUSE."                                               
					";							
						
				$result = $this->returnVector($sql);
                                            
                    return $result[0];
       	 }
       	 
       	 function retrieveClassRelayScore()
        {
                $sql = "SELECT 
                			ClassID, SUM(Score)
                        FROM 
                            SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
                        GROUP BY 
                        	ClassID 
                        ORDER BY 
                        	ClassID
						";
						
                $temp = $this->returnArray($sql, 2);
				$result = array();
				
				foreach($temp as $k=>$d)
					$result[$d[0]] = $d[1];
				
                return  $result;
        }
 
 		function returnBestRecordResultOfTrackEventGroup($eventGroupID)
		{
			$result = "(a.resultMin*60*100+a.resultSec*100+a.resultMs)";
			$oldrecord= "(b.recordMin*60*100+b.recordSec*100+b.recordMs)";
			$orderType = "ASC";

			$sql = 	"	SELECT 
							a.StudentID, a.ResultMin, a.ResultSec, a.ResultMs
						FROM 
							SPORTS_LANE_ARRANGEMENT a 
							LEFT JOIN SPORTS_EVENTGROUP_EXT_TRACK b ON a.eventGroupID = b.eventGroupID
						WHERE a.EventGroupID=$eventGroupID
							AND $result <> 0
							AND ($result < $oldrecord OR $oldrecord = 0) 
						ORDER BY $result $orderType, a.dateModified ASC
						LIMIT 0,1
						";
			$temp = $this->returnArray($sql,4);
			return $temp;
		}


		
		function removeNewRecordOfTrackEventGroup($eventGroupID)
		{
			$sql = "	UPDATE 
							SPORTS_EVENTGROUP_EXT_TRACK 
						SET 
							NewRecordMin = NULL,
							NewRecordSec = NULL,
							NewRecordMs = NULL, 
							NewRecordHolderUserID = NULL,
							NewRecordHouseID = NULL,
							NewRecordHolderName = NULL
						WHERE 
							 EventGroupID = $eventGroupID ";
			$this->db_db_query($sql);

		}

		//added by Marcus 25/8/2009
		function returnEnrolmentRecordByClassID($ClassID)
		{
			$ClassIDAry=is_array($ClassID)?$ClassID:Array($ClassID);
			$ClassIDStr=count($ClassIDAry)>1?implode(",",$ClassIDAry):$ClassIDAry[0];
			
			$sql = "	SELECT 
							d.ClassTitleEN,
							c.ClassNumber,
							b.EventCode
						FROM 
							SPORTS_STUDENT_ENROL_EVENT a 
							LEFT JOIN SPORTS_EVENTGROUP b ON a.EventGroupID = b.EventGroupID
							LEFT JOIN YEAR_CLASS_USER c ON a.StudentID = c.UserID
							LEFT JOIN YEAR_CLASS d ON c.YearClassID = d.YearClassID
						WHERE 
							d.YearClassID IN ($ClassIDStr)
						ORDER BY
							c.YearClassID , a.StudentID
							
			";

			$temp=$this->returnArray($sql,3);
			
			return $temp;
			
		}

		function returnEnrolmentRecordByYearID($YearID,$AcademicYearID='')
		{
			if ($AcademicYearID=='')
			{
	       		$AcademicYearID = Get_Current_Academic_Year_ID();
				$YearFilter = " AND d.AcademicYearID = '$AcademicYearID' ";
			}
			
			$YearIDAry=is_array($YearID)?$YearID:Array($YearID);
			$YearIDStr=count($YearIDAry)>1?implode(",",$YearIDAry):$YearIDAry[0];
			
			// 2012-11-21 Ivan: Changed from "LEFT JOIN" to "INNER JOIN" for table SPORTS_EVENTGROUP to hide the record if the event has been deleted
			$sql = "	SELECT 
							d.ClassTitleEN,
							c.ClassNumber,
							b.EventCode
						FROM 
							SPORTS_STUDENT_ENROL_EVENT a 
							INNER JOIN SPORTS_EVENTGROUP b ON a.EventGroupID = b.EventGroupID
							LEFT JOIN YEAR_CLASS_USER c ON a.StudentID = c.UserID
							LEFT JOIN YEAR_CLASS d ON c.YearClassID = d.YearClassID
							LEFT JOIN YEAR e ON d.YearID = e.YearID
						WHERE 
							d.YearID IN ($YearIDStr)
							$YearFilter
						ORDER BY
							c.YearClassID , a.StudentID
							
			";

			$temp=$this->returnArray($sql,3);

			return $temp;
			
		}
		
		function returnStudentIDByClassNameAndClassNumber($ClassName,$ClassNumber)
		{
			$CurrentAcademicYear = Get_Current_Academic_Year_ID();
			$sql = "	SELECT 
							a.UserID 
						FROM 
							YEAR_CLASS_USER a 
							INNER JOIN YEAR_CLASS b ON a.YearClassID = b.YearClassID
						WHERE
							b.ClassTitleEN = '$ClassName' 
							AND a.ClassNumber = '$ClassNumber'
							AND b.AcademicYearID = '$CurrentAcademicYear'
						";
			$temp = $this->returnVector($sql);
			return $temp[0];
		}
		
		function returnEventGroupInfoByEventCode($EventCode)
		{
			####################
			#	Case GroupID
			#	-1 : Boys Open
			#	-2 : Girls Open
			#	-4 : Mixed Open
			##################### 
			
			$sql = "SELECT
					EventID , 
					CASE GroupID 
					WHEN '-1' THEN 'M'
					WHEN '-2' THEN 'F'
					WHEN '-4' THEN 'B'
					ELSE GroupID
					END AS GroupID,
					EventGroupID,
					CountPersonalQuota
				FROM
					SPORTS_EVENTGROUP 
				WHERE
					EventCode = '$EventCode'";

			$temp=$this-> returnArray($sql);
			
			return $temp[0];
		}
		
		function returnStudentAgeGroup($StudentID, $targetGroupID=0)
		{
			
			$sql = "SELECT DateOfBirth , Gender FROM INTRANET_USER WHERE UserID = '$StudentID'";
			$temp=$this->returnArray($sql);
			
			list($DOB,$Gender)=$temp[0];
			$sql = "
					SELECT 
						AgeGroupID,			
						EnglishName,
						ChineseName,
						Gender,
						GroupCode,
						EnrolMaxTotal,
						EnrolMaxTrack,
						EnrolMaxField
					FROM
						SPORTS_AGE_GROUP
					WHERE
						(DOBLowLimit >= '$DOB' OR DOBLowLimit IS NULL)
						AND (DOBUpLimit <= '$DOB' OR DOBUpLimit IS NULL)
						AND Gender = '$Gender'
			";
			// [2016-0627-1013-19066]
			if($targetGroupID > 0)
				$sql .= " AND AgeGroupID = '$targetGroupID' ";
			$temp=$this->returnArray($sql);
			
			return $temp[0];
		}
		
		function retrieveStudentInfo($EnglishName="", $ChineseName="", $ClassName = "", $AgeGroupID="", $HouseID="", $OrderBy = "")
        {
                global $intranet_session_language;
                
                $CurrentAcademicYear = Get_Current_Academic_Year_ID();
                
                $conds1 = ($EnglishName=="")?"":" AND a.EnglishName LIKE '%$EnglishName%'";
                $conds2 = ($ChineseName=="")?"":" AND a.ChineseName LIKE '%$ChineseName%'";
                //$conds3 = ($ClassName=="")?"":" AND a.ClassName = '$ClassName'";

                if($ClassName != "")
                {
	                 $class_join = "LEFT JOIN YEAR_CLASS as d ON d.ClassTitleEN = a.ClassName";
                     $conds3 = "AND d.ClassTitleEN = '$ClassName' and d.AcademicYearID=$CurrentAcademicYear";
                }
                else
                {
// 	                 $class_join = "";
//                      $conds3 = "";

					$class_join = "LEFT JOIN YEAR_CLASS as d ON d.ClassTitleEN = a.ClassName";
					$conds3 = "AND d.AcademicYearID=$CurrentAcademicYear";
                }
                
                if($AgeGroupID != "")
                {
                        $sql = "SELECT Gender, DOBLowLimit, DOBUpLimit FROM SPORTS_AGE_GROUP WHERE AgeGroupID = '$AgeGroupID'";
                        $ageGrroupInfo = $this->returnArray($sql,3);

                        $t_gender = $ageGrroupInfo[0][0];
                        $t_low = $ageGrroupInfo[0][1];
                        $t_up = $ageGrroupInfo[0][2];

                        if($t_up == "")
                        {
                                $conds4 = "AND a.Gender = '$t_gender' AND a.DateOfBirth <= '$t_low' AND a.DateOfBirth <> '0000-00-00 00:00:00'";
                        }
                        else if($t_low == "")
                        {
                                $conds4 = "AND a.Gender = '$t_gender' AND a.DateOfBirth >= '$t_up' AND a.DateOfBirth  <> '0000-00-00 00:00:00'";
                        }
                        else
                                $conds4 = "AND a.Gender = '$t_gender' AND a.DateOfBirth <= '$t_low' AND a.DateOfBirth >= '$t_up' AND a.DateOfBirth <> '0000-00-00 00:00:00'";
                }
                else
                        $conds4 = "";

                if($HouseID != "")
                {
                        $sql = "SELECT GroupID FROM INTRANET_HOUSE WHERE HouseID = '$HouseID'";
                        $houseInfo = $this->returnArray($sql,1);
                        $houseGroupID = $houseInfo[0][0];

                        $house_join = "LEFT OUTER JOIN INTRANET_USERGROUP as c ON c.UserID = a.UserID";
                        $conds5 = "AND c.GroupID = '$houseGroupID'";
                }
                else
                {
                        $house_join = "";
                        $conds5 = "";
                }

				$engName = getNameFieldByLang("a.", "en");
				$chiName = getNameFieldByLang("a.", "b5");

				if($OrderBy == '')
					$OrderBySql = " ORDER BY a.UserID";
				else
					$OrderBySql = " ORDER BY $OrderBy ";
				
                # Retrieve Filtered Student Info
                $sql = "SELECT a.UserID, $engName, $chiName, d.ClassTitleEN, d.ClassTitleB5, a.ClassNumber, a.Gender
                                        FROM INTRANET_USER as a 
                                        $house_join 
                                        $class_join
                                        WHERE  
                                        a.RecordType = '2' AND a.RecordStatus IN (0,1,2)
                                        $conds1 $conds2 $conds3 $conds4 $conds5 $OrderBySql";
                $students = $this->returnArray($sql,7);
				return $students;
         }
         
         function retrieveStudentEnroledEvent($StudentID, $excludeOpenEvent=0, $presentOnly=0, $targetGroupID=0)
         {
         	if($excludeOpenEvent == 1)
         		$cond = " AND b.GroupID > 0 ";
         	
         	if($presentOnly == 1)
         	{
         		$cond .= " AND sla.RecordStatus > 1 ";
         		$GroupBy = " GROUP BY a.StudentID, a.EventGroupID ";
         		$JOINSLA = " INNER JOIN SPORTS_LANE_ARRANGEMENT sla ON sla.EventGroupID = b.EventGroupID AND sla.StudentID =a.StudentID  ";
         	}
         	
         	// [2016-0627-1013-19066]
         	if($targetGroupID > 0)
         		$cond .= " AND b.GroupID = '$targetGroupID' ";
         	
	        $sql = "SELECT 
						c.EnglishName, c.ChineseName 
					FROM 
						SPORTS_STUDENT_ENROL_EVENT as a
						INNER JOIN SPORTS_EVENTGROUP as b on (b.EventGroupID = a.EventGroupID)
						INNER JOIN SPORTS_EVENT as c on (c.EventID = b.EventID)
						$JOINSLA
					WHERE 
						a.StudentID = $StudentID
						$cond
					$GroupBy ";
    		$result = $this->returnArray($sql);
    		return $result;
         }
         
         function retrieveStudentParticipateEventList($StudentID, $excludeOpenEvent=0)
         {
         	if($excludeOpenEvent == 1)
         		$cond = " AND b.GroupID > 0 ";
         	
	         $sql = "
					SELECT 
						c.EnglishName, c.ChineseName 
					FROM 
						SPORTS_STUDENT_ENROL_EVENT as a
						left join SPORTS_EVENTGROUP as b on (b.EventGroupID = a.EventGroupID)
						left join SPORTS_EVENT as c on (c.EventID = b.EventID)
					WHERE 
						a.StudentID = $StudentID
						$cond
					";
    		$result = $this->returnArray($sql);
    		return $result;
         }
         
		function Get_Max_Heat_Of_Round($eventGroupID,$roundType)
		{
			$sql = "
				SELECT 
					MAX(Heat) 
				FROM 
					SPORTS_LANE_ARRANGEMENT 
				WHERE 
					EventGroupID = '$eventGroupID' 
					AND RoundType = '$roundType'
			";
			
			return $this->returnVector($sql);
         }
         
         function Get_Attend_And_Status($DBStatus)
         {
         	if($DBStatus== RESULT_STATUS_ABSENT)		# 1 - Absent
			{
//				$status=$roundType!=1?5:3;
				$status=3;
				$attend = 2;
			}
			else if($DBStatus== RESULT_STATUS_PRESENT)		# 2 - Present
			{
				$status = 3;
				$attend = 1;
			}
			else if($DBStatus== RESULT_STATUS_QUALIFY)		#3 - Present/Standard
			{
				$status = 2;
				$attend = 1;
			}
			else if($DBStatus== RESULT_STATUS_RECORDBROKEN)		#4 - Present/Record Broken
			{
				$status = 1;
				$attend = 1;
			}
			else if($DBStatus== RESULT_STATUS_FOUL)		#5 - Foul
			{
				$status = 4;
				$attend = 1;
			}
			else if($DBStatus == RESULT_STATUS_WAIVEDABSENT)
			{
				$status = 3;
				$attend = 3;
			}
			
			return array($status,$attend);
         }

         function Get_Group_Event_By_EventGroupID($EventGroupID)
         {
         	$sql = "
				SELECT 
					GroupID, 
					EventID 
				FROM 
					SPORTS_EVENTGROUP 
				WHERE 
					EventGroupID = '$EventGroupID'
			";

			$tempGroupID = $this->returnArray($sql);
			
			return $tempGroupID;
         }
         
         function Get_Event_Info($eventID)
         {
     		$db_field = Get_Lang_Selection("EnglishName","ChineseName");

			$sql = "
				SELECT 
					EventType, 
					$db_field 
				FROM 
					SPORTS_EVENT 
				WHERE 
					EventID = '$eventID'
			";
			
			$result = $this->returnArray($sql, 2);
		
        	return $result;	
         }
         
         function Get_Open_Event_Name($OpenEventType)
         {
         	
         	global $i_Sports_Event_Boys_Open,$i_Sports_Event_Girls_Open,$i_Sports_Event_Mixed_Open;
         	
         	if($OpenEventType == EVENT_BOYS_OPEN)
				$ageGroupName = $i_Sports_Event_Boys_Open;
			else if($OpenEventType == EVENT_GIRLS_OPEN)
				$ageGroupName = $i_Sports_Event_Girls_Open;
			else if($OpenEventType == EVENT_MIXED_OPEN)
				$ageGroupName = $i_Sports_Event_Mixed_Open;
				
			return $ageGroupName;
         }
         
         
         function Get_Track_Result($EventGroupID='',$StudentID='',$RoundType='')
         {
         	if(trim($EventGroupID)!=='')
         		$cond_EventGroupID = " AND EventGroupID = $EventGroupID ";
         	if(trim($StudentID)!=='')
         		$cond_StudentID = " AND StudentID = $StudentID ";
         	if(trim($RoundType)!=='')
         		$cond_RoundType = " AND RoundType = $RoundType ";
         	
         	$ResultSql = $this->Get_Pad_Track_Result_Sql("");	
         	$sql = "
				SELECT 
					$ResultSql, 
					RecordStatus,
					AbsentReason,
					Score,
					Rank,
					EventGroupID,					
					StudentID,
					RoundType
				FROM 
					SPORTS_LANE_ARRANGEMENT 
				WHERE 
					1 
					$cond_EventGroupID
					$cond_StudentID
					$cond_RoundType
				";
				
			$result =$this->returnArray($sql);
			
			return $result;	
         }
         
         function Get_Record_Status_Selection($ID_Name,$Status='',$OnChange='')
         {
         	global $i_Sports_Unsuitable, $i_Sports_RecordBroken, $i_Sports_Qualified, $i_Sports_Foul;
         	
         	if(trim($OnChange)!=='')
         		$OnChange = " onchange='$OnChange' ";  
         	
         	$optionArr = array();
         	$optionArr[] = array(0,'--');
         	$optionArr[] = array(3,$i_Sports_Unsuitable);
         	$optionArr[] = array(1,$i_Sports_RecordBroken);
         	$optionArr[] = array(2,$i_Sports_Qualified);
         	$optionArr[] = array(4,$i_Sports_Foul);
         	$Selection = getSelectByArray($optionArr," name='$ID_Name' id='$ID_Name' $OnChange ",$Status,0,1);

			return $Selection;       	
         }
         
         function Get_Present_Absent_Selection($heat, $line , $attend='', $reason='', $OnChange='')
         {
         	global $Lang,$i_Sports_Present,$i_Sports_Absent, $linterface;
         	         	
         	$OnChange = " onchange='AttendUpdate(\"$heat\",\"$line\",this); $OnChange' ";  
         		
			$optionArr = array();
			$optionArr[] = array(1,$i_Sports_Present);
			$optionArr[] = array(2,$i_Sports_Absent." - ".$Lang['eSports']['DeductScore']);
			$optionArr[] = array(3,$i_Sports_Absent." - ".$Lang['eSports']['Waive']);
			
			$ID_Name = 'attend_'.$heat.'_'.$line;
			$Selection = '<div class="table_filter">'.getSelectByArray($optionArr," name='$ID_Name' id='$ID_Name' $OnChange ", $attend,0,1).'</div>';
			
			$display = $attend==3?'style="display:block"':'style="display:none"';
			$Input = '<input type="text" size="30" name="'.$ID_Name.'_reason" id="'.$ID_Name.'_reason" value="'.$reason.'" '.$display.' class="attend_reason">'."\n";
			
			$html = $Selection.$Input;	
					
			return $html; 	
         }
         
         function Get_Obtained_Bonus_Score($RoundType,$EventGroupID,$StudentID)
         {
         	if($RoundType == ROUND_TYPE_FIRSTROUND)
         		return 0;
         	
         	switch($RoundType)
         	{
         		case ROUND_TYPE_SECONDROUND:
         			$PreviousRoundTypeSql = " RoundType IN (".ROUND_TYPE_FIRSTROUND.") ";
         		break;
         		case ROUND_TYPE_FINALROUND:
         			$PreviousRoundTypeSql = " RoundType IN (".ROUND_TYPE_FIRSTROUND.",".ROUND_TYPE_SECONDROUND.") ";
         		break;
         	}
         	
         	$sql = "
				SELECT 
					SUM(Score)
				FROM
					SPORTS_LANE_ARRANGEMENT
				WHERE
					$PreviousRoundTypeSql
					AND EventGroupID = '$EventGroupID'
					AND StudentID = '$StudentID'
				GROUP BY
					EventGroupID
			";
			
			$result = $this->returnVector($sql);
			
			return $result[0];
         }
         
          function Get_Exist_Round_Of_EventGroup($EventGroupID)
         {
         	$sql = "
				SELECT 
					RoundType,
					CASE RoundType
						WHEN 1 THEN 1
						WHEN 2 THEN 2
						WHEN 0 THEN 3
					END AS DisplayOrder
				FROM
					SPORTS_LANE_ARRANGEMENT
				WHERE
					EventGroupID = '$EventGroupID'
				GROUP BY
					RoundType
				ORDER BY
					DisplayOrder

			";
			
			$result = $this->returnVector($sql);
			
			return $result;
         }

         function Get_Event_Special_Lane_Arrangement($eventID=0)
         {
         	$sql = "
				SELECT 
					SpecialLaneArrangement 
				FROM 
					SPORTS_EVENT 
				WHERE 
					EventID='$eventID'
			";
			
			$result = $this->returnVector($sql);
			
			if(empty($result[0]))
         		$RoundLaneArrange = $this->defaultLaneArrangement;
			else
				$RoundLaneArrange = $result[0];
			
			$RoundLaneArrange = explode(DELIMITER,$RoundLaneArrange);
			
			foreach($RoundLaneArrange as $key => $LaneArrange)
			{
				$RoundLaneArrange[$key] = explode(",",$LaneArrange);
			}
			
			return $RoundLaneArrange;
         }
         
         function Get_Event_Selection($id_name, $selected='', $onchange='', $all=0, $noFirst=0, $FirstTitle="" )
         {
	     	$track_events = $this->retrieveTrackEventIDAndName();
	     	$optionArr = build_assoc_array($track_events); 
	     	
	     	$tags = " id='$id_name' name='$id_name' ";
	     	if($onchange)
	     		$tags .=  " onchange='$onchange' ";
	     	
	     	$selection = getSelectByAssoArray($optionArr,$tags,$selected,$all,$noFirst,$FirstTitle);
	     	
	     	return $selection;
         }
         
         function Get_Lane_Arrangement_Setting_Table($SpecialArrangement='', $Name='',  $NoOfHeat='')
         {
         	global $i_Sports_field_Lane,$i_Sports_field_Rank;
         	
         	$lanes = $this->numberOfLanes;
         	if(!$NoOfHeat)
         	{
         		$total_rank = count($SpecialArrangement);
         		$NoOfHeat = $total_rank/$lanes;
         	}
         	else
         		$total_rank = $NoOfHeat*$lanes;
         	
         	for($i=1; $i<=$total_rank; $i++)
         		$optionArr[] = array($i,$i); 
         	
         	# Gen Lane Number
	     	for($j=0; $j<$lanes; $j++)
	     		$LaneRow .= '<th>'.$this->Get_Lang_Line($j+1).'</td>'."\n";
	         
	        # loop heat 
	        $pos = 0;	
         	for($i=0; $i<$NoOfHeat; $i++)
         	{
         		$HeatNo = $i+1;
         		$ArrangeRow = "";
         		# loop arrange selection 
         		
	         	for($j=0; $j<$lanes; $j++)
	         	{
	         		$laneNo = $j+1;
	         		$id = $Name."_".$HeatNo."_".$laneNo; // e.g. fr_1_2
	         		$name = $Name."[".$pos."]"; // e.g. fr[1][2]
	         		$class = $Name."Selection"; // e.g. frSelection
	         		$selected = $SpecialArrangement[$pos]?$SpecialArrangement[$pos]:($pos+1);
	         		$selection = getSelectByArray($optionArr," id='$id' name='$name' class='$class' ",$selected,0,1);
	         		$ArrangeRow .= '<td>'.$selection.'</td>'."\n";
	         		$pos++;
	         	}
	         	
	         	$HeatRows .= '<tr>'."\n";
	         		$HeatRows .= '<td>'.$this->Get_Lang_Heat($HeatNo).'</td>'."\n";
	         		$HeatRows .= $ArrangeRow;	
	         	$HeatRows .= '</tr>'."\n";
         	}
         	
         	$table .= '<table class="common_table_list_v30 edit_table_list_v30">'."\n";
         		$table .= '<thead>'."\n";
		         	$table .= '<tr class="tabletop">'."\n";
		         		$table .= '<th>'.$i_Sports_field_Lane.'</td>'."\n";
		         		$table .= $LaneRow;	
		         	$table .= '</tr>'."\n";
	         	$table .= '</thead>'."\n";
	         	$table .= $HeatRows;
         	$table .= '</table>'."\n";
         	
         	return $table;
         }
         
         function Get_Lang_Heat($HeatNo)
         {
         	global $i_Sports_The, $i_Sports_Group;
         	
         	$TitleCh = $i_Sports_The.$HeatNo.$i_Sports_Group;
         	$TitleEn = $i_Sports_Group." ".$HeatNo;
         	
         	return Get_Lang_Selection($TitleCh,$TitleEn);
         }
         
         function Get_Lang_Line($LineNo)
         {
         	global $i_Sports_The, $i_Sports_Line;
         	
         	$TitleCh = $i_Sports_The.$LineNo.$i_Sports_Line;
         	$TitleEn = $i_Sports_Line." ".$LineNo;
         	
         	return Get_Lang_Selection($TitleCh,$TitleEn);
         }
         
         function Get_Enroled_Participant_List($return_method="array")
         {
         	$sql = "
				SELECT 
					iu.UserID
				FROM 
					INTRANET_USER iu
					INNER JOIN SPORTS_STUDENT_ENROL_EVENT ssee ON iu.UserID = ssee.StudentID
				WHERE 
					iu.RecordType = '2' 
					AND iu.RecordStatus IN (0,1,2)
				GROUP BY
					iu.UserID
				order by iu.ClassName, iu.ClassNumber
			";
			
			if($return_method=="array")
				$result = $this->returnArray($sql);
			else
				$result = $this->returnVector($sql);
			
			return $result;
			
         }
         
         function Get_Enroled_Participant_List2($return_method="array")
         {
             # Track and Field events
             $sql = "
				SELECT
					iu.UserID
				FROM
					INTRANET_USER iu
					INNER JOIN SPORTS_STUDENT_ENROL_EVENT ssee ON (ssee.StudentID = iu.UserID)
                    INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = iu.UserID)
                    INNER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."')
                    INNER JOIN YEAR y ON (y.YearID = yc.YearID)
				WHERE
					iu.RecordType = '2' AND 
                    iu.RecordStatus IN (0,1,2)
				GROUP BY
					iu.UserID
			";
            $tf_event_user_id = $this->returnVector($sql);
            
            # Class Relay
            $sql = "
                SELECT
                    iu.UserID
                FROM
					INTRANET_USER iu
                    INNER JOIN SPORTS_CLASS_RELAY_LANE_ARRANGEMENT_MAPPING crl_stu ON (crl_stu.UserID = iu.UserID)
                    INNER JOIN SPORTS_CLASS_RELAY_CLASS_GROUP crl_g ON (crl_g.ClassRelayGroupID = crl_stu.ClassRelayGroupID)
                    INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = iu.UserID)
                    INNER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."')
                    INNER JOIN YEAR y ON (y.YearID = yc.YearID)
                WHERE
					iu.RecordType = '2' AND
					iu.RecordStatus IN (0,1,2)
				GROUP BY
					iu.UserID
            ";
            $group_relay_user_id = $this->returnVector($sql);
            
            $target_user_id = array_merge((array)$tf_event_user_id, (array)$group_relay_user_id);
            $target_user_id = array_values(array_filter(array_unique($target_user_id)));
            $target_user_id_cond = " AND iu.UserID IN ('".implode("', '", (array)$target_user_id)."') ";
            
            # Participant List
            $sql = "
				SELECT
					iu.UserID
				FROM
					INTRANET_USER iu
                    INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = iu.UserID)
                    INNER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."')
                    INNER JOIN YEAR y ON (y.YearID = yc.YearID)
				WHERE
					iu.RecordType = '2' AND
					iu.RecordStatus IN (0,1,2)
                    $target_user_id_cond
				GROUP BY
					iu.UserID
				ORDER BY
                    y.WEBSAMSCode DESC, iu.ClassName, iu.ClassNumber+0
			";
            
            if($return_method=="array") {
                $result = $this->returnArray($sql);
            }
            else {
                $result = $this->returnVector($sql);
            }
            return $result;
         }
         
         function Get_Admin_Helper_List()
         {
         	$sql = "
				SELECT 
					AdminUserID 
				FROM 
					SPORTS_ADMIN_USER_ACL as a
			";
			
			$result = $this->returnVector($sql);
			
			return $result;
         }
         
         function Get_EventGroup_Info($EventID='', $EventGroupID='', $ParOrderBy='')
         {
         	if(trim($EventID)!='')
         		$cond_EventID = " AND EventID = '$EventID' ";
         	if(trim($EventGroupID)!='')
         		$cond_EventGroupID = " AND EventGroupID = '$EventGroupID' ";
         	if(trim($ParOrderBy)!='')
         		$OrderBy = " ORDER BY $ParOrderBy ";
         		
         	$sql = "
				SELECT
					*
				FROM
					SPORTS_EVENTGROUP
				WHERE
					1
					$cond_EventID
					$cond_EventGroupID
				$OrderBy
			";	
         		
         	$Result = $this->returnArray($sql);
         
         	return $Result;
         }

         function Get_Event_UnAssigned_Student($EventGroupID='')
         {
         	if(!empty($EventGroupID))
         		$cond_EventGroupID = " AND a.EventGroupID = '$EventGroupID' ";
         	
         	$sql = "
				SELECT 
					a.EventGroupID, 
					a.StudentID 
				FROM 
					SPORTS_STUDENT_ENROL_EVENT a 
					LEFT JOIN SPORTS_LANE_ARRANGEMENT b ON a.EventGroupID = b.EventGroupID AND a.StudentID = b.StudentID 
				WHERE 
					b.EventGroupID IS NULL
					$cond_EventGroupID
			";
			
			$result = $this->returnArray($sql);
			
			return $result;
         }
         
         //20101125
         function Get_Student_Basic_Info($StudentArr, $groupByStudent=false)
         {
         	if(empty($StudentArr))
         		return false;
         	else if(!is_array($StudentArr))
         		$StudentArr = (array)$StudentArr;
         	$StudentIDList = implode(',',$StudentArr);
         	$CurrentAcademicYear = Get_Current_Academic_Year_ID();
         	
         	$GroupSql = "";
			if($groupByStudent)
				$GroupSql = " GROUP BY iu.UserID";
         	
 			$name_field = getNameFieldWithClassNumberByLang ("iu.");
         	$sql = "
				SELECT
					iu.UserID,
					$name_field StudentName,
					iu.EnglishName,
					iu.ChineseName,
					iu.ClassName,
					iu.ClassNumber,
					iu.Gender,
					iu.DateOfBirth,
					ih.HouseID,
					sag.AgeGroupID,
					ssei.AthleticNum
				FROM 
					INTRANET_GROUP ig
					LEFT JOIN INTRANET_USERGROUP iug ON iug.GroupID = ig.GroupID AND ig.RecordType = 4 AND ig.AcademicYearID = '$CurrentAcademicYear' 
					RIGHT JOIN INTRANET_USER iu ON iu.UserID = iug.UserID
					LEFT JOIN INTRANET_HOUSE ih ON ih.GroupID = ig.GroupID
					LEFT JOIN SPORTS_AGE_GROUP sag ON
						(sag.DOBLowLimit IS NULL || iu.DateOfBirth <= sag.DOBLowLimit) AND 
						(sag.DOBUpLimit IS NULL || iu.DateOfBirth >= sag.DOBUpLimit) AND 
						iu.Gender = sag.Gender
					LEFT JOIN SPORTS_STUDENT_ENROL_INFO ssei ON ssei.StudentID = iu.UserID
					left JOIN YEAR_CLASS_USER ycu On ycu.UserID = iu.UserID
					left join YEAR_CLASS yc on yc.YearClassID = ycu.YearClassID 
					left join YEAR y on y.YearID=yc.YearID 
				WHERE  
					iu.UserID IN ($StudentIDList)
					and yc.AcademicYearID = ".$CurrentAcademicYear."
				$GroupSql
				ORDER BY
					y.Sequence,
					yc.Sequence,
					iu.ClassNumber
			";
			/*
			iu.ClassName,
					iu.ClassNumber
					*/
			
			$result = $this->returnArray($sql);
			return $result;
         }
         
         function Assign_Student_To_EventGroup($StudentID, $EventGroupID, $Heat, $ArrangeOrder, $RoundType='', $skipLaneChecking=false)
         {
         	$RoundType = trim($RoundType)==''?ROUND_TYPE_FIRSTROUND:$RoundType;
         	
         	$sql = " 
				SELECT 
					StudentID 
				FROM 
					SPORTS_LANE_ARRANGEMENT 
				WHERE 
					EventGroupID = $EventGroupID
					AND RoundType = $RoundType
					AND Heat = $Heat
					AND ArrangeOrder = $ArrangeOrder
			";
			$tmp = $this->returnVector($sql);
         	if(count($tmp)>0)
         		return false;
         	
         	// [2016-0301-1022-11066] auto assign line order if $skipLaneChecking is true
         	if($skipLaneChecking && empty($ArrangeOrder)){
				$sql = "SELECT 
							MAX(ArrangeOrder)+1 
						FROM 
							SPORTS_LANE_ARRANGEMENT 
						WHERE 
							EventGroupID = $EventGroupID
							AND RoundType = $RoundType
							AND Heat = $Heat
				";
				$orderResult = $this->returnVector($sql);
				
				// set to line 1 if return empty order
		     	if(!isset($orderResult[0]))
		     		$ArrangeOrder = 1;
		     	else
		     		$ArrangeOrder = $orderResult[0];
        	}
        	
         	# Insert lanes arrangement to the SPORT_LANE_ARRANGEMENT table
         	$fields = "(EventGroupID, RoundType, StudentID, Heat, ArrangeOrder, DateModified)";
         	$values = "($EventGroupID, $RoundType, $StudentID, $Heat, $ArrangeOrder, NOW())";
			$sql = "
				INSERT INTO 
					SPORTS_LANE_ARRANGEMENT 
						$fields 
					VALUES 
						$values
			";
			
			$Success = $this->db_db_query($sql);
			return $Success;
         }
         
         function Get_EventGroup_Enroled_Arranged_Number($EventGroupID='')
         {
         	if(trim($EventGroupID)!='')
         		$cond_EventGroupID = " AND a.EventGroupID = $EventGroupID ";

         	$sql = "
	         	SELECT 
					COUNT( DISTINCT a.StudentID),COUNT(DISTINCT b.StudentID), a.EventGroupID
				FROM 
					SPORTS_STUDENT_ENROL_EVENT a 
					LEFT JOIN SPORTS_LANE_ARRANGEMENT b ON a.EventGroupID = b.EventGroupID AND a.StudentID = b.StudentID 
				WHERE
					1			
					$cond_EventGroupID
				GROUP BY 
					a.EventGroupID
				
			";
			
			$result = $this->returnArray($sql);
			
			return $result;
         }
         
         function Get_All_Event_Round_Result($EventGroupID='')
         {
         	$TrackResultMS = $this->Get_Track_Result_To_Millisecond_Sql();
         	$TrackResultDisplay = $this->Get_Track_Result_Display_Sql();
         	
         	if(!empty($EventGroupID))
         		$cond_EventGroupID = " AND EventGroupID = '$EventGroupID' ";
         		
         	$sql = "
				SELECT 
					IFNULL(ResultMetre,$TrackResultDisplay) EventResult,
					EventGroupID,
					RoundType,
					StudentID,
					$TrackResultDisplay,
					ResultMetre
				FROM
					SPORTS_LANE_ARRANGEMENT
				WHERE 
					$TrackResultMS > 0
					OR ResultMetre > 0
					$cond_EventGroupID
			";
			
			$result = $this->returnArray($sql);
			
			return $result;
         }
         
         function Get_EventGroup_Involved_Round($EventGroupID='')
         {
         	if(!empty($EventGroupID))
         		$cond_EventGroupID = " AND EventGroupID = '$EventGroupID' ";
         		
         	$sql = "
				SELECT 
					seg.EventGroupID,
					segt.SecondRoundReq,
					IFNULL(segt.FinalRoundReq,segf.FinalRoundReq) FinalRoundReq
				FROM
					SPORTS_EVENT se
					INNER JOIN SPORTS_EVENTGROUP seg ON se.EventID = seg.EventID
					LEFT JOIN SPORTS_EVENTGROUP_EXT_TRACK segt ON seg.EventGroupID = segt.EventGroupID
					LEFT JOIN SPORTS_EVENTGROUP_EXT_FIELD segf ON seg.EventGroupID = segf.EventGroupID
				WHERE
					1
					$cond_EventGroupID
					
			";	
			
			$result = $this->returnArray($sql);
			if(count($result)==0)
				return array();
			
			foreach($result as $rec)
			{
				list($thisEventGroupID,$thisSecondRoundReq,$thisFinalRoundReq) = $rec;
				$ReturnArr[$thisEventGroupID][] = ROUND_TYPE_FIRSTROUND;
				if($thisSecondRoundReq)
					$ReturnArr[$thisEventGroupID][] = ROUND_TYPE_SECONDROUND;  
				if($thisFinalRoundReq)	
					$ReturnArr[$thisEventGroupID][] = ROUND_TYPE_FINALROUND;
			}	
			
			return $ReturnArr;
			
         }
         
         function Get_Student_EventGroup_Attendance($StudentID='',$EventGroupID='')
         {
         	if(!empty($EventGroupID))
         		$cond_EventGroupID = " AND ssee.EventGroupID IN (".implode(",",(array)$EventGroupID).") ";
         		
     		if(!empty($StudentID))
         		$cond_StudentID = " AND ssee.StudentID IN (".implode(",",(array)$StudentID).") ";
         	
         	$sql = "
				SELECT 
					ssee.StudentID, 
					ssee.EventGroupID,
					CASE 
						WHEN RecordStatus IN (2,3,4,5) THEN 1
						WHEN RecordStatus IN (1,6) THEN 2
						ELSE 0
					END AS Attend
				FROM
					SPORTS_STUDENT_ENROL_EVENT ssee
					LEFT JOIN SPORTS_LANE_ARRANGEMENT sla ON ssee.EventGroupID = sla.EventGroupID AND ssee.StudentID = sla.StudentID
				WHERE
					(RoundType = 1  OR RoundType IS NULL)
					$cond_EventGroupID
					$cond_StudentID

			";
			
			$result = $this->returnArray($sql);
			$ReturnArr = BuildMultiKeyAssoc($result,array("StudentID","EventGroupID"),"Attend",1);
			
			return $ReturnArr;
         }
         
         ##############################################################################################
         # formating function 
          function Get_Track_Result_Display_Sql($table='', $Result='')
         {
         	if($Result=='')
         		$Result = "Result";
         		         	
        	$sql = "IF({$table}{$Result}Min > 0 OR {$table}{$Result}Sec > 0 OR {$table}{$Result}MS > 0, CONCAT(LPAD({$table}{$Result}Min,2,'0'),'\'', LPAD({$table}{$Result}Sec,2,'0'),'\'\'', LPAD({$table}{$Result}MS,2,'0')),'-')";
			return $sql;
         }
         
         function Get_Pad_Track_Result_Sql($table='', $Result='')
         {
         	if($Result=='')
         		$Result = "Result";
         		
        	$sql = "LPAD({$table}{$Result}Min,2,'0') {$Result}Min, LPAD({$table}{$Result}Sec,2,'0') {$Result}Sec, LPAD({$table}{$Result}MS,2,'0') {$Result}MS";
			return $sql;
         }
         
         function Get_Track_Result_To_Millisecond_Sql($table='', $Result='')
         {
         	if($Result=='')
         		$Result = "Result";
         	
         	$sql = "({$table}{$Result}Min*60*100+{$table}{$Result}Sec*100+{$table}{$Result}MS)";
         	
			return $sql;
         }
         
         # e.g. 6108 => array(1,1,8); 
         function Convert_Millisecond_To_Time($ms)
         {         
         	$min = floor($ms/6000);
         	$ms = $ms%6000;

			if($ms == 0)
			{
				$sec = 0;
				$ms = 0;
			}
			else
			{
				$sec = floor($ms/100);
				$ms = $ms%100;
			}
			
			return array($min,$sec,$ms);
         }
         
         # e.g.  array(1,1,8) => 6108; 
         function Convert_TimeArr_To_Millisecond($TimeArr)
         {         
         	if(count($TimeArr) != 3)
         		return false;
         		
         	list($min,$sec,$ms) = $TimeArr;
			
			$result = $min*60*100+$sec*100+$ms;
			
			return $result;
         }
         
         # e.g. 6108 => 01'01''08
         function Format_Millisecond($ms)
         {
         	$TimeArr = $this->Convert_Millisecond_To_Time($ms);
         	$Time = implode(":", (array)$TimeArr);
         	
         	return $this->Format_TimeStr($Time);
         }
         
         # e.g. 1'1''8/1:1:8 => 01'01''08
         function Format_TimeStr($str)
         {
         	$str =  str_replace("'",":",str_replace("''",":",$str));
         	
         	$TimeArr = explode(":",$str);
         	if(count($TimeArr)!=3)
         		return false;
         		
         	foreach($TimeArr as $val) 
         		$PadTimeArr[] = str_pad($val, 2, 0, STR_PAD_LEFT);
         	
         	return $PadTimeArr[0]."'".$PadTimeArr[1]."''".$PadTimeArr[2];
         }
         
         # e.g. array(1,1,8) => 01'01''08
         function Format_TimerArr($TimeArr)
         {
         	$Time = implode(":", (array)$TimeArr);
         	
         	return $this->Format_TimeStr($Time);
         }
         
         # e.g. array(1,1,8) => array('01','01','08')
         function Pad_TimerArr($TimeArr)
         {
         	foreach($TimeArr as $key => $val) 
         		$PadTimeArr[$key] = str_pad($val, 2, 0, STR_PAD_LEFT);
         		
         	return $PadTimeArr;
         }


         
//         function Get_Lang_Round_Title($HeatNo, $RoundType='')
//         {
//         	$TitleCh = $i_Sports_The." ".$heat." ".$i_Sports_Group;
//         	$RoundTitle = $i_Sports_The." ".$heat." ".$i_Sports_Group ." (".$i_Sports_Second_Round .")";
//         }
         
         ################################ field event functions ####################################
  		function returnBestRecordResultOfFieldEventGroup($eventGroupID)
		{
			$result = "(a.ResultMetre)";
			$oldrecord= "(b.RecordMetre)";
			$orderType = "DESC";

			$sql = 	"	SELECT 
							a.StudentID, $result
						FROM 
							SPORTS_LANE_ARRANGEMENT a 
							LEFT JOIN SPORTS_EVENTGROUP_EXT_FIELD b ON a.eventGroupID = b.eventGroupID
						WHERE a.EventGroupID=$eventGroupID
							AND $result <> 0
							AND $result > $oldrecord 
						ORDER BY $result $orderType, dateModified DESC
						LIMIT 0,1
						";
			$temp = $this->returnArray($sql,2);
			return $temp;
		}
		
		function removeNewRecordOfFieldEventGroup($eventGroupID)
		{
			$sql = "	UPDATE 
							SPORTS_EVENTGROUP_EXT_FIELD
						SET 
							NewRecordMetre = NULL, 
							NewRecordHolderUserID = NULL,
							NewRecordHouseID = NULL,
							NewRecordHolderName = NULL
						WHERE 
							 EventGroupID = $eventGroupID ";
			$this->db_db_query($sql);

		}
		
        function Is_High_Jump($EventGroupID)
        {
         	$sql = "SELECT 
						se.IsJump 
					FROM
						SPORTS_EVENT se
						INNER JOIN SPORTS_EVENTGROUP seg ON  se.EventID = seg.EventID
					WHERE 
						seg.EventGroupID = '$EventGroupID' ";
			$result = $this->returnVector($sql);
			
			return $result[0]==1?1:0;
		}
		
        function Is_All_Trial_Included($EventGroupID)
        {
         	$sql = "SELECT 
						se.IsAllTrialIncluded 
					FROM
						SPORTS_EVENT se
						INNER JOIN SPORTS_EVENTGROUP seg ON  se.EventID = seg.EventID
					WHERE 
						seg.EventGroupID='$EventGroupID' ";
			$result = $this->returnVector($sql);
			
			return $result[0]==1?1:0;
        }
         
         function Get_Field_Result($EventGroupID='',$StudentID='',$RoundType='')
         {
         	if(trim($EventGroupID)!=='')
         		$cond_EventGroupID = " AND EventGroupID = $EventGroupID ";
         	if(trim($StudentID)!=='')
         		$cond_StudentID = " AND StudentID = $StudentID ";
         	if(trim($RoundType)!=='')
         		$cond_RoundType = " AND RoundType = $RoundType ";
         	
         	$sql = "
				SELECT 
					ResultMetre, 
					RecordStatus, 
					Rank,
					AbsentReason,
					ResultTrial1,
					ResultTrial2,
					ResultTrial3,
					Score,
					Rank,
					EventGroupID,					
					StudentID,
					RoundType
				FROM
					SPORTS_LANE_ARRANGEMENT 
				WHERE 
					1
					$cond_EventGroupID
					$cond_StudentID
					$cond_RoundType
			";
		 	$result = $this->returnArray($sql, 3);
		 	
		 	return $result;	
         }
         
         function Get_Tie_Break_Result($EventGroupID,$RoundType,$Record)
         {
         	$sql = "
				SELECT 
					ResultMetre,
					Rank 
				FROM
					SPORTS_LANE_ARRANGEMENT 
				WHERE 
					CAST(ResultMetre as char)='".$Record."' 
					AND EventGroupID=$EventGroupID 
					AND RoundType = '$RoundType' 
					AND Rank IS NOT NULL order by Rank
			";
			
			$tieResult = $this->returnArray($sql, 2);
			
			return $tieResult;
         }
         
         function Get_Lang_Trial($TrialNo)
         {
         	global $i_Sports_The, $Lang;
         	
         	$TitleCh = $i_Sports_The.$TrialNo.$Lang['eSports']['Trial'];
         	$TitleEn = Get_Sequence_Number($TrialNo)." ".$Lang['eSports']['Trial'];
         	
         	return Get_Lang_Selection($TitleCh,$TitleEn);
         }
         
         function returnEventEnroledNo($GroupEventID, $withoutStudentID='')
         {
			$sql = "select count(*) from  SPORTS_STUDENT_ENROL_EVENT where EventGroupID=".$GroupEventID;
			if($withoutStudentID)
			{
				$sql .= " and StudentID!=$withoutStudentID";	
			}
			$result = $this->returnVector($sql);
			return $result[0];
         }
          
         function returnEventQuota($GroupEventID)
         {  
			$sql = "select EventQuota from SPORTS_EVENTGROUP where EventGroupID=".$GroupEventID;
			$result = $this->returnVector($sql);
			return $result[0];
         }
         
         function returnEventIsCompleted($event_id, $group_id, $GroupEventID)
         {
	         $Ranking = $this->retrieveTFRanking($event_id, $group_id);
	         if(sizeof($Ranking))	return 1;
	         
	         $sql = "select count(*) from SPORTS_LANE_ARRANGEMENT where EventGroupID=$GroupEventID and RecordStatus is NULL";
	         $result = $this->returnVector($sql);
			 return $result[0] ? 0 : 1;
         }
         
         // Participation Report [Philips]
         function returnAgeGroupIDArr(){
             $AgeGroupInfoArr = $this->retrieveAgeGroupInfo();
             $AgeIdsArr = array();
             foreach($AgeGroupInfoArr as $agi){
                 $AgeIdsArr[] = $agi['AgeGroupID'];
             }
             return $AgeIdsArr;
         }
         function returnAgeGroupOrderArr(){
             $AgeGroupInfoArr = $this->retrieveAgeGroupInfo();
             //AgeGroupID, GradeChar, EnglishName, ChineseName, Gender, GroupCode,
             //DOBUpLimit, DOBLowLimit, DisplayOrder
             $AgeGroupArr = array();
             $AgeIdArr = array();
             
             $index = 0;
             foreach($AgeGroupInfoArr as $agi) {
                 if($agi['Gender'] == 'M') {
                    $AgeIdArr[$index] = 'CONCAT('.
                        'SUM(IF( h.AgeGroupID = '.
                        $agi['AgeGroupID'].
                        ',f.StudentID IS NOT NULL,0)), '.
                        '"/",'.
                        'SUM(IF( h.AgeGroupID = '.
                        $agi['AgeGroupID'].
                        ',e.StudentID IS NOT NULL, 0))'.
                        ') as AgeGroup'.$index;
                    $AgeGroupArr[$index] = array(
                         "id" => $agi['AgeGroupID'],
                         "name" => $agi[Get_Lang_Selection('ChineseName', 'EnglishName')]
                    );
                    $index++;
                 }
             }
             foreach($AgeGroupInfoArr as $agi) {
                 if($agi['Gender'] == 'F') {
                     $AgeIdArr[$index] = 'CONCAT('.
                         'SUM(IF( h.AgeGroupID = '.
                         $agi['AgeGroupID'].
                         ',f.StudentID IS NOT NULL,0)), '.
                         '"/",'.
                         'SUM(IF( h.AgeGroupID = '.
                         $agi['AgeGroupID'].
                         ',e.StudentID IS NOT NULL, 0))'.
                         ') as AgeGroup'.$index;
                     $AgeGroupArr[$index] = array(
                         "id" => $agi['AgeGroupID'],
                         "name" => $agi[Get_Lang_Selection('ChineseName', 'EnglishName')]
                     );
                     $index++;
                 }
             }
             $AgeIdArr = implode(',', $AgeIdArr);
             //debug_pr($AgeIdArr);die();
             $result = array(
                 "idArr" => $AgeIdArr,
                 "groupArr" => $AgeGroupArr
             );
             return $result;
         }
         
         function returnAgeGroupOrderArr2(){
             $AgeGroupInfoArr = $this->retrieveAgeGroupInfo();
             //AgeGroupID, GradeChar, EnglishName, ChineseName, Gender, GroupCode,
             //DOBUpLimit, DOBLowLimit, DisplayOrder
             $AgeGroupArr = array();
             $AgeIdArr = array();
             
             $index = 0;
             foreach($AgeGroupInfoArr as $agi) {
                 if($agi['Gender'] == 'M') {
                     $AgeIdArr[$index] = 'SUM(IF( h.AgeGroupID = '.
                         $agi['AgeGroupID'].
                         ',f.StudentID IS NOT NULL,0)) as AgeGroup'.$index;
                     $AgeGroupArr[$index] = array(
                         "id" => $agi['AgeGroupID'],
                         "name" => $agi[Get_Lang_Selection('ChineseName', 'EnglishName')]
                     );
                     $index++;
                 }
             }
             foreach($AgeGroupInfoArr as $agi) {
                 if($agi['Gender'] == 'F') {
                     $AgeIdArr[$index] = 'SUM(IF( h.AgeGroupID = '.
                         $agi['AgeGroupID'].
                         ',f.StudentID IS NOT NULL,0)) as AgeGroup'.$index;
                     $AgeGroupArr[$index] = array(
                         "id" => $agi['AgeGroupID'],
                         "name" => $agi[Get_Lang_Selection('ChineseName', 'EnglishName')]
                     );
                     $index++;
                 }
             }
             
             $AgeIdArr = implode(',', $AgeIdArr);
             //debug_pr($AgeIdArr);die();
             $result = array(
                 "idArr" => $AgeIdArr,
                 "groupArr" => $AgeGroupArr
             );
             return $result;
         }
         
         function returnAllClassArrangement($print = false){
             global $Lang;
             
             $CurrentAcademicYear = Get_Current_Academic_Year_ID();
//              if(!$print)
//                 $AgeGroupOrder = $this->returnAgeGroupOrderArr();
//              else
                $AgeGroupOrder = $this->returnAgeGroupOrderArr2();
             $AgeIdArr = $AgeGroupOrder['idArr'];
             $AgeGroupArr = $AgeGroupOrder['groupArr'];
             $AgeIdsArr = $this->returnAgeGroupIDArr();
             $AgeIdsArr = implode("','", $AgeIdsArr);
             $classTitle = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEn');
//              if(!$print)
//                 $tableFields = "a.$classTitle as Title, $AgeIdArr, CONCAT(SUM(IF(h.AgeGroupID IN ('$AgeIdsArr') AND f.studentID IS NOT NULL, 1, 0)),'/', SUM(IF(h.AgeGroupID IN ('$AgeIdsArr') AND e.studentID IS NOT NULL, 1, 0))) as ClassTotal";
//              else
                $tableFields = "a.$classTitle as Title, $AgeIdArr, SUM(IF(h.AgeGroupID IN ('$AgeIdsArr') AND f.studentID IS NOT NULL, 1, 0)) as ClassTotal";
             $tableSql = "YEAR_CLASS as a
                          INNER JOIN YEAR_CLASS_USER as b ON a.YearClassID = b.YearClassID
                          INNER JOIN INTRANET_USER as d ON b.UserID = d.UserID
                          LEFT OUTER JOIN SPORTS_STUDENT_ENROL_EVENT as e ON d.UserID = e.StudentID
                          LEFT OUTER JOIN SPORTS_LANE_ARRANGEMENT as f ON d.UserID = f.StudentID AND e.EventGroupID = f.EventGroupID AND f.RoundType = 1
                          LEFT OUTER JOIN SPORTS_EVENTGROUP g ON e.EventGroupID = g.EventGroupID
                          LEFT OUTER JOIN SPORTS_AGE_GROUP h ON g.GroupID = h.AgeGroupID
                          INNER JOIN YEAR as z ON a.YearID = z.YearID";
             $userCondition = "d.RecordStatus IN (0,1,2)
            			  AND d.RecordType = 2
            			  AND (d.ClassName <> '' AND d.ClassNumber <> ''  AND d.ClassName IS NOT NULL  AND d.ClassNumber IS NOT NULL )";
             $yearCondition = "AND a.AcademicYearID = '$CurrentAcademicYear'";
             $orderBySql = "ORDER BY z.WEBSAMSCode, a.ClassTitleEN";
             $groupBySql = "GROUP BY a.YearClassID, b.UserID";
             $sql = "SELECT $tableFields
                    FROM
                          $tableSql
                    WHERE
                          $userCondition
            			  $yearCondition
            			  $groupBySql
            			  $orderBySql
                    ";
		     $result = $this->returnArray($sql);
		     
		     
		     $totalResult = array();
		     $totalResult['Title'] = $Lang['Sports']['Report']['AgeGroupTotal'];
		     
// 		     if($print) {
    		     $rebuildResult = array();
    		     foreach($result as $thisResult) {
    		         if(!isset($rebuildResult[$thisResult['Title']])) {
    		             $rebuildResult[$thisResult['Title']] = $thisResult;
    		             foreach($thisResult as $this_key => $thisVal) {
    		                 if($this_key != 'Title') {
    		                      $rebuildResult[$thisResult['Title']][$this_key] = 0;
    		                      if(!isset($totalResult[$this_key])) {
    		                          $totalResult[$this_key] = 0;
    		                      }
    		                 }
    		             }
    		         }
    		         
    		         foreach($thisResult as $this_key => $thisVal) {
    		             if($this_key != 'Title' && $thisVal > 0) {
    		                 $rebuildResult[$thisResult['Title']][$this_key]++;
    		                 $totalResult[$this_key]++;
    		             }
    		         }
    		     }
    		     $result = array_values($rebuildResult);
// 		     }
    		     
// 		     $totalFieldString = $Lang['Sports']['Report']['AgeGroupTotal'];
//              $totalField = str_replace("a.ClassTitleB5", "'$totalFieldString'", $tableFields);
//              $sql = "SELECT $totalField
//                     FROM
//                           $tableSql
//                     WHERE
//                           $userCondition
//             			  $yearCondition";
//             			  $totalArr = $this->returnArray($sql);debug_pr($totalArr);
//              //debug_pr($sql);die();
//              $result[] = $totalArr[0];

    		     $result[] = $totalResult;
             return $result;
         }
         
         function returnAllEventArrangement($print = false){
             
             global $Lang;
             
             $CurrentAcademicYear = Get_Current_Academic_Year_ID();
             if(!$print)
                 $AgeGroupOrder = $this->returnAgeGroupOrderArr();
                 else
                $AgeGroupOrder = $this->returnAgeGroupOrderArr2();
             $AgeIdArr = $AgeGroupOrder['idArr'];
             $AgeGroupArr = $AgeGroupOrder['groupArr'];
             $AgeIdsArr = $this->returnAgeGroupIDArr();
             $AgeIdsArr = implode("','", $AgeIdsArr);
             $eventTitle = Get_Lang_Selection('ChineseName','EnglishName');
             if(!$print)
                 $tableFields = "a.$eventTitle as Title, $AgeIdArr, CONCAT(SUM(IF(h.AgeGroupID IN ('$AgeIdsArr') AND f.studentID IS NOT NULL, 1, 0)),'/', SUM(IF(h.AgeGroupID IN ('$AgeIdsArr') AND e.studentID IS NOT NULL, 1, 0))) as EventTotal";
             else
                 $tableFields = "a.$eventTitle as Title, $AgeIdArr, SUM(IF(h.AgeGroupID IN ('$AgeIdsArr') AND f.studentID IS NOT NULL, 1, 0)) as EventTotal";
             $tableSql = "SPORTS_EVENT a
                          LEFT OUTER JOIN SPORTS_EVENTGROUP g ON a.EventID = g.EventID
                          LEFT OUTER JOIN SPORTS_AGE_GROUP h ON g.GroupID = h.AgeGroupID
                          LEFT OUTER JOIN SPORTS_STUDENT_ENROL_EVENT e ON g.EventGroupID = e.EventGroupID
                          LEFT OUTER JOIN SPORTS_LANE_ARRANGEMENT f ON e.StudentID = f.StudentID AND e.EventGroupID = f.EventGroupID AND f.RoundType = 1";
             $orderBySql = "ORDER BY a.$eventTitle";
             $groupBySql = "GROUP BY a.EventID";
             $sql = "SELECT $tableFields
                     FROM
                          $tableSql
                     WHERE
                            1
                          $groupBySql
                          $orderBySql
            ";
		  $result = $this->returnArray($sql);
		  //debug_pr($sql);
		  //debug_pr($result);die();
		  $totalFieldString = $Lang['Sports']['Report']['AgeGroupTotal'];
		  $totalField = str_replace("a.$eventTitle", "'$totalFieldString'", $tableFields);
		  $sql = "SELECT $totalField
                  FROM
                    $tableSql
                  WHERE 1
          $userCondition
		  $yearCondition";
		  $totalArr = $this->returnArray($sql);
		  //debug_pr($totalArr);die();
		  $result[] = $totalArr[0];
		  return $result;
         }
         
         # [Class Relay - Related Settings]
         function retrieveClassRelayType()
         {
             $sql = "SELECT RecordID, RelayType, RelayOrder FROM SPORTS_CLASS_RELAY_TYPE ORDER BY RelayOrder";
             return $this->returnArray($sql);
         }
         
         function retrieveAgeGroupID($EventGroupID)
         {
             $table = "SPORTS_EVENTGROUP";
             $col = "GroupID";
             $conds = "EventGroupID = '$EventGroupID'";
             $sql = "SELECT $col FROM $table WHERE $conds";
             $result = $this->returnArray($sql);
             return $result;
         }
         
         function retrieveStudentIDAndName($ageGroupID, $withoutClassNumber = false)
         {
             global $intranet_session_language;
             
             $CurrentAcademicYear = Get_Current_Academic_Year_ID();
             $student_name = getNameFieldWithClassNumberByLang("iu.");
             if($withoutClassNumber){
             	$student_name = getNameFieldByLang("iu.");
             }
             $age_group_name = Get_Lang_Selection("sag.ChineseName","sag.EnglishName");
             
             $col = "iu.UserID, $student_name, $age_group_name, ssei.AthleticNum";
             $table = "SPORTS_STUDENT_ENROL_INFO as ssei
                       INNER JOIN INTRANET_USER as iu
                            ON ssei.StudentID = iu.UserID
                       INNER JOIN SPORTS_AGE_GROUP sag
                            ON (iu.DateOfBirth >= sag.DOBUpLimit AND iu.DateOfBirth <= sag.DOBLowLimit AND iu.Gender = sag.Gender)
                       INNER JOIN YEAR_CLASS_USER ycu
                            ON iu.UserID = ycu.UserID
                       INNER JOIN YEAR_CLASS yc
                            ON ycu.YearClassID = yc.YearClassID ";
             $cond = "iu.RecordType = 2
                      AND iu.RecordStatus IN (0,1,2)
                      AND yc.AcademicYearID = '$CurrentAcademicYear'";
             $order = "ssei.AthleticNum asc";
             
             $sql = "SELECT $col FROM $table WHERE $cond ORDER BY $order ";		
			 return $this->returnArray($sql,5);
         }
         
         function retrieveClassStudent($classID, $ageGroupID = '', $gender = '')
         {
            if($ageGroupID != EVENT_BOYS_OPEN && $ageGroupID != EVENT_GIRLS_OPEN && $ageGroupID != EVENT_MIXED_OPEN)
            {
                $table = "  YEAR_CLASS yc
                            INNER JOIN YEAR_CLASS_USER ycu ON yc.YearClassID = ycu.YearClassID
                            INNER JOIN INTRANET_USER iu ON ycu.UserID = iu.UserID
                            INNER JOIN SPORTS_AGE_GROUP sag ON (iu.DateOfBirth BETWEEN sag.DOBUPLimit AND sag.DOBLowLimit) AND iu.Gender = sag.Gender";
                
                $snameField = Get_Lang_Selection('ChineseName', 'EnglishName');
                $ctitleField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
                //$age_group_name = Get_Lang_Selection("sag.ChineseName","sag.EnglishName");
                $col = "iu.UserID, iu.$snameField as sname, yc.$ctitleField as ctitle, ycu.ClassNumber, sag.AgeGroupID, sag.Gender";
                
                $cond = "yc.YearClassID = '$classID' ";
                if($ageGroupID != '') {
                    $cond .= "AND sag.AgeGroupID = '$ageGroupID' ";
                }
                if($gender != '') {
                    $cond .= "AND sag.Gender = '$gender'";
                }
                $order = 'ycu.ClassNumber asc';
            }
            else
            {
                $table = " YEAR_CLASS yc
                           INNER JOIN YEAR_CLASS_USER ycu ON yc.YearClassID = ycu.YearClassID
                           INNER JOIN INTRANET_USER iu ON ycu.UserID = iu.UserID
                           INNER JOIN (SELECT Gender, MIN(DOBUPLimit) as DOBUPLimit, MAX(DOBLowLimit) as DOBLowLimit FROM SPORTS_AGE_GROUP GROUP BY Gender) sag ON iu.Gender = sag.Gender";
                 
                $snameField = Get_Lang_Selection('ChineseName', 'EnglishName');
                $ctitleField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
                $col = "iu.UserID, iu.$snameField as sname, yc.$ctitleField as ctitle, ycu.ClassNumber, '$ageGroupID', sag.Gender";
                
                $cond = "yc.YearClassID = '$classID' ";
                if($ageGroupID == EVENT_BOYS_OPEN) {
                    $gender = 'M';
                }
                if($ageGroupID == EVENT_GIRLS_OPEN) {
                    $gender = 'F';
                }
                if($gender != '') {
                    $cond .= "AND sag.Gender = '$gender'";
                }
                // $cond .= "AND (iu.DateOfBirth < sag.DOBUPLimit OR iu.DateOfBirth > sag.DOBLowLimit)";
                
                $order = 'ycu.ClassNumber asc';
            }
            
            $sql = "SELECT $col FROM $table WHERE $cond ORDER BY $order";
            return $this->returnArray($sql,1);
         }
         
         # [Class Relay - Related UI]
         function getClassRelayGroupSelection($EventGroupID='', $AgeGroupID='', $YearClassID='', $selectedRelayGroupID='', $selectionTag='', $relayRound='')
         {
             include_once('libclass.php');
             $libclass = new libclass();
             
             $allYearClassArr = $libclass->getClassList();
             $allYearClassArr = BuildMultiKeyAssoc((array)$allYearClassArr, 'ClassID', 'ClassLevelID', 1, 0);
             
             /* 
             $excludeClassRelayGroupArr = array();
             if($relayRound == ROUND_TYPE_FIRSTROUND && $currentEventGroupID > 0) {
                 $excludeClassRelayGroupArr = $this->retrieveCRLaneArrangementByRound($relayType, $ageGroupID, ROUND_TYPE_FIRSTROUND, $currentEventGroupID);
             }
             
             $finalistClassRelayGroupArr = array();
             if($relayRound == ROUND_TYPE_FINALROUND) {
                 $relatedEventGroupIDs = $this->returnRelatedClassRelayEvent($relayType, $ageGroupID);
                 if(!empty($relatedEventGroupIDs) && count($relatedEventGroupIDs) > 1)
                 {
                     $finalistClassRelayGroupArr = $this->retrieveCRLaneArrangementByResult($relayType, $ageGroupID, ROUND_TYPE_FIRSTROUND, $currentEventGroupID);
                     if(empty($finalistClassRelayGroupArr)) {
                         $finalistClassRelayGroupArr = array('');
                     }
                 }
             }
              */
             
             $dataArr = array();
             $relatedClassGroupAry = $this->retrieveClassRelayClassGroup($ClassRelayGroupID='', $AgeGroupID, $YearClassID, $GroupTitle='', $EventGroupID);
             foreach((array)$relatedClassGroupAry as $thisClassGroupInfo)
             {
                 /* 
                 // Exclude class relay group which is already added to another 1st round heats
                 if(!empty($excludeClassRelayGroupArr) && in_array($thisClassGroupInfo['ClassRelayGroupID'], $excludeClassRelayGroupArr)) {
                     continue;
                 }
                 
                 // Only includes class relay group which is top 10 in 1st round result
                 if(!empty($finalistClassRelayGroupArr) && !in_array($thisClassGroupInfo['ClassRelayGroupID'], $finalistClassRelayGroupArr)) {
                     continue;
                 }
                  */
                 
                 $thisClassLevelID = $allYearClassArr[$thisClassGroupInfo['YearClassID']];
                 $thisClassLevelName = $libclass->getLevelName($thisClassLevelID);
                 
                 $thisClassName = $libclass->getClassNameByLang($thisClassGroupInfo['YearClassID']);
                 $thisGroupTitle = $thisClassGroupInfo['GroupTitle'] == ''? '' : ' ('.$thisClassGroupInfo['GroupTitle'].')';
                 $dataArr[$thisClassLevelName][$thisClassGroupInfo['ClassRelayGroupID']] = $thisClassName.$thisGroupTitle;
             }
             
             return getSelectByAssoArray($dataArr, $selectionTag, $selectedRelayGroupID, 0, 0);
         }
         
         # [Class Relay - Class Group Settings & Related Students]
         //function retrieveClassRelayClassGroup($ClassRelayGroupID='', $AgeGroupID='', $YearClassID='', $GroupTitle='', $RelayType='')
         function retrieveClassRelayClassGroup($ClassRelayGroupID='', $AgeGroupID='', $YearClassID='', $GroupTitle='', $EventGroupID='', $excludeAgeGroupID='')
         {
             $cond = '';
             if($ClassRelayGroupID != '') {
                 $cond .= " AND crl_g.ClassRelayGroupID = '$ClassRelayGroupID' ";
             }
             if($AgeGroupID) {
                 $cond .= " AND crl_g.AgeGroupID IN ('".implode("', '", (array)$AgeGroupID)."') ";
             }
             if($YearClassID) {
                 $cond .= " AND crl_g.YearClassID IN ('".implode("', '", (array)$YearClassID)."') ";
             }
             if($GroupTitle != '') {
                 $cond .= " AND crl_g.GroupTitle LIKE '%$GroupTitle%' ";
             }
             /* 
             if($RelayType != '') {
                 $cond .= " AND crl_g.RelayType = '$RelayType' ";
             }
             */
             if($EventGroupID) {
                 $cond .= " AND crl_g.EventGroupID IN ('".implode("', '", (array)$EventGroupID)."') ";
             }
             if($excludeAgeGroupID) {
                 $cond .= " AND crl_g.AgeGroupID NOT IN ('".implode("', '", (array)$excludeAgeGroupID)."') ";
             }
             
             $sql = "SELECT
                        crl_g.ClassRelayGroupID, crl_g.YearClassID, crl_g.AgeGroupID, crl_g.GroupTitle, crl_g.EventGroupID
                    FROM 
                        SPORTS_CLASS_RELAY_CLASS_GROUP crl_g 
                        INNER JOIN YEAR_CLASS yc ON (yc.YearClassID = crl_g.YearClassID)
                        INNER JOIN YEAR y ON (y.YearID = yc.YearID)
                    WHERE 
                        1 $cond
                    ORDER BY
                        y.Sequence + 0, yc.Sequence + 0, crl_g.ClassRelayGroupID";
             return $this->returnArray($sql);
         }
         
         function retrieveClassRelayGroupStudent($ClassRelayGroupID, $EventGroupID, $getStudentInfo=false)
         {
             $cond = '';
             if($ClassRelayGroupID) {
                 $cond .= "AND crl_stu.ClassRelayGroupID IN ('".implode("', '", (array)$ClassRelayGroupID)."') ";
             }
             if($EventGroupID) {
                 $cond .= "AND crl_stu.EventGroupID IN ('".implode("', '", (array)$EventGroupID)."') ";
             }
             $sql = "SELECT * FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT_MAPPING crl_stu WHERE 1 $cond";
             
             if($getStudentInfo)
             {
                 $name_field = Get_Lang_Selection('iu.ChineseName', 'iu.EnglishName');
                 $class_field = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
                 $CurrentAcademicYear = Get_Current_Academic_Year_ID();
                 
                 $sql = "SELECT
                            crl_stu.ClassRelayGroupID, crl_stu.UserID, crl_g.GroupTitle,
                            $name_field as stu_name, $class_field as class_name, ycu.ClassNumber
                        FROM
                            SPORTS_CLASS_RELAY_LANE_ARRANGEMENT_MAPPING crl_stu
                            INNER JOIN SPORTS_CLASS_RELAY_CLASS_GROUP crl_g ON (crl_g.ClassRelayGroupID = crl_stu.ClassRelayGroupID)
                            INNER JOIN INTRANET_USER iu ON (iu.UserID = crl_stu.UserID)
                            INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = iu.UserID)
                            INNER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID)
                        WHERE
                            iu.RecordStatus IN (0,1,2) AND iu.RecordType = 2 AND 
                            yc.AcademicYearID = '$CurrentAcademicYear'
                            $cond";
             }
             
             return $this->returnArray($sql);
         }
         
         function updateClassRelayGroupStudent($ClassRelayGroupID, $EventGroupID, $StudentIDArr='')
         {
             if($StudentIDArr == '') {
                 $StudentIDArr = array();
             }
             
             $resultArr = array();
             $resultArr['insert'] = array();
             $resultArr['delete'] = array();
             
             # Get assigned Class Group Student
             $assignedStudentArr = $this->retrieveClassRelayGroupStudent($ClassRelayGroupID, $EventGroupID);
             $assignedStudentIDArr = Get_Array_By_Key((array)$assignedStudentArr, 'UserID');
             
             # DELETE
             if(!empty($assignedStudentIDArr)) {
                 foreach((array)$assignedStudentIDArr as $studentID) {
                     if(!in_array($studentID, (array)$StudentIDArr)) {
                         $resultArr['delete'][] = $this->deleteClassRelayGroupStudent($ClassRelayGroupID, $EventGroupID, $studentID);
                     }
                 }
             }
             
             # INSERT
             if(!empty($StudentIDArr)) {
                 foreach((array)$StudentIDArr as $studentID) {
                     if(!in_array($studentID, (array)$assignedStudentIDArr)) {
                         $resultArr['insert'][] = $this->insertClassRelayGroupStudent($ClassRelayGroupID, $EventGroupID, $studentID);
                     }
                 }
             }
             
             return $resultArr;             
         }
         
         function insertClassRelayGroupStudent($ClassRelayGroupID, $EventGroupID, $uid)
         {
             $sql = "INSERT INTO SPORTS_CLASS_RELAY_LANE_ARRANGEMENT_MAPPING (ClassRelayGroupID, EventGroupID, UserID, DateModified) VALUES ('$ClassRelayGroupID', '$EventGroupID', '$uid', NOW())";
             $this->db_db_query($sql);
             
             return $this->db_insert_id();
         }
         
         function deleteClassRelayGroupStudent($ClassRelayGroupID, $EventGroupID, $uid = '')
         {
             $cond = '';
             if($uid != '') {
                 $cond .= "AND UserID = '$uid'";
             }
             
             $sql = "DELETE FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT_MAPPING WHERE ClassRelayGroupID = '$ClassRelayGroupID' AND EventGroupID = '$EventGroupID' $cond";
             return $this->db_db_query($sql);
         }
         
         # [Class Relay - Relay Line Arrangement]   [not used]
         function retrieveClassRelayLaneMapping($EventGroupID, $ArrangeOrder = '')
         {
             $table = "SPORTS_CLASS_RELAY_LANE_ARRANGEMENT_MAPPING scrlam";
             
             $col = "EventGroupID, ArrangeOrder, UserID";
             
             $cond = "EventGroupID = '$EventGroupID' ";
             
             if($ArrangeOrder!=''){
                 $cond .="AND ArrangeOrder = '$ArrangeOrder' ";
             }
             
             $sql = "SELECT $col
             FROM $table
             WHERE $cond";
             $result = $this->returnArray($sql);
             return $result;
         }
         
         function updateClassRelayLaneMapping($EventGroupID, $ArrangeOrder, $StudArr = '')
         {
             $oriData = $this->retrieveClassRelayLaneMapping($EventGroupID, $ArrangeOrder);
             $oriArr = array();
             $result = array();
             $result['insert'] = array();
             $result['delete'] = array();
             if(!empty($oriData)){
                 foreach($oriData as $arrange){
                     $oriArr[] = $arrange['UserID'];
                 }
             }
             if($StudArr==''){
                 $StudArr = array();
             }
             if(!empty($oriArr)){
                 foreach($oriArr as $ori){
                     if(!in_array($ori, $StudArr)){
                         //delete
                         $result['delete'][] = $this->deleteClassRelayLaneMapping($EventGroupID, $ArrangeOrder, $ori);
                     }
                 }
             }
             if(!empty($StudArr)){
                 foreach($StudArr as $stud){
                     if(!in_array($stud, $oriArr)){
                         //insert
                         $result['insert'][] = $this->insertClassRelayLaneMapping($EventGroupID, $ArrangeOrder, $stud);
                     }
                 }
             }
             return $result;             
         }
         
         function insertClassRelayLaneMapping($EventGroupID, $ArrangeOrder, $uid)
         {
             $col = 'EventGroupID, ArrangeOrder, UserID, DateModified';
             $value = "'$EventGroupID','$ArrangeOrder', '$uid', NOW()";
             $table = "SPORTS_CLASS_RELAY_LANE_ARRANGEMENT_MAPPING";
             $sql = "INSERT INTO $table ($col) VALUES ($value)";
             $this->db_db_query($sql);
             $resultid = $this->db_insert_id();
             return $resultid;
         }
         
         function deleteClassRelayLaneMapping($EventGroupID, $ArrangeOrder, $uid = '')
         {
             $table = "SPORTS_CLASS_RELAY_LANE_ARRANGEMENT_MAPPING";
             $cond = "EventGroupID = '$EventGroupID' ";
             $cond .="AND ArrangeOrder = '$ArrangeOrder' ";
             if($uid!=''){
                $cond .="AND UserID = '$uid'";
             }
             $sql = "DELETE FROM $table WHERE $cond";
             $result = $this->db_db_query($sql);
             return $result;
         }
         
         function returnRelatedClassRelayEvent($eventRelayType, $ageGroupID)
         {
            $sql = "SELECT
                        a.EventGroupID
                    FROM
                        SPORTS_EVENTGROUP as a
                        INNER JOIN SPORTS_EVENT as b ON (a.EventID = b.EventID)
                    WHERE 
                        b.RelayEventType = '$eventRelayType' AND 
                        a.GroupID = '$ageGroupID' AND 
                        b.EventType IN (".EVENT_TYPE_CLASSRELAY.")";
             $temp = $this->returnVector($sql, 2);
             return $temp;
         }
         
         function retrieveClassRelayLaneArrangementByRound($eventGroupID, $roundType='', $heatGroup='')
         {
             $cond = '';
             if($roundType != '') {
                 $cond .= " AND RoundType = '".$roundType."'";
             }
             if($heatGroup != '') {
                 $cond .= " AND Heat = '".$heatGroup."'";
             }
             
             $sql = "SELECT
                        ClassID, ClassGroupID, RoundType, Heat, ArrangeOrder
                    FROM
                        SPORTS_CLASS_RELAY_LANE_ARRANGEMENT
                    WHERE
                        EventGroupID = '$eventGroupID'
                        $cond ";
             $temp = $this->returnArray($sql, 2);
             return $temp;
         }
         
         function retrieveClassRelayGroupRankingByRound($eventID, $groupID)
         {
             global $intranet_session_language;
             
             # 20081014 add result in "Event Rankings" list
             $result_sql = $this->Get_Pad_Track_Result_Sql('a.');
             $ClassTitle = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
             
             $sql = "SELECT
                        c.$ClassTitle, a.ClassID, a.Rank, a.RecordStatus, $result_sql
                    FROM
                        SPORTS_CLASS_RELAY_LANE_ARRANGEMENT as a
                        LEFT JOIN SPORTS_EVENTGROUP as b ON a.EventGroupID = b.EventGroupID
                        INNER JOIN YEAR_CLASS as c ON a.ClassID = c.YearClassID
                    WHERE
                        b.EventID = '$eventID' AND 
                        b.GroupID = '$groupID' AND 
                        a.Rank IS NOT NULL
                    ORDER BY 
                        a.Rank
                    LIMIT 0, 8 ";
             $temp = $this->returnArray($sql,7);
             
             return $temp;
         }
         
         function returnClassRelayHeatNumberByEventGroupID($eventGroupID)
         {
             $sql = "SELECT DISTINCT a.Heat
                        FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT as a
                        WHERE a.EventGroupID = '$eventGroupID' AND a.RoundType = '1' ";
             $temp = $this->returnVector($sql);
             
             return sizeof($temp);
         }
         
         function Get_Obtained_Bonus_Score_Class_Relay($RoundType, $EventGroupID, $ClassGroupID)
         {
             if($RoundType == ROUND_TYPE_FIRSTROUND) {
                 return 0;
             }
             
             switch($RoundType)
             {
                 case ROUND_TYPE_FINALROUND:
                     $PreviousRoundTypeSql = " RoundType IN (".ROUND_TYPE_FIRSTROUND.") ";
                     break;
             }
             
             $sql = "SELECT
                        SUM(Score)
                     FROM
                        SPORTS_CLASS_RELAY_LANE_ARRANGEMENT
                     WHERE
                        $PreviousRoundTypeSql
                        AND EventGroupID = '$EventGroupID'
                        AND ClassGroupID = '$ClassGroupID'
                     GROUP BY
                        EventGroupID ";
                 $result = $this->returnVector($sql);
                 
                 return $result[0];
         }
         
         # Retrieve Class Relay Event Lane Arrangement Detail
         function returnClassRelayLaneArrangeDetailByEventGroupID($eventGroupID="", $round="")
         {
             if(trim($round) != '') {
                 $cond_RoundType = " AND a.RoundType = '$round' ";
             }
             if(trim($eventGroupID) != '') {
                 $cond_EventGroupID = " AND a.EventGroupID = '$eventGroupID' ";
             }
             
             $TrackResult = $this->Get_Track_Result_Display_Sql("a.");
             $TrackPadResult = $this->Get_Pad_Track_Result_Sql("a.");
             $name_fields = Get_Lang_Selection("b.ClassTitleB5", "b.ClassTitleEN");
             
             $sql = "SELECT
                        a.Heat, a.ArrangeOrder, a.ClassGroupID, a.Rank, a.Score, $TrackResult as Result, $TrackPadResult, a.RecordStatus,
                        b.YearClassID, $name_fields as ClassName, b.ClassTitleB5, b.ClassTitleEN, c.GroupTitle, a.AbsentReason
                     FROM
                        SPORTS_CLASS_RELAY_LANE_ARRANGEMENT as a
                        INNER JOIN YEAR_CLASS as b ON a.ClassID = b.YearClassID
                        INNER JOIN SPORTS_CLASS_RELAY_CLASS_GROUP c ON (a.ClassGroupID = c.ClassRelayGroupID)
                     WHERE 1
                        $cond_EventGroupID
                        $cond_RoundType
                     ORDER BY 
                        a.Heat, a.ArrangeOrder";
             $temp = $this->returnArray($sql);
             
             return $temp;
         }
         
         function retrieveCRLaneArrangementByResult($relayType, $ageGroupID, $relayRound, $excludeEventGroupID='')
         {
             $resultField = " (rl.resultMin*60*100+rl.resultSec*100+rl.resultMs) ";
             
             $extra_cond = '';
             if(is_array($excludeEventGroupID) || $excludeEventGroupID != '') {
                 $extra_cond .= " AND a.EventGroupID NOT IN ('".implode("', '", (array)$excludeEventGroupID)."') ";
             }
             
             $sql = "SELECT
                        rl.ClassGroupID
                     FROM
                         SPORTS_EVENTGROUP as a
                         INNER JOIN SPORTS_EVENT as b ON (a.EventID = b.EventID)
                         INNER JOIN SPORTS_CLASS_RELAY_LANE_ARRANGEMENT as rl ON (a.EventGroupID = rl.EventGroupID)
                     WHERE
                         b.RelayEventType = '$relayType' AND
                         a.GroupID = '$ageGroupID' AND
                         b.EventType IN (".EVENT_TYPE_CLASSRELAY.") AND
                         b.RelayRoundType = '$relayRound' AND
                         $resultField <> 0
                         $extra_cond
                     ORDER BY
                         $resultField ASC
                     LIMIT 10 ";
             $temp = $this->returnVector($sql, 2);
             return $temp;
         }
         
         function returnBestRecordResultOfClassRelayEvent($eventGroupID)
         {
             $result = " (a.resultMin*60*100+a.resultSec*100+a.resultMs) ";
             $oldRecord = " (b.recordMin*60*100+b.recordSec*100+b.recordMs) ";
             $orderType = "ASC";
             
             $sql = "SELECT
                        a.ClassGroupID, c.YearClassID, c.GroupTitle, a.ResultMin, a.ResultSec, a.ResultMs
                    FROM
                        SPORTS_CLASS_RELAY_LANE_ARRANGEMENT a
                        INNER JOIN SPORTS_CLASS_RELAY_CLASS_GROUP c ON a.ClassGroupID = c.ClassRelayGroupID
                        LEFT JOIN SPORTS_EVENTGROUP_EXT_RELAY b ON a.eventGroupID = b.eventGroupID
                    WHERE 
                        a.EventGroupID = '$eventGroupID' AND
                        $result <> 0 AND
                        ($result < $oldRecord OR $oldRecord = 0)
                    ORDER BY 
                        $result $orderType, 
                        a.dateModified ASC
                    LIMIT 0,1 ";
                        $temp = $this->returnArray($sql,4);
             return $temp;
         }
         
         function returnOldBestResultOfClassRelayEventType($eventGroupIDArr)
         {
             $oldResultField = " (recordMin*60*100+recordSec*100+recordMs) ";
             
             $sql = "SELECT $oldResultField FROM SPORTS_EVENTGROUP_EXT_RELAY
                        WHERE EventGroupID IN ('".implode("', '", $eventGroupIDArr)."') AND $oldResultField <> 0
                        ORDER BY $oldResultField ASC
                        LIMIT 0,1";
             $temp = $this->returnVector($sql);
             $oldResult = $temp[0];
             
             $oldResult = $oldResult? $oldResult : 0;
             return $oldResult;
         }
         
         ################################ field event functions end ####################################
         
         ################################ event group schedule functions start ###############################
         function insertEventGroupSchedule($arr = array()){
         	global $_SESSION;
         	// EventGroupID, EventRound, EventScheduleCode, EventTime
         	$table = "SPORTS_EVENTGROUP_SCHEDULE";
         	$cols = "EventGroupID, EventRound,";
         	$cols .= "EventScheduleCode,"; 
         	$cols .= " EventTime,";
         	$cols .= "InputBy, DateInput";
         	$vals = "";
         	foreach($arr as $eg){
         		if($vals != "") $vals .= ", ";
         		$vals .= "(";
         		$vals .= "'$eg[EventGroupID]', '$eg[EventRound]', ";
         		if($eg['EventScheduleCode']){
         			$vals .= "'$eg[EventScheduleCode]',"; 
         		} else {
         			$vals .= " NULL,";
         		}
         		if($eg['EventTime']){
         			$vals .= "'$eg[EventTime]',";
         		} else {
         			$vals .= "NULL,";
         		}
         		$vals .= "'$_SESSION[UserID]',";
         		$vals .= "NOW()";
         		$vals .= ")";
         	}
         	$sql = "INSERT INTO $table ($cols) VALUES $vals";
         	$result = $this->db_db_query($sql);
         	return $result;
         }
         function updateEventGroupSchedule($ary){
         	global $_SESSION;
         	// ScheduleID, EventGroupID, EventRound, EventScheduleCode, EventTime
         	$table = "SPORTS_EVENTGROUP_SCHEDULE";
         	$cols = "";
         	$conds = "";
         	$cols .= "EventScheduleCode = ".($ary['EventScheduleCode'] ? "'$ary[EventScheduleCode]'" : "NULL" );
         	$cols .= ",EventTime = '$ary[EventTime]'";
         	$cols .= ",ModifiedBy = '$_SESSION[UserID]'";
         	$cols .= ",DateModified = NOW()";
         	$conds .= "ScheduleID = '$ary[ScheduleID]'";
         	$sql = "UPDATE $table SET $cols WHERE $conds";
         	$result = $this->db_db_query($sql);
         	return $result;
         }
         function deleteEventGroupSchedule($SID){
         	global $_SESSION;
         	$table = "SPORTS_EVENTGORUP_SCHEDULE";
         	$cols = "isDeleted = '1', ModifiedBy = '$_SESSION[UserID]', DateModified = NOW()'";
         	$conds = "ScheduleID = '$SID'";
         	$sql = "UPDATE $table SET $cols WHERE $conds";
         	$result = $this->db_db_query($sql);
         	return $result;
         }
         function deleteAllEventGroupSchedule(){
         	$table = "SPORTS_EVENTGROUP_SCHEDULE";
         	$cols = "isDeleted = '1'";
         	$sql = "UPDATE $table SET $cols, ModifiedBy = '$_SESSION[UserID]', DateModified = NOW()";
         	$result = $this->db_db_query($sql);
         	return $result;
         }
         
         function initEventGroupSchedule(){
         	$deleteAll = $this->deleteAllEventGroupSchedule();
         	// Get All Event Group Details
         	$eventGroupAry = array();
         	$cols = "
					a.EventGroupID,
					b.EventType,
					CASE b.EventType
					WHEN 1 THEN IF(d_track.EventGroupID IS NOT NULL AND d_track.FirstRoundDay <> '0', d_track.FirstRoundDay, '-1')
                    WHEN 2 THEN IF(d_field.EventGroupID IS NOT NULL AND d_field.FirstRoundDay <> '0', d_field.FirstRoundDay, '-1')
                    WHEN 3 THEN '-1'
                    WHEN 4 THEN '-1'
					END AS FirstRoundDay,
					CASE b.EventType
					WHEN 1 THEN IF(d_track.EventGroupID IS NOT NULL AND d_track.SecondRoundDay <> '0', d_track.SecondRoundDay, '-1')
                    WHEN 2 THEN '-1'
                    WHEN 3 THEN '-1'
                    WHEN 4 THEN '-1'
					END AS SecondRoundDay,
					CASE b.EventType
					WHEN 1 THEN IF(d_track.EventGroupID IS NOT NULL AND d_track.FinalRoundDay <> '0',d_track.FinalRoundDay, '-1')
                    WHEN 2 THEN IF(d_field.EventGroupID IS NOT NULL AND d_field.FinalRoundDay <> '0', d_field.FinalRoundDay, '-1')
                    WHEN 3 THEN IF(d_relay.EventGroupID IS NOT NULL AND d_relay.FinalRoundDay <> '0', d_relay.FinalRoundDay, '-1')
                    WHEN 4 THEN IF(d_relay.EventGroupID IS NOT NULL AND d_relay.FinalRoundDay <> '0', d_relay.FinalRoundDay, '-1')
					END AS FinalRoundDay
					";
         	$conds = "";
         	$conds .= "AND CASE b.EventType
                    WHEN 1 THEN (d_track.EventGroupID IS NOT NULL)
                    WHEN 2 THEN (d_field.EventGroupID IS NOT NULL)
                    WHEN 3 THEN (d_relay.EventGroupID IS NOT NULL)
                    WHEN 4 THEN (d_relay.EventGroupID IS NOT NULL)
                    END ";
         	$tables = "";
         	$tables .= "SPORTS_EVENTGROUP as a ";
         	$tables .= "LEFT OUTER JOIN SPORTS_EVENT as b ON a.EventID = b.EventID ";
         	$tables .= "LEFT OUTER JOIN SPORTS_EVENT_TYPE_NAME as c ON b.EventType = c.EventTypeID ";
         	$tables .= "LEFT OUTER JOIN SPORTS_EVENTGROUP_EXT_TRACK as d_track ON a.EventGroupID = d_track.EventGroupID ";
         	$tables .= "LEFT OUTER JOIN SPORTS_EVENTGROUP_EXT_FIELD as d_field ON a.EventGroupID = d_field.EventGroupID ";
         	$tables .= "LEFT OUTER JOIN SPORTS_EVENTGROUP_EXT_RELAY as d_relay ON a.EventGroupID = d_relay.EventGroupID ";
         	$tables .= "LEFT OUTER JOIN SPORTS_AGE_GROUP as age_group ON a.GroupID = age_group.AgeGroupID ";
         	
         	$sql = "SELECT $cols
                    FROM
                    $tables
                    WHERE
                    1
                    $conds";
         	$eventgroups = $this->returnArray($sql);
         	//debug_pr($eventgroups);die();
         	$a = array(0,0,0);
         	foreach($eventgroups as $eg){
         		if($eg['FirstRoundDay'] != '-1' && trim($eg['FirstRoundDay']) != ''){
         			$eventGroupAry[] = array(
         					'EventGroupID' => $eg['EventGroupID'],
         					'EventRound' => ROUND_TYPE_FIRSTROUND,
         			);
         			$a[0]++;
         		}
         		if($eg['SecondRoundDay'] != '-1' && trim($eg['SecondRoundDay']) != ''){
         			$eventGroupAry[] = array(
         					'EventGroupID' => $eg['EventGroupID'],
         					'EventRound' => ROUND_TYPE_SECONDROUND,
         			);
         			$a[1]++;
         		}
         		if($eg['FinalRoundDay'] != '-1' && trim($eg['FinalRoundDay']) != ''){
         			$eventGroupAry[] = array(
         					'EventGroupID' => $eg['EventGroupID'],
         					'EventRound' => ROUND_TYPE_FINALROUND,
         			);
         			$a[2]++;
         		}
         	}
         	// Insert Event Group Schedule
         	//debug_pr($a);
         	//debug_pr($eventGroupAry);die();
         	$insertAll = $this->insertEventGroupSchedule($eventGroupAry);
         	return $insertAll;
         }
         
         function Get_Student_Profile_Attendance_Data($ClassID, $StartDate = '', $EndDate = '', $ClassName = '', $StudentIDAry = '', $Reason = '')
         {
         	// move from libreportcard2008
         	include_once ("libclass.php");
         	include_once ("libattendance.php");
         	$lattend = new libattendance();
         	if ($ClassName == '') {
         		$class_name = $lattend->getClassName($ClassID);
         	} else {
         		$class_name = $ClassName;
         	}
         	
         	$result = $lattend->getAttendanceListByClass($class_name, $StartDate, $EndDate, $Reason, $StudentIDAry);
         	$ReturnArr = array();
         	for ($i = 0; $i < sizeof($result); $i ++) {
         		list ($id, $name, $classnumber, $absence, $late, $earlyleave) = $result[$i];
         		$ReturnArr[$id]["Days Absent"] = $absence;
         		$ReturnArr[$id]["Time Late"] = $late;
         		$ReturnArr[$id]["Early Leave"] = $earlyleave;
         	}
         	
         	return $ReturnArr;
         }
         
         ################################ event group schedule functions end #################################
 }

} // End of directives

?>