<?php
// editing by 

/**
 * Change Log:
 * 2020-10-05 Siuwan
 *  - Modified Get_Book_Description(), to show lang by elib book info
 * 2020-09-16 Paul
 *  - Modified Get_Table_of_Content_Layer(), Get_Table_of_Content_Layer_IE(), add new subcategory
 * 2018-12-12 Pun [ip.2.5.10.1.1]
 *  - Modified Get_Table_of_Content_Layer(), Get_Table_of_Content_Layer_IE(), added ISBN
 * 2017-06-26 Paul
 * Modified function getTableOfContentEntry() to handle the problem of NOT display a sub-chapter if that chapter has only one sub-chapter
 * 2015-12-11 Paul
 * Modify PowerSpeech expiration policy
 * 2012-11-15 CharlesMa
 * [eBook] Feedback and Schedule Arrangement of eBook Comment ---- recommended reading :
 * 			a. $Lang['eBookReader']['Message']
 * 			b. btn_tool_book_progress_cancel; btn_recommend_cancel; btn_tool_book_review_close;
 *
 */
include_once('libinterface.php');
include_once('libebookreader.php');
include_once($intranet_root.'/lang/ebook_reader_lang.'.$intranet_session_language.'.php');

class libebookreader_ui extends interface_html
{
    var $libebookreader = null;

    function libebookreader_ui()
    {
        parent::interface_html();
    }

    function Get_libebookreader()
    {
        if(!isset($this->libebookreader) || !$this->libebookreader){
            $this->libebookreader = new libebookreader();
        }

        return $this->libebookreader;
    }

    function Include_eBookReader_CSS_JS()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $userBrowser;

        $x = '<link rel="stylesheet" href="/templates/'.$LAYOUT_SKIN.'/css/ebook_reader.css" type="text/css" />'."\n";
        $x.= '<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />';
        $x.= '<link rel="stylesheet" href="/templates/jquery/jquery.fancybox.css" type="text/css" media="screen" />';
        $x.= '<link rel="stylesheet" href="/templates/jquery/jquery.fancybox-thumbs.css" type="text/css" media="screen" />';
        $x.= '<link rel="stylesheet" href="/templates/jquery/jquery.qtip.min.css" type="text/css" media="screen" />';
        //	$x.= '<script type="text/javascript" src="/home/eLearning/ebook_reader/jquery-1.5.1.min.js" language="JavaScript" ></script>'."\n";
        //	$x.= '<script type="text/javascript" src="/home/eLearning/ebook_reader/jquery-ui-1.8.11.custom.min.js" language="JavaScript" ></script>'."\n";

        //	$x.= '<link rel="stylesheet" href="/home/eLearning/ebook_reader/css/jquery-ui-1.8.18.custom.css" type="text/css" media="screen" />';
        $x.= '<script type="text/javascript" src="/home/eLearning/ebook_reader/jquery-1.7.1.min.js" language="JavaScript" ></script>'."\n";
        //$x.= '<script type="text/javascript" src="/home/eLearning/ebook_reader/jquery.mobile-1.1.2.min.js" language="JavaScript" ></script>'."\n";
        $x.= '<script type="text/javascript" src="/home/eLearning/ebook_reader/jquery-ui-1.8.18.custom.min.js" language="JavaScript" ></script>'."\n";
        //if($userBrowser->platform == 'iPad'){
        $x.= '<script type="text/javascript" src="/home/eLearning/ebook_reader/jquery.ui.mouse.touch.js" language="JavaScript" ></script>'."\n";
        //}
        $x .= '<script type="text/javascript" src="/home/eLearning/ebook_reader/node.prototype.js" language="JavaScript"></script>';

        $x.= '<script type="text/javascript" src="/home/eLearning/ebook_reader/jquery.highlight-3.js" language="JavaScript" ></script>'."\n";
        $x.= '<script type="text/javascript" src="/home/eLearning/ebook_reader/sketch.js" language="JavaScript" ></script>'."\n";

        $x.= '<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>'."\n";
        $x.= '<script type="text/javascript" src="/templates/jquery/jquery.blockUI.js"></script>'."\n";
        $x.= '<script type="text/javascript" src="/templates/jquery/jquery.fancybox.js"></script>'."\n";
        $x.= '<script type="text/javascript" src="/templates/jquery/jquery.qtip.js"></script>'."\n";
        $x.= '<script type="text/javascript" src="/templates/script.js" language="JavaScript"></script>'."\n";

        $x.= $this->Get_JS_Lang();

        return $x ;
    }

    function Get_JS_Lang()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

        $x = '<script language="JavaScript">'."\n";
        $x.= 'var jsLang = {};'."\n";
        $x.= 'jsLang["PleaseSelectText"] = \''.$Lang['eBookReader']['Warning']['PleaseSelectText'].'\';'."\n";
        $x.= 'jsLang["ConfirmDeleteNotes"] = \''.$Lang['eBookReader']['Warning']['ConfirmDeleteNotes'].'\';'."\n";
        $x.= 'jsLang["NoNotesAtTheMoment"] = \''.$Lang['eBookReader']['NoNotesAtTheMoment'].'\';'."\n";
        $x.= 'jsLang["PleaseSelectYourProgress"] = \''.$Lang['eBookReader']['Warning']['PleaseSelectYourProgress'].'\';'."\n";
        $x.= 'jsLang["PleaseInputEmailAddress"] = \''.$Lang['eBookReader']['Warning']['PleaseInputEmailAddress'].'\';'."\n";
        $x.= 'jsLang["PleaseInputValidEmailAddresses"] = \''.$Lang['eBookReader']['Warning']['PleaseInputValidEmailAddresses'].'\';'."\n";
        $x.= 'jsLang["PleaseInputRecommendMessage"] = \''.$Lang['eBookReader']['Warning']['PleaseInputRecommendMessage'].'\';';
        $x.= 'jsLang["PleaseInputContent"] = \''.$Lang['eBookReader']['Warning']['PleaseInputContent'].'\';'."\n";
        $x.= 'jsLang["ResultsFound"] = \''.$Lang['eBookReader']['ResultsFound'].'\';'."\n";
        $x.= 'jsLang["PleaseInputSearchKeyword"] = \''.$Lang['eBookReader']['Warning']['PleaseInputSearchKeyword'].'\';'."\n";
        $x.= 'jsLang["Search"] = \''.$Lang['eBookReader']['Search'].'\';'."\n";
        $x.= 'jsLang["Stop"] = \''.$Lang['eBookReader']['Stop'].'\';'."\n";
        $x.= 'jsLang["FeatureNotAvailableForTrial"] = \''.$Lang['eBookReader']['Warning']['FeatureNotAvailableForTrial'].'\';'."\n";
        $x.= 'jsLang["Today"] = \''.$Lang['eBookReader']['today'].'\';'."\n";
        $x.= 'jsLang["Yesterday"] = \''.$Lang['eBookReader']['yesterday'].'\';'."\n";
        $x.= 'jsLang["Page"] = \''.$Lang['eBookReader']['Page'].'\';'."\n";
        $x.= 'jsLang["Loading"] = \''.$Lang['eBookReader']['Loading'].'\';'."\n";
        $x.= '</script>'."\n";

        return $x;
    }

    function Get_Layout_Start()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

        $x = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			  <html xmlns="http://www.w3.org/1999/xhtml">
			  <head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>eClass eBook Reader</title>';
        //$x.= $this->Include_JS_CSS();
        $x.= $this->Include_eBookReader_CSS_JS();
        $x.= '</head>'."\n";
        $x.= '<body>'."\n";

        return $x;
    }

    function Get_Layout_Stop()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;

        $x = '</body>'."\n";
        $x.= '</html>';

        return $x;
    }

    function Get_eBook_Reader_Index($PageMode=2, $IsImageBook=false, $ImageWidth=910)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $userBrowser;

        $browsertype = $userBrowser->browsertype;

        $x = $this->Get_Top_Tool_Layer($PageMode);
        if($IsImageBook){
            $x.= $this->Get_Book_Comic_Layer($ImageWidth,$PageMode);
        }
        if(!$IsImageBook && $browsertype == 'MSIE'){
            $x.= $this->Get_Book_Content_Layer_IE();
        }else{
            $x.= $this->Get_Book_Content_Layer($PageMode);
        }
        if($IsImageBook){
            $x.=$this->Get_Book_Comic_BG_Structure_Layer($ImageWidth,$PageMode);
        }else{
            if(!$IsImageBook && $browsertype == 'MSIE'){
                $x.= $this->Get_Book_BG_Structure_Layer_IE();
            }else{
                $x.= $this->Get_Book_BG_Structure_Layer($PageMode);
            }
        }
        $x.= $this->Get_Bottom_Tool_Layer($PageMode);

        // hidden layers
        $x.= $this->Get_Tool_Book_Board_Layer();
        $x.= $this->Get_Tool_Setting_Board_Layer();
        $x.= $this->Get_Tool_Page_Search_Layer();
        $x.= $this->Get_Tool_Book_Progress_Layer();
        $x.= $this->Get_Tool_Book_Recommend_Layer();
        $x.= $this->Get_Tool_Book_Review_Layer();
        $x.= $this->Get_Tool_Book_Review_Edit_Layer();
        $x.= $this->Get_Tool_Notes_Pager_Layer();
        $x.= $this->Get_Tool_Summary_Pager_Layer();//20140919-newfeature2014
        $x.= $this->Get_Tool_Glossary_Board_Layer();//20140919-newfeature2014
        $x.= $this->Get_Tool_Glossary_Pager_Layer();//20140919-newfeature2014
        $x.= $this->Get_Tool_Highlight_Notes_Pager_Layer();
        $x.= $this->Get_Tool_Notes_Book_Layer();
        $x.= $this->Get_Tool_Bookmark_List_Layer();
        $x.= $this->Get_Tool_Hightlight_Board_Layer();
        $x.= $this->Get_Page_Number_Layer();
        $x.= $this->Get_Tool_Voice_Board_Short_Layer();
        $x.= $this->Get_Bookmark_Image_Layer("bookmark_img_left");
        $x.= $this->Get_Bookmark_Image_Layer("bookmark_img_right");

        return $x;
    }


    // PageMode: 1 or 2
    function Get_Top_Tool_Layer($PageMode=2)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang, $userBrowser, $special_feature ; // 2013-08-19 Charles Ma

        $browsertype = $userBrowser->browsertype;
        $btn_style = '';
        if($browsertype == 'MSIE'){
            $btn_style = 'style="display:none;"';
        }

        $today = strtotime(date("Y-m-d"));
        $expiration_date =  $_SESSION['ss_intranet_plugin']['power_speech_expire'] == "" ? "" : strtotime($_SESSION['ss_intranet_plugin']['power_speech_expire']);
        $elib_expiration_date =  $_SESSION['ss_intranet_plugin']['elib_power_speech_expire'] == "" ? "" : strtotime($_SESSION['ss_intranet_plugin']['elib_power_speech_expire']);

        // $isNotExpired = ( $today < $expiration_date && $_SESSION['ss_intranet_plugin']['power_speech']) || ( $today < $elib_expiration_date && $_SESSION['ss_intranet_plugin']['elib_power_speech']);
        $isNotExpired = (check_powerspeech_is_expired()!=2 && $_SESSION['ss_intranet_plugin']['power_speech'])|| ( $today < $elib_expiration_date && $_SESSION['ss_intranet_plugin']['elib_power_speech']);

        // 2013-08-19 Charles Ma
        $texttospeech = !$special_feature['emag'] && $isNotExpired && $_SESSION['UserType']!=USERTYPE_ALUMNI ? '<a href="javascript:void(0);" id="btn_texttospeech" class="btn_texttospeech" title="'.$Lang['eBookReader']['TextToSpeech'].'" '.$btn_style.'>&nbsp;</a>' : '<a style="display:none" href="javascript:void(0);" id="btn_texttospeech" class="btn_texttospeech" title="'.$Lang['eBookReader']['TextToSpeech'].'" '.$btn_style.'>&nbsp;</a>';
        $texttospeech1 = !$special_feature['emag'] && $isNotExpired && $_SESSION['UserType']!=USERTYPE_ALUMNI ? '<a href="javascript:void(0);" class="btn_texttospeech" id="btn_texttospeech_pdf_left" title=""  style="display:none">&nbsp;</a>' : '<a style="display:none" href="javascript:void(0);" class="btn_texttospeech" id="btn_texttospeech_pdf_left" title=""  style="display:none">&nbsp;</a>' ;
        $texttospeech2 = !$special_feature['emag'] && $isNotExpired && $_SESSION['UserType']!=USERTYPE_ALUMNI ? '<a href="javascript:void(0);" class="btn_texttospeech" id="btn_texttospeech_pdf_left" title="" style="display:none">&nbsp;</a>' : '<a style="display:none" href="javascript:void(0);" class="btn_texttospeech" id="btn_texttospeech_pdf_left" title="" style="display:none">&nbsp;</a>';
        $texttospeech3 = !$special_feature['emag'] && $isNotExpired && $_SESSION['UserType']!=USERTYPE_ALUMNI ? '<a href="javascript:void(0);" class="btn_texttospeech" id="btn_texttospeech_pdf_right" title="" style="display:none">&nbsp;</a>' : '<a style="display:none" href="javascript:void(0);" class="btn_texttospeech" id="btn_texttospeech_pdf_right" title="" style="display:none">&nbsp;</a>';
        $texttospeech4 = !$special_feature['emag'] && $isNotExpired && $_SESSION['UserType']!=USERTYPE_ALUMNI ? '<a href="javascript:void(0);" id="btn_texttospeech" class="btn_texttospeech" title="'.$Lang['eBookReader']['TextToSpeech'].'" '.$btn_style.'>&nbsp;</a>' : '<a style="display:none" href="javascript:void(0);" id="btn_texttospeech" class="btn_texttospeech" title="'.$Lang['eBookReader']['TextToSpeech'].'" '.$btn_style.'>&nbsp;</a>';

        $mp3_player = '<div id="mp3_box" style=" display: none; position: absolute;" ><ul></ul></div>';

        $mp3 =  '<a href="javascript:void(0);" id="btn_mp3" class="btn_texttospeech" title="" '.$btn_style.'>&nbsp;</a>' ;
        $mp3_r =  '<a href="javascript:void(0);" id="btn_mp3_r" class="btn_texttospeech" title="" '.$btn_style.'>&nbsp;</a>' ;

        /////20140811-tw

        $mp3_playertw = '<div id="mp3_box" class="player_board" style=" display: none; position: absolute;" >' .
            '<audio id="mp3_player" controls> <source id="mp3_src" type="audio/mp3" />Your browser does not support the audio element.</audio>' .
            '<div><ul></ul></div></div>';

        $mp3tw =  '<a href="javascript:void(0);" id="btn_mp3_tw" class="btn_speech" title="" '.$btn_style.'>&nbsp;</a>' ;


        // 20140422-cupbook
        $cupleft_mp3 =  '<a href="javascript:void(0);" id="btn_cupleft_mp3" class="btn_speech" title="" '.$btn_style.'>&nbsp;</a>' .
            '<!-- player_board start --><div id="player_board1" class="player_board">
		                      <p class="spacer">
		                      </p><div><audio id="cup_mp3_player" class="cup_mp3_player" controls> <source id="cup_mp3_src" type="audio/mp3" />Your browser does not support the audio element.</audio>
		                          <ul>
		                          </ul>
		                      </div>
						</div><!-- player_board end -->' ;
        $cupright_mp3 =  '<a href="javascript:void(0);" id="btn_cupright_mp3" class="btn_speech" title="" '.$btn_style.'>&nbsp;</a>' .
            '<!-- player_board start --><div id="player_board2" class="player_board">
                   	  
                      <p class="spacer">
                      </p><div><audio id="cup_mp3_player_r" class="cup_mp3_player" controls> <source id="cup_mp3_src_r" type="audio/mp3" />Your browser does not support the audio element.</audio>
                          <ul>
                          </ul>
                      </div>
				</div><!-- player_board end -->' ;

        $cup_mp3_player = '<div id="cup_mp3_box" style=" display: none; position: absolute;" ><ul></ul></div>';

//		$glossary = '<a class="btn_glossary glossary_btn" style=" display: none; position: absolute;" >&nbsp;</a>';
//		$glossary = '<a href="javascript:void(0);" id="btn_glossary" class="btn_glossary glossary_btn" title="'.$Lang['eBookReader']['ShowTools'].'">&nbsp;</a> ';

        $glossary = '<a href="javascript:void(0);" id="btn_feature" class="btn_feature" title="'.$Lang['eBookReader']['ShowTools'].'">&nbsp;</a> ';
        $glossaryl = '<a href="javascript:void(0);" id="btn_glossary_left" class="btn_glossary glossary_btn" title="'.$Lang['eBookReader']['ShowTools'].'">&nbsp;</a> ';
        $glossaryr = '<a href="javascript:void(0);" id="btn_glossary_right" class="btn_glossary glossary_btn" title="'.$Lang['eBookReader']['ShowTools'].'">&nbsp;</a> ';

        if($PageMode==1){
            $x = '<div id="tool_top" class="tool_top_1page" style="display:none;">
			    	<div class="tool_top_content">
			    		<div class="tool_top_left">
							<div class="tool_top_right">
								<div class="tool_top_bg">
			        				<div class="tool_1page">
			                			<div class="tool_left">
			                				<a href="javascript:void(0);" id="btn_book_tool" class="btn_book_tool" title="'.$Lang['eBookReader']['ShowTools'].'">&nbsp;</a>
											<a href="javascript:void(0);" class="btn_add_notes" id="btn_add_notes_left" title="'.$Lang['eBookReader']['AddNotesToThisPage'].'" '.$btn_style.'>&nbsp;</a>
											<a href="javascript:void(0);" class="btn_add_bookmarkes" id="btn_bookmark_left" title="'.$Lang['eBookReader']['BookmarkThisPage'].'" '.$btn_style.'>&nbsp;</a>
											'.$glossaryl.$glossary.$texttospeech1.$mp3.$mp3tw.$cup_mp3_player.'
			                			</div>
			                 			<div class="tool_right">
			                				<a href="javascript:void(0);" class="btn_book_close" id="btn_book_close" title="'.$Lang['eBookReader']['CloseToolbar'].'">&nbsp;</a>
											<a href="javascript:void(0);" class="btn_book_setting" id="btn_book_setting" title="'.$Lang['eBookReader']['Settings'].'" >&nbsp;</a>
											<a href="javascript:void(0);" class="btn_zoomin" id="btn_book_enlarge" title="'.$Lang['eBookReader']['Settings'].'" >&nbsp;</a>
											<a href="javascript:void(0);" class="btn_zoomout" id="btn_book_diminish" title="'.$Lang['eBookReader']['Settings'].'" >&nbsp;</a>
											'.$texttospeech.$cupright_mp3.'
			                			</div>
			           				</div>
			        			</div>
							</div>
						</div>
			        </div>
			     </div>'."\n";
        }else{
            $x = '<div id="tool_top" class="tool_top_'.$PageMode.'page" style="display:none;">'.$mp3_player.'
			    	<div class="tool_top_content">
			    		<div class="tool_top_left">
							<div class="tool_top_right">
								<div class="tool_top_bg">
			        				<div class="tool_'.$PageMode.'page_left">
			                			<div class="tool_left">
			                				<a href="javascript:void(0);" id="btn_book_tool" class="btn_book_tool" title="'.$Lang['eBookReader']['ShowTools'].'">&nbsp;</a> 
			                				'.$glossary.'
			                			</div>
			                 			<div class="tool_right">
			                				<a href="javascript:void(0);" class="btn_add_bookmarkes" id="btn_bookmark_left" title="'.$Lang['eBookReader']['BookmarkThisPage'].'" '.$btn_style.'>&nbsp;</a> <a href="javascript:void(0);" class="btn_add_notes" id="btn_add_notes_left" title="'.$Lang['eBookReader']['AddNotesToThisPage'].'" '.$btn_style.'>&nbsp;</a>
			                				'.$glossaryl.$texttospeech2.$mp3.$mp3tw.$cupleft_mp3.$cup_mp3_player.$mp3_playertw.'
			                			</div>
			           				</div>
			           				<div class="tool_'.$PageMode.'page_right">
			                			<div class="tool_left">
			                				<a href="javascript:void(0);" class="btn_add_bookmarkes" id="btn_bookmark_right" title="'.$Lang['eBookReader']['BookmarkThisPage'].'" '.$btn_style.'>&nbsp;</a> <a href="javascript:void(0);" class="btn_add_notes" id="btn_add_notes_right" title="'.$Lang['eBookReader']['AddNotesToThisPage'].'" '.$btn_style.'>&nbsp;</a>
			                				'.$glossaryr.$texttospeech3.$mp3_r.$cupright_mp3.'
			                			</div>
			                 			<div class="tool_right">
			                				<a href="javascript:void(0);" id="btn_book_close" class="btn_book_close" title="'.$Lang['eBookReader']['CloseToolbar'].'">&nbsp;</a>
			                				 <a href="javascript:void(0);" id="btn_book_setting" class="btn_book_setting" title="'.$Lang['eBookReader']['Settings'].'" >&nbsp;</a>'.$texttospeech4.'
			                				 <a href="javascript:void(0);" class="btn_zoomin" id="btn_book_enlarge" title="'.$Lang['eBookReader']['Settings'].'" >&nbsp;</a>
											<a href="javascript:void(0);" class="btn_zoomout" id="btn_book_diminish" title="'.$Lang['eBookReader']['Settings'].'" >&nbsp;</a>
			                			</div>
			           				</div>
			        			</div>
							</div>
						</div>
			        </div>
			    </div>'."\n";
        }
        return $x;
    }

    function Get_Bottom_Tool_Layer($PageMode=2)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_bottom" class="tool_bottom_'.$PageMode.'page" style="display:none;">
			  	<div class="tool_bottom_content">
			    	<div class="tool_bottom_left">
						<div class="tool_bottom_right">
							<div class="tool_bottom_bg">
			       	  			<div class="tool_page_panel">
			            			<div class="tool_page_left">
										<a href="javascript:void(0);" id="btn_toc" class="btn_tableofcontent" title="'.$Lang['eBookReader']['TableOfContents'].'">&nbsp;</a>
									</div>
			                		<div class="tool_page_right">
										<a href="javascript:void(0);" id="btn_search" class="btn_search" title="'.$Lang['eBookReader']['Search'].'">&nbsp;</a>
									</div>
			              			<div id="tool_page_select" class="tool_page_select">
									<!--	<a href="javascript:void(0);" class="btn_select_page" title="Jump to selected page">&nbsp;</a>-->
									</div>
			        			</div>
							</div>
						</div>
			    	</div>
			  	</div>
			  </div>'."\n";

        return $x;
    }

    function Get_Book_Content_Layer($PageMode=2)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        if($PageMode==1){
            $x = '<div id="book_content" class="book_1page_content" >
	    			<div class="book_content_container" >
	    				<div class="book_info_top_right" id="book_content_title">
						</div>
						<div id="book_content_main" class="book_content_main">
	      				</div>
	        			<div class="book_info_bottom_right">
							<span id="book_right_page_number"></span>
						</div>
					</div><div id="book_note_main_container"></div>
	    		  </div>';
        }else{
            $x = '<div id="book_content" class="book_'.$PageMode.'page_content">
				    <div class="book_content_container">
				    	<div class="book_info_top_left">
						</div>
				    	<div class="book_info_top_right">
						</div>
				   	  	<div id="book_content_main" class="book_content_main">
				 		</div>
				        <div class="book_info_bottom_left"><span id="book_left_page_number"></span></div>
						<div class="book_info_bottom_right"><span id="book_right_page_number"></span></div>
					</div><div id="book_note_main_container"></div>
				  </div>';
        }
        $x .= '<div id="btn_prev_page" style="width:35px;height:50px;z-index:2;position:absolute;left:0px;top:0px;background-color:#ffffff;opacity:0;cursor:pointer;" title="'.$Lang['eBookReader']['PreviousPage'].'"></div>
			   <div id="btn_next_page" style="width:45px;height:50px;z-index:2;position:absolute;left:0px;top:0px;background-color:#ffffff;opacity:0;cursor:pointer;" title="'.$Lang['eBookReader']['NextPage'].'"></div>';
        $x .= '<div id="btn_top_tool_bar" style="position:absolute;left:0px;top:0px;z-index:5;background-color:#ffffff;opacity:0;cursor:pointer;" title="'.$Lang['eBookReader']['ShowTools'].'"></div>';
        $x .= '<div id="btn_bottom_tool_bar" style="position:absolute;left:0px;top:0px;z-index:5;background-color:#ffffff;opacity:0;cursor:pointer;" title="'.$Lang['eBookReader']['ShowTools'].'"></div>';

        return $x;
    }

    // IE special one page mode
    function Get_Book_Content_Layer_IE()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="book_content" class="book_1page_content" >
    			<div class="book_content_container" >
    				<div class="book_info_top_right" id="book_content_title">
					</div>
					<div id="book_content_main" style="overflow-x: hidden; overflow-y:auto;">
      				</div>
        			<div class="book_info_bottom_right">
						<span id="book_right_page_number"></span>
					</div>
				</div>
    		  </div>';

        $x .= '<div id="btn_prev_page" style="width:45px;height:50px;z-index:2;position:absolute;left:0px;top:0px;background-color:#ffffff;opacity:0;cursor:pointer;" title="'.$Lang['eBookReader']['PreviousPage'].'"></div>
			   <div id="btn_next_page" style="width:50px;height:50px;z-index:2;position:absolute;left:0px;top:0px;background-color:#ffffff;opacity:0;cursor:pointer;" title="'.$Lang['eBookReader']['NextPage'].'"></div>';
        $x .= '<div id="btn_top_tool_bar" style="position:absolute;left:0px;top:0px;z-index:5;background-color:#ffffff;opacity:0;cursor:pointer;" title="'.$Lang['eBookReader']['ShowTools'].'"></div>';
        $x .= '<div id="btn_bottom_tool_bar" style="position:absolute;left:0px;top:0px;z-index:5;background-color:#ffffff;opacity:0;cursor:pointer;" title="'.$Lang['eBookReader']['ShowTools'].'"></div>';

        return $x;
    }

    function Get_Book_BG_Structure_Layer($PageMode=2)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $paperStyle = empty($_SESSION['eBookReaderPaperStyle'])? 'white' : $_SESSION['eBookReaderPaperStyle'];
        if($PageMode==1){
            $x = $this->Get_Book_BG_Structure_Layer_IE();
        }else{
            $search_x = '<div id="tool_page_related" class="tool_big_related_board" style="display:none">
						<div class="tool_big_related_board_top"><div class="tool_big_related_board_top_right"><div class="tool_big_related_board_top_bg">
							  <div id="related_result_close" class="tool_right"><a href="javascript:void(0)" class="btn_common_close"></a></div>  	
					    </div></div></div>
					    <div class="tool_big_related_board_content"><div class="tool_big_related_board_content_right"><div class="tool_big_related_board_content_bg">
					      <div class="related_result"> <span id="related_result_num" class="related_result_num"></span>
					          <ul>
					            
					          </ul>
					      </div>
					    </div>
					    </div></div>
					    <div class="tool_big_related_board_bottom"><div class="tool_big_related_board_bottom_right"><div class="tool_big_related_board_bottom_bg"></div></div></div>
					</div>';

            $x = '<div id="book_structure" class="book_'.$PageMode.'page_structure">'.$search_x.'		        	
					<div id="btn_return" class="tool_back_to_page" style="display:none;">
						<div class="btn_back_to_page"><a href="#"><span class="icon_back_to_page"></span>Page <span id="inner_page_num"></span></a></div>
						<div class="btn_back_to_page_right"><a href="#" class="btn_common_close"></a></div>
					</div>
					<div id="book_page_style" class="book_'.$PageMode.'page_left_style  page_left_'.$paperStyle.'_style ">
			        	<div class="page_top">
							<div class="page_top_right">
								<div class="page_top_bg">
								</div>
							</div>
						</div>
			            <div class="page_content">
							<div class="page_content_right" 6>
								<div class="page_content_bg" >
			            			<div class="page_pattern" style="background-image:url(book/001/page_pattern_L.png)">&nbsp;
									</div>
			            		</div>
							</div>
						</div>
			            <div class="page_bottom">
							<div class="page_bottom_right">
								<div class="page_bottom_bg">
			         				&nbsp;
			            		</div>
							</div>
						</div>
					</div>
			        <div id="book_page_style" class="book_'.$PageMode.'page_right_style page_right_'.$paperStyle.'_style ">
			        	<div class="page_top">
							<div class="page_top_right">
								<div class="page_top_bg">
								</div>
							</div>
						</div>
			            <div class="page_content">
							<div class="page_content_right" 5>
								<div class="page_content_bg">
			            			<div class="page_pattern" style="background-image:url(book/001/page_pattern_R.png)">&nbsp;
									</div>
			            		</div>
							</div>
						</div>
			            <div class="page_bottom">
							<div class="page_bottom_right">
								<div class="page_bottom_bg">
								</div>
							</div>
						</div>
					</div>
				</div>
			    <p class="spacer"></p>'."\n";
        }
        return $x;
    }
    // One Page Mode
    function Get_Book_BG_Structure_Layer_IE()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $paperStyle = empty($_SESSION['eBookReaderPaperStyle'])? 'white' : $_SESSION['eBookReaderPaperStyle'];
        $search_x = '<div id="tool_page_related" class="tool_big_related_board" style="display:none;font-size: 16px;width:650px">
			<div class="tool_big_related_board_top"><div class="tool_big_related_board_top_right"><div class="tool_big_related_board_top_bg">
				  <div id="related_result_close" class="tool_right"><a href="javascript:void(0)" class="btn_common_close"></a></div>  	
		    </div></div></div>
		    <div class="tool_big_related_board_content"><div class="tool_big_related_board_content_right"><div class="tool_big_related_board_content_bg">
		      <div class="related_result"> <span id="related_result_num" class="related_result_num"></span>
		          <ul>
		            
		          </ul>
		      </div>
		    </div>
		    </div></div>
		    <div class="tool_big_related_board_bottom"><div class="tool_big_related_board_bottom_right"><div class="tool_big_related_board_bottom_bg"></div></div></div>
		</div>';

        $x = '<div id="book_structure" class="book_1page_structure">'.$search_x.
            '<div id="btn_return" class="tool_back_to_page" style="display:none;">
						<div class="btn_back_to_page"><a href="#"><span class="icon_back_to_page"></span>Page <span id="inner_page_num"></span></a></div>
						<div class="btn_back_to_page_right"><a href="#" class="btn_common_close"></a></div>
					</div>
				<div id="book_page_style" class="book_1page_style page_right_'.$paperStyle.'_style">
        			<div class="page_top">
						<div class="page_top_right">
							<div class="page_top_bg">
							</div>
						</div>
					</div>
            		<div class="page_content">
						<div class="page_content_right" 4>
							<div class="page_content_bg">
            					<div class="page_pattern" style="background-image:url(book/001/page_pattern_R.png)">
									&nbsp;
								</div>
            				</div>
						</div>
					</div>
            		<div class="page_bottom">
						<div class="page_bottom_right">
							<div class="page_bottom_bg">
							</div>
						</div>
					</div>
				</div>
			</div>';
        $x.='<p class="spacer"></p>'."\n";

        return $x;
    }

    function Get_Cover_Image_Layer($cover_image_path, $PageMode=2, $cover_width='', $cover_height='')
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        //$x = '<div class="ebook_cover_image" onclick="$(this).remove();">';
        //$x.= '<img id="ebook_cover_image" src="'.$cover_image_path.'" />';
        //$x.='</div>'."\n";

        $css_dimension = '';
        //if($cover_width != '' && $cover_height != ''){
        //	$css_dimension = 'width:'.$cover_width.'px;height:'.$cover_height.'px;';
        //}

        $x = '<div class="book_'.$PageMode.'page_cover_structure" id="book_cover_image" '.($css_dimension!=''?'style="'.$css_dimension.'"':'').'>
        		<div class="book_'.$PageMode.'page_cover_style book_cover_page_style " id="book_page_style">
        			<div class="page_top">
						<div class="page_top_right">
							<div class="page_top_bg">
							</div>
						</div>
					</div>
            		<div class="page_content">
						<div class="page_content_right" 3>
							<div class="page_content_bg">';
        /*  $x.= '<div style="background-image:url('.$cover_image_path.');'.$css_dimension.'" class="page_pattern">
                    <!--temp link start-->
                    <a style="display:block; height:200px;" href="javascript:void(0);">&nbsp;</a>
                    <!--temp link end-->
                </div>'; */
        $x.= '<div id="book_cover_page_pattern" class="page_pattern">
									<img id="book_cover" src="'.$cover_image_path.'" width="100%" height="100%" />
								</div>';
        $x.='</div>
						</div>
					</div>
            		<div class="page_bottom">
						<div class="page_bottom_right">
							<div class="page_bottom_bg">
							</div>
						</div>
					</div>
				</div>
			</div>';
        $x.= '<div id="ebook_transparent_block"></div>';

        return $x;
    }

    function tidyupXmlText($text)
    {
        return htmlspecialchars(nl2br($text),ENT_QUOTES);
    }

    function stripHtmlLinkAnchor($link)
    {
        $pos = strpos($link, '#');
        if($pos !== FALSE)
            $stripped_link = substr($link,0,$pos);
        else
            $stripped_link = $link;
        return $stripped_link;
    }

    function encrypt($str){
        $Encrypt1 = base64_encode($str);
        $MidPos = ceil(strlen($Encrypt1)/2);
        $Encrypta = substr($Encrypt1, 0, $MidPos);
        $Encryptb = substr($Encrypt1, $MidPos);

        return base64_encode($Encryptb.$Encrypta."=".(strlen($Encrypt1)-$MidPos));
    }

    function decrypt($str){
        $Encrypt1 = base64_decode($str);
        $SplitSizePos = strrpos($Encrypt1, "=");
        $MidPos = (int) substr($Encrypt1, $SplitSizePos+1);
        $Encrypt1 = substr($Encrypt1, 0, $SplitSizePos);
        $Encrypta = substr($Encrypt1, 0, $MidPos);
        $Decryptb = substr($Encrypt1, $MidPos);

        return base64_decode($Decryptb.$Encrypta);
    }

    function Get_Table_of_Content_Layer($BookID, $PageMode=2, $ImageBook=false, $ImageWidth='')
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $libebookreader = $this->Get_libebookreader();

        $book_info = $libebookreader->Get_Book_List($BookID);

        // 2013-05-29 eBook-update-130529c
        $book_elibinfo = $libebookreader->Get_eLib_Book_Info($BookID);

        $Title = $book_elibinfo[0]['Title'];
        $Language = $book_elibinfo[0]['Language'];	// 20140115-storybooklang
        //if((strlen($Title) != strlen(utf8_decode($Title)))){
        if($Language != "eng"){
            include_once($intranet_root.'/lang/elib_lang.b5.php');
        }else{
            include_once($intranet_root.'/lang/elib_lang.en.php');
        }
        $Level = $book_elibinfo[0]['Level'] || $book_elibinfo[0]['Level']=="0"  ? $eLib['Book']["Level"]." ".$book_elibinfo[0]['Level'] : "";
        $Category = $book_elibinfo[0]['Category'];
        $Publisher = $book_elibinfo[0]['Publisher'];
        $Author = $book_elibinfo[0]['Author'];
        $SubCategory = $book_elibinfo[0]['SubCategory'];

        $FirstPublished = $book_elibinfo[0]['FirstPublished'];
        $ePublisher = $book_elibinfo[0]['ePublisher'];
        $ISBN = $book_elibinfo[0]['ISBN'];
        $CopyrightYear = trim($book_elibinfo[0]['CopyrightYear']); // 2013-08-08 Charles Ma
        $CopyrightStatement = $book_elibinfo[0]['CopyrightStatement'];
        // 2013-05-29 eBook-update-130529c 	END

        $book_path = $intranet_root.$book_info[0]['BookPath'];
        $book_sub_folder = current(explode("/",$book_info[0]['BookSubFolder']));

        $engclassicMode = $Publisher=="English Classics"||$SubCategory=="English Classics";
        $youngReaders = (($Category=="eClass Readers"&&$SubCategory=="Young Readers") || $SubCategory=="和爺爺一起過的那些傳統節日");

        if($ISBN != "")
            $ISBN_html = " <li><span>".$eLib['Book']["ISBN"]." </span>: $ISBN</li>";
        else $ISBN_html = "";

        ////////////////2013-07-12 Charles Ma
        if($book_info[0]['InputFormat'] != 4)
            $parse_success = $libebookreader->Parse_EPUB_XML($book_path);
        else{
            $libebookreader->GET_NEW_TABLE_OF_CONTENT($BookID, $book_sub_folder);
            $table_of_content = $libebookreader->table_of_content;

            $page_index = $libebookreader->Get_Page_Index($BookID,$PageMode);
            $hrefToIndex = array();
            for($i=1;$i<=count($page_index);$i++){
                if(!isset($hrefToIndex[$page_index[$i]['href']])){
                    if($ImageBook || $ImageBook == '1'){
                        $hrefToIndex[$page_index[$i]['href']] = $i;
                    }
                    else{
                        $hrefToIndex[$page_index[$i]['href']] = ($PageMode == 2? ($i * 2 - 1) : $i);
                    }
                }
            }
            if(!isset($table_of_content['navPoint'][0]) && $youngReaders==true){
                $print_content = $this->Get_Book_Description($Title, $BookID, $ImageBook);
            }else{
                $print_content = $this->Get_Table_of_Content($hrefToIndex, $Title,$table_of_content['navPoint'],2,$ImageBook);
            }
        }
        ////////////////2013-07-12 Charles Ma
        if($parse_success){
            $dcTitle = $libebookreader->metadata_array['dc:title'];
            $dcCreator = $libebookreader->metadata_array['dc:creator'];
            $dcPublisher = $libebookreader->metadata_array['dc:publisher'];
            $dcRights = $libebookreader->metadata_array['dc:rights'];
            $dcDate = $libebookreader->metadata_array['dc:date'];
            $dcSubject = $libebookreader->metadata_array['dc:subject']; // book category
            $dcInfo = $libebookreader->metadata_array['dc:description']; // book introduction
            $dcLanguage = strtolower($libebookreader->metadata_array['dc:language']); // book language
            if($dcLanguage == 'en'){
                include_once($intranet_root.'/lang/ebook_reader_lang.en.php');
            }else if($dcLanguage == 'zh' || $dcLanguage == 'b5'){
                include_once($intranet_root.'/lang/ebook_reader_lang.b5.php');
            }


            //$elib_book_info = $libebookreader->Get_eLib_Book_Info($BookID);
            $tagName = $libebookreader->Get_eLib_TagName_By_BookID($BookID);
            $tagName = trim($tagName);
            if($tagName==''){
                $tagName = '--';
            }

            $page_index = $libebookreader->Get_Page_Index($BookID,$PageMode);
            $hrefToIndex = array();
            for($i=1;$i<=count($page_index);$i++){
                if(!isset($hrefToIndex[$page_index[$i]['href']])){
                    if($ImageBook || $ImageBook == '1'){
                        $hrefToIndex[$page_index[$i]['href']] = $i;
                    }else{
                        $hrefToIndex[$page_index[$i]['href']] = ($PageMode == 2? ($i * 2 - 1) : $i);
                    }
                }
            }

            $table_of_content = $libebookreader->table_of_content;
            if(!isset($table_of_content['navPoint'][0]) && $youngReaders==true){
                $print_content = $this->Get_Book_Description($dcTitle, $BookID, $ImageBook);
            }else{
                //$print_content = $this->Get_Table_of_Content($hrefToIndex, $table_of_content['docTitle'],$table_of_content['navPoint'],2,$ImageBook);
                $print_content = $this->Get_Table_of_Content($hrefToIndex, $dcTitle,$table_of_content['navPoint'],2,$ImageBook);
            }
        }
        $css_width = ($ImageBook && $ImageWidth != '' && $ImageWidth > 0) ? ("style='width:".($ImageWidth * 2)."px;'"): "";

        // 2013-08-08 Charles Ma
        if($Title == "" && $dcTitle != "" )	$Title = $dcTitle;
        if($Category == "" && $dcSubject != "" ) 	$Category = $dcSubject;
        if($Publisher == "" && $dcPublisher != "" ) $Publisher = $dcPublisher;
        if($CopyrightStatement == "" && $dcRights != "" ) 	$CopyrightStatement = $dcRights;
        // 2013-08-08 END

        if($Publisher != "")
            $Publisher_html = " <li><span>".$eLib['Book']["Publisher"]."</span><em>:</em> $Publisher</li>";
        else $Publisher_html = "";
        if($ePublisher != "")
            $ePublisher_html = " <li><span>".$eLib['Book']["ePublisher"]." </span><em>:</em> $ePublisher</li>";
        else $ePublisher_html = "";
        if($FirstPublished != "")
            $FirstPublished_html = " <li><span>".$eLib['Book']["FirstPublished"]." </span><em>:</em> $FirstPublished</li>";
        else $FirstPublished_html = "";

        if (file_exists($intranet_root."/file/elibrary/content/$BookID/worksheet.pdf")) {

            if($_SESSION['UserType'] == USERTYPE_STAFF){
                $encode_entry_id = $this->encrypt($BookID);
                $worksheet_html = '<div class="download_worksheet download_worksheet_2btn">
                        	<a  target="_blank" href="/file/elibrary/content/'.$BookID.'/worksheet.pdf" title="Download Worksheet" class="btn_download_worksheet_normal"></a>
                            <a  target="_blank" href="/file/elibrary/content/'.$BookID.'/worksheetans_'.$encode_entry_id.'.pdf" title="Download Worksheet(with Answers)" class="btn_download_worksheet_answer"></a>
                        </div>';
            }else{
                $worksheet_html = '<div class="download_worksheet">
                        	<a target="_blank" href="/file/elibrary/content/'.$BookID.'/worksheet.pdf" title="Download Worksheet"><em></em></a>
                        </div>';
            }
            $copyright_style = '';
        }else{
            $worksheet_html = '';
            $copyright_style = ' style= "padding-right: 70px" ';
        }

        if($CopyrightYear != "")
            $copyright_html = '<div class="book_copyright" '.$copyright_style.'> 
                        '.$CopyrightYear.'
				<span>'.$CopyrightStatement.'</span>
                        </div>';
        else $copyright_html = '<div class="book_copyright" '.$copyright_style.'>
				<span>'.$CopyrightStatement.'</span>
                        </div>';
        $info_width = $ImageWidth >=500 || $ImageWidth == "" || $ImageWidth == 0 ? 400 : $ImageWidth - 85;

        $x = '<div id="book_table_of_content" class="book_'.$PageMode.'page_first" style="z-index:4;position:absolute;left:0px;top:0px;">
			    <div class="book_content_container" '.$css_width.'>';
        if (file_exists($intranet_root."/file/elibrary/content/$BookID/image/indexpageimage.png")) {
            $img_src = 'src="/file/elibrary/content/'.$BookID.'/image/indexpageimage.png"';
        }else{
            $img_src = 'src="/images/2009a/ebook_reader/info_pic_temp.png"';
        }

        // 2013-11-12 Charles Ma

        if (file_exists($intranet_root."/file/elibrary/content/$BookID/dlpdf.pdf")) {
            $dlpdf_html = '<div class="download_pdf download_pdf_small">
                        	<a target="_blank" href="/file/elibrary/content/'.$BookID.'/dlpdf.pdf" title="Download PDF Version"><em></em></a>
                        </div>';
        }else{
            $dlpdf_html = '';
        }

        $x .= '<div class="book_info_detail_page"> 
            	<div class="book_info_detail_story book_info_worksheet" style="width: '.$info_width.'px;margin-left: auto;">
                	<h1>'.$Title.'</h1>
                		<div class="book_author">'.$Author.'</div>
                    	<div class="book_cat">'.$Category.'</div>
                        <div class="book_level">'.$Level.'</div>
                        <div class="book_info_pic"><img '.$img_src.'>'.$dlpdf_html.'</div>
                        <div class="book_publish_detail">
                        	<ul>
                            	'.$FirstPublished_html.'
                                '.$Publisher_html.'
                                '.$ePublisher_html.'
                                '.$ISBN_html.'
           					</ul>
         
                        </div>
                        '.$copyright_html.'

                 		 '.$worksheet_html.'           ';
        // 2013-11-12 Charles Ma END

        $x.='        	</div>
		            </div>';
        $x.= $print_content;
        /*
              $x.= '<div class="book_toc_list_page">
                        <div class="book_info_top_right">Book Title
                        </div>
                        <div class="book_toc_list_page_detail">
                            <h1>Table of Contents</h1>
                            <ul>
                                <li><a href="#"><span><u>Chapter 1</u></span><em> 1</em></a></li>
                                <li><a href="#"><span><u>Chapter 2</u></span><em> 15</em></a></li>
                                <li><a href="#"><span><u>Chapter 3</u></span><em> 36</em></a></li>
                            </ul>
                        </div>
                    </div>';
        */
        $x.= '</div>';
        $x.= '</div>'."\n";
        if($book_info[0]['InputFormat'] == 4)
            $libebookreader->CoverImageName = 'images/'.$book_sub_folder.'_0001.jpg';
        if($libebookreader->CoverImageName != ''){
            /*	include_once("libimage.php");
                $libimage = new SimpleImage();
                $desire_width = 457;
                $desire_height = 672;
                $cover_image_path = $intranet_root.$libebookreader->CoverImagePath;
                $libimage->load($cover_image_path);
                $cover_width = $libimage->getWidth();
                $cover_height = $libimage->getHeight();

                $real_width = $desire_width;
                $real_height = ($real_width / $cover_width) * $cover_height;
                */
            $x .= $this->Get_Cover_Image_Layer('/home/eLearning/ebook_reader/'.$BookID.'/reader/'.$libebookreader->CoverImageName, $PageMode);
        }

        return $x;
    }

    function Get_Table_of_Content_Layer_IE($BookID, $PageMode=1, $ImageBook=false, $ImageWidth='')
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang, $userBrowser;

        $libebookreader = $this->Get_libebookreader();

        $book_info = $libebookreader->Get_Book_List($BookID);
        // 2013-05-29 eBook-update-130529c
        $book_elibinfo = $libebookreader->Get_eLib_Book_Info($BookID);

        $Title = $book_elibinfo[0]['Title'];

        $Language = $book_elibinfo[0]['Language'];	// 20140115-storybooklang
        //if((strlen($Title) != strlen(utf8_decode($Title)))){
        if($Language != "eng"){
            include_once($intranet_root.'/lang/elib_lang.b5.php');
        }else{
            include_once($intranet_root.'/lang/elib_lang.en.php');
        }
        $Level = $book_elibinfo[0]['Level'] || $book_elibinfo[0]['Level']=="0" ? $eLib['Book']["Level"]." ".$book_elibinfo[0]['Level'] : "";
        $Category = $book_elibinfo[0]['Category'];
        $SubCategory = $book_elibinfo[0]['SubCategory']; //2013-09-18 Charles Ma
        $Publisher = $book_elibinfo[0]['Publisher'];
        $Author = $book_elibinfo[0]['Author'];
        $ISBN = $book_elibinfo[0]['ISBN'];

        $cupMode = $Publisher=="Cambridge University Press";
        $engclassicMode = $Publisher=="English Classics"||$SubCategory=="English Classics";
        $youngReaders = (($Category=="eClass Readers"&&$SubCategory=="Young Readers") || $SubCategory=="和爺爺一起過的那些傳統節日");

        $FirstPublished = $book_elibinfo[0]['FirstPublished'];
        $ePublisher = $book_elibinfo[0]['ePublisher'];
        $CopyrightYear = trim($book_elibinfo[0]['CopyrightYear']); // 2013-08-08 Charles Ma
        $CopyrightStatement = $book_elibinfo[0]['CopyrightStatement'];
        // 2013-05-29 eBook-update-130529c 	END

        $ForceToOnePageMode = $book_info[0]['ForceToOnePageMode'];
        $book_path = $intranet_root.$book_info[0]['BookPath'];
        $book_sub_folder = current(explode("/",$book_info[0]['BookSubFolder']));

        if($ISBN != "")
            $ISBN_html = " <li><span>".$eLib['Book']["ISBN"]." </span>: $ISBN</li>";
        else $ISBN_html = "";

        ////////////////2013-07-12 Charles Ma
        if($book_info[0]['InputFormat'] != 4)
            $parse_success = $libebookreader->Parse_EPUB_XML($book_path);
        ////////////////2013-07-12 Charles Ma
        if($parse_success){
            $dcTitle = $libebookreader->metadata_array['dc:title'];
            $dcCreator = $libebookreader->metadata_array['dc:creator'];
            $dcPublisher = $libebookreader->metadata_array['dc:publisher'];
            $dcRights = $libebookreader->metadata_array['dc:rights'];
            $dcDate = $libebookreader->metadata_array['dc:date'];
            $dcSubject = $libebookreader->metadata_array['dc:subject']; // book category
            $dcInfo = $libebookreader->metadata_array['dc:description']; // book introduction
            $dcLanguage = strtolower($libebookreader->metadata_array['dc:language']); // book language
            if($dcLanguage == 'en'){
                include_once($intranet_root.'/lang/ebook_reader_lang.en.php');
            }else if($dcLanguage == 'zh' || $dcLanguage == 'b5'){
                include_once($intranet_root.'/lang/ebook_reader_lang.b5.php');
            }

            //$elib_book_info = $libebookreader->Get_eLib_Book_Info($BookID);
            //$dcRelevantSubject = trim($elib_book_info[0]['RelevantSubject']);
            //if($dcRelevantSubject==''){
            //	$dcRelevantSubject = '--';
            //}
            $tagName = $libebookreader->Get_eLib_TagName_By_BookID($BookID);
            $tagName = trim($tagName);
            if($tagName==''){
                $tagName = '--';
            }

            //$page_index = $libebookreader->Get_Page_Index($BookID,$PageMode);
            if($userBrowser->browsertype == 'MSIE'){
                $page_info_list = $libebookreader->Get_Book_Page_Info($BookID);
                $page_index = array();
                $page_index[] = array('idref'=>'','href'=>'','session'=>''); // pad 0 with dummy item since page count is start from page 1
                for($i=0;$i<count($page_info_list);$i++){
                    $page_index[] = array('idref'=> $page_info_list[$i]['RefID'], 'href' => $page_info_list[$i]['FileName'], 'session' => $i);
                }
                $hrefToIndex = array();
                for($i=1;$i<=count($page_index);$i++){
                    $hrefToIndex[$page_index[$i]['href']] = $i;
                }
            }else{
                $page_index = $libebookreader->Get_Page_Index($BookID,$PageMode);

                $hrefToIndex = array();	$j = 0;
                for($i=1;$i<=count($page_index);$i++){
                    if(!isset($hrefToIndex[$page_index[$i]['href']])){
                        if($ImageBook || $ImageBook == '1'){
                            $hrefToIndex[$page_index[$i]['href']] = $i;
                        }
                        else if ($engclassicMode){
                            $hrefToIndex[$page_index[$i]['href']] = $i * 2 - 1;
                        }
                        else{
                            $hrefToIndex[$page_index[$i]['href']] = ($PageMode == 2? ($i * 2 - 1) : $cupMode ? $i-$j : $i);
                            $j++;
                        }
                    }
                }
            }
            $table_of_content = $libebookreader->table_of_content;
            if(!isset($table_of_content['navPoint'][0]) && $youngReaders==true){
                $print_content = $this->Get_Book_Description($Title, $BookID, $ImageBook);
            }else{
                //$print_content = $this->Get_Table_of_Content($hrefToIndex, $table_of_content['docTitle'],$table_of_content['navPoint'],2,$ImageBook);
                $print_content = $this->Get_Table_of_Content($hrefToIndex, $Title,$table_of_content['navPoint'],2,$ImageBook);
            }
        }

        // 2013-05-29 eBook-update-130529c
        if($ForceToOnePageMode){
            if (file_exists($intranet_root."/file/elibrary/content/$BookID/image/indexpageimage.png")) {
                $img_src = 'src="/file/elibrary/content/'.$BookID.'/image/indexpageimage.png"';
            }else{
                $img_src = 'src="/images/2009a/ebook_reader/info_pic_temp.png"';
            }

            if (file_exists($intranet_root."/file/elibrary/content/$BookID/worksheet.pdf")) {
                if($_SESSION['UserType'] == USERTYPE_STAFF){
                    $encode_entry_id = $this->encrypt($BookID);
                    $worksheet_html = '<div class="download_worksheet download_worksheet_2btn">
                        	<a  target="_blank" href="/file/elibrary/content/'.$BookID.'/worksheet.pdf" title="Download Worksheet" class="btn_download_worksheet_normal"></a>
                            <a  target="_blank" href="/file/elibrary/content/'.$BookID.'/worksheetans_'.$encode_entry_id.'.pdf" title="Download Worksheet(with Answers)" class="btn_download_worksheet_answer"></a>
                        </div>';
                }else{
                    $worksheet_html = '<div class="download_worksheet">
                        	<a target="_blank" href="/file/elibrary/content/'.$BookID.'/worksheet.pdf" title="Download Worksheet"><em></em></a>
                        </div>';
                }
                $copyright_style = '  ';
                $worksheet_class = ' book_info_worksheet ';
            }else if(file_exists($intranet_root."/file/elibrary/content/$BookID/download.pdf")){// 2013-08-19 Charles Ma
                $worksheet_html = '<div class="download_pdf">
                        	<a  target="_blank" href="/file/elibrary/content/'.$BookID.'/download.pdf" title="Download PDF Version" class="btn_download_worksheet_normal"><em></em></a>
                        </div>';
                $copyright_style = '  ';
                $worksheet_class = ' book_info_detail_story book_info_worksheet book_info_learnthenews ';	// 2013-08-19 Charles Ma
            }else{
                $worksheet_html = '';
                $copyright_style = ' style= "padding-right: 70px" ';
                $worksheet_class = '  ';
            }
            // 2013-08-19 Charles Ma
            if($book_info[0]['InputFormat'] == 5){
                $worksheet_class = ' book_info_detail_story book_info_worksheet book_info_learnthenews ';	// 2013-08-19 Charles Ma
            }
            // 2013-08-19 Charles Ma END
            if($Publisher != "")
                $Publisher_html = " <li><span>".$eLib['Book']["Publisher"]." </span>: $Publisher</li>";
            else $Publisher_html = "";

            $x = '<div id="book_table_of_content" class="book_'.$PageMode.'page_first" style="z-index:4;position:absolute;left:0px;top:0px; background-color: rgb(255, 255, 255);
					height: 650px;
					width: 990px;
					margin-top: 5px;
					margin-left: 20px;
					box-shadow: rgb(153, 153, 153) 0px 0px 1px;
					border-top-left-radius: 15px;
					border-top-right-radius: 0px;
					border-bottom-right-radius: 0px;
					border-bottom-left-radius: 15px;">';
            $x .= '<span style="display: block;
						float: left;
						background: url(../../../../../images/2009a/ebook_reader/new_page/page_shadow_right.png) repeat-y left;
						width: 42px;
						height: 650px;
						border-radius: 15px 0 0 15px;
						position: absolute;"></span>';
            $x .= '<div class="book_info_detail_story '.$worksheet_class.'">
                	<h1>'.$Title.'</h1>
                    	<div class="book_cat">'.$SubCategory.'</div>
                        <div class="book_level">'.$Level.'</div>
                        <div class="book_info_pic"><img '.$img_src.'></div>
                        <div class="book_publish_detail">
                        	<ul>
                            	<li><span>'.$eLib['Book']["FirstPublished"].'     </span>: '.$FirstPublished.'</li>
                                '.$Publisher_html.'
                                <li><span>'.$eLib['Book']["ePublisher"].'     </span>: '.$ePublisher.'</li>
                                '.$ISBN_html.'
           					</ul>
         
                        </div>
                        <div class="book_copyright" '.$copyright_style.'>
                        '.$CopyrightYear.'
						<span>'.$CopyrightStatement.'	</span>
                        </div>
						' .   $worksheet_html.'
                 <!-- <div id="book_info_slip">
                   			<ul>
                            	<li><em>Level </em> <span> 3-4</span></li>
                                <li><em>Title </em> <span> Operation Scarfinger</span></li>
                                 <li><em>Aurthor </em> <span> Kate McRae (adapted by BroadLearning)</span></li>
                                 <li><em>ePublisher </em> <span> BroadLearning Education (Asia) Ltd.</span></li>
                                 <li><em>Copyright</em> <span>Witman Publishing Co. (H.K.) Ltd.</span></li>
                            </ul>
                   </div>-->
                
                </div>';
            $x.= '</div>'."\n";
        }else{
            $x = '<div id="book_table_of_content" class="book_ver book_'.$PageMode.'page_first" style="z-index:4;position:absolute;left:0px;top:0px;">
			    <div class="book_content_container" id="book_table_of_content_container">'."\n";

            // 2013-08-08 Charles Ma
            if($Title == "" && $dcTitle != "" )	$Title = $dcTitle;
            if($Category == "" && $dcSubject != "" ) 	$Category = $dcSubject;
            if($Publisher == "" && $dcPublisher != "" ) $Publisher = $dcPublisher;
            if($CopyrightStatement == "" && $dcRights != "" ) 	$CopyrightStatement = $dcRights;
            // 2013-08-08 END

            if($Publisher != "")
                $Publisher_html = " <li><span>".$eLib['Book']["Publisher"]." </span><em>:</em> $Publisher</li>";
            else $Publisher_html = "";
            if($ePublisher != "")
                $ePublisher_html = " <li><span>".$eLib['Book']["ePublisher"]."</span><em>:</em> $ePublisher</li>";
            else $ePublisher_html = "";
            if($FirstPublished != "")
                $FirstPublished_html = " <li><span>".$eLib['Book']["FirstPublished"]."</span><em>:</em> $FirstPublished</li>";
            else $FirstPublished_html = "";

            if (file_exists($intranet_root."/file/elibrary/content/$BookID/worksheet.pdf")) {

                if($_SESSION['UserType'] == USERTYPE_STAFF){
                    $encode_entry_id = $this->encrypt($BookID);
                    $worksheet_html = '<div class="download_worksheet download_worksheet_2btn">
                        	<a  target="_blank" href="/file/elibrary/content/'.$BookID.'/worksheet.pdf" title="Download Worksheet" class="btn_download_worksheet_normal"></a>
                            <a  target="_blank" href="/file/elibrary/content/'.$BookID.'/worksheetans_'.$encode_entry_id.'.pdf" title="Download Worksheet(with Answers)" class="btn_download_worksheet_answer"></a>
                        </div>';
                }else{
                    $worksheet_html = '<div class="download_worksheet">
                        	<a target="_blank" href="/file/elibrary/content/'.$BookID.'/worksheet.pdf" title="Download Worksheet"><em></em></a>
                        </div>';
                }
                $copyright_style = '  ';
            }else{
                $worksheet_html = '';
                $copyright_style = ' style= "padding-right: 70px" ';
            }

            if($CopyrightYear != "")
                $copyright_html = '<div class="book_copyright" '.$copyright_style.'>
                        '.$CopyrightYear.'
				<span>'.$CopyrightStatement.'</span>
                        </div>';
            else $copyright_html = '<div class="book_copyright" '.$copyright_style.'>
				<span>'.$CopyrightStatement.'</span>
                        </div>';

//			 $Title = $book_elibinfo[0]['Title'];
//				$Level = $book_elibinfo[0]['Level'] ? "Level ".$book_elibinfo[0]['Level'] : "";
//				$Category = $book_elibinfo[0]['Category'];
//				$Publisher = $book_elibinfo[0]['Publisher'];
//
//				$FirstPublished = $book_elibinfo[0]['FirstPublished'];
//				$ePublisher = $book_elibinfo[0]['ePublisher'];
//				$CopyrightYear = $book_elibinfo[0]['CopyrightYear'];
//				$CopyrightStatement = $book_elibinfo[0]['CopyrightStatement'];

//			  $x.= '<div class="book_info_detail_page">
//		            	<div class="book_info_detail">
//		                   <div id="book_info_slip" class="slip_lv_4" >
//	                   			<ul>
//	                                <li><em>'.$Lang['eBookReader']['Title'].' </em> <span> '.$dcTitle.'</span></li>
//	                                <li><em>'.$Lang['eBookReader']['Author'].' </em> <span> '.$dcCreator.'</span></li>
//	                                <li><em>'.$Lang['eBookReader']['Publisher'].' </em> <span> '.$dcPublisher.'</span></li>
//	                                <li><em>'.$Lang['eBookReader']['Copyright'].' </em> <span> '.$dcRights.'</span></li>
//									<li><em>'.$Lang['eBookReader']['Category'].' </em> <span> '.$dcSubject.'</span></li>
//									<li><em>'.$Lang['eBookReader']['Tag'].' </em> <span> '.$tagName.'</span></li>';
//							$x.='</ul>
//		                   </div>
//		                </div>
//		            </div>';
            // table of content list
            $margin_left = $ImageWidth == "" || $ImageWidth == 0 ? 90 : 55;
            $info_width = $ImageWidth >=500 || $ImageWidth == "" || $ImageWidth == 0 ? 594 : $ImageWidth + 80;

            if (file_exists($intranet_root."/file/elibrary/content/$BookID/image/indexpageimage.png")) {
                $img_src = 'src="/file/elibrary/content/'.$BookID.'/image/indexpageimage.png"';
            }else{
                $img_src = 'src="/images/2009a/ebook_reader/info_pic_temp.png"';
            }

            //2013-11-12 Charles Ma
            if (file_exists($intranet_root."/file/elibrary/content/$BookID/dlpdf.pdf")) {
                $dlpdf_html = '<div class="download_pdf download_pdf_small">
	                        	<a target="_blank" href="/file/elibrary/content/'.$BookID.'/dlpdf.pdf" title="Download PDF Version"><em></em></a>
	                        </div>';
            }else{
                $dlpdf_html = '';
            }

            $x .= '<div class="book_info_detail_page"> 
            	<div class="book_info_detail_story book_info_worksheet" style="width: '.$info_width.'px;margin-left: '.$margin_left.'px">
                	<h1>'.$Title.'</h1>' .
                '<div class="book_author">'.$Author.'</div>
                    	<div class="book_cat">'.$Category.'</div>
                        <div class="book_level">'.$Level.'</div>
                        <div class="book_info_pic"><img '.$img_src.'>'.$dlpdf_html.'</div>
                        <div class="book_publish_detail">
                        	<ul>
                            	'.$FirstPublished_html.'
                                '.$Publisher_html.'
                                '.$ePublisher_html.'
                                '.$ISBN_html.'
           					</ul>
         
                        </div>
                        '.$copyright_html.'

                 		 '.$worksheet_html.'              
                </div>         
            </div>';

            //2013-11-12 Charles Ma END

            $x.= $print_content;

            $x.= '</div>'."\n";
            $x.= '</div>'."\n";
        }
        if($book_info[0]['InputFormat'] == 4)
            $libebookreader->CoverImageName = 'images/'.$book_sub_folder.'_0001.jpg';
        // 2013-05-29 eBook-update-130529c END
        if($libebookreader->CoverImageName != ''){
            $x .= $this->Get_Cover_Image_Layer('/home/eLearning/ebook_reader/'.$BookID.'/reader/'.$libebookreader->CoverImageName, $PageMode);
        }

        return $x;
    }

    function Get_Table_of_Content($pageIndex, $title,$navPointList,$indent_size = 2, $isImageBook=false)
    {
        global $Lang, $userBrowser;
        if(($userBrowser->browsertype == 'MSIE' && !$isImageBook) || $_SESSION['eBookReaderPageMode']==1){
            $ul_css = 'style="height:700px;"';
            $dv_css = 'style="display:none;"';
        }else{
            $ul_css = '';
            $dv_css = '';
        }

        if( count($title) > 1 ) $title = $title[2];									//2012-11-15 Charles Ma : temporary solution
        $x = '<div class="book_toc_list_page" '.$dv_css.'>';
//		$x.= '<div id ="book_name" class="book_info_top_right">'.($title=='Unknown' ?'':htmlspecialchars($title,ENT_QUOTES)).'</div>';  // 2012-11-15 Charles Ma --> may have problem found in dev book /720/
        //$x.= '<div class="book_info_top_right"></div>';
        $x.= '<div class="book_toc_list_page_detail">';
        $x.= '<h1>'.$Lang['eBookReader']['TableOfContents'].'</h1>';
        $x.= '<ul '.$ul_css.'>';
        $navPoint = array();
        if(!isset($navPointList['navPointList']) && isset($navPointList[0])){
            if(isset($navPointList['navLabel'])){
                $navPoint['navLabel'] = $navPointList['navLabel'];
            }
            if(isset($navPointList['content'])){
                $navPoint['content'] = $navPointList['content'];
            }
            $navPoint['navPointList'] = $navPointList;
        }else{
            $navPoint = $navPointList;
        }

        $indent = 0;
        //$indent_size = 2;
        if(trim($title)!=''){
            //$x .= $title."\n<br>";
            $indent++;
        }
        //if(isset($navPoint['navLabel']))
        //{

        $x .= $this->getTableOfContentEntry($pageIndex, $navPoint, $indent, $indent_size);
        //}
        $x .= '</ul>';
        $x .= '</div>';
        $x.= '</div>';

        return $x;
    }

    // print Book Description instead of Table of Content for Young Readers series
    function Get_Book_Description($title, $bookID, $isImageBook=false){
        global $intranet_root, $userBrowser, $eclass_prefix;
        if(($userBrowser->browsertype == 'MSIE' && !$isImageBook) || $_SESSION['eBookReaderPageMode']==1){
            $ul_css = 'style="height:700px;"';
            $dv_css = 'style="display:none;"';
        }else{
            $ul_css = '';
            $dv_css = '';
        }
        $libebookreader = $this->Get_libebookreader();

        # database for this module
        //$libmsDB = $eclass_prefix . "eClass_LIBMS";

        $sql = "SELECT Language, Preface FROM INTRANET_ELIB_BOOK WHERE BookID='".$bookID."'";
        $bookInfo = $libebookreader->returnArray($sql);

        $x = '';
        if(count($bookInfo) > 0){
            if($bookInfo[0]['Language'] !== "eng"){
                include($intranet_root.'/lang/ebook_reader_lang.b5.php');
            }else{
                include($intranet_root.'/lang/ebook_reader_lang.en.php');
            }

            $description = $bookInfo[0]['Preface'];
            if( count($title) > 1 ) $title = $title[2];									//2012-11-15 Charles Ma : temporary solution
            $x = '<div class="book_toc_list_page" '.$dv_css.'>';
            //		$x.= '<div id ="book_name" class="book_info_top_right">'.($title=='Unknown' ?'':htmlspecialchars($title,ENT_QUOTES)).'</div>';  // 2012-11-15 Charles Ma --> may have problem found in dev book /720/
            //$x.= '<div class="book_info_top_right"></div>';
            $x.= '<div class="book_toc_list_page_detail">';
            $x.= '<h1>'.$Lang['eBookReader']['Description'].'</h1>';
            $x.= '<ul '.$ul_css.'>';
            $x.= '<p>'.$description.'</p>';
            $x .= '</ul>';
            $x .= '</div>';
            $x.= '</div>';
        }

        return $x;
    }

    // print one line table of content recursively
    function getTableOfContentEntry($pageIndex, $node, $level=0, $indent_size=4)
    {
        $x = '';
        $indent = $level;
        if(isset($node['navLabel']))
        {
            $indent++;
            $href = $this->stripHtmlLinkAnchor($node['content']);
            $page_number = $pageIndex[$href];
            //$x .= str_repeat('&nbsp;', $indent * $indent_size);
            $x .= '<li><a href="javascript:jsToPage(\''.$href.'\');"><span><u style="background-image: none">'.$node['navLabel'].'</u></span><em style="background-image: none"> '.$page_number.'</em></a></li>';
        }
        if(isset($node['navPointList'][0])){
            for($i=0;$i<count($node['navPointList']);$i++){
                $x .= $this->getTableOfContentEntry($pageIndex, $node['navPointList'][$i], $indent, $indent_size);
            }
        }elseif(count($node['navPointList']) > 0){
            $x .= $this->getTableOfContentEntry($pageIndex, $node['navPointList'], $indent, $indent_size);
        }

        return $x;
    }

    function Get_Tool_Book_Board_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang, $userBrowser, $special_feature; // 2013-08-19 Charles Ma

        $browsertype = $userBrowser->browsertype;
        $btn_Style='';
        if($browsertype == 'MSIE'){
            $btn_style = 'style="display:none;"';
        }

        //2013-08-19 Charles Ma
        if($special_feature['emag']){
            $btn_style_emag = 'style="display:none;"';
        }

        $x = '<div id="tool_book_board" class="tool_board" style="display:none;">
				<div class="tool_board_top">
					<div class="tool_board_top_right">
						<div class="tool_board_top_bg">&nbsp;
						</div>
					</div>
				</div>
			    <div class="tool_board_content">
					<div class="tool_board_content_right">
						<div class="tool_board_content_bg">
						    <a href="javascript:void(0);" class="btn_my_progress" title="'.$Lang['eBookReader']['MyProgress'].'" id="btn_my_progress" '.$btn_style.' '.$btn_style_emag.'>&nbsp;</a>
						    <a href="javascript:void(0);" class="btn_book_review" title="'.$Lang['eBookReader']['BookReviews'].'" id="btn_book_review" '.$btn_style_emag.'>&nbsp;</a>
						    <a href="javascript:void(0);" class="btn_send_recommend" title="'.$Lang['eBookReader']['SendRecommend'].'" id="btn_send_recommend" '.$btn_style_emag.'>&nbsp;</a>
						    <span class="sep" '.$btn_style.' '.$btn_style_emag.'>&nbsp;</span>
						    <a href="javascript:void(0);" class="btn_my_notes" title="'.$Lang['eBookReader']['MyNotes'].'" id="btn_my_notes" '.$btn_style.'>&nbsp;</a>
						    <a href="javascript:void(0);" class="btn_my_bookmarkes" title="'.$Lang['eBookReader']['MyBookmarks'].'" id="btn_my_bookmarks" '.$btn_style.'>&nbsp;</a>
						    <p class="spacer"></p>
			    		</div>
					</div>
				</div>
			    <div class="tool_board_bottom">
					<div class="tool_board_bottom_right">
						<div class="tool_board_bottom_bg">
						</div>
					</div>
				</div>
			</div>';
        //2013-08-19 Charles Ma END
        return $x;
    }

    function Get_Tool_Setting_Board_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang, $userBrowser;
        /*
        $x = '<div id="tool_setting_board" class="tool_board" style="display:none;">
                <div class="tool_board_top">
                    <div class="tool_board_top_right">
                        <div class="tool_board_top_bg">&nbsp;
                        </div>
                    </div>
                </div>
                <div class="tool_board_content">
                    <div class="tool_board_content_right">
                        <div class="tool_board_content_bg">
                            <ul>
                                <li class="current_skin"><a href="javascript:void(0);" class="skin_white"><span>&nbsp;</span></a></li>
                                <li><a href="javascript:void(0);" class="skin_yellow"><span>&nbsp;</span></a></li>
                                <p class="spacer"></p>
                            </ul>
                            <p class="spacer"></p>
                        </div>
                    </div>
                </div>
                <div class="tool_board_bottom">
                    <div class="tool_board_bottom_right">
                        <div class="tool_board_bottom_bg">
                        </div>
                    </div>
                </div>
            </div>';
        */
        $browsertype = $userBrowser->browsertype;
        $btn_style = '';
        if($browsertype == 'MSIE'){
            $btn_style = 'style="display:none;"';
        }
        $paperStyle = empty($_SESSION['eBookReaderPaperStyle'])? 'white' : $_SESSION['eBookReaderPaperStyle'];
        $x = '<div class="tool_board" id="tool_setting_board" style="display:none;">
				<div class="tool_board_top">
					<div class="tool_board_top_right">
						<div class="tool_board_top_bg">&nbsp;
						</div>
					</div>
				</div>
			    <div class="tool_board_content">
					<div class="tool_board_content_right">
						<div class="tool_board_content_bg">
						    <ul>
						    	<li '.($paperStyle=='white'?'class="current_skin"':'').'><a class="skin_white" href="javascript:jsSwitchPaperStyle(\'white\');" id="btn_skin_white" title="'.$Lang['eBookReader']['ChangeToWhiteSkin'].'"><span>&nbsp;</span></a></li>
						        <li '.($paperStyle=='yellow'?'class="current_skin"':'').'><a class="skin_yellow" href="javascript:jsSwitchPaperStyle(\'yellow\');" id="btn_skin_yellow" title="'.$Lang['eBookReader']['ChangeToYellowSkin'].'"><span>&nbsp;</span></a></li>
						        <p class="spacer" '.$btn_style.'></p><span class="sep" '.$btn_style.'>&nbsp;</span>
						        <li '.($_SESSION['eBookReaderPageMode']==1?'class="current_mode"':'').' '.$btn_style.'><a class="pagemode_one" href="javascript:jsSwitchPageMode(1);" id="btn_pagemode_one" title="'.$Lang['eBookReader']['ChangeToOnePageMode'].'"><span>&nbsp;</span></a></li>
						        <li '.($_SESSION['eBookReaderPageMode']==2?'class="current_mode"':'').' '.$btn_style.'><a class="pagemode_two" href="javascript:jsSwitchPageMode(2);" id="btn_pagemode_two" title="'.$Lang['eBookReader']['ChangeToTwoPageMode'].'"><span>&nbsp;</span></a></li>
						        <p class="spacer"></p>
						    </ul>
						    <p class="spacer"></p>
			    		</div>
					</div>
				</div>
			    <div class="tool_board_bottom">
					<div class="tool_board_bottom_right">
						<div class="tool_board_bottom_bg">
						</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Page_Number_Layer($PageNum='')
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_page_num" class="tool_board" style="display:none;">
				<div class="tool_board_top">
					<div class="tool_board_top_right">
						<div class="tool_board_top_bg">&nbsp;
						</div>
					</div>
				</div>
    			<div class="tool_board_content">
					<div class="tool_board_content_right">
						<div class="tool_board_content_bg">
  							<span><span style="max-width: 100%;">'.$Lang['eBookReader']['Page'].'</span><strong><span id="tool_page_num_span">'.$PageNum.'</span></strong> <p class="spacer"></p></span>
    						<p class="spacer"></p>
    					</div>
					</div>
				</div>
    			<div class="tool_board_bottom">
					<div class="tool_board_bottom_right">
						<div class="tool_board_bottom_bg">
						</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Tool_Page_Search_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_page_search" class="tool_big_board" style="display:none;">
				<div class="tool_big_board_top">
					<div class="tool_big_board_top_right">
						<div class="tool_big_board_top_bg">
			    			<div class="search_form">
			        			<input class="inputbox" id="search_keyword" />
			        			<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Search'].'" id="btn_do_search" />
			       	 		</div>
			    		</div>
					</div>
				</div>
			    <div class="tool_big_board_content">
					<div class="tool_big_board_content_right">
						<div class="tool_big_board_content_bg">
			  				<div class="search_result">';

        $x .= '<span class="search_result_num" id="search_result_total">'.$Lang['eBookReader']['ResultsFound'].'</span>
					        	<ul>';
        /*
            $x .= '<li><a href="#"><h1><span class="search_result_chapter">Chapter 1</span><span class="page_num">Page 5</span><p class="spacer"></p></h1>
                        <span class="search_result_content">noticed that she was wearing a thick <strong>gold</strong> wedding ring. There was an ugly scar on her little finger. </span></a>
                       <p class="spacer"></p>
                    </li>
                   <li><a href="#"><h1><span class="search_result_chapter">Chapter 1</span><span class="page_num">Page 5</span><p class="spacer"></p></h1>
                        <span class="search_result_content">noticed that she was wearing a thick <strong>gold</strong> wedding ring. There was an ugly scar on her little finger. </span></a>
                       <p class="spacer"></p>
                    </li>
                    <li><a href="#"><h1><span class="search_result_chapter">Chapter 1</span><span class="page_num">Page 5</span><p class="spacer"></p></h1>
                        <span class="search_result_content">noticed that she was wearing a thick <strong>gold</strong> wedding ring. There was an ugly scar on her little finger. </span></a>
                       <p class="spacer"></p>
                    </li>
                   <li><a href="#"><h1><span class="search_result_chapter">Chapter 1</span><span class="page_num">Page 5</span><p class="spacer"></p></h1>
                        <span class="search_result_content">noticed that she was wearing a thick <strong>gold</strong> wedding ring. There was an ugly scar on her little finger. </span></a>
                       <p class="spacer"></p>
                    </li>
                   <li><a href="#"><h1><span class="search_result_chapter">Chapter 1</span><span class="page_num">Page 5</span><p class="spacer"></p></h1>
                        <span class="search_result_content">noticed that she was wearing a thick <strong>gold</strong> wedding ring. There was an ugly scar on her little finger. </span></a>
                       <p class="spacer"></p>
                    </li>
                    <li><a href="#"><h1><span class="search_result_chapter">Chapter 1</span><span class="page_num">Page 5</span><p class="spacer"></p></h1>
                        <span class="search_result_content">noticed that she was wearing a thick <strong>gold</strong> wedding ring. There was an ugly scar on her little finger. </span></a>
                       <p class="spacer"></p>
                    </li>
                   <li><a href="#"><h1><span class="search_result_chapter">Chapter 1</span><span class="page_num">Page 5</span><p class="spacer"></p></h1>
                        <span class="search_result_content">noticed that she was wearing a thick <strong>gold</strong> wedding ring. There was an ugly scar on her little finger. </span></a>
                       <p class="spacer"></p>
                    </li>
                   <li><a href="#"><h1><span class="search_result_chapter">Chapter 1</span><span class="page_num">Page 5</span><p class="spacer"></p></h1>
                        <span class="search_result_content">noticed that she was wearing a thick <strong>gold</strong> wedding ring. There was an ugly scar on her little finger. </span></a>
                       <p class="spacer"></p>
                    </li>
                    <li><a href="#"><h1><span class="search_result_chapter">Chapter 1</span><span class="page_num">Page 5</span><p class="spacer"></p></h1>
                        <span class="search_result_content">noticed that she was wearing a thick <strong>gold</strong> wedding ring. There was an ugly scar on her little finger. </span></a>
                       <p class="spacer"></p>
                    </li>';
                   */
        $x .= '</ul>';
        $x .= '</div>
			   			 </div>
					</div>
				</div>
			    <div class="tool_big_board_bottom">
					<div class="tool_big_board_bottom_right">
						<div class="tool_big_board_bottom_bg">
							
						</div>
					</div>
				</div>
			</div>';

        $x.= '<iframe id="search_frame" width="0px" height="0px" src="" ></iframe><iframe id="search_frame2" width="0px" height="0px" src="" ></iframe>';

        return $x;
    }

    function Get_Tool_Book_Progress_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $progressSelection = '<select name="Percentage" id="tool_book_progress_percentage">';
        for($i=5;$i<100;$i+=5){
            $progressSelection .= '<option value="'.$i.'">'.$i.'</option>';
        }
        $progressSelection.= '</select>';

        $x = '<div id="tool_book_progress" class="tool_big_board" style="display:none;">
				<div class="tool_big_board_top">
					<div class="tool_big_board_top_right">
						<div class="tool_big_board_top_bg">
			    			<h1 class="tool_board_title">'.$Lang['eBookReader']['MyProgress'].'</h1>
			    		</div>
					</div>
				</div>
			    <div class="tool_big_board_content">
					<div class="tool_big_board_content_right">
						<div class="tool_big_board_content_bg">
			  				<div class="progress_form">';
        $x .= '<p><span><input name="ProgressStatus[]" type="radio" value="1" /></span> '.$Lang['eBookReader']['Progress'][0].'</p>';
        $x .= '<p><span><input name="ProgressStatus[]" type="radio" value="2" /></span> '.$Lang['eBookReader']['Progress'][1].'</p>';
        $x .= '<p><span><input name="ProgressStatus[]" type="radio" value="3" /></span> '.$Lang['eBookReader']['Progress'][2].' '.$progressSelection.' %.</p>';
        $x .= '</div>
			    		</div>
					</div>
				</div>
			    <div class="tool_big_board_bottom">
					<div class="tool_big_board_bottom_right">
						<div class="tool_big_board_bottom_bg">
			     			<div class="tool_board_btn">' .
            '<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Cancel'].'" id="btn_tool_book_progress_cancel" />
								<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Done'].'" id="btn_tool_book_progress_done" />
							</div>
			    		</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Tool_Book_Recommend_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_book_recommend" class="tool_big_board" style="display:none;">
				<div class="tool_big_board_top">
					<div class="tool_big_board_top_right">
						<div class="tool_big_board_top_bg">
			    			<h1 class="tool_board_title">'.$Lang['eBookReader']['Recommend'].'</h1>
			    		</div>
					</div>
				</div>
			    <div class="tool_big_board_content">
					<div class="tool_big_board_content_right">
						<div class="tool_big_board_content_bg">
			  				<div class="recommend_form">
			        			'.$Lang['eBookReader']['To'].' : <input class="inputbox" id="tool_book_recommend_to" />
			        			'.$Lang['eBookReader']['Message'].' : <textarea wrap="virtual" class="inputbox" id="tool_book_recommend_text"></textarea>
			        		</div>
			    		</div>
					</div>
				</div>
			    <div class="tool_big_board_bottom">
					<div class="tool_big_board_bottom_right">
						<div class="tool_big_board_bottom_bg">
			     			<div class="tool_board_btn">' .
            '<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Cancel'].'" id="btn_recommend_cancel" />
								<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Send'].'" id="btn_recommend_send" />
							</div>
			    		</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Tool_Book_Review_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_book_review" class="tool_big_board" style="display:none;">
				<div class="tool_big_board_top">
					<div class="tool_big_board_top_right">
						<div class="tool_big_board_top_bg">
			    			<h1 class="tool_board_title">'.$Lang['eBookReader']['BookReviews'].'</h1>
							<div id="tool_common">
								<a href="javascript:void(0);" class="btn_write" id="btn_review_call_writer" Title="'.$Lang['eBookReader']['AddNewReview'].'">&nbsp;</a>
							</div>
			        		<p class="spacer"></p>
			    		</div>
					</div>
				</div>
			    <div class="tool_big_board_content">
					<div class="tool_big_board_content_right">
						<div class="tool_big_board_content_bg">
			    			<div class="review_log_list" id="review_log_list">
							</div>
			    		</div>
					</div>
				</div>
				<div class="tool_big_board_bottom">
					<div class="tool_big_board_bottom_right">
						<div class="tool_big_board_bottom_bg">' .
            '<div class="tool_board_btn"> ' . '<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Close'].'" id="btn_tool_book_review_close"> ' . '</div>
				    	</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Tool_Book_Review_Log_List($BookID)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $libebookreader = $this->Get_libebookreader();

        $reviews = $libebookreader->Get_Book_Reviews($BookID);

        $x1 = '';
        $rating = 0;
        $rating_count = 0;

        if(count($reviews)==0){
            $x1 = $Lang['eBookReader']['NoReviewsAtTheMoment'];
        }
        for($i=0;$i<count($reviews);$i++){
            $rating += $reviews[$i]['Rating'];
            $rating_count++;
            $x1 .= $this->Get_Tool_Book_Review_Log_Entry($reviews[$i]);
        }
        $average_rating = $rating_count==0?0:sprintf("%d", $rating / $rating_count);

        $average_rating_str = str_replace('<!--NUMBER-->',$average_rating,$Lang['eBookReader']['AverageOfRatings']);

        //$rating_on_class = 'class="star_on"';
        $x2 = '<div class="review_log_overall">  
	   				'.$average_rating_str.'
			        <div class="review_star">
			        	<ul class="review_star_overall">';
        for($i=1;$i<=5;$i++){
            $rating_on_class = ($average_rating >= $i)? 'class="star_on"' : '';
            $x2 .= '<li '.$rating_on_class.'><span></span></li>';
        }
        $x2.='</ul>
			        </div>
					<p class="spacer"></p>
				</div>';

        $x = $x2.$x1;

        return $x;
    }

    function Get_Tool_Book_Review_Log_Entry($DataArr)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang, $intranet_session_language;

        $libebookreader = $this->Get_libebookreader();

        $reviewID = $DataArr['ReviewID'];
        $_userID = $DataArr['UserID'];
        $rating = $DataArr['Rating'];
        $content = nl2br(htmlspecialchars($DataArr['Content'],ENT_QUOTES));
        $dateModified = $libebookreader->FormatDatetime($DataArr['DateModified']);
        if(($intranet_session_language == 'b5' || $intranet_session_language == 'gb') && trim($DataArr['ChineseName'])!=''){
            $userName = htmlspecialchars($DataArr['ChineseName'],ENT_QUOTES);
        }else{
            $userName = htmlspecialchars($DataArr['EnglishName'],ENT_QUOTES);
        }
        $className = trim($DataArr['ClassName']);
        $classNumber = trim($DataArr['ClassNumber']);
        if($className != '' && $classNumber != ''){
            $userName .= ' ('.$className.' '.$classNumber.')';
        }
        $helpful_checked = $libebookreader->Get_Book_Review_Helpful_Check($reviewID);

        $x = '<div class="review_log">  
				<div class="review_log_board_top">
					<div class="review_log_board_top_right">
						<div class="review_log_board_top_bg">
						</div>
					</div>
				</div>
                <div class="review_log_board_content">
					<div class="review_log_board_content_right">
						<div class="review_log_board_content_bg">
                			<div class="review_log_content_info">
	                    		<div class="review_star">
									<ul class="review_star_log">';
        for($i=1;$i<=5;$i++){
            $class = $rating >= $i ? 'class="star_on"' : '';
            $x .= '<li '.$class.'><span></span></li>';
        }
        $x .= '</ul>
	                            </div>
                            	<div class="review_writer">'.$Lang['eBookReader']['by'].' '.$userName.'
								</div>
                            	<div class="review_time">'.$dateModified.'
								</div>
                    		</div>
                			<div class="review_log_content_detail">'.$content.'
							</div>
                		</div>
					</div>
				</div>
                <div class="review_log_board_bottom">
					<div class="review_log_board_bottom_right">
						<div class="review_log_board_bottom_bg" id="review_log_helpful_'.$reviewID.'">';
        if($helpful_checked){
            $x.= $this->Get_Tool_Book_Review_Helpful_Vote($reviewID);
        }else{
            $vote = $libebookreader->Get_Book_Review_Helpful_Vote($reviewID);
            $voteYes = $vote['yesNum'];
            $voteNo = $vote['total']-$voteYes;
            $x.= $Lang['eBookReader']['UsefulReview'].' <a href="javascript:jsVoteReviewHelpful('.$reviewID.',1);">'.$Lang['eBookReader']['yes'].'('.$voteYes.')</a> <a href="javascript:jsVoteReviewHelpful('.$reviewID.',0)">'.$Lang['eBookReader']['no'].'('.$voteNo.')</a>';
        }
        $x.='</div>
					</div>
				</div>
    		</div>';

        return $x;
    }

    function Get_Tool_Book_Review_Helpful_Vote($pReviewID)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $libebookreader = $this->Get_libebookreader();
        $vote = $libebookreader->Get_Book_Review_Helpful_Vote($pReviewID);
        $str = $Lang['eBookReader']['PeopleFoundThisReviewHelpful'];
        $str = str_replace('<!--NUMBER-->',$vote['yesNum'],$str);
        $str = str_replace('<!--TOTAL-->',$vote['total'],$str);
        //$x.= $vote['yesNum'].' of '.$vote['total'].' people found this Review helpful.';
        $x .= $str;

        return $x;
    }

    function Get_Tool_Book_Review_Edit_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_book_review_edit" class="tool_big_board" style="display:none;">
				<div class="tool_big_board_top">
					<div class="tool_big_board_top_right">
						<div class="tool_big_board_top_bg">
			    			<h1 class="tool_board_title">'.$Lang['eBookReader']['BookReviews'].'</h1>
			        		<p class="spacer"></p>
			    		</div>
					</div>
				</div>
			    <div class="tool_big_board_content">
					<div class="tool_big_board_content_right">
						<div class="tool_big_board_content_bg">
			  				<div class="review_form">
			        			<div class="review_star">
						        	<ul class="review_star_input">
						            <li><a href="javascript:void(0);" onmouseover="jsStarOn(this,1)" onmouseup="jsStarOn(this,1)"><span></span></a></li>
						            <li><a href="javascript:void(0);" onmouseover="jsStarOn(this,1)" onmouseup="jsStarOn(this,1)"><span></span></a></li>
						            <li><a href="javascript:void(0);" onmouseover="jsStarOn(this,1)" onmouseup="jsStarOn(this,1)"><span></span></a></li>
						            <li><a href="javascript:void(0);" onmouseover="jsStarOn(this,1)" onmouseup="jsStarOn(this,1)"><span></span></a></li>
						            <li><a href="javascript:void(0);" onmouseover="jsStarOn(this,1)" onmouseup="jsStarOn(this,1)"><span></span></a></li>
						            </ul>
			       				 </div>
			       				 <p class="spacer"></p>
			        			<textarea wrap="virtual" class="inputbox" id="tool_book_review_text"></textarea>
			        		</div>
			    		</div>
					</div>
				</div>
			    <div class="tool_big_board_bottom">
					<div class="tool_big_board_bottom_right">
						<div class="tool_big_board_bottom_bg">
			    			<div class="tool_board_btn">
								<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Cancel'].'" id="btn_tool_book_review_cancel"/>
								<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Done'].'" id="btn_tool_book_review_done"/>
							</div>
			    		</div>
					</div>
				</div>
			</div>';

        return $x;
    }
    function Get_Tool_Glossary_Pager_Layer(){//20140919-newfeature2014
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_glossary_paper" class="tool_glossary_list" style="position:absolute;z-index: 6;display:none; top: 11.5px; width: 520px;">
				<div class="tool_glossary_list_top">
					<div class="tool_glossary_list_top_right">
						<div class="tool_glossary_list_top_bg">
			    			<h1>
							<div class="tool_right"><a href="javascript:void(0);" class="btn_common_close" id="btn_notes_glossary_close"></a>
							</div>
			        		</h1>
			        		<p class="spacer"></p>
			    		</div>
					</div>
				</div>
			    <div class="tool_glossary_list_content">
					<div class="tool_glossary_list_content_right">
						<div class="tool_glossary_list_content_bg">
							<span class="glossary_header" id="glossary_header">Glossary - </span><!--<span class="glossary_page" id="glossary_page">--></span>
			  				<div id="glossary_list" class="glossary_list" style="max-height: 490px;">
			  					<div id="notes_glossary_notes" class="glossary_text">
				        			
			        			</div>
			        		</div>
			    		</div>
					</div>
				</div>
			    <div class="tool_glossary_list_bottom">
					<div class="tool_glossary_list_bottom_right">
						<div class="tool_glossary_list_bottom_bg">
			     			<div class="tool_board_btn" style="display:none">
								<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Done'].'" id="btn_notes_summary_done" />
							</div>
			    		</div>
					</div>
				</div>
				<input type="hidden" id="notes_pager_page_number" value="" />
			</div>';

        return $x;
    }
    function Get_Tool_Summary_Pager_Layer(){//20140919-newfeature2014
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_summary_paper" class="tool_notes_paper" style="display:none; top: 11.5px; width: 520px;">
				<div class="tool_notes_top">
					<div class="tool_notes_top_right">
						<div class="tool_notes_top_bg">
			    			<h1>
							<div class="tool_right"><a href="javascript:void(0);" class="btn_common_close" id="btn_notes_summary_close"></a>
							</div>
			        		</h1>
			        		<p class="spacer"></p>
			    		</div>
					</div>
				</div>
			    <div class="tool_notes_content">
					<div class="tool_notes_content_right">
						<div class="tool_notes_content_bg">
			  				<div class="notes_form">
			        			<p class="spacer"></p>
			        			<textarea wrap="virtual" class="notesinput" id="notes_summary_notes" style="margin: 0px; height: 500px; width: 424px;">
			        			</textarea>
			        		</div>
			    		</div>
					</div>
				</div>
			    <div class="tool_notes_bottom">
					<div class="tool_notes_bottom_right">
						<div class="tool_notes_bottom_bg">
			     			<div class="tool_board_btn" style="display:none">
								<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Done'].'" id="btn_notes_summary_done" />
							</div>
			    		</div>
					</div>
				</div>
				<input type="hidden" id="notes_pager_page_number" value="" />
			</div>';

        return $x;
    }
    function Get_Tool_Notes_Pager_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_notes_paper" class="tool_notes_paper" style="display:none;">
				<div class="tool_notes_top">
					<div class="tool_notes_top_right">
						<div class="tool_notes_top_bg">
			    			<h1>
			        		<div class="tool_left"><a href="javascript:void(0);" class="btn_delete" id="btn_notes_pager_delete"></a>
							</div>
							<div class="tool_right"><a href="javascript:void(0);" class="btn_common_close" id="btn_notes_pager_close"></a>
							</div>
			        		<span class="notes_date" id="notes_pager_date"></span>
			        		</h1>
			        		<p class="spacer"></p>
			    		</div>
					</div>
				</div>
			    <div class="tool_notes_content">
					<div class="tool_notes_content_right">
						<div class="tool_notes_content_bg">
			  				<div class="notes_form">
			        			<p class="spacer"></p>
			        			<textarea wrap="virtual" class="notesinput" id="notes_pager_notes">
			        			</textarea>
			        		</div>
			    		</div>
					</div>
				</div>
			    <div class="tool_notes_bottom">
					<div class="tool_notes_bottom_right">
						<div class="tool_notes_bottom_bg">
			     			<div class="tool_board_btn">
								<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Done'].'" id="btn_notes_pager_done" />
							</div>
			    		</div>
					</div>
				</div>
				<input type="hidden" id="notes_pager_page_number" value="" />
			</div>';

        return $x;
    }

    function Get_Tool_Highlight_Notes_Pager_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_highlight_notes_paper" class="tool_notes_paper" style="display:none;">
				<div class="tool_notes_top">
					<div class="tool_notes_top_right">
						<div class="tool_notes_top_bg">
			    			<h1>
			        		<div class="tool_left"><a href="javascript:void(0);" class="btn_delete" id="btn_highlight_notes_pager_delete"></a>
							</div>
							<div class="tool_right"><a href="javascript:void(0);" class="btn_common_close" id="btn_highlight_notes_pager_close"></a>
							</div>
			        		<span class="notes_date" id="highlight_note_pager_date"></span>
			        		</h1>
			        		<p class="spacer"></p>
			    		</div>
					</div>
				</div>
			    <div class="tool_notes_content">
					<div class="tool_notes_content_right">
						<div class="tool_notes_content_bg">
			  				<div class="notes_form">
			        			<p class="spacer"></p>
			        			<textarea wrap="virtual" class="notesinput" id="highlight_notes_pager_notes">
			        			</textarea>
			        		</div>
			    		</div>
					</div>
				</div>
			    <div class="tool_notes_bottom">
					<div class="tool_notes_bottom_right">
						<div class="tool_notes_bottom_bg">
			     			<div class="tool_board_btn">
								<input class="form_btn" type="button" value="'.$Lang['eBookReader']['Done'].'" id="btn_highlight_notes_pager_done" />
							</div>
			    		</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Tool_Notes_Book_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_my_notes" class="tool_notes_book" style="display:none;">
				<div class="tool_notes_book_top">
					<div class="tool_notes_book_top_right">
						<div class="tool_notes_book_top_bg">
			    			<h1>
			        		<div class="tool_right">
								<a href="javascript:void(0);" class="btn_common_close" id="btn_my_notes_close"></a>
							</div>
			        		</h1>
			        		<p class="spacer"></p>
			    		</div>
					</div>
				</div>
			    <div class="tool_notes_book_content">
					<div class="tool_notes_book_content_right">
						<div class="tool_notes_book_content_bg">
			  				<p class="spacer"></p>
			        		<span class="notes_book_header">'.$Lang['eBookReader']['MyNotes'].'</span>
			        		<div class="notes_log_list" id="notes_log_list" style="width:500px;height:450px;overflow-x:hidden;overflow-y:auto;">';

        //$x .= $this->Get_Tool_Notes_Log_Entry();
        /*
         $x .= '<p class="spacer"></p>
                <div class="btn_more"><a href="javascript:void(0);"> more notes...</a>
                </div>';
                */
        $x .= '</div>
				    	</div>
					</div>
				</div>
				<div class="tool_notes_book_bottom">
					<div class="tool_notes_book_bottom_right">
						<div class="tool_notes_book_bottom_bg">
				    		&nbsp;
						</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Tool_Notes_Log_Entry()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<p class="spacer" id="noteslog_spacer_{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}"></p>
              <div class="notes_log" id="noteslog_{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}">
        		<h1>
				<div class="tool_left"><a href="javascript:jsDeleteNotesLog(\'{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}\');" class="btn_delete" id="btn_noteslog_delete_{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}"></a>
				</div>
				<span class="notes_page_num" style="cursor:pointer;" onclick="jsToPageFromNotes({PAGE_NUMBERd9dff820-6828-11e1-b86c-0800200c9a66});">{CHAPTERd9dff820-6828-11e1-b86c-0800200c9a66} : '.$Lang['eBookReader']['Page'].' {PAGE_NUMBERd9dff820-6828-11e1-b86c-0800200c9a66}</span><span class="notes_date" id="noteslog_datetime_{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}">{DATETIMEd9dff820-6828-11e1-b86c-0800200c9a66}</span> </h1>
        		<input type="hidden" id="chapterurl_{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}" value="{CHAPTERURLd9dff820-6828-11e1-b86c-0800200c9a66}" /><p class="spacer"></p>
       		 	<div class="notes_highlight" >
					<span class="{HIGHLIGHT_CLASSd9dff820-6828-11e1-b86c-0800200c9a66}" style="cursor:default;">{HIGHLIGHT_TEXTd9dff820-6828-11e1-b86c-0800200c9a66}</span>
				</div>
      		  	<p class="spacer"></p>
       			<div class="notes_log_close">
        			<div class="notes_log_top">
						<div class="notes_log_top_right">
							<div class="notes_log_top_bg">
							</div>
						</div>
					</div>
        		    <div class="notes_log_content">
						<div class="notes_log_content_right">
							<div class="notes_log_content_bg">
         		 	  			<div class="noteslog_notes notesinput" id="noteslog_notes_div_{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}" onclick="jsToggleEditNotesLog(\'{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}\')">{NOTESd9dff820-6828-11e1-b86c-0800200c9a66}
								</div>
								<textarea id="noteslog_textedit_{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}" class="noteslog_textedit notesinput" wrap="virtual" style="display:none;height:120px;"></textarea>
         		   			</div>
						</div>
					</div>
          		  	<div class="notes_log_bottom">
						<div class="notes_log_bottom_right">
							<div class="notes_log_bottom_bg">
								<div class="tool_board_btn" id="noteslog_btn_layer_{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}" style="display:none;">
								   <input type="button" value="'.$Lang['eBookReader']['Done'].'" class="form_btn" id="btn_noteslog_done_{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}" onclick="jsEditNotesLog(\'{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}\')" >
								 </div>
							</div>
						</div>
					</div>
       			</div>
			</div>';

        return $x;
    }

    function Get_Tool_Page_Notes_Log_Entries($DataList)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $libebookreader = $this->Get_libebookreader();

        $x = '';
        for($i=0;$i<count($DataList);$i++)
        {
            $record_id = $DataList[$i]['RecordID'];
            $notes = htmlspecialchars(trim($DataList[$i]['Notes']));
            $date_modified = $libebookreader->FormatDatetime($DataList[$i]['DateModified']);
            $page_number = $DataList[$i]['PageNumber'];

            $x.= '<p class="spacer" id="page_noteslog_spacer_'.$record_id.'"></p>
	              <div class="notes_log" id="page_noteslog_'.$record_id.'">
	        		<h1>
					<div class="tool_left"><a href="javascript:jsDeletePageNotesLog(\''.$record_id.'\');" class="btn_delete" id="btn_page_noteslog_delete_'.$record_id.'"></a>
					</div>
					<span class="notes_page_num" style="cursor:pointer;" onclick="jsToPageFromNotes('.$page_number.');">'.$Lang['eBookReader']['Page'].' '.$page_number.'</span><span class="notes_date" id="page_noteslog_datetime_'.$record_id.'">'.$date_modified.'</span> </h1>
	        		<p class="spacer"></p>
	       			<div class="notes_log_close">
	        			<div class="notes_log_top">
							<div class="notes_log_top_right">
								<div class="notes_log_top_bg">
								</div>
							</div>
						</div>
	        		    <div class="notes_log_content">
							<div class="notes_log_content_right">
								<div class="notes_log_content_bg">
	         		 	  			<div class="notesinput" id="page_noteslog_notes_div_'.$record_id.'" onclick="jsToggleEditPageNotesLog(\''.$record_id.'\')">'.$notes.'
									</div>
									<textarea id="page_noteslog_textedit_'.$record_id.'" class="notesinput" wrap="virtual" style="display:none;height:120px;"></textarea>
	         		   			</div>
							</div>
						</div>
	          		  	<div class="notes_log_bottom">
							<div class="notes_log_bottom_right">
								<div class="notes_log_bottom_bg">
									<div class="tool_board_btn" id="page_noteslog_btn_layer_'.$record_id.'" style="display:none;">
									   <input type="button" value="'.$Lang['eBookReader']['Done'].'" class="form_btn" id="btn_page_noteslog_done_'.$record_id.'" onclick="jsEditPageNotesLog(\''.$record_id.'\')" >
									 </div>
								</div>
							</div>
						</div>
	       			</div>
				</div>';
        }

        return $x;
    }

    function Get_Tool_Bookmark_List_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_bookmark_list" class="tool_bookmark_list" style="display:none;">
				<div class="tool_bookmark_list_top">
					<div class="tool_bookmark_list_top_right">
						<div class="tool_bookmark_list_top_bg">
			    			<h1>
			        		<div class="tool_right">
								<a href="javascript:void(0);" class="btn_common_close" id="btn_tool_bookmark_list_close"></a>
							</div>
			        		</h1>
			        		<p class="spacer"></p>
			    			</div>
						</div>
					</div>
			    	<div class="tool_bookmark_list_content">
						<div class="tool_bookmark_list_content_right">
							<div class="tool_bookmark_list_content_bg">
			  					<p class="spacer"></p>
			        			<span class="bookmark_list_header">'.$Lang['eBookReader']['MyBookmarks'].'</span>
			        			<p class="spacer"></p>
			        			<div class="bookmark_log_list" id="bookmark_log_list">';
        /*
                          $x.= '<ul>
                                    <li> <a href="#"><span class="bookmark_title">Chapter 1 - Page 2</span><span class="notes_date">yesterday <em>2:30pm</em></span></a></li>
                                    <li> <a href="#"><span class="bookmark_title">Chapter 1 - Page 2</span><span class="notes_date">2012-01-30 <em>2:30pm</em></span></a></li>
                                    <li> <a href="#"><span class="bookmark_title">Chapter 1 - Page 2</span><span class="notes_date">2012-01-30 <em>2:30pm</em></span></a></li>
                                    <li> <a href="#"><span class="bookmark_title">Chapter 1 - Page 2</span><span class="notes_date">2012-01-30 <em>2:30pm</em></span></a></li>
                                    <li> <a href="#"><span class="bookmark_title">Chapter 1 - Page 2</span><span class="notes_date">2012-01-30 <em>2:30pm</em></span></a></li>
                                </ul>';

                              $x.= '<p class="spacer"></p>
                                     <div class="btn_more">
                                        <a href="javascript:void(0);"> more notes...</a>
                                    </div>';
        */
        $x.= '</div>
			    			</div>
						</div>
					</div>
			    	<div class="tool_bookmark_list_bottom">
						<div class="tool_bookmark_list_bottom_right">
							<div class="tool_bookmark_list_bottom_bg">
			    				&nbsp;
			    			</div>
						</div>
					</div>
				</div>';

        return $x;
    }

    function Get_Tool_Bookmark_List_Log_List($pBookID, $pUserID, $pPageMode, $pImageBook=false)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $libebookreader = $this->Get_libebookreader();
        $bookmarks = $libebookreader->Get_Book_User_Bookmarks($pBookID, $pUserID, $pPageMode);

        if(count($bookmarks) == 0){
            $x = $Lang['eBookReader']['NoBookmarksAtTheMoment'];
        }else{
            $x = '<ul>';
            for($i=0;$i<count($bookmarks);$i++){
                $page_num = $bookmarks[$i]['PageNum'];
                $date_input = $libebookreader->FormatDatetime($bookmarks[$i]['DateInput']);
                $page_index = $libebookreader->TransformTruePageNumberToPageIndex($pPageMode, $page_num, $pImageBook);

                $x.= '<li><a href="javascript:jsToPageFromBookmark('.$page_index.');"><span class="bookmark_title">'.$Lang['eBookReader']['Page'].' '.$page_num.'</span><span class="notes_date"><em>'.$date_input.'</em></span></a></li>';
            }
            $x.= '</ul>';
        }

        return $x;
    }

    function Get_Tool_Glossary_Board_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_glossary_board" style="display:none;position:absolute;z-index: 6;">
				<div class="tool_glossary_left">
					<div class="tool_glossary_right">
						<div class="tool_glossary_bg" id="tool_glossary_bg" style="background-position:-500px;">
			    			<div class="tool_glossary_list">
					        	<ul class="btn_glossary">
						            <li class="selected_glossary" id="selected_glossary">
						            </li>
					            </ul>
					            <ul><span class="glossary_btn">　</span>					              
					             </ul>
					        </div>
			    		</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Tool_Hightlight_Board_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;
        // 2013-08-05 Charles Ma
        $today = strtotime(date("Y-m-d"));
        $expiration_date =  $_SESSION['ss_intranet_plugin']['power_speech_expire'] == "" ? "" : strtotime($_SESSION['ss_intranet_plugin']['power_speech_expire']);
        $elib_expiration_date =  $_SESSION['ss_intranet_plugin']['elib_power_speech_expire'] == "" ? "" : strtotime($_SESSION['ss_intranet_plugin']['elib_power_speech_expire']);

        // $isNotExpired = ( $today < $expiration_date && $_SESSION['ss_intranet_plugin']['power_speech']) || ( $today < $elib_expiration_date && $_SESSION['ss_intranet_plugin']['elib_power_speech']);
        $isNotExpired = (check_powerspeech_is_expired()!=2 && $_SESSION['ss_intranet_plugin']['power_speech'])|| ( $today < $elib_expiration_date && $_SESSION['ss_intranet_plugin']['elib_power_speech']);

        $texttospeech = $isNotExpired && $_SESSION['UserType']!=USERTYPE_ALUMNI ?  '<a href="javascript:void(0);" class="btn_texttospeech" id="btn_highlight_texttospeech">&nbsp;</a>' : '<a style="display:none" href="javascript:void(0);" class="btn_texttospeech" id="btn_highlight_texttospeech">&nbsp;</a>';

        $x = '<div id="tool_highlight_board" style="display:none;position:absolute;">
				<div class="tool_hightlight_left">
					<div class="tool_hightlight_right">
						<div class="tool_hightlight_bg" style="background-position:-500px;">
			    			<div class="tool_hightlight_list">
					        	<ul class="btn_hightlight">
						            <li id="highlight_pulldown_control1" class="selected_hightlight">
						        		<a id="btn_select_hightlight1" href="javascript:void(0);" class="btn_select_hightlight_01"></a>
						        		<div id="highlight_pulldown1" class="highlight_pulldown highlight_pulldown1" style="display:none">
					                        <div class="highlight_pulldown_top"></div>
					                    	<div class="highlight_pulldown_list">
					                        	<ul class="tool_selected_hightlight">
					                                <li><a href="#" class="btn_hightlight_01"></a></li>
					                                <li><a href="#" class="btn_hightlight_02"></a></li>
					                                <li><a href="#" class="btn_hightlight_03"></a></li>
					                                <li><a href="#" class="btn_hightlight_04"></a></li>
					                                <li><a href="#" class="btn_hightlight_05"></a></li>
					                            </ul>
												<p class="spacer"></p>
					                        </div>
					                        <div class="highlight_pulldown_bottom"></div>
					                    </div>
						            </li>
						            <li id="highlight_pulldown_control2">
						        		<a id="btn_select_hightlight2" href="javascript:void(0);" class="btn_select_hightlight_06"></a>
						        		<div id="highlight_pulldown2" class="highlight_pulldown highlight_pulldown2" style="display:none">
					                        <div class="highlight_pulldown_top"></div>
					                    	<div class="highlight_pulldown_list">
					                        	<ul class="tool_selected_hightlight">
					                                <li><a href="#" class="btn_hightlight_06"></a></li>
					                                <li><a href="#" class="btn_hightlight_07"></a></li>
					                            </ul>
												<p class="spacer"></p>
					                        </div>
					                        <div class="highlight_pulldown_bottom"></div>
					                    </div>
						            </li> 
					            </ul>
					            <ul>
					              	<li>
					        			<a href="javascript:void(0);" class="btn_clear_highlight">&nbsp;</a>
					             	</li>
					             	<li class="btn_hightlight_sep"> <span></span>
					             	</li>
					             	<li>
					        			<a href="javascript:void(0);" class="btn_add_notes" id="btn_add_highlight_notes">&nbsp;</a>
					             	</li>
					              	<li>
					        			'.$texttospeech.'
					             	</li>
					             </ul>
					        </div>
			    		</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Tool_Voice_Board_Long_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_voice" class="tool_voice_board" style="display:none;position:absolute;left:0px;top:0px;width:400px;height:32px;">
				<div class="tool_voice_board_top">
					<div class="tool_voice_board_top_right">
						<div class="tool_voice_board_top_bg">
			    			&nbsp;
			    		</div>
					</div>
				</div>
			    <div class="tool_voice_board_content">
					<div class="tool_voice_board_content_right">
						<div class="tool_voice_board_content_bg" id="tool_voice_control_container">
			     			
			   		 	</div>
					</div>
				</div>
			    <div class="tool_voice_board_bottom">
					<div class="tool_voice_board_bottom_right">
						<div class="tool_voice_board_bottom_bg" style="background-position:-1000px top">
			    			&nbsp;
			    		</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Tool_Voice_Board_Short_Layer()
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="tool_voice" class="tool_voice_board" style="display:none;position:absolute;left:0px;top:0px;width:150px;height:16px;">
				<div class="tool_voice_board_top">
					<div class="tool_voice_board_top_right">
						<div class="tool_voice_board_top_bg" style="background-position:-1000px top">
			    			&nbsp;
			    		</div>
					</div>
				</div>
			    <div class="tool_voice_board_content">
					<div class="tool_voice_board_content_right">
						<div class="tool_voice_board_content_bg" id="tool_page_control_container" style="background-color:#CCCCCC;">
			     			<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/loadingAnimation.gif" style="width:100%; height:100%;border:0;padding:0;" />
			    		</div>
					</div>
				</div>
			    <div class="tool_voice_board_bottom">
					<div class="tool_voice_board_bottom_right">
						<div class="tool_voice_board_bottom_bg">
			    			&nbsp;
			    		</div>
					</div>
				</div>
			</div>';

        return $x;
    }

    function Get_Audio_Control($src, $width='', $height='')
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $width_css = $width != '' ? 'width:'.$width.'px;' : '';
        $height_css = $height != '' ? 'height:'.$height.'px;' : '';
        $style = ($width_css!='' || $height_css!='') ? 'style="'.$width_css.$height_css.'"': '';
        $x = '<audio id="audio_control" controls="controls" autoplay="autoplay" '.$style.'>
				<source src="'.$src.'" type="audio/mp3" />
				Your browser does not support the audio tag.
			  </audio>';

        return $x;
    }

    function Get_Audio_Object_Control($src, $width='', $height='')
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $width_attr = $width != '' ? 'width="'.$width.'px"' : '';
        $height_attr = $height != '' ? 'height="'.$height.'px"':  '';
        $x = '<object '.$width_attr.' '.$height_attr.' data="'.$src.'">';
        $x.= '<param name="url" value="'.$src.'">';
        $x.= '<param name="filename" value="'.$src.'">';
        $x.= '<param name="autoplay" value="true">';
        $x.= '<param name="autoStart" value="1">';
        $x.= '<param name="uiMode" value="full" />';
        $x.= '<param name="autosize" value="1">';
        $x.= '<param name="playcount" value="1">';
        $x.= '<embed type="application/x-mplayer2" src="'.$src.'" width="100%" height="100%" autostart="true" showcontrols="true" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/"></embed>';
        $x.= '</object>';

        return $x;
    }

    function Get_Bookmark_Image_Layer($id)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang, $intranet_version;

        $IMG_LAYOUT_SKIN = $intranet_version == '2.5'? $LAYOUT_SKIN : '2009a';// EJ20's LAYOUT_SKIN is 2007a, so hardcode 2009a for EJ
        $x = '<img id="'.$id.'" src="'.$image_path.'/'.$IMG_LAYOUT_SKIN.'/ebook_reader/icon_bookmark.png" style="display:none;z-index:8;cursor:pointer;" title="'.$Lang['eBookReader']['RemoveBookmark'].'">';

        return $x;
    }

    function Get_Book_Comic_Layer($WidthPerPage, $PageMode=2)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang, $userBrowser;

        $TotalWidth = $WidthPerPage * 2;

        $version = explode('.',$userBrowser->version);
        if($userBrowser->browsertype == 'MSIE' || ($userBrowser->browsertype == 'Firefox' && $version[0] <= 8)){
            // FF 3.6 and IE css background-image cannot fit with div size
            if($PageMode==1){
                $TotalWidth = floor($WidthPerPage * 1.4);
                $x = '<div id="book_comic">
					     <div id="book_comic_content" class="book_page_comic_1page" style="width:'.$TotalWidth.'px;">
							<div class="book_1page_style" >
								<img id="book_page_style" src="" width="100%" height="100%" />
								<!--<div class="page_top"></div>
					            <div class="page_content"></div>
					            <div class="page_bottom"></div>-->
							</div>
					     </div>
					  </div>';
            }else{
                $x = '<div id="book_comic">
					     <div id="book_comic_content" style="width: '.$TotalWidth.'px;" class="book_page_comic_2page"><!-- width="image width x 2  -->
							<div class="book_2page_left_style" >
					        	<img id="book_comic_image_left" src="" width="100%" height="100%" />
								<!--<div class="page_top_right"></div>
					            <div class="page_content_right" 2></div>
					            <div class="page_bottom_right"></div>-->
							</div>
					        <div class="book_2page_right_style" >
								<img id="book_comic_image_right" src="" width="100%" height="100%" />
								<!--<div class="page_top"></div>
					            <div class="page_content"></div>
					            <div class="page_bottom"></div>-->
							</div>
					     </div>
					  </div>';
            }
        }else{
            if($PageMode==1){
                $TotalWidth = floor($WidthPerPage * 1.4);
                $x = '<div id="book_comic">
					     <div id="book_comic_content" class="book_page_comic_1page" style="width:'.$TotalWidth.'px;">
							<img id="book_page_style" src="" width="100%" height="100%" />
							<!--<div id="book_page_style" class="book_1page_style" style="background-image: none; ;">
								<div class="page_top"></div>
					            <div class="page_content"></div>
					            <div class="page_bottom"></div>
							</div>-->
					     </div>
					  </div>';
            }else{
                $TotalWidth = $TotalWidth +20;
                $x = '<div id="book_comic">
					     <div id="book_comic_content" style="width: '.$TotalWidth.'px;" class="book_page_comic_2page"><!-- width="image width x 2  -->
												        
							<div id="book_comic_image_left" style="background-image: url(\'\');" class="book_2page_left_style" >
					        	<div class="page_top_right"></div>
					            <div id="page_content_right" class="main_content page_content_right" 1></div>
					            <div class="page_bottom_right"></div>
							</div>
							
					        <div id="book_comic_image_right" style="background-image: url(\'\');" class="book_2page_right_style" >
								<div class="page_top"></div>
					            <div id="page_content_left" class="main_content page_content">' .
                    '<!--<a href="http://www.w3schools.com" target="_blank">Visit W3Schools</a>-->' .
                    '</div>
					            <div class="page_bottom"></div>
							</div>
					     </div>
					  </div>';
            }
        }
        return $x;
    }
    /*
    function Get_Book_Comic_Image_Layer($left_image_path, $right_image_path)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $x = '<div id="book_comic_image_left" style="background-image: url(\''.$left_image_path.'\');" class="book_2page_left_style" >
                    <div class="page_top_right"></div>
                    <div class="page_content_right"></div>
                    <div class="page_bottom_right"></div>
              </div>
              <div id="book_comic_image_right" style="background-image: url(\''.$right_image_path.'\');" class="book_2page_right_style" >
                    <div class="page_top"></div>
                    <div class="page_content"></div>
                    <div class="page_bottom"></div>
              </div>';

        return $x;
    }
    */
    function Get_Book_Comic_BG_Structure_Layer($WidthPerPage, $PageMode=2)
    {
        global $intranet_root, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $Lang;

        $paperStyle = empty($_SESSION['eBookReaderPaperStyle'])? 'white' : $_SESSION['eBookReaderPaperStyle'];
        if($PageMode==1){
            $TotalWidth = floor($WidthPerPage * 1.4) + 39;
            $x = '<div id="book_structure" class="book_1page_structure" style="width:'.$TotalWidth.'px;">
					<div id="book_page_style" class="book_1page_style page_right_'.$paperStyle.'_style">
						<div class="page_top">
							<div class="page_top_right">
								<div class="page_top_bg">
								</div>
							</div>
						</div>
					    <div class="page_content">
							<div class="page_content_right" 9>
								<div class="page_content_bg">
					            	<div class="page_pattern">&nbsp;</div>
					            </div>
							</div>
						</div>
					    <div class="page_bottom">
							<div class="page_bottom_right">
								<div class="page_bottom_bg"></div>
							</div>
						</div>
					</div>
				</div>';
            $x.='<p class="spacer"></p>'."\n";
        }else{
            $TotalWidth = $WidthPerPage * 2 + 36;
            $x = '<div style="width: '.$TotalWidth.'px;" class="book_2page_structure" id="book_structure"><!-- width="image width x 2 +36 -->
					<div class="book_2page_left_style  page_left_'.$paperStyle.'_style " id="book_page_style">
			        	<div class="page_top">
							<div class="page_top_right">
								<div class="page_top_bg">
								</div>
							</div>
						</div>
			            <div class="page_content">
							<div class="page_content_right" 8>
								<div class="page_content_bg">
			            			<div class="page_pattern">&nbsp;</div>
			            		</div>
							</div>
						</div>
			            <div class="page_bottom" style="margin-top:-3px;">
							<div class="page_bottom_right">
								<div class="page_bottom_bg">&nbsp;
			           			</div>
							</div>
						</div>
					</div>
			        <div class="book_2page_right_style page_right_'.$paperStyle.'_style " id="book_page_style">
			        	<div class="page_top">
							<div class="page_top_right">
								<div class="page_top_bg">
								</div>
							</div>
						</div>
			            <div class="page_content">
							<div class="page_content_right" 7>
								<div class="page_content_bg">
			            			<div class="page_pattern">&nbsp;</div>
			            		</div>
							</div>
						</div>
			            <div class="page_bottom" style="margin-top:-3px;">
							<div class="page_bottom_right">
								<div class="page_bottom_bg"></div>
							</div>
						</div>
					</div>
				</div>';
        }

        return $x;
    }

}
?>