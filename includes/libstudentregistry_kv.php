<?php
/*
 * Modifying:
 *
 *  Purpose:    customized student registry library for Kentville Kindergarten [case #Y140316]
 *
 *  2020-04-16 Cameron
 *      - don't add guardian record if all related fields are empty in syncStudentRegistryToAccount()
 *      - update EnglishName only when both FirstName and LastName are not empty in syncStudentRegistryToAccount()
 *      - retrieve ID_NO in getSyncStudentList()
 *      - sync HKID in syncStudentRegistryToAccount() if it's not empty
 *      
 *  2020-04-06 Cameron
 *      - modify syncStudentRegistryToAccount(), return true if $studentInfoAry is empty
 *      
 *  2019-11-19 Cameron
 *      - add related functions for synchronization of student registry to account:
 *      syncStudentRegistryToAccount(), getSyncEmergencyContact(), getSyncGuardianList(), getSyncStudentList(),
 *      getRelationshipList(), getDistrictList(), getNationalityList(), getPlaceOfBirthList()
 *
 *  2019-10-29 Cameron
 *      - add function getStudentNameListWClassNumberByClassName()
 *
 *  2019-10-25 Cameron
 *      - add function getGardianStudentRecordID(), getGardianStudentExtRecordID(), isGudianStudentExtExist(),
 *      addGuardianStudentSql(), addGuardianStudentExtSql(), updateGuardianStudentSql(), updateGuardianStudentExtSql()
 *
 *  2019-08-15 Cameron
 *      - modify getSearchStudentID() so that confirm submitted is by AcademicYearID [case #Y164458]
 *      - modify getStudentInfoBasic() to retrieve DATA_CONFIRM_DATE and DATA_CONFIRM_BY from STUDENT_REGISTRY_SUBMITTED
 *
 *  2019-06-24 Cameron
 *      - add parameter $engOnly in getDistrictSelection()
 *      
 *  2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 * 
 *  2019-03-11 Cameron
 *      - create this file
 */
include_once ($intranet_root . "/includes/libstudentregistry.php");
if (! defined("LIBSUTDENTREGISTRY_KV_DEFINED")) // Preprocessor directives
{
    define("LIBSTUDENTREGISTRY_KV_DEFINED", true);

    class libstudentregistry_kv extends libstudentregistry
    {
        function getStudentInfoBasic($studentID, $academicYearID='')
        {
            if (!$academicYearID) {
                $academicYearID = Get_Current_Academic_Year_ID();
            }
            $startDate = getStartDateOfAcademicYear($academicYearID);
            $endDate = getEndDateOfAcademicYear($academicYearID);
            
            if (is_array($studentID)) {
                $cond = " a.UserID in ('".implode("','",$studentID)."') ";
            }
            else {
                $cond = " a.UserID = '$studentID' ";
            }
            
            // school bus retrieve normal bus route name
            $nameField = getNameFieldByLang("f.");
            $nameField_submit = getNameFieldByLang("g.");
            $sql = "SELECT
						a.UserID,
						a.EnglishName,
						a.ChineseName,
						a.Gender,						
						a.HomeTelNo,
						IF(b.ClassTitleEN IS NULL OR b.ClassTitleEN = '', '--', b.ClassTitleEN) AS ClassName,
						IF(b.ClassNumber IS NULL OR b.ClassNumber = '', '--', b.ClassNumber) AS ClassNumber,
						/*IF(a.ClassName IS NULL OR a.ClassName = '', '--', a.ClassName) AS ClassName,
						IF(a.ClassNumber IS NULL OR a.ClassNumber = '', '--', a.ClassNumber) AS ClassNumber,*/
						d.DateModified,
						d.ModifyBy,
						IF(c.YearName IS NULL OR c.YearName = '', '--', c.YearName) AS YearName,
						b.YearClassID,
						a.RecordStatus,
						DATE_FORMAT(a.DateOfBirth, '%Y-%m-%d') as DOB,
                        d.B_PLACE_CODE,
                        d.B_PLACE_OTHERS,
						d.ID_NO,
                        d.ETHNICITY_CODE,
                        d.RACE_OTHERS,						
						d.FAMILY_LANG,
                        d.FAMILY_LANG_OTHERS,
						a.UserEmail,
						d.ADDRESS_ROOM_EN,
						d.ADDRESS_FLOOR_EN,
						d.ADDRESS_BLK_EN,
						d.ADDRESS_BLD_EN,
						d.ADDRESS_EST_EN,
						d.ADDRESS_STR_EN,
						d.ADDRESS_AREA,
                        d.ADDRESS_STR_CH,
                        d.ADDRESS_DISTRICT_EN,
						d.LAST_SCHOOL_EN,
						". $nameField ." as ModifyByName,
						a.FirstName,
						a.LastName,
						d.DISTRICT_CODE,						
                        d.NATION_CODE,
                        d.NATION_OTHERS,
						a.UserLogin,
						". $nameField_submit ." as SubmitByName,
						srs2.DATA_CONFIRM_DATE,						
                        sb.SCHOOL_BUS,
                        d.NANNY_BUS
					FROM
						INTRANET_USER as a
                        LEFT JOIN (SELECT   yc.YearClassID,
                        					yc.ClassTitleEN, 
                        					ycu.ClassNumber, 
                        					ycu.UserID,
                        					yc.YearID
                    				FROM
                    					    YEAR_CLASS_USER ycu
                    				INNER JOIN
                    					    YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                    				WHERE 
                    					    yc.AcademicYearID='". $academicYearID ."'
                    				) AS b ON b.UserID=a.UserID
                        /*LEFT JOIN YEAR_CLASS as b on (b.ClassTitleEN = a.ClassName AND b.AcademicYearID='". $academicYearID ."')*/
						LEFT JOIN YEAR as c on (c.YearID = b.YearID)
						LEFT JOIN STUDENT_REGISTRY_STUDENT as d on (a.UserID = d.UserID)
						LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as e on (e.UserID=a.UserID)
						LEFT JOIN INTRANET_USER as f on (f.UserID=d.ModifyBy)
						LEFT JOIN STUDENT_REGISTRY_SUBMITTED srs2 ON (srs2.UserID=a.UserID AND srs2.AcademicYearID='".$academicYearID."')
						LEFT JOIN INTRANET_USER as g on (g.UserID=srs2.DATA_CONFIRM_BY AND srs2.AcademicYearID='".$academicYearID."')
                        LEFT JOIN (
                            SELECT 
                            	r.RouteName AS SCHOOL_BUS,
                                ur.UserID
                            FROM
                            	INTRANET_SCH_BUS_ROUTE r
                            INNER JOIN
                            	INTRANET_SCH_BUS_ROUTE_COLLECTION c ON c.RouteID=r.RouteID
                            INNER JOIN
                            	INTRANET_SCH_BUS_USER_ROUTE ur ON ur.RouteCollectionID=c.RouteCollectionID
                            WHERE
                            	ur.AcademicYearID=r.AcademicYearID
                            	AND ur.AcademicYearID='".$academicYearID."'
                            	AND ur.DateStart>='".$startDate."'
                            	AND ur.DateEnd<='".$endDate."'
                            	AND c.Weekday IS NULL
                            	AND c.IsDeleted=0
                            	AND r.IsDeleted=0
                            	AND r.StartDate>='".$startDate."'
                            	AND r.EndDate<='".$endDate."'
                            	AND r.Type='N'
                            GROUP BY ur.UserID                        
                        ) AS sb ON sb.UserID=a.UserID                        
					WHERE
						$cond
						";
			$result = $this->returnResultSet($sql);
//debug_pr($sql);			
			if (!is_array($studentID)) {
			    $result = current($result);
			}
			else {
			    $result = BuildMultiKeyAssoc($result, array('UserID'));
			}
			return $result;
            
        }
        
        function getGuardianInfo($studentID='', $seq=1)
        {
            $result = array();
            
            if (is_array($studentID)) {
                $cond = " StudentID in ('".implode("','",$studentID)."') ";
            }
            else {
                $cond = " StudentID = '$studentID' ";
            }
            
            $sql = "SELECT
                            StudentID,
                            PG_TYPE,
                            NAME_C,
                            NAME_E,
                            LAST_NAME_E,
                            FIRST_NAME_E,
                            JOB_TITLE,
                            JOB_NATURE,
                            TEL,
                            MOBILE,
                            EMAIL,
                            ADDRESS_ROOM_EN,
                            ADDRESS_FLOOR_EN,
                            ADDRESS_BLK_EN,
                            ADDRESS_BLD_EN,
                            ADDRESS_EST_EN,
                            ADDRESS_STR_EN,
                            ADDRESS_DISTRICT_EN,
                            ADDRESS_AREA,
                            DISTRICT_CODE,
                            BUSINESS_NAME,
                            IS_ALUMNI,
                            GRADUATE_YEAR
                    FROM
                            STUDENT_REGISTRY_PG
                    WHERE
                            $cond
                            AND SEQUENCE='$seq'
                    ";
            $result = $this->returnResultSet($sql);

            if (is_array($studentID)) {
                $result = BuildMultiKeyAssoc($result, 'StudentID');
            }
            else {
                $result = current($result);
            }
            return $result;            
        }
        
        function getEmergencyContactInfo($studentID='')
        {
            $result = array();
            
            if (is_array($studentID)) {
                $cond = " StudentID in ('".implode("','",$studentID)."') ";
            }
            else {
                $cond = " StudentID = '$studentID' ";
            }
            
            $sql = "SELECT
                            StudentID,
                            RecordID,
                            Contact3_Title,
                            Contact3_EnglishName,
                            Contact3_ChineseName,
                            Contact3_RelationshipCode,
                            Contact3_Relationship,
                            Contact3_Phone,
                            Contact3_Mobile,
                            Contact3_EglishLastName,
                            Contact3_EglishFirstName
                    FROM
                            STUDENT_REGISTRY_EMERGENCY_CONTACT
                    WHERE
                            $cond
                            ";
            $result = $this->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($result);
            if (is_array($studentID)) {
                $result = BuildMultiKeyAssoc($result, array('StudentID','RecordID'));
            }
            
            return $result;
            
        }
        
        function getStudentKeyInfo($studentID) 
        {
            $sql = "SELECT UserLogin, UserEmail, EnglishName, ChineseName, WebSAMSRegNo FROM INTRANET_USER WHERE UserID='$studentID'";
            $result = $this->returnResultSet($sql);
            return current($result);
        }
        
        function getGenderDisplay($gender)
        {
            global $Lang;
            switch ($gender) {
                case 'F':
                    $ret = $Lang['General']['Female'];
                    break;
                case 'M':
                    $ret = $Lang['General']['Male'];
                    break;
                default:
                    $ret = '--';
                    break;
            }
            return $ret;
        }
        
        function getGenderAry()
        {
            global $Lang;
            $ary = array();
            $ary['M'] = $Lang['General']['Male'];
            $ary['F'] = $Lang['General']['Female'];
            return $ary;
        }

        function getAreaCode($areaVal)
        {
            switch ($areaVal) {
                case 1:
                    $code = 'HK';
                    break;
                case 2:
                    $code = 'KLN';
                    break;
                case 3:
                    $code = 'NT';
                    break;
                default:
                    $code = 'NA';
                    break;
            }
            return $code;
        }
        
        function getAreaVal($areaCode)
        {
            switch ($areaCode) {
                case 'HK':
                    $val = 1;
                    break;
                case 'KLN':
                    $val = 2;
                    break;
                case 'NT':
                    $val = 3;
                    break;
                default:
                    $val = 4;
                    break;
            }
            return $val;
        }
        
        function getPresetCodeList($code_type="", $excludeOthers=true) 
        {
            $sql = "select Code, ".Get_Lang_Selection("NameChi","NameEng")." AS Name from PRESET_CODE_OPTION where CodeType='". $code_type ."'";
            if ($excludeOthers) {
                $sql .= " AND Code<>'999' ";
            }
            $sql .= "and RecordStatus=1 order by DisplayOrder";
            $result = $this->returnResultSet($sql);
            return $result;
        }
        
        function getPresetCodeEnglishName($code_type="", $selected_code="")
        {
            $sql = "SELECT NameEng FROM PRESET_CODE_OPTION WHERE CodeType='". $code_type ."' AND Code='". $selected_code ."'";
            $result = $this->returnResultSet($sql);
            return count($result) ? $result[0]['NameEng'] : '';
        }
        
        function getPresetCodeEnglishSelection($code_type="", $selection_name="", $code_selected="", $noFirst=1, $id="", $groupByName=false)
        {
            if ($groupByName) {
                $code = "MIN(Code) AS Code";
                $groupBy = "GROUP BY NameEng";
            }
            else {
                $code = "Code";
                $groupBy = "";
            }
            $sql = "SELECT $code, NameEng FROM PRESET_CODE_OPTION WHERE CodeType='". $code_type ."' AND RecordStatus=1 $groupBy ORDER BY DisplayOrder";
            $result = $this->returnArray($sql);
            
            if ($code_type=='RELATIONSHIP' && $code_selected) {
                $sql = "SELECT NameEng FROM PRESET_CODE_OPTION WHERE CodeType='". $code_type ."' AND Code='".$code_selected."' AND RecordStatus=1";
                $rs = $this->returnResultSet($sql);
                if (count($rs)) {
                    $selectedNameEng = $rs[0]['NameEng'];
                    foreach((array)$result as $_idx=>$_resultAry) {
                        if ($_resultAry['NameEng'] == $selectedNameEng) {
                            $result[$_idx][0] = $code_selected;             // force to use selected code for Family Member Relationship in English
                            $result[$_idx]['Code'] = $code_selected;
                        }
                    }
                }
            }
            
            $displayID = $id ? " id=".$id : "";
            
            return getSelectByArray($result, "name=".$selection_name.$displayID, $code_selected,0,$noFirst);
        }
        
        function getDistrictSelection($selection_name="", $code_selected="", $engOnly=false)
        {
            if ($engOnly) {
                $subDistrictName = "s.NameEng";
                $districtName = "d.NameEng";
                $firstTitle = "-- Please Select --";
            }
            else {
                $subDistrictName = Get_Lang_Selection("s.NameChi","s.NameEng");
                $districtName = Get_Lang_Selection("d.NameChi","d.NameEng");
                $firstTitle = "";
            }
        
            $sql = "SELECT  s.Code AS SubDistrictCode, 
                            ".$subDistrictName." AS SubDistrictName,
                            d.Code AS DistrictCode,
                            ".$districtName." AS DistrictName
                    FROM 
                            PRESET_CODE_OPTION s
                    LEFT JOIN
                            PRESET_CODE_OPTION d ON d.Code=LEFT(s.Code,1) AND d.CodeType='DISTRICT'
                    WHERE 
                            s.CodeType='SUBDISTRICT' 
                            AND s.RecordStatus=1 
                    ORDER BY 
                            s.DisplayOrder";
            
            $subDistrictAry = $this->returnResultSet($sql);
            $subDistrictAry = BuildMultiKeyAssoc($subDistrictAry, array('DistrictName', 'SubDistrictCode'), array('SubDistrictName'),$singleValue=true);
            $ret = getSelectByAssoArray($subDistrictAry, "name=".$selection_name." class=district", $code_selected, $all=0, $noFirst=0, $firstTitle);
            return $ret;
        }
        
        
        function getBroterAndSister($studentID)
        {
            if (is_array($studentID)) {
                $cond = " StudentID in ('".implode("','",$studentID)."') ";
            }
            else {
                $cond = " StudentID = '$studentID' ";
            }
            
            $sql = "SELECT *, IF(DateOfBirth IS NULL, 0,(YEAR(CURDATE())-YEAR(DateOfBirth))) AS Age FROM STUDENT_REGISTRY_BROSIS WHERE $cond ORDER BY Age, StudentName";
            $result = $this->returnResultSet($sql);
            if (is_array($studentID)) {
                $result = BuildMultiKeyAssoc($result, array('StudentID','RecordID'));
            }
            return $result;
        }
        
        function isDuplicateCertificateNo($studentID, $id_no)
        {
            $isDuplicate = false;
            
            $sql = "select count(UserID) AS CountUser from INTRANET_USER where HKID='$id_no' and UserID!='$studentID'";
            $result = $this->returnResultSet($sql);
            if($result[0]['CountUser']>0) {
                $isDuplicate = true;
            }
            
//             $sql = "select count(StudentID) AS CountUser from STUDENT_REGISTRY_PG where HKID='$id_no' and StudentID!='$studentID'";
//             $result = $this->returnResultSet($sql);
//             if($result[0]['CountUser']>0) {
//                 $isDuplicate = true;
//             }
            
            $sql = "select count(UserID) AS CountUser from INTRANET_USER_PERSONAL_SETTINGS where PassportNo='$id_no' and UserID!='$studentID'";
            $result = $this->returnResultSet($sql);
            if($result[0]['CountUser']>0) {
                $isDuplicate = true;
            }
            
            $sql = "select count(UserID) AS CountUser from STUDENT_REGISTRY_STUDENT where ID_NO='$id_no' and UserID!='$studentID'";
            $result = $this->returnResultSet($sql);
            if($result[0]['CountUser']>0) {
                $isDuplicate = true;
            }
                
            return $isDuplicate;                
        }
        
        function isDuplicateEmail($studentID, $email)
        {
            $isDuplicate = false;
            $sql = "select count(StudentID) AS CountUser from STUDENT_REGISTRY_PG where EMAIL='$email' and StudentID!='$studentID'";
            $result = $this->returnResultSet($sql);
            if($result[0]['CountUser']>0) {
                $isDuplicate = true;
            }
            return $isDuplicate;
        }
        
        function isStudentRegistryExist($studentID)
        {
            $isExist = false;
            $sql = "SELECT COUNT(*) AS CountUser FROM STUDENT_REGISTRY_STUDENT WHERE UserID='$studentID'";
            $result = $this->returnResultSet($sql);
            if($result[0]['CountUser']>0) {
                $isExist= true;
            }
            return $isExist;
        }
        
        function isParentGuardianExist($studentID, $sequence)
        {
            $isExist = false;
            $sql = "SELECT SRPG_ID FROM STUDENT_REGISTRY_PG WHERE StudentID='$studentID' and SEQUENCE='". $sequence."'";
            $result = $this->returnResultSet($sql);
            if(count($result)) {
                $isExist= true;
            }
            return $isExist;
        }

        function isEmergencyContactExist($studentID, $sequence, $setSequence=false)
        {
            $isExist = false;
            $sql = "SELECT RecordID FROM STUDENT_REGISTRY_EMERGENCY_CONTACT WHERE StudentID='$studentID' and Sequence='". $sequence."'";
            $result = $this->returnResultSet($sql);
            if(count($result)) {
                $isExist= true;
            }
            else {
                $sql = "SELECT RecordID FROM STUDENT_REGISTRY_EMERGENCY_CONTACT WHERE StudentID='$studentID' and Sequence='0' ORDER BY RecordID LIMIT 1";
                $result = $this->returnResultSet($sql);
                if(count($result)) {
                    $isExist= true;
                    if ($setSequence) {
                        $sql = "UPDATE STUDENT_REGISTRY_EMERGENCY_CONTACT SET Sequence='".$sequence."' WHERE RecordID='".$result[0]['RecordID']."'";
                        $res = $this->db_db_query($sql);
                    }
                }                
            }
            return $isExist;
        }
        
        function verifyParentChild($studentID, $parentID) 
        {
            $isExist = false;
            $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '".$parentID."' AND StudentID='".$studentID."'";
            $result = $this->returnResultSet($sql);
            if(count($result)) {
                $isExist= true;
            }
            return $isExist;
        }
        
        function getSearchStudentID()
        {
            $conds = "";
            
            if ($_GET['StudentID']) {
                $conds .= " AND u.UserID='".IntegerSafe($_GET['StudentID'])."'";
            }
            if (!isset($_POST['AcademicYearID'])) {
                $academicYearID = Get_Current_Academic_Year_ID();
            }
            else {
                $academicYearID = IntegerSafe($_POST['AcademicYearID']);
            }
            
            if($_POST['targetClass']!="") {
                if(is_numeric($_POST['targetClass'])) {
                    $conds .= " AND y.YearID=".$_POST['targetClass'];
                }
                else {
                    $conds .= " AND yc.YearClassID='".IntegerSafe(str_replace("::","",$_POST['targetClass']))."'";
                }
            }
            
            $searchText = $_POST['searchText'];
            if($searchText!="") {
                $conds .= " AND (
					".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." LIKE '%$searchText%' OR
					u.EnglishName LIKE '%$searchText%' OR
					u.ChineseName LIKE '%$searchText%' OR
					srs.ID_NO LIKE '%$searchText%' OR
					u.HomeTelNo LIKE '%$searchText%'
					)";
            }
            unset($searchText);
            
            if($_POST['submit_status']==1) {
                $conds .= " and srs2.DATA_CONFIRM_DATE is not null";
            }
            
            if($_POST['submit_status']==-1)
            {
                $conds .= " and srs2.DATA_CONFIRM_DATE is null";
            }
            
            if ($_POST['IncludeEverUpdatedOnly']){
                $conds .= " and srs.DateModified IS NOT NULL";
            }
            if ($_POST['StartDate'] != '') {
                if ($_POST['IncludeEverUpdatedOnly']){
                    $conds .= " and srs.DateModified >='".$_POST['StartDate']." 00:00:00'";
                }
                else {
                    $conds .= " and (srs.DateModified >='".$_POST['StartDate']." 00:00:00' or srs.DateModified IS NULL)";
                }
            }
            if ($_POST['EndDate'] != '') {
                if ($_POST['IncludeEverUpdatedOnly']){
                    $conds .= " and srs.DateModified <='".$_POST['EndDate']." 23:59:59'";
                }
                else {
                    $conds .= " and (srs.DateModified <='".$_POST['EndDate']." 23:59:59' or srs.DateModified IS NULL)";
                }
            }
            
            $name_field = getNameFieldByLang("u.");
            
            $cond2 = ($_POST['IncludeLeft'] == '1') ? " AND u.RecordType IN(2,4) AND u.RecordStatus IN (0,1,2,3)" : " AND u.RecordType=2 AND u.RecordStatus IN (0,1,2)";
            
            $sql = "SELECT
                            u.UserID
                    FROM
                            INTRANET_USER u LEFT OUTER JOIN
                            YEAR_CLASS_USER ycu ON (ycu.UserID=u.UserID) LEFT OUTER JOIN
                            YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
                            YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
                            STUDENT_REGISTRY_STUDENT srs ON (srs.UserID=u.UserID) LEFT OUTER JOIN
                            STUDENT_REGISTRY_SUBMITTED srs2 ON (srs2.UserID=u.UserID AND srs2.AcademicYearID='".$academicYearID."')
                    WHERE
                            yc.AcademicYearID='".$academicYearID."'
                            {$cond2}
                            $conds
                    ORDER BY
                            yc.ClassTitleEN, ycu.ClassNumber
                    ";
            $studentIDAry = $this->returnResultSet($sql);
            $studentIDAry = Get_Array_By_Key($studentIDAry,'UserID');
            
            return $studentIDAry;
        }
        
        function removeSiblings($studentID)
        {
            # remove all existing records
            $sql = "delete from STUDENT_REGISTRY_BROSIS where StudentID='$studentID'";
            $ret = $this->db_db_query($sql);
            return $ret;
        }

        function getGardianStudentRecordID($userID, $englishName)
        {
            global $eclass_db;

            $sql = "SELECT RecordID FROM $eclass_db.GUARDIAN_STUDENT WHERE UserID='".$userID."' AND EnName='".$englishName."'";
            $recordAry = $this->returnResultSet($sql);
            return count($recordAry) ? $recordAry[0]['RecordID'] : '';
        }

        function getGardianStudentExtRecordID($recordID)
        {
            global $eclass_db;

            $sql = "SELECT ExtendRecordID FROM $eclass_db.GUARDIAN_STUDENT_EXT_1 WHERE RecordID='".$recordID."'";
            $recordAry = $this->returnResultSet($sql);
            return count($recordAry) ? $recordAry[0]['ExtendRecordID'] : '';
        }

        function isGudianStudentExtExist($recordID)
        {
            global $eclass_db;

            $sql = "SELECT ExtendRecordID FROM $eclass_db.GUARDIAN_STUDENT_EXT_1 WHERE RecordID='".$recordID."'";
            $checkRecordAry = $this->returnResultSet($sql);
            return count($checkRecordAry) ? true : false;
        }

        function addGuardianStudentSql($userID, $englishName, $chineseName, $relationShip, $mobile, $tel='')
        {
            global $eclass_db;

            $dataAry = array();
            $dataAry['UserID'] = $userID;
            $dataAry['EnName'] = $englishName;
            $dataAry['ChName'] = $chineseName;
            $dataAry['Relation'] = $relationShip;
            $dataAry['Phone'] = $tel;
            $dataAry['EmPhone'] = $mobile;
            $dataAry['InputDate'] = "now()";
            $dataAry['ModifiedDate'] = "now()";

            $condAry = array();
            $sql = $this->insert2Table("$eclass_db.GUARDIAN_STUDENT", $dataAry, $condAry, $run = false, $insertIgnore = false, $updateDateModified=false);
            return $sql;
        }

        function addGuardianStudentExtSql($userID, $recordID, $occupation, $companyName, $isLiveTogether=1)
        {
            global $eclass_db;

            $dataAry = array();
            $dataAry['UserID'] = $userID;
            $dataAry['RecordID'] = $recordID;
            $dataAry['Occupation'] = $occupation;
            $dataAry['CompanyName'] = $companyName;
            $dataAry['IsLiveTogether'] = $isLiveTogether;
            $dataAry['InputDate'] = "now()";
            $dataAry['ModifiedDate'] = "now()";

            $condAry = array();
            $sql = $this->insert2Table("$eclass_db.GUARDIAN_STUDENT_EXT_1",  $dataAry, $condAry, $run=false, $insertIgnore = false, $updateDateModified=false);
            return $sql;
        }

        function updateGuardianStudentSql($recordID, $englishName, $chineseName, $tel, $mobile)
        {
            global $eclass_db;

            $dataAry = array();
            if ($englishName) {
                $dataAry['EnName'] = $englishName;
            }
            if ($chineseName) {
                $dataAry['ChName'] = $chineseName;
            }
            if ($tel) {
                $dataAry['Phone'] = $tel;
            }
            if ($mobile) {
                $dataAry['EmPhone'] = $mobile;
            }
            $dataAry['ModifiedDate'] = "now()";

            $condAry = array();
            $condAry['RecordID'] = $recordID;
            $sql = $this->update2Table("$eclass_db.GUARDIAN_STUDENT", $dataAry, $condAry, $run = false, $updateDateModified=false);
            return $sql;
        }

        function updateGuardianStudentExtSql($extendRecordID, $occupation, $company)
        {
            global $eclass_db;

            $dataAry = array();
            if ($occupation) {
                $dataAry['Occupation'] = $occupation;
            }
            if ($company) {
                $dataAry['CompanyName'] = $company;
            }
            $dataAry['ModifiedDate'] = "now()";

            $condAry = array();
            $condAry['ExtendRecordID'] = $extendRecordID;
            $sql = $this->update2Table("$eclass_db.GUARDIAN_STUDENT_EXT_1", $dataAry, $condAry, $run = false, $updateDateModified = false);
            return $sql;
        }

        // for IP, compare against ClassTitleEN, exclude left student
        public function getStudentNameListWClassNumberByClassName($className, $includeUserIdAry = '', $excludeUserIdAry = '', $assoc = false)
        {
            $conds_userId = '';
            $recordstatus = "'0','1','2'";

            if ($includeUserIdAry != '') {
                $conds_userId .= " AND u.UserID IN ('" . implode("','", (array) $includeUserIdAry) . "') ";
            }

            if ($excludeUserIdAry != '') {
                $conds_userId .= " AND u.UserID NOT IN ('" . implode("','", (array) $excludeUserIdAry) . "') ";
            }

            $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
            $username_field = getNameFieldByLang("u.", $displayLang = "", $isTitleDisabled = true);

            $classNameField = 'ClassTitleEN'; // use English ClassName
            $student_name = "CONCAT($username_field,IF(ycu.ClassNumber IS NULL OR ycu.ClassNumber='','',CONCAT(' (',yc.{$classNameField},'-',ycu.ClassNumber,')'))) ";

            $sql = "SELECT 		u.UserID, 
                                {$student_name} AS StudentName							 
                    FROM 
                                INTRANET_USER u 
                    INNER JOIN 
                                YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
                    INNER JOIN 
                                YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID 
                    AND 	
                                yc.AcademicYearID='" . $CurrentAcademicYearID . "' 
                    AND		
                                yc.ClassTitleEN='" . $this->Get_Safe_Sql_Query($className) . "' 
                    WHERE 
                                u.RecordType=2
                    AND 
                                u.RecordStatus IN ($recordstatus) 
                                {$conds_userId}
                    ORDER BY ycu.ClassNumber+0";

            $rs = $assoc ? $this->returnResultSet($sql) : $this->returnArray($sql); // need returnArray for getSelectByArray function

            return $rs;
        }

        function getPlaceOfBirthList()
        {
            $sql = "SELECT Code, NameEng FROM PRESET_CODE_OPTION WHERE CodeType='PLACEOFBIRTH' ORDER BY Code";
            $placeOfBirthAry = $this->returnResultSet($sql);
            $placeOfBirthAssoc = BuildMultiKeyAssoc($placeOfBirthAry,array('Code'),array('NameEng'),1);
            return $placeOfBirthAssoc;
        }
        
        function getNationalityList()
        {
            $sql = "SELECT Code, NameEng FROM PRESET_CODE_OPTION WHERE CodeType='NATIONALITY' ORDER BY Code";
            $nationalityAry = $this->returnResultSet($sql);
            $nationalityAssoc = BuildMultiKeyAssoc($nationalityAry,array('Code'),array('NameEng'),1);
            return $nationalityAssoc;
        }
        
        function getDistrictList()
        {
            $sql = "SELECT Code, NameEng FROM PRESET_CODE_OPTION WHERE CodeType='SUBDISTRICT' ORDER BY Code";
            $districtAry = $this->returnResultSet($sql);
            $districtAssoc = BuildMultiKeyAssoc($districtAry,array('Code'),array('NameEng'),1);
            return $districtAssoc;            
        }
        
        function getRelationshipList()
        {
            $sql = "SELECT 
                        Code, 
                        CASE NameChi
                            WHEN '爺爺' THEN '03'
                            WHEN '嫲嫲' THEN '04'
                            WHEN '姑母' THEN '07'
                            WHEN '姨母' THEN '07'
                            WHEN '嬸嬸' THEN '07'
                            WHEN '伯母' THEN '07'
                            WHEN '舅母' THEN '07'
                            WHEN '外公' THEN '07'
                            WHEN '外婆' THEN '07'
                            WHEN '舅父' THEN '07'
                            WHEN '伯父' THEN '07'
                            WHEN '叔父' THEN '07'
                            WHEN '姨父' THEN '07'
                            WHEN '姑父' THEN '07'
                            WHEN '其他' THEN '08'
                            WHEN '工人' THEN '08'
                            ELSE '08'
                        END AS Relationship
                    FROM 
                        PRESET_CODE_OPTION 
                    WHERE 
                        CodeType='RELATIONSHIP' 
                        ORDER BY Code+0";
            $relationshipAry = $this->returnResultSet($sql);
            $relationshipAssoc = BuildMultiKeyAssoc($relationshipAry,array('Code'),array('Relationship'),1);
            return $relationshipAssoc;
        }
        
        function getSyncStudentList($studentIDAry)
        {
            $studentIDStr = implode("','",(array)$studentIDAry);
            
            // get student info from INTRANET_USER and STUDENT_REGISTRY_STUDENT
            $sql = "SELECT
                        u.UserID,
                        u.FirstName,
                        u.LastName, 
                        s.B_PLACE_CODE, 
                        s.B_PLACE_OTHERS,
                        s.ID_NO, 
                        s.NATION_CODE,
                        s.NATION_OTHERS,
                        s.ADDRESS_ROOM_EN,
                        s.ADDRESS_FLOOR_EN,
                        s.ADDRESS_BLK_EN,
                        s.ADDRESS_BLD_EN,
                        s.ADDRESS_EST_EN,
                        s.ADDRESS_STR_EN,
                        s.ADDRESS_AREA,
                        s.DISTRICT_CODE
                    FROM
                        INTRANET_USER u
                        INNER JOIN STUDENT_REGISTRY_STUDENT s ON s.UserID=u.UserID
                    WHERE
                        u.UserID IN ('".$studentIDStr."')
                        ORDER BY u.UserID";

            $studentInfoAry = $this->returnResultSet($sql);
            return $studentInfoAry;
        }
        
        function getSyncGuardianList($studentIDAry)
        {
            $studentIDStr = implode("','",(array)$studentIDAry);
            $sql = "SELECT 
                        StudentID,
                        NAME_E,
                        NAME_C,
                        PG_TYPE as Relation,
                        TEL,
                        MOBILE,
                        JOB_NATURE,
                        BUSINESS_NAME
                    FROM 
                        STUDENT_REGISTRY_PG
                    WHERE 
                        StudentID IN ('".$studentIDStr."')
                        ORDER BY StudentID";
            $guardianAry = $this->returnResultSet($sql);
            $guardianAssoc = BuildMultiKeyAssoc($guardianAry, array('StudentID', 'Relation'));
            return $guardianAssoc;             
        }
        
        function getSyncEmergencyContact($studentIDAry)
        {
            $studentIDStr = implode("','",(array)$studentIDAry);
            $sql = "SELECT
                        StudentID, 
                        Contact3_EnglishName,
                        Contact3_ChineseName,
                        Contact3_RelationshipCode,
                        Contact3_Relationship,
                        Contact3_Mobile,
                        Sequence
                    FROM
                        STUDENT_REGISTRY_EMERGENCY_CONTACT 
                    WHERE
                        StudentID IN ('".$studentIDStr."')
                        ORDER BY StudentID";
            $emergencyContactAry = $this->returnResultSet($sql);
            $emergencyContactAssoc = BuildMultiKeyAssoc($emergencyContactAry, array('StudentID', 'Sequence'));
            return $emergencyContactAssoc;
        }
        
        function syncStudentRegistryToAccount($studentIDAry)
        {
            global $enableDebug, $Lang;

            $allResultAry = array();
            $placeOfBirthAssoc = $this->getPlaceOfBirthList();
            $nationalityAssoc = $this->getNationalityList();
            $districtAssoc = $this->getDistrictList();
            $relationshipAssoc = $this->getRelationshipList();

            $studentInfoAry = $this->getSyncStudentList($studentIDAry);
            $guardianAssoc = $this->getSyncGuardianList($studentIDAry);
            $emergencyContactAssoc = $this->getSyncEmergencyContact($studentIDAry);
            
            if ($enableDebug) {
                echo "studentInfoList<br>";
                debug_pr($studentInfoAry);
            }
            
            if (count($studentInfoAry)){
                foreach((array)$studentInfoAry as $_studentInfoAry) {

                    $resultAry = array();
                    // commit one student at a time rather than waiting to commit all
                    $this->Start_Trans();

                    $_userID = $_studentInfoAry['UserID'];
                    $_studentInfoAry['LastName'] = trim($_studentInfoAry['LastName']);
                    $_studentInfoAry['FirstName'] = trim($_studentInfoAry['FirstName']);
                    if (($_studentInfoAry['LastName'] != '') && ($_studentInfoAry['FirstName'] != '')) { 
                        $_englishName = ucwords(strtolower($_studentInfoAry['LastName'] . ' ' . $_studentInfoAry['FirstName']));        // convert first letter to upper case for each word
                    }
                    else {
                        $_englishName = '';
                    }

                    $_placeOfBirthCode = $_studentInfoAry['B_PLACE_CODE'];
                    $_placeOfBirth = ($_placeOfBirthCode == '999') ? $_studentInfoAry['B_PLACE_OTHERS'] : $placeOfBirthAssoc[$_placeOfBirthCode];   // use English Name

                    $_nationalityCode = $_studentInfoAry['NATION_CODE'];
                    $_nationality = ($_nationalityCode == '999') ? $_studentInfoAry['NATION_OTHERS'] : $nationalityAssoc[$_nationalityCode];

                    $_districtCode = $_studentInfoAry['DISTRICT_CODE'];
                    $_district = ($_districtCode == '999') ? $_studentInfoAry['ADDRESS_DISTRICT_EN'] : $districtAssoc[$_districtCode];

                    $_englishAddress = '';
                    if ($_studentInfoAry['ADDRESS_ROOM_EN']) {
                        $_flat = strtoupper($_studentInfoAry['ADDRESS_ROOM_EN']);
                        if ((strpos($_flat, "FLAT") === false) && (strpos($_flat, "FT") === false) && (strpos($_flat, "RM") === false) && (strpos($_flat, "ROOM") === false)) {
                            $_englishAddress .= 'Flat ' . $_studentInfoAry['ADDRESS_ROOM_EN'] . ',';
                        }
                        else {
                            $_englishAddress .= $_studentInfoAry['ADDRESS_ROOM_EN'] . ',';
                        }
                    }
                    if ($_studentInfoAry['ADDRESS_FLOOR_EN']) {
                        $_floor = $_studentInfoAry['ADDRESS_FLOOR_EN'];
                        $_englishAddress .= strtoupper($_floor);
                        $_englishAddress .= (strpos($_floor, "/F") === false) ? '/F,' : ',';
                    }
                    if ($_studentInfoAry['ADDRESS_BLK_EN']) {
                        $_block = strtoupper($_studentInfoAry['ADDRESS_BLK_EN']);
                        if ((strpos($_block, "BLK") === false) && (strpos($_block, "BLOCK") === false)) {
                            $_englishAddress .= 'Block ' . $_studentInfoAry['ADDRESS_BLK_EN'] . ',';
                        }
                        else {
                            $_englishAddress .= $_studentInfoAry['ADDRESS_BLK_EN'] . ',';
                        }
                    }
                    if ($_studentInfoAry['ADDRESS_BLD_EN']) {
                        if ($_englishAddress) {
                            $_englishAddress .= "\n";
                        }
                        $_englishAddress .= $_studentInfoAry['ADDRESS_BLD_EN'];
                    }
                    if ($_studentInfoAry['ADDRESS_EST_EN']) {
                        if ($_englishAddress) {
                            $_englishAddress .= "\n";
                        }
                        $_englishAddress .= $_studentInfoAry['ADDRESS_EST_EN'];
                    }
                    if ($_studentInfoAry['ADDRESS_STR_EN']) {
                        if ($_englishAddress) {
                            $_englishAddress .= "\n";
                        }
                        $_englishAddress .= $_studentInfoAry['ADDRESS_STR_EN'];
                    }
                    if ($_englishAddress && $_district) {
                        $_englishAddress .= "\n".$_district;
                    }

                    $_addressArea = $Lang['StudentRegistry']['EnglishAreaOptions'][$_studentInfoAry['ADDRESS_AREA']-1];
                    if ($_englishAddress && $_addressArea) {
                        $_englishAddress .= ($_district ? ", " : "\n").$_addressArea;
                    }
                    $_englishAddress = rtrim($_englishAddress,',');


                    ## 1. update student EnglishName and Address
                    $dataAry = array();
                    $updateStudentInfo = false;
                    if ($_englishName) {
                        $dataAry['EnglishName'] = $_englishName;
                        $updateStudentInfo = true;
                    }
                    $_studentInfoAry['ID_NO'] = trim($_studentInfoAry['ID_NO']);
                    if ($_studentInfoAry['ID_NO']) {
                        $dataAry['HKID'] = $_studentInfoAry['ID_NO'];
                        $updateStudentInfo = true;
                    }
                    if ($_englishAddress) {
                        $dataAry['Address'] = $_englishAddress;
                        $updateStudentInfo = true;
                    }
                    $dataAry['ModifyBy'] = $_SESSION['UserID'];

                    if ($updateStudentInfo) {
                        $condAry = array();
                        $condAry['UserID'] = $_userID;
                        $sql = $this->update2Table('INTRANET_USER', $dataAry, $condAry, $run = false, $updateDateModified = true);
                        $result = $this->db_db_query($sql);
                        $resultAry['Update_INTRANET_USER_'.$_userID] = $result;
                        if ($enableDebug) {
                            echo "Update_INTRANET_USER_$_userID = $result<br>";
                            echo "$sql <br>";
                        }
                    }
                    

                    ## 2. update Nationality, Place of Birth
                    if ($_nationality || $_placeOfBirth) {
                        // check if record already exist or not
                        $sql = "SELECT UserID FROM INTRANET_USER_PERSONAL_SETTINGS WHERE UserID = '". $_userID . "'";
                        $checkRecordAry = $this->returnResultSet($sql);

                        if (count($checkRecordAry)) {
                            $dataAry = array();
                            if ($_nationality) {
                                $dataAry['Nationality'] = $_nationality;
                                $dataAry['Nationality_DateModified'] = "now()";
                            }
                            if ($_placeOfBirth) {
                                $dataAry['PlaceOfBirth'] = $_placeOfBirth;
                                $dataAry['PlaceOfBirth_DateModified'] = "now()";
                            }

                            $condAry = array();
                            $condAry['UserID'] = $_userID;
                            $sql = $this->update2Table('INTRANET_USER_PERSONAL_SETTINGS', $dataAry, $condAry, $run = false, $updateDateModified=false);
                            $result = $this->db_db_query($sql);
                            $resultAry['Update_PERSONAL_SETTINGS_'.$_userID] = $result;
                            if ($enableDebug) {
                                echo "Update_PERSONAL_SETTINGS_$_userID = $result<br>";
                                echo "$sql <br>";
                            }
                        }
                        else {
                            $dataAry = array();
                            $dataAry['UserID'] = $_userID;
                            $dataAry['Nationality'] = $_nationality;
                            $dataAry['Nationality_DateModified'] = "now()";
                            $dataAry['PlaceOfBirth'] = $_placeOfBirth;
                            $dataAry['PlaceOfBirth_DateModified'] = "now()";

                            $condAry = array();
                            $sql = $this->insert2Table('INTRANET_USER_PERSONAL_SETTINGS', $dataAry, $condAry, $run=false, $insertIgnore = false, $updateDateModified=false);
                            $result = $this->db_db_query($sql);
                            $resultAry['Insert_PERSONAL_SETTINGS_' . $_userID] = $result;
                            if ($enableDebug) {
                                echo "Insert_PERSONAL_SETTINGS_$_userID = $result<br>";
                                echo "$sql <br>";
                            }
                        }
                    }       // end update Nationality, Place of Birth


                    ## 3. update Guardians
                    if ($guardianAssoc[$_userID]) {
                        foreach($guardianAssoc[$_userID] as $__relation=>$__rs){
                            $_gEnglishName = trim($__rs['NAME_E']);
                            $_gChineseName = trim($__rs['NAME_C']);
                            $_gRelationship = $__rs['Relation'] == 'F' ? '01' : '02';      // 01 - Father, 02 - Mother
                            $_gTel = trim($__rs['TEL']);
                            $_gMobile = trim($__rs['MOBILE']);
                            $_gOccupation = trim($__rs['JOB_NATURE']);
                            $_gCompany = trim($__rs['BUSINESS_NAME']);

                            $_recordID = $this->getGardianStudentRecordID($_userID, $_gEnglishName);

                            if ($_recordID) {
                                if ($_gEnglishName || $_gChineseName || $_gTel || $_gMobile) {
                                    $sql = $this->updateGuardianStudentSql($_recordID, $_gEnglishName, $_gChineseName, $_gTel, $_gMobile);
                                    $result = $this->db_db_query($sql);
                                    $resultAry['Update_GUARDIAN_STUDENT_'.$_recordID] = $result;
                                    if ($enableDebug) {
                                        echo "Update_GUARDIAN_STUDENT_$_recordID = $result<br>";
                                        echo "$sql <br>";
                                    }
                                }

                                if ($_gOccupation || $_gCompany) {
                                    $_extendRecordID = $this->getGardianStudentExtRecordID($_recordID);
                                    if ($_extendRecordID) {
                                        $sql = $this->updateGuardianStudentExtSql($_extendRecordID, $_gOccupation, $_gCompany);
                                        $result = $this->db_db_query($sql);
                                        $resultAry['Update_GUARDIAN_STUDENT_EXT_1_' . $_userID] = $result;
                                        if ($enableDebug) {
                                            echo "Update_GUARDIAN_STUDENT_EXT_1_$_userID = $result<br>";
                                            echo "$sql <br>";
                                        }
                                    } else {
                                        $sql = $this->addGuardianStudentExtSql($_userID, $_recordID, $_gOccupation, $_gCompany);
                                        $result = $this->db_db_query($sql);
                                        if ($result) {
                                            $_extendRecordID = $this->db_insert_id();
                                        }
                                        $resultAry['Insert_GUARDIAN_STUDENT_EXT_1_' . $_recordID] = $result;
                                        if ($enableDebug) {
                                            echo "Insert_GUARDIAN_STUDENT_EXT_1_$_recordID = $result<br>";
                                            echo "$sql <br>";
                                        }
                                    }
                                }
                            }
                            else {      // guardian record not exist, insert it
                                if ($_gEnglishName || $_gChineseName || $_gTel || $_gMobile) {
                                    $sql = $this->addGuardianStudentSql($_userID, $_gEnglishName, $_gChineseName, $_gRelationship, $_gMobile, $_gTel);
                                    $result = $this->db_db_query($sql);
                                    if ($result) {
                                        $_recordID = $this->db_insert_id();
                                    }
                                    $resultAry['Insert_GUARDIAN_STUDENT_'.$_userID.'_'.$_gRelationship] = $result;
                                    if ($enableDebug) {
                                        echo "Insert_GUARDIAN_STUDENT_$_userID"."_".$_gRelationship." = $result<br>";
                                        echo "$sql <br>";
                                    }

                                    if ($_recordID) {
                                        $sql = $this->addGuardianStudentExtSql($_userID, $_recordID, $_gOccupation, $_gCompany);
                                        $result = $this->db_db_query($sql);
                                        if ($result) {
                                            $_extendRecordID = $this->db_insert_id();
                                        }
                                        $resultAry['Insert_GUARDIAN_STUDENT_EXT_1_' . $_recordID] = $result;
                                        if ($enableDebug) {
                                            echo "Insert_GUARDIAN_STUDENT_EXT_1_$_recordID = $result<br>";
                                            echo "$sql <br>";
                                        }
                                    }
                                }
                                else {
                                    // do nothing if all are empty
                                }
                            }
                        }
                    }       // end update Guardian

                    ## 4 update emergency contact person
                    if ($emergencyContactAssoc[$_userID]) {
                        foreach ($emergencyContactAssoc[$_userID] as $__sequence => $__rs) {
                            $_gEnglishName = trim($__rs['Contact3_EnglishName']);
                            $_gChineseName = trim($__rs['Contact3_ChineseName']);
                            $_relationshipCode = trim($__rs['Contact3_RelationshipCode']);
                            $_gRelationship = $relationshipAssoc[$_relationshipCode];
                            $_gMobile = trim($__rs['Contact3_Mobile']);

                            // if English Name of the contact person for the student is the same, assume it's the same person

                            $_recordID = $this->getGardianStudentRecordID($_userID, $_gEnglishName);

                            if ($_recordID) {   // update
                                $sql = $this->updateGuardianStudentSql($_recordID, $_gEnglishName, $_gChineseName, $_tel='', $_gMobile);
                                $result = $this->db_db_query($sql);
                                $resultAry['Update_GUARDIAN_STUDENT_EMC_'.$_recordID] = $result;
                                if ($enableDebug) {
                                    echo "Update_GUARDIAN_STUDENT_EMC_$_recordID = $result<br>";
                                    echo "$sql <br>";
                                }

                                // no need to update GUARDIAN_STUDENT_EXT_1 as there's no company and occupation fields in emergency contact in student registry

                                // check if extend record exist, add it if not
                                $isGudianStudentExtExist = $this->isGudianStudentExtExist($_recordID);

                                if (!$isGudianStudentExtExist) {
                                    // addGuardianStudentExt
                                    $sql = $this->addGuardianStudentExtSql($_userID, $_recordID, $_occupation='', $_companyName='', $isLiveTogether=0);

                                    $result = $this->db_db_query($sql);
                                    if ($result) {
                                        $_extendRecordID = $this->db_insert_id();
                                    }
                                    $resultAry['Insert_GUARDIAN_STUDENT_EXT_1_' . $_recordID] = $result;
                                    if ($enableDebug) {
                                        echo "Insert_GUARDIAN_STUDENT_EXT_1_$_recordID = $result<br>";
                                        echo "$sql <br>";
                                    }
                                }
                            }
                            else {      // insert
                                if ($_gEnglishName || $_gChineseName || $_gMobile) {
                                    // addGuardianStudent
                                    $sql = $this->addGuardianStudentSql($_userID, $_gEnglishName, $_gChineseName, $_gRelationship, $_gMobile);
                                    $result = $this->db_db_query($sql);
                                    if ($result) {
                                        $_recordID = $this->db_insert_id();
                                    }
                                    $resultAry['Insert_GUARDIAN_STUDENT_EMC_'.$_userID.'_'.$__sequence] = $result;
                                    if ($enableDebug) {
                                        echo "Insert_GUARDIAN_STUDENT_EMC_$_userID"."_".$__sequence." = $result<br>";
                                        echo "$sql <br>";
                                    }
    
                                    // addGuardianStudentExt
                                    $sql = $this->addGuardianStudentExtSql($_userID, $_recordID, $_occupation='', $_companyName='', $isLiveTogether=0);
                                    $result = $this->db_db_query($sql);
    
                                    if ($result) {
                                        $_extendRecordID = $this->db_insert_id();
                                    }
                                    $resultAry['Insert_GUARDIAN_STUDENT_EXT_1_' . $_recordID] = $result;
                                    if ($enableDebug) {
                                        echo "Insert_GUARDIAN_STUDENT_EXT_1_$_recordID = $result<br>";
                                        echo "$sql <br>";
                                    }
                                }
                                else {
                                    // don't add record if all are empty
                                }
                            }
                        }
                    }       // end update emergency contact person

                    if ($enableDebug) {
                        echo "User $_userID Sync result <br>";
                        debug_pr($resultAry);
                    }

                    if (!in_array(false, $resultAry)) {
                        $this->Commit_Trans();
                        $allResultAry[$_userID] = true;
                    }
                    else {
                        $this->RollBack_Trans();
                        $allResultAry[$_userID] = false;

                        // force to show fail error
                        if (!$enableDebug) {
                            echo "User $_userID Sync result <br>";
                            debug_pr($resultAry);
                        }
                    }

                }       // loop number of students

                return in_array(false, $allResultAry) ? false : true;
            }
            else {
                return true;        // return true if there's no record
            }
            
        }   // end function syncStudentRegistryToAccount
        
    }   // -- End of Class
}

?>