<?php
/*
 * 	modify by: 
 * 
 * 	Log
 *
 *  2019-12-10 Tommy
 *      - add "index.php?clearCoo=1" to call admin index page
 *
 *  2019-10-09 Cameron
 *      - show thismonth option for ebook_most_hit_range in getPortalSettingField(), getRankingHeader() and getOpacRankingHeader() [case #X157865]
 *      - allow range=thismonth in geteBookSlides() for most_hit
 * 
 *  2019-07-04 Cameron
 *      - add column NumOfChiBookRead and NumOfEngBookRead in getStatisticsAllClassesSummaryContent() [case #A163164]
 *
 *  2019-06-12 Henry
 *      - modified getPortalOpeningHoursItemUI to fix the wrong css of holiday
 *      
 *  2019-01-15 Cameron
 *      - fix: add onclick="void(0)" to li element in getStarRatingUI() and getReviewEditUI() to set it as clickable element for iPad [case #X155667]
 *      
 *  2018-12-12 Pun  [ip.2.5.10.1.1]/[ej.5.0.9.2.1]
 *      - Modified getBookItemsNextToCover(), added isbn
 *
 *  2018-09-26 [Cameron]
 *      - add $showEditButton, $reviewID, $showAnchor to getStarRatingUI(), getReviewItemUI() [case #W139304]
 *      - add function getReviewEditUIByReviewID(), getBookReviewContentUI()
 *      
 *  2018-07-26 [Cameron]
 *      - fix: show book category name according to login language in getBookItemsNextToCover()
 *      
 *  2018-06-26 [Cameron]
 *      - modify getPortalTopPart() and getOpacPortalTopPart() to get SchoolName from eLib+, if not exist, get it from system. [case #P141566]
 *      
 *  2018-06-21 [Cameron]
 *      - load first top tab(current loan) on first load, don't preload other tabs (loan history, penalty, lost record) in getMyRecordSideTabLoanBookRecordUI()
 *      - add parameter $nav_para to getMyRecordLoanRecordUI() [case #Y140487]
 *      - add function getMyRecordLoanRecordContent()
 *      - set class=active to the outermost div in getMyRecordLoanRecordUI(), getMyRecordPenaltyUI(), getMyRecordLostBookUI() 
 *      
 *  2018-02-28 [Cameron]
 *      - fix: set $bookCategoryType to empty array rather than empty string when there's no record in getBookCatalogUI() [case #W135968]
 *      
 *	2017-12-12 [Cameron]
 *		- modify getBookCatalogUI() to show BookCategoryType in tab [case #F130026]
 *
 * 	2017-10-25 [Cameron]
 * 		- add hyperlink to User Name when he/she becomes my friends in getMyFriendList(), getMyFriendRankingViewersUI(),
 * 		getMyFriendRankingBorrowersUI(), getMyFriendRankingReviewersUI()
 *  
 * 	2017-10-16 [Cameron]
 * 		- show pending friend list (waiting for accept) in getMyFriendList() and getMyFriendSearchFriendList()
 * 		- add function getMyFriendPendingFriend()
 * 
 * 	2017-09-29 [Cameron]
 * 		- modify getTopMenuBar() to allow staff use 'My Friends' function
 * 		- add function getUserListToAddFriend(), getMyFriendSearchFriendList()
 * 		- add search by keyword of my friends 
 * 
 * 	2017-09-28 [Cameron]
 * 		- modify getAvailableStudentByClass() by calling getMyFriendExcludeUser()
 * 
 * 	2017-09-27 [Cameron]
 * 		- add my_friends_scope to getPortalSettingForm() and getPortalSettingView()
 * 		- add function getMyFriendSearchToAddFriend()
 * 
 * 	2017-09-25 [Cameron]
 * 		- fix: don't show "Special Time" when library status is "Closed" in getPortalOpeningHoursItemUI() [case #G124658]
 * 
 * 	2017-09-14 [Cameron]
 * 		- add $is_open_loan in getMyRecordCurrentLoan() to control renew book (case #R125534)
 * 
 * 	2017-08-10 [Cameron]
 * 		- check if $userInfo in getHeaderItemsUI() to avoid error if User account is not synchronized from IP to eLib+
 * 
 * 	2017-07-13 [Henry]
 * 		- modified getPortalOpeningHoursItemUI() to fix the calender problem [Case#C117663]
 * 
 * 	2017-07-13 [Cameron]
 * 		- add open_my_firends function control in getPortalSettingForm() and getPortalSettingView()
 * 		- apply open_my_firends setting to getTopMenuBar()
 *
 * 	2017-05-18 [Cameron]
 * 		- replace " with &quot; for hyperlink title in geteBookCover(), getpBookCover(), getBookItemsNextToCover()
 * 
 *	2017-05-11 [Cameron]
 *		- modify getAvailableStudentByClass() to support parameter difference in calling getStudentNameListByClassName()
 *		- to support both ej and ip in the same function, conditionally call convert2unicode for ej by modifying following functions: 
 *			getHeaderItemsUI(), geteLibCloseUI(), getPortalTopPart(), getOpacPortalTopPart()
 *		- modify getStatisticsAllClassesHeader() to support both ej & ip 
 *  
 * 	2017-03-28 ~ 2017-04-24 (Cameron)
 * 		- add my friend functions
 * 
 * 	2017-04-06 [Cameron]
 * 		- modify getHeaderItemsUI() & getOpacHeaderItemsUI()
 * 			to handle apostrophe problem for search field by applying stripslashes($keyword) when get_magic_quotes_gpc() is not set (php5.4). [case #N115357]
 * 
 * 	2017-01-18 [Cameron]
 * 		- change tags title in getBookCatalogUI(): do not show "(All Books)" if eBooks only or physical books only
 * 
 * 	2017-01-17 [Cameron]
 * 		- align carousel and control button in center if there's no ranking box in geteBookCarousel(), getpBookCarousel(), geteBookControlButton(), getpBookControlButton()
 * 		- don't show eBook if client does not purchase, don't show pBook if it's $plugin['eLib_Lite'] in getRankingHeader()
 * 		- if $plugin['eLib_Lite'] == true, hide pBook related setting in getPortalSettingView() and getPortalSettingForm()
 * 		- don't show Most Hit if client does not purchase eBook in getRankingLeftMenu(), don't show Most Loan and Most Active Borrowers if $plugin['eLib_Lite'] == true 
 * 
 * 	2017-01-16 [Cameron]
 * 		- add parameter $isPurchasedeBook to getPortalSettingView() and getPortalSettingForm(), hide eBook related setting if client does not purchase any eBook
 * 
 * 	2017-01-10 [Cameron]
 * 		- remove width and height of image in getRankingMostActiveBorrowers(), getRankingMostActiveReviewers(), getRankingMostHelpfulReview(), getReviewItemUI() 
 * 			as css class changed [case# H106629]
 * 
 * 	2016-11-21 [Cameron]
 * 		- displaying expired book row in dimmed in getStatisticsAllReviewsContent(), getStatisticsByStudentContent(), getMyRecordMyReadingRecordContent(),
 * 			getMyRecordMyNotesContent(), getMyRecordMyFavouriteListView(), getRankingListviewBooks()
 * 		- add checking if data exist in getStatisticsByClassSummaryContent()
 * 
 * 	2016-11-16 [Cameron]
 * 		- add argument $classNameWithBracket=false to getMostActiveUsers() in geteBookRankingTable() and getpBookRankingTable(), 
 * 			getMostUsefulReviews() in getMostUserfulReviewsTable(),
 * 			getMostBorrowUsers() in getpBookRankingTable()
 * 
 * 	2016-11-11 [Cameron] 
 * 		- use $permission['portal_setting'] to control portal setting button in getTopMenuBar() [case #Z108442]
 * 
 * 	2016-05-18 [Cameron] create this file
 * 
 */  
 
 define("SIXTY", 60);
 
 class libelibplus_ui {
	 
	# JQuery Datepicker generator (copy and modify from libinterface, use bootstrap style and icon)
	function GET_DATE_PICKER($Name,$DefaultValue="",$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="form-control", $calendarIcon=true) {
		global $Defined,$Lang;
		
		$DateFormat = ($DateFormat == "")? "yy-mm-dd":$DateFormat;
		if(!$CanEmptyField)
			$DefaultValue = ($DefaultValue == "")? date('Y-m-d'):$DefaultValue;
		$ID = ($ID == "")? $Name:$ID;
		$WarningLayerID = ($ExtWarningLayerID == "")? 'DPWL-'.$ID:$ExtWarningLayerID;
		$Disabled = ($Disable)? 'disabled' : '';
		
		$x = "";
		if(!$CanEmptyField)
			$x .= '<input type=text name="'.$Name.'" id="'.$ID.'" value="'.$DefaultValue.'" size=10 maxlength=10 class="'.$cssClass.'" '.$OtherMember.' onkeyup="Date_Picker_Check_Date_Format(this,\''.$WarningLayerID.'\',\''.$ExtWarningLayerContainer.'\');" '.$Disabled.'>';
		else
			$x .= '<input type=text name="'.$Name.'" id="'.$ID.'" value="'.$DefaultValue.'" size=10 maxlength=10 class="'.$cssClass.'" '.$OtherMember.' onkeyup="Date_Picker_Check_Date_Format_CanEmpty(this,\''.$WarningLayerID.'\',\''.$ExtWarningLayerContainer.'\');" '.$Disabled.'>';
		if ($calendarIcon) {
			$x .= '	<span class="input-group-addon">
                    	<span id="dp_icon_'.$ID.'" class="glyphicon glyphicon-calendar"></span>
                    </span>';
		}
		if ($ExtWarningLayerID == "")
			$x .= '<span style="color:red;" id="'.$WarningLayerID.'"></span>';
		$x .= "\n";
		
		if ($Defined<1) {
			if($SkipIncludeJS!=1)
			{
				$x .= '<link rel="stylesheet" href="/templates/jquery/jquery.datepick.5.0.1.css" type="text/css" />
						<script type="text/javascript" src="/templates/jquery/jquery.plugin.1.0.1.min.js"></script>
						<script type="text/javascript" src="/templates/jquery/jquery.datepick.5.0.1.min.js"></script>';
			}			

					$x .= '<script>
							$.datepick.setDefaults({
								autoSize: false,
								buttonText: \'Calendar\'});
						
						function Date_Picker_Check_Date_Format(DateObj,WarningLayer,WarningLayerContainer) { ';
						
						$x .= 'if (!check_date_without_return_msg(DateObj)) {';
						
			if ($ExtWarningLayerContainer != "") {
				$x .= '	if (WarningLayerContainer != "") 
									$(\'#\'+WarningLayerContainer).css(\'display\',\'\');';
			}
			$x .= '		$(\'#\'+WarningLayer).html(\''.$Lang['General']['InvalidDateFormat'].'\'); 
							}
							else {';
			if ($ExtWarningLayerContainer != "") {
				$x .= '	if (WarningLayerContainer != "") 
									$(\'#\'+WarningLayerContainer).css(\'display\',\'none\');';
			}
			$x .= '		$(\'#\'+WarningLayer).html(\'\');
							}
						}
						
						function Date_Picker_Check_Date_Format_CanEmpty(DateObj,WarningLayer,WarningLayerContainer) { 
							';
						
						$x .= 'if (!check_date_allow_null_30(DateObj)) {';
						
			if ($ExtWarningLayerContainer != "") {
				$x .= '	if (WarningLayerContainer != "") 
									$(\'#\'+WarningLayerContainer).css(\'display\',\'\');';
			}
			$x .= '		$(\'#\'+WarningLayer).html(\''.$Lang['General']['InvalidDateFormat'].'\'); 
							}
							else {';
			if ($ExtWarningLayerContainer != "") {
				$x .= '	if (WarningLayerContainer != "") 
									$(\'#\'+WarningLayerContainer).css(\'display\',\'none\');';
			}
			$x .= '		$(\'#\'+WarningLayer).html(\'\');
							}
						}
						
						</script>
						';
		}
		$x .= "<script>
						$('input#".$ID."').ready(function(){
							$('input#".$ID."').datepick({
								".(($MaskFunction != "")? 'beforeShowDay: '.$MaskFunction.',':'')."
								dateFormat: '".$DateFormat."',
								dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
								changeFirstDay: false,
								firstDay: 0,
								onSelect: function(dateText, inst) {
				";
							if($CanEmptyField)
									$x .= "Date_Picker_Check_Date_Format_CanEmpty(document.getElementById('".$ID."'),'".$WarningLayerID."','".$ExtWarningLayerContainer."');";
							else		
									$x .= "Date_Picker_Check_Date_Format(document.getElementById('".$ID."'),'".$WarningLayerID."','".$ExtWarningLayerContainer."');";
							
									$x .= $OnDatePickSelectedFunction."
									}
								});
							});
					</script>
					";
		
		$Defined++;
		return $x;
	}	// end GET_DATE_PICKER
 	
 	function geteLibCloseUI() {
		global $intranet_session_language, $Lang, $junior_mck, $LAYOUT_SKIN;
		ob_start();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>eLibrary plus</title>
<link href="/templates/<?=$LAYOUT_SKIN?>/css/bootstrap.min.css" rel="stylesheet">
<link href="/templates/<?=$LAYOUT_SKIN?>/css/elibplus2.css" rel="stylesheet">
</head>
<body>
<!--TOP starts-->
<div class="elib-top container-fluid">
  <div class="navbar">
  	<!--hamburger button-->
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span> <span class="icon-bar"></span><span class="icon-bar"></span></button>
    <div class="container">
      <div class="navbar-header"><a class="header-logo navbar-brand" href="#"><img height="60" alt="eLibrary plus" src="/images/<?=$LAYOUT_SKIN?>/eLibplus2/<?=($intranet_session_language=="en"?'logo-en.png':'logo-chi.png')?>"><?=$junior_mck ? convert2unicode(GET_SCHOOL_NAME(),true) : GET_SCHOOL_NAME()?></a></div>
      <div class="close"><?=$Lang["libms"]["portal"]["closing"]?></div>
    </div>
  </div>
</div>
<!--TOP ends-->
</body>
</html>		
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end geteLibCloseUI

 	/*
 	 * open fancybox for book details
 	 */
 	function getBookDetailFancybox() {
		$x .= "\$('.book-detail-fancybox').fancybox({
				maxWidth	: 1200,
				fitToView	: true,
				nextEffect	: 'fade',
				prevEffect	: 'fade',
				width		: '92%',
				height		: '92%',
				autoSize	: false,
				openEffect	: 'fade',
				closeEffect	: 'fade'
			});";
		return $x;
 	}	// end getBookDetailFancybox

 	/*
 	 * show message about add / update /delete status
 	 */
 	function getActionResultUI() {
 		global $Lang;
		$x = '<div id="system_message_box" class="SystemReturnMessage" style="display:block; visibility: hidden;"> 
				<div class="msg_board_left">
					<div class="msg_board_right" id="message_body"> 
				  		<a href="#" onclick="document.getElementById(\'system_message_box\').style.display = \'none\'; return false;">['.$Lang['Btn']['Clear'].']</a>
					</div>
				</div>
			  </div>';
		return $x;
 	}	// end getActionResultUI
 	
 	
 	// 5 star rating UI
	function getStarRatingUI($rating, $with_div=true, $showEditButton=false, $reviewID='', $showAnchor=false) {
	    global $Lang;
	    
	    $anchor = $showAnchor ? '<a></a>' : '';
	    
		$x  = $with_div ? '<div class="ratings">':'';
		$x .= ' <ul>';
		for ($i=0; $i<5; $i++) {
		    $x .= '<li class="'.($i<$rating? 'star-on': 'star-off').'" onclick="void(0)">'.$anchor.'</li>';
		}
		$x .='  </ul>';
		
		if ($showEditButton && !empty($reviewID)) {
		    $x .= '<div style="float:right;"><a href="ajax.php?action=editBookReview&ReviewID='.$reviewID.'" data-reviewID="'.$reviewID.'" class="btn-edit btnEditReview"><span class="glyphicon glyphicon-pencil"></span> '.$Lang['Btn']['Edit'].'</a></div>';
		}
		
		$x .= $with_div ? '</div>':'';
		return $x;
	}


	// return like / dislike UI
	function getLikesUI($data) {
		global $eLib_plus;
		$x = '<div class="review-likes" id="like_'.$data['review_id'].'">
	        	<a href="#" data-review_id="'.$data['review_id'].'" data-book_id="'.$data['book_id'].'" class="btn-type1">'.($data['is_like'] ? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"]).'</a>
	        	<span class="number-like">'.$data['likes'].'<span class="icon-like"></span></span>
	          </div>';
        return $x;
	}
	
	function getRankingPeriod($range,$id='',$class='',$by_friend='') {
		global $eLib_plus;
		$x = '<select id="'.$id.'" name="'.$id.'" class="'.$class.'">
	            <option value="thisweek" '.($range=='thisweek'?'selected':'').'>'.$eLib_plus["html"]["thisweek"].'</option>
	            <option value="accumulated'.$by_friend.'" '.($range=='accumulated'.$by_friend?'selected':'').'>'.$eLib_plus["html"]["accumulated"].'</option>
	          </select>';
	    return $x;
	}	 	
	
 	function getBookCatalogUI($tab_id_prefix='') {
 		global $sys_custom, $intranet_session_language, $eLib_plus, $PATH_WRT_ROOT, $plugin;
		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");		 		
	    $elibplus_cfg['category_limit'] = $elibplus_cfg['category_limit']?$elibplus_cfg['category_limit']:500;
	    $elibplus_cfg['eLearning_path'] = $elibplus_cfg['eLearning_path']?$elibplus_cfg['eLearning_path']:'/eLearning';

		$libelibplus = new libelibplus();		
		$book_category_chi = $libelibplus->get_portal_setting('book_category_chi_ebooks');
		$book_category_eng = $libelibplus->get_portal_setting('book_category_eng_ebooks');
		$book_category_others = $libelibplus->get_portal_setting('book_category_physical_books');
		$book_category_tags = $libelibplus->get_portal_setting('book_category_tags');
	    
	    $book_type 	= 'type_all';
		if (!isset($_SESSION['UserID']) || $_SESSION['UserID'] <= 0 ) {	// from opac
			$opac_search_ebook = $libelibplus->get_portal_setting('opac_search_ebook');
			if (!$opac_search_ebook) {
				$book_type 	= 'type_physical';
			}
		}
	    
 		$lelibplus = new elibrary_plus(0,0,$book_type);
 		if ($book_category_others) {
 			$bookCategoryType = $lelibplus->getBookCategoryType();
 			
 			if (count($bookCategoryType)) {
 				$bookCategoryTypeVal = array_values($bookCategoryType);
 				foreach((array)$bookCategoryType as $k=>$v) {
 					$bookCategoryTypeKey[$v] = $k;		// swap key / value for lookup
 				}
 			}
 		}
 		else {
 			$bookCategoryType = array();
 		}
		$book_catalog	= $lelibplus->getBookCatrgories($elibplus_cfg['category_limit'],$book_type);

	 	if (!$sys_custom['eLibraryPlus']['HideBookCategoryTags'] && $book_category_tags) {
			$book_catalog_tags	= $lelibplus->getBookTags();
 		}

	    $tabs = ($intranet_session_language=='en')? array('eng','chi'): array('chi','eng');
	    if (count($bookCategoryType)) {
	    	$tabs = array_merge($tabs,(array)$bookCategoryTypeVal);
	    }
	    
	    $selected_tab = 'tags';
	    foreach ((array)$tabs as $l){
    		$part = in_array($l,(array)$bookCategoryTypeVal) ? "others" : $l;
			if ($book_catalog[$l]['count']>0 && ${"book_category_{$part}"}) {
			    $selected_tab = $l;
			    break;
			}
	    }
 		
 		$tags = '';
 		## book categories
 		if (count($book_catalog) > 0) {
            foreach ((array)$book_catalog as $type=>$book_language) {
            	if (in_array($type,(array)$bookCategoryTypeVal)) {
            		$part = "others";
            		$id = $part.'_'.$bookCategoryTypeKey[$type];
            		$displayTab = $eLib_plus["html"][$part]."-".$type;
            	}
            	else {
            		$part = $type;
            		$id = $type;
            		$displayTab = $eLib_plus["html"][$type];
            	}
		        if (($book_language['categories']) && (${"book_category_{$part}"})) {
		        	$tags .= '<li class="'.($type==$selected_tab?'active':'').'"><a href="#'.$tab_id_prefix.$id.'" '.($tab_id_prefix?'':'data-toggle="tab"').'>'.$displayTab. ' ('.$book_language['count'].')</a></li>';
		        }
            }                                               
 		}

 		$isPurchasedeBook = $lelibplus->isPurchasedeBook();
 		if (!$plugin['eLib_Lite'] && $isPurchasedeBook) {
 			$tagsTitle = $eLib_plus["html"]["tags_all_books"];
 		}
 		else {
 			$tagsTitle = $eLib_plus["html"]["tags"];
 		}
 		
 		## tags
		if (!$sys_custom['eLibraryPlus']['HideBookCategoryTags'] && $book_category_tags && count($book_catalog_tags)>0) {
			$tags .= '<li class="'.($selected_tab=='tags'?'active':'').'"><a href="#'.$tab_id_prefix.'tags" '.($tab_id_prefix?'':'data-toggle="tab"').'>'.$tagsTitle. ' ('.count($book_catalog_tags).')</a></li>';
		}
		
		ob_start(); 		
?>
<!-- book categories -->
                    <ul class="nav nav-tabs<?=$tab_id_prefix?' pull-right':''?>">
                    	<?=$tags?>
                    </ul>
                    <div class="tab-content">
                    
<?php foreach ((array)$book_catalog as $type=>$book_language) : ?>
<?
    	if (in_array($type,(array)$bookCategoryTypeVal)) {
    		$part = "others";
    		$id = $part.'_'.$bookCategoryTypeKey[$type];
    	}
    	else {
    		$part = $type;
    		$id = $type;
    	}
?>
	<?php if (${"book_category_{$part}"}):?>
                      <div class="tab-pane <?=($type==$selected_tab?'active':'')?>" id="<?=$tab_id_prefix.$id?>">
                        <ul>
		<?php foreach ((array)$book_language['categories'] as $cat) {
				if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
					$catTitle = explode('-',$cat['title']);
					if (count($catTitle) > 2) {
						if ($intranet_session_language == 'en') {
							$dispTitle = $catTitle[0] . ' -' . $catTitle[1]; 
						}	
						else {
							$dispTitle = $catTitle[0] . ' -' . $catTitle[2];
						}
					}
					else {
						$dispTitle = $cat['title'];
					}
				}
				else {
					$dispTitle = $cat['title'];
				}
		
			    if (!$cat['subcategories']):?>
                          <li><a href="<?=('/home'.$elibplus_cfg['eLearning_path'])?>/elibplus2/advanced_search.php?search_type=category&language=<?=$id?>&category=<?=urlencode($cat['title'])?>"><?=$dispTitle.' ('.$cat['count'].')'?></a>
                          </li>
	  	  <?php else:?>
                          <li><a href="<?=('/home'.$elibplus_cfg['eLearning_path'])?>/elibplus2/advanced_search.php?search_type=category&language=<?=$id?>&category=<?=urlencode($cat['title'])?>"><?=$dispTitle.' ('.$cat['count'].')'?></a>
                          	<ul>
		                <?php foreach ((array)$cat['subcategories'] as $subcategories) :?>
				                <li><a href="<?=('/home'.$elibplus_cfg['eLearning_path'])?>/elibplus2/advanced_search.php?search_type=category&language=<?=$id?>&category=<?=urlencode($cat['title'])?>&subcategory=<?=urlencode($subcategories['title'])?>"><?=$subcategories['title']?> (<?=$subcategories['count']?>)</a></li>
        		        <?php endforeach; ?>
                          	</ul>
                          </li>
      	  <?php endif; ?>
		<?php } ?>
                        </ul>
                      </div>
	<?php endif; ?>                      
<?php endforeach; ?>

<!-- book tags -->
<?php if (!$sys_custom['eLibraryPlus']['HideBookCategoryTags'] && $book_category_tags): ?>
                      <div class="tab-pane<?=($selected_tab=='tags'?' active':'')?>" id="<?=$tab_id_prefix?>tags">            
                        <ul>
           <?php foreach ((array)$book_catalog_tags as $tag) : ?>
                          <li><a href="<?=('/home'.$elibplus_cfg['eLearning_path'])?>/elibplus2/advanced_search.php?tag_id=<?=$tag['tag_id']?>"><?=$tag['title']?> (<?=$tag['count']?>)</a></li>
           <?php endforeach; ?>
                        </ul>
                      </div>
<?php endif;?>
					</div>
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getBookCatalogUI()

 	
 	function getHeaderItemsUI() {
 		global $Lang, $eLib_plus, $intranet_session_language, $sys_custom, $junior_mck;

		# Basic Information
		$intranet_default_lang = $_SESSION['intranet_default_lang'] ? $_SESSION['intranet_default_lang'] : $intranet_default_lang;
		
		if ($intranet_default_lang == "gb" || get_client_region()=="zh_MY" || get_client_region()=="zh_CN")
			($intranet_session_language=="en") ? $lang_to_change = "gb" : $lang_to_change = "en";
		else
			($intranet_session_language=="en") ? $lang_to_change = "b5" : $lang_to_change = "en";
		$change_lang_to = $intranet_session_language=="en" ? $Lang['Header']['B5'] : $Lang['Header']['ENG'];
 		
		# user identity
		switch($_SESSION['UserType'])
		{
			case USERTYPE_STAFF:	
				$UserIdentity = $_SESSION['isTeaching']==1 ? $Lang['Identity']['TeachingStaff'] : $Lang['Identity']['NonTeachingStaff'];
				break;
			case USERTYPE_STUDENT:
				$UserIdentity = $Lang['Identity']['Student'];
				break;
			case USERTYPE_PARENT:
				$UserIdentity = $Lang['Identity']['Parent'];
				break;
			case USERTYPE_ALUMNI:
				$UserIdentity = $Lang['Identity']['Alumni'];
				break;
			default:
				$UserIdentity = "";
				break;
		}
 		
 		if ($UserIdentity) {
	 		$libelibplus = new libelibplus();
	 		$userInfo = $libelibplus->getUserInfo($_SESSION['UserID']);
	 		$dispUser = ($junior_mck ? convert2unicode($UserIdentity,true) : $UserIdentity) . ', ' . ($userInfo ? $userInfo[$Lang['libms']['SQL']['UserNameFeild']] : '');
 		}
 		
 		$search_type = $_POST['search_type'];
 		$keyword = $_POST['keyword'];
 		if ($search_type == 'search') {
 			$keyword = str_replace("ltltgtgt","<>",$keyword);
 		}
 		$keyword = trim($keyword);
		if (!get_magic_quotes_gpc()) {
			$keyword 	= stripslashes($keyword);
		}
		 		 		
 		ob_start();
?>
      <!--HEADER ITEMS starts-->
      <div class="container header-right-container">
        <div class="header-right">
        <?php if ($UserIdentity): ?>
          <div class="header-user dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span><?=$dispUser?><span class="caret"></span></a> 
            <!--USER MENU starts-->
            <ul class="dropdown-menu" role="menu">
              <li> <a href="javascript:logout()"><?=$Lang['Header']['Logout']?></a> </li>
            </ul>
            <!--USER MENU ends--> 
          </div>
          <?php endif;?>
          <div class="lang-switcher">
          <?php if(!$sys_custom['hide_lang_selectioin'][$_SESSION['UserType']] || ($sys_custom['hide_lang_selectioin_except_school_admin'] && $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {?> 
          	<a href="/lang.php?lang=<?= $lang_to_change?>"><?=$change_lang_to?></a>
          <?php }?> 
          </div>
          <div class="header-search">
            <div class="header-search-form form-group has-feedback">
              <form name="search_form" id="search_form" class="search" method="post" action="simple_search.php"> 
              	<input type="text" class="form-control input-sm" name="keyword" id="keyword" value="<?=$search_type=='simple_search'?intranet_htmlspecialchars($keyword):''?>" placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>">
              	<input type="hidden" name="search_type" value="simple_search" />
              	<input type="hidden" name="form_method" value="post">
              	<input type="hidden" name="update_navigation" value="1" />
              </form>
              <i class="glyphicon glyphicon-search form-control-feedback"></i> </div>
            <a href="/home/eLearning/elibplus2/advanced_search.php" class="header-search-text"><?=$Lang["libms"]["portal"]["advanced_search"]?><span class="caret-right"></span></a> </div>
        </div>
      </div>
      <!--HEADER ITEMS ends-->
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	} 	// end getHeaderItemsUI()


 	function getTopMenuBar() {
 		global $eLib_plus, $Lang, $PATH_WRT_ROOT, $elibplus2_menu_tab, $permission, $plugin;
 		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
 		$lelibplus = new elibrary_plus(0,$_SESSION['UserID']);
 		$notification_count 	= $lelibplus->getNotificationCount();
 		$friend_notification_count 	= $lelibplus->getFriendNotificationCount();

		$libelibplus = new libelibplus(); 		
 		$book_category_show_all = $libelibplus->get_portal_setting('book_category_show_all');
 		$open_my_friends = $libelibplus->get_portal_setting('open_my_friends');
 		
 		ob_start();
?>
      <!--MAIN MENU starts-->
      <div class="elib-menu container-fluid">
        <div class="container">
          <ul class="elib-main-menu nav nav-pills navbar-left">
            <li class="dropdown<?=($elibplus2_menu_tab == 'category' ? ' current' : '')?>"> <a class="dropdown-toggle book-category-menu" data-toggle="dropdown" href="#"><?=$eLib_plus["html"]["bookcategory"]?><span class="caret"></a>
              <ul class="dropdown-menu megamenu browse-cat">
              <? if ($book_category_show_all):?>
                <li class="list-all"><a href="<?=('/home'.$elibplus_cfg['eLearning_path'])?>/elibplus2/advanced_search.php?search_type=category" class=""><?=$eLib_plus["html"]["listallbooks"]?> <span class="caret-right"></span></a></li>
			  <? else:?>
			  	<li class="list-all"><span class="caret-right"></span></li>                
              <? endif;?>
                <form>
                  <div class="tabs general-tabs">
            		<?=$this->getBookCatalogUI()?>
                  </div>
                </form>
              </ul>
            </li>
            <li class="<?=($elibplus2_menu_tab == 'ranking' ? 'current' : '')?>"> <a href="/home/eLearning/elibplus2/ranking.php"><?=$eLib_plus["html"]["ranking"]?></a> </li>
            <li class="<?=($elibplus2_menu_tab == 'statistics' ? 'current' : '')?>"> <a href="/home/eLearning/elibplus2/statistics.php"><?=$eLib_plus["html"]["stats"]?></a> </li>
            <li class="<?=($elibplus2_menu_tab == 'myrecord' ? 'current' : '')?>"> <a href="/home/eLearning/elibplus2/myrecord.php"><?=$eLib_plus["html"]["myrecord"]?>
            <?php if ($notification_count): ?>
            	<span class="badge"><?=$notification_count?></span>
            <?php endif;?>
            	</a> </li>
		<? if ($open_my_friends && (($_SESSION['UserType'] == USERTYPE_STUDENT) || ($_SESSION['UserType'] == USERTYPE_STAFF))):?>            	
            <li class="<?=($elibplus2_menu_tab == 'myfriends' ? 'current' : '')?>"> <a href="/home/eLearning/elibplus2/myfriends.php"><?=$Lang["libms"]["portal"]["my_friends"]?>
            <?php if ($friend_notification_count): ?>
            	<span id="MyFriendNotificationCount" class="badge"><?=$friend_notification_count?></span>
            <?php endif;?>
            	</a> </li>
        <? endif;?>
          </ul>
          <!--teacher SETTING MENU starts-->
          <ul class="teacher elib-menu-settings nav nav-pills pull-right">
          
          <? if (!$plugin['eLib_Lite']):?>
          
          	<? if ($permission['circulation']):?>
            	<li> <a href="/home/library_sys/management/circulation/" class="icon-circulations"><?=$eLib_plus["html"]["circulations"]?></a> </li>
          	<? endif;?>  
          	<? if ($permission['setting']):?>
          		<li> <a href="/home/library_sys/admin/book/index.php?clearCoo=1" class="icon-admin"><?=$Lang["libms"]["portal"]["admin_settings"]?></a> </li>
          	<? endif;?>
          <? else:?>	
          	<? if ($_SESSION['LIBMS']['admin']['current_right']['admin']['book management'] || $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']){?>
          		<li> <a href="/home/library_sys/admin/book/elib_setting_license.php" class="icon-admin"><?=$Lang['libms']['bookmanagement']['ebook_license']?></a> </li>
			<? }?>            	
          <? endif;?>
          <? if ($permission['portal_setting']):?> 	
            	<li> <a href="portal_settings_edit.php" class="icon-admin"><?=$Lang["libms"]["portal"]["settings"]?></a> </li>
          <? endif;?>
          
            <!--<li> <a href="#" class="icon-theme"><?=$Lang["libms"]["portal"]["theme_settings"]?></a> </li>-->
          </ul>
          <!--teacher SETTING MENU ends--> 
        </div>
      </div>      
      <!--MAIN MENU ends-->
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	} 	// end getTopMenuBar()

 	
 	function getPortalTopPart() {
 		global $intranet_session_language, $junior_mck, $LAYOUT_SKIN;

 		// get school name from eLib+ settings
 		$libms = new liblms();
 		if ($junior_mck) {
 		    mysql_query("set names latin1");
 		}
 		$schoolChineseName = $libms->get_system_setting('school_display_name');
 		$schoolEnglishName = $libms->get_system_setting('school_display_name_eng');
 		$schoolName = Get_Lang_Selection($schoolChineseName,$schoolEnglishName);
 		if (!get_magic_quotes_gpc()) {
 		    $schoolName = stripslashes($schoolName);
 		}
 		
 		// if not exist, get it from system
 		if (empty($schoolName)) {
 		    $schoolName = $junior_mck ? convert2unicode(GET_SCHOOL_NAME(),true) : GET_SCHOOL_NAME();
 		}
 		
 		ob_start();
?> 		
<!--TOP starts-->
<div class="elib-top container-fluid">
  <div class="navbar">
  	<!--hamburger button-->
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span> <span class="icon-bar"></span><span class="icon-bar"></span></button>
    <div class="container">
      <div class="navbar-header"><a class="header-logo navbar-brand" href="/home/eLearning/elibplus2/index.php"><img height="60" alt="eLibrary plus" src="/images/<?=$LAYOUT_SKIN?>/eLibplus2/<?=($intranet_session_language=="en"?'logo-en.png':'logo-chi.png')?>"><?php echo $schoolName;?></a></div>
    </div>
    <!--MOBILE COLLAPSE content starts-->
    <div class="collapse navbar-collapse" id="navbar-ex-collapse">
    <?=$this->getHeaderItemsUI()?>
    <?=$this->getTopMenuBar()?> 
    </div>
    <!--MOBILE COLLAPSE content ends-->     
  </div>
</div>
<!--TOP ends-->
<?php  
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getPortalTopPart()
 	
 	
 	/*
 	 * 	$type		- recommend / most hit / new / best rated
 	 * 			
 	 */
	function geteBookCarousel($type='recommend') {
		global $eLib_plus, $Lang;
		$libelibplus = new libelibplus();
		$ebook_carousel_interval = $libelibplus->get_portal_setting('ebook_carousel_interval');
		$ebook_carousel_interval = $ebook_carousel_interval * 1000;		// in milliseconds
		
		$ebook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_active_reviewers');
		$ebook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_helpful_reviewers');
		
		$carousel_css = '';
		$ctrl_btn_css = '';
		if (!$ebook_ranking_most_active_reviewers && !$ebook_ranking_most_helpful_reviewers) {
			// check pbook for display in symmetric
			$pbook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_active_reviewers');
			$pbook_ranking_most_active_borrowers = $libelibplus->get_portal_setting('pbook_ranking_most_active_borrowers');
			$pbook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_helpful_reviewers');
			$pbook_recommend_book = $libelibplus->get_portal_setting('pbook_recommend_book');
			$pbook_most_loan = $libelibplus->get_portal_setting('pbook_most_loan');
			$pbook_new = $libelibplus->get_portal_setting('pbook_new');
			$pbook_best_rated = $libelibplus->get_portal_setting('pbook_best_rated');
			if (!(($pbook_recommend_book || $pbook_most_loan || $pbook_new || $pbook_best_rated) && ($pbook_ranking_most_active_reviewers || $pbook_ranking_most_active_borrowers || $pbook_ranking_most_helpful_reviewers))) {
				$carousel_css = ' style="width:100%"';
				$ctrl_btn_css = ' style="left:50%"'; 
			}	
		}
		
		$slideInfo = $this->geteBookSlides($type);
		if ($slideInfo) {
			$slideLayout = $slideInfo['html'];
		}
		else {
			$slideLayout = '';
		}
		
 		ob_start();
?> 		
      <!-- CAROUSEL starts-->
      <div class="carousel-box"<?=$carousel_css?>>
        <div class="shelf-info">
          <div class="shelf-type"><?=$Lang["libms"]["portal"]["eBooks"]?></div>
          <a id="ebook_more" href="#" class="btn-more"><?=$eLib_plus["html"]["more"]?> <span class="caret-right-yellow"></span></a> </div>
        <div data-ride="carousel" class="carousel slide" data-interval="<?=$ebook_carousel_interval?>" id="carousel-ebooks"> 
<?=$slideLayout?>        
          <!-- Controls --> 
           </div> <!-- carousel-ebooks -->
<?=$this->geteBookControlButton($ctrl_btn_css)?>		
      </div>
      <!-- CAROUSEL ends--> 
      
<?php  
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end geteBookCarousel  	


	/*
	 * 	@return eBook control button group in eBook shelf
	 */
	function geteBookControlButton($ctrl_btn_css='') {
		global $eLib_plus, $Lang;
		$libelibplus = new libelibplus();
		$ebook_recommend_book = $libelibplus->get_portal_setting('ebook_recommend_book');
		$ebook_most_hit = $libelibplus->get_portal_setting('ebook_most_hit');
		$ebook_new = $libelibplus->get_portal_setting('ebook_new');
		$ebook_best_rated = $libelibplus->get_portal_setting('ebook_best_rated');

		if ($ebook_recommend_book || $ebook_most_hit || $ebook_new || $ebook_best_rated) {
			$show_control = true;
		}
		else {
			$show_control = false;
		}

		$active = false;	
		$x = '';
		if ($show_control) {           
	        $x .= '<div id="ebook_btn_group" class="shelf-filter btn-group"'.$ctrl_btn_css.'>';
			if ($ebook_recommend_book) {
	        	$x .= '<a id="ebook_recommend_book" href="#" class="btn btn-default'.($active?'':' active').'">'.$eLib_plus["html"]["recommend"].'</a>';
	        	if (!$active) $active = true;
			}
			if ($ebook_most_hit) {        	 
	        	$x .= '<a id="ebook_most_hit" href="#" class="btn btn-default'.($active?'':' active').'">'.$Lang["libms"]["portal"]["most_hit"].'</a>';
	        	if (!$active) $active = true;
			}
			if ($ebook_new) {        	 
	        	$x .= '<a id="ebook_new" href="#" class="btn btn-default'.($active?'':' active').'">'.$eLib_plus["html"]["new"].'</a>';
	        	if (!$active) $active = true;
			}
			if ($ebook_best_rated) {        	 
	        	$x .= '<a id="ebook_best_rated" href="#" class="btn btn-default'.($active?'':' active').'">'.$Lang["libms"]["portal"]["best_rated"].'</a>';
	        	if (!$active) $active = true;
			} 
			$x .= '</div>';
		}
		return $x;		
	}
	
	
 	/*
 	 * 	$type		- recommend / most hit / new / best rated
 	 * 	@return		- array['no_books' => $no_books, 'html' => $x] 
 	 * 			
 	 */
	function geteBookSlides($type='recommend') {
		global $PATH_WRT_ROOT, $eLib_plus, $Lang;
		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
		$elibplus_cfg['carousel_ebook_limit'] = $elibplus_cfg['carousel_ebook_limit']?$elibplus_cfg['carousel_ebook_limit']:50;
		$elibplus_cfg['carousel_ebook_accumulated_days'] = $elibplus_cfg['carousel_ebook_accumulated_days']?$elibplus_cfg['carousel_ebook_accumulated_days']:60;
		$elibplus_cfg['carousel_ebook_most_hit'] = $elibplus_cfg['carousel_ebook_most_hit']?$elibplus_cfg['carousel_ebook_most_hit']:3;
		$elibplus_cfg['carousel_ebook_best_rated'] = $elibplus_cfg['carousel_ebook_best_rated']?$elibplus_cfg['carousel_ebook_best_rated']:3;
		$elibplus_cfg['carousel_active_books'] = $elibplus_cfg['carousel_active_books']?$elibplus_cfg['carousel_active_books']:6;

		$libelibplus = new libelibplus();
		
		$book_type = 'ebook';
		switch ($type) {
			case 'recommend':
				$libms = new liblms();				
				$shuffle = $libms->get_system_setting("preference_recommend_book_order") ? false : true;	// default uses random
				$ebook_recommend_book_range = $libelibplus->get_portal_setting('ebook_recommend_book_range');
				$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
				list($total, $bookList) = $lelibplus->getRecommendBooks(0, $elibplus_cfg['carousel_ebook_limit'], $shuffle, $book_type, $ebook_recommend_book_range);
				break;
			case 'most_hit':
				$ebook_most_hit_range = $libelibplus->get_portal_setting('ebook_most_hit_range');
				if ($ebook_most_hit_range == 1) {
					$range = 'thisweek';
				}
				else if ($ebook_most_hit_range == 2) {
					$range = 'thismonth';
				}
				else {
					$range = 'accumulated';
				}				 
				$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$range);
//				$lelibplus->set_accumulated_days($elibplus_cfg['carousel_ebook_accumulated_days']);				
				$bookList = $lelibplus->getMostHitBooks($elibplus_cfg['carousel_ebook_most_hit'], $book_type);	// number of most hit books shown
				break;
			case 'new':
				$ebook_new_range = $libelibplus->get_portal_setting('ebook_new_range');
				$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
				list($total, $bookList) = $lelibplus->getNewBooks($elibplus_cfg['carousel_ebook_limit'], $book_type, 0, 10, $ebook_new_range);
				break;
			case 'best_rated':
				$ebook_best_rated_range = $libelibplus->get_portal_setting('ebook_best_rated_range');
				if ($ebook_best_rated_range == 1) {
					$range = 'thisweek';
				}
				else {
					$range = 'accumulated';
				}				 
				$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$range);
//				$lelibplus->set_accumulated_days($elibplus_cfg['carousel_ebook_accumulated_days']);				
				$bookList = $lelibplus->getBestRateBooks($elibplus_cfg['carousel_ebook_best_rated'], $book_type);	// number of best rated books shown
				break;
		}
		$no_books = count($bookList);
		
		if($no_books>1) {
			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
		}
		else {
			$navigation_id = '';
		}
		
		$ret = array();
		$ret['no_books'] = $no_books;
		
		$showRead = $_SESSION['UserID'] ? true : false;
		
 		ob_start();
?> 		
          <!-- Wrapper for slides start -->          
          <div class="carousel-inner">
<?php 
	if ($no_books) :
		for ($i=0; $i<$no_books; $i++) : 
			$book = $bookList[$i];
			$book['navigation_id'] = $navigation_id;
			if ($i%$elibplus_cfg['carousel_active_books'] == 0) {
?>			
            <div class="item <?=($i==0?'active':'')?> table">
              <ul class="book-covers">
<?php		} 
			echo $this->geteBookCover($book, true, "li", "", $showRead);

			if (($i%$elibplus_cfg['carousel_active_books'] == $elibplus_cfg['carousel_active_books']-1) || ($i==$no_books-1)) {
?>
              </ul>
            </div>
<?php 		}         
 		endfor;
?>
 		<a class="left carousel-control" href="#carousel-ebooks" role="button" data-slide="prev"><span class="glyphicon glyphicon-menu-left"></span></a> 
 		<a class="right carousel-control" href="#carousel-ebooks" role="button" data-slide="next"><span class="glyphicon glyphicon-menu-right"></span></a>
<?php
	else:
		echo $this->getNoRecordFoundBookCover('ebook');
 	endif; 
?>
          </div> <!-- Wrapper for slides end -->
<?php  
		$x = ob_get_contents();
		ob_end_clean();
		$ret['html'] = $x;
		return $ret;
	}	// end geteBookSlides  	
 
 
  	/*
 	 * 	$type		- recommend / most loan / new / best rated
 	 * 			
 	 */
	function getpBookCarousel($type='recommend') {
		global $eLib_plus, $Lang;
		
		$libelibplus = new libelibplus();
		$pbook_carousel_interval = $libelibplus->get_portal_setting('pbook_carousel_interval');
		$pbook_carousel_interval = $pbook_carousel_interval * 1000;		// in milliseconds
		
		$pbook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_active_reviewers');
		$pbook_ranking_most_active_borrowers = $libelibplus->get_portal_setting('pbook_ranking_most_active_borrowers');
		$pbook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_helpful_reviewers');
		
		$carousel_css = '';
		$ctrl_btn_css = '';
		if (!$pbook_ranking_most_active_reviewers && !$pbook_ranking_most_active_borrowers && !$pbook_ranking_most_helpful_reviewers) {
			// check ebook for display in symmetric
			$ebook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_active_reviewers');
			$ebook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_helpful_reviewers');
			
			$ebook_recommend_book = $libelibplus->get_portal_setting('ebook_recommend_book');
			$ebook_most_hit = $libelibplus->get_portal_setting('ebook_most_hit');
			$ebook_new = $libelibplus->get_portal_setting('ebook_new');
			$ebook_best_rated = $libelibplus->get_portal_setting('ebook_best_rated');
			if (!(($ebook_recommend_book || $ebook_most_hit || $ebook_new || $ebook_best_rated) && ($ebook_ranking_most_active_reviewers || $ebook_ranking_most_helpful_reviewers))) {
				$carousel_css = ' style="width:100%"';
				$ctrl_btn_css = ' style="left:50%"';
			}	
		}
		
		$slideInfo = $this->getpBookSlides($type);
		if ($slideInfo) {
			$slideLayout = $slideInfo['html'];
		}
		else {
			$slideLayout = '';
		}
		
 		ob_start();
?> 		
      <!-- CAROUSEL starts-->
      <div class="carousel-box"<?=$carousel_css?>>
        <div class="shelf-info">
          <div class="shelf-type"><?=$eLib_plus["html"]["others"]?></div>
          <a id="pbook_more" href="#" class="btn-more"><?=$eLib_plus["html"]["more"]?> <span class="caret-right-yellow"></span></a> </div>
        <div data-ride="carousel" class="carousel slide" data-interval="<?=$pbook_carousel_interval?>" id="carousel-pbooks"> 
<?=$slideLayout?>        
          <!-- Controls --> 
           </div> <!-- carousel-pbooks -->
<?=$this->getpBookControlButton($ctrl_btn_css)?>		
      </div>
      <!-- CAROUSEL ends--> 
      
<?php  
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getpBookCarousel  	


	/*
	 * 	@return physical book control button group in physical book shelf
	 */
	function getpBookControlButton($ctrl_btn_css='') {
		global $eLib_plus, $Lang;
		$libelibplus = new libelibplus();
		$pbook_recommend_book = $libelibplus->get_portal_setting('pbook_recommend_book');
		$pbook_most_loan = $libelibplus->get_portal_setting('pbook_most_loan');
		$pbook_new = $libelibplus->get_portal_setting('pbook_new');
		$pbook_best_rated = $libelibplus->get_portal_setting('pbook_best_rated');

		if ($pbook_recommend_book || $pbook_most_loan || $pbook_new || $pbook_best_rated) {
			$show_control = true;
		}
		else {
			$show_control = false;
		}
		$active = false;
		$x = '';
		if ($show_control) {           
	        $x .= '<div id="pbook_btn_group" class="shelf-filter btn-group"'.$ctrl_btn_css.'>';
			if ($pbook_recommend_book) {
	        	$x .= '<a id="pbook_recommend_book" href="#" class="btn btn-default'.($active?'':' active').'">'.$eLib_plus["html"]["recommend"].'</a>';
	        	if (!$active) $active = true;
			}
			if ($pbook_most_loan) {        	 
	        	$x .= '<a id="pbook_most_loan" href="#" class="btn btn-default'.($active?'':' active').'">'.$Lang["libms"]["portal"]["most_loan"].'</a>';
	        	if (!$active) $active = true;
			}
			if ($pbook_new) {        	 
	        	$x .= '<a id="pbook_new" href="#" class="btn btn-default'.($active?'':' active').'">'.$eLib_plus["html"]["new"].'</a>';
	        	if (!$active) $active = true;
			}
			if ($pbook_best_rated) {        	 
	        	$x .= '<a id="pbook_best_rated" href="#" class="btn btn-default'.($active?'':' active').'">'.$Lang["libms"]["portal"]["best_rated"].'</a>';
	        	if (!$active) $active = true;
			} 
			$x .= '</div>';
		}
		return $x;		
	}
	
	
 	/*
 	 * 	$type		- recommend / most loan / new / best rated
 	 * 	@return		- array['no_books' => $no_books, 'html' => $x] 
 	 * 			
 	 */
	function getpBookSlides($type='recommend') {
		global $PATH_WRT_ROOT, $eLib_plus, $Lang;
		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
		$elibplus_cfg['carousel_pbook_limit'] = $elibplus_cfg['carousel_pbook_limit']?$elibplus_cfg['carousel_pbook_limit']:50;
		$elibplus_cfg['carousel_pbook_accumulated_days'] = $elibplus_cfg['carousel_pbook_accumulated_days']?$elibplus_cfg['carousel_pbook_accumulated_days']:60;
		$elibplus_cfg['carousel_pbook_most_loan'] = $elibplus_cfg['carousel_pbook_most_loan']?$elibplus_cfg['carousel_pbook_most_loan']:3;
		$elibplus_cfg['carousel_pbook_best_rated'] = $elibplus_cfg['carousel_pbook_best_rated']?$elibplus_cfg['carousel_pbook_best_rated']:3;
		$elibplus_cfg['carousel_active_books'] = $elibplus_cfg['carousel_active_books']?$elibplus_cfg['carousel_active_books']:6;

		$libelibplus = new libelibplus();
	
		$book_type = 'physical';
		switch ($type) {
			case 'recommend':
				$libms = new liblms();				
				$shuffle = $libms->get_system_setting("preference_recommend_book_order") ? false : true;	// default uses random
				$pbook_recommend_book_range = $libelibplus->get_portal_setting('pbook_recommend_book_range');
				$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
				list($total, $bookList) = $lelibplus->getRecommendBooks(0, $elibplus_cfg['carousel_pbook_limit'], $shuffle, $book_type, $pbook_recommend_book_range);
				break;
			case 'most_loan':
				$pbook_most_loan_range = $libelibplus->get_portal_setting('pbook_most_loan_range');
				if ($pbook_most_loan_range == 1) {
					$range = 'thisweek';
				}
				else {
					$range = 'accumulated';
				}				 
				$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$range);
//				$lelibplus->set_accumulated_days($elibplus_cfg['carousel_pbook_accumulated_days']);
				$bookList = $lelibplus->getMostLoanBooks($elibplus_cfg['carousel_pbook_most_loan']);	// number of most loan books shown
				break;
			case 'new':
				$pbook_new_range = $libelibplus->get_portal_setting('pbook_new_range');
				$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
				list($total, $bookList) = $lelibplus->getNewBooks($elibplus_cfg['carousel_pbook_limit'], $book_type, 0, 10, $pbook_new_range);
				break;
			case 'best_rated':
				$pbook_best_rated_range = $libelibplus->get_portal_setting('pbook_best_rated_range');
				if ($pbook_best_rated_range == 1) {
					$range = 'thisweek';
				}
				else {
					$range = 'accumulated';
				}				 
				$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$range);
//				$lelibplus->set_accumulated_days($elibplus_cfg['carousel_pbook_accumulated_days']);				
				$bookList = $lelibplus->getBestRateBooks($elibplus_cfg['carousel_pbook_best_rated'], $book_type);	// number of best rated books shown
				break;
		}
		$no_books = count($bookList);

		if($no_books>1) {
			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
		}
		else {
			$navigation_id = '';
		}
		
		$ret = array();
		$j = 0;		// counter for books with no cover image
		$ret['no_books'] = $no_books;
		
 		ob_start();
?> 		
          <!-- Wrapper for slides start -->          
          <div class="carousel-inner">
<?php 
	if ($no_books) :
		for ($i=0; $i<$no_books; $i++) : 
			$book = $bookList[$i];
			if ($i%$elibplus_cfg['carousel_active_books'] == 0) {
?>			
            <div class="item <?=($i==0?'active':'')?> table">
              <ul class="book-covers">
<?php		}  
 
			if ($book['image']) {
				$image_no = '';
			}
			else {
				$image_no = $j%10+1;
				$j++;
			}
			$book['image_no'] = $image_no;
			$book['navigation_id'] = $navigation_id;
			echo $this->getpBookCover($book, true, "li");   

			if (($i%$elibplus_cfg['carousel_active_books'] == $elibplus_cfg['carousel_active_books']-1) || ($i==$no_books-1)) {
?>
              </ul>
            </div>
<?php 		}         
 		endfor;
?>
 		<a class="left carousel-control" href="#carousel-pbooks" role="button" data-slide="prev"><span class="glyphicon glyphicon-menu-left"></span></a> 
 		<a class="right carousel-control" href="#carousel-pbooks" role="button" data-slide="next"><span class="glyphicon glyphicon-menu-right"></span></a>
<?php
	else:
		echo $this->getNoRecordFoundBookCover('pbook');	
 	endif; 
?>
          </div> <!-- Wrapper for slides end -->
<?php  
		$x = ob_get_contents();
		ob_end_clean();
		$ret['html'] = $x;
		return $ret;
	}	// end getpBookSlides  	
 
 	
 	/*
 	 * 	eBook Ranking Layout in portal
 	 */
 	function geteBookRankingUI(){
 		global $Lang, $eLib_plus;
 		
 		$libelibplus = new libelibplus();
		$ebook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_active_reviewers');
		$ebook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_helpful_reviewers');
		
 		if ($ebook_ranking_most_active_reviewers) {
			$ebook_ranking_most_active_reviewers_range = $libelibplus->get_portal_setting('ebook_ranking_most_active_reviewers_range');
 			$range = $ebook_ranking_most_active_reviewers_range ? 'thisweek' : 'accumulated';
 			$tableContent = $this->geteBookRankingTable($range, 'most_active_reviewers');
 		}
 		else if ($ebook_ranking_most_helpful_reviewers) {
 			$ebook_ranking_most_helpful_reviewers_range = $libelibplus->get_portal_setting('ebook_ranking_most_helpful_reviewers_range');
 			$range = $ebook_ranking_most_helpful_reviewers_range ? 'thisweek' : 'accumulated';
 			$tableContent = $this->getMostUserfulReviewsTable($range, 'ebook');	
 		}
 		
 		ob_start();
 		if ($ebook_ranking_most_active_reviewers || $ebook_ranking_most_helpful_reviewers) :
?>
      <!--RANKING BOX starts-->
      <form name="ebook_ranking_form" id="ebook_ranking_form" method="post" action="<?=$_SESSION['UserID']?'ranking.php':'opac_ranking.php'?>">
      <div class="ranking-box">
        <div class="ranking-title"><?=$Lang["libms"]["portal"]["eBooks_ranking"]?><a id="ebook_ranking" class="btn-more" href="#"><?=$eLib_plus["html"]["more"]?><span class="caret-right-yellow"></span></a> </div>
        <div class="ranking-content">
          <select id="ebook_ranking_type" name="ebook_ranking_type" class="">
          <?php if ($ebook_ranking_most_active_reviewers):?>
            <option value="most_active_reviewers" selected="selected"><?=$eLib_plus["html"]["mostactivereviewers"]?></option>
          <?php endif;?>
          <?php if ($ebook_ranking_most_helpful_reviewers):?>
            <option value="most_helpful_review"><?=$eLib_plus["html"]["mosthelpfulreview"]?></option>
          <?php endif;?>
          </select>
          <?=$this->getRankingPeriod($range,'ebook_ranking_range')?>
          <table id="ebook_ranking_table" class="ranking-table table">
          <?=$tableContent?>
          </table>
        </div>
      </div>
      <input type="hidden" name="book_type" value="ebook">
      </form>
      <!--RANKING BOX ends--> 
<?php 		
		endif;
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end geteBookRankingUI
 	
 	/*
 	 * 	@param		$rangge	- accumulated (default), thisweek, thismonth, thisyear, specific
 	 */
 	function geteBookRankingTable($range='accumulated', $type='most_active_reviewers') {
 		global $eLib, $eLib_plus;
 		
 		$row_limit = 5;		// show top 5 students only
 		$book_type = 'ebook';
 		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$range);
 		switch($type) {
 			case 'most_active_reviewers':
				$users = $lelibplus->getMostActiveUsers($row_limit, $book_type, USERTYPE_STUDENT, false);	// $classNameWithBracket=false 
				break;
//			case 'most_active_readers':
//				$users = $lelibplus->getMostActiveReaders($row_limit);
//				break;
 		}
		ob_start();		
?>
            <colgroup>
            <col width="80%">
            <col width="20%">
            </colgroup>
            <thead>
              <tr>
                <th><?=$eLib["html"]["student"]?></th>
                <th><?=$eLib["html"]['Total']?></th>
              </tr>
            </thead>
            <tbody>
<?php if (!$users):?>
              <tr>
                <td colspan="2"><?=$eLib_plus["html"]["norecord"]?></td>
              </tr>
<?php endif;
		foreach ((array)$users as $i=>$user): 
?>
              <tr>
                <td><span class="ellipsis"><?=(empty($user['class'])?'&nbsp;':$user['class'])?></span><?=$user['name']?></td>
                <td><?=($type=='most_active_readers'?$user['hit_rate']:$user['review_count'])?></td>
              </tr>
<?php	endforeach; ?>
            </tbody>
<?php
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// geteBookRankingTable
 	

 	function getMostUserfulReviewsTable($range='accumulated', $book_type='ebook') {
 		global $eLib, $eLib_plus, $Lang;
 		
 		$row_limit = 5;		// show top 5 students only
 		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$range);
		$reviews = $lelibplus->getMostUsefulReviews($row_limit, $book_type, false);		// $classNameWithBracket=false

		$libelibplus = new libelibplus();
		if(count($reviews)>1) {
			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
		}
		else {
			$navigation_id = '';
		}

		ob_start();		
?>
            <colgroup>
            <col width="50%">
            <col width="50%">
            </colgroup>
            <thead>
              <tr>
              	<th><?=$Lang["libms"]["portal"]["book_title"]?></th>
                <th><?=$Lang["libms"]["portal"]["reader"]?></th>
              </tr>
            </thead>
            <tbody>
<?php if (!$reviews):?>
              <tr>
                <td colspan="2"><?=$eLib_plus["html"]["norecord"]?></td>
              </tr>
<?php endif;
		foreach ((array)$reviews as $i=>$review): 
			if ($review['book_type'] == 'ebook') {
				$para = "";
			}   
			else {   
				$cover_type_id = rand(1,10);
				if ($review['image']) {
					$para = '';
				}
				else {
					$para = "&cover_type_id=$cover_type_id";
				}
			}
?>
              <tr>
                <td><a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$navigation_id?>" href="book_details.php?book_id=<?=$review['book_id'].$para?>" title="<?=$review['title'].' - '.$review['author']?>"><?=$review['title']?></a></td>
                <td><?=((empty($review['class']) || $review['class'] =='-')?'':$review['class'].'&nbsp;')?><?=$review['name']?></td>
              </tr>
<?php	endforeach; ?>
            </tbody>
<?php
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// getMostUserfulReviewsTable


 	/*
 	 * 	physical Book Ranking Layout in portal
 	 */
 	function getpBookRankingUI(){
 		global $Lang, $eLib_plus;
 		
 		$libelibplus = new libelibplus();
		$pbook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_active_reviewers');
		$pbook_ranking_most_active_borrowers = $libelibplus->get_portal_setting('pbook_ranking_most_active_borrowers');
		$pbook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_helpful_reviewers');
		if ($pbook_ranking_most_active_reviewers ) {
			$pbook_ranking_most_active_reviewers_range = $libelibplus->get_portal_setting('pbook_ranking_most_active_reviewers_range');
  			$range = $pbook_ranking_most_active_reviewers_range ? 'thisweek' : 'accumulated'; 
  			$tableContent = $this->getpBookRankingTable($range,'most_active_reviewers');
		}
		else if ($pbook_ranking_most_active_borrowers) {
			$pbook_ranking_most_active_borrowers_range = $libelibplus->get_portal_setting('pbook_ranking_most_active_borrowers_range');
          	$range = $pbook_ranking_most_active_borrowers_range ? 'thisweek' : 'accumulated';
          	$tableContent = $this->getpBookRankingTable($range,'most_active_borrowers');
		}
		else if ($pbook_ranking_most_helpful_reviewers) {
			$pbook_ranking_most_helpful_reviewers_range = $libelibplus->get_portal_setting('pbook_ranking_most_helpful_reviewers_range');
          	$range = $pbook_ranking_most_helpful_reviewers_range ? 'thisweek' : 'accumulated';
          	$tableContent = $this->getMostUserfulReviewsTable($range,'physical');
		}
 		
 		ob_start();
 		if ($pbook_ranking_most_active_reviewers || $pbook_ranking_most_active_borrowers || $pbook_ranking_most_helpful_reviewers) :
?>
      <!--RANKING BOX starts-->
      <form name="pbook_ranking_form" id="pbook_ranking_form" method="post" action="<?=$_SESSION['UserID']?'ranking.php':'opac_ranking.php'?>">
      <div class="ranking-box">
        <div class="ranking-title"><?=$Lang["libms"]["portal"]["pBooks_ranking"]?><a id="pbook_ranking" class="btn-more" href="#"><?=$eLib_plus["html"]["more"]?><span class="caret-right-yellow"></span></a> </div>
        <div class="ranking-content">
          <select id="pbook_ranking_type" name="pbook_ranking_type" class="">
          <?php if ($pbook_ranking_most_active_reviewers):?>
            <option value="most_active_reviewers" selected="selected"><?=$eLib_plus["html"]["mostactivereviewers"]?></option>
          <?php endif;?>
          <?php if ($pbook_ranking_most_active_borrowers):?>
            <option value="most_active_borrowers"><?=$eLib_plus["html"]["mostactiveborrowers"]?></option>
          <?php endif;?>
          <?php if ($pbook_ranking_most_helpful_reviewers):?>
            <option value="most_helpful_review"><?=$eLib_plus["html"]["mosthelpfulreview"]?></option>
          <?php endif;?>
          </select>
          <?=$this->getRankingPeriod($range,'pbook_ranking_range')?>
          <table id="pbook_ranking_table" class="ranking-table table">
          <?=$tableContent?>
          </table>
        </div>
      </div>
      <input type="hidden" name="book_type" value="physical">
      </form>
      <!--RANKING BOX ends--> 
<?php 		
		endif;
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getpBookRankingUI
 	
 	
 	/*
 	 * 	@param		$rangge	- accumulated (default), thisweek, thismonth, thisyear, specific
 	 */
 	function getpBookRankingTable($range='accumulated', $type='most_active_reviewers') {
 		global $eLib, $eLib_plus;
 		
 		$row_limit = 5;		// show top 5 students only
 		$book_type = 'physical';
 		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$range);
 		switch($type) {
 			case 'most_active_reviewers':
				$users = $lelibplus->getMostActiveUsers($row_limit, $book_type, USERTYPE_STUDENT, false);	// $classNameWithBracket=false
				break;
			case 'most_active_borrowers':
				$users = $lelibplus->getMostBorrowUsers($row_limit, false);		// $classNameWithBracket=false
				break;
 		}
		ob_start();		
?>
            <colgroup>
            <col width="80%">
            <col width="20%">
            </colgroup>
            <thead>
              <tr>
                <th><?=$eLib["html"]["student"]?></th>
                <th><?=$eLib["html"]['Total']?></th>
              </tr>
            </thead>
            <tbody>
<?php if (!$users):?>
              <tr>
                <td colspan="2"><?=$eLib_plus["html"]["norecord"]?></td>
              </tr>
<?php endif;
		foreach ((array)$users as $i=>$user): 
?>
              <tr>
              	<td><span class="ellipsis"><?=(empty($user['class'])?'&nbsp;':$user['class'])?></span><?=$user['name']?></td>
                <td><?=($type=='most_active_reviewers'?$user['review_count']:$user['loan_count'])?></td>
              </tr>
<?php	endforeach; ?>
            </tbody>
<?php
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// getpBookRankingTable



	
	function getPortalBottomPart() {
		global $eLib_plus, $plugin, $sys_custom, $intranet_session_language, $Lang, $junior_mck;
 		$libelibplus = new libelibplus();
		$show_news = $libelibplus->get_portal_setting('show_news');
		$show_opening_hours = $libelibplus->get_portal_setting('show_opening_hours');
		$show_rules = $libelibplus->get_portal_setting('show_rules');

		if (($_GET['notice_type'] == 'news') && $show_news) {
			$active_tab = 'news';
		}
		else if (($_GET['notice_type'] == 'rules') && $show_rules) {
			$active_tab = 'rules';
		}
		else {
			if ($show_news) {
				$active_tab = 'news';
			}
			else if ($show_opening_hours && !$plugin['eLib_Lite'] && !$sys_custom['eLibraryPlus']['HideOpeningHours']) {
				$active_tab = 'op-hr';
			}
			else if ($show_rules){
				$active_tab = 'rules';
			}
			else {
				$active_tab = '';	// do not show announcement
			}
		}

		ob_start();
?>
  <div class="elib-news">
  <?=$this->getActionResultUI()?>
    <div class="container">
    <? if ($show_news || $show_opening_hours || $show_rules): ?>
      <!-- start Announcement Pane -->
      <div class="<?=($plugin['elib_video'] || $plugin['elib_epost'] || $plugin['elib_cosmos'] || $plugin['elib_emag'])?'news-box':'news-box-no-rb-item'?> general-tabs">
        <div class="announcement"><?=$eLib_plus["html"]["announcement"]?></div>
        <ul id="announcement_tab" class="nav nav-tabs pull-right">
<?php
	if ($show_news) {        
    	echo '<li class="'.($active_tab=='news'?'active':'').'"><a href="#news" data-toggle="tab">'.$eLib_plus["html"]["news"].'</a></li>';
	}
	// plugin and custom flag overwrite settings
	if (!$plugin['eLib_Lite'] && !$sys_custom['eLibraryPlus']['HideOpeningHours'] && $show_opening_hours) {        
    	echo '<li class="'.($active_tab=='op-hr'?'active':'').'"><a href="#op-hr" data-toggle="tab">'.$eLib_plus["html"]["openinghours"].'</a></li>';
	}
	if ($show_rules) {        
    	echo '<li class="'.($active_tab=='rules'?'active':'').'"><a href="#rules" data-toggle="tab">'.$eLib_plus["html"]["rules"].'</a></li>';
	}
?>	
        </ul>
        
        <div class="tab-content" id="announcement_content">
<?php 	
	if ($active_tab=='news') {
		echo $this->getPortalNewsUI();
	}
	else if ($active_tab=='op-hr') {
		echo $this->getPortalOpeningHoursUI();
	}
	else if ($active_tab=='rules') {
		echo $this->getPortalRulesUI();
	}
	else {
		// show nothing
	}
?>		
		</div>        
      </div>
    <? endif;?>  
	<!-- end Announcement Pane -->

<? // following parts are not for opac ?>
<?php if ($_SESSION['UserID']): ?>      
<?php if($plugin["elib_plus_demo"]) {?>      
	    <script>	<!-- Temporary added for demo 2013-05-03 Charles MA-->
	    	function openNewspaper(NewspaperID, UpdateView)
			{
				var intWidth  = screen.width;
			 	var intHeight = screen.height-100;
				window.open('/home/ePost/newspaper/view_newspaper.php?NewspaperID='+NewspaperID+'&UpdateView=1',"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
			}
	    </script>
<?php }?>
      
      <!-- Video & Magazine Pane -->
      <div class="module-box">
        <ul>
      	  
<?php if($plugin['elib_video']){?>
          <li><a href="<?=$junior_mck?"../../":"../"?>elibvideo/access<?=$plugin['elib_video_local_ver'] ? "2" : ""?>.php" target="_blank" class="video-btn <?=$intranet_session_language=='b5'?'video-btn-chn':''?>">
            <div class="video-green"></div>
            <div class="video-blue-area">
              <div class="video-cam"></div>
              <div class="video-cam-shadow"></div>
              <div class="video-text">
                <div class="video-text-shadow"></div>
              </div>
            </div>
            </a>
		  </li>	
<?php }?>	
	      
		  <li>	
        	<div class="others-epost">
<?php 
	if($plugin['elib_epost'] && ($plugin['elib_cosmos'] || $plugin['elib_emag'])){

        if($plugin['elib_video']){
        	echo $this->getCosmosMagazineUI('magazine');
		} else {
        	echo $this->getePostUIWithDiv();
		}
	} else if($plugin['elib_epost']){
		echo $this->getePostUIWithDiv();
	} else if(($plugin['elib_cosmos'] || $plugin['elib_emag'])){
		echo $this->getCosmosMagazineUI('');	
	}
?>		
            </div>
		  </li>
		</ul>			
	  </div>
<?php endif;?>
  	</div>
  </div>
      
<?php		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}	// end getPortalBottomPart
	

	function getPortalNewsUI() {
		global $PATH_WRT_ROOT, $eLib_plus, $Lang, $permission;
		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
	    $limit = $elibplus_cfg['portal_news_limit'] ? $elibplus_cfg['portal_news_limit'] : 5;
	    
	    $lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		$total_notice = $lelibplus->getTotalPortalNotices('news');
		$newsItemList = $this->getNewsItemUI(0,$limit);
		
		ob_start();
?>
          <div class="tab-pane active" id="news"> 
            <!--teacher EDIT-->
	<?php if ($permission['admin']): ?>            
            <div class="teacher news-settings"><a href="notice_edit.php?action=add&type=news" class="teacher btn-add"><span class="glyphicon glyphicon-plus"></span> <?=$Lang['Btn']['Add']?></a></div>
	<?php endif;?>            
            <ul>
	<?=$newsItemList['html']?>            
			</ul>
		  </div>
<?php	if ($total_notice > $limit) :?>
			<div class="news-settings">&nbsp;</div>
			<div class="news-settings"><a href="#" id="more_news" data-news-offset="<?=$limit?>" class="btn-more"><?=$eLib_plus["html"]["more"]?>...</a></div>		
<?php 	endif;
					
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}	// end getPortalNewsUI

	/*
	 * 	$offset --> starting point to get news announcement record order by Priority
	 * 	$limit	--> limit number of record to show
	 */
	function getNewsItemUI($offset=0, $limit=5) {
		global $PATH_WRT_ROOT, $eLib_plus, $Lang, $permission;
	    $lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
	    $portal_data['news'] = $lelibplus->getPortalNotices('news', false, $offset, $limit, true, true);
		$nr_of_news = count($portal_data['news']);
		$ret = array();
		$ret['offset'] = $offset+($nr_of_news<$limit?$nr_of_news:$limit);
		
		ob_start();
?>
<?php foreach ((array)$portal_data['news'] as $i=>$news_item):
 		$date_start = $news_item['date_start'] == '0000-00-00 00:00:00' ? '' : substr($news_item['date_start'],0,10);
		$j = $offset + $i;
		$image = '';
		if ($news_item['attachment_name']) {
			$file_extention = getFileExtention($news_item['attachment_name'],true);
			if (($file_extention == 'jpg') || ($file_extention == 'jpeg') || ($file_extention == 'png') || ($file_extention == 'gif')) {
				$image = '<br><img src="'.$news_item['attachment'].'" style="max-width:90%;">';
			}
		}
		$attachment = ($news_item['attachment'])?'<br><br>'.$eLib_plus["html"]["attachment"].': <a href="'.str_replace($news_item['attachment_name'], urlencode($news_item['attachment_name']), $news_item['attachment']).'" target="_blank" class="attachment">'.$news_item['attachment_name'].'</a>'.$image:'';
?>            
              <li class="<?=($j==0?'latest':'others')?>"> 
                <!--teacher EDIT-->
	<?php if ($permission['admin']): ?>                
                <div class="teacher news-settings"><a href="notice_edit.php?notice_id=<?=$news_item['id']?>&type=news" class="btn-edit"><span class="glyphicon glyphicon-pencil"></span> <?=$Lang['Btn']['Edit']?></a></div>
	<?php endif;?>                
                <div class="news-title" id="news_title_<?=$j?>"><?=($j==0?$news_item['title']:'<a href="">'.$news_item['title'].'</a>')?></div>
                <div class="news-date">
				<?=$news_item['not_started'] ? " [".$Lang['General']['Hide']."] " : ""?>
				<?=$news_item['priority'] ? " [".$Lang["libms"]["portal"]["pinned"]."] " : ""?>                
                <?=$date_start?>
            <? 	if ($permission['admin']) {
        			$date_end = $news_item['date_end'] == '0000-00-00 00:00:00' ? '' : ' ~ ' . substr($news_item['date_end'],0,10);
					echo $date_end;         			    	
            	}
            ?>
                </div>
					<div class="news-content" id="news_content_<?=$j?>"><?=($j==0?$news_item['content'].$attachment:'<span id="brief_news_content_'.$j.'">'.$news_item['content'].'</span><span id="full_news_content_'.$j.'" style="display:none">'.$news_item['content'].$attachment.'</span>')?></div>				
                                
<?php 	if ($j > 0) {
			echo '<a href="" id="expand_'.$j.'" class="btn-more btn-expand">'.$Lang["libms"]["portal"]["expand"].' <span class="caret-right"></a>';	
		}
?>                
              </li>
<?php endforeach;
					
 		$x = ob_get_contents();
 		$ret['html'] = $x;
		ob_end_clean();
		return $ret;
		
	}	// end getNewsItemUI
	

	function getPortalRulesUI() {
		global $PATH_WRT_ROOT, $eLib_plus, $Lang, $permission;
	    
	    $lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
	    $rules = current($lelibplus->getPortalNotices('rules', false, 0, 1,true));

		$image = '';
		if ($rules['attachment_name']) {
			$file_extention = getFileExtention($rules['attachment_name'],true);
			if (($file_extention == 'jpg') || ($file_extention == 'jpeg') || ($file_extention == 'png') || ($file_extention == 'gif')) {
				$image = '<br><img src="'.$rules['attachment'].'" style="max-width:90%;">';
			}
		}
		$attachment = ($rules['attachment'])?'<br><br>'.$eLib_plus["html"]["attachment"].': <a href="'.str_replace($rules['attachment_name'], urlencode($rules['attachment_name']), $rules['attachment']).'" target="_blank" class="attachment">'.$rules['attachment_name'].'</a>'.$image:'';

		ob_start();
?>
          <div class="tab-pane active" id="rules"> 
            <!--teacher EDIT-->
	<?php if ($permission['admin']): ?>
			<div class="teacher news-settings"><a href="notice_edit.php?notice_id=<?=$rules['id']?>&type=rules" class="teacher btn-manage"><span class="glyphicon glyphicon-pencil"></span> <?=$Lang['Btn']['Edit']?></a></div>            
	<?php endif;?>            
            <ul>
				<li>
				<?='<div>'.intranet_undo_htmlspecialchars($rules['content']).'</div>'.$attachment?>
				</li>            
			</ul>
		  </div>
<?php
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}	// end getPortalRulesUI


	function getPortalOpeningHoursUI() {
		global $PATH_WRT_ROOT, $eLib_plus, $Lang, $permission;
	    
		$timestamp = time();	// current timestamp
		$is_now = true;
		
		ob_start();
?>
          <div class="tab-pane active" id="op-hr"> 
            <!--teacher EDIT-->
	<?php if ($permission['admin']): ?>
            <div class="teacher news-settings"><a href="/home/library_sys/admin/settings/open_time.php" target="_blank" class="teacher btn-manage"><span class="glyphicon glyphicon-pencil"></span> <?=$eLib_plus["html"]["manage"]?></a></div>
	<?php endif;?>
			<div id='calendar_items'>
            <?=$this->getPortalOpeningHoursItemUI($timestamp,$is_now)?>
            </div>
          </div>
<?php
					
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}	// end getPortalOpeningHoursUI


	function getPortalOpeningHoursItemUI($timestamp_start, $is_now=true) {
		global $eLib_plus, $Lang;
	    
	    $lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		$calendars = $lelibplus->getLibraryCalendarByWeek($timestamp_start);
		
		list($open_date, $end_date) = $lelibplus->getLibraryOpenEndDates();
		
		$today = date('Y-m-d');
		$not_open = false;
		$temp_close = false;	
		ob_start();
?>
			<div class="navigation">
<?		
		if ($calendars[0]['date']>$today) {
			echo '<a href="#" class="btn-prev"><span class="caret-left"></span>'.$Lang["libms"]["portal"]["previous_week"].'</a>';			
		}
		else {	// don't show
//			echo '<a class="btn-prev dim"><span class="caret-left"></span>'.$Lang["libms"]["portal"]["previous_week"].'</a>';
		}
?>
            <a href="#" class="btn-next"><?=$Lang["libms"]["portal"]["next_week"]?> <span class="caret-right"></span></a>
            </div>
            <ul class="calendar">
<?php foreach ((array)$calendars as $i=>$calendar): ?>            
            	<li id="date_<?=$i?>" data-date="<?=$calendar['date']?>" class="<?=($is_now && $i==0?'today':'').($calendar['is_open']?'':' holiday')?>">
                	<div class="date-box">
                    	<div class="month"><?=$Lang["libms"]["portal"]["month"][$calendar['month']]?></div>
                        <div class="date"><?=$calendar['day']?></div>
                        <div class="day"><?=$calendar['weekday']?></div>
                    </div>
<?php
	if ($calendar['description'] == '' && $open_date && $calendar['close_timestamp'] < strtotime($open_date)){	// not open yet on the date
		echo '<div class="time-box">'.$Lang["libms"]["portal"]["not_open"].'</div>';
		$not_open = true;	
	}
	else if ($calendar['description'] == '' && $end_date && $calendar['open_timestamp'] > strtotime($end_date)){	// closed on the date
		echo '<div class="time-box">'.$Lang["libms"]["portal"]["temp_close"].'</div>';
		$temp_close = true;
	}
	else {
		if ($calendar['is_open']) {
			echo '<div class="time-box">'.$calendar['open'].'<br>-<br>'.$calendar['close'].'</div>';
		} 
		else {
			echo '<div class="time-box">'.$eLib_plus["html"]["closed"].'</div>';
		}
		
		if ($is_now && $i==0) {
			$date = date('Y-m-d',$timestamp_start);
			$open_timestamp 	= strtotime($date.' '.$calendar['open']);
			$close_timestamp 	= strtotime($date.' '.$calendar['close']);
			$now_open_status = $calendar['is_open'] && ($timestamp_start >= $open_timestamp) && ($timestamp_start < $close_timestamp) ? $Lang["libms"]["portal"]["opening"] : $eLib_plus["html"]["closing"];
			if ($calendar['is_open']) {
				if ($now_open_status == $eLib_plus["html"]["closing"]) {
					echo $now_open_status;
				}
				else {
					echo $calendar['description'].($calendar['description'] ? "<br>" : "").$now_open_status;
				}
			}
			else {	// closed
				if ($calendar['description'] == $eLib_plus["html"]["specialtime"]) {
					// do nothing
				}
				else {
					echo $calendar['description'];
				}
			}
		}
		else {
			if ($calendar['is_open']) {
				echo $calendar['description'];
			} 
			else {	// closed
				if ($calendar['description'] == $eLib_plus["html"]["specialtime"]) {
					// do nothing
				}
				else {
					echo $calendar['description'];
				}
			}
		}			
	}
?>                    
                </li>
<?php endforeach;
		echo '</ul>';
		
		if ($not_open) {
			echo '<ul><div>'.sprintf($eLib_plus["html"]["librarywillopenedon"], $open_date).'</div></ul>';
		}
		if ($temp_close) {
			echo '<ul><div>'.sprintf($eLib_plus["html"]["libraryclosedsince"], $end_date).'</div></ul>';
		}					
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
		
	}	// end getPortalOpeningHoursItemUI
 
 
 	function getePostUI() {
 		global $plugin, $Lang, $eLib_plus;

 		ob_start();
?> 				
	<?php if($plugin["elib_plus_demo"]){?>
            <a href="javascript:openNewspaper(43,0)" class="newspaper">
            	 <div class="post-title"> <h1><?=$Lang["libms"]["portal"]["demo"]["read_info"]?> </h1><h2>April 2013</h2></div>
            </a>
	<?php }else{?>
            <a href='javascript:newWindow("/home/ePost/index.php", 36)' class="newspaper">
            	 <div class="post_title"> <h1><?=$Lang['ePost']['ePost']?> </h1><h2></h2></div>
            </a>
	<?php }?>
            <span class="stand-glass"></span>
            <span class="logo-epost"></span>
            <a href='javascript:newWindow("/home/ePost/index.php", 36)' class="btn-more"><?=$eLib_plus["html"]["more"]?></a>
<?php
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getePostUI
 
 
 	function getePostUIWithDiv() {
 	   	$x = '<div class="epost-stand">';
       	$x .= $this->getePostUI();
        $x .= '</div>';
 		return $x;	
 	}	// end getePostUIWithDiv
 	
 	
 	function getCosmosMagazineUI($type='magazine') {
 		global $plugin, $junior_mck;
 		ob_start();
?>
		<?php //if(!$plugin['elib_cosmos'] && $plugin['elib_emag']){ // what does following line do? ?>
    	<a id="redirect_to_emag_site" style="display:none;" href="<?=$junior_mck?"../../":"../"?>elibplus/magazine_list.php?redirect_to_emag_site=1" class="magazine fancybox fancybox_magazine_learnthenews fancybox.iframe" title=""></a>
    	<?php //} ?>
    	<div class="<?=($type=='magazine'?'epost-magazine-stand':'epost-stand')?>">
    	<?php if($plugin['elib_cosmos'] && $plugin['elib_emag']){?>
        	<a href="<?=$junior_mck?"../../":"../"?>elibplus/magazine_list.php" class="magazine fancybox fancybox_magazine fancybox.iframe" title=""></a>
        <?php } else if($plugin['elib_cosmos']){?>
        	<a href='javascript:newWindow("/api/cosmos_access.php", 36)' class="magazine" title=""></a>
        <?php } else if($plugin['elib_emag']){?>
        	<a id="redirect_to_emag_site" href="<?=$junior_mck?"../../":"../"?>elibplus/magazine_list.php?redirect_to_emag_site=1" class="magazine  fancybox fancybox_magazine_learnthenews fancybox.iframe" title=""></a>
		<?php } ?>
			<span class="<?=($type=='magazine'?'stand-glass-magazine':'stand-glass')?>"></span>
		<?=($type=='magazine'?$this->getePostUI():'')?>
		</div>
<?php
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getCosmosMagazineUI
 	
 	/*
 	 * 	$book			--> array that contains book info ( id, title, author, url etc.)
 	 * 	$link_details	--> show "details" linking or not
 	 */
 	function geteBookCover($book, $link_details=true, $book_box_tag="li", $target="", $link_read=true){
 		global $Lang, $eLib_plus;
 		
 		ob_start();
			if ($book['type']=='expired') {
?>
                <<?=$book_box_tag?> class="off-shelf book-box">
<?php if ($link_details): ?>   
	<? 	if ($target == '_blank') { ?>
			<a class="" data-fancybox-group="<?=$book['navigation_id']?>" href="javascript:newWindow('book_details.php?book_id=<?=$book['id']?>&target=<?=$target?>',31)" title="<?=str_replace('"','&quot;',$book['title'].' - '.$book['author'])?>">				
	<?	} else { ?>
			<a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$book['navigation_id']?>" href="book_details.php?book_id=<?=$book['id']?>"<?=($target?' target="'.$target.'"':'')?> title="<?=str_replace('"','&quot;',$book['title'].' - '.$book['author'])?>">					
	<?	} ?>
<?php endif; ?>                
                  <div class="icon-ebooks"></div>
                  <div class="title ellipsis multiline"><?=$book['title']?></div>
                  <div class="off-shelf-tag"><?=$Lang["libms"]["portal"]["off_shelf"]?></div>
                  <div class="author"><?=$book['author']?></div>
<?php if ($link_details): ?>                  
                  </a>
<?php endif; ?>                  
                </<?=$book_box_tag?>>
<?php				
			}
			else  {
?>
                <<?=$book_box_tag?> class="book-box">
                  <div class="front face">
                    <div class="icon-ebooks"></div>
<?php if (!$book['image']) :?>
                  	<div class="generic type10">
						<div class="title ellipsis multiline"><?=$book['title']?></div>
						<div class="author"><?=$book['author']?></div>
                    </div>
<?php else : ?>
                    <img src="<?=$book['image']?>">
<?php endif; ?>     
                    
                  </div>
                  <div class="back face">
                    <div class="title ellipsis multiline"><?=$book['title']?></div>
                    <ul class="book-btns">
<?php if ($link_read): ?>                    
                      <li><a href="javascript:<?=$book['url']?>"><?=$eLib_plus["html"]["readnow"]?></a></li>
<?php endif;?>                      
<?php if ($link_details): ?>
					  <li>
			<? 	if ($target == '_blank') { ?>
					<a class="" data-fancybox-group="<?=$book['navigation_id']?>" href="javascript:newWindow('book_details.php?book_id=<?=$book['id']?>&target=<?=$target?>',31)" title="<?=$book['title'].' - '.$book['author']?>"><?=$Lang["libms"]["portal"]["book_details"] ?></a>		
			<?	} else { ?>
					<a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$book['navigation_id']?>" href="book_details.php?book_id=<?=$book['id']?>"<?=($target?' target="'.$target.'"':'')?> title="<?=$book['title'].' - '.$book['author']?>"><?=$Lang["libms"]["portal"]["book_details"] ?></a>		
			<?	} ?>
                      </li>
<?php endif;?>                      
                    </ul>
                    <div class="author"><?=$book['author']?></div>
                  </div>
                </<?=$book_box_tag?>>
<?php
			} 		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end geteBookCover
 	

 	/*
 	 * 	$book			--> array that contains book info ( id, title, author, url etc.)
 	 * 	$link_details	--> show "details" linking or not
 	 * 	$book_box_tag	--> li or div
 	 *  $target			--> "" or _blank
 	 *  $link_my_record --> show link to my record or not
 	 */
 	function getpBookCover($book, $link_details=true, $book_box_tag="li", $target="", $link_my_record=true){
 		global $Lang;
 		
 		ob_start();
?>
				<<?=$book_box_tag?> class="book-box">
                  <div class="front face">
<?php if ($book['image']) :?>
                  	<img src="<?=$book['image']?>">
<?php else: ?>
                  	<div class="generic type<?=$book['image_no']?>">
						<div class="title ellipsis multiline"><?=$book['title']?></div>
						<div class="author"><?=$book['author']?></div>
                    </div>
<?php endif; ?>     
                  </div>
                  <div class="back face">
                    <div class="title ellipsis multiline"><?=$book['title']?></div>
                    <ul class="book-btns">
<?php if ($link_my_record): ?>                    
	<?php if ($book['user_status'] == 'borrowed'): ?>                     
     				  <li><a href="./myrecord.php"><?=$Lang["libms"]["portal"]["borrowed"]?></a></li>
	<?php elseif ($book['user_status'] == 'reserved') : ?>     				  
     				  <li><a href="./myrecord.php"><?=$Lang["libms"]["portal"]["reserved"]?></a></li>
	<?php endif; ?>
<?php endif;?>	     				  
<?php if ($link_details): ?>
					  <li>
			<? 	if ($target == '_blank') { ?>
					<a class="" data-fancybox-group="<?=$book['navigation_id']?>" href="javascript:newWindow('book_details.php?book_id=<?=$book['id']?>&cover_type_id=<?=$book['image_no']?>&target=<?=$target?>',31)" title="<?=str_replace('"','&quot;',$book['title'].' - '.$book['author'])?>"><?=$Lang["libms"]["portal"]["book_details"] ?></a>		
			<?	} else { ?>
					<a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$book['navigation_id']?>" href="book_details.php?book_id=<?=$book['id']?>&cover_type_id=<?=$book['image_no']?>"<?=($target?' target="'.$target.'"':'')?> title="<?=str_replace('"','&quot;',$book['title'].' - '.$book['author'])?>"><?=$Lang["libms"]["portal"]["book_details"] ?></a>		
			<?	} ?>
                      </li>
<?php endif;?>                      
                    </ul>
                    <div class="author"><?=$book['author']?></div>
                  </div>
				</<?=$book_box_tag?>>
<?php 		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getpBookCover

	
	function getNoRecordFoundBookCover($book_type='pbook') {
		global $eLib_plus;
		$x = '
            <div class="item active table">
              <ul class="book-covers">
                <li class="off-shelf book-box">';
        $x .= ($book_type == 'ebook') ? '   <div class="icon-ebooks"></div>' : '';
		$x .= '   <div class="off-shelf-tag">'.$eLib_plus["html"]["norecord"].'</div>
                </li>
              </ul>
            </div>';
        return $x;
	}	// end getNoRecordFoundBookCover
	
 	
 	function getBookItemsNextToCover($book, $showCategoryAndCallNum=true) {
 	    global $PATH_WRT_ROOT, $eLib, $eLib_plus, $sys_custom, $intranet_session_language;
		include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
 		
 		ob_start();
?>
          <table class="list-table">
            <tr><td class="list-title" colspan=2><a class="fancybox book-detail-fancybox fancybox.iframe" href="book_details.php?book_id=<?=$book['id']?>&cover_type_id=<?=$book['image_no']?>" data-fancybox-group="<?=($book['navigation_id']+1)?>" title="<?=str_replace('"','&quot;',$book['title'].' - '.$book['author'])?>"><?=$book['title']?></a></td></tr>
            <tr><td class="list-label"><?=$eLib["html"]["author"]?>:</td><td>
      	<? if (count($book['authors']) > 0):?>
			<? foreach ((array)$book['authors'] as $i=>$author): ?>
				<?=$i>0?', '.$author:'<a href="advanced_search.php?author='.(libelibplus::special_raw_string($author)).'" target="_parent">'.$author.'</a>'?>			    
			<? endforeach; ?>
		<? else:?>
				--
		<? endif;?>
            	</td></tr>
            
            <tr><td class="list-label"><?=$eLib["html"]["publisher"]?>:</td><td>
		<? if (!empty($book['publisher'])) : ?>            
            	<a href="advanced_search.php?publisher=<?=libelibplus::special_raw_string($book['publisher'])?>"><?=$book['publisher']?></a>
        <? else: ?>
        		--
        <? endif; ?>
            	</td></tr>
	<? if ($showCategoryAndCallNum):?>
			<tr><td class="list-label"><?=$eLib["html"]["category"]?>:</td><td>
		<? if (!empty($book['category'])) : 
			if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
				$catTitle = explode('-',$book['category']);
				if (count($catTitle) > 2) {
					if ($intranet_session_language == 'en') {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[1];
					}	
					else {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[2];
					}
				}
				else {
					$dispTitle = $book['category'];
				}
			}
			else {
				$dispTitle = $book['category']; 
			}		
		?>
				<a href="advanced_search.php?category=<?=libelibplus::special_raw_string($book['category'])?>"><?=$dispTitle?></a><?=(empty($book['subcategory']))? '' : ' > <a href="advanced_search.php?subcategory='.libelibplus::special_raw_string($book['subcategory']).'">'.$book['subcategory'].'</a>'?>
        <? else: ?>
        		--
		<? endif; ?>
				</td></tr>
				
		<?php if($book['callnumber']): ?>
			<tr><td class="list-label"><?=$eLib_plus["html"]['call_number']?>:</td><td><?=$book['callnumber']?></td></tr>
		<?php endif; ?>
		<?php if($book['isbns']): ?>
			<tr><td class="list-label"><?=$eLib["html"]['ISBN']?>:</td><td><?=implode(', ', $book['isbns'])?></td></tr>
		<?php endif; ?>
	<? endif;?>				
			<?=$this->getBookStatUI($book['id'], $book['ranking_statistics']?$book:array())?>            
            
          </table>
<?php 		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getBookItemsNextToCover
 	
 	
 	## book stat info: star rating, review count, hit rate, loan count
 	function getBookStatUI($book_id, $book=array()) {
 		global $eLib, $eLib_plus;
 		
		if ($book['ranking_statistics']) {
			$stat_data = $book;
		}
		else {
			$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
			$stat_data = $lelibplus->getBookStat();
		}
 		
 		ob_start();
?>
            <tr id="book_stat_row"><td class="ratings" colspan="2">
            <?=$this->getStarRatingUI($stat_data['rating'],false)?>
              <span data-review-count="<?=$stat_data['review_count']?>">(<?=$stat_data['review_count']?> <?=$eLib["html"]["reviews"]?><?=isset($stat_data['hit_rate'])? ', '.$stat_data['hit_rate'].' '.$eLib_plus["html"]["hitsofreading"]: ''?><?=isset($stat_data['loan_count'])? ', '.$eLib_plus["html"]["borrowed"].' '.$stat_data['loan_count'].' '.$eLib_plus["html"]["times"]: ''?>)</span></td></tr>
<?php 		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getBookStatUI
 
 
 	function getNavigationBar($data) {
 		global $intranet_session_language,$Lang;
 		$total = $data['total'];
 		$record_per_page = $data['record_per_page']?$data['record_per_page']:10;
 		$current_page = $data['current_page'];
 		$no_of_pages = ceil($total/$record_per_page);
 		ob_start();
 		
 		if ($data['show_total']) {
?> 			
			<span class="left">
				<span aria-hidden="true" class="center"><?=$Lang['libms']['bookmanagement']['total'].' '.$total.($data['unit'] ? ' '. $data['unit'] : '')?></span>
			</span>
<?						
 		}
 		
		if ($current_page == 1) {
?>			
			<span class="center">
				<span aria-hidden="true" class="center">&nbsp;</span>
			</span>			
<?			
		} 	
		else {
?>			
			<span class="center>">
				<a href="#" aria-label="Previous" id="PreviousBtn" class="nav-page"><span aria-hidden="true" class="center">&lt;</span></a>
			</span>
<?
		}	
?>
 			<?=$intranet_session_language=='b5'?'<span class="center">'.$Lang["libms"]["portal"]["navigation"]["num_name"].'</span>':''?>
 			<select name="page_no" id="page_no" class="center">
		<? for ($i=1; $i<=$no_of_pages; $i++) {?>
 				<option <?=($i==$current_page ? 'class="active" selected' : '')?> value="<?=$i?>"><?=$i?></option>
 		<? }?>
 			</select><span class="center"><?=$Lang["libms"]["portal"]["navigation"]["page"]?></span>

<?
		if ($current_page == $no_of_pages) {
?>			
			<span class="center hidden">
				<span aria-hidden="true" class="center">&gt;</span>
			</span>			
<?			
		} 	
		else {
?>			
			<span class="center>">
				<a href="#" aria-label="Next" id="NextBtn" class="nav-page"><span aria-hidden="true" class="center">&gt;</span></a>
			</span>
<?
		}	
?>
			<span class="center"> | </span>
			<span class="center"><?=$Lang["libms"]["portal"]["navigation"]["display_per_page"]?></span>
			<select name="record_per_page" id="record_per_page" class="center">
			<? for($i=10; $i<=100; $i=$i+10) {?>
	                 <option <?=($i==$record_per_page ?  'class="active" selected' : '')?> value="<?=$i?>"><?=$i?></option>
	        <? }?>   
			</select>
			<span class="center"><?=$Lang["libms"]["portal"]["navigation"]["item"]?></span>
		
<?php 		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getNavigationBar


 	function getSearchResultListFooter($nav_para){
 		ob_start();
?>
    <div class="table-footer">
    	<nav>
   		<?if ($nav_para['total']):?>
			<div style="width:100%; display:inline-block;">    		
  				<div class="<?=$nav_para['cust_nav_style']=='cust-pagination2' ? 'cust-pagination2' : 'cust-pagination'?>">
					<?=$this->getNavigationBar($nav_para)?>
            	</div>
            </div>
        <?else:?>
        	<ul class="pagination">
        		<li>&nbsp;</li>
        	</ul>
        <?endif;?>            
        </nav>
    </div>
<?php 		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getSearchResultListFooter


 	function getBookSubCategoryList($category){
 		global $eLib_plus;
 		
 		$lelibplus = new elibrary_plus();
 		$sub_categories = $lelibplus->getBookSubCategories($category);
 		
 		ob_start();
?> 		
 		<option>- <?=$eLib_plus["html"]["all"]?> -</option>
<? 				
 		if ($sub_categories) {
 			foreach((array)$sub_categories as $sub) {
?>
				<option value="<?=$sub['SubCategory']?>"><?=$sub['SubCategory']?></option>
<?				
 			}
 		}
 		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getBookSubCategoryList
 	
 	
 	function getReviewEditUI($book_id){
 		global $eLib;
 		
  		ob_start();		
?> 		
    			<div class="tab-pane active" id="act-review">
                	<div class="ratings">
                		<ul><li class="star-on" onclick="void(0)"><a></a></li><li onclick="void(0)"><a></a></li><li onclick="void(0)"><a></a></li><li onclick="void(0)"><a></a></li><li onclick="void(0)"><a></a></li></ul>
                    </div>
                    <form id="review_form">
                    <div class="form-group">
                    <textarea class="form-control" rows="3" name="content"></textarea>
                    </div>
					<input type='hidden' name='rate' value='1'/>
					<input type='hidden' name='book_id' value='<?=$book_id?>'/>
            		<button type="submit" class="btn-type1"><?=$eLib["html"]["submit"]?></button>
            		<button type="button" class="btn-type1 btn-cancel"><?=$eLib["html"]["cancel"]?></button>
                    </form>                    
        		</div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getReviewEditUI
 	
 	
 	function getReviewEditUIByReviewID($reviewID, $rating, $content){
 	    global $eLib;
 	    
 	    ob_start();
 	    ?>
    			<div class="tab-pane active" id="act-review-edit">
                    <?php echo $this->getStarRatingUI($rating,true,false,'',true);?>
                    <form id="review_edit_form">
                    <div class="form-group">
                    <textarea class="form-control" rows="3" name="editContent" id="editContent"><?php echo $content;?></textarea>
                    </div>
					<input type='hidden' name='editRating' id="editRating" value='<?php echo $rating;?>'/>
					<input type='hidden' name='ReviewID' id="ReviewID" value='<?php echo $reviewID;?>'/>
            		<button type="button" class="btn-type1" id="btnUpdateReview"><?php echo $eLib["html"]["submit"];?></button>
            		<button type="button" class="btn-type1 btn-cancel" id="btnCancelReview"><?=$eLib["html"]["cancel"]?></button>
                    </form>                    
        		</div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getReviewEditUIByReviewID
 	
 	
 	function getReviewListUI($book_id){
 		global $PATH_WRT_ROOT, $eLib, $eLib_plus, $Lang, $permission;
 		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
 		$limit = $elibplus_cfg['book_reviews_limit'] ? $elibplus_cfg['book_reviews_limit'] : 10;
 		
	    $lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
		$total_reviews = $lelibplus->getTotalBookReviews();
		$reviewItemList = $this->getReviewItemUI($book_id,0,$limit);
 		
  		ob_start();		
?>
        <div class="tab-pane active" id="view-review">

			<? if ($permission['admin']) : ?>            	
    			<a id="remove_all_reviews" href="ajax_ui_update.php?action=removeAllReview&book_id=<?=$book_id?>" class="teacher btn-remove pull-right <?=($total_reviews > 0)? 'show':'hidden'?>"><?=$eLib["html"]["remove_all_reviews"]?> <span class="glyphicon glyphicon-trash"></span></a>
    		<? endif;?>
            	<ul class="review_detail_list">
            		<?=$reviewItemList['html']?>
                </ul>
                
			<? if ($total_reviews > $limit) :?>
				<ul>				
					<li><a href="#" id="more_reviews" data-reviews-offset="<?=$limit?>" data-book_id="<?=$book_id?>" class="btn-more"><?=$eLib_plus["html"]["more"]?>...</a></li>
				</ul>		
			<? endif; ?>
                
        </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getReviewListUI
 	

	/*
	 * 	$offset --> starting point to get review items order by DateModified
	 * 	$limit	--> limit number of record to show
	 */
 	function getReviewItemUI($book_id, $offset=0, $limit=10, $review_id=''){
		global $eLib_plus, $Lang, $permission;
	    $lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
	    $reviews_data=$lelibplus->getBookReviews($offset, $limit, $review_id);
		$nr_of_reviews = count($reviews_data);
		$ret = array();
		$ret['offset'] = $offset+($nr_of_reviews<$limit?$nr_of_reviews:$limit);
 		
		$libms = new liblms();
		$book_review_allow_edit_own_book_review = $libms->get_system_setting('book_review_allow_edit_own_book_review');
		
  		ob_start();
  		
  		if (empty($reviews_data)&&$offset==0):?>
			<li class="no-record"><?=$eLib_plus["html"]["noreviews"]?></li>
	<?	else: 	
			foreach ((array)$reviews_data as $review):?>
	        	<li id="review_log_<?=$review['id']?>">
	            	<div class="col-xs-2 user-photo"><img src="<?=$review['image']?>"></div>
	                <div class="col-xs-10 review-detail">
	                	<div class="user-name"><?=$review['name'] ? $review['name'] : $eLib_plus["html"]["anonymous"] ?><span> <?=$review['class']?></span>
						<? if ($review['user_id'] == $_SESSION['UserID']): ?>                        	
	                		<span class="user-role"><span class="glyphicon glyphicon-user"></span><?=$Lang["libms"]["portal"]["me"]?></span>
	                	<? endif;?>
	                	</div>
	                	<? if ($permission['admin']): ?>
	                    	<a href="ajax_ui_update.php?action=removeReview&review_id=<?=$review['id']?>" class="btn-remove"><span class="glyphicon glyphicon-trash"></span></a>
	                    <? endif; ?>
	                    <?php if ($permission['admin'] || ($review['user_id'] == $_SESSION['UserID'] && $book_review_allow_edit_own_book_review)) {
	                              $isShowEditButton = true;
	                              $reviewID = $review['id'];
 	                          }
 	                          else {
 	                              $isShowEditButton = false;
 	                              $reviewID = '';
 	                          }
 	                    ?>
 	                    <span id="review_span_<?php echo $review['id'];?>">
    	                   	<?=$this->getStarRatingUI($review['rating'], true, $isShowEditButton, $reviewID)?>
    	                    <div class="review-content"><?=nl2br($review['content'])?></div>                            
    	                    <div class="review-date"><?=date('Y-m-d',strtotime($review['date']))?></div>
	                    </span> 
	                    <div class="review-likes" id="like_<?=$review['id']?>">
	                    	<a href="#" data-review_id="<?=$review['id']?>" data-book_id="<?=$book_id?>" class="btn-type1"><?=($lelibplus->getHelpfulCheck($review['id'])) ? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"] ?></a>
	                    	<span class="number-like"><?=$review['likes']?><span class="icon-like"></span></span></div>
	                </div>
	            </li>
<?
			endforeach;
		endif;
		
 		$x = ob_get_contents();
 		$ret['html'] = $x;
		ob_end_clean();
		return $ret;
 	}	// end getReviewItemUI

 	function getBookReviewContentUI($reviewID)
 	{
 	    $book_id = 0;
 	    $x = '';
 	    $lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
 	    $reviewAry = $lelibplus->getBookReviews($offset=0, $limit=1, $reviewID);
 	    if (count($reviewAry)) {
 	        $reviewAry = $reviewAry[0];
 	        $rating = $reviewAry['rating'];
 	        $content = $reviewAry['content'];
 	        $date = $reviewAry['date'];
 	        
 	        $x = $this->getStarRatingUI($rating, true, true, $reviewID);
 	        $x .= '<div class="review-content">'.nl2br($content).'</div>';
 	        $x .= '<div class="review-date">'.date('Y-m-d',strtotime($date)).'</div>';
 	    }
 	    return $x;
 	}   // end getBookReviewContentUI

	/* show recommend class level and content in book details */
	function getRecommendedInfo($book_id) {
		global $Lang;
		
    	$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
        $detail_data['recommend']=$lelibplus->getRecommendedClassLevels();
        
		ob_start();
?>
            <div class="recommended" id="recommended-info">
            	<span class="icon-recommend"></span><?=$Lang["libms"]["portal"]["recommendbookto"]?>: 
<?php 
	$i=0; 
	foreach ((array)$detail_data['recommend']['class_levels'] as$class_level) {
		if ($class_level['class_level_recommended']) {
			echo ($i>0?', ':'').'<a href="advanced_search.php?class_level_id='.$class_level['class_level_id'].'" class="recommend-to" target="_parent">'.$class_level['class_level_name'].'</a>';
			$i++;
		}            	
	}
?>	            	
                <div class="recommend-info"><?=$detail_data['recommend']['content']?></div>
            </div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getRecommendInfo
	
	
	/* for recommend edit form  */
	function getRecommendUI($book_id) {
		global $Lang, $eLib, $permission;
	
    	$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
        $detail_data['recommend']=$lelibplus->getRecommendedClassLevels();
	
		$class_table = array_chunk($detail_data['recommend']['class_levels'],3);
			
		ob_start();
?>
		<div class="tab-pane" id="act-recommend">
        	<form id="recommend_form">
 			<label class="recommend-all"><input type="checkbox" id="check_all"> <span class="custom-check"></span> <?=$Lang['Btn']['All']?></label>
       		<ul class="recommend-list">
	<? foreach ($class_table as $class_level_rows):?>    
	        
    	<? foreach ($class_level_rows as $class_level): ?>
				<li><label><input type="checkbox" <?=$class_level['class_level_recommended']? "checked='checked'":""?> name="class_level_ids[]" value="<?=$class_level['class_level_id']?>"><span class="custom-check"></span> <?=$class_level['class_level_name']?></label></li>
    	<? endforeach ?>

	<? endforeach ?>
       		</ul>
            <div class="form-group">
            <textarea name="content" class="form-control" rows="3"><?=$detail_data['recommend']['content']?></textarea>
			<input type='hidden' name='mode' value='<?=$detail_data['recommend']['mode']?>'/>
			<input type='hidden' name='book_id' value='<?=$book_id?>'/>
            </div>
    		<button type="submit" class="btn-type1"><?=$eLib["html"]["submit"]?></button>
    		<button type="button" class="btn-type1 btn-cancel"><?=$eLib["html"]["cancel"]?></button>
    		<button type="button" class="btn-type1 btn-delete"><?=$eLib["html"]["Delete"]?></button>
            </form>
		</div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getRecommendUI
	
 
 	function getRelatedBooksUI($book_id,$book_type='type_all') {
		global $PATH_WRT_ROOT, $Lang;
		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
		
		$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID'], $book_type);
		$percentage_1 = $elibplus_cfg['similarity_percentage_1'] ? $elibplus_cfg['similarity_percentage_1'] : 50;
		$percentage_2 = $elibplus_cfg['similarity_percentage_2'] ? $elibplus_cfg['similarity_percentage_2'] : 80;
		$related_books = $lelibplus->getSimilarBooks($percentage_1, $percentage_2);
		$number_of_books = count($related_books);
		
		ob_start();
?>
  	<div class="action-right <?=($_SESSION['UserID']? 'col-sm-4': '')?>">
    	<div class="related-books">
        	<div class="table-header"><?=$Lang["libms"]["portal"]["related_books"]?></div>
    		<div class="table-body">
            	<ul class="">
			<? 	for ($i=0; $i < $number_of_books; $i++) { ?>
					<li>
						<div class="book-covers">
				<?	if ($related_books[$i]['book_type'] == 'ebook') {
						echo $this->geteBookCover($related_books[$i], true, "div", "_blank", false);
					}   
					else {   
						$cover_type_id = rand(1,10);
						if ($related_books[$i]['image']) {
							$image_no = '';
						}
						else {
							$image_no = $cover_type_id;
						}
						$related_books[$i]['image_no'] = $image_no;												      	
						echo $this->getpBookCover($related_books[$i], true, "div", "_blank", false);
					}
				?>		
						</div>			
					</li>
			<?	}?>
                </ul>
            </div>
        </div>    
    </div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getRelatedBooksUI
 
 
 	/*
 	 * 	$data is in format of associative array (name=>array(id=>$id, name=>$name, page_type=>$page_type, type=>$type, value=>$value ...))
 	 */  	
 	function getPortalSettingView($data,$isPurchasedeBook=true) {
		global $Lang, $eLib_plus, $plugin;
		
		if ($data['ebook_carousel_interval']['value'] == 0) {
			$disp_eBookInterval = $Lang["libms"]["portal"]["setting"]["carousel_stop"];	
		}
		else {
			$ebookInterval = $this->convertSecToMin($data['ebook_carousel_interval']['value']);
			$disp_eBookInterval = $ebookInterval['min'] .' '. $Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_min"] . ' ';
			$disp_eBookInterval .=  $ebookInterval['sec'] .' '. $Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_sec"]; 
		}

		if ($data['pbook_carousel_interval']['value'] == 0) {
			$disp_pBookInterval = $Lang["libms"]["portal"]["setting"]["carousel_stop"];	
		}
		else {
			$pbookInterval = $this->convertSecToMin($data['pbook_carousel_interval']['value']);
			$disp_pBookInterval = $pbookInterval['min'] .' '. $Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_min"] . ' ';
			$disp_pBookInterval .=  $pbookInterval['sec'] .' '. $Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_sec"]; 
		}
		
		ob_start();
?>
		<div class="view-form"<?=$_GET['msg']?' style="display:none"':''?>>
			<div class="view-edit"><a href="#" class="btn-edit"><span class="glyphicon glyphicon-pencil"></span> <?=$Lang['Btn']['Edit']?></a></div>

		<? if ($data['page_type'] == 'opac'):?>
			<div class="category">
	           	<span class="category"><?=$Lang["libms"]["portal"]["settings_tab_opac"]?></span>
		  	</div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["open_opac"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['open_opac']['display_value']?></div>
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_keyword_search"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['keyword_search']['display_value']?></div>
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_advanced_search"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['advanced_search']['display_value']?></div>
	        </div>
	        <? if ($isPurchasedeBook):?>
				<div class="view-row">
		          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["opac_allow_search_ebook"]?></label>
		          <div class="col-sm-8 view-value"><?=$data['opac_search_ebook']['display_value']?></div>
		        </div>
	        <? endif;?>
		<? endif;?>			
		
		
			<div class="category">
	           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["book_category"]?></span>
		  	</div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_list_all_books"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['book_category_show_all']['display_value']?></div>
	        </div>
	    <? if ($isPurchasedeBook):?>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_category_chi_ebooks"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['book_category_chi_ebooks']['display_value']?></div>
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_category_eng_ebooks"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['book_category_eng_ebooks']['display_value']?></div>
	        </div>
	    <? endif;?>
	    <? if (!$plugin['eLib_Lite']):?>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_category_physical_books"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['book_category_physical_books']['display_value']?></div>
	        </div>
	    <? endif;?>    
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_category_tags"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['book_category_tags']['display_value']?></div>
	        </div>

	
			<div class="category">
	           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["carousel"]?></span>
		  	</div>
		  	
		<? if ($isPurchasedeBook):?>
			<div class="subcategory">
				<span class="subcategory"><?=$Lang["libms"]["portal"]["eBooks"]?></span>
	        </div>
	    	<div class="view-row">
				<label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["carousel_ebook_inteval"]?></label>
				<div class="col-sm-8 view-value"><?=$disp_eBookInterval?></div>
	    	</div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_recommend"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['ebook_recommend_book']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['ebook_recommend_book_range']['display_value']?></div>							
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_hit"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['ebook_most_hit']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['ebook_most_hit_range']['display_value']?></div>							
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_new"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['ebook_new']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['ebook_new_range']['display_value']?></div>							
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_best_rated"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['ebook_best_rated']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['ebook_best_rated_range']['display_value']?></div>							
	        </div>
		<? endif;?>
	
		<? if (!$plugin['eLib_Lite']):?>
			<div class="subcategory">
				<span class="subcategory"><?=$Lang["libms"]["portal"]["pBooks"]?></span>
	        </div>
	    	<div class="view-row">
				<label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["carousel_pbook_inteval"]?></label>
				<div class="col-sm-8 view-value"><?=$disp_pBookInterval?></div>
	    	</div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_recommend"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['pbook_recommend_book']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['pbook_recommend_book_range']['display_value']?></div>							
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_loan"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['pbook_most_loan']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['pbook_most_loan_range']['display_value']?></div>							
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_new"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['pbook_new']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['pbook_new_range']['display_value']?></div>							
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_best_rated"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['pbook_best_rated']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['pbook_best_rated_range']['display_value']?></div>							
	        </div>
	    <? endif;?>
	        
	        
			<div class="category">
	           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["ranking"]?></span>
		  	</div>
	        
        <? if ($isPurchasedeBook):?>
			<div class="subcategory">
				<span class="subcategory"><?=$Lang["libms"]["portal"]["eBooks"]?></span>
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_active_reviewers"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['ebook_ranking_most_active_reviewers']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['ebook_ranking_most_active_reviewers_range']['display_value']?></div>							
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_helpful_reviews"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['ebook_ranking_most_helpful_reviewers']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['ebook_ranking_most_helpful_reviewers_range']['display_value']?></div>							
	        </div>
		<? endif;?>
	
		<? if (!$plugin['eLib_Lite']):?>
			<div class="subcategory">
				<span class="subcategory"><?=$Lang["libms"]["portal"]["pBooks"]?></span>
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_active_reviewers"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['pbook_ranking_most_active_reviewers']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['pbook_ranking_most_active_reviewers_range']['display_value']?></div>							
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_active_borrowers"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['pbook_ranking_most_active_borrowers']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['pbook_ranking_most_active_borrowers_range']['display_value']?></div>							
	        </div>
	
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_helpful_reviews"]?></label>
	          <div class="col-sm-3 view-value"><?=$data['pbook_ranking_most_helpful_reviewers']['display_value']?></div>
			  <label class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
			  <div class="col-sm-2 view-value"><?=$data['pbook_ranking_most_helpful_reviewers_range']['display_value']?></div>							
	        </div>
	    <? endif;?>
	        
		<? if ($data['page_type'] == 'internal'):?>
			<div class="category">
	           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["my_friends"]?></span>
		  	</div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["open_my_friends"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['open_my_friends']['display_value']?></div>
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["my_friends_search_scope"]?></label>
	          <div class="col-sm-8 view-value">
	          	<? 	switch ($data['my_friends_scope']['value']) {
	          			case 1:
	          				echo $Lang["libms"]["portal"]["setting"]["my_friends_scope"]["same_classlevel"];
	          				break;
	          			case 2:
	          				echo $Lang["libms"]["portal"]["setting"]["my_friends_scope"]["all_classes"];
	          				break;
	          			default:
	          				echo $Lang["libms"]["portal"]["setting"]["my_friends_scope"]["same_class"];
	          				break;
	          		}
	          	?>
	          </div>
	        </div>
		<? endif;?>			
	        
			<div class="category">
	           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["announcement"]?></span>
		  	</div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_news"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['show_news']['display_value']?></div>
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_opening_hours"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['show_opening_hours']['display_value']?></div>
	        </div>
			<div class="view-row">
	          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_rules"]?></label>
	          <div class="col-sm-8 view-value"><?=$data['show_rules']['display_value']?></div>
	        </div>

		</div>        
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// getPortalSettingView


 	/*
 	 * 	$data is in format of associative array (id=>$id, name=>$name, page_type=>$page_type, type=>$type, value=>$value ...)
 	 */  	
 	function getPortalSettingField($data, $remark='') {
 		global $Lang, $eLib_plus;
		ob_start();
		switch ($data['type']) {
			case 'BOOL':
?>
		<input type="radio" id='id_input_<?=$data['name']?>_1' name="setting[<?=$data['name']?>]" value="1" <?=empty($data['value'])?'':'checked';?>> <label for="id_input_<?= $data['name'] ?>_1"><?=$Lang['libms']["settings"]['system']['yes']?></label>&nbsp;
<?
	if (!empty($remark)) {
		echo "<span>(".$remark.")</span>&nbsp;";
	}
?>		  
		<input type="radio" id='id_input_<?=$data['name']?>_0' name="setting[<?=$data['name']?>]" value="0" <?=empty($data['value'])?'checked':'';?>> <label for="id_input_<?= $data['name'] ?>_0"><?=$Lang['libms']["settings"]['system']['no']?></label>
<?				
			break;
			
			case 'INT':
?>			
		<select class="select-field form-control" id='id_input_<?=$data['name']?>' name="setting[<?=$data['name']?>]">
	      <option value="1"<?=$data['value']==1? ' selected':''?>><?=$eLib_plus["html"]["thisweek"]?></option>
<?php
    if ($data['name'] == 'ebook_most_hit_range') {
        $selected = $data['value']==2? ' selected':'';
        echo '<option value="2"'.$selected.'>'.$eLib_plus["html"]["thismonth"].'</option>';
    }
?>
	      <option value="0"<?=empty($data['value'])? ' selected':''?>><?=$eLib_plus["html"]["accumulated"]?></option>
	    </select>
<?			    			
			break;
		}
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getPortalSettingField
 	
 	
 	// convert second(s) to min : sec 	
 	function convertSecToMin($val) {
		if ($val > 0) {
			$minute = floor($val / SIXTY);
			$second = $val % SIXTY;
		}
		else {
			$minute = 0;
			$second = 0;
		}
		return (array('min'=>$minute,'sec'=>$second));
 	}
 	
 	function getInterval($data) {
 		global $Lang;
		
		$type = substr($data['name'],0,5);	//	ebook / pbook
		$interval = $this->convertSecToMin($data['value']);
		$minute = $interval['min'];
		$second = $interval['sec'];
	
		$x = '<div class="col-sm-1 edit-value"><select class="select-field form-control" id="id_input_'.$type.'_min" name="setting['.$type.'_carousel_interval_min]">';
		for ($i=0; $i<SIXTY; $i++) {
			$x .= '<option value="'.$i.'"'. ($i==$minute ? ' selected':'').'>'.$i.'</option>';
		}
		$x .= '</select></div>';
		$x .= '<label class="col-sm-1 interval-unit">'.$Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_min"].'</label>';

		$x .= '<div class="col-sm-1 edit-value"><select class="select-field form-control" id="id_input_'.$type.'_sec" name="setting['.$type.'_carousel_interval_sec]">';
		for ($i=0; $i<SIXTY; $i++) {
			$x .= '<option value="'.$i.'"'. ($i==$second ? ' selected':'').'>'.$i.'</option>';
		}
		$x .= '</select></div>';
		$x .= '<label class="col-sm-1 interval-unit">'.$Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_sec"].'</label>';
						
		return $x;
 	}	// end getInterval
 	
 	/*
 	 * 	$data is in format of associative array (name=>array(id=>$id, name=>$name, page_type=>$page_type, type=>$type, value=>$value ...))
 	 */  	
  	function getPortalSettingForm($data,$isPurchasedeBook=true) {
		global $Lang, $eLib_plus,$eLib, $plugin;
		
		ob_start();
?>
			<div class="edit-form"<?=$_GET['msg']?'':' style="display:none"'?>>
			    <div class="table-header"><?=$Lang["libms"]["portal"]["setting"]["edit_settings"]?></div>
			  	<div class="table-body">
			    	<form class="form-horizontal">

					<? if ($data['page_type'] == 'opac'):?>
						<div class="category">
				           	<span class="category"><?=$Lang["libms"]["portal"]["settings_tab_opac"]?></span>
					  	</div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["open_opac"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['open_opac'])?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_keyword_search"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['keyword_search'])?></div>
				        </div>
						<div class="view-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_advanced_search"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['advanced_search'])?></div>
				        </div>
				        <? if ($isPurchasedeBook):?>
							<div class="view-row">
					          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["opac_allow_search_ebook"]?></label>
					          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['opac_search_ebook'])?></div>
					        </div>
					    <? endif;?>    
					    <input type="hidden" name="page_type" id="page_type" value="">
					<? endif;?>
					
					
						<div class="category">
				           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["book_category"]?></span>
					  	</div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_list_all_books"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['book_category_show_all'])?></div>
				        </div>
				    <? if ($isPurchasedeBook):?>    
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_category_chi_ebooks"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['book_category_chi_ebooks'],($data['page_type'] == 'opac')?$Lang["libms"]["portal"]["setting"]["opac_remark_allow_search_ebook"]:'')?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_category_eng_ebooks"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['book_category_eng_ebooks'],($data['page_type'] == 'opac')?$Lang["libms"]["portal"]["setting"]["opac_remark_allow_search_ebook"]:'')?></div>
				        </div>
				    <? endif;?>				    
				    <? if (!$plugin['eLib_Lite']):?>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_category_physical_books"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['book_category_physical_books'])?></div>
				        </div>
				    <? endif;?>    
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_category_tags"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['book_category_tags'])?></div>
				        </div>
			    	
						<div class="category">
				           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["carousel"]?></span>
					  	</div>
					  	
					<? if ($isPurchasedeBook):?>					  	
						<div class="subcategory">
							<span class="subcategory"><?=$Lang["libms"]["portal"]["eBooks"]?></span>
				        </div>
				    	<div class="edit-row">
							<label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["carousel_ebook_inteval"]?></label>
							<?=$this->getInterval($data['ebook_carousel_interval'])?>
							<div class="col-sm-4 interval-unit"><?=$Lang["libms"]["portal"]["setting"]["carousel_remark"]?></div>
				    	</div>				        
				    	<div class="edit-row">
							<label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_recommend"]?></label>
							<div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['ebook_recommend_book'])?></div>
							<label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
							<div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['ebook_recommend_book_range'])?></div>							
				    	</div>
						<div class="edit-row">
				          	<label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_hit"]?></label>
				          	<div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['ebook_most_hit'])?></div>
				          	<label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          	<div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['ebook_most_hit_range'])?></div>
				        </div>
						<div class="edit-row">
				          	<label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_new"]?></label>
				          	<div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['ebook_new'])?></div>
				          	<label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          	<div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['ebook_new_range'])?></div>
				        </div>
						<div class="edit-row">
				          	<label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_best_rated"]?></label>
				          	<div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['ebook_best_rated'])?></div>
				          	<label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          	<div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['ebook_best_rated_range'])?></div>
				        </div>
					<? endif;?>
				
					<? if (!$plugin['eLib_Lite']):?>
						<div class="subcategory">
							<span class="subcategory"><?=$Lang["libms"]["portal"]["pBooks"]?></span>
				        </div>
				    	<div class="edit-row">
							<label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["carousel_pbook_inteval"]?></label>
							<?=$this->getInterval($data['pbook_carousel_interval'])?>
							<div class="col-sm-4 interval-unit"><?=$Lang["libms"]["portal"]["setting"]["carousel_remark"]?></div>
				    	</div>				        
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_recommend"]?></label>
				          <div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['pbook_recommend_book'])?></div>
				          <label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          <div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['pbook_recommend_book_range'])?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_loan"]?></label>
				          <div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['pbook_most_loan'])?></div>
				          <label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          <div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['pbook_most_loan_range'])?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_new"]?></label>
				          <div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['pbook_new'])?></div>
				          <label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          <div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['pbook_new_range'])?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_best_rated"]?></label>
				          <div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['pbook_best_rated'])?></div>
				          <label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          <div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['pbook_best_rated_range'])?></div>
				        </div>
				    <? endif;?>    
				        
				        
						<div class="category">
				           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["ranking"]?></span>
					  	</div>
				        
					<? if ($isPurchasedeBook):?>				        
						<div class="subcategory">
							<span class="subcategory"><?=$Lang["libms"]["portal"]["eBooks"]?></span>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_active_reviewers"]?></label>
				          <div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['ebook_ranking_most_active_reviewers'])?></div>
				          <label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          <div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['ebook_ranking_most_active_reviewers_range'])?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_helpful_reviews"]?></label>
				          <div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['ebook_ranking_most_helpful_reviewers'])?></div>
				          <label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          <div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['ebook_ranking_most_helpful_reviewers_range'])?></div>
				        </div>
					<? endif;?>
				
					<? if (!$plugin['eLib_Lite']):?>
						<div class="subcategory">
							<span class="subcategory"><?=$Lang["libms"]["portal"]["pBooks"]?></span>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_active_reviewers"]?></label>
				          <div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['pbook_ranking_most_active_reviewers'])?></div>
				          <label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          <div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['pbook_ranking_most_active_reviewers_range'])?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_active_borrowers"]?></label>
				          <div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['pbook_ranking_most_active_borrowers'])?></div>
				          <label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          <div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['pbook_ranking_most_active_borrowers_range'])?></div>
				        </div>
				
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_most_helpful_reviews"]?></label>
				          <div class="col-sm-3 edit-value"><?=$this->getPortalSettingField($data['pbook_ranking_most_helpful_reviewers'])?></div>
				          <label class="col-sm-1 control-label"><?=$Lang["libms"]["portal"]["setting"]["data_range"]?></label>
				          <div class="col-sm-1 edit-value"><?=$this->getPortalSettingField($data['pbook_ranking_most_helpful_reviewers_range'])?></div>
				        </div>
				    <? endif;?>   

					<? if ($data['page_type'] == 'internal'):?>
						<div class="category">
				           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["my_friends"]?></span>
					  	</div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["open_my_friends"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['open_my_friends'])?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["my_friends_search_scope"]?></label>
				          <div class="col-sm-8 edit-value">
							<input type="radio" id="id_input_my_friends_scope_0" name="setting[my_friends_scope]" value="0" <?=empty($data['my_friends_scope']['value']) ? 'checked':''?>> <label for="id_input_my_friends_scope_0"><?=$Lang["libms"]["portal"]["setting"]["my_friends_scope"]["same_class"]?></label>&nbsp;
							<input type="radio" id="id_input_my_friends_scope_1" name="setting[my_friends_scope]" value="1" <?=$data['my_friends_scope']['value'] == '1' ? 'checked':''?>> <label for="id_input_my_friends_scope_1"><?=$Lang["libms"]["portal"]["setting"]["my_friends_scope"]["same_classlevel"]?></label>&nbsp;
							<input type="radio" id="id_input_my_friends_scope_2" name="setting[my_friends_scope]" value="2" <?=$data['my_friends_scope']['value'] == '2' ? 'checked':''?>> <label for="id_input_my_friends_scope_2"><?=$Lang["libms"]["portal"]["setting"]["my_friends_scope"]["all_classes"]?></label>
				          </div>
				        </div>
					<? endif;?>			

				        
						<div class="category">
				           	<span class="category"><?=$Lang["libms"]["portal"]["setting"]["announcement"]?></span>
					  	</div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_news"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['show_news'])?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_opening_hours"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['show_opening_hours'])?></div>
				        </div>
						<div class="edit-row">
				          <label class="col-sm-3 control-label"><?=$Lang["libms"]["portal"]["setting"]["display_rules"]?></label>
				          <div class="col-sm-8 edit-value"><?=$this->getPortalSettingField($data['show_rules'])?></div>
				        </div>
				
				        <div class="edit-row">
				          <div class="col-sm-12 row-center">
				            <button type="submit" class="btn-type1"><?=$eLib["html"]["submit"]?></button>	
				            <button class="btn-type1 btn-cancel"><?=$eLib["html"]["cancel"]?></button>
				          </div>
				        </div>

			    	</form>
			  	</div>  
			</div>
 
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
 	}	// getPortalSettingForm
 
 	
 	/* show all left menu for internal user */ 
 	function getRankingLeftMenu($ranking_type='best_rated'){
		global $Lang, $eLib_plus, $PATH_WRT_ROOT, $plugin;

		include_once($PATH_WRT_ROOT."includes/libelibrary.php");
		
		$lelibrary = new elibrary();
		$isPurchasedeBook = $lelibrary->isPurchasedeBook();

		ob_start();
?>
    <ul class="side-tabs nav nav-pills nav-stacked col-md-3">
      <div class="icon1<?=(($ranking_type == 'best_rated' || $ranking_type == 'most_hit' || $ranking_type == 'most_loan') ? ' on' : '')?>"></div>
      <li class="type1<?=$ranking_type == 'best_rated' ? ' active' : ''?>"><a href="#tab-best-rated" id="best_rated" data-toggle="pill"><?=$Lang["libms"]["portal"]["best_rated"]?></a></li>
    <? if ($isPurchasedeBook):?>  
      <li class="type1<?=$ranking_type == 'most_hit' ? ' active' : ''?>"><a href="#tab-most-hit" id="most_hit" data-toggle="pill"><?=$Lang["libms"]["portal"]["most_hit"]?></a></li>
    <? endif;?>
    <? if (!$plugin['eLib_Lite']):?>  
      <li class="type1<?=$ranking_type == 'most_loan' ? ' active' : ''?>"><a href="#tab-most-loan" id="most_loan" data-toggle="pill"><?=$Lang["libms"]["portal"]["most_loan"]?></a></li>
    <? endif;?>  
	  <div class="icon2<?=(($ranking_type == 'most_active_borrowers' || $ranking_type == 'most_active_reviewers') ? ' on' : '')?>"></div>
	<? if (!$plugin['eLib_Lite']):?>  
      <li class="type2<?=$ranking_type == 'most_active_borrowers' ? ' active' : ''?>"><a href="#tab-most-active-borrowers" id="most_active_borrowers" data-toggle="pill"><?=$eLib_plus["html"]["mostactiveborrowers"]?></a></li>
    <? endif;?>  
      <li class="type2<?=$ranking_type == 'most_active_reviewers' ? ' active' : ''?>"><a href="#tab-most-active-reviewers" id="most_active_reviewers" data-toggle="pill"><?=$eLib_plus["html"]["mostactivereviewers"]?></a></li>
      <div class="icon3<?=(($ranking_type == 'most_helpful_review') ? ' on' : '')?>"></div>
      <li class="type3<?=$ranking_type == 'most_helpful_review' ? ' active' : ''?>"><a href="#tab-most-helpful-review" id="most_helpful_review" data-toggle="pill"><?=$eLib_plus["html"]["mosthelpfulreview"]?></a></li>
    </ul>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
 	}	// end getRankingLeftMenu
 	
 	
 	/*
 	 * 	@param	$ranking_type:	best_rated, most_hit, most_loan, most_borrower, most_reviewer, most_helpful_review
 	 * 			$range: accumulated, thisweek, thismonth
 	 * 			$layout: cover, list
 	 * 			$book_type: ""(all), ebook, physical
 	 */
 	function getRankingHeader($ranking_type="best_rated",$range="accumulated",$layout="cover", $book_type=""){
		global $Lang, $eLib_plus, $PATH_WRT_ROOT, $plugin;
		include_once($PATH_WRT_ROOT."includes/libelibrary.php");
		
		$lelibrary = new elibrary();
		$isPurchasedeBook = $lelibrary->isPurchasedeBook();
		
		switch($ranking_type) {
			case "best_rated":
					$title = $Lang["libms"]["portal"]["best_rated"];
				break;
			case "most_hit":
					$title = $Lang["libms"]["portal"]["most_hit"];
				break;
			case "most_loan":
					$title = $Lang["libms"]["portal"]["most_loan"];
				break;
			case "most_active_borrowers":
					$title = $eLib_plus["html"]["mostactiveborrowers"];
				break;
			case "most_active_reviewers":
					$title = $eLib_plus["html"]["mostactivereviewers"];
				break;
			case "most_helpful_review":
					$title = $eLib_plus["html"]["mosthelpfulreview"];
				break;
		}
		ob_start();
?>
        <div class="table-header"> <span id="header-title"><?=$title?></span>
          <div class="btn-views">
<? if (($ranking_type == 'best_rated') || ($ranking_type == 'most_active_reviewers') || ($ranking_type == 'most_helpful_review')):?>           
            <select class="form-control" id="book_type">
           <? if ($isPurchasedeBook && !$plugin['eLib_Lite']):?>
              <option value="" <?=($book_type=='')?'selected':''?>><?=$Lang['libms']['bookmanagement']['allBooks']?></option>
           <? endif;?>
           <? if ($isPurchasedeBook):?>
              <option value="ebook" <?=($book_type=='ebook')?'selected':''?>><?=$Lang["libms"]["portal"]["eBooks"]?></option>
           <? endif;?>
           <? if (!$plugin['eLib_Lite']):?>
              <option value="physical" <?=($book_type=='physical')?'selected':''?>><?=$Lang["libms"]["portal"]["pBooks"]?></option>
		   <? endif;?>              
            </select>
<? endif;?>          
            <select class="form-control" id="range">
              <option value="thisweek" <?=($range=='thisweek')?'selected':''?>><?=$eLib_plus["html"]["thisweek"]?></option>
<?php
    if ($ranking_type == 'most_hit') {
        $selected = $range=='thismonth' ? ' selected' : '';
        $style = ' style="display:"';
    }
    else {
        $selected = '';
        $style = ' style="display:none;"';
    }
    echo '<option id="thismonth" value="thismonth"'.$selected.$style.'>'.$eLib_plus["html"]["thismonth"].'</option>';
?>
              <option value="accumulated" <?=($range=='accumulated')?'selected':''?>><?=$eLib_plus["html"]["accumulated"]?></option>
            </select>
<? if ($layout == "cover"):?>            
            <span class="btn-table-view off glyphicon glyphicon-th-large"></span> 
            <a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>
<? elseif ($layout == "list"):?>
            <a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a> 
            <span class="btn-list-view off glyphicon glyphicon-th-list"></span>
<? endif;?>             
          </div>
        </div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
 	}	// end getRankingHeader
 	
 	
 	
 	function getRankingCoverviewBooks($rank_data){
 		$showRead = $_SESSION['UserID'] ? true : false;
 		
		ob_start();
?>
		<ul class="ranking-list">
<?		if ($rank_data):
			$j = 0;
  			foreach (array_slice($rank_data,0,5) as $k=>$book):
	 			if ($book['image']) {
					$image_no = '';
				}
				else {
					$image_no = $j%10+1;
					$j++;
				}
				$book['image_no'] = $image_no;
				$num = $k + 1;
				$book['ranking_statistics'] = true;	// force to use ranking statistics
?>
            <li class="rank-<?=$num?>">
              <div class="rank"><?=$num?></div>
              <div class="book-covers">
				<?=($book['book_type'] == 'ebook') ? $this->geteBookCover($book, true, "div", "", $showRead) : $this->getpBookCover($book, true, "div")?>              
              </div>
              <?=$this->getBookItemsNextToCover($book)?>
            </li>
<? 			endforeach;
 		else:
?> 		
			<li>
				<?=$this->getNoRecordFoundBookCover()?>		
			</li>
<?
		endif;
?>
		</ul>
<? 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
 	}	// end getRankingCoverviewBooks
 	

 	function getRankingListviewBooks($rank_data){
 		global $eLib, $Lang, $eLib_plus;
 		
 		$libelibplus = new libelibplus();
		if(count($rank_data)>1) {
			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
		}
		else {
			$navigation_id = '';
		}
 		
		ob_start();
?>
    <div class="table-responsive">
      <table class="table-list">
      	<thead>
        	<tr valign="bottom">
            	<th>#</th>
            	<th>&nbsp;</th>
            	<th><?=$eLib["html"]["book_title"]?></th>
            	<th><?=$eLib["html"]["author"]?></th>
            	<th><?=$eLib["html"]["publisher"]?></th>
            	<th><?=$Lang["libms"]["portal"]["result_list"]["hits_loans"]?></th>
            	<th><?=$eLib["html"]["rating"]?></th>
            </tr>
        </thead>
      	<tbody>
<? if ($rank_data):
		foreach ((array)$rank_data as $i=>$book):
		$j = $i + 1;
?>
        	<tr>
            	<td><?=$j?></td>
            	<td>
			<? if (!empty($book['url']) && !($book['type'] == 'expired')) :?>
			  <a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon-ebooks" href="javascript:<?=$book['url']?>" ></a>
			<? elseif(($book['type'] == 'expired')): ?>
			  <a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon-ebooks-off"></a>
			<? endif; ?>
            		</td>
            		
			<? if (!($book['type'] == 'expired')) : ?>
				<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>				
				<td><?=($book['author']?$book['author']:'--')?></td>
			    <td><?=($book['publisher']?$book['publisher']:'--')?></td>
			    <td><?=isset($book['hit_rate'])? $book['hit_rate']: $book['loan_count']?></td>
			    <td><?=$book['rating']?></td>
			<? else: ?>
				<td class="off"><a class="fancybox book-detail-fancybox fancybox.iframe title-off" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a></td>
				<td class="off"><?=($book['author']?$book['author']:'--')?></td>
			    <td class="off"><?=($book['publisher']?$book['publisher']:'--')?></td>
			    <td class="off"><?=isset($book['hit_rate'])? $book['hit_rate']: $book['loan_count']?></td>
			    <td class="off"><?=$book['rating']?></td>
			<? endif; ?>
            </tr>
<?		endforeach;            
   else:?>
        	<tr>
            	<td colspan="7"><?=$eLib["html"]["no_record"]?></td>
            </tr>
<? endif;?>            
         </tbody>
      </table>
    </div>
 <?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
 	}	// end getRankingListviewBooks

 	
	function getRankingMostActiveBorrowers($rank_data) {
		global $eLib, $eLib_plus;
		
		ob_start();
?>
        <div class="table-responsive">
			<table class="table-list ranking-list">
				<thead>
					<tr valign="bottom">
						<th>&nbsp;</th>
						<th><?=$eLib["html"]["student"]?></th>
            			<th>&nbsp;</th>
            			<th><?=$eLib_plus["html"]["noofbooksborrowed"]?></th>
            		</tr>
        		</thead>
      			<tbody>
<? if ($rank_data):      			
   	   foreach ((array)$rank_data as $num=>$user) :?>
        			<tr<?=$num<3 ? ' class="rank-'.($num+1).'"' : ''?>>
            			<td><div class="rank"><?=$num+1?></div></td>
            			<td><div class="user-photo"><img src="<?=$user['image']?>"></div></td>
            			<td><?=$user['name']?> <?=$user['class']?></td>
            			<td><?=$user['loan_count']?> <?=$eLib_plus["html"]["books_unit"]?></td>
            		</tr>
<? 	   endforeach;
   else:?>
        	<tr>
            	<td colspan="4"><?=$eLib["html"]["no_record"]?></td>
            </tr>
<? endif;?>            
            	</tbody>
            </table>
        </div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}	// end getRankingMostActiveBorrowers


	function getRankingMostActiveReviewers($rank_data) {
		global $eLib, $eLib_plus, $Lang;
		
		ob_start();
?>
        <div class="table-responsive">
			<table class="table-list ranking-list">
				<thead>
					<tr valign="bottom">
						<th>&nbsp;</th>
						<th><?=$eLib_plus["html"]["reviewer"]?></th>
            			<th>&nbsp;</th>
            			<th><?=$eLib["html"]["num_of_review"]?></th>
            			<th><?=$eLib_plus["html"]["lastreview"]?></th>
            		</tr>
        		</thead>
      			<tbody>
<? if ($rank_data):
 		foreach ((array)$rank_data as $num=>$user) :?>
        			<tr<?=$num<3 ? ' class="rank-'.($num+1).'"' : ''?>>
            			<td><div class="rank"><?=$num+1?></div></td>
            			<td><div class="user-photo"><img src="<?=$user['image']?>"></div></td>
            			<td><?=$user['name']?> <?=$user['class']?></td>
      			
            			<td class="no_of_review"><a href="#rank_review_<?=$num+1?>"><?=$user['review_count']?> <?=$eLib["html"]["reviewsUnit"]?><span class="id" style="display:none;"><?=$user['user_id']?></span></a></td>
            			<td><a href="book_details.php?book_id=<?=$user['reviews'][0]['id']?>" class="fancybox book-detail-fancybox fancybox.iframe title" title="<?=$user['reviews'][0]['title'].' - '.$user['reviews'][0]['author']?>"><?=$user['reviews'][0]['title']?></a></td>
            		</tr>
            		
                    <tr class="ranking_user_review_container" id="rank_review_<?=$num+1?>" style="display:none">
                    	<td colspan="5" class="ranking_user_review_detail ranking_user_list_log_open">
	                       	<ul class="ranking_user_review"><p class="spacer"></p></ul>
	                       	<div class="more_reviews"><a href="#" data-reviews-offset="0" data-user-id="<?=$user['user_id']?>" class="btn-more"><?=$eLib_plus["html"]["more"]?>...</a></div>
	                       	<div class="ranking_user_list_log_open_shadow"><a href="#rank_review_<?=$num+1?>"><?=$Lang["libms"]["portal"]["hide"]?></a></div>
			  				
                       	</td>
                    </tr>
                    
<? 		endforeach;
   else:?>
        	<tr>
            	<td colspan="6"><?=$eLib["html"]["no_record"]?></td>
            </tr>
<? endif;?> 
            	</tbody>
            </table>
        </div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}	// end getRankingMostActiveReviewers
	
	
	function getRankingMostHelpfulReview($rank_data) {
		global $eLib, $eLib_plus;
		
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);

		$showRead = $_SESSION['UserID'] ? true : false;
						
		$j = 0;
		ob_start();
?>
          <ul class="ranking-list reviews">
<? if ($rank_data):          
 	foreach ((array)$rank_data as $k=>$review) :
		if ($review['image']) {
			$image_no = '';
		}
		else {
			$image_no = $j%10+1;
			$j++;
		}
		$review['image_no'] = $image_no;
		$num = $k + 1;
?>
			<li<?=$k<3 ? ' class="rank-'.($num).'"' : ''?>>
			  <div class="rank"><?=$num?></div>
    		  <div class="user-photo"><img src="<?=$review['user_image']?>"></div>
              <div class="review-detail">
				<div class="user-name"><?=$review['name'] ? $review['name'] : $eLib_plus["html"]["anonymous"] ?><span> <?=$review['class']?></span></div>
				<?=$this->getStarRatingUI($review['rating'])?>
                <div class="review-content"><?=nl2br($review['content'])?></div>                            
                <div class="review-date"><?=date('Y-m-d',strtotime($review['date']))?></div>    
                <div class="review-likes" id="like_<?=$review['review_id']?>">
                <? if ($_SESSION['UserID']):?>
                	<a href="#" data-review_id="<?=$review['review_id']?>" data-book_id="<?=$review['book_id']?>" class="btn-type1"><?=($lelibplus->getHelpfulCheck($review['review_id'])) ? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"] ?></a>
                <? endif;?>	
                	<span class="number-like"><?=$review['likes']?><span class="icon-like"></span></span></div>
              </div>
              <div class="book-covers">
              	<?=($review['book_type'] == 'ebook') ? $this->geteBookCover($review, true, "div", "", $showRead) : $this->getpBookCover($review, true, "div")?>
              </div>              
            </li>
<? 	endforeach;
   else:?>
        	<li>
            	<?=$eLib["html"]["no_record"]?>
            </li>
<? endif;?> 
          </ul>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}	// end getRankingMostHelpfulReview
 
 
 	function getStatisticsAllReviewsHeader() {
		global $eLib, $eLib_plus, $Lang;

		// default one week before up to today
		$searchFromDate = ($searchFromDate == '') ? date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 7, date("Y"))) : $searchFromDate;
		$searchToDate = ($searchToDate == '') ? date('Y-m-d') : $searchToDate;
		$Period = ($Period == 'Date') ? $Period : 'All';	// default All
		
		ob_start();
?>
        	<p><?=$eLib["html"]["all_reviews"]?></p>
            <label><input name="searchDataPeriod" type="radio" class="btn-radio-all" id="radioPeriod_All" value="All" <? echo ($Period=="All")?" checked":"" ?>><span class=""></span>&nbsp;&nbsp;<?=$eLib_plus["html"]["all"]?></label> <br> 
            <label><input name="searchDataPeriod" type="radio" class="btn-radio-date" id="radioPeriod_Date" value="Date" <? echo ($Period=="Date")?" checked":"" ?>>
            		&nbsp;<?=$Lang['General']['From']?> &nbsp;
            		<label>
	 					<div class="form-group date-pick">
	                		<div class='input-group date' id='datetimepicker1'>
	                		<?=$this->GET_DATE_PICKER('searchFromDate',$searchFromDate,$OtherMember="",$DateFormat="yyyy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="searchFromDate",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="form-control", $calendarIcon=true)?>
	                		</div>
	            		</div>
	            	</label>
                    &nbsp; <?=$Lang['General']['To']?>  &nbsp;
                    <label>
	 					<div class="form-group date-pick">
	                		<div class='input-group date' id='datetimepicker2'>
	                		<?=$this->GET_DATE_PICKER('searchToDate',$searchToDate,$OtherMember="",$DateFormat="yyyy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="searchToDate",$SkipIncludeJS=1, $CanEmptyField=1, $Disable=false, $cssClass="form-control", $calendarIcon=true)?>
	                		</div>
	            		</div>
            		</label>
            		<button class="btn-type1" id="apply_all_reviews"><?=$Lang['Btn']['Apply']?></button>
            </label>
		<? if($_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ) {?>            
            <div class="btn-export">
            	<a href="javascript:jsExportAllReviews('stats')"><span class="icon-export"></span><?=$eLib_plus["html"]["export"]["review_statistics"]?></a>
            	<a href="javascript:jsExportAllReviews('reviews')"><span class="icon-export"></span><?=$eLib_plus["html"]["export"]["reviews"]?></a>
            </div>
        <? }?>         
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}	// end getStatisticsAllReviewsHeader


 	function getStatisticsAllReviewsContent($data,$start_record_index=0) {
		global $eLib, $eLib_plus, $Lang;

		$libelibplus = new libelibplus();
		if(count($data)>1) {
			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
		}
		else {
			$navigation_id = '';
		}
		
		ob_start();

	    foreach ($data as $i=>$book) : 
			$j = $start_record_index + $i + 1;
?>
        	<tr>
            	<td><?=$j?></td>
            	<td>
			<? if (!empty($book['url']) && ($book['BookFormat'] != 'physical') && ($book['readable'])) :?>
			  <a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon-ebooks" href="javascript:<?=$book['url']?>" ></a>
			<? elseif(!$book['readable']): ?>
			  <a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon-ebooks-off"></a>
			<? endif; ?>
            		&nbsp;</td>
            		
			<? if ($book['readable']) : ?>
				<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>"title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
				<td><?=($book['author']?$book['author']:'--')?></td>			    
			    <td><?=$book['num_review']?></td>
			    <td><?=$book['last_submitted']?></td>
			<? else: ?>
				<td class="off"><a class="fancybox book-detail-fancybox fancybox.iframe title-off" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>"title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a></td>
				<td class="off"><?=($book['author']?$book['author']:'--')?></td>			    
			    <td class="off"><?=$book['num_review']?></td>
			    <td class="off"><?=$book['last_submitted']?></td>
			<? endif; ?>
            </tr>
	<?  endforeach;            
				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}	// end getStatisticsAllReviewsContent


 	function getStatisticsAllClassesHeader() {
		global $eLib, $Lang, $intranet_root, $button_select, $junior_mck;
	 	include_once("$intranet_root/includes/form_class_manage.php");
		include_once("$intranet_root/includes/libclass.php");
	 	$fcm = new form_class_manage();
		$libclass = new libclass();
		
		$tmp_button_select = $button_select; 
		$button_select = $Lang["libms"]["portal"]["all"];	// show first option to 'All'		

		if ($junior_mck) {
	        $ClassSelection= $libclass->getSelectClass_elibplus( "name='ClassName' id='ClassName_Search' class='form-control'", "" );
	        $ClassSelection= iconv("BIG5-HKSCS", "UTF-8//IGNORE",$ClassSelection);
		}
		else {
			$ClassSelection= $libclass->getSelectClass( "name='ClassName' id='ClassName_Search' class='form-control'", "" );
		}
		$button_select = $tmp_button_select; 
		
	 	include_once("$intranet_root/includes/libinterface.php");
	 	$linterface 	= new interface_html("default3.html");
	 	
	 	$ThisYear = Date("Y")*1;
		$aryMonth = array(array('01',1),array('02',2),array('03',3),array('04',4),array('05',5),array('06',6),array('07',7),array('08',8),array('09',9),array(10,10),array(11,11),array(12,12));
		$aryYear = array(array($ThisYear,$ThisYear), array($ThisYear-1,$ThisYear-1),array($ThisYear-2,$ThisYear-2),array($ThisYear-3,$ThisYear-3),array($ThisYear-4,$ThisYear-4),array($ThisYear-5,$ThisYear-5)); 
		$ThisMonth = Date("m")*1;
				
		$FromMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='FromMonth' id='FromMonth_Search' class='form-control'", "", $ThisMonth);
		$FromYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='FromYear' id='FromYear_Search' class='form-control'", "", $ThisYear);
		$ToMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='ToMonth' id='ToMonth_Search' class='form-control'", "", $ThisMonth);
		$ToYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='ToYear' id='ToYear_Search' class='form-control'", "", $ThisYear);
		
		ob_start();
?>
        	<p><?=$eLib["html"]["class_summary"]?></p> 
            <label><span class="width1"><?=$eLib["html"]["class"]?>:</span>
            	<?=$ClassSelection?>
			</label><br>    
            <label><span class="width1"><?=$Lang['Header']['Menu']['Period']?>:</span>
            	<?=$FromMonthSelection?><?=$FromYearSelection?>
            	<label> <?=$Lang['General']['To']?> </label>
            	<?=$ToMonthSelection?><?=$ToYearSelection?>
            </label>
            <button class="btn-type1" id="apply_all_classes"><?=$Lang['Btn']['Apply']?></button>
            
            <div class="btn-export">
            	<a href="javascript:jsExportStatistics()"><span class="icon-export"></span><?=$Lang['Btn']['Export']?></a>
            </div>
            
            <input type="hidden" name="student_id" id="student_id" value=''>         
            <input type="hidden" name="statistics_type" id="statistics_type" value=''>
<?            
				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}	// end getStatisticsAllClassesHeader


  	function getStatisticsAllClassesSummaryContent($data,$start_record_index=0, $show_column_title=false) {
		global $eLib, $eLib_plus, $Lang;
		
		ob_start();
?>
<? if ($show_column_title):?>
		<thead name="table-header-all-class">
        	<tr valign="bottom">
            	<th>#</th>
            	<th><a href="#" name="sort_c" class="sort-asc"><?=$eLib["html"]["class"]?><span class="glyphicon glyphicon-triangle-top"></span></a></th>
            	<th><a href="#" name="sort_nos"><?=$eLib["html"]["num_of_students"]?></a></th>
            	<th><a href="#" name="sort_tnobr"><?=$eLib["html"]['Total']."(".$eLib["html"]["num_of_book_hit"].")"?></a></th>
            	<th><a href="#" name="sort_tnobr"><?=$Lang["libms"]["portal"]["statistics"]["totalHitsChi"]?></a></th>
            	<th><a href="#" name="sort_tnobr"><?=$Lang["libms"]["portal"]["statistics"]["totalHitsEng"]?></a></th>
            	<th><a href="#" name="sort_anobr"><?=$eLib["html"]["Average"]."(".$eLib["html"]["num_of_book_hit"].")"?></a></th>
            	<th><a href="#" name="sort_tnor"><?=$eLib["html"]['Total']."(".$eLib["html"]["num_of_review"].")"?></a></th>
            	<th><a href="#" name="sort_anor"><?=$eLib["html"]["Average"]."(".$eLib["html"]["num_of_review"].")"?></a></th>
            </tr>
    	</thead>
<? endif;?>    	
  		<tbody>
  			<!-- Statistics AllClasses Content -->
<?
	    foreach ($data as $i=>$book) : 
			$j = $start_record_index + $i + 1;
?>
        	<tr>
            	<td><?=$j?></td>
            	<td class="col-class-name"><a href="#" data-class-name="<?=$book['class_name']?>"><?=$book['class_name']?></a></td>
            	<td><?=$book['num_student']?></td>
            	<td><?=$book['NumBookRead']?></td>
            	<td><?=$book['NumChiBookRead']?></td>
            	<td><?=$book['NumEngBookRead']?></td>
            	<td><?=$book['read_average']?></td>
            	<td><?=$book['NumBookReview']?></td>
            	<td><?=$book['review_average']?></td>
            </tr>
	<?  endforeach;?>            
        </tbody>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}	// end getStatisticsAllClassesSummaryContent
 

  	function getStatisticsByClassSummaryContent($data,$start_record_index=0, $show_column_title=false) {
		global $eLib, $eLib_plus, $Lang;
		
		ob_start();
?>
<? if ($show_column_title):?>
		<thead name="table-header-by-class">
        	<tr valign="bottom">
            	<th>#</th>
            	<th><a href="#" name="sort_sn"><?=$eLib["html"]["student_name"]?></a></th>
            	<th><?=$eLib["html"]["class"]?></th>
            	<th><a href="#" name="sort_cn" class="sort-asc"><?=$eLib["html"]["class_number"]?></a><span class="glyphicon glyphicon-triangle-top"></span></th>
            	<th><a href="#" name="sort_nobr"><?=$eLib["html"]["num_of_book_read"]?></a></th>
            	<th><a href="#" name="sort_hit"><?=$eLib["html"]["num_of_book_hit"]?></a></th>
            	<th><a href="#" name="sort_nor"><?=$eLib["html"]["num_of_review"]?></a></th>
            </tr>
    	</thead>
<? endif;?>    	
  		<tbody>
  			<!-- Statistics ByClass Content -->
<?
	if (count($data) > 0) {
	    foreach ((array)$data as $i=>$student) : 
			$j = $start_record_index + $i + 1;
	
			$link = '<a href="#" data-year-class-id="'.$student['YearClassID'].'" data-student-id="'.$student['user_id'].'" data-class-name="'.$student['class_name'].'">';
			$book_read = strip_tags($student['NumBookRead']);
			$book_read = $book_read == 0 ? '0' : $link.$book_read.'</a>';
			$book_review = strip_tags($student['NumBookReview']); 
			$book_review = $book_review == 0 ? '0' : $link.$book_review.'</a>';
?>
        	<tr>
            	<td><?=$j?></td>
            	<td class="col-student-name"><?=$link?><?=$student['student_name']?></a></td>
            	<td><?=$student['class_name']?></td>
            	<td><?=$student['class_number']?></td>
            	<td><?=$student['NumBooks']?></td>
            	<td class="col-book-read"><?=$book_read?></td>
            	<td class="col-book-review"><?=$book_review?></td>
            </tr>
	<?  endforeach;?>
<? 	}
	else {?>
			<tr><td colspan="7"><?=$eLib_plus["html"]["norecord"]?></td></tr>	
<?	} ?>
        </tbody>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}	// end getStatisticsByClassSummaryContent


  	function getStatisticsByStudentContent($data,$start_record_index=0, $show_column_title=false) {
		global $eLib, $eLib_plus;
		
		$libelibplus = new libelibplus();
		if(count($data)>1) {
			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
		}
		else {
			$navigation_id = '';
		}
		
		ob_start();
?>
<? if ($show_column_title):?>
		<thead name="table-header-student">
        	<tr valign="bottom">
            	<th>#</th>
            	<th>&nbsp;</th>
			    <th><a href="#" name="sort_bt" class="sort-asc"><?=$eLib["html"]["book_title"]?></a><span class="glyphicon glyphicon-triangle-top"></span></th>
			    <th><a href="#" name="sort_a" class="sort_column"><?=$eLib["html"]["author"]?></a></th>
			    <th><a href="#" name="sort_p" class="sort_column"><?=$eLib["html"]["Progress"]?></a></th>
			    <th><a href="#" name="sort_rt" class="sort_column"><?=$eLib["html"]["ReadTimes"]?></a></th>
			    <th><a href="#" name="sort_lr" class="sort_column"><?=$eLib["html"]["last_read"]?></a></th>
            </tr>
    	</thead>
<? endif;?>    	
  		<tbody>
  			<!-- Statistics ByStudent Content -->
<?
	    foreach ($data as $i=>$book) : 
			$j = $start_record_index + $i + 1;
			$row_class = ($book['readable']) ? '' : ' class="off"';
?>
        	<tr>
            	<td><?=$j?></td>
            	<td>
			<? if (!empty($book['url']) && ($book['BookFormat'] != 'physical') && ($book['readable'])) :?>
			  <a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon-ebooks" href="javascript:<?=$book['url']?>" ></a></td>
			  <td<?=$row_class?>><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
			<? elseif(!$book['readable']): ?>
			  <a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon-ebooks-off"></a></td>
			  <td<?=$row_class?>><a class="fancybox book-detail-fancybox fancybox.iframe title-off" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a></td>
			<? endif; ?>
				
				<td<?=$row_class?>><?=($book['author']?$book['author']:'--')?></td>			    
			    <td<?=$row_class?>>
			<? if (!empty($book['percentage'])) :?>
					<div class="book_progress"><span <?=($book['percentage']==100)? "class='done'" : 'style="left:'.($book['percentage']-100).'px;"'?>></span><em><?=$book['percentage']?></em></div>
			<? else: ?>
					--
			<? endif; ?>
			    </td>
			    <td<?=$row_class?>><?=$book['read_times']?></td>
			    <td<?=$row_class?>><?=$book['last_accessed']?></td>
            </tr>
	<?  endforeach;?>            
        </tbody>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
	}	// end getStatisticsByStudentContent


	// use in statistics
  	function getReviewByUserUI($reviews_data, $total=0, $start_record_index=0, $with_total_header=true){
		global $eLib;
		
 		$j = 0;
 		
 		$libelibplus = new libelibplus();
		if(count($reviews_data)>1) {
			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
		}
		else {
			$navigation_id = '';
		}
 		
 		$showRead = $_SESSION['UserID'] ? true : false;
 		
  		ob_start();
?>
	<? if ($with_total_header):?>
		<thead name="table-header-reviews">
        	<tr valign="bottom"><td class="reviews-header"><?=$eLib["html"]['Total'] .': '.$total.' '.$eLib["html"]["reviewsUnit"].$eLib["html"]["reviews"]?></td></tr>
      	</thead>
      	<tbody>
        <tr><td>
	<? endif;?>        
          <ul class="reviews">
  		
  	<? 	foreach ((array)$reviews_data as $k=>$review):
 			if ($review['image']) {
				$image_no = '';
			}
			else {
				$image_no = $j%10+1;
				$j++;
			}
			$review['image_no'] = $image_no;
			$review['navigation_id'] = $navigation_id;
  	?>
	        <li>
              	<div class="book-covers">
					<?=($review['book_type'] == 'ebook') ? $this->geteBookCover($review, true, "div", "", $showRead) : $this->getpBookCover($review, true, "div")?>              
              	</div>
                <div class="review-detail">
					<?=$this->getStarRatingUI($review['rating'])?>                
                    <div class="review-content"><?=nl2br($review['content'])?></div>                            
                    <div class="review-date"><?=date('Y-m-d',strtotime($review['date']))?></div>    
                    <div class="review-likes" id="like_<?=$review['review_id']?>"><span class="number-like"><?=$review['likes']?><span class="icon-like"></span></span></div>
                </div>
            </li>
	<?	endforeach;?>
		  </ul>
	<? if ($with_total_header):?>		  
		</td></tr></tbody>
	<? endif;?>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getReviewByUserUI
 
 
   	function getRankingMostActiveUserReview($reviews_data){
		global $eLib,$eLib_plus;
		
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		
		$showRead = $_SESSION['UserID'] ? true : false;
		
 		$j = 0; 		
  		ob_start();
?>
  		
  	<? 	foreach ((array)$reviews_data as $k=>$review):
 			if ($review['image']) {
				$image_no = '';
			}
			else {
				$image_no = $j%10+1;
				$j++;
			}
			$review['image_no'] = $image_no;
  	?>
	        <li class="user-review-detail">
                <div class="col-xs-9 review-detail">
					<?=$this->getStarRatingUI($review['rating'])?>                
                    <div class="review-content"><?=nl2br($review['content'])?></div>                            
                    <div class="review-date"><?=date('Y-m-d',strtotime($review['date']))?></div>    
                    <div class="review-likes" id="like_<?=$review['review_id']?>">
                    <? if ($_SESSION['UserID']):?>
                    	<a href="#" data-review_id="<?=$review['review_id']?>" data-book_id="<?=$review['book_id']?>" class="btn-type1"><?=($lelibplus->getHelpfulCheck($review['review_id'])) ? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"] ?></a>
                    <? endif;?>
                    	<span class="number-like"><?=$review['likes']?><span class="icon-like"></span></span></div>
                </div>
              	<div class="col-xs-2 book-covers">
					<?=($review['book_type'] == 'ebook') ? $this->geteBookCover($review, true, "div", "", $showRead) : $this->getpBookCover($review, true, "div")?>              
              	</div>
            </li>
	<?	endforeach;?>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getRankingMostActiveUserReview
 

	function getMyRecordCurrentLoan($loan_data){
		global $Lang, $eLib_plus, $eLib, $PATH_WRT_ROOT, $permission;
//		include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");
		$borrowCount = count($loan_data['loan_books']);
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		$loan_quota = $lelibplus->getUserLoanQuota('-1Def');		// all circulaton
		$libms = new liblms();
		$is_open_loan = $libms->IsSystemOpenForLoan($_SESSION['UserID']);
//		$tm = new TimeManager();
		
  		ob_start();
?>
		  	<!-- check out (current loan record) -->
       	  	<div class="table-cat current_loan">
            	<span class="title"><?=$eLib_plus["html"]["checkedout"]?></span>
                <span class="quota"><?=$Lang["libms"]["portal"]["quota"]["loan"]?>: <?=$borrowCount.'/'.$loan_quota?></span>
                <div class="table-responsive">
				<table class="table-list">
					<thead>
                        <tr valign="bottom">
							<th>#</th>
							<th><?=$eLib["html"]["book_title"]?></th>
							<th><?=$eLib_plus["html"]["checkedoutdate"]?></th>
							<th><?=$eLib_plus["html"]["duedate"]?></th>
							<th><?=$eLib_plus["html"]["renewcount"]?></th>
							<th><?=$eLib_plus["html"]["status"]?></th>
                        </tr>
					</thead>
					<tbody>
				<? if (!$loan_data['loan_books']): ?>
						<tr><td colspan="6"><?=$eLib_plus["html"]["norecord"]?></td></tr>
				<? else:?>
					<? foreach ($loan_data['loan_books'] as $num=>$book):
						$renewalQuotaResult = $lelibplus->IsRenewalAllowed($book['borrow_id']);
						if (!$is_open_loan) {
							$renewalQuotaResult = 0;	// cannot renew
						}
						// don't show number of overdue days due to performance?  
//						if ($book['overdue_days'] > 0 ) {
//							$duetime = strtotime($book['due_date']);
//							$nowtime = time();
//							$overdue_day_count = $tm->dayForPenalty($duetime, $nowtime);
//						}
					?>
						<tr <?=$book['overdue_days']>0? 'class="warning"' : ''?>>									
                        	<td><?=$num+1?></td>
                        	<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="record_loan" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
						    <td><?=$book['borrow_date']?></td>
						    <td><?=$book['due_date']?></td>
						    <td><?=$book['renew_count']?></td>
                        	<td>
						<? if ($book['overdue_days']>0) :?>                        	
                        	<?=''//$Lang["libms"]["CirculationManagement"]["msg"]["overdue1"].' '.$overdue_day_count.' '.$Lang["libms"]["CirculationManagement"]["msg"]["overdue2"]?>
                        	<?=$eLib_plus["html"]["overdue"] ?>
                        <? elseif ($permission['renew'] && $renewalQuotaResult==1 && $is_open_loan): ?>
                        	<a href="#" class="btn-type1 renew-button" data-borrow-id="<?=$book['borrow_id']?>"><?=$eLib_plus["html"]["renew"]?></a>
						<? elseif ($renewalQuotaResult==-4): ?>
						  	<?= $eLib['Book']["RenewalHoldBook"] ?>
						<? else: ?>
						  	<?= $eLib['Book']["Renewal"][$renewalQuotaResult] ?>
						<? endif; ?>
                        	</td>
						</tr>
					<? endforeach;?>
				<? endif;?>						
					</tbody>
				</table>
			  </div>                    
            </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordCurrentLoan


	function getMyRecordCurrentReserve($loan_data){
		global $Lang, $eLib_plus, $eLib;
		$reserveCount = count($loan_data['reserved_books']);
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		$reserve_quota = $lelibplus->getUserReserveQuota('-1Def');	// all circulaton
		
  		ob_start();
?>
          	<!-- reserve record -->
          	<div class="table-cat reserve">
            	<span class="title"><?=$eLib_plus["html"]["requesthold"] ?></span>
            	<span class="quota"><?=$Lang["libms"]["portal"]["quota"]["reserve"]?>: <?=$reserveCount.'/'.$reserve_quota?></span>
                <div class="table-responsive">
				<table class="table-list">
					<thead>
                        <tr valign="bottom">
							<th>#</th>
							<th><?=$eLib["html"]["book_title"]?></th>
							<th><?=$eLib_plus["html"]["requesteddate"]?></th>
<!--							<th><?=$Lang["libms"]["portal"]["my_record"]["checkout_due_date"]?></th>-->
							<th><?=$eLib_plus["html"]["status"]?></th>
							<th>&nbsp;</th>
                        </tr>
					</thead>
					<tbody>
				<? if (!$loan_data['reserved_books']): ?>
						<tr><td colspan="5"><?=$eLib_plus["html"]["norecord"]?></td></tr>
				<? else:?>
					<? foreach ($loan_data['reserved_books'] as $num=>$book): ?>
						<tr <?=$book['status']=='READY'? 'class="reminder"' :''?>>
                        	<td><?=$num+1?></td>
                        	<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="record_reserved" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
                        	<td><?=$book['reserve_date']?></td>
							<td><?=(($book['status']=='READY') ? $eLib_plus["html"]["readyforpickup"] : $eLib_plus["html"]["notreturnedyet"])?></td>
                        	<td>
								<div class="table_row_tool">
									<a href="ajax.php?action=handleReservation&book_id=<?=$book['id']?>" class="btn-remove"><span class="glyphicon glyphicon-trash"></span></a>
								</div>                        	
                        	</td>
						</tr>
					<? endforeach;?>
				<? endif;?>
					</tbody>
				</table>
			  </div>                    
            </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordCurrentReserve


	function getMyRecordCurrentPenalty($loan_data){
		global $eLib_plus, $eLib;
		
  		ob_start();
?>
            <!-- penalty --> 
            <div class="table-cat penalty">
           	  <span class="title"><?=$eLib_plus["html"]["outstandingpenalty"] ?></span>
                <div class="table-responsive">
				<table class="table-list">
					<thead>
                        <tr valign="bottom">
							<th>#</th>
							<th><?=$eLib["html"]["book_title"]?></th>
							<th><?=$eLib_plus["html"]["duedate"]?></th>
							<th><?=$eLib_plus["html"]["returndate"] ?></th>
							<th><?=$eLib_plus["html"]["reason"] ?></th>
							<th><?=$eLib_plus["html"]["totalpenalty"] ?></th>
							<th><?=$eLib_plus["html"]["outstandingpenalty"] ?></th>
                        </tr>
					</thead>
					<tbody>
				<? if (!$loan_data['penalty_current']): ?>
					    <tr><td colspan="7"><?=$eLib_plus["html"]["norecord"]?></td></tr>
				<? else: ?>
				    <? foreach ($loan_data['penalty_current'] as $num=>$book): ?>
						<tr>
                        	<td><?=$num+1?></td>
                        	<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="record_penalty_current" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
					        <td><?=$book['due_date']?></td>
					        <td><?=$book['return_date']?></td>
					        <td><?=$book['penalty_reason']=='LOST' ? $eLib_plus["html"]["booklost"] : $eLib_plus["html"]["overdue"].' '.$book['penalty_reason'].' '.$eLib_plus["html"]["days"]?></td>
					        <td>$<?=$book['penalty_total']?></td>
					        <td>$<?=$book['penalty_owing']?></td>
						</tr>
					<? endforeach;?>
				<? endif;?>
					</tbody>
				</table>
				</div>                    
            </div>               
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordCurrentPenalty


   	function getMyRecordCurrentStatusUI($loan_data){
  		ob_start();
?>
        <!--SIDE TAB 1 tab 1-->
		<div class="tab-pane active" id="loan-current">
			<?=$this->getMyRecordCurrentLoan($loan_data)?>
			<?=$this->getMyRecordCurrentReserve($loan_data)?>
			<?=$this->getMyRecordCurrentPenalty($loan_data)?>
		</div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordCurrentStatusUI


 	function getMyRecordLoanRecordContent($loan_data, $start_record_index=0){
		global $eLib_plus;
		
  		ob_start();
?>
		<? if (!$loan_data['loan_history'] && $start_record_index==0): ?>
			    <tr><td colspan="4"><?=$eLib_plus["html"]["norecord"]?></td></tr>
		<? else: ?>
		    <? foreach ($loan_data['loan_history'] as $i=>$book): 
		          $j = $start_record_index + $i + 1;
		    ?>
				<tr>
                	<td><?php echo $j;?></td>
                	<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="record_loan_history" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
				    <td><?=$book['borrow_date']?></td>
				    <td><?=$book['return_date']?></td>
				</tr>
			<? endforeach;?>
		<? endif;?>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordLoanRecordContent

 	function getMyRecordLoanRecordUI($loan_data, $nav_para){
 	    global $eLib_plus, $eLib;
 	    
 	    ob_start();
 	    ?>
        <!--SIDE TAB 1 tab 2-->
        <div class="tab-pane active" id="loan-record">
            <div class="table-responsive">
				<table class="table-list">
					<thead>
                        <tr valign="bottom">
							<th>#</th>
							<th><?=$eLib["html"]["book_title"]?></th>
							<th><?=$eLib_plus["html"]["checkedoutdate"]?></th>
							<th><?=$eLib_plus["html"]["returndate"]?></th>
                        </tr>
					</thead>
					<tbody>
					<?php echo $this->getMyRecordLoanRecordContent($loan_data, 0);?>
					</tbody>
				</table>
			</div>
			
        	<!-- footer: navigation bar -->
        	<?=$this->getSearchResultListFooter($nav_para)?>
			  					                   
		</div>
		
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordLoanRecordUI
 	
   	function getMyRecordPenaltyUI($loan_data){
		global $eLib_plus, $eLib;
		
  		ob_start();
?>
        <!--SIDE TAB 1 tab 3-->
        <div class="tab-pane active" id="loan-penalty">
            <div class="table-responsive">
				<table class="table-list">
					<thead>
                        <tr valign="bottom">
							<th>#</th>
							<th><?=$eLib["html"]["book_title"]?></th>
							<th><?=$eLib_plus["html"]["duedate"]?></th>
							<th><?=$eLib_plus["html"]["returndate"]?></th>
							<th><?=$eLib_plus["html"]["reason"]?></th>
							<th><?=$eLib_plus["html"]["totalpenalty"]?></th>
                        </tr>
					</thead>
					<tbody>
				<? if (!$loan_data['penalty_history']): ?>
					    <tr><td colspan="6"><?=$eLib_plus["html"]["norecord"]?></td></tr>
				<? else: ?>
				    <? foreach ($loan_data['penalty_history'] as $num=>$book): ?>
						<tr>
                        	<td><?=$num+1?></td>
                        	<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="record_penalty_history" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
						    <td><?=$book['due_date']?></td>
						    <td><?=$book['return_date']?></td>
						    <td><?=$book['penalty_reason']=='LOST' ? $eLib_plus["html"]["booklost"] : $eLib_plus["html"]["overdue"].' '.$book['penalty_reason'].' '.$eLib_plus["html"]["days"]?></td>
						    <td>$<?=$book['penalty_total']?></td>
						</tr>
					<? endforeach;?>
				<? endif;?>
					</tbody>
				</table>
			</div>  					                   
		</div>                     
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordPenaltyUI


   	function getMyRecordLostBookUI($loan_data){
		global $eLib_plus, $eLib, $Lang;
		
  		ob_start();
?>
        <!--SIDE TAB 1 tab 4-->
        <div class="tab-pane active" id="loan-lost">
            <div class="table-responsive">
				<table class="table-list">
					<thead>
                        <tr valign="bottom">
							<th>#</th>
							<th><?=$eLib["html"]["book_title"]?></th>
							<th><?=$Lang["libms"]["book"]["lost_date"]?></th>
                        </tr>
					</thead>
					<tbody>
				<? if (!$loan_data['lost_books']): ?>
					    <tr><td colspan="3"><?=$eLib_plus["html"]["norecord"]?></td></tr>
				<? else: ?>
				    <? foreach ($loan_data['lost_books'] as $num=>$book): ?>
						<tr>
                        	<td><?=$num+1?></td>
                        	<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="record_lost_books" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
						    <td><?=$book['LostDate']?></td>
						</tr>
					<? endforeach;?>
				<? endif;?>
					</tbody>
				</table>
			</div>  					                   
		</div>                     
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordLostBookUI



 	function getMyRecordSideTabLoanBookRecordUI($loan_data){
		global $eLib_plus;
		
  		ob_start();
?>
      <!--SIDE TAB 1-->
      <div class="tab-pane active" id="tab-loan">
      	<div class="loan-tabs tabs general-tabs">
    		<ul class="nav nav-tabs">
        		<li class="active"><a href="#loan-current" data-toggle="tab"><?=$eLib_plus["html"]["currentstatus"]?></a></li>
        		<li class="teacher"><a href="#loan-record" data-toggle="tab"><?=$eLib_plus["html"]["loanrecord"]?></a></li>
        		<li><a href="#loan-penalty" data-toggle="tab"><?=$eLib_plus["html"]["penaltyrecord"]?></a></li>
        		<li><a href="#loan-lost" data-toggle="tab"><?=$eLib_plus["html"]["lostrecord"]?></a></li>
        	</ul>
    		<div class="tab-content">
    			<!-- content by top tab -->
    			 <?php echo $this->getMyRecordCurrentStatusUI($loan_data);?>
			</div>
        </div>
      </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordSideTabLoanBookRecordUI
 

   	function getMyRecordMyReadingRecordContent($ebook_data, $start_record_index=0){
		global $eLib, $eLib_plus;
		
  		ob_start();
?>
		<? if (!$ebook_data['reading'] && $start_record_index==0): ?>
	    		<tr><td colspan="6"><?=$eLib_plus["html"]["norecord"]?></td></tr>
		<? else: ?>
			<? foreach ($ebook_data['reading'] as $i=>$progress) :
				$j = $start_record_index + $i + 1;
				$row_class = ($progress['readable']) ? '' : ' class="off"';
			?>
	        	<tr>
	            	<td><?=$j?></td>
	            	<td>
				<? if (!empty($progress['url']) && ($progress['BookFormat'] != 'physical') && ($progress['readable'])) :?>
				  <a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon-ebooks inline" href="javascript:<?=$progress['url']?>" ></a></td>
					<td<?=$row_class?>>
	            		<a class="fancybox book-detail-fancybox fancybox.iframe title inline" href="book_details.php?book_id=<?=$progress['id']?>" data-fancybox-group="my_reading_record" title="<?=$progress['title'].' - '.$progress['author']?>"><?=$progress['title']?></a></td>
				<? elseif(!$progress['readable']): ?>
				  <a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon-ebooks-off inline"></a></td>
					<td<?=$row_class?>>
	            		<a class="fancybox book-detail-fancybox fancybox.iframe title-off inline" href="book_details.php?book_id=<?=$progress['id']?>" data-fancybox-group="my_reading_record" title="<?=$progress['title'].' - '.$progress['author']?>"><?=$progress['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a></td>
				<? endif; ?>
					<td<?=$row_class?>><?=($progress['author']?$progress['author']:'--')?></td>			    
				    <td<?=$row_class?>>
				<? if (!empty($progress['percentage'])) :?>
						<div class="book_progress"><span <?=($progress['percentage']==100)? "class='done'" : 'style="left:'.($progress['percentage']-100).'px;"'?>></span><em><?=$progress['percentage']?>%</em></div>
				<? else: ?>
						--
				<? endif; ?>
				    </td>
				    <td<?=$row_class?>><?=$progress['last_accessed']?></td>
	            </tr>
			<?  endforeach;?>            
		<? endif;?>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordMyReadingRecordContent


   	function getMyRecordMyReadingRecordUI($ebook_data,$nav_para){
		global $eLib;
		
  		ob_start();
?>
        <!--SIDE TAB 2 tab 1-->
		<div class="tab-pane active" id="ebook-record">
       	  	<div class="table-responsive">
				<table class="table-list">
					<thead>
                        <tr valign="bottom">
							<th>#</th>
							<th>&nbsp;</th>
						    <th><?=$eLib["html"]["book_title"]?></th>
						    <th><?=$eLib["html"]["author"]?></th>
						    <th><?=$eLib["html"]["Progress"]?></th>
						    <th><?=$eLib["html"]["last_read"]?></th>
                        </tr>
					</thead>
					<tbody>
						<?=$this->getMyRecordMyReadingRecordContent($ebook_data, 0)?>
					</tbody>
				</table>
       	  	</div>
        </div>
        
    	<!-- footer: navigation bar -->
    	<?=$this->getSearchResultListFooter($nav_para)?>
                            
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordMyReadingRecordUI


   	function getMyRecordMyNotesContent($ebook_data, $start_record_index=0){
		global $eLib, $eLib_plus;
		
  		ob_start();
?>
		<? if (!$ebook_data['notes'] && $start_record_index==0): ?>
	    		<tr><td colspan="5"><?=$eLib_plus["html"]["norecord"]?></td></tr>
		<? else: ?>
			<? foreach ($ebook_data['notes'] as $i=>$note) :
				$j = $start_record_index + $i + 1;
				$row_class = ($note['readable']) ? '' : ' class="off"';
			?>
	        	<tr>
	            	<td><?=$j?></td>
				<? if ($note['readable']) :?>
					<td><a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon-ebooks inline" href="javascript:<?=$note['url']?>" ></a></td>
					<td<?=$row_class?>><a class="fancybox book-detail-fancybox fancybox.iframe title inline" href="book_details.php?book_id=<?=$note['id']?>" data-fancybox-group="my_notes_content" title="<?=$note['title'].' - '.$note['author']?>"><?=$note['title']?></a></td>
					<td<?=$row_class?>><?=$note['note_url']?></td>
				<? else : 
					$noteContent = explode('>',$note['content']);
				?>
				  	<td><a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon-ebooks-off inline"></a></td>
				  	<td<?=$row_class?>><a class="fancybox book-detail-fancybox fancybox.iframe title-off inline" href="book_details.php?book_id=<?=$note['id']?>" data-fancybox-group="my_notes_content" title="<?=$note['title'].' - '.$note['author']?>"><?=$note['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a></td>
				  	<td<?=$row_class?>><?=$noteContent[1]?></td>
				<? endif; ?>
				    <td<?=$row_class?>><?=$note['last_modified']?></td>
	            </tr>
			<?  endforeach;?>            
		<? endif;?>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordMyNotesContent


   	function getMyRecordMyNotesUI($ebook_data, $nav_para){
		global $eLib;
		
  		ob_start();
?>
        <!--SIDE TAB 2 tab 2-->
        <div class="tab-pane active" id="ebook-notes">
            <div class="table-responsive">
				<table class="table-list">
					<thead>
                        <tr valign="bottom">
							<th>#</th>
							<th>&nbsp;</th>
							<th><?=$eLib["html"]["book_title"]?></th>
							<th><?=$eLib["html"]["notes_content"]?></th>
							<th><?=$eLib["html"]["last_modified"] ?></th>
                        </tr>
					</thead>
					<tbody>
						<?=$this->getMyRecordMyNotesContent($ebook_data, 0)?>
					</tbody>
				</table>
			</div>  					                   
		</div>
		
    	<!-- footer: navigation bar -->
    	<?=$this->getSearchResultListFooter($nav_para)?>
    	
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordMyNotesUI


   	function getMyRecordSideTabeBookRecordUI($ebook_data, $nav_para){
		global $eLib;
		
  		ob_start();
?>
      <!--SIDE TAB 2-->
      <div class="tab-pane active" id="tab-ebook">
      	<div class="loan-tabs tabs general-tabs">
    		<ul class="nav nav-tabs">
        		<li class="active"><a href="#ebook-record" data-toggle="tab"><?=$eLib["html"]["my_reading_history"] ?></a></li>
        		<li class="teacher"><a href="#ebook-notes" data-toggle="tab"><?=$eLib["html"]["my_notes"]?></a></li>
        	</ul>
    		<div class="tab-content">
    			<!-- content by top tab -->
    			 <?=$this->getMyRecordMyReadingRecordUI($ebook_data, $nav_para)?>
			</div>
        </div>
      </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordSideTabeBookRecordUI


   	function getMyRecordMyFavouriteCoverView($books_data){
		global $eLib, $eLib_plus;
		$number_of_books = count($books_data);
		
		$libelibplus = new libelibplus();
		if($number_of_books>1) {
			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
		}
		else {
			$navigation_id = '';
		}
		
		$showRead = $_SESSION['UserID'] ? true : false;
		
  		ob_start();
?>
        <ul class="fav-list book-covers">
<?        
	if ($number_of_books):
		$j = 0;
		foreach((array)$books_data as $num=>$book):
			if ($book['image']) {
				$image_no = '';
			}
			else {
				$image_no = $j%10+1;
				$j++;
			}
			$book['image_no'] = $image_no;
			$book['navigation_id'] = $navigation_id;

			echo ($book['book_type'] == 'ebook') ? $this->geteBookCover($book, true, "li", "", $showRead) : $this->getpBookCover($book, true, "li");
			
		endforeach;
	else: 
?>
			<li>
				<?=$this->getNoRecordFoundBookCover('ebook')?>
			</li>
<? 	endif;?>
        </ul>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordMyFavouriteCoverView


   	function getMyRecordMyFavouriteListView($books_data){
		global $eLib,$eLib_plus;
		$number_of_books = count($books_data);
		
		$libelibplus = new libelibplus();
		if($number_of_books>1) {
			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
		}
		else {
			$navigation_id = '';
		}
		
  		ob_start();
?>
    <div class="table-responsive">
      	<table class="table-list">
      		<thead>
        		<tr valign="bottom">
            		<th>#</th>
	            	<th>&nbsp;</th>
				    <th><?=$eLib["html"]["book_title"]?></th>
				    <th><?=$eLib["html"]["author"]?></th>
				    <th><?=$eLib["html"]["publisher"]?></th>
	            	<th>&nbsp;</th>
	            </tr>
	        </thead>
      		<tbody>

    <? if (!$books_data): ?>
			<tr><td rowspan="6"><?=$eLib_plus["html"]["norecord"]?></td></tr>
    <? else: ?>
		<? foreach((array)$books_data as $num=>$book):?>
			<tr>
				<td><?=$num+1?></td>
				<td>
			<? if (!empty($book['url']) && !($book['type'] == 'expired')) :?>
			  		<a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon-ebooks" href="javascript:<?=$book['url']?>" ></a>
			<? elseif(($book['type'] == 'expired')): ?>
			  		<a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon-ebooks-off"></a>
			<? endif; ?>
	        	</td>
			<? if (!($book['type'] == 'expired')) : ?>
				<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
				<td><?=($book['author']?$book['author']:'--')?></td>
			    <td><?=($book['publisher']?$book['publisher']:'--')?></td>
			<? else: ?>
				<td class="off"><a class="fancybox book-detail-fancybox fancybox.iframe title-off" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a></td>
				<td class="off"><?=($book['author']?$book['author']:'--')?></td>
			    <td class="off"><?=($book['publisher']?$book['publisher']:'--')?></td>
			<? endif; ?>
				<td><a href="#" class="btn-remove" data-book-id="<?=$book['id']?>"><span class="glyphicon glyphicon-trash"></span></a></td>
	        </tr>
		<? endforeach;?>
	<? endif;?>
        	</tbody>
      	</table>
    </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordMyFavouriteListView


   	function getMyRecordSideTabMyFavouriteUI($books_data){
		global $eLib,$eLib_plus;
		$number_of_books = count($books_data);
  		ob_start();
?>
      <!--SIDE TAB 3-->
      <div class="tab-pane active" id="tab-fav">
      	<div class="table-header">
    		<span id="fav-total"><?=$number_of_books.' '.$eLib["html"]["books"]?></span>
    		<div class="btn-views">
	            <span class="btn-table-view off glyphicon glyphicon-th-large"></span> 
	            <a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>
        	</div>  
    	</div>
      	<div class="table-body bookshelf">
        	<!-- Favourite Book cover / list view -->
        	<?=$this->getMyRecordMyFavouriteCoverView($books_data)?>
        </div>
      </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordSideTabMyFavouriteUI


   	function getMyRecordSideTabMyReviewUI($reviews_data, $total, $start_record_index){
		global $eLib;
		
  		ob_start();
?>
      <!--TAB 4-->
      <div class="tab-pane active" id="tab-review">
      	<div class="table-header"><?=$eLib["html"]['Total'] .': '.$total.' '.$eLib["html"]["reviewsUnit"].$eLib["html"]["reviews"]?>
        </div>
        <div class="table-body">
          	<!-- review content -->
          	<?=$this->getReviewByUserUI($reviews_data, $total, $start_record_index, false)?>
        </div>
      </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyRecordSideTabMyReviewUI


	###############################################################################################
	## start opac functions
	
 	function getOpacPortalTopPart() {
 		global $PATH_WRT_ROOT, $intranet_session_language, $junior_mck, $LAYOUT_SKIN;
 		
 		// get school name from eLib+ settings
 		$libms = new liblms();
 		if ($junior_mck) {
 		    mysql_query("set names latin1");
 		}
 		$schoolChineseName = $libms->get_system_setting('school_display_name');
 		$schoolEnglishName = $libms->get_system_setting('school_display_name_eng');
 		$schoolName = Get_Lang_Selection($schoolChineseName,$schoolEnglishName);
 		if (!get_magic_quotes_gpc()) {
 		    $schoolName = stripslashes($schoolName);
 		}
 		
 		// if not exist, get it from system
 		if (empty($schoolName)) {
 		    $schoolName = $junior_mck ? convert2unicode(GET_SCHOOL_NAME(),true) : GET_SCHOOL_NAME();
 		}
 		
 		ob_start();
?> 		
<!--TOP starts-->
<div class="elib-top container-fluid">
  <div class="navbar">
  	<!--hamburger button-->
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span> <span class="icon-bar"></span><span class="icon-bar"></span></button>
    <div class="container">
      <div class="navbar-header"><a class="header-logo navbar-brand" href="/home/eLearning/elibplus2/opac_index.php"><img height="60" alt="eLibrary plus" src="/images/<?=$LAYOUT_SKIN?>/eLibplus2/<?=($intranet_session_language=="en"?'logo-en-opac.png':'logo-chi-opac.png')?>"><?php echo $schoolName;?></a></div>
    </div>
    <!--MOBILE COLLAPSE content starts-->
    <div class="collapse navbar-collapse" id="navbar-ex-collapse">
    <?=$this->getOpacHeaderItemsUI()?>
    <?=$this->getOpacTopMenuBar()?> 
    </div>
    <!--MOBILE COLLAPSE content ends-->     
  </div>
</div>
<!--TOP ends-->
<?php  
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getOpacPortalTopPart()
	
 	function getOpacHeaderItemsUI() {
 		global $Lang, $eLib_plus, $intranet_session_language, $PATH_WRT_ROOT, $sys_custom;

		$libelibplus = new libelibplus();
		$keyword_search = $libelibplus->get_portal_setting('keyword_search');

		# Basic Information
		$intranet_default_lang = $_SESSION['intranet_default_lang'] ? $_SESSION['intranet_default_lang'] : $intranet_default_lang;
		
		if ($intranet_default_lang == "gb" || get_client_region()=="zh_MY" || get_client_region()=="zh_CN")
			($intranet_session_language=="en") ? $lang_to_change = "gb" : $lang_to_change = "en";
		else
			($intranet_session_language=="en") ? $lang_to_change = "b5" : $lang_to_change = "en";
		$change_lang_to = $intranet_session_language=="en" ? $Lang['Header']['B5'] : $Lang['Header']['ENG'];
 		
 		$search_type = $_POST['search_type'];
 		$keyword = $_POST['keyword'];
 		if ($search_type == 'search') {
 			$keyword = str_replace("ltltgtgt","<>",$keyword);
 		}
 		$keyword = trim($keyword);
		if (!get_magic_quotes_gpc()) {
			$keyword 	= stripslashes($keyword);
		}
		 		 		
 		ob_start();
?>
      <!--HEADER ITEMS starts-->
      <div class="container header-right-container">
        <div class="header-right">
		  <div class="header-user"><a href="/templates/"><?=$Lang["libms"]["opac"]["member_login"]?><span class="caret-right"></span></a></div>
          <div class="lang-switcher">
          	<a href="/lang.php?lang=<?= $lang_to_change?>"><?=$change_lang_to?></a>
          </div>
          
        <? if ($keyword_search):?>
          <div class="header-search">
            <div class="header-search-form form-group has-feedback">
              <form name="search_form" id="search_form" class="search" method="post" action="simple_search.php"> 
              	<input type="text" class="form-control input-sm" name="keyword" id="keyword" value="<?=$search_type=='simple_search'?intranet_htmlspecialchars($keyword):''?>" placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>">
              	<input type="hidden" name="search_type" value="simple_search" />
              	<input type="hidden" name="form_method" value="post">
              	<input type="hidden" name="update_navigation" value="1" />
              </form>
              <i class="glyphicon glyphicon-search form-control-feedback"></i>
            </div>
          </div>
        <? endif;?>
        </div>
      </div>
      <!--HEADER ITEMS ends-->
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	} 	// end getOpacHeaderItemsUI()

 	function getOpacTopMenuBar() {
 		global $eLib_plus, $Lang, $PATH_WRT_ROOT;
 		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
 		
		$libelibplus = new libelibplus();
		$book_category_show_all = $libelibplus->get_portal_setting('book_category_show_all');
		$book_category_chi = $libelibplus->get_portal_setting('book_category_chi_ebooks');
		$book_category_eng = $libelibplus->get_portal_setting('book_category_eng_ebooks');
		$book_category_others = $libelibplus->get_portal_setting('book_category_physical_books');
		$book_category_tags = $libelibplus->get_portal_setting('book_category_tags');
 		
		$ebook_most_hit = $libelibplus->get_portal_setting('ebook_most_hit');
		$pbook_most_loan = $libelibplus->get_portal_setting('pbook_most_loan');
		$ebook_best_rated = $libelibplus->get_portal_setting('ebook_best_rated');
		$pbook_best_rated = $libelibplus->get_portal_setting('pbook_best_rated');
		$ebook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_active_reviewers');
		$pbook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_active_reviewers');
		$ebook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_helpful_reviewers');
		$pbook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_helpful_reviewers');
 		
 		ob_start();
?>
      <!--MAIN MENU starts-->
      <div class="elib-menu container-fluid">
        <div class="container">
          <ul class="elib-main-menu nav nav-pills navbar-left">
          <? if ($book_category_show_all || $book_category_chi || $book_category_eng || $book_category_others || $book_category_tags): ?>
            <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?=$eLib_plus["html"]["bookcategory"]?><span class="caret"></a>
              <ul class="dropdown-menu megamenu browse-cat">
               	<? if ($book_category_show_all):?>
                	<li class="list-all"><a href="<?=('/home'.$elibplus_cfg['eLearning_path'])?>/elibplus2/advanced_search.php?search_type=category" class=""><?=$eLib_plus["html"]["listallbooks"]?> <span class="caret-right"></span></a></li>
                <? else:?>
                	<li class="list-all"><span class="caret-right"></span></a></li>
                <? endif;?>
                <form>
                  <div class="tabs general-tabs">
            		<?=$this->getBookCatalogUI()?>
                  </div>
                </form>
              </ul>
            </li>
		  <? endif;?>            
          <? if ($ebook_most_hit || $pbook_most_loan || $ebook_best_rated || $pbook_best_rated || $ebook_ranking_most_active_reviewers || $pbook_ranking_most_active_reviewers || $ebook_ranking_most_helpful_reviewers || $pbook_ranking_most_helpful_reviewers):?>  
            <!--<li> <a href="/home/eLearning/elibplus2/ranking.php"><?=$eLib_plus["html"]["ranking"]?></a> </li>-->
          <? endif;?>  
          </ul>
        </div>
      </div>      
      <!--MAIN MENU ends-->
<?php
		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	} 	// end getOpacTopMenuBar()
 	
 	function getOpacRankingLeftMenu($ranking_type='best_rated'){
		global $Lang, $eLib_plus;

		$libelibplus = new libelibplus();
		$ebook_most_hit = $libelibplus->get_portal_setting('ebook_most_hit');
		$ebook_best_rated = $libelibplus->get_portal_setting('ebook_best_rated');
		$pbook_most_loan = $libelibplus->get_portal_setting('pbook_most_loan');
		$pbook_best_rated = $libelibplus->get_portal_setting('pbook_best_rated');
		$pbook_ranking_most_active_borrowers = $libelibplus->get_portal_setting('pbook_ranking_most_active_borrowers');
		$ebook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_active_reviewers');
		$pbook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_active_reviewers');
		$ebook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_helpful_reviewers');
		$pbook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_helpful_reviewers');
		
		ob_start();
?>
    <ul class="side-tabs nav nav-pills nav-stacked col-md-3">
    <? if ($ebook_best_rated || $pbook_best_rated || $ebook_most_hit || $pbook_most_loan):?>
      <div class="icon1<?=(($ranking_type == 'best_rated' || $ranking_type == 'most_hit' || $ranking_type == 'most_loan') ? ' on' : '')?>"></div>
	<? endif;?>      
    <? if ($ebook_best_rated || $pbook_best_rated):?>
      <li class="type1<?=$ranking_type == 'best_rated' ? ' active' : ''?>"><a href="#tab-best-rated" id="best_rated" data-toggle="pill"><?=$Lang["libms"]["portal"]["best_rated"]?></a></li>
	<? endif;?>
    <? if ($ebook_most_hit):?>	      
      <li class="type1<?=$ranking_type == 'most_hit' ? ' active' : ''?>"><a href="#tab-most-hit" id="most_hit" data-toggle="pill"><?=$Lang["libms"]["portal"]["most_hit"]?></a></li>
    <? endif;?>
    <? if ($pbook_most_loan):?>  
      <li class="type1<?=$ranking_type == 'most_loan' ? ' active' : ''?>"><a href="#tab-most-loan" id="most_loan" data-toggle="pill"><?=$Lang["libms"]["portal"]["most_loan"]?></a></li>
	<? endif;?>
	<? if ($pbook_ranking_most_active_borrowers || $ebook_ranking_most_active_reviewers || $pbook_ranking_most_active_reviewers):?>
	  <div class="icon2<?=(($ranking_type == 'most_active_borrowers' || $ranking_type == 'most_active_reviewers') ? ' on' : '')?>"></div>
	<? endif;?>  
    <? if ($pbook_ranking_most_active_borrowers):?>  
      <li class="type2<?=$ranking_type == 'most_active_borrowers' ? ' active' : ''?>"><a href="#tab-most-active-borrowers" id="most_active_borrowers" data-toggle="pill"><?=$eLib_plus["html"]["mostactiveborrowers"]?></a></li>
    <? endif;?>  
    <? if ($ebook_ranking_most_active_reviewers || $pbook_ranking_most_active_reviewers):?>  
      <li class="type2<?=$ranking_type == 'most_active_reviewers' ? ' active' : ''?>"><a href="#tab-most-active-reviewers" id="most_active_reviewers" data-toggle="pill"><?=$eLib_plus["html"]["mostactivereviewers"]?></a></li>
    <? endif;?>
    <? if ($ebook_ranking_most_helpful_reviewers || $pbook_ranking_most_helpful_reviewers):?>
      <div class="icon3<?=(($ranking_type == 'most_helpful_review') ? ' on' : '')?>"></div>
    <? endif;?>  
    <? if ($ebook_ranking_most_helpful_reviewers || $pbook_ranking_most_helpful_reviewers):?>  
      <li class="type3<?=$ranking_type == 'most_helpful_review' ? ' active' : ''?>"><a href="#tab-most-helpful-review" id="most_helpful_review" data-toggle="pill"><?=$eLib_plus["html"]["mosthelpfulreview"]?></a></li>
    <? endif;?>  
    </ul>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
 	}	// end getOpacRankingLeftMenu
 	
  	/*
 	 * 	@param	$ranking_type:	best_rated, most_hit, most_loan, most_borrower, most_reviewer, most_helpful_review
 	 * 			$range: accumulated, thisweek
 	 * 			$layout: cover, list
 	 * 			$book_type: ""(all), ebook, physical
 	 */
 	function getOpacRankingHeader($ranking_type="best_rated",$range="accumulated",$layout="cover", $book_type=""){
		global $Lang, $eLib_plus;
		
		$libelibplus = new libelibplus();
		$ebook_best_rated = $libelibplus->get_portal_setting('ebook_best_rated');
		$pbook_best_rated = $libelibplus->get_portal_setting('pbook_best_rated');
		$ebook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_active_reviewers');
		$pbook_ranking_most_active_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_active_reviewers');
		$ebook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('ebook_ranking_most_helpful_reviewers');
		$pbook_ranking_most_helpful_reviewers = $libelibplus->get_portal_setting('pbook_ranking_most_helpful_reviewers');
		
		switch($ranking_type) {
			case "best_rated":
					$title = $Lang["libms"]["portal"]["best_rated"];
				break;
			case "most_hit":
					$title = $Lang["libms"]["portal"]["most_hit"];
				break;
			case "most_loan":
					$title = $Lang["libms"]["portal"]["most_loan"];
				break;
			case "most_active_borrowers":
					$title = $eLib_plus["html"]["mostactiveborrowers"];
				break;
			case "most_active_reviewers":
					$title = $eLib_plus["html"]["mostactivereviewers"];
				break;
			case "most_helpful_review":
					$title = $eLib_plus["html"]["mosthelpfulreview"];
				break;
		}
		ob_start();
?>
        <div class="table-header"> <span id="header-title"><?=$title?></span>
          <div class="btn-views">
<? if (($ranking_type == 'best_rated') || ($ranking_type == 'most_active_reviewers') || ($ranking_type == 'most_helpful_review')):?>           
            <select class="form-control" id="book_type">
		<? if (($ranking_type == 'best_rated' && $ebook_best_rated && $pbook_best_rated) || ($ranking_type == 'most_active_reviewers' && $ebook_ranking_most_active_reviewers && $pbook_ranking_most_active_reviewers) || ($ranking_type == 'most_helpful_review' && $ebook_ranking_most_helpful_reviewers && $pbook_ranking_most_helpful_reviewers)):?>            
              <option value="" <?=($book_type=='')?'selected':''?>><?=$Lang['libms']['bookmanagement']['allBooks']?></option>
        <? endif;?>      
		<? if (($ranking_type == 'best_rated' && $ebook_best_rated) || ($ranking_type == 'most_active_reviewers' && $ebook_ranking_most_active_reviewers) || ($ranking_type == 'most_helpful_review' && $ebook_ranking_most_helpful_reviewers)):?>		              
              <option value="ebook" <?=($book_type=='ebook')?'selected':''?>><?=$Lang["libms"]["portal"]["eBooks"]?></option>
		<? endif;?>
		<? if (($ranking_type == 'best_rated' && $pbook_best_rated) || ($ranking_type == 'most_active_reviewers' && $pbook_ranking_most_active_reviewers) || ($ranking_type == 'most_helpful_review' && $pbook_ranking_most_helpful_reviewers)):?>              
              <option value="physical" <?=($book_type=='physical')?'selected':''?>><?=$Lang["libms"]["portal"]["pBooks"]?></option>
    	<? endif;?>          
            </select>
<? endif;?>          
            <select class="form-control" id="range">
              <option value="thisweek" <?=($range=='thisweek')?'selected':''?>><?=$eLib_plus["html"]["thisweek"]?></option>
<?php
    if ($ranking_type == 'most_hit') {
        $selected = $range=='thismonth' ? ' selected' : '';
        $style = ' style="display:"';
    }
    else {
        $selected = '';
        $style = ' style="display:none;"';
    }
    echo '<option id="thismonth" value="thismonth"'.$selected.$style.'>'.$eLib_plus["html"]["thismonth"].'</option>';
?>
              <option value="accumulated" <?=($range=='accumulated')?'selected':''?>><?=$eLib_plus["html"]["accumulated"]?></option>
            </select>
<? if ($layout == "cover"):?>            
            <span class="btn-table-view off glyphicon glyphicon-th-large"></span> 
            <a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>
<? elseif ($layout == "list"):?>
            <a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a> 
            <span class="btn-list-view off glyphicon glyphicon-th-list"></span>
<? endif;?>             
          </div>
        </div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;		
 	}	// end getOpacRankingHeader
 	
	## end opac functions
	###############################################################################################



	###############################################################################################
	## start myfriend functions

	// eBook
   	function getMyFriendNotificationeBookCover($book){
		global $Lang, $eLib_plus;
		
  		ob_start();
?>
    <div class="share-book">
    	<div class="book-covers">
			
<?  		
	if ($book['type']=='expired') {
?>
			<div class="off-shelf book-box">
				<a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$book['navigation_id']?>" href="book_details.php?book_id=<?=$book['id']?>" title="<?=$book['title'].' - '.$book['author']?>">
                  	<div class="icon-ebooks"></div>
                  	<div class="title ellipsis multiline"><?=$book['title']?></div>
                  	<div class="off-shelf-tag"><?=$Lang["libms"]["portal"]["off_shelf"]?></div>
                  	<div class="author"><?=$book['author']?></div>
				</a>					
			</div>
<?
	}
	else {
?>
			<div class="icon-ebooks"></div>
			<a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$book['navigation_id']?>" href="book_details.php?book_id=<?=$book['id']?>" title="<?=$book['title'].' - '.$book['author']?>">
<?		
		if (!$book['image']) :?>
	          	<div class="generic type10">
					<div class="title ellipsis multiline"><?=$book['title']?></div>
					<div class="author"><?=$book['author']?></div>
	            </div>
	<?  else : ?>
		        <img src="<?=$book['image']?>">
	<? 	endif; ?>
			</a>     
<?	} ?>		
		</div>
        <div class="text">
	        <a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$book['navigation_id']?>" href="book_details.php?book_id=<?=$book['id']?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a>
        	<div class="message"><?=$book['Comments']?></div>
        </div>
	</div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendNotificationeBookCover


	// physical book 
   	function getMyFriendNotificationpBookCover($book){
		global $Lang, $eLib_plus;
		
  		ob_start();
?>
    <div class="share-book">
    	<div class="book-covers">
	    	<a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$book['navigation_id']?>" href="book_details.php?book_id=<?=$book['id']?>&cover_type_id=<?=$book['image_no']?>" title="<?=$book['title'].' - '.$book['author']?>">
<?php if ($book['image']) :?>
    	       	<img src="<?=$book['image']?>">
<?php else: ?>
	          	<div class="generic type<?=$book['image_no']?>">
					<div class="title ellipsis multiline"><?=$book['title']?></div>
					<div class="author"><?=$book['author']?></div>
	            </div>
<?php endif; ?>     
	    	</a>
		</div>
        <div class="text">
	        <a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$book['navigation_id']?>" href="book_details.php?book_id=<?=$book['id']?>&cover_type_id=<?=$book['image_no']?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a>
        	<div class="message"><?=$book['Comments']?></div>
        </div>
    </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendNotificationpBookCover


   	function getMyFriendNotificationShareBookUI($data){
		global $Lang, $eLib_plus, $intranet_session_language;
		
		$date = substr($data['IssuedOn'],0,10);
		$time = substr($data['IssuedOn'],11,5);
		$displayTime = ($intranet_session_language=="en") ? ($date . ' '. $Lang["libms"]["portal"]["myfriends"]["at"] .' ' . $time) : ($Lang["libms"]["portal"]["myfriends"]["at"] . ' '. $date . ' ' .$time); 

		if ($data['Status'] == '1') {
			$displayNew = '<div class="new">'.$Lang["libms"]["portal"]["myfriends"]["new"].'</div>';
			$data_status = '1';
		}
		else {
			$displayNew = '';
			$data_status = '';
		} 
		
  		ob_start();
?>
    	<li>
        	<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$data['SenderID']?>"><img src="<?=$data['user_image']?>"></a></div>
            <div class="notify-detail">
            	<div class="text"><div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$data['SenderID']?>"><?=$data['name'] ? $data['name'] : $eLib_plus["html"]["anonymous"]?></a></div> <?=$Lang["libms"]["portal"]["myfriends"]["share_a_book"]?><div class="date"><?=$displayTime?></div><?=$displayNew?></div>
<?
	if ($data['book_type'] == 'ebook') {
		echo $this->getMyFriendNotificationeBookCover($data);	
	}
	else {
		$cover_type_id = rand(1,10);
		if ($data['image']) {
			$image_no = '';
		}
		else {
			$image_no = $cover_type_id;
		}
		$data['image_no'] = $image_no;
		echo $this->getMyFriendNotificationpBookCover($data);												      	
	}
?>            	
            </div>
            <a href="#" data-notification_id="<?=$data['NotificationID']?>" data-status="<?=$data_status?>" class="btn-remove remove_notification"><span class="glyphicon glyphicon-trash"></span></a>
        </li>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendNotificationShareBookUI


   	function getMyFriendNotificationFriendAcceptUI($data){
		global $Lang, $eLib_plus, $intranet_session_language;
		
		$date = substr($data['IssuedOn'],0,10);
		$time = substr($data['IssuedOn'],11,5);
		$displayTime = ($intranet_session_language=="en") ? ($date . ' '. $Lang["libms"]["portal"]["myfriends"]["at"] .' ' . $time) : ($Lang["libms"]["portal"]["myfriends"]["at"] . ' '. $date . ' ' .$time); 
		$displayNew = ($data['Status'] == '1') ? '<div class="new">'.$Lang["libms"]["portal"]["myfriends"]["new"].'</div>' : '';
		
  		ob_start();
?>
    	<li>
        	<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$data['SenderID']?>"><img src="<?=$data['user_image']?>"></a></div>
            <div class="notify-detail">
            	<div class="text"><div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$data['SenderID']?>"><?=$data['name'] ? $data['name'] : $eLib_plus["html"]["anonymous"]?></a></div> <?=$Lang["libms"]["portal"]["myfriends"]["accept_friend_request"]?><div class="date"><?=$displayTime?></div><?=$displayNew?></div>
            </div>
            <a href="#" data-notification_id="<?=$data['NotificationID']?>" class="btn-remove remove_notification"><span class="glyphicon glyphicon-trash"></span></a>
        </li>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendNotificationFriendAcceptUI


   	function getMyFriendNotificationFriendRequestUI($data){
		global $Lang, $eLib_plus, $intranet_session_language;
		
		$date = substr($data['IssuedOn'],0,10);
		$time = substr($data['IssuedOn'],11,5);
		$displayTime = ($intranet_session_language=="en") ? ($date . ' '. $Lang["libms"]["portal"]["myfriends"]["at"] .' ' . $time) : ($Lang["libms"]["portal"]["myfriends"]["at"] . ' '. $date . ' ' .$time); 
		$displayNew = ($data['Status'] == '1') ? '<div class="new">'.$Lang["libms"]["portal"]["myfriends"]["new"].'</div>' : '';
		
  		ob_start();
?>
    	<li>
        	<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$data['SenderID']?>"><img src="<?=$data['user_image']?>"></a></div>
            <div class="notify-detail">
            	<div class="text"><div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$data['SenderID']?>"><?=$data['name'] ? $data['name'].$data['class'] : $eLib_plus["html"]["anonymous"]?></a></div> <?=$Lang["libms"]["portal"]["myfriends"]["send_friend_request"]?><div class="date"><?=$displayTime?></div><?=$displayNew?></div>
            	<a href="#" data-notification_id="<?=$data['NotificationID']?>" class="btn-type1 accept_request"><?=$Lang["libms"]["portal"]["myfriends"]["accept"]?></a>
            	<a href="#" data-notification_id="<?=$data['NotificationID']?>" class="btn-type1 red ignore_request"><?=$Lang["libms"]["portal"]["myfriends"]["ignore"]?></a>
            </div>
        </li>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendNotificationFriendRequestUI


   	function getMyFriendNotificationUI($notification){
		global $Lang;
		
		$layout = '';
		
		if (count($notification)) {
			foreach((array)$notification as $k=>$v) {
				$event = $v['Event'];
				switch ($event) {
					case '0':	// friend request
						$layout .= $this->getMyFriendNotificationFriendRequestUI($v);
						break;
						
					case '1':	// friend accept
						$layout .= $this->getMyFriendNotificationFriendAcceptUI($v);
						break;
						
					case '3':	// share book
					
						$lelibplus = new elibrary_plus($v['BookID'], $_SESSION['UserID']);
						$book=$lelibplus->getBookDetail();
						$book['IssuedOn'] = $v['IssuedOn'];
						$book['Status'] = $v['Status'];
						$book['NotificationID'] = $v['NotificationID'];
						$book['SenderID'] = $v['SenderID'];
						$book['user_image'] = $v['user_image'];
						$book['name'] = $v['name'];
						$book['class'] = $v['class'];
						$book['Comments'] = $v['Comments'];
						
						$layout .= $this->getMyFriendNotificationShareBookUI($book);
						unset($lelibplus);
						break;
						
					default:	// skip the case
 		
						break;
				}
			}
		}
		else {
			$layout = '<li class="nothing">'.$Lang["libms"]["portal"]["myfriends"]["no_notifications"].'</li>';
		}
  		ob_start();
  		
?>
    	<div class="notifications">
        	<div class="table-header"><?=$Lang["libms"]["portal"]["myfriends"]["notifications"]?></div>
    		<div class="table-body bg-brown">
            	<ul class="notify-list">
                	<?=$layout?>
                </ul>
      		</div>
        </div>        
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendNotificationUI


	// display one activity
   	function getMyFriendActivityReview($data){
		global $Lang, $intranet_session_language;
		
		$date = substr($data['activity_date'],0,10);
		$time = substr($data['activity_date'],11,5);
		$displayTime = ($intranet_session_language=="en") ? ($date . ' '. $Lang["libms"]["portal"]["myfriends"]["at"] .' ' . $time) : ($Lang["libms"]["portal"]["myfriends"]["at"] . ' '. $date . ' ' .$time); 

		$lelibplus = new elibrary_plus($data['book_id'], $_SESSION['UserID']);
		$stat_data = $lelibplus->getBookStat();
		$data['is_like'] = $lelibplus->getHelpfulCheck($data['review_id']);
		
		ob_start();
?>
    	<li>
			<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$data['user_id']?>"><img src="<?=$data['user_image']?>"></a></div>
            <div class="feed-detail">
            	<div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$data['user_id']?>"><?=$data['name']?></a></div><?=$Lang["libms"]["portal"]["myfriends"]["review_a_book"]?><div class="date"><?=$displayTime?></div>
            	<div class="book-review feed-book">
                	<div class="book-covers">
				<?	if ($data['book_type'] == 'ebook') {
						echo $this->geteBookCover($data, true, "div", "", false);
					}   
					else {   
						$cover_type_id = rand(1,10);
						if ($data['image']) {
							$image_no = '';
						}
						else {
							$image_no = $cover_type_id;
						}
						$data['image_no'] = $image_no;												      	
						echo $this->getpBookCover($data, true, "div", "", false);
					}
				?>		
            		</div>
                    <div class="text">
						<a class="fancybox book-detail-fancybox fancybox.iframe" href="book_details.php?book_id=<?=$data['book_id']?>&cover_type_id=<?=$data['image_no']?>" title="<?=$data['title'].' - '.$data['author']?>"><?=$data['title']?></a>                    
                    	<div class="message">
                    		<?=$this->getStarRatingUI($stat_data['rating'])?>
                            <?=nl2br($data['content'])?>
                        </div>
                        <?=$this->getLikesUI($data)?>
                    </div>
                </div>
            </div>
        </li>
<?
		unset($lelibplus);
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendActivityReview


	// display one activity
   	function getMyFriendActivityBorrow($data){
		global $Lang, $intranet_session_language;
		
		$date = substr($data['activity_date'],0,10);
		$time = substr($data['activity_date'],11,5);
		$displayTime = ($intranet_session_language=="en") ? ($date . ' '. $Lang["libms"]["portal"]["myfriends"]["at"] .' ' . $time) : ($Lang["libms"]["portal"]["myfriends"]["at"] . ' '. $date . ' ' .$time); 

		$lelibplus = new elibrary_plus($data['book_id'], $_SESSION['UserID']);
		$stat_data = $lelibplus->getBookStat();

		
		ob_start();
?>
    	<li>
			<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$data['user_id']?>"><img src="<?=$data['user_image']?>"></a></div>
            <div class="feed-detail">
            	<div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$data['user_id']?>"><?=$data['name']?></a></div><?=$Lang["libms"]["portal"]["myfriends"]["borrow_a_book"]?><div class="date"><?=$displayTime?></div>
            	<div class="book-borrow feed-book">
                	<div class="book-covers">
				<?	if ($data['book_type'] == 'ebook') {
						echo $this->geteBookCover($data, true, "div", "", false);
					}   
					else {   
						$cover_type_id = rand(1,10);
						if ($data['image']) {
							$image_no = '';
						}
						else {
							$image_no = $cover_type_id;
						}
						$data['image_no'] = $image_no;												      	
						echo $this->getpBookCover($data, true, "div", "", false);
					}
				?>		
            		</div>
            	<?=$this->getBookItemsNextToCover($data,false)?>	
                </div>
            </div>
        </li>
<?
		unset($lelibplus);
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendActivityBorrow


   	function getMyFriendActivitiesUI($data){
		global $Lang;
		
		$x = '';
  		ob_start();
?>
    <div class="news-feed">
    	<div class="table-header"><?=$Lang["libms"]["portal"]["myfriends"]["friends_activities"]?></div>
		<div class="table-body">
        	<ul class="feed-list">
<? 	if (count($data)):
		foreach((array)$data as $da) {
			$activity = $da['activity'];
			switch($activity) {
				case 'review':
					$x .= $this->getMyFriendActivityReview($da);
					break;
					
				case 'borrow':
					$x .= $this->getMyFriendActivityBorrow($da);
					break;
			}
		}
		echo $x;

	else:
?>
				<li class="nothing"><?=$Lang["libms"]["portal"]["myfriends"]["no_friends_activities"]?></li>	
<?	endif;

 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendActivitiesUI


	function getStaffSelectionGroup() {
		global $Lang;
		
		$selection = '<optgroup label="'.$Lang["libms"]["portal"]["myfriends"]["identity"]["Staff"].'">';
			$selection .= '<option value="TeachingStaff">'.$Lang["libms"]["portal"]["myfriends"]["identity"]["TeachingStaff"].'</option>';
			$selection .= '<option value="NonTeachingStaff">'.$Lang["libms"]["portal"]["myfriends"]["identity"]["NonTeachingStaff"].'</option>';
		$selection .= '</optgroup>';
		return $selection;
	}
	
   	function getMyFriendAddFriendsUI(){
		global $Lang, $intranet_root, $junior_mck, $PATH_WRT_ROOT,$button_select;
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
		
		$libelibplus = new libelibplus();				
		$lelibplus = new elibrary_plus(0,$_SESSION['UserID']);
		$lclass = new libclass();
		$groupSelection = '';
		$userListUI = '';
		$addButton = '<a href="#" id="btn_add_friend" class="btn-type1">'.$Lang['Btn']['Add'].'</a>';
		
		if ($_SESSION['UserType'] == '1') {			// staff
			if ($junior_mck) {
				$lelibplus->dbConnectionLatin1();
				$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' class='form-control' ",$selected="",$optionSelect="", convert2Big5('-- '.$button_select.' --'));
				$classSelection = convert2Unicode($classSelection);
				$lelibplus->dbConnectionUTF8();
			}
			else {
				$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' class='form-control' ");
			}			
			$pos = strpos(strtolower($classSelection),"</option>");
			if ($pos !== false) {
				$groupSelection = substr($classSelection,0,$pos+9);
				$groupSelection .= $this->getStaffSelectionGroup();
				$groupSelection .= substr($classSelection,$pos+9);
			}
		}
		else if ($_SESSION['UserType'] == '2') {	// student
		
			if ($junior_mck) {
				$myClassName = $lelibplus->getClassByStudentID($_SESSION['UserID']);
			}
			else {
				$myClassName = $lclass->getClassByStudentID($_SESSION['UserID']);
				$myClassName = current($myClassName);
			}
		
			$scope = $libelibplus->get_portal_setting('my_friends_scope');
			$classList = $lelibplus->getMyClassLevelClassList($_SESSION['UserID'],$myClassName);
			if ($scope == 0) {			// self class
				$groupSelection  = '<select id="ClassName" name="ClassName" class="form-control">';
					$groupSelection .= '<option value=""> -- '.$Lang['Btn']['Select'].' -- </option>';
					$groupSelection .= $this->getStaffSelectionGroup();
					$groupSelection .= '<option value="'.$myClassName.'">'.$classList[0]['DispClassName'].'</option>';
				$groupSelection .= '</select>';				
			}
			else if ($scope == 1) {		// class level
				$classList = $lelibplus->getMyClassLevelClassList($_SESSION['UserID']);
				$classListUI = getSelectByArray($classList, 'id="ClassName" name="ClassName" class="form-control"', $myClassName);
				$pos = strpos(strtolower($classListUI),'</option>');
				if ($pos !== false) {
					$groupSelection = substr($classListUI,0,$pos+9);
					$groupSelection .= $this->getStaffSelectionGroup();
					$groupSelection .= substr($classListUI,$pos+9);
				}
			}
			else {	// all classes
				if ($junior_mck) {
					$lelibplus->dbConnectionLatin1();
					$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' class='form-control' ",$selected="",$optionSelect="", convert2Big5('-- '.$button_select.' --'));
					$classSelection = convert2Unicode($classSelection);
					$lelibplus->dbConnectionUTF8();
				}
				else {
					$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' class='form-control' ");
				}			
				$pos = strpos(strtolower($classSelection),'</option>');
				if ($pos !== false) {
					$groupSelection = substr($classSelection,0,$pos+9);
					$groupSelection .= $this->getStaffSelectionGroup();
					$groupSelection .= substr($classSelection,$pos+9);
				}
			}
		}		// not staff and not student
		else {
			$addButton = $Lang["libms"]["portal"]["myfriends"]["warning"]["add_friends"];
		}
		 
		$userListUI = $this->getAvailableStudentByClass('');
		
  		ob_start();
?>
    	<div class="add-friends">
        	<div class="table-header"><?=$Lang["libms"]["portal"]["myfriends"]["add_friends"]?></div>
    		<div class="table-body">
            	<div class="form-group has-feedback">
					<input type="text" class="form-control input-sm" name="AddFriendKeyword" id="AddFriendKeyword" placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>">
              		<i class="glyphicon glyphicon-search form-control-feedback"></i>
                </div>
                <?=$Lang['General']['Or']?>
    			<?=$groupSelection?>
    			<?=$userListUI?>
                <?=$addButton?>            	
            </div>
        </div>    
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendAddFriendsUI


	/*
	 * 	available student:  (1) not in notification list (exclude status=ignore)
	 * 						(2) not in friend list
	 * 						(3) exlcude myself
	 */ 
	function getAvailableStudentByClass($className) {
		global $intranet_root, $junior_mck;
		
		if ($className) {
			$lelibplus = new elibrary_plus(0,$_SESSION['UserID']);
			
			$excludeStudent = $lelibplus->getMyFriendExcludeUser();
			
			if (count($excludeStudent) == 0) {
				$excludeStudent = '';
			}
			if ($junior_mck) {
				$studentList = $lelibplus->getStudentNameListByClassName($className,$excludeStudent);
			}
			else {
				include_once("$intranet_root/includes/libclass.php");
				$libclass = new libclass();
				$studentList = $libclass->getStudentNameListWClassNumberByClassName($className,'0,1,2','',$excludeStudent);
			}
		}
		else {
			$studentList = array();
		}
		$studentListUI = getSelectByArray($studentList, 'id="StudentID" name="StudentID" class="form-control"');
		return $studentListUI;
				
	}	// end getAvailableStudentByClass
	
	
   	function getMyFriendList(){
		global $Lang, $lelibplus;
		
		if (!isset($lelibplus)) {
			$lelibplus = new elibrary_plus(0,$_SESSION['UserID']);
		}
		$myPendingFriends = $lelibplus->getPendingFriends();
		$myFriends = $lelibplus->getMyFriends();
		
  		ob_start();
?>
    	<div class="friend-list">
        	<div class="table-header"><?=$Lang["libms"]["portal"]["myfriends"]["friends_list"]?></div>
          	<div class="table-search">
             	<div class="form-group has-feedback">
					<input type="text" class="form-control input-sm" name="FriendKeyword" id="FriendKeyword" placeholder="<?=$Lang["libms"]["portal"]["type_here"]?>">
              		<i class="glyphicon glyphicon-search form-control-feedback"></i>
             	</div>
          	</div>
    		<div class="table-body">
            	<ul>
	<? 	if (count($myPendingFriends)):
			foreach((array)$myPendingFriends as $friend):
	?>
                	<li>
                    	<div class="user-photo"><img src="<?=$friend['image']?>"></div>
                        <div class="user-info">
                        	<div class="user-name"><?=$friend['name']?></div>
                        	<div class="class-no"><?=$friend['class']?></div>
                        	<div>(<?=$Lang["libms"]["portal"]["myfriends"]["status"]["wainting_for_accept"]?>)</div>
                        </div>
                        <div>
                        	<a href="#" data-notification_id="<?=$friend['NotificationID']?>" class="btn-remove btn-remove-request"><?=$Lang["libms"]["portal"]["myfriends"]["remove_request"]?> <span class="glyphicon glyphicon-trash"></span></a>
                        </div>
                    </li> 
	<?		endforeach;?>
    <? endif;?>
            	
	<? 	if (count($myFriends)):
			foreach((array)$myFriends as $friend):
	?>
                	<li>
                    	<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$friend['user_id']?>"><img src="<?=$friend['image']?>"></a></div>
                        <div class="user-info">
                        	<div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$friend['user_id']?>"><?=$friend['name']?></a></div>
                        	<div class="class-no"><a href="myfriends_profile.php?FriendID=<?=$friend['user_id']?>"><?=$friend['class']?></a></div>
                        </div>
                    </li> 
	<?		endforeach;?>
	<?	endif;?>
	<?	if (count($myPendingFriends) == 0 && count($myFriends) == 0):?>
					<li class="nothing"><?=$Lang["libms"]["portal"]["myfriends"]["no_friends"]?></li>	
	<?	endif;?>
                </ul>            	   	    	
            </div>
        </div>    
<?  		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendList
	
   	function getMyFriendPendingFriend($notificationID){
		global $Lang, $lelibplus;
		
		if (!isset($lelibplus)) {
			$lelibplus = new elibrary_plus(0,$_SESSION['UserID']);
		}
		$myPendingFriends = $lelibplus->getPendingFriends($notificationID);
		
  		ob_start();
?>
	<? 	if (count($myPendingFriends)):
			foreach((array)$myPendingFriends as $friend):
	?>
            	<li>
                	<div class="user-photo"><img src="<?=$friend['image']?>"></div>
                    <div class="user-info">
                    	<div class="user-name"><?=$friend['name']?></div>
                    	<div class="class-no"><?=$friend['class']?></div>
                    	<div>(<?=$Lang["libms"]["portal"]["myfriends"]["status"]["wainting_for_accept"]?>)</div>
                    </div>
                    <div>
                    	<a href="#" data-notification_id="<?=$friend['NotificationID']?>" class="btn-remove btn-remove-request"><?=$Lang["libms"]["portal"]["myfriends"]["remove_request"]?> <span class="glyphicon glyphicon-trash"></span></a>
                    </div>
                </li> 
	<?		endforeach;?>
    <? endif;?>
<?  		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendPendingFriend
	
   	function getMyFriendRankingUI($data, $tab='viewers',$dateRange='accumulated_by_friend'){
		global $Lang;
		
  		ob_start();
?>
		<div class="action-tabs tabs general-tabs friend-rank">
    		<ul class="nav nav-tabs" id="friend-rank-tab">
        		<li class="<?=$tab=='viewers'?'active':''?>"><a href="#act-viewers" data-toggle="tab"><i class="fa fa-trophy"></i> <?=$Lang["libms"]["portal"]["myfriends"]["ranking"]["viewers"]?></a></li>
        		<li class="<?=$tab=='borrowers'?'active':''?>"><a href="#act-borrowers" data-toggle="tab"><i class="fa fa-trophy"></i> <?=$Lang["libms"]["portal"]["myfriends"]["ranking"]["borrowers"]?></a></li>
        		<li class="<?=$tab=='reviewers'?'active':''?>"><a href="#act-reviewers" data-toggle="tab"><i class="fa fa-trophy"></i> <?=$Lang["libms"]["portal"]["myfriends"]["ranking"]["reviewers"]?></a></li>
        	</ul>
        	
    		<div class="tab-content">
				<div class="tab-pane active">
					<?=$this->getRankingPeriod($dateRange,"DateRange","form-control","_by_friend")?>
<?
	switch($tab) {
		case 'viewers':
				echo $this->getMyFriendRankingViewersUI($data);
			break;
		case 'borrowers':
				echo $this->getMyFriendRankingBorrowersUI($data);
			break;
		case 'reviewers':
				echo $this->getMyFriendRankingReviewersUI($data);
			break;
	}
?>    			
				</div>
			</div>
        </div>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendRankingUI


   	function getMyFriendRankingViewersUI($data){
		global $Lang;
		
		$i = 1;		// ranking 
		
  		ob_start();
?>
        <ul class="ranking-list">
<? 	if (count($data)):
		foreach((array)$data as $rank):  			
  			$rankClass = $i < 4 ? "rank-{$i}" : "";
?>
        	<li class="<?=$rankClass?>">
        		<div class="rank"><?=$i?></div>
            	<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$rank['user_id']?>"><img src="<?=$rank['image']?>"></a></div>
                <div class="user-info">
                	<div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$rank['user_id']?>"><?=$rank['name']?></a></div>
                	<div class="number"><?=$rank['book_count']?><?=$Lang["libms"]["portal"]["myfriends"]["ranking"]["ebook"]?></div>
                </div>
            </li> 
<? $i++; ?>
<?		endforeach;?>
<?	else:?>
			<li class="nothing"><?=$Lang["libms"]["portal"]["myfriends"]["ranking"]["no_friends_reading"]?></li>	
<?	endif;?>
    	</ul>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendRankingViewersUI


   	function getMyFriendRankingBorrowersUI($data){
		global $Lang;
		
		$i = 1;		// ranking 
		
  		ob_start();
?>
        <ul class="ranking-list">
<? 	if (count($data)):
		foreach((array)$data as $rank):
  			$rankClass = $i < 4 ? "rank-{$i}" : "";
?>
        	<li class="<?=$rankClass?>">
        		<div class="rank"><?=$i?></div>
            	<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$rank['user_id']?>"><img src="<?=$rank['image']?>"></a></div>
                <div class="user-info">
                	<div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$rank['user_id']?>"><?=$rank['name']?></a></div>
                	<div class="number"><?=$rank['loan_count']?><?=$Lang["libms"]["portal"]["myfriends"]["ranking"]["pbook"]?></div>
                </div>
            </li> 
<? $i++; ?>
<?		endforeach;?>
<?	else:?>
			<li class="nothing"><?=$Lang["libms"]["portal"]["myfriends"]["ranking"]["no_friends_borrowing"]?></li>	
<?	endif;?>
    	</ul>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendRankingBorrowersUI


   	function getMyFriendRankingReviewersUI($data){
		global $Lang;
		
		$i = 1;		// ranking 
		
  		ob_start();
?>
        <ul class="ranking-list">
<? 	if (count($data)):
		foreach((array)$data as $rank):  			
  			$rankClass = $i < 4 ? "rank-{$i}" : "";
?>
        	<li class="<?=$rankClass?>">
        		<div class="rank"><?=$i?></div>
            	<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$rank['user_id']?>"><img src="<?=$rank['image']?>"></a></div>
                <div class="user-info">
                	<div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$rank['user_id']?>"><?=$rank['name']?></a></div>
                	<div class="number"><?=$rank['review_count']?><?=$Lang["libms"]["portal"]["myfriends"]["ranking"]["review"]?></div>
                </div>
            </li> 
<? $i++; ?>
<?		endforeach;?>
<?	else:?>
			<li class="nothing"><?=$Lang["libms"]["portal"]["myfriends"]["ranking"]["no_friends_review"]?></li>	
<?	endif;?>
    	</ul>
<?
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendRankingReviewersUI

	
	function getMyFriendList4Select($myFriends=array(),$share_to_friend_ids=array()) {
		ob_start();
?>
    	<ul class="student-list">
<? foreach((array)$myFriends as $friend):?>                            	
         	<li data-friend-id="<?=$friend['user_id']?>"<?=(in_array($friend['user_id'],(array)$share_to_friend_ids) ? ' class="selected"' : '')?>>
  				<div class="user-photo"><img src="<?=$friend['image']?>" width="40" height="40"></div>
    			<div class="user-info">
					<div class="user-name"><?=$friend['name']?></div>
    		   		<div class="class-no"><?=$friend['class']?></div>
	            </div>
            </li> 
<? endforeach;?>				                    
		</ul>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getMyFriendList4Select
	

 	function getMyFriendShareBookEditUI($book_id) {
		global $Lang, $eLib, $lelibplus;
		
		if (!isset($lelibplus)) {
			$lelibplus = new elibrary_plus($book_id,$_SESSION['UserID']);
		}
		$myFriends = $lelibplus->getMyFriends();
		$share_to_friend_ids = array();
		$nrSelected = 0;	// default
		$dispSelected = sprintf($Lang["libms"]["portal"]["myfriends"]["selected_friends"],$nrSelected);
		
		ob_start();
?>
		<div class="tab-pane" id="act-share">
        	<div class="select-friends">
            	<span><?=$Lang["libms"]["portal"]["myfriends"]["friends_list"]?>: </span>
                <div class="result-box">
                	<ul id="select_friend_tab" class="nav nav-pills share-search">
						<li class="active"><a data-toggle="tab" data-type="all" href="#all_friends"><?=$Lang["libms"]["portal"]["all"]?></a></li>
						<li><a data-toggle="tab" data-type="selected" href="#all_friends" id="selected_friends_count"><?=$dispSelected?></a></li>
					</ul>
                    <div class="tab-content">
						<div id="all_friends" class="tab-pane active">
							<?=$this->getMyFriendList4Select($myFriends,$share_to_friend_ids)?>
                        </div>
                        <div id="selected_friends" class="tab-pane">
                        	
                        </div>
                    </div>                                                  
                </div>    
                <div><?=$Lang["libms"]["portal"]["myfriends"]["comments"]?>:</div>
               
                <div class="form-group">
                	<textarea class="form-control" id="Comments" name="Comments" rows="3"></textarea>
                </div>
    			<button id="btn_share" type="submit" class="btn-type1"><?=$Lang["libms"]["portal"]["share"]?></button>
    			<button type="button" class="btn-type1 btn-cancel"><?=$eLib["html"]["cancel"]?></button>
            </div>
		</div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getMyFriendShareBookEditUI
	

	function getFriendsProfileSummary($summary=array()) {
		global $Lang;
		
		ob_start();
?>
    <div class="friend-profile">
      <div class="table-header"><?=$Lang["libms"]["portal"]["myfriends"]["friend_profile"]?></div>
      <div class="table-body bg-brown">
        <div class="user-photo"><img src="<?=$summary['image']?>"></div>
        <div class="user-info">
          <div class="user-name"><?=$summary['name']?></div>
          <div class="class-no"><?=$summary['class']?></div>
        </div>
        <div class="user-stat">
          <table>
		<? if ($summary['has_ebook']):?>          
            <tr>
              <td><?=$Lang["libms"]["portal"]["myfriends"]["summary"]["ebook_viewed"]?>:</td>
              <td><?=$summary['viewed_total']?></td>
            </tr>
        <? endif;?>
        <? if ($summary['has_pbook']):?>
            <tr>
              <td><?=$Lang["libms"]["portal"]["myfriends"]["summary"]["books_borrowed"]?>:</td>
              <td><?=$summary['borrowed_total']?></td>
            </tr>
        <? endif;?>
            <tr>
              <td><?=$Lang["libms"]["portal"]["myfriends"]["summary"]["no_of_reviews"]?>:</td>
              <td><?=$summary['reviewed_total']?></td>
            </tr>
            </tr>
          </table>
        </div>
        <a href="#" id="unfriend" data-friend_id="<?=$summary['friend_id']?>" class="btn-unfriend"><?=$Lang["libms"]["portal"]["myfriends"]["unfriend"]?> <span class="glyphicon glyphicon-trash"></span></a>
      </div>
    </div>       
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getFriendsProfileSummary


	function getMyFriendReadBookCoverUI($books_data,$nav_para) {
		global $Lang;
		
		ob_start();
?>
	<div class="tab-pane active" id="<?=$nav_para['tab_id']?>">
	    <div class="table-header">
          <div class="list-top"><?=$nav_para['total'].' '.$nav_para['unit']?>
            <div class="btn-views">
            	<span class="btn-table-view off glyphicon glyphicon-th-large"></span> 
            	<a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a> 
            </div>
          </div>
	    </div>
	    <div class="table-body">
	    	<?=$this->getMyFriendBookCoverView($books_data,$nav_para);?>
	    </div>  
	  	<div class="table-footer">
	  		<?=$this->getSearchResultListFooter($nav_para);?>
	  	</div>
	</div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getMyFriendReadBookCoverUI


	function getMyFriendBookCoverView($books_data, $nav_para) {
		
		ob_start();
?>
      <ul class="result-list">
<? if ($nav_para['total']==0):?>
		<li>
			<?=$this->getNoRecordFoundBookCover()?>		
		</li>
<? else:
		$j = 0;
		foreach ((array)$books_data as $book):
			if ($book['id']) {
				if ($book['image']) {
					$image_no = '';
				}
				else {
					$image_no = $j%10+1;
					$j++;
				}
				$book['image_no'] = $image_no;
				$book['navigation_id'] = $navigation_id;
?>			
				<li>
					<div class="book-covers">
						<?=($book['book_type'] == 'ebook') ? $this->geteBookCover($book, true, "div", "", ($_SESSION['UserID']?true:false)) : $this->getpBookCover($book, true, "div", "", ($_SESSION['UserID']?true:false))?>
					</div>
					<?=$this->getBookItemsNextToCover($book)?>			
				</li>
<?
			}
			else {
?>
				<li>
					<?=$this->getDeletedPhysicalBookCover($book)?>
				</li>				
<?				
			}						
		endforeach;
   endif;
?>      
      </ul>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getMyFriendBookCoverView
	
	
	function getMyFriendReadBookListUI($books_data,$nav_para) {
		global $Lang;
		
		ob_start();
?>
	<div class="tab-pane active" id="<?=$nav_para['tab_id']?>">
	    <div class="table-header">
          <div class="list-top"><?=$nav_para['total'].' '.$nav_para['unit']?>
            <div class="btn-views">
            	<a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a> 
            	<span class="btn-list-view off glyphicon glyphicon-th-list"></span>
            </div>
          </div>
	    </div>
	    <div class="table-body">
	    <?	if ($nav_para['tab_id'] == 'act-history-books') {
	    		echo $this->getMyFriendBooksBorrowedListView($books_data,$nav_para);
	    	}
	    	else {
	    		echo $this->getMyFriendeBookReadingListView($books_data,$nav_para);
	    	}
		?>
	    </div>  
	  	<div class="table-footer">
	  		<?=$this->getSearchResultListFooter($nav_para);?>
	  	</div>
	</div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getMyFriendReadBookListUI


	function getMyFriendBooksBorrowedListView($books_data,$nav_para) {
		global $eLib, $Lang;
		
		ob_start();
?>
    <div class="table-responsive">
      <table class="table-list">
      	<thead>
        	<tr valign="bottom">
            	<th>#</th>
				<th>
				<? if ($nav_para['column_sorting']): ?>
				 <a <? if ($nav_para['sortby']=='title') :?>
				  		href='#' onclick="sortBookList('title', '<?=($nav_para['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$nav_para['order']?>"
					<? else: ?>
				  		href='#' onclick="sortBookList('title', 'asc'); return false;"
					<? endif; ?>><?=$eLib["html"]["book_title"]?></a>
			  	<? else:?>
			  		<?=$eLib["html"]["book_title"]?>
			  	<? endif;?>
				</th>

			  	<th>
			  	<? if ($nav_para['column_sorting']): ?>
			  	 <a <? if ($nav_para['sortby']=='author') :?>
			  			href='#' onclick="sortBookList('author', '<?=($nav_para['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$nav_para['order']?>"
			  		<? else: ?>
			  			href='#' onclick="sortBookList('author', 'asc'); return false;"
			  		<? endif; ?>><?=$eLib["html"]["author"]?></a>
			  	<? else:?>
			  		<?=$eLib["html"]["author"]?>
			  	<? endif;?>
			  	</th>
			  	
			  	<th>
			  	<? if ($nav_para['column_sorting']): ?>
			  	 <a <? if ($nav_para['sortby']=='publisher') :?>
				  		href='#' onclick="sortBookList('publisher', '<?=($nav_para['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$nav_para['order']?>"
					<? else: ?>
						href='#' onclick="sortBookList('publisher', 'asc'); return false;"
					<? endif; ?>><?=$eLib["html"]["publisher"]?></a>
			  	<? else:?>
			  		<?=$eLib["html"]["publisher"]?>
			  	<? endif;?>
			  	</th>
			  	
            </tr>
        </thead>

      	<tbody>
<? if ($nav_para['total']==0):?>
        	<tr>
            	<td colspan="6"><?=$eLib["html"]["no_record"]?></td>
            </tr>
<? else:
		foreach ((array)$books_data as $i=>$book):
			$j = $nav_para['record_per_page'] * ($nav_para['current_page']-1) + $i + 1;
			if ($book['id']) {
?>			
	        	<tr>
	            	<td><?=$j?></td>
					<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
					<td><?=($book['author']?$book['author']:'--')?></td>
				    <td><?=($book['publisher']?$book['publisher']:'--')?></td>
	            </tr>
<?
			}
			else {
?>
	        	<tr>
	            	<td><?=$j?></td>
					<td><?=$book['title'].' ('.$Lang["libms"]["portal"]["off_shelf"].')'?></td>
					<td><?=($book['author']?$book['author']:'--')?></td>
				    <td><?=($book['publisher']?$book['publisher']:'--')?></td>
	            </tr>
<?				
			}						
		endforeach;
   endif;
?>      
        </tbody>
      </table>
    </div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getMyFriendBooksBorrowedListView


	function getMyFriendeBookReadingListView($books_data,$nav_para) {
		global $eLib, $Lang;
		
		ob_start();
?>
    <div class="table-responsive">
      <table class="table-list">
      	<thead>
        	<tr valign="bottom">
            	<th>#</th>
				<th>
				<? if ($nav_para['column_sorting']): ?>
				 <a <? if ($nav_para['sortby']=='title') :?>
				  		href='#' onclick="sortBookList('title', '<?=($nav_para['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$nav_para['order']?>"
					<? else: ?>
				  		href='#' onclick="sortBookList('title', 'asc'); return false;"
					<? endif; ?>><?=$eLib["html"]["book_title"]?></a>
			  	<? else:?>
			  		<?=$eLib["html"]["book_title"]?>
			  	<? endif;?>
				</th>

			  	<th>
			  	<? if ($nav_para['column_sorting']): ?>
			  	 <a <? if ($nav_para['sortby']=='author') :?>
			  			href='#' onclick="sortBookList('author', '<?=($nav_para['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$nav_para['order']?>"
			  		<? else: ?>
			  			href='#' onclick="sortBookList('author', 'asc'); return false;"
			  		<? endif; ?>><?=$eLib["html"]["author"]?></a>
			  	<? else:?>
			  		<?=$eLib["html"]["author"]?>
			  	<? endif;?>
			  	</th>
			  	
			  	<th>
			  	<? if ($nav_para['column_sorting']): ?>
			  	 <a <? if ($nav_para['sortby']=='reading_date') :?>
				  		href='#' onclick="sortBookList('reading_date', '<?=($nav_para['order']=='asc')? 'desc': 'asc'?>'); return false;" class="sort_<?=$nav_para['order']?>"
					<? else: ?>
						href='#' onclick="sortBookList('reading_date', 'asc'); return false;"
					<? endif; ?>><?=$Lang["libms"]["portal"]["myfriends"]["reading_date"]?></a>
			  	<? else:?>
			  		<?=$Lang["libms"]["portal"]["myfriends"]["reading_date"]?>
			  	<? endif;?>
			  	</th>
			  	
            </tr>
        </thead>

      	<tbody>
<? if ($nav_para['total']==0):?>
        	<tr>
            	<td colspan="6"><?=$eLib["html"]["no_record"]?></td>
            </tr>
<? else:
		foreach ((array)$books_data as $i=>$book):
			$j = $nav_para['record_per_page'] * ($nav_para['current_page']-1) + $i + 1;
?>			
        	<tr>
            	<td><?=$j?></td>
			<? if (!($book['type'] == 'expired')) : ?>
				<td><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
				<td><?=($book['author']?$book['author']:'--')?></td>
			    <td><?=($book['reading_date']?$book['reading_date']:'--')?></td>
			<? else: ?>
				<td class="off"><a class="fancybox book-detail-fancybox fancybox.iframe title" href="book_details.php?book_id=<?=$book['id']?>" data-fancybox-group="<?=$navigation_id?>" title="<?=$book['title'].' - '.$book['author']?>"><?=$book['title']?></a></td>
				<td class="off"><?=($book['author']?$book['author']:'--')?></td>
			    <td class="off"><?=($book['reading_date']?$book['reading_date']:'--')?></td>
			<? endif; ?>
            </tr>
<?						
		endforeach;
   endif;
?>      
        </tbody>
      </table>
    </div>

<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getMyFriendeBookReadingListView

	
	function getMyFriendReviewsUI($reviews_data,$nav_para) {
		global $eLib_plus, $lelibplus, $Lang;
		
		if (!isset($lelibplus)) {
			$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		}
		
 		$j = 0;
 		
// 		$libelibplus = new libelibplus();
//		if(count($reviews_data)>1) {
//			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
//		}
//		else {
//			$navigation_id = '';
//		}
 		
 		$showRead = false;
 		
  		ob_start();
?>
		<div class="tab-pane active" id="<?=$nav_para['tab_id']?>">
          <ul class="review-list">

<? 	if ($nav_para['total']==0) {
		echo '<li>'.$Lang['libms']['NoRecordAtThisMoment'].'</li>';
	}
?>  		
  	<? 	foreach ((array)$reviews_data as $k=>$review):
 			if ($review['image']) {
				$image_no = '';
			}
			else {
				$image_no = $j%10+1;
				$j++;
			}
			$review['image_no'] = $image_no;
//			$review['navigation_id'] = $navigation_id;
  	?>
	        <li>
	          	<div class="book-review feed-book">
	            	<div class="book-covers">
						<?=($review['book_type'] == 'ebook') ? $this->geteBookCover($review, true, "div", "", $showRead) : $this->getpBookCover($review, true, "div")?>              
	            	</div>
	            	<div class="text"><a class="fancybox book-detail-fancybox fancybox.iframe" href="book_details.php?book_id=<?=$review['id']?>&cover_type_id=<?=$review['image_no']?>" data-fancybox-group="<?=($review['navigation_id']+1)?>" title="<?=$review['title'].' - '.$review['author']?>"><?=$review['title']?></a>	              
                  		<div class="message">
							<?=$this->getStarRatingUI($review['rating'])?>                
                    		<?=nl2br($review['content'])?>
                  		</div>                              
                  		<div class="review-date"><?=date('Y-m-d',strtotime($review['date']))?></div>    
                  		<div class="review-likes" id="like_<?=$review['review_id']?>">
		                	<a href="#" data-review_id="<?=$review['review_id']?>" data-book_id="<?=$review['book_id']?>" class="btn-type1"><?=($lelibplus->getHelpfulCheck($review['review_id'])) ? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"] ?></a>
		                	<span class="number-like"><?=$review['likes']?><span class="icon-like"></span></span>
		                </div>
                	</div>
                </div>
            </li>
	<?	endforeach;?>
		  </ul>
		  <div class="table-footer">
	  		<?=$this->getSearchResultListFooter($nav_para);?>
	  	  </div>
		</div>
<?				
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
	}	// end getMyFriendReviewsUI
	
	
   	function getMyFriendShareBookUI($sharings_data,$nav_para){
		global $Lang, $eLib_plus, $intranet_session_language;
		
 		$j = 0;
// 		$libelibplus = new libelibplus();
//		if(count($sharings_data)>1) {
//			$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
//		}
//		else {
//			$navigation_id = '';
//		}
 		$showRead = false;
		
  		ob_start();
?>
		<div class="tab-pane active" id="<?=$nav_para['tab_id']?>">
          <ul class="review-list">
          
<? 	if ($nav_para['total']==0) {
		echo '<li>'.$Lang['libms']['NoRecordAtThisMoment'].'</li>';
	}
?>  		
          
  	<? 	foreach ((array)$sharings_data as $k=>$sharing):
 			if ($sharing['image']) {
				$image_no = '';
			}
			else {
				$image_no = $j%10+1;
				$j++;
			}
			$sharing['image_no'] = $image_no;
//			$sharing['navigation_id'] = $navigation_id;
			
			$date = substr($sharing['DateModified'],0,10);
			$time = substr($sharing['DateModified'],11,5);
			$displayTime = ($intranet_session_language=="en") ? ($date . ' '. $Lang["libms"]["portal"]["myfriends"]["at"] .' ' . $time) : ($Lang["libms"]["portal"]["myfriends"]["at"] . ' '. $date . ' ' .$time);
			if ($sharing['Direction'] == 'ByMe') {
				$share_msg = $Lang["libms"]["portal"]["myfriends"]["share_a_book_by_me"].'<div class="user-name">'.$nav_para['friend_name'].'</div>';
			}
			else {
				$share_msg = '<div class="user-name">'.$nav_para['friend_name'].'</div>'.$Lang["libms"]["portal"]["myfriends"]["share_a_book_to_me"];
			} 
  	?>
            <li>
              <div class="text"><?=$share_msg?><div class="date"><?=$displayTime?></div></div>
              <div class="book-review feed-book">
            	<div class="book-covers">
					<?=($sharing['book_type'] == 'ebook') ? $this->geteBookCover($sharing, true, "div", "", $showRead) : $this->getpBookCover($sharing, true, "div")?>              
            	</div>
              
		        <div class="text">
			        <a class="fancybox book-detail-fancybox fancybox.iframe" data-fancybox-group="<?=$sharing['navigation_id']?>" href="book_details.php?book_id=<?=$sharing['id']?>&cover_type_id=<?=$sharing['image_no']?>" title="<?=$sharing['title'].' - '.$sharing['author']?>"><?=$sharing['title']?></a>
		        	<div class="message"><?=$sharing['Comments']?></div>
		        </div>
              </div>
        	</li>
	<?	endforeach;?>
		  </ul>
		  <div class="table-footer">
	  		<?=$this->getSearchResultListFooter($nav_para);?>
	  	  </div>
		  
		</div>
<?		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendShareBookUI


   	function getSharedToMeOfABookUI($sharings_data){
		global $Lang, $eLib_plus;
		
  		ob_start();
?>
		<div class="tab-pane active" id="shared-to-me">
          <ul>
          
<? 	if (count($sharings_data)==0) {
		echo '<li>'.$Lang['libms']['NoRecordAtThisMoment'].'</li>';
	}
?>  		
          
  	<? 	foreach ((array)$sharings_data as $k=>$sharing):?>
            <li>
            	<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$sharing['user_id']?>" target="_parent"><img src="<?=$sharing['image']?>" width="75" height="75"></a></div>
                <div class="review-detail">
                	<div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$sharing['user_id']?>" target="_parent"><?=$sharing['name'] ? $sharing['name'] : $eLib_plus["html"]["anonymous"] ?><?=' '.$sharing['class']?></a></div>
                      	<a href="#" data-ShareID="<?=$sharing['ShareID']?>" data-BookID="<?=$sharing['BookID']?>" class="btn-remove"><span class="glyphicon glyphicon-trash"></span></a>
                      	<div class="review-content"><?=$sharing['Comments']?></div>
                      	<div class="review-date"><?=date('Y-m-d',strtotime($sharing['DateModified']))?></div>
                  	</div>
            	</div>
            </li>
	<?	endforeach;?>
		  </ul>
		</div>
<?		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getSharedToMeOfABookUI
 	

   	function getSharedByMeOfABookUI($sharings_data){
		global $Lang, $eLib_plus, $intranet_session_language;
		
		
		
  		ob_start();
?>
		<div class="tab-pane active" id="shared-by-me">
          <ul>
          
<? 	if (count($sharings_data)==0) {
		echo '<li>'.$Lang['libms']['NoRecordAtThisMoment'].'</li>';
	}
?>  		
          
  	<? 	foreach ((array)$sharings_data as $k=>$sharing):
			$date = substr($sharing['DateModified'],0,10);
			$time = substr($sharing['DateModified'],11,5);
			$displayTime = ($intranet_session_language=="en") ? ($date . ' '. $Lang["libms"]["portal"]["myfriends"]["at"] .' ' . $time) : ($Lang["libms"]["portal"]["myfriends"]["at"] . ' '. $date . ' ' .$time); 
  	?>
            <li>
                <div class="review-detail">
                	<?=$Lang["libms"]["portal"]["book"]["I_shared_to"]?>
	    	<? $i = 0;
	    	   $nrSharedTo = count($sharing['friends']);
	    	   foreach ((array)$sharing['friends'] as $kk=>$friend):
	    	   	echo ($i > 0 ? ',' : '');
	    	   	$i++;
	    	?>
	    			<div class="user-name"><a href="myfriends_profile.php?FriendID=<?=$kk?>" target="_parent"><?=$friend ? $friend : $eLib_plus["html"]["anonymous"] ?></a></div>
	    	<? endforeach;?>
					<div class="review-date"><?=$displayTime?></div>
                    <div class="message"><?=$sharing['Comments']?></div>        
            	</div>
            </li>
	<?	endforeach;?>
		  </ul>
		</div>
<?		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getSharedByMeOfABookUI


	function getDeletedPhysicalBookCover($book) {
		global $Lang;
		ob_start();
?>
		<div class="book-covers">
	        <div class="off-shelf book-box">
	          	<div class="title ellipsis multiline"><?=$book['title']?></div>
	          	<div class="off-shelf-tag"><?=$Lang["libms"]["portal"]["off_shelf"]?></div>
	          	<div class="author"><?=$book['author']?></div>
	        </div>
	    </div>
<?		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getDeletedPhysicalBookCover
		
	
   	function getMyFriendSearchToAddFriend($keyword,$scope){
		global $Lang, $lelibplus;
		
		if (!isset($lelibplus)) {
			$lelibplus = new elibrary_plus(0,$_SESSION['UserID']);
		}
		$friend_to_add = $lelibplus->getSearchUser($keyword,$scope,'add');
		
  		ob_start();
?>
	<? 	if (count($friend_to_add)):
			foreach((array)$friend_to_add as $friend):
	?>
                	<li>
                    	<div class="user-photo"><img src="<?=$friend['image']?>"></div>
                        <div class="user-info">
                        	<div class="user-name"><?=$friend['name']?></div>
                        	<div class="class-no"><?=$friend['class']?></div>
                        </div>
                        <a href="#" data-user_id="<?=$friend['user_id']?>" class="btn-type1 add" onClick="add_friend('<?=$friend['user_id']?>');"><?=$Lang["libms"]["portal"]["myfriends"]["add_friends"]?></a>
                    </li> 
	<?		endforeach;?>
	<?	else:?>
					<li class="nothing"><?=$Lang['General']['NoRecordFound']?></li>	
	<?	endif;?>
<?  		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendSearchToAddFriend
	
	
	function getUserListToAddFriend() {
		global $lelibplus;
		
		if (!isset($lelibplus)) {
			$lelibplus = new elibrary_plus(0,$_SESSION['UserID']);
		}
		
		if (($_POST['ClassName'] == 'TeachingStaff') || ($_POST['ClassName'] == 'NonTeachingStaff')) {
			$teaching = $_POST['ClassName'] == 'TeachingStaff' ? '1' : '0';
			$excludeUserIdAry = $lelibplus->getMyFriendExcludeUser();
			$cond = " AND UserID NOT IN ('".implode("','", (array)$excludeUserIdAry)."') ";
			$staffList = $lelibplus->getTeacher($teaching, $cond);
			$x = getSelectByArray($staffList, 'id="StudentID" name="StudentID" class="form-control"');
		}
		else {
	        $x = $this->getAvailableStudentByClass($_POST['ClassName']);
		}
		return $x;
	}
	
	
   	function getMyFriendSearchFriendList($keyword,$scope){
		global $Lang, $lelibplus;
		
		if (!isset($lelibplus)) {
			$lelibplus = new elibrary_plus(0,$_SESSION['UserID']);
		}
		$pendingFriend = $lelibplus->getSearchPendingFriend($keyword); 
		$friends = $lelibplus->getSearchUser($keyword,$scope,'present');	// present friends
		
  		ob_start();
?>
	<? 	if (count($pendingFriend)):
			foreach((array)$pendingFriend as $friend):
	?>
            	<li>
                	<div class="user-photo"><img src="<?=$friend['image']?>"></div>
                    <div class="user-info">
                    	<div class="user-name"><?=$friend['name']?></div>
                    	<div class="class-no"><?=$friend['class']?></div>
                    	<div>(<?=$Lang["libms"]["portal"]["myfriends"]["status"]["wainting_for_accept"]?>)</div>
                    </div>
                    <a href="#" data-notification_id="<?=$friend['NotificationID']?>" class="btn-remove btn-remove-request"><?=$Lang["libms"]["portal"]["myfriends"]["remove_request"]?> <span class="glyphicon glyphicon-trash"></span></a>
                </li> 
	<?		endforeach;?>
	<?	endif;?>
	
	<? 	if (count($friends)):
			foreach((array)$friends as $friend):
	?>
            	<li>
                	<div class="user-photo"><a href="myfriends_profile.php?FriendID=<?=$friend['user_id']?>" target="_parent"><img src="<?=$friend['image']?>"></a></div>
                    <div class="user-info">
                    	<div class="user-name"><?=$friend['name']?></div>
                    	<span class="user-role"><span class="glyphicon glyphicon-user"></span> <?=$Lang["libms"]["portal"]["my_friend"]?></span>
                    	<div class="class-no"><?=$friend['class']?></div>
                    </div>
                </li> 
	<?		endforeach;?>
	<?	endif;?>
	
	<? 	if (count($pendingFriend) == 0 && count($friends) == 0): ?>
				<li class="nothing"><?=$Lang['General']['NoRecordFound']?></li>	
	<?	endif;?>
<?  		
 		$x = ob_get_contents();
		ob_end_clean();
		return $x;
 	}	// end getMyFriendSearchFriendList
	
	## end myfriend functions
	###############################################################################################

 	
 }		// end class libelibplus_ui
 
?>