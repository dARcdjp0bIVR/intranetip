<?php
/*
 * 	Using:
 * 	Log
 *
 *  2019-10-09 [Cameron]
 *      - show thismonth in get_portal_setting_by_page_type() [case #X157865]
 *
 *  2019-06-27 [Cameron]
 *      - add advance search field $introduction in get_search_result() 
 *      
 * 	2018-01-11 [Cameron]
 * 		- don't include language code in advanced search for book category [IP 2.5.9.1.1]
 * 
 * 	2017-12-13 [Cameron]
 * 		add book_category_type in get_search_result() [case #F130026]
 * 
 * 	2017-04-06 [Cameron]
 * 		modify get_search_result() to support special characters search in php5.4 by applying stripslashes()
 * 
 * 	2017-03-01 [Cameron]
 * 		change default sortby to title in get_search_result() [case #W113744]
 * 
 * 	2016-11-08 [Cameron]
 * 		- set names to latin1 in getUserInfo()
 */
class libelibplus extends libdb {
	private $page_type;
	
	function libelibplus() {
		global $eclass_prefix;
		# database for this module
		$this->db = $eclass_prefix . "eClass_LIBMS";
		if (isset($_SESSION['UserID']) && $_SESSION['UserID'] > 0) {
			$this->page_type = 'internal';	// internal
		}
		else {
			$this->page_type = 'opac';		// public
		}
	}
	
	function getUserInfo($uid) {
		global $junior_mck;
		
		$result = '';
		if ($uid) {
			$sql = "SELECT * FROM `LIBMS_USER` WHERE `UserID`='$uid' LIMIT 1";
			if ($junior_mck) {	
				mysql_query("set names latin1");
				$result = $this->returnResultSet($sql);
				mysql_query("set names utf8");
			}
			else {
				$result = $this->returnResultSet($sql);	
			}
		}
		return ($result)?$result[0]:'';
	}	// end class getUserInfo()
	
	// $page_type: 'internal' or 'opac'
	function get_portal_setting($key){
		if (empty($key)){
			return FALSE;
		}
		$sql = "SELECT `value` FROM `LIBMS_PORTAL_SETTING` WHERE name='$key' AND page_type='".$this->page_type."'";
		$result = $this->returnResultSet($sql);
		
		if (empty($result)){
			return FALSE;
		}
		return $result[0]['value'];
	}
	
	function get_portal_setting_by_page_type($page_type){
		global $Lang, $eLib_plus;
		
		if (empty($page_type)){
			return FALSE;
		}
		$sql = "SELECT * FROM `LIBMS_PORTAL_SETTING` WHERE page_type='".$page_type."'";
		$result = $this->returnResultSet($sql);
		
		if (empty($result)){
			return FALSE;
		}
		foreach((array)$result as $k=>$v) {
			switch ($v['type']) {
				case 'BOOL':
					$result[$k]['display_value'] = ($v['value'] == 1) ? $Lang['libms']["settings"]['system']['yes'] : $Lang['libms']["settings"]['system']['no'];
					break;
				case 'INT':
					if (substr($v['name'],-6) == '_range') {
					    switch($v['value']) {
                            case 1:
                                $result[$k]['display_value'] = $eLib_plus["html"]["thisweek"];
                                break;
                            case 2:
                                $result[$k]['display_value'] = $eLib_plus["html"]["thismonth"];
                                break;
                            default:
                                $result[$k]['display_value'] = $eLib_plus["html"]["accumulated"];
                                break;
                        }
					}
					else {
						$result[$k]['display_value'] = '';
					}
					break;
				default:
					$result[$k]['display_value'] = '';
					break;
			}
		}
		
		return $result;
	}
	
	function get_bulk_portal_setting($category){
		if (empty($category)){
			return FALSE;
		}
		$name_array = array();
		switch($category) {
			case 'ebook_shelf':
				$name_array[] = 'ebook_recommend_book'; 
				$name_array[] = 'ebook_most_hit';
				$name_array[] = 'ebook_new';
				$name_array[] = 'ebook_best_rated';
				break;
			case 'pbook_shelf':
				$name_array[] = 'pbook_recommend_book'; 
				$name_array[] = 'pbook_most_loan';
				$name_array[] = 'pbook_new';
				$name_array[] = 'pbook_best_rated';
				break;
			case 'all_book_shelf':
				$name_array[] = 'ebook_recommend_book'; 
				$name_array[] = 'ebook_most_hit';
				$name_array[] = 'ebook_new';
				$name_array[] = 'ebook_best_rated';
				$name_array[] = 'pbook_recommend_book'; 
				$name_array[] = 'pbook_most_loan';
				$name_array[] = 'pbook_new';
				$name_array[] = 'pbook_best_rated';
				break;
		}
		$sql = "SELECT `name`, `value` FROM `LIBMS_PORTAL_SETTING` WHERE name in ('".implode("','",$name_array)."') AND page_type='".$this->page_type."'";
		$result = $this->returnResultSet($sql);
		return $result;
	}

	function special_raw_string($str) {
		return rawurlencode(str_replace("<>","ltltgtgt",$str));
	}
	
	
	function get_search_result() {
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

		## get parameters:
		$update_navigation 	= $_POST['update_navigation'];
		if (isset($_POST['form_method']) && $_POST['form_method'] == 'post') {

			// common fields 			
			$sortby			= $_POST['sortby'];
			$order			= $_POST['order'];			// asc / desc
			$view_type		= $_POST['view_type'];		// cover / list
			$page_no		= $_POST['page_no']?$_POST['page_no']:1;
			$record_per_page= $_POST['record_per_page'];
			
			switch ($_POST['search_type']) {
				case 'simple_search':
					$keyword 			= $_POST['keyword'];
					$title 				= '';
					$subtitle 			= '';
					$author				= '';
					$subject			= '';
					$publisher			= '';
					$isbn				= '';
					$call_number		= '';
					$class_level_id		= '';
					$tag_id				= '';
					$category			= '';
					$subcategory		= '';
					$book_category_type	= '';
					$language			= '';
					$book_type 			= 'type_all';
					$introduction       = '';
					break;
				case 'category':			// from navigation / clicking listview in result list by category 
					$keyword 			= '';
					$title 				= '';
					$subtitle 			= '';
					$author				= '';
					$subject			= '';
					$publisher			= '';
					$isbn				= '';
					$call_number		= '';
					$class_level_id		= '';
					$tag_id				= '';
					$category			= $_POST['category'];
					$subcategory		= $_POST['subcategory'];
					$book_category_type	= $_POST['book_category_type'];
					$language			= $_POST['language'];
					$book_type 			= $_POST['book_type'] ? $_POST['book_type'] : 'type_all';
					$introduction       = '';
					break;
				case 'advanced_search':
				default:
					$keyword 			= '';
					$title 				= $_POST['title'];
					$subtitle 			= $_POST['subtitle'];
					$author				= $_POST['author'];
					$subject			= $_POST['subject'];
					$publisher			= $_POST['publisher'];
					$isbn				= $_POST['isbn'];
					$call_number		= $_POST['call_number'];
					$class_level_id		= $_POST['class_level_id'];
					$tag_id				= $_POST['tag_id'];
					$category			= $_POST['category'];				// from navigation: category only
					$subcategory		= $_POST['subcategory'];
					$book_category_type	= $_POST['book_category_type'];
					$language			= $_POST['language'];
					$lang_and_category	= $_POST['lang_and_category'];	// format: language,category
					if ($lang_and_category) {
						$category= str_replace("&amp;","&",$lang_and_category);
						list($cat_language, $category) = explode(',',$lang_and_category);		// category & language are overwritten, don't include language code to search
					}
					$book_type 		= $_POST['book_type'] ? $_POST['book_type'] : 'type_all';
					$introduction		= $_POST['introduction'];
//					session_register_intranet('ck_eLib_bookType',$_POST['book_type']);
					break;
			}

		}
		else {		// Get method
			$page_no = 1;
			switch ($_GET['search_type']) {
				case 'category':
					$keyword 			= '';
					$title 				= '';
					$subtitle 			= '';
					$author				= '';
					$subject			= '';
					$publisher			= '';
					$isbn				= '';
					$call_number		= '';
					$class_level_id		= '';
					$tag_id				= '';
					$category			= $_GET['category'];
					$subcategory		= $_GET['subcategory'];
					$book_category_type	= '';
					$language			= $_GET['language'];
					$book_type 			= 'type_all';
					$introduction       = '';
					break;
				default:
					$keyword 			= '';
					$title 				= $_GET['title'];
					$subtitle 			= $_GET['subtitle'];
					$author				= $_GET['author'];
					$subject			= $_GET['subject'];
					$publisher			= $_GET['publisher'];
					$isbn				= $_GET['isbn'];
					$call_number		= $_GET['call_number'];
					$class_level_id		= $_GET['class_level_id'];
					$tag_id				= $_GET['tag_id'];
					$category			= $_GET['category'];				// from navigation: category only
					$subcategory		= $_GET['subcategory'];
					$book_category_type	= '';
					$language			= $_GET['language'];
					$lang_and_category	= '';							// format: language,category
					$book_type 			= 'type_all';
					$introduction       = '';
					break;
			}			
		}

		if (!isset($_SESSION['UserID']) || $_SESSION['UserID'] <= 0 ) {	// from opac
			$opac_search_ebook = $this->get_portal_setting('opac_search_ebook');
			if (!$opac_search_ebook) {
				$book_type 	= 'type_physical';
			}
		}

		$keyword = str_replace("ltltgtgt","<>",trim($keyword));
		$title = trim($title);
		$subtitle = trim($subtitle);
		$author = trim($author);
		$subject = trim($subject);
		$publisher = trim($publisher);
		$isbn = trim($isbn);
		$call_number = trim($call_number);
		$tag_id= str_replace("&amp;","&",$tag_id);
		$introduction = trim($introduction);
		
//		$sortby = empty($sortby)? 'random' : $sortby;
		$sortby = empty($sortby)? 'title' : $sortby;	// 2017-03-01, use title as it'll navigate in advanced_search.php page [case #W113744]
		
		if (!in_array($order,array('asc','desc'))){
		    $order = in_array($sortby, array('hitrate','rating'))? 'desc' :'asc';
		}
		
		
		$param = array(
		    'language'			=> $language,
		    'category'			=> $category,
		    'subcategory'		=> $subcategory,
		    'book_category_type'=> $book_category_type,
		    'title'				=> $title,
		    'subtitle'			=> $subtitle,
		    'author'			=> $author,
		    'subject'			=> $subject,
		    'publisher'			=> $publisher,
		    'isbn'				=> $isbn,
		    'call_number'		=> $call_number,
		    'class_level_id'	=> $class_level_id,
		    'keyword'			=> $keyword,
		    'tag_id'			=> $tag_id,
		    'introduction'      => $introduction
		
		);
		
		if (!get_magic_quotes_gpc()) {
			foreach((array)$param as $k=>$v) {
				$param[$k] = stripslashes($v);
			}
		}		
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $book_type);
		
		$view_type = $view_type ? $view_type : 'cover';
		$offset = $offset ? $offset : 0;
		$cover_per_page = $elibplus_cfg['navigation_cover_per_page'] ? $elibplus_cfg['navigation_cover_per_page'] : 10;
		$item_per_page = $elibplus_cfg['navigation_item_per_page'] ? $elibplus_cfg['navigation_item_per_page'] : 20;
		$navigation_number_of_pages = $elibplus_cfg['navigation_number_of_pages'] ? $elibplus_cfg['navigation_number_of_pages'] : 5;
		
		$record_per_page = $record_per_page ? $record_per_page : ($view_type=='cover'?$cover_per_page:$item_per_page);
		$offset = $record_per_page * ($page_no-1);
		list($total, $books_data)=$lelibplus->getBookList($param, $offset, $record_per_page, $sortby, $order);
		
		if (count($books_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$offset = 0;			
			list($total, $books_data)=$lelibplus->getBookList($param, $offset, $record_per_page, $sortby, $order);
		}				
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;

		$ret = array();
		$ret['books_data'] = $books_data;
		$ret['nav_para'] = $nav_para;
		$ret['keyword'] = $keyword;
		$ret['title'] = $title;
		$ret['subtitle'] = $subtitle;
		$ret['author'] = $author;
		$ret['subject'] = $subject;
		$ret['publisher'] = $publisher;
		$ret['isbn'] = $isbn;
		$ret['call_number'] = $call_number;
		$ret['class_level_id'] = $class_level_id;
		$ret['tag_id'] = $tag_id;
		$ret['category'] = $category;
		$ret['subcategory'] = $subcategory;
		$ret['book_category_type'] = $book_category_type;
		$ret['language'] = $language;
		$ret['lang_and_category'] = $lang_and_category;
		$ret['sortby'] = $sortby;
		$ret['order'] = $order;
		$ret['view_type'] = $view_type;
		$ret['update_navigation'] = $update_navigation;
		$ret['column_sorting'] = 1;
		$ret['introduction'] = $introduction;
		
		return $ret;
	}	// end get_search_result
	
	
	function get_more_records_by_group() {
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
		include($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

		if (isset($_POST['form_method']) && $_POST['form_method'] == 'post') {
			$sortby			= $_POST['sortby'];
			$order			= $_POST['order'];			// asc / desc
			$view_type		= $_POST['view_type'];		// cover / list
			$page_no		= $_POST['page_no']?$_POST['page_no']:1;
			$record_per_page= $_POST['record_per_page'];
			$update_navigation 	= $_POST['update_navigation'];
			$more_type 		= $_POST['more_type'];
		}
		else {
			$sortby			= $_GET['sortby'];
			$order			= $_GET['order'];			// asc / desc
			$view_type		= $_GET['view_type'];		// cover / list
			$page_no		= $_GET['page_no']?$_GET['page_no']:1;
			$record_per_page= $_GET['record_per_page'];
			$update_navigation 	= $_GET['update_navigation'];
			$more_type 		= $_GET['more_type'];
		}
		$sortby = empty($sortby)? 'random' : $sortby;
		
		if (!in_array($order,array('asc','desc'))){
		    $order = in_array($sortby, array('hitrate','rating'))? 'desc' :'asc';
		}

		$view_type = $view_type ? $view_type : 'cover';
		$offset = $offset ? $offset : 0;
		$cover_per_page = $elibplus_cfg['navigation_cover_per_page'] ? $elibplus_cfg['navigation_cover_per_page'] : 10;
		$item_per_page = $elibplus_cfg['navigation_item_per_page'] ? $elibplus_cfg['navigation_item_per_page'] : 20;
		$navigation_number_of_pages = $elibplus_cfg['navigation_number_of_pages'] ? $elibplus_cfg['navigation_number_of_pages'] : 5;
		
		$record_per_page = $record_per_page ? $record_per_page : ($view_type=='cover'?$cover_per_page:$item_per_page);
		$offset = $record_per_page * ($page_no-1);
		
		if (isset($more_type)) {
			switch ($more_type) {
				case 'ebook_recommend_book':
					$libms = new liblms();				
					$shuffle = $libms->get_system_setting("preference_recommend_book_order") ? false : true;	// default uses random
					$ebook_recommend_book_range = $this->get_portal_setting('ebook_recommend_book_range');
					$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
					list($total, $books_data) = $lelibplus->getRecommendBooks($offset, $record_per_page, $shuffle, 'ebook', $ebook_recommend_book_range);
					if (count($books_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
						$page_no = 1;
						$offset = 0;
						list($total, $books_data) = $lelibplus->getRecommendBooks($offset, $record_per_page, $shuffle, 'ebook', $ebook_recommend_book_range);			
					}					
					break;
					
				case 'ebook_new':
					$ebook_new_range = $this->get_portal_setting('ebook_new_range');
					$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
					list($total, $books_data, $total_new) = $lelibplus->getNewBooks($record_per_page, 'ebook', $offset, $elibplus_cfg['more_new_ebooks_limit'], $ebook_new_range);
					if (count($books_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
						$page_no = 1;
						$offset = 0;
						list($total, $books_data, $total_new) = $lelibplus->getNewBooks($record_per_page, 'ebook', $offset,$elibplus_cfg['more_new_ebooks_limit'], $ebook_new_range);									
					}
					$total = $total_new;					
					break;
					
				case 'pbook_recommend_book':
					$libms = new liblms();				
					$shuffle = $libms->get_system_setting("preference_recommend_book_order") ? false : true;	// default uses random
					$pbook_recommend_book_range = $this->get_portal_setting('pbook_recommend_book_range');
					$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
					list($total, $books_data) = $lelibplus->getRecommendBooks($offset, $record_per_page, $shuffle, 'physical', $pbook_recommend_book_range);
					if (count($books_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
						$page_no = 1;
						$offset = 0;
						list($total, $books_data) = $lelibplus->getRecommendBooks($offset, $record_per_page, $shuffle, 'physical', $pbook_recommend_book_range);			
					}					
					break;
					
				case 'pbook_new':
					$pbook_new_range = $this->get_portal_setting('pbook_new_range');
					$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
					list($total, $books_data, $total_new) = $lelibplus->getNewBooks($record_per_page, 'physical', $offset, $elibplus_cfg['more_new_pbooks_limit'], $pbook_new_range);
					if (count($books_data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
						$page_no = 1;
						$offset = 0;
						list($total, $books_data, $total_new) = $lelibplus->getNewBooks($record_per_page, 'physical', $offset, $elibplus_cfg['more_new_pbooks_limit'], $pbook_new_range);									
					}
					$total = $total_new;					
					break;
					
			}
		}
		
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;

		$ret = array();
		$ret['books_data'] = $books_data;
		$ret['nav_para'] = $nav_para;
		$ret['sortby'] = $sortby;
		$ret['order'] = $order;
		$ret['view_type'] = $view_type;
		$ret['more_type'] = $more_type;
		$ret['update_navigation'] = $update_navigation;
		$ret['column_sorting'] = 0;
		
		return $ret;
		
	}	// end get_more_records_by_group
	
	
	function getNavigationID() {
		return mt_rand(0, 1000000);		// for fancybox prev / next navigation	
	}
	
}	// end class libelibplus
?>