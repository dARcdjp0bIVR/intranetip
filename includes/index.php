<?php
# Editing by:
include_once("../includes/libxml.php");
include_once("../includes/libmethod.php");
include_once("../includes/libdb.php");
include_once("../includes/libauth.php");
include_once("../includes/global.php");
include_once("../includes/settings_ischoolbag.php");

//$xmlContent = $_REQUEST['xmlContent'];

$xmlContent = $HTTP_RAW_POST_DATA;

// debug_r($_REQUEST);
$xmlContent = stripslashes($xmlContent);

if($xmlContent != '') {
	$xml = new LibXML();	
	$arr = $xml->XML_ToArray($xmlContent, 0);
	
	intranet_opendb();

	if(is_array($arr)) {
		if($arr['eClassRequest']) {
			$SessionID = $arr['eClassRequest']['SessionID'];
			$RequestID = $arr['eClassRequest']['RequestID'];
			$RequestMethod = $arr['eClassRequest']['RequestMethod'];
			
			$method = new LibMethod();
			$libauth = new libauth();
			
			# Checking before call method
			if(trim($SessionID) != '') {				
				$tUserID = $libauth->GetUserIDFromSessionKey(trim($SessionID));
								
				# Check SessionKey valid or not
				if($tUserID != '') {				
					$Result = $method->callMethod($RequestMethod, $arr);						
				}else{
					$Result = $method->getErrorArray('5');	
				}
			}elseif($RequestMethod == 'Login') {
				$RequestID = $method->Generate_RequestID();
				$Result = $method->callMethod($RequestMethod, $arr);				
			}else{
				$Result = $method->getErrorArray('5');	
			}
			
			# if $Result is not array, it is the error code
			if(!is_array($Result)) {
				$Result = $method->GetErrorArray($Result);	
			}			
			
			$ReturnContent = array();
			$ReturnContent['RequestID'] = $RequestID;
			$ReturnContent['Result'] = $Result;
			$returnContent = $xml->Array_To_XML($ReturnContent);		
		}
	}
	
	header('Content-Type: text/xml;');
	echo $returnContent;	
}
?>