<?php
// Editing by
/*
 *  2020-09-14 Cameron
 *      - filter out auto created AM application leave record in getApplyLeaveResultByStudent() to avoid showing it in parent app [IP.2.5.11.9.1]
 *      - add parameter $isClassFilter and $date to getUserRouteRecords()
 *      - modify getSingleRouteInfo(), add argument $isClassFilter and $date
 *      - handle not need to take attendance class in getAvailableTimeSlotByDateRange()
 *  2020-08-19 Cameron
 *      - retrieve ClassName and StudentName in getStudentTakingNormalRoute(), getStudentTakingASARoute() and getSpecialArrangementStudent()
 *  2020-07-06 Cameron
 *      - modify updateSchoolBusApplyLeaveToDelete() and updateSchoolBusTimeSlotApplyLeaveToDelete() to fix unable to update both AM and PM school bus apply leave record of the same date [case #Y188408]
 *  2020-06-19 Cameron
 *      - add parameter $studentID to getAvailableTimeSlotByDateRange() [case #Y188036]
 *      - pass argument $studentID to getAvailableTimeSlotByDateRange() in isTimeSlotAppliedLeave() and synceSchoolBusApplyLeaveRecord()
 *  2020-06-15 Cameron
 *      - add function getYearClassIDByUserID()
 *  2020-06-12 Cameron
 *      - retrieve YearClassID in getStudentTakingNormalRoute() and getStudentListWithClassBuilding()
 *      - modify getStudentListByBus() and getStudentListByClass(), set to absent(not taking bus) if the class is not need to take attendance in the date [case #Y187482]
 *  2019-10-14 Cameron
 *      - retrieve RecordStatus, TimeSlotID from INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT in getApplyLeaveRecord()
 *      - add function getRecordStatusByApplication(), isRecordStatusConsistent(), getTheOtherTimeSlotOfTheDate(), getApplicationIDByTimeSlotID()
 *      - add parameter $timeSlotID to pushApplyLeaveResultToParent() and getApplyLeaveRecord()
 *  2019-10-11 Cameron
 *      - add parameter $attendanceApplyLeaveID to updateSchoolBusApplyLeaveStatus() and updateSchoolBusTimeSlotApplyLeaveStatus()
 *      - pass argument $attendanceApplyLeaveID to the above functions in synceSchoolBusApplyLeaveRecord()
 *  2019-10-03 Cameron
 *      - add parameter $attendanceRecordID to updateSchoolBusApplyLeaveStatus() and updateSchoolBusTimeSlotApplyLeaveStatus()
 *      - pass argument $attendanceRecordID to the above functions in synceSchoolBusApplyLeaveRecord()
 *  2019-09-23 Cameron
 *      - add order by InputDate desc to sql in getApplyLeaveRecordForTeacher() so that the latest status is shown on top for the same student
 *  2019-09-20 Cameron
 *      - add parameter $recordStatus, $addedFrom, $attendanceRecordID, $attendanceApplyLeaveID to addApplyLeaveSlotSql() [case #Y170676]
 *      - also update RecordStatus to INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT in approveAppliedLeave()
 *      - modify getApplyLeaveRecordByStudent(), getApplyLeaveUserByDate(), isTimeSlotAppliedLeave() retrieve/compare RecordStatus from INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT
 *      - should also update RecordStatus in INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT by calling updateSchoolBusTimeSlotApplyLeaveStatus()  after updateSchoolBusApplyLeaveStatus() in synceSchoolBusApplyLeaveRecord()
 *      - pass argument $recordStatus, $addedFrom, $attendanceRecordID, $attendanceApplyLeaveID to addApplyLeaveSlotSql() in addApplyLeaveRecord()
 *      - add function updateSchoolBusTimeSlotAttendanceRecordID(), updateSchoolBusTimeSlotApplyLeaveToDelete(), updateSchoolBusTimeSlotApplyLeaveStatus()
 *      - check condition in INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT rather than INTRANET_SCH_BUS_APPLY_LEAVE in isSchoolBusApplyLeaveRecordExist()
 *
 *  2019-08-09 Cameron
 *      - add filter to checkDuplicate()
 *
 *  2019-06-27 Cameron
 *      - retrieve LastModifiedBy and DateModified in getApplyLeaveRecordForTeacher()
 *      
 *  2019-04-16 Cameron
 *      - add parameter hasRouteOrNot in getStudentListByClass() [case #Y159784]
 *      
 *  2019-04-03 Cameron
 *      - retrieve EndDate in getApplyLeaveResultByStudent()
 *      
 *  2019-04-02 Cameron
 *      - fix sql in getApplyLeaveRecordForTeacher() so that any date within the filter date range can show result
 *      
 *  2019-04-01 Cameron
 *      - fix sql error and add commit/rollback transaction control to saveUserRoutes(),
 *      
 *  2019-03-29 Cameron
 *      - fix logic in getRouteBusSeat() [case #Y152911]
 *      
 *  2019-03-08 Cameron
 *      - fix pushApplyLeaveResultToParent(): relatedUserIdAssoAry key is ParentID, value is StudentID [case #A156842]
 *      - retrieve EnglishName, ChineseName and ClasstitleEN in getApplyLeaveRecord() for push message
 *      - fix student ChineseName and EnglishName show in push message in pushApplyLeaveRecordToTeacher() and pushApplyLeaveResultToParent(), also don't show second in input time
 *      
 *  2019-03-07 Cameron
 *      - add function getTakingBusClassList() [case #A156825]
 *      - add parameter $isTakingBus to getStudentsByClassTeacher(), getAllStudentOfCurrentYear(), getStudentNameListWClassNumberByClassName()
 *      - don't pass $fromModule and $applicationID to sendPushMessage() in pushApplyLeaveRecordToTeacher() and pushApplyLeaveResultToParent() 
 *          as it's not need to show 'View Details' [case #A156842]
 *      
 *  2019-03-06 Cameron
 *      - add parameter ClassGroupIDAry to filter by class group in getStudentListByClass(), getStudentListWithClassBuilding(), getStudentTakingNormalRoute(),  
 *      getStudentTakingASARoute(), getSpecialArrangementStudent(), getStudentListByBus()
 *      - add function getUserIDByClassGroup()
 *      - add parameter filterUserIDAry to getRouteCollectionUserCount()
 *      
 *  2019-03-04 Cameron
 *      - fix sql in isSchoolBusApplyLeaveRecordExist()
 *      
 *  2019-02-27 Cameron
 *      - modify isSchoolBusApplyLeaveRecordExist(), set default $dayTypeStr to empty 
 *      
 *  2019-02-18 Cameron
 *      - add $attendanceApplyLeaveID to synceSchoolBusApplyLeaveRecord() and addApplyLeaveRecord()
 *      - add function isSchoolBusApplyLeaveRecordExist()
 *      - fix updateSchoolBusApplyLeaveToDelete(), should update both am and pm to delete
 *      - should filter $recordStatusAry in synceSchoolBusApplyLeaveRecord > getApplyLeaveRecordByStudent()
 *      
 *  2019-02-14 Cameron
 *      - add function updateSchoolBusAttendanceRecordID()
 *      
 *  2019-02-11 Cameron
 *      - add parameter $attendanceType to syncAbsentRecordFromAttendance(), 
 *        sync eAttendance to apply leave of fromSchool in eSchoolBus if $sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly'] [case #Y154315]
 *      - modifiy syncApplyLeaveRecordFromAttendance() to use the above flag
 *      
 *  2019-01-30 Cameron
 *      - add parameter $startDate and $endDate to getApplyLeaveRecordForTeacher() [case #Y154190]
 *      
 *  2019-01-29 Cameron
 *      - add parameter $activeRouteOnly in getUserRouteRecords()
 *      - add parameter $includeNullRoute in getStudentBusSchedule()
 *      - add function getSelectedTeacher(), getTeacher()
 *      - modify getRouteDbTableSql: support multiple teachers for the same route [case #Y154206]
 *      
 *  2019-01-28 Cameron
 *      - modify getToSchoolUser(), set appliedLeave status checking at the end
 *      
 *  2019-01-25 Cameron
 *      - add parameter $takeOrNot in getStudentListByBus(), retrieve absent user list by vehicle [case #Y154128]
 *      - change getFromSchoolUser(), should reset to not take and empty route if the date is picked by parent and have normal route 
 *      - modify getStudentListByClass(), filter out not take schoolbus students ( no matter normal / special )
 *      
 *  2018-12-17 Cameron
 *      - add function updateReasonAndRemark()
 *      
 *  2018-11-30 Cameron
 *      - add function getRouteBusSeat()
 *      
 *  2018-11-26 Cameron
 *      - add order by in getStudentTakingNormalRoute(), getStudentTakingASARoute(), getSpecialArrangementStudent()
 *      - modify isAllowedApplyLeave() so that it compare dates only if $daysBeforeApplyLeave > 0 (.e.g application not require to submit before 24 hrs if $daysBeforeApplyLeave=1
 *      - add function isTakingBusStudent()
 *      
 *  2018-11-23 Cameron
 *      - add $startDateType to isAllowedApplyLeave() and modify the return array
 *      
 *  2018-11-22 Cameron
 *      - add function getStudentListWithClassBuilding(), getToSchoolUser(), getFromSchoolUser(), getStudentListByClass()
 *      
 *  2018-11-20 Cameron
 *      - apply statusCondition to outer sql only in getApplyLeaveRecordByStudent(), 
 *      - add function: getStudentTakingNormalRoute(), getStudentTakingASARoute(), getSpecialArrangementStudent(), getApplyLeaveUserByDate(), getStudentListByBus()

 *  2018-11-19 Cameron
 *      - add function getClassBuilding()
 *  
 * 	2018-11-09 Carlos
 * 		- Modified getRouteDbTableSql() and getRouteInfo(), changed inner join to left join INTRANET_SCH_BUS_ROUTE_COLLECTION and INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP. (As the routes would not be shown if no stop mapped)
 * 
 *  2018-11-06 Cameron
 *      - add function updateSchoolBusApplyLeaveToDelete()
 *      
 *  2018-11-05 Cameron
 *      - add function getApplyLeaveStatusByAction(), addApplyLeaveRecord()
 *      
 *  2018-11-01 Cameron
 *      - fix getApplyLeaveRecordByStudent to get the latest leave record for the same student on the same time section in the same date
 *
 *  2018-10-31 Cameron
 *      - add function synceSchoolBusApplyLeaveRecord()
 *      - add parameter $isCurrentAcademicYear to getStudentBusSchedule() so that it retrieve current academic year record only
 *      - fix sorting order in getApplyLeaveResultByStudent
 *
 *  2018-10-30 Cameron
 *      - add function syncAbsentRecordFromAttendance(), updateSchoolBusApplyLeaveStatus()
 *
 *  2018-10-29 Cameron
 *      - add parameter $insertIgnore to addApplyLeaveSlotSql()
 *      - add function getClassTimeMode()
 *
 *  2018-10-26 Cameron
 *      - add function syncApplyLeaveRecordFromAttendance()
 *
 *  2018-10-24 Cameron
 *      - return empty array in getApplyLeaveResultByStudent() and getApplyLeaveRecordForTeacher() if there's no record
 *
 *  2018-10-18 Cameron
 *      - fix sql in isChild()
 *
 *  2018-06-29 Cameron
 *      - add function getAllStudentOfCurrentYear()
 *
 *  2018-06-26 Cameron
 *      - add function getStudentsByClassTeacher(), getStudentNameListWClassNumberByClassName()
 *
 *  2018-06-22 Cameron
 *      - add function getStudentByYearClassTeacher(), getApplyLeaveRecordForTeacher(), getParentByStudentID(), pushApplyLeaveResultToParent()
 *
 *  2018-06-20 Cameron
 *      - add function isChild(), isTimeSlotAppliedLeave()
 *
 *  2018-06-19 Cameron
 *      - add function addApplyLeaveSlot(), getApplyLeaveResultByStudent()
 *
 *  2018-06-13 Cameron
 *      - add function getHoliday(), getApplyLeaveRecordByStudent()
 *
 *  2018-06-12 Cameron
 *      - add function getApplyLeaveCutoffTimeSettingsByStudent(), isAllowedApplyLeave(), insert2Table(), update2Table(), getApplyLeaveRecord(),
 *      pushApplyLeaveRecordToTeacher()
 *
 *  2018-06-07 Cameron
 *      - add function getClassGroup(), getSchoolBusSettings(), isApplyLeaveCutoffTimeSettings(), getApplyLeaveCutoffTimeSettings(),
 *      updateSetting(), updateCutoffTimeSetting()
 */

include_once('schoolBusConfig.php');

class libSchoolBus_db extends libdb {
    ######################################################################################
    ######################################################################################
    ###																					##
    ###																					##
    ###																					##
    ###																					##
    ###																					##
    ###																					##
    ######################################################################################
    ######################################################################################
    
    public $debugMode = false;
    public function getAttendanceTableName(){
        $ay_id = Get_Current_Academic_Year_ID();
        $sql = "CREATE TABLE IF NOT EXISTS INTRANET_SCH_BUS_ATTENDANCE_$ay_id (
        AttendanceID int(11) NOT NULL auto_increment,
        RouteCollectionID int(11) NOT NULL,
        UserID int(11) NOT NULL,
        Status int(11) NOT NULL,
        Remarks varchar(45) default NULL,
        AttendanceDate date NOT NULL,
        DateInput datetime NOT NULL,
        DateModified datetime NOT NULL,
        InputBy int(11) NOT NULL,
        ModifiedBy int(11) NOT NULL,
        PRIMARY KEY (AttendanceID),
        KEY IdxAttendanceDate (AttendanceDate),
        KEY IdxUserID (UserID)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $this->db_db_query($sql);
        return 'INTRANET_SCH_BUS_ATTENDANCE_'.$ay_id;
    }
    private function getStandardInsertValue($stringArr, $excludeCommonField=false){
        $userID = $_SESSION['UserID'];
        
        $delimeter = ', ';
        $numString = count($stringArr);
        if($numString > 0){
            $result = '( ';
            for($i = 0; $i < $numString; $i++){
                $_string = $this->Get_Safe_Sql_Query($stringArr[$i]);
                $result .= "'$_string'";
                if($i != ($numString-1)){
                    $result .= $delimeter;
                }
            }
            
            // before closing add "isDeleted", "DateInput", "InputBy", "DateModified", "ModifiedBy"
            if(!$excludeCommonField){
                $result .= "$delimeter'0' ,now(), $userID, now(), $userID";
            }
            $result .= ' )';
        }
        return $result;
    }
    
    private function getSafeInsertValueList($fieldNameArr, $multiRowValueArr, $excludeCommonField=false){
        $valueListArr = array();
        foreach((array)$multiRowValueArr as $key => $row){
            $tempString = '';
            $valueListArr[] = $this->getStandardInsertValue($row, $excludeCommonField);
        }
        return $valueListArr;
    }
    
    public function insertData($tableName, $fieldNameArr, $multiRowValueArr, $excludeCommonField=false){
        
        if(empty($fieldNameArr) || empty($multiRowValueArr) || $tableName=='' ){
            // 			$sql = "/*Cannot empty any param when calling libPCM_db->getInsertSQL */";
        }
        else{
            // Must have this 4 fields - "DateInput", "InputBy", "DateModified", "ModifiedBy"
            // and it will be add automatically
            if(!$excludeCommonField){
                $fieldNameArr[] = 'IsDeleted';
                $fieldNameArr[] = 'DateInput';
                $fieldNameArr[] = 'InputBy';
                $fieldNameArr[] = 'DateModified';
                $fieldNameArr[] = 'ModifiedBy';
            }
            
            $valueListArr = $this->getSafeInsertValueList($fieldNameArr, $multiRowValueArr, $excludeCommonField);
            if(!empty($valueListArr)){
                $sql = "INSERT INTO $tableName (".implode(', ',$fieldNameArr).") Values ".implode(',', $valueListArr).";";
            }
            else{
                // 				$sql = "/* NO VALUE */";
            }
        }
        if($this->debugMode){
            debug_pr($sql);
        }
        else{
            return $this->db_db_query($sql);
            
        }
    }
    
    public function deleteData($tableName, $primaryKeyName, $primaryKeyValArr){
        $userID = $_SESSION['UserID'];
        $sql = "UPDATE $tableName SET IsDeleted = 1, DateModified = now(), ModifiedBy = '$userID' WHERE `$primaryKeyName` IN ('".implode("','", (array)$primaryKeyValArr)."') ;";
        return $this->db_db_query($sql);
    }
    public function permanentDeleteData($tableName, $primaryKeyName, $primaryKeyValArr,$additionCond=''){
        $sql = "DELETE FROM $tableName WHERE $primaryKeyName IN ('".implode("','", (array)$primaryKeyValArr)."') $additionCond" ;
        return $this->db_db_query($sql);
    }
    
    private function getSafeUpateValueList($fieldNameArr, $multiRowValueArr){
        $valueListArr = array();
        foreach((array)$multiRowValueArr as $key => $row){
            $tempString = '';
            $valueListArr[] = $this->getStandardUpdateValue($fieldNameArr, $row);
        }
        return $valueListArr;
    }
    
    private function getStandardUpdateValue($fieldNameArr, $row){
        $numField = count($fieldNameArr);
        if($numField > 0){
            $count = 0;
            $result = '';
            foreach((array)$fieldNameArr as $_key => $_fieldName){
                $count ++;
                $_fieldValue = $this->Get_Safe_Sql_Query($row[$_key]);
                $result .= ' ';
                $result .= " `$_fieldName` = '$_fieldValue' ";
                if($count != $numField){
                    $result .= ", ";
                }
            }
        }
        
        return $result;
    }
    
    public function updateData($tableName, $changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField=false){
        $userID = $_SESSION['UserID'];
        $updateSQLArr = $this->getSafeUpateValueList($changefieldNameArr, $multiRowValueArr);
        foreach((array)$updateSQLArr as $updateSQL){
            if(!$excludeCommonField){
                $sql = "UPDATE $tableName SET $updateSQL,  DateModified = now(), ModifiedBy = '$userID' WHERE IsDeleted = 0 AND $conditions";
            }
            else{
                $sql = "UPDATE $tableName SET $updateSQL WHERE $conditions";
            }
            
            if($this->debugMode){
                debug_pr($sql);
                return false;
            }
            else{
                $result[] = $this->db_db_query($sql);
            }
        }
        if(!in_array(0,$result)){
            return true;
        }
        else{
            return false;
        }
    }
    
    
    public function getBusStopsDbTableSql($keyword){
        if($keyword!==''&&$keyword!==NULL){
            $conds .= " AND
						(
							BusStopsNameChi Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
							OR BusStopsName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
						)			";
        }
        $sql = "SELECT
        BusStopsNameChi,
        BusStopsName,
        CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', BusStopsID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
        FROM INTRANET_SCH_BUS_BUS_STOPS
        WHERE IsDeleted = 0 $conds ";
        return $sql;
    }
    public function getRouteDbTableSql($keyword='',$academicYear){
        global $Lang;
        if($keyword!==''&&$keyword!==NULL){
            $conds .= " AND
						(
							RouteName Like '%".$this->Get_Safe_Sql_Like_Query($keyword)."%'
						)			";
        }
        $conds .= " AND AcademicYearID = '$academicYear'";
        $nameField = getNameFieldByLang2('u.');
        $sql = "SELECT
					 RouteName,
					IF(Type='N','".$Lang['eSchoolBus']['Settings']['Route']['NormalRoute']."',IF(Type='S','".$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute']."','---')) AS Type,
					IF(Type='N',FORMAT(COUNT(map.RouteStopsID)/4,0),'---') AS NoOfStops,
					IF(Type='S',REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT collection.Weekday),'MON','".$Lang['eSchoolBus']['Settings']['Route']['Weekday']['MON']."'),'TUE','".$Lang['eSchoolBus']['Settings']['Route']['Weekday']['TUE']."'),'WED','".$Lang['eSchoolBus']['Settings']['Route']['Weekday']['WED']."'),'THU','".$Lang['eSchoolBus']['Settings']['Route']['Weekday']['THU']."'),'FRI','".$Lang['eSchoolBus']['Settings']['Route']['Weekday']['FRI']."'),REPLACE(REPLACE(GROUP_CONCAT(distinct collection.AmPm),'AM','".$Lang['eSchoolBus']['Settings']['Route']['Am']."'),'PM','".$Lang['eSchoolBus']['Settings']['Route']['Pm']."')) AS Slot,
					CONCAT(route.StartDate,' - ',route.EndDate) AS Date,
					GROUP_CONCAT(DISTINCT vehicle.CarNum) AS Vehicle,
					/*$nameField AS Teacher,*/
                    teacher.Teacher,
					IF(route.IsActive=1,'".$Lang['eSchoolBus']['Settings']['Route']['Status']['Active']."','<span style=\"color:red\">".$Lang['eSchoolBus']['Settings']['Route']['Status']['Inactive']."</span>') AS Status,
					route.DateModified,
					CONCAT('<input type=\"checkbox\" name=\"targetIdAry[]\" id=\"targetIdChk_Global\" value=\"', route.RouteID ,'\" onclick=\"unset_checkall(this, document.getElementById(\'form1\'));\" />')
					FROM INTRANET_SCH_BUS_ROUTE AS route
					LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION AS collection ON (route.RouteID=collection.RouteID)
					LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP AS map ON (map.RouteCollectionID = collection.RouteCollectionID)
					LEFT JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP AS vehiclemap ON (vehiclemap.RouteID=route.RouteID)
					LEFT JOIN INTRANET_SCH_BUS_VEHICLE AS vehicle ON (vehiclemap.VehicleID=vehicle.VehicleID)
                    LEFT JOIN (SELECT 
                                        GROUP_CONCAT($nameField) AS Teacher, t.RouteID 
                                FROM 
                                        INTRANET_SCH_BUS_ROUTE_TEACHER t
                                INNER JOIN INTRANET_USER u ON u.UserID=t.TeacherID
                                        GROUP BY t.RouteID
                            ) AS teacher ON (teacher.RouteID = route.RouteID)
					/*LEFT JOIN INTRANET_USER AS teacher ON (teacher.UserID = route.TeacherID)*/
					WHERE route.IsDeleted = 0 AND collection.IsDeleted = 0 $conds
					GROUP BY route.RouteID
					";
					return $sql;
    }
    public function getRouteInfo($routeIDAry='',$isDeleted=0,$type='',$filterAry=array(),$additionalCond='',$includeInActive=true){
        if($routeIDAry!==''){
            $cond = " AND route.RouteID IN ('".implode("','",(array)$routeIDAry)."')";
        }
        if($type!==''){
            $cond .= " AND Type = '$type' ";
        }
        
        if(!empty($filterAry)){
            foreach($filterAry as $key => $value){
                $cond .= " AND $key = '$value' ";
            }
        }
        
        if(!$includeInActive){
            $cond .= " AND route.IsActive = '1' ";
        }
        
        $nameField = getNameFieldByLang2('teacher.');
        $sql = "SELECT
        route.RouteID,
        route.RouteName,
        route.AcademicYearID,
        route.Remarks,
        route.IsActive,
        route.Type,
        route.TeacherID,
        $nameField AS TeacherName,
        route.StartDate,
        route.EndDate,
        collection.RouteCollectionID,
        collection.Weekday,
        collection.AmPm,
        map.RouteStopsID,
        map.BusStopsID,
        map.Order,
        map.Time
        FROM INTRANET_SCH_BUS_ROUTE AS route
        LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION AS collection ON (route.RouteID=collection.RouteID)
        LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP AS map ON (map.RouteCollectionID=collection.RouteCollectionID)
        LEFT JOIN INTRANET_USER AS teacher ON (teacher.UserID = route.TeacherID)
        WHERE
        route.IsDeleted = '$isDeleted'
        AND collection.IsDeleted = 0
        $cond $additionalCond
        ORDER BY RouteCollectionID,`ORDER`";
        return $this->returnResultSet($sql);
    }
    public function getSingleRouteInfo($routeID='',$date=''){
        global $intranet_root;

        //date for selecting user with dateStart and dateEnd
        if($routeID!==''){
            $cond = " AND RouteID = '$routeID'";
        }
        $sql = "SELECT * FROM INTRANET_SCH_BUS_ROUTE WHERE 1 $cond";
        $routeInfoAry = $this->returnResultSet($sql);
        
        
        $sql = "SELECT * FROM INTRANET_SCH_BUS_ROUTE_COLLECTION WHERE RouteID = '$routeID'";
        $routeInfoAry[0]['RouteCollection'] = $this->returnResultSet($sql);
        
        if(!empty($routeInfoAry[0]['RouteCollection'])){
            $routeCollectionIDAry = Get_Array_By_Key($routeInfoAry[0]['RouteCollection'], "RouteCollectionID");
            $routeCollectionStops = $this->getRouteCollectionStops($routeCollectionIDAry);
            $routeInfoAry[0]['BusStops'] = $routeCollectionStops;
            //  			debug_pr($routeCollectionIDAry);

            $isClassFilter = false;
            $weekday_num = date("N", strtotime($date));
            if ($weekday_num == 6 || $weekday_num == 7) {
                include_once($intranet_root . "/includes/libcardstudentattend2.php");
                $lattend = new libcardstudentattend2();

                $classListToTakeAttendance = $lattend->getClassListToTakeAttendanceByDate($date);
                if (count($classListToTakeAttendance)) {
                    $isClassFilter = true;
                    foreach((array)$classListToTakeAttendance as $_classListToTakeAttendance) {
                        $classIDAry[] = $_classListToTakeAttendance[0];
                    }
                }
            }

            //Get User
            $filterMap = array('RouteCollectionID'=>$routeCollectionIDAry);
            $additionalCond = " AND (ur.DateStart <= '$date' AND ur.DateEnd >= '$date')";
            $includeUserName = true;
            if ($isClassFilter) {
                $additionalCond .= " AND yc.YearClassID IN (".implode(',',(array)$classIDAry).")";
            }

            $userRoute = $this->getUserRouteRecords($filterMap,$additionalCond,$includeUserName,$includeTime=false,$activeRouteOnly=false,$isClassFilter,$date);
            $routeInfoAry[0]['UserRoute'] = $userRoute;
            
            //Get Special Arrangement , by date
            $specialArrangeAry = $this->getSpecialUserRouteRecords(array('Date'=>$date),'',true);
            $routeInfoAry[0]['SpecialArrangement'] = $specialArrangeAry;

        }
        
        return $routeInfoAry[0];
    }
    public function getAllSpecialRouteByDate($date){
        $weekday_num = date("N", strtotime($date));
        switch($weekday_num){
            case 1:
                $weekday = 'MON';
                break;
            case 2:
                $weekday = 'TUE';
                break;
            case 3:
                $weekday = 'WED';
                break;
            case 4:
                $weekday = 'THU';
                break;
            case 5:
                $weekday = 'FRI';
                break;
            default:
                return array();
                break;
        }
        $additionalCond = "AND route.StartDate <= '$date' AND route.EndDate >= '$date'";
        return $this->getRouteInfo($routeIDAry='',$isDeleted=0,$type='S',array("Weekday"=>$weekday),$additionalCond);
        
    }
    public function getBusStopsInfo($busStopIDAry=''){
        if($busStopIDAry!==''){
            $busStopIDAry = (array) $busStopIDAry;
            $cond = " AND BusStopsID IN ('".implode("','",$busStopIDAry)."') ";
        }
        $sql = "SELECT
        *
        FROM INTRANET_SCH_BUS_BUS_STOPS
        WHERE IsDeleted = 0 $cond" ;
        return $this->returnResultSet($sql);
    }
    
    public function getVehicleRecords($filterMap=array())
    {
        //$filters = array('VehicleID','keyword','RecordStatus');
        $cond = "";
        if(isset($filterMap['VehicleID'])){
            $recordId = IntegerSafe($filterMap['VehicleID']);
            if(is_array($recordId)){
                $cond .= " AND VehicleID IN (".implode(",",$recordId).") ";
            }else{
                $cond .= " AND VehicleID='$recordId' ";
            }
        }
        
        if(isset($filterMap['RecordStatus']) && ($filterMap['RecordStatus']!='' || count($filterMap['RecordStatus']))){
            $record_status = IntegerSafe($filterMap['RecordStatus']);
            if(is_array($filterMap['RecordStatus'])){
                $cond .= " AND RecordStatus IN ('".implode("','",$record_status)."') ";
            }else{
                $cond .= " AND RecordStatus='".$record_status."' ";
            }
        }
        
        if(isset($filterMap['ExcludeVehicleID'])){
            $exclude_id = IntegerSafe($filterMap['ExcludeVehicleID']);
            if(is_array($filterMap['ExcludeVehicleID'])){
                $cond .= " AND VehicleID NOT IN (".implode(",",$exclude_id).") ";
            }else{
                $cond .= " AND VehicleID<>'".$exclude_id."' ";
            }
        }
        
        if(isset($filterMap['CheckCarNum'])){
            $cond .= " AND CarNum='".$this->Get_Safe_Sql_Query(trim($filterMap['CheckCarNum']))."' ";
        }
        if(isset($filterMap['CheckCarPlateNum'])){
            $cond .= " AND CarPlateNum='".$this->Get_Safe_Sql_Query(trim($filterMap['CheckCarPlateNum']))."' ";
        }
        
        if(isset($filterMap['keyword']) && trim($filterMap['keyword']) != ''){
            $keyword = $this->Get_Safe_Sql_Like_Query(trim($filterMap['keyword']));
            $cond .= " AND (CarNum LIKE '%$keyword%' OR CarPlateNum LIKE '%$keyword%' OR DriverName LIKE '%$keyword%') ";
        }
        
        $sql = "SELECT * FROM INTRANET_SCH_BUS_VEHICLE WHERE 1 $cond ORDER BY CarNum";
        $records = $this->returnResultSet($sql);
        return $records;
    }
    
    public function upsertVehicle($dataMap)
    {
        $fields = array('VehicleID','CarNum','DriverName','DriverTel','CarPlateNum','Seat','RecordStatus');
        $input_user_id = $_SESSION['UserID'];
        
        if(count($dataMap) == 0) return false;
        
        if(isset($dataMap['VehicleID']))
        {
            $id = $dataMap['VehicleID'];
            $sql = "UPDATE INTRANET_SCH_BUS_VEHICLE SET ";
            $sep = "";
            foreach($dataMap as $key => $val){
                if(!in_array($key, $fields) || $key == 'VehicleID') continue;
                
                $sql .= $sep."$key='".$this->Get_Safe_Sql_Query(trim($val))."'";
                $sep = ",";
            }
            $sql.= " ,DateModified=NOW(),ModifiedBy='$input_user_id' WHERE VehicleID='$id'";
            $success = $this->db_db_query($sql);
        }else{
            $keys = '';
            $values = '';
            $sep = "";
            foreach($dataMap as $key => $val){
                if(!in_array($key, $fields)) continue;
                $keys .= $sep."$key";
                $values .= $sep."'".$this->Get_Safe_Sql_Query(trim($val))."'";
                $sep = ",";
            }
            $keys .= ",DateInput,DateModified,InputBy,ModifiedBy";
            $values .= ",NOW(),NOW(),'$input_user_id','$input_user_id'";
            
            $sql = "INSERT INTO INTRANET_SCH_BUS_VEHICLE ($keys) VALUES ($values)";
            $success = $this->db_db_query($sql);
        }
        return $success;
    }
    /*
     public function upsertBusStop($dataMap)
     {
     $fields = array('BusStopsID','BusStopsName','BusStopsNameChi','IsDeleted');
     $input_user_id = $_SESSION['UserID'];
     
     if(count($dataMap) == 0) return false;
     
     if(isset($dataMap['BusStopsID']))
     {
     $id = $dataMap['BusStopsID'];
     $sql = "UPDATE INTRANET_SCH_BUS_BUS_STOPS SET ";
     $sep = "";
     foreach($dataMap as $key => $val){
     if(!in_array($key, $fields) || $key == 'BusStopsID') continue;
     
     $sql .= $sep."$key='".$this->Get_Safe_Sql_Query($val)."'";
     $sep = ",";
     }
     $sql.= " ,DateModified=NOW(),ModifiedBy='$input_user_id' WHERE BusStopsID='$id'";
     $success = $this->db_db_query($sql);
     }else{
     $keys = '';
     $values = '';
     $sep = "";
     foreach($dataMap as $key => $val){
     if(!in_array($key, $fields)) continue;
     $keys .= $sep."$key";
     $values .= $sep."'".$this->Get_Safe_Sql_Query($val)."'";
     $sep = ",";
     }
     $keys .= ",DateInput,DateModified,InputBy,ModifiedBy,IsDeleted";
     $values .= ",NOW(),NOW(),'$input_user_id','$input_user_id',0";
     
     $sql = "INSERT INTO INTRANET_SCH_BUS_BUS_STOPS ($keys) VALUES ($values)";
     $success = $this->db_db_query($sql);
     }
     return $success;
     }
     */
    public function getRouteCollection($routeIDAry=array(),$routeCollectionIDAry=array()){
        if(!empty($routeIDAry)){
            $routeIDAry = (array)$routeIDAry;
            $cond = " AND RouteID IN ('".implode("','",$routeIDAry)."') ";
        }
        if(!empty($routeCollectionIDAry)){
            $routeCollectionIDAry = (array)$routeCollectionIDAry;
            $cond .= " AND RouteCollectionID IN ('".implode("','",$routeCollectionIDAry)."') ";
        }
        $sql = "SELECT * FROM INTRANET_SCH_BUS_ROUTE_COLLECTION
        WHERE IsDeleted='0' $cond";
        return $this->returnResultSet($sql);
    }
    
    public function getRouteCollectionStops($routeCollectionIDAry){
        $routeCollectionIDAry = (array)$routeCollectionIDAry;
        $sql = "SELECT
					rcsm.RouteStopsID,rcsm.RouteCollectionID,rcsm.BusStopsID,bs.BusStopsName,bs.BusStopsNameChi,rcsm.Order,rcsm.Time
				FROM INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP AS rcsm
				INNER JOIN INTRANET_SCH_BUS_BUS_STOPS AS bs ON (bs.BusStopsID=rcsm.BusStopsID)
				WHERE 1 AND RouteCollectionID IN ('".implode("','",$routeCollectionIDAry)."')
				ORDER BY `Order` ASC
				";
        return $this->returnResultSet($sql);
    }
    public function getAllRouteCollectionIDs($routeID){
        $sql = "SELECT * FROM INTRANET_SCH_BUS_ROUTE_COLLECTION WHERE IsDeleted = '0' AND RouteID = '$routeID'";
        $routeCollections = $this->returnResultSet($sql);
        $returnAry = array();
        foreach($routeCollections as $routeCollection){
            if($routeCollection['Weekday']==''){
                // Normal Route
                if($routeCollection['AmPm']=='AM'){
                    $returnAry['N']['AM'] = $routeCollection['RouteCollectionID'];
                }else if($routeCollection['AmPm']=='PM'){
                    $returnAry['N']['PM'] = $routeCollection['RouteCollectionID'];
                }
            }else{
                //Special Route
                $returnAry['S'][$routeCollection['Weekday']]=$routeCollection['RouteCollectionID'];
            }
        }
        return $returnAry;
    }
    
    public function getRouteCollectionByType($filterMap)
    {
        $cond = '';
        if(isset($filterMap['AcademicYearID'])){
            $academic_year_id = IntegerSafe($filterMap['AcademicYearID']);
            $cond .= " AND r.AcademicYearID='$academic_year_id' ";
        }
        if(isset($filterMap['Type'])){
            if(is_array($filterMap['Type'])){
                $cond .= " AND r.Type IN ('".implode("','",$filterMap['Type'])."') ";
            }else{
                $cond .= " AND r.Type='".$filterMap['Type']."' ";
            }
            
            if(isset($filterMap['DayType']))
            {
                if(is_array($filterMap['Type'])){
                    $cond .= " AND (";
                    for($i=0;$i<count($filterMap['Type']);$i++){
                        $type_field = $filterMap['Type'][$i]=='N'? 'AmPm' : 'Weekday';
                        if(is_array($filterMap['DayType'])){
                            $cond .= ($i>0?" OR ":"")." (r.Type='".$filterMap['Type'][$i]."' AND rc.$type_field IN ('".implode("','",$filterMap['DayType'])."')) ";
                        }else{
                            $cond .= ($i>0?" OR ":"")." (r.Type='".$filterMap['Type'][$i]."' AND rc.$type_field='".$filterMap['DayType']."') ";
                        }
                    }
                    $cond .= ") ";
                }else{
                    $type_field = $filterMap['Type']=='N'? 'AmPm' : 'Weekday';
                    if(is_array($filterMap['DayType'])){
                        $cond .= " AND rc.$type_field IN ('".implode("','",$filterMap['DayType'])."') ";
                    }else{
                        $cond .= " AND rc.$type_field='".$filterMap['DayType']."' ";
                    }
                }
            }
        }
        if(isset($filterMap['Status']))
        {
            if(is_array($filterMap['Status'])){
                $cond .= " AND r.IsActive IN ('".implode("','",$filterMap['Status'])."') ";
            }else{
                $cond .= " AND r.IsActive='".$filterMap['Status']."' ";
            }
        }
        if(isset($filterMap['Date'])){
            $cond .= " AND ('".$filterMap['Date']."' BETWEEN r.StartDate AND r.EndDate) ";
        }
        
        $sql = "SELECT
        r.RouteID,
        r.RouteName,
        r.StartDate,
        r.EndDate,
        rc.RouteCollectionID,
        rc.Weekday,
        rc.AmPm
        FROM INTRANET_SCH_BUS_ROUTE as r
        INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON r.RouteID=rc.RouteID
        WHERE r.IsDeleted='0' AND rc.IsDeleted='0' $cond
        ORDER BY r.RouteName,rc.AmPm,rc.Weekday";
        
        $records = $this->returnResultSet($sql);
        return $records;
    }
    
    function getBusStopsByRouteCollection($routeCollectionIdAry)
    {
        $busNameField = Get_Lang_Selection('bs.BusStopsNameChi','bs.BusStopsName');
        
        $sql = "SELECT
        rsm.BusStopsID,
        $busNameField as BusStopsName,
        rc.RouteCollectionID
        FROM INTRANET_SCH_BUS_ROUTE_COLLECTION as rc
        INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP rsm ON rsm.RouteCollectionID=rc.RouteCollectionID
        INNER JOIN INTRANET_SCH_BUS_BUS_STOPS as bs ON bs.BusStopsID=rsm.BusStopsID
        WHERE rc.RouteCollectionID ".(is_array($routeCollectionIdAry)? " IN (".implode(",",IntegerSafe($routeCollectionIdAry)).") " : " ='".IntegerSafe($routeCollectionIdAry)."' " )."
				ORDER BY rsm.Order";
        
        $records = $this->returnResultSet($sql);
        return $records;
    }
    
    function getUserRouteRecords($filterMap,$additionalCond='',$includeUserName=false,$includeTime=false,$activeRouteOnly=false,$isClassFilter=false,$date='')
    {
        $cond = "";
        if(isset($filterMap['UserRouteID'])){
            $id = IntegerSafe($filterMap['UserRouteID']);
            if(is_array($id)){
                $cond .= " AND ur.UserRouteID IN (".implode(",",$id).") ";
            }else{
                $cond .= " AND ur.UserRouteID='$id' ";
            }
        }
        if(isset($filterMap['AcademicYearID'])){
            $academic_year_id = IntegerSafe($filterMap['AcademicYearID']);
            $cond .= " AND ur.AcademicYearID='$academic_year_id' ";
        }
        if(isset($filterMap['UserID'])){
            $id = IntegerSafe($filterMap['UserID']);
            if(is_array($id)){
                $cond .= " AND ur.UserID IN (".implode(",",$id).") ";
            }else{
                $cond .= " AND ur.UserID='$id' ";
            }
        }
        if(isset($filterMap['RouteCollectionID'])){
            $id = IntegerSafe($filterMap['RouteCollectionID']);
            if(is_array($id)){
                $cond .= " AND ur.RouteCollectionID IN (".implode(",",$id).") ";
            }else{
                $cond .= " AND ur.RouteCollectionID='$id' ";
            }
        }
        
        if($includeUserName){
            $iuJoin = " LEFT JOIN INTRANET_USER AS iu ON (iu.UserID=ur.UserID) ";
            $additionSelect = getNameFieldByLang2('iu.')." AS UserName, iu.EnglishName, iu.ChineseName,";
        }
        
        if($includeTime){
            $mapJoin = " LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP AS map ON (bs.BusStopsID=map.BusStopsID AND map.RouteCollectionID = rc.RouteCollectionID) ";
            $additionSelect .= " map.Time, ";
        }
        
        $classJoin = "";
        if ($isClassFilter) {
            $academicYearAndTermInfo = getAcademicYearAndYearTermByDate($date);
            $academicYearID = $academicYearAndTermInfo['AcademicYearID'];
            $classJoin = " LEFT JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=ur.UserID) ";
            $classJoin .= " LEFT JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$academicYearID') ";
        }

        $routeCond = $activeRouteOnly ? ' AND r.IsActive = 1 ' : '';
        
        $busStopsNameField = Get_Lang_Selection('bs.BusStopsNameChi','bs.BusStopsName');
        $sql = "SELECT
        ur.UserRouteID,
        ur.RouteCollectionID,
        ur.UserID,
        $additionSelect
        ur.AcademicYearID,
        ur.BusStopsID,
        ur.ActivityID,
        ur.DateStart,
        ur.DateEnd,
        r.RouteID,
        r.RouteName,
        IF(ur.RouteCollectionID=0,'S',r.Type) as Type,
        IF(ur.RouteCollectionID=0,ur.Weekday,rc.Weekday) as Weekday,
        rc.AmPm,
        $busStopsNameField as BusStopsName,
        v.CarNum,
        GROUP_CONCAT(v.CarNum) as Cars
        FROM INTRANET_SCH_BUS_USER_ROUTE as ur
        $iuJoin
        $classJoin
        LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteCollectionID=ur.RouteCollectionID
        LEFT JOIN INTRANET_SCH_BUS_ROUTE as r ON r.RouteID=rc.RouteID $routeCond
        LEFT JOIN INTRANET_SCH_BUS_BUS_STOPS as bs ON bs.BusStopsID=ur.BusStopsID
        LEFT JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP as rvm ON rvm.RouteID=r.RouteID
        LEFT JOIN INTRANET_SCH_BUS_VEHICLE as v ON v.VehicleID=rvm.VehicleID
        $mapJoin
        WHERE 1 $cond $additionalCond
        GROUP BY ur.UserRouteID ";
//debug_pr($sql);
        $records = $this->returnResultSet($sql);
//debug_pr($records);
        if($filterMap['IsAssoc']){
            $ary = array();
            $record_size = count($records);
            for($i=0;$i<$record_size;$i++){
                if(!isset($ary[$records[$i]['UserID']])){
                    $ary[$records[$i]['UserID']] = array();
                }
                if(!isset($ary[$records[$i]['UserID']][$records[$i]['Type']])){
                    $ary[$records[$i]['UserID']][$records[$i]['Type']] = array();
                }
                $daytype = $records[$i]['Type'] == 'N'? 'AmPm' : 'Weekday';
                $ary[$records[$i]['UserID']][$records[$i]['Type']][$records[$i][$daytype]] = $records[$i];
            }
            //debug_pr($ary);
            return $ary;
        }
        
        return $records;
    }
    
    function saveUserRoutes($dataMap)
    {
        $input_by = $_SESSION['UserID'];
        $keys = array('AM','PM','MON','TUE','WED','THU','FRI');
        $normal_types = array('AM','PM');
        
        $AcademicYearID = $dataMap['AcademicYearID'];
        $StudentID = $dataMap['StudentID'];
        $DateStart = $dataMap['DateStart'];
        $DateEnd = $dataMap['DateEnd'];
        
        $this->Start_Trans();
        $resultAry = array();
        foreach($keys as $key => $val){
            if(isset($dataMap['UserRouteID']) && isset($dataMap['UserRouteID'][$val])){
                
                if(isset($dataMap['RouteCollectionID']) && $dataMap['RouteCollectionID'][$val] != '')
                {
                    // do update user route record
                    $route_collection_id = $dataMap['RouteCollectionID'][$val];
                    $bus_stops_id = $dataMap['BusStopsID'][$val];
                    $activity_id = in_array($val,$normal_types)? 'NULL' : $dataMap['ActivityID'][$val];
                    $weekday = in_array($val,$normal_types)? 'NULL' : "'".$val."'";
                    
                    $sql = "UPDATE INTRANET_SCH_BUS_USER_ROUTE SET RouteCollectionID='$route_collection_id',BusStopsID='$bus_stops_id',ActivityID='$activity_id',Weekday=$weekday
                    ,DateStart='$DateStart',DateEnd='$DateEnd', DateModified=NOW(), ModifiedBy='$input_by'
                    WHERE UserRouteID='".$dataMap['UserRouteID'][$val]."'";
                    $resultAry['Update_'.$val.'_'.$dataMap['UserRouteID'][$val]] = $this->db_db_query($sql);
                }else{
                    // delete
                    $sql = "DELETE FROM INTRANET_SCH_BUS_USER_ROUTE WHERE UserRouteID='".$dataMap['UserRouteID'][$val]."'";
                    $resultAry['Delete_'.$val.'_'.$dataMap['UserRouteID'][$val]] = $this->db_db_query($sql);
                }
            }else{
                // do insert new user route record
                if(isset($dataMap['RouteCollectionID']) && $dataMap['RouteCollectionID'][$val] != '')
                {
                    $route_collection_id = $dataMap['RouteCollectionID'][$val];
                    $bus_stops_id = $dataMap['BusStopsID'][$val];
                    $activity_id = in_array($val,$normal_types)? 'NULL' : "'".$dataMap['ActivityID'][$val]."'";
                    $weekday = in_array($val,$normal_types)? 'NULL' : "'".$val."'";
                    
                    $sql = "INSERT INTO INTRANET_SCH_BUS_USER_ROUTE (RouteCollectionID,UserID,AcademicYearID,BusStopsID,ActivityID,Weekday,DateStart,DateEnd,DateInput,DateModified,InputBy,ModifiedBy) VALUES
                    ('$route_collection_id','$StudentID','$AcademicYearID','$bus_stops_id',$activity_id,$weekday,'$DateStart','$DateEnd',NOW(),NOW(),'$input_by','$input_by')";
                    $resultAry['Insert_'.$val] = $this->db_db_query($sql);
                }
            }
        }
        
        if (!in_array(false,$resultAry)) {
            $this->Commit_Trans();
            $ret = true;
        }
        else {
            $this->RollBack_Trans();
            $ret = false;
        }
        
        //debug_pr($resultAry);
        return $ret;
    }
    
    public function getRouteVehicle($routeID){
        $sql = "SELECT * FROM INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP WHERE RouteID = '$routeID'";
        return $this->returnResultSet($sql);
    }
    
    function getSpecialUserRouteRecords($filterMap,$additionalCond='',$includeUserName=false,$includeTime=false)
    {
        //global $indexVar;
        
        $cond = "";
        if(isset($filterMap['SpecialArrangementID'])){
            $id = IntegerSafe($filterMap['SpecialArrangementID']);
            if(is_array($id)){
                $cond .= " AND s.SpecialArrangementID IN (".implode(",",$id).") ";
            }else{
                $cond .= " AND s.SpecialArrangementID='$id' ";
            }
        }
        if(isset($filterMap['AcademicYearID'])){
            $academic_year_id = IntegerSafe($filterMap['AcademicYearID']);
            $cond .= " AND s.AcademicYearID='$academic_year_id' ";
        }
        if(isset($filterMap['UserID'])){
            $id = IntegerSafe($filterMap['UserID']);
            if(is_array($id)){
                $cond .= " AND s.UserID IN (".implode(",",$id).") ";
            }else{
                $cond .= " AND s.UserID='$id' ";
            }
        }
        if(isset($filterMap['RouteCollectionID'])){
            $id = IntegerSafe($filterMap['RouteCollectionID']);
            if(is_array($id)){
                $cond .= " AND s.RouteCollectionID IN (".implode(",",$id).") ";
            }else{
                $cond .= " AND s.RouteCollectionID='$id' ";
            }
        }
        if(isset($filterMap['Date'])){
            $date = $filterMap['Date'];
            if(is_array($date)){
                $cond .= " AND s.Date IN ('".implode("','",$date)."') ";
            }else{
                $cond .= " AND s.Date='$date' ";
            }
        }
        
        if($includeUserName){
            $iuJoin = " INNER JOIN INTRANET_USER AS iu ON (iu.UserID=s.UserID) ";
            $additionSelect = getNameFieldByLang2('iu.')." AS UserName,";
        }
        if($includeTime){
            $mapJoin = " LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP AS map ON (bs.BusStopsID=map.BusStopsID AND map.RouteCollectionID = rc.RouteCollectionID) ";
            $additionSelect .= " map.Time, ";
        }
        
        $busStopsNameField = Get_Lang_Selection('bs.BusStopsNameChi','bs.BusStopsName');
        $sql = "SELECT
        s.SpecialArrangementID,
        s.RouteCollectionID,
        s.UserID,
        $additionSelect
        s.AcademicYearID,
        s.BusStopsID,
        s.Date,
        s.DayType,
        r.RouteID,
        r.RouteName,
        $busStopsNameField as BusStopsName,
        v.CarNum,
        GROUP_CONCAT(v.CarNum) as Cars
        FROM INTRANET_SCH_BUS_SPEICAL_ARRANGEMENT as s
        $iuJoin
        LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteCollectionID=s.RouteCollectionID
        LEFT JOIN INTRANET_SCH_BUS_ROUTE as r ON r.RouteID=rc.RouteID
        LEFT JOIN INTRANET_SCH_BUS_BUS_STOPS as bs ON bs.BusStopsID=s.BusStopsID
        LEFT JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP as rvm ON rvm.RouteID=r.RouteID
        LEFT JOIN INTRANET_SCH_BUS_VEHICLE as v ON v.VehicleID=rvm.VehicleID
        $mapJoin
        WHERE 1 $cond $additionalCond
        GROUP BY s.SpecialArrangementID ";
        //debug_pr($sql);
        $records = $this->returnResultSet($sql);
        //debug_pr($records);
        if($filterMap['IsAssoc']){
            $ary = array();
            $record_size = count($records);
            for($i=0;$i<$record_size;$i++){
                if(!isset($ary[$records[$i]['UserID']])){
                    $ary[$records[$i]['UserID']] = array();
                }
                if(!isset($ary[$records[$i]['UserID']][$records[$i]['Date']])){
                    $ary[$records[$i]['UserID']][$records[$i]['Date']] = array();
                }
                $ary[$records[$i]['UserID']][$records[$i]['Date']][$records[$i]['DayType']] = $records[$i];
            }
            //debug_pr($ary);
            return $ary;
        }
        
        return $records;
    }
    
    function saveSpecialUserRoutes($dataMap)
    {
        //global $indexVar;
        $input_by = $_SESSION['UserID'];
        $day_types = array('AM','PM');
        
        $AcademicYearID = IntegerSafe($dataMap['AcademicYearID']);
        $StudentID = IntegerSafe($dataMap['StudentID']);
        
        $resultAry = array();
        $sql = "DELETE FROM INTRANET_SCH_BUS_SPEICAL_ARRANGEMENT WHERE UserID='$StudentID' AND AcademicYearID='$AcademicYearID' ";
        $resultAry['Delete'] = $this->db_db_query($sql);
        for($i=0;$i<count($dataMap['Date']);$i++){
            
            foreach($day_types as $daytype)
            {
                if(isset($dataMap['RouteCollectionID'][$daytype][$i]) && $dataMap['RouteCollectionID'][$daytype][$i] != ''){
                    $sql = "INSERT INTO INTRANET_SCH_BUS_SPEICAL_ARRANGEMENT (UserID,AcademicYearID,Date,RouteCollectionID,BusStopsID,DayType,DateInput,InputBy) VALUES
                    ('$StudentID','$AcademicYearID','".$dataMap['Date'][$i]."','".$dataMap['RouteCollectionID'][$daytype][$i]."','".$dataMap['BusStopsID'][$daytype][$i]."','$daytype',NOW(),'$input_by')";
                    $resultAry['Insert_'.$i.'_'.$daytype] = $this->db_db_query($sql);
                }
            }
        }
        
        return !in_array(false,$resultAry);
    }
    function getAttendanceInfo($routeCollectionID,$userIDAry,$attendanceDate){
        $tablename = $this->getAttendanceTableName();
        $sql = "SELECT * FROM $tablename
        WHERE RouteCollectionID = '$routeCollectionID'
        AND UserID IN ('".implode("','",$userIDAry)."')
        AND AttendanceDate = '$attendanceDate'";
        return $this->returnResultSet($sql);
    }
    public function getStudentBusSchedule($studentId,$dateStart,$dateEnd, $isCurrentAcademicYear=false, $includeNullRoute=false){
        if ($isCurrentAcademicYear) {
            $currentAcademicYearID = Get_Current_Academic_Year_ID();
            $filterMap = array('UserID'=>$studentId, 'AcademicYearID'=>$currentAcademicYearID);
        }
        else {
            $filterMap=array('UserID'=>$studentId);
        }
        
        //Get nomal route
        if ($includeNullRoute) {
            $userRouteInfo = $this->getUserRouteRecords($filterMap,$additionalCond='',$includeUserName=false,true,true);
        }
        else {
            $userRouteInfo = $this->getUserRouteRecords($filterMap,$additionalCond=' AND r.IsActive = 1 ',$includeUserName=false,true);
        }
        
        $userRouteInfo = BuildMultiKeyAssoc($userRouteInfo, 'Type', $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);
        $userRouteInfo['N'] = BuildMultiKeyAssoc($userRouteInfo['N'], 'AmPm', $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);
        //Get Special Route
        $userRouteInfo['S'] = BuildMultiKeyAssoc($userRouteInfo['S'], 'Weekday', $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);
        
        //Get Speical Arrangement
        $speciArrangement = $this->getSpecialUserRouteRecords($filterMap,$additionalCond="AND Date BETWEEN '$dateStart' AND '$dateEnd'",$includeUserName=false,true);
        $userRouteInfo['SpecialArrangement'] = BuildMultiKeyAssoc($speciArrangement, array('Date','DayType'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);

        return $userRouteInfo;
    }
    function getStaffUserInfo($teaching='',$excludeUserIdAry=''){
        if($teaching==-1) {
            $conds = "";
        }
        else if($teaching==1)
            $conds = " AND Teaching=1";
            else if($teaching==0)
                $conds = " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";
                if ($excludeUserIdAry !== '') {
                    $conds_excludeUserId = " And UserID Not In ('".implode("','", (array)$excludeUserIdAry)."') ";
                }
                
                $sql = "SELECT UserID, EnglishName,ChineseName, UserLogin FROM INTRANET_USER WHERE RecordType = 1 AND RecordStatus = 1 $conds  $conds_excludeUserId ORDER BY EnglishName, ChineseName";
                return $this->returnResultSet($sql);
    }
    function getRouteCollectionUserCount($RouteCollectionIDAry, $filterUserIDAry=''){
        $condition = "";
        if ($filterUserIDAry) {
            $condition = " AND UserID IN ('".implode("','", (array)$filterUserIDAry)."') ";
        }
        $sql = "Select RouteCollectionID, BusStopsID, count(UserID) AS CountUser From INTRANET_SCH_BUS_USER_ROUTE
			WHERE RouteCollectionID IN ('".implode("','",$RouteCollectionIDAry)."') ".$condition."
			GROUP BY RouteCollectionID, BusStopsID";
        return $this->returnResultSet($sql);
    }
    function checkDuplicate($dbTable,$dbField,$value,$excludeIsDeleted = false, $filter=''){
        if($excludeIsDeleted){
            
        }else{
            $isDeletedStatment = " AND IsDeleted = '0' ";
        }
        $sql = "SELECT * FROM $dbTable WHERE $dbField = '$value' $isDeletedStatment".$filter;
        $ary = $this->returnResultSet($sql);
        if(empty($ary)){
            return 'OK';
        }else{
            return 'DUPLICATED';
        }
    }
    
    function getClassGroup()
    {
        global $intranet_session_language;
        $groupName = ($intranet_session_language == 'b5' || $intranet_session_language == 'gb') ? 'g.TitleCh' : 'g.TitleEn';
        $sql = "SELECT g.ClassGroupID, {$groupName} AS GroupName FROM YEAR_CLASS_GROUP g WHERE g.RecordStatus='1' ORDER BY g.DisplayOrder";
        $retAry = $this->returnResultSet($sql);
        $retAry = BuildMultiKeyAssoc($retAry, array('ClassGroupID'), $IncludedDBField = array('GroupName'), $SingleValue = 1, $BuildNumericArray = 0);
        return $retAry;
    }
    
    function getSchoolBusSettings($settingKey='')
    {
        $sql = "SELECT SettingKey, SettingValue FROM INTRANET_SCH_BUS_SETTING";
        if ($settingKey) {
            $sql .= " WHERE SettingKey='".$this->Get_Safe_Sql_Query($settingKey)."'";
            $retAry = $this->returnResultSet($sql);
            return count($retAry) ? $retAry[0]['SettingValue'] : '';
        }
        else {
            $retAry = $this->returnResultSet($sql);
            $retAry = BuildMultiKeyAssoc($retAry, array('SettingKey'), $IncludedDBField = array('SettingValue'), $SingleValue = 1, $BuildNumericArray = 0);
            return $retAry;
        }
    }
    
    function isApplyLeaveCutoffTimeSettings()
    {
        $ret = $this->getSchoolBusSettings('enableApplyLeaveCutOffTiming');
        return $ret ? true : false;
    }
    
    function getApplyLeaveCutoffTimeSettings($classGroupID='', $timeSlot='')
    {
        $sql = "SELECT TimeSlot, ClassGroupID, SettingValue FROM INTRANET_SCH_BUS_SETTING WHERE TimeSlot IN (1,2) AND ClassGroupID>0";
        if ($classGroupID) {
            $sql .= " AND ClassGroupID='".$classGroupID."'";
        }
        if ($timeSlot) {
            $sql .= " AND TimeSlot='".$timeSlot."'";
        }
        $sql .= " ORDER BY ClassGroupID, TimeSlot";
        $retAry = $this->returnResultSet($sql);
        $retAry = BuildMultiKeyAssoc($retAry, array('ClassGroupID','TimeSlot'), $IncludedDBField = array('SettingValue'), $SingleValue = 1, $BuildNumericArray = 0);
        return $retAry;
    }
    
    function updateSetting($settingKey, $settingValue)
    {
        $settingKey = trim($settingKey);
        $settingValue = trim($settingValue);
        
        $sql = "INSERT INTO INTRANET_SCH_BUS_SETTING ( SettingKey , SettingValue, InputDate, InputBy, ModifiedDate, ModifiedBy)
					Values ('".$settingKey."', '".$this->Get_Safe_Sql_Query($settingValue)."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')
					ON DUPLICATE KEY UPDATE SettingValue = VALUES(SettingValue), ModifiedDate = VALUES(ModifiedDate), ModifiedBy = VALUES(ModifiedBy)";
        return $this->db_db_query($sql);
    }
    
    function updateCutoffTimeSetting($settingKey, $settingValue, $timeSlot, $classGroupID)
    {
        $settingKey = trim($settingKey);
        $settingValue = trim($settingValue);
        $timeSlot = trim($timeSlot);
        $classGroupID = trim($classGroupID);
        
        $sql = "INSERT INTO INTRANET_SCH_BUS_SETTING ( SettingKey , SettingValue, TimeSlot, ClassGroupID, InputDate, InputBy, ModifiedDate, ModifiedBy)
					Values ('".$settingKey."', '".$this->Get_Safe_Sql_Query($settingValue)."', '".$timeSlot."', '".$classGroupID."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')
					ON DUPLICATE KEY UPDATE SettingValue = VALUES(SettingValue), TimeSlot = VALUES(TimeSlot), ClassGroupID = VALUES(ClassGroupID), ModifiedDate = VALUES(ModifiedDate), ModifiedBy = VALUES(ModifiedBy)";
        return $this->db_db_query($sql);
    }
    
    function getApplyLeaveCutoffTimeSettingsByStudent($studentID, $timeSlot='')
    {
        
        $currentYearID = Get_Current_Academic_Year_ID();
        
        $sql = "SELECT
                            s.TimeSlot,
                            s.SettingValue
                FROM
                            YEAR_CLASS_USER ycu
                INNER JOIN
                            YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$currentYearID."'
                INNER JOIN
                            INTRANET_SCH_BUS_SETTING s ON s.ClassGroupID=yc.ClassGroupID
                WHERE
                            s.TimeSlot IN (1,2)
                AND
                            s.ClassGroupID>0
                AND
                            ycu.UserID='".$studentID."'";
        if ($timeSlot) {
            $sql .= " AND s.TimeSlot='".$timeSlot."'";
        }
        $sql .= " ORDER BY s.TimeSlot";
        $retAry = $this->returnResultSet($sql);
        $retAry = BuildMultiKeyAssoc($retAry, array('TimeSlot'), $IncludedDBField = array('SettingValue'), $SingleValue = 1, $BuildNumericArray = 0);
        
        return $retAry;
    }
    
    // return associated array
    function isAllowedApplyLeave($studentID, $targetDate, $startDateType='WD')
    {
        $ret = array('isAllowed'=>true);
        $enableApplyLeaveCutOffTiming = $this->getSchoolBusSettings('enableApplyLeaveCutOffTiming');
        if ($enableApplyLeaveCutOffTiming) {
            $daysBeforeApplyLeave = $this->getSchoolBusSettings('daysBeforeApplyLeave');
            $daysBeforeApplyLeave = $daysBeforeApplyLeave ? $daysBeforeApplyLeave : 0;
            $cutoffTimeSettings = $this->getApplyLeaveCutoffTimeSettingsByStudent($studentID);
            
            if (count($cutoffTimeSettings)) {
                $gotoSchoolCutoffTime = $cutoffTimeSettings['1'];
                $leaveSchoolCutoffTime = $cutoffTimeSettings['2'];
            }
            else {
                $gotoSchoolCutoffTime = '00:00';
                $leaveSchoolCutoffTime = "00:00";
            }
            $ret['daysBeforeApplyLeave'] = $daysBeforeApplyLeave;
            
            if ($daysBeforeApplyLeave) {
                $cutoffDate = date('Y-m-d',strtotime("+".$daysBeforeApplyLeave." day"));
                if ($cutoffDate <= $targetDate) {
                    $ret['isAllowed'] = true;
                }
                else {
                    $ret['isAllowed'] = false;
                    $ret['error'] = 'cutoffDate';
                }
            }
            else {
                $ts = time();
                if ($startDateType == 'AM' || $startDateType == 'WD' ) {
                    if ($ts < strtotime($targetDate.' '.$gotoSchoolCutoffTime.':00')) {
                        $ret['isAllowed'] = true;
                    }
                    else {
                        $ret['isAllowed'] = false;
                        $ret['error'] = 'cutoffTime';
                        $ret['cutoffTime'] = $gotoSchoolCutoffTime;
                    }
                }
                else {
                    if ($ts < strtotime($targetDate.' '.$leaveSchoolCutoffTime.':00')) {
                        $ret['isAllowed'] = true;
                    }
                    else {
                        $ret['isAllowed'] = false;
                        $ret['error'] = 'cutoffTime';
                        $ret['cutoffTime'] = $leaveSchoolCutoffTime;
                    }
                }                        
            }
        }
        
        return $ret;
    }
    
    // Add record to a table
    // $table = "table_name" ;
    // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
    // $condition = array( 'field1' => 'data1', field2 => 'data2');
    //
    function insert2Table($table, $dataAry = array(), $condition = array(), $run = true, $insertIgnore = false, $updateDateModified = true)
    {
        global $is_debug;
        
        $result = false;
        $recordID = 0;
        if ($insertIgnore) {
            $IGNORE = " IGNORE ";
        }
        $sql = "INSERT {$IGNORE} INTO `{$table}` SET ";
        foreach ($dataAry as $field => $value) {
            $sql .= ($value == "now()") ? "`" . $field . "`= now()," : "`" . $field . "`= '" . $this->Get_Safe_Sql_Query($value) . "',";
        }
        
        if ($updateDateModified) {
            $sql .= "`DateModified`=now(),";
        }
        $sql = substr($sql, 0, - 1); // remove last comma
        
        if (! empty($condition)) {
            foreach ($condition as $field => $value) {
                $tmp_cond[] = "`{$field}` = '" . $this->Get_Safe_Sql_Query($value) . "'";
            }
            $sql .= " WHERE " . implode(' AND ', $tmp_cond);
        }
        if ($is_debug) {
            echo $sql . '<br>';
            return;
        } else {
            if ($run) {
                $result = $this->db_db_query($sql);
                if ($result) {
                    $recordID = $this->db_insert_id();
                }
                return $recordID ? $recordID : $result;
            } else {
                return $sql;
            }
        }
    }
    
    // Update a table
    // $table = "table_name" ;
    // $dataAry = array( 'field1' => 'data1', field2 => 'data2');
    // $condition = array( 'field1' => 'data1', field2 => 'data2');
    //
    function update2Table($table, $dataAry = array(), $condition = array(), $run = true, $updateDateModified = true)
    {
        global $UserID;
        if (! is_array($dataAry)) {
            return false;
        }
        
        $sql = "UPDATE `{$table}` SET ";
        
        foreach ($dataAry as $field => $value) {
            $sql .= ($value == "now()") ? "`" . $field . "`= now()," : "`" . $field . "`= '" . $this->Get_Safe_Sql_Query($value) . "',";
        }
        
        if ($updateDateModified) {
            $sql .= "`DateModified`=now(),";
        }
        $sql = substr($sql, 0, - 1); // remove last comma
        
        if (! empty($condition)) {
            foreach ($condition as $field => $value) {
                $tmp_cond[] = "`{$field}` = '" . $this->Get_Safe_Sql_Query($value) . "'";
            }
            $sql .= " WHERE " . implode(' AND ', $tmp_cond);
        }

        if ($run) {
            $result = $this->db_db_query($sql);
            return $result;
        } else {
            return $sql;
        }
    }
    
    function getApplyLeaveRecord($recordIDAry=array(), $timeSlotID='')
    {
        $studentName = getNameFieldByLang2('u.');
        $className = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
        $currentYearID = Get_Current_Academic_Year_ID();
        
        if (count($recordIDAry)) {
            $sql = "SELECT      ".$studentName." AS StudentName,
    	                        ".$className." AS ClassName,
                    	        ycu.ClassNumber,
                                yc.YearClassID,
                                s.LeaveDate,
                                s.TimeSlot,
                                s.RecordStatus as DetailRecordStatus,
                                s.TimeSlotID,
                                a.StartDate,                                 
                                a.StartDateType,
                                a.EndDate,
                                a.EndDateType,
                                a.Reason,
                                a.Remark,
                                a.RecordStatus,
                                a.DateInput,
                                a.ApplicationID,
                                a.StudentID,
                                u.EnglishName,
                                u.ChineseName,
                                yc.ClassTitleEN
    	            FROM
    	                       INTRANET_SCH_BUS_APPLY_LEAVE a
                    INNER JOIN
                               INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s ON s.ApplicationID=a.ApplicationID
                    INNER JOIN
    	                       INTRANET_USER u ON u.UserID=a.StudentID
    	            INNER JOIN
    	                       YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
    	            INNER JOIN
    	                       YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$currentYearID."'
    	            WHERE
                                a.ApplicationID IN ('".implode("','",(array)$recordIDAry)."')";

            if ($timeSlotID) {
                $sql .= " AND s.TimeSlotID='".$timeSlotID."'";
            }
            $sql .= " ORDER BY    s.LeaveDate, s.TimeSlot";
            
            $retAry = $this->returnResultSet($sql);
            
            return $retAry;
        }
        else {
            return false;
        }
    }
    
    // applied list of not taking school bus
    function getApplyLeaveRecordByStudent($studentID, $dateStart='', $dateEnd='', $recordStatus='')
    {
        if ($studentID) {
            $condition = " WHERE a.StudentID='".$studentID."'";
            if ($dateStart) {
                $condition .= " AND s.LeaveDate>='".$dateStart."'";
            }
            if ($dateEnd) {
                $condition .= " AND s.LeaveDate<='".$dateEnd."'";
            }
            $statusCondition = '';
            if ($recordStatus) {
                $statusCondition .= " AND s.RecordStatus IN ('".implode("','",((array)$recordStatus))."')";
            }
            
            $sql = "SELECT      s.LeaveDate,
                                IF(s.TimeSlot=1,'AM','PM') AS TimeSlot,
                                a.Reason,
                                a.Remark,
                                s.RecordStatus,
                                s.DateInput,
                                a.ApplicationID
    	            FROM
    	                        INTRANET_SCH_BUS_APPLY_LEAVE a
                    INNER JOIN
                                INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s ON s.ApplicationID=a.ApplicationID
                    INNER JOIN	(SELECT
                                        MAX(a.ApplicationID) AS ApplicationID, a.StudentID, s.LeaveDate, s.TimeSlot, MAX(s.DateInput) AS DateInput
                    			 FROM
                                        INTRANET_SCH_BUS_APPLY_LEAVE a
                    			 INNER JOIN
                                        INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s ON s.ApplicationID=a.ApplicationID ".$condition ."
                    			 GROUP BY a.StudentID, s.LeaveDate, s.TimeSlot
                                ) AS g ON g.ApplicationID=a.ApplicationID AND g.StudentID=a.StudentID AND g.LeaveDate=s.LeaveDate AND g.TimeSlot=s.TimeSlot AND g.DateInput=s.DateInput
                    ".$condition . $statusCondition . " ORDER BY s.LeaveDate, s.TimeSlot";
            
            $leaveDateAry = $this->returnResultSet($sql);
            $leaveDateAry = BuildMultiKeyAssoc($leaveDateAry, array('LeaveDate','TimeSlot'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 0);
            // debug_pr($sql);
            // debug_pr($leaveDateAry);
            
            return $leaveDateAry;
        }
        else {
            return false;
        }
    }
    
    function getYearClassTeacher($yearClassID)
    {
        if ($yearClassID) {
            $sql = "SELECT UserID FROM YEAR_CLASS_TEACHER WHERE YearClassID='".$yearClassID."'";
            $retAry = $this->returnResultSet($sql);
            $retAry= BuildMultiKeyAssoc($retAry, array('UserID'), $IncludedDBField = array('UserID'), $SingleValue = 1);
            return $retAry;
        }
        else {
            return false;
        }
    }
    
    // applicable to one child only, return UserID of the child
    function getChild($userID)
    {
        if ($userID) {
            $sql = "SELECT      s.UserID
                    FROM
                                INTRANET_USER s
                    INNER JOIN
                                INTRANET_PARENTRELATION r ON r.StudentID=s.UserID
                    WHERE       r.ParentID='".$userID."'
                    AND         s.RecordStatus<>'3'
                    AND         s.RecordType='".USERTYPE_STUDENT."'";
            $retAry = $this->returnResultSet($sql);
            return count($retAry) == 1 ? $retAry[0]['UserID'] : false;
        }
        else {
            return false;
        }
    }
    
    function isChild($parentID, $childID)
    {
        if ($parentID && $childID) {
            $sql = "SELECT      s.UserID
                    FROM
                                INTRANET_USER s
                    INNER JOIN
                                INTRANET_PARENTRELATION r ON r.StudentID=s.UserID
                    WHERE       r.ParentID='".$parentID."'
                    AND         r.StudentID='".$childID."'
                    AND         s.RecordStatus<>'3'
                    AND         s.RecordType='".USERTYPE_STUDENT."'";
            $retAry = $this->returnResultSet($sql);
            return count($retAry) == 1 ? true : false;
        }
        else {
            return false;
        }
    }
    
    function pushApplyLeaveRecordToTeacher($applicationID)
    {
        global $intranet_root, $plugin, $Lang, $eclassAppConfig;
        include_once ($intranet_root . "/includes/libuser.php");
        include_once ($intranet_root . "/includes/eClassApp/libeClassApp.php");
        
        $rs = $this->getApplyLeaveRecord($applicationID);
        $subject = $Lang['eSchoolBus']['App']['PushMessage']['ToTeacher']['ApplyLeaveTitle'];
        if (count($rs)) {
            $rs = current($rs);     // use first record
            $studentID = $rs['StudentID'];
            $studentChineseName = $rs['ChineseName'];
            $studentEnglishName = $rs['EnglishName'];
            $className = $rs['ClassTitleEN'];
            $classNumber = $rs['ClassNumber'];
            $studentChineseNameWithClass = $studentChineseName. ' ('.$className.'-'.$classNumber.')';
            $studentEnglishNameWithClass = $studentEnglishName. ' ('.$className.'-'.$classNumber.')';
            
            $yearClassID = $rs['YearClassID'];
            $notifierIDAry = $this->getYearClassTeacher($yearClassID);
            $dateInput = $rs['DateInput'];
            $content = $Lang['eSchoolBus']['App']['PushMessage']['ToTeacher']['ApplyLeaveContent'];
            
            $content = str_replace("[StudentChineseName]", $studentChineseNameWithClass, $content);
            $content = str_replace("[StudentEnglishName]", $studentEnglishNameWithClass, $content);
            $content = str_replace("[ApplyDate]", date('Y-m-d H:i', strtotime($dateInput)), $content);
            
            // send push message
            $leClassApp = new libeClassApp();
            if ($plugin['eClassTeacherApp'] && $leClassApp->isSchoolInLicense('T') && count($notifierIDAry)) {
                $appType = 'T';
                foreach ((array) $notifierIDAry as $_nid) {
                    $individualMessageInfoAry[0]['relatedUserIdAssoAry'][$_nid][] = $_nid;
                }
                
                $content = intranet_undo_htmlspecialchars(strip_tags(str_replace("<br>", "\n", $content)));
                // don't pass $fromModule and $applicationID as it's not need to show 'View Details' in push message
                $pushResult = $notifyMessageId = $leClassApp->sendPushMessage($individualMessageInfoAry, $subject, $content, $isPublic = 'N', $recordStatus = 1, $appType);
                
            } else {
                $pushResult = false;
            }
            
            return ($pushResult) ? true : false;
        } else {
            return false;
        }
    }
    
    // return associative array: e.g. [2018-06-18] => Dragon Boat Festival
    function getHoliday($dateStart, $dateEnd)
    {
        $sql = "SELECT
                        Title, DATE(EventDate) AS Date
                FROM
                        INTRANET_EVENT AS a
                WHERE
                        a.EventDate BETWEEN '$dateStart' AND '$dateEnd' AND RecordType IN (3,4)
                ORDER BY
                        a.EventDate, a.RecordType DESC";
        $holidays = $this->returnResultSet($sql);
        $holidays = BuildMultiKeyAssoc($holidays, 'Date', $IncludedDBField = array('Title'), $SingleValue = 1, $BuildNumericArray = 0);
        return $holidays;
    }
    
    function addApplyLeaveSlotSql($applicationID, $leaveDate, $timeSlot, $insertIgnore=false, $recordStatus=1, $addedFrom=1, $attendanceRecordID=null, $attendanceApplyLeaveID=null)
    {
        $dataAry = array();
        $dataAry['ApplicationID'] = $applicationID;
        $dataAry['LeaveDate'] = $leaveDate;
        $dataAry['TimeSlot'] = $timeSlot;
        $dataAry['RecordStatus'] = $recordStatus;
        $dataAry['AddedFrom'] = $addedFrom;
        if ($attendanceRecordID) {
            $dataAry['AttendanceRecordID'] = $attendanceRecordID;
        }
        if ($attendanceApplyLeaveID) {
            $dataAry['AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
        }
        $dataAry['DateInput'] = 'now()';
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        $sql = $this->insert2Table('INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT',$dataAry,array(),false,$insertIgnore);
        return $sql;
    }
    
    function getApplyLeaveResultByStudent($studentID, $recordStatus='', $startIndex=0, $numberOfRecord='')
    {
        global $sys_custom;

        if ($studentID) {
            $sql = "SELECT
                                a.ApplicationID,
                                a.Reason,
                                a.Remark,
                                a.RecordStatus,
                                MIN(s.LeaveDate) AS StartDate,
                                MAX(s.LeaveDate) AS EndDate
    	            FROM
    	                        INTRANET_SCH_BUS_APPLY_LEAVE a
                    INNER JOIN
                                INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s ON s.ApplicationID=a.ApplicationID
    	            WHERE
                                a.StudentID='".$studentID."'";
            if ($recordStatus) {
                $sql .= " AND a.RecordStatus IN ('".implode("','",((array)$recordStatus))."')";
            }
            if ($sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly']) {
                $sql .= " AND NOT (a.StartDateType='AM' AND a.EndDateType=a.StartDateType AND a.StartDate=a.EndDate) ";
            }
            $sql .= " GROUP BY a.ApplicationID";
            $sql .= " ORDER BY a.StartDate DESC, a.ApplicationID DESC";
            if ($numberOfRecord) {
                $sql .= " LIMIT " . $startIndex. ",".$numberOfRecord;
            }
            
            $leaveDateAry = $this->returnResultSet($sql);
            
            return $leaveDateAry;
        }
        else {
            return array();
        }
    }
    
    
    // given date range with time slot, return time slot array in this format: $retAry[date][timeSlot]
    // e.g. $retAry = array([2018-06-20] => array('AM'=>1, 'PM'=>1)
    // filter out Saturday, Sunday, Public Holidays and School Holidays
    
    function getAvailableTimeSlotByDateRange($startDate, $endDate, $startDateType, $endDateType, $studentID)
    {
        global $intranet_root;
        include_once ($intranet_root . "/includes/libcardstudentattend2.php");

        $validTimeSlotAry = array('WD','AM','PM');

        if (($startDate == '') || ($endDate == '') || !in_array($startDateType,$validTimeSlotAry) || !in_array($endDateType,$validTimeSlotAry)) {
            return false;
        }
        
        if ($startDate > $endDate) {
            return false;
        }
        
        $holidays = $this->getHoliday($startDate, $endDate);
        $retAry = array();
        
        $_startDateType = ($startDateType == 'AM' || $startDateType == 'WD') ? 'AM' : 'PM';
        $_endDateType = ($endDateType == 'PM' || $endDateType == 'WD') ? 'PM' : 'AM';
        $_startDate = $startDate;

        $yearClassID = $this->getYearClassIDByUserID($studentID);
        $lattend = new libcardstudentattend2();

        while ($_startDate <= $endDate)
        {
            $_timeStamp = strtotime($_startDate);
            $_weekDay = date('N', $_timeStamp);

            $notNeedToTakeAttendanceYearClassIDAry = array();
            $notNeedToTakeAttendanceClassAry = $lattend->getClassListNotTakeAttendanceByDate($_startDate);
            foreach((array)$notNeedToTakeAttendanceClassAry as $_notNeedToTakeAttendanceClassAry ) {
                $notNeedToTakeAttendanceYearClassIDAry[] = $_notNeedToTakeAttendanceClassAry[0];
            }

            $needToTakeAttendanceYearClassIDAry = array();
            $classListToTakeAttendance = $lattend->getClassListToTakeAttendanceByDate($_startDate);
            foreach ((array)$classListToTakeAttendance as $_classListToTakeAttendance) {
                $needToTakeAttendanceYearClassIDAry[] = $_classListToTakeAttendance[0];
            }

            if (in_array($yearClassID, $notNeedToTakeAttendanceYearClassIDAry)) {       // NotNeedToTakeAttendance
                // do nothing
            }
            else {
                $process = true;
                if (($_weekDay == '6') || ($_weekDay == '7')) {     // Saturday & Sunday
                    if (!in_array($yearClassID, (array)$needToTakeAttendanceYearClassIDAry)) {
                        $process = false;
                    }
                }

                if ($process) {
                    if (!empty($holidays[$_startDate])) {           // public holiday & school holiday
                        // do nothing
                        } else {
                        if ($_startDate == $endDate) {

                            if ($_startDateType == 'AM') {
                                $retAry[$_startDate]['AM'] = true;
                            }

                            if ($_endDateType == 'PM') {
                                $retAry[$_startDate]['PM'] = true;
                            }
                        }       // start date = end date
                        else {

                            // first page
                            if (($_startDateType == 'PM') && ($_startDate == $startDate)) {
                                $retAry[$_startDate]['PM'] = true;
                                } else {
                                $retAry[$_startDate]['AM'] = true;
                                $retAry[$_startDate]['PM'] = true;
                            }

                            $_startDateType = 'AM';
                        }
                    }   // end non-holidays
                }       // end weekdays
            }

            $_startDate = date('Y-m-d',mktime(0,0,0,date('n',$_timeStamp), date('j',$_timeStamp) + 1, date('Y',$_timeStamp)));
            
        }       // end while
        
        return $retAry;
    }
    
    function isTimeSlotAppliedLeave($studentID, $startDate, $endDate, $startDateType, $endDateType)
    {
        global $schoolBusConfig;
        
        $recordStatusAry = array(
            $schoolBusConfig['ApplyLeaveStatus']['Waiting'],
            $schoolBusConfig['ApplyLeaveStatus']['Confirmed'],
            $schoolBusConfig['ApplyLeaveStatus']['Absent']
        );
        
        $amDateAry = array();
        $pmDateAry = array();
        $availableTimeSlotAry = $this->getAvailableTimeSlotByDateRange($startDate, $endDate, $startDateType, $endDateType, $studentID);
        
        // get available time slot
        if (count($availableTimeSlotAry)) {
            foreach((array)$availableTimeSlotAry as $_date => $_timeSlotAry) {
                foreach((array)$_timeSlotAry as $_timeSlot=>$val) {
                    if ($_timeSlot == 'AM') {
                        $amDateAry[] = $_date;
                    }
                    else {
                        $pmDateAry[] = $_date;
                    }
                }
            }
        }
        
        if (count($amDateAry)) {
            $sql = "SELECT      s.TimeSlotID
                    FROM
                                INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s
                    INNER JOIN
                                INTRANET_SCH_BUS_APPLY_LEAVE a ON a.ApplicationID=s.ApplicationID
                    WHERE
                                a.StudentID='".$studentID."'
                    AND         s.TimeSlot=1
                    AND         s.RecordStatus IN ('".implode("','",$recordStatusAry)."')
                    AND         s.LeaveDate IN ('".implode("','",$amDateAry)."')
                    LIMIT 1";
            $rs = $this->returnResultSet($sql);
            if (count($rs)) {
                return true;
            }
        }
        
        if (count($pmDateAry)) {
            $sql = "SELECT      s.TimeSlotID
                    FROM
                                INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s
                    INNER JOIN
                                INTRANET_SCH_BUS_APPLY_LEAVE a ON a.ApplicationID=s.ApplicationID
                    WHERE
                                a.StudentID='".$studentID."'
                    AND         s.TimeSlot=2
                    AND         s.RecordStatus IN ('".implode("','",$recordStatusAry)."')
                    AND         s.LeaveDate IN ('".implode("','",$pmDateAry)."')
                    LIMIT 1";
            $rs = $this->returnResultSet($sql);
            if (count($rs)) {
                return true;
            }
        }
        
        return false;
    }
    
    // get those of current year only, return array of StudentID
    function getStudentByYearClassTeacher($teacherID)
    {
        if ($teacherID) {
            $currentYearID = Get_Current_Academic_Year_ID();
            $sql = "SELECT      ycu.UserID AS StudentID
                    FROM
                                YEAR_CLASS_USER ycu
                    INNER JOIN
                                YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$currentYearID."'
                    INNER JOIN
                                YEAR_CLASS_TEACHER yt ON yt.YearClassID=yc.YearClassID
                    WHERE
                                yt.UserID='".$teacherID."'
                    ORDER BY ycu.UserID";
            
            $retAry = $this->returnResultSet($sql);
            $retAry= BuildMultiKeyAssoc($retAry, array('StudentID'), $IncludedDBField = array('StudentID'), $SingleValue = 1);
            return $retAry;
        }
        else {
            return false;
        }
    }
    
    // suppose pass in $teacherID is the UserID of a class teacher, admin need not pass in this parameter or let it be empty
    function getApplyLeaveRecordForTeacher($teacherID='', $recordStatus='', $offset=0, $numberOfRecord=10, $selectClassName='', $selectStudentID='', $inputSearch='',$orderBy='DateInput', $startDate='', $endDate='')
    {
        $isAllowedProcess = true;
        if ($teacherID) {
            $studentIDAry = $this->getStudentByYearClassTeacher($teacherID);
            if (count($studentIDAry) == 0) {
                $isAllowedProcess = false;
            }
        }
        else {
            $studentIDAry = array();
        }
        $retAry = array();
        
        if ($isAllowedProcess) {
            $studentName = getNameFieldByLang2('u.');
            $className = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
            $lastModifiedBy = getNameFieldByLang2('up.');
            $currentYearID = Get_Current_Academic_Year_ID();
            
            $sql = "SELECT      ".$studentName." AS StudentName,
    	                        ".$className." AS ClassName,
                    	        ycu.ClassNumber,
                                yc.YearClassID,
                                a.StartDate,
                                a.StartDateType,
                                a.EndDate,
                                a.EndDateType,
                                a.Reason,
                                a.Remark,
                                a.RecordStatus,
                                a.DateInput,
                                a.ApplicationID,
                                a.StudentID,
                                ".$lastModifiedBy." AS LastModifiedBy,
                                a.DateModified
    	            FROM
    	                       INTRANET_SCH_BUS_APPLY_LEAVE a
                    INNER JOIN
    	                       INTRANET_USER u ON u.UserID=a.StudentID
    	            INNER JOIN
    	                       YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
    	            INNER JOIN
    	                       YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$currentYearID."'
                    INNER JOIN
    	                       INTRANET_USER up ON up.UserID=a.ModifiedBy
    	            WHERE      1=1";
            
            if ($recordStatus) {
                $sql .= " AND a.RecordStatus IN ('".implode("','",((array)$recordStatus))."')";
            }
            if (count($studentIDAry)) {
                $sql .= " AND a.StudentID IN ('".implode("','",((array)$studentIDAry))."')";
            }
            
            if (!empty($selectClassName)) {
                $sql .= " AND yc.ClassTitleEN='".$this->Get_Safe_Sql_Query($selectClassName)."'";
            }
            if ($selectStudentID) {
                $sql .= " AND ycu.UserID='".$selectStudentID."'";
            }
            if (!empty($inputSearch)) {
                $sql .= " AND (u.EnglishName LIKE '%".$this->Get_Safe_Sql_Like_Query($inputSearch)."%'";
                $sql .= " OR u.ChineseName LIKE '%".$this->Get_Safe_Sql_Like_Query($inputSearch)."%'";
                $sql .= " OR a.Reason LIKE '%".$this->Get_Safe_Sql_Like_Query($inputSearch)."%')";
            }
            
            if (($startDate != '') && ($endDate != '')) {
                $sql .= " AND (";
                    $sql .= "(a.StartDate=a.EndDate AND a.StartDate>='".$startDate."' AND a.EndDate<='".$endDate."') OR ";
                    $sql .= "(a.StartDate < a.EndDate AND (";
                        $sql .= "(a.StartDate<='".$startDate."' AND a.EndDate>='".$endDate."') OR ";
                        $sql .= "(a.StartDate>='".$startDate."' AND a.StartDate<='".$endDate."' AND a.EndDate>='".$endDate."') OR ";
                        $sql .= "(a.EndDate>='".$startDate."' AND a.EndDate<='".$endDate."' AND a.StartDate<='".$startDate."') OR ";
                        $sql .= "(a.StartDate>='".$startDate."' AND a.EndDate<='".$endDate."')";
                    $sql .= ")))";
            }
            else { 
                if ($startDate != '') {
                    $sql .= " AND a.StartDate>='".$startDate."'";
                }
                
                if ($endDate != '') {
                    $sql .= " AND a.EndDate<='".$endDate."'";
                }
            }
            
            $sql .= " ORDER BY";
            
            if ($orderBy == 'DateInput') {
                $sql .= " a.DateInput DESC";
            }
            else {
                $sql .= " a.StartDate DESC, {$className}, ycu.ClassNumber, a.DateInput DESC";
            }
            $sql .= " LIMIT " . $offset. ", ".$numberOfRecord;
            
            $retAry = $this->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($retAry);
            
        }
        return $retAry;
    }
    
    function getParentByStudentID($studentID)
    {
        if ($studentID) {
            $sql = "SELECT      p.UserID
                    FROM
                                INTRANET_USER p
                    INNER JOIN
                                INTRANET_PARENTRELATION r ON r.ParentID=p.UserID
                    WHERE       r.StudentID='".$studentID."'
                    AND         p.RecordStatus<>'3'
                    AND         p.RecordType='".USERTYPE_PARENT."'";
            $retAry = $this->returnResultSet($sql);
            return count($retAry) == 1 ? $retAry[0]['UserID'] : false;
        }
        else {
            return false;
        }
    }
    
    function pushApplyLeaveResultToParent($applicationID, $timeSlotID='')
    {
        global $intranet_root, $plugin, $Lang, $eclassAppConfig, $schoolBusConfig;
        include_once ($intranet_root . "/includes/libuser.php");
        include_once ($intranet_root . "/includes/eClassApp/libeClassApp.php");
        
        $rs = $this->getApplyLeaveRecord($applicationID, $timeSlotID);
        
        if (count($rs)) {
            $rs = current($rs);     // use first record
            $studentID = $rs['StudentID'];
            $studentChineseName = $rs['ChineseName'];
            $studentEnglishName = $rs['EnglishName'];
            $className = $rs['ClassTitleEN'];
            $classNumber = $rs['ClassNumber'];
            $studentChineseNameWithClass = $studentChineseName. ' ('.$className.'-'.$classNumber.')';
            $studentEnglishNameWithClass = $studentEnglishName. ' ('.$className.'-'.$classNumber.')';
            $notifierIDAry = $this->getParentByStudentID($studentID);
            $dateInput = $rs['DateInput'];
            $recordStatus = $rs['DetailRecordStatus'];
            
            switch ($recordStatus) {
                case $schoolBusConfig['ApplyLeaveStatus']['Cancelled']:
                    $subject = $Lang['eSchoolBus']['App']['PushMessage']['ToParent']['CancelApplicationTitle'];
                    $content = $Lang['eSchoolBus']['App']['PushMessage']['ToParent']['CancelApplicationContent'];
                    break;
                    
                case $schoolBusConfig['ApplyLeaveStatus']['Confirmed']:
                    $subject = $Lang['eSchoolBus']['App']['PushMessage']['ToParent']['ApproveApplicationTitle'];
                    $content = $Lang['eSchoolBus']['App']['PushMessage']['ToParent']['ApproveApplicationContent'];
                    break;
                    
                case $schoolBusConfig['ApplyLeaveStatus']['Rejected']:
                    $subject = $Lang['eSchoolBus']['App']['PushMessage']['ToParent']['RejectApplicationTitle'];
                    $content = $Lang['eSchoolBus']['App']['PushMessage']['ToParent']['RejectApplicationContent'];
                    break;
            }
            
            $content = str_replace("[StudentChineseName]", $studentChineseNameWithClass, $content);
            $content = str_replace("[StudentEnglishName]", $studentEnglishNameWithClass, $content);
            $content = str_replace("[ApplyDate]", date('Y-m-d H:i', strtotime($dateInput)), $content);
            
            // send push message
            $leClassApp = new libeClassApp();
            if ($plugin['eClassApp'] && $leClassApp->isSchoolInLicense('P') && count($notifierIDAry)) {
                $appType = 'P';
                foreach ((array) $notifierIDAry as $_nid) {
                    $individualMessageInfoAry[0]['relatedUserIdAssoAry'][$_nid][] = $studentID;     // key - ParentID, value - StudentID
                }
                $content = intranet_undo_htmlspecialchars(strip_tags(str_replace("<br>", "\n", $content)));
                $pushResult = $notifyMessageId = $leClassApp->sendPushMessage($individualMessageInfoAry, $subject, $content, $isPublic = 'N', $pushMsgRecordStatus = 1, $appType);
                
            } else {
                $pushResult = false;
            }
            
            return ($pushResult) ? true : false;
        } else {
            return false;
        }
    }
    
    // get list of students that are taught by specific class teacher
    function getStudentsByClassTeacher($teacherID, $isTakingBus=true)
    {
        if ($teacherID) {
            $currentAcademicYearID = Get_Current_Academic_Year_ID();
            $userNameField = getNameFieldByLang("u.", $displayLang = "", $isTitleDisabled = true);
            $classNameField = 'ClassTitleEN'; // use English ClassName
            $studentName = "CONCAT($userNameField,IF(ycu.ClassNumber IS NULL OR ycu.ClassNumber='','',CONCAT(' (',yc.{$classNameField},'-',ycu.ClassNumber,')'))) ";
            
            $joinSql = '';
            if ($isTakingBus) {
                $joinSql .= " INNER JOIN INTRANET_SCH_BUS_USER_ROUTE ur ON ur.UserID=ycu.UserID AND ur.AcademicYearID=yc.AcademicYearID ";
            }
                
            $sql = "SELECT 		u.UserID,
                                {$studentName} AS StudentName
                    FROM
                                INTRANET_USER u
                    INNER JOIN
                                YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
                    INNER JOIN
                                YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                                AND yc.AcademicYearID='" . $currentAcademicYearID . "'
                    INNER JOIN
                                YEAR_CLASS_TEACHER yct ON yct.YearClassID=yc.YearClassID ".$joinSql."
					WHERE
								u.RecordType='2'
					            AND u.RecordStatus='1'
					            AND yct.UserID='".$teacherID."'
                    GROUP BY u.UserID
					ORDER BY yc.ClassTitleEN, ycu.ClassNumber+0";
            
            $retAry = $this->returnArray($sql);     // need to build array, so cannot just return resultSet
            return $retAry;
        }
        else {
            return false;
        }
    }
    
    // for IP, compare against ClassTitleEN, exclude left student
    public function getStudentNameListWClassNumberByClassName($className, $includeUserIdAry = '', $excludeUserIdAry = '', $assoc = false, $isTakingBus=true)
    {
//         global $junior_mck;
        
        $conds_userId = '';
        $recordstatus = "'1'";
        
        if ($includeUserIdAry != '') {
            $conds_userId .= " AND u.UserID IN ('" . implode("','", (array) $includeUserIdAry) . "') ";
        }
        
        if ($excludeUserIdAry != '') {
            $conds_userId .= " AND u.UserID NOT IN ('" . implode("','", (array) $excludeUserIdAry) . "') ";
        }
        
        $joinSql = '';
        if ($isTakingBus) {
            $joinSql .= " INNER JOIN INTRANET_SCH_BUS_USER_ROUTE ur ON ur.UserID=ycu.UserID AND ur.AcademicYearID=yc.AcademicYearID ";
        }
        
//         if ($junior_mck) {
//             $name_field = getNameFieldWithClassNumberByLang('u.');
//             $sql = "SELECT
//                             u.UserID,
//                             $name_field as StudentName
//                     FROM
//                             INTRANET_USER u
//                     WHERE
//                             u.RecordType = 2
//                             AND u.RecordStatus IN ($recordstatus)
//                             AND u.ClassName = '" . $this->Get_Safe_Sql_Query($className) . "'
//                             $conds_userId
//                     ORDER BY u.ClassNumber+0";
//         } else {
            $CurrentAcademicYearID = Get_Current_Academic_Year_ID();
            $username_field = getNameFieldByLang("u.", $displayLang = "", $isTitleDisabled = true);
            
            $classNameField = 'ClassTitleEN'; // use English ClassName
            $student_name = "CONCAT($username_field,IF(ycu.ClassNumber IS NULL OR ycu.ClassNumber='','',CONCAT(' (',yc.{$classNameField},'-',ycu.ClassNumber,')'))) ";
            
            $sql = "SELECT 		u.UserID,
                                {$student_name} AS StudentName
                    FROM
                                INTRANET_USER u
                    INNER JOIN
                                YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
                    INNER JOIN
                                YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                                AND yc.AcademicYearID='" . $CurrentAcademicYearID . "'
                                AND yc.ClassTitleEN='" . $this->Get_Safe_Sql_Query($className) . "' ".$joinSql."
					WHERE
								u.RecordType=2
								AND u.RecordStatus IN ($recordstatus)
								{$conds_userId}
                    GROUP BY u.UserID 
					ORDER BY ycu.ClassNumber+0";
//         }
        $rs = $assoc ? $this->returnResultSet($sql) : $this->returnArray($sql); // need returnArray for getSelectByArray function
        
        return $rs;
    }
    
    // get list of students of current year
    function getAllStudentOfCurrentYear($isTakingBus=true)
    {
        $joinSql = '';
        if ($isTakingBus) {
            $joinSql .= " INNER JOIN INTRANET_SCH_BUS_USER_ROUTE ur ON ur.UserID=ycu.UserID AND ur.AcademicYearID=yc.AcademicYearID ";
        }
        
        $currentAcademicYearID = Get_Current_Academic_Year_ID();
        $userNameField = getNameFieldByLang("u.", $displayLang = "", $isTitleDisabled = true);
        $classNameField = 'ClassTitleEN'; // use English ClassName
        $studentName = "CONCAT($userNameField,IF(ycu.ClassNumber IS NULL OR ycu.ClassNumber='','',CONCAT(' (',yc.{$classNameField},'-',ycu.ClassNumber,')'))) ";
        
        $sql = "SELECT 		u.UserID,
                            {$studentName} AS StudentName
                FROM
                            INTRANET_USER u
                INNER JOIN
                            YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
                INNER JOIN
                            YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
                            AND yc.AcademicYearID='" . $currentAcademicYearID . "' ".$joinSql."
				WHERE
							u.RecordType='2'
				            AND u.RecordStatus='1'
                GROUP BY u.UserID 
				ORDER BY yc.ClassTitleEN, ycu.ClassNumber+0";
        
        $retAry = $this->returnArray($sql);     // need to build array, so cannot just return resultSet
        
        return $retAry;
    }
    
    // $timeSlotAry = array('AM','PM');
    function addApplyLeaveRecord($studentID, $startDate, $startDateType, $endDate, $endDateType, $reason, $remark, $attendanceRecordID, $addedFrom, $availableTimeSlotAry, $targetTimeSlotAry, $attendanceApplyLeaveID='')
    {
        global $schoolBusConfig;
        
        $resultAry = array();
        $recordStatus = $schoolBusConfig['ApplyLeaveStatus']['Absent'];     // Absent
        $dataAry = array();
        $dataAry['StudentID']= $studentID;
        $dataAry['StartDate'] = $startDate;
        $dataAry['StartDateType'] = $startDateType;
        $dataAry['EndDate'] = $endDate;
        $dataAry['EndDateType'] = $endDateType;
        $dataAry['Reason'] = $reason;
        $dataAry['Remark'] = $remark;
        $dataAry['RecordStatus'] = $recordStatus;
        $dataAry['AddedFrom'] = $addedFrom;        // from attendance:  apply leave = 2 / absent = 3
        $dataAry['AttendanceRecordID'] = $attendanceRecordID;
        $dataAry['AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
        $dataAry['ApprovedBy'] = $_SESSION['UserID'];
        $dataAry['DateInput'] = 'now()';
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        
        $sql = $this->insert2Table('INTRANET_SCH_BUS_APPLY_LEAVE',$dataAry,array(),false, true);    // insert ignore
        
        $this->Start_Trans();
        $resultAry[] = $this->db_db_query($sql);
        $applicationID = $this->db_insert_id();
        
        foreach((array)$availableTimeSlotAry as $_date => $_timeSlotAry) {
            foreach((array)$_timeSlotAry as $__timeSlot=>$__val) {
                if (in_array($__timeSlot, (array)$targetTimeSlotAry)) {
                    $__timeSlotVal = $__timeSlot == 'AM' ? '1' : '2';
                    $sql = $this->addApplyLeaveSlotSql($applicationID, $_date, $__timeSlotVal, true, $recordStatus, $addedFrom, $attendanceRecordID, $attendanceApplyLeaveID);     // insert ignore
                    $resultAry[] = $this->db_db_query($sql);
                }
            }
        }
        
        if (!in_array(false,$resultAry)) {
            $this->Commit_Trans();
            $ret = true;
        }
        else {
            $this->RollBack_Trans();
            $ret = false;
        }
        return $ret;
    }
    
    // $attendanceApplyLeaveID=CARD_STUDENT_APPLY_LEAVE_RECORD.RecordID $attendanceRecordID=CARD_STUDENT_DAILY_LOG_{year}_{month}.RecordID
    // $addedFrom: 2 - for approve, 3 - for absent
    function synceSchoolBusApplyLeaveRecord($studentID, $recordDate, $startDateType, $endDateType, $reason, $remark, $attendanceRecordID, $timeSection, $addedFrom=2, $attendanceApplyLeaveID='')
    {
        global $schoolBusConfig;
        
        $nrTimeSection = count($timeSection);       // from eAttendance
        if ($nrTimeSection == 2) {
            $eAttendanceMode = 'WD';
        }
        else if ($nrTimeSection == 1) {
            $eAttendanceMode = $timeSection[0];     // AM or PM
        }
        else {
            $eAttendanceMode = '';
        }
        
        $retAry = array();
        if ($startDateType != '' && $eAttendanceMode) {
            
            $startDate = $recordDate;
            $endDate = $recordDate;
            
            // actually, just one day
            $availableTimeSlotAry = $this->getAvailableTimeSlotByDateRange($startDate, $endDate, $startDateType, $endDateType, $studentID);
            $numberOfTimeSlot = count($availableTimeSlotAry);

            if ($numberOfTimeSlot) {
                
                $recordStatusAry = array(
                    $schoolBusConfig['ApplyLeaveStatus']['Waiting'],
                    $schoolBusConfig['ApplyLeaveStatus']['Confirmed'],
                    $schoolBusConfig['ApplyLeaveStatus']['Absent']
                );
                
                $appliedeSchoolBusLeaveAry = $this->getApplyLeaveRecordByStudent($studentID, $startDate, $endDate, $recordStatusAry);

                if (count($appliedeSchoolBusLeaveAry)) {      // has applied leave in both school bus and attendance
                    $nreSchoolBusTimeSection = count($appliedeSchoolBusLeaveAry[$recordDate]);
                    if ($nreSchoolBusTimeSection == 2) {
                        $eSchoolBusMode = 'WD';
                    }
                    else if ($nreSchoolBusTimeSection == 1) {
                        $eSchoolBusMode = key($appliedeSchoolBusLeaveAry[$recordDate]);
                    }
                    else {
                        $eSchoolBusMode = null;
                    }
                }
                else {
                    $eSchoolBusMode = null;     // not applied eSchoolBus leave
                }

                $_amApplicationID = '';
                $_amStatus = '';
                $_pmApplicationID = '';
                $_pmStatus = '';
                foreach((array)$appliedeSchoolBusLeaveAry[$recordDate] as $_timeSection => $_appliedeSchoolBusLeaveAry) {   // at most two loops: AM & PM
                    if ($_timeSection == 'AM') {
                        $_amApplicationID = $_appliedeSchoolBusLeaveAry['ApplicationID'];
                        $_amStatus = $_appliedeSchoolBusLeaveAry['RecordStatus'];
                    }
                    else {
                        $_pmApplicationID = $_appliedeSchoolBusLeaveAry['ApplicationID'];
                        $_pmStatus = $_appliedeSchoolBusLeaveAry['RecordStatus'];
                    }
                }

                $excludeStatusAry = array(
                    $schoolBusConfig['ApplyLeaveStatus']['Confirmed'],
                    $schoolBusConfig['ApplyLeaveStatus']['Absent']
                );
                
                // eSchoolBus & eAttendance apply leave case
                if ($eSchoolBusMode == 'AM') {
                    // update status
                    if (!in_array($_amStatus, $excludeStatusAry)) {
                        $toStatus = $this->getApplyLeaveStatusByAction($_amStatus);

                        switch ($_amStatus) {
                            case $schoolBusConfig['ApplyLeaveStatus']['Waiting']:
                                $conditionAry = array('ApplicationID'=>$_amApplicationID);
                                $retAry[] = $this->updateSchoolBusApplyLeaveStatus($toStatus, $conditionAry, $attendanceRecordID, $attendanceApplyLeaveID);
                                $conditionAry['LeaveDate'] = $recordDate;
                                $retAry[] = $this->updateSchoolBusTimeSlotApplyLeaveStatus($toStatus, $conditionAry, $attendanceRecordID, $attendanceApplyLeaveID);
                                break;
                            default:
                                // add am not take record
                                $targetTimeSlotAry = array('AM');
                                $startDateType = 'AM';
                                $endDateType = 'AM';      
                                $retAry[] = $this->addApplyLeaveRecord($studentID, $startDate, $startDateType, $endDate, $endDateType, $reason, $remark, $attendanceRecordID, $addedFrom, $availableTimeSlotAry, $targetTimeSlotAry, $attendanceApplyLeaveID);
                                break;
                        }
                    }
                    
                    // add pm not take record
                    if ($eAttendanceMode == 'PM' || $eAttendanceMode == 'WD') {
                        $targetTimeSlotAry = array('PM');
                        $startDateType = 'PM';
                        $endDateType = 'PM';
                        $retAry[] = $this->addApplyLeaveRecord($studentID, $startDate, $startDateType, $endDate, $endDateType, $reason, $remark, $attendanceRecordID, $addedFrom, $availableTimeSlotAry, $targetTimeSlotAry, $attendanceApplyLeaveID);
                    }
                }
                elseif ($eSchoolBusMode == 'PM') {
                    // update status
                    if (!in_array($_pmStatus, $excludeStatusAry)) {
                        $toStatus = $this->getApplyLeaveStatusByAction($_pmStatus);

                        switch ($_pmStatus) {
                            case $schoolBusConfig['ApplyLeaveStatus']['Waiting']:
                                $conditionAry = array('ApplicationID'=>$_pmApplicationID);
                                $retAry[] = $this->updateSchoolBusApplyLeaveStatus($toStatus, $conditionAry, $attendanceRecordID, $attendanceApplyLeaveID);
                                $conditionAry['LeaveDate'] = $recordDate;
                                $retAry[] = $this->updateSchoolBusTimeSlotApplyLeaveStatus($toStatus, $conditionAry, $attendanceRecordID, $attendanceApplyLeaveID);
                                break;
                            default:
                                // add pm not take record
                                $targetTimeSlotAry = array('PM');
                                $startDateType = 'PM';
                                $endDateType = 'PM';                                
                                $retAry[] = $this->addApplyLeaveRecord($studentID, $startDate, $startDateType, $endDate, $endDateType, $reason, $remark, $attendanceRecordID, $addedFrom, $availableTimeSlotAry, $targetTimeSlotAry, $attendanceApplyLeaveID);
                                break;
                        }
                    }
                    
                    // add am not take record
                    if ($eAttendanceMode == 'AM' || $eAttendanceMode == 'WD') {
                        $targetTimeSlotAry = array('AM');
                        $startDateType = 'AM';
                        $endDateType = 'AM';                        
                        $retAry[] = $this->addApplyLeaveRecord($studentID, $startDate, $startDateType, $endDate, $endDateType, $reason, $remark, $attendanceRecordID, $addedFrom, $availableTimeSlotAry, $targetTimeSlotAry, $attendanceApplyLeaveID);
                    }
                }
                elseif ($eSchoolBusMode == 'WD') {
                    if (!in_array($_amStatus, $excludeStatusAry)) {
                        $toStatus = $this->getApplyLeaveStatusByAction($_amStatus);

                        switch ($_amStatus) {
                            case $schoolBusConfig['ApplyLeaveStatus']['Waiting']:           // AM status = PM status
                                $conditionAry = array('ApplicationID'=>$_amApplicationID);  // AM ApplicationID = PM ApplicationID (same record)
                                $retAry[] = $this->updateSchoolBusApplyLeaveStatus($toStatus, $conditionAry, $attendanceRecordID, $attendanceApplyLeaveID);
                                $conditionAry['LeaveDate'] = $recordDate;
                                $retAry[] = $this->updateSchoolBusTimeSlotApplyLeaveStatus($toStatus, $conditionAry, $attendanceRecordID, $attendanceApplyLeaveID);
                                break;
                            default:
                                // add am & pm not take record
                                $targetTimeSlotAry = array('AM', 'PM');
                                $retAry[] = $this->addApplyLeaveRecord($studentID, $startDate, $startDateType, $endDate, $endDateType, $reason, $remark, $attendanceRecordID, $addedFrom, $availableTimeSlotAry, $targetTimeSlotAry, $attendanceApplyLeaveID);
                                break;
                        }
                    }
                }
                else {      // $eSchoolBusMode = null
                    if ($eAttendanceMode == 'AM') {
                        $targetTimeSlotAry = array('AM');
                        $startDateType = 'AM';
                        $endDateType = 'AM';                        
                        $retAry[] = $this->addApplyLeaveRecord($studentID, $startDate, $startDateType, $endDate, $endDateType, $reason, $remark, $attendanceRecordID, $addedFrom, $availableTimeSlotAry, $targetTimeSlotAry, $attendanceApplyLeaveID);
                    }
                    elseif ($eAttendanceMode == 'PM') {
                        $targetTimeSlotAry = array('PM');
                        $startDateType = 'PM';
                        $endDateType = 'PM';                        
                        $retAry[] = $this->addApplyLeaveRecord($studentID, $startDate, $startDateType, $endDate, $endDateType, $reason, $remark, $attendanceRecordID, $addedFrom, $availableTimeSlotAry, $targetTimeSlotAry, $attendanceApplyLeaveID);
                    }
                    else {      // WD
                        $targetTimeSlotAry = array('AM','PM');
                        $retAry[] = $this->addApplyLeaveRecord($studentID, $startDate, $startDateType, $endDate, $endDateType, $reason, $remark, $attendanceRecordID, $addedFrom, $availableTimeSlotAry, $targetTimeSlotAry, $attendanceApplyLeaveID);
                    }
                }
                
            }   // time slot exist (non-holiday, non Sat & non Sun)
            else {
                // do nothing
            }
        }       // startDateType exist
        return !(in_array(false, $retAry)) ? true : false;
    }
    
    // $timeSection = array('AM','PM')
    function syncApplyLeaveRecordFromAttendance($attendanceRecordID, $recordDate, $remark, $timeSection)
    {
        global $schoolBusConfig, $sys_custom;
        
        $ret = true;
        if ($attendanceRecordID) {
            $sql = "SELECT
                            RecordID, StudentID, Reason
                    FROM
                            CARD_STUDENT_APPLY_LEAVE_RECORD
                    WHERE
                            RecordID='".$attendanceRecordID."'";
            $applyLeaveRecordAry =  $this->returnResultSet($sql);
            if (count($applyLeaveRecordAry)) {
                $applyLeaveRecordAry = current($applyLeaveRecordAry);
                
                $attendanceRecordID = '';
                $attendanceApplyLeaveID = $applyLeaveRecordAry['RecordID'];
                $studentID = $applyLeaveRecordAry['StudentID'];
                $reason = $applyLeaveRecordAry['Reason'];
                
                $startDateType = '';
                $classTimeMode = $this->getClassTimeMode('',$studentID);        // from class setting
                
                $nrSection = count($timeSection);
                if ($nrSection == 2) {
                    $startDateType = 'WD';
                    $endDateType = 'WD';
                }
                else if ($nrSection == 1) {
                    $selectedTimeMode = $timeSection[0];
                    switch ($classTimeMode) {
                        case 'WD':
                            if ($sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly']) {
                                $startDateType = 'AM';      
                                $endDateType = 'PM';   
                            }
                            else {
                                $startDateType = $selectedTimeMode;
                                $endDateType = $selectedTimeMode;
                            }
                            break;
                        default:        // AM or PM
                            // regarded as WD even selected wrong timesection
                            $startDateType = 'AM';      // go to school (to school)
                            $endDateType = 'PM';        // left school (from school)
                            break;
                    }
                }
                else {  // no selection
                    // stop process
                }
                
                if ($startDateType != '') {
                    $ret = $this->synceSchoolBusApplyLeaveRecord($studentID, $recordDate, $startDateType, $endDateType, $reason, $remark, $attendanceRecordID, $timeSection, $addedFrom=2, $attendanceApplyLeaveID);
                }       // startDateType exist
            }           // number of record > 0
        }               // $attendanceRecordID > 0
        
        return $ret;
        
    }               // end function syncApplyLeaveRecordFromAttendance
    
    
    // $attendanceType: 1-absent, 3-early
    function syncAbsentRecordFromAttendance($studentID, $recordDate, $reason, $teacherRemark, $dayTypeStr, $attendanceRecordID, $attendanceType=1)
    {
        global $schoolBusConfig, $sys_custom;
        
        $ret = true;
        $startDateType = '';
        $timeSection = array($dayTypeStr);

        $classTimeMode = $this->getClassTimeMode('',$studentID);
        if ($classTimeMode != 'WD') {           // AM or PM mode (half day class)
            if ($attendanceType == 3) {         // early leave
                $startDateType = 'PM';              
                $endDateType = 'PM';
                $timeSection = array('PM');
            }
            else {
                $startDateType = 'WD';              // apply leave is regarded as whole day
                $endDateType = 'WD';
                $timeSection = array('AM','PM');    // full day
            }
        }
        else {
            if ($sys_custom['eSchoolBus']['syncAttendance']['fromSchoolOnly']) {
                if ($attendanceType == 1) {
                    $startDateType = 'WD';
                    $endDateType = 'WD';
                    $timeSection = array('AM','PM');    // full day
                }
                else {
                    $startDateType = $dayTypeStr;
                    $endDateType = $dayTypeStr;
                }
            }
            else {
                $startDateType = $dayTypeStr;
                $endDateType = $dayTypeStr;
            }
        }
            
        if ($startDateType != '') {
            $ret = $this->synceSchoolBusApplyLeaveRecord($studentID, $recordDate, $startDateType, $endDateType, $reason, $teacherRemark, $attendanceRecordID, $timeSection, $addedFrom=3);
        }
        
        return $ret;
        
    }   // end function syncAbsentRecordFromAttendance
    
    
    /**
     *  return AM, PM or WD, for current year class only
     */
    function getClassTimeMode($yearClassID='', $studentID='')
    {
        $ret = '';
        if ($yearClassID || $studentID) {
            $currentAcademicYearID = Get_Current_Academic_Year_ID();
            $sql = "SELECT
                            UPPER(ycg.Code) AS Code
                    FROM
                            YEAR_CLASS_GROUP ycg
                    INNER JOIN
                            YEAR_CLASS yc ON yc.ClassGroupID=ycg.ClassGroupID
                    INNER JOIN
                            YEAR_CLASS_USER ycu ON ycu.YearClassID=yc.YearClassID AND yc.AcademicYearID='" . $currentAcademicYearID . "'
        		    WHERE 1";
            
            if ($yearClassID) {
                $sql .= " AND yc.YearClassID='".$yearClassID."'";
            }
            
            if ($studentID) {
                $sql .= " AND ycu.UserID='".$studentID."'";
            }
            $sql .= " LIMIT 1";
            
            $retAry = $this->returnResultSet($sql);
            if (count($retAry)){
                $ret = $retAry[0]['Code'];
            }
        }
        return $ret;
    }
    
    
    function updateSchoolBusApplyLeaveStatus($status, $conditionAry, $attendanceRecordID='', $attendanceApplyLeaveID='')
    {
        $dataAry = array();
        $dataAry['RecordStatus'] = $status;
        if ($attendanceRecordID != ''){
            $dataAry['AttendanceRecordID'] = $attendanceRecordID;
        }
        if ($attendanceApplyLeaveID != '') {
            $dataAry['AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
        }
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        
        $ret = array();
        if (count($conditionAry)) {
            $sql = $this->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE',$dataAry,$conditionAry,false);
            $ret[] = $this->db_db_query($sql);
        }
        return in_array(false,$ret) ? false : true;
    }

    function updateSchoolBusTimeSlotApplyLeaveStatus($status, $conditionAry, $attendanceRecordID='', $attendanceApplyLeaveID='')
    {
        $dataAry = array();
        $dataAry['RecordStatus'] = $status;
        if ($attendanceRecordID != ''){
            $dataAry['AttendanceRecordID'] = $attendanceRecordID;
        }
        if ($attendanceApplyLeaveID != '') {
            $dataAry['AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
        }
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];

        $ret = array();
        if (count($conditionAry)) {
            $sql = $this->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT',$dataAry,$conditionAry,false);
            $ret[] = $this->db_db_query($sql);
        }
        return in_array(false,$ret) ? false : true;
    }

    function updateSchoolBusAttendanceRecordID($attendanceRecordID, $conditionAry)
    {
        global $schoolBusConfig;
        
        $condition = '';
        $tmp_cond = array();
        if (! empty($conditionAry)) {
            foreach ($conditionAry as $field => $value) {
                $tmp_cond[] = "`{$field}` = '" . $this->Get_Safe_Sql_Query($value) . "'";
            }
            $condition = " AND " . implode(' AND ', $tmp_cond);
        }
        
        $recordStatusAry = array(
            $schoolBusConfig['ApplyLeaveStatus']['Waiting'],
            $schoolBusConfig['ApplyLeaveStatus']['Confirmed'],
            $schoolBusConfig['ApplyLeaveStatus']['Absent']
        );

        $ret = true;
        if ($condition != '') {
            $sql = "UPDATE 
                        INTRANET_SCH_BUS_APPLY_LEAVE 
                SET 
                        AttendanceRecordID='" . $attendanceRecordID . "', 
                        ModifiedBy='" . $_SESSION['UserID'] . "',
                        DateModified=NOW()
                WHERE
                        RecordStatus IN ('" . implode("','", $recordStatusAry) . "')"
                . $condition;
            $ret = $this->db_db_query($sql);
        }
        return $ret;
    }

    function updateSchoolBusTimeSlotAttendanceRecordID($studentID, $attendanceRecordID, $conditionAry)
    {
        global $schoolBusConfig;

        $condition = '';
        $tmp_cond = array();
        if (! empty($conditionAry)) {
            foreach ($conditionAry as $field => $value) {
                $tmp_cond[] = "`{$field}` = '" . $this->Get_Safe_Sql_Query($value) . "'";
            }
            $condition = " AND " . implode(' AND ', $tmp_cond);
        }

        $recordStatusAry = array(
            $schoolBusConfig['ApplyLeaveStatus']['Waiting'],
            $schoolBusConfig['ApplyLeaveStatus']['Confirmed'],
            $schoolBusConfig['ApplyLeaveStatus']['Absent']
        );

        $ret = true;
        if ($condition != '') {
            $sql = "UPDATE 
                        INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT t
                INNER JOIN
                        INTRANET_SCH_BUS_APPLY_LEAVE a ON a.ApplicationID=t.ApplicationID 
                SET 
                        t.AttendanceRecordID='" . $attendanceRecordID . "', 
                        t.ModifiedBy='" . $_SESSION['UserID'] . "',
                        t.DateModified=NOW()
                WHERE
                        t.RecordStatus IN ('" . implode("','", $recordStatusAry) . "')
                        AND a.StudentID='" . $studentID . "'"
                . $condition;
            $ret = $this->db_db_query($sql);
        }
        return $ret;
    }

    function updateSchoolBusApplyLeaveToDelete($conditionAry)
    {
        global $schoolBusConfig;
        
        $ret = true;
        if (! empty($conditionAry)) {
            $tmp_cond = array();
            $sql = "SELECT ApplicationID FROM INTRANET_SCH_BUS_APPLY_LEAVE ";
            foreach ($conditionAry as $field => $value) {
                $tmp_cond[] = "`{$field}` = '" . $this->Get_Safe_Sql_Query($value) . "'";
            }
            $sql .= " WHERE " . implode(' AND ', $tmp_cond);
            $sql .= " AND     (AddedFrom IN(2,3) OR (AddedFrom=1 AND (AttendanceRecordID>0 OR AttendanceApplyLeaveID>0)))";
            $sql .= " ORDER BY DateInput DESC LIMIT 2";         // retrieve the latest record (at most two: AM,PM)
            $leaveAry = $this->returnResultSet($sql);
            if (count($leaveAry)) {
                $applicationIDAry = Get_Array_By_Key($leaveAry,'ApplicationID');
                if (count($applicationIDAry)) {
                    $sql = "UPDATE INTRANET_SCH_BUS_APPLY_LEAVE SET
                                    RecordStatus='".$schoolBusConfig['ApplyLeaveStatus']['Deleted']."',
                                    DateModified=now(),
                                    ModifiedBy='".$_SESSION['UserID']."'
                            WHERE ApplicationID IN ('".implode("','",$applicationIDAry)."')";
                    $ret = $this->db_db_query($sql);
                }
            }
        }

        return $ret;
    }

    function updateSchoolBusTimeSlotApplyLeaveToDelete($studentID, $conditionAry)
    {
        global $schoolBusConfig;

        $ret = true;
        $condition = '';
        $tmp_cond = array();
        if (! empty($conditionAry)) {
            foreach ($conditionAry as $field => $value) {
                $tmp_cond[] = "{$field} = '" . $this->Get_Safe_Sql_Query($value) . "'";
            }
            $condition = " AND " . implode(' AND ', $tmp_cond);
        }

        if ($condition != '') {
            $sql = "UPDATE 
                    INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT t
            INNER JOIN
                    INTRANET_SCH_BUS_APPLY_LEAVE a ON a.ApplicationID=t.ApplicationID 
            SET 
                    t.RecordStatus='" . $schoolBusConfig['ApplyLeaveStatus']['Deleted'] . "', 
                    t.ModifiedBy='" . $_SESSION['UserID'] . "',
                    t.DateModified=NOW()
            WHERE
                    a.StudentID='" . $studentID . "'";

            $sql .= " AND (t.AddedFrom IN(2,3) OR (t.AddedFrom=1 AND (t.AttendanceRecordID>0 OR t.AttendanceApplyLeaveID>0))) ";
            $sql .=  $condition;
            $ret = $this->db_db_query($sql);
        }
        return $ret;
    }

    function approveAppliedLeave($applicationID, $remark='', $recordStatus='', $isPushToParent=true)
    {
        global $schoolBusConfig;
        
        $resultAry = array();
        $dataAry = array();
        $conditionAry = array();
        
        if ($remark != '') {
            $dataAry['Remark'] = $remark;
        }
        $dataAry['RecordStatus'] = $recordStatus ? $recordStatus : $schoolBusConfig['ApplyLeaveStatus']['Confirmed'];
        $dataAry['ApprovedBy'] = $_SESSION['UserID'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        
        $conditionAry['ApplicationID'] = $applicationID;
        
        $this->Start_Trans();
        $sql = $this->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE',$dataAry,$conditionAry,false);
        $resultAry[] = $this->db_db_query($sql);

        unset($dataAry);
        $dataAry['RecordStatus'] = $recordStatus ? $recordStatus : $schoolBusConfig['ApplyLeaveStatus']['Confirmed'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        $sql = $this->update2Table('INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT',$dataAry,$conditionAry,false);
        $resultAry[] = $this->db_db_query($sql);

        if (!in_array(false,$resultAry)) {
            $this->Commit_Trans();
            if ($isPushToParent) {
                $notifyResult = $this->pushApplyLeaveResultToParent($applicationID);
            }
            return true;
        }
        else {
            $this->RollBack_Trans();
            return false;
        }
    }
  
    // action = approve or absent, both use the same logic, therefore ignore the parameter
    function getApplyLeaveStatusByAction($statusBefore) 
    {
        global $schoolBusConfig;
        
        switch ($statusBefore) {
            case $schoolBusConfig['ApplyLeaveStatus']['Confirmed']:
                $statusAfter = $schoolBusConfig['ApplyLeaveStatus']['Confirmed'];
                break;
            default:
                $statusAfter = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                break;
        }
        return $statusAfter;
    }
    
    function getClassBuilding($date='')    
    {
        if ($date == '') {
            $date = date('Y-m-d');            
        }
        $aytAry = getAcademicYearAndYearTermByDate($date);
        $academicYearID = $aytAry['AcademicYearID'];
        
        $buildingName = Get_Lang_Selection('NameChi', 'NameEng');
        $sql = "SELECT      DISTINCT b.BuildingID, b.".$buildingName." 
                FROM 
                            INVENTORY_LOCATION_BUILDING b
                INNER JOIN 
                            YEAR_CLASS yc ON yc.BuildingID=b.BuildingID
                WHERE
                            yc.AcademicYearID='".$academicYearID."'
                ORDER BY b.NameEng";
        
        $buildingAry = $this->returnArray($sql);        // use returnArray for creating selection list
        return $buildingAry;
    }
    
    // active record only
    function getStudentTakingNormalRoute($academicYearID,$date,$timeSlot='PM',$buildingID='',$yearClassIDAry='',$classGroupIDAry='')
    {
        $className = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
        $studentName = getNameFieldByLang2('iu.');
        $buildingName = Get_Lang_Selection('b.NameChi','b.NameEng');
        $buildingIDCond = $buildingID ? " AND yc.BuildingID='".$buildingID."' " : "";
        
        $condYearClassID = "";
        if ($yearClassIDAry) {
            if (!is_array($yearClassIDAry)) {
                $yearClassIDAry = array($yearClassIDAry);
            }
            $condYearClassID .= " AND yc.YearClassID IN (".implode(',', $yearClassIDAry).") ";
        }
        
        if ($classGroupIDAry) {
            if (!is_array($classGroupIDAry)) {
                $classGroupIDAry= array($classGroupIDAry);
            }
            $condYearClassID .= " AND yc.ClassGroupID IN (".implode(',', $classGroupIDAry).") ";
        }
        
        if ($timeSlot == 'WD') {
            $condTimeSlot = " AND rc.AmPm IN ('AM','PM') ";    
        }
        else {
            $condTimeSlot = " AND rc.AmPm='".$timeSlot."' ";
        }
        
        $sql = "
            SELECT
                    CONCAT(".$className.", ' ', ".$studentName.") AS Student,
                    ur.UserID,
                    rc.AmPm AS TimeSlot,
                    r.RouteID,
                    r.RouteName,
                    v.VehicleID,
                    v.CarNum,
                    ".$buildingName." AS BuildingName,
                    yc.YearClassID,
                    ".$className." as ClassName,
                    ".$studentName." as StudentName
            FROM
                    INTRANET_SCH_BUS_USER_ROUTE as ur
                    INNER JOIN INTRANET_USER AS iu ON iu.UserID=ur.UserID
                    INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=iu.UserID
                    INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$academicYearID."'".$buildingIDCond."
                    LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=yc.BuildingID
                    LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteCollectionID=ur.RouteCollectionID
                    LEFT JOIN INTRANET_SCH_BUS_ROUTE as r ON r.RouteID=rc.RouteID AND r.IsActive='1' AND r.Type='N' AND r.AcademicYearID='".$academicYearID."'
                    LEFT JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP as rvm ON rvm.RouteID=r.RouteID AND rvm.IsActive='1'
                    LEFT JOIN INTRANET_SCH_BUS_VEHICLE as v ON v.VehicleID=rvm.VehicleID AND v.RecordStatus='1'
            WHERE
                    ur.AcademicYearID='".$academicYearID."'".$condYearClassID.$condTimeSlot."
            AND     rc.IsDeleted='0'
            AND     ur.DateStart<='".$date."'
            AND     ur.DateEnd>='".$date."'
            AND     rvm.StartDate<='".$date."'
            AND     rvm.EndDate>='".$date."'
            ORDER BY ".$className.", ycu.ClassNumber";
        $normalRouteAry = $this->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($normalRouteAry);
        return $normalRouteAry;
    }
    
    function getStudentTakingASARoute($academicYearID,$date,$buildingID='',$yearClassIDAry='',$classGroupIDAry='')
    {
        $className = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
        $studentName = getNameFieldByLang2('iu.');
        $weekDay = strtoupper(date('D',strtotime($date)));
        $buildingName = Get_Lang_Selection('b.NameChi','b.NameEng');
        $buildingIDCond = $buildingID ? " AND yc.BuildingID='".$buildingID."' " : "";
        
        $condYearClassID = "";
        if ($yearClassIDAry) {
            if (!is_array($yearClassIDAry)) {
                $yearClassIDAry = array($yearClassIDAry);
            }
            $condYearClassID .= " AND yc.YearClassID IN (".implode(',', $yearClassIDAry).") ";
        }

        if ($classGroupIDAry) {
            if (!is_array($classGroupIDAry)) {
                $classGroupIDAry= array($classGroupIDAry);
            }
            $condYearClassID .= " AND yc.ClassGroupID IN (".implode(',', $classGroupIDAry).") ";
        }
        
        $sql = "
            SELECT
                    CONCAT(".$className.", ' ', ".$studentName.") AS Student,
                    ur.UserID,
                    r.RouteID,
                    r.RouteName,
                    v.VehicleID,
                    v.CarNum,
                    ".$buildingName." AS BuildingName,
                    ".$className." as ClassName,
                    ".$studentName." as StudentName
            FROM
                    INTRANET_SCH_BUS_USER_ROUTE as ur
                    INNER JOIN INTRANET_USER AS iu ON iu.UserID=ur.UserID
                    INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=iu.UserID
                    INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$academicYearID."'".$buildingIDCond."
                    LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=yc.BuildingID
                    LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteCollectionID=ur.RouteCollectionID AND rc.IsDeleted='0'
                    LEFT JOIN INTRANET_SCH_BUS_ROUTE as r ON r.RouteID=rc.RouteID AND r.IsActive='1' AND r.Type='S' AND r.AcademicYearID='".$academicYearID."'
                    LEFT JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP as rvm ON rvm.RouteID=r.RouteID AND rvm.IsActive='1'
                        AND     rvm.StartDate<='".$date."'
                        AND     rvm.EndDate>='".$date."'
                    LEFT JOIN INTRANET_SCH_BUS_VEHICLE as v ON v.VehicleID=rvm.VehicleID AND v.RecordStatus='1'
            WHERE
                    ur.AcademicYearID='".$academicYearID."'".$condYearClassID."
            AND     ur.DateStart<='".$date."'
            AND     ur.DateEnd>='".$date."'
            AND     ucase(ur.Weekday)='".$weekDay."'
            ORDER BY ".$className.", ycu.ClassNumber";
        $specialRouteAry = $this->returnResultSet($sql);
//debug_pr($sql);        
        return $specialRouteAry;
    }
    
    function getSpecialArrangementStudent($academicYearID,$date,$timeSlot='PM',$buildingID='',$yearClassIDAry='',$classGroupIDAry='')
    {
        $className = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
        $studentName = getNameFieldByLang2('iu.');
        $buildingName = Get_Lang_Selection('b.NameChi','b.NameEng');
        $buildingIDCond = $buildingID ? " AND yc.BuildingID='".$buildingID."' " : "";
        
        $condYearClassID = "";
        if ($yearClassIDAry) {
            if (!is_array($yearClassIDAry)) {
                $yearClassIDAry = array($yearClassIDAry);
            }
            $condYearClassID = " AND yc.YearClassID IN (".implode(',', $yearClassIDAry).") ";
        }

        if ($classGroupIDAry) {
            if (!is_array($classGroupIDAry)) {
                $classGroupIDAry= array($classGroupIDAry);
            }
            $condYearClassID .= " AND yc.ClassGroupID IN (".implode(',', $classGroupIDAry).") ";
        }
        
        if ($timeSlot == 'WD') {
            $condTimeSlot = " AND sa.DayType IN ('AM','PM') ";
        }
        else {
            $condTimeSlot = " AND sa.DayType='".$timeSlot."' ";
        }
        
        
        $sql = "
            SELECT
                    CONCAT(".$className.", ' ', ".$studentName.") AS Student,
                    sa.UserID,
                    sa.DayType AS TimeSlot,
                    r.RouteID,
                    r.RouteName,
                    v.VehicleID,
                    v.CarNum,
                    ".$buildingName." AS BuildingName,
                    ".$className." as ClassName,
                    ".$studentName." as StudentName
            FROM
                    INTRANET_SCH_BUS_SPEICAL_ARRANGEMENT as sa
                    INNER JOIN INTRANET_USER AS iu ON iu.UserID=sa.UserID
                    INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=iu.UserID
                    INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$academicYearID."'".$buildingIDCond."
                    LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=yc.BuildingID
                    LEFT JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteCollectionID=sa.RouteCollectionID AND rc.IsDeleted='0'
                    LEFT JOIN INTRANET_SCH_BUS_ROUTE as r ON r.RouteID=rc.RouteID AND r.IsActive='1' AND r.AcademicYearID='".$academicYearID."'
                    LEFT JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP as rvm ON rvm.RouteID=r.RouteID AND rvm.IsActive='1'
                            AND     rvm.StartDate<='".$date."'
                            AND     rvm.EndDate>='".$date."'
                    LEFT JOIN INTRANET_SCH_BUS_VEHICLE as v ON v.VehicleID=rvm.VehicleID AND v.RecordStatus='1'
            WHERE
                    sa.AcademicYearID='".$academicYearID."'".$condYearClassID."
            AND     sa.Date='".$date."'".$condTimeSlot."
            ORDER BY ".$className.", ycu.ClassNumber";
            
        $specialArrangementAry = $this->returnResultSet($sql);
//  debug_pr($sql);
//  debug_pr($specialArrangementAry);
        return $specialArrangementAry;
    }
  
    // default timeSlot = 2 (PM)
    function getApplyLeaveUserByDate($date, $recordStatus='', $timeSlot='2')
    {
        if ($date) {
            $condition = " WHERE s.LeaveDate='".$date."' AND s.TimeSlot='".$timeSlot."'";
            
            $statusCondition = '';
            if ($recordStatus) {
                $statusCondition .= " AND s.RecordStatus IN ('".implode("','",((array)$recordStatus))."')";
            }
            
            $sql = "SELECT  a.StudentID AS UserID,
                            a.Reason,
                            a.Remark,
                            s.RecordStatus,
                            a.ApplicationID
                FROM
                            INTRANET_SCH_BUS_APPLY_LEAVE a
                INNER JOIN
                            INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s ON s.ApplicationID=a.ApplicationID
                INNER JOIN	(SELECT
                                    MAX(a.ApplicationID) AS ApplicationID, a.StudentID, s.LeaveDate, s.TimeSlot, MAX(s.DateInput) AS DateInput
                			 FROM
                                    INTRANET_SCH_BUS_APPLY_LEAVE a
                			 INNER JOIN
                                    INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s ON s.ApplicationID=a.ApplicationID ".$condition ."
                			 GROUP BY a.StudentID, s.LeaveDate, s.TimeSlot
                            ) AS g ON g.ApplicationID=a.ApplicationID AND g.StudentID=a.StudentID AND g.LeaveDate=s.LeaveDate AND g.TimeSlot=s.TimeSlot AND g.DateInput=s.DateInput
                ".$condition . $statusCondition . " ORDER BY a.StudentID";
            
            $leaveUserAry = $this->returnResultSet($sql);
            
// debug_pr($sql);
// debug_pr($leaveUserAry);
            
            return $leaveUserAry;
        }
        else {
            return false;
        }
    }
    
    // $takeOrNot:  0 - all, 1 - take, 2 - not take
    function getStudentListByBus($date, $timeSlot='PM', $buildingID='', $takeOrNot='2', $classGroupIDAry='')
    {
        global $schoolBusConfig, $intranet_root;
        include_once ($intranet_root . "/includes/libcardstudentattend2.php");

        if ($date == '') {
            $date = date('Y-m-d');
        }

        $lattend = new libcardstudentattend2();
        $notNeedToTakeAttendanceYearClassIDAry = array();
        $notNeedToTakeAttendanceClassAry = $lattend->getClassListNotTakeAttendanceByDate($date);
        foreach((array)$notNeedToTakeAttendanceClassAry as $_notNeedToTakeAttendanceClassAry ) {
            $notNeedToTakeAttendanceYearClassIDAry[] = $_notNeedToTakeAttendanceClassAry[0];
        }

        $aytAry = getAcademicYearAndYearTermByDate($date);
        $academicYearID = $aytAry['AcademicYearID'];

        $presentUserCountAry = array();
        $absentUserCountAry = array();
        
        // get vehicle list
        $filterMap['RecordStatus'] = '1';      // valid vehicle
        $vehicleAry = $this->getVehicleRecords($filterMap);
        $vehicleAssoc = BuildMultiKeyAssoc($vehicleAry, 'VehicleID', $IncludedDBField = array('CarNum'));
        $vehicleIDIdxAry = array();
        foreach((array)$vehicleAry as $idx=>$_vehicleAry) {
            $_vehicleID = $_vehicleAry['VehicleID'];
            $vehicleIDIdxAry[$_vehicleID] = $idx;
            $presentUserCountAry[$_vehicleID] = 0;
            $absentUserCountAry[$_vehicleID] = 0;
        }
        
        
        // get student list that taking normal route bus
        $normalRouteAry = $this->getStudentTakingNormalRoute($academicYearID,$date,$timeSlot,$buildingID,$yearClassIDAry='',$classGroupIDAry);
        $normalRouteAssoc = BuildMultiKeyAssoc($normalRouteAry, array('UserID'));

        // get student list that taking special route bus
        $specialRouteAry = $this->getStudentTakingASARoute($academicYearID,$date,$buildingID,$yearClassIDAry='',$classGroupIDAry);
        $specialRouteAssoc = BuildMultiKeyAssoc($specialRouteAry, array('UserID'));

        // get special arrangement
        $specialArrangementAry = $this->getSpecialArrangementStudent($academicYearID,$date,$timeSlot,$buildingID,$yearClassIDAry='',$classGroupIDAry);
        $specialArrangementAssoc = BuildMultiKeyAssoc($specialArrangementAry, array('UserID'));
        
        $recordStatus = array(
            $schoolBusConfig['ApplyLeaveStatus']['Confirmed'],
            $schoolBusConfig['ApplyLeaveStatus']['Absent']
        );
        
        $applyLeaveUserAry = $this->getApplyLeaveUserByDate($date, $recordStatus);
        $appliedLeaveUserIDAry = Get_Array_By_Key($applyLeaveUserAry, 'UserID');

        $absentUserAry = array();
        $absentUserAssoc = array();         // not taking bus
        $presentUserAry = array();
        $presentUserAssoc = array();        // taking bus
        
        $presentUserAssoc = $normalRouteAssoc;

        foreach((array)$specialRouteAssoc as $_userID=>$_userAry) {
            $_vehicleID = $_userAry['VehicleID'];

            if ($_vehicleID == '') {        // picked by parent
                unset($presentUserAssoc[$_userID]);         // delete from taking list
            }
            else {
                $presentUserAssoc[$_userID] = $_userAry;    // overwrite normal route
            }
        }
        
        foreach((array)$specialArrangementAssoc as $_userID=>$_userAry) {
            $_vehicleID = $_userAry['VehicleID'];
            if ($_vehicleID == '') {        // picked by parent
                $absentUserAssoc[$_userID] = $presentUserAssoc[$_userID];   // set absent list
                unset($presentUserAssoc[$_userID]);         // delete from taking list
            }
            else {
                $presentUserAssoc[$_userID] = $_userAry;    // overwrite ASA and normal route
            }
        }

        foreach((array)$presentUserAssoc as $_userID=>$_userAry) {
            if ((in_array($_userID, $appliedLeaveUserIDAry)) || (in_array($_userAry['YearClassID'],(array)$notNeedToTakeAttendanceYearClassIDAry))) {
                $absentUserAssoc[$_userID] = $presentUserAssoc[$_userID];   // set absent list
                unset($presentUserAssoc[$_userID]);
            }
        }

        if ($takeOrNot == '0' || $takeOrNot == '1' ) {      // all or taking bus
            foreach((array)$presentUserAssoc as $_userID=>$_userAry) {
                $_vehicleID = $_userAry['VehicleID'];
                $_userName = $_userAry['Student'];
                $_buildingName = $_userAry['BuildingName'];
                
                if (!empty($_buildingName)) {
                    $vehicleAssoc[$_vehicleID]['BuildingName'] = $_buildingName;
                }
                
                foreach((array)$vehicleIDIdxAry as $__vehicleID=>$column) {
                    if ($__vehicleID == $_vehicleID) {
                        $row = $presentUserCountAry[$_vehicleID];
                        $presentUserAry[$row][$column] = $_userName;
                        $presentUserCountAry[$_vehicleID] = $row + 1;
                    }
                }
            }

            for ($i=0, $iMax=count($presentUserAry); $i<$iMax; $i++) {
                for ($j=0,$jMax=count($vehicleIDIdxAry); $j<$jMax; $j++) {
                    if (!isset($presentUserAry[$i][$j])) {
                        $presentUserAry[$i][$j] = '';        // set it to empty
                    }
                }
            }
        }
        
        if ($takeOrNot == '0' || $takeOrNot == '2' ) {      // all or not taking bus
            foreach((array)$absentUserAssoc as $_userID=>$_userAry) {
                $_vehicleID = $_userAry['VehicleID'];
                $_userName = $_userAry['Student'];
                $_buildingName = $_userAry['BuildingName'];
                
                if (!empty($_buildingName)) {
                    $vehicleAssoc[$_vehicleID]['BuildingName'] = $_buildingName;
                }
                
                foreach((array)$vehicleIDIdxAry as $__vehicleID=>$column) {
                    if ($__vehicleID == $_vehicleID) {
                        $row = $absentUserCountAry[$_vehicleID];
                        $absentUserAry[$row][$column] = $_userName;
                        $absentUserCountAry[$_vehicleID] = $row + 1;
                    }
                }
            }
            
            for ($i=0, $iMax=count($absentUserAry); $i<$iMax; $i++) {
                for ($j=0,$jMax=count($vehicleIDIdxAry); $j<$jMax; $j++) {
                    if (!isset($absentUserAry[$i][$j])) {
                        $absentUserAry[$i][$j] = '';        // set it to empty
                    }
                }
            }
            
        }
        
        $ret = array(   'present' => $presentUserAry,
                        'presentCount' => $presentUserCountAry,
                        'absent' => $absentUserAry,
                        'absentCount' => $absentUserCountAry,
                        'vehicleAssoc' => $vehicleAssoc
                    );
        
        return $ret;
    }
  
    function getStudentListWithClassBuilding($academicYearID, $buildingID='', $yearClassIDAry='', $withHtmlTag=true, $classGroupIDAry='')
    {
        $condYearClassID = "";
        if ($yearClassIDAry)
        {
            if (!is_array($yearClassIDAry)) {
                $yearClassIDAry = array($yearClassIDAry);
            }
            $condYearClassID .= " AND yc.YearClassID IN (".implode(',', $yearClassIDAry).") ";
        }
        
        if ($classGroupIDAry) {
            if (!is_array($classGroupIDAry)) {
                $classGroupIDAry= array($classGroupIDAry);
            }
            $condYearClassID .= " AND yc.ClassGroupID IN (".implode(',', $classGroupIDAry).") ";
        }
        
        $condBuilding = $buildingID ? " AND b.BuildingID='".$buildingID."' " : ""; 
        
        $userName = getNameFieldByLang2('u.');
        $archiveUserName = getNameFieldByLang2('au.');
        $asterisk = $withHtmlTag ? '<font style=\"color:red;\">*</font>' : '*';
        $className = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
        $buildingName = Get_Lang_Selection('b.NameChi','b.NameEng');
        
        $sql = "SELECT
                        ycu.UserID,
                        ycu.ClassNumber,
                        CASE
                            WHEN au.UserID IS NOT NULL THEN CONCAT('".$asterisk."', $archiveUserName)
                            WHEN u.RecordStatus = 3  THEN CONCAT('".$asterisk."', $userName)
                            ELSE $userName
                        END AS StudentName,".
                        $className." AS ClassName,".
                        $buildingName." AS BuildingName,
                        yc.YearClassID
                FROM
                        YEAR_CLASS_USER AS ycu
                        INNER JOIN YEAR_CLASS AS yc ON (ycu.YearClassID = yc.YearClassID)
                        LEFT JOIN INTRANET_USER AS u ON (ycu.UserID = u.UserID)
                        LEFT JOIN INTRANET_ARCHIVE_USER AS au ON (ycu.UserID = au.UserID)
                        LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=yc.BuildingID                        
                WHERE   yc.AcademicYearID='".$academicYearID."' 
                        ".$condYearClassID."
                        ".$condBuilding."
                ORDER BY
                        yc.Sequence, yc.ClassTitleEN, ycu.ClassNumber";
        $studentAry = $this->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($studentAry);
        return $studentAry;
    }
    
    function getToSchoolUser($studentAry, $appliedLeaveUserIDAry, $appliedLeaveUserAssoc, $normalRouteAssoc, $specialArrangementAssoc)
    {
        global $Lang;
        
        $userID = $studentAry['UserID'];
        $studentAry['TimeSlot'] = $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];
        $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];        // default not take
        $studentAry['RouteName'] = '';          // default empty route
        $studentAry['Remark'] = '';             // default apply leave remark
        
        if (in_array($userID, $appliedLeaveUserIDAry)) {       // applied leave
            $studentAry['Remark'] = $appliedLeaveUserAssoc[$userID]['Remark'];
        }

        if ($normalRouteAssoc[$userID]['AM']) {
            $studentAry['RouteName'] = $normalRouteAssoc[$userID]['AM']['RouteName'];
            $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['Take'];
        }
        
        // no ASA route because it's AM (To School)
        if ($specialArrangementAssoc[$userID]['AM']) {
            if ($specialArrangementAssoc[$userID]['AM']['RouteID'] != '') {        // not pick by parent, overwrite normal route
                $studentAry['RouteName'] = $specialArrangementAssoc[$userID]['AM']['RouteName'];
                $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['Take'];
            }
            else {
                if ($studentAry['TakeOrNot'] == $Lang['eSchoolBus']['Report']['ByClass']['Take']) { // pick by parent, reset it to not take and empty route
                    $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];
                    if (!in_array($userID, $appliedLeaveUserIDAry)) {
                        $studentAry['RouteName'] = '';
                    }
                }
            }
        }
        
        if (in_array($userID, $appliedLeaveUserIDAry)) {
            $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];     // overwrite all the above status
        }
        
        return $studentAry;
    }

    function getFromSchoolUser($studentAry, $appliedLeaveUserIDAry, $appliedLeaveUserAssoc, $normalRouteAssoc, $specialArrangementAssoc, $specialRouteAssoc)
    {
        global $Lang;
        
        $userID = $studentAry['UserID'];
        $studentAry['TimeSlot'] = $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];
        $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];        // default not take
        $studentAry['RouteName'] = '';          // default empty route
        $studentAry['Remark'] = '';             // default apply leave remark
        
        if (in_array($userID, $appliedLeaveUserIDAry)) {       // applied leave
            $studentAry['Remark'] = $appliedLeaveUserAssoc[$userID]['Remark'];
        }
        
        if ($normalRouteAssoc[$userID]['PM']) {
            $studentAry['RouteName'] = $normalRouteAssoc[$userID]['PM']['RouteName'];
            $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['Take'];
        }
        
        if ($specialRouteAssoc[$userID]) {
            if ($specialRouteAssoc[$userID]['RouteID'] != '') {        // not pick by parent, overwrite normal route
                $studentAry['RouteName'] = $specialRouteAssoc[$userID]['RouteName'];
                $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['Take'];
            }
            else {
                if ($normalRouteAssoc[$userID]['PM'] && $studentAry['TakeOrNot'] == $Lang['eSchoolBus']['Report']['ByClass']['Take']) { // pick by parent and have normal route, reset it to not take and empty route
                    $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];
                    if (!in_array($userID, $appliedLeaveUserIDAry)) {
                        $studentAry['RouteName'] = '';
                    }
                }
            }
        }
        
        if ($specialArrangementAssoc[$userID]['PM']) {
            if ($specialArrangementAssoc[$userID]['PM']['RouteID'] != '') {        // not pick by parent, overwrite special route or normal route
                $studentAry['RouteName'] = $specialArrangementAssoc[$userID]['PM']['RouteName'];
                $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['Take'];
            }
            else {
                if ($studentAry['TakeOrNot'] == $Lang['eSchoolBus']['Report']['ByClass']['Take']) { // pick by parent, reset it to not take
                    $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];
                }
            }
        }
    
        if (in_array($userID, $appliedLeaveUserIDAry)) {
            $studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];     // overwrite all the above status
        }
        
        return $studentAry;
    }
    
    function getStudentListByClass($date, $timeSlot='WD', $buildingID='', $takeOrNot='0', $hasRouteOrNot='A', $yearClassIDAry='', $classGroupIDAry='')
    {
        global $Lang, $schoolBusConfig, $intranet_root;
        include_once ($intranet_root . "/includes/libcardstudentattend2.php");

        if ($date == '') {
            $date = date('Y-m-d');
        }

        $lattend = new libcardstudentattend2();
        $notNeedToTakeAttendanceYearClassIDAry = array();
        $notNeedToTakeAttendanceClassAry = $lattend->getClassListNotTakeAttendanceByDate($date);
        foreach((array)$notNeedToTakeAttendanceClassAry as $_notNeedToTakeAttendanceClassAry ) {
            if (in_array($_notNeedToTakeAttendanceClassAry[0], (array)$yearClassIDAry)) {
                $notNeedToTakeAttendanceYearClassIDAry[] = $_notNeedToTakeAttendanceClassAry[0];
            }
        }
//debug_pr($notNeedToTakeAttendanceYearClassIDAry);

        $aytAry = getAcademicYearAndYearTermByDate($date);
        $academicYearID = $aytAry['AcademicYearID'];
        
        $studentAry = $this->getStudentListWithClassBuilding($academicYearID, $buildingID, $yearClassIDAry, $withHtmlTag=true, $classGroupIDAry);

        // get student list that taking normal route bus
        $normalRouteAry = $this->getStudentTakingNormalRoute($academicYearID,$date,$timeSlot,$buildingID,$yearClassIDAry,$classGroupIDAry);
        $normalRouteAssoc = BuildMultiKeyAssoc($normalRouteAry, array('UserID', 'TimeSlot'));
        
        // get student list that taking special route bus
        if ($timeSlot != 'AM') {
            $specialRouteAry = $this->getStudentTakingASARoute($academicYearID,$date,$buildingID,$yearClassIDAry,$classGroupIDAry);
            $specialRouteAssoc = BuildMultiKeyAssoc($specialRouteAry, array('UserID'));
        }
        
        // get special arrangement
        $specialArrangementAry = $this->getSpecialArrangementStudent($academicYearID,$date,$timeSlot,$buildingID,$yearClassIDAry,$classGroupIDAry);
        $specialArrangementAssoc = BuildMultiKeyAssoc($specialArrangementAry, array('UserID', 'TimeSlot'));
// debug_pr($normalRouteAssoc);
// debug_pr($specialRouteAssoc);
// debug_pr($specialArrangementAssoc);
        $takeBusUserAry = array();
        $notTakeBusUserAry = array();
        foreach((array)$studentAry as $_studentAry ) {
            $_userID = $_studentAry['UserID'];
            $_yearClassID = $_studentAry['YearClassID'];
            $isTakeBusUser = false;

            if ($timeSlot == 'WD') {
                if ($normalRouteAssoc[$_userID]['AM'] || $normalRouteAssoc[$_userID]['PM'] || $specialRouteAssoc[$_userID]['RouteID']
                    || $specialArrangementAssoc[$_userID]['AM']['RouteID'] || $specialArrangementAssoc[$_userID]['PM']['RouteID']) {
                    $takeBusUserAry[] = $_studentAry;
                    $isTakeBusUser = true;
                }
            } else if ($timeSlot == 'AM') {
                if ($normalRouteAssoc[$_userID]['AM'] || $specialRouteAssoc[$_userID]['RouteID'] || $specialArrangementAssoc[$_userID]['AM']['RouteID']) {
                    $takeBusUserAry[] = $_studentAry;
                    $isTakeBusUser = true;
                }
            } else {
                if ($normalRouteAssoc[$_userID]['PM'] || $specialRouteAssoc[$_userID]['RouteID'] || $specialArrangementAssoc[$_userID]['PM']['RouteID']) {
                    $takeBusUserAry[] = $_studentAry;
                    $isTakeBusUser = true;
                }
            }

            if (!$isTakeBusUser) {
                $notTakeBusUserAry[] = $_studentAry;
            }
        }

        if ($hasRouteOrNot == 'Y') {
            $studentAry = $takeBusUserAry;
        }
        else if ($hasRouteOrNot == 'N') {
            $studentAry = $notTakeBusUserAry;
        }
        else {
            // do nothing
        }
        $numberOfStudent = count($studentAry);

        $recordStatus = array(
            $schoolBusConfig['ApplyLeaveStatus']['Confirmed'],
            $schoolBusConfig['ApplyLeaveStatus']['Absent']
        );
        
  
        $retStudentAry = array();
        
        switch ($timeSlot) {
            case 'WD':
                
                $applyAMLeaveUserAry = $this->getApplyLeaveUserByDate($date, $recordStatus, 1);   // AM
                $appliedAMLeaveUserIDAry = Get_Array_By_Key($applyAMLeaveUserAry, 'UserID');
                $appliedAMLeaveUserAssoc = BuildMultiKeyAssoc($applyAMLeaveUserAry, array('UserID'));

                $applyPMLeaveUserAry = $this->getApplyLeaveUserByDate($date, $recordStatus, 2);   // PM
                $appliedPMLeaveUserIDAry = Get_Array_By_Key($applyPMLeaveUserAry, 'UserID');
                $appliedPMLeaveUserAssoc = BuildMultiKeyAssoc($applyPMLeaveUserAry, array('UserID'));
                
                for($i=0; $i<$numberOfStudent;$i++) {
                    $_studentAry = $studentAry[$i];
                    $retStudentAry[] = $this->getToSchoolUser($_studentAry, $appliedAMLeaveUserIDAry, $appliedAMLeaveUserAssoc, $normalRouteAssoc, $specialArrangementAssoc);
                    $retStudentAry[] = $this->getFromSchoolUser($_studentAry, $appliedPMLeaveUserIDAry, $appliedPMLeaveUserAssoc, $normalRouteAssoc, $specialArrangementAssoc, $specialRouteAssoc);
                }
                
                break;
                
            case 'AM':
                $applyAMLeaveUserAry = $this->getApplyLeaveUserByDate($date, $recordStatus, 1);   // AM
                $appliedAMLeaveUserIDAry = Get_Array_By_Key($applyAMLeaveUserAry, 'UserID');
                $appliedAMLeaveUserAssoc = BuildMultiKeyAssoc($applyAMLeaveUserAry, array('UserID'));
                for($i=0; $i<$numberOfStudent;$i++) {
                    $_studentAry = $studentAry[$i];
                    $retStudentAry[] = $this->getToSchoolUser($_studentAry, $appliedAMLeaveUserIDAry, $appliedAMLeaveUserAssoc, $normalRouteAssoc, $specialArrangementAssoc);
                }
                break;
                
            case 'PM':
                $applyPMLeaveUserAry = $this->getApplyLeaveUserByDate($date, $recordStatus, 2);   // PM
                $appliedPMLeaveUserIDAry = Get_Array_By_Key($applyPMLeaveUserAry, 'UserID');
                $appliedPMLeaveUserAssoc = BuildMultiKeyAssoc($applyPMLeaveUserAry, array('UserID'));
                
                for($i=0; $i<$numberOfStudent;$i++) {
                    $_studentAry = $studentAry[$i];
                    $retStudentAry[] = $this->getFromSchoolUser($_studentAry, $appliedPMLeaveUserIDAry, $appliedPMLeaveUserAssoc, $normalRouteAssoc, $specialArrangementAssoc, $specialRouteAssoc);
                }
                break;
        }
//debug_pr($retStudentAry);
        if ($takeOrNot) {
            $filteredStudentAry = array();
            $langTakeOrNot = ($takeOrNot == 1) ? $Lang['eSchoolBus']['Report']['ByClass']['Take'] : $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];
            foreach((array)$retStudentAry as $_studentAry) {
                if (in_array($_studentAry['YearClassID'], (array)$notNeedToTakeAttendanceYearClassIDAry)) {
                    $_studentAry['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];
                }
                if ($_studentAry['TakeOrNot'] == $langTakeOrNot) {
                    $filteredStudentAry[] = $_studentAry;
                }
            }
            return $filteredStudentAry;
        }
        else {
            foreach((array)$retStudentAry as $k=>$_studentAry) {
                if (in_array($_studentAry['YearClassID'], (array)$notNeedToTakeAttendanceYearClassIDAry)) {
                    $retStudentAry[$k]['TakeOrNot'] = $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];
                }
            }
        }
        return $retStudentAry;
    }
    
    // current academic year only
    function isTakingBusStudent($studentID)
    {
        $currentAcademicYearID = Get_Current_Academic_Year_ID();
        if ($studentID) {
            $sql = "SELECT      UserRouteID
                    FROM
                                INTRANET_SCH_BUS_USER_ROUTE 
                    WHERE       UserID='".$studentID."'
                    AND         AcademicYearID='".$currentAcademicYearID."'
                    LIMIT 1";
            $retAry = $this->returnResultSet($sql);
            
            return count($retAry) ? true : false;
        }
        else {
            return false;
        }
    }
  
    // use date range to identify which bus in the route (Carlos: date range should not be overlapped)
    // Note: NrReserved is correct only when there's one route-one bus mapping
    function getRouteBusSeat($routeCollectionIDAry, $dateStart, $dateEnd, $academicYearID='')
    {
        if ($academicYearID == '') {
            $academicYearID = Get_Current_Academic_Year_ID();
        }
        if (!is_array($routeCollectionIDAry)) {
            $routeCollectionIDAry = array($routeCollectionIDAry);
        }
        
        $sql = "SELECT
                		COUNT(ur.RouteCollectionID) AS NrReserved,
                		v.Seat AS NrSeat,
                        rc.RouteCollectionID,
                        v.VehicleID,
                        rvm.StartDate,
                        rvm.EndDate
                FROM
                        INTRANET_SCH_BUS_VEHICLE AS v
                        INNER JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP AS rvm ON rvm.VehicleID=v.VehicleID AND rvm.IsActive='1'
                        INNER JOIN INTRANET_SCH_BUS_ROUTE AS r ON r.RouteID=rvm.RouteID AND r.IsActive='1'
                        INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION AS rc ON rc.RouteID=r.RouteID
                        LEFT JOIN (SELECT
                                            RouteCollectionID
                                    FROM
                                            INTRANET_SCH_BUS_USER_ROUTE
                                    WHERE
                                            AcademicYearID='".$academicYearID."'
                                            AND ((DateStart<='".$dateStart."' AND DateEnd>='".$dateStart."') OR
                                                 (DateStart<='".$dateEnd."' AND DateEnd>='".$dateEnd."') OR
                                                 (DateStart>='".$dateStart."' AND DateEnd<='".$dateEnd."') OR
                                                 (DateStart<='".$dateStart."' AND DateEnd>='".$dateEnd."')
                                                )
                                                     
                                    ) AS ur ON ur.RouteCollectionID=rc.RouteCollectionID
                WHERE
                	    rc.RouteCollectionID IN ('".implode("','",$routeCollectionIDAry)."')
                        AND rvm.StartDate<='".$dateStart."'
                        AND rvm.EndDate>='".$dateEnd."'
                GROUP BY rc.RouteCollectionID, v.VehicleID";
        
//                 AND rvm.StartDate<='".$dateEnd."'
//                 AND rvm.EndDate>='".$dateStart."'
//                 AND ur.DateStart<='".$dateStart."'
//                 AND ur.DateEnd>='".$dateEnd."'
                        
//debug_pr($sql);        
        $reservedAry = $this->returnResultSet($sql);
//debug_pr($reservedAry);
        $reservedAssoc = BuildMultiKeyAssoc($reservedAry, array('RouteCollectionID', 'VehicleID'));
        
        return $reservedAssoc;
    }
    
    function updateReasonAndRemark($studentID, $recordDate, $attendanceRecordID, $reason='', $remark='')
    {
        $sql = "UPDATE INTRANET_SCH_BUS_APPLY_LEAVE SET 
                        Reason='".$this->Get_Safe_Sql_Query($reason)."', 
                        Remark='".$this->Get_Safe_Sql_Query($remark)."',
                        DateModified=NOW(), 
                        ModifiedBy='".$_SESSION['UserID']."' 
                WHERE 
                        StudentID='".$studentID."' 
                        AND StartDate='".$recordDate."' 
                        AND AddedFrom IN ('2','3') 
                        AND AttendanceRecordID='".$attendanceRecordID."'";
        
        return $this->db_db_query($sql);
    }
    
    function getSelectedTeacher($routeID)
    {
        if (is_array($routeID)) {
            $cond = " t.RouteID IN ('".implode("','", $routeID)."')";
        }
        else if ($routeID) {
            $cond = " t.RouteID='".$routeID."'";
        }
        else {
            return false;
        }
        
        $name_field = getNameFieldByLang('u.');
        $sql = "SELECT 	u.UserID,
                        {$name_field} AS Name,
                        t.RouteID
                FROM
                        INTRANET_USER u
                INNER JOIN
                        INTRANET_SCH_BUS_ROUTE_TEACHER t ON t.TeacherID=u.UserID
                WHERE   {$cond}
                        ORDER BY Name";
        $rs = $this->returnResultSet($sql);
        return $rs;
    }
    
    // $teacherType = -1 denotes all teachers
    // get approved (RecordStatus=1) teacher only
    function getTeacher($teacherType = "-1", $conds = "")
    {
        if ($teacherType == 1) {
            $conds .= " AND Teaching=1";
        }
        else if ($teacherType == 0) {
            $conds .= " AND (Teaching IS NULL OR Teaching=0 OR Teaching='')";
        }
            
        $name_field = getNameFieldByLang();
        $sql = "SELECT UserID, $name_field as Name FROM INTRANET_USER WHERE RecordType=1 AND RecordStatus='1' $conds ORDER BY EnglishName";
        
        $rs = $this->returnArray($sql);
        return $rs;
    }

    function isSchoolBusApplyLeaveRecordExist($attendanceApplyLeaveID, $studentID, $leaveDate, $dayTypeStr='')
    {
        if ($dayTypeStr == 'AM') {
            $timeSlot = 1;
            $condition = " AND s.TimeSlot='".$timeSlot."'";
        }
        else if ($dayTypeStr == 'PM'){
            $timeSlot = 2;
            $condition = " AND s.TimeSlot='".$timeSlot."'";
        }
        else {
            $condition = "";
        }
        $sql = "SELECT 	a.ApplicationID
                FROM
                        INTRANET_SCH_BUS_APPLY_LEAVE a 
                INNER JOIN
                        INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s ON s.ApplicationID=a.ApplicationID
                WHERE  
                        a.StudentID='".$studentID."'
                    AND s.AttendanceApplyLeaveID='".$attendanceApplyLeaveID."'
                    AND s.RecordStatus='5'
                    AND s.AddedFrom<>'1'
                    AND s.LeaveDate='".$leaveDate."'
                    ".$condition." LIMIT 1";
        $rs = $this->returnResultSet($sql);
        
        return count($rs) ? true : false;
    }

    function getUserIDByClassGroup($classGroupIDAry, $academicYearID='') 
    {
        if (empty($academicYearID)) {
            $academicYearID= Get_Current_Academic_Year_ID();
        }
        
        $sql = "SELECT      
                            ycu.UserID
                FROM
                            YEAR_CLASS_USER ycu
                INNER JOIN
                            YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$academicYearID."'
                WHERE
                            yc.ClassGroupID IN ('".implode("','",(array)$classGroupIDAry)."')
                AND
                            yc.ClassGroupID>0
                ORDER BY
                            ycu.UserID";
        $userIDAry = $this->returnResultSet($sql);
        $userIDAry = Get_Array_By_Key($userIDAry, 'UserID');
        return $userIDAry;
    }
    
    function getTakingBusClassList($academicYearID='')
    {
        if (!$academicYearID) {
            $academicYearID = Get_Current_Academic_Year_ID();
        }
        
        $sql=  "SELECT
					   y.YearID,
					   y.YearName,
					   yc.YearClassID,
					   yc.ClassTitleEN,
					   yc.ClassTitleB5
			    FROM
                       YEAR_CLASS_USER ycu
                INNER JOIN
					   YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$academicYearID."'
			    INNER JOIN
					   YEAR y ON y.YearID = yc.YearID 
                INNER JOIN 
                       INTRANET_SCH_BUS_USER_ROUTE ur ON ur.UserID=ycu.UserID AND ur.AcademicYearID=yc.AcademicYearID
                WHERE
                       yc.AcademicYearID = '".$academicYearID."'
                GROUP BY
                       yc.YearClassID 
				ORDER BY
					   y.Sequence, y.YearName, yc.Sequence, yc.ClassTitleEN";
        
        $yearClassAry = $this->returnResultSet($sql);
        
        return $yearClassAry;
    }

    function getRecordStatusByApplication($applicationID)
    {
        $sql = "SELECT 	
                        TimeSlotID,
                        LeaveDate,
                        TimeSlot,
                        RecordStatus
                FROM
                        INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT 
                WHERE  
                        ApplicationID='".$applicationID."'
                        ORDER BY LeaveDate, TimeSlot";
        $rs = $this->returnResultSet($sql);

        return $rs;
    }

    function isRecordStatusConsistent($applicationID, $detailOnly=false)
    {
        $sql = "SELECT 
                        DISTINCT s.RecordStatus 
                FROM
                        INTRANET_SCH_BUS_APPLY_LEAVE a 
                INNER JOIN
                        INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT s ON s.ApplicationID=a.ApplicationID
                WHERE  
                        a.ApplicationID='".$applicationID."'";
        if (!$detailOnly) {
            $sql .= " AND a.RecordStatus<>s.RecordStatus";
            $rs = $this->returnResultSet($sql);
            $ret = (count($rs)) ? false : true;
        }
        else {
            $rs = $this->returnResultSet($sql);
            $ret = (count($rs) > 1) ? false : true;
        }

        return $ret;
    }

    // get the other TimeSlotID of the same date for the same application by a givent TimeSlotID
    function getTheOtherTimeSlotOfTheDate($timeSlotID)
    {
        $sql = "SELECT 	
                        b.TimeSlotID,
                        b.LeaveDate,
                        b.ApplicationID,
                        b.TimeSlot,
                        b.RecordStatus
                FROM
                        INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT a
                        INNER JOIN INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT b ON b.ApplicationID=a.ApplicationID AND a.LeaveDate=b.LeaveDate
                WHERE  
                        a.TimeSlotID='".$timeSlotID."'
                        AND a.TimeSlot<>b.TimeSlot";
        $rs = $this->returnResultSet($sql);

        return $rs;
    }

    function getApplicationIDByTimeSlotID($timeSlotID)
    {
        $sql = "SELECT 	
                        ApplicationID
                FROM
                        INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT 
                WHERE  
                        TimeSlotID='".$timeSlotID."'
                        ";
        $rs = $this->returnResultSet($sql);
        return count($rs) ? $rs[0]['ApplicationID'] : '';
    }

    function getYearClassIDByUserID($studentID)
    {
        $academicYearID= Get_Current_Academic_Year_ID();

        $sql = "SELECT      
                            yc.YearClassID
                FROM
                            YEAR_CLASS_USER ycu
                INNER JOIN
                            YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='".$academicYearID."'
                WHERE
                            ycu.UserID='".IntegerSafe($studentID)."'";
        $yearClassIDAry = $this->returnResultSet($sql);
        return count($yearClassIDAry) ? $yearClassIDAry[0]['YearClassID'] : '';
    }

}

?>