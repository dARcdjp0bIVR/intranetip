<?php
## Editing by: 

$MenuGroup['management']=$Lang['eSchoolBus']['ManagementArr']['MenuTitle'];
$MenuGroup['report']=$Lang['eSchoolBus']['ReportArr']['MenuTitle'];
$MenuGroup['settings']=$Lang['eSchoolBus']['SettingsArr']['MenuTitle'];


$MenuArr['management']['Attendance']['Title'] = $Lang['eSchoolBus']['ManagementArr']['AttendanceArr']['MenuTitle'];
$MenuArr['management']['Attendance']['URL'] = 'management/attendance';
$MenuArr['management']['SpecialArrangement']['Title'] = $Lang['eSchoolBus']['ManagementArr']['SpecialArrangementArr']['MenuTitle'];
$MenuArr['management']['SpecialArrangement']['URL'] = 'management/specialArrangement';

$MenuArr['report']['ClassList']['Title'] = $Lang['eSchoolBus']['ReportArr']['ClassListArr']['MenuTitle'];
$MenuArr['report']['ClassList']['URL'] = 'report/classList';
$MenuArr['report']['RouteArrangement']['Title'] = $Lang['eSchoolBus']['ReportArr']['RouteArrangementArr']['MenuTitle'];
$MenuArr['report']['RouteArrangement']['URL'] = 'report/routeArrangement';
$MenuArr['report']['StudentList']['Title'] = $Lang['eSchoolBus']['ReportArr']['StudentListArr']['MenuTitle'];
$MenuArr['report']['StudentList']['URL'] = 'report/studentList';
$MenuArr['report']['AsaContact']['Title'] = $Lang['eSchoolBus']['ReportArr']['AsaContactArr']['MenuTitle'];
$MenuArr['report']['AsaContact']['URL'] = 'report/asaContact';
// $MenuArr['report']['AsaRoute']['Title'] = $Lang['eSchoolBus']['ReportArr']['AsaRouteArr']['MenuTitle'];
// $MenuArr['report']['AsaRoute']['URL'] = 'report/asaRoute';
$MenuArr['report']['PickUp']['Title'] = $Lang['eSchoolBus']['ReportArr']['PickUpArr']['MenuTitle'];
$MenuArr['report']['PickUp']['URL'] = 'report/pickup';
$MenuArr['report']['StudentListByBus']['Title'] = $Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['MenuTitle'];
$MenuArr['report']['StudentListByBus']['URL'] = 'report/studentListByBus';
$MenuArr['report']['StudentListByClass']['Title'] = $Lang['eSchoolBus']['ReportArr']['StudentListByClassArr']['MenuTitle'];
$MenuArr['report']['StudentListByClass']['URL'] = 'report/studentListByClass';

if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSchoolBus"] && $_SESSION['SSV_PRIVILEGE']['plugin']['eSchoolBus'])
{

$MenuArr['settings']['BusStops']['Title'] =$Lang['eSchoolBus']['SettingsArr']['BusStopsArr']['MenuTitle'] ;
$MenuArr['settings']['BusStops']['URL'] = 'settings/busStops';
$MenuArr['settings']['Route']['Title'] = $Lang['eSchoolBus']['SettingsArr']['RouteArr']['MenuTitle'] ;
$MenuArr['settings']['Route']['URL'] = 'settings/route';
$MenuArr['settings']['Vehicle']['Title'] = $Lang['eSchoolBus']['SettingsArr']['VehicleArr']['MenuTitle'] ;
$MenuArr['settings']['Vehicle']['URL'] = 'settings/vehicle';
$MenuArr['settings']['Students']['Title'] = $Lang['eSchoolBus']['SettingsArr']['StudentsArr']['MenuTitle'] ;
$MenuArr['settings']['Students']['URL'] = 'settings/students';
$MenuArr['settings']['CutoffTime']['Title'] = $Lang['eSchoolBus']['SettingsArr']['CutoffTimeArr']['MenuTitle'] ;
$MenuArr['settings']['CutoffTime']['URL'] = 'settings/cutoffTime';

}
?>