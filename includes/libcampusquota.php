<?php
// Editing by 
/*
 * 2017-05-22 (Carlos): Modified returnUsedQuota($forceRecalculate=0), added optional $forceRecalculate parameter to force recalculate used quota.
 * 2017-03-06 (Carlos): Modified returnUsedQuota(), only SUM attachment size when quota cache record does not exist. 
 */
class libcampusquota extends libdb{

      var $quota;
      var $uid;
      var $msg;
      var $QuotaArray;
      var $quota_used;

      function libcampusquota($UserID="")
      {
               global $intranet_root;
               $this->libdb();
               $this->quota = "";
               $this->uid = $UserID;
               global $intranet_root, $_SESSION;
              if ($_SESSION['campusmail_set_content']=="")
              {
              	$file_content = get_file_content ("$intranet_root/file/campusmail_set.txt");
              	$_SESSION['campusmail_set_content'] = (trim($file_content)=="")?"NULL_CONTENT":$file_content;
              } else
              {
              	$file_content = ($_SESSION['campusmail_set_content']=="NULL_CONTENT") ? "":$_SESSION['campusmail_set_content'];
              }
               if ($file_content == "")
               {
                   $this->QuotaArray = array(10,10,10);
               }
               else
               {
                   $fquota = explode("\n", $file_content);
                   $teacher = explode(":",$fquota[0]);
                   $student = explode(":",$fquota[1]);
                   $parent = explode(":",$fquota[3]);
                   $this->QuotaArray = array($teacher[1],$student[1],$parent[1]);
               }
      }

      function returnQuota()
      {
               if ($this->quota == "")
               {
                   global $special_feature;
                   if ($special_feature['imail'])
                   {
                       $this->quota = $this->retrieveiMailQuota();
                   }
                   else $this->quota = $this->retrieveQuota();
               }
               return $this->quota;
      }

      # 1 - teacher/staff, 2 - student, 3 - parent
      function getQuotaForUserType($type)   # Not used
      {
               if ($type < 1 || $type > 3) return 0;
               return $this->QuotaArray[$type-1];
      }
      function retrieveQuota()          # CampusMail version
      {
               if ($this->uid == "") return 0;    # default
               $sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '".$this->uid."' ";
               $result = $this->returnVector($sql);
               if (sizeof($result)==0) return 0;
               return $this->getQuotaForUserType($result[0]);
      }

      function retrieveiMailQuota()              # iMail version
      {
               if ($this->uid == "") return 0;    # default

               $sql = "SELECT Quota FROM INTRANET_CAMPUSMAIL_USERQUOTA WHERE UserID = '".$this->uid."' ";
               $temp = $this->returnVector($sql);
               return ($temp[0]+0);
      }

      function returnUsedQuota($forceRecalculate=0)
      {
      		global $sys_custom;
      		
               if ($this->quota_used!="")
               {
                   return $this->quota_used;
               }
               
               if(!$sys_custom['iMail']['RecalculateUsedQuota'] && !$forceRecalculate)
               {
	               $sql = "SELECT QuotaUsed FROM INTRANET_CAMPUSMAIL_USED_STORAGE WHERE UserID = '".$this->uid."' ";
	               $quota_record = $this->returnResultSet($sql);
	               if(count($quota_record)>0){
	               	  $this->quota_used = round($quota_record[0]['QuotaUsed'],1);
	               }else{
	               	  $sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID = ".$this->uid."' ";
	               	  $sum_record = $this->returnVector($sql);
	               	  $this->quota_used = round(intval($sum_record[0]),1);
	               	  $sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed, RequestUpdate) VALUES ('".$this->uid."','".$this->quota_used."','0')";
	               	  $this->db_db_query($sql);
	               }
	               return $this->quota_used;
               }
               
               $sql = "SELECT SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL WHERE UserID = '".$this->uid."' ";
               $v = $this->returnVector($sql);
               if ($v[0]=="")
               {
                   $this->quota_used = 0;
               }
               else
               {
                   $this->quota_used = round($v[0],1);
               }
               return $this->quota_used;
      }


      function displayQuota ()
      {
               global $i_Campusquota_noLimit, $i_Campusquota_max, $i_Campusquota_used,$i_Campusquota_left,$i_Campusquota_using1,$i_Campusquota_using2;
               $total = $this->returnQuota() * 1024;
               $used = $this->returnUsedQuota();
               if ($total != 0)
                   $pused = round($used/$total * 100);
               else $pused = 100;
               $left = $total - $used;
               if ($left < 0) $left = 0;
               if ($pused > 100) $pused = 100;


               $total .= " Kb(s) ";
               $left .= " Kb(s) ";
               $used .= " Kb(s), $pused% ";
               $storage = "<TABLE width=80% align=left><tr><td style=\"font-size:10px;\">0%</td>";
               $storage .= "<td width=100% class=td_left_middle>
<table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td width=100% class=td_left_middle bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table>
</td>";
               $storage .= "<td style=\"font-size:10px;\">100%</td></tr>";
               $storage .= "</table>";
               $x .= "<table width=300 border=1 cellspacing=0 cellpadding=5 bordercolor=#555555 bgcolor=ACEFD5 class=decription>\n";
               $x .= "<tr><td><table width=100% border=0 cellspacing=1 cellpadding=0>";
               $x .= "<tr><td class=td_description align=right width=15%>$i_Campusquota_max </td><td class=td_description>:</td><td width=85% class=td_description> $total</td></tr>
                      <tr><td class=td_description align=right>$i_Campusquota_left</td><td class=td_description>:</td><td class=td_description> $left</td></tr>
                      <tr><td colspan=3 class=td_description>$storage </td></tr>
                      <tr><td class=td_description align=right>$i_Campusquota_used</td><td class=td_description>:</td><td class=td_description> $used</td></tr>
                      <tr><td colspan=3 class=td_description>$i_Campusquota_using1 $pused% $i_Campusquota_using2 </td></tr>
                         ";
               $x .= "</table></td></tr></table>\n";


               return $x;
      }

      function displayPercentageBar()
      {
               $total = $this->returnQuota() * 1024;
               $used = $this->returnUsedQuota();
               if ($total != 0)
                   $pused = round($used/$total * 100);
               else $pused = 100;
               $left = $total - $used;
               if ($left < 0) $left = 0;
               if ($pused > 100) $pused = 100;
               $x = "$pused %";
               return $x;
      }
      function displayiMailPercentageBar()
      {
               global $i_CampusMail_New_Quota1, $i_CampusMail_New_Quota2;
               $total = $this->returnQuota() * 1024;
               $used = $this->returnUsedQuota();
               if ($total != 0)
                   $pused = round($used/$total * 100);
               else $pused = 100;
               $left = $total - $used;
               if ($left < 0) $left = 0;
               if ($pused > 100) $pused = 100;

$storage = "<TABLE width=80% align=center><tr><td style=\"font-size:10px;\">0%</td>";
               $storage .= "<td width=100% class=td_left_middle>
<table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td width=100% class=td_left_middle bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table>
</td>";
$storage .= "<td style=\"font-size:10px;\">100%</td></tr>";
               $storage .= "</table>";

               $x = "<table width=150 border=0 cellspacing=0 cellpadding=0>
              <tr>
                <td class=td_center_bottom>
                $storage</td>
              </tr>
              <tr>
                <td align=center><font size=2>$i_CampusMail_New_Quota1 $pused% $i_CampusMail_New_Quota2</font></td>
              </tr>
            </table>";


               return $x;
      }

      function displayInformation()
      {
               global $i_Campusquota_max,$i_Campusquota_left,$i_Campusquota_used;
               $total = $this->returnQuota() * 1024;
               $used = $this->returnUsedQuota();
               if ($total != 0)
                   $pused = round($used/$total * 100);
               else $pused = 100;
               $left = $total - $used;
               if ($left < 0) $left = 0;
               if ($pused > 100) $pused = 100;
               $x = "$i_Campusquota_used $used kb ($i_Campusquota_max: $total kb) $i_Campusquota_left: $left kb";
               return $x;
      }

      function displayiMailInformation()
      {
               global $i_Campusquota_max,$i_Campusquota_left,$i_Campusquota_used;
               $total = $this->returnQuota() * 1024;
               $used = $this->returnUsedQuota();
               $left = $total - $used;
               if ($left < 0) $left = 0;
               $x = "<table width=80% border=1 cellspacing=0 cellpadding=3 bordercolordark=#DA8C61 bordercolorlight=#F2BF3E>
              <tr align=center valign=middle>
                <td bgcolor=#F2D13E><font color=#104A7B size=2>$i_Campusquota_used : $used kB ($i_Campusquota_max: $total kB)</font></td>
                <td bgcolor=#F2BF3E><font color=#104A7B size=2>$i_Campusquota_left : $left kB</font></td>
              </tr>
            </table>";
               return $x;
      }


	
	
            
}
?>