<?php
// Editing by:

/*
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Modification of this file may affect different modules.                          !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Please check carefully and consult team leaders after modifying it.              !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

###################################################### Change Log ######################################################
# Date      : 2020-11-16 (Ray)
# Detail    : Added ApiKeyVersion
#
# Date		: 2020-11-11 (Ray)
# Detail    : Modified UpdatePaymentCallback add OriginalAmount, NewAmount
#
# Date      : 2020-10-07 (Crystal)
# Detail    : Added api DeleteScheduledPushMessageFromCourse and modified SendPushMessageFromCourse
# Deploy    : ip.2.5.11.11.1
#
# Date      : 2020-08-11 (Ray)
# Detail    : Added ForgetPasswordVersion, ForgetPasswordV2
#
# Date      : 2020-07-09 (Ray)
# Detail    : Added SubmitBodyTemperatureRecord, GetStudentBodyTemperatureRecords
#
# Date      : 2020-07-02 (Ray)
# Detail    : Added WS_GetPaymentItemMerchantInfo ServiceProvider
#
# Date		: 2020-06-09 (Ray)
# Detail    : Added api GetSessionNumberForDate, modify SubmitApplyLeaveRecord
#
# Date      : 2020-02-18 (Thomas)
# Detail    : Added api SendPushMessageFromCourse
# Deploy    : ip.2.5.11.3.1
#
# Date		: 2019-10-29 (Carlos)
# Detail	: Added $method["UpdatePaymentCallback"]["Param"][13] = "DateInput";
# Deploy	: ip.2.5.10.12.1
#
# Date		: 2019-05-09 (Carlos)
# Detail	: Added iCalendar api method signatures WS_iCalendar*
#
# Date      : 2019-03-07 (Cameron)
# Detail    : add WS_DigitalChannelDeleteAlbum, WS_DigitalChannelDeletePhotosForAlbum
# Deploy	: ip.2.5.10.4.1
#
# Date		: 2019-01-25 (Carlos)
# Detail	: Modified UpdatePaymentCallback to add param StudentID, PaymentAmount. (Copied from EJ)
# Deploy    : ip.2.5.10.2.1
#
# Date      : 2019-01-23 (Cameron)
# Detail    : add WS_DigitalChannelUploadAlbumDetails, WS_DigitalChannelUploadPhotoDetails, WS_GetTargetGroupList, WS_GetCommonChooseGroups, 
#               WS_GetCommonChooseUsers, WS_DigitalChannelGetAlbumDetails, WS_DigitalChannelPhotoView
# Deploy	: ip.2.5.10.2.1
#
# Date      : 2019-01-15 (Tiffany)
# Detail    : Modified WS_GetAnnouncementForApp(), WS_GetSchoolEventForApp(), WS_GetDigitalChannelPhotoList() add parameter "ParLang" to support language change
# Deploy	: ip.2.5.10.1.1
#
# Date      : 2018-10-19 (Ivan)
# Detail    : Added methods SaveUserPhoto, UseOfficialPhotoAsPersonalPhoto and CancelOfficialPhotoAsPersonalPhoto
# Deploy	: ip.2.5.9.11.1
#
# Date      : 2018-10-19 (Ivan)
# Detail    : Added methods SaveUserPhoto, UseOfficialPhotoAsPersonalPhoto and CancelOfficialPhotoAsPersonalPhoto
# Deploy	: ip.2.5.9.11.1
#
# Date      : 2018-10-12 (Ivan)
# Detail    : Added methods GetUserOfficialPhotoPath and GetUserOfficialPersonalPath
# Deploy	: ip.2.5.9.11.1
#
# Date		: 20180912 (Tiffany)
# Detail	: Added method SubmitApplyLeaveRecord
# Deploy	: ip.2.5.9.10.1
#
# Date		: 20180717 (Carlos)
# Detail	: Added method GetPaymentItemMerchantInfo
# Deploy	: ip.2.5.9.10.1
#
# Date		: 20180717 (Carlos)
# Detail	: Added method GetPaymentItemMerchantInfo
# Deploy	: ip.2.5.9.10.1
#
# Date      : 20180606 (Ronald)CancelLeaveRecord
# Detail    : Add api
# Deploy    : ip.2.5.9.7.1.0
#
# Date		: 2018-02-01 (Carlos)
# Detail	: Added $method["NewHKUFluSickLeaveRecord"]["Param"][8] = "From";
# Deploy	: ip.2.5.9.3.1.0
#
# Date		: 20180116 (Ivan)
# Detail	: Modified GetMedicalCaringForTeacherApp to add param IntranetUserID
# Deploy    : ip.2.5.9.3.1.0
#
# Date      : 20171202 (Siuwan)
# Detail    : Added method PowerLesson
# Deploy    : ip.2.5.9.1.1.0
#
# Date      : 20170925 (Henry)
# Detail    : Added api GetCoursesTeachers
# Deploy    : ip.2.5.8.10.1.0
#
# Date      : 20170921 (Henry)
# Detail    : Added api GetCoursesUsers
# Deploy    : ip.2.5.8.10.1.0
#
# Date      : 20170920 (Ivan)
# Detail    : Add api ClearLicenseCache 
# Deploy    : ip.2.5.8.10.1.0
#
# Date      : 20170828 (Thomas)
# Detail    : Add api GetPL2LessonPlanUsersUdid 
# Deploy    : ip.2.5.8.9.1.0
#
# Date      : 20170821 (Henry)
# Detail    : Added api GetPL2PortalUserInfoWithAllCourses
# Deploy    : ip.2.5.8.9.1.0
#
# Date      : 20170815 (Tiffany)
# Detail    : Modified api GetNoticeUrlForApp
# Deploy    : ip.2.5.8.9.1.0
#
# Date      : 20170620 (Roy)
# Detail    : Added api GetTargetableUsersForMessaging
# Deploy    : ip.2.5.8.7.1.0
#
# Date      : 20170516 (Siuwan)
# Detail    : Added api IsPowerLessonCompatible
# Deploy    : ip.2.5.8.7.1.0
#
# Date      : 2017-05-04 (Tiffany)
# Detail    : Added api UpdateEmailForDHL
# Deploy    : ip.2.5.8.6.1.0
#
# Date      : 2017-05-02 (Tiffany)
# Detail    : Added api ForgetPasswordForDHL
# Deploy    : ip.2.5.8.6.1.0
#
# Date      : 2017-03-31 (Siuwan)
# Detail    : Added api GetEclassGroups / GetCourseUsers
# Deploy    : ip.2.5.8.4.1.0
#
# Date		: 2016-12-19 (Ivan)
# Detail	: Added api UpdatePaymentCallback
# Deploy	: ip.2.5.8.1.1.0
#
# Date		: 2016-11-11 (Ronald)
# Detail	: Added api CentennialAppFileUpload
# Deploy	: ip.2.5.7.10.1.0
#
# Date		: 20161110 (Henry HM)
# Details	: Add api LoginCentennial
# Deploy	: ip.2.5.7.10.1.0
#
# Date		: 20161013 (Pun)
# Details	: Add api RemoveAuthToken
# Deploy	: ip.2.5.7.10.1.0
#
# Date		: 20160906 (Pun)
# Details	: Add api RenewSessionLastUpdated
# Deploy	: ip.2.5.7.10.1.0
#
# Date		: 20160902 (Henry HM)
# Details	: Add api WS_IsHKUFluRecordExists
# Deploy	: ip.2.5.7.10.1.0
#
# Date		: 20160901 (Pun)
# Details	: Add api RenewAuthToken
# Deploy	: ip.2.5.7.10.1.0
#
# Date		: 20160820 (Pun)
# Details	: Add api GetCourseInfo
# Deploy	: ip.2.5.7.10.1.0
#
# Date		: 20160816 (Thomas)
# Details	: Add api AppRelaunchLessonValidate
# Deploy	: ip.2.5.7.10.1.0
#
# Date		: 20160809 (Pun)
# Details	: Add api WS_GetPL2PortalUserInfo
# Deploy	: ip.2.5.7.10.1.0
#
# Date		: 20160809 (Henry HM)
# Details	: Add api WS_GetHKUFluAgreementRecord, WS_NewHKUFluAgreementRecord, WS_GetHKUFluSickLeaveRecord, WS_NewHKUFluSickLeaveRecord
# Deploy	: 
#
# Date		: 20160606 (Tiffany)
# Details	: Add api WS_SubmitWeeklyDiaryRecord
# Deploy	: ip.2.5.7.7.1.0
#
# Date		: 20160530 (Siuwan)
# Details	: Add api WS_eClassGetLessonUsersUdid
# Deploy	: ip.2.5.7.7.1.0
#
# Date		: 20160512 (Tiffany)
# Details	: Add api WS_GetWeeklyDiaryForApp
# Deploy	: ip.2.5.7.7.1.0
#
# Date		: 20160125 (Tiffany)
# Details	: Add api WS_GetMedicalCaringForTeacherApp
# Deploy	: ip.2.5.7.1.1.0
#
# Date		: 20151203 (Tiffany)
# Details	: Add api WS_GetMedicalCaringForApp
# Deploy	: ip.2.5.7.1.1.0
#
# Date		: 20151112 (Ivan)
# Details	: for request UpdateScheduledPushMessageStatus added SendTime parameter;
# Deploy	: ip.2.5.7.1.1.0
#
# Date		: 20150710 (Qiao)
# Details	: Modified WS_UserChangePassword, add $method["UserChangePassword"]["Param"][1] = "OldPassword"
# Deploy	: ip.2.5.6.7.1.0
#
# Date		: 20150616 (Thomas)
# Details	: Modified $method["AppFileUploadByChunk"]["Param"][9], $method["AppFileUploadByChunk"]["Param"][10] & $method["AppFileUploadByChunk"]["Param"][11]
# Deploy	: ip.2.5.6.7.1.0
#
# Date		: 20150401 (Roy)
# Details	: add parameter "GeTuiClientID" to deleteAppAccountServerFollowUp()
# Deploy	: ip.2.5.6.3.1.0
#
# Date		: 20150320 (Qiao)
# Details	: modified function LibMethod() to add GetDigitalChannelPhotoList
# Deploy	: ip.2.5.6.3.1.0
#
# Date		: 20150311 (Ivan)
# Details	: Addded API "SendPushMessageToUser"
# Deploy	: ip.2.5.6.3.1.0
#
# Date		: 20150226 (Thomas)
# Details	: modified function LibMethod() to add AppCreateLessonPlanWithVideoKey
# Deploy	: ip.2.5.6.3.1.0
#
# Date		: 20150224 (Qiao)
# Details	: modified function LibMethod() to add GetSubjectListForApp
# Deploy	: ip.2.5.6.3.1.0
#
# Date		: 20150223 (Pun)
# Details	: modified function LibMethod() to add WPNSUri in saveUserDevice API for Windows Phone
# Deploy	: ??
#
# Date		: 20150126 (Ivan)
# Details	: modified function LibMethod() to add error code 410 for blacklist user 
# Deploy	: ip.2.5.6.3.1.0
#
# Date		: 20141107 (Jason)
# Details	: edit 'AppGetClassroom' to add 2 paramaters withActiveLessonClassroom, DisplayLang
# Deploy	: ip.2.5.6.1.1.0
#
# Date 		: 20100211 (Henry Y)
# Details 	: copy from IP25
#

class LibMethod {
	var $RequestMethod;
	var $Error;
	
	function LibMethod() {
		## Method
		
		# Begin Henry Chow (2011-07-19): get Announcement (School & Class announcements)
		$method["GetAnnouncementList"]["class"] = "libeclass_ws.php";
		$method["GetAnnouncementList"]["constructor"] = "libeclass_ws()";
		$method["GetAnnouncementList"]["method"] = "WS_GetAnnouncement";
		$method["GetAnnouncementList"]["Param"][0] = "UserName";	
		$method["GetAnnouncementList"]["Param"][1] = "NoOfRecords";		
		$method["GetAnnouncementList"]["Param"][2] = "TargetGroup";	
		# End Henry Chow (2011-07-19): get Announcement (School & Class announcements)
		
		# Begin YatWoon (2011-07-19): get event
		$method["GetEventList"]["class"] = "libeclass_ws.php";
		$method["GetEventList"]["constructor"] = "libeclass_ws()";
		$method["GetEventList"]["method"] = "WS_GetEvent";
		$method["GetEventList"]["Param"][0] = "UserName";	
		$method["GetEventList"]["Param"][1] = "NoOfRecords";		
		# End YatWoon (2011-07-19): get event
		
		# Begin Henry Chow (2011-07-19): get student notice (include discipline notice, payment notice & notices of other modules)
		$method["GetNoticeList"]["class"] = "libeclass_ws.php";
		$method["GetNoticeList"]["constructor"] = "libeclass_ws()";
		$method["GetNoticeList"]["method"] = "WS_GetNotice";
		$method["GetNoticeList"]["Param"][0] = "UserName";		
		$method["GetNoticeList"]["Param"][1] = "NoOfRecords";
		# End Henry Chow (2011-07-19): get student notice (include discipline notice, payment notice & notices of other modules)
		
		
		# Begin YatWoon (2011-07-19): get student balance and outstanding payment item
		$method["GetUserSmartCardBalance"]["class"] = "libeclass_ws.php";
		$method["GetUserSmartCardBalance"]["constructor"] = "libeclass_ws()";
		$method["GetUserSmartCardBalance"]["method"] = "WS_GetPayment";
		$method["GetUserSmartCardBalance"]["Param"][0] = "UserName";		
		# End YatWoon (2011-07-19): get student balance and outstanding payment item
		
		/*
		# Begin Henry Yuen (2011-07-14): get student info
		$method["GetEvent"]["class"] = "libeclass_ws.php";
		$method["GetEvent"]["constructor"] = "libeclass_ws()";
		$method["GetEvent"]["method"] = "WS_GetEvent";
		$method["GetEvent"]["Param"][0] = "UserID";		
		$method["GetEvent"]["Param"][1] = "Name";		
		# End Henry Yuen (2011-07-14): get student info	
		*/
		
		# Begin Henry Yuen (2011-07-14): get student info
		$method["GetStudentInfo"]["class"] = "libeclass_ws.php";
		$method["GetStudentInfo"]["constructor"] = "libeclass_ws()";
		$method["GetStudentInfo"]["method"] = "WS_GetStudentInfo";
		$method["GetStudentInfo"]["Param"][0] = "UserID";		
		$method["GetStudentInfo"]["Param"][1] = "UserName";
		# End Henry Yuen (2011-07-14): get student info	
		
		# Begin Henry Yuen (2011-07-13): get a list of student of parent
		# Joe (2011-08-23): or return the studnet himself if it's a student id/login 
		$method["GetChildrenList"]["class"] = "libeclass_ws.php";
		$method["GetChildrenList"]["constructor"] = "libeclass_ws()";
		$method["GetChildrenList"]["method"] = "WS_GetChildrenList";
		$method["GetChildrenList"]["Param"][0] = "UserID";	
		$method["GetChildrenList"]["Param"][1] = "UserName";
		# End Henry Yuen (2011-07-06): get a list of student of parent		
		
		# Begin Henry Yuen (2011-07-06): get assignment list -- specially designed for ASL
		$method["GetHomeworkList"]["class"] = "libeclass_ws.php";
		$method["GetHomeworkList"]["constructor"] = "libeclass_ws()";
		$method["GetHomeworkList"]["method"] = "WS_GetHomeworkList";
		$method["GetHomeworkList"]["Param"][0] = "CourseID";
		$method["GetHomeworkList"]["Param"][1] = "UserID";
		$method["GetHomeworkList"]["Param"][2] = "UserName";
		$method["GetHomeworkList"]["Param"][3] = "HwType";
		$method["GetHomeworkList"]["Param"][4] = "SubjectID";
		$method["GetHomeworkList"]["Param"][5] = "Timestamp";	
		$method["GetHomeworkList"]["Param"][6] = "AsgnID";				
		$method["GetHomeworkList"]["Param"][7] = "StartDate";		
		$method["GetHomeworkList"]["Param"][8] = "Status";		
		$method["GetHomeworkList"]["Param"][9] = "Deadline";
		# End Henry Yuen (2011-07-06): get assignment list -- specially designed for ASL
		
		# Begin Henry Yuen (2011-01-20): get user basic information
		$method["GetUserBasic"]["class"] = "libeclass_ws.php";
		$method["GetUserBasic"]["constructor"] = "libeclass_ws()";
		$method["GetUserBasic"]["method"] = "WS_GetUserBasic";
		$method["GetUserBasic"]["Param"][0] = "UserEmail";
		# End Henry Yuen (2011-01-20): get user basic information
				
		# Begin Henry Yuen (2010-12-22): GetIPortfolio
		$method["IPortfolio_RestoreFile"]["class"] = "libeclass_ws.php";
		$method["IPortfolio_RestoreFile"]["constructor"] = "libeclass_ws()";
		$method["IPortfolio_RestoreFile"]["method"] = "WS_IPortfolio_RestoreFile";
		$method["IPortfolio_RestoreFile"]["Param"][0] = "UserEmail";
		$method["IPortfolio_RestoreFile"]["Param"][1] = "ServerPath";
		$method["IPortfolio_RestoreFile"]["Param"][2] = "FileByteStream";
		$method["IPortfolio_RestoreFile"]["Param"][3] = "EcFileAttrs";
		# End Henry Yuen (2010-12-22): GetIPortfolio
		
		# Begin Henry Yuen (2010-12-22): GetIPortfolio
		$method["GetIPortfolio"]["class"] = "libeclass_ws.php";
		$method["GetIPortfolio"]["constructor"] = "libeclass_ws()";
		$method["GetIPortfolio"]["method"] = "WS_GetIPortfolio";
		$method["GetIPortfolio"]["Param"][0] = "UserEmail";
		$method["GetIPortfolio"]["Param"][1] = "Timestamp";
		# End Henry Yuen (2010-12-22): GetIPortfolio
		
		# Begin Henry Yuen (2010-10-28): GetConfig
		$method["GetConfig"]["class"] = "libeclass_ws.php";
		$method["GetConfig"]["constructor"] = "libeclass_ws()";
		$method["GetConfig"]["method"] = "WS_GetConfig";
		$method["GetConfig"]["Param"][0] = "UserName";
		$method["GetConfig"]["Param"][1] = "Password";		
		# End Henry Yuen (2010-10-28): GetConfig
		
		# Begin Henry Yuen (2010-07-16): Get Learning Center
		$method["GetLearningCentre"]["class"] = "libeclass_ws.php";
		$method["GetLearningCentre"]["constructor"] = "libeclass_ws()";
		$method["GetLearningCentre"]["method"] = "WS_GetLearningCentre";
		$method["GetLearningCentre"]["Param"][0] = "UserEmail";		
		# End Henry Yuen (2010-07-16): Get Learning Center	
		
		# Begin Henry Yuen (2010-06-25): Get Latest Version
		$method["GetLatestVersion"]["class"] = "libeclass_ws.php";
		$method["GetLatestVersion"]["constructor"] = "libeclass_ws()";
		$method["GetLatestVersion"]["method"] = "WS_GetLatestVersion";
		$method["GetLatestVersion"]["Param"][0] = "MainVersion";
		$method["GetLatestVersion"]["Param"][1] = "UpdaterVersion";
		# End Henry Yuen (2010-06-25): Get Latest Version
		
		# Begin Henry Yuen (2010-05-03): Get eBook Token
		$method["GetEBookToken"]["class"] = "libeclass_ws.php";
		$method["GetEBookToken"]["constructor"] = "libeclass_ws()";
		$method["GetEBookToken"]["method"] = "WS_GetEBookToken";
		$method["GetEBookToken"]["Param"][0] = "UserEmail";
		$method["GetEBookToken"]["Param"][1] = "BookID";
		$method["GetEBookToken"]["Param"][2] = "IsUnlimited";		
		# End Henry Yuen (2010-05-03): Get eBook Token			
		
		# Begin Henry Yuen (2010-05-03): Get eBook List  
		$method["GetEBookList"]["class"] = "libeclass_ws.php";
		$method["GetEBookList"]["constructor"] = "libeclass_ws()";
		$method["GetEBookList"]["method"] = "WS_GetEBookList";
		$method["GetEBookList"]["Param"][0] = "UserEmail";
		$method["GetEBookList"]["Param"][1] = "Timestamp";		
		# End Henry Yuen (2010-05-03): Get eBook List
				
		# 2011-07-28: Logout method
		$method["Logout"]["class"] = "libauth.php";
		$method["Logout"]["constructor"] = "libauth()";
		$method["Logout"]["method"] = "WS_Logout";
		$method["Logout"]["Param"][0] = "UserID";				
				
		## Method
		$method["Login"]["class"] = "libauth.php";
		$method["Login"]["constructor"] = "libauth()";
		$method["Login"]["method"] = "WS_Login";
		$method["Login"]["Param"][0] = "UserName";
		$method["Login"]["Param"][1] = "Password";
		$method["Login"]["Param"][2] = "hasAPIKey";
		$method["Login"]["Param"][3] = "fromEClassApp";
		
		$method["LoginCentennial"]["class"] = "libauth.php";
		$method["LoginCentennial"]["constructor"] = "libauth()";
		$method["LoginCentennial"]["method"] = "WS_Login_Centennial";
		$method["LoginCentennial"]["Param"][0] = "UserName";
		$method["LoginCentennial"]["Param"][1] = "Password";
		$method["LoginCentennial"]["Param"][2] = "hasAPIKey";
		$method["LoginCentennial"]["Param"][3] = "fromEClassApp";
		
		$method["GetUserCourse"]["class"] = "libeclass_ws.php";
		$method["GetUserCourse"]["constructor"] = "libeclass_ws()";
		$method["GetUserCourse"]["method"] = "WS_GetUserCourse";
		$method["GetUserCourse"]["Param"][0] = "UserEmail";
		
		$method["GetUserNoteList"]["class"] = "libeclass_ws.php";
		$method["GetUserNoteList"]["constructor"] = "libeclass_ws()";
		$method["GetUserNoteList"]["method"] = "WS_GetUserNoteList";
		$method["GetUserNoteList"]["Param"][0] = "CourseID";
		$method["GetUserNoteList"]["Param"][1] = "Timestamp";
		$method["GetUserNoteList"]["Param"][2] = "SelectType";
		
		$method["GetNoteSection"]["class"] = "libeclass_ws.php";
		$method["GetNoteSection"]["constructor"] = "libeclass_ws()";
		$method["GetNoteSection"]["method"] = "WS_GetNoteSection";
		$method["GetNoteSection"]["Param"][0] = "NotesSectionID";
		$method["GetNoteSection"]["Param"][1] = "CourseID";
		
		/*
		// not used anymore -- deprecated method
		$method["GeteContentUpdate"]["class"] = "libeclass_ws.php";
		$method["GeteContentUpdate"]["constructor"] = "libeclass_ws()";
		$method["GeteContentUpdate"]["method"] = "WS_GeteContentUpdate";
		$method["GeteContentUpdate"]["Param"][0] = "Timestamp";
		$method["GeteContentUpdate"]["Param"][1] = "CourseID";
		*/
		
		/*
		// not used anymore -- deprecated method
		$method["GetCourseNoteList"]["class"] = "libeclass_ws.php";
		$method["GetCourseNoteList"]["constructor"] = "libeclass_ws()";
		$method["GetCourseNoteList"]["method"] = "WS_GetCourseNoteList";
		$method["GetCourseNoteList"]["Param"][0] = "CourseID";				
		*/
		
		$method["GetCourseSubjectList"]["class"] = "libeclass_ws.php";
		$method["GetCourseSubjectList"]["constructor"] = "libeclass_ws()";
		$method["GetCourseSubjectList"]["method"] = "WS_GetCourseSubjectList";
		$method["GetCourseSubjectList"]["Param"][0] = "CourseID";						
		
		$method["GetAsgnList"]["class"] = "libeclass_ws.php";
		$method["GetAsgnList"]["constructor"] = "libeclass_ws()";
		$method["GetAsgnList"]["method"] = "WS_GetAsgnList";
		$method["GetAsgnList"]["Param"][0] = "CourseID";
		$method["GetAsgnList"]["Param"][1] = "UserEmail";
		$method["GetAsgnList"]["Param"][2] = "HwType";
		$method["GetAsgnList"]["Param"][3] = "SubjectID";
		$method["GetAsgnList"]["Param"][4] = "Timestamp";		
		
		# 2010-11-05:
		$method["GetAsgnMarkedFile"]["class"] = "libeclass_ws.php";
		$method["GetAsgnMarkedFile"]["constructor"] = "libeclass_ws()";
		$method["GetAsgnMarkedFile"]["method"] = "WS_GetAsgnMarkedFile";
		$method["GetAsgnMarkedFile"]["Param"][0] = "CourseID";
		$method["GetAsgnMarkedFile"]["Param"][1] = "UserEmail";
		$method["GetAsgnMarkedFile"]["Param"][2] = "HwType";
		$method["GetAsgnMarkedFile"]["Param"][3] = "SubjectID";
		$method["GetAsgnMarkedFile"]["Param"][4] = "Timestamp";
		$method["GetAsgnMarkedFile"]["Param"][5] = "AsgnID";
		
		$method["GetAsgnAttachment"]["class"] = "libeclass_ws.php";
		$method["GetAsgnAttachment"]["constructor"] = "libeclass_ws()";
		$method["GetAsgnAttachment"]["method"] = "WS_GetAsgnAttachment";
		$method["GetAsgnAttachment"]["Param"][0] = "CourseID";
		$method["GetAsgnAttachment"]["Param"][1] = "UserEmail";
		$method["GetAsgnAttachment"]["Param"][2] = "HwType";
		$method["GetAsgnAttachment"]["Param"][3] = "SubjectID";
		$method["GetAsgnAttachment"]["Param"][4] = "Timestamp";
		$method["GetAsgnAttachment"]["Param"][5] = "AsgnID";				
		
		$method["GetUserAsgnList"]["class"] = "libeclass_ws.php";
		$method["GetUserAsgnList"]["constructor"] = "libeclass_ws()";
		$method["GetUserAsgnList"]["method"] = "WS_GetUserAsgnList";
		$method["GetUserAsgnList"]["Param"][0] = "CourseID";
		$method["GetUserAsgnList"]["Param"][1] = "UserEmail";
		$method["GetUserAsgnList"]["Param"][2] = "HwType";
		$method["GetUserAsgnList"]["Param"][3] = "SubjectID";
		$method["GetUserAsgnList"]["Param"][4] = "Timestamp";
		$method["GetUserAsgnList"]["Param"][5] = "AsgnID";
		
		# Begin Henry Yuen (2010-06-02): Get submitted assignment
		$method["GetAsgnSubmitted"]["class"] = "libeclass_ws.php";
		$method["GetAsgnSubmitted"]["constructor"] = "libeclass_ws()";
		$method["GetAsgnSubmitted"]["method"] = "WS_GetAsgnSubmitted";
		$method["GetAsgnSubmitted"]["Param"][0] = "CourseID";
		$method["GetAsgnSubmitted"]["Param"][1] = "UserEmail";
		$method["GetAsgnSubmitted"]["Param"][2] = "HwType";
		$method["GetAsgnSubmitted"]["Param"][3] = "SubjectID";
		$method["GetAsgnSubmitted"]["Param"][4] = "Timestamp";
		$method["GetAsgnSubmitted"]["Param"][5] = "AsgnID";
		# End Henry Yuen (2010-06-02): Get submitted assignment		
		
		# Begin Henry Yuen (2010-06-15): Get assignment template
		$method["GetAsgnTemplate"]["class"] = "libeclass_ws.php";
		$method["GetAsgnTemplate"]["constructor"] = "libeclass_ws()";
		$method["GetAsgnTemplate"]["method"] = "WS_GetAsgnTemplate";
		$method["GetAsgnTemplate"]["Param"][0] = "CourseID";
		$method["GetAsgnTemplate"]["Param"][1] = "UserEmail";
		$method["GetAsgnTemplate"]["Param"][2] = "HwType";
		$method["GetAsgnTemplate"]["Param"][3] = "SubjectID";
		$method["GetAsgnTemplate"]["Param"][4] = "Timestamp";
		$method["GetAsgnTemplate"]["Param"][5] = "AsgnID";
		# End Henry Yuen (2010-06-15): Get submitted assignment		
		
		
		/*
		# NOT USED ANYMORE -- deprecated method, modified into GetAsgnAttachment		
		$method["GetAsgnDetail"]["class"] = "libeclass_ws.php";
		$method["GetAsgnDetail"]["constructor"] = "libeclass_ws()";
		$method["GetAsgnDetail"]["method"] = "WS_GetAsgnDetail";
		$method["GetAsgnDetail"]["Param"][0] = "CourseID";
		$method["GetAsgnDetail"]["Param"][1] = "AsgnID";
		*/	
					
		$method["UploadAsgn"]["class"] = "libeclass_ws.php";
		$method["UploadAsgn"]["constructor"] = "libeclass_ws()";
		$method["UploadAsgn"]["method"] = "WS_UploadAsgn";
		$method["UploadAsgn"]["Param"][0] = "CourseID";				
		$method["UploadAsgn"]["Param"][1] = "UserEmail";
		$method["UploadAsgn"]["Param"][2] = "AsgnID";
		$method["UploadAsgn"]["Param"][3] = "FileName";
		$method["UploadAsgn"]["Param"][4] = "FileByteStream";						
		$method["UploadAsgn"]["Param"][5] = "IsCorrection";
		
		$method["GetUserComment"]["class"] = "libeclass_ws.php";
		$method["GetUserComment"]["constructor"] = "libeclass_ws()";
		$method["GetUserComment"]["method"] = "WS_GetUserComment";
		$method["GetUserComment"]["Param"][0] = "CourseID";
		$method["GetUserComment"]["Param"][1] = "UserEmail";		
		$method["GetUserComment"]["Param"][2] = "Timestamp";		
		
		## 2011-07-29: a special version of UserComment method for ASL
		$method["GetUserComment2"]["class"] = "libeclass_ws.php";
		$method["GetUserComment2"]["constructor"] = "libeclass_ws()";
		$method["GetUserComment2"]["method"] = "WS_GetUserComment2";
		$method["GetUserComment2"]["Param"][0] = "CourseID";
		$method["GetUserComment2"]["Param"][1] = "UserEmail";		
		$method["GetUserComment2"]["Param"][2] = "Timestamp";
		$method["GetUserComment2"]["Param"][3] = "UserName";
		$method["GetUserComment2"]["Param"][4] = "NoOfRecords";
		## 2011-07-29: a special UserComment method for ASL
		
		## 2012-01-17: For Handling AppFileUpload
		$method["AppFileUpload"]["class"] = "libeclass_ws.php";
		$method["AppFileUpload"]["constructor"] = "libeclass_ws()";
		$method["AppFileUpload"]["method"] = "WS_eClassAppFileUpload";
		$method["AppFileUpload"]["Param"][0] = "CourseID";
		$method["AppFileUpload"]["Param"][1] = "uploadDir";
		$method["AppFileUpload"]["Param"][2] = "FileName";
		$method["AppFileUpload"]["Param"][3] = "FileByteStream";
		
		## 2012-03-12: For Getting Classroom with PowerLesson enabled
		$method["AppGetClassroom"]["class"] = "libeclass_ws.php";
		$method["AppGetClassroom"]["constructor"] = "libeclass_ws()";
		$method["AppGetClassroom"]["method"] = "WS_eClassAppGetClassroom";
		$method["AppGetClassroom"]["Param"][0] = "userID";
		$method["AppGetClassroom"]["Param"][1] = "PowerLessonOnly";
		$method["AppGetClassroom"]["Param"][2] = "withActiveLessonClassroom";
		$method["AppGetClassroom"]["Param"][3] = "DisplayLang";
		$method["AppGetClassroom"]["Param"][4] = "withMemberType";
		$method["AppGetClassroom"]["Param"][5] = "noWWSClassroom";
		
		## 2012-03-12: For Getting PowerLesson
		$method["AppGetLesson"]["class"] = "libeclass_ws.php";
		$method["AppGetLesson"]["constructor"] = "libeclass_ws()";
		$method["AppGetLesson"]["method"] = "WS_eClassAppGetLesson";
		$method["AppGetLesson"]["Param"][0] = "UserCourseID";
		$method["AppGetLesson"]["Param"][1] = "DisplayLang";
		
		## 2012-05-04: For Validating the PowerLesson App
		$method["AppValidate"]["class"] = "libeclass_ws.php";
		$method["AppValidate"]["constructor"] = "libeclass_ws()";
		$method["AppValidate"]["method"] = "WS_eClassAppValidate";
		
		## 2013-05-30: For Handling AppFileUploadByChunk
		$method["AppFileUploadByChunk"]["class"] = "libeclass_ws.php";
		$method["AppFileUploadByChunk"]["constructor"] = "libeclass_ws()";
		$method["AppFileUploadByChunk"]["method"] = "WS_eClassAppFileUploadByChunk";
		$method["AppFileUploadByChunk"]["Param"][0]  = "CourseID";
		$method["AppFileUploadByChunk"]["Param"][1]  = "uploadDir";
		$method["AppFileUploadByChunk"]["Param"][2]  = "TmpName";
		$method["AppFileUploadByChunk"]["Param"][3]  = "FileName";
		$method["AppFileUploadByChunk"]["Param"][4]  = "ChunkNo";
		$method["AppFileUploadByChunk"]["Param"][5]  = "TotalNoOfChunk";
		$method["AppFileUploadByChunk"]["Param"][6]  = "IsReady";
		$method["AppFileUploadByChunk"]["Param"][7]  = "FileByteStream";
		$method["AppFileUploadByChunk"]["Param"][8]  = "ChunkCheck";
		$method["AppFileUploadByChunk"]["Param"][9]  = "AllChunkCheck";
		$method["AppFileUploadByChunk"]["Param"][10] = "FileCheck";
		$method["AppFileUploadByChunk"]["Param"][11] = "IsRetry";
		
		## 2013-07-31: For Validate App Login QR Code
		$method["AppQRCodeValidate"]["class"] = "libeclass_ws.php";
		$method["AppQRCodeValidate"]["constructor"] = "libeclass_ws()";
		$method["AppQRCodeValidate"]["method"] = "WS_eClassAppQRCodeValidate";
		$method["AppQRCodeValidate"]["Param"][0] = "CourseID";
		$method["AppQRCodeValidate"]["Param"][1] = "LessonID";
		$method["AppQRCodeValidate"]["Param"][2] = "SessionID";
		
		## 2013-07-31: For Getting Student List of selected lesson plan
		$method["AppGetStudent"]["class"] = "libeclass_ws.php";
		$method["AppGetStudent"]["constructor"] = "libeclass_ws()";
		$method["AppGetStudent"]["method"] = "WS_eClassAppGetStudent";
		$method["AppGetStudent"]["Param"][0] = "CourseID";
		$method["AppGetStudent"]["Param"][1] = "LessonID";
		$method["AppGetStudent"]["Param"][2] = "SessionID";
		
		$method["LoginByQRCode"]["class"] = "libauth.php";
		$method["LoginByQRCode"]["constructor"] = "libauth()";
		$method["LoginByQRCode"]["method"] = "WS_QRCodeLogin";
		$method["LoginByQRCode"]["Param"][0] = "CourseID";
		$method["LoginByQRCode"]["Param"][1] = "LessonID";
		$method["LoginByQRCode"]["Param"][2] = "SessionID";
		$method["LoginByQRCode"]["Param"][3] = "UserID";
		$method["LoginByQRCode"]["Param"][4] = "hasAPIKey";
		
		# 2013-11-26: Save user device info for eclass app
		$method["SaveUserDeviceInfo"]["class"] = "eClassApp/libeClassApp.php";
		$method["SaveUserDeviceInfo"]["constructor"] = "libeClassApp()";
		$method["SaveUserDeviceInfo"]["method"] = "saveUserDevice";
		$method["SaveUserDeviceInfo"]["Param"][0] = "UserLogin";	
		$method["SaveUserDeviceInfo"]["Param"][1] = "DeviceID";
		$method["SaveUserDeviceInfo"]["Param"][2] = "DeviceOS"; // "Android", "iOS", "Win"
		$method["SaveUserDeviceInfo"]["Param"][3] = "DeviceModel";
		$method["SaveUserDeviceInfo"]["Param"][4] = "APNSType";	// "sandbox", "production"
		$method["SaveUserDeviceInfo"]["Param"][5] = "WPNSUri";	// For Windows Phone
		
		# 2013-12-02: Get children list for eclass app
		$method["GetChildrenListForApp"]["class"] = "libeclass_ws.php";
		$method["GetChildrenListForApp"]["constructor"] = "libeclass_ws()";
		$method["GetChildrenListForApp"]["method"] = "WS_GetChildrenListForApp";
		$method["GetChildrenListForApp"]["Param"][0] = "UserName";	
		
		# 2013-12-05: Get children attendance data for eclass app
		$method["GetStudentAttendanceInfo"]["class"] = "libeclass_ws.php";
		$method["GetStudentAttendanceInfo"]["constructor"] = "libeclass_ws()";
		$method["GetStudentAttendanceInfo"]["method"] = "WS_GetStudentAttendanceInfo";
		$method["GetStudentAttendanceInfo"]["Param"][0] = "IntranetUserID";
		$method["GetStudentAttendanceInfo"]["Param"][1] = "ParLang";

		# 2013-12-11: Update push message read status
		$method["UpdatePushMessageReadStatus"]["class"] = "eClassApp/libeClassApp.php";
		$method["UpdatePushMessageReadStatus"]["constructor"] = "libeClassApp()";
		$method["UpdatePushMessageReadStatus"]["method"] = "updatePushMessageReadStatus";
		$method["UpdatePushMessageReadStatus"]["Param"][0] = "UserLogin";
		$method["UpdatePushMessageReadStatus"]["Param"][1] = "IntranetReferenceIdList";
		
		# 2013-12-24: Get School Banner file path
		$method["GetSchoolBannerUrl"]["class"] = "eClassApp/libeClassApp.php";
		$method["GetSchoolBannerUrl"]["constructor"] = "libeClassApp()";
		$method["GetSchoolBannerUrl"]["method"] = "returnSchoolBanner";
		$method["GetSchoolBannerUrl"]["Param"][0] = "AppType";
		
		# 2013-12-24: Get eNotice for eClassApp
		$method["GetNoticeListForApp"]["class"] = "libeclass_ws.php";
		$method["GetNoticeListForApp"]["constructor"] = "libeclass_ws()";
		$method["GetNoticeListForApp"]["method"] = "WS_GetNoticeForApp";
		$method["GetNoticeListForApp"]["Param"][0] = "TargetUserID";
		$method["GetNoticeListForApp"]["Param"][1] = "CurrentUserID";
		
		# 2015-06-26: Get eNotice URL for eClassApp
		$method["GetNoticeUrlForApp"]["class"] = "libeclass_ws.php";
		$method["GetNoticeUrlForApp"]["constructor"] = "libeclass_ws()";
		$method["GetNoticeUrlForApp"]["method"] = "WS_GetNoticeUrlForApp";
		$method["GetNoticeUrlForApp"]["Param"][0] = "TargetUserID";
		$method["GetNoticeUrlForApp"]["Param"][1] = "CurrentUserID";
		$method["GetNoticeUrlForApp"]["Param"][2] = "IntranetNoticeID";
		$method["GetNoticeUrlForApp"]["Param"][3] = "ModuleName";
		
		# 2013-12-24: Get school news for eClassApp
		$method["GetAnnouncementListForApp"]["class"] = "libeclass_ws.php";
		$method["GetAnnouncementListForApp"]["constructor"] = "libeclass_ws()";
		$method["GetAnnouncementListForApp"]["method"] = "WS_GetAnnouncementForApp";
		$method["GetAnnouncementListForApp"]["Param"][0] = "TargetUserID";	
		$method["GetAnnouncementListForApp"]["Param"][1] = "CurrentUserID";
		$method["GetAnnouncementListForApp"]["Param"][2] = "NewestDateTime";
		$method["GetAnnouncementListForApp"]["Param"][3] = "NoOfRecords";	
		$method["GetAnnouncementListForApp"]["Param"][4] = "TargetGroup";
        $method["GetAnnouncementListForApp"]["Param"][5] = "ParLang";

        # 2014-01-06: Get school news for eClassApp
		$method["DeleteAccountForApp"]["class"] = "eClassApp/libeClassApp.php";
		$method["DeleteAccountForApp"]["constructor"] = "libeClassApp()";
		$method["DeleteAccountForApp"]["method"] = "deleteAppAccountServerFollowUp";
		$method["DeleteAccountForApp"]["Param"][0] = "UserLogin";	
		$method["DeleteAccountForApp"]["Param"][1] = "DeviceID";
		$method["DeleteAccountForApp"]["Param"][2] = "GeTuiClientID";
		$method["DeleteAccountForApp"]["Param"][3] = "LoginStatus";
		$method["DeleteAccountForApp"]["Param"][4] = "SessionID";
		
		## 2014-01-08: For Getting Both Classroom List and Lesson List
		$method["AppGetClassroomLesson"]["class"] = "libeclass_ws.php";
		$method["AppGetClassroomLesson"]["constructor"] = "libeclass_ws()";
		$method["AppGetClassroomLesson"]["method"] = "WS_eClassAppGetClassroomLesson";
		$method["AppGetClassroomLesson"]["Param"][0] = "userID";
		$method["AppGetClassroomLesson"]["Param"][1] = "PowerLessonOnly";
		$method["AppGetClassroomLesson"]["Param"][2] = "UserCourseID";
		$method["AppGetClassroomLesson"]["Param"][3] = "DisplayLang";
		
		# 2014-01-13: Get homework list for eClass App
		$method["GetHomeworkListForApp"]["class"] = "libeclass_ws.php";
		$method["GetHomeworkListForApp"]["constructor"] = "libeclass_ws()";
		$method["GetHomeworkListForApp"]["method"] = "WS_GetHomeworkListForApp";
		$method["GetHomeworkListForApp"]["Param"][0] = "TargetUserID";
		$method["GetHomeworkListForApp"]["Param"][1] = "CurrentUserID";
		
		# 2014-01-15: Get school event for eClass App
		$method["GetSchoolEventForApp"]["class"] = "libeclass_ws.php";
		$method["GetSchoolEventForApp"]["constructor"] = "libeclass_ws()";
		$method["GetSchoolEventForApp"]["method"] = "WS_GetSchoolEventForApp";
		$method["GetSchoolEventForApp"]["Param"][0] = "TargetUserID";
		$method["GetSchoolEventForApp"]["Param"][1] = "StartDate";
		$method["GetSchoolEventForApp"]["Param"][2] = "EndDate";
		$method["GetSchoolEventForApp"]["Param"][3] = "ParentUserID";
        $method["GetSchoolEventForApp"]["Param"][4] = "ParLang";

        # 2014-01-24: Get student payment data for eClass App
		$method["GetPaymentDataForApp"]["class"] = "libeclass_ws.php";
		$method["GetPaymentDataForApp"]["constructor"] = "libeclass_ws()";
		$method["GetPaymentDataForApp"]["method"] = "WS_GetPaymentForApp";
		$method["GetPaymentDataForApp"]["Param"][0] = "TargetUserID";
		
		# 2014-02-10: Get student attendance record for eClass App
		$method["GetAttendanceDetailsForApp"]["class"] = "libeclass_ws.php";
		$method["GetAttendanceDetailsForApp"]["constructor"] = "libeclass_ws()";
		$method["GetAttendanceDetailsForApp"]["method"] = "WS_GetAttendanceDetailsForApp";
		$method["GetAttendanceDetailsForApp"]["Param"][0] = "TargetUserID";
		$method["GetAttendanceDetailsForApp"]["Param"][1] = "Year";
		$method["GetAttendanceDetailsForApp"]["Param"][2] = "Month";
		$method["GetAttendanceDetailsForApp"]["Param"][3] = "NumberOfMonthRequired";
		
		# 2014-02-13: Get student take leave record
		$method["GetStudentTakeLeaveRecords"]["class"] = "libeclass_ws.php";
		$method["GetStudentTakeLeaveRecords"]["constructor"] = "libeclass_ws()";
		$method["GetStudentTakeLeaveRecords"]["method"] = "WS_GetStudentTakeLeaveRecords";
		$method["GetStudentTakeLeaveRecords"]["Param"][0] = "TargetUserID";
		
		# 2014-02-14: Get student take leave settings
		$method["GetStudentTakeLeaveSettings"]["class"] = "libeclass_ws.php";
		$method["GetStudentTakeLeaveSettings"]["constructor"] = "libeclass_ws()";
		$method["GetStudentTakeLeaveSettings"]["method"] = "WS_GetStudentTakeLeaveSettings";
		
		# 2014-02-19: Receive take leave record
		$method["SubmitApplyLeaveRecord"]["class"] = "libeclass_ws.php";
		$method["SubmitApplyLeaveRecord"]["constructor"] = "libeclass_ws()";
		$method["SubmitApplyLeaveRecord"]["method"] = "WS_SubmitApplyLeaveRecord";
		$method["SubmitApplyLeaveRecord"]["Param"][0] = "RecordID";
		$method["SubmitApplyLeaveRecord"]["Param"][1] = "ParentUserID";
		$method["SubmitApplyLeaveRecord"]["Param"][2] = "StudentUserID";
		$method["SubmitApplyLeaveRecord"]["Param"][3] = "StartDate";
		$method["SubmitApplyLeaveRecord"]["Param"][4] = "StartDateType";
		$method["SubmitApplyLeaveRecord"]["Param"][5] = "EndDate";
		$method["SubmitApplyLeaveRecord"]["Param"][6] = "EndDateType";
		$method["SubmitApplyLeaveRecord"]["Param"][7] = "Duration";
		$method["SubmitApplyLeaveRecord"]["Param"][8] = "Reason";
		$method["SubmitApplyLeaveRecord"]["Param"][9] = "ImageData";
		$method["SubmitApplyLeaveRecord"]["Param"][10] = "FileID";
		$method["SubmitApplyLeaveRecord"]["Param"][11] = "MethodID";
		$method["SubmitApplyLeaveRecord"]["Param"][12] = "Method";
		$method["SubmitApplyLeaveRecord"]["Param"][13] = "LeaveTypeID";
		$method["SubmitApplyLeaveRecord"]["Param"][14] = "AttendanceType";
		$method["SubmitApplyLeaveRecord"]["Param"][15] = "SessionFrom";
		$method["SubmitApplyLeaveRecord"]["Param"][16] = "SessionTo";

		# 2014-03-12: get school calendar ical format data
		$method["GetSchooliCal"]["class"] = "libeclass_ws.php";
		$method["GetSchooliCal"]["constructor"] = "libeclass_ws()";
		$method["GetSchooliCal"]["method"] = "WS_GetSchooliCal";
		$method["GetSchooliCal"]["Param"][0] = "StartDate";
		$method["GetSchooliCal"]["Param"][1] = "EndDate";
		$method["GetSchooliCal"]["Param"][2] = "APIKey";
		
		# 2014-04-08: get specific push message records
		$method["GetPushNotificationData"]["class"] = "eClassApp/libeClassApp.php";
		$method["GetPushNotificationData"]["constructor"] = "libeClassApp()";
		$method["GetPushNotificationData"]["method"] = "getUserRelatedPushMessage";
		$method["GetPushNotificationData"]["Param"][0] = "UserLogin";	
		$method["GetPushNotificationData"]["Param"][1] = "ReferenceIDList";
		
		# 2014-04-30: get latest push message records
		$method["GetLatestPushNotificationData"]["class"] = "eClassApp/libeClassApp.php";
		$method["GetLatestPushNotificationData"]["constructor"] = "libeClassApp()";
		$method["GetLatestPushNotificationData"]["method"] = "getUserRelatedLatestPushMessage";
		$method["GetLatestPushNotificationData"]["Param"][0] = "UserLogin";	
		$method["GetLatestPushNotificationData"]["Param"][1] = "ReferenceID";
		
		# 2014-05-09: get latest push message id
		$method["GetLastPushNotificationId"]["class"] = "eClassApp/libeClassApp.php";
		$method["GetLastPushNotificationId"]["constructor"] = "libeClassApp()";
		$method["GetLastPushNotificationId"]["method"] = "getUserRelatedLatestPushMessageId";
		$method["GetLastPushNotificationId"]["Param"][0] = "UserLogin";
		
		# 2014-05-27: get apply leave record images
		$method["GetLeaveRecordImages"]["class"] = "libeclass_ws.php";
		$method["GetLeaveRecordImages"]["constructor"] = "libeclass_ws()";
		$method["GetLeaveRecordImages"]["method"] = "WS_GetLeaveRecordImages";
		$method["GetLeaveRecordImages"]["Param"][0] = "RecordID";
		
		# 2014-06-30: get latest push message records by date time
		$method["GetLatestPushNotificationDataByTs"]["class"] = "eClassApp/libeClassApp.php";
		$method["GetLatestPushNotificationDataByTs"]["constructor"] = "libeClassApp()";
		$method["GetLatestPushNotificationDataByTs"]["method"] = "getUserRelatedLatestPushMessageByTs";
		$method["GetLatestPushNotificationDataByTs"]["Param"][0] = "UserLogin";	
		$method["GetLatestPushNotificationDataByTs"]["Param"][1] = "LastRetrieveTime";
		$method["GetLatestPushNotificationDataByTs"]["Param"][2] = "IncludeLastRetrieveTimeMessage";
		
		# 2014-07-21: save app auth code
		$method["ValidateDeviceAuthCode"]["class"] = "eClassApp/libeClassApp_authCode.php";
		$method["ValidateDeviceAuthCode"]["constructor"] = "libeClassApp_authCode()";
		$method["ValidateDeviceAuthCode"]["method"] = "WS_saveDeviceAuthCode";
		$method["ValidateDeviceAuthCode"]["Param"][0] = "TargetUserID";
		$method["ValidateDeviceAuthCode"]["Param"][1] = "AuthCode";	
		$method["ValidateDeviceAuthCode"]["Param"][2] = "DeviceID";
		
		# 2014-08-01: update scheduled push message result
		$method["UpdateScheduledPushMessageStatus"]["class"] = "eClassApp/libeClassApp.php";
		$method["UpdateScheduledPushMessageStatus"]["constructor"] = "libeClassApp()";
		$method["UpdateScheduledPushMessageStatus"]["method"] = "updateScheduledPushMessageStatus";
		$method["UpdateScheduledPushMessageStatus"]["Param"][0] = "NotifyMessageID";
		$method["UpdateScheduledPushMessageStatus"]["Param"][1] = "ResponseJson";
		$method["UpdateScheduledPushMessageStatus"]["Param"][2] = "SendTime";
		
		# 2014-08-15: get current server date time (for push message retrieve)
		$method["GetCurrentServerDateTime"]["class"] = "libeclass_ws.php";
		$method["GetCurrentServerDateTime"]["constructor"] = "libeclass_ws()";
		$method["GetCurrentServerDateTime"]["method"] = "WS_GetCurrentServerDateTime";
		
		# 2014-08-20: get staff attendance today card record
		$method["GetStaffAttendanceTodayRecord"]["class"] = "libeclass_ws.php";
		$method["GetStaffAttendanceTodayRecord"]["constructor"] = "libeClassApp()";
		$method["GetStaffAttendanceTodayRecord"]["method"] = "getStaffAttendanceTodayRecord";
		$method["GetStaffAttendanceTodayRecord"]["Param"][0] = "TargetUserID";
		
		# 2014-08-26: get multiple request result
		$method["GetMultipleRequestResult"]["class"] = "libeclass_ws.php";
		$method["GetMultipleRequestResult"]["constructor"] = "libeclass_ws()";
		$method["GetMultipleRequestResult"]["method"] = "WS_getMultipleRequestResult";
		$method["GetMultipleRequestResult"]["Param"][0] = "JsonRequestString";
		
		# 2014-08-26: get eCircular for teacher
		$method["GeteCircularForApp"]["class"] = "libeclass_ws.php";
		$method["GeteCircularForApp"]["constructor"] = "libeclass_ws()";
		$method["GeteCircularForApp"]["method"] = "WS_GeteCircularForApp";
		$method["GeteCircularForApp"]["Param"][0] = "TargetUserID";
		
		# 2014-09-02: get device auth code
		$method["GetDeviceAuthCode"]["class"] = "eClassApp/libeClassApp_authCode.php";
		$method["GetDeviceAuthCode"]["constructor"] = "libeClassApp_authCode()";
		$method["GetDeviceAuthCode"]["method"] = "WS_getDeviceAuthCode";
		$method["GetDeviceAuthCode"]["Param"][0] = "TargetUserID";
		$method["GetDeviceAuthCode"]["Param"][1] = "DeviceID";
		
		# 2014-10-15: get eNoticeS for teacher
		$method["GeteNoticeSForApp"]["class"] = "libeclass_ws.php";
		$method["GeteNoticeSForApp"]["constructor"] = "libeclass_ws()";
		$method["GeteNoticeSForApp"]["method"] = "WS_GeteNoticeSForApp";
		$method["GeteNoticeSForApp"]["Param"][0] = "TargetUserID";
		
		#2015-02-24: get SubjectList for teacher
	    $method["GetSubjectListForApp"]["class"] = "libeclass_ws.php";
		$method["GetSubjectListForApp"]["constructor"] = "libeclass_ws()";
		$method["GetSubjectListForApp"]["method"] = "WS_GetSubjectListForApp";
		
		# 2015-02-26: create lesson plan with video uploaded in video library
		$method["AppCreateLessonPlanWithVideoKey"]["class"] = "libeclass_ws.php";
		$method["AppCreateLessonPlanWithVideoKey"]["constructor"] = "libeclass_ws()";
		$method["AppCreateLessonPlanWithVideoKey"]["method"] = "WS_CreateLessonPlanWithVideoKey";
		$method["AppCreateLessonPlanWithVideoKey"]["Param"][0] = "CourseID";
		$method["AppCreateLessonPlanWithVideoKey"]["Param"][1] = "LessonPlanTitle";
		$method["AppCreateLessonPlanWithVideoKey"]["Param"][2] = "PreLessonStartDate";
		$method["AppCreateLessonPlanWithVideoKey"]["Param"][3] = "PreLessonEndDate";
		$method["AppCreateLessonPlanWithVideoKey"]["Param"][4] = "VideoTitle";
		$method["AppCreateLessonPlanWithVideoKey"]["Param"][5] = "VideoKey";
		$method["AppCreateLessonPlanWithVideoKey"]["Param"][6] = "TeacherUserID";
		
		# 2015-03-11: for flipped channels to send push message to teacher after video converted 
		$method["SendPushMessageToUser"]["class"] = "eClassApp/libeClassApp.php";
		$method["SendPushMessageToUser"]["constructor"] = "libeClassApp()";
		$method["SendPushMessageToUser"]["method"] = "sendPushMessageFromApi";
		$method["SendPushMessageToUser"]["Param"][0] = "UserIDList";
		$method["SendPushMessageToUser"]["Param"][1] = "Title";
		$method["SendPushMessageToUser"]["Param"][2] = "Content";
		
		# 2015-03-18: get photo information for eclass Digital Channel
		$method["GetDigitalChannelPhotoList"]["class"] = "libeclass_ws.php";
		$method["GetDigitalChannelPhotoList"]["constructor"] = "libeclass_ws()";
 		$method["GetDigitalChannelPhotoList"]["method"] = "WS_GetDigitalChannelPhotoList";
		$method["GetDigitalChannelPhotoList"]["Param"][0] = "TargetUserID";
		$method["GetDigitalChannelPhotoList"]["Param"][1] = "CurrentUserID";
		$method["GetDigitalChannelPhotoList"]["Param"][2] = "SchoolType";
        $method["GetDigitalChannelPhotoList"]["Param"][3] = "ParLang";

        $method["DigitalChannelPhotoLike"]["class"] = "libeclass_ws.php";
		$method["DigitalChannelPhotoLike"]["constructor"] = "libeclass_ws()";
		$method["DigitalChannelPhotoLike"]["method"] = "WS_DigitalChannelPhotoLike";
		$method["DigitalChannelPhotoLike"]["Param"][0] = "CurrentUserID";
		$method["DigitalChannelPhotoLike"]["Param"][1] = "PhotoID";

		# 2019-01-16: upload album details for Digital Channels
		$method["DigitalChannelUploadAlbumDetails"]["class"] = "libeclass_ws.php";
		$method["DigitalChannelUploadAlbumDetails"]["constructor"] = "libeclass_ws()";
		$method["DigitalChannelUploadAlbumDetails"]["method"] = "WS_DigitalChannelUploadAlbumDetails";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][0] = "CurrentUserID";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][1] = "CategoryCode";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][2] = "AlbumID";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][3] = "AlbumTitle";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][4] = "AlbumDescription";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][5] = "PeriodStartTime";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][6] = "PeriodEndTime";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][7] = "TargetGroup";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][8] = "TargetGroupIDs";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][9] = "PersonInChargeIDs";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][10] = "AlbumChiTitle";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][11] = "AlbumChiDescription";
		$method["DigitalChannelUploadAlbumDetails"]["Param"][12] = "AccessDate";
		
		# 2019-01-16: upload photo/video for Digital Channels
		$method["DigitalChannelUploadPhotoDetails"]["class"] = "libeclass_ws.php";
		$method["DigitalChannelUploadPhotoDetails"]["constructor"] = "libeclass_ws()";
		$method["DigitalChannelUploadPhotoDetails"]["method"] = "WS_DigitalChannelUploadPhotoDetails";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][0] = "CurrentUserID";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][1] = "AlbumID";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][2] = "FileID";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][3] = "FileName";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][4] = "FileDescription";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][5] = "FileChiDescription";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][6] = "ChunkNo";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][7] = "TotalNoOfChunk";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][8] = "FileByteStream";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][9] = "ChunkCheck";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][10] = "FileCheck";
		$method["DigitalChannelUploadPhotoDetails"]["Param"][11] = "IsReady";
		
		# 2019-01-16: get list of target group request (in Digital Channels)
		$method["GetTargetGroupList"]["class"] = "libeclass_ws.php";
		$method["GetTargetGroupList"]["constructor"] = "libeclass_ws()";
		$method["GetTargetGroupList"]["method"] = "WS_GetTargetGroupList";
		$method["GetTargetGroupList"]["Param"][0] = "CurrentUserID";
		$method["GetTargetGroupList"]["Param"][1] = "ShareGroups";

		# 2019-01-16: get list of target group request for PIC (in Digital Channels)
		$method["GetCommonChooseGroups"]["class"] = "libeclass_ws.php";
		$method["GetCommonChooseGroups"]["constructor"] = "libeclass_ws()";
		$method["GetCommonChooseGroups"]["method"] = "WS_GetCommonChooseGroups";
		$method["GetCommonChooseGroups"]["Param"][0] = "CurrentUserID";

		# 2019-01-16: get user list in target group (in Digital Channels)
		$method["GetCommonChooseUsers"]["class"] = "libeclass_ws.php";
		$method["GetCommonChooseUsers"]["constructor"] = "libeclass_ws()";
		$method["GetCommonChooseUsers"]["method"] = "WS_GetCommonChooseUsers";
		$method["GetCommonChooseUsers"]["Param"][0] = "CurrentUserID";
		$method["GetCommonChooseUsers"]["Param"][1] = "TargetID";
		$method["GetCommonChooseUsers"]["Param"][2] = "GroupID";

		# 2019-01-16: get album details request (in Digital Channels)
		$method["DigitalChannelGetAlbumDetails"]["class"] = "libeclass_ws.php";
		$method["DigitalChannelGetAlbumDetails"]["constructor"] = "libeclass_ws()";
		$method["DigitalChannelGetAlbumDetails"]["method"] = "WS_DigitalChannelGetAlbumDetails";
		$method["DigitalChannelGetAlbumDetails"]["Param"][0] = "CurrentUserID";
		$method["DigitalChannelGetAlbumDetails"]["Param"][1] = "AlbumID";
		$method["DigitalChannelGetAlbumDetails"]["Param"][2] = "CategoryCode";

		# 2019-01-23: add photo view log (in Digital Channels)
		$method["DigitalChannelPhotoView"]["class"] = "libeclass_ws.php";
		$method["DigitalChannelPhotoView"]["constructor"] = "libeclass_ws()";
		$method["DigitalChannelPhotoView"]["method"] = "WS_DigitalChannelPhotoView";
		$method["DigitalChannelPhotoView"]["Param"][0] = "CurrentUserID";
		$method["DigitalChannelPhotoView"]["Param"][1] = "PhotoID";

		# 2019-03-07: delete album (in Digital Channels)
		$method["DigitalChannelDeleteAlbum"]["class"] = "libeclass_ws.php";
		$method["DigitalChannelDeleteAlbum"]["constructor"] = "libeclass_ws()";
		$method["DigitalChannelDeleteAlbum"]["method"] = "WS_DigitalChannelDeleteAlbum";
		$method["DigitalChannelDeleteAlbum"]["Param"][0] = "CurrentUserID";
		$method["DigitalChannelDeleteAlbum"]["Param"][1] = "AlbumID";

		# 2019-03-07: delete photo(s) in album (in Digital Channels)
		$method["DigitalChannelDeletePhotosForAlbum"]["class"] = "libeclass_ws.php";
		$method["DigitalChannelDeletePhotosForAlbum"]["constructor"] = "libeclass_ws()";
		$method["DigitalChannelDeletePhotosForAlbum"]["method"] = "WS_DigitalChannelDeletePhotosForAlbum";
		$method["DigitalChannelDeletePhotosForAlbum"]["Param"][0] = "CurrentUserID";
		$method["DigitalChannelDeletePhotosForAlbum"]["Param"][1] = "AlbumID";
		$method["DigitalChannelDeletePhotosForAlbum"]["Param"][2] = "PhotoID";

	    $method["UserChangePassword"]["class"] = "libeclass_ws.php";
		$method["UserChangePassword"]["constructor"] = "libeclass_ws()";
		$method["UserChangePassword"]["method"] = "WS_UserChangePassword";
		$method["UserChangePassword"]["Param"][0] = "CurrentUserID";
		$method["UserChangePassword"]["Param"][1] = "OldPassword";
		$method["UserChangePassword"]["Param"][2] = "NewPassword";
		$method["UserChangePassword"]["Param"][3] = "ParUserLogin";
		$method["UserChangePassword"]["Param"][4] = "LangSetting";	
		
	    # 2015-12-03: get MedicalCaring for parent
		$method["GetMedicalCaringForApp"]["class"] = "libeclass_ws.php";
		$method["GetMedicalCaringForApp"]["constructor"] = "libeclass_ws()";
		$method["GetMedicalCaringForApp"]["method"] = "WS_GetMedicalCaringForApp";
		$method["GetMedicalCaringForApp"]["Param"][0] = "IntranetUserID";
		
		# 2016-01-25: get MedicalCaring for teacher
		$method["GetMedicalCaringForTeacherApp"]["class"] = "libeclass_ws.php";
		$method["GetMedicalCaringForTeacherApp"]["constructor"] = "libeclass_ws()";
		$method["GetMedicalCaringForTeacherApp"]["method"] = "WS_GetMedicalCaringForTeacherApp";
		$method["GetMedicalCaringForTeacherApp"]["Param"][0] = "IntranetUserID";
		
		# 2016-05-12: get WeeklyDiary for student
		$method["GetWeeklyDiaryForStudentApp"]["class"] = "libeclass_ws.php";
		$method["GetWeeklyDiaryForStudentApp"]["constructor"] = "libeclass_ws()";
		$method["GetWeeklyDiaryForStudentApp"]["method"] = "WS_GetWeeklyDiaryForApp";
		$method["GetWeeklyDiaryForStudentApp"]["Param"][0] = "CurrentUserID";
		
		# 2016-05-16: get eLib+ loan book record
		$method["GetELibPlusLoanBookRecord"]["class"] = "libeclass_ws.php";
		$method["GetELibPlusLoanBookRecord"]["constructor"] = "libeclass_ws()";
		$method["GetELibPlusLoanBookRecord"]["method"] = "WS_GetELibPlusLoanBookRecord";
		$method["GetELibPlusLoanBookRecord"]["Param"][0] = "TargetUserID";		
		
		# 2016-05-16: get eLib+ loan book record
		$method["GetELibPlusReservedBookRecord"]["class"] = "libeclass_ws.php";
		$method["GetELibPlusReservedBookRecord"]["constructor"] = "libeclass_ws()";
		$method["GetELibPlusReservedBookRecord"]["method"] = "WS_GetELibPlusReservedBookRecord";
		$method["GetELibPlusReservedBookRecord"]["Param"][0] = "TargetUserID";		
		
		# 2016-05-16: get eLib+ loan book record
		$method["GetELibPlusOutstandingPenalty"]["class"] = "libeclass_ws.php";
		$method["GetELibPlusOutstandingPenalty"]["constructor"] = "libeclass_ws()";
		$method["GetELibPlusOutstandingPenalty"]["method"] = "WS_GetELibPlusOutstandingPenalty";
		$method["GetELibPlusOutstandingPenalty"]["Param"][0] = "TargetUserID";
		
		# 2016-05-30: get PowerLesson User Device UDID
		$method['GetLessonPlanUsersUdid']['class'] = "libeclass_ws.php";
		$method['GetLessonPlanUsersUdid']['constructor'] = "libeclass_ws()";
		$method['GetLessonPlanUsersUdid']['method'] = "WS_eClassGetLessonUsersUdid";
		$method["GetLessonPlanUsersUdid"]["Param"][0] = "LessonUniqueID";		
		
	    # 2016-06-06: Receive weekly diary data
		$method["SubmitWeeklyDiaryRecord"]["class"] = "libeclass_ws.php";
		$method["SubmitWeeklyDiaryRecord"]["constructor"] = "libeclass_ws()";
		$method["SubmitWeeklyDiaryRecord"]["method"] = "WS_SubmitWeeklyDiaryRecord";
		$method["SubmitWeeklyDiaryRecord"]["Param"][0] = "HomeworkID";
		$method["SubmitWeeklyDiaryRecord"]["Param"][1] = "UserID";
		$method["SubmitWeeklyDiaryRecord"]["Param"][2] = "Title";
		$method["SubmitWeeklyDiaryRecord"]["Param"][3] = "Content";
		$method["SubmitWeeklyDiaryRecord"]["Param"][4] = "ArticleID";
		$method["SubmitWeeklyDiaryRecord"]["Param"][5] = "NewAttachmentAry";
		$method["SubmitWeeklyDiaryRecord"]["Param"][6] = "UnchangedAttachmentAry";
		$method["SubmitWeeklyDiaryRecord"]["Param"][7] = "ShowInListID";
		
		# 2016-07-19: Get HKUFlu Agreement Record
		$method["GetHKUFluAgreementRecord"]["class"] = "libeclass_ws.php";
		$method["GetHKUFluAgreementRecord"]["constructor"] = "libeclass_ws()";
		$method["GetHKUFluAgreementRecord"]["method"] = "WS_GetHKUFluAgreementRecord";
		$method["GetHKUFluAgreementRecord"]["Param"][0] = "StudentID";
		
		# 2016-07-19: Set HKUFlu Agreement Record
		$method["NewHKUFluAgreementRecord"]["class"] = "libeclass_ws.php";
		$method["NewHKUFluAgreementRecord"]["constructor"] = "libeclass_ws()";
		$method["NewHKUFluAgreementRecord"]["method"] = "WS_NewHKUFluAgreementRecord";
		$method["NewHKUFluAgreementRecord"]["Param"][0] = "ParentID";
		$method["NewHKUFluAgreementRecord"]["Param"][1] = "StudentID";
		$method["NewHKUFluAgreementRecord"]["Param"][2] = "IsAgreed";
		$method["NewHKUFluAgreementRecord"]["Param"][3] = "ParentName";
		
		# 2016-07-19: Get HKUFlu Sick Leave Record
		$method["GetHKUFluSickLeaveRecord"]["class"] = "libeclass_ws.php";
		$method["GetHKUFluSickLeaveRecord"]["constructor"] = "libeclass_ws()";
		$method["GetHKUFluSickLeaveRecord"]["method"] = "WS_GetHKUFluSickLeaveRecord";
		$method["GetHKUFluSickLeaveRecord"]["Param"][0] = "StudentID";
		
		# 2016-07-19: Set HKUFlu Sick Leave Record
		$method["NewHKUFluSickLeaveRecord"]["class"] = "libeclass_ws.php";
		$method["NewHKUFluSickLeaveRecord"]["constructor"] = "libeclass_ws()";
		$method["NewHKUFluSickLeaveRecord"]["method"] = "WS_NewHKUFluSickLeaveRecord";
		$method["NewHKUFluSickLeaveRecord"]["Param"][0] = "ParentID";
		$method["NewHKUFluSickLeaveRecord"]["Param"][1] = "StudentID";
		$method["NewHKUFluSickLeaveRecord"]["Param"][2] = "EClassLeaveID";
		$method["NewHKUFluSickLeaveRecord"]["Param"][3] = "SickDate";
		$method["NewHKUFluSickLeaveRecord"]["Param"][4] = "LeaveDate";
		$method["NewHKUFluSickLeaveRecord"]["Param"][5] = "SickLeaveSick";
		$method["NewHKUFluSickLeaveRecord"]["Param"][6] = "SickLeaveSymptom";
		$method["NewHKUFluSickLeaveRecord"]["Param"][7] = "IsNotSickLeave";
		$method["NewHKUFluSickLeaveRecord"]["Param"][8] = "From";
		
		# 2016-09-02: Check if HKUFlu Record Exists
		$method["IsHKUFluRecordExists"]["class"] = "libeclass_ws.php";
		$method["IsHKUFluRecordExists"]["constructor"] = "libeclass_ws()";
		$method["IsHKUFluRecordExists"]["method"] = "WS_IsHKUFluRecordExists";
		$method["IsHKUFluRecordExists"]["Param"][0] = "StudentID";
		$method["IsHKUFluRecordExists"]["Param"][1] = "LeaveDate";

		# 2016-08-09: Get User Info for PL20 portal
		$method["GetPL2PortalUserInfo"]["class"] = "libeclass_ws.php";
		$method["GetPL2PortalUserInfo"]["constructor"] = "libeclass_ws()";
		$method["GetPL2PortalUserInfo"]["method"] = "WS_GetPL2PortalUserInfo";
		$method["GetPL2PortalUserInfo"]["Param"][0] = "MemberType";
		
		# 2016-08-15: Validate Lesson for Relaunch
		$method["AppRelaunchLessonValidate"]["class"] = "libeclass_ws.php";
		$method["AppRelaunchLessonValidate"]["constructor"] = "libeclass_ws()";
		$method["AppRelaunchLessonValidate"]["method"] = "WS_eClassAppRelaunchLessonValidate";
		$method["AppRelaunchLessonValidate"]["Param"][0] = "CourseID";
		$method["AppRelaunchLessonValidate"]["Param"][1] = "LessonID";
		$method["AppRelaunchLessonValidate"]["Param"][2] = "SessionID";

		# 2016-09-01: Renew AuthToken for PL20
		$method["RenewAuthToken"]["class"] = "libeclass_ws.php";
		$method["RenewAuthToken"]["constructor"] = "libeclass_ws()";
		$method["RenewAuthToken"]["method"] = "WS_RenewAuthToken";
		$method["RenewAuthToken"]["Param"][0] = "CourseId";
		$method["RenewAuthToken"]["Param"][1] = "UserId";
		
		# 2016-09-01: Remove AuthToken for PL20
		$method["RemoveAuthToken"]["class"] = "libeclass_ws.php";
		$method["RemoveAuthToken"]["constructor"] = "libeclass_ws()";
		$method["RemoveAuthToken"]["method"] = "WS_RemoveAuthToken";
		$method["RemoveAuthToken"]["Param"][0] = "CourseId";
		$method["RemoveAuthToken"]["Param"][1] = "UserId";
		$method["RemoveAuthToken"]["Param"][2] = "AuthToken";
		
		# 2016-09-06: Renew SessionLastUpdated
		$method["RenewSessionLastUpdated"]["class"] = "libeclass_ws.php";
		$method["RenewSessionLastUpdated"]["constructor"] = "libeclass_ws()";
		$method["RenewSessionLastUpdated"]["method"] = "WS_RenewSessionLastUpdated";
		
		# 2016-10-28: For Handling CentennialAppFileUpload
		$method["CentennialAppFileUpload"]["class"] = "libeclass_ws.php";
		$method["CentennialAppFileUpload"]["constructor"] = "libeclass_ws()";
		$method["CentennialAppFileUpload"]["method"] = "WS_eClassAppFileUpload_cceap";
		$method["CentennialAppFileUpload"]["Param"][0] = "uploadDir";
		$method["CentennialAppFileUpload"]["Param"][1] = "FileName";
		$method["CentennialAppFileUpload"]["Param"][2] = "FileByteStream";
		
		# 2016-12-19: Request from Payment central server
		$method["UpdatePaymentCallback"]["class"] = "libeclass_ws.php";
		$method["UpdatePaymentCallback"]["constructor"] = "libeclass_ws()";
		$method["UpdatePaymentCallback"]["method"] = "WS_updatePaymentCallbackFromCentralServer";
		$method["UpdatePaymentCallback"]["Param"][0] = "FromServer";
		$method["UpdatePaymentCallback"]["Param"][1] = "PaymentID";
		$method["UpdatePaymentCallback"]["Param"][2] = "ServiceProvider";
		$method["UpdatePaymentCallback"]["Param"][3] = "PayerUserID";
		$method["UpdatePaymentCallback"]["Param"][4] = "PaymentStatus";
		$method["UpdatePaymentCallback"]["Param"][5] = "UniqueCode";
		$method["UpdatePaymentCallback"]["Param"][6] = "ServiceProviderTxNo";
		$method["UpdatePaymentCallback"]["Param"][7] = "ErrorCode";
		$method["UpdatePaymentCallback"]["Param"][8] = "NoticeID";
		$method["UpdatePaymentCallback"]["Param"][9] = "NoticePaymentID";
		$method["UpdatePaymentCallback"]["Param"][10] = "NoticeReplyAnswer";
		$method["UpdatePaymentCallback"]["Param"][11] = "StudentID";
		$method["UpdatePaymentCallback"]["Param"][12] = "PaymentAmount";
		$method["UpdatePaymentCallback"]["Param"][13] = "DateInput";
		$method["UpdatePaymentCallback"]["Param"][14] = "ModuleName";
		$method["UpdatePaymentCallback"]["Param"][15] = "ModuleTransactionID";
		$method["UpdatePaymentCallback"]["Param"][16] = "OriginalAmount";
		$method["UpdatePaymentCallback"]["Param"][17] = "NewAmount";
		$method["UpdatePaymentCallback"]["Param"][18] = "AdministrativeFeeFixAmount";
		$method["UpdatePaymentCallback"]["Param"][19] = "AdministrativeFeePercentageAmount";

		# 2018-07-16: Request from Payment gateway central server 
		$method["GetPaymentItemMerchantInfo"]["class"] = "libeclass_ws.php";
		$method["GetPaymentItemMerchantInfo"]["constructor"] = "libeclass_ws()";
		$method["GetPaymentItemMerchantInfo"]["method"] = "WS_GetPaymentItemMerchantInfo";
		$method["GetPaymentItemMerchantInfo"]["Param"][0] = "FromServer";
		$method["GetPaymentItemMerchantInfo"]["Param"][1] = "PaymentID";
		$method["GetPaymentItemMerchantInfo"]["Param"][2] = "ServiceProvider";
		$method["GetPaymentItemMerchantInfo"]["Param"][3] = "ModuleName";
		$method["GetPaymentItemMerchantInfo"]["Param"][4] = "ModuleTransactionID";

		# 2017-02-26: Login Courses (For PL2 Use)
		$method["LoginCourses"]["class"] = "libeclass_ws.php";
		$method["LoginCourses"]["constructor"] = "libeclass_ws()";
		$method["LoginCourses"]["method"] = "WS_LoginCourses";
		
		# 2017-02-26: Logout Courses (For PL2 Use)
		$method["LogoutCourses"]["class"] = "libeclass_ws.php";
		$method["LogoutCourses"]["constructor"] = "libeclass_ws()";
		$method["LogoutCourses"]["method"] = "WS_LogoutCourses";
		
		# 2017-02-26: LoginCourseByLessonPin (For PL2 Use)
		$method["LoginCourseByLessonPin"]["class"] = "libeclass_ws.php";
		$method["LoginCourseByLessonPin"]["constructor"] = "libeclass_ws()";
		$method["LoginCourseByLessonPin"]["method"] = "WS_LoginCourseByLessonPin";
		$method["LoginCourseByLessonPin"]["Param"][0] = "courseId";
		$method["LoginCourseByLessonPin"]["Param"][1] = "lessonId";
		$method["LoginCourseByLessonPin"]["Param"][2] = "userId";
		$method["LoginCourseByLessonPin"]["Param"][3] = "pin";

		# 2017-02-26: LogoutCourseByAuthToken (For PL2 Use)
		$method["LogoutCourseByAuthToken"]["class"] = "libeclass_ws.php";
		$method["LogoutCourseByAuthToken"]["constructor"] = "libeclass_ws()";
		$method["LogoutCourseByAuthToken"]["method"] = "WS_LogoutCourseByAuthToken";
		$method["LogoutCourseByAuthToken"]["Param"][0] = "courseId";
		$method["LogoutCourseByAuthToken"]["Param"][1] = "lessonId";
		$method["LogoutCourseByAuthToken"]["Param"][2] = "authToken";

		# 2017-02-26: Get User Info (For PL2 Use)
		$method["GetUserInfo"]["class"] = "libeclass_ws.php";
		$method["GetUserInfo"]["constructor"] = "libeclass_ws()";
		$method["GetUserInfo"]["method"] = "WS_GetUserInfo";

		# 2017-03-31: Get Eclass Groups for PL2 
		$method["GetEclassGroups"]["class"] = "libeclass_ws.php";
		$method["GetEclassGroups"]["constructor"] = "libeclass_ws()";
		$method["GetEclassGroups"]["method"] = "WS_GetEclassGroups";
		$method["GetEclassGroups"]["Param"][0] = "courseId";
		
		# 2017-04-04: Get Course Users for PL2 
		$method["GetCourseUsers"]["class"] = "libeclass_ws.php";
		$method["GetCourseUsers"]["constructor"] = "libeclass_ws()";
		$method["GetCourseUsers"]["method"] = "WS_GetCourseUsers";
		$method["GetCourseUsers"]["Param"][0] = "courseId";	

		# 2017-04-04: Validate Lesson Pin for PL2 quick login
		$method["ValidateLessonPin"]["class"] = "libeclass_ws.php";
		$method["ValidateLessonPin"]["constructor"] = "libeclass_ws()";
		$method["ValidateLessonPin"]["method"] = "WS_ValidateLessonPin";
		$method["ValidateLessonPin"]["Param"][0] = "pin";
		
	    # 2017-05-02: Forget Password for dhl
		$method["ForgetPasswordForDHL"]["class"] = "libeclass_ws.php";
		$method["ForgetPasswordForDHL"]["constructor"] = "libeclass_ws()";
		$method["ForgetPasswordForDHL"]["method"] = "WS_ForgetPasswordForDHL";
		$method["ForgetPasswordForDHL"]["Param"][0] = "LoginID";
	
		# 2017-05-04: Update Email for dhl
		$method["UpdateEmailForDHL"]["class"] = "libeclass_ws.php";
		$method["UpdateEmailForDHL"]["constructor"] = "libeclass_ws()";
		$method["UpdateEmailForDHL"]["method"] = "WS_UpdateEmailForDHL";
		$method["UpdateEmailForDHL"]["Param"][0] = "CurrentUserID";	
		$method["UpdateEmailForDHL"]["Param"][1] = "Email";	
		
		# 2017-05-16: Check PowerLesson version compatible for PL2 
		$method["IsPowerLessonCompatible"]["class"] = "libeclass_ws.php";
		$method["IsPowerLessonCompatible"]["constructor"] = "libeclass_ws()";
		$method["IsPowerLessonCompatible"]["method"] = "WS_IsPowerLessonCompatible";
		$method["IsPowerLessonCompatible"]["Param"][0] = "version";

        # 2017-06-20: Get targetable user for instant messaging in eClass Apps
        $method["GetTargetableUsersForMessaging"]["class"] = "libeclass_ws.php";
        $method["GetTargetableUsersForMessaging"]["constructor"] = "libeclass_ws()";
        $method["GetTargetableUsersForMessaging"]["method"] = "WS_GetTargetableUsersForMessaging";
        $method["GetTargetableUsersForMessaging"]["Param"][0] = "IntranetUserID";
        
        # 2017-06-27: open group message chartroom
        $method["CreateGroupMessageChatroom"]["class"] = "libeclass_ws.php";
        $method["CreateGroupMessageChatroom"]["constructor"] = "libeclass_ws()";
        $method["CreateGroupMessageChatroom"]["method"] = "WS_OpenGroupMessageChatroom";
        $method["CreateGroupMessageChatroom"]["Param"][0] = "IntranetUserID";
        $method["CreateGroupMessageChatroom"]["Param"][1] = "TargetUserIDAry";
        
        # 2017-08-21: Get User Info with all courses for PL20 portal
		$method["GetPL2PortalUserInfoWithAllCourses"]["class"] = "libeclass_ws.php";
		$method["GetPL2PortalUserInfoWithAllCourses"]["constructor"] = "libeclass_ws()";
		$method["GetPL2PortalUserInfoWithAllCourses"]["method"] = "WS_GetPL2PortalUserInfoWithAllCourses";
		$method["GetPL2PortalUserInfoWithAllCourses"]["Param"][0] = "MemberType";

		# 2017-08-28: Get Lesson, User, Device Mapping for PL2 
		$method["GetPL2LessonPlanUsersUdid"]["class"] = "libeclass_ws.php";
		$method["GetPL2LessonPlanUsersUdid"]["constructor"] = "libeclass_ws()";
		$method["GetPL2LessonPlanUsersUdid"]["method"] = "WS_GetPL2LessonPlanUsersUdid";
		$method["GetPL2LessonPlanUsersUdid"]["Param"][0] = "ClassroomUniqueID";
		$method["GetPL2LessonPlanUsersUdid"]["Param"][1] = "LessonUniqueID";
		
		# 2017-09-20: Clear school app license
		$method["ClearLicenseCache"]["class"] = "eClassApp/libeClassApp.php";
		$method["ClearLicenseCache"]["constructor"] = "libeClassApp()";
		$method["ClearLicenseCache"]["method"] = "clearLicenseCache";
		
		# 2017-09-21: Get Courses Users for PL2 
		$method["GetCoursesUsers"]["class"] = "libeclass_ws.php";
		$method["GetCoursesUsers"]["constructor"] = "libeclass_ws()";
		$method["GetCoursesUsers"]["method"] = "WS_GetCoursesUsers";
		$method["GetCoursesUsers"]["Param"][0] = "courseIds";
		
		# 2017-09-25: Get Courses Teachers for PL2 
		$method["GetCoursesTeachers"]["class"] = "libeclass_ws.php";
		$method["GetCoursesTeachers"]["constructor"] = "libeclass_ws()";
		$method["GetCoursesTeachers"]["method"] = "WS_GetCoursesTeachers";
		$method["GetCoursesTeachers"]["Param"][0] = "courseIds";
		
		## 2017-11-29: get homework image for app
		$method["GetHomeworkImageForApp"]["class"] = "libeclass_ws.php";
		$method["GetHomeworkImageForApp"]["constructor"] = "libeclass_ws()";
		$method["GetHomeworkImageForApp"]["method"] = "WS_GetHomeworkImageForApp";
		$method["GetHomeworkImageForApp"]["Param"][0] = "TargetUserID";
		$method["GetHomeworkImageForApp"]["Param"][1] = "GetAllHomework";
        
        # 2017-12-02: Dedicated Method for PL2
        $method["PowerLesson"]["class"]       = "PowerLesson/PowerLesson.php";
        $method["PowerLesson"]["constructor"] = "PowerLesson()";
        $method["PowerLesson"]["method"]      = "run";
        $method["PowerLesson"]["Param"][0]    = "procedure";
        $method["PowerLesson"]["Param"][1]    = "args";

        # 2018-06-06: cancel leave record for parent app
        $method["CancelLeaveRecord"]["class"] = "libeclass_ws.php";
        $method["CancelLeaveRecord"]["constructor"] = "libeclass_ws()";
        $method["CancelLeaveRecord"]["method"] = "WS_CancelLeaveRecord";
        $method["CancelLeaveRecord"]["Param"][0] = "RecordID";

        # 2018-08-23: Dedicated Method for STEM x PL2
        $method["StemLicense"]["class"]       = "StemLicense/StemLicense.php";
        $method["StemLicense"]["constructor"] = "StemLicense()";
        $method["StemLicense"]["method"]      = "run";
        $method["StemLicense"]["Param"][0]    = "procedure";
        $method["StemLicense"]["Param"][1]    = "args";
        
        # 2018-10-12: get official photo path for apps
        $method["GetUserOfficialPhotoPath"]["class"] = "libeclass_ws.php";
        $method["GetUserOfficialPhotoPath"]["constructor"] = "libeclass_ws()";
        $method["GetUserOfficialPhotoPath"]["method"] = "WS_GetUserOfficialPhotoPath";
        $method["GetUserOfficialPhotoPath"]["Param"][0] = "IntranetUserID";
        
        # 2018-10-12: get personal photo path for apps
        $method["GetUserPersonalPhotoPath"]["class"] = "libeclass_ws.php";
        $method["GetUserPersonalPhotoPath"]["constructor"] = "libeclass_ws()";
        $method["GetUserPersonalPhotoPath"]["method"] = "WS_GetUserPersonalPhotoPath";
        $method["GetUserPersonalPhotoPath"]["Param"][0] = "IntranetUserID";
        
        # 2018-10-18： save personal or official photo
        $method["SaveUserPhoto"]["class"] = "eClassApp/libeClassApp.php";
        $method["SaveUserPhoto"]["constructor"] = "libeClassApp()";
        $method["SaveUserPhoto"]["method"] = "ws_saveUserPhoto";
        $method["SaveUserPhoto"]["Param"][0] = "IntranetUserID";
        $method["SaveUserPhoto"]["Param"][1] = "PhotoType";     // 'O' for official; 'P' for personal
        $method["SaveUserPhoto"]["Param"][2] = "ImageData";
        
        # 2018-10-18： replace personal photo by official photo
        $method["UseOfficialPhotoAsPersonalPhoto"]["class"] = "eClassApp/libeClassApp.php";
        $method["UseOfficialPhotoAsPersonalPhoto"]["constructor"] = "libeClassApp()";
        $method["UseOfficialPhotoAsPersonalPhoto"]["method"] = "ws_useOfficialPhotoAsPersonalPhoto";
        $method["UseOfficialPhotoAsPersonalPhoto"]["Param"][0] = "IntranetUserID";
        
        # 2018-10-18： cancel the use of personal photo by official photo
        $method["CancelOfficialPhotoAsPersonalPhoto"]["class"] = "eClassApp/libeClassApp.php";
        $method["CancelOfficialPhotoAsPersonalPhoto"]["constructor"] = "libeClassApp()";
        $method["CancelOfficialPhotoAsPersonalPhoto"]["method"] = "ws_cancelOfficialPhotoAsPersonalPhoto";
        $method["CancelOfficialPhotoAsPersonalPhoto"]["Param"][0] = "IntranetUserID";
        
        # 2018-10-19： apply for card reprint
        $method["AddReprintCardRecord"]["class"] = "eClassApp/libeClassApp_reprintCard.php";
        $method["AddReprintCardRecord"]["constructor"] = "libeClassApp_reprintCard()";
        $method["AddReprintCardRecord"]["method"] = "ws_addReprintCardRecord";
        $method["AddReprintCardRecord"]["Param"][0] = "ApplicantUserID";
        $method["AddReprintCardRecord"]["Param"][1] = "StudentUserID";
        $method["AddReprintCardRecord"]["Param"][2] = "Reason";
        $method["AddReprintCardRecord"]["Param"][3] = "ImageData";
        $method["AddReprintCardRecord"]["Param"][4] = "DeliveryMethod";
        $method["AddReprintCardRecord"]["Param"][5] = "DeliveryAddress";
        $method["AddReprintCardRecord"]["Param"][6] = "DeliveryDistrict";
        $method["AddReprintCardRecord"]["Param"][7] = "DeliveryName";
        $method["AddReprintCardRecord"]["Param"][8] = "DeliveryTelNum";

        # 2018-10-19： get data for card reprint
        $method["GetReprintCardRecord"]["class"] = "eClassApp/libeClassApp_reprintCard.php";
        $method["GetReprintCardRecord"]["constructor"] = "libeClassApp_reprintCard()";
        $method["GetReprintCardRecord"]["method"] = "ws_getCurrentReprintCardRecord";
        $method["GetReprintCardRecord"]["Param"][0] = "StudentUserID";
        
        # 2018-10-19： cancel for card reprint
        $method["CancelReprintCardRecord"]["class"] = "eClassApp/libeClassApp_reprintCard.php";
        $method["CancelReprintCardRecord"]["constructor"] = "libeClassApp_reprintCard()";
        $method["CancelReprintCardRecord"]["method"] = "ws_cancelReprintCardRecord";
        $method["CancelReprintCardRecord"]["Param"][0] = "RecordID";
        
        # 2018-10-19： update delivery method for card reprint
        $method["UpdateReprintCardRecordDelivery"]["class"] = "eClassApp/libeClassApp_reprintCard.php";
        $method["UpdateReprintCardRecordDelivery"]["constructor"] = "libeClassApp_reprintCard()";
        $method["UpdateReprintCardRecordDelivery"]["method"] = "ws_updateReprintCardRecordDelivery";
        $method["UpdateReprintCardRecordDelivery"]["Param"][0] = "RecordID";
        $method["UpdateReprintCardRecordDelivery"]["Param"][1] = "DeliveryMethod";
        $method["UpdateReprintCardRecordDelivery"]["Param"][2] = "DeliveryAddress";
        $method["UpdateReprintCardRecordDelivery"]["Param"][3] = "DeliveryDistrict";
        $method["UpdateReprintCardRecordDelivery"]["Param"][4] = "DeliveryName";
        $method["UpdateReprintCardRecordDelivery"]["Param"][5] = "DeliveryTelNum";

        # 2018-10-19： get data for card reprint
        $method["GetReprintCardRecordHistory"]["class"] = "eClassApp/libeClassApp_reprintCard.php";
        $method["GetReprintCardRecordHistory"]["constructor"] = "libeClassApp_reprintCard()";
        $method["GetReprintCardRecordHistory"]["method"] = "ws_getReprintCardRecordHistory";
        $method["GetReprintCardRecordHistory"]["Param"][0] = "StudentUserID";
        
        # 2018-10-22： update payment status for card reprint
        $method["UpdateReprintCardRecordPaymentStatus"]["class"] = "eClassApp/libeClassApp_reprintCard.php";
        $method["UpdateReprintCardRecordPaymentStatus"]["constructor"] = "libeClassApp_reprintCard()";
        $method["UpdateReprintCardRecordPaymentStatus"]["method"] = "ws_updateReprintCardRecordPaymentStatus";
        $method["UpdateReprintCardRecordPaymentStatus"]["Param"][0] = "RecordID";
        $method["UpdateReprintCardRecordPaymentStatus"]["Param"][1] = "PaymentStatus";
        $method["UpdateReprintCardRecordPaymentStatus"]["Param"][2] = "ReferenceNum";
        $method["UpdateReprintCardRecordPaymentStatus"]["Param"][3] = "ServiceProvider";
        $method["UpdateReprintCardRecordPaymentStatus"]["Param"][4] = "PaymentDate";
        
        # 2018-10-31： update order status from reprint card
        $method["UpdateReprintCardOrderStatus"]["class"] = "eClassApp/libeClassApp_reprintCard.php";
        $method["UpdateReprintCardOrderStatus"]["constructor"] = "libeClassApp_reprintCard()";
        $method["UpdateReprintCardOrderStatus"]["method"] = "ws_updateReprintCardOrderStatus";
        $method["UpdateReprintCardOrderStatus"]["Param"][0] = "RecordID";
        $method["UpdateReprintCardOrderStatus"]["Param"][1] = "CardStatus";

		# 2019-05-09 : [BEGIN] iCalendar api
		$method["iCalendarSystemSettings"]["class"] = "libeclass_ws.php";
		$method["iCalendarSystemSettings"]["constructor"] = "libeclass_ws()";
		$method["iCalendarSystemSettings"]["method"] = "WS_iCalendarSystemSettings";

		$method["iCalendarUsers"]["class"] = "libeclass_ws.php";
		$method["iCalendarUsers"]["constructor"] = "libeclass_ws()";
		$method["iCalendarUsers"]["method"] = "WS_iCalendarUsers";

		$method["iCalendarUserPreference"]["class"] = "libeclass_ws.php";
		$method["iCalendarUserPreference"]["constructor"] = "libeclass_ws()";
		$method["iCalendarUserPreference"]["method"] = "WS_iCalendarUserPreference";
		$method["iCalendarUserPreference"]["Param"][0] = "CurrentUserID";
		$method["iCalendarUserPreference"]["Param"][1] = "SettingName";
		
		$method["iCalendarUserCalendars"]["class"] = "libeclass_ws.php";
		$method["iCalendarUserCalendars"]["constructor"] = "libeclass_ws()";
		$method["iCalendarUserCalendars"]["method"] = "WS_iCalendarUserCalendars";
		$method["iCalendarUserCalendars"]["Param"][0] = "CurrentUserID";
		
		$method["iCalendarInitData"]["class"] = "libeclass_ws.php";
		$method["iCalendarInitData"]["constructor"] = "libeclass_ws()";
		$method["iCalendarInitData"]["method"] = "WS_iCalendarInitData";
		$method["iCalendarInitData"]["Param"][0] = "CurrentUserID";		
		
		$method["iCalendarUserEvents"]["class"] = "libeclass_ws.php";
		$method["iCalendarUserEvents"]["constructor"] = "libeclass_ws()";
		$method["iCalendarUserEvents"]["method"] = "WS_iCalendarUserEvents";
		$method["iCalendarUserEvents"]["Param"][0] = "CurrentUserID";
		$method["iCalendarUserEvents"]["Param"][1] = "StartTimestamp";
		$method["iCalendarUserEvents"]["Param"][2] = "EndTimestamp";
		
		$method["iCalendarEventDetail"]["class"] = "libeclass_ws.php";
		$method["iCalendarEventDetail"]["constructor"] = "libeclass_ws()";
		$method["iCalendarEventDetail"]["method"] = "WS_iCalendarEventDetail";
		$method["iCalendarEventDetail"]["Param"][0] = "CurrentUserID";
		$method["iCalendarEventDetail"]["Param"][1] = "EventID";
		$method["iCalendarEventDetail"]["Param"][2] = "CalType";
		
		$method["iCalendarUpsertEvent"]["class"] = "libeclass_ws.php";
		$method["iCalendarUpsertEvent"]["constructor"] = "libeclass_ws()";
		$method["iCalendarUpsertEvent"]["method"] = "WS_iCalendarUpsertEvent";
		$method["iCalendarUpsertEvent"]["Param"][0] = "CurrentUserID";
		$method["iCalendarUpsertEvent"]["Param"][1] = "CalType";
		$method["iCalendarUpsertEvent"]["Param"][2] = "EventDataAry"; // array type
		
		$method["iCalendarRemoveEvent"]["class"] = "libeclass_ws.php";
		$method["iCalendarRemoveEvent"]["constructor"] = "libeclass_ws()";
		$method["iCalendarRemoveEvent"]["method"] = "WS_iCalendarRemoveEvent";
		$method["iCalendarRemoveEvent"]["Param"][0] = "CurrentUserID";
		$method["iCalendarRemoveEvent"]["Param"][1] = "EventDataAry"; // array type
		
		$method["iCalendarChooseUsers"]["class"] = "libeclass_ws.php";
		$method["iCalendarChooseUsers"]["constructor"] = "libeclass_ws()";
		$method["iCalendarChooseUsers"]["method"] = "WS_iCalendarChooseUsers";
		$method["iCalendarChooseUsers"]["Param"][0] = "CurrentUserID";
		$method["iCalendarChooseUsers"]["Param"][1] = "OptValue";
		$method["iCalendarChooseUsers"]["Param"][2] = "ChooseGroupID"; // array
		$method["iCalendarChooseUsers"]["Param"][3] = "ChooseUserID"; // array
		
		# 2019-05-09 : [END] iCalendar api
		
		# 2020-02-17 : Send Push Message from Classroom
		$method["SendPushMessageFromCourse"]["class"]       = "libeclass_ws.php";
		$method["SendPushMessageFromCourse"]["constructor"] = "libeclass_ws()";
		$method["SendPushMessageFromCourse"]["method"]      = "WS_SendPushMessageFromCourse";
		$method["SendPushMessageFromCourse"]["Param"][0]    = "CourseId";
		$method["SendPushMessageFromCourse"]["Param"][1]    = "FunctionType";
		$method["SendPushMessageFromCourse"]["Param"][2]    = "FunctionId";
		$method["SendPushMessageFromCourse"]["Param"][3]    = "LangSetting";
		$method["SendPushMessageFromCourse"]["Param"][4]    = "ToParentApp";
		$method["SendPushMessageFromCourse"]["Param"][5]    = "ToStudentApp";
        $method["SendPushMessageFromCourse"]["Param"][6]    = "PushMessageDate";

		# 2020-06-09: get session number for date
		$method["GetSessionNumberForDate"]["class"] = "libeclass_ws.php";
		$method["GetSessionNumberForDate"]["constructor"] = "libeclass_ws()";
		$method["GetSessionNumberForDate"]["method"] = "WS_GetSessionNumberForDate";
		$method["GetSessionNumberForDate"]["Param"][0] = "TargetDate";

		# 2020-07-08: get school day count for date
		$method["GetSchoolDayCount"]["class"] = "libeclass_ws.php";
		$method["GetSchoolDayCount"]["constructor"] = "libeclass_ws()";
		$method["GetSchoolDayCount"]["method"] = "WS_GetSchoolDayCount";
		$method["GetSchoolDayCount"]["Param"][0] = "StartDate";
		$method["GetSchoolDayCount"]["Param"][1] = "EndDate";


		# 2020-07-09: Receive body temperature record
		$method["SubmitBodyTemperatureRecord"]["class"] = "libeclass_ws.php";
		$method["SubmitBodyTemperatureRecord"]["constructor"] = "libeclass_ws()";
		$method["SubmitBodyTemperatureRecord"]["method"] = "WS_SubmitBodyTemperatureRecord";
		$method["SubmitBodyTemperatureRecord"]["Param"][0] = "ParentUserID";
		$method["SubmitBodyTemperatureRecord"]["Param"][1] = "StudentUserID";
		$method["SubmitBodyTemperatureRecord"]["Param"][2] = "TargetDate";
		$method["SubmitBodyTemperatureRecord"]["Param"][3] = "RecordTime";
		$method["SubmitBodyTemperatureRecord"]["Param"][4] = "TemperatureValue";


        # 2020-07-09: Get student body temperature record
		$method["GetStudentBodyTemperatureRecords"]["class"] = "libeclass_ws.php";
		$method["GetStudentBodyTemperatureRecords"]["constructor"] = "libeclass_ws()";
		$method["GetStudentBodyTemperatureRecords"]["method"] = "WS_GetStudentBodyTemperatureRecords";
		$method["GetStudentBodyTemperatureRecords"]["Param"][0] = "TargetUserID";

        # 2020-10-05 : Delete scheduled push message from Classroom
        $method["DeleteScheduledPushMessageFromCourse"]["class"]       = "libeclass_ws.php";
        $method["DeleteScheduledPushMessageFromCourse"]["constructor"] = "libeclass_ws()";
        $method["DeleteScheduledPushMessageFromCourse"]["method"]      = "WS_DeleteScheduledPushMessageFromCourse";
        $method["DeleteScheduledPushMessageFromCourse"]["Param"][0]    = "MessageIds";

		# 2020-08-11: add forget password version
		$method["ForgetPasswordVersion"]["class"] = "libeclass_ws.php";
		$method["ForgetPasswordVersion"]["constructor"] = "libeclass_ws()";
		$method["ForgetPasswordVersion"]["method"] = "WS_ForgetPasswordVersion";

		$method["ForgetPasswordV2"]["class"] = "libeclass_ws.php";
		$method["ForgetPasswordV2"]["constructor"] = "libeclass_ws()";
		$method["ForgetPasswordV2"]["method"] = "WS_ForgetPasswordV2";
		$method["ForgetPasswordV2"]["Param"][0] = "LoginID";
		$method["ForgetPasswordV2"]["Param"][1] = "UserEmail";
		
		$method["ApiKeyVersion"]["class"] = "libeclass_ws.php";
		$method["ApiKeyVersion"]["constructor"] = "libeclass_ws()";
		$method["ApiKeyVersion"]["method"] = "WS_ApiKeyVersion";

		/*
		# NOT USED ANYMORE -- testing code
		$method["UploadAsgnCommon"]["class"] = "libeclass_ws.php";
		$method["UploadAsgnCommon"]["constructor"] = "libeclass_ws()";
		$method["UploadAsgnCommon"]["method"] = "WS_UploadAsgnCommon";
		$method["UploadAsgnCommon"]["Param"][0] = "CourseID";
		$method["UploadAsgnCommon"]["Param"][1] = "UserEmail";
		$method["UploadAsgnCommon"]["Param"][2] = "AsgnID";
		$method["UploadAsgnCommon"]["Param"][3] = "FileName";
		$method["UploadAsgnCommon"]["Param"][4] = "FileByteStream";
		*/
				
		$this->RequestMethod = $method;
		
		## Error Code
		$ErrorArr['1'] = 'Unknown error';
		$ErrorArr['2'] = 'Service temporarily unavailable';
		$ErrorArr['3'] = 'Unknown method';
		$ErrorArr['4'] = 'Application request limit reached';
		$ErrorArr['5'] = 'Invalid session ID';
		$ErrorArr['6'] = 'Invalid pair of session ID and IP address';
		$ErrorArr['7'] = 'Invalid request ID';
		$ErrorArr['8'] = 'This method is deprecated';
		$ErrorArr['9'] = 'Application does not have permission for this action';		
		
		$ErrorArr['101'] = 'Invalid XML format';
		$ErrorArr['102'] = 'Invalid parameters';		
		$ErrorArr['103'] = 'Invalid Asgn ID';
		$ErrorArr['104'] = 'User Not Found';
		$ErrorArr['105'] = 'Invalid FileName';
		$ErrorArr['106'] = 'No Data Received';
		$ErrorArr['107'] = 'Chunk Corrupted';
		$ErrorArr['108'] = 'Some Chunk Missing';
		$ErrorArr['109'] = 'File Corrupted';
		
		$ErrorArr['201'] = 'Cannot view/modify other users\' record';
		$ErrorArr['202'] = 'User is not granted permission to access this module/method';
		$ErrorArr['203'] = 'Assignment is graded, cannot modify submission';
		$ErrorArr['204'] = 'Inconsistent IsCorrection status';
		
		$ErrorArr['301'] = 'Exceed maximum character';
		$ErrorArr['302'] = 'Invalid data type';		
		
		$ErrorArr['401'] = 'Invalid username or password';
		$ErrorArr['402'] = 'Invalid license';
		$ErrorArr['403'] = 'Invalid QR Code Login';
		$ErrorArr['409'] = 'Invalid version';
		$ErrorArr['410'] = 'Blacklisted User';
		
		$ErrorArr['501'] = 'Invalid Token';
		$ErrorArr['502'] = 'Permission to access this method is not granted';
		$ErrorArr['503'] = 'Token expired';
		
		$ErrorArr['601'] = 'No payment account';
		
		$ErrorArr['701'] = 'No such announcement type';
		$ErrorArr['702'] = 'No class teacher announcement';
		$ErrorArr['703'] = 'No school announcement';
		$ErrorArr['704'] = 'Student does not belong to any class';
		
		$ErrorArr['801'] = 'PowerLesson is not installed and PowerLesson App Connection is not enabled.';
		$ErrorArr['802'] = 'PowerLesson App Connection is not enabled.';
		$ErrorArr['803'] = 'PowerLesson App Connection is expired.';
		$ErrorArr['804'] = 'Classroom does not exist.';
		$ErrorArr['805'] = 'Lesson does not exist.';
		$ErrorArr['806'] = 'Session does not exist.';

		$ErrorArr['807'] = 'Fail to create';
		
		$ErrorArr['811'] = 'Operation Failed.'; // Fail to insert lesson plan
		$ErrorArr['812'] = 'Operation Failed.'; // Fail to insert lesson plan section
		$ErrorArr['813'] = 'Operation Failed.'; // Fail to insert note
		$ErrorArr['814'] = 'Operation Failed.'; // Fail to insert note section
		$ErrorArr['815'] = 'Operation Failed.'; // Fail to send push message
		
		$this->Error = $ErrorArr;
	}
	
	function callMethod($MethodName, $ParArr) {	
		global $UserID, $intranet_root;
		
		if($MethodName != '') {
			$Param = '';
			if(count($ParArr['eClassRequest']['Request']) > 0 && count($this->RequestMethod[$MethodName]["Param"]) > 0) {				
				for($i=0; $i<count($this->RequestMethod[$MethodName]["Param"]); $i++) {					
					
					# 2011-04-13: pass variable instead of string, so that argument can be array. i.e. request xml supports using subElement
					//$Param .= '\''.$ParArr['eClassRequest']['Request'][$this->RequestMethod[$MethodName]["Param"][$i]].'\',';						
					$Param .= '$ParArr[\'eClassRequest\'][\'Request\'][\''.$this->RequestMethod[$MethodName]["Param"][$i]. '\'],';
				}
			}			
			
			$Param = rtrim($Param, ',');
				
			$str = 'include_once "'.$this->RequestMethod[$MethodName]["class"].'"; ';
			$str .= '$newPage = new '.$this->RequestMethod[$MethodName]["constructor"].'; ';			
			$str .= '$result = $newPage->'.$this->RequestMethod[$MethodName]["method"].'('.$Param.'); ';

			eval($str);
		}
		return $result;
	}
	
	function Generate_RequestID() {
		$RequestID = 'req';	
		$RequestID .= Date('Y').Date('m').Date('d').time();
		
		return $RequestID;
	}
	
	function GetErrorArray($ErrorCode) {		
		$ReturnContent = array('Error'=>array('ErrorCode'=>$ErrorCode,
											  'ErrorDescription'=>$this->Error[$ErrorCode]
											  )
								);	
							
		return $ReturnContent;
	}
	
	
}
?>