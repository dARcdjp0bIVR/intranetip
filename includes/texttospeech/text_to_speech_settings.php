<?php
## Using By : 
##########################################################
## Modification Log
## 2009-12-23: Joe
## Changed the return data in JSON
##
## 2009-12-03: Joe
## Returns the wordings of the text to speech player button
## assume no escape characters in the wordings
##########################################################
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");

/*
echo $LAYOUT_SKIN.'||';
echo $Lang['TextToSpeech']['SpeechSetting'].'||';
echo $Lang['TextToSpeech']['FullText'].'||';
echo $Lang['TextToSpeech']['FullPage'].'||';
echo $Lang['TextToSpeech']['Highlighted'].'||';
echo $Lang['TextToSpeech']['HighlightedText'].'||';
echo $Lang['TextToSpeech']['Voice'].'||';
echo $Lang['TextToSpeech']['Male'].'||';
echo $Lang['TextToSpeech']['Female'].'||';
echo $Lang['TextToSpeech']['Speed'].'||';
echo $Lang['TextToSpeech']['Slow'].'||';
echo $Lang['TextToSpeech']['Fast'].'||';
echo $Lang['Btn']['Apply'].'||';
echo $Lang['Btn']['Cancel'].'||';
echo $Lang['TextToSpeech']['NoHighlightError'];
*/

function FormatJSONValue(&$str)
{
	$str = '"'.$str.'"';
}

$tts_exclude_chinese = array("—");

$excludeList = "[]";
$exclude_list = $tts_exclude_chinese;
if (is_array($exclude_list))
{
	array_walk($exclude_list, 'FormatJSONValue');
	$excludeList = '['.implode($exclude_list, ',').']';
}

/*if($intranet_session_language=='b5'){
	$SpeechSetting 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['SpeechSetting']);
	$FullText 		= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['FullText']);
	$FullPage 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['FullPage']);
	$Highlighted 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['Highlighted']);
	$HighlightedText 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['HighlightedText']);
	$Voice 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['Voice']);
	$Male 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['Male']);
	$Female 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['Female']);
	$Speed 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['Speed']);
	$Slowest 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['Slowest']);
	$Slow 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['Slow']);
	$Normal 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['Normal']);
	$Apply 	= iconv('Big5', 'UTF-8',$Lang['Btn']['Apply']);
	$Cancel 	= iconv('Big5', 'UTF-8',$Lang['Btn']['Cancel']);
	$NoHighlightError 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['NoHighlightError']);
	$NoChineseMaleVoice 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['NoChineseMaleVoice']);
	$Expired 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['Expired']);
	$NotLoggedIn 	= iconv('Big5', 'UTF-8',$Lang['TextToSpeech']['NotLoggedIn']);
}else{*/
	$SpeechSetting = $Lang['TextToSpeech']['SpeechSetting'];
	$FullText = $Lang['TextToSpeech']['FullText'];
	$FullPage = $Lang['TextToSpeech']['FullPage'];
	$Highlighted = $Lang['TextToSpeech']['Highlighted'];
	$HighlightedText = $Lang['TextToSpeech']['HighlightedText'];
	$Voice = $Lang['TextToSpeech']['Voice'];
	$Male = $Lang['TextToSpeech']['Male'];
	$Female = $Lang['TextToSpeech']['Female'];
	$Speed = $Lang['TextToSpeech']['Speed'];
	$Slowest = $Lang['TextToSpeech']['Slowest'];
	$Slow = $Lang['TextToSpeech']['Slow'];
	$Normal = $Lang['TextToSpeech']['Normal'];	
	$Apply = $Lang['Btn']['Apply'];
	$Cancel = $Lang['Btn']['Cancel'];
	$NoHighlightError = $Lang['TextToSpeech']['NoHighlightError'];
	$NoChineseMaleVoice = $Lang['TextToSpeech']['NoChineseMaleVoice'];
	$Expired = $Lang['TextToSpeech']['Expired'];
	 	
//}




echo '{
		"TTSSettings":
		{
			"settingsString":"Initialized",
			"layout":"'.$LAYOUT_SKIN.'",
			"txtSpeechSetting":"'.$SpeechSetting.'",
			"txtFullText":"'.$FullText.'",
			"txtFullPage":"'.$FullPage.'",
			"txtHighlighted":"'.$Highlighted.'",
			"txtHighlightedText":"'.$HighlightedText.'",
			"txtVoice":"'.$Voice.'",
			"txtMale":"'.$Male.'",
			"txtFemale":"'.$Female.'",
			"txtSpeed":"'.$Speed.'",
			"txtSlowest":"'.$Slowest.'",
			"txtSlow":"'.$Slow.'",
			"txtNormal":"'.$Normal.'",
			"txtApply":"'.$Apply.'",
			"txtCancel":"'.$Cancel.'",
			"txtNoHighlightError":"'.$NoHighlightError.'",
			"txtNoChineseMaleVoice":"'.$NoChineseMaleVoice.'",
			"txtExpired" : "'.$Expired.'",			
			"excludeChinese" : '.$excludeList.' 
						
		}
	   }';

?>