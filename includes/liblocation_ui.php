<?php
// Using: 
/********************************* Modification Log *************************************
 *
 * 2020-04-09 (Cameron)
 *      don't show BuildingName for ej in getBuildingFloorSelectionWithClass() to be consitent with web 
 *  
 * 2020-03-09 (Cameron)
 *      add parameter $WithOthersOpt=false to Get_Floor_Selection() so that it's consistent with that in ej
 *      
 * 2020-02-11 (Cameron)
 *      add parameter $class to Get_Floor_Selection(), Get_Room_Selection()
 *      
 * 2020-02-10 (Cameron)
 *      add parameter $class to Get_Building_Selection()
 *      
 * 2020-01-22 (Cameron)
 *      add function getBuildingFloorSelectionWithClass(), getRoomSelectionWithClass()
 *      
 * 2019-12-18 (Cameron)
 *      add function getSelection() and getBuildingFloorRoomSelectionWithClass()
 *
 * 2018-08-10 (Cameron)
 *      modify Get_Print_Content() and Get_Export_Content() to include suited course category for HKPF
 *      
 * 2018-07-31 (Cameron)
 *      Add function getEnrolCategorySelection()
 *      Apply getEnrolCategorySelection() to Get_Room_Add_Edit_Form_Div()
 *      
 * 2018-07-06 (Vito)
 *      Add print function
 * 
 * 2018-02-08 (Isaac)
 *      Modified Get_Export_Content(), export floor name in both Chinese and English even when no sub-location.
 * 
 * 2017-06-09 (Icarus)
 * 		Modified Get_Room_Add_Edit_Form_Div(), move the buttons to the bottom of the thickbox
 * 		Modified Last Updated Info Div, added the the action button > closebtn into Last Updated Info Div &
 * 		move it to the bottom of the thickbox, 
 * 		details:
 * 			Moved Last Updated Info Div of Get_Building_Add_Edit_Table() to Get_Building_Add_Edit_Form_Div()
 * 			Moved Last Updated Info Div of Get_Floor_Add_Edit_Table() to Get_Floor_Add_Edit_Form_Div()
 * 		
 * 2016-06-01 (Kenneth)
 * 		Modified: Get_Room_Add_Edit_Form_Div() & Get_Export_Content(), add capacity for room
 * 
 * 
 *	2013-05-22	(YatWoon)
 *		update Get_Building_Floor_Room_Selection(), add parameter $CanSelectWholeFloor, $multiple
 *
 *	2011-03-22	(YatWoon)
 *		update Get_Export_Content(), IP25 export standard
 *
 * 2011-02-22 (Yuen) : 
 * 		i. added Get_Building_Floor_Selection() to display building and floor list in one pull-down menu
 * 		ii. added Get_Building_Floor_Room_Selection() to display buildings, floors and rooms in one pull-down menu
 * 
 * 2010-12-06 (Carlos) : 
 * 		added Get_Export_Content() to export floor and room info to csv
 * 
 ****************************************************************************************/

include_once('liblocation.php');

if (!defined("LIBLOCATION_UI_DEFINED"))         // Preprocessor directives
{
	define("LIBLOCATION_UI_DEFINED",true);
	
	class liblocation_ui extends liblocation
    {
        var $thickBoxWidth;
        var $thickBoxHeight;
        var $toolCellWidth;
        var $Code_Maxlength;

        function liblocation_ui()
        {
            parent:: liblocation();

            $this->thickBoxWidth = 780;
            $this->thickBoxHeight = 450;
            $this->toolCellWidth = "120px";
            $this->Code_Maxlength = "10";
        }

        function Include_JS_CSS()
        {
            global $PATH_WRT_ROOT, $LAYOUT_SKIN;

            $x = '
				<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/' . $LAYOUT_SKIN . '/js/script.js"></script>
				<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.js"></script>
				<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.blockUI.js"></script>
				<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.jeditable.js"></script>
				<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.tablednd_0_5.js"></script>
				
				<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/thickbox.css" type="text/css" media="screen" />
				';

            return $x;
        }

        function Get_Building_Tab_Div($SelectedBuildingID, $viewOnly = false)
        {
            global $Lang;

            include_once("libinterface.php");
            $libinterface = new interface_html();

            # Get buildings
            $libBuilding = new Building();
            $buildingInfoArr = $libBuilding->Get_All_Building();
            $numOfBuilding = count($buildingInfoArr);

            if ($SelectedBuildingID == '')
                $SelectedBuildingID = $buildingInfoArr[0]['BuildingID'];

            $tab = "";
            $tab .= '<div class="function_tab">';
            $tab .= '<ul>';
            for ($i = 0; $i < $numOfBuilding; $i++) {
                $thisBuildingID = $buildingInfoArr[$i]['BuildingID'];
                $thisNameChi = $buildingInfoArr[$i]['NameChi'];
                $thisNameEng = $buildingInfoArr[$i]['NameEng'];
                $thisNameDisplay = Get_Lang_Selection($thisNameChi, $thisNameEng);

                # check if selected tab
                $thisClass = "";
                if ($SelectedBuildingID == $thisBuildingID)
                    $thisClass = ' class="selected" ';

                $tab .= '<li ' . $thisClass . '><a href="#" onclick="js_Switch_Building_Tab(' . $thisBuildingID . ')">' . $thisNameDisplay . '</a></li>';
            }

            if (!$viewOnly) {
                # Add Building Button
                $tab .= '<li>';
                $tab .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "dim_add", $Lang['SysMgr']['Location']['Add']['Building'],
                    "js_Show_Building_Add_Edit_Layer('1'); return false;");
                $tab .= '</li>';
                # Edit Building Button
                $tab .= '<li class="tab_settinga">';
                $tab .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "setting_row", $Lang['SysMgr']['Location']['Setting']['Building'],
                    "js_Show_Building_Add_Edit_Layer('0'); return false;");
                $tab .= '</li>';

                /*
                $tab .= '
                <li><a href="#" class="dim_add" title="Add Building/Area">&nbsp;</a></li>
                               <li class="tab_setting"><a href="#" title="Edit Building/Area">&nbsp;</a></li>
                ';
                */
            }
            $tab .= '</ul>';
            $tab .= '<br style="clear:both" />';
            $tab .= '</div>';

            return $tab;
        }

        function Get_Content_Table($BuildingID, $Keyword = "")
        {
            global $Lang;
            include_once("libinterface.php");

            $libinterface = new interface_html();

            $showRedWordRemark = false;
            $table = '';
            $table .= '<table class="common_table_list" id="ContentTable">' . "\n";
            if ($BuildingID == "") {
                $table .= '<tr><td>' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>' . "\n";
            } else {
                ## Standardize the display of IE and Firefox
                $table .= '<col align="left" style="width:10%" />' . "\n";
                $table .= '<col align="left" style="width:10%" />' . "\n";
                $table .= '<col align="left" style="width:10%" />' . "\n";
                $table .= '<col align="left" style="width:70%" />' . "\n";

                ## Header
                $table .= '<tr>' . "\n";
                $table .= '<th>' . $Lang['SysMgr']['Location']['Code'] . '</th>' . "\n";
                $table .= '<th>' . $Lang['SysMgr']['Location']['Barcode'] . '</th>' . "\n";
                $table .= '<th>' . $Lang['SysMgr']['Location']['Title'] . '</th>' . "\n";
                $table .= '<th class="sub_row_top" colspan="4">&nbsp;</th>' . "\n";
                $table .= '</tr>' . "\n";

                $table .= '<thead>' . "\n";
                $table .= '<tr>' . "\n";
                $table .= '<th colspan="3">' . "\n";
                $table .= '<span class="row_content" style="float:left">' . $Lang['SysMgr']['Location']['Floor'] . '</span>' . "\n";
                # Edit Floor Button
                $table .= '<span class="table_row_tool row_content_tool" style="float:right">' . "\n";
                $table .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "setting_row",
                    $Lang['SysMgr']['Location']['Setting']['Floor'], "js_Show_Floor_Add_Edit_Layer(); return false;");
                $table .= '</span>' . "\n";
                $table .= '</th>' . "\n";

                $table .= '<th class="sub_row_top">' . "\n";
                $table .= '<span class="row_content" style="float:left">' . $Lang['SysMgr']['Location']['Room'] . '</span>' . "\n";
                # Add Location
                $table .= '<span class="table_row_tool row_content_tool" style="float:right">' . "\n";
                $table .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add",
                    $Lang['SysMgr']['Location']['Add']['Room'], "js_Show_Room_Add_Edit_Layer('', ''); return false;");
                $table .= '</span>' . "\n";
                $table .= '</th>' . "\n";

                $table .= '</tr>' . "\n";
                $table .= '</thead>' . "\n";

                ## Content
                # Get Floor info of the building
                $buildingObj = new Building($BuildingID);
                $floorInfoArr = $buildingObj->Get_All_Floor();
                $numOfFloor = count($floorInfoArr);

                if ($numOfFloor == 0) {
                    $table .= '<tr><td colspan="100%" align="center">' . $Lang['General']['NoRecordAtThisMoment'] . '</td></tr>' . "\n";
                } else {
                    # Loop Floor
                    for ($i = 0; $i < $numOfFloor; $i++) {
                        $thisLocationLevelID = $floorInfoArr[$i]['LocationLevelID'];
                        $thisCode = Get_Table_Display_Content($floorInfoArr[$i]['Code']);
                        $thisBarcode = Get_Table_Display_Content($floorInfoArr[$i]['Barcode']);
                        $thisNameChi = $floorInfoArr[$i]['NameChi'];
                        $thisNameEng = $floorInfoArr[$i]['NameEng'];
                        $thisNameDisplay = Get_Table_Display_Content(Get_Lang_Selection($thisNameChi, $thisNameEng));

                        # Get Room info of the floor
                        $floorObj = new Floor($thisLocationLevelID);
                        $roomInfoArr = $floorObj->Get_All_Room();
                        $numOfRoom = count($roomInfoArr);

                        /* 22 May 2009 - Do not display duplicated code as red
                        # Check if the code is duplicated
                        $isDuplicateCode = $floorObj->Is_Code_Existed($thisCode, $thisLocationLevelID, "Floor");
                        if ($isDuplicateCode)
                        {
                            $showRedWordRemark = true;
                            $thisCode = '<font color="red">'.$thisCode.'</font>';
                        }
                        */

                        $RowSpan = ($numOfRoom > 0) ? 'rowspan="' . ($numOfRoom + 3) . '"' : '';
                        $FirstDisplay = ($numOfRoom > 0) ? 'display:none;' : '';

                        # Display Floor info
                        $table .= '<tbody>';

                        $table .= '<tr class="nodrag nodrop">' . "\n";
                        $table .= '<td ' . $RowSpan . '>' . $thisCode . '</td>' . "\n";
                        $table .= '<td ' . $RowSpan . '>' . $thisBarcode . '</td>' . "\n";
                        $table .= '<td ' . $RowSpan . '>' . $thisNameDisplay . '</td>' . "\n";

                        # Add Room Button
                        $table .= '<td width="10%" >' . "\n";
                        if ($numOfRoom == 0) {
                            $table .= "<div id='NumOfSubLocation_" . $thisLocationLevelID . "' style='display:inline; float: left;'>" . $Lang['SysMgr']['Location']['FieldTitle']['TotalNumOfSubLocation'] . ": <span class='num_of_room_" . $thisLocationLevelID . "'>0</span></div>";
                        } else {
                            $tempArr = array();
                            for ($j = 0; $j < $numOfRoom; $j++) {
                                $tempArr[] = $roomInfoArr[$j]['LocationID'];
                            }
                            if (sizeof($tempArr) > 0) {
                                $targetLocationID = implode(",", $tempArr);
                            }

                            $table .= "<div id='NumOfSubLocation_" . $thisLocationLevelID . "' style='display:inline; float: left;'>" . $Lang['SysMgr']['Location']['FieldTitle']['TotalNumOfSubLocation'] . ": <span class='num_of_room_" . $thisLocationLevelID . "'><span class='row_link_tool' style='display:inline-block;'><a href='#' id='zoom_" . $thisLocationLevelID . "' class='zoom_in' onClick='js_Show_Sub_Location(\"$thisLocationLevelID\",\"$targetLocationID\"); return false;' >$numOfRoom</a></span></span></div>";
                        }
                        $table .= '<div class="table_row_tool row_content_tool" style="display:inline; float: right;">' . "\n";
                        $table .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim",
                            $Lang['SysMgr']['Location']['Add']['RoomWithinFloor'], "js_Show_Room_Add_Edit_Layer('$thisLocationLevelID', ''); return false;");
                        $table .= '</div>' . "\n";
                        $table .= "<br><br><div id='sub_location_div_" . $thisLocationLevelID . "' style='display:none; '></div>";
                        $table .= '</td>' . "\n";
                        $table .= '</tr>' . "\n";

                        # the following code is diabled by Ronald (20091208)
                        /*
                        # Loop Room
                        for ($j=0; $j<$numOfRoom; $j++)
                        {
                            $thisLocationID 	= $roomInfoArr[$j]['LocationID'];
                            $thisCode 			= Get_Table_Display_Content($roomInfoArr[$j]['Code']);
                            $thisNameChi 		= $roomInfoArr[$j]['NameChi'];
                            $thisNameEng 		= $roomInfoArr[$j]['NameEng'];
                            $thisNameDisplay 	= Get_Table_Display_Content(Get_Lang_Selection($thisNameChi,$thisNameEng));
                            $thisDescription	= Get_Table_Display_Content($roomInfoArr[$j]['Description']);

                            $roomObj = new Room($thisLocationID);
                            $canDelete = $roomObj->Check_Can_Delete();

                            $thisDescription = str_replace("\n", "<br />", $thisDescription);

                            // 22 May 2009 - Do not display duplicated code as red
                            # Check if the code is duplicated
                            //$isDuplicateCode = $floorObj->Is_Code_Existed($thisCode, $thisLocationID, "Room");
                            //if ($isDuplicateCode)
                            //{
                            //	$showRedWordRemark = true;
                            //	$thisCode = '<font color="red">'.$thisCode.'</font>';
                            //}


                            $table .= '<tr class="sub_row" id="RoomRow_'.$thisLocationID.'">'."\n";
                                $table .= '<td>'.$thisCode.'</td>'."\n";
                                $table .= '<td>'.$thisNameDisplay.'</td>'."\n";
                                $table .= '<td>'.$thisDescription.'</td>'."\n";
                                $table .= '<td class="Dragable">'."\n";
                                    $table .= '<div class="table_row_tool">'."\n";
                                        # Move
                                        $table .= $libinterface->GET_LNK_MOVE("#", $Lang['SysMgr']['Location']['Reorder']['Room']);
                                        # Edit Room Button
                                        $table .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit_dim",
                                                    $Lang['SysMgr']['Location']['Edit']['Room'], "js_Show_Room_Add_Edit_Layer('$thisLocationLevelID', '$thisLocationID'); return false;");
                                        if ($canDelete)
                                        {
                                            # Delete Room Button
                                            $table .= $libinterface->GET_LNK_DELETE("#", $Lang['SysMgr']['Location']['Delete']['Room'], "js_Delete_Room('$thisLocationID'); return false;");
                                        }
                                    $table .= '</div>'."\n";
                                $table .= '</td>'."\n";
                            $table .= '</tr>'."\n";
                        }
                        */

                        ### Disabled By Ronald (20091215)
                        /*
                        if($numOfRoom > 0)
                        {
                            $tempArr = array();
                            for ($j=0; $j<$numOfRoom; $j++) {
                                $tempArr[] = $roomInfoArr[$j]['LocationID'];
                            }
                            if(sizeof($tempArr)>0) {
                                $targetLocationID = implode(",",$tempArr);
                            }
                            $table .= "<tr><td align='left'><div style='display:inline; float: left;'>".$Lang['SysMgr']['Location']['FieldTitle']['TotalNumOfSubLocation'].": <span class='num_of_room_".$thisLocationLevelID."'><span class='row_link_tool' style='display:inline-block;'><a href='#' id='zoom_".$thisLocationLevelID."' class='zoom_in' onClick='js_Show_Sub_Location(\"$thisLocationLevelID\",\"$targetLocationID\"); return false;' >$numOfRoom</a></span></span></div>";
                            $table .= '<div class="table_row_tool row_content_tool">'."\n";
                            $table .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim",
                                    $Lang['SysMgr']['Location']['Add']['RoomWithinFloor'], "js_Show_Room_Add_Edit_Layer('$thisLocationLevelID', ''); return false;");
                            $table .= '</div>'."\n";
                            $table .= "</td></tr>";
                            //$table .= "<tr class='sub_row' id='sub_location_div_".$thisLocationLevelID."' style='display:none'><td colspan='4' align='left' style='padding:0px; spacing:0px; border:0px;'><div ></div></td></tr>";
                            $table .= "<tr class='sub_row' id='sub_location_div_".$thisLocationLevelID."' style='display:none'></tr>";
                        }

                        if ($numOfRoom > 0)
                        {
                            $table .= '<tr class="nodrag nodrop">'."\n";
                                //$table .= '<td>&nbsp;</td>'."\n";
                                //$table .= '<td>&nbsp;</td>'."\n";
                                //$table .= '<td>&nbsp;</td>'."\n";
                                # Add Room Button
                                $table .= '<td>'."\n";
                                    $table .= '<div class="table_row_tool row_content_tool">'."\n";
                                        $table .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim",
                                                $Lang['SysMgr']['Location']['Add']['RoomWithinFloor'], "js_Show_Room_Add_Edit_Layer('$thisLocationLevelID', ''); return false;");
                                    $table .= '</div>'."\n";
                                $table .= '</td>'."\n";
                            $table .= '</tr>'."\n";
                        }
                        */

                        $table .= '</tbody>' . "\n";
                    } // End loop floor
                }
            } // End if (BuildingID == "")
            $table .= '</table>' . "\n";

            $table .= '<div class="FakeLayer"></div>' . "\n";

            /* 22 May 2009 - Hide the warning message
            # Use this hidden input to communicate with the index page
            # So that the index page knows to show the warning box or not
            $showWarningInputValue = ($showRedWordRemark)? '1' : '0';
            $showWarningInput = '';
            $showWarningInput .= '<input type="hidden" id="showWarningInput" name="showWarningInput" value="'.$showWarningInputValue.'" />';
            */

            return $table;
        }

        function Get_Building_Add_Edit_Form_Div()
        {
            global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
            include_once("libinterface.php");
            $libinterface = new interface_html();
            $libBuilding = new Building();

            $x = "";
            $x .= '<div id="debugArea"></div>';

            $x .= '<div class="edit_pop_board" style="height:410px;">';
            $x .= $libinterface->Get_Thickbox_Return_Message_Layer();
            //$x .= '<h1> '.$Lang['SysMgr']['Location']['Location'].' &gt; <span>'.$Lang['SysMgr']['Location']['Setting']['Building'].'</span></h1>';
            $x .= '<div id="BuildingInfoSettingLayer" class="edit_pop_board_write" style="height:330px;">';
            $x .= $this->Get_Building_Add_Edit_Table();
            $x .= '</div>';


            #the action button > close
            $htmlAry['closeBtn'] = $libinterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'closeBtn_tb', $ParOtherAttribute = "", $ParDisabled = 0, $ParClass = "", $ParExtraClass = "actionBtn");


            # Last Updated Info Div
            $LastModifiedInfoArr = $libBuilding->Get_Last_Modified_Info();
            if ($LastModifiedInfoArr != NULL) {
                $lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
                $lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
                $lastModifiedName = $this->Get_UserName_By_UserID($lastModifiedBy);

                $LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, $lastModifiedName);
                $x .= '<div class="edit_bottom_v30">';
                $x .= '<span>' . $LastModifiedDisplay . '</span>';
                $x .= '<p class="spacer"></p>';
                $x .= $htmlAry['closeBtn'];
                $x .= '<p class="spacer"></p>';
                $x .= '</div>';
            }
            $x .= '</div>';


            return $x;
        }

        function Get_Building_Add_Edit_Table()
        {
            global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;

            include_once("libinterface.php");
            $libinterface = new interface_html();

            $libBuilding = new Building();
            $buildingInfoArr = $libBuilding->Get_All_Building();
            $numOfBuilding = count($buildingInfoArr);

            $x .= '<table id="BuildingInfoTable" class="common_table_list" style="width:95%">';
            $x .= '<thead>';
            $x .= '<tr>';
            $x .= '<th style="width:15%">' . $Lang['SysMgr']['Location']['Code'] . '</th>';
            $x .= '<th style="width:15%">' . $Lang['SysMgr']['Location']['Barcode'] . '</th>';
            $x .= '<th style="width:25%">' . $Lang['SysMgr']['Location']['TitleEn'] . '</th>';
            $x .= '<th style="width:25%">' . $Lang['SysMgr']['Location']['TitleCh'] . '</th>';
            $x .= '<th style="width:' . $this->toolCellWidth . '">&nbsp;</th>';
            $x .= '</tr>';
            $x .= '</thead>';

            $x .= '<tbody>';

            # Display Building Info
            if ($numOfBuilding > 0) {
                $moveToolTips = $Lang['SysMgr']['Location']['Reorder']['Building'];
                $deleteToolTips = $Lang['SysMgr']['Location']['Delete']['Building'];

                for ($i = 0; $i < $numOfBuilding; $i++) {
                    $thisBuildingID = $buildingInfoArr[$i]['BuildingID'];
                    $thisCode = $buildingInfoArr[$i]['Code'];
                    $thisBarcode = $buildingInfoArr[$i]['Barcode'];
                    $thisTitleEn = $buildingInfoArr[$i]['NameEng'];
                    $thisTitleCh = $buildingInfoArr[$i]['NameChi'];

                    # show delete icon if the building has no linked floor
                    $thisLibBuilding = new Building($thisBuildingID);
                    $floorInfoArr = $thisLibBuilding->Get_All_Floor();
                    $numOfFloor = count($floorInfoArr);
                    $showDeleteIcon = ($numOfFloor == 0) ? true : false;

                    /* 22 May 2009 - Do not display duplicated code as red
                    # Check if the code is duplicated
                    $isDuplicateCode = $thisLibBuilding->Is_Code_Existed($thisCode, $thisBuildingID, "Building");
                    if ($isDuplicateCode)
                        $jEditStyle_Code = $this->Get_jEdit_Style('red');
                    else
                        $jEditStyle_Code = $this->Get_jEdit_Style();
                    */

                    $x .= '<tr id="BuildingRow_' . $thisBuildingID . '">';
                    # Code
                    $x .= '<td>';
                    $x .= $libinterface->Get_Thickbox_Edit_Div("BuildingCode_" . $thisBuildingID, "BuildingCode_" . $thisBuildingID, "jEditCode", $thisCode);
                    $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv_" . $thisBuildingID);
                    $x .= '</td>';
                    # Barcode
                    $x .= '<td>';
                    $x .= $libinterface->Get_Thickbox_Edit_Div("BuildingBarcode_" . $thisBuildingID, "BuildingBarcode_" . $thisBuildingID, "jEditBarcode", $thisBarcode);
                    $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("BarcodeWarningDiv_" . $thisBuildingID);
                    $x .= '</td>';
                    # Title (English)
                    $x .= '<td>';
                    $x .= $libinterface->Get_Thickbox_Edit_Div("BuildingTitleEn_" . $thisBuildingID, "BuildingTitleEn_" . $thisBuildingID, "jEditTitleEn", $thisTitleEn);
                    $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleEnWarningDiv_" . $thisBuildingID);
                    $x .= '</td>';
                    # Title (Chinese)
                    $x .= '<td>';
                    $x .= $libinterface->Get_Thickbox_Edit_Div("BuildingTitleCh_" . $thisBuildingID, "BuildingTitleCh_" . $thisBuildingID, "jEditTitleCh", $thisTitleCh);
                    $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleChWarningDiv_" . $thisBuildingID);
                    $x .= '</td>';

                    # Functional icons
                    $x .= '<td class="Dragable" align="left">';
                    # Move
                    $x .= $libinterface->GET_LNK_MOVE("#", $moveToolTips);
                    # Delete
                    if ($showDeleteIcon) {
                        $x .= $libinterface->GET_LNK_DELETE("#", $deleteToolTips, "js_Delete_Building('$thisBuildingID'); return false;");
                    }
                    $x .= '</td>';
                    $x .= '</tr>';
                }
            }

            # Add Building Button
            $x .= '<tr id="AddBuildingRow" class="nodrop nodrag">';
            $x .= '<td>&nbsp;</td>';
            $x .= '<td>&nbsp;</td>';
            $x .= '<td>&nbsp;</td>';
            $x .= '<td>&nbsp;</td>';
            $x .= '<td>';
            $x .= '<div class="table_row_tool row_content_tool">';
            $x .= '<a href="#" onclick="js_Add_Building_Row(); return false;" class="add_dim" title="' . $Lang['SysMgr']['Location']['Add']['Building'] . '"></a>';
            $x .= '</div>';
            $x .= '</td>';
            $x .= '</tr>';
            $x .= '</tbody>';
            $x .= '</table>';

            $x .= '<br />';


            /********************    ORIGINAL CODE  >>> now moved to [Get_Building_Add_Edit_Form_Div()]
             * # Last Updated Info Div
             * $LastModifiedInfoArr = $libBuilding->Get_Last_Modified_Info();
             * if ($LastModifiedInfoArr != NULL)
             * {
             * $lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
             * $lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
             * $lastModifiedName = $this->Get_UserName_By_UserID($lastModifiedBy);
             *
             * $LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, $lastModifiedName);
             * $x .= '<div class="edit_bottom">';
             * $x .= '<span>'.$LastModifiedDisplay.'</span>';
             * $x .= '<p class="spacer"></p>';
             * $x .= '</div>';
             * }
             ************************************************************************/

            return $x;
        }

        function Get_Floor_Add_Edit_Form_Div($BuildingID)
        {
            global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $intranet_session_language;
            include_once("libinterface.php");
            $libBuilding = new Building($BuildingID);
            $builidingName = $libBuilding->Get_Name($intranet_session_language);

            $floorInfoArr = $libBuilding->Get_All_Floor();
            $numOfFloor = count($floorInfoArr);

            $libinterface = new interface_html();

            $x = "";
            $x .= '<div id="debugArea"></div>';
            $x .= '<div class="edit_pop_board" style="height:410px;">';
            $x .= $libinterface->Get_Thickbox_Return_Message_Layer();
            //$x .= '<h1>'.$Lang['SysMgr']['Location']['Location'].' &gt; '.$builidingName.' &gt; <span>'.$Lang['SysMgr']['Location']['Setting']['Floor'].'</span></h1>';
            $x .= '<div id="FloorInfoSettingLayer" class="edit_pop_board_write" style="height:330px;">';
            $x .= $this->Get_Floor_Add_Edit_Table($BuildingID);
            $x .= '</div>';


            #the action button > close
            $htmlAry['closeBtn'] = $libinterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'closeBtn_tb', $ParOtherAttribute = "", $ParDisabled = 0, $ParClass = "", $ParExtraClass = "actionBtn");


            # Last Updated Info
            $libFloor = new Floor();
            $LastModifiedInfoArr = $libFloor->Get_Last_Modified_Info($BuildingID);
            if ($LastModifiedInfoArr != NULL) {
                $lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
                $lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
                $lastModifiedName = $this->Get_UserName_By_UserID($lastModifiedBy);

                $LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, $lastModifiedName);
                $x .= '<div class="edit_bottom_v30">';
                $x .= '<span>' . $LastModifiedDisplay . '</span>';
                $x .= '<p class="spacer"></p>';
                $x .= $htmlAry['closeBtn'];
                $x .= '<p class="spacer"></p>';
                $x .= '</div>';
            }


            $x .= '</div>';
            return $x;
        }

        function Get_Floor_Add_Edit_Table($BuildingID)
        {
            global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
            include_once("libinterface.php");

            $libinterface = new interface_html();

            $libBuilding = new Building($BuildingID);
            $floorInfoArr = $libBuilding->Get_All_Floor();
            $numOfFloor = count($floorInfoArr);

            $x = "";
            $x .= '<table id="FloorInfoTable" class="common_table_list" style="width:95%">';
            $x .= '<thead>';
            $x .= '<tr>';
            $x .= '<th style="width:15%">' . $Lang['SysMgr']['Location']['Code'] . '</th>';
            $x .= '<th style="width:15%">' . $Lang['SysMgr']['Location']['Barcode'] . '</th>';
            $x .= '<th style="width:25%">' . $Lang['SysMgr']['Location']['TitleEn'] . '</th>';
            $x .= '<th style="width:25%">' . $Lang['SysMgr']['Location']['TitleCh'] . '</th>';
            $x .= '<th style="width:' . $this->toolCellWidth . '">&nbsp;</th>';
            $x .= '</tr>';
            $x .= '</thead>';

            $x .= '<tbody>';

            # Display Floor Info
            if ($numOfFloor > 0) {
                $jEditStyle = $this->Get_jEdit_Style();
                $moveToolTips = $Lang['SysMgr']['Location']['Reorder']['Floor'];
                $deleteToolTips = $Lang['SysMgr']['Location']['Delete']['Floor'];

                for ($i = 0; $i < $numOfFloor; $i++) {
                    $thisLocationLevelID = $floorInfoArr[$i]['LocationLevelID'];
                    $thisCode = $floorInfoArr[$i]['Code'];
                    $thisBarcode = $floorInfoArr[$i]['Barcode'];
                    $thisTitleEn = $floorInfoArr[$i]['NameEng'];
                    $thisTitleCh = $floorInfoArr[$i]['NameChi'];

                    # show delete icon if the building has no linked floor
                    $thisLibFloor = new Floor($thisLocationLevelID);
                    $roomInfoArr = $thisLibFloor->Get_All_Room();
                    $numOfRoom = count($roomInfoArr);
                    $showDeleteIcon = ($numOfRoom == 0) ? true : false;

                    /* 22 May 2009 - Do not display duplicated code as red
                    # Check if the code is duplicated
                    $isDuplicateCode = $thisLibFloor->Is_Code_Existed($thisCode, $thisLocationLevelID, "Floor");
                    if ($isDuplicateCode)
                        $jEditStyle_Code = $this->Get_jEdit_Style('red');
                    else
                        $jEditStyle_Code = $this->Get_jEdit_Style();
                    */

                    $x .= '<tr id="FloorRow_' . $thisLocationLevelID . '">';
                    # Code
                    $x .= '<td>';
                    $x .= $libinterface->Get_Thickbox_Edit_Div("FloorCode_" . $thisLocationLevelID, "FloorCode_" . $thisLocationLevelID, "jEditCode", $thisCode);
                    $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv_" . $thisLocationLevelID);
                    $x .= '</td>';
                    # Barcode
                    $x .= '<td>';
                    $x .= $libinterface->Get_Thickbox_Edit_Div("FloorBarcode_" . $thisLocationLevelID, "FloorBarcode_" . $thisLocationLevelID, "jEditBarcode", $thisBarcode);
                    $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("BarcodeWarningDiv_" . $thisLocationLevelID);
                    $x .= '</td>';
                    # Title (English)
                    $x .= '<td>';
                    $x .= $libinterface->Get_Thickbox_Edit_Div("FloorTitleEn_" . $thisLocationLevelID, "FloorTitleEn_" . $thisLocationLevelID, "jEditTitleEn", $thisTitleEn);
                    $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleEnWarningDiv_" . $thisLocationLevelID);
                    $x .= '</td>';
                    # Title (Chinese)
                    $x .= '<td>';
                    $x .= $libinterface->Get_Thickbox_Edit_Div("FloorTitleCh_" . $thisLocationLevelID, "FloorTitleCh_" . $thisLocationLevelID, "jEditTitleCh", $thisTitleCh);
                    $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleChWarningDiv_" . $thisLocationLevelID);
                    $x .= '</td>';

                    # Functional icons
                    $x .= '<td class="Dragable" align="left">';
                    # Move
                    $x .= $libinterface->GET_LNK_MOVE("#", $moveToolTips);
                    # Delete
                    if ($showDeleteIcon) {
                        $x .= $libinterface->GET_LNK_DELETE("#", $deleteToolTips, "js_Delete_Floor('$thisLocationLevelID'); return false;");
                    }
                    $x .= '</td>';
                    $x .= '</tr>';
                }
            }

            # Add Floor Button
            $x .= '<tr id="AddFloorRow" class="nodrop nodrag">';
            $x .= '<td>&nbsp;</td>';
            $x .= '<td>&nbsp;</td>';
            $x .= '<td>&nbsp;</td>';
            $x .= '<td>&nbsp;</td>';
            $x .= '<td>';
            $x .= '<div class="table_row_tool row_content_tool">';
            $x .= '<a href="#" onclick="js_Add_Floor_Row(); return false;" class="add_dim" title="' . $Lang['SysMgr']['Location']['Add']['Floor'] . '"></a>';
            $x .= '</div>';
            $x .= '</td>';
            $x .= '</tr>';

            $x .= '</tbody>';
            $x .= '</table>';

            $x .= '<br />';


            /********************    ORIGINAL CODE  >>> now moved to [Get_Floor_Add_Edit_Form_Div()]
             * # Last Updated Info
             * $libFloor = new Floor();
             * $LastModifiedInfoArr = $libFloor->Get_Last_Modified_Info($BuildingID);
             * if ($LastModifiedInfoArr != NULL)
             * {
             * $lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
             * $lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
             * $lastModifiedName = $this->Get_UserName_By_UserID($lastModifiedBy);
             *
             * $LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, $lastModifiedName);
             * $x .= '<div class="edit_bottom">';
             * $x .= '<span>'.$LastModifiedDisplay.'</span>';
             * $x .= '<p class="spacer"></p>';
             * $x .= '</div>';
             * }
             ************************************************************************/


            return $x;
        }

        function Get_Room_Add_Edit_Form_Div($LocationID = '', $LocationLevelID = '', $BuildingID = '')
        {
            global $Lang, $PATH_WRT_ROOT, $intranet_session_language, $sys_custom;
            include_once("libinterface.php");

            $libinterface = new interface_html();

            # Add / Edit
            $isEdit = ($LocationID == '') ? false : true;

            # Title
            $thisTitle = ($isEdit) ? $Lang['SysMgr']['Location']['Edit']['Room'] : $Lang['SysMgr']['Location']['Add']['Room'];

            # Building Selection
            $buildingSelection = $this->Get_Building_Selection($BuildingID, "BuildingSelected", "js_Update_Floor_Selection()", 0, 0, '', 0);

            # Floor Selection
            $floorSelection = $this->Get_Floor_Selection($BuildingID, $LocationLevelID, "FloorSelected", '', 0, 1);

            if ($isEdit) {
                # Button Submit
                $btn_Edit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button",
                    $onclick = "Insert_Edit_Room('" . $LocationID . "', 0)", $id = "Btn_Edit");
            } else {
                # Button Add & Finish
                $btn_Add_Finish = $libinterface->GET_ACTION_BTN($Lang['SysMgr']['Location']['Button']['Add&Finish'], "button",
                    $onclick = "Insert_Edit_Room('', 0)", $id = "Btn_AddFinish");
                # Button Add & Add More Location
                $btn_Add_AddMore = $libinterface->GET_ACTION_BTN($Lang['SysMgr']['Location']['Button']['Add&AddMoreLocation'], "button",
                    $onclick = "Insert_Edit_Room('', 1)", $id = "Btn_AddFinish");
            }
            # Reset
            //$btn_Reset = $libinterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "button",
            //					$onclick="document.LocationForm.reset(); $('#CodeWarningDiv').hide('fast');", $id="Btn_Reset");
            # Cancel
            $btn_Cancel = $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button",
                $onclick = "js_Hide_ThickBox()", $id = "Btn_Cancel");

            # Show Room info if it is edit mode
            if ($isEdit) {
                $thisRoomObj = new Room($LocationID);
                $thisCode = intranet_htmlspecialchars($thisRoomObj->Code);
                $thisBarcode = intranet_htmlspecialchars($thisRoomObj->Barcode);
                $thisNameEng = intranet_htmlspecialchars($thisRoomObj->NameEng);
                $thisNameChi = intranet_htmlspecialchars($thisRoomObj->NameChi);
                $thisDescription = $thisRoomObj->Description;
                $thisCapacity = $thisRoomObj->Capacity;
            }

            $x = '';
            $x .= '<div id="debugArea"></div>';
            $x .= '<div class="edit_pop_board edit_pop_board_reorder" style="height:380px">' . "\n";
            $x .= $libinterface->Get_Thickbox_Return_Message_Layer();
            //$x .= '<h1> '.$Lang['SysMgr']['Location']['Location'].' &gt; <span>'.$thisTitle.'</span></h1>'."\n";
            $x .= '<div class="edit_pop_board_write" style="height:330px">' . "\n";
            $x .= '<form id="LocationForm" name="LocationForm">' . "\n";
            $x .= '<table class="form_table">' . "\n";
            $x .= '<col class="field_title" />' . "\n";
            $x .= '<col class="field_c" />' . "\n";

            # Building Selection
            $x .= '<tr>' . "\n";
            $x .= '<td>' . $Lang['SysMgr']['Location']['Building'] . '</td>' . "\n";
            $x .= '<td>:</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= $buildingSelection . "\n";
            $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("BuildingWarningDiv") . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            # Floor Selection
            $x .= '<tr>' . "\n";
            $x .= '<td>' . $Lang['SysMgr']['Location']['Floor'] . '</td>' . "\n";
            $x .= '<td>:</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<div id="FloorSelectionDiv">' . "\n";
            $x .= $floorSelection . "\n";
            $x .= '</div>' . "\n";
            $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("FloorWarningDiv") . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            # Code input
            $x .= '<tr>' . "\n";
            $x .= '<td>' . $Lang['SysMgr']['Location']['Code'] . '</td>' . "\n";
            $x .= '<td>:</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<input id="RoomCode" name="RoomCode" type="text" class="textbox" maxlength="' . $this->Code_Maxlength . '" value="' . $thisCode . '" />' . "\n";
            $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv");
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            # Barcode input
            $x .= '<tr>' . "\n";
            $x .= '<td>' . $Lang['SysMgr']['Location']['Barcode'] . '</td>' . "\n";
            $x .= '<td>:</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<input id="RoomBarcode" name="RoomBarcode" type="text" class="textbox" maxlength="' . $this->Code_Maxlength . '" value="' . $thisBarcode . '" />' . "\n";
            $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("BarcodeWarningDiv");
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            # TitleEn input
            $x .= '<tr>' . "\n";
            $x .= '<td>' . $Lang['SysMgr']['Location']['TitleEn'] . '</td>' . "\n";
            $x .= '<td>:</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<input id="RoomTitleEn" name="RoomTitleEn" type="text" class="textbox" value="' . $thisNameEng . '" />' . "\n";
            $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleEnWarningDiv");
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            # TitleCh input
            $x .= '<tr>' . "\n";
            $x .= '<td>' . $Lang['SysMgr']['Location']['TitleCh'] . '</td>' . "\n";
            $x .= '<td>:</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<input id="RoomTitleCh" name="RoomTitleCh" type="text" class="textbox" value="' . $thisNameChi . '" />' . "\n";
            $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleChWarningDiv");
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            # Desciption input
            $x .= '<tr>' . "\n";
            $x .= '<td>' . $Lang['SysMgr']['Location']['Description'] . '</td>' . "\n";
            $x .= '<td>:</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= $libinterface->GET_TEXTAREA("RoomDescription", $thisDescription);
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";
            # Capacity input
            $x .= '<tr>' . "\n";
            $x .= '<td>' . $Lang['SysMgr']['Location']['Capacity'] . '</td>' . "\n";
            $x .= '<td>:</td>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<input id="Capacity" name="Capacity" type="text" class="textbox" value="' . $thisCapacity . '" />' . "\n";
            $x .= $libinterface->Get_Thickbox_Warning_Msg_Div("CapacityWarningDiv") . "\n";
            $x .= '</td>' . "\n";
            $x .= '</tr>' . "\n";

            # Location Category Selection
            if ($sys_custom['project']['HKPF']) {
                $x .= '<tr>' . "\n";
                $x .= '<td>' . $Lang['eEnrolment']['category']['applyToCourseCategory'] . '</td>' . "\n";
                $x .= '<td>:</td>' . "\n";
                $x .= '<td>' . "\n";
                $x .= $this->getEnrolCategorySelection($LocationID);
                $x .= '</td>' . "\n";
                $x .= '</tr>' . "\n";
            }

            $x .= '</table>' . "\n";
            $x .= '</form>' . "\n";
            $x .= '</div>' . "\n";


            $x .= '</div>';


            $x .= '<div class="edit_bottom_v30">';
            $x .= '<span></span>';
            $x .= '<p class="spacer"></p>';
            if ($isEdit) {
                $x .= $btn_Edit . "\n";
            } else {
                $x .= $btn_Add_Finish . "\n";
                $x .= $btn_Add_AddMore . "\n";
            }
            $x .= $btn_Reset . "\n";
            $x .= $btn_Cancel . "\n";
            $x .= '<p class="spacer"></p>';
            $x .= '</div>';


#			$x .= '</div>';

            return $x;
        }

        function Get_jEdit_Style($FontColor = "")
        {
            global $PATH_WRT_ROOT, $LAYOUT_SKIN;

            $style = "";
            $style .= 'background-image:url(' . $PATH_WRT_ROOT . '/images/' . $LAYOUT_SKIN . '/icon_edit_b.gif);';
            $style .= 'background-position:center right;';
            $style .= 'background-repeat:no-repeat;';

            if ($FontColor != "")
                $style .= 'color:' . $FontColor . ';';

            return $style;
        }

        function Get_UserName_By_UserID($UserID)
        {
            $nameField = getNameFieldByLang();
            $table = $this->DBName . '.INTRANET_USER';
            $sql = 'Select ' . $nameField . ' From ' . $table . ' Where UserID = \'' . $UserID . '\' ';
            $nameArr = $this->returnVector($sql);
            $name = $nameArr[0];

            return $name;
        }

        function Get_Building_Selection($SelectedBuildingID = '', $ParID = 'BuildingSelected', $ParOnchange = '', $noFirst = 0, $WithOthersOpt = 0, $ParFirstTitle = '', $HasFloorOnly = 1, $class='')
        {
            global $Lang;

            $libBuilding = new Building();
            $BuildingArr = $libBuilding->Get_All_Building(0, $HasFloorOnly);
            $numOfBuilding = count($BuildingArr);

            $selectArr = array();
            for ($i = 0; $i < $numOfBuilding; $i++) {
                $thisBuildingID = $BuildingArr[$i]['BuildingID'];
                $thisNameEng = $BuildingArr[$i]['NameEng'];
                $thisNameChi = $BuildingArr[$i]['NameChi'];
                $thisBuildingName = Get_Lang_Selection($thisNameChi, $thisNameEng);

                $objBuilding = new Building($thisBuildingID);
                $FloorArr = $objBuilding->Get_All_Floor();
                $numOfFloor = count($FloorArr);

                if ($HasFloorOnly && $numOfFloor == 0)
                    continue;

                $selectArr[$thisBuildingID] = $thisBuildingName;
            }

            if ($WithOthersOpt)
                $selectArr['-1'] = $Lang['SysMgr']['Location']['OthersLocation'] . "...";

            $onchange = '';
            if ($ParOnchange != "")
                $onchange = 'onchange="' . $ParOnchange . '"';

            $selectionTags = ' id="' . $ParID . '" name="' . $ParID . '" ' . $onchange;
            if ($class) {
                $selectionTags .= ' class="'.$class.'"';
            }
            $firstTitle = ($ParFirstTitle == '') ? $Lang['SysMgr']['Location']['Select']['Building'] : $ParFirstTitle;
            $firstTitle = Get_Selection_First_Title($firstTitle);

            $buildingSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedBuildingID, $all = 0, $noFirst, $firstTitle);

            return $buildingSelection;
        }


        function Get_Building_Floor_Room_Selection($SelectedLocationLevelID = '', $ParID = 'RoomSelected', $ParOnchange = '', $ParNoFirst = 0, $ShowNoRoomFloor = 0, $ParTitle = '', $GivenBuildings = "", $GivenLocationLevel = "", $GivenLocations = "", $CanSelectWholeFloor = 0, $multiple = 0)
        {
            global $intranet_session_language, $Lang;

            $objBuilding = new Building();
            $BuildingArr = $objBuilding->Get_All_Building(0, 1);
            $numOfBuilding = count($BuildingArr);

            $selectionArr = array();

            if (!$ParNoFirst) {
                $firstTitle = ($ParTitle == '') ? "-- " . $Lang['SysMgr']['Location']['Select']['Floor'] . " --" : $ParTitle;
                $selectionArr[] = array("", $firstTitle);
            }

            $objFloor = new Floor();

            for ($i = 0; $i < $numOfBuilding; $i++) {
                $thisBuildingID = $BuildingArr[$i]['BuildingID'];

                # skip this building for management group if no right
                $DisplayThisBuilding = true;
                if ($GivenBuildings != "" && is_array($GivenBuildings)) {
                    $DisplayThisBuilding = false;
                    # match
                    for ($j = 0; $j < sizeof($GivenBuildings); $j++) {
                        if ($GivenBuildings[$j] == $thisBuildingID) {
                            $DisplayThisBuilding = true;
                            break;
                        }
                    }
                }
                if (!$DisplayThisBuilding) {
                    continue;
                }

                $thisNameEng = $BuildingArr[$i]['NameEng'];
                $thisNameChi = $BuildingArr[$i]['NameChi'];
                $thisBuildingName = Get_Lang_Selection($thisNameChi, $thisNameEng);

                $FloorArr = $objBuilding->Get_All_Floor_Cached(0, $thisBuildingID);
                $numOfFloor = count($FloorArr);

                if (!$ParNoFirst) {
                    $selectionArr[] = array("OPTGROUP", "=======================");
                }

                $selectionArr[] = array("OPTGROUP", "[ " . $thisBuildingName . " ]");


                for ($j = 0; $j < $numOfFloor; $j++) {
                    $thisLocationLevelID = $FloorArr[$j]['LocationLevelID'];

                    # skip this level for management group if no right
                    $DisplayThisLevel = true;
                    if ($GivenLocationLevel != "" && is_array($GivenLocationLevel)) {
                        $DisplayThisLevel = false;
                        # match
                        for ($k = 0; $k < sizeof($GivenLocationLevel); $k++) {
                            if ($GivenLocationLevel[$k] == $thisLocationLevelID) {
                                $DisplayThisLevel = true;
                                break;
                            }
                        }
                    }
                    if (!$DisplayThisLevel) {
                        continue;
                    }

                    $thisTitle = Get_Lang_Selection($FloorArr[$j]['NameChi'], $FloorArr[$j]['NameEng']);
                    $thisTitle = intranet_htmlspecialchars($thisTitle);


                    # retrieve all rooms (if no room and !$ShowNoRoomFloor, do not show the floor)
                    //$objFloor = new Floor();
                    $RoomArr = $objFloor->Get_All_Room("", $thisLocationLevelID);
                    $numOfRoom = count($RoomArr);
                    if ($numOfRoom == 0 && !$ShowNoRoomFloor) {
                        continue;
                    } else {
                        if ($CanSelectWholeFloor) {
                            $selectionArr[] = array($thisLocationLevelID, $Lang['General']['All'] . " " . $thisTitle);
                        } else {
                            $selectionArr[] = array("OPTGROUP", $thisTitle);
                        }
                        for ($k = 0; $k < sizeof($RoomArr); $k++) {
                            # skip this level for management group if no right
                            $DisplayThisLocation = true;
                            if ($GivenLocations != "" && is_array($GivenLocations)) {
                                $DisplayThisLocation = false;
                                # match
                                for ($m = 0; $m < sizeof($GivenLocations); $m++) {
                                    if ($GivenLocations[$m] == $RoomArr[$k]['LocationID']) {
                                        $DisplayThisLocation = true;
                                        break;
                                    }
                                }
                            }
                            if ($DisplayThisLocation) {
                                $roomTitle = Get_Lang_Selection($RoomArr[$k]['NameChi'], $RoomArr[$k]['NameEng']);
                                $roomTitle = ($CanSelectWholeFloor ? "&nbsp;&nbsp;&nbsp;" : "") . $roomTitle;
                                $selectionArr[] = array($RoomArr[$k]['LocationID'], $roomTitle);
                            }
                        }
                    }
                }


            }
            $onchange = '';
            if ($ParOnchange != "")
                $onchange = 'onchange="' . $ParOnchange . '"';

            $selectionTags = ' id="' . $ParID . '" name="' . $ParID . '" ' . $onchange;

            if ($multiple) $selectionTags .= ' multiple size=' . $multiple;

            $floorSelection = returnSelection($selectionArr, $SelectedLocationLevelID, $ParID, $selectionTags);

            return $floorSelection;
        }

        function Get_Building_Floor_Selection($SelectedLocationLevelID = '', $ParID = 'FloorSelected', $ParOnchange = '', $ParNoFirst = 0, $ShowNoRoomFloor = 0, $ParTitle = '')
        {
            global $intranet_session_language, $Lang;

            $objBuilding = new Building();
            $BuildingArr = $objBuilding->Get_All_Building(0, 1);
            $numOfBuilding = count($BuildingArr);

            $selectionArr = array();
            $firstTitle = ($ParTitle == '') ? "-- " . $Lang['SysMgr']['Location']['Select']['Floor'] . " --" : $ParTitle;
            $selectionArr[] = array("", $firstTitle);

            for ($i = 0; $i < $numOfBuilding; $i++) {
                $thisBuildingID = $BuildingArr[$i]['BuildingID'];
                $thisNameEng = $BuildingArr[$i]['NameEng'];
                $thisNameChi = $BuildingArr[$i]['NameChi'];
                $thisBuildingName = Get_Lang_Selection($thisNameChi, $thisNameEng);

                $FloorArr = $objBuilding->Get_All_Floor_Cached(0, $thisBuildingID);
                $numOfFloor = count($FloorArr);

                $selectionArr[] = array("OPTGROUP", $thisBuildingName);

                for ($j = 0; $j < $numOfFloor; $j++) {
                    $thisLocationLevelID = $FloorArr[$j]['LocationLevelID'];
                    $thisTitle = Get_Lang_Selection($FloorArr[$j]['NameChi'], $FloorArr[$j]['NameEng']);
                    $thisTitle = intranet_htmlspecialchars($thisTitle);

                    if (!$ShowNoRoomFloor && !$objBuilding->Check_Any_Room_By_Floor($thisLocationLevelID)) {
                        /*
                        $objFloor = new Floor($thisLocationLevelID);
                        $RoomArr = $objFloor->Get_All_Room();
                        $numOfRoom = count($RoomArr);
                        if ($numOfRoom == 0)
                        */
                        continue;
                    }
                    $selectionArr[] = array($thisLocationLevelID, $thisTitle);
                }


            }
            $onchange = '';
            if ($ParOnchange != "")
                $onchange = 'onchange="' . $ParOnchange . '"';

            $selectionTags = ' id="' . $ParID . '" name="' . $ParID . '" ' . $onchange;
            
            //debug_r($selectionArr);
            $floorSelection = returnSelection($selectionArr, $SelectedLocationLevelID, $ParID, $selectionTags);

            return $floorSelection;
        }

        function Get_Floor_Selection($BuildingID = '', $SelectedLocationLevelID = '', $ParID = 'FloorSelected', $ParOnchange = '', $ParNoFirst = 0, $ShowNoRoomFloor = 0, $ParTitle = '', $WithOthersOpt=false, $class='')
        {
            global $intranet_session_language, $Lang;

            if ($BuildingID != '' && $BuildingID != 'null') {
                $libBuilding = new Building($BuildingID);
                $allFloorInfoArr = $libBuilding->Get_All_Floor();
                $numOfFloor = count($allFloorInfoArr);
            } else {
                $numOfFloor = 0;
            }

            $selectionArr = array();
            for ($i = 0; $i < $numOfFloor; $i++) {
                $thisLocationLevelID = $allFloorInfoArr[$i]['LocationLevelID'];
                $thisTitle = ($intranet_session_language == 'en') ? $allFloorInfoArr[$i]['NameEng'] : $allFloorInfoArr[$i]['NameChi'];
                $thisTitle = intranet_htmlspecialchars($thisTitle);

                if (!$ShowNoRoomFloor) {
                    $objFloor = new Floor($thisLocationLevelID);
                    $RoomArr = $objFloor->Get_All_Room();
                    $numOfRoom = count($RoomArr);

                    if ($numOfRoom == 0)
                        continue;
                }

                $selectionArr[$thisLocationLevelID] = $thisTitle;
            }

            if ($WithOthersOpt)
                $selectionArr['-1'] = $Lang['SysMgr']['Location']['OthersLocation']."...";
                
            $onchange = '';
            if ($ParOnchange != "")
                $onchange = 'onchange="' . $ParOnchange . '"';

            $selectionTags = ' id="' . $ParID . '" name="' . $ParID . '" ' . $onchange;
            if ($class) {
                $selectionTags .= ' class="'.$class.'"';
            }
            
            $firstTitle = ($ParTitle == '') ? $Lang['SysMgr']['Location']['Select']['Floor'] : $ParTitle;
            $firstTitle = Get_Selection_First_Title($firstTitle);

            $floorSelection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedLocationLevelID, $all = 0, $ParNoFirst, $firstTitle);

            return $floorSelection;
        }

        function Get_Room_Selection($LocationLevelID = '', $SelectedLocationID = '', $ParID = 'RoomSelected', $ParOnchange = '', $ParNoFirst = 0, $ParTitle = '', $ParIsMultiple = '', $class='')
        {
            global $intranet_session_language, $Lang;

            if ($LocationLevelID != '' && $LocationLevelID != 'null') {
                $libFloor = new Floor($LocationLevelID);
                $allRoomInfoArr = $libFloor->Get_All_Room();
                $numOfRoom = count($allRoomInfoArr);
            } else {
                $numOfRoom = 0;
            }

            $selectionArr = array();
            for ($i = 0; $i < $numOfRoom; $i++) {
                $thisLocationID = $allRoomInfoArr[$i]['LocationID'];
                $thisTitle = ($intranet_session_language == 'en') ? $allRoomInfoArr[$i]['NameEng'] : $allRoomInfoArr[$i]['NameChi'];
                $thisTitle = intranet_htmlspecialchars($thisTitle);

                $selectionArr[$thisLocationID] = $thisTitle;
            }

            $onchange = '';
            if ($ParOnchange != "")
                $onchange = 'onchange="' . $ParOnchange . '"';

            $multiple = '';
            if ($ParIsMultiple)
                $multiple = ' multiple="true" size="10"';

            $selectionTags = ' id="' . $ParID . '" name="' . $ParID . '" ' . $onchange . ' ' . $multiple;
            if ($class) {
                $selectionTags .= ' class="'.$class.'"';
            }
            
            $firstTitle = ($ParTitle == '') ? $Lang['SysMgr']['Location']['Select']['Room'] : $ParTitle;
            $firstTitle = Get_Selection_First_Title($firstTitle);

            $roomSelection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedLocationID, $all = 0, $ParNoFirst, $firstTitle);

            return $roomSelection;
        }

        function Get_Room_Selection_With_Option_Group($ID_Name, $Selected = '', $OnChange = '', $ID = '', $isMultiple = false, $noFirst = 0)
        {
            global $Lang, $intranet_session_language;

            if ($OnChange != '')
                $onchange = 'onchange="' . $OnChange . '"';

            $spacer = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

            $Name = $ID_Name;
            if ($ID == '') {
                $ID = $ID_Name;
            }

            if ($isMultiple) {
                $multiple = 'multiple size="10" ';
            }

            $select = '';
            $select .= '<select name="' . $Name . '" id="' . $ID . '" class="formtextbox" ' . $onchange . ' ' . $multiple . '>';
            if ($noFirst) {
                // do nth
            } else {
                $select .= '<option value="">' . Get_Selection_First_Title($Lang['SysMgr']['Location']['All']['Room']) . '</option>';
            }

            $libBuilding = new Building();
            $BuildingArr = $libBuilding->Get_All_Building();
            $numOfBuilding = count($BuildingArr);

            for ($i = 0; $i < $numOfBuilding; $i++) {
                $thisBuildingID = $BuildingArr[$i]['BuildingID'];
                $objBuilding = new Building($thisBuildingID);
                $builidingName = intranet_htmlspecialchars($objBuilding->Get_Name($intranet_session_language));

                $select .= '<optgroup label="' . $builidingName . '">';

                $FloorArr = $objBuilding->Get_All_Floor();
                $numOfFloor = count($FloorArr);

                for ($j = 0; $j < $numOfFloor; $j++) {
                    $thisLocationLevelID = $FloorArr[$j]['LocationLevelID'];
                    $objFloor = new Floor($thisLocationLevelID);
                    $floorName = intranet_htmlspecialchars($objFloor->Get_Name($intranet_session_language));

                    $select .= '<optgroup label="' . $spacer . $floorName . '">';

                    $RoomArr = $objFloor->Get_All_Room();
                    $numOfRoom = count($RoomArr);

                    for ($k = 0; $k < $numOfRoom; $k++) {
                        $thisLocationID = $RoomArr[$k]['LocationID'];
                        $objRoom = new Room($thisLocationID);
                        $roomName = intranet_htmlspecialchars($objRoom->Get_Name($intranet_session_language));

                        $select .= '<option value="' . $thisLocationID . '">' . $roomName . '</option>';
                    }

                    $select .= '</optgroup>';
                }

                $select .= '</optgroup>';
            }

            $select .= '</select>';

            return $select;
        }

        function Get_Export_Content($Params)
        {
            global $PATH_WRT_ROOT, $Lang, $sys_custom;
            include_once($PATH_WRT_ROOT . "includes/libexporttext.php");
            $lexport = new libexporttext();

            $Rows = array(); // data rows
            $Details = array(); // one data row
            $HeaderColumn = array();// header columns

            $BuildingID = $Params['BuildingID'];

            $buildingObj = new Building($BuildingID);
            $floorInfoArr = $buildingObj->Get_All_Floor();
            $numOfFloor = count($floorInfoArr);
            $buildingNameChi = $buildingObj->NameChi;
            $buildingNameEng = $buildingObj->NameEng;
            $buildingNameDisplay = Get_Lang_Selection($buildingObj->NameChi, $buildingObj->NameEng);

            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorCode']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['Barcode']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorNameEn']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorNameChi']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCode']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomBarcode']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomNameEn']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomNameChi']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomDescription']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCapacity']['EN'];
            if ($sys_custom['project']['HKPF']) {
                $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCategory']['EN'];
            }
            $exportColumn[0] = $HeaderColumn;

            $HeaderColumn = array();// header columns
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorCode']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['Barcode']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorNameEn']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorNameChi']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCode']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomBarcode']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomNameEn']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomNameChi']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomDescription']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCapacity']['B5'];
            if ($sys_custom['project']['HKPF']) {
                $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCategory']['B5'];
            }
            $exportColumn[1] = $HeaderColumn;
// 			debug_pr($numOfFloor);
            if ($numOfFloor == 0) {
                $Details[] = $Lang['General']['NoRecordAtThisMoment'];
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                if ($sys_custom['project']['HKPF']) {
                    $Details[] = " ";
                }
                $Rows[] = $Details;
            } else {
                for ($i = 0; $i < $numOfFloor; $i++) {
                    $thisLocationLevelID = $floorInfoArr[$i]['LocationLevelID'];
                    $thisFloorCode = $floorInfoArr[$i]['Code'];
                    $thisFloorBarcode = $floorInfoArr[$i]['Barcode'];
                    $thisFloorNameChi = $floorInfoArr[$i]['NameChi'];
                    $thisFloorNameEng = $floorInfoArr[$i]['NameEng'];
// 					$thisFloorNameDisplay = Get_Lang_Selection($thisFloorNameChi,$thisFloorNameEng);

                    $floorObj = new Floor($thisLocationLevelID);
                    $roomInfoArr = $floorObj->Get_All_Room();
                    $numOfRoom = count($roomInfoArr);
                    if ($numOfRoom > 0) {
                        for ($j = 0; $j < $numOfRoom; $j++) {
                            $roomCode = $roomInfoArr[$j]['Code'];
                            $roomBarcode = $roomInfoArr[$j]['Barcode'];
                            $roomNameChi = $roomInfoArr[$j]['NameChi'];
                            $roomNameEng = $roomInfoArr[$j]['NameEng'];
// 							$roomNameDisplay = Get_Lang_Selection($roomNameChi,$roomNameEng);
                            $roomDescription = $roomInfoArr[$j]['Description'];
                            $roomCapacity = $roomInfoArr[$j]['Capacity'];

                            unset($Details);
                            $Details[] = $thisFloorCode;
                            $Details[] = $thisFloorBarcode;
                            $Details[] = $thisFloorNameEng;
                            $Details[] = $thisFloorNameChi;
                            $Details[] = $roomCode;
                            $Details[] = $roomBarcode;
                            $Details[] = $roomNameEng;
                            $Details[] = $roomNameChi;
                            $Details[] = $roomDescription;
                            $Details[] = $roomCapacity;

                            if ($sys_custom['project']['HKPF']) {
                                $locationID = $roomInfoArr[$j]['LocationID'];
                                $categoryAry = $floorObj->getEnrolLocationCategory($locationID);
                                if (count($categoryAry)) {
                                    $categoryNameAry = BuildMultiKeyAssoc($categoryAry, array('CategoryID'), array('CategoryName'), 1);
                                    $Details[] = implode("##", $categoryNameAry);
                                } else {
                                    $Details[] = " ";
                                }
                            }

                            $Rows[] = $Details;
                        }
                    } else {
                        unset($Details);
                        $Details[] = $thisFloorCode;
                        $Details[] = $thisFloorBarcode;
                        $Details[] = $thisFloorNameEng;
                        $Details[] = $thisFloorNameChi;
                        $Details[] = " ";
                        $Details[] = " ";
                        $Details[] = " ";
                        $Details[] = " ";
                        $Details[] = " ";
                        $Details[] = " ";
                        if ($sys_custom['project']['HKPF']) {
                            $Details[] = " ";
                        }
                        $Rows[] = $Details;
                    }
                }
            }
// 			debug_pr($Rows);
// 			$exportContent = $lexport->GET_EXPORT_TXT($Rows, $HeaderColumn);

            $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");

            $lexport->EXPORT_FILE($buildingNameDisplay . '.csv', $exportContent);
            return;
        }

        //Print
        function Get_Print_Content($Params)
        {
            global $PATH_WRT_ROOT, $Lang, $LAYOUT_SKIN, $sys_custom;
            include_once($PATH_WRT_ROOT . "includes/libexporttext.php");
            include_once($PATH_WRT_ROOT . "templates/" . $LAYOUT_SKIN . "/layout/print_header.php");
            include_once($PATH_WRT_ROOT . "includes/libinterface.php");

            $lexport = new libexporttext();
            $linterface = new interface_html();

            $Rows = array(); // data rows
            $Details = array(); // one data row
            $HeaderColumn = array();// header columns

            $BuildingID = $Params['BuildingID'];

            $buildingObj = new Building($BuildingID);
            $floorInfoArr = $buildingObj->Get_All_Floor();
            $numOfFloor = count($floorInfoArr);
            $buildingNameChi = $buildingObj->NameChi;
            $buildingNameEng = $buildingObj->NameEng;
            $buildingNameDisplay = Get_Lang_Selection($buildingObj->NameChi, $buildingObj->NameEng);

            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorCode']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['Barcode']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorNameEn']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorNameChi']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCode']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomBarcode']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomNameEn']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomNameChi']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomDescription']['EN'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCapacity']['EN'];
            if ($sys_custom['project']['HKPF']) {
                $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCategory']['EN'];
            }
            $exportColumn[0] = $HeaderColumn;

            $HeaderColumn = array();// header columns
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorCode']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['Barcode']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorNameEn']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['FloorNameChi']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCode']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomBarcode']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomNameEn']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomNameChi']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomDescription']['B5'];
            $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCapacity']['B5'];
            if ($sys_custom['project']['HKPF']) {
                $HeaderColumn[] = $Lang['eInventory']['ExportFieldTitle']['RoomCategory']['B5'];
            }
            $exportColumn[1] = $HeaderColumn;
            // 			debug_pr($numOfFloor);
            if ($numOfFloor == 0) {
                $Details[] = $Lang['General']['NoRecordAtThisMoment'];
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                $Details[] = " ";
                if ($sys_custom['project']['HKPF']) {
                    $Details[] = " ";
                }
                $Rows[] = $Details;
            } else {
                for ($i = 0; $i < $numOfFloor; $i++) {
                    $thisLocationLevelID = $floorInfoArr[$i]['LocationLevelID'];
                    $thisFloorCode = $floorInfoArr[$i]['Code'];
                    $thisFloorBarcode = $floorInfoArr[$i]['Barcode'];
                    $thisFloorNameChi = $floorInfoArr[$i]['NameChi'];
                    $thisFloorNameEng = $floorInfoArr[$i]['NameEng'];
                    // 					$thisFloorNameDisplay = Get_Lang_Selection($thisFloorNameChi,$thisFloorNameEng);

                    $floorObj = new Floor($thisLocationLevelID);
                    $roomInfoArr = $floorObj->Get_All_Room();
                    $numOfRoom = count($roomInfoArr);
                    if ($numOfRoom > 0) {
                        for ($j = 0; $j < $numOfRoom; $j++) {
                            $roomCode = $roomInfoArr[$j]['Code'];
                            $roomBarcode = $roomInfoArr[$j]['Barcode'];
                            $roomNameChi = $roomInfoArr[$j]['NameChi'];
                            $roomNameEng = $roomInfoArr[$j]['NameEng'];
                            // 							$roomNameDisplay = Get_Lang_Selection($roomNameChi,$roomNameEng);
                            $roomDescription = $roomInfoArr[$j]['Description'];
                            $roomCapacity = $roomInfoArr[$j]['Capacity'];

                            unset($Details);
                            $Details[] = $thisFloorCode;
                            $Details[] = $thisFloorBarcode;
                            $Details[] = $thisFloorNameEng;
                            $Details[] = $thisFloorNameChi;
                            $Details[] = $roomCode;
                            $Details[] = $roomBarcode;
                            $Details[] = $roomNameEng;
                            $Details[] = $roomNameChi;
                            $Details[] = $roomDescription;
                            $Details[] = $roomCapacity;
                            if ($sys_custom['project']['HKPF']) {
                                $locationID = $roomInfoArr[$j]['LocationID'];
                                $categoryAry = $floorObj->getEnrolLocationCategory($locationID);
                                if (count($categoryAry)) {
                                    $categoryNameAry = BuildMultiKeyAssoc($categoryAry, array('CategoryID'), array('CategoryName'), 1);
                                    $Details[] = implode("##", $categoryNameAry);
                                } else {
                                    $Details[] = " ";
                                }
                            }
                            $Rows[] = $Details;
                        }
                    } else {
                        unset($Details);
                        $Details[] = $thisFloorCode;
                        $Details[] = $thisFloorBarcode;
                        $Details[] = $thisFloorNameEng;
                        $Details[] = $thisFloorNameChi;
                        $Details[] = " ";
                        $Details[] = " ";
                        $Details[] = " ";
                        $Details[] = " ";
                        $Details[] = " ";
                        $Details[] = " ";
                        if ($sys_custom['project']['HKPF']) {
                            $Details[] = " ";
                        }
                        $Rows[] = $Details;
                    }
                }
            }
//             debug_pr($Rows);

            $countRow = sizeof($Rows);
//             debug_pr($countRow);
//             debug_pr($countRow);
//             debug_pr($exportColumn);

//             debug_pr($buildingNameChi);
//             debug_pr($buildingNameEng);
//             debug_pr($buildingNameDisplay);
            $display = "";
            $nrColumn = $sys_custom['project']['HKPF'] ? 11 : 10;
            $display .= "<table class='common_table_list_v30 view_table_list_v30'>";
            if ($buildingNameDisplay == $buildingNameEng) {
                $display .= "<tr>";
                for ($i = 0; $i < $nrColumn; $i++) {
                    $display .= "<th width='10%'>" . $exportColumn[0][$i] . "</th>";
                }
                $display .= "</tr>";
            } else {
                $display .= "<tr>";
                for ($i = 0; $i < $nrColumn; $i++) {
                    $display .= "<th width='10%'>" . $exportColumn[1][$i] . "</th>";
                }
                $display .= "</tr>";
            }
            if ($buildingNameDisplay == $buildingNameEng) {
                $display .= "<tr>";
                for ($j = 0; $j < $countRow; $j++) {
                    for ($i = 0; $i < $nrColumn; $i++) {
                        $display .= "<td width='10%'>" . $Rows[$j][$i] . "</td>";
                    }
                    $display .= "</tr>";
                }
            } else {
                for ($j = 0; $j < $countRow; $j++) {
                    $display .= "<tr>";
                    for ($i = 0; $i < $nrColumn; $i++) {
                        $display .= "<td width='10%'>" . $Rows[$j][$i] . "</td>";
                    }
                    $display .= "</tr>";
                }
            }

            // loop the sql result array
//             for ($i = 0; $i < $countRow; $i++){
//                 $display .= "<tr>";
//                 $display .= "<td width='40%'>" . $result[$i][TeacherName] . "</td>";
//                 $display .= "<td width='30%'>" . $result[$i][SubjectName] . "</td>";
//                 $copyYearName = str_replace(",", ", ", $result[$i][YearName]);
//                 $display .= "<td width='30%'>" . $copyYearName . "</td>";
//                 $display .= "</tr>";
//             }
            $display .= "</table>";

            $displayButton = "";
            $displayButton .= '<table width="100%" align="center" class="print_hide" border="0">
                                    <tr>
                                    <td align="right">' . $linterface->GET_SMALL_BTN("Print", "button", "javascript:window.print();", "submit2") . '</td>
                                    </tr>
                                </table>';
            echo $displayButton;
//             Display result
            echo $display;

// 		    return;
        }


        function getEnrolCategorySelection($locationID)
        {
            global $Lang;
            include_once("libinterface.php");
            $libinterface = new interface_html();

            if ($locationID) {
                $selectedCategoryIDAry = $this->getEnrolLocationCategory($locationID);
            } else {
                $selectedCategoryIDAry = array();
            }
            $selectedCategoryIDAssoc = array();

            $availableCategoryAry = $this->getEnrolCategoryList();

            $x = '<table class="no_bottom_border">' . "\n";
            $x .= '<tr>' . "\n";
            $x .= '<td>' . "\n";
            $x .= '<select name=CategoryID[] id=CategoryID style="min-width:200px; height:160px;" multiple>' . "\n";

            foreach ((array)$selectedCategoryIDAry as $category) {
                $categoryID = $category['CategoryID'];
                $categoryName = $category['CategoryName'];
                $selectedCategoryIDAssoc[$categoryID] = $categoryID;
                $x .= '<option value="' . $categoryID . '">' . intranet_htmlspecialchars($categoryName) . '</option>' . "\n";
            }

            $x .= '</select>' . "\n";
            $x .= '</td>' . "\n";

            $x .= '<td align=center>' . "\n";
            $x .= $libinterface->GET_BTN("<< " . $Lang['Btn']['Add'], "submit", "checkOptionTransfer(this.form.elements['AvailableCategoryID[]'],this.form.elements['CategoryID[]']);return false;", "submit11") . "<br /><br />";
            $x .= $libinterface->GET_BTN($Lang['Btn']['Delete'] . " >>", "submit", "checkOptionTransfer(this.form.elements['CategoryID[]'],this.form.elements['AvailableCategoryID[]']);return false;", "submit12");
            $x .= '</td>' . "\n";

            $x .= '<td>' . "\n";
            $x .= '<select name=AvailableCategoryID[] id=AvailableCategoryID style="min-width:200px; height:160px;" multiple>' . "\n";

            foreach ((array)$availableCategoryAry as $category) {
                $categoryID = $category['CategoryID'];
                $categoryName = $category['CategoryName'];
                if (empty($selectedCategoryIDAssoc[$categoryID])) {      // don't show selected record here
                    $x .= '<option value="' . $categoryID . '">' . intranet_htmlspecialchars($categoryName) . '</option>' . "\n";
                }
            }

            $x .= '</select>' . "\n";
            $x .= '</td>' . "\n";

            $x .= '</tr>' . "\n";
            $x .= '</table>';

            return $x;

        }

        # with selectpicker class, used with bootstrap style
        function getBuildingFloorRoomSelectionWithClass($SelectedLocationLevelID = '', $ParID = 'RoomSelected', $ParOnchange = '', $ParNoFirst = 0, $ShowNoRoomFloor = 0, $ParTitle = '', $GivenBuildings = "", $GivenLocationLevel = "", $GivenLocations = "", $CanSelectWholeFloor = 0, $multiple = 0, $class="selectpicker")
        {
            global $intranet_session_language, $Lang;

            $objBuilding = new Building();
            $BuildingArr = $objBuilding->Get_All_Building(0, 1);
            $numOfBuilding = count($BuildingArr);

            $selectionArr = array();

            if (!$ParNoFirst) {
                $firstTitle = ($ParTitle == '') ? "-- " . $Lang['SysMgr']['Location']['Select']['Floor'] . " --" : $ParTitle;
                $selectionArr[] = array("", $firstTitle);
            }

            $objFloor = new Floor();

            for ($i = 0; $i < $numOfBuilding; $i++) {
                $thisBuildingID = $BuildingArr[$i]['BuildingID'];

                # skip this building for management group if no right
                $DisplayThisBuilding = true;
                if ($GivenBuildings != "" && is_array($GivenBuildings)) {
                    $DisplayThisBuilding = false;
                    # match
                    for ($j = 0; $j < sizeof($GivenBuildings); $j++) {
                        if ($GivenBuildings[$j] == $thisBuildingID) {
                            $DisplayThisBuilding = true;
                            break;
                        }
                    }
                }
                if (!$DisplayThisBuilding) {
                    continue;
                }

                $thisNameEng = $BuildingArr[$i]['NameEng'];
                $thisNameChi = $BuildingArr[$i]['NameChi'];
                $thisBuildingName = Get_Lang_Selection($thisNameChi, $thisNameEng);

                $FloorArr = $objBuilding->Get_All_Floor_Cached(0, $thisBuildingID);
                $numOfFloor = count($FloorArr);

                if (!$ParNoFirst) {
                    $selectionArr[] = array("OPTGROUP", "=======================");
                }

                $selectionArr[] = array("disabled", "[ " . $thisBuildingName . " ]");


                for ($j = 0; $j < $numOfFloor; $j++) {
                    $thisLocationLevelID = $FloorArr[$j]['LocationLevelID'];

                    # skip this level for management group if no right
                    $DisplayThisLevel = true;
                    if ($GivenLocationLevel != "" && is_array($GivenLocationLevel)) {
                        $DisplayThisLevel = false;
                        # match
                        for ($k = 0; $k < sizeof($GivenLocationLevel); $k++) {
                            if ($GivenLocationLevel[$k] == $thisLocationLevelID) {
                                $DisplayThisLevel = true;
                                break;
                            }
                        }
                    }
                    if (!$DisplayThisLevel) {
                        continue;
                    }

                    $thisTitle = Get_Lang_Selection($FloorArr[$j]['NameChi'], $FloorArr[$j]['NameEng']);
                    $thisTitle = intranet_htmlspecialchars($thisTitle);


                    # retrieve all rooms (if no room and !$ShowNoRoomFloor, do not show the floor)
                    //$objFloor = new Floor();
                    $RoomArr = $objFloor->Get_All_Room("", $thisLocationLevelID);
                    $numOfRoom = count($RoomArr);
                    if ($numOfRoom == 0 && !$ShowNoRoomFloor) {
                        continue;
                    } else {
                        if ($CanSelectWholeFloor) {
                            $selectionArr[] = array($thisLocationLevelID, $Lang['General']['All'] . " " . $thisTitle);
                        } else {
                            $selectionArr[] = array("OPTGROUP", $thisTitle);
                        }
                        for ($k = 0; $k < sizeof($RoomArr); $k++) {
                            # skip this level for management group if no right
                            $DisplayThisLocation = true;
                            if ($GivenLocations != "" && is_array($GivenLocations)) {
                                $DisplayThisLocation = false;
                                # match
                                for ($m = 0; $m < sizeof($GivenLocations); $m++) {
                                    if ($GivenLocations[$m] == $RoomArr[$k]['LocationID']) {
                                        $DisplayThisLocation = true;
                                        break;
                                    }
                                }
                            }
                            if ($DisplayThisLocation) {
                                $roomTitle = Get_Lang_Selection($RoomArr[$k]['NameChi'], $RoomArr[$k]['NameEng']);
                                $roomTitle = ($CanSelectWholeFloor ? "&nbsp;&nbsp;&nbsp;" : "") . $roomTitle;
                                $selectionArr[] = array($RoomArr[$k]['LocationID'], $roomTitle);
                            }
                        }
                    }
                }


            }
            $onchange = '';
            if ($ParOnchange != "")
                $onchange = 'onchange="' . $ParOnchange . '"';

            $selectionTags = ' id="' . $ParID . '" name="' . $ParID . '" class="'.$class.'" ' . $onchange;

            if ($multiple) $selectionTags .= ' multiple size=' . $multiple;

            $floorSelection = $this->getSelection($selectionArr, $SelectedLocationLevelID, $ParID, $selectionTags);

            return $floorSelection;
        }


        function getSelection($arr2D, $curSelected, $objName, $objProperty)
        {
            $rx = "<select name=\"$objName\" $objProperty>\n";

            $selectedAlready = false;
            for ($i = 0; $i < sizeof($arr2D); $i++) {
                list($value, $name) = $arr2D[$i];
                if ($value == "OPTGROUP" || ($value == null && !($value == 0) && !($value == ""))) {
                    if ($isOptionGroupBefore) {
                        $rx .= "</optgroup>\n";
                    }

                    $rx .= "<optgroup label=\"" . $name . "\">\n";
                    $isOptionGroupBefore = true;
                } else if ($value == "disabled") {
                    if ($isOptionGroupBefore) {
                        $rx .= "</optgroup>\n";
                    }
                    $rx .= "<option disabled style=\"padding-left: 0px\">$name</option>\n";
                    $isOptionGroupBefore = false;
                } else {
                    $optionSelect = "";
                    if ($curSelected == $value && !$selectedAlready) {
                        $optionSelect = "selected='selected'";
                        $selectedAlready = true;
                    }

                    $rx .= "<option value=\"$value\" $optionSelect>$name</option>\n";
                }
            }
            if ($isOptionGroupBefore) {
                $rx .= "</optgroup>\n";
            }
            $rx .= "</select>\n";

            return $rx;
        }

        # with selectpicker class, used with bootstrap style
        # with group access right control
        function getBuildingFloorSelectionWithClass($SelectedLocationLevelID = '', $ParID = 'FloorSelected', $ParOnchange = '', $ParNoFirst = 0, $ShowNoRoomFloor = 0, $ParTitle = '', $GivenBuildings = "", $GivenLocationLevel = "", $class="selectpicker")
        {
            global $intranet_session_language, $Lang, $junior_mck;
            
            $objBuilding = new Building();
            $BuildingArr = $objBuilding->Get_All_Building(0, 1);
            $numOfBuilding = count($BuildingArr);

            $selectionArr = array();
            
            if (!$ParNoFirst) {
                $firstTitle = ($ParTitle == '') ? "-- " . $Lang['SysMgr']['Location']['Select']['Floor'] . " --" : $ParTitle;
                $selectionArr[] = array("", $firstTitle);
            }
            
            for ($i = 0; $i < $numOfBuilding; $i++) {
                $thisBuildingID = $BuildingArr[$i]['BuildingID'];
                
                # skip this building for management group if no right
                $DisplayThisBuilding = true;
                if ($GivenBuildings != "" && is_array($GivenBuildings)) {
                    $DisplayThisBuilding = false;
                    # match
                    for ($j = 0; $j < sizeof($GivenBuildings); $j++) {
                        if ($GivenBuildings[$j] == $thisBuildingID) {
                            $DisplayThisBuilding = true;
                            break;
                        }
                    }
                }
                if (!$DisplayThisBuilding) {
                    continue;
                }
                
                $thisNameEng = $BuildingArr[$i]['NameEng'];
                $thisNameChi = $BuildingArr[$i]['NameChi'];
                $thisBuildingName = Get_Lang_Selection($thisNameChi, $thisNameEng);
                
                $FloorArr = $objBuilding->Get_All_Floor_Cached(0, $thisBuildingID);
                $numOfFloor = count($FloorArr);
                
                if (!$junior_mck) {
                    $selectionArr[] = array("OPTGROUP", $thisBuildingName);
                }
                
                for ($j = 0; $j < $numOfFloor; $j++) {
                    $thisLocationLevelID = $FloorArr[$j]['LocationLevelID'];
                    
                    # skip this level for management group if no right
                    $DisplayThisLevel = true;
                    if ($GivenLocationLevel != "" && is_array($GivenLocationLevel)) {
                        $DisplayThisLevel = false;
                        # match
                        for ($k = 0; $k < sizeof($GivenLocationLevel); $k++) {
                            if ($GivenLocationLevel[$k] == $thisLocationLevelID) {
                                $DisplayThisLevel = true;
                                break;
                            }
                        }
                    }
                    if (!$DisplayThisLevel) {
                        continue;
                    }
                    
                    $thisTitle = Get_Lang_Selection($FloorArr[$j]['NameChi'], $FloorArr[$j]['NameEng']);
                    $thisTitle = intranet_htmlspecialchars($thisTitle);
                    
                    if (!$ShowNoRoomFloor && !$objBuilding->Check_Any_Room_By_Floor($thisLocationLevelID)) {
                         continue;
                    }
                    $selectionArr[] = array($thisLocationLevelID, $thisTitle);
                }
                
            }
            $onchange = '';
            if ($ParOnchange != "") {
                $onchange = 'onchange="' . $ParOnchange . '"';
            }
            
            $selectionTags = ' id="' . $ParID . '" name="' . $ParID . '" class="'.$class.'" ' . $onchange;
            
            //debug_r($selectionArr);
            $floorSelection = $this->getSelection($selectionArr, $SelectedLocationLevelID, $ParID, $selectionTags);
            
            return $floorSelection;
        }
        
        # with selectpicker class, used with bootstrap style
        # with group access right control
        function getRoomSelectionWithClass($LocationLevelID = '', $SelectedLocationID = '', $ParID = 'RoomSelected', $ParOnchange = '', $ParNoFirst = 0, $ParTitle = '', $ParIsMultiple = '', $GivenLocations = "", $class="selectpicker")
        {
            global $intranet_session_language, $Lang;
            
            if ($LocationLevelID != '' && $LocationLevelID != 'null') {
                $libFloor = new Floor($LocationLevelID);
                $allRoomInfoArr = $libFloor->Get_All_Room();
                $numOfRoom = count($allRoomInfoArr);
            } else {
                $numOfRoom = 0;
            }
            
            $selectionArr = array();
            
            if (!$ParNoFirst) {
                $firstTitle = ($ParTitle == '') ? "-- " . $Lang['SysMgr']['Location']['Select']['Room'] . " --" : $ParTitle;
                $selectionArr[] = array("", $firstTitle);
            }
            
            for ($i = 0; $i < $numOfRoom; $i++) {
                $thisLocationID = $allRoomInfoArr[$i]['LocationID'];
                # skip this level for management group if no right
                $DisplayThisLocation = true;
                if ($GivenLocations != "" && is_array($GivenLocations)) {
                    $DisplayThisLocation = false;
                    # match
                    for ($m = 0; $m < sizeof($GivenLocations); $m++) {
                        if ($GivenLocations[$m] == $thisLocationID) {
                            $DisplayThisLocation = true;
                            break;
                        }
                    }
                }
                if ($DisplayThisLocation) {
                    $roomTitle = Get_Lang_Selection($allRoomInfoArr[$i]['NameChi'], $allRoomInfoArr[$i]['NameEng']);
                    $selectionArr[] = array($thisLocationID, intranet_htmlspecialchars($roomTitle));
                }
                
            }
            
            $onchange = '';
            if ($ParOnchange != "") {
                $onchange = 'onchange="' . $ParOnchange . '"';
            }
            
            $multiple = '';
            if ($ParIsMultiple) {
                $multiple = ' multiple="true" size="10"';
            }
            
            $selectionTags = ' id="' . $ParID . '" name="' . $ParID . '" class="'.$class.'" ' . $onchange . ' ' . $multiple;
            
            //debug_r($selectionArr);
            $roomSelection = $this->getSelection($selectionArr, $SelectedLocationID, $ParID, $selectionTags);
            
            return $roomSelection;
        }
        
    }   // end class

} // End of directives
?>