<?php
// editing by
/********************
 * Date : 2017-05-31 Frankie
 ********************/

if (defined("LIBEFORM_DEFINED")) {
	class FormInfo extends FormCommon {
		####################################################################################################
		function __construct($ModuleTitle = 'eForm') {
			parent::__construct($ModuleTitle);
		}
		####################################################################################################
		## Get Table Grid Information
		## - Fields array for libdbtable
		## - SQL for libdbtable
		####################################################################################################
		function getTableGridInfo($args = array()) {
			if (count($args) > 0) extract($args);
			
			$strSQL = "SELECT FormID, TemplateID, FolderID, FormTitle, StartDate, DueDate, AllowDraft, FormStatus, DateModified as LastModified  
				FROM " . $this->formInfoTable . " WHERE " . $this->isDeletedCondition() . " AND ModuleTitle='" . $this->ModuleTitle . "'
			";
			if (isset($keyword) && !empty($keyword)) {
				$strSQL .= " AND (FormTitle LIKE '%" . $keyword . "%' OR StartDate  LIKE '%" . $keyword . "%' OR DueDate LIKE '%" . $keyword . "%')";
			}
			
			if (isset($folderID) && !empty($folderID) && $folderID > 0) {
			    $strSQL .= " AND (FolderID='" . $folderID. "')";
			} else {
			    $strSQL .= " AND (FolderID='0')";
			}
			
			$infoArr = array(
						"fields" => array("StartDate", "DueDate", "FormTitle", "ViewForm", "SubmittedTotal", "LastModified"),
						"order2"=> ", StartDate Desc, DueDate Desc, FormTitle ASC",
						"sql" => $this->sqlOpt($strSQL)
					);
			return $infoArr;
		}
		
		####################################################################################################
		function getTotalTargetUser($formIDs = array()) {
			$formTargetTotal = array();
			if (count($formIDs) > 0) {
				$strSQL = "SELECT FormID, COUNT(UserID) AS Total FROM " . $this->formInfoTargetUserTable . " WHERE FormID IN (" . implode(", ", $formIDs) . ") GROUP BY FormID";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					$formTargetTotal = BuildMultiKeyAssoc($result, "FormID");
				}
			}
			return $formTargetTotal;
		}
		
		####################################################################################################
		function getTotalSubmitted($formIDs = array(), $withProcessing = false) {
			$formSubmittedTotal = array();
			if (count($formIDs) > 0) {
				$strSQL = "SELECT fgt.FormID, COUNT(fgt.UserID) AS Total
										FROM " . $this->formGridTable . " AS fgt
										JOIN " . $this->formInfoTargetUserTable . " AS fiftut ON (fgt.FormID=fiftut.FormID AND fgt.UserID=fiftut.UserID)
										WHERE fgt.FormID IN (" . implode(", ", $formIDs) . ") ";
				if ($withProcessing) {
					$strSQL .= " AND GridStatus NOT IN ('" . $this->GridStatusArr["READY"] . "')";
				} else {
					$strSQL .= " AND GridStatus IN ('" . $this->GridStatusArr["COMPLETED"] . "')";
				}
				$strSQL .= " GROUP BY fgt.FormID";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					$formSubmittedTotal = BuildMultiKeyAssoc($result, "FormID");
				}
			}
			return $formSubmittedTotal;
		}
		
		####################################################################################################
		## Get Table Grid Data with limit condition
		## - run with SQL
		####################################################################################################
		function getTableGridData($args = array()) {
			global $Lang, $indexVar, $LAYOUT_SKIN;
			if (count($args) > 0) extract($args);
			
			$dataArr = array();
			if (isset($sql) && !empty($sql)) {
				$result = $this->returnResultSet($sql);
				if (count($result) > 0) {
					/*********************************************************/
					$formArr = BuildMultiKeyAssoc($result, "FormID");
					if (count($formArr) > 0) {
						$formIDs = array_keys($formArr);
					}
					if (count($formIDs) > 0) {
						$formTargetTotal = $this->getTotalTargetUser($formIDs);
						$formSubmittedTotal = $this->getTotalSubmitted($formIDs);
					}
					/*********************************************************/
					$kk = 0;
					foreach ($formArr as $formID => $vv) {
						if (isset($girdInfo["fields"]) && count($girdInfo["fields"]) > 0) {
							foreach ($girdInfo["fields"] as $fieldIndx => $fieldLabel) {
								$thisIsUsed = false;
								$submitted = 0;
								if (isset($formSubmittedTotal[$formID]["Total"])) {
									if ($formSubmittedTotal[$formID]["Total"] > 0) {
										$thisIsUsed = true;
										$submitted = $formSubmittedTotal[$formID]["Total"];
									}
								}
								switch ($fieldLabel) {
									case "ViewForm":
										$dataArr[$kk][$fieldIndx] = " <a href=\"?task=management/form_management/edit&folder=" . $vv["FolderID"] . "&fid=" . $vv["FormID"] . "&ctk=" . $this->getToken($vv["FormID"]) . "\"><img src=\"/images/".$LAYOUT_SKIN."/icon_edit_b.gif\" style='max-width:20px;' alt='" . $Lang["eForm"]["img_alt_edit"] . "' align=\"absmiddle\" border=0></a>";
										$dataArr[$kk][$fieldIndx] .= " <a href=\"?task=management/form_management/edit&folder=" . $vv["FolderID"] . "&cid=" . $vv["FormID"] . "&ctk=" . $this->getToken($vv["FormID"] . '-copy') . "\"><img src=\"/images/".$LAYOUT_SKIN."/icon_copy_b.gif\" style='max-width:20px;' alt='" . $Lang["eForm"]["img_alt_copy"] . "' align=\"absmiddle\" border=0></a>";
										break;
									case "SubmittedTotal":
										$total = 0;
										if (isset($formTargetTotal[$formID]["Total"])) {
											if ($formTargetTotal[$formID]["Total"] > 0) {
												$total = $formTargetTotal[$formID]["Total"];
											}
										}
										$dataArr[$kk][$fieldIndx] = "<a href=\"?task=management/form_management/recordlist&folder=" . $vv["FolderID"] . "&fid=" . $vv["FormID"] . "&ctk=" . $this->getToken($vv["FormID"]) . "\"><img src='/images/homework/en/hw/btn_handin_list.gif' align='absmiddle' > " . $submitted . " / " . $total . "</a>";
										break;
									case "FormTitle":
										if (isset($vv[$fieldLabel])) {
											$dataArr[$kk][$fieldIndx] = " <a class='preview' rel='" . $vv["FormID"] . "' href=\"?task=management/form_management/preview&fid=" . $vv["FormID"] . "&folder=" . $vv["FolderID"] . "&ctk=" . $this->getToken($vv["FormID"]) . "\"><img src=\"/images/".$LAYOUT_SKIN."/icon_view.gif\" height=\"20\" width=\"20\" align=\"absmiddle\" border=0> " . $vv[$fieldLabel] . "</a>";
											// $dataArr[$kk][$fieldIndx] = " <a href='#' class='thickboxBTN' rel=\"?task=management/form_management/preview&fid=" . $vv["FormID"] . "&ctk=" . $this->getToken($vv["FormID"]) . "\" title=\"" . $vv[$fieldLabel] . " " . $Lang["eForm"]["preview"] . "\"><img src=\"/images/".$LAYOUT_SKIN."/icon_view.gif\" height=\"20\" width=\"20\" align=\"absmiddle\" border=0> " . $vv[$fieldLabel] . "</a>";
										} else {
											$dataArr[$kk][$fieldIndx] = "-";
										}
										break;
									default:
										if (isset($vv[$fieldLabel])) $dataArr[$kk][$fieldIndx] = $vv[$fieldLabel];
										else $dataArr[$kk][$fieldIndx] = "-";
										break;
								}
							}
						} else {
							$dataArr[$kk] = array_values($vv);
						}
						if ($thisIsUsed) {
							$dataArr[$kk][] = "-";
						} else {
							$dataArr[$kk][] = "<input type=checkbox name='groupIdAry[]' id='groupIdAry[]' value='" . $vv["FormID"] . "'>";
						}
						$kk++;
					}
				}
			}
			return $dataArr;
		}
		
		####################################################################################################
		## Get Form Information By ID
		####################################################################################################
		function getFormInfoById($FormID, $extraArgs = array()) {
			if (count($extraArgs)) extract($extraArgs);
			$formInfo = array();
			$strSQL = "SELECT FormID, FolderID, TemplateID, FormTitle, FormRecordType, StartDate, DueDate, AllowDraft, FormStatus FROM " . $this->formInfoTable . " WHERE ModuleTitle='" . $this->ModuleTitle . "' AND " . $this->isDeletedCondition() . " AND FormID='" . $FormID. "'";
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				$formInfo = $result[0];
				$formInfo["TotalTargetUser"] = $this->getTargetUsers($formInfo["FormID"], $extraArgs = array('totalonly' => true ));
				
				
				$strSQL = "SELECT YearTermID, AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE '".$formInfo["StartDate"]."' BETWEEN TermStart AND TermEnd";
                $result = $this->returnResultSet($strSQL);
                $formInfo["YearTermID"] = $result["0"]["YearTermID"];
                $formInfo["AcademicYearID"] = $result["0"]["AcademicYearID"];
			}
			return $formInfo;
		}
		
		####################################################################################################
		## Get Target User By ID
		####################################################################################################
		function getTargetUsers($FormID, $extraArgs = array()) {
			if (count($extraArgs)) extract($extraArgs);
			$targetUser = array();
			if ($FormID > 0) {
				$fieldstr = "itutb.UserID, EnglishName, ChineseName, UserLogin";
				if (isset($extraArgs["totalonly"]) && $extraArgs["totalonly"]) {
					$fieldstr = "count(itutb.UserID) as TotalUser";
				}
				$strSQL = "SELECT " . $fieldstr . " FROM " . $this->formInfoTargetUserTable. " AS itutb
							JOIN " . $this->userTable . " AS ui ON (ui.UserID=itutb.UserID) 
							WHERE FormID='" . $FormID . "' AND ui.RecordStatus='1'";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					if (isset($extraArgs["totalonly"]) && $extraArgs["totalonly"]) {
						$targetUser = $result["0"]["TotalUser"];
					} else {
						$targetUser = BuildMultiKeyAssoc($result, "UserID");
					}
				}
			}
			return $targetUser;
		}
		
		####################################################################################################
		## Handle form submit
		####################################################################################################
		function editFormInformation($args, $target=array()) {
			$formInfoArr = array(
				"TemplateID" => $args["TemplateID"],
			    "FolderID" => isset($args["folder"]) ? $args["folder"] : '0',
				"ModuleTitle" => $this->ModuleTitle,
				"FormTitle" => $args["FormTitle"],
				"StartDate" => $args["StartDate"],
				"DueDate" => $args["DueDate"],
				"FormStatus" => 1,
				"FormRecordType" => $args["type"]
			);
			$isUpdate = false;
			if (isset($args["FormID"]) && $args["FormID"] > 0) {
				$FormID = $args["FormID"];
				$strSQL = "SELECT FormID FROM " . $this->formInfoTable . " WHERE FormID='" . $args["FormID"] . "' AND ModuleTitle='" . $this->ModuleTitle . "'";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					$isUpdate = true;
				} else {
					$formInfoArr["FormID"] = $args["FormID"];
				}
			}
			if (!$isUpdate) {
				$formInfoArr["InputBy"] = $_SESSION["UserID"];
				$formInfoArr["DateInput"] = date("Y-m-d H:i:s");
			}
			
			$formInfoArr["ModifyBy"] = $_SESSION["UserID"];
			$formInfoArr["DateModified"] = date("Y-m-d H:i:s");
			
			if ($isUpdate) {
				$strSQL = "UPDATE " . $this->formInfoTable . " SET ";
				$update_field = "";
				foreach ($formInfoArr as $kk => $vv) {
					if (!empty($update_field)) {
						$update_field .= ", ";
					}
					$update_field .= " " . $kk . "='" . $vv . "' ";
				}
				if (!empty($update_field)) {
					$strSQL .= $update_field . " WHERE FormID='" . $args["FormID"] . "' AND ModuleTitle='" . $this->ModuleTitle . "'";
				}
			} else {
				$strSQL = "INSERT INTO " . $this->formInfoTable . " (" . implode(", ", array_keys($formInfoArr)) . ") VALUES ('" . implode("', '", array_values($formInfoArr)) . "')";
			}
			$result = $this->db_db_query($strSQL);
			if (!$isUpdate) {
				if (isset($formInfoArr["FormID"])) {
					$FormID = $formInfoArr["FormID"];
				} else {
					$FormID = $this->db_insert_id();
				}
			}
			$removedUser = array();
			if ($result) {
				$removedUser = $this->updateTargetUsers($FormID, $target);
			}
			return array( "result" => $result, "fid" => $FormID, "remUser" => $removedUser);
		}
		
		####################################################################################################
		## Edit target User List
		####################################################################################################
		function updateTargetUsers($FormID, $targetUser = array()) {
			$removedUser = array();
			if ($FormID > 0) {
				if (count($targetUser) > 0) {
					$strSQL = "SELECT UserID FROM " . $this->formInfoTargetUserTable . " WHERE FormID='" . $FormID . "' AND UserID NOT IN (" . implode(", ", $targetUser) . ")";
					$result = $this->returnVector($strSQL);
					if ($result) {
						$removedUser = $result;
					}
					$strSQL = "DELETE FROM " . $this->formInfoTargetUserTable . " WHERE FormID='" . $FormID . "' AND UserID NOT IN (" . implode(", ", $targetUser) . ")";
				} else {
					$strSQL = "DELETE FROM " . $this->formInfoTargetUserTable . " WHERE FormID='" . $FormID . "'";
				}
				$this->db_db_query($strSQL);
				
				$strSQL = "SELECT UserID FROM " . $this->formInfoTargetUserTable . " WHERE FormID='" . $FormID . "' AND UserID IN (" . implode(", ", $targetUser) . ")";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					$existingUsers = BuildMultiKeyAssoc($result, "UserID");
					$existingUsers = array_keys($existingUsers);
					$finalUsersList = array_diff($targetUser, $existingUsers);
				} else {
					$finalUsersList = $targetUser;
				}
				
				if (count($finalUsersList) > 0) {
					foreach ($finalUsersList as $kk => $vv) {
						$param = array(
							"FormID" => $FormID,
							"UserID" => $vv,
							"InputBy" => $_SESSION["UserID"],
							"DateInput" => date("Y-m-d H:i:s"),
							"ModifyBy" => $_SESSION["UserID"],
							"DateModified" => date("Y-m-d H:i:s")
						);
						$strSQL = "INSERT INTO " . $this->formInfoTargetUserTable . " (" . implode(", ", array_keys($param)) . ") VALUES ('" . implode("', '", array_values($param)) . "')";
						$result = $this->db_db_query($strSQL);
					}
				}
			}
			return $removedUser;
		}

		####################################################################################################
		function removeTargetUser($FormID, $targetUser = array()) {
			if ($FormID> 0 && count($targetUser) > 0) {
				$strSQL = "DELETE FROM " . $this->formInfoTargetUserTable . " WHERE FormID='" . $FormID . "' AND UserID IN (" . implode(", ", $targetUser) . ")";
				$this->db_db_query($strSQL);
			}
		}
		
		####################################################################################################
		function removeForm($groupID) {
			global $indexVar, $Lang;
			if (count($groupID) > 0) {
				$strSQL = "SELECT TemplateID FROM " . $this->formInfoTable . " WHERE FormID IN ('" . implode("', '", $groupID) . "')";
				$TemplateIDs = $this->returnVector($strSQL);
				if (count($TemplateIDs) > 0) {
					$indexVar["FormTemplate"]->removeTableTemplate($TemplateIDs);
				}
	
				if ($this->allowDirectDelete) {
					$strSQL = "DELETE FROM " . $this->formInfoTable . " WHERE FormID IN ('" . implode("', '", $groupID) . "')";
					$this->db_db_query($strSQL);
					
				} else {
					$strSQL = "UPDATE " . $this->formInfoTable. " SET isDeleted='1', DeletedDate=NOW(), ModifyBy='" . $_SESSION["UserID"] . "' WHERE FormID IN ('" . implode("', '", $groupID) . "') AND " . $this->isDeletedCondition();
					$this->db_db_query($strSQL);
				}
				
				$strSQL = "DELETE FROM " . $this->formInfoTargetUserTable . " WHERE FormID IN ('" . implode("', '", $groupID) . "')";
				$this->db_db_query($strSQL);
				
				$indexVar["FormGrid"]->removeGridData($groupID);
			}
			return $groupID;
		}
	}
}
?>