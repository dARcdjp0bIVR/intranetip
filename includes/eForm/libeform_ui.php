<?php
// editing by
/*****************************************************************************
 * Modification Log:
 *      Date : 2018-03-12 Frankie
 *      -   Add Folder Structure to eForm
 *
 *      Date : 2017-02-08 Frankie
 * 		-	Add getSLRSInfoForTimeTable for Table Time
 *
 *****************************************************************************/

if (!defined("LIBEFORM_UI_DEFINED")) {
	define("LIBEFORM_UI_DEFINED", true);

	class libeform_ui extends interface_html {

		####################################################################################################
	    public function libeform_ui($parTemplate='') {
			$template = ($parTemplate=='')? 'default.html' : $parTemplate;
			$this->interface_html($template);
			$this->isDebug = false;
			$this->elmTypeArr = array();
		}
		public function setElmTypeArr($ElmTypeArr) {
			$this->elmTypeArr= $ElmTypeArr;
		}
		####################################################################################################
		public function echoModuleLayoutStart($parPageCode, $parReturnMsg='', $parForPopup=false) {
			global $indexVar, $CurrentPage, $MODULE_OBJ, $TAGS_OBJ, $CurrentPageArr, $PATH_WRT_ROOT, $intranet_session_language;

			$CurrentPage = $parPageCode;
			$MODULE_OBJ = $indexVar['libeform']->getModuleObjArr();
			$TAGS_OBJ = $indexVar['libeform']->geteServiceTagObj();
			if (strpos($parPageCode, 'forminfo') !== false) {
				$CurrentPageArr['eServiceeForm'] = 1;
			} else {
				$CurrentPageArr['eAdmineForm'] = 1;
			}

			if ($parForPopup) {
				$this->interface_html('popup.html');
			}

			$this->LAYOUT_START($parReturnMsg);
			echo '<!-- task: '.$indexVar['taskScript'].'-->';
			echo '<!-- template: '.$indexVar['templateScript'].'-->';
		}

		####################################################################################################
		public function echoModuleLayoutStop() {
			$this->LAYOUT_STOP();
		}

		####################################################################################################
		/**
		 * Modified Information:
		 * -	[2017-11-22 - Frankie] For adding copy function
		 *
		 * @param array $dataArr
		 * @param string $isLockEditScheme
		 * @param string $pageAction
		 * @return string
		 */
		public function return_FormContent($dataArr = array(), $isLockEditScheme = false, $pageAction='edit')
		{
			global $PATH_WRT_ROOT, $Lang, $indexVar;

			if (count($dataArr) > 0) extract($dataArr);
			if ($pageAction == "copy")
			{
				$FormTitle .= " - " . $Lang["eForm"]["img_alt_copy"];
				$StartDate = "";
				$DueDate = "";
			}
			/*
			include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$li = new libfilesystem();
			$objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "Basic2_withInsertImageFlash", htmlspecialchars_decode($Description));
			$objHtmlEditor->Config['FlashImageInsertPath'] = $li->returnFlashImageInsertPath($cfg['fck_image']['eCircular'], $id);
			*/
			$x = "
				<table class=\"form_table_v30\">";

			if (!$isLockEditScheme) {
				$x .= "
					<tr>
						<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["th_FormTitle"] ."</td>
						<td class='field_content_short required'>
							<input type='text' name='FormTitle' class='textboxtext' value='". $FormTitle."'>
							<br><span id='div_FormTitle_err_msg'></span>
						</td>
					</tr>
					<tr>
						<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["StartDate"] ."</td>
						<td class='field_content_short required'>" . $indexVar["libinterface"]->GET_DATE_PICKER('StartDate', $StartDate) . "
						<br><span id='div_StartDate_err_msg'></span>
						</td>
					</tr>
					<tr>
						<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["DueDate"]."</td>
						<td class='field_content_short required'>" . $indexVar["libinterface"]->GET_DATE_PICKER('DueDate', $DueDate) . "
						<br><span id='div_DueDate_err_msg'></span>
						</td>
					</tr>";

				$x .= "	<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["FormScheme"]."</td>
							<td class='field_content_short json_required'>";

				$x .= "		<button class='thickboxBTN' rel='/templates/eForm/'>" . $Lang["eForm"]["EditFormScheme"] . "</button>
							<br><span id='div_FormTemplateJSON_err_msg'></span>";
				$x .= "</td>
						</tr>";
			} else {
				$x .= "
						<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["th_FormTitle"] ."</td>
							<td class='field_content_short required'>
								<input type='hidden' name='FormTitle' class='textboxtext' value='". $FormTitle."'>" . $FormTitle . "
								<br><span id='div_FormTitle_err_msg'></span>
							</td>
						</tr>
						<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["StartDate"] ."</td>
							<td class='field_content_short required'><input type='hidden' name='StartDate' value='" . $StartDate . "'>" . $StartDate. "
							<br><span id='div_StartDate_err_msg'></span>
							</td>
						</tr>
						<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["DueDate"]."</td>
							<td class='field_content_short required'><input type='hidden' name='DueDate' value='" . $DueDate . "'>" . $DueDate . "
							<br><span id='div_DueDate_err_msg'></span>
							</td>
						</tr>
						<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["FormScheme"]."</td>
							<td class='field_content_short json_required'>-</td>
						</tr>";
			}
			$x .= "<tr><td colspan='2' id='editlocked' style='border-bottom:0px;'><div>** " . $Lang["eForm"]["Msg_eFormOnProcessingCanNotEdit"]. "</div></td><tr>";
			if ($pageAction == "copy")
			{
				$x .= "	</table>
						<input type='hidden' name='FormID' value=''>
						<input type='hidden' name='TemplateID' value=''>
						<input type='hidden' name='token' value='" . md5("-eForm-". $_SESSION["UserID"]) . "'>
						<input type='hidden' name='FormSchemeJSON' id='FormSchemeJSON' value=''>
						<div id='initJSON' style='display:none;'>" . $TemplateInfo["FormElementsJSON"] . "</div>
					";
			}
			else
			{
				$x .= "	</table>
						<input type='hidden' name='FormID' value='" . $FormID. "'>
						<input type='hidden' name='TemplateID' value='" . $TemplateID. "'>
						<input type='hidden' name='token' value='" . md5($FormID . $TemplateID . "-eForm-". $_SESSION["UserID"]) . "'>
						<input type='hidden' name='FormSchemeJSON' id='FormSchemeJSON' value=''>
						<div id='initJSON' style='display:none;'>" . $TemplateInfo["FormElementsJSON"] . "</div>
					";
			}

			$x .= $this->getLoadingElm();
			return $x;
		}

		####################################################################################################
		/**
		 * Modified Information:
		 * -	[2017-11-22 - Frankie] For adding copy function
		 *
		 * @param array $dataArr
		 * @param string $isView
		 * @param string $pageAction
		 * @return string
		 */
		public function return_FormTemplateContent($dataArr = array(), $isView = false, $pageAction='edit') {
			global $PATH_WRT_ROOT, $Lang, $indexVar;
			if ($pageAction == "add") $pageAction = "edit";
			if (count($dataArr) > 0) extract($dataArr);
			/*
			 include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
			 include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			 $li = new libfilesystem();
			 $objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "Basic2_withInsertImageFlash", htmlspecialchars_decode($Description));
			 $objHtmlEditor->Config['FlashImageInsertPath'] = $li->returnFlashImageInsertPath($cfg['fck_image']['eCircular'], $id);
			 */
			if ($pageAction == "copy")
			{
				$TemplateTitle .= " - " . $Lang["eForm"]["img_alt_copy"];
			}
			$is_Fixed = "";
			$is_Dynamic = "";
			if ($TemplateType == "DynamicTable") {
				$is_Dynamic = " selected";
			} else {
				$is_Fixed = " selected";
			}
			if (empty($TemplateSettingsJSON["settings_noofrows"]) || $TemplateSettingsJSON["settings_noofrows"] <= 0) {
				$TemplateSettingsJSON["settings_noofrows"] = "1";
			}
			if (empty($TemplateSettingsJSON["settings_maxrows"]) || $TemplateSettingsJSON["settings_maxrows"] <= 0) {
				$TemplateSettingsJSON["settings_maxrows"] = "0";
			}
			$x = "<table class=\"form_table_v30\">";
			if ($isUsed && $pageAction != "copy") {
				$x .= "<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["th_TemplateTitle"] ."</td>
							<td class='field_content_short required'>" . $TemplateTitle . "</td>
						</tr>";
			} else {
				$x .= "<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["th_TemplateTitle"] ."</td>
							<td class='field_content_short required'>
								<input type='text' name='TemplateTitle' class='textboxtext' value='". $TemplateTitle."'>
								<br><span id='div_TemplateTitle_err_msg'></span>
							</td>
						</tr>";
			}
			if ($isUsed && $pageAction != "copy") {
				$x .= "<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["th_TemplateType"] ."</td>
							<td class='field_content_short required'>";
				if ($is_Dynamic) {
					$x .= $Lang["eForm"]["DynamicTable"];
				} else {
					$x .= $Lang["eForm"]["FixedTable"];;
				}
				$x .= "		<select name='TemplateType' style='display:none;'>
									<option value='FixedTable'" . $is_Fixed . ">" . $Lang["eForm"]["FixedTable"] . "</option>
									<option value='DynamicTable'" . $is_Dynamic. ">" . $Lang["eForm"]["DynamicTable"] . "</option>
								</select>
								<br><span id='div_TemplateType_err_msg'></span></td>
							</tr>";
			} else {
				$x .= "<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["th_TemplateType"] ."</td>
							<td class='field_content_short required'>
							<select name='TemplateType'>
								<option value='FixedTable'" . $is_Fixed . ">" . $Lang["eForm"]["FixedTable"] . "</option>
								<option value='DynamicTable'" . $is_Dynamic. ">" . $Lang["eForm"]["DynamicTable"] . "</option>
							</select>
							<br><span id='div_TemplateType_err_msg'></span></td>
						</tr>";
			}
			if ($isUsed && $pageAction != "copy") {
				$x .= "<tr id='dynTblMax' style='display:none;'>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["th_MaxRows"] ."</td>
							<td class='field_content_short required'>". $TemplateSettingsJSON["settings_maxrows"] . "</td></tr>";
			} else {
				$x .= "<tr id='dynTblMax' style='display:none;'>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["th_MaxRows"] ."</td>
							<td class='field_content_short required'>
							<input type='number' name='settings_maxrows' value='" . $TemplateSettingsJSON["settings_maxrows"] . "'>
							<br><span id='div_TemplateType_err_msg'></span>
							</td>
						</tr>";
			}
			if ($isUsed && $pageAction != "copy") {
				$x .= "<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> <span id='fixTbl' style='display:none;'>". $Lang["eForm"]["th_NoOfRows"] ."</span> <span id='dynTblMin' style='display:none;'>". $Lang["eForm"]["th_MinRows"] ."</span></td>
							<td class='field_content_short required'>
							" . $TemplateSettingsJSON["settings_noofrows"] . "<input type='hidden' name='settings_noofrows' value='" . $TemplateSettingsJSON["settings_noofrows"] . "'></td>
							</tr>";
			} else {
				$x .= "<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> <span id='fixTbl' style='display:none;'>". $Lang["eForm"]["th_NoOfRows"] ."</span> <span id='dynTblMin' style='display:none;'>". $Lang["eForm"]["th_MinRows"] ."</span></td>
							<td class='field_content_short required'>
							<input type='number' name='settings_noofrows' value='" . $TemplateSettingsJSON["settings_noofrows"] . "'>
							<br><span id='div_TemplateType_err_msg'></span></td>
						</tr>";
			}
			if ($isUsed && $pageAction != "copy") {
				$x .= "<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["TemplateScheme"]."</td>
							<td class='field_content_short json_required'>
							<div id='tablesetting' style='display:none;'></div></td>
						</tr>";
			} else {
				$x .= "<tr>
							<td class='field_title'><span class='tabletextrequire'>*</span> ". $Lang["eForm"]["TemplateScheme"]."</td>
							<td class='field_content_short json_required'>
							<button class='thickboxBTN' rel='/templates/eForm/?type=table'>" . $Lang["eForm"]["EditScheme"] . "</button>
							<br><span id='div_TemplateJSON_err_msg'></span>
							<div id='tablesetting' style='display:none; margin-top:10px;'></div>
							<div class='fixedtable_msg'>** " . $Lang["eForm"]["Msg_eFormFixedTableEditing"] . "</div></td>
						</tr>";
			}
			$x .= "<tr><td colspan='2' style='border-bottom:0px;'><div>** " . $Lang["eForm"]["Msg_TableOnProcessingCanNotEdit"] . "</div></td><tr>";
			if ($isUsed) {
				$x .= "</table>";
				$x .= "<input type='hidden' name='SchemeJSON' id='FormSchemeJSON' value=''>
				<input type='hidden' name='SettingsJSON' id='SettingsJSON' value=''>
				<div id='initJSON' style='display:none;'>" . $FormElementsJSON . "</div>
				<div id='initTableJSON' style='display:none;'>" . $TemplateSettingsJSON["SettingsJSON"] . "</div>";
			} else {
				$x .= "</table>";
				if ($pageAction == "copy")
				{
					$x .= "<input type='hidden' name='TemplateID' value=''>
								<input type='hidden' name='token' value='" . md5('' . "-eForm-" . $_SESSION["UserID"]) . "'>
								<input type='hidden' name='SchemeJSON' id='FormSchemeJSON' value=''>
								<input type='hidden' name='SettingsJSON' id='SettingsJSON' value=''>
								<div id='initJSON' style='display:none;'>" . $FormElementsJSON . "</div>
								<div id='initTableJSON' style='display:none;'>" . $TemplateSettingsJSON["SettingsJSON"] . "</div>
							";
				}
				else
				{
					$x .= "<input type='hidden' name='TemplateID' value='" . $TemplateID. "'>
								<input type='hidden' name='token' value='" . md5($TemplateID . "-eForm-" . $_SESSION["UserID"]) . "'>
								<input type='hidden' name='SchemeJSON' id='FormSchemeJSON' value=''>
								<input type='hidden' name='SettingsJSON' id='SettingsJSON' value=''>
								<div id='initJSON' style='display:none;'>" . $FormElementsJSON . "</div>
								<div id='initTableJSON' style='display:none;'>" . $TemplateSettingsJSON["SettingsJSON"] . "</div>
							";
				}
				$x .= $this->getLoadingElm();
			}
			return $x;
		}

		####################################################################################################
		public function return_FormBuilderByTemplate($formInfo = array(), $data = array(), $isPreview = false, $isDisplay = false) {

			$this->dataArr = array();
			$this->isPreview = $isPreview;
			$this->isDisplay = $isDisplay;
			global $Lang, $indexVar;
			if ($isPreview) {
				$strHTML = "<div class='form-header'>" . $formInfo["FormTitle"] . " " . $Lang["eForm"]["preview"] . "</div>";
			} else {
				$strHTML = "<div class='form-header'>" . $formInfo["FormTitle"] . "</div>";
			}
			if ($formInfo["GridInfo"]["SuppReqInfo"] !== NULL && $formInfo["GridInfo"]["GridStatus"] != $this->GridStatusArr["COMPLETED"]) {
				$strHTML .= "<div class='form-suppreq'>";
				$strHTML .= $this->getSuppHTMLBody($formInfo["GridInfo"]["SuppReqInfo"]);
				$strHTML .= "</div>";
			}
			if (!$isDisplay) {
				$strHTML .= "<div class='form-info'>";
				$strHTML .= "	<div class='inforow'>";
				$strHTML .= "		<label class='infolabel'>" . $Lang["eForm"]["StartDate"] . " :</label>";
				$strHTML .= "		<div class='infoVal'>" . $formInfo["StartDate"] . "</div>";
				$strHTML .= "	</div>";
				$strHTML .= "	<div class='clearboth'></div>";
				$strHTML .= "	<div class='inforow'>";
				$strHTML .= "		<label class='infolabel'>" . $Lang["eForm"]["DueDate"] . " :</label>";
				$strHTML .= "		<div class='infoVal'>" . $formInfo["DueDate"] . "</div>";
				$strHTML .= "	</div>";
				$strHTML .= "	<div class='clearboth'></div>";
				$strHTML .= "</div>";
			}
			if ($isPreview) {
				$strHTML .= "<form name='eFormMod preview'>";
			} else {
				if (!$isDisplay) {
					$strHTML .= "<form name='eFormMod' id='userform' method='POST' action='?task=forminfo/editform_update' target='submithdr'>";
				} else {
					$strHTML .= "<form name='eFormMod'>";
				}
			}
			$strHTML .= "<div class='form-body'>";
			if (count($formInfo["TemplateInfo"]) > 0) {
				$elmNumIndex = 0;
				foreach ($formInfo["TemplateInfo"]["FormElementsJSON"] as $key => $val) {
					$i = $elmNumIndex;
					if ($isDisplay) {
						$val["isDisplayMode"] = true;
					} else {
						$val["isDisplayMode"] = false;
					}

					if (isset($formInfo["YearTermID"])) {
					    $val["YearTermID"] = $formInfo["YearTermID"];
					}
					if (isset($formInfo["AcademicYearID"])) {
					    $val["AcademicYearID"] = $formInfo["AcademicYearID"];
					}

					$elmBody = $this->elmSwapHandle($val, $data, true, $formInfo["TemplateInfo"]["TableInfo"]);
					if ($val["type"] == "hidden") {
						if ($this->isDebug) {
							$strHTML .= "<div class='elm'>";
							$strHTML .= "<div class='elm-type'>";
							if (isset($val["name"])) {
								$strHTML .= "Name: <u>" . $val["name"] . "</u><br>";
							}
							$strHTML .= "Type: <u>" . strtoupper($val["type"]) . "</u><br>";
							$strHTML .= "Custom Name: <u>" . $val["custom_name"] . "</u><br>";
							if ($val["required"]) {
								$strHTML .= "Required: <u>TRUE</u>";
							}
							$strHTML .= "</div>";
							$strHTML .= "</div>";
						}
						$strHTML .= $elmBody;
					} else {
						$strHTML .= "<div class='elm'>";
						if ($this->isDebug) {
							$strHTML .= "<div class='elm-type'>";
							if (isset($val["name"])) {
								$strHTML .= "Name: <u>" . $val["name"] . "</u><br>";
							}
							if (isset($val["fieldid"])) {
								$strHTML .= "Field ID: <u>" . $val["fieldid"] . "</u><br>";
							}
							$strHTML .= "Type: <u>" . strtoupper($val["type"]) . "</u><br>";
							$strHTML .= "Custom Name: <u>" . $val["custom_name"] . "</u><br>";
							if ($val["required"]) {
								$strHTML .= "Required: <u>TRUE</u><br>";
							}
							$strHTML .= "AcademicYearID: <u>" . $val["AcademicYearID"] . "</u><br>";
							$strHTML .= "YearTermID: <u>" . $val["YearTermID"] . "</u><br>";
							$strHTML .= "</div>";
						}
						if (!empty($elmBody)) {
							$strHTML .= "<div class='elm-body'>" . $elmBody. "</div>";
						}
						$strHTML .= "</div>";
					}
					$elmNumIndex++;
				}
			}
			$strHTML .= "</div>";

			// $buttonStyle = "formBtn btn-blue"; ## Blue
			// $buttonStyle = "formBtn btn-green"; ## Green
			// $buttonStyle = "formBtn btn-red"; ## Red
			$buttonStyle = "formbutton_v30";

			if (!$isPreview && !$isDisplay) {
				$strHTML .= $this->getLoadingElm();
				$strHTML .= "<div class='form-foot'>";
				$strHTML .= "	<div class='elm'>";
				$strHTML .= "		<div class='elm-body text-center'>";
				$strHTML .= "			<input type='button' class='submitBTN " . $buttonStyle . "' value='" . $Lang["eForm"]["SubmitButton"] . "' /> ";
				$strHTML .= "			<input type='button' class='saveBTN " . $buttonStyle . "' value='" . $Lang["eForm"]["SaveAsDraftButton"] . "' /> ";
				$strHTML .= "			<input type='button' class='cancelBTN " . $buttonStyle . "' value='" . $Lang["eForm"]["CancelButton"] . "' /> ";
				// $strHTML .= "			<input type='button' class='formBtn' value='" . $Lang["eForm"]["EditButton"] . "' /> ";
				$strHTML .= "		</div>";
				$strHTML .= "	</div>";
				$strHTML .= "</div>";
			}
			$strHTML .= "<input type='hidden' name='fid' value='" . $formInfo["FormID"] . "'>";
			$strHTML .= "<input type='hidden' name='gid' value='" . $formInfo["GridInfo"]["GridID"]. "'>";
			$strHTML .= "<input type='hidden' name='saveAsDraft' value='1'>";
			$strHTML .= "<input type='hidden' name='stk' value='" . md5($formInfo["FormID"] . "_" . $formInfo["GridInfo"]["GridID"] . "_TRUE-eForm-" . $_SESSION["UserID"]) . "'>";
			$strHTML .= "<input type='hidden' name='ftk' value='" . md5($formInfo["FormID"] . "_" . $formInfo["GridInfo"]["GridID"] . "_FALSE-eForm-" . $_SESSION["UserID"]) . "'>";
			$strHTML .= "</form>";
			return $strHTML;
		}

		public function elmSwapHandle($elm = array(), $data = array(), $NotFromTable = true, $TableInfo = array()) {

			switch ($elm["type"]) {
				case "header":
					$elmBody = $this->formElm_header($TableInfo, $elm);
					break;
				case "paragraph" :
					$elmBody = $this->formElm_paragraph($TableInfo, $elm);
					break;
				case "fixedTableElm" :
					if ($NotFromTable) {
						if (isset($TableInfo[$elm["value"]])) {
							$elm["tplInfo"] = $TableInfo[$elm["value"]];
						}
					}
					$elmBody = $this->formElm_fixedTableElm($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "dynamicTableElm" :
					if ($NotFromTable) {
						if (isset($TableInfo[$elm["value"]])) {
							$elm["tplInfo"] = $TableInfo[$elm["value"]];
						}
					}
					$elmBody = $this->formElm_dynamicTableElm($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "textarea" :
					$elmBody = $this->formElm_textarea($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "text" :
					$elmBody = $this->formElm_text($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "number" :
					$elmBody = $this->formElm_number($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "checkbox-group" :
					$elmBody = $this->formElm_checkbox($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "radio-group" :
					$elmBody = $this->formElm_radio($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "select" :
					$elmBody = $this->formElm_select($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "dateFieldElm" :
					$elmBody = $this->formElm_dateFieldElm($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "hidden" :
					$elmBody = $this->formElm_hidden($TableInfo, $elm, $data, $NotFromTable);
					break;
				case "timeFieldElm" :
				    $elmBody = $this->formElm_timeFieldElm($TableInfo, $elm, $data, $NotFromTable);
				    break;
				case "subjectGroupElm" :
				case "teacherElm" :
				case "studentElm" :
				    $elmBody = $this->formElm_dualSelectElm($TableInfo, $elm, $data, $NotFromTable);
				    break;
			    default:
					$elmBody = "";
					break;
			}
			return $elmBody;
		}
		/*
		 * @param ($elm, $data)
		 * @return HTML
		 */
		public function formElm_header($tableInfo = array(), $elm = array()) {
			$elmBody = "";
			switch ($elm["subtype"]) {
				case "h1": $elmBody = sprintf("<h1>%s</h1>", $elm["label"]); break;
				case "h2": $elmBody = sprintf("<h2>%s</h2>", $elm["label"]); break;
				default:
					$elmBody = sprintf("<h3>%s</h3>", $elm["label"]); break;
			}
			return $elmBody;
		}

		/*
		 * @param ($elm, $data)
		 * @return HTML
		 */
		public function formElm_paragraph($tableInfo = array(), $elm = array()) {
			return "<div class='elmParagraph'>" . str_replace("\n", "<br>", $elm["label"]) . "</div>";
		}


		public function formElm_fixedTableElm($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			return $this->formElm_TableElm('fixed', $tableInfo, $elm, $data, $NotFromTable);
		}

		public function formElm_dynamicTableElm ($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			return $this->formElm_TableElm('dynamic', $tableInfo, $elm, $data, $NotFromTable);
		}

		public function formElm_TableElm($tableType = 'dynamic', $tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			global $Lang;
			$elmBody = "";
			if (isset($tableInfo[$elm["value"]])) {
				$thisTemplateInfo = $tableInfo[$elm["value"]];
				$elmBody = "";
				if ($NotFromTable) {
					$elmBody .= "<label class='elmLabel'>" . $elm["label"] . "</label>";
					if (isset($thisTemplateInfo["FormElementsJSON"]) && count($thisTemplateInfo["FormElementsJSON"]) > 0) {

						if ($thisTemplateInfo["TemplateSettingsJSON"]["settings_noofrows"] <= 0)
							$thisTemplateInfo["TemplateSettingsJSON"]["settings_noofrows"] = 1;
						if ($tableType == "dynamic") {
							if (empty($thisTemplateInfo["TemplateSettingsJSON"]["settings_maxrows"]) || $thisTemplateInfo["TemplateSettingsJSON"]["settings_maxrows"] < 0)
								$thisTemplateInfo["TemplateSettingsJSON"]["settings_maxrows"] = 0;
							$replaceArr = array(
									"_EF_MIN_ROW_" => $thisTemplateInfo["TemplateSettingsJSON"]["settings_noofrows"],
									"_EF_MAX_ROW_" => $thisTemplateInfo["TemplateSettingsJSON"]["settings_maxrows"]
							);
							$elmBody .= "<br>" . str_replace(array_keys($replaceArr), array_values($replaceArr), $Lang["eForm"]["dynamicLimitMsg"]);
						}
					}
				}
				switch ($tableType) {
					case "fixed":
						$tableClass = "fixedTable";
						break;
					case "dynamic":
					default:
						$tableClass = "dynamicTable";
						break;
				}
				if ($elm["isDisplayMode"]) {
					$tableClass = " displayTable";
				}

				$elmBody .= "<table class='" . $tableClass . "' cellspacing=0 cellpadding=0 >";
				if (isset($thisTemplateInfo["FormElementsJSON"]) && count($thisTemplateInfo["FormElementsJSON"]) > 0) {
					$minRecord = 2;
					if ($tableType == "fixed") {
						if (isset($elm["tplInfo"]["TemplateSettingsJSON"]["SettingsJSON"]["tddata"])) {
							$presetValues = $elm["tplInfo"]["TemplateSettingsJSON"]["SettingsJSON"]["tddata"];
						}
						$minRecord = $thisTemplateInfo["TemplateSettingsJSON"]["settings_noofrows"];
					} else {
						if ($thisTemplateInfo["TemplateSettingsJSON"]["settings_noofrows"] > $thisTemplateInfo["TemplateSettingsJSON"]["settings_maxrows"] && $thisTemplateInfo["TemplateSettingsJSON"]["settings_maxrows"] > 0) {
							$thisTemplateInfo["TemplateSettingsJSON"]["settings_noofrows"] = $thisTemplateInfo["TemplateSettingsJSON"]["settings_maxrows"];
						}
						$minRecord = $thisTemplateInfo["TemplateSettingsJSON"]["settings_noofrows"] + 1;
					}

					if (count($thisTemplateInfo["FormElementsJSON"]) > 0) {
						$width = floor(100 / count($thisTemplateInfo["FormElementsJSON"]));
					} else {
						$width = 0;
					}
					$elmBody .= "<thead>";
					$elmBody .= "<tr valign='top'>";

					foreach ($thisTemplateInfo["FormElementsJSON"] as $kk => $vv) {
						$required = "";
						if ($vv["required"]) {
							$required = " class='elmRequired'";
						}
						if ($width > 0) {
							$elmBody .= "<th style='width:" . $width . "%'" . $required . ">" . $vv["label"] . "</th>";
						} else {
							$elmBody .= "<th" . $required . ">" . $vv["label"] . "</th>";
						}
					}
					if ($tableType == "dynamic" && !$this->isPreview && !$this->isDisplay) {
						$elmBody .= "<th class='text-center' style='width:1%'" . $required . "><input type='checkbox'></th>";
					}
					$elmBody .= "</tr>";
					$elmBody .= "</thead>";
					$elmBody .= "<tbody>";
					$isDynamicFillRecord = false;
					if ($tableType == "dynamic") {
						$before_minRecord = $minRecord;
						if (isset($data["table"][$elm["custom_name"] . "-RWL4"][0]["arrFieldValue"]) && count($data["table"][$elm["custom_name"] . "-RWL4"][0]["arrFieldValue"]) > 0) {
							$isDynamicFillRecord = true;
							if (count($data["table"][$elm["custom_name"] . "-RWL4"][0]["arrFieldValue"]) > $minRecord) {
								$minRecord = count($data["table"][$elm["custom_name"] . "-RWL4"][0]["arrFieldValue"]);
							}
						}
					}
					for($i=0; $i < $minRecord; $i++ ) {
						if ($i == 0 && $tableType == "dynamic") {
							$elmBody .= "<tr class='TABLE_TPL'>";
						} else {
							if ($isDynamicFillRecord) {
								$elmBody .= "<tr rel='" . $data["table"][$elm["custom_name"] . "-RWL4"][0]["arrFieldValue"][$i] . "'>";
							} else {
								$elmBody .= "<tr rel='" . $i . "'>";
							}
						}
						$j=0;
						$elmNumIndex = 0;
						foreach ($thisTemplateInfo["FormElementsJSON"] as $kk => $vv) {
							if ($isDynamicFillRecord) {
								$vv["tableRowIndex"] = $data["table"][$elm["custom_name"] . "-RWL4"][0]["arrFieldValue"][$i];
							} else {
								$vv["tableRowIndex"] = $i;
							}
							$vv["tableColIndex"] = $elmNumIndex;
							if ($tableType == "dynamic") {
								if ($i == 0) {
									$vv["tableRowIndex"] = "_TPL_";
								}
							}
							if ($tableType == "fixed") {
								if (isset($presetValues[$i][$j]) && !empty($presetValues[$i][$j])) {
									$vv["presetValue"] = $presetValues[$i][$j];
								}
							}

							$t_elmBody = "";
							if ($this->isDebug) {
								$t_elmBody .= "<div class='elm-type'>";
								if (isset($vv["name"])) {
									$t_elmBody .= "Name: <u>" . $vv["name"] . "</u><br>";
								}
								if (isset($vv["fieldid"])) {
									$t_elmBody .= "Field ID: <u>" . $vv["fieldid"] . "</u><br>";
								}
								$t_elmBody .= "Type: <u>" . strtoupper($vv["type"]) . "</u><br>";
								$t_elmBody .= "Custom Name: <u>" . strtoupper($vv["custom_name"]) . "</u><br>";
								if ($vv["required"]) {
									$t_elmBody .= "Required: <u>TRUE</u>";
								}
								$t_elmBody .= "</div>";
							}
							$vv["isDisplayMode"] = $elm["isDisplayMode"];
							$vv["custom_name"] = $elm["custom_name"] . "_" . $vv["custom_name"];

							if (isset($elm["YearTermID"])) {
							    $vv["YearTermID"] = $elm["YearTermID"];
							}
							if (isset($elm["AcademicYearID"])) {
							    $vv["AcademicYearID"] = $elm["AcademicYearID"];
							}

							$t_elmBody .= $this->elmSwapHandle($vv, $data, false, null);
							if (empty($t_elmBody)) $t_elmBody = "&nbsp;";
							$elmBody .= "<td><div>" . $t_elmBody. "</div></td>";
							$j++;
							$elmNumIndex++;
						}
						if ($tableType == "dynamic" && !$this->isPreview && !$this->isDisplay) {
							if ($i == 0 || $i >= $before_minRecord) {
								$elmBody .= "<td class='text-center' style='width:1%'" . $required . "><input type='checkbox'>" . "<input type='hidden' name='" . $elm["custom_name"] . "-RWL4[" . $vv["tableRowIndex"] ."]' value='" . $vv["tableRowIndex"] . "'></td>";
							} else {
								$elmBody .= "<td class='text-center' style='width:1%; color:#fff;'" . $required . "><input type='hidden' name='" . $elm["custom_name"] . "-RWL4[" . $vv["tableRowIndex"] ."]' value='" . $vv["tableRowIndex"] . "'>&nbsp;-&nbsp;</td>";
							}
						}
					}
					$elmBody .= "</tr>";
					$elmBody .= "</tbody>";
				}
				$elmBody .= "</table>";
				##$buttonStyle = "formBtn btn-blue";
				##$buttonStyle = "formBtn btn-red";
				$buttonStyle = "formbutton_v30";
				if ($tableType == "dynamic" && !$this->isPreview && !$this->isDisplay) {
					$elmBody .= "<div class='text-right' style='margin-top:5px;'>";
					$elmBody .= "<input type='button' class='addRowBtn " . $buttonStyle . "' value='" . $Lang["eForm"]["AddRowButton"] . "'>";
					$elmBody .= " <input type='button' class='delRowBtn " . $buttonStyle . "' value='" . $Lang["eForm"]["DelRowButton"] . "'>";
					$elmBody .= "</div>";
				}
				$elmBody .= "<div class='clearboth'></div>";
			}
			return $elmBody;
		}

		public function formElm_text($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			global $Lang;
			$elmBody = "";
			$id = $elm["custom_name"];
			$elmID = $elm["custom_name"];
			$required = "";
			if (isset($elm["required"]) && $elm["required"] && !$elm["isDisplayMode"]) {
				$required = " required";
				$label_required = " elmRequired";
			}
			if ($NotFromTable) {
				$elmBody .= "<label class='elmLabel" . $label_required . "'>" . $elm["label"] . "</label>";
			} else {
				$elmID .= "[" . $elm["tableRowIndex"] . "]";
			}
			if ($this->isDebug) {
				$elmBody .= " ( <span class='text-red'>" . $elmID. "</span> )";
			}
			switch ($elm["subtype"]) {
				case "tel":
				case "email":
					$subtype = $elm["subtype"];
					break;
				case "text" :
				default: $subtype = "text";
				break;
			}
			$maxlength = "";
			if (isset($elm["maxlength"]) && !empty($elm["maxlength"])) {
				$maxlength = " maxlength='" . $elm["maxlength"] . "'";
			}

			$readonly = "";
			if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
				$value = $elm["presetValue"];
				/*
				$readonly = " readonly style='height:auto;'";
				$elmBody .= "<div class='elm_" . $elm["type"] . "'><textarea class='textarea-field' name='" . $elmID . "'";
				$elmBody .= $readonly .">" . $value . "</textarea></div>";
				*/
				$elmBody .= "<div class='elm_" . $elm["type"] . "'><div class='presetVal'>" . $value . "</div></div>";
			} else {
				if ($NotFromTable) {
					if (isset($data["text"][$id])) {
						$FieldValue = $data["text"][$id]["FieldValue"];
					}
				} else {
					if (isset($data["text"][$id][$elm["tableRowIndex"]])) {
						$FieldValue = $data["text"][$id][$elm["tableRowIndex"]]["FieldValue"];
					}
				}

				$elmBody .= "<div class='elm_" . $elm["type"] . "'>";
				if ($elm["isDisplayMode"]) {
					if (empty($FieldValue)) $FieldValue = "-";
					$elmBody .= "<div class='userdata'>" . $FieldValue . "</div>";
				} else {
					$elmBody .= "<input type='" . $subtype . "' class='input-field' " . $required. " name='" . $elmID . "' value=\"" . intranet_htmlspecialchars($FieldValue) . "\"" . $readonly . $maxlength;
					$elmBody .= " />";
				}
				$elmBody .= "</div>";
			}
			return $elmBody;
		}

		public function formElm_number($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			global $Lang;
			$elmBody = "";
			$id = $elm["custom_name"];
			$elmID = $elm["custom_name"];
			if (isset($elm["required"]) && $elm["required"] && !$elm["isDisplayMode"]) {
				$required = " required";
				$label_required = " elmRequired";
			}
			if ($NotFromTable) {
				$elmBody .= "<label class='elmLabel" . $label_required . "'>" . $elm["label"] . "</label>";
			} else {
				$elmID .= "[" . $elm["tableRowIndex"] . "]";
			}
			if ($this->isDebug) {
				$elmBody .= " ( <span class='text-red'>" . $elmID . "</span> )";
			}

			$readonly = "";
			if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
				$value = $elm["presetValue"];
				$readonly = " readonly";
			}
			$min = "";
			if (isset($elm["min"]) && !empty($elm["min"])) {
				$min = " min='" . $elm["min"] . "'";
			}
			$max = "";
			if (isset($elm["max"]) && !empty($elm["max"])) {
				$max = " max='" . $elm["max"] . "'";
			}
			$elmBody .= "<div class='elm_" . $elm["type"] . "'>";
			if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
				$value = $elm["presetValue"];
				$elmBody .= "<div class='elm_" . $elm["type"] . "'><div class='presetVal'>" . $value . "</div></div>";
			} else {
				if ($NotFromTable) {
					if (isset($data["number"][$id])) {
						$FieldValue = $data["number"][$id]["FieldValue"];
					}
				} else {
					if (isset($data["number"][$id][$elm["tableRowIndex"]])) {
						$FieldValue = $data["number"][$id][$elm["tableRowIndex"]]["FieldValue"];
					}
				}
				if ($elm["isDisplayMode"]) {
					if (empty($FieldValue)) $FieldValue = "-";
					$elmBody .= "<div class='userdata'>" .$FieldValue ."</div>";
				} else {
					$elmBody .= "<input type='text' class='input-field number-field' name='" . $elmID . "' value=\"" . intranet_htmlspecialchars($FieldValue) . "\"" . $readonly . $min . $max . $required;
					$elmBody .= " />";
				}
			}
			$elmBody .= "</div>";
			return $elmBody;
		}

		public function formElm_textarea($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			global $Lang;
			$elmBody = "";
			$id = $elm["custom_name"];
			$elmID = $elm["custom_name"];
			if (isset($elm["required"]) && $elm["required"] && !$elm["isDisplayMode"]) {
				$required = " required";
				$label_required = " elmRequired";
			}
			if ($NotFromTable) {
				$elmBody .= "<label class='elmLabel" . $label_required . "'>" . $elm["label"] . "</label>";
			} else {
				$elmID .= "[" . $elm["tableRowIndex"] . "]";
			}
			if ($this->isDebug) {
				$elmBody .= " ( <span class='text-red'>" . $elmID. "</span> )";
			}

			if ($NotFromTable) {
				if (isset($data["textarea"][$id])) {
					if ($elm["isDisplayMode"]) {
						$FieldValue = $data["textarea"][$id]["disFieldValue"];
					} else {
						$FieldValue = $data["textarea"][$id]["FieldValue"];
					}
				}
			} else {
				if (isset($data["textarea"][$id][$elm["tableRowIndex"]])) {
					if ($elm["isDisplayMode"]) {
						$FieldValue = $data["textarea"][$id][$elm["tableRowIndex"]]["disFieldValue"];
					} else {
						$FieldValue = $data["textarea"][$id][$elm["tableRowIndex"]]["FieldValue"];
					}
				}
			}

			$elmBody .= "<div class='elm_" . $elm["type"] . "'>";

			if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
				$value = $elm["presetValue"];
				$elmBody .= "<div class='elm_" . $elm["type"] . "'><div class='presetVal'>" . $value . "</div></div>";
			} else {
				if ($elm["isDisplayMode"]) {
					if (empty($FieldValue)) $FieldValue = "-";
					$elmBody .= "<div class='userdata'>" . nl2br($FieldValue) ."</div>";
				} else {
					$elmBody .= "<textarea class='textarea-field' name='" . $elmID . "'" . $required;
					if (isset($elm["maxlength"]) && $elm["maxlength"] > 0) {
						$elmBody .= " maxlength='" . $elm["maxlength"] . "'";
					}
					if (isset($elm["rows"]) && $elm["rows"] > 0) {
						$elmBody .= " rows='" . $elm["rows"] . "'";
					}
					$readonly = "";
					if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
						$value = $elm["presetValue"];
						$readonly = " readonly style='height:auto;'";
					}
					$elmBody .= $readonly .">" . str_replace("<br>", "\n", intranet_undo_htmlspecialchars($FieldValue)) . "</textarea>";
				}
			}
			$elmBody .= "</div>";
			return $elmBody;
		}

		public function formElm_checkbox($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			global $Lang;
			$elmBody = "";
			$id = $elm["custom_name"];
			$elmID = $elm["custom_name"];
			if (isset($elm["required"]) && $elm["required"] && !$elm["isDisplayMode"]) {
				$required = " required";
				$label_required = " elmRequired";
			}
			if ($NotFromTable) {
				$elmBody .= "<label class='elmLabel" . $label_required . "'>" . $elm["label"] . "</label>";
			} else {
				$elmID .= "[" . $elm["tableRowIndex"] . "]";
			}
			$elmID .= "[]";
			$ass_id = str_replace(array("[", "]"), "-", $elmID);
			if ($this->isDebug) {
				$elmBody .= " ( <span class='text-red'>" . $elmID. "</span> )";
			}
			$elmBody .= "<div class='elm_" . $elm["type"] . "'>";
			if (count($elm["values"]) > 0) {
				if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
					$value = $elm["presetValue"];
					foreach ($elm["values"] as $kk => $vv) {
						if ($vv["value"] == $elm["presetValue"]) {
							$value = $vv["label"];
						}
					}
					$elmBody .= "<div class='elm_" . $elm["type"] . "'><div class='presetVal'>" . $value . "</div></div>";
				} else {
					if ($NotFromTable) {
						if (isset($data["checkbox-group"][$id])) {
							$FieldValue = $data["checkbox-group"][$id]["arrFieldValue"];
						}
					} else {
						if (isset($data["checkbox-group"][$id][$elm["tableRowIndex"]])) {
							$FieldValue = $data["checkbox-group"][$id][$elm["tableRowIndex"]]["arrFieldValue"];
						}
					}
					$outValue = "";
					foreach ($elm["values"] as $kk => $vv) {
						$selected = "";
						if (isset($FieldValue)) {
							$aleady_checked = false;
							foreach ($FieldValue as $valIndex => $valData) {
								if (!is_array($valData) && $valData == $vv["value"] && !$aleady_checked) {
									$aleady_checked = true;
									$selected = " checked";
									if (!empty($outValue)) $outValue .= ", ";
									$outValue .= intranet_undo_htmlspecialchars($vv["label"]);
								}
							}
						} else {
							if ($vv["selected"]) {
								$selected = " checked";
							}
						}
						if (!$elm["isDisplayMode"]) {
							$elmBody .= "<label><input type='checkbox' name='" . $elmID . "' value=\"" . intranet_htmlspecialchars($vv["value"]) . "\"" . $selected . $required . "> " . $vv["label"] . "</label>";
							$elmBody .= "<br>";
						}
					}
					if ($elm["other"]) {
						$otherKey = count($FieldValue) - 2;
						$otherValKey = count($FieldValue) - 1;
						if ($elm["isDisplayMode"]) {
							if (isset($FieldValue[$otherKey]) && $FieldValue[$otherKey] == "other" && !empty($FieldValue[$otherValKey]["other"])) {
								if (!empty($outValue)) $outValue .= ", ";
								$outValue .= $Lang["eForm"]["other"] . ": " . $FieldValue[$otherValKey]["other"];
							}
							if (empty($outValue)) $outValue = "-";
							$elmBody .= "<div class='userdata'>" . $outValue ."</div>";
						} else {
							$selected = "";
							if (isset($FieldValue[$otherKey]) && $FieldValue[$otherKey] == "other" && !empty($FieldValue[$otherValKey]["other"])) {
								$selected = " checked";
							}
							$elmBody .= "<label><input type='checkbox' id='" . $ass_id . "' name='" . $elmID . "' value='other'" . $selected . $required . "> " . $Lang["eForm"]["other"];
							$elmBody .= "<input type='text' id='" . $ass_id . "-other' name='" . $elmID. "[other]' value=\"" . intranet_htmlspecialchars($FieldValue[$otherValKey]["other"]) . "\" class='input-field-other input-field'>";
							if ($this->isDebug) {
								$elmBody .= " ( <span class='text-red'>" . $elmID. "[other] </span> )";
							}
							$elmBody .= "</label>";
						}
					} else {
						if ($elm["isDisplayMode"]) {
							if (empty($outValue)) $outValue = "-";
							$elmBody .= "<div class='userdata'>" . $outValue . "</div>";
						}
					}
				}
			}
			$elmBody .= "</div>";
			return $elmBody;
		}

		public function formElm_radio($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			global $Lang;

			$elmBody = "";
			$id = $elm["custom_name"];
			$elmID = $elm["custom_name"];
			if (isset($elm["required"]) && $elm["required"] && !$elm["isDisplayMode"]) {
				$required = " required";
				$label_required = " elmRequired";
			}
			if ($NotFromTable) {
				$elmBody .= "<label class='elmLabel" . $label_required . "'>" . $elm["label"] . "</label>";
			} else {
				$elmID .= "[" . $elm["tableRowIndex"] . "]";
			}
			$elmID .= "[]";
			$ass_id = str_replace(array("[", "]"), "-", $elmID);
			if ($this->isDebug) {
				$elmBody .= " ( <span class='text-red'>" . $elmID. "</span> )";
			}
			$elmBody .= "<div class='elm_" . $elm["type"] . "'>";
			if (count($elm["values"]) > 0) {
				if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
					foreach ($elm["values"] as $kk => $vv) {
						if ($vv["value"] == $elm["presetValue"]) {
							$value = $vv["label"];
						}
					}
					$elmBody .= "<div class='elm_" . $elm["type"] . "'><div class='presetVal'>" . $value . "</div></div>";
				} else {
					if ($NotFromTable) {
						if (isset($data["radio-group"][$id])) {
							$FieldValue = $data["radio-group"][$id]["arrFieldValue"];
						}
					} else {
						if (isset($data["radio-group"][$id][$elm["tableRowIndex"]])) {
							$FieldValue = $data["radio-group"][$id][$elm["tableRowIndex"]]["arrFieldValue"];
						}
					}
					$outValue = "";
					$already_selected = false;
					foreach ($elm["values"] as $kk => $vv) {
						$selected = "";
						if (isset($FieldValue)) {
							foreach ($FieldValue as $valIndex => $valData) {
								if (!is_array($valData) && $valData == $vv["value"] && !$already_selected) {
									$selected = " checked";
									$already_selected = true;
									if (!empty($outValue)) $outValue .= ", ";
									$outValue .= $vv["label"];
								}
							}
						} else {
							if ($vv["selected"]) {
								$selected = " checked";
							}
						}
						if (!$elm["isDisplayMode"]) {
							$elmBody .= "<label><input type='radio' name='" . $elmID . "' value=\"" . intranet_htmlspecialchars($vv["value"]) . "\"" . $selected . $required . "> " . $vv["label"] . "</label>";
							$elmBody .= "<br>";
						}
					}
					if ($elm["other"]) {
						$otherKey = count($FieldValue) - 2;
						$otherValKey = count($FieldValue) - 1;
						if ($elm["isDisplayMode"]) {
							if (isset($FieldValue[$otherKey]) && $FieldValue[$otherKey] == "other" && !empty($FieldValue[$otherValKey]["other"])) {
								if (!empty($outValue)) $outValue .= ", ";
								$outValue .= $Lang["eForm"]["other"] . ": " . $FieldValue[$otherValKey]["other"];
							}
							if (empty($outValue)) $outValue = "-";
							$elmBody .= "<div class='userdata'>" . $outValue . "</div>";
						} else {
							$selected = "";
							if (isset($FieldValue[$otherKey]) && $FieldValue[$otherKey] == "other" && !empty($FieldValue[$otherValKey]["other"])) {
								$selected = " checked";
							}
							$elmBody .= "<label><input type='radio' id='" . $ass_id . "' name='" . $elmID . "' value='other'" . $selected . $required . "> " . $Lang["eForm"]["other"];
							$elmBody .= "<input type='text' id='" . $ass_id . "-other' name='" . $elmID. "[other]' value=\"" . intranet_htmlspecialchars($FieldValue[$otherValKey]["other"]) . "\" class='input-field-other input-field'>";
							if ($this->isDebug) {
								$elmBody .= " ( <span class='text-red'>" . $elmID. "[other] </span> )";
							}
							$elmBody .= "</label>";
						}
					} else {
						if ($elm["isDisplayMode"]) {
							if (empty($outValue)) $outValue = "-";
							$elmBody .= "<div class='userdata'>" . $outValue . "</div>";
						}
					}
				}
			}
			$elmBody .= "</div>";
			return $elmBody;
		}

		public function formElm_select($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			global $Lang, $indexVar;
			$elmBody = "";
			$id = $elm["custom_name"];
			$elmID = $elm["custom_name"];
			if (isset($elm["required"]) && $elm["required"] && !$elm["isDisplayMode"]) {
				$required = " required";
				$label_required = " elmRequired";
			}
			if ($NotFromTable) {
				$elmBody .= "<label class='elmLabel" . $label_required . "'>" . $elm["label"] . "</label>";
			} else {
				$elmID .= "[" . $elm["tableRowIndex"] . "]";
			}
			$elmID .= "[]";
			if ($this->isDebug) {
				$elmBody .= " ( <span class='text-red'>" . $elmID . "</span> )";
			}
			$elmBody .= "<div class='elm_" . $elm["type"] . "'>";
			if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
				foreach ($elm["values"] as $kk => $vv) {
					if ($vv["value"] == $elm["presetValue"]) {
						$value = $vv["label"];
					}
				}
				$elmBody .= "<div class='elm_" . $elm["type"] . "'><div class='presetVal'>" . $value . "</div></div>";
			} else {
				if (count($elm["values"]) > 0) {
					if ($NotFromTable) {
						if (isset($data["select"][$id])) {
							$FieldValue = $data["select"][$id]["arrFieldValue"];
						}
					} else {
						if (isset($data["select"][$id][$elm["tableRowIndex"]])) {
							$FieldValue = $data["select"][$id][$elm["tableRowIndex"]]["arrFieldValue"];
						}
					}
					if (!$elm["isDisplayMode"]) {
						if ($elm["multiple"]) {
							$elmBody .= "<select name='" . $elmID . "' multiple class='isMultiple select-field'" . $required . ">";
						} else {
							$elmBody .= "<select name='" . $elmID . "' class='select-field'" . $required . ">";
						}
					}
					$outValue = "";

					foreach ($elm["values"] as $kk => $vv) {
						$selected = "";
						if (isset($FieldValue)) {
							$already_selected = false;
							$aleardy_assigned = false;
							foreach ($FieldValue as $valIndex => $valData) {
								if (!is_array($valData) && $valData == $vv["value"] && !$already_selected) {
									if (!$elm["multiple"]) {
										$already_selected = true;
									}
									$selected = " selected";
									if (!$aleardy_assigned) {
										$aleardy_assigned = true;
										if (!empty($outValue)) $outValue .= ", ";
										$outValue .= $vv["label"];
									}
								}
							}
						} else {
							if ($vv["selected"]) {
								$selected = " selected";
							}
						}
						if (!$elm["isDisplayMode"]) {
							$elmBody .= "<option value=\"" . intranet_htmlspecialchars($vv["value"]) . "\"" . $selected . "> " . $vv["label"] . "</option>";
						}
					}
					if ($elm["isDisplayMode"]) {
						$elmBody .= "<div class='userdata'>" . $outValue . "</div>";
					} else {
						$elmBody .= "</select>";
					}
				}
			}
			$elmBody .= "</div>";
			return $elmBody;
		}

		public function formElm_dateFieldElm($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			global $Lang;
			include_once(dirname(dirname(__FILE__)) . "/libinterface.php");
			$interface = new interface_html();
			$elmBody = "";
			$id = $elm["custom_name"];
			$elmID = $elm["custom_name"];
			if (!$NotFromTable) {
				$elmID .= "[" . $elm["tableRowIndex"] . "]";
			}

			$ass_id = str_replace(array("[", "]"), "-", $elmID);
			if (isset($elm["required"]) && $elm["required"] && !$elm["isDisplayMode"]) {
				$required = " required";
				$label_required = " elmRequired";
			}
			if ($NotFromTable) {
				$elmBody .= "<label class='elmLabel" . $label_required . "'>" . $elm["label"] . "</label>";
			}
			if ($this->isDebug) {
				$elmBody .= " ( <span class='text-red'>" . $elmID . "</span> )";
			}
			$elmBody .= "<div class='elm_" . $elm["type"] . "'>";
			$readonly = "";
			if ($NotFromTable) {
				if (isset($data["dateFieldElm"][$id])) {
					$FieldValue = $data["dateFieldElm"][$id]["FieldValue"];
				}
			} else {
				if (isset($data["dateFieldElm"][$id][$elm["tableRowIndex"]])) {
					$FieldValue = $data["dateFieldElm"][$id][$elm["tableRowIndex"]]["FieldValue"];
				}
			}
			if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
				if (!empty($elm["presetValue"])) {
					$value = date("Y-m-d", strtotime($elm["presetValue"]));
				}
				// $elmBody .= "<input type='text' class='input-field' " . $required. " name='" . $elmID . "' value='" . $value . "'" . $readonly;
				/*
				 $readonly = " readonly style='height:auto;'";
				 $elmBody .= "<div class='elm_" . $elm["type"] . "'><textarea class='textarea-field' name='" . $elmID . "'";
				 $elmBody .= $readonly .">" . $value . "</textarea></div>";
				 */
				$elmBody .= "<div class='elm_" . $elm["type"] . "'><div class='presetVal'>" . $value . "</div></div>";
			} else {
				// $elmBody .= $interface->GET_DATE_PICKER($name = $elmID, $DefaultValue="",$OtherMember="",$DateFormat="yy-mm-dd", $MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="", $OnDatePickSelectedFunction="", $ass_id, $SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="input-field");
				if ($elm["isDisplayMode"]) {
					if (empty($FieldValue)) $FieldValue = "-";
					$elmBody .= "<div class='userdata'>" . $FieldValue . "</div>";
				} else {
					$elmBody .= "<input type='text' data-toggle='datepicker' class='input-field' " . $required. " name='" . $elmID . "' value='" . $FieldValue . "'" . $readonly . $maxlength;
					$elmBody .= " />";
				}
			}
			$elmBody .= "</div>";
			return $elmBody;
		}

		public function formElm_timeFieldElm($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true)
		{
		    global $Lang;
		    include_once(dirname(dirname(__FILE__)) . "/libinterface.php");
		    $interface = new interface_html();
		    $elmBody = "";
		    $id = $elm["custom_name"];
		    $elmID = $elm["custom_name"];
		    if (!$NotFromTable) {
		        $elmID .= "[" . $elm["tableRowIndex"] . "]";
		    }

		    $ass_id = str_replace(array("[", "]"), "-", $elmID);
		    if (isset($elm["required"]) && $elm["required"] && !$elm["isDisplayMode"]) {
		        $required = " required";
		        $label_required = " elmRequired";
		    }
		    if ($NotFromTable) {
		        $elmBody .= "<label class='elmLabel" . $label_required . "'>" . $elm["label"] . "</label>";
		    }
		    if ($this->isDebug) {
		        $elmBody .= " ( <span class='text-red'>" . $elmID . "</span> )";
		    }
		    $elmBody .= "<div class='elm_" . $elm["type"] . "'>";
		    $readonly = "";
		    if ($NotFromTable) {
		        if (isset($data["timeFieldElm"][$id])) {
		            $FieldValue = $data["timeFieldElm"][$id]["FieldValue"];
		        }
		    } else {
		        if (isset($data["timeFieldElm"][$id][$elm["tableRowIndex"]])) {
		            $FieldValue = $data["timeFieldElm"][$id][$elm["tableRowIndex"]]["FieldValue"];
		        }
		    }
		    if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
		        if (!empty($elm["presetValue"])) {
		            $value = $elm["presetValue"];
		        }
		        // $elmBody .= "<input type='text' class='input-field' " . $required. " name='" . $elmID . "' value='" . $value . "'" . $readonly;
		        /*
		         $readonly = " readonly style='height:auto;'";
		         $elmBody .= "<div class='elm_" . $elm["type"] . "'><textarea class='textarea-field' name='" . $elmID . "'";
		         $elmBody .= $readonly .">" . $value . "</textarea></div>";
		         */
		        $elmBody .= "<div class='elm_" . $elm["type"] . "'><div class='presetVal'>" . $value . "</div></div>";
		    } else {
		        // $elmBody .= $interface->GET_DATE_PICKER($name = $elmID, $DefaultValue="",$OtherMember="",$DateFormat="yy-mm-dd", $MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="", $OnDatePickSelectedFunction="", $ass_id, $SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="input-field");
		        if ($elm["isDisplayMode"]) {
		            if (empty($FieldValue)) $FieldValue = "-";
		            $elmBody .= "<div class='userdata'>" . $FieldValue . "</div>";
		        } else {
		            $elmBody .= "<input type='text' data-toggle='timepicker' class='input-field input-time' " . $required. " name='" . $elmID . "' value='" . $FieldValue . "'" . $readonly . $maxlength;
		            $elmBody .= " />";
		        }
		    }
		    $elmBody .= "</div>";
		    return $elmBody;
		}

		public function formElm_dualSelectElm($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true)
		{
		    global $Lang, $indexVar;
		    include_once(dirname(dirname(__FILE__)) . "/libinterface.php");
		    $interface = new interface_html();
		    $elmBody = "";
		    $id = $elm["custom_name"];
		    $elmID = $elm["custom_name"];
		    if (!$NotFromTable) {
		        $elmID .= "[" . $elm["tableRowIndex"] . "]";
		    }

		    $ass_id = str_replace(array("[", "]"), "-", $elmID);
		    if (isset($elm["required"]) && $elm["required"] && !$elm["isDisplayMode"]) {
		        $required = " required";
		        $label_required = " elmRequired";
		    }
		    if ($NotFromTable) {
		        $elmBody .= "<label class='elmLabel" . $label_required . "'>" . trim($elm["label"]) . "</label>";
		    }
		    if ($this->isDebug) {
		        $elmBody .= " ( <span class='text-red'>" . $elmID . "</span> )";
		    }
		    $elmBody .= "<div class='elm_" . $elm["type"] . "'>";
		    $readonly = "";

		    $meaningFieldValue = "";
		    if ($NotFromTable) {
		        if (isset($data[$elm["type"]][$id])) {
		            $FieldValue = $data[$elm["type"]][$id]["FieldValue"];
		            $meaningFieldValue = $data[$elm["type"]][$id]["meaningFieldValue"];
		        }
		    } else {
		        if (isset($data[$elm["type"]][$id][$elm["tableRowIndex"]])) {
		            $FieldValue = $data[$elm["type"]][$id][$elm["tableRowIndex"]]["FieldValue"];
		            $meaningFieldValue = $data[$elm["type"]][$id][$elm["tableRowIndex"]]["meaningFieldValue"];
		        }
		    }
		    if (isset($elm["presetValue"]) && !empty($elm["presetValue"])) {
		        if (!empty($elm["presetValue"])) {
		        }
		        $elmBody .= "<div class='elm_" . $elm["type"] . "'><div class='presetVal'>" . $value . "</div></div>";
		    } else {
		        $task = $indexVar['task'];
		        $taskArr = explode("/", $task);
		        if (count($taskArr) > 0) {
		            array_pop($taskArr);
		            $task = implode("/", $taskArr);
		        }
		        $panelURL = "?task=" . $task . "/panel";
		        $panelURL .= "&yid=" . (isset($meaningFieldValue["YearInfo"]["AcademicYearID"]) ? $meaningFieldValue["YearInfo"]["AcademicYearID"] : $elm["AcademicYearID"]);
		        $panelURL .= "&tid=" . (isset($meaningFieldValue["TermInfo"]["YearTermID"]) ? $meaningFieldValue["TermInfo"]["YearTermID"] : $elm["YearTermID"]);
		        if (isset($meaningFieldValue["opt"])) {
		            $panelURL .= "&opt=" . $meaningFieldValue["opt"];
		        }
		        $panelURL .= "&elm=dselect_" . $elmID;
		        $panelURL .= "&type=" . $elm["type"];
		        $panelURL .= "&subtype=" . $elm["subtype"] . "";
		        if (isset($elm["max"]) && $elm["max"] > 0 && $elm["subtype"] == "multiple") {
		            $panelURL .= "&maxopt=" . $elm["max"] . "";
		        }
		        if (!empty($meaningFieldValue)) {
		            $displayLabel = "";
		            if (count($meaningFieldValue["dataArr"]) > 0) {
		                if (in_array($elm["type"], array("studentElm", "subjectGroupElm"))) {
		                    $displayLabel .= "<div class='yeartag'><i class='fa fa-calendar-minus-o'></i> " . Get_Lang_Selection((empty($meaningFieldValue["YearInfo"]["YearNameB5"]) ? $meaningFieldValue["YearInfo"]["YearNameEN"] : $meaningFieldValue["YearInfo"]["YearNameB5"]), $meaningFieldValue["YearInfo"]["YearNameEN"]) . "</div>";
		                }
		                foreach ($meaningFieldValue["dataArr"] as $kk => $val) {
		                    switch ($elm["type"]) {
		                        case "studentElm" :
		                            if ($elm["isDisplayMode"]) {
		                                $label = "<i class='fa fa-user'></i> ";
		                            } else {
		                                $label = "";
		                            }
		                            $label .= ""  . Get_Lang_Selection((empty($val["ClassTitleB5"]) ? $val["ClassTitleEN"] : $val["ClassTitleB5"]), $val["ClassTitleEN"]) . " (" . $val['ClassNumber'] . ") - ";
		                            $label .= Get_Lang_Selection((empty($val["ChineseName"]) ? $val["EnglishName"] : $val["ChineseName"]), $val["EnglishName"]);
		                            break;
		                        case "teacherElm" :
		                            if ($elm["isDisplayMode"]) {
		                                $label = "<i class='fa fa-user'></i> ";
		                            } else {
		                                $label = "";
		                            }
		                            $label .= Get_Lang_Selection((empty($val["ChineseName"]) ? $val["EnglishName"] : $val["ChineseName"]), $val["EnglishName"]);
		                            break;
		                        case "subjectGroupElm" :
		                            if ($elm["isDisplayMode"]) {
		                                $label = "<i class='fa fa-group'></i> ";
		                            } else {
		                                $label = "";
		                            }
		                            $label .= Get_Lang_Selection((empty($val["ClassTitleB5"]) ? $val["ClassTitleEN"] : $val["ClassTitleB5"]), $val["ClassTitleEN"]);
		                            break;
		                    }
		                    $label = trim(str_replace("<br>", "", $label));
		                    if ($elm["isDisplayMode"]) {
		                        $displayLabel .= "<span class='tags' rel='" . $kk . "'><nobr>" . $label . "</nobr></span>";
		                    } else {
		                        $displayLabel .= "<a href='#' class='tags' rel='" . $kk . "'><nobr><i class='fa fa-close'></i> " . $label . "</nobr></a>";
		                    }
		                }
		            }
		        }
		        if ($elm["isDisplayMode"]) {
		            if (empty($displayLabel)) $displayLabel= "-";

		            $elmBody .= "<div class='dualselect'><div class='userdata'>" . $displayLabel . "<div class='clearboth'></div></div></div>";
		        } else {
		            $elmBody .= "<div class='dualselect userdata' id='dselect_" . $id . "' title=' " . str_replace("'", "&#39;", trim($elm['label'])) . " '><input type='hidden' " . $required. " name='" . $elmID . "' value='" . $FieldValue . "'><div class='tagsbox' id='" . $elmID . "_data'>" . $displayLabel . "</div>";
		            $elmBody .= "<div class='clearboth'></div><a href='#' class='btn' data-type='iframe' data-src='" . $panelURL. "' data-fancybox>" . $Lang["eForm"]["open_select_panel"] . "</a>";
		            $elmBody .= "</div>";
		        }
		    }
		    $elmBody .= "</div>";
		    return $elmBody;
		}

		public function formElm_hidden($tableInfo = array(), $elm = array(), $data = array(), $NotFromTable = true) {
			global $Lang;
			$id = $elm["custom_name"];
			$elmID = $elm["custom_name"];
			if ($tableInfo["isDisplayMode"]) {
				$elmBody .= "<div class='elm'>";
				$elmBody .= "	<div class='elm-body'>";
				$elmBody .= "		<label class='elmLabel" . $label_required . "'>" . $elm["label"] . "</label>";
				$elmBody .= "		<div class='userdata'>-</div>";
				$elmBody .= "	</div>";
				$elmBody .= "</div>";
			} else {
				$elmBody = "<input type='hidden' name='" . $elmID . "' value=\"" . intranet_htmlspecialchars($elm["value"]) . "\">";
			}
			if ($this->isDebug) {
				$elmBody .= "<div class='elm'>";
				$elmBody .= "<div class='elm-body'>Hidden Value here ( <span class='text-red'>" . $id . "</span> )</div>";
				$elmBody . "</div>";
			}
			return $elmBody;
		}

		public function getLoadingElm() {
			global $Lang;
			$strHTML = '<div class="loadingContainer" style="display:none">';
			/*
			$strHTML .= '	<div class="loader">';
			$strHTML .= '		<span class="dot dot_1"></span>';
			$strHTML .= '		<span class="dot dot_2"></span>';
			$strHTML .= '		<span class="dot dot_3"></span>';
			$strHTML .= '		<span class="dot dot_4"></span>';
			$strHTML .= '	</div>';
			*/
			$strHTML .= '	<div class="loadingtext">... ' . $Lang["eForm"]["DataProcessing"] . ' ...</div>';
			$strHTML .= '</div><br>';
			return $strHTML;
		}

		public function getSuppHTMLBody($suppInfo) {
			global $Lang;
			$strHTML = "<div class='suppTitle'>";
			$strHTML .= $Lang["eForm"]["SUPPLEMENTARY_REQUEST"];
			$strHTML .= "</div><br>";
			$strHTML .= "<div class='SuppFormBody'>";
			$strHTML .= "<div class='SupFormLabel'>" . $Lang["eForm"]["SUPPLEMENTARY_REQUEST_Remark"] . ":</div>";
			$strHTML .= "<div class='SupFormInfo'>" . $suppInfo["SuppMsg"] . "</div>";
			$strHTML .= "</div>";
			return $strHTML;
		}

		public function getPromptHTML($data) {
			global $Lang, $LAYOUT_SKIN;
			if (count($data) > 0) {
				$strHTML .= "<div id=\"eForm_PromptWindow\" style='display:none;'><br>";
				$strHTML .= "	<table cellpadding=0 cellspacing=0 width=\"100%\" style=\"padding-left: 5px; padding-right: 5px; padding-bottom: 5px;\">";
				$strHTML .= "	<thead>";
				$strHTML .= "	<tr>";
				$strHTML .= "		<th style=\"padding:10px; background-color:#8f8f8f; color:#fff; -webkit-border-top-left-radius: 5px; -moz-border-radius-topleft: 5px; border-top-left-radius: 5px;\">" . $Lang["eForm"]["th_FormTitle"] . "</th>";
				$strHTML .= "		<th style=\"padding:10px; background-color:#8f8f8f; color:#fff; -webkit-border-top-right-radius: 5px; -moz-border-radius-topright: 5px; border-top-right-radius: 5px; width:15%; \">" . $Lang["eForm"]["th_DueDate"] . "</th>";
				$strHTML .= "	<tr>";
				$strHTML .= "	</thead>";

				$now = time(); // or your date as well
				$strHTML .= "	<tbody>";
				foreach ($data as $kk => $vv) {
					$your_date = strtotime($vv["DueDate"]);
					$datediff = $now - $your_date;
					$NoOfDate = floor($datediff / (60 * 60 * 24));
					$url = "/home/eService/eForm/?task=forminfo/editform&fid=" . $vv["FormID"] . "&gid=" . $vv["GridID"] . "&ctk=" . md5($vv["FormID"] . "_" . $vv["GridID"] ."-eForm-" . $_SESSION["UserID"]);
					if ($NoOfDate > 3) {
						$status = "<td style=\"border-bottom:1px solid #efefef; background-color:#e14040;\"><a href=\"" . $url . "\" style=\"display:block; line-height:25px; padding:4px; padding-left:10px; padding-right:10px; color:#fff;\">%s</a></td>";
					} else if ($NoOfDate == 3) {
						$status= "<td style=\"border-bottom:1px solid #efefef; background-color:#ffae45;\"><a href=\"" . $url . "\" style=\"display:block; line-height:25px; padding:4px; padding-left:10px; padding-right:10px; color:#000;\">%s</a></td>";
					} else if ($NoOfDate == 2) {
						$status = "<td style=\"border-bottom:1px solid #efefef; background-color:#f1de23;\"><a href=\"" . $url . "\" style=\"display:block; line-height:25px; padding:4px; padding-left:10px; padding-right:10px; color:#000;\">%s</a></td>";
					} else if ($NoOfDate == 1) {
						$status = "<td style=\"border-bottom:1px solid #efefef; background-color:#32b123;\"><a href=\"" . $url . "\" style=\"display:block; line-height:25px; padding:4px; padding-left:10px; padding-right:10px; color:#fff;\">%s</a></td>";
					} else {
						$status = "<td style=\"border-bottom:1px solid #efefef; background-color:#fff;\"><a href=\"" . $url . "\" style=\"display:block; line-height:25px; padding:4px; padding-left:10px; padding-right:10px; color:#000;\">%s</a></td>";
					}
					$strHTML .= "	<tr>";
					$strHTML .= "		" . sprintf($status, "<img src=\"/images/".$LAYOUT_SKIN."/icon_edit_b.gif\" border=0 align=absmiddle> " . $vv["FormTitle"]) . "";
					$strHTML .= "		" . sprintf($status, $vv["DueDate"]) . "";
					$strHTML .= "	<tr>";
				}
				$strHTML .= "	</tbody>";
				$strHTML .= "	</table>";
				$strHTML .= "</div>";
			}
			return $strHTML;
		}


		public function getBreadcrumbHTML($breadcrumbArr = array())
		{
		    $str_breadcrumb = '';
		    if (count($breadcrumbArr) > 0)
		    {
		        foreach ($breadcrumbArr as $kk => $val)
		        {
		            if ($kk == 0)
		            {
		                $str_breadcrumb .= '<a href="?task=management/form_management/list"><i class="fa fa-home"></i> ' . $val["title"] . "</a>";
		            }
		            else
		            {
		                if (!empty($str_breadcrumb)) $str_breadcrumb .= ' <i class="fa fa-caret-right"></i> ';
		                if ($val["selected"]) {
		                    $str_breadcrumb .= '<a href="?task=management/form_management/list&folder=' . $val["id"] . '" class="selected">' . $val["title"] . "</a>";
		                } else {
		                    $str_breadcrumb .= '<a href="?task=management/form_management/list&folder=' . $val["id"] . '">' . $val["title"] . "</a>";
		                }
		            }

		        }
		    }
		    return $str_breadcrumb;
		}

		public function treeMenu($slug = null)
		{
		    global $Lang;
		    $is_debug = true;
		    $tree = '<script src="/templates/eForm/assets/ftree/js/jquery.js"></script>' . "\n";
		    $tree .= '<script src="/templates/eForm/assets/ftree/js/jquery-ui.custom.js"></script>' . "\n";
		    $tree .= '<link href="/templates/eForm/assets/ftree/skin-win8/ui.fancytree.css" rel="stylesheet">' . "\n";
		    $tree .= '<script src="/templates/eForm/assets/ftree/js/jquery.fancytree.min.js"></script>' . "\n";

		    $tree .= '<script src="/templates/eForm/assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>' . "\n";
		    $tree .= '<script src="/templates/eForm/assets/bootstrap-3.3.7/js/popper.min.js"></script>' . "\n";

		    $tree .= '<div id="tree" title="' . $Lang["eForm"]["Folders"]["tip_select_folder"] . '" data-placement="right" class="" aria-label="' . $Lang["eForm"]["Folders"]["tip_select_folder"] . '" >';
            $tree .= '</div>';

            $tools = '<a href="#" class="eform_expandAll btn pri"><i class="fa fa-folder-open-o"></i> ' . $Lang["eForm"]["Folders"]["expandAll"] . '</a>';
            $tools .= '<a href="#" class="eform_hiddenAll btn sec"><i class="fa fa-folder"></i> ' . $Lang["eForm"]["Folders"]["hiddenAll"] . '</a>';

            if (!empty($tools)) {
                $tree .= '<div class="treetools set black">';
                $tree .= $tools;
                $tree .= '</div>';
                if ($is_debug)
                {
//                     $tree .= '<div>Active node: <span id="echoActive">-</span></div>';
//                     $tree .= '<div id="selectedhdr">' . $Lang["eForm"]["Folders"]["selectedFolder"]. ':<br><span id="item_title">-</span></div>';
//                     $tree .= '<div>Focused node: <span id="echoFocused">-</span></div>';
                }
            }

		    return $tree;
		}


	}
}