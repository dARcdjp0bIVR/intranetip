<?php
// editing by  
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
 * 
 * Date : 2017-05-31 Frankie
********************/
if (!defined("LIBEFORM_DEFINED")) {
	define("LIBEFORM_DEFINED", true);

	class libeform extends libdb {
		var $ModuleTitle;
		
		function libeform() {
		    global $eFormConfig, $intranet_session_language;
			$this->libdb();
			$this->ModuleTitle = $eFormConfig['moduleCode'] . "";
			
			if ($junior_mck) {
			    // EJ User Table
			    $this->userTable = "INTRANET_USER_UTF8";
			}
			else
			{
			    // IP User Type
			    $this->userTable = "INTRANET_USER";
			}
			$this->userGroupTable = "INTRANET_USERGROUP";
			$this->groupTable = "INTRANET_GROUP";
			
			$this->currLang = ($intranet_session_language == "en") ? "en":"b5";
		}
		
		function getModuleObjArr() {
			global $PATH_WRT_ROOT, $intranet_root, $image_path, $LAYOUT_SKIN, $CurrentPage;
			global $plugin, $special_feature, $eFormConfig, $indexVar;
			global $intranet_session_language, $Lang;

			$currentPageAry = explode($eFormConfig['taskSeparator'], $CurrentPage);
			$pageFunction = $currentPageAry[0];
			$pageMenu = $currentPageAry[0].$eFormConfig['taskSeparator'].$currentPageAry[1];
			
			/*
			# eForm
			$MenuArr["eForm"] = array($Lang['eForm']['Menu']['eForm'], "#", ($pageFunction=='eForm'));
			
			$menuTask = 'form'.$eFormConfig['taskSeparator'].'information'.$eFormConfig['taskSeparator'].'list';
			$MenuArr["eForm"]["Child"]["FormInfo"] = array($Lang['eForm']['FormInfo'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			*/
			if (strpos($CurrentPage, 'forminfo') !== false) {
				$MODULE_OBJ['title'] = $Lang['eForm']["title"];
				/*
				$MenuArr["eForm"] = array($Lang['eForm']['Menu']['eForm'], "#", ($pageFunction=='forminfo'));
				$main_menuTask = 'forminfo'.$eFormConfig['taskSeparator'].'list';
				if (stripos($main_menuTask, $pageMenu)!==false) {
					$thisStatus = true;
				}
				$menuTask = 'forminfo'.$eFormConfig['taskSeparator'].'editform';
				if (stripos($menuTask, $pageMenu)!==false) {
					$thisStatus = true;
				}
				$MenuArr["eForm"]["Child"]["FormInfo"] = array($Lang['eForm']['FormInfo'], '?task='.$main_menuTask, $thisStatus);
				
				$thisStatus = false;
				$main_menuTask = 'forminfo'.$eFormConfig['taskSeparator'].'submittedlist';
				if (stripos($main_menuTask, $pageMenu)!==false) {
					$thisStatus = true;
				}
				$menuTask = 'forminfo'.$eFormConfig['taskSeparator'].'viewform';
				if (stripos($menuTask, $pageMenu)!==false) {
					$thisStatus = true;
				}
				$MenuArr["eForm"]["Child"]["SubmittedRecord"] = array($Lang['eForm']['SubmittedRecord'], '?task='.$main_menuTask, $thisStatus);
				*/
			} else {
				
				$MODULE_OBJ['title'] = $Lang['eForm']["title"];
				
				/*
			    # Management 
				$MenuArr["Management"] = array($Lang['eForm']['Menu']['Management'], "#", ($pageFunction=='management'));
				
				$menuTask = 'management'.$eFormConfig['taskSeparator'].'form_management'.$eFormConfig['taskSeparator'].'list';
				$MenuArr["Management"]["Child"]["FormManagement"] = array($Lang['eForm']['FormManagement'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
	
				## $menuTask = 'settings'.$eFormConfig['taskSeparator'].'basic_settings'.$eFormConfig['taskSeparator'].'list';
				## $MenuArr["Settings"]["Child"]["BasicSettings"] = array($Lang['eForm']['BasicSettings'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
				$menuTask = 'settings'.$eFormConfig['taskSeparator'].'table_template'.$eFormConfig['taskSeparator'].'list';
				$MenuArr["Management"]["Child"]["TableTemplate"] = array($Lang['eForm']['TableTemplate'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
				
				### module information
				$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT.$eFormConfig['eAdminPath'];
				$MODULE_OBJ['title'] = $Lang['eForm']["title"];
				$MODULE_OBJ['title_css'] = "menu_opened";
				$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_doc_routing.png";
				$MODULE_OBJ['menu'] = $MenuArr;
				*/
			}
	        
	        return $MODULE_OBJ;
		}
		function geteServiceTagObj() {
			global $Lang, $eFormConfig, $CurrentPage;
			
			if (strpos($CurrentPage, 'forminfo') !== false) {
				$currentPageAry = explode($eFormConfig['taskSeparator'], $CurrentPage);
				$pageFunction = $currentPageAry[0];
				$pageMenu = $currentPageAry[0].$eFormConfig['taskSeparator'].$currentPageAry[1];
				
				$main_menuTask = 'forminfo'.$eFormConfig['taskSeparator'].'list';
				if (stripos($main_menuTask, $pageMenu)!==false) {
					$thisStatus = true;
				}
				$menuTask = 'forminfo'.$eFormConfig['taskSeparator'].'editform';
				if (stripos($menuTask, $pageMenu)!==false) {
					$thisStatus = true;
				}
				$TAGS_OBJ[] = array($Lang['eForm']['FormInfo'], '?task='.$main_menuTask, $thisStatus);
				
				$thisStatus = false;
				$main_menuTask = 'forminfo'.$eFormConfig['taskSeparator'].'submittedlist';
				if (stripos($main_menuTask, $pageMenu)!==false) {
					$thisStatus = true;
				}
				$menuTask = 'forminfo'.$eFormConfig['taskSeparator'].'viewform';
				if (stripos($menuTask, $pageMenu)!==false) {
					$thisStatus = true;
				}
				$TAGS_OBJ[] = array($Lang['eForm']['SubmittedRecord'], '?task='.$main_menuTask, $thisStatus);
			} else {
				$currentPageAry = explode($eFormConfig['taskSeparator'], $CurrentPage);
				$pageFunction = $currentPageAry[0];
				$pageMenu = $currentPageAry[0].$eFormConfig['taskSeparator'].$currentPageAry[1];
				
				$menuTask = 'management'.$eFormConfig['taskSeparator'].'form_management'.$eFormConfig['taskSeparator'].'list';
				$TAGS_OBJ[] = array($Lang['eForm']['FormManagement'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
				
				$menuTask = 'settings'.$eFormConfig['taskSeparator'].'table_template'.$eFormConfig['taskSeparator'].'list';
				$TAGS_OBJ[]= array($Lang['eForm']['TableTemplate'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			}
			return $TAGS_OBJ;
		}
		function checkAccessRight($fromEAdmin = false) {
			global $plugin;
			
			$canAccess = true;
			if (!$plugin['eForm']) {
// 			if (!$plugin['eForm'] ) {
				$canAccess = false;
			} else {
				if ($fromEAdmin) {
					if (!$this->isAdminUser()) {
						$canAccess = false;
					}
				} else {
					if ($_SESSION["UserType"] != USERTYPE_STAFF) {
						$canAccess = false;
					}
				}
			}
			if (!$canAccess) {
				No_Access_Right_Pop_Up();
			}
		}		
		function isAdminUser() {
			return $_SESSION["SSV_USER_ACCESS"]["eAdmin-eForm"]? true : false;
		}
		function isEJ(){
			global $junior_mck;
			return $junior_mck;
		}
		
		function setDefaultSettings() {
			
		}
		
		# Split group and user target
		function splitTargetGroupUserID($target)
		{
			$GroupIDList = "";
			$UserIDsList = "";
			$group_delimiter = "";
			$user_delimiter = "";
			$row = explode(",",$target);
			for($i=0; $i<sizeof($row); $i++)
			{
				$targetType = substr($row[$i],0,1);
				if(is_numeric($row[$i])) {
					$targetType = "U";
					$targetID = $row[$i];
				} else {
					$targetID = substr($row[$i],1);
				}
				if ($targetType=="G")
				{
					$GroupIDList .= $group_delimiter.$targetID;
					$group_delimiter = ",";
				}
				else
				{
					$UserIDsList .= $user_delimiter.$targetID;
					$user_delimiter = ",";
				}
			}
			$x[0] = ($GroupIDList == ""? 0:$GroupIDList);
			$x[1] = ($UserIDsList == ""? 0:$UserIDsList);
			return $x;
		}
		
		function returnIndividualTypeNames($targetUser)
		{
			$IDs = $this->splitTargetGroupUserID($targetUser);
			$groups = $IDs[0];
			$users = $IDs[1];
			
			$sql = "SELECT Title FROM " . $this->groupTable . " WHERE GroupID IN ($groups) ORDER BY Title";
			$result1 = $this->returnVector($sql);
			
			//                  $namefield = getNameFieldWithLoginByLang();
			$namefield = getNameFieldByLang();
			$sql = "SELECT $namefield FROM " . $this->userTable . " WHERE UserID IN ($users) ORDER BY ClassName, ClassNumber+0, ClassNumber";
			$result2 = $this->returnVector($sql);
			
			return array_merge($result1,$result2);
		}
		
		function returnTargetUserName($formInfo)
		{
			global $Lang;
			if ($formInfo["FormID"] == "") return array("","");
			
			if ($formInfo["FormRecordType"] == 1) return array($Lang["eForm"]["TargetUserAllStaff"],"");
			if ($formInfo["FormRecordType"] == 2)     # Teaching
			{
				return array($Lang["eForm"]["TargetUserAllTeaching"],"");
			}
			else if ($formInfo["FormRecordType"] == 3)              # Non-teaching
			{
				return array($Lang["eForm"]["TargetUserAllNonTeaching"],"");
			}
			else if ($formInfo["FormRecordType"] == 4)              # Individual
			{
				$targetUser = $formInfo["FormRecordType"];
				$result = $this->returnIndividualTypeNames($targetUser);
				return array($Lang["eForm"]["TargetUserIndividual"], implode(", ",$result));
			}
			return array("","");
		}
		
		
		# Find the UserID of target for type 4
		# return UserID, name , user type
		function returnTargetUserIDArray($target)
		{
			$row = $this->splitTargetGroupUserID($target);
			$GroupIDList = $row[0];
			$UserIDsList = $row[1];
			
			$username_field = getNameFieldWithClassNumberForRecord("a.");
			# Group
			
			if ($GroupIDList!="")
			{
				// revised SQL due to slow query [CRM : 2011-1012-0928-23066], by Henry Chow @ 20111014
				//$conds = "b.GroupID IN ($GroupIDList) and a.UserID = b.UserID";
				//                  	$joinTableCond = " a.UserID = b.UserID and b.GroupID IN ($GroupIDList)";
				$joinTableCond = " a.UserID = b.UserID ";
				$conds = "b.GroupID IN ($GroupIDList)";
				
			}
			else
			{
				$conds = "";
			}
			
			if ($UserIDsList!="")
			{
				if ($conds == "")
					$conds = "AND a.UserID IN ($UserIDsList)";
					else
						$conds = "AND ($conds OR a.UserID IN ($UserIDsList))";
			}
			else
			{
				if($conds!="")
					$conds = "AND $conds";
			}
			
			if ($UserIDsList=="" && $GroupIDList=="") return array();
			
			$sql = "SELECT DISTINCT a.UserID,$username_field,a.RecordType
			FROM " . $this->userTable . " as a";
			
			if($GroupIDList) {
				// revised SQL due to slow query [CRM : 2011-1012-0928-23066]
				// $sql .=", INTRANET_USERGROUP as b";
				//                  		$sql .=" INNER JOIN INTRANET_USERGROUP as b ON ($joinTableCond)";
				$sql .=" left JOIN " . $this->userGroupTable . " as b ON ($joinTableCond)";
			}
			$sql .="
                         WHERE
                         a.RecordType = '1'
                         $conds";
                         $result = $this->returnResultSet($sql);
                         return $result;
		}
		
		function returnTargetUserByType($type) {
			global $indexVar;
			$strSQL = "SELECT UserID FROM " . $indexVar["FormInfo"]->userTable . " WHERE RecordStatus='1' AND RecordType='1'";
			switch ($type) {
				case "2": ## All Teacher Staff
					$strSQL .= " AND Teaching='1'";
					break;
				case "3": ## All Non-Teacher Staff
					$strSQL .= " AND Teaching='0'";
					break;
				case "1": ## All Staff
				default: break;
			}
			$result = $this->returnVector($strSQL);
			return $result;
		}
		
		
		function getNodeInfo($folder)
		{
		    global $indexVar, $Lang;
		    $nodeInfo = array();
		    if ($folder > 0)
		    {
		        $strSQL = "SELECT
                            FolderID, FolderSlug, FolderTitleEN, FolderTitleCN, ParentID, IdTrees, Priority
                        FROM
                            " . $indexVar["FormInfo"]->formFolderNodeTable. "
                        WHERE FolderID='" . $folder . "' LIMIT 1";
		        
		        $result = $this->returnResultSet($strSQL);
		        $nodeInfo = (isset($result[0]) ? $result[0] : null);
		        if ($nodeInfo != null) {
		            $strSQL = "SELECT COUNT(FolderID) as total_folders FROM " . $indexVar["FormInfo"]->formFolderNodeTable. " WHERE ParentID='" . $nodeInfo["FolderID"] . "'";
		            $result = $this->returnResultSet($strSQL);
		            $nodeInfo["total_folders"] = $result[0]["total_folders"] ? $result[0]["total_folders"] : 0;
		            
		            $strSQL = "SELECT COUNT(FolderID) as total_form FROM " . $indexVar["FormInfo"]->formInfoTable. " WHERE FolderID='" . $nodeInfo["FolderID"] . "'";
		            $result = $this->returnResultSet($strSQL);
		            $nodeInfo["total_forms"] = $result[0]["total_form"] ? $result[0]["total_form"] : 0;
		            
		            $strSQL = "SELECT COUNT(ParentID) as total_form FROM " . $indexVar["FormInfo"]->formFolderNodeTable. "
                                 WHERE 
                                    IdTrees like '" . $nodeInfo["FolderID"] . "' or 
                                    IdTrees like '" . $nodeInfo["FolderID"] . ",%' or
                                    IdTrees like '%," . $nodeInfo["FolderID"] . ",%' or
                                    IdTrees like '%," . $nodeInfo["FolderID"] . "'
                                 GROUP BY ParentID";
		            $result = $this->returnResultSet($strSQL);
		            $nodeInfo["child_folders"] = $result[0]["total_form"] ? $result[0]["total_form"] : 0;
		        }
		        return $nodeInfo;
		    } else {
		        $nodeInfo["FolderID"] = 0;
		        $nodeInfo["parentids"] = array();
		        
		        $strSQL = "SELECT COUNT(FolderID) as total_folders FROM " . $indexVar["FormInfo"]->formFolderNodeTable. " WHERE ParentID='" . $nodeInfo["FolderID"] . "'";
		        $result = $this->returnResultSet($strSQL);
		        $nodeInfo["total_folders"] = $result[0]["total_folders"] ? $result[0]["total_folders"] : 0;
		        
		        $strSQL = "SELECT COUNT(FolderID) as total_form FROM " . $indexVar["FormInfo"]->formInfoTable. " WHERE FolderID='" . $nodeInfo["FolderID"] . "'";
		        $result = $this->returnResultSet($strSQL);
		        $nodeInfo["total_forms"] = $result[0]["total_form"] ? $result[0]["total_form"] : 0;
		        
		        $strSQL = "SELECT COUNT(ParentID) as total_form FROM " . $indexVar["FormInfo"]->formFolderNodeTable. "
                                 WHERE
                                    IdTrees like '" . $nodeInfo["FolderID"] . "' or
                                    IdTrees like '" . $nodeInfo["FolderID"] . ",%' or
                                    IdTrees like '%," . $nodeInfo["FolderID"] . ",%' or
                                    IdTrees like '%," . $nodeInfo["FolderID"] . "'
                                 GROUP BY ParentID";
		        $result = $this->returnResultSet($strSQL);
		        $nodeInfo["child_folders"] = $result[0]["total_form"] ? $result[0]["total_form"] : 0;
		        
		        return $nodeInfo;
		    }
		}
		
		function getTreeNode($selected_folder, $load_nodeTree = null, $parentkey = '')
		{
		    global $indexVar, $Lang;
            $nodeInfo = $this->getNodeInfo($selected_folder);
            if ($selected_folder == '0') {
                if ($load_nodeTree === null)
                {
                    $nodeTree = array();
                    $nodeTree[0] = array(
                        "key" => 0,
                        "title" => $Lang["eForm"]["Folders"]["FormRootFolder"],
                        "folder" => true,
                        "selected" => true,
                        "expanded" => true,
                        "children" => $this->getChildNodes('0')
                    );
                } else {
                    $nodeTree[0] = array(
                        "key" => 0,
                        "title" => $Lang["eForm"]["Folders"]["FormRootFolder"],
                        "folder" => true,
                        "expanded" => true,
                        "children" => $load_nodeTree
                    );
                }
            } else {
                $myParentLevel = $this->getChildNodes($nodeInfo['ParentID']);
                if (isset($myParentLevel) && count($myParentLevel) > 0)
                {
                    foreach ($myParentLevel as $kk => $vv) {
                        if ($vv["key"] == $nodeInfo["FolderID"])
                        {
                            $myParentLevel[$kk]['currnet'] = true;
                            $myChilds = $this->getChildNodes($vv["key"]);
                            if (count($myChilds) > 0) {
                                $myParentLevel[$kk]['expanded'] = true;
                                $myParentLevel[$kk]['children'] = $myChilds;
                            }
                            if ($load_nodeTree === null)
                            {
                                $myParentLevel[$kk]['selected'] = true;
                                $nodeTree = $this->getTreeNode($nodeInfo["ParentID"], $myParentLevel, $kk);
                            } else {
                                $myParentLevel[$kk]["children"] = $load_nodeTree;
                                $nodeTree = $this->getTreeNode($nodeInfo["ParentID"], $myParentLevel, $kk);
                            }
                        }
                    }
                }
            }
            // 
            return $nodeTree;
		}
		
		
		function getChildNodes($folder = '0')
		{
		    global $indexVar, $Lang;
		    $strSQL = "SELECT 
                            main_folder.FolderID, main_folder.FolderSlug, main_folder.FolderTitleEN, main_folder.FolderTitleCN, main_folder.ParentID, main_folder.IdTrees, main_folder.Priority, COUNT(child_folder.FolderID) as childs
                        FROM
                            " . $indexVar["FormInfo"]->formFolderNodeTable. " as main_folder
                        LEFT JOIN 
                            " . $indexVar["FormInfo"]->formFolderNodeTable. " as child_folder on (main_folder.FolderID=child_folder.ParentID)
                        WHERE
                            main_folder.ParentID='" . $folder . "'
                        GROUP BY
                            main_folder.FolderID
                        ORDER BY 
                            main_folder.Priority asc, main_folder.FolderTitleEN asc";

		    $result = $this->returnResultSet($strSQL);
		    $folderNode = array();
		    if (count($result) > 0) {
		        foreach ($result as $key => $val) { 
		            $folderNode[$key] = array(
		                "key" => $val['FolderID'],
		                "parentKey" => $val["ParentID"],
		                "title" => ($this->currLang == 'en') ? $val['FolderTitleEN'] : (!empty($val['FolderTitleCN']) ? $val['FolderTitleCN'] : $val['FolderTitleEN']),
		                "folder" => true,
		                "parentids" => (!empty($val["IdTrees"]) ? explode(",", $val["IdTrees"]) : array('0')),
		                "slug" => $val["FolderSlug"],
		                "total_childs" => $val['childs'],
		                "lazy" => ($val["childs"] > 0) ? true : false
		            );
		        }
		    }
		    return $folderNode;
		}
		
		public function getBreadcrumb($folderID)
		{
		    global $indexVar, $Lang;
		    $folderInfo = $this->getNodeInfo($folderID);
		    
		    $breadcrumbArr = array();
		    $breadcrumbArr[0] = array( "id" => "0", "title" => $Lang["eForm"]["Folders"]["FormRootFolder"]);
		    
		    if (count($folderInfo) > 0)
		    {
		        $idTrees = explode(",", $folderInfo["IdTrees"]);
		        $idTrees[] = $folderInfo["FolderID"];
	            $strSQL = "SELECT
                                FolderID, FolderTitleEN, FolderTitleCN
                            FROM
                                " . $indexVar["FormInfo"]->formFolderNodeTable . "
                            WHERE
                                FolderID in ('" . implode("','", $idTrees) . "')";
	            $result = $this->returnResultSet($strSQL);
	            $folderData = array();
	            if (count($result) > 0)
	            {
	                foreach ($result as $kk => $val)
	                {
	                    $folderData[$val["FolderID"]] = $val;
	                }
	            }
	            foreach ($idTrees as $kk => $val) {
	                if (isset($folderData[$val])) {
	                    $breadcrumbArr[] = array(
	                        "id" => $folderData[$val]["FolderID"],
	                        "title" => ($this->currLang == 'en') ? $folderData[$val]['FolderTitleEN'] : (!empty($folderData[$val]['FolderTitleCN']) ? $folderData[$val]['FolderTitleCN'] : $folderData[$val]['FolderTitleEN']),
	                        "selected" => ($folderData[$val]["FolderID"] == $folderInfo["FolderID"]) ? true : false
	                    );
	                }
	            }
		    }
		    
		    return $breadcrumbArr;
		}
		
		public function slugify($text)
		{
		    $text = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $text);
		    $text = preg_replace("/[^ \w]+/", "", $text);
		    $text = str_replace(" ", "-", $text);
		    return strtolower($text);
		}
		
		public function str_replace_first($from, $to, $content)
		{
		    $from = '/'.preg_quote($from, '/').'/';
		    
		    return preg_replace($from, $to, $content, 1);
		}
		
		public function createNode($postdata)
		{
		    global $indexVar, $Lang;
		    if (!isset($postdata["folder"]) || empty($postdata["folder"]))
		    {
		        $postdata["folder"] = '0';
		        $parentID = '0';
		        $IdTrees = '0';
		    }
		    else
		    {
		        $parentFolderInfo = $this->getNodeInfo($postdata["folder"]);
		        $parentID = $parentFolderInfo["FolderID"];
		        $IdTrees = $parentFolderInfo["IdTrees"] . "," . $parentFolderInfo["FolderID"];
		    }
		    
		    $title_en = $indexVar['FormInfo']->retrivDBValue(isset($postdata["title_en"]) ? $postdata["title_en"] : 'New Folder');
		    $title_cn = $indexVar['FormInfo']->retrivDBValue(isset($postdata["title_cn"]) ? $postdata["title_cn"] : $postdata["title_en"]);

		    $strSQL = "SELECT * FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderTitleEN like '" . $title_en . "' and ParentID='" . $parentID . "'";
		    $result = $this->returnResultSet($strSQL);
		    if (count($result) > 0) {
		        $strSQL = "SELECT COUNT(FolderID) as total_same FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderTitleEN like '" . $title_en . "%' and ParentID='" . $parentID . "'";
		        $result = $this->returnResultSet($strSQL);
		        $total_same = $result[0]["total_same"];
		        for($i=1; $i <= $total_same; $i++) {
		            $new_title_en = $title_en . " (" . $i . ")";
		            $strSQL = "SELECT * FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderTitleEN like '" . $new_title_en . "' and ParentID='" . $parentID . "'";
		            $result = $this->returnResultSet($strSQL);
		            if (!$result) {
		                $title_en = $new_title_en;
		                $title_cn = $new_title_en;
		                break;
		            }
		        }
		    }
		    $result = false;

		    $param = array(
		        "FolderSlug" => $this->slugify($title_en),
		        "FolderTitleEN" => $title_en,
		        "FolderTitleCN" => $title_cn,
		        'ParentID' => $parentID,
		        'IdTrees' => $IdTrees,
		        'Priority' => '0',
		        "InputBy" => $_SESSION["UserID"],
		        "DateInput" => date("Y-m-d H:i:s"),
		        "ModifyBy" => $_SESSION["UserID"],
		        "DateModified" => date("Y-m-d H:i:s")
		    );
		    if (!empty($title_en)) {
    		    $strSQL = "INSERT INTO " . $indexVar["FormInfo"]->formFolderNodeTable . " (" . implode(", ", array_keys($param)) . ") VALUES ('" . implode("', '", $param) . "')";
    		    $result = $this->db_db_query($strSQL);
    		    if ($result) {
    		        $output = array( "result" => 'success', "ParentID" => $parentID );
    		    }
    		    else 
    		    {
    		        header("HTTP/1.0 404 Update failed");
    		        exit;
    		    }
		    } else {
		        header("HTTP/1.0 404 Update failed");
		        exit;
		    }
		    return $output;
		}
		
		public function updateNode($postdata)
		{
		    global $indexVar, $Lang;
		    if (!isset($postdata["folder"]) || empty($postdata["folder"]))
		    {
		        header("HTTP/1.0 404 Not found");
		        exit;
		    }
		    else
		    {
		        $parentFolderInfo = $this->getNodeInfo($postdata["folder"]);
		        $parentID = $parentFolderInfo["ParentID"];
		        $IdTrees = $parentFolderInfo["IdTrees"] . "," . $parentFolderInfo["FolderID"];
		    }
		    
		    $title_en = $indexVar['FormInfo']->retrivDBValue(isset($postdata["title_en"]) ? $postdata["title_en"] : 'New Folder');
		    $title_cn = $indexVar['FormInfo']->retrivDBValue(isset($postdata["title_cn"]) ? $postdata["title_cn"] : $postdata["title_en"]);
		    
		    $strSQL = "SELECT * FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderTitleEN like '" . $title_en . "' and FolderID !='" . $postdata["folder"] . "' and ParentID='" . $parentID . "'";
		    $result = $this->returnResultSet($strSQL);
		    if (count($result) > 0) {
		        $strSQL = "SELECT COUNT(FolderID) as total_same FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderTitleEN like '" . $title_en . "%' and FolderID !='" . $postdata["folder"] . "' and ParentID='" . $parentID . "'";
		        $result = $this->returnResultSet($strSQL);
		        $total_same = $result[0]["total_same"];
		        for($i=1; $i <= $total_same; $i++) {
		            $new_title_en = $title_en . " (" . $i . ")";
		            $strSQL = "SELECT * FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderTitleEN like '" . $new_title_en . "' and FolderID !='" . $postdata["folder"] . "' and ParentID='" . $parentID . "'";
		            $result = $this->returnResultSet($strSQL);
		            if (!$result) {
		                $title_en = $new_title_en;
		                $title_cn = $new_title_en;
		                break;
		            }
		        }
		    }
		    $result = false;
		    
		    $param = array(
		        "FolderSlug" => $this->slugify($title_en),
		        "FolderTitleEN" => $title_en,
		        "FolderTitleCN" => $title_cn,
		        "ModifyBy" => $_SESSION["UserID"],
		        "DateModified" => date("Y-m-d H:i:s")
		    );
		    
		    if (!empty($title_en) && $postdata["folder"] != "0") {
		        $strSQL = "UPDATE " . $indexVar["FormInfo"]->formFolderNodeTable . " SET ";
		        $update_param = "";
		        foreach ($param as $key => $val) {
		            if (!empty($update_param)) {
		                $update_param .= ", ";
		            }
		            $update_param .= $key . " = '" . $val . "'";
		        }
		        $strSQL .= $update_param . " WHERE FolderID='" . $postdata["folder"] . "'";
		        $result = $this->db_db_query($strSQL);
		        if ($result) {
		            $output = array( "result" => 'success', "ParentID" => $postdata["folder"] );
		        }
		        else
		        {
		            header("HTTP/1.0 404 Update failed");
		            exit;
		        }
		    } else {
		        header("HTTP/1.0 404 Update failed");
		        exit;
		    }
		    return $output;
		}
		
		public function moveNodeToAnother($postdata)
		{
		    global $indexVar, $Lang;
		    if (!isset($postdata["folder"]) || empty($postdata["folder"]))
		    {
		        header("HTTP/1.0 404 Not found");
		        exit;
		    }
		    else
		    {
		        $parentFolderInfo = $this->getNodeInfo($postdata["folder"]);
		        $parentID = $parentFolderInfo["FolderID"];
		        $IdTrees = $parentFolderInfo["IdTrees"] . "," . $parentFolderInfo["FolderID"];
		    }
		    if ($parentID == $postdata["newParent"]) {
		        header("HTTP/1.0 404 Not found");
		        exit;
		    }
		    if ($postdata["newParent"] == "0")
		    {
		        $newIdsTrees = '0';
		    } else {
		        $newParentInfo = $this->getNodeInfo($postdata["newParent"]);
		        if (empty($newParentInfo["IdTrees"])) $newParentInfo["IdTrees"] = '0';
		        $newIdsTrees = $newParentInfo["IdTrees"] . "," . $newParentInfo["FolderID"];
		    }
		    
		    $title_en = $parentFolderInfo["FolderTitleEN"];
		    $title_cn = $parentFolderInfo["FolderTitleCN"];
		    
		    $strSQL = "SELECT * FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderTitleEN like '" . $title_en . "' and FolderID !='" . $postdata["folder"] . "' and ParentID='" . $postdata["newParent"] . "'";
		    $result = $this->returnResultSet($strSQL);
		    if (count($result) > 0) {
		        $strSQL = "SELECT COUNT(FolderID) as total_same FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderTitleEN like '" . $title_en . "%' and FolderID !='" . $postdata["folder"] . "' and ParentID='" . $postdata["newParent"]. "'";
		        $result = $this->returnResultSet($strSQL);
		        $total_same = $result[0]["total_same"];
		        for($i=1; $i <= $total_same; $i++) {
		            $new_title_en = $title_en . " (" . $i . ")";
		            $strSQL = "SELECT * FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderTitleEN like '" . $new_title_en . "' and FolderID !='" . $postdata["folder"] . "' and ParentID='" . $postdata["newParent"]. "'";
		            $result = $this->returnResultSet($strSQL);
		            if (!$result) {
		                $title_en = $new_title_en;
		                $title_cn = $new_title_en;
		                break;
		            }
		        }
		    }
		    
		    $param = array(
		        "FolderSlug" => $this->slugify($title_en),
		        "FolderTitleEN" => $title_en,
		        "FolderTitleCN" => $title_cn,
		        "ParentID" => $postdata["newParent"],
		        "IdTrees" => $newIdsTrees,
		        "ModifyBy" => $_SESSION["UserID"],
		        "DateModified" => date("Y-m-d H:i:s")
		    );
		    $strSQL = "UPDATE " . $indexVar["FormInfo"]->formFolderNodeTable . " SET ";
		    $update_param = "";
		    foreach ($param as $key => $val) {
		        if (!empty($update_param)) {
		            $update_param .= ", ";
		        }
		        $update_param .= $key . " = '" . $val . "'";
		    }
		    $strSQL .= $update_param . " WHERE FolderID='" . $postdata["folder"] . "'";
		    $result = $this->db_db_query($strSQL);
		    if ($result) {
		        $strSQL = "SELECT FolderID, IdTrees FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE IdTrees like '" . $IdTrees . "%'";
		        $result = $this->returnResultSet($strSQL);
		        $output = array( "result" => 'success', "ParentID" => $postdata["folder"] );
		        if (count($result) > 0) {
		            foreach ($result as $kk => $val) {
		                $child_IdTrees = $this->str_replace_first($parentFolderInfo["IdTrees"], $param["IdTrees"], $val["IdTrees"]);
		                $strSQL = "UPDATE " . $indexVar["FormInfo"]->formFolderNodeTable . " SET IdTrees='" . $child_IdTrees . "' WHERE FolderID='" . $val["FolderID"] . "'";
		                $result = $this->db_db_query($strSQL);
		            }
		        }
		    }
		    else
		    {
		        header("HTTP/1.0 404 Update failed");
		        exit;
		    }
		    return $output;
		}
		
		public function deleteNode($postdata)
		{
		    global $indexVar, $Lang;
		    if (!isset($postdata["folder"]) || empty($postdata["folder"]))
		    {
		        header("HTTP/1.0 404 Update failed");
		        exit;
		    }
		    else
		    {
		        $folderInfo = $this->getNodeInfo($postdata["folder"]);
		        if ($folderInfo["total_forms"] > 0 || $folderInfo["child_folders"] > 0) {
		            header("HTTP/1.0 404 Update failed");
		            exit;
		        } else {
		            $strSQL = "DELETE FROM " . $indexVar["FormInfo"]->formFolderNodeTable . " WHERE FolderID='" . $folderInfo["FolderID"] . "'";
		            $result = $this->db_db_query($strSQL);
		            $output = array( "result" => 'success', "ParentID" => $folderInfo["ParentID"] );
		        }
		    }
		    return $output;
		}
		
		public function moveItemToNode($postdata)
		{
		    global $indexVar, $Lang;
		    $folder = (!isset($postdata["folder"]) || empty($postdata["folder"])) ? '0' : $postdata["folder"];
		    $newFolder = (!isset($postdata["newParent"]) || empty($postdata["newParent"])) ? '0' : $postdata["newParent"];
		    
		    $folderInfo = $this->getNodeInfo($newFolder);
		    
		    if ($folder == $newFolder || $folderInfo == null) {
		        header("HTTP/1.0 404 Update failed");
		        exit;
		    }
		    $strSQL = "SELECT FormID FROM " . $indexVar["FormInfo"]->formInfoTable . " WHERE FolderID='" . $folder. "' and FormID in ('" . implode("','", $postdata["formItems"]) . "')";
		    $result = $this->returnResultSet($strSQL);
		    if (count($result) > 0) {
		        $existingFolderIDs = Get_Array_By_Key($result, "FormID");
		        $strSQL = "UPDATE " . $indexVar["FormInfo"]->formInfoTable . " SET
                            FolderID='" . $newFolder . "', ModifyBy='" . $_SESSION["UserID"] . "', DateModified=NOW()  
                            WHERE FolderID='" . $folder. "' and FormID in ('" . implode("', '", $existingFolderIDs) . "')";
		        $result = $this->db_db_query($strSQL);
		        $output = array( "result" => 'success', "ParentID" => $newFolder );
		    }
		    return $output;
		}
	}
}
?>