<?php 
// editing by
/********************
 * Date : 2018-03-19 Frankie
 ********************/

if (defined("LIBEFORM_DEFINED")) {
    class FormSelectPanel  extends libdb
    {
        
        function __construct($ModuleTitle = 'eForm') {
            parent::__construct($ModuleTitle);
        }
        
        public function getYearSelectionPanel($selected_Year)
        {
            global $indexVar;
            if (isset($selected_Year)) {
                $AcademicYearID = $selected_Year;
            } else {
                $AcademicYearID = Get_Current_Academic_Year_ID();
            }
            
            $strSQL = "SELECT AcademicYearID, YearNameEN, YearNameB5 From " . $indexVar["FormInfo"]->AcademicYearTable . " ORDER BY Sequence ASC";
            $result = $this->returnResultSet($strSQL);
            if (count($result) > 0) {
                foreach ($result as $key => $val) {
                    $yearOptionArr[$val["AcademicYearID"]] = Get_Lang_Selection((empty($val["YearNameB5"]) ? $val["YearNameEN"] : $val["YearNameB5"]), $val["YearNameEN"]);
                }
            }
            $year_selection = "<SELECT name='year' class='form-control" . $select_class . "'>";
            if (count($yearOptionArr)) {
                foreach ($yearOptionArr as $key => $val) {
                    $year_selection .= "<OPTION value='" . $key . "'" . (($AcademicYearID == $key) ? " selected" : "") . ">" . $val . "</OPTION>";
                }
            }
            $year_selection .= "</SELECT>";
            return $year_selection;
        }
        
        public function getYearTermSelectionPanel($args)
        {
            global $indexVar;
            $AcademicYearID = $args["yearID"];
            $YearTermID = $args["yearTermID"];
            
            $strSQL = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 From " . $indexVar["FormInfo"]->AcademicYearTermTable . " WHERE AcademicYearID='" . $AcademicYearID . "' ORDER BY TermStart ASC";
            $result = $this->returnResultSet($strSQL);
            if (count($result) > 0) {
                foreach ($result as $key => $val) {
                    $yearOptionArr[$val["YearTermID"]] = Get_Lang_Selection((empty($val["YearTermNameB5"]) ? $val["YearTermNameEN"] : $val["YearTermNameB5"]), $val["YearTermNameEN"]);
                }
            }
            $yearterm_selection = "<SELECT name='yearterm' class='form-control" . $select_class . "'>";
            if (count($yearOptionArr)) {
                foreach ($yearOptionArr as $key => $val) {
                    $yearterm_selection .= "<OPTION value='" . $key . "'" . (($YearTermID== $key) ? " selected" : "") . ">" . $val . "</OPTION>";
                }
            }
            $yearterm_selection .= "</SELECT>";
            return $yearterm_selection;
        }
        
        public function getTypeSelectionPanel($recordType ='1', $args = array())
        {
            global $indexVar, $intranet_session_language, $button_select, $Lang;
            $typeOptionArr = array();
            switch ($recordType) {
                case "1": // Staff
                    $selectedType = 't1';
                    if (isset($args["selectedType"]) && $args["selectedType"] == "t2") {
                        $selectedType = 't2';
                    }
                    $typeOptionArr = array(
                        "t1" => $Lang["eForm"]["TargetUserTeaching"],
                        "t2" => $Lang["eForm"]["TargetUserNonTeaching"]
                    );
                    $select_class = " teacheropt";
                    break;
                case "2" :
                    if (isset($args["yearID"])) {
                        $AcademicYearID = $args['yearID'];
                    } else {
                        $AcademicYearID = Get_Current_Academic_Year_ID();
                    }
                    
                    $selectedType = '';
                    if (isset($args["selectedType"]) && !empty($args["selectedType"])) {
                        $selectedType = $args["selectedType"];
                    }
                    
                    $strSQL = "SELECT 
                                    YC.YearClassID, ClassTitleEN, ClassTitleB5 
                                FROM " . $indexVar["FormInfo"]->YearClassTable . " YC
                                JOIN
                                    " . $indexVar["FormInfo"]->YearClassUserTable . " AS YCU ON (YCU.YearClassID=YC.YearClassID)
                                WHERE AcademicYearID='" . $AcademicYearID . "'
                                GROUP BY YC.YearClassID
                                ORDER BY ClassTitleEN ASC
                            ";
                    $select_class = " studentopt";
                    $result = $this->returnResultSet($strSQL);
                    if (count($result) > 0) {
                        foreach ($result as $key => $val) {
                            $typeOptionArr[$val["YearClassID"]] = Get_Lang_Selection((empty($val["ClassTitleB5"]) ? $val["ClassTitleEN"] : $val["ClassTitleB5"]), $val["ClassTitleEN"]);
                        }
                    }
                    break;
            }
            $type_selection = "<SELECT name='type' class='form-control" . $select_class . "'>";
            if (count($typeOptionArr)) {
                foreach ($typeOptionArr as $key => $val) {
                    $type_selection.= "<OPTION value='" . $key . "'" . (($selectedType == $key) ? " selected" : "") . ">" . $val . "</OPTION>";
                }
            }
            $type_selection.= "</SELECT>";
            return $type_selection;
        }
        
        public function getSubjectSelectionPanel($args) {
            global $indexVar, $intranet_session_language, $button_select, $Lang;
            
            if (isset($args["yearTermID"])) {
                $yearTermID = $args['yearTermID'];
            }
            
            $typeOptionArr = array();
            
            $strSQL = "SELECT RecordID, EN_DES, CH_DES FROM " . $indexVar["FormInfo"]->SubjectTable . " WHERE RecordID in (Select SubjectID From " . $indexVar["FormInfo"]->SubjectRelationTable . " where YearTermID='" . $yearTermID . "') AND RecordStatus='1'";
            $result = $this->returnResultSet($strSQL);
            if (count($result) > 0) {
                foreach ($result as $key => $val) {
                    $typeOptionArr[$val["RecordID"]] = Get_Lang_Selection((empty($val["CH_DES"]) ? $val["EN_DES"] : $val["CH_DES"]), $val["EN_DES"]);
                }
            }
            $select_class = "subjectgroupopt";
            $type_selection = "<SELECT name='type' class='form-control " . $select_class . "'>";
            if (count($typeOptionArr)) {
                foreach ($typeOptionArr as $key => $val) {
                    $type_selection.= "<OPTION value='" . $key . "'" . (($selectedType == $key) ? " selected" : "") . ">" . $val . "</OPTION>";
                }
            }
            $type_selection .= "</SELECT>";
            return $type_selection;
        }
        
        public function getUserOptions($recordType, $args = array())
        {
            global $indexVar, $intranet_session_language;
            $optionArr = array();
            $subType = (isset($args["subtype"]) && $args["subtype"] == "multiple") ? "multiple" : "";
            if ($subType != "multiple") {
                $subType = "multiple";
            }
            $hasSelected = false;
            switch ($recordType) {
                case "1": // Staff
                    if (isset($args["selected"]) && !empty($args["selected"]) && count($args["selected"]) > 0) {
                        // Selected User
                        $strSQL = "SELECT
                                        UserID, EnglishName, ChineseName
                                    FROM
                                        " . $indexVar["FormInfo"]->userTable . "
                                    WHERE
                                        RecordType='" . $recordType . "' AND UserID IN ('" . implode("', '", $args["selected"]) . "')
                                    ORDER BY EnglishName ASC
                                    ";
                        $result = $this->returnResultSet($strSQL);
                        if (count($result) > 0){
                            $hasSelected = true;
                            $i = 0;
                            foreach ($result as $key => $val) {
                                $optionArr[$val['UserID']]["id"] = $val['UserID'];
                                $optionArr[$val['UserID']]["label"] = Get_Lang_Selection((empty($val["ChineseName"]) ? $val["EnglishName"] : $val["ChineseName"]), $val["EnglishName"]);
                                $optionArr[$val['UserID']]["selected"] = true;
                                $optionArr[$val['UserID']]["data-sortindex"] = $i;
                                $i++;
                            }
                        }
                    }
                    
                    $is_teaching = 1;
                    $is_html = true; 
                    if (isset($args['teaching']) && $args['teaching'] == "0") {
                        $is_teaching = 0;
                    }
                    if (isset($args['html']) && $args['html'] == false) {
                        $is_html = false; 
                    }
                    $strSQL = "SELECT 
                                    UserID, EnglishName, ChineseName 
                                FROM
                                    " . $indexVar["FormInfo"]->userTable. "
                                WHERE
                                    RecordType='" . $recordType . "' AND RecordStatus='1' and Teaching='" . $is_teaching . "'
                                ";
                    if (count($optionArr) > 0) {
                        $strSQL .= " AND UserID NOT IN ('" . implode("', '", $args["selected"]) . "')";
                    }
                    $strSQL .= " ORDER BY EnglishName ASC";
                    $result = $this->returnResultSet($strSQL);
                    if (count($result) > 0){
                        foreach ($result as $key => $val) {
                            $optionArr[$val['UserID']]["id"] = $val['UserID'];
                            $optionArr[$val['UserID']]["label"] = Get_Lang_Selection((empty($val["ChineseName"]) ? $val["EnglishName"] : $val["ChineseName"]), $val["EnglishName"]);
                            $optionArr[$val['UserID']]["label"] = trim(str_replace("<br>", "", $optionArr[$val['UserID']]["label"]));
                            $optionArr[$val['UserID']]["selected"] = false;
                        }
                    }
                    break;
                case "2": // Student
                    if (isset($args["yearID"])) {
                        $AcademicYearID = $args['yearID'];
                    } else {
                        $AcademicYearID = Get_Current_Academic_Year_ID();
                    }
                    if (isset($args["selected"]) && !empty($args["selected"]) && count($args["selected"]) > 0) {
                        // Selected User
                        $strSQL = "SELECT 
                                        UI.UserID, UI.EnglishName, UI.ChineseName, YC.YearClassID, YCU.ClassNumber, ClassTitleEN, ClassTitleB5
                                    FROM
                                        " . $indexVar["FormInfo"]->userTable. " AS UI
                                    JOIN
                                        " . $indexVar["FormInfo"]->YearClassUserTable . " AS YCU ON (YCU.UserID=UI.UserID)
                                    JOIN
                                        " . $indexVar["FormInfo"]->YearClassTable . " AS YC ON (YC.YearClassID=YCU.YearClassID AND AcademicYearID='" . $AcademicYearID . "')
                                    WHERE
                                        UI.RecordType='" . $recordType . "' AND UI.RecordStatus='1' AND UI.UserID IN ('" . implode("', '", $args["selected"]) . "')
                                    ORDER BY ClassTitleEN, ClassNumber, EnglishName ASC";

                        $result = $this->returnResultSet($strSQL);
                        if (count($result) > 0){
                            $hasSelected = true;
                            $i = 0;
                            foreach ($result as $key => $val) {
                                $optionArr[$val['UserID']]["id"] = $val['UserID'];
                                $optionArr[$val['UserID']]["label"] = "" . Get_Lang_Selection((empty($val["ClassTitleB5"]) ? $val["ClassTitleEN"] : $val["ClassTitleB5"]), $val["ClassTitleEN"]) . " (" . $val['ClassNumber'] . ") - ";
                                $optionArr[$val['UserID']]["label"] .= Get_Lang_Selection((empty($val["ChineseName"]) ? $val["EnglishName"] : $val["ChineseName"]), $val["EnglishName"]);
                                $optionArr[$val['UserID']]["selected"] = true;
                                $optionArr[$val['UserID']]["YearClassID"] = $val["YearClassID"];
                                $optionArr[$val['UserID']]["data-sortindex"] = $i;
                                $i++;
                            }
                        }
                    }
                    
                    $is_html = true;
                    if (isset($args['html']) && $args['html'] == false) {
                        $is_html = false;
                    }
                    $strSQL = "SELECT 
                                    UI.UserID, UI.EnglishName, UI.ChineseName, YC.YearClassID, YCU.ClassNumber, ClassTitleEN, ClassTitleB5
                                FROM
                                    " . $indexVar["FormInfo"]->userTable. " AS UI
                                JOIN
                                    " . $indexVar["FormInfo"]->YearClassUserTable . " AS YCU ON (YCU.UserID=UI.UserID)
                                JOIN
                                    " . $indexVar["FormInfo"]->YearClassTable . " AS YC ON (YC.YearClassID=YCU.YearClassID AND AcademicYearID='" . $AcademicYearID . "')
                                WHERE
                                    UI.RecordType='" . $recordType . "' AND UI.RecordStatus='1' and YCU.YearClassID='" . $args["yearClassID"] . "'";
                    if (count($optionArr) > 0) {
                        $strSQL .= " AND UI.UserID NOT IN ('" . implode("', '", $args["selected"]) . "')";
                    }
                    $strSQL .= " ORDER BY ClassNumber, EnglishName ASC";
                    $result = $this->returnResultSet($strSQL);
                    if (count($result) > 0){
                        foreach ($result as $key => $val) {
                            $optionArr[$val['UserID']]["id"] = $val['UserID'];
                            $optionArr[$val['UserID']]["label"] = "" .  Get_Lang_Selection((empty($val["ClassTitleB5"]) ? $val["ClassTitleEN"] : $val["ClassTitleB5"]), $val["ClassTitleEN"]) . " (" . $val['ClassNumber'] . ") - ";
                            $optionArr[$val['UserID']]["label"] .= Get_Lang_Selection((empty($val["ChineseName"]) ? $val["EnglishName"] : $val["ChineseName"]), $val["EnglishName"]);
                            $optionArr[$val['UserID']]["YearClassID"] = $val["YearClassID"];
                            $optionArr[$val['UserID']]["selected"] = false;
                        }
                    }
                    break;
            }
            if ($is_html) {
                $strHtml = "<select class='option_panel' id='myds' name='dualselect[]' " . $subType . ">";
//                 if ($subType != "multiple" && !$hasSelected) {
//                     $strHtml .= "<option value='' class='nullopt' selected></option>";
//                 }
                if (count($optionArr) > 0){
                    foreach ($optionArr as $key => $val) {
                        $strHtml .= "<option ";
                        $strHtml .= "value='" . $val["id"] . "' ";
                        if (isset($val["YearClassID"])) {
                            $strHtml .= " rel='" . $val["YearClassID"] . "'";
                        }
                        if ($val["selected"]) {
                            $strHtml .= "selected data-sortindex='" . $val["data-sortindex"] . "'";
                        }
                        $strHtml .= ">" . $val["label"] . "</option>\n";
                    }
                }
                $strHtml .= "</select>";
                return $strHtml;
            }
            return $optionArr;
        }
        
        public function getSubjectGroupOptions($args)
        {
            global $indexVar, $intranet_session_language;
            $optionArr = array();
            $subType = (isset($args["subtype"]) && $args["subtype"] == "multiple") ? "multiple" : "";
            if ($subType != "multiple") {
                $subType = "multiple";
            }
            $hasSelected = false;
            if (isset($args["selected"]) && !empty($args["selected"]) && count($args["selected"]) > 0) {
                $strSQL = "SELECT
                            stc.SubjectGroupID, stc.ClassCode, stc.ClassTitleEN, stc.ClassTitleB5
                        FROM
                            " . $indexVar["FormInfo"]->SubjectGroupTable . " AS stc
                        JOIN " . $indexVar["FormInfo"]->SubjectRelationTable . " AS srt ON ( stc.SubjectGroupID = srt.SubjectGroupID )
                        JOIN " . $indexVar["FormInfo"]->AcademicYearTermTable . " AS ayt ON (ayt.YearTermID=srt.YearTermID AND AcademicYearID='" . $args["yearID"] . "')
                        WHERE stc.SubjectGroupID in ('" . implode("', '", $args["selected"]) . "')";
                $strSQL .= " ORDER BY ClassTitleEN ASC";
                $result = $this->returnResultSet($strSQL);
                if (count($result) > 0){
                    foreach ($result as $key => $val) {
                        $optionArr[$val['SubjectGroupID']]["id"] = $val['SubjectGroupID'];
                        $optionArr[$val['SubjectGroupID']]["label"] = Get_Lang_Selection((empty($val["ClassTitleB5"]) ? $val["ClassTitleEN"] : $val["ClassTitleB5"]), $val["ClassTitleEN"]);
                        $optionArr[$val['SubjectGroupID']]["selected"] = true;
                    }
                }
            }
            
            $is_html = true;
            if (isset($args['html']) && $args['html'] == false) {
                $is_html = false;
            }
            $strSQL = "SELECT 
                            stc.SubjectGroupID, stc.ClassCode, stc.ClassTitleEN, stc.ClassTitleB5 
                        FROM
                            " . $indexVar["FormInfo"]->SubjectGroupTable . " AS stc
                        JOIN " . $indexVar["FormInfo"]->SubjectRelationTable . " AS srt ON ( stc.SubjectGroupID = srt.SubjectGroupID AND YearTermID='" . $args["yearTermID"] . "' AND SubjectID='" . $args["subjectID"] . "')
                        ";
            if (count($optionArr) > 0) {
                $strSQL .= " AND stc.SubjectGroupID NOT IN ('" . implode("', '", $args["selected"]) . "')";
            }
            $strSQL .= " ORDER BY ClassTitleEN ASC";
            
            $result = $this->returnResultSet($strSQL);
            if (count($result) > 0){
                foreach ($result as $key => $val) {
                    $optionArr[$val['SubjectGroupID']]["id"] = $val['SubjectGroupID'];
                    $optionArr[$val['SubjectGroupID']]["label"] = Get_Lang_Selection((empty($val["ClassTitleB5"]) ? $val["ClassTitleEN"] : $val["ClassTitleB5"]), $val["ClassTitleEN"]);
                    $optionArr[$val['SubjectGroupID']]["selected"] = false;
                }
            }
            if ($is_html) {
                $strHtml = "<select class='option_panel' id='myds' name='dualselect[]' " . $subType . ">";
//                 if ($subType != "multiple" && !$hasSelected) {
//                     $strHtml .= "<option value='' class='nullopt' selected></option>";
//                 }
                if (count($optionArr) > 0){
                    foreach ($optionArr as $key => $val) {
                        $strHtml .= "<option ";
                        $strHtml .= "value='" . $val["id"] . "' ";
                        if ($val["selected"]) {
                            $strHtml .= "selected data-sortindex='" . $val["data-sortindex"] . "'";
                        }
                        $strHtml .= ">" . $val["label"] . "</option>\n";
                    }
                }
                $strHtml .= "</select>";
                return $strHtml;
            }
            return $optionArr;
        }
    }
}
?>