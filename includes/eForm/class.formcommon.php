<?php
// editing by
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
 * 
 * Date : 2017-05-31 Frankie
 ********************/
if (defined("LIBEFORM_DEFINED")) {
	
	function retrivPostValue(&$val) {
		// $specialChar = array("'" => "&#39;", '"' => "&#34;", '>' => "&gt;", '<' => "&lt;", "\n" => "<br>", "\r" => "", "&" => "&amp;");
		$val = trim($val);
		// $val = str_replace(array_values($specialChar), array_keys($specialChar), $val);
		// $val = str_replace(array_keys($specialChar), array_values($specialChar), $val);
	}
	
	function retrivDBValue(&$val) {
		// $specialChar = array("'" => "&#39;", '"' => "&#34;", '>' => "&gt;", '<' => "&lt;", "\n" => "<br>", "\r" => "", "&" => "&amp;");
		// $specialChar = array("&" => "&amp;");
		$val = trim($val);
		// $val = str_replace(array_values($specialChar), array_keys($specialChar), $val);
	}
	
	class FormCommon extends libdb {
	    public function __construct($ModuleTitle = 'eForm') {
	        global $Lang, $intranet_session_language, $junior_mck;
			$this->libdb();
			$this->formLang = $Lang["eForm"];
			$this->ModuleTitle = $ModuleTitle;
			switch ($ModuleTitle) {
				case "eForm":
				default:
					$this->TablePrefix = "INTRANET_EFORM";
					break;
			}
			if ($junior_mck) {
			    // EJ User Table
			    $this->userTable = "INTRANET_USER_UTF8";
			}
			else 
			{
			    // IP User Type
			    $this->userTable = "INTRANET_USER";
			}
			$this->formInfoTable = $this->TablePrefix . "_FORM_INFO";
			$this->formInfoTargetUserTable = $this->TablePrefix . "_FORM_TARGET_USER";
			$this->formGridTable = $this->TablePrefix . "_FORM_GRID";
			$this->formGridDataTable = $this->TablePrefix . "_FORM_GRID_DATA";
			$this->formTemplateTable = $this->TablePrefix . "_TEMPLATE";
			$this->formTemplateTVTable = $this->TablePrefix . "_TEMPLATE_TV";
			$this->formSuppleRequestTable = $this->TablePrefix . "_SUPPLEMENTAL_INFO";
			$this->formFolderNodeTable = $this->TablePrefix . "_FOLDER";

			/*****************************/
			$this->YearClassTable = "YEAR_CLASS";
			$this->YearClassUserTable = "YEAR_CLASS_USER";
			$this->SubjectGroupTable = "SUBJECT_TERM_CLASS";
			$this->SubjectRelationTable = "SUBJECT_TERM";
			$this->SubjectTable = "ASSESSMENT_SUBJECT";
			$this->AcademicYearTable = "ACADEMIC_YEAR";
			$this->AcademicYearTermTable = "ACADEMIC_YEAR_TERM";
			/*****************************/
			
			$this->allowDirectDelete = false; ## for FormInfo & Template
			$this->allowResetAutoIncID = true;
			
			include_once(dirname(dirname(__FILE__))."/json.php");
			$this->JSON_obj = new JSON_obj();

			$this->elmTypeArr = array(
					"text" => "TX",
					"number" => "NO",
					"textarea" => "TA",
					"select" => "SL",
					"hidden" => "HI",
					"dateFieldElm" => "DF",
			        "timeFieldElm" => "TF",
					"checkbox-group" => "CG",
					"radio-group" => "RG",
					"fixedTableElm" => "FT",
			        "dynamicTableElm" => "DT",
			        "subjectGroupElm" => "DBSG",
			        "teacherElm" => "DBTR",
			        "studentElm" => "DBST"
			);
			
			$this->GridStatusArr = array(
					"COMPLETED" => "1",
					"READY" => "-1",
					"SUPPLEMENTARY_REQUEST" => "2",
					"NOT_COMPLETED" => "3",
					"EXPIRED" => "4"
			);
			$this->currLang = ($intranet_session_language == "en") ? "en":"b5";
		}
		
		public function getElmTypeArr() {
			return $this->elmTypeArr;
		}
		
		public function sqlOpt($sql) {
			$replaceArr = array( "\n", "\t", "\r", "   ", "  ");
			return str_replace($replaceArr, " ", $sql);
		}
		
		public function strToJSON($str) {
			$json = $this->JSON_obj->decode($str);
			return $json;
		}
		
		public function jsonToStr($json) {
			$str = $this->JSON_obj->encode($json);
			return $str;
		}
		
		public function jsonToDBFormat($json, $isString = true) {
			if ($isString) {
				$templateObj = $this->strToJSON($json);
			} else {
				$templateObj = $json;
			}
			$res = base64_encode($this->jsonToStr($templateObj));
			return $res;
		}
		
		public function getToken($str) {
			return md5($str . "-" . $this->ModuleTitle . "-" . $_SESSION["UserID"]);
		}
		
		public function dbToJsonFormat($str, $toString = true) {
			if (!empty($str)) {
				$tmp = base64_decode($str);
				$output = $this->strToJSON($tmp);
				if ($toString) {
					$res = $this->jsonToStr($output);
				} else {
					$res = $output;
				}
				return $res;
			}
			return "";
		}
		####################################################################################################
		public function isDeletedCondition($_prefix = "", $isDeleted = 0) {
			return $_prefix . "isDeleted='" . $isDeleted . "'";
		}
		
		public function retrivDBValue($val, $isRev = false) {
			$val = trim($val);
			if ($isRev) {
				$specialChar = array("'" => "&#39;", '"' => "&#34;", '>' => "&gt;", '<' => "&lt;");
				$val =  str_replace(array_values($specialChar), array_keys($specialChar), $val);
				$val =  str_replace("<br>", "\n", $val);
			} else {
				$specialChar = array("'" => "&#39;", '"' => "&#34;", '>' => "&gt;", '<' => "&lt;", "\n" => "<br>", "\r" => "", "\\" => "\\\\");
				$val =  str_replace(array_keys($specialChar), array_values($specialChar), $val);
				$val = str_replace("&lt;br&gt;", "<br>", $val);
			}
			return $val;
		}
		
		public function resetNextAutoIncreID($tablename, $prikey = "") {
			if ($this->allowResetAutoIncID && !empty($prikey)) {
				$strSQL = "SELECT MAX(" . $prikey . ") as latestID FROM " . $tablename;
				$result = $this->returnResultSet($strSQL);
				if ($result) {
					$latestID = $result[0]["latestID"] + 1;
				} else {
					$latestID = 1;
				}
				$strSQL = "ALTER TABLE " . $tablename. " AUTO_INCREMENT = " . $latestID;
				$this->db_db_query($strSQL);
				// $result = $this->db_db_query($strSQL);
			}
		}
		
		public function getDBSelectionData($type, $data) {
		    $meaningData = array();
		    
		    if (isset($data["year"]) && !empty($data["year"])) {
		        $strSQL = "SELECT AcademicYearID, YearNameEN, YearNameB5 FROM " . $this->AcademicYearTable . " WHERE AcademicYearID='" . $data["year"] . "' LIMIT 1";
		        $result = $this->returnResultSet($strSQL);
		        if (count($result) > 0) {
		            $meaningData["YearInfo"] = $result[0];
		        }
		    }
		    
		    if (isset($data["term"]) && !empty($data["term"])) {
		        $strSQL = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM " . $this->AcademicYearTermTable. " WHERE AcademicYearID='" . $data["year"] . "' and YearTermID='" . $data["term"] . "' LIMIT 1";
		        $result = $this->returnResultSet($strSQL);
		        if (count($result) > 0) {
		            $meaningData["TermInfo"] = $result[0];
		        }
		    }
		    
		    if (isset($data["otype"]) && !empty($data["otype"])) {
		        $meaningData["opt"] = $data["otype"];
		    }
		    
		    switch ($type) {
		        case "subjectGroupElm" :
		            if (isset($data["selectOpt"]) && !empty($data["selectOpt"])) {
		                $strSQL = "SELECT
                            stc.SubjectGroupID, stc.ClassCode, stc.ClassTitleEN, stc.ClassTitleB5
                        FROM
                            " . $this->SubjectGroupTable . " AS stc
                        JOIN " . $this->SubjectRelationTable . " AS srt ON ( stc.SubjectGroupID = srt.SubjectGroupID )
                        JOIN " . $this->AcademicYearTermTable . " AS ayt ON (ayt.YearTermID=srt.YearTermID AND AcademicYearID='" . $data["year"] . "')
                        WHERE stc.SubjectGroupID in ('" . implode("', '", $data["selectOpt"]) . "')";
		                if (count($optionArr) > 0) {
		                    $strSQL .= " AND str.SubjectGroupID NOT IN ('" . implode("', '", $args["selected"]) . "')";
		                }
		                $strSQL .= " ORDER BY ClassTitleEN ASC";
		                $result = $this->returnResultSet($strSQL);
		                if (count($result) > 0){
		                    foreach ($result as $key => $val) {
		                        $meaningData["dataArr"][$val['SubjectGroupID']]["id"] = $val['SubjectGroupID'];
		                        $meaningData["dataArr"][$val['SubjectGroupID']]["ClassTitleB5"] = $val["ClassTitleB5"];
		                        $meaningData["dataArr"][$val['SubjectGroupID']]["ClassTitleEN"] = $val["ClassTitleEN"];
		                    }
		                }
		            }
		            break;
		        case "studentElm":
		            if (isset($data["selectOpt"]) && !empty($data["selectOpt"])) {
    		            $strSQL = "SELECT
                                        UI.UserID, UI.EnglishName, UI.ChineseName, YC.YearClassID, YCU.ClassNumber, ClassTitleEN, ClassTitleB5
                                    FROM
                                        " . $this->userTable. " AS UI
                                    JOIN
                                        " . $this->YearClassUserTable . " AS YCU ON (YCU.UserID=UI.UserID)
                                    JOIN
                                        " . $this->YearClassTable . " AS YC ON (YC.YearClassID=YCU.YearClassID AND AcademicYearID='" . $data["year"] . "')
                                    WHERE
                                        UI.RecordType='2' AND UI.RecordStatus='1' AND UI.UserID IN ('" . implode("', '", $data["selectOpt"]) . "')
                                    ORDER BY ClassNumber, EnglishName ASC";
    		            $result = $this->returnResultSet($strSQL);
    		            if (count($result) > 0) {
    		                foreach ($result as $key => $val) {
    		                    $meaningData["dataArr"][$val['UserID']]["id"] = $val['UserID'];
    		                    $meaningData["dataArr"][$val['UserID']]["ChineseName"] = $val["ChineseName"];
    		                    $meaningData["dataArr"][$val['UserID']]["EnglishName"] = $val["EnglishName"];
    		                    $meaningData["dataArr"][$val['UserID']]["YearClassID"] = $val["YearClassID"];
    		                    $meaningData["dataArr"][$val['UserID']]["ClassNumber"] = $val["ClassNumber"];
    		                    $meaningData["dataArr"][$val['UserID']]["ClassTitleB5"] = $val["ClassTitleB5"];
    		                    $meaningData["dataArr"][$val['UserID']]["ClassTitleEN"] = $val["ClassTitleEN"];
    		                }
    		            }
		            }
		            break;
		        case "teacherElm" :
		            if (isset($data["selectOpt"]) && !empty($data["selectOpt"])) {
		                // Selected User
		                $strSQL = "SELECT
                                        UserID, EnglishName, ChineseName
                                    FROM
                                        " . $this->userTable . "
                                    WHERE
                                        RecordType='1' AND UserID IN ('" . implode("', '", $data["selectOpt"]) . "')
                                    ORDER BY EnglishName ASC
                                    ";
		                $result = $this->returnResultSet($strSQL);
		                if (count($result) > 0){
		                    foreach ($result as $key => $val) {
		                        $meaningData["dataArr"][$val['UserID']]["id"] = $val['UserID'];
		                        $meaningData["dataArr"][$val['UserID']]["ChineseName"] = $val["ChineseName"];
		                        $meaningData["dataArr"][$val['UserID']]["EnglishName"] = $val["EnglishName"];
		                    }
		                }
		            }
		            break;
		    }
		    return $meaningData;
		}
	}
}
?>