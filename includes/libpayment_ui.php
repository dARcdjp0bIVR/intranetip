<?php
// Editing by 
###############################################
## Please edit this page with UTF-8 encoding.##
## 請使用 UTF-8 編輯此頁。                                                       ##
###############################################
#
#   Date:   2019-07-02 Ray
#           add Get_Subsidy_Identity_Import_confirm, Get_Subsidy_Identity_Import_Finish_Page
#
#	Date:	2015-02-18 Carlos
#			$sys_custom['ePayment']['PaymentMethod'] - modified getPaymentItemReceipt(), added [Payment Method] and [Receipt Remark].
#
#	Date:	2014-12-30 Carlos
#			modified getPaymentItemReceipt() get last print receipt number for printing	
#
#	Date:	2014-06-17 Carlos
#			added getPaymentItemReceipt($params) for printing payment item receipts, sort by students with $sys_custom checking
#
#	Date:	2011-09-15 YatWoon
#			remark Get_General_Settings_Index()
#
###############################################

include_once('libinterface.php');

class libpayment_ui extends interface_html {
	function libpos_ui() {
		parent::interface_html();
		
		$this->thickBoxWidth = 750;
		$this->thickBoxHeight = 450;
		$this->toolCellWidth = "100px";
	}
		
	/*
	function Get_General_Settings_Index() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		
		include_once('libgeneralsettings.php');
		
		$GeneralSetting = new libgeneralsettings();
		$Settings = $GeneralSetting->Get_General_Setting('ePayment');
		
		$x .= '<form name="form1" method="post" action="edit.php">
					<br />
					<table width="96%" border="0" cellspacing="0" cellpadding="5">
					<tr> 
						<td class="tabletop">
							'.$Lang['ePayment']['SystemProperties'].'
						</td>
						<td class="tabletop">
							'.$Lang['ePayment']['SettineValue'].'
						</td>
					</tr>
					<tr>
						<td>
							'.$Lang['ePayment']['PaymentNoticeFeature'].'
						</td>
						<td>
							'.(($Settings['PaymentNoticeEnable'])? $Lang['General']['Yes']:$Lang['General']['No']).'
						</td>
					</tr>
					<tr>
						<td height="1" colspan="2" class="dotline">
							<img src="'.$image_path.'/'.$LAYOUT_SKIN.'"/10x10.gif" width="10" height="1">
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2">
							'.$this->GET_ACTION_BTN($Lang['Btn']['Edit'], "submit").'
						</td>
					</tr>
					</table>
					
					
					</form>
					<br />';
		return $x;
	}
	*/
	
	function Get_General_Settings_Form() {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $image_path;
		
		include_once('libgeneralsettings.php');
		
		$GeneralSetting = new libgeneralsettings();
		$Settings = $GeneralSetting->Get_General_Setting('ePayment');
		
		$x .= '<form name="form1" method="post" action="edit_update.php">
					<br />
					<table width="96%" border="0" cellspacing="0" cellpadding="5">
					<tr> 
						<td class="tabletop">
							'.$Lang['ePayment']['SystemProperties'].'
						</td>
						<td class="tabletop">
							'.$Lang['ePayment']['SettineValue'].'
						</td>
					</tr>
					<tr>
						<td>
							'.$Lang['ePayment']['PaymentNoticeFeature'].'
						</td>
						<td>
							<input type="radio" name="PaymentNoticeEnable" id="PaymentNoticeEnableTrue" value="1" '.(($Settings['PaymentNoticeEnable'])? 'checked="checked"':'').'>
							<label for="PaymentNoticeEnableTrue">'.$Lang['General']['Yes'].'</label>
							<input type="radio" name="PaymentNoticeEnable" id="PaymentNoticeEnableFalse" value="0" '.((!$Settings['PaymentNoticeEnable'])? 'checked="checked"':'').'>
							<label for="PaymentNoticeEnableFalse">'.$Lang['General']['No'].'</label>
						</td>
					</tr>
					<tr>
						<td height="1" colspan="2" class="dotline">
							<img src="'.$image_path.'/'.$LAYOUT_SKIN.'"/10x10.gif" width="10" height="1">
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2">
							'.$this->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit").'
              '.$this->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset").'
							'.$this->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location = 'index.php';","cancelbtn").'
						</td>
					</tr>
					</table>
					</form>
					<br />';
		return $x;
	}
	
	function getPaymentItemReceipt($params)
	{
		global $PATH_WRT_ROOT, $intranet_root, $Lang, $sys_custom, $lpayment;
		global $i_no_record_exists_msg,$i_Payment_Receipt_Person_In_Charge, $i_Payment_Receipt_Payment_Category, $i_Payment_Receipt_Payment_Item, $i_Payment_Receipt_Payment_Description;
		global $i_Payment_Field_RefCode, $i_Payment_Receipt_Date, $i_Payment_Receipt_Payment_Amount,$i_Payment_Receipt_Payment_Total, $i_Payment_Receipt_ReceivedFrom, $i_Payment_Receipt_PaidAmount, $i_Payment_Receipt_ForItems;
		global $i_Payment_Receipt_SchoolChop,$i_Payment_Receipt_Date,$i_Payment_Receipt_ThisIsComputerReceipt,$i_Payment_Receipt_Remark,$i_Payment_Receipt_UpTo,$i_Payment_Receipt_AccountBalance;
		
		$StudentID = $params['StudentID'];
		$PaymentID = $params['PaymentID'];
		$ClassName = $params['ClassName'];
		$date_from = $params['FromDate'];
		$date_to = $params['ToDate'];
		
		$Signature = $params['Signature'];
		$SignatureText = $params['SignatureText'];
		$StartingReceiptNumber = $params['StartingReceiptNumber'];
		if($sys_custom['PaymentReceiptNumber']){
			if($StartingReceiptNumber == ""){
				$StartingReceiptNumber = $lpayment->Get_Next_Receipt_Number();
			}
		}
		
		if($sys_custom['ePayment']['ReceiptByCategoryDesc']) {
			$Signature = 1;
			$SignatureText = $i_Payment_Receipt_Person_In_Charge;
		}
		
		$ReceiptTitle= $params['ReceiptTitle'] != '' ? $params['ReceiptTitle'] : $Lang['ePayment']['OfficialReceipt'];
		
		$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
		$school_name = $school_data[0];
		$school_org = $school_data[1];
		
		if ($sys_custom['PaymentReceiptNumber']) {
			// consolidate school year to recipt number
			$SchoolYear = explode(' - ',GET_ACADEMIC_YEAR());
			$ReceiptHead = substr($SchoolYear[0],-2).substr($SchoolYear[1],-2);
		}
		
		$table_header  = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"2\" class=\"eSporttableborder\">";
		$table_header .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Category</td>";
		$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Item</td>";
		$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Description</td>";
		$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Field_RefCode</td>";
		if($sys_custom['ePayment']['PaymentMethod']){
			$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['ePayment']['PaymentMethod']."</td>";
			$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['General']['Remark']."</td>";
		}
		$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Date</td>";
		$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Amount</td></tr>";
		
		//$display = "";
		
		//$lclass = new libclass();
		if ($StudentID == "" || count($StudentID)==0)
		{
		    if ($ClassName == "")
		    {
		        # All students
		        $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 ORDER BY ClassName, ClassNumber, EnglishName";
		        $target_students = $lpayment->returnVector($sql);
		    }
		    else
		    {
		        # All students in $ClassName
		        $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$ClassName' ORDER BY ClassName, ClassNumber, EnglishName";
		        $target_students = $lpayment->returnVector($sql);
		    }
		}
		else
		{
			if (is_array($StudentID)) {
				$target_students = $StudentID;
			}
			else {
		    $target_students = array($StudentID);
		  }
		}
		
		$date_field = "a.PaidTime";
		$conds = "";
		if ($date_from != "")
		{
		    $conds .= " AND $date_field >= '$date_from'";
		}
		if ($date_to != "")
		{
		    $conds .= " AND $date_field <= '$date_to 23:59:59'";
		}
		
		# Get Latest Balance
		//$lpayment = new libpayment();
		$list = "'".implode("','", $target_students)."'";
		$sql = "SELECT StudentID, Balance FROM PAYMENT_ACCOUNT WHERE StudentID IN ($list)";
		$temp = $lpayment->returnArray($sql,2);
		$balance_array = build_assoc_array($temp);
		//$PaymentID = explode(',', $_REQUEST['PaymentID']);
		$total_record = 0;
		
		$conds .= " AND a.PaymentID IN ('".implode("','",$PaymentID)."') ";
		
		$colspan = 5;
		if($sys_custom['ePayment']['PaymentMethod']){
			$colspan += 2;
		}
		
		$x = '';
		for ($j=0; $j<sizeof($target_students); $j++)
		{
			$target_id = $target_students[$j];
			$lu = new libuser($target_id);
			$studentname = $lu->UserNameClassNumber();
			$i_title = $studentname;
			$curr_balance = $balance_array[$target_id];
		           
		    $sql = "SELECT
								c.Name, 
								b.Name,
								b.Description, 
								a.PaidTime,
								a.Amount, 
								d.RefCode,
								d.RelatedTransactionID,
								c.Description ";
			if($sys_custom['ePayment']['PaymentMethod']){
				$sql .= ",a.PaymentMethod,a.ReceiptRemark ";
			}
			$sql .=	" FROM 
								PAYMENT_PAYMENT_ITEMSTUDENT as a
					  inner join PAYMENT_OVERALL_TRANSACTION_LOG as d on (d.TransactionType=2 and d.RelatedTransactionID=a.PaymentID and d.StudentID='$target_id')
			          LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
			          LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
			        WHERE 
			        	a.StudentID = '$target_id' 
			        	AND a.RecordStatus = 1 AND d.TransactionType=2 
			        	$conds 	        			
			        ORDER BY c.DisplayOrder, b.DisplayOrder ".($sys_custom['ePayment']['ReceiptByCategoryDesc']?",c.Description ":"")." ";
			$student_itemlist = $lu->returnArray($sql);
			
			if($sys_custom['ePayment']['ReceiptByCategoryDesc']){
				// items are group by student and payment item category description
				$sql = "SELECT 
							DISTINCT c.Description 
						FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
					  	INNER JOIN PAYMENT_OVERALL_TRANSACTION_LOG as d on (d.TransactionType=2 and d.RelatedTransactionID=a.PaymentID and d.StudentID='$target_id')
			          	LEFT JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
			          	LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
			        	WHERE 
			        		a.StudentID = '$target_id' 
			        		AND a.RecordStatus = 1 AND d.TransactionType=2 
			        		$conds ";
				$tmp_category_desc_list = $lu->returnVector($sql);
				$category_desc_list = array();
				for($k=0;$k<sizeof($tmp_category_desc_list);$k++)
				{
					$category_records = array();
					for($i=0;$i<sizeof($student_itemlist);$i++) {
						if($tmp_category_desc_list[$k] == $student_itemlist[$i]['Description']){
							$category_records[] = $student_itemlist[$i];
						}
					}
					if(sizeof($category_records)>0){
						$category_desc_list[] = $category_records;
					}
				}
			}else{
				// normal flow, items are group by student
				$category_desc_list = array();
				$category_desc_list[] = $student_itemlist;
			}
			
			for($n=0;$n<sizeof($category_desc_list);$n++)
			{
				$student_itemlist = $category_desc_list[$n];
				if(!empty($student_itemlist))
				{
					$RecordByRelatedTransactionID = array();
					for ($i=0; $i<sizeof($student_itemlist); $i++)
					{
						$RecordByRelatedTransactionID[$student_itemlist[$i]['RelatedTransactionID']]  = $student_itemlist[$i];
					}
					
					$total_record++;
					# start student
					$total_amount = 0;
					$list_table = "$table_header";
					
					foreach($RecordByRelatedTransactionID as $key => $recordAry)
					{
					     list($cat_name, $item_name, $item_desp,$item_paidtime, $item_amount, $ref_code) = $recordAry;
					     $total_amount += $item_amount;
					     $item_amount = '$'.number_format($item_amount,2);
					     $list_table .= "<tr><td class=\"eSporttdborder eSportprinttext\">$cat_name&nbsp;</td>";
					     $list_table .= "<td class=\"eSporttdborder eSportprinttext\">$item_name&nbsp;</td>";
					     $list_table .= "<td class=\"eSporttdborder eSportprinttext\">$item_desp&nbsp;</td>";
					     $list_table .= "<td class=\"eSporttdborder eSportprinttext\">$ref_code&nbsp;</td>";
					     if($sys_custom['ePayment']['PaymentMethod']){
					     	$payment_method = $recordAry[8];
					     	$receipt_remark = $recordAry[9];
					     	$list_table .= "<td class=\"eSporttdborder eSportprinttext\">".$sys_custom['ePayment']['PaymentMethodItems'][$payment_method]."&nbsp;</td>";
					     	$list_table .= "<td class=\"eSporttdborder eSportprinttext\">".nl2br($receipt_remark)."&nbsp;</td>";
					     }
					     $list_table .= "<td class=\"eSporttdborder eSportprinttext\">$item_paidtime&nbsp;</td>";
					     $list_table .= "<td class=\"eSporttdborder eSportprinttext\" align=\"center\">$item_amount</td></tr>\n";
					}
			
					$total_amount = '$'.number_format($total_amount,2);
					$list_table .= "<tr>";
					$list_table .= "<td class=\"eSporttdborder eSportprinttext\" colspan=\"$colspan\" align=\"right\">$i_Payment_Receipt_Payment_Total</td>";
					$list_table .= "<td class=\"eSporttdborder eSportprinttext\" align=\"center\">$total_amount</td></tr>\n";
					$list_table .= "</table>";
					$name_string = "$i_Payment_Receipt_ReceivedFrom $studentname $i_Payment_Receipt_PaidAmount$total_amount $i_Payment_Receipt_ForItems";
					
					// consolidate receipt number
					$ReceiptNumber = $ReceiptHead;
					$ReceiptNumber .= (trim($lu->ClassName) != "")? '-'.$lu->ClassName:'';
					$ReceiptNumber .= (trim($lu->ClassNumber) != "")? '-'.$lu->ClassNumber:'';
					$ReceiptNumber .= '-'.$StartingReceiptNumber;
					
					if ($j < (sizeof($target_students)-1) || ($sys_custom['ePayment']['ReceiptByCategoryDesc'] && $n < (sizeof($category_desc_list)-1)))
						$x .= '<div style="page-break-after:always">';
				
					$x .= "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"><tr><td>\n";
					$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class='tabletext'>\n";
					
					if($sys_custom['ePayment']['ReceiptByCategoryDesc'] && $student_itemlist[0]['Description'] != ''){
						$x .= '<br>
							<div align=center><font size=+2>'.$student_itemlist[0]['Description'].'</font></div><br>
							<div align=center><font size=+1>'.$ReceiptTitle.'</font></div><br>
							<br><br><br>';
					}else if (is_file("$intranet_root/templates/reportheader/paymentlist_header.php"))
					{
						ob_start();
					    include("$intranet_root/templates/reportheader/paymentlist_header.php");
					    $header_output = ob_get_clean();
					    $x .= $header_output;
					}
					$x .= "</td></tr></table>\n";
					if ($sys_custom['PaymentReceiptNumber']) {
						$x .= "<table width=100% border=0 cellspacing=0 cellpadding=0 >
									<tr>
										<td align=right>
											".$Lang['Payment']['ReceiptNumber'].": ".$ReceiptNumber."
										</td>
									</tr>
								</table>";
					}
					$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class='tabletext'>$name_string</td></tr></table>\n";
					$x .= "<br>";
					$x .= $list_table;
					$x .= "</td></tr></table>\n";
					$x .= "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\"><tr><td>\n";
					if (is_file("$intranet_root/templates/reportheader/paymentlist_footer.php"))
					{
						ob_start();	
					    include("$intranet_root/templates/reportheader/paymentlist_footer.php");
					    $footer_output = ob_get_clean();
					    $x .= $footer_output;
					}
					$x .= "</td></tr></table>\n";
					$x .= "</table>\n";
					
					if ($j < sizeof($target_students))
						$x .= "</div>";
					# End of 1 student
					
					if ($sys_custom['PaymentReceiptNumber']) {
						$StartingReceiptNumber++;
						switch (strlen($StartingReceiptNumber)) {
							case 0:
								$StartingReceiptNumber = '0001';
								break;
							case 1:
								$StartingReceiptNumber = '000'.$StartingReceiptNumber;
								break;
							case 2:
								$StartingReceiptNumber = '00'.$StartingReceiptNumber;
								break;
							case 3:
								$StartingReceiptNumber = '0'.$StartingReceiptNumber;
								break;
							default:
								break;
						}
					}
				}
			}
		}
		
		$StartingReceiptNumber += 0;
		if ($sys_custom['PaymentReceiptNumber']) {
			$lpayment->Save_Receipt_Number($StartingReceiptNumber);
		}
		
		if($total_record==0)
		{
			$x .= "<div align='center'>$i_no_record_exists_msg</div>";
		}
		
		return $x;
	}
	
	function generateReceiptFromTransactions($studentId, $TransactionLogIdAry)
	{
		global $PATH_WRT_ROOT, $intranet_root, $Lang, $sys_custom, $lpayment;
		global $i_no_record_exists_msg,$i_Payment_Receipt_Person_In_Charge, $i_Payment_Receipt_Payment_Category, $i_Payment_Receipt_Payment_Item, $i_Payment_Receipt_Payment_Description;
		global $i_Payment_Field_RefCode, $i_Payment_Receipt_Date, $i_Payment_Receipt_Payment_Amount,$i_Payment_Receipt_Payment_Total, $i_Payment_Receipt_ReceivedFrom, $i_Payment_Receipt_PaidAmount, $i_Payment_Receipt_ForItems;
		global $i_Payment_Receipt_SchoolChop,$i_Payment_Receipt_Date,$i_Payment_Receipt_ThisIsComputerReceipt,$i_Payment_Receipt_Remark,$i_Payment_Receipt_UpTo,$i_Payment_Receipt_AccountBalance;
		global $i_Payment_TransactionType_Credit, $i_Payment_TransactionType_Payment, $i_Payment_TransactionType_Purchase, $i_Payment_TransactionType_CancelPayment;
		global $i_Payment_TransactionType_TransferTo, $i_Payment_TransactionType_TransferFrom, $i_Payment_TransactionType_Refund, $i_Payment_PPS_Charge, $i_Payment_TransactionType_Other;
		global $i_Payment_Field_TransactionTime, $i_Payment_Field_TransactionType, $i_Payment_Field_TransactionDetail;
		
		if(!$lpayment){
			include_once('libpayment.php');
			$lpayment = new libpayment();
		}
		
		$no_balance = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];
		
		//debug_r($TransactionLogIdAry);
		$ReceiptTitle = $Lang['ePayment']['OfficialReceipt'];
		$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
		$school_name = $school_data[0];
		$school_org = $school_data[1];
		
		$current_academic_year_id = Get_Current_Academic_Year_ID();
		
		$name_field = getNameFieldWithClassNumberByLang("u.");
		$sql = "SELECT u.UserID,u.ClassName,u.ClassNumber,$name_field as StudentName,u.ChineseName,u.EnglishName,a.Balance FROM INTRANET_USER as u INNER JOIN PAYMENT_ACCOUNT as a ON a.StudentID=u.UserID WHERE u.UserID='$studentId'";
		$account_record = $lpayment->returnResultSet($sql);
		$studentname = $account_record[0]['StudentName'];
		$curr_balance = $account_record[0]['Balance'];
		
		$paramMap = array('UserID'=>$studentId, 'AcademicYearID'=>$current_academic_year_id, 'ClassName'=>$account_record[0]['ClassName'], 'ClassNumber'=>$account_record[0]['ClassNumber'], 'ChineseName'=>$account_record[0]['ChineseName'],
						 'EnglishName'=>$account_record[0]['EnglishName'], 'GeneratedBy'=>$_SESSION['UserID'], 'GeneratedDate'=>date('Y-m-d H:i:s'));
		$record_id = $lpayment->insertReceiptRecord($paramMap);
		
		$ReceiptNumber = sprintf("%08d", $record_id);
		
		$x = '<style type="text/css">';
		$x .= 'html, body, table, div, p {margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline;} 
			.container {font-family:新細明體,sans-serif;font-size:1em;}
			.title {font-size:1.2em;font-weight:bold;}
			.subtitle {font-size:1.1em;font-weight:bold;}
			.bold {font-weight:bold;}
			table {width:100%;border-collapse:collapse;border:0px none;margin:1em 0 1em 0;padding:0;}
			table th {font-weight:bold;background-color:#EEEEEE;}
			table.detail td, table.detail th {border: 1px solid #000000;padding:0.2em;}
			div{width:100%;margin:0;padding:0;}';
		$x .= '</style>';
		
		$sql = "SELECT 
					DATE_FORMAT(t.TransactionTime,'%Y-%m-%d %H:%i:%s') as TransactionTime,
               		CASE t.TransactionType
                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
                    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
                    WHEN 8 THEn '$i_Payment_PPS_Charge'
                    ELSE '$i_Payment_TransactionType_Other' END as TransactionType,
					t.Amount,";
		//			"IF(t.TransactionType IN (".implode(',',$lpayment->CreditTransactionType)."),".$lpayment->getWebDisplayAmountFormatDB("t.Amount").",'&nbsp;') as Credit,"
		//			"IF(t.TransactionType IN (".implode(',',$lpayment->DebitTransactionType)."),".$lpayment->getWebDisplayAmountFormatDB("t.Amount").",'&nbsp;') as Debit,"
        	$sql.= "IF(t.TransactionDetails IS NOT NULL,t.TransactionDetails,t.Details) as Details,
					t.BalanceAfter,
					t.RefCode 
				FROM PAYMENT_OVERALL_TRANSACTION_LOG as t 
				LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON t.RelatedTransactionID = c.TransactionID 
				LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON (t.TransactionType=2 and t.RelatedTransactionID=s.PaymentID and t.StudentID=s.StudentID) 
				LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON s.ItemID = i.ItemID
	          	LEFT JOIN PAYMENT_PAYMENT_CATEGORY as g ON g.CatID = i.CatID 
				WHERE t.LogID IN ('".implode("','",$TransactionLogIdAry)."')
				ORDER BY t.TransactionTime,t.LogID";
		$records = $lpayment->returnResultSet($sql);
		$record_count = count($records);
		//debug_r($records);
		$total_amount = 0;
		
		$table  = '<table width="100%" cellspacing="0" cellpadding="2" class="detail">';
		$table .= '<tr><th>'.$i_Payment_Field_TransactionTime.'</th>';
		$table .= '<th>'.$i_Payment_Field_TransactionType.'</th>';
		$table .= '<th>'.$i_Payment_Field_TransactionDetail.'</th>';
		$table .= '<th>'.$i_Payment_Field_RefCode.'</th>';
		$table .= '<th>'.$i_Payment_Receipt_Payment_Amount.'</th>';
		$table .= '</tr>';
		
		for($i=0;$i<$record_count;$i++){
			
			$total_amount += $records[$i]['Amount'];
			
			$table .= '<tr>';
				$table .= '<td>'.$records[$i]['TransactionTime'].'</td>';
				$table .= '<td>'.$records[$i]['TransactionType'].'</td>';
				$table .= '<td>'.nl2br(htmlspecialchars(str_replace(array('<br>','<br />'),"\n",$records[$i]['Details']))).'</td>';
				$table .= '<td>'.Get_String_Display($records[$i]['RefCode']).'</td>';
				$table .= '<td align="center">'.$lpayment->getWebDisplayAmountFormat($records[$i]['Amount']).'</td>';
			$table .= '</tr>';
		}
		
		$total_amount = $lpayment->getWebDisplayAmountFormat($total_amount);
		$table .= '<tr><td colspan="4" align="right">'.$i_Payment_Receipt_Payment_Total.'</td><td align="center">'.$total_amount.'</td></tr>';
		
		$table .= '</table>';
		
		$x .= '<div class="container">';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
			$x .= '<tr><td class="title" align="center">'.$school_name.'</td></tr>';
		$x .= '</table>';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
			$x .= '<tr><td class="title" align="center">'.getCurrentAcademicYear().'</td></tr>';
		$x .= '</table>';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
			$x .= '<tr><td class="subtitle" align="center">'.$ReceiptTitle.'</td></tr>';
		$x .= '</table>';
		/*
		if (is_file("$intranet_root/templates/reportheader/paymentlist_header.php"))
		{
			ob_start();
		    include("$intranet_root/templates/reportheader/paymentlist_header.php");
		    $header_output = ob_get_clean();
		    $x .= $header_output;
		}
		*/
		
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="right">'.$Lang['Payment']['ReceiptNumber'].": ".$ReceiptNumber.'</td>
					</tr>
				</table>';
		
		
		$name_string = "$i_Payment_Receipt_ReceivedFrom $studentname $i_Payment_Receipt_PaidAmount$total_amount $i_Payment_Receipt_ForItems";
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td class="tabletext">'.$name_string.'</td></tr></table>';
		
		$x .= $table;
		
		$x .= '<br /><br />';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
			$x .= '<tr><td align="right" style="white-space:pre;">'.$i_Payment_Receipt_Date.': <u>'.str_repeat(' ',8).date('Y-m-d').str_repeat(' ',8).'</u></td></tr>';
		$x .= '</table>';
		
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		$x .= '<tr><td align="center" width="10%"><span style="letter-spacing:-1px;">'.str_repeat('_',10).str_repeat('_',mb_strlen($i_Payment_Receipt_SchoolChop)).str_repeat('_',10).'</span></td><td width="80%">&nbsp;</td></tr>';
		$x .= '<tr><td align="center" width="10%">'.$i_Payment_Receipt_SchoolChop.'</td><td width="80%">&nbsp;</td></tr>';
		$x .= '</table>';
		
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		$x .= '<tr><td align="left">'.$i_Payment_Receipt_ThisIsComputerReceipt.'</td></tr>';
		$x .= '</table>';
		
		if(!$no_balance)
		{
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
			$x .= '<tr><td align="left" class="bold">'.$i_Payment_Receipt_Remark.': '.$i_Payment_Receipt_UpTo.' '.date('Y-m-d').' '.$i_Payment_Receipt_AccountBalance.': '.$lpayment->getWebDisplayAmountFormat($curr_balance).'</td></tr>';
			$x .= '</table>';
		}
		/*
		if (is_file("$intranet_root/templates/reportheader/paymentlist_footer.php"))
		{
			ob_start();	
		    include("$intranet_root/templates/reportheader/paymentlist_footer.php");
		    $footer_output = ob_get_clean();
		    $x .= $footer_output;
		}
		*/
		$x .= '</div>';
		
		$paramMap['ReceiptNumber'] = $ReceiptNumber;
		$paramMap['ReceiptContent'] = $x;
		$lpayment->updateReceiptRecord($record_id, $paramMap);
		
		return $record_id;
	}

    function Get_Subsidy_Identity_Import_confirm($data, $format)
    {
        global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $lpayment;

        $PAGE_NAVIGATION[] = array($Lang['Btn']['Import']);

        # step information
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 1);
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);

        $x = '<br/>
            <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>'.$this->GET_NAVIGATION($PAGE_NAVIGATION).'</td>
                </tr>
                <tr>
                    <td>'.$this->GET_STEPS($STEPS_OBJ).'</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <table width="90%" border="0" cellpadding="3" cellspacing="0">
                <tr>
                    <td class="formfieldtitle" width="30%" align="left">'.$Lang['General']['TotalRecord'].'</td>
                    <td class="tabletext">'.sizeof($data).'</td>
                </tr>';
        if (sizeof($data) > 0) {
            list($ErrorArray, $WarningCount) = $lpayment->Import_Subsidy_Identity_Record_To_Temp($data, $format);

            $x .= '
					<tr>
						<td class="formfieldtitle" width="30%" align="left">'.$Lang['General']['SuccessfulRecord'].'</td>
						<td class="tabletext">'.(sizeof($data)-sizeof($ErrorArray)).'</td>
					</tr>
					<tr>
						<td class="formfieldtitle" width="30%" align="left">'.$Lang['General']['FailureRecord'].'</td>
						<td class="tabletext">'.(sizeof($ErrorArray) - $WarningCount).'</td>
					</tr>
					<tr>
						<td class="formfieldtitle" width="30%" align="left">'.$Lang['General']['WarningRecord'].'</td>
						<td class="tabletext">'.$WarningCount.'</td>
					</tr>
				</table>
				';

            $x .= '<table width="95%" border="0" cellpadding="3" cellspacing="0">
						<tr>
							<td class="tablebluetop tabletopnolink" width="10">#</td>
							<td class="tablebluetop tabletopnolink">'.$Lang['ePayment']['SubsidyIdentityCode'].'</td>';
            if($format == '2') {
                $x .= '<td class="tablebluetop tabletopnolink">'.$Lang['General']['UserLogin'].'</td>';
            } else {
                $x .= '<td class="tablebluetop tabletopnolink">'.$Lang['SysMgr']['FormClassMapping']['ClassTitle'].'</td>
				   <td class="tablebluetop tabletopnolink">'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</td>';
            }

            $x .= '<td class="tablebluetop tabletopnolink">'.$Lang['General']['Name'].'</td>';

            $x .= '<td class="tablebluetop tabletopnolink">'.$Lang['General']['Error'].'/'.$Lang['General']['Warning'].'</td>
						</tr>';

            for ($i=0; $i< sizeof($ErrorArray); $i++) {
                $css_i = ($i % 2) ? "2" : "";
                $x .= '
							<tr>
								<td valign="top" class="tablebluerow'.$css_i.'" width="10">
									'.$ErrorArray[$i]['RowNumber'].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$ErrorArray[$i]['RecordDetail'][0].'
								</td>
								<td valign="top" class="tablebluerow'.$css_i.'">
									'.$ErrorArray[$i]['RecordDetail'][1].'
								</td>';
                if($format == '1') {
                    $x .= '<td valign="top" class="tablebluerow'.$css_i.'">
									'.$ErrorArray[$i]['RecordDetail'][2].'
								</td>
							<td valign="top" class="tablebluerow'.$css_i.'">
									'.$ErrorArray[$i]['RecordDetail'][3].'
								</td>';
                } else {
                    $x .= '<td valign="top" class="tablebluerow'.$css_i.'">
									'.$ErrorArray[$i]['RecordDetail'][2].'
								</td>';
                }

                $x .= '<td valign="top" class="tablebluerow'.$css_i.'">
									<font style="color:red;">'.$ErrorArray[$i]['ErrorMsg'].'</font>
									<font style="color:orange;">'.$ErrorArray[$i]['WarnMsg'].'</font>
								</td>
							</tr>';
            }
            $x .= '</table>';

            $x .= '
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td class="dotline"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center">';
            if ((sizeof($ErrorArray) - $WarningCount)== 0)
                $x .= $this->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "window.location='import_update.php';");

            $x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
            $x .= '</td>
				</tr>
				</table>';
        } else {
            $x .= '</table>';

            $x .= '
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td class="dotline"><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center">';
            $x .= $this->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import.php';");
            $x .= '</td>
				</tr>
				</table>';
        }

        return $x;
    }

    function Get_Subsidy_Identity_Import_Finish_Page()
    {
        global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang, $lpayment;

        $Result = $lpayment->Finalize_Import_Subsidy_Identity();

        $PAGE_NAVIGATION[] = array($Lang['Btn']['Import']);

        # step information
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
        $STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 1);


        $x .= '<br />
					<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>'.$this->GET_NAVIGATION($PAGE_NAVIGATION).'</td>
						</tr>
						<tr>
							<td>'.$this->GET_STEPS($STEPS_OBJ).'</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="center">
							'.$Result.' '.$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'].'
							</td>
						</tr>
						<tr>
							<td class="dotline">
								<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" />
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="center">
								'.$this->GET_ACTION_BTN($Lang['Btn']['Back'],"button","window.location='index.php';").'
							</td>
						</tr>
					</table>';

        return $x;
    }
}
?>