<?php
// Modifing by : 

##########################################################################
# Modification Log:
# 2020-01-17 (Tommy) added checkColumnIsExist() to check column is exist in table
# 2019-07-18 (Bill) modified getCacheResult(), setCacheResult(), isCacheResultExist(), to remove ';' to prevent eval() security problem
# 2019-05-16 (Bill) modified db_db_query(), added logic to store query info to INTRANET_QUERY_LOG - for sql without single & double quotes  [disabled]  ($QueryDetailsLogMode = true)
# 2019-05-13 (Cameron) enclosed var with apostrophe in UpdateRole_UserGroup()
# 2019-04-26 (Carlos) Modified Get_Safe_Sql_Query($value) to support array. Useful for escaping csv of non-number values, e.g. " ... ClassName IN ('".implode("','",array("1A","1B"))."') ..." .
# 2018-09-28 (Henry) Modified db_sub_select() to remove empty value
# 2017-10-23 (Carlos) Modified db_db_query(), $sys_custom['QueryLog'] store the query and elapsed time for logging.
# 2017-08-30 (Carlos) [ip.2.5.8.10.1] Added isMagicQuotesOn() to check whether posted values would auto added slashes to escape quotes.
# 2016-09-05 (Pun)	[ip.2.5.7.10.1] - Added getSessionKeyLastUpdatedFromUserId(), updateSessionByUserId(), updateSessionLastUpdatedByUserId()
# 2015-12-14 (Bill)	[ip.2.5.7.1.1] - modified validateEmail(), validateURL(), convertURLS(), convertURLS2(), convertMail(), convertMail2(), for PHP 5.4
# 2015-12-14 (Carlos) [ip.2.5.7.1.1] - added opendb($db_host, $db_user, $db_password, $db_name) and closedb() for connect to another server's db.
# 2015-12-11 (Omas) [ip.2.5.7.1.1]
# modified validateLogin() - replace ereg() by preg_match() for PHP5.4
# 2012-05-09 (Marcus)
# new function Reorder_Display_Order(), Reorder DisplayOrder of records in a single query, for better performance
# 2011-11-26 (fai)
# new function concatFieldValueToSqlStr() concat Associative Array to a pair of field and value
# 2011-10-14 (fai)
# new function returnResultSet() Make use of function returnArray() with default return Associative Array only
# 2011-08-09 (Ivan)
# modified returnArray() and db_fetch_array(), added para $ReturnAssoOnly to return array with associate key only to save memory usage
# 2011-07-08 (Marcus)
# modified db_db_query, add find query location coding in order to display the flow of calling the query.
# 2011-02-23 (Ivan)"
# moved function pack_value() from iPf libraries to this library for common use
# 2010-10-23 (FAI):
# enhance log the queries executed for debug mode : export SQL to a log file in the /tmp directory , according to the login user ID
# 2010-10-08 (Yuen):
# log the queries executed for debug mode
# 2010-03-15 (Kenneth):
# Get_Safe_Sql_Query(). add one more criteria for save query "\", and convert to "\\"
# 2010-03-15 (Kenneth):
# Get_Safe_Sql_Like_Query(). replace (',%,_,\) to \',\%,\_,\\ for LIKE operation
# 2007-09-20 (Kenneth):
# validateLogin(). Add $special_option['login_uppercase'] to allow uppercase login name
# 2007-03-14 (Kenneth):
# validateLogin(). Changed to accept $special_option['login_first_num'] to allow using numerics as first char of user login
# 2005-07-05 (Kenneth):
# db_create_db(), db_db_query()  Modify to use in MySQL 4.1 or later
##########################################################################

if (!defined("LIBDB_DEFINED"))                     // Preprocessor directive
{
define("LIBDB_DEFINED", true);

class libdb {
        var $sql;
        var $db;
        var $rs;
        var $cacheResultAry;

        function libdb(){
                global $intranet_db;
                $this->db = $intranet_db;
                $this->cacheResultAry = array();
        }

        function db_insert_id(){
                return mysql_insert_id();
        }

        function db_num_rows(){
                return mysql_num_rows($this->rs);
		
        }

        function db_create_db($database){
                $sql = 'CREATE DATABASE `'.$database.'`';
                return mysql_query($sql);
#                return mysql_create_db($database);
        }
        
        function get_microtime_ms(){
        	list($usec, $sec) = explode(" ", microtime());
        	return round(1000 * ((float)$usec + (float)$sec));
        }

        function db_db_query($query)
        {
                // global $DebugMode, $QueryDetailsLogMode, $sys_custom;
                global $DebugMode, $sys_custom;
                
                if($sys_custom['QueryLog'])
                {
                	global $GLOBAL_LOG_QUERY;
                	
                	$start_mtime = microtime();
					$start_mtime = explode(" ",$start_mtime);
					$start_mtime = $start_mtime[1] + $start_mtime[0];
                }
                $query_start = $this->get_microtime_ms();
                
                mysql_select_db($this->db) or exit(mysql_error());
                
                # show sql statement if failed and debug mode is ON
				$q_result = mysql_query($query);
				
				if($sys_custom['QueryLog'])
				{
					$end_mtime = microtime();
					$end_mtime = explode(" ",$end_mtime);
					$end_mtime = $end_mtime[1] + $end_mtime[0];
					
					$elapsed_time = $end_mtime - $start_mtime;
					$GLOBAL_LOG_QUERY[] = array(base64_encode($query),$elapsed_time);
				}
				
				if ($DebugMode)
				{
					$query_end = $this->get_microtime_ms();
					$query_time = $query_end - $query_start;
					
					// find query location
					$query_info = debug_backtrace();
					
					$loc = '<a href="javascript:void(0);" onclick="$(\'#query_info'.$GLOBALS[debug][db_query_count].'\').toggle();">[detail]</a> ';
					$loc .= "<div style='display:none;' id='query_info".$GLOBALS[debug][db_query_count]."'>";
					
					$j=1;
					$queryInfo = '';
					for($i=sizeof($query_info)-1; $i>=0; $i--)
					{
						$func = $query_info[$i];
						
						$queryInfo .= $j.". ".$func['file'].": ";
//						if($func['class'])
//						$loc .= $func['class']."->";
						$queryInfo .= $func['function'];
						$queryInfo .= "(line:". $func['line'].")<br>";
						
						$j++;
					}
					$loc .= $queryInfo;
					$loc .= "</div>";
					// find query location end
					
					$GLOBALS[debug][db_query_time] += $query_time;
					$GLOBALS[debug][log] .= '<tr><td style="vertical-align:top;">'.$query_time.'ms</td><td style="vertical-align:top;">'.$query.' '.$loc.'</td></tr>'."\n";
					$GLOBALS[debug][log_file] .= "==> {$query_time}ms::{$query}\n\n";
					$GLOBALS[debug][db_query_count] ++;
					$GLOBALS[debug][db_query_arr][$query]['count']++;
					$GLOBALS[debug][db_query_arr][$query]['details'][] = $queryInfo;
				}
				
				if(false)
				{
				    # Log Query without single & double quotes
    				if(strpos($query, "'") !== false || strpos($query, '"') !== false || strpos($query, ".INTRANET_QUERY_LOG") !== false)
    				{
    				    // do nothing
    				}
    				else
    				{
    				    $tempQuery = strtolower($query);
    				    if(strpos($tempQuery, "where") !== false || strpos($tempQuery, 'insert into') !== false || strpos($tempQuery, 'update') !== false || strpos($tempQuery, 'delete') !== false)
    				    {
    				        global $intranet_db;
    				        
    				        /* 
        				    for($i=sizeof($queryInfo)-1; $i>=0; $i--)
        				    {
        				        $thisQueryInfo = $queryInfo[$i];
        				        $logSQL = " INSERT INTO ".$intranet_db.".INTRANET_QUERY_LOG 
                                                (Script, ScriptFunction, Query, CurrentAddress, InputBy, DateInput)
                                            VALUES
                                                ('".$this->Get_Safe_Sql_Query($thisQueryInfo['file'])."', '".$this->Get_Safe_Sql_Query($thisQueryInfo['function'])."',
                                                    '".$this->Get_Safe_Sql_Query($query)."', '".getRemoteIpAddress()."', '".$_SESSION['UserID']."', NOW())";
        				        $this->db_db_query($logSQL);
        				    }
        				    */
    				        
    				        $queryInfo = debug_backtrace();
    				        if(sizeof($queryInfo) > 1)
    				        {
    				            $targetBase = 1;
    				            if($queryInfo[1]['class'] == 'libdb') {
    				                $targetBase++;
    				            }
    				            
    				            $targetScript = $queryInfo[$targetBase]['file'];
    				            $targetScriptFunction = $queryInfo[($targetBase + 1)]['function'] != ''? $queryInfo[($targetBase + 1)]['function'] : '';
    				        }
    				        else if(sizeof($queryInfo) == 1)
    				        {
    				            // Call db_db_query() directly
    				            $targetScript = $queryInfo[0]['file'];
    				            $targetScriptFunction = '';
    				        }
    				        
    				        if($targetScript != '')
    				        {
    				            $logSQL = " INSERT INTO ".$intranet_db.".INTRANET_QUERY_LOG
                                                (RequestUrl, Script, ScriptFunction, Query, CurrentAddress, InputBy, DateInput)
                                            VALUES
                                                ('".$this->Get_Safe_Sql_Query($_SERVER["REQUEST_URI"])."', '".$this->Get_Safe_Sql_Query($targetScript)."', '".$this->Get_Safe_Sql_Query($targetScriptFunction)."',
                                                    '".$this->Get_Safe_Sql_Query($query)."', '".getRemoteIpAddress()."', '".$_SESSION['UserID']."', NOW())";
    				            $this->db_db_query($logSQL);
    				        }
    				    }
    				}
				}
				
				//echo ("<br />\nTIME: {$query_time}ms<br />\nQUERY: $query");
				//echo ("<tr><td>{$query_time}ms</td><td>$query</td></tr>\n");								
				//echo ("\n{$query_time}ms\t{$query}");
				
				//debug($GLOBALS[debug][db_query_time], $GLOBALS[debug][db_query_count]);
				
				/*
				if (!$q_result)
				{
					//$this->add2LoggingTable($query);
					//echo "<!--\n".$query."\n-->\n";
				}
				*/
				
				return $q_result;
                //return mysql_query($query);
				//return mysql_db_query($this->db, $query);
        }
        
        function db_show_debug_log(){
        	echo '<font face="arial">' .
        			'In query view, this page takes '.$GLOBALS[debug][db_query_time].'ms by '.$GLOBALS[debug][db_query_count].' queries.</font>' .
				'<table style="border:1px #666666 solid" border="0" cellpadding="10" cellspacing="4">';
			echo $GLOBALS[debug][log];
			echo '</table>';

			$logFile = "/tmp/".$_SESSION['UserID'].".log";
			error_log("\n====s:  ".date("Y-m-d H:i:s")." ===\n".$GLOBALS[debug][log_file]."\n====e :".date("Y-m-d H:i:s")."===\n", 3, $logFile);
        }
        
        function db_show_debug_log_by_query_number($minOccurance=1) {
        	$sqlAry = $GLOBALS[debug][db_query_arr];
        	
        	$maxOccurance = 0;
        	foreach ((array)$sqlAry as $_query => $_queryInfoAry) {
        		$_queryCount = $_queryInfoAry['count'];
        		
        		if ($_queryCount > $maxOccurance) {
        			$maxOccurance = $_queryCount; 
        		}
        	}
        	
        	$x = '';
        	$x .= '<table style="border:1px #666666 solid" border="0" cellpadding="10" cellspacing="4">';
        		$counter = 0;
        		for ($i=$maxOccurance; $i>=$minOccurance; $i--) {
        			foreach ((array)$sqlAry as $__query => $__queryInfoAry) {
		        		$__queryCount = $__queryInfoAry['count'];
		        		
		        		if ($__queryCount == $i) {
		        			$__queryDetailsAry = $__queryInfoAry['details'];
		        			$x .= '<tr>';
								$x .= '<td style="vertical-align:top;">'.$__queryCount.'</td>';
								$x .= '<td style="vertical-align:top;">';
									$x .= $__query;
									$x .= '<br>';
									$x .= '<a href="javascript:void(0);" onclick="$(\'#query_info_count_'.$counter.'\').toggle();">[detail]</a> ';
									$x .= '<div style="display:none;" id="query_info_count_'.$counter.'">';
										$x .= implode('<br><hr><br>', (array)$__queryDetailsAry);
									$x .= '</div>';
								$x .= '</td>';
							$x .= '</tr>';
							$counter++;
		        		}
		        	}
        		}
        		
	        	if ($counter == 0) {
	        		$x .= '<tr><td>There is no query run '.$minOccurance.' time(s) or more</td></tr>';
	        	}
        	$x .= '</table>';

        	echo $x;
        }

        function db_data_seek($row_number){
                return mysql_data_seek($this->rs, $row_number);
        }

        function db_fetch_array($ResultArrayType=0){
        		switch ($ResultArrayType) {
        			case 1:
        				$result_type = MYSQL_ASSOC;
        				break;
        			case 2:
        				$result_type = MYSQL_NUM;
        				break;
        			default:
        				$result_type = MYSQL_BOTH;
        		}
        		
                return mysql_fetch_array($this->rs, $result_type);
        }

        function db_free_result(){
                return mysql_free_result($this->rs);
        }

        function db_affected_rows(){
                 return mysql_affected_rows();
        }

        ######################################################################################

        /*
        function returnArray($sql, $field_no=null){
                $i = 0;
                $this->rs = $this->db_db_query($sql);
                if($this->rs && $this->db_num_rows()!=0)
                {
                        while($row = $this->db_fetch_array())
                        {
                                # if no field number given, count all fields
                                if ($field_no==null)
                                {
                                        $field_no = sizeof($row);
                                }
                            for($k=0; $k<$field_no; $k++)
                            $x[$i][$k] = $row[$k];
                            $i++;
                        }
                }
                if ($this->rs)
                {
                    $this->db_free_result();
                }

                return $x;
        }
        */

		/**
		* Make use of function returnArray() with default return Associative Array only
		* @owner : Fai (20111014)
		* @param : String $sql SQL that need to execute , 
		* @param : Int $ResultArrayType , Optional variable , control return Associative Array or not.  0--> Return Both Index and Associative array , 1--> Return Associative Array only , 
		* @return : Array, DB resultset of the SQL 
		* 
		*/
		function returnResultSet($sql,$ResultArrayType=1){
			return $this->returnArray($sql,null,$ResultArrayType);
		}

        function returnArray($sql, $field_no=null, $ResultArrayType=0){
			$i = 0;
			
			$this->rs = $this->db_db_query($sql);
			$x = array();
			if ($this->rs && $this->db_num_rows()!=0)
			{				while ($row = $this->db_fetch_array($ResultArrayType))
				{
					$x[] = $row;
				}
				$this->db_free_result();
			}
			return $x;
		}

        #########################################################################
        # return 1-D array to store the sql result of selecting 1 column only
        #
        function returnVector($sql){
                $i = 0;
                $x = array();
                $this->rs = $this->db_db_query($sql);
                if($this->rs && $this->db_num_rows()!=0){
                        while($row = $this->db_fetch_array()){
                                $x[$i] = $row[0];
                                $i++;
                        }
                }
                if ($this->rs)
                    $this->db_free_result();
                return $x;
        }


        function db_sub_select($sql){
                $x = "";
                $row = $this->returnArray($sql,1);
                if (!$row) return 0;
                $delimiter = "";
                for($i=0; $i<sizeof($row); $i++)
                {
                	if($row[$i][0] != ""){
	                    $x .= $delimiter.$row[$i][0];
	                    $delimiter = ",";
                	}
                }
                if ($x == "") return 0;
                else return $x;
        }

        ######################################################################################
        # utility functions
        # New function
        function validateLogin($x) {
                 global $system_reserved_account;
                 if (is_array($system_reserved_account))
                 {
                     if (in_array($x,$system_reserved_account))
                     {
                         return false;
                     }
                 }
                 global $special_option;
                 if ($special_option['login_uppercase'])
                 {
                     $additional_chars = "A-Z";
                 }
                 else
                 {
                     $additional_chars = "";
                 }

                 if ($special_option['login_hypen'] && $special_option['login_first_num'])
                 {
                     $myRE="^[_a-z".$additional_chars."0-9-]{0,30}$";
                 }
                 else if ($special_option['login_hypen'])
                 {
                     $myRE="^[a-z".$additional_chars."]([_a-z".$additional_chars."0-9-]{0,30})$";
                 }
                 else if ($special_option['login_first_num'])
                 {
                     $myRE="^[_a-z".$additional_chars."0-9]{0,30}$";
                 }
                 else
                 {
                     $myRE="^[a-z".$additional_chars."]([_a-z".$additional_chars."0-9]{0,30})$";
                 }

                 //  ^[a-z]([_a-z0-9]{0,30})$
                 //  ^[a-z]([_a-z0-9]{0,30})$
                 return preg_match('/'.$myRE.'/',$x);
                 #return eregi("^[_a-z0-9]+$", $x);
        }
        /*
        function validateLogin($x) {
                 global $system_reserved_account;
                 if (is_array($system_reserved_account))
                 {
                     if (in_array($x,$system_reserved_account))
                     {
                         return false;
                     }
                 }
                 global $special_option;
                 if ($special_option['login_hypen'])
                 {
                     $myRE="^[a-z]([_a-z0-9-]{0,30})$";
                 }
                 else
                 {
                     $myRE="^[a-z]([_a-z0-9]{0,30})$";
                 }
                 return ereg($myRE,$x);
                 #return eregi("^[_a-z0-9]+$", $x);
        }
        */

        function validateEmail($email) {
                //return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
                // replace by preg_match() for PHP 5.4
                //return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z0-9-]{2,3})$", $email);
                return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z0-9-]{2,3})$/i", $email);
        }

        function validateURL($url) {
        		// replace by preg_match() for PHP 5.4
                //return eregi("^((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$", $url);
                return preg_match("/^((http|ftp|https):\/\/)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((\/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)$/i", $url);
        }

        function convertURLS($text) {
        		// replace by preg_replace() for PHP 5.4
                //$text = eregi_replace("((ht|f)tp://www\.|www\.)([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((/|\?)[a-z0-9~#%&\\/'_\+=:\?\.-]*)*)", "http://www.\\3", $text);
                //$text = eregi_replace("((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)", "<a class=tableContent href=\"\\0\" target=_blank>\\0</a>", $text);
                $text = preg_replace("/((http|ftp|https):\/\/)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((\/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)/i", "<a class=tableContent href=\"\\0\" target=_blank>\\0</a>", $text);

				# new code but not tested, to be confirmed
				# $text = preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', '<a href="$1" target="_blank">$1</a>', $text);

                return $text;
        }

        function convertMail($text) {
        		// replace by preg_replace() for PHP 5.4
                //$text = eregi_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))", "<a class=tableContent href='mailto:\\0'>\\0</a>", $text);
                $text = preg_replace("/([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))/i", "<a class=tableContent href='mailto:\\0'>\\0</a>", $text);
                return $text;
        }

        function convertAllLinks($text) {
                $text = $this->convertURLS($text,$num=0);
                $text = $this->convertMail($text,$num=0);
                return $text;
        }

        function convertURLS2($text) {
        /* Commented by Kenneth: use new regular expression to fit URL w/ port number
                $text = eregi_replace("((ht|f)tp://www\.|www\.)([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})((/|\?)[a-z0-9~#%&\\/'_\+=:\?\.-]*)*)", "http://www.\\3", $text);
                $text = eregi_replace("((ht|f)tp://)((([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))|(([0-9]{1,3}\.){3}([0-9]{1,3})))((/|\?)[a-z0-9~#%&'_\+=:\?\.-]*)*)", "<a href=\"\\0\" target=_blank>\\0</a>", $text);
                */
        		// replace by preg_replace() for PHP 5.4
                //$text = ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]","<a target=_blank href=\"\\0\">\\0</a>", $text);
                $text = preg_replace("/[[:alpha:]]+:\/\/[^<>[:space:]]+[[:alnum:]\/]/","<a target=_blank href=\"\\0\">\\0</a>", $text);
                return $text;
        }

        function convertMail2($text) {
        		// replace by preg_replace() for PHP 5.4
                //$text = eregi_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))", "<a href='mailto:\\0'>\\0</a>", $text);
                $text = preg_replace("/([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3}))/i", "<a href='mailto:\\0'>\\0</a>", $text);
                return $text;
        }

        function convertAllLinks2($text,$count="") {
                 $text = intranet_undo_htmlspecialchars($text);
                 $ord = $text;
                $text = $this->convertURLS2($text);
                $text = $this->convertMail2($text);

                if ($count == "") $count = 30;

                if ($ord == $text)
                {
                    return intranet_wordwrap($text,$count,"\n",1);
                }

                $pos = strpos($text, ">http");
                while ($pos)
                {
                       $pos1 = $pos;
                       $pos2 = strpos($text, "</a>", $pos);
                       $inside = substr($text,$pos1+1,$pos2-$pos1-1);
                       $inside = chopword($inside,$count);
                       $text = substr($text,0,$pos1+1).$inside.substr($text,$pos2);
                       $pos = strpos($text,">http",$pos+strlen($inside));
                }
                return $text;
        }


        ######################################################################################
        # Special Function for update default role in user group table

        function UpdateRole_UserGroup(){
                $sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE RoleID IS NULL GROUP BY GroupID";
                $row = $this->returnArray($sql,1);
                for($i=0; $i<sizeof($row); $i++){
                        $GroupID = $row[$i][0];
                        $sql = "SELECT a.RoleID FROM INTRANET_ROLE AS a, INTRANET_GROUP AS b WHERE a.RecordType = b.RecordType AND a.RecordStatus = 1 AND b.GroupID = '$GroupID'";
                        $Role = $this->returnArray($sql,1);
                        $RoleID = $Role[0][0];
                        $sql = "UPDATE INTRANET_USERGROUP SET RoleID = '$RoleID' WHERE RoleID IS NULL AND GroupID = '$GroupID'";
                        $this->db_db_query($sql);
                }
        }

        # compare date in timestamp
        # if a > b, return 1
        # a = b, return 0
        # a < b, return -1
        function compareDate ($a, $b)
        {
                 $day_a = getdate($a);
                 $day_b = getdate($b);

                 if ($day_a['year'] > $day_b['year'])
                 {
                     return 1;
                 }
                 else if ($day_a['year'] < $day_b['year'])
                 {
                      return -1;
                 }
                 else if ($day_a['mon'] > $day_b['mon'])
                 {
                      return 1;
                 }
                 else if ($day_a['mon'] < $day_b['mon'])
                 {
                      return -1;
                 }
                 else if ($day_a['mday'] > $day_b['mday'])
                 {
                      return 1;
                 }
                 else if ($day_a['mday'] < $day_b['mday'])
                 {
                      return -1;
                 }
                 else return 0;
        }

        function generateSessionKey()
        {
                 $original = session_id().time();
                 $hash_value = md5($original);
                 return $hash_value;
        }

        function updateSession($key)
        {
                 global $UserID;
                 $this->updateSessionByUserId($key, $UserID);
        }
        
        function updateSessionByUserId($key, $userId)
        {
                 $sql = "UPDATE INTRANET_USER SET SessionKey = '$key', SessionLastUpdated = now() WHERE UserID = '$userId'";
                 return $this->db_db_query($sql);
        }
        
        function updateSessionLastUpdatedByUserId($userId)
        {
                 $sql = "UPDATE INTRANET_USER SET SessionLastUpdated = now() WHERE UserID = '$userId'";
                 return $this->db_db_query($sql);
        }

        function getSessionKeyLastUpdatedFromUserId($UserID)
        {
                 global $session_expiry_time;
                 $limit = $session_expiry_time * 60;
                 $sql = "SELECT SessionKey, SessionLastUpdated FROM INTRANET_USER WHERE UserID = '$UserID' AND UNIX_TIMESTAMP(SessionLastUpdated)+$limit>=UNIX_TIMESTAMP(now())";
                 $rs = $this->returnResultSet($sql);
                 if (sizeof($rs)==1)
                     return $rs[0];
                 else return array('SessionKey' => '', 'SessionLastUpdated' => '');
        }
        
        function getLoginFromSessionKey($key)
        {
                 global $session_expiry_time;
                 $limit = $session_expiry_time * 60;
                 $sql = "SELECT UserLogin FROM INTRANET_USER WHERE SessionKey = '$key' AND UNIX_TIMESTAMP(SessionLastUpdated) + $limit > UNIX_TIMESTAMP(now())";
                 $rs = $this->returnVector($sql);
                 if (sizeof($rs)==1)
                     return $rs[0];
                 else return "";
        }
        function getEmailFromSessionKey($key)
        {
                 global $session_expiry_time;
                 $limit = $session_expiry_time;
                 $sql = "SELECT UserEmail FROM INTRANET_USER WHERE SessionKey = '$key' AND UNIX_TIMESTAMP(SessionLastUpdated) + $limit > UNIX_TIMESTAMP(now())";
                 $rs = $this->returnVector($sql);
                 if (sizeof($rs)==1)
                     return $rs[0];
                 else return "";
        }
        # For eClass Login
        function getEmailFromSessionKeyForeClass($key)
        {
                 global $eclass_linkup_time;
                 $limit = ($eclass_linkup_time==0? 30: $eclass_linkup_time);
                 $sql = "SELECT UserEmail FROM INTRANET_USER WHERE SessionKey = '$key' AND UNIX_TIMESTAMP(SessionLastUpdated) + $limit > UNIX_TIMESTAMP(now())";
                 $rs = $this->returnVector($sql);
                 if (sizeof($rs)==1)
                     return $rs[0];
                 else return "";
        }

        # Special Function for checking number of license used
        function getLicenseUsed()
        {
        	global $intranet_db;

             $license_sql =        "SELECT
                                     	count(*) as number_license_used
                                    FROM
                                      	{$intranet_db}.INTRANET_USER
                                    WHERE
                                      	RecordType = '2'
                                   		AND ( RecordStatus = '0' or RecordStatus = '1' )
                                ";

             $license_used = $this->db_db_query($license_sql);
             $license_used = mysql_result($license_used , 0, 'number_license_used');
             return $license_used;
        }
        
        # Modified by Key (2008-10-21)
        # Function for add problematic query to DB logging table
        # Modified by Key (2008-11-12)
        # Change the log tables from DB to txt file
        # Modified by Key (2008-11-25)
        # Change the log tables txt file into a folder & generated by date
        function add2LoggingTable($query)
        {
	        global $intranet_db, $intranet_root;
	        
	        // intranet
	        $UserID = $_SESSION["UserID"];
	        $CourseID = "";
	         
	        // eclass
	        //$UserID = $_SESSION["ck_intranet_user_id"];
	        //$CourseID = $_SESSION["ck_course_id"];
	        
	        $IPAddress = $_SERVER['REMOTE_ADDR'];
	        $SourcePath = $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	        $ErrorMsg = mysql_errno()." : ".mysql_error();
	        
	        $ErrorMsg = htmlspecialchars($ErrorMsg, ENT_QUOTES);
	        $query = htmlspecialchars($query, ENT_QUOTES);
	        $query = str_replace("\n", "\t", $query);
	        $SourcePath = htmlspecialchars($SourcePath, ENT_QUOTES);
	        $IPAddress = htmlspecialchars($IPAddress, ENT_QUOTES);
	        $tUserID = htmlspecialchars($UserID, ENT_QUOTES);
	        $CourseID = htmlspecialchars($CourseID, ENT_QUOTES);
	        
	        $InputDate = date("Y-m-d H:i:s");
	        $YY = date("Y");
	        $mm = date("m");
	        $dd = date("d");

	     	$log_array = array($tUserID, $CourseID, $query, $SourcePath, $InputDate, $IPAddress, $ErrorMsg);
	     	$body = serialize($log_array)."\n";
	     	
	     	$folder_path = "$intranet_root/file/log_error_query";
	     	
	     	if(!is_dir($folder_path))
	     	{
		     	@mkdir($folder_path, 0777);
	     	}
	     	
	        //$file = "$intranet_root/file/log_error_query_$Year.txt";
	        $file = $folder_path."/log_error_query_".$YY."-".$mm."-".$dd.".txt";
	        
	        $fd = @fopen($file, "a+");
	        @chmod($file, 0777);
	        @fwrite($fd, $body);
            @fclose ($fd);
	        /*
	        $sql = "INSERT INTO 
	        		{$intranet_db}.INTRANET_LOGGING_TABLE
	        		(UserID, CourseID, SqlQuery, SourcePath, InputDate, IPAddress, ErrorMsg)
	        		values
	        		('$UserID', '$CourseID', '$query', '$SourcePath', now(), '$IPAddress', '$ErrorMsg')
	        		";
	        		
	        mysql_select_db($this->db) or exit(mysql_error());
	        mysql_query($sql);
	        */
        }

	function Get_Safe_Sql_Query($value) {
		if(is_array($value)){
			foreach($value as $k => $v){
				$value[$k] = $this->Get_Safe_Sql_Query($v);
			}
		}else{
    		$value = str_replace(array("\\","'"),array("\\\\","\\'"), $value);
		}
		return $value;
  }
  
  function Get_Safe_Sql_Like_Query($value) {
  	return str_replace(array("\\",'%','_',"'"),array("\\\\\\\\",'\%','\_',"\\'"),$value);
  }
  
  function Start_Trans() {
  	mysql_query("START TRANSACTION");
  }
  
  function Start_Trans_For_Lock_Table_Procedure() {
  	mysql_query("SET autocommit = 0");
  }
  
	function Commit_Trans() {
		mysql_query("COMMIT");
	}
	
	function RollBack_Trans() {
		mysql_query("ROLLBACK");
	}
	
	function With_Nolock_Trans() {
		mysql_query("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
	}
	
	function pack_value($value, $type='')
	{
		$returnValue = "";
		switch (strtolower($type))
		{
			case "int":
				if ($value === 0 || $value === '0') {
					$returnValue = 0;
				}
				else if ($value == null || $value == 'null') {
					$returnValue = "null";
				}
				else if (empty($value)) {
					$returnValue = "''";
				}
				else {
					$returnValue = $value;
				}
			break;

			case "date":
				switch (strtolower($value))
				{
					case empty($value):
					case null:
					case 'null':
						$returnValue = "null";
						break;
					case "now()":
						$returnValue = "now()";
						break;
					default :
						$returnValue = "'$value'";
					break;
				}
			break;

			default :
				$returnValue = "'".$this->Get_Safe_Sql_Query($value)."'";
		}
		return $returnValue;
	}

	//passing variable
		//$DataArr["TITLE"]	= 'Title 1'
		//$DataArr["INTRODUCTION"] = 'Introduction 1';
	//return 
		// $returnAry['sqlField'] = 'TITLE,INTRODUCTION'
		// $returnAry['sqlValue'] = 'Title 1 ,Introduction 1'
	function concatFieldValueToSqlStr($DataArr){
		# set field and value string
		$fieldArr = array();
		$valueArr = array();

		$sqlFieldText = '';
		$sqlValueText = '';

		if(is_array($DataArr)){
			foreach ($DataArr as $field => $value){
				$fieldArr[] = $field;
				$valueArr[] = $value;
			}

			$sqlFieldText = implode(", ", $fieldArr);
			$sqlValueText = implode(", ", $valueArr);
		
		}			
		
		$returnAry['sqlField'] = $sqlFieldText;
		$returnAry['sqlValue'] = $sqlValueText;

		return $returnAry;
	}
	
	function Get_Next_Auto_Increment($table)
	{
		$sql = "SHOW TABLE STATUS LIKE '$table'";
		$Result = $this->returnArray($sql);
		return $Result[0]['Auto_increment'];
	}
	
	function Get_Update_SQL_Set_String($DataArr) {
		$valueArr = array();
		foreach ($DataArr as $field => $value) {
			if ($value == 'null' || $value == 'now()') {
				$valueArr[] = $field." = ".$this->Get_Safe_Sql_Query($value);
			}
			else {
				$valueArr[] = $field." = '".$this->Get_Safe_Sql_Query($value)."'";
			}
		}
		
		return implode(',', (array)$valueArr);
	}
	
	function Reorder_Display_Order($Table, $IDField, $OrderField, $IDOrderArr, $Starting=1)
	{

		if(!is_numeric($Starting)) return false;		

	    foreach($IDOrderArr as $order => $thisID) 
	    {
	      $CaseSql .= " WHEN $thisID THEN ".($Starting+$order)."\n";
	    }

		$sql = "
			UPDATE 
				$Table 
			SET 
				$OrderField = (CASE $IDField
					$CaseSql 
				END) 
			WHERE 
				$IDField IN (" . implode(",", (array)$IDOrderArr) . ")
		";
	
	    return $this->db_db_query($sql);		
	}
	
	function opendb($db_host, $db_user, $db_password, $db_name)
	{
		mysql_connect($db_host , $db_user, $db_password);
        mysql_query("set character_set_database='utf8'");
		mysql_query("set names utf8");
		$this->db = $db_name;
	}
	
	function closedb()
	{
		@mysql_close();
	}
	
	##########################################################################
	######################## Preset Function [Start] #########################
	####### Please leave these functions in the bottom of this library #######
	##########################################################################
	function getCacheResult($parFuncName, $parFuncParamAry) {
		$cacheAryVariableStr = $this->getCacheAryVariableStr($parFuncName, $parFuncParamAry);
    	$isCacheResultExist = $this->isCacheResultExist($parFuncName, $parFuncParamAry);
    	
    	$returnAry = array();
    	if ($isCacheResultExist) {
            $cacheAryVariableStr = str_replace(';','',$cacheAryVariableStr);
    		eval('$returnAry = '.$cacheAryVariableStr.';');
    		return $returnAry;
    	}
    	else {
    		return null;
    	}
	}
	
	function setCacheResult($parFuncName, $parFuncParamAry, $resultAry) {
		$cacheAryVariableStr = $this->getCacheAryVariableStr($parFuncName, $parFuncParamAry);
        $cacheAryVariableStr = str_replace(';','',$cacheAryVariableStr);
		eval($cacheAryVariableStr.' = $resultAry;');
	}
	
	function getCacheAryVariableStr($parFuncName, $parFuncParamAry) {
		$cacheAryVariableStr = '$this->cacheResultAry[\''.$parFuncName.'\']';
    	foreach( (array)$parFuncParamAry as $arg_key => $arg_val) {
    		if (is_array($arg_val)) {
    			$key_name = implode(',', $arg_val);
    		}
    		else {
    			$key_name = trim($arg_val);
    		}
    		$cacheAryVariableStr .= "['".addslashes($key_name)."']";
    	}
    	return $cacheAryVariableStr;
	}
	
	function isCacheResultExist($parFuncName, $parFuncParamAry) {
		$cacheAryVariableStr = $this->getCacheAryVariableStr($parFuncName, $parFuncParamAry);
        $cacheAryVariableStr = str_replace(';','',$cacheAryVariableStr);
		
		$isCacheResultExist = false;
    	eval('$isCacheResultExist = isset('.$cacheAryVariableStr.');');
    	return $isCacheResultExist;
	}
	##########################################################################
	####### Please leave these functions in the bottom of this library #######
	######################### Preset Function [End] ##########################
	##########################################################################
	
	function isMagicQuotesOn()
	{
		$is_magic_quotes_on = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");
		return $is_magic_quotes_on;
	}
	
	function checkColumnIsExist($table, $column){
	    $sql = "SELECT COUNT(*) FROM information_schema.COLUMNS WHERE TABLE_NAME = '".$table."' AND  COLUMN_NAME = '".$column."';";
	    $result = $this->returnVector($sql);
	    return $result[0];
	}
}

}        // End of directive

?>
