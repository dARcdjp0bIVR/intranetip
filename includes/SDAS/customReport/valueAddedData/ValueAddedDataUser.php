<?php
/**
 * Change Log:
 * 2017-08-25 Pun
 *  - File Create
 */

if(!class_exists('ValueAddedDataUser')){
class ValueAddedDataUser {
    
    public $userId;
    public $chineseName;
    public $englishName;
    
	public function __construct($userId, $chineseName, $englishName){
        $this->userId = $userId;
        $this->chineseName = $chineseName;
        $this->englishName = $englishName;
	}
    
    
    
    

	public function __toString(){
	    return Get_Lang_Selection($this->chineseName,$this->englishName);
	}
    
	static function compareName($a, $b){
	    return strcmp($a->englishName, $b->englishName);
	}
    
} // End Class
}
