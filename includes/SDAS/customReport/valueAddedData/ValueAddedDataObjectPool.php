<?php
/**
 * Change Log:
 * 2018-01-23 Pun
 *  - Modified addResult(), fixed cannot use in PHP5.4
 * 2017-08-25 Pun
 *  - File Create
 */

if(!class_exists('ValueAddedDataObjectPool')){
class ValueAddedDataObjectPool {

    public $subjectPool;
    public $subjectGroupPool;
    public $classPool;
    public $resultPool;
    public $userPool;
    
	
	public function __construct(){
	    $this->subjectPool = array();
	    $this->subjectGroupPool = array();
	    $this->classPool = array();
	    $this->resultPool = array();
	    $this->userPool = array();
	}
    
	
	
	
    public function addSubject($subject){
        $this->subjectPool[$subject->subjectId] = $subject;
    }
    
    public function &getSubjectById($subjectId){
        if(empty($this->subjectPool[$subjectId])){
            return null;
        }
        return $this->subjectPool[$subjectId];
    }
    
    
    
    public function addSubjectGroup($subjectGroup){
        $this->subjectGroupPool[$subjectGroup->subjectGroupId] = $subjectGroup;
    }

    public function &getSubjectGroupById($subjectGroupId){
        if(empty($this->subjectGroupPool[$subjectGroupId])){
            return null;
        }
        return $this->subjectGroupPool[$subjectGroupId];
    }
    
    


    public function addClass($class){
        $this->classPool[$class->classId] = $class;
    }
    
    public function &getClassById($classId){
        if(empty($this->classPool[$classId])){
            return null;
        }
        return $this->classPool[$classId];
    }

    
    
    public function addResult(&$result){
        $this->resultPool[$result->resultId] = $result;
    }
    
    public function &getResultByClassNameSubjectId($className, $subjectId){
        foreach($this->resultPool as $result){
            if($result->className == $className && $result->subjectId == $subjectId){
                return $result;
            }
        }
        return null;
    }
    
    
    public function addUser($user){
        $this->userPool[$user->userId] = $user;
    }
    
    public function &getUserById($userId){
        if(empty($this->userPool[$userId])){
            return null;
        }
        return $this->userPool[$userId];
    }
    
} // End Class
}

