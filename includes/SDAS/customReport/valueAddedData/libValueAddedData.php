<?php


if(!class_exists('libValueAddedData')){
class libValueAddedData extends libdb {
	
    const VALUE_ADDED_TYPE_SUBJECT_GROUP = '0';
    const VALUE_ADDED_TYPE_TEACHER = '1';
    const VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_CLASS = '0';
    const VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_SPECIAL = '1';
    
	private function getPortfolioDatabase(){
	    global $eclass_db;
	    return $eclass_db;
	}
    
    private function getYearTermId($academicYearId, $yearTerm){
        $TermID = explode('_',$yearTerm);
        if(empty($TermID[0])){
            return 0;
        }else{
            $sql = "SELECT
                YearTermID
            FROM
                ACADEMIC_YEAR_TERM
            WHERE
                AcademicYearID = '{$academicYearId}'
            AND
            (
                YearTermNameEN = '{$TermID[0]}'
            OR
                YearTermNameB5 = '{$TermID[0]}'
            )
            ";
            $rs = $this->returnVector($sql);
             
            if(count($rs == 1)){
                return $rs[0];
            }else{
                throw new Exception("Cannot find ACADEMIC_YEAR_TERM by {$yearTerm}");
            }
        }
    }
    
    private function getTermAssessment($yearTerm){
        $TermID = explode('_',$yearTerm);
    
        if(empty($TermID[1])){
            return '';
        }else{
            return $TermID[1];
        }
    }
    
    private function hasScore($score){
        return ($score !== -1 && $score !== null);
    }
    
    private function getYear(){
        $sql = "SELECT
            Y.YearID,
            Y.YearName
        FROM
            YEAR Y
        ORDER BY 
            Y.Sequence";
        $rs = $this->returnResultSet($sql);
        
        return $rs;
    }
    
    private function getClass(){
        $sql = "SELECT
            YC.YearClassID,
            YC.AcademicYearID,
            YC.ClassTitleEN,
            YC.ClassTitleB5,
            Y.YearID,
            Y.YearName
        FROM
            YEAR_CLASS YC
        INNER JOIN
            YEAR Y
        ON
            YC.YearID = Y.YearID
        WHERE
            AcademicYearID IN ('{$this->academicYearId}', '{$this->vsAcademicYearId}')
        ORDER BY 
            Y.Sequence
        ";
        $rs = $this->returnResultSet($sql);
        
        foreach ($rs as $index=>$r){
            $rs[$index]['ClassTitle'] = Get_Lang_Selection($r['ClassTitleB5'],$r['ClassTitleEN']);
        }
        
        return $rs;
    }

    private function getSubjectGroupByYearTermIdSubjectId($yearTermIds, $subjectIds){
        $yearTermIdSql = implode("','", (array)$yearTermIds);
        if($subjectIds){
            $subjectIdSql = implode("','", (array)$subjectIds);
            $subjectIdCond = "AND ST.SubjectID IN ('{$subjectIdSql}')";
        }
        $sql = "SELECT
            ST.SubjectID,
            ST.YearTermID,
            ST.SubjectGroupID,
            STC.ClassTitleEN,
            STC.ClassTitleB5,
            Y.YearID,
            Y.Sequence
        FROM
            SUBJECT_TERM_CLASS STC
        INNER JOIN
            SUBJECT_TERM ST
        ON
            STC.SubjectGroupID = ST.SubjectGroupID
        INNER JOIN
	        SUBJECT_TERM_CLASS_YEAR_RELATION STCYR
        ON
            STCYR.SubjectGroupID = ST.SubjectGroupID
        INNER JOIN
	        YEAR Y
        ON
	        STCYR.YearID = Y.YearID
        WHERE
            ST.SubjectComponentID=0
        AND
            ST.YearTermID IN ('{$yearTermIdSql}')
        {$subjectIdCond}
        ";
        $rs = $this->returnResultSet($sql);
        
        foreach ($rs as $index=>$r){
            $rs[$index]['ClassTitle'] = $r['ClassTitle'] = Get_Lang_Selection($r['ClassTitleB5'],$r['ClassTitleEN']);
            $rs[$index]['Type'] = $this->getSubjectGroupType($r);
        }
        
        usort($rs, function($a, $b){
            return strcmp("{$a['Sequence']}_{$a['ClassTitle']}", "{$b['Sequence']}_{$b['ClassTitle']}");
        });
        return $rs;
    }

    private function getSubjectGroupStudentBySubjectGroupId($subjectGroupIds){
        $subjectGroupIdSql = implode("','", (array)$subjectGroupIds);
        $sql = "SELECT
            STC.SubjectGroupID,
            STCU.UserID
        FROM
            SUBJECT_TERM_CLASS STC
        INNER JOIN
            SUBJECT_TERM_CLASS_USER STCU
        ON
            STC.SubjectGroupID = STCU.SubjectGroupID
        WHERE
            STC.SubjectGroupID IN ('{$subjectGroupIdSql}')";
        $rs = $this->returnResultSet($sql);
        
        return $rs;
    }

    private function getSubject(){
        $sql = "SELECT 
            ASS.RecordID AS SubjectID,
            ASS.CH_DES,
	        ASS.EN_DES
        FROM 
            ASSESSMENT_SUBJECT AS ASS
        INNER JOIN
            LEARNING_CATEGORY LC
        ON
            ASS.LearningCategoryID = LC.LearningCategoryID
        ORDER BY
            LC.DisplayOrder, 
            ASS.DisplayOrder";
        $rs = $this->returnResultSet($sql);
        
        foreach ($rs as $index=>$r){
            $rs[$index]['SubjectName'] = Get_Lang_Selection($r['CH_DES'],$r['EN_DES']);
        }
        
        return $rs;
    }

    private function getSubjectGroupTeacherBySubjectGroupId($subjectGroupIds){
        $subjectGroupIdSql = implode("','", (array)$subjectGroupIds);
        $sql = "SELECT
            STC.SubjectGroupID,
            STCT.UserID
        FROM
            SUBJECT_TERM_CLASS STC
        INNER JOIN
            SUBJECT_TERM_CLASS_TEACHER STCT
        ON
            STC.SubjectGroupID = STCT.SubjectGroupID
        WHERE
            STC.SubjectGroupID IN ('{$subjectGroupIdSql}')";
        $rs = $this->returnResultSet($sql);
        
        return $rs;
    }
    
    private function getSubjectGroupType($subjectGroup){
        $yearNameArr = Get_Array_By_Key($this->years, 'YearName');
        $classNameEnArr = Get_Array_By_Key($this->classes, 'ClassTitleEN');
        $classNameB5Arr = Get_Array_By_Key($this->classes, 'ClassTitleB5');
        $classNameArr = array_unique(array_merge($classNameEnArr, $classNameB5Arr));
        
        $_title = (explode(' ', $subjectGroup['ClassTitle']));
        foreach($yearNameArr as $yearName){
            foreach($_title as $t){
                if( $t == $yearName ){
                    return self::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_SPECIAL;
                }
            }
        }
        foreach($classNameArr as $className){
            foreach($_title as $t){
                if( $t == $className ){
                    return self::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_CLASS;
                }
            }
        }
        
        return self::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_SPECIAL;
    }

    private function getScoreData($filterArr) {
        $eclass_db = $this->getPortfolioDatabase();
         
        if($this->yearTermId){
            $filterYearTermSql = " ASSR.YearTermID = '{$this->yearTermId}'";
        }else{
            $filterYearTermSql = " (ASSR.YearTermID IS NULL OR ASSR.YearTermID = '0')";
        }
        if($this->termAssessment){
            $filterTermAssessmentSql = " ASSR.TermAssessment = '{$this->termAssessment}'";
        }else{
            $filterTermAssessmentSql = " (ASSR.TermAssessment IS NULL OR ASSR.TermAssessment = '')";
        }
         
        if($this->vsYearTermId){
            $filterVsYearTermSql = " ASSR.YearTermID = '{$this->vsYearTermId}'";
        }else{
            $filterVsYearTermSql = " (ASSR.YearTermID IS NULL OR ASSR.YearTermID = '0')";
        }
        if($this->vsTermAssessment){
            $filterVsTermAssessmentSql = " ASSR.TermAssessment = '{$this->vsTermAssessment}'";
        }else{
            $filterVsTermAssessmentSql = " (ASSR.TermAssessment IS NULL OR ASSR.TermAssessment = '')";
        }
         
        $filterSql = '';
        if($filterArr['SubjectId']){
            $filterSql = " AND SubjectID IN ('{$filterArr['SubjectId']}')";
        }
         
        $sql = "SELECT
            ASSR.RecordID,
            ASSR.UserID,
            ASSR.SubjectID,
            ASSR.SubjectComponentID,
            ASSR.Score,
            ASSR.AcademicYearID,
            ASSR.YearTermID,
            ASSR.TermAssessment
        FROM
            {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
        WHERE
        (
            ASSR.AcademicYearID = '{$this->academicYearId}'
        AND
            {$filterYearTermSql}
        AND
            {$filterTermAssessmentSql}
        ) OR (
            ASSR.AcademicYearID = '{$this->vsAcademicYearId}'
        AND
            {$filterVsYearTermSql}
        AND
            {$filterVsTermAssessmentSql}
        )
            {$filterSql}
        ";
        $rs = $this->returnResultSet($sql);
        return $rs;
    }

    private function getUserInfoByUserId($userIds){
        $userIdSql = implode("','", (array)$userIds);
	    $sql = "SELECT
	        UserID,
	        ChineseName,
	        EnglishName
        FROM
	        INTRANET_USER
        WHERE
	        UserID IN ('{$userIdSql}')
        ";
	    $rs = $this->returnResultSet($sql);
	    
	    $sql = "SELECT
	        UserID,
	        ChineseName,
	        EnglishName
        FROM
	        INTRANET_ARCHIVE_USER
        WHERE
	        UserID IN ('{$userIdSql}')
        ";
	    $rs2 = $this->returnResultSet($sql);
	    
	    $rs = array_merge($rs, $rs2);
	    foreach($rs as $index=>$r){
            $rs[$index]['Name'] = Get_Lang_Selection($r['ChineseName'],$r['EnglishName']);
	    }
	    
	    return $rs;
    }
    
    private function calculateCorrelationSubjectGroup($data){
        //$data = BuildMultiKeyAssoc($rs, array('YearTermID', 'TermAssessment', 'SubjectID', 'SubjectComponentID', 'UserID'));
        
        #### Get subject group student START ####
        foreach($this->subjectGroups as $index=>$subjectGroup){
            $subjectGroupStudents = array_filter($this->subjectGroupStudents, function($subjectGroupStudent) use ($subjectGroup){
                return $subjectGroupStudent['SubjectGroupID'] == $subjectGroup['SubjectGroupID'];
            });
            
            $this->subjectGroups[$index]['SubjectGroupStudent'] = $subjectGroupStudents;
        }
        foreach($this->vsSubjectGroups as $index=>$vsSubjectGroup){
            $subjectGroupStudents = array_filter($this->vsSubjectGroupStudents, function($subjectGroupStudent) use ($vsSubjectGroup){
                return $subjectGroupStudent['SubjectGroupID'] == $vsSubjectGroup['SubjectGroupID'];
            });
            
            $this->vsSubjectGroups[$index]['SubjectGroupStudent'] = $subjectGroupStudents;
        }
        #### Get subject group student END ####
        
        #### Get mapping START ####
        $subjectGroupMapping = array();
        $vsSubjectGroupMapping = array();
        foreach($this->subjectGroups as $subjectGroup){
            $subjectId = $subjectGroup['SubjectID'];
            $_title = (explode(' ', $subjectGroup['ClassTitle']));
            if($subjectGroup['Type'] == self::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_CLASS){
                $subjectGroupName = $_title[0];
                $subjectGroupMapping[$_title[0]][$subjectId][$subjectGroupName] = $subjectGroup;
            }else{
                $yearName = $this->yearInfos[$subjectGroup['YearID']]['YearName'];
                $subjectGroupName = $_title[1];
                $subjectGroupMapping[$yearName][$subjectId][$subjectGroupName] = $subjectGroup;
            }
        }
        foreach($this->vsSubjectGroups as $subjectGroup){
            $subjectId = $subjectGroup['SubjectID'];
            $_title = (explode(' ', $subjectGroup['ClassTitle']));
            if($subjectGroup['Type'] == self::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_CLASS){
                $subjectGroupName = $_title[0];
                $vsSubjectGroupMapping[$_title[0]][$subjectId][$subjectGroupName] = $subjectGroup;
            }else{
                $yearName = $this->yearInfos[$subjectGroup['YearID']]['YearName'];
                $subjectGroupName = $_title[1];
                $vsSubjectGroupMapping[$yearName][$subjectId][$subjectGroupName] = $subjectGroup;
            }
        }
        #### Get mapping END ####
        
        #### Get score tuple START ####
        $studentCountData = array();
        $scoreTuple = array();
        foreach($vsSubjectGroupMapping as $className => $d0){
            foreach($d0 as $subjectId=>$d1){
                $studentScoreData = $data[$this->yearTermId][$this->termAssessment][$subjectId][''];
                $vsStudentScoreData = $data[$this->vsYearTermId][$this->vsTermAssessment][$subjectId][''];
                
                foreach($d1 as $subjectGroupName=>$vsSubjectGroup){
                    $subjectGroup = $subjectGroupMapping[$className][$subjectId][$subjectGroupName];
                    $yearId = $vsSubjectGroup['YearID'];
                    $subjectGroupID = $subjectGroup['SubjectGroupID'];
                    $vsSubjectGroupID = $vsSubjectGroup['SubjectGroupID'];
                    $subjectGroupStudents = $subjectGroup['SubjectGroupStudent'];
                    $vsSubjectGroupStudents = $vsSubjectGroup['SubjectGroupStudent'];

                    $studentCountData[$yearId][$className][$vsSubjectGroupID][$this->yearTermId] = 0;
                    $studentCountData[$yearId][$className][$vsSubjectGroupID][$this->vsYearTermId] = 0;
                    $studentCountData[$yearId][$className][$vsSubjectGroupID]['totalAttempt'] = 0;
                    $scoreTuple[$yearId][$className][$vsSubjectGroupID]['X'] = array();
                    $scoreTuple[$yearId][$className][$vsSubjectGroupID]['Y'] = array();
                    foreach($vsSubjectGroupStudents as $vsSubjectGroupStudent){
                        $vsSubjectGroupStudentId = $vsSubjectGroupStudent['UserID'];
                        $score = $studentScoreData[$vsSubjectGroupStudentId]['Score'];
                        $vsScore = $vsStudentScoreData[$vsSubjectGroupStudentId]['Score'];
                        
                        if($this->hasScore($score)){
                            $studentCountData[$yearId][$className][$vsSubjectGroupID][$this->yearTermId]++;
                        }
                        if($this->hasScore($vsScore)){
                            $studentCountData[$yearId][$className][$vsSubjectGroupID][$this->vsYearTermId]++;
                        }
                        
                        if($this->hasScore($score) && $this->hasScore($vsScore)){
                            $scoreTuple[$yearId][$className][$vsSubjectGroupID]['X'][] = $score;
                            $scoreTuple[$yearId][$className][$vsSubjectGroupID]['Y'][] = $vsScore;
                            $studentCountData[$yearId][$className][$vsSubjectGroupID]['totalAttempt']++;
                        }
                    }
                }
            }
        }
        #### Get score tuple END ####
        
        #### Calculate correlation START ####
        $correlationData = array();
        foreach($scoreTuple as $yearId => $d1){
            foreach ($d1 as $className => $d2){
                foreach ($d2 as $vsSubjectGroupID => $tuple){
                    $correlationData[$yearId][$className][$vsSubjectGroupID] = $this->calculateCorrelation($tuple);
                }
            }
        }
        #### Calculate correlation END ####
        return array(
            'correlationData' => $correlationData,
            'studentCountData' => $studentCountData,
        );
    }

    private function calculateCorrelationTeacher($data, $filterArr){
        //$data = BuildMultiKeyAssoc($rs, array('YearTermID', 'TermAssessment', 'SubjectID', 'SubjectComponentID', 'UserID'));

        ## Get teacher's student START ##
        $teacherStudentArr = array();
        foreach($this->vsSubjectGroupTeachers as $subjectGroupTeacher){
            $teacherId = $subjectGroupTeacher['UserID'];
            $vsSubjectGroupID = $subjectGroupTeacher['SubjectGroupID'];
        
            $subjectGroupStudents = array_filter($this->vsSubjectGroupStudents, function($subjectGroupStudent) use ($vsSubjectGroupID){
                return $subjectGroupStudent['SubjectGroupID'] == $vsSubjectGroupID;
            });
            $subjectGroupStudentIds = Get_Array_By_Key(array_values($subjectGroupStudents), 'UserID');
            $teacherStudentArr[$teacherId][$vsSubjectGroupID] = $subjectGroupStudentIds;
        }
        ## Get teacher's student END ##
        
        ## Get score tuple START ##
        $studentScoreData = $data[$this->yearTermId][$this->termAssessment][$filterArr['SubjectId']][''];
        $vsStudentScoreData = $data[$this->vsYearTermId][$this->vsTermAssessment][$filterArr['SubjectId']][''];
        
        $studentCountData = array();
        $scoreTuple = array();
        foreach($teacherStudentArr as $teacherId => $d1){
            foreach($d1 as $vsSubjectGroupID => $subjectGroupStudentIds){
                $subjectGroupStudentIds = (array)$subjectGroupStudentIds;
                $studentCountData[$teacherId][$vsSubjectGroupID][$this->yearTermId] = 0;
                $studentCountData[$teacherId][$vsSubjectGroupID][$this->vsYearTermId] = 0;
                $studentCountData[$teacherId][$vsSubjectGroupID]['totalAttempt'] = 0;
                $studentCountData[$teacherId][$vsSubjectGroupID]['vsStudentId'] = implode(',', $subjectGroupStudentIds);
                $scoreTuple[$teacherId][$vsSubjectGroupID]['X'] = array();
                $scoreTuple[$teacherId][$vsSubjectGroupID]['Y'] = array();
                foreach($subjectGroupStudentIds as $subjectGroupStudentId){
                    $score = $studentScoreData[$subjectGroupStudentId]['Score'];
                    $vsScore = $vsStudentScoreData[$subjectGroupStudentId]['Score'];
                    
                    if($this->hasScore($score)){
                        $studentCountData[$teacherId][$vsSubjectGroupID][$this->yearTermId]++;
                    }
                    if($this->hasScore($vsScore)){
                        $studentCountData[$teacherId][$vsSubjectGroupID][$this->vsYearTermId]++;
                    }
        
                    if($this->hasScore($score) && $this->hasScore($vsScore)){
                        $scoreTuple[$teacherId][$vsSubjectGroupID]['X'][] = $score;
                        $scoreTuple[$teacherId][$vsSubjectGroupID]['Y'][] = $vsScore;
                        $studentCountData[$teacherId][$vsSubjectGroupID]['totalAttempt']++;
                    }
                }
            }
        }
        ## Get score tuple END ##
        
        ## Calculate correlation START ##
        $correlationData = array();
        foreach($teacherStudentArr as $teacherId => $d1){
            foreach($d1 as $vsSubjectGroupID => $d2){
                $correlationData[$teacherId][$vsSubjectGroupID] = $this->calculateCorrelation($scoreTuple[$teacherId][$vsSubjectGroupID]);
            }
        }
        ## Calculate correlation END ##
        return array(
            'correlationData' => $correlationData,
            'studentCountData' => $studentCountData,
        );
    }

    private function calculateCorrelation($scoreTuple){
        $libSDAS = new libSDAS();
        
        $countY = count($scoreTuple['Y']);
        $countX = count($scoreTuple['X']);
        if($countX < 2 || $countY < 2){
            return null;
        }
        
        $meanY = array_sum($scoreTuple['Y']) / $countY;
        $meanX = array_sum($scoreTuple['X']) / $countX;
         
        $sdY = $libSDAS->sd($scoreTuple['Y']);
        $sdX = $libSDAS->sd($scoreTuple['X']);
         
        if($sdY && $sdX){
            $productArr = array();
            for($i=0;$i<$countY;$i++){
                $scoreY = $scoreTuple['Y'][$i];
                $ySubtractBarY = $scoreY - $meanY;
                $ySubtractBarYOverSdY = $ySubtractBarY / $sdY;
                 
                $scoreX = $scoreTuple['X'][$i];
                $xSubtractBarX = $scoreX - $meanX;
                $xSubtractBarXOverSdX = $xSubtractBarX / $sdX;
                $product = $ySubtractBarYOverSdY * $xSubtractBarXOverSdX;
                $productArr[] = $product;
            }
            	
            $correlation = array_sum($productArr) / $countY;
        }else{
            $correlation = null;
        }
         
        return $correlation;
    }
    
    public function getReportData($reportType, $academicYearId, $vsAcademicYearId, $yearTerm, $vsYearTerm, $filterArr){
        #### Init object data START ####
        $this->academicYearId = $academicYearId;
        $this->vsAcademicYearId = $vsAcademicYearId;
         
        $this->yearTermId = $this->getYearTermId($this->academicYearId, $yearTerm);
        $this->vsYearTermId = $this->getYearTermId($this->vsAcademicYearId, $vsYearTerm);
         
        $this->termAssessment = $this->getTermAssessment($yearTerm);
        $this->vsTermAssessment = $this->getTermAssessment($vsYearTerm);
        #### Init object data END ####
         
        #### Get year START ####
        $this->years = $this->getYear();
        $this->yearInfos = BuildMultiKeyAssoc($this->years, array('YearID'));
        #### Get year END ####
         
        #### Get class START ####
        $this->classes = $this->getClass();
        #### Get class END ####
        
        #### Get subject START ####
        $this->subjects = $this->getSubject();
        $this->subjectInfos = BuildMultiKeyAssoc($this->subjects, array('SubjectID'));
        #### Get subject END ####
        
        #### Get subject group START ####
        $this->subjectGroups = $this->getSubjectGroupByYearTermIdSubjectId($this->yearTermId, $filterArr['SubjectId']);
        $this->vsSubjectGroups = $this->getSubjectGroupByYearTermIdSubjectId($this->vsYearTermId, $filterArr['SubjectId']);
        $subjectGroupIds = Get_Array_By_Key($this->subjectGroups, 'SubjectGroupID');
        $vsSubjectGroupIds = Get_Array_By_Key($this->vsSubjectGroups, 'SubjectGroupID');
        
        $rs = array_merge($this->subjectGroups, $this->vsSubjectGroups);
        $this->subjectGroupInfos = BuildMultiKeyAssoc($rs, array('SubjectGroupID'));
        #### Get subject group END ####
        
        #### Get subject group student START ####
        $this->subjectGroupStudents = $this->getSubjectGroupStudentBySubjectGroupId($subjectGroupIds);
        $this->vsSubjectGroupStudents = $this->getSubjectGroupStudentBySubjectGroupId($vsSubjectGroupIds);
        #### Get subject group student END ####
        
        #### Get subject group teacher START ####
        $this->subjectGroupTeachers = $this->getSubjectGroupTeacherBySubjectGroupId($subjectGroupIds);
        $this->vsSubjectGroupTeachers = $this->getSubjectGroupTeacherBySubjectGroupId($vsSubjectGroupIds);

        $subjectGroupTeacherIds = Get_Array_By_Key($this->subjectGroupTeachers, 'UserID');
        $vsSubjectGroupTeacherIds = Get_Array_By_Key($this->vsSubjectGroupTeachers, 'UserID');
        $teacherIds = array_merge($subjectGroupTeacherIds, $vsSubjectGroupTeacherIds);
        
        $vsSubjectGroupTeacherMapping = array();
        foreach($this->subjectGroupTeachers as $teacher){
            $vsSubjectGroupTeacherMapping[$teacher['SubjectGroupID']][] = $teacher['UserID'];
        }
        foreach($this->vsSubjectGroupTeachers as $teacher){
            $vsSubjectGroupTeacherMapping[$teacher['SubjectGroupID']][] = $teacher['UserID'];
        }
        #### Get subject group teacher END ####
        
        #### Get user name START ####
        $rs = $this->getUserInfoByUserId($teacherIds);
        $this->userInfos = BuildMultiKeyAssoc($rs, array('UserID'));
        #### Get user name END ####
        
        #### Get score data START ####
        $rs = $this->getScoreData($filterArr);
        $data = BuildMultiKeyAssoc($rs, array('YearTermID', 'TermAssessment', 'SubjectID', 'SubjectComponentID', 'UserID'));
        #### Get score data END ####
         
        #### Calculate data START ####
        if($reportType == self::VALUE_ADDED_TYPE_SUBJECT_GROUP){
            $rs = $this->calculateCorrelationSubjectGroup($data);
        }else if($reportType == self::VALUE_ADDED_TYPE_TEACHER){
            $rs = $this->calculateCorrelationTeacher($data, $filterArr);
        }
        $correlationData = $rs['correlationData'];
        $studentCountData = $rs['studentCountData'];
        #### Calculate data END ####
         
        #### Pack data START ####
        $data = array();
        if($reportType == self::VALUE_ADDED_TYPE_SUBJECT_GROUP){
            foreach($correlationData as $yearId => $d1){
                foreach($d1 as $className => $d2){
                    foreach($d2 as $vsSubjectGroupID => $correlation){
                        $studentCount = $studentCountData[$yearId][$className][$vsSubjectGroupID][$this->yearTermId];
                        $vsStudentCount = $studentCountData[$yearId][$className][$vsSubjectGroupID][$this->vsYearTermId];
                        if($this->yearTermId == $this->vsYearTermId){
                            $studentCount /= 2;
                            $vsStudentCount /= 2;
                        }
                        $attemptStudentCount = $studentCountData[$yearId][$className][$vsSubjectGroupID]['totalAttempt'];
                        $subjectGroup = $this->subjectGroupInfos[$vsSubjectGroupID];
                        
                        $subjectName = $this->subjectInfos[$subjectGroup['SubjectID']]['SubjectName'];
                        if($subjectGroup['Type'] == self::VALUE_ADDED_SUBJECT_GROUP_CLASS_TYPE_SPECIAL){
                            $_title = (explode(' ', $subjectGroup['ClassTitle']));
                            $subjectName = "{$subjectName} ({$_title[1]})";
                        }
                        
                        $vsSubjectGroupTeacherName = array();
                        foreach((array)$vsSubjectGroupTeacherMapping[$vsSubjectGroupID] as $teacherId){
                            $vsSubjectGroupTeacherName[] = $this->userInfos[$teacherId]['Name'];
                        }
                        
                        $data[$yearId][$className][$subjectName] = array(
                            'vsSubjectGroupID' => $vsSubjectGroupID,
                            'correlation' => $correlation,
                            'studentCount' => $studentCount,
                            'vsStudentCount' => $vsStudentCount,
                            'attemptStudentCount' => $attemptStudentCount,
                            'vsSubjectGroupTeacherName' => $vsSubjectGroupTeacherName
                        );
                    }
                }
            }
        }else if($reportType == self::VALUE_ADDED_TYPE_TEACHER){
            foreach($correlationData as $teacherId => $d1){
                $teacherName = $this->userInfos[$teacherId]['Name'];
                foreach($d1 as $vsSubjectGroupID => $correlation){
                    $studentCount = $studentCountData[$teacherId][$vsSubjectGroupID][$this->yearTermId];
                    $vsStudentCount = $studentCountData[$teacherId][$vsSubjectGroupID][$this->vsYearTermId];
                    if($this->yearTermId == $this->vsYearTermId){
                        $studentCount /= 2;
                        $vsStudentCount /= 2;
                    }
                    $attemptStudentCount = $studentCountData[$teacherId][$vsSubjectGroupID]['totalAttempt'];
                    $vsAttemptStudentId = $studentCountData[$teacherId][$vsSubjectGroupID]['vsStudentId'];
                    $subjectGroupName = $this->subjectGroupInfos[$vsSubjectGroupID]['ClassTitle'];
                    $data[$teacherName][$subjectGroupName] = array(
                        'correlation' => $correlation,
                        'studentCount' => $studentCount,
                        'vsStudentCount' => $vsStudentCount,
                        'vsAttemptStudentId' => $vsAttemptStudentId,
                        'attemptStudentCount' => $attemptStudentCount,
                    );
                }
            }
    	    ksort($data);
        }
        #### Pack data END ####
         
        return $data;
    }
    
} // End class
}