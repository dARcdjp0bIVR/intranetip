<?php
/**
 * Change Log:
 * 2017-08-25 Pun
 *  - File Create
 */

if(!class_exists('ValueAddedDataSubject')){
class ValueAddedDataSubject {
    
    public $subjectId;
    
    public $subjectTitleEN;
    public $subjectTitleB5;
    
    private $yearTermID;
    private $academicYearID;
    
    private $subjectGroups;
	
	public function __construct($subjectId, $subjectTitleEN, $subjectTitleB5, $yearTermID, $academicYearID){
	    $this->subjectId = $subjectId;
	    $this->subjectTitleEN = $subjectTitleEN;
	    $this->subjectTitleB5 = $subjectTitleB5;
	    $this->yearTermID = $yearTermID;
	    $this->academicYearID = $academicYearID;
	    $this->subjectGroups = array();
	}
    
    public function addSubjectGroups($subjectGroup){
        $this->subjectGroups[$subjectGroup->subjectGroupId] = $subjectGroup;
    }
    
    
    
    public function __toString(){
        return Get_Lang_Selection($this->subjectTitleB5,$this->subjectTitleEN);
    }
    
    
    
    
    
    
    
} // End Class
}
