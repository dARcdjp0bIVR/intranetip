<?php 

class learnAndTeach{
	
    const OVERALL_STAT_TYPE_TEACHER = 0; //teacher class based average
    const OVERALL_STAT_TYPE_SUBJECT = 1;
    const OVERALL_STAT_TYPE_TQBASED = 2; //teacher question based average
    
	private $db;
	
	public function __construct($db){
		$this->db = $db;
	}
	
	/******** Question START ********/
	public function getAllAcademicYear(){
	    $sql = "SELECT DISTINCT
	        LQ.AcademicYearID,
	        AY.YearNameEN,
	        AY.YearNameB5
        FROM 
	        LNT_QUESTION LQ
        INNER JOIN
	        ACADEMIC_YEAR AY
        ON
	        LQ.AcademicYearID = AY.AcademicYearID
        ORDER BY
            AY.Sequence
	    ";
	    $rs = $this->db->returnResultSet($sql);
	    return $rs;
	}
	public function getAcademicYearByUserId($userID = ''){
	    if($userID == ''){
	        $userID = $_SESSION['UserID'];
	    }
	    $sql = "SELECT DISTINCT
	        LQ.AcademicYearID,
	        AY.YearNameEN,
	        AY.YearNameB5
        FROM 
	        LNT_QUESTION LQ
        INNER JOIN
	        LNT_RECORD LR
        ON
	        LQ.RecordID = LR.QuestionID
        AND
	        LR.TeacherID = '{$userID}'
        INNER JOIN
	        ACADEMIC_YEAR AY
        ON
	        LQ.AcademicYearID = AY.AcademicYearID
        ORDER BY
            AY.Sequence
        ";
	    $rs = $this->db->returnResultSet($sql);
	    return $rs;
	}
	
	private function getAllQuestionIdByAcademicYearId($AcademicYearID){
	    $sql = "SELECT 
	        RecordID
        FROM
	        LNT_QUESTION
        WHERE 
	        AcademicYearID='{$AcademicYearID}'
        ORDER BY
            Sequence
        ";
	    $rs = $this->db->returnVector($sql);
	    return $rs;
	}
	
	public function getAllQuestionByAcademicYearId($AcademicYearID){
	    $sql = "SELECT 
	        *
        FROM
	        LNT_QUESTION
        WHERE 
	        AcademicYearID='{$AcademicYearID}'
        ORDER BY
            Sequence
        ";
	    $rs = $this->db->returnResultSet($sql);
	    return $rs;
	}
	/******** Question END ********/
	
	/******** Record START ********/
	public function getAllTeacher(){
	    $nameSql = getNameFieldByLang2('IU.').' As Name';
	    $sql = "SELECT DISTINCT
	        LR.TeacherID,
	        {$nameSql}
        FROM
	        LNT_RECORD LR
        INNER JOIN
	        INTRANET_USER IU
        ON
	        LR.TeacherID = IU.UserID
        ORDER BY 
            Name";
	    $rs = $this->db->returnResultSet($sql);
	    return $rs;
	}

	public function getAllSubject(){
	    $sql = "SELECT DISTINCT
	        LR.LearningCategoryID,
	        LR.SubjectID,
	        ASS.CH_DES,
	        ASS.EN_DES
        FROM
	        LNT_RECORD LR
        INNER JOIN
	        ASSESSMENT_SUBJECT ASS
        ON
	        LR.SubjectID = ASS.RecordID
        ";
	    $rs = $this->db->returnResultSet($sql);
	    return $rs;
	}
	
	public function getAllLearningCategory(){
	    $sql = "SELECT DISTINCT
	        LR.LearningCategoryID,
	        LC.NameChi,
	        LC.NameEng
        FROM 
	        LNT_RECORD LR
        INNER JOIN
	        LEARNING_CATEGORY LC
        ON
	        LR.LearningCategoryID = LC.LearningCategoryID
        ";
	    $rs = $this->db->returnResultSet($sql);
	    return $rs;
	}
	
	public function getAllRecordClass(){
	    $sql = "SELECT DISTINCT
	        LR.YearClassID,
	        LR.SpecialClass
        FROM 
	        LNT_RECORD LR
        ";
	    $rs = $this->db->returnResultSet($sql);
	    return $rs;
	}
	
	public function getAllRecordByAcademicYearId($AcademicYearID){
	    $sql = "SELECT 
	        LR.*,
	        LQ.Sequence AS QuestionSequence
        FROM
	        LNT_RECORD LR
        INNER JOIN
	        LNT_QUESTION LQ
        ON
            LR.QuestionID=LQ.RecordID
        WHERE 
	        LQ.AcademicYearID='{$AcademicYearID}'
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    foreach($rs as $index => $r){
	        if(!$r['YearClassID']){
	            $rs[$index]['YearClassID'] = $r['SpecialClass'];
	        }
	    }
	    return $rs;
	}
	
	public function getMeanDetailsByAcademicYearId($AcademicYearID){
	    $questionId = $this->getAllQuestionIdByAcademicYearId($AcademicYearID);
	    $questionIdSql = implode("','", $questionId);
	    
	    $sql = "SELECT
	        LearningCategoryID, 
	        YearClassID, 
	        SpecialClass,
	        TeacherID,
            SUM(TotalCount) AS TotalCount,
            SUM(Mean) AS TotalSum,
            AVG(Mean) AS Mean,
            MAX(Mean) AS MaxScore,
            MIN(Mean) AS MinScore
        FROM
	        LNT_RECORD
        WHERE 
	        QuestionID IN ('{$questionIdSql}') 
        GROUP BY
            SubjectID, 
            YearClassID, 
            TeacherID
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    foreach($rs as $index => $r){
	        if(!$r['YearClassID']){
	            $rs[$index]['YearClassID'] = $r['SpecialClass'];
	        }
	    }
	    
	    $result = BuildMultiKeyAssoc($rs, array('YearClassID','LearningCategoryID','TeacherID') , array('TotalCount','TotalSum','Mean','MaxScore','MinScore'));
	    return $result;
	}
	
	public function getMeanDetailsByAcademicYearIdTeacherId($AcademicYearID, $teacherIdArr){
	    $teacherIdSql = implode("','", (array)$teacherIdArr);
	    
	    $sql = "SELECT 
	        *
        FROM
	        LNT_AVERAGE_STAT
        WHERE 
	        AcademicYearID = '{$AcademicYearID}'
	    AND
            TeacherID IN ('{$teacherIdSql}')";
	    $rs = $this->db->returnResultSet($sql);
	    
	    foreach($rs as $index => $r){
	        if(!$r['YearClassID']){
	            $rs[$index]['YearClassID'] = $r['SpecialClass'];
	        }
	    }
	    
	    $result = BuildMultiKeyAssoc($rs, array('YearClassID','SubjectID','TeacherID') , array('TotalCount','Mean'));
	    return $result;
	}
	
	public function getqBasedMeanDetailsByAcademicYearIdTeacherId($AcademicYearID, $teacherIdArr){
	    $teacherIdSql = implode("','", (array)$teacherIdArr);
	    
	    $sql = "SELECT 
	        *
        FROM
	        LNT_QBASED_AVERAGE_STAT
        WHERE 
	        AcademicYearID = '{$AcademicYearID}'
	    AND
            TeacherID IN ('{$teacherIdSql}')";
	    $rs = $this->db->returnResultSet($sql);
	   	    
	    $result = BuildMultiKeyAssoc($rs, array('TeacherID') , array('TotalCount','Mean'));
	    return $result;
	}
	
	public function getMeanDetailsByAcademicYearIdSubjectId($AcademicYearID, $subjectIdArr){
	    $subjectIdSql = implode("','", (array)$subjectIdArr);
	    
	    $sql = "SELECT 
	        *
        FROM
	        LNT_AVERAGE_STAT
        WHERE 
	        AcademicYearID = '{$AcademicYearID}'
	    AND
            SubjectID IN ('{$subjectIdSql}')";
	    $rs = $this->db->returnResultSet($sql);
	    
	    foreach($rs as $index => $r){
	        if(!$r['YearClassID']){
	            $rs[$index]['YearClassID'] = $r['SpecialClass'];
	        }
	    }

	    $result = BuildMultiKeyAssoc($rs, array('YearClassID','LearningCategoryID','TeacherID') , array('TotalCount','Mean'));
	    return $result;
	}
	
	public function getMeanDetailsByAcademicYearIdLearningCategoryId($AcademicYearID, $learningCategoryIdArr){
	    $learningCategoryIdSql = implode("','", (array)$learningCategoryIdArr);
	    
	    $sql = "SELECT 
	        *
        FROM
	        LNT_AVERAGE_STAT
        WHERE 
	        AcademicYearID = '{$AcademicYearID}'
	    AND
            LearningCategoryID IN ('{$learningCategoryIdSql}')";
	    $rs = $this->db->returnResultSet($sql);
	    
	    foreach($rs as $index => $r){
	        if(!$r['YearClassID']){
	            $rs[$index]['YearClassID'] = $r['SpecialClass'];
	        }
	    }

	    $result = BuildMultiKeyAssoc($rs, array('YearClassID','SubjectID','TeacherID') , array('TotalCount','Mean'));
	    return $result;
	}
	
	public function getTeacherOverallMeanDetailsByAcademicYearId($AcademicYearID){
	    $questionId = $this->getAllQuestionIdByAcademicYearId($AcademicYearID);
	    $questionIdSql = implode("','", $questionId);
	    
	    $sql = "SELECT
	        *
        FROM
	        LNT_OVERALL_STAT
        WHERE 
	        AcademicYearID = '{$AcademicYearID}'
        AND
            Type = '".self::OVERALL_STAT_TYPE_TEACHER."'
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    if(count($rs)){
	        return $rs[0];
	    }
	    return array('TotalCount'=>-1,'Mean'=>-1,'SD'=>-1,'Max'=>-1,'Min'=>-1);
	}
	
	public function getTeacherOverallQBasedMeanDetailsByAcademicYearId($AcademicYearID){   
	    $sql = "SELECT
	        *
        FROM
	        LNT_OVERALL_STAT
        WHERE 
	        AcademicYearID = '{$AcademicYearID}'
        AND
            Type = '".self::OVERALL_STAT_TYPE_TQBASED."'
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    if(count($rs)){
	        return $rs[0];
	    }
	    return array('TotalCount'=>-1,'Mean'=>-1,'SD'=>-1,'Max'=>-1,'Min'=>-1);
	
	}
	
	public function getOverallMeanDetailsByAcademicYearIdSubjectId($AcademicYearID, $subjectId){
	    $questionId = $this->getAllQuestionIdByAcademicYearId($AcademicYearID);
	    $questionIdSql = implode("','", $questionId);
	    
	    $sql = "SELECT
            *
        FROM
	        LNT_OVERALL_STAT
        WHERE 
	        AcademicYearID = '{$AcademicYearID}'
        AND
            SubjectID = '{$subjectId}'
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    if(count($rs)){
	        return $rs[0];
	    }
	    return array('TotalCount'=>-1,'Mean'=>-1,'SD'=>-1,'Max'=>-1,'Min'=>-1);
	}
	
	public function getOverallMeanDetailsByAcademicYearIdLearningCategoryId($AcademicYearID, $learningCategoryId){
	    $questionId = $this->getAllQuestionIdByAcademicYearId($AcademicYearID);
	    $questionIdSql = implode("','", $questionId);
	    
	    $sql = "SELECT
            *
        FROM
	        LNT_OVERALL_STAT
        WHERE 
	        AcademicYearID = '{$AcademicYearID}'
        AND
            LearningCategoryID = '{$learningCategoryId}'
        AND
            SubjectID IS NULL
        ";
	    $rs = $this->db->returnResultSet($sql);
	    
	    if(count($rs)){
	        return $rs[0];
	    }
	    return array('TotalCount'=>-1,'Mean'=>-1,'SD'=>-1,'Max'=>-1,'Min'=>-1);
	}
	/******** Record END ********/
	
	/******** Transfer Log START ********/
	public function getAllTransferDetails(){
	    $sql = "SELECT * FROM LNT_TRANSFER_LOG ORDER BY RecordID DESC";
	    $rs = $this->db->returnResultSet($sql);
	    return $rs;
	}
	/******** Transfer Log END ********/
	
}// End Class

?>