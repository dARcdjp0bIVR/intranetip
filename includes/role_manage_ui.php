<?
// using : 
/********************************************* Change log *******************************************************
 * 2020-08-04 (thomas) Show eLearning-PowerLesson2 module for powerclass
 * 2020-07-27 (thomas) Show eLearning-eClass module for powerclass
 * 2020-02-17 (Henry) hide other-campusLink module for powerclass
 * 2020-01-02 (Henry) hide ePolling, eSurvey module for powerclass
 * 2019-12-23 (Sam) for Mail-targeting, uncheck both Yes and No options if no config was found to avoid confusion (2019-0918-1436-31235)
 * 2019-07-15 (Henry) show eAdmin-eCircular when $special_feature['circular'] = true
 * 2019-07-04 (Tommy) remove "eAdmin-eEnrolment" in $hideModules and add "eEnrolment" enable module
 * 2018-08-15 (Cameron) hide parent and student column for HKPF
 * 2018-07-05 (Vito) modified function Get_Role_Detail_Ex()
 * 2017-09-13 (Bill) [KIS][ip.2.5.8.10.1]: Modified Get_Top_Management_Form()
 * 			- show SchoolSettings-Subject when $sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] = true,
 * 			- show eAdmin-eHomework when $sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] = true
 * 2017-07-17 (Paul) [NCS][ip.2.5.8.7.1]: Modified Get_Top_Management_Form(), hide Campus and Group for NCS project
 * 2016-03-21 (Ivan) [A94217] [ip.2.5.7.4.1.0]: modified function Get_Top_Management_Form() to show "SchoolSettings-Campus" for KIS with eBooking or eInventory
 * 2015-09-16 (Ivan): modified function Get_Top_Management_Form(), show "SchoolSettings-Timetable" for KIS with eBooking
 * 2015-06-04 (Henry): do not hide eBooking module for KIS in Get_Top_Management_Form()
 * 2014-08-14 (Carlos): do not hide Staff Attendance module for KIS in Get_Top_Management_Form()
 * 2013-08-13 (Carlos): modified Get_Top_Management_Form(), hide non KIS modules
 * 2011-11-11 (Carlos): updated Get_Role_Target_Form() 
 * 2011-10-20 (Carlos): modified Get_Role_Target_Form() - add Alumni mail target
 ****************************************************************************************************************/
include_once('role_manage.php');
include_once('user_right_target.php');
include_once('form_class_manage.php');

class role_manage_ui {
	function role_manage_ui() {
	}

	function Get_Identity_Manage() {
		global $Lang, $PATH_WRT_ROOT;

		$RoleManage = new role_manage();
		$x = '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
					<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
					<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>
					<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>

					<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" media="screen" />
					<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';

		$x .= '<div id="IdentityManageLayer">';
		$x .= $this->Get_Role_List_Table_All();
    $x .= '</div>';

    $x .= '<div class="FakeLayer"></div>';

    return $x;
	}

	function Get_Role_List_Table_All() {
		global $Lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'/includes/libinterface.php');
		$linterface = new interface_html();
		
		$x = '<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="main_content"><div class="content_top_tool">
						<div class="Conntent_tool">
							'.$linterface->GET_LNK_PRINT('#',$Lang['SysMgr']['RoleManagement']['PrintRoleDetail'],"Pop_Role_Detail('Print'); return false;","","",0).'
							<form id="ExportForm"></form>
						</div>
       	    <div class="Conntent_search">
	        		<!--<input name="" type="text"/>-->
        		</div>
           	<br style="clear:both" />
						<div class="table_board">';
		$x .= $this->Get_Role_List_Table();
		$x .= '	</div>
           	</td>
          </tr>
          </table>';

  	return $x;
	}

	function Get_Role_Form() {
		global $Lang;

		$x = '<div class="edit_pop_board edit_pop_board_simple" style="height:180px;">
						<form id="RoleForm" name"RoleForm" onsubmit="return false;">
						<h1> '.$Lang['SysMgr']['RoleManagement']['ModuleTitle'].' &gt; <span>'.$Lang['SysMgr']['RoleManagement']['NewRole'].' </span> </h1>
						<div class="edit_pop_board_write" style="height:90px;">
							<table class="form_table">
							  <tr>
					        	<td>'.$Lang['SysMgr']['RoleManagement']['Identity'].'</td>
							    <td>:</td>
							    <td >
							    	<select name="RoleType" id="RoleType">
								      <option value="">'.$Lang['SysMgr']['RoleManagement']['SelectIdentity'].'</option>
								      <option value="Teaching">'.$Lang['SysMgr']['RoleManagement']['TeachingStaff'].'</option>
								      <option value="NonTeaching">'.$Lang['SysMgr']['RoleManagement']['SupportStaff'].'</option>
								      <option value="Student">'.$Lang['SysMgr']['RoleManagement']['Student'].'</option>
								      <option value="Parent">'.$Lang['SysMgr']['RoleManagement']['Parent'].'</option>
					          </select>
					          <div id="IdentityWarningLayer" name="IdentityWarningLayer" style="display: none; color:red;"></div>
					         </td>
						      </tr>
								<col class="field_title" />
								<col  class="field_c" />
								<tr>
									<td>'.$Lang['SysMgr']['RoleManagement']['RoleTitle'].'</td>
									<td>:</td>
									<td >
										<input name="RoleName" type="text" id="RoleName" class="textbox" onkeyup="Check_Role_Name();"/>
										<div id="RoleNameWarningLayer" name="RoleNameWarningLayer" style="display: none; color:red;"></div>
									</td>
								</tr>
							</table>
						  <br />
						</div>
						<div class="edit_bottom">
							<span> </span>
							<p class="spacer"></p>
							<input id="SubmitRoleBtn" name="SubmitRoleBtn" type="button" class="formbutton" onclick="Save_Role(false);" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Submit'].'" />
							<input id="SubmitEditRoleBtn" name="SubmitEditRoleBtn" type="button" class="formbutton" onclick="Save_Role(true);" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['SysMgr']['RoleManagement']['SubmitAndEdit'].'" />
						  <input id="CancelRoleBtn" name="CancelRoleBtn" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
						  <p class="spacer"></p>
						</div>
						</form>
					</div>';

		return $x;
	}

	function Get_Role_List_Table() {
		global $Lang, $sys_custom;

		$RoleManage = new role_manage();

		$RoleList = $RoleManage->Get_Role_List();
		$roleNameStyle = ($sys_custom['DHL'])? 'style="width:50%;"' : ''; 
		$x = '<table class="common_table_list" id="RoleListTable">
						<thead>
							<tr>
								<th '.$roleNameStyle.'>'.$Lang['SysMgr']['RoleManagement']['RoleName'].'</th>';
						if ($sys_custom['DHL']) {
							$x .= '	<th style="width:40%;">'.$Lang['SysMgr']['RoleManagement']['TeachingStaff'].'</th>';
						}
						else {
								$x .= '	<th>'.$Lang['SysMgr']['RoleManagement']['TeachingStaff'].'</th>
										<th>'.$Lang['SysMgr']['RoleManagement']['SupportStaff'].'</th>';
						    if (!$sys_custom['project']['HKPF']) {
								$x .= ' <th>'.$Lang['SysMgr']['RoleManagement']['Student'].'</th>
										<th>'.$Lang['SysMgr']['RoleManagement']['Parent'].'</th>';
							}
						}
							  	
					$x .= '		<th>&nbsp;</th>
							 </tr>
						</thead>
						<tbody>';
		for ($i=0; $i< sizeof($RoleList); $i++) {
			$x .= '<tr>
							<td>
								<a href="#" class="classForRegression" onclick="Get_Role_Detail(\''.$RoleList[$i]['RoleID'].'\',\'Member\'); return false;">
								'.$RoleList[$i]['RoleName'].'
								</a>
							</td>
							<td>
								'.$RoleList[$i]['TeachingMemberCount'].'
							</td>';
			if ($sys_custom['DHL']) {
				// hide non-teaching staff, student, and parent columns for DHL	
			}
			else {
				$x .= '		<td>
								'.$RoleList[$i]['NonTeachingMemberCount'].'
							</td>';
				if (!$sys_custom['project']['HKPF']) {
    				$x .= '		<td>
    								'.$RoleList[$i]['StudentMemberCount'].'
    							</td>
    							<td>
    								'.$RoleList[$i]['ParentMemberCount'].'
    							</td>';
				}
			}
			$x .= '			<td>
								<span class="table_row_tool row_content_tool">
								<a href="#" class="edit_dim" title="'.$Lang['Btn']['Edit'].'" onclick="Get_Role_Detail(\''.$RoleList[$i]['RoleID'].'\',\'Member\'); return false;"></a>
								<a href="#" class="delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="Remove_Role(\''.$RoleList[$i]['RoleID'].'\'); return false;"></a>
								</span>
							</td>
						</tr>';
		}
		$x .= '
					<tr id="AddRoleRow">
						<td colspan="6">
							<div class="table_row_tool row_content_tool">
								<a href="#" class="add_dim" title="'.$Lang['SysMgr']['RoleManagement']['NewRole'].'" onclick="Add_Form_Row(); return false;"></a>
							</div>
						</td>
					';
		$x .= '</tbody>
				</table>';

		/*$x = '<table class="common_table_list" style="width:100%;">
						<tbody>';
		$RoleList = $RoleManage->Get_Role_List($RoleType,true);
		for ($i=0; $i< sizeof($RoleList); $i++) {
			$x .= '<tr>
					  	<td style="border-right:0px;">
					  		<span class="row_content" style="float:left;">
							  	<a href="#" onclick="Get_Role_Detail(\''.$RoleList[$i]['RoleID'].'\',\'RightTarget\'); return false;">'.$RoleList[$i]['RoleName'].' </a>
							  	( <a href="#" onclick="Get_Role_Detail(\''.$RoleList[$i]['RoleID'].'\',\'Member\'); return false;">'.$RoleList[$i]['MemberCount'].' </a>)
						  	</span>
					  	</td>
					  	<td style="width:15px; border-left:0px;">
					  		<span class="table_row_tool row_content_tool" style="float:right;">
	    						<a href="#" class="delete_dim" title="'.$Lang['SysMgr']['RoleManagement']['RemoveRole'].'" onclick="Remove_Role('.$RoleList[$i]['RoleID'].'); return false;"></a>
	    					</span>
					  	</td>
					  	</tr>';
		}
		$x .= '	</tbody>';
		$x .= '</table>';*/

		return $x;
	}

	// $Show: "RightTarget" or "Member"
	function Get_Role_Edit_Form($RoleID,$Show="Member") {
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom;
		
		include_once($PATH_WRT_ROOT.'/includes/libinterface.php');
		$linterface = new interface_html();
		
		$Role = new role($RoleID,true,true,true);

		if ($Show == "")
			$Show = "TopManagement";

		$x ='
				<table width="99%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td class="main_content">
				  <div class="navigation">
				  	<img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif" width="15" height="15" align="absmiddle" />
				  	<a href="#" onclick="Get_Role_List_Table(); return false;">'.$Lang['SysMgr']['RoleManagement']['RoleList'].'</a>
				  	<img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif" width="15" height="15" align="absmiddle" />
				  	<span id="RoleNameNavLayer">'.$Role->RoleName.'</span>
				  	<br />
				  </div>
          <div class="detail_title_box">
          	<table width="100%">
          		<tr>
          			<td width="4%" valign="top">
          				'.$Lang['SysMgr']['RoleManagement']['ModuleTitle'].':
          			</td>
          			<td width="97%" align="left">
          			<strong>
          			<div id="'.$RoleID.'" onmouseover="Show_Edit_Icon(this);" onmouseout="Hide_Edit_Icon(this);" class="jEditInput">'.$Role->RoleName.'</div>
		          	</strong>
		          	<div id="RoleNameWarningLayer" style="display:none; color:red;"></div>
          			</td>
          		</tr>
          	</table>
          </div>
		'.$linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['SysMgr']['RoleManagement']['RoleUpdateWarning']).'			
				<br style="clear:both" />
				<div class="shadetabs">
					 <ul>';
		$Selected = ($Show=="Member")? 'selected':'';
		$x .= '  <li class="'.$Selected.' SubMenu">
					   	<a href="#" onclick="$(\'li.selected\').removeClass(\'selected\'); this.parentNode.className=\'selected SubMenu\'; Switch_Layer(\'Member\'); return false;">'.$Lang['SysMgr']['RoleManagement']['UserList'].'</a>
					   </li>';
		$Selected = ($Show=="TopManagement")? 'selected':'';
		$x .= '   <li class="'.$Selected.' SubMenu">
					   	<a href="#" onclick="$(\'li.selected\').removeClass(\'selected\'); this.parentNode.className=\'selected SubMenu\'; Switch_Layer(\'TopManagement\'); return false;">'.$Lang['SysMgr']['RoleManagement']['TopManagement'].'</a>
					   </li>';
    $Selected = ($Show=="Target")? 'selected':'';
        if ($sys_custom['DHL'] || $sys_custom['HideMailTargeting']) {
    		// hide mail targeting tab for DHL since DHL does not use email	
    	}
    	else {
    		$x .= '   <li class="'.$Selected.' SubMenu">
					   	<a href="#" onclick="$(\'li.selected\').removeClass(\'selected\'); this.parentNode.className=\'selected SubMenu\'; Switch_Layer(\'Target\'); return false;">'.$Lang['SysMgr']['RoleManagement']['Targeting'].'</a>
					   </li>';
    	}
		
		$x .= ' </ul>
					</div>
          <br style="clear:both" />';


		// Member Layer
		$Display = ($Show=="Member")? '':'style="display:none;"';
		$x .= '<div id="MemberListSubLayer" '.$Display.'>';
		$x .= $this->Get_Role_Member_Form($Role);
		$x .= '</div>';

    // Top Management Layer
    $Display = ($Show == "TopManagement")? '':'style="display:none;"';
    $x .= '<div id="TopManagementDetailSubLayer" '.$Display.'>';
		$x .= $this->Get_Top_Management_Form($Role);
		$x .= '</div>';

    // Target layer
    $Display = ($Show == "TargetDetail")? '':'style="display:none;"';
    $x .= '<div id="TargetDetailSubLayer" '.$Display.'>';
		$x .= $this->Get_Role_Target_Form($Role);
		$x .= '</div>';

		$x .= '</td>
				</tr>
			</table>';

		return $x;
	}

	function Get_Add_Role_Member_Form($RoleID) {
		global $Lang, $sys_custom;

		$RoleManage = new role_manage();
		$Role = new role($RoleID,false,false,true);
		$x = '<input type="hidden" name="RoleID" id="RoleID" value="'.$RoleID.'">
					<div class="edit_pop_board" style="height:440px;">
						<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
					    <table class="form_table">
					    	<tr><td>'.$Lang['SysMgr']['RoleManagement']['Users'].': </td></tr>
							<tr>

							    <td >
							    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
				              <tr>
				              	<td>
				              		<table width="100%" border="0" cellspacing="0" cellpadding="5">
													<tr>
														<td width="50%" bgcolor="#EEEEEE">';
														
												if ($sys_custom['DHL']) {
													$x .= '&nbsp;'; 
												}
												else {
													$x .= $Lang['SysMgr']['RoleManagement']['UserType'].':
														<select name="IdentityType" id="IdentityType" onchange="Get_User_List('.$RoleID.');">
															<option value="Teaching">'.$Lang['SysMgr']['RoleManagement']['TeachingStaff'].'</option>
															<option value="NonTeaching">'.$Lang['SysMgr']['RoleManagement']['SupportStaff'].'</option>
															<option value="Student">'.$Lang['SysMgr']['RoleManagement']['Student'].'</option>
															<option value="Parent">'.$Lang['SysMgr']['RoleManagement']['Parent'].'</option>
														</select>
				';

														// get class list
														$CurrentYearTermID = getCurrentAcademicYearAndYearTerm();
														$CurrentAcademicYearID = $CurrentYearTermID['AcademicYearID'];
														$fcm = new form_class_manage();
														$YearClassList = $fcm->Get_Class_List_By_Academic_Year($CurrentAcademicYearID);
														$x .= '<select name="YearClassSelect" id="YearClassSelect" class="formtextbox" onchange="Get_User_List('.$RoleID.');" style="display:none;">';
														$x .= '<option value="">'.$Lang['SysMgr']['SubjectClassMapping']['SelectClass'].'</option>';
													  for ($i=0; $i< sizeof($YearClassList); $i++) {
													  	if ($YearClassList[$i]['YearID'] != $YearClassList[$i-1]['YearID']) {
													  		if ($i == 0)
													  			$x .= '<optgroup label="'.$YearClassList[$i]['YearName'].'">';
													  		else {
													  			$x .= '</optgroup>';
													  			$x .= '<optgroup label="'.$YearClassList[$i]['YearName'].'">';
													  		}
													  	}
												
													  	$x .= '<option value="'.$YearClassList[$i]['YearClassID'].'">'.Get_Lang_Selection($YearClassList[$i]['ClassTitleB5'],$YearClassList[$i]['ClassTitleEN']).'</option>';
												
													  	if ($i == (sizeof($YearClassList)-1))
													  		$x .= '</optgroup>';
													  }
													  $x .= '</select>';
												
														$x .= '<div id="ParentStudentLayer" name="ParentStudentLayer">';
														$x .= '</div>';
												}
												$x .= '
														</td>
													<td width="40"></td>
													<td width="50%" bgcolor="#EFFEE2" class="steptitletext">'.$Lang['SysMgr']['RoleManagement']['SelectedUser'].' </td>
												</tr>
												<tr>
												<td bgcolor="#EEEEEE" align="center">';
		// get user list with selected criteria
		$x .= '							<div id="AvalUserLayer">';
		for ($i=0; $i< sizeof($Role->MemberList); $i++) {
			$AddUserID[] = $Role->MemberList[$i]['UserID'];
		}
		$x .= $this->Get_User_Selection($AddUserID,"Teaching","","",$RoleID);
		$x .= '
												</div>';

		$x .= '
												<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].' </span>
												</td>
												<td><input name="AddAll" onclick="Add_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;&gt;" style="width:40px;" title="'.$Lang['Btn']['AddAll'].'"/>
													<br />
													<input name="Add" onclick="Add_Selected_Class_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;" style="width:40px;" title="'.$Lang['Btn']['AddSelected'].'"/>
													<br /><br />
													<input name="Remove" onclick="Remove_Selected_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;" style="width:40px;" title="'.$Lang['Btn']['RemoveSelected'].'"/>
													<br />
													<input name="RemoveAll" onclick="Remove_All_User();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;&lt;" style="width:40px;" title="'.$Lang['Btn']['RemoveAll'].'"/>
												</td>
												<td bgcolor="#EFFEE2" id="SelectedUserCell">';

			// selected User list
			$x .= '<select name="AddUserID[]" id="AddUserID[]" size="10" style="width:99%" multiple="true">';
			/*for ($i=0; $i< sizeof($Role->MemberList); $i++) {
				$x .= '<option value="'.$Role->MemberList[$i]['UserID'].'">'.$Role->MemberList[$i]['MemberName'].'('.$Role->MemberList[$i]['UserType'].')</option>';
			}*/
			$x .= '</select>';
			$x .= '						</td>
											</tr>
											<tr>
												<td width="50%" bgcolor="#EEEEEE">';
		$x .= $Lang['SysMgr']['FormClassMapping']['Or'].'<Br>';
		$x .= $Lang['SysMgr']['RoleManagement']['SearchUser'].'<br>';

		$x .= '
												<!--<div class="Conntent_search" style="float:left;">-->
												<div style="float:left;">
													<input type="text" id="UserSearch" name="UserSearch" value=""/>
												</div>
											</td>
											<td>&nbsp;</td>
										</tr>
									</table>
									<p class="spacer"></p>
	              	</td>
	              </tr>
							</table>
						  <br />
						</td>
						</tr>
						</table>
						</div>
					  <div class="edit_bottom">
							<span> </span>
							<p class="spacer"></p>
							<input name="AddMemberSubmitBtn" id="AddMemberSubmitBtn" type="button" class="formbutton" onclick="Add_Role_Member(\''.$Role->RoleID.'\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Add'].'" />
						  <input name="AddMemberCancelBtn" id="AddMemberCancelBtn" type="button" class="formbutton" onclick="window.top.tb_remove(); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
					    <p class="spacer"></p>
						</div>
					</div>';

	return $x;
	}

	function Get_Module_Manage() {
	}

	function Get_Role_Member_Form($Role) {
		global $Lang, $sys_custom, $PATH_WRT_ROOT;
		
		$rm = new role_manage();

		$x = '<div class="content_top_tool">
            <div class="Conntent_tool">
            	<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Role_Add_Member_Form(\''.$Role->RoleID.'\'); return false;" class="new thickbox" title="'.$Lang['SysMgr']['RoleManagement']['AddUser'].'"> '.$Lang['SysMgr']['RoleManagement']['New'].' </a>
            	<!--<a href="#" class="import"> Import</a>
            	<a href="#" class="export"> Export</a>-->
            </div>
            <div class="Conntent_search">
              <!--<input name="input" type="text"/>-->
            </div>
            <br style="clear:both" />
          </div>
          <div class="table_board">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
					    <td valign="bottom"><div class="table_filter"></div></td>
					    <td valign="bottom">
					    	<div class="common_table_tool">
					    		<a href="#" class="tool_delete" title="'.$Lang['SysMgr']['RoleManagement']['Delete'].'" onclick="Delete_Role_Member(\''.$Role->RoleID.'\'); return false;"></a>
					    	</div>
					    </td>
					  </tr>
					</table>
					<table class="common_table_list">
					  <thead>
					    <tr>
					      <th class="num_check">#</th>
					      <th> '.$Lang['SysMgr']['RoleManagement']['Name'].'</th>';
		if ($sys_custom['DHL']) {
			// hide class name, class number, and identity column
			$x .= '<th> '.$Lang['DHL']['Company'].'</th>
					      <th> '.$Lang['DHL']['Division'].'</th>
					      <th> '.$Lang['DHL']['Department'].'</th>';
		}
		else {
			$x .= '<th> '.$Lang['SysMgr']['RoleManagement']['Class'].'</th>
					      <th> '.$Lang['SysMgr']['RoleManagement']['ClassNumber'].'</th>
					      <th> '.$Lang['SysMgr']['RoleManagement']['UserType'].'</th>';
		}
					      
				  $x .= '    <th class="num_check" ><input type="checkbox" name="CheckAll" id="CheckAll" onclick="Set_Checkbox_Value(\'UserID[]\',this.checked);"/></th>
					      </tr>
					  </thead>
					  <tbody>';
		if ($sys_custom['DHL']) {
			include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
			$ldhl = new libdhl();
			
			$filterMap = array();
			$filterMap['user_id'] = Get_Array_By_Key($Role->MemberList, 'UserID');
			$filterMap['GetOrganizationInfo'] = 1;
			$filterMap['GetOrganizationField'] = 1;
			$userCompanyAssoAry = BuildMultiKeyAssoc($ldhl->getUserRecords($filterMap), 'UserID');
		}
					  
		for ($i=0; $i< sizeof($Role->MemberList); $i++) {
			$MemberClassInfo = $rm->Get_Member_Class_Info($Role->MemberList[$i]['UserID'],$Role->MemberList[$i]['RecordType']);
			$_company = $userCompanyAssoAry[$Role->MemberList[$i]['UserID']]['CompanyCode'];
			$_division = $userCompanyAssoAry[$Role->MemberList[$i]['UserID']]['DivisionCode'];
			$_department = $userCompanyAssoAry[$Role->MemberList[$i]['UserID']]['DepartmentCode'];
			
			$x .= '		<tr>
						      <td>'.($i+1).'</td>
						      <td>'.$Role->MemberList[$i]['MemberName'].'</td>';
			if ($sys_custom['DHL']) {
				// hide class name, class number, and identity column
				$x .= '	      <td>'.$_company.'</td>
						      <td>'.$_division.'</td>
						      <td>'.$_department.'</td>';
			}
			else {
				$x .= '	      <td>'.Get_Lang_Selection($MemberClassInfo['ClassTitleB5'],$MemberClassInfo['ClassTitleEN']).'</td>
						      <td>'.$MemberClassInfo['ClassNumber'].'</td>
						      <td>'.$Role->MemberList[$i]['UserType'].'</td>';
			}
			
			$x .= '			<td><input type="checkbox" name="UserID[]" id="UserID[]" value="'.$Role->MemberList[$i]['UserID'].'"/></td>
						   </tr>';
		}
		$x .= ' 		</tbody>
					</table>
					<p class="spacer"></p>
					  </div>
					<div class="edit_bottom">
						<!--<span> '.Get_Last_Modified_Remark($Role->DateModified,$Role->ModifiedByName).'</span>-->
						<p class="spacer"></p>
						<!--<input type="hidden" name="targetfield" id="targetfield" value=""/>-->
						<!--<input type="hidden" name="Ordering" id="Ordering" value="0"/>-->
						<!--<input name="submit2" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save" />-->
						<!--<input name="submit3" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save As new" />-->
						<!--<input name="RemoveRoleBtn" id="RemoveRoleBtn" type="button" class="formbutton" onclick="Remove_Role(\''.$Role->RoleID.'\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['SysMgr']['RoleManagement']['RemoveThisRole'].'" />-->
						<!--<input name="submit2" type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'school_setting_subject_class.htm\');return document.MM_returnValue" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Cancel" />-->
						<p class="spacer"></p>
					</div>';
					
		return $x;
	}

	/*function Get_Role_Target_Form1($Role) {
		global $Lang;

		$UserRightTarget = new user_right_target();
		$x = '<div class="table_board">
					 <table width="100%" border="0">
					  <tr>
					    <td width="75%" valign="top"><table class="common_table_list">
					    <col /><col class="num_check" />
					      <tr>
					        <th width="72%">'.$Lang['SysMgr']['RoleManagement']['Targeting'].'</th>
					        <th width="7%">'.$Lang['SysMgr']['RoleManagement']['TeachingStaff'].'</th>
									<th width="7%">'.$Lang['SysMgr']['RoleManagement']['SupportStaff'].'</th>
									<th width="7%">'.$Lang['SysMgr']['RoleManagement']['Student'].'</th>
									<th width="7%">'.$Lang['SysMgr']['RoleManagement']['Parent'].'</th>
					      </tr>

					      <tbody>';
		$TargetList = $UserRightTarget->Combine_Target_Permission_With_Identity($Role->TargetList,"Display");

		for ($i=0; $i< sizeof($TargetList); $i++) {
			$FunctionLevelArray = explode('-',$TargetList[$i]['TargetString']);
			if ($FunctionLevelArray[0] != $PreviousTargetGroup) {
				$PreviousTargetGroup = $FunctionLevelArray[0];
				$x .= '   <tr>
					          <td align="left" colspan="5"><strong>'.$FunctionLevelArray[0].'</strong></td>
					        </tr>';
			}


			$x .= '<tr class="sub_row">
		          <td align="left" class="sub_function_row">'.$TargetList[$i]['TargetName'].'</td>';

		  $checked = ($TargetList[$i]['Teaching'])? 'checked="checked"':'';
		  $Style = ($TargetList[$i]['Teaching'])? 'style="background:#EFFDDB;"':'';
		  $x .= ' <td '.$Style.'><input onclick="Swap_Style(this);" name="TeachingTarget[]" id="TeachingTarget[]" value="'.$TargetList[$i]['TargetString'].'" type="checkbox" '.$checked.'/></td>';

		  $checked = ($TargetList[$i]['NonTeaching'])? 'checked="checked"':'';
		  $Style = ($TargetList[$i]['NonTeaching'])? 'style="background:#EFFDDB;"':'';
		  $x .= ' <td '.$Style.'><input onclick="Swap_Style(this);" name="NonTeachingTarget[]" id="NonTeachingTarget[]" value="'.$TargetList[$i]['TargetString'].'" type="checkbox" '.$checked.'/></td>';

		  $checked = ($TargetList[$i]['Student'])? 'checked="checked"':'';
		  $Style = ($TargetList[$i]['Student'])? 'style="background:#EFFDDB;"':'';
		  $x .= '	<td '.$Style.'><input onclick="Swap_Style(this);" name="StudentTarget[]" id="StudentTarget[]" value="'.$TargetList[$i]['TargetString'].'" type="checkbox" '.$checked.'/></td>';

		  $checked = ($TargetList[$i]['Parent'])? 'checked="checked"':'';
		  $Style = ($TargetList[$i]['Parent'])? 'style="background:#EFFDDB;"':'';
		  $x .= ' <td '.$Style.'><input onclick="Swap_Style(this);" name="ParentTarget[]" id="ParentTarget[]" value="'.$TargetList[$i]['TargetString'].'" type="checkbox" '.$checked.'/></td>
		        </tr>';
		}
		$x .= '
					      </tbody>
					    </table></td>
					  </tr>
					 </table>
					<p class="spacer"></p>
					</div>
					<div class="edit_bottom">
						<!--<span> '.Get_Last_Modified_Remark($Role->DateModified,$Role->ModifiedByName).'</span>-->
						<p class="spacer"></p>
						<input name="SaveRoleBtn" id="SaveRoleBtn" type="button" class="formbutton" onclick="Save_Role_Right_Target(\''.$Role->RoleID.'\',\'Target\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].'" />
						<!--<input name="submit3" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save As new" />-->
						<!--<input name="RemoveRoleBtn" id="RemoveRoleBtn" type="button" class="formbutton" onclick="Remove_Role(\''.$Role->RoleID.'\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['SysMgr']['RoleManagement']['RemoveThisRole'].'" />-->
						<!--<input name="RoleDetailCancelBtn" id="RoleDetailCancelBtn" type="button" class="formbutton" onclick="Get_Role_List_Table();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />-->
						<p class="spacer"></p>
					</div>';

		return $x;
	}*/

	function Get_Role_Target_Form($Role) {
		global $Lang, $special_feature;

		$TargetList = $Role->TargetList;
		$NotSetBefore = (!isset($TargetList['All-Yes']) && !isset($TargetList['All-No']));

		$x = '<div class="table_board">
					'.$Lang['SysMgr']['RoleManagement']['ToAllUser'];

		$DetailTargetUI .= '     <tr class="sub_row">
						      								<td align="left">'.$Lang['SysMgr']['RoleManagement']['ToAll'].'</td>';
		$TeachingAllChecked = ($TargetList['Staff-AllTeaching']['RightFlag'] || $NotSetBefore)? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-AllTeaching" id="TeachingAll" onclick="Toggle_Teaching_Options(\'Teaching\',this.checked);" '.$TeachingAllChecked.'></td>';
		$NonTeachingAllChecked = ($TargetList['Staff-AllNonTeaching']['RightFlag'] || $NotSetBefore)? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-AllNonTeaching" id="NonTeachingAll" onclick="Toggle_Teaching_Options(\'NonTeaching\',this.checked);" '.$NonTeachingAllChecked.'></td>';
		$StudentAllChecked = ($TargetList['Student-All']['RightFlag'] || $NotSetBefore)? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-All" id="StudentAll" onclick="Toggle_Teaching_Options(\'Student\',this.checked);" '.$StudentAllChecked.'></td>';
		$ParentAllChecked = ($TargetList['Parent-All']['RightFlag'] || $NotSetBefore)? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-All" id="ParentAll" onclick="Toggle_Teaching_Options(\'Parent\',this.checked);" '.$ParentAllChecked.'></td>';
		if($special_feature['alumni']){
			$AlumniAllChecked = ($TargetList['Alumni-All']['RightFlag'] || $NotSetBefore)? 'checked="checked"':'';
			$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Alumni-All" id="AlumniAll" onclick="Toggle_Teaching_Options(\'Alumni\',this.checked);" '.$AlumniAllChecked.'></td>';
		
			$DetailTargetDisplay = ($TeachingAllChecked != '' && $StudentAllChecked != '' && $ParentAllChecked != '' && $AlumniAllChecked != '')? 'style="display:none;"':'';
		}else{
			$DetailTargetDisplay = ($TeachingAllChecked != '' && $StudentAllChecked != '' && $ParentAllChecked != '')? 'style="display:none;"':'';
		}
		$TeachingDetailDisplay = ($TeachingAllChecked != '')? 'style="display:none;"':'';
		$NonTeachingDetailDisplay = ($NonTeachingAllChecked != '')? 'style="display:none;"':'';
		$StudentDetailDisplay = ($StudentAllChecked != '')? 'style="display:none;"':'';
		$ParentDetailDisplay = ($ParentAllChecked != '')? 'style="display:none;"':'';
		if($special_feature['alumni']) $AlumniDetailDisplay = ($AlumniAllChecked != '')? 'style="display:none;"':'';
		
		$DetailTargetUI .= '     </tr>
					      <tbody id="SubTargetRow" '.$DetailTargetDisplay.'>
					      <tr class="sub_row">
					      	<td align="left">'.$Lang['SysMgr']['RoleManagement']['BelongToSameForm'].'</td>';
		$StaffMyFormChecked = ($TargetList['Staff-MyForm']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MyForm" class="TeachingTargetOption" '.$TeachingDetailDisplay.' '.$StaffMyFormChecked.'>&nbsp;</td>
					      	<td>&nbsp;</td>';
		$StudentMyFormChecked = ($TargetList['Student-MyForm']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MyForm" class="StudentTargetOption" '.$StudentDetailDisplay.' '.$StudentMyFormChecked.'>&nbsp;</td>';
		$ParentMyFormChecked = ($TargetList['Parent-MyForm']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MyForm" class="ParentTargetOption" '.$ParentDetailDisplay.' '.$ParentMyFormChecked.'>&nbsp;</td>';
		if($special_feature['alumni']) $DetailTargetUI .= '<td>&nbsp;</td>';
		//$AlumniMyFormChecked = ($TargetList['Alumni-MyForm']['RightFlag'])? 'checked="checked"':'';
		//$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Alumni-MyForm" class="AlumniTargetOption" '.$AlumniDetailDisplay.' '.$AlumniMyFormChecked.'>&nbsp;</td>';
		$DetailTargetUI .= '</tr>';
		$DetailTargetUI .= '<tr class="sub_row">
					      	<td align="left">'.$Lang['SysMgr']['RoleManagement']['BelongToSameFormClass'].'</td>';
		$StaffMyClassChecked = ($TargetList['Staff-MyClass']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MyClass" class="TeachingTargetOption" '.$TeachingDetailDisplay.' '.$StaffMyClassChecked.'>&nbsp;</td>
					      	<td>&nbsp;</td>';
		$StudentMyClassChecked = ($TargetList['Student-MyClass']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MyClass" class="StudentTargetOption" '.$StudentDetailDisplay.' '.$StudentMyClassChecked.'>&nbsp;</td>';
		$ParentMyClassChecked = ($TargetList['Parent-MyClass']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MyClass" class="ParentTargetOption" '.$ParentDetailDisplay.' '.$ParentMyClassChecked.'>&nbsp;</td>';
		if($special_feature['alumni']) $DetailTargetUI .= '<td>&nbsp;</td>';
		//$AlumniMyClassChecked = ($TargetList['Alumni-MyClass']['RightFlag'])? 'checked="checked"':'';
		//$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Alumni-MyClass" class="AlumniTargetOption" '.$AlumniDetailDisplay.' '.$AlumniMyClassChecked.'>&nbsp;</td>';
		$DetailTargetUI .= '</tr>
					      <tr class="sub_row">
					      	<td align="left">'.$Lang['SysMgr']['RoleManagement']['BelongToSameSubject'].'</td>';
		$StaffMySubjectChecked = ($TargetList['Staff-MySubject']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MySubject" class="TeachingTargetOption" '.$TeachingDetailDisplay.' '.$StaffMySubjectChecked.'>&nbsp;</td>
					      	<td>&nbsp;</td>';
		$StudentMySubjectChecked = ($TargetList['Student-MySubject']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MySubject" class="StudentTargetOption" '.$StudentDetailDisplay.' '.$StudentMySubjectChecked.'>&nbsp;</td>';
		$ParentMySubjectChecked = ($TargetList['Parent-MySubject']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MySubject" class="ParentTargetOption" '.$ParentDetailDisplay.' '.$ParentMySubjectChecked.'>&nbsp;</td>';
		if($special_feature['alumni']) $DetailTargetUI .= '<td>&nbsp;</td>';
		//$AlumniMySubjectChecked = ($TargetList['Alumni-MySubject']['RightFlag'])? 'checked="checked"':'';
		//$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Alumni-MySubject" class="AlumniTargetOption" '.$AlumniDetailDisplay.' '.$AlumniMySubjectChecked.'>&nbsp;</td>';
		$DetailTargetUI .= '</tr>
					      <tr class="sub_row">
					      	<td align="left">'.$Lang['SysMgr']['RoleManagement']['BelongToSameSubjectGroup'].'</td>';
		$StaffMySubjectGroupChecked = ($TargetList['Staff-MySubjectGroup']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MySubjectGroup" class="TeachingTargetOption" '.$TeachingDetailDisplay.' '.$StaffMySubjectGroupChecked.'>&nbsp;</td>
					      	<td>&nbsp;</td>';
		$StudentMySubjectGroupChecked = ($TargetList['Student-MySubjectGroup']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MySubjectGroup" class="StudentTargetOption" '.$StudentDetailDisplay.' '.$StudentMySubjectGroupChecked.'>&nbsp;</td>';
		$ParentMySubjectGroupChecked = ($TargetList['Parent-MySubjectGroup']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MySubjectGroup" class="ParentTargetOption" '.$ParentDetailDisplay.' '.$ParentMySubjectGroupChecked.'>&nbsp;</td>';
		if($special_feature['alumni']) $DetailTargetUI .= '<td>&nbsp;</td>';
		//$AlumniMySubjectGroupChecked = ($TargetList['Alumni-MySubjectGroup']['RightFlag'])? 'checked="checked"':'';
		//$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Alumni-MySubjectGroup" class="AlumniTargetOption" '.$AlumniDetailDisplay.' '.$AlumniMySubjectGroupChecked.'>&nbsp;</td>';
		$DetailTargetUI .= '</tr>
					      <tr class="sub_row">
					      	<td align="left">'.$Lang['SysMgr']['RoleManagement']['BelongToSameGroup'].'</td>';
		$StaffMyGroupChecked = ($TargetList['Staff-MyGroup']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Staff-MyGroup" class="TeachingTargetOption" '.$TeachingDetailDisplay.' '.$StaffMyGroupChecked.'>&nbsp;</td>';
		$NonTeachingMyGroupChecked = ($TargetList['NonTeaching-MyGroup']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="NonTeaching-MyGroup" class="NonTeachingTargetOption" '.$NonTeachingDetailDisplay.' '.$NonTeachingMyGroupChecked.'>&nbsp;</td>';
		$StudentMyGroupChecked = ($TargetList['Student-MyGroup']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Student-MyGroup" class="StudentTargetOption" '.$StudentDetailDisplay.' '.$StudentMyGroupChecked.'>&nbsp;</td>';
		$ParentMyGroupChecked = ($TargetList['Parent-MyGroup']['RightFlag'])? 'checked="checked"':'';
		$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Parent-MyGroup" class="ParentTargetOption" '.$ParentDetailDisplay.' '.$ParentMyGroupChecked.'>&nbsp;</td>';
		if($special_feature['alumni']){
			$AlumniMyGroupChecked = ($TargetList['Alumni-MyGroup']['RightFlag'])? 'checked="checked"':'';
			$DetailTargetUI .= '      	<td><input type="checkbox" name="Target[]" value="Alumni-MyGroup" class="AlumniTargetOption" '.$AlumniDetailDisplay.' '.$AlumniMyGroupChecked.'>&nbsp;</td>';
		}
		$DetailTargetUI .= '</tr>
					      ';
					      
		$AlumniMyGroupCheckedNotNULL = true;
		if($special_feature['alumni']){
			$AlumniMyGroupCheckedNotNULL = $AlumniMyGroupChecked != '';
		}
		$AllChecked = (
									($TeachingAllChecked != '' &&
									$NonTeachingAllChecked != '' &&
									$StudentAllChecked != '' &&
									$ParentAllChecked != '' && 
									$AlumniAllChecked != '' && 
									$StaffMyFormChecked != '' &&
									$StudentMyFormChecked != '' &&
									$ParentMyFormChecked != '' && 
									//$AlumniMyFormChecked != '' && 
									$StaffMyClassChecked != '' &&
									$StudentMyClassChecked != '' &&
									$ParentMyClassChecked != '' && 
									//$AlumniMyClassChecked != '' && 
									$StaffMySubjectChecked != '' &&
									$StudentMySubjectChecked != '' &&
									$ParentMySubjectChecked != '' && 
									//$AlumniMySubjectChecked != '' && 
									$StaffMySubjectGroupChecked != '' &&
									$StudentMySubjectGroupChecked != '' &&
									$ParentMySubjectGroupChecked != '' && 
									//$AlumniMySubjectGroupChecked != '' && 
									$StaffMyGroupChecked != '' &&
									$NonTeachingMyGroupChecked != '' &&
									$StudentMyGroupChecked != '' &&
									$ParentMyGroupChecked != '' && 
									$AlumniMyGroupCheckedNotNULL) ||
									//$NotSetBefore ||
									$TargetList['All-Yes']['RightFlag']
									)? 'checked="checked"':'';
		$AllNoChecked = ($TargetList['All-No']['RightFlag'])? 'checked="checked"':'';
		$HeaderUI .= '	<input type="Radio" name="Target[]" id="TargetAll" value="All-Yes" '.$AllChecked.' onclick="$(\'div#TargetTable\').hide(\'slow\');"> <label for="TargetAll">'.$Lang['General']['Yes'].'</label>
						<input type="Radio" name="Target[]" id="TargetNone" value="All-No" '.$AllNoChecked.' onclick="$(\'div#TargetTable\').show(\'slow\');"> <label for="TargetNone">'.$Lang['General']['No'].'</label>';

		$TableHeaderDisplay = ($AllChecked != '' || $NotSetBefore)? 'display:none;':'';
		$HeaderUI .= '	<div id="TargetTable" style="width:100%; '.$TableHeaderDisplay.'">
					 <table width="100%" border="0" id="TargetTable">
					  <tr>
					    <td width="75%" valign="top"><table class="common_table_list">
					    <col /><col class="num_check" />
					      <tr>
					        <th width="65%">'.$Lang['SysMgr']['RoleManagement']['CanSendTo'].'</th>
					        <th width="7%">'.$Lang['SysMgr']['RoleManagement']['TeachingStaff'].'</th>
									<th width="7%">'.$Lang['SysMgr']['RoleManagement']['SupportStaff'].'</th>
									<th width="7%">'.$Lang['SysMgr']['RoleManagement']['Student'].'</th>
									<th width="7%">'.$Lang['SysMgr']['RoleManagement']['Parent'].'</th>';
		if($special_feature['alumni']) $HeaderUI .= '<th width="7%">'.$Lang['SysMgr']['RoleManagement']['Alumni'].'</th>';
		$HeaderUI .= '</tr>';

		$x .= $HeaderUI.$DetailTargetUI;
		/*
		$UserRightTarget = new user_right_target();
		for ($i=0; $i< sizeof($TargetList); $i++) {
			$FunctionLevelArray = explode('-',$TargetList[$i]['TargetString']);
			if ($FunctionLevelArray[0] != $PreviousTargetGroup) {
				$PreviousTargetGroup = $FunctionLevelArray[0];
				$x .= '   <tr>
					          <td align="left" colspan="5"><strong>'.$FunctionLevelArray[0].'</strong></td>
					        </tr>';
			}


			$x .= '<tr class="sub_row">
		          <td align="left" class="sub_function_row">'.$TargetList[$i]['TargetName'].'</td>';

		  $checked = ($TargetList[$i]['Teaching'])? 'checked="checked"':'';
		  $Style = ($TargetList[$i]['Teaching'])? 'style="background:#EFFDDB;"':'';
		  $x .= ' <td '.$Style.'><input onclick="Swap_Style(this);" name="TeachingTarget[]" id="TeachingTarget[]" value="'.$TargetList[$i]['TargetString'].'" type="checkbox" '.$checked.'/></td>';

		  $checked = ($TargetList[$i]['NonTeaching'])? 'checked="checked"':'';
		  $Style = ($TargetList[$i]['NonTeaching'])? 'style="background:#EFFDDB;"':'';
		  $x .= ' <td '.$Style.'><input onclick="Swap_Style(this);" name="NonTeachingTarget[]" id="NonTeachingTarget[]" value="'.$TargetList[$i]['TargetString'].'" type="checkbox" '.$checked.'/></td>';

		  $checked = ($TargetList[$i]['Student'])? 'checked="checked"':'';
		  $Style = ($TargetList[$i]['Student'])? 'style="background:#EFFDDB;"':'';
		  $x .= '	<td '.$Style.'><input onclick="Swap_Style(this);" name="StudentTarget[]" id="StudentTarget[]" value="'.$TargetList[$i]['TargetString'].'" type="checkbox" '.$checked.'/></td>';

		  $checked = ($TargetList[$i]['Parent'])? 'checked="checked"':'';
		  $Style = ($TargetList[$i]['Parent'])? 'style="background:#EFFDDB;"':'';
		  $x .= ' <td '.$Style.'><input onclick="Swap_Style(this);" name="ParentTarget[]" id="ParentTarget[]" value="'.$TargetList[$i]['TargetString'].'" type="checkbox" '.$checked.'/></td>
		        </tr>';
		}*/
		$x .= '
					      </tbody>
					    </table></td>
					  </tr>
					 </table>
					 </div>
					<p class="spacer"></p>
					</div>
					<div class="edit_bottom">
						<!--<span> '.Get_Last_Modified_Remark($Role->DateModified,$Role->ModifiedByName).'</span>-->
						<p class="spacer" style="text-align:left"></p>
						<div class="tabletext" style="color:red; float:left;">*</div><div class="tabletext" style="float:left;">'.$Lang['SysMgr']['RoleManagement']['UncheckForMoreOptions'].'</div>
						<input name="SaveRoleBtn" id="SaveRoleBtn" type="button" class="formbutton" onclick="Save_Role_Right_Target(\''.$Role->RoleID.'\',\'Target\',\''.$NotSetBefore.'\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].'" />
						<!--<input name="submit3" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save As new" />-->
						<!--<input name="RemoveRoleBtn" id="RemoveRoleBtn" type="button" class="formbutton" onclick="Remove_Role(\''.$Role->RoleID.'\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['SysMgr']['RoleManagement']['RemoveThisRole'].'" />-->
						<!--<input name="RoleDetailCancelBtn" id="RoleDetailCancelBtn" type="button" class="formbutton" onclick="Get_Role_List_Table();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />-->
						<p class="spacer"></p>
					</div>';

		return $x;
	}

	function Get_Top_Management_Form($Role) {
		global $Lang, $plugin, $sys_custom, $special_feature;

		$isKIS = $_SESSION["platform"]=="KIS";
		$applyHideModules = ($isKIS || $sys_custom['DHL'] || $sys_custom['PowerClass'])? true : false;

		$hideModuleGroups = array("eLearning");
		$hideModules = array("eAdmin-eDiscipline",
							"eAdmin-ePolling","eAdmin-eReportCard","eAdmin-eReportCard_Rubrics","eAdmin-eSportsAdmin","eAdmin-eSurvey","eAdmin-LessonAttendance",
							"eLearning-eClass","eLearning-IES","eLearning-ReadingScheme","eLearning-SBA","eLearning-SubjecteResources","eLearning-iTextbook","eLearning-W2","eLearning-ePost", 
							"other-campusLink","other-iCalendar");
							
		if($sys_custom['DHL'] || ($isKIS && !$sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'])) {
			$hideModules[] = "SchoolSettings-Subject";
		}
		if($sys_custom['DHL'] || ($isKIS && !$sys_custom['KIS_eHomework']['EnableModule'])) {
			$hideModules[] = "eAdmin-eHomework";
		}
		
		if($sys_custom['DHL'] || ($isKIS && !$sys_custom['KIS_eEnrolment']['EnableModule'])) {
		    $hideModules[] = "eAdmin-eEnrolment";
		}
		
		if ($isKIS && !$plugin['eBooking']) {
			// hide timetable for KIS without eBooking module
			$hideModules[] = "SchoolSettings-Timetable";
		}
		if ($isKIS && !$plugin['Inventory'] && !$plugin['eBooking']) {
			$hideModules[] = "SchoolSettings-Campus";
		}
		
		if($sys_custom['project']['NCS']){
			$hideModules[] = "SchoolSettings-Campus";
			$hideModules[] = "SchoolSettings-Group";
		}
		else if ($sys_custom['DHL']) {
			$hideModules[] = "SchoolSettings-Campus";
			$hideModules[] = "SchoolSettings-Class";
			$hideModules[] = "SchoolSettings-SchoolCalendar";
			$hideModules[] = "SchoolSettings-Subject";
			$hideModules[] = "SchoolSettings-Timetable";
			
			$hideModules[] = "eAdmin-eBooking";
			$hideModules[] = "eAdmin-eInventory";
			$hideModules[] = "eAdmin-LibraryMgmtSystem";
			$hideModules[] = "eAdmin-eNotice";
			$hideModules[] = "eAdmin-ePayment";
			$hideModules[] = "eAdmin-ePOS";
			$hideModules[] = "eAdmin-ePCM";
			$hideModules[] = "eAdmin-SLRS";
			$hideModules[] = "eAdmin-StudentAttendance";
			$hideModules[] = "eAdmin-eAppraisal";
		}
		else {
			if($sys_custom['PowerClass']){
                $hideModuleGroups = array();
                $hideModules = array(
                    "eAdmin-ePolling","eAdmin-eSurvey",
                    "eLearning-IES","eLearning-ReadingScheme","eLearning-SBA","eLearning-SubjecteResources","eLearning-iTextbook","eLearning-W2","eLearning-ePost","eLearning-FlippedChannels",
                    "other-campusLink"
                );
			}
			if($isKIS && !$special_feature['circular']){
				$hideModules[] = "eAdmin-eCircular";
			}
		}
		
		$UserRightTarget = new user_right_target();
		$x = '<div class="table_board">
					 <table width="100%" border="0">
					  <tr>
					    <td width="75%" valign="top">
					    <table class="common_table_list">
					    	<col /><col class="num_check" />
					      <tr>
					        <th>'.$Lang['SysMgr']['RoleManagement']['AccessRight'].'</th>
					        <th><input type="checkbox" id="moduleGroupChkGlobal" onclick="Check_All_Options_By_Class(\'moduleGroupChk\', this.checked, true); Check_All_Options_By_Class(\'moduleChk\', this.checked, true);" /></th>
					      </tr>
					      <tbody>';

		$RightList = $UserRightTarget->Combine_Right_Permission($Role->RightList);

		for ($i=0; $i< sizeof($RightList); $i++) {
			$FunctionLevelArray = explode('-',$RightList[$i][0]);
			
			//if($isKIS && in_array($FunctionLevelArray[0],$hideModuleGroups)){
			if ($applyHideModules && in_array($FunctionLevelArray[0],$hideModuleGroups)){
				continue;
			}
			if ($FunctionLevelArray[0] != $PreviousModuleGroup) {
				$PreviousModuleGroup = $FunctionLevelArray[0];

				$ModuleGroupDisplay = $Lang['SysMgr']['RoleManagement'][$PreviousModuleGroup];
//				$x .= '   <tr>
//					          <td align="left"><strong>'.$ModuleGroupDisplay.'</strong></td>
//					          <td>&nbsp;</td>
//					        </tr>';
				$x .= '   <tr>
					          <td align="left"><strong>'.$ModuleGroupDisplay.'</strong></td>
					          <td><input type="checkbox" id="moduleGroupChk_'.$PreviousModuleGroup.'" class="moduleGroupChk" onclick="Check_All_Options_By_Class(\'moduleChk_'.$PreviousModuleGroup.'\', this.checked, true); Uncheck_SelectAll(\'moduleGroupChkGlobal\', this.checked);" /></td>
					        </tr>';
			}

			$checked = ($RightList[$i][2])? 'checked="checked"':'';
			$Style = ($RightList[$i][2])? 'selected_row':'sub_row';

			# 20090824 Ivan
			# Display the module name in grey and disable the checkbox if the client has not bought the module yet
			$isEnabledPlugin = $RightList[$i][4];
			$FontStyle = '';
			$ChkboxDisabled = '';
			if (!$isEnabledPlugin)
			{
				$FontStyle = 'style="color:#A6A6A6"';
				$ChkboxDisabled = 'disabled';
			}
			
			//if($isKIS && in_array($RightList[$i][0],$hideModules)){
			if ($applyHideModules && in_array($RightList[$i][0],$hideModules)){
				continue;
			}
			$x .= '<tr class="'.$Style.'">
		          <td align="left" class="sub_function_row" '.$FontStyle.'><label for="Right'.$i.'">'.$RightList[$i][1].'</label></td>
		          <td><input type="checkbox" id="Right'.$i.'" name="Right[]" class="moduleChk moduleChk_'.$PreviousModuleGroup.'" onclick="Swap_Style(this); Uncheck_SelectAll(\'moduleGroupChk_'.$PreviousModuleGroup.'\', this.checked); Uncheck_SelectAll(\'moduleGroupChkGlobal\', this.checked);" value="'.$RightList[$i][0].'" '.$checked.' '.$ChkboxDisabled.' /></td>
		        </tr>';
		}

		$x .= '   </tbody>
					    </table>
					   </td>
					 </tr>
					</table>
					<p class="spacer"></p>
				</div>
				<div class="edit_bottom">
					<!--<span> '.Get_Last_Modified_Remark($Role->DateModified,$Role->ModifiedByName).'</span>-->
					<p class="spacer"></p>
					<input name="SaveRoleBtn" id="SaveRoleBtn" type="button" class="formbutton" onclick="Save_Role_Right_Target(\''.$Role->RoleID.'\',\'Right\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Save'].'" />
					<!--<input name="submit3" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Save As new" />-->
					<!--<input name="RemoveRoleBtn" id="RemoveRoleBtn" type="button" class="formbutton" onclick="Remove_Role(\''.$Role->RoleID.'\'); return false;" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['SysMgr']['RoleManagement']['RemoveThisRole'].'" />-->
					<!--<input name="RoleDetailCancelBtn" id="RoleDetailCancelBtn" type="button" class="formbutton" onclick="Get_Role_List_Table();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />-->
					<p class="spacer"></p>
				</div>';
		return $x;
	}

	function Get_User_Selection($AddUserID,$IdentityType,$YearClassID,$ParentStudentID,$RoleID) {
		global $Lang;

		if ($AddUserID[0] == "")
			$AddUserID = array();

		$RoleManage = new role_manage();

		$UserList = $RoleManage->Get_Avaliable_User_List($AddUserID,$IdentityType,$YearClassID,$ParentStudentID,$RoleID);

		if ($IdentityType == "Parent" && $YearClassID != "" && trim($ParentStudentID) == "") {
			$x .= '<select name="ParentStudentID" id="ParentStudentID" style="width:99%" onchange="Get_User_List('.$RoleID.');">';
			$x .= '<option value="all">'.$Lang['SysMgr']['RoleManagement']['SelectAll'].'</option>';
		}
		else {
			$x .= '<select name="AvalUserList[]" id="AvalUserList[]" size="10" style="width:99%" multiple="true">';
		}
		for ($i=0; $i< sizeof($UserList); $i++) {
			$x .= '<option value="'.$UserList[$i]['UserID'].'">';
			$x .= $UserList[$i]['Name'];
			$x .= '</option>';
		}
		$x .= '</select>';

		return $x;
	}
	
	function Get_Role_Detail($Mode="Print") {
		global $Lang,$PATH_WRT_ROOT,$MODULE_OBJ,$LAYOUT_SKIN;

		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		
		include_once($PATH_WRT_ROOT."includes/role_manage.php");
		include_once($PATH_WRT_ROOT."includes/user_right_target.php");
		$RoleManage = new role_manage();
		$UserRightTarget = new user_right_target();
		
		$RoleList = $RoleManage->Get_Role_List(false);
		
		$RoleCount = sizeof($RoleList);
		

		for ($i=0; $i< $RoleCount; $i++) {
			$Role = new role($RoleList[$i]['RoleID'],true,true,true);
			$RoleName = $Role->RoleName;
			
			$table_content .= "<div style=\"width:100%; text-align:left;\"><b>".$Lang['SysMgr']['RoleManagement']['RoleName'].": </b>".$RoleName.'</div>';
			$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"eSporttableborder\" align='center'>\n";
			$table_content .= "<tr class=\"eSporttdborder eSportprinttabletitle\">";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"33%\"><b>".$Lang['SysMgr']['RoleManagement']['TopManagement']."</b></td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"33%\"><b>".$Lang['SysMgr']['RoleManagement']['Targeting']."</b></td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" width=\"34%\"><b>".$Lang['SysMgr']['RoleManagement']['UserList']."</b></td>";
			$table_content .= "</tr>";
			
			
			// Right List
			$table_content .= "<tr>
												<td class=\"eSporttdborder\" valign=\"top\">
												<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align='center'>";
			for ($j=0; $j< sizeof($Role->RightList); $j++) {
				$RightArray = explode('-',$Role->RightList[$j]['FunctionName']);
				if (sizeof($RightArray) == 2) 
					$RightName = $UserRightTarget->SysFunctionList[$RightArray[0]][$RightArray[1]][0];
				else 
					$RightName = "";
			
				$table_content .= "<tr>";
				$table_content .= "<td class='eSportprinttext'>".$RightName."&nbsp;</td>";
				$table_content .= "</tr>";
			}
			$table_content .= "</table>";
			$table_content .= "</td>";
			
			// Target List
			$TargetList = (is_array($Role->TargetList))? array_keys($Role->TargetList):array();
			$AllTeachingEnabled = in_array("Staff-AllTeaching",$TargetList);
			$AllNonTeachingEnabled = in_array("Staff-AllNonTeaching",$TargetList);
			$AllParentEnabled = in_array("Student-All",$TargetList);
			$AllStudentEnabled = in_array("Parent-All",$TargetList);
			$table_content .= "<td class=\"eSporttdborder\" valign=\"top\">
												<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align='center'>";
			for ($j=0; $j< sizeof($TargetList); $j++) {
				if ($TargetList[$j] == "All-No") continue;
				
				$TargetArray = explode('-',$TargetList[$j]);
				if ($TargetArray[0] == "Staff" && $AllTeachingEnabled && $TargetList[$j] != "Staff-AllNonTeaching" && $TargetList[$j] != "Staff-AllTeaching") continue;
				if ($TargetArray[0] == "NonTeaching" && $AllNonTeachingEnabled && $TargetList[$j] != "Staff-AllNonTeaching") continue;
				if ($TargetArray[0] == "Student" && $AllStudentEnabled && $TargetList[$j] != "Student-All") continue;
				if ($TargetArray[0] == "Parent" && $AllParentEnabled && $TargetList[$j] != "Parent-All") continue;
				switch ($TargetArray[0]) {
					case "Staff":
						$Target = $Lang['SysMgr']['RoleManagement']['TeachingStaff'];
						break;
					case "NonTeaching":
						$Target = $Lang['SysMgr']['RoleManagement']['SupportStaff'];
						break;
					case "Student":
						$Target = $Lang['SysMgr']['RoleManagement']['Student'];
						break;
					case "Parent":
						$Target = $Lang['SysMgr']['RoleManagement']['Parent'];
						break;
					default:
						$Target = "";
						break;
				}
				
				switch ($TargetArray[1]) {
					case 'AllTeaching':
						$TargetName = str_replace("...",$Lang['SysMgr']['RoleManagement']['TeachingStaff'],$Lang['SysMgr']['RoleManagement']['ToAll']);
						break;
					case 'AllNonTeaching':
						$TargetName = str_replace("...",$Lang['SysMgr']['RoleManagement']['SupportStaff'],$Lang['SysMgr']['RoleManagement']['ToAll']);
						break;
					case 'MyForm':
						$TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameForm']);
						break;
					case 'MyClass':
						$TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameFormClass']);
						break;
					case 'MySubject':
						$TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameSubject']);
						break;
					case 'MySubjectGroup':
						$TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameSubjectGroup']);
						break;
					case 'MyGroup':
						$TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameGroup']);
						break;
					default: 
						$TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['ToAll']);
						break;
				}
				$table_content .= "<tr>";
				$table_content .= "<td class='eSportprinttext'>".$TargetName."&nbsp;</td>";
				$table_content .= "</tr>";
				
				if ($TargetList[$j] == "All-Yes") break;
			}
			$table_content .= "</table>";
			$table_content .= "</td>";
			
			// Member List
			$table_content .= "<td class=\"eSporttdborder\" valign=\"top\">
													<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align='center'>";
			for ($j=0; $j< sizeof($Role->MemberList); $j++) {
				if ($Role->MemberList[$j]['RecordType'] == "2") {
					$MemberName = $Role->MemberList[$j]['StudentMemberName'];
				}
				else {
					if ($Role->MemberList[$j]['RecordType'] == "3") {
						$MemberName = $Role->MemberList[$j]['ParentMemberName'];
					}
					else {
						$MemberName = $Role->MemberList[$j]['MemberName'];
					}
				}

				$table_content .= "<tr>";
				$table_content .= "<td class='eSportprinttext'>".$MemberName."&nbsp;</td>";
				$table_content .= "</tr>";
			}
			$table_content .= "</table>
											</td>
										</tr>
									</table>
									<br><br><br>";
		}
		
		$print_btn_table ="<table width=\"100%\" align=\"center\" class=\"print_hide\" border=\"0\"><tr><td align=\"right\">".$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2")."</td></tr></table><BR>";
		
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		echo $print_btn_table;
		echo $table_content;
	}

function Get_Role_Detail_Ex($Mode = 'Export'){
    global $Lang, $PATH_WRT_ROOT, $MODULE_OBJ, $LAYOUT_SKIN;
    
    include_once($PATH_WRT_ROOT."includes/libinterface.php");
    include_once($PATH_WRT_ROOT."includes/user_right_target.php");
    include_once($PATH_WRT_ROOT."includes/libexporttext.php");
    
    $RoleManage = new role_manage();
    $UserRightTarget = new user_right_target();
    $lexport = new libexporttext();
    
    $RoleList = $RoleManage->Get_Role_List(false);
    $RoleCount = sizeof($RoleList);
    
    $headerAry = array();
    $dataAry = array();
    
    // loop the RoleList until find the last element of it
    for ($no = 0, $i = 0; $no < $RoleCount; $no++, $i++){
            $Role = new role($RoleList[$no]['RoleID'],true,true,true);
            //debug_pr($Role);
            //Role name
            $RoleName = $Role->RoleName;
            $dataAry[$i++][0] = $Lang['SysMgr']['RoleManagement']['RoleName'] . ": " .  $RoleName;
            $dataAry[$i][0] = $Lang['SysMgr']['RoleManagement']['TopManagement'];
            $dataAry[$i][1] = $Lang['SysMgr']['RoleManagement']['Targeting'];
            $dataAry[$i][2] = $Lang['SysMgr']['RoleManagement']['UserList'];
            $i++;
            $_col = 0;
            
            // Right List
            for ($j=0; $j< sizeof($Role->RightList); $j++) {
                $RightArray = explode('-',$Role->RightList[$j]['FunctionName']);
                //debug_pr($RightArray);
                if (sizeof($RightArray) == 2)
                    $RightName = $UserRightTarget->SysFunctionList[$RightArray[0]][$RightArray[1]][0];
                else{
                    $RightName = "";
                }   
                $dataAry[$i][$_col] .= $RightName . "\n";      
                //debug_pr($UserRightTarget->SysFunctionList[$RightArray[0]][$RightArray[1]][0]);
            }
            //debug(sizeof($Role->RightList));

            $_col++;
            
            if (sizeof($RightArray) == 2)
                $RightName = $UserRightTarget->SysFunctionList[$RightArray[0]][$RightArray[1]][0];
                else
                    $RightName = "";
            
            // Target List
            $TargetList = (is_array($Role->TargetList))? array_keys($Role->TargetList):array();
            $AllTeachingEnabled = in_array("Staff-AllTeaching",$TargetList);
            $AllNonTeachingEnabled = in_array("Staff-AllNonTeaching",$TargetList);
            $AllParentEnabled = in_array("Student-All",$TargetList);
            $AllStudentEnabled = in_array("Parent-All",$TargetList);
            
            for ($j=0; $j< sizeof($TargetList); $j++) {                
                if ($TargetList[$j] == "All-No") continue;
                
                $TargetArray = explode('-',$TargetList[$j]);
                if ($TargetArray[0] == "Staff" && $AllTeachingEnabled && $TargetList[$j] != "Staff-AllNonTeaching" && $TargetList[$j] != "Staff-AllTeaching") continue;
                if ($TargetArray[0] == "NonTeaching" && $AllNonTeachingEnabled && $TargetList[$j] != "Staff-AllNonTeaching") continue;
                if ($TargetArray[0] == "Student" && $AllStudentEnabled && $TargetList[$j] != "Student-All") continue;
                if ($TargetArray[0] == "Parent" && $AllParentEnabled && $TargetList[$j] != "Parent-All") continue;
                switch ($TargetArray[0]) {
                    case "Staff":
                        $Target = $Lang['SysMgr']['RoleManagement']['TeachingStaff'];
                        break;
                    case "NonTeaching":
                        $Target = $Lang['SysMgr']['RoleManagement']['SupportStaff'];
                        break;
                    case "Student":
                        $Target = $Lang['SysMgr']['RoleManagement']['Student'];
                        break;
                    case "Parent":
                        $Target = $Lang['SysMgr']['RoleManagement']['Parent'];
                        break;
                    default:
                        $Target = "";
                        break;
                }
                
                switch ($TargetArray[1]) {
                    case 'AllTeaching':
                        $TargetName = str_replace("...",$Lang['SysMgr']['RoleManagement']['TeachingStaff'],$Lang['SysMgr']['RoleManagement']['ToAll']);
                        break;
                    case 'AllNonTeaching':
                        $TargetName = str_replace("...",$Lang['SysMgr']['RoleManagement']['SupportStaff'],$Lang['SysMgr']['RoleManagement']['ToAll']);
                        break;
                    case 'MyForm':
                        $TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameForm']);
                        break;
                    case 'MyClass':
                        $TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameFormClass']);
                        break;
                    case 'MySubject':
                        $TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameSubject']);
                        break;
                    case 'MySubjectGroup':
                        $TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameSubjectGroup']);
                        break;
                    case 'MyGroup':
                        $TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['BelongToSameGroup']);
                        break;
                    default:
                        $TargetName = str_replace("...",$Target,$Lang['SysMgr']['RoleManagement']['ToAll']);
                        break;
                }
                //debug_pr($Lang['SysMgr']['RoleManagement']['ToAll']);
                //debug_pr($TargetName);
                $dataAry[$i][$_col] .= $TargetName;
                //debug_pr($dataAry[$i][$_col]);
                if ($TargetList[$j] == "All-Yes") break;
            }
            $_col++;
            // Member List
            $table_content .= "<td class=\"eSporttdborder\" valign=\"top\">
													<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align='center'>";
            for ($j=0; $j< sizeof($Role->MemberList); $j++) {
                if ($Role->MemberList[$j]['RecordType'] == "2") {
                    $MemberName = $Role->MemberList[$j]['StudentMemberName'];
                }
                else {
                    if ($Role->MemberList[$j]['RecordType'] == "3") {
                        $MemberName = $Role->MemberList[$j]['ParentMemberName'];
                    }
                    else {
                        $MemberName = $Role->MemberList[$j]['MemberName'];
                    }
                }
                $dataAry[$i][$_col] .= $MemberName . "\n";
            }
        $_col++;          
        $dataAry[++$i][0] = null;
    
    
    }
//     debug_pr($RoleCount);
//     debug_pr(sizeof($Role->RightList));
//     debug_pr(sizeof($TargetList));
//     debug_pr(sizeof($Role->MemberList));
//     debug_pr($i);
    $exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);
    //debug_pr($exportContent);
//     debug_pr(count($dataAry));
    //debug_pr($dataAry);
    $fileName = 'export_role_detail.csv';
    $lexport->EXPORT_FILE($fileName, $exportContent);
}
}
?>