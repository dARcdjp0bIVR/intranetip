<?php
// page editing by: 
/*
Change History: 
2018/02/05 (Carlos): fixed Get_Close_Parent_Pos($str, $start), return start position if cannot find the close bracket position to avoid parent code dead loop.
2014/10/27 (Carlos): modified Go_To_Folder(), escape " and \ in folder name when executing raw IMAP command
2014/06/10 (Carlos): modified &_structure_part() - add charset case "x-unknown"
2014/02/25 (Carlos): modified &_structure_part() - use iconv() to do BIG5-HKSCS convertion 
2012/07/27 (Carlos): discard an element part of imap_mime_header_decode() that is \t,\n,\r\n or space
					before php 5.3, imap_mime_header_decode() will not discard \t
2012/05/08 (Carlos): added Get_UID_ARRAY() to batch find UID from msg nos
2012/04/17 (Carlos): modified Check_Quota(), return quota precise to 2 floating point numbers
2011/08/11 (Carlos): modified _structure_part(), pad UTF-8 charset to filename_charset to avoid error/warning msg
2011/04/14 (Carlos): modified Get_Message_Cache_By_Batch(), add header item X-eClass-MailID
2011/04/13 (Carlos): added destructor to close socket connection 
2011/03/18 (Carlos): Fixed the parsed logic of Get_Structure_Array() and Get_Close_Parent_Pos() - mis-treat escaped \" as a pair of "".

2008/8/9
imap_socket(): added checking for $SYS_CONFIG['Mail']['ExchangeUsernameLogin']

2008/8/5 
Get_Message_Cache(): 

*/
//include_once("common_function.php");
include_once("imap_message_part.php");
//include_once("database.php");
//include_once("gmail_api_agent.php");

class imap_socket 
{
	var $ServerHost;
	var $ServerPort;
	var $ServerType;
	var $SocketStream;
	var $CurUserPassword;
  var $CurUserAddress;
  var $DebugMode; // false in normal situation, true only for debug, when true, will echo all command pass to server and response from server
  var $ShowTime;
  var $Silent;
  var $ShowLevel;
  var $UserType="T";
  var $RootPrefix;
  var $FolderDelimiter;
  var $FolderPrefix;
  var $UserNameSuffix;
	
	var $GmailMode = false; // use Gmail Imap Service
	function imap_socket($TargetUser="",$TargetPassword="",$UserType="") {
		global $SYS_CONFIG;
		
		$this->UserType = (trim($UserType) != "")? $UserType:$_SESSION['SSV_USER_TYPE'];
		
//		if ($this->UserType == 'S') {
//			$this->FolderPrefix = $SYS_CONFIG['StudMail']['FolderPrefix'];
//			$this->FolderDelimiter = $SYS_CONFIG['StudMail']['FolderDelimiter'];
//			$this->RootPrefix = ($SYS_CONFIG['StudMail']['FolderPrefix'] == "")? "":$SYS_CONFIG['StudMail']['FolderPrefix'].$this->FolderDelimiter;
//			$this->ServerHost = 'ssl://'.$SYS_CONFIG['StudMail']['ServerIP'];
//			//$this->ServerHost = $SYS_CONFIG['StudMail']['ServerIP'];
//	    $this->ServerPort = $SYS_CONFIG['StudMail']['Port'];
//	    $this->ServerType = $SYS_CONFIG['StudMail']['ServerType'];
//	    $this->CurUserName = substr($_SESSION['SSV_LOGIN_EMAIL'],0,strpos($_SESSION['SSV_LOGIN_EMAIL'],'@'));
//	    $this->CurUserPassword = ($TargetPassword == "")? $_SESSION['SSV_EMAIL_PASSWORD']:$TargetPassword;
//			$this->UserNameSuffix = $SYS_CONFIG['StudMail']['UserNameSubfix'];
//	    if ($TargetUser == "") 
//	    	$this->CurUserAddress = $_SESSION['SSV_LOGIN_EMAIL'];
//	    else 
//	    	$this->CurUserAddress = $TargetUser;
//		}
//		else {
		if($_SESSION['SSV_LOGIN_EMAIL'] == 'gamma_t1@broadlearning.com'
		 || $_SESSION['SSV_LOGIN_EMAIL']=='gammademo@g1.broadlearning.com')
		{
			$this->GmailMode = true;
			$SYS_CONFIG['StudMail']['MimeLineBreak'] = "\r\n";
			$SYS_CONFIG['StudMail']['ServerIP'] = "imap.gmail.com"; // gmail
			$SYS_CONFIG['StudMail']['Port'] = "993";
			$SYS_CONFIG['StudMail']['SystemMailBox'] = array("inbox" => "INBOX", "SentFolder" => "[Gmail]/Sent Mail", "DraftFolder" => "[Gmail]/Drafts", "SpamFolder" => "[Gmail]/Spam", "TrashFolder" => "[Gmail]/Trash");
			$SYS_CONFIG['StudMail']['FolderDelimiter'] = "/";
			$SYS_CONFIG['StudMail']['FolderPrefix'] = "";
			$SYS_CONFIG['StudMail']['ServerType'] = "gmail"; //imap or exchange or gmail
			
			$this->FolderPrefix = $SYS_CONFIG['StudMail']['FolderPrefix'];
			$this->FolderDelimiter = $SYS_CONFIG['StudMail']['FolderDelimiter'];
			$this->RootPrefix = ($SYS_CONFIG['StudMail']['FolderPrefix'] == "")? "":$SYS_CONFIG['StudMail']['FolderPrefix'].$this->FolderDelimiter;
			$this->ServerHost = 'ssl://'.$SYS_CONFIG['StudMail']['ServerIP'];
		    $this->ServerPort = $SYS_CONFIG['StudMail']['Port'];
		    $this->ServerType = $SYS_CONFIG['StudMail']['ServerType'];
		    
		    $_SESSION['SSV_EMAIL_LOGIN'] = "gammademo@g1.broadlearning.com";
            $_SESSION['SSV_EMAIL_PASSWORD'] = "gamma123";
            $_SESSION['SSV_LOGIN_EMAIL'] = "gammademo@g1.broadlearning.com";
		    $this->CurUserName = substr($_SESSION['SSV_LOGIN_EMAIL'],0,strpos($_SESSION['SSV_LOGIN_EMAIL'],'@'));
		    $this->CurUserPassword = ($TargetPassword == "")? $_SESSION['SSV_EMAIL_PASSWORD']:$TargetPassword;
			$this->UserNameSuffix = $SYS_CONFIG['StudMail']['UserNameSubfix'];
		    if ($TargetUser == "") 
		    	$this->CurUserAddress = $_SESSION['SSV_LOGIN_EMAIL'];
		    else 
		    	$this->CurUserAddress = $TargetUser;
		}else
		{
			$this->FolderPrefix = $SYS_CONFIG['Mail']['FolderPrefix'];
			$this->FolderDelimiter = $SYS_CONFIG['Mail']['FolderDelimiter'];
			$this->RootPrefix = ($SYS_CONFIG['Mail']['FolderPrefix'] == "")? "":$SYS_CONFIG['Mail']['FolderPrefix'].$this->FolderDelimiter;
			$this->ServerHost = $SYS_CONFIG['Mail']['ServerIP'];
		    $this->ServerPort = $SYS_CONFIG['Mail']['Port'];
		    $this->ServerType = $SYS_CONFIG['Mail']['ServerType'];
		    $this->UserNameSuffix = $SYS_CONFIG['Mail']['UserNameSubfix'];
		    $this->CurUserName = $_SESSION['SSV_EMAIL_LOGIN'];
		    $this->CurUserPassword = ($TargetPassword == "")? $_SESSION['SSV_EMAIL_PASSWORD']:$TargetPassword;
		    if ($TargetUser == "") 
		    	$this->CurUserAddress = $_SESSION['SSV_LOGIN_EMAIL'];
		    else 
		    	$this->CurUserAddress = $TargetUser;
		}
	    if ($this->CurUserAddress == 'tony.chan@mail22.esf.edu.hk' || 
	    		$this->CurUserAddress == 'testing3@esfcentre.edu.hk' || 
	    		$this->CurUserAddress == 'testing2@esfcentre.edu.hk' || 
	    		$this->CurUserAddress == 'testing1@esfcentre.edu.hk' || 
	    		$this->CurUserAddress == 'tgcestest1@esfcentre.edu.hk' || 
	    		$this->CurUserAddress == 'tgcestest2@esfcentre.edu.hk' || 
	    		$this->CurUserAddress == 'tgcestest3@esfcentre.edu.hk') {
	   		$this->ServerHost = $SYS_CONFIG['ExchangeServerIP'];
	  	}
	  	
		
			//if ($SYS_CONFIG['Mail']['ServerType'] == "exchange" && $SYS_CONFIG['Mail']['ExchangeUsernameLogin'] && $this->CurUserAddress != "tgcestest1@esfcentre.edu.hk"  && $this->CurUserAddress != 'testing2@esfcentre.edu.hk' && $this->CurUserAddress != 'testing3@esfcentre.edu.hk' && $this->CurUserAddress != 'testing1@esfcentre.edu.hk')
		if($SYS_CONFIG['Mail']['UsernameLoginWithoutDomain'] === true)
		{
		  	if(stristr($this->CurUserAddress,"@")){
					$this->CurUserName = substr($this->CurUserAddress, 0, strpos($this->CurUserAddress, "@")); //get the$
		        	$this->CurUserAddress = substr($this->CurUserAddress, 0, strpos($this->CurUserAddress, "@"));
		  	}else{
		  			$this->CurUserName = $this->CurUserAddress;
		  	}
		}
		

    //$this->CurUserAddress = "roundcube@".$SYS_CONFIG['Mail']['UserNameSubfix'];
    //$this->CurUserPassword = "roundcubeeclass";
    $this->DebugMode = false; 
    $this->ShowTime = false;
    $this->Silent = false;
    $this->ShowLevel = 0;
    if($this->GmailMode==true || $this->CurUserAddress=='carlos_s2@testmail2.broadlearning.com'){
    	//$this->DebugMode = true;
    	//$this->ShowTime = true;
    }
   
	}
	
	function Connect() {
		if ($this->ServerType == 'gmail') {			    	
	    //$GmailAPI = new gmail_api_agent();
	    //$GmailAPI->Enable_IMap_Setting();
		}
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		
		$this->SocketStream = fsockopen($this->ServerHost,$this->ServerPort,$errno,$errstr,10);
		if (!$this->SocketStream) {
    	//echo "Could not connect to $this->ServerHost at port $this->ServerPort: $errstr";
			return false;
		}
		
		///echo "Connected.<br>";
		do {
        $line = trim($this->Server_Read_Line($this->SocketStream, 1024));
        //echo $line."<br>";
        if ($line === false) {
            break;
        }
    } while ($line === FALSE);
    
    if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Connect): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Connect): '.$this->Stop_Timer().'<br>';
    }
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
			
		if (!$this->Do_Command($this->SocketStream, 'alogin LOGIN "'.$this->Parse_Login($this->CurUserAddress).'" "'.$this->Parse_Login($this->CurUserPassword).'"')) {
			//echo 'Command Fail';
			return false; 
		}
			
    do {
        $line = trim($this->Server_Read_Line($this->SocketStream, 1024));
        //echo $line."<br>";
        $a = explode(' ', $line);
        if ($line === false) {
          break;
        }
    } while (stristr($line, "alogin") === FALSE && $a[1] != "BYE");
    if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Login): '.$this->Stop_Timer());
    	else
    		echo 'Time Different (Login): '.$this->Stop_Timer().'<br>';
    }
    
    $a = explode(' ', $line);
    if (strcmp($a[1], 'OK') == 0) {
      //echo "Logined Successfully.<br>";
      global $FolderSelectedPath;
      $FolderSelectedPath = '';
      return true;
    }
    else {
    	//echo "Logined Fail for user $this->CurUserAddress.<br>";
    	return false;
    }
	}
	
	function Go_To_Folder($FolderName) {
		global $FolderSelectedPath;
		if ($FolderSelectedPath == $FolderName) {
			return true;
		}
		else {
			if ($this->ShowTime && $this->ShowLevel ==  0) 
				$this->Start_Timer();
			$this->Do_Command($this->SocketStream, 'a002 SELECT "'.str_replace(array('\\','"'),array('\\\\','\"'),$FolderName).'"');
			
			if (!$this->SocketStream) {
	    	//echo "Could not connect to $this->ServerHost at port $this->ServerPort: $errstr";
				return false;
			}
			
			do {
	        $line = trim($this->Server_Read_Line($this->SocketStream, 1024));
	        //echo $line."<br>";
	        $a = explode(' ',$line);
	        if ($a[0] == "*") {
	        	if ($a[1] != "OK") {
	        		$Folder[$a[2]] = $a[1];
	        	}
	        	else {
	        		if (strcmp($a[2], '[UNSEEN') ==0) 
	        			$Folder['unseen'] = substr($a[3],0,strlen($a[3])-1);
	        	}
	        }
	    } while (stristr($line, "a002 ") === FALSE);
	   
	    if ($this->ShowTime && $this->ShowLevel ==  0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Time Different (Select Folder): '.$this->Stop_Timer());
	    	else
	    		echo 'Time Different (Select Folder): '.$this->Stop_Timer().'<br>';
	    }
	    	
	    $a = explode(' ', $line);
	    
	    if (strcmp($a[1], 'OK') == 0) {
	      //echo "Folder $FolderName Selected.<br>";
	      $FolderSelectedPath = $FolderName;
	      return $Folder;
	    }
	    else {
	    	//echo "Error on navigating to folder $FolderName.<br>";
	    	return false;
	    }
		}
	}
	
	function Close_Connect() {
		global $FolderSelectedPath;
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
			
		if ($this->SocketStream) {
			$this->Do_Command($this->SocketStream,"alogout LOGOUT");
			if ($this->ShowTime && $this->ShowLevel ==  0) { 
		    	if ($this->Silent)
		    		Write_Message_Log("Log",'Time Different (Close Connect): '.$this->Stop_Timer());
		    	else 
		    		echo 'Time Different (Close Connect): '.$this->Stop_Timer().'<br>';
	    	}
			fclose($this->SocketStream);
			$this->SocketStream = 0;
			$FolderSelectedPath = '';
			//echo "Connection Closed";
		}
	}
	
	function Do_Command($fp, $string, $endln=true) {
		global $SYS_CONFIG;
		
		if ($this->DebugMode) 
			echo $string.'<br>';
			
		return fputs($fp, $string . ($endln ? $SYS_CONFIG['Mail']['MimeLineBreak'] : ''));
	}
	
	function Server_Read_Line($fp, $size) {
		$line = '';
		if (!$fp) {
	    return $line;
		}
	    
		if (!$size) {
			$size = 1024;
		}
	    
		do {
  		$buffer = fgets($fp, $size);
  		if ($buffer === false) {
      	return false;
  		}
	//		console('S: '. chop($buffer));
	    $line .= $buffer;
		} while ($buffer[strlen($buffer)-1] != "\n");
		
		if ($this->DebugMode) 
			echo $line.'<br>';
		
		return $line;
	}
	
	function Server_Read_Reply($fp) {
	if (!$fp) {
    	//echo "Could not connect to $this->ServerHost at port $this->ServerPort: $errstr";
			return false;
		}	
		
	do {
		$line = trim($this->Server_Read_Line($fp, 1024));
	} while ($line[0] == '*');
	
	return $line;
}
	
	function Parse_Login($string) {
		return strtr($string, array('"'=>'\\"', '\\' => '\\\\')); 
	}
	
	function Search_Mail($folderName,$KeySubject="",$KeyBody="",$KeyFrom="",$KeyTo="",$KeyDateFrom="",$KeyDateTo="") {
			$this->Go_To_Folder($folderName);
			
			$SearchEncoding = array("big5","UTF-8","iso-8859-1");
			$DetectEncoding = array("big5","UTF-8","iso-8859-1");
	    for ($i=0; $i< sizeof($SearchEncoding); $i++) {	    
	    	$TempSubject = mb_convert_encoding($KeySubject,$SearchEncoding[$i],$DetectEncoding);
	    	$TempBody = mb_convert_encoding($KeyBody,$SearchEncoding[$i],$DetectEncoding);
	    	$TempFrom = mb_convert_encoding($KeyFrom,$SearchEncoding[$i],$DetectEncoding);
	    	$TempTo = mb_convert_encoding($KeyTo,$SearchEncoding[$i],$DetectEncoding);
	    	
	    	//$ThisQuery = 'OR (OR (OR (SUBJECT "'.$TempSubject.'") (TO "'.$TempTo.'")) (FROM "'.$TempFrom.'")) (BODY "'.$TempBody.'")';
	    	$ThisQuery = 'OR (OR (SUBJECT "'.$TempSubject.'") (TO "'.$TempTo.'")) (FROM "'.$TempFrom.'")';
	    	//$ThisQuery = 'TEXT "'.$TempSubject.'"';
	    	if ($i != 0) 
	    		$searchCriteria = 'OR ('.$searchCriteria.') ('.$ThisQuery.')';
	    	else 
	    		$searchCriteria = $ThisQuery;
	    }
	    
			$Query = 'a282 SEARCH '.$searchCriteria;
			
			$this->Do_Command($this->SocketStream, $Query);
			
			if(!$this->SocketStream)
			{
				return false;
			}
			
			do {
				$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
	
				$a = explode(' ', $line);
				if ($line[0] == '*') {
					while (list($k, $w) = each($a)) {
						if ($w != '*' && $w != 'SEARCH' )
	    					$MsgNoList[] = $w;
					}
				}
			} while ($a[0] != 'a282');
			
		/*echo '<pre>';
		var_dump($MsgNoList);
		echo '</pre>';	*/
		
		for ($i=0; $i< sizeof($MsgNoList) ; $i++) {
				$ReturnArray[] = array($folderName, $MsgNoList[$i]);
		}
		return $ReturnArray;
	}

	function Get_Message_Cache($MsgNo,$IsUID=false) {
		// function disabled after batch cache syn introduced
		/*$PrevDirective = "";
		$Done = true;
		$FieldArray = array('to:','from:','cc:','bcc:','date:','subject:','content-type:','x-priority:','x-msmail-priority:');
		
		//$Query = 'a003 SORT (REVERSE '.$SortBy.') UTF-8 ALL';
		//$Query = "a$MsgNo FETCH $MsgNo (BODY.PEEK[HEADER.FIELDS (DATE SUBJECT FROM TO CC BCC)] UID RFC822.SIZE) \r\n";
		if (!$IsUID)
			$Query = "afetch FETCH $MsgNo (BODY.PEEK[HEADER.FIELDS (DATE SUBJECT FROM TO CC BCC X-Priority X-MSMail-Priority content-type)] UID RFC822.SIZE FLAGS)";
		else 
			$Query = "afetch UID FETCH $MsgNo (BODY.PEEK[HEADER.FIELDS (DATE SUBJECT FROM TO CC BCC X-Priority X-MSMail-Priority content-type)] UID RFC822.SIZE FLAGS)";
			
		//echo $Query; die;
		$this->Do_Command($this->SocketStream, $Query);
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			$a = explode(' ', $line);
			//echo $line.'<br>';
			//echo $a[0].'<br>';
			if ($a[0] != "afetch") {
				if ($a[0] == '*') {
					$MessageNumber = $a[1];
	    		$MsgCache['MsgNo'] = $a[1];
				}
				else if ($a[0] == 'UID') {
					
					$MsgCache[$a[0]] = $a[1];
					$MsgCache[$a[2]] = $a[3];
					$MsgCache["Seen"] = "0";
					for ($i = 5; $i < sizeof($a); ++$i)
					{
						if (stripos($a[$i], "seen") !== false)
						{
							$MsgCache["Seen"] = "1";
						}
					}
				}
				else if ($a[0] != '' && $a[0] != 'a'.$MsgNo && !in_array(strtolower($a[0]),$FieldArray)) {
					//for ($i=0; $i< sizeof($a); $i++) {
						$MsgCache[$PrevDirective] .= $line;
					//}
				}
				else {
					$b = explode(': ', $line);
					if (!($b[0] == '' && $b[1] == '')) {
						if (in_array(strtolower($b[0]),array('subject','from','to','cc','bcc')) && sizeof($b) > 2) {
							for ($i=1; $i< sizeof($b); $i++) {
								if ($i == (sizeof($b)-1))
									$MsgCache[strtolower($b[0])] .= $b[$i];
								else
									$MsgCache[strtolower($b[0])] .= $b[$i].": ";
							}
						}
						else 
		    			$MsgCache[strtolower($b[0])] = $b[1];
		    			
	    			$PrevDirective = $b[0];
	    		}
				}
			}
			else {
				$Done = false;
			}
		} while ($Done);
    
    $ReturnVal['MsgNo'] = $MsgCache['MsgNo'];
    $ReturnVal['UID'] = $MsgCache['UID'];
    $ReturnVal["RFC822.SIZE"] = $MsgCache["RFC822.SIZE"];
    $ReturnVal['Seen'] = $MsgCache['Seen'];
    $ReturnVal['Date'] = strtotime(trim($MsgCache['date']));
      
    // Subject Decode
    $elements=imap_mime_header_decode(imap_utf8($MsgCache['subject']));
    unset($subtmp);
    for($k=0;$k<count($elements);$k++) {
    		$subtmp .= $elements[$k]->text;
    }
    $ReturnVal['Subject'] = $subtmp; # Subject to return
    
    $ContentType = explode(';',$MsgCache['content-type']);
    
    preg_match("/boundary=\".*?\"/i", $MsgCache['content-type'], $Boundary);
    $find = array("/boundary=\"/i", "/\"/i"); 
    $ReturnVal['Boundary'] = preg_replace($find, "", $Boundary[0]); 
    $ReturnVal['HaveAttachment'] = (preg_match("/multipart\/m/i", $ContentType[0]) == 0)? 'false':'true';
    
    $ReturnVal['ImportantFlag'] = ($MsgCache['x-priority'] == 1 || $MsgCache['x-msmail-priority'] == 'High')? 'true':'false';
    
    $elements = imap_rfc822_parse_adrlist(imap_utf8($MsgCache['to']),"");
    for ($i=0; $i< sizeof($elements); $i++) {
    	// for displau field
			$DisplayName = (trim($elements[$i]->personal) == "")? "": '"'.$elements[$i]->personal.'"';
			$Address = $DisplayName." <".$elements[$i]->mailbox."@".$elements[$i]->host.">";
			
			if ($i == 0) 
				$ReturnVal['To'] = $Address;
			else 
				$ReturnVal['To'] .= ', '.$Address;
			
			// for sorting field	
			$DisplayName = (trim($elements[$i]->personal) == "")? "": $elements[$i]->personal;
			if ($DisplayName == "") 
				$Address = $elements[$i]->mailbox."@".$elements[$i]->host;
			else
				$Address = $DisplayName." <".$elements[$i]->mailbox."@".$elements[$i]->host.">";
				
			if ($i == 0) 
				$ReturnVal['SortTo'] = $Address;
			else 
				$ReturnVal['SortTo'] .= ', '.$Address;
		}
    
    $elements = imap_rfc822_parse_adrlist(imap_utf8($MsgCache['from']),"");
    for ($i=0; $i< sizeof($elements); $i++) {
    	// for display field
			$DisplayName = (trim($elements[$i]->personal) == "")? "": '"'.$elements[$i]->personal.'"';
			$Address = $DisplayName." <".$elements[$i]->mailbox."@".$elements[$i]->host.">";
			
			if ($i == 0) 
				$ReturnVal['From'] = $Address;
			else 
				$ReturnVal['From'] .= ', '.$Address;
			
			// sorting field	
			$DisplayName = (trim($elements[$i]->personal) == "")? "": $elements[$i]->personal;
			if ($DisplayName == "") 
				$Address = $elements[$i]->mailbox."@".$elements[$i]->host;
			else
				$Address = $DisplayName." <".$elements[$i]->mailbox."@".$elements[$i]->host.">";
				
			if ($i == 0) 
				$ReturnVal['SortFrom'] = $Address;
			else 
				$ReturnVal['SortFrom'] .= ', '.$Address;
		}
    
    $elements=imap_mime_header_decode(imap_utf8($MsgCache['cc']));
    unset($subtmp);
    $CcArray = explode(",",trim($elements[0]->text));
    for($k=0;$k<count($CcArray);$k++) {
    	$subtmp[] = $this->Validate_Mail_Address($CcArray[$k]);
    	//$subtmp .= trim($elements[$k]->text);
    }
    $ReturnVal['Cc'] = (count($subtmp) > 1)? implode(",",$subtmp):$subtmp[0];
    
    $elements=imap_mime_header_decode(imap_utf8($MsgCache['bcc']));
    unset($subtmp);
    $BccArray = explode(",",trim($elements[0]->text));
    for($k=0;$k<count($BccArray);$k++) {
    	$subtmp[] = $this->Validate_Mail_Address($BccArray[$k]);
    	//$subtmp .= trim($elements[$k]->text);
    }
    $ReturnVal['Bcc'] = (count($subtmp) > 1)? implode(",",$subtmp):$subtmp[0];
		
		return $ReturnVal;*/
	}
	
	function Get_Message_Cache_By_Batch($FromMsgNo,$ToMsgNo, $isUid=0) {
		$PrevDirective = "";
		$Done = true;
		$FieldArray = array('to:','from:','cc:','bcc:','date:','subject:','content-type:','x-priority:','x-msmail-priority:','x-eclass-mailid:');
		$MessageIndex = null;
		
		//$Query = 'a003 SORT (REVERSE '.$SortBy.') UTF-8 ALL';
		//$Query = "a$MsgNo FETCH $MsgNo (BODY.PEEK[HEADER.FIELDS (DATE SUBJECT FROM TO CC BCC)] UID RFC822.SIZE) \r\n";
		//$Query = "afetch FETCH $FromMsgNo:$ToMsgNo (BODY.PEEK[HEADER.FIELDS (DATE SUBJECT FROM TO CC BCC X-Priority X-MSMail-Priority content-type)] UID RFC822.SIZE FLAGS)";
		$UID = $isUid==1?"UID":"";
		$Query = "afetch $UID FETCH $FromMsgNo:$ToMsgNo (BODY.PEEK[HEADER.FIELDS (DATE SUBJECT FROM TO CC BCC X-Priority X-MSMail-Priority X-eClass-MailID content-type)] UID RFC822.SIZE BODYSTRUCTURE)";
		
		//echo $Query; die;
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
			
		$this->Do_Command($this->SocketStream, $Query);
		if(!$this->SocketStream)
		{
			return false;
		}
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			$a = explode(' ', $line);
			$b = explode(':',$line);
			//echo $line.'<br>';
			//echo $a[0].'<br>';
			if($a[0]=='afetch') break;
			if ($a[0] != "afetch") {
				if ($a[0] == '*') {
					// special handling for the trailing ")" at the end of each message
					if (!is_null($MessageIndex)) 
						$MsgCache[$MessageIndex][$PrevDirective] = substr($MsgCache[$MessageIndex][$PrevDirective],0,-1);
					
					$MessageNumber = $a[1];
	    		$MsgCache[]['MsgNo'] = $a[1];
	    		$MessageIndex = sizeof($MsgCache)-1;
	    		//echo $MessageIndex; die;
	    		//echo 'OtherMsg<br>';
	    		
	    		if ($this->ServerType == 'gmail') {
	    			preg_match('/UID \d{1,1000}/',$line,$UID);
	    			$UID = explode(" ",$UID[0]);
	    			$MsgCache[$MessageIndex]['UID'] = $UID[1]; 
	    			
	    			preg_match('/RFC822.SIZE \d{1,1000}/',$line,$Size);
	    			$Size = explode(" ",$Size[0]);
	    			$MsgCache[$MessageIndex]['RFC822.SIZE'] = $Size[1];
	    			
	    			preg_match('/BODYSTRUCTURE \(.*\) /',$line,$BodyStruct);
	    			$find = array("/BODYSTRUCTURE /"); 
	    			$MsgCache[$MessageIndex]['BODYSTRUCTURE'] = preg_replace($find, "", $BodyStruct[0]);
	    		}
				}
				else if ($a[0] == 'UID') {
					// the UID
					$MsgCache[$MessageIndex][$a[0]] = $a[1];
					// the Size of mail
					$MsgCache[$MessageIndex][$a[2]] = $a[3];
					
					// the body structure
					for ($i=5; $i < sizeof($a); $i++) {
						$MsgCache[$MessageIndex][$a[4]] .= $a[$i]." ";
					}
					$PrevDirective = $a[4];
				}
				else if ($a[0] != '' && !in_array(strtolower($a[0]),$FieldArray) && !in_array(strtolower($b[0].':'),$FieldArray)) {
					//for ($i=0; $i< sizeof($a); $i++) {
						$MsgCache[$MessageIndex][$PrevDirective] .= $line;
					//}
					/*echo $MessageIndex.chr(13);
					echo $line.chr(13);
					echo $PrevDirective.'-Continue'.chr(13);*/
				}
				else {
					if (!($b[0] == '' && $b[1] == '')) {
						if (in_array(strtolower($b[0]),array('subject','from','to','cc','bcc','date')) && sizeof($b) > 2) {
							for ($i=1; $i< sizeof($b); $i++) {
								if ($i == (sizeof($b)-1))
									$MsgCache[$MessageIndex][strtolower($b[0])] .= ltrim($b[$i]);
								else
									$MsgCache[$MessageIndex][strtolower($b[0])] .= $b[$i].":";
							}
						}
						else 
		    			$MsgCache[$MessageIndex][strtolower($b[0])] = ltrim($b[1]);
		    			
	    			$PrevDirective = strtolower($b[0]);
	    		}
	    		//echo 'New Field<br>';
				}
			}
			else {
				// special handling for the trailing ")" at the end of each message
				if (!is_null($MessageIndex)) 
					$MsgCache[$MessageIndex][$PrevDirective] = substr($MsgCache[$MessageIndex][$PrevDirective],0,-1);
						
				$Done = false;
			}
		} while ($Done);
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get Batch Cache): '.$this->Stop_Timer());
    	else 
    		echo 'Time Different (Get Batch Cache): '.$this->Stop_Timer().'<br>';
   	}
		// trim the mini second for the date
		/*echo strtotime($MsgCache['Date']).'<Br>';
    $TempDate = explode(',',$MsgCache['Date']);    
    $DayOfWeek = $TempDate[0];
    $DateTime = explode(' ',trim($TempDate[1]));
    list($Day,$Month,$Year,$Time,$Zone) = $DateTime;
    list($Hour,$Min,$Sec) = explode(':',$Time);
    $SortTime = mktime($Hour,$Min,$Sec,Get_Month_Number($Month),$Day,$Year);*/
    
    for ($z=0; $z< sizeof($MsgCache); $z++) {
	    $ReturnVal[$z]['MsgNo'] = $MsgCache[$z]['MsgNo'];
	    $ReturnVal[$z]['UID'] = $MsgCache[$z]['UID'];
	    $ReturnVal[$z]["RFC822.SIZE"] = $MsgCache[$z]["RFC822.SIZE"];
	    $ReturnVal[$z]['Seen'] = $MsgCache[$z]['Seen'];

	    $TimeString = $MsgCache[$z]['date'];
	    do {
	    	$TempTime = strtotime(trim($TimeString));
	    	if ($TempTime === false) 
	    		$TimeString = substr($TimeString,0,strlen($TimeString)-1);
	    } while ($TempTime === false && $TimeString != "");
	    $ReturnVal[$z]['Date'] = $TempTime;
	    
	    // Subject Decode
	    $elements=imap_mime_header_decode(imap_utf8($MsgCache[$z]['subject']));
	    
	    unset($subtmp);
	    for($k=0;$k<count($elements);$k++) {
	    		$subtmp .= $elements[$k]->text;
	    }
	    $ReturnVal[$z]['Subject'] = $subtmp; # Subject to return
	    $ContentType = explode(';',$MsgCache[$z]['content-type']);
	    
	    preg_match("/boundary=\".*?\"/i", $MsgCache[$z]['content-type'], $Boundary);
	    $find = array("/boundary=\"/i", "/\"/i"); 
	    $ReturnVal[$z]['Boundary'] = preg_replace($find, "", $Boundary[0]); 
	    
	    $ReturnVal[$z]['MessageStructure'] = $MsgCache[$z]['BODYSTRUCTURE'];
	    $StructureArray = $this->Get_Message_Structure("","",$MsgCache[$z]['BODYSTRUCTURE'],false);
	    $ReturnVal[$z]['HaveAttachment'] = ($this->Check_Attach_Exists($StructureArray))? 'true':'false';
	    //$ReturnVal[$z]['HaveAttachment'] = (preg_match("/multipart\/m/i", $ContentType[0]) == 0)? 'false':'true';

	    $ReturnVal[$z]['ImportantFlag'] = ($MsgCache[$z]['x-priority'] == 1 || $MsgCache[$z]['x-msmail-priority'] == 'High')? 'true':'false';
	    
	    $ReturnVal[$z]['x-eclass-mailid'] = $MsgCache[$z]['x-eclass-mailid'];
	    
	    unset($Temp);
	    $elements = imap_rfc822_parse_adrlist(imap_utf8($MsgCache[$z]['to']),"");
	    $Temp = $this->Get_To_Cache_Address_List($elements);
	    $ReturnVal[$z]['To'] = $Temp['Actual'];
	    $ReturnVal[$z]['SortTo'] = $Temp['Sort'];
	    
	    unset($Temp);
	    $elements = imap_rfc822_parse_adrlist(imap_utf8($MsgCache[$z]['from']),"");
	    $Temp = $this->Get_To_Cache_Address_List($elements);
	    $ReturnVal[$z]['From'] = $Temp['Actual'];
	    $ReturnVal[$z]['SortFrom'] = $Temp['Sort'];

	    unset($Temp);
	    $elements = imap_rfc822_parse_adrlist(imap_utf8($MsgCache[$z]['cc']),"");
	    $Temp = $this->Get_To_Cache_Address_List($elements);
	    $ReturnVal[$z]['Cc'] = $Temp['Actual'];

	    unset($Temp);
	    $elements = imap_rfc822_parse_adrlist(imap_utf8($MsgCache[$z]['bcc']),"");
	    $Temp = $this->Get_To_Cache_Address_List($elements);
	    $ReturnVal[$z]['Bcc'] = $Temp['Actual'];
		}

		/*echo '<pre>';
		var_dump($ReturnVal);
		echo '</pre>';*/
		return $ReturnVal;
	}
	
	// get mail flags
	function Get_Message_Status($UID,$FolderName,$NeedGoTo=true, $isUID=true) {
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		if ($NeedGoTo) $Folder = $this->Go_To_Folder($FolderName);
		
		$PrevDirective = "";
		if($isUID)
			$FetchUID = "UID";
		$Query = "aStatus $FetchUID FETCH $UID (FLAGS)";
		//echo $Query; die;
		$this->Do_Command($this->SocketStream, $Query);
		
		if(!$this->SocketStream)
		{
			return false;
		}
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			$a = explode(' ', $line);
			//echo $line.'<br>';
			if ($a[0] == '*') {
				if ($this->ServerType == "exchange") {
					$Flags = preg_replace(array("/.*FLAGS \(/i","/\) UID.*\)$/i"),"",$line);
	    		$UID = preg_replace(array("/.*UID /i","/\)$/i"),"",$line);
	    		$MsgCache[$UID]['Flags'] = explode(' ',$Flags);
				}
				else if ($this->ServerType == "imap" || $this->ServerType == "gmail") {
	    		$Flags = substr($line,stripos($line,"FLAGS (")+7,stripos($line,"))") - (stripos($line,"FLAGS (") + 7));
	    		
	    		if($isUID)
	    		{
	    			$UID = substr($line,stripos($line,"(UID ")+5,stripos($line,"FLAGS (") - (stripos($line,"(UID ") + 6));
	    			$MsgCache[$UID]['Flags'] = explode(' ',$Flags);
	    		}
	    		else
	    		{
	    			$ID = trim(substr($line,stripos($line,"* ")+2,stripos($line,"FETCH (") - (stripos($line,"* ")+2)));
	    			$MsgCache[$ID]['Flags'] = explode(' ',$Flags);
	    		}
	    	}
			}
		} while ($a[0] != "aStatus");
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get Msg Flags): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Get Msg Flags): '.$this->Stop_Timer().'<br>';
    }
		/*echo '<pre>';
		var_dump($MsgCache);
		echo '</pre>';
		die;*/
		if (is_array($MsgCache)) {
			foreach ($MsgCache as $Key => $Val) {
				$ReturnArray[$Key]["recent"] = in_array('\Recent',$MsgCache[$Key]['Flags']);
				$ReturnArray[$Key]["flagged"] = in_array('\Flagged',$MsgCache[$Key]['Flags']);
				$ReturnArray[$Key]["answered"] = in_array('\Answered',$MsgCache[$Key]['Flags']);
				$ReturnArray[$Key]["deleted"] = in_array('\Deleted',$MsgCache[$Key]['Flags']);
				$ReturnArray[$Key]["seen"] = in_array('\Seen',$MsgCache[$Key]['Flags']);
				$ReturnArray[$Key]["draft"] = in_array('\Draft',$MsgCache[$Key]['Flags']);
				$ReturnArray[$Key]["forwarded"] = in_array('Forwarded',$MsgCache[$Key]['Flags']);
			}
		}
		
		return $ReturnArray;
	}
	
//	function Get_Multi_Mail_Status($UID,$FolderName,$NeedGoTo=true) {
//		if ($this->ShowTime && $this->ShowLevel ==  0) 
//			$this->Start_Timer();
//		if ($NeedGoTo) $Folder = $this->Go_To_Folder($FolderName);
//		
//		$PrevDirective = "";
//		$Query = "aStatus UID FETCH $UID (FLAGS)";
//		//echo $Query; die;
//		$this->Do_Command($this->SocketStream, $Query);
//		
//		if(!$this->SocketStream)
//		{
//			return false;
//		}
//		do {
//			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
//			$a = explode(' ', $line);
//			//echo $line.'<br>';
//			if ($a[0] == '*') {
////				if ($this->ServerType == "exchange") {
////					$Flags = preg_replace(array("/.*FLAGS \(/i","/\) UID.*\)$/i"),"",$line);
////	    		$UID = preg_replace(array("/.*UID /i","/\)$/i"),"",$line);
////	    		$MsgCache[$UID]['Flags'] = explode(' ',$Flags);
////				}
////				else if ($this->ServerType == "imap" || $this->ServerType == "gmail") 
//				if ($this->ServerType == "imap")
//				{
//		    		$Flags = substr($line,stripos($line,"FLAGS (")+7,stripos($line,"))") - (stripos($line,"FLAGS (") + 7));
//		    		$UID = substr($line,stripos($line,"(UID ")+5,stripos($line,"FLAGS (") - (stripos($line,"(UID ") + 6));
//		    		$MsgCache[$UID]['Flags'] = explode(' ',$Flags);
//	    		}
//			}
//		} while ($a[0] != "aStatus");
//		if ($this->ShowTime && $this->ShowLevel ==  0) { 
//    	if ($this->Silent)
//    		Write_Message_Log("Log",'Time Different (Get Msg Flags): '.$this->Stop_Timer());
//    	else  
//    		echo 'Time Different (Get Msg Flags): '.$this->Stop_Timer().'<br>';
//    }
		/*echo '<pre>';
//		var_dump($MsgCache);
//		echo '</pre>';
//		die;*/
//		if (is_array($MsgCache)) {
//			foreach ($MsgCache as $Key => $Val) {
//				$ReturnArray[$Key]["recent"] = in_array('\Recent',$MsgCache[$Key]['Flags']);
//				$ReturnArray[$Key]["flagged"] = in_array('\Flagged',$MsgCache[$Key]['Flags']);
//				$ReturnArray[$Key]["answered"] = in_array('\Answered',$MsgCache[$Key]['Flags']);
//				$ReturnArray[$Key]["deleted"] = in_array('\Deleted',$MsgCache[$Key]['Flags']);
//				$ReturnArray[$Key]["seen"] = in_array('\Seen',$MsgCache[$Key]['Flags']);
//				$ReturnArray[$Key]["draft"] = in_array('\Draft',$MsgCache[$Key]['Flags']);
//			}
//		}
//		
//		return $ReturnArray;
//	}
	
	function Get_UID($MessageNumber) {
		$Query = "afuid FETCH $MessageNumber (UID)";
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		if(!$this->SocketStream)
		{
			return false;
		}
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
			if ($a[0] == '*') {
				$UID = substr($a[4],0,strlen($a[4])-1);
			}
			else {
				continue;
			}
		} while ($a[0] != "afuid");
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get UID): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Get UID): '.$this->Stop_Timer().'<br>';
    }
    	
		if (trim($UID) == "") {
			return false;
		}
		else {
			return $UID;
		}
	}
	
	function Get_UID_ARRAY($MessageNumber) {
		$Query = "afuid FETCH $MessageNumber (UID)";
		$UID = array();
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
			if ($a[0] == '*') {
				$UID[] = substr($a[4],0,strlen($a[4])-1);
			}
			else {
				continue;
			}
		} while ($a[0] != "afuid");
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    		if ($this->Silent)
    			Write_Message_Log("Log",'Time Different (Get UID): '.$this->Stop_Timer());
    		else  
    			echo 'Time Different (Get UID): '.$this->Stop_Timer().'<br>';
    	}
    	
		if (count($UID) == 0) {
			return false;
		}
		else {
			return $UID;
		}
	}
	
	function Get_MsgNo($UID) {
		$Query = "afmsgno UID FETCH ".$UID." (UID)";
		//echo $Query.'<br>'; 
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		if(!$this->SocketStream)
		{
			return false;
		}
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
			if ($a[0] == '*') {
				$MessageNumber = $a[1];
			}
			else {
				continue;
			}
		} while ($a[0] != 'afmsgno');
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get MsgNo): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Get MsgNo): '.$this->Stop_Timer().'<br>';
    }
    	
		if (trim($MessageNumber) == "") {
			return false;
		}
		else {
			return $MessageNumber;
		}
	}
	
	function Get_Expired_UID($StartUID,$EndUID) {
		$Query = "afmsgno UID FETCH ".$StartUID.":".$EndUID." (UID)";
		// echo $Query.'<br>'; 
		
		$UID = array();
		// if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		if(!$this->SocketStream)
		{
			return false;
		}
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
			if ($a[0] == '*') {
				$UID[] = substr($a[4],0,strlen($a[4])-1);
			}
			else {
				continue;
			}
		} while ($a[0] != 'afmsgno');
		Write_Message_Log("Log",'Time Different (Get Expired UID): '.$this->Stop_Timer());
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
			if ($this->Silent)
				Write_Message_Log("Log",'Time Different (Get Expired UID): '.$this->Stop_Timer());
			else  
				echo 'Time Different (Get Expired UID): '.$this->Stop_Timer().'<br>';
		}
		
		for ($i=$StartUID; $i<= $EndUID; $i++) {
			if (!in_array($i,$UID)) 
				$ExpiredUID[] = $i;
		}
    
		if (sizeof($ExpiredUID) == 0) {
			return false;
		}
		else {
			return implode(",",$ExpiredUID);
		}
	}
	
	function Validate_Mail_Address($Address) {
		$BraceRemove = array("<",">","&lt;","&gt;");
		$TempStr = $Address;
  	
  	if (strpos($TempStr,'"') !== false) {
  		$DisplayName = substr($TempStr,strpos($TempStr,'"')+1,strrpos($TempStr,'"')-strpos($TempStr,'"')-1);
  		//echo strpos($TempStr,'"').' '.(strrpos($TempStr,'"')-strpos($TempStr,'"')).' '.$DisplayName.'<br>';
  		if (trim($DisplayName) == "") {
  			$Result = trim(str_replace($BraceRemove,"",substr($TempStr, strrpos($TempStr,'"')+1, strlen($TempStr)-strrpos($TempStr,'"')-1)));
  		}
  		else 
  			$Result = $TempStr;
  	}
  	else
  		$Result = str_replace($BraceRemove,"",$TempStr);
  	
  	//echo $Result.'<br>';	
		return $Result;
	}
	
	function Append_Msg_List($Message,$Folder,$Flags) {
		$Query = 'aAppend APPEND "'.$Folder.'" ('.$Flags.') {'.strlen($Message).'}';
		//echo $Query.'<br>';
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
		} while ($a[0] != '+' && $a[0] != 'aAppend');		
		
		//echo $a[1].'<br>';
		if ($a[1] != 'NO') {
			$this->Do_Command($this->SocketStream, $Message, false);
			$this->Do_Command($this->SocketStream, "");
			do {
				$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
				//echo $line.'<br>';
				$a = explode(' ', $line);
			} while ($a[0] != 'aAppend');
			
			if ($this->ShowTime && $this->ShowLevel ==  0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Time Different (Append Msgs): '.$this->Stop_Timer());
	    	else  
	    		echo 'Time Different (Append Msgs): '.$this->Stop_Timer().'<br>';
	    }
    	
			if ($a[1] == 'NO' || $a[1] == 'BAD') {
				return false;
			}
			else {
				return true;
				/*
				if ($this->ServerType == "gmail") 
					return true;
				else
					return substr($a[4],0,strlen($a[4])-1);
				*/
			}
		}
		else {
			if ($this->ShowTime && $this->ShowLevel ==  0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Time Different (Append Msgs): '.$this->Stop_Timer());
	    	else  
    			echo 'Time Different (Append Msgs): '.$this->Stop_Timer().'<br>';
    	}
    	
			return false;
		}
	}
	
	function Set_Msg_Flags($Flags,$UID,$Folder,$NeedGoTo=true) {
		
		$Query = 'sStore UID STORE '.$UID.' '.'+FLAGS.SILENT ('.$Flags.')';
		//echo $Query; die;
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		if ($NeedGoTo) $this->Go_To_Folder($Folder);
		
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
		} while ($a[0] != 'sStore');
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Save Flags): '.$this->Stop_Timer());
    	else  
	    	echo 'Time Different (Save Flags): '.$this->Stop_Timer().'<br>';
	  }
    
		if ($a[1] == "OK") return true;
		else return false;
	}
	
	function Remove_Msg_Flags($Flags,$UID,$Folder,$NeedGoTo=true) {
		$Query = 's'.$UID.' UID STORE '.$UID.' -FLAGS.SILENT ('.$Flags.')';
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		if ($NeedGoTo) $this->Go_To_Folder($Folder);
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
		} while ($a[0] != 's'.$UID);
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Remove Flags): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Remove Flags): '.$this->Stop_Timer().'<br>';
    }
    	
		if ($a[1] == "OK") return true;
		else return false;
	}
	
	function Move_Mail($FromFolder,$ToFolder,$UID,$NeedGoTo=true) {
		$Query = 'aMove UID COPY '.$UID.' "'.$ToFolder.'"';
		//echo $Query; 
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		if ($NeedGoTo) $this->Go_To_Folder($FromFolder);
		
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
		} while ($a[0] != 'aMove');
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Move Mail): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Move Mail): '.$this->Stop_Timer().'<br>';
    }
		//echo "11"; die;
		if ($a[1] == "OK") {
			$this->Delete_Server_Mail($FromFolder, $UID, false);
			if ($this->ServerType == 'gmail') {
				return true;
			}
			else {
				$NewUID = substr($a[5],0,strlen($a[5])-1);
				return $NewUID;
			}
		}
		else return false;
	}
	
	function Expunge_Folder($Folder,$NeedGoTo=true) {
		if ($NeedGoTo) $this->Go_To_Folder($Folder);
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$Query = 'dExpunge EXPUNGE';
		$this->Do_Command($this->SocketStream, $Query);
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
		} while ($a[0] != 'dExpunge');
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Expunge Folder): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Expunge Folder): '.$this->Stop_Timer().'<br>';
    }
    	
		if ($a[1] == "OK") return true;
		else return false;
	}
	
	function Delete_Server_Mail($Folder, $UID, $NeedGoTo=true) {
		if ($NeedGoTo) $this->Go_To_Folder($Folder);
		
		$Result = $this->Set_Msg_Flags('\Deleted',$UID,$Folder,false);
		$this->Expunge_Folder($Folder,false);
		
		return $Result;
	}
	
	function Get_Mail_Folders() {
		global $SYS_CONFIG;
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
			
		if (!isset($_SESSION['FolderList'])) {
	    $Query = 'aList LIST "" "*"';
	    
			$this->Do_Command($this->SocketStream, $Query);
			do {
				$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
				//echo $line.'<br>';
				$a = explode(' ', $line);
				
				if ($a[0] == "*") {
					$Temp["FolderAttribute"] = substr($line,strpos($line,"(")+1,strpos($line,")")-strpos($line,"(")-1);
					
					if ($this->ServerType == "exchange") {
						$TempPos = strpos($line,'"'.$this->FolderDelimiter.'"')+4;
						$TempFolderName = substr($line,$TempPos,strlen($line)-$TempPos);
						if (stristr(IMap_Decode_Folder_Name($TempFolderName)," ") !== false) {
							$TempFolderName = substr($TempFolderName,1,strlen($TempFolderName)-2);
						}
						$Temp["FolderFullPath"] = IMap_Decode_Folder_Name($TempFolderName);
					}
					else if ($this->ServerType == "imap" || $this->ServerType == "gmail") {
						$StartOfFolderName = strrpos(substr($line,0,strlen($line)-1),'"')+1;
						$EndOfFolderName = strrpos($line,'"');
						$Temp["FolderFullPath"] = IMap_Decode_Folder_Name(substr($line,$StartOfFolderName,$EndOfFolderName-$StartOfFolderName));
					}
					
					//$Temp["FolderFullPath"] = str_replace(" ","|-|",$Temp["FolderFullPath"]);
					//if (stristr($Temp["FolderAttribute"],"\Noselect") === false) { 
						$Temp["FolderFullPath"] = str_replace(" ","|-|",$Temp["FolderFullPath"]);
						$Folders[] = $Temp;
					/*}
					else {
						$NoSelectFolderList[] = $Temp["FolderFullPath"];
					}*/
				}
			} while ($a[0] != 'aList');
			
			if (sizeof($Folders) > 0)
				usort($Folders,"To_Lower_Case_Folder_Compare");
			
			// hide folders With (No select attribute) and in auto hide folder list 
			for ($i=0; $i< sizeof($Folders); $i++) {
				$Folders[$i]["FolderFullPath"] = str_replace("|-|"," ",$Folders[$i]["FolderFullPath"]);
				
				$AutoHideFolderList = $SYS_CONFIG['Mail']['AutoHideFolder'];
				$AutoHide = false;
				if ($this->ServerType == 'exchange') {
					for ($j=0; $j< sizeof($AutoHideFolderList); $j++) {
						if (strpos($Folders[$i]["FolderFullPath"],$AutoHideFolderList[$j]) === 0) {
							$AutoHide = true;
						}
					}
				}
				
				/*for ($j=0; $j< sizeof($NoSelectFolderList); $j++) {
					if (strpos($Folders[$i]["FolderFullPath"],$NoSelectFolderList[$j]) === 0) {
						$AutoHide = true;
					}
				}*/
					
				if (!$AutoHide) {
					$Result[] = $Folders[$i];
					$CurrentFolderList .= UTF8_To_DB_Handler($Folders[$i]["FolderFullPath"]).',';
				}
			}
			
			$CurrentFolderList = substr($CurrentFolderList,0,strlen($CurrentFolderList)-1);
			$db = new database(true);
			$sql = 'delete 
							from 
								Mail_Local_list 
							where 
								EmailAddress = \''.$this->CurUserAddress.'\'
								And 
								CAST(MailFolder AS varbinary(8000)) not in 
								('.$CurrentFolderList.')';
			$db->db_db_query($sql);
			
			$_SESSION['FolderList'] = $Result;
		}
		
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get Folder Structure): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Get Folder Structure): '.$this->Stop_Timer().'<br>';
    }
    
		return $_SESSION['FolderList'];
  }
  
  function Get_Mail_Content($Folder,$UID,$MessageBoundary) {
  	$Query = 'aFetch UID FETCH '.$UID.' (RFC822.TEXT)';
  	
  	$ReturnVal = array();
  	$i = 0;
  	$this->Go_To_Folder($Folder);
  	//echo '--'.$MessageBoundary.'<br>';
  	$this->Do_Command($this->SocketStream, $Query);
		do {
			//if ($i == 0) {
				$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
				//echo $line.'<br>';
				$a = explode(" ", $line);
				$ServerResponse .= $line;
			//}
						
			/*do {
				if ($i == 0) $i = 1;
				$parts = $this->Server_Read_Line($this->SocketStream, 1024);
				//echo $parts.'<br>';
				$ServerResponse = explode(" ", $parts);
				$content .= $parts;
			} while (trim($parts) != "--".$MessageBoundary && $ServerResponse[0] != 'aFetch');
			
			echo '<pre>';
			var_dump($content);
			echo '</pre>';
			unset($content);*/
		} while ($a[0] != 'aFetch');
		//echo $ServerResponse; die;
		
		$MessageContent = $this->Get_Message_Body_Parts($ServerResponse,$MessageBoundary);
		
		/*echo '<pre>';
		var_dump($MessageContent);
		echo '</pre>';*/
		
		if (array_key_exists("SubMessage",$MessageContent)) {
			if (strtolower($MessageContent["SubMessage"]["ContentType"]) == "alternative") {
				if (array_key_exists("html",$MessageContent["SubMessage"]["message"])) {
					$ReturnValue["IsHtml"] = true;
				}
				else {
					$ReturnValue["IsHtml"] = false;
				}
				$ReturnValue["Message"] = $MessageContent["SubMessage"]["message"];
			}
		}
		else {
			if (array_key_exists("html",$MessageContent["message"])) {
				$ReturnValue["IsHtml"] = true;
			}
			else {
				$ReturnValue["IsHtml"] = false;
			}
			$ReturnValue["Message"] = $MessageContent["message"];
		}
		
		if (array_key_exists("attachment",$MessageContent)) {
			$ReturnValue["HaveAttachment"] = true;
			$ReturnValue["attachment"] = $MessageContent["attachment"];
		}
		else {
			$ReturnValue["HaveAttachment"] = false;
		}
		
		/*echo '<pre>';
		var_dump($ReturnValue);
		echo '</pre>';*/
		
		return $ReturnValue;
  }
  
  function Analyst_Body_Parts($Content) {
		global $SYS_CONFIG; 
		
		$ContentTypeReg = "/Content-Type: .*\/\D*;/i";
		$TransferEncodeReg = "/Content-Transfer-Encoding: (quoted-printable|base64|7bit|8bit|binary)\n?/i";
		$ContentDispositionReg = "/Content-Disposition: .*\;/i";
		$BoundaryReg = "/boundary=\".*?\"/i";
		$CharsetReg = "/charset=\".*?\"/i";
		$SysResponseReg = "/aFetch.*\./";
		$FileNameReg = "/filename=\".*?\"/i";
		$FileName1Reg = "/name=\".*?\"/i";
		
		
		// get message content type
		//echo $Content.'<br>125852<Br>';
		preg_match($ContentTypeReg, $Content, $Match);
		/*echo '<pre>';
		var_dump($Match);
		echo '</pre>';*/
		$Match = explode(" ",$Match[0]);
		$FullContentType = substr($Match[1],0,strlen($Match[1])-1);
		//echo $FullContentType.'<br>';
		$Match = explode("/",$FullContentType);
		$SubContentType = $Match[1];
		$MainContentType = $Match[0];
		/*echo '<pre>';
		var_dump($Match);
		echo '</pre>';*/
		
		// get Transfer Encoding
    preg_match($TransferEncodeReg, $Content, $TransferEncoding);
    $TransferEncoding = explode(" ",$TransferEncoding[0]);
    $find = array("\r", "\n"); 
    $TransferEncoding = str_replace($find,"",$TransferEncoding[1]);
		//echo $TransferEncoding.'--END--<br>';
		// get disposition type
		preg_match($ContentDispositionReg, $Content, $ContentDisposition);
    $ContentDisposition = explode(" ",$ContentDisposition[0]);
    $ContentDisposition = substr($ContentDisposition[1],0,strlen($ContentDisposition[1])-1);
		//echo $Content.'--END--<br>';
		if (strtolower($ContentDisposition) != "attachment") {
			Switch (strtolower($MainContentType)) {
				case "multipart":
					//echo $MainContentType.'<br>';
					preg_match($BoundaryReg, $Content, $Boundary);
					$find = array("/boundary=\"/i", "/\"/i"); 
					$Boundary = preg_replace($find,"",$Boundary[0]);
					
					//echo $Content.'<br>';
					$find = array("/Content-Type: multipart\/.*\; .*?boundary=\".*?\"/i");
					$Content = preg_replace($find,"",$Content);
					//echo strlen($Content);
					
					//echo $Boundary.'<br>';
			    $ReturnVal['SubMessage'] = $this->Get_Message_Body_Parts($Content,$Boundary);
			    $ReturnVal['SubMessage']['ContentType'] = $SubContentType;
			    
			    return $ReturnVal;
					break;
				case "text":
					// get message charset
					preg_match($CharsetReg, $Content, $CharSet);
			    $find = array("/charset=\"/i", "/\"/i"); 
			    $CharSet = preg_replace($find, "", $CharSet[0]);
			    
			    // replace all content other than message content
			    $find = array($CharsetReg,$ContentTypeReg,$TransferEncodeReg,$SysResponseReg);
			    $Message = preg_replace($find,"",$Content);
			    
			    if ($TransferEncoding == "base64") 
			    	$Message = imap_base64($Message);
			    
			    $Message = Convert_Encoding($Message, "UTF-8", $CharSet);
			    
			    if ($SubContentType == 'plain') 
			    	$ReturnVal["PlainText"] = $Message;
			    
			    if ($SubContentType == 'html') 
			    	$ReturnVal["html"] = $Message;
					
					return array("message" => $ReturnVal);
			    break;
				case "message":
					break;
				default:
					//$find = "/\-\-".$MotherMessageBoundary."\-*/i";
					//$Message = preg_replace($find,"",$Content);
					
					return array();
					break;
			}		
		}
		else {
			$FileContentType = $MainContentType.'/'.$SubContentType;
			//echo $Content;
			preg_match($FileNameReg, $Content, $FileName);
	    $find = array("/filename=\"/i", "/\"/i"); 
	    $FileName = preg_replace($find, "", $FileName[0]); 
	    
	    if (trim($FileName) == "") {
	    	preg_match($FileName1Reg, $Content, $FileName);
		    $find = array("/name=\"/i", "/\"/i"); 
		    $FileName = preg_replace($find, "", $FileName[0]); 
	    }
	    $FileName = imap_utf8($FileName);
	    
	    $find = array($FileName1Reg,$ContentTypeReg,$TransferEncodeReg,$SysResponseReg,$ContentDispositionReg,$FileNameReg);
	    $FileContent = preg_replace($find,"",$Content);
	    //echo $FileContent;
	    if ($TransferEncoding == 'base64') 
	    	$FileContent = imap_base64($FileContent);
	    
	    $ReturnVal["FileName"] = $FileName;
	    $ReturnVal["ContentType"] = $FileContentType;
	    $ReturnVal["FileContent"] = $FileContent;
	    $ReturnVal["Size"] = strlen($FileContent)/1024;
	    
	    return array("attachment" => $ReturnVal);
		}
		
		/*
		echo '<pre>';
		echo $SubContentType.'<br>';
		echo $FullContentType.'<br>';
		echo '</pre>';
		*/
	}
	
	function Get_Message_Body_Parts($Content,$Boundary) {
    $body = explode('--'.$Boundary,$Content);
    //echo sizeof($body).'<br>';
    for($i=0; $i< sizeof($body); $i++) {
    	//echo $i.'<br>';
    	$Temp = $this->Analyst_Body_Parts($body[$i]);
			foreach ($Temp as $Key => $Val) {
				$ReturnVal[$Key][] = $Val;
			}
    }
    
    return $ReturnVal;
	}
	
	function Get_Unseen_In_Folder($Folder) {
		$Query = 'aStatus STATUS "'.$Folder.'" (UNSEEN)';
		//echo $Query;
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(" ", $line);
			
			if ($a[0] == "*") {
				preg_match("/\(UNSEEN .*\)/i",$line,$Matchs);
				$Matchs = explode(" ",$Matchs[0]);
				$Unseen = substr($Matchs[1],0,strlen($Matchs[1])-1);
			}
		} while ($a[0] != "aStatus");
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get unseen count): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Get unseen count): '.$this->Stop_Timer().'<br>';
    }
    	
		return $Unseen;
	}
	
	function Get_Check_Mail_Count_In_Folder($Folder) {
		//$Folder = IMap_Encode_Folder_Name($Folder);
		$Query = 'aStatus STATUS "'.$Folder.'" (MESSAGES)';
		//echo $Query;
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(" ", $line);
			
			if ($a[0] == "*") {
				preg_match("/\(MESSAGES .*\)/i",$line,$Matchs);
				$Matchs = explode(" ",$Matchs[0]);
				$MailCount = substr($Matchs[1],0,strlen($Matchs[1])-1);
			}
		} while ($a[0] != "aStatus");
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get total msg count): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Get total msg count): '.$this->Stop_Timer().'<br>';
   	}
    	
		return $MailCount;
	}
	
	// return quota counting in MB
	function Check_Quota() {
		if($this->GmailMode==true) return array('UsedQuota'=>0,'TotalQuota'=>0);
		if ($this->ServerType != "exchange") {
			$Query = 'aQuota GETQUOTA "ROOT"';
			
			if ($this->ShowTime && $this->ShowLevel ==  0) 
				$this->Start_Timer();
			$this->Do_Command($this->SocketStream, $Query);
			
			do {
				$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
				// echo $line.'<br>';
				$a = explode(" ", $line);
				
				if ($a[0] == "*") {
					preg_match("/\(STORAGE .*\)/i",$line,$Matchs);
					$Matchs = explode(" ",$Matchs[0]);
					//$ReturnVal['UsedQuota'] = round($Matchs[1]/1024);
					//$ReturnVal['TotalQuota'] = round(substr($Matchs[2],0,strlen($Matchs[2])-1)/1024);
					$ReturnVal['UsedQuota'] = sprintf("%.2f",$Matchs[1]/1024);
					$ReturnVal['TotalQuota'] = sprintf("%.2f",substr($Matchs[2],0,strlen($Matchs[2])-1)/1024);
				}
			} while ($a[0] != "aQuota");
			if ($this->ShowTime && $this->ShowLevel ==  0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Time Different (Get Quota): '.$this->Stop_Timer());
	    	else  
	    		echo 'Time Different (Get Quota): '.$this->Stop_Timer().'<br>';
	   	}
		}
		else {
			$ReturnVal['UsedQuota'] = 0;
			$ReturnVal['TotalQuota'] = 0;
		}
		
		return $ReturnVal;
	}
	
	function Clear_Folder($Folder,$NeedGoTo=true) {
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		if ($NeedGoTo)
			$this->Go_To_Folder($Folder);
			
		$MaxMsgNo = $this->Get_Check_Mail_Count_In_Folder($Folder);
		
		$Query = 'sStore STORE 1:'.$MaxMsgNo.' +FLAGS.SILENT (\Deleted)';
		//echo $Query; die;
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			//echo $line.'<br>';
			$a = explode(' ', $line);
		} while ($a[0] != 'sStore');
    		
		if ($a[1] == "OK") {
			$Result = $this->Expunge_Folder($Folder,false);
			if ($this->ShowTime && $this->ShowLevel ==  0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Time Different (Get Clear Folder): '.$this->Stop_Timer());
	    	else  
    			echo 'Time Different (Get Clear Folder): '.$this->Stop_Timer().'<br>';
    	}
			return $Result;
		}
		else {
			if ($this->ShowTime && $this->ShowLevel ==  0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Time Different (Get Clear Folder): '.$this->Stop_Timer());
	    	else  
    			echo 'Time Different (Get Clear Folder): '.$this->Stop_Timer().'<br>';
    	}
			return false;
		}
	}
	
	function Create_Folder($FolderName) {
		$Query = 'aCF CREATE "'.$FolderName.'"';
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			$a = explode(' ', $line);
		} while ($a[0] != 'aCF');
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Create Folder): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Create Folder): '.$this->Stop_Timer().'<br>';
    }
    		
		if ($a[1] == "OK") {
			$this->Subscribe_Folder($FolderName);
			return true;
		}
		else {
			return false;
		}	
	}
	
	function Subscribe_Folder($FolderName) {
		$Query = 'aS SUBSCRIBE "'.$FolderName.'"';
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			$a = explode(' ', $line);
		} while ($a[0] != 'aS');
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Subscribe Folder): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Subscribe Folder): '.$this->Stop_Timer().'<br>';
    }
    	
		if ($a[1] == "OK") {
			return true;
		}
		else {
			return false;
		}	
	}
	
	/* ------ function to get message's structure ------ */ 
	function Get_Message_Structure($UID="",$Folder="",$RawStructure="",$FurtherInvestigate=true) {
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
			
		if ($FurtherInvestigate)
			$this->Go_To_Folder($Folder);
		// if RawStructure parameter is defined, use it (E.G. in syn mail process) 
		// otherwise will try to get from cache record, if not exist then get in mail server, and put back to cache record
		if ($RawStructure == "") {
//			$db = new database(true);
//			$sql = "select 
//								MessageStructure 
//							From 
//								Mail_Local_List with (nolock) 
//							Where 
//								EmailAddress = '".$this->CurUserAddress."' 
//								And 
//								CAST(MailFolder AS varbinary(max)) = ".UTF8_To_DB_Handler(IMap_Decode_Folder_Name($Folder))." 
//								And
//								MessageUID = $UID";
//			$Result = $db->returnVector($sql);
//			$RawStructure = trim($Result[0]);
			
			if ($RawStructure == "") {
				$this->Go_To_Folder($Folder);
				
				$Query = 'aFetch UID FETCH '.$UID.' (BODYSTRUCTURE)';
				$this->Do_Command($this->SocketStream, $Query);
				
				do {
					$line = trim($this->Server_Read_Line($this->SocketStream, 2048));
					$RawStructure .= $line;
					$a = explode(' ', $line);
				} while ($a[0] != 'aFetch');
				///echo $RawStructure.'<br>';
				$RawStructure = trim(substr($RawStructure, strpos($RawStructure, 'BODYSTRUCTURE')+13, -(strlen($RawStructure)-strrpos($RawStructure, 'aFetch')+1)));		
				//echo $RawStructure.'<br>'; 
				// insert it back into cache record
//				$sql = 'Update Mail_Local_List Set
//									MessageStructure = \''.$db->Get_Safe_Sql_Query($RawStructure).'\'
//								Where 
//									EmailAddress = \''.$this->CurUserAddress.'\' 
//									And 
//									CAST(MailFolder AS varbinary(max)) = '.UTF8_To_DB_Handler(IMap_Decode_Folder_Name($Folder)).' 
//									And
//									MessageUID = '.$UID;
//				$db->db_db_query($sql);
			}
		}
		else {
			$RawStructure = trim($RawStructure);
		}
		//echo $RawStructure.'<br>';
		$RawStructure = substr($RawStructure, 1, strlen($RawStructure) - 2);
		//echo $RawStructure;
    $RawStructure = str_replace(")(", ") (", $RawStructure);
    
    $RawStructureArray = $this->Get_Structure_Array($RawStructure);
    
    $StructureArray = &$this->_structure_part($RawStructureArray,0,'',$UID,$FurtherInvestigate);
    //debug_r($StructureArray);		
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get Msg Structure): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Get Msg Structure): '.$this->Stop_Timer().'<br>';
    }
    //debug_r($StructureArray);	
    return $StructureArray;
	}
	/*
	function Get_Close_Parent_Pos($str, $start){
    $level=0;
    $len = strlen($str);
    $in_quote = 0;
    for ($i=$start;$i< $len;$i++){
    	if ($str[$i]=="\"") $in_quote = ($in_quote + 1) % 2;
    	if (!$in_quote){
        	if ($str[$i]=="(") $level++;
        	else if (($level > 0) && ($str[$i]==")")) $level--;
        	else if (($level == 0) && ($str[$i]==")")) return $i;
  		}
  	}
  }
  
  function Get_Structure_Array($str){	
    
    $id = 0;
    $a = array();
    $len = strlen($str);
    
    $in_quote = 0;
    for ($i=0; $i< $len; $i++){
        if ($str[$i] == "\"") $in_quote = ($in_quote + 1) % 2;
        else if (!$in_quote){
            if ($str[$i] == " "){ //space means new element
                $id++;
                while ($str[$i+1] == " ") $i++;  // skip additional spaces
            } else if ($str[$i]=="("){ //new part
                $i++;
                $endPos = $this->Get_Close_Parent_Pos($str, $i);
                $partLen = $endPos - $i;
                $part = substr($str, $i, $partLen);
                $a[$id] = $this->Get_Structure_Array($part); //send part string
                $i = $endPos;
            }else $a[$id].=$str[$i]; //add to current element in array
        }else if ($in_quote){
            if ($str[$i]=="\\") $i++; //escape backslashes
            else $a[$id].=$str[$i]; //add to current element in array
        }
    }
        
    reset($a);
    return $a;
	}
	*/
	function Get_Close_Parent_Pos($str, $start){
	    $level=0;
	    $len = strlen($str);
	    $in_quote = 0;
	    for ($i=$start;$i< $len;$i++){
	    	if ($str[$i]=="\"" && $str[$i-1]!="\\") $in_quote = ($in_quote + 1) % 2;
	    	if (!$in_quote){
	        	if ($str[$i]=="(") $level++;
	        	else if (($level > 0) && ($str[$i]==")")) $level--;
	        	else if (($level == 0) && ($str[$i]==")")) return $i;
	  		}
	  	}
	  	return $start; // cannot find the close bracket above, just return the start position to avoid code dead loop
  	}
	
	function Get_Structure_Array($str){	
    	
	    $id = 0;
	    $a = array();
	    $len = strlen($str);
	    
	    $in_quote = 0;
	    for ($i=0; $i< $len; $i++){
	        if ($str[$i] == "\"" && $str[$i-1] != "\\") $in_quote = ($in_quote + 1) % 2;
	        else if (!$in_quote){
	            if ($str[$i] == " "){ //space means new element
	                $id++;
	                while ($str[$i+1] == " ") $i++;  // skip additional spaces
	            } else if ($str[$i]=="("){ //new part
	                $i++;
	                $endPos = $this->Get_Close_Parent_Pos($str, $i);
	                $partLen = $endPos - $i;
	                $part = substr($str, $i, $partLen);
	                $a[$id] = $this->Get_Structure_Array($part); //send part string
	                $i = $endPos;
	            }else $a[$id].=$str[$i]; //add to current element in array
	        }else if ($in_quote){
	            if ($str[$i]=="\\") $i++; //escape backslashes
	            else $a[$id].=$str[$i]; //add to current element in array
	        }
	    }
	        
	    reset($a);
	    return $a;
	}
	
	function &_structure_part($part,$count=0,$parent='',$UID="",$FurtherInvestigate=true) {
    $struct = new imap_message_part();
    $struct->mime_id = empty($parent) ? (string)$count : "$parent.$count";
    
    // multipart
    if (is_array($part[0])) {
      $struct->ctype_primary = 'multipart';
      
      // find first non-array entry
      for ($i=1; $i<count($part); $i++)
        if (!is_array($part[$i]))
          {
          $struct->ctype_secondary = strtolower($part[$i]);
          break;
          }
          
      $struct->mimetype = 'multipart/'.$struct->ctype_secondary;

      $struct->parts = array();
      for ($i=0, $count=0; $i<count($part); $i++)
        if (is_array($part[$i]) && count($part[$i]) > 3)
          $struct->parts[] = $this->_structure_part($part[$i], ++$count, $struct->mime_id,$UID,$FurtherInvestigate);
          
      return $struct;
   	}
    
    
    // regular part
    $struct->ctype_primary = strtolower($part[0]);
    $struct->ctype_secondary = strtolower($part[1]);
    $struct->mimetype = $struct->ctype_primary.'/'.$struct->ctype_secondary;

    // read content type parameters
    if (is_array($part[2])) {
      $struct->ctype_parameters = array();
      for ($i=0; $i<count($part[2]); $i+=2)
        $struct->ctype_parameters[strtolower($part[2][$i])] = $part[2][$i+1];
        
      if (isset($struct->ctype_parameters['charset']))
        $struct->charset = $struct->ctype_parameters['charset'];
    }
    
    // read content encoding
    if (!empty($part[5]) && $part[5]!='NIL') {
      $struct->encoding = strtolower($part[5]);
      $struct->headers['content-transfer-encoding'] = $struct->encoding;
    }
    
    // get part size
    if (!empty($part[6]) && $part[6]!='NIL')
      $struct->size = intval($part[6]);

    // read part disposition
    $di = count($part) - 2;
    if ((is_array($part[$di]) && count($part[$di]) == 2 && is_array($part[$di][1])) ||
        (is_array($part[--$di]) && count($part[$di]) == 2)) {
      $struct->disposition = strtolower($part[$di][0]);

      if (is_array($part[$di][1]))
        for ($n=0; $n<count($part[$di][1]); $n+=2)
          $struct->d_parameters[strtolower($part[$di][1][$n])] = $part[$di][1][$n+1];
    }
    
    // add "attachment" disposition if content type = "application/ms-tnef", for outlook's winmail.dat issue
    if (strtolower($struct->ctype_primary) == "application" && strtolower($struct->ctype_secondary) == "ms-tnef") {
    	$struct->disposition = "attachment";
    }
      
    // get child parts
    if (is_array($part[8]) && $di != 8) {
      $struct->parts = array();
      for ($i=0, $count=0; $i<count($part[8]); $i++)
        if (is_array($part[8][$i]) && count($part[8][$i]) > 5)
          $struct->parts[] = $this->_structure_part($part[8][$i], ++$count, $struct->mime_id,$UID,$FurtherInvestigate);
    }

    // get part ID
    if (!empty($part[3]) && $part[3]!='NIL') {
      $struct->content_id = $part[3];
      $struct->headers['content-id'] = $part[3];
    
      if (empty($struct->disposition))
        $struct->disposition = 'inline';
    }

    if ($struct->ctype_primary=='message') {
    	// fetch message headers if message/rfc822
    	if ($struct->ctype_secondary=='rfc822') {
	    	if ($FurtherInvestigate) {
	      	$struct->headers = $this->Get_Message_Content_Type_Header($UID, $struct->mime_id, $struct->encoding);
	      	
	      	$struct->filename = $struct->headers['subject'].'.msg';
	      	if (is_array($part[8]) && empty($struct->parts))
	        	$struct->parts[] = $this->_structure_part($part[8], ++$count, $struct->mime_id,$UID,$FurtherInvestigate);
	    	}
	    	else 
	    		$struct->filename = 'no name.msg';
	      $struct->disposition = "attachment";
	      //$di = count($part) - 2;
	      //$struct->disposition = strtolower($part[$di][0]);
	    }
	    // for message/delivery-status type message
	    else if ($struct->ctype_secondary=='delivery-status') {
	    	$struct->ctype_primary=='text';
	    	$struct->ctype_secondary=='plain';
	    	$struct->mimetype = 'text/plain';
	    	$struct->filename = 'Details.txt';
	    	$struct->disposition = "attachment";
	    }
    }

    // normalize filename property
    if ($filename_mime = $struct->d_parameters['filename'] ? $struct->d_parameters['filename'] : $struct->ctype_parameters['name'])
    {
	    //$elements=imap_mime_header_decode(imap_utf8($filename_mime));
	    $elements=imap_mime_header_decode($filename_mime);
	    unset($subtmp);
	    for($k=0;$k<count($elements);$k++) {
	    	if(trim($elements[$k]->text)==""){
	    		continue;
	    	}
	    	if(strtolower($elements[$k]->charset)=="gb2312")
	    		$subtmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8","GB2312,GBK");
	    	else if(strtolower($elements[$k]->charset)=="big5-hkscs")
	    		$subtmp .= @iconv($elements[$k]->charset,"UTF-8",$elements[$k]->text);
	    	else if(strtolower($elements[$k]->charset)!="utf-8" && strtolower($elements[$k]->charset)!="default" && strtolower($elements[$k]->charset != "x-unknown"))
	    		$subtmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8",$elements[$k]->charset);
	    	else
	    		$subtmp .= $elements[$k]->text;
	    }
	    $FileName = $subtmp;
      $struct->filename = $FileName;
    }
    else if ($filename_encoded = $struct->d_parameters['filename*'] ? $struct->d_parameters['filename*'] : $struct->ctype_parameters['name*'])
    {
      // decode filename according to RFC 2231, Section 4
      list($filename_charset,, $filename_urlencoded) = explode('\'', $filename_encoded);
      $struct->filename = mb_convert_encoding(urldecode($filename_urlencoded),"UTF-8",$filename_charset.",UTF-8");
    }
    else if (trim($struct->d_parameters['filename*0']) != "") 
    {
      // decode filename according to RFC 2231, Section 4
      $k= 0;
      do {
      	$filename_encoded =  $struct->d_parameters['filename*'.$k];
      	list($filename_charset,, $filename_urlencoded) = explode('\'', $filename_encoded);
      	$Temp = mb_convert_encoding(urldecode($filename_urlencoded),"UTF-8",$filename_charset.",UTF-8");
      	$struct->filename .= (trim($Temp) != "")? $Temp:$filename_encoded;
      	$k++;
      } while (trim($struct->d_parameters['filename*'.$k]) != "");
    }
    else if (trim($struct->ctype_parameters['name*0']) != "") 
    {
      // decode filename according to RFC 2231, Section 4
      $k= 0;
      do {
      	$filename_encoded =  $struct->ctype_parameters['name*'.$k];
      	list($filename_charset,, $filename_urlencoded) = explode('\'', $filename_encoded);
      	$Temp = mb_convert_encoding(urldecode($filename_urlencoded),"UTF-8",$filename_charset.",UTF-8");
      	$struct->filename .= (trim($Temp) != "")? $Temp:$filename_encoded;
      	$k++;
      } while (trim($struct->ctype_parameters['name*'.$k]) != "");
    }
    else if (!empty($struct->headers['content-description'])) {
    	//$elements=imap_mime_header_decode(imap_utf8($struct->headers['content-description']));
    	$elements=imap_mime_header_decode($struct->headers['content-description']);
	    unset($subtmp);
	    for($k=0;$k<count($elements);$k++) {
	    	if(trim($elements[$k]->text)==""){
	    		continue;
	    	}
	    	if(strtolower($elements[$k]->charset) == "gb2312")
	    		$subtmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8","GB2312,GBK");
	    	else if(strtolower($elements[$k]->charset)=="big5-hkscs")
	    		$subtmp .= @iconv($elements[$k]->charset,"UTF-8",$elements[$k]->text);
	    	else if(strtolower($elements[$k]->charset)!= "utf-8" && strtolower($elements[$k]->charset)!= "default" && strtolower($elements[$k]->charset != "x-unknown"))
	    		$subtmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8",$elements[$k]->charset);
	    	else
	    		$subtmp .= $elements[$k]->text;
	    }
	    $FileName = $subtmp;
    	$struct->filename = $FileName;
    }
    else if(!empty($struct->filename))
    {
    	$elements=imap_mime_header_decode($struct->filename);
	    unset($subtmp);
	    for($k=0;$k<count($elements);$k++) {
	    	if(trim($elements[$k]->text)==""){
	    		continue;
	    	}
	    	if(strtolower($elements[$k]->charset) == "gb2312")
	    		$subtmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8","GB2312,GBK");
	    	else if(strtolower($elements[$k]->charset)=="big5-hkscs")
	    		$subtmp .= @iconv($elements[$k]->charset,"UTF-8",$elements[$k]->text);
	    	else if(strtolower($elements[$k]->charset)!= "utf-8" && strtolower($elements[$k]->charset)!= "default" && strtolower($elements[$k]->charset != "x-unknown"))
	    		$subtmp .= @mb_convert_encoding($elements[$k]->text,"UTF-8",$elements[$k]->charset);
	    	else
	    		$subtmp .= $elements[$k]->text;
	    }
	    $FileName = $subtmp;
    	$struct->filename = $FileName;
    }
        
    return $struct;
  }
	/* ------ End of Get Message Structure Functions ------ */
	
	function Get_Message_Part($PartID,$UID,$Folder) {
		$this->Go_To_Folder($Folder);
		
		$PartID = ($PartID == "0")? 1:$PartID;
		$Query = 'aFetch UID FETCH '.$UID.' (BODY['.$PartID.'])';
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		
		$Count = 1;
		do {
			$line = $this->Server_Read_Line($this->SocketStream, 1024);
			
			$a = explode(' ', $line);
			
			if ($Count != 1 && $a[0] != 'aFetch' && $a[0] != '*')
				$RawContent[] = $line;
		
			$Count++;
		} while ($a[0] != 'aFetch');
		//debug_pr($RawContent);		
		for ($i=0; $i< sizeof($RawContent); $i++) {
			// multi line body
			if (sizeof($RawContent) > 1) {			
				if ($i < (sizeof($RawContent)-1)) {
					//if (trim($RawContent[$i]) != "") {
						$RealText .= $RawContent[$i];
					//}
				}
				else {
					if ($this->ServerType == 'imap' || $this->ServerType == 'gmail') 
						$RealText .= substr(trim($RawContent[$i]),0,strlen(trim($RawContent[$i]))-1);
					else if ($this->ServerType == 'exchange') {
						$a = explode(' ', $RawContent[$i]);
						if ($a[1] != "UID") { 
							$RealText .= substr(trim($RawContent[$i]),0,strlen(trim($RawContent[$i]))-1);
						}
					}
				}
			}
			// single line body
			else {
				$RealText .= substr(trim($RawContent[$i]),0,strlen(trim($RawContent[$i]))-1);
			}
		}		
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get Msg Part): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Get Msg Part): '.$this->Stop_Timer().'<br>';
   	}
   	
   	if ($this->ServerType == 'gmail') {
			$this->Set_Msg_Flags('\Seen',$UID,$Folder);
   	}
   	
      	
   	return $RealText;
	}
	
	// Handler of message/rfc822 content type body part
	function Get_Message_Content_Type_Header($UID,$Part,$Encoding) {
		$Part = ($Part == "0")? "1.HEADER":$Part.".HEADER";
        
    // format request
		$Query = "aFetch UID FETCH $UID (BODY.PEEK[$Part])";
    		
    // send request
		$this->Do_Command($this->SocketStream, $Query);
        
		// receive reply line
		do {
    		$line = chop($this->Server_Read_Line($this->SocketStream, 1024));
    		$a    = explode(' ', $line);
		} while ($a[2] != 'FETCH');
		$len = strlen($line);
    
    if ($line[$len-1] == ')') {
    	// one line response, get everything between first and last quotes
			if (substr($line, -4, 3) == 'NIL') {
				// NIL response
				$result = '';
			} else {
		    $from = strpos($line, '"') + 1;
    		$to   = strrpos($line, '"');
		   	$len  = $to - $from;
				$result = substr($line, $from, $len);
			}
    } else if ($line[$len-1] == '}') {
      //multi-line request, find sizes of content and receive that many bytes
  		$from     = strpos($line, '{') + 1;
      $to       = strrpos($line, '}');
  		$len      = $to - $from;
      $sizeStr  = substr($line, $from, $len);
  		$bytes    = (int)$sizeStr;
      $received = 0;

  		while ($received < $bytes) {
  			$remaining = $bytes - $received;
        $line      = $this->Server_Read_Line($this->SocketStream, 1024);
  			$len       = strlen($line);
      
        if ($len > $remaining) {
			  	$line = substr($line, 0, $remaining);
        }
  			$received += strlen($line);
  			
		    $result .= rtrim($line, "\t\r\n\0\x0B") . "\n";
			}
    }
	        // read in anything up until 'til last line
		do {
    	$line = $this->Server_Read_Line($this->SocketStream, 1024);
    	$a = explode(" ",$line);
		} while (!($a[0] == "aFetch"));
        
    if ($result) {
	  	$result = rtrim($result, "\t\r\n\0\x0B");
    	
    	// parse header to array
    	$a_headers = array();
		  $lines = explode("\n", $result);
		  $c = count($lines);
		  for ($i=0; $i<$c; $i++)
	    {
	    	if ($p = strpos($lines[$i], ': '))
	      {
	      $field = strtolower(substr($lines[$i], 0, $p));
	      $value = trim(substr($lines[$i], $p+1));
	      if (!empty($value))
	        $a_headers[$field] = $value;
	      }
	    }
		  
		  $ContentType = explode("/",$a_headers['content-type']);
		  $a_headers['c_primary'] = $ContentType[0];
		  $a_headers['c_secondary'] = substr($ContentType[1],0,strlen($ContentType[1])-1);
		  
		  return $a_headers;
    }
    else 
    	return false;
	}
	
	function Check_Attach_Exists($StructureArray) {
		if ($StructureArray->parts) {
  		$StructureParts = $StructureArray->parts;
  	}
  	else {
  		if (trim($StructureArray->mime_id) == "0")
  			$StructureParts = array($StructureArray);
  		else 
  			$StructureParts = array();
  	}
  	
  	$AttachExists = false;
  	for ($i=0; $i< sizeof($StructureParts); $i++) {
  		if (strtolower($StructureParts[$i]->ctype_primary) == 'multipart' && $StructureArray->parts) {
  			if ($this->Check_Attach_Exists($StructureParts[$i])) 
  				return true;
  		}
  		if (strtolower($StructureParts[$i]->disposition) == 'attachment' || 
  				(strtolower($StructureParts[$i]->disposition) == 'inline' && strtolower($StructureParts[$i]->ctype_primary) != 'text') || 
  				trim($StructureParts[$i]->filename) != "") {
  			if (trim($StructureParts[$i]->filename) != "") {
  				if (!(strtolower($StructureParts[$i]->ctype_primary) == "application" && strtolower($StructureParts[$i]->ctype_secondary) == "ms-tnef") && 
  						!(strtolower($StructureParts[$i]->ctype_primary) == "application" && strtolower($StructureParts[$i]->ctype_secondary) == 'applefile')) {
	  				return true;
	  			}
  			}
  		}
  	}
  	
  	return false;
	}
	
	function Get_Message_Raw_Structure($UIDList) {
		$Query = "aFetch UID FETCH $UIDList (UID BODYSTRUCTURE)";
		
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		do {
			unset($Temp);
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			
			$a = explode(' ', $line);
			if ($a[0] == "*") {
				$Temp['UID'] = $a[4];
				// the body structure
				for ($i=6; $i < sizeof($a); $i++) {
					$Temp['Structure'] .= $a[$i]." ";
				}
				$Temp['Structure'] = trim($Temp['Structure']);
				$Result[] = $Temp;
			}
		} while ($a[0] != 'aFetch');
		
		if ($this->ShowTime && $this->ShowLevel ==  0) { 
    	if ($this->Silent)
    		Write_Message_Log("Log",'Time Different (Get Msg Raw Struct): '.$this->Stop_Timer());
    	else  
    		echo 'Time Different (Get Msg Raw Struct): '.$this->Stop_Timer().'<br>';
   	}
		return $Result;
	}
	
	function Get_To_Cache_Address_List($elements) {
		for ($i=0; $i< sizeof($elements); $i++) {
    	// Display field
    	$DisplayName = (trim($elements[$i]->personal) == "")? "": '"'.$elements[$i]->personal.'" ';
			$Subfix = (trim($elements[$i]->host) == "")? "": '@'.$elements[$i]->host;
			$MailBox = (trim($elements[$i]->mailbox) == "")? "": $elements[$i]->mailbox;
			if ($Subfix == "" && $DisplayName == "" && $MailBox == "") 
				$Address = "";
			else 
				$Address = $DisplayName." <".$elements[$i]->mailbox."@".$elements[$i]->host.">";
			
			if ($Address != "") {
				if (trim($ReturnVal['Actual']) == "") 
					$ReturnVal['Actual'] = $Address;
				else 
					$ReturnVal['Actual'] .= ', '.$Address;
			}
			
			// sorting field	
			$DisplayName = (trim($elements[$i]->personal) == "")? "": $elements[$i]->personal;
			if ($DisplayName == "") 
				$Address = $elements[$i]->mailbox."@".$elements[$i]->host;
			else
				$Address = $DisplayName." <".$elements[$i]->mailbox."@".$elements[$i]->host.">";
				
			if ($i == 0) 
				$ReturnVal['Sort'] = $Address;
			else 
				$ReturnVal['Sort'] .= ', '.$Address;
		}
		
		return $ReturnVal;
	}
	
	function Start_Timer() {
		global $timestart;
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		$timestart = $mtime;
		return true;
	}

	function Stop_Timer($precision=5) {
		global $timestart;
		$mtime = microtime();
		$mtime = explode(" ",$mtime);
		$mtime = $mtime[1] + $mtime[0];
		$timeend = $mtime;
		$timetotal = $timeend-$timestart;
		$scripttime = number_format($timetotal,$precision);
		return $scripttime;
	}
	
	function Do_Command_Debug($Command) {
		$this->Do_Command($this->SocketStream, 'a002 '.$Command);
		if (!$this->DebugMode) { 
    	echo $Command."<--Command<br>";
  	}
		do {
        $line = trim($this->Server_Read_Line($this->SocketStream, 1024));
        if (!$this->DebugMode) { 
        	echo $line."<br>";
      	}
        $a = explode(' ',$line);
    } while (stristr($line, "a002 ") === FALSE);
	}
	
	function Get_Unseen_Mail_Subject($Folder)
	{
		$this->Go_To_Folder($Folder);
		
		$Query = 'aSearch Search UNSEEN';
		//echo $Query;
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			echo $line.'<br>';
			$a = explode(" ", $line);
			
			if ($a[0] == "*") {
				
				$UIDStr = str_replace("* SEARCH ","",$line);
				$UID = explode(" ",$UIDStr);
			}
		} while ($a[0] != "aSearch");
		
		if(count($UID)==0) return false;
		
		$Query = 'aFetch Fetch '.implode(",",$UID).' (UID BODY.PEEK[HEADER.FIELDS (Subject)])';
		//echo $Query;
		if ($this->ShowTime && $this->ShowLevel ==  0) 
			$this->Start_Timer();
		$this->Do_Command($this->SocketStream, $Query);
		
		do {
			$line = trim($this->Server_Read_Line($this->SocketStream, 1024));
			echo $line.'<br>';
			$a = explode(" ", $line);
			
			if ($a[0] == "*") {
//				$UIDStr = str_replace("* SEARCH ","",$line);
//				$UID = explode(" ",$UIDStr);
//				preg_match("/\(UNSEEN .*\)/i",$line,$Matchs);
//				$Matchs = explode(" ",$Matchs[0]);
//				$Unseen = substr($Matchs[1],0,strlen($Matchs[1])-1);
			}
		} while ($a[0] != "aFetch");

		if ($this->ShowTime && $this->ShowLevel ==  0) { 
	    	if ($this->Silent)
	    		Write_Message_Log("Log",'Time Different (Get unseen count): '.$this->Stop_Timer());
	    	else  
	    		echo 'Time Different (Get unseen count): '.$this->Stop_Timer().'<br>';
	    }
    	
		//return $Unseen;
	}
	
	// ensure close connection when object is out of scope
	function __destructor()
	{
		$this->Close_Connect();
	}
	
	//function __destruct()
	//{
		//$this->Close_Connect();
	//}
}        // End of directive
?>