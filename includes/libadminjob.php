<?php
# using: 
/*
 *  2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

if (!defined("LIBADMINJOB_DEFINED"))         // Preprocessor directives
{

 define("LIBADMINJOB_DEFINED",true);

 # Type 0 - Announcement Approval
 # Type 1 - Circular Admin
 # Type 2 - Student Profile

 class libadminjob extends libdb{
       var $AdminUserID;
       var $tableFormat;
       var $titleClass;
       var $jobs;

       function libadminjob($uid="")
       {
                $this->libdb();
                $this->tableFormat = array(
                "width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0"
                );
                $this->titleClass = array(
                "tableTitle"
                );
                $this->jobs = "";
                if ($uid!="")
                {
                    $this->AdminUserID = $uid;
                    $this->retrieveAdminJobs();
                }
       }
       function retrieveAdminJobs($uid="")
       {
                if ($uid=="") $uid = $this->AdminUserID;
                $sql = "SELECT RecordType FROM INTRANET_ADMIN_USER WHERE UserID = '$uid' ORDER BY RecordType";
                $this->jobs = $this->returnVector($sql);
                return $this->jobs;
       }
       function isAdminForType($type,$uid="")
       {
                if ($uid=="") $uid = $this->AdminUserID;
                
                if ($this->AdminUserID != $uid)
                {
                    $this->jobs = "";
                }
                if ($this->jobs == "")
                {
                    $this->retrieveAdminJobs();
                }
                if (sizeof($this->jobs)!=0)
                {
                    return (in_array($type,$this->jobs));
                }
                else return false;
       }
       function isAnnouncementAdmin($uid="")
       {
                return $this->isAdminForType(0, $uid);
       }
       function isCircularAdmin($uid="")
       {
                return $this->isAdminForType(1, $uid);
       }
       function isProfileAdmin($uid="")
       {
                return $this->isAdminForType(2, $uid);
       }

       function returnAdminLevel($uid,$admintype)
       {
                $sql = "SELECT AdminLevel FROM INTRANET_ADMIN_USER WHERE UserID = '$uid' AND RecordType = '$admintype'";
                $temp = $this->returnVector($sql);
                return $temp[0];
       }
       /*
       Kenneth: 2006-09-25
       Deprecated:
       Modified to use $this->isAdminJobsAvailable()
       This function would be used for this object called internal only
       */
       function isAdmin($uid="")
       {
                if ($uid=="") $uid = $this->AdminUserID;
                if ($this->AdminUserID != $uid)
                {
                    $this->jobs = "";
                }
                if ($this->jobs == "")
                {
                    $this->retrieveAdminJobs($uid);
                }

                if (sizeof($this->jobs)!=0)
                {
                    return true;
                }
                else
                {
                    global $plugin;
                    if ($plugin['Discipline'] && sizeof($_SESSION['intranet_discipline_acl'])!=0)
                    {
                        return true;
                    }
                    elseif ($plugin['Sports'] && sizeof($_SESSION['intranet_sports_right'])!=0)
                    {
                        return true;
                    }
                    elseif ($plugin['ServiceMgmt'] && ($_SESSION['intranet_servicemgmt_adminlevel']==1 || $_SESSION['intranet_servicemgmt_adminlevel']==2) )
                    {
                        return true;
                    }
                    elseif ($plugin['QualiEd_StarPicking'] && ($_SESSION['intranet_qualied_starpicking_adminlevel']==1 || $_SESSION['intranet_qualied_starpicking_adminlevel']==2) )
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
       }
       function returnAdminList($type)
       {
                if ($type === "") return;
                $name_field = getNameFieldWithLoginByLang("b.");
                $sql = "SELECT a.UserID, $name_field, a.AdminLevel FROM INTRANET_ADMIN_USER as a
                        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        WHERE a.RecordType = '$type'
                        ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
                return $this->returnArray($sql,3);
       }
       function returnAnnouncementAdminUsers()
       {
                return $this->returnAdminList(0);
       }
       function returnCircularAdminUsers()
       {
                return $this->returnAdminList(1);
       }
       function returnProfileAdminUsers()
       {
                return $this->returnAdminList(2);
       }
       function returnAnnouncementAdminList()
       {
                $counts = array();
                $type_array = array(1,2,3);
                for ($i=0; $i<sizeof($type_array); $i++)
                {
                     $type = $type_array[$i];
                     $sql = "SELECT UserID,COUNT(RecordID)
                             FROM INTRANET_ANNOUNCEMENT_APPROVAL
                             WHERE RecordStatus = $type
                             GROUP BY UserID";
                     #echo "$type: $sql\n";
                     $result = $this->returnArray($sql,2);
                     for ($j=0; $j<sizeof($result); $j++)
                     {
                          list ($AdminUserID,$count) = $result[$j];
                          $counts[$i][$AdminUserID] = $count;
                     }

                }
                #print_r($counts);
                $adminList = $this->returnAnnouncementAdminUsers();
                for ($i=0; $i<sizeof($adminList); $i++)
                {
                     list($AdminUserID, $UserName) = $adminList[$i];
                     $returnResult[$i][0] = $AdminUserID;
                     $returnResult[$i][1] = $UserName;
                     for ($j=0; $j<sizeof($type_array); $j++)
                     {
                          $count = $counts[$j][$AdminUserID]+0;
                          $returnResult[$i][2+$j] = $count;
                     }
                }
                return $returnResult;
       }
       function displayAnnouncementAdminTable($format=0)
       {
                global $i_AdminJob_StaffName,$i_status_approved,$i_status_waiting,$i_status_rejected;
                global $i_no_record_exists_msg,$image_path;
                $x = "<table ".$this->tableFormat[$format]." >\n";
                $x .= "<tr><td class=".$this->titleClass[$format].">$i_AdminJob_StaffName</td><td class=".$this->titleClass[$format].">$i_status_approved</td><td class=".$this->titleClass[$format].">$i_status_waiting</td><td class=".$this->titleClass[$format].">$i_status_rejected</td></tr>\n";
                $staffList = $this->returnAnnouncementAdminList();
                for ($i=0; $i<sizeof($staffList); $i++)
                {
                     $css = ($i%2==0? "":"2");
                     list($AdminUserID,$UserName,$count_approved,$count_waiting,$count_rejected) = $staffList[$i];
                     $link = "<a class=functionlink href=\"javascript:removeAdminUser($AdminUserID)\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
                     $x .= "<tr class=tableContent$css><td>$UserName $link</td><td>$count_approved</td><td>$count_waiting</td><td>$count_rejected</td></tr>\n";
                }
                if (sizeof($staffList)==0)
                {
                    $x .= "<tr><td colspan=4 align=center>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";

                return $x;
       }

       function returnNumOfWaitingAnnouncement($uid="")
       {
                if ($uid == "") $uid = $this->AdminUserID;
                $sql = "SELECT COUNT(*) FROM INTRANET_ANNOUNCEMENT_APPROVAL WHERE UserID = '$uid' AND RecordStatus = 2";
                $temp = $this->returnVector($sql);
                return $temp[0];
       }

       function returnCircularAdminList()
       {
                return $this->returnCircularAdminUsers();
       }

       function displayCircularAdminTable($format=0)
       {
                global $i_AdminJob_StaffName;
                global $i_no_record_exists_msg,$image_path,$i_Circular_AdminLevel,$i_Circular_AdminLevel_Full,$i_Circular_AdminLevel_Normal;
                $x = "<table ".$this->tableFormat[$format]." >\n";
                $x .= "<tr><td class=".$this->titleClass[$format].">$i_AdminJob_StaffName</td><td class=".$this->titleClass[$format].">$i_Circular_AdminLevel</td></tr>\n";
                $staffList = $this->returnCircularAdminList();
                for ($i=0; $i<sizeof($staffList); $i++)
                {
                     $css = ($i%2==0? "":"2");
                     list($AdminUserID,$UserName,$admin_level) = $staffList[$i];
                     if ($admin_level == 1)
                     {
                         $str_Admin_level = $i_Circular_AdminLevel_Full;
                     }
                     else $str_Admin_level = $i_Circular_AdminLevel_Normal;
                     $link = "";
                     $link .= "<a class=functionlink href=\"javascript:editAdminUser($AdminUserID)\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
                     $link .= "<a class=functionlink href=\"javascript:removeAdminUser($AdminUserID)\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
                     $x .= "<tr class=tableContent$css><td>$UserName $link</td><td>$str_Admin_level</td></tr>\n";
                }
                if (sizeof($staffList)==0)
                {
                    $x .= "<tr><td colspan=2 align=center>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";

                return $x;

       }
       function displayNonAdminUserInput($type)
       {
                if ($type=="") return;
                $name_field = getNameFieldWithLoginByLang("a.");
                $sql = "SELECT a.UserID, $name_field FROM
                               INTRANET_USER as a LEFT OUTER JOIN INTRANET_ADMIN_USER as b ON a.UserID = b.UserID AND b.RecordType = '$type'
                        WHERE a.RecordStatus = 1 AND b.UserID IS NULL AND a.RecordType IN (1)";
                return $this->returnArray($sql,2);
       }

       function displayNonProfileAdminUserInput()
       {
                $users = $this->displayNonAdminUserInput(2);
                $x = "<table width=95% border=0 cellpadding=2 cellspacing=2>";
                for ($i=0; $i<sizeof($users); $i++)
                {
                     list ($id, $name) = $users[$i];
                     if ($i%2==0)       # first col
                     {
                         $x .= "<tr>";
                     }
                     $x .= "<td><input type=checkbox name=targetUserID[] value=$id> $name</td>";
                     if ($i%2==1)
                     {
                         $x .= "</tr>\n";
                     }
                }
                $x .= "</table>";
                return $x;
       }

       function returnProfileAdminList()
       {
                return $this->returnProfileAdminUsers();
       /*
                $name_field = getNameFieldWithLoginByLang("b.");
                $sql = "SELECT a.UserID, $name_field, a.AdminLevel,
                               c.AllowedAttendance, c.AllowedMerit, c.AllowedService,
                               c.AllowedActivity, c.AllowedAward, c.AllowedAssessment,
                               c.AllowedFile
                         FROM INTRANET_ADMIN_USER as a
                        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        LEFT OUTER JOIN INTRANET_PROFILE_ADMIN_ACL as c ON a.UserID = c.UserID
                        WHERE a.RecordType = '2'
                        ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
                return $this->returnArray($sql,10);
                */
       }
       function displayProfileAdminTable($format=0)
       {
                global $i_AdminJob_StaffName;
                global $i_AdminJob_AdminLevel,$i_AdminJob_AdminLevel_Normal,$i_AdminJob_AdminLevel_Full;
                #global $i_Profile_Attendance,$i_Profile_Merit,$i_Profile_Service,$i_Profile_Activity,$i_Profile_Award,$i_Profile_Assessment,$i_Profile_Files;
                global $i_no_record_exists_msg,$image_path;
                $x = "<table ".$this->tableFormat[$format]." >\n";
                $x .= "<tr><td class=".$this->titleClass[$format].">$i_AdminJob_StaffName</td>";
                $x .= "<td class=".$this->titleClass[$format].">$i_AdminJob_AdminLevel</td>";
                /*
                $x .= "<td class=".$this->titleClass[$format].">$i_Profile_Attendance</td>";
                $x .= "<td class=".$this->titleClass[$format].">$i_Profile_Merit</td>";
                $x .= "<td class=".$this->titleClass[$format].">$i_Profile_Service</td>";
                $x .= "<td class=".$this->titleClass[$format].">$i_Profile_Activity</td>";
                $x .= "<td class=".$this->titleClass[$format].">$i_Profile_Award</td>";
                $x .= "<td class=".$this->titleClass[$format].">$i_Profile_Assessment</td>";
                $x .= "<td class=".$this->titleClass[$format].">$i_Profile_Files</td>";
                */
                $x .= "</tr>\n";
                $staffList = $this->returnProfileAdminList();
                for ($i=0; $i<sizeof($staffList); $i++)
                {
                     $css = ($i%2==0? "":"2");
                     list($AdminUserID, $UserName, $adminLvl) = $staffList[$i];
                     $lvlWords = ($adminLvl==1?"$i_AdminJob_AdminLevel_Full":"$i_AdminJob_AdminLevel_Normal");
                     /*
                     list($AdminUserID,$UserName, $attendance_allowed, $merit_allowed,
                          $service_allowed,$activity_allowed, $award_allowed, $assessment_allowed,
                          $file_allowed) = $staffList[$i];
                          */
                     #$link = "<a class=functionlink href=\"javascript:removeAdminUser($AdminUserID)\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
                     $link = "";
                     $link .= "<a class=functionlink href=\"javascript:editAdminUser($AdminUserID)\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
                     $link .= "<a class=functionlink href=\"javascript:removeAdminUser($AdminUserID)\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
                     $x .= "<tr class=tableContent$css><td>$UserName $link</td>";
                     $x .= "<td>$lvlWords</td>";

                     /*
                     $x .= "<td>".($attendance_allowed?"$image_path/frontpage/campusmail/icon_notification.gif":"&nbsp;")."</td>";
                     $x .= "<td>".($merit_allowed?"$image_path/frontpage/campusmail/icon_notification.gif":"&nbsp;")."</td>";
                     $x .= "<td>".($service_allowed?"$image_path/frontpage/campusmail/icon_notification.gif":"&nbsp;")."</td>";
                     $x .= "<td>".($activity_allowed?"$image_path/frontpage/campusmail/icon_notification.gif":"&nbsp;")."</td>";
                     $x .= "<td>".($award_allowed?"$image_path/frontpage/campusmail/icon_notification.gif":"&nbsp;")."</td>";
                     $x .= "<td>".($assessment_allowed?"$image_path/frontpage/campusmail/icon_notification.gif":"&nbsp;")."</td>";
                     $x .= "<td>".($file_allowed?"$image_path/frontpage/campusmail/icon_notification.gif":"&nbsp;")."</td>";
                     */
                     $x .= "</tr>\n";
                }
                if (sizeof($staffList)==0)
                {
                    $x .= "<tr><td colspan=2 align=center>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";

                return $x;
       }
       function returnProfileRights($aid)
       {
                $sql = "SELECT AllowedAttendance, AllowedMerit, AllowedService, AllowedActivity,
                               AllowedAward, AllowedAssessment, AllowedFile
                               FROM INTRANET_PROFILE_ADMIN_ACL where UserID = '$aid'";
                $temp = $this->returnArray($sql,7);
                return $temp[0];
       }
       function returnProfileAllowedClass($aid)
       {
                $sql = "SELECT ClassID FROM INTRANET_PROFILE_ADMIN_CLASS WHERE UserID = '$aid'";
                return $this->returnVector($sql);
       }

       ### for smart card ###
       # check access right
       function isSmartAttendenceAdmin($uid="")
       {
           		global $intranet_root;
           		include_once("$intranet_root/includes/libuser.php");
   				
                $uid = ($uid=="")?$this->AdminUserID:$uid;

                $sql = "SELECT COUNT(*) FROM CARD_STUDENT_HELPER_STUDENT WHERE StudentID = '$uid'";
                $result = $this->returnVector($sql);
                $isCardStudentHelper = ($result[0]=="1")?true:false;
				
                $lu = new libuser($uid);
                if($lu->isTeacherStaff() || $isCardStudentHelper){
                	return true;
                }

                return false;
       }
       function isAdminJobsAvailable($uid=""){
               global $special_feature, $plugin, $intranet_root, $_SESSION;

               if($uid =="")
                        $uid = $this->AdminUserID;

               if ($_SESSION['intranet_reportcard_admin']==1)
					return true;
                        
               # is the user Admin
               if(!$this->isAdmin())
                               return false;

               # Announcement
               if($this->isAnnouncementAdmin())
                               return true;

               # e-Circular
               include_once ("$intranet_root/includes/libcircular.php");
               $lcircular = new libcircular();
               if (isset($special_feature['circular']) && $special_feature['circular'] && $this->isCircularAdmin() && !$lcircular->disabled)
                               return true;

               # Discipline
                   if ($plugin['Discipline'])
                                   return true;

                   # Sports
                   if ($plugin['Sports'] && $_SESSION['intranet_sports_right']==1)
                                   return true;

                   # Student Service Time Management
               if ($plugin['ServiceMgmt'] && ($_SESSION['intranet_servicemgmt_adminlevel']==1 || $_SESSION['intranet_servicemgmt_adminlevel']==2 ))
                               return true;

               # QualiEd_StarPicking
               if ($plugin['QualiEd_StarPicking'] && ($_SESSION['intranet_qualied_starpicking_adminlevel']==1 || $_SESSION['intranet_qualied_starpicking_adminlevel']==2 ))
                               return true;

               return false;
           }

           
			#######################################################################
			# IP25
			#######################################################################
			function RemoveAdminUser($IDstr="")
	        {
		        if($IDstr)
		        {
					$sql = "DELETE FROM INTRANET_ADMIN_USER WHERE AdminID in ($IDstr)";
					$this->db_db_query($sql);
		        }
	        }  
	        
	        function InsertAdminUser($RecordType, $AdminLevel=0, $UserAry=array())
	        {
		        if(isset($UserAry) && $RecordType)
		        {
		        	for ($i=0; $i<sizeof($UserAry); $i++)
					{
						$targetType = substr($UserAry[$i],0,1);
						$target = substr($UserAry[$i],1);
						
						if($targetType=="U")
						{
							$values = "('$target', '$RecordType', '$AdminLevel')";	
							$sql = "INSERT IGNORE INTO INTRANET_ADMIN_USER (UserID, RecordType, AdminLevel) VALUES $values";
							$this->db_db_query($sql);
						}
						else
						{
							include_once("libgroup.php");
							$lg = new libgroup($target);
							$GroupMember = $lg->returnGroupUser();
							foreach($GroupMember as $k=>$d)
							{
								$values = "('". $d['UserID']." ', '$RecordType', '$AdminLevel')";	
								$sql = "INSERT IGNORE INTO INTRANET_ADMIN_USER (UserID, RecordType, AdminLevel) VALUES $values";
								$this->db_db_query($sql);
							}
							
						}
					}
		        }
	        }  
	        
	        function RetrieveAdminUser($AdminID=0)
	        {
		        if($AdminID)
		        {
			        $sql = "select UserID, AdminLevel, RecordType from INTRANET_ADMIN_USER where AdminID='$AdminID'";
			        $result = $this->returnArray($sql);
			        return $result[0];
		        }
		        else
		        	return array();
	        }  
	        
	        function UpdateAdminUser($AdminID=0, $AdminLevel)
	        {
		        if($AdminID)
		        {
			        $sql = "update INTRANET_ADMIN_USER set AdminLevel='$AdminLevel' where AdminID='$AdminID'";
			        $this->db_db_query($sql);
		        }
	        }  
	        
	        
	        
	        
 }


} // End of directives
?>