<?php
## Using By : 

##########################################################
## Modification Log
##
##  2020-10-29 Tommy
##  modified loadCalendar(), set class instead of style, for print and break page
##
##  2020-07-27 Cameron
##  modify getImportHolidaysEventsUI() to
##      - fix js error by passing LocationID to checkSiteCode() [case #L137933]
##      - add z-index to popup layer to avoid overlap
##
##  2019-02-04 Vito
##  - modified getImportHolidaysEventsUI(), RemarksArr
##
##  2019-01-09  Anna
##  - modified newHolidayEventForm, editHolidayEventForm,getImportHolidaysEventsConfirmUI,getImportHolidaysEventsUI- added DescriptionEng, EventNatureEng for general
##
## 2018-09-03 Anna
##  - modified getImportHolidaysEventsConfirmUI() - show error records when $errorCount>0
##
## 2018-08-08: Henry
##  - fixed sql injection in loadTermTable()
##
## 2018-04-09: Anna
##  - modified editHolidayEventForm(),newHolidayEventForm() , added sfoc cust
##
## 2017-09-04: Carlos
##	- modified getImportHolidaysEventsUI(), fixed the display of column Class Group Code for KIS cust.
## 2017-06-14: Anna
##	-  modified Get_Timezone_Special_Timetable_Settings_UI(), add remark after specical timetable
##
## 2016-08-04: Ivan [K99877] [ip.2.5.7.9.1]
##	-  modified getImportHolidaysEventsConfirmUI(), isGroupIDValid() to check GroupID from selected academic year instead of current academic year when importing events
##
## 2016-03-11: Kenneth
##	-  modified  getImportHolidaysEventsUI(), show reference thickbox for group ID
##
## 2016-01-04: Omas [ip.2.5.7.1.1]
## 	-  modified loadAcademicYearTable(),showTermAddEditLayer(),loadAcademicYearTable() - for edit current, future term
##
## 2015-10-16: Kenneth
##	-  modified getImportHolidaysEventsUI(), Style Alignment in Table
##
## 2015-09-02: Bill [2015-0831-1444-14222]
##	-  modified loadPreviewCalendar(), loadCalendar(), to display correct number of month in academic year
##
## 2015-08-13: Bill	[2015-0813-1031-48207] - commented
##	-  modified loadAcademicYearTable(), added button for school adding new school year
##	   		prevent school from trying to add new school year using buttons which are used for adding new year term 
##
## Date:	2015-06-03 (Ivan) [V79280] [ip.2.5.6.8.1.0]
##	- modified newHolidayEventForm(), editHolidayEventForm() to add academic year of the selected date logic
##
## 2014-09-11: Bill
##	- modified getImportHolidaysEventsConfirmUI() to stop update and show error message when import Group Event with empty Group ID
##
## 2014-09-02: Bill	
##	- modified getImportHolidaysEventsConfirmUI() to update error message displayed when import GroupID with other holiday/evnet type
##
## 2013-10-04: Carlos 
##	- [KIS] modified getProductionCycleDaysCalendarView(), loadCalendar(), newHolidayEventForm(), editHolidayEventForm(),
##			 calendarDivision(), getImportHolidaysEventsUI(), getImportHolidaysEventsConfirmUI(), add parameter $ClassGroupID and classGroupSelection
##
## 2013-07-23: Ivan [2013-0723-1346-16132]
##	- updated insertDataToTmpSchoolHolidayEventTable() to support date format "D/M/YYYY" and "DD/MM/YYYY"
##
## 2012-02-26: Rita
## - modified generatePrintingVersionLabels() - add $spanWithCss
##
## 2012-09-14: YatWoon
## - add event type checking, only GE can import with GroupID
##
## 2011-10-26: YatWoon
## - updated loadCalendar(), loadPreviewCalendar(), not use "round" to count the month display, should be use "ceil"
##
## 2011-09-20: YatWoon
## - updated editHolidayEventForm(), newHolidayEventForm(), hidden "isSkipSAT" and "isSkipSUN"() if necessary
##
## 2011-08-04: Ivan
## - added Special Timetable related functions
##
## 2011-08-03: YatWoon
## - update loadAcademicYearTable(), change the important note display
##
## 2011-07-06: YatWoon
## - update newHolidayEventForm(), editHolidayEventForm(), add "isSkipSAT" and "isSkipSUN"(), add "Skip SAT" and "Skip SUN" options
## 
## 2011-06-28: Ivan
## - Added Get_School_Settings_Top_Tab()
##
## 2011-04-04: Kenneth Chung
## - modified newDefaultPeriodForm, fixed the problem on selecting timetable for IP25 defect 2120
##
## 2011-02-18: Kenneth Chung
## - modified isGroupIDValid(), allow academic year ID is NULL to be valid also, for identity group
##
## 2010-09-30: Ronald
## - modified loadCalendar(), add a link to remove all events.
##
## 2010-06-21: Max (201006071657)
## - modified function generateCalPrintingOptionsBox not to display the row print months without event
##
## 2009-12-17: Max (200912141001)
## - Add functions [calendarDivision(), printingVersionCSS(),generateCalPrintingOptionsBox()]
##
## 2009-12-14: Max (200912141001)
## - Link to a page with printing version [getProductionCycleDaysCalendarView()]
## - Add printing Options [getProductionCycleDaysCalendarView()]
##########################################################
include_once('libcycleperiods.php');
include_once('libuser.php');

class libcycleperiods_ui extends libcycleperiods {
	
	var $thickBoxWidth;
	var $thickBoxHeight;
	var $toolCellWidth;
	var $Code_Maxlength;
	
	function libcycleperiods_ui()
	{
		$this->libcycleperiods();
		
		$this->thickBoxHeight = '400px';
		$this->thickBoxWidth = '400px';
		$this->toolCellWidth = '';
		$this->Code_Maxlength = '';
	}
	
	function initJavaScript()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x = '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/script.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
				<!--<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>-->
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.colorPicker.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.scrollTo-min.js"></script>
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.colorPicker.css" type="text/css" />
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" />
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.datepick.css" type="text/css" />';
				
		return $x;
	}
	
	function getDefinePeriodTable()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libinterface.php");
		$linterface = new interface_html();
		
		$arrPeriods = $this->returnPeriods_NEW();
		// jquery include
		$x = $this->initJavaScript();
		$x .= "<table width='99%' border='0' cellpadding='3' cellspacing='3'>";
		//$x .= "<tr><td align='left'><a href='index.php'>".$Lang['SysMgr']['CycleDay']['BackToProductionView']."</a></td></tr>";
		$x .= "<tr><td><div class='navigation'>
				<img src='".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/nav_arrow.gif' width='15' height='15' align='absmiddle' />
				".$Lang['SysMgr']['CycleDay']['EditPeriod']."
				<img src='".$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/nav_arrow.gif' width='15' height='15' align='absmiddle' />
				".$Lang['SysMgr']['CycleDay']['DefinePeriod']."
				<br />
				<br />
				</div></td></tr>";
		$x .= "</table>";
		$x .= "<table width='99%' border='0' cellpadding='0' cellspacing='0'>
					<tr>
						<td class='main_content'>
							<div class='content_top_tool'>
							<div class='Conntent_tool'>
							<!--<a href='ajax_period_add.php?height=400&width=500' class='thickbox add_dim'>".$Lang['SysMgr']['CycleDay']['NewPeriod']."</a>-->
							<a href='#TB_inline?height=400&width=500&inlineId=FakeLayer' class='thickbox add_dim' title='".$Lang['SysMgr']['CycleDay']['NewPeriod']."' onclick='newPeriodForm(); return false;'>".$Lang['SysMgr']['CycleDay']['NewPeriod']."</a>
							</div>
							</div>
							<div id='PeriodInfoLayer'>";
		$x .= $this->getPeriodInfoTable();
		$x .= "				</div>
						</td>
					</tr>
				</table>";
				
		$x .= "<table width='99%' border='0' cellpadding='0' cellspacing='0'>";
		$x .= "<tr><td>
					<input id='preview' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['SysMgr']['CycleDay']['Preview']."' onClick='gotoPage(\"generate_preview.php\");'>
					<input id='publish' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['SysMgr']['CycleDay']['Publish']."' onClick='gotoPage(\"makeproduction.php\");'>
					<input id='cancel' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Cancel']."' onClick='gotoPage(\"index.php\");'>
				</td></tr>";
		$x .= "</table>";
		
		$x .= '<div class="FakeLayer"></div>';
		return $x;
	}
	
	function getPeriodInfoTable()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path;
		
		$arrPeriods = $this->returnPeriods_NEW();
		$x .= "<br>";
		$x .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>
					<tr>
						<td valign='bottom'><div class='table_filter'></div></td>
						<td valign='bottom'><div class='common_table_tool'> 
							<div id='edit_button' style='display:none'><a href='#TB_inline?height=300&width=500&inlineId=FakeLayer' class='thickbox tool_edit' title='".$Lang['SysMgr']['CycleDay']['EditPeriod']."' onClick='return editPeriodForm(); return false;'></a></div>
							<a href='' class='tool_delete' title='".$Lang['SysMgr']['CycleDay']['DeletePeriod']."' onClick='removePeriodForm(); return false;'></a> 
						</div></td>
					</tr>
				</table>";
		$x .= "<table class='common_table_list'>
					<thead>
						<tr>
							<th width='10%'>".$Lang['SysMgr']['CycleDay']['PeriodStart']['FieldTitle']."</th>
							<th width='10%'>".$Lang['SysMgr']['CycleDay']['PeriodEnd']['FieldTitle']."</th>
							<th width='10%'>".$Lang['SysMgr']['CycleDay']['PeriodType']['FieldTitle']."</th>
							<th width='15%'>".$Lang['SysMgr']['CycleDay']['CycleType']['FieldTitle']."</th>
							<th width='10%'>".$Lang['SysMgr']['CycleDay']['DaysOfACycle']['FieldTitle']."</th>
							<th width='5%'>".$Lang['SysMgr']['CycleDay']['FirstDay']['FieldTitle']."</th>
							<th width='12%'>".$Lang['SysMgr']['CycleDay']['SaturdayCounted']['FieldTitle']."</th>
							<!--<th width='1%'>&nbsp;</th>-->
							<th width='1%'><input type='checkbox' id='checkAll' name='checkAll'></th>
						</tr>
					</thead>
					<tbody>";
		
		if(sizeof($arrPeriods)>0){
			for($i=0; $i<sizeof($arrPeriods); $i++){
				list($period_id, $period_start, $period_end, $period_type, $cycle_type, $period_day, $first_day, $sat_count, $record_type, $record_status, $date_input, $date_modified) = $arrPeriods[$i];
								
				switch ($period_type)
				{
					case 0: $str_period_type = $Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle']; break;
					case 1: $str_period_type = $Lang['SysMgr']['CycleDay']['PeriodType']['Generation']; break;
					case 2: $str_period_type = $Lang['SysMgr']['CycleDay']['PeriodType']['FileImport']; break;
					default: $str_period_type = $Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle']; break;
				}
				switch ($cycle_type)
				{
					case 0: $str_cycle_type = $Lang['SysMgr']['CycleDay']['CycleType']['Numeric']; break;
					case 1: $str_cycle_type = $Lang['SysMgr']['CycleDay']['CycleType']['Alphabets']; break;
					case 2: $str_cycle_type = $Lang['SysMgr']['CycleDay']['CycleType']['Roman']; break;
					default: $str_cycle_type = $Lang['SysMgr']['CycleDay']['CycleType']['Numeric']; break;
				}
				//$str_satCount = ($sat_count==1? "<img src=\"$image_path/".$LAYOUT_SKIN."/icon_tick.gif\">":"&nbsp;");
				//$str_satCount = ($sat_count==1? "<img src=\"$image_path/".$LAYOUT_SKIN."/icon_tick.gif\">":"<img src=\"$image_path/".$LAYOUT_SKIN."/icon_cross.gif\">");
				$str_satCount = ($sat_count==1? $Lang['SysMgr']['CycleDay']['SaturdayCounted']['Yes'] : $Lang['SysMgr']['CycleDay']['SaturdayCounted']['No']);
				
				if ($period_type==0)
				{
					$str_cycle_type = "--";
					$period_day = "--";
					$str_satCount = "--";
					$first_day = "--";
				}
				else if ($period_type==2)
				{
					$str_cycle_type = "--";
					$period_day = "--";
					$str_satCount = "--";
					$first_day = "--";
				}
				else
				{
					if ($cycle_type==1)
					{
						$first_day = $this->array_alphabet[$first_day];
					}
					else if ($cycle_type==2)
					{
						$first_day = $this->array_roman[$first_day];
					}
					else
					{
						$first_day = $this->array_numeric[$first_day];
					}
				}
				
				$x .= "	<tr class='sub_row' id='$period_id'>
							<td width='10%'>$period_start</td>
							<td width='10%'>$period_end</td>
							<td width='10%'>$str_period_type</td>
							<td width='10%'>$str_cycle_type</td>
							<td width='10%'>$period_day</td>
							<td width='10%'>$first_day</td>
							<td width='10%'>$str_satCount</td>
							<!--<td width='1%'><a href='#TB_inline?height=300&width=500&inlineId=FakeLayer' class='thickbox' title='".$Lang['SysMgr']['CycleDay']['EditPeriod']."' onClick='return editPeriodForm($period_id); return false;'><img src=\"$image_path/".$LAYOUT_SKIN."/icon_edit_b.gif\" border='0'></a></td>-->
							<td width='1%'><input type='checkbox' id='period_id[]' name='period_id[]' value='$period_id' onClick='checkEdit(this.value);'></td>
						</tr>";				
			}
		}else{
			$x .= "	<tr class='sub_row'>
						<td colspan='8' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td>
					</tr>";
		}
			
		$x .= " 		</tbody>
					</table>";
		return $x;
	}
	
	function newDefaultPeriodForm($AcademicYearID,$StartDate,$EndDate='',$PeriodType='')
	{
		include_once('libinterface.php');
		global $Lang;
		
		if($AcademicYearID != ""){
			$selectedSchoolYear = $AcademicYearID;
		}else{
			$arrResult = getCurrentAcademicYearAndYearTerm();
			$selectedSchoolYear = $arrResult[0];
		}
				
		$linterface = new interface_html();
		
		## Gen Cycle Type Selection
		$select_cycletype = "<SELECT name=CycleType id='CycleType' onChange=changeFirstDaySelect()>\n";
		$select_cycletype .= "<OPTION value=0 >".$Lang['SysMgr']['CycleDay']['CycleType']['Numeric']."</OPTION>";
		$select_cycletype .= "<OPTION value=1 >".$Lang['SysMgr']['CycleDay']['CycleType']['Alphabets']."</OPTION>";
		$select_cycletype .= "<OPTION value=2 >".$Lang['SysMgr']['CycleDay']['CycleType']['Roman']."</OPTION>";
		$select_cycletype .= "</SELECT>";
		
		## Gen No of day selection
		$select_days = "<SELECT name=PeriodDays id='PeriodDays' onChange='changeFirstDaySelect(); loadTimetableSelection( \"$selectedSchoolYear\",\"$StartDate\",this.value);'>";
		for ($i=1; $i<=26; $i++)
		{
		     $select_days .= "<OPTION value=$i ".($i==6?"SELECTED":"").">$i</OPTION>";
		}
		$select_days.= "</SELECT>";
		
		## Gen First Day Selection
		$select_firstday = "<SELECT name=FirstDay id='FirstDay'>";
		for ($i=0; $i<6; $i++)
		{
		     $string = $this->array_numeric[$i];
		     $select_firstday .= "<OPTION value=$i>$string</OPTION>";
		}
		$select_firstday .= "</SELECT>";

		$x .= "<br>";
		$x .= "<center>";
		$x .= "<div id='add_period' style='height:300px;'>";
		$x .= "<table width='80%' border='0' cellpadding='3' cellspacing='0'>";
		## Period Start
		$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['CycleDay']['PeriodStart']['FieldTitle']."</td><td>:</td><td align='left'><input type='input' name='period_start' id='period_start' value='$StartDate' onKeyUp='jsClearWarningMsg(\"PeriodStart_WarningLayer\");' OnChange='jsClearWarningMsg(\"PeriodStart_WarningLayer\");'><span id='PeriodStart_WarningLayer'></div></td></tr>";
		## Period End
		$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['CycleDay']['PeriodEnd']['FieldTitle']."</td><td>:</td><td align='left'><input type='input' name='period_end' id='period_end' value='$EndDate' onKeyUp='jsClearWarningMsg(\"PeriodEnd_WarningLayer\");' OnChange='jsClearWarningMsg(\"PeriodEnd_WarningLayer\");'><span id='PeriodEnd_WarningLayer'></div></td></tr>";
		
		if($PeriodType != "")
		{
			if($PeriodType == 1) {
				$PeriodType1_Check = " CHECKED ";
				$display = "inline";
			} else {
				$PeriodType0_Check = " CHECKED ";
				$display = "none";
			}
		}else{
			$PeriodType1_Check = " CHECKED ";
			$PeriodType0_Check = " ";
			$display = "inline";
		}
		
		## Period Type
		$x .= "<tr>
					<td width='40%' align='left' valign='top'>".$Lang['SysMgr']['CycleDay']['PeriodType']['FieldTitle']."</td><td valign='top'>:</td>
					<td align='left'>
								<input type='radio' id='targetPeriodType1' value='1' onClick='showAdditionalInfo( (this.checked==true?this.value:\"-1\") ); loadTimetableSelection(\"$selectedSchoolYear\",\"$StartDate\",document.getElementById(\"PeriodDays\").value);' $PeriodType1_Check>".$Lang['SysMgr']['CycleDay']['PeriodType']['HaveCycle']." 
								<input type='radio' id='targetPeriodType0' value='0' onClick='showAdditionalInfo( (this.checked==true?this.value:\"-2\") ); loadTimetableSelection(\"$selectedSchoolYear\",\"$StartDate\",7);' $PeriodType0_Check>".$Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle']."
					</td>
				</tr>";
		## BgColor
		$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['CycleDay']['BackgroundColor']['FieldTitle']."</td><td>:</td><td align='left'><input id='bgColorCode' name='bgColorCode' type='text' value='#8c66d9' /></td></tr>";
		## Gen TimeTable Selection
		$arrTimeTable  = getAvailableTimetableTemplateWithDaysChecking($selectedSchoolYear,'',$StartDate,'cycle',6);	// 6 is the default num of cycle day
		$timetable_template_selection = getSelectByArray($arrTimeTable, "name='TimetableTemplate' id='TimetableTemplate'",'',0,0,$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SelectTimeTable']);
		
		## Time Table Template
		$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['TimeTable']."</td><td>:</td><td align='left' id='timetable_layer'>$timetable_template_selection</td></tr>";
		$x .= "</table>";
		
		$x .= "<div id='normal_additional_info' style='display: $display;'>";
		$x .= "<table width='80%' border='0' cellpadding='3' cellspacing='0'>";
		## Cycle Type
		$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['CycleDay']['CycleType']['FieldTitle']."</td><td>:</td><td align='left'>".$select_cycletype."</td></tr>";
		## No of cycle day
		$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['CycleDay']['DaysOfACycle']['FieldTitle']."</td><td>:</td><td align='left'>".$select_days."</td></tr>";
		## First Day
		$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['CycleDay']['FirstDay']['FieldTitle']."</td><td>:</td><td align='left'>".$select_firstday."</td></tr>";
		## Count Saturday
		//$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['CycleDay']['SaturdayCounted']['FieldTitle']."</td><td>:</td><td align='left'><input type=checkbox name=satCount id='satCount' value=1></td></tr>";
		## Skip on weekday
		$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['CycleDay']['SkipOnWeekday']['FieldTitle']."</td><td>:</td>";
		$x .= "<td>";
		for($i=1; $i<=6; $i++){
			if($i != 1)
				$x .= "&nbsp;";
			$default_checked = "";
			if($i == 6)
				$default_checked = "CHECKED";
			$x .= "<input type='checkbox' name='SkipOnWeekDay[]' id='SkipOnWeekDay$i' value='$i' $default_checked>".$Lang['General']['DayType4'][$i];
		}
		$x .= "</td></tr>";
		
		$x .= "</table>";
		$x .= "</div>";
		$x .= "</center>";
		$x .= "</div>";
		
		$x .= "<center>";					
		$x .= "<div class='edit_bottom'>";
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
		## Button Layer
		$x .= "<tr><td><p class='spacer'></p></td></tr>";
		$x .= "<tr><td align='center'>
					<input id='NewPeriodFormSubmitBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Submit']."' onclick='updateNewPeriod();' >
					<input id='NewPeriodFormCancelBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Cancel']."' onclick='window.top.tb_remove();' >
				</td></tr>";
		$x .= "</table>";
		$x .= "</div>"; 
		$x .= "</center>";
		
		return $x;
	}
	
	function newPeriodForm($PeriodStart='',$PeriodEnd='',$PeriodType='')
	{
		include_once('libinterface.php');
		global $Lang;
		
		$linterface = new interface_html();
		
		$arr_period_type = array(
								array(0,$Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle']),
								array(1,$Lang['SysMgr']['CycleDay']['PeriodType']['Generation']),
								array(2,$Lang['SysMgr']['CycleDay']['PeriodType']['FileImport']),
								);
		
		$selection_period_type = getSelectByArray($arr_period_type,' name="targetPeriodType" id="targetPeriodType" onChange="changePeriodType(this.value);" ',$PeriodType,0,1);
		
		$select_cycletype = "<SELECT name=CycleType id='CycleType' onChange=changeFirstDaySelect()>\n";
		$select_cycletype .= "<OPTION value=0 >".$Lang['SysMgr']['CycleDay']['CycleType']['Numeric']."</OPTION>";
		$select_cycletype .= "<OPTION value=1 >".$Lang['SysMgr']['CycleDay']['CycleType']['Alphabets']."</OPTION>";
		$select_cycletype .= "<OPTION value=2 >".$Lang['SysMgr']['CycleDay']['CycleType']['Roman']."</OPTION>";
		$select_cycletype .= "</SELECT>";
		
		$select_days = "<SELECT name=PeriodDays id='PeriodDays' onChange=changeFirstDaySelect()>";
		for ($i=1; $i<=26; $i++)
		{
		     $select_days .= "<OPTION value=$i ".($i==6?"SELECTED":"").">$i</OPTION>";
		}
		$select_days.= "</SELECT>";
		
		$select_firstday = "<SELECT name=FirstDay id='FirstDay'>";
		for ($i=0; $i<6; $i++)
		{
		     $string = $this->array_numeric[$i];
		     $select_firstday .= "<OPTION value=$i>$string</OPTION>";
		}
		$select_firstday .= "</SELECT>";

		$x = "<center>";
		$x .= "<div id='add_period'>";
		$x .= "<table width='70%' border='0' cellpadding='3' cellspacing='0'>";
		$x .= "<tr><td width='50%' align='left'>".$Lang['SysMgr']['CycleDay']['PeriodStart']['FieldTitle'].":</td><td align='left'><input type='input' name='period_start' id='period_start' value='$PeriodStart'></td></tr>";
		$x .= "<tr><td width='50%' align='left'>".$Lang['SysMgr']['CycleDay']['PeriodEnd']['FieldTitle'].":</td><td align='left'><input type='input' name='period_end' id='period_end' value='$PeriodEnd'></td></tr>";
		$x .= "<tr><td width='50%' align='left'>".$Lang['SysMgr']['CycleDay']['PeriodType']['FieldTitle'].":</td><td align='left'>$selection_period_type</td></tr>";
		$x .= "</table>";
		
		if($PeriodType == 1)
			$display1 = "inline";
		else
			$display1 = "none";
		$x .= "<div id='normal_additional_info' style='display: ".$display1.";'>";
		$x .= "<br>";
		$x .= "<table width='70%' border='0' cellpadding='3' cellspacing='0'>";
		$x .= "<tr><td width='50%' align='left'>".$Lang['SysMgr']['CycleDay']['CycleType']['FieldTitle'].":</td><td align='left'>".$select_cycletype."</td></tr>";
		$x .= "<tr><td width='50%' align='left'>".$Lang['SysMgr']['CycleDay']['DaysOfACycle']['FieldTitle'].":</td><td align='left'>".$select_days."</td></tr>";
		$x .= "<tr><td width='50%' align='left'>".$Lang['SysMgr']['CycleDay']['FirstDay']['FieldTitle'].":</td><td align='left'>".$select_firstday."</td></tr>";
		$x .= "<tr><td width='50%' align='left'>".$Lang['SysMgr']['CycleDay']['SaturdayCounted']['FieldTitle'].":</td><td align='left'><input type=checkbox name=satCount id='satCount' value=1></td></tr>";
		$x .= "</table>";
		$x .= "</div>";
		
		if($PeriodType == 2)
			$display2 = "inline";
		else
			$display2 = "none";
			
		$x .= "<div id='import_additional_info' style='display: ".$display2.";'>";
		$x .= "<br>";
		$x .= "<table width='70%' border='0' cellpadding='3' cellspacing='0'>";
		$x .= "<tr><td width='50%' align='left'>&nbsp;</td><td align='left'><input type=file size='40' name=userfile><br>
				". str_replace("'", "\'",$linterface->GET_IMPORT_CODING_CHKBOX()) ." ".
				$Lang['SysMgr']['CycleDay']['FileImport']['Description']."
				<br>".$Lang['SysMgr']['CycleDay']['FileImport']['Warning']."<br>
				<a class=functionlink_new href=\"".GET_CSV("importsample.csv")."\">$i_general_clickheredownloadsample</a>
				</td></tr>";
		$x .= "</table>";
		$x .= "</div>";

		$x .= "</center>";
		
		$x .= "<br>";
		$x .= "<center>";
		$x .= "<div class='edit_bottom'>";
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
		$x .= "<tr><td><p class='spacer'></p></td></tr>";
		$x .= "<tr><td align='center'>
					<input id='NewPeriodFormSubmitBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Submit']."' onclick='updateNewPeriod();' >
					<input id='NewPeriodFormCancelBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Cancel']."' onclick='window.top.tb_remove();' >
				</td></tr>";
		$x .= "</table>";
		$x .= "</div>"; 
		$x .= "</div>"; 
		$x .= "</center>";
		
		return $x;
	}
	
	function editPeriodForm($PeriodID)
	{
		include_once('libinterface.php');
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$linterface = new interface_html();
		
		## get Selected Period's School Year ID ##
		$sql = "SELECT 
					DISTINCT a.AcademicYearID 
				FROM 
					ACADEMIC_YEAR_TERM AS a 
				WHERE 
					(SELECT PeriodStart FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE PeriodID = $PeriodID) 
					BETWEEN a.TermStart AND a.TermEnd";
		$arrResult = $this->returnVector($sql);
		if(sizeof($arrResult)>0){
			$selectedSchoolYear = $arrResult[0];
		}else{
			$arrResult = getCurrentAcademicYearAndYearTerm();
			$selectedSchoolYear = $arrResult[0];
		}
		
		$sql = "SELECT 
					a.PeriodID, a.PeriodStart, a.PeriodEnd, a.PeriodType, a.CycleType, a.PeriodDays, a.FirstDay, a.SaturdayCounted, a.SkipOnWeekday, a.ColorCode, b.TimetableID
				FROM 
					INTRANET_CYCLE_GENERATION_PERIOD AS a LEFT OUTER JOIN
					INTRANET_PERIOD_TIMETABLE_RELATION AS b ON (a.PeriodID = b.PeriodID)
				WHERE a.PeriodID IN ($PeriodID)";
		$arrResult = $this->returnArray($sql,10);
		
		
		## Get Special Timetable Settings of the Time Zone
		$SpecialTimetableInfoArr = $this->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsIDArr='', $PeriodID);
		$numOfSpecialTimetable = count($SpecialTimetableInfoArr);
		if ($numOfSpecialTimetable > 0) {
			$WarningMsgArr = array();
			
			$WarningMsgArr[] = $Lang['SysMgr']['CycleDay']['WarningArr']['EditTimezoneDayRangeWithSpecialTimetable'];
			
			// For cycle day settings only => disable related options if there are special timetable settings
			if ($arrResult[0]['PeriodType']==1) {
				$WarningMsgArr[] = $Lang['SysMgr']['CycleDay']['WarningArr']['EditCycleDayTimezoneWithSpecialTimetable'];
				$Disabled = 1;
				$DisabledText = 'disabled';
			}
			
			$SpecialtimetableWarningTable = '<tr><td colspan="3">'.$linterface->Get_Warning_Message_Box('', $WarningMsgArr).'</td></tr>'."\n";
		}
		
		$x .= "<br>";
		$x .= "<center>";
		$x .= "<div id='edit_period' style='height:300px; overflow: auto;'>";
		$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0'>";
			$x .= $SpecialtimetableWarningTable;
		if(sizeof($arrResult)>0){
			for($i=0; $i<sizeof($arrResult); $i++){
				list($period_id, $period_start, $period_end, $period_type, $cycle_type, $period_days, $first_day, $sat_count, $SkipOnWeekday, $colorCode, $timetable_id) = $arrResult[$i];
					
				$x .= "<input type='hidden' id='period_id' name='period_id' value='$period_id' />";
				$x .= "<input type='hidden' id='HasSpecialTimetableSettings' name='HasSpecialTimetableSettings' value='$Disabled' />";
				## Period Start
				$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['CycleDay']['PeriodStart']['FieldTitle']."</td><td>:</td><td align='left'><input type='input' name='period_start' id='period_start' value='$period_start'></td></tr>";
				## Period End
				$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['CycleDay']['PeriodEnd']['FieldTitle']."</td><td>:</td><td align='left'><input type='input' name='period_end' id='period_end' value='$period_end'></td></tr>";
				
				if($period_type == 1){
					$PeriodType1_Checked = " CHECKED ";
					$display = "Inline";
				} else {
					$PeriodType0_Checked = " CHECKED ";
					$display = "none";
				}
				
				## Period Type
				$x .= "<tr>";
					$x .= "<td width='30%' align='left' valign='top'>".$Lang['SysMgr']['CycleDay']['PeriodType']['FieldTitle']."</td><td valign='top'>:</td>";
					$x .= "<td align='left'>";
						//$x .= "<input type='radio' id='targetPeriodType1' value='1' onClick='showAdditionalInfo( (this.checked==true?this.value:\"-1\") ); loadTimetableSelection(\"$selectedSchoolYear\",\"$period_start\",document.getElementById(\"PeriodDays\").value);' $PeriodType1_Checked >".$Lang['SysMgr']['CycleDay']['PeriodType']['HaveCycle']; 
						//$x .= "<input type='radio' id='targetPeriodType0' value='0' onClick='showAdditionalInfo( (this.checked==true?this.value:\"-2\") ); loadTimetableSelection(\"$selectedSchoolYear\",\"$period_start\",7);' $PeriodType0_Checked >".$Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle'];
						$x .= $linterface->Get_Radio_Button('targetPeriodType1', 'targetPeriodType1', 1, $PeriodType1_Checked, $Class="", $Lang['SysMgr']['CycleDay']['PeriodType']['HaveCycle'], "showAdditionalInfo( (this.checked==true?this.value:'-1') ); loadTimetableSelection('$selectedSchoolYear','$period_start',document.getElementById('PeriodDays').value);", $Disabled);
						$x .= $linterface->Get_Radio_Button('targetPeriodType0', 'targetPeriodType0', 0, $PeriodType0_Checked, $Class="", $Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle'], "showAdditionalInfo( (this.checked==true?this.value:'-2') ); loadTimetableSelection('$selectedSchoolYear','$period_start',7);", $Disabled);
					$x .= "</td>";
				$x .= "</tr>";

				## BgColor
				$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['CycleDay']['BackgroundColor']['FieldTitle']."</td><td>:</td><td align='left'><input id='bgColorCode' name='bgColorCode' type='text' value='$colorCode' /></td></tr>";
				## Gen TimeTable Selection
				if($period_type == 1){
					$arrTimeTable  = getAvailableTimetableTemplateWithDaysChecking($selectedSchoolYear,'',$period_start,'cycle',$period_days);		//  6 - default num of day for cycle day
				}else{
					$arrTimeTable  = getAvailableTimetableTemplateWithDaysChecking($selectedSchoolYear,'',$period_start,'weekday',7);		//  7 - default num of day for weekday
				}
				$timetable_template_selection = getSelectByArray($arrTimeTable, "name='TimetableTemplate' id='TimetableTemplate'",$timetable_id,0,0,$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SelectTimeTable']);
				## TimeTable Template
				$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['TimeTable']."</td><td>:</td><td align='left'><div id='timetable_layer'>".$timetable_template_selection."</div></td></tr>";
				$x .= "</table>";
				### Div for Cycleday info
				$x .= "<center>";
				$x .= "<div id='normal_additional_info' style='display: ".$display."'>";
				$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0'>";
				switch($cycle_type){
					case 0: $str_cycle_type1 = " SELECTED "; break;
					case 1: $str_cycle_type2 = " SELECTED "; break;
					case 2: $str_cycle_type3 = " SELECTED "; break;
					default: $str_cycle_type1 = " SELECTED "; break;
				}
				## Gen Cycle day type selection
				$select_cycletype = "<SELECT name='CycleType' id='CycleType' onChange='changeFirstDaySelect();' $DisabledText>\n";
				$select_cycletype .= "<OPTION value='0' $str_cycle_type1>".$Lang['SysMgr']['CycleDay']['CycleType']['Numeric']."</OPTION>";
				$select_cycletype .= "<OPTION value='1' $str_cycle_type2>".$Lang['SysMgr']['CycleDay']['CycleType']['Alphabets']."</OPTION>";
				$select_cycletype .= "<OPTION value='2' $str_cycle_type3>".$Lang['SysMgr']['CycleDay']['CycleType']['Roman']."</OPTION>";
				$select_cycletype .= "</SELECT>";
				
				## Gen no of day selection
				$select_days = "<SELECT name='PeriodDays' id='PeriodDays' onChange='changeFirstDaySelect(); loadTimetableSelection( \"$selectedSchoolYear\",\"$period_start\",this.value);' $DisabledText>";
				for ($i=1; $i<=26; $i++)
				{
					 $select_days .= "<OPTION value=$i ".($i==$period_days?"SELECTED":"").">$i</OPTION>";
				}
				$select_days.= "</SELECT>";
				
				## Gen First day selection
				$select_firstday = "<SELECT name='FirstDay' id='FirstDay' $DisabledText>";
				for ($i=0; $i<$period_days; $i++)
				{
					if($cycle_type == 0){
						$string = $this->array_numeric[$i];
					}elseif($cycle_type == 1){
						$string = $this->array_alphabet[$i];
					}else{
						$string = $this->array_roman[$i];
					}
					$select_firstday .= "<OPTION value=$i ".($i == $first_day?" SELECTED ":"").")>$string</OPTION>";
				}
				$select_firstday .= "</SELECT>";
				
				## Cycle day type
				$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['CycleDay']['CycleType']['FieldTitle']."</td><td>:</td><td align='left'>$select_cycletype</td></tr>";
				## No of Day for a period
				$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['CycleDay']['DaysOfACycle']['FieldTitle']."</td><td>:</td><td align='left'>".$select_days."</td></tr>";
				## Start Day for a period
				$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['CycleDay']['FirstDay']['FieldTitle']."</td><td>:</td><td align='left'>".$select_firstday."</td></tr>";
				## Saturday Count?
				//$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['CycleDay']['SaturdayCounted']['FieldTitle']."</td><td>:</td><td align='left'><input type=checkbox name=satCount id='satCount' value=1 ".($sat_count==1?"CHECKED":"")."></td></tr>";
				
				$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['CycleDay']['SkipOnWeekday']['FieldTitle']."</td><td>:</td>";
				$x .= "<td>";
				$arrSkipOnWeekday = explode(",",$SkipOnWeekday);
				for($i=1; $i<=6; $i++){
					if($i != 1)
						$x .= "&nbsp;";
					$skip_on_weekday_checked = "";
					if(in_array($i,$arrSkipOnWeekday)){
						$skip_on_weekday_checked = "CHECKED";
					}
					$x .= "<input type='checkbox' name='SkipOnWeekDay[]' id='SkipOnWeekDay$i' value='$i' $skip_on_weekday_checked $DisabledText>".$Lang['General']['DayType4'][$i];
				}
				$x .= "</td></tr>";
				
				$x .= "</table>";
				$x .= "</div>";
				$x .= "</center>";
				$x .= "</div>";
				$x .= "<br>";
				## Button layer
				$x .= "<div class='edit_bottom'>";
				$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
				$x .= "<tr><td><p class='spacer'></p></td></tr>";
				$x .= "<tr><td align='center'>
							<input id='EditPeriodFormSubmitBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Submit']."' onclick='updateEditPeriod($selectedSchoolYear);' >
							<input id='EditPeriodFormCancelBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Cancel']."' onclick='window.top.tb_remove();' >
							<input id='EditPeriodFormDeleteBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Delete']."' onclick='deletePeriod($PeriodID);' >
						</td></tr>";
				$x .= "</table>";
				$x .= "</div>";
			}
		}		
		return $x;
	}
	
	function getProductionCycleDaysCalendarView($type=1,$current_school_year_id='',$class_group_id='',$viewOnly=false)
	{
		include_once("libdb.php");
		include_once("form_class_manage.php");
		include_once("libcal.php");
		include_once("libcalevent.php");
		include_once("libcalevent2007a.php");
		include_once("libinterface.php");
		
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path, $button_print, $sys_custom;
		$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
		if($isKIS) {
			include_once("libclassgroup.php");
			$lclassgroup = new Class_Group();
			$classGroupSelection = $lclassgroup->Get_Class_Group_Selection("class_group_id", '', 'changeClassGroup(this.value);', 0, $Lang['SysMgr']['FormClassMapping']['AllClassGroup']);
		}
		
		$lfcm = new form_class_manage();
		$libcalevent = new libcalevent2007();
		$linterface = new interface_html();
				
		//$academic_year_selection = getSelectAcademicYear("academic_year","onChange='ChangeSchoolYear($type,this.value);'", 0);
		$showSchoolYearSelect = true;
		$showOptionsBox = true;
		if ($sys_custom['LivingHomeopathy']) {
			$showSchoolYearSelect = false;
			$showOptionsBox = false;
			if ($_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]) {
				$showOptionsBox = true;
			}
		}
		if ($showSchoolYearSelect) {
			// get school year selection
            $NoPastYear = $viewOnly ? 1 : 0;
			$SchoolYearList = $lfcm->Get_Academic_Year_List($AcademicYearIDArr='', $OrderBySequence=1, $excludeYearIDArr=array(), $NoPastYear);
			$academic_year_selection .= '<select name="academic_year" id="academic_year" onchange="ChangeSchoolYear('.$type.',this.value);">';
			$academic_year_selection .= '<option value="">'.$Lang['SysMgr']['FormClassMapping']['SelectSchoolYear'].'</option>';
			for ($i=0; $i< sizeof($SchoolYearList); $i++) {
				$SchoolYearName = Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN']);
				
				if (!$NoCheckCurrent) {
					if ($SchoolYearList[$i]['CurrentSchoolYear'] == '1') 
						$AcademicYearID = $SchoolYearList[$i]['AcademicYearID'];
				}
				
				unset($Selected);
				$Selected = ($current_school_year_id == $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
				$academic_year_selection .= '<option value="'.$SchoolYearList[$i]['AcademicYearID'].'" '.$Selected.'>'.$SchoolYearName.'</option>';
			}
			$academic_year_selection .= '</select>';
		}
		
		$x = $this->initJavaScript();
		
		if ($showSchoolYearSelect) {
			$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
			$x .= "<tr><td align='left'>".$Lang['General']['SchoolYear']." : ".$academic_year_selection;
			if($isKIS) {
				$x .= "&nbsp;".$Lang['SysMgr']['FormClassMapping']['ClassGroup']." : ".$classGroupSelection;
			}
			$x .= "</td><td>&nbsp;</td></tr>";
			$x .= "</table>";
		}else{
			$x .= "<input type=\"hidden\" name=\"academic_year\" id=\"academic_year\" value=\"".$current_school_year_id."\" />"; // as ajax parameter
		}
		if ($showOptionsBox && !$viewOnly) {
			$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
			$x .= $this->generateCalPrintingOptionsBox();
			$x .= "</table>";
		}
		$x .= "";
		if ($showOptionsBox && !$viewOnly) {
			$x .= "<div class='bottom_remark' style='text-align:left;'>";
		} else {
			$x .= "<div style='text-align:left;'>";
		}
		$x .= $this->generatePrintingVersionLabels(true);
		$x .= "<div id='main_body'>";
		$x .= "<div id='Production_Calendar'>";
		$x .= $this->loadCalendar($current_school_year_id,1, $ParIsPrintingVersion=false, $ParPrintingVersionArr="", $ClassGroupID="", $viewOnly);
		$x .= "</div>";
		$x .= "</div>";
		return $x;
	}
	
	/**
	 * @param $printingVersionArr[] parameters for printing version
	 * 		array(bool ["IS_COLOR_PRINT"], bool ["IS_PRINT_NO_EVENT"], int ["NUM_CAL_PER_PAGE"])
	 * 
	 */
	function loadCalendar($SchoolYearID='',$InitCall=0, $ParIsPrintingVersion=false, $ParPrintingVersionArr="", $ClassGroupID="", $viewOnly=false)
	{
		include_once("libdb.php");
		include_once("form_class_manage.php");
		include_once("libcal.php");
		include_once("libcalevent.php");
		include_once("libcalevent2007a.php");
		include_once("libinterface.php");
		
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom;

		$lfcm = new form_class_manage();
		$linterface = new interface_html();
		$libcalevent = new libcalevent2007();
		
		if($SchoolYearID == "" && $this->IsCurrentDateInTerm() && $InitCall == 1){
			$SchoolYearID = $lfcm->getCurrentAcademicaYearID();
		}
		
		if($SchoolYearID != ""){
			
			## Check the selected school year id >= current school year
			## step 1: get all the academic year which is current year or year in future
			$sql = "SELECT DISTINCT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE NOW() <= TermEnd";	
			$result = $this->returnVector($sql);
			## step 2: check the selected school year id is in the return array
			if(in_array($SchoolYearID,$result))
			{
				$ShowArchiveRecord = 0;
			}else{
				$ShowArchiveRecord = 1;
			}
			if ($ShowArchiveRecord == 0 && $sys_custom["SchoolCalendarWhenNotAdminReadOnly"] && !$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]) {
				$ShowArchiveRecord = 1;
			}
			
			$showThreeYearWithPastToNextOnly = false;
			if ($sys_custom['LivingHomeopathy']) {
				$showThreeYearWithPastToNextOnly = true;
			}
			
			if ($showThreeYearWithPastToNextOnly) {
				$arrResult = $lfcm->Get_Academic_Year_List( array($SchoolYearID) );
			} else {
				$arrResult = $lfcm->Get_Academic_Year_List();
			}
			
			if(sizeof($arrResult)>0){
				for($i=0; $i<sizeof($arrResult); $i++){
					list($AcademicYearID, $YearNameEN, $YearNameB5, $Sequence, $CurrentSchoolYear, $StartDate, $EndDate) = $arrResult[$i];
					
					if($AcademicYearID==$SchoolYearID){
						$TargetStartDate = $StartDate;
						$TargetEndDate = $EndDate;
						$StartYear = substr($StartDate,0,4);
						$StartMonth = substr($StartDate,5,2);
						$EndYear = substr($EndDate,0,4);
						$EndMonth = substr($EndDate,5,2);
					}
				}
			}
					
			$month = intval($StartMonth);
			$year = intval($StartYear); // get Current Year
			if($sys_custom['Calendar']['ShowCurrentOnly']){
			    $month = intval(date('m'));
			    $year = date('Y');
			}
			$end_month = intval($EndMonth);
			$end_year = intval($EndYear);
				
			// calculate how many months dose the school year have
			$FinalEndDate = date("Y-m-d",mktime(0, 0, 0, ($EndMonth + 1), 0, $EndYear));
			$FinalStartDate = date("Y-m-d",mktime(0, 0, 0, ($StartMonth), 1, $StartYear));
			
			// [2015-0831-1444-14222] fixed: display extra month due to calculation error in timestamp
			//$NumOfMonth = round( abs(strtotime($FinalEndDate)-strtotime($FinalStartDate)) / 60/60/24/7/4 , 0 ) - 1;
			//$NumOfMonth = ( abs(strtotime($FinalEndDate)-strtotime($FinalStartDate)) / 60/60/24/7/4 ) - 1;
			$NumOfMonth = getMonthDifferenceBetweenDates($FinalStartDate, $FinalEndDate) + 1;
			
			if($NumOfMonth < 1)
			{
				$NumOfMonth = 1;
			}else{
				//$NumOfMonth = round($NumOfMonth,0);
				$NumOfMonth = ceil($NumOfMonth);
			}
			//for($i=1; $i<=12; $i++)
			for($i=1; $i<=$NumOfMonth; $i++)
			{
				$arrDate[$i]['Month'] = $month;
				$arrDate[$i]['Year'] = $year;
				$month = $month + 1;
				if($month > 12){
					$month = $month - 12;
					$year = $year + 1;	
				}
			}

			if ($ParIsPrintingVersion) {
				
				for($i=1; $i<=$NumOfMonth; $i++){
					$month = $arrDate[$i]['Month'];
					$year = $arrDate[$i]['Year'];
					${calendarTable.$i} = $libcalevent->displayCalandar_CyclePeriodPrintingView($month, $year, $ShowArchiveRecord, $ParPrintingVersionArr['IS_COLOR_PRINT'], $ClassGroupID);
				}
				
				for($i=1; $i<=$NumOfMonth; $i++){
					$month = $arrDate[$i]['Month'];
					$year = $arrDate[$i]['Year'];
					${eventFlag.$i} = false;
					${eventTable.$i} = $libcalevent->displayCalandar_MonthlyEventTablePrintingView($month, $year, $ShowArchiveRecord, $ParPrintingVersionArr['IS_COLOR_PRINT'], ${eventFlag.$i}, $ClassGroupID);
				}
				
				$x .= "<table id='calendarTable' width='100%' border='0' cellpadding='3' cellspacing='3'>";
				$x .= "
					<tr>
					  <td>&nbsp;</td>
					  <td style='border-bottom:1px solid #CFCFCF;'>" .
						$Lang['SysMgr']['SchoolCalendar']['Print']['EventTitle'] .
						"</td>
					</tr>
					";
				$recordShown = 0;
//				debug_r($ParPrintingVersionArr);
				for($i=1; $i<=$NumOfMonth; $i++)
				{
					if (!${eventFlag.$i} && !$ParPrintingVersionArr['IS_PRINT_NO_EVENT']) {}
					else {
//						$x .= "<tr valign=top>";
//						$x .= "<td width=\"50%\"><div id='CalendarLayer_$i'>".${calendarTable.$i}."</div></td>";
//						$x .= "<td width=\"50%\"><div id='EventLayer_$i'>".${eventTable.$i}."</div></td>";
//						$x .= "</tr>";
//						
//						if (fmod(++$recordShown, $ParPrintingVersionArr['NUM_CAL_PER_PAGE'])==0) {
//							$x .= "
//								<tr style='page-break-after: always;'>
//									<td colspan='2'>
//									</td>
//								</tr>
//								";
//							//$x .= "<hr style='page-break-after: always;' />";
//						}
						$pageBreak = "";
						if (fmod(++$recordShown, $ParPrintingVersionArr['NUM_CAL_PER_PAGE'])==0) {
// 							$pageBreak = "style='page-break-after: always;'";
						    $pageBreak = "class='pageBreak'";
						}
						$x .= "<tr valign=top $pageBreak>";
						$x .= "<td width=\"50%\"><div id='CalendarLayer_$i'>".${calendarTable.$i}."</div></td>";
						$x .= "<td width=\"50%\"><div id='EventLayer_$i'>".${eventTable.$i}."</div></td>";
						$x .= "</tr>";
					}
				}
				$x .= "</table>";
			} else {
				//for($i=1; $i<=12; $i++){
				for($i=1; $i<=$NumOfMonth; $i++){
					$month = $arrDate[$i]['Month'];
					$year = $arrDate[$i]['Year'];
					${calendarTable.$i} = $libcalevent->displayCalandar_CyclePeriodProductionView($month, $year, $ShowArchiveRecord, "", '', $ClassGroupID, $viewOnly);
				}
				
				//for($i=1; $i<=12; $i++){
				for($i=1; $i<=$NumOfMonth; $i++){
					$month = $arrDate[$i]['Month'];
					$year = $arrDate[$i]['Year'];
					${eventTable.$i} = $libcalevent->displayCalandar_MonthlyEventTable($month, $year, $ShowArchiveRecord, $ClassGroupID, $viewOnly);
				}
				
				$lastPublishInfo = $this->getHolidayLastModifiedInfo();

				if ($viewOnly == false) {
                    $x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
                    $x .= "<tr><td align='left'><span class='tabletextremark'>" . Get_Last_Modified_Remark($lastPublishInfo[0], $lastPublishInfo[1]) . "</span></td></tr>";
                    $x .= "</table>";
                }
				
				$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
				if($this->checkIntranetEventExistByDateRange($FinalStartDate,$FinalEndDate)){
					if ($_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] && !$viewOnly) {
						$remove_link = 'remove_all_holiday_event_confirm.php?TargetYearID='.$SchoolYearID;
						$x .= '<tr><td></td><td align="right">'.$linterface->GET_LNK_REMOVEALL($remove_link).'</td></tr>';
					}
				}
				for($i=1; $i<=$NumOfMonth; $i++)
				{
					$x .= "<tr valign=top>";
					$x .= "<td width=\"50%\"><div id='CalendarLayer_$i'>".${calendarTable.$i}."</div></td>";
					$x .= "<td width=\"50%\"><div id='EventLayer_$i'>".${eventTable.$i}."</div></td>";
					$x .= "</tr>";
				}
				$x .= "</table>";
			}
		}
		
		return $x;
	}
	
	function getCycleDaysCalendarPreview($current_school_year_id='')
	{
		include_once("libdb.php");
		include_once("form_class_manage.php");
		include_once("libcal.php");
		include_once("libcalevent.php");
		include_once("libcalevent2007a.php");
		include_once("libinterface.php");
		
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $image_path;

		$lfcm = new form_class_manage();
		$libcalevent = new libcalevent2007();
		$linterface = new interface_html();
		
		$academic_year_selection = getSelectAcademicYear("academic_year", "onChange='ChangeSchoolYear(this.value);'", $noFirst=0, $noPastYear=0, $current_school_year_id);
		
		$x = $this->initJavaScript();
					
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
		$x .= "<tr><td align='left'>".$Lang['General']['SchoolYear']." : ".$academic_year_selection."</td></tr>";
		$x .= "</table>";
		
		$x .= "<div id='main_body'>";
			$x .= "<div id='Production_Calendar'>";
			$x .= $this->loadPreviewCalendar($current_school_year_id,1);
			$x .= "</div>";
		$x .= "</div>";
		$x .= "<br />";
		
		$x .= $this->Get_Edit_Icon_Remarks();
		$x .= "<br />";
		
		$x .= '<div id="EditOptionDiv" class="selectbox_layer" style="visibility:hidden; width:200px; overflow:auto;"">'."\n";	
			### Hide Layer Button
			$x .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'."\n";
				$x .= '<tbody>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td align="right" style="border-bottom: medium none;">'."\n";
							$x .= '<a href="javascript:js_Hide_Option_Layer(\'EditOptionDiv\')"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td align="left" style="border-bottom: medium none;">'."\n";
							$x .= '<div id="EditOptionContentDiv"></div>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
		$x .= '</div>'."\n";
		$x .= "<br />";
		
		return $x;
	}
	
	function loadPreviewCalendar($SchoolYearID,$InitCall=0)
	{
		include_once("libdb.php");
		include_once("form_class_manage.php");
		include_once("libcal.php");
		include_once("libcalevent.php");
		include_once("libcalevent2007a.php");
		
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;

		$lfcm = new form_class_manage();
		$libcalevent = new libcalevent2007();
		
		
		if($SchoolYearID == "" && $this->IsCurrentDateInTerm() && $InitCall == 1){
			$SchoolYearID = $lfcm->getCurrentAcademicaYearID();
		}
		
		if($SchoolYearID != "")
		{
			## Check the selected school year id >= current school year
			## step 1: get all the academic year which is current year or year in future
			$sql = "SELECT DISTINCT AcademicYearID FROM ACADEMIC_YEAR_TERM WHERE NOW() <= TermEnd";	
			$result = $this->returnVector($sql);
			## step 2: check the selected school year id is in the return array
			if(in_array($SchoolYearID,$result))
			{
				$ShowArchiveRecord = 0;
			}else{
				$ShowArchiveRecord = 1;
			}
			
			$arrResult = $lfcm->Get_Academic_Year_List();
// 			debug_pr($arrResult);
			if(sizeof($arrResult)>0){
				for($i=0; $i<sizeof($arrResult); $i++){
					list($AcademicYearID, $YearNameEN, $YearNameB5, $Sequence, $CurrentSchoolYear, $StartDate, $EndDate) = $arrResult[$i];
	
					if($AcademicYearID==$SchoolYearID){
						$TargetStartDate = $StartDate;
						$TargetEndDate = $EndDate;
						$StartYear = substr($StartDate,0,4);
						$StartMonth = substr($StartDate,5,2);
						$EndYear = substr($EndDate,0,4);
						$EndMonth = substr($EndDate,5,2);
// 						debug_pr  ($arrResult);
					}
				}
			}

			$month = intval($StartMonth);
			$year = intval($StartYear); // get Current Year
			$end_month = intval($EndMonth);
			$end_year = intval($EndYear);
			
			// calculate how many months dose the school year have
			$FinalEndDate = date("Y-m-d",mktime(0, 0, 0, ($EndMonth + 1), 0, $EndYear));
			$FinalStartDate = date("Y-m-d",mktime(0, 0, 0, ($StartMonth), 1, $StartYear));
			
			// [2015-0831-1444-14222] fixed: display extra month due to calculation error in timestamp
			//$NumOfMonth = round( abs(strtotime($FinalEndDate)-strtotime($FinalStartDate)) / 60/60/24/7/4 , 0 ) - 1;
			//$NumOfMonth = ( abs(strtotime($FinalEndDate)-strtotime($FinalStartDate)) / 60/60/24/7/4 ) - 1;
			$NumOfMonth = getMonthDifferenceBetweenDates($FinalStartDate, $FinalEndDate) + 1;
			
			if($NumOfMonth < 1)
			{
				$NumOfMonth = 1;
			}else{
				//$NumOfMonth = round($NumOfMonth,0);
				$NumOfMonth = ceil($NumOfMonth);
			}
			
			//for($i=1; $i<=12; $i++)
			for($i=1; $i<=$NumOfMonth; $i++)
			{
				$arrDate[$i]['Month'] = $month;
				$arrDate[$i]['Year'] = $year;
				$month = $month + 1;
				if($month > 12){
					$month = $month - 12;
					$year = $year + 1;	
				}
			}
			
			### Get Special Timetable Settings
			$TimezoneInfoArr = $this->Get_All_TimeZone_By_Academic_Year($SchoolYearID);
			$TimezoneIDArr = Get_Array_By_Key($TimezoneInfoArr, 'PeriodID');
			$SpecialTimetableSettingsArr = $this->Get_Special_Timetable_Settings_Date_Info($SpecialTimetableSettingsIDArr='', $TimezoneIDArr);

			//for($i=1; $i<=12; $i++){
			for($i=1; $i<=$NumOfMonth; $i++){
				$month = $arrDate[$i]['Month'];
				$year = $arrDate[$i]['Year']; 
				${calendarTable.$i} = $libcalevent->displayCalandar_CyclePeriodPreview($month, $year, $ShowArchiveRecord, $order="", $SpecialTimetableSettingsArr);
			}
			
			$lastModifedInfo = $this->getTimeZoneLastModifiedInfo();
			$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
			$x .= "<tr><td align='left'><span class='tabletextremark'>".Get_Last_Modified_Remark($lastModifedInfo[0],$lastModifedInfo[1])."</span></td></tr>";
			$x .= "</table>";
			
			$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
			for($i=1; $i<=$NumOfMonth; $i++)
			{
				if($i%4 == 1)
					$x .= "<tr valign=top>";
				
				$x .= "<td style='width:25%'>${"calendarTable$i"}</td>";
				
				if($i%4 == 0)
					$x .= "</tr>";
			}
			
			$x .= "</table>";
		}
		
		return $x;
	}
	
	function getCyclePeriodSelection($ID, $Name, $SelectedPeriodID='', $Onchange='', $AcademicYearID='', $noFirst=1)
	{
		global $Lang;
		
		# select current PeriodID if there is no specific period selection
		if ($SelectedPeriodID == '')
		{
			# get current period info
			$CurrentCyclePeriodArr = $this->getCurrentCyclePeriod();
			$SelectedPeriodID = $CurrentCyclePeriodArr[0]['PeriodID'];
		}
		
		if ($AcademicYearID=='')
		{
			$AcademicYearArr = getCurrentAcademicYearAndYearTerm();
			$AcademicYearID = $AcademicYearArr['AcademicYearID'];
		}
		
		# get all periods info
		$CyclePeriodInfoArr = $this->returnPeriods_NEW('', 1, $AcademicYearID);
		$numOfCyclePeriod = count($CyclePeriodInfoArr);
		
		$selectionArr = array();
		for ($i=0; $i<$numOfCyclePeriod; $i++)
		{
			$thisPeriodID = $CyclePeriodInfoArr[$i]['PeriodID'];
			$thisPeriodStart = $CyclePeriodInfoArr[$i]['PeriodStart'];
			$thisPeriodEnd = $CyclePeriodInfoArr[$i]['PeriodEnd'];
			$thisDisplay = $thisPeriodStart.' '.$Lang['General']['To'].' '.$thisPeriodEnd;
			
			$selectionArr[$thisPeriodID] = $thisDisplay;
		}
		
		$onchange = '';
		if ($Onchange != "")
			$onchange = 'onchange="'.$Onchange.'"';
				
		$selectionTags = ' id="'.$ID.'" name="'.$Name.'" '.$onchange;
		$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['Timetable']['Select']['Cycle']);
		
		$cycleSelection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedPeriodID, $all=0, $noFirst, $firstTitle);
		
		return $cycleSelection;
	}
	
	function getCycleDaysSelection($ID, $Name, $SelectedCycleDays='', $Onchange='', $AcademicYearID='', $noFirst=1)
	{
		global $Lang;
		
		# select current cycle days if there is no specific cycle days selection
		if ($SelectedCycleDays == '')
		{
			# get current period info
			$CurrentCyclePeriodArr = $this->getCurrentCyclePeriod();
			$CurrentPeriodID = $CurrentCyclePeriodArr[0]['PeriodID'];
			$PeriodInfoArr = $this->returnPeriods_NEW($CurrentPeriodID);
			$SelectedCycleDays = $PeriodInfoArr[0]['PeriodDays'];
		}
		
		if ($AcademicYearID=='')
		{
			$AcademicYearArr = getCurrentAcademicYearAndYearTerm();
			$AcademicYearID = $AcademicYearArr['AcademicYearID'];
		}
		
		# get all periods info
		$CyclePeriodInfoArr = $this->returnPeriods_NEW('', 1, $AcademicYearID);
		$numOfCyclePeriod = count($CyclePeriodInfoArr);
		
		$selectionArr = array();
		for ($i=0; $i<$numOfCyclePeriod; $i++)
		{
			$thisPeriodType = $CyclePeriodInfoArr[$i]['PeriodType'];
			$thisPeriodDays = $CyclePeriodInfoArr[$i]['PeriodDays'];
			
			# show 6 days also if the cycle type is week
			$thisDays = ($thisPeriodType==0)? 6 : $thisPeriodDays;
			
			$selectionArr[$thisDays] = $thisDays;
		}
		
		$onchange = '';
		if ($Onchange != "")
			$onchange = 'onchange="'.$Onchange.'"';
				
		$selectionTags = ' id="'.$ID.'" name="'.$Name.'" '.$onchange;
		$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['Timetable']['Select']['CycleDays']);
		
		$cycleSelection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedCycleDays, $all=0, $noFirst, $firstTitle);
		
		return $cycleSelection;
	}
	
	function newHolidayEventForm($TargetDate)
	{
	    global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom,$intranet_session_language;
		include_once("libinterface.php");
		include_once("liblocation.php");
		include_once("liblocation_ui.php");
		include_once("libgrouping.php");
		$linterface = new interface_html();
		$llocation_ui = new liblocation_ui();
		$lgrouping = new libgrouping();
		
		$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
		if($isKIS) {
			include_once("libclassgroup.php");
			$lclassgroup = new Class_Group();
			$classGroupSelection = $lclassgroup->Get_Class_Group_Selection("ClassGroupID", '', '', 0, $Lang['SysMgr']['FormClassMapping']['AllClassGroup']);
		}
		
		$thisLocationID = '';
		$thisLocationLevelID = '';
		$thisBuildingID = '';
		
		### generate a event type selection drop down menu
		$typeSelection = "<select name='eventType' id='eventType' onChange='jsIsGroupEvent(this.value);'>";
		$typeSelection .= "<option value=''>".$Lang['SysMgr']['SchoolCalendar']['SelectType']."</option>";
		for($i=0; $i<sizeof($Lang['SysMgr']['SchoolCalendar']['EventType']); $i++){
				$typeSelection .= "<option value=$i>".$Lang['SysMgr']['SchoolCalendar']['EventType'][$i]."</option>";
		}
		$typeSelection .= "</select>";
		
		$buildingSelection = $llocation_ui->Get_Building_Selection(
									$thisBuildingID, 
									'SelectedBuildingID_InAddEditLayer', 
									"js_Change_Floor_Selection(	'$PATH_WRT_ROOT',
																'SelectedBuildingID_InAddEditLayer', 
																'OthersLocationTb',
																'FloorSelectionTr_InAddEditLayer',
																'FloorSelectionDiv_InAddEditLayer',
																'SelectedFloorID_InAddEditLayer',
																'',
																'RoomSelectionTr_InAddEditLayer',
																'RoomSelectionDiv_InAddEditLayer',
																'SelectedRoomID_InAddEditLayer',
																'',
																''
																 );
									",
									$noFirst = 0,
									$withOthersOption = 1
								);
		if ($thisLocationID != 0)
		{
			# Floor Filter
			$floorSelDisplay = '';
			$floorFilter = $llocation_ui->Get_Floor_Selection($thisBuildingID, $thisLocationLevelID, 'SelectedFloorID_InAddEditLayer', 
																"js_Change_Room_Selection(	'SelectedFloorID_InAddEditLayer', 
																	'RoomSelectionDiv_InAddEditLayer', 'SelectedRoomID_InAddEditLayer')", 
																'---',
																1);
			# Room Filter
			$roomSelDisplay = '';
			$roomFilter = $llocation_ui->Get_Room_Selection($thisLocationLevelID, $thisLocationID, "SelectedRoomID_InAddEditLayer", "", 1);
		}
		else
		{
			$selection_display1 = ' style="display:table-row" ';
			$floorSelDisplay = ' style="display:none" ';
			$roomSelDisplay = ' style="display:none" ';
		}
		
		# Others location textbox
		if ($thisLocationID == 0 && $EventVenue != '')
		{
		    $othersLocationTb = '<br/><input id="OthersLocationTb" name="OthersLocation" type="text" size="20" value="'.$EventVenue.'" />';
		    $othersLocationTb .= '<br/><input id="OthersLocationTb" name="OthersLocationEng" type="text" size="20" value="'.$EventVenue.'" />';
		    
		}
		else
		{
		    $othersLocationTb = '<br/><input id="OthersLocationTb" name="OthersLocation" type="text" size="20" style="display:none" />';
		    $othersLocationTb .= '<br/><input id="OthersLocationTb" name="OthersLocationEng" type="text" size="20" style="display:none" />';
		}
		$x .= "<form>";
		$x .= "<center>";
		$x .= "<div class='edit_pop_board' style='height:370px'>";
		$x .= "<div class='edit_pop_board_write' id='EditLayer' name='EditLayer' style='height:350px'>";
		$x .= '<div id="SpecialTimetableWarningDiv" style="display:none;">'.$linterface->Get_Warning_Message_Box('', $Lang['SysMgr']['CycleDay']['WarningArr']['NewHolidayOrEventWithSpecialTimetable'], 'id="SpecialTimetableWarningSpan"').'</div>'."\n";
		$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0'>";
		## Event Start Date 
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventStartDate'].":</td><td align='left'>$TargetDate</td><input type='hidden' name='EventStartDate' id='EventStartDate' value='$TargetDate'></tr>";
		## Event End Date
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventEndDate'].":</td><td align='left'><input type='text' name='EventEndDate' id='EventEndDate' value='$TargetDate' onChange='changeEventEndDate();'></td></tr>";
		## Event Type
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventType'].":</td><td align='left'>$typeSelection</td></tr>";
		if($isKIS) {
			$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['FormClassMapping']['ClassGroup'].":</td><td align='left'>$classGroupSelection</td></tr>";
		}
		## Event Group ##
		$targetAcademicYearAry = getAcademicYearAndYearTermByDate($TargetDate);
		$targetAcademicYearId = $targetAcademicYearAry['AcademicYearID'];
		$GroupInEvent = $lgrouping->returnSelectedEventGroups('selected',$EventID, "INTRANET_GROUPEVENT", "EventID", 0, $targetAcademicYearId);
		$GroupNotInEvent = $lgrouping->returnSelectedEventGroups('all',0, "INTRANET_GROUPEVENT", "EventID", 0, $targetAcademicYearId);
		
		$x .= "<tr><td width='30%' align='left' valign='top'><div class='targetEventGroup' style='display: none'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Group'].":</div></td>
				<td><div class='targetEventGroup' style='display: none'>".$linterface->GET_SELECTION_BOX($GroupInEvent,"name='targetGroupID' id='targetGroupID' size=5 multiple ","")." ".$linterface->GET_BTN($Lang['Btn']['Remove'],"Button","jQueryRemoveGroup('targetGroupID','avaliableGroupID');","RemoveGroup")."<BR></div><div class='targetEventGroup' style='display: none'>".getSelectByArray($GroupNotInEvent,"name='avaliableGroupID' id='avaliableGroupID' multiple size=5","",0,1)." ".$linterface->GET_BTN($Lang['Btn']['Add'],"Button","jQueryAddGroup('avaliableGroupID','targetGroupID');","AddGroup")."</div></td></tr>";
		
		if($sys_custom['eClassApp']['SFOC']){
// 		    include_once('SFOC/libmedallist.php');
// 		    $medalDB = new libmedallist();
// 		    $sportTypeResult = $medalDB->getAllSport();
// 		    //$sql = "SELECT SportID,SportChiName,SportEngName,IconPath
//             //        FROM SFOC_SPORTS_TYPE";
// 		    //$sportTypeResult = $this->returnArray($sql);
		    
// 		    $sportTypeSelection = "<select name='sportType' id='sportType'>";
// 		    $sportTypeSelection.= "<option value=''>".$Lang['SysMgr']['SchoolCalendar']['SelectType']."</option>";
// 		    for($i=0; $i<sizeof($sportTypeResult); $i++){
// 		        $SportName = $intranet_session_language=='b5'?$sportTypeResult[$i]['SportChiName']:$sportTypeResult[$i]['SportEngName'];
// 		        if($SportType== ($i+1))
// 		            $selected = " SELECTED ";
// 		            else
// 		                $selected = "";
// 		                $sportTypeSelection.= "<option value=".($i+1)." $selected>".$SportName."</option>";
// 		    }
// 		    $sportTypeSelection.= "</select>";
		    
		    
// 		    $sportTypeSelection = "<input id='Sport' name='Sport' type='text' class='textboxtext inputselect'  maxlength='128' size='40'  value='".$SportsName."'/>";
// 		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SportsType'].":</td><td align='left'>$sportTypeSelection</td></tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Association'].":</td><td align='left'><input type='input' name='event_association' id='event_association' value='' size='40'></td></tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AssociationEng'].":</td><td align='left'><input type='input' name='event_associationEng' id='event_associationEng' value='' size='40'></td></tr>";
		}
		
		## Event Name
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitle'].":</td><td align='left'><input type='input' name='event_title' id='event_title' value='' size='40'></td></tr>";
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitleEng'].":</td><td align='left'><input type='input' name='event_titleEng' id='event_titleEng' value='' size='40'></td></tr>";
		## Event Venue
		$x .= "<tr><td width='30%' align='left' valign='top'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenue'].":</td>";
		$x .= "<td align='left'>
					<input type='radio' name='location_type' id='location_type_school' value='1' onClick='js_LocationTypeSelection(this.value)' CHECKED><label for='location_type_school'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['Applicable']."</label> 
					<input type='radio' name='location_type' id='location_type_none' value='0' onClick='js_LocationTypeSelection(this.value)'><label for='location_type_none'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['NotApplicable']."</label> 
				</td></tr>";
		
		# Building Selection
		$x .= '<tr>'."\n";
			$x .= '<td align="left"></td>'."\n";
			$x .= '<td align="left">'."\n";
				$x .= '<div id="location_selection1" '.$selection_display1.'><span>'.$buildingSelection.'</span>&nbsp;&nbsp;';
				$x .= '<br><span>'.$othersLocationTb.'</span></div>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		# Floor Selection
		$x .= '<tr id="FloorSelectionTr_InAddEditLayer" '.$floorSelDisplay.'>'."\n";
			$x .= "<td></td>";
			$x .= '<td align="left">'."\n";
				$x .= '<div id="FloorSelectionDiv_InAddEditLayer">';
					$x .= $floorFilter;
				$x .= '</div>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		# Location Selection
		$x .= '<tr id="RoomSelectionTr_InAddEditLayer" '.$roomSelDisplay.'>'."\n";
			$x .= "<td></td>";
			$x .= '<td align="left">'."\n";
				$x .= '<div id="RoomSelectionDiv_InAddEditLayer">';
					$x .= $roomFilter;
				$x .= '</div>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		
		if($sys_custom['eClassApp']['SFOC']){
		    
		    $TimeSeletion .= $linterface->Get_Time_Selection_Box('HourStart', 'hour', 0);
		    $TimeSeletion .= ' : ';
		    $TimeSeletion .= $linterface->Get_Time_Selection_Box('MinStart', 'min', 0, $others_tab='', $interval=5);
		    $TimeSeletion .= ' to ';
		    $TimeSeletion .= $linterface->Get_Time_Selection_Box('HourEnd', 'hour', 0);
		    $TimeSeletion .= ' : ';
		    $TimeSeletion .= $linterface->Get_Time_Selection_Box('MinEnd', 'min', 0, $others_tab='', $interval=5);
		    
		    
		    $x .= "<tr>
             <td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Time'].":</td>
             <td> $TimeSeletion</td>
             </tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ToBeDeterminedEvent'].":</td>
            		    <td align='left'>
            		    <input type='radio' name='to_be_determine_event' id='to_be_determine_event_yes' value='1' ><label for='to_be_determine_event_yes'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes']."</label>
            		    <input type='radio' name='to_be_determine_event' id='to_be_determine_event_no' value='0' checked><label for='to_be_determine_event_no'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No']."</label>
					</td>
				</tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Email'].":</td><td align='left'><input type='input' name='event_email' id='event_email' value='' size='40'></td></tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ContactNo'].":</td><td align='left'><input type='input' name='event_contact' id='event_contact' value='' size='40'></td></tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Website'].":</td><td align='left'><input type='input' name='event_website' id='event_website' value='' size='40'></td></tr>";
		    
		}
		
		# Event Nature
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventNature'].":</td><td align='left'><input type='input' name='event_nature' id='event_nature' value='' size='40'></td></tr>";
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventNatureEng'].":</td><td align='left'><input type='input' name='event_natureEng' id='event_natureEng' value='' size='40'></td></tr>";
		
		# Event description
		$x .= "<tr><td width='30%' align='left' valign='top'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDescription'].":</td><td align='left'><textarea name='event_description' id='event_description' cols=40 rows=5></textarea></td></tr>";
		$x .= "<tr><td width='30%' align='left' valign='top'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDescriptionEng'].":</td><td align='left'><textarea name='event_descriptionEng' id='event_descriptionEng' cols=40 rows=5></textarea></td></tr>";
		
		
		# skip cycle day
		//$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipCycleDay'].":</td><td align='left'><input type='checkbox' name='skip_cycle_day' id='skip_cycle_day' value='0'></td></tr>";
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipCycleDay'].":</td>
					<td align='left'>
						<input type='radio' name='skip_cycle_day' id='skip_cycle_day_yes' value='1' CHECKED><label for='skip_cycle_day_yes'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes']."</label> 
						<input type='radio' name='skip_cycle_day' id='skip_cycle_day_no' value='0'><label for='skip_cycle_day_no'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No']."</label> 
					</td>
				</tr>";
		# skip sat
		$x .= "<tr><td width='30%' align='left'>". $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipSAT'] .":</td>
					<td align='left'>
						<input type='radio' name='skip_sat' id='skip_sat1' value='1' disabled><label for='skip_sat1'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes']."</label> 
						<input type='radio' name='skip_sat' id='skip_sat0' value='0' CHECKED disabled><label for='skip_sat0'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No']."</label> 
					</td>
				</tr>";
		# skip sun
		$x .= "<tr><td width='30%' align='left'>". $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipSUN'] .":</td>
					<td align='left'>
						<input type='radio' name='skip_sun' id='skip_sun1' value='1' disabled><label for='skip_sun1'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes']."</label> 
						<input type='radio' name='skip_sun' id='skip_sun0' value='0' CHECKED disabled><label for='skip_sun0'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No']."</label> 
					</td>
				</tr>";		
		$x .= "<input type='hidden' name='event_date' id='event_date' value='$TargetDate'>";
		$x .= "</table>";
		$x .= "</div>";
		$x .= "</div>";
		$x .= "</center>";
		$x .= "<br>";		
		$x .= "<center>";					
		$x .= "<div class='edit_bottom'>";
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
		$x .= "<tr><td><p class='spacer'></p></td></tr>";
		
		$x .= "<tr><td align='center'>
					<input id='NewHolidayEventSubmitBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Submit']."' onclick='updateNewHolidayEvent();' >
					<input id='NewHolidayEventCancelBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Cancel']."' onclick='window.top.tb_remove();' >
				</td></tr>";
		$x .= "</table>";
		$x .= "</div>"; 
		$x .= "</center>";
		$x .= "</form>";
		return $x;
	}
	
	function showHolidayEventForm($TargetDate)
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libinterface.php");
		$linterface = new interface_html();
		
		$x .= "<br>";
		$x .= "<center>";
		$x .= "<div class='edit_pop_board' style='height:300px;'>";
		$x .= $linterface->Get_Thickbox_Return_Message_Layer();
		$x .= "<h1>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDate']." : <span>".$TargetDate."</span></h1>";
		$x .= "<div class='edit_pop_board_write' style='height:300px;'>";
		$x .= "<table class='common_table_list'>";
		$x .= "<tr><th class='row_content' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitle']."</th>";
		$x .= "<th class='row_content' width='20%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventType']."</th>";
		$x .= "<th class='row_content' width='20%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipCycleDay']."</th>";
		$x .= "<th class='row_content' align='left'>&nbsp;</th></tr>";
		
		$arrEvents = $this->retriveEventsByDate($TargetDate);
		if(sizeof($arrEvents)>0){
			for($i=0; $i<sizeof($arrEvents); $i++){
				list($event_id, $event_title, $event_type, $skip_cycle_day) = $arrEvents[$i];
				switch($event_type){
					case 0: 
							$event_type_str = $Lang['SysMgr']['SchoolCalendar']['EventType'][0];
							$event_font_color = "#5A8B35";
							break;
					case 1: 
							$event_type_str = $Lang['SysMgr']['SchoolCalendar']['EventType'][1];
							$event_font_color = "#6583BC";
							break;
					case 2: 
							$event_type_str = $Lang['SysMgr']['SchoolCalendar']['EventType'][2];
							$event_font_color = "";
							break;
					case 3: 
							$event_type_str = $Lang['SysMgr']['SchoolCalendar']['EventType'][3];
							$event_font_color = "#FF0000";
							break;
					case 4: 
							$event_type_str = $Lang['SysMgr']['SchoolCalendar']['EventType'][4];
							$event_font_color = "#FF6633";
							break;
					default: 
							$event_type_str = $Lang['SysMgr']['SchoolCalendar']['EventType'][3];
							$event_font_color = "";
							break;
				}
				if($skip_cycle_day)
					$skip_cycle_day_str = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes'];
				else
					$skip_cycle_day_str = $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No'];
				
				$x .= "<tr><td align='left'><font color='$event_font_color'>$event_title</font></td><td align='left'>$event_type_str</td><td align='left'>$skip_cycle_day_str</td>";
				$x .= "<td><div class='table_row_tool row_content_tool'>";
				$arrResult = $this->getAllRelatedEvents($event_id);
				if(sizeof($arrResult)>1) {
					$x .= $linterface->Get_Thickbox_Link(450, 500, "edit_dim", $Lang['SysMgr']['SchoolCalendar']['Event']['Edit'], "editHolidayEventForm('$event_id',1,'Calendar'); return false;");
					$x .= $linterface->GET_LNK_DELETE("#", $Lang['Btn']['Delete'], "deleteHolidayEvent('$event_id','$TargetDate',0,'Calendar'); return false;");				
					//$x .= $linterface->Get_Thickbox_Link(450, 500, "delete_period_dim", $Lang['SysMgr']['SchoolCalendar']['ToolTip']['DeleteRelatedHolidaysEvents'], "deleteHolidayEvent('$event_id','$TargetDate',1,'Calendar'); return false;");
					$x .= $linterface->GET_ACTION_LNK("#", $Lang['SysMgr']['SchoolCalendar']['ToolTip']['DeleteRelatedHolidaysEvents'], "deleteHolidayEvent('$event_id','$TargetDate',1,'Calendar'); return false;","delete_period_dim");
				}else{
					$x .= $linterface->Get_Thickbox_Link(450, 500, "edit_dim", $Lang['SysMgr']['SchoolCalendar']['Event']['Edit'], "editHolidayEventForm('$event_id',0,'Calendar'); return false;");
					$x .= $linterface->GET_LNK_DELETE("#", $Lang['Btn']['Delete'], "deleteHolidayEvent('$event_id','$TargetDate',0,'Calendar'); return false;");				
				}
				$x .= "</div></td></tr>";
			}
		}else{
			$x .= "<tr><td colspan='4' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
		}
		$x .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><div class='table_row_tool row_content_tool'>";
		$x .= $linterface->Get_Thickbox_Link(450, 500, "add_dim", 
										$Lang['SysMgr']['SchoolCalendar']['NewHolidaysOrEvents'], "newHolidayEventForm('$TargetDate'); return false;");
		$x .= "</div></td></tr>";
		$x .= "</table>";
		$x .= "</div>";
		$x .= "</div>";
		
		return $x;
	}
	
	function editHolidayEventForm($EventID, $ShowEndDate, $CallFrom)
	{
	    global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom,$intranet_session_language;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		include_once("libcal.php");
		include_once("libcalevent.php");
		include_once("libcalevent2007a.php");
		include_once("libinterface.php");
		include_once("liblocation.php");
		include_once("liblocation_ui.php");
		include_once("libgrouping.php");
		
		$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
		if($isKIS) {
			include_once("libclassgroup.php");
			$lclassgroup = new Class_Group();
		}
		$linterface = new interface_html();
		$lcalevent = new libcalevent();
		$llocation_ui = new liblocation_ui();
		$lgrouping = new libgrouping();
		if($sys_custom['eClassApp']['SFOC']){
            $Fields = ",EventAssociation,EventStartTime,EventEndTime,Email,ContactNo,Website,EventStatus, TitleEng, EventAssociationEng, EventVenueEng";
		} else {
		    $Fields = ", TitleEng, EventVenueEng";
		}
		
// 		if($sys_custom['iCalendar']['ShowEventEnglishView']){
		    $Fields .=" , DescriptionEng, EventNatureEng";
// 		}
	 	$sql = "SELECT 
	 				EventDate, RecordType, Title, EventLocationID, EventVenue, EventNature, Description, isSkipCycle, RecordStatus, isSkipSAT, isSkipSUN, ClassGroupID $Fields
	 			FROM
	 				INTRANET_EVENT
	 			WHERE 
	 				EventID = $EventID";
	 	$result = $this->returnArray($sql);
	 	
	 	if(sizeof($result)>0)
	 	{
	 	    if($sys_custom['eClassApp']['SFOC']){
	 	        list($EventDate, $RecordType, $Title, $EventLocationID, $EventVenue, $EventNature, $Description, $isSkipCycle, $RecordStatus, $isSkipSAT, $isSkipSUN,$ClassGroupID,$EventAssociation,$EventStartTime,$EventEndTime,$Email,$ContactNo,$Website,$EventStatus, $TitleEng, $EventAssociationEng, $EventVenueEng, $DescriptionEng ,$EventNatureEng) = $result[0];
	 	    }else{
	 	        list($EventDate, $RecordType, $Title, $EventLocationID, $EventVenue, $EventNature, $Description, $isSkipCycle, $RecordStatus, $isSkipSAT, $isSkipSUN, $ClassGroupID, $TitleEng, $EventVenueEng, $DescriptionEng ,$EventNatureEng) = $result[0];
	 	    }
	 	    
		 	$TargetDate = substr($EventDate,0,10);
		 	
		 	if($EventLocationID != 0)
		 	{
		 		$thisLocationID = $EventLocationID;
		 		$objRoom = new Room($EventLocationID);
				$thisLocationLevelID = $objRoom->LocationLevelID;
			
				$objFloor = new Floor($thisLocationLevelID);
				$thisBuildingID = $objFloor->BuildingID;
			}else{
				$thisLocationID = '';
				$thisLocationLevelID = '';
				if($EventVenue == '')
					$thisBuildingID = '';
				else
					$thisBuildingID = -1;
			}
	 	}
		
		### generate a event type selection drop down menu
		$typeSelection = "<select name='eventType' id='eventType' onChange='jsIsGroupEvent(this.value);'>";
		$typeSelection .= "<option value=''>".$Lang['SysMgr']['SchoolCalendar']['SelectType']."</option>";
		for($i=0; $i<sizeof($Lang['SysMgr']['SchoolCalendar']['EventType']); $i++){
			if($RecordType == $i)
				$selected = " SELECTED ";
			else
				$selected = "";
			$typeSelection .= "<option value=$i $selected>".$Lang['SysMgr']['SchoolCalendar']['EventType'][$i]."</option>";
		}
		$typeSelection .= "</select>";
		
		$buildingSelection = $llocation_ui->Get_Building_Selection(
									$thisBuildingID, 
									'SelectedBuildingID_InAddEditLayer', 
									"js_Change_Floor_Selection(	'$PATH_WRT_ROOT',
																'SelectedBuildingID_InAddEditLayer', 
																'OthersLocationTb',
																'FloorSelectionTr_InAddEditLayer',
																'FloorSelectionDiv_InAddEditLayer',
																'SelectedFloorID_InAddEditLayer',
																'',
																'RoomSelectionTr_InAddEditLayer',
																'RoomSelectionDiv_InAddEditLayer',
																'SelectedRoomID_InAddEditLayer',
																'',
																''
																 );
									",
									$noFirst = 0,
									$withOthersOption = 1
								);
		
		if ($thisLocationID != 0)
		{
			# Floor Filter
			$floorSelDisplay = '';
			$floorFilter = $llocation_ui->Get_Floor_Selection($thisBuildingID, $thisLocationLevelID, 'SelectedFloorID_InAddEditLayer', 
																"js_Change_Room_Selection(	'SelectedFloorID_InAddEditLayer', 
																	'RoomSelectionDiv_InAddEditLayer', 'SelectedRoomID_InAddEditLayer')", 
																'---',
																1);
			# Room Filter
			$roomSelDisplay = '';
			$roomFilter = $llocation_ui->Get_Room_Selection($thisLocationLevelID, $thisLocationID, "SelectedRoomID_InAddEditLayer", "", 1);
		}
		else
		{
			$floorSelDisplay = ' style="display:none" ';
			$roomSelDisplay = ' style="display:none" ';
		}
		
		# Others location textbox
		if ($thisLocationID == 0 && $EventVenue != '')
		{
		    $othersLocationTb = '<br/><input id="OthersLocationTb" name="OthersLocation" type="text" size="20" value="'.$EventVenue.'" />';
		    $othersLocationTb .= '<br/><input id="OthersLocationTb" name="OthersLocationEng" type="text" size="20" value="'.$EventVenueEng.'" />';
			
		}
		else
		{
		    $othersLocationTb = '<input id="OthersLocationTb" name="OthersLocation" type="text" size="20" style="display:none" />';
		    $othersLocationTb .= '<input id="OthersLocationTb" name="OthersLocationEng" type="text" size="20" style="display:none" />';
		}
	
		//$x .= "<br>";
		$x .= "<center>";
		$x .= "<div class='edit_pop_board' style='height:370px'>";
		$x .= "<div class='edit_pop_board_write' id='EditLayer' name='EditLayer' style='height:350px'>";
		$x .= '<div id="SpecialTimetableWarningDiv" style="display:none;">'.$linterface->Get_Warning_Message_Box('', $Lang['SysMgr']['CycleDay']['WarningArr']['EditHolidayOrEventWithSpecialTimetable'], 'id="SpecialTimetableWarningSpan"').'</div>'."\n";
		$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0'>";
				
		if($ShowEndDate == 1)
		{
			$sql = "SELECT 
						c.EndDate AS EndDate 
					FROM 
						INTRANET_EVENT AS a INNER JOIN 
						(SELECT b.EventID, MAX(b.EventDate) AS EndDate FROM INTRANET_EVENT AS b WHERE b.RelatedTo IS NOT NULL OR b.RelatedTo != '' GROUP BY b.RelatedTo) AS c ON (a.EventID = c.EventID) 
					WHERE 
						a.RelatedTo = $EventID GROUP BY a.RelatedTo ORDER By a.EventDate, a.RecordType DESC";
			
			$result = $this->returnVector($sql);
			if(sizeof($result)>0)
				$EndDate = substr($result[0],0,10);
			# event start date
			$DateFields = "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventStartDate'].":</td><td align='left'>$TargetDate</td><input type='hidden' name='EventStartDate' id='EventStartDate' value='$TargetDate'></tr>";
			# event end date (show only when the event have other related events)
			$DateFields .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventEndDate'].":</td><td align='left'>$EndDate</td><input type='hidden' name='EventEndDate' id='EventEndDate' value='$EndDate'></tr>";
			
// 			$disable_skip = ($ShowEndDate == 1) ? "" : " disabled ";
			$NumOfDay = round( abs(strtotime($TargetDate)-strtotime($EndDate)) / 86400, 0 ) + 1;
			if($NumOfDay==2 && date("w",strtotime($TargetDate))==6)	$disable_skip = " disabled ";
			
		}else{
		    $DateFields = "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDate'].":</td><td align='left'>$TargetDate</td><input type='hidden' name='EventStartDate' id='EventStartDate' value='$TargetDate'></tr>";
		    $DateFields .= "<input type='hidden' name='EventEndDate' id='EventEndDate' value='$TargetDate'>";
			
			$disable_skip = " disabled ";
		}
		
		if($sys_custom['eClassApp']['SFOC']){
		    $ReleatedEventAry = $this->getAllRelatedEvents($EventID);
		    $DateRange = $this->Get_Event_Date_Range($ReleatedEventAry);		    
		    $EndDate = $DateRange[0]['EndDate'];

		    ## Event Start Date
		    $DateFields = "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventStartDate'].":</td><td align='left'><input  type='text' name='EventStartDate' id='EventStartDate' value='$TargetDate' onChange='changeEventEndDate();'></td></tr>";
		    ## Event End Date
		    $DateFields .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventEndDate'].":</td><td align='left'><input type='text' name='EventEndDate' id='EventEndDate' value='$EndDate' onChange='changeEventEndDate();'></td></tr>";
		    
		}
			
		$x .=$DateFields;
		
		# event type
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventType'].":</td><td align='left'>$typeSelection</td></tr>";
		
		if($isKIS) {
			$classGroupSelection = $lclassgroup->Get_Class_Group_Selection("ClassGroupID", $ClassGroupID, '', 0, $Lang['SysMgr']['FormClassMapping']['AllClassGroup']);
			$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['FormClassMapping']['ClassGroup'].":</td><td align='left'>$classGroupSelection</td></tr>";
		}
		
		## Event Group ##
		$display = "";
		if($RecordType != 2)
			$display = "none";
			
		$targetAcademicYearAry = getAcademicYearAndYearTermByDate($TargetDate);
		$targetAcademicYearId = $targetAcademicYearAry['AcademicYearID'];
			
		$GroupInEvent = $lgrouping->returnSelectedEventGroups('selected',$EventID, "INTRANET_GROUPEVENT", "EventID", 0, $targetAcademicYearId);
		$GroupNotInEvent = $lgrouping->returnSelectedEventGroups('non-selected',$EventID, "INTRANET_GROUPEVENT", "EventID", 0, $targetAcademicYearId);
		$x .= "<tr><td width='30%' align='left' valign='top'><div class='targetEventGroup' style='display: $display'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Group'].":</div></td>
				<td><div class='targetEventGroup' style='display: $display'>".$linterface->GET_SELECTION_BOX($GroupInEvent,"name='targetGroupID' id='targetGroupID' size=5 multiple ","")." ".$linterface->GET_BTN($Lang['Btn']['Remove'],"Button","jQueryRemoveGroup('targetGroupID','avaliableGroupID');","RemoveGroup")."<BR></div><div class='targetEventGroup' style='display: $display'>".getSelectByArray($GroupNotInEvent,"name='avaliableGroupID' id='avaliableGroupID' multiple size=5","",0,1)." ".$linterface->GET_BTN($Lang['Btn']['Add'],"Button","jQueryAddGroup('avaliableGroupID','targetGroupID');","AddGroup")."</div></td></tr>";
		
		if($sys_custom['eClassApp']['SFOC']){
// 		    include_once('SFOC/libmedallist.php');
// 		    $medalDB = new libmedallist();
// 		    $sportTypeResult = $medalDB->getAllSport();
		    //$sql = "SELECT SportID,SportChiName,SportEngName,IconPath
		    //        FROM SFOC_SPORTS_TYPE";
		    //$sportTypeResult = $this->returnArray($sql);
		   
// 		    $sportTypeSelection = "<select name='sportType' id='sportType'>";
// 		    $sportTypeSelection.= "<option value=''>".$Lang['SysMgr']['SchoolCalendar']['SelectType']."</option>";	    
// 		    for($i=0; $i<sizeof($sportTypeResult); $i++){
// 		        $SportName = $intranet_session_language=='b5'?$sportTypeResult[$i]['SportChiName']:$sportTypeResult[$i]['SportEngName'];
// 		        if($SportType== ($sportTypeResult[$i]['SportID']))
// 		            $selected = " SELECTED ";
// 		            else
// 		                $selected = "";
// 		                $sportTypeSelection.= "<option value=".($i+1)." $selected>".$SportName."</option>";
// 		    }
// 		    $sportTypeSelection.= "</select>";
// 	//	    $sportTypeSelection = "<input id='Sport' name='Sport' type='text' class='textboxtext inputselect'  maxlength='128' size='40'  value='".$SportsName."'/>";
		    
// 		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SportsType'].":</td>
// <td align='left'>$sportTypeSelection</td>
// </tr>";
		    //<input class='textboxtext inputselect' type='text' id='sportType' name='sportType' >
		    
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Association'].":</td><td align='left'><input type='input' name='event_association' id='event_association' value='".intranet_htmlspecialchars($EventAssociation)."' size='40'></td></tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AssociationEng'].":</td><td align='left'><input type='input' name='event_associationEng' id='event_associationEng' value='".intranet_htmlspecialchars($EventAssociationEng)."' size='40'></td></tr>";
		}

		# event title
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitle'].":</td><td align='left'><input type='input' name='event_title' id='event_title' value='".intranet_htmlspecialchars($Title)."' size='40'></td></tr>";
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitleEng'].":</td><td align='left'><input type='input' name='event_titleEng' id='event_titleEng' value='".intranet_htmlspecialchars($TitleEng)."' size='40'></td></tr>";

		
		if($EventLocationID == 0 && $EventVenue == "")
		{
			$none_check = "CHECKED";
			$selection_display1 = ' style="display:none" ';
			$floorSelDisplay = ' style="display:none" ';
			$roomSelDisplay = ' style="display:none" ';
		}
		if($EventLocationID != 0 && $EventVenue == "")
		{
			$school_check = "CHECKED";
			$selection_display1 = ' style="display:table-row" ';
			$floorSelDisplay = ' style="display:table-row" ';
			$roomSelDisplay = ' style="display:table-row" ';
		}
		if($EventLocationID == 0 && $EventVenue != "")
		{
			$school_check = "CHECKED";
			$selection_display1 = ' style="display:table-row" ';
		}
		$x .= "<tr><td width='30%' align='left' valign='top'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenue'].":</td>";
		$x .= "<td align='left'>
					<input type='radio' name='location_type' id='location_type_school' value='1' onClick='js_LocationTypeSelection(this.value)' $school_check><label for='location_type_school'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['Applicable']."</label> 
					<input type='radio' name='location_type' id='location_type_none' value='0' onClick='js_LocationTypeSelection(this.value)' $none_check><label for='location_type_none'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['NotApplicable']."</label> 
				</td></tr>";
		
		# Building Selection
		$x .= '<tr>'."\n";
			$x .= '<td align="left"></td>'."\n";
			$x .= '<td align="left">'."\n";
				$x .= '<div id="location_selection1" '.$selection_display1.'><span>'.$buildingSelection.'</span>&nbsp;&nbsp;';
				$x .= '<span>'.$othersLocationTb.'</span></div>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		# Floor Selection
		$x .= '<tr id="FloorSelectionTr_InAddEditLayer" '.$floorSelDisplay.'>'."\n";
			$x .= "<td></td>";
			$x .= '<td align="left">'."\n";
				$x .= '<div id="FloorSelectionDiv_InAddEditLayer">';
					$x .= $floorFilter;
				$x .= '</div>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		# Location Selection
		$x .= '<tr id="RoomSelectionTr_InAddEditLayer" '.$roomSelDisplay.'>'."\n";
			$x .= "<td></td>";
			$x .= '<td align="left">'."\n";
				$x .= '<div id="RoomSelectionDiv_InAddEditLayer">';
					$x .= $roomFilter;
				$x .= '</div>';
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		
		if($sys_custom['eClassApp']['SFOC']){
		    $StartHour = substr($EventStartTime, 0,2);
		    $StartMin = substr($EventStartTime, 3,2);
		    $EndHour = substr($EventEndTime, 0,2);
		    $EndMin = substr($EventEndTime, 3,2);

		    $TimeSeletion .= $linterface->Get_Time_Selection_Box('HourStart', 'hour', $StartHour);
		    $TimeSeletion .= ' : ';
		    $TimeSeletion .= $linterface->Get_Time_Selection_Box('MinStart', 'min', $StartMin, $others_tab='', $interval=5);
		    $TimeSeletion .= ' to ';
		    $TimeSeletion .= $linterface->Get_Time_Selection_Box('HourEnd', 'hour', $EndHour);
		    $TimeSeletion .= ' : ';
		    $TimeSeletion .= $linterface->Get_Time_Selection_Box('MinEnd', 'min', $EndMin, $others_tab='', $interval=5);
		    

		    $x .= "<tr>
             <td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Time'].":</td>
             <td> $TimeSeletion</td> 
            </tr>";
		    if($EventStatus== 1){
		        $checked_yes = " CHECKED ";
		        $checked_no = " ";
		    }else{
		        $checked_yes = "";
		        $checked_no = " CHECKED ";
		    }
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ToBeDeterminedEvent'].":</td>
            		    <td align='left'>
            		    <input type='radio' name='to_be_determine_event' id='to_be_determine_event_yes' value='1' $checked_yes><label for='to_be_determine_event_yes'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes']."</label>
            		    <input type='radio' name='to_be_determine_event' id='to_be_determine_event_no' value='0' $checked_no><label for='to_be_determine_event_no'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No']."</label>
					</td>
				</tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Email'].":</td><td align='left'><input type='input' name='event_email' id='event_email' value='".intranet_htmlspecialchars($Email)."' size='40'></td></tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ContactNo'].":</td><td align='left'><input type='input' name='event_contact' id='event_contact' value='".intranet_htmlspecialchars($ContactNo)."' size='40'></td></tr>";
		    $x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Website'].":</td><td align='left'><input type='input' name='event_website' id='event_website' value='".intranet_htmlspecialchars($Website)."' size='40'></td></tr>";
		    
		}
		# event nature
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventNature'].":</td><td align='left'><input type='input' name='event_nature' id='event_nature' value='$EventNature' size='40'></td></tr>";
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventNatureEng'].":</td><td align='left'><input type='input' name='event_natureEng' id='event_natureEng' value='$EventNatureEng' size='40'></td></tr>";
		
		# event description
		$x .= "<tr><td width='30%' align='left' valign='top'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDescription'].":</td><td align='left'><textarea name='event_description' id='event_description' cols=40 rows=5>$Description</textarea></td></tr>";
		$x .= "<tr><td width='30%' align='left' valign='top'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDescriptionEng'].":</td><td align='left'><textarea name='event_descriptionEng' id='event_descriptionEng' cols=40 rows=5>$DescriptionEng</textarea></td></tr>";
		
		if($isSkipCycle == 1){
			$checked_yes = " CHECKED ";
			$checked_no = " ";
		}else{
			$checked_yes = "";
			$checked_no = " CHECKED ";
		}
		//$x .= "<tr><td width='40%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipCycleDay'].":</td><td align='left'><input type='checkbox' name='skip_cycle_day' id='skip_cycle_day' value='0' $checked></td></tr>";
		$x .= "<tr><td width='30%' align='left'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipCycleDay'].":</td>
					<td align='left'>
						<input type='radio' name='skip_cycle_day' id='skip_cycle_day_yes' value='1' $checked_yes><label for='skip_cycle_day_yes'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes']."</label> 
						<input type='radio' name='skip_cycle_day' id='skip_cycle_day_no' value='0' $checked_no><label for='skip_cycle_day_no'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No']."</label> 
					</td>
				</tr>";
		
		# skip sat
		if($isSkipSAT == 1){
			$checked_yes = " CHECKED ";
			$checked_no = " ";
		}else{
			$checked_yes = "";
			$checked_no = " CHECKED ";
		}
		
		$x .= "<tr><td width='30%' align='left'>". $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipSAT'] .":</td>
					<td align='left'>
						<input type='radio' name='skip_sat' id='skip_sat1' value='1' $checked_yes $disable_skip><label for='skip_sat1'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes']."</label> 
						<input type='radio' name='skip_sat' id='skip_sat0' value='0' $checked_no $disable_skip><label for='skip_sat0'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No']."</label> 
					</td>
				</tr>";
		# skip sun
		if($isSkipSUN == 1){
			$checked_yes = " CHECKED ";
			$checked_no = " ";
		}else{
			$checked_yes = "";
			$checked_no = " CHECKED ";
		}
		$x .= "<tr><td width='30%' align='left'>". $Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipSUN'] .":</td>
					<td align='left'>
						<input type='radio' name='skip_sun' id='skip_sun1' value='1' $checked_yes $disable_skip><label for='skip_sun1'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes']."</label> 
						<input type='radio' name='skip_sun' id='skip_sun0' value='0' $checked_no $disable_skip><label for='skip_sun0'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No']."</label> 
					</td>
				</tr>";		
				
				
		# apply to all relative event 
		$arrResult = $this->getAllRelatedEvents($EventID);
		if(sizeof($arrResult)>1) {
			//$x .= "<tr><td width='40%' align='left' valign='top'>".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ApplyToRelatedEvents'].":</td><td align='left'><input type='checkbox' name='ApplyToRelatedEvents' id='ApplyToRelatedEvents' value=1 checked></td></tr>";
			$x .= "<tr><td width='30%' align='left' valign='top'></td><td align='left'><input type='hidden' name='ApplyToRelatedEvents' id='ApplyToRelatedEvents' value=1 ></td></tr>";
		}
		
		$x .= "<input type='hidden' name='event_date' id='event_date' value='$TargetDate'>";
		$x .= "<input type='hidden' name='event_id' id='event_id' value='$EventID'>";
		$x .= "</table>";
		$x .= "</div>";
		$x .= "</div>";
		$x .= "</center>";
		
		$x .= "<center>";
		$x .= "<div class='edit_bottom'>";
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
		$x .= "<tr><td><p class='spacer'></p></td></tr>";
		if($CallFrom == "Calendar")
		{
			$x .= "<tr><td align='center'>
						<input id='NewHolidayEventSubmitBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Submit']."' onclick='updateEditHolidayEvent(\"$CallFrom\");' >
						<input id='NewHolidayEventCancelBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Cancel']."' onclick='showHolidayEventForm(\"$TargetDate\");' >
					</td></tr>";
		}else if($CallFrom == "MonthlyEvent"){
			$x .= "<tr><td align='center'>
						<input id='NewHolidayEventSubmitBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Submit']."' onclick='updateEditHolidayEvent(\"$CallFrom\");' >
						<input id='NewHolidayEventCancelBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Cancel']."' onclick='window.top.tb_remove();' >
					</td></tr>";
		}
		$x .= "</table>";
		$x .= "</div>";
		$x .= "</center>";
		
		return $x;
	}
	
	function getAcademicYearTermInfo()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libdb.php");
		include_once("form_class_manage.php");
		include_once("libcal.php");
		include_once("libcalevent.php");
		include_once("libcalevent2007a.php");
		include_once("libinterface.php");
		
		global $Lang;
		
		$linterface = new interface_html();
		
		$x .= $this->initJavaScript();
		
		$x .= "<div id='main_body'>";
		$x .= '<div id="academic_table">';
		$x .= $this->loadAcademicYearTable();
		$x .= '</div>';
		$x .= '</div>';
		$x .= '<div class="FakeLayer"></div>'."\n";
		
		return $x;
	}

	function loadAcademicYearTable($targetSchoolYearID='')
	{
		include_once("libinterface.php");
		include_once("form_class_manage.php");
		
		global $Lang,$LAYOUT_SKIN,$image_path;

		$linterface = new interface_html();
		$lfcm = new form_class_manage();
		
		$arrLastPublishInfo = $this->getAcademaicYearSetttingLastUpdatedDate();
		
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
		$x .= "<tr>";
		$x .= "<td align='left'><span class='tabletextremark'>".Get_Last_Modified_Remark($arrLastPublishInfo[0],$arrLastPublishInfo[1])."</span></td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='3'>";
		//$x .= "<tr><td><div id='DIV_ShowImportantNoteLink'><a href='#' onClick='ShowImportantNote(\"ShowImportantNote\",\"DIV_ShowImportantNoteLink\")'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantProperSchoolYearOrTermSettings']['Title']."</a></div></td></tr>";
		$x .= "<tr><td><font color='ff0000'>". $Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantProperSchoolYearOrTermSettings_NewStr'] ."</font></td></tr>";
		$x .= "</table>";
		
		### Layer to display Item Booking Details
		$x .= '<div id="ImportantNoteLayer" class="selectbox_layer" style="width:500px; height:280px; overflow:auto;">'."\n";
			$x .= '<table cellspacing="0" cellpadding="3" border="0" width="95%">'."\n";
				$x .= '<tbody>'."\n";
					### Hide Layer Button
					$x .= '<tr>'."\n";
						$x .= '<td align="right" style="border-bottom: medium none;">'."\n";
							$x .= '<a href="javascript:js_Hide_Detail_Layer();"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					### Layer Content
					$x .= '<tr>'."\n";
						$x .= '<td align="left" style="border-bottom: medium none;">'."\n";
							$x .= '<div id="ImportantNoteLayerContentDiv"></div>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
		$x .= '</div>'."\n";
		

		$x .= '<table class="common_table_list" id="ContentTable">'."\n";
			
				## Standardize the display of IE and Firefox
				$x .= '<col align="left" style="width:20%" />'."\n";

				$x .= '<col align="left" style="width:20%" />'."\n";
				$x .= '<col align="left" style="width:20%" />'."\n";
				$x .= '<col align="left" style="width:20%" />'."\n";
				$x .= '<col align="left" style="width:10%" />'."\n";

				## Header
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearName'].'</th>'."\n";
					
					$x .= '<th class="sub_row_top">'.$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermName'].'</th>'."\n";
					$x .= '<th class="sub_row_top">'.$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermStartDate'].'</th>'."\n";
					$x .= '<th class="sub_row_top">'.$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermEndDate'].'</th>'."\n";
					$x .= '<th class="sub_row_top">&nbsp;</th>'."\n";
				$x .= '</tr>'."\n";
				
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>'."\n";
							$x .= '<span class="row_content">'.$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYear'].'</span>'."\n";
							# Academic Year Setting Button
							$x .= '<div class="table_row_tool row_content_tool">'."\n";
								$x .= $linterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "setting_row", 
											$Lang['SysMgr']['AcademicYear']['ToolTip']['AcademicYearAddEdit'], "js_Show_AcademicYear_Add_Edit_Layer(); return false;");
							$x .= '</div>'."\n";
						$x .= '</th>'."\n";
						
						$x .= '<th colspan="4" class="sub_row_top">'."\n";
							$x .= '<span class="row_content">'.$Lang['SysMgr']['AcademicYear']['FieldTitle']['Term'].'</span>'."\n";
						$x .= '</th>'."\n";
							
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				
				## Content
				# Get Academic Year Info
				//$cond = '';
				//if($targetSchoolYearID != '')
				//	$cond = ' WHERE AcademicYearID = '.$targetSchoolYearID;
				//$sql = 'SELECT AcademicYearID, YearNameEN, YearNameB5 FROM ACADEMIC_YEAR '.$cond .' ORDER BY Sequence';
				//$arrResult = $this->returnArray($sql,3);
				
				$arrResult = $lfcm->Get_All_Academic_Year();
				$numOfAcademiceYear = count($arrResult);
				
				if ($numOfAcademiceYear == 0)
				{
					$x .= '<tr><td colspan="5" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
				}
				else
				{
					# Loop Term
					for ($i=0; $i<$numOfAcademiceYear; $i++)
					{
						//list($academic_year_id, $academic_year_name_eng, $academic_year_name_b5) = $arrResult[$i];
						$academic_year_id = $arrResult[$i]['AcademicYearID'];
						$academic_year_name_eng = $arrResult[$i]['YearNameEN'];
						$academic_year_name_b5 = $arrResult[$i]['YearNameB5'];
						$isCurrentSchoolYear = $arrResult[$i]['CurrentSchoolYear'];
												
						$academic_year_id = $academic_year_id;
						$academic_year_name = Get_Lang_Selection($academic_year_name_b5,$academic_year_name_eng);
						
						# Get Academic Year Term Info
						$sql = "SELECT YearTermID, TermID, YearTermNameEN, YearTermNameB5, TermStart, TermEnd FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = $academic_year_id ORDER BY TermStart";
						$arr_term = $this->returnArray($sql,6);
						$numOfTerm = count($arr_term);
						
						$RowSpan = ($numOfTerm > 0)? 'rowspan="'.($numOfTerm + 2).'"':'';
						$FirstDisplay = ($numOfTerm > 0)? 'display:none;':'';
						
						# Display Term info
						$x .= '<tbody>';
						
						$x .= '<tr class="nodrag nodrop">'."\n";
							if($isCurrentSchoolYear){
								$x .= '<td '.$RowSpan.'>'.intranet_htmlspecialchars($academic_year_name).'<br><span class="tabletextremark">['.$Lang['SysMgr']['AcademicYear']['FieldTitle']['CurrentSchoolYear'].']</span></td>'."\n";
							}else{
								$x .= '<td '.$RowSpan.'>'.intranet_htmlspecialchars($academic_year_name).'</td>'."\n";
							}
							$x .= '<td style="'.$FirstDisplay.'">&nbsp;</td>'."\n";
							$x .= '<td style="'.$FirstDisplay.'">&nbsp;</td>'."\n";
							$x .= '<td style="'.$FirstDisplay.'">&nbsp;</td>'."\n";
							# Add Term Button (only show if no Term Record)
							$x .= '<td width="10%" style="'.$FirstDisplay.'">'."\n";
								$x .= '<div class="table_row_tool row_content_tool">'."\n";
									$x .= $linterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
													$Lang['SysMgr']['AcademicYear']['ToolTip']['AddTerm'], "js_Show_Term_Add_Edit_Layer('$academic_year_id',0); return false;");
								$x .= '</div>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
												
						# Loop Term
						for ($j=0; $j<$numOfTerm; $j++)
						{
							list($year_term_id, $term_id, $term_name_eng, $term_name_b5, $start_date, $end_date) = $arr_term[$j];
							$term_id = $term_id;
							$term_name = Get_Lang_Selection($term_name_b5,$term_name_eng);
							$start_date = substr($start_date,0,10);
							$end_date = substr($end_date,0,10);
							
							$x .= '<tr class="sub_row" id="TermRow_'.$term_id.'">'."\n";
								$x .= '<td>'.intranet_htmlspecialchars($term_name).'</td>'."\n";
								$x .= '<td>'.$start_date.'</td>'."\n";
								$x .= '<td>'.$end_date.'</td>'."\n";
								$x .= '<td class="Dragable">'."\n";
									$x .= '<div class="table_row_tool">'."\n";
										# Move
										//$x .= $linterface->GET_LNK_MOVE("#", $moveToolTips);
//										if(!$this->checkTermIsInUse($year_term_id))
//										{
//											# Edit Term Button
//											$x .= $linterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit_dim", 
//														$Lang['SysMgr']['AcademicYear']['ToolTip']['EditTerm'], "js_Show_Term_Add_Edit_Layer('$academic_year_id', '$year_term_id'); return false;");
//											# Delete Term Button
//											$x .= $linterface->GET_LNK_DELETE("#", $Lang['SysMgr']['AcademicYear']['ToolTip']['DeleteTerm'], "js_Delete_Term('$year_term_id'); return false;");
//										}else{
//											$x .= "<img src='".$image_path."/".$LAYOUT_SKIN."/icalendar/icon_lock_own.gif' align='absmiddle' onClick='alert(\"".$Lang['SysMgr']['AcademicYear']['Warning']['TermCannotEditOrDelete']."\");'/>";
//										}
										# IP 2.5.7.1.1 Edit Term New Logic
										if($this->IsFutureTerm($year_term_id)){
											// future term
											# Edit Term Button
											$x .= $linterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit_dim", 
											$Lang['SysMgr']['AcademicYear']['ToolTip']['EditTerm'], "js_Show_Term_Add_Edit_Layer('$academic_year_id', '$year_term_id','','','','','future'); return false;");
											if(!$this->checkTermIsInUse($year_term_id,true)){
												# Delete Term Button
												$x .= $linterface->GET_LNK_DELETE("#", $Lang['SysMgr']['AcademicYear']['ToolTip']['DeleteTerm'], "js_Delete_Term('$year_term_id'); return false;");
											}
										}else{
											// current term
											if( $year_term_id == getCurrentSemesterID() ){
												# Edit Term Button
												$x .= $linterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit_dim", 
												$Lang['SysMgr']['AcademicYear']['ToolTip']['EditTerm'], "js_Show_Term_Add_Edit_Layer('$academic_year_id', '$year_term_id','','','','','current'); return false;");
											}
											else{
												$x .= "<img src='".$image_path."/".$LAYOUT_SKIN."/icalendar/icon_lock_own.gif' align='absmiddle' onClick='alert(\"".$Lang['SysMgr']['AcademicYear']['Warning']['TermCannotEditOrDelete']."\");'/>";
											}
										}
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						}
						
						if ($numOfTerm > 0)
						{
							$x .= '<tr class="nodrag nodrop">'."\n";
								$x .= '<td>&nbsp;</td>'."\n";
								$x .= '<td>&nbsp;</td>'."\n";
								$x .= '<td>&nbsp;</td>'."\n";
								# Add Term Button 
								$x .= '<td>'."\n";
									$x .= '<div class="table_row_tool row_content_tool">'."\n";
										$x .= $linterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
												$Lang['SysMgr']['AcademicYear']['ToolTip']['AddTerm'], "js_Show_Term_Add_Edit_Layer('$academic_year_id', 0); return false;");
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						}
							
						$x .= '</tbody>'."\n";
						
					} // End loop Academic Year
				}
				
				// [2015-0813-1031-48207] added button for adding new academic year
//				$x .= '<tbody>';
//					$x .= '<tr>'."\n";
//						$x .= '<td>'."\n";
//							$x .= '<div class="table_row_tool row_content_tool">'."\n";
//								$x .= $linterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
//											$Lang['SysMgr']['AcademicYear']['ToolTip']['NewAcademicYear'], "js_Show_AcademicYear_Add_Edit_Layer(); return false;");
//							$x .= '</div>'."\n";
//						$x .= '</td>'."\n";
//						$x .= '<td>&nbsp;</td>'."\n";
//						$x .= '<td>&nbsp;</td>'."\n";
//						$x .= '<td>&nbsp;</td>'."\n";
//						$x .= '<td>&nbsp;</td>'."\n";
//					$x .= '</tr>'."\n";
//				$x .= '</tbody>'."\n";
				
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function thickbox_AcademicYearTable()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libinterface.php");
		$linterface = new interface_html();
		
		$x .= "<center>";
		$x .= "<div class='edit_pop_board' style='height:300px;'>";
		$x .= $linterface->Get_Thickbox_Return_Message_Layer();
		$x .= "<div class='edit_pop_board_write' style='height:300px;'>";
		$x .= "<div class='AcademicYearTableLayer'>";
		$x .= "<table id='AcademicYearTable' class='common_table_list'>";
		$x .= "<thead>";
		$x .= "<tr><th class='row_content' width='40%' align='left'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearNameChi']."</th>";
		$x .= "<th class='row_content' width='40%' align='left'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearNameEng']."</th>";
		$x .= "<th class='row_content' align='left'>&nbsp;</th></tr>";
		$x .= "</thead>";
		
		$sql = "select AcademicYearID, YearNameEN, YearNameB5 from ACADEMIC_YEAR ORDER BY Sequence";
		$result = $this->returnArray($sql,2);
		
		$x .= '<tbody>';
		if(sizeof($result)>0){
			for($i=0; $i<sizeof($result); $i++){
				list($AcademicYearID, $AcademicYear_EngName, $AcademicYear_ChiName) = $result[$i];
				
				$x .= "<tr id='AcademicYearRow_".$AcademicYearID."'><td align='left'>";
				$x .= $linterface->Get_Thickbox_Edit_Div("AcademicYearTitleCh_".$AcademicYearID, "AcademicYearTitleCh_".$AcademicYearID, "jEditTitleCh", $AcademicYear_ChiName);
				$x .= $linterface->Get_Thickbox_Warning_Msg_Div("ChTitleWarningDiv_".$AcademicYearID);
				$x .= "</td>";
				$x .= "<td align='left'>";
				$x .= $linterface->Get_Thickbox_Edit_Div("AcademicYearTitleEn_".$AcademicYearID, "AcademicYearTitleEn_".$AcademicYearID, "jEditTitleEn", $AcademicYear_EngName);
				$x .= $linterface->Get_Thickbox_Warning_Msg_Div("EnTitleWarningDiv_".$AcademicYearID);
				$x .= "</td>";
				
				$x .= "<td class='Dragable' align='left'>";
				# Move
				$x .= $linterface->GET_LNK_MOVE("#", $Lang['SysMgr']['AcademicYear']['ToolTip']['MoveToArrangeDisplayOrder']);
				# Delete
				$sql = "SELECT CASE WHEN COUNT(YearTermID)>0 THEN 0 ELSE 1 END AS tmp_result FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = $AcademicYearID";
				$tmp_arr = $this->returnVector($sql);
				$showDeleteIcon = $tmp_arr[0];
				if ($showDeleteIcon)
				{
					$x .= $linterface->GET_LNK_DELETE("#", $Lang['SysMgr']['AcademicYear']['ToolTip']['DeleteAcademicYear'], "js_Delete_AcademicYear('$AcademicYearID'); return false;");
				}
				$x .= '</td>';
				$x .= "<input type='hidden' name='AcademicYearID_".$AcademicYearID."' id='AcademicYearID' value='$AcademicYearID'>";
				$x .= "</tr>";
			}
		}else{
			$x .= "<tr><td colspan='3' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
		}
		
		$x .= "<tr id='AddAcademicYearRow' class='nodrop nodrag'>";
		$x .= "<td>&nbsp;</td>";
		$x .= "<td>&nbsp;</td>";
		$x .= "<td>";
		$x .= '<div class="table_row_tool row_content_tool">'."\n";
		$x .= '<a href="#" onclick="js_Add_AcademicYear_Row(); return false;" class="add_dim" title="'.$Lang['SysMgr']['AcademicYear']['ToolTip']['NewAcademicYear'].'"></a>';
		$x .= '</div>'."\n";
		$x .= "</td></tr>";
		$x .= '</tbody>';
		$x .= "</table>";
		$x .= "</div>";
		$x .= "</div>";
		return $x;
	}
	
	function showTermAddEditLayer($AcademicYearID,$YearTermID=0,$TermTitleCh='',$TermTitleEn='',$TermStartDate='',$TermEndDate='',$RecordType='')
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libinterface.php");
		$linterface = new interface_html();
				
		$x .= "<center>";
		$x .= "<div class='edit_pop_board' style='height:300px;'>";
		$x .= $linterface->Get_Thickbox_Return_Message_Layer();
		$x .= "<div class='TermTableLayer'>";
		$x .= $this->loadTermTable($AcademicYearID,$YearTermID,$TermTitleCh,$TermTitleEn,$TermStartDate,$TermEndDate,$RecordType);
		$x .= "</div>";
		
		return $x;
	}
	
	function loadTermTable($AcademicYearID,$YearTermID=0,$YearTermNameB5='',$YearTermNameEN='',$TermStart='',$TermEnd='',$RecordType='')
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libinterface.php");
		$linterface = new interface_html();
		
		$edit = 0;
		
		if($_SESSION['intranet_session_language'] == "en") {
			$sql = "SELECT YearNameEN FROM ACADEMIC_YEAR WHERE AcademicYearID = '".$AcademicYearID."'";
		} else {
			$sql = "SELECT YearNameB5 FROM ACADEMIC_YEAR WHERE AcademicYearID = '".$AcademicYearID."'";
		}
		$arrAcademicYearName = $this->returnVector($sql);
				
		if($YearTermID != 0){
			if($YearTermNameB5=='' && $YearTermNameEN=='' && $TermStart=='' && $TermEnd==''){
				$edit = 1;
				$sql = "SELECT YearTermNameEN, YearTermNameB5, TermStart, TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID = '".$YearTermID."'";
				$arr_result = $this->returnArray($sql,4);
				if(sizeof($arr_result)>0){
					list($YearTermNameEN, $YearTermNameB5, $TermStart, $TermEnd) = $arr_result[0];
					$TermStart = substr($TermStart,0,10);
					$TermEnd = substr($TermEnd,0,10);
				}
			}
		}
		
		$x .= "<div class='edit_pop_board_write' style='height:280px;'>";
		$x .= "<table id='TermTable' class='form_table' width='100%'>";
		## Show the Academic Year Name
		$x .= "<tr>";
		$x .= "<td width='25%'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYear']."</td><td width='1%'>:</td>";
		$x .= "<td width='30%'>".$arrAcademicYearName[0]."</td>";
		$x .= "</tr>";	
		if($RecordType == 'current'){
			// not allow to edit Title Chinese, Title English, Start Date for edit current term
			# Term Title Chinese
			$x .= "<tr><td width='25%'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermNameChi']."</td><td width='1%'>:</td>";
			$x .= "<td width='30%'>$YearTermNameB5<input type='hidden' name='TermTitleCh' id='TermTitleCh' value='".intranet_htmlspecialchars($YearTermNameB5)."'></td><td width='45%'><div id='TermTitleCh_WarningMsgLayer' style='display:none'></div></td></tr>";
			# Term Title English
			$x .= "<tr><td width='25%'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermNameEng']."</td><td width='1%'>:</td>";
			$x .= "<td width='30%'>$YearTermNameEN<input type='hidden' name='TermTitleEn' id='TermTitleEn' value='".intranet_htmlspecialchars($YearTermNameEN)."'></td><td width='45%'><div id='TermTitleEn_WarningMsgLayer' style='display:none'></div></td></tr>";
			# Term Start Date
			$x .= "<tr><td width='25%'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermStartDate']."</td><td width='1%'>:</td>";
			$x .= "<td width='35%'>$TermStart<input type='hidden' name='TermStartDate' id='TermStartDate' value='$TermStart'></td><td width='45%'><div id='TermStartDate_WarningMsgLayer' style='display:none'></div></td></tr>";			
		}
		else{
			# Term Title Chinese
			$x .= "<tr><td width='25%'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermNameChi']."</td><td width='1%'>:</td>";
			$x .= "<td width='30%'><input type='text' name='TermTitleCh' id='TermTitleCh' value='".intranet_htmlspecialchars($YearTermNameB5)."' onKeyUp='jsClearWarningMsg(\"TermTitleCh_WarningMsgLayer\");'></td><td width='45%'><div id='TermTitleCh_WarningMsgLayer' style='display:none'></div></td></tr>";
			# Term Title English
			$x .= "<tr><td width='25%'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermNameEng']."</td><td width='1%'>:</td>";
			$x .= "<td width='30%'><input type='text' name='TermTitleEn' id='TermTitleEn' value='".intranet_htmlspecialchars($YearTermNameEN)."' onKeyUp='jsClearWarningMsg(\"TermTitleEn_WarningMsgLayer\");'></td><td width='45%'><div id='TermTitleEn_WarningMsgLayer' style='display:none'></div></td></tr>";
			# Term Start Date
			$x .= "<tr><td width='25%'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermStartDate']."</td><td width='1%'>:</td>";
			$x .= "<td width='35%'><input type='text' name='TermStartDate' id='TermStartDate' value='$TermStart' onChange='jsClearWarningMsg(\"TermStartDate_WarningMsgLayer\");' onKeyUp='jsClearWarningMsg(\"TermStartDate_WarningMsgLayer\");'></td><td width='45%'><div id='TermStartDate_WarningMsgLayer' style='display:none'></div></td></tr>";			
		}
		# Term End Date
		$x .= "<tr><td width='25%'>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermEndDate']."</td><td width='1%'>:</td>";
		$x .= "<td width='35%'><input type='text' name='TermEndDate' id='TermEndDate' value='$TermEnd' onChange='jsClearWarningMsg(\"TermEndDate_WarningMsgLayer\");' onKeyUp='jsClearWarningMsg(\"TermEndDate_WarningMsgLayer\");'></td><td width='45%'><div id='TermEndDate_WarningMsgLayer' style='display:none'></div></td></tr>";
		$x .= "</table>";
		$x .= "<br>";
		$x .= "<span class='tabletextremark'>";
		$x .= "<table border='0' width='95%' align='center'>";
		$x .= "<tr><td>".$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantNote_TermSetting']."</td></tr>";
		$x .= "</table>";
		$x .= "</span>";
		$x .= "</div>";
		# submit button & cancel button
		$x .= "<div class='edit_bottom'>";
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>";
		$x .= "<tr><td><p class='spacer'></p></td></tr>";
		$x .= "<tr><td align='center'>
						<input id='NewTermSubmitBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Submit']."' onclick='insertUpdateTerm($edit,$AcademicYearID,$YearTermID,\"$RecordType\");' >
						<input id='NewTermCancelBtn' type='button' class='formbutton' onmouseover='this.className=\"formbuttonon\"' onmouseout='this.className=\"formbutton\"' value='".$Lang['Btn']['Cancel']."' onclick='window.top.tb_remove();' >
					</td></tr>";
		
		$x .= "</table>";
		$x .= "</div>";
		
		return $x;
	}
	
	function loadTimetableSelection($SchoolYearID, $PeriodStart, $PeriodType, $NumOfCycleDay)
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libinterface.php");
		$linterface = new interface_html();
		
		if($PeriodType == 0){
			$PeriodType = "weekday";
		}else{
			$PeriodType = "cycle";
		}
		
		$arrResult = getAvailableTimetableTemplateWithDaysChecking($SchoolYearID, '', $PeriodStart, $PeriodType, $NumOfCycleDay);
		
		$x = getSelectByArray($arrResult, "name='TimetableTemplate' id='TimetableTemplate'",'',0,0,$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SelectTimeTable']);
		
		return $x;
	}
	
	function getImportHolidaysEventsUI($AcademicYearID)
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom,$image_path;
		include_once("libinterface.php");
		$linterface = new interface_html();
		
		$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
		
		# step information
		$STEPS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['SelectImportFile'], 1);
		$STEPS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ConfirmImportRecord'], 0);
		$STEPS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ImportedResult'], 0);
		
		$WarningMsgBox = $linterface->Get_Warning_Message_Box($title, $Lang['SysMgr']['CycleDay']['WarningArr']['ImportHolidayOrEventWithSpecialTimetable'], $others="");
		
		// SFOC condition
		
		if($sys_custom['eClassApp']['SFOC']){
		
		  if($isKIS) {
			 $csvFile = "<a href='". GET_CSV("sample_import_school_event_holiday_SFOC2.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
		  }else{
			 $csvFile = "<a href='". GET_CSV("sample_import_school_event_holiday_SFOC.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
		  }
		
		  /*
		  $csv_format = "";
		  for($i=1; $i<=sizeof($Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']); $i++)
		  {
			 if($i < 12 || ($i == 12 && $isKIS)){
				    $csv_format .= $delim.$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][$i];
			 }
			 if($i == 5)
			 {
				    $csv_format .= " <a href='javascript:checkSiteCode();' class=\"tablelink\">".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckSiteCode']."</a><div id='SiteCode' style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>";
			 }
			 if($i == 9)
			 {
				    $csv_format .= " <a href='javascript:checkGroupID();' class=\"tablelink\">".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckGroupCode']."</a><div id='GroupID' style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>";
			 }
			 if($i == 12) 
			 {
				    if($isKIS){
					   $csv_format .= " <a href='javascript:checkClassGroupCode();' class=\"tablelink\">".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckClassGroupCode']."</a><div id='ClassGroupCode' style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>";
				    }
			 }
			 $delim = "<br>";
		  }*/
		
		  ### data column
		  $DataColumnTitleArr = "";
		  $DataColumnSize = 21 + ($isKIS?1:0); 
			 for($n = 0;$n<$DataColumnSize;$n++){
				    $DataColumnTitleArr[$n] =$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['SFOC'][$n+1];
			 }
			
		  $DataColumnPropertyArr = array(3,3,3,3,3,3,3,3,3,3,3,3,3,3,3);
		
		  ### remark 
// 		  $RemarksArr[3] = '<a id="sportLink" class="tablelink" href="javascript:checkSports();">['.$Lang['SysMgr']['SchoolCalendar']['ImportHolidaysEvent']['Remarks']['GroupID'].']</a>';
// 		  $RemarksArr[7] = '<a id="locationID" class="tablelink" href="javascript:checkSiteCode();">['.$Lang['SysMgr']['SchoolCalendar']['ImportHolidaysEvent']['Remarks']['SiteCode'].']</a>';
		  $RemarksArr[7] = '<a id="locationID" class="tablelink" href="javascript:checkSiteCode(\'locationID\');">['.$Lang['SysMgr']['SchoolCalendar']['ImportHolidaysEvent']['Remarks']['SiteCode'].']</a>';		  
		  $RemarksArr[14] = '<a id="groupIDType" class="tablelink" href="javascript:Load_Reference(\'groupIDType\');">['.$Lang['SysMgr']['SchoolCalendar']['ImportHolidaysEvent']['Remarks']['GroupID'].']</a>';

		} else {
		    if($isKIS) {
		        $csvFile = "<a href='". GET_CSV("sample_import_school_event_holiday2.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
		    }else{
		        $csvFile = "<a href='". GET_CSV("sample_import_school_event_holiday.csv") ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";
		    }
		    /*
		    $csv_format = "";
		    for($i=1; $i<=sizeof($Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']); $i++)
		    {
		        if($i < 12 || ($i == 12 && $isKIS)){
		            $csv_format .= $delim.$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][$i];
		        }
		        if($i == 5)
		        {
		            $csv_format .= " <a href='javascript:checkSiteCode();' class=\"tablelink\">".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckSiteCode']."</a><div id='SiteCode' style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>";
		        }
		        if($i == 9)
		        {
		            $csv_format .= " <a href='javascript:checkGroupID();' class=\"tablelink\">".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckGroupCode']."</a><div id='GroupID' style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>";
		        }
		        if($i == 12)
		        {
		            if($isKIS){
		                $csv_format .= " <a href='javascript:checkClassGroupCode();' class=\"tablelink\">".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckClassGroupCode']."</a><div id='ClassGroupCode' style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>";
		            }
		        }
		        $delim = "<br>";
		    }*/
		    
		    ### data column
		    $DataColumnTitleArr = "";
		    $DataColumnSize = 15 + ($isKIS?1:0);
		    for($n = 0;$n<$DataColumnSize;$n++){
		        $DataColumnTitleArr[$n] =$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn']['Normal'][$n+1];
		    }
		    
		    $DataColumnPropertyArr = array(3,3,3,3,3,3,3,3,3,3,3,3,3,3,3);
		    
		    ### remark
		    $RemarksArr[5] = '<a id="locationID" class="tablelink" href="javascript:checkSiteCode(\'locationID\');">['.$Lang['SysMgr']['SchoolCalendar']['ImportHolidaysEvent']['Remarks']['SiteCode'].']</a>';
		    $RemarksArr[12] = '<a id="groupIDType" class="tablelink" href="javascript:Load_Reference(\'groupIDType\');">['.$Lang['SysMgr']['SchoolCalendar']['ImportHolidaysEvent']['Remarks']['GroupID'].']</a>';
		}
		$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $RemarksArr);
		$x .= '<div id="SportSuggest" class="selectbox_layer" style="width:400px"></div>';
		$x .= '<div id="SiteCode" class="selectbox_layer" style="width:470px"></div>';
		$x .= '<div id="remarkDiv_type" class="selectbox_layer" style="width:400px; z-index:1000"></div>';
		$x .= "<br>\n";
		$x .= "<br>\n";
		$x .= "<form name='form1' method='post' action='import_holidays_events_confirm.php' enctype='multipart/form-data'>\n";
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
		$x .= "<tr><td>\n";
		$x .= "<table width='100%' border='0' cellpadding='3' cellspacing='0'>\n";
		$x .= "<tr><td colspan='2'>".$linterface->GET_STEPS($STEPS_OBJ)."</td></tr>\n";
		$x .= "<tr><td colspan='2' align='center'>".$WarningMsgBox."</td></tr>\n";
		$x .= "<tr><td colspan='2'>\n";
		$x .= "	<table width='90%' border='0' cellpadding='5' cellspacing='0' align='center'>\n<td>\n
       	<br />\n
		<table class='form_table_v30' align='center' width='100%' border='0' cellpadding='5' cellspacing='0''>\n
					<tr>\n
						<td class='field_title' align='left'>".$Lang['General']['SourceFile']."<span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span></td>\n
						<td class='tabletext'><input class='file' type='file' name='csvfile' id='csvfile'></td>\n
					</tr>\n
					<tr>\n
						<td class='field_title' align='left'>".$Lang['General']['CSVSample']."</td>\n
						<td class='tabletext'>$csvFile</td>\n
					</tr>\n
					<tr>\n
						<td class='field_title' align='left'>".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['DataColumn']."</td>\n
						<td class='tabletext'>$DataColumn</td>\n
					</tr>\n
					
				</td>\n
				</tr>\n
        </table> \n<tr>\n
						<td class='tabletextremark' colspan='2'>".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['RequiredField']."</td>\n
					</tr>\n
					<tr>\n
						<td class='tabletextremark' colspan='2'>".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['VenueFormatExplanation']."</td>\n
					</tr>\n
					<tr>\n
						<td class='tabletextremark' colspan='2'>".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['GroupIDFormatExplanation']."</td>\n
					</tr>\n
				</table>\n";
		$x .= "</td></tr>\n";
		$x .= "	<tr>\n
					<td colspan='2'>\n
						<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>\n
						<tr>\n
							<td colspan='3' class='dotline'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='1' /></td>\n
						</tr>\n
						</table>\n
					</td>\n
				</tr>\n";
		$x .= "<tr>
					<td align='center' colspan='2'>".
					$linterface->GET_ACTION_BTN($Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Submit'], "submit")." ".$linterface->GET_ACTION_BTN($Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Cancel'], "button", "window.location='index.php'")
					."</td>
				</tr>";
		$x .= "</table>\n";
		
		$x .= "</td></tr>\n";
		$x .= "</table>\n";
		$x .= "<input type='hidden' name='AcademicYearID' id='AcademicYearID' value='$AcademicYearID'>\n";
		$x .= "</form>\n";
		
		return $x;
	}
	
	function getImportHolidaysEventsConfirmUI($AcademicYearID, $csv_data)
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom;
		include_once("libinterface.php");
		if($sys_custom['eClassApp']['SFOC']){
		  include_once("SFOC/libmedallist.php");
		  $libmedallist = new libmedallist();
		}
		
		$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
		if($isKIS) {
			include_once("libclassgroup.php");
			$lclassgroup = new Class_Group();
		}

		$linterface = new interface_html();
				
		$errorCount = 0;
		$successCount = 0;
		$error_result = array();
		
		$this->createTmpSchoolHolidayEventTable();
		
		## 	import data validation - Start 	##
		# ---------------------------------- #
		## Step 1: check "Start Date" (Empty? correct format?)
		## Step 2: check "End Date" (Empty? correct format?)
		## Step 3: check "End Date" >= "Start Date"?
		## Step 4: check "Event Title" is null?
		## Step 5: check "Skip School Day" is null?
		foreach($csv_data as $row=>$value){
		    if($sys_custom['eClassApp']['SFOC']){
// 			    list($StartDate, $EndDate, $Type, $Title, $Venue, $Nature, $Description, $SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN) = $value;
		        list($StartDate, $EndDate, $Type, $Association, $AssociationEng, $Title, $TitleEng, $Venue, $VenueEng, $Nature,$NatureEng,$Description,$DescriptionEng,$SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $Email, $ContactNo, $Website) = $value;

		    } else {
		        if($isKIS) {
		            list($StartDate, $EndDate, $Type, $Title, $TitleEng, $Venue, $VenueEng, $Nature,$NatureEng,$Description,$DescriptionEng,  $SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $ClassGroupCode) = $value;
		        }else{
		            list($StartDate, $EndDate, $Type, $Title, $TitleEng, $Venue, $VenueEng, $Nature,$NatureEng,$Description,$DescriptionEng,$SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN) = $value;
		        }
		    }
			$RowNum++;
			$Validation = true;
			$error_msg = array();
			
			$StartDate = getDefaultDateFormat($StartDate);
			$EndDate = getDefaultDateFormat($EndDate);
			
			## check empty ##
			if(!$this->isImportDataEmpty($StartDate,$EndDate,$Title,$SkipSchoolDay))
				$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['DataMissing'] . " (***)";
				
			## check start date valid ##
			if(!empty($StartDate)) {
				if(!$this->isImportDateValid($StartDate))
					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['StartDateInvalid'];
			} else {
				$empty_field[$RowNum]['1'] = "(***)";
			}
			
			## check end date valid ##
			if(!empty($EndDate)) {
				if(!$this->isImportDateValid($EndDate))
					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['EndDateInvalid'];
			} else {
				$empty_field[$RowNum]['2'] = "(***)";
			}
			
			## check date range valid ##
			if((!empty($StartDate)) && (!empty($EndDate)))
			{
				if($StartDate > $EndDate)
				{
					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['InvalidDateRange'];
				}
			}
			
			## check event is in current school year ##
			if((!empty($StartDate)) && (!empty($EndDate)))
			{
				$SchoolYearStartDate = getStartDateOfAcademicYear($AcademicYearID);
				$SchoolYearStartDate = substr($SchoolYearStartDate,0,10);
				if($SchoolYearStartDate > $StartDate)
				{
					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['NotInCurrentSchoolYear'];
				}
			}
			
			## check event type ##
			if(!empty($Type)){
				if(!$this->isImportTypeValid($Type))
					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['TypeInvalid'];
			}else{
				$empty_field[$RowNum]['3'] = "(***)";
			}
			
			if($sys_custom['eClassApp']['SFOC']){
//     			if(!empty($SportType)){
//     			    $sportIDAry = $libmedallist->getSportID();
//     			    if(!in_array($SportType, $sportIDAry))
//     			        $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SportTypeInvalid'];
//     			}else{
//     			    $empty_field[$RowNum]['4'] = "(***)";
//     			}
    			
    			if(empty($Association)){
    			    $empty_field[$RowNum]['4'] = "(***)";
    			}
    			
    			if(empty($AssociationEng)){
    			    $empty_field[$RowNum]['5'] = "(***)";
    			}
    			
    			if(empty($Title)){
    				$empty_field[$RowNum]['6'] = "(***)";
    			}
    			
    			if(empty($TitleEng)){
    			    $empty_field[$RowNum]["7"] = "(***)";
    			}
    			
    			## check event venue ##
    			if(!empty($Venue)){
    			    if(!$this->isVenueValid($Venue)){
    					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['VenueInvalid'];
    			    }else{
    			        /*if(substr($Venue, 0, 6)!='Others' && $Venue != $VenueEng){
    			            $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['VenueEngInvalid'];
    			        }*/
    			        if($VenueEng == ''){
    			            $empty_field[$RowNum]['9'] = "(***)";
    			        }
    				}
    			} else {
    			    $empty_field[$RowNum]['9'] = "(***)";
    			}
    			
    			## check skip school day ##
    			if(!empty($SkipSchoolDay)) {
    				if(!$this->isImportSkipSchoolDayValid($SkipSchoolDay))
    					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSchoolDayInvalid'];
    			} else {
    				$empty_field[$RowNum]['13'] = "(***)";
    			}				
    				
    			### Check GroupID ###
    			# Not Group Event with Group ID
    			if($Type != "GE" && $GroupID != "")
    			{
    				$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['TypeNotWithGroupID'];
    			}
    			# Group Event
    			if($Type=="GE")
    			{	
    				# Empty Group ID
    				if($GroupID == ""){
    					$empty_field[$RowNum]['14'] = "(****)";
    					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectTheGroup'] . " (****)";
    				} 
    				# Invalid Group ID
    				elseif(!$this->isGroupIDValid($GroupID, $AcademicYearID)){
    					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['GroupIDError'];
    				}
    			}
    			
    			## check skip SAT ##
    			if(!empty($SkipSAT)) {
    				if(!$this->isImportSkipSchoolDayValid($SkipSAT))
    					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSATInvalid'];
    			} else {
    				$empty_field[$RowNum]['15'] = "(***)";
    			}		
    			
    			## check skip SUN ##
    			if(!empty($SkipSUN)) {
    				if(!$this->isImportSkipSchoolDayValid($SkipSUN))
    					$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSUNInvalid'];
    			} else {
    				$empty_field[$RowNum]['16'] = "(***)";
    			}
    			
    			if(empty($Email)){
    			    $empty_field[$RowNum]['17'] = "(***)";
    			}
    			if(empty($ContactNo)){
    			    $empty_field[$RowNum]['18'] = "(***)";
    			}
    			if(empty($Website)){
    			    $empty_field[$RowNum]['19'] = "(***)";
    			}
			} else{
			    if(empty($Title)){
			        $empty_field[$RowNum]['4'] = "(***)";
			    }
			    
			    if(empty($TitleEng)){
			        $empty_field[$RowNum]["5"] = "(***)";
			    }
			    
			    ## check event venue ##
			    if(!empty($Venue)){
			        if(!$this->isVenueValid($Venue)){
			            $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['VenueInvalid'];
			        }else{
			            /*if(substr($Venue, 0, 6)!='Others' && $Venue != $VenueEng){
			             $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['VenueEngInvalid'];
			             }*/
			            if($VenueEng == ''){
			                $empty_field[$RowNum]['7'] = "(***)";
			            }
			        }
			    } else {
			        $empty_field[$RowNum]['6'] = "(***)";
			    }
			    
			    ## check skip school day ##
			    if(!empty($SkipSchoolDay)) {
			        if(!$this->isImportSkipSchoolDayValid($SkipSchoolDay))
			            $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSchoolDayInvalid'];
			    } else {
			        $empty_field[$RowNum]['10'] = "(***)";
			    }
			    
			    ### Check GroupID ###
			    # Not Group Event with Group ID
			    if($Type != "GE" && $GroupID != "")
			    {
			        $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['TypeNotWithGroupID'];
			    }
			    # Group Event
			    if($Type=="GE")
			    {
			        # Empty Group ID
			        if($GroupID == ""){
			            $empty_field[$RowNum]['11'] = "(****)";
			            $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectTheGroup'] . " (****)";
			        }
			        # Invalid Group ID
			        elseif(!$this->isGroupIDValid($GroupID, $AcademicYearID)){
			            $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['GroupIDError'];
			        }
			    }
			    
			    ## check skip SAT ##
			    if(!empty($SkipSAT)) {
			        if(!$this->isImportSkipSchoolDayValid($SkipSAT))
			            $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSATInvalid'];
			    } else {
			        $empty_field[$RowNum]['12'] = "(***)";
			    }
			    
			    ## check skip SUN ##
			    if(!empty($SkipSUN)) {
			        if(!$this->isImportSkipSchoolDayValid($SkipSUN))
			            $error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSUNInvalid'];
			    } else {
			        $empty_field[$RowNum]['13'] = "(***)";
			    }
			}
			
			## check Class Group Code ##
			if($isKIS) {
				if(!empty($ClassGroupCode)) {
					if(!$lclassgroup->Is_Code_Existed($ClassGroupCode)){
						$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['ClassGroupCodeInvalid'];
					}
				}
				//else{
				//	$empty_field[$RowNum]['12'] = "(***)";
				//	$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['ClassGroupCodeInvalid'];
				//}
			}
		
			if(empty($error_msg))
			{
				$successCount++;
			}
			else
			{
				$error_result[$RowNum] = $error_msg;
				$error_RowNum_str .=  $RowNum . ",";
				$errorCount++;
			}
		}
		
		# step information
		$STEPS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['SelectImportFile'], 0);
		$STEPS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ConfirmImportRecord'], 1);
		$STEPS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ImportedResult'], 0);
		
		$x .= "<BR>\n";
		$x .= "<BR>\n";
		$x .= "<form name='form1' id='form1' method='post' action='import_holidays_events_update.php' enctype='multipart/form-data'>\n";
		$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center'>\n";
		$x .= "<tr><td>\n";
		
		### Show step table ###
		$x .= "<table width='100%' border='0' cellpadding=\"3\" cellspacing=\"0\">\n";
		$x .= "<tr><td colspan='2'>".$linterface->GET_STEPS($STEPS_OBJ)."</td></tr>\n";
		$x .= "<tr><td colspan='2'></td></tr>\n";
		$x .= "</table>\n";
		
		### Show import result summary ###
		$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
		$x .= "<tr>\n";
		$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>\n";
		$x .= "<td class='tabletext'>". $successCount ."</td>\n";
		$x .= "</tr>\n";
		$x .= "<tr>\n";
		$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>\n";
		$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>\n";
		$x .= "</tr>\n";
		$x .= "</table><br>\n";
		
		if($errorCount > 0){
    		### Show import result w/ details ###
    		$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
    		$x .= "<tr>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">Row#</td>";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventStartDate']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventEndDate']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventType']."</td>\n";
    		if($sys_custom['eClassApp']['SFOC']){
    		  $x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SportsType']."</td>\n";
    		  $x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Association']."</td>\n";
    		  $x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AssociationEng']."</td>\n";
    		}
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitle']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitleEng']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenue']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenueEng']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventNature']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventNatureEng']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDescription']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDescriptionEng']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipCycleDay']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['GroupID']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipSAT']."</td>\n";
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipSUN']."</td>\n";
    		if($sys_custom['eClassApp']['SFOC']){
    		  $x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Email']."</td>\n";
    		  $x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ContactNo']."</td>\n";
    		  $x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Website']."</td>\n";
    		}
    		if($isKIS) {
    			$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['FormClassMapping']['ClassGroupCode']."</td>\n";
    		}
    		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['Remark']."</td>\n";
    		$x .= "</tr>\n";
		
	   }
		if($sys_custom['eClassApp']['SFOC']){
    		foreach($csv_data as $row=>$value){
    		    if($isKIS) {
    		        //list($StartDate, $EndDate, $Type, $Title, $Venue, $Nature, $Description, $SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $ClassGroupCode) = $value;
    		        list($StartDate, $EndDate, $Type, $Association, $AssociationEng, $Title, $TitleEng, $Venue, $VenueEng, $Nature,$NatureEng,$Description,$DescriptionEng,$SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $Email, $ContactNo, $Website, $ClassGroupCode) = $value;
    		    }else{
    		        //list($StartDate, $EndDate, $Type, $Title, $Venue, $Nature, $Description, $SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN) = $value;
    		        list($StartDate, $EndDate, $Type, $Association, $AssociationEng, $Title, $TitleEng, $Venue, $VenueEng, $Nature,$NatureEng,$Description,$DescriptionEng,$SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $Email, $ContactNo, $Website) = $value;
    		    }
    			$i++;
    			
    			$x .= "<tr>\n";
    			$x .= "<td>$i</td>\n";
    			
    			if($empty_field[$i]['1'] != "")
    				$x .= "<td>".$empty_field[$i]['1']."</td>\n";
    			else
    				$x .= "<td>$StartDate</td>\n";
    				
    			if($empty_field[$i]['2'] != "")
    				$x .= "<td>".$empty_field[$i]['2']."</td>\n";
    			else
    				$x .= "<td>$EndDate</td>\n";
    			
    			if($empty_field[$i]['3'] != "")
    				$x .= "<td>".$empty_field[$i]['3']."</td>\n";
    			else
    				$x .= "<td>$Type</td>\n";	
    			
//     			if($empty_field[$i]['4'] != "")
//     				$x .= "<td>".$empty_field[$i]['4']."</td>\n";
//     			else
//     				$x .= "<td>$SportType</td>\n";
    			
    			if($empty_field[$i]['4'] != "")
    			    $x .= "<td>".$empty_field[$i]['4']."</td>\n";
    			else
    			    $x .= "<td>$Association</td>\n";
    			
    			if($empty_field[$i]['5'] != "")
    			    $x .= "<td>".$empty_field[$i]['5']."</td>\n";
    			else
    			    $x .= "<td>$AssociationEng</td>\n";
    			
    			if($empty_field[$i]['6'] != "")
    				$x .= "<td>".$empty_field[$i]['6']."</td>\n";
    			else
    				$x .= "<td>$Title</td>\n";
    			if($empty_field[$i]['7'] != "")
    			    $x .= "<td>".$empty_field[$i]['7']."</td>\n";
    			else
    			    $x .= "<td>$TitleEng</td>\n";
    			$x .= "<td>$Venue</td>\n";
    			$x .= "<td>$VenueEng</td>\n";
    			$x .= "<td>$Nature</td>\n";
    			$x .= "<td>$Description</td>\n";
    			
    			if($empty_field[$i]['12'] != "")
    				$x .= "<td>".$empty_field[$i]['12']."</td>\n";
    			else
    				$x .= "<td>$SkipSchoolDay</td>\n";
    			
    			if($empty_field[$i]['13'] != "")
    				$x .= "<td>".$empty_field[$i]['13']."</td>\n";
    			else
    				$x .= "<td>$GroupID</td>\n";
    			
    			if($empty_field[$i]['14'] != "")
    				$x .= "<td>".$empty_field[$i]['14']."</td>\n";
    			else
    				$x .= "<td>$SkipSAT</td>\n";
    				
    			if($empty_field[$i]['15'] != "")
    				$x .= "<td>".$empty_field[$i]['15']."</td>\n";
    			else
    				$x .= "<td>$SkipSUN</td>\n";
    			
    			if($empty_field[$i]['16'] != "")
    			    $x .= "<td>".$empty_field[$i]['16']."</td>\n";
    			else
    			    $x .= "<td>$Email</td>\n";
    			
    			if($empty_field[$i]['17'] != "")
    			    $x .= "<td>".$empty_field[$i]['17']."</td>\n";
    			else
    			    $x .= "<td>$ContactNo</td>";
    			
    			if($empty_field[$i]['18'] != "")
    			    $x .= "<td>".$empty_field[$i]['18']."</td>\n";
    			else
    			    $x .= "<td>$Website</td>\n";
    			
    			if($isKIS){
    				if($empty_field[$i]['19'] != "")
    					$x .= "<td>".$empty_field[$i]['19']."</td>\n";
    				else
    					$x .= "<td>$ClassGroupCode</td>\n";
    			}
    			
    			$error_remark = "";
    			if($errorCount > 0)
    			{
    				if(is_array($error_result[$i]))
    				{
    					foreach($error_result[$i] as $k1=>$d1)
    					{
    						$error_remark .= "<li>". $d1."</li>";
    					}
    				}
    			}
    			$x .= "<td>$error_remark</td>\n";
    			$x .= "</tr>\n";
    		}
		} else {
    		foreach($csv_data as $row=>$value){
    		    // SFOC == false
    		    if($isKIS) {
    		        list($StartDate, $EndDate, $Type, $Title, $TitleEng, $Venue, $VenueEng, $Nature,$NatureEng,$Description,$DescriptionEng,$SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $ClassGroupCode) = $value;
    		    }else{
    		        list($StartDate, $EndDate, $Type, $Title, $TitleEng, $Venue, $VenueEng, $Nature,$NatureEng,$Description,$DescriptionEng,$SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN) = $value;
    		    }
        		$i++;
        		
        		if($errorCount > 0)
        		{
        		    if(is_array($error_result[$i]))
            		{            		        
                		$x .= "<tr>\n";
                		$x .= "<td>$i</td>\n";
                		
                		if($empty_field[$i]['1'] != "")
                			$x .= "<td>".$empty_field[$i]['1']."</td>\n";
                		else
                			$x .= "<td>$StartDate</td>\n";
                			
                		if($empty_field[$i]['2'] != "")
                			$x .= "<td>".$empty_field[$i]['2']."</td>\n";
                		else
                			$x .= "<td>$EndDate</td>\n";
                		
                		if($empty_field[$i]['3'] != "")
                			$x .= "<td>".$empty_field[$i]['3']."</td>\n";
                		else
                			$x .= "<td>$Type</td>\n";	
                		
                		if($empty_field[$i]['4'] != "")
                			$x .= "<td>".$empty_field[$i]['4']."</td>\n";
                		else
                			$x .= "<td>$Title</td>\n";
                		if($empty_field[$i]['5'] != "")
                		    $x .= "<td>".$empty_field[$i]['5']."</td>\n";
                		else
                		    $x .= "<td>$TitleEng</td>\n";
                		$x .= "<td>$Venue</td>\n";
                		$x .= "<td>$VenueEng</td>\n";
                		$x .= "<td>$Nature</td>\n";
                		$x .= "<td>$NatureEng</td>\n";
                		$x .= "<td>$Description</td>\n";
                		$x .= "<td>$DescriptionEng</td>\n";
                		
                		if($empty_field[$i]['10'] != "")
                			$x .= "<td>".$empty_field[$i]['10']."</td>\n";
                		else
                			$x .= "<td>$SkipSchoolDay</td>\n";
                		
                		if($empty_field[$i]['11'] != "")
                			$x .= "<td>".$empty_field[$i]['11']."</td>\n";
                		else
                			$x .= "<td>$GroupID</td>\n";
                		
                		if($empty_field[$i]['12'] != "")
                			$x .= "<td>".$empty_field[$i]['12']."</td>\n";
                		else
                			$x .= "<td>$SkipSAT</td>\n";
                			
                		if($empty_field[$i]['13'] != "")
                			$x .= "<td>".$empty_field[$i]['13']."</td>\n";
                		else
                			$x .= "<td>$SkipSUN</td>\n";
                		
                		if($isKIS){
                			if($empty_field[$i]['14'] != "")
                				$x .= "<td>".$empty_field[$i]['14']."</td>\n";
                			else
                				$x .= "<td>$ClassGroupCode</td>\n";
                		}           	
            		    $error_remark = "";	 
            		    foreach($error_result[$i] as $k1=>$d1)
            			{
            				    
            				$error_remark .= "<li>". $d1."</li>";
            			}
            			$x .= "<td>$error_remark</td>\n";
            			$x .= "</tr>\n";
            		}
        		}
        	
     
    		}
		}
		
		if($errorCount>0)
		{
			$import_button = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import_holidays_events.php?AcademicYearID=$AcademicYearID'");
		}
		else
		{
			$this->insertDataToTmpSchoolHolidayEventTable($csv_data);
			$import_button = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit")." &nbsp;".$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import_holidays_events.php?AcademicYearID=$AcademicYearID'");
		}
		if($errorCount > 0){
		    $x .= "</table>\n";
		}
	
		$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
		$x .= "<tr>\n";
		$x .= "<td class='dotline'><img src='$image_path'/$LAYOUT_SKIN/10x10.gif' width='10' height='1' /></td>\n";
		$x .= "</tr>\n";			
		$x .= "<tr><td align='center' colspan='2'>$import_button</td></tr>";
		$x .= "</table>\n";
		
		$x .= "</td></tr>\n";
		$x .= "</table><br>\n";
		$x .= "<input type='hidden' name='AcademicYearID' id='AcademicYearID' value='$AcademicYearID'>\n";
		$x .= "</form\n";
		return $x;
	}
	
	function isImportDataEmpty($StartDate,$EndDate,$Title,$SkipSchoolDay)
	{
		if(empty($StartDate)||empty($EndDate)||empty($Title)||empty($SkipSchoolDay)){
			//$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['DataMissing'] . " (***)";
			return false;
		} else {
			return true;
		}
	}
	
	function isImportDateValid($Date)
	{
		$Year = substr($Date,0,4);
		$Month = substr($Date,5,2);
		$Day = substr($Date,8,2);
		if(date("Y-m-d",mktime(0,0,0,$Month,$Day,$Year)) != $Date){
			//$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['StartDateInvalid'];
			return false;
		} else {
			return true;
		}
	}
	
	function isImportTypeValid($Type)
	{
		$Type = trim($Type);
		$EventTypeArray = array("SE","AE","PH","SH","GE");
		if(!in_array($Type,$EventTypeArray)){
			//$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['TypeInvalid'];
			return false;
		} else {
			return true;
		}
	}
	
	function isVenueValid($Venue)
	{
		$Venue = trim($Venue);
		$location_type = substr($Venue, 0, strpos($Venue,">"));
					
		if(strtoupper($location_type) != "OTHERS")
		{
			$sql = "SELECT BuildingID FROM INVENTORY_LOCATION_BUILDING WHERE Code = '$location_type'";
			$result = $this->returnVector($sql);
			if(sizeof($result)>0){
				$BuildingCode = $location_type;
				$Venue2 = substr($Venue,strpos($Venue,">")+1,strlen($Venue));
				$FloorCode = substr($Venue2,0,strpos($Venue2,">"));
				$RoomCode = substr($Venue2,strpos($Venue2,">")+1,strlen($Venue));
				
				$sql = "SELECT LocationID FROM INVENTORY_LOCATION AS room INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (room.LocationLevelID = floor.LocationLevelID) WHERE floor.Code = '$FloorCode' AND room.Code = '$RoomCode'";
				$result = $this->returnVector($sql);
				if(sizeof($result)>0){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		else 
			return true;
	}
	
	function isImportSkipSchoolDayValid($SkipSchoolDay)
	{
		$SkipSchoolDay = trim($SkipSchoolDay);
		$SkipSchoolDayArray = array("Y","N");
		if(!in_array($SkipSchoolDay,$SkipSchoolDayArray)){
			//$error_msg[] = $Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSchoolDayInvalid'];
			return false;
		} else {
			return true;
		}
	}
	
	function showImportHolidaysEventsResult($AcademicYearID,$ImportResult)
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		include_once("libinterface.php");
		$linterface = new interface_html();
		
		# step information
		$STEPS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['SelectImportFile'], 0);
		$STEPS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ConfirmImportRecord'], 0);
		$STEPS_OBJ[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ImportedResult'], 1);
		
		$x .= "<br>\n";
		$x .= "<br>\n";
		$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center'>\n";
		$x .= "<tr><td>\n";
		
		### Show step table ###
		$x .= "<table width='100%' border='0' cellpadding=\"3\" cellspacing=\"0\">\n";
		$x .= "<tr><td colspan='2'>".$linterface->GET_STEPS($STEPS_OBJ)."</td></tr>\n";
		$x .= "<tr><td colspan='2'></td></tr>\n";
		$x .= "</table>\n";
		
		$x .= "</td></tr>\n";
		$x .= "<tr><td>\n";
		
		if($ImportResult){
			$ImportResultMsg = $Lang['SysMgr']['SchoolCalendar']['Import']['Result']['Success'];
		} else {
			$ImportResultMsg = $Lang['SysMgr']['SchoolCalendar']['Import']['Result']['Fail'];
		}
		
		### Show import result ###
		$x .= "<table width='100%' border='0' cellpadding=\"3\" cellspacing=\"0\" align=\"center\">\n";
		$x .= "<tr><td align='center'>".$ImportResultMsg."</td></tr>\n";
		$x .= "</table>";
		
		$button = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='import_holidays_events.php?AcademicYearID=$AcademicYearID'");
		
		$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
		$x .= "<tr>\n";
		$x .= "<td class='dotline'><img src='$image_path'/$LAYOUT_SKIN/10x10.gif' width='10' height='1' /></td>\n";
		$x .= "</tr>\n";			
		$x .= "<tr><td align='center' colspan='2'>$button</td></tr>";
		$x .= "</table>\n";
		
		$x .= "</td></tr>\n";
		$x .= "</table>";
		
		return $x;
	}
	
	function isGroupIDValid($GroupID, $parAcademicYearId='')
	{
		include_once("form_class_manage.php");
		$lfcm = new form_class_manage();
		
		//$CurrentAcademicYearID = $lfcm->getCurrentAcademicaYearID();
		if ($parAcademicYearId == '') {
			$parAcademicYearId = $lfcm->getCurrentAcademicaYearID();
		}
		
		$Group_Array = explode(",",$GroupID);
		if(sizeof($Group_Array)>0){
			for($i=0; $i<sizeof($Group_Array); $i++){
				//$sql = "SELECT Count(*) FROM INTRANET_GROUP WHERE GroupID = ".$Group_Array[$i]." AND (AcademicYearID = $CurrentAcademicYearID OR AcademicYearID IS NULL)";
				$sql = "SELECT Count(*) FROM INTRANET_GROUP WHERE GroupID = ".$Group_Array[$i]." AND (AcademicYearID = '$parAcademicYearId' OR AcademicYearID IS NULL)";
				$result = $this->returnVector($sql);
				if($result[0] == 0){
					$error++;
				}
			}
		}
		if($error > 0)
		{
			return false;
		}else{
			return true;
		}
	}
	
	
	/**
	 * Generate the calendar part of school calendar printing version
	 * @param	int|string	current academic id
	 * @param	array		(bool ["IS_COLOR_PRINT"], bool ['IS_PRINT_NO_EVENT'], int ['NUM_CAL_PER_PAGE'])
	 * @return	string		HTML of the calendar division
	 */
	function calendarDivision($current_school_year_id='', $ParPrintParArr, $ClassGroupID='') {
		global $Lang;
		
		include_once('form_class_manage.php');
		$academicYear = new academic_year($current_school_year_id);
		$academicYearName = $academicYear->Get_Academic_Year_Name();
		
		$IS_PRINTING_VERSION = true;
		
		$x = $this->printingVersionCSS($ParPrintParArr['IS_COLOR_PRINT']);
		
		$x .= "<div class='page_title_print'>" . $Lang['SysMgr']['SchoolCalendar']['ModuleTitle'] . " " . $academicYearName . "</div>";
		$x .= "<div class='bottom_remark' style='text-align:left;'>";
		$x .= $this->generatePrintingVersionLabels($ParPrintParArr['IS_COLOR_PRINT']);
		$x .= "<div id='main_body'>";
		$x .= "<div id='Production_Calendar'>";
		$x .= $this->loadCalendar($current_school_year_id, 1, $IS_PRINTING_VERSION, $ParPrintParArr, $ClassGroupID);
		$x .= "</div>";
		$x .= "</div>";

		return $x;
	}
	
	
	function printingVersionCSS($ParIsColorPrint=false) {
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
			$x .= "
		<link href='$PATH_WRT_ROOT/templates/$LAYOUT_SKIN/css/print.css' rel='stylesheet' type='text/css'>
				";
		if ($ParIsColorPrint) {
		} else { 
			// css override
			$x .= "
		<link href='$PATH_WRT_ROOT/templates/$LAYOUT_SKIN/css/school_calendar_print.css' rel='stylesheet' type='text/css'>
				";
		}
		return $x;
	}
	
	function generatePrintingVersionLabels($ParIsColorPrint=false) {
		global $Lang, $image_path, $LAYOUT_SKIN;
		
		$PUBLIC_HOLIDAY = $Lang['SysMgr']['SchoolCalendar']['EventType'][3];
		$SCHOOL_HOLIDAY = $Lang['SysMgr']['SchoolCalendar']['EventType'][4];
		$SCHOOL_EVENTS = $Lang['SysMgr']['SchoolCalendar']['EventType'][0];
		$ACADEMIC_EVENTS = $Lang['SysMgr']['SchoolCalendar']['EventType'][1];
		$GROUP_EVENTS = $Lang['SysMgr']['SchoolCalendar']['EventType'][2];
		
		$PUBLIC_HOLIDAY_SPAN = "";
		$SCHOOL_HOLIDAY_SPAN = "";
		$SCHOOL_EVENTS_SPAN = "";
		$ACADEMIC_EVENTS_SPAN = "";
		$GROUP_EVENTS_SPAN = "";
		
		if($ParIsColorPrint) {
			
			$spanWithCss = 'style="width:110px;"';
			
			$PUBLIC_HOLIDAY_SPAN = "
							<span class='event_type_icon'>1</span>
							<span $spanWithCss >" . $PUBLIC_HOLIDAY . "</span>
							";
			$SCHOOL_HOLIDAY_SPAN = "
							<span class='event_type_icon'>1</span>
							<span $spanWithCss >" . $SCHOOL_HOLIDAY . "</span>
							";
			$SCHOOL_EVENTS_SPAN = "
							<span class='event_type_icon'>&nbsp;</span>
							<span $spanWithCss >" . $SCHOOL_EVENTS . "</span>
							";
			$ACADEMIC_EVENTS_SPAN = "
							<span class='event_type_icon'>&nbsp;</span>
							<span $spanWithCss >" . $ACADEMIC_EVENTS . "</span>
							";
			$GROUP_EVENTS_SPAN = "
							<span class='event_type_icon'><img src='" . $image_path . "/" . $LAYOUT_SKIN. "/" . "index/calendar/icon_groupevent.gif' align='right'></span>
							<span $spanWithCss >" . $GROUP_EVENTS . "</span>
							";
		}
		$x .= "
		<table>
			<tbody>
				<tr>
					<td>
						<div class='event_list_title title_public_holiday'>
							" . $PUBLIC_HOLIDAY_SPAN . "
						</div>
					</td>
					<td>
						<div class='event_list_title title_school_holiday'>
							" . $SCHOOL_HOLIDAY_SPAN . "
						</div>
					</td>
					<td>
						<div class='event_list_title title_school_event'>
							" . $SCHOOL_EVENTS_SPAN . "
						</div>
					</td>
					<td>
						<div class='event_list_title title_academic'>
							" . $ACADEMIC_EVENTS_SPAN . "
						</div>
					</td>
					<td>
						<div class='event_list_title title_group_event'>
							" . $GROUP_EVENTS_SPAN . "
						</div>
					</td>
		
				</tr>
			</tbody>
		</table>
		</div>
		";
		return $x;
	}
	
	function generateCalPrintingOptionsBox() {
		global $Lang,$linterface;
			$MAX_CALENDAR_DISPLAY_SIZE = 6;
			for($i=1; $i<=$MAX_CALENDAR_DISPLAY_SIZE; $i++) {
				$radioElement .= "<input  type='radio' name='maxCal' id='max_cal_$i' value='$i' /> $i";
			}
			$x = "
				<tr>
					<td align='left' width='15%'><div class='Conntent_tool'><div id='import_button_div'>".$linterface->GET_LNK_IMPORT("javascript:importHolidayEvent()",$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ImportHolidayOrEvent'])."</div></div></td>
					<td align='left' width='15%'><div class='Conntent_tool'><div id='export_button_div'>".$linterface->GET_LNK_EXPORT("javascript:exportHolidayEvent()",$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ExportHolidayOrEvent'])."</div></div></td>
					<td>
						<div class='Conntent_tool'><a href='#' class='print' onclick=\"MM_showHideLayers('print_option_choice','','show');\">  " . $Lang['SysMgr']['SchoolCalendar']['Print']['Print'] . "</a> 
							<div class='print_option_layer' id='print_option_choice'>
	                         <em > - " . $Lang['SysMgr']['SchoolCalendar']['Print']['PrintOptions'] . " -</em>
	                          <table class='form_table'>
	                             <col class='field_title' />
	                             <col class='field_c' />
	                            <tr>
	                              <td width='40%'>" . $Lang['SysMgr']['SchoolCalendar']['Print']['PrintingColor'] . "</td>
	                              <td>:</td>
	                              <td><input type='radio' name='printColor' id='color_BW' value='0' />
	                                " . $Lang['SysMgr']['SchoolCalendar']['Print']['Black/White'] . "
	                                  <input type='radio' name='printColor' id='color_color' value='1' />
	                                " . $Lang['SysMgr']['SchoolCalendar']['Print']['Color'] . "</td>
	                            </tr>
	                            <tr>
	                              <td>" . $Lang['SysMgr']['SchoolCalendar']['Print']['MaximumCalendarsPrintedPerPage'] . "</td>
	                              <td>:</td>
	                              <td>$radioElement
								  </td>
	                            </tr>
	                          </table>
	                          <div class='edit_bottom'>
	                            <!-- <span> Last updated : 2 days ago by Admin</span>-->
	                            <p class='spacer'></p>
	                            <input type='button' class='formsmallbutton'  onclick=\"printPreview();\"
			 onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value='" . $Lang['SysMgr']['SchoolCalendar']['Print']['Print'] . "' />
	                            <input type='button' class='formsmallbutton'
			onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value='" . $Lang['SysMgr']['SchoolCalendar']['Print']['Cancel'] . "' onclick=\"MM_showHideLayers('print_option_choice','','hide')\"/>
	                            <p class='spacer'></p>
	                          </div>
	                        </div>
						</div>
					</td>
				</tr>";
				return $x;	
	}
	
	function GetDeleteAllSchoolEventsOrHolidaysLayer($TargetYearID)
	{
		global $Lang;
		
		include_once("libinterface.php");
		$linterface = new interface_html();
		
		$StartDate = getStartDateOfAcademicYear($TargetYearID);
		$EndDate = getEndDateOfAcademicYear($TargetYearID);
		$SchoolYearInfo = getAcademicYearByAcademicYearID($TargetYearID,$intranet_session_language);
		
		## No. Of Total School Event
		$NoOfSchoolEvent = $this->GetTotalNumOfEventsByDateRange($StartDate,$EndDate,0);
		
		## No. Of Total Academic Event
		$NoOfAcademicEvent = $this->GetTotalNumOfEventsByDateRange($StartDate,$EndDate,1);
		
		## No. Of Total Group Event
		$NoOfGroupEvent = $this->GetTotalNumOfEventsByDateRange($StartDate,$EndDate,2);
		
		## No. Of Total Public Holiday
		$NoOfPublicHoliday = $this->GetTotalNumOfEventsByDateRange($StartDate,$EndDate,3);
		
		## No. Of Total School Holiday
		$NoOfSchoolHoliday = $this->GetTotalNumOfEventsByDateRange($StartDate,$EndDate,4);
		
		$table_content .= '<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">';
		$table_content .= '<tr>';
		$table_content .= '<td align="center">'.$linterface->GET_WARNING_TABLE($Lang['SysMgr']['SchoolCalendar']['DeleteAll']['WarningMsg']['DeleteAllEventsHolidays']).'</td>';
		$table_content .= '</tr>';
		$table_content .= '<tr><td>';
		$table_content .= '<table width="60%" border="0" cellpadding="3" cellspacing="0" align="right">';
		$table_content .= '<tr>';
		$table_content .= '<td>'.$Lang['General']['SchoolYear'].'</td><td>'.$SchoolYearInfo.'</td>';
		$table_content .= '</tr>';
		$table_content .= '<tr>';
		$table_content .= '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfSchoolEvents'].'</td><td>'.$NoOfSchoolEvent.'</td>';
		$table_content .= '</tr>';
		$table_content .= '<tr>';
		$table_content .= '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfAcademicEvents'].'</td><td>'.$NoOfAcademicEvent.'</td>';
		$table_content .= '</tr>';
		$table_content .= '<tr>';
		$table_content .= '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfGroupEvents'].'</td><td>'.$NoOfGroupEvent.'</td>';
		$table_content .= '</tr>';
		$table_content .= '<tr>';
		$table_content .= '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfPublicHolidays'].'</td><td>'.$NoOfPublicHoliday.'</td>';
		$table_content .= '</tr>';
		$table_content .= '<tr>';
		$table_content .= '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['SysMgr']['SchoolCalendar']['DeleteAll']['FieldTitle']['TotalNoOfSchoolHolidays'].'</td><td>'.$NoOfSchoolHoliday.'</td>';
		$table_content .= '</tr>';
		$table_content .= '</table>';
		$table_content .= '</td></tr>';
		$table_content .= '<td align="center">
								<div class="edit_bottom_v30">
								<p class="spacer"></p>'.
									$linterface->GET_ACTION_BTN($Lang['Btn']['DeleteAll'], "submit", "", "", "", "", "formbutton_alert").' '.
									$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "JavaScript:window.location='index.php?AcademicYearID=$TargetYearID'", "", "", "", "formbutton_v30")
								.'<p class="spacer"></p>
							</td>';
		$table_content .= '</tr>';
		$table_content .= '</table>';
		return $table_content;
	} 
	
	
	
	public function Get_School_Settings_Top_Tab($CurrentTab) {
		global $Lang;
		
		$TagArr = array();
		$TagArr[] = array($Lang['SysMgr']['SchoolCalendar']['SettingTitle'],"index.php", ($CurrentTab=='Event' || $CurrentTab==''));
		$TagArr[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'],"time_zone.php", ($CurrentTab=='Timezone'));
		$TagArr[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'],"academic_year.php", ($CurrentTab=='SchoolYear'));
		
		return $TagArr; 
	}
	
	public function Get_Timezone_Special_Timetable_Settings_UI($AcademicYearID, $TimezoneID, $SpecialTimetableSettingsID) {
		global $Lang, $PATH_WRT_ROOT, $linterface;
		
		include_once($PATH_WRT_ROOT.'includes/libtimetable_ui.php');
		$libtimetable_ui = new libtimetable_ui();
		
		$EditMode = ($SpecialTimetableSettingsID=='')? false : true;
		if ($EditMode) {
			### Get Special Settings Info
			$SpecialTimetableSettingsInfoArr = $this->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsID);
			
			$TimetableID = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['TimetableID'];
			$BgColor = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['BgColor'];
			$RepeatType = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['RepeatType'];
			$LastModifiedBy = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['ModifiedBy'];
			$LastModifiedDate = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['ModifyDate'];
		}
		
				
		### Get TimeZone Info
		$TimezoneInfoArr = $this->returnPeriods_NEW($TimezoneID);
		$TimezoneStartDate = $TimezoneInfoArr[0]['PeriodStart'];
		$TimezoneEndDate = $TimezoneInfoArr[0]['PeriodEnd'];
		$PeriodType = $TimezoneInfoArr[0]['PeriodType'];
		$PeriodTypeText = $this->Get_Period_Type_Text($PeriodType);
		$NumOfCycleDay = $TimezoneInfoArr[0]['PeriodDays'];
		
		$TimezoneAcademicInfoArr = $this->Get_Academic_Year_And_Term_Info_By_TimeZone($TimezoneID);
		$YearTermID = $TimezoneAcademicInfoArr[0]['YearTermID'];
		
		
		### Get Timetable Selection
//		$ApplicableTimetableInfoArr = getAvailableTimetableTemplateWithDaysChecking($AcademicYearID, $YearTermID, $TimezoneStartDate, $PeriodTypeText, $NumOfCycleDay);
//		$ApplicableTimetableIDArr = Get_Array_By_Key($ApplicableTimetableInfoArr, 'TimeTableID');
//		unset($ApplicableTimetableInfoArr);
//		$TimetableSelection = $libtimetable_ui->Get_Timetable_Selection('TimetableID', $AcademicYearID, $YearTermID, $TimetableID, $OnChange='', $noFirst=0, $ParFirstTitle='', $ShowAllocatedOnly=0, $ExcludeTimetableID='', $ApplicableTimetableIDArr);
		
		
		### Det Default Special Timetable Settings
		$BgColor = ($BgColor=='')? '#994499' : $BgColor;
		$RepeatType = ($RepeatType=='')? $PeriodTypeText : $RepeatType;
		
		
		### Navaigtion
		$PAGE_NAVIGATION[] = array($Lang['SysMgr']['CycleDay']['SettingTitle'], "javascript:js_Go_Back_Time_Zone();");
		$PAGE_NAVIGATION[] = array($Lang['SysMgr']['CycleDay']['SpecialTimetableSettings'], "");
		
		### Buttons
		$SubmitBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Save_Settings();");
		$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back_Time_Zone();");
		$DeleteBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Delete'], "button", "js_Delete_Settings();", '', '', 0, 'formbutton_alert');
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
			$x .= '<br />'."\n";
			$x .= '<div class="table_board">'."\n";
				$x .= '<table id="html_body_frame" width="100%">'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td>'."\n";
							$x .= '<table class="form_table_v30">'."\n";
								// Start Date
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['SysMgr']['CycleDay']['PeriodStart']['FieldTitle'].'</td>'."\n";
									$x .= '<td>'.$TimezoneStartDate.'</td>'."\n";
								$x .= '</tr>'."\n";
								
								// End Date
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$Lang['SysMgr']['CycleDay']['PeriodEnd']['FieldTitle'].'</td>'."\n";
									$x .= '<td>'.$TimezoneEndDate.'</td>'."\n";
								$x .= '</tr>'."\n";
								
								// Repeat Type
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['SysMgr']['CycleDay']['RepeatType'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= $this->Get_Special_Timetable_Repeat_Type_Selection('RepeatTypeSel', 'RepeatType', $RepeatType, 'js_Changed_RepeatType(this.value);', $PeriodTypeText)."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								
								// Repeat Every
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['SysMgr']['CycleDay']['RepeatEvery'].'</td>'."\n";
									$x .= '<td><div id="RepeatDayDiv"></div></td>'."\n";
								$x .= '</tr>'."\n";
								
								// Special Timetable
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['SysMgr']['CycleDay']['SpecialTimetable'].'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= '<div id="TimetableSelDiv"></div>'."\n";
										$x .= $linterface->Get_Form_Warning_Msg($DivID='TimetableWarningDiv', $Lang['SysMgr']['CycleDay']['jsWarningArr']['SelectTimetable'], $Class='WarningDiv');
										$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['CycleDay']['RemarkSpecialTimetable'].'</span>'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
								
								// Display Colour
								$x .= '<tr>'."\n";
									$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['SysMgr']['CycleDay']['DisplayColour'].'</td>'."\n";
									
									// Hide the textbox (the textbox is only used for initializing the color picker plugin)
									$x .= '<td><input type="text" id="BgColor" name="BgColor" value="'.$BgColor.'" style="display:none;" /></td>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</table>'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
				
				$x .= '<div id="SelectedDatesDiv"></div>'."\n";
				$x .= '<br />'."\n";
				
				$x .= '<div class="tabletextremark">'."\n";
					$x .= $this->Get_Has_Special_Timetable_Indicator().' '.$Lang['SysMgr']['CycleDay']['TheDateHasSpecialTimetableSettingsAlready'];
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";
			$x .= '<br />'."\n";
			
			
			# Buttons
			$x .= '<div class="edit_bottom_v30">'."\n";
				if ($EditMode) {
					$x .= '<div class="tabletextremark" style="float:left;">'.Get_Last_Modified_Remark($LastModifiedDate, '', $LastModifiedBy).'</div>'."\n";
					$x .= '<br style="clear:both;" />'."\n";
				}
				$x .= $SubmitBtn;
				$x .= "&nbsp;";
				$x .= $CancelBtn;
				if ($EditMode) {
					$x .= "&nbsp;";
					$x .= $DeleteBtn;
				}
			$x .= '</div>'."\n";
			
			# Hidden Field
			$x .= '<input type="hidden" id="AcademicYearID" name="AcademicYearID" value="'.$AcademicYearID.'">'."\n";
			$x .= '<input type="hidden" id="TimezoneID" name="TimezoneID" value="'.$TimezoneID.'">'."\n";
			$x .= '<input type="hidden" id="SpecialTimetableSettingsID" name="SpecialTimetableSettingsID" value="'.$SpecialTimetableSettingsID.'">'."\n";
			
		$x .= '</form>'."\n";
		$x .= '<br />'."\n";
			
		return $x;
	}
	
	public function Get_Special_Timetable_Repeat_Type_Selection($ID, $Name, $Selected='', $OnChange='', $TimezoneTypeText='') {
		global $Lang;
		
		$OptionArr = array();
		if ($TimezoneTypeText == 'cycle') {
			$OptionArr['cycle'] = $Lang['SysMgr']['CycleDay']['PeriodType']['HaveCycle'];
		}
		$OptionArr['weekday'] = $Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle'];
		
		$onchange = '';
		if ($OnChange != "")
			$onchange = 'onchange="'.$OnChange.'"';
		
		$selectionTags = ' id="'.$ID.'" name="'.$Name.'" '.$onchange;
		$Selection = getSelectByAssoArray($OptionArr, $selectionTags, $Selected, $all=0, $noFirst=1, $FirstTitle="");
		
		return $Selection;
	}
	
	public function Get_Weekday_Checkboxes_Table($SpecialTimetableSettingsID='') {
		global $Lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		
		if ($SpecialTimetableSettingsID != '') {
			### Get Special Timetable Settings
			$SettingsInfoArr = $this->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsID);
			$RepeatDayArr = Get_Array_By_Key((array)$SettingsInfoArr[$SpecialTimetableSettingsID]['RepeatDayInfo'], 'RepeatDay');
			$numOfRepeatDay = count($RepeatDayArr);
		}
		
		$ChkClass = 'DayChk';
		$ID = 'CheckAllChk';
		$Name = '';
		$OnClick = "Check_All_Options_By_Class('".$ChkClass."', this.checked); js_Clicked_RepeatDay_Checkbox();";
		$thisChecked = ($numOfRepeatDay==7)? true : false;
		$CheckAllChk = $linterface->Get_Checkbox($ID, $Name, '', $thisChecked, $Class='', $Lang['Btn']['All'], $OnClick, $Disabled='');
		
		$x = '';
		$x .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="border-bottom:0px solid #FFFFFF;">'."\n";
					$x .= $CheckAllChk;
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="padding-left:25px; border-bottom:0px solid #FFFFFF;">'."\n";
					for($i=0; $i<7; $i++) {
						$thisIndex = $i;
						
						$thisID = 'DayChk_'.$thisIndex;
						$thisName = 'DayArr[]';
						$thisDisplay = $this->Get_Weekday_Display_Lang($thisIndex);
						$thisOnClick = 'js_Clicked_RepeatDay_Checkbox();';
						
						if ($SpecialTimetableSettingsID != '') {
							if (in_array($thisIndex, (array)$RepeatDayArr)) {
								$thisChecked = true;
							}
							else {
								$thisChecked = false;
							}
						}
						else {
							$thisChecked = false;
						}
						
						$x .= $linterface->Get_Checkbox($thisID, $thisName, $thisIndex, $thisChecked, $ChkClass, $thisDisplay, $thisOnClick);
						$x .= '&nbsp;&nbsp;';
					}
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	// $Weekday starts from 0 and 0 means Sun
	private function Get_Weekday_Display_Lang($Weekday) {
		global $Lang;
		return $Lang['General']['DayType4'][$Weekday];
	}
	
	public function Get_Cycle_Day_Checkboxes_Table($TimezoneID, $SpecialTimetableSettingsID='') {
		global $Lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		
		if ($SpecialTimetableSettingsID != '') {
			### Get Special Timetable Settings
			$SettingsInfoArr = $this->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsID);
			$RepeatDayArr = Get_Array_By_Key((array)$SettingsInfoArr[$SpecialTimetableSettingsID]['RepeatDayInfo'], 'RepeatDay');
			$numOfRepeatDay = count($RepeatDayArr);
		}
		
		$CycleDayDisplayArr = $this->Get_Cycle_Day_By_TimeZone($TimezoneID);
		$numOfCycleDay = count($CycleDayDisplayArr);
		
		$ChkClass = 'DayChk';
		$ID = 'CheckAllChk';
		$Name = '';
		$OnClick = "Check_All_Options_By_Class('".$ChkClass."', this.checked); js_Clicked_RepeatDay_Checkbox()";
		$thisChecked = ($numOfRepeatDay==$numOfCycleDay)? true : false;
		$CheckAllChk = $linterface->Get_Checkbox($ID, $Name, '', $thisChecked, $Class='', $Lang['Btn']['All'], $OnClick, $Disabled='');
		
		$x = '';
		$x .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="border-bottom:0px solid #FFFFFF;">'."\n";
					$x .= $CheckAllChk;
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="padding-left:25px; border-bottom:0px solid #FFFFFF;">'."\n";
					for($i=0; $i<$numOfCycleDay; $i++) {
						$thisIndex = $i + 1;
						
						$thisID = 'DayChk_'.$thisIndex;
						$thisName = 'DayArr[]';
						$thisDisplay = $CycleDayDisplayArr[$thisIndex];
						$thisOnClick = 'js_Clicked_RepeatDay_Checkbox();';
						
						if ($SpecialTimetableSettingsID != '') {
							if (in_array($thisIndex, (array)$RepeatDayArr)) {
								$thisChecked = true;
							}
							else {
								$thisChecked = false;
							}
						}
						else {
							$thisChecked = false;
						}
						
						$x .= $linterface->Get_Checkbox($thisID, $thisName, $thisIndex, $thisChecked, $ChkClass, $thisDisplay, $thisOnClick);
						$x .= '&nbsp;&nbsp;';
					}
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	public function Get_Special_Timetable_Date_Selection_Table($TimezoneID, $RepeatType, $RepeatDayArr, $SpecialTimetableSettingsID='', $IsInitial=0) {
		global $Lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		
		$IsEditMode = ($SpecialTimetableSettingsID=='')? false : true;
		
		$TimezoneSpecialDateArr = $this->Get_Special_Timetable_Settings_Date_Info($SpecialTimetableSettingsIDArr='', $TimezoneID);
		
		if ($SpecialTimetableSettingsID != '') {
			### Get Special Timetable Settings
			$SettingsInfoArr = $this->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsID);
			$ThisSettingsSpecialDateArr = Get_Array_By_Key((array)$SettingsInfoArr[$SpecialTimetableSettingsID]['SpecialDateInfo'], 'SpecialDate');
		}
		
		$DateArr = $this->Get_Dates_By_TimeZone($TimezoneID, $RepeatDayArr, $ReturnAssocWithDay=1, $RepeatType);
		$numOfDate = count($DateArr);
		
		$HasSpecialTimetableIndicator = $this->Get_Has_Special_Timetable_Indicator();
		
		
		$x = '';
		$x .= '<div id="DateNavigationDiv">'.$linterface->GET_NAVIGATION2_IP25($Lang['SysMgr']['CycleDay']['SelectedDates']).'</div>'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		$x .= $linterface->Get_Form_Warning_Msg($DivID='SelectDateWarningDiv', $Lang['SysMgr']['CycleDay']['jsWarningArr']['SelectDate'], $Class='WarningDiv');
		$x .= '<table class="common_table_list_v30">'."\n";
			$x .= '<col align="center" style="width:10px">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="left">'."\n";
			$x .= '<col align="center" style="width:10px;">'."\n";
			
			# table head
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>#</th>'."\n";
					$x .= '<th>'.$Lang['General']['Date'].'</th>'."\n";
					if ($RepeatType == 'cycle') {
						$x .= '<th>'.$Lang['SysMgr']['CycleDay']['PeriodType']['HaveCycle'].'</th>'."\n";
					}
					else if ($RepeatType == 'weekday') {
						$x .= '<th>'.$Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle'].'</th>'."\n";
					}
					$thisChecked = ($IsEditMode)? false : true;
					$x .= '<th>'.$linterface->Get_Checkbox($ID='', $Name='checkmaster', $Value='', $thisChecked, $Class='', $Display='', "js_Clicked_Select_All_Date_Checkbox(this.checked);").'</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$x .= '<tbody>'."\n";
				
			if ($numOfDate == 0) {
				$x .= '<tr><td colspan="100%" style="text-align:center;">'.$Lang['SysMgr']['CycleDay']['NoDateMessage'].'</td></tr>'."\n";
			}
			else
			{
				$DateCount = 0;
				foreach ((array)$DateArr as $thisDate => $thisDay)
				{
					if ($RepeatType == 'weekday') {
						$thisDay = $this->Get_Weekday_Display_Lang($thisDay);
					}
					
					$thisDateDisplay = $thisDate;
					$thisTrClass = '';
					if ($IsEditMode) {
						### Edit Special Timetable Settings
						
						if (in_array($thisDate, (array)$ThisSettingsSpecialDateArr)) {
							// The date is in this Special Timetable Settings
							$thisTrClass = ($IsInitial)? '' : 'tabletextremark';
							$thisChecked = ($IsInitial)? true : false;
							$thisDisabled = false;
						}
						else {
							// The date is not in thie Special Timetable Settings
							if (isset($TimezoneSpecialDateArr[$thisDate])) {
								// The date is in another
								$thisDateDisplay = $HasSpecialTimetableIndicator.$thisDate;
								$thisTrClass = 'tabletextremark';
								$thisChecked = false;
								$thisDisabled = true;
							}
							else {
								$thisTrClass = 'tabletextremark';
								$thisChecked = false;
								$thisDisabled = false;
							}
						}
					}
					else {
						### New Special Timetable Settings
						
						// Disable if the date has special timetable already
						if (isset($TimezoneSpecialDateArr[$thisDate])) {
							$thisDateDisplay = $HasSpecialTimetableIndicator.$thisDate;
							$thisTrClass = 'tabletextremark';
							$thisChecked = false;
							$thisDisabled = true;
						}
						else {
							$thisChecked = true;
							$thisDisabled = false;
						}
					}
					
					
					$thisName = '';
					$thisID = 'DateChk_'.$thisDate;
					$thisName = 'DateArr[]';
					$thisClass = 'DateChk';
					$thisCheckbox = $linterface->Get_Checkbox($thisID, $thisName, $thisDate, $thisChecked, $thisClass, $Display='', 'js_Clicked_Selected_Date_Checkbox(this.id);', $thisDisabled);
					
					$thisRowID = 'SelectDateTr_'.$thisDate;
					$x .= '<tr id="'.$thisRowID.'" class="'.$thisTrClass.'">'."\n";
						$x .= '<td>'.(++$DateCount).'</td>'."\n";
						$x .= '<td>'.$thisDateDisplay.'</td>'."\n";
						$x .= '<td>'.$thisDay.'</td>'."\n";
						$x .= '<td>'.$thisCheckbox.'</td>'."\n";
					$x .= '</tr>'."\n";
				}
			}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	public function Get_Edit_Timezone_Option_Layer_Table($TimezoneID, $Date) {
		global $Lang;
		
		$TimezoneInfoArr = $this->returnPeriods_NEW($TimezoneID);
		$PeriodStart = $TimezoneInfoArr[0]['PeriodStart'];
		
		$DateSpecialSettingsArr = $this->Get_Special_Timetable_Settings_Date_Info($SpecialTimetableSettingsIDArr='', $TimezoneID, $Date);
		
		$x = '';
		$x .= '<div class="content_top_tool">'."\n";
			$x .= '<div class="Conntent_tool">'."\n";
				// Timezone option
				if ($Date == $PeriodStart) {
					$x .= '<div>'."\n";
						$x .= '<div><b>'.$Lang['SysMgr']['CycleDay']['SettingTitle'].'</b></div>'."\n";
						$x .= '<div>'."\n";
							$x .= '<div style="float:left;">&nbsp;&nbsp;&nbsp;&nbsp;</div>'."\n";
							$x .= '<a href="#TB_inline?height=400&width=600&inlineId=FakeLayer" title="'.$Lang['Btn']['Edit'].'" class="thickbox edit_content" onClick="editPeriodRange(\''.$TimezoneID.'\');">';
								$x .= $Lang['Btn']['Edit'];
							$x .= '</a>'."\n";
						$x .= '</div>'."\n";
						$x .= '<br />'."\n";
						$x .= '<br />'."\n";
					$x .= '</div>'."\n";
				}
				
				// Special Timetable
				$x .= '<div>'."\n";
					$x .= '<div><b>'.$Lang['SysMgr']['CycleDay']['SpecialTimetable'].'</b></div>'."\n";
					if ($Date == $PeriodStart) {
						// new settings
						$x .= '<div>'."\n";
							$x .= '<div style="float:left;">&nbsp;&nbsp;&nbsp;&nbsp;</div>'."\n";
							$x .= '<a href="javascript:void(0);" title="'.$Lang['SysMgr']['CycleDay']['AddNewSettings'].'" class="add" onclick="js_Go_Special_Timetable_Settings(\''.$TimezoneID.'\');">';
								$x .= $Lang['SysMgr']['CycleDay']['AddNewSettings'];
							$x .= '</a>'."\n";
						$x .= '</div>'."\n";
						$x .= '<br style="clear:both;" />'."\n";
					}
					
					if ($DateSpecialSettingsArr[$Date]['FirstDateInSpecialSettings']) {
						$SpecialTimetableSettingsID = $DateSpecialSettingsArr[$Date]['SpecialTimetableSettingsID'];
						
						// edit settings
						$x .= '<div>'."\n";
							$x .= '<div style="float:left;">&nbsp;&nbsp;&nbsp;&nbsp;</div>'."\n";
								$x .= '<a href="javascript:void(0);" title="'.$Lang['SysMgr']['CycleDay']['EditThisSettings'].'" class="edit_content" onclick="js_Go_Special_Timetable_Settings(\''.$TimezoneID.'\', \''.$SpecialTimetableSettingsID.'\');">';
									$x .= $Lang['SysMgr']['CycleDay']['EditThisSettings'];
								$x .= '</a>'."\n";
						$x .= '</div>'."\n";
						$x .= '<br style="clear:both;" />'."\n";
					}
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	private static function Get_Has_Special_Timetable_Indicator() {
		return '<span class="tabletextrequire">#</span>'; 
	}
	
	private static function Get_Edit_Icon_Remarks() {
		global $image_path, $LAYOUT_SKIN, $Lang;
		
		$x = '';
		$x .= '<div class="tabletextremark"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit.gif" style="border:0;"> '.$Lang['SysMgr']['CycleDay']['FirstDayOfTimezone'].'</div>'."\n";
		$x .= '<div class="tabletextremark"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_special.gif" style="border:0;"> '.$Lang['SysMgr']['CycleDay']['FirstDayOfSpecialTimetableSettings'].'</div>'."\n";
		$x .= '<div class="tabletextremark"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_both_special.gif" style="border:0;"> '.$Lang['SysMgr']['CycleDay']['BothFirstDayOfTimezoneAndSpecialTimetableSettings'].'</div>'."\n";
		
		return $x;
	}
}
?>