<?php
/*
 * 	Modifying: 
 * 
 * 	Log
 * 
 *  2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 * 
 * 	2016-01-06 [Cameron]
 *		- remove pass by reference in retrieve_favourite_list_html() and retrieve_favouritepage_html() to support php5.4+
 *		- replace session_unregister to session_unregister_intranet for supporting php5.4+ 
 * 
 */
	include_once('libdb.php');
	
	class libelibvideo extends libdb{
  
  		var $user_id;
  		
  		function libelibvideo($UserID,$SchoolCode, $SchoolUserID, $isLogin=false, $loginArr=array()){
			$this->libdb();
			$this->schoolCode = $SchoolCode;	
			$this->schoolUserID = $SchoolUserID;			
			
			$this->set_basic_login();
			
			$redirectToFrontPage = false;
			
			if($isLogin){ 
				session_unregister_intranet("ELIB_VIDEO_SchoolCode");
				session_unregister_intranet("ELIB_VIDEO_SchoolUserID");
				session_unregister_intranet("ELIB_VIDEO_UserID");
				$_SESSION["ELIB_VIDEO_SchoolCode"] = $this->schoolCode;
				$_SESSION["ELIB_VIDEO_SchoolUserID"] = $this->schoolUserID;
				$_SESSION["ELIB_VIDEO_UserID"] = $this->userID;		
				if($this->package != ''){
					
					if($this->userID != ''){
						$this->update_user($loginArr);						
						$redirectToFrontPage = true;
					}else{
						
						foreach($loginArr as $key=>$value){
							$form_html .= '<input name="'.$key.'"  value="'.$value.'" type="hidden"/>';
						}
						
						$html = '<body onLoad="document.MainForm.submit();"><form name="MainForm" id="MainForm" action="" method="post">' .
									'<input name="Mode"  value="content" type="hidden"/>' .
				        			'<input name="Task"  value="termspage" type="hidden"/>	' .
				        			$form_html .' 
									</form></body>';
						echo($html);
						//$this->add_user($loginArr);
						exit();
					}
				}else{
					$html = '<body onLoad="document.MainForm.submit();"><form name="MainForm" id="MainForm" action="" method="post">' .
						'<input name="Mode"  value="content" type="hidden"/>' .
	        			'<input name="Task"  value="blankpage" type="hidden"/>	
						</form></body>';
					echo($html);
				}
			}else{
				if($this->package == ''){
//					$html = '<body onLoad="document.MainForm.submit();"><form name="MainForm" id="MainForm" action="" method="post">' .
//						'<input name="Mode"  value="content" type="hidden"/>' .
//	        			'<input name="Task"  value="blankpage" type="hidden"/>	
//						</form></body>';
//					echo($html);
				}
			}
			
			$sql = "Select Count(*) From ELIB_VIDEO_CATEGORY";//
			$category_count = current($this->returnVector($sql));
			
			if($category_count == 0){
				$sql = "INSERT INTO ELIB_VIDEO_CATEGORY (DateInput,CategoryName,CategoryNameChi) Values " .
						"(NOW(),'Agriculture', '農業'),(NOW(),'Animals', '動物'),(NOW(),'Art', '藝術')," .
						"(NOW(),'Conflict', '衝突'),(NOW(),'Conservation', '保育'),(NOW(),'Crime', '罪案')," .
						"(NOW(),'Culture', '文化'),(NOW(),'Economy', '經濟'),(NOW(),'Education', '教育')," .
						"(NOW(),'Environment', '環境'),(NOW(),'Food', '食物'),(NOW(),'Globalization', '全球化')," .
						"(NOW(),'Health', '健康'),(NOW(),'Industry', '工業'),(NOW(),'Lifestyle', '生活方式')," .
						"(NOW(),'Politics', '政治'),(NOW(),'Science & Technology', '科學與科技'),(NOW(),'Society', '社會')," .
						"(NOW(),'Sports', '體育'),(NOW(),'Transport', '交通')";
				$result = $this->db_db_query($sql);
			}
			 
			$sql = "Select Count(*) From ELIB_VIDEO_LOCALITY";// 
			$locality_count = current($this->returnVector($sql));
			
			if($locality_count == 0){
				$sql = "INSERT INTO ELIB_VIDEO_LOCALITY (DateInput,LocalityName,LocalityNameChi) VALUES" .
						" (NOW(),'Afghanistan','阿富汗'),(NOW(),'Algeria','阿爾及利亞'),(NOW(),'Angola','安哥拉')," .
						"(NOW(),'Antarctica','南極'),(NOW(),'Argentina','阿根廷'),(NOW(),'Australia','澳洲')," .
						"(NOW(),'Austria','奧地利'),(NOW(),'Bangladesh','孟加拉'),(NOW(),'Bhutan','不丹')," .
						"(NOW(),'Bolivia','玻利維亞'),(NOW(),'Bosinia and Hercegovina','波士尼亞與赫塞哥維納')," .
						"(NOW(),'Brazil','巴西'),(NOW(),'Cambodia','柬埔寨'),(NOW(),'Canada','加拿大')," .
						"(NOW(),'Chile','智利'),(NOW(),'China','中國'),(NOW(),'Colombia','哥倫比亞')," .
						"(NOW(),'Cuba','古巴'),(NOW(),'Cyprus','塞浦路斯'),(NOW(),'Czech Republic','捷克')," .
						"(NOW(),'Democratic Republic of the Congo','剛果民主共和國'),(NOW(),'Ecuador','厄瓜多爾')," .
						"(NOW(),'Egypt','埃及'),(NOW(),'Finland','芬蘭'),(NOW(),'France','法國'),(NOW(),'Gaza','加沙地帶')," .
						"(NOW(),'Germany','德國'),(NOW(),'Greece','希臘'),(NOW(),'Hong Kong','香港'),(NOW(),'Hungary','匈牙利')," .
						"(NOW(),'India','印度'),(NOW(),'Indonesia','印尼'),(NOW(),'Iraq','伊拉克'),(NOW(),'Israel','以色列')," .
						"(NOW(),'Italy','意大利'),(NOW(),'Ivory Coast','象牙海岸'),(NOW(),'Japan','日本'),(NOW(),'Kenya','肯雅')," .
						"(NOW(),'Libya','利比亞'),(NOW(),'Madagascar','馬達加斯加'),(NOW(),'Mexico','墨西哥'),(NOW(),'Middle East','中東')," .
						"(NOW(),'Morroco','摩洛哥'),(NOW(),'Myanmar','緬甸'),(NOW(),'Namibia','納米比亞'),(NOW(),'Nepal','尼泊爾')," .
						"(NOW(),'New Zealand','紐西蘭'),(NOW(),'Nigeria','尼日利亞'),(NOW(),'North Korea','北韓')," .
						"(NOW(),'Norway','挪威'),(NOW(),'Pakistan','巴基斯坦'),(NOW(),'Poland','波蘭'),(NOW(),'Portugal','葡萄牙')," .
						"(NOW(),'Romania','羅馬尼亞'),(NOW(),'Russia','俄羅斯'),(NOW(),'Senegal','塞內加爾'),(NOW(),'Sierra Leone','塞拉利昂')," .
						"(NOW(),'Singapore','新加坡'),(NOW(),'Somalia','索馬里'),(NOW(),'South Africa','南非'),(NOW(),'South Korea','南韓')," .
						"(NOW(),'Spain','西班牙'),(NOW(),'Switzerland','瑞士'),(NOW(),'Syria','敘利亞'),(NOW(),'Thailand','泰國')," .
						"(NOW(),'The Philippines','菲律賓'),(NOW(),'Tunisia','突尼斯'),(NOW(),'Turkey','土耳其'),(NOW(),'Uganda','烏干達')," .
						"(NOW(),'UK','英國'),(NOW(),'Uruguay','烏拉圭'),(NOW(),'USA','美國'),(NOW(),'Vatican','梵蒂岡'),(NOW(),'Vietnam','越南'),(NOW(),'Yemen','葉門')";
				$result = $this->db_db_query($sql);
			}
			
			$sql = "Select Count(*) From ELIB_VIDEO_LOCALITY WHERE LocalityName = 'Mozambique' ";// 
			$locality_count_Mozambique = current($this->returnVector($sql));
			
			if($locality_count_Mozambique == 0){
				$sql = "INSERT INTO ELIB_VIDEO_LOCALITY (DateInput,LocalityName,LocalityNameChi) VALUES" .
						" (NOW(),'Mozambique','莫三比克')";
				$result = $this->db_db_query($sql);
			}			
			
			if($redirectToFrontPage){						
				
				$html = 'Now Loading......<body onLoad="document.MainForm.submit();"><form name="MainForm" id="MainForm" action="" method="post">		
						</form></body>';
				echo($html);
			}
			
			
//			var_dump($this->schoolCode);
//			var_dump($this->schoolUserID);	
//			var_dump($this->userID);		
//			var_dump($this->package);	
		}		
		
		function add_user($loginArr){
			return false;
			$sql = " INSERT INTO INTRANET_USER (EnglishName,ChineseName," .
					" NickName,ClassLevel,ClassName,ClassNumber,LastUsed,UserEmail, SchoolUserID, SchoolCode ) VALUES " .
					" ('".$loginArr['UserNameEng']."',N'".$loginArr['UserNameChi']."','".$loginArr['UserNickname']."'" .
					",'".$loginArr['ClassLevel']."','".$loginArr['ClassName']."','".$loginArr['ClassNumber']."'" .
					",'".$loginArr['TimeStamp']."','".$loginArr['UserEmail']."','".$this->schoolUserID."' ,'".$this->schoolCode."' ) ";
			$result = $this->db_db_query($sql);
			
			$sql = " SELECT UserID FROM INTRANET_USER WHERE SchoolUserID = '".$this->schoolUserID."' AND SchoolCode = '".$this->schoolCode."'";
			
			$this->userID = current($this->returnVector($sql));
		}
		
		function update_user($loginArr){
//			$sql = " UPDATE INTRANET_USER Set EnglishName = '".$loginArr['UserNameEng']."',ChineseName = '".$loginArr['UserNameChi']."' ," .
//					" NickName = '".$loginArr['UserNickname']."', ClassLevel = '".$loginArr['ClassLevel']."', " .
//					" ClassName = '".$loginArr['ClassName']."', ClassNumber = '".$loginArr['ClassNumber']."', " .
//					" LastUsed = '".$loginArr['TimeStamp']."' ,UserEmail = '".$loginArr['UserEmail']."', " .
//					" SchoolUserID = '".$this->schoolUserID."', SchoolCode = '".$this->schoolCode."' WHERE UserID = ".$this->userID;
//			$result = $this->db_db_query($sql);	
			
			$sql = "INSERT INTO ELIB_VIDEO_USER_LOG (UserID,DateInput, LoginCount ) VALUES ('".$this->userID."','".$loginArr['TimeStamp']."',1) " .
					" ON DUPLICATE KEY UPDATE LoginCount =LoginCount + 1, DateInput = '".$loginArr['TimeStamp']."' ";
			$result = $this->db_db_query($sql);	
			
			//echo($sql);		
		}
		
		function set_basic_login(){
//			$sql = " SELECT UserID FROM INTRANET_USER WHERE SchoolUserID = ".$this->schoolUserID." AND SchoolCode = ".$this->schoolCode;
			$sql = " SELECT UserID FROM INTRANET_USER WHERE UserID = '".$this->schoolUserID."'";
			
			$this->userID = current($this->returnVector($sql));
			
			$sql = " SELECT Package FROM ELIB_VIDEO_SETTING WHERE SchoolCode = '".$this->schoolCode."'";
			
			$package_arr = explode(",", current($this->returnVector($sql)));
			
			$package = "";
			
			foreach($package_arr as $key=>$value){
				if($package == "") $package .= "'".$value."'";
				else $package .= ",'".$value."'";
			}			
			
			$this->package = $package;
		}
		
		/////////////////////////////////////////////////////////////////////////////////////
		///////////////////////// Common Function
		/////////////////////////////////////////////////////////////////////////////////////
		
		function convert_date_YMD($date){return date("Y-m-d", strtotime($date)); }				
		
		function retrieve_tag_by_keyword($keyword){
			if(is_array($keyword)){
				$keyword  = "'".str_replace( ",",  "','",implode(",", $keyword)). "'";
			}
			$sql = " SELECT TagID FROM ELIB_VIDEO_TAG WHERE TagName IN ( $keyword ) ";
			$result = $this->returnVector($sql);
			return $result;
		}		
		
		function retrieve_tag_html($videoID){
			$sql = " SELECT TagID, TagName FROM ELIB_VIDEO_TAG WHERE TagID IN (SELECT TagID FROM ELIB_VIDEO_VIDEO_TAG WHERE VideoID = '$videoID') ";
			$result = $this->returnArray($sql);
			
			foreach($result as $key => $item){
				$html .='<a value="'.$item["TagID"].'" href="javascript:void();" onclick="$(\'#searchpage_tag\').attr(\'value\', \''.$item["TagName"].'\'); document.searchpage_form.submit();">'.$item["TagName"].'</a>';
			}
			return $html;
		}
		
		function handle_quote_sql ($value){
			$value = str_replace("'", "''",$value);
			return $value;
		}
		
		function retrieve_video_info($category, $locality, $keyword, $orderByPublishedTime,$orderByViewCount,$filter){
			
			if($filter == "favourite")	$favourite_filter = " INNER JOIN ELIB_VIDEO_BOOKMARK AS b ON v.VideoID = b.VideoID AND b.UserID = '".$this->userID."'"; 
			else $favourite_filter = "";
			
			
			///////////////////////////////////////
			//$condition = " WHERE vpac.Package ='".$this->package."' AND (vct.SchoolCode = ".$this->schoolCode." OR ViewCount =0) ";
			$condition = " WHERE vpac.Package IN ( ".$this->package." ) ";
			
			if($keyword !=""){
				$keyword_condition = " false ";
				foreach($keyword as $key=>$value){
					$keyword_condition .= " OR v.Title LIKE '%".$this->handle_quote_sql($value)."%' OR v.Summary LIKE '%".$this->handle_quote_sql($value)."%' OR v.Script LIKE '%".$this->handle_quote_sql($value)."%' ";
				}
				
				$match_tag_id = implode(",", $this->retrieve_tag_by_keyword($keyword));
				
				if($match_tag_id != "") {
					$left_join_tag_table = " LEFT JOIN ELIB_VIDEO_VIDEO_TAG AS vtag ON v.VideoID = vtag.VideoID ";
					$tag_condition = "OR vtag.TagID IN ($match_tag_id)";
				}
				$condition .= " AND ( $keyword_condition $tag_condition ) ";				
			}
			
						
			if($category !=""){		
				if(is_array($category)){
					$category  = implode(",", $category);
				}	
				$condition .= " AND vcat.CategoryID IN ($category) ";
				
				$column_categoryID = " vcat.CategoryID AS CategoryID, ";
				$table_category = " LEFT JOIN ELIB_VIDEO_VIDEO_CATEGORY AS vcat ON v.VideoID = vcat.VideoID ";
			}
			if($locality !=""){
				if(is_array($locality)){
					$locality  = implode(",", $locality);
				}	
				$condition .= " AND vloc.LocalityID IN ($locality) ";
				
				$column_localityID = " vloc.LocalityID AS LocalityID, ";
				$table_locality = " LEFT JOIN ELIB_VIDEO_VIDEO_LOCALITY AS vloc ON v.VideoID = vloc.VideoID ";
			}
			//////////////////////////////////////
			
			$sql = "SELECT v.VideoID AS VideoID, v.VideoLength AS VideoLength, v.Title AS Title,
					v.Summary AS Summary, v.Reference AS Reference, v.Script AS Script, v.Thumbnail AS Thumbnail,
					v.PublishedTime AS PublishedTime, vpac.Package AS Package, $column_categoryID $column_localityID 

					CASE WHEN vct.SchoolCode != '".$this->schoolCode."' OR vct.ViewCount IS NULL THEN 0 ELSE vct.ViewCount END AS ViewCount,
					vct.SchoolCode AS ViewCountSchoolCode		
		
					FROM ELIB_VIDEO_VIDEO AS v
					$left_join_tag_table
					LEFT JOIN ELIB_VIDEO_VIDEO_PACKAGE AS vpac ON v.VideoID = vpac.VideoID
					$table_category
					$table_locality
					LEFT JOIN ELIB_VIDEO_VIEW_COUNT AS vct ON v.VideoID = vct.VideoID $favourite_filter ";			
			
			///////////////////////////////////////////////
			$order = " ORDER BY ";
			
			if($orderByPublishedTime)
				$order .= " PublishedTime DESC, VideoID DESC ";
			else if ($orderByViewCount)
				$order .= " ViewCount DESC, VideoID DESC ";
			else if($filter == "favourite")
				$order .= " b.BookmarkID DESC ";
			else $order .= " VideoID DESC ";			
			///////////////////////////////////////////////	
			
			$sql .= $condition.$order;
			$result = $this->returnArray($sql);	
			
			$video_array = array();
			$video_key_pair = array();
			
			foreach($result as $key=>$item){				
				
				if(!is_int($video_key_pair[$item["VideoID"]])){
					$video_array[] = $item;
					$video_key_pair[$item["VideoID"]] = count($video_array) - 1;
				}else if($item["ViewCountSchoolCode"] == $this->schoolCode){
					$video_array[$video_key_pair[$item["VideoID"]]] = $item;
				}
			}		
			//var_dump($sql);
			//var_dump($video_key_pair);
			return $video_array;
		}		
		
		
		function retrieve_category_info($Lang){
			
			$lang_chi = $_SESSION["ELIB_VIDEO_Lang"] == "b5" ? ", cat.CategoryNameChi " : "" ;
			
			$sql = "SELECT  cat.CategoryID AS CategoryID , cat.CategoryName $lang_chi AS CategoryName,
					CASE WHEN vcat.CategoryCount IS NULL THEN 0 ELSE vcat.CategoryCount END AS CategoryCount 
					
					FROM ELIB_VIDEO_CATEGORY AS cat
					
					LEFT JOIN (SELECT CategoryID, COUNT(VideoCategoryID) AS CategoryCount FROM ELIB_VIDEO_VIDEO_CATEGORY 
							 AS vcati LEFT JOIN ELIB_VIDEO_VIDEO_PACKAGE AS vpac ON vcati.VideoID = vpac.VideoID WHERE vpac.Package IN ( ".$this->package." )
							 GROUP BY CategoryID) AS vcat
					
							 ON cat.CategoryID = vcat.CategoryID ORDER BY cat.CategoryName";
			$result = $this->returnArray($sql);	
			
			return $result;
		}
		
		function retrieve_locality_info($Lang){
			
			$lang_chi = $_SESSION["ELIB_VIDEO_Lang"] == "b5" ? ", loc.LocalityNameChi " : "" ;
			
			$sql = "SELECT loc.LocalityID AS LocalityID , loc.LocalityName $lang_chi AS LocalityName,
					CASE WHEN vloc.LocalityCount IS NULL THEN 0 ELSE vloc.LocalityCount END AS LocalityCount 
					
					FROM ELIB_VIDEO_LOCALITY AS loc
					
					LEFT JOIN (SELECT LocalityID, COUNT(VideoLocalityID) AS LocalityCount FROM ELIB_VIDEO_VIDEO_LOCALITY 
							 AS vloci LEFT JOIN ELIB_VIDEO_VIDEO_PACKAGE AS vpac ON vloci.VideoID = vpac.VideoID WHERE vpac.Package IN ( ".$this->package." ) 
							 GROUP BY LocalityID) AS vloc
							 ON loc.LocalityID = vloc.LocalityID ORDER BY loc.LocalityName";			
			
			$result = $this->returnArray($sql);	
			return $result;
		}
		
		
		function retrieve_total_video_count(){
			$sql = "SELECT COUNT(VideoID) FROM ELIB_VIDEO_VIDEO_PACKAGE WHERE Package IN ( ".$this->package." )";
			
			return current($this->returnVector($sql));
		}
		
		function retrieve_favourite_info($userID){
			$sql =" SELECT v.VideoID AS VideoID, v.VideoLength AS VideoLength, v.Title AS Title,
					v.Summary AS Summary, v.Reference AS Reference, v.Script AS Script, v.Thumbnail AS Thumbnail,
					v.PublishedTime AS PublishedTime,
					
					CASE WHEN vct.ViewCount IS NULL THEN 0 ELSE vct.ViewCount END AS ViewCount
							
					FROM ELIB_VIDEO_VIDEO AS v
					LEFT JOIN ELIB_VIDEO_VIEW_COUNT AS vct ON v.VideoID = vct.VideoID AND vct.SchoolCode = '".$this->schoolCode."'
					
					INNER JOIN  ELIB_VIDEO_BOOKMARK AS b ON v.VideoID = b.VideoID 
					WHERE b.UserID = '$userID' ORDER BY BookmarkID DESC ";
			return $this->returnArray($sql);
		}
		
		function retrieve_ifFavouriteVideo($videoID, $userID){
			$sql = "SELECT * FROM ELIB_VIDEO_BOOKMARK WHERE VideoID = '$videoID' AND UserID = '$userID' ";
			return current($this->returnVector($sql));
		}
		
		function insert_favourite($videoID, $userID){
			$sql = "INSERT IGNORE INTO ELIB_VIDEO_BOOKMARK (VideoID, UserID, DateInput ) Values ('$videoID', '$userID', NOW())";
			echo($sql);
			return $this->db_db_query($sql);
		}
		
		function delete_favourite($videoID, $userID){
			$sql = "DELETE FROM ELIB_VIDEO_BOOKMARK  WHERE VideoID = '$videoID' AND UserID = '$userID'";
			
			return $this->db_db_query($sql);
		}
		
		/////////////////////////////////////////////////////////////////////////////////////
		///////////////////////// Common Function END
		/////////////////////////////////////////////////////////////////////////////////////
		
		/////////////////////////////////////////////////////////////////////////////////////
		///////////////////////// Front Page
		/////////////////////////////////////////////////////////////////////////////////////
		
		function retrieve_category_list_html($Lang){
			$result = $this->retrieve_category_info($Lang);	
			
			$categoryCount = count($result);
			
			$allCount = $this->retrieve_total_video_count();			
			$html = '';
			
			for($i=0; $i <($categoryCount); $i++ ){				
				$item = $result[$i];		
				$html .= '<li><a class="category_item" value="'.$item["CategoryID"].'" href="javascript: void(0)" >'.$item["CategoryName"].' <em>('.$item["CategoryCount"].')</em></a></li>';
			}			
			
			$html = '<ul><li class="selected_cat"><a class="category_item" value="" href="javascript: void(0)" class="cat_all">'.$Lang['FrontPage']['All'].' <em>('.$allCount.')</em></a></li>'.$html;
			
            $html .= '</ul>';
            
            return $html;
		}
		
		function retrieve_video_category_list_html($category,$orderByPublishedTime,$orderByViewCount,$pageCount,$pageNumber,$viewMode, &$itemTotalNumber,$Lang){
			$result = $this->retrieve_video_info($category, "","",$orderByPublishedTime,$orderByViewCount, "");	
			$itemTotalNumber = count($result);
			
			if($itemTotalNumber<=0){				
				$html = '<div title="'.$Lang['Common']['NoRecordsFound'].'" id="video_item">'.$Lang['Common']['NoRecordsFound'].'</div>';
				$html .= '<div id="video_page"><p class="spacer"></p>' .
            		'<input id="itemTotalNumber"   value="0" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="1" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$pageCount.'" type="hidden" /></div>';
            	return $html;
			}
			
			$html = "";
			for($i=$pageCount * ($pageNumber-1); $i <($pageCount * $pageNumber) && $i < $itemTotalNumber; $i++ ){				
				$item = $result[$i];	
				if($viewMode == 1){
					$html .= '<a title="'.$item["Title"].'" class="video" value="'.$item["VideoID"].'" href="javascript:void(0)">
                               	<span class="video_thumb" style="background-size: 100%;background-image:url(thumbnail/'.$item["Thumbnail"].'.jpg)"><em></em></span>
                                    <h1>'.$item["Title"].'</h1>
                                    <span class="video_viewed">'.$Lang['Common']['Views'].''.$item["ViewCount"].''.$Lang['Common']['ViewsEnd'].'</span>
                                    <span class="video_date">'.$this->convert_date_YMD($item["PublishedTime"]).'</span>
                                </a>';
				}else if($viewMode == 2){
					$html .= '<a title="'.$item["Title"].'" class="video" value="'.$item["VideoID"].'" href="javascript:void(0)">
                                	<span class="video_thumb" style="background-size: 100%;background-image:url(thumbnail/'.$item["Thumbnail"].'.jpg)"><em></em></span>
                                    <h1>'.$item["Title"].'</h1>
                                    <span class="video_viewed">'.$Lang['Common']['Views'].''.$item["ViewCount"].''.$Lang['Common']['ViewsEnd'].'</span>
                                   <span class="video_date">'.$this->convert_date_YMD($item["PublishedTime"]).'</span>
                                     <div class="view_brief_summary">
                                    	'.$item["Summary"].'                                    </div>
                                </a>';
				}		
				
			}			
            $html .='<form method="GET" action="" name="videopage_form" id="videopage_form">' .
            		'<input name="Mode"  value="content" type="hidden"/>' .
            		'<input name="Task"  value="videopage" type="hidden"/>' .
            		'<input name="videoID"  value="" type="hidden"/>' .
            		'<input name="filter"  value="category" type="hidden"/>' .
            		'<input name="category"  value="'.$category.'" type="hidden" />' .
            		'<input name="orderByPublishedTime"  value="'.$orderByPublishedTime.'" type="hidden"/>' .
            		'<input name="orderByViewCount"  value="'.$orderByViewCount.'" type="hidden" />' .
            		'</form>';
            		
            if($viewMode == 2)	
            	$html = '<div class="view_list_detail">'.$html.'</div>';
            	
            return $html;
		}
		
		function retrieve_video_category_frontpage_html($category,$orderByPublishedTime,$orderByViewCount,$itemCount,$pageNumber,$viewMode,$Lang){			
			
			$itemTotalNumber = 0;
				
			$html = '<div id="video_item">';
			$html .= $this->retrieve_video_category_list_html($category,$orderByPublishedTime,$orderByViewCount,$itemCount,$pageNumber,$viewMode, $itemTotalNumber,$Lang);
			$html .= '</div>';	
			
			if($itemTotalNumber<=0){				
				$html = '<div id="video_item">'.$Lang['Common']['NoRecordsFound'].'</div>';
				$html .= '<div id="video_page"><p class="spacer"></p>' .
            		'<input id="itemTotalNumber"   value="0" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="1" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$itemCount.'" type="hidden" /></div>';
            	return $html;
			}
			
			$pageTotalNumber = ceil($itemTotalNumber / $itemCount);
			
			$html_page = '';
			for($i = 1; $i <=$pageTotalNumber; $i++ ){
				if($i == 1)
					$html_page .= '<option selected value="'.$i.'">'.$i.'</option>';
				else $html_page .= '<option  value="'.$i.'">'.$i.'</option>';
			}
			
			if($itemTotalNumber > $itemCount * $pageNumber)
				$itemCurrentNumber = $itemCount * $pageNumber;
			else $itemCurrentNumber = $itemTotalNumber;
			
            $html .='<div id="video_page"><p class="spacer"></p> ' .
            		'<input id="itemTotalNumber"   value="'.$itemTotalNumber.'" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="'.$pageNumber.'" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$itemCount.'" type="hidden" />
	                    <div class="common_table_bottom">
							<div id="video_page_record" class="record_no">'.$Lang['Common']['Records'].' <span id="video_page_item_floor">1</span> ' .
									'- <span id="video_page_item_ceil">'.$itemCurrentNumber.'</span>, ' .
											''.$Lang['FrontPage']['Total'].' <span id="video_page_item_total">'.$itemTotalNumber.'</span></div>
								<div class="page_no">
										<a id="video_page_prev" href="javascript: void(0)" class="prev">&nbsp;</a>
										<span> '.$Lang['FrontPage']['Page'].' </span>
										<span><select id="video_page_option" name="PageNo">'.$html_page.'</select>'.$Lang['FrontPage']['PageEnd'].'</span>
										<a id="video_page_next" href="javascript: void(0)" class="next">&nbsp;</a>
										<span>  | '.$Lang['FrontPage']['Display'].' </span> <span><select id="video_display_option" name="DisplayNo"><option value="6" >6</option><option value="9">9</option><option value="12">12</option></select></span><span>'.$Lang['FrontPage']['DisplayEnd'].'</span>
					  			</div>
				   		</div></div>';
            return $html;
		}
		
		function retrieve_locality_list_html($Lang){
			
			$result = $this->retrieve_locality_info($Lang);	
			
			$localityCount = count($result);
			
			$allCount = $this->retrieve_total_video_count();			
			$html = '';
			
			for($i=0; $i <($localityCount); $i++ ){				
				$item = $result[$i];		
				$html .= '<li><a class="locality_item" value="'.$item["LocalityID"].'" href="javascript: void(0)" >'.str_replace('&', '/', $item["LocalityName"]).' <em>('.$item["LocalityCount"].')</em></a></li>';
			}			
			
			$html = '<ul><li class="selected_cat"><a class="locality_item" value="" href="javascript: void(0)" class="cat_all">'.$Lang['FrontPage']['All'].' <em>('.$allCount.')</em></a></li>'.$html;
			
            $html .= '</ul>';
            
            return $html;
		}
		
		function retrieve_video_locality_list_html($locality,$orderByPublishedTime,$orderByViewCount,$pageCount,$pageNumber,$viewMode, &$itemTotalNumber, $Lang){
			$result = $this->retrieve_video_info("", $locality,"",$orderByPublishedTime,$orderByViewCount, "");	
			$itemTotalNumber = count($result);
			
			if($itemTotalNumber<=0){				
				$html = '<div id="video_item">No Records Found.</div>';
				$html .= '<div id="video_page"><p class="spacer"></p>' .
            		'<input id="itemTotalNumber"   value="0" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="1" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$pageCount.'" type="hidden" /></div>';
            	return $html;
			}
			
			$html = "";
			for($i=$pageCount * ($pageNumber-1); $i <($pageCount * $pageNumber) && $i < $itemTotalNumber; $i++ ){				
				$item = $result[$i];	
				
				if($viewMode == 1){	
					$html .= '<a title="'.$item["Title"].'" class="video" value="'.$item["VideoID"].'" href="javascript:void(0)">
	                               	<span class="video_thumb" style="background-size: 100%;background-image:url(thumbnail/'.$item["Thumbnail"].'.jpg)"><em></em></span>
	                                    <h1>'.$item["Title"].'</h1>
	                                    <span class="video_viewed">'.$Lang['Common']['Views'].''.$item["ViewCount"].''.$Lang['Common']['ViewsEnd'].'</span>
	                                    <span class="video_date">'.$this->convert_date_YMD($item["PublishedTime"]).'</span>
	                                </a>';
               }else if($viewMode == 2){
					$html .= '<a title="'.$item["Title"].'" class="video" value="'.$item["VideoID"].'" href="javascript:void(0)">
                                	<span class="video_thumb" style="background-size: 100%;background-image:url(thumbnail/'.$item["Thumbnail"].'.jpg)"><em></em></span>
                                    <h1>'.$item["Title"].'</h1>
                                    <span class="video_viewed">'.$Lang['Common']['Views'].''.$item["ViewCount"].''.$Lang['Common']['ViewsEnd'].'</span>
                                   <span class="video_date">'.$this->convert_date_YMD($item["PublishedTime"]).'</span>
                                     <div class="view_brief_summary">
                                    	'.$item["Summary"].'                                    </div>
                                </a>';
				}		                 
               
			}			
            
	         $html .='<form method="GET" action="" name="videopage_form" id="videopage_form">' .
	        		'<input name="Mode"  value="content" type="hidden"/>' .
	        		'<input name="Task"  value="videopage" type="hidden"/>' .
	        		'<input name="videoID"  value="" type="hidden"/>' .
	        		'<input name="filter"  value="locality" type="hidden"/>' .
	        		'<input name="locality"  value="'.$locality.'" type="hidden" />' .
	        		'<input name="orderByPublishedTime"  value="'.$orderByPublishedTime.'" type="hidden"/>' .
	        		'<input name="orderByViewCount"  value="'.$orderByViewCount.'" type="hidden" />' .
	        		'</form>';
	        		
            if($viewMode == 2)	
            	$html = '<div class="view_list_detail">'.$html.'</div>';
            	
            return $html;
		}
		
		function retrieve_video_locality_frontpage_html($locality,$orderByPublishedTime,$orderByViewCount,$itemCount,$pageNumber,$viewMode, $Lang){			
			
			$itemTotalNumber = 0;
				
			$html = '<div id="video_item">';
			$html .= $this->retrieve_video_locality_list_html($locality,$orderByPublishedTime,$orderByViewCount,$itemCount,$pageNumber,$viewMode, $itemTotalNumber, $Lang);
			$html .= '</div>';	
			
			if($itemTotalNumber<=0){				
				$html = '<div id="video_item">'.$Lang['Common']['NoRecordsFound'].'</div>';
				$html .= '<div id="video_page"><p class="spacer"></p>' .
            		'<input id="itemTotalNumber"   value="0" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="1" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$itemCount.'" type="hidden" /></div>';
            	return $html;
			}
			
			$pageTotalNumber = ceil($itemTotalNumber / $itemCount);
			
			$html_page = '';
			for($i = 1; $i <=$pageTotalNumber; $i++ ){
				if($i == 1)
					$html_page .= '<option selected value="'.$i.'">'.$i.'</option>';
				else $html_page .= '<option  value="'.$i.'">'.$i.'</option>';
			}
			
			if($itemTotalNumber > $itemCount * $pageNumber)
				$itemCurrentNumber = $itemCount * $pageNumber;
			else $itemCurrentNumber = $itemTotalNumber;
			
            $html .='<div id="video_page"><p class="spacer"></p> ' .
            		'<input id="itemTotalNumber"   value="'.$itemTotalNumber.'" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="'.$pageNumber.'" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$itemCount.'" type="hidden" />
	                    <div class="common_table_bottom">
							<div id="video_page_record" class="record_no">'.$Lang['Common']['Records'].' <span id="video_page_item_floor">1</span> ' .
									'- <span id="video_page_item_ceil">'.$itemCurrentNumber.'</span>, ' .
											''.$Lang['FrontPage']['Total'].' <span id="video_page_item_total">'.$itemTotalNumber.'</span></div>
								<div class="page_no">
										<a id="video_page_prev" href="javascript: void(0)" class="prev">&nbsp;</a>
										<span> '.$Lang['FrontPage']['Page'].' </span>
										<span><select id="video_page_option" name="PageNo">'.$html_page.'</select>'.$Lang['FrontPage']['PageEnd'].' </span>
										<a id="video_page_next" href="javascript: void(0)" class="next">&nbsp;</a>
										<span><span> | '.$Lang['FrontPage']['Display'].' </span> <span><select id="video_display_option" name="DisplayNo"><option value="6" >6</option><option value="9">9</option><option value="12">12</option></select></span><span>'.$Lang['FrontPage']['DisplayEnd'].'</span>
					  			</div>
				   		</div></div>';
            return $html;
		}
		/////////////////////////////////////////////////////////////////////////////////////
		///////////////////////// Front Page END
		/////////////////////////////////////////////////////////////////////////////////////
		
		/////////////////////////////////////////////////////////////////////////////////////
		///////////////////////// Video Page
		/////////////////////////////////////////////////////////////////////////////////////
		
		function add_view_count ($videoID, $currentViewCount){
			$updatedViewCount = $currentViewCount + 1;
			$sql = " UPDATE ELIB_VIDEO_VIEW_COUNT Set ViewCount = $updatedViewCount, DateInput = NOW() WHERE VideoID = '$videoID' AND SchoolCode = '".$this->schoolCode."'";
			$result = $this->db_db_query($sql);
			
			if(!mysql_affected_rows()){
				$sql = " INSERT INTO ELIB_VIDEO_VIEW_COUNT (ViewCount,VideoID, SchoolCode, DateInput) Values (1, '$videoID', '".$this->schoolCode."', NOW() )";
				$result = $this->db_db_query($sql);
			}
			
			$sql = " SELECT CASE WHEN vct.ViewCount IS NULL THEN 0 ELSE vct.ViewCount END AS ViewCount FROM ELIB_VIDEO_VIDEO AS v " .
					" LEFT JOIN ELIB_VIDEO_VIEW_COUNT AS vct ON v.VideoID = vct.VideoID " .
					" WHERE vct.VideoID = '$videoID' AND vct.SchoolCode = '".$this->schoolCode."'";
					
			return current($this->returnVector($sql));
		}
		
		function retrieve_videopage_link($videoID){ // 2013-08-20 Charles Ma
			$sql = "SELECT v.VideoID AS VideoID, v.Reference AS Reference FROM ELIB_VIDEO_VIDEO AS v WHERE v.VideoID = '".$videoID."'";
			$currentVideoItem = current($this->returnArray($sql));
			
			$secret = "mysecretstringelib";             // Same as AuthTokenSecret
			$protectedPath = "/eLibvideoeclasshk/";        // Same as AuthTokenPrefix
			$hexTime = dechex(time());             // Time in Hexadecimal      
			$fileName = "/".$currentVideoItem["Reference"].".mp4";    // The file to access
			$fileName_ogg = "/".$currentVideoItem["Reference"].".ogg";    // The file to access
			
			$token = md5($secret . $fileName. $hexTime);
			$token_ogg = md5($secret . $fileName_ogg. $hexTime);
			
			// We build the url
			$url = $protectedPath . $token. "/" . $hexTime . $fileName;
			return $url;
		}
		
		function retrieve_videopage_html($category, $locality, $keyword, $orderByPublishedTime, $orderByViewCount, $videoID, $filter,$Lang){
						
			$result = $this->retrieve_video_info($category, $locality,$keyword,$orderByPublishedTime,$orderByViewCount, $filter);	
			
			
			$tag_html .= $this->retrieve_tag_html($videoID);
			
			$currentVideoItem = "";
			
			$totalVideoItem = count($result);
			
			
			foreach($result as $key=>$videoItem){
				//var_dump($result[$key]["VideoID"]);
				if($videoItem["VideoID"] == $videoID){
					if($key - 1< 0) $prevVideoItem = $result[$totalVideoItem - 1];
					else $prevVideoItem = $result[$key - 1];
					
					if($key + 1 >= $totalVideoItem) $nextVideoItem = $result[0];
					else $nextVideoItem = $result[$key + 1];
					
					$currentVideoItem = $videoItem;
					//break;
				}				 
			} 
			
			if($currentVideoItem == "") return "";					
			
			$updatedViewCount = $this->add_view_count($videoID, $currentVideoItem["ViewCount"]);
			
			$currentVideoFavourited = $this->retrieve_ifFavouriteVideo($videoID, $this->userID);
			
			if($currentVideoFavourited){
				$html_favourite = '<a id="add_bookmark_btn" href="javascript:void(0)" style="display:none;" class="btn_add_bookmark bookmark_btn">'.$Lang['VideoPage']['AddToFavourite'].'</a>
                                    		 <a id="delete_bookmark_btn"  href="javascript:void(0)" class="btn_remove_bookmark bookmark_btn">'.$Lang['VideoPage']['RemoveBookmark'].'</a>';
			}else{
				$html_favourite = '<a id="add_bookmark_btn" href="javascript:void(0)" class="btn_add_bookmark bookmark_btn">'.$Lang['VideoPage']['AddToFavourite'].'</a>
                                    		 <a id="delete_bookmark_btn" style="display:none;" href="javascript:void(0)" class="btn_remove_bookmark bookmark_btn">'.$Lang['VideoPage']['RemoveBookmark'].'</a>';
			}
			
			$html = '';
//			var_dump("<br>");var_dump("<br>");
//			var_dump($videoID);
//			var_dump($prevVideoItem["VideoID"]);
//			var_dump($nextVideoItem["VideoID"]);

			$secret = "mysecretstringelib";             // Same as AuthTokenSecret
			$protectedPath = "/eLibvideoeclasshk/";        // Same as AuthTokenPrefix
			$hexTime = dechex(time());             // Time in Hexadecimal      
			$fileName = "/".trim($currentVideoItem["Reference"]).".mp4";    // The file to access
			$fileName_ogg = "/".trim($currentVideoItem["Reference"]).".ogg";    // The file to access
			
			$token = md5($secret . $fileName. $hexTime);
			$token_ogg = md5($secret . $fileName_ogg. $hexTime);
			
			// We build the url
			$url = $protectedPath . $token. "/" . $hexTime . $fileName;
			$url_ogg = $protectedPath . $token_ogg. "/" . $hexTime . $fileName_ogg;

			$html .='<form method="GET" action="" name="videopage_form" id="videopage_form">' .
	            		'<input name="Mode"  value="content" type="hidden"/>' .
	            		'<input name="Task"  value="videopage" type="hidden"/>' .
	            		'<input name="videoID"  value="'.$videoID.'" type="hidden"/>' .
	            		'<input name="prevVideoID"  value="'.$prevVideoItem["VideoID"].'" type="hidden" id="prevVideoID"/>' .
	            		'<input name="nextVideoID"  value="'.$nextVideoItem["VideoID"].'" type="hidden" id="nextVideoID"/>' .
	            		'<input name="prevVideoTitle"  value="'.$prevVideoItem["Title"].'" type="hidden" id="prevVideoTitle"/>' .
	            		'<input name="nextVideoTitle"  value="'.$nextVideoItem["Title"].'" type="hidden" id="nextVideoTitle"/>' .
	            		'<input name="filter"  value="" type="hidden"/>' .
	            		'<input name="category"  value="'.$category.'" type="hidden" />' .
	            		'<input name="locality"  value="'.$locality.'" type="hidden" />' .
	            		'<input name="orderByPublishedTime"  value="'.$orderByPublishedTime.'" type="hidden"/>' .
	            		'<input name="orderByViewCount"  value="'.$orderByViewCount.'" type="hidden" />' .
	            		'<input name="Summary"  value="'.$currentVideoItem["Summary"].'" type="hidden" id="video_summary"/>' .
	            		'<input name="Reference"  value="'.$url.'" type="hidden" id="video_reference"/>' .
	            				'<input name="Reference_ogg"  value="'.$url_ogg.'" type="hidden" id="video_reference_ogg"/>' .
	            		'<input name="Script"  value="'.$currentVideoItem["Script"].'" type="hidden" id="video_script"/>' .
            		'</form>';
			
			$html .= '
                          <!---->
                          <div id="video_board" class="video_board_script">
                          		 <!---->
                          		<h1> '.$currentVideoItem["Title"].' </h1>
                                 <!---->
                          		<div class="video_content">
                                	<div class="video_clip">
                                		<video class="video_item" onerror="" autoplay id="video_clip" style="background: black" width="480" height="390" oncontextmenu="return false;" controls="">
										  	<source id="video_item_mp4" src="" type="video/mp4;"/><source id="video_item_ogg" src="" type="video/ogg;"/>
										  	Your browser does not support the video tag.
										</video>
									</div>
											<div width="" class="video_item" style="margin-top: -10px;margin-bottom: 10px;font-size: 9px; color: white;background-color: black;width: 480px;height: 15px;">
													<span style=" margin-right: 10px; float: right;">©AFP 2013</span></div>
                                    <div class="tool_box">
                                    		'.$html_favourite.'
                                    </div>
                                	<span class="video_viewed">'.$Lang['Common']['Views'].''.$updatedViewCount.''.$Lang['Common']['ViewsEnd'].'</span>
                                    <span class="video_date">'.$this->convert_date_YMD($currentVideoItem["PublishedTime"]).'</span>
                                    <span class="tag_list"> '.$Lang['VideoPage']['Tag'].' :  '.$tag_html.'</span>
                                    
                                </div>
                                 <!---->
                                <div class="video_script_board">
                                	<div class="script_board_top"><div class="script_board_top_right"><div class="script_board_top_bg">
                                    	<div class="script_tab">
                                        	<ul>
                                            	<li class="current_tab"><a id="video_summary_btn" href="javascript:void(0);" class="tab_left"><span><u>'.$Lang['VideoPage']['Summary'].'</u></span></a></li>
                                                <li><a id="video_script_btn" href="javascript:void(0);" class="tab_right"><span><u>'.$Lang['VideoPage']['Script'].'</u></span></a></li>
                                            </ul>
                                        </div>
                                	 </div></div></div>
                                     <div class="script_board_content"><div class="script_board_content_right"><div class="script_board_content_bg">
                                     		<div id="summary_content" class="script_content">
	                                            Summary Content 
                                            </div>
                                            <div id="script_content" class="script_content">
	                                            Script Content 
                                            </div>
                                	 </div></div></div>
                                     <div class="script_board_bottom"><div class="script_board_bottom_right"><div class="script_board_bottom_bg">
                                     <span>© AFP 2013</span>
                               	  </div></div></div>
                                </div>
                          		 <!---->
                          </div>';
                          
              
              return $html;
		}
		
		/////////////////////////////////////////////////////////////////////////////////////
		///////////////////////// Video Page END
		/////////////////////////////////////////////////////////////////////////////////////
		
		/////////////////////////////////////////////////////////////////////////////////////
		///////////////////////// Favourite Page 
		/////////////////////////////////////////////////////////////////////////////////////
		
		function retrieve_favourite_list_html($pageCount,$pageNumber, $itemTotalNumber,$Lang){
			$result = $this->retrieve_favourite_info($this->userID);
			
			$itemTotalNumber = count($result);
			
			$html = "";
			for($i=$pageCount * ($pageNumber-1); $i <($pageCount * $pageNumber) && $i < $itemTotalNumber; $i++ ){
				$item = $result[$i];	
							
				$html .= '<a title="'.$item["Title"].'" class="video" value="'.$item["VideoID"].'" href="javascript:void(0)">
                           	<span class="video_thumb" style="background-size: 100%;background-image:url(thumbnail/'.$item["Thumbnail"].'.jpg)"><em></em>
                           		<span value="'.$item["VideoID"].'" name="btn_remove_bookmark" type="button" class="btn_remove_bookmark" title="'.$Lang['FavouritePage']['RemovefromFavourite'].'" />   	
                           		</span>
                                <h1>'.$item["Title"].'</h1>
                                <span class="video_viewed">'.$Lang['Common']['Views'].''.$item["ViewCount"].''.$Lang['Common']['ViewsEnd'].'</span>
                                <span class="video_date">'.$this->convert_date_YMD($item["PublishedTime"]).'</span>
                         </a>';	
			}
			 $html .='<form method="GET" action="" name="videopage_form" id="videopage_form">' .
	        		'<input name="Mode"  value="content" type="hidden"/>' .
	        		'<input name="Task"  value="videopage" type="hidden"/>' .
	        		'<input name="videoID"  value="" type="hidden"/>' .
	        		'<input name="filter"  value="favourite" type="hidden"/>' .
	        		'</form>';
	        		
            $ret = array();
            $ret['html'] = $html;
            $ret['itemTotalNumber'] = $itemTotalNumber;
            
            return $ret;
			
		}
		
		function retrieve_favouritepage_html($itemCount,$pageNumber,$Lang){
			$itemTotalNumber = 0;

			$favourite_list = $this->retrieve_favourite_list_html($itemCount,$pageNumber, $itemTotalNumber,$Lang); 				
			$html = '<div id="video_item">';
			$html .= $favourite_list['html'];
			$html .= '</div>';	
			$itemTotalNumber = $favourite_list['itemTotalNumber'];
			
			if($itemTotalNumber<=0){				
				$html = '<div id="video_item">'.$Lang['Common']['NoRecordsFound'].'</div>';
				$html .= '<div id="video_page"><p class="spacer"></p>' .
            		'<input id="itemTotalNumber"   value="0" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="1" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$itemCount.'" type="hidden" /></div>';
            	return $html;
			}
			
			$pageTotalNumber = ceil($itemTotalNumber / $itemCount);
			
			$html_page = '';
			for($i = 1; $i <=$pageTotalNumber; $i++ ){
				if($i == 1)
					$html_page .= '<option selected value="'.$i.'">'.$i.'</option>';
				else $html_page .= '<option  value="'.$i.'">'.$i.'</option>';
			}
			
			if($itemTotalNumber > $itemCount * $pageNumber)
				$itemCurrentNumber = $itemCount * $pageNumber;
			else $itemCurrentNumber = $itemTotalNumber;
			
            $html .='<div id="video_page"><p class="spacer"></p> ' .
            		'<input id="itemTotalNumber"   value="'.$itemTotalNumber.'" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="'.$pageNumber.'" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$itemCount.'" type="hidden" />
	                    <div class="common_table_bottom">
							<div id="video_page_record" class="record_no">'.$Lang['Common']['Records'].' <span id="video_page_item_floor">1</span> ' .
									'- <span id="video_page_item_ceil">'.$itemCurrentNumber.'</span>, ' .
											''.$Lang['FrontPage']['Total'].' <span id="video_page_item_total">'.$itemTotalNumber.'</span></div>
								<div class="page_no">
										<a id="video_page_prev" href="javascript: void(0)" class="prev">&nbsp;</a>
										<span>'.$Lang['FrontPage']['Page'].'</span>
										<span><select id="video_page_option" name="PageNo">'.$html_page.'</select>'.$Lang['FrontPage']['PageEnd'].'</span>
										<a id="video_page_next" href="javascript: void(0)" class="next">&nbsp;</a>
										<span>  | '.$Lang['FrontPage']['Display'].' </span> <span><select id="video_display_option" name="DisplayNo"><option value="6" >6</option><option value="9">9</option><option value="12">12</option></select></span><span>'.$Lang['FrontPage']['DisplayEnd'].'</span>
					  			</div>
				   		</div></div>';
            return $html;
		}
		
		function delete_particular_favourite_video($videoID){
			$sql = " DELETE FROM ELIB_VIDEO_BOOKMARK WHERE UserID = '".$this->userID."' AND VideoID = '".$videoID."'";			
			$result = $this->db_db_query($sql);
		}
		
		function delete_all_favourite_video(){
			$sql = " DELETE FROM ELIB_VIDEO_BOOKMARK WHERE UserID = '".$this->userID."'";			
			$result = $this->db_db_query($sql);
		}
		/////////////////////////////////////////////////////////////////////////////////////
		///////////////////////// Favourite Page END
		/////////////////////////////////////////////////////////////////////////////////////	
		
		function retrieve_category_option_html($Lang){			
			$lang_chi = $_SESSION["ELIB_VIDEO_Lang"] == "b5" ? "AS CategoryNameEng , CategoryNameChi AS CategoryName " : "" ;
			$lang_chi_order = 	$_SESSION["ELIB_VIDEO_Lang"] == "b5" ? "Eng" : "" ;
			
			$sql = " SELECT CategoryID, CategoryName $lang_chi FROM ELIB_VIDEO_CATEGORY ORDER BY CategoryName$lang_chi_order ";
			$result = $this->returnArray($sql);
			
			$html ="";
			
			
			
			foreach($result as $key=>$categoryItem){
				$html .= '<li><label><input name="category[]" type="checkbox" value="'.$categoryItem['CategoryID'].'" wording="'.$categoryItem['CategoryName'].'" />'.$categoryItem['CategoryName'].'</label></li>';
			}
			return $html;
		}
		
		function retrieve_locality_option_html($Lang){
			$lang_chi = $_SESSION["ELIB_VIDEO_Lang"] == "b5" ? "AS LocalityNameEng , LocalityNameChi AS LocalityName " : "" ;
			$lang_chi_order = 	$_SESSION["ELIB_VIDEO_Lang"] == "b5" ? "Eng" : "" ;
			
			$sql = " SELECT LocalityID, LocalityName $lang_chi FROM ELIB_VIDEO_LOCALITY ORDER BY LocalityName$lang_chi_order ";
			$result = $this->returnArray($sql);
			
			$html ="";
			
			foreach($result as $key=>$localityItem){
				$html .= '<li><label><input name="locality[]" type="checkbox" value="'.$localityItem['LocalityID'].'" wording="'.str_replace('&', '/', $localityItem['LocalityName']).'" />'.str_replace('&', '/', $localityItem['LocalityName']).'</label></li>';
			}
			return $html;
		}
		
		function retrieve_searchpage_html($Lang){				
					
			$html = '';
			
			$category_option = $this->retrieve_category_option_html($Lang);
			$locality_option =$this->retrieve_locality_option_html($Lang);
			$html .= '<tr>
                                        <td> '.$Lang['SearchPage']['Keywords'].' </td>
                                        <td>:</td>
                                        <td ><label>
                                          <input name="keyword" type="text" id="keyword_field" class="textbox" placeholder="" />
                                        </label></td>
                                      </tr>
                                      <tr>
                                        <td> '.$Lang['SearchPage']['Category'].' </td>
                                        <td>:</td>
                                        <td >
                                            <div class="selection"><input id="field_search_cat_default" type="hidden" value= "'.$Lang['SearchPage']['Search'].'"/>	
                                                <div class="selection_filter" onclick="MM_showHideLayers(\'selection_layer_01\',\'\',\'show\')"><a id="field_search_cat" href="#" class="">'.$Lang['SearchPage']['Search'].'</a>
                                                	
                                                </div>
                                                <div class="selection_layer" id="selection_layer_01">
	                                               	<form method="POST" action="" name="category_option" id="category_option">
	                                                    <ul>
	                                                        '.$category_option.'
	                                                    </ul>
	                                                </form>	
                                                <p class="spacer"></p>
                                                 <div class="edit_bottom">
                                                     
                                                      <input id="btn_search_cat" name="submit3" type="button" class="formsubbutton" onclick="MM_showHideLayers(\'selection_layer_01\',\'\',\'hide\')" value="OK" />
                                                      <!--<input name="submit2" type="button" class="formsubbutton" onclick="MM_showHideLayers(\'selection_layer_01\',\'\',\'hide\')" value="Cancel" />-->
                                                   
                                                      </div>
                                                </div>
                                            </div>
                                        
                                        </td>
                                      </tr>
                                  <tr>
                                    <td> '.$Lang['SearchPage']['Location'].' </td>
                                    <td>:</td>
                                    <td ><div class="selection"><input id="field_search_loc_default" type="hidden" value= "'.$Lang['SearchPage']['Search'].'"/>
                                                <div class="selection_filter" onclick="MM_showHideLayers(\'selection_layer_02\',\'\',\'show\')"><a id="field_search_loc" href="#" class="">'.$Lang['SearchPage']['Search'].'</a>
                                                			</div>
                                                <div class="selection_layer" id="selection_layer_02">
	                                                <form method="POST" action="" name="locality_option" id="locality_option">		
	                                                    <ul>
	                                                      '.$locality_option.'                                                        
	                                                    </ul>
	                                                </form>
                                                <p class="spacer"></p>
                                                 <div class="edit_bottom">
                                                     
                                                      <input id="btn_search_loc" name="submit3" type="button" class="formsubbutton" onclick="MM_showHideLayers(\'selection_layer_02\',\'\',\'hide\')" value="OK" />
                                                      <!--<input name="submit2" type="button" class="formsubbutton" onclick="MM_showHideLayers(\'selection_layer_02\',\'\',\'hide\')" value="Cancel" />-->
                                                   
                                                      </div>
                                                </div>
                                            </div></td>
                                  </tr>';
  			return $html;                          
		}
		
		function retrieve_searchresultpage_html($category,$locality,$keyword,$orderByPublishedTime,$orderByViewCount,$itemCount, $pageNumber, $Lang){
			$itemTotalNumber = 0;
			$html = '<div id="video_item">';
			$html .= $this->retrieve_searchresultlist_html($category,$locality,$keyword,$orderByPublishedTime,$orderByViewCount,$itemCount, $pageNumber, $itemTotalNumber, $Lang);
			$html.= '</div>';
			
			if($itemTotalNumber<=0){				
				$html = '<div id="video_item">'.$Lang['Common']['NoRecordsFound'].'</div>';
				$html .= '<div id="video_page"><p class="spacer"></p>' .
            		'<input id="itemTotalNumber"   value="0 '.$Lang['SearchPage']['Found'].'" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="1" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$itemCount.'" type="hidden" /></div>';
            	return $html;
			}
			
			$pageTotalNumber = ceil($itemTotalNumber / $itemCount);
			
			$html_page = '';
			for($i = 1; $i <=$pageTotalNumber; $i++ ){
				if($i == 1)
					$html_page .= '<option selected value="'.$i.'">'.$i.'</option>';
				else $html_page .= '<option  value="'.$i.'">'.$i.'</option>';
			}
			
			if($itemTotalNumber > $itemCount * $pageNumber)
				$itemCurrentNumber = $itemCount * $pageNumber;
			else $itemCurrentNumber = $itemTotalNumber;
			
            $html .='<div id="video_page"><p class="spacer"></p> ' .
            		'<input id="itemTotalNumber" wording="'.$itemTotalNumber.' '.$Lang['SearchPage']['Found'].'"  value="'.$itemTotalNumber.'" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="'.$pageNumber.'" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$itemCount.'" type="hidden" />
	                    <div class="common_table_bottom">
							<div id="video_page_record" class="record_no">'.$Lang['Common']['Records'].' <span id="video_page_item_floor">1</span> ' .
									'- <span id="video_page_item_ceil">'.$itemCurrentNumber.'</span>, ' .
											''.$Lang['SearchPage']['Total'].' <span id="video_page_item_total">'.$itemTotalNumber.'</span></div>
								<div class="page_no">
										<a id="video_page_prev" href="javascript: void(0)" class="prev">&nbsp;</a>
										<span> '.$Lang['FrontPage']['Page'].' </span>
										<span><select id="video_page_option" name="PageNo">'.$html_page.'</select> '.$Lang['FrontPage']['PageEnd'].' </span>
										<a id="video_page_next" href="javascript: void(0)" class="next">&nbsp;</a>
										<span>
					  			</div>
				   		</div></div>';
			
			return $html;
		}
		
		function retrieve_searchresultlist_html($category,$locality,$keyword,$orderByPublishedTime,$orderByViewCount,$pageCount, $pageNumber, &$itemTotalNumber, $Lang){
			$result = $this->retrieve_video_info($category, $locality,$keyword,$orderByPublishedTime,$orderByViewCount,"");	
			$itemTotalNumber = count($result);
			
			if($itemTotalNumber<=0){				
				$html = '<div id="video_item">'.$Lang['Common']['NoRecordsFound'].'</div>';
				$html .= '<div id="video_page"><p class="spacer"></p>' .
            		'<input id="itemTotalNumber"   value="0" type="hidden" />' .
            		'<input id="currentPageNumber" 	   value="1" type="hidden" />' .
            		'<input id="itemCount" 		   value="'.$pageCount.'" type="hidden" /></div>';
            	return $html;
			}
			
			$html = "";
			for($i=$pageCount * ($pageNumber-1); $i <($pageCount * $pageNumber) && $i < $itemTotalNumber; $i++ ){				
				$item = $result[$i];	
							
				$html .= '<a title="'.$item["Title"].'" class="video" value="'.$item["VideoID"].'" href="javascript:void(0)">
                               	<span class="video_thumb" style="background-size: 100%;background-image:url(thumbnail/'.$item["Thumbnail"].'.jpg)"><em></em></span>
                                    <h1>'.$item["Title"].'</h1>
                                    <span class="video_viewed">'.$Lang['Common']['Views'].''.$item["ViewCount"].''.$Lang['Common']['ViewsEnd'].'</span>
                                    <span class="video_date">'.$this->convert_date_YMD($item["PublishedTime"]).'</span>
                                </a>';
			}			
            if(is_array($locality))
            foreach($locality as $key => $value){
            	$locality_form_value .= '<input name="locality[]"  value="'.$value.'" type="hidden" />';
            }
            if(is_array($category))
            foreach($category as $key => $value){
            	$category_form_value .= '<input name="category[]"  value="'.$value.'" type="hidden" />';
            }
            if(is_array($keyword))
            foreach($keyword as $key => $value){
            	$keyword_form_value .= '<input name="keyword[]"  value="'.$value.'" type="hidden" />';
            }
            
	         $html .='<form method="GET" action="" name="videopage_form" id="videopage_form">' .
	        		'<input name="Mode"  value="content" type="hidden"/>' .
	        		'<input name="Task"  value="videopage" type="hidden"/>' .
	        		'<input name="videoID"  value="" type="hidden"/>' .
	        		'<input name="filter"  value="keyword" type="hidden"/>' .$locality_form_value.$category_form_value.$keyword_form_value.
	        		'</form>';
            
            return $html;
		}	
		
		function retrieve_termpage_html(){
			$html = '<div class="terms_board">
		    	Please read and accept the Terms and Conditions before accessing the video platform. <br>
				請先細閱及接受本影片平台的使用者條款及細則。<br><br>
		        <div class="terms_content">
			        <ol>
			          <li>All Content and Service of this video  platform are owned by AFP and BroadLearning Education (Asia), subject to  copyright and other intellectual property rights under the law.</li>
			          <li>You may access Content for your  information and personal use solely as intended through the functionality of  the video platform.</li>
			          <li>You shall not download any Content.  You shall not copy, reproduce, make available online or electronically  transmit, publish, adapt, distribute, transmit, broadcast, display, sell, or  otherwise exploit any Content for any other purposes.</li>
			    </ol>
			    <ol>
			          <li>本影片平台所有內容及服務的版權是由AFP及博文教育（亞洲）有限公司擁有。 </li>
			          <li>本影片平台所有內容 (影片) 只能作學習及觀賞用途；使用者只可以使用限於本平台的功能選擇及瀏覽各影片。 </li>
			          <li>使用者不得下載任何內容 (影片)，不得複製、上載、轉載、刊登、分發、廣播、公開展示、出售、轉售、出租、改編或以任何方法更改本平台內的內容及作任何其他用途。 </li>
			    </ol>
			        <p>&nbsp;</p>
			  </div>';
		        
		    return $html;
		}
		
		function retrieve_termpageform_html($loginArr){
			foreach($loginArr as $key=>$value){
							$form_html .= '<input name="'.$key.'"  value="'.$value.'" type="hidden"/>';
						}
						
			$html = '<form method="POST" action="" name="registerpage_form" id="registerpage_form">
			            <label class="terms_accept"><span> <input name="readUnderstandTerm" type="checkbox" value="1" /></span>I have read, understood and accepted the above  terms.<br>
              我已細閱，明白及接受以上條款。 </label><br />
			            <br />
			            <p class="spacer"></p>		            
			              <input name="acceptTerm" type="submit" class="formbutton"  value="Accept" />
			              <input name="acceptTerm" type="submit" class="formsubbutton" value="Decline" />
			              <input name="Mode"  value="content" type="hidden"/>
			              <input name="Task"  value="termspage" type="hidden"/>' .
			              		$form_html.'
	           		</form>    ';
	       return $html;
		}
		
		function checkIfAcceptTerm($readUnderstandTerm, $acceptTerm, $loginArr){
			if($readUnderstandTerm && $acceptTerm == "Accept"){
				$this->add_user($loginArr);
				$html = '<body onLoad="document.MainForm.submit();">
							<form name="MainForm" id="MainForm" action="" method="post">		
							</form>
						</body>';
				echo($html);
			}
		}
		
		function getDefaultDateFormat($RecordDate){
			
			$record = explode('/',$RecordDate);
			
			if((strpos($RecordDate, '/') == true) && (count($record) == 3)){ 	
				list($day, $month, $year) = $record;
				#D/M/YYYY or DD/MM/YYYY		
				$year = trim($year);
				$day = trim($day);
				$month = trim($month);
				
				$day = str_pad($day, 2, '0', STR_PAD_LEFT);		
				$month = str_pad($month, 2, '0', STR_PAD_LEFT);
						
				$RecordDate = $year . '-' . $month . '-' . $day;			
			}
				
			return $RecordDate;		
		}
		
		
		
		function uploadfile($filepath){			
			
			include_once('libimporttext.php');
			
			$result = array();
			
			if($filepath == ""){
				echo "<br/>No csv is found. <br/>";
				sleep(10);
				header("location: index.php");
				exit();
			}
			
			$limport 	= new libimporttext();
			$data = $limport->GET_IMPORT_TXT($filepath);			
			$col_array = array_shift($data);	
			$col_name = explode("|" ,$col_array[0]);
			
			$file_format =  explode("," ,"Video ID,Video Length,CategoryEng,CategoryChi,LocalityEng,LocalityChi,Tag,Package,Channel,Title,Summary,Reference,Script,Thumbnail,Publish Time");
			
			$format_wrong = false;
			
			for($i=0; $i<sizeof($file_format); $i++)
			{
				if ($col_name[$i]!=$file_format[$i])
				{
					$format_wrong = true;
					break;
				}
			}
			
			if($format_wrong)
			{
				echo "<br/>The format in csv is incorrect. <br/>";
				sleep(10);
				header("location: index.php");
				exit();
			}
			
			$arr_item_package = array();	// For update VIDEO PACKAGE LIST
			$arr_tag = array();			// For update TAG LIST
			$arr_item_tag = array();	// For update VIDEO TAG LIST
			$arr_category = array();	// For update VIDEO TAG LIST
			$arr_item_category = array();	// For update VIDEO TAG LIST
			$arr_locality = array();	// For update VIDEO TAG LIST
			$arr_item_locality = array();	// For update VIDEO TAG LIST
			
			$update_sql	= array();		
			$update_sql_index = -1;
			
			echo("<br><br>");
			echo("Now Updating CSV");
			
			for($i=0; $i<sizeof($data); $i++){
				$item = $data[$i][0];
				$inDoubleQuote = false;
				
				$current_array = explode("|" ,$item);
				$arr_new_data[] = $current_array;
				list($videoID,$videoLength,$category,$categoryChi,$locality,$localityChi,$tag,$package,$channel,$title,$summary,$reference,$script,$thumbnail,$publishedTime)	= $current_array;
								
				$channel = str_replace('"', '',$channel);
				$title = str_replace('"', '',$title);		
				$title = str_replace('<br/>', '',$title);			
				$reference = str_replace('"', '',$reference);
				$reference = str_replace('<br/>', '',$reference);
				$script = nl2br(str_replace('"', '',$script));
				$script = str_replace('  ', '<br/><br/>',$script);
				
				$summary = str_replace('"', '',$summary);
				$thumbnail = str_replace('"', '',$thumbnail);				
				$thumbnail = str_replace('<br/>', '',$thumbnail);
				$publishedTime = $this->getDefaultDateFormat( str_replace('"', '',$publishedTime) );
				
				$category = str_replace('"', '',$category);
				
				$update_sql_index = $i % 400 == 0 ? $update_sql_index + 1: $update_sql_index; 				
								
				if($update_sql[$update_sql_index] == "")
					$update_sql[$update_sql_index] = " ($videoID, \"$videoLength\", \"$channel\", \"$title\", \"$summary\", \"$reference\", \"$script\", \"$thumbnail\", \"$publishedTime\", NOW()) ";
				else $update_sql[$update_sql_index] .= ","." ($videoID, \"$videoLength\", \"$channel\", \"$title\", \"$summary\", \"$reference\", \"$script\", \"$thumbnail\", \"$publishedTime\", NOW()) ";
								
				//////////////////////////Package////////////////////////////	
				$current_arr_package = explode(";" ,$package);				
				$arr_item_package[$videoID] = $current_arr_package;
								
				//////////////////////////Tag////////////////////////////
				$current_arr_tag = explode(";" ,$tag);	
				$arr_item_tag[$videoID] = $current_arr_tag;
				$arr_tag = array_merge($arr_tag, $current_arr_tag);	
				
				//////////////////////////Category////////////////////////////
				$current_arr_category = explode(";" ,$category);	
				$arr_item_category[$videoID] = $current_arr_category;					
				$arr_category 	= array_merge($arr_category, $current_arr_category);	
				
				//////////////////////////Locality////////////////////////////
				$current_arr_locality = explode(";" ,$locality);	
				$arr_item_locality[$videoID] = $current_arr_locality;					
				$arr_locality 	= array_merge($arr_locality, $current_arr_locality);	
				
			}

			//////////////////////////Package////////////////////////////
			
			$current_package_sql = "";
			
			$sql = " DELETE FROM ELIB_VIDEO_VIDEO_PACKAGE WHERE VideoPackageID > 0 ";
			$result["delete_package"] = $this->db_db_query($sql);
			
			$sql = " ALTER TABLE ELIB_VIDEO_VIDEO_PACKAGE AUTO_INCREMENT=0";
			$result["reset_package_increment"] = $this->db_db_query($sql);
			
			$fail_videoID_arr = array();
			
			foreach($arr_item_package as $i=>$arr_package){
				foreach($arr_package as $j=>$package){
										
					$current_videoID = $i ; 
					if(is_int($current_videoID)){
						if($current_package_sql == "")
						$current_package_sql .= " (\"$current_videoID\",\"$package\", NOW()) ";
						else $current_package_sql .= ", (\"$current_videoID\",\"$package\", NOW()) ";						
					}else{
						array_push($fail_videoID_arr, $current_videoID);
					}
				}	
			}
			
			
			$sql = " INSERT INTO ELIB_VIDEO_VIDEO_PACKAGE (VideoID, Package, DateInput) VALUES ".$current_package_sql;
			$result["insert_package"] = $this->db_db_query($sql);
			
			//////////////////////////Tag////////////////////////////
			$arr_tag = array_unique($arr_tag);
			array_multisort($arr_tag);			
			if($arr_tag[0] == ""){
				array_shift($arr_tag);
			}
			
			function length_sort($a,$b){
			    return (strlen($a)-strlen($b) > 0) ? -1 : 1;
			}
			usort($arr_tag,'length_sort');
			
			$sql = " DELETE FROM ELIB_VIDEO_TAG WHERE TagID > 0 ";
			$result["delete_tag"] = $this->db_db_query($sql);
			
			$sql = " ALTER TABLE ELIB_VIDEO_TAG AUTO_INCREMENT=0";
			$result["reset_tag_increment"] = $this->db_db_query($sql);
			
			$current_tag_sql = "";
			
			$tag_replacements = array();
			
			foreach($arr_tag as $key=>$tag){
				$tag = str_replace("\"", "", $tag);
				if($current_tag_sql == "")
					$current_tag_sql .= " (\"$tag\", NOW()) ";
				else $current_tag_sql .= ", (\"$tag\", NOW()) ";
				
				$tag_replacements['/\b'.$tag.'\b/'] = $key + 1;
			}
			$result["number_tag"] = count($arr_tag);
			
			$sql = " INSERT INTO ELIB_VIDEO_TAG (TagName, DateInput) VALUES ".$current_tag_sql ;
			
			$result["insert_tag"] = $this->db_db_query($sql);
			
			$sql = " DELETE FROM ELIB_VIDEO_VIDEO_TAG WHERE VideoTagID > 0 ";
			$result["delete_tag_video_relation"] = $this->db_db_query($sql);
			
			$sql = " ALTER TABLE ELIB_VIDEO_VIDEO_TAG AUTO_INCREMENT=0 ";
			$result["reset_tag_video_relation_increment"] = $this->db_db_query($sql);
			
			$current_tag_sql = "";
			
			foreach($arr_item_tag as $i=>$arr_tag){
				foreach($arr_tag as $j=>$tag){
					$tag = str_replace("\"", "", $tag);
					$current_videoID = $i ; 
					$tag = preg_replace(array_keys($tag_replacements), array_values($tag_replacements), $tag); 
					
					if(is_int($current_videoID)){
						if($current_tag_sql == "")
						$current_tag_sql .= " (\"$current_videoID\",\"$tag\", NOW()) ";
						else $current_tag_sql .= ", (\"$current_videoID\",\"$tag\", NOW()) ";
					}
				}	
			}
			$result["number_tag_video_relation"] = 	count($arr_item_tag);
					
			$sql = " INSERT INTO ELIB_VIDEO_VIDEO_TAG (VideoID, TagID, DateInput) VALUES ".$current_tag_sql;
			
			$result["insert_tag_video_relation"] = $this->db_db_query($sql);
			
			
			$sql = " SELECT CategoryName, CategoryID FROM ELIB_VIDEO_CATEGORY ";
			$result_s = $this->returnArray($sql);
			
			$category_replacements = array();
			
			foreach($result_s as $key=>$value){
				$category_replacements['/\b'.$value["CategoryName"].'\b/'] = $value["CategoryID"];
			}
			$category_replacements = array_reverse($category_replacements);
			
			$current_category_sql ="";
			
			
			foreach($arr_item_category as $i=>$arr_category){
				foreach($arr_category as $j=>$category){
					$arr_item_category[$i][$j] = preg_replace(array_keys($category_replacements), array_values($category_replacements), $category); 
					
					$category = $arr_item_category[$i][$j];					
					$current_videoID = $i; 
					
					if(is_int($current_videoID)){
						if($current_category_sql == "")
						$current_category_sql .= " (\"$current_videoID\",\"$category\", NOW()) ";
						else $current_category_sql .= ", (\"$current_videoID\",\"$category\", NOW()) ";
					}
				}
			}				
			
			$sql = " DELETE FROM ELIB_VIDEO_VIDEO_CATEGORY WHERE VideoCategoryID > 0 ";
			$result["delete_category_relation"] = $this->db_db_query($sql);
			
			$sql = " ALTER TABLE ELIB_VIDEO_VIDEO_CATEGORY AUTO_INCREMENT=0 ";
			$result["reset_category_relation_increment"] = $this->db_db_query($sql);
			
			$sql = " INSERT INTO ELIB_VIDEO_VIDEO_CATEGORY (VideoID, CategoryID, DateInput) VALUES ".$current_category_sql;
			$result["insert_category_relation"] = $this->db_db_query($sql);
			
			//////////////////////////Locality////////////////////////////
			
			$sql = " SELECT LocalityName, LocalityID FROM ELIB_VIDEO_LOCALITY ";
			$result_s = $this->returnArray($sql);
			
			$locality_replacements = array();
			
			foreach($result_s as $key=>$value){
				$locality_replacements['/\b'.str_replace('&', '/', $value["LocalityName"]).'\b/'] = $value["LocalityID"];
			}
			$locality_replacements = array_reverse($locality_replacements);
			
			$current_locality_sql ="";
			
			foreach($arr_item_locality as $i=>$arr_locality){
				foreach($arr_locality as $j=>$locality){
					$arr_item_locality[$i][$j] = preg_replace(array_keys($locality_replacements), array_values($locality_replacements), $locality); 
					
					$locality = $arr_item_locality[$i][$j];					
					$current_videoID = $i; 
					
					if(is_int($current_videoID)){
						if($current_locality_sql == "")
						$current_locality_sql .= " (\"$current_videoID\",\"$locality\", NOW()) ";
						else $current_locality_sql .= ", (\"$current_videoID\",\"$locality\", NOW()) ";
					}
				}
			}
			
			$sql = " DELETE FROM ELIB_VIDEO_VIDEO_LOCALITY WHERE VideoLocalityID > 0 ";
			$result["delete_locality_relation"] = $this->db_db_query($sql);
			
			$sql = " ALTER TABLE ELIB_VIDEO_VIDEO_LOCALITY AUTO_INCREMENT=0 ";
			$result["reset_locality_relation_increment"] = $this->db_db_query($sql);
			
			$sql = " INSERT INTO ELIB_VIDEO_VIDEO_LOCALITY (VideoID, LocalityID, DateInput) VALUES ".$current_locality_sql;
			$result["delete_locality_relation"] = $this->db_db_query($sql);
						
			//////////////////////////END UP////////////////////////////
			
			
			foreach($update_sql as $key=>$current_sql){
				var_dump($current_sql."<br/>");
				$current_sql = " Insert into ELIB_VIDEO_VIDEO (VideoID,VideoLength, Channel, Title, Summary,Reference,Script,Thumbnail, PublishedTime, DateInput ) values $current_sql " .
						" on duplicate key update VideoLength=VALUES(VideoLength), Channel=VALUES(Channel), Title=VALUES(Title), Summary=VALUES(Summary), Reference=VALUES(Reference), Script=VALUES(Script), Thumbnail=VALUES(Thumbnail), PublishedTime=VALUES(PublishedTime), DateInput=VALUES(DateInput) ";
						
				$result["insert_update_video_info"] = $this->db_db_query($current_sql);				
			}			
			
			
			if(sizeof($fail_videoID_arr) > 0){
				$html .='<br/><span>Fail Video ID:</span><br/>';
				foreach($fail_videoID_arr as $value){
					$html .='<span>'.$value.'</span><br/>';					
				}				
				echo($html);	
				sleep(10);
			}
			
			
			if($result["insert_update_video_info"])
			{
				 $html .='<form method="POST" action="" name="uploadpage_form" id="uploadpage_form">' .
	        		'<input name="Mode"  value="content" type="hidden"/>' .
	        		'<input name="Task"  value="uploadpage" type="hidden"/>' .
	        		'<input name="message"  value="Update success." type="hidden"/>' .
	        		'</form>';
				$html .= '<script>document.uploadpage_form.submit();</script>';
			}
			else{
				$html .='<form method="POST" action="" name="uploadpage_form" id="uploadpage_form">' .
	        		'<input name="Mode"  value="content" type="hidden"/>' .
	        		'<input name="Task"  value="uploadpage" type="hidden"/>' .
	        		'<input name="message"  value="Update fail." type="hidden"/>' .
	        		'</form>';
				$html .= '<script>document.uploadpage_form.submit();</script>';
			}
			
			//die();
			echo($html);				
			
			//header("location: index.php");
			exit();
			//echo("Testing END.");
		}		
	}
?>