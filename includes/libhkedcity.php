<?php
// editing by 
 
/********************
 * 
 * 	Date : 2015-10-30	(Bill)
 * 		 - added function getCentralServerPath(), getSendHKEdCityData(), getEncryptHKEdCityData(), getHKEdCityKey()
 * 
 ********************/
if (!defined("LIBHKEDCITY_DEFINED")) {
	define("LIBHKEDCITY_DEFINED", true);
	
	class libhkedcity extends libdb {
		function libhkedcity() {
			$this->libdb();
			$this->ModuleTitle = 'HKEdCity';
		}
		
		function enabledSingleSignOn() {
			global $ssoservice;
			
			return $ssoservice["HKEdCity"]["Valid"]; 
		}
		
		function getCentralServerPath() {
			return "http://api.eclasscloud.hk/edconnect/edlogin.php";
		}
		
		function getSendHKEdCityData($type="login") {
			global $PATH_WRT_ROOT;
			if($PATH_WRT_ROOT=="") $PATH_WRT_ROOT = "../";
			
			$UserCurrentIP = $_SERVER["REMOTE_ADDR"];
			$schoolURL = curPageURL(1, 0);
		
			return $this->getEncryptHKEdCityData("$schoolURL||$type||$UserCurrentIP", $this->getHKEdCityKey());
		}
		
		function getEncryptHKEdCityData($text, $key){
			global $PATH_WRT_ROOT;
			
			include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");
			$lurlparahandler = new liburlparahandler(liburlparahandler::actionEncrypt, $text, $key);
			return $lurlparahandler->getParaEncrypted();
		}
		
		function getHKEdCityKey($action=""){
			$key1 = "Hk1eD%c7I";
			$key2 = "i^8q_Td6y";
			return $key1.$action.$key2;
		}
	}
}
?>