<?
# Modifying :  

if (!defined("LIBPF_DBS_TRANSCRIPT_UI_DEFINED"))
{
	define("LIBPF_DBS_TRANSCRIPT_UI_DEFINED",true);

	class libpf_dbs_transcript_ui {
	    private $libpf_dbs;
		private $linterface;
		
		public function __construct() {
		    global $lpf_dbs;
		    $this->libpf_dbs = $lpf_dbs;
		}
		
		public function setObjFunction($obj) {
		    $this->libpf_dbs = $obj;
		}
		
		public function getImageMmgtTab($curTab) {
		    global $Lang;
		    
		    $tagAry = array();
		    $tagAry[] = array($Lang['iPortfolio']['DBSTranscript']['ImageManagement']['BackCover'], "back_cover.php", ($curTab=='back_cover'));
		    $tagAry[] = array($Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Header'], "header.php", ($curTab=='header'));
		    $tagAry[] = array($Lang['iPortfolio']['DBSTranscript']['ImageManagement']['SchoolSeal'], "school_seal.php", ($curTab=='school_seal'));
		    $tagAry[] = array($Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Signature'], "signature.php", ($curTab=='signature'));
		    
		    return $tagAry;
		}
		
		public function getPredictedGradeTab($curTab, $limitRight=false) {
		    global $Lang;
		    
		    $tagAry = array();
		    if($limitRight) {
		        $tagAry[] = array($Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Adjustment'], "");
		    } else {
    		    $tagAry[] = array($Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Adjustment'], "index.php", ($curTab=='index'));
    		    $tagAry[] = array($Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['AdjustmentPeriodSetting'], "period.php", ($curTab=='period'));
		    }
		    
		    return $tagAry;
		}
		
		public function getCurriculumSelection($selectionIDName, $selectedCurriculumID='', $onChange='', $noFirst=0, $firstTitleText='', $useCode=false, $forPredictedGrade=false)
		{
		    global $Lang;
		    
		    $selectArr = array();
		    $CurriculumArr = $this->libpf_dbs->getCurriculumSetting();
		    for ($i=0; $i<count($CurriculumArr); $i++)
		    {
		        $thisCurriculumID = $CurriculumArr[$i]['CurriculumID'];
		        $thisCurriculumIDName = Get_Lang_Selection($CurriculumArr[$i]['NameCh'], $CurriculumArr[$i]['NameEn']);
		        $thisCurriculumCode = $CurriculumArr[$i]['Code'];
		        if($forPredictedGrade && !($thisCurriculumCode == 'IB' || $thisCurriculumCode == 'IBDP' || $thisCurriculumCode == 'DSE' || $thisCurriculumCode == 'HKDSE')) {
		            continue;
		        }
		        
		        $selectArr[$thisCurriculumID] = $useCode? $thisCurriculumCode : $thisCurriculumIDName;
		    }
		    
		    $onchange= '';
		    if ($onChange != "") {
		        $onchange= ' onchange="'.$onChange.'" ';
		    }
		    $selectionTags = ' id="'.$selectionIDName.'" name="'.$selectionIDName.'" '.$onchange;
	        
	        if ($noFirst == 0) {
	            if($firstTitleText != '') {
	                $firstTitle = Get_Selection_First_Title($firstTitleText);
	            } else {
	                $firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Curriculum']);
	            }
	        }
	        $curriculumSelection = getSelectByAssoArray($selectArr, $selectionTags, $selectedCurriculumID, $isAll=0, $noFirst, $firstTitle);
	        
	        return $curriculumSelection;
		}
		
		public function getReportTypeSelection($selectionIDName, $selectedReportTypeID='', $onChange='', $noFirst=0, $firstTitleText='')
		{
		    global $ipf_cfg, $Lang;
		    
		    $selectArr = array();
		    $ReportTypeArr = $ipf_cfg["DSBTranscript"]["REPORT_TYPE"];
		    foreach ($ReportTypeArr as $thisReportType => $thisReportTypeID) {
		        $selectArr[] = array($thisReportTypeID, $Lang['iPortfolio']['DBSTranscript'][$thisReportType]);
		    }
		    
		    $onchange = '';
		    if ($onChange != "") {
		        $onchange = ' onchange="'.$onChange.'" ';
		    }
		    $selectionTags = ' id="'.$selectionIDName.'" name="'.$selectionIDName.'" '.$onchange;
		    
		    if ($noFirst == 0) {
		        if($firstTitleText != '') {
		            $firstTitle = Get_Selection_First_Title($firstTitleText);
		        } else {
		            $firstTitle = Get_Selection_First_Title($Lang['iPortfolio']['DBSTranscript']['SelectReportType']);
		        }
		    }
		    $reportTypeSelection = getSelectByArray($selectArr, $selectionTags, $selectedReportTypeID, $isAll=0, $noFirst, $firstTitle);
		    
		    return $reportTypeSelection;
		}
		
		public function getAcademicYearClassHistorySelection($selectionIDName, $selectedVal='', $onChange='', $noFirst=0, $firstTitleText='')
		{
		    $onchange = '';
		    if ($onChange != "") {
		        $onchange = ' onchange="'.$onChange.'" ';
		    }
		    $selectionTags = ' id="'.$selectionIDName.'" name="'.$selectionIDName.'" '.$onchange;
		    
		    $yearList = $this->libpf_dbs->GetAcademicYearListFromClassHistory();
		    return getSelectByArray($yearList, $selectionTags, $selectedVal, $isAll=0, $noFirst, $firstTitle='');
		}
	}
}