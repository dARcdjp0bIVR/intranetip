<?php
// modify : 

/*********************************************************
 * Date:	2017-11-07 Bill		[2017-0403-1552-04240]
 * Details: modified getSchoolRecordTags() - support HKUGA access	($sys_custom['iPf']['HKUGA_eDis_Tab'])
 * Date:	2017-08-03 Carlos
 * Details: $sys_custom['LivingHomeopathy'] modified getSchoolRecordTags(), customize the tags
 * Date:	2016-11-29 Omas
 * Details: modified getSchoolRecordTags() - stpaulPAS access right
 * Date:	2013-03-20 Rita
 * Details: add getReportTags();
 *********************************************************/
 
###########################################################
# This class intends to return the tab entry array only,
# but not including menu generation
###########################################################

class libpf_tabmenu{

	static function getSlpMgmtTopTags($ParCurrentIndex=0) {
		global $ipf_cfg, $OLEDefaultView,$isReadOnly;
		
		// Tab Menu Settings
//		debug_r("isReadOnly is ->".$isReadOnly);
		if($isReadOnly == 1){
			$TabMenuArr = $ipf_cfg["TOP_TAB"]["SLP_MGMT_VIEW"];
		}else{
			$TabMenuArr = $ipf_cfg["TOP_TAB"]["SLP_MGMT"];
		}

    # show first page as student view according to system setting
    # OLE
    if($OLEDefaultView[0] == "student")
  	{
  		$TabMenuArr[IPF_CFG_SLP_MGMT_OLE] = $ipf_cfg["TOP_TAB"]["SLP_MGMT_RPC"][IPF_CFG_SLP_MGMT_OLE];
  	}
  	# external performance
    if($OLEDefaultView[1] == "student")
  	{
  		$TabMenuArr[IPF_CFG_SLP_MGMT_OUTSIDE] = $ipf_cfg["TOP_TAB"]["SLP_MGMT_RPC"][IPF_CFG_SLP_MGMT_OUTSIDE];
  	}
		$TabMenuArr[$ParCurrentIndex][2] = 1;
  
		return $TabMenuArr;
	}

  static function getSlpMgmtTabs($ParTagType, $ParCurrentIndex=0) {
  	global $ipf_cfg, $ck_function_rights, $OLEDefaultView;
  	
  	// Tab Menu Settings
  	$TabMenuArr = $ipf_cfg["MODULE_TAB"]["SLP_MGMT"][$ParTagType];
  	
  	$TabMenuArr[$ParCurrentIndex][2] = 1;
  	return $TabMenuArr;
  }
  
  static function getLpMgmtTabs($ParCurrentIndex=0) {
  	global $ipf_cfg, $ck_function_rights, $OLEDefaultView;
  	
  	// Tab Menu Settings
  	$TabMenuArr = $ipf_cfg["MODULE_TAB"]["LP_MGMT"];
    	
  	$TabMenuArr[$ParCurrentIndex][2] = 1;
  	return $TabMenuArr;
  }
  
	static function getSlpSettingsTopTags($ParCurrentIndex=0) {
		global $ipf_cfg;
		
		// Tab Menu Settings
		$TabMenuArr = $ipf_cfg["TOP_TAB"]["SLP_SETTING"];
		$TabMenuArr[$ParCurrentIndex][2] = 1;
		return $TabMenuArr;
	}
  
	static function getSlpSettingsTags($ParTagType, $ParCurrentIndex=0) {
		global $ipf_cfg;
		
		// Tab Menu Settings
		$TabMenuArr = $ipf_cfg["MODULE_TAB"]["SLP_SETTING"][$ParTagType];
		$TabMenuArr[$ParCurrentIndex][2] = 1;
		return $TabMenuArr;
	}
	
	static function getLpSettingsTags($ParCurrentIndex=0) {
		global $ipf_cfg;
		
		// Tab Menu Settings
		$TabMenuArr = $ipf_cfg["MODULE_TAB"]["LP_SETTING"];
		$TabMenuArr[$ParCurrentIndex][2] = 1;
		return $TabMenuArr;
	}
	
	static function getSaSettingsTags($ParCurrentIndex=0) {
		global $ipf_cfg;
		
		// Tab Menu Settings
		$TabMenuArr = $ipf_cfg["MODULE_TAB"]["SA_SETTING"];
		$TabMenuArr[$ParCurrentIndex][2] = 1;
		return $TabMenuArr;
	}
	
	static function getTcSettingsTags($ParCurrentIndex=0) {
		global $ipf_cfg;
		
		// Tab Menu Settings
		$TabMenuArr = $ipf_cfg["MODULE_TAB"]["TC_SETTING"];
		$TabMenuArr[$ParCurrentIndex][2] = 1;
		return $TabMenuArr;
	}
	
	static function getGroupSettingsTopTags($ParCurrentIndex=0) {
		global $ipf_cfg;
		
		// Tab Menu Settings
		$TabMenuArr = $ipf_cfg["TOP_TAB"]["GROUP_SETTING"];
		$TabMenuArr[$ParCurrentIndex][2] = 1;
		return $TabMenuArr;
	}
	
	static function getGroupSettingsTags($ParTagType, $ParCurrentIndex=0) {
		global $ipf_cfg;
		
		// Tab Menu Settings
		$TabMenuArr = $ipf_cfg["MODULE_TAB"]["GROUP_SETTING"][$ParTagType];
		$TabMenuArr[$ParCurrentIndex][2] = 1;
		return $TabMenuArr;
	}

  static function getSchoolRecordTags($ParCurrentKey="")
  {
    global $ipf_cfg, $UserType, $ck_user_rights_ext, $sys_custom, $ec_iPortfolio;
    
    $lpf = new libportfolio();
    
    $ncsFilterArr = array();
    if(isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!=""){
        $ncsFilterArr = array('merit', 'award', 'teacher_comment', 'ole',
            'plkp', 'attendance','service','assessment_report');
    }
    
    switch($UserType)
    {
      case 1:
        $availTab = array();
        foreach($ipf_cfg["MODULE_TAB"]["SR_MGMT"] AS $key => $tabDetail) {
          if(in_array($key, $ncsFilterArr))continue;
          $access_right = ($key == "plkp") ? "ole" : $key;
          $current = ($key == $ParCurrentKey) ? 1 : 0;

          if(in_array($key, array('third_party','pl2_quiz_report','assessment')) && (isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
              $current = ($key == $ParCurrentKey) ? 1 : 0;
              $availTab[] = array($tabDetail[0], $tabDetail[1], $current);
              continue;
          }
          
          if($lpf->HAS_RIGHT($access_right) || ($sys_custom['iPf']['HKUGA_eDis_Tab'] && in_array($key, array('student_record', 'pastoral_record'))))
          {
            $availTab[] = array($tabDetail[0], $tabDetail[1], $current);
          }
        }
        break;
      case 2:
      case 3:
        $availTab = array();
        
        foreach($ipf_cfg["MODULE_TAB"]["SR_VIEW"] AS $key => $tabDetail) {
          if(in_array($key, $ncsFilterArr))continue;
          if(strpos($ck_user_rights_ext, $key) !== false)
          {
            $current = ($key == $ParCurrentKey) ? 1 : 0;
            $availTab[] = array($tabDetail[0], $tabDetail[1], $current);
            continue;
          }
          // st paul cust only allow student to access
          if($key == 'stpaul_academic_points' && $sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'] && $_SESSION['UserType'] == 2)
          {
          	$current = ($key == $ParCurrentKey) ? 1 : 0;
          	$availTab[] = array($tabDetail[0], $tabDetail[1], $current);
          	continue;
          }
          
          if(in_array($key, array('third_party','pl2_quiz_report','assessment')) && (isset($_SESSION['ncs_role'])&&$_SESSION['ncs_role']!="")){
          	$current = ($key == $ParCurrentKey) ? 1 : 0;
          	$availTab[] = array($tabDetail[0], $tabDetail[1], $current);
          	continue;
          }
          
          if($sys_custom['LivingHomeopathy'] && in_array($key,array('activity','homework'))){
          	$current = ($key == $ParCurrentKey) ? 1 : 0;
          	$availTab[] = array($tabDetail[0], $tabDetail[1], $current);
          	continue;
          }
        }
    }
    
    return $availTab;
  }
  
  static function getAssessmentStatTags($ParCurrentKey="")
  {
  	global $ipf_cfg;
		
		// Tab Menu Settings
		$TabMenuArr = $ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"];
		$TabMenuArr[$ParCurrentKey][2] = 1;
		return $TabMenuArr;
  }
  
  static function getJupasStudentSettingsTags($ParTagType, $ParTagTask) {
		global $oea_cfg, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/portfolio25/oea/liboea.php');
		$liboea = new liboea();
		
		$tabTaskArr = explode('_', $ParTagTask);	// $task = 'tabName_xxx';
		$tabTask = $tabTaskArr[0];
		
		$CurrentTabIndex = 0;
		switch ($tabTask) {
			case "index":
		    case "oea":
		    	$CurrentTabIndex = IPF_OEA_OEA_TAB;		// IPF_OEA_OEA_TAB is defined in oeaConfig.inc.php
		    	break;
		    case "addiInfo":
		    	$CurrentTabIndex = IPF_OEA_ADDITIONAL_INFO_TAB;
		    	break;
			case "report":
				$CurrentTabIndex = IPF_OEA_REPORT_TAB;
				break;
//		    case "ability":
//		    	
//		        $CurrentTabIndex = IPF_OEA_ABILITY_TAB;
//		        break;
//		    case "academic":
//		        $CurrentTabIndex = IPF_OEA_ACADEMIC_TAB;
//		        break;
//		    case "suppInfo":
//		        $CurrentTabIndex = IPF_OEA_SUPPLEMENTARY_INFO_TAB;
//		        break;
		    default:
		        break;
		}
		
		// Tab Menu Settings
		$TabMenuArr = $oea_cfg["TOP_TAB"][$ParTagType];
		$TabMenuArr[$CurrentTabIndex][2] = 1;
		
		if (!$liboea->Is_Module_In_Use($liboea->getOEAItemInternalCode())) {
			unset($TabMenuArr[IPF_OEA_OEA_TAB]);
		}
		if (!$liboea->Is_Module_In_Use($liboea->getAddiInfoInternalCode())) {
			unset($TabMenuArr[IPF_OEA_ADDITIONAL_INFO_TAB]);
		}
		
		return $TabMenuArr;
	}
	
	
	static function getJupasAdminSettingsTags($ParTagType, $ParTagTask) {
		global $oea_cfg;
		
		$tabTaskArr = explode('_', $ParTagTask);	// $task = 'tabName_xxx';
		$tabTask = $tabTaskArr[0];
		
		$CurrentTabIndex = 0;
		switch ($tabTask) {
			case "jupasBasic":
				$CurrentTabIndex = IPF_JUPAS_BASIC_TAB;
				break;
			case "oeaSettings":
		    case "oeaItemMap":
		    case "oeaItemMap_importStep1":
		    case "oeaItemMap_importStep2":
		    case "oeaItemMap_importStep3":
		    case "oeaRoleMap":
		        $CurrentTabIndex = IPF_OEA_SETTINGS_TAB;		
		        break;
//		    case "appNoImport":
//		        $CurrentTabIndex = IPF_OEA_APPNO_IMPORT_TAB;
//		        break;
		    case "academicSettings":
		    case "academicSubject":
		        $CurrentTabIndex = IPF_OEA_ACADEMIC_SETTINGS_TAB;
		        break;
		    default:
		    	$CurrentTabIndex = IPF_JUPAS_BASIC_TAB;
		        break;
		}
		
		// Tab Menu Settings
		$TabMenuArr = $oea_cfg["TOP_TAB"][$ParTagType];
		$TabMenuArr[$CurrentTabIndex][2] = 1;
		return $TabMenuArr;
	}
	
	static function getJupasAdminAcademicSettingsTags($ParTagType, $ParTagTask) {
		global $oea_cfg;
		
		$tabTaskArr = explode('_', $ParTagTask);	// $task = 'tabName_xxx';
		$tabTask = $tabTaskArr[0];
		
		$CurrentTabIndex = 0;
		switch ($tabTask) {
			case "academicSettings":
		        $CurrentTabIndex = IPF_OEA_SETTINGS_ACADEMIC_BASIC;		
		        break;
		    case "academicSubject":
		        $CurrentTabIndex = IPF_OEA_SETTINGS_ACADEMIC_SUBJECT;		
		        break;
		    case "academicPercentile":
		        $CurrentTabIndex = IPF_OEA_SETTINGS_ACADEMIC_PERCENTILE_AND_OVERALL_RATING;
		        break;	
		    default:
		    	$CurrentTabIndex = IPF_OEA_SETTINGS_ACADEMIC_BASIC;
		        break;
		}
		
		// Tab Menu Settings
		$TabMenuArr = $oea_cfg["TOP_TAB"][$ParTagType];
		$TabMenuArr[$CurrentTabIndex][2] = 1;
		return $TabMenuArr;
	}
	
	static function getJupasAdminOEASettingsTags($ParTagType, $ParTagTask) {
		global $oea_cfg;
		
		$tabTaskArr = explode('_', $ParTagTask);	// $task = 'tabName_xxx';
		$tabTask = $tabTaskArr[0];
		
		$CurrentTabIndex = 0;
		switch ($tabTask) {
			case "oeaSettings":
		        $CurrentTabIndex = IPF_OEA_SETTINGS_OEA_BASIC;		
		        break;
		    case "oeaItemMap":
		    case "oeaItemMap_importStep1":
		    case "oeaItemMap_importStep2":
		    case "oeaItemMap_importStep3":
		        $CurrentTabIndex = IPF_OEA_SETTINGS_OEA_OLE_MAPPING;		
		        break;
		    case "oeaRoleMap":
		    	$CurrentTabIndex = IPF_OEA_SETTINGS_OEA_OLE_ROLE_MAPPING;		
		        break;
		    default:
		    	$CurrentTabIndex = IPF_OEA_SETTINGS_OEA_BASIC;
		        break;
		}
		
		// Tab Menu Settings
		$TabMenuArr = $oea_cfg["TOP_TAB"][$ParTagType];
		$TabMenuArr[$CurrentTabIndex][2] = 1;
		return $TabMenuArr;
	}
	
	static function getVolunteerTeacherTopTags($ParCurrentIndex=0) {
		global $ipf_cfg, $lpf;
		
		// Tab Menu Settings
		$TabMenuArr = $ipf_cfg["TOP_TAB"]["VOLUNTEER_TEACHER_MGMT"];
		$TabMenuArr[$ParCurrentIndex][2] = 1;
		
		if (!$lpf->IS_IPF_SUPERADMIN()) {
			unset($TabMenuArr[IPF_CFG_VOLUNTEER_TEACHER_MGMT_EVENT]);
		}		
  
		return $TabMenuArr;
	}
	
	static function getReportTags($ParCurrentKey="")
	{
		global $ipf_cfg, $luser, $objiPf;
		$availTab = array();
		foreach($ipf_cfg["MODULE_TAB"]["REPORT"] AS $key => $tabDetail) {
			
			if ($key == 'StudentLearningProfile') {
				if (!$objiPf->showSLPReportPrintingForStduent($luser)) {
					continue;
				}
			}
			else if ($key == 'dynamicReport') {
				if (!$objiPf->showDynamicReportPrintingForStudent($luser)) {
					continue;
				}
			}
			
			$current = ($key == $ParCurrentKey) ? 1 : 0;
			$availTab[] = array($tabDetail[0], $tabDetail[1], $current);
		}
		return $availTab;
	}
}

?>