<?php

include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/libaccessright.php");
$ldb = new libdb();

class libpf_accessright extends libaccessright {

  var $user_id;
  var $course_user_id;

  function __construct(){
  
  
  }
  
  function CLEAR_IPF_ADMIN(){
    global $ldb, $eclass_db;
    
    $sql =  "
              UPDATE
                {$eclass_db}.PORTFOLIO_TEACHER
              SET
                IsAdmin = '0',
                ModifiedDate = NOW()
            ";
    $ldb->db_db_query($sql);
  }
  
  function UPDATE_IPF_ADMIN(){
    global $ldb, $eclass_db;
    
    $sql =  "
              UPDATE
                {$eclass_db}.PORTFOLIO_TEACHER
              SET
                IsAdmin = '1',
                ModifiedDate = NOW()
              WHERE
                UserID = '".$this->GET_CLASS_VARIABLE("user_id")."' OR
                CourseUserID = '".$this->GET_CLASS_VARIABLE("course_user_id")."'
            ";

    $ldb->db_db_query($sql);
  }
  
  function GET_IPF_ADMIN(){
    global $ldb, $eclass_db;
    
    $sql =  "
              SELECT
                UserID
              FROM
                {$eclass_db}.PORTFOLIO_TEACHER
              WHERE
                IsAdmin = '1'
            ";
    $returnArray = $ldb->returnVector($sql);
    
    return $returnArray;
  }

  function IS_IPF_ADMIN($ParUserID){

    $ipf_admin_arr = $this->GET_IPF_ADMIN();
    
    return in_array($ParUserID, $ipf_admin_arr);
  }
  
  function GET_PORTFOLIO_TEACHER(){
    global $ldb, $eclass_db;
  
    $sql =  "
              SELECT
                UserID
              FROM
                {$eclass_db}.PORTFOLIO_TEACHER
            ";
    $returnArray = $ldb->returnVector($sql);
    
    return $returnArray;
  }
  
  function GET_PORTFOLIO_MANAGE_RIGHT($ParUserID)
  {
    global $intranet_root;
  
    if($this->IS_IPF_ADMIN($ParUserID))
    {
      include_once($intranet_root."/includes/portfolio25/accessright.inc.php");
      
      if(is_array($SysMgmtRightList))
      {
        foreach($SysMgmtRightList AS $Module => $SectionList)
        {
          foreach($SectionList AS $Section => $ActionList)
          {
            if($Section == "0") continue;
            $returnArray[$Module][$Section] = array_keys($ActionList);
          }
        }
      } 
    }
    else
    {
      $returnArray = $this->retrieveUserAccessRight($ParUserID);
    }
    
    return $returnArray;
  }
  
  function DELETE_ACCESS_RIGHT_GROUP_SETTING($GroupID)
  {
    global $ldb;
  
    $sql = "DELETE FROM ACCESS_RIGHT_GROUP_SETTING WHERE GroupID=$GroupID";
    $ldb->db_db_query($sql);
  }
  
	function INSERT_ACCESS_RIGHT($GroupID, $Module, $array)
	{
    global $ldb;

    if(is_array($array))
    {
      foreach($array AS $SectionName => $FunctionList)
      {
        if(is_array($FunctionList))
        {
          foreach($FunctionList AS $FunctionName => $ActionList)
          {
            for($i=0;$i<count($ActionList);$i++) {
              $ActionName = $ActionList[$i];
              $sql = "INSERT INTO ACCESS_RIGHT_GROUP_SETTING SET GroupID=$GroupID, Module='$Module', Section='$SectionName', Function='$FunctionName', Action='$ActionName', DateInput=NOW(), DateModified=NOW()";
              $ldb->db_db_query($sql);
            }
          }
        }
      }
    }
	}
  
  function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    $this->{$ParVariableName} = $ParValue;
  }
  
  function GET_CLASS_VARIABLE($ParVariableName)
  {
    return $this->{$ParVariableName};
  }
}


?>