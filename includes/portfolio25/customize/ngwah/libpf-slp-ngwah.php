<?php

class studentInfo{
	
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $uid;
	private $academicYearID;
	private $printType;
	
	private $SAS_records;
	private $SAS_category;
	private $total_credit;
	
	private $SAS_award_settings;
	private $SAS_category_settings;
	
	private $html;
	private $EmptySymbol;
	
	private $row_count;
	private $total_row;
	private $first_page_header;
	private $second_page_header;

	public function studentInfo($uid='', $yearID=''){
		global $intranet_db, $eclass_db;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearID = $yearID;

		$this->SAS_category = array();
		$this->SAS_category[1] = "A) 認可職銜 Elected/Appointed Post";
		$this->SAS_category[2] = "B) 獎項成就 Achievement";
		$this->SAS_category[3] = "C) 校內傑出表現 Outstanding Performances (Inside School)";
		$this->SAS_category[4] = "D) 校外傑出表現 Outstanding Performances (Outside School)";
		$this->SAS_category[5] = "E) 服務 Services";
		$this->SAS_category[6] = "F) 其他學習活動 Other Learning Activities";
		
		$this->total_credit = 0;
		
		$this->html = '';
		$this->EmptySymbol = '--';
		
		// 1 row: ~ 18px
		$this->total_row = 56;
		// Header & Student Name ~ 180px
		$this->first_page_header = 10;
		// Other Page ~ 60px
		$this->second_page_header = 3.2;
		
		$this->row_count = $this->total_row - $this->first_page_header;
	}
	
	public function getStudentSAS($set=false){
		$sql = "SELECT
					OS.RecordID,
					OP.Title,
					OC.ChiTitle AS Category,
					OP.Details,
					OS.Role,
					OS.Hours,
					OS.Achievement,
					OP.SASCategory,
					IF(OS.SASPoint !='' OR OS.SASPoint IS NOT NULL, OS.SASPoint, OP.SASPoint) AS SASPoint,
					IU.ChineseName AS TeacherName
				FROM
					{$this->eclass_db}.OLE_STUDENT OS
				INNER JOIN 
					{$this->eclass_db}.OLE_PROGRAM OP ON (OS.ProgramID = OP.ProgramID)
				LEFT JOIN
					{$this->eclass_db}.OLE_CATEGORY OC ON (OP.Category = OC.RecordID)
				LEFT JOIN
					{$this->intranet_db}.INTRANET_USER IU ON (OP.CreatorID = IU.USerID)
				WHERE
					OS.UserID = '$this->uid' AND OP.AcademicYearID = '$this->academicYearID' AND OS.RecordStatus IN ('2', '4')
					AND OP.SASCategory != '0' AND OP.SASCategory != '' AND OP.SASCategory IS NOT NULL";
					
		$returnAry = BuildMultiKeyAssoc($this->objDB->returnResultSet($sql), array("SASCategory", "RecordID"));
		
		if($set){
			$this->SAS_records = $returnAry;
		} else {
			return $returnAry;
		}
	}
	
	public function getSASSettings($settingsName, $set=false){
		$cond = $settingsName? " AND SettingName = '".trim($settingsName)."'" : "";
		$cond .= " AND AcademicYearID = '$this->academicYearID'";
		
		$sql = "SELECT
					SettingID,
					SettingName,
					SettingValue
				FROM
					{$this->eclass_db}.OLE_SAS_SETTINGS
				Where 
					1 $cond";	
					
//		return BuildMultiKeyAssoc($this->objDB->returnResultSet($sql), array("SettingName", "SettingID"));
		$returnAry = BuildMultiKeyAssoc($this->objDB->returnResultSet($sql), array("SettingName"));
		
		if($set){
			
			# Handle empty category settings
			if($returnAry['category']['SettingValue'] == ''){
				$returnAry['category']['SettingValue'] = "1###30;;2###20;;3###10;;4###5;;5###15";
			}
			# Handle empty award settings
			if($returnAry['award']['SettingValue'] == ''){
				$returnAry['award']['SettingValue'] = "金獎###Gold Award###56###;;銀獎###Silver Award###41###55;;銅獎###Bronze Award###25###40";
			}
			
			# Category Settings
			$sas_category = explode(';;', $returnAry['category']['SettingValue']);
			$category_settings = array();
			
			for($j = 0; $j<count($sas_category); $j++){
				list($index, $data) = explode('###', $sas_category[$j]);
				$category_settings[$index] = $data;
			}
			$this->SAS_category_settings = $category_settings;
	
			# Award Settings
			$sas_award = explode(';;', $returnAry['award']['SettingValue']);
			$award_settings = array();
			
			for($j = 0; $j<count($sas_award); $j++){
				list($chi, $en, $st_pt, $end_pt) = explode('###', $sas_award[$j]);
				$award_settings[$chi]['start'] = $st_pt;
				$award_settings[$chi]['end'] = $end_pt;
			}
			$this->SAS_award_settings = $award_settings;
		} else {
			return $returnAry;
		}
	}
	
	public function setSASSettings($settingsName, $settingsValue){
		global $_SESSION;
		
		$sql = "SELECT Count(*) FROM {$this->eclass_db}.OLE_SAS_SETTINGS
					WHERE SettingName='$settingsName' AND AcademicYearID='$this->academicYearID'";
		$result = $this->objDB->returnVector($sql);
		
		if($result[0] > 0){
			$sql = "UPDATE {$this->eclass_db}.OLE_SAS_SETTINGS SET SettingValue = '$settingsValue', ModifiedDate = now(), ModifyBy = '".$_SESSION['UserID']."'
						WHERE SettingName='$settingsName' AND AcademicYearID='$this->academicYearID'";		
		} else {
			$sql = "INSERT INTO {$this->eclass_db}.OLE_SAS_SETTINGS (AcademicYearID, SettingName, SettingValue, InputDate, InputBy, ModifiedDate, ModifyBy)
						VALUES ('$this->academicYearID', '$settingsName', '$settingsValue', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
		}

		return $this->objDB->db_db_query($sql);
	}
	
	public function getDefaultSASSettings($settingsName, $langs=''){
		$data_ary = array();
		
		if($settingsName == 'category'){
			$data_Ary[] = array(1, $langs['Post'], 30);
			$data_Ary[] = array(2, $langs['Achievement'], 20);
			$data_Ary[] = array(3, $langs['Internal_Performance'], 10);
			$data_Ary[] = array(4, $langs['External_Performance'], 5);
			$data_Ary[] = array(5, $langs['Service'], 15);
		}
		else if($settingsName == 'award'){
			$data_Ary[] = array('金獎', 'Gold Award', '56', '');
			$data_Ary[] = array('銀獎', 'Silver Award', '41', '55');
			$data_Ary[] = array('銅獎', 'Bronze Award', '25', '40');		
		}
		
		return $data_Ary;
	}
	
	public function getCategoryName(){
		return array('Post', 'Achievement', 'Internal_Performance', 'External_Performance', 'Service');
	}
	
	public function setPrintType($printType){
		$this->printType = $printType;
	}
	
	public function setSASCategory($setting){
		$this->SAS_category_settings = $setting;
	}
	
	public function setReportHeader($targetYear){
		$sch_logo = "../customizeReport/templates/ngwah/ngwah_logo.jpg";
		if($this->printType == 'pdf'){
			$this->html .= 	"<table>
							<tr>
								<td class=\"sch_logo_td\"><img src=\"$sch_logo\"></td>
								<td><table>
										<tr>
											<td><h2 class=\"chi_sch_name\">天主教伍華中學</h2></td>
										</tr>
										<tr>
											<td><h2>學生課外活動及獎勵紀錄表 $targetYear</h2></td>
										</tr>
								</table></td>
							</tr>
						</table>";
		} else {		
			$this->html .= 	"<div align=\"center\"><div class=\"main_container\">";
			$this->html .= 	"<table class=\"report_header\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" align=\"center\">
								<tr>
									<td class=\"sch_logo_td\"><img src=\"$sch_logo\"></td>
									<td><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" align=\"center\">
											<tr>
												<td><h2 class=\"chi_sch_name\">天主教伍華中學</h2></td>
											</tr>
											<tr>
												<td><h2>學生課外活動及獎勵紀錄表 $targetYear</h2></td>
											</tr>
									</table></td>
								</tr>
							</table>";
		}
	}
	
	public function setReportStudent($student, $issuedate){
		
		$this->html .= "<table class=\"student_info\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" align=\"center\">
							<tr>
								<td width=\"20%\" align=\"left\">學生姓名:</td>
								<td width=\"45%\" align=\"left\">(中文) ".$student['ChineseName']."</td>
								<td width=\"20%\" align=\"left\">班別:</td>
								<td width=\"15%\" align=\"left\">".$student['ClassTitleEN']."(".$student['ClassNumber'].")</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td align=\"left\">(英文) ".$student['EnglishName']."</td>
								<td align=\"left\">列印日期:</td>
								<td align=\"left\">".$issuedate."</td>
							</tr>
						</table>
						<br>";
	}
	
	public function setPageBreak($types=''){
		if($this->printType == 'pdf'){
			return;
		}

		if($types == 'records'){
			$this->html .= "</table>";
		}
		
		$this->html .= "</div><div style=\"page-break-after:always\">&nbsp;</div>";
		$this->html .= "<div class=\"main_container\"><div class=\"page_break_headers\">&nbsp;</div>";
		
		if($types == 'records'){
			$this->html .= "<table class=\"record_table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">";
		}
//		debug_pr($this->row_count);
		$this->row_count = $this->total_row - $this->second_page_header;
	}
	
	public function setReportRecords($targetCat){
		$total_cat_pt = 0;
		$max_pt = $this->SAS_category_settings[$targetCat];
		
		if($this->row_count < 3.4){
			$this->setPageBreak('');
		}
		
		if($this->printType == 'pdf'){
			$headers = "<hr><font size=\"11\">".$this->SAS_category[$targetCat]." </font><hr>";
//			$content_style_start = "<font size=\"9.5\">";
//			$content_style_end = "</font>";
			$pt_style_start = "<u>";
			$pt_style_end = "</u>";
		} else {
			$headers = "<div class=\"record_table_header\" align=\"left\"> ".$this->SAS_category[$targetCat]." </div>";
			$border_style = "style=\"border-bottom: 1px solid #000000\"";
		}
		
		$this->html .= "$headers
						<table class=\"record_table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">
							<tr>
								<td width=\"10%\" align=\"center\"> 編號 </td>
								<td width=\"45%\" align=\"left\"> 活動項目/職務/獎頂/成就/時數 </td>
								<td width=\"40%\" align=\"center\"> 負責老師 </td>
								<td width=\"5%\" align=\"center\"> 積點 </td>
							</tr>";
		$this->row_count -= 2.2;
		
		$sas_record = $this->SAS_records[$targetCat];
		if(count($sas_record) > 0){
			$count = 1;
		
			foreach($sas_record as $recordid => $current_record){
				if($this->row_count < 1.2){
					$this->setPageBreak('records');
				}
				
				$details_1 = $current_record['Category'].' -- '.$current_record['Title'];
				
				if($targetCat == 1){
					$record_role = $current_record['Role']? $current_record['Role'] : "--";
					$details_1 .= ' : '.$record_role;
				} else if($targetCat == 5) {
					$time = $current_record['Hours']? $current_record['Hours'] : "0";
					$details_1 .= ' ( '.$time.'時 ) ';
				} else if($targetCat != 6) {
					$achieve = $current_record['Achievement']? $current_record['Achievement'] : "--";
					$details_1 .= ' : '.$achieve;
				}
				
				$pt = ($current_record['SASPoint'] != '')? $current_record['SASPoint'] : 0;
				
				$this->html .= "<tr>
									<td align=\"center\"> $count </td>
									<td align=\"left\"> $details_1 </td>
									<td align=\"center\"> ".$current_record['TeacherName']." </td>
									<td align=\"center\"> $pt </td>
								</tr>";
								
//				if($details_2 != ''){
//				$this->html .= "<tr>
//									<td width=\"10%\" align=\"center\">$content_style_start &nbsp; $content_style_end</td>
//									<td width=\"45%\" align=\"left\">$content_style_start $details_2 $content_style_end</td>
//									<td width=\"40%\" align=\"center\">$content_style_start &nbsp; $content_style_end</td>
//									<td width=\"5%\" align=\"center\">$content_style_start &nbsp; $content_style_end</td>
//								</tr>";
//				}
				$this->row_count--;
				
				$total_cat_pt += $pt;
				$count++;
			}
		}
		else {
			$this->html .= "<tr>
								<td align=\"center\">&nbsp;</td>
								<td align=\"left\"> 不適用 </td>
								<td align=\"center\">&nbsp;</td>
								<td align=\"center\">&nbsp;</td>
							</tr>";
			$this->row_count--;
		}
		$this->html .= "</table>";
		
		$max_pt = (!$max_pt)? 0 : $max_pt;
		$real_cat_pt = ($total_cat_pt > $max_pt)? $max_pt : $total_cat_pt;
		
		if($targetCat == 6){
			$max_pt = "--";
			$real_cat_pt = 0;
		}

		if($this->row_count < 1.8){
			$this->setPageBreak('');
		}
		
		$this->html .= "<table class=\"record_table_summary\" width=\"100%\">
							<tr>
								<td width=\"65%\">&nbsp;</td>
								<td width=\"80px\" align=\"center\"> 最高計算積點  </td>
								<td width=\"15px\" align=\"center\"> $max_pt </td>
								<td width=\"40px\" align=\"center\"> 總計 </td>
								<td width=\"15px\" align=\"center\" $border_style> ".$pt_style_start.$total_cat_pt.$pt_style_end." </td>
								<td width=\"60px\" align=\"center\"> 實得積點 </td>
								<td width=\"15px\" align=\"center\" $border_style> ".$pt_style_start.$real_cat_pt.$pt_style_end." </td>
							</tr>";
		if($this->printType == 'pdf'){
			$this->html .= "<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>";
		}
		$this->html .= "</table>";
		$this->row_count -= 1.6;
		
		$this->total_credit += $real_cat_pt;
	}
	
	public function setReportSummary($student, $class_teacher, $disable_page_break=false){
		$match = false;
		$total_pt = $this->total_credit;
		$awardstr = '';
		
		# Receive Award
		$count = 0;
		foreach($this->SAS_award_settings as $award_name => $award_range){
			$start_pt = $award_range['start'];
			$end_pt = $award_range['end'];
			
			if($count != 0){
				$awardstr .= ' / ';
			}
			
			if($start_pt != "" && $end_pt != "" && $total_pt >= $start_pt && $total_pt <= $end_pt){
				$match = true;
			} else if($end_pt == "" && $total_pt >= $start_pt) {
				$match = true;
			} else if($start_pt == "" && $total_pt <= $end_pt) {
				$match = true;
			}
			
			if($match){
				$awardstr .= "<b><u>";
			} 
			$awardstr .= $award_name ." (";
			
			if($start_pt == ""){
				$awardstr .= $end_pt . "或以下) ";
			} else if($end_pt == ""){
				$awardstr .= $start_pt . "或以上) ";
			} else {
				$awardstr .= $start_pt . " - " . $end_pt . ") ";
			}
			if($match){
				$awardstr .= "</u></b>";
				$match = false;
			} 
			
			$count ++;
		}
		
		if($this->printType == 'pdf'){
			$seperator = "*******************************************************************************************************************";
			$height_style = "height=\"25\"";
			$pt_style_start = "<u>";
			$pt_style_end = "</u>";
		} else {
			$seperator = "**********************************************************************************";
			$border_style = "style=\"border-bottom: 1px solid #000000\"";
		}
		
		if($this->row_count < 6.5){
			$this->setPageBreak('');
		}
		
		$this->html .= "<div class=\"report_seperator\" align=\"center\"> $seperator </div>";
		$this->html .= "<table class=\"summary_table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"99%\" align=\"center\">
							<tr>
								<td width=\"35%\" align=\"left\" $height_style><u> 個人獎勵總結  </u></td>
								<td width=\"47%\" align=\"left\" $height_style> (".$student['ClassTitleEN'].$student['ClassNumber'].") ".$student['ChineseName']."</td>
								<td width=\"14%\" align=\"left\" $height_style> 實得績點總計: </td>
								<td $border_style width=\"5%\" align=\"center\" $height_style > ".$pt_style_start.$this->total_credit.$pt_style_end." </td>
							</tr>
						</table>";

		$this->html .= "<table class=\"summary_table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" align=\"center\">
							<tr>
								<td width=\"20%\" align=\"right\" $height_style> 獲取獎項: </td>
								<td width=\"10%\" align=\"left\" $height_style> &nbsp; </td>
								<td width=\"70%\" align=\"left\" $height_style> $awardstr </td>
							</tr>
						</table>
						<table class=\"summary_table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\" align=\"center\">
							<tr>
								<td width=\"20%\" align=\"right\"> 班主任: </td>
								<td width=\"2%\" align=\"right\">&nbsp;</td>
								<td width=\"30%\" $border_style align=\"center\"> ".$pt_style_start.$class_teacher.$pt_style_end." </td>
								<td align=\"left\" $height_style>&nbsp;</td>
								<td> &nbsp; </td>
							</tr>
						</table>";
		
		$this->row_count -= 6.3;
		
		if($this->printType != 'pdf'){
			$this->html .= "</div></div>";
			
			if(!$disable_page_break){
				$this->html .= "<div style=\"page-break-after:always\">&nbsp;</div>";
			}
		}
	}
	
	public function resetCredit(){
		$this->total_credit = 0;
	}
	
	public function emptyHtml(){
		$this->html = '';
	}
	
	public function returnHtml(){
		return $this->html;
	}
	
}
?>