<?php
/*
################
#	Date	2017-03-17 Omas
#	- modified getStudentListAssessmentResult(), getSchoolSubjectInfo() - exclude subject cmp
################
 */


function displayHKID($ID){
	$id_length = strlen($ID);
	$lastCharPosition = $id_length - 1;
	$lastChar = $ID[$lastCharPosition];
	$tempNewID = substr($ID,0,$lastCharPosition);
	$newID = $tempNewID.'('.$lastChar.')';
	return $newID;
}
function displayScore($scoreArr){
	
	if($scoreArr['Score'] > 0){
		$score = $scoreArr['Score'];
		$displayScore = number_format($score,1);
		if($score < 50){
			return '('.$displayScore.')';
		}
		else{
			return $displayScore;
		}
	}
	else{
		return $scoreArr['Grade'];
	}
	
}

function EmptyContentAsEmptyString($Content=''){
	
	global $EmptyString;
	
	if($Content ==''){
		$Content = $EmptyString;
	}
	else{
		$Content = $Content;
	}
	
	return $Content;
}

class objPrinting{
//-- Start of Class

	var $EmptyString = '---';
	var $AcademicYearName;
	
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	private $cfgValue;
	
	private $AcademicYear;
	private $studentListAry;
	private $studentId;
	private $Student_info_Ary;
	private $Student_Assessment_Ary;
	private $Student_Assessment_YearID_Ary;
	private $Student_OLE_Ary;
	private $Student_Comment_Ary;
	private $Student_SelfAccount_Ary;
	private $Student_PA_Ary;
	private $PA_Header_Ary;
	private $Student_ClassName;
	private $yearClassID;
	private $Student_YearIDAry;
	private $ELE_Description;
	
	public function __construct(){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		$this->cfgValue = $ipf_cfg;
	}
		
	private function returnLatest3YearID($curAcademicYear){
		$StartOfCurYear = getStartDateOfAcademicYear($curAcademicYear);
		
		$sql = "select 
							AcademicYearID 
						from 
							ACADEMIC_YEAR_TERM 
						where 
							TermEnd < '".$StartOfCurYear."' 
						order by 
							TermEnd Desc 
						limit 0,1";
		$PrevYearID = $this->objDB->returnVector($sql);

		$StartOfPrevYear = getStartDateOfAcademicYear($PrevYearID[0]);
		
		$sql = "select 
							AcademicYearID 
						from 
							ACADEMIC_YEAR_TERM 
						where 
							TermEnd < '".$StartOfPrevYear."' 
						order by 
							TermEnd Desc 
						limit 0,1";
		$PrevPrevYearID = $this->objDB->returnVector($sql);
		
		$returnArray = array($curAcademicYear,$PrevYearID[0],$PrevPrevYearID[0]);
		return $returnArray;
	}
	public function setAcademicYear($AcademicYear){
		$this->AcademicYear = $AcademicYear;
		$this->AcademicYearName = getAcademicYearByAcademicYearID($AcademicYear);
		$this->AcademicYearArr = $this->returnLatest3YearID($AcademicYear);
	}
	public function getAcademicYear($format='ID'){
		if($format == 'ID'){
			return $this->AcademicYear;
		}
		else{
			return $this->AcademicYearName;	
		}
	}
	
	public function setYearClassID($yearClassId){
		$this->yearClassID = $yearClassId;
	}
	public function getYearClassID(){
		return $this->yearClassID;
	}
	
	public function setStudentList($studentListAry){
		$this->studentListAry = $studentListAry;
	}
	public function getStudentList(){
		return $this->studentListAry;
	}
	
	
	public function setStudent($studentId){
		$this->studentId = $studentId;
	}
	public function getStudent(){
		return $this->studentId;
	}
	
	public function contentReady(){
		//Please Set StudentList Before call this function		
		$this->Student_info_Ary = $this->getStudentList_Info();
		$this->Student_Assessment_Ary = $this->getStudentListAssessmentResult();
		$this->Student_OLE_Ary = $this->getStudentList_OLE();
		$this->Student_Comment_Ary = $this->getStudentListComment();
		$this->Student_SelfAccount_Ary = $this->getStudentList_SelfAccount();
		$this->Student_PA_Ary = $this->getAllStudentPA();
		$this->School_Subject_Ary = $this->getSchoolSubjectInfo();
		$this->ELE_Description = $this->getELEDescription();
	}
	
			
	//Get Student Info
	private function getStudentList_Info(){
		$sql = "select 
					iu.EnglishName,
					iu.ChineseName,
					/*DATE_FORMAT(iu.DateOfBirth, '%d/%m/%Y') AS DateOfBirth,*/
					DATE_FORMAT(iu.DateOfBirth, '%Y-%m-%d') AS DateOfBirth,
					iu.ClassName,
					iu.ClassNumber,
					iu.Gender,
					iu.BarCode,
					iu.STRN,
					iu.HKID,
					substring(iu.WebSAMSRegNo from 2) as WebSAMSRegNo ,
					iu.userid,
					iups.AdmissionDate as AdmissionDate
				from
					".$this->intranet_db.".INTRANET_USER AS iu
					LEFT OUTER JOIN INTRANET_USER_PERSONAL_SETTINGS as iups on iu.UserID = iups.UserID
				where
					iu.userid IN ('".implode('\',\'',$this->getStudentList())."') 
					AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";

		$result = $this->objDB->returnResultSet($sql);
		
		return BuildMultiKeyAssoc($result,'userid');
	}
	public function getStudentInfo(){
		return $this->Student_info_Ary[$this->getStudent()];
	}


	private function getSchoolSubjectInfo(){
		
		$sql = "Select 
			ass.RecordID, 
			ass2.CODEID, 
			ass2.EN_SNAME as SubjectCode, 
			ass2.EN_DES as SubjectDisplayName,
			ass2.CH_DES as SubjectDisplayNameChi,
			ass2.DisplayOrder,
			ass2.CMP_CODEID as isComponent
		From 
			LEARNING_CATEGORY as lc 
			inner join ASSESSMENT_SUBJECT as ass on lc.LearningCategoryID = ass.LearningCategoryID and ass.RecordStatus =1
			left outer join ASSESSMENT_SUBJECT as ass2 on ass.CODEID = ass2.CODEID and ass2.RecordStatus =1
		where 
			lc.RecordStatus = 1 and (ass.CMP_CODEID is null or ass.CMP_CODEID = '') and (ass2.CMP_CODEID is null or ass2.CMP_CODEID = '')
		order by lc.DisplayOrder, ass.DisplayOrder, ass2.CMP_CODEID , ass2.DisplayOrder";
			
		$result = $this->objDB->returnResultSet($sql);
		
		return $result;
	}
	
	private function getStudentListAssessmentResult(){
		# Class History
		$sql = "Select UserID, ClassName, ay.YearNameEN as AcademicYear, pch.AcademicYearID 
				From PROFILE_CLASS_HISTORY as pch 
					INNER JOIN ACADEMIC_YEAR as ay on pch.AcademicYearID = ay.AcademicYearID
				where 
					pch.userid IN  ('".implode('\',\'',$this->getStudentList())."') 
					/*AND pch.ClassName REGEXP '".implode('|',array(4,5,6))."'*/
					AND ay.AcademicYearID IN ('".implode("','",$this->AcademicYearArr)."') 
				order by pch.AcademicYear";
		$result = $this->objDB->returnResultSet($sql);
		$academicYearIDArr = array_unique(Get_Array_By_Key($result,'AcademicYearID'));
		foreach((array)$result as $_AYdataArr){
			$AYAssocArr[$_AYdataArr['UserID']][] = $_AYdataArr;
		}
		$this->Student_Assessment_YearID_Ary =  $AYAssocArr;
		
		$sql ="SELECT 
						a.RecordID,
						IFNULL(iu.WebSAMSRegNo, iau.WebSAMSRegNo) as WebSAMSRegNo,
						a.ClassName as ClassName,
						a.ClassNumber as ClassNumber,
						a.Year,
						a.Semester as Semester,
						a.AcademicYearID,
						a.YearTermID,
						a.SubjectCode,
						a.SubjectComponentCode,
						a.Score,
						a.Grade,
						a.OrderMeritForm,
						asub.EN_ABBR as Subject,
						a.SubjectID,
						a.UserID
					FROM
						".$this->eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD as a
						LEFT JOIN ".$this->intranet_db.".INTRANET_USER AS iu ON a.UserID = iu.UserID
						LEFT JOIN ".$this->intranet_db.".INTRANET_ARCHIVE_USER AS iau ON a.UserID = iau.UserID
						LEFT JOIN ".$this->intranet_db.".ASSESSMENT_SUBJECT AS asub ON a.subjectID = asub.RecordID
					WHERE
						a.AcademicYearID IN  ('".implode($academicYearIDArr,"','")."')
						AND (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
						AND a.userid IN ('".implode('\',\'',$this->getStudentList())."')  
						AND IsAnnual = 1
						AND SubjectComponentID is Null
					ORDER BY
						a.ClassName,
						ClassNumber,
						a.Year desc,
						a.Semester desc,
						asub.DisplayOrder
						/*a.SubjectCode,
						a.SubjectComponentCode*/
				";
		$result = $this->objDB->returnResultSet($sql);
		
		foreach((array)$result as $dataArr){
			$AssocArr[$dataArr['UserID']][$dataArr['SubjectID']][$dataArr['AcademicYearID']] = $dataArr;
		}
		return $AssocArr;
	}
	public function getStudentAssessmentResult(){
		
		$resultArr['SchoolSubject']= $this->School_Subject_Ary;
		$resultArr['AssessmentResult'] = $this->Student_Assessment_Ary[$this->getStudent()];
		$resultArr['Year'] = $this->Student_Assessment_YearID_Ary[$this->getStudent()];
		return $resultArr; 
	}
	
	//Get Student OLE
	private function getStudentList_OLE(){
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";
		
		$sql = "select 
					op.Title,
					op.StartDate,
					op.EndDate,
					os.Role,
					os.Hours,
					os.Achievement,
					op.ELE,
					op.Details,
					ay.YearNameEN as AcademicYear,
					IF(ayt.YearTermNameEN IS NULL, 'Whole Year',ayt.YearTermNameEN) as Term,
					oc.EngTitle as Category,
					os.userid,
					os.Organization as Organization,
					oc.RecordID as ProgramCategoryID,
					Concat(oc.ChiTitle ,'<br>', oc.EngTitle) as CategoryName,
					op.IntExt as IntExt
				from 
					".$this->eclass_db.".OLE_STUDENT as os 
					inner join ".$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid 
					inner join ".$this->eclass_db.".OLE_CATEGORY as oc on op.Category = oc.RecordID 
					inner join ".$this->intranet_db.".ACADEMIC_YEAR as ay on op.AcademicYearID = ay.AcademicYearID
					left join ".$this->intranet_db.".ACADEMIC_YEAR_TERM  as ayt on op.YearTermID = ayt.YearTermID
				where 
					os.userid IN ('".implode('\',\'',$this->getStudentList())."')  
					$condApprove
					AND op.AcademicYearID IN ('".implode("','",$this->AcademicYearArr)."')
				order by 
					AcademicYear desc,op.Title asc
				";


		$result = $this->objDB->returnResultSet($sql);
		
		foreach($result as $_OLEInfoArr){
			$_studentId = $_OLEInfoArr['userid'];
			$_programCategoryID = $_OLEInfoArr['ProgramCategoryID'];
			$_IntExt = $_OLEInfoArr['IntExt'];

// forgot why do this.. 	comment on 20170418 - by #C115837
// 			if($_programCategoryID == 5){
// 				//services
// 				$resultAssoArr[$_studentId][$_IntExt]['SERVICE'][] = $_OLEInfoArr;
// 			}
// 			else{
				//external or internal OLE
// 				$resultAssoArr[$_studentId][$_IntExt]['OLE'][] = $_OLEInfoArr;
// 			}
			$resultAssoArr[$_studentId][$_IntExt]['OLE'][] = $_OLEInfoArr;

		}
	
		return $resultAssoArr;
	}
	public function GetOLE($IntExt, $Category){
		$IntExt = strtoupper($IntExt);
		$Category= strtoupper($Category);
		
		return $this->Student_OLE_Ary[$this->getStudent()][$IntExt][$Category];
	}
	
	private function getELEDescription(){
		$sql = "SELECT
					IF(DefaultID IS NULL,CONCAT('[', RecordID , ']'),DefaultID) as DefaultID, 
					ChiTitle, 
					EngTitle
				FROM
					".$this->eclass_db.".OLE_ELE 
				WHERE
					RecordStatus IN (1, 2)";
		$result = $this->objDB->returnResultSet($sql);
		
		return BuildMultiKeyAssoc($result,'DefaultID');
	}
	
	//Get Self Account
	private function getStudentList_SelfAccount(){
		$sql = "select 
					sas.Details as Details,
					userid
				from 
					".$this->eclass_db.".SELF_ACCOUNT_STUDENT as sas 
				where 
					Userid IN ('".implode('\',\'',$this->getStudentList())."')  
					and DefaultSA = 'SLP' 
			   ";

		$result = $this->objDB->returnResultSet($sql);
		return BuildMultiKeyAssoc($result,'userid');
	}
	public function getSelfAccount(){
		return $this->Student_SelfAccount_Ary[$this->getStudent()];
	}
	
	private function getStudentListComment(){
		//C94268 - improved to show English comment if Chinese comment is empty
		$sql ="
			SELECT
				UserID,
				IF (CommentChi is null or CommentChi = '', CommentEng, CommentChi) as CommentChi
			FROM
				".$this->eclass_db.".CONDUCT_STUDENT 
			WHERE
				UserID IN ('".implode("','",$this->getStudentList())."')
			ORDER BY ModifiedDate";

		$result = $this->objDB->returnArray($sql); 
		
		return BuildMultiKeyAssoc($result,'UserID');
	}
	public function getStudentComment(){
		
		$resultArr = $this->Student_Comment_Ary[$this->getStudent()];

		return $resultArr;
	}
	
	private function getAllStudentPA(){
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libimporttext.php");
		$libimport = new libimporttext();
		
		# Personal Attributes 
		$FileCSV = $PATH_WRT_ROOT."file/portfolio/twghwfns_pa_".$this->AcademicYear.".csv";
		if (file_exists($FileCSV))
		{
			$csv_data = $libimport->GET_IMPORT_TXT($FileCSV);
			$csv_header = $csv_data[0];
			$this->PA_Header_Ary = $csv_header;		
			for ($i=1; $i<sizeof($csv_data); $i++)
			{
				$rowObj = $csv_data[$i];
				$PersonalAttributes[trim($rowObj[0], "#")] = $rowObj;
			}
		}
		return $PersonalAttributes;
	}
	public function getStudentPAHeader(){
		return $this->PA_Header_Ary;
	}
	public function getStudentPA($WebSAMSNum){
		return $this->Student_PA_Ary[$WebSAMSNum];
	}
	
	public function getDisplayFormName($ClassName){
		$numberChiArr = array( 1=>'一',2=>'二',3=>'三',4=> '四',5=> '五',6=> '六');
		preg_match("/\d/", $ClassName, $formNumArr);
		$string = '中'.$numberChiArr[$formNumArr[0]].' Sec '.$formNumArr[0];
		return $string;
	}
	
	public function display2Lang($string){
		$ChineseStringArr = explode('[',$string);
		$EnglishStringArr = explode(']',$ChineseStringArr[1]);
		return trim($ChineseStringArr[0]).'<br>'.trim($EnglishStringArr[0]);
	}
	
	public function getELEdisaply($ELE_str){
		$ELE_Info_Arr = $this->ELE_Description;
		$targetELE_Arr = explode(',',$ELE_str);
//		debug_pr($ELE_str);
//		debug_pr($ELE_Info_Arr);
		$return = '';
		foreach((array)$targetELE_Arr as $_ELE_ID){
			$_ChiTitle = $ELE_Info_Arr[$_ELE_ID]['ChiTitle'];
			$_EngTitle = $ELE_Info_Arr[$_ELE_ID]['EngTitle'];
			if($return!=''){
				$return .= '<br><br>';
			}
			$return .= $_ChiTitle.'<br>'.$_EngTitle;
		}
		return $return;
	}
	
//	for ($j=3; $j<sizeof($csv_header); $j++)
//{
//	$attribute_name = $csv_header[$j];
//	$tmpArr = explode("/", $attribute_name);
//	$attribute_name_eng = trim($tmpArr[0]);
//	$attribute_name_chi = trim($tmpArr[1]);
//	
//	$Grade4 = (trim($StudentAttributeResult[$j])==4) ? "&#10003;" : "&nbsp;";
//	$Grade3 = (trim($StudentAttributeResult[$j])==3) ? "&#10003;" : "&nbsp;";
//	$Grade2 = (trim($StudentAttributeResult[$j])==2) ? "&#10003;" : "&nbsp;";
//	$Grade1 = (trim($StudentAttributeResult[$j])==1) ? "&#10003;" : "&nbsp;";
//	
//$html .= <<<EOF
//              <tr>
//                <td width="26%">{$attribute_name_eng}</td>
//                <td width="18%">{$attribute_name_chi}</td>
//                <td width="14%">{$Grade4}</td>
//                <td width="14%">{$Grade3}</td>
//                <td width="14%">{$Grade2}</td>
//                <td width="14%">{$Grade1}</td>
//            </tr>
//EOF;
//}
	
	//------------------------------------------------ ABOVE ARE DATA HANDLING RELATED ------------------------------------------------//
	#####################################################################################################################################
	#																																	#
	#																																	#
	#####################################################################################################################################	
	//------------------------------------------------ BELOW ARE HTML AND CSS RELATED -------------------------------------------------//

	public function returnStyleSheet(){
		$style = '<style>
			/*@media all {
				.page-break {display: none;}
			}
			@media print {
				.page-break {display: block; page-break-before: always;}
				html, body {width: 21cm;}
			}
			@page {size: A4 portrait; margin: 1.2cm 0 0 0;}*/
			body {font-family: "aa";color: #000000; }
			.page_wrapper {width: 16.0cm; height: 27.2cm; margin: 0 auto ;  /*background: yellow; page-break-after:always*/}
			.header_wrapper { width: 16.0cm;margin: 0 auto ;  /*background: yellow;*/}
			.alignCenter { text-align: center;}
			.alignRight { text-align: right;}
			.alignLeft { text-align: left;}
			.fontTitle1 { font-size:1.3em; font-weight:bold;}
			.fontTitle2 { font-size:1.6em; font-weight:bold;}
			
			.font_1 {font-size: 0.75em;}
			.font_1b {font-size: 0.75em; font-weight:bold;}
			.font_1_5 {font-size: 0.8em;}
			.font_2 {font-size: 0.85em;}
			.font_2b {font-size: 0.85em; font-weight:bold;}
			.font_3 {font-size: 1.0em; font-weight:bold;}
			.border_1 {border: 1px solid #000; border-collapse:collapse;}
			.border_2 {border: 2px solid #000; border-collapse:collapse;}
			.bottom_border_1 {border-bottom: 1px solid #000; border-collapse:collapse;}
			.bottom_border_2 {border-bottom: 2px solid #000; border-collapse:collapse;}
			table {width:100%;}
			.noPadding {padding: 0px;}
			.Padding3 {padding: 2px;}
			td {padding: 5px;}
		</style>';
				
		return $style;
	}
	
	public function returnSignature(){
	}
	
	public function returnHeaderAndFooter(){
	}
	
//-- End of Class
}


?>