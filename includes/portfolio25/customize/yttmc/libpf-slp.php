<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	
	private $NumberOfFirstPage;
	private $NumberOfOtherPage;
	private $nextLeftRow;
	private $RowNoNow;
	
	private $YearClassID;
	
	private $AcademicYearStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '--';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->NumberOfFirstPage = '10';
		$this->NumberOfOtherPage = '27';
		
		$this->nextLeftRow = $this->NumberOfFirstPage;
		$this->RowNoNow = 0;
		
		$this->SchoolTitleEn = 'TWGHs Yau Tze Tin Memorial College';
		$this->SchoolTitleCh = '東華三院邱子田紀念中學';
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	
	public function setNoOfFirstPage($NoOfFirstPage){
		$this->NumberOfFirstPage = $NoOfFirstPage;
		$this->nextLeftRow = $this->NumberOfFirstPage;
	}
	public function setNoOfOtherPage($NoOfOtherPage){
		$this->NumberOfOtherPage = $NoOfOtherPage;
	}
	
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	

	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '15';
	
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">
					<tr>
						<td rowspan="3" align="center" " style="width:18%;">&nbsp;</td>
						<td style="font-size:'.$header_font_size.'px;"><b>'.$this->SchoolTitleEn.' '.$this->SchoolTitleCh.'</b></td>
						<td rowspan="3" align="left" style="width:18%;">&nbsp;</td>
				   </tr>';
			
			$x .= '<tr><td style="font-size:'.$header_font_size.'px;"><b>Student Learning Profolio 學生學習概覽</b></td></tr>'."\r\n";
			$x .= '<tr><td style="font-size:15px;"><b>Acadamic Year '.$this->YearRangeStr.' 年度</b></td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						ClassNumber,
						ClassName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnArray($sql);
		
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{		
		$fontSize = 'font-size:15px';
		
		$thisPartTitle = 'Student Particulars 學生資料';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;
		} else {
			$IssueDate = date('Y-n-j',strtotime($IssueDate));
		}
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		
		if($Gender=='M')
		{
			$Gender ='Male 男';
		}
		else if($Gender=='F')
		{
			$Gender ='Female 女';
		}
		
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$RegistrationNo = $result[0]["STRN"];
		$RegistrationNo = ($RegistrationNo=='')? $this->EmptySymbol:$RegistrationNo;
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;


		$html = '';
		$html .= '<table width="90%" cellpadding="4" border="0px" align="center" style="border-collapse: collapse;">
					<col width="25%" />
					<col width="25%" />
					<col width="50%" />

					<tr>
							<td style="'.$fontSize.'" colspan="3"><b>'.$thisPartTitle.'</b></td>
					</tr>
					<tr>
							<td style="'.$_borderTop.' '.$_borderLeft.' '.$fontSize.'" colspan="2">Name 姓名: '.$EngName.' ('.$ChiName.')</td>
							<td style="'.$_borderTop.' '.$_borderRight.' '.$fontSize.'">Sex 性別: '.$Gender.'</td>
					</tr>
					<tr>
							<td style="'.$_borderBottom.' '.$_borderLeft.' '.$fontSize.'">Class 班別: '.$ClassName.'</td>
							<td style="'.$_borderBottom.' '.$fontSize.'">Class No. 學號: '.$ClassNumber.'</td>
							<td style="'.$_borderBottom.' '.$_borderRight.' '.$fontSize.'">Registration No. 註冊編號: '.$RegistrationNo.'</td>
					</tr>
					';

		$html .= '</table>';
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Academic Performance******************/	
	public function getPart3_HTML($OLEInfoArr){
		
		$Part3_Title = 'Membership/Duty 職務';
		$Part3_Remark = '&nbsp;';
		
		$titleArray = array();
		$titleArray[] = 'No. 編號';
		$titleArray[] = 'School Year 學年';
		$titleArray[] = 'Club/Team 學會/團隊';
		$titleArray[] = 'Post/Duty 職務';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="10%" />';
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="30%" />';
		
		$html = '';
		
		$DataArray = $OLEInfoArr[$this->uid];
		
		$html .= $this->getPartContentDisplay_NoRowLimit('3',$Part3_Title,$titleArray,$colWidthArr,$DataArray);
		
		return $html;	
	}		

	/***************** End Part 3 ******************/
	
	/***************** Part 4 ******************/	
	public function getPart4_HTML($OLEInfoArr){
		
		$Part4_Title = 'Activities/Programmes/Competitions 活動/比賽';
		
		$titleArray = array();
		$titleArray[] = 'No.<br/>編號';
		$titleArray[] = 'School Year<br/>學年';
		$titleArray[] = 'Activities/Programmes/<br/>Competitions<br/>活動/比賽';
		$titleArray[] = 'Role of<br/>Participation<br/>參與角色';
		$titleArray[] = 'Area of Other<br/>Learning<br/>Experiences<br/>其他學習經歷<br/>範疇*';
		$titleArray[] = 'Organization<br/>合辦機構';
		$titleArray[] = 'Award/<br/>Certificate/<br/>Achievement<br/>獎項/證書文憑/成就';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="5%" />';
		$colWidthArr[] = '<col width="14%" />';
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="12%" />';
		$colWidthArr[] = '<col width="16%" />';
		$colWidthArr[] = '<col width="12%" />';
		$colWidthArr[] = '<col width="16%" />';  
		  
		$DataArr = array();

		$DataArray = $OLEInfoArr[$this->uid];
		

		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('4',$Part4_Title,$titleArray,$colWidthArr,$DataArray);
		
		return $html;
		
	}

	/***************** End Part 4 ******************/
	
	
	
	/***************** Part 5 : Other Learning Experience******************/	
	public function getPart5_HTML(){
		
		$Part5_Title = 'Scholarship 獎學金';
				
		$titleArray = array();
		$titleArray[] = 'No. 編號';	
		$titleArray[] = 'School Year 學年';
		$titleArray[] = 'Title 項目';
		$titleArray[] = 'Organization 頒發機構';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="10%" />';
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="30%" />';  
		
		$DataArray = array();
		$DataArray = $this->getAward_Info();  

		$html = '';
	
		if(count($DataArray)>0)
		{
			$html .= $this->getPartContentDisplay_NoRowLimit('5',$Part5_Title,$titleArray,$colWidthArr,$DataArray);
		}
			
		return $html;
		
	}	
	   
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'"; 
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And s.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						AwardName,
						Organization
				From
						$Award_student s
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = s.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc, 
						AwardName asc		

				";
		$roleData = $this->objDB->returnResultSet($sql);

		$returnArr = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$returnArr[$i]['No'] = $i+1;
			$returnArr[$i]['YEAR_NAME'] = $roleData[$i]['YEAR_NAME'];
			$returnArr[$i]['AwardName'] = $roleData[$i]['AwardName'];
			$returnArr[$i]['Organization'] = $roleData[$i]['Organization'];
		}

		return $returnArr;
	}
	
	/***************** End Part 5 ******************/

	
	
	/***************** Part 6 ******************/
	
	public function getPart6_HTML(){
		
		$Part7_Title = "Students' \"Self-Account\" 學生自述";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '<table width=100% height="860px">';
		$html .= '<tr><td style="vertical-align:top;">';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		$html .= '</td></tr>';
		$html .='</table>';
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnArray($sql,2);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:16px;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];


			$Detail = strip_tags($Detail,'<p><br>');


		}
		else
		{
			$Detail = 'No Record Found.';
		}	
		$html .= '<table width="90%" height="" cellpadding="2" border="0px" align="center" style="font-size:16px;border-collapse: collapse;">
					<tr> 
						<td style=" font-size:15px;text-align:left;vertical-align:text-top;"><b>
						'.$MainTitle.'
						</b></td>
				  	</tr>
				  </table>';
		
		$html .= '<table width="90%" height="800px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr>				 							
						<td style="vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'">'.$Detail.'</td>						 														
					</tr>
				  </table>';	
		
			
		return $html; 
	}
	
	
	/***************** End Part 6 ******************/
	
	
	
	/***************** Part 7 : Footer ******************/
	public function getSignature_HTML($PageBreak='')
	{
		$DateOfIssueStr = "Date of issue 發出日期";
		$SchoolChopStr = "School Chop 校印";
		$PrincipalStr = "Principal 校長";
		
		$_borderTop =  'border-top: 1px solid black; ';
		
		
		
		$html = '';
		
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="font-size:15px;'.$PageBreak.'">		
					<col width="27%" />
					<col width="10%" />
					<col width="26%" />
					<col width="10%" />
					<col width="27%" />

					<tr>					
					    <td align="center"><b>'.$this->issuedate.'</b></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
				 	<tr>					
					    <td align="center" style="'.$_borderTop.'"><b>'.$DateOfIssueStr.'</b></td>
						<td>&nbsp;</td>
						<td align="center" style="'.$_borderTop.'"><b>'.$SchoolChopStr.'</b></td>
						<td>&nbsp;</td>
						<td align="center" style="'.$_borderTop.'"><b>'.$PrincipalStr.'</b></td>
					</tr>

				  </table>';
		return $html;
		
	}
	/***************** End Part 7 ******************/

	
	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='') 
	{
				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$Page_break='page-break-after:always;';
		
		$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 

		
		$subtitle_html = '';
				
		foreach((array)$subTitleArr as $key=>$_title)
		{
			$text_alignStr='left';
			
			$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'">';
			$subtitle_html .=$_title;
			$subtitle_html .='</td>';
			
		}	
					
		$total_Record=10;
		$rowHight = 30;


		$text_alignStr='center';
		
		$content_html = '';;
		
		$rowMax_count = 0;
		
		if(count($DataArr)>0)
		{
			$rowMax_count = count($DataArr);
		}	

		$count_col =count($colWidthArr);


		$mainNumberOfPage = $this->NumberOfOtherPage;

		$ContentArray = array();

		$StartIndex = 0;;
		
		$a_MAX = $this->nextLeftRow;
		
		if($rowMax_count<$this->nextLeftRow)
		{
			$a_MAX = $rowMax_count;
		}
		
		for($a=0;$a<$a_MAX;$a++)
		{
			$this_content_html = '';
			$this_content_html .= '<tr>';	
					
			$j=0;		 
			foreach((array)$DataArr[$a] as $_title=>$_titleVal)
			{
				if($_titleVal=='')
				{
					$_titleVal =  $this->EmptySymbol;
				}
				
				$text_alignStr = 'left';
				
				$this_content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'">'.$_titleVal.'</td>';								
				$j++;
			}				
			$this_content_html .= '</tr>';		
		
			$ContentArray['first'][] = $this_content_html;
			$this->RowNoNow++;
			$StartIndex = $a;
		}
		$StartIndex++;
		
		$loopCount = 0;
		$numberOfRow = 1;
		for($a=$StartIndex;$a<$rowMax_count;$a++)
		{
			$this_content_html = '';
			$this_content_html .= '<tr>';	
					
			$j=0;		 
			foreach((array)$DataArr[$a] as $_title=>$_titleVal)
			{
				if($_titleVal=='')
				{
					$_titleVal =  $this->EmptySymbol;
				}
				
				$text_alignStr = 'left';
				
				$this_content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'">'.$_titleVal.'</td>';								
				$j++;
			}				
			$this_content_html .= '</tr>';	
			
			$ContentArray[$loopCount][] = $this_content_html;
			
			if($numberOfRow%$mainNumberOfPage ==0 && $numberOfRow!=1)
			{
				$loopCount++;
			}		
			$this->RowNoNow++;
			$numberOfRow++;
		}

		
		$colNumber = count($colWidthArr);
			
		/****************** Main Table ******************************/
		
		$html = '';

		$html .= '<table width="90%" height="" cellpadding="2" border="0px" align="center" style="font-size:10px;border-collapse: collapse;">
					<tr> 
						<td style=" font-size:15px;text-align:left;vertical-align:text-top;"><b>
						'.$mainTitle.'
						</b></td>
				  	</tr>';
				  	
		if($PartNum=='4')
		{
			$longSpace='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$thisStr =''; 
			$thisStr .='*PD(Physical Development 體育發展) '.$longSpace;
			$thisStr .='AD(Aesthetic Developement 藝術發展) '.$longSpace;
			$thisStr .='CS(Community Services 社會服務)<br/>';
			$thisStr .='MCE(Moral and Civic Education 德育及公民教育) '.$longSpace;
			$thisStr .='CRE(Career-related Development 與工作有關的經驗)';			
			
			$html .=  '<tr> 
						 <td style="text-align:left;vertical-align:text-top; font-size:12px;">
						 '.$thisStr.'
						 </td>
				  	   </tr>';			  
		} 
		
		$html .=    '</table>';

		if($rowMax_count>0)
		{
			$thisNumberPage = ($PartNum=='3')?$this->NumberOfFirstPage : $this->NumberOfOtherPage;	
	
			if($this->RowNoNow>$thisNumberPage)
			{
				$Page_break='page-break-after:always;';		
		
			}
			else if ($this->RowNoNow==0)
			{
				$Page_break='';
			}
			
			$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
			
			if(count($ContentArray)==1)
			{
				$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;"'; 
				
				if($PartNum=='3')
				{
					$this->nextLeftRow =$this->NumberOfFirstPage -count($ContentArray['first']);
				}
				else
				{
					$this->nextLeftRow =$this->NumberOfOtherPage -count($ContentArray['first']);
				}
			}
		
			$html .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
			$html .= '<tr>'.$subtitle_html.'</tr>';
			for($i=0,$i_MAX=count($ContentArray['first']);$i<$i_MAX;$i++)
			{
				$html .= $ContentArray['first'][$i];
			}
		
			$html .='</table>';
				
	
			for($a=0,$a_MAX=count($ContentArray)-1;$a<$a_MAX;$a++)
			{
				if($a==$a_MAX-1)
				{			
					if($PartNum=='3')
					{
						$this->nextLeftRow =$this->NumberOfFirstPage -count($ContentArray[$a]);
					}
					else
					{
						$this->nextLeftRow =$this->NumberOfOtherPage -count($ContentArray[$a]);
					}
						
					if($this->nextLeftRow<0)
					{
						$Page_break='page-break-after:always;';	
						$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
					}
					else
					{					
						$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;"'; 
					}
					
				}
				$html .=$this->getEmptyTable('3%');
				$html .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
				for($i=0,$i_MAX=count($ContentArray[$a]);$i<$i_MAX;$i++)
				{
					$html .= $ContentArray[$a][$i];
				}
				$html .='</table>';
				
			} 
		}
		else
		{
			$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;'; 
			
			$html .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
				foreach((array)$colWidthArr as $key=>$_colWidthInfo)
						{
							$html .=$_colWidthInfo;
						}
				$html .= '<tr>'.$subtitle_html.'</tr>';
				$html .='<tr>';
					$html .='<td colspan="'.$count_col.'" style="text-align:center;vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'">';
					$html .='No record found.';
					$html .='</td>';
				$html .='</tr>';
			$html .='</table>';
		}
	
//debug_r($ContentArray);			
		return $html;
	}


	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false,$isMember=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$OLE_Cate = $this->Get_eClassDB_Table_Name('OLE_CATEGORY'); 
		
		$MembershipNo = 7;
		if($isMember)
		{		
			$cond_membershipNo =" And ole_cate.RecordID ='$MembershipNo'";
		}
		else
		{
			$cond_membershipNo =" And ole_cate.RecordID !='$MembershipNo'";
		}
		
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
		
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = " And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
				Inner join
						$OLE_Cate as ole_cate
				On
						ole_prog.Category = ole_cate.RecordID
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
						$cond_membershipNo
				order by 
						y.Sequence desc,	
						ole_prog.Title asc		

				";
		$roleData = $this->objDB->returnResultSet($sql);

		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
				
			if($isMember)
			{
				$thisAcademicYear = $roleData[$i]['YEAR_NAME'];

				$thisDataArr = array();	
	
				$thisDataArr['No'] =$i+1;
				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['Role'] =$roleData[$i]['Role'];
						
				$returnData[$_StudentID][] = $thisDataArr;
			}
			else
			{
				$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
			
				for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
				{
					$_str = $thisOleComponentArr[$a];
					$_str = substr($_str, 0, strlen($_str)-1); 
					$_str = substr($_str, 1);
					
					$thisOleComponentArr[$a] = $_str;
				}
				
				$thisOLEstr = implode(",",(array)$thisOleComponentArr);
	
				$thisDataArr = array();	
	
				$thisDataArr['No'] =$i+1;
				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['OLEComp'] =$thisOLEstr;	
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
						
				$returnData[$_StudentID][] = $thisDataArr;
			}
		
			
		}
		return $returnData;
	}
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
			$firstYear = substr($returnArray[0], 0, 4);
			$lastYear = substr($returnArray[$lastNo],-4);
			
			$returnValue = $firstYear."-".$lastYear;
			
			return $returnValue;	
		}
		
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center"><font face="Arial">'.$Content.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>