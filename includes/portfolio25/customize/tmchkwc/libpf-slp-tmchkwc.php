<?php
/*
 * 	Using:  
 * 	Note: please use utf-8 to edit!
 *
 *  2020-11-10 Cameron
 *      - print parts according to flag control in getReportContentLayout() [case #J200351]
 *      - add parameter partCounter to getBSQTable(), getEPWTable(), getOFSTable(), getBTPTable() and getBTPTable2()
 *
 *  2020-02-26 Cameron
 *      - temporarily disable printing eclass teacher signature in getReportContentLayout() [case #J180768]
 *       
 *  2019-12-05 Cameron
 *      - add function getEPWWithTermTable()
 *      - modify getEPWTable(), getOFSTable(), getBTPTable(), getBTPTable2() for new report layout
 *
 *  2019-12-04 Cameron
 *      - modify getPerformanceWoTerm(), add BTP-IS and BTP-TP to the sql [case #J161484]
 *      - modify getBSQTable() for new report layout
 *
 *  2019-12-03 Cameron
 *      - modify getServiceInSchoolAward() and getInterSchoolCompetitionsAward() to use new award description mapping [case #J161484]
 *
 *  2019-07-16 Cameron
 *      - rollback to use mingliu for Chinese only [case #J164766]
 *      - add function replaceUtf8ToAscii() for English field
 *
 *  2019-07-11 Cameron
 *      - use the same font in the report [case #J164766] <- Cancel
 *
 *  2019-07-04 Cameron
 *      - retrieve Award field so that it can print multiple external competitions awards for the same activity [case #J164242]
 *      - add function getPerformance_BTP_IC()
 *      - modify getBTPTable() and getBTPTable2() to support multiple awards for a student in same activity
 *       
 *  2019-07-03 Cameron
 *      - print performance description in chiFont style to eliminate unicode character problem [case #J164111]
 *
 *  2018-07-10 Cameron
 *      - change max number of record per page from 18 to 11 in getBTPTable() [case #J142433]
 *      
 *  2018-04-24 Cameron
 *      - add function getReportHeader(), move printing student info to this function
 *      
 * 	2018-01-19 Cameron
 * 		- should retrieve record for 'BTP-EA-N-01' and 'BTP-IC-N-01' in getPerformanceWiActivity() [from email request: 2018-01-03]
 * 		- modify getBTPTable() and getBTPTable2() to support the above change
 * 
 * 	2017-10-24 Cameron
 * 		- add function getBTPTable() for showing extra-curricular activities and external competition with repeated item name if they exceed one page limit
 * 		- modify to always show if a student get award or not for student profile report 
 * 
 * 	2017-10-23 Cameron
 * 		- retrieve organization name in getPerformanceWiActivity()
 * 		- show 'Not Applicable' for performance and '-' for award for following items if there's no record:
 * 			excursion, training (bsq), Physical agility (swimming), training (epw), service in / out school, extracurricular activities, inter-school competitions
 * 
 * 	2017-06-28 Cameron
 * 		- fix grammatical error in getOFSTable(): 1 good point	[case #J119404]
 * 
 * 	2017-04-27 Cameron
 * 		- change getServiceInSchoolAward() and getInterSchoolCompetitionsAward() to public
 * 
 * 	2017-03-07 Cameron
 * 		- adjust spacing around principal signature
 * 		- report Part D is printed in the same page as signature
 * 
 * 	2017-03-06 Cameron
 * 		- do not wrap student name in getReportContentLayout()
 * 		- add reportCode and apply to report so that the value in JinYing Report maybe different from that in Student Learning Profile
 * 
 * 	2017-01-13 Cameron
 * 		- change schoolBadge, add principal signature stamp in reports
 * 		- user WebSAMSRegNo as register number in getStudentInfo()
 * 		- show 'No record found' in scope if there's no record
 * 		- avoid page-break-inside between scope name and the details
 * 
 * 	2016-12-07 Cameron
 * 		- create this file
 */

class objPrinting{
//-- Start of Class

	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $task;
	private $reportCode;
	private $studentList;
	private $academicYearID;
	private $academicStartYear;
	private $academicEndYear;
	private $academicYearTerms;
	private $studentID;
	private $reportInfo;	// one student only
	private $issueDate;
	private $schoolBadge;
	private $schoolName;
	
	
	public function __construct($task='',$studentList='',$academicYearID=''){
		global $eclass_db,$intranet_db,$intranet_root;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		$this->task = $task;
		$this->reportCode = strlen($task)>3 ? strtoupper(substr($task,-3)) : 'NIL';
		$this->studentList = $studentList;
		$this->academicYearID = $academicYearID;
		$this->schoolName = ($_SESSION['intranet_session_language'] == 'en') ? 'The Methodist Church Hong Kong Wesley College' : '衞理中學';
		$this->schoolBadge = $intranet_root.'/file/customization/tmchkwc/wesley_logo.jpg';
		
	}

	public function setTask($task) {
		$this->task = $task;
	}
	public function setReportCode($reportCode) {
		$this->reportCode = $reportCode;
	}
	public function setStudentList($studentList) {
		$this->studentList = $studentList;
	}
	public function setAcademicYearID($academicYearID) {
		$this->academicYearID = $academicYearID;
	}
	public function setAcademicStartYear($academicStartYear) {
		$this->academicStartYear = $academicStartYear;
	}
	public function setAcademicEndYear($academicEndYear) {
		$this->academicEndYear = $academicEndYear;
	}
	public function setAcademicYearTerms($academicYearTerms) {
		$this->academicYearTerms = $academicYearTerms;
	}
	public function setStudentID($studentID) {
		$this->studentID = $studentID;
	}
	public function setReportInfo($reportInfo) {
		$this->reportInfo = $reportInfo;
	}
	public function setIssueDate($issueDate) {
		$this->issueDate = $issueDate;
	}
	public function setSchoolBadge($schoolBadge) {
		$this->schoolBadge = $schoolBadge;
	}
	public function setSchoolName($schoolName) {
		$this->schoolName = $schoolName;
	}

	public function getTask() {
		return $this->task;
	}
	public function getReportCode() {
		return $this->reportCode;
	}
	public function getStudentList() {
		return $this->studentList;
	}
	public function getAcademicYearID() {
		return $this->academicYearID;
	}
	public function getAcademicStartYear() {
		return $this->academicStartYear;
	}
	public function getAcademicEndYear() {
		return $this->academicEndYear;
	}
	public function getAcademicYearTerms() {
		return $this->academicYearTerms;
	}
	public function getStudentID() {
		return $this->studentID;
	}
	public function getReportInfo() {
		return $this->reportInfo;
	}
	public function getIssueDate() {
		return $this->issueDate;
	}
	public function getSchoolBadge() {
		return $this->schoolBadge;
	}
	public function getSchoolName() {
		return $this->schoolName;
	}
	
	public function isForm6Student() {
		return ($this->getFormNameByFirstStudent() == '6') ? true : false;	// for Wesley Only
	}
	
	// for removing 2nd term for Form 6 students
	public function getLastAcademicYearTermID() {
		if ($this->academicYearID) {
			$sql = "SELECT 	YearTermID							
					FROM	".$this->intranet_db.".ACADEMIC_YEAR_TERM
					WHERE	AcademicYearID='".$this->academicYearID."'
					ORDER BY TermEnd DESC";
			$rs = $this->objDB->returnVector($sql);
			if (count($rs)>1) {	// at least two terms 
				return $rs[0];
			}
		}
		return '';
	}

	private function getNumberOfTerms() {
		return count($this->academicYearTerms);
	}

	public function getServiceInSchoolAward($recommendMerit) {
		global $Lang;
		
		if ($recommendMerit) {
/*
            list($merit,$number) = explode('_',$recommendMerit);
			if ($_SESSION['intranet_session_language'] == 'en') {
				$awardName = $Lang['iPortfolio']['JinYing']['Report']['Award'][$merit];
				if ($number>1) {
					$awardName .= 's';
				}
				$award = sprintf($Lang['iPortfolio']['JinYing']['CodeAward']['OFS-SI-P-01'],$number,$awardName);
			}
			else {
				$award = sprintf($Lang['iPortfolio']['JinYing']['CodeAward']['OFS-SI-P-01'],$Lang['iPortfolio']['JinYing']['Report']['Award'][$merit],$number);
			}*/

            $award = $Lang['iPortfolio']['JinYing']['Award'][$recommendMerit];
        }
		else {
			$award = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
		}
		return $award;
	}

	public function getInterSchoolCompetitionsAward($recommendMerit) {
		global $Lang;
		
		if ($recommendMerit) {
//			list($merit,$number) = explode('_',$recommendMerit);
//			if ($_SESSION['intranet_session_language'] == 'en') {
//				$awardName = $Lang['iPortfolio']['JinYing']['Report']['Award'][$merit];
//				if ($number>1) {
//					$awardName .= 's';
//				}
//				$award = sprintf($Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-03'],$number,$awardName);
//			}
//			else {
//				$award = sprintf($Lang['iPortfolio']['JinYing']['CodeAward']['BTP-IC-P-03'],$Lang['iPortfolio']['JinYing']['Report']['Award'][$merit],$number);
//			}

            $award = $Lang['iPortfolio']['JinYing']['Award'][$recommendMerit];
		}
		else {
			$award = $Lang['iPortfolio']['JinYing']['Report']['Award']['Excellent'];
		}
		return $award;
	}
						
	// Note: WebSAMSRegNo without prefix # is regarded as register number, it'll be blank if not input this field
	// return student associate array by UserID
	public function getStudentInfo() {
		if ($this->studentList && $this->academicYearID) {
			$name_field = getNameFieldByLang("u.");			
			$sql = "SELECT 	u.UserID,
							$name_field AS StudentName,
							IF(LEFT(u.WebSAMSRegNo,1)='#',RIGHT(u.WebSAMSRegNo,LENGTH(u.WebSAMSRegNo)-1),u.WebSAMSRegNo) as RegisterNumber,
							".Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN')." AS ClassName,
							ycu.ClassNumber
					FROM 
						".$this->intranet_db.".INTRANET_USER u
					INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID 
                    WHERE 
						yc.AcademicYearID='".$this->academicYearID."'
					AND u.UserID IN ('".implode("','",$this->studentList)."') 
 					ORDER BY ycu.ClassNumber";
 					
			$rs = $this->objDB->returnResultSet($sql);
			$result = BuildMultiKeyAssoc($rs,'UserID');
			
			return $result; 					
		}
		else {
			return '';
		}
	}

	/*
	 * 	get form name by first student in the list, use to check Form 6 student or not
	 */
	public function getFormNameByFirstStudent() {
		if ($this->studentList && $this->academicYearID) {
			if ($this->studentList[0]) {			
				$sql = "SELECT 	y.YearName
						FROM 
							".$this->intranet_db.".INTRANET_USER u
						INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
						INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID
						INNER JOIN YEAR y ON y.YearID=yc.YearID 
	                    WHERE 
							yc.AcademicYearID='".$this->academicYearID."'
						AND u.UserID='".$this->studentList[0]."'";
	 					
				$rs = $this->objDB->returnResultSet($sql);
				return (count($rs) == 1) ? $rs[0]['YearName'] : '';
			}
		}
		return '';
	}

	// performance by semester
	// include: BSQ-HW, BSQ-PC, BSQ-AD, BSQ-AI, BSQ-RC, BSQ-RE, EPW-CA, EPW_PW, EPW_OC
	public function getPerformanceWiTerm() {
		if ($this->task && $this->studentList && $this->academicYearID) {
			$cond  = " AND p.YearTermID>0";
			$cond .= ($this->task == 'tmchkwc_slp') ? " AND p.PerformanceCode LIKE '___-__-P-%'" : "";   
			$sql = "SELECT 	p.UserID,
							LEFT(p.PerformanceCode,6) AS Item,
							p.PerformanceCode,
							p.YearTermID
					FROM	".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
					WHERE	p.AcademicYearID='".$this->academicYearID."'
					AND 	p.UserID IN ('".implode("','",$this->studentList)."')
					$cond
					ORDER BY p.UserID, Item, p.YearTermID";
						
			$rs = $this->objDB->returnResultSet($sql);
			$result = BuildMultiKeyAssoc($rs,array('UserID','Item','YearTermID'));
			return $result; 					
		}
		else {
			return '';
		}
	}

	// performance by whole year, no activity
	// include: EPW-PY, EPW-PJ, EPW-PS, OFS-SO, BTP-IS, BTP-TP
	public function getPerformanceWoTerm() {
		if ($this->task && $this->studentList && $this->academicYearID) {
			$cond  = " AND p.YearTermID=0 AND LEFT(p.PerformanceCode,6) IN ('EPW-PY','EPW-PJ','EPW-PS','OFS-SO','BTP-IS','BTP-TP')";
			$cond .= ($this->task == 'tmchkwc_slp') ? " AND p.PerformanceCode LIKE '___-__-P-%'" : "";   
			$sql = "SELECT 	p.UserID,
							LEFT(p.PerformanceCode,6) AS Item,
							p.PerformanceCode,
							p.ServiceHours							
					FROM	".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
					WHERE	p.AcademicYearID='".$this->academicYearID."'
					AND 	p.UserID IN ('".implode("','",$this->studentList)."')
					$cond
					ORDER BY p.UserID, Item";
						
			$rs = $this->objDB->returnResultSet($sql);
			$result = BuildMultiKeyAssoc($rs,array('UserID','Item'));
			return $result; 					
		}
		else {
			return '';
		}
	}

	// performance by whole year with Activity, no term
	// include: BSQ-EC, BSQ-TA, EPW-TA, OFS-SI, BTP-EA
	public function getPerformanceWiActivity() {
		if ($this->task && $this->studentList && $this->academicYearID) {
			$cond  = " AND p.YearTermID=0 AND LEFT(p.PerformanceCode,6) IN ('BSQ-EC','BSQ-TA','EPW-TA','OFS-SI','BTP-EA')";
			$cond .= ($this->task == 'tmchkwc_slp') ? " AND (p.PerformanceCode LIKE '___-__-P-%' OR p.PerformanceCode IN ('BTP-EA-N-01'))" : "";   
			$sql = "SELECT 	p.UserID,
							LEFT(p.PerformanceCode,6) AS Item,
							p.PerformanceCode,
							p.ActivityNameEng,
							p.ActivityNameChi,
							IF(p.ActivityNameEng='' OR p.ActivityNameEng IS NULL,p.ActivityNameChi, p.ActivityNameEng) AS Activity,
							p.AwardNameEng,
							p.AwardNameChi,
							p.RecommendMerit,
							p.OrganizationNameEng,
							p.OrganizationNameChi
					FROM	".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
					WHERE	p.AcademicYearID='".$this->academicYearID."'
					AND 	p.UserID IN ('".implode("','",$this->studentList)."')
					$cond
					ORDER BY p.UserID, Item";
						
			$rs = $this->objDB->returnResultSet($sql);
			$result = BuildMultiKeyAssoc($rs,array('UserID','Item','Activity'));

			return $result; 					
		}
		else {
			return '';
		}
	}

	// performance by whole year with Activity, no term
	// include: BTP-IC
	public function getPerformance_BTP_IC() {
	    if ($this->task && $this->studentList && $this->academicYearID) {
	        $cond  = " AND p.YearTermID=0 AND LEFT(p.PerformanceCode,6) IN ('BTP-IC')";
	        $cond .= ($this->task == 'tmchkwc_slp') ? " AND (p.PerformanceCode LIKE '___-__-P-%' OR p.PerformanceCode IN ('BTP-IC-N-01')) AND p.PerformanceCode<>'BTP-IC-P-04'" : "";
	        $sql = "SELECT 	p.UserID,
							LEFT(p.PerformanceCode,6) AS Item,
							p.PerformanceCode,
							p.ActivityNameEng,
							p.ActivityNameChi,
							IF(p.ActivityNameEng='' OR p.ActivityNameEng IS NULL,p.ActivityNameChi, p.ActivityNameEng) AS Activity,
                            IF(p.AwardNameEng IS NOT NULL AND p.AwardNameEng<>'',p.AwardNameEng, IF(p.AwardNameChi IS NOT NULL AND p.AwardNameChi<>'',p.AwardNameChi, '0')) AS Award,
							p.AwardNameEng,
							p.AwardNameChi,
							p.RecommendMerit,
							p.OrganizationNameEng,
							p.OrganizationNameChi
					FROM	".$this->eclass_db.".PORTFOLIO_JINYING_SCHEME p
					WHERE	p.AcademicYearID='".$this->academicYearID."'
					AND 	p.UserID IN ('".implode("','",$this->studentList)."')
					$cond
					ORDER BY p.UserID, Item";
					
					$rs = $this->objDB->returnResultSet($sql);
					$result = BuildMultiKeyAssoc($rs,array('UserID','Item','Activity','Award'));
					
					return $result;
	    }
	    else {
	        return '';
	    }
	}
	
	public function _handleCurrentAlumniStudentBeforePrinting($trgType,$alumni_StudentID,$StudentArr){
		global $intranet_root;
		
		$returnStudentAry = array();
		if(strtolower($trgType) == 'alumni' && count($alumni_StudentID) > 0){
			$returnStudentAry = $alumni_StudentID;

			for($a = 0, $a_max = count($returnStudentAry);$a < $a_max; $a++){

				$sql = 'select 
							 	a.UserID as `UserID`,
								a.UserLogin as `UserLogin`
						from '.$this->intranet_db.'.INTRANET_ARCHIVE_USER as a 
							 left join '.$this->intranet_db.'.INTRANET_USER as i on i.userid = a.userid 
						where 
							a.userid = \''.$returnStudentAry[$a].'\' and i.userid is NULL';
				$rs = $this->objDB->returnResultSet($sql);
		
				for($i = 0,$i_max = count($rs);$i < $i_max; $i++){
					$_userId = $rs[$i]['UserID'];
					$_userLogin = 'tmp'.$_userId.'_'.$rs[$i]['UserLogin'];
					$_userEmail = 'tmpUserEmail_'.$_userId.'@tmp.com';
		
					$sql = 'insert into '.$this->intranet_db.'.INTRANET_USER
								(UserID,UserLogin,UserEmail,EnglishName,ChineseName,WebSAMSRegNo, ClassName, ClassNumber, DateOfBirth, HKID, Gender)
								select 
									a.UserID , \''.$_userLogin.'\', \''.$_userEmail.'\',a.EnglishName, a.ChineseName, a.WebSAMSRegNo,a.ClassName, a.ClassNumber, DateOfBirth, HKID, Gender
								from 
									'.$this->intranet_db.'.INTRANET_ARCHIVE_USER as a 
								where 
									a.userid ='.$_userId;
					$result = $this->objDB->db_db_query($sql);
				}
			}

		}else{
			$returnStudentAry = $StudentArr;
		}
		return $returnStudentAry;
	}
	
	public function _handleCurrentAlumniStudentAfterPrinting($trgType,$StudentArr){
		global $intranet_root;
		
		if(strtolower($trgType) == 'alumni') {
			//remove the user id from intranet user
			for($a = 0, $a_max = count($StudentArr);$a < $a_max; $a++){
				$_userId = $StudentArr[$a];
				$_userLogin = 'tmp'.$_userId.'_';
				$_userEmail = 'tmpUserEmail_'.$_userId.'@tmp.com';
		
				//those tmp User Login make sure don't delete exist INTRANET_USER USER
				$delSQL = 'delete from '.$this->intranet_db.'.INTRANET_USER where UserID ='.$_userId.' and UserLogin LIKE \''.$_userLogin.'%\' and UserEmail = \''.$_userEmail.'\'';
				$delResult = $this->objDB->db_db_query($delSQL);
			}
		}
	}

	
	//------------------------------------------------ ABOVE ARE DATA HANDLING RELATED ------------------------------------------------//
	#####################################################################################################################################
	#																																	#
	#																																	#
	#####################################################################################################################################	
	//------------------------------------------------ BELOW ARE HTML AND CSS RELATED -------------------------------------------------//

	public function returnStyleSheet(){
		global $intranet_session_language;
		if ($intranet_session_language == 'b5') {
			$fonts = "'mingliu', 'Times New Roman'";
			$principalSignatureLineWidth = "2.1cm";
			$spaceBeforeSignature = "1.8cm";		// actual space is 2cm
		}
		else {
//            $fonts = "'mingliu', 'Times New Roman'";    // use the same font for all [case #J164766] <- Cancel
			$fonts = "'Times New Roman', 'mingliu'";
			$principalSignatureLineWidth = "4.1cm";
			$spaceBeforeSignature = "1.6cm";		// actual space is 2cm
		}
		
		$style = "<style>
			body {font-family: $fonts; color: #000000; font-size: 10pt;}
			table { width:100%;}
			td {padding: 3px;}
			.contentTable {border-collapse:collapse;}
			.contentTable th, .contentTable td {
				border: 1px solid black;
			} 
			.schoolName {font-size: 1.2em; font-weight:bold; text-align:center;}
			.reportTitle {font-weight:bold; text-align:center;}
			.scope {margin-bottom:5px; margin-left:5px; font-weight:bold;}
			.colHeader {text-align:center;}
			.alignCenter {text-align:center;}
			.bold {font-weight:bold;}
			.chiFont {font-family:'mingliu' !important}
			.principalSignatureTable{border:0px;width:$principalSignatureLineWidth;border-collapse: collapse; border-spacing: 0;}
			.teacherSignatureTable{border:0px;width:4cm;border-collapse: collapse; border-spacing: 0;}
			.signatureLine{border-top:1px solid #000000;}
			.spaceBeforeSignature{line-height: $spaceBeforeSignature;}
		</style>";

		return $style;
	}
	
	// get header part that include school and student info
	public function getReportHeader()
	{
	    global $Lang;
	
	    $x = '';
	    if ($this->studentID && $this->reportInfo) {
	        
	        $reportTitle = sprintf(($this->task == 'tmchkwc_slp') ? $Lang['iPortfolio']['JinYing']['Report']['Title']['SLP'] : $Lang['iPortfolio']['JinYing']['Report']['Title']['JYR'],$this->academicStartYear,$this->academicEndYear );
	        $rs = $this->reportInfo;
	        
	        ## school badge, school name & report title
	        $x .= '	<table width="100%" border="0">
						<tr>
							<td width="20%" rowspan="3"><img src="'.$this->schoolBadge.'" style="width:100px; height:99px;"></td>
							<td width="60%">&nbsp;</td>
							<td width="20%">&nbsp;</td>
						</tr>
						<tr>
							<td width="60%" class="schoolName">'.$this->schoolName.'</td>
							<td width="20%">&nbsp;</td>
						</tr>
						<tr>
							<td width="60%" class="reportTitle">'.$reportTitle.'</td>
							<td width="20%">&nbsp;</td>
						</tr>
				  	</table>';
	        
	        ## student info
	        $x .= '<table width="100%" border="0">
					<tr>
						<td width="14%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['StudentName'].'</td>
						<td width="3%"> : </td>
						<td width="46%" colspan="2" style="white-space: nowrap">'.$rs['StudentName'].'</td>
						<td width="14%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['ClassName'].'</td>
						<td width="3%"> : </td>
						<td width="20%">'.$rs['ClassName'].'</td>
					</tr>
					<tr>
						<td width="14%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['RegisterNumber'].'</td>
						<td width="3%"> : </td>
						<td width="20%">'.$rs['RegisterNumber'].'</td>
						<td width="26%"></td>
						<td width="14%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['ClassNumber'].'</td>
						<td width="3%"> : </td>
						<td width="20%">'.$rs['ClassNumber'].'</td>
					</tr>
					<tr>
						<td colspan="4">&nbsp;</td>
						<td width="14%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['DateOfIssue'].'</td>
						<td width="3%"> : </td>
						<td width="20%">'.$this->issueDate.'</td>
					</tr>
				  </table>';
	    }
	    
	    return $x;	        
	}
	
	public function getReportContentLayout() {
		global $Lang,$intranet_root,$intranet_session_language, $sys_custom;
		
		if ($this->studentID && $this->reportInfo) {

			## student info
//		    $x = $this->getReportHeader();
            $x = '';
		    
			$x .= '<table width="100%" border="0">
					<tr>
						<td class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Details'].'</td>
					</tr>
					<tr>
						<td class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['DetailsRemark'].'</td>
					</tr>
				  </table>';
				
			$x .= '<br>';

			$partCounter = 0;
			$totalPartsToShow = 0;
			foreach((array)$sys_custom['iPf']['JinYingReport'] as $showPart) {
			    if ($showPart) {
                    $totalPartsToShow++;
                }
            }
			## core report content
            if ($sys_custom['iPf']['JinYingReport']['ShowBetteringSelfQuality']) {
                if ($partCounter == $totalPartsToShow - 1) {
                    $x .= '<div style="page-break-inside: avoid;">';        // print with Signature in the same page
                }
                $x .= $this->getBSQTable($partCounter);
                $partCounter++;
            }

            if ($sys_custom['iPf']['JinYingReport']['ShowEnhancingPersonalWellBeing']) {
                if ($partCounter) {
                    $x .= '<br><br>';
                }
                if ($partCounter == $totalPartsToShow - 1) {
                    $x .= '<div style="page-break-inside: avoid;">';
                }
                $x .= $this->getEPWTable($partCounter);
                $partCounter++;
            }

            if ($sys_custom['iPf']['JinYingReport']['ShowOfferingServices']) {
                if ($partCounter) {
                    $x .= '<br><br>';
                }
                if ($partCounter == $totalPartsToShow - 1) {
                    $x .= '<div style="page-break-inside: avoid;">';
                }
                $x .= $this->getOFSTable($partCounter);
                $partCounter++;
            }

            if ($sys_custom['iPf']['JinYingReport']['ShowBuildingTalents']) {
                if ($partCounter) {
                    $x .= '<br><br>';
                }
                if ($partCounter == $totalPartsToShow - 1) {
                    $x .= '<div style="page-break-inside: avoid;">';
                }
                $x .= $this->getBTPTable($partCounter);
                $partCounter++;
            }

			if ($this->task == 'tmchkwc_jyr') {
				$medal = $this->getMedal();
				if ($medal) {
					$x .= '<br><br>';
					$x .= $medal;
				}
			}

			## report footer
			$principal_stamp = $intranet_session_language == 'b5' ? $intranet_root.'/file/customization/tmchkwc/principal_stamp_chi.jpg' : $intranet_root.'/file/customization/tmchkwc/principal_stamp_eng.jpg'; 

			$x .= '<table width="100%" border="0" style="page-break-inside: avoid; border-collapse: collapse; border-spacing: 0;">
					<tr><td colspan="2" class="spaceBeforeSignature">&nbsp;</td></tr>
					<tr><td width="50%" class="alignCenter">&nbsp;</td>
						<td width="50%" class="alignCenter"><img src="'.$principal_stamp.'"></td>
					</tr>
					<tr>
<!-- 						<td width="50%" class="alignCenter"><table class="teacherSignatureTable"><tr><td class="signatureLine">'.$Lang['iPortfolio']['JinYing']['Report']['ClassTeacher'].'</td></tr></table></td>-->
                        <td width="50%" class="alignCenter">&nbsp;</td>
						<td width="50%" class="alignCenter"><table class="principalSignatureTable"><tr><td class="signatureLine">'.$Lang['iPortfolio']['JinYing']['Report']['Principal'].'</td></tr></table></td>
					</tr>
				   </table>';
			$x .= '</div>';		// page with part D
								
			return $x;
			
		}
	}
	
	
	public function getBSQTable($partCounter) {
		global $Lang;
//		$goodPointCode = array('BSQ-HW-P-01','BSQ-PC-P-01','BSQ-AD-P-01');
//		$targetMetCode = array('BSQ-HW-P-02','BSQ-PC-P-02','BSQ-AI-P-01','BSQ-RC-P-01','BSQ-RE-P-01','BSQ-EC-P-01','BSQ-EC-P-02','BSQ-TA-P-01','BSQ-TA-P-02');
		$excludeCodeAry = array('EPW-CA','EPW-PW','EPW-OC');

		$nrSemester = $this->getNumberOfTerms();
		$terms = $this->academicYearTerms;
		$rs = $this->reportInfo;
        $scopeTitle = sprintf($Lang['iPortfolio']['JinYing']['Report']['Scope']['BSQ'], $Lang['iPortfolio']['JinYing']['Report']['Index'][$partCounter]);
		$x = '<div style="page-break-inside: avoid;">';
		$x .= '<div class="scope">'.$scopeTitle . '</div>';
		
		if (($this->task == 'tmchkwc_slp') && (!$rs['BSQ-HW']) && (!$rs['BSQ-PC']) && (!$rs['BSQ-AD']) && (!$rs['BSQ-AI']) && (!$rs['BSQ-RC']) && (!$rs['BSQ-RE'])
			&& (!$rs['BSQ-EC'])	&& (!$rs['BSQ-TA'])) {
				$x .= '<div>'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . '</div>';				
			}
		else {
			$x .= '	<table width="100%" class="contentTable">
						<thead>
							<tr>
								<th width="20%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Item'].'</th>
								<th width="30%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['BasicRequirements'].'</th>
								<th width="38%" class="colHeader" colspan="2">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Performance'].'</th>
								<th width="12%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Award'].'</th>
							</tr>
						</thead>
						<tbody>';
						
			## Homework, Punctuality, Attendance, Attire, Extensive Reading (Chinese & English) 					
			foreach((array)$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm'] as $item_code=>$item_name) {
				if (!in_array($item_code, (array)$excludeCodeAry)) {
					$i = 0;
					$nrShowTerms = $nrSemester;
					$showTerms = array();
					if ($this->task == 'tmchkwc_slp') {
						foreach((array)$terms as $term_id=>$term_name) {
							if ($rs[$item_code][$term_id]['PerformanceCode']) {
								$showTerms[$term_id] = $term_name;
							}
							else {
								$nrShowTerms--;
							}
						}
					}
					else {
						$showTerms = $terms;
					}

					foreach((array)$showTerms as $term_id=>$term_name) {
						$performanceCode = $rs[$item_code][$term_id]['PerformanceCode'];

						$x .= '	<tr>';
						if ($i == 0) {
                            $x .= '<td width="20%" rowspan="'.$nrShowTerms.'" class="bold">'.$item_name.'</td>';
                            $x .= '<td width="30%" rowspan="'.$nrShowTerms.'">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements'][$item_code] .'</td>';
                        }
						$x .= '		<td width="10%" class="alignCenter">'.$term_name.'</td>
									<td width="28%">'.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode].'</td>
									<td width="12%">'.$Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode].'</td>
								</tr>';
						$i++;
					}
				}
			}
			
			## Excursion
			if ($rs['BSQ-EC']) {
				$nrActivity = count($rs['BSQ-EC']);
				$i = 1;		// row counter
				foreach((array)$rs['BSQ-EC'] as $item) {
					$performanceCode = $item['PerformanceCode'];
					$performanceDesc = ($nrActivity > 1) ? '('.$i.') ' : '';
					$performanceDesc .= Get_Lang_Selection($item['ActivityNameChi'],$this->replaceUtf8ToAscii($item['ActivityNameEng']));
					if ($_SESSION['intranet_session_language'] == 'en') { 
						$performanceDesc .= '(<span class="chiFont">'.$item['ActivityNameChi'].'</span>)';
					}
                    $performanceDesc .= ' : ' . $Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode];

					$x .= '	<tr>';
					if ($i == 1) {
                        $x .= '<td width="20%" rowspan="'.$nrActivity.'" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BSQ-EC'].'</td>';
                        $x .= '<td width="30%" rowspan="'.$nrActivity.'">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-EC'].'</td>';
                    }

					$x .= '		<td width="38%" colspan="2">'.$performanceDesc.'</td>
								<td width="12%">'.$Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode].'</td>
							</tr>';
					$i++;
				}
			}
			else {
				if ($this->task == 'tmchkwc_jyr') {
					$x .= '	<tr>
								<td width="20%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BSQ-EC'].'</td>
								<td width="30%">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-EC'].'</td>
								<td width="38%" colspan="2">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="12%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
				}
			}
			
			## Training
			if ($rs['BSQ-TA']) {
				$nrActivity = count($rs['BSQ-TA']);
				$i = 1;		// row counter
				foreach((array)$rs['BSQ-TA'] as $item) {
					$performanceCode = $item['PerformanceCode'];
					$performanceDesc = ($nrActivity > 1) ? '('.$i.') ' : '';
					$performanceDesc .= Get_Lang_Selection($item['ActivityNameChi'],$this->replaceUtf8ToAscii($item['ActivityNameEng']));
                    $performanceDesc .= ' : ' . $Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode];

                    $x .= '	<tr>';
//					$x .= ($i == 1) ? '<td width="18%" rowspan="'.$nrActivity.'" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BSQ-TA'].'</td>' : '';
//                    $x .= '		<td width="57%" colspan="2">'.$performanceDesc.'</td>
//								<td width="25%">'.$Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode].'</td>
//							</tr>';
                    if ($i == 1) {
                        $x .= '<td width="20%" rowspan="'.$nrActivity.'" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BSQ-TA'].'</td>';
                        $x .= '<td width="30%" rowspan="'.$nrActivity.'">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-TA'].'</td>';
                    }
                        $x .= '<td width="38%" colspan="2">'.$performanceDesc.'</td>
								<td width="12%">'.$Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode].'</td>
							</tr>';
					$i++;
				}
			}
			else {
				if ($this->task == 'tmchkwc_jyr') {
					$x .= '	<tr>
								<td width="20%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BSQ-TA'].'</td>
								<td width="30%">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['BSQ-TA'].'</td>
								<td width="38%" colspan="2">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="12%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
				}
			}
			
			$x .= '		</tbody>
					</table>';
		}
		$x .= '</div>';
		
		return $x; 
	}	


	private function getEPWWithTermTable($code)
    {
        global $Lang;
        $nrSemester = $this->getNumberOfTerms();
        $terms = $this->academicYearTerms;
        $rs = $this->reportInfo;

        $i = 0;
        $nrShowTerms = $nrSemester;
        $showTerms = array();
        if ($this->task == 'tmchkwc_slp') {
            foreach((array)$terms as $term_id=>$term_name) {
                if ($rs[$code][$term_id]['PerformanceCode']) {
                    $showTerms[$term_id] = $term_name;
                }
                else {
                    $nrShowTerms--;
                }
            }
        }
        else {
            $showTerms = $terms;
        }

        if (count($showTerms) > 0) {
            $recordExist = true;
        }

        $x = '';
        foreach((array)$showTerms as $term_id=>$term_name) {
            $performanceCode = $rs[$code][$term_id]['PerformanceCode'];

            $x .= '	<tr>';
            if ($i==0) {
                $x .= '<td width="20%" rowspan="'.$nrShowTerms.'" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiTerm'][$code].'</td>';
                $x .= '<td width="40%" rowspan="'.$nrShowTerms.'">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements'][$code].'</td>';
            }

            $x .= '		<td width="10%" class="alignCenter">'.$term_name.'</td>
						<td width="30%">'.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode].'</td>
					</tr>';
            $i++;
        }
        return $x;
    }

	public function getEPWTable($partCounter) {
		global $Lang;
		$nrSemester = $this->getNumberOfTerms();
		$terms = $this->academicYearTerms;
		$rs = $this->reportInfo;
		$recordExist = false;
		
		$x = '	<table width="100%" class="contentTable">
					<thead>
						<tr>
                            <th width="20%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Item'].'</th>
                            <th width="40%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['BasicRequirements'].'</th>
                            <th width="40%" class="colHeader" colspan="2">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Performance'].'</th>
						</tr>
					</thead>
					<tbody>';

        ## Physique, Physical agility (jogging), Physical agility (swimming)
        foreach((array)$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm'] as $item_code=>$item_name) {
            if (substr($item_code,0,3) == 'EPW') {
                $performanceCode = $rs[$item_code]['PerformanceCode'];

                if ($item_code == 'EPW-PS') {	// Physical agility (swimming)
                    if($rs[$item_code]) {
                        $recordExist = true;
                        $x .= '	<tr>';
                        $x .= '		<td width="20%" class="bold">'.$item_name.'</td>
									<td width="40%">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements'][$item_code].'</td>
									<td width="40%" colspan="2">'.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode].'</td>
								</tr>';
                    }
                    else {
                        if ($this->task == 'tmchkwc_jyr') {
                            $x .= '	<tr>
										<td width="20%" class="bold">'.$item_name.'</td>
										<td width="40%">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements'][$item_code].'</td>
										<td width="40%" colspan="2">'.$Lang['iPortfolio']['JinYing']['Report']['NotApplicable'].'</td>
									</tr>';
                        }
                    }
                }
                else {
                    if ($performanceCode || ($this->task == 'tmchkwc_jyr')) {
                        $recordExist = true;
                        $x .= '	<tr>';
                        $x .= '		<td width="20%" class="bold">'.$item_name.'</td>
									<td width="40%">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements'][$item_code].'</td>
									<td width="40%" colspan="2">'.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode].'</td>
								</tr>';
                    }
                }
            }
        }

		## Physical well-being and Participation in Christian activities
        $epwTermCodeAry = array('EPW-PW','EPW-CA');
        foreach($epwTermCodeAry as $code) {
            $x .= $this->getEPWWithTermTable($code);
        }

		## Training
		if ($rs['EPW-TA']) {
			$recordExist = true;
			$nrActivity = count($rs['EPW-TA']);
			$i = 1;		// row counter
			foreach((array)$rs['EPW-TA'] as $item) {
				$performanceCode = $item['PerformanceCode'];
				$performanceDesc = ($nrActivity > 1) ? '('.$i.') ' : '';
				$performanceDesc .= Get_Lang_Selection($item['ActivityNameChi'],$this->replaceUtf8ToAscii($item['ActivityNameEng']));
                $performanceDesc .= ' : ' . $Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode];

				$x .= '	<tr>';
                if ($i == 1) {
                    $x .= '<td width="20%" rowspan="'.$nrActivity.'" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['EPW-TA'].'</td>';
                    $x .= '<td width="40%" rowspan="'.$nrActivity.'">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-TA'].'</td>';
                }
				$x .= '		<td width="40%" colspan="2">'.$performanceDesc.'</td>
						</tr>';
				$i++;
			}
		}
		else {
			if ($this->task == 'tmchkwc_jyr') {
				$x .= '	<tr>
							<td width="20%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['EPW-TA'].'</td>
                            <td width="40%">'.$Lang['iPortfolio']['JinYing']['Report']['BasicRequirements']['EPW-TA'].'</td>							
							<td width="40%" colspan="2">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
						</tr>';
			}
		}

		## Overall comment
        $x .= $this->getEPWWithTermTable('EPW-OC');

		$x .= '		</tbody>
				</table>';

        $scopeTitle = sprintf($Lang['iPortfolio']['JinYing']['Report']['Scope']['EPW'], $Lang['iPortfolio']['JinYing']['Report']['Index'][$partCounter]);

		$y = '<div class="scope">'.$scopeTitle . '</div>';
		if (!$recordExist && ($this->task == 'tmchkwc_slp')) {
			$x = '<div style="page-break-inside: avoid;">'.$y .'<div>'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . '</div></div>';
		}
		else {
			$x = '<div style="page-break-inside: avoid;">'.$y.$x.'</div>';
		}
		return $x; 
	}	

	public function getOFSTable($partCounter) {
		global $Lang, $intranet_root;
		$rs = $this->reportInfo;

        $scopeTitle = sprintf($Lang['iPortfolio']['JinYing']['Report']['Scope']['OFS'], $Lang['iPortfolio']['JinYing']['Report']['Index'][$partCounter]);

		$x = '<div style="page-break-inside: avoid;">';
		$x .= '<div class="scope">'.$scopeTitle . '</div>';
		if (!$rs['OFS-SI'] && !$rs['OFS-SO'] && ($this->task == 'tmchkwc_slp') ) {
			$x .= '<div>'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . '</div>';
		}
		else {	
			
			$x .= '	<table width="100%" class="contentTable">
						<thead>
							<tr>
								<th width="30%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Item'].'</th>
								<th width="45%" class="colHeader">'.$Lang['General']['Record'].'</th>
								<th width="25%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['PerformanceAward'].'</th>
							</tr>
						</thead>
						<tbody>';
				
			## Services in school
			if ($rs['OFS-SI']) {
				$nrActivity = count($rs['OFS-SI']);
				$i = 1;		// row counter
				foreach((array)$rs['OFS-SI'] as $item) {
					$performanceCode = $item['PerformanceCode'];
					$performanceDesc = ($nrActivity > 1) ? '('.$i.') ' : '';
					$performanceDesc .= Get_Lang_Selection($item['ActivityNameChi'],$this->replaceUtf8ToAscii($item['ActivityNameEng']));
					$award = $this->getServiceInSchoolAward($item['RecommendMerit']);
					
					$x .= '	<tr>';
					$x .= ($i == 1) ? '<td width="30%" rowspan="'.$nrActivity.'" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['OFS-SI'].'</td>' : '';
					$x .= '		<td width="45%">'.$performanceDesc.'</td>
								<td width="25%">'.$award.'</td>
							</tr>';
					$i++;
				}
			}
			else {
				if ($this->task == 'tmchkwc_jyr') {
					$x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['OFS-SI'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
				}
			}
	
			## Services outside school
			if ($rs['OFS-SO']) {
				$performanceCode = $rs['OFS-SO']['PerformanceCode'];
				$performanceDesc = sprintf($Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode],round($rs['OFS-SO']['ServiceHours'],0));
				include_once($intranet_root."/includes/portfolio25/customize/tmchkwc/libpf-jinying.php");
				$ljy = new libJinYing();
				$nbrGP = $ljy->getGoodPointsByHours($rs['OFS-SO']['ServiceHours']);
				
				if ($nbrGP) {
					$award = sprintf($Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode],$nbrGP);
					if (($_SESSION['intranet_session_language'] == 'en') && ($nbrGP>1)) {
						$award .= 's';
					}
				}
				else {
					$award = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
				}
				$x .= '	<tr>
							<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['OFS-SO'].'</td>
							<td width="45%">'.$performanceDesc.'</td>
							<td width="25%">'.$award.'</td>
						</tr>';
			}
			else {
				if ($this->task == 'tmchkwc_jyr') {
					$x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['OFS-SO'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
				}
			}
		
			$x .= '		</tbody>
					</table>';
		}
		$x .= '</div>';		
		return $x; 
	}	

	/*
	 * 	Limitation:
	 * 	1. Assume total number of EA, IS, IC and TP are limited to one page
	 */
	public function getBTPTable($partCounter) {
		global $Lang;
		$rs = $this->reportInfo;
		$nrEA = count($rs['BTP-EA']);
		$nrIC= 0;
		foreach ((array)$rs['BTP-IC'] as $_BTPIC) {
		    $nrIC += count($_BTPIC);
		}
		$nrIS = (isset($rs['BTP-IS']) && count($rs['BTP-IS'])) ? 1 : 0;     // at most one line only for BTP-IS
        $nrTP = (isset($rs['BTP-TP']) && count($rs['BTP-TP'])) ? 1 : 0;     // at most one line only for BTP-TP

		$nrRec = $nrEA + $nrIC + $nrIS + $nrTP;
		if ($nrRec > 11 ) {
			$x = $this->getBTPTable2($partCounter);
			return $x;
		}

        $scopeTitle = sprintf($Lang['iPortfolio']['JinYing']['Report']['Scope']['BTP'], $Lang['iPortfolio']['JinYing']['Report']['Index'][$partCounter]);

		$x = '<div style="page-break-inside: avoid;">';
		$x .= '<div class="scope">'.$scopeTitle . '</div>';
		
		if (!$rs['BTP-EA'] && !$rs['BTP-IC'] && !$rs['BTP-IS'] && !$rs['BTP-TP'] && ($this->task == 'tmchkwc_slp') ) {
			$x .= '<div>'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . '</div>';
		}
		else {
			$x .= '	<table width="100%" class="contentTable">
						<thead>
							<tr>
								<th width="30%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Item'].'</th>
								<th width="45%" class="colHeader">'.$Lang['General']['Record'].'</th>
								<th width="25%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['PerformanceAward'].'</th>
							</tr>
						</thead>
						<tbody>';
				
			## Participation in extracurricular activities
			if ($rs['BTP-EA']) {
				$nrActivity = count($rs['BTP-EA']);
				$i = 1;		// row counter
				foreach((array)$rs['BTP-EA'] as $item) {
					$performanceCode = $item['PerformanceCode'];
					$performanceDesc = ($nrActivity > 1) ? '('.$i.') ' : '';
					$performanceDesc .= Get_Lang_Selection($item['ActivityNameChi'],$this->replaceUtf8ToAscii($item['ActivityNameEng'])).' : '.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode];
					
					$x .= '	<tr>';
					$x .= ($i == 1) ? '<td width="30%" rowspan="'.$nrActivity.'" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-EA'].'</td>' : '';
					$x .= '		<td width="45%">'.$performanceDesc.'</td>
								<td width="25%">'.(($this->task == 'tmchkwc_slp' && $performanceCode == 'BTP-EA-N-01') ? $Lang['iPortfolio']['JinYing']['EmptySymbol'] : $Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode]).'</td>
							</tr>';
					$i++;
				}
			}
			else {
				if ($this->task == 'tmchkwc_jyr') {
					$x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-EA'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
				}
			}

			## Competitions in school
            if ($rs['BTP-IS']) {
                $performanceCode = $rs['BTP-IS']['PerformanceCode'];
                $x .= '	<tr>
                            <td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-IS'].'</td>
                            <td width="45%">'.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode].'</td>
                            <td width="25%">'.$Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode].'</td>
                        </tr>';
            }
            else {
                if ($this->task == 'tmchkwc_jyr') {
                    $x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-IS'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
                }
            }

			## Participation in inter-school competitions
			if ($rs['BTP-IC']) {
			    $nrActivity = $nrIC;
				$i = 1;		// row counter
				foreach((array)$rs['BTP-IC'] as $_item) {
				    foreach((array)$_item as $__item) {     // award
    					$performanceCode = $__item['PerformanceCode'];
    					$performanceDesc = ($nrActivity > 1) ? '('.$i.') ' : '';
    					$performanceDesc .= Get_Lang_Selection($__item['ActivityNameChi'],$this->replaceUtf8ToAscii($__item['ActivityNameEng']));
    					if ($_SESSION['intranet_session_language'] == 'en') { 
    						$performanceDesc .= '(<span class="chiFont">'.$__item['ActivityNameChi'].'</span>)';
    					}
    					$organization = Get_Lang_Selection($__item['OrganizationNameChi'],$this->replaceUtf8ToAscii($__item['OrganizationNameEng']));
    					
    					switch ($performanceCode) {
    						case 'BTP-IC-N-01':
    							$performanceDesc .= ' : ';
    							$performanceDesc .= $Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode];
    							$award = ($this->task == 'tmchkwc_slp') ? $Lang['iPortfolio']['JinYing']['EmptySymbol'] : $Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode];
    							break;
    						case 'BTP-IC-P-01':
    						case 'BTP-IC-P-02':
    						case 'BTP-IC-P-04':
    							$performanceDesc .= ' : ';
    							$performanceDesc .= $Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode];
    							$award = $Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode];
    							break;
    						case 'BTP-IC-P-03': 
    							$performanceDesc .= ' : ';
    							$performanceDesc .= Get_Lang_Selection($__item['AwardNameChi'],$__item['AwardNameEng']);
    							$award = $this->getInterSchoolCompetitionsAward($__item['RecommendMerit']);
    							break;
    						default:
    							$award = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
    							break;
    					} 
    
    					if (!empty($organization)) {
    						$performanceDesc .= "<br>".$Lang['iPortfolio']['JinYing']['Report']['Organizer']." : ".$organization;
    					}
    					
    					$x .= '	<tr>';
    					$x .= ($i == 1) ? '<td width="30%" rowspan="'.$nrActivity.'" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-IC'].'</td>' : '';
    					$x .= '		<td width="45%">'.$performanceDesc.'</td>
    								<td width="25%">'.$award.'</td>
    							</tr>';
    					$i++;
				    }
				}
			}
			else {
				if ($this->task == 'tmchkwc_jyr') {
					$x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-IC'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
				}
			}

            ## Talent Performances
            if ($rs['BTP-TP']) {
                $performanceCode = $rs['BTP-TP']['PerformanceCode'];
                $x .= '	<tr>
                            <td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-TP'].'</td>
                            <td width="45%">'.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode].'</td>
                            <td width="25%">'.$Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode].'</td>
                        </tr>';
            }
            else {
                if ($this->task == 'tmchkwc_jyr') {
                    $x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-TP'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
                }
            }

			$x .= '		</tbody>
					</table>';
		}
		$x .= '</div>';
				
		return $x; 
	}	


	/*
	 * 	Auto-page break is applied to following method, but 
	 * 	Item name is repeated in each line
	 */
	public function getBTPTable2($partCounter) {
		global $Lang;
		$rs = $this->reportInfo;
		$nrEA = count($rs['BTP-EA']);
		$nrIC= 0;
		foreach ((array)$rs['BTP-IC'] as $_BTPIC) {
		    $nrIC += count($_BTPIC);
		}
        $nrIS = (isset($rs['BTP-IS']) && count($rs['BTP-IS'])) ? 1 : 0;     // at most one line only for BTP-IS
        $nrTP = (isset($rs['BTP-TP']) && count($rs['BTP-TP'])) ? 1 : 0;     // at most one line only for BTP-TP

        $nrRec = $nrEA + $nrIC + $nrIS + $nrTP;

        $scopeTitle = sprintf($Lang['iPortfolio']['JinYing']['Report']['Scope']['BTP'], $Lang['iPortfolio']['JinYing']['Report']['Index'][$partCounter]);

		$x = '<div>';
		$x .= '<div class="scope">'.$scopeTitle . '</div>';

        if (!$rs['BTP-EA'] && !$rs['BTP-IC'] && !$rs['BTP-IS'] && !$rs['BTP-TP'] && ($this->task == 'tmchkwc_slp') ) {
			$x .= '<div>'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'] . '</div>';
		}
		else {
			$x .= '	<table width="100%" class="contentTable">
						<thead>
							<tr>
								<th width="30%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['Item'].'</th>
								<th width="45%" class="colHeader">'.$Lang['General']['Record'].'</th>
								<th width="25%" class="colHeader">'.$Lang['iPortfolio']['JinYing']['Report']['ColumnTitle']['PerformanceAward'].'</th>
							</tr>
						</thead>
						<tbody>';
				
			## Participation in extracurricular activities
			if ($rs['BTP-EA']) {
				$nrActivity = count($rs['BTP-EA']);
				$i = 1;		// row counter
				foreach((array)$rs['BTP-EA'] as $item) {
					$performanceCode = $item['PerformanceCode'];
					$performanceDesc = ($nrActivity > 1) ? '('.$i.') ' : '';
					$performanceDesc .= Get_Lang_Selection($item['ActivityNameChi'],$this->replaceUtf8ToAscii($item['ActivityNameEng'])).' : '.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode];
					
					$x .= '	<tr>';
					$x .= '<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-EA'].'</td>';
//					if ($i==1 && $i!=$nrEA) {
//						$x .= '<td width="30%" class="bold" style="border-bottom:none;">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-EA'].'</td>';						
//					}
//					else if ($i==1 && $i==$nrEA) {
//						$x .= '<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-EA'].'</td>';
//					}
//					else if ($i!=1 && $i==$nrEA) {
//						$x .= '<td style="border-top:none;"></td>';
//					}
//					else {
//						$x .= '<td style="border-top:none; border-bottom:none;"></td>';
//					}
					$x .= '		<td width="45%">'.$performanceDesc.'</td>
								<td width="25%">'.(($this->task == 'tmchkwc_slp' && $performanceCode == 'BTP-EA-N-01') ? $Lang['iPortfolio']['JinYing']['EmptySymbol'] : $Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode]).'</td>
							</tr>';
					$i++;
				}
			}
			else {
				if ($this->task == 'tmchkwc_jyr') {
					$x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-EA'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
				}
			}

            ## Competitions in school
            if ($rs['BTP-IS']) {
                $performanceCode = $rs['BTP-IS']['PerformanceCode'];
                $x .= '	<tr>
                            <td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-IS'].'</td>
                            <td width="45%">'.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode].'</td>
                            <td width="25%">'.$Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode].'</td>
                        </tr>';
            }
            else {
                if ($this->task == 'tmchkwc_jyr') {
                    $x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-IS'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
                }
            }

			## Participation in inter-school competitions
			if ($rs['BTP-IC']) {
			    $nrActivity = $nrIC;
				$i = 1;		// row counter
				foreach((array)$rs['BTP-IC'] as $_item) {
				    foreach((array)$_item as $__item) {
    					$performanceCode = $__item['PerformanceCode'];
    					$performanceDesc = ($nrActivity > 1) ? '('.$i.') ' : '';
    					$performanceDesc .= Get_Lang_Selection($__item['ActivityNameChi'],$this->replaceUtf8ToAscii($__item['ActivityNameEng']));
    					if ($_SESSION['intranet_session_language'] == 'en') { 
    						$performanceDesc .= '(<span class="chiFont">'.$__item['ActivityNameChi'].'</span>)';
    					}
    					$organization = Get_Lang_Selection($__item['OrganizationNameChi'],$this->replaceUtf8ToAscii($__item['OrganizationNameEng']));
    					
    					switch ($performanceCode) {
    						case 'BTP-IC-N-01':
    							$performanceDesc .= ' : ';
    							$performanceDesc .= $Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode];
    							$award = ($this->task == 'tmchkwc_slp') ? $Lang['iPortfolio']['JinYing']['EmptySymbol'] : $Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode];
    							break;
    						case 'BTP-IC-P-01':
    						case 'BTP-IC-P-02':
    						case 'BTP-IC-P-04':
    							$performanceDesc .= ' : ';
    							$performanceDesc .= $Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode];
    							$award = $Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode];
    							break;
    						case 'BTP-IC-P-03': 
    							$performanceDesc .= ' : ';
    							$performanceDesc .= Get_Lang_Selection($__item['AwardNameChi'],$this->replaceUtf8ToAscii($__item['AwardNameEng']));
    							$award = $this->getInterSchoolCompetitionsAward($__item['RecommendMerit']);
    							break;
    						default:
    							$award = $Lang['iPortfolio']['JinYing']['EmptySymbol'];
    							break;
    					} 
    
    					if (!empty($organization)) {
    						$performanceDesc .= "<br>".$Lang['iPortfolio']['JinYing']['Report']['Organizer']." : ".$organization;
    					}
    					
    					$x .= '	<tr>';
    					$x .= '<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-IC'].'</td>';
    //					if ($i==1 && $i!=$nrIC) {
    //						$x .= '<td width="30%" class="bold" style="border-bottom:none;">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-IC'].'</td>';						
    //					}
    //					else if ($i==1 && $i==$nrIC) {
    //						$x .= '<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-IC'].'</td>';
    //					}
    //					else if ($i!=1 && $i==$nrIC) {
    //						$x .= '<td style="border-top:none;"></td>';
    //					}
    //					else {
    //						$x .= '<td style="border-top:none; border-bottom:none;"></td>';
    //					}
    					$x .= '		<td width="45%">'.$performanceDesc.'</td>
    								<td width="25%">'.$award.'</td>
    							</tr>';
    					$i++;
				    }
				}
			}
			else {
				if ($this->task == 'tmchkwc_jyr') {
					$x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WiActivity']['BTP-IC'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
				}
			}

            ## Talent Performances
            if ($rs['BTP-TP']) {
                $performanceCode = $rs['BTP-TP']['PerformanceCode'];
                $x .= '	<tr>
                            <td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-TP'].'</td>
                            <td width="45%">'.$Lang['iPortfolio']['JinYing']['CodeDesc'][$this->reportCode][$performanceCode].'</td>
                            <td width="25%">'.$Lang['iPortfolio']['JinYing']['CodeAward'][$performanceCode].'</td>
                        </tr>';
            }
            else {
                if ($this->task == 'tmchkwc_jyr') {
                    $x .= '	<tr>
								<td width="30%" class="bold">'.$Lang['iPortfolio']['JinYing']['Report']['Item']['WoTerm']['BTP-TP'].'</td>
								<td width="45%">'.$Lang['iPortfolio']['JinYing']['Report']['NoRecord'].'</td>
								<td width="25%">'.$Lang['iPortfolio']['JinYing']['EmptySymbol'].'</td>
							</tr>';
                }
            }

			$x .= '		</tbody>
					</table>';
		}
		$x .= '</div>';
				
		return $x; 
	}	

	public function getMedal() {
		global $Lang;

		$sql = "SELECT Medal FROM ".$this->eclass_db.".PORTFOLIO_JINYING_AWARD WHERE UserID='".$this->studentID."' AND AcademicYearID='".$this->academicYearID."'";
		$rs = $this->objDB->returnResultSet($sql);
		if (count($rs)) {
			$medal = $rs[0]['Medal'];
		}
		else {
			$medal = '';
		}
		
		$x = '';
		if ($medal) {
			$x = '<div class="scope">'.$Lang['iPortfolio']['JinYing']['Report']['Award'][$medal] . '</div>';
		}
		else {
			$x = '<div class="scope">'.$Lang['iPortfolio']['JinYing']['Report']['Award']['None'] . '</div>';
		}
		
		return $x;
	}

    public function replaceUtf8ToAscii($str)
    {
        $fromAry = array();
        $fromAry[] = '‐';       // utf8 character Hex=E28090
        $toAry = array();       // must keep in sequence with $fromAry
        $toAry[] = '-';         // ascii: 2D

        $return = str_replace($fromAry, $toAry, $str);
        return $return;
    }
	
//-- End of Class
}


?>