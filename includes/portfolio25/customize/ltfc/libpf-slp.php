<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	
	private $StudentName;
	private $StudentNumber;
	
	private $colorBlue;
	
	private $YearClassID;
	
	private $AcademicYearStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '---';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->StudentName = '';
		$this->StudentNumber = '';
		
		$this->colorBlue = '#0174DF';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->SchoolTitleEn = 'LAM TAI FAI COLLEGE';
		$this->SchoolTitleCh = '林大輝中學';
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	

	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '24';
	
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">
					<tr>
						<td rowspan="4" align="center" " style="width:18%;">&nbsp;</td>
						<td style="font-size:'.$header_font_size.'px;"><font face="Arial" color="'.$this->colorBlue.'"><b>'.$this->SchoolTitleEn.'<br/>'.$this->SchoolTitleCh.'</b></font></td>
						<td rowspan="4" align="left" style="width:18%;">&nbsp;</td>
				   </tr>';
			$x .= '<tr><td style="font-size:17px;">&nbsp;</td></tr>';
			$x .= '<tr><td style="font-size:17px;"><font face="Arial">STUDNET LEARNING PROFILE</font></td></tr>';
			$x .= '<tr><td style="font-size:17px;"><font face="Arial">('.$this->YearRangeStr.')</font></td></tr>';
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						WebSAMSRegNo,
						ClassNumber,
						ClassName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{		
		$fontSize = 'font-size:15px';
		
		$thisPartTitle = 'Student Particulars 學生資料';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;
		} else {
			$IssueDate = date('d/m/Y',strtotime($IssueDate));
		}
		
		$DOB_str = $result[0]["DateOfBirth"];
		if (date($DOB_str)==0) {
			$DOB_str = $this->EmptySymbol;
		} else {
			$DOB_str = date('d/m/Y',strtotime($DOB_str));
		}
		
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$EngNameArr = explode(' ',$EngName);
		$EngNameArr[0] = strtoupper($EngNameArr[0]);
		$EngName = implode(' ',$EngNameArr);
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$this->StudentName = $EngName.' ('.$ChiName.')';
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		
		if($Gender=='M')
		{
			$Gender ='Male';
		}
		else if($Gender=='F')
		{
			$Gender ='Female';
		}
		
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$RegistrationNo = $result[0]["WebSAMSRegNo"];
		$RegistrationNo = str_replace('#','',$RegistrationNo);
		$RegistrationNo = ($RegistrationNo=='')? $this->EmptySymbol:$RegistrationNo;
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$this->StudentNumber = $RegistrationNo;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;


		$html = '';
		$html .= '<table width="80%" cellpadding="2" border="0px" align="center" style="border-collapse: collapse;">
					<col width="10%" />
					<col width="25%" />
					<col width="12%" />
					<col width="18%" />
					<col width="15%" />
					<col width="20%" />

					<tr>
							<td style="">Name: </td>	
							<td style="" colspan="6">'.$EngName.' ('.$ChiName.')</td>				
					</tr>
					<tr>					
							<td style="'.$fontSize.'">Class: </td>
							<td style="'.$fontSize.'">'.$ClassName.'</td>
							<td style="'.$fontSize.'">Class No.: </td>
							<td style="'.$fontSize.'">'.$ClassNumber.'</td>
							<td style="'.$fontSize.'">Student No.: </td>
							<td style="'.$fontSize.'">'.$RegistrationNo.'</td>
					</tr>
					<tr>
							<td style="'.$fontSize.'">Sex: </td>
							<td style="'.$fontSize.'">'.$Gender.'</td>
							<td style="'.$fontSize.'">DOB:</td>
							<td style="'.$fontSize.'">'.$DOB_str.'</td>
							<td style="'.$fontSize.'">Date of Issue: </td>
							<td style="'.$fontSize.'">'.$IssueDate.'</td>
					</tr>
					';

		$html .= '</table>';
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Awards and Scholarships issued by the School******************/	
		public function getPart3_HTML($OLEInfoArr_Award){
		
		$Part5_Title = 'Awards and Scholarships issued by the School';
				
		$titleArray = array();
		$titleArray[] = 'School Year';
		$titleArray[] = 'Awards and Scholarships';
		$titleArray[] = 'Details';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="50%" />';

		$html = '';
	
	//	if(count($OLEInfoArr_Award)>0)
	//	{
			$html .= $this->getPartContentDisplay_NoRowLimit('3',$Part5_Title,$titleArray,$colWidthArr,$OLEInfoArr_Award,$hasPageBreak=false);
	//	}
			
		return $html;
		
	}	
	   

	/***************** End Part 3 ******************/
	
	/***************** Part 4 ******************/	
	public function getPart4_HTML($OLEInfoArr_SchoolPost){
		
		$Part4_Title = 'School Posts';
		
		$titleArray = array();
		$titleArray[] = 'School Year';
		$titleArray[] = 'Society / Club / Team / Committee';
		$titleArray[] = 'Post';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="50%" />';
		$colWidthArr[] = '<col width="35%" />';
		  
		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('4',$Part4_Title,$titleArray,$colWidthArr,$OLEInfoArr_SchoolPost,$hasPageBreak=false);
		
		return $html;
		
	}

	/***************** End Part 4 ******************/
	
	
	
	/***************** Part 5 : Other Learning Experience******************/	
	public function getPart5_HTML($OLEInfoArr_SchoolActivity){
		
		$Part5_Title = 'Major activities or services organized by the school';
				
		$titleArray = array();
		$titleArray[] = 'School Year';
		$titleArray[] = 'Activities or Services';
		$titleArray[] = 'Role';
		$titleArray[] = 'OLE Component*';
		$titleArray[] = 'Achievement';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="30%" />';
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="15%" />';  
		$colWidthArr[] = '<col width="25%" />'; 
		

		$html = '';
	//
	//	if(count($OLEInfoArr_SchoolActivity)>0)
	//	{
			$html .= $this->getPartContentDisplay_NoRowLimit('5',$Part5_Title,$titleArray,$colWidthArr,$OLEInfoArr_SchoolActivity,$hasPageBreak=false);
	//	}
			
		return $html;
		
	}	

	
	/***************** End Part 5 ******************/
	
	/***************** Part 6 : Other Learning Experience******************/	
	public function getPart6_HTML($OLEInfoArr_SchoolActivity){
		
		$Part6_Title = 'Major external activities / competitions';
				
		$titleArray = array();
		$titleArray[] = 'School Year';
		$titleArray[] = 'Activities / Competitions';
		$titleArray[] = 'Organizer';
		$titleArray[] = 'Achievement';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="50%" />';
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="15%" />';  

		$html = '';
	//
	//	if(count($OLEInfoArr_SchoolActivity)>0)
	//	{
			$html .= $this->getPartContentDisplay_NoRowLimit('6',$Part6_Title,$titleArray,$colWidthArr,$OLEInfoArr_SchoolActivity,$hasPageBreak=false);
	//	}
			
		return $html;
		
	}	

	
	/***************** End Part 6 ******************/

	
	
	/***************** Part 7 ******************/
	
	public function getPart7_HTML(){
		
		$Part7_Title = "Student's Self-Account";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:16px;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			
			$Detail = strip_tags($Detail,'<p>');
		}
		else
		{
			$Detail = 'No Record Found.';
		}	
		$html .= '<table width="90%" height="" cellpadding="2" border="0px" align="center" style="font-size:16px;border-collapse: collapse;">
					<tr> 
						<td style=" font-size:15px;text-align:left;vertical-align:text-top;">
						'.$MainTitle.'
						</td>
				  	</tr>
				  </table>';
		
		$html .= '<table width="90%" height="750px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr>				 							
						<td style="vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'">'.$Detail.'</td>						 														
					</tr>
				  </table>';	
		
		$html .=$this->getEmptyTable('1%');
		$html .=$this->getEmptyTable('1%','*** END OF PROFILE ***');
		return $html; 
	}
	
	
	/***************** End Part 6 ******************/
	
	
	
	/***************** Part 7 : Footer ******************/
	public function getSignature_HTML($PageBreak='')
	{
		$PrincipalName = "Wong Kwong Wai";
		$PrincipalStr = "Principal";
		
		$_borderTop =  'border-top: 1px solid black; ';
		
		$html = '';
		
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="font-size:15px;'.$PageBreak.'">		
					<col width="80%" />
					<col width="20%" />

				 	<tr>					
						<td>&nbsp;</td>
						<td align="center" style="'.$_borderTop.'"><b>'.$PrincipalName.'</b></td>
					</tr>
					<tr>					
						<td>&nbsp;</td>
						<td align="center" style=""><b>'.$PrincipalStr.'</b></td>
					</tr>

				  </table>';
		return $html;
		
	}
	/***************** End Part 7 ******************/

	
	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false) 
	{
				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 

		
		$subtitle_html = '';
				
		foreach((array)$subTitleArr as $key=>$_title)
		{
			$text_alignStr='center';
			
			$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'">';
			$subtitle_html .=$_title;
			$subtitle_html .='</td>';
			
		}	
					
		$total_Record=10;
		$rowHight = 30;


		$text_alignStr='center';
		$content_html='';	
		$rowMax_count = 0;
		
		if(count($DataArr)>0)
		{
			$rowMax_count = count($DataArr);
		}	
		
		$count_col =count($colWidthArr);

		if($PartNum=='3')
		{
			$total_Record=20;
		}
		else if($PartNum=='4')
		{
			$total_Record=15;
		}
		else if($PartNum=='5')
		{
			$total_Record=25;
		}
		else if($PartNum=='6')
		{
			$total_Record=20;
		}
		
		if($rowMax_count>0)
		{
			for ($i=0; $i<$rowMax_count; $i++)
			{		
				$content_html .= '<tr>';	
					
				$j=0;		 
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{
					if($_titleVal=='')
					{
						$_titleVal =  $this->EmptySymbol;
					}
					
					$text_alignStr = 'center';
					if($j==1)
					{
						$text_alignStr = 'left';
					}
										
					$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'">'.$_titleVal.'</td>';								
					$j++;
				}				
				$content_html .= '</tr>';		
			}
		}
		else
		{
			$content_html = '<tr>';
			$content_html .= ' <td colspan="'.$count_col.'" style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'">No Record Found.</td>';
			$content_html .= '</tr>';
		}

		
		$colNumber = count($colWidthArr);
			
		/****************** Main Table ******************************/
		
		$html = '';
				  
		$html .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
		$html .= '	<tr> 
						<td colspan="'.$colNumber.'" style=" font-size:15px;text-align:left;vertical-align:text-top;">
						'.$mainTitle.'
						</td>
					</tr>';
		$html .= '<tr>'.$subtitle_html.'</tr>';
		
		$html .= $content_html;
	
		if(count($DataArr)<$total_Record && count($DataArr)!=0)
		{
			$html .= '<tr>';
			
				for($a=0,$a_MAX=$colNumber;$a<$a_MAX;$a++)
				{
					$text_alignStr = 'center';
					if($a==1)
					{
						$text_alignStr = 'left';
					}
					
					$html .= '<td style=" font-size:15px;text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderTop.$_borderRight.$_borderBottom.'">';
					$html .= $this->EmptySymbol;
					$html .= '</td>';
				}
				
			$html .= '</tr>';
		}
	
		if($PartNum=='5')
		{
			$longSpace='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$thisStr =''; 
			$thisStr .='*OLE Components-<br/><br/>';
			$thisStr .='AD - Aesthetic Developement '.$longSpace;
			$thisStr .='CRE - Career Related Experience'.$longSpace;
			$thisStr .='CS - Community Services<br/><br/>';
			$thisStr .='MCE - Moral and Civic Education'.$longSpace;
			$thisStr .='PD - Physical Development';			
			
			$html .=  '<tr> 
						 <td colspan="'.$colNumber.'" style="text-align:left;vertical-align:text-top; font-size:14px;">
						 '.$thisStr.'
						 </td>
				  	   </tr>';			  
		} 
		
		$html .='</table>';

			
		return $html;
	}
	
	

	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false,$thisType='')
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$OLE_Cate = $this->Get_eClassDB_Table_Name('OLE_CATEGORY'); 
		
		$cond_OLEcate ="";
		$cond_limit = "";
		if($thisType == 'award')
		{	
			$cond_OLEcate =" And ole_cate.RecordID = '15'"; //Major awards and achievements inside school && House
			$cond_limit = "limit 20";
		} 
		else if($thisType == 'schoolPost')
		{	
			$cond_OLEcate =" And ole_cate.RecordID = '10'"; //House / Society Activity
			$cond_limit = "limit 15";
		}
		else if($thisType == 'schoolActivity')
		{
			$cond_OLEcate =" And ole_cate.RecordID not in('15','10')"; //Major awards and achievements inside school && House / Society Activity  
			$cond_limit = "limit 25";
		}
		else if($thisType == 'externalActivity')
		{
			$cond_OLEcate =""; 
			$cond_limit = "limit 20";
		}
		
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='') 
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
		
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = " And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
				Inner join
						$OLE_Cate as ole_cate
				On
						ole_prog.Category = ole_cate.RecordID
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
						$cond_OLEcate
				order by 
						y.Sequence asc,	
						ole_prog.Title asc		
				$cond_limit
				";
		$roleData = $this->objDB->returnResultSet($sql);
//hdebug_r($sql);
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];			
				
			if($thisType == 'award')
			{		
				$thisDataArr = array();	

				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['Details'] =$roleData[$i]['Details'];
					
				$returnData[] = $thisDataArr;
			} 
			else if($thisType == 'schoolPost')
			{	
				$thisDataArr = array();	

				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['Role'] =$roleData[$i]['Role'];
					
				$returnData[] = $thisDataArr;
			}
			else if($thisType == 'schoolActivity')
			{
 				$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
			
				for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
				{
					$_str = $thisOleComponentArr[$a];
					$_str = substr($_str, 0, strlen($_str)-1); 
					$_str = substr($_str, 1);
					
					$thisOleComponentArr[$a] = $_str;
				}
				
				$thisOLEstr = implode(",",(array)$thisOleComponentArr);
	
				$thisDataArr = array();	
	
				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['OLEComp'] =$thisOLEstr;	
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
						
				$returnData[] = $thisDataArr;
			}
			else if($thisType == 'externalActivity')
			{
				$thisDataArr = array();	
	
				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];				
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
						
				$returnData[] = $thisDataArr;
			}
			
		}
		return $returnData;
	}
	
	public function getAddress_HTML() 
	{
		$shortSpaceStr = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		
		$Address_html = '25 Ngan Shing Street, Shatin, Hong Kong 香港沙田銀城街25號<br/>';	
		$Address_html .='Telephone: 2786 1990'.$shortSpaceStr.'Fax: 2786 9617'.$shortSpaceStr.'http://www.ltfc.edu.hk';
		
		$header_font_size = '13';
	
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">
					<tr>
						<td style="font-size:'.$header_font_size.'px;"><font face="Arial" color="'.$this->colorBlue.'">'.$Address_html.'</font></td>
				   </tr>';
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	public function getNameNumber()
	{
		$fontSize = 'font-size:15px';
		$html = '';
		$html .= '<table width="80%" cellpadding="2" border="0px" align="center" style="border-collapse: collapse;">
					<col width="10%" />
					<col width="25%" />
					<col width="12%" />
					<col width="18%" />

					<tr>					
							<td style="'.$fontSize.'">Name: </td>
							<td style="'.$fontSize.'">'.$this->StudentName.'</td>
							<td style="'.$fontSize.'">Student No.: </td>
							<td style="'.$fontSize.'">'.$this->StudentNumber.'</td>
					</tr>
					';

		$html .= '</table>';
	
		return $html;
		
	}
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
			$firstYear = substr($returnArray[0], 0, 4);
			$lastYear = substr($returnArray[$lastNo],-4);
			
			$returnValue = $firstYear."-".$lastYear;
			
			return $returnValue;	
		}
		
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	public function getPageNumberTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:right">'.$Content.'</td>
					 </tr>
				  </table>';
		return $html;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center">'.$Content.'</td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>