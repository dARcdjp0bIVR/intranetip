<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	private $All_AcademicYearID;
	private $OLEcomponentArr;
	
	private $HeaderTitleColor;
	
	private $StdNameCh;
	private $StdNameEn;
	
	private $PageNo;
	private $TotalPage;

	private $NoOfRecord;
	
	private $StdRegNo;
	
	private $YearClassID;
	
	private $AcademicYearStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '--';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->OLEcomponentArr = array();
		$this->OLEcomponentArr[] = '[MCE]';
		$this->OLEcomponentArr[] = '[CS]';
		$this->OLEcomponentArr[] = '[PD]';
		$this->OLEcomponentArr[] = '[AD]';
		$this->OLEcomponentArr[] = '[CE]';
		
		$this->TableTitleBgColor = '#808080';
		$this->HeaderTitleColor = '#FFFFFF';
		$this->SubTitleColor = '#D0D0D0';
		$this->RemarkColor = '#E8E8E8';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->NoOfRecordInt = '10';
		$this->NoOfRecordExt = '10';
		
		$this->RemarkFont = '11px';
		
		$this->PageNo = 1;
		$this->TotalPage = 1;
		
		$this->SchoolTitleEn = 'Pentecostal Lam Hon Kwong School';
		$this->SchoolTitleCh = '五旬節林漢光中學';
		
		$this->StdNameCh = '';
		$this->StdNameEn = '';
		
		$this->StdRegNo = '';
	}
	
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	public function setAll_AcademicYearID($All_AcademicYearID)
	{
		$this->All_AcademicYearID = $All_AcademicYearID;
	}
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setNoOfRecordInt($ParNoOfRecord){
		$this->NoOfRecordInt  = $ParNoOfRecord;
	}
	public function setNoOfRecordExt($ParNoOfRecord){
		$this->NoOfRecordExt  = $ParNoOfRecord;
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	public function setPageNumber($ParPageNo){
		$this->PageNo++;
	}
	public function getPageNumber(){
		return $this->PageNo;
	}

	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '13';
		$spaceStr = '&nbsp;&nbsp;&nbsp;&nbsp;';
	
		$imgFile = get_website().'/includes/portfolio25/customize/plhk/SchoolHeader.jpg';
		$schoolLogoImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:700px;">' : '&nbsp;';

//		$IssueDate = $this->issuedate;
//		
//		if (date($IssueDate)==0) {
//			$IssueDate = $this->EmptySymbol;
//		} else {
//			$IssueDate = date('d/m/Y',strtotime($IssueDate));
//		}
//		
	
		$x = '';
		$x .= '<table width="90%" cellpadding="1" border="0px" align="center" style="border-collapse: collapse;">
				  <tr>
						<td align="left">'.$schoolLogoImage.'</td>
				  </tr>
			   </table>';
//$x = $schoolLogoImage;
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();			
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	public function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						ClassNumber,
						ClassName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnResultSet($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{		
		$fontSize = 'font-size:13px;';
		
		$thisPartTitle = '學生資料<br/>Student Particulars';
		
		$SchoolNumStr ='00000';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		
		$DOB = $result[0]["DateOfBirth"];
		if (date($DOB)==0) {
			$DOB = $this->EmptySymbol;
		} else {
			$DOB = date('d/m/Y',strtotime($DOB));
		}
		
		$EngName = strtoupper($result[0]["EnglishName"]);
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;		
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];	
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;	
		
		$RegistrationNo = $result[0]["STRN"];
		$RegistrationNo = ($RegistrationNo=='')? $this->EmptySymbol:$RegistrationNo;
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;

		$this->StdNameCh = $ChiName;
		$this->StdNameEn = $EngName;
		
		$this->StdRegNo = $RegistrationNo;

		$html = '';
		$html .= '<table width="90%" cellpadding="5" border="0px" align="center" style="border-collapse: collapse;">
					<col width="25%" />
					<col width="45%" />
					<col width="15%" />
					<col width="15%" />

					<tr>
							<td bgcolor="'.$this->TableTitleBgColor.'" style="font-size:15px;'.$_paddingLeft.$_borderLeft.$_borderRight.$_borderTop.$_borderBottom.'" colspan="4"><font face="Arial" color="'.$this->HeaderTitleColor.'"><b>'.$thisPartTitle.'</b></font></td>
					</tr>

					<tr>
							<td style="'.$_borderLeft.$fontSize.$_paddingLeft.'" ><font face="Arial">Student Name:</font></td>
							<td style="'.$fontSize.'" ><font face="Arial">'.$EngName.' ('.$ChiName.')'.'</font></td>					
							<td style="'.$fontSize.'"><font face="Arial">HKID:</font></td>
							<td style="'.$_borderRight.'"><font face="Arial">'.$HKID.'</font></td>
					</tr>

					<tr>
							<td style="'.$_borderLeft.$fontSize.$_paddingLeft.$_borderBottom.'" ><font face="Arial">Class (Class no.):</font></td>
							<td style="'.$fontSize.$_borderBottom.'" ><font face="Arial">'.$ClassName.' ('.$ClassNumber.')'.'</font></td>					
							<td style="'.$fontSize.$_borderBottom.'"><font face="Arial">Gender:</font></td>
							<td style="'.$_borderRight.$_borderBottom.'"><font face="Arial">'.$Gender.'</font></td>
					</tr>

					';

		$html .= '</table>';
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Academic Performance******************/	
	public function getPart3_HTML($SubjectArr,$AcademicResultInfoArr,$FullMarkArr){
		
		$Part3_Title = '高中學業成績<br/>Academic Performance at Senior Secondary Level';
//		$Part3_Title = '校內高中學科成績<br/>Academic Performance in School at Senior Secondary Level';
		$Part3_Remark = '&nbsp;';

		$html = '';
	
		$html .= $this ->getAcademicResultDisplay($Part3_Title,$SubjectArr,$AcademicResultInfoArr,$FullMarkArr,$hasPageBreak=false);
		
		return $html;	
	}		

	public function getAcademicResultDisplay($mainTitle,$SubjectArr,$AcademicResultInfoArr,$FullMarkArr,$hasPageBreak=false)
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';			
  		

		
		$Page_break='page-break-after:always;';	
		if($hasPageBreak==false)
		{
			$Page_break = '';
		}
		
		$table_style = 'style="font-size:12px;border-collapse: collapse; border:1px solid #000000;'.$Page_break.'"';
		
		$LastYearAcademicID = '';
		$LastYearClassID = '';
		
	
		$rowMax_count = count($SubjectArr);

		$rowHight = 5;
		$text_alignStr = 'center';

		$noRecordFound_html = '';

		$AcademicID_Array = $this->AcademicYearStr;

		//$countCol = 3+2;
		
		$ClassNameArr = $this->getAcademicClassName();		
		
		$NewSubjectSequenceArr = array();
		$countMAX_NewSubjectSequenceArr = 0;
	
		$YearClassArray = $this->getStudentAcademic_YearClassNameArr(true);
		
		$AcademicYearArray = $this->getAcademicYearArr($this->All_AcademicYearID);
		$YearClassArray = $this->getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,'desc');
		
		// only show latest form 4 - #Z96176
		$existFormArr = array();
		$newYearClassArray = array();
		foreach((array)$YearClassArray  as $_classInfo){
			$_formNo = substr($_classInfo['ClassName'],0,1);
			if($_formNo == 4){
				if(!in_array($_formNo, $existFormArr)){
					$newYearClassArray[] = $_classInfo;
					$existFormArr[] = $_formNo;
				}
			}
			else{
				$newYearClassArray[] = $_classInfo;
			}
		}
		$YearClassArray = $newYearClassArray;
		
		$countCol = count($YearClassArray) + 1;
		
		for($a=0,$a_MAX=count($SubjectArr);$a<$a_MAX;$a++)
		{
			$thisSubjectNameEn = $SubjectArr[$a]["SubjectDescEN"];
			$thisSubjectNameCh = $SubjectArr[$a]["SubjectDescB5"];
			
			$thisSubjectID = $SubjectArr[$a]["SubjectID"];
			
			$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $thisSubjectNameEn;
			$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameCh'] = $thisSubjectNameCh;
					
			$SubjectAcademicResultArr = $AcademicResultInfoArr[$thisSubjectID];

			$hasStudy = false;
		
			$count_AcademicID_Array = 0; 

			for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
			{			
				
				$thisYear = $YearClassArray[$i]['AcademicYear'];
				$thisClassName = $YearClassArray[$i]['ClassName'];
				
				$_academicID =array_search($thisYear,$AcademicYearArray);
			
				$thisFormNo = (int)$thisClassName;
				preg_match('/\d+/', $thisClassName, $number);
				
				if(count($number)>0 && is_array($number))
				{
					$thisFormNo = $number[0];		
				}
				
				$thisScore = $SubjectAcademicResultArr[$_academicID]['Score'];
			
				$_yearID = $ClassNameArr[$_academicID]['YearID'];
				  
				if($count_AcademicID_Array==0)
				{
					$LastYearAcademicID = $_academicID;
					$LastYearClassID = $_yearID;
				}
				
				if($thisScore!='')
				{
					$hasStudy = true;
				}
				$thisScore = ($thisScore=='')? $this->EmptySymbol:$thisScore;
				
				$NewSubjectSequenceArr[$thisSubjectID]['Score'][$_academicID] = $thisScore;
				
				$_fullMark = $FullMarkArr[$_academicID][$_yearID][$thisSubjectID]['FullMarkInt'];
				$_fullMark = ($_fullMark=='')?$this->EmptySymbol:$_fullMark;

				$NewSubjectSequenceArr[$thisSubjectID]['fullMark'][$_academicID] = $_fullMark;
				
				$count_AcademicID_Array++;
			}
	//		$_fullMark = $FullMarkArr[$LastYearAcademicID][$LastYearClassID][$thisSubjectID]['FullMarkGrade'];

			
			
			$NewSubjectSequenceArr[$thisSubjectID]['hasStudy'] = $hasStudy;
			
			if($hasStudy==true)
			{
				$countMAX_NewSubjectSequenceArr++;
			}

		}	
		
		$subTitle_html = '';
		
		
		
		$subTitle_html = '';
		$subTitle_html .='<tr>';
		$subTitle_html .= ' <td style="height:'.$rowHight.'px;text-align:center;vertical-align: center;'.$_borderLeft.'"><font face="Arial"><b>Subject</b></font></td>';
//		$subTitle_html .= ' <td style="height:'.$rowHight.'px;text-align:center;vertical-align: center;"><font face="Arial"><b>滿分<br/>Full Mark</b></font></td>';
		
		$count_AcademicID_Array = 0;
		
		
		for ($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{							
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			
			$_academicID =array_search($thisYear,$AcademicYearArray);
			
			$thisFormNo = (int)$thisClassName;
			
			preg_match('/\d+/', $thisClassName, $number);
				
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = $number[0];		
			}
			
			$thisBorder='';
			
			if($i==($i_MAX-1))
			{
				$thisBorder = $_borderRight;
			}
			
			$_className = $ClassNameArr[$_academicID]['ClassName'];
			$_yearID = $ClassNameArr[$_academicID]['YearID'];
						

			$thisClassName = 'S'.$thisFormNo;
		
			$subTitle_html .= ' <td style="height:'.$rowHight.'px;text-align:center;vertical-align: top;'.$thisBorder.'"><font face="Arial"><b>'.$thisClassName.'<br/>'.$thisYear.'</b></font></td>';
		
		
			$count_AcademicID_Array++;
		}
		$subTitle_html .='</tr>';
		
		
		$content_html = '';
		$count_SubjectArray = 0;		
		foreach((array)$NewSubjectSequenceArr as $thisSubjectID=>$thisSubjectResultInfoArr)
		{		
			if($NewSubjectSequenceArr[$thisSubjectID]['hasStudy']==false)
			{
				continue;
			}
			
			$thisBorderRight='';
			$thisBorderBottom='';
			
			if($count_SubjectArray==($countMAX_NewSubjectSequenceArr-1))
			{
				$thisBorderRight = $_borderRight;
				$thisBorderBottom = $_borderBottom;
			}
			
			$content_html .='<tr>';
			$thisSubjectNameEn = $thisSubjectResultInfoArr['SubjectNameEn'];
			$thisSubjectNameCh = $thisSubjectResultInfoArr['SubjectNameCh'];
			
			$thisFullMark = $thisSubjectResultInfoArr['fullMark'];
//debug_r($thisFullMark);			
			$content_html .= ' <td style="height:'.$rowHight.'px;text-align:left;vertical-align: top;'.$_borderLeft.$thisBorderBottom.'"><font face="Arial">'.$thisSubjectNameEn.'</font></td>';		
	//		$content_html .= ' <td style="height:'.$rowHight.'px;text-align:left;vertical-align: top;'.$thisBorderBottom.'"><font face="Arial">'.$thisFullMark.'</font></td>';
			
			$SubjectAcademicResultArr = $AcademicResultInfoArr[$thisSubjectID];

			$count_AcademicID_Array = 0;
	
			for ($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
			{			
					
				$thisYear = $YearClassArray[$i]['AcademicYear'];
				$thisClassName = $YearClassArray[$i]['ClassName'];
				
				$_academicID =array_search($thisYear,$AcademicYearArray);
		
				$thisFormNo = (int)$thisClassName;
				
				preg_match('/\d+/', $thisClassName, $number);
				
				if(count($number)>0 && is_array($number))
				{
					$thisFormNo = $number[0];		
				}
				
				$thisBorderRight = '';
				if($i==($i_MAX-1))
				{
					$thisBorderRight = $_borderRight;
				}
			
						
				$thisScore = $thisSubjectResultInfoArr['Score'][$_academicID];
				$thisFullMark = $thisSubjectResultInfoArr['fullMark'][$_academicID];
				// fullmark changed to decimal
				$fullmarkArr = explode('.',$thisFullMark);
				if($fullmarkArr[1] === '0'){
					// if demical is 0 ignore it.
					$thisFullMark = (int)$thisFullMark;
				}
				$content_html .= ' <td style="height:'.$rowHight.'px;text-align:center;vertical-align: top;'.$thisBorderRight.$thisBorderBottom.'"><font face="Arial">'.$thisScore.'/'.$thisFullMark.'</font></td>';
			
				$count_AcademicID_Array++;
			}
			$count_SubjectArray++;			
			$content_html .='</tr>';
		}

		if($content_html=='')
		{				
			$content_html .='<tr>';
			$content_html .= ' <td colspan="'.$countCol.'" style="height:'.$rowHight.'px;text-align:center;vertical-align: top;'.$_borderTop.$_borderLeft.$_borderRight.$_borderBottom.'"><font face="Arial">No Record Found.</font></td>';
			$content_html .='</tr>';
		}
		
		/****************** Main Table ******************************/
		
		$html = '';

		$html .= '<table width="90%" height="" cellpadding="4" border="0px" align="center" '.$table_style.'>';

		  $html .= '<tr> 
						<td colspan="'.$countCol.'"  bgcolor="'.$this->TableTitleBgColor.'" style=" font-size:15px;text-align:left;vertical-align:text-top;'.$_borderTop.$_borderLeft.$_borderRight.$_borderBottom.'"><font face="Arial" color="'.$this->HeaderTitleColor.'"><b>
						'.$mainTitle.'
						</b></font></td>
				  	</tr>';
		$html .=$subTitle_html;
		$html .= $content_html;
		
		$html .='</table>';

			
		return $html;		
	}

	private function getAcademicClassName()
	{
		$_studentID = $this->uid;
		
		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$SelectStr= 'h.AcademicYearID as AcademicYearID,
					 h.ClassName as ClassName,
					 y.YearID as YearID';
		
		$academicYearIDArr = $this->academicYearIDArr;
		$cond_academicYear = "And h.AcademicYearID In ('".implode("','",(array)$academicYearIDArr)."')";		
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				inner join
						'.$YEAR_CLASS.' as y
				on 
					h.YearClassID = y.YearClassID
				where 
						h.UserID = "'.$_studentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear asc';
						
		$roleData = $this->objDB->returnResultSet($sql);
		$returnArr = array();
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisAcademicYearID = $roleData[$i]['AcademicYearID'];
			$thisClassName =  $roleData[$i]['ClassName'];
			$thisYearID =  $roleData[$i]['YearID'];
			$returnArr[$thisAcademicYearID] = array('ClassName'=>$thisClassName,'YearID'=>$thisYearID);
		}
//debug_r($sql);
		return $returnArr;
		
	}
		
	/***************** End Part 3 ******************/
	
	/***************** Part 4 ******************/	
	public function getPart4a_HTML(){
		
		$_studentID = $this->uid;
		
		$Part4_Title = '其他學習經歷<br/>Other Learning Experiences (OLE)';
		
		$Part4_Remark = 'Apart from core and elective subjects, the learning programmes that the student participated in during the ';
		$Part4_Remark .='senior secondary years include Moral and Civic Education, Aesthetic Development, Physical Development, '; 
		$Part4_Remark .='Community Service and Career-related Experiences. \'Other Learning Experiences\' could be gained through programmes ';
		$Part4_Remark .='organized by the school or co-organized by the school with outside organizations.';
		
		
		$html = '';
		$html .= $this->getOLEcompTable_html($Part4_Title,$Part4_Remark);
		$html .= $this->getEmptyTable('3%');
		$html .= $this->getSchoolChopTable_html();
		
		return $html;
		
	}
	public function getPart4b_HTML()
	{
		
		$_studentID = $this->uid;
		
		$titleArray = array();	
		$titleArray[] = '';
		$titleArray[] = 'Programmes (with description)';
		$titleArray[] = 'Role';
		$titleArray[] = 'Hours';
		$titleArray[] = 'Components of<br/>OLE';
		$titleArray[] = 'School Year';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="9%" />';
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="5%" />';
		$colWidthArr[] = '<col width="17%" />';
		$colWidthArr[] = '<col width="17%" />';
		$colWidthArr[] = '<col width="17%" />';

		$Part4_BottomRemark = '<i>* Number of hours are included in the "Internal Services"</i>';

		$academicYearIDArr = $this->academicYearIDArr;
		$OLEInfoArrINT = $this->getOLE_Info($_studentID,$academicYearIDArr,$IntExt='INT',$Selected=true);

		$showRemark = $OLEInfoArrINT['showRemark'];
		$dataArr = $OLEInfoArrINT['Data'];
		
		if(count($dataArr)==0)
		{
			$OLEInfoArrINT = $this->getOLE_Info($_studentID,$academicYearIDArr,$IntExt='INT',$Selected=false);
			$showRemark = $OLEInfoArrINT['showRemark'];
			$dataArr = $OLEInfoArrINT['Data'];
		}		
		
		$html = '';
		$html .= $this->getEmptyTable('3%');
		$html .= $this->getPartContentDisplay_PageRowLimit('4','',$titleArray,$colWidthArr,$dataArr,$hasPageBreak=true,'',$Part4_BottomRemark,$showRemark);
		return $html;
	}
	
	function getOLEcompTable_html($mainTitle,$ParRemark)
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$OLEcompConfigArr = $this->OLEcomponentArr;
		$_studentID = $this->uid;
		
		$YearClassArray = $this->getStudentAcademic_YearClassNameArr(true);
		$OLEcomponentArr = $this->getOLEcomponentName();
		
		$OLECompInfINT = $this->getOLE_Component($_studentID,$this->academicYearIDArr,$IntExt='INT',$Selected=false);
		
		$TotalCountArr = array();
		
		$colNumber = count($OLEcompConfigArr)+1;
		
		$html = '';
		$html .= '<table width="90%" cellpadding="5" border="0px" align="center" style="border-collapse: collapse;">';
		$html .= '<tr> 
					<td colspan="'.$colNumber.'"  bgcolor="'.$this->TableTitleBgColor.'" style=" font-size:15px;text-align:left;vertical-align:text-top;'.$_borderTop.$_borderLeft.$_borderRight.'"><font color="'.$this->HeaderTitleColor.'"><b>
					'.$mainTitle.'
					</b></font></td>
			  	</tr>';
		$html .= '<tr> 
					<td colspan="'.$colNumber.'"  bgcolor="'.$this->RemarkColor.'" style="font-size:'.$this->RemarkFont.';text-align:left;vertical-align:text-top;'.$_borderLeft.$_borderRight.'">
					'.$ParRemark.'
					</td>
			  	</tr>';
		$html .= '<tr> 
					<td bgcolor="'.$this->SubTitleColor.'" style="font-size:12px;text-align:center;vertical-align:text-top;'.$_borderLeft.'"><b>School Year</b></td>
					<td colspan="'.count($OLEcompConfigArr).'"  bgcolor="'.$this->SubTitleColor.'" style="font-size:12px;text-align:center;vertical-align:text-top;'.$_borderRight.'">
					<b>Component of OLE (Hours)</b>
					</td>
			  	</tr>';
		$html .= '<tr>';
			
				$html .= '<td style="text-align:center;'.$_borderLeft.'"  bgcolor="'.$this->SubTitleColor.'">&nbsp;</td>';
				for($a=0,$a_MAX=count($OLEcompConfigArr);$a<$a_MAX;$a++)
				{
					$thisOleComponentID = $OLEcompConfigArr[$a];		
					$OLEcompStr = $OLEcomponentArr[$thisOleComponentID].'<br/>'. $thisOleComponentID;
					
					$thisBorder= '';
					if($a==$a_MAX-1)
					{
						$thisBorder = $_borderRight;
					}
					
					$html .= '<td style="font-size:12px;text-align:center;'.$thisBorder.'"  bgcolor="'.$this->SubTitleColor.'">'.$OLEcompStr.'</td>';
				}
		$html .= '</tr>';
		
		$academicYearIDArr = $this->academicYearIDArr;
		$academicYearArr = $this-> getAcademicYearArr($academicYearIDArr);
	
		for ($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{							
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
		
			if(in_array($thisYear,$academicYearArr))
			{
				$html .= '<tr>';
			
				$html .= '<td style="font-size:12px;text-align:center;'.$_borderLeft.'">'.$thisYear.'</td>';
				
				for($a=0,$a_MAX=count($OLEcompConfigArr);$a<$a_MAX;$a++)
				{
					$thisOleComponentID = $OLEcompConfigArr[$a];		
					$thisCount = $OLECompInfINT[$thisYear][$thisOleComponentID];				
					
					$thisCount = ($thisCount=='')? 0:$thisCount;

					$TotalCountArr[$thisOleComponentID] = ($TotalCountArr[$thisOleComponentID]=='')? 0:$TotalCountArr[$thisOleComponentID];
					$TotalCountArr[$thisOleComponentID]+=$thisCount;

					$thisBorder= '';
					if($a==$a_MAX-1)
					{
						$thisBorder = $_borderRight;
					}

					$html .= '<td style="font-size:12px;text-align:center;'.$thisBorder.'">'.$thisCount.'</td>';
				}
				$html .= '</tr>';
			}		
		}
		
		$html .= '<tr>';
			
			$html .= '<td style="font-size:12px;text-align:center;'.$_borderLeft.$_borderBottom.'">Total</td>';
			
			for($a=0,$a_MAX=count($OLEcompConfigArr);$a<$a_MAX;$a++)
			{
				$thisOleComponentID = $OLEcompConfigArr[$a];		
				$thisCount = $TotalCountArr[$thisOleComponentID];		
				
				$thisBorder= '';
				if($a==$a_MAX-1)
				{
					$thisBorder = $_borderRight;
				}		

				$html .= '<td style="font-size:12px;text-align:center;'.$_borderBottom.$thisBorder.'">'.$thisCount.'</td>';
			}
		$html .= '</tr>';
		
		$html .= '</table>';
		
		return $html;
	}
	public function getOLE_Component($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$OLE_Cate = $this->Get_eClassDB_Table_Name('OLE_CATEGORY'); 
		
		
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}	
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = " And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						ole_std.Hours as Hours,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
				Inner join
						$OLE_Cate as ole_cate
				On
						ole_prog.Category = ole_cate.RecordID
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
				order by 
						y.Sequence desc,	
						ole_prog.Title asc		
				
				";
		$roleData = $this->objDB->returnResultSet($sql);
//$this->OLEcomponentArr 

//debug_r($sql);		
		$ReturnArr = array();

		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$_IntExt = $roleData[$i]['IntExt'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
			$thisHour = $roleData[$i]['Hours'];
			
			$thisHour = ($thisHour=='')? 0:$thisHour;
	
			$thisOleCompShortArr = explode(',',$roleData[$i]['OleComponent']);

			for($k=0,$k_max=count($thisOleCompShortArr);$k<$k_max;$k++)
			{
				$thisOLEcompConfigStr = $thisOleCompShortArr[$k];
				
				if($ReturnArr[$thisAcademicYear][$thisOLEcompConfigStr]=='')
				{
					$ReturnArr[$thisAcademicYear][$thisOLEcompConfigStr]== 0;
				}
				$ReturnArr[$thisAcademicYear][$thisOLEcompConfigStr]+=$thisHour;
				
				
			}				
			
		}

		return $ReturnArr;
	}
	
	function getOLEComp_HeaderArr()
	{
		$OLEcompConfigArr = $this->OLEcomponentArr;
		$OLEcomponentArr = $this->getOLEcomponentName();
		
		$OLEcompLongArr = array();	
		for($a=0,$a_MAX=count($OLEcompConfigArr);$a<$a_MAX;$a++)
		{
			$thisOleComponentID = $OLEcompConfigArr[$a];		
			$OLEcompLongArr[$thisOleComponentID] = $OLEcomponentArr[$thisOleComponentID].'<br/>'. $thisOleComponentID;
		}
		
		return $OLEcompLongArr;
	}

	function getSchoolChopTable_html()
	{
		$DateOfIssueStr = 'Date of Issue';
		$TotalNumberOfPagesStr = 'Total Number of Pages';
		
		$SchoolChopStr = 'INVALID UNLESS IMPRESSED WITH THE SCHOOL CHOP';

		
		$StdNameStr = $this->StdNameEn;
		
		$fontSize = '13px';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="5" border="0px" align="center" style="border-collapse: collapse;">';
		$html .= '<tr> 
					<td style=" font-size:'.$fontSize.';text-align:left;vertical-align:text-top;"><b>
					'.$DateOfIssueStr.': '.$this->issuedate.'
					</b></td>
			  	  </tr>
				  <tr> 
					<td style=" font-size:'.$fontSize.';text-align:left;vertical-align:text-top;"><b>
					'.$TotalNumberOfPagesStr.': <!--TotalNumber-->
					</b></td>
			  	  </tr>
				  <tr> 
					<td style=" font-size:'.$fontSize.';text-align:left;vertical-align:text-top;">&nbsp;</td>
			  	  </tr>
				  <tr> 
					<td style=" font-size:'.$fontSize.';text-align:left;vertical-align:text-top;"><b>
					'.$SchoolChopStr.'
					</b></td>
			  	  </tr>
				  ';
		$html .='</table>';
		
		return $html;
	}
	

	/***************** End Part 4 ******************/
	

	/***************** Part 5 : Performance / Awards and Key Participation Outside School ******************/
	public function getPart5_HTML()
	{
		
		$_studentID = $this->uid;
		
		$Part5_Title = '校外的表現 / 獎項<br/>Performance / Awards and Key Participation Outside School';
		$Part5_Remark = 'For learning programmes not organized by the school during the senior secondary years, ';
		$Part5_Remark .='students could provide the information to the school. It is not necessary for the school '; 
		$Part5_Remark .='to validate the information below. Students should hold full responsibility for providing ';
		$Part5_Remark .='evidence to relevant people when requested.';
		
		$Part5_BottomRemark = '<i># Evidence of awards / certifications / achievements listed is available for submission when required.</i>';
		
		$titleArray = array();
		$titleArray[] = '';
		$titleArray[] = 'Programmes (with description)';
		$titleArray[] = 'Role';
		$titleArray[] = 'Organization';
		$titleArray[] = 'Awards /<br>Certifications /<br>Achievements#<br>(if any)';
		$titleArray[] = 'School Year';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="5%" />';
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="10%" />';
		$colWidthArr[] = '<col width="17%" />';
		$colWidthArr[] = '<col width="18%" />';
		$colWidthArr[] = '<col width="17%" />';
		
		$academicYearIDArr = $this->academicYearIDArr;
		$OLEInfoArrEXT = $this->getOLE_Info($_studentID,$academicYearIDArr,$IntExt='EXT',$Selected=true);

		if(count($OLEInfoArrEXT)==0)
		{
			$OLEInfoArrEXT = $this->getOLE_Info($_studentID,$academicYearIDArr,$IntExt='EXT',$Selected=false);
		}	

		$html = '';
		$html .= $this->getPartContentDisplay_PageRowLimit('5',$Part5_Title,$titleArray,$colWidthArr,$OLEInfoArrEXT,$hasPageBreak=true,$Part5_Remark,$Part5_BottomRemark);
		
		return $html;
		
	}
	/***************** End Part 5 ******************/
	
	/***************** Part 6 ******************/
	
	public function getPart6_HTML(){
		
		$Part6_Title = '學生自述<br/>Self Account';
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part6_Title) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}
		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:12px;border:1px solid #000000;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			
			$Detail = strip_tags($Detail,'<p><br>');
			
			$thisReplaceArr = array();
			$thisReplaceArr[] = '<p class="MsoNormal" style="margin: 0cm 0cm 10pt">&nbsp;</p>';
			$thisReplaceArr[] = '<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph">&nbsp;</p>';
			$thisReplaceArr[] = '<p style="margin-bottom: 0cm">&nbsp;</p>';
			
			$Detail = str_replace($thisReplaceArr,'',$Detail);
//			$Detail .= '<center>===========================================</center>';
		}
		else
		{
			$Detail = '<p align=CENTER>No Record Found.</p>';
		}	
		
		$html = '<table width="100%" height="990px">';
		$html .= '<tr><td style="vertical-align:top">';
		$html .= '<table width="90%" height="800px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr> 
						<td bgcolor="'.$this->TableTitleBgColor.'" style="height:5px;font-size:15px;text-align:left;vertical-align:text-top;'.$_borderBottom.'"><font face="Arial" color="'.$this->HeaderTitleColor.'"><b>
						'.$MainTitle.'
						</font></b></td>
				  	</tr>
					<tr>				 							
						<td style="vertical-align: top;"><font face="Arial">'.$Detail.'</font></td>						 														
					</tr>
				  </table>';
	
		$html .= '<table width="90%" height="10px"  cellpadding="7" border="0px" align="center">
					<tr>				 							
						<td style="vertical-align: top;text-align:center;"><font face="Arial">-- End of Report --</font></td>						 														
					</tr>
				  </table>';
		$html .= '</td></tr>';
		
		$html .= '</table>';	
		$html .= $this->getFooter_HTML();	
			
		return $html; 
	}
	
	
	/***************** End Part 6 ******************/	
		
	private function getPartContentDisplay_PageRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false,$ParRemark='',$ParBottomRemark='',$ParShowRemark=true) 
	{
		// it indicate the number of rows perpage. If you want to set 2 row perpage, change it to 2;
		if($PartNum=='4')
		{
			$RowPerPage =$this->NoOfRecordInt;
		}
		else if($PartNum=='5')
		{
			$RowPerPage =$this->NoOfRecordExt;
		}
	
		$rowMax_count = count($DataArr);
		
		$RowHeight = 'height="35px"';

	//	$ParPageNo = $this->PageNo;

		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		//	$table_style = 'style="font-size:10px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
		$table_style = 'style="font-size:10px;border-collapse: collapse;'.$Page_break.'"';
	
		$subtitle_html = '';
		$NumberOfCol = count($colWidthArr);
				
		$count_subTitleArr=0;
		foreach((array)$subTitleArr as $key=>$_title)
		{
			$text_alignStr='center';
			
			$thisBorder='';
			if($count_subTitleArr==0)
			{
				$thisBorder = $_borderLeft;
			}
			else if($count_subTitleArr==(count($subTitleArr)-1))
			{
				$thisBorder = $_borderRight;
			}
//vertical-align:text-top;

			$subtitle_html .='<td bgcolor="'.$this->SubTitleColor.'" style="font-size:12px;text-align:'.$text_alignStr.';'.$_borderBottom.$_borderRight.$thisBorder.'"><b>';
			$subtitle_html .=$_title;
			$subtitle_html .='</b></td>';
			
			$count_subTitleArr++;		
		}
		
		if($ParRemark!='')
		{
			$remark_html = '<tr> 
								<td colspan="'.$NumberOfCol.'"  bgcolor="'.$this->RemarkColor.'" style="font-size:'.$this->RemarkFont.';text-align:left;vertical-align:text-top;'.$_borderLeft.$_borderRight.$_borderBottom.'">
								'.$ParRemark.'
								</td>
						  	</tr>';
		}	
		
		$colSpanStr = '';

		$text_alignStr ='center;';		
		
		if($PartNum=='5')
		{
			$text_alignStr='left;';
		}
		
		
		
		$html = '';

		$HeaderInfo = '<table width="100%" height="990px">';
		$HeaderInfo .= '<tr><td style="vertical-align:top">';
		$HeaderInfo .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}	
		$HeaderInfo .= '<tr> 
						<td colspan="'.$NumberOfCol.'"  bgcolor="'.$this->TableTitleBgColor.'" style=" font-size:15px;text-align:left;vertical-align:text-top;'.$_borderTop.$_borderLeft.$_borderRight.'"><font face="Arial" color="'.$this->HeaderTitleColor.'"><b>
						'.$mainTitle.'
						</b></font></td>
				  	</tr>';
		$HeaderInfo .= $remark_html;

		$HeaderInfo .= '<tr>'.$subtitle_html.'</tr>';

		$tableBottom_html ='</table>';
		$tableBottom_html .= '</td></tr>';
		$tableBottom_html .= '</table>';
//		if($hasPageBreak==true)
//		{
//			$tableBottom_html .=$this->getPagebreakTable();
//		}
		
		$count_col =count($colWidthArr);
		
		if($rowMax_count<=0)
		{	
			$html_content =' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderBottom.$_borderRight.'text-align:center"><b><font face="Arial">No Record Found.</font></b></td></tr>';	
			
			$html .= $HeaderInfo;
			$html .= $html_content;
		//	$html .= ' <td colspan="'.$count_col.'" style="vertical-align: top;'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'"><font face="Arial">'.$ParBottomRemark.'</font></td>';
			$html .= $tableBottom_html;
			$html .= $this->getFooter_HTML();	
			$html .= $this->getPagebreakTable();
		}

		else if($rowMax_count>0)
		{ 
			### 10 rows in one page	
			$NumOfDisplayedRow = 0;
			$SetHeader=false;
			
			
			for ($i=0; $i<$rowMax_count; $i++)
			{		
				
				$tableBottom_close_html='';
				$tableBottom_close_html .= '<tr><td colspan="'.$NumberOfCol.'" style="vertical-align:top;text-align:left;">';
				$tableBottom_close_html .= '<font face="Arial">'.$Remark.'</font>';
				$tableBottom_close_html .= '</td></tr>';
				$tableBottom_close_html .='</table>';
				$tableBottom_close_html .= '</td></tr>';
 				
				$tableBottom_close_html .= '</table>';
				//$tableBottom_close_html .= ($hasPageBreak==true)? $this->getPagebreakTable():'';
				
				
				
				$NumOfDisplayedRow = $i;
				
				$html_content = '';
				$html_content .= '<tr '.$RowHeight.'>';
				
				$thisNum = $i+1;	
				$html_content .=' <td style="vertical-align: top;'.$_borderTop.$_borderBottom.$_borderLeft.$_borderRight.'"><b><font face="Arial">'.$thisNum.'.</font></b></td>';	
				
				$k=0;		 
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{				
					if($_titleVal=='')
					{
						$_titleVal =  $this->EmptySymbol;
						$html_content .= ' <td style="text-align:center;vertical-align:top;'.$_borderTop.$_borderBottom.$_borderRight.'"><font face="Arial">'.$_titleVal.'</font></td>';						
					}
					else
					{
						if($k==0)
						{
							$html_content .= ' <td style="vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'"><b><font face="Arial">'.$_titleVal.'</font></b></td>';
						}
						else
						{
							$html_content .= ' <td style="vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'"><font face="Arial">'.$_titleVal.'</font></td>';
						}						
					}										
											 	
					$k++;
				}	
				$html_content .= '</tr>';	

				 if($NumOfDisplayedRow==0)  // if it is first row, add the header
				{
					$header_html=$HeaderInfo;
		
					if($RowPerPage<=1)  // if $RowPerPage is equal or small than one 
					{
						if($ParShowRemark)
						{
							$bottom_html = ' <td colspan="'.$count_col.'" style="vertical-align: top;'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'"><font face="Arial">'.$ParBottomRemark.'</font></td>';
						}
						$bottom_html .= $tableBottom_html;
						$bottom_html .= $this->getFooter_HTML();	
						$bottom_html .= $this->getPagebreakTable();
					}
					else if($rowMax_count<=1)
					{	
						if($ParShowRemark)
						{
							$bottom_html = ' <td colspan="'.$count_col.'" style="vertical-align: top;'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'"><font face="Arial">'.$ParBottomRemark.'</font></td>';
						}
						$bottom_html .=  $tableBottom_close_html;
						$bottom_html .= $this->getFooter_HTML();	
						$bottom_html .= $this->getPagebreakTable();
					}
					
				}
				else
				{
					$header_html='';						
					
					if(($NumOfDisplayedRow+1)%$RowPerPage==0)   // if it is  10th or 20th or 30th ....row (last row), add </table> 
					{
						$bottom_html = $tableBottom_html;
						$bottom_html .= $this->getFooter_HTML();	
						$bottom_html .= $this->getPagebreakTable();
						$SetHeader=true;
					}
					else if($SetHeader==true) // if it is 11th or 21th or 31th....row (first row), add the header
					{
						$header_html=$HeaderInfo;
						$SetHeader=false;
						
					}
					
					if($RowPerPage<=1)  // if $RowPerPage is equal or small than one 
					{
						$header_html = $HeaderInfo;
						$bottom_html = $tableBottom_html;	
						$bottom_html .= $this->getFooter_HTML();	
						$bottom_html .= $this->getPagebreakTable();	
					}
				}
				
				if($NumOfDisplayedRow >= $rowMax_count-1)  // if it is the last row , add </table> 
				{
					if($bottom_html=='')
					{
						if($ParShowRemark)
						{
							$bottom_html = ' <td colspan="'.$count_col.'" style="vertical-align: top;'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'"><font face="Arial">'.$ParBottomRemark.'</font></td>';	
						}
						
						$bottom_html .= $tableBottom_close_html;
						$bottom_html .= $this->getFooter_HTML();	
						$bottom_html .= $this->getPagebreakTable();
					}		
				}
				
				if($NumOfDisplayedRow!=0 && $header_html!='') // if starting a new page, insert an empty table
				{
					$html .= $this->getEmptyTable('3%');
				}
				$html .= $header_html; 		
				$html .= $html_content; // it is the subject content			
				$html .= $bottom_html;

				$header_html='';
				$bottom_html='';

			}
		}
		

		
		return $html;
	}

	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$OLE_Cate = $this->Get_eClassDB_Table_Name('OLE_CATEGORY'); 
		
		$OLEcomponentArr = $this->getOLEcomponentName();
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		$orderBy = '';
		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
			$orderBy = 'order by 
						SLPOrder asc	';
		}
		else
		{
			$orderBy = 'order by 
						y.Sequence asc,	
						ole_prog.Title asc	';
		}
		
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
		
	
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = " And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						ole_std.Hours as Hours,
						ole_std.Details as Details,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
				Inner join
						$OLE_Cate as ole_cate
				On
						ole_prog.Category = ole_cate.RecordID
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
				$orderBy	
				
				";
		$roleData = $this->objDB->returnResultSet($sql);
		
//hdebug_r($sql);

		$returnData = array();
		$dataArr = array();

		$count_star = 0;
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$_IntExt = $roleData[$i]['IntExt'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];

			$thisDataArr = array();
			
			if(strtoupper($IntExt)=='INT')
			{			
				$thisDataArr['Program'] =$roleData[$i]['Title'];						
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['Hours'] =($roleData[$i]['Hours']==0)? '*':$roleData[$i]['Hours'];
				$thisDataArr['OLEComponent'] =$roleData[$i]['OleComponent'];
				$thisDataArr['SchoolYear'] =$thisAcademicYear;	
				
				if($roleData[$i]['Hours']==0)
				{
					$count_star++;
				}
				
			}
			else if(strtoupper($IntExt)=='EXT')
			{		
				$thisDetails = $roleData[$i]['Details'];
				
				if($thisDetails!='')
				{
					$thisDetails = '<br/>。<i>'.$roleData[$i]['Details'].'</i>';
				}
				
				$thisDataArr['Program'] =$roleData[$i]['Title'].$thisDetails;						
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
				$thisDataArr['SchoolYear'] =$thisAcademicYear;	
			}
					
			$dataArr[] = $thisDataArr;
		}
		
		if(strtoupper($IntExt)=='INT')
		{			
			$returnData['showRemark'] = ($count_star>0)? true:false;
			$returnData['Data'] = $dataArr;
		}
		else if(strtoupper($IntExt)=='EXT')
		{
			$returnData = $dataArr;
		}
			
		return $returnData;
	}
	
	
	/****************** Footer *********************/
	public function getFooter_HTML()
	{	
		$PageNo = $this->PageNo;
		$this->PageNo++;
		
		$StdNameStr = $this->StdNameEn;
		
		$fontSize = '13px';
				
		$html = '';
		$html .= '<table width="90%" cellpadding="7" border="0px" align="center" style="font-size:10px">					
		   			  <tr> 
						<td style=" font-size:'.$fontSize.';text-align:right;vertical-align:text-top;"><b>
						'.$StdNameStr.' / P '.$PageNo.' of <!--TotalNumber-->
						</b></td>
			  	  	</tr>
				  </table>';
		return $html;
	}
	
	/****************** End Footer *********************/
	
	public function getOLEcomponentName()
	{
		$OLE_ELE = $this->Get_eClassDB_Table_Name('OLE_ELE');
		
		$sql="Select 
					EngTitle,
					ChiTitle,
					DefaultID 
			  from
					$OLE_ELE
			";
		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnArray = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisName_en = $roleData[$i]['EngTitle'];
			$thisName_ch = $roleData[$i]['ChiTitle'];
			$thisID = $roleData[$i]['DefaultID'];
			
			$returnArray[$thisID] =$thisName_en;
		}
	
		return $returnArray;
	}
	
	/********** Display Form 4, 5 and 6  ***********/
	private function getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,$order='asc')
	{
		
		$YearDisplayArr = array();
	
		$Form4_no = 0;
		$Form5_no = 0;
		$Form6_no = 0;
		

		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{						
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			
			$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
			
			$thisFormNo = (int)$thisClassName;
		
			preg_match('/\d+/', $thisClassName, $number);
			
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = intval($number[0]);
				
				if($thisFormNo==4 || $thisFormNo==5 || $thisFormNo==6)
				{				
					$YearDisplayArr[] = $YearClassArray[$i];
				}				
			}
		}	


		if($order=='desc')
		{
			$YearDisplayArr = array_reverse($YearDisplayArr);
		}
		

		return $YearDisplayArr;
	}
	
//	/********** Display Form 4, 5 and 6  ***********/
//	private function getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,$order='asc')
//	{
//		
//		$YearDisplayArr = array();
//	
//		$Form4_no = 0;
//		$Form5_no = 0;
//		$Form6_no = 0;
//		
//
//		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
//		{						
//			$thisYear = $YearClassArray[$i]['AcademicYear'];
//			$thisClassName = $YearClassArray[$i]['ClassName'];
//			
//			$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
//			
//			$thisFormNo = (int)$thisClassName;
//		
//			preg_match('/\d+/', $thisClassName, $number);
//			
//			if(count($number)>0 && is_array($number))
//			{
//				$thisFormNo = intval($number[0]);
//				
//				if($thisFormNo==4 || $thisFormNo==5 || $thisFormNo==6)
//				{				
//					$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
//				}				
//			}
//		}	
//	
//		if($order=='desc')
//		{
//			$YearDisplayArr = array_reverse($YearDisplayArr);
//		}
//		
//		$ThisClassArr = array();
//		$count_YearDisplayArr = 0;
//		foreach((array)$YearDisplayArr as $_formNo=>$_ClassArr)
//		{
//			$ThisClassArr[$count_YearDisplayArr] = $_ClassArr;
//			$count_YearDisplayArr++;
//		}
//		
//		return $ThisClassArr;
//	}
	
	public function getStudentAcademic_YearClassNameArr($AllYear=false)
	{
		$StudentID = $this->uid;
		
		$academicYearIDArr = $this->academicYearIDArr;
		
		if($AllYear)
		{
			$academicYearIDArr = $this->All_AcademicYearID;
		}
		
		$SelectStr= 'h.AcademicYear as AcademicYear,
					 h.ClassName as ClassName';
		
		if($academicYearIDArr!='')
		{		
			$YearArr = array();
			for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
			{
				if($academicYearIDArr[$i]=='')
				{
					continue;
				}
				$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
				$YearArr[] = $thisAcademicYear;
			}
			
			$cond_academicYear = "And h.AcademicYear In ('".implode("','",(array)$YearArr)."')";		
		}
		

		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				where 
						h.UserID = "'.$StudentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear asc';
						
		$roleData = $this->objDB->returnResultSet($sql);
		
	
		return $roleData;
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		asort($returnArray);	
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center"><font face="Arial">'.$Content.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	private function ch_num($num,$mode=true)
	{
		$char = array("零","一","二","三","四","五","六","七","八","九");
		$dw = array("","十","百","千","","萬","億","兆");
		$dec = "點";
		$retval = "";
		if($mode)
		preg_match_all("/^0*(\d*)\.?(\d*)/",$num, $ar);
		else
		preg_match_all("/(\d*)\.?(\d*)/",$num, $ar);
		if($ar[2][0] != "")
		$retval = $dec . ch_num($ar[2][0],false); //如果有小數，先遞歸處理小數
		if($ar[1][0] != "") {
		$str = strrev($ar[1][0]);
		for($i=0;$i<strlen($str);$i++) {
		$out[$i] = $char[$str[$i]];
		if($mode) {
		$out[$i] .= $str[$i] != "0"? $dw[$i%4] : "";
		if($str[$i]+$str[$i-1] == 0)
		$out[$i] = "";
		if($i%4 == 0)
		$out[$i] .= $dw[4+floor($i/4)];
		}
		}
		$retval = join("",array_reverse($out)) . $retval;
		}
		return $retval;
	}
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>