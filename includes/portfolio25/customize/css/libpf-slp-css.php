<?php

function EmptyContentAsEmptyString($Content=''){
	
	global $EmptyString;
	
	if($Content ==''){
		$Content = $EmptyString;
	}
	else{
		$Content = $Content;
	}
	
	return $Content;
}

class objPrinting{
//-- Start of Class

	var $EmptyString = '---';
	var $AcademicYearName;
	
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	private $cfgValue;
	
	private $AcademicYear;
	private $studentListAry;
	private $studentId;
	private $Student_info_Ary;
	private $Student_OLE_Ary;
	private $Student_Attendance_Ary;
	private $Student_Detention_Ary;
	private $Student_ClassName;
	private $Student_HouseTutor;
	private $headOfSchoolSignatureUrl;
	private $yearClassID;
	
	public function __construct(){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		$this->cfgValue = $ipf_cfg;
	}
		
	public function setAcademicYear($AcademicYear){
		$this->AcademicYear = $AcademicYear;
		$this->AcademicYearName = getAcademicYearByAcademicYearID($AcademicYear);
	}
	public function getAcademicYear($format='ID'){
		if($format == 'ID'){
			return $this->AcademicYear;
		}
		else{
			return $this->AcademicYearName;	
		}
	}
	
	public function setYearClassID($yearClassId){
		$this->yearClassID = $yearClassId;
	}
	public function getYearClassID(){
		return $this->yearClassID;
	}
	
	public function setStudentList($studentListAry){
		$this->studentListAry = $studentListAry;
	}
	public function getStudentList(){
		return $this->studentListAry;
	}
	
	
	public function setStudent($studentId){
		$this->studentId = $studentId;
	}
	public function getStudent(){
		return $this->studentId;
	}
	
	public function contentReady(){
		
		//Please Set StudentList Before call this function		
		$this->Student_info_Ary = $this->getStudentList_Info();
		$this->Student_OLE_Ary = $this->getStudentList_OLE();
		$this->Student_Attendance_Ary = $this->getStudentListAttendanceRecords();
		$this->Student_Detention_Ary = $this->getStudentListDetentionRecords();
		$this->headOfSchoolSignatureUrl = $this->getHeadOfSchoolSignatureUrl();
		$this->Student_HouseTutor = $this->getHouseTutor();

	}
	
			
	//Get Student Info
	private function getStudentList_Info(){
		$sql = "select 
					iu.EnglishName,
					iu.ChineseName,
					DATE_FORMAT(iu.DateOfBirth, '%d/%m/%Y') AS DateOfBirth,
					iu.ClassName,
					iu.ClassNumber,
					iu.Gender,
					iu.BarCode,
					iu.STRN,
					iu.HKID,
					substring(iu.WebSAMSRegNo from 2) as WebSAMSRegNo ,
					iu.userid
				from
					".$this->intranet_db.".INTRANET_USER AS iu
				where
					iu.userid IN ('".implode('\',\'',$this->getStudentList())."') 
					AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";

		$result = $this->objDB->returnResultSet($sql);
		
		return BuildMultiKeyAssoc($result,'userid');
	}
	public function getStudentInfo(){
		return $this->Student_info_Ary[$this->getStudent()];
	}
	
	# House Tutor = Class Teacher
	private function getHouseTutor(){

// 					 /*CONCAT('.getNameFieldByLang('u.','en').') AS TeacherName*/
		
		$sql = 'SELECT
					 IF(u.EnglishName IS NULL OR TRIM(u.EnglishName) = \'\',u.ChineseName,u.EnglishName) AS TeacherName
				FROM 
					YEAR_CLASS_TEACHER as yct 
					LEFT JOIN 
					INTRANET_USER as u ON (yct.UserID = u.UserID)
				WHERE
					YearClassID = \''.$this->yearClassID.'\'';
		$result = $this->objDB->returnResultSet($sql);
		$TeacherNameAry = Get_Array_By_Key($result,'TeacherName');
		
		return implode(' and ',$TeacherNameAry);
		
	}
	public function getStudentHouseTutor(){

		return $this->Student_HouseTutor;
		
	}
	
	//Get Student OLE
	private function getStudentList_OLE(){
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";
		
		$sql = "select 
					op.Title,
					op.StartDate,
					op.EndDate,
					os.Role,
					os.Hours,
					os.Achievement,
					op.ELE,
					op.Details,
					ay.YearNameEN as AcademicYear,
					IF(ayt.YearTermNameEN IS NULL, 'Whole Year',ayt.YearTermNameEN) as Term,
					oc.EngTitle as Category,
					os.userid,
					os.Organization as Organization,
					oc.RecordID as ProgramCategoryID,
					op.IntExt as IntExt
				from 
					".$this->eclass_db.".OLE_STUDENT as os 
					inner join ".$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid 
					inner join ".$this->eclass_db.".OLE_CATEGORY as oc on op.Category = oc.RecordID 
					inner join ".$this->intranet_db.".ACADEMIC_YEAR as ay on op.AcademicYearID = ay.AcademicYearID
					left join ".$this->intranet_db.".ACADEMIC_YEAR_TERM  as ayt on op.YearTermID = ayt.YearTermID
				where 
					os.userid IN ('".implode('\',\'',$this->getStudentList())."')  
					$condApprove
					AND op.AcademicYearID = '".$this->AcademicYear."'
				order by 
					op.Title asc
				";


		$result = $this->objDB->returnResultSet($sql);
		
		foreach($result as $_OLEInfoArr){
			$_studentId = $_OLEInfoArr['userid'];
			$_programCategoryID = $_OLEInfoArr['ProgramCategoryID'];
			$_IntExt = $_OLEInfoArr['IntExt'];
					
			if($_programCategoryID == 5){
				//services
				$resultAssoArr[$_studentId][$_IntExt]['SERVICE'][] = $_OLEInfoArr;
			}
			else{
				//external or internal OLE
				$resultAssoArr[$_studentId][$_IntExt]['OLE'][] = $_OLEInfoArr;
			}

		}
	
		return $resultAssoArr;
	}
	public function GetOLE($IntExt, $Category){
		$IntExt = strtoupper($IntExt);
		$Category= strtoupper($Category);
		
		return $this->Student_OLE_Ary[$this->getStudent()][$IntExt][$Category];
	}
	
	private function getStudentListAttendanceRecords(){
		
		$sql = "SELECT 
					UserID,
					COUNT(IF(RecordType=1 AND DayType=2 ,1,NULL)) AS NumAbsAM,
					COUNT(IF(RecordType=1 AND DayType=3 ,1,NULL)) AS NumAbsPM,
					COUNT(IF(RecordType=2 AND DayType=2 ,1,NULL)) AS NumLateAM, 
					COUNT(IF(RecordType=2 AND DayType=3,1,NULL)) AS NumLatePM   
				FROM 
					".$this->intranet_db.".PROFILE_STUDENT_ATTENDANCE 
				WHERE 
					UserID IN ('".implode('\',\'',$this->getStudentList())."')
					AND AcademicYearID = '".$this->AcademicYear."'
				GROUP BY UserID ;";
		
		$result = $this->objDB->returnResultSet($sql);
		
		return BuildMultiKeyAssoc($result, 'UserID', array('NumAbsAM','NumAbsPM','NumLateAM','NumLatePM'));
	}
	public function getStudentAttendanceRecords(){
		
		return $this->Student_Attendance_Ary[$this->getStudent()];
		
	}
	
	private function getStudentListDetentionRecords(){

		$ayStartDate = substr(getStartDateOfAcademicYear($this->getAcademicYear()), 0, 10 );
		$ayEndDate = substr(getEndDateOfAcademicYear($this->getAcademicYear()), 0, 10 );
		
		$sql = "SELECT 
					ddss.StudentID,
					COUNT(IF(dds.SessionType=1, 1, NULL)) AS NumLunch,
					COUNT(IF(dds.SessionType=2, 1, NULL)) AS NumAfterSchool, 
					COUNT(IF(dds.SessionType=3, 1, NULL)) AS NumSuspension   
				FROM 
					".$this->intranet_db.".DISCIPLINE_DETENTION_STUDENT_SESSION  as ddss 
					inner join ".$this->intranet_db.".DISCIPLINE_DETENTION_SESSION as dds on ddss.detentionID = dds.detentionID
				WHERE 
					ddss.StudentID IN ('".implode('\',\'',$this->getStudentList())."')
					AND dds.DetentionDate BETWEEN '".$ayStartDate."' AND '".$ayEndDate."'  
				GROUP BY ddss.StudentID ;";
		
		$result = $this->objDB->returnResultSet($sql);

		return BuildMultiKeyAssoc($result, 'StudentID', array('NumLunch','NumAfterSchool','NumSuspension'));
	}
	public function getStudentDetentionRecords(){
		
		return $this->Student_Detention_Ary[$this->getStudent()];
		
	}
	
	private function getHeadOfSchoolSignatureUrl(){
		
		// Get YearID
		$sql = 'Select 
					YearID
				from
					'.$this->intranet_db.'.YEAR_CLASS
				where
					YearClassID = "'.$this->getYearClassID().'" 
				';
				
		$result = $this->objDB->returnVector($sql);
		
		//get Setting
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
		$libgeneralsettings = new libgeneralsettings();
		
		$getGeneralSettingArr = array("'cust_CSS_slp_HeadOfSchoolConfig_$result[0]'");
		$GeneralSettingArr = $libgeneralsettings->Get_General_Setting('iPortfolio',$getGeneralSettingArr);
		
		$schoolHeadUserId = $GeneralSettingArr["cust_CSS_slp_HeadOfSchoolConfig_$result[0]"];
		
		$sql = 'SELECT
					SignatureLink
				FROM
					'.$this->intranet_db.'.INTRANET_USER as iu
					LEFT OUTER JOIN '.$this->intranet_db.'.INTRANET_USER_SIGNATURE as ius on iu.UserLogin = ius.UserLogin
				WHERE
					iu.UserID = "'.$schoolHeadUserId.'";
				';
		$signatureArr = $this->objDB->returnVector($sql);
		
		return $signatureArr[0]; 
	}
	
	function getHouseName($className){
		
		$HouseNameArray = array(
			'Ba' => 'Bauhinia',
			'Bo' => 'Bombax',
			'Ca' => 'Cassia',
			'De' => 'Delonix',
			'Ja' => 'Jacaranda',
			'Ju' => 'Juniper'
		);
		$count = 0;
		foreach((array)$HouseNameArray as $_key => $_val){
			$classNameModified = str_replace($_key,'',$className,$count);
			if($count == 1){
				$classNameModified = $classNameModified.' '.$_val;
				break;
			}	
		}
		if($count==1){
			return $classNameModified;
		}
		else{
			return $className;
		}
		
	}
	
	//------------------------------------------------ ABOVE ARE DATA HANDLING RELATED ------------------------------------------------//
	#####################################################################################################################################
	#																																	#
	#																																	#
	#####################################################################################################################################	
	//------------------------------------------------ BELOW ARE HTML AND CSS RELATED -------------------------------------------------//

	public function returnStyleSheet(){
		$style = '<head>
				<style TYPE=\'text/css\'>
					
					body { font-family:msjh;}

					.contentTable  { 
						border-top: 0.1mm solid #000000; 
						border-bottom: 0.1mm solid #000000; 
						border-left: 0.1mm solid #000000; 
						border-right: 0.1mm solid #000000;
						/*font-size: 12pt;*/ 
					}

					table.contentTable thead td { 
						text-align: center;
						border: 0.1mm solid #000000;
					}
					table.contentTable thead td.header {
						text-align: left;
						background-color: #EEEEEE; 
						font-weight: bold;
					}
					table.contentTable tbody {
						border: 0.1mm solid #000000;
					}
					table.contentTable td {
						border-left: 0.1mm solid #000000;
						border-right: 0.1mm solid #000000;
						padding: 5 5 5 5;
					}
					table.contentTable tr.bottom td{
						border-bottom: 0.1mm solid #000000;
					}
					table.contentTable tr.emptyRow td{
						text-align: center;
						border-bottom: 0.1mm solid #000000;
					}

				</style>
				</head>';
				
		return $style;
	}
	
	public function returnSignature(){
		
		$headOfSchoolSign = $this->headOfSchoolSignatureUrl;
		if($headOfSchoolSign == ''){
			$signImage = '';
		}
		else{
			$signImage = '<img src="'.$headOfSchoolSign.'" width="35mm"/>';
		}
		
		$sign = '<div style="position:absolute; left:0;bottom:100;"><table width="100%" style=" text-align: bottom; vertical-align: bottom;">
					<tr>
						<td width="9%"></td>
						<td width="22%"><br><br><br>House Tutor Signature:</td>
						<td width="18%" style="border-bottom: 0.1mm solid #000000;"></td>
						<td width="24%"><br><br><br>Head of School Signature:</td>
						<td width="18%" style="border-bottom: 0.1mm solid #000000; text-align:center;">'.$signImage.'</td>
						<td width="9%"></td>
					</tr>
				</table></div>';
		return $sign;
	}
	
	public function returnHeaderAndFooter($String){
		
		global $PATH_WRT_ROOT;
		
		//header images
		//$schoolbanner_url = $PATH_WRT_ROOT."/file/customization/css/school_banner.png";
		$schoolbanner_url = $PATH_WRT_ROOT."/file/customization/css/school_logo.jpg";
		$schoollogo2_url = $PATH_WRT_ROOT."/file/customization/css/school_logo2.png";
		$schoollogo1_url = $PATH_WRT_ROOT."/file/customization/css/school_logo1.png";
		$footermain_url = $PATH_WRT_ROOT."/file/customization/css/footer_main.png";
		$footerleft_url = $PATH_WRT_ROOT."/file/customization/css/footer_left2.png";
		$footerright_url = $PATH_WRT_ROOT."/file/customization/css/footer_right2.png";
		
		//header and footer
		$header = '<table width="100%" style=" text-align: center; vertical-align: middle; font-size: 13pt;">
				<tr>
					<td width="17%"></td>
					<td width="68%" align="center"><img src="'.$schoolbanner_url.'" width="53mm"/></td>
					<td width="17%"></td>
				</tr>
				<tr>
					<td colspan="3"><strong>Student Development Progress Report '.$this->AcademicYearName.'</strong></td>
				</tr>
			</table>';
		
		$footer = '<table width="100%" style="vertical-align: bottom; padding:0;">
					<tr>
						<td colspan="2" width="100%" align="center" style="font-size:9pt"><br>{CUST_PAGENO_1}</td>
					</tr>
				</table>
';

//					<tr>/{nbpg}
//						<td width="89%"><img src="'.$footermain_url.'" width="147mm"/></td><td><img src="'.$footerleft_url.'" width="13mm"/></td>
//					</tr>
		if(strtolower($String) == 'header'){
			return $header;
		}
		else if(strtolower($String) == 'footer'){
			return $footer;
		}
	}
	
//-- End of Class
}


?>