<?php
/*
 *  Change log:
 *  Date: 2016-11-14 Villa [B106517]
 *  modified getPartD_Display() - check to display part D if contect is not null
 *  Date: 2016-11-08 Villa [B106517]
 *  modified getPartD_Display() - check isForm6 to decide display part D or not
 *  add isForm6() - check if the student is form 6 or not
 */
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $academcYearDisplay; // store the academic Year display in the student Info
	private $uid;
	private $cfgValue;
	private $requireELE; // record the client require ELE type
	private $eleNameAry; // store the ELE display Name for the reprot

	public function studentInfo($uid){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid;
		$this->cfgValue = $ipf_cfg;
		$this->setRequireELE();

		$this->setELENameFromDB();
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	private function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	private function setELENameFromDB(){
		$requireELE = $this->getRequireELE();


		$eleStr = "";
		for($i = 0,$i_max = sizeof($requireELE);$i< $i_max;$i++){
			$_eleName = $requireELE[$i];

			$eleStr .= "'{$_eleName}',";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$eleStr = substr($eleStr,0,-1);
		$sql = "select DefaultID,concat(ChiTitle,' (',EngTitle,')') as 'eleName'  from ".$this->eclass_db.".OLE_ELE where defaultid in({$eleStr})";
		$result = $this->objDB->returnArray($sql);

		for($i=0,$i_max = sizeof($result);$i < $i_max;$i++){
			$_eleID = $result[$i]["DefaultID"];
			$_eleNAME = $result[$i]["eleName"];
			$ele[$_eleID] = $_eleNAME;
		}
		$this->eleNameAry = $ele;

	}
	private function getELENameDetails($eleType){
		return $this->eleNameAry[$eleType];
	}
	private function getELENameAry(){
		return $this->eleNameAry;
	}
	private function getRequireELE(){
		return $this->requireELE;
	}
	private function setRequireELE(){
		$_ary[]="[MCE]";
		$_ary[]="[AD]";
		$_ary[]="[PD]";
		$_ary[]="[CS]";
		$_ary[]="[CE]";
		$this->requireELE = $_ary;
	}
	public function getUid(){
		return $this->uid;
	}
	/*************************/
	/*******STUDENT INFO******/
	/*************************/
	public function getStudentInfo_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		$sql = "select " .
					"EnglishName," .
					"ChineseName," .
					"concat(if(ChineseName is null or trim(ChineseName) = '','',ChineseName), ' ', if (EnglishName is null or trim(EnglishName ) = '','',concat('',EnglishName ,'') ) ) as 'studentName',".
					"STRN," .
					"ClassName," .
					"ClassNumber," .
					"concat(ClassName,'',if(classnumber is null or trim(classnumber) = '','',concat('(',classnumber,')'))) as 'classNameNo',".
					"substring(WebSAMSRegNo from 2) as 'WebSAMSRegNo'" .
				"from " .
					$this->intranet_db.".INTRANET_USER " .
				"where " .
					"userid =".$this->uid." AND (RecordType = '2' OR UserLogin like 'tmpUserLogin_%') ";
				
		$result = $this->objDB->returnArray($sql);

		return $result;
	}
	private function getStudentInfoDisplay($result){
		
		$strn = $result[0]["STRN"];
		
		//$strn = $this->specialHandleStrn($strn);
	
		$str .= "<table width=\"100%\" border = \"0\">";

		$str .= "<col width= \"25%\"/>";
		$str .= "<col width= \"30%\"/>";
		$str .= "<col width= \"30%\"/>";
		$str .= "<col width= \"15%\"/>";

		$str .= "<tr><td >學生姓名 Name : </td><td>".$result[0]["studentName"]."&nbsp</td><td>學生編號 STRN : </td><td>".$strn."&nbsp</td></tr>";
		$str .= "<tr><td >班別 Class : </td><td>".$result[0]["classNameNo"]."&nbsp</td><td>註冊編號 Reg. No : </td><td>".$result[0]["WebSAMSRegNo"]."&nbsp</td></tr>";
		$str .= "<tr><td >學年 Academic Year : </td><td>".$this->getAcademicYearDisplay()."</td><td>派發日期 Date of Issue : </td><td>".$this->printIssueDate."</td></tr>";
		$str .= "</table>";
		
		return $str;	
	}
	
	private function getPartA($createdBy = null){

		$createdBy = ($createdBy === null)? $this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"] :$createdBy;

		$result = $this->getOLE($this->cfgValue["OLE_TYPE_STR"]["INT"],$createdBy);

		return $result;
	}
	private function getPartB(){
		$result = $this->getOLE($this->cfgValue["OLE_TYPE_STR"]["INT"],$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]);
		return $result;
	}
	private function getPartC(){
		$result = $this->getOLE($this->cfgValue["OLE_TYPE_STR"]["EXT"]);
		return $result;	
	}

	//return OLE result for both INT /EXT
	private function getOLE($oleType,$createBy = null){
		$requireELE = $this->getRequireELE();
		
		$requestYear = $this->getAcademicYear();

		if(is_array($requestYear) && sizeof($requestYear) > 0){
			$requestYearStr = implode(",",$requestYear);
			$cond_requestYear = " and op.AcademicYearID in ({$requestYearStr})";
		}


		$cond = " and op.INTEXT ='{$oleType}' ";
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";
		$cond_CreatedBY = (trim($createBy)=="") ?"":" and op.PROGRAMTYPE = '{$createBy}' "; 

		//common sql for both INT / EXT
		$sql = "select " .
						"op.title , " .
						"op.StartDate, " .
						"op.EndDate,".
						"concat(date_format(op.StartDate, '%d-%m-%Y' ),'',if(op.enddate is null or (date_format(op.enddate,'%d-%m-%Y')='00-00-0000'),'',concat('-<br />',date_format(op.enddate,'%d-%m-%Y')))) as 'pDate',".
						"os.Role,".
						"os.HOURS,".
						"op.Organization,".
						"os.Achievement, ".
						"op.ProgramType ".
					"from " .
						$this->eclass_db.".OLE_STUDENT as os " .
						"inner join " .$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid ".
					"where " .
						"os.userid = ".$this->getUid()." ".
						$cond.
						$cond_CreatedBY.
						$condApprove.
						$cond_requestYear." ";
		$sqlOrder = " order by op.StartDate desc, os.Hours desc ";

		//case handle for INT / EXT
		if($oleType == $this->cfgValue["OLE_TYPE_STR"]["EXT"]){
			$sql .= $sqlOrder;
			$result = $this->objDB->returnArray($sql);	 

		}else{

			for($i = 0,$i_max = sizeof($requireELE);$i<$i_max;$i++){
				$_ele = $requireELE[$i];
				$sql2 = $sql." and INSTR(op.ELE, '{$_ele}') > 0";
				$sql2 .= $sqlOrder;

				$result[$_ele] = $this->objDB->returnArray($sql2);	 

			}

		}
		return $result;	
	}
	
	public function getIntCreatedBySchoolOnly_HTML(){
		//$result = $this->getPartA($this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);

		$result = $this->getOLE($this->cfgValue["OLE_TYPE_STR"]["INT"],$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);
		$html	= $this->getPartA_Display($result);
		return $html;
	}


	public function getIntCreatedByStudentMsg_HTML(){

		$header = '乙) &nbsp;其他學習經歷 (學生自行申報項目)';
		//$desc = '校方未能核實此部份資料，學生須自行列印並向相關人士提供適當證明。';
		$desc = '學生自行提供證明文件予相關人士。';

		//$content = "<table border =\"1\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" class=\"section\"><tr><td><br/>&nbsp;</td></tr></table>";
		$html = $this->getSectionHTML($content,$header,$desc);

		return $html;
	}

	public function getExtCreatedBySchoolOnly_HTML(){
		$result = $this->getOLE($this->cfgValue["OLE_TYPE_STR"]["EXT"],$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);
//		$html = $this->getOLEExt_Display($result);
		$html = $this->getOLEExt_HTML($result,$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);

		//$html	= $this->getPartA_Display($result);
		return $html;

	}

	public function getSelfAccount_HTML(){
		$result = $this->getSelfAccount();
		$html = $this->getPartD_Display($result);
		return $html;
	}

	public function getIntCreatedByStudentOnly_HTML(){
		$result = $this->getOLE($this->cfgValue["OLE_TYPE_STR"]["INT"],$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]);
		$html = $this->getOLEInt_HTML($result,$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]);
//		$html	= $this->getPartA_Display($result);
		return $html;
	}
	public function getExtCreatedByStudentOnly_HTML(){
		$result = $this->getOLE($this->cfgValue["OLE_TYPE_STR"]["EXT"],$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]);
		$html = $this->getOLEExt_HTML($result,$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]);
//		$html	= $this->getPartA_Display($result);
		return $html;
	}

	

	public function getPartA_HTML(){
		$result = $this->getPartA();
		$html = $this->getPartA_Display($result);
		return $html;
	}
	private function getPartA_Display($result){

		$html = $this->getOLEInt_HTML($result,$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);

		return $html;
	}

	
	private function getOLEExt_HTML($data,$createBy){




		$header[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]]="丙) &nbsp;校外校內的表現 / 獎項或重要參與";
		//$desc[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]] = "由校方申報資料已被核實。<br/>校方未能核實學生申報資料，學生須自行列印並向相關人士提供適當證明。";
		$recordNotFindMsg[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]] = '未有紀錄';


		$header[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]]="II) &nbsp;校外校內的表現 / 獎項或重要參與";
		$desc[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]] = ""; // no value
		$recordNotFindMsg[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]] = '未有紀錄';
		

		$displayHeader = $header[$createBy];
		$displayDesc = $desc[$createBy];		
		$dispalyRecordNotFindMsg = $recordNotFindMsg[$createBy];

		$display = 	"<col width= \"220\"/>".
					"<col width= \"80\"/>".
					"<col width= \"75\"/>".
					"<col width= \"150\"/>".
					"<col width= \"*\"/>".
					"<!--col width= \"75\"/-->".
		$display .= "<tr>".
					"<td align =\"center\">活動項目</td>".
					"<td align =\"center\">舉行時段</td>".
					"<td align =\"center\">參與角色</td>".
					"<td align =\"center\">主辦機構</td>".
					"<td align =\"center\" nowrap=\"nowrap\">獎項／証書文憑／成就 (如有)</td>".
					"<!--td align =\"center\">申報者</td-->".
					"</tr>";


			$eleHtml = "";
			$totalHr = 0;
			for($i =0,$i_max= sizeof($data);$i<$i_max;$i++){
				$_title = $data[$i]["title"];
				$_date = $data[$i]["pDate"];
				$_role = $data[$i]["Role"];
				$_org = $data[$i]["Organization"];
				$_award = $data[$i]["Achievement"];
				$_createdBY = $data[$i]["ProgramType"];

				$_createdBY = ($_createdBY=="S") ?  "學生" : "校方" ;
				
				$totalHr += $_hr;
				
				$eleHtml .="<tr>".
						"<td >{$_title}&nbsp;</td>".
						"<td nowrap=\"nowrap\" align =\"center\">{$_date}&nbsp;</td>".
						"<td align =\"center\">{$_role}&nbsp;</td>".
						"<td >{$_org}&nbsp;</td>".
						"<td >{$_award}&nbsp;</td>".
						"<!--td align =\"center\">{$_createdBY}&nbsp;</td-->".
						"</tr>";	
			}

			if(trim($eleHtml) == ""){

				$eleHtml = "<tr>".
						   "<td colspan=\"6\" >{$dispalyRecordNotFindMsg}</td>".
				           "</tr>";
			}
			$display .= $eleHtml;			


		$html = "<table border =\"1\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" class=\"section\">".$display."</table>";

		return $this->getSectionHTML($html,$displayHeader,$displayDesc);


	}

	private function getOLEInt_HTML($result,$createdBy){

		$header[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]] = "甲) &nbsp;其他學習經歷 (學校舉辦項目)";
		$desc[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]] = "「其他學習經歷」泛指同學在下列五範疇之經歷：德育及公民教育、藝術發展、體育發展、社會服務及與工作有關經驗。以下活動資料已由校方核實。";
		$organization[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]] = "合辦機構 (如有)";
		$recordNotFindMsg[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]] = '未有紀錄';

		$header[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]] = "I) &nbsp;其他學習經歷";
		$organization[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]] = "主辦機構";
		$recordNotFindMsg[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]] = '未有紀錄';
		$desc[$this->cfgValue["OLE_PROGRAM_PROGRAMTYPE"]["createdByStudent"]] = ''; //empty

		$displayHeader = $header[$createdBy];
		$displayDesc = $desc[$createdBy];
		$displayRecordNotFindMsg = $recordNotFindMsg[$createdBy];
		$orgDisplay = $organization[$createdBy];


		$display = 	"<col width= \"220\"/>".
					"<col width= \"80\"/>".
					"<col width= \"75\"/>".
					"<col width= \"75\"/>".
					"<col width= \"150\"/>".
					"<col width= \"150\"/>".


		$display .= "<tr>".
					"<td align =\"center\">活動項目</td>".
					"<td align =\"center\">舉行時段</td>".
					"<td align =\"center\">參與角色</td>".
					"<td align =\"center\">時數<br/>(小時)</td>".
					"<td align =\"center\">{$orgDisplay}</td>".
					"<td align =\"center\" nowrap=\"nowrap\">獎項／証書文憑／成就 <br />(如有)</td>".
					"</tr>";

		foreach($result as $ele=>$data){
			$eleHtml = "";
			$totalHr = 0;
			for($i =0,$i_max= sizeof($data);$i<$i_max;$i++){
				$_title = $data[$i]["title"];
				$_date = $data[$i]["pDate"];
				$_role = $data[$i]["Role"];
				$_hr = $data[$i]["HOURS"];
				$_org = $data[$i]["Organization"];
				$_award = $data[$i]["Achievement"];
				
				$totalHr += $_hr;
				
				$eleHtml .="<tr>".
						"<td >{$_title}&nbsp;</td>".
						"<td nowrap=\"nowrap\" align =\"center\">{$_date}&nbsp;</td>".
						"<td align =\"center\">{$_role}&nbsp;</td>".
						"<td align =\"center\">{$_hr}&nbsp;</td>".
						"<td >{$_org}&nbsp;</td>".
						"<td >{$_award}&nbsp;</td>".
						"</tr>";	
			}

			$ele = $this->getELENameDetails($ele);
			$display .="<tr>".
				    "<td colspan=\"6\"><span class=\"section_title\">{$ele} 共 {$totalHr} 小時</span></td>".
				    "</tr>";
			if(trim($eleHtml) == ""){
				$eleHtml = "<tr>".
						   "<td colspan=\"6\" >{$displayRecordNotFindMsg}</td>".
				           "</tr>";
			}

			$display .= $eleHtml;			
		}

		$html = "<table border =\"1\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" class=\"section\">".$display."</table>";
		return $this->getSectionHTML($html,$displayHeader,$displayDesc);
	}

	private function getSectionHTML($content,$header="",$desc=""){
			$returnStr  = '<table width = "100%"">'.			
	                      '<tr><td colspan="2"><span class="section_title">'.$header.'</span></td></tr>';
			if($desc != ""){
					$returnStr .= "<tr><td nowrap='nowrap'>&nbsp; &nbsp; &nbsp;</td><td width='99%'>".$desc."</td></tr>";
			}

			$returnStr .= "</table>";
			$returnStr .= $content;
			return $returnStr;
	}
	public function getPartB_HTML(){
		$result = $this->getPartB();
		$html = $this->getPartB_Display($result);
		return $html;
	}
	private function getPartB_Display($result){

		return $this->getOLEInt_Display($result,'B');
//		return $html;
	}
	
	public function getPartC_HTML(){
		$result = $this->getPartC();
		$html = $this->getPartC_Display($result);
		return $html;
	}
	private function getPartC_Display($result){
		return $this->getOLEExt_Display($result);	
	}
	
	
	public function getPartD_HTML(){
		$result = $this->getPartD();
		$html = $this->getPartD_Display($result);
		return $html;
	}
	private function getPartD(){
		$result = $this->getSelfAccount();
		return $result;	
	}
	private function getSelfAccount(){
		$sql = "select sas.Details as Details from ".$this->eclass_db.".SELF_ACCOUNT_STUDENT as sas where Userid = ".$this->getUid()." and instr(DefaultSA,'SLP')>0 limit 1";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}

	private function getPartD_Display($result){

		$header="丁) &nbsp;學生自述";
		$desc="學生概略地敘述最少一項印象深刻的學習經歷，說明該經歷如何影響其個人成長及人生目標，或如何影響其個人抱負及全人發展。";

		if(is_array($result) && sizeof($result) > 0){
			$studentDetails = $result[0]["Details"];
		}
		if ($this->isForm6() && trim($studentDetails) != ""){
		//$_align = (trim($studentDetails) == "") ?' align="center" ':'';
			$studentDetails = (trim($studentDetails) == "") ?"未有紀錄":"<span style='line-height:20px;'>".$studentDetails."<span>";
	
	
			$html = "<table border =\"0\" width=\"100%\" height=\"750\" cellpadding=\"0\" cellspacing=\"10\" class='self_intro_text' ><tr><td valign='top'>".$studentDetails."</td></tr></table>";
			return $this->getSectionHTML($html,$header,$desc);
		}else{
			return false;
		}

	}
	public function setAcademicYearDisplay($academcYearDisplay){
		$this->academcYearDisplay = $academcYearDisplay;
	}
	public function getAcademicYearDisplay(){
		return $this->academcYearDisplay;
	}
	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	private function specialHandleStrn($strn){

			//USER request to replace the last character of strn to XXX
			$pattern = '/^(.*).{3}$/';
			$replacement = '${1}XXX';
			$strn = preg_replace($pattern,$replacement,$strn);
			return $strn;
	}
	
	private function isForm6(){
		$result = $this->getStudentInfo();
		$sql = "select y.WEBSAMSCode from YEAR as y left join YEAR_CLASS as yc1 on (y.YearID = yc1.YearID)
				WHERE yc1.ClassTitleEN ='". $result[0]['ClassName']."' 
				AND AcademicYearID = '".$_SESSION['CurrentSchoolYearID']."'";
		$result = $this->objDB->returnResultSet($sql);
		$result = $result[0]['WEBSAMSCode'];
		if($result=='S6'){
			return true;
		}else{
			return false;
		}
	}

}
?>