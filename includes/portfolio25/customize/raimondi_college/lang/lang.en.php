<?php
### Report Selection
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['Title'] = "Non-academic Performance Report";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['WithStudentSelfAccountAndYearGrouping'] = "With Student's Self-Account and Year Grouping";

### Report Header
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['SchoolNameEn'] = "RAIMONDI COLLEGE";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['SchoolNameCh'] = "高主教書院";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TitleEn'] = "Non-academic Performance Report";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TitleCh'] = "非學術表現記錄";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['SchoolNameCh'] = "高主教書院";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['AddressEn'] = "2 Robinson Road, Hong Kong";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['AddressCh'] = "香港羅便臣道二號";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TelephoneEn'] = "Tel: (852) 2522 2159";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['FaxEn'] = "Fax: (852) 2525 6725";

### Student Info
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['StudentNameEn'] = "Name";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['StudentNameCh'] = "學生姓名";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['SexEn'] = "Sex";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['SexCh'] = "性別";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassEn'] = "Class";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassCh'] = "班別";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassNoEn'] = "Class .";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassNoCh'] = "學號";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['DateOfBirthEn'] = "DOB";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['DateOfBirthCh'] = "出生日期";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['Gender']['M']['En'] = "M";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['Gender']['M']['Ch'] = "男";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['Gender']['F']['En'] = "F";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['Gender']['F']['Ch'] = "女";

### Membership
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['MembershipEn'] = "Membership";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['MembershipCh'] = "會員";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClubEn'] = "Club";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClubCh'] = "學會";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TeamEn'] = "Team";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TeamCh'] = "團隊";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['YearEn'] = "Year";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['YearCh'] = "年份";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['PostEn'] = "Post";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['PostCh'] = "職位";

### Activities and Awards
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ActivitiesAndAwardsEn'] = "Activities and Awards";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ActivitiesAndAwardsCh'] = "活動及獎項";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ActivitiesEn'] = "Activities";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ActivitiesCh'] = "活動";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['AwardsEn'] = "Awards";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['AwardsCh'] = "獎項";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['EventTitleEn'] = "Event Title";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['EventTitleCh'] = "事項名稱";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['OrganizedByEn'] = "Organized By";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['OrganizedByCh'] = "主辦機構";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['RoleEn'] = "Role";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['RoleCh'] = "參與角色";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['OLEEn'] = "OLE";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['OLECh'] = "其他學習經歷";

### Signature
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['PrincipalEn'] = "Principal";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['PrincipalCh'] = "校長";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassTeacherEn'] = "Class Teacher";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassTeacherCh'] = "班主任";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ParentGuardianEn'] = "Parent / Guardian";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ParentGuardianCh'] = "家長 / 監護人";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['DateOfIssueEn'] = "Date of Issue";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['DateOfIssueCh'] = "派發日期";


### Self Account
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['StudentSelfAccountEn'] = "Student's Self-Account";
$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['StudentSelfAccountCh'] = "學生自述";
?>