<?
/**
 * [Modification Log] Modifying By: ivan
 * ***********************************************
 * ***********************************************
 */
include_once($intranet_root."/includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include("lang/lang.$intranet_session_language.php");

class libpf_raimondi_college
{
	public function __construct() {
		$this->MaxItemArr['Membership'] = 4;
		$this->MaxItemArr['ActivitiesAndAwardsWithoutGrouping'] = 10;
		$this->MaxItemArr['ActivitiesAndAwardsWithGrouping'] = 20;
		
		$this->HeightArr['TableTop'] = 680; 		//before 22-10-2012 : 1000
		$this->HeightArr['SelfAccountTable'] = 680;	//before 22-10-2012 : 900, 100px assigned to signature table, others used for student info
		
		$this->EmptySymbol = '--';
		
		$this->SignatureArr = array('Principal', 'ClassTeacher', 'ParentGuardian', 'DateOfIssue');
	}
	
	function Get_Max_Display_Item($Type)
	{
		return $this->MaxItemArr[$Type];
	}
	
	function Get_Signature_Item()
	{
		return $this->SignatureArr;
	}
	
	function Get_Height($Type)
	{
		return $this->HeightArr[$Type];
	}
	
	function Get_Student_OLE_Consolidated_Array($StudentID, $AcademicYearIDArr, $WithYearGrouping)
	{
		$libpf_slp = new libpf_slp();
		
		$OLEInfoArr = $libpf_slp->GET_OLE_ASSIGNED_RECORD_INFO($StudentID, $AcademicYearIDArr, '', $OrderByELE=1);
		$numOfOLE = count($OLEInfoArr);
		
		$OLEAssoArr = array();
		for ($i=0; $i<$numOfOLE; $i++)
		{
			$thisCategory = $OLEInfoArr[$i]['CategoryNameEn'];
			$thisTitle = $OLEInfoArr[$i]['Title'];
			$thisRole = $OLEInfoArr[$i]['Role'];
			$thisELE = $OLEInfoArr[$i]['ELE'];
			$thisAcademicYearNameEn = $OLEInfoArr[$i]['AcademicYearNameEn'];
			$thisIntExt = $OLEInfoArr[$i]['IntExt'];
			
			if ($thisIntExt == 'EXT') {
				$thisTargetCategory = 'Award';		// Show Ext items in Activity Section
			}
			else if ($thisCategory == 'Award') {		// Do not show original Award records
				continue;
			}
			else if ($thisCategory == 'Membership') {	// Show Membership Category records in the Membership Section
				$thisTargetCategory = 'Membership';
			}
			else {										// Show all other records in the Activity Section
				$thisTargetCategory = 'Activity';
			}
				
			if ($WithYearGrouping)
			{
				### Check if this Item can be grouped with other existing items (with the same Title, Role and OLE)
				$numOfExistingItem = count((array)$OLEAssoArr[$thisTargetCategory]);
				
				$thisIsMatched = false;
				for ($j=0; $j<$numOfExistingItem; $j++)
				{
					$thisItemTitle = $OLEAssoArr[$thisTargetCategory][$j]['Title'];
					$thisItemRole = $OLEAssoArr[$thisTargetCategory][$j]['Role'];
					$thisItemELE = $OLEAssoArr[$thisTargetCategory][$j]['ELE'];
					$thisItemAcademicYearNameEn = $OLEAssoArr[$thisTargetCategory][$j]['AcademicYearNameEn'];
					
					# Match
					if (strtolower($thisTitle)==strtolower($thisItemTitle) && strtolower($thisRole)==strtolower($thisItemRole) && strtolower($thisELE)==strtolower($thisItemELE))
					{
						$thisIsMatched = true;
						
						$thisAcademicYearArr = array();
						$thisAcademicYearArr[] = $thisAcademicYearNameEn;
						$thisAcademicYearArr[] = $thisItemAcademicYearNameEn;
						$OLEAssoArr[$thisTargetCategory][$j]['AcademicYearNameEn'] = $this->Get_Year_Range($thisAcademicYearArr);
						break;
					}
				}
				
				# Not Match => Add to the display array directly
				if ($thisIsMatched == false)
					(array)$OLEAssoArr[$thisTargetCategory][] = $OLEInfoArr[$i];
			}
			else
			{
				### Without Year Grouping => Add to the display array directly
				(array)$OLEAssoArr[$thisTargetCategory][] = $OLEInfoArr[$i];
			}
		}
		
		return $OLEAssoArr;
	}
	
	/*
	 * $AcademicYearArr[0] = '2009-2010';
	 * $AcademicYearArr[1] = '2010-2011';
	 * $AcademicYearArr[2] = '2011-2012';
	 * 
	 * return '2009-2012'
	 */
	function Get_Year_Range($AcademicYearArr)
	{
		$FirstYear = '';
		$LastYear = '';
		$numOfAcademicYear = count($AcademicYearArr);
		for ($i=0; $i<$numOfAcademicYear; $i++)
		{
			$thisAcademicYear = $AcademicYearArr[$i];
			$thisYearNameArr = explode('-', $thisAcademicYear);
			$thisYearStart = $thisYearNameArr[0];
			$thisYearEnd = $thisYearNameArr[1];
			
			if ($FirstYear == '' || $thisYearStart < $FirstYear)
				$FirstYear = $thisYearStart;
			if ($LastYear == '' || $thisYearEnd > $LastYear)
				$LastYear = $thisYearEnd;
		}
		
		return $FirstYear.'-'.$LastYear;
	}
	
	public function Get_Print_Header()
	{
		global $PATH_WRT_ROOT, $Lang;
		
		include_once($PATH_WRT_ROOT.'includes/libinterface.php');
		$linterface = new interface_html();
		
		$x = '';
		$x .= '<link href="'.$PATH_WRT_ROOT.'includes/portfolio25/customize/raimondi_college/css/print.css" rel="stylesheet" type="text/css" />'."\n";
		$x .= '<style type="text/css" media="print">'."\n";
			$x .= '.print_hide {display:none;}'."\n";
		$x .= '</style>'."\n";
		$x .= '<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">'."\n";
			$x .= '<tr><td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</span></td></tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	public function Get_School_Logo_Image($Height)
	{
		global $PATH_WRT_ROOT;
		
		$SchoolLogoPath = $PATH_WRT_ROOT."includes/portfolio25/customize/raimondi_college/images/logo.jpg";
		return '<img border="0" height="'.$Height.'" src="'.$SchoolLogoPath.'" />'."\n";
	}
	
	public function Get_Non_Academic_Report_HTML($AcademicYearInfoArr, $StudentInfoArr, $WithSelfAccount, $IssueDate)
	{
		$libpf_slp = new libpf_slp();
		
		### Get the Report Year for display
		$AcademicYearNameArr = Get_Array_By_Key($AcademicYearInfoArr, 'YearNameEN');
		$ReportYear = $this->Get_Year_Range($AcademicYearNameArr);		
		
		### Generate Report for each Student
		$x = '';
		$WithYearGrouping = ($WithSelfAccount)? true : false;
		$AcademicYearIDArr = Get_Array_By_Key($AcademicYearInfoArr, 'AcademicYearID');
		$numOfStudent = count($StudentInfoArr);
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID = $StudentInfoArr[$i]['UserID'];
			
			### Get Student Self Account Info
			if ($WithSelfAccount)
				$thisSelfAccountInfoArr = $libpf_slp->GET_SELF_ACCOUNT_LIST($thisStudentID, $DefaultSAOnly=1);
			
			### Get and Consolidate OLE Info
			$thisOLEAssoArr = $this->Get_Student_OLE_Consolidated_Array($thisStudentID, $AcademicYearIDArr, $WithYearGrouping);
			
			### Generate Student Report
			$thisPageBreak = ($i < $numOfStudent - 1)? true : false;
			$x .= '<div class="container">'."\n";
				if ($i==0)
					$x .= '<br />'."\n";
				$x .= $this->Get_Report_Layout($StudentInfoArr[$i], $thisPageBreak, $ReportYear, $thisOLEAssoArr, $WithSelfAccount, $thisSelfAccountInfoArr, $IssueDate);
			$x .= '</div>'."\n";
		}
		
		return $x;
	}
	
	public function Get_Report_Layout($StudentInfoArr, $ApplyPageBreak, $ReportYear, $OLEInfoArr, $WithSelfAccount, $SelfAccountInfoArr, $IssueDate)
	{
		$PageBreak = ($ApplyPageBreak)? 'style="page-break-after:always"' : '';
		$TableTopHeight = $this->Get_Height('TableTop');
		$WithYearGrouping = ($WithSelfAccount)? true : false;
		
		if ($WithSelfAccount)
		{
			$Page1TableStyle = '';
			if($OLEInfoArr['Membership']||$OLEInfoArr['Activity']||$OLEInfoArr['Award']){
				$Page1TableStyle = 'page-break-after:always'; 
			}
			$Page1 = '';
			$Page1 .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top" style="'.$Page1TableStyle.'">'."\n";
				$Page1 .= '<tr><td>'.$this->Get_Report_Header($ReportYear).'</td></tr>'."\n";
				$Page1 .= '<tr><td>'.$this->Get_Empty_Image_Div('19px').'</td></tr>'."\n";	// 0.5cm
				$Page1 .= '<tr><td>'.$this->Get_Student_Info_Table($StudentInfoArr).'</td></tr>'."\n";
				if($OLEInfoArr['Membership']||$OLEInfoArr['Activity']||$OLEInfoArr['Award']){
					$Page1 .= '<tr><td>'.$this->Get_Empty_Image_Div('38px').'</td></tr>'."\n";	// 1cm
				}
				if($OLEInfoArr['Membership']){
					$Page1 .= '<tr><td>'.$this->Get_Membership_Table($OLEInfoArr).'</td></tr>'."\n";
				}
				if($OLEInfoArr['Activity']||$OLEInfoArr['Award']){
					if($OLEInfoArr['Membership']){
						$Page1 .= '<tr><td>'.$this->Get_Empty_Image_Div('25px').'</td></tr>'."\n";
					}
					$Page1 .= '<tr><td>'.$this->Get_Activities_And_Awards_Table($OLEInfoArr, $WithYearGrouping).'</td></tr>'."\n";
				}
			$Page1 .= '</table>'."\n";
			
			$UpperTable = '';
			$UpperTable .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top">'."\n";
				$UpperTable .= '<tr><td>'.$this->Get_Self_Account_Table($SelfAccountInfoArr).'</td></tr>'."\n";
			$UpperTable .= '</table>'."\n";
			
			$LowerTable = '';
			$LowerTable .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="bottom">'."\n";
				$UpperTable .= '<tr><td>'.$this->Get_Signature_Table($IssueDate).'</td></tr>'."\n";
			$LowerTable .= '</table>'."\n";
			
			$Page2 = '';
			$Page2 .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top" '.$PageBreak.'>'."\n";
				if($Page1TableStyle){ //If Page 1 has content, repeat student info in page 2, if no content, page 2 doesnot need to show student info
					$Page2 .= '<tr><td>'.$this->Get_Report_Header($ReportYear).'</td></tr>'."\n";;
					$Page2 .= '<tr><td>'.$this->Get_Empty_Image_Div('19px').'</td></tr>'."\n";	// 0.5cm
					$Page2 .= '<tr><td>'.$this->Get_Student_Info_Table($StudentInfoArr).'</td></tr>'."\n";
				}
				$Page2 .= '<tr><td>'.$this->Get_Empty_Image_Div('19px').'</td></tr>'."\n";	// 0.5cm
				$Page2 .= '<tr><td style="vertical-align:top;height:'.$TableTopHeight.'px;">'.$UpperTable.'</td></tr>'."\n";
				$Page2 .= '<tr><td style="vertical-align:bottom;">'.$LowerTable.'</td></tr>'."\n";
			$Page2 .= '</table>'."\n";
			
			$x = '';
			$x .= $Page1;
			$x .= $Page2;
		}
		else
		{
			$UpperTable = '';
			$UpperTable .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top">'."\n";
				$UpperTable .= '<tr><td>'.$this->Get_Report_Header($ReportYear).'</td></tr>'."\n";
				$UpperTable .= '<tr><td>'.$this->Get_Empty_Image_Div('19px').'</td></tr>'."\n";	// 0.5cm
				if($OLEInfoArr['Membership']||$OLEInfoArr['Activity']||$OLEInfoArr['Award']){
					$UpperTable .= '<tr><td>'.$this->Get_Empty_Image_Div('38px').'</td></tr>'."\n";	// 1cm
				}
				if($OLEInfoArr['Membership']){
					$UpperTable .= '<tr><td>'.$this->Get_Membership_Table($OLEInfoArr).'</td></tr>'."\n";
				}
				if($OLEInfoArr['Activity']||$OLEInfoArr['Award']){
					if($OLEInfoArr['Membership']){
						$UpperTable .= '<tr><td>'.$this->Get_Empty_Image_Div('25px').'</td></tr>'."\n";
					}
					$UpperTable .= '<tr><td>'.$this->Get_Activities_And_Awards_Table($OLEInfoArr, $WithYearGrouping).'</td></tr>'."\n";
				}
			$UpperTable .= '</table>'."\n";
			
			$LowerTable = '';
			$LowerTable .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="bottom">'."\n";
				$UpperTable .= '<tr><td>'.$this->Get_Signature_Table($IssueDate).'</td></tr>'."\n";
			$LowerTable .= '</table>'."\n";
			
			$x = '';
			$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" valign="top" '.$PageBreak.'>'."\n";
				$x .= '<tr><td style="vertical-align:top;height:'.$TableTopHeight.'px;">'.$UpperTable.'</td></tr>'."\n";
				$x .= '<tr><td style="vertical-align:bottom;">'.$LowerTable.'</td></tr>'."\n";
			$x .= '</table>'."\n";
		}
		
		return $x;
	}
	
	public function Get_Report_Header($ReportYear)
	{
		global $Lang;
		
		$SchoolLogoImg = $this->Get_School_Logo_Image('120px');
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="width:190px;text-align:center;vertical-align:top;">'.$SchoolLogoImg.'</td>'."\n";
				$x .= '<td style="text-align:center;vertical-align:top;">'."\n";
					$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">'."\n";
						$x .= '<tr><td class="text_pt18 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['SchoolNameEn'].'</td></tr>'."\n";
						$x .= '<tr><td class="text_pt18 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['SchoolNameCh'].'</td></tr>'."\n";
						$x .= '<tr><td class="text_pt12 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TitleEn'].'</td></tr>'."\n";
						$x .= '<tr><td class="text_pt12 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TitleCh'].'</td></tr>'."\n";
						$x .= '<tr><td class="text_pt12 text_bold">'.$ReportYear.'</td></tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
				$x .= '<td style="width:190px;text-align:center;vertical-align:top;">'."\n";
					$x .= '<table width="100%" border="0" cellpadding="2" cellspacing="0" align="center">'."\n";
						$x .= '<tr><td class="text_pt10_5 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['AddressEn'].'</td></tr>'."\n";
						$x .= '<tr><td class="text_pt10_5 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['AddressCh'].'</td></tr>'."\n";
						$x .= '<tr><td class="text_pt10_5 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TelephoneEn'].'</td></tr>'."\n";
						$x .= '<tr><td class="text_pt10_5 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['FaxEn'].'</td></tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	public function Get_Student_Info_Table($StudentInfoArr)
	{
		global $Lang;
		
		### Get Student Info for Display
		$StudentNameEn = $StudentInfoArr['EnglishName'];
		$StudentNameCh = $StudentInfoArr['ChineseName'];
		$StudentGender = $StudentInfoArr['Gender'];
		$GenderEn = $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['Gender'][$StudentGender]['En'];
		$GenderCh = $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['Gender'][$StudentGender]['Ch'];
		$ClassNameEn = $StudentInfoArr['ClassTitleEN'];
		$ClassNameCh = $StudentInfoArr['ClassTitleB5'];
		$ClassNumber = $StudentInfoArr['ClassNumber'];
		
		# Convert DOB format to "07 September 1997"
		$DateOfBirth = $StudentInfoArr['DateOfBirth'];
		$DateOfBirth = substr($DateOfBirth, 0, 10);
		
		if($DateOfBirth=='')
		{
			$DateOfBirth = $this->EmptySymbol;
		}
		else
		{
			$DateOfBirth = date('d F Y', strtotime($DateOfBirth));
		}
				
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="text_pt8" style="text-align:left;">'."\n";
			### English
			$x .= '<tr>'."\n";
				// Name
				$x .= '<td style="width:20%;">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['StudentNameEn'].':</td>'."\n";
				$x .= '<td style="width:41%;">'.$StudentNameEn.'</td>'."\n";
				
				// Sex
				$x .= '<td style="width:5%;">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['SexEn'].':</td>'."\n";
				$x .= '<td style="width:9%;">'.$GenderEn.'</td>'."\n";
				
				// Class
				$x .= '<td style="width:7%;">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassEn'].':</td>'."\n";
				$x .= '<td style="width:6%;">'.$ClassNameEn.'</td>'."\n";
				
				// Class No.
				$x .= '<td style="width:7%;">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassNoEn'].':</td>'."\n";
				$x .= '<td style="width:5%;">'.$ClassNumber.'</td>'."\n";
				
				// DOB
				/*
				$x .= '<td style="width:10%;">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['DateOfBirthEn'].':</td>'."\n";
				$x .= '<td style="width:15%;">'.$DateOfBirth.'</td>'."\n";
				*/
			$x .= '</tr>'."\n";
			
			### Chinese
			$x .= '<tr>'."\n";
				// Name
				$x .= '<td>'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['StudentNameCh'].':</td>'."\n";
				$x .= '<td>'.$StudentNameCh.'</td>'."\n";
				
				// Sex
				$x .= '<td>'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['SexCh'].':</td>'."\n";
				$x .= '<td>'.$GenderCh.'</td>'."\n";
				
				// Class
				$x .= '<td>'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassCh'].':</td>'."\n";
				$x .= '<td>&nbsp;</td>'."\n";
				
				// Class No.
				$x .= '<td>'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClassNoCh'].':</td>'."\n";
				$x .= '<td>&nbsp;</td>'."\n";
				
				// DOB
				/*
				$x .= '<td>'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['DateOfBirthCh'].':</td>'."\n";
				$x.= '<td>&nbsp;</td>'."\n";
				*/
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Membership_Table($OLEInfoArr)
	{
		global $Lang;
		$MaxNumOfDisplayItem = $this->Get_Max_Display_Item('Membership');
		$MembershipInfoArr = $OLEInfoArr['Membership'];
			
		$x = '';	
		if(count($OLEInfoArr)==0)
		{
			return $x;
		}
		else
		{
			$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="text_pt10 text_bold" style="text-align:left;">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['MembershipEn'].' ('.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['MembershipCh'].'):'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0" class="report_border text_pt10" style="text-align:left;">'."\n";
				### Title
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td style="width:5%;">&nbsp;</td>'."\n";
					
						# Club / Term
						$thisTitle = '';
						$thisTitle .= $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClubEn'].' / '.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TeamEn'];
						$thisTitle .= ' ';
						$thisTitle .= '('.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ClubCh'].' / '.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['TeamCh'].')';
						$x .= '<td  style="width:47%;" class="text_bold" style="text-align:center;">'.$thisTitle.'</td>'."\n";
						
						# Year
						$thisTitle = $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['YearEn'].' ('.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['YearCh'].')';
						$x .= '<td  style="width:25%;" class="text_bold" style="text-align:center;">'.$thisTitle.'</td>'."\n";
						
						# Post
						$thisTitle = $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['PostEn'].' ('.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['PostCh'].')';
						$x .= '<td  style="width:23%;" class="text_bold" style="text-align:center;">'.$thisTitle.'</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				
				### Content
				$x .= '<tbody>'."\n";
					for ($i=0; $i<$MaxNumOfDisplayItem; $i++)
					{
						$thisMembershipInfoArr = $MembershipInfoArr[$i];
						if (!isset($thisMembershipInfoArr))
							continue;
							
						$thisIndex = $i + 1; 
						$thisTitle = $thisMembershipInfoArr['Title'];
						$thisYear = $thisMembershipInfoArr['AcademicYearNameEn'];
						$thisPost = $thisMembershipInfoArr['Role'];
						
						$x .= '<tr>'."\n";
							$x .= '<td style="text-align:center;">'.$thisIndex.'.</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisTitle.'</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisYear.'</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisPost.'</td>'."\n";
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
		}		
		
		
		return $x;
	}
	
	function Get_Activities_And_Awards_Table($OLEInfoArr, $WithYearGrouping)
	{
		global $Lang;
		
		$MaxNumOfDisplayItem = ($WithYearGrouping)? $this->Get_Max_Display_Item('ActivitiesAndAwardsWithGrouping') : $this->Get_Max_Display_Item('ActivitiesAndAwardsWithoutGrouping');
		$ActivityInfoArr = $OLEInfoArr['Activity'];
		$numOfActivity = count($ActivityInfoArr);
		$AwardInfoArr = $OLEInfoArr['Award'];
		$numOfAward = count($AwardInfoArr);
		$numOfDisplayedItem = 0;
		
		$x = '';	
		if(count($OLEInfoArr)==0)
		{
			return $x;
		}
		else
		{
			$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="text_pt10 text_bold" style="text-align:left;">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ActivitiesAndAwardsEn'].' ('.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ActivitiesAndAwardsCh'].'):'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" class="report_border text_pt9" style="text-align:left;">'."\n";
				$x .= '<thead>'."\n";
					### Title English
					$x .= '<tr style="text-align:center;">'."\n";
						$x .= '<td style="width:3%;">&nbsp;</td>'."\n";
					
						# Event Title
						$x .= '<td  style="width:27%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['EventTitleEn'].'</td>'."\n";
						
						# Organized By
						$x .= '<td  style="width:25%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['OrganizedByEn'].'</td>'."\n";
						
						# Year
						$x .= '<td  style="width:15%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['YearEn'].'</td>'."\n";
						
						# Role
						$x .= '<td  style="width:15%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['RoleEn'].'</td>'."\n";
						
						# OLE
						$x .= '<td  style="width:15%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['OLEEn'].'</td>'."\n";
					$x .= '</tr>'."\n";
					
					### Title Chinese
					$x .= '<tr style="text-align:center;">'."\n";
						$x .= '<td style="width:5%;padding:0px;">&nbsp;</td>'."\n";
					
						# Event Title
						$x .= '<td  style="width:25%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['EventTitleCh'].'</td>'."\n";
						
						# Organized By
						$x .= '<td  style="width:25%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['OrganizedByCh'].'</td>'."\n";
						
						# Year
						$x .= '<td  style="width:15%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['YearCh'].'</td>'."\n";
						
						# Role
						$x .= '<td  style="width:15%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['RoleCh'].'</td>'."\n";
						
						# OLE
						$x .= '<td  style="width:15%;padding:0px;" class="text_pt10 text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['OLECh'].'</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				
				### Content
				$x .= '<tbody>'."\n";
					# Empty Rows
					$x .= '<tr><td colspan="6">'.$this->Get_Empty_Image('23px').'</td></tr>'."\n";
					
					# Activities Title
					if($numOfActivity){
						$x .= '<tr>'."\n";
							$x .= '<td colspan="6" class="text_pt10 text_bold">'."\n";
								$x .= $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ActivitiesEn'].' ('.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['ActivitiesCh'].'):'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					}
					
					# Activities Content
					for ($i=0; $i<$numOfActivity; $i++)
					{
						$thisActivityInfoArr = $ActivityInfoArr[$i];
							
						if ($numOfDisplayedItem >= $MaxNumOfDisplayItem)
							break;
							
						$thisIndex = $i + 1; 
						$thisTitle = $thisActivityInfoArr['Title'];
						$thisOrganizedBy = $thisActivityInfoArr['Organization'];
						$thisYear = $thisActivityInfoArr['AcademicYearNameEn'];
						$thisPost = $thisActivityInfoArr['Role'];
						
						# Display the first OLE only
						$thisELE = $thisActivityInfoArr['ELE'];
						$thisELEArr = explode(',', $thisELE);
						$thisELE = $thisELEArr[0];
						$thisELE = str_replace('[', '', $thisELE);
						$thisELE = str_replace(']', '', $thisELE);
						
						$x .= '<tr style="vertical-align:top;">'."\n";
							$x .= '<td style="text-align:center;">'.$thisIndex.'.</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisTitle.'</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisOrganizedBy.'</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisYear.'</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisPost.'</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisELE.'</td>'."\n";
						$x .= '</tr>'."\n";
						
						$numOfDisplayedItem++;
					}
					
					# Awards Title
					if($numOfAward){
					$x .= '<tr>'."\n";
						$x .= '<td colspan="6" class="text_pt10 text_bold">'."\n";
							$x .= $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['AwardsEn'].' ('.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['AwardsCh'].'):'."\n";
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
					}
					
					# Awards Content
					for ($i=0; $i<$numOfAward; $i++)
					{
						$thisAwardInfoArr = $AwardInfoArr[$i];
							
						if ($numOfDisplayedItem >= $MaxNumOfDisplayItem)
							break;
							
						$thisIndex = $i + 1; 
						$thisTitle = $thisAwardInfoArr['Title'];
						$thisOrganizedBy = $thisAwardInfoArr['Organization'];
						$thisYear = $thisAwardInfoArr['AcademicYearNameEn'];
						$thisPost = $thisAwardInfoArr['Role'];
						
						# Display the first OLE only
						$thisELE = $thisAwardInfoArr['ELE'];
						$thisELEArr = explode(',', $thisELE);
						$thisELE = $thisELEArr[0];
						$thisELE = str_replace('[', '', $thisELE);
						$thisELE = str_replace(']', '', $thisELE);
						
						$x .= '<tr style="vertical-align:top;">'."\n";
							$x .= '<td style="text-align:center;">'.$thisIndex.'.</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisTitle.'</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisOrganizedBy.'</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisYear.'</td>'."\n";
							$x .= '<td style="text-align:center;">'.$thisPost.'</td>'."\n";
							//$x .= '<td>'.$thisELE.'</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
						$x .= '</tr>'."\n";
						
						$numOfDisplayedItem++;
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
		}
		return $x;
	}
	
	function Get_Signature_Table($IssueDate)
	{
		global $Lang;
		
		$SignatureArr = $this->Get_Signature_Item();
		$numOfSignature = count($SignatureArr);
		
		$SideWidth = 2;
		$BlankWidth = 6;
		$SignatureWidth = floor((100 - ($SideWidth * 2) - ($BlankWidth * ($numOfSignature - 1))) / $numOfSignature);
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="text_pt10" style="text-align:center;">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="height:100px; width:'.$SideWidth.'%;">&nbsp;</td>'."\n";
				for ($i=0; $i<$numOfSignature; $i++)
				{
					$thisSignatureKey = $SignatureArr[$i];
					if ($thisSignatureKey == 'DateOfIssue' && $IssueDate != '')
					{
						$thisContent = date('d F Y', strtotime($IssueDate));
					}
					else
					{
						$thisContent = '&nbsp;';
					}
					
					$x .= '<td style="width:'.$SignatureWidth.'%;" valign="bottom">'.$thisContent.'</td>'."\n";
					
					if ($i < $numOfSignature - 1)
						$x .= '<td style="width:'.$BlankWidth.'%;">&nbsp;</td>'."\n";
				}
				$x .= '<td style="width:'.$SideWidth.'%;">&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
			
			$x .= '<tr>'."\n";
				$x .= '<td>&nbsp;</td>'."\n";
				for ($i=0; $i<$numOfSignature; $i++)
				{
					$thisSignatureKey = $SignatureArr[$i];
					$thisTitleEn = $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport'][$thisSignatureKey.'En'];
					$thisTitleCh = $Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport'][$thisSignatureKey.'Ch'];
					
					$x .= '<td class="border_top">'.$thisTitleEn.'<br />'.$thisTitleCh.'</td>'."\n";
					
					if ($i < $numOfSignature - 1)
						$x .= '<td>&nbsp;</td>'."\n";
				}
				$x .= '<td>&nbsp;</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Self_Account_Table($SelfAccountInfoArr)
	{
		global $Lang;
		
		$StudentSelfAccount = $SelfAccountInfoArr[0]['Details'];
		$TableHeight = $this->Get_Height('SelfAccountTable');
		
		$x = '';
		$x .= '<table height="680px" width="100%" border="0" cellpadding="2" cellspacing="0" class="report_border text_pt10" style="text-align:left;">'."\n";
			$x .= '<tr><td style="padding-left:5px;" class="text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['StudentSelfAccountEn'].'</td></tr>'."\n";
			$x .= '<tr><td style="padding-left:5px;" class="text_bold">'.$Lang['iPortfolio']['Reports']['NonAcademicPerformanceReport']['StudentSelfAccountCh'].'</td></tr>'."\n";
			$x .= '<tr><td style="padding-left:5px;" ><img src="/images/spacer.gif" height="5" width="3" border="0" /></td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td style="padding-left:5px;padding-right:5px;vertical-align:top;height:100%;" class="arial_text_pt9_5">'."\n";
					$x .= '<p style="width:100%;text-align:justify;line-height:120%;font-family: times; font-size:12pt">'.$StudentSelfAccount.'</p>'."\n";

					//some student self account cannot display by below line 
					//$x .= '--><p style="width:100%;height:100%;text-align:justify;line-height:200%;">'.$StudentSelfAccount.'</p><--'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	
	function Get_Empty_Image_Div($Height)
	{
		return "<div>".$this->Get_Empty_Image($Height)."</div>";
	}
	
	function Get_Empty_Image($Height)
	{
		global $PATH_WRT_ROOT;
		$emptyImagePath = $PATH_WRT_ROOT."images/2009a/10x10.gif";
		
		return "<img src='".$emptyImagePath."' height='".$Height."'>";
	}
}
?>