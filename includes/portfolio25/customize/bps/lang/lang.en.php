<?php
/**
 * Modifying By: Max
 */
$Lang["Cust_Bps"]["NonAcademicPerformanceRecord"] = "Non-academic Performance Record";
$Lang["Cust_Bps"]["Name"] = "Name";
$Lang["Cust_Bps"]["RegistrationNumber"] = "Registration Number";
$Lang["Cust_Bps"]["Class"] = "Class";
$Lang["Cust_Bps"]["ClassNo"] = "Class no.";
$Lang["Cust_Bps"]["Club"] = "Club";
$Lang["Cust_Bps"]["Team"] = "Team";
$Lang["Cust_Bps"]["Period"] = "Period";
$Lang["Cust_Bps"]["Post"] = "Post";
$Lang["Cust_Bps"]["Performance"] = "Performance";
$Lang["Cust_Bps"]["Activity"] = "Activity";
$Lang["Cust_Bps"]["Service"] = "Service";
$Lang["Cust_Bps"]["Organizer"] = "Organizer";
$Lang["Cust_Bps"]["Title"] = "Title";
$Lang["Cust_Bps"]["Award"] = "Award";
$Lang["Cust_Bps"]["Sponsor"] = "Sponsor";
$Lang["Cust_Bps"]["Donor"] = "Donor";
$Lang["Cust_Bps"]["Membership"] = "Membership";
$Lang["Cust_Bps"]["AS_by_School"] = "Activities or Services organized by/via the school";
$Lang["Cust_Bps"]["InternalCompetitions"] = "Internal Competitions";
$Lang["Cust_Bps"]["ExternalCompetitions"] = "External Competitions";
$Lang["Cust_Bps"]["InternalAndExternalCompetitions"] = "Internal And External Competitions";
$Lang["Cust_Bps"]["Scholarships"] = "Scholarships";
$Lang["Cust_Bps"]["Principal"] = "Principal";
$Lang["Cust_Bps"]["DateOfIssue"] = "Date of Issue";
$Lang["Cust_Bps"]["NoRecord"] = "There is no record at the moment.";
$Lang["Cust_Bps"]["WholeYear"] = "Whole Year";
?>