<?php
/**
 * Modifying By: Max
 */
$Lang["Cust_Bps"]["NonAcademicPerformanceRecord"] = "非學術表現記錄";
$Lang["Cust_Bps"]["Name"] = "姓名";
$Lang["Cust_Bps"]["RegistrationNumber"] = "登記編號";
$Lang["Cust_Bps"]["Class"] = "班別";
$Lang["Cust_Bps"]["ClassNo"] = "班號";
$Lang["Cust_Bps"]["Club"] = "學會";
$Lang["Cust_Bps"]["Team"] = "隊伍";
$Lang["Cust_Bps"]["Period"] = "時段";
$Lang["Cust_Bps"]["Post"] = "職位";
$Lang["Cust_Bps"]["Performance"] = "表現";
$Lang["Cust_Bps"]["Activity"] = "活動";
$Lang["Cust_Bps"]["Service"] = "服務";
$Lang["Cust_Bps"]["Organizer"] = "籌辦單位";
$Lang["Cust_Bps"]["Title"] = "標題";
$Lang["Cust_Bps"]["Award"] = "獎項";
$Lang["Cust_Bps"]["Sponsor"] = "贊助";
$Lang["Cust_Bps"]["Donor"] = "捐贈者";
$Lang["Cust_Bps"]["Membership"] = "會籍";
$Lang["Cust_Bps"]["AS_by_School"] = "經學校籌辦的活動或服務";
$Lang["Cust_Bps"]["InternalCompetitions"] = "內部比賽";
$Lang["Cust_Bps"]["ExternalCompetitions"] = "外部比賽";
$Lang["Cust_Bps"]["InternalAndExternalCompetitions"] = "內部及外部比賽";
$Lang["Cust_Bps"]["Scholarships"] = "獎學金";
$Lang["Cust_Bps"]["Principal"] = "學校";
$Lang["Cust_Bps"]["DateOfIssue"] = "出版日期";
$Lang["Cust_Bps"]["NoRecord"] = "未有紀錄。";
$Lang["Cust_Bps"]["WholeYear"] = "全年";
?>