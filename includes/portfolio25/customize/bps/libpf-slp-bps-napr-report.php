<?
/**
 * [Modification Log] Modifying By: 
 * ***********************************************
 * 2015-12-10 (Omas) #G90302
 * - modified initializePrincipalInfoArray() - changed current principal
 * 2011-03-11 (Ivan) 2011-0217-1612-15128
 * 	- modified function generateMembershipTable() to cancel the hard-coding for always show "Whole Year" when AcademicYearID = 1 
 * ***********************************************
 */
include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/portfolio25/libpf-common.php");
include_once("libpf-slp-bps-napr.php");
include("lang/lang.$intranet_session_language.php");

class libpf_slp_bps_napr_report
{
	private $db;
	private $studentList;
	private $academicYearId;
	private $schoolInfoArray;
	private $principalInfoArray;
	private $issueDate;
	private $DisplayRecordNum;
	private $RemainRecordsNum;
	private $TableContentHtmlAry;
	
	public function __construct($ParStudentList, $ParAcademicYearId, $ParYearTermId=-1,$DisplayRecordNum) {
		$this->db = new libdb();
		$this->studentList = $ParStudentList;
		$this->academicYearId = $ParAcademicYearId;
		$this->yearTermId = $ParYearTermId;
		$this->initializeSchoolInfoArray();

		$this->DisplayRecordNum = $DisplayRecordNum;
// 		$this->RemainRecordsNum = $DisplayRecordNum;
		$this->issueDate = date("d-m-Y");
		$this->initializePrincipalInfoArray($ParAcademicYearId);

	}
	
	private function initializeSchoolInfoArray() {
		$this->schoolInfoArray["SchoolName"]["en"] = "BELILIOS PUBLIC SCHOOL";
		$this->schoolInfoArray["Address1"]["en"] = "51 TIN HAU TEMPLE ROAD";
		$this->schoolInfoArray["Address2"]["en"] = "HONG KONG";
		$this->schoolInfoArray["Tel"]["en"] = "Tel. No.: 2571 8018";
		$this->schoolInfoArray["Fax"]["en"] = "Fax. No.: 2578 5698";
		$this->schoolInfoArray["SchoolName"]["b5"] = "庇 理 羅 士 女 子 中 學";
		$this->schoolInfoArray["Address1"]["b5"] = "香 港 天 后 廟 道 五 十 一 號";
		$this->schoolInfoArray["Address2"]["b5"] = "&nbsp;";
		$this->schoolInfoArray["Tel"]["b5"] = "電　　話 : 二五七一八&#x25CB;一八";
		$this->schoolInfoArray["Fax"]["b5"] = "圖文傳真 : 二五七八五六九八";
		$this->schoolInfoArray["SchoolIcon"] = "<img src='BPS_logo_real.jpg' alt='BPS_logo_real.jpg' width='120px'/>";
	}
	
	private function initializePrincipalInfoArray($ParAcademicYearId) {		
		if( strtotime($this->issueDate) >= strtotime('03-09-2012') && strtotime($this->issueDate) <= strtotime('31-08-2015')  ){
			//agree with client if issues date > 20120903 , display this name . Although the print issues cannot input by user now 
			//case 2012-0913-1456-53147 Request to change principal name in Customized Report in iPortfolio
			//date format must be DD-MM-YYYY
			$this->principalInfoArray["PrincipalName"]["en"] = "CHAN Tsze-ying";
			$this->principalInfoArray["PrincipalName"]["b5"] = "CHAN Tsze-ying";
		}else if ( strtotime($this->issueDate) < strtotime('03-09-2012') ){
			$this->principalInfoArray["PrincipalName"]["en"] = "Ho Fung-ping, Wendy";
			$this->principalInfoArray["PrincipalName"]["b5"] = "Ho Fung-ping, Wendy";
		}
		else{
			// current principal #G90302
			$this->principalInfoArray["PrincipalName"]["en"] = "HO Yuk-yee";
			$this->principalInfoArray["PrincipalName"]["b5"] = "HO Yuk-yee";
		}
	}
	
	public function generateHtmlReport() {
		global $Lang;
		$reportTemplate = $this->getReportTemplate();
		$reportsArray = array();
		foreach($this->studentList as $key => $studentId) {
			$libpf_slp_bps_napr = new libpf_slp_bps_napr($studentId, $this->academicYearId, $this->yearTermId);
			$reportsArray[] = $this->generateAReport($reportTemplate."<div class='pageBreakAfter'>&nbsp;</div>", $libpf_slp_bps_napr);
		}
		$reports = implode("<div class='pageBreakAfter'>&nbsp;</div>", $reportsArray);
// 		if($_SESSION['UserID'] == '1'){
// 		    debug_pr($reportTemplate);
// 		    debug_pr('==================');
// 		}
		$Html = "<html><head><title>" . $Lang["Cust_Bps"]["NonAcademicPerformanceRecord"] . "</title><link href='report_style.css' rel='stylesheet' type='text/css'></head><body><div id='globalDiv'>$reports</div></body></html>";
		return $Html;
	}
	
	private function generateAReport($ParReportTemplate, $libpf_slp_bps_napr) {
	    $this->TableContentHtmlAry = array();
		$studentInfoTable = $this->generateStudentInfoTable($libpf_slp_bps_napr->getStudentInfo());
		$reportContentTableAry = $this->generateReportContentTable($libpf_slp_bps_napr);
		$reportHeader  = $this->generateReportHeader();
		$reportFooter = $this->generateReportFooter();
		
		$numOfContentTable = count($reportContentTableAry);
		$Html = '';
		for ($i=0; $i<$numOfContentTable; $i++) {
		    
		    $_reportContentTableAry = $reportContentTableAry[$i];
		    if($i == 0 ){
		        if($numOfContentTable == 1){
		            $Html .= str_replace(array("{{{ STUDENT_INFO }}}","{{{ REPORT_CONTENT }}}","{{{ REPORT_HEADER }}}",  "{{{ REPORT_FOOTER }}}"),array($studentInfoTable,$_reportContentTableAry,$reportHeader,$reportFooter),$ParReportTemplate);   
		        }else{
		            $Html .= str_replace(array("{{{ STUDENT_INFO }}}","{{{ REPORT_CONTENT }}}","{{{ REPORT_HEADER }}}",  "{{{ REPORT_FOOTER }}}"),array($studentInfoTable,$_reportContentTableAry,$reportHeader,''),$ParReportTemplate);
		            
		        }
		        
		    }else if($i+1 == $numOfContentTable){
		        $Html .= str_replace(array("{{{ STUDENT_INFO }}}","{{{ REPORT_CONTENT }}}","{{{ REPORT_HEADER }}}",  "{{{ REPORT_FOOTER }}}"),array('',$_reportContentTableAry,'',$reportFooter),$ParReportTemplate);	        
		    }else{
		        $Html .= str_replace(array("{{{ STUDENT_INFO }}}","{{{ REPORT_CONTENT }}}","{{{ REPORT_HEADER }}}", "{{{ REPORT_FOOTER }}}"),array('',$_reportContentTableAry,'',''),$ParReportTemplate);
		    }
		}
		
		return $Html;
	}
	
	private function generateReportHeader(){
	    $academicYearName = getAcademicYearInfoByAcademicYearId($this->academicYearId);
	    
	    $tableHeader = <<<HTMLEND
		<tr>
    		<td>
        		<table id="schoolInfoTable">
            		<tr>
                		<td class="schoolNameField Column1 chineseInfo">
                		{$this->schoolInfoArray["SchoolName"]["b5"]}
                		</td>
                		<td id="schoolIconField" rowspan="5">
                		{$this->schoolInfoArray["SchoolIcon"]}
                		</td>
                		<td class="schoolNameField Column3 englishInfo">
                		{$this->schoolInfoArray["SchoolName"]["en"]}
                		</td>
            		</tr>
            		<tr>
                		<td class="chineseInfo">
                		{$this->schoolInfoArray["Address1"]["b5"]}
                		</td>
                		<td class="englishInfo">
                		{$this->schoolInfoArray["Address1"]["en"]}
                		</td>
            		</tr>
            		<tr>
                		<td class="chineseInfo">
                		{$this->schoolInfoArray["Address2"]["b5"]}
                		</td>
                		<td class="englishInfo">
                		{$this->schoolInfoArray["Address2"]["en"]}
                		</td>
            		</tr>
            		<tr>
                		<td class="chineseInfo">
                		{$this->schoolInfoArray["Tel"]["b5"]}
                		</td>
                		<td class="englishInfo">
                		{$this->schoolInfoArray["Tel"]["en"]}
                		</td>
            		</tr>
            		<tr>
                		<td class="chineseInfo">
                		{$this->schoolInfoArray["Fax"]["b5"]}
                		</td>
                		<td class="englishInfo">
                		{$this->schoolInfoArray["Fax"]["en"]}
                		</td>
            		</tr>
            		<tr>
            		<td colspan="3" id="reportTitle">
            		{$Lang["Cust_Bps"]["NonAcademicPerformanceRecord"]} {$academicYearName}
            		</td>
            		</tr>
        		</table>
    		</td>
		</tr>
HTMLEND;
            		return $tableHeader;
	}
	private function generateReportFooter (){
	    global $intranet_session_language,$Lang;
	    $reportFooter = <<<HTMLEND
	    <tr>
    	    <td>
        	    <table id="principalAndDateTable">
            	    <tr height="100px">
                	    <td class="Column1">
                	    &nbsp;
                	    </td>
                	    <td class="Column2">
                	    &nbsp;
                	    </td>
                	    <td class="Column3">
                	    &nbsp;
                	    </td>
            	    </tr>
                	<tr valign="top">
                	    <td id="principalField">
                	    {$this->principalInfoArray["PrincipalName"][$intranet_session_language]}<br />
                	    ({$Lang["Cust_Bps"]["Principal"]})
                	    </td>
                	    <td>
                	    &nbsp;
                	    </td>
                	    <td id="dateField">
                	    {$Lang["Cust_Bps"]["DateOfIssue"]}: {$this->issueDate}
                	    </td>
            	    </tr>
        	    </table>
    	    </td>
	    </tr>
HTMLEND;
          return  $reportFooter;
	}
	private function generateStudentInfoTable($ParStudentInfo) {
		global $Lang, $intranet_session_language;
		$studentName = ($intranet_session_language == "en" ? $ParStudentInfo->EnglishName : $ParStudentInfo->ChineseName);
		$registrationNumber = substr_replace($ParStudentInfo->WebSamsRegNo, "", 0, 1);
		$class = $ParStudentInfo->ClassName;
		$classNumber = $ParStudentInfo->ClassNumber;
		
		$studentInfoTable = <<<HTMLEND
	<table id="studentInfoTable" >
      <tr>
        <td class="Column1" nowrap="nowrap">
          {$Lang["Cust_Bps"]["Name"]} : {$studentName}
        </td>
        <td class="Column2" align="center">
          {$Lang["Cust_Bps"]["RegistrationNumber"]} : {$registrationNumber}
        </td>
        <td class="Column3">
          {$Lang["Cust_Bps"]["Class"]} / {$Lang["Cust_Bps"]["ClassNo"]} : $class($classNumber)
        </td>
      </tr>
    </table>
HTMLEND;
		return $studentInfoTable;
	}
	private function generateMembershipTable($ParData) {
		global $Lang, $intranet_session_language;
		
		$table = "  <table class='categoryContentTable' id='membershipTable'>";
		
		$headerHtml = "     
        	  <caption>" . $Lang["Cust_Bps"]["Membership"] . "</caption>
              <tr>
                <th class='Column1'>
                  " . $Lang["Cust_Bps"]["Club"] . " / " . $Lang["Cust_Bps"]["Team"] . "
                </th>
                <!--<th class='Column2'>
                  " . $Lang["Cust_Bps"]["Period"] . "
                </th>-->
                <th class='Column3'>
                  " . $Lang["Cust_Bps"]["Post"] . "
                </th>
                <th class='Column4'>
                  " . $Lang["Cust_Bps"]["Performance"] . "
                </th>
              </tr>";
		
		if (count($ParData)>0) {
		    $table .= $headerHtml;		   
	        $i= 0;
	        $j = 1;   // $j is page number 
			foreach($ParData as $key => $element) {			      
			   $i++;	
			  
			   $table .= "
                      <tr>
                        <td>" . $element["ACTIVITYNAME"] . "</td>";
			   $table .= "<td>" . $element["ROLE"] . "</td>
                        <td>" . $element["PERFORMANCE"] . "</td>
                      </tr>";		
			
			    if($i == ($this->DisplayRecordNum)*$j){ // need add page break if reach page display number 	
			       
        			    if(count($ParData) == $i){        
        			        $table .= "</table>";
        			        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
      			        
        			    }else{
        			        $table .= "</table>";
        			        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
            			    $table = "<table class='categoryContentTable' id='membershipTable'>";
            			    $table .= $headerHtml;
            			    $j++;
        			  }       			
			    }  		 
        	}
        $table .= "</table>";
     
		} else {
			// do nothing
		}
		$remain =  ($this->DisplayRecordNum)*$j - count($ParData);
		$this->RemainRecordsNum  = $remain;

		if($this->RemainRecordsNum != 0){		  
		    $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => false);
		}

	}
	private function generateAS_by_SchoolTable($ParData) {
		global $Lang,$intranet_session_language;
		$table = "";
		
		if (count($ParData)>0) {
    		$table .= " <table class='categoryContentTable' id='AS_by_SchoolTable'> ";
    		$tableHeader = "
            <caption>" .$Lang["Cust_Bps"]["AS_by_School"] . "</caption>
              <tr>
                <th class='Column1'>
                  " . $Lang["Cust_Bps"]["Activity"] . " / " . $Lang["Cust_Bps"]["Service"] . "
                </th>
                <th class='Column2'>
                  " . $Lang["Cust_Bps"]["Organizer"] . "
                </th>
              </tr>";
    		$table .=$tableHeader;
		     $i = 0; 
		     $j = 1;
		  
			 foreach($ParData as $key => $element) {
			    $i++;
			   
				$table .= "
                  <tr>
                    <td>" . $element["TITLE"] . "</td>
                    <td>" . $element["ORGANIZATION"] . "</td>
                  </tr>";
// 				if($i == 16){
// 				    debug_pr($i);
// 				    debug_pr($j);
// 				}
				if($i == $this->RemainRecordsNum){ 
				    // reached remain or meet max number 
				    if(count($ParData) == $i){
				        $table .= "</table>";
				        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
				    }else{
    				    $table .= "</table>";
    				    $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
    				    $table = " <table class='categoryContentTable' id='AS_by_SchoolTable'> ";
    				    $table .= $tableHeader;
				    }
				    
				}else  if ($i-$this->RemainRecordsNum == ($this->DisplayRecordNum)*$j ){
				    if(count($ParData) == $i){
				        $table .= "</table>";
				        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
				    }else{
    				    $table .= "</table>";
    				    $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
    				    $table = " <table class='categoryContentTable' id='AS_by_SchoolTable'> ";
    				    $table .= $tableHeader;
    				    $j++;
				    }
				}
			 }
		  $table .= "</table>";
		} else {
			// do nothing
		}

		if($this->RemainRecordsNum >=  count($ParData)){

		    $remain = $this->RemainRecordsNum - count($ParData);
		}else{
		    $remain =  ($this->DisplayRecordNum)*$j + $this->RemainRecordsNum - count($ParData);
		}

 
		$this->RemainRecordsNum  = $remain;
		
		if($this->RemainRecordsNum != 0){
		    $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => false);
		}
		
		return $table;
	}

	private function generateInternalExternalCompetitionsTable($ParIntData, $ParExtData) {
		
		global $Lang;
		
		$mergedData = array_merge($ParIntData,$ParExtData);
		array_multisort($mergedData);

		$table = "";
		if (( count($ParIntData)+count($ParExtData) )>0) {
		    
        	$table = "<table class='categoryContentTable' id='internalExternalCompTable'>";
        	$tableHeader = "
            <caption>" . $Lang["Cust_Bps"]["InternalAndExternalCompetitions"] . "</caption>
                  <tr>
                    <th class='Column1'>
                      " . $Lang["Cust_Bps"]["Title"] . "
                    </th>
                    <th class='Column2'>
                      " . $Lang["Cust_Bps"]["Organizer"] . "
                    </th>
                    <th class='Column3'>
                      " . $Lang["Cust_Bps"]["Award"] . "
                    </th>
                  </tr>";
			if ($mergedData > 0) {
			    $table .=  $tableHeader;
			    $i = 0;
			    $j = 1;
			    foreach($mergedData as $key => $element) {
			        $i++;	
					$table .= "
                      <tr>
                        <td>" . $element["TITLE"] . "</td>
                        <td>" . $element["ORGANIZATION"] . "</td>
                        <td>" . $element["AWARD"] . "</td>
                      </tr>";
					
					if($i == $this->RemainRecordsNum)
					{ // reached remain or meet max number
					    if(count($mergedData) == $i){
					        $table .= "</table>";
// 					        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
					    }
					    else{
					        $table .= "</table>";
					        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
					        $table = " <table class='categoryContentTable' id='internalExternalCompTable'>";
					        $table .= $tableHeader;
					    
					    }
					}else if ($i-$this->RemainRecordsNum == ($this->DisplayRecordNum)*$j ){
					    if(count($mergedData) == $i){
					        $table .= "</table>";
// 					        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
					    }
					    else{
					        $table .= "</table>";
					        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
					        $table = " <table class='categoryContentTable' id='internalExternalCompTable'>";
					        $table .= $tableHeader;
					        $j++;
					    }
					}			
				}
			}
		$table .= "</table>";
		} else {
			// do nothing
		}

		if($this->RemainRecordsNum >=  count($mergedData)){
		    
		    $remain = $this->RemainRecordsNum - count($mergedData);
		}else{

		    $remain =  ($this->DisplayRecordNum)*$j + $this->RemainRecordsNum - count($mergedData);
		}
		
// 		$remain =  ($this->DisplayRecordNum)*$j + $this->RemainRecordsNum - count($ParData);
		$this->RemainRecordsNum  = $remain;
		
		if($this->RemainRecordsNum != 0){
		    $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => false);
		}	
	
		return $table;
	}
	
	private function generateScholarshipsTable($ParData) {
		global $Lang;
		$table = "";
		if (count($ParData)>0) {
		$table = "<table class='categoryContentTable' id='scholarshipTable'> ";
		$tableHeader = "<caption>" . $Lang["Cust_Bps"]["Scholarships"] . "</caption>
              <tr>
                <th class='Column1'>
                  " . $Lang["Cust_Bps"]["Title"] . "
                </th>
                <th class='Column2'>
                  " . $Lang["Cust_Bps"]["Sponsor"] . " / " . $Lang["Cust_Bps"]["Donor"] . "
                </th>
              </tr>";
		$table .= $tableHeader;
		$i=0;
		$j=1;
			foreach($ParData as $key => $element) {
			    $i++;
				$table .= "
                  <tr>
                    <td>" . $element["TITLE"] . "</td>
                    <td>" . $element["SPONSOR_OR_DONOR"] . "</td>
                  </tr>";
				
				
				if($i == $this->RemainRecordsNum)
				{ // reached remain or meet max number
				    if(count($ParData) == $i){
				        $table .= "</table>";
// 				        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
				    }
				    else{
				        $table .= "</table>";
				        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
				        $table = "<table class='categoryContentTable' id='scholarshipTable'> ";
				        $table .= $tableHeader;
				    }
				}else if ($i-$this->RemainRecordsNum == ($this->DisplayRecordNum)*$j ){
				    if(count($ParData) == $i){
				        $table .= "</table>";
// 				        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
				    }
				    else{
				        $table .= "</table>";
				        $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => true);
				        $table = "<table class='categoryContentTable' id='scholarshipTable'> ";
				        $table .= $tableHeader;
				        $j++;
				    }
				}	
				
			}
		$table .= "</table>";
		} else {
			// do nothing
		}
		
		if($this->RemainRecordsNum >=  count($ParData)){    
		    $remain = $this->RemainRecordsNum - count($ParData);
		}else{
		    $remain =  ($this->DisplayRecordNum)*$j + $this->RemainRecordsNum - count($ParData);
		}
		
		$remain =  ($this->DisplayRecordNum)*$j + $this->RemainRecordsNum - count($ParData);
		$this->RemainRecordsNum  = $remain;
		
		if($this->RemainRecordsNum != 0){
		    $this->TableContentHtmlAry[] = array('tableHtml' => $table, 'needPageBreak' => false);
		}

	}
	private function generateReportContentTable($libpf_slp_bps_napr) {
		$memberShipTable = $this->generateMembershipTable($libpf_slp_bps_napr->getMembershipArray());
		$AS_by_SchoolTable = $this->generateAS_by_SchoolTable($libpf_slp_bps_napr->getActivitiesOrServicesArray());
//		$internalCompetitionsTable = $this->generateInternalCompetitionsTable($libpf_slp_bps_napr->getInternalCompetitionsArray());
//		$externalCompetitionsTable = $this->generateExternalCompetitionsTable($libpf_slp_bps_napr->getExternalCompetitionsArray());
		$internalExternalCompetitionsTable = $this->generateInternalExternalCompetitionsTable($libpf_slp_bps_napr->getInternalCompetitionsArray(),$libpf_slp_bps_napr->getExternalCompetitionsArray());
		$scholarshipsTable = $this->generateScholarshipsTable($libpf_slp_bps_napr->getScholarshipsArray());

		$numOfTable = count($this->TableContentHtmlAry);
		
		$reportContentTable =  '<table id="reportContentTable">';
		for ($i=0;$i<$numOfTable;$i++) {
		    $_tableHtml = $this->TableContentHtmlAry[$i]['tableHtml'];
		    $_tablePageBreak = $this->TableContentHtmlAry[$i]['needPageBreak'];
		    if($_tablePageBreak){
		        $reportContentTable .= '<tr><td>'.$_tableHtml.'</td></tr>';
		        $reportContentTable .='</table>';	
		        $reportContentTableAry[] = $reportContentTable;
		        if(($i+1) != $numOfTable){
		            $reportContentTable = '<table id="reportContentTable">';		            
		        }	        
		    }else{
// 		        $reportContentTable = '';
// 		        $reportContentTable .= '<tr><td>'.$_tableHtml.'</td></tr>';
		        $reportContentTable .= '<tr><td>'.$_tableHtml.'</td></tr>';
		        if(($i+1) == $numOfTable){
		            $reportContentTable .='</table>';	
		            $reportContentTableAry[] = $reportContentTable;
		        }	
// 		        $reportContentTableAry[] = $reportContentTable;
		    }
		}		
// 		debug_pr($this->TableContentHtmlAry);

		return $reportContentTableAry;
	}
	
	private function getReportTemplate() {
		global $Lang, $intranet_session_language;
	
		
		$globalTable = <<<HTMLEND
    <table id="globalTable">
   {{{ REPORT_HEADER }}}
      <tr>
        <td>
          {{{ STUDENT_INFO }}}
        </td>
      </tr>
      <tr>
        <td class="aReportContent" valign="top">
          {{{ REPORT_CONTENT }}}
        </td>
      </tr>
{{{ REPORT_FOOTER }}}
    </table>
HTMLEND;
		return $globalTable;
	}
}
?>
