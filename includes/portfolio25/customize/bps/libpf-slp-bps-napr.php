<?php
include_once($intranet_root."/includes/portfolio25/iPortfolioConfig.inc.php");
include_once($intranet_root."/includes/libuser.php");
class libpf_slp_bps_napr {
	private $db;
	private $studentId;
	private $academicYearId;
	private $yearTermId;
	private $studentInfo;
	private $membershipArray;
	private $activitiesOrServicesArray;
	private $internalCompetitionsArray;
	private $externalCompetitionsArray;
	private $scholarshipsArray;
		
	const ID_COMPETITIONS = 1;
	const ID_ACTIVITIES = 2;
	const ID_SERVICES = 4;
	const ID_SCHOLARSHIPS = 7;
	
	private $MAX_LIMIT = 16;
	const LIMIT_MEMBER = 4;
	private $LIMIT_SERVICES = 0;	// Limit of services is depending on the others, i.e. $MAX_LIMIT - "Other limits"
	const LIMIT_COMPETITIONS_INT = 5;
	const LIMIT_COMPETITIONS_EXT = 3;
	const LIMIT_SCHOLARSHIPS = 3;
	

	public function __construct($ParStudentId, $ParAcademicYearId, $ParYearTermId=-1) {
		$this->db = new libdb();
		$this->LIMIT_SERVICES = $this->MAX_LIMIT;
		$this->studentId = $ParStudentId;
		$this->academicYearId = $ParAcademicYearId;
		$this->yearTermId = $ParYearTermId;
		$this->studentInfo = new libuser($this->studentId);
		$this->membershipArray = $this->generateMemberShipArray();
		$this->internalCompetitionsArray = $this->generateInternalCompetitionsArray();
		$this->externalCompetitionsArray = $this->generateExternalCompetitionsArray();
		$this->scholarshipsArray = $this->generateScholarshipsArray();
		
		# This one should be generated at last amount the result arrays since the size is determined by others
		$this->activitiesOrServicesArray = $this->generateActivitiesOrServicesArray();
	}
	
	private function groupMembershipRecordsToYears($ParOriginalResult, $ParYearTermIds) {
		global $intranet_session_language, $Lang;
//debug_r($ParOriginalResult);
		#############################################
		##	Start: Get the year term ids of an activity
		#############################################
		$membershipYearCount = array();
		foreach($ParOriginalResult as $key => $element) {
			$membershipYearCount[$element["ACTIVITYNAME"]][] = array("KEY"=>$key, "YEARTERMID"=>$element["YEARTERMID"]);
		}		
		#############################################
		##	End: Get the year term ids of an activity
		#############################################
		
		#############################################
		##	Start: Re-construct the original result
		#############################################
		$keyToSplice = array();
		foreach($membershipYearCount as $mKey => $mElement) {
			# if the number of year term ids of an activity is the same as the target years,
			# pad the information of the latter years to the 1st one 
			if (count($mElement)==count($ParYearTermIds)) {	# Handling the WHOLE YEAR membership
				$sizeOfmElement = count($mElement);
				$ParOriginalResult[$mElement[0]["KEY"]]["YEARTERMNAME".strtoupper($intranet_session_language)] = $Lang["Cust_Bps"]["WholeYear"];
				
				# Put the 1st role and performance into respective array for checking existance below
				$existedRole = array($ParOriginalResult[$mElement[0]["KEY"]]["ROLE"]);
				$existedPerformance = array($ParOriginalResult[$mElement[0]["KEY"]]["PERFORMANCE"]);
				for($i=1;$i<$sizeOfmElement; $i++) {
					# if the role is already displayed in the previous term, dont display again
					if (in_array($ParOriginalResult[$mElement[$i]["KEY"]]["ROLE"], $existedRole)) {
						// do nothing
					} else {
						$ParOriginalResult[$mElement[0]["KEY"]]["ROLE"] = $ParOriginalResult[$mElement[0]["KEY"]]["ROLE"].(empty($ParOriginalResult[$mElement[0]["KEY"]]["ROLE"])?"":" / ").$ParOriginalResult[$mElement[$i]["KEY"]]["ROLE"];
						$existedRole[] = $ParOriginalResult[$mElement[$i]["KEY"]]["ROLE"];
					}
					# if the performance is already displayed in the previous term, dont display again
					if (in_array($ParOriginalResult[$mElement[$i]["KEY"]]["PERFORMANCE"], $existedPerformance)) {
						// do nothing
					} else {
						$ParOriginalResult[$mElement[0]["KEY"]]["PERFORMANCE"] = $ParOriginalResult[$mElement[0]["KEY"]]["PERFORMANCE"].(empty($ParOriginalResult[$mElement[0]["KEY"]]["PERFORMANCE"])?"":" / ").$ParOriginalResult[$mElement[$i]["KEY"]]["PERFORMANCE"];
						$existedPerformance[] = $ParOriginalResult[$mElement[$i]["KEY"]]["PERFORMANCE"];
					}
					$keyToSplice[] = $mElement[$i]["KEY"];					
				}
			}
		}
		# remove the unwanted activities that with information already concatenated to the 1st one
		sort($keyToSplice);
		$keyToSplice = array_reverse($keyToSplice);
		
		foreach($keyToSplice as $kKey => $keyToKill) {
			array_splice($ParOriginalResult, $keyToKill, 1);
		}
		array_splice($ParOriginalResult, self::LIMIT_MEMBER);
		$resultArray = $ParOriginalResult;
		#############################################
		##	End: Re-construct the original result
		#############################################		
		return $resultArray;
	}
	private function generateMemberShipArray() {
		global $eclass_db, $intranet_db, $intranet_session_language;
		if ($this->yearTermId>0) {
			$cond_yearTerm = " AND AYT.YEARTERMID = {$this->yearTermId}";
		}
		$sql = "SELECT
					AY.YEARNAME" . strtoupper($intranet_session_language) . ",
					AYT.YEARTERMID,
					AYT.YEARTERMNAME" . strtoupper($intranet_session_language) . ",
					ACTS.ACTIVITYNAME,
					ACTS.ROLE,
					ACTS.PERFORMANCE
				FROM {$eclass_db}.ACTIVITY_STUDENT ACTS
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR AY
						ON ACTS.ACADEMICYEARID = AY.ACADEMICYEARID
					LEFT JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AYT
						ON AYT.YEARTERMID = ACTS.YEARTERMID
				WHERE
					ACTS.USERID = {$this->studentId}
					AND
					AY.ACADEMICYEARID = {$this->academicYearId}
					$cond_yearTerm
				ORDER BY
					ACTS.ACTIVITYNAME ASC
					";
//				LIMIT " . self::LIMIT_MEMBER;	// * WHEN USE THE groupMembershipRecordsToYears(), NO NEED TO SPECIFY LIMIT_MEMBER HERE
		$yearTermIds = $this->getYearTermIds();
//		echo $sql.'<br/>';
//		$resultArray = $this->db->returnArray($sql);
		$resultArray = $this->groupMembershipRecordsToYears($this->db->returnArray($sql),$yearTermIds);
//		DEBUG_R($yearTermIds);
		$this->LIMIT_SERVICES -= count($resultArray);
		return $resultArray;
	}
	private function generateActivitiesOrServicesArray() {
		global $eclass_db, $intranet_db, $intranet_session_language, $ipf_cfg;
		if ($this->yearTermId>0) {
			$join_yearTerm = "
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AYT
						ON AYT.YEARTERMID = OP.YEARTERMID";
			$cond_yearTerm = " AND AYT.YEARTERMID = {$this->yearTermId}";
		}
		$sql = "SELECT
					OP.TITLE,
					AY.YEARNAME" . strtoupper($intranet_session_language) . ",
					OP.STARTDATE,
					OP.ORGANIZATION
				FROM
					{$eclass_db}.OLE_STUDENT OS
					INNER JOIN {$eclass_db}.OLE_PROGRAM OP
						ON OS.PROGRAMID = OP.PROGRAMID
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR AY
						ON OP.ACADEMICYEARID = AY.ACADEMICYEARID
					$join_yearTerm
				WHERE
					OS.USERID = {$this->studentId}
					AND
					AY.ACADEMICYEARID = {$this->academicYearId}
					AND
					OP.CATEGORY IN (" . self::ID_SERVICES . "," . self::ID_ACTIVITIES . ")
					AND 
					(
						OS.RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"] . "
						OR
						OS.RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"] . "
					)
					AND
					(
						OS.SLPORDER IS NOT NULL AND OS.SLPORDER <> ''
					)
					$cond_yearTerm
				ORDER BY
					OP.TITLE ASC, OP.CATEGORY ASC
				LIMIT " . $this->LIMIT_SERVICES;
//				debug_r($sql);
		return $this->db->returnArray($sql);
	}
	private function generateInternalCompetitionsArray() {
		return $this->generateCompetitionsArrayByIntExt("INT");
	}
	private function generateExternalCompetitionsArray() {
		return $this->generateCompetitionsArrayByIntExt("EXT");
	}	
	private function generateCompetitionsArrayByIntExt($ParIntExt) {
		global $eclass_db, $intranet_db, $intranet_session_language, $ipf_cfg;
		if ($this->yearTermId>0) {
			$join_yearTerm = "
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AYT
						ON AYT.YEARTERMID = OP.YEARTERMID";
			$cond_yearTerm = " AND AYT.YEARTERMID = {$this->yearTermId}";
		}
		$LIMIT_COMPEITION = ($ParIntExt == "EXT"?self::LIMIT_COMPETITIONS_EXT:self::LIMIT_COMPETITIONS_INT);
		$sql = "SELECT
					OP.TITLE,
					OP.ORGANIZATION,
					IF ( (OS.ACHIEVEMENT IS NOT NULL AND OS.ACHIEVEMENT <> ''), OS.ACHIEVEMENT, OS.TEACHERCOMMENT ) AS AWARD/* Since the record is transferred from INTRANET_ENROL_EVENTSTUDENT, the performance is mapped to TeacherComment in OS */
				FROM
					{$eclass_db}.OLE_STUDENT OS
					INNER JOIN {$eclass_db}.OLE_PROGRAM OP
						ON OS.PROGRAMID = OP.PROGRAMID
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR AY
						ON OP.ACADEMICYEARID = AY.ACADEMICYEARID
					$join_yearTerm
				WHERE
					OS.USERID = {$this->studentId}
					AND
					AY.ACADEMICYEARID = {$this->academicYearId}
					AND
					OP.CATEGORY = " . self::ID_COMPETITIONS . "
					AND 
					(
						OS.RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"] . "
						OR
						OS.RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"] . "
					)
					AND
					(
						OS.SLPORDER IS NOT NULL AND OS.SLPORDER <> ''
					)
					AND OS.INTEXT = '$ParIntExt'
					$cond_yearTerm
				ORDER BY
					OP.TITLE ASC
				LIMIT " . $LIMIT_COMPEITION;
//		debug_r($sql);
		$resultArray = $this->db->returnArray($sql);
		$this->LIMIT_SERVICES -= count($resultArray);
		return $resultArray;
	}
	private function generateScholarshipsArray() {
		global $eclass_db, $intranet_db, $intranet_session_language, $ipf_cfg;
		if ($this->yearTermId>0) {
			$join_yearTerm = "
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AYT
						ON AYT.YEARTERMID = OP.YEARTERMID";
			$cond_yearTerm = " AND AYT.YEARTERMID = {$this->yearTermId}";
		}
		$sql = "SELECT
					OP.TITLE,
					IF ( (OS.ORGANIZATION IS NOT NULL AND OS.ORGANIZATION <> ''), OS.ORGANIZATION, OS.TEACHERCOMMENT ) AS SPONSOR_OR_DONOR/* Since the record is transferred from INTRANET_ENROL_EVENTSTUDENT, the performance is mapped to TeacherComment in OS */
				FROM
					{$eclass_db}.OLE_STUDENT OS
					INNER JOIN {$eclass_db}.OLE_PROGRAM OP
						ON OS.PROGRAMID = OP.PROGRAMID
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR AY
						ON OP.ACADEMICYEARID = AY.ACADEMICYEARID
					$join_yearTerm
				WHERE
					OS.USERID = {$this->studentId}
					AND
					AY.ACADEMICYEARID = {$this->academicYearId}
					AND
					OP.CATEGORY = " . self::ID_SCHOLARSHIPS . "
					AND 
					(
						OS.RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"] . "
						OR
						OS.RECORDSTATUS = " . $ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"] . "
					)
					AND
					(
						OS.SLPORDER IS NOT NULL AND OS.SLPORDER <> ''
					)
					$cond_yearTerm
				ORDER BY
					OP.TITLE ASC
				LIMIT " . self::LIMIT_SCHOLARSHIPS;
		$resultArray = $this->db->returnArray($sql);
		$this->LIMIT_SERVICES -= count($resultArray);
		return $resultArray;
	}
	private function getYearTermIds() {
		$sql = "SELECT YEARTERMID FROM {$intranet_db}.ACADEMIC_YEAR_TERM
				WHERE ACADEMICYEARID = {$this->academicYearId}";
		return $this->db->returnArray($sql);
	}
	
	
	#########################################
	##	Accessors
	#########################################
	public function getDb() {
		return $this->db;
	}
	public function getStudentId() {
		return $this->studentId;
	}
	public function getAcademicYearId() {
		return $this->academicYearId;
	}
	public function getYearTermId() {
		return $this->yearTermId;
	}
	public function getStudentInfo() {
		return $this->studentInfo;
	}
	public function getMembershipArray() {
		return $this->membershipArray;
	}
	public function getActivitiesOrServicesArray() {
		return $this->activitiesOrServicesArray;
	}
	public function getInternalCompetitionsArray() {
		return $this->internalCompetitionsArray;
	}
	public function getExternalCompetitionsArray() {
		return $this->externalCompetitionsArray;
	}
	public function getScholarshipsArray() {
		return $this->scholarshipsArray;
	}
	#########################################
	##	Mutators
	#########################################
	public function setDb($value) {
		$this->db=$value;
	}
	public function setStudentId($value) {
		$this->studentId=$value;
	}
	public function setAcademicYearId($value) {
		$this->academicYearId=$value;
	}
	public function setYearTermId($value) {
		$this->yearTermId=$value;
	}
	public function setStudentInfo($value) {
		$this->studentInfo=$value;
	}
	public function setMembershipArray($value) {
		$this->membershipArray=$value;
	}
	public function setActivitiesOrServicesArray($value) {
		$this->activitiesOrServicesArray=$value;
	}
	public function setInternalCompetitionsArray($value) {
		$this->internalCompetitionsArray=$value;
	}
	public function setExternalCompetitionsArray($value) {
		$this->externalCompetitionsArray=$value;
	}
	public function setScholarshipsArray($value) {
		$this->scholarshipsArray=$value;
	}
	
}
?>