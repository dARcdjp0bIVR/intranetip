<?php
#using:thomas

# modifications:
# Henry Yuen (2010-06-29): change header/ footer/ freetext default font to msungstdlight

// This file may be included through ClassLoader, so need to global some variables.
global $intranet_root;
require_once("$intranet_root/includes/tcpdf/config/lang/eng.php");
require_once("$intranet_root/includes/tcpdf/tcpdf.php");

//require_once("$intranet_root/includes/tcpdf.201007141704/tcpdf.php");

/**
 * Create a sub-class of TCPDF to help customization later.
 */
class StudentReportPDFGenerator_TCPDF extends TCPDF
{
	/**
	 * The header for a student.
	 * Set by set_student_report_header(); Used by Header().
	 */
	protected $student_report_header_html;
	
	/**
	 * Indicate whether to show the header for the student once only.
	 */
	protected $is_to_show_header_once_only;
	
	/**
	 * The footer for a student.
	 * Set by set_student_report_footer(); Used by Footer().
	 */
	protected $student_report_footer_html;
	
	/**
	 * Indicate whether to show the footer for the student once only.
	 */
	protected $is_to_show_footer_once_only;
	
	# Henry Yuen (2010-06-21) 
	protected $header_height;	
	protected $footer_height;
	protected $is_first_page;
	protected $is_last_page;
	
	function __construct() {
		// Just set properties here to simplify StudentReportPDFGenerator.
		parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 

//		$default_fontType = "arialunicid0-chi";
//		$default_fontType = "msungstdlight";  // support with chinese , but no arial in eng
//		$default_fontType = "stsongstdlight"; // support with chinese , but no arial in eng
//		$default_fontType = "arialunicid0";
		$default_fontType = "droidsansfallback";               		         

		// set document information
		$this->SetCreator(PDF_CREATOR);
		$this->SetAuthor('BroadLearning.com');
//		$this->SetTitle('TCPDF Example 048');
//		$this->SetSubject('TCPDF Tutorial');
//		$this->SetKeywords('TCPDF, PDF, example, test, guide');
		
		// set default header data
		$this->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		
		// set header and footer fonts
		//$this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		//$this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		//$this->setHeaderFont(Array('arialunicid0-chi', '', PDF_FONT_SIZE_MAIN));
		//$this->setFooterFont(Array('arialunicid0-chi', '', PDF_FONT_SIZE_DATA));
		//$this->setHeaderFont(Array('msungstdlight', '', PDF_FONT_SIZE_MAIN));
		//$this->setFooterFont(Array('msungstdlight', '', PDF_FONT_SIZE_DATA));		
		//$this->setHeaderFont(Array('stsongstdlight', '', PDF_FONT_SIZE_MAIN));
		//$this->setFooterFont(Array('stsongstdlight', '', PDF_FONT_SIZE_DATA));							
		$this->setHeaderFont(Array($default_fontType, '', PDF_FONT_SIZE_MAIN));
		$this->setFooterFont(Array($default_fontType, '', PDF_FONT_SIZE_DATA));		
		
		// set default monospaced font
		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);		
		$this->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
						
		//set image scale factor
		$this->setImageScale(PDF_IMAGE_SCALE_RATIO); 
		
		//set some language-dependent strings
//		$this->setLanguageArray($l); 
		
		// ---------------------------------------------------------
		
		// set font
		//$this->SetFont('helvetica', '', 8);
		//$this->SetFont('msungstdlight', '', 8);
		//$this->SetFont('arialunicid0-chi', '', 8);
		//$this->SetFont('stsongstdlight', '', 8);
		$this->SetFont($default_fontType, '', 8);		
	}
	
	public function AddPage($orientation='', $format='') {		
		parent::AddPage($orientation, $format);
		//$this->SetTopMargin(PDF_MARGIN_TOP); // default top margin
		
		/*
		if($this->is_first_page){
			$this->SetTopMargin($this->header_height);
			$this->set_is_first_page(false);
		}
		elseif(!$this->is_to_show_header_once_only){
			$this->SetTopMargin($this->header_height);
		}
		else{			
			$this->SetTopMargin(PDF_MARGIN_TOP); // default top margin					
		}		
		
		if($this->is_last_page){	
			$this->set_is_last_page(false);			
		}
		*/						
	}
	
	/**
	 * Override the header output function for the need of student report.
	 */
	public function Header() {					
		if($this->is_first_page){
			$this->writeHTML($this->student_report_header_html);
			$this->SetTopMargin($this->header_height);
			$this->set_is_first_page(false);
		}
		elseif(!$this->is_to_show_header_once_only){
			$this->writeHTML($this->student_report_header_html);
			$this->SetTopMargin($this->header_height);
		}
		else{			
			$this->SetTopMargin(PDF_MARGIN_TOP); // default top margin
			//$this->writeHTML('</br>');			
		}				
	}
	
	/**
	 * Override the footer output function for the need of student report.
	 */
	public function Footer() {
		if($this->is_last_page){
			$this->writeHTML($this->student_report_footer_html);
			//echo '<br>footer';
			$this->set_is_last_page(false);																
		}
		elseif(!$this->is_to_show_footer_once_only){												
			$this->writeHTML($this->student_report_footer_html);
		}		
		
		
		// print page number
		/*
		$this->SetY(-10);
		$ormargins = $this->getOriginalMargins();
		//Print page number
		if (empty($this->pagegroups)) {
			$pagenumtxt = $this->l['w_page'].' '.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
		} else {
			$pagenumtxt = $this->l['w_page'].' '.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
		}		
			
		if ($this->getRTL()) {
			$this->SetX($ormargins['right']);
			$this->Cell(0, 0, $pagenumtxt, 0, 0, 'L');
		} else {
			$this->SetX($ormargins['left']);
			$this->Cell(0, 0, $pagenumtxt, 0, 0, 'R');
		}
		*/	
	}
	
	/**
	 * Set the header for student report.
	 * Should call this before Header() is called.
	 */
	 
	# Henry Yuen (2010-06-21): new parameter header height
	public function set_student_report_header($header_html, $is_to_show_header_once_only, $header_height) {
		$this->student_report_header_html = $header_html;
		$this->is_to_show_header_once_only = $is_to_show_header_once_only;
		$this->header_height = $header_height;
	}
	
	/**
	 * Set the footer for student report.
	 * Should call this before Footer() is called.
	 */
	public function set_student_report_footer($footer_html, $is_to_show_footer_once_only, $footer_height) {
		$this->student_report_footer_html = $footer_html;
		$this->is_to_show_footer_once_only = $is_to_show_footer_once_only;
		$this->footer_height = $footer_height;
	}
	
	# Henry Yuen (2010-06-21): functions for setting isFirstPage/ isLastPage for current page
	public function set_is_first_page($is_first_page){		
		$this->is_first_page = $is_first_page;
	}
	public function set_is_last_page($is_last_page){
		$this->is_last_page = $is_last_page;
	} 
	
}

?>
