<?php
function BuildMappingMultiKeyAssoc($dataAry,$MappingKey){
	
	$resultAssocAry = array();
	foreach($dataAry as $_data){
		$_newMappingKey = $_data[$MappingKey];
		
		$resultAssocAry[$_newMappingKey][] = $_data;
	}
	
	return $resultAssocAry;
}


class objPrinting{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	private $cfgValue;
	
	var $AcademicYear;
	//var $yearClassID;
	var $studentListAry;
	var $studentId;
	
	var $School_SubjectInfo;
	var $Student_info_Ary;
	var $Student_Assessment_Ary;
	var $Student_OLE_Ary;
	var $Student_Awards_Ary;
	var $Student_OLE_Ext_Ary;
	var $Student_SelfAccount_Ary;
	var	$Student_ClassHistory_Ary;
	var $Student_FormName_Ary;
	
	public function __construct(){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		$this->cfgValue = $ipf_cfg;
	}
		
	public function setAcademicYear($AcademicYear){
		$this->AcademicYear = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYear;
	}
	
	public function setYearClassID($yearClassId){
		$this->yearClassID = $yearClassId;
	}
	public function getYearClassID(){
		return $this->yearClassID;
	}
	
	public function setStudentList($studentListAry){
		$this->studentListAry = $studentListAry;
	}
	public function getStudentList(){
		return $this->studentListAry;
	}
	
	
	public function setStudent($studentId){
		$this->studentId = $studentId;
	}
	public function getStudent(){
		return $this->studentId;
	}
	
	public function contentReady(){
		$this->getTargetFormReady();
	//Please Set StudentList Before call this function		
		$this->School_SubjectInfo = $this->getSchoolSubjectInfo();
		$this->Student_info_Ary = $this->getStudentList_Info();
		$this->Student_Assessment_Ary =$this->getStudentList_AssessmentReport();
		$this->Student_OLE_Ary = $this->getStudentList_OLE('Int');
		$this->Student_Awards_Ary = $this->getStudentList_Awards();
		$this->Student_OLE_Ext_Ary = $this->getStudentList_OLE('Ext');
		$this->Student_SelfAccount_Ary = $this->getStudentList_SelfAccount();
		$this->Student_ClassHistory_Ary = $this->getStudentList_ClassHistory();
	}
	
	//Get Subject Info
	private function getSchoolSubjectInfo(){
		$sql = "Select 
			ass.RecordID, 
			ass2.CODEID, 
			ass2.EN_SNAME as SubjectCode, 
			ass2.EN_DES as SubjectDisplayName, 
			ass2.DisplayOrder,
			ass2.CMP_CODEID as isComponent
		From 
			LEARNING_CATEGORY as lc 
			inner join ASSESSMENT_SUBJECT as ass on lc.LearningCategoryID = ass.LearningCategoryID and ass.RecordStatus =1
			left outer join ASSESSMENT_SUBJECT as ass2 on ass.CODEID = ass2.CODEID and ass2.RecordStatus =1
		where 
			lc.RecordStatus = 1 order by lc.DisplayOrder, ass.DisplayOrder, ass2.CMP_CODEID , ass2.DisplayOrder";
			
		$result = $this->objDB->returnResultSet($sql);
		
		return $result;
	}
		
	//Get Student Info
	private function getStudentList_Info(){
		$sql = "select 
					iu.EnglishName,
					iu.ChineseName,
					DATE_FORMAT(iu.DateOfBirth, '%Y-%m-%d') AS DateOfBirth,
					iu.ClassName,
					iu.ClassNumber,
					iu.Gender,
					iu.BarCode,
					iu.STRN,
					substring(iu.WebSAMSRegNo from 2) as WebSAMSRegNo ,
					iu.userid
				from
					".$this->intranet_db.".INTRANET_USER AS iu LEFT JOIN ".$this->intranet_db.".INTRANET_USER_PERSONAL_SETTINGS AS uextra ON uextra.UserID=iu.UserID 
				where
					iu.userid IN ('".implode('\',\'',$this->getStudentList())."') AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";

		$result = $this->objDB->returnResultSet($sql);
		
		return BuildMultiKeyAssoc($result,'userid');
	}
	public function getStudentInfo(){
		return $this->Student_info_Ary[$this->getStudent()];
	}
	
	private function getTargetFormReady(){
		$sql = 'select ClassTitleEN from YEAR_CLASS where yearclassid = '.$this->yearClassID.'';
		$targetclassnameAry = $this->objDB->returnVector($sql); 
		preg_match_all("/\d+\B/", $targetclassnameAry[0], $tempform);
		
		$form = $tempform[0][0];
		$formAry[] = $form;
		if($form > 1){
			$formAry[] = --$form;
		}
		if($form > 1){
			$formAry[] = --$form;
		}
		sort($formAry);
		$this->Student_FormName_Ary = $formAry;
		
		if(count($formAry)>1){
			return true;
		}
	}
	public function getVerifiedFormAry(){
		$formAry = $this->Student_FormName_Ary;
		$StudentClassHistroyArray = $this->getStudentClassHistory();
		
		foreach($formAry as $_formName){
			if(isset($StudentClassHistroyArray[$_formName])){
				$verifiedFormAry[] = $_formName;
			}
			else{
				continue;
			}
		}
		sort($verifiedFormAry);
		return $verifiedFormAry;
		
	}
	
	private function getStudentList_ClassHistory(){
		
		$formAry = $this->Student_FormName_Ary;		
		
		$sql = "Select UserID, ClassName, ay.YearNameEN as AcademicYear, pch.AcademicYearID 
				From PROFILE_CLASS_HISTORY as pch 
					INNER JOIN ACADEMIC_YEAR as ay on pch.AcademicYearID = ay.AcademicYearID
				where 
					pch.userid IN  ('".implode('\',\'',$this->getStudentList())."') 
					AND pch.ClassName REGEXP '".implode('|',$formAry)."' 
				order by pch.AcademicYear";
		$result = $this->objDB->returnResultSet($sql);
		$academicYearIdAry = Get_Array_By_Key($result, 'AcademicYearID');
		
		
		$sql = "Select AcademicYearID, YearTermID From ACADEMIC_YEAR_TERM where YearTermNameEN !='Summer Holiday' AND AcademicYearID IN ('".implode("','", (array)$academicYearIdAry)."') order by TermStart";
		$academicYearAry = $this->objDB->returnResultSet($sql);
		
		// $yearTermAssoAry[$academicYearId][0] = $yearTermId1
		// $yearTermAssoAry[$academicYearId][1] = $yearTermId2
		$yearTermAssoAry = BuildMultiKeyAssoc($academicYearAry, 'AcademicYearID', $IncludedDBField=array('YearTermID'), $SingleValue=1, $BuildNumericArray=1);
				
		
		foreach($result as $_userClassHistoryAry){
			$_userID = $_userClassHistoryAry['UserID'];
			$tempClassName = $_userClassHistoryAry['ClassName'];
			preg_match_all("/\d+\B/", $tempClassName, $formNameAry);
			$resultAssoAry[$_userID][$formNameAry[0][0]] = $_userClassHistoryAry;
			
			$_academicYearID = $_userClassHistoryAry['AcademicYearID'];
			$resultAssoAry[$_userID][$formNameAry[0][0]]['YearTermIDAry'] = $yearTermAssoAry[$_academicYearID];
		}
		
		return $resultAssoAry;
	}
	public function getStudentClassHistory(){
		return $this->Student_ClassHistory_Ary[$this->getStudent()];
	}
	
	//Get Student Assessment Report
	private function getStudentList_AssessmentReport(){
		
		$formAry = $this->Student_FormName_Ary;
		
		$sql ="SELECT 
						a.RecordID,
						IFNULL(iu.WebSAMSRegNo, iau.WebSAMSRegNo) as WebSAMSRegNo,
						a.ClassName as ClassName,
						a.ClassNumber as ClassNumber,
						a.Year,
						a.Semester as Semester,
						a.AcademicYearID,
						a.YearTermID,
						a.SubjectCode,
						a.SubjectComponentCode,
						a.Score,
						a.Grade,
						a.OrderMeritForm,
						asub.EN_ABBR as Subject,
						a.SubjectID,
						a.userid
					FROM
						".$this->eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD as a
						LEFT JOIN ".$this->intranet_db.".INTRANET_USER AS iu ON a.UserID = iu.UserID
						LEFT JOIN ".$this->intranet_db.".INTRANET_ARCHIVE_USER AS iau ON a.UserID = iau.UserID
						LEFT JOIN ".$this->intranet_db.".ASSESSMENT_SUBJECT AS asub ON a.subjectID = asub.RecordID
					WHERE
						/*a.AcademicYearID = '$AcademicYearID'*/
						 (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)
						AND a.ClassName REGEXP '".implode('|',$formAry)."'
						AND a.userid IN ('".implode('\',\'',$this->getStudentList())."')  
					ORDER BY
						a.ClassName,
						ClassNumber,
						a.Year desc,
						a.Semester desc,
						asub.DisplayOrder
						/*a.SubjectCode,
						a.SubjectComponentCode*/
				";
				
		$result = $this->objDB->returnResultSet($sql);
		return BuildMappingMultiKeyAssoc($result,'userid');		
	}
	public function GetAssessmentReport(){
		return $this->Student_Assessment_Ary[$this->getStudent()];
	}
	
	//Get Student OLE(Int)
	private function getStudentList_OLE($IntExt){
	    
	    $formAry = $this->Student_FormName_Ary;

	    $sql = "SELECT yc.AcademicYearID                       
                FROM 
                    ".$this->intranet_db.".YEAR_CLASS AS yc
                INNER JOIN ".$this->intranet_db.".YEAR_CLASS_USER AS ycu ON (ycu.YearClassID = yc.YearClassID)
                LEFT JOIN ".$this->intranet_db.".INTRANET_USER AS iu ON ycu.UserID = iu.UserID
                LEFT JOIN ".$this->intranet_db.".INTRANET_ARCHIVE_USER AS iau ON ycu.UserID = iau.UserID
                WHERE 
                ycu.UserID IN ('".implode('\',\'',$this->getStudentList())."')  
                AND (yc.ClassTitleEN REGEXP '".implode('|',$formAry)."' OR yc.ClassTitleB5 REGEXP '".implode('|',$formAry)."' )
                AND  (iu.WebSAMSRegNo IS NOT NULL OR iau.WebSAMSRegNo IS NOT NULL)               
            ";
	    
	    $AcademicYearIDAry = $this->objDB->returnVector($sql);
	
		//Get the latest 3 Academic Year
// 		$sql = 'Select distinct(AcademicYearID) From ACADEMIC_YEAR_TERM where TermStart< now() order by TermStart desc limit 3';
// 		$AcademicYearIDAry = $this->objDB->returnVector($sql);
		
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";
		if($IntExt == 'Int'){
			$conds_IntExt = ' and op.IntExt = "Int" ';
	
		}
		else if($IntExt == 'Ext'){
			$conds_IntExt = ' and op.IntExt = "Ext" ';
		}
		
		// 20180316 Omas
		/*
		 * added "OR" condition for hardcode #L136904 
		 */
		$sql = "select 
					op.Title,
					op.StartDate,
					op.EndDate,
					os.Role,
					os.Hours,
					os.Achievement,
					op.ELE,
					op.Details,
					ay.YearNameEN as AcademicYear,
					oc.EngTitle as Category,
					os.userid,
					os.Organization as Organization
				from 
					".$this->eclass_db.".OLE_STUDENT as os 
					inner join ".$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid 
					inner join ".$this->eclass_db.".OLE_CATEGORY as oc on op.Category = oc.RecordID 
					inner join ".$this->intranet_db.".ACADEMIC_YEAR as ay on op.AcademicYearID = ay.AcademicYearID
				where 
					(os.userid IN ('".implode('\',\'',$this->getStudentList())."')
					AND op.AcademicYearID IN('".implode('\',\'',(array)$AcademicYearIDAry)."')  
					$condApprove
					$conds_IntExt)
					OR ( os.UserID IN (6590) AND op.AcademicYearID IN (7,9) $conds_IntExt $condApprove)
				order by 
					ay.YearNameEN desc
				";
				
		$result = $this->objDB->returnResultSet($sql);
		return BuildMappingMultiKeyAssoc($result,'userid');	
	}
	public function GetOLE(){
		return $this->Student_OLE_Ary[$this->getStudent()];
	}
	
	
	//Get Student Awards
	private function getStudentList_Awards(){
	      
		$sql ="
			SELECT
				awds.userid,
				ay.YearNameEN as AcademicYear,
				yt.YearTermNameEN as Term,
				awds.AwardName
			FROM
				".$this->eclass_db.".AWARD_STUDENT as awds
				LEFT JOIN ".$this->intranet_db.".ACADEMIC_YEAR as ay on awds.AcademicYearID = ay.AcademicYearID
				LEFT JOIN ".$this->intranet_db.".ACADEMIC_YEAR_TERM as yt on awds.YearTermID = yt.YearTermID
			WHERE
				awds.Userid IN ('".implode('\',\'',$this->getStudentList())."')  
				AND awds.RecordType = '1'
			ORDER BY
				ay.YearNameEN desc
			";

		$result = $this->objDB->returnResultSet($sql);
		return BuildMappingMultiKeyAssoc($result,'userid');
	}
	public function getAwards(){
		return $this->Student_Awards_Ary[$this->getStudent()];
	}
	
	//Get OLE(Ext)
	public function GetOLE_Ext(){
		return $this->Student_OLE_Ext_Ary[$this->getStudent()];
	}
	
	//Get Self Account
	private function getStudentList_SelfAccount(){
		$sql = "select 
					sas.Details as Details,
					userid
				from 
					".$this->eclass_db.".SELF_ACCOUNT_STUDENT as sas 
				where 
					Userid IN ('".implode('\',\'',$this->getStudentList())."')  
					and DefaultSA = 'SLP' 
			   ";

		$result = $this->objDB->returnResultSet($sql);
		return BuildMappingMultiKeyAssoc($result,'userid');
	}
	public function getSelfAccount(){
		return $this->Student_SelfAccount_Ary[$this->getStudent()];
	}
}

?>