<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $academcYearDisplay; // store the academic Year display in the student Info
	private $uid;
	private $cfgValue;
	private $requireELE; // record the client require ELE type
	private $eleNameAry; // store the ELE display Name for the reprot

	public function studentInfo($uid){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid;
		$this->cfgValue = $ipf_cfg;
		$this->setRequireELE();

		$this->setELENameFromDB();
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	private function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	private function setELENameFromDB(){
		//$requireELE = $this->getRequireELE();


		$eleStr = "";
		for($i = 0,$i_max = sizeof($requireELE);$i< $i_max;$i++){
			$_eleName = $requireELE[$i];

			$eleStr .= "'{$_eleName}',";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$eleStr = substr($eleStr,0,-1);
		$sql = "select DefaultID,concat(ChiTitle,' (',EngTitle,')') as 'eleName'  from ".$this->eclass_db.".OLE_ELE where defaultid in({$eleStr})";
		$result = $this->objDB->returnArray($sql);

		for($i=0,$i_max = sizeof($result);$i < $i_max;$i++){
			$_eleID = $result[$i]["DefaultID"];
			$_eleNAME = $result[$i]["eleName"];
			$ele[$_eleID] = $_eleNAME;
		}
		$this->eleNameAry = $ele;

	}
	private function getELENameDetails($eleType){
		return $this->eleNameAry[$eleType];
	}
	private function getELENameAry(){
		return $this->eleNameAry;
	}
	private function getRequireELE(){
		return $this->requireELE;
	}
	private function setRequireELE(){
		$_ary[]="[MCE]";
		$_ary[]="[AD]";
		$_ary[]="[PD]";
		$_ary[]="[CS]";
		$_ary[]="[CE]";
		$this->requireELE = $_ary;
	}
	public function getUid(){
		return $this->uid;
	}
	/*************************/
	/*******STUDENT INFO******/
	/*************************/
	public function getStudentInfo_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	public function getStudentInfo(){
		$targetClassTitle = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
		$targetClassOtherTitle = Get_Lang_Selection("yc.ClassTitleEN", "yc.ClassTitleB5");
		$targetYear = $this->AcademicYearAry[0];
		
		$sql = "select " .
					"iu.EnglishName," .
					"iu.ChineseName," .
					"DATE_FORMAT(iu.DateOfBirth, '%d/%m/%Y') AS DateOfBirth," .
					"iu.ClassName," .
					"iu.ClassNumber," .
					"iu.Gender,".
					"iu.BarCode,".
					"iu.HKID,".
					"DATE_FORMAT(uextra.AdmissionDate, '%d/%m/%Y') AS AdmissionDate,".
					"substring(iu.WebSAMSRegNo from 2) as 'WebSAMSRegNo' " .
				" from " .
					$this->intranet_db.".INTRANET_USER AS iu LEFT JOIN ".$this->intranet_db.".INTRANET_USER_PERSONAL_SETTINGS AS uextra ON uextra.UserID=iu.UserID " .
				" where " .
					"iu.userid =".$this->uid." AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";

		$result = $this->objDB->returnArray($sql);
		
		$sql = "select " .
					"IF(".$targetClassTitle." IS NOT NULL OR TRIM(".$targetClassTitle.") != '', ".$targetClassTitle.", ".$targetClassOtherTitle.") AS ClassName," .
					"IF(TRIM(ClassName) != '', ycu.ClassNumber, '--') AS ClassNumber " .
				" from " .
					$this->intranet_db.".INTRANET_USER AS iu 
					INNER JOIN ".$this->intranet_db.".YEAR_CLASS_USER ycu ON iu.UserID = ycu.UserID 
					INNER JOIN ".$this->intranet_db.".YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID".
				" where " .
					"iu.userid =".$this->uid." AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') AND yc.AcademicYearID = ".$targetYear."";
		
		$classResult = $this->objDB->returnArray($sql);
		
		if(count($result) > 0){
			$result[0]['ClassName'] = $classResult[0]['ClassName'];
			$result[0]['ClassNumber'] = $classResult[0]['ClassNumber'];
		}
		
		return $result;
	}
	public function getStudentAwards($AcademicYearID=""){
		
    	$sql_cond  = ($AcademicYearID!="") ? " AND AcademicYearID='".$AcademicYearID."' " : "";
    
	    $sql =  "
	              SELECT
	                RecordID,
	                UserID,
	                AcademicYearID,
	                Year,
	                YearTermID,
	                Semester,
	                IsAnnual,
	                ClassName,
	                ClassNumber,
	                AwardDate,
	                AwardName,
	                AwardFile,
	                Details,
	                Organization,
	                SubjectArea,
	                Remark,
	                RecordType,
	                RecordStatus,
	                ProcessDate,
	                InputDate,
	                ModifiedDate
	              FROM
	                ".$this->eclass_db.".AWARD_STUDENT WHERE UserID='".$this->uid."' {$sql_cond} 
	              ORDER BY
	                SubjectArea, AwardName
	            ";

		$result = $this->objDB->returnArray($sql);

		return $result;
	}
	public function displayAwards($StudentAwards, $BySubjectArea, $TableHeader, $PageNow, $RowLeft, $PageRowMax, $JustPageBreak){
		
		$Total = 0;
		for ($i=0; $i<sizeof($StudentAwards); $i++)
		{
			$counted = false;
			
			$RowObj = $StudentAwards[$i];
			$SubjectArea = trim($RowObj["SubjectArea"]);
			
			if (is_array($BySubjectArea) && in_array($SubjectArea, $BySubjectArea))
			{
				$counted = true;
			} elseif (!is_array($BySubjectArea) && $BySubjectArea=="" && $SubjectArea=="")
			{
				# the others - Issued by Other Agencies 
				$counted = true;
			}
			
			if ($counted)
			{
				if ($JustPageBreak)
				{
					# table header
					$RecordHtml .= $TableHeader;
					$JustPageBreak = false;
					$RowLeft -= 3;
				}
				
				$RowLeft --;
				$Total ++;
				$RecordHtml .= '<tr><td class="col_year">'.trim($RowObj["Year"]).'</td><td>'.trim($RowObj["AwardName"]).'</td></tr>';
				
				if ($RowLeft<=0)
				{
					$RecordHtml .= '</table></div><!-- Table list end-->';
					//$RecordHtml .= '<br class="pageBreak" clear="all" />';
					$RecordHtml .= '<div class="page_no"> Page '.$PageNow.'/[TOTAL_PAGES]
					       <!--<span> School Chop 校印</span>--></div>
					       </div>
					       <p class="page_break" clear="all" />';
					$PageNow ++;
					$RowLeft = $PageRowMax;
					if($PageNow % 2){
						$RowLeft -= 5;
					}
						
					# new page header
					$RecordHtml .= '[SUB_PAGE_HEADER]';

					$JustPageBreak = true;
				}
			}
		}
		
		if ($Total==0)
		{
			$RowLeft --;
			$RecordHtml = '<tr><td class="col_year">NIL</td><td>&nbsp;</td></tr>';
		}
		
		return array($PageNow, $RowLeft, $RecordHtml, $JustPageBreak);
	}
	
	public function getELE()
	{
		$sql = "SELECT if(RecordStatus=2, DefaultID, CONCAT('[', RecordID, ']')) AS ELEID, ChiTitle FROM ".$this->eclass_db.".OLE_ELE where RecordStatus<>'0'";
		$rows = $this->objDB->returnResultSet($sql);
		
		/*
		$results = array();
		
		for ($i=0; $i<sizeof($rows); $i++)
		{
			$results[$rows[$i]["ELEID"]] = $rows[$i]["ChiTitle"];
		}
		*/
		return $rows;
	}
	
	
	public function displayOLEByOthers($OLERecords, $Year, $TableHeader, $PageNow, $RowLeft, $PageRowMax){
		
		
		for ($i=0; $i<sizeof($OLERecords); $i++)
		{
			$counted = false;
			
			$RowObj = $OLERecords[$i];
			
			$RowLeft --;
			
			$RecordHtml .= '<tr><td >'.$Year.'</td><td>'.trim($RowObj["title"]).'</td><td>'.trim($RowObj["Organization"]).'</td><td>'.trim($RowObj["Achievement"]).'</td></tr>';
			if ($RowLeft<=0)
			{
				$RecordHtml .= '</table></div><!-- Table list end-->';
				//$RecordHtml .= '<br class="pageBreak" clear="all" />';
				### IMPORTANT: spaces are very important for top margin replacement!!!
				$RecordHtml .= '<div class="page_no"> Page '.$PageNow.'/[TOTAL_PAGES]</div>
	       </div>
	       <p class="page_break" clear="all" />';	
	       
				$PageNow ++;
				$RowLeft = $PageRowMax;
				if($PageNow % 2){
					$RowLeft -= 5;
				} 

				# new page header
				$RecordHtml .= '[SUB_PAGE_HEADER]';

				if ($i<sizeof($OLERecords)-1)
				{
					$RecordHtml .= $TableHeader;
				} else
				{
					$NoTableHeader = true;
				}
			}
		}
		
		if (sizeof($OLERecords)==0)
		{
			$RecordHtml = '<tr><td colspan="4">NIL</td></tr>';
			$RowLeft --;
		}
		
		if (!$NoTableHeader)
		{		
              
$RecordHtml .= <<<EOF
            </table>
      </div>
       <!-- Table list end-->
EOF;
		}
		return array($PageNow, $RowLeft, $RecordHtml);
	}
	
	public function displayOLEBySchool($OLERecords, $Year, $TableHeader, $PageNow, $RowLeft, $PageRowMax){
		
		
		for ($i=0; $i<sizeof($OLERecords); $i++)
		{
			$counted = false;
			
			$RowObj = $OLERecords[$i];
			
			$RowLeft --;
			
			$RecordHtml .= '<tr><td >'.$Year.'</td><td>'.trim($RowObj["title"]).'</td><td>'.trim($RowObj["Achievement"]).'</td></tr>';
			if ($RowLeft<=0)
			{
				$RecordHtml .= '</table></div><!-- Table list end-->';
				//$RecordHtml .= '<br class="pageBreak" clear="all" />';
				### IMPORTANT: spaces are very important for top margin replacement!!!
				$RecordHtml .= '<div class="page_no"> Page '.$PageNow.'/[TOTAL_PAGES]</div>
 	       </div>
	       <p class="page_break" clear="all" />';	
	       
				$PageNow ++;
				$RowLeft = $PageRowMax;
				if($PageNow % 2){
					$RowLeft -= 5;
				} 
				
				# new page header
				$RecordHtml .= '[SUB_PAGE_HEADER]';
				
				if ($i<sizeof($OLERecords)-1)
				{
					$RecordHtml .= $TableHeader;
				} else
				{
					$NoTableHeader = true;
				}
			}
		}
		
		if (sizeof($OLERecords)==0)
		{
			$RowLeft --;
			$RecordHtml = '<tr><td colspan="3">NIL</td></tr>';
		}


		if (!$NoTableHeader)
		{	
$RecordHtml .= <<<EOF
            </table>
      </div>
       <!-- Table list end-->
EOF;
		}
		
		return array($PageNow, $RowLeft, $RecordHtml);
	}
	
	
	public function displayOLE($OLERecords, $Year, $TableHeader, $PageNow, $RowLeft, $PageRowMax){
		
		$ELEArray = $this->getELE();
		
		for ($i=0; $i<sizeof($OLERecords); $i++)
		{
			$counted = false;
			
			$RowObj = $OLERecords[$i];
			
			$RowLeft --;
			
			$ELE = trim($RowObj["ELE"]);
			for ($j=0; $j<sizeof($ELEArray); $j++)
			{
				$ELE = str_replace($ELEArray[$j]["ELEID"], $ELEArray[$j]["ChiTitle"] , $ELE);
			}
			$ELEMulti = (mb_substr_count($ELE, ",")>=1);
			$ELE = str_replace(",", " /<br />", $ELE);
			
			$Organization = trim($RowObj["Organization"]);
			if ($Organization!="")
			{
				$Organization = "<br />( ".$Organization." )";
				
			}
			
//			if ($ELEMulti || $Organization!="" || countString(trim($RowObj["title"]))>23)
			if ($ELEMulti || $Organization!="")
			{
				$RowLeft --;
			}		
			if(countString(trim($RowObj["title"]))>17){
				$RowLeft --;
			}
			if(countString(trim($RowObj["Organization"]))>15){
				$RowLeft --;
			}
			
			$RecordHtml .= '<tr><td >'.$Year.'</td><td>'.trim($RowObj["title"]).$Organization.'</td><td>'.trim($RowObj["Role"]).'</td><td>'.$ELE.'</td><td>'.trim($RowObj["Hours"]).'</td></tr>';
			if ($RowLeft<=0)
			{
				$RecordHtml .= '</table></div><!-- Table list end-->';
				//$RecordHtml .= '<br class="pageBreak" clear="all" />';
				### IMPORTANT: spaces are very important for top margin replacement!!!
				$RecordHtml .= '<div class="page_no"> Page '.$PageNow.'/[TOTAL_PAGES]</div>
	       </div>
	       <p class="page_break" clear="all" />';
	       
				$PageNow ++;
				$RowLeft = $PageRowMax;
				if($PageNow % 2){
					$RowLeft -= 5;
				}
				
				# new page header
				$RecordHtml .= '[SUB_PAGE_HEADER]';
				
				if ($i<sizeof($OLERecords)-1)
				{
					$RecordHtml .= $TableHeader;
				} else
				{
					$NoTableHeader = true;
				}

			}
		}
		
		if (sizeof($OLERecords)==0)
		{
			$RowLeft --;
			$RecordHtml = '<tr><td colspan="5">NIL</td></tr>';
		}     
		
		if (!$NoTableHeader)
		{
$RecordHtml .= <<<EOF
            </table>
      </div>
       <!-- Table list end-->
EOF;
		}
		
		return array($PageNow, $RowLeft, $RecordHtml);
	}
	
	private function getStudentInfoDisplay($result){
		
		$strn = $result[0]["STRN"];
		
		//$strn = $this->specialHandleStrn($strn);
	
		$str .= "<table width=\"100%\" border = \"0\">";

		$str .= "<col width= \"25%\"/>";
		$str .= "<col width= \"30%\"/>";
		$str .= "<col width= \"30%\"/>";
		$str .= "<col width= \"15%\"/>";

		$str .= "<tr><td >學生姓名 Name : </td><td>".$result[0]["studentName"]."&nbsp</td><td>學生編號 STRN : </td><td>".$strn."&nbsp</td></tr>";
		$str .= "<tr><td >班別 Class : </td><td>".$result[0]["classNameNo"]."&nbsp</td><td>註冊編號 Reg. No : </td><td>".$result[0]["WebSAMSRegNo"]."&nbsp</td></tr>";
		$str .= "<tr><td >學年 Academic Year : </td><td>".$this->getAcademicYearDisplay()."</td><td>派發日期 Date of Issue : </td><td>".$this->printIssueDate."</td></tr>";
		$str .= "</table>";
		
		return $str;	
	}
	
	
	
	
	//return OLE result for both INT /EXT
	public function getOLE($oleType,$createBy = null, $OleCat=""){
		//$requireELE = $this->getRequireELE();
		
		$requestYear = $this->getAcademicYear();

		if(is_array($requestYear) && sizeof($requestYear) > 0){
			$requestYearStr = implode(",",$requestYear);
			$cond_requestYear = " and op.AcademicYearID in ({$requestYearStr})";
		}
		
		if ($OleCat=="BySchool")
		{
			$sql = "SELECT RecordID FROM " .$this->eclass_db.".OLE_CATEGORY WHERE (EngTitle='Awards and Achievements Issued by School' OR ChiTitle='學校頒發獎項及成就') AND recordStatus='1'";
			$row = $this->objDB->returnVector($sql);
		} elseif ($OleCat=="ByOthers")
		{
			$sql = "SELECT RecordID FROM " .$this->eclass_db.".OLE_CATEGORY WHERE (EngTitle='Awards and Achievements Issued by Other Agencies' OR ChiTitle='其他機構頒發獎項及成就') AND recordStatus='1'";
			$row = $this->objDB->returnVector($sql);
		} else
		{
			
			$sql = "SELECT RecordID FROM " .$this->eclass_db.".OLE_CATEGORY WHERE (EngTitle='Awards and Achievements Issued by Other Agencies' OR ChiTitle='其他機構頒發獎項及成就' OR EngTitle='Awards and Achievements Issued by School' OR ChiTitle='學校頒發獎項及成就') AND recordStatus='1'";
			$row = $this->objDB->returnVector($sql);
		}
		
		
		if (is_array($row) && sizeof($row)>0)
		{
			$CategoryIDs = implode(",", $row);
		}
		$sql_category = ($OleCat=="BySchool" || $OleCat=="ByOthers") ? " AND op.Category IN ({$CategoryIDs}) " : "  AND op.Category NOT IN ({$CategoryIDs})  ";
		if ($CategoryIDs=="")
		{
			die("Please set two categories under Programme Catogory of Student Learning Profile Settings.");
		}
		


		$cond = " and op.INTEXT ='{$oleType}' ";
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";
		$cond_CreatedBY = (trim($createBy)=="") ?"":" and op.PROGRAMTYPE = '{$createBy}' "; 

		//common sql for both INT / EXT
		$sql = "select " .
						"op.title , " .
						"op.StartDate, " .
						"op.EndDate,".
						"concat(date_format(op.StartDate, '%d-%m-%Y' ),'',if(op.enddate is null or (date_format(op.enddate,'%d-%m-%Y')='00-00-0000'),'',concat('-<br />',date_format(op.enddate,'%d-%m-%Y')))) as 'pDate',".
						"os.Role,".
						"os.Hours,".
						"op.Organization,".
						"os.Achievement, ".
						"op.ELE, ".
						"op.Category, ".
						"op.ProgramType ".
					"from " .
						$this->eclass_db.".OLE_STUDENT as os " .
						"inner join " .$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid ".
					"where " .
						"os.userid = ".$this->getUid()." ".
						$cond.
						$cond_CreatedBY. $sql_category.
						$condApprove.
						$cond_requestYear." ";
		$sqlOrder = " order by op.StartDate desc, os.Hours desc ";


		//case handle for INT / EXT
		$result = $this->objDB->returnArray($sql);
		
		return $result;	
	}
	

	public function getSelfAccount(){
		$sql = "select sas.Details as Details from ".$this->eclass_db.".SELF_ACCOUNT_STUDENT as sas where Userid = ".$this->getUid()." and instr(DefaultSA,'SLP')>0 limit 1";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	public function setAcademicYearDisplay($academcYearDisplay){
		$this->academcYearDisplay = $academcYearDisplay;
	}
	public function getAcademicYearDisplay(){
		return $this->academcYearDisplay;
	}
	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	private function specialHandleStrn($strn){

			//USER request to replace the last character of strn to XXX
			$pattern = '/^(.*).{3}$/';
			$replacement = '${1}XXX';
			$strn = preg_replace($pattern,$replacement,$strn);
			return $strn;
	}

}
?>