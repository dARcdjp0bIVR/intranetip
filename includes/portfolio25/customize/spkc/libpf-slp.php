<?php

class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	private $StdName;
	private $passMark;
	
	private $PageNo;
	
	private $DisplayComponent_Array;
	
	private $YearClassID;
	
	private $AcademicYearStr;
	
	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid;
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '--';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#585858';
		$this->TableTitleRemarkBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->TableTitleFontColor = '#FFFFFF';
		
		$this->TableWidth = '90%';
		
		$this->StdClassName = '';
		
		$this->NumberOfFirstPage = '12';
		$this->passMark = '';
		
		$this->RowNoNow = 0;
		
		$this->PageNo = 1;
		
		$this->SchoolTitleEn = 'Stewards Pooi Kei College';
		$this->SchoolTitleCh = '香港神託會培基書院';
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	
	public function setAll_AcademicYearID($All_AcademicYearID)
	{
		$this->All_AcademicYearID = $All_AcademicYearID;
	}
	
	public function addPageNo(){
		$this->PageNo++;
	}
	public function getPageNo(){
		return $this->PageNo;
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}
	public function setPassMark($ParPassMark)
	{
		$this->passMark = $ParPassMark;
	}
	
	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}
	
	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	
	private function getYearID($YearName='thisYear')
	{
		if($YearName=='thisYear')
		{
			$YearClassID = $this->YearClassID;
			
			$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
			$sql = "select
			ClassTitleEN,
			YearID
			from
			$YEAR_CLASS
			where
			YearClassID =$YearClassID
			";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		else
		{
			$YEAR = $this->Get_IntranetDB_Table_Name('YEAR');
			$sql = "select
			YearID
			from
			$YEAR
			where
			YearName =$YearName
			";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		
		return $yearID;
	}
	
	
	
	/***************** Part 1 : Report Header ******************/
	public function getPart1_HTML()  {
		$header_font_size = '18';
		
		//	$imgFile = get_website().'/includes/portfolio25/customize/ychwws/wws_logo.jpg';
		//	$schoolLogoImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:90px;">' : '&nbsp;';
		
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:'.$this->TableWidth.'; text-align:center; border:0px;">
					<tr>
						<td rowspan="3" align="center" " style="width:18%;">'.$schoolLogoImage.'</td>
						<td style="font-size:24px;"><font face="Lucida Calligraphy"><b>'.$this->SchoolTitleEn.'</b></font></td>
						<td rowspan="3" align="left" style="width:18%;">&nbsp;</td>
				   </tr>';
		$x .= '<tr><td style="font-size:21px;"><font face="新細明體"><b>'.$this->SchoolTitleCh.'</b></font></td></tr>';
		$x .= '<tr><td style="font-size:16px;"><font face="Lucida Calligraphy"><b>Student Learning Profile '.$this->YearRangeStr.'</b></font></td></tr>';
		
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/
	
	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select
		EnglishName,
		ChineseName,
		HKID,
		DateOfBirth,
		Gender,
		STRN,
		ClassNumber,
		WebSAMSRegNo,
		ClassName
		From
		$INTRANET_USER
		Where
		UserID = '".$this->uid."'
						And RecordType = '2'
				";
		
		$result = $this->objDB->returnResultSet($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{
		$fontSize = 'font-size:15px;';
		
		$thisPartTitle = 'Student Particulars';
		
		$td_space = '<td>&nbsp;</td>';
		$_paddingLeft = 'padding-left:10px;';
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$DateOfLeaveYear = $this->EmptySymbol;
			$IssueDate = $this->EmptySymbol;
		} else {
			$DateOfLeaveYear = date('Y',strtotime($IssueDate));
			$IssueDate = date('d F Y',strtotime($IssueDate));
		}
		
		$DOB = $result[0]["DateOfBirth"];
		if (date($DOB)==0) {
			$DOB = $this->EmptySymbol;
		} else {
			$DOB = date('d F Y',strtotime($DOB));
		}
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$this->StdName = $ChiName.''.$EngName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];
		
		if($Gender=='M')
		{
			$Gender ='Male';
		}
		else if($Gender=='F')
		{
			$Gender ='Female';
		}
		
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$RegistrationNo = $result[0]["WebSAMSRegNo"];
		$RegistrationNo = str_replace('#',' ',$RegistrationNo);
		if($RegistrationNo=='')
		{
			$RegistrationNo = $this->EmptySymbol;
		}
		
		$DateOfAdmisDate = substr($RegistrationNo,1,4);
		if($DateOfAdmisDate=='')
		{
			$DateOfAdmisDate = $this->EmptySymbol;
		}
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = str_replace('-',' ',$ClassName);
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;
		
		$this->StdClassName = $ClassName;
		
		$html = '';
		$html .= '<table width="'.$this->TableWidth.'" cellpadding="2" border="0px" align="center" style="border-collapse: collapse;">
					<col width="3%" />
					<col width="20%" />
					<col width="27%" />
					<col width="25%" />
					<col width="25%" />
				
					<tr>
							<td colspan="2" style="'.$fontSize.'"><b>Student Number: </b></td>
							<td style="'.$fontSize.'">'.$RegistrationNo.'</td>
							<td style="'.$fontSize.'"><b>Class: </b></td>
							<td style="'.$fontSize.'">'.$ClassName.'</td>
					</tr>
					<tr>
							<td style="'.$fontSize.'"><b>Name</b></td>
							<td style="text-align:left;'.$fontSize.'"><b>(in English): </b></td>
							<td style="'.$fontSize.'">'.$EngName.'</td>
							<td style="'.$fontSize.'"><b>Class Number: </b></td>
							<td style="'.$fontSize.'">'.$ClassNumber.'</td>
					</tr>
					<tr>
							<td style="'.$fontSize.'"><b>&nbsp;</b></td>
							<td style="text-align:left;'.$fontSize.'"><b>(in Chinese): </b></td>
							<td style="'.$fontSize.'">'.$ChiName.'</td>
							<td style="'.$fontSize.'"><b>Date of Issue: </b></td>
							<td style="'.$fontSize.'">'.$IssueDate.'</td>
					</tr>
									
					';
		
		$html .= '</table>';
		
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Academic Performance******************/
	public function getPart3_HTML(){
		
		$Part3_Title = 'Academic Performance in School';
		
		$Page_break='page-break-after:always;';
		$table_style = 'border-collapse: collapse; ';
		
		$html = '';
		
		//	$YearID = $this->getYearID('thisYear');
		
		//if($YearID=='6')
		//{
		$html .=$this->getAcademicResultContentDisplay($Part3_Title);
		//}
		return $html;
	}
	
	private function getAcademicResultContentDisplay($MainTitle='')
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$remark_html ='';
		$remark_html = '<table width="100%"><tr>';
		
		$ContentTable = $this->getAcademicResult_ContentTable($MainTitle);
		
		$html='';
		
		$html .='<table width="'.$this->TableWidth.'" height="" cellpadding="0" border="0px" align="center" style="border-collapse: collapse;">
					<tr>
						<td style="vertical-align: top;">'.$ContentTable.'</td>
					</tr>
			   </table>';
		
		return $html;
	}
	
	private function getAcademicResult_ContentTable($MainTitle='')
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$YearClassArray = $this->getStudentAcademic_YearClassNameArr(true);
		
		$Subject_string = 'Subjects';
		$FullMark_string = 'Full Mark';
		$Mark_string = 'Mark / Performance in School';
		
		$StudentID = $this->uid;
		
		$lpf_report = new libpf_report();
		list($SubjectIDNameAssoArr, $StudentFormInfoArr, $StudentAcademicResultInfoArr, $SubjectInfoOrderedArr) = $lpf_report->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($StudentID,false,'',$ignoreAssessment = false);
		
		$lpf_academic = new libpf_academic();
		$FullMarkArr = $lpf_academic ->Get_Subject_Full_Mark($this->All_AcademicYearID);
		
		$countColumn=0;
		
		$MarkArr = array();
		
		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{
			$countColumn++;
		}
		$AcademicYearArray = $this->getAcademicYearArr($this->All_AcademicYearID);
		$YearClassArray = $this->getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,'desc');
		
		$lpf_academic = new libpf_academic();
		$FullMarkArr = $lpf_academic ->Get_Subject_Full_Mark($this->All_AcademicYearID);
		
		
		for($a=0,$a_MAX=count($SubjectInfoOrderedArr);$a<$a_MAX;$a++)
		{
			$thisSubjectID = $SubjectInfoOrderedArr[$a]['SubjectID'];
			$thisSubjectID = $thisSubjectID=='658' ||$thisSubjectID=='657'? '639':$thisSubjectID;
			// 			$thisSubjectName = $SubjectInfoOrderedArr[$a]['SubjectDescEN'];
			$thisSubjectName = $SubjectIDNameAssoArr[$thisSubjectID][0];
			
			$IsStudy = false;
			for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
			{
				
				$passMark = 0;
				
				$thisYear = $YearClassArray[$i]['AcademicYear'];
				$thisClassName = $YearClassArray[$i]['ClassName'];
				
				$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
				
				$thisFormNo = (int)$thisClassName;
				preg_match('/\d+/', $thisClassName, $number);
				
				if(count($number)>0 && is_array($number))
				{
					$thisFormNo = $number[0];
				}
				
				
				$thisMark = $StudentAcademicResultInfoArr[$StudentID][$thisSubjectID][$thisAcademicYearID]['Score'];
				$thisGrade = $StudentAcademicResultInfoArr[$StudentID][$thisSubjectID][$thisAcademicYearID]['Grade'];
				
				if ($thisMark != 'N.A.' && $thisMark != '') {
					$IsStudy = true;
				}
				
				
				//	$thisSubjectPositionArr = $this->getSubjectPosition($thisSubjectID,$thisAcademicYearID);
				$thisFormPosition = $thisSubjectPositionArr['OrderMeritForm'];
				$thisTotalStd = $thisSubjectPositionArr['OrderMeritFormTotal'];
				
				$thisPercent = 100;
				
				$thisConditionArr = array();
				$thisConditionArr[] = $thisTotalStd!=0;
				$thisConditionArr[] = $thisFormPosition!=0;
				$thisConditionArr[] = $thisTotalStd!='';
				$thisConditionArr[] = $thisFormPosition!='';
				
				if(in_array(false,$thisConditionArr))
				{
				}
				else
				{
					$thisPercent = ($thisFormPosition/$thisTotalStd)*100;
				}
				
				$thisStar = '';
				if($thisPercent<=10)
				{
					$thisStar = '*';
				}
				
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Star'] = $thisStar;
				
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Score'] = $thisMark;
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Grade'] = $thisGrade;
				
				$YearName = 'S'.$thisFormNo;
				$thisYearID = $this->getYearID($YearName);
				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['FullMark'] =$FullMarkArr[$thisAcademicYearID][$thisYearID][$thisSubjectID];
				
			}
			//			$thisYearID = $this->getYearID($YearName);
			
			$NewSubjectSequenceArr[$thisSubjectID]['IsStudy']=$IsStudy;
			
			$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $thisSubjectName;
			
		}
		
		$colSpan_no = ($countColumn*2)+1;
		
		$html='';
		
		$html .='<table width="100%"  cellpadding="4" style="border-collapse: collapse;border:1px solid #000000;">';
		
		$html .='<col width="30%"/>';
		
		if($countColumn>0)
		{
			$thisWidth = 70/($countColumn*2);
		}
		
		for($i=0,$i_MAX=($countColumn*2);$i<$i_MAX;$i++)
		{
			$html .='<col width="'.$thisWidth.'%"/>';
		}
		
		$html .='<tr>
						<td colspan="'.$colSpan_no.'" style=" font-size:15px;text-align:left;vertical-align:text-top;"  bgcolor="'.$this->TableTitleBgColor.'"><font color="'.$this->TableTitleFontColor.'"><b>
						'.$MainTitle.'
						</b></font></td>
				  	</tr>';
		
		$html .='<tr>';
		$html .='<td bgcolor="'.$this->TableTitleRemarkBgColor.'" style="font-size:12px;text-align:center;padding-left:8px;'.$_borderBottom.$_borderTop.$_borderLeft.'"><b>&nbsp;</b></td>';
		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			$thisFormNo = (int)$thisClassName;
			preg_match('/\d+/', $thisClassName, $number);
			
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = $number[0];
			}
			
			
			//	$colspan ='colspan="2"';
			
			$thisBorder = $_borderBottom.$_borderTop;
			// 2020-04-03 (Philips) fill border
			if($i == $i_MAX-1) $thisBorder .= $_borderRight;
			
			$html .='<td '.$colspan.' bgcolor="'.$this->TableTitleRemarkBgColor.'" style="font-size:12px;text-align:center;'.$thisBorder.'"><b>'.$thisYear.'<br/>S'.$thisFormNo.'</b></td>';
		}
		$html .='</tr>';
		$html .='<tr>';
		$html .='<td style="font-size:13px;text-align:left;padding-left:8px;'.$_borderBottom.$_borderLeft.$_borderTop.'"><b>'.$Subject_string.'</b></td>';
		
		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{
			$thisClassName = $YearClassArray[$i]['ClassName'];
			$thisFormNo = (int)$thisClassName;
			preg_match('/\d+/', $thisClassName, $number);
			
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = $number[0];
			}
			
			$thisBorder = $_borderBottom.$_borderTop;
			// 2020-04-03 (Philips) fill border
			if($i == $i_MAX-1) $thisBorder .= $_borderRight;
			
			$html .='<td style="font-size:13px;text-align:center;'.$thisBorder.'"><b>'.$Mark_string.'</b></td>';
			//		$html .='<td style="font-size:13px;text-align:center;'.$_borderTop.$_borderBottom.'"><b>'.$FullMark_string.'</b></td>';
		}
		$html .='</tr>';
		
		// 2020-04-14 (Philips) - check last subject display
		$lastStudy = null;
		foreach((array)$NewSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr){
			$_IsStudy = $NewSubjectSequenceArr[$thisSubjectID]['IsStudy'];
			if($_IsStudy) $lastStudy = $thisSubjectID;
		}
		
		foreach((array)$NewSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr)
		{
			$_IsStudy = $NewSubjectSequenceArr[$thisSubjectID]['IsStudy'];
			$_fullMark = $NewSubjectSequenceArr[0]['FullMark'];
			$_fullMark = ($_fullMark=='')? $this->EmptySymbol:$_fullMark;
			
			if($_IsStudy==false)
			{
				continue;
			}
			$thisSubjectName = $thisSubjectInfoArr['SubjectNameEn'];
			$thisIsComponentSubject = $thisSubjectInfoArr['thisIsComponentSubject'];
			
			if($thisIsComponentSubject)
			{
				$fontSize='9px';
			}
			else
			{
				$fontSize='13px';
			}
			
			$html .='<tr>';
			
			$thisSubjectBorder = $_borderLeft;
			// 2020-04-03 (Philips) fill border
// 			if($thisSubjectInfoArr == end($NewSubjectSequenceArr)) $thisSubjectBorder .= $_borderBottom;
			if($thisSubjectID == $lastStudy) $thisSubjectBorder .= $_borderBottom;
			
			$html .='<td style="padding-left:9px;font-size:'.$fontSize.';text-align:left;'.$thisSubjectBorder.' nowrap">'.$thisSubjectName.'</td>';
			
			for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
			{
				$thisYear = $YearClassArray[$i]['AcademicYear'];
				$thisClassName = $YearClassArray[$i]['ClassName'];
				$thisFormNo = (int)$thisClassName;
				preg_match('/\d+/', $thisClassName, $number);
				
				if(count($number)>0 && is_array($number))
				{
					$thisFormNo = $number[0];
				}
				
				$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
				
				if($thisAcademicYearID==1 || $thisAcademicYearID==7) // 2009-2010   2010-2011
				{
					$passMark = 40;
				}
				else
				{
					$passMark = $this->passMark;
				}
				if($_SESSION['UserID'] == '2377'){
					// 					debug_pr($passMark);
				}
				
				$thisStar = $thisSubjectInfoArr[$thisYear]['Star'] ;
				
				$thisMark = $thisSubjectInfoArr[$thisYear]['Score'] ;
				if ($thisMark == 'N.A.' || $thisMark == '' || $thisMark<0) {
					$thisMark = $this->EmptySymbol;
				}
				
				$thisFullMark = $thisSubjectInfoArr[$thisYear]['FullMark'] ;
				if ($thisFullMark == '' ) {
					$thisFullMark = $this->EmptySymbol;
				}
				
				if($passMark=='' || $thisMark==$this->EmptySymbol)
				{
				}
				else
				{
					if($thisMark>=$passMark)
					{
					}
					else
					{
						$thisMark = '('.$thisMark.')';
					}
				}
				
				$thisMarkBorder = '';
				// 2020-04-03 (Philips) fill border
				if($i == $i_MAX-1) $thisMarkBorder .= $_borderRight;
// 				if($thisSubjectInfoArr == end($NewSubjectSequenceArr)) $thisSubjectBorder .= $_borderBottom;
				if($thisSubjectID == $lastStudy) $thisSubjectBorder .= $_borderBottom;
				
				$html .='<td style="padding-left:13px;font-size:'.$fontSize.';text-align:center;'.$thisMarkBorder.'">'.$thisMark.'</td>';
				//			$html .='<td style="padding-left:13px;font-size:'.$fontSize.';text-align:center;">'.$thisFullMark.'</td>';
				
			}
			
			$html .='</tr>';
		}
		
		$html .='<tr>';
		
		
		if($countColumn==0)
		{
			$noRecord = 'No Record Found.';
			$html .='<td style="padding-left:9px;font-size:13px;text-align:center;'.$_borderLeft.$_borderRight.'">'.$noRecord.'</td>';
		}
		
		
		$html .='</tr>';
		
		$html .='</table>';
		
		$html .= '<table width="100%" height="1%" cellpadding="0" border="0px" align="center" style="">
	   			 <tr>
				    <td style="text-align:left;font-size:13px;" >( ): fail</td>
				 </tr>
			  </table>';
		
		return $html;
		
	}
	
	
	/***************** End Part 3 ******************/
	
	/***************** Part 4 : Other Learning Experience******************/
	public function getPart4_HTML($OLEInfoArr_INT){
		
		$Part4_Title = '<b>Other Learning Experiences</b>';
		$Part4_Remark = '*Apart from explaining what the programme is about, the description also shows briefly what knowledge, generic skills, ';
		$Part4_Remark .= 'values and attitudes would be developed through the experience.<br/><br/>';
		$Part4_Remark .= '**Evidence of awards/certifications/achievements listed is available for submission when required.<br/><br/>';
		$Part4_Remark .= 'Remarks: The above list, which does not mean to be exhaustive, merely illustrates the \'key\' learning experiences acquired ';
		$Part4_Remark .= 'by the student throughout the senior secondary years.';
		
		$Part4_HeaderRemark ='Other Learning Experiences can be achieved through programmes organised by the school or co-organised by the school ';
		$Part4_HeaderRemark .='with outside organisations. They may include learning experiences implemented during time-tabled and/or non-time-tabled ';
		$Part4_HeaderRemark .='learning time. Apart from core and elective subjects, Other Learning Experiences that the student participates in during ';
		$Part4_HeaderRemark .='his/her senior secondary education include Moral and Civic Education, Aesthetic Development, Physical Development, ';
		$Part4_HeaderRemark .='Community Service and Career-related Experiences.';
		
		$titleArray = array();
		$titleArray[] = 'Programmes<br/>(with description)*';
		$titleArray[] = 'School Year';
		$titleArray[] = 'Role of Participation';
		$titleArray[] = 'Partner Organisations<br/>(if any)';
		$titleArray[] = 'Major Components<br/>of Other Learning Experiences';
		$titleArray[] = 'Awards / Certifications / Achievements**(if any)';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="29%" />';
		$colWidthArr[] = '<col width="13%" />';
		$colWidthArr[] = '<col width="13%" />';
		$colWidthArr[] = '<col width="18%" />';
		$colWidthArr[] = '<col width="8%" />';
		$colWidthArr[] = '<col width="19%" />';
		
		$DataArray = array();
		$DataArray = $OLEInfoArr_INT;
		
		$StringDisplayArr ['Title']= $Part4_Title;
		$StringDisplayArr ['Remark']=  $Part4_Remark;
		
		$html = '';
		
		$html .= $this->getPartContentDisplay_PageRowLimit('4',$titleArray,$colWidthArr,$DataArray,$hasPageBreak=true,$StringDisplayArr,$Part4_HeaderRemark);
		
		return $html;
		
	}
	/***************** End Part 4 ******************/
	
	/***************** Part 5 ******************/
	public function getPart5_HTML(){
		
		$Part5_Title = '<b>List of Awards and Major Achievements Issued by the School</b>';
		$Part5_Remark = '';
		
		$Part5_HeaderRemark = '';
		
		$titleArray = array();
		$titleArray[] = 'Year';
		$titleArray[] = 'Awards and Achievements';
		$titleArray[] = 'Remarks';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="50%" />';
		$colWidthArr[] = '<col width="30%" />';
		
		$DataArr = array();
		$DataArr = $this->getAward_Info();
		
		$StringDisplayArr ['Title']= $Part5_Title;
		$StringDisplayArr ['Remark']=  $Part5_Remark;
		
		$html = '';
		
		$html .= $this->getPartContentDisplay_NoRowLimit('5', $Part5_Title,$titleArray,$colWidthArr,$DataArr,$hasPageBreak=false,$StringDisplayArr,$Part5_HeaderRemark);
		
		return $html;
		
	}
	
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$_UserID = $this->uid;
		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And a.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						AwardName,
						Remark
						From
						$Award_student as a
						inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = a.AcademicYearID
						
						Where
						1
						$cond_studentID
						$cond_academicYearID
						order by
						y.Sequence asc,
						AwardName asc
						
						";
						$roleData = $this->objDB->returnResultSet($sql);
						
						return $roleData;
	}
	
	/***************** End Part 5 ******************/
	
	
	/***************** Part 6 ******************/
	public function getPart6_HTML($OLEInfoArr_EXT){
		
		$Part6_Title = '<b>Performances / Awards Gained Outside of School</b>';
		$Part6_Remark = 'Evidence of awards/ certifications/ achievements listed is available for submission when required.';
		
		$Part6_HeaderRemark ='For learning programmes not organised by the school during the senior secondary education period, ';
		$Part6_HeaderRemark .='students may provide information to the school.';
		
		$titleArray = array();
		$titleArray[] = 'Programmes<br/>(with description)*';
		$titleArray[] = 'School Year';
		$titleArray[] = 'Role of Participation';
		$titleArray[] = 'Partner Organisations<br/>(if any)';
		$titleArray[] = 'Awards / Certifications / Achievements**(if any)';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="24%" />';
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="21%" />';
		
		//$DataArr = array();
		//$DataArr = $this->getAward_Info();
		
		$StringDisplayArr ['Title']= $Part6_Title;
		$StringDisplayArr ['Remark']=  $Part6_Remark;
		
		$html = '';
		
		$html .= $this->getPartContentDisplay_PageRowLimit('6',$titleArray,$colWidthArr,$OLEInfoArr_EXT,$hasPageBreak=true,$StringDisplayArr,$Part6_HeaderRemark);
		
		return $html;
		
	}
	
	/***************** End Part 6 ******************/
	
	
	/***************** Part 7 ******************/
	
	public function getPart7_HTML(){
		
		$Part7_Title = "Student's Self-account";
		$Part7_Remark = '';
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();
		
		
		$StringDisplayArr ['Title']= $Part7_Title;
		$StringDisplayArr ['Remark']=  $Part7_Remark;
		
		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$StringDisplayArr) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$_UserID = $this->uid;
		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
		Details
		From
		$self_acc_std
		Where
		1
		$cond_studentID
		$cond_DefaultSA
		
		";
		$roleData = $this->objDB->returnResultSet($sql);
		//hdebug_r($sql);
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			$thisDataArr['Details'] = strip_tags($thisDataArr['Details'],'<p><br>');
			
			$returnDate[] = $thisDataArr;
			
		}
		
		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$StringDisplayArr='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';
		
		$title_font_size = '16px';
		$content_font_size = '13px';
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:14px;" ';
		
		
		$html = '';
		
		$rowMax_count = count($DataArr);
		
		if($rowMax_count<=0)
		{
			$html =$this->getEmptyTable('5%');
			//			$html .= $this->getPart1_HTML();
			//			$html .= $this->getPart2_HTML();
			
			$html .= '<table cellpadding="0" style="" width="100%">
						<tr>
							<td height="960px" valign="top">
								<table width="'.$this->TableWidth.'" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>
									<tr>
										<td style="vertical-align: top;font-size:'.$title_font_size.';'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'text-align:left;height:10px;" bgcolor="'.$this->TableTitleBgColor.'"><font color="'.$this->TableTitleFontColor.'"><b>'.$StringDisplayArr['Title'].'</b></font></td>
									</tr>
									<tr>
										<td style="vertical-align: top;font-size:'.$content_font_size.';'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'text-align:center;"><b>No record found.</b></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>';
			$html .=$this->getFooter_HTML($Page_break,$this->PageNo);
			$this->PageNo++;
			
		}
		else if ($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			
			$DetailArr = explode($this->SelfAccount_Break,$Detail);
			
			for ($i=0,$i_MAX=count($DetailArr); $i<$i_MAX; $i++)
			{
				if($i==$i_MAX-1)
				{
					$Page_break='';
				}
				
				$html .= $this->getEmptyTable('5%');
				//				$html .= $this->getPart1_HTML();
				//				$html .= $this->getPart2_HTML();
				
				//user type some space at the font of a paragraph to make it intent, since HTML do not support multiple " " , so, do the replace ment
				$accountDetails = str_replace("  ","&nbsp;&nbsp;",$DetailArr[$i]);
				
				$html .= '<table cellpadding="0" style="" width="100%">';
				$html .= '<tr>
								<td height="960px" valign="top">
									<table width="'.$this->TableWidth.'" height="650px" cellpadding="7" border="0px" align="center" '.$table_style.'>
										<tr>
											<td style="vertical-align: top;font-size:'.$title_font_size.';'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'text-align:left;height:10px;" bgcolor="'.$this->TableTitleBgColor.'"><font color="'.$this->TableTitleFontColor.'"><b>'.$StringDisplayArr['Title'].'</b></font></td>
										</tr>
										<tr>
											<td style="vertical-align: top;font-size:'.$content_font_size.';'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'font-size:16px;"><font face="Times New Roman">'.$accountDetails.'</font></td>
										</tr>
									</table>
								</td>
							 </tr>';
				$html .='</table>';
				$html .=$this->getFooter_HTML($Page_break,$this->PageNo);
				$this->PageNo++;
				
			}
		}
		
		return $html;
	}
	
	
	/***************** End Part 7 ******************/
	
	
	
	/***************** Footer ******************/
	public function getFooter_HTML($PageBreak='',$ParPageNo='')
	{
		$FooterStr = $this->StdName.' / P.'.$ParPageNo;
		
		$_borderBottom =  'border-bottom: 1px solid black; ';
		
		$html = '';
		
		$html .= '<table width="'.$this->TableWidth.'" cellpadding="0" border="0px" align="center" style="'.$PageBreak.'">
				
				 	<tr>
						<td style="text-align:right;">'.$FooterStr.'</td>
					</tr>
								
				  </table>';
		return $html;
		
	}
	/***************** END Footer ******************/
	
	
	
	
	private function getStringDisplay($String='',$class_html='',$Page_break='')
	{
		$html = '';
		$html .= '<table width="'.$this->TableWidth.'" cellpadding="7" border="0px" align="center" class="'.$class_html.'" style="" '.$Page_break.'>
		   			 <tr>
					    <td><font style="">'.$String.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	
	
	private function getPartContentDisplay_PageRowLimit($PartNum, $subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false,$StringDisplayArr='',$HeaderRemark='')
	{
		$Page_break='page-break-after:always;';
		$Page_break = ($hasPageBreak==false)? '':$Page_break;
		
		
		/****** Settings *****/
		###		$RowPerPage : it indicate the number of rows perpage. If you want to set 2 row perpage, change it to 2;
		###     $RowHeight  : the height of each row
		###	    $font_size  : the font size of data
		###     $footer_html: the signature footer
		
		
		$RowPerPage = 5;
		$RowHeight = 60;
		$title_font_size = '13px';
		$content_font_size = '13px';
		
		
		//	$footer_html = $this->getFooter_HTML($Page_break); //principal signature
		
		/***** End Settings *********/
		
		
		$rowMax_count = count($DataArr);
		$RowHeight_style = 'height="'.$RowHeight.'px"';
		
		$count_col =count($colWidthArr);
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$colSpanStr = '';
		
		$text_alignStr ='center;';
		
		$html = '';
		
		$html .=$this->getEmptyTable('5%');
		
		$html .= '<table align="center" width="'.$this->TableWidth.'" height="960px" cellpadding="0" style="">
						<tr><td valign="top">';
		$html .= '<table class="tabletext" height="" width="100%" cellpadding="7" border="0px" align="center" style="border-collapse: collapse;">';
		
		$html .= ' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$title_font_size.';" bgcolor="'.$this->TableTitleBgColor.'"><font color="'.$this->TableTitleFontColor.'">'.$StringDisplayArr['Title'].'</font></td></tr>';
		$html .= ' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$content_font_size.';" bgcolor="'.$this->TableTitleRemarkBgColor.'">'.$HeaderRemark.'</td></tr>';
		$html .= '</table>';
		$html .=$this->getEmptyTable('1%');
		
		
		//		$HeaderInfo .= '<table align="center" width="'.$this->TableWidth.'" height="980px" cellpadding="0" style="">
		//						<tr><td valign="top">';
		
		$HeaderInfo .= '<table class="tabletext" height="" width="100%" cellpadding="4" border="0px" align="center" style="border-collapse: collapse;">';
		
		foreach((array)$colWidthArr as $key=>$_colWidthInfo)
		{
			$HeaderInfo .=$_colWidthInfo;
		}
		
		$HeaderInfo .= '<tr '.$RowHeight_style.'>';
		$k=0;
		foreach((array)$subTitleArr as $key=>$_title)
		{
			if($k==0)
			{
				$HeaderInfo .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;font-size:'.$title_font_size.';'.$_borderLeft.$_borderTop.$_borderRight.'">';
			}
			else
			{
				$HeaderInfo .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;font-size:'.$title_font_size.';'.$_borderTop.$_borderRight.'">';
			}
			
			$HeaderInfo .='<b>'.$_title.'</b>';
			$HeaderInfo .='</td>';
			$k++;
		}
		$HeaderInfo .= '</tr>';
		
		
		//		$tableBottomInfo .= '<tr><td align="left" style="font-size:'.$content_font_size.';" colspan="'.count($colWidthArr).'">'.$StringDisplayArr['Remark'].'</td></tr>';
		$tableBottomInfo .='</table>';
		$tableBottomInfo .='</td></tr></table>';
		$tableBottomInfo .=$footer_html;
		
		if($rowMax_count<=0)
		{
			$content_html ='<tr>';
			
			for ($i=0; $i<$count_col; $i++)
			{
				$content_html .='<td style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$content_font_size.';text-align:'.$text_alignStr.'">'.$this->EmptySymbol.'</td>';
			}
			$content_html .='</tr>';
			
			$html .=$HeaderInfo;
			$html .=$content_html;
			$html .=$tableBottomInfo;
			$html .=$this->getFooter_HTML($Page_break,$this->PageNo);
			$this->PageNo++;
		}
		
		else if($rowMax_count>0)
		{
			### 15 rows in one page
			$NumOfDisplayedRow = 0;
			$SetHeader=false;
			
			for ($i=0; $i<$rowMax_count; $i++)
			{
				$NumOfDisplayedRow = $i;
				
				$content_html = '';
				$content_html .= '<tr '.$RowHeight_style.'>';
				
				$k=0;
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{
					$text_Align= 'left';
					
					if($_titleVal=='')
					{
						$_titleVal = $this->EmptySymbol;
					}
					
					$content_html .= ' <td style="text-align:'.$text_Align.';vertical-align:top;font-size:'.$content_font_size.';'.$_borderTop.$_borderRight.$_borderLeft.$_borderBottom.'">'.$_titleVal.'</td>';
					
					$k++;
				}
				$content_html .= '</tr>';
				//debug_r($this->PageNo);
				if($NumOfDisplayedRow==0)  // if it is first row, add the header
				{
					$header_html=$HeaderInfo;
				}
				else
				{
					$header_html='';
					
					if(($NumOfDisplayedRow+1)%$RowPerPage==0)   // if it is  10th or 20th or 30th ....row (last row), add </table>
					{
						$bottom_html = $tableBottomInfo;
						$bottom_html .=$this->getFooter_HTML($Page_break,$this->PageNo);
						$this->PageNo++;
						$SetHeader=true;
					}
					else if($SetHeader==true) // if it is 11th or 21th or 31th....row (first row), add the header
					{
						$header_html =$this->getEmptyTable('5%');
						$header_html .= '<table align="center" width="'.$this->TableWidth.'" height="960px" cellpadding="0" style="">
											<tr><td valign="top">';
						$header_html .=$HeaderInfo;
						$SetHeader=false;
						
					}
					
				}
				
				if($NumOfDisplayedRow >= $rowMax_count-1)  // if it is the last row , add </table>
				{
					if($bottom_html=='')
					{
						$bottom_html = '<tr><td align="left" style="font-size:'.$content_font_size.';" colspan="'.count($colWidthArr).'">'.$StringDisplayArr['Remark'].'</td></tr>';
						$bottom_html .= $tableBottomInfo;
						$bottom_html .=$this->getFooter_HTML($Page_break,$this->PageNo);
						$this->PageNo++;
					}
				}
				
				$html .= $header_html;
				$html .= $content_html; // it is the subject content
				$html .= $bottom_html;
				
				$header_html='';
				$bottom_html='';
				
			}
		}
		
		return $html;
	}
	
	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false,$StringDisplayArr='',$HeaderRemark='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';
		
		##Setting
		$total_Record=5;
		$rowHight = 40;
		
		
		$BgColor = $this->TableTitleBgColor;
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		$table_style = 'style="font-size:12px;border-collapse: collapse;border:1px solid #000000;"';
		
		$subtitle_html = '';
		
		$title_font_size = '16px';
		$content_font_size = '13px';
		
		$text_alignStr='center';
		$subtitle_html= '';
		foreach((array)$subTitleArr as $key=>$_title)
		{
			$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;font-size:'.$title_font_size.';"><b>';
			$subtitle_html .=$_title;
			$subtitle_html .='</b></td>';
		}
		
		
		$content_html='';
		$rowMax_count = 0;
		$rowMax_count = count($DataArr);
		$count_col =count($colWidthArr);
		
		if($rowMax_count<=0)
		{
			$content_html ='<tr>';
			
			for ($i=0; $i<$count_col; $i++)
			{
				$content_html .='<td style="font-size:'.$content_font_size.';text-align:'.$text_alignStr.'">'.$this->EmptySymbol.'</td>';
			}
			$content_html .='</tr>';
			
		}
		
		else if($rowMax_count>0)
		{
			
			for ($i=0; $i<$rowMax_count; $i++)
			{
				$content_html .= '<tr>';
				
				$j=0;
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{
					if($_titleVal=='')
					{
						$_titleVal =  $this->EmptySymbol;
					}
					$text_alignStr = 'left';
					
					if($j==0 || j==2)
					{
						$text_alignStr = 'center';
					}
					
					$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;font-size:'.$content_font_size.';">'.$_titleVal.'</td>';
					$j++;
				}
				$content_html .= '</tr>';
			}
		}
		
		
		$colNumber = count($colWidthArr);
		
		/****************** Main Table ******************************/
		
		$html = '';
		
		$html = '<table align="center" width="100%" height="960px" cellpadding="0" style="">
						<tr><td valign="top">';
		$html .= '<table width="'.$this->TableWidth.'" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>';
		foreach((array)$colWidthArr as $key=>$_colWidthInfo)
		{
			$html .=$_colWidthInfo;
		}
		$html .= ' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$title_font_size.';" bgcolor="'.$this->TableTitleBgColor.'"><font color="'.$this->TableTitleFontColor.'">'.$StringDisplayArr['Title'].'</font></td></tr>';
		
		if($PartNum!='5')
		{
			$html .= ' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderRight.$_borderBottom.'font-size:'.$content_font_size.';" bgcolor="'.$this->TableTitleRemarkBgColor.'">'.$HeaderRemark.'</td></tr>';
		}
		
		
		$html .= '<tr>'.$subtitle_html.'</tr>';
		
		$html .= $content_html;
		
		$html .='</table>';
		
		$html .= '<table width="'.$this->TableWidth.'" height="" cellpadding="7" border="0px" align="center" style="font-size:11px;border-collapse: collapse;">';
		
		$html .= '<tr><td align="left" style="font-size:'.$content_font_size.';" colspan="'.count($colWidthArr).'">'.$StringDisplayArr['Remark'].'</td></tr>';
		$html .='</table>';
		$html .='</td></tr></table>';
		$html .=$this->getFooter_HTML($Page_break,$this->PageNo);
		$this->PageNo++;
		
		return $html;
	}
	
	
	public function getStudentAcademic_YearClassNameArr($IsTitle=false)
	{
		$StudentID = $this->uid;
		
		$academicYearIDArr = $this->academicYearIDArr;
		
		$SelectStr= 'h.AcademicYear as AcademicYear,
					 h.ClassName as ClassName';
		
		if($academicYearIDArr!='')
		{
			$YearArr = array();
			for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
			{
				if($academicYearIDArr[$i]=='')
				{
					continue;
				}
				$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
				
				if($IsTitle)
				{
					$thisAcademicYear = substr($thisAcademicYear,0,4);
				}
				
				$YearArr[] = $thisAcademicYear;
			}
			
			$cond_academicYear = "And h.AcademicYearID In ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select
						'.$SelectStr.'
				from
						'.$Profile_his.' as h
				where
						h.UserID = "'.$StudentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear asc';
		
		$roleData = $this->objDB->returnResultSet($sql);
		
		//hdebug_r($sql);
		return $roleData;
	}
	
	
	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false)
	{
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM');
		$OLE_CATE = $this->Get_eClassDB_Table_Name('OLE_CATEGORY');
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}
		
		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null";
		}
		
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
		
		$OLEcomponentArr = $this->getOLEcomponentName();
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = "And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$selectStr = "ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						ole_prog.Details as detail,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent";
		
		if($IntExt=='INT')
		{
			$cond_limit = 'limit 20';
		}
		else if($IntExt=='EXT')
		{
			$selectStr = "  ole_prog.Title as Title,
							y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
							ole_std.role as Role,
							ole_prog.Organization as Organization,
							ole_std.Achievement as Achievement";
		}
		
		//		$order_by = 'y.Sequence asc,
		//						ole_prog.Title asc	';
		
		$order_by = 'SLPOrder asc';
		
		
		$sql = "Select
		$selectStr
		From
		$OLE_STD as ole_std
		Inner join
		$OLE_PROG as ole_prog
		On
		ole_std.ProgramID = ole_prog.ProgramID
		
		inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
		
		Where
		1
		$cond_studentID
		$cond_academicYearID
		$cond_IntExt
		$cond_SLPOrder
		$cond_Approve
		order by
		$order_by
		$cond_limit
		
		";
		$roleData = $this->objDB->returnResultSet($sql);
		
		$longLine = '--------------------------';
		
		if($IntExt=='INT')
		{
			for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
			{
				$_StudentID = $roleData[$i]['StudentID'];
				$_IntExt = $roleData[$i]['IntExt'];
				$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
				
				$thisDataArr = array();
				
				$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
				
				for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
				{
					$thisOleComponentID = $thisOleComponentArr[$a];
					$thisOleComponentArr[$a] = $OLEcomponentArr[$thisOleComponentID];
				}
				$thisOLEstr = implode(", ",(array)$thisOleComponentArr);
				
				$descStr='';
				if($roleData[$i]['detail']!='')
				{
					$descStr = '<br/>'.$longLine.'<br/>'.$roleData[$i]['detail'];
				}
				
				$thisDataArr['Program'] =$roleData[$i]['Title'].$descStr;
				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisRole =$roleData[$i]['Role'];
				$thisDataArr['Role'] = $thisRole;
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
				$thisDataArr['OLEcomponent'] =$thisOLEstr;
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
				
				$returnData[] = $thisDataArr;
			}
			return $returnData;
		}
		
		else if($IntExt=='EXT')
		{
			return $roleData;
		}
		
		return '';
	}
	
	public function getOLEcomponentName()
	{
		$OLE_ELE = $this->Get_eClassDB_Table_Name('OLE_ELE');
		
		$sql="Select
		EngTitle,
		ChiTitle,
		DefaultID
		from
		$OLE_ELE
		";
		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnArray = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisName_en = $roleData[$i]['EngTitle'];
			$thisName_ch = $roleData[$i]['ChiTitle'];
			$thisID = $roleData[$i]['DefaultID'];
			
			$returnArray[$thisID] = $thisName_en;
		}
		
		return $returnArray;
	}
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
			$firstYear = substr($returnArray[0], 0, 4);
			$lastYear = substr($returnArray[$lastNo],-4);
			
			$returnValue = $firstYear."-".$lastYear;
			
			return $returnValue;
		}
		
		
	}
	/********** Display Form 4, 5 and 6  ***********/
	private function getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,$order='asc')
	{
		
		$YearDisplayArr = array();
		
		$Form4_no = 0;
		$Form5_no = 0;
		$Form6_no = 0;
		
		
		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			
			$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
			
			$thisFormNo = (int)$thisClassName;
			
			preg_match('/\d+/', $thisClassName, $number);
			
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = $number[0];
				
				//				if($order=='asc')
				//				{
				//					if($thisFormNo==4 || $thisFormNo==5 || $thisFormNo==6)
				//					{
				//						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
				//					}
				//				}
				//				else if($order=='desc')
				//				{
				//					$CondArr = array();
				//					$CondArr[] = ($thisFormNo==4 && $Form4_no==0);
				//					$CondArr[] = ($thisFormNo==5 && $Form5_no==0);
				//					$CondArr[] = ($thisFormNo==6 && $Form6_no==0);
				//
				//					if(in_array(true,$CondArr))
				//					{
				//						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
				//					}
				//				}
				//
				//				if($thisFormNo==4)
				//				{
				//					$Form4_no++;
				//				}
				//				else if($thisFormNo==5)
				//				{
				//					$Form5_no++;
				//				}
				//				else if($thisFormNo==6)
				//				{
				//					$Form6_no++;
				//				}
				
				
				if($thisFormNo==4 || $thisFormNo==5 || $thisFormNo==6)
				{
					$YearDisplayArr[] = $YearClassArray[$i];
				}
				
			}
		}
		
		$ThisClassArr = array();
		$count_YearDisplayArr = 0;
		foreach((array)$YearDisplayArr as $_formNo=>$_ClassArr)
		{
			$ThisClassArr[$count_YearDisplayArr] = $_ClassArr;
			$count_YearDisplayArr++;
		}
		
		return $ThisClassArr;
	}
	
	private function getAcademicYearArr($academicYearIDArr,$IsTrim=false)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			
			if($IsTrim)
			{
				$thisAcademicYear = substr($thisAcademicYear,0,4);
			}
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='') {
		
		
		$html = '';
		$html .= '<table width="'.$this->TableWidth.'" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">
		   			 <tr>
					    <td>&nbsp;</td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="'.$this->TableWidth.'" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable)
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable)
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>