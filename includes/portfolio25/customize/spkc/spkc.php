<?php
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/spkc/libpf-slp-RptMgr.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/customize/spkc/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libpf-report.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-academic.php");



$YearClassID = trim($YearClassID);
$issuedate  = trim($issuedate);


//debug_r($_POST);



$objReportMgr = new RptMgr();


$objReportMgr->setYearClassID($YearClassID);

$objReportMgr->setStudentID($StudentID);

$objReportMgr->setAcademicYearID($academicYearID);
$academicYearStartEndDisplay = $objReportMgr->getAcademicYearDisplayMultiple();

$studentList = $objReportMgr->getStudentIDList();

for($i = 0,$i_max = sizeof($studentList);$i < $i_max; $i++){
	
	$_studentId = $studentList[$i];
	
	$objStudentInfo = new studentInfo($_studentId,$academicYearID,$issuedate);
	$objStudentInfo -> setAcademicYear($academicYearID);
	$objStudentInfo -> setAcademicYearStr();
	$objStudentInfo -> setYearRange();
	
	$objStudentInfo -> setAll_AcademicYearID($All_AcademicYearID);

	$OLEInfoArr_INT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='INT',$Selected=false);
	$OLEInfoArr_EXT = $objStudentInfo->getOLE_Info($_studentId,$academicYearID,$IntExt='EXT',$Selected=false);

	### Page Content
	
	$html = '<table align="center" width="100%" height="1030px" cellpadding="0" style="">
						<tr><td valign="top">';
	$html .=$objStudentInfo->getEmptyTable('5%');
	$html .=$objStudentInfo->getPart1_HTML();
	$html .=$objStudentInfo->getPart2_HTML();
	$html .= $objStudentInfo->getPart3_HTML();	
	
	$html .='</td></tr></table>';
	$html .= $objStudentInfo->getFooter_HTML($Page_break,$objStudentInfo->getPageNo()); 	
	$html .=$objStudentInfo->getPagebreakTable();
	$objStudentInfo->addPageNo();
	
	$html .= $objStudentInfo->getPart4_HTML($OLEInfoArr_INT);	
	
	$html .=$objStudentInfo->getEmptyTable('5%');
	$html .= $objStudentInfo->getPart5_HTML();
	$html .=$objStudentInfo->getPagebreakTable();
	
	$html .= $objStudentInfo->getPart6_HTML($OLEInfoArr_EXT);
	$html .= $objStudentInfo->getPart7_HTML();


	
	if($i!=$i_max-1)
	{
		$html .=$objStudentInfo->getPagebreakTable();
	}

}


if($PrintType == "word"){
	$objReportMgr->outPutToWord($html);
}else{
	echo $objReportMgr->outPutToHTML($htmlCSS.$html);
}


exit();
?>