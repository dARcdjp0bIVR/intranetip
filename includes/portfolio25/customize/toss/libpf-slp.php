<?php
/*
 * Modification Log:
 *
 * 2020-09-22 (Bill) getSignature_HTML() - update Principal name    [2020-0918-0945-37235]
 * 2014-11-21 (Omas) getSignature_HTML() - change Acting Principal to Principal (2014-1120-1446-00073)
 * 
 */
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	
	private $YearClassID;
	
	private $AcademicYearStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '--';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->SchoolTitleEn = 'TAK OI SECONDARY SCHOOL';
		$this->SchoolTitleCh = '德 愛 中 學';
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	

	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '23';
	
		$shortSpace='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		$AddressStr = '8, TSZ WAN SHAN ROAD, TSZ WAN SHAN, KOWLOON, HONG KONG';
		$TelStr = 'Tel. No.: 23238504'.$shortSpace.'Fax. No.:27261153'.$shortSpace.'Website: www.takoi.edu.hk';
		
		$thisName = $this->getStudentName();
		
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">';
			$x .= '<tr><td colspan="3" style="font-size:11px; text-align:right;"><font face="Arial">'.$thisName.'</font></td></tr>';
//			$x .= '	<tr>
//						<td rowspan="8" align="center" " style="width:18%;">&nbsp;</td>
//						<td style="font-size:'.$header_font_size.'px;"><font face="Arial"><b>'.$this->SchoolTitleEn.'</b></font></td>
//						<td rowspan="8" align="left" style="width:18%;">&nbsp;</td>
//				   </tr>';
//			$x .= '<tr><td style="font-size:11px;">&nbsp;</td></tr>';
//			$x .= '<tr><td style="font-size:'.$header_font_size.'px;"><font face="Arial"><b>'.$this->SchoolTitleCh.'</b></font></td></tr>';
//			$x .= '<tr><td style="font-size:11px;">&nbsp;</td></tr>';
//			$x .= '<tr><td style="font-size:11px;"><font face="Arial">'.$AddressStr.'</font></td></tr>';
//			$x .= '<tr><td style="font-size:11px;"><font face="Arial">'.$TelStr.'</font></td></tr>';
//			$x .= '<tr><td style="font-size:5px;">&nbsp;</td></tr>';
			$x .= '<tr><td style="height:140px;font-size:5px;">&nbsp;</td></tr>';
//			$x .= '<tr><td style="font-size:18px;"><font face="Arial"><b>STUDENT LEARNING PROFILE'.$shortSpace.'學生學習概覽</b></font></td></tr>';
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	private function getStudentName(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$resultArr = $this->objDB->returnResultSet($sql);
		
		$result='';
		if(count($resultArr)>0)
		{
			$result = $resultArr[0]['EnglishName'];
		}
		return $result;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Awards / Scholarships ******************/
	public function getPart2_HTML(){
		
		$Part2_Title = 'Awards / Scholarships';
				
		$DataArray = array();
		$DataArray = $this->getAward_Info();  

		$html = '';
	
		if(count($DataArray)>0)
		{
			$html .= $this->getAwardDisplay($Part2_Title,$DataArray,$hasPageBreak=false);
		}

		return $html;		
	}	
	   
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'"; 
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And s.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						AwardName
				From
						$Award_student s
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = s.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence asc, 
						AwardName asc		

				";
		$roleData = $this->objDB->returnResultSet($sql);

		$returnArr = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisYearName = $roleData[$i]['YEAR_NAME'];
			$thisYearName = str_replace("-", " / ", $thisYearName);
			
			$thisAward = $roleData[$i]['AwardName'];
			$returnArr[$thisYearName][] = $thisAward;
		}
		return $returnArr;
	}
	
	private function getAwardDisplay($MainTitle,$DataArray,$hasPageBreak=false)
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
		
		$html='';
		
		$html .= '<table width="90%" height="" cellpadding="5" border="0px" align="center" '.$table_style.';">
					<tr> 
						<td style="font-size:14px;text-align:left;vertical-align:text-top;'.$_borderBottom.'"><font face="Arial"><b>
						'.$MainTitle.'
						</b></font></td>
				  	</tr>';
				  	
				  	foreach((array)$DataArray as $thisYearName=>$thisAwardArr)
				  	{
				  		$html .= '<tr> 
									<td style=" font-size:14px;text-align:left;vertical-align:text-top;'.$_borderTop.'"><font face="Arial"><b>
									'.$thisYearName.'
									</b></font></td>
							  	  </tr>';
						$thisAwardName = implode('<br/>',(array)$thisAwardArr);

						$html .= '<tr> 
									<td style=" font-size:11px;text-align:left;vertical-align:text-top;"><font face="Arial">
									'.$thisAwardName.'
									</font></td>
							  	  </tr>';			  	
				  	}			  	
				  	
		$html .= ' </table>';
		
		return $html;
	}
	
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Performance / Awards and Key Performance Outside School******************/	
	public function getPart3_HTML($OLEInfoArr){
		
		$Part3_Title = 'Performance / Awards and Key Participation Outside School';
			
		$titleArray = array();
		$titleArray[] = 'Programme';
		$titleArray[] = 'Organizer';
		$titleArray[] = 'Role of<br/>Participation';
		$titleArray[] = 'Awards / Certifications /<br/>Achievements (if any)';

		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="25%" />';

				
		$html = '';
	
		if(count($OLEInfoArr)>0)
		{
			$html .= $this->getOLE_Display($Part3_Title,$colWidthArr,$titleArray,$OLEInfoArr,$hasPageBreak=false);
		}
		return $html;	
	}		


	/***************** End Part 3 ******************/
	
	/***************** Part 4 ******************/	
	public function getPart4_HTML(){
		
		$Part4_Title = 'Participation in Student Activities';
		
		$subTitleArray= 'Post / Status';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="70%" />';
		$colWidthArr[] = '<col width="30%" />'; 
		  
		$DataArr = array();

		$DataArray = $this->getStudentService();	

		$html = '';
		$html .= $this->getStdActivity_Display($Part4_Title,$colWidthArr,$subTitleArray,$DataArray,$hasPageBreak=false);
		
		return $html;
		
	}
	
	private function getStudentService()
	{
		//Select * From SERVICE_STUDENT Limit 100
		
		global $ipf_cfg;
		$service_std = $this->Get_eClassDB_Table_Name('SERVICE_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA is not null";
		
		$thisAcademicYearArr = $this->academicYearIDArr;
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And sa.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						ServiceName,
						Role,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' 
				From
						$service_std sa
						inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = sa.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by 
						y.Sequence asc	,
						sa.ServiceName asc


				";//limit 20
		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnData = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisYearName = $roleData[$i]['YEAR_NAME'];	
			$thisYearName = str_replace("-", " / ", $thisYearName);
			
			$thisDataArr = array();
			$thisDataArr['ServiceName'] =$roleData[$i]['ServiceName'];
			$thisDataArr['Role'] =$roleData[$i]['Role'];
		
			$returnData[$thisYearName][] = $thisDataArr;		
		}
//debug_r($sql);
		return $returnData;
	}

	private function getStdActivity_Display($MainTitle,$colWidthArr,$subTitle,$DataArray,$hasPageBreak=false)
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		$count_col = count($colWidthArr);
		
		$table_style = 'style="font-size:13px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
		
		$colWidth_html='';
		foreach((array)$colWidthArr as $key=>$_colWidthInfo)
		{
			$colWidth_html .=$_colWidthInfo;
		}
		

		$html='';
		
		$html .= '<table width="90%" height="" cellpadding="5" border="0px" align="center" '.$table_style.';">';
		$html .= $colWidth_html;
		$html .= '	<tr> 
						<td colspan="'.$count_col.'" style="font-size:14px;text-align:left;vertical-align:text-top;'.$_borderBottom.'"><font face="Arial"><b>
						'.$MainTitle.'
						</b></font></td>
				  	</tr>';
			
		if(count($DataArray)>0)
		{			
			$count_year = 0;		  	
		  	foreach((array)$DataArray as $thisYearName=>$thisAwardArr)
		  	{		  			
		  		$html .= '<tr> 
							<td style=" font-size:13px;text-align:left;vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><font face="Arial"><b>
							'.$thisYearName.'
							</b></font></td>
					  	 ';
				if($count_year==0)
				{
					$html .= '<td style=" font-size:13px;text-align:left;vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><font face="Arial"><b>
								'.$subTitle.'
							  </b></font></td>';
				}
				else
				{
					$html .= '<td style=" font-size:13px;text-align:left;vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><font face="Arial"><b>
								&nbsp;
							  </b></font></td>';
				}
				$html .= '</tr>';	  
				for($i=0,$i_MAX=count($thisAwardArr);$i<$i_MAX;$i++)
				{
					$html .= '<tr> ';
					foreach((array)$thisAwardArr[$i] as $key=>$thisContent)
		  			{
		  				$thisContent = ($thisContent=='')? $this->EmptySymbol:$thisContent;
		  				
						$html .= '<td style="font-size:11px;text-align:left;vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><font face="Arial">
									'.$thisContent.'
								  </font></td>
									';
		  			}
					$html .= ' </tr>';	
				}	  	  
						
				$count_year++;	  	
		  	}	
		}
		else
		{
			$html .= '	<tr> 
							<td colspan="'.$count_col.'" style="font-size:13px;text-align:center;vertical-align:text-top;'.$_borderBottom.'"><font face="Arial"><b>
							Record Not Found.
							</b></font></td>
					  	</tr>';
		}
				  	
				  	
		$html .= ' </table>';
		
		return $html;
	}

	/***************** End Part 4 ******************/
	
	
	
	/***************** Part 5 : Other Learning Experience******************/	
	public function getPart5_HTML($OLEInfoArr){
		$Part5_Title = 'Other Learning Experiences';
			
		$titleArray = array();
		$titleArray[] = 'Event / Title';
		$titleArray[] = 'Partner Organization (if any)';
		$titleArray[] = 'Role of Participation';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="40%" />';
		$colWidthArr[] = '<col width="30%" />';
		$colWidthArr[] = '<col width="30%" />';
				
		$html = '';
	
		if(count($OLEInfoArr)>0)
		{
			$html .= $this->getOLE_Display($Part5_Title,$colWidthArr,$titleArray,$OLEInfoArr,$hasPageBreak=false);
		}
		return $html;	
	}
		
	
	/***************** End Part 5 ******************/

	
	
	/***************** Part 6 ******************/
	
	public function getPart6_HTML(){
		
		$Part7_Title = "Student's Self Account";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
	//	$cond_SLPOrder = "And SLPOrder is not null";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			
						$cond_SLPOrder
				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$table_style = 'style="font-size:14px;border-collapse: collapse;border:1px solid #000000;"'; 

		$StrReplaceArr = array();
		$StrReplaceArr[] = '<p class="MsoNormal" style="line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal"></p>';
		$StrReplaceArr[] = '<p class="14">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="line-height: 150%; text-indent: 24pt; margin: 0cm 0cm 0pt"></p>';
		$StrReplaceArr[] = '<p style="margin-bottom: 0cm; line-height: 150%;">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="text-indent:22.0pt;mso-char-indent-count:2.0">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="text-indent:22.0pt;mso-char-indent-count:2.0"></p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="line-height: 150%; margin: 0cm 0cm 0pt"></p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="line-height: 150%; margin: 0cm 0cm 0pt">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="text-align:justify;text-justify:inter-ideograph"></p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="text-justify: inter-ideograph; text-align: justify; line-height: 150%; margin: 0cm 0cm 0pt">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="text-justify: inter-ideograph; text-align: justify; line-height: 150%; margin: 0cm 0cm 0pt"></p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="text-justify: inter-ideograph; margin: 0cm 0cm 0pt; text-align: justify">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="text-justify: inter-ideograph; margin: 0cm 0cm 0pt; text-align: justify"></p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="font-family: Verdana, Arial, Helvetica, sans-serif, 新細明體; text-align: justify; text-indent: 33pt; ">&nbsp;</p>';
		$StrReplaceArr[] = '<p class="MsoNormal" style="font-family: Verdana, Arial, Helvetica, sans-serif, 新細明體; text-align: justify; text-indent: 33pt; "></p>';	
		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			
			$Detail = strip_tags($Detail,'<p><br>');
//debug_r(strpos($StrReplace2,$Detail));			
			$Detail = str_replace($StrReplaceArr,'',$Detail);

		}
		else
		{
			$Detail = 'No Record Found.';
		}	
		
		$html .= '<table width="90%" height="600px" cellpadding="10" border="0px" align="center" '.$table_style.'>
					<tr> 
						<td style="height:40px;font-size:14px;text-align:left;vertical-align:text-top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'"><font face="Arial"><b>
						'.$MainTitle.'
						</b></font></td>
				  	</tr>
					<tr>				 							
						<td style="vertical-align: top;font-size:13px;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'"><font face="Calibri">'.$Detail.'</font></td>						 														
					</tr>
				  </table>';	
		
			
		return $html; 
	}
	
	
	/***************** End Part 6 ******************/
	
	
	
	/***************** Part 7 : Footer ******************/
	public function getSignature_HTML($PageBreak='')
	{
	    //$ClassTeacherStr = "Ms. Lam Yee Wah<br/>Class Teacher";
		//2014-1120-1446-00073
		//$PrincipalStr = "Ms. Wong Sau Ping, Kitty<br/>Acting Principal";
        // [2020-0918-0945-37235]
		//$PrincipalStr = "Ms. Wong Sau Ping, Kitty<br/>Principal";
        $PrincipalStr = "Ms. Sit Yee Mei, Rowan<br/>Principal";
		
		$_borderTop =  'border-top: 1px solid black; ';
		
		$html = '';

		 
		$ClassTeacherStr = $this->GetClassTeacherInfo().'<br/>Class Teacher';

		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="font-size:13px;'.$PageBreak.'">		
					<col width="30%" />
					<col width="40%" />
					<col width="30%" />

				 	<tr>					
					    <td align="center" style="'.$_borderTop.'"><font face="Arial"><b>'.$ClassTeacherStr.'</b></font></td>
						<td>&nbsp;</td>
						<td align="center" style="'.$_borderTop.'"><font face="Arial"><b>'.$PrincipalStr.'</b></font></td>
					</tr>

				  </table>';
		return $html;
		
	}
	/***************** End Part 7 ******************/

	private function getOLE_Display($MainTitle,$colWidthArr,$titleArray,$DataArray,$hasPageBreak=false){
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		$count_col = count($colWidthArr);
		
		$table_style = 'style="font-size:13px;border-collapse: collapse;border:1px solid #000000;'.$Page_break.'"'; 
		
		$colWidth_html='';
		foreach((array)$colWidthArr as $key=>$_colWidthInfo)
		{
			$colWidth_html .=$_colWidthInfo;
		}
		
		$subtitle_html='';
		foreach((array)$titleArray as $key=>$_title)
		{
			$text_alignStr='center';
			
			$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><font face="Arial"><b>';
			$subtitle_html .=$_title;
			$subtitle_html .='</b></font></td>';
			
		}	
		
		
		$html='';
		
		$html .= '<table width="90%" height="" cellpadding="5" border="0px" align="center" '.$table_style.';">';
		$html .= $colWidth_html;
		$html .= '	<tr> 
						<td colspan="'.$count_col.'" style="font-size:14px;text-align:left;vertical-align:text-top;'.$_borderBottom.'"><font face="Arial"><b>
						'.$MainTitle.'
						</b></font></td>
				  	</tr>';
		$html .=$subtitle_html;	 
				  	
				  	foreach((array)$DataArray as $thisYearName=>$thisAwardArr)
				  	{			  		
				  		$html .= '<tr> 
									<td colspan="'.$count_col.'" style=" font-size:13px;text-align:left;vertical-align:text-top;"><font face="Arial"><b>
									'.$thisYearName.'
									</b></font></td>
							  	  </tr>';
						 	  
						for($i=0,$i_MAX=count($thisAwardArr);$i<$i_MAX;$i++)
						{
							$html .= '<tr> ';
							foreach((array)$thisAwardArr[$i] as $key=>$thisContent)
				  			{
				  				$thisContent = ($thisContent=='')? $this->EmptySymbol:$thisContent;
				  				
								$html .= '<td style="font-size:11px;text-align:left;vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><font face="Arial">
											'.$thisContent.'
										  </font></td>
											';
				  			}
							$html .= ' </tr>';	
						}	  	  
									  	
				  	}			  	
				  	
		$html .= ' </table>';
		
		return $html;
	}
	

	

	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$OLE_Cate = $this->Get_eClassDB_Table_Name('OLE_CATEGORY'); 
		
	
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
		
		if(strtoupper($IntExt)=='EXT')
		{
	//		$cond_limit = " limit 5";
		}
		else if(strtoupper($IntExt)=='INT')
		{
	//		$cond_limit = " limit 10";
		}
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = " And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID

				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
				order by 
						y.Sequence asc,	
						ole_prog.Title asc		
				$cond_limit
				";
		$roleData = $this->objDB->returnResultSet($sql);

		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
			$thisAcademicYear = str_replace("-", " / ", $thisAcademicYear);

			$thisDataArr = array();	

			$thisDataArr['Program'] =$roleData[$i]['Title'];		
			$thisDataArr['Organization'] =$roleData[$i]['Organization'];
			$thisDataArr['Role'] =$roleData[$i]['Role'];
			if(strtoupper($IntExt)=='EXT')
			{
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
			}
			$returnData[$thisAcademicYear][] = $thisDataArr;		
		}
		return $returnData;
	}
	public function GetClassTeacherInfo() {

		$AcademicId = Get_Current_Academic_Year_ID();
		
		//		$libfcm = new form_class_manage();
//		$yearClassID = $libfcm->Get_Student_Class_Info_In_AcademicYear($this->uid, Get_Current_Academic_Year_ID());
//		$objClass = new year_class($yearClassID,$GetYearDetail=false,$GetClassTeacherList=true);
//		$classTeacherAry = $objClass->ClassTeacherList;

		$sql = "Select
						iu.EnglishName,
						iu.ChineseName,
						iu.TitleEnglish,
						iu.TitleChinese
				From
						YEAR_CLASS_USER as ycu
						Inner Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Inner Join YEAR_CLASS_TEACHER as yct On (yc.YearClassID = yct.YearClassID)
						Inner Join INTRANET_USER as iu On (yct.UserID = iu.UserID)
				Where
						ycu.UserID = '".$this->uid."'
						And yc.AcademicYearID = '".$AcademicId."'
				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnStr = '';
		$break_html = '';
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_EnglishName = $roleData[$i]['EnglishName'];
			$_TitleEnglish = $roleData[$i]['TitleEnglish'];

			$returnStr .= $break_html.$_TitleEnglish.' '.$_EnglishName;
			$break_html = '<br/>';
			
		}

		return $returnStr;
	}

	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
			$firstYear = substr($returnArray[0], 0, 4);
			$lastYear = substr($returnArray[$lastNo],-4);
			
			$returnValue = $firstYear."-".$lastYear;
			
			return $returnValue;	
		}
		
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center"><font face="Arial">'.$Content.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>