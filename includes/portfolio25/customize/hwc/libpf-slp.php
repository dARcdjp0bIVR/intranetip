<?php
/*
 * Using:
 * 
 * Modification:
 *	2017-04-24	(Omas)
 * 		- modified as R116067 , R116204
 * 2016-12-15	(Omas)
 * 		- changed title and content for OLE part for R108353 
 * 2015-03-26	(Omas)
 * 		- Part 4 cerfications->certifications, CE: 社會服務 -> CS: 社會服務
 * 2013-03-26	(Ivan) [2013-0326-1646-48073]
 * 		- modified getOLE_Info() to update records display ordering
 */
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	private $StdClassName;

	private $StdAcademicResultArr;
	private $YearClassID;
	
	private $AcademicYearStr;
	private $AcademicTermArr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = 'N.A.';
		$this->RoleEmptySymbol = '參與者';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->AcademicTermArr = array();
		
		$this->StdClassName = '';
				
		$this->nextLeftRow = $this->NumberOfFirstPage;
		$this->RowNoNow = 0;
		
		$this->PageNumberOfThisPage = 1;
		
		$this->SchoolTitleEn = 'Hon Wah College';
		$this->SchoolTitleCh = '漢華中學';
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	
	public function setAcademicTermArr($ParAcademicTermArr){
		$this->AcademicTermArr = $ParAcademicTermArr;
	}
	public function setStdAcademicResultArr($ParStdAcademicResultArr){
		$this->StdAcademicResultArr = $ParStdAcademicResultArr;
	}

	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
		
	private function getYearID($YearName='thisYear')
	{
		if($YearName=='thisYear')
		{
			$YearClassID = $this->YearClassID;
		
			$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
			$sql = "select 
							ClassTitleEN,
							YearID
				    from 
							$YEAR_CLASS 
					where 
							YearClassID =$YearClassID
					";
			$result = $this->objDB->returnResultSet($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		else
		{
			$YEAR = $this->Get_IntranetDB_Table_Name('YEAR');
			$sql = "select 
							YearID
				    from 
							$YEAR 
					where 
							YearName =$YearName
					";
			$result = $this->objDB->returnResultSet($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		
		return $yearID;
	}
	

	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '18';

//		$imgFile = get_website().'/includes/portfolio25/customize/ychwws/wws_logo.jpg';
//		$schoolLogoImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:90px;">' : '&nbsp;';

		$YearRange = $this->getYearRange($this->academicYearIDArr);

		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">
					<tr>
						<td rowspan="3" align="center" " style="width:18%;">'.$schoolLogoImage.'</td>
						<td style="font-size:'.$header_font_size.'px;">'.$this->SchoolTitleCh.'&nbsp;'.$this->SchoolTitleEn.'</td>
						<td rowspan="3" align="left" style="width:18%;">&nbsp;</td>
				   </tr>';
			$x .= '<tr><td style="font-size:'.$header_font_size.'px;">學生學習概覽 Student Learning Profile</td></tr>';
			$x .= '<tr><td style="font-size:'.$header_font_size.'px;">'.$YearRange.'年度(Academic Year '.$YearRange.')</td></tr>';

		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						ClassNumber,
						WebSAMSRegNo,
						ClassName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{		
		$fontSize = 'font-size:15px';
		
		$thisPartTitle = 'Student Particulars';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;	
		} else {
			$IssueDate = date('d/m/Y',strtotime($IssueDate));
		}
		
		$DOB = $result[0]["DateOfBirth"];
		if (date($DOB)==0) {
			$DOB = $this->EmptySymbol;
		} else {
			$DOB = date('d/m/Y',strtotime($DOB));
		} 
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		
		if($Gender=='M')
		{
			$Gender ='男 Male';
		}
		else if($Gender=='F')
		{
			$Gender ='女 Female';
		}
		
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$STRN_No = $result[0]["STRN"];		
		if($STRN_No=='')
		{
			$STRN_No = $this->EmptySymbol;
		}
		
		$RegistrationNo = $result[0]["WebSAMSRegNo"];	
		$RegistrationNo = str_replace('#','S',$RegistrationNo);	
		if($RegistrationNo=='')
		{
			$RegistrationNo = $this->EmptySymbol;
		}
		
		$DateOfAdmisDate = substr($RegistrationNo,1,4);
		if($DateOfAdmisDate=='')
		{
			$DateOfAdmisDate = $this->EmptySymbol;
		}
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;

		$this->StdClassName = $ClassName;
		
		$html = '';
		$html .= '<table width="90%" cellpadding="2" border="0px" align="center" style="border-collapse: collapse;">
					<col width="34%" />
					<col width="33%" />
					<col width="33%" />

					<tr style="vertical-align:top;">
							<td style="'.$fontSize.'" colspan="2">學生姓名 Name : '.$ChiName.' '.$EngName.'</td>
							<td style="'.$fontSize.'">班別 Class Name : '.$ClassName.'</td>
					</tr>
					<tr style="vertical-align:top;">
							<td style="'.$fontSize.'">性別 Sex : '.$Gender.'</td>
							<td style="'.$fontSize.'">學生編號 STRN : '.$STRN_No.'</b></td>
							<td style="'.$fontSize.'">班號 Class No : '.$ClassNumber.'</td>
					</tr>
					
					<tr style="vertical-align:top;">
							<td style="'.$fontSize.'">出生日期 Date of Birth : '.$DOB.'</td>
							<td style="'.$fontSize.'">註冊編號 Reg. no : '.$RegistrationNo.'</td>
							<td style="'.$fontSize.'">派發日期 Date of Issue : '.$IssueDate.'</td>
					</tr>
					';

		$html .= '</table>'; 
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
/***************** Part 3 : Academic Performance******************/	
	public function getPart3_HTML(){
		
		$Part3_Title = '學業報告 Academic Results';
		
		$Page_break='page-break-after:always;';
		$table_style = 'border-collapse: collapse; '; 
		
		$html = '';
		
	//	$YearID = $this->getYearID('thisYear');
		
		//if($YearID=='6')
		//{
			$html .=$this->getAcademicResultContentDisplay($Part3_Title);
		//}
		return $html;	
	}		
	
	private function getAcademicResultContentDisplay($MainTitle='')
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$remark_html ='';
		$remark_html = '<table width="100%"><tr>';
		
		$ContentTable = $this->getAcademicResult_ContentTable($MainTitle);	
		
		$html='';
		$html='<table width="90%" height="" cellpadding="0" border="0px" align="center" style="border-collapse: collapse;">
					<tr>				 							
						<td style="vertical-align: top;">'.$ContentTable.'</td>						 														
					</tr>
			   </table>';
	
		return $html;
	}
	
	private function getAcademicResult_ContentTable($MainTitle='')
	{	
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$YearClassArray = $this->getStudentAcademic_YearClassNameArr();
	
		$Subject_string = '科目<br/>Subject';
		$Year_string = '年度<br/>Year';
		$Form_string = '年級<br/>Form';
		$Term_string = '學期<br/>Term';
		$Mark_string = 'Mark';
		
		$FirstTerm_string = '上學期<br/>First Term';
		$SecondTerm_string = '下學期<br/>Second Term';
		$AcademicYear_string = '全學年<br/>Academic Year';
		
		$StudentID = $this->uid;
		
		$lpf_report = new libpf_report();
		list($SubjectIDNameAssoArr, $StudentFormInfoArr, $StudentAcademicResultInfoArr, $SubjectInfoOrderedArr) = $lpf_report->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($StudentID);
		
		//debug_pr($StudentAcademicResultInfoArr[1971]);
		
		$StudentAcademicResultArr = $this->StdAcademicResultArr;
	
		$hasRecord = false;
		$countColumn=0;
		
		$AcademicYearArray = $this->getAcademicYearArr($this->academicYearIDArr);
		
		
		$YearClassArray = $this->getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,'asc');

		$MarkArr = array();	
	
		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{		
			$countColumn++;
		}	
		
		$AcademicYearTermArr = $this->AcademicTermArr;	
		for($a=0,$a_MAX=count($SubjectInfoOrderedArr);$a<$a_MAX;$a++)
		{
			$thisSubjectID = $SubjectInfoOrderedArr[$a]['SubjectID'];
			$thisSubjectNameEn = $SubjectInfoOrderedArr[$a]['SubjectDescEN'];
			$thisSubjectNameCh = $SubjectInfoOrderedArr[$a]['SubjectDescB5'];
		
			$IsStudy = false;
			
			for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
			{						
				$thisYear = $YearClassArray[$i]['AcademicYear'];
				$thisClassName = $YearClassArray[$i]['ClassName'];
			
				$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
				
				$thisFormNo = (int)$thisClassName;
				
				preg_match('/\d+/', $thisClassName, $number);
				
				if(count($number)>0 && is_array($number))
				{
					$thisFormNo = $number[0];
				}			
				$thisYearTermArr = $AcademicYearTermArr[$thisAcademicYearID];
				for($j=0,$j_MAX=count($thisYearTermArr);$j<$j_MAX;$j++)
				{
					$thisYearTermID = $thisYearTermArr[$j];
					$thisGrade = $StudentAcademicResultArr[$StudentID][$thisSubjectID][$thisAcademicYearID][$thisYearTermID] ;
					
					if ($thisGrade != 'N.A.' && $thisGrade != '') {
						$IsStudy = true;	
						$hasRecord = true;				
					}			

					$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Grade'][] = $thisGrade;			
				
				}
				
				$thisGrade_Over = $StudentAcademicResultInfoArr[$StudentID][$thisSubjectID][$thisAcademicYearID]['Grade'];
				
				if ($thisGrade_Over != 'N.A.' && $thisGrade_Over != '') {
					$IsStudy = true;	
					$hasRecord = true;					
				}	

				$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['GradeOverall'] = $thisGrade_Over;
					
				$YearName = 'S'.$thisFormNo;
				$thisYearID = $this->getYearID($YearName);	
							
			}
//			$thisYearID = $this->getYearID($YearName);			
			
			$NewSubjectSequenceArr[$thisSubjectID]['IsStudy']=$IsStudy;
						
			$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $thisSubjectNameEn;
			$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameCh'] = $thisSubjectNameCh;
			
		}
		

		$html='';
		$html .= '<table width="100%" height="" cellpadding="2" border="0px" align="center" style="font-size:10px;border-collapse: collapse;">
					<tr> 
						<td style=" font-size:15px;text-align:left;vertical-align:text-top;">
						'.$MainTitle.'
						</td>
				  	</tr>
				  </table>';
		
		$html .='<table width="100%"  cellpadding="4" style="border-collapse: collapse;border:1px solid #000000;">';
		
			$html .='<col width="10%"/>';
			$html .='<col width="10%"/>';
			if($countColumn>0)
			{				
				$thisWidth = 80/(($countColumn-1)*3+1);
			}
			
			$thisCountColumn = (($countColumn-1)*3+1);
			for($i=0,$i_MAX=$thisCountColumn;$i<$i_MAX;$i++)
			{			
				$html .='<col width="'.$thisWidth.'%"/>';
			}

		
			$html .='<tr>'; 
				$html .='<td>&nbsp;</td>';
				$html .='<td style="font-size:12px;text-align:center;padding-left:8px;'.$_borderBottom.$_borderTop.$_borderLeft.$_borderRight.'">'.$Year_string.'</td>';
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisYear = $YearClassArray[$i]['AcademicYear'];
					
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNoEn = (int)$thisClassName;
					
					preg_match('/\d+/', $thisClassName, $number);
				
					if(count($number)>0 && is_array($number))
					{
						$thisFormNoEn = $number[0];
						
					}

					$colspan ='';
					if($thisFormNoEn=='6')
					{				
					}
					else
					{
						$colspan= 'colspan="3"';
					}

					$thisBorder = $_borderBottom.$_borderTop.$_borderRight;

					$html .='<td '.$colspan.'  style="font-size:12px;text-align:center;'.$thisBorder.'">'.$thisYear.'</td>';
				}			
			$html .='</tr>';
			
			$html .='<tr>'; 
				$html .='<td rowspan="2" style="font-size:12px;text-align:center;padding-left:8px;'.$_borderBottom.$_borderTop.$_borderLeft.$_borderRight.'">'.$Subject_string.'</td>';
				$html .='<td style="font-size:12px;text-align:center;padding-left:8px;'.$_borderBottom.$_borderTop.$_borderLeft.$_borderRight.'">'.$Form_string.'</td>';
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNoEn = (int)$thisClassName;
					preg_match('/\d+/', $thisClassName, $number);
				
					if(count($number)>0 && is_array($number))
					{
						$thisFormNoEn = $number[0];
						
					}

					$thisFormNoCh = $this->ch_num($thisFormNoEn);
					$colspan ='';
					if($thisFormNoEn=='6')
					{				
					}
					else
					{
						$colspan= 'colspan="3"';
					}

					$thisBorder = $_borderBottom.$_borderTop.$_borderRight;

					$html .='<td '.$colspan.' style="font-size:12px;text-align:center;'.$thisBorder.'">中'.$thisFormNoCh.'<br/>S'.$thisFormNoEn.'</b></td>';
				}			
			$html .='</tr>';
			
			$html .='<tr>';
				$html .='<td style="font-size:13px;text-align:center;padding-left:8px;'.$_borderBottom.$_borderLeft.$_borderTop.$_borderRight.'">'.$Term_string.'</td>';

				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;
					
					preg_match('/\d+/', $thisClassName, $number);
				
					if(count($number)>0 && is_array($number))
					{
						$thisFormNo = $number[0];
						
					}
					
					if($thisFormNo=='6')
					{					
					}
					else
					{
						$html .='<td style="font-size:13px;text-align:center;'.$_borderTop.$_borderBottom.$_borderRight.'">'.$FirstTerm_string.'</td>';	
						$html .='<td style="font-size:13px;text-align:center;'.$_borderTop.$_borderBottom.$_borderRight.'">'.$SecondTerm_string.'</td>';	
					}
					$html .='<td style="font-size:13px;text-align:center;'.$_borderTop.$_borderBottom.$_borderRight.'">'.$AcademicYear_string.'</td>';											
				}								
			$html .='</tr>';
					
					
			$lastSubjectId = '';
			foreach((array)$NewSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr)
			{		
				$_IsStudy = $NewSubjectSequenceArr[$thisSubjectID]['IsStudy'];
				if($_IsStudy==false) {
					continue;
				}
				else {
					$lastSubjectId = $thisSubjectID;
				}
			}
			
			
			foreach((array)$NewSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr)
			{		
				$_IsStudy = $NewSubjectSequenceArr[$thisSubjectID]['IsStudy'];
				
				if($_IsStudy==false) {
					continue;
				}
				
				if ($thisSubjectID == $lastSubjectId) {
					$_subjectBorderBottom = $_borderBottom;
				}
				else {
					$_subjectBorderBottom = '';
				}
				
						
				$thisSubjectNameEn = $thisSubjectInfoArr['SubjectNameEn'];
				$thisSubjectNameCh = $thisSubjectInfoArr['SubjectNameCh'];
				$thisIsComponentSubject = $thisSubjectInfoArr['thisIsComponentSubject'];
				
				if($thisIsComponentSubject)
				{
					$fontSize='9px';
				}
				else
				{
					$fontSize='13px';
				}
				
				$html .='<tr>';
			
				$html .='<td colspan="2" style="padding-left:9px;font-size:'.$fontSize.';text-align:left;'.$_borderLeft.$_borderRight.$_subjectBorderBottom.'">'.$thisSubjectNameCh.'<br/>'.$thisSubjectNameEn.'</td>';

				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisYear = $YearClassArray[$i]['AcademicYear'];
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;				

					$thisGrade = $this->EmptySymbol;

					preg_match('/\d+/', $thisClassName, $number);
				
					if(count($number)>0 && is_array($number))
					{
						$thisFormNo = $number[0];
						
					}

					if($thisFormNo=='6')
					{					
					}
					else
					{

						$thisYearTermArr = $AcademicYearTermArr[$thisAcademicYearID];
						for($j=0,$j_MAX=count($thisYearTermArr);$j<$j_MAX;$j++)
						{	
							$thisGrade = $NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Grade'][$j];
					
							if ($thisGrade == 'N.A.' || $thisGrade == '') {
								$thisGrade = $this->EmptySymbol;
							}		
							$html .='<td style="padding-left:13px;font-size:'.$fontSize.';text-align:center;'.$_borderRight.$_subjectBorderBottom.'">'.$thisGrade.'</td>';
						}
						
						if($thisYearTermArr=='')
						{
							for($j=0,$j_MAX=2;$j<$j_MAX;$j++)
							{	
								$html .='<td style="padding-left:13px;font-size:'.$fontSize.';text-align:center;'.$_borderRight.$_subjectBorderBottom.'">'.$this->EmptySymbol.'</td>';
							}
						}
						
					}
					$thisGradeOverall = $thisSubjectInfoArr[$thisYear]['GradeOverall'] ;				
					if ($thisGradeOverall == 'N.A.' || $thisGradeOverall == '') {
						$thisGradeOverall = $this->EmptySymbol;
					}		
					
					$html .='<td style="padding-left:13px;font-size:'.$fontSize.';text-align:center;'.$_borderRight.$_subjectBorderBottom.'">'.$thisGradeOverall.'</td>';
		
				}			
				
				$html .='</tr>';								
			}		
			
			$html .='<tr>';
	
			
			if($hasRecord==false)
			{
				$thisCountColumn = (($countColumn-1)*3+3);
				$noRecord = 'No Record Found.';
				$html .='<td colspan="'.$thisCountColumn.'" style="padding-left:9px;font-size:13px;text-align:center;'.$_borderLeft.$_borderRight.'">'.$noRecord.'</td>';
			}

			
			$html .='</tr>';
			
		$html .='</table>';
		
		return $html;
		
	}


	/***************** End Part 3 ******************/
	
	/***************** Part 4 ******************/	
	public function getPart4_HTML($OLEInfoArr){
		
		#R108353 
// 		$Part4_Title = '獎項 Awards';
		$Part4_Title = '校外表現 /獎項及重要參與<br>Performance / Awards and Key Participation Outside School';
		
		$Part5_Remark = '**ID: 智育發展 (Intellectual Development) MCE: 德育及公民教育 (Moral and Civic Education) CS: 社會服務 (Community Service) ';
		$Part5_Remark .= 'PD: 體育發展 (Physical Development) AD: 藝術發展 (Aesthetic Development) CE: 與工作有關的經驗 (Career-related Experiences) ';
		$Part5_Remark .= 'Others: 其他 (Others) <br/>';
		$Part5_Remark .= '**有需要時學生會負責提供獎項/證書文憑/成就 作證明。Evidence of awards/ certifications / achievements /will be provided by students upon request. ';
		
		$titleArray = array();
		$titleArray[] = '事項名稱<br/>Event Title';
		$titleArray[] = '主辦機構<br/>Organized By';
		$titleArray[] = '學年<br/>Year';
		$titleArray[] = '參與角色<br/>Role';
		$titleArray[] = '獎項<br/>Award';

		$DataArr = array();

		$DataArray = $OLEInfoArr;

		$colWidthArr = array();
	
		//$colWidthArr[] = '<col width="5%" />';
		$colWidthArr[] = '<col width="40%" />';
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="13%" />';
		$colWidthArr[] = '<col width="14%" />';
		$colWidthArr[] = '<col width="13%" />';


		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('4',$Part4_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false,$Part5_Remark);
		
		return $html;
		
	}

	/***************** End Part 4 ******************/
	
	/***************** Part 5 ******************/	
	public function getPart5_HTML($OLEInfoArr){
		
		// R108353
		//$Part5_Title = '活動 Activities Record';
		$Part5_Title = '其他學習經歷及校內頒發的主要獎項及成就<br>Other Learning Experiences and List of Awards and Major Achievements Issued by the School';
		
//		$Part5_Remark = '**ID: 智育發展 (Intellectual Development) MCE: 德育及公民教育 (Moral and Civic Education) CE: 社會服務 (Community Service) ';
//		$Part5_Remark .= 'PD: 體育發展 (Physical Development) AD: 藝術發展 (Aesthetic Development) CE: 與工作有關的經驗 (Career-related Experiences) ';
//		$Part5_Remark .= 'Others: 其他 (Others) <br/>';
//		$Part5_Remark .= '**有需要時可提供獎項/證書文憑/成就 作證明。Evidence of awards / certifications / achievements is/are available upon request.';
		
		$titleArray = array();
		$titleArray[] = '事項名稱<br/>Event Title';
		$titleArray[] = '主辦機構<br/>Organized By';
		$titleArray[] = '學年<br/>Year';
		$titleArray[] = '參與角色<br/>Role';
		$titleArray[] = '其他學習<br/>經歷範疇<br/>OLE';	
		
		$DataArr = array();

		$DataArray = $OLEInfoArr;
		
		$colWidthArr = array();

		//$colWidthArr[] = '<col width="5%" />';
		$colWidthArr[] = '<col width="40%" />';
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="13%" />';
		$colWidthArr[] = '<col width="14%" />';
		$colWidthArr[] = '<col width="13%" />';
	

		$html = '';
		$html .= $this->getPartContentDisplay_NoRowLimit('5',$Part5_Title,$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false,$Part5_Remark);
		
		return $html;
		
	}

	/***************** End Part 5 ******************/
	
	
	/***************** Part 6 ******************/
	
	public function getPart6_HTML(){
		
		$Part7_Title = "學生自述<br/>Student's Self-Account";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:15px;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			
			$Detail = strip_tags($Detail,'<p><br>');
		}
		else
		{
			$Detail = 'No Record Found.';
		}	
//		$html .= '<table width="90%" height="" cellpadding="2" border="0px" align="center" style="font-size:16px;border-collapse: collapse;">
//					<tr> 
//						<td style=" font-size:15px;text-align:left;vertical-align:text-top;"><b>
//						'.$MainTitle.'
//						</b></td>
//				  	</tr>
//				  </table>';
		
		$html .= $this->getEmptyTable('40px');
		$html .= '<table width="90%" height="940px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr> 
						<td style=" font-size:15px;text-align:left;vertical-align:text-top;height:10px;'.$_borderTop.$_borderLeft.$_borderRight.'">
						'.$MainTitle.'
						</td>
				  	</tr>
					<tr>				 							
						<td style="vertical-align: top;'.$_borderLeft.$_borderBottom.$_borderRight.'">'.$Detail.'</td>						 														
					</tr>
				  </table>';	
		
			
		return $html; 
	}
	
	
	/***************** End Part 5 ******************/
	
	
	
	/***************** Part 6 : Footer ******************/
	public function getSignature_HTML($PageBreak='')
	{
		$PrincipalStr = "校長 (Principal)";
		$SchoolChopStr = "校印 (School Chop)";		
		
		$_borderTop =  'border-top: 1px solid black; ';	
		
		$colwidth_html = '<col width="20%" />
						  <col width="10%" />
						  <col width="20%" />
						  <col width="50%" />';		

		$html = '';
		
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="font-size:15px;'.$PageBreak.'">';		
		   $html .= $colwidth_html;

		$html .= '	<tr>					
					    <td align="center" style="'.$_borderTop.'vertical-align:top;">'.$SchoolChopStr.'</td>
						<td>&nbsp;</td>
						<td align="center" style="'.$_borderTop.'vertical-align:top;">'.$PrincipalStr.'</td>
						<td>&nbsp;</td>
					</tr>';

		$html .= ' </table>';

		return $html;
		
	}
	/***************** End Part 6 ******************/

	
	
	private function getPartContentDisplay_NoRowLimit($PartNum, $mainTitle,$subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false,$RemarkStr='') 
	{
				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		$table_style = 'style="font-size:10px;border-collapse: collapse;'.$Page_break.'"'; 

		$rowMax_count= 0;
		if(count($DataArr)>0)
		{
			$rowMax_count = count($DataArr);
		}
	
		$subtitle_html = '';
		
		if($PartNum=='4')
		{						
			$total_Record=20;
			$rowHight = 10;			
		}
		else if ($PartNum=='5')
		{					
			$total_Record=20;
			$rowHight = 10;		  
		}
		
//$rowMax_count = 0;	
		$subtitle_html = '';
	//	if($rowMax_count>0)
	//	{			
			//$subtitle_html .='<td style="'.$_borderLeft.$_borderBottom.'">&nbsp;</td>';
	//	}
		$i=0;
		foreach((array)$subTitleArr as $key=>$_title)
		{
			$text_alignStr='center';
			$subtitle_html .='<td style="text-align:'.$text_alignStr.';vertical-align:text-top;font-size:13px;'.$_borderRight.$_borderTop.$_borderLeft.$_borderBottom.'"><font face="Arial">';
			$subtitle_html .=$_title;
			$subtitle_html .='</font></td>';
			
			$i++;
		}	
		
		$text_alignStr='center';
		$content_html='';			
		
		$count_col =count($colWidthArr);

		for ($i=0; $i<$rowMax_count; $i++)
		{		
			$content_html .= '<tr>';	
		
			if($i==19)
			{
				$thisBorder = $_borderBottom;
			}
			else
			{
				$thisBorder = '';
			}
		
			$j=0;	
		//	if($rowMax_count>0)
		//	{
				//$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderRight.$_borderLeft.$thisBorder.'"><font face="Arial">'.($i+1).'.</font></td>';	 
		//	} 

			foreach((array)$DataArr[$i] as $_title=>$_titleVal)
			{
				if($_titleVal=='')
				{
					$_titleVal =  $this->EmptySymbol;
				}
				
				if($PartNum=='4' || $PartNum=='5')
				{
					if($j==0)
					{
						$text_alignStr = 'left';
					}
					else
					{
						$text_alignStr = 'center';
					}
					
					# R116204 
					if($_title == 'Role' && $_titleVal == $this->EmptySymbol){
						$_titleVal = $this->RoleEmptySymbol;
					}
				} 
												
				$content_html .= ' <td style="height:'.$rowHight.'px;text-align:'.$text_alignStr.';vertical-align: top;'.$_borderRight.$_borderLeft.$thisBorder.'"><font face="Arial">'.$_titleVal.'</font></td>';
	
				$j++;
			}				
			$content_html .= '</tr>';		
		}
		
		$remainRow = $total_Record-$rowMax_count;
		for($i=0,$i_MAX=$remainRow;$i<$i_MAX;$i++)
		{
			if($i==$i_MAX-1)
			{
				$thisBorder = $_borderBottom;
			}
			
			$thisContent = '&nbsp;';
			$thisNumber = $rowMax_count+1+$i;
			
			$text_alignStr='center';
			$content_html .= '<tr>';	
			
		//	if($rowMax_count>0)
		//	{
				//$content_html .= ' <td style="height:'.$rowHight.'px;vertical-align: top;text-align:'.$text_alignStr.';'.$_borderRight.$_borderLeft.$thisBorder.'"><font face="Arial">'.$thisNumber.'.</font></td>';
		//	}
						
			$j_MAX = $count_col; //$count_col-1;

			for($j=0;$j<$j_MAX;$j++)
			{					
				$content_html .= ' <td style="height:'.$rowHight.'px;vertical-align: top;text-align:'.$text_alignStr.';'.$_borderLeft.$_borderRight.$thisBorder.'"><font face="Arial">'.$thisContent.'</font></td>';
			}				
			$content_html .= '</tr>';
		}			
		
		if($PartNum=='4')
		{
			$content_html .= '<tr><td colspan="'.$count_col.'" >';	
			$content_html .= $RemarkStr;
			$content_html .= '</td></tr>';	
		}
		
		$colNumber = count($colWidthArr);
			
		/****************** Main Table ******************************/
		
		$html = '';

		$html = '<table width="90%" height="" cellpadding="4" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
		
		$html .= '<tr> 
					<td colspan="'.$colNumber.'" style=" font-size:15px;text-align:left;vertical-align:text-top;'.$_borderBottom.'"><font face="Arial">
					'.$mainTitle.'
					</font></td>
				  </tr>';
		
		$html .= '<tr>'.$subtitle_html.'</tr>';
		
		$html .= $content_html;
		
		$html .='</table>';

			
		return $html;
	}
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false,$category='')
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		
		$OLEcomponentArr = $this->getOLEcomponentName();
//RecordID = 5

		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}	
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = " And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		if($category=='Award')
		{
			$cond_category = "And ole_prog.Category = 5";
		}
		else if($category=='OutOfAward')
		{
			$cond_category = "And ole_prog.Category <> 5";
		}
		else{
			$cond_category = "";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$order_by = 'ole_prog.ELE,
					 y.Sequence asc,	
					 ole_prog.Title asc	';
		
		if($category=='Award')
		{
			$selectStr = "ole_prog.Title as Title,
						ole_prog.Organization as Organization,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME',		
						ole_std.role as Role,			
						ole_std.Achievement as Achievement";
			//$order_by = 'SLPOrder asc';
			$order_by = 'ole_std.SLPOrder asc, ole_prog.IntExt asc ';
		}
		else if($category=='OutOfAward')
		{
			$selectStr = "ole_prog.Title as Title,
						ole_prog.Organization as Organization,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME',
						ole_std.role as Role,
						ole_prog.ELE as OleComponent";
			//$order_by = 'SLPOrder asc';
			$order_by = 'ole_std.SLPOrder asc, ole_prog.IntExt asc ';
		}
		else{
			if($IntExt == 'INT'){
				$lastColumn = "ole_prog.ELE as OleComponent";
			}else{
				$lastColumn = "ole_std.Achievement as Achievement";
			}
			$selectStr = "ole_prog.Title as Title,
							ole_prog.Organization as Organization,
							y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME',
							ole_std.role as Role,
							$lastColumn";
		}
		
		$sql = "Select
						$selectStr
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID

				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
						$cond_category
				order by 
						$order_by		
				limit 20
				";
		$roleData = $this->objDB->returnResultSet($sql);

		if($category=='Award')
		{
			return $roleData;
		}
		if($category=='OutOfAward')
		{
			for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
			{		
			    $thisDataArr = array();	
	
				$thisDataArr['Program'] =$roleData[$i]['Title'];			
				$thisDataArr['Organization'] =str_replace(',','<br/>',$roleData[$i]['Organization']);	
				$thisDataArr['SchoolYear'] =$roleData[$i]['YEAR_NAME'];				
				$thisDataArr['Role'] =$roleData[$i]['Role'];				

				$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
			
				for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
				{
					$_str = $thisOleComponentArr[$a];
					$_str = substr($_str, 0, strlen($_str)-1); 
					$_str = substr($_str, 1);
					
					$thisOleComponentArr[$a] = $_str;
				}
				
				$thisOLEstr = implode(",",(array)$thisOleComponentArr);
				$thisDataArr['OLEComp'] =$thisOLEstr;
								
				$returnData[] = $thisDataArr;	
			}
			
			return $returnData;
		}else{
			return $roleData;
		}
	}
	

	public function getOLEcomponentName()
	{
		$OLE_ELE = $this->Get_eClassDB_Table_Name('OLE_ELE');
		
		$sql="Select 
					EngTitle,
					DefaultID 
			  from
					$OLE_ELE
			";
		$roleData = $this->objDB->returnResultSet($sql);
		
		$returnArray = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$thisName_en = $roleData[$i]['EngTitle'];
			$thisID = $roleData[$i]['DefaultID'];
			
			$returnArray[$thisID] = $thisName_en;
		}
	
		return $returnArray;
	}
	
	
	public function getStudentAcademic_YearClassNameArr()
	{
		$StudentID = $this->uid;
		
		$academicYearIDArr = $this->academicYearIDArr;
		
		$SelectStr= 'h.AcademicYear as AcademicYear,
					 h.ClassName as ClassName';
		
		if($academicYearIDArr!='')
		{		
			$YearArr = array();
			for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
			{
				if($academicYearIDArr[$i]=='')
				{
					continue;
				}
				$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
				$YearArr[] = $thisAcademicYear;
			}
			
			$cond_academicYear = "And h.AcademicYear In ('".implode("','",(array)$YearArr)."')";		
		}
		

		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				where 
						h.UserID = "'.$StudentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear asc';
						
		$roleData = $this->objDB->returnResultSet($sql);
		
	
		return $roleData;
	}
	
	private function ch_num($num,$mode=true)
	{
		$char = array("零","一","二","三","四","五","六","七","八","九");
		$dw = array("","十","百","千","","萬","億","兆");
		$dec = "點";
		$retval = "";
		if($mode)
		preg_match_all("/^0*(\d*)\.?(\d*)/",$num, $ar);
		else
		preg_match_all("/(\d*)\.?(\d*)/",$num, $ar);
		if($ar[2][0] != "")
		$retval = $dec . ch_num($ar[2][0],false); //如果有小數，先遞歸處理小數
		if($ar[1][0] != "") {
		$str = strrev($ar[1][0]);
		for($i=0;$i<strlen($str);$i++) {
		$out[$i] = $char[$str[$i]];
		if($mode) {
		$out[$i] .= $str[$i] != "0"? $dw[$i%4] : "";
		if($str[$i]+$str[$i-1] == 0)
		$out[$i] = "";
		if($i%4 == 0)
		$out[$i] .= $dw[4+floor($i/4)];
		}
		}
		$retval = join("",array_reverse($out)) . $retval;
		}
		return $retval;
	}
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
//			$firstYear = substr($returnArray[0], 0, 4);
//			$lastYear = substr($returnArray[$lastNo],-4);
//			
//			$returnValue = $firstYear."-".$lastYear;
			
			$returnValue = $returnArray[$lastNo];
			return $returnValue;	
		}
		
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	/********** Display Form 4, 5 and 6  ***********/
	private function getThreeYearDisplayArr($YearClassArray,$AcademicYearArray,$order='asc')
	{
		
		$YearDisplayArr = array();
	
		$Form4_no = 0;
		$Form5_no = 0;
		$Form6_no = 0;
		
		
		if($order=='asc')
		{
			$YearDisplayArr[4]["AcademicYear"] = $this->EmptySymbol;
			$YearDisplayArr[4]["ClassName"] = 4;
			$YearDisplayArr[5]["AcademicYear"] = $this->EmptySymbol;
			$YearDisplayArr[5]["ClassName"] = 5;
			$YearDisplayArr[6]["AcademicYear"] = $this->EmptySymbol;
			$YearDisplayArr[6]["ClassName"] = 6;
		}
		else
		{
			$YearDisplayArr[6]["AcademicYear"] = $this->EmptySymbol;
			$YearDisplayArr[6]["ClassName"] = 6;
			$YearDisplayArr[5]["AcademicYear"] = $this->EmptySymbol;
			$YearDisplayArr[5]["ClassName"] = 5;
			$YearDisplayArr[4]["AcademicYear"] = $this->EmptySymbol;
			$YearDisplayArr[4]["ClassName"] = 4;
		}

		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{						
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			
			$thisAcademicYearID =array_search($thisYear,$AcademicYearArray);
			
			$thisFormNo = (int)$thisClassName;
			
			preg_match('/\d+/', $thisClassName, $number);
			
			if(count($number)>0 && is_array($number))
			{
				$thisFormNo = $number[0];
				
				if($order=='asc')
				{
					if($thisFormNo==4 || $thisFormNo==5 || $thisFormNo==6)
					{				
						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
					}
				}
				else if($order=='desc')
				{
					$CondArr = array();
					$CondArr[] = ($thisFormNo==4 && $Form4_no==0);
					$CondArr[] = ($thisFormNo==5 && $Form5_no==0);
					$CondArr[] = ($thisFormNo==6 && $Form6_no==0);
					
					if(in_array(true,$CondArr))
					{
						$YearDisplayArr[$thisFormNo] = $YearClassArray[$i];
					}
				}			
				
				if($thisFormNo==4)
				{
					$Form4_no++;
				}
				else if($thisFormNo==5)
				{
					$Form5_no++;
				}
				else if($thisFormNo==6)
				{
					$Form6_no++;
				}
				
				
			}
		}	

		
		$ThisClassArr = array();
		$count_YearDisplayArr = 0;
		foreach((array)$YearDisplayArr as $_formNo=>$_ClassArr)
		{
			$ThisClassArr[$count_YearDisplayArr] = $_ClassArr;
			$count_YearDisplayArr++;
		}
	
		return $ThisClassArr;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center">'.$Content.'</td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>