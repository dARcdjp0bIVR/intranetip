<?php
// Using:

################# Change Log [Start] #####
#
#	Date	: 2020-03-03	Philips	[2020-0219-1200-29207]
#			  Modified getAcademicResult_ContentTable(), display "Not Assessed" When display is '+'
#
#   Date    : 2018-03-08    Bill    [2018-0306-1449-23207]
#             Modified getMarkArr(), checkReportIsTransferred(), to check if report data is transferred before display report mark
#
#	Date	: 2017-11-06	Bill	[2017-1106-0904-28236]
#			  Modified getPart1_HTML(), to update School Name & Logo
#
#	Date	: 2016-01-12	Omas
#			  Modified getStudentInfoDisplay() , getAcademicResult_ContentTable()
#		      - improve display issue as #F91216 
#
################## Change Log [End] ######

class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	
	private $AcademicYearStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->issuedate = $issuedate;
		
		$this->colorBlue = "#3300FF";
		//$this->colorBlue = "#000000";
		
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '—';
		$this->SelfAccount_Break = '[pagebreak]';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#FFFFFF';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->SchoolTitleEn = 'SHA TIN METHODIST COLLEGE';
		//$this->SchoolTitleCh = '沙田循道衛理中學';
		$this->SchoolTitleCh = '沙田循道衞理中學';
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	
	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '20';

		//$imgFile = get_website().'/file/reportcard2008/templates/sha_tin_methodhist.gif';
		//$schoolLogoImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:90px;">' : '&nbsp;';
		$imgFile = get_website().'/file/reportcard2008/templates/sha_tin_methodhist.png';
		$schoolLogoImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:90px; height:91.344px;">' : '&nbsp;';
		
		$header_title1 = '<b>'.$this->SchoolTitleEn.'</b>';
		$header_title2 = $this->SchoolTitleCh;
		$header_title3 = "Student Learning Profile";
		
		$EmptySpace_html = '<tr><td>'.'&nbsp;'.'</td></tr>'."\r\n";
		
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:85%; text-align:center; border:0px;">
					<tr>';
				$x .=	'<td rowspan="4" align="center">'.$schoolLogoImage.'</td>';
				$x .= 	'<td class="report_title" style="color:'.$this->colorBlue.';"><b>'.$header_title1.'</b></td>
						<td rowspan="4" align="left" style="width:20%;">&nbsp;</td>
				   </tr>';

			$x .= '<tr><td class="report_title" style="color:'.$this->colorBlue.';"><font face="新細明體">'.$header_title2.'</font></td></tr>'."\r\n";
			$x .= '<tr><td class="font_16px" style="color:'.$this->colorBlue.';">'.$header_title3.'</td></tr>'."\r\n";
			$x .= '<tr><td class="font_16px" style="color:'.$this->colorBlue.';">('.$this->YearRangeStr.')</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						WebSAMSRegNo,
						STRN
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{	
		$fontSize = 'font-size:14px;';
		$fontSize2 = 'font-size:13px;';
		
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		$_borderBottom =  'border-bottom: 1px solid black; ';
	
			
		$DOB = $result[0]["DateOfBirth"];

		if (date($DOB)==0) {
			$DOB = $this->EmptySymbol;
		} else {
			$DOB = date('j/n/Y',strtotime($DOB));
		} 
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;
		} else {
			$IssueDate = date('j/n/Y',strtotime($IssueDate));
		}
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$RegistrationNo = str_replace("#", "", $result[0]["WebSAMSRegNo"]);
		$RegistrationNo = ($RegistrationNo=='')? $this->EmptySymbol:$RegistrationNo;

		$STRNNo = $result[0]["STRN"];	
		$STRNNo = ($STRNNo=='')? $this->EmptySymbol:$STRNNo;

		if($this->uid == '579' || $this->uid == '606') //YEUNG TSZ YING JACQUELINE   && CHEUNG SUET TING FIONA 
		{
			$colWidth_html = '<col width="51%" />
							  <col width="24%" />
							  <col width="25%" />';	
		}
		else
		{
			$colWidth_html = '<col width="51%" />
							  <col width="24%" />
							  <col width="25%" />';	

			// 2012-0221-1625-24066
//			$colWidth_html = '<col width="45%" />
//							  <col width="30%" />
//							  <col width="25%" />';	
/*
			$colWidth_html = '<col width="45%" />
							  <col width="28%" />
							  <col width="27%" />';
*/
		}

		$html = '';
		$html .= '<table width="90%" cellpadding="4" border="0px" align="center" style="border-collapse: collapse;">';
		
		$html .= $colWidth_html;
		
		// #F91216 if student name too long go to next line for Chi Name
		if(strlen($EngName) > 22){
			$nextline = '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';			
		}
		else{
			$nextline = '';
		}
		$html .=	'<tr>
							<td class="font_16px"><font color="'.$this->colorBlue.'">Name : </font>'.$EngName.' '.$nextline.'<font face="新細明體">'.$ChiName.'</font></td>
							<td class="font_16px"><font color="'.$this->colorBlue.'">Sex&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </font>'.$Gender.'</td>
							<td class="font_16px"><font color="'.$this->colorBlue.'">Date of Birth : </font>'.$DOB.'</td>
							
					</tr>
					<tr>
							<td class="font_16px"><font color="'.$this->colorBlue.'">Registration No. : </font>'.$RegistrationNo.'</td>
							<td class="font_16px"><font color="'.$this->colorBlue.'">STRN : </font>'.$STRNNo.'</td>
							<td class="font_16px"><font color="'.$this->colorBlue.'">Date of Issue : </font>'.$IssueDate.'</td>
					</tr>
					<tr>
							<td><font face="Times New Roman" style="color:'.$this->colorBlue.';'.$fontSize2.'">School Address : Sun Tin Wai Estate, Shatin, HK</font></td>
							<td><font face="Times New Roman" style="color:'.$this->colorBlue.';'.$fontSize2.'">Website : www.stmc.edu.hk</font></td>
							<td><font face="Times New Roman" style="color:'.$this->colorBlue.';'.$fontSize2.'">School Phone : 26021300</font></td>
					</tr>';

		$html .= "</table>";
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	
	/***************** Part 3 : Academic Performance******************/	
	public function getPart3_HTML(){
		
		$space = '&nbsp;&nbsp;';
		
		$Part3_Title = "<b>Academic Performance</b>";
		$Part3_RemarkArr = array();
		
		// Change percentage remark [2014-1014-1144-21164]
//		$Part3_RemarkArr[] = "A".$space."Top 5%";
//		$Part3_RemarkArr[] = "B".$space."Next 15%";
//		$Part3_RemarkArr[] = "C".$space."Next 20%";
//		$Part3_RemarkArr[] = "D".$space."Next 30%";
//		$Part3_RemarkArr[] = "E".$space."Next 15%-30%";
//		$Part3_RemarkArr[] = "F".$space."Next 0%-15%";
//		$Part3_RemarkArr[] = "A-E".$space."Pass";
//		$Part3_RemarkArr[] = "F".$space."Fail";
		$Part3_RemarkArr[] = "A".$space."Top 10%";
		$Part3_RemarkArr[] = "B".$space."Next 15%";
		$Part3_RemarkArr[] = "C".$space."Next 25%";
		$Part3_RemarkArr[] = "D".$space."Next 20%";
		$Part3_RemarkArr[] = "E".$space."Next 15%-30%";
		$Part3_RemarkArr[] = "F".$space."Next 0%-15%";
		$Part3_RemarkArr[] = "A-E".$space."Pass";
		$Part3_RemarkArr[] = "F".$space."Fail";
		
		// Added remark [2014-1014-1144-21164]
		$Part3_ExtraRemark = "(Minor adjustment of the above percentages for subjects with less than 10 students)";
		
		$Page_break='page-break-after:always;';
		
		$table_style = 'border-collapse: collapse; '; 
		
		$Part3Table_html =$this->getAcademicResultContentDisplay($DataArr='',$Part3_RemarkArr,$Part3_ExtraRemark);
		
		$html = '';
		$html .= $this->getEmptyTable('2%');
		$html .= $this->getPart1_HTML();
		$html .= $this->getPart2_HTML();
		$html .= $this->getStringDisplay($Part3_Title,'font_16px');
		$html .= '<table cellpadding="5" style="'.$table_style.'" width="100%">';
			$html .= '<tr>
						<td height="800px" valign="top">
						'.$Part3Table_html.'
						</td>
					 </tr>';
		$html .='</table>';
		$html .=$this->getPart8A_HTML($Page_break);
		

		return $html;
		
	}		
	
	private function getAcademicResultContentDisplay($DataArr='',$RemarkArr='',$ExtraRemark='')
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$_borderTop_blue =  'border-top: 1px solid '.$this->colorBlue.'; ';
		$_borderLeft_blue =  'border-left: 1px solid '.$this->colorBlue.'; ';
		$_borderBottom_blue =  'border-bottom: 1px solid '.$this->colorBlue.'; ';
		$_borderRight_blue =  'border-right: 1px solid '.$this->colorBlue.'; ';
		
		$remark_html ='';
		$remark_html = '<table width="100%"><tr>';
		
		for($i=0,$i_MAX=count($RemarkArr);$i<$i_MAX;$i++)
		{
			$remark_html .='<td style="font-size:12px;color:'.$this->colorBlue.';"><font face="Times New Roman">'.$RemarkArr[$i].'</font></td>';
		}
		$remark_html .= '</tr></table>';
		
		// Add one remark line [2014-1014-1144-21164]
		if($ExtraRemark){
			$remark_html .= '<table width="100%"><tr>';
				$remark_html .= '<td style="font-size:12px;color:'.$this->colorBlue.';"><font face="Times New Roman">'.$ExtraRemark.'</font></td>';
			$remark_html .= '</tr></table>';
		}
		
		$ContentTable = $this->getAcademicResult_ContentTable();	
		
		$html='';
		$html='<table width="90%" height="" cellpadding="0" border="0px" align="center" style="border-collapse: collapse;">
					<tr>				 							
						<td style="vertical-align: top;">'.$ContentTable.'</td>						 														
					</tr>
					<tr>				 							
						<td style="padding-left:8px;vertical-align: top;'.$_borderTop_blue.$_borderLeft_blue.$_borderBottom_blue.$_borderRight_blue.'">'.$remark_html.'</td>						 														
					</tr>
			   </table>';
	
		return $html;
	}
	
	private function getAcademicResult_ContentTable()
	{	
		// this variable can control whether combine 
		//1) 902	Aesthetic Education	AE	AE	 subject id (55), 
		//2) 904	Aesthetic Education (Drama and Music)	AEA	AEA	 subject id (109), 
		//3) 905	Aesthetic Education (Drama and Music)	AEB	AEB subject id (110)
		//if seperate the three subject ($combineAEA_AEB_AE = false), the three subject must be "SELECTED" for the report template
		//if combine the three subject ($combineAEA_AEB_AE = ture), suppose the there is only one AE subject for the report template
		$combineAEA_AEB_AE = false; 

		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$_borderTop_blue =  'border-top: 1px solid '.$this->colorBlue.'; ';
		$_borderLeft_blue =  'border-left: 1px solid '.$this->colorBlue.'; ';
		$_borderBottom_blue =  'border-bottom: 1px solid '.$this->colorBlue.'; ';
		$_borderRight_blue =  'border-right: 1px solid '.$this->colorBlue.'; ';
				
	//	$_borderLeft_blue_double =  'border-left: 3px double '.$this->colorBlue.'; ';
	//	$_borderRight_blue_double =  'border-right: 3px double '.$this->colorBlue.'; ';
	
		$_borderLeft_blue_double = 'border-left: 1px solid '.$this->colorBlue.'; ';
		$_borderRight_blue_double =  'border-right: 1px solid '.$this->colorBlue.'; ';
		
		$YearClassArray = $this->getStudentAcademic_YearClassNameArr(true);
	
		$YearTitleArr = $this->getAcademicYearArr($this->academicYearIDArr,$IsTrim=false);
	
		$Subject_string = 'Subjects';
		$FirstTerm_string = 'First Term';
		$SecondTerm_string = 'Second Term';
		$Yearly_string = 'Yearly';
		
		$StudentID = $this->uid;
		
		$F6_year='';
		$F6_className='';

		$countColumn=0;

		$MarkArr = array();	
//hdebug_r($YearClassArray);		
		for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
		{
			$thisYear = $YearClassArray[$i]['AcademicYear'];
			$thisClassName = $YearClassArray[$i]['ClassName'];
			$thisFormNo = (int)$thisClassName;
			
			if($i==($i_MAX-1))
			{
				$Final_year = $thisYear;
				$Final_className = $thisClassName;
			}
			
			if($thisFormNo==6)
			{				
				$thisMark_Sem2 = $this->getMarkArr($thisYear,$thisClassName,"2");
				
				$MarkArr[$thisYear]['Yearly'] = $thisMark_Sem2;
				
				$countColumn++;
			}
			else
			{				
				$thisMark_Sem1 = $this->getMarkArr($thisYear,$thisClassName,"1");
				$thisMark_Sem2 = $this->getMarkArr($thisYear,$thisClassName,"2");
					
				$MarkArr[$thisYear]['Sem1'] = $thisMark_Sem1;
				$MarkArr[$thisYear]['Sem2'] = $thisMark_Sem2;
			
				$countColumn +=2;
			}	
		}
		
		
//		if ($_SESSION['UserID']==1) {
//			debug_pr($MarkArr['2009']['Sem1'][51][0]);
//		}
		
		
		$Final_year = substr($Final_year,0,4);

		$MainSubjectInfoArr = $this->getSubjectInfo($Final_year,$Final_className);	
//debug_r($Final_year);
		//hdebug_r($MainSubjectInfoArr);			
		
		$haveICTA = false;
		$haveICTB= false;
		
		$haveAE = false;
		$haveAEA= false;
		$haveAEB = false;
	
		foreach((array)$MainSubjectInfoArr as $thisParentSubjectID => $thisSubjectInfoArr)
		{




			foreach((array)$thisSubjectInfoArr as $thisSubjectID => $thisSubjectName)
			{ 		
//error_log("thisParentSubjectID [".$thisParentSubjectID."] thisSubjectID [".$thisSubjectID."] thisSubjectName [".$thisSubjectName."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");				
//hdebug_r($thisParentSubjectID.' '.$thisSubjectID.' '.$thisSubjectName);			
				if ($thisSubjectID==0) {
					$thisIsComponentSubject = false;
					$thisSubjectID = $thisParentSubjectID;
				}
				else {
					$thisIsComponentSubject = true;
					// continue;  // if you do not want to show the component subject, just uncomment it.
					
					if($thisParentSubjectID=='27')	 //Combined Science I (Physics, Chemistry)
					{
						continue;
					}
					if($thisParentSubjectID=='26')	 //Combined Science II (Biology, Chemistry)
					{
						continue;
					}
				}
				
				
				$IsStudy = false;
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{			
					
					$thisYear = $YearClassArray[$i]['AcademicYear'];
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;
						
					if($thisFormNo==6)
					{
						$thisMarkArr_Year = $MarkArr[$thisYear]['Yearly'] ;
					
						$thisScaleInput = $thisMarkArr_Year[$thisSubjectID]['ScaleInput'];
						$thisScaleDisplay = $thisMarkArr_Year[$thisSubjectID]['ScaleDisplay'];
						
						$thisGrade = $thisMarkArr_Year[$thisSubjectID][0]["Grade"];
						$thisMark = $thisMarkArr_Year[$thisSubjectID][0]["Mark"]; 
						
						if ($thisGrade != 'N.A.' && $thisGrade != '') {
							$IsStudy = true;					
						}
												
						$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Yearly'] = $thisGrade;
						
					}
					else
					{			
						$thisMarkArr_Sem1 = $MarkArr[$thisYear]['Sem1'] ;
						$thisMarkArr_Sem2 = $MarkArr[$thisYear]['Sem2'] ;
						
						$thisScaleInput_Sem1 = $thisMarkArr_Sem1[$thisSubjectID]['ScaleInput'];
						$thisScaleDisplay_Sem1 = $thisMarkArr_Sem1[$thisSubjectID]['ScaleDisplay'];
						
						$thisScaleInput_Sem2 = $thisMarkArr_Sem2[$thisSubjectID]['ScaleInput'];
						$thisScaleDisplay_Sem2 = $thisMarkArr_Sem2[$thisSubjectID]['ScaleDisplay'];
						
						$thisGrade_Sem1 = $thisMarkArr_Sem1[$thisSubjectID][0]["Grade"];
						$thisMark_Sem1 = $thisMarkArr_Sem1[$thisSubjectID][0]["Mark"]; 
						
						$thisGrade_Sem2 = $thisMarkArr_Sem2[$thisSubjectID][0]["Grade"];
						$thisMark_Sem2 = $thisMarkArr_Sem2[$thisSubjectID][0]["Mark"];
						
						if ($thisGrade_Sem1 != 'N.A.' && $thisGrade_Sem1 != '') {
							$IsStudy = true;
						}

						if ($thisGrade_Sem2 != 'N.A.' && $thisGrade_Sem2 != '') {
							$IsStudy = true;
						}
						
//						if ($_SESSION['UserID']==1 && $thisYear=='2009' && $thisSubjectID==51) {
//							debug_pr('$thisGrade_Sem1 = '.$thisGrade_Sem1);
//							debug_pr('$thisMark_Sem1 = '.$thisMark_Sem1);
//							debug_pr('$thisGrade_Sem2 = '.$thisGrade_Sem2);
//							debug_pr('$thisMark_Sem2 = '.$thisMark_Sem2);
//							debug_pr('$IsStudy = '.$IsStudy);
//							debug_pr('=========================================');
//						}	
			
						$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Sem1'] = $thisGrade_Sem1;
						$NewSubjectSequenceArr[$thisSubjectID][$thisYear]['Sem2'] = $thisGrade_Sem2;	
					}			
				}
				
				if($IsStudy)
				{
					
					if($thisSubjectID=='51')
					{
						$haveICTA=true;
					}
					if($thisSubjectID=='108')
					{
						$haveICTB=true;
					}
					if($thisSubjectID=='55')
					{
						if($combineAEA_AEB_AE){						
							$haveAE=true;
						}
					}
					if($thisSubjectID=='109')
					{
						if($combineAEA_AEB_AE){						
							$haveAEA=true;
						}
					}
					if($thisSubjectID=='110')
					{
						if($combineAEA_AEB_AE){						
							$haveAEB=true;
						}
					}
				}
				
				$NewSubjectSequenceArr[$thisSubjectID]['IsStudy']=$IsStudy;
				$NewSubjectSequenceArr[$thisSubjectID]['thisIsComponentSubject']=$thisIsComponentSubject;
				if($thisIsComponentSubject)
				{
					$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = '&nbsp;&nbsp;&nbsp;-&nbsp;'.$thisSubjectName;					
				}
				else
				{						
					$NewSubjectSequenceArr[$thisSubjectID]['SubjectNameEn'] = $thisSubjectName;
				}			
			}
		}	
		
		
		$SpecialCase_SubjectID_Array = array();	
		
		$SpecialCase_SubjectID_Array[] ='51';
		$SpecialCase_SubjectID_Array[] ='108';
		
		if($combineAEA_AEB_AE){						
			$SpecialCase_SubjectID_Array[] ='55';
			$SpecialCase_SubjectID_Array[] ='109';
			$SpecialCase_SubjectID_Array[] ='110';
		}


		$html='';
		
		$html .='<table width="100%" cellpadding="3" style="border-collapse: collapse;">';
		
			$html .='<col width="40%"/>';
			
			if($countColumn>0)
			{
				$thisWidth = 60/$countColumn;
			}
			
			for($i=0,$i_MAX=$countColumn;$i<$i_MAX;$i++)
			{			
				$html .='<col width="'.$thisWidth.'%"/>';
			}
					
			$html .='<tr>';
				$html .='<td rowspan="2" class="subject_score" style="text-align:left;padding-left:8px;color:'.$this->colorBlue.';'.$_borderTop_blue.$_borderLeft_blue.$_borderBottom_blue.$_borderRight_blue.'">'.$Subject_string.'</td>';
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisYear = $YearClassArray[$i]['AcademicYear'];
					
					foreach((array)$YearTitleArr as $_AcaYearID => $_year)
					{
						$tmpYear = substr($_year,0,4);
						
						if($thisYear==$tmpYear)
						{
							$thisYear = $_year;
							break;
						}
					}
					
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;
					
					if($thisFormNo==6)
					{
						$colspan ='';
					}
					else
					{
						$colspan ='colspan="2"';
					}
					
					$html .='<td '.$colspan.' class="subject_score"  style="text-align:center;color:'.$this->colorBlue.';'.$_borderTop_blue.$_borderLeft_blue_double.$_borderBottom_blue.$_borderRight_blue.'">'.$thisYear.'<br/>F.'.$thisFormNo.'</td>';
				}			
			$html .='</tr>';
			$html .='<tr>';
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;
					
					if($thisFormNo==6)
					{
						$html .='<td class="" style="font-size:12px;text-align:center;color:'.$this->colorBlue.';'.$_borderTop_blue.$_borderLeft_blue_double.$_borderBottom_blue.$_borderRight_blue.'"><font face="Times New Roman">'.$Yearly_string.'</font></td>';
					}
					else
					{
						$html .='<td class="" style="font-size:12px;text-align:center;color:'.$this->colorBlue.';'.$_borderTop_blue.$_borderLeft_blue_double.$_borderBottom_blue.$_borderRight_blue.'"><font face="Times New Roman">'.$FirstTerm_string.'</font></td>';
						$html .='<td class="" style="font-size:12px;text-align:center;color:'.$this->colorBlue.';'.$_borderTop_blue.$_borderLeft_blue.$_borderBottom_blue.$_borderRight_blue.'"><font face="Times New Roman">'.$SecondTerm_string.'</font></td>';
					}									
				}								
			$html .='</tr>';
					
			
			$ICT_array = array();
			
			$AE_array=array();
			
			foreach((array)$NewSubjectSequenceArr as $thisSubjectID => $thisSubjectInfoArr)
			{		
				$_IsStudy = $NewSubjectSequenceArr[$thisSubjectID]['IsStudy'];
//error_log("thisSubjectID [".$thisSubjectID."] [".$thisSubjectName."]  _IsStudy [".$_IsStudy."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");				
				if($_IsStudy==false)
				{
					continue;
				}	
				
				
				$thisSubjectName = $thisSubjectInfoArr['SubjectNameEn'];
				$thisIsComponentSubject = $thisSubjectInfoArr['thisIsComponentSubject'];
				
				if($thisIsComponentSubject)
				{
					$fontSize='9px';
					$class_html = 'class="sub_subject_score" ';
				}
				else
				{
					$fontSize='12px';
					$class_html = 'class="subject_score" ';
				}
//error_log("thisSubjectID [".$thisSubjectID."] [".$thisSubjectName."]  <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
				if(in_array($thisSubjectID,$SpecialCase_SubjectID_Array))
				{
					if($haveICTA && $haveICTB)
					{
						if($thisSubjectID!=51)
						{
							$html .='<tr>';		
							$html .='<td '.$class_html.'  style="padding-left:9px;text-align:left;'.$_borderLeft_blue.$_borderRight_blue.'">'.$thisSubjectName.'</td>';	
						}
					}
					else
					{
						if(($haveAE && $haveAEA) || ($haveAE && $haveAEB))
						{
							if($thisSubjectID!=55)
							{
								$html .='<tr>';		
								$html .='<td '.$class_html.' style="padding-left:9px;text-align:left;'.$_borderLeft_blue.$_borderRight_blue.'">'.$thisSubjectName.'</td>';	
							}
						}
						else
						{
							$html .='<tr>';		
							$html .='<td '.$class_html.' style="padding-left:9px;text-align:left;'.$_borderLeft_blue.$_borderRight_blue.'">'.$thisSubjectName.'</td>';
						}					
					}	
				}
				else
				{
					$html .='<tr>';		
					$html .='<td '.$class_html.' style="padding-left:9px;text-align:left;'.$_borderLeft_blue.$_borderRight_blue.'">'.$thisSubjectName.'</td>';
				}
					
									
				
				for($i=0,$i_MAX=count($YearClassArray);$i<$i_MAX;$i++)
				{				
					$thisYear = $YearClassArray[$i]['AcademicYear'];
					$thisClassName = $YearClassArray[$i]['ClassName'];
					$thisFormNo = (int)$thisClassName;				

					if($thisFormNo==6)
					{
						$thisMarkArr_Year = $MarkArr[$thisYear]['Yearly'] ;
						
						$thisGrade = $thisMarkArr_Year[$thisSubjectID][0]["Grade"];
						$thisMark = $thisMarkArr_Year[$thisSubjectID][0]["Mark"]; 
						
						if ($thisGrade == 'N.A.' || $thisGrade == '') {
							$thisGrade = $this->EmptySymbol;
						}
						
						if($haveICTA && $haveICTB)
						{
							if($thisSubjectID==51)
							{
								$ICT_array[$thisYear][$thisFormNo]['Yearly'] = $thisGrade;
								continue;
							}
							if($thisSubjectID==108)
							{
								$thisGrade = ($thisGrade==$this->EmptySymbol)? $ICT_array[$thisYear][$thisFormNo]['Yearly'] : $thisGrade;
							}
						}
						
						if ($thisGrade == 'N.A.' || $thisGrade == '' || $thisGrade == null) {
							$thisGrade = $this->EmptySymbol;
						}
						
						if(($haveAE && $haveAEA) || ($haveAE && $haveAEB))
						{
							if($thisSubjectID==55)
							{
								$AE_array[$thisYear][$thisFormNo]['Yearly'] = $thisGrade;
								continue;
							}
							if($thisSubjectID==109)
							{
								$thisGrade = ($thisGrade=='$this->EmptySymbol')? $AE_array[$thisYear][$thisFormNo]['Yearly'] : $thisGrade;
							}
							if($thisSubjectID==110)
							{
								$thisGrade = ($thisGrade==$this->EmptySymbol)? $AE_array[$thisYear][$thisFormNo]['Yearly'] : $thisGrade;
							}
						}
						
						// 2020-03-03 (Philips) [F180281]
						if($thisGrade == '+'){
							$thisGrade = "Not Assessed";
						}
						
						$html .='<td '.$class_html.' style="padding-left:9px;text-align:center;'.$_borderLeft_blue_double.$_borderRight_blue.'">'.$thisGrade.'</td>';
					}
					else
					{			
						$thisMarkArr_Sem1 = $MarkArr[$thisYear]['Sem1'] ;
						$thisMarkArr_Sem2 = $MarkArr[$thisYear]['Sem2'] ;
						
						$thisGrade_Sem1 = $thisMarkArr_Sem1[$thisSubjectID][0]["Grade"];
						$thisMark_Sem1 = $thisMarkArr_Sem1[$thisSubjectID][0]["Mark"]; 
						
						$thisGrade_Sem2 = $thisMarkArr_Sem2[$thisSubjectID][0]["Grade"];
						$thisMark_Sem2 = $thisMarkArr_Sem2[$thisSubjectID][0]["Mark"]; 
						
						if ($thisGrade_Sem1 == 'N.A.' || $thisGrade_Sem1 == '' || $thisGrade_Sem1 == null) {
							$thisGrade_Sem1 = $this->EmptySymbol;
						}
						
						if ($thisGrade_Sem2 == 'N.A.' || $thisGrade_Sem2 == '' || $thisGrade_Sem2 == null) {
							$thisGrade_Sem2 = $this->EmptySymbol;
						}
						
						if($haveICTA && $haveICTB)
						{
//							if($thisSubjectID==51)
//							{
//								$ICT_array[$thisYear][$thisFormNo]['Sem1'] = $thisGrade_Sem1;
//								$ICT_array[$thisYear][$thisFormNo]['Sem2'] = $thisGrade_Sem2;
//								
//								continue;
//							}
//							if($thisSubjectID==108)
//							{
//								$thisGrade_Sem1 = ($thisGrade_Sem1==$this->EmptySymbol)? $ICT_array[$thisYear][$thisFormNo]['Sem1'] : $thisGrade_Sem1;
//								$thisGrade_Sem2 = ($thisGrade_Sem2==$this->EmptySymbol)? $ICT_array[$thisYear][$thisFormNo]['Sem2'] : $thisGrade_Sem2;
//							}
							
							// 2012-0220-1413-58132
							if ($thisSubjectID==51) {
								continue;
							}
							else if ($thisSubjectID==108) {
								$thisGrade_Sem1 = ($thisGrade_Sem1==$this->EmptySymbol)? $MarkArr[$thisYear]['Sem1'][51][0]["Grade"] : $thisGrade_Sem1;
								$thisGrade_Sem2 = ($thisGrade_Sem2==$this->EmptySymbol)? $MarkArr[$thisYear]['Sem2'][51][0]["Grade"] : $thisGrade_Sem2;
							}
						}
						
						if(($haveAE && $haveAEA) || ($haveAE && $haveAEB))
						{
							if($thisSubjectID==55)
							{
								$AE_array[$thisYear][$thisFormNo]['Sem1'] = $thisGrade_Sem1;
								$AE_array[$thisYear][$thisFormNo]['Sem2'] = $thisGrade_Sem2;
								continue;
							}
							if($thisSubjectID==109)
							{
								$thisGrade_Sem1 = ($thisGrade_Sem1==$this->EmptySymbol)? $AE_array[$thisYear][$thisFormNo]['Sem1'] : $thisGrade_Sem1;
								$thisGrade_Sem2 = ($thisGrade_Sem2==$this->EmptySymbol)? $AE_array[$thisYear][$thisFormNo]['Sem2'] : $thisGrade_Sem2;
							}
							if($thisSubjectID==110)
							{
								$thisGrade_Sem1 = ($thisGrade_Sem1==$this->EmptySymbol)? $AE_array[$thisYear][$thisFormNo]['Sem1'] : $thisGrade_Sem1;
								$thisGrade_Sem2 = ($thisGrade_Sem2==$this->EmptySymbol)? $AE_array[$thisYear][$thisFormNo]['Sem2'] : $thisGrade_Sem2;
							}
						}
						
						if ($thisGrade_Sem1 == 'N.A.' || $thisGrade_Sem1 == '' || $thisGrade_Sem1 == null) {
							$thisGrade_Sem1 = $this->EmptySymbol;
						}
						
						if ($thisGrade_Sem2 == 'N.A.' || $thisGrade_Sem2 == '' || $thisGrade_Sem2 == null) {
							$thisGrade_Sem2 = $this->EmptySymbol;
						}
						
						//F111534
						if ($thisGrade_Sem1=="+"){
							$thisGrade_Sem1 = "Not Assessed";
						}
						if ($thisGrade_Sem2=="+"){
							$thisGrade_Sem2 = "Not Assessed";
						}
						
						// #F91216 
						if($thisGrade_Sem1 == 'Not Assessed' ||$thisGrade_Sem2 == 'Not Assess'){
							$class_html1 = ' class="sub_subject_score" ';
						}
						else{
							$class_html1 = $class_html;
						}
						if($thisGrade_Sem2 == 'Not Assessed' || $thisGrade_Sem2 == 'Not Assess'){
							$class_html2 = ' class="sub_subject_score" ';
						}
						else{
							$class_html2 = $class_html;
						}
						
						
						$html .='<td '.$class_html1.' style="padding-left:9px;text-align:center;'.$_borderLeft_blue_double.$_borderRight_blue.'">'.$thisGrade_Sem1.'</td>';
						$html .='<td '.$class_html2.' style="padding-left:9px;text-align:center;'.$_borderLeft_blue.$_borderRight_blue.'">'.$thisGrade_Sem2.'</td>';
					}				
				}	
				
				if($haveICTA && $haveICTB)
				{
					if($thisSubjectID!=51)
					{
						$html .='</tr>';			
					}
				}
				else
				{
					if(($haveAE && $haveAEA) || ($haveAE && $haveAEB))
					{
						if($thisSubjectID!=55)
						{
							$html .='</tr>';								
						}
					}
					else
					{
						$html .='</tr>';		
					}		
				}		
							
			}		
			
			$html .='<tr>';
//	hdebug_r($ICT_array);

			
			if($countColumn==0)
			{
				$noRecord = 'No record found.';
				$html .='<td style="padding-left:9px;font-size:12px;text-align:center;'.$_borderLeft_blue.$_borderRight_blue.'">'.$noRecord.'</td>';
			}
			else
			{
				for($i=0;$i<($countColumn+1);$i++)
				{
					$thisVal = ($i+2);
					
					if($thisVal%2==0)
					{
						$thisBorderLeft = $_borderLeft_blue; 
					}
					else
					{
						$thisBorderLeft = $_borderLeft_blue_double;
						
					}
					$html .='<td style="padding-left:9px;font-size:12px;text-align:left;'.$thisBorderLeft.$_borderRight_blue.'">&nbsp;</td>';
				}
			}
			
			$html .='</tr>';
			
		$html .='</table>';
		
		return $html;
		
	}
	
	private function get_AcademicResult_Data()
	{
		$UserID = $this->uid;
		if($UserID != '')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}	
		
		$Assessment_StdSubject_Record = $this->Get_eClassDB_Table_Name('ASSESSMENT_STUDENT_SUBJECT_RECORD');
		$sql = "SELECT
						*
				FROM
						$Assessment_StdSubject_Record
				WHERE
						1	
						$cond_studentID	
				ORDER BY
						Year asc ";
		$roleData = $this->objDB->returnArray($sql, 2);
		for($i=0, $i_MAX=count($roleData); $i<$i_MAX; $i++)
		{
			$thisSubjectID = $roleData[$i]['SubjectID'];
			$thisAcademicYearID = $roleData[$i]['AcademicYearID'];
			$thisYearTermID = $roleData[$i]['YearTermID'];
			$returnDate[$thisSubjectID][$thisAcademicYearID][$thisYearTermID] = $roleData[$i];
		}
        
		return $returnDate;
	}
	
	private function getSubjectInfo($Year, $ClassName)
	{			
		$AcademicYearIDArr = $this->academicYearIDArr;
		$academicYearArr = $this->getAcademicYearArr($AcademicYearIDArr, true);
        
		$AcaYearID = array_search($Year, $academicYearArr);
		$YearTermID = $this->getYearTermID ($AcaYearID, '1');
		$ClassLevelID = $this->getClassLevelID($AcaYearID, $ClassName);
		
		//hdebug_r('$ClassLevelID '.$ClassLevelID);
		//hdebug_r('$ClassName '.$ClassName);
		//hdebug_r('$AcaYearID '.$AcaYearID);
		//hdebug_r('$YearTermID '.$YearTermID);
		//hdebug_r('$Year '.$Year);
		//hdebug_r($academicYearArr);
		
		$lreportcard = new libreportcard();
		$lreportcard->schoolYear = substr($Year, 0, 4);
		$lreportcard->DBName = $lreportcard->GET_DATABASE_NAME($lreportcard->schoolYear);
		
		$ReportID = $this->getReportID($lreportcard, $ClassLevelID, $YearTermID);
		
		$SubjectTitleArr = $lreportcard->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);
		return $SubjectTitleArr;
	}
	
	private function getSubjectAcademicResult($lreportcard,$ClassLevelID,$ReportID)
	{
		$GradingSchemeInfoArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
		return $GradingSchemeInfoArr;
	}
	
	private function getStudentMark($lreportcard,$ReportID)
	{
		### Get Student Average Marks and Position
	    // $MarksAry[$thisReportID] = $this->getMarks($thisReportID, $StudentID,"","",1);
	}
	
	private function getMarkArr($Year, $ClassName, $Sem, $stopThis=false)
	{
		$StudentID = $this->uid;
		
		$AcademicYearIDArr = $this->academicYearIDArr;
		$academicYearArr = $this->getAcademicYearArr($AcademicYearIDArr, true);
		
		//2012-0216-0942-16066
		//$AcaYearID = array_search($Year, $academicYearArr);
		$AcaYearID = array_search(substr($Year, 0, 4), $academicYearArr);
		$YearTermID = $this->getYearTermID ($AcaYearID, $Sem);
		$ClassLevelID = $this->getClassLevelID($AcaYearID, $ClassName);
		
		$lreportcard = new libreportcard();
		$lreportcard->schoolYear = substr($Year, 0, 4);
		$lreportcard->DBName = $lreportcard->GET_DATABASE_NAME($lreportcard->schoolYear);
		
//		if ($_SESSION['UserID']==1) {
//			debug_pr('$ClassLevelID = '.$ClassLevelID);
//			debug_pr('$YearTermID = '.$YearTermID);
//			debug_pr('$lreportcard->schoolYear = '.$lreportcard->schoolYear);
//			debug_pr('$lreportcard->DBName = '.$lreportcard->DBName);
//			debug_pr('$ReportID = '.$ReportID);
//			debug_pr('$StudentID = '.$StudentID);
//		}

//      if ($stopThis==false && $ReportID == '') {
//          $nextTargetSem = ($Sem==1) ? 2 : 1;
//         	return $this->getMarkArr($Year,$ClassName,$nextTargetSem, $stopNext=true);
//      }
//      else {
//         	if ($_SESSION['UserID']==1) {
//              //debug_r($lreportcard->getMarks($ReportID, $StudentID,"","",1));
//         	}
//         	return $lreportcard->getMarks($ReportID, $StudentID,"","",1);
//      }
		
		$ReportID = $this->getReportID($lreportcard, $ClassLevelID, $YearTermID);
		if ($stopThis == true || $ReportID != '')
		{
		    // [2018-0306-1449-23207] display report result > only when report data is transferred
		    if($ReportID != '' && $this->checkReportIsTransferred($lreportcard, $ReportID))
		    {
		        return $lreportcard->getMarks($ReportID, $StudentID, "", "", 1);
		    }
		    else
		    {
		        return array();
		    }
		}
		else
		{
		    $nextTargetSem = ($Sem == '1') ? '2' : '1';
		    return $this->getMarkArr($Year, $ClassName, $nextTargetSem, $stopNext=true);
		}
		
// 		$MarksAry = $lreportcard->getMarks($ReportID, $StudentID,"","",1);
		
		return '';	
	}
	
	private function getYearTermID($AcaYearID, $Sem)
	{
		if($Sem == '1')
		{
			$cond_orderBy = 'YearTermID asc';
		}
		else if($Sem == '2')
		{
			$cond_orderBy = 'YearTermID desc';
		}
		
		$Academic_YearTerm = $this->Get_IntranetDB_Table_Name('ACADEMIC_YEAR_TERM');
		$sql = "SELECT
						YearTermID
				FROM
						$Academic_YearTerm
				WHERE
						AcademicYearID = '$AcaYearID'
				ORDER BY 
						$cond_orderBy ";
		$returnData = $this->objDB->returnArray($sql, 2);
		if(count($returnData) > 0)
		{
			$YearTermID = $returnData[0]['YearTermID'];
		}
        
		return $YearTermID;
	}
	
	private function getClassLevelID($AcaYearID, $ClassName)
	{
		$Year_Class = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
		$sql = "SELECT
						YearID as ClassLevelID
				FROM
						$Year_Class
				WHERE
						AcademicYearID = '$AcaYearID'
						AND ClassTitleEN = '$ClassName'	";
		$returnData = $this->objDB->returnArray($sql, 2);
		$returnData = current($returnData);
		$ClassLevelID = $returnData['ClassLevelID'];
		
		return $ClassLevelID;
	}
	
	private function getReportID($lreportcard, $ClassLevelID, $YearTermID)
	{
		$ReportBasicInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID='', $others='', $ClassLevelID, $YearTermID, $isMainReport=1, $excludeSemester='');
		$ReportID = $ReportBasicInfoArr['ReportID'];
        
		return $ReportID;
	}
	
	private function checkReportIsTransferred($lreportcard, $ReportID='')
	{
	    $isReportTransferred = false;
	    if($ReportID == '')
	    {
	        // do nothing
	    }
	    else
	    {
	        $report_template = $lreportcard->DBName.".RC_REPORT_TEMPLATE";
	        $sql = "SELECT LastDateToiPortfolio FROM $report_template WHERE ReportID = '$ReportID'";
	        $ReportTransferDate = $this->objDB->returnVector($sql, 2);
	        $ReportTransferDate = $ReportTransferDate[0];
	        
	        $isReportTransferred = !is_date_empty($ReportTransferDate) && checkDateIsValid(substr($ReportTransferDate, 0, 10));
	    }
	    
	    return $isReportTransferred;
	}
	/***************** End Part 3 ******************/
	
	
	
	/***************** Part 4 : Other Learning Experience******************/	
	public function getPart4_HTML($OLEInfoArr_INT){
		
		$Part3_Title = '<b>Other Learning Experiences</b>';
		$Part3_Remark = '*Evidence of awards/certifications/achievements listed is available for submission when required.';
		
		
		$titleArray = array();	
		$titleArray[] = 'School Years';
		$titleArray[] = 'Programmes';
		$titleArray[] = 'Roles';
		$titleArray[] = 'Categories';
		$titleArray[] = 'Awards /<br/>Achievements';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="12%" />';
		$colWidthArr[] = '<col width="28%" />';
		//F111534  16->12
		$colWidthArr[] = '<col width="12%" />';
		$colWidthArr[] = '<col width="28%" />';
		//F111534  16->20
		$colWidthArr[] = '<col width="20%" />';
		
		$DataArray = array();
		$DataArray = $OLEInfoArr_INT[$this->uid]['INT'];
		$StringDisplayArr ['Title']= $Part3_Title;
		$StringDisplayArr ['Remark']=  $Part3_Remark;
		
		$html = '';
		
		$html .= $this->getPartContentDisplay_PageRowLimit('4',$titleArray,$colWidthArr,$DataArray,$hasPageBreak=true,$StringDisplayArr);
		
		return $html;
		
	}	
	/***************** End Part 4 ******************/
	
	
	
	/***************** Part 5 ******************/	
	public function getPart5_HTML(){
		
		$Part5_Title = '<b>List of Awards and Major Achievements Issued by the School</b>';
		$Part5_Remark = '&nbsp;';
		
		$titleArray = array();
		$titleArray[] = 'School Years';
		$titleArray[] = 'Terms';
		$titleArray[] = 'Awards';
		//$titleArray[] = 'Awards (Yearly)';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="70%" />';
		
		$DataArr = array();
		$DataArr = $this->getAward_Info();
		
		$StringDisplayArr ['Title']= $Part5_Title;
		$StringDisplayArr ['Remark']=  $Part5_Remark;
		
		$html = '';
		$html .= $this->getPartContentDisplay_PageRowLimit('5',$titleArray,$colWidthArr,$DataArr,$hasPageBreak=true,$StringDisplayArr);
		
		return $html;
		
	}
	
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$UserID = $this->uid;
		
		if($UserID!='')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And a.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						IFNULL(NULLIF(Semester,''),'Whole Year') as 'Semester',
						AwardName
				From
						$Award_student as a
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = a.AcademicYearID
				
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc, 
						Semester,
						AwardName asc		

				";
		$roleData = $this->objDB->returnArray($sql,2);
//hdebug_r($sql);
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();

			$thisDataArr['AcademicYearID'] =$roleData[$i]['YEAR_NAME'];
			$thisDataArr['Semester'] =$roleData[$i]['Semester'];
			$thisDataArr['AwardName'] =$roleData[$i]['AwardName'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	/***************** End Part 5 ******************/
	
	
	/***************** Part 6 ******************/
	public function getPart6_HTML($OLEInfoArr_EXT){
		
		$Part6_Title = '<b>Performance / Awards and Key Participation Outside School</b>';
		$Part6_Remark = '*Information is provided by the student.<br/>*Evidence of awards/certifications/achievements listed is available for submission when required.';
		
		$titleArray = array();
		$titleArray[] = 'School Years';
		$titleArray[] = 'Programmes';	
		$titleArray[] = 'Roles';
		$titleArray[] = 'Organizations';
		$titleArray[] = 'Awards /<br/>Achievements';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="12%" />';
		$colWidthArr[] = '<col width="28%" />';
		$colWidthArr[] = '<col width="16%" />';
		$colWidthArr[] = '<col width="28%" />';
		$colWidthArr[] = '<col width="16%" />';	
		
		
		$StringDisplayArr ['Title']= $Part6_Title;
		$StringDisplayArr ['Remark']=  $Part6_Remark;
		
		
		$DataArray = array();
		$DataArray = $OLEInfoArr_EXT[$this->uid]['EXT'];
		
		$html = '';
		$html .= $this->getPartContentDisplay_PageRowLimit('6',$titleArray,$colWidthArr,$DataArray,$hasPageBreak=true,$StringDisplayArr);
		return $html;
		
	}
	

	
	/***************** End Part 6 ******************/
	
	
	
	/***************** Part 7 ******************/
	
	public function getPart7_HTML(){
		
		$Part7_Title = "Student's Self-Account";
		$Part7_Remark = '';
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		
		$StringDisplayArr ['Title']= $Part7_Title;
		$StringDisplayArr ['Remark']=  $Part7_Remark;
		
		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$StringDisplayArr) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$UserID = $this->uid;
		
		if($UserID!='')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}	
		$cond_DefaultSA = "And DefaultSA is not null And DefaultSA!='NULL' And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details']; 
			$thisDataArr['Details'] = strip_tags($thisDataArr['Details'],'<p><br>');
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$StringDisplayArr='')
	{
		
		$_borderTop =  'border-top: 1px solid '.$this->colorBlue .'; ';
		$_borderLeft =  'border-left: 1px solid '.$this->colorBlue .'; ';
		$_borderBottom =  'border-bottom: 1px solid '.$this->colorBlue .'; ';
		$_borderRight =  'border-right: 1px solid '.$this->colorBlue .'; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:14px;" '; 


		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count<=0)
		{		
			$html =$this->getEmptyTable('2%');
			$html .= $this->getPart1_HTML();
			$html .= $this->getPart2_HTML();
			
			$html .= $this->getStringDisplay($StringDisplayArr['Title'],'font_14px');
			$html .= '<table cellpadding="0" style="" width="100%">
						<tr>
							<td height="800px" valign="top">
								<table width="90%" height="" cellpadding="7" border="0px" align="center" '.$table_style.'>			
									<tr>
									<td style="vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'text-align:center;"><b>No record found.</b></td>	
									</tr>
								</table>
							</td>
						</tr>
					</table>';
			$html .= $this->getPart8B_HTML();

		}
		else if ($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];			
	
			$DetailArr = explode($this->SelfAccount_Break,$Detail);	
	
			for ($i=0,$i_MAX=count($DetailArr); $i<$i_MAX; $i++)
			{		
				if($i==0)
				{
					$thisTitle = '<b>'.$StringDisplayArr['Title'].'</b>';
				}
				else
				{
					$thisTitle = '<b>'.$StringDisplayArr['Title']." (Continued)".'</b>';
				}
				if($i==$i_MAX-1)
				{
					$Page_break='';
				}
								
				$html .= $this->getEmptyTable('2%');
				$html .= $this->getPart1_HTML();
				$html .= $this->getPart2_HTML();
				$html .= $this->getStringDisplay($thisTitle,'font_16px');
				$html .= '<table cellpadding="0" style="" width="100%">';
					$html .= '<tr>
								<td height="800px" valign="top">
									<table width="90%" height="650px" cellpadding="7" border="0px" align="center" '.$table_style.'>
										<tr>				 							
										<td style="vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'font-size:16px;"><font face="Times New Roman">'.$DetailArr[$i].'</font></td>						 														
										</tr>
									</table>
								</td>
							 </tr>';
				$html .='</table>';
				$html .=$this->getPart8B_HTML($Page_break);
				
			}
		}
			
		return $html;
	}
	
	
	/***************** End Part 7 ******************/
	
	
	
	/***************** Part 8 : Footer A ******************/
	public function getPart8A_HTML($PageBreak='')
	{
		$SchoolChopStr = "School Chop";
		$PrincipalSignStr = "Principal's Signature";
		
		$_borderTop =  'border-top: 1px solid '.$this->colorBlue.'; ';
		
		$html = '';
		
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$PageBreak.'">		
					<col width="12%" />
					<col width="25%" />
					<col width="26%" />
					<col width="25%" />
					<col width="12%" />

				 	<tr>
						<td>&nbsp;</td>
					    <td class="ms_footer_score" align="center" style="'.$_borderTop.';color:'.$this->colorBlue.';">'.$SchoolChopStr.'</td>
						<td>&nbsp;</td>
						<td class="ms_footer_score" align="center" style="'.$_borderTop.';color:'.$this->colorBlue.';">'.$PrincipalSignStr.'</td>
						<td>&nbsp;</td>
					</tr>

				  </table>';
		return $html;
		
	}
	/***************** End Part 8A ******************/
	
	
	
	
	/***************** Part 8 : Footer B ******************/
	public function getPart8B_HTML($PageBreak='')
	{
		$StudentSign = "Student's Signature";
		
		$_borderTop =  'border-top: 1px solid '.$this->colorBlue.'; ';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$PageBreak.'">		
					<col width="80%" />
					<col width="20%" />

				 	<tr>
						<td>&nbsp;</td>
						<td class="ms_footer_score" align="center" style="'.$_borderTop.';color:'.$this->colorBlue.';font-size:16px;">'.$StudentSign.'</td>
					</tr>

				  </table>';
		return $html;
	}
	/***************** End Part 8B ******************/
	
	
	private function getStringDisplay($String='',$class_html='',$Page_break='') {
		
		$html = '';
		$html .= '<table width="90%" cellpadding="7" border="0px" align="center" class="'.$class_html.'" style="" '.$Page_break.'>					
		   			 <tr>
					    <td><font style="color:'.$this->colorBlue.';">'.$String.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}

	
	
	
	private function getPartContentDisplay_PageRowLimit($PartNum, $subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false,$StringDisplayArr='')
	{	
		$Page_break='page-break-after:always;';
		$Page_break = ($hasPageBreak==false)? '':$Page_break;
		
		
		/****** Settings *****/		
		###		$RowPerPage : it indicate the number of rows perpage. If you want to set 2 row perpage, change it to 2;			
		###     $RowHeight  : the height of each row
		###	    $font_size  : the font size of data
		###     $footer_html: the signature footer
		
		if($PartNum=='4')
		{
			// #F133749 
			//$RowPerPage = 12;
			$RowPerPage = 10;
			$RowHeight = 40; 	 
			$font_size = '10px';
			$footer_html = $this->getPart8A_HTML($Page_break);  //the school chop and principal signature								
		}			
		else if($PartNum=='5')
		{
			$RowPerPage = 20;
			$RowHeight = 30; 
			$font_size = '10px';	
			$footer_html = $this->getPart8A_HTML($Page_break);  						
		}
		if($PartNum=='6')
		{
			$RowPerPage = 12;
			$RowHeight = 40; 	 
			$font_size = '10px';	
			$footer_html = $this->getPart8B_HTML($Page_break);  //the Student signature								
		}	
		
		/***** End Settings *********/
		
				
		$rowMax_count = count($DataArr);
		$RowHeight_style = 'height="'.$RowHeight.'px"'; 
		

			
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$_borderTop_blue =  'border-top: 1px solid '.$this->colorBlue.'; ';
		$_borderLeft_blue =  'border-left: 1px solid '.$this->colorBlue.'; ';
		$_borderBottom_blue =  'border-bottom: 1px solid '.$this->colorBlue.'; ';
		$_borderRight_blue =  'border-right: 1px solid '.$this->colorBlue.'; ';
		
		$colSpanStr = '';

		$text_alignStr ='center;';		
		
		
		$html = '';
		
		$HeaderInfo =$this->getEmptyTable('2%');
		$HeaderInfo .=$this->getPart1_HTML();
		$HeaderInfo .=$this->getPart2_HTML();
		$HeaderInfo .= $this->getStringDisplay($StringDisplayArr['Title'],'font_16px');

		$HeaderInfo .= '<table align="center" width="90%" height="800px" cellpadding="0" style="">
						<tr><td valign="top">';
	//	$HeaderInfo .= '<table height="" width="100%" cellpadding="7" border="0px" align="center" style="font-size:'.$font_size.';border-collapse: collapse;">';
		$HeaderInfo .= '<table class="tabletext" height="" width="100%" cellpadding="7" border="0px" align="center" style="border-collapse: collapse;">';
	
						foreach((array)$colWidthArr as $key=>$_colWidthInfo)
						{
							$HeaderInfo .=$_colWidthInfo;
						}
					
		$HeaderInfo .= '<tr '.$RowHeight_style.'>';
					$k=0;
					foreach((array)$subTitleArr as $key=>$_title)
					{
						if($k==0)
						{
							$HeaderInfo .='<td class="font_16px" bgcolor="'.$this->TableTitleBgColor.'" style="text-align:'.$text_alignStr.';vertical-align:text-top;color:'.$this->colorBlue.';'.$_borderLeft_blue.$_borderTop_blue.$_borderRight_blue.'">';
						}
						else
						{
							$HeaderInfo .='<td class="font_16px"  bgcolor="'.$this->TableTitleBgColor.'" style="text-align:'.$text_alignStr.';vertical-align:text-top;color:'.$this->colorBlue.';'.$_borderTop_blue.$_borderRight_blue.'">';
						}
						
						$HeaderInfo .=$_title;
						$HeaderInfo .='</td>';
						$k++;
					}					
		$HeaderInfo .= '</tr>';	
		
		
		$tableBottomInfo .= '<tr><td align="left" style="font-size:14px;color:'.$this->colorBlue.';" colspan="'.count($colWidthArr).'">'.$StringDisplayArr['Remark'].'</td></tr>';	
		$tableBottomInfo .='</table>';
		$tableBottomInfo .='</td></tr></table>';
		$tableBottomInfo .=$footer_html;
		
		if($rowMax_count<=0)
		{
			$count_col =count($colWidthArr);
			
			$content_html =' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft_blue.$_borderTop_blue.$_borderRight_blue.$_borderBottom_blue.'text-align:'.$text_alignStr.'"><b>No record found.</b></td></tr>';	
			
			$html .=$HeaderInfo;
			$html .=$content_html;
			$html .=$tableBottomInfo;
			
		}

		else if($rowMax_count>0)
		{ 
			### 15 rows in one page	
			$NumOfDisplayedRow = 0;
			$SetHeader=false;
			
			
			for ($i=0; $i<$rowMax_count; $i++)
			{		
				$NumOfDisplayedRow = $i;
				
				$content_html = '';
				$content_html .= '<tr '.$RowHeight_style.'>';
							
				$k=0;		 
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{				
					$text_Align= 'center';
					if($k==1)
					{
						$text_Align= 'left';
					}
					
					if($_titleVal=='')
					{
						$_titleVal = $this->EmptySymbol;
					}
						
					if($PartNum=='6')
					{
						if($k==3)
						{
							$text_Align= 'left';
						}
					}					
					if($PartNum == '5'){
						if($k == 1){
							$text_Align= 'center';
						}
					}
// 					$content_html .= ' <td style="text-align:'.$text_Align.';vertical-align:top;'.$_borderTop_blue.$_borderRight_blue.$_borderLeft_blue.$_borderBottom_blue.'">'.$_titleVal.'</td>';
//F111534				
					$content_html .= ' <td style="text-align:'.$text_Align.';vertical-align:top;'.$_borderTop_blue.$_borderRight_blue.$_borderLeft_blue.$_borderBottom_blue.'">'.nl2br($_titleVal).'</td>';			
											 	
					$k++;
				}	
				$content_html .= '</tr>';	

				 if($NumOfDisplayedRow==0)  // if it is first row, add the header
				{
					$header_html=$HeaderInfo;
					
					 if($rowMax_count<=1)  // if there is only one record
					{
						$bottom_html = $tableBottomInfo;
					}				
				}
				else
				{
					$header_html='';						
						
					if(($NumOfDisplayedRow+1)%$RowPerPage==0)   // if it is  10th or 20th or 30th ....row (last row), add </table> 
					{
						$bottom_html = $tableBottomInfo;
						$SetHeader=true;
					}
					else if($SetHeader==true) // if it is 11th or 21th or 31th....row (first row), add the header
					{
						$header_html=$HeaderInfo;
						$SetHeader=false;
						
					}
				
				}
				
				if($NumOfDisplayedRow >= $rowMax_count-1)  // if it is the last row , add </table> 
				{			
					$bottom_html = $tableBottomInfo;			
				}
				

				$html .= $header_html; 		
				$html .= $content_html; // it is the subject content			
				$html .= $bottom_html;
		
				$header_html='';
				$bottom_html='';

			}
		}
		return $html;
	}
	
	public function getStudentAcademic_YearClassNameArr($IsTitle=false)
	{
		$StudentID = $this->uid;
		
		$academicYearIDArr = $this->academicYearIDArr;
		
		$SelectStr= 'h.AcademicYear as AcademicYear,
					 h.ClassName as ClassName';
		
		if($academicYearIDArr!='')
		{		
			$YearArr = array();
			for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
			{
				if($academicYearIDArr[$i]=='')
				{
					continue;
				}
				$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
				
				if($IsTitle)
				{
					$thisAcademicYear = substr($thisAcademicYear,0,4);
				}
				
				$YearArr[] = $thisAcademicYear;
			}
			
			$cond_academicYear = "And h.AcademicYearID In ('".implode("','",(array)$academicYearIDArr)."')";		
		}

		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				where 
						h.UserID = "'.$StudentID.'"
						'.$cond_academicYear.'
				Order by
						h.AcademicYear asc';
						
		$roleData = $this->objDB->returnArray($sql,2);
		
	//hdebug_r($sql);
		return $roleData;
	}
	

	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$OLE_CATE = $this->Get_eClassDB_Table_Name('OLE_CATEGORY');
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
	
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = "And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_cate. EngTitle as category
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
				left join
						$OLE_CATE as ole_cate
				On
						ole_prog.Category =ole_cate.RecordID 
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
									
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
				order by 
						y.Sequence desc,	
						ole_prog.Title asc		

				";
		$roleData = $this->objDB->returnArray($sql,2);

		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$_IntExt = $roleData[$i]['IntExt'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
			
			$thisDataArr = array();
			$thisDataArr['SchoolYear'] =$thisAcademicYear;
			$thisDataArr['Program'] =$roleData[$i]['Title'];			
			$thisDataArr['Role'] =$roleData[$i]['Role'];
			
			if($IntExt=='INT')
			{
				$thisDataArr['Category'] =$roleData[$i]['category'];
			}
			else if($IntExt=='EXT')
			{
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
			}
			$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
			
			$returnDate[$_StudentID][$_IntExt][] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
			$firstYear = substr($returnArray[0], 0, 4);
			$lastYear = substr($returnArray[$lastNo],-4);
			
			$returnValue = $firstYear."-".$lastYear;
			
			return $returnValue;	
		}
		
		
	}
	
	private function getAcademicYearArr($academicYearIDArr,$IsTrim=false)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			
			if($IsTrim)
			{
				$thisAcademicYear = substr($thisAcademicYear,0,4);
			}
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='') {
	
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td>&nbsp;</td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>