<?php
class stmcRptMgr extends ipfReportManager {
	public function stmcRptMgr(){
		parent::ipfReportManager();
	}

	public function getIssueDate(){
		$dateAry = explode('-',$this->issuedate);
		$_year =$dateAry[0]; 
		$_month =$dateAry[1]; 
		$_day =$dateAry[2]; 

		$returnDate = $_day."/".$_month."/".$_year;

		return $returnDate;
	}
}
?>