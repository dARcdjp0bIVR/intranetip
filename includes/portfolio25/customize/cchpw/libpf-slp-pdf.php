<?php

################# Change Log [Start] #####
#
#	Date	:	2016-03-22	Omas
#				Support output PDF format by mpdf
#				- modified displayPage() to writeHTML to pdf
#				- removed all the font tag and font related style
#				- modified getSelfAccContentDisplay() to remove space in self account for chinese contents
#				- added getFooter_HTML()
#
################## Change Log [End] ######

class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	
	private $NumberOfFirstPage;
	private $NumberOfOtherPage;
	private $nextLeftRow;
	private $RowNoNow;
	
	private $YearClassID;
	
	private $AcademicYearStr;
	private $studentName;
	
	private $oleBlockAry;
	private $totalPageNumber;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;
		
		$this->cfgValue = $ipf_cfg;
		
		$this->oleBlockAry = array();
		
		$this->EmptySymbol = '--';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->NumberOfFirstPage = '10';
		$this->NumberOfOtherPage = '27';
		
		$this->nextLeftRow = $this->NumberOfFirstPage;
		$this->RowNoNow = 0;
		
		$this->SchoolTitleEn = 'CHING CHUNG HAU PO WOON SECONDARY SCHOOL';
		$this->SchoolTitleCh = '青 松 侯 寶 垣 中 學';
	}
	
	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	public function setAll_AcademicYearID($All_AcademicYearID)
	{
		$this->All_AcademicYearID = $All_AcademicYearID;
	}
	public function setNoOfFirstPage($NoOfFirstPage){
		$this->NumberOfFirstPage = $NoOfFirstPage;
		$this->nextLeftRow = $this->NumberOfFirstPage;
	}
	public function setNoOfOtherPage($NoOfOtherPage){
		$this->NumberOfOtherPage = $NoOfOtherPage;
	}
	
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	

	
	/***************** Report Header ******************/	
	public function getReportHeader_HTML()  {
	
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:90%; text-align:center; border:0px;">
					<tr>
						<td style="font-size:22px; font-weight:bold; text-align:center; ">'.$this->SchoolTitleCh.'<br/>'.$this->SchoolTitleEn.'</td>
				   </tr>';
			
			$x .= '<tr><td style="font-size:16px; font-weight:bold; ">學 生 學 習 概 覽<br/>Student Learning Profile</td></tr>'."\r\n";
		//	$x .= '<tr><td style="font-size:15px;"><b>Acadamic Year '.$this->YearRangeStr.' 年度</b></td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/******************** End *********************/

	
	/*****************  Student Details ******************/
	public function getStudentInfo_HTML(){
		$result = $this->getStudentInfo();
		$this->studentName = $this->setStudentName($result);
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}
	
	private function setStudentName($studentInfo){
		return $studentInfo["ChineseName"].'&nbsp;&nbsp;&nbsp;&nbsp;'.$studentInfo["EnglishName"];
	}
	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						ChineseName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						ClassNumber,
						ClassName
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";

		$result = $this->objDB->returnArray($sql);
		return $result[0];
	}
	private function getStudentInfoDisplay($result)
	{	
		global $slp_report;
		$studentName = $result["ChineseName"].'<br /><font >'.$result["EnglishName"].'</font>';
		$ClassName = $result["ClassName"] ? $result["ClassName"] : $this->EmptySymbol;
		$ClassNumber = $result["ClassNumber"] ? $result["ClassNumber"] : $this->EmptySymbol;
		$ClassInfo = '<font >'.$ClassName.'('.$ClassNumber.')</font>';
		$RegistrationNo = $result["STRN"] ? '<font >'.$result["STRN"].'</font>' : $this->EmptySymbol;
		$studentGender = '<font >'.( ($result['Gender'] == "F") ? "Female" : "Male" ). "</font>";
		
		$html = <<<HTML
		<table width="100%" style="font-size:12px;">
		<tr>
			<td width="20%"><font >{$slp_report->BiLangOut("['StudentName']")}:</font></td>
			<td width="30%">{$studentName}</td>
			<td width="20%"><font >{$slp_report->BiLangOut("['ClassInfo']")}:</font></td>
			<td width="30%">{$ClassInfo}</td>
		</tr>
		<tr>
		<td colspan="4"><img src="/images/spacer.gif" height="5" width="1" border="0" /></td>
		</tr>
		<tr>
			<td><font >{$slp_report->BiLangOut("['RegistrationNo']")}:</font></td>
			<td>{$RegistrationNo}</td>
			<td><font >{$slp_report->BiLangOut("['Gender']")}:</font></td>
			<td>{$studentGender}</td>
		</tr>	
		</table>
		
HTML;
		$html = $slp_report->GEN_SLP_MODULE_TABLE($slp_report->BiLangOut("['StudentParticulars']",'','','H'), "", $html);
		
		return $html;
	}
	
	/********************** End ***********************/
	
	
	/***************** OLE ******************/	
	
	public function initializeOLE($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false,$isMember=false){
		$OLEInfoArr = $this->getOLE_Info($_UserID,$academicYearIDArr,$IntExt,$Selected,$isMember);
		$this->oleBlockAry = $this->setOLE_Block($OLEInfoArr);
		return count($this->oleBlockAry);
	}	
	
	public function getOLE_Info($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false,$isMember=false)
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		$OLE_Cate = $this->Get_eClassDB_Table_Name('OLE_CATEGORY'); 
		
		$MembershipNo = 7;
		if($isMember)
		{		
			$cond_membershipNo =" And ole_cate.RecordID ='$MembershipNo'";
		}
		else
		{
			$cond_membershipNo =" And ole_cate.RecordID !='$MembershipNo'";
		}
		
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		if($Selected==true)
		{
			$cond_SLPOrder = " And SLPOrder is not null"; 
		}
	
		if($IntExt!='')
		{
			$cond_IntExt = " And ole_prog.IntExt='".$IntExt."'";
		}
		
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = " And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = "And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement,
						ole_prog.ELE as OleComponent,
						ole_std.RecordID,
						ole_std.SLPOrder
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
						ole_std.ProgramID = ole_prog.ProgramID
				Inner join
						$OLE_Cate as ole_cate
				On
						ole_prog.Category = ole_cate.RecordID
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_IntExt
						$cond_SLPOrder	
						$cond_Approve
						$cond_membershipNo
				order by 
						ole_prog.StartDate asc,	
						ole_prog.Title asc		

				";
		$roleData = $this->objDB->returnResultSet($sql);
		
//		debug_r($roleData);
		global $slp_report;
		$roleData = $slp_report->GET_PRINTABLE_OLE_RECORD($roleData, $_UserID, 0, false);
		list($roleData, $NumRecordAllowed, $IsPrintAll) = $roleData;
	//	debug_r($roleData);
	
		if ($IsPrintAll)
		{
			$i_MAX=count($roleData);
		} else
		{
			$i_MAX = $NumRecordAllowed;
			if (count($roleData)<$i_MAX)
			{
				$i_MAX = count($roleData);
			}
		}
		//for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		for($i=0;$i<$i_MAX;$i++)
		{
			$_StudentID = $roleData[$i]['StudentID'];
			$thisAcademicYear = $roleData[$i]['YEAR_NAME'];
				
			if($isMember)
			{
				$thisAcademicYear = $roleData[$i]['YEAR_NAME'];

				$thisDataArr = array();	
	
				$thisDataArr['No'] =$i+1;
				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['Role'] =$roleData[$i]['Role'];
						
				$returnData[$_StudentID][] = $thisDataArr;
			}
			else
			{
				$thisOleComponentArr = explode(',',$roleData[$i]['OleComponent']);
			
				for($a=0,$a_MAX=count($thisOleComponentArr);$a<$a_MAX;$a++)
				{
					$_str = $thisOleComponentArr[$a];
					$_str = substr($_str, 0, strlen($_str)-1); 
					$_str = substr($_str, 1);
					
					$thisOleComponentArr[$a] = $_str;
				}
				
				$thisOLEstr = implode(",",(array)$thisOleComponentArr);
	
				$thisDataArr = array();	
	
				$thisDataArr['No'] =$i+1;
				$thisDataArr['SchoolYear'] =$thisAcademicYear;
				$thisDataArr['Program'] =$roleData[$i]['Title'];
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['OLEComp'] =$thisOLEstr;	
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
						
				$returnData[$_StudentID][] = $thisDataArr;
			}
		
			
		}
		return $returnData;
	}
	
	public function getOLE_HeaderDisply(){
		global $slp_report;
		
		$desc_customized = "<u>其他學習經歷的有關資料，已由學校確認。</u>其他學習經歷可透過由學校舉辦或學校與校外機構合辦的學習活動獲得，包括在上課時間表以內及/或以外的學習時間進行的有關學習經歷。除核心及選修科目外，在高中學習階段的其他學習經歷，尚包括德育及公民教育、藝術發展、體育發展、社會服務及與工作有關的經驗。<br />
<u>Information about Other Learning Experiences has been validated by school.</u> Other Learning Experiences can be achieved through programmes organised by the school or co-organised by the school with outside organisations. They may include learning experiences implemented during time-tabled and/or non-time-tabled learning time. Apart from core and elective subjects, Other Learning Experiences that the student particpates in during his/her senior secondary education include Moral and Civic Education, Aesthetic Development, Physical Development, Community Service and Career-related Experiences.";
		$html = $slp_report->GEN_SLP_MODULE_TABLE($slp_report->BiLangOut("['OtherLearningExperiences']"), '<div style="text-align:justify"><span style="font-size:13px">'.$desc_customized.'</span></div>', '');
		return $html;
	}
	public function getOLE_FooterDisply(){
		global $slp_report;
		
		$html .= <<<HTML
			<table width="100%" style="font-size:10px">
				<tr><td>* {$slp_report->BiLangOut("['AwardsSkill']")}</td></tr>
				<tr><td>** {$slp_report->BiLangOut("['RequiredRemark']")}</td></tr>
				<tr><td>{$slp_report->BiLangOut("['ExperienceRemark']")}</td></tr>
			</table>
HTML;
		return $html;
	}
	public function setOLE_Block($OLEInfoArr){
		global $slp_report;
		
		$oleAry = array();
		
		$emptyRecTable = <<<HTML
			<table width='100%' border='1' cellpadding='0' cellspacing='0'>
				<tr>
					<td height='100' valign='middle' align='center'>--</td>
				</tr>
			</table>		
HTML;

/*
        $ele_arr = $slp_report->GET_ELE();
        $ele_code_arr = array_keys($ele_arr);
        $ele_title_arr = array_values($ele_arr);
        $this->NumberOfOtherPage
		$this->NumberOfFirstPage
		//$Page_break='page-break-after:always;';
*/
		$_OLEInfoArr = $OLEInfoArr[$this->uid]; 
		$_OLETotal = count($_OLEInfoArr);
		$i = 0;
		
		$title_desc_customized = "學會/活動項目(及簡介)*<br />
Clubs/Programmes<br />(with description)*";
		if($_OLEInfoArr){
			$OLETableHeader = "<table width='100%' border='1' cellpadding='5' cellspacing='0' align='center' class='table_print' style=''>\n";
			$OLETableHeader .= "<tr>
							<td class='ole_head_sub' style='text-align:center; font-weight:bold; font-size:12px' width='30%'>{$title_desc_customized}</td>
							<td class='ole_head_sub' style='text-align:center; font-weight:bold; font-size:12px' width='24%'>{$slp_report->BiLangOut("['PartnerOrganizations']")}</td>
							<td class='ole_head_sub' style='text-align:center; font-weight:bold; font-size:12px' width='12%'>{$slp_report->BiLangOut("['SchoolYear']")}</td>
							<td class='ole_head_sub' style='text-align:center; font-weight:bold; font-size:12px' width='12%'>{$slp_report->BiLangOut("['RoleOfParticipation']")}</td>
							<td class='ole_head_sub' style='text-align:center; font-weight:bold; font-size:12px' width='22%'>{$slp_report->BiLangOut("['AchievementsStars']")}</td>
							</tr>\n";
			$OLETableFooter .= "</table>\n";
			foreach($_OLEInfoArr as $oleRecord){
				
				$_programName = $slp_report->stringLanguageSplitByWords($oleRecord['Program'],0,'B5') ? $slp_report->stringLanguageSplitByWords($oleRecord['Program'],0,'B5') : '&nbsp;';
				$_organization = $slp_report->stringLanguageSplitByWords($oleRecord['Organization'],0,'B5') ? $slp_report->stringLanguageSplitByWords($oleRecord['Organization'],0,'B5') : '&nbsp;';
				$_schoolYear = $oleRecord['SchoolYear'] ? $oleRecord['SchoolYear'] : '&nbsp;';
				$_role = $slp_report->stringLanguageSplitByWords($oleRecord['Role'],0,'B5') ? $slp_report->stringLanguageSplitByWords($oleRecord['Role'],0,'B5') : '&nbsp;';
				$_archivement = $slp_report->stringLanguageSplitByWords($oleRecord['Achievement'],0,'B5') ? $slp_report->stringLanguageSplitByWords($oleRecord['Achievement'],0,'B5') : '&nbsp;';
				
				
				$OLETableContent .= "<tr height='25'>
										<td valign='middle' class='ole_content' style='text-align:center; font-size:12px'>".$_programName."</td>
										<td valign='middle' class='ole_content' style='text-align:center; font-size:12px'>".$_organization."</td>
										<td valign='middle' class='ole_content' style='text-align:center; font-size:12px'>".$_schoolYear."</td>
										<td valign='middle' class='ole_content' style='text-align:center; font-size:12px'>".$_role."</td>
										<td valign='middle' class='ole_content' style='text-align:center; font-size:12px'>".$_archivement."</td>
									  </tr>\n";
				$i++;
				if($i==$_OLETotal||$i==$this->NumberOfFirstPage||($i-$this->NumberOfFirstPage)%$this->NumberOfOtherPage==0){
					
					$oleAry[] = $OLETableHeader.$OLETableContent.$OLETableFooter;
					$OLETableContent = '';
				}
			}
		}else{
			$oleAry[] = $emptyRecTable;
		}
		//$html = $slp_report->GEN_SLP_MODULE_TABLE($slp_report->BiLangOut("['OtherLearningExperiences']"), $slp_report->BiLangOut("['OLE_TableDescription']"), $html);
		
		return $oleAry;
	}

	/********************* End ***********************/
	
	
	/***************** School Award ******************/
	function getSchoolAward_HTML(){
		$awardDetails = $this->getAward_Info();
		$html = $this->getAward_HTML($awardDetails);
		return $html;
	}
	public function getAward_HTML($awardDetails){
		global $slp_report;
		/*
		 * $record['YEAR_NAME'];
		 * $record['AwardName'];
		 */
		 
		 $emptyRecTable = "
			<table width='100%' border='0' cellpadding='0' cellspacing='0'>
				<tr>
					<td height='100' valign='middle' align='center'>--</td>
				</tr>
			</table>";
			
		if($awardDetails){
			$content = '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center" class="table_print">';
			$content .= '<tr><td style="text-align:left; vertical-align: top">'.$slp_report->BiLangOut("['SchoolYear']").'</td><td style="text-align:left; vertical-align: top">'.$slp_report->BiLangOut("['AwardsAndAchievements']").'</td></tr>';
			foreach($awardDetails as $record){
				// Change line height: 80px->40px [2015-0213-0951-07207]
				$content .= '<tr style="height:40px"><td style="text-align:left; vertical-align: top; font-size:12px">'.$record['YEAR_NAME'].'</td><td style="text-align:left; vertical-align: top; font-size:12px">'.$slp_report->stringLanguageSplitByWords($record['AwardName'],0,'B5').'</td></tr>';
			}
			$content .= '</table>';
		}else{
			$content = $emptyRecTable;
		}
		$html = $slp_report->GEN_SLP_MODULE_TABLE($slp_report->BiLangOut("['ListOfAwardsAndMABySchool']"), '', $content);
	
		return $html;
	}
	public function getAward_Info(){
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'"; 
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And s.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						AwardName,
						Organization
				From
						$Award_student s
				inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = s.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence asc, 
						AwardName asc		

				";
		$roleData = $this->objDB->returnResultSet($sql);

		$returnArr = array();
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{
			$returnArr[$i]['No'] = $i+1;
			$returnArr[$i]['YEAR_NAME'] = $roleData[$i]['YEAR_NAME'];
			$returnArr[$i]['AwardName'] = $roleData[$i]['AwardName'];
			$returnArr[$i]['Organization'] = $roleData[$i]['Organization'];
		}

		return $returnArr;
	}
		
	/********************* End ***********************/
	
	/********************* Self Account ***********************/
	public function getSelfAccount_HTML(){
		
		$DataArr = $this->get_Self_Account_Info();
		$html = $this->getSelfAccContentDisplay($DataArr) ;
		
		return $html;
		
	}
	
	
	private function get_Self_Account_Info(){
		
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnArray($sql,2);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='')
	{
		global $slp_report;
		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			$Detail = strip_tags($Detail,'<p><br>');
			if(str_lang($Detail) != 'ENG'){
				$Detail = str_replace(' ', '', $Detail);				
			}
			$html = "<table width='100%' border='1' cellpadding='0' cellspacing='0'>
					<tr>
						<td height='100' valign='top' align='left' style='font-size:14px; padding:4px'><div style='text-align:justify'>{$Detail}</div></td>
					</tr>
				</table>";
		}
		else
		{
					
			$html = "<table width='100%' border='1' cellpadding='0' cellspacing='0'>
					<tr>
						<td height='100' valign='middle' align='center' style='font-size:10px; padding:4px'>--</td>
					</tr>
				</table>";
		}
			
		$desc = "學生可於本欄提供額外資料，重點描述其在高中或以前的學習階段中的學習生活及個人發展方面的情況，
				以便其他人士（例如各大專院校及未來僱主等）參考。</br>
				舉例說，學生可概略地敘述一項印象深刻的學習經歷，如何影響其個人成長及人生目標。
				學生也可於本欄述說其訂定人生目標的故事，如何影響其個人抱負及全人發展。<br/>
				In this column, student may provide additional information to <span style='font-weight:bold'>highlight</span> 
				any aspects of his/her learning life and personal development <span style='font-style:italic'>during</span> 
				or <span style='font-style:italic'>before</span> the senior secondary education for readers' 
				(e.g. tertiary education institutions, future employers references.)<br/>
				For example, the student may take the opportunity to briefly highlight an impressive learning experience 
				that has had an impact on his/her personal growth and life goals. The student may also use this column to tell 
				his/her story about personal goal setting that is influencing his/her career aspirations or whole person development.
				";	

		$title = $slp_report->BiLangOut("['StudentsSelfAccount']",'','','H').'&nbsp;&nbsp;&nbsp;&nbsp;( 可選擇填寫 / Optional )';
		$title .= '<br/><span style="font-size:10px">'.$slp_report->BiLangOut("['SelfDesc2']").'</span>';
		$html = $slp_report->GEN_SLP_MODULE_TABLE($title, "<div style='text-align:justify'><span style='font-size:13px;'>".$desc."</span></div>", '').'<br>'.$html;
		return $html; 
	}
	/********************* End ***********************/
	
	
	/***************** Signature ******************/
	public function getSignature_HTML($PageBreak='')
	{
		//$this->issuedate
		$html = '';
		
		$html .= '<table style="width:100%">
					<tr>
						<td style="text-align:center">
							<table cellpadding="0" cellspacing="0" align="center">
							<tr><td style="border-bottom:1px solid #000; width:240px; text-align:center">&nbsp;</td></tr>
							</table>
						</td>
						<td style="text-align:center">
							<table cellpadding="0" cellspacing="0" align="center">
							<tr><td style="border-bottom:1px solid #000; width:240px; text-align:center">'.$this->issuedate.'</td></tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="text-align:center; height:36px"><font ><span style="font-size:12px">學校蓋章<br />School Chop</span></font></td>
						<td style="text-align:center"><font ><span style="font-size:12px">發出日期<br />Date of Issue</span></font></td>
					</tr>
				  </table>';
		return $html;
		
	}
	
	public function getFooter_HTML($name='',$withSign=false)
	{
		//$this->issuedate
		$html = '';
	
		$html .= '<table style="width:100%">';
		if($withSign){
			$html .= '<tr>
							<td width="100" style="text-align:center">&nbsp;</td>
							<td style="text-align:center; border-bottom:1px solid #000;"></td>
							<td width="100" style="text-align:center">&nbsp;</td>
							<td style="text-align:center; border-bottom:1px solid #000;"></td>
							<td width="100" style="text-align:center">&nbsp;</td>
						</tr>
						<tr>
							<td style="text-align:center"></td>
							<td style="text-align:center; height:36px"><span style="font-size:12px">學校蓋章<br />School Chop</span></td>
							<td style="text-align:center"></td>
							<td style="text-align:center"><span style="font-size:12px">發出日期<br />Date of Issue</span></td>
							<td style="text-align:center"></td>
						</tr>';
		}
		$html .= '<tr>
						<td colspan="5" style="text-align:center"><span style="font-size:12px">'.$name.' / Page {PAGENO} of {nbpg}</span></td>
					</tr>
				  </table>';
		return $html;
	
	}
	/******************* End ******************/
	
	function displayPage($content, $pageNumber, $footer='', $islastStudent=false, $islastpage=false){
		global $intranet_root, $ipf_cfg;
		if($islastStudent){
		}else{ 
			$pageBreak = "page-break-after:always;";
		}
		if($islastpage){
			$pageBreak ="";
		}
		
		
		$objIpfSetting = iportfolio_settings::getInstance();

$ShowSchoolHeaderImage  = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpWithSchoolHeaderImage"]);


		
		$SchoolImgDir = $intranet_root.$ipf_cfg['SLPArr']['SLPFolderPath'];
 
		if($ShowSchoolHeaderImage && is_dir($SchoolImgDir)) {

			$dh = opendir($SchoolImgDir);
		 
			$LogofileName = $ipf_cfg['SLPArr']['SLPSchoolHeaderImageName'];
			$ExistFileName = '';
			 
			while (($file = readdir($dh)) !== false)
			{		
				$pos = strpos($file,$LogofileName);
				
				if($pos === false) {
					 // string needle NOT found in $file
				}
				else {
					$ExistFileName = $file;
				}
			}
			closedir($dh);

			$SchoolImgFile = $SchoolImgDir.$ExistFileName;
			if(file_exists($SchoolImgFile))
			{
				$SchoolImageFile_HTTP = $ipf_cfg['SLPArr']['SLPFolderPath'].$ExistFileName;
				$header_image = "<img height='40mm' src='{$SchoolImageFile_HTTP}' border='0' />";
			}
		}
		
		$ReportHeaderRow = ($header_image!="") ? $header_image : $this->getReportHeader_HTML();
		
		$html = <<<HTML
	<table style="width:100%; {$pageBreak}">
		<tr><td valign="top">{$content}</td></tr>
	</table>
		
HTML;
		
		global $pdf;
		$withSign = false;
		if($footer != ''){
			$withSign = true;
		}
		$footer = $this->getFooter_HTML($this->studentName,$withSign);
		$pdf->SetHeader($ReportHeaderRow);
		$pdf->SetFooter($footer);
		$pdf->writeHTML($html);
		
		if($islastpage && !$islastStudent){
			$pdf->writeHTML('<pagebreak resetpagenum="1"/>');
		}
// debug_pr($html);
		return $html;
	}
	
	function generateReport($_UserID='',$academicYearIDArr='',$IntExt='',$Selected=false,$isMember=false, $islastStudent=false){
		$oleBlockCount = $this->initializeOLE($_UserID,$academicYearIDArr,$IntExt,$Selected,$isMember);
		
		//$this->getSchoolAward_HTML()
		//$this->getSignature_HTML()
		//getOLE_HeaderDisply()
		//getOLE_FooterDisply()
		//getSelfAccount_HTML()
		//$this->oleBlockAry
		
		$this->totalPageNumber = $oleBlockCount+2;
		
		$html = '';
		$pageContent = $this->getStudentInfo_HTML().'<br/ ><br/>'.$this->getOLE_HeaderDisply().'<br/>';
		
		for($i=0;$i<$oleBlockCount;$i++){
			$pageContent .= $this->oleBlockAry[$i];
			if($i+1==$oleBlockCount){
				$pageContent .= $this->getOLE_FooterDisply();
			}
			$html .= $this->displayPage($pageContent, $i+1);
			$pageContent = '';
		}
		$html .= $this->displayPage($this->getSchoolAward_HTML(), ++$i);
		$html .= $this->displayPage($this->getSelfAccount_HTML(), ++$i, $this->getSignature_HTML(), $islastStudent, $islastpage=true);
		return $html;
		
	}
	
	
	/***************** Part 3 : Academic Performance******************/	
	public function getPart3_HTML($OLEInfoArr){
		
		$Part3_Title = 'Membership/Duty 職務';
		$Part3_Remark = '&nbsp;';
		
		$titleArray = array();
		$titleArray[] = 'No. 編號';
		$titleArray[] = 'School Year 學年';
		$titleArray[] = 'Club/Team 學會/團隊';
		$titleArray[] = 'Post/Duty 職務';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="10%" />';
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="30%" />';
		
		$html = '';
		
		$DataArray = $OLEInfoArr[$this->uid];
		
		$html .= $this->getPartContentDisplay_NoRowLimit('3',$Part3_Title,$titleArray,$colWidthArr,$DataArray);
		
		return $html;	
	}		

	/***************** End Part 3 ******************/
	
	/***************** Part 5 : Other Learning Experience******************/	
	public function getPart5_HTML(){
		
		$Part5_Title = 'Scholarship 獎學金';
				
		$titleArray = array();
		$titleArray[] = 'No. 編號';	
		$titleArray[] = 'School Year 學年';
		$titleArray[] = 'Title 項目';
		$titleArray[] = 'Organization 頒發機構';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="10%" />';
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="35%" />';
		$colWidthArr[] = '<col width="30%" />';  
		
		$DataArray = array();
		$DataArray = $this->getAward_Info();  

		$html = '';
	
		if(count($DataArray)>0)
		{
			$html .= $this->getPartContentDisplay_NoRowLimit('5',$Part5_Title,$titleArray,$colWidthArr,$DataArray);
		}
			
		return $html;
		
	}	
	  
	
	/***************** End Part 5 ******************/

	
	
	
	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		
		if(count($returnArray)==1)
		{
			return $returnArray[0];
		}
		else
		{
			$lastNo = count($returnArray)-1;
			
			$firstYear = substr($returnArray[0], 0, 4);
			$lastYear = substr($returnArray[$lastNo],-4);
			
			$returnValue = $firstYear."-".$lastYear;
			
			return $returnValue;	
		}
		
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center"><font >'.$Content.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>