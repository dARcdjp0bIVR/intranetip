<?php
class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $academcYearDisplay; // store the academic Year display in the student Info
	private $PrincipalName;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $ClassGroupCategoryName;	// store the Category Name for the Group of Small Classes (1A1, 1A2)
	private $FontSizeArr;		// store the font size info for different field
	private $OrderInfoArr;		// store ordering fields of different section
	
	private $admissionNumber;
	
	private $PageNo;
	private $PhotoLink;

	public function studentInfo($uid,$academicYearIDArr){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid;
		$this->academicYearIDArr = $academicYearIDArr;
		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '/';
		$this->TableTitleBgColor = '#E8E8E8';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->admissionNumber='';
		
		$this->OrderInfoArr['GroupCategory'] = array();
		$this->OrderInfoArr['GroupCategory'][] = 'SERVICE';
		$this->OrderInfoArr['GroupCategory'][] = 'HOUSE';
		$this->OrderInfoArr['GroupCategory'][] = 'TEAM';
		$this->OrderInfoArr['GroupCategory'][] = 'CLUB';
		$this->OrderInfoArr['GroupCategory'][] = 'OTHERS';
		
		//getCurrentAcademicYear
		
		$this->OrderInfoArr['ELECode'] = array();
		$this->OrderInfoArr['ELECode'][] = '[AD]';		// Aesthetic Development
		$this->OrderInfoArr['ELECode'][] = '[CE]';		// Career - related Experiences
		$this->OrderInfoArr['ELECode'][] = '[CS]';		// Community Service
		$this->OrderInfoArr['ELECode'][] = '[MCE]';		// Moral and Civic Education
		$this->OrderInfoArr['ELECode'][] = '[PD]';		// Physical Development
		$this->OrderInfoArr['ELECode'][] = '[8]';		// Others (Cultural Experience)
		$this->OrderInfoArr['ELECode'][] = '[9]';		// Others (Intellectual Development)
	}
	
	public function setPageNumber($PageNumber){
		$this->PageNo = $PageNumber;
	}
	
	public function getPageNumber(){
		return $this->PageNo;
	}
	
	public function setPhotoLink($PhotoLink){
		$this->PhotoLink = $PhotoLink;
	}
	
	public function getPhotoLink(){
		return $this->PhotoLink;
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearDisplay($academcYearDisplay){
		$this->academcYearDisplay = $academcYearDisplay;
	}
	public function getAcademicYearDisplay(){
		return $this->academcYearDisplay;
	}
	public function setPrincipalName($Name) {
		$this->PrincipalName = $Name;
	}
	public function getPrincipalName(){
		return $this->PrincipalName;
	}
	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}
	
	
	/***************** Part 1 ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '25';
		
		//$imgFile = get_website().'/file/reportcard2008/templates/wesley_methodist_ma.png';
		
		$imgFile = get_website().'/includes/portfolio25/customize/kc/Kings_SchoolLogo.png';
		$schoolLogoImage = ($imgFile != '') ? '<img src="'.$imgFile.'" style="width:15%;">' : '&nbsp;';
		
		$header_title1 = "KING'S COLLEGE";
		$header_title2 = "STUDENT LEARNING PROFILE";
		
		$EmptySpace_html = '<tr><td>'.'&nbsp;'.'</td></tr>'."\r\n";
		
		$x = '';
		$x .= '<table align="center" style="width:90%; text-align:center; border:0px;">'."\r\n";
			$x .= '<tr><td>'.$schoolLogoImage.'</td></tr>'."\r\n";
			$x .= $EmptySpace_html;
			$x .= '<tr><td style="font-size:'.$header_font_size.'"><font face="Arial"><b>'.$header_title1.'</b></font></td></tr>'."\r\n";
			$x .= $EmptySpace_html;
			$x .= '<tr><td style="font-size:'.$header_font_size.'"><font face="Arial"><b>'.$header_title2.'</b></font></td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select  
						EnglishName,
						HKID,
						DateOfBirth,
						Gender,
						STRN,
						WebSAMSRegNo,
						UserLogin,
						PhotoLink
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
						And RecordType = '2'
				";
//hdebug_r($sql);	
		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result){
			
//		debug_pr($result);

		$fontSize = 'font-size:13';
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		$_borderBottom =  'border-bottom: 1px solid black; ';

		$EmptySymbol = '--';
		
	//	$DateOfAdmis = date('j F Y');
		$DateOfAdmisArr = $this->getDateOfAdmisArr();
		
		if($DateOfAdmisArr!='')
		{
			$DateOfAdmisDate = $DateOfAdmisArr['AdmissionDate'];
			if (date($DateOfAdmisDate)==0) {
				$DateOfAdmisDate = $EmptySymbol;
			} else {
				$DateOfAdmisDate = date('j F Y',strtotime($DateOfAdmisDate));
			} 
			$DateOfAdmisClassName = 'S'.(int)$DateOfAdmisArr['ClassName'];
		}
//hdebug_r($DateOfAdmisClassName);		
		if($DateOfAdmisDate=='' || $DateOfAdmisClassName=='')
		{
			$DateOfAdmis = $EmptySymbol;
		}
		else
		{
			$DateOfAdmis =$DateOfAdmisDate.','.$DateOfAdmisClassName;
		}
				
			
		$DOB = $result[0]["DateOfBirth"];

		if (date($DOB)==0) {
			$DOB = $EmptySymbol;
		} else {
			$DOB = date('j F Y',strtotime($DOB));
		} 
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $EmptySymbol:$EngName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		if($Gender=='M')
		{
			$Gender='Male';
		}
		else if($Gender=='F')
		{
			$Gender='Female';
		}
		$Gender = ($Gender=='')? $EmptySymbol:$Gender;
		
		
		$UserLogin = $result[0]["UserLogin"];
		$studentPhotoLink = $result[0]["PhotoLink"];
		$photo_link = '/file/user_photo/'.$UserLogin;
		$this->setPhotoLink($studentPhotoLink);
		
		
		$RegistrationNo = $result[0]["WebSAMSRegNo"];
		$RegistrationNo = str_replace('#','',$RegistrationNo);
		
		$this->admissionNumber = $RegistrationNo;
		$RegistrationNo = ($RegistrationNo=='')? $EmptySymbol:$RegistrationNo;

		$DateOfLeaveArr = $this->getStudentAcademic_YearClassNameArr('max');
		
		if($DateOfLeaveArr!='')
		{
		//	$DateOfLeaveYear = $DateOfLeaveArr['AcademicYear'];
			$DateOfLeaveClassName = (int)$DateOfLeaveArr['ClassName'];
		}
		
		$DateOfLeaveYear = getCurrentAcademicYear('en');
		$DateOfLeaveYear = substr($DateOfLeaveYear,-4,4);
		
		$DateOfLeave = '30 June '.$DateOfLeaveYear.', S'.$DateOfLeaveClassName;

		$html = '';
		$html .= '<table width="90%" cellpadding="7" border="0px" align="center" style="border-collapse: collapse;border:1px solid #000000;">
					<col width="60%" />
					<col width="40%" />
					<tr>
							<td bgcolor="'.$this->TableTitleBgColor.'" colspan="2" style="'.$_borderBottom.'"><font face="Arial"><b '.$_paddingLeft.'>Student Particulars</b></font></td>
					</tr>
					<tr>
							'.$td_space.'
							'.$td_space.'
					</tr>
					<tr>
							<td><font face="Arial" style="'.$_paddingLeft.$fontSize.'">Student Name: '.$EngName.'</font></td>
							<td><font face="Arial" style="'.$fontSize.'">Registration No.: '.$RegistrationNo.'</font></td>
					</tr>
					<tr>
							<td><font face="Arial" style="'.$_paddingLeft.$fontSize.'">Date of Birth: '.$DOB.'</font></td>
							<td><font face="Arial" style="'.$fontSize.'">Sex: '.$Gender.'</font></td>
					</tr>
					<tr>
							<td><font face="Arial" style="'.$_paddingLeft.$fontSize.'">Date & Level of Admission: '.$DateOfAdmis.'</font></td>
							<td><font face="Arial" style="'.$fontSize.'">&nbsp;</font></td>
					</tr>
					<tr>
							<td><font face="Arial" style="'.$_paddingLeft.$fontSize.'">Date & Level of Leaving: '.$DateOfLeave.'</font></td>
							'.$td_space.'
					</tr>
					<tr>
							'.$td_space.'
							'.$td_space.'
					</tr>';
		$html .= "</table>";
		
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	
	/***************** Part 3 ******************/	
	public function getPart3_HTML(){
		
		$_paddingLeft = ' style="padding-left:30px;"';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="7" border="0px" align="center"  >
					<col width="5%" />
					<col width="95%" />

					<tr>
							<td colspan="2" '.$_paddingLeft.'><font face="Arial">This Student Learning Profile comprises four parts:</font></td>
					</tr>

					<tr>
							<td><font face="Arial" '.$_paddingLeft.'>I.</font></td>
							<td><font face="Arial" >Other Learning Experiences</font></td>
					</tr>
					<tr>
							<td><font face="Arial" '.$_paddingLeft.'>II.</font></td>
							<td><font face="Arial" >List of Awards and Major Achievements Issued by the School</font></td>
					</tr>
					<tr>
							<td><font face="Arial" '.$_paddingLeft.'>III.</font></td>
							<td><font face="Arial" >Performance/Awards and Key Participation Outside School</font></td>
					</tr>
					<tr>
							<td><font face="Arial" '.$_paddingLeft.'>IV.</font></td>
							<td><font face="Arial" >My Self-Account</font></td>
					</tr>';

		$html .= "</table>";
		
		return $html;
	}
	
	
	/***************** End Part 3 ******************/
	
	
	/***************** Part 4 ******************/	
	public function getPart4_HTML($OLEInfoArr){
		
		$Part4_Title = 'I. Other Learning Experiences';
		
		$titleArray = array();
		$titleArray[] = 'Key Participation /<br/>Programmes';
		$titleArray[] = 'School<br/>Year';
		$titleArray[] = 'Role of<br/>Participation';
		$titleArray[] = 'Partner<br/>Organisations<br/>(if any)';
		$titleArray[] = 'Certifications /<br/>Awards /<br/>Achievements<br/>(if any)';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="1%" />';
		$colWidthArr[] = '<col width="29%" />';
		$colWidthArr[] = '<col width="10%" />';
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="25%" />';
		
		$DataArray = array();
		$DataArray = $OLEInfoArr[$this->uid]['INT'];
	
		
		$html = '';
// 		$html .= $this->getPartTitleDisplay($Part4_Title);
		$mainTitle = $this->getPartTitleDisplay($Part4_Title);
		$html .= $this->getPartContentDisplay_PageRowLimit('4',$titleArray,$colWidthArr,$DataArray,$hasPageBreak=true, '', $mainTitle);
		
		return $html;
		
	}	
	/***************** End Part 4 ******************/
	
	
	/***************** Part 5 ******************/	
	public function getPart5_HTML(){
		
		$Part5_Title = 'II. List of Awards and Major Achievements issued by the School';
		
		$titleArray = array();
		$titleArray[] = 'Year';
		$titleArray[] = 'Awards and Achievements';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="20%" />';
		$colWidthArr[] = '<col width="80%" />';
		
		$DataArr = array();
		$DataArr = $this->getAward_Info();
		
		$html = '';
// 		$html .= $this->getPartTitleDisplay($Part5_Title);
		$mainTitle = $this->getPartTitleDisplay($Part5_Title);
		$html .= $this->getPartContentDisplay_NoRowLimit('5',$titleArray,$colWidthArr,$DataArr,$hasPageBreak=false, $mainTitle);
		
		return $html;
		
	}
	
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		$Academic_year = $this->Get_IntranetDB_Table_Name('ACADEMIC_YEAR');
		
		$_UserID = $this->uid;
		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$cond_academicYearID = "And s.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME',
						s.AwardName
				From
						$Award_student as s
				inner join 
						$Academic_year as y 
				on
						y.AcademicYearID = s.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc, 
						s.AwardName asc		

				";
				
				
			$roleData = $this->objDB->returnResultSet($sql,2);


			return $roleData;
	}
	
	/***************** End Part 5 ******************/
	
	
	/***************** Part 6 ******************/
	public function getPart6_HTML($OLEInfoArr){
		
		$Part6_Title = 'III. Performance / Awards and Key Participation Outside School';
		
		$titleArray = array();
		$titleArray[] = 'Programmes / Awards and Key<br/>Participation';
		$titleArray[] = 'School<br/>Year';
		$titleArray[] = 'Role of<br/>Participation';
		$titleArray[] = 'Organisation';
		$titleArray[] = 'Awards /<br/>Certifications /<br/>Achievements* (if any)';
		
		$colWidthArr = array();
		$colWidthArr[] = '<col width="1%" />';
		$colWidthArr[] = '<col width="29%" />';
		$colWidthArr[] = '<col width="10%" />';
		$colWidthArr[] = '<col width="15%" />';
		$colWidthArr[] = '<col width="25%" />';
		$colWidthArr[] = '<col width="25%" />';
		
		$Remarks = '*Evidence of awards/certifications/achievements listed will be provided by student whenever requested.';
		
		$DataArray = array();
		$DataArray = $OLEInfoArr[$this->uid]['EXT'];
		
		$html = '';
// 		$html .= $this->getPartTitleDisplay($Part6_Title);
		$mainTitle = $this->getPartTitleDisplay($Part6_Title);
		$html .= $this->getPartContentDisplay_PageRowLimit('6',$titleArray,$colWidthArr,$DataArray,$hasPageBreak=false,$Remarks, $mainTitle) ;

		return $html;
		
	}
	

	
	/***************** End Part 6 ******************/
	
	
	
	/***************** Part 7 ******************/
	
	public function getPart7_HTML(){
		
		$Part7_Title = 'IV. My Self-Account';
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();
		
//		$html = '';
		
		
//		$html .= $this->getPartTitleDisplay($Part7_Title);
//		$html .= $this->getPartContentDisplay_NoRowLimit('7','','',$DataArr,$hasPageBreak=false) ;
//		$html .='</td></tr>';
//		$html .='</table>';	
		return $this->getPartContentDisplay_NoRowLimit('7','','',$DataArr,$hasPageBreak=false);
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$_UserID = $this->uid;
//debug_r($_UserID);		
		if($_UserID!='')
		{
			$cond_studentID = "And UserID ='$_UserID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
			$roleData = $this->objDB->returnResultSet($sql);
			
			for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
			{			
				$thisDataArr = array();
				
				if($_UserID=='35')  // 6B - 12
				{
					$thisDataArr['Details'] = $roleData[$i]['Details'];
				}
				else
				{
					$thisDataArr['Details'] =strip_tags($roleData[$i]['Details'],"<p><br>");
					$thisDataArr['Details'] = str_replace('<p class="MsoNormal">&nbsp;</p>','',$thisDataArr['Details']);
				}
				
				
				$returnDate[] = $thisDataArr;			
			}
	
			return $returnDate;
	}
	
	/***************** End Part 7 ******************/
	
	
	/***************** Part 8 ******************/
	public function getPart8_HTML(){
		
		global $PATH_WRT_ROOT,$intranet_root;
		
		$Part8_Title = "Part I 'Other Learning Experiences' and Part II 'List of Awards and Major Achievements issued by the School' are certified correct.";
		
		$thisPhotoLink = $this->getPhotoLink();

		// #S116225 hide student photo
		//$schoolChopImage = ($thisPhotoLink != '') ? '<img src="'.$thisPhotoLink.'" style="width:110px;">' : '&nbsp;';
	
		$font_style = 'style="text-align:center;"';
		$padding_right='padding-right:70px;';
		$OneSpace = '&nbsp;';
		$html = '';
		$html .=$this->getPart8TitleDisplay($Part8_Title);
		$DateOfLeaveYear = getCurrentAcademicYear('en');
		$DateOfLeaveYear = substr($DateOfLeaveYear,-4,4);
		$html .= '<table width="100%" cellpadding="0" border="0px" align="center" style="">		
					<col width="50%" />
					<col width="50%" />

				 	<tr>
					    <td rowspan="7" style="padding-left:70px;">'.$schoolChopImage.'</td>
						<td>'.$OneSpace.'</td>
					</tr>
					<tr>
						<td>'.$OneSpace.'</td>
					</tr>
					<tr>
						<td>'.$OneSpace.'</td>
					</tr>
					<tr>
						<td '.$font_style.' style="'.$padding_right.'"><font face="Arial">______________________________</font></td>
					</tr>
					<tr>
						<td '.$font_style.' style="'.$padding_right.'"><font face="Arial">(TANG Kai-chak)</font></td>
					</tr>
					<tr>
						<td '.$font_style.' style="'.$padding_right.'"><font face="Arial">Principal</font></td>
					</tr>
					<tr>
						<td '.$font_style.' style="'.$padding_right.'"><font face="Arial">30 June '.$DateOfLeaveYear.'</font></td>
					 </tr>

				  </table>';
		
		return $html;
		
	}

	
	/***************** End Part 8 ******************/
	
	/****************** Footer *********************/
	public function getFooter_HTML($ParPageNo='',$ParTotalPage=''){
		
		$thisAddress = 'ADDRESS: 63A, BONHAM ROAD, HONG KONG<br/>SCHOOL CODE: 510408<br/>TEL: 2547 0310';
		$thisKCSLP_str = 'KC_SLP_'.$this->admissionNumber;
		
				
		$html = '';
		$html .= '<table width="90%" cellpadding="7" border="0px" align="center" style="font-size:10px">					
		   			 <tr>
						<td><font face="Arial">'.$thisAddress.'</font></td>
					    <td style="text-align:right"><font face="Arial">'.$thisKCSLP_str.'&nbsp;&nbsp;&nbsp;&nbsp;'.$ParPageNo.' out of <!--totalPageNo--></font></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	/****************** End Footer *********************/
	
	public function getPartTitleDisplay($PartTitle='') {
		
		$html = '';
		$html .= '<table width="90%" cellpadding="7" border="0px" align="center" style="">					
		   			 <tr>
					    <td><font face="Arial"><b>'.$PartTitle.'</b></font></td>
					 </tr>
				  </table>';
		return $html;
	}
	private function getPart8TitleDisplay($ParTitle='') {
		
		$html = '';
		$html .= '<table width="100%" cellpadding="" border="0px" align="center">					
		   			 <tr>
					    <td><font face="Arial"><b>'.$ParTitle.'</b></font></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	private function getPartContentDisplay_NoRowLimit($PartNum, $subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false, $MainTitle = '') 
	{
				
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		$BgColor = $this->TableTitleBgColor;
		
		$ParPageNo = $this->PageNo;
		
		$rowMax_count = count($DataArr);
		$tableHeight_html='';
		
		if($PartNum=='5')
		{
			$text_alignStr='left';
		}
		
		$Page_break='page-break-after:always;';
		
		if($hasPageBreak==false)
		{
			$Page_break='';
		}
		
		
		if($PartNum=='7')
		{
			$characterCount = strlen($DataArr[0]['Details']);
			$firstPageLimit = 2590;
			if($characterCount > $firstPageLimit){
				$newLineCount = substr_count($DataArr[0]['Details'], '\n', 0, $firstPageLimit);
				$firstPageLimit -= $newLineCount * 70;
				$printData = array(substr($DataArr[0]['Details'], 0, $firstPageLimit), substr($DataArr[0]['Details'], $firstPageLimit));
			} else {
				$printData = array($DataArr[0]['Details']);
			}
			$table_style = 'style="border-collapse: collapse;font-size:14px;'.$Page_break.'"'; 
			$_borderRight='';
			$BgColor='';
// 			$tableHeight_html='1940px';
			if($rowMax_count<=0)
			{
// 				$tableHeight_html='945px';
			}
			else
			{
// 				$ParPageNo++;
			}
		
		}
		else
		{
			$table_style = 'style="border-collapse: collapse;font-size:13px;border:1px solid #000000;'.$Page_break.'"'; 
		}
			
		$html = $MainTitle;
		if($PartNum=='7'){
			$html .= '<div style="height:930px;">';
			$html .= count($printData) == 1 ? '<table width="100%">' : '<table width="100%" height="840px">';
		} else {
			$html .= '<table width="100%" height="940px">';
		}
		$html .= '<tr><td style="vertical-align:top">';
		$html .= '<table width="90%" height="'.$tableHeight_html.'" cellpadding="7" border="0px" align="center" '.$table_style.'>';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$html .=$_colWidthInfo;
					}
					
		// # S108828 
		if(!empty($subTitleArr)){
			$html .= '<tr>';
						foreach((array)$subTitleArr as $key=>$_title)
						{
							$html .='<td bgcolor="'.$BgColor.'" style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderRight.'"><b><font face="Arial">';
							$html .=$_title;
							$html .='</font></b></td>';
						}
			$html .= '</tr>';
		}
		
		
	
		if($PartNum=='7')
		{
			$Part7_borderBottom = count($printData) == 1 ? $_borderBottom : '';
			//$Part7_WordHeight = 'line-height: 1.7;"';
			$Part7_WordHeight = 'line-height: 1.7;';
		}
	
		if($rowMax_count<=0)
		{			
			$count_col =count($colWidthArr);
			
			$html .= '<tr >';
			$html .=' <td colspan="'.$count_col.'" style="'.$_borderTop.$Part7_borderBottom.'text-align:center;vertical-align: top;"><b><font face="Arial">No Record Found.</font></b></td>';	
			$html .= '</tr>';

		}
		else if ($rowMax_count>0)
		{
			for ($i=0; $i<$rowMax_count; $i++)
			{		
				$html .= '<tr>';				 
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{
					if($_titleVal=='')
					{
						$_titleVal =  $this->EmptySymbol;
						$html .= ' <td style="text-align:center;vertical-align: top;'.$_borderTop.$_borderRight.$Part7_borderBottom.'"><font face="Arial">'.$_titleVal.'</font></td>';					
					}
					else
					{					 
						if($PartNum=='7') {
							$tdClass = 'selfAccountTd';
							foreach($printData as $pd){
								$html .= ' <td class="'.$tdClass.'" style="vertical-align: top;text-align:justify;'.$_borderTop.$_borderRight.$Part7_borderBottom.$Part7_WordHeight.'"><font face="Arial">'.$pd.'</font></td>';
								$Part7_borderBottom = $_borderBottom;
								$html.='</tr>';
								if($pd != end($printData)){
									$html .= "</table></td></tr></table></div>";
									$html .= $this-> getFooter_HTML($ParPageNo,$ParTotalPage='');
									$html .= $this->getPagebreakTable();
									$ParPageNo++;
									$this->setPageNumber($ParPageNo);
									$html .= "<div style='height:940px;' ><table width='100%' ><tr><td style='vertical-align:top'>";
									$html .= '<table width="90%"  cellpadding="7" border="0px" align="center" '.$table_style.'>';
								} else {
									$html .= '</td><tr>';
								}
							}
						} else {
							$html .= ' <td class="'.$tdClass.'" style="vertical-align: top;text-align:justify;'.$_borderTop.$_borderRight.$Part7_borderBottom.$Part7_WordHeight.'"><font face="Arial">'.$_titleVal.'</font></td>';		
						}
					}				
				}	
				$html .= '</tr>';
			}
		}
		if($PartNum=='7')
		{
			$html .= '<tr height="100px"><td>';
			$html .= $this->getPart8_HTML();
// 			$html .= $this-> getFooter_HTML($ParPageNo,$ParTotalPage='');
			$html .= '</td></tr>';
			
		}	

		$html .='</table>';
		$html .= '</td></tr>';
		
		$html .= '</table>';
		
		if($PartNum=='7'){
			$html .='</div>';
		}
		if($PartNum!='7') {
			$html .= $this-> getFooter_HTML($ParPageNo,$ParTotalPage='');
		} else {
			$html .= '<div style="margin-bottom:5px">' . $this-> getFooter_HTML($ParPageNo,$ParTotalPage='') . '</div>';
		}
		
		$ParPageNo++;
		$this->setPageNumber($ParPageNo);
		
		return $html;
	}	
	
	private function getPartContentDisplay_PageRowLimit($PartNum, $subTitleArr='',$colWidthArr='',$DataArr='',$hasPageBreak=false,$Remark='', $MainTitle = '')
	{
		$RowPerPage = 10; // it indicate the number of rows perpage. If you want to set 2 row perpage, change it to 2;
		
		$rowMax_count = count($DataArr);
		
		$RowHeight = 'height="75px"';
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		
		$colSpanStr = '';

		$ParPageNo = $this->PageNo;

		$text_alignStr ='center;';		
		
		if($PartNum=='5')
		{
			$text_alignStr='left;';
		}
		
		$NumberOfCol = count($colWidthArr);
		
		$html = '';
		$HeaderInfo = $MainTitle;
		$HeaderInfo .= '<table width="100%" height="940px">';
		$HeaderInfo .= '<tr><td style="vertical-align:top">';
		$HeaderInfo .= '<table width="90%" height="" cellpadding="7" border="0px" align="center" style="border-collapse: collapse; font-size:13px;">';
					foreach((array)$colWidthArr as $key=>$_colWidthInfo)
					{
						$HeaderInfo .=$_colWidthInfo;
					}
					
		$HeaderInfo .= '<tr '.$RowHeight.'>';
					$k=0;
					foreach((array)$subTitleArr as $key=>$_title)
					{
						if($k==0)
						{
							$colSpanStr = 'colspan="2"';
						}
						else
						{
							$colSpanStr = '';
						}
						$HeaderInfo .='<td '.$colSpanStr.' bgcolor="'.$this->TableTitleBgColor.'" style="text-align:'.$text_alignStr.';vertical-align:text-top;'.$_borderRight.$_borderTop.$_borderLeft.'"><b><font face="Arial">';
						$HeaderInfo .=$_title;
						$HeaderInfo .='</font></b></td>';
						$k++;
					}					
		$HeaderInfo .= '</tr>';

		$tableBottom_html ='</table>';
		$tableBottom_html .= '</td></tr>';
		$tableBottom_html .= '</table>';
		$tableBottom_html .= $this->getFooter_HTML($ParPageNo,$ParTotalPage='');	
		if($hasPageBreak==true)
		{
			$tableBottom_html .=$this->getPagebreakTable();
		}
		
		
		
		if($rowMax_count<=0)
		{
			$count_col =count($colWidthArr);
			
			$html_content =' <tr><td colspan="'.$count_col.'" style="'.$_borderLeft.$_borderTop.$_borderBottom.$_borderRight.'text-align:'.$text_alignStr.'"><b><font face="Arial">No Record Found.</font></b></td></tr>';	
			
			$ParPageNo++;
			
			$html .=$HeaderInfo;
			$html .=$html_content;
			$html .=$tableBottom_html;
		}

		else if($rowMax_count>0)
		{ 
			### 10 rows in one page	
			$NumOfDisplayedRow = 0;
			$SetHeader=false;
			
			
			for ($i=0; $i<$rowMax_count; $i++)
			{		
				
				$tableBottom_close_html='';
				$tableBottom_close_html .= '<tr><td colspan="'.$NumberOfCol.'" style="vertical-align:top;text-align:left;">';
				$tableBottom_close_html .= '<font face="Arial">'.$Remark.'</font>';
				$tableBottom_close_html .= '</td></tr>';
				$tableBottom_close_html .='</table>';
				$tableBottom_close_html .= '</td></tr>';
				
				$tableBottom_close_html .= '</table>';
				$tableBottom_close_html .= $this->getFooter_HTML($ParPageNo,$ParTotalPage='');	
				$tableBottom_close_html .= ($hasPageBreak==true)? $this->getPagebreakTable():'';
				
				
				
				$NumOfDisplayedRow = $i;
				
				$html_content = '';
				$html_content .= '<tr '.$RowHeight.'>';
				
				$thisNum = $i+1;	
				$html_content .=' <td style="vertical-align: top;'.$_borderTop.$_borderBottom.$_borderLeft.'"><b><font face="Arial">'.$thisNum.'.</font></b></td>';	
				
				$k=0;		 
				foreach((array)$DataArr[$i] as $_title=>$_titleVal)
				{				
					if($_titleVal=='')
					{
						$_titleVal =  $this->EmptySymbol;
						$html_content .= ' <td style="text-align:center;vertical-align:top;'.$_borderTop.$_borderBottom.$_borderRight.'"><font face="Arial">'.$_titleVal.'</font></td>';						
					}
					else
					{
						if($k==0)
						{
							$html_content .= ' <td style="vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'"><b><font face="Arial">'.$_titleVal.'</font></b></td>';
						}
						else
						{
							$html_content .= ' <td style="vertical-align: top;'.$_borderTop.$_borderRight.$_borderBottom.'"><font face="Arial">'.$_titleVal.'</font></td>';
						}						
					}										
											 	
					$k++;
				}	
				$html_content .= '</tr>';	

				 if($NumOfDisplayedRow==0)  // if it is first row, add the header
				{
					$header_html=$HeaderInfo;
					
					if($RowPerPage<=1)  // if $RowPerPage is equal or small than one 
					{
						$bottom_html = $tableBottom_html;
					}
					else if($rowMax_count<=1)
					{
						$bottom_html =  $tableBottom_close_html;
					}
					
				}
				else
				{
					$header_html='';						
						
					if(($NumOfDisplayedRow+1)%$RowPerPage==0)   // if it is  10th or 20th or 30th ....row (last row), add </table> 
					{
						$bottom_html = $tableBottom_html;
						$SetHeader=true;
					}
					else if($SetHeader==true) // if it is 11th or 21th or 31th....row (first row), add the header
					{
						//S89563
						//$header_html=$HeaderInfo;
						$header_html = '';
						if($PartNum=='6') {
// 							$header_html .= $this->getEmptyTable('69px');
							$header_html .= $this->getEmptyTable('3%');
						}
						$header_html .= $HeaderInfo;
						$SetHeader=false;
						
					}
					
					if($RowPerPage<=1)  // if $RowPerPage is equal or small than one 
					{
						$header_html=$HeaderInfo;
						$bottom_html = $tableBottom_html;			
					}
				}
				
				if($NumOfDisplayedRow >= $rowMax_count-1)  // if it is the last row , add </table> 
				{
					$bottom_html = $tableBottom_close_html;
					
				}
				
				if($NumOfDisplayedRow!=0 && $header_html!='') // if starting a new page, insert an empty table
				{
// 					$html .= $this->getEmptyTable('6%');
					$html .= ($MainTitle!='') ? $this->getEmptyTable('3%') : $this->getEmptyTable('6%');
				}
				$html .= $header_html; 		
				$html .= $html_content; // it is the subject content			
				$html .= $bottom_html;
				
				if($bottom_html!='')
				{
					$ParPageNo++;
				}
		
				$header_html='';
				$bottom_html='';

			}
		}
		
		$this->setPageNumber($ParPageNo);
		
		return $html;
	}
	
	private function getStudentAcademic_YearClassNameArr($ParMaxMin)
	{
		$StudentID = $this->uid;
		
		if($ParMaxMin=='max')
		{
			$SelectStr= 'h.AcademicYear as AcademicYear,
						max(i.ClassName) as ClassName';
		}
		else if($ParMaxMin=='min')
		{
			$SelectStr='h.AcademicYear as AcademicYear,
						min(h.ClassName) as ClassName';
		}
		
		$Profile_his = $this->Get_IntranetDB_Table_Name('PROFILE_CLASS_HISTORY');
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql = 'select 
						'.$SelectStr.' 
				from 
						'.$Profile_his.' as h 
				inner join 
						'.$intranetUserTable.' as i 
				on 
						h.UserID = i.UserID 
				where 
						h.UserID = "'.$StudentID.'"';
						
		$roleData = $this->objDB->returnResultSet($sql);
//hdebug_r($sql);
		$rowCount = count($roleData);
		if($rowCount>0)
		{
			$thisDataArray = current($roleData);
		}	
//hdebug_r($thisDataArray);			
		return $thisDataArray;
	}
	
	private function getDateOfAdmisArr()
	{
		$returnData = array();
		$returnData['AcademicYear']='';
		
		$StudentID = $this->uid;
		$Profile_std = $this->Get_eClassDB_Table_Name('PORTFOLIO_STUDENT'); 
		$intranetUserTable=$this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$roleData = $this->getStudentAcademic_YearClassNameArr('min');

		$rowCount = count($roleData);
		if($rowCount>0)
		{
			$returnData['ClassName'] = $roleData['ClassName'];
		}	
//hdebug_r($roleData['ClassName']);		
		
		$sql = 'select 
						AdmissionDate 
				from 
						'.$Profile_std.' as h 
				inner join 
						'.$intranetUserTable.' as i 
				on 
						h.UserID = i.UserID 
				where 
						h.UserID = "'.$StudentID.'"
				';
				
		$roleData = $this->objDB->returnResultSet($sql);
//hdebug_r($sql);
		$rowCount = count($roleData);
		if($rowCount>0)
		{
			$thisDataArray = current($roleData);
			$returnData['AdmissionDate'] = $thisDataArray['AdmissionDate'];
		}	
				
		return $returnData;
	}
	
	public function getOLE_Info($_UserID='',$academicYearIDArr='')
	{	
		global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 
		
		if($_UserID!='')
		{
			$cond_studentID = " And UserID ='$_UserID'";
		}

		$cond_SLPOrder = " And SLPOrder is not null"; 
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = "And ole_prog.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}
		
		$approve =$ipf_cfg['OLE_STUDENT_RecordStatus']['approved'];
		$teacherSubmit = $ipf_cfg['OLE_STUDENT_RecordStatus']['teacherSubmit'];
		
		$cond_Approve = " And (ole_std.RecordStatus = '$approve' or ole_std.RecordStatus = '$teacherSubmit')";
	
		
		$sql = "Select
						ole_std.UserID as StudentID,
						ole_prog.Title as Title,
						ole_prog.IntExt as IntExt,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' ,
						ole_std.role as Role,
						ole_prog.Organization as Organization,
						ole_std.Achievement as Achievement
				From
						$OLE_STD as ole_std	
				Inner join
						$OLE_PROG as ole_prog
				On
					ole_std.ProgramID = ole_prog.ProgramID
					inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = ole_prog.AcademicYearID
									
				Where
						1	
						$cond_studentID
						$cond_academicYearID
						$cond_SLPOrder	
						$cond_Approve
				order by 
						ole_prog.IntExt,
						y.Sequence asc,	
						ole_std.SLPOrder asc		

				";
			$roleData = $this->objDB->returnArray($sql,2);

			for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
			{
				$_StudentID = $roleData[$i]['StudentID'];
				$_IntExt = $roleData[$i]['IntExt'];
				$thisAcademicYear =  $roleData[$i]['YEAR_NAME'];
				
				$thisDataArr = array();
				$thisDataArr['Title'] =$roleData[$i]['Title'];
				$thisDataArr['YEAR_NAME'] =$thisAcademicYear;
				$thisDataArr['Role'] =$roleData[$i]['Role'];
				$thisDataArr['Organization'] =$roleData[$i]['Organization'];
				$thisDataArr['Achievement'] =$roleData[$i]['Achievement'];
				
				$returnDate[$_StudentID][$_IntExt][] = $thisDataArr;
				
			}
//hdebug_r($sql);	
			return $returnDate;
	}
	
	public function getEmptyTable($Height='') {
		
		$html = '';
		$html .= '<table width="90%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td>&nbsp;</td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	private function getEmptySymbol() {
		return $this->EmptySymbol;
	}
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
}
?>