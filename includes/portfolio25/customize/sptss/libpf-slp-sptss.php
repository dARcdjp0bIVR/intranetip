<?php

function EmptyContentAsEmptyString($Content=''){
	
	$EmptyString = '- -';
	
	if($Content ==''){
		$Content = $EmptyString;
	}
	else{
		$Content = $Content;
	}
	
	return $Content;
}

class objPrinting{
//-- Start of Class

//	var $EmptyString = '---';
//	var $AcademicYearName;
	
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	private $cfgValue;
	
	// variable for retrieving all data
	private $task;
	private $yearTermID;
	private $academicYearID;
	private $academicYearName;
	private $studentList;
	private $studentId;
 
	// variable for storing Student Records (associate array of studentID)
	private $Student_info_Ary;
	private $Student_Demerit_Ary;
	private $PC_Header_Ary;
	private $PC_Info_Ary;
	private $student_Merit_Arr;
	
	// for 3 years report
	private $reportType;
	private $Student_Years_Ary;
	private $Student_comments_Ary;
//	private $AcademicYear;
//	private $studentListAry;
//	private $Student_OLE_Ary;
//	private $Student_ClassName;
//	private $yearClassID;
	
	public function __construct($task,$studentList,$academicYearID='',$yearTermID='',$reportType=''){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		// only get current academic year id data
		switch($task){
			case 'sptss_1':
			case 'sptss_2':
			case 'sptss_3':
			$academicYearID[0] = Get_Current_Academic_Year_ID();
			break;
		}
		if($task=='sptss_1'){
			$termArr = getAllSemesterByYearID($academicYearID[0]);
			$yearTermID = $termArr[0]['YearTermID'];
		}
		else if($task=='sptss_2'){
			$termArr = getAllSemesterByYearID($academicYearID[0]);
			$yearTermID = $termArr[1]['YearTermID'];
		}

//		$this->objDB = new libdb();
//		$this->eclass_db = $eclass_db;
//		$this->intranet_db = $intranet_db;
//		$this->cfgValue = $ipf_cfg;
		$this->setObjVariable('objDB',new libdb());
		$this->setObjVariable('eclass_db',$eclass_db);
		$this->setObjVariable('intranet_db',$intranet_db);
		$this->setObjVariable('ipf_cfg',$ipf_cfg);

		$this->setObjVariable('task',$task);		
		$this->setObjVariable('studentList',$studentList);
		$this->setObjVariable('yearTermID',$yearTermID);
		$this->setObjVariable('reportType',$reportType);
		if($reportType==''){
			$this->setObjVariable('academicYearID',$academicYearID);
		}
		else if($reportType != ''){
			// academicYearID set in  getStudentListYears
			$this->setObjVariable('Student_Years_Ary',$this->getStudentListYears());
		}
		
		$this->contentReady();
	}
	
	private function setObjVariable($VarName, $VarValue){
		$this->{$VarName} = $VarValue;
	}
	private function getObjVariable($VarName){
		return $this->{$VarName};
	}
	private function getStudentRecords($RecordType){
		return $this->{$RecordType}[$this->getStudent()];
	}
	
	public function setStudent($studentId){
//		$this->studentId = $studentId;
		$this->setObjVariable('studentId',$studentId);
	}
	public function getStudent(){
		return $this->getObjVariable('studentId');
	}
	
	private function contentReady(){
		$reportType = $this->getObjVariable('reportType');
		$academicYearID = $this->getObjVariable('academicYearID');
		$this->setObjVariable('Student_info_Ary', $this->getStudentList_Info() );

		if($reportType == ''){
			// demerit
			$this->setObjVariable('Student_Demerit_Ary', $this->getStudentListDemeritRecords() );
			$PC_Info_Years_Ary[$academicYearID[0]] = $this->getAllStudentPC($academicYearID[0]);
			$this->setObjVariable('PC_Info_Ary',$PC_Info_Years_Ary);
		}
		else{
			//teacher comments
			$this->setObjVariable('Student_comments_Ary', $this->getStudentListComment() );
			
			//
			foreach((array)$academicYearID as $_ID){
				$PC_Info_Years_Ary[$_ID] = $this->getAllStudentPC($_ID);
			}
			$this->setObjVariable('PC_Info_Ary',$PC_Info_Years_Ary);
		}
		$this->setObjVariable('student_Merit_Arr', $this->getStudentListMeritRecords() );
	}	
	
	private function getStudentListYears(){
		$studentList = $this->getObjVariable('studentList');
		$reportType = $this->getObjVariable('reportType');
		
		if($reportType == 1){
			$formAry = array(1,2,3);
		}
		else if($reportType == 2){
			$formAry = array(4,5,6);
		}
		$sql = "Select UserID, ClassName, ay.YearNameEN as AcademicYear, pch.AcademicYearID 
				From PROFILE_CLASS_HISTORY as pch 
					INNER JOIN ACADEMIC_YEAR as ay on pch.AcademicYearID = ay.AcademicYearID
				where 
					pch.userid IN  ('".implode("','",$studentList)."') 
					AND pch.ClassName REGEXP '".implode('|',$formAry)."' 
				order by pch.AcademicYear asc";
		$result = $this->objDB->returnResultSet($sql);
		
		$this->setObjVariable('academicYearID',array_unique(Get_Array_By_Key($result,'AcademicYearID')));
		
		foreach((array) $result as $_key => $_infoAry){
			$_userID = $_infoAry['UserID'];
			$_academicYearID = $_infoAry['AcademicYearID'];
			$userYearsAssoArr[$_userID][$_academicYearID] = $_infoAry;
		}
		return $userYearsAssoArr;
	}
	public function getStudentYears(){
		return $this->getStudentRecords('Student_Years_Ary');
	}
	
	//Get Student Info
	private function getStudentList_Info(){
		
		$studentList = $this->getObjVariable('studentList');
		
		$sql = "select " .
				"iu.userid,
					iu.EnglishName,
					iu.ChineseName,
					DATE_FORMAT(iu.DateOfBirth, '%d/%m/%Y') AS DateOfBirth,
					iu.ClassName,
					iu.ClassNumber,
					iu.Gender,
					iu.BarCode,
					iu.STRN,
					iu.HKID,
					substring(iu.WebSAMSRegNo from 2) as WebSAMSRegNo ,
					iu.userid,
					iu.UserLogin
				from
					".$this->intranet_db.".INTRANET_USER AS iu
				where
					iu.userid IN ('".implode("','",(array)$studentList)."') 
					AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";

		$result = $this->objDB->returnResultSet($sql);
		
		return BuildMultiKeyAssoc($result,'userid');
	}
	public function getStudentInfo(){
		return $this->getStudentRecords('Student_info_Ary');
	}
	
	function getEventStudentInfo($event_id,$student_id){
		$sql="SELECT `CommentStudent`,`Performance`,`Achievement` 
				FROM `INTRANET_ENROL_EVENTSTUDENT` 
				WHERE `EnrolEventID`='".$event_id."' AND `StudentID`='".$student_id."' AND `RecordStatus` = 2;";
		$rows_event_student = $this->objDB->returnResultSet($sql);
		if(count($rows_event_student)>0){
			return $rows_event_student[0];
		}else{
			return false;
		}
	}
	
	function getStudentListMeritRecords(){
		
		global $Lang;
		$task = $this->getObjVariable('task');
		
		$studentList = $this->getObjVariable('studentList');
		$yearTermID = $this->getObjVariable('yearTermID');
		$isTerm2 = $this->isTerm2($yearTermID);
		$academicYearID = $this->getObjVariable('academicYearID');
		if($yearTermID!=''){
			$yearTerm_conds = " AND iegi.Semester IN ('".implode("','",(array)$yearTermID)."') ";
			if($isTerm2){
				// Term2 include whole year record
				$yearTerm_conds = " AND (iegi.Semester IN ('".implode("','",(array)$yearTermID)."') 
										OR iegi.Semester = 0 OR iegi.Semester = '' OR iegi.Semester is NULL) ";
			}
		}
		
		// Get Groups
		$sql = "SELECT 
					iegi.EnrolGroupID,
					oc.RecordID as CatID,
					oc.ChiTitle as OLECatName, 
					ig.TitleChinese as ItemName,
					ig.academicYearID
				FROM 
					".$this->intranet_db.".INTRANET_ENROL_GROUPINFO  as iegi 
					inner join ".$this->intranet_db.".INTRANET_GROUP as ig on ig.groupid = iegi.groupid
					inner join ".$this->intranet_db.".INTRANET_ENROL_CATEGORY as iec on iegi.GroupCategory = iec.CategoryID and iec.OleCategoryID > 0
					inner join ".$this->eclass_db.".OLE_CATEGORY as oc on oc.RecordID = iec.OleCategoryID
				WHERE 
					ig.academicYearID IN ('".implode("','",(array)$academicYearID)."') 
					$yearTerm_conds
				ORDER BY 
					iec.DisplayOrder ASC";
		$groupInfoArr = $this->objDB->returnResultSet($sql);
		$groupInfoAssoArr = BuildMultiKeyAssoc($groupInfoArr,'EnrolGroupID');
		$EnrolGroupIDArr = Get_Array_By_Key($groupInfoArr,'EnrolGroupID');
		
		// Get Events
		$sql = "SELECT 
					ieei.EnrolEventID,
					ifnull(oc.RecordID,0) as CatID,
					ifnull(oc.ChiTitle,'其他領域') as OLECatName, 
					ieei.EventTitle as ItemName,
					ieei.academicYearID
				FROM 
					".$this->intranet_db.".INTRANET_ENROL_EVENTINFO  as ieei 
					inner join ".$this->intranet_db.".INTRANET_ENROL_CATEGORY as iec on iec.CategoryID = ieei.EventCategory
					inner join ".$this->eclass_db.".OLE_CATEGORY as oc on oc.RecordID = iec.OleCategoryID
				WHERE 
					ieei.academicYearID IN ('".implode("','",(array)$academicYearID)."') and ieei.IncludeInReport = 1
				ORDER BY 
					iec.DisplayOrder ASC";
		$eventInfoArr = $this->objDB->returnResultSet($sql);
		$eventInfoAssoArr = BuildMultiKeyAssoc($eventInfoArr,'EnrolEventID');
		$EnrolEventIDArr = Get_Array_By_Key($eventInfoArr,'EnrolEventID');
		
		// Get Student Group Comments, achievement
		$sql = "SELECT 
					iug.UserID, iug.EnrolGroupID, iug.GroupID , iug.Performance , iug.CommentStudent, concat(iug.Achievement) AS `Achievement` , ir.Title AS `RoleTitle`
				FROM 
					".$this->intranet_db.".INTRANET_USERGROUP as iug
					inner join ".$this->intranet_db.".INTRANET_GROUP as ig on ig.GroupID = iug.GroupID
					inner join ".$this->intranet_db.".INTRANET_ENROL_GROUPINFO  as iegi on ig.GroupID = iegi.GroupID
					inner join ".$this->intranet_db.".INTRANET_ENROL_CATEGORY as iec on iec.CategoryID = iegi.GroupCategory 
					left join ".$this->intranet_db.".INTRANET_ROLE as ir on ir.RoleID = iug.RoleID
				WHERE 
 					ig.RecordType = 5
 					AND (ir.RecordType = 5 OR ir.RecordType IS NULL)
					AND iug.EnrolGroupID IN ('".implode("','",$EnrolGroupIDArr)."') 
					AND iug.UserID In ('".implode("','", (array)$studentList)."')
				ORDER BY 
					iec.DisplayOrder ASC";

		$studentInfoArr = $this->objDB->returnResultSet($sql);
		$studentMeritAssoArr = array();
		foreach($studentInfoArr as $_infoArr){
			$_studentID = $_infoArr['UserID'];
			$_enrolGroupID = $_infoArr['EnrolGroupID'];
			$_catID = $groupInfoAssoArr[$_enrolGroupID]['CatID'];
			
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolGroupID] = $_infoArr;
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolGroupID]['CatID'] = $_catID;
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolGroupID]['OLECatName'] = $groupInfoAssoArr[$_enrolGroupID]['OLECatName'];
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolGroupID]['ItemName'] = $groupInfoAssoArr[$_enrolGroupID]['ItemName'].''.($_infoArr['RoleTitle']=='---'?'':$_infoArr['RoleTitle']);
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolGroupID]['academicYearID'] = $groupInfoAssoArr[$_enrolGroupID]['academicYearID'];
		}
		
		// Get Student Group Merit
		$sql = "SELECT 
					StudentID,EnrolGroupID, MeritType , MeritCount 
				FROM 
					".$this->intranet_db.".INTRANET_ENROL_MERIT_RECORD 
				WHERE 
					StudentID In ('".implode("','", (array)$studentList)."') 
					AND EnrolGroupID IN ('".implode("','",$EnrolGroupIDArr)."') 
					AND MeritType IS NOT NULL";
		$studentGroupArr =  $this->objDB->returnResultSet($sql);
		
		foreach((array)$studentGroupArr as $_meritArr){
			$_studentID = $_meritArr['StudentID'];
			$_enrolGroupID = $_meritArr['EnrolGroupID'];
			$_catID = $groupInfoAssoArr[$_enrolGroupID]['CatID'];
			$_meritType = $_meritArr['MeritType'];
			$_meritcount = $_meritArr['MeritCount'];
			if(isset($studentMeritAssoArr[$_studentID][$_catID][$_enrolGroupID])){
				$studentMeritAssoArr[$_studentID][$_catID][$_enrolGroupID][$_meritType] = $this->getNumberString((int)$_meritcount).$Lang['eDiscipline']['TimesCH'];
			}
		}
		
		// Get Student Group Event Merit
		$sql = "SELECT 
					iee.StudentID,iee.EnrolEventID, ieemr.MeritType , ieemr.MeritCount 
				FROM 
					".$this->intranet_db.".INTRANET_ENROL_EVENTSTUDENT iee 
					left join ".$this->intranet_db.".INTRANET_ENROL_EVENT_MERIT_RECORD ieemr
				on (iee.StudentID=ieemr.StudentID and iee.EnrolEventID=ieemr.EnrolEventID)
				WHERE 
					iee.StudentID In ('".implode("','", (array)$studentList)."') 
					AND iee.EnrolEventID IN ('".implode("','",$EnrolEventIDArr)."')
					AND iee.RecordStatus = 2
					";
		$studentEventArr =  $this->objDB->returnResultSet($sql);
		
		foreach((array)$studentEventArr as $_meritArr){
			$_studentID = $_meritArr['StudentID'];
			$_enrolEventID = $_meritArr['EnrolEventID'];
			$_catID = $eventInfoAssoArr[$_enrolEventID]['CatID'];
			$_meritType = $_meritArr['MeritType'];
			$_meritcount = $_meritArr['MeritCount'];
			
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolEventID]['CatID'] = $_catID;
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolEventID]['OLECatName'] = $eventInfoAssoArr[$_enrolEventID]['OLECatName'];
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolEventID]['ItemName'] = $eventInfoAssoArr[$_enrolEventID]['ItemName'];
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolEventID]['academicYearID'] = $eventInfoAssoArr[$_enrolEventID]['academicYearID'];
			$row_event_student=$this->getEventStudentInfo($_enrolEventID,$_studentID);
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolEventID]['Performance'] = $row_event_student['Performance'];
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolEventID]['CommentStudent'] = $row_event_student['CommentStudent'];
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolEventID]['Achievement'] = $row_event_student['Achievement'];
			$studentMeritAssoArr[$_studentID][$_catID][$_enrolEventID][$_meritType] = $this->getNumberString((int)$_meritcount).$Lang['eDiscipline']['TimesCH'];
		}
		
		//from eDis
		// eDis records do not show in report 3 & 5
		if(in_array($task,array('sptss_1','sptss_2','sptss_4'))){
			$rows_edis=$this->getStudentListMeritProfileRecords();
			
			foreach((array)$rows_edis as $row_edis){
				$_studentID = $row_edis['UserID'];
				$_StudentMeritID = $row_edis['StudentMeritID'];
				$_catID = 0;
				$_meritType = $row_edis['RecordType'];
				$_meritcount = $row_edis['NumberOfUnit'];
				
				$studentMeritAssoArr[$_studentID][$_catID][$_StudentMeritID]['CatID'] = $_catID;
				$studentMeritAssoArr[$_studentID][$_catID][$_StudentMeritID]['OLECatName'] = '其他領域';
				$studentMeritAssoArr[$_studentID][$_catID][$_StudentMeritID]['ItemName'] = $row_edis['Reason'];
				$studentMeritAssoArr[$_studentID][$_catID][$_StudentMeritID]['academicYearID'] = $row_edis['AcademicYearID'];
				$studentMeritAssoArr[$_studentID][$_catID][$_StudentMeritID]['Performance'] = '';
				$studentMeritAssoArr[$_studentID][$_catID][$_StudentMeritID]['CommentStudent'] = '';
				$studentMeritAssoArr[$_studentID][$_catID][$_StudentMeritID]['Achievement'] = '';
				$studentMeritAssoArr[$_studentID][$_catID][$_StudentMeritID][$_meritType] = $this->getNumberString((int)$_meritcount).$Lang['eDiscipline']['TimesCH'];
			}
		}
		
//		declare at the beginning		
// 		$task = $this->getObjVariable('task');

//		this part do at the end of this function
// 		if(in_array($task,array('sptss_1','sptss_2','sptss_4'))){
// 			foreach((array)$studentMeritAssoArr as $i_student_id=>$array_cat){
// 				foreach((array)$array_cat as $i_cat => $array_id){
// 					foreach((array)$array_id as $i_id => $array_record){
// 						if(
// 							(!isset($array_record['1']) || $array_record['1']=='0' || $array_record['1']=='')
// 							&& (!isset($array_record['2']) || $array_record['2']=='0' || $array_record['2']=='')
// 							&& (!isset($array_record['3']) || $array_record['3']=='0' || $array_record['3']=='')
// 						){
// 							unset($studentMeritAssoArr[$i_student_id][$i_cat][$i_id]);
// 						}
// 					}
// 					if(count($studentMeritAssoArr[$i_student_id][$i_cat])<=0){
// 						unset($studentMeritAssoArr[$i_student_id][$i_cat]);
// 					}
// 				}
// 				if(count($studentMeritAssoArr[$i_student_id])<=0){
// 					unset($studentMeritAssoArr[$i_student_id]);
// 				}
// 			}
// 		}		
		
		// from eReportCard > iPo Assessment
		if($yearTermID!='') {
			$yearTerm_assesment_conds = " AND YearTermID IN ('".implode("','",(array)$yearTermID)."') ";
			if($isTerm2) {
			    // Term 2 include whole year record
				$yearTerm_assesment_conds = " AND (YearTermID IN ('".implode("','",(array)$yearTermID)."') 
										              OR YearTermID = 0 OR YearTermID = '' OR YearTermID is NULL) ";
			}
		} else {
			$yearTerm_assesment_conds = "";
		}
		$yearTerm_assesment_conds .= " AND TermAssessment IS NULL ";
		
		// Get CatID 
		$sql = "SELECT RecordID FROM ".$this->eclass_db.".OLE_CATEGORY WHERE ChiTitle = '其他領域'";
		$OthersCatID = $this->objDB->returnResultSet($sql);
		$eRC_CatID = $OthersCatID[0]['RecordID'];
		if($eRC_CatID == 0 || $eRC_CatID == '') {
			$eRC_CatID = 0;
		}
		
		// form rank 1- 5
		$sql = "SELECT 
					UserID, AcademicYearID , YearTermID ,(SELECT YearTermNameB5 FROM ".$this->intranet_db.".ACADEMIC_YEAR_TERM ayt WHERE asmr.YearTermID = ayt.YearTermID) as YearTermNameB5, OrderMeritForm as FormRank
				FROM ".$this->eclass_db.".ASSESSMENT_STUDENT_MAIN_RECORD asmr
				WHERE 
					asmr.OrderMeritForm BETWEEN 1 AND 5 
					AND asmr.UserID In ('".implode("','", (array)$studentList)."')
					AND asmr.academicYearID IN ('".implode("','",(array)$academicYearID)."') 
					$yearTerm_assesment_conds";
		$formRankArr = $this->objDB->returnResultSet($sql);
		foreach((array)$formRankArr as $_formRankArr){
			$_studentID = $_formRankArr['UserID'];
			$_tempArr['ItemName'] = ($_formRankArr['YearTermNameB5']==''?'全年':$_formRankArr['YearTermNameB5']).'全級第'.$this->getNumberString($_formRankArr['FormRank']).'名';
			$_tempArr['OLECatName'] = '其他領域';
			$_tempArr['academicYearID'] = $_formRankArr['AcademicYearID'];
			$studentMeritAssoArr[$_studentID][$eRC_CatID][] = $_tempArr;
			unset($_tempArr);
		}
		
		// class rank 1
		$sql = "SELECT 
					UserID, AcademicYearID , YearTermID ,(SELECT YearTermNameB5 FROM ".$this->intranet_db.".ACADEMIC_YEAR_TERM ayt WHERE asmr.YearTermID = ayt.YearTermID) as YearTermNameB5, OrderMeritClass as ClassRank
				FROM ".$this->eclass_db.".ASSESSMENT_STUDENT_MAIN_RECORD asmr
				WHERE 
					asmr.OrderMeritClass = 1 
					AND asmr.UserID In ('".implode("','", (array)$studentList)."') 
					AND asmr.academicYearID IN ('".implode("','",(array)$academicYearID)."') 
					$yearTerm_assesment_conds";
		$classFirstArr = $this->objDB->returnResultSet($sql);
		foreach((array)$classFirstArr as $_classFirstArr){
			$_studentID = $_classFirstArr['UserID'];
			$_tempArr['ItemName'] = ($_classFirstArr['YearTermNameB5']==''?'全年':$_classFirstArr['YearTermNameB5']).'全班第'.$this->getNumberString($_classFirstArr['ClassRank']).'名';
			$_tempArr['OLECatName'] = '其他領域';
			$_tempArr['academicYearID'] = $_classFirstArr['AcademicYearID'];
			$studentMeritAssoArr[$_studentID][$eRC_CatID][] = $_tempArr;
			unset($_tempArr);
		}
		
		// subject form rank 1
		$sql = "SELECT 
					UserID, 
					AcademicYearID, 
					YearTermID,(SELECT YearTermNameB5 FROM ".$this->intranet_db.".ACADEMIC_YEAR_TERM ayt WHERE assr.YearTermID = ayt.YearTermID) as YearTermNameB5,
					SubjectCode,
					SubjectID, 
					SubjectComponentCode, 
					SubjectComponentID, 
					OrderMeritForm
				FROM ".$this->eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD  assr
				WHERE 
					OrderMeritForm = 1
					AND (SubjectComponentCode = '' or SubjectComponentCode IS NULL)
					AND (SubjectComponentID = '' or SubjectComponentID IS NULL)
					AND UserID In ('".implode("','", (array)$studentList)."') 
					AND academicYearID IN ('".implode("','",(array)$academicYearID)."') 
					$yearTerm_assesment_conds";
		$subjectRank1Arr = $this->objDB->returnResultSet($sql);

		$sql = "Select RecordID, CH_DES as SubjectChi From ASSESSMENT_SUBJECT where CMP_CODEID = '' or CMP_CODEID is null";
		$schoolSujectAssocArr = BuildMultiKeyAssoc($this->objDB->returnResultSet($sql) , 'RecordID' , array('SubjectChi'),1 );
		foreach((array)$subjectRank1Arr as $_subjectRankArr ){
			$_studentID = $_subjectRankArr['UserID'];
			$_subjectID = $_subjectRankArr['SubjectID'];
			$_tempArr['ItemName'] = ($_subjectRankArr['YearTermNameB5']==''?'全年':$_subjectRankArr['YearTermNameB5']).'全級'.$schoolSujectAssocArr[$_subjectID].'科第'.$this->getNumberString($_subjectRankArr['OrderMeritForm']).'名';
			$_tempArr['OLECatName'] = '其他領域';
			$_tempArr['academicYearID'] = $_subjectRankArr['AcademicYearID'];
			$studentMeritAssoArr[$_studentID][$eRC_CatID][] = $_tempArr;
			unset($_tempArr);
		}
		
		// remove all the records without merit for report 1, 2, 4
		if(in_array($task,array('sptss_1','sptss_2','sptss_4'))){
			foreach((array)$studentMeritAssoArr as $i_student_id=>$array_cat){
				foreach((array)$array_cat as $i_cat => $array_id){
					foreach((array)$array_id as $i_id => $array_record){
						if(
								(!isset($array_record['1']) || $array_record['1']=='0' || $array_record['1']=='')
								&& (!isset($array_record['2']) || $array_record['2']=='0' || $array_record['2']=='')
								&& (!isset($array_record['3']) || $array_record['3']=='0' || $array_record['3']=='')
								){
									unset($studentMeritAssoArr[$i_student_id][$i_cat][$i_id]);
						}
					}
					if(count($studentMeritAssoArr[$i_student_id][$i_cat])<=0){
						unset($studentMeritAssoArr[$i_student_id][$i_cat]);
					}
				}
				if(count($studentMeritAssoArr[$i_student_id])<=0){
					unset($studentMeritAssoArr[$i_student_id]);
				}
			}
		}
		
		return $studentMeritAssoArr;
	}
	public function getStudentMeritRecords(){
		return $this->getStudentRecords('student_Merit_Arr');
	}
	public function getStudentAYMeritRecords(){
		$meritCatArr = $this->getStudentMeritRecords();
		$meritAssoArr = array();
		foreach((array) $meritCatArr as $catID => $merit){			
			foreach((array)$merit as $_enrolGroupID => $_infoAry){
				$_academicYearID = $_infoAry['academicYearID'];
				$meritAssoArr[$_academicYearID][$catID][] = $_infoAry;
			}
		}
		
		foreach((array)$meritAssoArr  as $_yearID => $_temp1){
			$countItem = 0;
			foreach((array)$_temp1  as $__catID => $__temp2){
				$countItem += count($__temp2);
			}
			$meritAssoArr[$_yearID]['countItem'] = $countItem;
		}
		return $meritAssoArr;
	}
	
	private function getStudentListDemeritRecords(){
		$studentList = $this->getObjVariable('studentList');
		$yearTermID = $this->getObjVariable('yearTermID');
		$academicYearID = $this->getObjVariable('academicYearID');
		
		if($yearTermID != ''){
			$yearTerm_conds = " AND YearTermID IN ('".implode("','",(array)$yearTermID)."') ";	
		}
		
		$sql = "SELECT UserID ,Reason, RecordType, SUM(NumberOfUnit) as NumberOfUnit
				FROM
					".$this->intranet_db.".PROFILE_STUDENT_MERIT 
				WHERE
					UserID IN ('".implode("','", (array)$studentList)."')
					AND AcademicYearID IN ('".implode("','", (array)$academicYearID)."')
					$yearTerm_conds
					AND RecordType < 0
				GROUP BY
					UserID ,Reason, RecordType
				";
		$result = $this->objDB->returnResultSet($sql);
		
		$resultAry = array();
		foreach((array)$result as $_ary){
			$resultAry[$_ary['UserID']][] = $_ary;
		}
		return $resultAry;
	}
	public function getStudentDemeritRecords(){
		return $this->getStudentRecords('Student_Demerit_Ary');
	}
	
	private function getStudentListMeritProfileRecords(){
		$studentList = $this->getObjVariable('studentList');
		$yearTermID = $this->getObjVariable('yearTermID');
		$academicYearID = $this->getObjVariable('academicYearID');
		
		if($yearTermID != ''){
			$yearTerm_conds = " AND YearTermID IN ('".implode("','",(array)$yearTermID)."') ";	
		}
		
		$sql = "SELECT StudentMeritID, UserID ,Reason, RecordType, SUM(NumberOfUnit) as NumberOfUnit, AcademicYearID
				FROM
					".$this->intranet_db.".PROFILE_STUDENT_MERIT 
				WHERE
					UserID IN ('".implode("','", (array)$studentList)."')
					AND AcademicYearID IN ('".implode("','", (array)$academicYearID)."')
					AND RecordType > 0
				GROUP BY
					UserID, Reason
				";
		$result = $this->objDB->returnResultSet($sql);

		$resultAry = array();
		foreach((array)$result as $_ary){
			$resultAry[$_ary['UserID']][] = $_ary;
		}
		return $result;
	}
	
	private function getStudentListComment(){
		// 3 years comments
		
		$studentList = $this->getObjVariable('studentList');
		$yearTermID = $this->getObjVariable('yearTermID');
		$academicYearID = $this->getObjVariable('academicYearID');
		
		if($yearTermID != ''){
			$yearTerm_conds = " AND YearTermID IN ('".implode("','",(array)$yearTermID)."') ";	
		}
		
		$sql ="
			SELECT
				cs.UserID,
				cs.AcademicYearID,
				cs.CommentChi as Comment,
				cs.Semester, 
				if(yt.YearTermNameB5='上學期' || cs.Semester='上學期', 1 , 2) as TermNum
			FROM
				".$this->eclass_db.".CONDUCT_STUDENT as cs
				LEFT JOIN ".$this->intranet_db.".ACADEMIC_YEAR_TERM as yt ON cs.YEARTERMID = yt.YEARTERMID 
			WHERE
				cs.UserID IN ('".implode("','",$studentList)."')
				AND cs.AcademicYearID IN ('".implode("','",(array)$academicYearID)."')
				AND cs.YEARTERMID > 0
			ORDER BY 
				yt.TermStart asc
			";

		$result = $this->objDB->returnArray($sql); 
		foreach($result as $_infoAry){
			$_UserID = $_infoAry['UserID'];
			$_AcademicYearID = $_infoAry['AcademicYearID'];
			$_TermNum = $_infoAry['TermNum'];
			$returnArr[$_UserID][$_AcademicYearID][$_TermNum] = $_infoAry;
		}
		
		return $returnArr;
	}
	public function getStudentComment(){
		return $this->getStudentRecords('Student_comments_Ary');
	}
	
	private function getAllStudentPC($AcademicYearID){
		global $PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libimporttext.php");
		$libimport = new libimporttext();
		
		# Personal Attributes 
		$FileCSV = $PATH_WRT_ROOT."file/portfolio/sptss_pc_".$AcademicYearID.".csv";
		if (file_exists($FileCSV))
		{
			$csv_data = $libimport->GET_IMPORT_TXT($FileCSV);
			$csv_header = $csv_data[0];
			####  cust header catering here ####
			$num_header = count($csv_header);
			for($x = 3; $x <$num_header; $x++){
				$_headerString = $csv_header[$x];
				$_headerArr = explode(':',$_headerString);
				$resultHeaderTopArr[$_headerArr[0]][] = $_headerArr[1];
				$resultHeaderDownArr[$x] = $_headerArr[1];
			}
			$headerAssoArr['Top'] =  $resultHeaderTopArr;
			$headerAssoArr['Down'] =  $resultHeaderDownArr;
			$this->setObjVariable('PC_Header_Ary',$headerAssoArr);	
			
			for ($i=1; $i<sizeof($csv_data); $i++)
			{
				$rowObj = $csv_data[$i];
				$array_rowObj = explode(',',$rowObj[0]);
				$returnArr[ltrim(trim($array_rowObj[0], "#"),'0' )] = $rowObj;
			}
		}
		
		return $returnArr;
	}
	public function getStudentPCHeader(){
		return $this->getObjVariable('PC_Header_Ary');
	}
	public function getStudentPC($AcademicYearID,$WebSAMSNum,$studend_id=''){
		global $eclass_db;
		
		if(is_array($AcademicYearID) && count($AcademicYearID) == 1){
			$AcademicYearID = $AcademicYearID[0];
		}
		
		if($studend_id==''){
			$array_pc = explode(',',$this->PC_Info_Ary[$AcademicYearID]['s'.ltrim($WebSAMSNum)][0]);
			if(count($array_pc)>1){
				return $array_pc;
			}else{
				return $this->PC_Info_Ary[$AcademicYearID]['s'.ltrim($WebSAMSNum)];
			}
		}else{
			$row_personal_character = $this->getStudentPCFromDB($AcademicYearID,$studend_id);
			return array(
				'',
				'',
				'',
				$row_personal_character['Integrity'],
				$row_personal_character['Courtesy'],
				$row_personal_character['RightFromWrong'],
				$row_personal_character['Relationship'],
				$row_personal_character['LearningAttitude'],
				$row_personal_character['Autonomy'],
			);
		}
	}
	public function getStudentPCFromDB($academicYearID,$StudentID){
		global $intranet_db,$eclass_db;
		
		$sql =  "SELECT csspc.*
          FROM
            {$intranet_db}.INTRANET_USER AS iu
          INNER JOIN
            {$intranet_db}.YEAR_CLASS_USER AS ycu
          ON
            iu.UserID = ycu.UserID
          INNER JOIN
            {$intranet_db}.YEAR_CLASS AS yc
          ON
            ycu.YearClassID = yc.YearClassID
          LEFT JOIN
            {$eclass_db}.CUSTOM_SPTSS_STUDENT_PERSONAL_CHARACTER AS csspc
          ON
            csspc.StudentID = iu.UserID  AND yc.YearClassID = csspc.YearClassID
          WHERE
            yc.AcademicYearID = ".$academicYearID."
            AND iu.UserID = ".$StudentID."
          GROUP BY
            iu.UserID
        ";
		$rows_personal_character = $this->objDB->returnArray($sql);
		return $rows_personal_character[0];
	}
	
	public function getUserPhoto($UserLogin){
		global $intranet_root;
		//return $user_photo_path = $intranet_root."/file/user_photo/".$UserLogin.".jpg";
		return $user_photo_path = "/file/user_photo/".$UserLogin.".jpg";
	}
	
	public function isTerm2($YermTermID){
		
		$task = $this->getObjVariable('task');
		if($task == 'sptss_2'){
			return true;
		}
		else{
			return false;
		}
//		$sql = "SELECT YearTermNameB5 from ACADEMIC_YEAR_TERM WHERE YearTermID = '$yearTermID'";
//		$YearTermName = $this->objDB->returnVector($sql);
//		if(trim($YearTermName[0])=='下學期'){
//			return true;
//		}
//		else{
//			return false;
//		}
	}
	//----------------------------------------------- ABOVE ARE DATA COLLECTION RELATED ------------------------------------------------//
	#####################################################################################################################################
	#																																	#
	#																																	#
	#####################################################################################################################################	
	//------------------------------------------------ BELOW ARE DATA HANDLING RELATED -------------------------------------------------//

	public function getDigestedMeritArray($MeritArr,$MeritType='', $Numbers=''){
		global $Lang;
		
		foreach((array)$MeritArr as $_MeritInfo){
			$reason = $_MeritInfo['Reason'];
			$recordType = $_MeritInfo['RecordType'];
			$numUnit = $_MeritInfo['NumberOfUnit'];

			$tempArr[$reason][$recordType] = $numUnit+0;
		}
		
		
		
		foreach((array)$tempArr as $_reason => $_temp2){
			foreach((array)$_temp2 as $__recordType => $__number){
				$resultArr[$_reason][$__recordType] = $this->getNumberString($__number).$Lang['eDiscipline']['TimesCH'];
			}
		}
		return $resultArr;
	}
	
	public function getNumberString($Integer){
		global $Lang;
		// Only support up to 39
		
		if($Integer == 0 || $Integer == ''){
			return '- -';
		}
		$firstDigit = $Integer%10;
		
		if($Integer >= 10){
			$tens = ($Integer - $firstDigit)%100;
			
			if($firstDigit==0){
				// 10 20 30
				return $Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][$tens];
			}
			else{
				// 11 12 13 ....
				return $Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][$tens].$Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][$firstDigit];
			}
		}
		else{
			// 1 2 3 ....
			return $Lang['eDiscipline']['IssueDateChi']['ChineseNumber'][$firstDigit];
		}
	}
	//------------------------------------------------ ABOVE ARE DATA HANDLING RELATED ------------------------------------------------//
	#####################################################################################################################################
	#																																	#
	#																																	#
	#####################################################################################################################################	
	//------------------------------------------------ BELOW ARE HTML AND CSS RELATED -------------------------------------------------//

	public function returnStyleSheet(){
		
		$task = $this->getObjVariable('task');
		
		$style = '';
		$style .= '<style>';
		$style .= 'body { font-size:1px; font-family:"mingliu";}
					table { overflow : wrap; }
					th,td {font-size:23ex;}
					span.remarks {font-size:22ex;}

					.spacing {font-size:30ex;}

					.headerTable td{font-size:20ex; 
								  font-family:"mingliu";
								  text-align:center;}

					.footerTable td{font-size:20ex; 
								  font-family:"mingliu";
								  }
					.signature {
									border-bottom :1px solid #000; 
								}
					.pageBreakAvoid{
						page-break-inside: avoid;
					}
					.reportTitle {font-size:50ex; font-weight:bold;  text-align:center;}
					/*.stuInfoTable {border-bottom :3px solid #000; }*/
					.stuInfotable .toRight {text-align:right; /*padding: 5 0;*//*line-height:300%;*/}
					/*.stuInfotable .specialPadding {padding: 0 0 5 0;} */';
					
		switch($task){
			
			case 'sptss_1':
			case 'sptss_2':
			$style .= '
					.contentTable { border-collapse:collapse;}
					.contentTable th{text-align:left; font-weight:normal; border-bottom :1px solid #000; padding:10 0 2 0;}
					.contentTable th.merit { text-align:center;}
					.contentTable td.merit { text-align:center;padding:5 0 5 0;}
					.contentTable td {padding:5 10 5 0; vertical-align:top;}

					.title {font-size:30ex;
							font-weight:bold; 
							border-bottom :1px solid #000; }
					.personalQuality { border-collapse:collapse; border:1px solid #000; text-align:center;}
					.personalQuality td { border:1px solid #000; padding:5;}
					/*.contentTable {border-bottom :1px solid #000;}*/

					.alignCenter {text-align:center;}';
			break;
			case 'sptss_3':
			$style .= '
					.contentTable { border-collapse:collapse;}
					.contentTable th{text-align:left; font-weight:normal; border-bottom :3px solid #000; padding:10 0 2 0;}
					.contentTable td.merit { text-align:center;}
					.contentTable td.merit { text-align:center;}
					.contentTable td {padding:5 10 5 0; vertical-align:top;}

					.title {font-size:30ex;
							font-weight:bold; 
							border-bottom :1px solid #000; }
					.personalQuality { border-collapse:collapse; border:1px solid #000; text-align:center; }
					.personalQuality td { border:1px solid #000; padding:5;}
					/*.contentTable {border-bottom :1px solid #000;}*/

					.alignCenter {text-align:center;}';
			break;
			case 'sptss_4':
			case 'sptss_5':
			$style .= '				
					.contentTable { border-collapse:collapse;}
					.contentTable th{text-align:left; font-weight:normal; border-bottom :3px solid #000; }
					.contentTable th.merit { text-align:center;}
					.contentTable td.merit { text-align:center;}
					.contentTable td {padding:5 10 5 0; vertical-align:top;}

					.title {font-size:30ex;
							
							 }
					.personalQuality { border-collapse:collapse; text-align:center; }
					.personalQuality td { padding:5;}
					.highlight { font-weight:bold; text-decoration: underline;}

					.alignCenter {text-align:center;}

					';
			break;
		}
		
		$style .= '</style>';
		return $style;
	}
	
	public function returnHeader(){
		$header = '<table width="100%" class="headerTable" align="center">
			<tr>
			<td width="30%"></td>
			<td>Tel: 23265211</td>
			<td>Email: info@pooitun.edu.hk</td>
			<td width="30%"></td>
			</tr>
			</table>';
		return $header;
	}
	
	public function returnFooter(){
		global $issuedate;
		
		$footer = '<table width="100%" class="footerTable" align="center">
			<tr>
				<td width="10%">簽發日期:</td>
				<td width="36%">'.$issuedate.'</td>
				<td width="8%">校長簽署:</td>
				<td width="26%" class="signature"></td>
				<td width="20%"></td>
			</tr>
			<tr>
				<td colspan="5">Note: English version could be provided upon request.</td>
			</tr>
			<tr>
				<td colspan="5" align="center">頁 {PAGENO}</td>
			</tr>
			</table>';
		return $footer;
	}
	
	public function returnStudentInfoTable(){
		global $issueAYName; // from sptss_1.php - sptss_5.php
		
			$studentInfo = $this->getStudentInfo();
			$photoAbsPath = $this->getUserPhoto($studentInfo['UserLogin']);
			
			if(file_exists($_SERVER['DOCUMENT_ROOT'].$photoAbsPath)){
				$officialPhoto= '<img src="'.$photoAbsPath.'" height="120"/>';//width="92" 
			}
			else{
				$officialPhoto= '';
			}
			
			$task = $this->getObjVariable('task');
			
			switch($task){
				case 'sptss_1':
				$reportTitle = '學生功過記錄明細';
				break;
				case 'sptss_2':
				$reportTitle = '學生生命素質涵養及功過記錄明細';
				break;
				case 'sptss_3':
				$reportTitle = '學生全人發展記錄表';
				$header_bottom_line = 
				'<table width="100%" style="border-bottom:3px solid #000;">
					<tr>
						<td style="font-size:1ex;">&nbsp;</td>
					</tr>
				</table>';
				break;
				case 'sptss_4':
				//$reportTitle = '歷年生命素養涵養記錄';
                $reportTitle = '歷年生命素質涵養記錄';
				break;
				case 'sptss_5':
				$reportTitle = '歷年全人發展記錄表';
				break;
			}
			
			switch($task){
				case 'sptss_1':
				case 'sptss_2':
				case 'sptss_4':
				$header_bottom_line = 
				'<table class="line" width="100%" style="border-bottom:3px solid #000;">
					<tr>
						<td style="font-size:1ex;">&nbsp;</td>
					</tr>
				</table>';
				break;
				case 'sptss_3':
				case 'sptss_5':
				$header_bottom_line = '';
				break;
			}
			
			$stu_info_table = '
			<table width="100%" class="stuInfoTable" style="border-collapse:collapse;">
				<tr>
					<td width="20%" colspan="2"></td>
					<td width="60%" colspan="3" class="reportTitle">'.$reportTitle.'</td>
					<td width="20%" rowspan="5" align="center">'.$officialPhoto.'</td>
				</tr>
				<tr>
					<td>年度:</td>
					<td>'.$issueAYName.'</td>
					<td colspan="3" class="specialPadding">&nbsp;</td>
				</tr>
				<tr>
					<td>學生編號:</td>
					<td>'.$studentInfo['WebSAMSRegNo'].'</td>
					<td colspan="2" width="44%" class="toRight">班別:</td>
					<td width="16%">&nbsp;&nbsp;&nbsp;'.$studentInfo['ClassName'].'</td>
				</tr>
				<tr>
					<td>姓名:</td>
					<td>'.$studentInfo['ChineseName'].'</td>
					<td width="22%">'.$studentInfo['EnglishName'].'</td>
					<td width="22%" class="toRight">學號:</td>
					<td>&nbsp;&nbsp;&nbsp;'.$studentInfo['ClassNumber'].'</td>
				</tr>
				<tr>
					<td colspan="5" style="font-size:11ex;">&nbsp;</td>
				</tr>
			</table>';
			
			$stu_info_table .= $header_bottom_line;
			
			return $stu_info_table;
	}
	
	public function returnStudentMeritTable(){
		$meritCatArr = $this->getStudentMeritRecords();
		$stu_merit_table = '';
		
		$task = $this->getObjVariable('task');
		if($task != 'sptss_3'){
			$stu_merit_table .= '<span class="title">記功記錄</span>';
			$col_title = '活動記錄';
		}
		else{
			$col_title = '活動/比賽/職位';
		}
		$stu_merit_table .=	'<table width="100%" class="contentTable">
								<thead>
									<tr>
										<th width="16%">類別</th>
										<th width="30%">'.$col_title.'</th>
										<th width="18%">參與程度</th>
										<th width="18%">獎項/成就</th>
										<th class="merit" width="6%">優點</th>
										<th class="merit" width="6%">小功</th>
										<th class="merit" width="6%">大功</th>
									</tr>
								</thead>
								<tbody>';
		if(!empty($meritCatArr)){
			$array_category_order=array();
			for($i=1;$i<=50;$i++){
				array_push($array_category_order,$i);
			}
			array_push($array_category_order,0);
			$countItem = 0;
			
			$displayCat_old = '';
			foreach((array)$array_category_order as $category_order_id){
				foreach((array) $meritCatArr as $cat_id => $merit){
					if($cat_id == $category_order_id){
						$countItem += count($merit);
						$_count = 0;
						foreach((array) $merit as $_infoAry ){
							if($_count==0){
								$displayCat = $_infoAry['OLECatName'];
							}
							else{
								$displayCat = '';
							}
							$stu_merit_table .=		'<tr>
														<td>'.(($displayCat_old == $_infoAry['OLECatName']) ? '' : $displayCat).'</td>
														<td>'.$_infoAry['ItemName'].'</td>
														<td>'.$_infoAry['Performance'].'</td>
														<td>'.$_infoAry['Achievement'].'</td>
														<td class="merit">'.EmptyContentAsEmptyString($_infoAry['1']).'</td>
														<td class="merit">'.EmptyContentAsEmptyString($_infoAry['2']).'</td>
														<td class="merit">'.EmptyContentAsEmptyString($_infoAry['3']).'</td>
													</tr>';
							$displayCat_old = $_infoAry['OLECatName'];
							$_count++;
						}
						break;
					}
				}
			}
			$stu_merit_table .= '<tr>'; 
			$stu_merit_table .= '<td colspan="7">&nbsp;&nbsp;記功項目: 共 '.$countItem.' 項</td>';
			$stu_merit_table .= '</tr>';
		}
		else{
			$stu_merit_table .= '<tr>'; 
			$stu_merit_table .= '<td colspan="7">&nbsp;&nbsp;沒有記功記錄</td>';
			$stu_merit_table .= '</tr>';
		}
		$stu_merit_table .= '<tr><td></td></tr>';
		$stu_merit_table .=	'</tbody>
							</table>';
		return $stu_merit_table;
	}
	
	public function returnStudent3YearMeritTable(){
//		$merit = $this->getStudentMeritRecords();
		$meritAssoArr = $this->getStudentAYMeritRecords();
		$StudentYearsArr = $this->getStudentYears();
		
//		$stu_merit_table = '<span class="title">記功記錄</span>';
		$stu_merit_table .= '<table width="100%" class="contentTable">
								<thead>
									<tr>
										<th width="16%">&nbsp;&nbsp;類別</th>
										<th width="30%">活動/比賽/職位</th>
										<th width="18%">參與程度</th>
										<th width="18%">獎項/成就</th>
										<th class="merit" width="6%">優點</th>
										<th class="merit" width="6%">小功</th>
										<th class="merit" width="6%">大功</th>
									</tr>
								</thead>
								<tbody>';
			$stu_merit_table .= '<tr><td></td></tr>';
			$stu_merit_table .= '<tr><td></td></tr>';
		foreach((array) $StudentYearsArr as $_yearID => $_yearInfoAry ){
			$stu_merit_table .= '<tr>';
			$stu_merit_table .= '<tr>';
			$stu_merit_table .= '<td class="highlight">'.$_yearInfoAry['AcademicYear'].'</td>';
			$stu_merit_table .= '<td class="highlight">'.$_yearInfoAry['ClassName'].'</td>';
			$stu_merit_table .= '</tr>';
			
			$array_category_order=array();
			for($i=1;$i<=50;$i++){
				array_push($array_category_order,$i);
			}
			array_push($array_category_order,0);
			$countItem = 0;
			
			$displayCat_old = '';
			foreach((array)$array_category_order as $category_order_id){
				foreach((array) $meritAssoArr[$_yearID] as $__catID => $__catMeritAry ){
					if($__catID == $category_order_id){
						if($__catID!=='countItem'){
							$countItem += count($__catMeritAry);
							$___count = 0;
							foreach((array) $__catMeritAry as $___meritInfoAry ){
								if($___count==0){
									$displayCat = $___meritInfoAry['OLECatName'];
								}
								else{
									$displayCat = '';
								}
								$stu_merit_table .=		'<tr class="pageBreakAvoid">
															<td>&nbsp;&nbsp;'.(($displayCat_old == $___meritInfoAry['OLECatName']) ? '' : $displayCat).'</td>
															<td class="pageBreakAvoid">'.$___meritInfoAry['ItemName'].'</td>
															<td class="pageBreakAvoid">'.$___meritInfoAry['Performance'].'</td>
															<td class="pageBreakAvoid">'.$___meritInfoAry['Achievement'].'</td>
															<td class="merit pageBreakAvoid">'.EmptyContentAsEmptyString($___meritInfoAry['1']).'</td>
															<td class="merit pageBreakAvoid">'.EmptyContentAsEmptyString($___meritInfoAry['2']).'</td>
															<td class="merit pageBreakAvoid">'.EmptyContentAsEmptyString($___meritInfoAry['3']).'</td>
														</tr>';
								$displayCat_old = $___meritInfoAry['OLECatName'];
								$___count ++;
							}
						}
					}
				}
			}
			$displayNumber = ($meritAssoArr[$_yearID]['countItem'] > 0 ? $meritAssoArr[$_yearID]['countItem']: 0);
			$stu_merit_table .= '<tr>'; 
			$stu_merit_table .= '<td colspan="7">&nbsp;&nbsp;記功項目: 共 '.$displayNumber.' 項</td>';
			$stu_merit_table .= '</tr>';
			$stu_merit_table .= '<tr><td></td></tr>';
		}						
		$stu_merit_table .=	'</tbody>
							</table>';
		$stu_merit_table .= $this->returnDashedLine();
		return $stu_merit_table;
	}
	
	public function returnStudent3YearSumMeritTable(){
		
		$meritAssoArr = $this->getStudentAYMeritRecords();
		$StudentYearsArr = $this->getStudentYears();
		
		$stu_merit_table = '<span class="title">記功數目</span>
							<table width="100%" class="contentTable">';
		foreach((array) $StudentYearsArr as $_thisAcademicYearID => $_infoAry ){
			$displayNumber = ($meritAssoArr[$_thisAcademicYearID]['countItem'] > 0 ? $meritAssoArr[$_thisAcademicYearID]['countItem']: 0);
			$stu_merit_table .=		'<tr>
										<td width="10%" class="highlight">'.$_infoAry['AcademicYear'].'</td>
										<td class="highlight">'.$_infoAry['ClassName'].'</td>
										<td>記功項目: 共 '.$displayNumber.' 項</td>
									</tr>';
		}
		$stu_merit_table .=	'</table>';
		return $stu_merit_table;
	}
	
	public function returnStudentDemeritTable(){
		$demerit = $this->getStudentDemeritRecords();
		$digested_demerit = $this->getDigestedMeritArray($demerit);
		$countItem = count($digested_demerit);
		$stu_demerit_table = '';
		//$stu_demerit_table .= '<div style="page-break-inside: avoid;">';
		$stu_demerit_table .= '<div class="pageBreakAvoid">';
		$stu_demerit_table .= '<span class="title">記過記錄</span>
							<table width="100%" class="contentTable">
								<thead>
									<tr>
										<th>&nbsp;&nbsp;記過原因</th>
										<th class="merit" width="6%">缺點</th>
										<th class="merit" width="6%">小過</th>
										<th class="merit" width="6%">大過</th>
									</tr>
								</thead>
								<tbody>';
			if($countItem > 0){
				foreach((array)$digested_demerit as $_reason => $_typeArr){
					
					$stu_demerit_table .= '<tr>
											<td>&nbsp;&nbsp;'.$_reason.'</td>
											<td class="merit">'.EmptyContentAsEmptyString($_typeArr['-1']).'</td>
											<td class="merit">'.EmptyContentAsEmptyString($_typeArr['-2']).'</td>
											<td class="merit">'.EmptyContentAsEmptyString($_typeArr['-3']).'</td>
										</tr>';
				}
				$stu_demerit_table .= '<tr>'; 
				$stu_demerit_table .= '<td colspan="4">&nbsp;&nbsp;記過項目: 共 '.$countItem.' 項</td>';
				$stu_demerit_table .= '</tr>';
			}
			else{
				$stu_demerit_table .= '<tr>'; 
				$stu_demerit_table .= '<td colspan="4">&nbsp;&nbsp;沒有記過記錄</td>';
				$stu_demerit_table .= '</tr>';
			}
		$stu_demerit_table .= '</tbody>
							</table>';
		$stu_demerit_table .= '</div>';
		return $stu_demerit_table;
	}
	
	public function returnStudentCharacterTable($db_mode=false){
		$studentInfo = $this->getStudentInfo();
		
		$academic_year_id = $this->getObjVariable('academicYearID');
		$year = substr(getStartDateOfAcademicYear($academic_year_id[0]),0,4);
		
		if($db_mode==true && !in_array($year,array('2013','2014'))){
			$student_id = $studentInfo['userid'];
			$PC_info = $this->getStudentPC($academic_year_id,'',$student_id);
		}else{
			$WebSAMSNum = $studentInfo['WebSAMSRegNo'];
			$PC_info = $this->getStudentPC($academic_year_id,$WebSAMSNum);
		}
		
		$header = $this->getStudentPCHeader();
		$numHeaderCol = count($header['Down']);
		if($numHeaderCol>0){
			$colWidth = (100/$numHeaderCol).'%';
		}
		$numCsvCol = count($PC_info);
		
		$html = '<span class="title">學生生命素質涵養概覽</span>';
		$html .= $this->returnSpacing(5);
		$html .= '<table align="center" width="97%" class="personalQuality">';
			$html .= '<tr>';
			if($db_mode==true && $year>='2015'){
				$html .= '<td colspan="2">品性</td><td colspan="2">能力</td><td colspan="2">態度</td>';
			}else{
				foreach((array)$header['Top'] as $header_top => $itemArr){
					$colspan = count($itemArr);
					$html .= '<td colspan="'.$colspan.'">'.$header_top.'</td>';
				}
			}
			$html .= '</tr>';
			$html .= '<tr>';
			if($db_mode==true && $year>='2015'){
				$html .= '<td>誠信</td><td>禮貌</td><td>明辨是非 </td><td>人際關係 </td><td>學習態度</td><td>自治</td>';
			}else{
				foreach((array)$header['Down'] as $header_down){
					$html .= '<td width="'.$colWidth.'">'.$header_down.'</td>';
				} 
			}
			$html .= '</tr>';
			$html .= '<tr>';
			for($x=3;$x<$numCsvCol ; $x++){
				$html .= '<td>'.$PC_info[$x].'</td>';
			}
			$html .= '</tr>';					
		$html .= '</table>';
		$html .= $this->returnSpacing(10);	
		$html .= '<span class="remarks">&nbsp;&nbsp;&nbsp;&nbsp;備註：（評分標準： A - Excellent 優良； B - Good 良好； C - Satisfactory 滿意； D - Fair 尚可）</span>';

		return $html;
	}
	
	public function return3YearsStudentCharacterTable($db_mode=false){
		
		$studentInfo = $this->getStudentInfo();
		$StudentYearsArr = $this->getStudentYears();
		
		$student_id=$studentInfo['userid'];
		$WebSAMSNum = $studentInfo['WebSAMSRegNo'];
		
		$header = $this->getStudentPCHeader();
		$numHeaderCol = count($header['Down']);
		if($numHeaderCol ==0){
			$colWidth = '85%';
		}
		else{
			$colWidth = (85/$numHeaderCol).'%';
		}
		
		$html = '<span class="title">學生生命素質涵養概覽</span>';
		$html .= $this->returnSpacing(5);
		$html .= '<table width="80%" class="personalQuality">';
			$html .= '<tr>';
			$html .= '<td></td><td></td>';
			if($db_mode==true){
				$html .= '<td colspan="2">品性</td><td colspan="2">能力</td><td colspan="2">態度</td>';
			}else{
				foreach((array)$header['Top'] as $header_top => $itemArr){
					$colspan = count($itemArr);
					$html .= '<td colspan="'.$colspan.'">'.$header_top.'</td>';
				}
			}
			$html .= '</tr>';
			$html .= '<tr>';
			$html .= '<td width="12%"></td><td></td>';
			if($db_mode==true){
				$html .= '<td>誠信</td><td>禮貌</td><td>明辨是非 </td><td>人際關係 </td><td>學習態度</td><td>自治</td>';
			}else{
				foreach((array)$header['Down'] as $header_down){
					$html .= '<td width="'.$colWidth.'">'.$header_down.'</td>';
				} 
			}
			$html .= '</tr>';
			foreach((array)$StudentYearsArr as $_academicYearID =>$_infoAry){
		
				$year = substr(getStartDateOfAcademicYear($_academicYearID),0,4);
				
				if($db_mode==true && !in_array($year,array('2013','2014'))){
					$PC_info = $this->getStudentPC($_academicYearID,'',$student_id);
				}else{
					$PC_info = $this->getStudentPC($_academicYearID,$WebSAMSNum);
				}
				
				$numCsvCol = count($PC_info);
				
				$html .= '<tr>';
				$html .= '<td class="highlight">'.$_infoAry['AcademicYear'].'</td>';
				$html .= '<td class="highlight">'.$_infoAry['ClassName'].'</td>';
				for($x=3;$x<$numCsvCol ; $x++){
					$html .= '<td>'.$PC_info[$x].'</td>';
				}
				$html .= '</tr>';				
			}
		$html .= '</table>';
		$html .= $this->returnSpacing(10);	
		$html .= '<span class="remarks">&nbsp;&nbsp;&nbsp;備註：（評分標準：A - Excellent 優良；B - Good 良好；C - Satisfactory 滿意；D - Fair 尚可）</span>';
		
		return $html;
	}
	
	public function returnStudentCommentTable(){
		$comments = $this->getStudentComment();
		$StudentYearsArr = $this->getStudentYears(); 
		
		$html = '<span class="title">評語</span>';
		$html .= '<table width="100%" class="contentTable">';
		foreach((array) $StudentYearsArr as $_academicYearID => $_infoAry){
			//$comments[$_academicYearID][1]['Semester']
			if(!empty($comments[$_academicYearID])){	
				$html .= '<tr>';
				$html .= '<td width="10%" class="highlight">'.$_infoAry['AcademicYear'].'</td>';
				$html .= '<td width="5%" class="highlight">'.$_infoAry['ClassName'].'</td>';
				$html .= '<td width="10%">'.'上學期'.'</td>';
				$html .= '<td class="comment">'.$comments[$_academicYearID][1]['Comment'].'</td>';
				$html .= '</tr>';
				if($comments[$_academicYearID][2]['Comment'] != '' && $comments[$_academicYearID][2]['Comment'] != null){
					$html .= '<tr>';
					$html .= '<td></td>';
					$html .= '<td></td>';
					$html .= '<td>'.'下學期'.'</td>';
					$html .= '<td class="comment">'.$comments[$_academicYearID][2]['Comment'].'</td>';
					$html .= '</tr>';
				}
			}
		}
		$html .= '</table>';
		
		return $html;
	}
	
	public function returnEndSection(){
		$endHTML = '<table width="100%" class="alignCenter"><tr><td>-- 完 --</td></tr></table>';
		return $endHTML;
	}
	
	public function returnSpacing($length=15){
		$endHTML = '';
		for($x=1;$x<=$length;$x++){
			$endHTML .= '<br>';
		}
		return $endHTML;
	}
	
	public function returnDashedLine(){
		$DashedLine = 
		'<table class="line" width="100%" style="border-bottom:1px dashed #000;">
			<tr>
				<td style="font-size:1ex;">&nbsp;</td>
			</tr>
		</table>';
		return $DashedLine;
	}
//-- End of Class
}


?>