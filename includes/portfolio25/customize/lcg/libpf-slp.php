<?php
class studentInfo{
	private $objDB;
	private $objFormClassManage; 

	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $uid;
	private $cfgValue;
	
	private $EmptySymbol;
	private $TableTitleBgColor;
	private $SelfAccount_Break;
	private $YearRangeStr;
	
	private $YearClassID;
	
	private $AcademicYearStr;

	private $borderTop;
	private $borderLeft;
	private $borderBottom;
	private $borderRight;

	private $recordNotFindStr;

	public function studentInfo($uid,$academicYearIDArr,$issuedate){
		global $eclass_db,$intranet_db,$ipf_cfg;

		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid; 
		$this->academicYearIDArr = $academicYearIDArr;
		$this->AcademicYearStr = '';
		
		$this->YearClassID = '';
		
		$this->issuedate = $issuedate;

		$this->cfgValue = $ipf_cfg;
		
		$this->EmptySymbol = '';
		$this->SelfAccount_Break = 'BreakHere';
		$this->YearRangeStr ='';
		
		$this->TableTitleBgColor = '#D0D0D0';
		$this->ClassGroupCategoryName = '16_Report Group';
		
		$this->SchoolTitleEn = 'Lung Cheung Government Secondary School';
		$this->SchoolTitleCh = '龍翔官立中學';

		$this->schoolAddress = '九龍黃大仙馬仔坑道1號';

		$this->borderTop =  'border-top: 1px solid black; ';
		$this->borderLeft =  'border-left: 1px solid black; ';
		$this->borderBottom =  'border-bottom: 1px solid black; ';
		$this->borderRight =  'border-right: 1px solid black; ';

		$this->recordNotFindStr = 'No Record Found.';
	}
	


	public function setYearClassID($YearClassID){
		$this->YearClassID = $YearClassID;
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setAcademicYearArr($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	public function setYearRange()
	{
		$this->YearRangeStr = $this->getYearRange($this->AcademicYearAry);
	}

	public function setAcademicYearStr(){
		$this->AcademicYearStr =$this->getAcademicYearArr($this->AcademicYearAry);
	}

	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	public function getUid(){
		return $this->uid;
	}

	public function getborderTop(){
		return $this->borderTop;
	}

	public function getborderLeft(){
		return $this->borderLeft;
	}

	public function borderBottom(){
		return $this->borderBottom;
	}

	public function borderRight(){
		return $this->borderRight;
	}


	private function getYearID($YearName='thisYear')
	{
		if($YearName=='thisYear')
		{
			$YearClassID = $this->YearClassID;
		
			$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
			$sql = "select 
							ClassTitleEN,
							YearID
				    from 
							$YEAR_CLASS 
					where 
							YearClassID =$YearClassID
					";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		else
		{
			$YEAR = $this->Get_IntranetDB_Table_Name('YEAR');
			$sql = "select 
							YearID
				    from 
							$YEAR 
					where 
							YearName =$YearName
					";
			$result = $this->objDB->returnArray($sql);
			$result = current($result);
			
			$yearID = $result['YearID'];
		}
		
		return $yearID;
	}
	
	/***************** Part 1 : Report Header ******************/	
	public function getPart1_HTML()  {
		$header_font_size = '15';
	
		$fontFace = '';
		
		$header_title1 = $this->SchoolTitleEn;
		$header_title2 = $this->SchoolTitleCh;
		$header_title3 = "Student Learning Profile 學生學習概覽<br/>".''.$this->YearRangeStr.'';
		
		$imageResize = '50%';
		
		$x = '';
		$x .= '<table align="center" cellpadding="0" style="width:100%; text-align:center;" border = "0">
					<tr>
						<td><img src="/home/portfolio/profile/report/customizeReport/templates/lcg_slp/schoolLogo.jpg" border="0" height="100"/></td>
					</tr>
					<tr>
						<td ><font face="'.$fontFace.'">'.$header_title1.' '.$header_title2.'</font><br/></td>
				   </tr>';			
			$x .= '<tr><td style="font-size:15px;"><font face="'.$fontFace.'">'.$header_title3.'</font><br/></td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	/***************** End Part 1 ******************/

	
	/***************** Part 2 : Student Details ******************/
	public function getPart2_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	private function getStudentInfo(){
		
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$PORTFOLIO_STUDENT = $this->Get_eClassDB_Table_Name('PORTFOLIO_STUDENT');
		$sql = "Select  
						i.EnglishName as  EnglishName,
						i.ChineseName as ChineseName,
						i.HKID as  HKID,
						DATE_format(i.DateOfBirth, '%Y-%m-%d') as `DateOfBirth`,
						i.Gender as  Gender,
						i.STRN as STRN,
						i.ClassNumber as ClassNumber ,
						i.ClassName as ClassName,
						DATE_format(p.AdmissionDate , '%Y-%m-%d') as `AdmissionDate`						
				From
						$INTRANET_USER as i
						inner join $PORTFOLIO_STUDENT as p on i.userid = p.userid 
				Where
						i.UserID = '".$this->uid."'
						And i.RecordType = '2'
				";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	private function getStudentInfoDisplay($result)
	{		
//		$fontSize = 'font-size:12px';
		$fontSize = '';
		$fontFace = '';
		$td_space = '<td>&nbsp;</td>';	
		$_paddingLeft = 'padding-left:10px;';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		
		$_borderBottom_dot =  'border-bottom: 1px dashed black; ';
		
		$IssueDate = $this->issuedate;
		
		if (date($IssueDate)==0) {
			$IssueDate = $this->EmptySymbol;
		} else {
			$IssueDate = date('Y-n-j',strtotime($IssueDate));
		}
		
		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;
		
		$HKID = $result[0]["HKID"];
		$HKID = ($HKID=='')? $this->EmptySymbol:$HKID;
		
		$Gender = $result[0]["Gender"];		
		if(strtolower($Gender) == 'm'){
			$Gender = '男';
		}elseif(strtolower($Gender) == 'f'){
			$Gender = '女';
		}

		$Gender = ($Gender=='')? $this->EmptySymbol:$Gender;
		
		$RegistrationNo = $result[0]["STRN"];
		$RegistrationNo = ($RegistrationNo=='')? $this->EmptySymbol:$RegistrationNo;
		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;

		$DOB = $result[0]['DateOfBirth'];
		$DOB = ($DOB=='')? $this->EmptySymbol:$DOB;

		$AdmissionDate = $result[0]['AdmissionDate'];
		$AdmissionDate = ($AdmissionDate=='')? $this->EmptySymbol:$AdmissionDate;


		$html = '<table width = "100%" align="center">';
		$html .= '<tr><td>Students Particulars 學生資料 :<br/></td></tr>';
		$html .= '</table>';
		
		$html .= '<table width="100%" cellpadding="2" border="0px" align="center" style="border-collapse: collapse;">
					<col width="16%" />
					<col width="1%" />
					<col width="16%" />
					<col width="13%" />
					<col width="1%" />
					<col width="9%" />
					<tr>
							<td style="vertical-align: top"><font face="'.$fontFace.'" style="'.$fontSize.'" >Name 姓名</font></td>
							<td valign="top"><font face="'.$fontFace.'" style="'.$fontSize.'">:</font></td>
							<td ><font face="'.$fontFace.'" style="'.$fontSize.'">'.$EngName." <br/>(".$ChiName.')</font></td>
							<td style="vertical-align: top"><font face="'.$fontFace.'" style="'.$fontSize.'">ID No 身份證號碼</font></td>
							<td style="vertical-align: top"><font face="'.$fontFace.'" style="'.$fontSize.'">:</font></td>
							<td style="vertical-align: top"><font face="'.$fontFace.'" style="'.$fontSize.'">'.$HKID.'</font></td>
							
					</tr>
					<tr>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">Date of Birth 出生日期</font></td>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">:</font></td>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">'.$DOB.'</font></td>

							<td><font face="'.$fontFace.'" style="'.$fontSize.'">Sex 性別</font></td>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">:</font></td>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">'.$Gender.'</font></td>
					</tr>
					<tr>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">School Name 學校名稱</font></td>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">:</font></td>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">'.$this->SchoolTitleCh.'</font></td>

							<td><font face="'.$fontFace.'" style="'.$fontSize.'">School Code 學校編號</font></td>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">:</font></td>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'">510122</font></td>
					</tr>
					<tr>
							<td><font face=""'.$fontFace.'"" style="'.$fontSize.'">Date of Admission 入學日期 </font></td>
							<td><font face=""'.$fontFace.'"" style="'.$fontSize.'">:</font></td>
							<td colspan="4"><font face=""'.$fontFace.'"" style="'.$fontSize.'">'.$AdmissionDate.'</font></td>
					</tr>
					<tr>
							<td><font face=""'.$fontFace.'"" style="'.$fontSize.'">School Address 學校地址</font></td>
							<td><font face=""'.$fontFace.'"" style="'.$fontSize.'">:</font></td>
							<td colspan="4"><font face="'.$fontFace.'" style="'.$fontSize.'">'.$this->schoolAddress.'</font></td>
					</tr>
					';

		$html .= '</table>';
	
		return $html;
	}
	
	/***************** End Part 2 ******************/
	
	public function getPerformanceAndAwards(){
		$title = 'Performance and Awards 表現及獎項成就';

		$DataArray = array();
//		$DataArray = $this->getAward_Info();
		$DataArray = $this->getAward_Info2();

		$displayColumn = array();

		$displayColumn[] = array('AcademicYearID','Year 學年');
		$displayColumn[] = array('TitleName' , 'Item 項目');

		$html = $this->getAwardContentTable($title, $DataArray, $displayColumn, 'awards');
		return $html;

	}
	
	//array $displayColumn format  ==> first element , DB field name , second column Report Display Title
	private function getAwardContentTable($title, $data, $displayColumn,$type){

		$_borderTop =  $this->getborderTop();
		$_borderLeft =  $this->getborderLeft();
		$_borderBottom =  $this->borderBottom();
		$_borderRight =  $this->borderRight();

		$html_data = '';
		for($d = 0,$d_max = count($data);$d < $d_max; $d++){
			$_year = $data[$d]['YEAR_NAME'];
			$_item = $data[$d]['TitleName'];
			$_award = $data[$d]['Achievement'];
			$html_data .= '<tr>
							<td width="10%" style="vertical-align: left;">'.$_year.'</td>
							<td style="vertical-align: left;">'.$_item.'</td>
							<td style="vertical-align: left;">'.$_award.'</td>
						   </tr>';
		}

		if($html_data == ''){
			return '';
		}else{

		$html .= '<table width="100%" border="1px" style = "'.$_borderTop.$_borderLeft.$_borderRight.$_borderBottom.'"  style="border-collapse: collapse;" cellpadding="0" >
					<tr><td>
					<table width="100%" cellpadding="0" border="0px" align="center" style="border-collapse: collapse;">
					<tr>				 							
						<td  colspan="3" style="vertical-align: top;" align="Center"><b><u>'.$title.'</u></b></td>
					</tr>
					<tr>
						<td width="15%" style="vertical-align: top;" align="left"><u>Year 學年</u></td>
						<td style="vertical-align: top;" align="left"><u>Item 項目</u></td>
						<td style="vertical-align: top;" align="left"><u>Award 獎項/成就</u></td>						
					</tr>
						'.$html_data.'

					</table>
					</td></td>
		</table>';				  			  
		}
		return $html;
	}
	
	public function getServices(){
		$title = 'Services 服務';
//		$DataArray = $this->getService_Info();
		$DataArray = $this->getService_Info2();

		$html = $this->getServicesContentTable($title, $DataArray, $displayColumn, 'services');
		return $html;
	}
	private function getService_Info2(){
		$categoryID = 4; //Service
		$limitRecord = 5;

		$returnData = $this->getFormOLE($categoryID,$limitRecord);
		return $returnData;
	}

	private function getService_Info(){
		global $ipf_cfg;
		$service_student = $this->Get_eClassDB_Table_Name('SERVICE_STUDENT');
		
		$UserID = $this->uid;
		
		if($UserID!='')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$thisAcademicYearArr = array_filter($thisAcademicYearArr);
			$cond_academicYearID = "And s.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}		
		$sql = "Select
						s.ServiceName as 'TitleName',
						s.Role as 'ROLE',
						s.AcademicYearID as 'ACADEMICYEARID',
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' 
				From
						$service_student as s
						inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = s.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc, 
						s.ServiceName asc	,
						s.Role asc
				limit 5
				";

		$dataArray = $this->objDB->returnResultSet($sql);
//error_log($sql."    e:".mysql_error()."<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		return $dataArray;
	}

		//array $displayColumn format  ==> first element , DB field name , second column Report Display Title
	private function getServicesContentTable($title, $data, $displayColumn,$type){


		$_borderTop =  $this->getborderTop();
		$_borderLeft =  $this->getborderLeft();
		$_borderBottom =  $this->borderBottom();
		$_borderRight =  $this->borderRight();

		$html_data = '';

		for($d = 0,$d_max = count($data);$d < $d_max; $d++){
			$_name = $data[$d]['TitleName'];
			$_year = $data[$d]['YEAR_NAME'];
			$_role = $data[$d]['ROLE'];

			$html_data .= '<tr><td style="vertical-align: left;">'.$_year.'</td><td style="vertical-align: left;">'.$_name.'</td><td>'.$_role.'</td></tr>';
		}

		if($html_data == ''){
			return '';
		}else{
			$html .= '<table width="100%" border="1px" style = "'.$_borderTop.$_borderLeft.$_borderRight.$_borderBottom.'"  style="border-collapse: collapse;" cellpadding="0" >
					<tr><td>
					<table width="100%" cellpadding="0" border="0px" align="center" style="border-collapse: collapse;">
					<tr>				 							
						<td  colspan="3" style="vertical-align: top;" align="Center"><b><u>'.$title.'</u></b></td>
					</tr>
					<tr>
						<td style="vertical-align: top;" align="left"><u>Year 學年</u></td>
						<td style="vertical-align: top;" align="left"><u>Item 項目</u></td>
						<td style="vertical-align: top;" align="left"><u>Post 職務</u></td>
						'.$html_data.'
					</tr>
					</table>
					</td></td>
		</table>';				  			 
			/*
			$html = '<table>';
			$html .= '<tr><td colspan="3">'.$title.'</td></tr>';
			$html .= '<tr><td>Year 學年</td><td>Item 項目</td><td>Post 職務</td></tr>';
			$html .= $html_data;
			$html .= '</table>';
			*/
		}
		return $html;
	}


	public function getOLE(){
		$title = 'Major Other Learning Experiences 其他學習經歷摘要';
		$dataArray = array();
		$dataArray = $this->getOLE_Info();		
		$html = $this->getOLEContentTable($title , $dataArray);
		return $html;
	}

	private function getOLE_Info(){
				global $ipf_cfg;
		$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$OLE_PROG = $this->Get_eClassDB_Table_Name('OLE_PROGRAM'); 

		$academicYearIDArr = array_filter($this->academicYearIDArr);
		
		if($academicYearIDArr!='')
		{
			$cond_academicYearID = "And p.AcademicYearID IN ('".implode("','",(array)$academicYearIDArr)."')";
		}

		$sql = 'select 
				p.title as `TITLE` ,
				s.role as `ROLE`,
				p.Organization as `ORGANIZATION`,
				y.'.Get_Lang_Selection('YearNameB5','YearNameEN').' as `YEAR_NAME` ,
				p.ele as `ELE` from '.$OLE_STD.' as s 
				inner join '.$OLE_PROG.' as p on p.ProgramID = s.ProgramID 
				left join ACADEMIC_YEAR as y on y.AcademicYearID= p.AcademicYearID
				where 1 '.
				$cond_academicYearID.
				' and s.UserID = '.$this->uid.
				' and p.intext = \'int\' 
				  and SLPOrder >=0 
				  and s.RecordStatus in(2,4)'.
				 'order by y.Sequence asc,p.title asc limit 30
		       ';
		$result = $this->objDB->returnResultSet($sql);
//hdebug_r($sql);
		return $result;

	}

	private function getOLEContentTable($title , $dataArray){
		


		$_borderTop =  $this->getborderTop();
		$_borderLeft =  $this->getborderLeft();
		$_borderBottom =  $this->borderBottom();
		$_borderRight =  $this->borderRight();


		$headerAry = array();
		$headerAry[] = 'Item <br/>項目';
		$headerAry[] = 'School Year <br/>學年';
		$headerAry[] = 'Contents <br/>內容';
		$headerAry[] = 'Role of Participation <br/>參與角色';
		$headerAry[] = 'Organisation <br/>合作機構';

		$ole_eleItemCode = array();
		$ole_eleItemCode[] = array('MCE','Moral and Civic Education <br/>德育及公民教育');
		$ole_eleItemCode[] = array('AD','Aesthetic Development <br/>藝術發展');
		$ole_eleItemCode[] = array('PD','Physical Development <br/>體育發展');
		$ole_eleItemCode[] = array('CS','Community Service <br/>社會服務');
		$ole_eleItemCode[] = array('CE','Career-related Experiences <br/>與工作有關經驗');
		$ole_eleItemCode[] = array('OTHERS','Others <br/>其他');
		
		$ole_eleCodeAll = array('MCE','AD','PD','CS','CE','OTHERS');
		
		$activity_show_count = 0;
		$activity_show_limit = 30;
		
		$html = '';
		$html .= '<table width="100%" border="0px" style = "'.$_borderTop.$_borderLeft.$_borderRight.$_borderBottom.'"  style="border-collapse: collapse;" cellpadding="0" >';

		$html .= '<tr><td colspan="5" align="center" style=""><br/><b><u>Major Other Learning Experiences 其他學習經歷摘要</u><b/><br/><br/></td></tr>';
		$html .= '<tr>';
		for($i = 0,$i_max = count($headerAry); $i < $i_max; $i++){
			$html .= '<td align="center" style="'.$_borderBottom.'">'.$headerAry[$i].'</td>';
		}	
		$html .='</tr>';

		$dataGroupByEleAry= array();

		for($j = 0,$j_max = count($dataArray);$j < $j_max && $activity_show_count < $activity_show_limit; $j++){
			$_studentEle = $dataArray[$j]['ELE'];

			// data for $_studentEle like '[MCE]' , such that remove "[" , "]" for the compare
			$_studentEle = str_replace("[", "", $_studentEle);
			$_studentEle = str_replace("]", "", $_studentEle);
			$_studentEle = strtoupper($_studentEle);
			$_studentEle = explode(',',$_studentEle);
			$_studentEle = array_intersect($_studentEle, $ole_eleCodeAll);
//hdebug_r($_studentEle);
			
			for($i=0,$i_max=count($_studentEle);$i<$i_max&&$activity_show_count<$activity_show_limit;$i++){
				$dataGroupByEleAry[$_studentEle[$i]][] = $dataArray[$j];
				$activity_show_count++;
			}
		}
//hdebug_r($dataGroupByEleAry);
$recordFoundInGroupOLE = 0;
			for($i = 0,$i_max = count($ole_eleItemCode);$i < $i_max; $i++){
				$_eleCode = $ole_eleItemCode[$i][0];
				$_eleName = $ole_eleItemCode[$i][1];

				$_studentGroupData = $dataGroupByEleAry[$_eleCode];
//debug_r($_studentGroupData);
				if(is_array($_studentGroupData) && (count($_studentGroupData) > 0) ){
					$recordFoundInGroupOLE = 1;
					$j_max = count($_studentGroupData);
					for($j = 0; $j< $j_max; $j++){

						$_BottomStyle = ($j == $j_max -1)? $_style = $_borderBottom :'' ;

						$_year = $_studentGroupData[$j]['YEAR_NAME'];
						$_title = $_studentGroupData[$j]['TITLE'];
						$_role = $_studentGroupData[$j]['ROLE'];
						$_organization = $_studentGroupData[$j]['ORGANIZATION'];
						//$_ele = $_studentGroupData[$j]['ELE'];  // get this is useless in this loop
						$html .= '<tr>';
						if($j ==0){
							$html .= '<td rowspan="'.$j_max.'" align="center" style="'.$_borderRight.$_borderBottom.'">'.$_eleName.'</td>';
						}
						$html .= '<td style="vertical-align: top; '.$_BottomStyle.'">'.$_year.'</td>';
						$html .= '<td style="vertical-align: top; '.$_BottomStyle.'">'.$_title.'</td>';
						$html .= '<td style="vertical-align: top; '.$_BottomStyle.'; text-align:center">'.$_role.'</td>';
						$html .= '<td style="vertical-align: top; '.$_BottomStyle.'">'.$_organization.'</td>';
						$html .= '</tr>';
					}				
				}
			}

		$html .= '</table>';
		if($recordFoundInGroupOLE == 0){
// 			$html .= '<tr><td colspan="5" align="center">No Record Found.</td></tr>';
		    $html = '';
		}
	


	//	return $h_table1;
		return $html;
	}
	

	private function getAward_Info2(){
		$categoryID = 5; //Award
		$limitRecord = 7;

		$returnData = $this->getFormOLE($categoryID,$limitRecord);
		return $returnData;
	}

	private function getFormOLE($categoryID, $limitRecord,$returnRecordSeletedByStudentOnly = true,$orderBySequence = ''){
				global $ipf_cfg, $UserID;
//		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		$ole_program = $this->Get_eClassDB_Table_Name('OLE_PROGRAM');
		$ole_category = $this->Get_eClassDB_Table_Name('OLE_CATEGORY');
		$ole_student = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
		$academic_year = $this->Get_IntranetDB_Table_Name('ACADEMIC_YEAR');
		
		$_UserID = $this->uid;
		
		if($_UserID != '')
		{
			$cond_studentID = "And UserID ={$_UserID}";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$thisAcademicYearArr = array_filter($thisAcademicYearArr);
			$cond_academicYearID = " And y.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$cond_seletedByStudent =  ($returnRecordSeletedByStudentOnly)? '  and (s.SLPOrder is not null or s.SLPOrder != \'\')' : '' ;

		$sqlOrderBy = ($orderBySequence == '')?' y.Sequence, p.title ': $orderBySequence;

		$sql = 'select
					y.'.Get_Lang_Selection('YearNameB5','YearNameEN').' as `YEAR_NAME` ,
					p.title as `TitleName`,
					s.Role as `ROLE`,
					s.Achievement as `Achievement`
				from 
					'.$ole_category.' as ole_cat 
					inner join '.$ole_program.' as p on ole_cat.recordid = p.category
					inner join '.$ole_student.' as s on s.ProgramID = p.ProgramID
					inner join '.$academic_year.' as y on y.AcademicYearID= p.AcademicYearID
				where 
					ole_cat.recordid = '.$categoryID.'
					'.$cond_studentID.'
					'.$cond_academicYearID.'
					and s.RecordStatus in(2,4)
					'.$cond_seletedByStudent.'
				order by 
					'.$sqlOrderBy.'
				limit '.$limitRecord.' ';

		$returnData = $this->objDB->returnResultSet($sql);
		return $returnData;

	}
	private function getAward_Info(){
		
		global $ipf_cfg;
		$Award_student = $this->Get_eClassDB_Table_Name('AWARD_STUDENT');
		
		$UserID = $this->uid;
		
		if($UserID!='')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}	
		$thisAcademicYearArr = $this->academicYearIDArr;
		
		if($thisAcademicYearArr!='')
		{
			$thisAcademicYearArr = array_filter($thisAcademicYearArr);
			$cond_academicYearID = " And a.AcademicYearID IN ('".implode("','",(array)$thisAcademicYearArr)."')";
		}
		
		$sql = "Select
						a.AwardName AS TitleName,
						a.Details,
						a.AcademicYearID,
						y.".Get_Lang_Selection('YearNameB5','YearNameEN')." as 'YEAR_NAME' 
				From
						$Award_student as a
						inner join ".$this->intranet_db.".ACADEMIC_YEAR as y on y.AcademicYearID = a.AcademicYearID
				Where
						1	
						$cond_studentID	
						$cond_academicYearID
				order by
						y.Sequence desc,
						a.Details asc		
				limit 7
				";
		$returnData = $this->objDB->returnResultSet($sql);
//error_log($sql."    e:".mysql_error()."<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		return $returnData;
	}
	
	/***************** End Part 4 ******************/
	
	public function getStudentSelf_Account(){
		
		$Part7_Title = "Student's Self-account 學生自述";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();

		$html = '';
		$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
		
		return $html;
		
	}
	

	public function getFooter($displaySign, $displayIssueDate){

		$_borderTop =  $this->getborderTop();
		$_borderLeft =  $this->getborderLeft();
		$_borderBottom =  $this->borderBottom();
		$_borderRight =  $this->borderRight();
//style="vertical-align: top;'.$_borderBottom.'"
		$issueDate = $this->issuedate;
		
		if($displaySign && $displayIssueDate){
			$html = <<<HTML
				<table width="100%" border="0px">
					<tr>
						<td width="26%" style="{$_borderBottom}" align="Center">&nbsp;</td>
						<td>&nbsp;</td>
						<td width="26%" align="Center" style="{$_borderBottom}">&nbsp;</td>
						<td>&nbsp;</td>
						<td width="26%" align="Center" style="{$_borderBottom}">{$issueDate}&nbsp;</td>
					</tr>
					<tr>
						<td align="Center">Principal 校長&nbsp;</td>
						<td>&nbsp;</td>
						<td align="Center">Class Teacher 班主任&nbsp;</td>
						<td>&nbsp;</td>
						<td align="Center">Date of Issue 簽發日期&nbsp;</td>
					</tr>
				</table>
HTML;
		}
		else if(!$displaySign && $displayIssueDate){
			$html = <<<HTML
				<table width="100%" border="0px">
					<tr>
						<td width="26%" align="Center">&nbsp;</td>
						<td>&nbsp;</td>
						<td width="26%" align="Center">&nbsp;</td>
						<td>&nbsp;</td>
						<td width="26%" align="Center" style="{$_borderBottom}">{$issueDate}&nbsp;</td>
					</tr>
					<tr>
						<td align="Center">&nbsp;</td>
						<td>&nbsp;</td>
						<td align="Center">&nbsp;</td>
						<td>&nbsp;</td>
						<td align="Center">Date of Issue 簽發日期&nbsp;</td>
					</tr>
				</table>
HTML;
		}
		else if($displaySign && !$displayIssueDate){
			$html = <<<HTML
				<table width="100%" border="0px">
					<tr>
						<td width="26%" style="{$_borderBottom}" align="Center">&nbsp;</td>
						<td>&nbsp;</td>
						<td width="26%" align="Center" style="{$_borderBottom}">&nbsp;</td>
						<td>&nbsp;</td>
						<td width="26%" align="Center">&nbsp;</td>
					</tr>
					<tr>
						<td align="Center">Principal 校長&nbsp;</td>
						<td>&nbsp;</td>
						<td align="Center">Class Teacher 班主任&nbsp;</td>
						<td>&nbsp;</td>
						<td align="Center">&nbsp;</td>
					</tr>
				</table>
HTML;
		}
		else{
			$html = '';
		}
		
		return $html;

	}
	private function get_Self_Account_Info(){
		
		global $ipf_cfg;
		$self_acc_std = $this->Get_eClassDB_Table_Name('SELF_ACCOUNT_STUDENT');
		
		$UserID = $this->uid;
		
		if($UserID!='')
		{
			$cond_studentID = "And UserID ='$UserID'";
		}	
		$cond_DefaultSA = "And DefaultSA is not null";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnResultSet($sql);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		

		//$fontFace= 'Arial';
		$fontFace= '';
		//$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;" '; 

		$html = '';

		$rowMax_count = count($DataArr);
		if($rowMax_count > 0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];
			$Detail = strip_tags($Detail);

			// [2020-0605-0911-21207]
			if(trim($Detail) != '')
            {
                $html .= '<table width="100%" height="200px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
                        <tr>				 							
                            <td  style="font-size:15px;height:30px;vertical-align: top;'.$_borderTop.$_borderLeft.$_borderRight.'" align="Center"><font face="'.$fontFace.'"><b><u>'.$MainTitle.'</u></b></font></td>						 														
                        </tr>
                        <tr>				 							
                            <td style="vertical-align: top;'.$_borderLeft.$_borderBottom.$_borderRight.'"><font face="Arial">'.$Detail.'</font></td>						 														
                        </tr>
                      </table>';
            }
		}
		/*
		else
		{
			$Detail = 'No Record Found.';
		}*/	

		return $html;
	}

	public function getYearRange($academicYearIDArr)
	{
		$returnArray =  $this->getAcademicYearArr($academicYearIDArr);
		
		sort($returnArray);
		
		$lastNo = count($returnArray)-1;
		
		$firstYear = substr($returnArray[0], 0, 4);
		$lastYear = substr($returnArray[$lastNo], 0, 4);
		# 20200131 (Philips) [2020-0123-1005-57207] - Should always display last year
// 		if($firstYear==$lastYear){
			$lastYear = (int)$lastYear + 1;
// 		}
		
		$returnValue = $firstYear."-".$lastYear;
		
		return $returnValue;
		
	}
	
	private function getAcademicYearArr($academicYearIDArr)
	{
		$returnArray = array();
		for($i=0,$i_MAX=count($academicYearIDArr);$i<$i_MAX;$i++)
		{
			if($academicYearIDArr[$i]=='')
			{
				continue;
			}
			$thisAcademicYear = getAcademicYearByAcademicYearID($academicYearIDArr[$i],$ParLang='en');
			$returnArray[$academicYearIDArr[$i]] = $thisAcademicYear;
		}
		
		return $returnArray;
	}
	
	public function getEmptyTable($Height='',$Content='&nbsp;') {
		
		$html = '';
		$html .= '<table width="100%" height="'.$Height.'" cellpadding="0" border="0px" align="center" style="">					
		   			 <tr>
					    <td style="text-align:center"><font face="Arial">'.$Content.'</font></td>
					 </tr>
				  </table>';
		return $html;
	}
	public function getPagebreakTable() {
		
		$Page_break='page-break-after:always;';
		
		$html = '';
		$html .= '<table width="90%" cellpadding="0" border="0px" align="center" style="'.$Page_break.'">					
		   			 <tr>
					    <td></td>
					 </tr>
				  </table>';
		return $html;
	}
	
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}




	/////////2 nd report related function /////


	/***************** Part 1 : Report Header ******************/	
	public function get2ndReportHeaderHTML($displayReportHeader='')  {		
		$header_font_size = '15';
	
		$fontFace = '';
		
		$header_title1 = $this->SchoolTitleEn;
		$header_title2 = $this->SchoolTitleCh;
		$header_title3 = $this->YearRangeStr." Student Learning Profile 學生學習概覽<br/>";
		
		$imageResize = '50%';
				$fontSize = 'font-size:20px';
//<font face="'.$fontFace.'" style="'.$fontSize.'" >		
		$x = '';
		if($displayReportHeader){
			$x .= '<table align="center" cellpadding="0" style="width:100%; text-align:center;" border = "0">
						<tr>
							<td align="center"><img src="/home/portfolio/profile/report/customizeReport/templates/lcg_slp/schoolLogo.jpg" border="0" height="100"/></td>
							<td ><font face="'.$fontFace.'" style="'.$fontSize.'" ><b>'.$header_title1.' '.$header_title2.'</b></font><br/><br/><font face="'.$fontFace.'" style="'.$fontSize.'" ><b/>'.$header_title3.'</b></font></td>
					   </tr>';
			$x .= '</table>'."\r\n";
		}else{
			$x .= '<table align="center" cellpadding="0" style="width:100%; text-align:center;" border = "0">
						<tr>
							<td style="height:104px">&nbsp;</td>
					   </tr>
						<tr>
							<td><font face="'.$fontFace.'" style="'.$fontSize.'; font-weight:bold" >'.$header_title3.'</font></td>
					   </tr>';
			$x .= '</table>'."\r\n";
		}
		return $x;
	}

	public function get2ndReportStudentDetailsHTML(){
		$result = $this->getStudentInfo();




		$EngName = $result[0]["EnglishName"];
		$EngName = ($EngName=='')? $this->EmptySymbol:$EngName;
		
		$ChiName = $result[0]["ChineseName"];
		$ChiName = ($ChiName=='')? $this->EmptySymbol:$ChiName;

		
		$ClassNumber = $result[0]["ClassNumber"];
		$ClassNumber = ($ClassNumber=='')? $this->EmptySymbol:$ClassNumber;
		
		$ClassName = $result[0]["ClassName"];
		$ClassName = ($ClassName=='')? $this->EmptySymbol:$ClassName;


		$html = '<table width="100%"><tr><td>Name 姓名 : '.$EngName.'('.$ChiName.')</td><td>Class 班別 : '.$ClassName.'</td><td>Class No 學號 : '.$ClassNumber.'</td></tr></table>';
		return $html;
	}	




	public function get2ndReportStudentPersonalInfoHtml(){
		$rs = $this->get2ndReportStudentPersonalInfoFromStorage();

	}

	private function get2ndReportStudentPersonalInfoFromStorage(){
	}

	public function get2ndReportExtraCurricularActivitesHtml(){
		$rs = $this->get2ndReportExtraCurricularActivitesFromStorage();

		$title = 'Extra Curricular Activities 聯課活動';
		$columnAry[]  = 'Item 項目';		
		$columnAry[]  = 'Post 職務';

		$h_table = $this->get2ndReportOLERelatedSecionTableTemplate($title , $columnAry);
		
		$h_studentData = '';
		
		if(count($rs)>0){
			for($i = 0, $i_max = count($rs);$i<$i_max ; $i++){
				$h_studentData .= '<tr><td valign="top">'.$rs[$i]['TitleName'].'&nbsp;</td><td align="left" valign="top">'.$rs[$i]['ROLE'].'&nbsp;</td></tr>';
			}
			$h_table = str_replace('<!--student data here-->',$h_studentData,$h_table);
		}else{
// 			$h_studentData = '<tr><td align="center" colspan="2">'.$this->recordNotFindStr.'</td></tr>';
			$h_table = '';
		}

		//combine the template with data
	

		return $h_table;
	}
	private function get2ndReportExtraCurricularActivitesFromStorage(){
		$categoryID = 4; //Service
		//$categoryID = 2; 
		$limitRecord = 8;

		$returnData = $this->getFormOLE($categoryID,$limitRecord,$returnRecordSeletedByStudent = false,$orderBy = ' p.StartDate ');
		return $returnData;
	}



	public function get2ndReportPerformanceAndAwardsHtml(){
		$rs = $this->get2ndReportPerformanceAndAwardsFromStorage();

		$title = 'Performance and Awards 表現及獎項 / 成就';
		$columnAry[]  = 'Item 項目';		
		$columnAry[]  = 'Award 成就';

		$h_table = $this->get2ndReportOLERelatedSecionTableTemplate($title , $columnAry);
		
		$h_studentData = '';
		
		if(count($rs)){
			for($i = 0, $i_max = count($rs);$i<$i_max ; $i++){
				$h_studentData .= '<tr><td valign="top">'.$rs[$i]['TitleName'].'&nbsp;</td><td valign="top">'.$rs[$i]['Achievement'].'&nbsp;</td></tr>';
			}
			
			//combine the template with data
			$h_table = str_replace('<!--student data here-->',$h_studentData,$h_table);
			
		}else{
// 			$h_studentData = '<tr><td align="center" colspan="2">'.$this->recordNotFindStr.'</td></tr>';
		    $h_table = '';
		}

		
		return $h_table;
	}
	
	private function get2ndReportPerformanceAndAwardsFromStorage(){
		
		$categoryID = 5; //Award
//		$categoryID = 2; 
		$limitRecord = 15;

		$returnData = $this->getFormOLE($categoryID,$limitRecord,$returnRecordSeletedByStudent = false);
		return $returnData;

	}


	public function get2ndReportOtherHtml(){
		$rs = $this->get2ndReportOtherFromStorage();


		$h_table .= '<table class="oleRecordTable">';
		$h_table .= '<tr><td class="oleSectionTitle">Others 其他</td></tr>';


		$h_table .= '<tr><td>';

		$h_table .= '<table width = "100%">';		
		$h_table .= '<!--student data here-->';
		$h_table .= '</table>';

		$h_table .= '</td></tr>';
		$h_table .= '</table>';



		$lastTrTagIsClose = true;
		$allTdBlockisFilled = true;

		if(count($rs) > 0){
			for($i = 0, $i_max = count($rs); $i<$i_max ; $i++){
				if($i % 2 == 0){

					$h_studentData .= '<tr>';
					$lastTrTagIsClose = false;
					$allTdBlockisFilled = false;
				}


				$h_studentData .= '<td width="30%" align="center" valign="top">( '.$rs[$i]['TitleName'].'</td><td width="20%" valign="top">'.$rs[$i]['Achievement'].' )</td>';
				//$h_studentData = '<tr><td>('.$rs[$i]['TitleName'].'</td><td>'.$rs[$i]['Achievement'].'</td></tr>';
				if($i % 2 == 1){

					$h_studentData .= '</tr>';
					$lastTrTagIsClose = true;
					$allTdBlockisFilled = true;
				}

			}
			
			if($allTdBlockisFilled){
				//do nothing
			}else{
				$h_studentData .= '<td width="35%">&nbsp;</td><td width="15%">&nbsp;</td>';
			}

			if($lastTrTagIsClose){
				//do nothing
			}else{
				$h_studentData .= '</tr>';
			}
			
			$h_table = str_replace('<!--student data here-->',$h_studentData,$h_table);
		}else{
// 			$h_studentData = '<tr><td align="center" colspan="2">'.$this->recordNotFindStr.'</td></tr>';
		    $h_table = '';
		}
		

// 		$h_table = str_replace('<!--student data here-->',$h_studentData,$h_table);

		return $h_table;

	}
	private function get2ndReportOtherFromStorage(){
		$categoryID = 7; //附件 Attachment
		//$categoryID = 2;
		$limitRecord = 99999; // around to (MAX) for a student

		$returnData = $this->getFormOLE($categoryID,$limitRecord,$returnRecordSeletedByStudent = false);
		return $returnData;

	}
	

	public function get2ndReportFooter($displaySign, $displayIssueDate){



		$_borderTop =  $this->getborderTop();
		$_borderLeft =  $this->getborderLeft();
		$_borderBottom =  $this->borderBottom();
		$_borderRight =  $this->borderRight();
//style="vertical-align: top;'.$_borderBottom.'"
		$issueDate = $this->issuedate;

		$classTeacherStr = $this->getStudentClassTeacherName();
		
		//$principleName = Get_Lang_Selection('廖綵煥先生', 'Mr MR LIU CHOY WOON');
		$principleName = $this->getPrincipalName();

		if($displaySign && $displayIssueDate){
			$html = <<<HTML
			<table width="100%" border="0px">
				<tr>
					<td width="26%" style="{$_borderBottom}" align="Center">&nbsp;</td>
					<td>&nbsp;</td>
					<td width="26%" align="Center" style="{$_borderBottom}">&nbsp;</td>
					<td>&nbsp;</td>
					<td width="26%" align="Center"style="{$_borderBottom}">{$issueDate}&nbsp;</td>
				</tr>
				<tr>
					<td align="Center" valign="top">Principal 校長&nbsp;
						<br/>{$principleName}<br/>
					</td>
					<td>&nbsp;</td>
					<td align="Center" valign="top">Class Teacher 班主任&nbsp;<br/>{$classTeacherStr}</td>
					<td>&nbsp;</td>
					<td align="Center" valign="top">Date of Issue 簽發日期&nbsp;</td>
				</tr>
			</table>
HTML;
		}
		else if(!$displaySign && $displayIssueDate){
			$html = <<<HTML
			<table width="100%" border="0px">
				<tr>
					<td width="26%" align="Center">&nbsp;</td>
					<td>&nbsp;</td>
					<td width="26%" align="Center">&nbsp;</td>
					<td>&nbsp;</td>
					<td width="26%" align="Center" style="{$_borderBottom}">{$issueDate}&nbsp;</td>
				</tr>
				<tr>
					<td align="Center" valign="top">&nbsp;</td>
					<td>&nbsp;</td>
					<td align="Center" valign="top">&nbsp;</td>
					<td>&nbsp;</td>
					<td align="Center" valign="top">Date of Issue 簽發日期&nbsp;</td>
				</tr>
			</table>
HTML;
		}
		else if($displaySign && !$displayIssueDate){
			$html = <<<HTML
			<table width="100%" border="0px">
				<tr>
					<td width="26%" style="{$_borderBottom}" align="Center">&nbsp;</td>
					<td>&nbsp;</td>
					<td width="26%" align="Center" style="{$_borderBottom}">&nbsp;</td>
					<td>&nbsp;</td>
					<td width="26%" align="Center">&nbsp;</td>
				</tr>
				<tr>
					<td align="Center" valign="top">Principal 校長&nbsp;
						<br/>{$principleName}<br/>
					</td>
					<td>&nbsp;</td>
					<td align="Center" valign="top">Class Teacher 班主任&nbsp;<br/>{$classTeacherStr}</td>
					<td>&nbsp;</td>
					<td align="Center" valign="top"></td>
				</tr>
			</table>
HTML;
		}
		else{
			$html = '';
		}
		
		return $html;

	}
	private function get2ndReportOLERelatedSecionTableTemplate($title , $columnAry){
		$h_table = '';		
		$h_table .= '<table class="oleRecordTable" border="0">';
		$h_table .= '<tr><td class="oleSectionTitle">'.$title.'</td></tr>';
		$h_table .= '<tr><td>';

		$h_table .= '<table align="Center" width="85%" border="0">';
		$h_table .= '<tr><td width="65%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>'.$columnAry[0].'</u></td><td align="left" width="35%"><u>'.$columnAry[1].'</u></td></tr>';
		$h_table .= '<!--student data here-->';
		$h_table .= '</table>';

		$h_table .= '</td></tr>';
		$h_table .= '</table>';
		return $h_table;

	}

	//return Type =1 , Teacher name concat with ',' (String)
	//return type = 2, Teacher name in array format (Array)
	private function getStudentClassTeacherName($returnType = 1){


			$objFormClassManage = new form_class_manage();
			$studentStudentStudyingClass = $objFormClassManage->Get_Student_Studying_Class($this->uid);

			$objYearClass = new year_class($studentStudentStudyingClass,true,true);

			$classTeacherList = $objYearClass->ClassTeacherList;
			$classTeacherStr = '';

			for($i = 0, $i_max = count($classTeacherList) ; $i < $i_max ; $i++) {			
				$teacherName[] = $classTeacherList[$i]['TeacherName'];
			}

			if(count($teacherName) > 0){
				$classTeacherStr = implode(', ',$teacherName);
			}
			if($returnType == 1){
				return $classTeacherStr;
			}else{
				return $teacherName;
			}
	}

	private function getPrincipalName() {
		$INTRANET_USER = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		$sql = "Select EnglishName, ChineseName,Gender From $INTRANET_USER Where TitleEnglish = 'Principal' OR TitleChinese = '校長'";
		$result = $this->objDB->returnResultSet($sql);
		$Gender = $result[0]['Gender']== "M" || $result[0]['Gender'] == ''? Get_Lang_Selection('先生','Mr '): Get_Lang_Selection('女士','Ms. ');
		
		return Get_Lang_Selection($result[0]['ChineseName'].$Gender, $Gender.$result[0]['EnglishName']);
	}
}
?>