<?php

function compareHours($a, $b)
{
  return $a['Hours'] - $b['Hours'];
}

function compareScore($a, $b)
{
  return $a['Score'] - $b['Score'];
}


class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $academcYearDisplay; // store the academic Year display in the student Info
	private $uid;
	private $cfgValue;
	private $requireELE; // record the client require ELE type
	private $eleNameAry; // store the ELE display Name for the reprot
	private $Forms;
	private $OLEConsolidatedResults;
	private $SubjectConsolidatedResults;
	private $YearTermID;
	public $ELEID;

	public function studentInfo($uid){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid;
		$this->cfgValue = $ipf_cfg;
		$this->setRequireELE();

		$this->setELENameFromDB();
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	
	public function setYearTerm($YearTermID){
		$this->YearTermID = $YearTermID;
	
	}
	
	private function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	private function setELENameFromDB(){
		$requireELE = $this->getRequireELE();


		$eleStr = "";
		for($i = 0,$i_max = sizeof($requireELE);$i< $i_max;$i++){
			$_eleName = $requireELE[$i];

			$eleStr .= "'{$_eleName}',";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$eleStr = substr($eleStr,0,-1);
		$sql = "select DefaultID,concat(ChiTitle,' (',EngTitle,')') as 'eleName'  from ".$this->eclass_db.".OLE_ELE where defaultid in({$eleStr})";
		$result = $this->objDB->returnArray($sql);

		for($i=0,$i_max = sizeof($result);$i < $i_max;$i++){
			$_eleID = $result[$i]["DefaultID"];
			$_eleNAME = $result[$i]["eleName"];
			$ele[$_eleID] = $_eleNAME;
		}
		$this->eleNameAry = $ele;

	}
	private function getELENameDetails($eleType){
		return $this->eleNameAry[$eleType];
	}
	private function getELENameAry(){
		return $this->eleNameAry;
	}
	private function getRequireELE(){
		return $this->requireELE;
	}
	private function setRequireELE(){
		$_ary[]="[MCE]";
		$_ary[]="[AD]";
		$_ary[]="[PD]";
		$_ary[]="[CS]";
		$_ary[]="[CE]";
		$this->requireELE = $_ary;
	}
	public function getUid(){
		return $this->uid;
	}
	/*************************/
	/*******STUDENT INFO******/
	/*************************/
	public function getStudentInfo_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	public function getStudentInfo(){
		$sql = "select " .
					"iu.EnglishName," .
					"iu.ChineseName," .
					"DATE_FORMAT(iu.DateOfBirth, '%d/%m/%Y') AS DateOfBirth," .
					"iu.ClassName," .
					"iu.ClassNumber," .
					"iu.Gender,".
					"iu.BarCode,".
					"iu.STRN,".
					"iu.HKID,".
					"DATE_FORMAT(uextra.AdmissionDate, '%d/%m/%Y') AS AdmissionDate,".
					"substring(iu.WebSAMSRegNo from 2) as 'WebSAMSRegNo' " .
				" from " .
					$this->intranet_db.".INTRANET_USER AS iu LEFT JOIN ".$this->intranet_db.".INTRANET_USER_PERSONAL_SETTINGS AS uextra ON uextra.UserID=iu.UserID " .
				" where " .
					"iu.userid =".$this->uid." AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";

		$result = $this->objDB->returnArray($sql);

		return $result;
	}
	
	
	public function getELE()
	{
		$sql = "SELECT if(RecordStatus=2, DefaultID, CONCAT('[', RecordID, ']')) AS ELEID, ChiTitle FROM ".$this->eclass_db.".OLE_ELE ";
		$rows = $this->objDB->returnArray($sql);
		
		return $rows;
	}
	
	
	public function getCategory()
	{
		$sql = "SELECT RecordID, ChiTitle FROM ".$this->eclass_db.".OLE_CATEGORY ";
		$rows = $this->objDB->returnArray($sql);
		
		return $rows;
	}
	
	
	//return OLE result for both INT /EXT
	public function getOLE()
	{
		if (is_array($this->AcademicYearAry))
		{
			$AcademicYearID = $this->AcademicYearAry[0];
		} else
		{
			$AcademicYearID = $this->AcademicYearAry;
		}
		
		$condYearAndTerm = " AND op.AcademicYearID='{$AcademicYearID}' ";
		if (trim($this->YearTermID)<>"")
		{
			$condYearAndTerm .= " AND op.YearTermID='".$this->YearTermID."' ";
		}
		
		
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";

		$sql = "select " .
						"op.Title , " .
						"op.StartDate, " .
						"op.EndDate,".						
						"os.Role,".
						"os.Hours,".
						"os.Organization,".
						"op.ELE,".
						"os.Remark,".
						"op.AcademicYearID,".
						"op.Category,".
						"op.ProgramType ".
					"from " .
						$this->eclass_db.".OLE_STUDENT as os " .
						"inner join " .$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid ".
					"where " .
						"os.userid = ".$this->getUid()." ". $condApprove . " AND op.INTEXT='INT' {$condYearAndTerm} " .
						" order by op.ELE, op.Title ";

		//case handle for INT / EXT
		$result = $this->objDB->returnArray($sql);
		
		return $result;
		
	}
	
	
	function getClubEnrolment()
	{
		if (is_array($this->AcademicYearAry))
		{
			$AcademicYearID = $this->AcademicYearAry[0];
		} else
		{
			$AcademicYearID = $this->AcademicYearAry;
		}
		
		$condYearAndTerm = " AND g.AcademicYearID='{$AcademicYearID}' ";
		if (trim($this->YearTermID)<>"")
		{
			$condYearAndTerm .= " AND egi.Semester='".$this->YearTermID."' ";
		}
		
		$sql = "select " .
						"gs.GroupID, ug.CommentStudent , if(g.TitleChinese IS NULL OR g.TitleChinese='', g.Title, g.TitleChinese) AS GroupTitle, r.Title as RoleTitle, egi.Semester ".
					" from " .
						" INTRANET_ENROL_GROUPSTUDENT AS gs" .
						" LEFT JOIN INTRANET_USERGROUP AS ug on (ug.GroupID=gs.GroupID and ug.UserID=gs.StudentID) " .
						" LEFT JOIN INTRANET_GROUP As g on g.GroupID=gs.GroupID " .
						" LEFT JOIN INTRANET_ROLE as r on r.RoleID=ug.RoleID " .
						" LEFT JOIN INTRANET_ENROL_GROUPINFO as egi on (egi.GroupID=gs.GroupID and egi.EnrolGroupID=gs.EnrolGroupID)".
					" where " .
						" gs.studentid='".$this->getUid()."' and gs.recordstatus='2' {$condYearAndTerm} " .
						" order by GroupTitle ";

		$result = $this->objDB->returnArray($sql);
		
		return $result;
	}
	
	

	public function getSelfAccount(){
		$sql = "select sas.Details as Details from ".$this->eclass_db.".SELF_ACCOUNT_STUDENT as sas where Userid = ".$this->getUid()." and instr(DefaultSA,'SLP')>0 limit 1";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	public function setAcademicYearDisplay($academcYearDisplay){
		$this->academcYearDisplay = $academcYearDisplay;
	}
	public function getAcademicYearDisplay(){
		return $this->academcYearDisplay;
	}
	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	private function specialHandleStrn($strn){

			//USER request to replace the last character of strn to XXX
			$pattern = '/^(.*).{3}$/';
			$replacement = '${1}XXX';
			$strn = preg_replace($pattern,$replacement,$strn);
			return $strn;
	}
	

}
?>