<?php

function compareHours($a, $b)
{
	// [2015-0310-1120-00164] ording: Marks > Title in alphabetical order
	// return $a['Hours'] - $b['Hours'];
	return $a['Hours'] == $b['Hours']? strcmp(strtoupper($b['Title']), strtoupper($a['Title'])) : $a['Hours'] - $b['Hours'];
}

// [2015-0310-1120-00164] ording: Marks > Year > Title in alphabetical order 
function compareYearHours($a, $b)
{	
	// get the last year for sorting
	$a_year = $a['YearNameEN'];
	$a_pos = strpos($a_year, "<br />");
	if($a_pos !== false){
		$a_year = substr($a_year,($a_pos+6));
	}
	$b_year = $b['YearNameEN'];
	$b_pos = strpos($b_year, "<br />");
	if($b_pos !== false){
		$b_year = substr($b_year,($b_pos+6));
	}
	
	return $a['Hours'] == $b['Hours']? ($a_year == $b_year? strcmp(strtoupper($b['Title']), strtoupper($a['Title'])) : strcmp($a_year, $b_year)) : $a['Hours'] - $b['Hours'];	
}

function compareScore($a, $b)
{
  return $a['Score'] - $b['Score'];
}

// [2015-0310-1120-00164] ording: Title
function compareRoleName($a, $b)
{
  return strcmp(trim(strtoupper($b)), trim(strtoupper($a)));
}

class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $academcYearDisplay; // store the academic Year display in the student Info
	private $uid;
	private $cfgValue;
	private $requireELE; // record the client require ELE type
	private $eleNameAry; // store the ELE display Name for the reprot
	private $Forms;
	private $OLEConsolidatedResults;
	private $SubjectConsolidatedResults;
	public $ELEID;

	public function studentInfo($uid){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid;
		$this->cfgValue = $ipf_cfg;
		$this->setRequireELE();

		$this->setELENameFromDB();
	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	private function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	private function setELENameFromDB(){
		$requireELE = $this->getRequireELE();


		$eleStr = "";
		for($i = 0,$i_max = sizeof($requireELE);$i< $i_max;$i++){
			$_eleName = $requireELE[$i];

			$eleStr .= "'{$_eleName}',";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$eleStr = substr($eleStr,0,-1);
		$sql = "select DefaultID,concat(ChiTitle,' (',EngTitle,')') as 'eleName'  from ".$this->eclass_db.".OLE_ELE where defaultid in({$eleStr})";
		$result = $this->objDB->returnArray($sql);

		for($i=0,$i_max = sizeof($result);$i < $i_max;$i++){
			$_eleID = $result[$i]["DefaultID"];
			$_eleNAME = $result[$i]["eleName"];
			$ele[$_eleID] = $_eleNAME;
		}
		$this->eleNameAry = $ele;

	}
	private function getELENameDetails($eleType){
		return $this->eleNameAry[$eleType];
	}
	private function getELENameAry(){
		return $this->eleNameAry;
	}
	private function getRequireELE(){
		return $this->requireELE;
	}
	private function setRequireELE(){
		$_ary[]="[MCE]";
		$_ary[]="[AD]";
		$_ary[]="[PD]";
		$_ary[]="[CS]";
		$_ary[]="[CE]";
		$this->requireELE = $_ary;
	}
	public function getUid(){
		return $this->uid;
	}
	/*************************/
	/*******STUDENT INFO******/
	/*************************/
	public function getStudentInfo_HTML(){
		$result = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($result);
		return $html;
	}	
	public function getStudentInfo(){
		$sql = "select " .
					"iu.EnglishName," .
					"iu.ChineseName," .
					"DATE_FORMAT(iu.DateOfBirth, '%d/%m/%Y') AS DateOfBirth," .
					"iu.ClassName," .
					"iu.ClassNumber," .
					"iu.Gender,".
					"iu.BarCode,".
					"iu.HKID,".
					"DATE_FORMAT(uextra.AdmissionDate, '%d/%m/%Y') AS AdmissionDate,".
					"substring(iu.WebSAMSRegNo from 2) as 'WebSAMSRegNo' " .
				" from " .
					$this->intranet_db.".INTRANET_USER AS iu LEFT JOIN ".$this->intranet_db.".INTRANET_USER_PERSONAL_SETTINGS AS uextra ON uextra.UserID=iu.UserID " .
				" where " .
					"iu.userid =".$this->uid." AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";

		$result = $this->objDB->returnArray($sql);

		return $result;
	}
	
	
	public function displayOLE($FORM_CODE, $ELE_ROW, $TopTotal=3){
		
		$RecordSummaryObj = $this->OLEConsolidatedResults[$FORM_CODE][$ELE_ROW];
		
		if (is_array($RecordSummaryObj) && sizeof($RecordSummaryObj)>0)
		{
			$counter = 0;
			$other_hours = 0;
			$total_hours = 0;

			for ($i=sizeof($RecordSummaryObj)-1; $i>=0; $i--)
			{
				$counter ++;
				
				if ($counter<=$TopTotal)
				{
					$RecordHtml .= "<tr>
	                <td>".$RecordSummaryObj[$i]["Title"]."</td>
	                <td class=\"col_role\">".$RecordSummaryObj[$i]["Role"]."</td>
	                <td class=\"col_marks\">".$RecordSummaryObj[$i]["Hours"]."</td>
	              </tr>";
				} else
				{
					$other_hours += $RecordSummaryObj[$i]["Hours"];
				}
				$total_hours += $RecordSummaryObj[$i]["Hours"];
			}
			
			// [2015-0310-1120-00164] hide if $other_hours = 0
			if($other_hours != 0){
				$RecordHtml .= "
	              <tr class=\"others_row\">
	                <td><em>Marks from Other Activities</em></td>
	                <td class=\"col_role\">-</td>
	                <td class=\"col_marks\">{$other_hours}</td>
	              </tr>";
			} else {
				$RecordHtml .= "
	              <tr style='height:9px'>
	                <td colspan='3'></td>
	              </tr>";
			}
            $RecordHtml .= "  
              <tr class=\"subtotal_row\">
                <td>&nbsp;</td>
                <td align='center'>Subtotal:</td>
                <td class=\"col_marks\">{$total_hours}</td>
              </tr>";
		} else
		{
			$RecordHtml = "
              <tr class=\"others_row\">
                <td colspan=\"3\"><em>NIL</em></td>
              </tr>
              <tr class=\"subtotal_row\">
                <td colspan=\"3\">&nbsp;</td>
              </tr>";
		}  
              
		
		return $RecordHtml;
	}
	
	public function getAndDisplayRDorAA($FORM_CODE, $ELE_ROW, $TopTotal=10){
		
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";

		
		$IsRD = ($ELE_ROW=="[".$this->ELEID["R&D"]."]");
		
		
		$sql = "select " .
						"op.Title , " .
						"op.StartDate, " .
						"op.EndDate,".						
						"os.Role,".
						"os.Hours,".
						"os.Achievement,".
						"op.ELE,".
						"if (os.Details<>'' AND os.Details is not null, os.Details, op.Details) AS Details,".
						"op.AcademicYearID,".
						"ay.YearNameEN,".
						"op.ProgramType ".
					" from " .
						$this->eclass_db.".OLE_STUDENT as os " .
						"inner join " .$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid " .
						" LEFT JOIN ACADEMIC_YEAR AS ay ON ay.AcademicYearID=op.AcademicYearID ".
					" where " .
						"os.userid = ".$this->getUid()." ". $condApprove . " AND op.ELE like '%".$ELE_ROW."%' ";
		$sql .= ($IsRD) ? " order by ay.YearNameEN asc, os.Hours DESC, op.Title ASC " : " order by os.Hours asc, ay.YearNameEN asc, op.Title ASC ";

		//case handle for INT / EXT
		$result = $this->objDB->returnArray($sql);
		$RecordSummary = array();
		
		$Forms = $this->Forms; //["JUNIOR"]  / ["SENIOR"]
		
		if (!is_array($Forms) || sizeof($Forms)<=0)
		{
			die ("This student has no class history! Therefore, no record can be found!");
		}
		
		$MarkedArr = array();
		
		for ($i=0; $i<sizeof($result); $i++)
		{
			$result_row = $result[$i];
			
			$ELEArray = explode(",", $result_row["ELE"]);
			
			if (in_array($result_row["AcademicYearID"], $Forms["JUNIOR"]))
			{
				$FORM_CODE_now = "JUNIOR";
			} else
			{
				# junior
				$FORM_CODE_now = "SENIOR";
			}
			
			if ($IsRD)
			{
				if (!$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])])
				{			
					$RecordSummary[$FORM_CODE_now][$ELE_ROW][] = array("Title"=>trim($result_row["Title"]), "Role"=>trim($result_row["Role"]), "Hours"=>(float)$result_row["Hours"], "YearNameEN"=>$result_row["YearNameEN"], "Achievement"=>$result_row["Achievement"], "Details"=>$result_row["Details"]);
					$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])] = sizeof($RecordSummary[$FORM_CODE_now][$ELE_ROW]);
					//$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])] = true;
					//debug("added ".$result_row["Title"]." post ". $result_row["Role"]." ", $MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])] );
				} else
				{
					# record of same title and post
					$RecordSummary[$FORM_CODE_now][$ELE_ROW][$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])]-1]["Hours"] += $result_row["Hours"];
					
					//debug("appended ".$result_row["Title"]." post ". $result_row["Role"]." ", $MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])] );
					if (!strstr($RecordSummary[$FORM_CODE_now][$ELE_ROW][$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])]-1]["YearNameEN"], $result_row["YearNameEN"]))
					{	
						// [2015-0310-1120-00164] show "&" before line break
						// $RecordSummary[$FORM_CODE_now][$ELE_ROW][$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])]-1]["YearNameEN"] .= "<br />& ".$result_row["YearNameEN"];
						$RecordSummary[$FORM_CODE_now][$ELE_ROW][$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])]-1]["YearNameEN"] .= " &<br />".$result_row["YearNameEN"];
					}
				}
			} else
			{
				$RecordSummary[$FORM_CODE_now][$ELE_ROW][] = array("Title"=>trim($result_row["Title"]), "Role"=>trim($result_row["Role"]), "Hours"=>(float)$result_row["Hours"], "YearNameEN"=>$result_row["YearNameEN"], "Achievement"=>$result_row["Achievement"], "Details"=>$result_row["Details"]);
			}
			
			
			if ($FORM_CODE_now != "SENIOR" && in_array($result_row["AcademicYearID"], $Forms["SENIOR"]))
			{
				$FORM_CODE_now = "SENIOR";
				
				if ($IsRD)
				{
					if (!$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])])
					{			
						$RecordSummary[$FORM_CODE_now][$ELE_ROW][] = array("Title"=>trim($result_row["Title"]), "Role"=>trim($result_row["Role"]), "Hours"=>(float)$result_row["Hours"], "YearNameEN"=>$result_row["YearNameEN"], "Achievement"=>$result_row["Achievement"], "Details"=>$result_row["Details"]);
						$MarkedArr[$FORM_CODE_now][$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])] = sizeof($RecordSummary[$FORM_CODE_now][$ELE_ROW]);
						//$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])] = true;
					} else
					{
						# record of same title and post
						$RecordSummary[$FORM_CODE_now][$ELE_ROW][$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])]-1]["Hours"] += $result_row["Hours"];
						if (!strstr($RecordSummary[$FORM_CODE_now][$ELE_ROW][$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])]-1]["YearNameEN"], $result_row["YearNameEN"]))
						{
							// [2015-0310-1120-00164] show "&" before line break
							// $RecordSummary[$FORM_CODE_now][$ELE_ROW][$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])]-1]["YearNameEN"] .= "<br />& ".$result_row["YearNameEN"];
							$RecordSummary[$FORM_CODE_now][$ELE_ROW][$MarkedArr[$FORM_CODE_now][trim($result_row["Title"])][trim($result_row["Role"])]-1]["YearNameEN"] .= " &<br />".$result_row["YearNameEN"];
						}
					}
				} else
				{
					$RecordSummary[$FORM_CODE_now][$ELE_ROW][] = array("Title"=>trim($result_row["Title"]), "Role"=>trim($result_row["Role"]), "Hours"=>(float)$result_row["Hours"], "YearNameEN"=>$result_row["YearNameEN"], "Achievement"=>$result_row["Achievement"], "Details"=>$result_row["Details"]);
				}
			}
			
			
		}
		
		
		// [2015-0310-1120-00164] OLE records of category - Awards & Achievements also need to sort
//		if ($IsRD)
//		{
			# need to sort as hours are accumulated for records of same title and post
			# sorting
			if (is_array($RecordSummary) && sizeof($RecordSummary)>0)
			{
				foreach ($RecordSummary as $FORM_CODE_CUR => $ArrayB2)
				{
					if (is_array($ArrayB2) && sizeof($ArrayB2)>0)
					{
						foreach ($ArrayB2 as $ELE_row => $ArrayC3)
						{
							// [2015-0310-1120-00164] use compareYearHours() to sort $RecordSummary
							// usort($RecordSummary[$FORM_CODE_CUR][$ELE_row], 'compareHours');
							usort($RecordSummary[$FORM_CODE_CUR][$ELE_row], 'compareYearHours');
						} 
					}
				} 
			}
//		}
		
		$RecordSummaryObj = $RecordSummary[$FORM_CODE][$ELE_ROW];
		
		if (is_array($RecordSummaryObj) && sizeof($RecordSummaryObj)>0)
		{
			$counter = 0;
			$other_hours = 0;
			$total_hours = 0;

			for ($i=sizeof($RecordSummaryObj)-1; $i>=0; $i--)
			//for ($i=0; $i<sizeof($RecordSummaryObj); $i++)
			{
				$counter ++;
				
				if ($counter<=$TopTotal)
				{
					if ($IsRD)
					{
						$RecordHtml.= "<tr>
		                <td class=\"col_title\">".$RecordSummaryObj[$i]["Title"]."</td>
		                <td class=\"col_year\">".$RecordSummaryObj[$i]["YearNameEN"]."</td>
		                <td class=\"col_post\">".$RecordSummaryObj[$i]["Role"]."</td>
		                <td class=\"col_marks\">".$RecordSummaryObj[$i]["Hours"]."</td>
		              </tr>";
					} else
					{
						$RecordHtml.= "<tr>
			                <td class=\"col_title\">".$RecordSummaryObj[$i]["Title"]."</td>
			                <td class=\"col_year\">".$RecordSummaryObj[$i]["YearNameEN"]."</td>
			                <td class=\"col_activity\">".$RecordSummaryObj[$i]["Details"]."</td>
			                <td class=\"col_award\">".$RecordSummaryObj[$i]["Achievement"]."</td>
			                <td class=\"col_marks\">".$RecordSummaryObj[$i]["Hours"]."</td>
			              </tr>";
					}
				} else
				{
					$other_hours += $RecordSummaryObj[$i]["Hours"];
				}
				$total_hours += $RecordSummaryObj[$i]["Hours"];
			}
			
			if ($IsRD)
			{
				if($other_hours != 0){
					$RecordHtml .= "
						<tr class=\"others_row\">
		                <td class=\"others_row\" ><em>Marks from Other Responsibilities &amp; Duties</em></td>
		                <td class=\"col_year\">-</td>
		                <td>-</td>
		                <td class=\"col_marks\">{$other_hours}</td>
		              </tr>";
				} else {
					$RecordHtml .= "
		              <tr style='height:3px'>
		                <td colspan='4'></td>
		              </tr>";
				}
	            $RecordHtml .= "<tr class=\"subtotal_row\">
	                <td class=\"subtotal_row\">&nbsp;</td>
	                <td class=\"col_year\">&nbsp;</td>
	                <td >Subtotal:</td>
	                <td class=\"col_marks\">{$total_hours}</td>
	              </tr>";
			} else
			{	
				// [2015-0310-1120-00164] hide if $other_hours = 0
				if($other_hours != 0){
					$RecordHtml .= "<tr class=\"others_row\">
		                <td class=\"others_row\" ><em>Marks from Other Awards &amp; Achievements</em></td>
			                <td class=\"col_year\">-</td>
			                <td>-</td>
			                <td>-</td>
		                <td class=\"col_marks\">{$other_hours}</td>
		              </tr>";
	           } else {
					$RecordHtml .= "
		              <tr style='height:3px'>
		                <td colspan='4'></td>
		              </tr>";
				}
              $RecordHtml .= "<tr class=\"subtotal_row\">
                <td class=\"subtotal_row\">&nbsp;</td>
              	<td class=\"col_year\">&nbsp;</td>
                <td>&nbsp;</td>
                <td >Subtotal:</td>
                <td class=\"col_marks\">{$total_hours}</td>
              </tr>";
			}
		} else
		{
			$spanCols = ($IsRD) ? 4 : 5;
			$RecordHtml = "
              <tr class=\"others_row\">
                <td colspan=\"{$spanCols}\"><em>NIL</em></td>
              </tr>
              <tr class=\"subtotal_row\">
                <td colspan=\"{$spanCols}\">&nbsp;</td>
              </tr>";
		}  
              
		
		return $RecordHtml;
	}
	
	
	public function GetRDandAA()
	{
		#get preset school base ELE
		$sql = "SELECT RecordID FROM ".$this->eclass_db.".OLE_ELE where EngTitle='Responsibilities &amp; Duties'";
		$rows = $this->objDB->returnVector($sql);
		$ELEID["R&D"] = $rows[0];
		
		$sql = "SELECT RecordID FROM ".$this->eclass_db.".OLE_ELE where EngTitle='Awards &amp; Achievements'";
		$rows = $this->objDB->returnVector($sql);
		$ELEID["A&A"] = $rows[0];		
		
		$this->ELEID = $ELEID;
				
	}
	
	//return OLE result for both INT /EXT
	public function getOLE()
	{
		
		$condApprove = " and os.RecordStatus in( ".$this->cfgValue["OLE_STUDENT_RecordStatus"]["approved"]." ,".$this->cfgValue["OLE_STUDENT_RecordStatus"]["teacherSubmit"].")";

		$sql = "select " .
						"op.Title , " .
						"op.StartDate, " .
						"op.EndDate,".						
						"os.Role,".
						"os.Hours,".
						"os.Achievement,".
						"op.ELE,".
						"op.Details,".
						"op.AcademicYearID,".
						"op.ProgramType ".
					"from " .
						$this->eclass_db.".OLE_STUDENT as os " .
						"inner join " .$this->eclass_db.".OLE_PROGRAM as op on os.programid = op.programid ".
					"where " .
						"os.userid = ".$this->getUid()." ". $condApprove . " " .
						" order by op.ELE, op.AcademicYearID desc ";


		//case handle for INT / EXT
		$result = $this->objDB->returnArray($sql);
		
		
		
		$RecordSummary = array();
		
		$Forms = $this->Forms; //["JUNIOR"]  / ["SENIOR"]
		
		if (!is_array($Forms) || sizeof($Forms)<=0)
		{
			die ("This student has no class history! Therefore, no record can be found!");
		}
		
		for ($i=0; $i<sizeof($result); $i++)
		{
			$result_row = $result[$i];
			
			$ELEArray = explode(",", $result_row["ELE"]);
			
			if (in_array($result_row["AcademicYearID"], $Forms["JUNIOR"]))
			{
				$FORM_CODE = "JUNIOR";
			} else
			{
				# junior
				$FORM_CODE = "SENIOR";
			}
			
			for ($j=0; $j<sizeof($ELEArray); $j++)
			{
				$ELE_row = trim($ELEArray[$j]);
				
				$IsNew = true;
				for ($k=0; $k<sizeof($RecordSummary[$FORM_CODE][$ELE_row]); $k++)
				{
					if ($RecordSummary[$FORM_CODE][$ELE_row][$k]["Title"]==trim($result_row["Title"]))
					{
						$IsNew = false;
						$record_index = $k;
						break;
					}
				}
				
				if ($IsNew)
				{
					$RecordSummary[$FORM_CODE][$ELE_row][] = array("Title"=>trim($result_row["Title"]), "Role"=>$result_row["Role"], "Hours"=>(float)$result_row["Hours"], "AcademicYearID"=>$result_row["AcademicYearID"], "Achievement"=>$result_row["Achievement"]);
				} else
				{
					$RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Hours"] += $result_row["Hours"];
					$RoleArray = explode("&",  $RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Role"]);
										
					$IsNewRole = true;
					for ($k=0; $k<sizeof($RoleArray); $k++)
					{
						if (trim(strtoupper($RoleArray[$k]))==trim(strtoupper($result_row["Role"])))
						{
							$IsNewRole = false;
							break;
						}
					}
					
					if ($IsNewRole)
					{
						$RoleArray[] = trim($result_row["Role"]);
						// [2015-0310-1120-00164] Sort OLE Role
						if(count($RoleArray) > 1){
							usort($RoleArray, 'compareRoleName');
						}
						$Roles = "";
						for ($k=0; $k<sizeof($RoleArray); $k++)
						{
							//$separator = ($k<sizeof($RoleArray)-1) ? ", " : " & ";
							$separator = " & ";
							$Roles .= (($Roles!="") ? $separator : "" ) . trim($RoleArray[$k]);
						}
						
						$RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Role"] = $Roles;
					} 
					// [2015-0310-1120-00164] Sort OLE Role
					else if(count($RoleArray) > 1)
					{
						usort($RoleArray, 'compareRoleName');
						$Roles = "";
						for ($k=0; $k<sizeof($RoleArray); $k++)
						{
							$separator = " & ";
							$Roles .= (($Roles!="") ? $separator : "" ) . trim($RoleArray[$k]);
						}
						
						$RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Role"] = $Roles;
					}
					
				}
				
				$RecordSummary[$FORM_CODE][$ELE_row."_[HOURS]"] += $result_row["Hours"];
			}
						
			if ($FORM_CODE != "SENIOR" && in_array($result_row["AcademicYearID"], $Forms["SENIOR"]))
			{
				$FORM_CODE = "SENIOR";
				
				for ($j=0; $j<sizeof($ELEArray); $j++)
				{
					$ELE_row = trim($ELEArray[$j]);
					
					$IsNew = true;
					for ($k=0; $k<sizeof($RecordSummary[$FORM_CODE][$ELE_row]); $k++)
					{
						if ($RecordSummary[$FORM_CODE][$ELE_row][$k]["Title"]==trim($result_row["Title"]))
						{
							$IsNew = false;
							$record_index = $k;
							break;
						}
					}
					
					if ($IsNew)
					{
						$RecordSummary[$FORM_CODE][$ELE_row][] = array("Title"=>trim($result_row["Title"]), "Role"=>$result_row["Role"], "Hours"=>(float)$result_row["Hours"], "AcademicYearID"=>$result_row["AcademicYearID"], "Achievement"=>$result_row["Achievement"]);
					} else
					{
						$RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Hours"] += $result_row["Hours"];
						$RoleArray = explode("&",  $RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Role"]);
						
						$IsNewRole = true;
						for ($k=0; $k<sizeof($RoleArray); $k++)
						{
							if (trim(strtoupper($RoleArray[$k]))==trim(strtoupper($result_row["Role"])))
							{
								$IsNewRole = false;
								break;
							}
						}
						
						if ($IsNewRole)
						{
							$RoleArray[] = trim($result_row["Role"]);
							// [2015-0310-1120-00164] Sort OLE Role
							if(count($RoleArray) > 1){
								usort($RoleArray, 'compareRoleName');
							}
							$Roles = "";
							for ($k=0; $k<sizeof($RoleArray); $k++)
							{
								//$separator = ($k<sizeof($RoleArray)-1) ? ", " : " & ";
								$separator = " & ";
								$Roles .= (($Roles!="") ? $separator : "" ) . trim($RoleArray[$k]);
							}
							
							$RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Role"] = $Roles;
						}
						// [2015-0310-1120-00164] Sort OLE Role
						else if(count($RoleArray) > 1)
						{
							usort($RoleArray, 'compareRoleName');
							$Roles = "";
							for ($k=0; $k<sizeof($RoleArray); $k++)
							{
								$separator = " & ";
								$Roles .= (($Roles!="") ? $separator : "" ). trim($RoleArray[$k]);
							}
							
							$RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Role"] = $Roles;
						}
						
					}
					
					$RecordSummary[$FORM_CODE][$ELE_row."_[HOURS]"] += $result_row["Hours"];
				}
			}
			
		}
		
		# sorting
		if (is_array($RecordSummary) && sizeof($RecordSummary)>0)
		{
			foreach ($RecordSummary as $FORM_CODE => $ArrayB2)
			{
				if (is_array($ArrayB2) && sizeof($ArrayB2)>0)
				{
					foreach ($ArrayB2 as $ELE_row => $ArrayC3)
					{
						if (!strstr($ELE_row, "_[HOURS]"))
						{
							usort($RecordSummary[$FORM_CODE][$ELE_row], 'compareHours');
						}
					} 
				}
			} 
		}
		//debug_r($RecordSummary);
		$this->OLEConsolidatedResults = $RecordSummary;
	}
	
	public function GetOLESubtotal($FORM_CODE, $ELE_ROW)
	{
		$xValue = 0;
		$xValue += $this->OLEConsolidatedResults[$FORM_CODE][$ELE_ROW."_[HOURS]"];		 
		 
		return $xValue;
	}
	
	
	public function DetermineAward($mark)
	{
		$awards[] = array("mark"=>1700, "title"=>"Platinum Award");
		$awards[] = array("mark"=>1500, "title"=>"Diamond Award");
		$awards[] = array("mark"=>1200, "title"=>"Gold Award");
		$awards[] = array("mark"=>900, "title"=>"Silver Award");
		$awards[] = array("mark"=>600, "title"=>"Bronze Award");
		$awards[] = array("mark"=>0, "title"=>"NIL");
		
		if ($mark!="")
		{
			for ($i=0; $i<sizeof($awards); $i++)
			{
				if ($mark>=$awards[$i]["mark"])
				{
					$result = $awards[$i]["title"];
					break;
				}
			}
		} else
		{
			$result = $awards[sizeof($awards)-1]["title"];
		}
		
	/*
		1700 marks or above : Platinum Award 
1500 to 1699.9 marks : Diamond Award 
1200 to 1499.9 marks : Gold Award
900 to 1199.9 marks : Silver Award 
600 to 899.9 marks : Bronze Award
*/
		return $result;
	}
	
	
	
	public function displayAcademicPerformancePoint($FORM_CODE, $TopTotal=20){
		
		$RecordSummaryObj = $this->SubjectConsolidatedResults[$FORM_CODE]["SUBJECTS"];
		
		if (is_array($RecordSummaryObj) && sizeof($RecordSummaryObj)>0)
		{
			$counter = 0;
			for ($i=sizeof($RecordSummaryObj)-1; $i>=0; $i--)
			{
				$counter ++;
				
				if ($counter<=$TopTotal)
				{
					$RecordHtml .= "<tr>
	                <td>".$RecordSummaryObj[$i]["EN_DES"]."</td>
	                <td class=\"col_marks\">".$RecordSummaryObj[$i]["Score"]."</td>
	              </tr>";
	              
				}
			}
			
			$RecordHtml .= "
              <tr class=\"subtotal_row\">
              <td class=\"sub_title\" align='right' >Subtotal</td>
                <td class=\"col_marks\">".$this->SubjectConsolidatedResults[$FORM_CODE]["SUBJECTS_[Score]"]."</td>
            </tr>";
		} else
		{
			$RecordHtml = "
              <tr class=\"others_row\">
                <td colspan=\"3\"><em>NIL</em></td>
              </tr>
              <tr class=\"subtotal_row\">
                <td colspan=\"3\">&nbsp;</td>
              </tr>";
		}  
              
		
		return $RecordHtml;
	}
	
	
	public function getAcademicPerformancePoint()
	{
		$sql = "select " .
						"ssp.StudentID , " .
						"ssp.AcademicYearID, " .
						"ssp.TermID,".						
						"ssp.SubjectID,".				
						"subj.EN_DES ,".
						"ssp.Score  ".
					" from " .
						$this->eclass_db.".SLP_STUDENT_SUBJECT_POINTS  as ssp " .
						"LEFT join " .$this->intranet_db.".ASSESSMENT_SUBJECT as subj on subj.RecordID = ssp.SubjectID ".
					" where " .
						"ssp.StudentID='".$this->getUid()."' ".
						" ORDER BY ssp.AcademicYearID desc ";


		//case handle for INT / EXT
		$result = $this->objDB->returnArray($sql);
		
		
		$RecordSummary = array();
		
		$Forms = $this->Forms; //["JUNIOR"]  / ["SENIOR"]
		
		if (!is_array($Forms) || sizeof($Forms)<=0)
		{
			die ("This student has no class history! Therefore, no record can be found!");
		}
		
		for ($i=0; $i<sizeof($result); $i++)
		{
			$result_row = $result[$i];
			
			$ELEArray = explode(",", $result_row["ELE"]);
			
			if (in_array($result_row["AcademicYearID"], $Forms["JUNIOR"]))
			{
				$FORM_CODE = "JUNIOR";
			} else
			{
				# junior
				$FORM_CODE = "SENIOR";
			}
			
			for ($j=0; $j<sizeof($ELEArray); $j++)
			{
				$ELE_row = "SUBJECTS";
				
				$IsNew = true;
				for ($k=0; $k<sizeof($RecordSummary[$FORM_CODE][$ELE_row]); $k++)
				{
					if ($RecordSummary[$FORM_CODE][$ELE_row][$k]["EN_DES"]==trim($result_row["EN_DES"]))
					{
						$IsNew = false;
						$record_index = $k;
						break;
					}
				}
				
				if ($IsNew)
				{
					$RecordSummary[$FORM_CODE][$ELE_row][] = array("EN_DES"=>trim($result_row["EN_DES"]), "SubjectID"=>$result_row["SubjectID"], "Score"=>(int)$result_row["Score"], "AcademicYearID"=>$result_row["AcademicYearID"]);
				} else
				{
					$RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Score"] += $result_row["Score"];					
				}
				
				$RecordSummary[$FORM_CODE][$ELE_row."_[Score]"] += $result_row["Score"];
			}
			
			if ($FORM_CODE != "SENIOR" && in_array($result_row["AcademicYearID"], $Forms["SENIOR"]))
			{
				$FORM_CODE = "SENIOR";
				
				for ($j=0; $j<sizeof($ELEArray); $j++)
				{
					$ELE_row = "SUBJECTS";
					
					$IsNew = true;
					for ($k=0; $k<sizeof($RecordSummary[$FORM_CODE][$ELE_row]); $k++)
					{
						if ($RecordSummary[$FORM_CODE][$ELE_row][$k]["EN_DES"]==trim($result_row["EN_DES"]))
						{
							$IsNew = false;
							$record_index = $k;
							break;
						}
					}
					
					if ($IsNew)
					{
						$RecordSummary[$FORM_CODE][$ELE_row][] = array("EN_DES"=>trim($result_row["EN_DES"]), "SubjectID"=>$result_row["SubjectID"], "Score"=>(int)$result_row["Score"], "AcademicYearID"=>$result_row["AcademicYearID"]);
					} else
					{
						$RecordSummary[$FORM_CODE][$ELE_row][$record_index]["Score"] += $result_row["Score"];					
					}
					
					$RecordSummary[$FORM_CODE][$ELE_row."_[Score]"] += $result_row["Score"];
				}
			}
			
			
		}
		
		if (is_array($RecordSummary) && sizeof($RecordSummary)>0)
		{
			foreach ($RecordSummary as $FORM_CODE => $ArrayB2)
			{
				if (is_array($ArrayB2) && sizeof($ArrayB2)>0)
				{
					foreach ($ArrayB2 as $ELE_row => $ArrayC3)
					{
						if (!strstr($ELE_row, "_[Score]"))
						{
							usort($RecordSummary[$FORM_CODE][$ELE_row], 'compareScore');
						}
					} 
				}
			} 
		}
		
		$this->SubjectConsolidatedResults = $RecordSummary;
	}
	
	
	public function getAcademicPerformancePointSubtotal($FORM_CODE)
	{
		$xValue = 0;
		$ELE_ROW = "SUBJECTS";
		$xValue += $this->SubjectConsolidatedResults[$FORM_CODE][$ELE_ROW."_[Score]"];		 
		 
		return $xValue;
	}
	

	public function getSelfAccount(){
		$sql = "select sas.Details as Details from ".$this->eclass_db.".SELF_ACCOUNT_STUDENT as sas where Userid = ".$this->getUid()." and instr(DefaultSA,'SLP')>0 limit 1";

		$result = $this->objDB->returnArray($sql);
		return $result;
	}
	public function setAcademicYearDisplay($academcYearDisplay){
		$this->academcYearDisplay = $academcYearDisplay;
	}
	public function getAcademicYearDisplay(){
		return $this->academcYearDisplay;
	}
	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	private function specialHandleStrn($strn){

			//USER request to replace the last character of strn to XXX
			$pattern = '/^(.*).{3}$/';
			$replacement = '${1}XXX';
			$strn = preg_replace($pattern,$replacement,$strn);
			return $strn;
	}
	
	function GetClassTeachers($academicYearID)
	{
		
		$sql = "select yc.YearClassID, yct.UserID, iu.EnglishName from YEAR_CLASS As yc, YEAR_CLASS_USER AS ycu, YEAR_CLASS_TEACHER AS yct, INTRANET_USER AS iu " .
				" WHERE yc.AcademicYearID='{$academicYearID}' AND ycu.UserID='".$this->getUid()."' ANd ycu.YearClassID=yc.YearClassID " .
				" AND yct.YearClassID=yc.YearClassID AND iu.UserID=yct.UserID";
		$teacher_rows = $this->objDB->returnResultSet($sql);
		
		$ClassTeacher = $teacher_rows[0]["EnglishName"];
		if (trim($teacher_rows[1]["EnglishName"])!="")
		{
			$ClassTeacher .= " & " . trim($teacher_rows[1]["EnglishName"]);
		}
		
		return $ClassTeacher;
	}
	
	
	
	
	function GetClassHistory($current_form_value)
	{
		
		$sql = "select yc.YearClassID, yc.ClassTitleEN,  yc.AcademicYearID from YEAR_CLASS As yc, YEAR_CLASS_USER AS ycu" .
				"  WHERE ycu.UserID='".$this->getUid()."' ANd ycu.YearClassID=yc.YearClassID";
		$ClassHistory = $this->objDB->returnResultSet($sql);
		
		$FormJunior = array();
		$FormSenior = array();
		for ($i=0; $i<sizeof($ClassHistory); $i++)
		{
			$classhistory_row = $ClassHistory[$i];
			$form_integer = (int) $classhistory_row["ClassTitleEN"];
			if ($form_integer<=3)
			{
				$FormJunior[] = trim($classhistory_row["AcademicYearID"]);
				if ($current_form_value<=3)
				{
					$FormSenior[] = trim($classhistory_row["AcademicYearID"]);
				}
			} else
			{
				$FormSenior[] = trim($classhistory_row["AcademicYearID"]);
			}
		}
		
		$this->Forms["JUNIOR"] = $FormJunior;
		$this->Forms["SENIOR"] = $FormSenior;
	}
	
	
	function GetNumberOfYears()
	{
		//debug_r($this->Forms["JUNIOR"] );
		//debug_r($this->Forms["SENIOR"] );
		return sizeof($this->Forms["SENIOR"]);
	}

}
?>