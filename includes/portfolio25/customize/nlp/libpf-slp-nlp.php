<?php
class objPrinting{
    //-- Start of Class
    
    //	var $EmptyString = '---';
    //	var $AcademicYearName;
    
    private $objDB;
    private $eclass_db;
    private $intranet_db;
    private $cfgValue;
    
    // variable for retrieving all data
    private $task;
    private $yearTermID;
    private $academicYearID;
    private $academicYearName;
    private $studentList;
    private $studentId;
    
    // variable for storing Student Records (associate array of studentID)
    private $Student_info_Ary;
    
    // for 3 years report
    private $reportType;
    var $Student_FormName_Ary;
//     private $Student_Years_Ary;
//     private $Student_comments_Ary;
    //	private $AcademicYear;
    //	private $studentListAry;
    //	private $Student_OLE_Ary;
    //	private $Student_ClassName;
    //	private $yearClassID;
    
    public function __construct($task,$studentList,$reportType=''){
        global $eclass_db,$intranet_db;
        
        // only get current academic year id data
        switch($task){
            case 'nlp_slp_1':
                $academicYearID[0] = Get_Current_Academic_Year_ID();
                break;
        }
       
        $this->setObjVariable('objDB',new libdb());
        $this->setObjVariable('eclass_db',$eclass_db);
        $this->setObjVariable('intranet_db',$intranet_db);
        
        $this->setObjVariable('task',$task);
        $this->setObjVariable('studentList',$studentList);

        $this->setObjVariable('reportType',$reportType);
            // academicYearID set in  getStudentListYears
        $this->setObjVariable('Student_Years_Ary',$this->getStudentListYears());
        $this->setObjVariable('Student_info_Ary', $this->getStudentList_Info() );
        
        $this->getTargetFormReady();
        
    }
    
    private function setObjVariable($VarName, $VarValue){
        $this->{$VarName} = $VarValue;
    }
    private function getObjVariable($VarName){
        return $this->{$VarName};
    }
    private function getStudentRecords($RecordType){

        return $this->{$RecordType}[$this->getStudent()];
    }
    public function setStudent($studentId){
        //		$this->studentId = $studentId;
        $this->setObjVariable('studentId',$studentId);
    }
    public function getStudent(){
        return $this->getObjVariable('studentId');
    }

    //Get Student Info
    private function getStudentList_Info(){
        
        $studentList = $this->getObjVariable('studentList');
        $sql = "select " .
            "iu.userid,
					iu.EnglishName,
					iu.ChineseName,
					DATE_FORMAT(iu.DateOfBirth, '%d/%m/%Y') AS DateOfBirth,
					iu.ClassName,
					iu.ClassNumber,
					iu.Gender,
					iu.BarCode,
					iu.STRN,
					iu.HKID,
					substring(iu.WebSAMSRegNo from 2) as WebSAMSRegNo ,
					iu.userid,
					iu.UserLogin
				from
					".$this->intranet_db.".INTRANET_USER AS iu
				where
					iu.userid IN ('".implode("','",(array)$studentList)."')
					AND (iu.RecordType = '2' OR iu.UserLogin like 'tmpUserLogin_%') ";
        
        $result = $this->objDB->returnResultSet($sql);
        return BuildMultiKeyAssoc($result,'userid');
    }
    public function getStudentInfo(){
        return $this->getStudentRecords('Student_info_Ary');        
    }
    
    private function getTargetFormReady(){
        $sql = 'select ClassTitleEN from YEAR_CLASS where yearclassid = '.$this->yearClassID.'';
        $targetclassnameAry = $this->objDB->returnVector($sql);
        preg_match_all("/\d+\B/", $targetclassnameAry[0], $tempform);
        
        $form = $tempform[0][0];
        $formAry[] = $form;
        if($form > 1){
            $formAry[] = --$form;
        }
        if($form > 1){
            $formAry[] = --$form;
        }
        sort($formAry);
        $this->Student_FormName_Ary = $formAry;
        
        if(count($formAry)>1){
            return true;
        }
    }
    
    private function getStudentListYears(){
        $studentList = $this->getObjVariable('studentList');
        $reportType = $this->getObjVariable('reportType');
        
//         $formAry = $this->Student_FormName_Ary;
        $sql = "select ClassName from INTRANET_USER WHERE userid IN  ('".implode("','",$studentList)."')			";
        $StudentResult = $this->objDB->returnVector($sql);
   
        preg_match_all("/\d+\B/", $StudentResult[0], $tempform);
        $form = $tempform[0][0];
        $formConds = " ";
        $YearConds = " ";
        if($reportType == 'nlp_slp2'){ // ALL YEAR ){
          
            if($form == '6'){
                $formAry= array('4','5','6');
              
            }
            else if($form == '3'){
                $formAry= array('1','2','3');
            }
            else{
                $formAry= array('4','5','6');
            }                       
            $formConds = "  AND pch.ClassName REGEXP '".implode('|',$formAry)."'";
        }else{
            $AcademicYearID = Get_Current_Academic_Year_ID();   
            $YearConds = " AND ay.AcademicYearID = '$AcademicYearID' ";
        }

        $sql = "Select UserID, ClassName, ay.YearNameEN as AcademicYear, pch.AcademicYearID
				From PROFILE_CLASS_HISTORY as pch
					INNER JOIN ACADEMIC_YEAR as ay on pch.AcademicYearID = ay.AcademicYearID
				where
					pch.userid IN  ('".implode("','",$studentList)."')			
	                $formConds
	                $YearConds
				order by pch.AcademicYear asc";
        $result = $this->objDB->returnResultSet($sql);

//         $this->setObjVariable('academicYearID',array_unique(Get_Array_By_Key($result,'AcademicYearID')));
        
        foreach((array) $result as $_key => $_infoAry){
            $_userID = $_infoAry['UserID'];
            $_academicYearID = $_infoAry['AcademicYearID'];
            $userYearsAssoArr[$_userID][$_academicYearID] = $_infoAry;
        } 
        return $userYearsAssoArr;
    }
   

	#
    #####################################################################################################################################
    //------------------------------------------------ BELOW ARE HTML AND CSS RELATED -------------------------------------------------//
    
    public function returnStyleSheet(){
        
        $task = $this->getObjVariable('task');
        
//         table { overflow : wrap; }
        $style = '';
        $style .= '<style>';
        $style .= 'body { font-size:12px; font-family:"droidsansfallback";}				
					th,td {font-size:23ex;}
					span.remarks {font-size:22ex;}           
					.spacing {font-size:30ex;}
              
            
					.headerTable td {font-size:12px;
								  font-family:"droidsansfallback";
								  text-align:center;
                                  font-weight: lighter;
                                }
            
					.footerTable td{font-size:20ex;
								  font-family:"droidsansfallback";
								  }

					.pageBreakAvoid{
						page-break-inside: avoid;
					}
                    .signature {
						border-bottom :1px solid #000; 
					}';
        
      
                $style .= '
                    .contentTable {
                      border-collapse: collapse;
                    }
                    .contentTable td,th {
                      border: 0.5px solid black;
                      font-size:9px;
                      font-weight: lighter;
					  font-family:"droidsansfallback";     
                    }
                    .contentTable th {
                       color:#FFFFFF;
                       font-weight:lighter;					
                    }
                    .selfAccountTable {
              	         font-family:"droidsansfallback";   	  
                    }

                    .selfAccountTable td {                     
                      font-size:12px;

                    }
                    div.tablebreak {
                        height:20px;
                    }   
                  ';
             
        
        $style .= '</style>';
        return $style;
    }
    
    public function returnHeader(){
        
        $studentInfo = $this->getStudentList_Info();
   
      
        return $header;
    }
    
    public function returnFooter(){
        
        $studentInfo = $this->getStudentInfo();
      
        $StudentName = $studentInfo['ChineseName'];
        $StudentID = $studentInfo['userid'];
        $footer = '<table width="100%" class="footerTable" align="center">
			<tr>

				<td width="30%"><barcode code="'.getCurrentAcademicYear().'" height="0.66"></td>
				
				<td width="30%"><barcode code="'.$StudentName.'" height="0.66"></td>
		
                <td width="30%"><barcode code="'.$StudentID.'" height="0.66"></td>
			</tr>

			</table>';
        return $footer;
    }
    
    
    public function returnStudentInfoTable(){
        global $issuedate;
        
        $studentInfo = $this->getStudentInfo();
    
        $stu_info_table= '<table width="100%" class="headerTable" align="center">
    			<tr>
        			<td width="20%" style="text-align:left; font-size: 10px;">English Name: </td>
        			<td width="50%" style="text-align:left; font-size: 10px;">'.$studentInfo['EnglishName'].'</td>
                    <td width="15%" style="text-align:left; font-size: 10px;">Class: </td>
        			<td width="15%" style="text-align:left; font-size: 10px;">'.$studentInfo['ClassName'].'('.$studentInfo['ClassNumber'].')</td>
    			</tr>
    			<tr>
        			<td width="20%" style="text-align:left; font-size: 10px;">Chinese Name: </td>
        			<td width="50%" style="text-align:left; font-size: 10px;">'.$studentInfo['ChineseName'].'</td>
                    <td width="15%" style="text-align:left; font-size: 10px;">Date of Issue:</td>
        			<td width="15%" style="text-align:left; font-size: 10px;">'.$issuedate.'</td>
    			</tr>
    			</table>
               ';
        
        return $stu_info_table;
    
    }
    
    public function returnWholeYearStudentInfoTable(){
        global $issuedate;
        
        $studentInfo = $this->getStudentInfo();
        
        $stu_info_table= '<table width="100%" class="headerTable" align="center">
        			<tr>
            			<td width="20%" style="text-align:left; font-size: 10px;">English Name: </td>
            			<td width="35%" style="text-align:left; font-size: 10px;">'.$studentInfo['EnglishName'].'</td>
                        <td width="13%" style="text-align:left; font-size: 10px;">Class: </td>
            			<td width="17%" style="text-align:left; font-size: 10px;">'.$studentInfo['ClassName'].'('.$studentInfo['ClassNumber'].')</td>
                        <td width="15%" style="text-align:left; font-size: 10px;"></td>
        			</tr>
        			<tr>
            			<td style="text-align:left; font-size: 10px;">Chinese Name: </td>
            			<td colspan="4" style="text-align:left; font-size: 10px;">'.$studentInfo['ChineseName'].'</td>                 
        			</tr>
                    <tr><td><br></td></tr>
                    <tr>
                        <td style="text-align:left; font-size: 10px;">Date of Issue:</td>
            			<td style="text-align:left; font-size: 10px;" colspan="4">'.$issuedate.'</td>
                    </tr>         
                    <tr>
                        <td class="signature" colspan="5">____________________________________________________________________________________________</td>       		
                    </tr>
    			</table>';
        
        return $stu_info_table;
        
    }
    
    public function returnStudentCurrentYearOLETable(){
        global $issuedate;
        
        $studentID = $this->getStudent();
        $thisStudentOLEInfo= $this->getStudentOLERecord($studentID);
     
//         $thisStudentAwardInfo = $studentAwardInfo;
        $OLECount = count($thisStudentOLEInfo);
        $stu_OLE_table = '';
        if($OLECount>0){
            $stu_OLE_table = '<table width="100%" class="contentTable">
                    <thead>
                        <tr style="font-family: droidsansfallback; color: rgb(255, 255, 255); background-color: rgb(187, 187, 187);" nobr="true">
                            <th colspan="3" style="text-align:left; font-size: 13px;">Academic Year '.getCurrentAcademicYear().'</th>
                        </tr>
                        <tr style="font-family: droidsansfallback; font-size: 10px; color: rgb(255, 255, 255); background-color: rgb(136, 136, 136);" nobr="true">
                            <th style="text-align:left;" width="50%">Item</th>
                            <th style="text-align:left;" width="25%">Award / Certifications</th>
                            <th style="text-align:left;" width="25%">Post</th>
                        </tr>
                    </thead>
                    <tbody>';
      
            for($i=0;$i<$OLECount;$i++){
                $stu_OLE_table.='<tr nobr="true">
            			<td width="50%">'.$thisStudentOLEInfo[$i]['Title'].'</td>
            			<td width="25%">'.$thisStudentOLEInfo[$i]['Achievement'].'</td>
                        <td width="25%">'.$thisStudentOLEInfo[$i]['Role'].'</td>      	
        			</tr>';
            }
    
    			
            $stu_OLE_table.='</tbody></table>';
        }

        return $stu_OLE_table;
    }
    
    public function returnStudentAllYearOLETable(){
        global $issuedate;
        
        $studentID = $this->getStudent();
        $thisStudentOLEInfo= $this->getStudentOLERecord($studentID);
      
        //         $thisStudentAwardInfo = $studentAwardInfo;
        $OLECount = sizeof($thisStudentOLEInfo);
        $stu_ole_table = '';
        if($OLECount >0){       
            $stu_ole_table = '<table width="100%" class="contentTable">
                    <thead>
                        <tr style="font-family: droidsansfallback; color: rgb(255, 255, 255); background-color: rgb(187, 187, 187);" >
                            <th colspan="7" style="text-align:left; font-size: 12px;">Other Learning Experiences</th>
                        </tr>
                        <tr style="font-family: droidsansfallback; color: rgb(255, 255, 255); background-color: rgb(136, 136, 136);" nobr="true">
                            <th width="15%" style="text-align:left; font-size: 9px; ">Programmes</th>
                            <th width="21%" style="text-align:left; font-size: 9px; ">Description</th>
                            <th width="10%" style="text-align:left; font-size: 9px; ">School Year</th>
                            <th width="12%" style="text-align:left; font-size: 9px; ">Role of Participation</th>
                            <th width="15%" style="text-align:left; font-size: 9px; ">Programme Co-organizer (if any)</th>
                            <th width="13%" style="text-align:left; font-size: 9px; ">Components of OLE</th>
                            <th width="14%" style="text-align:left; font-size: 9px; ">Awards / Certifications / Achievements* (if any)</th>
                        </tr>
                    </thead>
                    ';
            
            for($i=0;$i<$OLECount;$i++){
                $stu_ole_table .='<tr style="font-size: 10px;" nobr="true">
            			<td width="15%">'.$thisStudentOLEInfo[$i]['Title'].'</td>
            			<td width="21%">'.$thisStudentOLEInfo[$i]['Details'].'</td>
                        <td width="10%">'.$thisStudentOLEInfo[$i]['Year'].'</td>
            			<td width="12%">'.$thisStudentOLEInfo[$i]['Role'].'</td>
            			<td width="15%">'.$thisStudentOLEInfo[$i]['Organization'].'</td>
            			<td width="13%">'.$thisStudentOLEInfo[$i]['CategoryTitle'].'</td>
                        <td width="14%" >'.$thisStudentOLEInfo[$i]['Achievement'].'</td>
        			</tr>';
            }
            
            
            $stu_ole_table .='</table>';
        }
        return $stu_ole_table;
    }
    
    function getStudentOLERecord($studentID){
        
        $AcademicYearID = Get_Current_Academic_Year_ID();     
        $reportType = $this->getObjVariable('reportType');

        if($reportType == 'nlp_slp2'){ // ALL YEAR 
            $AcademicYearIDAry  = $this->getObjVariable('Student_Years_Ary');
            $AcademicYearIDAry = BuildMultiKeyAssoc($AcademicYearIDAry[$studentID],'AcademicYearID','AcademicYearID',1);
            $OLEResult = $this->GET_OLE_SLP($studentID, "INT", $AcademicYearIDAry,true);
        } else if($reportType == 'nlp_slp1'){
            $OLEResult = $this->GET_OLE_SLP($studentID, "INT", (array)$AcademicYearID ,false);
        }
       
      
        return $OLEResult[$studentID];
    }
    
    
    # Get OLE / External Performance
    function GET_OLE_SLP($ParStudentIDArr, $ParIntExt, $ParAcademicYearID=array(),$StudentOrderedRecord=false)
    {
        global $eclass_db, $intranet_db, $lpf_slp,$sys_custom;
        
        # Check by OLE year and term
//         $date_year_field = Get_Lang_Selection("ay.YearNameB5 AS YearName,", "ay.YearNameEN AS YearName,");
        $date_year_field = "ay.YearNameEN AS YearName,";
        $extra_join_table = "INNER JOIN {$intranet_db}.ACADEMIC_YEAR AS ay ON ay.AcademicYearID = op.AcademicYearID";
        $cond = "";
        $cond .= empty($ParAcademicYearID) ? "" : "AND op.AcademicYearID IN (".implode(", ", $ParAcademicYearID).") ";
//         $cond .= empty($ParYearTermID) ? "" : "AND op.YearTermID = ".$ParYearTermID." ";
        $cond .= $StudentOrderedRecord? " AND os.SLPOrder IS NOT NULL ":" ";
        $additionOrder .= $StudentOrderedRecord? " os.SLPOrder, ":" ";
        
        $sql =	"SELECT os.RecordID, os.UserID, op.Title, {$date_year_field} os.Role, ";
        $sql .= "os.Achievement, op.ELE, op.Organization, op.Details AS Remark, os.SLPOrder, op.Details, oc.EngTitle ";
        $sql .= "FROM {$extra_table} {$eclass_db}.OLE_STUDENT AS os ";
        $sql .= "INNER JOIN {$eclass_db}.OLE_PROGRAM AS op ON os.ProgramID = op.ProgramID ";
        $sql .= $extra_join_table." ";
        $sql .= "LEFT JOIN {$eclass_db}.OLE_CATEGORY AS oc ON oc.RecordID = op.Category ";
        $sql .= "WHERE ";
        $sql .= "os.UserID IN ('".implode("','", (array)$ParStudentIDArr)."') AND ";
        $sql .= "os.RecordStatus IN (2,4) AND op.IntExt = '{$ParIntExt}' $cond ";
        //$sql .= "ORDER BY os.StartDate DESC";
        //$sql .= "ORDER BY ay.Sequence ASC , op.StartDate desc";
        $sql .= "ORDER BY $additionOrder os.Title";
 
        $OLEArr = $this->objDB->returnArray($sql);

        for($i=0; $i<count($OLEArr); $i++)
        {
            if(isset($OLEArr[$i]['StartDate']))
            {
                $t_academic_year_arr = getAcademicYearInfoAndTermInfoByDate($OLEArr[$i]['StartDate']);
                $t_academic_year = $t_academic_year_arr[1];
            }
            else
            {
                $t_academic_year = $OLEArr[$i]['YearName'];
            }
            
            $ReturnArr[$OLEArr[$i]['UserID']][] = array	(
                "RecordID" => $OLEArr[$i]['RecordID'],
                "Title" => $OLEArr[$i]['Title'],
                "Year" => $t_academic_year,
                "Role" => $OLEArr[$i]['Role'],
                "ELE" => $OLEArr[$i]['ELE'],
                "Achievement" => $OLEArr[$i]['Achievement'],
                "Organization" => $OLEArr[$i]['Organization'],
                "Remark" => $OLEArr[$i]['Remark'],
                "SLPOrder" => $OLEArr[$i]['SLPOrder'],
                "Details" => $OLEArr[$i]['Details'],
                "CategoryTitle" => $OLEArr[$i]['EngTitle']
            );
        }
        return $ReturnArr;
    }
    
    
    public function returnStudentAllYearAwardTable(){
        global $issuedate;
        
        $studentID = $this->getStudent();
  
        $AcademicYearIDAry  = $this->getObjVariable('Student_Years_Ary');
        $AcademicYearIDAry = BuildMultiKeyAssoc($AcademicYearIDAry[$studentID],'AcademicYearID','AcademicYearID',1);
        
        
        $thisStudentAwardInfo= $this->GET_AWARDS_AND_MAJ_ACHIEVEMENT_SLP($studentID,$AcademicYearIDAry);
       
        //         $thisStudentAwardInfo = $studentAwardInfo;
        $awardCount = sizeof($thisStudentAwardInfo);
        $stu_award_table = '';
        if($awardCount>0){
            $stu_award_table = '<table width="100%" class="contentTable">
                <thead>
                    <tr style="font-family: droidsansfallback; color: rgb(255, 255, 255); background-color: rgb(187, 187, 187);">
                        <th colspan="3" style="text-align:left; font-size: 12px;">List of Awards and Major Achievements Issued by the School</th>
                    </tr>
                    <tr style="font-family: droidsansfallback; font-size: 9px; color: rgb(255, 255, 255); background-color: rgb(136, 136, 136);" nobr="true">
                        <th width="20%" style="text-align:left;"> School Year</th>
                        <th width="50%" style="text-align:left;"> Awards and Achievements*</th>
                        <th width="30%" style="text-align:left;"> Remark</th>
                    </tr>
                </thead>
                ';
            
            for($i=0;$i<$awardCount;$i++){
                $stu_award_table .='<tr style="font-size: 10px;" nobr="true">
        			<td width="20%">'.$thisStudentAwardInfo[$i]['Year'].'</td>
        			<td width="50%">'.$thisStudentAwardInfo[$i]['AwardName'].'</td>
                    <td width="30%" >'.$thisStudentAwardInfo[$i]['Remark'].'</td>
    			</tr>';
            }
            
            
            $stu_award_table .='</table>';
        }
        
        
        return $stu_award_table;
    }
    
    
    # Get awards and major achievement
    function GET_AWARDS_AND_MAJ_ACHIEVEMENT_SLP($studentID, $ParAcademicYearID=array())
    {
        global $intranet_db, $eclass_db;
        
        $sql =	"SELECT DISTINCT UserID, Year, AwardName, Remark, Organization ";
        $sql .= "FROM {$eclass_db}.AWARD_STUDENT ";
        $sql .= "WHERE UserID ='$studentID' AND RecordType = '1' ";
        $sql .= empty($ParAcademicYearID) ? "" : "AND AcademicYearID IN (".implode(", ", $ParAcademicYearID).") ";
        $sql .=	"ORDER BY Year DESC , AwardDate desc";
        
        $AwardArr = $this->objDB->returnArray($sql);
        
        $ReturnArr = array();
        for($i=0; $i<count($AwardArr); $i++)
        {
            $ReturnArr[$AwardArr[$i]['UserID']][] = array	(
                'Year' => $AwardArr[$i]['Year'],
                'AwardName' => $AwardArr[$i]['AwardName'],
                'Remark' => $AwardArr[$i]['Remark'],
                'Organization' => $AwardArr[$i]['Organization']
            );
        }
        
        return $ReturnArr[$studentID];
    }
    
    
    public function returnStudentPerformanceTable(){
        global $issuedate;
        
        $studentID = $this->getStudent();
        
        $AcademicYearIDAry  = $this->getObjVariable('Student_Years_Ary');
        $AcademicYearIDAry = BuildMultiKeyAssoc($AcademicYearIDAry[$studentID],'AcademicYearID','AcademicYearID',1);
        
 
        $StudentPerformanceInfo = $this->GET_OLE_SLP($studentID,"EXT",$AcademicYearIDAry,true);
    
        $thisStudentPerformanceInfo= $StudentPerformanceInfo[$studentID];

        $performanceCount = sizeof($thisStudentPerformanceInfo);
        
        $stu_performance_table = '';
        if($performanceCount>0){
            $stu_performance_table = '<div class="tablebreak"><br></div><table width="100%" class="contentTable">
                <thead>
                    <tr style="font-family: droidsansfallback; color: rgb(255, 255, 255); background-color: rgb(187, 187, 187);">
                        <th colspan="6" style="text-align:left; font-size: 12px;">Performance / Awards and Key Participation Outside School</th>
                    </tr>
                    <tr style="font-family: droidsansfallback; font-size: 9px; color: rgb(255, 255, 255); background-color: rgb(136, 136, 136);" nobr="true">
                        <th width="19%" style="text-align:left;">Programmes</th>
                        <th width="31%" style="text-align:left;">Description</th>
                        <th width="10%" style="text-align:left;">School Year</th>
                        <th width="13%" style="text-align:left;">Role of Participation</th>
                        <th width="13%" style="text-align:left;">Programme Organization (if any)</th>
                        <th width="14%" style="text-align:left;">Awards / Certifications / Achievements* (if any)</th>
                    </tr>
                </thead>
                ';
            
            for($i=0;$i<$performanceCount;$i++){
                $stu_performance_table .='<tr style="font-size: 10px;" nobr="true">
        			<td width="19%">'.$thisStudentPerformanceInfo[$i]['Title'].'</td>
        			<td width="31%">'.$thisStudentPerformanceInfo[$i]['Details'].'</td>
                    <td width="10%" >'.$thisStudentPerformanceInfo[$i]['Year'].'</td>
        			<td width="13%">'.$thisStudentPerformanceInfo[$i]['Role'].'</td>
        			<td width="13%">'.$thisStudentPerformanceInfo[$i]['Organization'].'</td>
                    <td width="14%">'.$thisStudentPerformanceInfo[$i]['Achievement'].'</td>
    			</tr>';
            }
            
            
            $stu_performance_table .='</table>';
        }
        
        
        return $stu_performance_table;
    }
    
    function returnStudentSelfAccountTable(){
        global $PATH_WRT_ROOT;
        
        $studentID = $this->getStudent();
        $AcademicYearID = Get_Current_Academic_Year_ID();     
        
        
        $studentSelfAccount = $this->GET_SELF_ACCOUNT_SLP($studentID);
      
        $studentSelfAccount['Details'] = strip_tags($studentSelfAccount['Details'], "<p><u><br><span>");
        $studentSelfAccount['Details'] = preg_replace('/<p.*?>/', '<p>', $studentSelfAccount['Details']);
        $studentSelfAccount['Details'] = str_replace('&nbsp;', "<span style=\"font-size:100%\">&nbsp;</span>", $studentSelfAccount['Details']);
        $studentSelfAccount['Details'] = str_replace(array('（','）'),array('(',')'),$studentSelfAccount['Details']);
        $studentSelfAccount['Details'] = str_replace('　',' ',$studentSelfAccount['Details']);
        
       
        include_once($PATH_WRT_ROOT."includes/libportfolio.php");

        
        $lpfObj = new libportfolio();
        $Details = $lpfObj->spaceIndentationForPDF($studentSelfAccount['Details']);
        $Details =  str_replace('’','\'',$Details);

        $stu_self_account_table = '';
        if(!empty($studentSelfAccount['Details'])){ 
            $stu_self_account_table = '<div class="tablebreak"  height="20px"><br></div>';
            $stu_self_account_table .= '<table width="100%" class="selfAccountTable">
                    <tr>
                        <td style="text-decoration:underline;"> 
                        '.$studentSelfAccount['Title'].'
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:10px; text-align:justify; text-justify:distribute-all-lines;">
                        <span style="text-align:justify;">'. $Details.'</span>
                        </td>
                    </tr>
                </table>';
        }     

     
        return $stu_self_account_table;
    }
    
    
    
    function GET_SELF_ACCOUNT_SLP($studentID)
    {
        global $intranet_db, $eclass_db;
        
        $sql =	"
                SELECT
                    Title,
                    Details
                FROM
                    {$eclass_db}.SELF_ACCOUNT_STUDENT
                WHERE
                    UserID = '$studentID' AND
					INSTR(DefaultSA, 'SLP') > 0
				ORDER BY
					 ModifiedDate DESC";
         $ReturnArr = $this->objDB->returnArray($sql);
       
        return $ReturnArr[0];
    } 
}

function BuildMappingMultiKeyAssoc($dataAry,$MappingKey){
    
    $resultAssocAry = array();
    foreach($dataAry as $_data){
        $_newMappingKey = $_data[$MappingKey];
        
        $resultAssocAry[$_newMappingKey][] = $_data;
    }
    
    return $resultAssocAry;
}


?>