<?php
/**
 * Modifying By: Max
 */
$Lang["Cust_Cnecc"]["Domain"] = "Domain";
$Lang["Cust_Cnecc"]["PointsAttained"] = "Points Attained";
$Lang["Cust_Cnecc"]["Event"] = "Event";
$Lang["Cust_Cnecc"]["Attainment"] = "Attainment";
$Lang["Cust_Cnecc"]["Role"] = "Role";
$Lang["Cust_Cnecc"]["Point(s)"] = "Point(s)";
$Lang["Cust_Cnecc"]["Success(v)"] = "Success(&#10004;)";
$Lang["Cust_Cnecc"]["EndDate"] = "End Date";
$Lang["Cust_Cnecc"]["NameOfStudent"] = "Name of Student";
$Lang["Cust_Cnecc"]["Class&ClassNo"] = "Class & Class No.";
$Lang["Cust_Cnecc"]["DateOfBirth"] = "Date of Birth";
$Lang["Cust_Cnecc"]["Sex"] = "Sex";
$Lang["Cust_Cnecc"]["RegNo"] = "Reg. No.";
$Lang["Cust_Cnecc"]["HkidNo"] = "HKID No.";
$Lang["Cust_Cnecc"]["DateOfIssue"] = "Date of Issue";
$Lang["Cust_Cnecc"]["Parent/Guardian"] = "Parent/Guardian";
$Lang["Cust_Cnecc"]["ClassTeacher"] = "Class Teacher";
$Lang["Cust_Cnecc"]["Principal"] = "Principal";
$Lang["Cust_Cnecc"]["PleaseChooseExactlyOneOption"] = "Please Choose Exactly One OLE Option";
$Lang["Cust_Cnecc"]["SetAsSunshineProgram"] = "Set As Sunshine Program";
$Lang["Cust_Cnecc"]["SunshineProgramInfo"] = "Sunshine Program Information";
$Lang["Cust_Cnecc"]["Attainment(s)Empty"] = "Attainment(s) Empty";
$Lang["Cust_Cnecc"]["Role(s)Empty"] = "Role(s) Empty";
$Lang["Cust_Cnecc"]["Point(s)Empty"] = "Point(s) Empty";
$Lang["Cust_Cnecc"]["Point(s)ShouldBeANumber"] = "Point(s) Should Be A Number";
$Lang["Cust_Cnecc"]["PleaseEnterSunshineProgramInfo"] = "Please Enter Sunshine Program Information";
$Lang["Cust_Cnecc"]["SunshineProgram"] = "Sunshine Program";
$Lang["Cust_Cnecc"]["Mr.NgaiShuChiu"] = "Mr. NGAI SHU CHIU";
$Lang["Cust_Cnecc"]["TitleOfProgram"] = "Title of Program";
$Lang["Cust_Cnecc"]["Category"] = "Category";
$Lang["Cust_Cnecc"]["GenerateReport"] = "Generate Report";
$Lang["Cust_Cnecc"]["SunshineProgramReport"] = "Sunshine Program Report";
$Lang["Cust_Cnecc"]["NoRecordFound"] = "No Record Found";
$Lang["Cust_Cnecc"]["RemarkContent"] = "Remark: This is to certify that {{{ STUDENT_NAME }}} has attained the {{{ ACHIEVEMENT }}} Level* of the Whole-Person Development Scheme.";
$Lang["Cust_Cnecc"]["RemarkDescription"] = "*The attainment of the {{{ ACHIEVEMENT }}} Level:-<br />A miniumum of {{{ CS_POINT }}} points from the domain of Community Service and a mininum of {{{ OTHER_DOMAIN_MIN }}} point from each of the {{{ OTHER_DOMAIN_OCCUPIED }}} domains out of {{{ ELE NUMBERS }}}:<br /> {{{ ELE ELEMENTS }}}";
$Lang["Cust_Cnecc"]["Participant"] = array("value"=>"Participant", "label"=>"Participant");
$Lang["Cust_Cnecc"]["Organizer"] = array("value"=>"Organizer", "label"=>"Organizer");
$Lang["Cust_Cnecc"]["Helper"] = array("value"=>"Helper", "label"=>"Helper");
$Lang["Cust_Cnecc"]["Error"]["InvalidDateFormat"] = "Invalid Date Format";
$Lang["Cust_Cnecc"]["CannotAssignStudentsForSunshinePrograms"] = "Cannot Assign Student(s) for Sunshine Programs";
$Lang["Cust_Cnecc"]["BasicCompetency"] = "Award of Basic Competency";
$Lang["Cust_Cnecc"]["AwardOfAchievement"] = "Award of Achievement";
$Lang["Cust_Cnecc"]["AwardOfExcellence"] = "Award of Excellence";
$Lang["Cust_Cnecc"]["NIL"] = "NIL";
$Lang["Cust_Cnecc"]["Total"] = "Total";
$Lang["Cust_Cnecc"]["AttainmentOfCnecWPDS"] = "Attainment of the CNEC Whole-Person Development Scheme";
$Lang["Cust_Cnecc"]["AwardAttained"] = "Award attained";
$Lang["Cust_Cnecc"]["CriteriaContent"] = "The criteria for the scheme is stated on the back of this record.";

$Lang["Cust_Cnecc"]["OwnDefineCategory"]["ParticipationInActivities"] = "Participation in Activities";
$Lang["Cust_Cnecc"]["OwnDefineCategory"]["CoursesAndTrainingTaken"] = "Courses and Training Taken";
$Lang["Cust_Cnecc"]["OwnDefineCategory"]["Services"] = "Services";
$Lang["Cust_Cnecc"]["OwnDefineCategory"]["Awards"] = "Awards";
$Lang["Cust_Cnecc"]["OwnDefineCategory"]["Others"] = "Others";
$Lang["Cust_Cnecc"]["CloneYear"]["Instruction"] = "You can create / copy Sunshine Program(s) from past school Year.";
$Lang["Cust_Cnecc"]["JS_EmptyYear"]= "Please select a Academic Year";
$Lang["Cust_Cnecc"]["JS_YearWithRecord"]= "Selected Academic Year has been copied before. Copy Aborted";
?>