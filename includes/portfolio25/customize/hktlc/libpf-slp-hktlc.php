<?php

class studentInfo{
	private $objDB;
	private $eclass_db;
	private $intranet_db;
	
	private $printIssueDate; // store the print issues date;
	private $AcademicYearAry;
	private $academcYearDisplay; // store the academic Year display in the student Info
	private $YearTermIDAry;
	private $uid;
	private $cfgValue;


	public function studentInfo($uid){
		global $eclass_db,$intranet_db,$ipf_cfg;
		
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		
		$this->uid = $uid;
		$this->cfgValue = $ipf_cfg;

	}
	
	public function setAcademicYear($AcademicYear){
		$this->AcademicYearAry = $AcademicYear;
	}
	private function getAcademicYear(){
		return $this->AcademicYearAry;
	}
	public function setYearTermID($YearTermID){
		$this->YearTermIDAry = $YearTermID;
	}
	private function getYearTermID(){
		return $this->YearTermIDAry;
	}
	public function getUid(){
		return $this->uid;
	}
	public function setAcademicYearDisplay($academcYearDisplay){
		$this->academcYearDisplay = $academcYearDisplay;
	}
	public function getAcademicYearDisplay(){
		return $this->academcYearDisplay;
	}
	public function setPrintIssueDate($p_date){
		$this->printIssueDate = $p_date;
	}
	public function getPrintIssueDate(){
		return $this->printIssueDate;
	}
	
	private function getEmptySpace($height) {
		return '<div style="height:'.$height.'; font-size:'.$height.';">&nbsp;</div>'."\r\n";
	}
	public function getSectionSeparatorSpace() {
		return $this->getEmptySpace('10px');
	}
	
	public function getHeader_HTML() {
		return $this->getReportHeaderDisplay();
	}
	private function getReportHeaderDisplay() {
		$x = '';
		$x .= '<table style="width:100%; border: 0px 0px 0px 0px;">'."\r\n";
			$x .= '<tr><td style="text-align:center;">'.$this->getEmptySpace('92px').'</td></tr>'."\r\n";
			$x .= '<tr><td style="text-align:center;">STUDENT LEARNING PROFILE REPORT</td></tr>'."\r\n";
			$x .= '<tr><td style="text-align:center;">學生學習檔案報告</td></tr>'."\r\n";
			$x .= '<tr><td style="text-align:center;">'.$this->getSectionSeparatorSpace().'</td></tr>'."\r\n";
			$x .= '<tr><td style="text-align:center;">Academic Year '.$this->getAcademicYearDisplay().' 年度</td></tr>'."\r\n";
			$x .= '<tr><td style="text-align:center;">'.$this->getSectionSeparatorSpace().'</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	
	public function getFooter_HTML() {
		return $this->getReportFooterDisplay();
	}
	private function getReportFooterDisplay() {
		$x = '';
		$x .= '<table style="width:100%; border: 0px 0px 0px 0px;">'."\r\n";
			$x .= '<tr><td style="text-align:center; font-size:14px;">ID: 智育發展  MCE: 德育及公民教育 CS: 社會服務 CRE: 與工作有關經驗 AD: 藝術發展 PD: 體育發展</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		return $x;
	}
	
	public function getStudentInfo_HTML(){
		$student_info_arr = $this->getStudentInfo();
		$html = $this->getStudentInfoDisplay($student_info_arr);
		return $html;
	}	

	private function getStudentInfo(){
		$INTRANET_USER = $this->intranet_db.".INTRANET_USER";
		$sql = "Select
						EnglishName,
						ChineseName,
						ClassName,
						ClassNumber,
						Gender,
						STRN,
						HKID,
						DateOfBirth,
						WebSAMSRegNo
				From
						$INTRANET_USER
				Where
						UserID = '".$this->uid."'
				";
		return $this->objDB->returnArray($sql);
	}
	private function getStudentInfoDisplay($student_info_arr){
		
		$ChineseNameDisplay = $student_info_arr[0]['ChineseName'];
		$EnglishNameDisplay = $student_info_arr[0]['EnglishName'];
		$ClassDisplay = $student_info_arr[0]['ClassName'].' ('.$student_info_arr[0]['ClassNumber'].')';
		
		switch ($student_info_arr[0]['Gender']) {
			case 'M':
				$SexDisplay = '男 Male';
				break;
			case 'F':
				$SexDisplay = '女 Female';
				break;
			default:
				$SexDisplay = '&nbsp;';
		}
		
		$STRNDisplay = $student_info_arr[0]['STRN'];
		$HKIDDisplay = $student_info_arr[0]['HKID'];
		$DateOfBirthDisplay = date('d/m/Y', strtotime($student_info_arr[0]['DateOfBirth']));
		$WebSAMSRegNoDisplay = str_replace('#', '', $student_info_arr[0]['WebSAMSRegNo']);
		$IssueDateDisplay = $this->getPrintIssueDate();
		
		
		$x = '';
		$x .= '<table style="width:100%; font-size:12px; border: 0px 0px 0px 0px;">'."\r\n";
			$x .= '<col width="9%">'."\r\n";	// Chinese Title
			$x .= '<col width="11%">'."\r\n";	// English Title
			$x .= '<col width="1%">'."\r\n";	// Colon
			$x .= '<col width="15%">'."\r\n";	// Student Data
			$x .= '<col width="9%">'."\r\n";	// Chinese Title
			$x .= '<col width="8%">'."\r\n";	// English Title
			$x .= '<col width="1%">'."\r\n";	// Colon
			$x .= '<col width="12%">'."\r\n";	// Student Data
			$x .= '<col width="10%">'."\r\n";	// Chinese Title
			$x .= '<col width="12%">'."\r\n";	// English Title
			$x .= '<col width="1%">'."\r\n";	// Colon
			$x .= '<col width="11%">'."\r\n";	// Student Data
			
			$x .= '<tr>'."\r\n";
				$x .= '<td >學生姓名</td>'."\r\n";
				$x .= '<td>Name</td>'."\r\n";
				$x .= '<td> : </td>'."\r\n";
				$x .= '<td>'.$ChineseNameDisplay.'</td>'."\r\n";
				
				$x .= '<td colspan="4">('.$EnglishNameDisplay.')</td>'."\r\n";
				
				$x .= '<td>班別</td>'."\r\n";
				$x .= '<td>Class Name</td>'."\r\n";
				$x .= '<td> : </td>'."\r\n";
				$x .= '<td>'.$ClassDisplay.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			$x .= '<tr>'."\r\n";
				$x .= '<td >性別</td>'."\r\n";
				$x .= '<td>Sex</td>'."\r\n";
				$x .= '<td> : </td>'."\r\n";
				$x .= '<td>'.$SexDisplay.'</td>'."\r\n";
				
				$x .= '<td >學生編號</td>'."\r\n";
				$x .= '<td>STRN</td>'."\r\n";
				$x .= '<td> : </td>'."\r\n";
				$x .= '<td>'.$STRNDisplay.'</td>'."\r\n";
				
//				$x .= '<td>身份證號碼</td>'."\r\n";
//				$x .= '<td>HKID No.</td>'."\r\n";
//				$x .= '<td> : </td>'."\r\n";
//				$x .= '<td>'.$HKIDDisplay.'</td>'."\r\n";
				$x .= '<td >&nbsp;</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
				$x .= '<td>&nbsp;</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			
			$x .= '<tr>'."\r\n";
				$x .= '<td >出生日期</td>'."\r\n";
				$x .= '<td>Date of Birth</td>'."\r\n";
				$x .= '<td> : </td>'."\r\n";
				$x .= '<td>'.$DateOfBirthDisplay.'</td>'."\r\n";
				
				$x .= '<td >註冊編號</td>'."\r\n";
				$x .= '<td>Reg. No.</td>'."\r\n";
				$x .= '<td> : </td>'."\r\n";
				$x .= '<td>'.$WebSAMSRegNoDisplay.'</td>'."\r\n";
				
				$x .= '<td>派發日期</td>'."\r\n";
				$x .= '<td>Date of Issue</td>'."\r\n";
				$x .= '<td> : </td>'."\r\n";
				$x .= '<td>'.$IssueDateDisplay.'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;	
	}


	public function getPartA_HTML() {
		return $this->getPartADisplay();
	}
	private function getPartADisplay() {
		global $ipf_cfg;
		
		$DataArr = $this->getOLEData($ipf_cfg["OLE_TYPE_STR"]["INT"]);
		$numOfData = count($DataArr);
		
		$td_padding = ' padding:10px 5px 5px 5px; ';
		$border_left = ' border-left:1px solid #000000; ';
		$border_bottom = ' border-bottom:1px solid #000000; ';
		
		$x = '';
		$x .= '<table style="width:100%; font-size:14px; font-weight:bold; border:0px 0px 0px 0px;">'."\r\n";
			$x .= '<tr><td>其他學習經歷</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		$x .= '<table cellspacing="0px" style="width:100%; font-size:14px; border: 0px 0px 0px 0px;">'."\r\n";
			$x .= '<col width="13%">'."\r\n";
			$x .= '<col width="28%">'."\r\n";
			$x .= '<col width="8%">'."\r\n";
			$x .= '<col width="20%">'."\r\n";
			$x .= '<col width="11%">'."\r\n";
			$x .= '<col width="7%">'."\r\n";
			$x .= '<col width="13%">'."\r\n";
			
			$x .= '<thead>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<th style="text-align:left; '.$td_padding.$border_bottom.'">日期/時段</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">標題</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">類別</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">其他學習經歷範疇</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">參與角色</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">時數</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">獎項/證書<br />/成果</th>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</thead>'."\r\n";
			
			$x .= '<tbody>'."\r\n";
				for ($i=0; $i<$numOfData; $i++) {
					$x .= '<tr style="vertical-align:top;">'."\r\n";
						$x .= '<td style="'.$td_padding.'">'.$DataArr[$i]['DateRangeDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['ProgramNameDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['CategoryNameDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['ELEDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['RoleDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['HoursDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['AchievementDisplay'].'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
				}
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	public function getPartB_HTML() {
		return $this->getPartBDisplay();
	}
	private function getPartBDisplay() {
		global $ipf_cfg;
		
		$DataArr = $this->getOLEData($ipf_cfg["OLE_TYPE_STR"]["EXT"]);
		$numOfData = count($DataArr);
		
		$td_padding = ' padding:10px 5px 5px 5px; ';
		$border_left = ' border-left:1px solid #000000; ';
		$border_bottom = ' border-bottom:1px solid #000000; ';
		
		$x = '';
		$x .= '<table style="width:100%; font-size:14px; font-weight:bold; border:0px 0px 0px 0px;">'."\r\n";
			$x .= '<tr><td>校外表現/獎項及重要參與</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		
		$x .= '<table cellspacing="0px" style="width:100%; font-size:14px; border: 0px 0px 0px 0px;">'."\r\n";
			$x .= '<col width="13%">'."\r\n";
			$x .= '<col width="37%">'."\r\n";
			$x .= '<col width="13%">'."\r\n";
			$x .= '<col width="17%">'."\r\n";
			$x .= '<col width="20%">'."\r\n";
			
			$x .= '<thead>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<th style="text-align:left; '.$td_padding.$border_bottom.'">日期/時段</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">標題</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">類別</th>'."\r\n";
					//$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">其他學習經歷範疇</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">參與角色</th>'."\r\n";
					//$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">時數</th>'."\r\n";
					$x .= '<th style="text-align:left; '.$border_left.$td_padding.$border_bottom.'">獎項/證書<br />/成果</th>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</thead>'."\r\n";
			
			$x .= '<tbody>'."\r\n";
				for ($i=0; $i<$numOfData; $i++) {
					$x .= '<tr style="vertical-align:top;">'."\r\n";
						$x .= '<td style="'.$td_padding.'">'.$DataArr[$i]['DateRangeDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['ProgramNameDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['CategoryNameDisplay'].'</td>'."\r\n";
						//$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['ELEDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['RoleDisplay'].'</td>'."\r\n";
						//$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['HoursDisplay'].'</td>'."\r\n";
						$x .= '<td style="'.$border_left.$td_padding.'">'.$DataArr[$i]['AchievementDisplay'].'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
				}
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
		
		return $x;
	}
	private function getOLEData($IntExt) {
		global $eclass_db, $ipf_cfg;
		
		$YearTermID = $this->getYearTermID();
		if ($YearTermID != '') {
			$conds_YearTermID = " And op.YearTermID In ('".implode("','", (array)$YearTermID)."') ";
		}
		
		$OLE_STUDENT = $eclass_db.'.OLE_STUDENT';
		$OLE_PROGRAM = $eclass_db.'.OLE_PROGRAM';
		$OLE_CATEGORY = $eclass_db.'.OLE_CATEGORY';
		$sql = "Select
						op.StartDate,
						op.EndDate,
						op.Title as ProgramName,
						oc.ChiTitle as CategoryName,
						op.ELE,
						os.Role,
						os.Hours,
						os.Achievement
				From
						$OLE_STUDENT as os
						Inner Join $OLE_PROGRAM as op On (os.ProgramID = op.ProgramID)
						Inner Join $OLE_CATEGORY as oc On (op.Category = oc.RecordID)
				Where
						os.UserID = '".$this->uid."'
						And op.AcademicYearID In ('".implode("','", (array)$this->getAcademicYear())."')
						And os.RecordStatus In ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."', '".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."')
						And op.IntExt = '".strtoupper($IntExt)."'
						And os.SLPOrder Is Not Null
						$conds_YearTermID
				Order By
						op.StartDate Desc
				";
		$DataArr = $this->objDB->returnArray($sql);
		$numOfData = count($DataArr);
		
		$ELEDisplayAssoArr = $this->getELECodeDisplayMapping();
		$ReturnArr = array();
		for ($i=0; $i<$numOfData; $i++) {
			// Date Range Display
			$thisStartDate = trim($DataArr[$i]['StartDate']);
			$thisEndDate = trim($DataArr[$i]['EndDate']);
			$thisDateDisplay = $thisStartDate;
			if (!is_date_empty($thisEndDate)) {
				$thisDateDisplay .= '<br />至<br />'.$thisEndDate;
			}
			$ReturnArr[$i]['DateRangeDisplay'] = Get_Table_Display_Content($thisDateDisplay);
			
			// Program Name
			$ReturnArr[$i]['ProgramNameDisplay'] = Get_Table_Display_Content($DataArr[$i]['ProgramName']);
			
			// Category
			$ReturnArr[$i]['CategoryNameDisplay'] = Get_Table_Display_Content($DataArr[$i]['CategoryName']);
			
			// ELE Display
			$thisELE = $DataArr[$i]['ELE'];
			$thisELEArr = explode(',', $thisELE);
			$thisNumOfELE = count($thisELEArr);
			
			$thisELEDisplayArr = array();
			for ($j=0; $j<$thisNumOfELE; $j++) {
				$thisELECode = $thisELEArr[$j];
				$thisDisplay = $ELEDisplayAssoArr[$thisELECode];
				
				if ($thisDisplay != '') {
					$thisELEDisplayArr[] = $thisDisplay;
				}
			}
			$ReturnArr[$i]['ELEDisplay'] = implode(', ', $thisELEDisplayArr);
			
			// Role
			$ReturnArr[$i]['RoleDisplay'] = Get_Table_Display_Content($DataArr[$i]['Role']);
			
			// Hours
			$ReturnArr[$i]['HoursDisplay'] = Get_Table_Display_Content($DataArr[$i]['Hours']);
			
			// Achievement
			$ReturnArr[$i]['AchievementDisplay'] = Get_Table_Display_Content($DataArr[$i]['Achievement']);
		}
		
		return $ReturnArr;
	}
	private function getELECodeDisplayMapping() {
		$MappingArr = array();
		$MappingArr['[ID]'] = 'ID';
		$MappingArr['[MCE]'] = 'MCE';
		$MappingArr['[CS]'] = 'CS';
		$MappingArr['[CE]'] = 'CRE';
		$MappingArr['[AD]'] = 'AD';
		$MappingArr['[PD]'] = 'PD';
		
		return $MappingArr;
	}
		/************** Self Account ***************/
	
	public function getSelfAccount_HTML(){
		
		$Part7_Title = "Students' \"Self-Account\" 學生自述";
		
		$DataArr = array();
		$DataArr = $this->get_Self_Account_Info();
		
		$Content = $this->getSelfAccContentDisplay($DataArr,$Part7_Title);

		if($Content) {
			$html = '<table width=100% height="860px">';
			$html .= '<tr><td style="vertical-align:top;">';
			$html .= $this->getSelfAccContentDisplay($DataArr,$Part7_Title) ;
			$html .= '</td></tr>';
			$html .='</table>';
			
			return $html;
		}
		
	}
	
	
	private function get_Self_Account_Info(){
		
		global $ipf_cfg, $eclass_db;
		$self_acc_std = $eclass_db.'.SELF_ACCOUNT_STUDENT';
		
		$StudentID = $this->uid;
		
		if($StudentID!='')
		{
			$cond_studentID = "And UserID ='$StudentID'";
		}	
		$cond_DefaultSA = "And DefaultSA='SLP'";
		
		$sql = "Select
						Details
				From
						$self_acc_std
				Where
						1	
						$cond_studentID	
						$cond_DefaultSA			

				";
		$roleData = $this->objDB->returnArray($sql,2);
		
		for($i=0,$i_MAX=count($roleData);$i<$i_MAX;$i++)
		{			
			$thisDataArr = array();
			$thisDataArr['Details'] =$roleData[$i]['Details'];
			
			$returnDate[] = $thisDataArr;
			
		}

		return $returnDate;
	}
	
	private function getSelfAccContentDisplay($DataArr='',$MainTitle='')
	{
		
		$_borderTop =  'border-top: 1px solid black; ';
		$_borderLeft =  'border-left: 1px solid black; ';
		$_borderBottom =  'border-bottom: 1px solid black; ';
		$_borderRight =  'border-right: 1px solid black; ';
		$text_alignStr ='center';		
		
		
		$Page_break='page-break-after:always;';
		$table_style = 'style="border-collapse: collapse;font-size:16px;" '; 

		$_page_break_br = '<table width="90%" cellpadding="0" border="0px" align="center" style="page-break-after:always;">					
				   			 <tr>
							    <td></td>
							 </tr>
						  </table>';

		$html = '';
					
		$rowMax_count = count($DataArr);
	
		if($rowMax_count>0)
		{
			$DataArr = current($DataArr);
			$Detail = $DataArr['Details'];


			$Detail = strip_tags($Detail,'<p><br>');


		}
		else
		{
			return '';
		}	
		$html .= $_page_break_br;
		$html .= '<table width="90%" height="" cellpadding="2" border="0px" align="center" style="font-size:16px;border-collapse: collapse;">
					<tr> 
						<td style=" font-size:15px;text-align:left;vertical-align:text-top;"><b>
						'.$MainTitle.'
						</b></td>
				  	</tr>
				  </table>';
		
		$html .= '<table width="90%" height="800px"  cellpadding="7" border="0px" align="center" '.$table_style.'>
					<tr>				 							
						<td style="vertical-align: top;'.$_borderTop.$_borderLeft.$_borderBottom.$_borderRight.'">'.$Detail.'</td>						 														
					</tr>
				  </table>';
		//$html .= $_page_break_br;
			
		return $html; 
	}
	
	
	/********************* End **********************/
}
?>