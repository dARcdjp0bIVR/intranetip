<?
include_once($intranet_root.'/includes/portfolio25/import/oleImportDataDefinition.php');
include_once($intranet_root.'/includes/portfolio25/slp/ole_program.php');
include_once($intranet_root.'/includes/portfolio25/slp/ole_student.php');
include_once($intranet_root."/includes/libpf-slp.php");
include_once($intranet_root."/includes/portfolio25/slp/libpf-ole.php");

class oleImportData extends importData{

	private $allOLEProgram = NULL;
	private $intExt = null;


	//store this batch no into table "OLE_IMPORT_RECORD", reference for other import record 
	private $batchNo = null;	

	public function oleImportData(){
		$this->objImportDefinition = new oleImportDataDefinition();
		$this->removePastBatchRecord();
	}	


	//passing $val should be a file with full paths. e.g /home/web/eclass40/intranetIP25/file/import_temp/iportfolio/ole//20120410_105701_1.CSV
	public function setBatchNo ($val){
		$pattern = '/^.*\/(.*)\.csv/i';
		$replacement = '$1';
		$batchNo = preg_replace($pattern,$replacement,$val);
		$this->batchNo = $batchNo;
	}

	public function getBatchNo(){
		return $this->batchNo;
	}

	public function setOLEIntExt($val){
		$this->intExt = $val;
	}
	public function getOLEIntExt(){
		global $ipf_cfg;
		if(trim($this->intExt) == ''){
			//default is int
			return $ipf_cfg["OLE_TYPE_STR"]["INT"];
		}else{
			return $this->intExt;
		}
	}
	public function processImportDataDetails(){
		global $ipf_cfg,$eclass_db;
		$objDB = new libdb();
		$allStudentClassNameClassNumbmer = $this->getAllStudentClassNameClassNumber();
		$allYearTermInfo = $this->getAllYearTermInfo();
		$existingEleCode = $this->getAllEleFromDB();
		for($i = 0, $i_max = count($this->dataArray); $i < $i_max; $i++){
			$_className = $this->dataArray[$i]['ClassName'];
			$_classNumber = $this->dataArray[$i]['ClassNumber'];
			$_webSAMSRegNo = $this->dataArray[$i]['WebSAMSRegNo'];
			$_loginName = $this->dataArray[$i]['LoginName'];

			$_startDate = $this->dataArray[$i]['StartDate'];
			$_endDate = $this->dataArray[$i]['EndDate'];
			$_OLETitle = $objDB->Get_Safe_Sql_Query($this->dataArray[$i]['OLETitle']);
			$_schoolYear = $this->dataArray[$i]['SchoolYear'];
			$_schoolTerm = $this->dataArray[$i]['SchoolTerm'];

			$_role = $objDB->Get_Safe_Sql_Query($this->dataArray[$i]['Role']);
			$_achievement = $objDB->Get_Safe_Sql_Query($this->dataArray[$i]['Achievement']);
			$_hour = $this->dataArray[$i]['Hour'];
			$_programRemark = $objDB->Get_Safe_Sql_Query($this->dataArray[$i]['ProgramRemark']);
			$_organization = $objDB->Get_Safe_Sql_Query($this->dataArray[$i]['Organization']);
			$_category = $this->dataArray[$i]['Category'];
			$_componentCode = $this->dataArray[$i]['ComponentCode'];
			$_ELEList = $this->getConvertedEle($_componentCode,$existingEleCode);
			$_details = $objDB->Get_Safe_Sql_Query($this->dataArray[$i]['Details']);
						
			$programId  = $this->getProgramID($_OLETitle,$_startDate,$_ELEList);
			
			
			if($programId >0){				

			}else{
				$objOLEProgram = new ole_program();
				$objOLEProgram->setTitle($_OLETitle);
				$objOLEProgram->setStartDate($_startDate);
				$objOLEProgram->setSchoolRemarks($_programRemark);
				$objOLEProgram->setOrganization($_organization);
				$objOLEProgram->setCategory($_category);
				$objOLEProgram->setCreatorID($_SESSION['UserID']);
				$objOLEProgram->setDetails($_details);
				$objOLEProgram->setProgramType($ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"]);
												


				if($_schoolYear == '' && $_schoolTerm == ''){
					//import file have not specify the year and term , get it from the start date
					$academicYearDetails = getAcademicYearAndYearTermByDate($_startDate);

					$_AcademicYearID = $academicYearDetails['AcademicYearID'];
					$_YearTermID = $academicYearDetails['YearTermID'];

				}else{
					$tmp = $this->getYearAndTermID($_schoolYear,$_schoolTerm,$allYearTermInfo);

					$_AcademicYearID = $tmp['AcademicYearID'];
					$_YearTermID = $tmp['YearTermID'];
				}

				$objOLEProgram->setAcademicYearID($_AcademicYearID);
				$objOLEProgram->setYearTermID($_YearTermID);
					

				$objOLEProgram->setIntExt($this->getOLEIntExt());
				$objOLEProgram->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["teacherImport"]);

				//value should be set , otherwise it will set to NOW() automatically
				$objOLEProgram->setCanJoinStartDate($value='dummy');
				$objOLEProgram->setCanJoinEndDate($value='dummy');

				$objOLEProgram->setELE($_ELEList);
				
				$programId = $objOLEProgram->SaveProgram();

				//reload the OLE Program From DB for duplicate program checking again
				$this->getAllOLEProgram();
			}

			//insert into ole_student
			$studentID = $this->getStudentIntranetID($_className , $_classNumber,$_webSAMSRegNo,$_loginName,$allStudentClassNameClassNumbmer);

			$objOLEStudent = new ole_student();

			$objOLEStudent->setUserID($studentID);
			$objOLEStudent->setProgramID($programId);
			$objOLEStudent->setAchievement($_achievement);
			$objOLEStudent->setRole($_role);
			$objOLEStudent->setHours($_hour);			
			$objOLEStudent->setComeFrom($ipf_cfg["OLE_STUDENT_COMEFROM"]["teacherImport"]);
			$objOLEStudent->setRecordStatus($ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]);
			$objOLEStudent->setApprovedBy($_SESSION['UserID']);
			$objOLEStudent->setProcessDate('now()');
			$objOLEStudent->setInputDate('now()');
			$objOLEStudent->setModifiedDate('now()');

			$recordID = $objOLEStudent->saveRecord();


			//save to batch table 
			$dbBatchValue["IMPORT_LINE_NO"]	= $objDB->pack_value($i,"str");
			$dbBatchValue["OLE_PROGRAM_ID"]	= $objDB->pack_value($programId,'int');
			$dbBatchValue["OLE_STUDENT_RECORD_ID"]	= $objDB->pack_value($recordID,'int');
			$dbBatchValue["OLE_STUDENT_ID"]	= $objDB->pack_value($studentID,'int');
			$dbBatchValue["IMPORT_BATCH_NO"]	= $objDB->pack_value($this->getBatchNo(),'str');
			$dbBatchValue["DATE_INPUT"]	= $objDB->pack_value('now()','date');

			$dbFieldArray = $objDB->concatFieldValueToSqlStr($dbBatchValue);
			$dbField = $dbFieldArray['sqlField'];
			$saveValue = $dbFieldArray['sqlValue'];
			$sql = "insert into ".$eclass_db.".OLE_IMPORT_RECORD (".$dbField.")	values (".$saveValue.")";
			$objDB->db_db_query($sql);
		}
	}

	//need to stress
	private function getProgramID($programTitle , $programStartDate,$ELEList){
		$programID = 0;
		$objDB = new libdb();
		if(is_null($this->allOLEProgram)){
			$this->getAllOLEProgram();			
		}

		for($i = 0, $i_max = count($this->allOLEProgram);$i < $i_max;$i++){
			if(	
				($objDB->Get_Safe_Sql_Query($this->allOLEProgram[$i]['Title']) == $programTitle) && 
				($this->allOLEProgram[$i]['StartDate'] == $programStartDate) && 
				($this->allOLEProgram[$i]['ELE'] == $ELEList)
			   ){
				$programID = $this->allOLEProgram[$i]['ProgramID'];

				//find the program ID , stop the loop here
				break;
			}
		}
		return $programID;
	}
	public function checkImportDataDetails(){
		global $ec_guide ,$Lang;
		$allStudentClassNameClassNumbmer = $this->getAllStudentClassNameClassNumber();
		$allOLEPROGRAMCategoryCode = $this->getAllCategoryCode();
		$existingEleCode = $this->getAllEleFromDB();
		$allYearTermInfo = $this->getAllYearTermInfo();

		for($i = 0, $i_max = count($this->dataArray); $i < $i_max; $i++){
			$error = array();
			$_className = $this->dataArray[$i]['ClassName'];
			$_title = $this->dataArray[$i]['OLETitle'];
			$_classNumber = $this->dataArray[$i]['ClassNumber'];
			$_startDate = $this->dataArray[$i]['StartDate'];
			$_endDate = $this->dataArray[$i]['EndDate'];
			$_category = $this->dataArray[$i]['Category'];
			$_componentCode = $this->dataArray[$i]['ComponentCode'];
			$_webSAMSRegNo = $this->dataArray[$i]['WebSAMSRegNo'];
			$_loginName = $this->dataArray[$i]['LoginName'];
			$_schoolYear = $this->dataArray[$i]['SchoolYear'];
			$_schoolTerm = $this->dataArray[$i]['SchoolTerm'];

			//check title is empty 
			if(trim($_title) == '' ){
				$this->registerError('Title_ERROR',$ec_guide['import_empty_title'],$error);
			}


			//check category code valid
			$categoryExist = $this->getCategoryExist($_category , $allOLEPROGRAMCategoryCode);
			if($categoryExist ){

			}else{
				$this->registerError('CATEGORY_ERROR',$ec_guide['import_ole_category_not_found'],$error);

			}

			//check student class name class number exist
			if($_className == '' && $_classNumber == ''){
				//skip this checking if both $_className and $_classNumber are empty 
			}else{
				if($this->checkClassNameClassNumberIsValid($_className , $_classNumber,$allStudentClassNameClassNumbmer)){
					//class name and class number is valid
				}else{
					$this->registerError('STUDENT_ERROR',$Lang['iPortfolio']['IMPORT']['ClassNameClassNumberInvalid'],$error);
				}
			}

			//check student websamsNo exist
			if(trim($_webSAMSRegNo) != ''){
				//check for _webSAMSRegNo with value only
				if($this->checkWebSamsRegNoExist($_webSAMSRegNo, $allStudentClassNameClassNumbmer)){

				}else{
					$this->registerError('STUDENT_Regno_ERROR',$Lang['iPortfolio']['IMPORT']['WebSAMSNotFind'],$error);
				}
			}

			if(trim($_loginName) != ''){
				if($this->checkLoginNameExist($_loginName,$allStudentClassNameClassNumbmer)){

				}else{
					$this->registerError('STUDENT_loginName_ERROR',$Lang['iPortfolio']['IMPORT']['StudentLoginIDNotFind'],$error);
				}
			}

			//check class name , class number , websams no , student login match to one student
			$studentIntranetUserId = $this->getStudentIntranetID($_className , $_classNumber,$_webSAMSRegNo,$_loginName,$allStudentClassNameClassNumbmer);

			if(intval($studentIntranetUserId) > 0){
			}else{
				$this->registerError('StudentIDError',$ec_guide['import_error_no_user'],$error);
			}
	

			//check year 
			if(trim($_schoolYear) == '' && trim($_schoolTerm) == '') {
				//both of schoolYear and schoolterm is empty , skip the checking
			}else{
				if($this->checkYearAndTermIsValid($_schoolYear , $_schoolTerm,$allYearTermInfo)){
				}else{
					$this->registerError('YearTermError',$ec_guide['import_type_year_term_not_found'],$error);
				}
			}
			//check term
			if ( !preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/i", $_startDate) || !checkDateIsValid($_startDate)) {
				$this->registerError('StartDateError',$ec_guide['import_dateformat_not_correct'],$error);
				//$error['StartDateError'] = 'Start Date Format is wrong';
			} 

			//if end date with data (!= '') and end date format is wrong
			if ((trim($_endDate) != '') && (!preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/i", $_endDate) || !checkDateIsValid($_endDate))) {
				$this->registerError('EndDateError',$ec_guide['import_dateformat_not_correct'],$error);

			} 

			if(trim($_componentCode) != ''){
				//check all the Input ele code exist
				if($this->checkEleValid($_componentCode,$existingEleCode)!= true){
					$this->registerError('EleCodeError',$Lang['iPortfolio']['IMPORT']['ELECodeError'],$error);
				}
				//check ele code duplicate in the CSV by itselves ie 1;2;1 duplicate 1 , 2;3;4;3 duplicate 3
				if($this->checkEleIsDuplicate($_componentCode) ==  true){
					$this->registerError('EleCodeDuplicateError',$Lang['iPortfolio']['IMPORT']['ELECodeDuplicate'],$error);
				}
			}

			$this->pushToError($i,$error);

		}


	}

	private function checkEleIsDuplicate($ParImportEle){
		$ELE_IDArr = explode(";", $ParImportEle);
		if(count($ELE_IDArr) != count(array_unique($ELE_IDArr))){
			//is duplicate 
			return true;
		}else{
			return false;
		}

	}
	private function getAllOLEProgram(){
		global $eclass_db;
		global $ipf_cfg;
		$sql = 'select ProgramID, Title,StartDate,ELE from '.$eclass_db.'.OLE_PROGRAM where IntExt=\''.$this->getOLEIntExt().'\'';

		$objDB = new libdb();
		$this->allOLEProgram = $objDB->returnResultSet($sql);

	}

	//private function getStudentIntranetID($checkClassName , $checkClassNumber,$allStudentClassNameClassNumbmer){
	private function getStudentIntranetID($parClassName, $parClassNumber,$parWebSAMSRegNo,$parLoginName,$allStudentClassNameClassNumbmer){
		$parClassName = trim($parClassName);
		$parWebSAMSRegNo = trim($parWebSAMSRegNo);
		$parLoginName = trim($parLoginName);

		if($parClassName == '' && $parClassNumber == '' && $parWebSAMSRegNo == '' && $parLoginName == ''){
			//if all the student info is empty , reture false
			return false;
		}
		$parWebSAMSRegNo = preg_replace('/^#/','',$parWebSAMSRegNo);
		for($i = 0, $i_max = count($allStudentClassNameClassNumbmer); $i < $i_max; $i++){

			$_className = $allStudentClassNameClassNumbmer[$i]['ClassName'];
			$_classNumber = $allStudentClassNameClassNumbmer[$i]['ClassNumber'];
			$_UserLogin = $allStudentClassNameClassNumbmer[$i]['UserLogin'];
			$_WebSAMSRegNo = $allStudentClassNameClassNumbmer[$i]['WebSAMSRegNo'];
			$_UserID = $allStudentClassNameClassNumbmer[$i]['UserID'];



			$_WebSAMSRegNo = preg_replace('/^#/','',$_WebSAMSRegNo);

			//case 1 , if only class name and class number are filled, then can leave for checking userloign and websams
			//case 2 , if only userlogin are filled, then can leave for checking class numebr  , class name and  websams
			//case 3 , if only websamsno  are filled, then can leave for checking class numebr  , class name and  userlogin
			//case 4 , if three type info is filled , we must check whether the three type info pointed to a exactly one userlogin

			//so we assign all the info to a $check* if any one of it is empty, then , one if statement can handle all the case

			$checkClassName = ($parClassName == '') ? $_className:  $parClassName;
			$checkClassNumber = ($parClassNumber == '') ? $_classNumber:  $parClassNumber;
			$checkWebSAMSRegNo = ($parWebSAMSRegNo == '') ? $_WebSAMSRegNo:  $parWebSAMSRegNo;
			$checkLoginName = ($parLoginName == '') ? $_UserLogin:  $parLoginName;

			if(($checkClassName == $_className) && ($checkClassNumber== $_classNumber)&& ($checkWebSAMSRegNo== $_WebSAMSRegNo)&& ($checkLoginName == $_UserLogin)){
				return $_UserID;
			}
		}
		return false;
	}

	private function getAllStudentClassNameClassNumber(){
				global $intranet_db;
		$objDb = new libdb();

//		$sql = 'select ClassName , ClassNumber,UserID,UserLogin ,WebSAMSRegNo from '.$intranet_db.'.INTRANET_USER where RecordType =2 and userlogin like \'%eric%\'';
		$sql = 'select ClassName , ClassNumber,UserID,UserLogin ,WebSAMSRegNo from '.$intranet_db.'.INTRANET_USER where RecordType =2';
		$result = $objDb->returnResultSet($sql);
		return $result;
	}
	private function getAllCategoryCode(){
		global $eclass_db;
		$objDb = new libdb();

		//where recordstatus is valid
		$sql  = 'select RecordID from '.$eclass_db.'.OLE_CATEGORY where recordstatus <> 0';
		$result = $objDb->returnResultSet($sql);
		return $result;

	}
	private function getCategoryExist($category , $allOLEPROGRAMCategoryCode){

		for($i = 0, $i_max = count($allOLEPROGRAMCategoryCode);$i < $i_max; $i++){

			if($category  == $allOLEPROGRAMCategoryCode[$i]['RecordID']){

				return true;
			}
		}
		return false;

	}
	private function getAllEleFromDB(){
			global $ipf_cfg;

			$lpf_ole = new libpf_ole();
			
			$ELEArray = $lpf_ole->Get_ELE_Info(array($ipf_cfg["OLE_ELE_RecordStatus"]["Public"], $ipf_cfg["OLE_ELE_RecordStatus"]["DefaultAndPublic"]));
			$ELEAssoArr = BuildMultiKeyAssoc($ELEArray, 'RecordID');
			return $ELEAssoArr;
			

	}

	private function checkEleValid($ParImportEle,$existingEleCode){

		$ELE_IDArr = explode(";", $ParImportEle);
		$numOfELE = count($ELE_IDArr);

		//loop each of the passing import ELE value
		for ($k=0; $k<$numOfELE; $k++) {

			$thisELE_ID = $ELE_IDArr[$k];
			$thisELE_Code = $existingEleCode[$thisELE_ID]['ELECode'];			

			if ($thisELE_Code == '') {
				return false;	
			}

		}

		return true;

	}
	private	function getConvertedEle($ParImportEle,$existingEleCode) {
	
		$ELE_IDArr = explode(";", $ParImportEle);
		$numOfELE = count($ELE_IDArr);
		$ELE_CodeArr = array();
		for ($k=0; $k<$numOfELE; $k++) {
			$thisELE_ID = $ELE_IDArr[$k];
			$thisELE_Code = $existingEleCode[$thisELE_ID]['ELECode'];			
			if ($thisELE_Code != '') {
				//$thisELE_Code != '' ==> find a code that map with the ID
				$ELE_CodeArr[] = $thisELE_Code;
			}
		}
		$ELE_CodeText = implode(',', (array)$ELE_CodeArr);
		
		return $ELE_CodeText;
	}
	private function checkWebSamsRegNoExist($parWebSAMSRegNo, $allStudentClassNameClassNumbmer){

		for($i = 0, $i_max = count($allStudentClassNameClassNumbmer);$i < $i_max;$i++){
			$_WebSAMSRegNo = trim($allStudentClassNameClassNumbmer[$i]['WebSAMSRegNo']);
			$_WebSAMSRegNo = preg_replace('/^#/','',$_WebSAMSRegNo);
			$parWebSAMSRegNo = preg_replace('/^#/','',$parWebSAMSRegNo);
			if($parWebSAMSRegNo == $_WebSAMSRegNo){
				return true;
			}
		}
		return false;
	}

	private function checkLoginNameExist($parLoginName,$allStudentClassNameClassNumbmer){
		for($i = 0, $i_max = count($allStudentClassNameClassNumbmer);$i  < $i_max;$i++){
			if($parLoginName == $allStudentClassNameClassNumbmer[$i]['UserLogin']){
				return true;
			}
		}
		return false;
	}

	
	private function checkClassNameClassNumberIsValid($parClassName, $parClassNumber,$allStudentClassNameClassNumbmer){

		if(trim($parClassName) == ''){
			//return false if class name is empty , class name must has value
			return false;
		}
		if(trim($parClassNumber) == ''){
			//return false if class number is empty , class number must has value
			return false;
		}
		for($i = 0, $i_max = count($allStudentClassNameClassNumbmer); $i < $i_max; $i++){
			$_className = $allStudentClassNameClassNumbmer[$i]['ClassName'];
			$_classNumber = $allStudentClassNameClassNumbmer[$i]['ClassNumber'];

			if((strtolower(trim($parClassName)) == strtolower(trim($_className))) && ($parClassNumber== $_classNumber)){
				//both class name and class number match to "A" student, return true
				return true;
			}
		}
		return false;
	}
	private function getAllYearTermInfo(){
		global $intranet_db;
		$objDB = new libdb();
		$sql = 'select y.AcademicYearID,y.YearNameEN,y.YearNameB5,t.YearTermID,t.YearTermNameEN,t.YearTermNameB5 from '.$intranet_db.'.ACADEMIC_YEAR as y inner join '.$intranet_db.'.ACADEMIC_YEAR_TERM as t on y.AcademicYearID = t.AcademicYearID order by y.Sequence;';
		$result = $objDB->returnResultSet($sql);

		return $result;
	}

	//queryType , return year, term id or just true / false  , 1 ==> return ture or false , 2==> return year , term id in pair
	//
	private function getYearAndTermInfo($parSchoolYear, $parSchoolTerm,$allYearTermInfo,$queryType){
		$parSchoolYear = trim($parSchoolYear);
		$parSchoolTerm = trim($parSchoolTerm);

		if($parSchoolYear == '' || $parSchoolTerm == ''){
			//if either $parSchoolYear or  $parSchoolTerm is empty , return false;
			return false;
		}
		for($i = 0,$i_max = count($allYearTermInfo); $i < $i_max; $i++){
//			debug_r($allYearTermInfo);
			//year info
			$_academicYearID = $allYearTermInfo[$i]['AcademicYearID'];
			$_yearNameEN = $allYearTermInfo[$i]['YearNameEN'];
			$_yearNameB5 = $allYearTermInfo[$i]['YearNameB5'];

			//term info
			$_yearTermID = $allYearTermInfo[$i]['YearTermID'];
			$_yearTermNameEN = $allYearTermInfo[$i]['YearTermNameEN'];
			$_yearTermNameB5 = $allYearTermInfo[$i]['YearTermNameB5'];

			$yearNameArray = array($_yearNameEN , $_yearNameB5);
			$termNameArray = array($_yearTermNameEN, $_yearTermNameB5);
			if(in_array($parSchoolYear,$yearNameArray) &&  in_array($parSchoolTerm,$termNameArray) ){
				if($queryType == 1){
					//
					return true;
				}else{
					$returnArray['AcademicYearID'] = $_academicYearID;
					$returnArray['YearTermID'] = $_yearTermID;
					return $returnArray;
				}
			}
		}
		return false;	
	}
	private function checkYearAndTermIsValid($parSchoolYear, $parSchoolTerm,$allYearTermInfo){

		$isValid = $this->getYearAndTermInfo($parSchoolYear, $parSchoolTerm,$allYearTermInfo,$queryType = 1);
		
		return $isValid;
	}
	private function getYearAndTermID($parSchoolYear, $parSchoolTerm,$allYearTermInfo){
		$returnArray = $this->getYearAndTermInfo($parSchoolYear, $parSchoolTerm,$allYearTermInfo,$queryType = 2);
		return $returnArray;
	}

	private function removePastBatchRecord(){
		global $eclass_db;
		//keep three day record only
//		$sql = 'delete FROM '.$eclass_db.'.OLE_IMPORT_RECORD WHERE DATE_SUB(CURDATE(),INTERVAL 3 DAY) >= DATE_INPUT;';
		$sql = 'delete FROM '.$eclass_db.'.OLE_IMPORT_RECORD WHERE DATE_SUB(CURDATE(),INTERVAL 1 DAY) >= DATE_INPUT;';
		$objDB = new libdb();
		$objDB->db_db_query($sql);
	}
}

?>