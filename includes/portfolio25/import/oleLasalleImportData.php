<?
include_once($intranet_root.'/includes/portfolio25/import/oleLasalleImportDataDefinition.php');
include_once($intranet_root.'/includes/portfolio25/slp/ole_program.php');
include_once($intranet_root.'/includes/portfolio25/slp/ole_student.php');
include_once($intranet_root."/includes/libpf-slp.php");
include_once($intranet_root."/includes/portfolio25/slp/libpf-ole.php");
include_once($intranet_root."/includes/portfolio25/oea/liboea_setting.php");

class oleLasalleImportData extends importData{

	private $allOLEProgram = null;
	private $allStudentClassNameClassNumbmer  = null;
	private $allTeacherAccount  = null;

	public function oleLasalleImportData(){
		$this->objImportDefinition = new oleLasalleImportDataDefinition();
	}	

	//passing $val should be a file with full paths. e.g /home/web/eclass40/intranetIP25/file/import_temp/iportfolio/ole//20120410_105701_1.CSV
	public function setBatchNo($val){
		$pattern = '/^.*\/(.*)\.csv/i';
		$replacement = '$1';
		$batchNo = preg_replace($pattern,$replacement,$val);
		$this->batchNo = $batchNo;
	}

	public function getBatchNo(){
		return $this->batchNo;
	}

	public function checkImportDataDetails(){
		$this->getAllTeacherAccount();
		$_data = $this->getImportDataSource();
		$objOea_setting = new liboea_setting();
		$objDB = new libdb();
		$errorResult = array();
		for($i = 0,$i_max = count($_data); $i < $i_max; $i++){
			$error = array();
			$teacherInCharge = $_data[$i]['TeacherInCharge'];

			$oeaYearStart = $_data[$i]['OEAYearStart'];
			$oeaYearEnd = $_data[$i]['OEAYearEnd'];

			$oeaCode = $_data[$i]['OEACode'];
			if(trim($oeaCode) == ''){

			}else{
				$oeaCode = str_pad($oeaCode, 5, '0', STR_PAD_LEFT);
			}

			$oeaStudentParticipationMode = $_data[$i]['OEAParticipationMode'];
			$oeaStudentAwardNature = $_data[$i]['OEAAwardNature'];
			$oeaStudentAwardBearing = $_data[$i]['OEAAwardBearing'];
			$oeaStudentAwardType = $_data[$i]['OEAAwardType'];

			//check oeaYearStart valid
			if (preg_match("/^[0-9]{4}/", $oeaYearStart)) {

			} else {
				$this->registerError('OEA Start Year Error',$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongOeaYear'],$error);
			}

			//check oeaYearEnd valid
			if (preg_match("/^[0-9]{4}/i", $oeaYearEnd)) {

			} else {
				$this->registerError('OEA End Year Error',$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongOeaYear'],$error);
			}

			//check oea code exist

			if(trim($oeaCode) != '' && count($objOea_setting->get_OEA_Item($oeaCode)) >0) {
				//oea code does not empty and match a item in the OEA item provdied from JUPAS
			}else{
					$this->registerError('OEA Code Error',$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['OEACodeNotExist'],$error);
			}
		
			//check OEAAwardNature exist
			if($this->getOEAStudentAwardNatureExist($oeaStudentAwardNature)){

			}else{
				$this->registerError('OEA Award Nature Error',$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardNature'],$error);
			}
		

			//check OEAAwardBearing exist
			if($this->getOEAStudentAwardBearingExist($oeaStudentAwardBearing)){

			}else{
				$this->registerError('OEA Award Bearing Error',$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardBearing'],$error);
			}


			//check OEAAwardType exist
			if($this->getOEAStudentAwardTypeExist($oeaStudentAwardType)){

			}else{
				$this->registerError('OEA Award Type Error',$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongAwardType'],$error);
			}

			//check OEAParticipationMode exist
			if($this->getOEAStudentParticipationModeExist($oeaStudentParticipationMode)){

			}else{
				$this->registerError('OEA_ROLE_ERROR',$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['WrongParticipationMode'],$error);
			}
			

			if($this->getStudentOEAAwardIsValid($oeaStudentAwardBearing,$oeaStudentAwardNature ,$oeaStudentAwardType)){

			}else{
				$this->registerError('OEA Award Error',$Lang['iPortfolio']['OEA']['OLEMappingArr']['ImportErrorMsgArr']['AwardDontMatch'],$error);
			}

			if(is_numeric($this->getTeacherIntranetID($teacherInCharge))){
			}else{
				$this->registerError('TeacherInChargeError',$Lang['iPortfolio']['IMPORT']['TeacherLoginIDNotFind'],$error);
			}

			$this->pushToError($i,$error);
		}


	}
	private function getOEAStudentParticipationModeExist($checkRole){
		$checkRole = strtolower($checkRole);

		$defaultRole= array('l', 'c', 'm');


		if(in_array($checkRole,$defaultRole)){
			return true;
		}else{
			return false;
		}

	}


	private function getOEAStudentAwardNatureExist($checkAwardNature){
		$checkAwardNature = strtolower($checkAwardNature);

		$defaultAwardNature= array('c', 'n', 'p');

		if(in_array($checkAwardNature,$defaultAwardNature)){
			return true;
		}else{
			return false;
		}

	}

	private function getOEAStudentAwardBearingExist($checkAwardBearing){
		$checkAwardBearing = strtolower($checkAwardBearing);

		$defaultAwardBearing= array('y','n');

		if(in_array($checkAwardBearing,$defaultAwardBearing)){
			return true;
		}else{
			return false;
		}

	}
	private function getOEAStudentAwardTypeExist($checkAwardType){
		$checkAwardType = strtolower($checkAwardType);

		$defaultAwardType= array('g','b','o','n');

		if(in_array($checkAwardType,$defaultAwardType)){
			return true;
		}else{
			return false;
		}

	}
	private function getStudentOEAAwardIsValid($studentAwardBearing,$studentAwardNature,$studentAwardType){
		$studentAwardBearing = strtolower($studentAwardBearing);
		$studentAwardNature = strtolower($studentAwardNature);
		$studentAwardType = strtolower($studentAwardType);

		if($studentAwardBearing == 'n'){
			if($studentAwardNature == 'c'){
				return false;
			}
			if($studentAwardType != 'n'){
				return false;
			}
		}

		return true;
	}

	public function processImportDataDetails(){
		global $eclass_db;
		$ParticipantType = 'S';
		$this->getAllStudentClassNameClassNumber();
		$_data = $this->getImportDataSource();
		$this->getAllTeacherAccount();
		$thisBatchSavedOLERecord = $this->getThisBatchSavedOLERecord($this->getBatchNo());

		$objoea_setting = new liboea_setting();

		$objDB = new libdb();
		for($i = 0,$i_max = count($_data); $i < $i_max; $i++){
			$lineNo = $i;
			$_startDate = $_data[$i]['StartDate'];
			$oleTitle = $_data[$i]['OLETitle'];
			$oeaCode = $_data[$i]['OEACode'];

			$oeaCode = str_pad($oeaCode, 5, '0', STR_PAD_LEFT);

			$oeaDetailsFormJupas= $objoea_setting->get_OEA_Item($oeaCode);
			$oeaTitle = $oeaDetailsFormJupas[$oeaCode]['ItemName'];


			$oeaYearStart = $_data[$i]['OEAYearStart'];
			$oeaYearEnd = $_data[$i]['OEAYearEnd'];
			
			$oeaStudentParticipationMode = $_data[$i]['OEAParticipationMode'];
			$oeaStudentAwardNature = $_data[$i]['OEAAwardNature'];
			$oeaStudentAwardBearing = $_data[$i]['OEAAwardBearing'];
			$oeaStudentAwardType = $_data[$i]['OEAAwardType'];



										
			$_className = $_data[$i]['ClassName'];
			$_classNumber = $_data[$i]['ClassNumber'];
			$teacherInCharge = $_data[$i]['TeacherInCharge'];




			$oleIDInfoAry = $this->getRelatedProgramIdAndStudentRecordIdForThisLine($lineNo,$thisBatchSavedOLERecord);

			$studentID = $oleIDInfoAry['OLE_STUDENT_ID'];
			$oleProgramId = $oleIDInfoAry['OLE_PROGRAM_ID'];
			$oleStudentRecordId = $oleIDInfoAry['OLE_STUDENT_RECORD_ID'];


			//Handle the Pre MAP of Program 
			if($oleProgramId > 0){			
				$sql = 'insert into '.$eclass_db.'.OEA_OLE_MAPPING (OEA_ProgramCode , OLE_ProgramID) value('.$oeaCode.','.$oleProgramId.')';
				$objDB->db_db_query($sql);
			}


			if(intval($studentID) > 0){

				$dbBatchValue["StudentID"]	= $objDB->pack_value($studentID,"int");
				$dbBatchValue["Title"]	= $objDB->pack_value($oeaTitle,'str');
				$dbBatchValue["StartYear"]	= $objDB->pack_value($oeaYearStart,'int');
				$dbBatchValue["EndYear"]	= $objDB->pack_value($oeaYearEnd,'int');
				$dbBatchValue["OEA_AwardBearing"]	= $objDB->pack_value($oeaStudentAwardBearing,'str');
				$dbBatchValue["Role"]	= $objDB->pack_value($oeaStudentParticipationMode,'str');
				$dbBatchValue["ParticipationNature"]	= $objDB->pack_value($oeaStudentAwardNature,'str');
				$dbBatchValue["Achievement"]	= $objDB->pack_value($oeaStudentAwardType,'str');
				$dbBatchValue["OEA_ProgramCode"]	= $objDB->pack_value($oeaCode,'str');
				$dbBatchValue["OLE_STUDENT_RecordID"]	= $objDB->pack_value($oleStudentRecordId,'int');
				$dbBatchValue["OLE_PROGRAM_ProgramID"]	= $objDB->pack_value($oleProgramId,'int');
				$dbBatchValue["InputBy"]	= $objDB->pack_value($_SESSION['UserID'],'int');
				$dbBatchValue["InputDate"]	= $objDB->pack_value('now()','date');
				$dbBatchValue["ModifiedBy"]	= $objDB->pack_value($_SESSION['UserID'],'int');
				$dbBatchValue["ModifyDate"]	= $objDB->pack_value('now()','date');
				$dbBatchValue["Participation"]	= $objDB->pack_value($ParticipantType,'str');


				$dbFieldArray = $objDB->concatFieldValueToSqlStr($dbBatchValue);
				$dbField = $dbFieldArray['sqlField'];
				$saveValue = $dbFieldArray['sqlValue'];
				$sql = "insert into ".$eclass_db.".OEA_STUDENT_PRE_SELECT (".$dbField.")	values (".$saveValue.")";
				$objDB->db_db_query($sql);

			}

			$teacherUserID = $this->getTeacherIntranetID($teacherInCharge);
			if(is_numeric($teacherUserID)){
				$sql = 'update '.$eclass_db.'.OLE_PROGRAM set TeacherInCharge = '.$teacherUserID.' where ProgramID = '.$oleProgramId;
				$objDB->db_db_query($sql);
			}		
		}
	}

	private function getOLEProgramId($programTitle, $programStartDate){
		if($this->allOLEProgram == null){
			$this->getAllOLEProgram();
		}

		$programID = 0;
		for($i = 0, $i_max = count($this->allOLEProgram);$i < $i_max;$i++){

			if(	
				($this->allOLEProgram[$i]['Title'] == $programTitle) && 
				($this->allOLEProgram[$i]['StartDate'] == $programStartDate)
			   ){
				$programID = $this->allOLEProgram[$i]['ProgramID'];

				//find the program ID , stop the loop here
				break;
			}
		}
		return $programID;



	}
	private function getAllOLEProgram(){
		global $eclass_db;
		global $ipf_cfg;
		$sql = 'select ProgramID, Title,StartDate,ELE from '.$eclass_db.'.OLE_PROGRAM where IntExt=\''.$ipf_cfg["OLE_TYPE_STR"]["INT"].'\'';

		$objDB = new libdb();
		$this->allOLEProgram = $objDB->returnResultSet($sql);
	}
	private function getAllStudentClassNameClassNumber(){
				global $intranet_db;

		if($this->allStudentClassNameClassNumbmer == null){
		}else{
			//$this->allStudentClassNameClassNumbmer not null , init before , just return is ok
			return; 
		}
		$objDb = new libdb();

		$sql = 'select ClassName , ClassNumber,UserID,UserLogin from '.$intranet_db.'.INTRANET_USER where RecordType =2';
		$result = $objDb->returnResultSet($sql);

		$this->allStudentClassNameClassNumbmer = $result;
	}

	private function getStudentIntranetID($checkClassName , $checkClassNumber){

		for($i = 0, $i_max = count($this->allStudentClassNameClassNumbmer); $i < $i_max; $i++){
			$_className = $this->allStudentClassNameClassNumbmer[$i]['ClassName'];
			$_classNumber = $this->allStudentClassNameClassNumbmer[$i]['ClassNumber'];
			$_UserID = $this->allStudentClassNameClassNumbmer[$i]['UserID'];

			if(($checkClassName == $_className) && ($checkClassNumber== $_classNumber)){
				return $_UserID;
			}
		}
		return NULL;
	}

	private function getAllTeacherAccount(){
				global $intranet_db;

		if($this->allTeacherAccount == null){
		}else{
			return; 
		}
		$objDb = new libdb();

		$sql = 'select UserID,UserLogin from '.$intranet_db.'.INTRANET_USER where RecordType =1';
		$result = $objDb->returnResultSet($sql);

		$this->allTeacherAccount = $result;

	}

	private function getTeacherIntranetID($userLogin){

		for($i = 0, $i_max = count($this->allTeacherAccount); $i < $i_max; $i++){
			$_userID = $this->allTeacherAccount[$i]['UserID'];
			$_userLogin = $this->allTeacherAccount[$i]['UserLogin'];


			if(($_userLogin == $userLogin)){

				return $_userID;
			}
		}
		return NULL;
	}
		private function getThisBatchSavedOLERecord($batchNo){
			global $eclass_db;
			$objDB = new libdb();
			if(trim($batchNo) == ''){
				return NULL;
			}else{
				$sql = '
						select IMPORT_LINE_NO,OLE_PROGRAM_ID,OLE_STUDENT_RECORD_ID,OLE_STUDENT_ID,IMPORT_BATCH_NO,DATE_INPUT from '.$eclass_db.'.OLE_IMPORT_RECORD where IMPORT_BATCH_NO = \''.$batchNo.'\' and IMPORT_BATCH_NO <> \'\'
					  ';

				return $objDB->returnResultSet($sql);
			}
		}
		private function getRelatedProgramIdAndStudentRecordIdForThisLine($lineNo, $DBRecord){

			for($i = 0,$i_max = count($DBRecord); $i < $i_max; $i++){
				$_importLineNo = intval($DBRecord[$i]['IMPORT_LINE_NO']);


				if($lineNo == $_importLineNo){
					$dataAry['IMPORT_LINE_NO'] = $_importLineNo;
					$dataAry['OLE_PROGRAM_ID'] = $DBRecord[$i]['OLE_PROGRAM_ID'];
					$dataAry['OLE_STUDENT_RECORD_ID'] = $DBRecord[$i]['OLE_STUDENT_RECORD_ID'];
					$dataAry['OLE_STUDENT_ID'] = $DBRecord[$i]['OLE_STUDENT_ID'];					

					return  $dataAry;
				}
			}
			return NULL;
		}
}
?>