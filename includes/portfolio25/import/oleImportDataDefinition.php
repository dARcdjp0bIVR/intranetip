<?
class oleImportDataDefinition extends importDataDefinition{
	public function oleImportDataDefinition(){
		$this->fields = array(
				'ClassName' => array(
					'description' => "Student Class Name",
					'data_type_description' => "String",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => '6A',
					'remark' =>'You may skip it , if WebSAMSRegNo or  LoginName input',
				),
				'ClassNumber' => array(
					'description' => "Student Class Number",
					'data_type_description' => "int",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => '12',
					'remark' =>'You may skip it , if WebSAMSRegNo or  LoginName input',
				),
				'WebSAMSRegNo' => array(
					'description' => "Student Websams Number",
					'data_type_description' => "String",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => '#760213',
					'remark' =>'You may skip it , if Student Class Name/ Student Class Number  or  LoginName input',
				),
				'LoginName' => array(
					'description' => "Student Login Name",
					'data_type_description' => "String",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => 'chanTaiYin',
					'remark' =>'You may skip it , if Student Class Name/ Student Class Number  or  LoginName WebSAMSRegNo',
				),
				'OLETitle' => array(
					'description' => "OLE Program Title",
					'data_type_description' => "String",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => 'Football Team',
				),
				'StartDate' => array(
					'description' => "Program Start Date",
					'data_type_description' => "yyyy-mm-dd",
					'data_mandatory' => $this->cfgFieldTypeValue['is_mandatory'],
					'sample_data' => '2012-02-03',
					'remark' =>'Program with the same Start Date , Program Name and ComponentCode will be recongized as a one Program',
				),
				'EndDate' => array(
					'description' => "Program End Date",
					'data_type_description' => "yyyy-mm-dd",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => '2012-02-04',
				),
				'SchoolYear' => array(
					'description' => "OLE School Year",
					'data_type_description' => "Depend on setting",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => '2011-2012',
					'remark' =>'You may skip it , School Year will depend on the Start Date',
				),
				'SchoolTerm' => array(
					'description' => "OLE School Term",
					'data_type_description' => "Depend on setting",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => 'Term2',
					'remark' =>'You may skip it , School Term will depend on the Start Date',
				),
				'ProgramRemark' => array(
					'description' => "OLE PROGRAM Remark",
					'data_type_description' => "String",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => 'This program is very good',
				),
				'Organization' => array(
					'description' => "OLE PROGRAM Organization",
					'data_type_description' => "String",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => 'HK Goverment',
				),
				'Role' => array(
					'description' => "OLE Student Role",
					'data_type_description' => "String",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => 'Leader',
				),
				'Hour' => array(
					'description' => "OLE STUDENT HOUR",
					'data_type_description' => "Number",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => '12',
				),
				'Achievement' => array(
					'description' => "OLE STUDENT Achievement",
					'data_type_description' => "String",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => 'Very Good',
				),
				'Category' => array(
					'description' => "OLE Program Category",
					'data_type_description' => "Number",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => '1',
				),
				'ComponentCode' => array(
					'description' => "OLE Component Code",
					'data_type_description' => "Number",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => '1;2;3',
				),
				'Details' => array(
					'description' => "OLE Details",
					'data_type_description' => "String",
					'data_mandatory' => $this->cfgFieldTypeValue['is_optional'],
					'sample_data' => 'This is school details',
				),
		);
	}
}
?>