<?
# modifying : 

if (!defined("LIBOEA_ITEM_DEFINED"))         // Preprocessor directives
{
  define("LIBOEA_ITEM_DEFINED",true);

	class liboea_item {
		private $objDB;
		private $recordID;

		private $StudentID;
		private $Title;
		private $StartYear;
		private $EndYear;
		private $Participation;
		private $Role;
		private $ParticipationNature;
		private $Achievement;
		private $Description;
		private $OEA_ProgramCode;
		private $OLE_STUDENT_RecordID;
		private $OLE_PROGRAM_ProgramID;
		private $RecordStatus;
		private $InputDate;
		private $InputBy;
		private $ApprovalDate;
		private $ApprovedBy;
		private $ModifyDate;
		private $ModifiedBy;
		private $MaxOEAItem;
		private $TempStudentID;
		private $TempTitle;
		private $TempStartYear;
		private $TempEndYear;
		private $TempAwardBearing;
		private $TempParticipation;
		private $TempParticipationNature;
		private $TempRole;
		private $TempAchievement;
		private $TempDescription;
		private $TempApprovalDate;
		private $TempApprovedBy;
		private $TempOEA_ProgramCode;
		private $TempOLE_STUDENT_RecordID;
		private $TempOLE_PROGRAM_ProgramID;
		private $TempRecordStatus;
		private $NewRecord;
		private $EditMode;
		

//		public function liboea_item(int $recordID = null){
		public function liboea_item($recordID = null){
			global $eclass_db, $oea_cfg;
			
			include_once("liboea.php");
			$liboea = new liboea();
			
			$this->objDB = new libdb();
			$this->MaxOEAItem = $liboea->getMaxNoOfOea();
			//$this->OEA_AwardBearing = "N";
			$this->EditMode = 0;
			
			//LOAD THE OEA DETAILS FORM STOREAGE (EG DB)
			if($recordID != null)
			{
				$this->recordID = intval($recordID);
				$this->loadRecordFromStorage();
			}
		}

		public function setStudentID($sID){
			$this->StudentID = $sID;
		}
		public function getStudentID(){
			return $this->StudentID;
		}
		public function setTitle ($str){
			$this->Title = $str;
		}
		public function getTitle(){
			return $this->Title;
		}
		public function setStartYear($intStarYear){
			$this->StartYear = $intStarYear;
		}
		public function getStartYear(){
			return $this->StartYear;
		}
		public function setEndYear($intEndYear){
			$this->EndYear = $intEndYear;
		}
		public function getEndYear(){
			return $this->EndYear;
		}
		public function getOEAYear() {
			global $i_To;
			return $this->StartYear.(($this->EndYear=="") ? "" : " ".$i_To." ".$this->EndYear);	
		}
		public function get_Edit_OEAYear() {
			global $i_To;
			$editYear = "<input type='text' name='StartYear' id='StartYear' size='4' laxlength='4' value='".$this->StartYear."'>";
			$editYear .= "<input type='text' name='EndYear' id='EndYear' size='4' laxlength='4' value='".$this->EndYear."'>";
			return $editYear;
		}
		public function setParticipation($str){
			$this->Participation = $str;
		}
		public function getParticipation(){
			return $this->Participation;
		}
		public function setRole($str){
			$this->Role= $str;
		}
		public function getRole(){
			return $this->Role;
		}
		public function setParticipationNature($str){
			$this->ParticipationNature= $str;
		}
		public function getParticipationNature(){
			return $this->ParticipationNature;
		}
		public function setOEA_AwardBearing($str){
			$this->OEA_AwardBearing= $str;
		}
		public function getOEA_AwardBearing(){
			return $this->OEA_AwardBearing;
		}
		public function setAchievement($str){
			$this->Achievement= $str;
		}
		public function getAchievement(){
			return $this->Achievement;
		}
		public function setDescription($str){
			$this->Description= $str;
		}
		public function getDescription(){
			return $this->Description;
		}
		public function setOEA_ProgramCode($str){
			$this->OEA_ProgramCode= $str;
		}
		public function getOEA_ProgramCode(){
			return $this->OEA_ProgramCode;
		}
		public function setOLE_STUDENT_RecordID($intID){
			$this->OLE_STUDENT_RecordID= $intID;
		}
		public function getOLE_STUDENT_RecordID(){
			return $this->OLE_STUDENT_RecordID;
		}
		public function setOLE_PROGRAM_ProgramID($intID){
			$this->OLE_PROGRAM_ProgramID= $intID;
		}
		public function getOLE_PROGRAM_ProgramID(){
			return $this->OLE_PROGRAM_ProgramID;
		}
		public function setRecordStatus($intStatus){
			$this->RecordStatus= $intStatus;
		}
		public function getRecordStatus(){
			return $this->RecordStatus;
		}
		public function setInputDate($value){
			$this->InputDate= $value;
		}
		public function getInputDate(){
			return $this->InputDate;
		}
		public function setInputBy($intValue){
			$this->InputBy= $intValue;
		}
		public function getInputBy(){
			return $this->InputBy;
		}
		public function setApprovalDate($value){
			$this->ApprovalDate= $value;
		}
		public function getApprovalDate(){
			return $this->ApprovalDate;
		}
		public function setApprovedBy($intValue){
			$this->ApprovedBy= $intValue;
		}
		public function getApprovedBy(){
			return $this->ApprovedBy;
		}
		public function setModifyDate($value){
			$this->ModifyDate= $value;
		}
		public function getModifyDate(){
			return $this->ModifyDate;
		}
		public function setModifiedBy($intValue){
			$this->ModifiedBy= $intValue;
		}
		public function getModifiedBy(){
			return $this->ModifiedBy;
		}
		public function setTempStudentID($value) {
			$this->TempStudentID = $value;
		}
		public function setTempTitle($value) {
			$this->TempTitle = $value;
		}
		public function setTempStartYear($value) {
			$this->TempStartYear = $value;
		}
		public function setTempEndYear($value) {
			$this->TempEndYear = $value;
		}
		public function setTempAwardBearing($value) {
			$this->TempAwardBearing = $value;
		}
		public function setTempParticipation($value) {
			$this->TempParticipation = $value;
		}
		public function setTempParticipationNature($value) {
			$this->TempParticipationNature = $value;
		}
		public function setTempRole($value) {
			$this->TempRole = $value;
		}
		public function setTempAchievement($value) {
			$this->TempAchievement = $value;
		}
		public function setTempDescription($value) {
			$this->TempDescription = $value;
		}
		public function setTempApprovalDate($value) {
			$this->TempApprovalDate = $value;
		}
		public function setTempApprovedBy($value) {
			$this->TempApprovedBy = $value;
		}
		public function setTempOEA_ProgramCode($value) {
			$this->TempOEA_ProgramCode = $value;
		}
		public function setTempOLE_STUDENT_RecordID($value) {
			$this->TempOLE_STUDENT_RecordID = $value;
		}
		public function setTempOLE_PROGRAM_ProgramID($value) {
			$this->TempOLE_PROGRAM_ProgramID = $value;
		}
		public function setTempRecordStatus($value) {
			$this->TempRecordStatus = $value;
		}
		public function setNewRecord($val) {
			$this->NewRecord = $val;
		}
		public function getNewRecord() {
			return $this->NewRecord;	
		}
		public function getMaxNoOfOea() {
			return $this->MaxOEAItem;	
		}
		public function setEditMode($val) {
			$this->EditMode = $val;
		}
		
		
		public function getDefaultParticipation($associateAry=1)
		{
			global $oea_cfg;
			if($associateAry)
				return $oea_cfg["OEA_Participation_Type"];
			else  {
				$_c = 0;
				if(sizeof($oea_cfg["OEA_Participation_Type"])>0) {
					foreach($oea_cfg["OEA_Participation_Type"] as $_key=>$_val) {
						$_ary[$_c][] = $_key;
						$_ary[$_c][] = $_val;
						$_c++;
					}	
				}
				return $_ary;	
			}	
		}

		private function loadRecordFromStorage(){
			global $eclass_db;			
			$result = null;
			
			$_recordID = $this->recordID;

			if( (trim($_recordID) == "") || (intval($_recordID) < 0)) {
				//DO NOTHING
//				$result = null;

			}else{
				
				$sql = "select		
							RecordID,StudentID,Title,
							StartYear,EndYear,OEA_AwardBearing, Participation,
							Role,ParticipationNature,Achievement, Description,
							OEA_ProgramCode,OLE_STUDENT_RecordID,OLE_PROGRAM_ProgramID,
							RecordStatus,
							InputDate,InputBy,ApprovalDate,ApprovedBy,ModifyDate,ModifiedBy 
						from 
							{$eclass_db}.OEA_STUDENT 
						where 
							RecordID = ".$this->recordID;
				$resultSet = $this->objDB->returnArray($sql);
				
				if(is_array($resultSet) && sizeof($resultSet) == 1){
					$this->StudentID = $resultSet[0]["StudentID"];
					$this->Title = $resultSet[0]["Title"];
					$this->StartYear = $resultSet[0]["StartYear"];
					$this->EndYear = $resultSet[0]["EndYear"];
					$this->OEA_AwardBearing = $resultSet[0]["OEA_AwardBearing"];
					$this->Participation = $resultSet[0]["Participation"];
					$this->Role = $resultSet[0]["Role"];
					$this->ParticipationNature = $resultSet[0]["ParticipationNature"];
					$this->Achievement = $resultSet[0]["Achievement"];
					$this->Description = $resultSet[0]["Description"];
					$this->OEA_ProgramCode = $resultSet[0]["OEA_ProgramCode"];
					$this->OLE_STUDENT_RecordID = $resultSet[0]["OLE_STUDENT_RecordID"];
					$this->OLE_PROGRAM_ProgramID = $resultSet[0]["OLE_PROGRAM_ProgramID"];
					$this->RecordStatus = $resultSet[0]["RecordStatus"];
					$this->InputDate = $resultSet[0]["InputDate"];
					$this->InputBy = $resultSet[0]["InputBy"];
					$this->ApprovalDate = $resultSet[0]["ApprovalDate"];
					$this->ApprovedBy = $resultSet[0]["ApprovedBy"];
					$this->ModifyDate = $resultSet[0]["ModifyDate"];
					$this->ModifiedBy  = $resultSet[0]["ModifiedBy"];
				}
			}
//			return $result;

		}
		
		
		

		public function loadRecordFromOLEMapping($ole_record_id){
			global $eclass_db;
			
				
			$sql = "select		
						* 
					from 
						{$eclass_db}.OEA_OLE_MAPPING 
					where 
						OLE_ProgramID = ".$ole_record_id;
			$resultSet = $this->objDB->returnArray($sql);
			if(is_array($resultSet) && sizeof($resultSet) == 1)
			{
				$this->Title = $resultSet[0]["Title"];
				$this->OEA_AwardBearing = $resultSet[0]["DefaultAwardBearing"];
				$this->OEA_ProgramCode = $resultSet[0]["OEA_ProgramCode"];
				
				return true;
			}
			return false;

		}

		public function save()
		{
			if((trim($this->recordID) != "") && (intval($this->recordID) > 0)) {
				$resultId = $this->update_record();
			}
			else{	
				$resultId = $this->new_record();
			}

			return $resultId;

		}
		
		private function update_record()
		{
			global $eclass_db, $UserID, $oea_cfg;
			
			include_once("liboea.php");
			$liboea = new liboea();
			
//			if($_SESSION['UserType']==USERTYPE_STUDENT) {
//				if($liboea->getOeaAutoApproval()==1 || $this->TempParticipation=='P') {
//					$field = "RecordStatus='".$oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"]."', ";
//				} else {
//					$field = "RecordStatus='".$oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"]."', ";	
//				}	
//			} else {
//				$field = "RecordStatus='".$oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"]."', ";
//			}

			$sql = "UPDATE 
						{$eclass_db}.OEA_STUDENT 
					SET 
						Title='".$this->objDB->Get_Safe_Sql_Query($this->TempTitle)."', 
						StartYear='".$this->TempStartYear."', 
						EndYear='".$this->TempEndYear."', 
						OEA_AwardBearing='".$this->TempAwardBearing."', 
						Participation='".$this->TempParticipation."', 
						Role='".$this->TempRole."', 
						ParticipationNature='".$this->TempParticipationNature."', 
						Achievement='".$this->TempAchievement."', 
						Description='".$this->objDB->Get_Safe_Sql_Query($this->TempDescription)."',
						OEA_ProgramCode='".$this->TempOEA_ProgramCode."', 
						RecordStatus='".$this->TempRecordStatus."',
						ModifyDate=NOW(), 
						ModifiedBy='$UserID'
					WHERE
						RecordID = '".$this->recordID."'";

			$result = $this->objDB->db_db_query($sql);
			
			return $result;

		}
		
		private function new_record()
		{
			global $eclass_db, $UserID;
			
			# check duplication
			$sql = "SELCET COUNT(*) FROM {$eclass_db}.OEA_STUDENT WHERE StudentID='".$this->TempStudentID."' AND OLE_STUDENT_RecordID='".$this->TempOLE_STUDENT_RecordID."'";
			$count = $this->objDB->returnVector($sql);
			
			if($count[0]==0) {
				include_once("liboea.php");
				$liboea = new liboea();
				
//				if($liboea->getOeaAutoApproval()==1 || $this->TempParticipation=='P') {
//					$approveField = ", ApprovalDate, ApprovedBy";	
//					$approveValue = ", NOW(), '$UserID'";
//				}
				
				if ($this->TempApprovalDate != '') {
					$approveDateField = ", ApprovalDate";
					$approveDateValue = ", ".$this->TempApprovalDate;
				}
				if ($this->TempApprovedBy != '') {
					$approvedByField = ", ApprovedBy";
					$approvedByValue = ", ".$this->TempApprovedBy;
				}
				
				$sql = "INSERT INTO {$eclass_db}.OEA_STUDENT
							(StudentID, Title, StartYear, EndYear, OEA_AwardBearing, Participation, Role, ParticipationNature, Achievement, Description, OEA_ProgramCode, OLE_STUDENT_RecordID, OLE_PROGRAM_ProgramID, RecordStatus, InputDate, InputBy, ModifyDate, ModifiedBy $approveDateField $approvedByField)
						VALUES 
							('".$this->TempStudentID."', '".$this->objDB->Get_Safe_Sql_Query($this->TempTitle)."', '".$this->TempStartYear."', '".$this->TempEndYear."', '".$this->TempAwardBearing."', '".$this->TempParticipation."', '".$this->TempRole."', '".$this->TempParticipationNature."', '".$this->TempAchievement."', '".$this->objDB->Get_Safe_Sql_Query($this->TempDescription)."', '".$this->TempOEA_ProgramCode."', '".$this->TempOLE_STUDENT_RecordID."', '".$this->TempOLE_PROGRAM_ProgramID."', '".$this->TempRecordStatus."', NOW(), '$UserID', NOW(), '$UserID' $approveDateValue $approvedByValue)
						";
				$result = $this->objDB->db_db_query($sql);
				$thisRecordID = $this->objDB->db_insert_id();
				$this->recordID = $thisRecordID;
			}
			
			return $this->recordID;
		}
		
		
		public function get_OEA_Role_Array()
		{
			global $oea_cfg;
			$ary = $oea_cfg["OEA_Role_Type"];
			return $ary; 	
		}		
		public function Get_OEA_RoleName_By_RoleCode($Code)
		{
			$InfoAssoArr = $this->get_OEA_Role_Array();
			return $InfoAssoArr[$Code];
		}
		
		public function get_OEA_Achievement_Array()
		{
			global $oea_cfg;
			$ary = $oea_cfg["OEA_Achievement_Type"];
			return $ary; 	
		}		
		public function Get_OEA_AchievementName_By_AchievementCode($Code)
		{
			$InfoAssoArr = $this->get_OEA_Achievement_Array();
			return $InfoAssoArr[$Code];
		}
		
		public function get_OEA_ParticipationNature_Array()
		{
			global $oea_cfg;
			$ary = $oea_cfg["OEA_ParticipationNature_Type"];
			return $ary;	
		}		
		public function Get_OEA_ParticipationNatureName_By_ParticipationNatureCode($Code)
		{
			$InfoAssoArr = $this->get_OEA_ParticipationNature_Array();
			return $InfoAssoArr[$Code];
		}
		
		public function get_OEA_Participation_Array()
		{
			global $oea_cfg;
			$ary = $oea_cfg["OEA_Participation_Type"];
			return $ary;	
		}		
		public function Get_OEA_ParticipationName_By_ParticipationCode($Code)
		{
			$InfoAssoArr = $this->get_OEA_Participation_Array();
			return $InfoAssoArr[$Code];
		}
		
		public function get_OEA_AwardBearingName_By_AwardBearingCode($Code) {
			global $oea_cfg, $Lang;
			
			$Name = '';
			if ($Code == $oea_cfg["OEA_AwardBearing_Type_Yes"]) {
				$Name = $Lang['iPortfolio']['OEA']['OEA_AwardBearing'];
			}
			else if ($Code == $oea_cfg["OEA_AwardBearing_Type_No"]) {
				$Name = $Lang['iPortfolio']['OEA']['OEA_NotAwardBearing'];
			}
			
			return $Name;
		}
		
		public function get_OEA_Pending_Status()
		{
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"];
		}
		
		public function get_OEA_Approved_Status()
		{
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"];
		}
		
		public function get_OEA_Rejected_Status()
		{
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_RECORDSTATUS_REJECTED"];
		}
		
		public function OeaQuotaUsed() {
			global $eclass_db, $UserID;
			$sql = "SELECT COUNT(*) FROM {$eclass_db}.OEA_STUDENT WHERE StudentID='$UserID'";
			$count = $this->objDB->returnVector($sql);
			return $count[0];
		}
		public function OeaQuotaRemain() {
			include_once("liboea.php");
			$liboea = new liboea();
			$maxOeaAllow = $liboea->getMaxNoOfOea();
			$OeaUsed = $this->OeaQuotaUsed();
			return ($maxOeaAllow - $OeaUsed);
		}
		
		public function checkDisplayNewButton()
		{
			global $UserID, $eclass_db;
			
			$count = $this->OeaQuotaUsed();

			return ($count>=$this->MaxOEAItem) ? 0 : 1;	
		}

		public function getAwardBearing($ItemCode="", $OLE_ProgramID="")
		{
			global $oea_cfg, $eclass_db;
			
			if($ItemCode!="") {
				$sql = "SELECT DefaultAwardBearing FROM {$eclass_db}.OEA_OLE_MAPPING WHERE OEA_ProgramCode='$ItemCode'";
				$awardBearing = $this->objDB->returnVector($sql);
				
			} else if($OLE_ProgramID!="") {
				$sql = "SELECT DefaultAwardBearing FROM {$eclass_db}.OEA_OLE_MAPPING WHERE OLE_ProgramID='$OLE_ProgramID'";
				$awardBearing = $this->objDB->returnVector($sql);
			}
			return ($awardBearing[0]=="Y") ? 1 : 0;
		}
		
		public function getOeaItem_NewRelated()
		{	# create / edit OEA record which is independent on OLE 
				
		}
		
		public function getOeaItemTable($IsForOLEEdit=false, $engTitle='', $oleProgramId='', $fromJupasMappingSettings=false)
		{	# create / edit OEA record
			$table = $this->get_New_Edit_OEA_Table($IsForOLEEdit, $engTitle, $oleProgramId, $fromJupasMappingSettings);
			return $table;
		}
		
		private function get_Edit_OEA_Table() 
		{
			# Do nothing
		}
		
		private function get_New_Edit_OEA_Table($IsForOLEEdit=false, $engTitle='', $oleProgramId='', $fromJupasMappingSettings=false)
		{
			global $Lang, $PATH_WRT_ROOT, $intranet_session_language, $i_To, $oea_cfg, $i_general_yes, $i_general_no, $i_general_or, $button_edit, $linterface;

			include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libole_student_item.php");
			include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting.php");
			include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
			include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea_setting_search.php");
			
			$liboea = new liboea();
			$liboea_setting = new liboea_setting();
			$liboea_setting_search = new liboea_setting_search($liboea_setting);
			

			# OLE Item 
			if($this->TempOLE_STUDENT_RecordID=="") {		# retrieve OLE data from OEA id
				$ole_item = new libole_student_item($this->OLE_STUDENT_RecordID);
			} else { 										# retrieve OLE data from OLE id
				$ole_item = new libole_student_item($this->TempOLE_STUDENT_RecordID);
			}

			# OLE Date	
			$ole_date = $ole_item->getOLEDate();
			
			# OLE Role
			$ole_role = ($ole_item->getRole()=="") ? "-" : $ole_item->getRole();
			
			# OLE Achievement 
			$ole_achievement = ($ole_item->getAchievement()=="") ? "-" : $ole_item->getAchievement();
			
			# OLE Details
			//$ole_details = $ole_item->getDetails();
			//$ole_details = ($ole_details=='')? '&nbsp;' : $ole_details;
			$ole_student_details = trim($ole_item->getDetails());
			$ole_program_details = trim($ole_item->getProgramDetails());
			$ole_details = '';
			if ($ole_student_details=='' && $ole_program_details=='') {
				$ole_details .= $Lang['General']['EmptySymbol'];
			}
			else {
				if($ole_program_details == $ole_student_details){
					//if ole program details is equal to ole student details show one row only
					$ole_details .= '<b>'.$Lang['iPortfolio']['OEA']['DetailsForOle'].' / '.$Lang['iPortfolio']['OEA']['DetailsFromStudent'].'</b>';
					$ole_details .= '<br />';
					$ole_details .= ($ole_program_details=='')? $Lang['General']['EmptySymbol'] : $ole_program_details;
				}else{
					$ole_details .= '<b>'.$Lang['iPortfolio']['OEA']['DetailsForOle'].'</b>';
					$ole_details .= '<br />';
					$ole_details .= ($ole_program_details=='')? $Lang['General']['EmptySymbol'] : $ole_program_details;
					$ole_details .= '<br />';
					$ole_details .= '<br />';
					$ole_details .= '<b>'.$Lang['iPortfolio']['OEA']['DetailsFromStudent'].'</b>';
					$ole_details .= '<br />';
					$ole_details .= ($ole_student_details=='')? $Lang['General']['EmptySymbol'] : $ole_student_details;
				}
			}
			
			
			# OLE IntExt
			$ole_IntExt = (strtoupper($ole_item->getIntExt())=="INT") ? $Lang['iPortfolio']['OEA']['OLE_Int'] : $Lang['iPortfolio']['OEA']['OLE_Ext'];
			
			# prepare OEA Item


		


			$oea_cat_ary = $liboea_setting->get_OEA_Item($this->OEA_ProgramCode, $withOthersWarning=true);
			
			# prepare OEA Program Code
			$targetProgramId = ($oleProgramId=='')? $ole_item->getProgramID() : $oleProgramId;
			$mapped_OEA_Code = $liboea_setting->getDefaultMappedOEACode($targetProgramId);



			$STUDENT_SELECTED_THIS_OLE = false;
			$TEACHER_MAPPED_THIS_OLE   = false;
			$TEACHER_STUDENT_MAPPED_CODE_EQUAL   = false;

			if($this->OEA_ProgramCode == ''){
				$STUDENT_SELECTED_THIS_OLE = false;
			}else {
				$STUDENT_SELECTED_THIS_OLE = true;
			}

			if($IsForOLEEdit){
				$STUDENT_SELECTED_THIS_OLE = false;
			}

			if($mapped_OEA_Code == ''){
				$TEACHER_MAPPED_THIS_OLE = false;
			}else{
				$TEACHER_MAPPED_THIS_OLE = true;
			}

			
			if($TEACHER_MAPPED_THIS_OLE && $STUDENT_SELECTED_THIS_OLE){
				if($this->OEA_ProgramCode == $mapped_OEA_Code){
					$TEACHER_STUDENT_MAPPED_CODE_EQUAL = true;
				}else{
					$TEACHER_STUDENT_MAPPED_CODE_EQUAL = false;
				}
			}
//error_log("\n\n\n<----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");			
//error_log("OEA_ProgramCode -->".$this->OEA_ProgramCode."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");			
//error_log("mapped_OEA_Code -->".$mapped_OEA_Code."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");			
//error_log("STUDENT_SELECTED_THIS_OLE -->".$STUDENT_SELECTED_THIS_OLE."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");			
//error_log("TEACHER_MAPPED_THIS_OLE -->".$TEACHER_MAPPED_THIS_OLE."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");			
//error_log("TEACHER_STUDENT_MAPPED_CODE_EQUAL --> ".$TEACHER_STUDENT_MAPPED_CODE_EQUAL."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");			


			//$mapped_OEA_Code = $liboea_setting->getDefaultMappedOEACode($ole_item->getProgramID());
			
			//$this->OEA_ProgramCode  , store MAPPED OEA program code for this OLE (OEA CODE from  OEA_STUDENT)
			//$this->OEA_ProgramCode==""  <-- student have not map this OLE program to a OEA (new map start)
			//$mapped_OEA_Code == ""  <-- OLE program does not mapped by teacher 

			if($this->OEA_ProgramCode=="" && $mapped_OEA_Code=="") {
				$OEA_Code_Display = "";
			} 
			else if($mapped_OEA_Code=="") {
				//this OLE have not map with a OEA code by teacher
				$OEA_Code_Display = $this->OEA_ProgramCode;

				if (!$IsForOLEEdit)
				{
					// to indicate it is mapped by student
					$remarkField = "<span class='tabletextrequire'>#</span> <span class='tabletextremark'>".$Lang['iPortfolio']['OEA']['Self_Mapped']."</span>";
					$asterisk = "<span class='tabletextrequire' style='float:left;'>#</span>";
				}
			} 
			else {
				//there is a map code for this OLE either by teacher or student
				$OEA_Code_Display = ($this->OEA_ProgramCode=='')? $mapped_OEA_Code : $this->OEA_ProgramCode;
				$remarkField = "<span class='tabletextrequire'>#</span> <span class='tabletextremark'>".$Lang['iPortfolio']['OEA']['MappedByTeacher']."</span>";
				$asterisk = "<span class='tabletextrequire' style='float:left;'>#</span>";
			}
				
			# prepare OEA Category	
			$oea_cat = ($oea_cat_ary[$OEA_Code_Display]['CatName']=="") ? "-" : $oea_cat_ary[$OEA_Code_Display]['CatName'];
			
			# prepare OEA Item
			$stored_oea_title = intranet_htmlspecialchars($this->Title);
			$oea_title = '';
			if ($stored_oea_title != '') {
				$oea_title = $stored_oea_title;
			}
			else {
				if($this->OEA_ProgramCode!=$oea_cfg["OEA_Default_Category_Others"]) {
					$oea_title = $oea_cat_ary[$OEA_Code_Display]['ItemName'];
				}
				if($this->OEA_ProgramCode==$oea_cfg["OEA_Default_Category_Others"]) {
					$oea_title .= $this->Title;
				}
			}
			
			if($oea_title == "") { 
				if(!$IsForOLEEdit && $this->NewRecord) {
					$preloadText = $Lang['iPortfolio']['OEA']['NoInfo'];
				} else {
					$preloadText = $Lang['iPortfolio']['OEA']['Mapping_No_OEA_Record_Is_Mapped'];
				}
				
				$oea_title = "<b><span  class='tabletextrequire'>".$preloadText."</span></b>";
				if($this->NewRecord) {
					//$editLink = " (<a href='javascript:;' onClick='CopyOthers()'>$button_edit</a>)";
				}
			} else {
				$oea_title = "<b>".$oea_title."</b>";
			}
			if($_SESSION['UserType']==USERTYPE_STAFF && $oea_cat_ary[$OEA_Code_Display]['ItemCode']==$oea_cfg["OEA_Default_Category_Others"]) {
				//$editLink = " (<a href='javascript:;' onClick='CopyOthers()'>$button_edit</a>)";	
			}		
			
					
			# prepare OEA Date
			$startyear = ($this->TempStartYear=="") ? $this->getStartYear() : $this->TempStartYear;
			if($startyear=="") $startyear = date("Y");
			$endyear = ($this->TempStartYear=="") ? $this->getEndYear() : $this->TempEndYear;
			if($endyear=="") $endyear = date("Y");
			
			# prepare OEA Item stored value
			$oea_itemSelect = "<span id='selectedItemSpan' style='float:left;'>".$oea_title." $editLink </span><input type='hidden' name='ItemCode' id='ItemCode' value='".$OEA_Code_Display."' />";
			
			$generateMapOptions = false;
			/*
			//if($fromJupasMappingSettings || ($this->EditMode && $mapped_OEA_Code=='') || ($this->OEA_ProgramCode!='' && $mapped_OEA_Code!=$this->OEA_ProgramCode)) {
			if(	$fromJupasMappingSettings || 
				($this->EditMode && !$TEACHER_MAPPED_THIS_OLE) || 
				!$STUDENT_SELECTED_THIS_OLE || 
				($STUDENT_SELECTED_THIS_OLE && !$TEACHER_STUDENT_MAPPED_CODE_EQUAL)) 
			{
				$divStyle = "display:block;";	// show pencil
				$generateMapOptions = true;
			} else {
				$divStyle = "display:none;";	// hide pencil
				$generateMapOptions = false;
			}*/
			
			$showPencil = false;
			if($TEACHER_MAPPED_THIS_OLE && (!$STUDENT_SELECTED_THIS_OLE || $TEACHER_STUDENT_MAPPED_CODE_EQUAL)){
				$showPencil = false;
			}else{
				$showPencil = true;
			}
			if($IsForOLEEdit){
				$showPencil = true;
			}

			if ($showPencil) {
				$divStyle = "display:block;";	// show pencil
				$generateMapOptions = true;
			}
			else {
				$divStyle = "display:none;";	// hide pencil
				$generateMapOptions = false;
			}

			$oea_itemSelect .= "<span id='divPencil' class='table_row_tool' style='float:left; $divStyle'><a title='".$button_edit."' class='edit_dim' href='javascript:;' onClick='js_show_layer(\"MapOptions\")'></a></span>";
			$oea_itemSelect .= "<br style='clear:both;' />";
			
			
//			if($this->EditMode) {
//				$divStyle = "display:none;";	
//			} else {
//				$divStyle = "display:block;";	
//			}
			if ($fromJupasMappingSettings) {
				$divStyle = ($mapped_OEA_Code=='')? "display:block;" : "display:none;";
			}
			else {
				$divStyle = ($this->EditMode)? "display:none;" : "display:block;";
			}
			
			$oea_itemSelect .= "<div id='MapOptions' style='$divStyle'>";

			//if($IsForOLEEdit || $mapped_OEA_Code=="" || ($this->OEA_ProgramCode!='' && $mapped_OEA_Code!=$this->OEA_ProgramCode))
			if ($generateMapOptions)
			{
				$oea_itemSelect .= "<br />";
				$oea_itemSelect .= $Lang['iPortfolio']['OEA']['selectMapMethod'].'<br/>'; 
				if($IsForOLEEdit || !$this->NewRecord)
				{			# display suggestion option
					$ole_title = $ole_item->getTitle();
					if ($fromJupasMappingSettings) {
						$ole_title = ($ole_title=='')? $engTitle : $ole_title;
					}
					//debug_pr($engTitle);die();
					$liboea_setting_search->setCriteriaTitle($ole_title);
					$liboea_setting_search->getSuggestResult();
					$numOfSuggestion = sizeof($liboea_setting_search->getSearchResultTitle());
					if($numOfSuggestion > 0)
					{
						if($fromJupasMappingSettings || (!$IsForOLEEdit && $_SESSION['UserType']==USERTYPE_STAFF)) {
							$ItemSuggestCountDisplay = "- <a href='javascript:;' onClick=\"loadNextPage('SearchDiv','suggestionList')\" class='setting_row' rel='advance_options' title='".$Lang['iPortfolio']['OEA']['SuggestedItems']."'>".$Lang['iPortfolio']['OEA']['SuggestedItems']." (".$Lang['iPortfolio']['OEA']['MatchItemCaption']." ".$numOfSuggestion.")</a>";
						} else {
							$ItemSuggestCountDisplay = "- <a href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onclick='Show_SuggestionList(); return false;' class='setting_row thickbox' title='".$Lang['iPortfolio']['OEA']['SuggestedItems']."'>".$Lang['iPortfolio']['OEA']['SuggestedItems']." (".$Lang['iPortfolio']['OEA']['MatchItemCaption']." ".$numOfSuggestion.")</a>";
						}	
					}
					else if ($fromJupasMappingSettings) {
						$ItemSuggestCountDisplay = "- ".$Lang['iPortfolio']['OEA']['SuggestedItems']." (".$Lang['iPortfolio']['OEA']['MatchItemCaption']." ".$numOfSuggestion.")";
					}
					elseif ($IsForOLEEdit)
					{
						$ItemSuggestCountDisplay = "- <a href='javascript:Show_SuggestionList();' title='".$Lang['iPortfolio']['OEA']['SuggestedItemsCheck'] ."'>".$Lang['iPortfolio']['OEA']['SuggestedItemsCheck'] ."</a> <a href='#TB_inline?height=500&width=750&inlineId=FakeLayer' id='HiddenClickTB' class='setting_row thickbox' title='".$Lang['iPortfolio']['OEA']['SuggestedItemsCheck'] ."'></a>";
					} else {
						$ItemSuggestCountDisplay = "- ".$Lang['iPortfolio']['OEA']['SuggestedItems']." (".$Lang['iPortfolio']['OEA']['MatchItemCaption']." ".$numOfSuggestion.")";
					}
//					$oea_itemSelect .= $ItemSuggestCountDisplay."; $i_general_or <br>";
					$oea_itemSelect .= $ItemSuggestCountDisplay."<br />";
					
				}
				
				if($fromJupasMappingSettings || (!$IsForOLEEdit && $_SESSION['UserType']==USERTYPE_STAFF)) {	# teacher view, display advance search in layer
					$oea_itemSelect .= "
						- <a href='javascript:;' onClick=\"loadNextPage('SearchDiv','advanceSearch')\" class='setting_row' rel='advance_options' title='".$Lang['iPortfolio']['OEA']['AdvanceSearch']."'>".$Lang['iPortfolio']['OEA']['AdvanceSearch']."</a> <br>";	
					
				} else {		# student view, display advance search in thickbox

//					if($OEA_Code_Display == ''){
						//display only if student have not map this before
						$oea_itemSelect .= "
						- <a href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onclick='Show_Advance_Search(); return false;' class='setting_row thickbox' rel='advance_options' title='".$Lang['iPortfolio']['OEA']['AdvanceSearch']."'>".$Lang['iPortfolio']['OEA']['AdvanceSearch']."</a> <br>";
//					}
				}
				
				# option "Others"
				if ($_SESSION['UserType']==USERTYPE_STUDENT && $liboea->getAllowStudentApplyOthersOea()==false) {
					// do not allow student to apply "Others" => do nth
				}
				else {
					if($fromJupasMappingSettings || $IsForOLEEdit || !$this->NewRecord) {	# from OLE
						$oea_itemSelect .= "	
							- <a href='javascript:;' onClick='CopyOthers()' rel='advance_options' id='oea_category_others'>".$Lang['iPortfolio']['OEA']['UseOleName']."</a><br />";
					} else {				# created from "NEW"
						$oea_itemSelect .= "	
							- <a href='javascript:;' onClick='CopyOthers()' rel='advance_options' id='oea_category_others'>".$Lang['iPortfolio']['OEA']['SelfInput']."</a><br />";
					}
				}
			}
			$oea_itemSelect .= "</div>";
			
			# OEA category stored value
			$oea_catSelect = "<span id='span_OEA_Category'>".$oea_cat."</span><input type='hidden' name='CatCode' id='CatCode' value='".$oea_cat_ary[$OEA_Code_Display]['CatCode']."'>";
			
			# OEA date
			if ($IsForOLEEdit)
			{
				$oea_date = "<div class='tabletextremark'>". $Lang['iPortfolio']['OLE_OEA']['Date']."</div>";
			} else
			{
				$oea_date = $this->getYearSelect('name="startdate" id="startdate"',$startyear)." $i_To ".$this->getYearSelect('name="enddate" id="enddate"',$endyear);	
			}
			
			# OEA Role selection
			$CanEditMappedRole = ($liboea->getForceStudentToApplyRoleMapping())? false : true;
			$ShowTeacherMappedText = false;
			$ShowRoleSelection = false;
			$ShowRoleText = false;
			$DisabledRoleCodeArr = '';
			
			$OleRole = $ole_item->getRole();
			$OeaMappedRole = $this->Get_OLE_OEA_Role_Mapping_By_OLE_Role($OleRole);
			
			if ($this->EditMode) {
				if($mapped_OEA_Code == '' || $OeaMappedRole == '') {
					// OEA not mapped from OLE or OLE Role not mapped
					$ShowRoleSelection = true;
				}
				else {
					// 1. OEA mapped from OLE
					// 2. OLE role mapped
					if ($CanEditMappedRole) {
						// Can override teacher mapped role
						$ShowTeacherMappedText = true;
						$ShowRoleSelection = true;
					}
					else {
						// CANNOT override teacher mapped role
						if ($OeaMappedRole == $this->getRole()) {
							// OEA Role is the same as the OLE mapped role 
							$ShowRoleText = true;
						}
						else {
							// OEA Role is NOT the same as the OLE mapped role => Show the role selection
							$ShowTeacherMappedText = true;
							$ShowRoleSelection = true;
							$PerformRoleFormChecking = 1;
							
							// Only allow the user to choose the existing role or the teacher mapped role
							$RoleAry = $this->get_OEA_Role_Array();
							foreach((array)$RoleAry as $thisRoleKey => $thisRoleLang) {
								if ($thisRoleKey != $this->getRole() && $thisRoleKey != $OeaMappedRole) {
									$DisabledRoleCodeArr[] = $thisRoleKey;
								}
							}
						}
					}
				}
			}
			else {
				if ($this->NewRecord) {
					// Create new OEA => Show role selection
					$ShowRoleSelection = true;
				}
				else {
					// Map from OLE
					if ($OeaMappedRole) {
						if ($CanEditMappedRole) {
							// Role mapped by teacher and can be changed
							$ShowTeacherMappedText = true;
							$ShowRoleSelection = true;
							$this->setRole($OeaMappedRole);		// preset selection value
						}
						else {
							// Role mapped by teacher and cannot be changed => show role text and set hidden form field
							$ShowRoleText = true;
						}
					}
					else {
						// Role NOT mapped by teacher => show role selection
						$ShowRoleSelection = true;
					}
				}
			}
			
			if ($IsForOLEEdit)
			{
				$roleDisplay = "<div class='tabletextremark'>".$Lang['iPortfolio']['OLE_OEA']['Role']."</div>";
			} else
			{
				$roleDisplay = '';
				if ($ShowTeacherMappedText) {
					$roleDisplay .= '<span class="tabletextrequire">#</span> '.$Lang['iPortfolio']['OEA']['TeacherMapping'].': <b>'.$this->Get_OEA_RoleName_By_RoleCode($OeaMappedRole).'</b>';
					$roleDisplay .= '<br />';
				}
				if ($ShowRoleSelection) {
					$roleDisplay .= $this->getRoleSelection($DisabledRoleCodeArr);
				}
				if ($ShowRoleText) {
					$roleDisplay .= '<span class="tabletextrequire">#</span> '.$this->Get_OEA_RoleName_By_RoleCode($OeaMappedRole);
					$roleDisplay .= '<input type="hidden" id="RoleID" name="RoleID" value="'.$OeaMappedRole.'">';
				}
			}
			
			
			# OEA Participation (Int / Ext)
			$OleIntExt = $ole_item->getIntExt();
			
			if ($IsForOLEEdit)
			{
				$oea_participationOption = "<div class='tabletextremark'>".$Lang['iPortfolio']['OLE_OEA']['Nature']."</div>";
			} else
			{
				$oea_participationOption = $this->get_OEA_IntExt($OleIntExt);
			}
			
			# OEA Achievement (Award Bearing + Participation Nature + Achievement)
			$award = $this->getAwardSelections($IsForOLEEdit);
			
			# OEA Description
			if ($IsForOLEEdit)
			{
				$oea_description = "<div class='tabletextremark'>".$Lang['iPortfolio']['OLE_OEA']['Description']."</div>";
			} else
			{
				$oea_description = $this->getDescriptionTextarea();
			}
			
			#####################
			# table content
			#####################
			$firstColumnWidth = 10;
			
			$DisplayIni = ($IsForOLEEdit) ? " style='display:none;' " : "";
			$h_result = "<div id='TableDiv' {$DisplayIni}>";
			$h_result .= "<table class='common_table_list_v30'>";
			if ($IsForOLEEdit)
			{
				$h_result .= "<th colspan='2'>".$Lang['iPortfolio']['OEA']['OEA_Name2']."</th>";
			} else
			{
				$h_result .= "<th width='$firstColumnWidth%'>&nbsp; </th>";
				if(!$this->NewRecord) {
					$h_result .= "<th width='$secondColumnWidth%'>".$Lang['iPortfolio']['OEA']['OLE']."</th>";
					$secondColumnWidth = 35;
				}
				$thirdColumnWidth = 100-$firstColumnWidth-$secondColumnWidth;
				$h_result .= "<th width='$thirdColumnWidth%'>".$Lang['iPortfolio']['OEA']['OEA_Name2']."</th>";
			}
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Title']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>".$ole_item->getTitle()."</td>";
			}
			$h_result .= "<td>{$asterisk} {$oea_itemSelect}</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Category']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>".$ole_item->getCategoryTitle($intranet_session_language)."</td>";
			}
			$h_result .= "<td>$oea_catSelect</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Date']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>$ole_date</td>";
			}
			$h_result .= "<td>$oea_date</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Role']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>$ole_role</td>";
			}
			$h_result .= "<td>$roleDisplay</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Achievement']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>$ole_achievement</td>";
			}
			$h_result .= "<td>$award</td>";
			$h_result .= "</tr>";
			$h_result .= "<tr>";
			$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Participation']."</td>";
			if(!$this->NewRecord) {
				$h_result .= "<td>$ole_IntExt</td>";
			}
			$h_result .= "<td>$oea_participationOption</td>";
			$h_result .= "</tr>";
			
			### Description
			$h_result .= "<tr>";
				$h_result .= "<td>".$Lang['iPortfolio']['OEA']['Description']."</td>";
				if(!$this->NewRecord) {
					$h_result .= "<td>$ole_details</td>";
				}
				$h_result .= "<td>$oea_description</td>";
			$h_result .= "</tr>";
			
			if ($fromJupasMappingSettings) {
				### Override Student Record
				$h_result .= "<tr>";
					$h_result .= "<td>".$Lang['iPortfolio']['OEA']['OverrideStudentMappedRecords']."</td>";
					$h_result .= "<td>";
						$h_result .= $linterface->Get_Checkbox('overrideStudentOeaChk', 'overrideStudentOea', 1, $isChecked=0, $Class='', $Display='', $Onclick='triggerOverrideWarning(this.checked);');
						$h_result .= '<span class="tabletextremark"> ('.$Lang['iPortfolio']['OEA']['NotApplicableToDelete'].')</span>';
						$h_result .= '<div id="overrideStudentRecordWarningDiv" style="display:none; width:430px;">';
							$h_result .= '<div class="Warningtable">';
								$h_result .= '<span class="Warningtitlebg Warningtitletext">'.$Lang['General']['Warning'].'</span>';
								$h_result .= '<br />';
								$h_result .= '<br />';
								$h_result .= '<span class="Warningtitletext">'.$Lang['iPortfolio']['OEA']['WarningArr']['OverrideStudentOeaRecords'].'</span>';
							$h_result .= '</div>';
						$h_result .= '</div>';
					$h_result .= "</td>";
				$h_result .= "</tr>";
			}
			
			$h_result .= "</table>";
			$h_result .= "</div>";
			$h_result .= $remarkField;
			$h_result .= "<div id='SearchDiv'></div>";
			
			$h_result .= "<input type='hidden' name='ole_title' id='ole_title' value='".intranet_htmlspecialchars($ole_item->getTitle())."'>";
			$h_result .= "<input type='hidden' name='stored_oea_title' id='stored_oea_title' value='".$stored_oea_title."'>";
			
			return $h_result;
			
		}
		
		private function getRoleSelection($DisabledRoleCodeArr='') 
		{
			$RoleAry = $this->get_OEA_Role_Array();
			$rAry = array();
			$DisabledIndexArr = array();
			
			if(sizeof($RoleAry)>0) {
				$i = 0;
				foreach($RoleAry as $key=>$val) {
					$rAry[$i] = array($key, $val); 
					
					if ($DisabledRoleCodeArr!='' && in_array($key, (array)$DisabledRoleCodeArr)) {
						$DisabledIndexArr[] = $i;
					}
					
					$i++;	
				}	
			}
			$roleSelect = $this->getSelectByArray_OEA($rAry, 'name="RoleID" id="RoleID"', $this->getRole(), $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1, $DisabledIndexArr);	
			
			return $roleSelect;
		}
		
		private function get_OEA_IntExt($ole_int_ext='')
		{
			global $PATH_WRT_ROOT, $oea_cfg, $ipf_cfg, $sys_custom;
			global $Lang;
			
			$liboea = new liboea();
			
//			$oea_int_ext = $this->getParticipation();
//			
//			if($oea_int_ext=="S" || $oea_int_ext=="") {
//				$int_selected = " checked";
//			}
//			else {
//				$ext_selected = " checked";
//			}
//				
//			$oea_participationOption = '';
//			$oea_participationOption .= "<input type='radio' name='Participation' id='Participation_S' value='S' $int_selected><label for='Participation_S'>".$oea_cfg["OEA_Participation_Type"]["S"]."</label>";
//			$oea_participationOption .= "<br>";
//			$oea_participationOption .= "<input type='radio' name='Participation' id='Participation_P' value='P' $ext_selected><label for='Participation_P'>".$oea_cfg["OEA_Participation_Type"]["P"]."</label>";
//			
//			return $oea_participationOption;
			
			$ParticipationInfoArr = $this->getDefaultParticipation($associateAry=1);
			$oea_int_ext = $this->getParticipation();
			$isEdit = $this->EditMode;
			$isFromNewOEARecord = $this->getNewRecord();
			
			$CanApplyPart1Only = false;
			$OleType_ApplicablePartArr = array();
			
			if ($ole_int_ext!='') {
				// OEA mapped from OLE
				$ApplicableParticipationTypeArr = $liboea->getAllowOEAPartByOLEType($ole_int_ext);
			}
			else {
				// New OEA record directly
//				if ($sys_custom['iPf']['JUPASArr']['OEA']['NewRecordForPartIIOnly']) {
//					$ApplicableParticipationTypeArr = $oea_cfg["OEA_Participation_Type_Private"];
//				}
//				else {
//					foreach ((array)$ParticipationInfoArr as $thisParticipationCode => $thisParticipationLang) {
//						if ($liboea->getThisOEAPartIsAllowed($thisParticipationCode)) {
//							$ApplicableParticipationTypeArr[] = $thisParticipationCode;
//						}
//					}
//				}

				if ($isFromNewOEARecord) {
					foreach ((array)$ParticipationInfoArr as $thisParticipationCode => $thisParticipationLang) {
						if ($liboea->getPartIsAllowForNewOEA($thisParticipationCode)) {
							$ApplicableParticipationTypeArr[] = $thisParticipationCode;
						}
					}
				}
				else {
					foreach ((array)$ParticipationInfoArr as $thisParticipationCode => $thisParticipationLang) {
						if ($liboea->getThisOEAPartIsAllowed($thisParticipationCode)) {
							$ApplicableParticipationTypeArr[] = $thisParticipationCode;
						}
					}
				}
			}
			$numOfApplicableParticipationType = count((array)$ApplicableParticipationTypeArr);
			
			$EnableBothRadio = false;
			$ShowTeacherSuggestion = false;
			$ShowParticipationText = false;
			$ShowParticipationRadio = false;
			$x = '';
			
			//if ($isEdit && $ole_int_ext!='' && !in_array($oea_int_ext, (array)$ApplicableParticipationTypeArr)) {
			if ($isEdit && !in_array($oea_int_ext, (array)$ApplicableParticipationTypeArr)) {
				// Previously saved settings but now the settings is invalid => Show teacher's suggestion and allow student to choose again
				$EnableBothRadio = true;
				$ShowTeacherSuggestion = true;
				$TargetParticipationCode = ($oea_int_ext==$oea_cfg["OEA_Participation_Type_School"])? $oea_cfg["OEA_Participation_Type_Private"] : $oea_cfg["OEA_Participation_Type_School"];
			}
			
			if (!$EnableBothRadio && $numOfApplicableParticipationType==1) {
				// Only Applicable to one type => Show the mapped type without radio buttons
				$ShowParticipationText = true;
				$TargetParticipationCode = $ApplicableParticipationTypeArr[0];
			}
			else {
				$Part1Checked = '';
				$Part2Checked = '';
				$ShowParticipationRadio = true;
					
				if($oea_int_ext==$oea_cfg["OEA_Participation_Type_School"] || $oea_int_ext=="") {
					$Part1Checked = " checked";
				}
				else {
					$Part2Checked = " checked";
				}
				
				//if ($ole_int_ext != '') {
					// for OEA which is mapped from OLE only
					
					$Part1Disabled = '';
					$Part2Disabled = '';
					if (in_array($oea_cfg["OEA_Participation_Type_School"], (array)$ApplicableParticipationTypeArr)==false) {
						if (!$EnableBothRadio) {
							$Part1Disabled = 'disabled';
						}
						$ShowTeacherSuggestion = true;
						$TargetParticipationCode = $oea_cfg["OEA_Participation_Type_Private"];
					}
					if (in_array($oea_cfg["OEA_Participation_Type_Private"], (array)$ApplicableParticipationTypeArr)==false) {
						if (!$EnableBothRadio) {
							$Part2Disabled = 'disabled';
						}
						$ShowTeacherSuggestion = true;
						$TargetParticipationCode = $oea_cfg["OEA_Participation_Type_School"];
					}
				//}
			}
			
			
			if ($ShowTeacherSuggestion) {
				$x .= '<span class="tabletextrequire">#</span> '.$Lang['iPortfolio']['OEA']['TeacherMapping'].': <b>'.$this->Get_OEA_ParticipationName_By_ParticipationCode($TargetParticipationCode).'</b>';
				$x .= '<br />';
			}
			
			if ($ShowParticipationText) {
				$x .= '<span class="tabletextrequire">#</span> '.$this->Get_OEA_ParticipationName_By_ParticipationCode($TargetParticipationCode);
				$x .= '<input type="hidden" id="Participation" name="Participation" value="'.$TargetParticipationCode.'" />';
			}
			
			if ($ShowParticipationRadio) {
				$x .= "<input type='radio' name='Participation' id='Participation_S' value='".$oea_cfg["OEA_Participation_Type_School"]."' $Part1Checked $Part1Disabled><label for='Participation_S'>".$this->Get_OEA_ParticipationName_By_ParticipationCode($oea_cfg["OEA_Participation_Type_School"])."</label>";
				$x .= "<br>";
				$x .= "<input type='radio' name='Participation' id='Participation_P' value='".$oea_cfg["OEA_Participation_Type_Private"]."' $Part2Checked $Part2Disabled><label for='Participation_P'>".$this->Get_OEA_ParticipationName_By_ParticipationCode($oea_cfg["OEA_Participation_Type_Private"])."</label>";
			}
			
			return $x;
		}
		
		private function getAwardSelections($IsForOLEEdit=false) 
		{
			global $PATH_WRT_ROOT, $Lang, $oea_cfg, $sys_custom;
			
			include_once($PATH_WRT_ROOT."includes/portfolio25/slp/libole_student_item.php");
			
			$_id = ($this->OLE_STUDENT_RecordID=="") ? $this->TempOLE_STUDENT_RecordID : $this->OLE_STUDENT_RecordID;
			$isEdit = $this->EditMode;
			
			$ole_item = new libole_student_item($_id);
			$ole_programID = $ole_item->getProgramID();
			
			//echo $_id;
			$mappedItemCode = $ole_item->getMapped_OEA_ProgramCode();	
			
			$defaultAwardBearing = 1;
			if($mappedItemCode) {
				$defaultAwardBearing = $this->getAwardBearing('', $ole_programID);
				$defaultAwardBeraingText = ($defaultAwardBearing)? $oea_cfg["OEA_AwardBearing_Type_Yes"] : $oea_cfg["OEA_AwardBearing_Type_No"];
			} 
			$oeaAwardBearing = $this->getOEA_AwardBearing();
			
			
			//echo $defaultAwardBearing.'/';
//			if ($mappedItemCode=='' || $defaultAwardBearing) { # with award bearing
//			
//				$AwardBearing = '';
//				
//				if ($mappedItemCode != '') {
//					$AwardBearing .= '<span class="tabletextrequire">#</span> '; 
//				}
//				$AwardBearing .= $Lang['iPortfolio']['OEA']['OEA_AwardBearing'];
//				
//				
//				if($this->getOEA_AwardBearing==$oea_cfg["OEA_AwardBearing_Type_No"]) {
//					$withoutAward = " checked";
//				}
//				else {
//					$withAward = " checked";
//				}
//				
//				if($mappedItemCode) {
//					$AwardBearing .= "<br /><input type='hidden' name='awardBearing' id='awardBearing' value='Y' />";
//				}
//				else {
//					if (!$IsForOLEEdit) {
//						$AwardBearing1OnClick = "onClick='changeAwardSetting(1)'";
//						$AwardBearing0OnClick = "onClick='changeAwardSetting(0)'";
//					}
//					$AwardBearing .= ": <input type='radio' name='awardBearing' id='awardBearing1' value='Y' $withAward {$AwardBearing1OnClick} /><label for='awardBearing1'>".$Lang['General']['Yes']."</label> &nbsp;";
//					$AwardBearing .= "<input type='radio' name='awardBearing' id='awardBearing0' value='N' $withoutAward {$AwardBearing0OnClick} /><label for='awardBearing0'>".$Lang['General']['No']."</label><br />";
//				}
//				
//				
//				$AchievementAry = $this->get_OEA_Achievement_Array();
//				$aAry = array();
//				if(sizeof($AchievementAry)>0) {
//					$i = 0;
//					foreach($AchievementAry as $key=>$val) {
//						$aAry[$i] = array($key,$val); $i++;	
//					}	
//				}
//				if($this->OEA_AwardBearing==$oea_cfg["OEA_AwardBearing_Type_No"]) {
//					$disabled = " disabled";
//				}
//					
//				$achievementSelect = getSelectByArray($aAry, 'name="AchievementID" id="AchievementID" '.$disabled, $this->Achievement);
//				/*
//				# OEA Participation Nature
//				$ParticipationNatureAry = $this->get_OEA_ParticipationNature_Array();
//				$aAry = array();
//				if(sizeof($ParticipationNatureAry)>0) {
//					$i = 0;
//					foreach($ParticipationNatureAry as $key=>$val) {
//						$aAry[$i] = array($key,$val); $i++;	
//					}	
//				}
//				*/
//				//$participationNatureSelect = $this->getParticipationNatureSelect();
//			} else {	# without award bearing
//				$AwardBearing = '<span class="tabletextrequire">#</span> '.$Lang['iPortfolio']['OEA']['OEA_NotAwardBearing'];
//				
//				$h_HiddenOLE_Item .= "<input type='hidden' name='awardBearing' id='awardBearing' value='N'>";
//				$h_HiddenOLE_Item .= "<input type='hidden' name='AchievementID' id='AchievementID' value='N'>";
//				/*
//				# OEA Participation Nature
//				$ParticipationNatureAry = $this->get_OEA_ParticipationNature_Array();
//				
//				$aAry = array();
//				if(sizeof($ParticipationNatureAry)>0) {
//					$i = 0;
//					foreach($ParticipationNatureAry as $key=>$val) {
//						if($key!="C") {
//							$aAry[$i] = array($key,$val); $i++;
//						}	
//					}	
//				}
//				*/
//				//$participationNatureSelect = $this->getParticipationNatureSelect();
//			}
			
			
			$showRadio = false;
			$showText = false;
			$showTeacherMapping = false;
			$targetAwardBearing = $oea_cfg["OEA_AwardBearing_Type_Yes"];
			if ($sys_custom['iPf']['JUPASArr']['AllowStudentChooseAwardBearingEvenOeaMappedOle']) {
				$showRadio = true;
				$targetAwardBearing = $oeaAwardBearing;
			}
			else {
				// General Logic
				if ($mappedItemCode=='') {
					// no mapping yet => show radio for selection
					$showRadio = true;
					$targetAwardBearing = ($oeaAwardBearing=='')? $targetAwardBearing : $oeaAwardBearing;
				}
				else if ($mappedItemCode != '' && $isEdit && $oeaAwardBearing != $defaultAwardBeraingText) {
					// mapped but mapping not match (student added record before teacher mapping) => show radio for selection and display teacher's mapping info 
					$showRadio = true;
					$showTeacherMapping = true;
					$targetAwardBearing = $oeaAwardBearing;
				}
				else {
					// mapped (new oea record or mapping same as teacher's mapping) => show teacher's mapping only and no radio for selection
					$showText = true;
					$targetAwardBearing = $defaultAwardBeraingText;
				}
			}
			
			
						
			$h_awardBearing = '';
			$h_HiddenOLE_Item = '';
			if ($showTeacherMapping) {
				$h_awardBearing .= '<span class="tabletextrequire">#</span> '.$Lang['iPortfolio']['OEA']['TeacherMapping'].': <b>'.$this->get_OEA_AwardBearingName_By_AwardBearingCode($defaultAwardBeraingText).'</b>';
				$h_awardBearing .= '<br>';
			}
			
			if ($showText) {
				$h_awardBearing .= '<span class="tabletextrequire">#</span> '.$this->get_OEA_AwardBearingName_By_AwardBearingCode($defaultAwardBeraingText);
				$h_awardBearing .= "<input type='hidden' name='awardBearing' id='awardBearing' value='".$targetAwardBearing."' />";
				
				if ($targetAwardBearing==$oea_cfg["OEA_AwardBearing_Type_No"]) {
					$h_awardBearing .= "<input type='hidden' name='AchievementID' id='AchievementID' value='N'>";
				}
				$h_awardBearing .= '<br>';
			}
			
			if ($showRadio) {
				if($targetAwardBearing==$oea_cfg["OEA_AwardBearing_Type_No"]) {
					$withoutAward = " checked";
				}
				else {
					$withAward = " checked";
				}
				
				if (!$IsForOLEEdit) {
					$AwardBearing1OnClick = "onClick='changeAwardSetting(1)'";
					$AwardBearing0OnClick = "onClick='changeAwardSetting(0)'";
				}
				
				$h_awardBearing .= $Lang['iPortfolio']['OEA']['OEA_AwardBearing'].": ";
				$h_awardBearing .= "<input type='radio' name='awardBearing' id='awardBearing1' value='".$oea_cfg["OEA_AwardBearing_Type_Yes"]."' $withAward {$AwardBearing1OnClick} /><label for='awardBearing1'>".$Lang['General']['Yes']."</label> &nbsp;";
				$h_awardBearing .= "<input type='radio' name='awardBearing' id='awardBearing0' value='".$oea_cfg["OEA_AwardBearing_Type_No"]."' $withoutAward {$AwardBearing0OnClick} /><label for='awardBearing0'>".$Lang['General']['No']."</label><br />";
			}
			
			
			$participationNatureSelect = $this->getParticipationNatureSelect();
			
			$AchievementAry = $this->get_OEA_Achievement_Array();
			$aAry = array();
			if(sizeof($AchievementAry)>0) {
				$i = 0;
				foreach($AchievementAry as $key=>$val) {
					$aAry[$i] = array($key,$val); $i++;	
				}	
			}
			if($targetAwardBearing==$oea_cfg["OEA_AwardBearing_Type_No"]) {
				$disabled = " disabled";
			}	
			$achievementSelect = getSelectByArray($aAry, 'name="AchievementID" id="AchievementID" '.$disabled, $this->Achievement);
			
			
			//$option = $AwardBearing;
			$option = '';
			$option .= $h_awardBearing;
			if ($IsForOLEEdit)
			{
				//$option .= "<div  class='tabletextremark'>Others are to be selected by each student.</div>";
			} else
			{
				$option .= " ".$participationNatureSelect." ".$achievementSelect." ";
			}
			$option .= $h_HiddenOLE_Item;

			return $option;
		} 
		
		private function getDescriptionTextarea() {
			global $linterface, $oea_cfg;
			
			$Description = $this->getDescription();
			
			$ProgramCode = $this->getOEA_ProgramCode();
//			if ($ProgramCode == $oea_cfg["OEA_Default_Category_Others"]) {
//				$readonly = '0';
//			}
//			else {
//				$readonly = '1';
//			}
			$readonly = '0';
			
			$onkeyup = ' onkeyup="js_Onkeyup_Info_Textarea(\'Description\', \'DescCharCountSpan\', \''.$oea_cfg["OEA_STUDENT_Description"]["MaxWordCount"].'\', \''.$oea_cfg["OEA_STUDENT_Description"]["MaxLength"].'\', \'char\');" ';
			
			$x = '';
			$x .= $linterface->GET_TEXTAREA('Description', $Description, $taCols=70, $taRows=5, $OnFocus = "", $readonly, $onkeyup, $class='', $taID='');
			$x .= '<br />';
			$x .= liboea::Get_Character_Count_Div('DescCharCountSpan', $oea_cfg["OEA_STUDENT_Description"]["MaxLength"]);
			return $x;
		}
		
		private function getParticipationNatureSelect() 
		{
			# OEA Participation Nature
			$ParticipationNatureAry = $this->get_OEA_ParticipationNature_Array();
			
			$_id = ($this->OLE_STUDENT_RecordID=="") ? $this->TempOLE_STUDENT_RecordID : $this->OLE_STUDENT_RecordID;
			
			$ole_item = new libole_student_item($_id);
			$ole_programID = $ole_item->getProgramID();
			
			//echo $_id;
			$mappedItemCode = $ole_item->getMapped_OEA_ProgramCode();	
			
			$defaultAwardBearing = 1;
			if($mappedItemCode) {
				$defaultAwardBearing = $this->getAwardBearing('', $ole_programID);	
			}
			if($this->OEA_AwardBearing=="N") 	# override default AwardBearing of mapping
				$defaultAwardBearing = 0;
				
			$aAry = array();
			if(sizeof($ParticipationNatureAry)>0) {
				$i = 0;
				
				foreach($ParticipationNatureAry as $key=>$val) {
					if($defaultAwardBearing || (!$defaultAwardBearing && $key!="C")) {
						$aAry[$i] = array($key,$val); $i++;
					}	
				}	
			}
			
			$participationNatureSelect = getSelectByArray($aAry, 'name="ParticipationNature" id="ParticipationNature"', $this->ParticipationNature);
			
			return $participationNatureSelect;
		}
		
		public function getYearSelect($tag="", $selected="")
		{
			global $oea_cfg;
			$currentYear = date("Y");
			$lowerBound = $currentYear - $oea_cfg["OEA_YearSelection_NoOfYearBefore"];
			$upperBound = $currentYear + $oea_cfg["OEA_YearSelection_NoOfYearAfter"];
			
			for($i=$lowerBound; $i<=$upperBound; $i++) {
				$years[] = array($i, $i);	
			}
			
			$yearSelect = getSelectByArray($years, $tag, $selected, 0, 1);
			
			return $yearSelect;

		}
		
		public function get_Selected_Oea_Item_List_SQL()
		{
			global $eclass_db, $UserID, $i_To, $ipf_cfg, $Lang;
			
			# with linkage with OEA item			
			$sql = "SELECT 
						p.title AS 'TITLE',
						IF(o.RecordID IS NULL, CONCAT('<input type =\"checkbox\" name=\"recordID[]\" id=\"recordID[]\" value=\"' , s.recordid, '\" onClick=\"countTotal()\" />'),'&nbsp;') AS 'ACTION',
						o.RecordID AS OEA_RecordID,
						s.RecordID AS OLE_Student_RecordID,
						IF(m.OEA_ProgramCode IS NOT NULL, m.OEA_ProgramCode, o.OEA_ProgramCode) as OEA_ProgramCode,
						p.StartDate as OLE_StartDate,
						IF(p.EndDate='0000-00-00','',p.EndDate) as OLE_EndDate,
						IF(m.OEA_ProgramCode IS NOT NULL, 1, 0) AS Mapped,
						CASE p.IntExt
							WHEN '".strtoupper($ipf_cfg["OLE_TYPE_STR"]["INT"])."' THEN '".$Lang['iPortfolio']['OEA']['Internal']."'
							WHEN '".strtoupper($ipf_cfg["OLE_TYPE_STR"]["EXT"])."' THEN '".$Lang['iPortfolio']['OEA']['External']."'
						END AS OLE_Type,
						If (s.SLPOrder Is Null, '".$Lang['General']['EmptySymbol']."', s.SLPOrder) as SLPOrder
					FROM 
						{$eclass_db}.OLE_STUDENT as s inner join
						{$eclass_db}.OLE_PROGRAM as p on p.programid = s.programid LEFT OUTER JOIN
						{$eclass_db}.OEA_STUDENT as o ON (o.OLE_STUDENT_RecordID=s.RecordID) LEFT OUTER JOIN
						{$eclass_db}.OEA_OLE_MAPPING m On (m.OLE_ProgramID=p.ProgramID)
					WHERE 
						s.UserID = '{$UserID}' AND (s.RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]." or s.RecordStatus=".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"].") AND s.RecordID IN (SELECT OLE_STUDENT_RecordID FROM {$eclass_db}.OEA_STUDENT WHERE StudentID='$UserID')
					ORDER BY
						p.Title, OLE_StartDate";	
						//debug_pr($this->returnArray($sql));
//						echo $sql;
			return $sql;
		}	
		
		private function Get_OLE_OEA_Role_Mapping_Settings() {
			global $eclass_db;
			
			$OEA_OLE_ROLE_MAPPING = $eclass_db.'.OEA_OLE_ROLE_MAPPING';
			$sql = "Select
							LOWER(OLE_ROLE) as OLE_ROLE,
							OEA_ROLE
					From
							$OEA_OLE_ROLE_MAPPING
					";
			$ReturnArr = BuildMultiKeyAssoc($this->objDB->returnArray($sql, null, 1), 'OLE_ROLE', array('OEA_ROLE'), $SingleValue=1, $BuildNumericArray=0);
			
			// For testing only
			//$ReturnArr['Prize Winner'] = 'L';
			//$ReturnArr['Prize Winner'] = 'M';
			
			return $ReturnArr;
		}
		
		public function Get_OLE_OEA_Role_Mapping_By_OLE_Role($OleRole) {
			$MappingSettingsArr = $this->Get_OLE_OEA_Role_Mapping_Settings();
			return $MappingSettingsArr[strtolower($OleRole)];
		}
		
		private function getSelectByArray_OEA($data, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1, $DisabledIndexArr='')
		{
			global $button_select,$i_status_all,$i_general_NotSet;
			$x = "<SELECT $tags>\n";
			if ($noFirst == 0)
			{
				$empty_selected = ($selected == '')? "SELECTED":"";
				if($FirstTitle=="")
				{
					if ($all==0)
					{
						$title = "-- $button_select --";
					}
					else if ($all == 2)
					{
						$title = "$i_general_NotSet";
					}
					else
					{
						$title = "$i_status_all";
					}
				}
				else
					$title = $FirstTitle;
		
				# Eric Yip : Quote values in pull-down list
				switch($ParQuoteValue)
				{
					case 1:
						$x .= "<OPTION value='' $empty_selected> $title </OPTION>\n";
						break;
					case 2:
						$x .= "<OPTION value=\"\" $empty_selected> $title </OPTION>\n";
						break;
				}
			}
		
			for ($i=0; $i<sizeof($data); $i++)
			{
				list ($id, $name) = $data[$i];
				if(is_array($selected))
					$sel_str = (in_array($id,$selected)? "SELECTED":"");
				else 
					$sel_str = ($selected == $id? "SELECTED":"");
					
				$thisDisabled = '';
				if ($DisabledIndexArr != '' && in_array($i, (array)$DisabledIndexArr)) {
					$thisDisabled = 'disabled';
				}
		
				# Eric Yip : Quote values in pull-down list
				switch($ParQuoteValue)
				{
					case 1:
						$x .= "<OPTION value='".htmlspecialchars($id,ENT_QUOTES)."' $sel_str $thisDisabled>$name</OPTION>\n";
						break;
					case 2:
						$x .= "<OPTION value=\"".htmlspecialchars($id,ENT_QUOTES)."\" $sel_str $thisDisabled>$name</OPTION>\n";
						break;
				}
			}
		
			$x .= "</SELECT>\n";
			return $x;
		}
				
	}

}
?>
