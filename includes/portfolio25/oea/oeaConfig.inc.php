<?
# modifying :  

//THIS CONFIG IS AVALIABLE FOR ALL CLIENT ENVIRONMENT
##	-	$varname["DB_[TABLE_NAME]_[FIELD_NAME]"]["MEANING"]

// TABLE : SELF_ACCOUNT_STUDENT
// FIELD : COMEFROM
//$oea_cfg["OLE_STUDENT_COMEFROM"]["teacherInput"] = 1;  // TEACHER INPUT FORM THE UI

### Default OEA Item
//$oea_cfg["OEA_Item_Info_Folder"] = $intranet_root.'/file/iportfolio/oea/'; // for generation
$oea_cfg["OEA_Item_Info_Folder"] = $intranet_root.'/home/portfolio/oea/';	// for deploy
$oea_cfg["OEA_Item_Csv_File_Name"] = 'oea_item.csv';
$oea_cfg["OEA_Item_Php_File_Name"] = 'oea_item.php';

$oea_cfg["OEA_Thickbox_Width"] = 750;
$oea_cfg["OEA_Thickbox_Height"] = 450;

$oea_cfg["OEA_Role_Type"] = array(
									"L"=>$Lang['iPortfolio']['OEA']['RoleType']['L'],
									"C"=>$Lang['iPortfolio']['OEA']['RoleType']['C'],
									"M"=>$Lang['iPortfolio']['OEA']['RoleType']['M']
									);
									
$oea_cfg["OEA_Achievement_Type_GoldMedal"] = "G";
$oea_cfg["OEA_Achievement_Type_BronzeMedal"] = "B";
$oea_cfg["OEA_Achievement_Type_OtherAwards"] = "O";
$oea_cfg["OEA_Achievement_Type_NoAward"] = "N";
$oea_cfg["OEA_Achievement_Type"] = array(
									$oea_cfg["OEA_Achievement_Type_GoldMedal"]=>$Lang['iPortfolio']['OEA']['AchievementType']['G'],
									$oea_cfg["OEA_Achievement_Type_BronzeMedal"]=>$Lang['iPortfolio']['OEA']['AchievementType']['B'],
									$oea_cfg["OEA_Achievement_Type_OtherAwards"]=>$Lang['iPortfolio']['OEA']['AchievementType']['O'],
									$oea_cfg["OEA_Achievement_Type_NoAward"]=>$Lang['iPortfolio']['OEA']['AchievementType']['N']
									);
$oea_cfg["OEA_ParticipationNature_Type"] = array(
									"C"=>$Lang['iPortfolio']['OEA']['ParticipationNatureType']['C'],
									"N"=>$Lang['iPortfolio']['OEA']['ParticipationNatureType']['N'],
									"P"=>$Lang['iPortfolio']['OEA']['ParticipationNatureType']['P']
									);


$oea_cfg["OEA_Participation_Type_School"] = 'S';   //school activity
$oea_cfg["OEA_Participation_Type_Private"] = 'P';  //non-school activity
$oea_cfg["OEA_Participation_Type"] = array(
									$oea_cfg["OEA_Participation_Type_School"]=>$Lang['iPortfolio']['OEA']['Part1'].' - '.$Lang['iPortfolio']['OEA']['OEA_Int'],
									$oea_cfg["OEA_Participation_Type_Private"]=>$Lang['iPortfolio']['OEA']['Part2'].' - '.$Lang['iPortfolio']['OEA']['OEA_Ext']
									);
									
$oea_cfg["OEA_AwardBearing_Type_Yes"] = 'Y';
$oea_cfg["OEA_AwardBearing_Type_No"] = 'N';
$oea_cfg["OEA_AwardBearing_Type"] = array(
									$oea_cfg["OEA_AwardBearing_Type_Yes"]=>$Lang['iPortfolio']['OEA']['OEA_AwardBearing'],
									$oea_cfg["OEA_AwardBearing_Type_No"]=>$Lang['iPortfolio']['OEA']['OEA_NotAwardBearing']
									);
									
									
### Student View Tabs		
define("IPF_OEA_OEA_TAB", 0);
define("IPF_OEA_ADDITIONAL_INFO_TAB", 1);
define("IPF_OEA_ABILITY_TAB", 2);
define("IPF_OEA_ACADEMIC_TAB", 3);
define("IPF_OEA_SUPPLEMENTARY_INFO_TAB", 4);
define("IPF_OEA_REPORT_TAB", 5);
$oea_cfg["TOP_TAB"]["STUDENT_SETTING"][IPF_OEA_OEA_TAB] = array("index.php", $Lang['iPortfolio']['OEA']['OEA_ShortName'], 0);
$oea_cfg["TOP_TAB"]["STUDENT_SETTING"][IPF_OEA_ADDITIONAL_INFO_TAB] = array("index.php?task=addiInfo_list", $Lang['iPortfolio']['OEA']['AdditionalInformation'], 0);
$oea_cfg["TOP_TAB"]["STUDENT_SETTING"][IPF_OEA_REPORT_TAB] = array("index.php?task=report", $Lang['eDiscipline']['Report'], 0);
//$oea_cfg["TOP_TAB"]["STUDENT_SETTING"][IPF_OEA_ABILITY_TAB] = array("index.php?task=ability_list", $Lang['iPortfolio']['OEA']['AdditionalInformation'], 0);
//$oea_cfg["TOP_TAB"]["STUDENT_SETTING"][IPF_OEA_ACADEMIC_TAB] = array("index.php?task=academic_list", $Lang['iPortfolio']['OEA']['AdditionalInformation'], 0);
//$oea_cfg["TOP_TAB"]["STUDENT_SETTING"][IPF_OEA_SUPPLEMENTARY_INFO_TAB] = array("index.php?task=suppInfo_list", $Lang['iPortfolio']['OEA']['SupplementaryInformation'], 0);


### Admin View Tabs
define("IPF_OEA_SETTINGS_TAB", 0);
define("IPF_OEA_ITEM_MAPPING_TAB", 1);
define("IPF_OEA_APPNO_IMPORT_TAB", 2);
define("IPF_OEA_ACADEMIC_SETTINGS_TAB", 3);
define("IPF_OEA_IPF_JUPAS_BASIC_TAB", 4);

$oea_cfg["TOP_TAB"]["ADMIN_SETTING"][IPF_JUPAS_BASIC_TAB] = array($Lang['iPortfolio']['OEA']['AcademicArr']['Basic'], "index.php?task=jupasBasic", 0);
$oea_cfg["TOP_TAB"]["ADMIN_SETTING"][IPF_OEA_SETTINGS_TAB] = array($Lang['iPortfolio']['OEA']['OEA_ShortName'], "index.php?task=oeaSettings", 0);
//$oea_cfg["TOP_TAB"]["ADMIN_SETTING"][IPF_OEA_ITEM_MAPPING_TAB] = array($Lang['iPortfolio']['OEA']['itemMap'], "index.php?task=oeaItemMap&clearCoo=1", 0);
//$oea_cfg["TOP_TAB"]["ADMIN_SETTING"][IPF_OEA_APPNO_IMPORT_TAB] = array($Lang['iPortfolio']['OEA']['appNoImport'], "index.php?task=appNoImport", 0);
$oea_cfg["TOP_TAB"]["ADMIN_SETTING"][IPF_OEA_ACADEMIC_SETTINGS_TAB] = array($Lang['iPortfolio']['OEA']['academicSettings'], "index.php?task=academicSettings", 0);


### Admin OEA Settings Sub-Tab
define("IPF_OEA_SETTINGS_OEA_BASIC", 0);
define("IPF_OEA_SETTINGS_OEA_OLE_MAPPING", 1);
define("IPF_OEA_SETTINGS_OEA_OLE_ROLE_MAPPING", 2);
$oea_cfg["TOP_TAB"]["ADMIN_SETTING_OEA"][IPF_OEA_SETTINGS_OEA_BASIC] = array("index.php?task=oeaSettings", $Lang['iPortfolio']['OEA']['AcademicArr']['Basic'], 0);
$oea_cfg["TOP_TAB"]["ADMIN_SETTING_OEA"][IPF_OEA_SETTINGS_OEA_OLE_MAPPING] = array("index.php?task=oeaItemMap&clearCoo=1", $Lang['iPortfolio']['OEA']['itemMap'], 0);
$oea_cfg["TOP_TAB"]["ADMIN_SETTING_OEA"][IPF_OEA_SETTINGS_OEA_OLE_ROLE_MAPPING] = array("index.php?task=oeaRoleMap", $Lang['iPortfolio']['OEA']['roleMap'] , 0);


### Admin Academic Settings Sub-Tabs
define("IPF_OEA_SETTINGS_ACADEMIC_BASIC", 0);
define("IPF_OEA_SETTINGS_ACADEMIC_SUBJECT", 1);
define("IPF_OEA_SETTINGS_ACADEMIC_PERCENTILE_AND_OVERALL_RATING", 2);
$oea_cfg["TOP_TAB"]["ADMIN_SETTING_ACADEMIC"][IPF_OEA_SETTINGS_ACADEMIC_BASIC] = array("index.php?task=academicSettings", $Lang['iPortfolio']['OEA']['AcademicArr']['Basic'], 0);
$oea_cfg["TOP_TAB"]["ADMIN_SETTING_ACADEMIC"][IPF_OEA_SETTINGS_ACADEMIC_SUBJECT] = array("index.php?task=academicSubject", $Lang['SysMgr']['SubjectClassMapping']['Subject'], 0);
//$oea_cfg["TOP_TAB"]["ADMIN_SETTING_ACADEMIC"][IPF_OEA_SETTINGS_ACADEMIC_PERCENTILE_AND_OVERALL_RATING] = array("index.php?task=academicPercentile", $Lang['iPortfolio']['OEA']['AcademicArr']['PercentileAndOverallRating'], 0);



### JUPAS DEFAULT OTHERS CODE 
$oea_cfg["OEA_Default_Category_Others"] = "00000";
$oea_cfg["OEA_Default_Category_Others_CatCode"] = "6";
									
### Jupas Item Type Internal Code
$oea_cfg["JupasItemInternalCode"]["OeaItem"] = 'OeaItem';
$oea_cfg["JupasItemInternalCode"]["AddiInfo"] = 'AddiInfo';
$oea_cfg["JupasItemInternalCode"]["Ability"] = 'Ability';
$oea_cfg["JupasItemInternalCode"]["Academic"] = 'Academic';
$oea_cfg["JupasItemInternalCode"]["SuppInfo"] = 'SuppInfo';


### Jupas Item Csv Name
$oea_cfg["JupasCsvName"]["OeaItem"] = 'oea.csv';
$oea_cfg["JupasCsvName"]["AddiInfo"] = 'additional_info.csv';
$oea_cfg["JupasCsvName"]["Ability"] = 'personal_and_general_abilities.csv';
$oea_cfg["JupasCsvName"]["Academic"] = 'academic_performance.csv';
$oea_cfg["JupasCsvName"]["AcademicPercentile"] = 'academic_performance_percentile.csv';
$oea_cfg["JupasCsvName"]["AcademicRating"] = 'academic_performance_rating.csv';
$oea_cfg["JupasCsvName"]["SuppInfo"] = 'supplementary_info.csv';


### Jupas Item Csv Header
// Student common info
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"] = array();
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'Application Number';
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'School Code';
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'HKID';
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'Passport Number';
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'Issue Country';
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'Last Name';
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'First Name';
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'Class';
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'Student Number';
$oea_cfg["JupasCsvHeaderArr"]["CommonItem"][] = 'Group';

// Oea Item
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"] = $oea_cfg["JupasCsvHeaderArr"]["CommonItem"];
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Sequence Number';
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Activity Name';
//$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Category Code';
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Year From';
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Year To';
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Participation';
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Award Bearing';
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Award Nature';
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Participation Mode';
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Award Type';
$oea_cfg["JupasCsvHeaderArr"]["OeaItem"][] = 'Description';

// Additional Info
$oea_cfg["JupasCsvHeaderArr"]["AddiInfo"] = $oea_cfg["JupasCsvHeaderArr"]["CommonItem"];
$oea_cfg["JupasCsvHeaderArr"]["AddiInfo"][] = 'Additional Information';

// Ability
$oea_cfg["JupasCsvHeaderArr"]["Ability"] = $oea_cfg["JupasCsvHeaderArr"]["CommonItem"];
//Comment out on 2 
//$oea_cfg["JupasCsvHeaderArr"]["Ability"][] = 'Attribute';
//$oea_cfg["JupasCsvHeaderArr"]["Ability"][] = 'Rating';

// Academic
$oea_cfg["JupasCsvHeaderArr"]["Academic"] = $oea_cfg["JupasCsvHeaderArr"]["CommonItem"];
$oea_cfg["JupasCsvHeaderArr"]["Academic"][] = 'Subject Code';
$oea_cfg["JupasCsvHeaderArr"]["Academic"][] = 'Percentile';
$oea_cfg["JupasCsvHeaderArr"]["Academic"][] = 'Overall Rating';

// Additional Info
$oea_cfg["JupasCsvHeaderArr"]["SuppInfo"] = $oea_cfg["JupasCsvHeaderArr"]["CommonItem"];
$oea_cfg["JupasCsvHeaderArr"]["SuppInfo"][] = 'Supplementary Information';


$oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"] = 0;
$oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"] = 1;
$oea_cfg["OEA_STUDENT_RECORDSTATUS_REJECTED"] = 2;

$oea_cfg["OEA_ADDITIONAL_INFO_RecordStatus"]["Pending"] = 0;
$oea_cfg["OEA_ADDITIONAL_INFO_RecordStatus"]["Approved"] = 1;
$oea_cfg["OEA_ADDITIONAL_INFO_RecordStatus"]["Rejected"] = 2;

$oea_cfg["OEA_STUDENT_ABILITY_RecordStatus"]["Pending"] = 0;
$oea_cfg["OEA_STUDENT_ABILITY_RecordStatus"]["Approved"] = 1;
$oea_cfg["OEA_STUDENT_ABILITY_RecordStatus"]["Rejected"] = 2;

$oea_cfg["OEA_STUDENT_ACADEMIC_RecordStatus"]["Pending"] = 0;
$oea_cfg["OEA_STUDENT_ACADEMIC_RecordStatus"]["Approved"] = 1;
$oea_cfg["OEA_STUDENT_ACADEMIC_RecordStatus"]["Rejected"] = 2;

$oea_cfg["OEA_SUPPLEMENTARY_INFO_RecordStatus"]["Pending"] = 0;
$oea_cfg["OEA_SUPPLEMENTARY_INFO_RecordStatus"]["Approved"] = 1;
$oea_cfg["OEA_SUPPLEMENTARY_INFO_RecordStatus"]["Rejected"] = 2;

$oea_cfg["OEA_ADDITIONAL_INFO_Details"]["MaxLength"] = 0;
$oea_cfg["OEA_ADDITIONAL_INFO_Details"]["MaxWordCount"] = 500;
$oea_cfg["OEA_SUPPLEMENTARY_INFO_Details"]["MaxLength"] = 2000;
$oea_cfg["OEA_SUPPLEMENTARY_INFO_Details"]["MaxWordCount"] = 0;
$oea_cfg["OEA_STUDENT_Description"]["MaxLength"] = 300;
$oea_cfg["OEA_STUDENT_Description"]["MaxWordCount"] = 0;

$oea_cfg["OEA_MaxOEAItem"] = 20;
$oea_cfg["OEA_YearSelection_NoOfYearBefore"] = 15;
$oea_cfg["OEA_YearSelection_NoOfYearAfter"] = 1;


## PLEASE DON'T CHANGE THE ARRAY VALUE UNLESS YOU SURE THE CHANGE ,SINCE THE VALUE IS SAVE IN THE DB
$oea_cfg["Ability"]["Attribute"] = array();
$oea_cfg["Ability"]["Attribute"]['AbilityToCommunicate']['JupasCode'] = 'Comm';
$oea_cfg["Ability"]["Attribute"]['AbilityToWorkWithOthers']['JupasCode'] = 'W/oth';
$oea_cfg["Ability"]["Attribute"]['AnalyticalPower']['JupasCode'] = 'AP';
$oea_cfg["Ability"]["Attribute"]['Conduct']['JupasCode'] = 'Cond';
$oea_cfg["Ability"]["Attribute"]['Creativity']['JupasCode'] = 'Creat';
$oea_cfg["Ability"]["Attribute"]['IndependenceOfMind']['JupasCode'] = 'Idp';
$oea_cfg["Ability"]["Attribute"]['Industriousness']['JupasCode'] = 'Ind';
$oea_cfg["Ability"]["Attribute"]['Initiative']['JupasCode'] = 'Init';
$oea_cfg["Ability"]["Attribute"]['Leadership']['JupasCode'] = 'Lead';
$oea_cfg["Ability"]["Attribute"]['Maturity']['JupasCode'] = 'Mat';
$oea_cfg["Ability"]["Attribute"]['Perseverance']['JupasCode'] = 'Pers';
$oea_cfg["Ability"]["Attribute"]['SenseOfResponsibility']['JupasCode'] = 'Resp';
$oea_cfg["Ability"]["Attribute"]['OverallEvaluation']['JupasCode'] = 'Ov';

$oea_cfg["Ability"]["Attribute"]['AbilityToCommunicate']['Order'] = '9';
$oea_cfg["Ability"]["Attribute"]['AbilityToWorkWithOthers']['Order'] = '10';
$oea_cfg["Ability"]["Attribute"]['AnalyticalPower']['Order'] = '3';
$oea_cfg["Ability"]["Attribute"]['Conduct']['Order'] = '8';
$oea_cfg["Ability"]["Attribute"]['Creativity']['Order'] = '12';
$oea_cfg["Ability"]["Attribute"]['IndependenceOfMind']['Order'] = '4';
$oea_cfg["Ability"]["Attribute"]['Industriousness']['Order'] = '1';
$oea_cfg["Ability"]["Attribute"]['Initiative']['Order'] = '6';
$oea_cfg["Ability"]["Attribute"]['Leadership']['Order'] = '5';
$oea_cfg["Ability"]["Attribute"]['Maturity']['Order'] = '2';
$oea_cfg["Ability"]["Attribute"]['Perseverance']['Order'] = '11';
$oea_cfg["Ability"]["Attribute"]['SenseOfResponsibility']['Order'] = '7';
$oea_cfg["Ability"]["Attribute"]['OverallEvaluation']['Order'] = '13';

$oea_cfg["Ability"]["Rating"] = array();
$oea_cfg["Ability"]["Rating"]['Excellent']['JupasCode'] = 1;
$oea_cfg["Ability"]["Rating"]['Good']['JupasCode'] = 2;
$oea_cfg["Ability"]["Rating"]['Average']['JupasCode'] = 3;
$oea_cfg["Ability"]["Rating"]['BelowAverage']['JupasCode'] = 4;
$oea_cfg["Ability"]["Rating"]['UnableToJudge']['JupasCode'] = 0;

$oea_cfg["Academic"]["Percentile"] = array();
$oea_cfg["Academic"]["Percentile"][1]['LowerLimit'] = '10';
$oea_cfg["Academic"]["Percentile"][1]['JupasCode'] = 'P1';
$oea_cfg["Academic"]["Percentile"][2]['LowerLimit'] = '25';
$oea_cfg["Academic"]["Percentile"][2]['JupasCode'] = 'P2';
$oea_cfg["Academic"]["Percentile"][3]['LowerLimit'] = '50';
$oea_cfg["Academic"]["Percentile"][3]['JupasCode'] = 'P3';
$oea_cfg["Academic"]["Percentile"][4]['LowerLimit'] = '75';
$oea_cfg["Academic"]["Percentile"][4]['JupasCode'] = 'P4';
$oea_cfg["Academic"]["Percentile"][5]['LowerLimit'] = '100';
$oea_cfg["Academic"]["Percentile"][5]['JupasCode'] = 'P5';
$oea_cfg["Academic"]["Percentile"][6]['LowerLimit'] = '';
$oea_cfg["Academic"]["Percentile"][6]['JupasCode'] = 'P6';

$oea_cfg["Academic"]["OverallRating"] = array();
$oea_cfg["Academic"]["OverallRating"]['A']['JupasCode'] = 'R1';
$oea_cfg["Academic"]["OverallRating"]['B']['JupasCode'] = 'R2';
$oea_cfg["Academic"]["OverallRating"]['C']['JupasCode'] = 'R3';
$oea_cfg["Academic"]["OverallRating"]['D']['JupasCode'] = 'R4';
$oea_cfg["Academic"]["OverallRating"]['E']['JupasCode'] = 'R5';
$oea_cfg["Academic"]["OverallRating"]['U']['JupasCode'] = 'R6';

//Academic Performance Unable to judge Array Keys
$oea_cfg["Academic"]["PercentileUnabletoJudge"] = 6;
$oea_cfg["Academic"]["OverallRatingUnabletoJudge"] = 'U';

$oea_cfg["OEA_SuggestListDisplayLimit"] = 10;
$oea_cfg["OEA_SuggestRankLowerLimit"] = 40;

$oea_cfg["OEA_JUPAS_Info_FilePath"] = 'jupas_oea_info.xls';

$oea_cfg["Setting"]["Max_No_Of_Oea_Record"] = 20;


// Property --> 1: Required, 2: Reference, 3: Optional
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"] = array();
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OLE_ProgramID",
																		"Property" => 1,
																		"ShowInUI" => 1,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramID'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramID'],
																		);
/*
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OLE_ProgramName",
																		"Property" => 1,
																		"ShowInUI" => 1,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramName'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramName'],
																		"SqlFiled" => "concat(OLE_ProgramName,\'abc\') as `OLE_ProgramName`",
																		);
*/
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OLE_ProgramName",
																		"Property" => 1,
																		"ShowInUI" => 1,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramName'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramName'],
																		);

$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OLE_AcademicYear",
																		"Property" => 2,
																		"ShowInUI" => 1,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_AcademicYear'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_AcademicYear'],
																		);
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OLE_NumOfStudentJoined",
																		"Property" => 2,
																		"ShowInUI" => 0,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_NumOfStudentJoined'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_NumOfStudentJoined'],
																		);
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OLE_ProgramNature",
																		"Property" => 2,
																		"ShowInUI" => 1,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OLE_ProgramNature'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OLE_ProgramNature'],
																		);
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OEA_Suggestions",
																		"Property" => 2,
																		"ShowInUI" => 0,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_Suggestions'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_Suggestions'],
																		);
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OEA_ProgramCode",
																		"Property" => 1,
																		"ShowInUI" => 1,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_ProgramCode'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_ProgramCode'],
																		);
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OEA_ProgramName",
																		"Property" => 2,
																		"ShowInUI" => 1,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_ProgramName'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_ProgramName'],
																		);
$oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"][] = array("DBField" => "OEA_AwardBearing",
																		"Property" => 1,
																		"ShowInUI" => 1,
																		"En" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['En']['OEA_AwardBearing'],
																		"Ch" => $Lang['iPortfolio']['OEA']['OLEMappingImportTitleArr']['Ch']['OEA_AwardBearing'],
																		);
																		
$oea_cfg["Setting"]["OLE_OEA_Mapping_Import"]["AwardBearing_Yes"] = "Y";
$oea_cfg["Setting"]["OLE_OEA_Mapping_Import"]["AwardBearing_No"] = "N";
$oea_cfg["Setting"]["OLE_OEA_Mapping_Import"]["DefaultAwardBearing"] = $oea_cfg["Setting"]["OLE_OEA_Mapping_Import"]["AwardBearing_Yes"];

$oea_cfg["Setting"]["ItemAry"] = array(
									"MaxNoOfOea", 
									"OeaAutoApproval", 
									"OeaAutoApprovalPart2",
									"StudentSubmissionPeriod",
									"EditableWhenApproved",
									"Academic_AcademicYearID",
									"Academic_YearTermID",
									"SchoolCode",
									"JupasApplicableFormIDList",
									"ModuleInUse",
									"Academic_PercentileMapWithOverAllRating",
									"AllowStudentNewOEA_NotFromOLE",
									"ForceStudentToApplyRoleMapping",
									"StudentApplyOLEToPart",
									"PartIAllowForNewOEA",
									"PartIIAllowForNewOEA",
									"AllowStudentApplyOthersOea"
								);
								
$oea_cfg["Setting"]["Default_ModuleInUse_Separator"] = "###";
$oea_cfg["Setting"]["Default_Separator2"] = ";;";

$ModuleInUseTmpArr = array();
foreach ((array)$oea_cfg["JupasItemInternalCode"] as $thisModule => $thisModuleInternalCode) {
	$ModuleInUseTmpArr[] = $thisModuleInternalCode;
}
$oea_cfg["Setting"]["Default_ModuleInUse"] = implode($oea_cfg["Setting"]["Default_ModuleInUse_Separator"], $ModuleInUseTmpArr);
unset($ModuleInUseTmpArr);
$oea_cfg["Setting"]["Default_AllowStudentNewOEA_NotFromOLE"] = "1";
$oea_cfg["Setting"]["Default_AllowStudentApplyOthersOea"] = "1";

$oea_cfg['Setting']['WebSAMSCodeOfApplicableFormForJupas'] = array('S5','S6','s5','s6');
$oea_cfg['Setting']['WebSAMSCodeOfApplicableFormForJupasAcademic'] = array('S6','s6');
$oea_cfg['faqLink'] = "http://support.broadlearning.com/doc/help/faq/";

$oea_cfg['defaultNumPerPage'] = 50;  //default value to display record per page

$oea_cfg['printMode']['html'] = 'html';
$oea_cfg['printMode']['csv'] = 'csv';
?>