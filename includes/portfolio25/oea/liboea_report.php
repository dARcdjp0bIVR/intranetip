<?
# modifying : ivan

if (!defined("LIBOEA_REPORT_DEFINED"))         // Preprocessor directives
{
  define("LIBOEA_REPORT_DEFINED",true);

	class liboea_report {

		public function liboea_report(){
		}


	
		function printReportAsHtml($DataArr, $includeOleData){
			global $intranet_root,$Lang;

			include_once($intranet_root.'/includes/libuser.php');
			
			$numOfOeaData = count($DataArr);

			$h_table = '';
			$h_table .= '<table class="border_table" style="width:100%;" cellpadding="2">'."\r\n";
				$h_table .= '<thead>'."\r\n";
					$h_table .= '<tr>'."\r\n";
						$h_table .= '<th style="width:1%;">#</th>'."\r\n";
						$h_table .= '<th style="width:5%;">'.$Lang['General']['Class'].'</th>'."\r\n";
						$h_table .= '<th style="width:4%;">'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</th>'."\r\n";
						$h_table .= '<th style="width:8%;">'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</th>'."\r\n";
						$h_table .= '<th style="width:6%;">'.$Lang['iPortfolio']['OEA']['ApplicationNumber'].'</th>'."\r\n";
						$h_table .= '<th style="width:6%;">'.$Lang['iPortfolio']['OEA']['OeaCode'].'</th>'."\r\n";
						$h_table .= '<th style="width:15%;">'.$Lang['iPortfolio']['OEA']['OeaName'].'</th>'."\r\n";
						$h_table .= '<th style="width:5%;">'.$Lang['iPortfolio']['OEA']['Checking']['OEA_StartDate'].'</th>'."\r\n";
						$h_table .= '<th style="width:5%;">'.$Lang['iPortfolio']['OEA']['Checking']['OEA_EndDate'].'</th>'."\r\n";
						$h_table .= '<th style="width:5%;">'.$Lang['iPortfolio']['OEA']['Checking']['OEA_Role'].'</th>'."\r\n";
						$h_table .= '<th style="width:5%;">'.$Lang['iPortfolio']['OEA']['Checking']['OEA_AwardBearing'].'</th>'."\r\n";
						$h_table .= '<th style="width:6%;">'.$Lang['iPortfolio']['OEA']['Checking']['OEA_ParticipationNature'].'</th>'."\r\n";
						$h_table .= '<th style="width:8%;">'.$Lang['iPortfolio']['OEA']['Checking']['OEA_Achievement'].'</th>'."\r\n";
						$h_table .= '<th style="width:6%;">'.$Lang['iPortfolio']['OEA']['Checking']['OEA_Participation'].'</th>'."\r\n";
						$h_table .= '<th style="width:15%;">'.$Lang['iPortfolio']['OEA']['OeaDescription'].'</th>'."\r\n";
						
						if ($includeOleData) {
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleTitle'].'</th>'."\r\n";
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleAcademicYear'].'</th>'."\r\n";
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleTerm'].'</th>'."\r\n";
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleStartDate'].'</th>'."\r\n";
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleEndDate'].'</th>'."\r\n";
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleRole'].'</th>'."\r\n";
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleAwards/Certifications/Achievements'].'</th>'."\r\n";
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleCategory'].'</th>'."\r\n";
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleComponentOfOle'].'</th>'."\r\n";
							$h_table .= '<th>'.$Lang['iPortfolio']['OLE']['OleNature'].'</th>'."\r\n";
						}
						
					$h_table .= '</tr>'."\r\n";
				$h_table .= '</thead>'."\r\n";
				$h_table .= '<tbody>'."\r\n";
					if ($numOfOeaData == 0) {
						$h_table .= '<tr><td colspan="100%" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\r\n";
					}
					else {
						$previousClassName = '';
						$previousClassNumber = '';
						$studentRecordCount = 0;
						for($i=0,$i_max=$numOfOeaData; $i<$i_max; $i++) {
							$_dataArr = $DataArr[$i];
							$_className = $DataArr[$i][0];
							$_classNumber = $DataArr[$i][1];
							
							$_tdStyle = '';
							if (($previousClassName=='' && $previousClassNumber=='') || ($previousClassName!=$_className || $previousClassNumber!=$_classNumber)) {
								// the first row of each student
								$_tdStyle = 'style="border-top-width: 3px;"';
								$studentRecordCount = 0;
							}
							
							
							$_numOfCol = count($_dataArr);
							$h_table .= '<tr>'."\r\n";
								$h_table .= '<td '.$_tdStyle.'>'.(++$studentRecordCount).'</td>'."\r\n";
								for ($j=0; $j<$_numOfCol; $j++) {
									$h_table .= '<td '.$_tdStyle.'>'.nl2br($_dataArr[$j]).'</td>'."\r\n";
								}
							$h_table .= '</tr>'."\r\n";
							
							$previousClassName = $_className;
							$previousClassNumber = $_classNumber;
						}
					}
				$h_table .= '</tbody>'."\r\n";
			$h_table .= '</table>'."\r\n";
			return $h_table;
		}

		function printReportAsCSV($DataArr, $includeOleData){
			global $intranet_root,$Lang;

			include_once($intranet_root.'/includes/libexporttext.php');

			$lexport = new libexporttext();
			$ExportHeaderArr = array();
			$ExportHeaderArr[] = $Lang['General']['Class'];
			$ExportHeaderArr[] = $Lang['SysMgr']['FormClassMapping']['ClassNo'];
			$ExportHeaderArr[] = $Lang['SysMgr']['FormClassMapping']['StudentName'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['ApplicationNumber'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['OeaCode'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['OeaName'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['Checking']['OEA_StartDate'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['Checking']['OEA_EndDate'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['Checking']['OEA_Role'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['Checking']['OEA_AwardBearing'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['Checking']['OEA_ParticipationNature'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['Checking']['OEA_Achievement'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['Checking']['OEA_Participation'];
			$ExportHeaderArr[] = $Lang['iPortfolio']['OEA']['OeaDescription'];
			
			if ($includeOleData) {
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleTitle'];
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleAcademicYear'];
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleTerm'];
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleStartDate'];
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleEndDate'];
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleRole'];
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleAwards/Certifications/Achievements'];
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleCategory'];
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleComponentOfOle'];
				$ExportHeaderArr[] = $Lang['iPortfolio']['OLE']['OleNature'];
			}
			
			$ExportText = $lexport->GET_EXPORT_TXT($DataArr, $ExportHeaderArr, $Delimiter="\t", $LineBreak="\r\n", $ColumnDefDelimiter="\t", $DataSize=0, $Quoted="11", $includeLineBreak=1);
			
			$lexport->EXPORT_FILE('oea_student.csv', $ExportText);
			die();
		}

		function getStudentDataForReport($StudentIDArr, $includeOleData){
			global $intranet_root, $Lang, $ipf_cfg, $iPort;
			include_once($intranet_root.'/includes/libuser.php');
			
			$liboea = new liboea();
			$liboea_item = new liboea_item();
			
			$objUser = new libuser($uid="",$UserLogin="", $StudentIDArr);

			### Get Student JUPAS info
			$StudentInfoArr = $liboea->getStudentInfoForCsv($StudentIDArr);

			### Get Student OEA info
			$ApprovedStatus = $liboea->getItemTypeApprovedStatus($liboea->getOEAItemInternalCode());
			$StudentOeaInfoArr = $liboea->getStudentOeaItemInfoArr($StudentIDArr, $ApprovedStatus, $ParticipationArr='', $ParRecordIDArr='', $ParExcludeRecordIDArr='', $OLE_ProgramIDArr='', $withCodeInOeaTitle=false);
			$StudentOeaInfoAssoArr = BuildMultiKeyAssoc($StudentOeaInfoArr, 'StudentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			
			if ($includeOleData) {
				$libpf_ole = new libpf_ole();
				$libpf_category = new Category();
				
				$OleProgramIDArr = array_values(array_unique(Get_Array_By_Key($StudentOeaInfoArr, 'OLE_PROGRAM_ProgramID')));
				$OleStudentRecordIDArr = array_values(array_unique(Get_Array_By_Key($StudentOeaInfoArr, 'OLE_STUDENT_RecordID')));
			
				### Get the related OLE Program Info
				$OleProgramInfoArr = $libpf_ole->Get_OLE_Info($FormIDArr='', $OleProgramIDArr);
				$OleProgramAcademicYearIDArr = array_values(array_unique(Get_Array_By_Key($OleProgramInfoArr, 'AcademicYearID')));
				$OleProgramAcademicYearTermIDArr = array_values(array_unique(Get_Array_By_Key($OleProgramInfoArr, 'YearTermID')));
				$OleProgramAssoArr = BuildMultiKeyAssoc($OleProgramInfoArr, 'ProgramID');
				unset($OleProgramInfoArr);
				
				### Get the related OLE Student Record Info
				$OleStudentRecordInfoArr = $libpf_ole->Get_Student_OLE_Info('', $OleStudentRecordIDArr, $forSlpOnly='');
				$OleStudentRecordAssoArr = BuildMultiKeyAssoc($OleStudentRecordInfoArr, 'OleStudentRecordID');
				unset($OleStudentRecordInfoArr);
				
				### Get the Academic Year Mapping
				$AcademicYearInfoArr = GetAllAcademicYearInfo();
				$AcademicYearAssoArr = BuildMultiKeyAssoc($AcademicYearInfoArr, 'AcademicYearID');
				unset($AcademicYearInfoArr);
				
				### Get the Academic Term Mapping
				$YearTermAssoArr = getSemesters();
				
				### Get the OLE Category Mapping
				$OleCategoryInfoArr = $libpf_category->GET_CATEGORY_LIST();
				$OleCategoryAssoArr = BuildMultiKeyAssoc($OleCategoryInfoArr, 'RecordID');
				unset($OleCategoryInfoArr);
				
				### Get ELE Mapping
				$EleInfoArr = $libpf_ole->Get_ELE_Info();
				$EleAssoArr = BuildMultiKeyAssoc($EleInfoArr, 'ELECode');
				unset($EleInfoArr);
			}
			unset($StudentOeaInfoArr);
			
			
			### Build display data array
			$DataArr = array();
			$numOfStudent = count($StudentInfoArr);
			$rowCounter = 0;
			for ($i=0; $i<$numOfStudent; $i++) {
				$_studentId = $StudentInfoArr[$i]['UserID'];
				$_applicationNumber = $StudentInfoArr[$i]['ApplicationNumber'];
				
				$objUser->LoadUserData($_studentId);
				$_className = $objUser->ClassName;
				$_classNumber = $objUser->ClassNumber;
				$_studentName = Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
				
				$_studentOeaInfoArr = $StudentOeaInfoAssoArr[$_studentId];
				$_studentNumOfOea = count($_studentOeaInfoArr);
				for ($j=0; $j<$_studentNumOfOea; $j++) {
					$__oeaCode = $_studentOeaInfoArr[$j]['OEA_ProgramCode'];
					$__oeaTitle = $_studentOeaInfoArr[$j]['Title'];
					$__oeaStartYear = $_studentOeaInfoArr[$j]['StartYear'];
					$__oeaEndYear = $_studentOeaInfoArr[$j]['EndYear'];
					$__oeaRole = $_studentOeaInfoArr[$j]['Role'];
					$__oeaAwardBearing = $_studentOeaInfoArr[$j]['OEA_AwardBearing'];
					$__oeaParticipationNature = $_studentOeaInfoArr[$j]['ParticipationNature'];
					$__oeaAchievement = $_studentOeaInfoArr[$j]['Achievement'];
					$__oeaParticipation = $_studentOeaInfoArr[$j]['Participation'];
					$__oeaDescription = $_studentOeaInfoArr[$j]['Description'];
					
					
					$__colCounter = 0;
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($_className);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($_classNumber);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($_studentName);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($_applicationNumber);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oeaCode);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oeaTitle);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oeaStartYear);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oeaEndYear);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($liboea_item->Get_OEA_RoleName_By_RoleCode($__oeaRole));
					$DataArr[$rowCounter][$__colCounter++] = ($__oeaAwardBearing=='Y')? $Lang['General']['Yes'] : $Lang['General']['No'];
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($liboea_item->Get_OEA_ParticipationNatureName_By_ParticipationNatureCode($__oeaParticipationNature));
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($liboea_item->Get_OEA_AchievementName_By_AchievementCode($__oeaAchievement));
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($liboea_item->Get_OEA_ParticipationName_By_ParticipationCode($__oeaParticipation));
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oeaDescription);
					
					if ($includeOleData) {
						$__oleProgramId = $_studentOeaInfoArr[$j]['OLE_PROGRAM_ProgramID'];
						$__oleStudentRecordId = $_studentOeaInfoArr[$j]['OLE_STUDENT_RecordID'];
						
						// Get data from OLE_PROGRAM Asso Array
						$__oleIntExt = $OleProgramAssoArr[$__oleProgramId]['IntExt'];
						$__oleAcademicYearID = $OleProgramAssoArr[$__oleProgramId]['AcademicYearID'];
						$__oleYearTermID = $OleProgramAssoArr[$__oleProgramId]['YearTermID'];
						$__oleStartDate = $OleProgramAssoArr[$__oleProgramId]['StartDate'];
						$__oleEndDate = $OleProgramAssoArr[$__oleProgramId]['EndDate'];
						$__oleCategoryID = $OleProgramAssoArr[$__oleProgramId]['Category'];
						$__oleEle = $OleProgramAssoArr[$__oleProgramId]['ELE'];
						
						// Get data from OLE_STUDENT Asso Array
						$__oleRole = $OleStudentRecordAssoArr[$__oleStudentRecordId]['Role'];
						$__oleAchievement = $OleStudentRecordAssoArr[$__oleStudentRecordId]['Achievement'];
						
						// Title
						$__oleProgramTitle = Get_Lang_Selection($OleProgramAssoArr[$__oleProgramId]['TitleCh'], $OleProgramAssoArr[$__oleProgramId]['TitleEn']);
						
						// IntExt
						$__oleIntExtDisplay = '';
						if (strtoupper($__oleIntExt) == strtoupper($ipf_cfg["OLE_TYPE_STR"]["INT"])) {
							$__oleIntExtDisplay = $iPort["internal_record"];
						}
						else if (strtoupper($__oleIntExt) == strtoupper($ipf_cfg["OLE_TYPE_STR"]["EXT"])) {
							$__oleIntExtDisplay = $iPort["external_record"];
						}
						
						// Academic Year
						$__oleAcademicYearName = Get_Lang_Selection($AcademicYearAssoArr[$__oleAcademicYearID]['YearNameB5'], $AcademicYearAssoArr[$__oleAcademicYearID]['YearNameEN']);
						
						// Year Term
						$__oleYearTermName = $YearTermAssoArr[$__oleYearTermID];
						
						// Start Date & End Date
						$__oleStartDate = (is_date_empty($__oleStartDate))? $Lang['General']['EmptySymbol'] : $__oleStartDate;
						$__oleEndDate = (is_date_empty($__oleEndDate))? $Lang['General']['EmptySymbol'] : $__oleEndDate;
						
						// OLE Category
						$__oleCategoryName = Get_Lang_Selection($OleCategoryAssoArr[$__oleCategoryID]['ChiTitle'], $OleCategoryAssoArr[$__oleCategoryID]['EngTitle']);
						
						// ELE
						$__oleEleArr = explode(',', $__oleEle);
						$__numOfEle = count($__oleEleArr);
						$__eleNameArr = array();
						for ($k=0; $k<$__numOfEle; $k++) {
							$___eleCode = $__oleEleArr[$k];
							$__eleNameArr[] = Get_Lang_Selection($EleAssoArr[$___eleCode]['ChiTitle'], $EleAssoArr[$___eleCode]['EngTitle']);
						}
						$__eleName = implode(', ', $__eleNameArr);
						
						
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleProgramTitle);
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleAcademicYearName);
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleYearTermName);
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleStartDate);
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleEndDate);
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleRole);
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleAchievement);
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleCategoryName);
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__eleName);
						$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleIntExtDisplay);
					}
					
					$rowCounter++;
				}
			}

			return $DataArr;
		}
		
		public function getReportTitle() {
			global $Lang, $sys_custom;
			
			$numOfCustReport = count((array)$sys_custom['iPf']['JUPASArr']['CustReportArr']);
			
			$reportTitle = ''; 
			$reportTitle .= $Lang['iPortfolio']['OEA']['ReportArr']['StudentOeaReport'];
			if ($numOfCustReport > 0) {
				$reportTitle .= ' ('.$Lang['iPortfolio']['OEA']['ReportArr']['DisplayInOeaFormat'].')';
			}
			return $reportTitle;
		}
	}
}
?>
