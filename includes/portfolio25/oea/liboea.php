<?
# modifying : Siuwan

/******************************************************************************
 * Modification log:
 * 2016-04-01 (Siuwan)[ip.2.5.7.4.1]
 * - Modified function getDisplayStudentSubmissionPeriod(),isSubmissionPeriod(), replace split() by explode() for PHP5.4
 * 
 * Ivan (2011-11-18)
 * Modified getStudentOeaItemInfoArr() added para $OLE_ProgramIDArr
 * 
 * Connie (2011/10/20): 
 * Modified getTeacher_listStuAllInfo_oeaItemTable_HTML(), 
 * getStudentOeaItemInfoArr(), getTeacher_listStuAllInfo_oeaItemTable()
 * 
 * 
 *****************************************************************************/


include_once($intranet_root."/includes/portfolio25/oea/oeaConfig.inc.php");

if (!defined("LIBOEA_DEFINED"))         // Preprocessor directives
{
  define("LIBOEA_DEFINED",true);

	class liboea extends libdb {
		
		private $SQL_Condition;
		private $NoOfYearBefore;
		private $NoOfYearAfter;
		private $MaxNoOfOea;
		private $StudentSubmissionPeriod;
		private $OeaAutoApproval;
		private $OeaAutoApprovalPart2;
		private $AllowSubmissionPeriod;
		private $EditableWhenApproved;
		private $Academic_AcademicYearID;
		private $Academic_YearTermID;
		private $SchoolCode;
		private $JupasApplicableFormIDList;
		private $ModuleInUse;
		private $Academic_PercentileMapWithOverAllRating;
		private $Subject_Academic_PercentileMapWithOverAllRatingArr;
		private $ForceStudentToApplyRoleMapping;
		private $PartIAllowForNewOEA;
		private $PartIIAllowForNewOEA;
		private $AllowStudentApplyOthersOea;
		
		private $StudentApplyOLEToPart;

		private $oea_part_settingAry;
		private $ole_type_settingAry;

		public function liboea() {
			global $oea_cfg;
			$this->libdb();	
			$this->NoOfYearBefore = $oea_cfg["OEA_YearSelection_NoOfYearBefore"];
			$this->NoOfYearAfter = $oea_cfg["OEA_YearSelection_NoOfYearAfter"];
			$setting = $this->getOeaSetting();
			
			if(sizeof($setting)>0) {
				for($i=0, $i_max=sizeof($setting); $i<$i_max; $i++) {
					list($name, $value) = $setting[$i];
					$this->$name = $value;
				}	
			} else {
				$this->MaxNoOfOea = $oea_cfg["Setting"]["Max_No_Of_Oea_Record"];
				$this->StudentSubmissionPeriod = "";
				$this->OeaAutoApproval = 0;
				$this->OeaAutoApprovalPart2 = 1;
				$this->EditableWhenApproved = 0;
				$this->SchoolCode = '';
				$this->JupasApplicableFormIDList = '';
				$this->Academic_PercentileMapWithOverAllRating = '';
				$this->ForceStudentToApplyRoleMapping = 0;
				$this->PartIAllowForNewOEA = 1;  //1--> default allowed
				$this->PartIIAllowForNewOEA = 1;  //1--> default allowed
			}

			$this->parseSettingForPartAndOLE();

			if (!isset($this->Academic_AcademicYearID)) {
				$this->Academic_AcademicYearID = Get_Current_Academic_Year_ID();
			}
			if (!isset($this->Academic_YearTermID)) {
				$this->Academic_YearTermID = 0;
			}
			if (!isset($this->ModuleInUse)) {
				$this->ModuleInUse = $oea_cfg["Setting"]["Default_ModuleInUse"];
			}
			if (!isset($this->AllowStudentNewOEA_NotFromOLE)) {
				$this->AllowStudentNewOEA_NotFromOLE = $oea_cfg["Setting"]["Default_AllowStudentNewOEA_NotFromOLE"];
			}
			if(!isset($this->ForceStudentToApplyRoleMapping)){
				$this->ForceStudentToApplyRoleMapping = 0; // default not force (0)
			}
			
			if(!isset($this->OeaAutoApprovalPart2)){
				$this->OeaAutoApprovalPart2 = 1; // default auto approve (1)
			}

			if(!isset($this->PartIAllowForNewOEA)){
				$this->PartIAllowForNewOEA = 1;  //1--> default allowed
			}

			if(!isset($this->PartIIAllowForNewOEA)){
				$this->PartIIAllowForNewOEA = 1;  //1--> default allowed
			}
			
			if (!isset($this->AllowStudentApplyOthersOea)) {
				$this->AllowStudentApplyOthersOea = $oea_cfg["Setting"]["Default_AllowStudentApplyOthersOea"];
			}
			

			$this->isSubmissionPeriod();
		}
		
		private function getOeaSetting()
		{
			$OEA_SETTING = $this->Get_Table_Name("OEA_SETTING");
			$sql = "SELECT SettingName, SettingValue FROM $OEA_SETTING";
			return $this->returnArray($sql);
		}
		
		private function Get_Table_Name($ParTable, $ParDB='')
		{
			global $eclass_db;
			
			$thisDB = ($ParDB=='')? $eclass_db : $ParDB;
			return $thisDB.'.'.$ParTable;
		}
		
		public function setMaxNoOfOea($maxOea="") {
			global $oea_cfg;
			$this->MaxNoOfOea = ($maxOea=="") ? $oea_cfg["Setting"]["Max_No_Of_Oea_Record"] : $maxOea;
		}
		public function getMaxNoOfOea() {
			return $this->MaxNoOfOea;
		}

		public function setPartIAllowForNewOEA($val = ''){
			$this->PartIAllowForNewOEA = (trim($val) == '')? 1:$val ;
		}
		public function getPartIAllowForNewOEA(){
			return $this->PartIAllowForNewOEA;
		}

		public function setPartIIAllowForNewOEA($val = ''){
			$this->PartIIAllowForNewOEA = (trim($val) == '')? 1:$val ;
		}
		public function getPartIIAllowForNewOEA(){
			return $this->PartIIAllowForNewOEA;
		}

		/**
		* CHECK WHETHER A GIVE PART IS ALLOWED FOR NEW OEA
		* @owner : Fai (20111024)
		* @param : String $part (Part 1 or Part 2)
		* @return : Boolean true ==> allow for this part , false ==> don't allow for this part
		* 
		*/

		public function getPartIsAllowForNewOEA($part){
			global $oea_cfg;
			$returnVal = false;

			$part = strtoupper($part);
			$compareFlag = '';

			//CHECK WHETHER USE WHICH FLAG
			if($part == strtoupper($oea_cfg["OEA_Participation_Type_School"])) {
				$compareFlag = $this->PartIAllowForNewOEA;
			}elseif($part == strtoupper($oea_cfg["OEA_Participation_Type_Private"])) {
				$compareFlag = $this->PartIIAllowForNewOEA;
			}else{
				return false;
			}

			//CHECK WHETHER THE FLAG IS ALLOWED
			if($compareFlag == 1){
				return true;
			}else{
				return false;
			}
		}
		
		public function setStudentSubmissionPeriod($period="") {
			$this->StudentSubmissionPeriod = $period;
		} 
		public function getStudentSubmissionPeriod() {
			return $this->StudentSubmissionPeriod;
		}
		public function getDisplayStudentSubmissionPeriod() {
			global $i_To, $Lang;
			$period = $this->StudentSubmissionPeriod;
			if($period=="") {
				return "-";	
			} else {
				$periodAry = explode("#", $period);
				return $periodAry[0]." $i_To ".$periodAry[1];			
			}
		}
		
		public function setOeaAutoApproval($autoApproval=0) {
			global $oea_cfg;
			$this->OeaAutoApproval = ($autoApproval) ? $autoApproval : $oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"];
		}

		public function setOeaAutoApprovalPart2($autoApproval=0) {
			global $oea_cfg;
			$this->OeaAutoApprovalPart2 = ($autoApproval) ? $autoApproval : $oea_cfg["OEA_STUDENT_RECORDSTATUS_PENDING"];
		}


		public function setForceStudentToApplyRoleMapping($val) {
			// 0--> do not force
			$this->ForceStudentToApplyRoleMapping = ($val == '') ? 0 : $val;
		}


		public function getOeaAutoApproval() {
			return $this->OeaAutoApproval;
		}	
		public function getOeaAutoApprovalPart2() {
			return $this->OeaAutoApprovalPart2;
		}	
		public function getForceStudentToApplyRoleMapping(){
			return $this->ForceStudentToApplyRoleMapping;
		}

		public function getStudentApplyOLEToPart(){
			return $this->StudentApplyOLEToPart;

		}
		public function setStudentApplyOLEToPart($val){
			$this->StudentApplyOLEToPart = $val;
		}

		public function setEditableWhenApproved($val) {
			$this->EditableWhenApproved = ($val=="") ? 0 : 1;
		}
		public function getEditableWhenApproved() {
			return $this->EditableWhenApproved;	
		}
		public function getIsSubmissionPeriod() {
			return $this->AllowSubmissionPeriod;
		}
		
		
		public function setAcademic_AcademicYearID($value='') {
			$this->Academic_AcademicYearID = ($value=='') ? Get_Current_Academic_Year_ID() : $value;
		}
		public function getAcademic_AcademicYearID() {
			return $this->Academic_AcademicYearID;
		}
		public function setAcademic_YearTermID($value='') {
			$this->Academic_YearTermID = ($value=='') ? 0 : $value;
		}
		public function getAcademic_YearTermID() {
			return $this->Academic_YearTermID;
		}
		
		public function setAcademic_PercentileMapWithOverAllRating($value='') {
			$this->Academic_PercentileMapWithOverAllRating = ($value=='') ? '' : $value;
		}

		public function getAcademic_PercentileMapWithOverAllRating($SubjectID=''){
			global $oea_cfg;
			
			if (!isset($this->Subject_Academic_PercentileMapWithOverAllRatingArr)) {
				$this->loadSubjectAcademicPercentileMapWithOverallRatingArr();
			}
			
			// Get Specific Subject Settings
			$SubjectSettingValue = '';
			if ($SubjectID != '') {
				$SubjectSettingValue = $this->getSubjectAcademicPercentileMapWithOverallRatingArr($SubjectID);
			}
			
			if ($SubjectID=='' || $SubjectSettingValue=='') {
				// Not Specific Subject or No Specfic Subject Settings => General Settings
				$_SettingValue = $this->Academic_PercentileMapWithOverAllRating;
			}
			else {
				$_SettingValue = $SubjectSettingValue;
			}
			if($_SettingValue != ''){
				$_tmpSetting = array();
				$_tmpSetting = explode($oea_cfg["Setting"]["Default_Separator2"],$_SettingValue);
				
				for($i =0,$i_max = count($_tmpSetting);$i< $i_max;$i++){
					$_tmpStr = $_tmpSetting[$i];
					list($code,$value) = explode($oea_cfg["Setting"]["Default_ModuleInUse_Separator"],$_tmpStr );
					$settingAry[$code] = $value;
				}
			}
			return $settingAry;
		}
		private function loadSubjectAcademicPercentileMapWithOverallRatingArr() {
			global $eclass_db;
			
			$OEA_SUBJECT_ACADEMIC_MAPPING = $eclass_db.'.OEA_SUBJECT_ACADEMIC_MAPPING';
			$sql = "Select
							SubjectID,
							SettingValue
					From
							OEA_SUBJECT_ACADEMIC_MAPPING
					";
			//$this->Subject_Academic_PercentileMapWithOverAllRatingArr = BuildMultiKeyAssoc($this->objDB->returnArray($sql, null, 1), 'SubjectID', array('SettingValue'), $SingleValue=1, $BuildNumericArray=0);
		}
		
		private function getSubjectAcademicPercentileMapWithOverallRatingArr($SubjectID) {
			return $this->Subject_Academic_PercentileMapWithOverAllRatingArr[$SubjectID];
		}

		public function setSchoolCode($value='') {
			$this->SchoolCode = $value;
		}
		public function getSchoolCode() {
			return $this->SchoolCode;
		}
		
		public function setJupasApplicableFormIDList($value='') {
			$this->JupasApplicableFormIDList = $value;
		}
		public function getJupasApplicableFormIDList() {
			return $this->JupasApplicableFormIDList;
		}
		public function getJupasApplicableFormIDArr() {
			return explode(',', $this->JupasApplicableFormIDList);
		}
		public function getJupasApplicableFormNameArr() {
			$libfcm = new form_class_manage();
			
			$FormInfoArr = $libfcm->Get_Form_List($GetMappedClassInfo=false, $ActiveOnly=1);
			$FormInfoAssoArr = BuildMultiKeyAssoc($FormInfoArr, array('YearID'), array('YearName'), $SingleValue=1);
			unset($FormInfoArr);
			
			$JupasFormIDArr = $this->getJupasApplicableFormIDArr();
			$numOfForm = count($JupasFormIDArr);
			
			$JupasFormNameArr = array();
			for ($i=0; $i<$numOfForm; $i++) {
				$thisFormID = $JupasFormIDArr[$i];
				$thisFormName = $FormInfoAssoArr[$thisFormID];
				
				if ($thisFormName != '') {
					$JupasFormNameArr[] = $thisFormName;
				}
			}
			
			return $JupasFormNameArr;
		}
		public function setModuleInUse($value='') {
			$this->ModuleInUse = $value;
		}
		public function getModuleInUse() {
			return $this->ModuleInUse;
		}
		public function setAllowStudentNewOEA_NotFromOLE($value='') {
			$this->AllowStudentNewOEA_NotFromOLE = $value;
		}
		public function getAllowStudentNewOEA_NotFromOLE() {
			return $this->AllowStudentNewOEA_NotFromOLE;
		}
		
		public function setAllowStudentApplyOthersOea($value) {
			$this->AllowStudentApplyOthersOea = $value;
		}
		public function getAllowStudentApplyOthersOea() {
			return $this->AllowStudentApplyOthersOea;
		}
		
		public static function getThickboxWidth() {
			global $oea_cfg;
			return $oea_cfg["OEA_Thickbox_Width"] ;
		}
		
		public static function getThickboxHeight() {
			global $oea_cfg;
			return $oea_cfg["OEA_Thickbox_Height"] ;
		}
		
		public function set_SQL_Condition($conds) {
			$this->SQL_Condition = $conds;
		}
		
		public function removeOEA_Item($ProgramID=array())
		{
			global $eclass_db;
			if(sizeof($ProgramID)>0) {
				$sql = "DELETE FROM $eclass_db.OEA_STUDENT WHERE RecordID IN (".implode(',',$ProgramID).")";
				$result = $this->db_db_query($sql);
				return $result;
			}
		}
		
		public function getOEA_StudentList_SQL()
		{
			global $eclass_db, $UserID, $oea_cfg, $image_path, $LAYOUT_SKIN, $Lang;
			
			$sql = "SELECT 
						If (o.OLE_STUDENT_RecordID != 0 && OLE_PROGRAM_ProgramID != 0,
							concat('<span class=\"tabletextrequire\">*</span>', 
								    if(o.Title is null,'',o.Title),' (',if(o.OEA_ProgramCode is null,'',o.OEA_ProgramCode),')'),
							concat(if(o.Title is null,'',o.Title),' (',if(o.OEA_ProgramCode is null,'',o.OEA_ProgramCode),')')
						) as TITLE,
						o.OEA_ProgramCode AS 'CATEGORY' , 
						o.Role AS 'ROLE' , 
						o.Achievement AS 'ACHIEVEMENT',
						CASE o.Participation
						    WHEN 'S' THEN '".$Lang['iPortfolio']['OEA']['Part1']."'
						    WHEN 'P' THEN '".$Lang['iPortfolio']['OEA']['Part2']."'
						END AS 'PARTICIPATION',
						LEFT(o.ModifyDate,10) AS 'MODIFYDATE',
						o.RecordStatus as 'RECORDSTATUS',
					";

			if($this->AllowSubmissionPeriod) {
				//$sql .= "IF((o.RecordStatus!=".$oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"]." OR ".$this->EditableWhenApproved." Or o.Participation='P'),CONCAT('<input type=\'checkbox\' name=\'programID[]\' id=\'programID[]\' value=', o.RecordID ,'>'),'-')";
				$sql .= " If (
								(o.Participation='".$oea_cfg["OEA_Participation_Type_School"]."' And '".$this->getOeaAutoApproval()."'='0' And o.RecordStatus='".$oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"]."')
								Or
								(o.Participation='".$oea_cfg["OEA_Participation_Type_Private"]."' And '".$this->getOeaAutoApprovalPart2()."'='0' And o.RecordStatus='".$oea_cfg["OEA_STUDENT_RECORDSTATUS_APPROVED"]."'),
								'-',
								CONCAT('<input type=\'checkbox\' name=\'programID[]\' id=\'programID[]\' value=', o.RecordID ,'>')
							 )
						";
			} else {
				$sql .= "'-'";	
			}
			
			$sql .= "	, 
						o.Title as TITLE_ORDER
					FROM 
						{$eclass_db}.OEA_STUDENT AS o 
					WHERE 
						StudentID = '$UserID'";
						
			# check condition 			
			if($this->SQL_Condition!="")
				$sql .= $this->SQL_Condition;
				
			return $sql;
		}	
		/*
		public function get_OLE_Student_List_SQL()
		{
			global $eclass_db, $UserID;
			
			$sql = "SELECT 
						p.title AS 'TITLE',
						IF(o.RecordID IS NULL, CONCAT('<input type =\"checkbox\" name=\"recordID[]\" value=\"' , s.recordid, '\" />'),'') AS 'ACTION',
						o.RecordID AS OEA_RecordID
					FROM 
						{$eclass_db}.OLE_STUDENT as s inner join
						{$eclass_db}.OLE_PROGRAM as p on p.programid = s.programid LEFT OUTER JOIN
						{$eclass_db}.OEA_STUDENT as o ON (o.OLE_STUDENT_RecordID=s.RecordID)
					WHERE 
						s.UserID = {$UserID}";	
						
			return $sql;
		}
		*/
		public function getOLEList($StudentID='')
		{
			$sql = "SELECT 
						p.Title, 
						p.CategoryID, 
						m.OEA_ProgramCode 
					FROM 
						OLE_PROGRAM p LEFT OUTER JOIN
						OEA_OLE_MAP m ON (p.ProgramID=m.OLE_ProgramID) 
					WHERE
						
					";
		} 
		
		public static function getOEAItemInternalCode() {
			global $oea_cfg;
			return $oea_cfg["JupasItemInternalCode"]["OeaItem"];
		}
		public static function getAddiInfoInternalCode() {
			global $oea_cfg;
			return $oea_cfg["JupasItemInternalCode"]["AddiInfo"];
		}
		public static function getAbilityInternalCode() {
			global $oea_cfg;
			return $oea_cfg["JupasItemInternalCode"]["Ability"];
		}
		public static function getAcademicInternalCode() {
			global $oea_cfg;
			return $oea_cfg["JupasItemInternalCode"]["Academic"];
		}
		public static function getSuppInfoInternalCode() {
			global $oea_cfg;
			return $oea_cfg["JupasItemInternalCode"]["SuppInfo"];
		}
		public static function getAPRemarksInternalCode() {
			global $oea_cfg;
			return $oea_cfg["JupasItemInternalCode"]["APRemarks"];
		}
		
		public function isEnabledOEAItem() {
			return $this->Is_Module_In_Use($this->getOEAItemInternalCode());
		}
		public function isEnabledAdditionalInfo() {
			return $this->Is_Module_In_Use($this->getAddiInfoInternalCode());
		}
		public function isEnabledAbility() {
			return $this->Is_Module_In_Use($this->getAbilityInternalCode());
		}
		public function isEnabledAcademicPerformance() {
			return $this->Is_Module_In_Use($this->getAcademicInternalCode());
		}
		public function isEnabledSupplementaryInfo() {
			return $this->Is_Module_In_Use($this->getSuppInfoInternalCode());
		}
		
		public static function getOEAItemJupasCsvName() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvName"]["OeaItem"];
		}
		public static function getAddiInfoJupasCsvName() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvName"]["AddiInfo"];
		}
		public static function getAbilityJupasCsvName() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvName"]["Ability"];
		}
		public static function getAcademicJupasCsvName($type=null) {
			global $oea_cfg;
			if($type=='percentile'){
				return $oea_cfg["JupasCsvName"]["AcademicPercentile"];
			}else if($type=='rating'){
				return $oea_cfg["JupasCsvName"]["AcademicRating"];
			}else{
				return $oea_cfg["JupasCsvName"]["Academic"];
			}
		}
		public static function getSuppInfoJupasCsvName() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvName"]["SuppInfo"];
		}
		
		public static function Include_JS_CSS()
		{
			global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
			
			$x = '
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/script.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/ipf_jupas.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.core.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.draggable.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.droppable.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/ui.selectable.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>
				<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.cookies.2.2.0.js"></script>
				
				
				<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
				';
				
			return $x;
		}
		
		function getStudentOEAItemCount($StudentIDArr)
		{
			global $eclass_db;
			
			$OEA_STUDENT = $eclass_db.'.OEA_STUDENT';
			$sql = "Select
							StudentID,
							RecordStatus,
							Count(*) as StudentOEAItemCount
					From
							$OEA_STUDENT
					Where
							StudentID In (".implode(',', (array)$StudentIDArr).")
					Group By
							StudentID, RecordStatus
					";
			return BuildMultiKeyAssoc($this->returnArray($sql), array('StudentID', 'RecordStatus'), $IncludedDBField=array('StudentOEAItemCount'), $SingleValue=1);
		}
		
		public function getStudentAddiInfoApprovalStatus($StudentIDArr)
		{
			global $eclass_db;
			
			$OEA_ADDITIONAL_INFO = $eclass_db.'.OEA_ADDITIONAL_INFO';
			$sql = "Select
							StudentID,
							RecordStatus
					From
							$OEA_ADDITIONAL_INFO
					Where
							StudentID In (".implode(',', (array)$StudentIDArr).")
					Group By
							StudentID
					";
			return BuildMultiKeyAssoc($this->returnArray($sql), array('StudentID'), $IncludedDBField=array('RecordStatus'), $SingleValue=1);
		}
		
		public function getStudentAbilityApprovalStatus($StudentIDArr)
		{
			global $eclass_db;
			
			$OEA_STUDENT_ABILITY = $eclass_db.'.OEA_STUDENT_ABILITY';
			$sql = "Select
							StudentID,
							RecordStatus
					From
							$OEA_STUDENT_ABILITY
					Where
							StudentID In (".implode(',', (array)$StudentIDArr).")
					Group By
							StudentID
					";
			return BuildMultiKeyAssoc($this->returnArray($sql), array('StudentID'), $IncludedDBField=array('RecordStatus'), $SingleValue=1);
		}
		
		public function getStudentAcademicApprovalStatus($StudentIDArr)
		{
			global $eclass_db;
			
			$OEA_STUDENT_ACADEMIC = $eclass_db.'.OEA_STUDENT_ACADEMIC';
			$sql = "Select
							StudentID,
							RecordStatus
					From
							$OEA_STUDENT_ACADEMIC
					Where
							StudentID In (".implode(',', (array)$StudentIDArr).")
					Group By
							StudentID
					";
			return BuildMultiKeyAssoc($this->returnArray($sql), array('StudentID'), $IncludedDBField=array('RecordStatus'), $SingleValue=1);
		}
		
		public function getStudentSuppInfoApprovalStatus($StudentIDArr)
		{
			global $eclass_db;
			
			$OEA_SUPPLEMENTARY_INFO = $eclass_db.'.OEA_SUPPLEMENTARY_INFO';
			$sql = "Select
							StudentID,
							RecordStatus
					From
							$OEA_SUPPLEMENTARY_INFO
					Where
							StudentID In (".implode(',', (array)$StudentIDArr).")
					Group By
							StudentID
					";
			return BuildMultiKeyAssoc($this->returnArray($sql), array('StudentID'), $IncludedDBField=array('RecordStatus'), $SingleValue=1);
		}
		
		public function getTeacherManagementIndexSql($YearID, $YearClassID, $TeachingClassOnly, $TargetApprovalStatus)
		{
			global $Lang, $linterface, $lpf;
			
			$libfcm = new form_class_manage();
			$PendingStatus = liboea_item::get_OEA_Pending_Status();
			$ApprovedStatus = liboea_item::get_OEA_Approved_Status();
			$RejectedStatus = liboea_item::get_OEA_Rejected_Status();
			
			$OEAItemInternalCode = $this->getOEAItemInternalCode();
			$AddiInfoInternalCode = $this->getAddiInfoInternalCode();
			$AbilityInternalCode = $this->getAbilityInternalCode();
			$AcademicInternalCode = $this->getAcademicInternalCode();
			$SuppInfoInternalCode = $this->getSuppInfoInternalCode();
			
			### Get all Students
			if ($YearClassID != '') {
				$YearClassIDArr = array($YearClassID);
			}
			else {
				if ($YearID == '') {
					// All Forms => Get All Applicable Forms of Jupas
					$YearID = $this->getJupasApplicableFormIDArr();
				}
				$YearClassInfoArr = $libfcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID(), $YearID, $TeachingClassOnly);
				$YearClassIDArr = Get_Array_By_Key($YearClassInfoArr, 'YearClassID');
			}
			$StudentInfoArr = $lpf->Get_Student_With_iPortfolio($YearClassIDArr, $IsSuspend='');
			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			$numOfStudent = count($StudentInfoArr);
			
			$TmpTableFieldArr = array();
			$TmpTableFieldArr[] = 'ClassName';
			$TmpTableFieldArr[] = 'ClassNumber';
			$TmpTableFieldArr[] = 'StudentNameLink';
			$TmpTableFieldArr[] = 'JupasApplicationNumber';			

			$StudentOEAItemCountAssoArr = array();
			if ($this->isEnabledOEAItem()) {
				//$TmpTableFieldArr[] = $OEAItemInternalCode;
				$TmpTableFieldArr[] = 'OEAApprovedCount';
				$TmpTableFieldArr[] = 'OEAPendingCount';
				$StudentOEAItemCountAssoArr = $this->getStudentOEAItemCount($StudentIDArr);
			}
			if ($this->isEnabledAdditionalInfo()) {
				$TmpTableFieldArr[] = $AddiInfoInternalCode;
				$StudentAddiInfoApprovalStatusAssoArr = $this->getStudentAddiInfoApprovalStatus($StudentIDArr);
			}
			if ($this->isEnabledAbility()) {
				$TmpTableFieldArr[] = $AbilityInternalCode;
				$StudentAbilityApprovalStatusAssoArr = $this->getStudentAbilityApprovalStatus($StudentIDArr);
			}
			if ($this->isEnabledAcademicPerformance()) {
				$TmpTableFieldArr[] = $AcademicInternalCode;
				$StudentAcademicApprovalStatusAssoArr = $this->getStudentAcademicApprovalStatus($StudentIDArr);
			}
			if ($this->isEnabledSupplementaryInfo()) {
				$TmpTableFieldArr[] = $SuppInfoInternalCode;
				$StudentSuppInfoApprovalStatusAssoArr = $this->getStudentSuppInfoApprovalStatus($StudentIDArr);
			}
			$TmpTableFieldArr[] = 'trCustClass';
			
			$InsertValueArr = array();
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$_insertSqlItemArr = array();
				
				$_studentID 				= $StudentInfoArr[$i]['UserID'];
				$_className 				= Get_Lang_Selection($StudentInfoArr[$i]['ClassTitleB5'], $StudentInfoArr[$i]['ClassTitleEN']);
				$_classNumber 				= $StudentInfoArr[$i]['ClassNumber'];
				$_studentName 				= $StudentInfoArr[$i]['StudentName'];
				$_isIPfActivated			= $StudentInfoArr[$i]['IsPortfolioActivated'];
				$_jupasApplicationNumber 	= $StudentInfoArr[$i]['JupasApplicationNumber'];
				
				### If Activated iPf => cannot view OEA by clicking the student name. Otherwise, only the student name is shown.
				if ($_isIPfActivated) {
					$_linkTitle = $Lang['iPortfolio']['OEA']['ViewAndApproveStudentJupasData'];
					$_studentNameLink = '<a class="tablelink" href="index.php?task=listStuAllInfo&clearCoo=1&StudentID='.$_studentID.'" title="'.$_linkTitle.'">'.$_studentName.'</a>'."\n";
				}
				else {
					$_studentNameLink = $_studentName."\n";
				}
				
				$_insertSqlItemArr[] = $this->pack_value($_className, 'str');
				$_insertSqlItemArr[] = $this->pack_value($_classNumber, 'int');
				$_insertSqlItemArr[] = $this->pack_value($_studentNameLink, 'str');
				$_insertSqlItemArr[] = ($_jupasApplicationNumber=='' || $_jupasApplicationNumber==0)? 'null' : $this->pack_value($_jupasApplicationNumber, 'int');
				
				$_displayFlagArr = array();
				if ($TargetApprovalStatus == '-1') {
					// no filtering => show all students
					$_displayFlagArr['Overall'] = true;
				}
				else {
					// have filtering => hide all student who has no iPf account
					if ($_isIPfActivated == false) {
						$_displayFlagArr['Overall'] = false;
					}
				}
				
				if ($this->isEnabledOEAItem()) 
				{
					$_oeaItemPendingCount = (isset($StudentOEAItemCountAssoArr[$_studentID][$PendingStatus]))? $StudentOEAItemCountAssoArr[$_studentID][$PendingStatus] : 0;
					
					// show student who has pending records only if the filter is selected
					if (isset($_displayFlagArr['Overall'])) {
						// follow the overall display flag if defined
					}
					else {
						// determine the display of this item by the filtering criteria
						if ($TargetApprovalStatus == $PendingStatus && $_oeaItemPendingCount > 0) {
							$_displayFlagArr[$OEAItemInternalCode] = true;
						}
						else {
							$_displayFlagArr[$OEAItemInternalCode] = false;
						}
					}
					
					if ($_isIPfActivated) {
						$_oeaItemApprovedCount = (isset($StudentOEAItemCountAssoArr[$_studentID][$ApprovedStatus]))? $StudentOEAItemCountAssoArr[$_studentID][$ApprovedStatus] : 0;
						$_oeaItemRejectedCount = (isset($StudentOEAItemCountAssoArr[$_studentID][$RejectedStatus]))? $StudentOEAItemCountAssoArr[$_studentID][$RejectedStatus] : 0;
						
						//$_oeaItemDisplay = $_oeaItemApprovedCount.' / '.($_oeaItemPendingCount + $_oeaItemApprovedCount + $_oeaItemRejectedCount);
						$_oeaItemDisplay1 = $_oeaItemApprovedCount;
						$_oeaItemDisplay2 = $_oeaItemPendingCount;
					}
					else {
						$_oeaItemDisplay1 = $Lang['General']['EmptySymbol'];
						$_oeaItemDisplay2 = $Lang['General']['EmptySymbol'];
					}
					$_insertSqlItemArr[] = $this->pack_value($_oeaItemDisplay1, 'str');
					$_insertSqlItemArr[] = $this->pack_value($_oeaItemDisplay2, 'str');
					 
				}
				if ($this->isEnabledAdditionalInfo()) {
					
					$_addiInfoApproveStatus = $StudentAddiInfoApprovalStatusAssoArr[$_studentID];
					
					// show student who has pending records only if the filter is selected
					if (isset($_displayFlagArr['Overall'])) {
						// follow the overall display flag if defined
					}
					else {
						// determine the display of this item by the filtering criteria
						if ($TargetApprovalStatus == $PendingStatus && $_addiInfoApproveStatus == $PendingStatus) {
							$_displayFlagArr[$AddiInfoInternalCode] = true;
						}
						else {
							$_displayFlagArr[$AddiInfoInternalCode] = false;
						}
					}
					
					if ($_isIPfActivated) {
						$_addiInfoStatusIcon = $this->getApprovalStatusImage($AddiInfoInternalCode, $_addiInfoApproveStatus);
					}
					else {
						$_addiInfoStatusIcon = $Lang['General']['EmptySymbol']; 
					}
					$_insertSqlItemArr[] = $this->pack_value($_addiInfoStatusIcon, 'str');
				}
				if ($this->isEnabledAbility()) {
					$_abilityApproveStatus = $StudentAbilityApprovalStatusAssoArr[$_studentID];
					
					// show student who has pending records only if the filter is selected
					if (isset($_displayFlagArr['Overall'])) {
						// follow the overall display flag if defined
					}
					else {
						// determine the display of this item by the filtering criteria
						if ($TargetApprovalStatus == $PendingStatus && $_abilityApproveStatus == $PendingStatus) {
							$_displayFlagArr[$AbilityInternalCode] = true;
						}
						else {
							$_displayFlagArr[$AbilityInternalCode] = false;
						}
					}
					
					if ($_isIPfActivated) {
						$_abilityStatusIcon = $this->getApprovalStatusImage($AbilityInternalCode, $_abilityApproveStatus);
					}
					else {
						$_abilityStatusIcon = $Lang['General']['EmptySymbol']; 
					}
					$_insertSqlItemArr[] = $this->pack_value($_abilityStatusIcon, 'str');
				}
				if ($this->isEnabledAcademicPerformance()) {
					$_academicApproveStatus = $StudentAcademicApprovalStatusAssoArr[$_studentID];
					
					// show student who has pending records only if the filter is selected
					if (isset($_displayFlagArr['Overall'])) {
						// follow the overall display flag if defined
					}
					else {
						// determine the display of this item by the filtering criteria
						if ($TargetApprovalStatus == $PendingStatus && $_academicApproveStatus == $PendingStatus) {
							$_displayFlagArr[$AcademicInternalCode] = true;
						}
						else {
							$_displayFlagArr[$AcademicInternalCode] = false;
						}
					}
					
					if ($_isIPfActivated) {
						$_academicStatusIcon = $this->getApprovalStatusImage($AcademicInternalCode, $_academicApproveStatus);
					}
					else {
						$_academicStatusIcon = $Lang['General']['EmptySymbol']; 
					}
					$_insertSqlItemArr[] = $this->pack_value($_academicStatusIcon, 'str');
				}
				if ($this->isEnabledSupplementaryInfo()) {
					$_suppInfoApproveStatus = $StudentSuppInfoApprovalStatusAssoArr[$_studentID];
					
					// show student who has pending records only if the filter is selected
					if (isset($_displayFlagArr['Overall'])) {
						// follow the overall display flag if defined
					}
					else {
						// determine the display of this item by the filtering criteria
						if ($TargetApprovalStatus == $PendingStatus && $_suppInfoApproveStatus == $PendingStatus) {
							$_displayFlagArr[$SuppInfoInternalCode] = true;
						}
						else {
							$_displayFlagArr[$SuppInfoInternalCode] = false;
						}
					}
					
					if ($_isIPfActivated) {
						$_suppInfoStatusIcon = $this->getApprovalStatusImage($SuppInfoInternalCode, $_suppInfoApproveStatus);
					}
					else {
						$_suppInfoStatusIcon = $Lang['General']['EmptySymbol']; 
					}
					$_insertSqlItemArr[] = $this->pack_value($_suppInfoStatusIcon, 'str');
				}
				
				$_trCustClass = ($_isIPfActivated==1)? 'row_on' : 'row_off';
				$_insertSqlItemArr[] = $this->pack_value($_trCustClass, 'str');
				
				// display the student if one of the JUPAS item fulfill the filtering criteria
				if (in_array(true, $_displayFlagArr)) {
					$InsertValueArr[] = '('.implode(',', (array)$_insertSqlItemArr).')';
				}
			}
			
			### Create Temp Table
			$sql = "Create Temporary Table If Not Exists TMP_OEA_TEACHER_MGMT_INDEX (
						RecordID Int(11) NOT NULL AUTO_INCREMENT,
						trCustClass varchar(128) Default Null,
						ClassName varchar(255) Default Null,
						ClassNumber int(8) Default Null,
						StudentNameLink text Default Null,
						JupasApplicationNumber int(10) Default Null,
						".$OEAItemInternalCode." text Default Null,
						OEAApprovedCount int(4) Default Null,
						OEAPendingCount int(4) Default Null,
						".$AddiInfoInternalCode." text Default Null,						
						".$AbilityInternalCode." text Default Null,
						".$AcademicInternalCode." text Default Null,
						".$SuppInfoInternalCode." text Default Null,
						PRIMARY KEY (RecordID)
					) ENGINE=InnoDB Charset=utf8";
			$SuccessArr['CreateTempTable'] = $this->db_db_query($sql);
			
			### Insert Temp Records
			if (count($InsertValueArr) > 0)
			{
				$sql = "Insert Into TMP_OEA_TEACHER_MGMT_INDEX
							(".implode(',', (array)$TmpTableFieldArr).")
						Values
							".implode(',', (array)$InsertValueArr)."
						";
				$SuccessArr['InsertTempData'] = $this->db_db_query($sql);
			}
			
			
			$SelectFieldArr = array();
			$numOfField = count((array)$TmpTableFieldArr);
			for ($i=0; $i<$numOfField; $i++) {
				$thisField = $TmpTableFieldArr[$i];
				
				if ($thisField == 'JupasApplicationNumber') {
					$SelectFieldArr[] = "If (JupasApplicationNumber Is Null, '".$Lang['General']['EmptySymbol']."', JupasApplicationNumber)";
				}
				else {
					$SelectFieldArr[] = $thisField;
				}
			}
			$DBTable_SQL = "Select 
									".implode(',', (array)$SelectFieldArr)."
							From 
									TMP_OEA_TEACHER_MGMT_INDEX	
							";
			$ReturnArr = array();
			$ReturnArr['DBTableSql'] = $DBTable_SQL;
			$ReturnArr['DBTableFieldArr'] = $TmpTableFieldArr;
//					debug_r($ReturnArr);
			return $ReturnArr;
		}
		
		public function getItemTypePendingStatus($InternalTypeCode)
		{
			$Status = '';
			
			switch ($InternalTypeCode)
			{
				case $this->getOEAItemInternalCode():
					$Status = liboea_item::get_OEA_Pending_Status();
					break;
				case $this->getAddiInfoInternalCode():
					$Status = liboea_additional_info::get_pending_status();
					break;
				case $this->getAbilityInternalCode():
					$Status = liboea_ability::get_pending_status();
					break;
				case $this->getAcademicInternalCode():
					$Status = liboea_academic::get_pending_status();
					break;
				case $this->getSuppInfoInternalCode():
					$Status = liboea_supplementary_info::get_pending_status();
					break;
			}
			
			return $Status;
		}
		
		public function getItemTypeApprovedStatus($InternalTypeCode)
		{
			$Status = '';
			
			switch ($InternalTypeCode)
			{
				case $this->getOEAItemInternalCode():
					$Status = liboea_item::get_OEA_Approved_Status();
					break;
				case $this->getAddiInfoInternalCode():
					$Status = liboea_additional_info::get_approved_status();
					break;
				case $this->getAbilityInternalCode():
					$Status = liboea_ability::get_approved_status();
					break;
				case $this->getAcademicInternalCode():
					$Status = liboea_academic::get_approved_status();
					break;
				case $this->getSuppInfoInternalCode():
					$Status = liboea_supplementary_info::get_approved_status();
					break;
			}
			
			return $Status;
		}
		
		public function getItemTypeRejectedStatus($InternalTypeCode)
		{
			$Status = '';
			
			switch ($InternalTypeCode)
			{
				case $this->getOEAItemInternalCode():
					$Status = liboea_item::get_OEA_Rejected_Status();
					break;
				case $this->getAddiInfoInternalCode():
					$Status = liboea_additional_info::get_rejected_status();
					break;
				case $this->getAbilityInternalCode():
					$Status = liboea_ability::get_rejected_status();
					break;
				case $this->getAcademicInternalCode():
					$Status = liboea_academic::get_rejected_status();
					break;
				case $this->getSuppInfoInternalCode():
					$Status = liboea_supplementary_info::get_rejected_status();
					break;
			}
			
			return $Status;
		}
		
		public function getApprovalStatusDisplay($InternalTypeCode, $Status, $ApprovedBy, $ApprovalDate)
		{
			global $Lang, $linterface;
			
			$x = '';
			
			if ($Status == null || $Status === '') {
				$x .= $Lang['General']['EmptySymbol'];
			}
			else if ($Status == $this->getItemTypePendingStatus($InternalTypeCode)) {
				$x .= '<span style="color:blue;">'.$linterface->Get_Pending_Image().$Lang['iPortfolio']['OEA']['RecordStatus_Pending'].'</span>';
			}
			else if ($Status == $this->getItemTypeApprovedStatus($InternalTypeCode)) {
				$x .= '<span style="color:green;">'.$linterface->Get_Approved_Image().$Lang['iPortfolio']['OEA']['RecordStatus_Approved'].'</span>';
			}
			else if ($Status == $this->getItemTypeRejectedStatus($InternalTypeCode)) {
				$x .= '<span style="color:red;font-weight:bold;">'.$linterface->Get_Rejected_Image().$Lang['iPortfolio']['OEA']['RecordStatus_Rejected'].'</span>';
			}
			
			if ($ApprovalDate == '' || $ApprovedBy == '') {
				// do nth
			}
			else {
				if (is_numeric($ApprovedBy))
				{
					$byUserName = '';
					$ApprovedBy = $ApprovedBy;
				}
				else
				{
					$byUserName = $ApprovedBy;
					$ApprovedBy = '';
				}
				
				$x .= '<span class="tabletextremark">'."\n";
					$x .= '('.Get_Last_Modified_Remark($ApprovalDate, $byUserName, $ApprovedBy, $Lang['iPortfolio']['OEA']['LastStatusUpdateRemark']).')';
				$x .= '</span>'."\n";
			}	
			
			
			
			
			return $x;
		}
		
		public function getApprovalStatusImage($InternalTypeCode, $Status)
		{
			global $linterface, $Lang;
			
			$x = '';
			
			if ($Status == null || $Status === '') {
				$x .= $Lang['General']['EmptySymbol'];
			}
			else if ($Status == $this->getItemTypePendingStatus($InternalTypeCode)) {
				$x .= $linterface->Get_Pending_Image();
			}
			else if ($Status == $this->getItemTypeApprovedStatus($InternalTypeCode)) {
				$x .= $linterface->Get_Approved_Image();
			}
			else if ($Status == $this->getItemTypeRejectedStatus($InternalTypeCode)) {
				$x .= $linterface->Get_Rejected_Image();
			}
			
			return $x;
		}
		
		public function getStudentOeaItemInfoArr($StudentIDArr='', $RecordStatusArr='', $ParticipationArr='', $ParRecordIDArr='', $ParExcludeRecordIDArr='', $OLE_ProgramIDArr='', $withCodeInOeaTitle=true)
		{
			global $Lang, $eclass_db;
			
			$conds_StudentID = '';
			if ($StudentIDArr != '') {
				$conds_StudentID = " And StudentID In (".implode(',', (array)$StudentIDArr).") ";
			}
			
			$conds_RecordStatus = '';
			if ($RecordStatusArr != '') {
				$conds_RecordStatus = " And RecordStatus In (".implode(',', (array)$RecordStatusArr).") ";
			}
			
			$conds_Participation = '';
			if ($ParticipationArr != '') {
				$conds_Participation = " And Participation In ('".implode("','", (array)$ParticipationArr)."') ";
			}
			
			$conds_RecordID='';
			if ($ParRecordIDArr != '') {
				$conds_RecordID = " And RecordID In ('".implode("','", (array)$ParRecordIDArr)."') ";
			}
			
			$conds_ExcludeRecordID='';
			if ($ParExcludeRecordIDArr != '') {
				$conds_ExcludeRecordID = " And RecordID Not In ('".implode("','", (array)$ParExcludeRecordIDArr)."') ";
			}
			
			$conds_OLE_ProgramIDArr = '';
			if ($OLE_ProgramIDArr != '') {
				$conds_OLE_ProgramIDArr = " And OLE_PROGRAM_ProgramID In ('".implode("','", (array)$OLE_ProgramIDArr)."') ";
			}
			
			if ($withCodeInOeaTitle) {
				$oeaTitleField = " concat(if(Title is null,'',Title),' (',if(OEA_ProgramCode is null,'',OEA_ProgramCode),')') as 'Title' ";
			}
			else {
				$oeaTitleField = " if(Title is null,'',Title) as 'Title' ";
			}
			
			$OEA_STUDENT = $eclass_db.'.OEA_STUDENT';
			$sql = "Select 
							RecordID,
							StudentID,
							$oeaTitleField,
							OEA_ProgramCode,
							StartYear,
							EndYear,
							Participation,
							Role,
							ParticipationNature,
							Achievement,
							Description,
							RecordStatus,
							If (OEA_AwardBearing Is Null Or OEA_AwardBearing = '', 'N', OEA_AwardBearing) as OEA_AwardBearing,
							OLE_STUDENT_RecordID,
							OLE_PROGRAM_ProgramID
					From
							$OEA_STUDENT
					Where
							1
							$conds_StudentID
							$conds_RecordStatus
							$conds_Participation
							$conds_RecordID
							$conds_ExcludeRecordID
							$conds_OLE_ProgramIDArr
					Order By
							StartYear Asc, Title Asc
					";
			return $this->returnArray($sql);
		}
		
		public function getStudentAddiInfoArr($StudentIDArr, $RecordStatusArr)
		{
			global $Lang, $eclass_db;
			
			$conds_RecordStatus = '';
			if ($RecordStatusArr != '') {
				$conds_RecordStatus = " And RecordStatus In (".implode(',', (array)$RecordStatusArr).") ";
			}
			
			$OEA_ADDITIONAL_INFO = $eclass_db.'.OEA_ADDITIONAL_INFO';
			$sql = "Select 
							AdditionalInfoID,
							StudentID,
							Title,
							Details
					From
							$OEA_ADDITIONAL_INFO
					Where
							StudentID In (".implode(',', (array)$StudentIDArr).")
							$conds_RecordStatus
					";
			return $this->returnArray($sql);
		}
		
		public function getStudentAbilityArr($StudentIDArr, $RecordStatusArr)
		{
			global $Lang, $eclass_db;
			
			$conds_RecordStatus = '';
			if ($RecordStatusArr != '') {
				$conds_RecordStatus = " And RecordStatus In (".implode(',', (array)$RecordStatusArr).") ";
			}
			
			$OEA_STUDENT_ABILITY = $eclass_db.'.OEA_STUDENT_ABILITY';
			$sql = "Select 
							StudentAbilityID,
							StudentID,
							AbilityValue
					From
							$OEA_STUDENT_ABILITY
					Where
							StudentID In (".implode(',', (array)$StudentIDArr).")
							$conds_RecordStatus
					";
			return $this->returnArray($sql);
		}
		
		public function getStudentAcademicArr($StudentIDArr, $RecordStatusArr)
		{
			global $Lang, $eclass_db;
			
			$conds_RecordStatus = '';
			if ($RecordStatusArr != '') {
				$conds_RecordStatus = " And RecordStatus In (".implode(',', (array)$RecordStatusArr).") ";
			}
			
			$OEA_STUDENT_ACADEMIC = $eclass_db.'.OEA_STUDENT_ACADEMIC';
			$sql = "Select 
							StudentAcademicID,
							StudentID
					From
							$OEA_STUDENT_ACADEMIC
					Where
							StudentID In (".implode(',', (array)$StudentIDArr).")
							$conds_RecordStatus
					";
			return $this->returnArray($sql);
		}
		
		public function getStudentSuppInfoArr($StudentIDArr, $RecordStatusArr)
		{
			global $Lang, $eclass_db;
			
			$conds_RecordStatus = '';
			if ($RecordStatusArr != '') {
				$conds_RecordStatus = " And RecordStatus In (".implode(',', (array)$RecordStatusArr).") ";
			}
			
			$OEA_SUPPLEMENTARY_INFO = $eclass_db.'.OEA_SUPPLEMENTARY_INFO';
			$sql = "Select 
							SupplementaryInfoID,
							StudentID,
							Title,
							Details
					From
							$OEA_SUPPLEMENTARY_INFO
					Where
							StudentID In (".implode(',', (array)$StudentIDArr).")
							$conds_RecordStatus
					";
			return $this->returnArray($sql);
		}
		
		public function getTeacher_listStuAllInfo_addiInfoDiv($ObjAddiInfo)
		{
			global $Lang, $iPort, $linterface, $oea_cfg;
			
			$ObjTypeCode = $this->getAddiInfoInternalCode();
			
			$ObjIdFieldName = $ObjAddiInfo->getObjectIdFieldName();
			$ObjID = $ObjAddiInfo->getObjectID();
			$ObjRecordStatus = $ObjAddiInfo->getRecordStatus();
			$ObjApprovedBy = $ObjAddiInfo->getApprovedBy();
			$ObjApprovalDate = $ObjAddiInfo->getApprovalDate();
			$ApprovedStatus = $ObjAddiInfo->get_approved_status();
			$RejectedStatus = $ObjAddiInfo->get_rejected_status();
			
			$h_ContentTable = '';
			$h_ApprovalBtnDivID = 'ApprovalBtnDiv_'.$ObjTypeCode;
			$h_ContentTableDivID = 'ContentTableDiv_'.$ObjTypeCode;
			
			$showApprove = false;
			$showReject = false;
			if ($ObjRecordStatus == $ApprovedStatus) {
				$showReject = true;
			}
			else if ($ObjRecordStatus == $RejectedStatus) {
				$showApprove = true;
			}
			else {
				// pending or not input additional info yet
				$showApprove = true;
				$showReject = true;
			}
			
			if ($showApprove==false) {
				$approveBtnStyleTag = ' style="display:none;" ';
			}
			if ($showReject==false) {
				$rejectBtnStyleTag = ' style="display:none;" ';
			}
			
			$BtnArr = array();
			$BtnArr[] = array('edit', "javascript:js_Reload_Content_Table('".$ObjTypeCode."', '".$ObjIdFieldName."', 'Edit');");
			$BtnArr[] = array('approve', "javascript:js_Change_Approval_Status('".$ObjTypeCode."', '".$ObjIdFieldName."', '".$ApprovedStatus."');", '', $h_ApprovalBtnDivID, $approveBtnStyleTag);
			$BtnArr[] = array('reject', "javascript:js_Change_Approval_Status('".$ObjTypeCode."', '".$ObjIdFieldName."', '".$RejectedStatus."');", '', $h_ApprovalBtnDivID, $rejectBtnStyleTag);

			
			$h_ContentTable .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
				$h_ContentTable .= '<tr>'."\n";
					$h_ContentTable .= '<td colspan="2" class="stu_info_log" align="center">'."\n";
						$h_ContentTable .= '<div id="'.$h_ContentTableDivID.'">'.$ObjAddiInfo->getViewTable().'</div>'."\n";
						$h_ContentTable .= '<br style="clear:both;" />'."\n";
						$h_ContentTable .= $this->getTeacher_listStuAllInfo_editModeBottomButtonDiv($ObjTypeCode, $ObjIdFieldName);
					$h_ContentTable .= '</td>'."\n";
				$h_ContentTable .= '</tr>'."\n";
			$h_ContentTable .= '</table>'."\n";
			
			//$WarningMsg = $Lang['iPortfolio']['OEA']['ModuleTitleArr'][$oea_cfg["JupasItemInternalCode"]["AddiInfo"]].' '.$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'];
			//$h_RemarksMsgTable = $linterface->Get_Warning_Message_Box('<span style="color:red">'.$Lang['General']['Remark'].'</span>', $Lang['iPortfolio']['JUPAS']['RemarksOEA']);
			
			return $this->generateTeacher_listStuAllInfo_sectionInfoDiv($ObjTypeCode, $Lang['iPortfolio']['OEA']['AdditionalInformation'], $BtnArr, $ObjRecordStatus, $ObjApprovedBy, $ObjApprovalDate, $h_ContentTable, $h_RemarksMsgTable);
		}
		
		public function getTeacher_listStuAllInfo_suppInfoDiv($ObjSuppInfo)
		{
			global $Lang, $iPort, $linterface;
			
			$ObjTypeCode = $this->getSuppInfoInternalCode();
			
			$ObjIdFieldName = $ObjSuppInfo->getObjectIdFieldName();
			$ObjID = $ObjSuppInfo->getObjectID();
			$ObjRecordStatus = $ObjSuppInfo->getRecordStatus();
			$ObjApprovedBy = $ObjSuppInfo->getApprovedBy();
			$ObjApprovalDate = $ObjSuppInfo->getApprovalDate();
			$ApprovedStatus = $ObjSuppInfo->get_approved_status();
			
			$h_ContentTable = '';
			$h_ApprovalBtnDivID = 'ApprovalBtnDiv_'.$ObjTypeCode;
			$h_ContentTableDivID = 'ContentTableDiv_'.$ObjTypeCode;
			
			$BtnArr = array();
//			if ($ObjRecordStatus == $ApprovedStatus) {
//				$BtnArr[] = array('reject', "javascript:js_Change_Approval_Status('".$ObjTypeCode."', '".$ObjIdFieldName."');", '', $h_ApprovalBtnDivID);
//			}
//			else {
//				$BtnArr[] = array('approve', "javascript:js_Change_Approval_Status('".$ObjTypeCode."', '".$ObjIdFieldName."');", '', $h_ApprovalBtnDivID);
//			}
			$BtnArr[] = array('edit', "javascript:js_Reload_Content_Table('".$ObjTypeCode."', '".$ObjIdFieldName."', 'Edit');");
			
			$h_ContentTable .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
				$h_ContentTable .= '<tr>'."\n";
					$h_ContentTable .= '<td colspan="2" class="stu_info_log" align="center">'."\n";
						$h_ContentTable .= '<div id="'.$h_ContentTableDivID.'">'.$ObjSuppInfo->getViewTable().'</div>'."\n";
						$h_ContentTable .= '<br style="clear:both;" />'."\n";
						$h_ContentTable .= $this->getTeacher_listStuAllInfo_editModeBottomButtonDiv($ObjTypeCode, $ObjIdFieldName);
					$h_ContentTable .= '</td>'."\n";
				$h_ContentTable .= '</tr>'."\n";
			$h_ContentTable .= '</table>'."\n";
				
			return $this->generateTeacher_listStuAllInfo_sectionInfoDiv($ObjTypeCode, $Lang['iPortfolio']['OEA']['SupplementaryInformation'], $BtnArr, $ObjRecordStatus, $ObjApprovedBy, $ObjApprovalDate, $h_ContentTable, $h_RemarksMsgTable);
		}
		
		public function getTeacher_listStuAllInfo_oeaItemDiv($StudentID)
		{
			global $Lang, $iPort, $linterface, $oea_cfg;
			$ObjTypeCode = $this->getOEAItemInternalCode();
			
			$h_ContentDiv = '';
			$h_ContentTableDivID = 'ContentTableDiv_'.$ObjTypeCode;
			
			//$WarningMsg = $Lang['iPortfolio']['OEA']['ModuleTitleArr'][$oea_cfg["JupasItemInternalCode"]["OeaItem"]].' '.$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'];
			
			//$h_RemarksMsgTable = $linterface->Get_Warning_Message_Box('<span style="color:red">'.$Lang['General']['Remark'].'</span>', $Lang['iPortfolio']['OEA']['OnlyExportApprovedOEARemarks'].'<br/><br/>'.$Lang['iPortfolio']['JUPAS']['RemarksOEA']);
						
			$BtnArr = array();
					
			$h_ContentDiv .= '<div id="'.$h_ContentTableDivID.'">'.$this->getTeacher_listStuAllInfo_oeaItemTable($StudentID).'</div>'."\n";
			
			return $this->generateTeacher_listStuAllInfo_sectionInfoDiv($ObjTypeCode, $Lang['iPortfolio']['OEA']['OEA_Name'], $BtnArr, '', '', '', $h_ContentDiv, $h_RemarksMsgTable);
		}
		
		public function getTeacher_listStuAllInfo_oeaItemTable($StudentID)
		{
			global $Lang, $linterface, $button_edit, $oea_cfg;
			
			$liboea_item = new liboea_item();
			
			$x = '';
			$x .= $this->Include_JS_CSS();
			// Attended as School Activity
			$ParticipantType = 'S';
			$OeaItemInfoArr = $this->getStudentOeaItemInfoArr($StudentID, '', $ParticipantType);
			
			
			$x .= '<div style="float:left;">'."\n";
				$x .= '<em class="form_sep_title">- '.$liboea_item->Get_OEA_ParticipationName_By_ParticipationCode($ParticipantType).' -</em>'."\n";
			$x .= '</div>'."\n";
			$x .= '<br style="clear:both;" />'."\n";
			$x .= $this->getTeacher_listStuAllInfo_oeaItemTable_HTML($OeaItemInfoArr, $ParticipantType,$StudentID);
			$x .= '<br style="clear:both;" />'."\n";
			$x .= '<br style="clear:both;" />'."\n";
			
			
			// Attended Privately
			$ParticipantType = 'P';
			$OeaItemInfoArr = $this->getStudentOeaItemInfoArr($StudentID, '', $ParticipantType);
			$x .= '<div style="float:left;">'."\n";
				$x .= '<em class="form_sep_title">- '.$liboea_item->Get_OEA_ParticipationName_By_ParticipationCode($ParticipantType).' -</em>'."\n";
			$x .= '</div>'."\n";
			$x .= '<br style="clear:both;" />'."\n";			
			$x .= $this->getTeacher_listStuAllInfo_oeaItemTable_HTML($OeaItemInfoArr, $ParticipantType,$StudentID);
			
			return $x;
		}

		//customization for Lasalle
		public function getTeacher_listStuAllInfo_oeaPreMapStudentItemDiv($StudentID){
			global $Lang, $iPort, $linterface;

			$ObjTypeCode = $this->getAcademicInternalCode();
			$h_ContentTableDivID = 'ContentTableDiv_'.$ObjTypeCode;

			$h_ContentDiv .= '<div id="'.$h_ContentTableDivID.'">'.$this->getTeacher_listStuAllInfo_oeaPreMapStudentItemTable($StudentID).'</div>'."\n";
			return $this->generateTeacher_listStuAllInfo_sectionInfoDiv($ObjTypeCode, $Lang['iPortfolio']['OEA']['OEA_Name_PreMap_Student'], $BtnArr, '', '', '', $h_ContentDiv, $h_RemarksMsgTable);
		}

		//customization for Lasalle
		public function getTeacher_listStuAllInfo_oeaPreMapStudentItemTable($StudentID)
		{
			global $Lang, $linterface, $button_edit, $oea_cfg;
			
			$liboea_item = new liboea_item();
			
			$x = '';
			$x .= $this->Include_JS_CSS();
			// Attended as School Activity
			$ParticipantType = 'S';
			$OeaItemInfoArr = $this->getOeaPreMapStudentItemInfoArr($StudentID, '', $ParticipantType);

			$x .= '<div style="float:left;">'."\n";
				$x .= '<em class="form_sep_title">- '.$liboea_item->Get_OEA_ParticipationName_By_ParticipationCode($ParticipantType).' -</em>'."\n";
			$x .= '</div>'."\n";
			$x .= '<br style="clear:both;" />'."\n";
			$x .= $this->getTeacher_listStuAllInfo_oeaPreMapStudentItemTable_HTML($OeaItemInfoArr, $ParticipantType,$StudentID);
			$x .= '<br style="clear:both;" />'."\n";
			$x .= '<br style="clear:both;" />'."\n";
			
			
			// Attended Privately
			$ParticipantType = 'P';
			$OeaItemInfoArr = $this->getOeaPreMapStudentItemInfoArr($StudentID, '', $ParticipantType);
			$x .= '<div style="float:left;">'."\n";
				$x .= '<em class="form_sep_title">- '.$liboea_item->Get_OEA_ParticipationName_By_ParticipationCode($ParticipantType).' -</em>'."\n";
			$x .= '</div>'."\n";
			$x .= '<br style="clear:both;" />'."\n";			
			$x .= $this->getTeacher_listStuAllInfo_oeaPreMapStudentItemTable_HTML($OeaItemInfoArr, $ParticipantType,$StudentID);

			return $x;
		}



		private function getTeacher_listStuAllInfo_oeaPreMapStudentItemTable_HTML($OeaItemInfoArr, $ParticipantType,$StudentID='') {
			global $Lang, $button_edit;
			global $linterface;
			global $image_path, $LAYOUT_SKIN, $button_view;

				
			$liboea_setting = new liboea_setting();
				
			$liboea_item = new liboea_item();
			
			$ObjTypeCode = $this->getOEAItemInternalCode();
			$numOfOeaItem = count((array)$OeaItemInfoArr);
			$CheckBoxName = 'OEA_RecordIDArr['.$ParticipantType.'][]';


			$BtnArr = array();
			/* hidden this first
			$BtnArr[] = array('edit', "javascript:js_Edit_Oea_Item('$ParticipantType');");
			$BtnArr[] = array('approve', "javascript:js_Approve_Reject_Oea_Item('Approve', '$ObjTypeCode', '$ParticipantType');");
			$BtnArr[] = array('reject', "javascript:js_Approve_Reject_Oea_Item('Reject', '$ObjTypeCode', '$ParticipantType');");
			*/			
			$BtnArr[] = array('delete', "javascript:js_delete_item('$ParticipantType');");


			$x = '';
			$x .= $linterface->Get_DBTable_Action_Button_IP25($BtnArr)."\n";
			$x .= '<table class="common_table_list_v30">'."\n";
				# col group
				$x .= '<col align="left" style="width: 25%;">'."\n";
				$x .= '<col align="left" style="width: 15%;">'."\n";
				$x .= '<col align="left" style="width: 8%;">'."\n";
				$x .= '<col align="left" style="width: 8%;">'."\n";
				$x .= '<col align="left" style="width: 15%;">'."\n";
				$x .= '<col align="left" style="width: 24%;">'."\n";
				$x .= '<col align="left" style="width: 10%;">'."\n";
//				$x .= '<col align="left" style="width: 5%;">'."\n";
				$x .= '<col align="left" style="width: 1px">'."\n";
				
				# table head
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Title'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Category'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['StartYear'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['EndYear'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Role'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Achievement'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Description'].'</th>'."\n";
//						$x .= '<th>'.$Lang['iPortfolio']['OEA']['ApprovalStatus'].'</th>'."\n";
						$x .= '<th>'.$linterface->Get_Checkbox($ID='', $Name='checkmaster', $Value='', $isChecked=0, $Class='', $Display='', $Onclick="(this.checked)?setChecked(1,this.form,'".$CheckBoxName."'):setChecked(0,this.form,'".$CheckBoxName."');").'</th>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";	

				# table body
				$x .= '<tbody>'."\n";
				
					if ($numOfOeaItem == 0)
					{
						$x .= '<tr><td colspan="9" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
					}
					else
					{
						for ($i=0; $i<$numOfOeaItem; $i++)
						{
							$_RecordID 				= $OeaItemInfoArr[$i]['RecordID'];
							$_OEAProgramCode		= $OeaItemInfoArr[$i]['OEA_ProgramCode'];
							$_Title 				= $OeaItemInfoArr[$i]['Title'];
							$_StartYear 			= $OeaItemInfoArr[$i]['StartYear'];
							$_EndYear 				= $OeaItemInfoArr[$i]['EndYear'];
							$_Participation 		= $OeaItemInfoArr[$i]['Participation'];
							$_Role 					= $OeaItemInfoArr[$i]['Role'];
							$_ParticipationNature 	= $OeaItemInfoArr[$i]['ParticipationNature'];
							$_Achievement 			= $OeaItemInfoArr[$i]['Achievement'];
//							$_RecordStatus 			= $OeaItemInfoArr[$i]['RecordStatus'];
							$_OLE_STUDENT_RecordID 	= $OeaItemInfoArr[$i]['OLE_STUDENT_RecordID'];
							$_OLE_PROGRAM_ProgramID = $OeaItemInfoArr[$i]['OLE_PROGRAM_ProgramID'];

							$_OLE_Description = $OeaItemInfoArr[$i]['Description'];
					
							# Get Title (with hyperlink
							$_TitlePrefix = ($_OLE_STUDENT_RecordID > 0 && $_OLE_PROGRAM_ProgramID > 0)? $linterface->RequiredSymbol() : '';
//							$_Title = "<a id='item_".$_RecordID."' href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onclick='js_edit_action(".$_RecordID."); return false;' class='setting_row thickbox' title='".$button_edit."'>".$_TitlePrefix.$_Title."</a>";

							$_Title = $_TitlePrefix.$_Title;

							# Get Category Name
							$_CategoryInfoArr = $liboea_setting->getOEACategoryInfoByOEAItemCode($_OEAProgramCode);
							$_CategoryName = $_CategoryInfoArr['CatName'];
							
							# Get Role Name
							$_RoleName = $liboea_item->Get_OEA_RoleName_By_RoleCode($_Role);
							
							# Get Achievement and Participant Nature Name
							$_AchievementName = $liboea_item->Get_OEA_AchievementName_By_AchievementCode($_Achievement);
							$_ParticipationNatureName = $liboea_item->Get_OEA_ParticipationNatureName_By_ParticipationNatureCode($_ParticipationNature);
							$_AchievementDisplay = '';
							if ($_ParticipationNatureName != '') {
								$_AchievementDisplay .= $_ParticipationNatureName.' - ';
							}
							$_AchievementDisplay .= $_AchievementName;
							
							# Get OEA Nature Name
							//$_NatureDisplay = $liboea_item->Get_OEA_ParticipationName_By_ParticipationCode($_Participation);
													
							# Get Approval Status Icon
							$_ApprovalStatusDisplay = $this->getApprovalStatusImage($this->getOEAItemInternalCode(), $_RecordStatus);
							
							# Get Checkbox
							$_class = 'OEA_RecordIDChk_'.$ParticipantType;
							$_Checkbox = $linterface->Get_Checkbox($ID='', $CheckBoxName, $Value=$_RecordID, $isChecked=0, $_class, $Display='', $Onclick='unset_checkall(this, this.form);');
							
							# Description
							$descIconDivId = 'descIconDivID'."_$ParticipantType"."_$i";						
							$thisDescRecordID =$_RecordID;	
							
										
							$MouseEvent = "onmouseover=\"showShortCutPanel('$descIconDivId','$thisDescRecordID','$ParticipantType','$StudentID');\" onmouseout=\"hideShortCutPanel();\" ";
		
							$DescIcon = "<div id='".$descIconDivId."' style=\"width:100%;\" >".
									'<img '. $MouseEvent .'src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_remark.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$Lang['iPortfolio']['OEA']['Description'].'">'.
								"</div>";	
							
							if($_OLE_Description=='')	
							{
								$DescIcon = $Lang['General']['EmptySymbol'];
							}					
													
							$x .= '<tr>'."\n";
								$x .= '<td>'.$_Title.'</td>'."\n";
								$x .= '<td>'.$_CategoryName.'</td>'."\n";
								$x .= '<td>'.$_StartYear.'</td>'."\n";
								$x .= '<td>'.$_EndYear.'</td>'."\n";
								$x .= '<td>'.$_RoleName.'</td>'."\n";
								$x .= '<td>'.$_AchievementDisplay.'</td>'."\n";
								$x .= '<td>'.$DescIcon.'</td>'."\n";
//								$x .= '<td>'.$_ApprovalStatusDisplay.'</td>'."\n";
								$x .= '<td>'.$_Checkbox.'</td>'."\n";
							$x .= '</tr>'."\n";
						}
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			$x .= '<span class="tabletextremark" style="float:left;">'.$linterface->RequiredSymbol().$Lang['iPortfolio']['OEA']['RepresentMappedFromOLE'].'</span>'."\n";

			return $x;
		}

//test
		//customization for Lasalle
		private function getOeaPreMapStudentItemInfoArr($StudentIDArr='', $RecordStatusArr='', $ParticipationArr='', $ParRecordIDArr='', $ParExcludeRecordIDArr='', $OLE_ProgramIDArr='', $withCodeInOeaTitle=true)
		{
			global $Lang, $eclass_db;

			$conds_StudentID = '';
			if ($StudentIDArr != '') {
				$conds_StudentID = " And StudentID In (".implode(',', (array)$StudentIDArr).") ";
			}
			
			$conds_RecordStatus = '';
			if ($RecordStatusArr != '') {
				$conds_RecordStatus = " And RecordStatus In (".implode(',', (array)$RecordStatusArr).") ";
			}
			
			$conds_Participation = '';
			if ($ParticipationArr != '') {
				$conds_Participation = " And Participation In ('".implode("','", (array)$ParticipationArr)."') ";
			}
			
			$conds_RecordID='';
			if ($ParRecordIDArr != '') {
				$conds_RecordID = " And RecordID In ('".implode("','", (array)$ParRecordIDArr)."') ";
			}
			
			$conds_ExcludeRecordID='';
			if ($ParExcludeRecordIDArr != '') {
				$conds_ExcludeRecordID = " And RecordID Not In ('".implode("','", (array)$ParExcludeRecordIDArr)."') ";
			}
			
			$conds_OLE_ProgramIDArr = '';
			if ($OLE_ProgramIDArr != '') {
				$conds_OLE_ProgramIDArr = " And OLE_PROGRAM_ProgramID In ('".implode("','", (array)$OLE_ProgramIDArr)."') ";
			}
			
			if ($withCodeInOeaTitle) {
				$oeaTitleField = " concat(if(Title is null,'',Title),' (',if(OEA_ProgramCode is null,'',OEA_ProgramCode),')') as 'Title' ";
			}
			else {
				$oeaTitleField = " if(Title is null,'',Title) as 'Title' ";
			}
			
			$OEA_STUDENT_PRE_SELECT = $eclass_db.'.OEA_STUDENT_PRE_SELECT';
			$sql = "Select 
							RecordID,
							StudentID,
							$oeaTitleField,
							OEA_ProgramCode,
							StartYear,
							EndYear,
							Participation,
							Role,
							ParticipationNature,
							Achievement,
							Description,
							RecordStatus,
							If (OEA_AwardBearing Is Null Or OEA_AwardBearing = '', 'N', OEA_AwardBearing) as OEA_AwardBearing,
							OLE_STUDENT_RecordID,
							OLE_PROGRAM_ProgramID
					From
							{$OEA_STUDENT_PRE_SELECT}
					Where
							1
							{$conds_StudentID}
							{$conds_Participation}
					Order By
							StartYear Asc, Title Asc
					";

			return $this->returnArray($sql);
		}

		private function getTeacher_listStuAllInfo_oeaItemTable_HTML($OeaItemInfoArr, $ParticipantType,$StudentID='') {
			global $Lang, $button_edit;
			global $linterface;
			global $image_path, $LAYOUT_SKIN, $button_view;
			
				
			$liboea_setting = new liboea_setting();
				
			$liboea_item = new liboea_item();
			
			$ObjTypeCode = $this->getOEAItemInternalCode();
			$numOfOeaItem = count((array)$OeaItemInfoArr);
			$CheckBoxName = 'OEA_RecordIDArr['.$ParticipantType.'][]';
			
			$BtnArr = array();
			$BtnArr[] = array('edit', "javascript:js_Edit_Oea_Item('$ParticipantType');");
			$BtnArr[] = array('approve', "javascript:js_Approve_Reject_Oea_Item('Approve', '$ObjTypeCode', '$ParticipantType');");
			$BtnArr[] = array('reject', "javascript:js_Approve_Reject_Oea_Item('Reject', '$ObjTypeCode', '$ParticipantType');");
			
			
			$x = '';
			$x .= $linterface->Get_DBTable_Action_Button_IP25($BtnArr)."\n";
			$x .= '<table class="common_table_list_v30">'."\n";
				# col group
				$x .= '<col align="left" style="width: 25%;">'."\n";
				$x .= '<col align="left" style="width: 15%;">'."\n";
				$x .= '<col align="left" style="width: 8%;">'."\n";
				$x .= '<col align="left" style="width: 8%;">'."\n";
				$x .= '<col align="left" style="width: 15%;">'."\n";
				$x .= '<col align="left" style="width: 24%;">'."\n";
				$x .= '<col align="left" style="width: 10%;">'."\n";
				$x .= '<col align="left" style="width: 5%;">'."\n";
				$x .= '<col align="left" style="width: 1px">'."\n";
				
				# table head
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Title'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Category'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['StartYear'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['EndYear'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Role'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Achievement'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['Description'].'</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['ApprovalStatus'].'</th>'."\n";
						$x .= '<th>'.$linterface->Get_Checkbox($ID='', $Name='checkmaster', $Value='', $isChecked=0, $Class='', $Display='', $Onclick="(this.checked)?setChecked(1,this.form,'".$CheckBoxName."'):setChecked(0,this.form,'".$CheckBoxName."');").'</th>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";	

				# table body
				$x .= '<tbody>'."\n";
				
					if ($numOfOeaItem == 0)
					{
						$x .= '<tr><td colspan="9" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
					}
					else
					{
						for ($i=0; $i<$numOfOeaItem; $i++)
						{
							$_RecordID 				= $OeaItemInfoArr[$i]['RecordID'];
							$_OEAProgramCode		= $OeaItemInfoArr[$i]['OEA_ProgramCode'];
							$_Title 				= $OeaItemInfoArr[$i]['Title'];
							$_StartYear 			= $OeaItemInfoArr[$i]['StartYear'];
							$_EndYear 				= $OeaItemInfoArr[$i]['EndYear'];
							$_Participation 		= $OeaItemInfoArr[$i]['Participation'];
							$_Role 					= $OeaItemInfoArr[$i]['Role'];
							$_ParticipationNature 	= $OeaItemInfoArr[$i]['ParticipationNature'];
							$_Achievement 			= $OeaItemInfoArr[$i]['Achievement'];
							$_RecordStatus 			= $OeaItemInfoArr[$i]['RecordStatus'];
							$_OLE_STUDENT_RecordID 	= $OeaItemInfoArr[$i]['OLE_STUDENT_RecordID'];
							$_OLE_PROGRAM_ProgramID = $OeaItemInfoArr[$i]['OLE_PROGRAM_ProgramID'];

							$_OLE_Description = $OeaItemInfoArr[$i]['Description'];
					
							# Get Title (with hyperlink
							$_TitlePrefix = ($_OLE_STUDENT_RecordID > 0 && $_OLE_PROGRAM_ProgramID > 0)? $linterface->RequiredSymbol() : '';
							$_Title = "<a id='item_".$_RecordID."' href='#TB_inline?height=500&width=750&inlineId=FakeLayer' onclick='js_edit_action(".$_RecordID."); return false;' class='setting_row thickbox' title='".$button_edit."'>".$_TitlePrefix.$_Title."</a>";
							
							# Get Category Name
							$_CategoryInfoArr = $liboea_setting->getOEACategoryInfoByOEAItemCode($_OEAProgramCode);
							$_CategoryName = $_CategoryInfoArr['CatName'];
							
							# Get Role Name
							$_RoleName = $liboea_item->Get_OEA_RoleName_By_RoleCode($_Role);
							
							# Get Achievement and Participant Nature Name
							$_AchievementName = $liboea_item->Get_OEA_AchievementName_By_AchievementCode($_Achievement);
							$_ParticipationNatureName = $liboea_item->Get_OEA_ParticipationNatureName_By_ParticipationNatureCode($_ParticipationNature);
							$_AchievementDisplay = '';
							if ($_ParticipationNatureName != '') {
								$_AchievementDisplay .= $_ParticipationNatureName.' - ';
							}
							$_AchievementDisplay .= $_AchievementName;
							
							# Get OEA Nature Name
							//$_NatureDisplay = $liboea_item->Get_OEA_ParticipationName_By_ParticipationCode($_Participation);
													
							# Get Approval Status Icon
							$_ApprovalStatusDisplay = $this->getApprovalStatusImage($this->getOEAItemInternalCode(), $_RecordStatus);
							
							# Get Checkbox
							$_class = 'OEA_RecordIDChk_'.$ParticipantType;
							$_Checkbox = $linterface->Get_Checkbox($ID='', $CheckBoxName, $Value=$_RecordID, $isChecked=0, $_class, $Display='', $Onclick='unset_checkall(this, this.form);');
							
							# Description
							$descIconDivId = 'descIconDivID'."_$ParticipantType"."_$i";						
							$thisDescRecordID =$_RecordID;	
							
										
							$MouseEvent = "onmouseover=\"showShortCutPanel('$descIconDivId','$thisDescRecordID','$ParticipantType','$StudentID');\" onmouseout=\"hideShortCutPanel();\" ";
		
							$DescIcon = "<div id='".$descIconDivId."' style=\"width:100%;\" >".
									'<img '. $MouseEvent .'src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_remark.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$Lang['iPortfolio']['OEA']['Description'].'">'.
								"</div>";	
							
							if($_OLE_Description=='')	
							{
								$DescIcon = $Lang['General']['EmptySymbol'];
							}					
													
							$x .= '<tr>'."\n";
								$x .= '<td>'.$_Title.'</td>'."\n";
								$x .= '<td>'.$_CategoryName.'</td>'."\n";
								$x .= '<td>'.$_StartYear.'</td>'."\n";
								$x .= '<td>'.$_EndYear.'</td>'."\n";
								$x .= '<td>'.$_RoleName.'</td>'."\n";
								$x .= '<td>'.$_AchievementDisplay.'</td>'."\n";
								$x .= '<td>'.$DescIcon.'</td>'."\n";
								$x .= '<td>'.$_ApprovalStatusDisplay.'</td>'."\n";
								$x .= '<td>'.$_Checkbox.'</td>'."\n";
							$x .= '</tr>'."\n";
						}
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			$x .= '<span class="tabletextremark" style="float:left;">'.$linterface->RequiredSymbol().$Lang['iPortfolio']['OEA']['RepresentMappedFromOLE'].'</span>'."\n";
			
			return $x;
		}
		
		public function getTeacher_listStuAllInfo_abilityDiv($ObjAbility)
		{
			global $Lang, $iPort, $linterface;
			
			$ObjTypeCode = $this->getAbilityInternalCode();
			$ObjIdFieldName = $ObjAbility->getObjectIdFieldName();
			$ObjID = $ObjAbility->getObjectID();
			$ObjRecordStatus = $ObjAbility->getRecordStatus();
			$ObjApprovedBy = $ObjAbility->getApprovedBy();
			$ObjApprovalDate = $ObjAbility->getApprovalDate();
			$ApprovedStatus = $ObjAbility->get_approved_status();
			
			$h_ContentDiv = '';
			$h_ContentTableDivID = 'ContentTableDiv_'.$ObjTypeCode;
			$h_ApprovalBtnDivID = 'ApprovalBtnDiv_'.$ObjTypeCode;
			
			$h_RemarksMsgTable = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['iPortfolio']['OEA']['AbilityArr']['Instruction']);
						
			$BtnArr = array();
//			if ($ObjRecordStatus == $ApprovedStatus) {
//				$BtnArr[] = array('reject', "javascript:js_Change_Approval_Status('".$ObjTypeCode."', '".$ObjIdFieldName."');", '', $h_ApprovalBtnDivID);
//			}
//			else {
//				$BtnArr[] = array('approve', "javascript:js_Change_Approval_Status('".$ObjTypeCode."', '".$ObjIdFieldName."');", '', $h_ApprovalBtnDivID);
//			}
			$BtnArr[] = array('edit', "javascript:js_Reload_Content_Table('".$ObjTypeCode."', '".$ObjIdFieldName."', 'Edit');");
			
			$h_ContentTable .= '<div id="'.$h_ContentTableDivID.'">'.$this->getTeacher_listStuAllInfo_abilityTable($ObjAbility).'</div>'."\n";
			$h_ContentTable .= '<br style="clear:both;" />'."\n";
			$h_ContentTable .= $this->getTeacher_listStuAllInfo_editModeBottomButtonDiv($ObjTypeCode, $ObjIdFieldName);
			
			return $this->generateTeacher_listStuAllInfo_sectionInfoDiv($ObjTypeCode, $Lang['iPortfolio']['OEA']['PersonalAndGeneralAbility'], $BtnArr, $ObjRecordStatus, $ObjApprovedBy, $ObjApprovalDate, $h_ContentTable, $h_RemarksMsgTable);
		}
		
		
		public function getTeacher_listStuAllInfo_abilityTable($ObjAbility)
		{
			global $Lang, $linterface, $oea_cfg;
			
			$AttributeArr = $ObjAbility->getJUPASAbilityAttributesConfigArr();
			$numOfAttribute = count($AttributeArr);
			$RatingArr = $ObjAbility->getJUPASAbilityRatingsConfigArr();
			$numOfRating = count($RatingArr);
			
			$StudentAbilityInfoArr = $ObjAbility->getAttributesValueArray();
			
			$x = '';
			$x .= '<table class="common_table_list_v30">'."\n";
				# col group
				$x .= '<col align="left" style="width: 35%;">'."\n";
				$x .= '<col align="center" style="width: 10%;">'."\n";
				$x .= '<col align="center" style="width: 10%;">'."\n";
				$x .= '<col align="center" style="width: 10%;">'."\n";
				$x .= '<col align="center" style="width: 10%;">'."\n";
				$x .= '<col align="center" style="width: 25%;">'."\n";
				
				# table head
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>&nbsp;</th>'."\n";
						foreach ((array)$RatingArr as $_RatingInternalCode => $_RatingInfoArr)
						{
							$_RatingName = liboea_ability::Get_Ability_Rating_Name($_RatingInternalCode);
							$x .= '<th style="text-align:center;">'.$_RatingName.'</th>'."\n";
						}
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";		
				
				# table body
				$x .= '<tbody>'."\n";
				
					if ($numOfAttribute == 0)
					{
						$x .= '<tr><td colspan="9" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
					}
					else
					{
						foreach ((array)$AttributeArr as $_AttributeInternalCode => $_AttributeInfoArr)
						{
							$_AttributeName = liboea_ability::Get_Ability_Attribute_Name($_AttributeInternalCode);
							
							$x .= '<tr>'."\n";
								$x .= '<td>'.$_AttributeName.'</td>'."\n";
								foreach ((array)$RatingArr as $_RatingInternalCode => $_RatingInfoArr)
								{
									if (isset($StudentAbilityInfoArr[$_AttributeInternalCode]) && $StudentAbilityInfoArr[$_AttributeInternalCode] == $_RatingInternalCode) {
										$_Display = $linterface->Get_Tick_Image();
									}
									else {
										$_Display = '&nbsp;';
									}
									
									if ($_RatingInternalCode == 'UnableToJudge') {
										// Unable to Judge => grey background
										$_BgStyle = ' background-color: #f0f0f0; ';
									}
									else {
										$_BgStyle = '';
									}
									$x .= '<td style="text-align:center;'.$_BgStyle.'">'.$_Display.'</td>'."\n";
								}
							$x .= '</tr>'."\n";
						}
					}
				
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			
			return $x;
		}
		
		public function getTeacher_listStuAllInfo_abilityEditTable($ObjAbility)
		{
			global $Lang, $linterface, $oea_cfg;
			
			$AttributeArr = liboea_ability::getJUPASAbilityAttributesConfigArr();
			$numOfAttribute = count($AttributeArr);
			$RatingArr = liboea_ability::getJUPASAbilityRatingsConfigArr();
			$numOfRating = count($RatingArr);
			
			$StudentAbilityInfoArr = $ObjAbility->getAttributesValueArray();
			
			$x = '';
			$x .= '<table class="common_table_list_v30">'."\n";
				# col group
				$x .= '<col align="left" style="width: 35%;">'."\n";
				$x .= '<col align="center" style="width: 10%;">'."\n";
				$x .= '<col align="center" style="width: 10%;">'."\n";
				$x .= '<col align="center" style="width: 10%;">'."\n";
				$x .= '<col align="center" style="width: 10%;">'."\n";
				$x .= '<col align="center" style="width: 25%;">'."\n";
				
				# table head
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>&nbsp;</th>'."\n";
						foreach ((array)$RatingArr as $_RatingInternalCode => $_RatingInfoArr)
						{
							$_RatingName = liboea_ability::Get_Ability_Rating_Name($_RatingInternalCode);
							$x .= '<th style="text-align:center;">'.$_RatingName.'</th>'."\n";
						}
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>&nbsp;</th>'."\n";
						foreach ((array)$RatingArr as $_RatingInternalCode => $_RatingInfoArr)
						{
							$_Class = "AbilityRadio_".$_RatingInternalCode;
							$_ApplyAllCheckbox = '<input type="radio" name="AbilityRadio_ApplyAll" onclick="Check_All_Options_By_Class(\''.$_Class.'\', true);" />';
							$x .= '<th style="text-align:center;">'.$_ApplyAllCheckbox.'</th>'."\n";
						}
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";		
				
				# table body
				$x .= '<tbody>'."\n";
				
					if ($numOfAttribute == 0)
					{
						$x .= '<tr><td colspan="9" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
					}
					else
					{
						foreach ((array)$AttributeArr as $_AttributeInternalCode => $_AttributeInfoArr)
						{
							$_AttributeName = liboea_ability::Get_Ability_Attribute_Name($_AttributeInternalCode);
							
							$x .= '<tr>'."\n";
								$x .= '<td>'.$_AttributeName.'</td>'."\n";
								
								foreach ((array)$RatingArr as $_RatingInternalCode => $_RatingInfoArr)
								{
									if (isset($StudentAbilityInfoArr[$_AttributeInternalCode]) && $StudentAbilityInfoArr[$_AttributeInternalCode] == $_RatingInternalCode) {
										$_Checked = 'checked';
									}
									else {
										$_Checked = '';
									}
									
									if ($_RatingInternalCode == 'UnableToJudge') {
										// Unable to Judge => grey background
										$_BgStyle = ' background-color: #f0f0f0; ';
									}
									else {
										$_BgStyle = '';
									}
									
									// use $_AttributeInternalCode instead of $_AttributeCode because cannot get the "W/oth" in the next page if $_AttributeCode is used
									$_id = "AbilityRadio_".$_AttributeInternalCode."_".$_RatingInternalCode;
									$_name = "AbilityRadio_".$_AttributeInternalCode;
									$_class = "AbilityRadio_".$_RatingInternalCode;
									$_RadioBtn = '<input type="radio" id="'.$_id.'" name="'.$_name.'" class="'.$_class.'" '.$_Checked.' value="'.$_RatingInternalCode.'"/>'."\n";
									
									$_onclick = "js_Clicked_Radio_Td('".$_id."');";
									$x .= '<td style="text-align:center;'.$_BgStyle.'" onclick="'.$_onclick.'">'.$_RadioBtn.'</td>'."\n";
								}
								
							$x .= '</tr>'."\n";
						}
					}
				
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			
			return $x;
		}
		
		public function getTeacher_listStuAllInfo_academicDiv($ObjAcademic)
		{
			global $Lang, $iPort, $linterface;
			
			$ObjTypeCode = $this->getAcademicInternalCode();
			$ObjIdFieldName = $ObjAcademic->getObjectIdFieldName();
			
			### Get Subject Info
			$SubjectInfoArr = $ObjAcademic->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=1);
			$numOfSubject = count($SubjectInfoArr);
			
			// TBD: Retrieve from Academic Settings
			$AllowManualAdjustment_Percentile = true;
			$AllowManualAdjustment_Ranking = true;
			
			$BtnArr = array();
			if ($numOfSubject > 0) {
				//$BtnArr[] = array('other', "javascript:js_Generate_Academic();", $Lang['iPortfolio']['OEA']['AcademicArr']['GenerateBasedOnSettings']);
				//$BtnArr[] = array('other', "javascript:js_Reload_Content_Table('".$ObjTypeCode."', '".$ObjIdFieldName."', 'View');", $Lang['iPortfolio']['OEA']['AcademicArr']['GenerateBasedOnSettings']);
				if ($AllowManualAdjustment_Percentile || $AllowManualAdjustment_Ranking) {
					$BtnArr[] = array('edit', "javascript:js_Reload_Content_Table('".$ObjTypeCode."', '".$ObjIdFieldName."', 'Edit');");
				}
			}			
			
			$h_ContentDiv = '';
			$h_ContentTableDivID = 'ContentTableDiv_'.$ObjTypeCode;
			
			$h_ContentDiv .= '<div id="'.$h_ContentTableDivID.'">'.$this->getTeacher_listStuAllInfo_academicTable($ObjAcademic).'</div>'."\n";
			$h_ContentDiv .= '<br style="clear:both;" />'."\n";
			$h_ContentDiv .= $this->getTeacher_listStuAllInfo_editModeBottomButtonDiv($ObjTypeCode, $ObjIdFieldName);
			
			return $this->generateTeacher_listStuAllInfo_sectionInfoDiv($ObjTypeCode, $Lang['iPortfolio']['OEA']['AcademicPerformance'], $BtnArr, '', '', '', $h_ContentDiv, '');
		}
		
		public function getTeacher_listStuAllInfo_academicTable($ObjAcademic)
		{
			global $Lang, $linterface, $oea_cfg, $PATH_WRT_ROOT;
			
			### Get Subject Info
			$SubjectInfoArr = $ObjAcademic->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=1);
			$numOfSubject = count($SubjectInfoArr);
			
			### Get Student Info
			$StudentID = $ObjAcademic->getStudentID();
			$libuser = new libuser($StudentID);
			$CurStudyFormInfoArr = $libuser->Get_User_Studying_Form();
			$CurStudyFormWebSAMSCode = $CurStudyFormInfoArr[0]['FormWebSAMSCode'];
			$IsAppicableForm = (in_array($CurStudyFormWebSAMSCode, (array)liboea::Get_Academic_Applicable_Form_WebSAMS_Code_Array()))? true : false;
			
			$x = '';
			
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="common_table_list_v30">'."\n";
				if (!$IsAppicableForm) {
					$x .= '<tr><td align="center" class="stu_info_log">'.$Lang['iPortfolio']['OEA']['AcademicArr']['AcademicForFormSixOnly'].'</td></tr>'."\n";
				}
				else if ($numOfSubject == 0) {
					$x .= '<tr><td align="center" class="stu_info_log">'.$Lang['iPortfolio']['OEA']['WarningArr']['NoSubjectOrAcademicSettings'].'</td></tr>'."\n";
				}
				else {
					$SubjectColWidth = 20;
					$PercentileColWidth = 8;
					$RatingColWidth = 8;
					$BorderWidth = 3;
					
					### Get Percentile Settings
					$PercentileInfoArr = liboea_academic::Get_Academic_Percentile_Config_Array();
					$numOfPercentile = count($PercentileInfoArr);
					
					### Get Overall Rating Settings
					$RatingGradeArr = liboea_academic::Get_Academic_Overall_Rating_Config_Array();
					$numOfRating = count($RatingGradeArr);
					
					
					### Get Student Percentile & Subject Overall Result
					$StudentSubjectPercentileAssoArr = $ObjAcademic->getObjectSubjectPercentileInfoArr();
					$StudentSubjectOverallRatingAssoArr = $ObjAcademic->getObjectSubjectOverallRatingInfoArr(); 
					
					### Get Pencentile Remarks
					$PercentileRemark = stripslashes($ObjAcademic->getPercentileRemark());
					
					### Get Rating Remarks
					$OverallRatingRemark = stripslashes($ObjAcademic->getOverallRatingRemark());
					
					# Table Column Settigns
					$x .= '<col style="width: '.$SubjectColWidth.'%;">'."\n";
					for ($i=0; $i<$numOfPercentile; $i++) {
						$x .= '<col style="width: '.$PercentileColWidth.'%;">'."\n";
					}					
					for ($i=0; $i<$numOfRating; $i++) {
						$x .= '<col style="width: '.$RatingColWidth.'%;">'."\n";
					}
					
					# Table Header
					$x .= '<thead>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<th rowspan="2" style="border-right-width:'.$BorderWidth.'px;">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</th>'."\n"; 
							$x .= '<th colspan="'.$numOfPercentile.'" class="sub_row_top" style="border-right-width:'.$BorderWidth.'px;text-align:center;">'.$Lang['iPortfolio']['OEA']['AcademicArr']['Percentile'].' ('.$Lang['iPortfolio']['OEA']['AcademicArr']['PositionInForm'].')</th>'."\n";
							$x .= '<th colspan="'.$numOfRating.'" class="sub_row_top" style="text-align:center;">'.$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating'].'</th>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$PercentileCount = 0;
							foreach ((array)$PercentileInfoArr as $thisPercentileInternalCode => $thisPercentileInfoArr) {
								$thisStyle = '';
								if ($PercentileCount == $numOfPercentile-1) {
									 $thisStyle = 'border-right-width:'.$BorderWidth.'px;';
								}
								$x .= '<th class="sub_row_top" style="text-align:center;'.$thisStyle.'">'.liboea_academic::Get_Academic_Percentile_Name($thisPercentileInternalCode).'</th>'."\n";
								$PercentileCount++;
							}					
							foreach ((array)$RatingGradeArr as $thisOverallRatingInternalCode => $thisOverallRatingInfoArr) {
								$x .= '<th class="sub_row_top" style="text-align:center;">'.liboea_academic::Get_Academic_Overall_Rating_Name($thisOverallRatingInternalCode).'</th>'."\n";
							}
						$x .= '</tr>'."\n";
					$x .= '</thead>'."\n";
					
					# Table Content
					$x .= '<tbody>'."\n";
						for ($i=0; $i<$numOfSubject; $i++) {
							$thisSubjectID = $SubjectInfoArr[$i]['SubjectID'];
							$thisSubjectName = Get_Lang_Selection($SubjectInfoArr[$i]['SubjectNameCh'], $SubjectInfoArr[$i]['SubjectNameEn']);
							$thisSubjectPercentile = $StudentSubjectPercentileAssoArr[$thisSubjectID];
							$thisSubjectRating = $StudentSubjectOverallRatingAssoArr[$thisSubjectID];
							
							$x .= '<tr>'."\n";
								$x .= '<td style="border-right-width:'.$BorderWidth.'px;">'.$thisSubjectName.'</td>'."\n";
								$PercentileCount = 0;
								foreach ((array)$PercentileInfoArr as $thisPercentileInternalCode => $thisPercentileInfoArr) {
									$thisStyle = '';
									if ($PercentileCount == $numOfPercentile-1) {
										 $thisStyle = 'border-right-width:'.$BorderWidth.'px;';
									}
									
									if ($thisSubjectPercentile == $thisPercentileInternalCode) {
										$thisDisplay = $linterface->Get_Tick_Image();
									}
									else {
										$thisDisplay = '&nbsp;';
									}
									
									$x .= '<td style="text-align:center;'.$thisStyle.'">'.$thisDisplay.'</td>'."\n";
									$PercentileCount++;
								}					
								foreach ((array)$RatingGradeArr as $thisOverallRatingInternalCode => $thisOverallRatingInfoArr) {
									if ($thisSubjectRating == $thisOverallRatingInternalCode) {
										$thisDisplay = $linterface->Get_Tick_Image();
									}
									else {
										$thisDisplay = '&nbsp;';
									}
									
									$x .= '<td style="text-align:center;">'.$thisDisplay.'</td>'."\n";
								}
							$x .= '</tr>'."\n";
						}
					//
					if($PercentileRemark||$OverallRatingRemark){
						$numOfCode = count($PercentileInfoArr);
						$x .= '<tr class="AcademicTr" id="APRemarksRow"><td style="border-right-width:'.$BorderWidth.'px;">Remarks</td><td colspan="'.$numOfCode.'" style="padding:2px; overflow:hidden; border-right-width:'.$BorderWidth.'px;">';
						if($PercentileRemark){
							$x .= '<div name="APpercentileRemarks" id="APpercentileRemarks" style="width:100%; height:100%; overflow:auto">'.nl2br($PercentileRemark).'</div>';
						}
						$x .= '<td colspan="'.$numOfCode.'" style="padding:2px; overflow:hidden;">';
						if($OverallRatingRemark){
							$x .= '<div name="APratingRemarks" id="APratingRemarks" style="width:100%; height:100%; overflow:auto">'.nl2br($OverallRatingRemark).'</div>';
						}
					}
					//
					$x .= '</tbody>'."\n";
				}
			$x .= '</table>';
			
			return $x;
		}
		
		public function getTeacher_listStuAllInfo_academicEditTable($ObjAcademic)
		{
			global $Lang, $linterface, $oea_cfg, $PATH_WRT_ROOT;
			
			### Get Subject Info
			$SubjectInfoArr = $ObjAcademic->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=1);
			$numOfSubject = count($SubjectInfoArr);
			
			
			### Action Button
			$BtnArr = array();
			if ($numOfSubject > 0) {
				$BtnArr[] = array('other', "javascript:js_Generate_Academic();", $Lang['iPortfolio']['OEA']['AcademicArr']['GenerateBasedOnSettings']);
			}
			
			
			### Determine View / Edit for Percentile and Overall Ranking
			// TBD: Retrieve from Academic Settings
			$AllowManualAdjustment_Percentile = true;
			$AllowManualAdjustment_Ranking = true;
			$ViewMode_Percentile = ($AllowManualAdjustment_Percentile)? 'Edit' : 'View';
			$ViewMode_Ranking = ($AllowManualAdjustment_Ranking)? 'Edit' : 'View';
			
			
			### Get Student Percentile & Subject Overall Result
			$StudentSubjectPercentileAssoArr = $ObjAcademic->getObjectSubjectPercentileInfoArr();
			$StudentSubjectOverallRatingAssoArr = $ObjAcademic->getObjectSubjectOverallRatingInfoArr(); 
			
			
			$x = '';
			
			### Auto Mapping Status
			$x .= '<div id="AllowClientProgramConnectDiv" style="float:left; cursor:pointer;" onclick="js_Switch_Academic_Auto_Mapping_Status();" onmouseover="js_Show_Academic_Mapping_Details_Layer();" onmouseout="js_Hide_Academic_Mapping_Details_Layer();">'."\n";
				$x .= $linterface->GET_LNK_ACTIVE('javascript:void(0);', 1, $Lang['General']['Enabled'], 1, 'AcademicAutoMappingIcon')."\n";
				$x .= '<span id="AcademicAutoMappingSpan"> '.$Lang['iPortfolio']['OEA']['AcademicArr']['AutoMappingForPercentileAndOverallRating'].'</span>'."\n";
			$x .= '</div>'."\n";
			
			# Auto Mapping Status Details Table
			$x .= '<div id="AutoMappingDetailsDiv" class="selectbox_layer" style="width:400px; display:none;">'."\n";
				$x .= $linterface->GET_NAVIGATION2_IP25($Lang['iPortfolio']['OEA']['AcademicArr']['MappingSettings']);
				$x .= $this->Get_Academic_Percentile_Overall_Rating_Mapping_View_Table();
			$x .= '</div>'."\n";
			
			
			
			### Main Table
			$x .= $linterface->Get_DBTable_Action_Button_IP25($BtnArr)."\n";
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="common_table_list_v30">'."\n";
				if ($numOfSubject == 0) {
					$x .= '<tr><td align="center" class="stu_info_log">'.$Lang['iPortfolio']['OEA']['WarningArr']['NoSubjectSettings'].'</td></tr>'."\n";
				}
				else {
					$SubjectColWidth = 20;
					$PercentileColWidth = 8;
					$RatingColWidth = 8;
					$BorderWidth = 3;
					
					### Get Percentile Settings
					$PercentileInfoArr = liboea_academic::Get_Academic_Percentile_Config_Array();
					$numOfPercentile = count($PercentileInfoArr);
					
					### Get Overall Rating Settings
					$RatingGradeArr = liboea_academic::Get_Academic_Overall_Rating_Config_Array();
					$numOfRating = count($RatingGradeArr); 
					
					### Get Pencentile Remarks
					$PercentileRemark = stripslashes($ObjAcademic->getPercentileRemark());
					
					### Get Rating Remarks
					$OverallRatingRemark = stripslashes($ObjAcademic->getOverallRatingRemark());
					
					
					# Table Column Settigns
					$x .= '<col style="width: '.$SubjectColWidth.'%;">'."\n";
					for ($i=0; $i<$numOfPercentile; $i++) {
						$x .= '<col style="width: '.$PercentileColWidth.'%;">'."\n";
					}					
					for ($i=0; $i<$numOfRating; $i++) {
						$x .= '<col style="width: '.$RatingColWidth.'%;">'."\n";
					}
					
					# Table Header
					$x .= '<thead>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<th rowspan="2" style="border-right-width:'.$BorderWidth.'px;">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</th>'."\n"; 
							$x .= '<th colspan="'.$numOfPercentile.'" class="sub_row_top" style="border-right-width:'.$BorderWidth.'px;text-align:center;">'.$Lang['iPortfolio']['OEA']['AcademicArr']['Percentile'].' ('.$Lang['iPortfolio']['OEA']['AcademicArr']['PositionInForm'].')</th>'."\n";
							$x .= '<th colspan="'.$numOfRating.'" class="sub_row_top" style="text-align:center;">'.$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating'].'</th>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$PercentileCount = 0;
							foreach ((array)$PercentileInfoArr as $thisPercentileInternalCode => $thisPercentileInfoArr) {
								$thisStyle = '';
								if ($PercentileCount == $numOfPercentile-1) {
									 $thisStyle = 'border-right-width:'.$BorderWidth.'px;';
								}
								
								$x .= '<th class="sub_row_top" style="text-align:center;'.$thisStyle.'">'.liboea_academic::Get_Academic_Percentile_Name($thisPercentileInternalCode).'</th>'."\n";
								$PercentileCount++;
							}				
								
							foreach ((array)$RatingGradeArr as $thisOverallRatingInternalCode => $thisOverallRatingInfoArr) {
								$x .= '<th class="sub_row_top" style="text-align:center;">'.liboea_academic::Get_Academic_Overall_Rating_Name($thisOverallRatingInternalCode).'</th>'."\n";
							}
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<th style="border-right-width:'.$BorderWidth.'px;">&nbsp;</th>'."\n";
							$PercentileCount = 0;
							foreach ((array)$PercentileInfoArr as $thisPercentileInternalCode => $thisPercentileInfoArr) {
								$_id = "PercentileRadio_ApplyAll_".$thisPercentileInternalCode;
								$_name = "PercentileRadio_ApplyAll";
								$_class = "PercentileRadio_".$thisPercentileInternalCode;
								$_onclick = "js_Clicked_Academic_Percentile_Select_All_Radio('".$_class."');";
								$thisDisplay = $linterface->Get_Radio_Button($_id, $_name, $thisPercentileInternalCode, $isChecked=0, $_class, $Display="", $_onclick, $isDisabled=0);
								
								$thisStyle = '';
								if ($PercentileCount == $numOfPercentile-1) {
									 $thisStyle = 'border-right-width:'.$BorderWidth.'px;';
								}
								
								$thisOnClick = "js_Clicked_Radio_Td('".$_id."');";
								$x .= '<th class="sub_row_top" style="text-align:center;'.$thisStyle.'" onclick="'.$thisOnClick.'">'.$thisDisplay.'</th>'."\n";
								$PercentileCount++;
							}
							foreach ((array)$RatingGradeArr as $thisOverallRatingInternalCode => $thisOverallRatingInfoArr) {
								$_id = "OverallRatingRadio_ApplyAll_".$thisOverallRatingInternalCode;
								$_name = "OverallRatingRadio_ApplyAll";
								$_class = "OverallRatingRadio_".$thisOverallRatingInternalCode;
								$_onclick = "Check_All_Options_By_Class('".$_class."', true);";
								$thisDisplay = $linterface->Get_Radio_Button($_id, $_name, $i, $isChecked=0, $_class, $Display="", $_onclick, $isDisabled=0);
								
								$thisOnClick = "js_Clicked_Radio_Td('".$_id."');";
								$x .= '<th class="sub_row_top" style="text-align:center;" onclick="'.$thisOnClick.'">'.$thisDisplay.'</th>'."\n";
							}
						$x .= '</tr>'."\n";
					$x .= '</thead>'."\n";
					
					# Table Content
					$x .= '<tbody>'."\n";
						for ($i=0; $i<$numOfSubject; $i++) {
							$thisSubjectID = $SubjectInfoArr[$i]['SubjectID'];
							$thisSubjectName = Get_Lang_Selection($SubjectInfoArr[$i]['SubjectNameCh'], $SubjectInfoArr[$i]['SubjectNameEn']);
							$thisStudentSubjectPercentile = $StudentSubjectPercentileAssoArr[$thisSubjectID];
							$thisStudentSubjectOverallRating = $StudentSubjectOverallRatingAssoArr[$thisSubjectID];
							
							$x .= '<tr class="AcademicTr" subjectId="'.$thisSubjectID.'">'."\n";
								$x .= '<td style="border-right-width:'.$BorderWidth.'px;">'.$thisSubjectName.'</td>'."\n";
								
								$PercentileCount = 0;
								foreach ((array)$PercentileInfoArr as $thisPercentileInternalCode => $thisPercentileInfoArr) {
									$thisStyle = '';
									$thisOnClick = '';
									
									if ($PercentileCount == $numOfPercentile-1) {
										 $thisStyle = 'border-right-width:'.$BorderWidth.'px;';
									}
									
									if ($ViewMode_Percentile == 'View') {
										if ($thisStudentSubjectPercentile !== '' && $thisStudentSubjectPercentile == $thisPercentileInternalCode) {
											$thisDisplay = $linterface->Get_Tick_Image();
										}
										else {
											$thisDisplay = '&nbsp;';
										}
									}
									else if ($ViewMode_Percentile == 'Edit') {
										if ($thisStudentSubjectPercentile !== '' && $thisStudentSubjectPercentile == $thisPercentileInternalCode) {
											$isChecked = 1;
										}
										else {
											$isChecked = 0;
										}
										
										$_value = $thisPercentileInternalCode;
										$_id = "PercentileRadio_".$thisSubjectID."_".$_value;
										$_name = "SubjectPercentileAssoArr[".$thisSubjectID."]";
										$_class = "AcademicRadio PercentileRadio_SubjectID_".$thisSubjectID." PercentileRadio_".$_value;
										$_onclick = "js_Clicked_Academic_Percentile_Radio('".$thisSubjectID."', '".$_value."');";
										$thisDisplay = $linterface->Get_Radio_Button($_id, $_name, $_value, $isChecked, $_class, $Display="", $_onclick ,$isDisabled=0);
										
										$thisOnClick = "js_Clicked_Radio_Td('".$_id."');";
									} 
									
									$x .= '<td style="text-align:center;'.$thisStyle.'" onclick="'.$thisOnClick.'">'.$thisDisplay.'</td>'."\n";
									$PercentileCount++;
								}	
												
								foreach ((array)$RatingGradeArr as $thisOverallRatingInternalCode => $thisOverallRatingInfoArr) {
									$thisOnClick = '';
									
									if ($ViewMode_Ranking == 'View') {
										if ($thisStudentSubjectOverallRating == $thisOverallRatingInternalCode) {
											$thisDisplay = $linterface->Get_Tick_Image();
										}
										else {
											$thisDisplay = '&nbsp;';
										}
									}
									else if ($ViewMode_Ranking == 'Edit') {
										if ($thisStudentSubjectOverallRating == $thisOverallRatingInternalCode) {
											$isChecked = 1;
										}
										else {
											$isChecked = 0;
										}
										
										$_id = "OverallRatingRadio_".$thisSubjectID."_".$thisOverallRatingInternalCode;
										$_name = "SubjectOverallRatingAssoArr[".$thisSubjectID."]";
										$_class = "AcademicRadio OverallRatingRadio_SubjectID_".$thisSubjectID." OverallRatingRadio_".$thisOverallRatingInternalCode;
										$_onclick = "js_Clicked_Academic_Remarks('".$thisSubjectID."', '".$_value."');";
										$thisDisplay = $linterface->Get_Radio_Button($_id, $_name, $thisOverallRatingInternalCode, $isChecked, $_class, $Display="", $_onclick, $isDisabled=0);
										
										$thisOnClick = "js_Clicked_Radio_Td('".$_id."');";
									} 
									
									$x .= '<td style="text-align:center;" onclick="'.$thisOnClick.'">'.$thisDisplay.'</td>'."\n";
								}
							$x .= '</tr>'."\n";
						}
					//
					$numOfCode = count($PercentileInfoArr);
					$textAreaDefaultValue = $Lang['iPortfolio']['OEA']['defaultEmptyTextAreaMsg'];
					$x .= '<tr class="AcademicTr" style="';
					$x .= ($PercentileRemark||$OverallRatingRemark)? '':"display:none";
					$x .= '" id="APRemarksRow"><td style="border-right-width:'.$BorderWidth.'px; height:200px">'.$Lang['iPortfolio']['OEA']['Remarks'].'</td><td colspan="'.$numOfCode.'" style="padding:0px; overflow:hidden; border-right-width:'.$BorderWidth.'px; height:200px">';
					$x .= '<textarea name="APpercentileRemarks" id="APpercentileRemarks" onfocus="if(this.value===\''.$textAreaDefaultValue.'\'){this.value=\'\';this.style.color=\'#000\'}" onblur="if(this.value===\'\'){this.value=\''.$textAreaDefaultValue.'\';this.style.color=\'#909090\'}" style="';
					$x .= $PercentileRemark?'':"display:none";
					$x .= '; width:100%; height:100%; overflow:auto; ';
					$x .= $PercentileRemark?'color:#000':'color:#909090';
					$x .= '">';
					$x .=  $PercentileRemark?$PercentileRemark:$textAreaDefaultValue;
					$x .= '</textarea>';
					$x .= '<td colspan="'.$numOfCode.'" style="padding:0px; overflow:hidden; height:200px">';
					$x .= '<textarea name="APratingRemarks" id="APratingRemarks" onfocus="if(this.value===\''.$textAreaDefaultValue.'\'){this.value=\'\';this.style.color=\'#000\'}" onblur="if(this.value===\'\'){this.value=\''.$textAreaDefaultValue.'\';this.style.color=\'#909090\'}" style="';
					$x .= $OverallRatingRemark?'':"display:none";
					$x .= '; width:100%; height:100%; overflow:auto; ';
					$x .= $OverallRatingRemark?'color:#000':'color:#909090';
					$x .= '">';
					$x .= $OverallRatingRemark?$OverallRatingRemark:$textAreaDefaultValue;
					$x .= '</textarea>';
					//
					$x .= '</tbody>'."\n";
				}
			$x .= '</table>';
			
			return $x;
		}
		
		
		private function generateTeacher_listStuAllInfo_sectionInfoDiv($Type, $Title, $BtnArr, $ApprovalStatus, $ApprovedBy, $ApprovalDate, $DisplayContent, $RemarkMsgTable='')
		{
			global $Lang, $linterface;
			
			$h_SectionOutestDivID = 'SectionOutestDiv_'.$Type;
			$h_SectionDivID = 'SectionDiv_'.$Type;
			$h_ReturnMsgDivID = 'ReturnMsgDiv_'.$Type;
			$h_ShowSectionSpanID = 'spanShowOption_'.$h_SectionDivID;
			$h_HideSectionSpanID = 'spanHideOption_'.$h_SectionDivID;
			$h_ApprovalStatusSpanID = 'ApprovalStatusSpan_'.$Type;
			$h_ActionBtnDivID = 'ActionBtnDiv_'.$Type;
			$h_ContentDivID = 'ContentDiv_'.$Type;
			
			$h_ApprovalStatusDiv = '';
			if ($Type == $this->getAddiInfoInternalCode()) {
				$h_ApprovalStatusDiv .= '<div style="float:left;">'."\n";
						$h_ApprovalStatusDiv .= $Lang['iPortfolio']['OEA']['ApprovalStatus'].': <span id="'.$h_ApprovalStatusSpanID.'">'.$this->getApprovalStatusDisplay($Type, $ApprovalStatus, $ApprovedBy, $ApprovalDate).'</span>'."\n";
				$h_ApprovalStatusDiv .= '</div>'."\n";
			}
			
			$x = '';
			
			
			$x .= '<div id="'.$h_SectionOutestDivID.'">'."\n";
				# Navigation and Return Message Div
				$x .= '<span style="float:left;">'.$linterface->GET_NAVIGATION2_IP25($Title).'</span>'."\n";
				$x .= '<span style="float:left;">'.$linterface->Get_Thickbox_Return_Message_Layer($h_ReturnMsgDivID).'</span>'."\n";
				$x .= '<span style="float:right;">'."\n";
					$x .= '<span id="'.$h_ShowSectionSpanID.'" style="display:none;width:100%;float:right;">'."\n";
						$x .= $linterface->Get_Show_Option_Link("javascript:js_Show_Option_Div('".$h_SectionDivID."');", "float:right;", $Lang['Btn']['ShowSection']);
					$x .= '</span>'."\n";
					$x .= '<span id="'.$h_HideSectionSpanID.'" style="width:100%;float:right;">'."\n";
						$x .= $linterface->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('".$h_SectionDivID."');", "float:right;", $Lang['Btn']['HideSection']);
					$x .= '</span>'."\n";
				$x .= '</span>'."\n";
				$x .= '<br style="clear:both;" />'."\n";
				
				
				# Approval Status Display and Edit, Approve Buttons
				$x .= '<div id="'.$h_SectionDivID.'">'."\n";
					if ($RemarkMsgTable == '') {
						// do nth
					}
					else {
						$x .= $RemarkMsgTable."\n";
					}
					
					$x .= $h_ApprovalStatusDiv."\n";
					
					if (count((array)$BtnArr) > 0) {
						$x .= '<div id="'.$h_ActionBtnDivID.'">'.$linterface->Get_DBTable_Action_Button_IP25($BtnArr).'</div>'."\n";
						$x .= '<br style="clear:both;" />'."\n";
					}
					$x .= '<div id="'.$h_ContentDivID.'">'.$DisplayContent.'</div>'."\n";
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";

			return $x;
		}
		
		private function getTeacher_listStuAllInfo_editModeBottomButtonDiv($TypeCode, $DbIdFieldName)
		{
			global $Lang, $linterface;
			
			$h_BottomButtonDivID = 'BottomButtonDiv_'.$TypeCode;
			
			if ($TypeCode == $this->getAddiInfoInternalCode()) {
				$BtnSaveAndApprove = $linterface->GET_ACTION_BTN($Lang['iPortfolio']['OEA']['SaveAndApprove'], "button", $onclick="js_Save_Info('".$TypeCode."', '".$DbIdFieldName."', 1);", $id="Btn_Save_And_Approve")."&nbsp;\n";
				$BtnSave = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="js_Save_Info('".$TypeCode."', '".$DbIdFieldName."');", $id="Btn_Save")."&nbsp;\n";
			}
			else {
				$BtnSaveAndApprove = '';
				
				// Do Save and Approve actually
				$BtnSave = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="js_Save_Info('".$TypeCode."', '".$DbIdFieldName."', 1);", $id="Btn_Save")."&nbsp;\n";
			}
			$BtnCancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Reload_Content_Table('".$TypeCode."', '".$DbIdFieldName."', 'View');", $id="Btn_Cancel")."\n";
			
			$x = '';
			$x .= '<div id="'.$h_BottomButtonDivID.'" class="edit_bottom_v30" style="display:none;">'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				$x .= $BtnSaveAndApprove;
				$x .= $BtnSave;
				$x .= $BtnCancel;
			$x .= '</div>'."\n";
			
			return $x;
		}
		
		public function approveOEAItem($ItemIDArr)
		{
			global $oea_cfg;
			return $this->updateOEAItemRecordStatus($ItemIDArr, liboea_item::get_OEA_Approved_Status());
		}
		
		public function rejectOEAItem($ItemIDArr)
		{
			global $oea_cfg;
			return $this->updateOEAItemRecordStatus($ItemIDArr, liboea_item::get_OEA_Rejected_Status());
		}
		
		private function updateOEAItemRecordStatus($ItemIDArr, $RecordStatus)
		{
			global $eclass_db;
			
			if (count((array)$ItemIDArr) > 0) {
				$OEA_STUDENT = $eclass_db.'.OEA_STUDENT';
				$sql = "Update $OEA_STUDENT set RecordStatus = '".$RecordStatus."' Where RecordID In (".implode(',', (array)$ItemIDArr).") ";
				return $this->db_db_query($sql);
			}
			else {
				return false;
			}
		}
		
		public function getStudentInfoForCsv($StudentIDArr)
		{
			$sql = "Select
							If (iau.UserID Is Null, iu.UserID, iau.UserID) as UserID,
							If (iau.UserID Is Null And iu.HKJApplNo Is Not Null And iu.HKJApplNo != 0, iu.HKJApplNo, '') as ApplicationNumber,
							'".$this->getSchoolCode()."' as SchoolCode,
							/* If (iau.UserID Is Null, iu.HKID, '') as HKID, */
							'' as HKID,
							'' as PassportNumber,
							'' as IssueCountry,
							/* If (iau.UserID Is Null, iu.LastName, '') as LastName, */
							/* If (iau.UserID Is Null, iu.FirstName, '') as FirstName, */
							'' as LastName,
							'' as FirstName,
							/* yc.WEBSAMSCode as Class, */
							'' as Class,
							/* If (iau.UserID Is Null, iu.WebSAMSRegNo, '') as StudentNumber, */
							'' as StudentNumber,
							'' as GroupName
					From
							YEAR_CLASS_USER as ycu
							Inner Join
							YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
							Left Join
							INTRANET_USER as iu On (ycu.UserID = iu.UserID)
							Left Join
							INTRANET_ARCHIVE_USER as iau On (ycu.UserID = iau.UserID)
					Where
							ycu.UserID In (".implode(',', (array)$StudentIDArr).")
							And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					Order By
							yc.Sequence, ycu.ClassNumber
					";

			return $this->returnArray($sql);
		}
		
		private static function getJupasCsvCommonHeaderArr() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvHeaderArr"]["CommonItem"];
		}
		
		private static function getOEAItemJupasCsvHeaderArr() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvHeaderArr"]["OeaItem"];
		}
		private static function getAddiInfoJupasCsvHeaderArr() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvHeaderArr"]["AddiInfo"];
		}
		private static function getAbilityJupasCsvHeaderArr() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvHeaderArr"]["Ability"];
		}
		private static function getAcademicJupasCsvHeaderArr() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvHeaderArr"]["Academic"];
		}
		private static function getSuppInfoJupasCsvHeaderArr() {
			global $oea_cfg;
			return $oea_cfg["JupasCsvHeaderArr"]["SuppInfo"];
		}
		
		public function getOEAItemJupasCsvExportContent($StudentInfoArr)
		{
			global $libexport, $oea_cfg;
			
			$liboea_setting = new liboea_setting();
			
			### Get OEA Item Category Code Mapping
			$OeaInfoAssoArr = $liboea_setting->get_OEA_Item();
			
			### Get Student OEA Info
			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			$ApprovedStatus = $this->getItemTypeApprovedStatus($this->getOEAItemInternalCode());
			$StudentOeaInfoArr = $this->getStudentOeaItemInfoArr($StudentIDArr, $ApprovedStatus, $ParticipationArr='', $ParRecordIDArr='', $ParExcludeRecordIDArr='', $OLE_ProgramIDArr='', $withCodeInOeaTitle=false);
			$StudentOeaInfoAssoArr = BuildMultiKeyAssoc($StudentOeaInfoArr, 'StudentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			unset($StudentOeaInfoArr);
			
			$ExportContentArr = array();
			$numOfStudent = count((array)$StudentInfoArr);
			$numOfStudentInfoItem = count((array)$this->getJupasCsvCommonHeaderArr());
			$RowCounter = 0;
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$_StudentID = $StudentInfoArr[$i]['UserID'];
				$_StudentOeaItemInfoArr = (array)$StudentOeaInfoAssoArr[$_StudentID];
				$numOfOeaItem = count($_StudentOeaItemInfoArr);
				
				for ($j=0; $j<$numOfOeaItem; $j++)
				{
					$_ActivityCode 			= $_StudentOeaItemInfoArr[$j]['OEA_ProgramCode'];
					$_ActivityName 			= $_StudentOeaItemInfoArr[$j]['Title'];
					$_CategoryCode 			= $OeaInfoAssoArr[$_ActivityCode]['CatCode'];
					$_YearFrom 				= $_StudentOeaItemInfoArr[$j]['StartYear'];
					$_YearTo 				= $_StudentOeaItemInfoArr[$j]['EndYear'];
					$_Participation 		= $_StudentOeaItemInfoArr[$j]['Participation'];
					$_AwardBearing 			= $_StudentOeaItemInfoArr[$j]['OEA_AwardBearing'];
					$_ParticipationNature 	= $_StudentOeaItemInfoArr[$j]['ParticipationNature'];
					$_Role 					= $_StudentOeaItemInfoArr[$j]['Role'];
					$_Achievement 			= $_StudentOeaItemInfoArr[$j]['Achievement'];
					$_Description 			= $_StudentOeaItemInfoArr[$j]['Description'];
					$_Description = $this->standardizeCsvLineBreakData(trim($_Description), $oea_cfg["OEA_STUDENT_Description"]["MaxLength"]);
					
					for ($k=0; $k<$numOfStudentInfoItem; $k++)	// k=0 is UserID => skip it
					{
						$ExportContentArr[$RowCounter][] =  $StudentInfoArr[$i][$k+1];
					}
					
					if ($_AwardBearing==$oea_cfg["OEA_AwardBearing_Type_No"]) {
						// make sure if no award bearing, the achievement is no award also
						$_Achievement = $oea_cfg["OEA_Achievement_Type_NoAward"];
					}
					
					$ExportContentArr[$RowCounter][] =  $_ActivityCode;
					$ExportContentArr[$RowCounter][] =  $_ActivityName;
					//$ExportContentArr[$RowCounter][] =  $_CategoryCode;
					$ExportContentArr[$RowCounter][] =  $_YearFrom;
					$ExportContentArr[$RowCounter][] =  $_YearTo;
					$ExportContentArr[$RowCounter][] =  $_Participation;
					$ExportContentArr[$RowCounter][] =  $_AwardBearing;
					$ExportContentArr[$RowCounter][] =  $_ParticipationNature;
					$ExportContentArr[$RowCounter][] =  $_Role;
					$ExportContentArr[$RowCounter][] =  $_Achievement;
					$ExportContentArr[$RowCounter][] =  $_Description;
					
					$RowCounter++;
				}
			}
			
			return $this->getJupasCsvExportText($ExportContentArr, $this->getOEAItemJupasCsvHeaderArr());
		}
		
		public function getAddiInfoJupasCsvExportContent($StudentInfoArr)
		{
			### Get Student Additional Info
			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			$ApprovedStatus = $this->getItemTypeApprovedStatus($this->getAddiInfoInternalCode());
			$StudentAddiInfoAssoArr = BuildMultiKeyAssoc($this->getStudentAddiInfoArr($StudentIDArr, $ApprovedStatus), 'StudentID', $IncludedDBField=array('Details'));
			
			$ExportContentArr = array();
			$numOfStudent = count((array)$StudentInfoArr);
			$numOfStudentInfoItem = count((array)$this->getJupasCsvCommonHeaderArr());
			$RowCounter = 0;
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$_StudentID = $StudentInfoArr[$i]['UserID'];
				//$_StudentAddiInfo = trim($StudentAddiInfoAssoArr[$_StudentID]['Details']);
				$_StudentAddiInfo = $this->standardizeCsvLineBreakData(trim($StudentAddiInfoAssoArr[$_StudentID]['Details']), liboea_additional_info::getDetailsMaxLength());
							
				if ($_StudentAddiInfo == '') {
					continue;
				}
				
				for ($j=0; $j<$numOfStudentInfoItem; $j++)	// k=0 is UserID => skip it
				{
					$ExportContentArr[$RowCounter][] =  $StudentInfoArr[$i][$j+1];
				}
				$ExportContentArr[$RowCounter][] =  $_StudentAddiInfo;
				
				$RowCounter++;
			}
			
			return $this->getJupasCsvExportText($ExportContentArr, $this->getAddiInfoJupasCsvHeaderArr());
		}
		
		public function getAbilityJupasCsvExportContent($StudentInfoArr)
		{
			global $oea_cfg;
			
			$libAbility = new liboea_ability();
			
			### Get Student Ability Info
			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			$ApprovedStatus = $this->getItemTypeApprovedStatus($this->getAbilityInternalCode());
			$StudentAbilityAssoArr = BuildMultiKeyAssoc($this->getStudentAbilityArr($StudentIDArr, $ApprovedStatus), 'StudentID', $IncludedDBField=array('AbilityValue'));
			
			$ExportContentArr = array();
			$numOfStudent = count((array)$StudentInfoArr);
			$numOfStudentInfoItem = count((array)$this->getJupasCsvCommonHeaderArr());
			
			$attributeAry = array();
			foreach($oea_cfg["Ability"]["Attribute"] as $ele){
				$attributeAry[$ele['Order']] = $ele['JupasCode'];
			}
			ksort($attributeAry);
			$RowCounter = 0;
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$_StudentID = $StudentInfoArr[$i]['UserID'];
				$_StudentAbilityValue = $StudentAbilityAssoArr[$_StudentID]['AbilityValue'];
				$_StudentAbilityAssoArrINORDER = $libAbility->getAttributesValueArray($_StudentAbilityValue);
				
				for ($j=0; $j<$numOfStudentInfoItem; $j++)	// k=0 is UserID => skip it
				{
					$ExportContentArr[$RowCounter][] =  $StudentInfoArr[$i][$j+1];
				}
				### Rearrange Attributes Array according to Order in Config
					foreach((array)$attributeAry as $key=>$val){
						$val = $libAbility->Get_Ability_Attribute_InternalCode_By_JupasCode($val);
						$_StudentAbilityAssoArr[$val] = $_StudentAbilityAssoArrINORDER[$val];
					}
				### One row for each Attributes
				foreach ((array)$_StudentAbilityAssoArr as $_AttributeInternalCode => $_RatingInternalCode)
				{
					//$ExportContentArr[$RowCounter][] =  liboea_ability::Get_Ability_Attribute_JupasCode_By_InternalCode($_AttributeInternalCode);
					$ExportContentArr[$RowCounter][] =  liboea_ability::Get_Ability_Rating_JupasCode_By_InternalCode($_RatingInternalCode);	
				}
				$RowCounter++;
			}
			
			foreach($attributeAry as &$val){
				$val = strtoupper($val);
			}
			
			return $this->getJupasCsvExportText($ExportContentArr, array_merge($this->getAbilityJupasCsvHeaderArr(),($attributeAry)));
		}
		
		public function getAcademicJupasCsvExportContent($StudentInfoArr,$Rating=false)
		{	### Get Subject Info
			$liboea_academic = new liboea_academic();
			$SubjectInfoArr = $liboea_academic->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=1, $OrderbyJupasCode=1);
			$SubjectInfoAssoArr = BuildMultiKeyAssoc($SubjectInfoArr, 'SubjectID');
			unset($SubjectInfoArr);
			
			### Get Student Academic Performance Info
			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			$ApprovedStatus = $this->getItemTypeApprovedStatus($this->getAcademicInternalCode());
			$StudentAcademicAssoArr = BuildMultiKeyAssoc($this->getStudentAcademicArr($StudentIDArr, $ApprovedStatus), 'StudentID');
			$SubjectHeaderarray = array();
			foreach($SubjectInfoAssoArr as $AryElement){
				$SubjectHeaderarray[] = $AryElement['JupasSubjectCode'];
			}
			$ExportContentArr = array();
			$numOfStudent = count((array)$StudentInfoArr);
			$numOfStudentInfoItem = count((array)$this->getJupasCsvCommonHeaderArr());
			$RowCounter = 0;
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$_StudentID = $StudentInfoArr[$i]['UserID'];
				$_StudentAcademicID = $StudentAcademicAssoArr[$_StudentID]['StudentAcademicID'];
				
				$_liboea_academic = new liboea_academic($_StudentAcademicID);
				if($Rating){
					$_SubjectOverallRatingInfoAssoArr = $_liboea_academic->getObjectSubjectOverallRatingInfoArr();
				}else{
					$_SubjectPercentileInfoAssoArr = $_liboea_academic->getObjectSubjectPercentileInfoArr();
				}
				$_StudentSubjectInfoAssoArr = $_liboea_academic->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=1, $CheckStudentHaveScore=1);
				
				
				### One row for each Subject
				$RecordExists = 0;
				$AcademicContentAry = array();
				foreach($SubjectInfoAssoArr as $AryElement){
					$_SubjectID = $AryElement['SubjectID'];
					if(array_key_exists($_SubjectID,$_StudentSubjectInfoAssoArr)&&$_StudentSubjectInfoAssoArr[$_SubjectID]['ApplyToJupas'] == 1){
						if($Rating){
							if(liboea_academic::Get_Academic_Overall_Rating_JupasCode($_SubjectOverallRatingInfoAssoArr[$_SubjectID])){	
								$RecordExists = 1;
							}
							$AcademicContentAry[] = liboea_academic::Get_Academic_Overall_Rating_JupasCode($_SubjectOverallRatingInfoAssoArr[$_SubjectID]);
						}else{
							if(liboea_academic::Get_Academic_Percentile_JupasCode($_SubjectPercentileInfoAssoArr[$_SubjectID])){	
								$RecordExists = 1;
							}
							$AcademicContentAry[] = liboea_academic::Get_Academic_Percentile_JupasCode($_SubjectPercentileInfoAssoArr[$_SubjectID]);
						}
					}else{
						$AcademicContentAry[] = '';
					}
				}
				
				if($RecordExists){
					$ExportStudentInfoAry = array();
					for ($j=0; $j<$numOfStudentInfoItem; $j++)	// k=0 is UserID => skip it
					{
						$ExportStudentInfoAry[] =  $StudentInfoArr[$i][$j+1];
					}
					$AcademicContentAry[] = $Rating? stripslashes($_liboea_academic->getOverallRatingRemark()) : stripslashes($_liboea_academic->getPercentileRemark());
					$ExportContentArr[$RowCounter] = array_merge($ExportStudentInfoAry,$AcademicContentAry);
					$RowCounter++;
				}
			}
			return $this->getJupasCsvExportText($ExportContentArr, array_merge($this->getJupasCsvCommonHeaderArr(),$SubjectHeaderarray,(array)'Remarks'));
		}
		/*
		public function getAcademicJupasCsvExportContent($StudentInfoArr)
		{
			### Get Subject Info
			$liboea_academic = new liboea_academic();
			$SubjectInfoArr = $liboea_academic->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=0, $CheckStudentHaveScore=1);
			$SubjectInfoAssoArr = BuildMultiKeyAssoc($SubjectInfoArr, 'SubjectID');
			unset($SubjectInfoArr);
			
			### Get Student Academic Performance Info
			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			$ApprovedStatus = $this->getItemTypeApprovedStatus($this->getAcademicInternalCode());
			$StudentAcademicAssoArr = BuildMultiKeyAssoc($this->getStudentAcademicArr($StudentIDArr, $ApprovedStatus), 'StudentID');
			
			$ExportContentArr = array();
			$numOfStudent = count((array)$StudentInfoArr);
			$numOfStudentInfoItem = count((array)$this->getJupasCsvCommonHeaderArr());
			$RowCounter = 0;
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$_StudentID = $StudentInfoArr[$i]['UserID'];
				$_StudentAcademicID = $StudentAcademicAssoArr[$_StudentID]['StudentAcademicID'];
				
				$_liboea_academic = new liboea_academic($_StudentAcademicID);
				$_SubjectPercentileInfoAssoArr = $_liboea_academic->getObjectSubjectPercentileInfoArr();
				$_SubjectOverallRatingInfoAssoArr = $_liboea_academic->getObjectSubjectOverallRatingInfoArr();
				$_StudentSubjectInfoAssoArr = $_liboea_academic->getSubjectInfoArr($WithJupasSubjectCodeOnly=1, $RecordStatus=1, $ReturnAsso=1, $CheckStudentHaveScore=1);
				
				
				### One row for each Subject
				foreach ((array)$_SubjectPercentileInfoAssoArr as $_SubjectID => $_SubjectPercentileInternalCode)
				{
					if ($_StudentSubjectInfoAssoArr[$_SubjectID]['ApplyToJupas'] != 1) {
						continue;
					}
					
					$_SubjectCode = $SubjectInfoAssoArr[$_SubjectID]['JupasSubjectCode'];
					$_SubjectOverallRatingInternalCode = $_SubjectOverallRatingInfoAssoArr[$_SubjectID];
					
					for ($j=0; $j<$numOfStudentInfoItem; $j++)	// k=0 is UserID => skip it
					{
						$ExportContentArr[$RowCounter][] =  $StudentInfoArr[$i][$j+1];
					}
					$ExportContentArr[$RowCounter][] =  $_SubjectCode;
					$ExportContentArr[$RowCounter][] =  liboea_academic::Get_Academic_Percentile_JupasCode($_SubjectPercentileInternalCode);
					$ExportContentArr[$RowCounter][] =  liboea_academic::Get_Academic_Overall_Rating_JupasCode($_SubjectOverallRatingInternalCode);
					
					$RowCounter++;
				}
			}
			
			return $this->getJupasCsvExportText($ExportContentArr, $this->getAcademicJupasCsvHeaderArr());
		}*/
		
		public function getSuppInfoJupasCsvExportContent($StudentInfoArr)
		{
			### Get Student Additional Info
			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			$ApprovedStatus = $this->getItemTypeApprovedStatus($this->getSuppInfoInternalCode());
			$StudentSuppInfoAssoArr = BuildMultiKeyAssoc($this->getStudentSuppInfoArr($StudentIDArr, $ApprovedStatus), 'StudentID', $IncludedDBField=array('Details'));
			
			$ExportContentArr = array();
			$numOfStudent = count((array)$StudentInfoArr);
			$numOfStudentInfoItem = count((array)$this->getJupasCsvCommonHeaderArr());
			$RowCounter = 0;
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$_StudentID = $StudentInfoArr[$i]['UserID'];
				//$_StudentSuppInfo = trim($StudentSuppInfoAssoArr[$_StudentID]['Details']);
				$_StudentSuppInfo = $this->standardizeCsvLineBreakData(trim($StudentSuppInfoAssoArr[$_StudentID]['Details']), liboea_supplementary_info::getDetailsMaxLength());
				
				if ($_StudentSuppInfo == '') {
					continue;
				}
				
				for ($j=0; $j<$numOfStudentInfoItem; $j++)	// k=0 is UserID => skip it
				{
					$ExportContentArr[$RowCounter][] =  $StudentInfoArr[$i][$j+1];
				}
				$ExportContentArr[$RowCounter][] =  $_StudentSuppInfo;
				
				$RowCounter++;
			}
			
			return $this->getJupasCsvExportText($ExportContentArr, $this->getSuppInfoJupasCsvHeaderArr());
		}
		
		private function getJupasCsvExportText($ExportContentArr, $ExportHeaderArr)
		{
			global $libexport;
			
			for ($i=0, $i_max=count($ExportContentArr); $i<$i_max; $i++) {
				$_rowContentArr = $ExportContentArr[$i];
				for ($j=0, $j_max=count($_rowContentArr); $j<$j_max; $j++) {
					$ExportContentArr[$i][$j] = $this->standardizeCsvData($ExportContentArr[$i][$j]);
				}
			}
			
			$ExportText = $libexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr, $Delimiter=",", $LineBreak="\r\n", $ColumnDefDelimiter=",", $DataSize=0, $Quoted="11", $includeLineBreak=1);
//			$ExportText = $libexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr, $Delimiter="\t", $LineBreak="\r\n", $ColumnDefDelimiter="\t", $DataSize=0, $Quoted="11", $includeLineBreak=1);
			
			// Copied from libexporttext.php
			$ExportText = mb_convert_encoding($ExportText,'UTF-16LE','UTF-8');
			$ExportText = "\xFF\xFE".$ExportText;
			//$ExportText = "\xEF\xBB\xBF".$ExportText;		// UTF-8 BOM  <- 20130208 testing by Ivan: seems work but not fully tested (user can edit the exported csv by excel if commented previous 2 lines of code and enable this line of code)
			
			return $ExportText;
		}
		
		public function getApprovalStatusRemarksDiv()
		{
			global $Lang, $linterface;
			
			$x = '';
			$x .= '<div>'."\n";
				$x .= $linterface->Get_Pending_Image().' <span style="vertical-align:middle;color:blue;">'.$Lang['iPortfolio']['OEA']['RecordStatus_Pending'].'</span>'."\n";
				$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
				$x .= $linterface->Get_Approved_Image().' <span style="vertical-align:middle;color:green;">'.$Lang['iPortfolio']['OEA']['RecordStatus_Approved'].'</span>'."\n";
				$x .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
				$x .= $linterface->Get_Rejected_Image().' <span style="vertical-align:middle;color:red;">'.$Lang['iPortfolio']['OEA']['RecordStatus_Rejected'].'</span>'."\n";
			$x .= '</div>'."\n";
			
			return $x;
		}
		
		public function getStudentApprovalStatusSelection($ID_Name, $SelectedStatus='', $Onchange='')
		{
			global $Lang;
			
			$selectArr = array();
			$selectArr['-1'] = $Lang['iPortfolio']['OEA']['AllStudents'];
			$selectArr[0] = $Lang['iPortfolio']['OEA']['WaitingForApprovalStudents'];
			
			$onchange = '';
			if ($Onchange != "")
				$onchange = ' onchange="'.$Onchange.'" ';
				
			$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
			
			return getSelectByAssoArray($selectArr, $selectionTags, $SelectedStatus, $isAll=0, $noFirst=1);
		}
		
		public function getYearSelection($Year="", $tag="", $selected="") {
			if($Year=="") $Year = date('Y');
			
			$startYear = $Year - $this->NoOfYearBefore;
			$endYear = $Year + $this->NoOfYearAfter;
			
			$yearAry = array();
			for($i=$startYear; $i<=$endYear; $i++) {
				$yearAry[] = array($i, $i);	
			}
			$yearSelect = getSelectByArray($yearAry, $tag, $selected, 0, 1);
			return $yearSelect;
			
		}
		
		
//		public function Get_Academic_Settings_Map_By_Percentile_Layer_UI()
//		{
//			global $linterface;
//			
//			$x .= '<div class="edit_pop_board" style="height:410px;">';
//				$x .= $linterface->Get_Thickbox_Return_Message_Layer();
//				$x .= '<div id="ThickboxContentDiv" class="edit_pop_board_write" style="height:370px;">';
//					$x .= $this->Get_Academic_Settings_Map_By_Percentile_Layer_Table();
//				$x .= '</div>';
//			$x .= '</div>';
//			
//			return $x;
//		}
//		
//		public function Get_Academic_Settings_Map_By_Percentile_Layer_Table()
//		{
//			global $Lang, $linterface;
//			
//			$AcademicOverallRatingArr = $this->Get_Academic_Overall_Rating_Array();
//			$numOfRating = count($AcademicOverallRatingArr);
//			
//			$SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "", $id="Btn_Save");
//			$CloseBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();");
//		
//			$x .= '<table id="ThickboxContentTable" class="common_table_list_v30">';
//				$x .= '<thead>';
//					$x .= '<tr style="vertical-align:top;">';
//						$x .= '<th>'.$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating'].'</th>'."\n";
//						$x .= '<th>'.$Lang['iPortfolio']['OEA']['AcademicArr']['Percentile'].' ('.$Lang['iPortfolio']['OEA']['AcademicArr']['LowerLimit'].')</th>'."\n";
//					$x .= '</tr>'."\n";
//				$x .= '</thead>';
//				
//				$x .= '<tbody>';
//					for ($i=0; $i<$numOfRating; $i++)
//					{
//						$thisRating = $AcademicOverallRatingArr[$i];
//						$thisRatingDisplay = $this->Get_Academic_Overall_Rating_Name($thisRating);
//						
//						$x .= '<tr>'."\n";
//							$x .= '<td class="field_title">'.$thisRatingDisplay.'</td>'."\n";
//							$x .= '<td>'."\n";
//								$x .= $linterface->GET_TEXTBOX_NUMBER($Id, $Name, $Value, $OtherClass='').' %';
//							$x .= '</td>'."\n";
//						$x .= '</tr>'."\n";
//					}
//				$x .= '</tbody>';
//			$x .= '</table>'."\n";
//			
//			
//			$x .= '<br style="clear:both;" />'."\n";
//			$x .= '<div class="edit_bottom_v30">';
//				$x .= $SaveBtn."\n";
//				$x .= '&nbsp;'."\n";
//				$x .= $CloseBtn."\n";
//			$x .= '</div>';
//			
//			return $x;
//		}
//		
//		public function Get_Academic_Settings_Map_By_Score_Layer_UI()
//		{
//			global $linterface;
//			
//			$x .= '<div class="edit_pop_board" style="height:410px;">';
//				$x .= $linterface->Get_Thickbox_Return_Message_Layer();
//				$x .= '<div id="ThickboxContentDiv" class="edit_pop_board_write" style="height:370px;">';
//					$x .= $this->Get_Academic_Settings_Map_By_Score_Layer_Table();
//				$x .= '</div>';
//			$x .= '</div>';
//			
//			return $x;
//		}
		
		//$percentileToOverAllMapping is an array
		public function Get_Academic_Settings_Overall_Rating_Mapping_Table($percentileToOverAllMapping)
		{
			global $Lang, $linterface,$ec_iPortfolio;
			
			$AcademicOverallRatingArr = liboea_academic::Get_Academic_Overall_Rating_Config_Array();
			$numOfRating = count($AcademicOverallRatingArr);

			$AcademicPercentileArr = liboea_academic::Get_Academic_Percentile_Config_Array();

			$AcademicOverall = array();

			foreach((array)$AcademicOverallRatingArr as $thisOverallRatingInternalCode => $thisOverallRatingInfoArr) {
				$_overallRatingCaption = liboea_academic::Get_Academic_Overall_Rating_Name($thisOverallRatingInternalCode);
				$AcademicOverall[] = array($thisOverallRatingInternalCode,$_overallRatingCaption);
				
			}
		
			$x .= '<table id="ThickboxContentTable" class="common_table_list_v30">';
				$x .= '<col style="width:25%;" />'."\n";
				$x .= '<col style="width:25%;" />'."\n";
				
				$x .= '<thead>';
					$x .= '<tr style="vertical-align:top;">';
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['AcademicArr']['Percentile'].' ('.$Lang['iPortfolio']['OEA']['AcademicArr']['PositionInForm'].')</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating'].'</th>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</thead>';
				
				$x .= '<tbody>';

					foreach((array)$AcademicPercentileArr as $JupasCode => $details) {
						$_percentileExportCode = $details['JupasCode'];
						$_percentileCaption = liboea_academic::Get_Academic_Percentile_Name($JupasCode);
						$_selectedValue = $percentileToOverAllMapping[$JupasCode];
						$thisRatingDisplay = liboea_academic::Get_Academic_Overall_Rating_Name($thisOverallRatingInternalCode);
						
						$x .= '<tr>'."\n";
							$x .= '<td>'.$_percentileCaption.'</td>'."\n";
							$x .= '<td>'."\n";
								$h_selectOverallRating = $linterface->GET_SELECTION_BOX($AcademicOverall, 'name="overAllMapping['.$JupasCode.']" id="overAllMapping_'.$JupasCode.'"', Get_Selection_First_Title($Lang['General']['PleaseSelect']), $_selectedValue);

								$x .= $h_selectOverallRating;

							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			return $x;
		}
		
		public function getOLEMappingImportColumnInfoArray($ShowInUIOnly=0)
		{
			global $oea_cfg;
			
			$ReturnArr = array();
			$ConfigArr = $oea_cfg["Setting"]["OLE_OEA_Mapping_ImportExport_HeaderArr"];
			if ($ShowInUIOnly) {
				$numOfConfig = count($ConfigArr);
				for ($i=0; $i<$numOfConfig; $i++) {
					$thisShowInUI = $ConfigArr[$i]['ShowInUI'];
					if ($thisShowInUI==1) {
						$ReturnArr[] = $ConfigArr[$i];
					}
				}
			}
			else {
				$ReturnArr = $ConfigArr;
			}
			
			return $ReturnArr;
		}
		
		public function getOLEMappingImportColumnTitleArray($ShowInUIOnly=0)
		{
			global $oea_cfg;
			
			$ConfigArr = $this->getOLEMappingImportColumnInfoArray($ShowInUIOnly);
			
			$ColumnTitleArr = array();
			$ColumnTitleArr['En'] = Get_Array_By_Key($ConfigArr, 'En');
			$ColumnTitleArr['Ch'] = Get_Array_By_Key($ConfigArr, 'Ch');

			return $ColumnTitleArr;
		}
		
		public function getOLEMappingImportColumnPropertyArray($forGetAllCsvContent=0, $ShowInUIOnly=0)
		{
			global $oea_cfg;
			$ConfigArr = $this->getOLEMappingImportColumnInfoArray($ShowInUIOnly);
			$PropertyArr = Get_Array_By_Key($ConfigArr, 'Property');
			
			if ($forGetAllCsvContent == 1)
			{
				$numOfProperty = count($PropertyArr);
				for ($i=0; $i<$numOfProperty; $i++) {
					$PropertyArr[$i] = 1;
				}
			}
			
			return $PropertyArr;
		}
		
		public function getJupasOeaItemInfoXlsFilePath()
		{
			global $oea_cfg;
			return $oea_cfg["OEA_JUPAS_Info_FilePath"];
		}
		
		public function Get_OLE_Program_OEA_Mapping_Info_Array($OLENatureArr='', $MappingStatusArr='', $OLE_ProgramIDArr='', $ForListing=0, $Keyword='', $AcademicYearIDArr='', $ProgramNatureArr='', $FormIDArr='')
		{
			global $intranet_db, $ipf_cfg, $oea_cfg;

			$liboea_setting = new liboea_setting();
			
			$OLE_TitleNameField = Get_Lang_Selection("If (ole.TitleChi Is Not Null And ole.TitleChi != '', ole.TitleChi, ole.Title)", "If (ole.Title Is Not Null And ole.Title != '', ole.Title, ole.TitleChi)");
			//$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN');
			$AcademicYearNameField = ($ForListing)? Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN') : 'ay.YearNameEN';
			
			if ($OLENatureArr != '') {
				$conds_IntExt = " And ole.IntExt In ('".implode("', '", (array)$OLENatureArr)."') ";
			}
			
			if ($MappingStatusArr != '') {
				$having_MappingStatus = " Having OLE_MappingStatus In (".implode(", ", (array)$MappingStatusArr).") ";
			}
			
			if ($OLE_ProgramIDArr != '') {
				$conds_OLE_ProgramID = " And ole.ProgramID In (".implode(", ", (array)$OLE_ProgramIDArr).") ";
			}
			
			if ($Keyword != '') {
				$conds_Keyword = " And (
											$OLE_TitleNameField Like '%".$Keyword."%'
											Or $OLE_TitleNameField Like '%".$Keyword."%'
											Or oleOeaMap.OEA_ProgramCode Like '%".$Keyword."%'
										)";
			}
			
			if ($AcademicYearIDArr != '') {
				$conds_AcademicYearID = " And ole.AcademicYearID In (".implode(',', (array)$AcademicYearIDArr).") ";
			}
			
			if ($ProgramNatureArr != '') {
				$conds_ProgramNature = " And ole.IntExt In ('".implode("','", (array)$ProgramNatureArr)."') ";
			}
			if ($FormIDArr != '') {
				$libpf_ole = new libpf_ole();
				$ProgramInfoArr = $libpf_ole->Get_OLE_Info($FormIDArr);
				$ProgramIDArr = Get_Array_By_Key($ProgramInfoArr, 'ProgramID');
				$conds_FormStudentProgramID = " And ole.ProgramID In ('".implode("','",(array)$ProgramIDArr)."') ";
				
				$libfcm = new form_class_manage();
				$StudentInfoArr = $libfcm->Get_Student_By_Form(Get_Current_Academic_Year_ID(), $FormIDArr);
				$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			}
			
			
			$OLE_PROGRAM = $this->Get_Table_Name('OLE_PROGRAM');
			$OLE_STUDENT = $this->Get_Table_Name('OLE_STUDENT');
			$OEA_OLE_MAPPING = $this->Get_Table_Name('OEA_OLE_MAPPING');
			$ACADEMIC_YEAR = $this->Get_Table_Name('ACADEMIC_YEAR', $intranet_db);
			$sql = "Select
							ole.ProgramID as OLE_ProgramID,
							$AcademicYearNameField as OLE_AcademicYear,
							$OLE_TitleNameField as OLE_ProgramName,
							Count(os.RecordID) as OLE_NumOfStudentJoined,
							If (ole.IntExt = 'INT', 'S', 'P') as OLE_ProgramNature,
							oleOeaMap.OEA_ProgramCode,
							oleOeaMap.DefaultAwardBearing as OEA_DefaultAwardBearing,
							If (oleOeaMap.OEA_ProgramCode Is Null, 0, 1) as OLE_MappingStatus
					From
							$OLE_PROGRAM as ole
							Left Outer Join
							$OEA_OLE_MAPPING as oleOeaMap On (ole.ProgramID = oleOeaMap.OLE_ProgramID)
							Left Outer Join
							$ACADEMIC_YEAR as ay On (ole.AcademicYearID = ay.AcademicYearID)
							Left Outer Join
							$OLE_STUDENT as os On (ole.ProgramID = os.ProgramID And os.UserID In ('".implode("','", (array)$StudentIDArr)."') And os.RecordStatus In ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."', '".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."'))
					Where
							1
							$conds_IntExt
							$conds_OLE_ProgramID
							$conds_FormStudentProgramID
							$conds_Keyword
							$conds_AcademicYearID
							$conds_ProgramNature
					Group By
							ole.ProgramID
					$having_MappingStatus
					Order By
							OLE_ProgramName
					";
			$OLEInfoArr = $this->returnArray($sql);
			$numOfOLE = count($OLEInfoArr);
			
			
			### Get the OEA Program Name
			$OEAInfoArr = $liboea_setting->get_OEA_Item();
			for ($i=0; $i<$numOfOLE; $i++)
			{
				$_OEA_ProgramCode = $OLEInfoArr[$i]['OEA_ProgramCode'];
				$OLEInfoArr[$i]['OEA_ProgramName'] = $OEAInfoArr[$_OEA_ProgramCode]['ItemName'];
				
				if(($this->Standardize_OEA_Program_Code($_OEA_ProgramCode, 1) == $oea_cfg["OEA_Default_Category_Others"])
					&& (trim($OLEInfoArr[$i]['OLE_ProgramName']) != '') ) {
						//IF OLE PROGRAM IS MAPPED WITH 00000 AND OLE PROGRAM TITLE NOT EMPTY, SET THE DISPLAY OF THE OEA PROGRAM NAME EQUAL TO THE OLE PROGRAM NAME
						$OLEInfoArr[$i]['OEA_ProgramName'] .= " (".$OLEInfoArr[$i]['OLE_ProgramName'].")";					
				}
				
				if ($ForListing == 1)
				{
					# Return Language (instead of Code) of Program Nature and Award Bearing
					$OLEInfoArr[$i]['OLE_ProgramNature'] = $this->getParticipationTypeDisplayByCode($OLEInfoArr[$i]['OLE_ProgramNature']);
					$OLEInfoArr[$i]['OEA_DefaultAwardBearing'] = $this->getAwardBearingDisplayByCode($OLEInfoArr[$i]['OEA_DefaultAwardBearing']);
					
					# Show '---' for empty data
					$OLEInfoArr[$i]['OLE_AcademicYear'] = Get_Standardized_Table_Data_Display($OLEInfoArr[$i]['OLE_AcademicYear']);
					$OLEInfoArr[$i]['OLE_ProgramNature'] = Get_Standardized_Table_Data_Display($OLEInfoArr[$i]['OLE_ProgramNature']);
					$OLEInfoArr[$i]['OEA_ProgramCode'] = Get_Standardized_Table_Data_Display($OLEInfoArr[$i]['OEA_ProgramCode']);
					$OLEInfoArr[$i]['OEA_ProgramName'] = Get_Standardized_Table_Data_Display($OLEInfoArr[$i]['OEA_ProgramName']);
					$OLEInfoArr[$i]['OEA_DefaultAwardBearing'] = Get_Standardized_Table_Data_Display($OLEInfoArr[$i]['OEA_DefaultAwardBearing']);
				}
			}
			
			return $OLEInfoArr;
		}
		
		
		public function Remove_OLE_OEA_Mapping($OLE_ProgramID)
		{
			if ($OLE_ProgramID=="" || $OLE_ProgramID<=0)
			{
				return false;
			}
			
			$OEA_OLE_MAPPING = $this->Get_Table_Name('OEA_OLE_MAPPING');
			$sql = "DELETE FROM ".$OEA_OLE_MAPPING." Where OLE_ProgramID = '".$OLE_ProgramID."' ";
			
			$success = $this->db_db_query($sql);
						
			return $success;
		}
		
		
		
		public function Insert_OLE_OEA_Mapping($DataArr)
		{
			if (count($DataArr) == 0)
				return false;
				
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = "'".$this->Get_Safe_Sql_Query($value)."'";
			}
						
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$OEA_OLE_MAPPING = $this->Get_Table_Name('OEA_OLE_MAPPING');
			$sql = "Insert Into $OEA_OLE_MAPPING
						( $fieldText )
					Values
						( $valueText )
					";
			$success = $this->db_db_query($sql);
			
			if ($success == false) 
				$this->RollBack_Trans();
			else
				$this->Commit_Trans();
			
			return $success;
		}
		
		public function Update_OLE_OEA_Mapping($OLE_ProgramID, $DataArr)
		{
			if (count($DataArr) == 0)
				return false;
				
			# Build field update values string
			$valueFieldArr = '';
			foreach ($DataArr as $field => $value)
			{
				$valueFieldArr[] = $field." = '".$this->Get_Safe_Sql_Query($value)."'";
			}
			$valueFieldText = implode(',', $valueFieldArr);
			
			$OEA_OLE_MAPPING = $this->Get_Table_Name('OEA_OLE_MAPPING');
			$sql = "Update ".$OEA_OLE_MAPPING."
						Set ".$valueFieldText."
					Where 
						OLE_ProgramID = '".$OLE_ProgramID."'
					";
			$success = $this->db_db_query($sql);
			
			return $success;
		}
		
		public function Update_OLE_OEA_Mapping_Student_Mapped_OEA($OLE_ProgramID) {
			global $oea_cfg;
			
			$mappedOeaInfoArr = $this->Get_OLE_Program_OEA_Mapping_Info_Array($OLENatureArr='', $MappingStatusArr='', $OLE_ProgramID);
			$oleProgramName = $mappedOeaInfoArr[0]['OLE_ProgramName'];
			$mappedOeaProgramCode = $mappedOeaInfoArr[0]['OEA_ProgramCode'];
			$mappedOeaAwardBearing = $mappedOeaInfoArr[0]['OEA_DefaultAwardBearing'];
			
			if ($mappedOeaProgramCode == $oea_cfg["OEA_Default_Category_Others"]) {
				$mappedOeaProgramName = $oleProgramName;
			}
			else {
				$liboea_setting = new liboea_setting();
				
				$oeaProgramInfoArr = $liboea_setting->get_OEA_Item($mappedOeaProgramCode);
				$mappedOeaProgramName = $oeaProgramInfoArr[$mappedOeaProgramCode]['ItemName'];
			}
			
			
			$mappedOeaInfoArr = $this->getStudentOeaItemInfoArr($StudentIDArr='', $RecordStatusArr='', $ParticipationArr='', $ParRecordIDArr='', $ParExcludeRecordIDArr='', $OLE_ProgramID);
			$numOfMappedOea = count($mappedOeaInfoArr);
			
			$SuccessArr = array();
			for ($i=0; $i<$numOfMappedOea; $i++) {
				$_oeaRecordID = $mappedOeaInfoArr[$i]['RecordID'];
				
				$_objOeaStudent = new liboea_item($_oeaRecordID);
				$_objOeaStudent->setTempOEA_ProgramCode($mappedOeaProgramCode);
				$_objOeaStudent->setTempTitle($mappedOeaProgramName);
				$_objOeaStudent->setTempStartYear($_objOeaStudent->getStartYear());
				$_objOeaStudent->setTempEndYear($_objOeaStudent->getEndYear());
				//$_objOeaStudent->setTempAwardBearing($mappedOeaAwardBearing);		// Don't override award bearing now. Otherwise, the student may have record NOT award bearing but "By Competition" 
				$_objOeaStudent->setTempAwardBearing($_objOeaStudent->getOEA_AwardBearing());
				$_objOeaStudent->setTempParticipation($_objOeaStudent->getParticipation());
				$_objOeaStudent->setTempRole($_objOeaStudent->getRole());
				$_objOeaStudent->setTempParticipationNature($_objOeaStudent->getParticipationNature());
				$_objOeaStudent->setTempAchievement($_objOeaStudent->getAchievement());
				$_objOeaStudent->setTempDescription($_objOeaStudent->getDescription());
				$_objOeaStudent->setTempRecordStatus($_objOeaStudent->getRecordStatus());
				$_success = $_objOeaStudent->save();
				
				$SuccessArr[] = ($_success)? true : false;
			}
			
			return (in_array(false, (array)$SuccessArr))? false : true;
		}
				
		public function saveSetting() 
		{
			global $oea_cfg, $UserID;
			
			$settingName = $oea_cfg["Setting"]["ItemAry"];
			$OEA_SETTING = $this->Get_Table_Name('OEA_SETTING');
			$result = array();
			
			for($i=0, $i_max=sizeof($settingName); $i<$i_max; $i++) {
				$_tempSettingName = $settingName[$i];
				$sql = "SELECT SettingID FROM $OEA_SETTING WHERE SettingName='$_tempSettingName'";
				$_tempResult = $this->returnVector($sql);

				# check insert / update setting
				if(sizeof($_tempResult)>0) {		# setting exist, then update 
					$sql = "UPDATE $OEA_SETTING SET SettingValue='".$this->$_tempSettingName."', DateModified=NOW(), ModifyBy='$UserID' WHERE SettingName='$_tempSettingName'";

				} else {							# setting not exist, then add 
					$sql = "INSERT INTO $OEA_SETTING (SettingName, SettingValue, DateInput, InputBy, DateModified, ModifyBy) VALUES ('".$_tempSettingName."', '".$this->$_tempSettingName."', NOW(), '$UserID', NOW(), '$UserID')";

				}
				$result[] = $this->db_db_query($sql);
			}
			return $result;	
		}
		
		function Delete_Import_OLE_OEA_Mapping_Temp_Data()
		{
			$OEA_OLE_MAPPING_IMPORT_TEMP = $this->Get_Table_Name('OEA_OLE_MAPPING_IMPORT_TEMP');
			$sql = "Delete From $OEA_OLE_MAPPING_IMPORT_TEMP Where ImportUserID = '".$_SESSION["UserID"]."'";
			$Success = $this->db_db_query($sql);
					
			return $Success;
		}
		
		public function Insert_Import_OLE_OEA_Mapping_Temp_Data($InsertValueArr)
		{
			if (count((array)$InsertValueArr) > 0)
			{
				$FieldArr = Get_Array_By_Key($this->getOLEMappingImportColumnInfoArray(), 'DBField');
				$FieldList = implode(', ', $FieldArr);
				
				$InsertList = implode(',', (array)$InsertValueArr);
		
				$OEA_OLE_MAPPING_IMPORT_TEMP = $this->Get_Table_Name('OEA_OLE_MAPPING_IMPORT_TEMP');
				$sql = "Insert Into $OEA_OLE_MAPPING_IMPORT_TEMP
							( ImportUserID, RowNumber, $FieldList, DateInput )
						Values
							$InsertList
						";
				return $this->db_db_query($sql);
			}
			else
			{
				return false;
			}
		}
		
		public function Get_Import_OLE_OEA_Mapping_Temp_Data($RowNumberArr='')
		{
			$conds_RowNumber = '';
			if ($RowNumberArr != '') {
				$conds_RowNumber = " And RowNumber In (".implode(',', (array)$RowNumberArr).") ";
			}
			
			$OEA_OLE_MAPPING_IMPORT_TEMP = $this->Get_Table_Name('OEA_OLE_MAPPING_IMPORT_TEMP');
			$sql = "Select * From $OEA_OLE_MAPPING_IMPORT_TEMP Where ImportUserID = '".$_SESSION["UserID"]."' $conds_RowNumber";
			return $this->returnArray($sql);
		}
		
		public function isSubmissionPeriod($userType="")
		{
			$period = $this->StudentSubmissionPeriod;
			$today = (date("Y-m-d H:i:s"));
			$periodAry = explode("#", $period);
			
			if($period=="") {
				//echo 1;
				$this->AllowSubmissionPeriod = 0;
				return false;
			} else {
				if($today>=($periodAry[0]) && ($today<=$periodAry[1])) {
					//echo 2;
					$this->AllowSubmissionPeriod = 1;
					return true;	
				} else {
					//echo 3;
					$this->AllowSubmissionPeriod = 0;
					return false;	
				}	
			}
		}
		
		public static function getParticipationTypeDisplayByCode($Code)
		{
			global $oea_cfg;
			return $oea_cfg["OEA_Participation_Type"][strtoupper($Code)];
		}
		
		public static function getAwardBearingDisplayByCode($Code)
		{
			global $oea_cfg;
			return $oea_cfg["OEA_AwardBearing_Type"][strtoupper($Code)];
		}
		
		public function Check_If_OLE_OEA_Mapping_Exist($OLE_ProgramID) 
		{
			$InfoArr = $this->Get_OLE_Program_OEA_Mapping_Info_Array($OLENatureArr='', $MappingStatusArr='', $OLE_ProgramID);
			return ($InfoArr[0]['OEA_ProgramCode'] == '')? false : true;
		}
		
		public function Get_Settings_OLE_Mapping_Index_DBTable($field, $order, $pageNo, $ForExport=0, $Keyword='', $AcademicYearID='', $ProgramNature='', $MappingStatus='')
		{
			global $Lang, $page_size, $ck_oea_settings_ole_mapping_page_size;
			
			$field = ($field=="")? 0 : $field;
			$order = ($order=="")? 1 : $order;
			$pageNo = ($pageNo=="")? 0 : $pageNo;
			if (isset($ck_oea_settings_ole_mapping_page_size) && $ck_oea_settings_ole_mapping_page_size != "") 
				$page_size = $ck_oea_settings_ole_mapping_page_size;
						
			$li = new libdbtable2007($field, $order, $pageNo);
			
			
			# Table Content
			$TargetMappingStatus = '';
			if ($MappingStatus == 'Mapped') {
				$TargetMappingStatus = '1';
			}
			else if ($MappingStatus == 'NotMapped') {
				$TargetMappingStatus = '0';
			}
			$li->sql = $this->Get_Settings_OLE_Mapping_Index_Sql($Keyword, $AcademicYearID, $ProgramNature, $TargetMappingStatus, $ForExport);


			$ColumnInfoArr = $this->getOLEMappingImportColumnInfoArray($ShowInUIOnly=1);

			array_shift($ColumnInfoArr);	// remove OLE_ProgramID in the display

			$li->field_array = Get_Array_By_Key($ColumnInfoArr, 'DBField');

			$x = '';
			if ($ForExport) {
				$ExportSQL = Get_Export_SQL_From_DB_Table_SQL($li->built_sql());
				$x = $this->returnArray($ExportSQL);
			}
			else {
				# Table Column
				$ColumnTitleArr = $this->getOLEMappingImportColumnTitleArray($ShowInUIOnly=1);

				$ColumnTitleArr = Get_Lang_Selection($ColumnTitleArr['Ch'], $ColumnTitleArr['En']);
				$numOfTitle = count($ColumnTitleArr);
				$pos = 0;
				
					$li->column_list .= "<th rowspan='2'>#</th>";
					$li->column_list .= "<th colspan='3'>".$Lang['iPortfolio']['OEA']['OLE_Name2']."</th>";
					$li->column_list .= "<th colspan='3' class='sub_row_top'>".$Lang['iPortfolio']['OEA']['OEA_Name']."</th>";
				$li->column_list .= "</tr>";
				$li->column_list .= "<tr>";
					for ($i=0; $i<$numOfTitle; $i++) {
						
						if ($i == 0) {	// hide OLE_ProgramID
							continue;
						}
						
						$thisTitle = $ColumnTitleArr[$i];
						$thisTitle = substr($thisTitle, 4, strlen($thisTitle));
						
						$thisWidth = ($i==1 || $i==5)? 30 : 10;		// widden for the Title row
						
						$thisClass = '';
						if ($i > $numOfTitle - 4) {		// last 3 is OEA info => different background
							$thisClass = "class='sub_row_top'";
						}
						
						$li->column_list .= "<th ".$thisClass." width='".$thisWidth."%' nowrap>".$li->column_IP25($pos++, $thisTitle)."</th>\n";
					}
				
				$li->no_col = $numOfTitle + 1 - 1;	// +1 means the "#" row, -1 means hide the "OLE_ProgramID"
				$li->IsColOff = 'IP25_table';
				
				//debug_pr($li->field_array);
//				debug_r($li->built_sql());
//				debug_pr($li->returnArray($li->built_sql()));
			
				$x .= $li->display();
				$x .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$li->pageNo.'" />';
				$x .= '<input type="hidden" id="order" name="order" value="'.$li->order.'" />';
				$x .= '<input type="hidden" id="field" name="field" value="'.$li->field.'" />';
				$x .= '<input type="hidden" id="page_size_change" name="page_size_change" value="" />';
				$x .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$li->page_size.'" />';
			}
			return $x;
		}
		
		public function Get_Settings_OLE_Mapping_Index_Sql($Keyword, $AcademicYearID, $ProgramNature, $MappingStatus, $ForExport=0)
		{
			global $Lang,$oea_cfg, $linterface;
			
			$libfcm = new form_class_manage();
			
			### Create Temp Table
			$OEA_OLE_MAPPING_INDEX_DBTABLE_TEMP = $this->Get_Table_Name('OEA_OLE_MAPPING_INDEX_DBTABLE_TEMP');
			$sql = "Create Temporary Table If Not Exists $OEA_OLE_MAPPING_INDEX_DBTABLE_TEMP (
						RecordID Int(8) NOT NULL AUTO_INCREMENT,
						OLE_ProgramID Int(11) Default Null,
						OLE_ProgramName text Default Null,
						OLE_AcademicYear varchar(255) Default Null,
						OLE_ProgramNature varchar(255) Default Null,
						OEA_ProgramCode varchar(64) Default Null,
						OEA_ProgramName text Default Null,
						OEA_AwardBearing varchar(255) Default Null,
						PRIMARY KEY (RecordID),
						KEY OLE_AcademicYear (OLE_AcademicYear),
						KEY OEA_ProgramCode (OEA_ProgramCode),
						KEY OEA_AwardBearing (OEA_AwardBearing),
						KEY OLE_ProgramNature (OLE_ProgramNature)
					) ENGINE=InnoDB Charset=utf8";
			$SuccessArr['CreateTempTable'] = $this->db_db_query($sql);

									
			// Do filtering here so that less records are required to insert to the temp table
			// Not do Keyword filtering here as the function cannot filter the OEA_ProgramName
			$ForListing = ($ForExport)? 0 : 1;
			
			$FormInfoArr = $libfcm->Get_Form_List($GetMappedClassInfo=true, $ActiveOnly=1, Get_Current_Academic_Year_ID(), $YearIDArr='');
			$FormIDArr = Get_Array_By_Key($FormInfoArr, 'YearID');
			$InfoArr = $this->Get_OLE_Program_OEA_Mapping_Info_Array($OLENatureArr='', $MappingStatus, $OLE_ProgramIDArr='', $ForListing, '', $AcademicYearID, $ProgramNature, $FormIDArr);
			$numOfItem = count($InfoArr);
			
			
			$InsertArr = array();
			for ($i=0; $i<$numOfItem; $i++)	{
				$_OLE_ProgramID = $InfoArr[$i]['OLE_ProgramID'];
				
				$_OLE_ProgramName = $InfoArr[$i]['OLE_ProgramName'];
				$_OLE_ProgramNameDisplay = '';
				$_OLE_ProgramNameDisplay .= '<span style="float:left;">'.$_OLE_ProgramName.'</span>';
				$_OLE_ProgramNameDisplay .= '<span style="float:left;">&nbsp;&nbsp;</span>';
				$_OLE_ProgramNameDisplay .= $linterface->Get_Thickbox_Div(500, 750, 'edit_dim', $Lang['Btn']['Edit'], "js_edit_MappingAction('$_OLE_ProgramID');", $InlineID="FakeLayer", $Content="", $DivID='', $LinkID='', $ExtraTags='');
				//$_OLE_ProgramNameDisplay .= '<span class="table_row_tool"><a id="item'.$_OLE_ProgramID.'" href="#TB_inline?height=500&width=750&inlineId=FakeLayer" class="thickbox" title="'.$Lang['Btn']['Edit'].'" onclick="js_edit_MappingAction(\''.$_OLE_ProgramID.'\');"> ['.$Lang['Btn']['Edit'].']</a></span>';
				$_OLE_ProgramNameDisplay = $this->Get_Safe_Sql_Query($_OLE_ProgramNameDisplay);
				
				$_OLE_AcademicYear = $this->Get_Safe_Sql_Query($InfoArr[$i]['OLE_AcademicYear']);
				$_OLE_ProgramNature = $this->Get_Safe_Sql_Query($InfoArr[$i]['OLE_ProgramNature']);
				$_OEA_ProgramCode = $this->Get_Safe_Sql_Query($this->Standardize_OEA_Program_Code($InfoArr[$i]['OEA_ProgramCode'], $KeepEmptyCode=1));

				$_OEA_NAME = $InfoArr[$i]['OEA_ProgramName'];

//				if(($this->Standardize_OEA_Program_Code($InfoArr[$i]['OEA_ProgramCode'],1) == $oea_cfg["OEA_Default_Category_Others"])
//					&& (trim($InfoArr[$i]['OLE_ProgramName']) != '') ) {					
//						//IF OLE PROGRAM IS MAPPED WITH 00000 AND OLE PROGRAM TITLE NOT EMPTY, SET THE DISPLAY OF THE OEA PROGRAM NAME EQUAL TO THE OLE PROGRAM NAME
//						$_OEA_NAME .= ($ForExport==1)? " (".$InfoArr[$i]['OLE_ProgramName'].")" : "&nbsp;&nbsp;(".$InfoArr[$i]['OLE_ProgramName'].")";					
//				}

				$_OEA_ProgramName = $this->Get_Safe_Sql_Query($_OEA_NAME);

				$_OEA_AwardBearing = $this->Get_Safe_Sql_Query($InfoArr[$i]['OEA_DefaultAwardBearing']);
				
				$InsertArr[] = " ('".$_OLE_ProgramID."', '".$_OLE_ProgramNameDisplay."', '".$_OLE_AcademicYear."', '".$_OLE_ProgramNature."', '".$_OEA_ProgramCode."', '".$_OEA_ProgramName."', '".$_OEA_AwardBearing."') ";
			}
			
			$DBFieldArr = Get_Array_By_Key($this->getOLEMappingImportColumnInfoArray($ShowInUIOnly=1), 'DBField');
			$numOfData = count($InsertArr);
			if ($numOfData > 0) {
				$numOfChunk = ceil($numOfData / 1000);
				$InsertArrPiece = array_chunk($InsertArr, $numOfChunk);
				$numOfPiece = count($InsertArrPiece);
				for ($i=0; $i<$numOfPiece; $i++) {
					$sql = "Insert Into $OEA_OLE_MAPPING_INDEX_DBTABLE_TEMP
								(".implode(',', (array)$DBFieldArr).")
								Values
								".implode(',', (array)$InsertArrPiece[$i])."
							";
					$SuccessArr['InsertTempData'][] = $this->db_db_query($sql);
				}
			}
			
			
			if ($Keyword != '') {
				$conds_Keyword = " And (
											OLE_ProgramName Like '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
											Or OEA_ProgramName Like '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
											Or OEA_ProgramCode Like '%".$this->Get_Safe_Sql_Like_Query($Keyword)."%'
										)";
			}
			
			
			if ($ForExport == 0) {
				array_shift($DBFieldArr);	// Do not display OLE_ProgramID in UI
			}

			// testing area
//			for($i = 0,$i_max = count($DBFieldArr);$i < $i_max; $i++){
//				$_title = $DBFieldArr[$i];
//				if($_title == 'OLE_ProgramName'){
//					$DBFieldArr[$i] = 'concat(OLE_ProgramName, \'<a id = "item123" href="#TB_inline?height=500&width=750&inlineId=FakeLayer" class="edit_dim thickbox" title="Edit" onclick="js_edit_MappingAction(\',OLE_ProgramID,\')">[Edit]</a>\') as `OLE_ProgramName`';
//				}
//			}

			// testing area
			$sql = "Select
							".implode(',', (array)$DBFieldArr)."
					From
							$OEA_OLE_MAPPING_INDEX_DBTABLE_TEMP
					Where
							1
							$conds_Keyword		
					";

			return $sql;
		}
		
		public function Standardize_OEA_Program_Code($ParCode, $KeepEmptyCode=0)
		{
			global $Lang;
			
			if ($ParCode == $Lang['General']['EmptySymbol']) {
				$Code = $ParCode;
			}
			else if ($ParCode == '' && $KeepEmptyCode == true) {
				$Code = $ParCode;
			}
			else {
				$Code = str_pad($ParCode, 5, "0", STR_PAD_LEFT);
			}	
			
			return $Code;
		}
		
		public function Get_OLE_Nature_Selection($ID_Name, $Nature, $OnChange='')
		{
			global $Lang;
			
			$SelectArr = array();
			$SelectArr['INT'] = $Lang['iPortfolio']['OEA']['OLE_Int'];
			$SelectArr['EXT'] = $Lang['iPortfolio']['OEA']['OLE_Ext'];
			
			$onchange = "";
			if ($OnChange != "")
				$onchange = ' onchange="'.$OnChange.'" ';
				
			$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
			$firstTitle = Get_Selection_First_Title($Lang['iPortfolio']['OEA']['AllProgramNature']);
			
			return getSelectByAssoArray($SelectArr, $selectionTags, $Nature, $isAll=1, $noFirst=0, $firstTitle);
		}
		
		public function Get_Mapping_Status_Selection($ID_Name, $Nature, $OnChange='')
		{
			global $Lang;
			
			$SelectArr = array();
			$SelectArr['NotMapped'] = $Lang['iPortfolio']['OEA']['NotMapped'];
			$SelectArr['Mapped'] = $Lang['iPortfolio']['OEA']['Mapped'];
			
			$onchange = "";
			if ($OnChange != "")
				$onchange = ' onchange="'.$OnChange.'" ';
				
			$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
			$firstTitle = Get_Selection_First_Title($Lang['iPortfolio']['OEA']['AllMappingStatus']);
			
			return getSelectByAssoArray($SelectArr, $selectionTags, $Nature, $isAll=1, $noFirst=0, $firstTitle);
		}
		
		public static function Get_Word_Count_Div($WordCountSpanID, $MaxLength=0)
		{
			global $Lang, $oea_cfg;
			
			$content = '<div class="tabletextremark">'.$Lang['iPortfolio']['OEA']['WordCount'].': <span id="'.$WordCountSpanID.'"></span> ';
			if($MaxLength>0){
				$content .= '/ '.$MaxLength.' ';
			}
			$content .= $Lang['iPortfolio']['OEA']['Word(s)'].' ('.$Lang['iPortfolio']['OEA']['JupasWordCountRemarks'].')</div>';
			
			return $content;
		}
		
		public static function Get_Character_Count_Div($CountSpanID, $MaxLength)	{
			global $Lang, $oea_cfg;
			
			return '<div class="tabletextremark">'.$Lang['iPortfolio']['OEA']['CharacterCount'].': <span id="'.$CountSpanID.'"></span> / '.$MaxLength.' ('.$Lang['iPortfolio']['OEA']['ChineseCharacterRemarks'].')</div>';
		}
		
		
		public function Get_Jupas_Basic_Settings_View_Mode_Table() {
			global $Lang, $linterface, $oea_cfg;
			
			### School Code
			$SchoolCode = $this->getSchoolCode();
			$SchoolCodeDisplay = ($SchoolCode=='')? $Lang['General']['EmptySymbol'] : $SchoolCode;
			
			### Module in-use
			$ModuleArr = $this->Get_Module_In_Use_Array();
			$numOfModule = count($ModuleArr);
			$ModuleDisplayArr = array();
			for ($i=0; $i<$numOfModule; $i++) {
				$thisModuleCode = $ModuleArr[$i];
				$ModuleDisplayArr[] = $this->Get_Module_Name($thisModuleCode);
			}
			$ModuleDisplay = implode('<br />', $ModuleDisplayArr);
			
			### OEA and Additional Information are not used by JUPAS edited on 13Nov2012
			$RemarkMsgTable = '';
			$ModuleDetected = array();
			if(in_array($oea_cfg["JupasItemInternalCode"]["OeaItem"],$ModuleArr)){
				$ModuleDetected[] = $this->Get_Module_Name($oea_cfg["JupasItemInternalCode"]["OeaItem"]);
			}
			if(in_array($oea_cfg["JupasItemInternalCode"]["AddiInfo"],$ModuleArr)){
				$ModuleDetected[] = $this->Get_Module_Name($oea_cfg["JupasItemInternalCode"]["AddiInfo"]);
			}
			if($ModuleDetected){
				//$WarningMsg = implode(' '.$Lang['iPortfolio']['JUPAS']['and'].' ',$ModuleDetected).' '.$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'];
				//$RemarkMsgTable = $linterface->Get_Warning_Message_Box('<span style="color:red">'.$Lang['General']['Remark'].'</span>', $WarningMsg);
				$RemarkMsgTable = $linterface->Get_Warning_Message_Box('<span style="color:red">'.$Lang['General']['Remark'].'</span>', $Lang['iPortfolio']['JUPAS']['RemarksOEA']);
			}
			
			### Applicable Form
			$FormNameArr = $this->getJupasApplicableFormNameArr();
			//$FormNameDisplay = (count((array)$FormNameArr) == 0)? '<span class="Warningtable">'.$Lang['iPortfolio']['OEA']['WarningArr']['NoApplicableFormSettings'].'</span>' : implode('<br />', (array)$FormNameArr);
			$FormNameDisplay = (count((array)$FormNameArr) == 0)? $Lang['General']['EmptySymbol'] : implode('<br />', (array)$FormNameArr);
			### Button
			$EditBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", 'javascript:js_Edit_Settings();', $id="Btn_Edit");
			
						
			$x = '';	
			$x .= $RemarkMsgTable;
			//$x .= '<div class="table_row_tool row_content_tool">'.$EditBtn.'</div>'."\n";
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['iPortfolio']['OEA']['SchoolCode'].'</td>'."\n";
					$x .= '<td>'.$SchoolCodeDisplay.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['iPortfolio']['OEA']['ModulesInUse'].'</td>'."\n";
					$x .= '<td>'.$ModuleDisplay.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['iPortfolio']['OEA']['ApplicableForms'].'</td>'."\n";
					$x .= '<td>'.$FormNameDisplay.'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br />'."\n";
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $EditBtn."\n";
			$x .= '</div>'."\n";
			
			return $x;
		}
		
		public function Get_Jupas_Basic_Settings_Edit_Mode_Table() {
			global $Lang, $linterface, $oea_cfg;
			
			$libfcm_ui = new form_class_manage_ui();
			
			### School Code
			$SchoolCode = $this->getSchoolCode();
			$SchoolCodeTb = '<input type="textbox" class="textboxtext" id="SchoolCodeTb" name="SchoolCode" value="'.intranet_htmlspecialchars($SchoolCode).'" />';
			
			### Module in-use
			$ModuleCheckboxes = $this->Get_Module_In_Use_Checkboxes_Table();
			
			### Applicable Form
			$FormIDArr = $this->getJupasApplicableFormIDArr();
			$FormCheckboxesTable = $this->Get_Form_Checkboxes_Table($FormIDArr);
			
			
			### Buttons
			$SaveBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", "js_Save_Settings();", $id="Btn_Save");
			$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Reload_Table('View');");
			
			
			// Empty Subject Code Warning
			$thisDivID = 'SchoolCodeEmptyWarningMsgDiv';
			$thisClass = 'WarningMsgDiv';
			$thisSchoolCodeEmptyWarningMsg = $linterface->Get_Form_Warning_Msg($thisDivID, $Lang['iPortfolio']['OEA']['SchoolCodeEmpty'], $thisClass);
			
			// Empty Applicable Form Warning
			$thisDivID = 'ApplicableFormEmptyWarningMsgDiv';
			$thisClass = 'WarningMsgDiv';
			$thisApplicableFormEmptyWarningMsg = $linterface->Get_Form_Warning_Msg($thisDivID, $Lang['iPortfolio']['OEA']['ApplicableFormsEmpty'], $thisClass);
			
			$x = '';
			$x .= '';
			$x .= '<fieldset id="remarksFieldset" class="instruction_box" style="display:none"><legend class="instruction_title" style="color:red">Remarks</legend><span id="remarksSpan"></span></fieldset>';
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['iPortfolio']['OEA']['SchoolCode'].'</td>'."\n";
					$x .= '<td>'.$SchoolCodeTb.$thisSchoolCodeEmptyWarningMsg.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['iPortfolio']['OEA']['ModulesInUse'].'</td>'."\n";
					$x .= '<td>'.$ModuleCheckboxes.'</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['iPortfolio']['OEA']['ApplicableForms'].'</td>'."\n";
					$x .= '<td>'.$FormCheckboxesTable.$thisApplicableFormEmptyWarningMsg.'</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			$x .= '<br />'."\n";
			
			$x .= '<div class="edit_bottom_v30">'."\n";
				$x .= $SaveBtn.'&nbsp;'.$CancelBtn."\n";
			$x .= '</div>'."\n";
			$x .= '<script>';
			$x .= 'function bounsModuleCheck(){
					var moduleDetected = new Array()
					var WarningMsg = \'\';		
					OLEchecked = $(\'#ModuleInUseChk_'.$oea_cfg["JupasItemInternalCode"]["OeaItem"].'\').attr(\'checked\');
					Addichecked = $(\'#ModuleInUseChk_'.$oea_cfg["JupasItemInternalCode"]["AddiInfo"].'\').attr(\'checked\');
				//	Abilitychecked = $(\'#ModuleInUseChk_'.$oea_cfg["JupasItemInternalCode"]["Ability"].'\').attr(\'checked\');
				//	Academicchecked = $(\'#ModuleInUseChk_'.$oea_cfg["JupasItemInternalCode"]["Academic"].'\').attr(\'checked\');
						
						if(OLEchecked){
							moduleDetected.push(\''.$this->Get_Module_Name($oea_cfg["JupasItemInternalCode"]["OeaItem"]).'\');
							//WarningMsg = \''.$this->Get_Module_Name($oea_cfg["JupasItemInternalCode"]["OeaItem"]).'\'+" '.$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'].'";
							//WarningMsg = \''.$Lang['iPortfolio']['JUPAS']['RemarksOEA'].'\';
							//$(\'#remarksFieldset_'.$oea_cfg["JupasItemInternalCode"]["OeaItem"].'\').slideDown(\'\',function(){
							//	$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["OeaItem"].'\').html(WarningMsg);
							//	$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["OeaItem"].'\').fadeIn();
							//});
						}else{
					//		$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["OeaItem"].'\').fadeOut();
					//		$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["OeaItem"].'\').html(\'\');
					//		$(\'#remarksFieldset_'.$oea_cfg["JupasItemInternalCode"]["OeaItem"].'\').slideUp();
						}
						if(Addichecked){
							moduleDetected.push(\''.$this->Get_Module_Name($oea_cfg["JupasItemInternalCode"]["AddiInfo"]).'\');
							//WarningMsg2 = \''.$this->Get_Module_Name($oea_cfg["JupasItemInternalCode"]["AddiInfo"]).'\'+" '.$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'].'";
							//WarningMsg2 = \''.$Lang['iPortfolio']['JUPAS']['RemarksOEA'].'\';
							//$(\'#remarksFieldset_'.$oea_cfg["JupasItemInternalCode"]["AddiInfo"].'\').slideDown(\'\',function(){
							//	$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["AddiInfo"].'\').html(WarningMsg2);
							//	$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["AddiInfo"].'\').fadeIn();
							//});
						}else{
						//	$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["AddiInfo"].'\').fadeOut();
						//	$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["AddiInfo"].'\').html(\'\');
						//	$(\'#remarksFieldset_'.$oea_cfg["JupasItemInternalCode"]["AddiInfo"].'\').slideUp();
						}
						
						/*
						if(Abilitychecked){
							WarningMsg3 = \''.$Lang['iPortfolio']['JUPAS']['RemarksAbility'].'\';
							$(\'#remarksFieldset_'.$oea_cfg["JupasItemInternalCode"]["Ability"].'\').slideDown(\'\',function(){
								$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["Ability"].'\').html(WarningMsg3);
								$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["Ability"].'\').fadeIn();
							});
						}else{
							$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["Ability"].'\').fadeOut();
							$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["Ability"].'\').html(\'\');
							$(\'#remarksFieldset_'.$oea_cfg["JupasItemInternalCode"]["Ability"].'\').slideUp();
						}
						if(Academicchecked){
							WarningMsg4 = \''.$Lang['iPortfolio']['JUPAS']['RemarksAcademic'].'\';
							$(\'#remarksFieldset_'.$oea_cfg["JupasItemInternalCode"]["Academic"].'\').slideDown(\'\',function(){
								$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["Academic"].'\').html(WarningMsg4);
								$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["Academic"].'\').fadeIn();
							});
						}else{
							$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["Academic"].'\').fadeOut();
							$(\'#remarksSpan_'.$oea_cfg["JupasItemInternalCode"]["Academic"].'\').html(\'\');
							$(\'#remarksFieldset_'.$oea_cfg["JupasItemInternalCode"]["Academic"].'\').slideUp();
						}*/
						
						if(moduleDetected.length){
							//WarningMsg = moduleDetected.join(\' '.$Lang['iPortfolio']['JUPAS']['and'].' \');
							WarningMsg = \''.$Lang['iPortfolio']['JUPAS']['RemarksOEA'].'\';
						}
						if(WarningMsg){
							//WarningMsg = WarningMsg+" '.$Lang['iPortfolio']['JUPAS']['WillNotBeUsed'].'";
							$(\'#remarksFieldset:hidden\').slideDown(\'\',function(){
								$(\'#remarksSpan\').fadeOut(\'\',function(){
									$(\'#remarksSpan\').html(WarningMsg);
									$(\'#remarksSpan\').fadeIn();
								});
							});
						
						}else{
							$(\'#remarksSpan\').fadeOut(\'\',function(){
								$(\'#remarksSpan\').html(\'\');
								$(\'#remarksFieldset\').slideUp();
							});
						}
						
					}
					bounsModuleCheck();'
					;
			$x .= '</script>';
			
			return $x;
		}
		
		public static function Get_Module_Internal_Code_Array() {
			global $oea_cfg;
			
			$ModuleCodeArr = array();
			foreach ((array)$oea_cfg["JupasItemInternalCode"] as $thisModule => $thisModuleInternalCode) {
				$ModuleCodeArr[] = $thisModuleInternalCode;
			}
			
			return $ModuleCodeArr;
		}
		
		public function Get_Module_In_Use_Array() {
			global $oea_cfg;
			return explode($oea_cfg["Setting"]["Default_ModuleInUse_Separator"], $this->getModuleInUse());
		}
		
		public function Is_Module_In_Use($InternalCode) {
			$ModuleInUseArr = $this->Get_Module_In_Use_Array();
			return in_array($InternalCode, (array)$ModuleInUseArr);
		}
		
		private function Get_Module_Name($ModuleCode) {
			global $Lang;
			return $Lang['iPortfolio']['OEA']['ModuleTitleArr'][$ModuleCode];
		}
		
		private function Get_Module_In_Use_Checkboxes_Table() {
			global $Lang, $linterface, $oea_cfg;
			
			$ModuleInUseArr = $this->Get_Module_In_Use_Array();
			$numOfInUseModule = count($ModuleInUseArr);
			$AllModuleCodeArr = $this->Get_Module_Internal_Code_Array();
			$numOfAllModule = count($AllModuleCodeArr);
			
			$CheckboxClass = 'ModuleInUseChk';
			
			$x = '';
			$x .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
				### All
				$thisCheckAllID = 'ModuleInUseChk_All';
				$thisCheckAllName = 'ModuleInUseChk_All';
				$thisChecked = ($numOfInUseModule==$numOfAllModule)? true : false;
				$x .= '<tr>'."\n";
					$x .= '<td style="border:0px;">'."\n";
						$x .= $linterface->Get_Checkbox($thisCheckAllID, $thisCheckAllName, '', $thisChecked, $thisClass='', $Lang['Btn']['All'], "Check_All_Options_By_Class('".$CheckboxClass."', this.checked); bounsModuleCheck();");
					$x .= '</td>'."\n";
					$x .= '<td style="border:0px;">&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";
				
				for ($i=0; $i<$numOfAllModule; $i++) {
					$thisModuleCode = $AllModuleCodeArr[$i];
					$thisModuleDisplay = $this->Get_Module_Name($thisModuleCode);
					$bounsModuleCheck = '';
					$bounsModuleCheckRemarks = '';
					
					$thisID = 'ModuleInUseChk_'.$thisModuleCode;
					$thisName = 'ModuleInUseArr[]';
					$thisChecked = (in_array($thisModuleCode, $ModuleInUseArr))? true : false;
					
					if($thisModuleCode==$oea_cfg["JupasItemInternalCode"]["AddiInfo"]||$thisModuleCode==$oea_cfg["JupasItemInternalCode"]["OeaItem"]||$thisModuleCode==$oea_cfg["JupasItemInternalCode"]["Ability"]||$thisModuleCode==$oea_cfg["JupasItemInternalCode"]["Academic"]){
						//OEA and Additional Information are not in used from 13Nov2012
						$bounsModuleCheck = 'bounsModuleCheck(); ';
						$bounsModuleCheckRemarks = '<fieldset id="remarksFieldset_'.$thisModuleCode.'" class="instruction_box" style="display:none"><legend class="instruction_title" style="color:red">Remarks</legend><span id="remarksSpan_'.$thisModuleCode.'"></span></fieldset>';
					}
					
					$x .= '<tr>'."\n";
						$x .= '<td style="border:0px; padding:0px;">&nbsp;</td>'."\n";
						$x .= '<td style="border:0px; padding:0px;">'."\n";
							$x .= $linterface->Get_Checkbox($thisID, $thisName, $thisModuleCode, $thisChecked, $CheckboxClass, $thisModuleDisplay, "Uncheck_SelectAll('".$thisCheckAllID."', this.checked); $bounsModuleCheck");
							$x .= $bounsModuleCheckRemarks;
						$x .= '</td>'."\n";
					$x .= '</tr>'."\n";
				}
			$x .= '</table>'."\n";
			
			return $x;
		}
		
		public static function Get_Applicable_Form_WebSAMS_Code_Array() {
			global $oea_cfg;
			return $oea_cfg['Setting']['WebSAMSCodeOfApplicableFormForJupas'];
		}
		
		public static function Get_Academic_Applicable_Form_WebSAMS_Code_Array() {
			global $oea_cfg;
			return $oea_cfg['Setting']['WebSAMSCodeOfApplicableFormForJupasAcademic'];
		}
		
		public function Get_Form_Checkboxes_Table($SelectedValueArr='', $FormPerRow=1, $ShowAppliedFormOnly=1) {
			global $Lang, $linterface;
		
			$libfcm = new form_class_manage();
			$ApplicableFormWebSAMSCodeArr = ($ShowAppliedFormOnly==1)? $this->Get_Applicable_Form_WebSAMS_Code_Array() : '';
			$FormInfoArr = $libfcm->Get_Form_List($GetMappedClassInfo=true, $ActiveOnly=1, Get_Current_Academic_Year_ID(), $YearIDArr='', $ApplicableFormWebSAMSCodeArr);
			$numOfAllOptions = count($FormInfoArr);
			
			$CheckboxClass = 'FormIDChk';
			if ($SelectedValueArr == '') {
				$numOfSelectedOption = $numOfAllOptions;
			}
			else {
				$numOfSelectedOption = count((array)$SelectedValueArr);
			}
			
			
			$x = '';
			$x .= '<table border="0" cellpadding="0" cellspacing="0">'."\n";
				### All
				$thisCheckAllID = 'FormIDChk_All';
				$thisCheckAllName = 'FormIDChk_All';
				$thisChecked = ($numOfSelectedOption==$numOfAllOptions)? true : false;
				$x .= '<tr>'."\n";
					$x .= '<td style="border:0px;">'."\n";
						$x .= $linterface->Get_Checkbox($thisCheckAllID, $thisCheckAllName, '', $thisChecked, $thisClass='', $Lang['Btn']['All'], "Check_All_Options_By_Class('".$CheckboxClass."', this.checked);");
					$x .= '</td>'."\n";
					$x .= '<td style="border:0px;">&nbsp;</td>'."\n";
				$x .= '</tr>'."\n";
				
				for ($i=0; $i<$numOfAllOptions; $i++) {
					$thisValue = $FormInfoArr[$i]['YearID'];
					$thisDisplay = $FormInfoArr[$i]['YearName'];
					$thisMappedClass = $FormInfoArr[$i]['MappedClass'];
					
					if ($thisMappedClass == 0) {
						continue;
					}
					
					$thisID = 'FormIDChk_'.$thisValue;
					$thisName = 'FormIDArr[]';
					$thisChecked = ($SelectedValueArr=='' || in_array($thisValue, (array)$SelectedValueArr))? true : false;
					
					if ($i % $FormPerRow == 0) {
						$x .= '<tr>'."\n";
					}
						$x .= '<td style="border:0px; padding:0px;">&nbsp;</td>'."\n";
						$x .= '<td style="border:0px; padding:0px;">'."\n";
							$x .= $linterface->Get_Checkbox($thisID, $thisName, $thisValue, $thisChecked, $CheckboxClass, $thisDisplay, "Uncheck_SelectAll('".$thisCheckAllID."', this.checked);");
						$x .= '</td>'."\n";
						
					if ( (($i+1) % $FormPerRow == 0) || ($i==$numOfAllOptions-1)) {
						$x .= '</tr>'."\n";
					}
				}
			$x .= '</table>'."\n";
			
			return $x;
		}
		
		public function Get_Academic_Percentile_Overall_Rating_Mapping_View_Table() {
			global $Lang;
			
			$PercentileCodeArr = liboea_academic::Get_Academic_Percentile_Code_Array();
			$numOfPercentile = count($PercentileCodeArr);
			$AcademicMappingArr = $this->getAcademic_PercentileMapWithOverAllRating();
			
			$x = '';
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="common_table_list_v30">'."\n";
				$x .= '<col style="width:50%;">'."\n";
				$x .= '<col style="width:50%;">'."\n";
				
				$x .= '<thead>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['AcademicArr']['Percentile'].' ('.$Lang['iPortfolio']['OEA']['AcademicArr']['PositionInForm'].')</th>'."\n";
						$x .= '<th>'.$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating'].'</th>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</thead>'."\n";
				$x .= '<tbody>'."\n";
					for ($i=0; $i<$numOfPercentile; $i++) {
						$thisPercentileCode = $PercentileCodeArr[$i];
						$thisOverallRating = $AcademicMappingArr[$thisPercentileCode];
						
						$thisPercentileDisplay = liboea_academic::Get_Academic_Percentile_Name($thisPercentileCode);
						$thisOverallRatingDisplay = ($thisOverallRating=='')? $Lang['General']['EmptySymbol'] : liboea_academic::Get_Academic_Overall_Rating_Name($thisOverallRating);
						
						$x .= '<tr>'."\n";
							$x .= '<td>'.$thisPercentileDisplay.'</td>'."\n";
							$x .= '<td>'.$thisOverallRatingDisplay.'</td>'."\n";
						$x .= '</tr>'."\n";
					}
				$x .= '</tbody>'."\n";
			$x .= '</table>'."\n";
			
			return $x;
		}

	   /**
		* private function , Parse settingName of "StudentApplyOLEToPart" in t:OEA_SETTING , in this function, it parse the value and create two setting array 
		* 1) OEA PART can with which INT TYPE ($this->oea_part_settingAry)
		* 2) INT TYPE can with which OEA PART ($this->ole_type_settingAry)
		* @owner : Fai (20111011)
		* @param : NIL
		* @return : NIL
		*/
		private function parseSettingForPartAndOLE(){
			global $oea_cfg,$ipf_cfg;
			

			$part_setting = array();
			$type_setting = array();

			$settingStr = $this->getStudentApplyOLEToPart();

			if($settingStr == ''){
				//IF USER DO NOT HAVE THIS SETTING, SET ALL THE VALUE AS DEFAULT ( HARDCODE)
				$part_setting[$oea_cfg["OEA_Participation_Type_School"]] = array($ipf_cfg["OLE_TYPE_STR"]["INT"],$ipf_cfg["OLE_TYPE_STR"]["EXT"]);
				$part_setting[$oea_cfg["OEA_Participation_Type_Private"]] = array($ipf_cfg["OLE_TYPE_STR"]["INT"],$ipf_cfg["OLE_TYPE_STR"]["EXT"]);

				$type_setting[$ipf_cfg["OLE_TYPE_STR"]["INT"]] = array($oea_cfg["OEA_Participation_Type_School"],$oea_cfg["OEA_Participation_Type_Private"]);
				$type_setting[$ipf_cfg["OLE_TYPE_STR"]["EXT"]] = array($oea_cfg["OEA_Participation_Type_School"],$oea_cfg["OEA_Participation_Type_Private"]);	
			}else{

				//WARNING : IF YOU CHANGE THIS PART , PLEASE ALSO CHECK WITH CLASE  [if($settingStr == '') ]
				$srcAry = explode($oea_cfg["Setting"]["Default_ModuleInUse_Separator"],$settingStr);
				for($i = 0, $i_max = sizeof($srcAry);$i< $i_max; $i++){
					$_tmpVal = $srcAry[$i];
					$_detailsForAPart = explode($oea_cfg["Setting"]["Default_Separator2"],$_tmpVal);

//					debug_r($_detailsForAPart);
					$_partName = trim($_detailsForAPart[0]);
					$_intVal = trim($_detailsForAPart[1]);
//					debug_r($_intVal);
					$_extVal = trim($_detailsForAPart[2]);
					$part_setting[$_partName] = array($_intVal,$_extVal);

					if($_intVal != ''){
						$type_setting[$ipf_cfg["OLE_TYPE_STR"]["INT"]][] = $_partName;
					}
					if($_extVal != ''){
						$type_setting[$ipf_cfg["OLE_TYPE_STR"]["EXT"]][] = $_partName;
					}
				}
			}
			$this->oea_part_settingAry = $part_setting;
			$this->ole_type_settingAry = $type_setting;
		}

	   /**
		* RETURN private variable oea_part_settingAry
		* @owner : Fai (20111011)
		* @param : NIL
		* @return : NIL
		* 
		*/
		public function getSettingForPartToType(){
			return $this->oea_part_settingAry;
		}

	   /**
		* RETURN private variable ole_type_settingAry
		* @owner : Fai (20111011)
		* @param : NIL
		* @return : NIL
		* 
		*/
		public function getSettingForTypeToPart(){
			return $this->ole_type_settingAry;
		}

	   /**
		* check for a ole Type (int ,ext) is allowed to apply which OEA part (i , ii)
		* @owner : Fai (20111011)
		* @param : String $oleType , request type of the OLE (int or ext)
		* @return : Array, array of allow part (p,s) as its element
		*/
		public function getAllowOEAPartByOLEType($oleType){
			$oleType = strtolower($oleType);

			return $this->ole_type_settingAry[$oleType];
		}

		/**
		* check for a OEA PART (P ,S ) is allowed to apply which OLE part (int,ext)
		* @owner : Fai (20111011)
		* @param : String $oeaPart , request type of the OEA(S or P)
		* @return : Array, array of ole type (int, ext) as its element
		*/
		public function getAllowOLETypeByOEAPart($oeaPart){
			$oeaPart = strtoupper($oeaPart);

			return $this->oea_part_settingAry[$oeaPart];
		}

		/**
		* GET ALL THE OEA Part that is allowed to apply (it is a teacher setting)
		* @owner : Fai (20111011)
		* @param : NIL
		* @return : Array, array of OEA Part with array('P','S')
		*/
		public function getAllowOEAPart(){
			$oeaAry = $this->oea_part_settingAry;

			$allowOea = array();
			foreach ($oeaAry as $part => $ole){
				for($i = 0, $i_max = count($ole);$i< $i_max;$i++){
					//IF ANY OLE TYPE AS VALUE , THAT INDICATE THE OEA PART IS READY FOR APPLY
					if(trim($ole[$i]) != ''){
						$allowOea[] = $part; 
						break;
					}
				}
			}
			return $allowOea;
			//return $this->oea_part_settingAry[$oeaPart];
		}

		/**
		* check for whether a OEA type is allowed to apply (it is a teacher setting)
		* @owner : Fai (20111011)
		* @param : String $oeaPart  , request checked OEA PART
		* @return : BOOLEAN , boolean to indicate to apply or not
		*/
		public function getThisOEAPartIsAllowed($oeaPart){
			$oeaPart = strtoupper($oeaPart);
			$oeaAry = $this->getAllowOEAPart();
			return in_array($oeaPart,$oeaAry);
		}
		
		/**
		* Standardize the csv output data
		* @owner : Ivan (20111117)
		* @param : String $data , csv data
		* @return : String , standardized csv data (current change double quote to single quote)
		*/
		private function standardizeCsvData($data) {
			$data = str_replace('"', "'", $data);
			$data = str_replace('&quot;', "'", $data);
			return $data;
		}
		
		public function getOeaItemAryForJs() {
			$liboea_setting = new liboea_setting();
			
			$default_oea_all = $liboea_setting->get_OEA_Item($itemCode='', $withOthersWarning=true);
			if(sizeof($default_oea_all)>0) {
				$js_default_item_array = "var itemAry = new Array();\n";
				foreach($default_oea_all as $_key=>$_ary) {
					$js_default_item_array .= "itemAry[\"$_key\"] = [\"".$_ary['CatCode']."\",\"".$_ary['CatName']."\"];\n";
				}
			}
			$h_js = $js_default_item_array."\n";
			
			return $h_js;
		}
		
		private function standardizeCsvLineBreakData($data, $maxLength) {
			if (strlen($data) > $maxLength) {
				$data = str_replace("\r", "", $data);
			}
			return $data;
		}

		public function cust_Lasalle_getTeacherManagementIndexSql($YearID, $YearClassID, $TeachingClassOnly, $TargetApprovalStatus)
		{
			global $Lang, $linterface, $lpf , $eclass_db;
			$sqlDebug = array();			
			$libfcm = new form_class_manage();
			$OEAItemInternalCode = $this->getOEAItemInternalCode();
			### Get all Students
			if ($YearClassID != '') {
				$YearClassIDArr = array($YearClassID);
			}
			else {
				if ($YearID == '') {
					// All Forms => Get All Applicable Forms of Jupas
					$YearID = $this->getJupasApplicableFormIDArr();

				}
				$YearClassInfoArr = $libfcm->Get_Class_List_By_Academic_Year(Get_Current_Academic_Year_ID(), $YearID, $TeachingClassOnly);

				$YearClassIDArr = Get_Array_By_Key($YearClassInfoArr, 'YearClassID');

			}
			$StudentInfoArr = $lpf->Get_Student_With_iPortfolio($YearClassIDArr, $IsSuspend='');

			$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
			$numOfStudent = count($StudentInfoArr);
			
			$TmpTableFieldArr = array();
			
			$objDB = new libdb();
			$sql = "select StudentID , count(*) as `numberOfRecord` from {$eclass_db}.OEA_STUDENT_PRE_SELECT as os where StudentID In (".implode(',', (array)$StudentIDArr).") group by studentid";
			$studentPreMapCountALLAry = $objDB->returnResultSet($sql);	
			$sqlDebug[] = $sql;
			$studentPreMapCountALLAry = BuildMultiKeyAssoc($studentPreMapCountALLAry, 'StudentID');

			
			
			$InsertValueArr = array();
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$_insertSqlItemArr = array();
				
				$_studentID 				= $StudentInfoArr[$i]['UserID'];
				$_className 				= Get_Lang_Selection($StudentInfoArr[$i]['ClassTitleB5'], $StudentInfoArr[$i]['ClassTitleEN']);
				$_classNumber 				= $StudentInfoArr[$i]['ClassNumber'];
				$_studentName 				= $StudentInfoArr[$i]['StudentName'];
				$_isIPfActivated			= $StudentInfoArr[$i]['IsPortfolioActivated'];
				$_jupasApplicationNumber 	= $StudentInfoArr[$i]['JupasApplicationNumber'];
				$_webSamsRegNo 	= $StudentInfoArr[$i]['WebSAMSRegNo'];
				
				$_numberOfPreMapForStudent  = $studentPreMapCountALLAry[$_studentID]['numberOfRecord'];

				### If Activated iPf => cannot view OEA by clicking the student name. Otherwise, only the student name is shown.
				if ($_isIPfActivated) {
					$_linkTitle = $Lang['iPortfolio']['OEA']['ViewAndApproveStudentJupasData'];
					$_studentNameLink = '<a class="tablelink" href="index.php?task=listAPreMapStudent&clearCoo=1&StudentID='.$_studentID.'" title="'.$_linkTitle.'">'.$_studentName.'</a>'."\n";
				}
				else {
					$_studentNameLink = $_studentName."\n";
					$_webSamsRegNo = $Lang['General']['EmptySymbol'];
					$_numberOfPreMapForStudent = $Lang['General']['EmptySymbol'];
				}
				
				$_insertSqlItemArr[] = $this->pack_value($_className, 'str');
				$_insertSqlItemArr[] = $this->pack_value($_classNumber, 'int');
				$_insertSqlItemArr[] = $this->pack_value($_studentNameLink, 'str');
				$_insertSqlItemArr[] = $this->pack_value($_webSamsRegNo , 'str');
				$_insertSqlItemArr[] = ($_jupasApplicationNumber=='' || $_jupasApplicationNumber==0)? 'null' : $this->pack_value($_jupasApplicationNumber, 'int');				
				$_insertSqlItemArr[] = ($_isIPfActivated)?$this->pack_value($_numberOfPreMapForStudent,'int') : $this->pack_value($_numberOfPreMapForStudent, 'str');

				
											
				$_trCustClass = ($_isIPfActivated==1)? 'row_on' : 'row_off';
				$_insertSqlItemArr[] = $this->pack_value($_trCustClass, 'str');

				$InsertValueArr[] = '('.implode(',', (array)$_insertSqlItemArr).')';
			}
			
			### Create Temp Table
			$sql = "Create Temporary Table If Not Exists TMP_OEA_TEACHER_MGMT_INDEX (
						RecordID Int(11) NOT NULL AUTO_INCREMENT,
						trCustClass varchar(128) Default Null,
						ClassName varchar(255) Default Null,
						ClassNumber int(8) Default Null,
						StudentNameLink text Default Null,
						WebSAMSRegNo varchar(100) default NULL,
						JupasApplicationNumber int(10) Default Null,
						TotalRecordOfPreMap varchar(255) default NULL,
						PRIMARY KEY (RecordID)
					) ENGINE=InnoDB Charset=utf8";

			$SuccessArr['CreateTempTable'] = $this->db_db_query($sql);
			$sqlDebug[] = $sql;


			$TmpTableFieldArr[] = 'ClassName';
			$TmpTableFieldArr[] = 'ClassNumber';
			$TmpTableFieldArr[] = 'StudentNameLink';						
			$TmpTableFieldArr[] = 'WebSAMSRegNo';
			$TmpTableFieldArr[] = 'JupasApplicationNumber';			
			$TmpTableFieldArr[] = 'TotalRecordOfPreMap';			
			$TmpTableFieldArr[] = 'trCustClass';

			### Insert Temp Records
			if (count($InsertValueArr) > 0)
			{
				$sql = "Insert Into TMP_OEA_TEACHER_MGMT_INDEX
							(".implode(',', (array)$TmpTableFieldArr).")
						Values
							".implode(',', (array)$InsertValueArr)."
						";
				$SuccessArr['InsertTempData'] = $this->db_db_query($sql);
$sqlDebug[] = $sql;
			}
			
			
			$SelectFieldArr = array();
			$numOfField = count((array)$TmpTableFieldArr);
			for ($i=0; $i<$numOfField; $i++) {
				$thisField = $TmpTableFieldArr[$i];
				
				switch ($thisField){
					case 'JupasApplicationNumber':
						$SelectFieldArr[] = "If (JupasApplicationNumber Is Null, '".$Lang['General']['EmptySymbol']."', JupasApplicationNumber)";
						break;
					case 'TotalRecordOfPreMap':
						$SelectFieldArr[] = "If (TotalRecordOfPreMap Is Null, 0, TotalRecordOfPreMap)";
						break;
					default:
						$SelectFieldArr[] = $thisField;
						break;
				}
			}
			$DBTable_SQL = "Select 
									".implode(',', (array)$SelectFieldArr)."
							From 
									TMP_OEA_TEACHER_MGMT_INDEX	
							";
$sqlDebug[] = $DBTable_SQL;

// don't delete , can open for debug
/*
for($i = 0,$i_max = count($sqlDebug); $i < $i_max;$i++){
	$_tmp = $sqlDebug[$i];
	error_log($_tmp."   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/cccc.txt");
}
*/

			$ReturnArr = array();
			$ReturnArr['DBTableSql'] = $DBTable_SQL;
			$ReturnArr['DBTableFieldArr'] = $TmpTableFieldArr;
					
			return $ReturnArr;
		}
	}

}
?>