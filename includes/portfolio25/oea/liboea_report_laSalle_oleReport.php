<?
# modifying : ivan

if (!defined("LIBOEA_REPORT_LASALLE_OLEREPORT_DEFINED"))         // Preprocessor directives
{
  define("LIBOEA_REPORT_LASALLE_OLEREPORT_DEFINED",true);

	class liboea_report_laSalle_oleReport {

		public function __construct(){
			
		}
	
		function printReportAsHtml($DataArr, $includeOleData){
			global $Lang;
			
			$numOfOeaData = count($DataArr);
			
			$headerInfoArr = $this->getReportHeaderInfoAry();
			$numOfHeader = count($headerInfoArr);
			
			$h_table = '';
			$h_table .= '<table class="border_table" style="width:100%;" cellpadding="2">'."\r\n";
				$h_table .= '<thead>'."\r\n";
					$h_table .= '<tr>'."\r\n";
						$h_table .= '<th style="width:1%;">#</th>'."\r\n";
						for ($i=0; $i<$numOfHeader; $i++) {
							$_name = $headerInfoArr[$i]['name'];
							$_width = $headerInfoArr[$i]['width'];
							
							$h_table .= '<th style="width:'.$_width.'%;">'.$_name.'</th>'."\r\n";
						}						
					$h_table .= '</tr>'."\r\n";
				$h_table .= '</thead>'."\r\n";
				$h_table .= '<tbody>'."\r\n";
					if ($numOfOeaData == 0) {
						$h_table .= '<tr><td colspan="100%" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\r\n";
					}
					else {
						$previousClassName = '';
						$previousClassNumber = '';
						$studentRecordCount = 0;
						for($i=0,$i_max=$numOfOeaData; $i<$i_max; $i++) {
							$_dataArr = $DataArr[$i];
							$_className = $DataArr[$i][0];
							$_classNumber = $DataArr[$i][1];
							
							$_tdStyle = '';
							if (($previousClassName=='' && $previousClassNumber=='') || ($previousClassName!=$_className || $previousClassNumber!=$_classNumber)) {
								// the first row of each student
								$_tdStyle = 'style="border-top-width: 3px;"';
								$studentRecordCount = 0;
							}
							
							
							$_numOfCol = count($_dataArr);
							$h_table .= '<tr>'."\r\n";
								$h_table .= '<td '.$_tdStyle.'>'.(++$studentRecordCount).'</td>'."\r\n";
								for ($j=0; $j<$_numOfCol; $j++) {
									$h_table .= '<td '.$_tdStyle.'>'.nl2br($_dataArr[$j]).'</td>'."\r\n";
								}
							$h_table .= '</tr>'."\r\n";
							
							$previousClassName = $_className;
							$previousClassNumber = $_classNumber;
						}
					}
				$h_table .= '</tbody>'."\r\n";
			$h_table .= '</table>'."\r\n";
			$h_table .= '<br style="clear:both;" />'."\r\n";
			$h_table .= '<span style="float:left; font-size:12px;">* Programme Nature: <b>INT</b> represents Other Learning Experiences; <b>EXT</b> represents Performance / Awards and Key Participation Outside School.</span>'."\r\n";
			
			return $h_table;
		}

		function printReportAsCSV($DataArr, $includeOleData){
			global $intranet_root,$Lang;

			include_once($intranet_root.'/includes/libexporttext.php');

			$lexport = new libexporttext();
			$ExportHeaderInfoArr = $this->getReportHeaderInfoAry();
			$ExportHeaderArr = Get_Array_By_Key($ExportHeaderInfoArr, 'name');
			unset($ExportHeaderInfoArr);
			
			$DataArr[count($DataArr)][0] = '* Programme Nature: INT represents Other Learning Experiences; EXT represents Performance / Awards and Key Participation Outside School.';
			
			$ExportText = $lexport->GET_EXPORT_TXT($DataArr, $ExportHeaderArr, $Delimiter="\t", $LineBreak="\r\n", $ColumnDefDelimiter="\t", $DataSize=0, $Quoted="11", $includeLineBreak=1);
			
			$lexport->EXPORT_FILE('ole_student.csv', $ExportText);
			die();
		}

		function getStudentDataForReport($StudentIDArr, $includeOleData){
			global $intranet_root, $Lang, $ipf_cfg, $iPort, $oea_cfg;
			include_once($intranet_root.'/includes/libuser.php');
			
			$liboea = new liboea();
			$liboea_item = new liboea_item();
			$liboea_setting = new liboea_setting();
			
			$objUser = new libuser($uid="",$UserLogin="", $StudentIDArr);

			### Get Student JUPAS info
			$StudentInfoArr = $liboea->getStudentInfoForCsv($StudentIDArr);

			### Get Student OEA info
			$ApprovedStatus = $liboea->getItemTypeApprovedStatus($liboea->getOEAItemInternalCode());
			$StudentOeaInfoArr = $liboea->getStudentOeaItemInfoArr($StudentIDArr, $ApprovedStatus, $ParticipationArr='', $ParRecordIDArr='', $ParExcludeRecordIDArr='', $OLE_ProgramIDArr='', $withCodeInOeaTitle=false);
			$StudentOeaInfoAssoArr = BuildMultiKeyAssoc($StudentOeaInfoArr, 'StudentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
						
			//if ($includeOleData) {
				$libpf_ole = new libpf_ole();
				$libpf_category = new Category();
				
				$OleProgramIDArr = array_values(array_unique(Get_Array_By_Key($StudentOeaInfoArr, 'OLE_PROGRAM_ProgramID')));
				$OleStudentRecordIDArr = array_values(array_unique(Get_Array_By_Key($StudentOeaInfoArr, 'OLE_STUDENT_RecordID')));
			
				### Get the related OLE Program Info
				$OleProgramInfoArr = $libpf_ole->Get_OLE_Info($FormIDArr='', $OleProgramIDArr);
				$OleProgramAcademicYearIDArr = array_values(array_unique(Get_Array_By_Key($OleProgramInfoArr, 'AcademicYearID')));
				$OleProgramAcademicYearTermIDArr = array_values(array_unique(Get_Array_By_Key($OleProgramInfoArr, 'YearTermID')));
				$OleProgramAssoArr = BuildMultiKeyAssoc($OleProgramInfoArr, 'ProgramID');
				unset($OleProgramInfoArr);
				
				### Get the related OLE Student Record Info
				$OleStudentRecordInfoArr = $libpf_ole->Get_Student_OLE_Info('', $OleStudentRecordIDArr, $forSlpOnly='');
				$OleStudentRecordAssoArr = BuildMultiKeyAssoc($OleStudentRecordInfoArr, 'OleStudentRecordID');
				unset($OleStudentRecordInfoArr);
				
//				### Get the Academic Year Mapping
//				$AcademicYearInfoArr = GetAllAcademicYearInfo();
//				$AcademicYearAssoArr = BuildMultiKeyAssoc($AcademicYearInfoArr, 'AcademicYearID');
//				unset($AcademicYearInfoArr);
//				
//				### Get the Academic Term Mapping
//				$YearTermAssoArr = getSemesters();
				
				### Get the OLE Category Mapping
				$OleCategoryInfoArr = $libpf_category->GET_CATEGORY_LIST();
				$OleCategoryAssoArr = BuildMultiKeyAssoc($OleCategoryInfoArr, 'RecordID');
				unset($OleCategoryInfoArr);
				
				### Get ELE Mapping
				$EleInfoArr = $libpf_ole->Get_ELE_Info();
				$EleAssoArr = BuildMultiKeyAssoc($EleInfoArr, 'ELECode');
				unset($EleInfoArr);
			//}
			unset($StudentOeaInfoArr);
			
			
			### Build display data array
			$DataArr = array();
			$numOfStudent = count($StudentInfoArr);
			$rowCounter = 0;
			for ($i=0; $i<$numOfStudent; $i++) {
				$_studentId = $StudentInfoArr[$i]['UserID'];
				
				$objUser->LoadUserData($_studentId);
				$_webSAMSRegNo = $objUser->WebSamsRegNo;
				$_studentName = Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
				$_className = $objUser->ClassName;
				$_classNumber = $objUser->ClassNumber;
				
				$_studentOeaInfoArr = $StudentOeaInfoAssoArr[$_studentId];
				$_studentNumOfOea = count($_studentOeaInfoArr);
				for ($j=0; $j<$_studentNumOfOea; $j++) {
					$__oleProgramId = $_studentOeaInfoArr[$j]['OLE_PROGRAM_ProgramID'];
					$__oleStudentRecordId = $_studentOeaInfoArr[$j]['OLE_STUDENT_RecordID'];
					
					if ($__oleProgramId == 0 && $__oleStudentRecordId == 0) {
						// new from OEA directly => show OEA data
						$__programName = $_studentOeaInfoArr[$j]['Title'];
						$__description = $_studentOeaInfoArr[$j]['Description'];
						$__participation = $_studentOeaInfoArr[$j]['Participation'];
						$__intExt = ($__participation == $oea_cfg["OEA_Participation_Type_School"])? strtoupper($ipf_cfg["OLE_TYPE_STR"]["INT"]) : strtoupper($ipf_cfg["OLE_TYPE_STR"]["EXT"]);
						
						$__startYear = $_studentOeaInfoArr[$j]['StartYear'];
						$__endYear = $_studentOeaInfoArr[$j]['EndYear'];
						$__role = $liboea_item->Get_OEA_RoleName_By_RoleCode($_studentOeaInfoArr[$j]['Role']);
						$__organization = $Lang['General']['EmptySymbol'];
						$__achievement = $liboea_item->Get_OEA_AchievementName_By_AchievementCode($_studentOeaInfoArr[$j]['Achievement']);
						$__datePeriod = $Lang['General']['EmptySymbol'];
						
						$__categoryArr = $liboea_setting->getOEACategoryInfoByOEAItemCode($_studentOeaInfoArr[$j]['OEA_ProgramCode']);
						$__category = $__categoryArr['CatName'];
						
						$__hours = $Lang['General']['EmptySymbol'];
						$__oleComponent = $Lang['General']['EmptySymbol'];
					}
					else {
						// new from OLE => show OLE data
						$__programName = Get_Lang_Selection($OleProgramAssoArr[$__oleProgramId]['TitleCh'], $OleProgramAssoArr[$__oleProgramId]['TitleEn']);
						$__description = $OleProgramAssoArr[$__oleProgramId]['Details'];
						$__intExt = strtoupper($OleProgramAssoArr[$__oleProgramId]['IntExt']);
						$__startYear = is_date_empty($OleProgramAssoArr[$__oleProgramId]['StartDate'])? $Lang['General']['EmptySymbol'] : substr($OleProgramAssoArr[$__oleProgramId]['StartDate'], 0, 4);
						$__endYear = is_date_empty($OleProgramAssoArr[$__oleProgramId]['EndDate'])? $Lang['General']['EmptySymbol'] : substr($OleProgramAssoArr[$__oleProgramId]['EndDate'], 0, 4);
						$__role = $OleStudentRecordAssoArr[$__oleStudentRecordId]['Role'];
						$__organization = $OleProgramAssoArr[$__oleProgramId]['Organization'];
						$__achievement = $OleStudentRecordAssoArr[$__oleStudentRecordId]['Achievement'];
						
						$__datePeriod = $OleProgramAssoArr[$__oleProgramId]['StartDate'];
						if (!is_date_empty($OleProgramAssoArr[$__oleProgramId]['EndDate'])) {
							$__datePeriod .= ' - '.$OleProgramAssoArr[$__oleProgramId]['EndDate'];
						}
						
						$__oleCategoryID = $OleProgramAssoArr[$__oleProgramId]['Category'];
						$__category = Get_Lang_Selection($OleCategoryAssoArr[$__oleCategoryID]['ChiTitle'], $OleCategoryAssoArr[$__oleCategoryID]['EngTitle']);
						
						$__hours = $OleStudentRecordAssoArr[$__oleStudentRecordId]['Hours'];
						
						$__oleEle = $OleProgramAssoArr[$__oleProgramId]['ELE'];
						$__oleEleArr = explode(',', $__oleEle);
						$__numOfEle = count($__oleEleArr);
						$__eleNameArr = array();
						for ($k=0; $k<$__numOfEle; $k++) {
							$___eleCode = $__oleEleArr[$k];
							$__eleNameArr[] = Get_Lang_Selection($EleAssoArr[$___eleCode]['ChiTitle'], $EleAssoArr[$___eleCode]['EngTitle']);
						}
						$__oleComponent = implode(', ', $__eleNameArr);
					}
					
					$__colCounter = 0;
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($_webSAMSRegNo);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($_studentName);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($_className);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($_classNumber);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__programName);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__description);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__startYear);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__endYear);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__role);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__organization);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__achievement);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__datePeriod);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__category);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__hours);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__oleComponent);
					$DataArr[$rowCounter][$__colCounter++] = Get_Standardized_Table_Data_Display($__intExt);
					$rowCounter++;
				}
			}
			
			return $DataArr;
		}
		
		private function getReportHeaderInfoAry() {
			$headerArr = array();
			
			$count = 0;
			$headerArr[$count]['name'] = 'Registration Number'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Student Name'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Class Name'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Class Number'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Programme'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Programme Description'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'School Year From'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'School Year To'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Role Of Participation'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Partner Organization'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Awards / Certifications/ Achievements (including grades/marks, if any)'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Date/Period'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Category'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Hours'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Component of OLE'; 
			$headerArr[$count++]['width'] = 6;
			$headerArr[$count]['name'] = 'Programme Nature *'; 
			$headerArr[$count++]['width'] = 6;
			
			return $headerArr;
		}
		
		public function getReportTitle() {
			global $Lang;
			return $Lang['iPortfolio']['OEA']['ReportArr']['StudentOeaReport'].' ('.$Lang['iPortfolio']['OEA']['ReportArr']['CustReportTitleArr']['LaSalle_StudentMappedOeaOleInfoReport'].')';
		}
	}
}
?>
