<?php
// Using:   
/**********************************************************************
 * Tabs Config
 * Date: 2018-05-02 AnnA
 *  - change twghczm personal charater access right #138947
 * Date: 2018-01-30 Anna
 *  - change IPF_CFG_SLP_MGMT_CUST_DATA_CZM path
 * Date: 2018-01-25 Cameron 
 * 	- add $ipf_cfg["MODULE_TAB"]["SR_MGMT"]['reading_records'] and $ipf_cfg["MODULE_TAB"]["SR_VIEW"]['reading_records'] [case #E118376]
 * Date: 2017-11-07 Bill	[2017-0403-1552-04240]
 * 	- $sys_custom['iPf']['HKUGA_eDis_Tab'] added Student & Pastoral Record Tab
 * Date: 2017-08-03 Carlos
 *  - $sys_custom['LivingHomeopathy'] customize the tags $ipf_cfg["MODULE_TAB"]["SR_VIEW"], $ipf_cfg["TOP_TAB"]["SLP_MGMT"], 
 * 		$ipf_cfg["TOP_TAB"]["SLP_MGMT_VIEW"], $ipf_cfg["MODULE_TAB"]["SR_MGMT"], $ipf_cfg["TOP_TAB"]["SLP_SETTING"]
 * Date: 2016-11-29 Omas
 *  - $sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'] - allow class teacher to access
 * Date: 2015-11-19 Omas
 * added $sys_custom['iPf']['sptss']['Report']['SLP'] - personal charater page
 * Date: 2015-11-19 Cameron
 * 	- use $Lang['iPortfolio']['other_reports'] from Lang file
 * 	- add $ipf_cfg["MODULE_TAB"]["SR_MGMT"]['other_reports'] 
 * Date: 2015-10-06 Omas
 * added $sys_custom['iPf']['twghczm']['Report']['SLP'] - personal charater page
 * Date: 2015-08-25 Omas
 * added $sys_custom['iPf']['twghwfns']['Report']['SLP'] - personal ability page
 *********************************************************************/

define("IPF_CFG_SLP_SETTING_PATH", "/home/portfolio/teacher/settings/slp/");
define("IPF_CFG_SLP_SETTING_CNECC_SS", "/home/portfolio/customize/cnecc/");

define("IPF_CFG_SLP_SETTING_OLE", 0);
define("IPF_CFG_SLP_SETTING_SR", 1);
define("IPF_CFG_SLP_SETTING_CWK", 2);
define("IPF_CFG_SLP_SETTING_CNECC_SS", 3);

$ipf_cfg["TOP_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE] = array($ec_iPortfolio['ole']." / ".$iPort["external_record"], IPF_CFG_SLP_SETTING_PATH."ele.php", 0);
$ipf_cfg["TOP_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_SR] = array($ec_iPortfolio['school_record'], IPF_CFG_SLP_SETTING_PATH."sr_year_list.php", 0);

if($sys_custom['iPf']['twghczm']['Report']['SLP']){
	define("IPF_CFG_SLP_SETTING_TWGHCZM", 4);
	$ipf_cfg["TOP_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_TWGHCZM] = array($Lang['iPortfolio']['twghczm_pesonalcharacter'], IPF_CFG_SLP_SETTING_PATH."personal_character_period.php", 0);
}

if($sys_custom['cwk_SLP'])
{
  include_once($intranet_root."/includes/portfolio25/customize/cwk/lang/lang.$intranet_session_language.php");
  $ipf_cfg["TOP_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_CWK] = array($Lang["Cust_Cwk"]["CommonAbility"], IPF_CFG_SLP_SETTING_PATH."cwk_common_ability.php", 0);
}

if($sys_custom['cnecc_SS']){
	$ipf_cfg["TOP_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_CNECC_SS] = array("Sunshine Scheme", IPF_CFG_SLP_SETTING_CNECC_SS."selectYear.php", 0);
}




$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE][0] = array(IPF_CFG_SLP_SETTING_PATH."ele.php", $iPort["ele"], 0);
$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE][1] = array(IPF_CFG_SLP_SETTING_PATH."category/index.php", $iPort["category"], 0);
//$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE][2] = array(IPF_CFG_SLP_SETTING_PATH."mgmt_group/index.php", $Lang['iPortfolio']['mgmtGroup'], 0);
$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE][3] = array(IPF_CFG_SLP_SETTING_PATH."preset_item_template.php?value=".base64_encode('type=4'), $iPort["activity_role_template"], 0);
$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE][6] = array(IPF_CFG_SLP_SETTING_PATH."preset_item_template.php?value=".base64_encode('type=5'), $iPort["activity_awards_template"], 0);
$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE][4] = array(IPF_CFG_SLP_SETTING_PATH."student_submission_period.php", $iPort["student_period_settings"], 0);
$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE][5] = array(IPF_CFG_SLP_SETTING_PATH."record_approval.php", $iPort["record_approval"], 0);
$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE][7] = array(IPF_CFG_SLP_SETTING_PATH."compulsory_field.php", $ec_iPortfolio['SLP']['CompulsoryFields'], 0);
$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_SR][] = array(IPF_CFG_SLP_SETTING_PATH."sr_year_list.php", $ec_iPortfolio['school_profile'], 0);
$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_SR][] = array(IPF_CFG_SLP_SETTING_PATH."subject_full_mark.php?clearCoo=1", $Lang['iPortfolio']['SubjectFullMark'], 0);
$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_SR][] = array(IPF_CFG_SLP_SETTING_PATH."academic_record_display.php", $ec_iPortfolio['academic_result'], 0);

//if($sys_custom['iPf']['twghczm']['Report']['SLP']){
	//$ipf_cfg["MODULE_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_TWGHCZM][0] = array(IPF_CFG_SLP_SETTING_PATH."personal_character.php", $iPort['menu']['personal_attributes'], 0);
//}
/* ==================================================================================================== */

define("IPF_CFG_LP_SETTING_PATH", "/home/portfolio/");
$ipf_cfg["MODULE_TAB"]["LP_SETTING"][] = array(IPF_CFG_LP_SETTING_PATH."contents_admin/learning_portfolio_setting.php", $ec_iPortfolio['peer_review_setting'], 0);
$ipf_cfg["MODULE_TAB"]["LP_SETTING"][] = array(IPF_CFG_LP_SETTING_PATH."teacher/settings/lp/learning_portfolio_setting.php", $Lang['iPortfolio']['LPSetting'], 0);

/* ==================================================================================================== */

define("IPF_CFG_SA_SETTING_PATH", "/home/portfolio/teacher/settings/self_account/");
$ipf_cfg["MODULE_TAB"]["SA_SETTING"][] = array(IPF_CFG_SA_SETTING_PATH."config.php", $Lang['iPortfolio']['SubmissionSetting'], 0);
$ipf_cfg["MODULE_TAB"]["SA_SETTING"][] = array(IPF_CFG_SA_SETTING_PATH."group_list.php", $Lang['iPortfolio']['GroupList'], 0);

/* ==================================================================================================== */

if($sys_custom['cwk_SLP'])
{
  define("IPF_CFG_TC_SETTING_PATH", "/home/portfolio/teacher/settings/teacher_comment/");
  $ipf_cfg["MODULE_TAB"]["TC_SETTING"][] = array(IPF_CFG_TC_SETTING_PATH."config.php", $Lang['iPortfolio']['SubmissionSetting'], 0);
  $ipf_cfg["MODULE_TAB"]["TC_SETTING"][] = array(IPF_CFG_TC_SETTING_PATH."group_list.php", $Lang['iPortfolio']['GroupList'], 0);
}

/* ==================================================================================================== */

define("IPF_CFG_GROUP_SETTING_PATH", "/home/portfolio/teacher/settings/group/");
define("IPF_CFG_GROUP_SETTING_MGMT", 1);
define("IPF_CFG_GROUP_SETTING_STD", 0);
define("IPF_CFG_GROUP_SETTING_TEACHER", 2);
define("IPF_CFG_GROUP_SETTING_ACCESS_RIGHT", 3);
$ipf_cfg["TOP_TAB"]["GROUP_SETTING"][IPF_CFG_GROUP_SETTING_MGMT] = array($Lang['iPortfolio']['mgmtGroup'], IPF_CFG_GROUP_SETTING_PATH."index.php?group_type=".IPF_CFG_GROUP_SETTING_MGMT, 0);
$ipf_cfg["TOP_TAB"]["GROUP_SETTING"][IPF_CFG_GROUP_SETTING_STD] = array($Lang['iPortfolio']['studentGroup'], IPF_CFG_GROUP_SETTING_PATH."index.php?group_type=".IPF_CFG_GROUP_SETTING_STD, 0);
$ipf_cfg["TOP_TAB"]["GROUP_SETTING"][IPF_CFG_GROUP_SETTING_TEACHER] = array($Lang['iPortfolio']['teacherGroup'], IPF_CFG_GROUP_SETTING_PATH."index.php?group_type=".IPF_CFG_GROUP_SETTING_TEACHER."&task=listTeacher", 0);
$ipf_cfg["TOP_TAB"]["GROUP_SETTING"][IPF_CFG_GROUP_SETTING_ACCESS_RIGHT] = array($Lang['iPortfolio']['FunctionAccessRight'], IPF_CFG_GROUP_SETTING_PATH."index.php?group_type=".IPF_CFG_GROUP_SETTING_ACCESS_RIGHT."&task=accessRight", 0);

define("IPF_CFG_GROUP_SETTING_BASIC", 0);
define("IPF_CFG_GROUP_SETTING_RIGHT", 1);
$ipf_cfg["MODULE_TAB"]["GROUP_SETTING"][IPF_CFG_GROUP_SETTING_MGMT][0] = array("javascript:change_edit_type(".IPF_CFG_GROUP_SETTING_BASIC.")", $Lang['iPortfolio']['GroupBasic'], 0);
$ipf_cfg["MODULE_TAB"]["GROUP_SETTING"][IPF_CFG_GROUP_SETTING_MGMT][1] = array("javascript:change_edit_type(".IPF_CFG_GROUP_SETTING_RIGHT.")", $Lang['iPortfolio']['GroupRight'], 0);
$ipf_cfg["MODULE_TAB"]["GROUP_SETTING"][IPF_CFG_GROUP_SETTING_STD][0] = array("javascript:change_edit_type(".IPF_CFG_GROUP_SETTING_BASIC.")", $Lang['iPortfolio']['GroupBasic'], 0);

/* ==================================================================================================== */

# for SLP_MGMT
define("IPF_CFG_SLP_MGMT_PATH", "/home/portfolio/");
define("IPF_CFG_SLP_MGMT_OLE", 0);
define("IPF_CFG_SLP_MGMT_OUTSIDE", 1);
define("IPF_CFG_SLP_MGMT_SUNSHINE", 2); //<-- customization
define("IPF_CFG_SLP_MGMT_CWK_SLP", 3);  //<-- customization
define("IPF_CFG_SLP_MGMT_SCHOOL_RECORD", 4);
define("IPF_CFG_SLP_MGMT_APPROVE_RECORD", 5);
define("IPF_CFG_SLP_MGMT_FYWSS_ATTRIBUTES", 6);
define("IPF_CFG_SLP_MGMT_STPAUL_ACADEMIC", 7);
define("IPF_CFG_SLP_MGMT_CUST_DATA", 8);
define("IPF_CFG_SLP_MGMT_OTHER_REPORT", 9);

//NEW TAB CONFIG , FOR VIEW OLE ONLY
$ipf_cfg["TOP_TAB"]["SLP_MGMT_VIEW"][IPF_CFG_SLP_MGMT_OLE] = array($iPort["internal_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=0", 0);
$ipf_cfg["TOP_TAB"]["SLP_MGMT_VIEW"][IPF_CFG_SLP_MGMT_OUTSIDE] = array($iPort["external_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=1", 0);

$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_OLE] = array($iPort["internal_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=0", 0);
$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_OUTSIDE] = array($iPort["external_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=1", 0);
$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_APPROVE_RECORD] = array($iPort["record_approval"], IPF_CFG_SLP_MGMT_PATH."teacher/management/oleIndex.php?clearCoo=1", 0);
if($sys_custom['cnecc_SS'])
{
	$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_SUNSHINE] = array("Sunshine Scheme", IPF_CFG_SLP_MGMT_PATH."customize/cnecc/", 0);
}
if($sys_custom['cwk_SLP'])
{
  $ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_CWK_SLP] = array($Lang['iPortfolio']['commonAC'], IPF_CFG_SLP_MGMT_PATH."customize/cwk_slp/", 0);
}
if($_SESSION["SESS_OLE_ACTION"] != $ipf_cfg["OLE_ACTION"]["view"] && strstr($ck_function_rights, "ImportData"))
{
	$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_SCHOOL_RECORD] = array($iPort['menu']['school_records'], IPF_CFG_SLP_MGMT_PATH."teacher/data_handling/update_info.php?View=program", 0);
}
if((strstr($ck_function_rights, "Profile") || strstr($ck_function_rights, "Sharing") || strstr($ck_function_rights, "Growth")) && $sys_custom['iPf']['CCCFongYunWahSS']['Report']['StudentLearningProfile'])
{
	$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_FYWSS_ATTRIBUTES] = array($iPort['menu']['personal_attributes'], IPF_CFG_SLP_MGMT_PATH."teacher/data_handling/personal_attributes.php", 0);
}
if(( (strstr($ck_function_rights, "Profile") || strstr($ck_function_rights, "Sharing") ||  $_SESSION['USER_BASIC_INFO']['is_class_teacher']) || strstr($ck_function_rights, "Growth")) && $sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme'])
{
	$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_STPAUL_ACADEMIC] = array($iPort['menu']['academic_points'], IPF_CFG_SLP_MGMT_PATH."teacher/data_handling/academic_points.php", 0);
}
if((strstr($ck_function_rights, "Profile") || strstr($ck_function_rights, "Sharing") || strstr($ck_function_rights, "Growth")) && $sys_custom['iPf']['twghwfns']['Report']['SLP'])
{
	$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_CUST_DATA_WFNS] = array($Lang['iPortfolio']['twghwfns_pesonalability'], IPF_CFG_SLP_MGMT_PATH."teacher/data_handling/twghwfns_slp/personal_ability.php", 0);
}
if( ((strstr($ck_function_rights, "Profile") || strstr($ck_function_rights, "Sharing") || strstr($ck_function_rights, "Growth")) || $_SESSION["USER_BASIC_INFO"]["is_class_teacher"]) && $sys_custom['iPf']['twghczm']['Report']['SLP'])
{
// 	$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_CUST_DATA_CZM] = array($Lang['iPortfolio']['twghczm_pesonalcharacter'], IPF_CFG_SLP_MGMT_PATH."teacher/data_handling/twghczm_slp/personal_character.php", 0);
	$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_CUST_DATA_CZM] = array($Lang['iPortfolio']['twghczm_pesonalcharacter'], IPF_CFG_SLP_MGMT_PATH."teacher/data_handling/twghczm_slp/personal_character/personal_character.php", 0);
	
}
if((strstr($ck_function_rights, "Profile") || strstr($ck_function_rights, "Sharing") || strstr($ck_function_rights, "Growth") || $_SESSION["USER_BASIC_INFO"]["is_class_teacher"]) && $sys_custom['iPf']['sptss']['Report']['SLP'])
{
	$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_CUST_DATA_SPTSS] = array($Lang['iPortfolio']['Cust_sptss']['studentCharacter'], IPF_CFG_SLP_MGMT_PATH."teacher/data_handling/sptss/personal_character.php", 0);
}
if((strstr($ck_function_rights, "Profile") || strstr($ck_function_rights, "Sharing") || strstr($ck_function_rights, "Growth")) && $sys_custom['iPf']['Report']['Other_Report_File'])
{
	$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_OTHER_REPORT] = array($Lang['iPortfolio']['other_reports'], IPF_CFG_SLP_MGMT_PATH."teacher/management/others/index.php", 0);
}


$ipf_cfg["TOP_TAB"]["SLP_MGMT_RPC"][IPF_CFG_SLP_MGMT_OLE] = array($iPort["internal_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole_studentview.php?IntExt=0", 0);
$ipf_cfg["TOP_TAB"]["SLP_MGMT_RPC"][IPF_CFG_SLP_MGMT_OUTSIDE] = array($iPort["external_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole_studentview.php?IntExt=1", 0);


define("IPF_CFG_VOLUNTEER_TEACHER_MGMT_PATH", "/home/portfolio/volunteer/teacher/");
define("IPF_CFG_VOLUNTEER_TEACHER_MGMT_APPROVAL", 0);
define("IPF_CFG_VOLUNTEER_TEACHER_MGMT_EVENT", 1);
$ipf_cfg["TOP_TAB"]["VOLUNTEER_TEACHER_MGMT"][IPF_CFG_VOLUNTEER_TEACHER_MGMT_APPROVAL] = array($iPort["record_approval"], IPF_CFG_VOLUNTEER_TEACHER_MGMT_PATH."?clearCoo=1", 0);
$ipf_cfg["TOP_TAB"]["VOLUNTEER_TEACHER_MGMT"][IPF_CFG_VOLUNTEER_TEACHER_MGMT_EVENT] = array($Lang['iPortfolio']['VolunteerArr']['VolunteerEvent'], IPF_CFG_VOLUNTEER_TEACHER_MGMT_PATH."?task=list_event&clearCoo=1", 0);



$ipf_cfg["MODULE_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_OLE][] = array(IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=0", $ec_iPortfolio['program'], 0);
$ipf_cfg["MODULE_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_OLE][] = array(IPF_CFG_SLP_MGMT_PATH."teacher/management/ole_studentview.php?IntExt=0&clearCoo=1", $i_identity_student, 0);
$ipf_cfg["MODULE_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_OUTSIDE][] = array(IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=1", $ec_iPortfolio['program'], 0);
$ipf_cfg["MODULE_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_OUTSIDE][] = array(IPF_CFG_SLP_MGMT_PATH."teacher/management/ole_studentview.php?IntExt=1&clearCoo=1", $i_identity_student, 0);
$ipf_cfg["MODULE_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_CWK_SLP][] = array(IPF_CFG_SLP_MGMT_PATH."customize/cwk_slp/activity.php", $ec_iPortfolio['activity'], 0);
$ipf_cfg["MODULE_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_CWK_SLP][] = array(IPF_CFG_SLP_MGMT_PATH."customize/cwk_slp/ole_program.php", $ec_iPortfolio['ole']." ".$ec_iPortfolio['programme'], 0);

/*
$ipf_cfg["MODULE_TAB"]["SLP_MGMT"]["OLE"] = array(IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=0", $iPort["internal_record"], 0);
$ipf_cfg["MODULE_TAB"]["SLP_MGMT"]["OUTSIDE"] = array(IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=1", $iPort["external_record"], 0);
if($sys_custom['cnecc_SS'])
{
  $ipf_cfg["MODULE_TAB"]["SLP_MGMT"]["SUNSHINE"] = array(IPF_CFG_SLP_MGMT_PATH."customize/cnecc/", "Sunshine Scheme", 0);
}
if($sys_custom['cwk_SLP'])
{
  $ipf_cfg["MODULE_TAB"]["SLP_MGMT"]["CWK_SLP"] = array(IPF_CFG_SLP_MGMT_PATH."customize/cwk_slp/", $Lang['iPortfolio']['commonAC'], 0);
}
$ipf_cfg["MODULE_TAB"]["SLP_MGMT"]["SCHOOL_RECORD"] = array(IPF_CFG_SLP_MGMT_PATH."teacher/data_handling/update_info.php?View=program", $iPort['menu']['school_records'], 0);
*/

/* ==================================================================================================== */

$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['merit'] = array("javascript:jCHANGE_RECORD_TYPE('merit')", $ec_iPortfolio['title_merit']);
$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['assessment_report'] = array("javascript:jCHANGE_RECORD_TYPE('assessment_report')", $ec_iPortfolio['title_academic_report']);
$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['activity'] = array("javascript:jCHANGE_RECORD_TYPE('activity')", $ec_iPortfolio['activity']);
$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['award'] = array("javascript:jCHANGE_RECORD_TYPE('award')", $ec_iPortfolio['title_award']);
if ($plugin['library_management_system']) {	// eLib+ reading records
	$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['reading_records'] = array("javascript:jCHANGE_RECORD_TYPE('reading_records')", $ec_iPortfolio['title_reading_records']);
}
$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['teacher_comment'] = array("javascript:jCHANGE_RECORD_TYPE('teacher_comment')", $ec_iPortfolio['title_teacher_comments']);
$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['attendance'] = array("javascript:jCHANGE_RECORD_TYPE('attendance')", $ec_iPortfolio['title_attendance']);
$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['service'] = array("javascript:jCHANGE_RECORD_TYPE('service')", $ec_iPortfolio['service']);
$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['ole'] = array("javascript:jCHANGE_RECORD_TYPE('ole')", $ec_iPortfolio['ole']);
$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['plkp'] = array("javascript:jCHANGE_RECORD_TYPE('plkp')", $iPort["external_ole_report"]["performance"]);
if ($sys_custom['iPf']['Report']['Other_Report_File']) { 
	$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['other_reports'] = array("javascript:jCHANGE_RECORD_TYPE('other_reports')", $Lang['iPortfolio']['other_reports']);
}
if($sys_custom['project']['NCS'] || (isset($_SESSION['ncs_role']))){
	$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['assessment'] = array("javascript:jCHANGE_RECORD_TYPE('assessment')", $iPort['menu']['assessment_report']);
	$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['third_party'] = array("javascript:jCHANGE_RECORD_TYPE('third_party')", $iPort['menu']['third_party']);
	$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['pl2_quiz_report'] = array("javascript:jCHANGE_RECORD_TYPE('pl2_quiz_report')", $iPort['menu']['pl2_quiz_report']);
}
/* ==================================================================================================== */

define("IPF_CFG_LP_MGMT_PATH", "/home/portfolio/");
define("IPF_CFG_LP_MGMT_CONTENT", 0);
define("IPF_CFG_LP_MGMT_TEMPLATE", 1);
if(strstr($ck_function_rights, "Sharing:Content") && strstr($ck_user_rights, ":web:")) {
	$ipf_cfg["MODULE_TAB"]["LP_MGMT"][IPF_CFG_LP_MGMT_CONTENT] = array(IPF_CFG_LP_MGMT_PATH."contents_admin/index_scheme.php", $ec_iPortfolio['portfolios']);
}
if(strstr($ck_function_rights, "Sharing:Template") && strstr($ck_user_rights, ":web:")) {
  $ipf_cfg["MODULE_TAB"]["LP_MGMT"][IPF_CFG_LP_MGMT_TEMPLATE] = array(IPF_CFG_LP_MGMT_PATH."learning_portfolio/contents_admin/templates_manage.php", $ec_iPortfolio['templates']);
}

/* ==================================================================================================== */

define("IPF_CFG_ASSESSMENT_STAT_PATH", "/home/portfolio/profile/management/");
$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['score_list'] = array(IPF_CFG_ASSESSMENT_STAT_PATH."score_list.php", $ec_iPortfolio['master_score_list'], 0);
$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['overall_performance'] = array(IPF_CFG_ASSESSMENT_STAT_PATH."overall_performance_stat.php", $ec_iPortfolio['overall_perform_stat'], 0);
if ($sys_custom['iPf']['Report']['AssessmentStatisticReport']['AcademicProgress']) {
	$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['AcademicProgress'] = array(IPF_CFG_ASSESSMENT_STAT_PATH."advancement.php?clearCoo=1", $Lang['iPortfolio']['AcademicProgress'], 0);
}

if ($sys_custom['iPf']['Report']['AssessmentStatisticReport']['CCHPWSS']) {
	$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['AcademicProgressByStd'] = array(IPF_CFG_ASSESSMENT_STAT_PATH."student_progress.php?clearCoo=1", $Lang['iPortfolio']['AcademicProgressByStd'], 0);
	$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['AssessmentResultOfStd'] = array(IPF_CFG_ASSESSMENT_STAT_PATH."student_results.php?clearCoo=1", $Lang['iPortfolio']['AssessmentResultOfStd'], 0);
	$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['SubjectStats'] = array(IPF_CFG_ASSESSMENT_STAT_PATH."subject_stats.php?clearCoo=1", $Lang['iPortfolio']['SubjectStats'], 0);
	$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['TeacherStats'] = array(IPF_CFG_ASSESSMENT_STAT_PATH."teacher_stats_passing.php?clearCoo=1", $Lang['iPortfolio']['TeacherStats'], 0);
	$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['ImprovementStatsBySbj'] = array(IPF_CFG_ASSESSMENT_STAT_PATH."subject_improved.php?clearCoo=1", $Lang['iPortfolio']['ImprovementStatsBySbj'], 0);
	$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['ImprovementStatsByTeacher'] = array(IPF_CFG_ASSESSMENT_STAT_PATH."teacher_stats_improved.php?clearCoo=1", $Lang['iPortfolio']['ImprovementStatsByTeacher'], 0);
}
/* ==================================================================================================== */

define("IPF_CFG_SLP_VIEW_PATH", "/home/portfolio/profile/");
$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['merit'] = array(IPF_CFG_SLP_VIEW_PATH."merit/index.php", $ec_iPortfolio['title_merit'], 0);
$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['assessment_report'] = array(IPF_CFG_SLP_VIEW_PATH."assessment/index.php", $ec_iPortfolio['title_academic_report'], 0);
$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['activity'] = array(IPF_CFG_SLP_VIEW_PATH."activity/index.php", $ec_iPortfolio['activity'], 0);
$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['award'] = array(IPF_CFG_SLP_VIEW_PATH."award/index.php", $ec_iPortfolio['title_award'], 0);
if ($plugin['library_management_system']) {	// eLib+ reading records
	$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['reading_records'] = array(IPF_CFG_SLP_VIEW_PATH."reading_records/index.php", $ec_iPortfolio['title_reading_records'], 0);
}
$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['comment'] = array(IPF_CFG_SLP_VIEW_PATH."comment/index.php", $ec_iPortfolio['title_teacher_comments'], 0);
$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['attendance'] = array(IPF_CFG_SLP_VIEW_PATH."attendance/index.php", $ec_iPortfolio['title_attendance'], 0);
$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['service'] = array(IPF_CFG_SLP_VIEW_PATH."service/index.php", $ec_iPortfolio['service'], 0);
// st paul cust $sys_custom['iPf']['stPaul']['Report']['SLP_PaulinianAwardScheme']
// access right checking is in portfolio25/libpf-tabmenu->getSchoolRecordTags()
$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['stpaul_academic_points'] = array(IPF_CFG_SLP_VIEW_PATH."stpaul_academic_points/index.php", $iPort['menu']['academic_points'], 0);
if($sys_custom['project']['NCS'] || (isset($_SESSION['ncs_role']))){
	$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['assessment'] = array(IPF_CFG_SLP_VIEW_PATH."assessment_report/index.php", $iPort['menu']['assessment_report'], 0);
	$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['third_party'] = array(IPF_CFG_SLP_VIEW_PATH."third_party/index.php", $iPort['menu']['third_party'], 0);
	$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['pl2_quiz_report'] = array(IPF_CFG_SLP_VIEW_PATH."pl2_quiz_report/index.php", $iPort['menu']['pl2_quiz_report'], 0);
}
if($sys_custom['LivingHomeopathy']){
	if(count($ipf_cfg["MODULE_TAB"]["SR_VIEW"])>0)
	{
		foreach($ipf_cfg["MODULE_TAB"]["SR_VIEW"] as $key => $val){
			if(!in_array($key,array('activity','service','award'))) unset($ipf_cfg["MODULE_TAB"]["SR_VIEW"][$key]);
		}
	}
	$ec_iPortfolio['title_homework'] = $Lang['eHomework']['HomeworkRecords'];
	$ipf_cfg["MODULE_TAB"]["SR_VIEW"]['homework'] = array(IPF_CFG_SLP_VIEW_PATH."homework/index.php", $Lang['eHomework']['HomeworkRecords'], 0);
	
	if(isset($ipf_cfg["TOP_TAB"]["SLP_MGMT"])){
		unset($ipf_cfg["TOP_TAB"]["SLP_MGMT"]);
		$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_OLE] = array($iPort["internal_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=0", 0);
		$ipf_cfg["TOP_TAB"]["SLP_MGMT"][IPF_CFG_SLP_MGMT_OUTSIDE] = array($iPort["external_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=1", 0);
	}
	if(isset($ipf_cfg["TOP_TAB"]["SLP_MGMT_VIEW"])){
		unset($ipf_cfg["TOP_TAB"]["SLP_MGMT_VIEW"]);
		$ipf_cfg["TOP_TAB"]["SLP_MGMT_VIEW"][IPF_CFG_SLP_MGMT_OLE] = array($iPort["internal_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=0", 0);
		$ipf_cfg["TOP_TAB"]["SLP_MGMT_VIEW"][IPF_CFG_SLP_MGMT_OUTSIDE] = array($iPort["external_record"], IPF_CFG_SLP_MGMT_PATH."teacher/management/ole.php?IntExt=1", 0);
	}
	
	if(isset($ipf_cfg["MODULE_TAB"]["SR_MGMT"])){
		unset($ipf_cfg["MODULE_TAB"]["SR_MGMT"]);
		$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['activity'] = array("javascript:jCHANGE_RECORD_TYPE('activity')", $ec_iPortfolio['activity']);
		$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['ole'] = array("javascript:jCHANGE_RECORD_TYPE('ole')", $ec_iPortfolio['ole']);
		$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['plkp'] = array("javascript:jCHANGE_RECORD_TYPE('plkp')", $iPort["external_ole_report"]["performance"]);
		$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['homework'] = array("javascript:jCHANGE_RECORD_TYPE('homework')", $Lang['eHomework']['HomeworkRecords']);
	}
	
	if(isset($ipf_cfg["TOP_TAB"]["SLP_SETTING"])){
		unset($ipf_cfg["TOP_TAB"]["SLP_SETTING"]);
		$ipf_cfg["TOP_TAB"]["SLP_SETTING"][IPF_CFG_SLP_SETTING_OLE] = array($ec_iPortfolio['ole']." / ".$iPort["external_record"], IPF_CFG_SLP_SETTING_PATH."ele.php", 0);
	}
}
// [2017-0403-1552-04240] Student & Pastoral Record Tab
if($sys_custom['iPf']['HKUGA_eDis_Tab'] && $_SESSION['UserType'] == 1){
	unset($ipf_cfg["MODULE_TAB"]["SR_MGMT"]['merit']);
	$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['student_record'] = array("javascript:jCHANGE_RECORD_TYPE('student_record')", $eDiscipline['Good_Conduct_and_Misconduct']);
	$ipf_cfg["MODULE_TAB"]["SR_MGMT"]['pastoral_record'] = array("javascript:jCHANGE_RECORD_TYPE('pastoral_record')", $eDiscipline['Award_and_Punishment']);
}
/* ==================================================================================================== */

define("IPF_CFG_STUDENT_REPORT_STAT_PATH", "/home/portfolio/student/");
$ipf_cfg["MODULE_TAB"]["REPORT"]['StudentLearningProfile'] = array(IPF_CFG_STUDENT_REPORT_STAT_PATH."report.php", $iPort['menu']['slp'], 0);
$ipf_cfg["MODULE_TAB"]["REPORT"]['dynamicReport'] = array(IPF_CFG_STUDENT_REPORT_STAT_PATH."dynamicReport.php?clearCoo=1", $iPort['menu']['dynReport'], 0);

/* ==================================================================================================== */
define("IPF_CFG_INTERNAL_VALUE_ADDED_STAT_PATH", "/home/portfolio/profile/management/");
if ($sys_custom['iPf']['Report']['internal_value_added']['MSS']) {
	$ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['INTERNAL_VALUE_ADDED_STAT'] = array(IPF_CFG_INTERNAL_VALUE_ADDED_STAT_PATH."internal_value_added.php?clearCoo=1", $Lang['iPortfolio']['InternalValueAdded'], 0);
}

?>