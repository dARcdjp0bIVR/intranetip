<?php

# Modifing by Shing
//include_once 'libportfolio.php';
//include_once 'libportfolio2007a.php';

//class libpf_sbs extends libportfolio2007
class libpf_sbs 
{
	function GET_SCHOOL_BASED_SCHEME_QUERY($ParArr="")
	{
		global $academic_year_start_month, $academic_year_start_day;
		
		$keyword = $ParArr["keyword"];
		$selectType = $ParArr["selectType"];
		$SchoolYear = $ParArr["SchoolYear"];
		$parent_id = $ParArr["parent_id"];
		$Status = $ParArr["Status"];
		
		if($keyword != "")
			$con = "AND (a.title like '%$keyword%') ";
		else
			$con = "";
		
		if($selectType == "count")
		{
			$fieldname = "count(*) as Num ";
			
			$order_con = "";
		}
		else
		{
			$fieldname .= "count(*) as Num, ";
			$fieldname .= "a.assignment_id, ";
			$fieldname .= "a.title, ";
			$fieldname .= "a.instruction, ";
			$fieldname .= "a.status, ";
			$fieldname .= "a.starttime, ";
			$fieldname .= "a.deadline, ";
			$fieldname .= "a.inputdate, ";
			$fieldname .= "a.modified as modified ";
			
			$order_con = "ORDER BY a.modified DESC ";
		}
		
		if($academic_year_start_month == "")
		$academic_year_start_month = 9;
		
		if($academic_year_start_day == "")
		$academic_year_start_day = 1;
		
		$EndSchoolYear = $SchoolYear + 1;
		
		if($SchoolYear != "" && $SchoolYear != -1)
		{
			$start_date = date('Y-m-d G:i:s', mktime(0, 0, 0, $academic_year_start_month, $academic_year_start_day, $SchoolYear));
			
			$end_date = date('Y-m-d G:i:s', mktime(23, 59, 59, $academic_year_start_month, $academic_year_start_day, $EndSchoolYear));
			$end_date = date("Y-m-d G:i:s", strtotime($end_date)-86400);
			
			$date_con = " AND (a.inputdate BETWEEN '$start_date' AND '$end_date') ";
		}
		else
		{
			$date_con = "";
		}
		
		if($parent_id != "")
		{
			$id_con = " AND a.assignment_id = $parent_id ";
		}
		else
		{
			$id_con = "";
		}
		
		if($Status != "")
		{
			$status_con = " AND a.Status = $Status ";	
		}
		
		$sql  = "SELECT $fieldname FROM $this->course_db.assignment AS a ";
		$sql .= "WHERE a.worktype=6 ";
		$sql .= $con;
		$sql .= $date_con;
		$sql .= $id_con;
		$sql .= $status_con;
		$sql .= "GROUP BY a.assignment_id ";
		$sql .= $order_con;
		
		return $sql;
	} // end function GET SCHOOL BASED SCHEME QUERY
	
	function GET_SCHOOL_BASED_SCHEME_BY_STUDENT_ID_QUERY($ParArr="")
	{
		global $lo, $academic_year_start_month, $academic_year_start_day;
		
		$keyword = $ParArr["keyword"];
		$selectType = $ParArr["selectType"];
		$SchoolYear = $ParArr["SchoolYear"];
		$StudentID = $ParArr["StudentID"];
		$parent_id = $ParArr["parent_id"];
		
		$user_id = $this->getCourseUserID($StudentID);
		
		if($user_id == "")
		$user_id = "-1";
		
		$workList = $lo->returnFunctionIDs("A", $user_id);
		
		if($keyword != "")
			$con = "AND (a.title like '%$keyword%') ";
		else
			$con = "";
		
		if($selectType == "count")
		{
			$fieldname = "count(*) as Num ";
			
			$order_con = "";
		}
		else
		{
			$fieldname .= "count(*) as Num, ";
			$fieldname .= "a.assignment_id, ";
			$fieldname .= "a.title, ";
			$fieldname .= "a.instruction, ";
			$fieldname .= "a.status, ";
			$fieldname .= "a.starttime, ";
			$fieldname .= "a.deadline, ";
			$fieldname .= "a.inputdate, ";
			$fieldname .= "a.modified as modified ";
			
			$order_con = "ORDER BY a.modified DESC ";
		}
		
		if($academic_year_start_month == "")
		$academic_year_start_month = 9;
		
		if($academic_year_start_day == "")
		$academic_year_start_day = 1;
		
		$EndSchoolYear = $SchoolYear + 1;
		
		hdebug_r($SchoolYear);
		hdebug_r($EndSchoolYear);
		
		if($SchoolYear != "" && $SchoolYear != -1)
		{
			$start_date = date('Y-m-d G:i:s', mktime(0, 0, 0, $academic_year_start_month, $academic_year_start_day, $SchoolYear));
			
			$end_date = date('Y-m-d G:i:s', mktime(23, 59, 59, $academic_year_start_month, $academic_year_start_day, $EndSchoolYear));
			$end_date = date("Y-m-d G:i:s", strtotime($end_date)-86400);
			
			//$date_con = " AND (a.inputdate BETWEEN '$start_date' AND '$end_date') ";
			$date_con = " AND (b.starttime >= '$start_date' AND b.deadline <= '$end_date') ";
		}
		else
		{
			$date_con = "";
		}
		
		if($parent_id != "")
		{
			$id_con = " AND a.assignment_id = $parent_id ";
		}
		else
		{
			$id_con = "";
		}
		
		$sql  = "SELECT $fieldname FROM $this->course_db.assignment AS a ";
		$sql .= "LEFT JOIN $this->course_db.assignment AS b ON a.assignment_id = b.parent_id ";
		$sql .= "LEFT JOIN $this->course_db.handin AS c ON (b.assignment_id = c.assignment_id AND c.user_id = '$user_id') ";
		$sql .= "WHERE a.worktype=6 AND a.assignment_id NOT IN ($workList) AND a.status='1' ";
		$sql .= $con;
		$sql .= $date_con;
		$sql .= $id_con;
		$sql .= "GROUP BY a.assignment_id ";
		$sql .= $order_con;
		
		//debug_r(mysql_error());
		
		return $sql;
	} // end function GET SCHOOL BASED SCHEME QUERY
	
	function GET_SCHOOL_BASED_SCHEME($ParArr="")
	{
		global $lpf_gDB;
		$StudentID = $ParArr["StudentID"];
		$Role = $ParArr["Role"];
		$ReturnArr = "";
		
		if($Role == "TEACHER")
		{
			$sql = $this->GET_SCHOOL_BASED_SCHEME_QUERY($ParArr);
		}
		else if($Role == "STUDENT" && $StudentID != "")
		{
			$sql = $this->GET_SCHOOL_BASED_SCHEME_BY_STUDENT_ID_QUERY($ParArr);
		}
		else
		{
			$sql = $this->GET_SCHOOL_BASED_SCHEME_BY_STUDENT_ID_QUERY($ParArr);
		}

		$ReturnArr = $lpf_gDB->returnArray($sql);
		
		//debug_r($ReturnArr);
		
		return $ReturnArr;
	} // end function GET SCHOOL BASED SCHEME
	
	// Get school based scheme
	function GET_SCHEME_TABLE($ParArr="")
	{
		global $lpf;
		
		//debug_r($ParArr);
		
		$SchemeData = $ParArr["SchemeData"];
		
		//debug_r($SchemeData);
		
		$Role = $ParArr["Role"]; // (TEACHER / STUDENT)
		$page_size = $ParArr["page_size"];
		$pageNo = $ParArr["pageNo"];
		
		//debug_r($Role);
		
		$StartIndex = ($pageNo - 1) * $page_size;
		$EndIndex = $StartIndex + $page_size;
		
		if($EndIndex > count($SchemeData))
		$EndIndex = count($SchemeData);
		
		// default phase number display in one row
		if($ParArr["NumOfCol"] != "")
		$NumOfCol = $ParArr["NumOfCol"];
		else
		$NumOfCol = 5; // default to 5
		
		$x = "";

		//debug_r($SchemeData);
		$InputArr["StudentID"] = $ParArr["StudentID"];
		$InputArr["Role"] = $Role;
		
		$row_count = 0;
		
		for($i = $StartIndex; $i < $EndIndex; $i++)
		{
			$InputArr["assignment_id"] = $SchemeData[$i]["assignment_id"];
			$InputArr["title"] = $SchemeData[$i]["title"];
			$InputArr["modified"] = $SchemeData[$i]["modified"];
			$InputArr["deadline"] = $SchemeData[$i]["deadline"];
			$InputArr["starttime"] = $SchemeData[$i]["starttime"];
			$InputArr["instruction"] = $SchemeData[$i]["instruction"];
			$InputArr["status"] = $SchemeData[$i]["status"];
			$InputArr["PhaseData"] = $lpf->GET_SCHOOL_BASED_SCHEME_WITH_PHASE($InputArr);
			
			# [2009-02-04]: check if student keep skip the empty scheme, if admin or teacher shown it
			if($Role == "STUDENT")
			{
				# Eric Yip: To skip schemes which have no students taught by user
				if(empty($InputArr["PhaseData"])) continue;
			}
			
			$NumofRow = ceil(count($InputArr["PhaseData"]) / $NumOfCol);
			$InputArr["RowNum"] = $NumofRow;
			$InputArr["ColNum"] = $NumOfCol;
			
			$x .= $this->GET_SCHEME_LAYOUT_OPEN($InputArr);
			$x .= $this->GET_TABLE_OPEN();
			for($t = 0; $t < $NumofRow; $t++)
			{
				$InputArr["RowIndex"] = $t;

				$x .= $this->GET_SCHEME_ROW_1($InputArr);
				$x .= $this->GET_SCHEME_ROW_2($InputArr);
				$x .= $this->GET_SCHEME_ROW_3($InputArr);
				
				if($Role == "TEACHER")
				{
					$x .= $this->GET_SCHEME_ROW_SUBMITTED($InputArr);
				}
				
				$x .= $this->GET_SCHEME_ROW_4($InputArr);
				$x .= $this->GET_SCHEME_ROW_5($InputArr);
				
				if($t < $NumofRow - 1) 
				$x .= $this->GET_ARROW_LINE($InputArr);
			}
			$x .= $this->GET_TABLE_CLOSE();
			
			$x .= $this->GET_SCHEME_LAYOUT_CLOSE();
			
			if($i < $EndIndex - 1)
			$x .= "<br />";
			$row_count++;
		}
		
		$ReturnArr = array();
		$ReturnArr["content"] = $x;
		$ReturnArr["count"] = $row_count;
		
		return $ReturnArr;
	} // end function get school based scheme
	
	function GET_ARROW_LINE($ParArr="")
	{
		Global $image_path, $LAYOUT_SKIN;
		
		$PhaseData = $ParArr["PhaseData"];
		$RowNum = $ParArr["RowNum"];
		$RowIndex = $ParArr["RowIndex"];
		$ColNum = $ParArr["ColNum"];
		
		/*
		switch($ColNum)
		{
			case 5: $LineNum = 17; break;
			case 4: $LineNum = 13; break;
			case 3: $LineNum = 9; break;
			case 2: $LineNum = 5; break;
			case 1: $LineNum = 1; break;
			default: $LineNum = 17; break;
		}
		*/
		$LineNum = $ColNum * 4 + 1 - 4; // count the column 4 
		
		$StartIndex = ($RowIndex + 1) * $ColNum - 1;
		
		$Status = $this->GET_PHASE_STATUS($PhaseData[$StartIndex]); // done / on / off / none
		$Status_original = $this->GET_PHASE_STATUS_ORIGINAL($PhaseData[$StartIndex]); // done / on / off / none
		
		if($Status == "done" || $Status == "on" || $Status_original != "off")
		$Status = "on";
		else if($Status == "none" || $Status == "off")
		$Status = "off";
		else
		{}
		
		$x = "";
		$x .= "<tr>";
		$x .= "<td height=\"29\">&nbsp;</td>";
		$x .= "<td height=\"29\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$Status."_line.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$Status."2.gif\" width=\"18\" height=\"30\" /></td>";
		
		for($i = 0; $i < $LineNum; $i++)
		{
			$x .= "<td height=\"29\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$Status."_line.gif\">&nbsp;</td>";
		}
		
		$x .= "<td height=\"29\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$Status."_corner_02.gif\" width=\"18\" height=\"30\" /></td>";
		$x .= "<td height=\"29\">&nbsp;</td>";
		$x .= "</tr>";
		
		return $x;
	} // end function get arrow line
	
	function GET_NEW_SCHEME_BUTTON($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $iPort;
		
		$x = "";
//		if($this->IS_IPF_ADMIN())  check with eric
//		{
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
		$x .= "<tr>";
		$x .= "<td><a href=\"new_scheme.php\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">&nbsp;".$iPort["new_scheme"]."</a></td>";
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\"></td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "<td align=\"right\" valign=\"bottom\">&nbsp;</td>";
		$x .= "</tr>";
		$x .= "</table>";
//		}
		return $x;
	} // end function get new phase button
	
	function GET_SELECTION_SEARCH_TABLE($ParArr="")
	{
		$Role = $ParArr["Role"];
		
		$x = "";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		$x .= "<tr><td>";
		
		if($Role == "TEACHER")
		{
			$x .= $this->GET_SELECTION_ACTIVATE($ParArr);
			//$x .= "</td>";
			//$x .= "<td>";
			$x .= $this->GET_SELECTION_SCHOOL_YEAR($ParArr);
			//$x .= "</td>";
		}
		else
		$x .= "&nbsp;";
		
		$x .= "</td>";
		$x .= "<td align=\"right\" valign=\"bottom\">";
		$x .= $this->GET_SEARCH_INPUT($ParArr);
		$x .= "</td></tr></table>";
		
		return $x;
	} // end function get selection box & search input
	
	function GET_SELECTION_ACTIVATE($ParArr="")
	{
		global $linterface;
		global $iPort, $i_general_all;
		
		$Status = $ParArr["Status"];
		$StatusArr = array();
		
		$StatusArr[] = array("", $i_general_all);
		$StatusArr[] = array("1", $iPort["activate"]);
		$StatusArr[] = array("0", $iPort["deactivate"]);
		
		$TmpSelection = $linterface->GET_SELECTION_BOX($StatusArr, "name='Status' id='Status' onChange='jChangeFilterStatus(this);' class='formtextbox'", "", $Status);
		
		$x = "";
		//$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>";
		$x .= $TmpSelection;
		//$x .= "</td></tr></table>";
		return $x;
	}
	
	function GET_SELECTION_SCHOOL_YEAR($ParArr="")
	{
		global $linterface, $lpf, $SchoolYear;
		### Words
		global $iPort;
		
		$ParArr["selectType"] = "";
		$ParArr["SchoolYear"] = "";
		$SchemeData = $lpf->GET_SCHOOL_BASED_SCHEME($ParArr);
		
		$InputDateArr = array();
		for($i = 0; $i < count($SchemeData); $i++)
		{
			$tmpDate = $SchemeData[$i]["inputdate"];
			$InputDateArr[] = substr($tmpDate, 0, 4);
		}
		
		
		// sort the input year
		$YearArr = array();
		for($i = 0; $i < count($InputDateArr); $i++)
		{
			$YearArr[$InputDateArr[$i]] = $InputDateArr[$i];
		}
		
		foreach($YearArr as $key => $val)
		{
			$ReturnSchoolYearArr[]["Year"] = $YearArr[$key];
		}
		
		if(is_array($ReturnSchoolYearArr))
			rsort($ReturnSchoolYearArr);
		
		$SchoolYearArr = array(
		array("-1", $iPort["all_school_years"])
		);

		for($i = 0; $i < count($ReturnSchoolYearArr); $i++)
		{
		
			if(trim($SchoolYearArr[$i]["Year"]) != "")
			$tmpSchoolYearArr = "";
			$tmpSchoolYearArr = array($ReturnSchoolYearArr[$i]["Year"], $ReturnSchoolYearArr[$i]["Year"]);
			$SchoolYearArr[$i+1] = $tmpSchoolYearArr;
		}

		if($SchoolYear == "")
		$SchoolYear = $SchoolYearArr[0][0];
	
		$SchoolYearSelection = $linterface->GET_SELECTION_BOX($SchoolYearArr, "name='SchoolYear' id='SchoolYear' onChange='jChangeSchoolYear(this);' class='formtextbox'", "", $SchoolYear);
		
		$x = "";
		//$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>";
		$x .= $SchoolYearSelection;
		//$x .= "</td></tr></table>";
		return $x;
	} // end function get selection school year
	
	function GET_SEARCH_INPUT($ParArr="")
	{
		global $iPort, $keyword;
		
		if($keyword == "")
		$keyword = $iPort['enter_scheme_name'];
		
		$keyword = "";
		
		$x = "";
		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
		$x .= "<td>";
		$x .= "<span class=\"tabletext\">";
		$x .= "<input name=\"keyword\" id=\"keyword\" type=\"text\" class=\"tabletext\" value=\"".$keyword."\" />";
		$x .= "&nbsp;";
		$x .= "<input name=\"submit332\" type=\"button\" class=\"formsubbutton\" value=\"".$iPort['btn']['search']."\"  onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" onClick=\"jSubmitForm();\">";
		$x .= "</span>";
		$x .= "</td>";
		$x .= "</tr></table>";
		return $x;
	} // end function get selection school year
	
	function GET_NAVIGATION($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $linterface;
		
		// for customize navigation
		$x = "";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
		$x .= "<tr>";
		$x .= "<td class=\"navigation\">";
		
		for($i = 0; $i < count($ParArr); $i++)
		{
			$x .= "<img src=\"$image_path/$LAYOUT_SKIN/nav_arrow.gif\" width=\"15\" height=\"15\" align=\"absmiddle\" />";
			if($ParArr[$i]["1"] == "")
			$x .= "<span class=\"tabletext\">".$ParArr[$i][0]."</span>";
			else
			$x .= "<span class=\"navigation\"><a href=\"".$ParArr[$i][1]."\">".$ParArr[$i][0]."</a></span>";
		}

		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		// for global interface navigation
		//$x = $linterface->GET_NAVIGATION($ParArr);
		
		return $x;
	}
	
	function GET_SCHEME_FORM($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $iPort, $linterface;
		
		$assignment_id = $ParArr["assignment_id"];
		
		$GroupList = $ParArr["GroupList"];
		$GroupListIn = $ParArr["GroupListIn"];
		$GroupListOut = $ParArr["GroupListOut"];
		
		if($GroupListOut == "")
		$GroupListOut = $GroupList;

		$SchemeData = $ParArr["SchemeData"];
		
		if($SchemeData != "")
		{
			$Title = $SchemeData[0]["title"];
			$Instruction = $SchemeData[0]["instruction"];
			$Status = $SchemeData[0]["status"];
		} 
		
		if($Status == 1)
		{
			$Check_Activate = "checked";
			$Check_Deactivate = "";
		}
		else
		{
			$Check_Activate = "";
			$Check_Deactivate = "checked";
		}
		
		$x = "";
		
		$x = "<form name=\"form1\" id=\"form1\" method=\"post\" action=\"update_scheme.php\">";
		
		$x .= "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		
		// title
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$iPort["title"].":<span class=\"tabletextrequire\">*</span></span></td>";
		$x .= "<td width=\"80%\" valign=\"top\"><input name=\"title\" id=\"title\" type=\"text\" class=\"textboxtext\" value=\"".$Title."\" /></td>";
		$x .= "</tr>";
		
		// description
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\"><span class=\"tabletext\">".$iPort["description"]."</span></td>";
		$x .= "<td valign=\"top\">";
		$x .= $linterface->GET_TEXTAREA("instruction", $Instruction);
		$x .= "</tr>";
		
		$x .= "<tr valign=\"top\">";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$iPort["participating_group"]."</span></td>";
		$x .= "<td><label for=\"grading_honor\" class=\"tabletext\"></label>";
		$x .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		$x .= "<tr valign=\"top\">";
		
		// choose group / selected group
		$x .= "<td width=\"40%\"><span class=\"tabletext\">".$iPort["choose_group"]." :</span></td>";
		$x .= "<td width=\"10\" nowrap=\"nowrap\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"10\"></td>";
		$x .= "<td width=\"60%\" nowrap=\"nowrap\"><span class=\"tabletext\">".$iPort["selected_groups"]." :</span></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td valign=\"top\" class=\"tablerow2\">";
		// Choose Group
		$x .= "<select name=\"source[]\" size=\"15\" class=\"select_studentlist\" style=\"width:200px\" multiple>";
		$x .= $GroupListOut;
		$x .= $this->GET_EMPTY_OPTION();
		$x .= "</select>";
		
		$x .= "<td align=\"center\" valign=\"middle\" nowrap=\"nowrap\">";
		$x .= "<input name=\"btn_select\" type=\"button\" class=\"formsubbutton\" value=\"&gt;&gt;\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" onclick=\"jSelectGroup(this, 1)\">";
		$x .= "<br />";
		$x .= "<br />";
		$x .= "<input name=\"btn_deselect\" type=\"button\" class=\"formsubbutton\" value=\"&lt;&lt;\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" onclick=\"jSelectGroup(this, 0)\">";
		$x .= "	</td>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\">";
		$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
		$x .= "<tr>";
		$x .= "<td><p><span class=\"tablerow2\">";
		// Selected Group
		$x .= "<select name=\"target[]\" size=\"15\" class=\"select_studentlist\" style=\"width:200px\" multiple>";
		$x .= $GroupListIn;
		$x .= $this->GET_EMPTY_OPTION();
		$x .= "</select>";
		$x .= "</span></p></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td align=\"left\" class=\"tabletextremark\">".$iPort["msg"]["group_no_selected"]."</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "<label for=\"grading_passfail\" class=\"tabletext\"></label></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$iPort["status"]."</span></td>";
		$x .= "<td><label for=\"grading_passfail\" class=\"tabletext\">";
		$x .= "<input name=\"status\" type=\"radio\" id=\"status_1\" value=\"1\" ".$Check_Activate." />".$iPort["activate"];
		$x .= "<input name=\"status\" type=\"radio\" id=\"status_0\" value=\"0\" ".$Check_Deactivate." />".$iPort["deactivate"]."</label></td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		
		// dot line
		$x .= "<tr>";
		$x .= "<td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>";
		$x .= "</tr>";
		
		$x .= "<tr>";
		$x .= "<td align=\"right\">";
		
		// submit button
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td class=\"tabletextremark\">".$iPort["msg"]["fields_with_asterisk_are_required"]."</td>";
		$x .= "<td align=\"right\">";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmit();", "btnSubmit");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "button", "jReset();", "btnReset");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "jCancel();", "btnCancel");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "<input type=\"hidden\" name=\"assignment_id\" value=\"".$assignment_id."\" />";
		$x .= "</form>";

		return $x;
	} // end function get new scheme form
	
	// get empty option for multiple select
	function GET_EMPTY_OPTION()
	{
		$x = "";
		$x .= "<option>";
		for($i = 0; $i < 30; $i++) 
		$x .= "&nbsp;";
		$x .= "</option>";
		
		return $x;
	}
	
	function GET_SCHEME_SUMMARY($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $iPort, $linterface;
		
		$GroupListIn = $ParArr["GroupListIn"];
		$SchemeData = $ParArr["SchemeData"];
		
		$Title = $SchemeData[0]["title"];
		$Instruction = $SchemeData[0]["instruction"];
		$Status = $SchemeData[0]["status"];
		
		if($Status == 1)
		$Current_Status = $iPort["activate"];
		else
		$Current_Status = $iPort["deactivate"];
		
		$GroupList = "";
		for($i = 0; $i < count($GroupListIn); $i++)
		{
			$GroupList .= $GroupListIn[$i][1]."&nbsp;";
			
			if($i % 5 == 0 && $i != 0)
			$GroupList .= "<br />";
		}
		
		$x = "";
		$x .= "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td width=\"7\" height=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_01_off.gif\" width=\"7\" height=\"7\"></td>";
		$x .= "<td height=\"7\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_02_off.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_02_off.gif\" width=\"7\" height=\"7\"></td>";
		$x .= "<td width=\"7\" height=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_03_off.gif\" width=\"9\" height=\"7\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width=\"7\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_04_off.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_04_off.gif\" width=\"7\" height=\"7\"></td>";
		$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_05_off.gif\"><span style=\"float:left\"><strong>".$Title."</strong></span>";
		$x .= "<br style=\"clear:both\" />";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
		$x .= "<tr>";
		$x .= "<td>".$Instruction."</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"5\">";
		$x .= "<tr>";
		$x .= "<td width=\"30%\" valign=\"top\">".$iPort["participating_group"]."</td>";
		$x .= "<td valign=\"top\" bgcolor=\"#FFFFFF\" >".$GroupList."</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td valign=\"top\">".$iPort["status"]."</td>";
		$x .= "<td valign=\"top\" bgcolor=\"#FFFFFF\">".$Current_Status."</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "<br />";
		$x .= "</td>";
		$x .= "<td width=\"7\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_06_off.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_06_off.gif\" width=\"9\" height=\"7\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width=\"7\" height=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_07_off.gif\" width=\"7\" height=\"7\"></td>";
		$x .= "<td height=\"7\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_08_off.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_08_off.gif\" width=\"9\" height=\"7\"></td>";
		$x .= "<td width=\"7\" height=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_09_off.gif\" width=\"9\" height=\"7\"></td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "<br />";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td align=\"right\">";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td align=\"center\">";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["create_phase_now"], "button", "jGoCreatePhase();", "btnGoPhase");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["back_to_scheme_list"], "button", "jGoSchemeList();", "btnGoScheme");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	} // end function get scheme summary
	
	function GET_ADMIN_PHASE_FORM($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $iPort, $linterface, $lgs;
		global $PATH_WRT_ROOT, $JS_Editor, $plugin, $ec_iPortfolio;
		
		$PhaseData = $ParArr["PhaseData"];
		//debug_r($PhaseData);
		
		$Title = $PhaseData[0]["title"];
		$Instruction = $PhaseData[0]["instruction"];
		// set assignment_id to parent id
		$assignment_id = $ParArr["parent_id"];
		$phase_id = $PhaseData[0]["assignment_id"];
		$MarkingScheme = $PhaseData[0]["person_response"];
		$SubjectID = $PhaseData[0]["teacher_subject_id"];
		$sheettype = $PhaseData[0]["sheettype"];
		$answer_display_mode = $PhaseData[0]["answer_display_mode"];
		
		if($sheettype == 2)
		$contents = $PhaseData[0]["answersheet"]; // informatic form
		else
		$answersheet = $PhaseData[0]["answersheet"];
		
		$StartTime = $PhaseData[0]["starttime"];
		$DeadLine = $PhaseData[0]["deadline"];
		$relevant_id = $PhaseData[0]["answer"];
		
		if($StartTime != "")
		{
			$StartDate = date("Y-m-d", strtotime($StartTime));
			$StartHour = date("H", strtotime($StartTime));
			$StartMin = date("i", strtotime($StartTime));
		}
		if($DeadLine != "")
		{
			$EndDate = date("Y-m-d", strtotime($DeadLine));
			$EndHour = date("H", strtotime($DeadLine));
			$EndMin = date("i", strtotime($DeadLine));
		}
		
		$DefaultStartHour = "00";
		$DefaultStartMin = "00";
		$DefaultEndHour = "23";
		$DefaultEndMin = "59";
		
		if($StartHour == "") $StartHour = $DefaultStartHour;
		if($StartMin == "") $StartMin = $DefaultStartMin;
		if($EndHour == "") $EndHour = $DefaultEndHour;
		if($EndMin == "") $EndMin = $DefaultEndMin;
		
		if($answer_display_mode == 0)
		{
			$AnswerModeCheck_0 = "checked";
			$AnswerModeCheck_1 = "";
		}
		else
		{
			$AnswerModeCheck_0 = "";
			$AnswerModeCheck_1 = "checked";
		}
		
		if($sheettype == 2)
		{
			$SheetFormatCheck_0 = "";
			$SheetFormatCheck_1 = "checked";
			$sheettype_select1 = "selected";
			$sheettype_select0 = "";
			$table_sheettype_style = "display:none;";
			$table_rich_text_style = "display:block;";
			$table_answer_mode_style = "display:none;";
		}
		else if($sheettype == 1)
		{
			$SheetFormatCheck_0 = "checked";
			$SheetFormatCheck_1 = "";
			$sheettype_select1 = "selected";
			$sheettype_select0 = "";
			$table_sheettype_style = "display:block;";
			$table_rich_text_style = "display:none;";
			$table_answer_mode_style = "display:block;";
		}
		else
		{
			$SheetFormatCheck_0 = "checked";
			$SheetFormatCheck_1 = "";
			$sheettype_select1 = "";
			$sheettype_select0 = "selected";
			$table_sheettype_style = "display:block;";
			$table_rich_text_style = "display:none;";
			$table_answer_mode_style = "display:block;";
		}
		
		$x = "";
		$x .= "<form name=\"form1\" action=\"update_phase.php\" method=\"post\">";
		$x .= "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		
		// title
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">";
		$x .= "<span class=\"tabletext\">".$iPort["phase_title"]."<span class=\"tabletextrequire\">*</span></span>";
		$x .= "</td>";
		$x .= "<td width=\"80%\" valign=\"top\"><input name=\"title\" type=\"text\" class=\"textboxtext\" value=\"".$Title."\">";
		$x .= "</td>";
		$x .= "</tr>";
		
		// instruction
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\"><span class=\"tabletext\">".$iPort["description"]."</span></td>";
		$x .= "<td valign=\"top\">";
		$x .= $linterface->GET_TEXTAREA("instruction", $Instruction);
		$x .= "</td>";
		$x .= "</tr>";
		
		// Participants
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\"><span class=\"tabletext\">".$iPort["participants"]."</span></td>";
		$x .= "<td valign=\"top\">";
		$x .= $this->GET_PARTICIPANT_SELECTION($MarkingScheme)."&nbsp;".$this->GET_SUBJECT_TABLE($MarkingScheme, $SubjectID);
		$x .= "</td>";
		$x .= "</tr>";
		
		// period
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\" style=\"border-bottom:1px #EEEEEE solid;\"><span class=\"tabletext\">".$iPort["period"]."</span><span class=\"tabletextrequire\">*</span></td>";
		$x .= "<td valign=\"top\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
		$x .= "<tr>";
		$x .= "<td class=\"tabletext\">".$iPort["start"]."</td>";
		$x .= "<td><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= "<input name=\"starttime\" id=\"starttime\" type=\"text\" class=\"tabletext\" value=\"".$StartDate."\" />";
		$x .= "</td>";
		$x .= "<td width=\"25\" align=\"center\">";
		$x .= $linterface->GET_CALENDAR("form1", "starttime");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td align=\"center\"><input name=\"sh\" type=\"text\" class=\"tabletext\" style=\"width:35px\" value=\"".$StartHour."\"/>";
		$x .= "&nbsp;:</td>";
		$x .= "<td align=\"center\"><input name=\"sm\" type=\"text\" class=\"tabletext\" style=\"width:35px\" value=\"".$StartMin."\"/></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td class=\"tabletext\">".$iPort["end"]."</td>";
		$x .= "<td><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td>";
		$x .= "<input name=\"endtime\" id=\"endtime\" type=\"text\" class=\"tabletext\" value=\"".$EndDate."\" />";
		$x .= "</td>";
		$x .= "<td width=\"25\" align=\"center\">";
		$x .= $linterface->GET_CALENDAR("form1", "endtime");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td align=\"center\"><input name=\"eh\" type=\"text\" class=\"tabletext\" style=\"width:35px\" value=\"".$EndHour."\"/>";
		$x .= "&nbsp;:</td>";
		$x .= "<td align=\"center\"><input name=\"em\" type=\"text\" class=\"tabletext\" style=\"width:35px\" value=\"".$EndMin."\"/></td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "</tr>";
		
		// Related phase
		$x .= "<tr valign=\"top\">";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$iPort["related_phase"]."</span></td>";
		$x .= "<td><label for=\"grading_honor\" class=\"tabletext\"></label>";
		$x .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		$x .= "<tr valign=\"top\">";
		$x .= "<td width=\"40%\"><span class=\"tabletext\">".$iPort["choose_phase"]." :</span></td>";
		$x .= "<td width=\"10\" nowrap=\"nowrap\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"10\"></td>";
		$x .= "<td width=\"60%\" nowrap=\"nowrap\"><span class=\"tabletext\">".$iPort["selected_phase"]." :</span></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td valign=\"top\" class=\"tablerow2\">";
		$x .= "<select name=\"src_phase[]\" size=\"5\" class=\"select_studentlist\" style=\"width:200px\" multiple>";
		$x .= $lgs->GET_NON_SELECTED_RELEVANT_PHASE($assignment_id, $phase_id, $relevant_id, "option");
		$x .= $this->GET_EMPTY_OPTION();
		$x .= "</select>";
		$x .= "<td align=\"center\" valign=\"middle\" nowrap=\"nowrap\">";
		$x .= "<input name=\"btnSelect\" type=\"button\" class=\"formsubbutton\" value=\"&gt;&gt;\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" onclick=\"jSelectPhase(this, 1);\">";
		$x .= "<br />";
		$x .= "<br />";
		$x .= "<input name=\"btnUnselect\" type=\"button\" class=\"formsubbutton\" value=\"&lt;&lt;\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" onclick=\"jSelectPhase(this, 0);\">";
		$x .= "</td>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\"><span class=\"tablerow2\">";
		$x .= "<select name=\"relevant_phase[]\" size=\"5\" class=\"select_studentlist\" style=\"width:200px\" multiple>";
		$x .= $lgs->GET_SELECTED_RELEVANT_PHASE($assignment_id, $phase_id, $relevant_id, "option");
		$x .= $this->GET_EMPTY_OPTION();
		$x .= "</select>";
		$x .= "</span></td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "<label for=\"grading_passfail\" class=\"tabletext\"></label></td>";
		$x .= "</tr>";
		
		// Content
		$x .= "<tr>";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$iPort["contents"]."</span></td>";
		$x .= "<td><label for=\"grading_passfail\" class=\"tabletext\"></label>";
		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td><input name=\"sheet_format\" type=\"radio\" id=\"s_format_0\" value=\"0\" onClick=\"displayTable('table_sheettype', 'block');displayTable('table_rich_text', 'none');displayTable('table_answer_mode', 'block');\" $SheetFormatCheck_0 /></td>";
		$x .= "<td class=\"tabletext\">".$iPort["form"]."</td>";
		$x .= "</tr>";
		$x .= "<tr id=\"table_sheettype\" style=\"".$table_sheettype_style."\">";
		$x .= "<td>&nbsp;</td>";
		$x .= "<td><select name=\"sheettype\">";
		$x .= "<OPTION value=\"1\" $sheettype_select1>".$iPort['form_display_h']."</OPTION>";
		$x .= "<OPTION value=\"0\" $sheettype_select0>".$iPort['form_display_v']."</OPTION>";
		$x .= "</select>";
		$x .= "&nbsp;";
		$x .= "<input name=\"btnCreateForm\" type=\"button\" class=\"formsubbutton\" value=\"".$iPort["create_form"]."\" onclick=\"editOnlineForm();\" onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\">";
		
		# Eric Yip (20090812): Add description to 2 sheet types
		$x .= "&nbsp;";		
		$x .= "<script src=\"".$PATH_WRT_ROOT."templates/oo_tooltip/oo_tooltip.js\" type=\"text/javascript\"></script>";
		$x .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$PATH_WRT_ROOT."templates/oo_tooltip/oo_tooltip.css\" />";
		$x .= "<img src=\"$image_path/icon_alt.gif\" width=\"11\" height=\"14\" onMouseOver=\"tooltip.show('".$ec_iPortfolio['distinct_form_desc']."')\" onMouseOut=\"tooltip.hide()\" />";
		
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "<label for=\"grading_passfail\" class=\"tabletext\">";
		$x .= "<input type=\"radio\" name=\"sheet_format\" id=\"s_format_1\" value=\"1\" onClick=\"displayTable('table_sheettype', 'none');displayTable('table_rich_text', 'block');displayTable('table_answer_mode', 'none');\" $SheetFormatCheck_1 />";
		$x .= $iPort["informatics"];
		$x .= "</label></td>";
		$x .= "</tr>";
		
		$x .= "<tr valign=\"top\" id=\"table_rich_text\" style=\"".$table_rich_text_style."\">";
		$x .= "<td colspan=\"2\">";

		if ($plugin['html_editor'] == true && $plugin['html_editor'] != "")
		{
			$x .= $JS_Editor->GET_HTML_EDITOR("contents", $contents);
		}
		else
		{
			$x .= $linterface->GET_TEXTAREA("contents", $contents);
		}
		$x .= "</td>";
		$x .= "</tr>";
		
		// Answer Display Mode
		$x .= "<tr id=\"table_answer_mode\" style=\"".$table_answer_mode_style."\">";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">".$iPort["answer_display_mode"]."</span></td>";
		$x .= "<td>";
		$x .= "<span class=\"tabletext\">";
		$x .= "<input type=\"radio\" name=\"answer_display_mode\" id=\"answer_mode_0\" value=\"0\" $AnswerModeCheck_0>".$iPort["display_answer_only"];
		$x .= "</span>";
		$x .= "<span class=\"tabletext\">";
		$x .= "<input type=\"radio\" name=\"answer_display_mode\" id=\"answer_mode_1\" value=\"1\" $AnswerModeCheck_1>".$iPort["display_answers_and_choices"];
		$x .= "</span>";
		$x .= "</td>";
		$x .= "</tr>";
		
		$x .= "</table></td>";
		$x .= "</tr>";
		
		// form button
		$x .= "<tr>";
		$x .= "<td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td align=\"right\">";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td class=\"tabletextremark\">".$iPort["msg"]["fields_with_asterisk_are_required"]."</td>";
		$x .= "<td align=\"right\" nowrap>";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit_and_create_phase"], "button", "jSubmitPhase();", "btnSubmitPhase");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit_and_finish"], "button", "jSubmitFinish();", "btnSubmitFinish");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["reset"], "button", "jReset();", "btnReset");
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "jCancel();", "btnCancel");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		$x .= "<input type=\"hidden\" name=\"phase_id\" value=\"".$phase_id."\">";
		$x .= "<input type=\"hidden\" name=\"assignment_id\" value=\"".$assignment_id."\">";
		$x .= "<input type=\"hidden\" name=\"answersheet\" value=\"".$answersheet."\">";
		$x .= "<input type=\"hidden\" name=\"fieldname\" value=\"answersheet\">";
		$x .= "<input type=\"hidden\" name=\"formname\" value=\"form1\">";
		$x .= "<input type=\"hidden\" name=\"NextAction\" value=\"\">";
		
		$x .= "</form>";
		
		return $x;
	} // end function get phase form
	
	function GET_PARTICIPANT_SELECTION($MarkingScheme="")
	{
		$x = "";
		$x .= "<SELECT name='marking_scheme' onChange='if(this.options[this.selectedIndex].value==\"ST\"){displayTable(\"table_subject\", \"block\");}else{displayTable(\"table_subject\", \"none\");}'>";
		$x .= "<OPTION value='S' ".$this->CHECK_SELECTION_INDEX('S', $MarkingScheme).">".$this->GET_ROLE_TYPE("S")."</OPTION>";
		$x .= "<OPTION value='CT' ".$this->CHECK_SELECTION_INDEX('CT', $MarkingScheme).">".$this->GET_ROLE_TYPE("CT")."</OPTION>";
		$x .= "<OPTION value='P' ".$this->CHECK_SELECTION_INDEX('P', $MarkingScheme).">".$this->GET_ROLE_TYPE("P")."</OPTION>";
		$x .= "<OPTION value='ST' ".$this->CHECK_SELECTION_INDEX('ST', $MarkingScheme).">".$this->GET_ROLE_TYPE("ST")."</OPTION>";
		$x .= "</SELECT>";
		
		return $x;
	} // end function get participant selection
	
	// check selected item
	function CHECK_SELECTION_INDEX($Value="", $Selected="")
	{
		if($Value == $Selected)
		return "selected";
		else
		return "";
	}
	
	// Get role type by role id
	function GET_ROLE_TYPE($RoleID="")
	{
		global $iPort;
		
		switch ($RoleID)
		{
			case "CT": $Role = $iPort["usertype_ct"]; break;
			case "P": $Role = $iPort["usertype_p"]; break;
			case "ST": $Role = $iPort["usertype_st"]; break;
			case "admin": $Role = $iPort["usertype_admin"]; break;
			default: $Role = $iPort["usertype_s"]; break;
		}
		return $Role;
	}
	
	function GET_SUBJECT_TABLE($marking_scheme="", $SubjectID="")
	{
		global $iPort, $lgs;
		
		$table_subject_style = ($marking_scheme=="ST") ? "display:block;" : "display:none;";
		$subject_html = $lgs->getSubjectSelection($SubjectID);
		
		$x = "";
		$x .= "<table border=\"0\" id=\"table_subject\" style=\"".$table_subject_style."\">";
		$x .= "<td valign=\"top\" nowrap=\"nowrap\">".$iPort["subject"].":</td>";
		$x .= "<td>".$subject_html."</td>";
		$x .= "</table>";	
		return $x;
	} // end function get subject table
	
	function GET_SCHOOL_BASED_SCHEME_WITH_PHASE($ParArr="")
	{
		global $lgs, $lo, $ck_intranet_user_id;
	
		$sql = $this->GET_SCHOOL_BASED_SCHEME_WITH_PHASE_QUERY($ParArr);
		
		$ReturnArr = $this->returnArray($sql);

		for($i=0; $i<count($ReturnArr); $i++)
		{
			$marking_scheme = $ReturnArr[$i]["marking_scheme"];
			$teacher_subject_id = $ReturnArr[$i]["teacher_subject_id"];
			
			# Eric Yip (20081208): get phase total users and handed-in users explicitly 
			# key [2008-12-12]: separate the class teacher and subject teacher
			# key [2009-02-09]: check the class is in selected group
			# Shing [2009-07-24]: call getSchemeUsers twice, one for class teacher classes, another for subject teacher classes,
			#   in order to handle subject groups properly and the case that a teacher who are both class teacher and subject teacher properly.
			if($this->IS_TEACHER() && !$this->IS_IPF_ADMIN())
			{
				if($marking_scheme == "CT")
				{
					$class_taught_arr = $this->GET_CLASS_TEACHER_CLASS();
					$st_class_taught_arr = array();
				}
				else if($marking_scheme == "ST")
				{
					$class_taught_arr = array();
					$st_class_taught_arr = $this->GET_SUBJECT_TEACHER_CLASS($teacher_subject_id);
				}
				else
				{
					$class_taught_arr = $this->GET_CLASS_TEACHER_CLASS();
					$st_class_taught_arr = $this->GET_SUBJECT_TEACHER_CLASS();
					$teacher_subject_id = "";
				}
			}
			else
			{
				$class_taught_arr = "";
			}
			
			$GroupListInData = $lo->getGroupsListIn_DataValue($ReturnArr[$i]['parent_id'], "A");
			$GroupIDs = array();
			for($j = 0; $j < count($GroupListInData); $j++)
			{
				$GroupIDs[] = $GroupListInData[$j]["group_id"];
			}
			$SelectedClassList = $lo->getClassListByGroupIDList($GroupIDs, $ReturnArr[$i]['parent_id']);

			// if the $class_taught_arr is not in $SelectedClassList, remove it
			if(count($SelectedClassList) > 0)
			{
				if($this->IS_IPF_ADMIN())
				{
					$class_taught_arr = $SelectedClassList;
				}
				else
				{
					if(is_array($class_taught_arr) && is_array($SelectedClassList))
					{
						$class_taught_arr = array_intersect($SelectedClassList, $class_taught_arr);
					}
					if(is_array($st_class_taught_arr) && is_array($SelectedClassList))
					{
						$st_class_taught_arr = array_intersect($SelectedClassList, $st_class_taught_arr);
					}
				}
			}
			
			//debug_r($class_taught_arr);
			
			if (is_array($class_taught_arr)) {
				if (count($class_taught_arr) > 0) {
					$PhaseUsers_ct = $lgs->getSchemeUsers($ReturnArr[$i]['parent_id'], "", $class_taught_arr);
				}
				else {
					$PhaseUsers_ct = array();
				}
				if (count($st_class_taught_arr) > 0) {
					$PhaseUsers_st = $lgs->getSchemeUsers($ReturnArr[$i]['parent_id'], "", $st_class_taught_arr);
					// Only accept students actually taught by the subject teacher only.
					$subject_students = $this->IP_USER_ID_TO_EC_USER_ID_SET_OPERATION($this->GET_SUBJECT_TEACHER_STUDENT($ck_intranet_user_id, $teacher_subject_id));
					$PhaseUsers_st = array_intersect($PhaseUsers_st, $subject_students);
				}
				else {
					$PhaseUsers_st = array();
				}
				$PhaseUsers = array_unique(array_merge($PhaseUsers_ct, $PhaseUsers_st));
			}
			else {
				$PhaseUsers = $lgs->getSchemeUsers($ReturnArr[$i]['parent_id']);
			}
			$HandinUsers = $lgs->getHandedinUser($ReturnArr[$i]['assignment_id']);
			
			$ReturnArr[$i][7] = $ReturnArr[$i]['TotalUser'] = count($PhaseUsers);
			$ReturnArr[$i][8] = $ReturnArr[$i]['HandinNum'] = count(array_intersect($HandinUsers, $PhaseUsers));
		}

		return $ReturnArr;
	} // end function GET SCHOOL BASED SCHEME WITH PHASE
	
	function GET_SCHOOL_BASED_SCHEME_WITH_PHASE_QUERY($ParArr="")
	{
		global $lgs;
		
		$assignment_id = ($ParArr["use_parent_id"])? $ParArr["parent_id"]:$ParArr["assignment_id"];
		
		//debug_r($ParArr);
		
		# get total number of user have to fill the phase
		if($this->IS_TEACHER() && !$this->IS_IPF_ADMIN())
			$class_taught_arr = array_merge($this->GET_SUBJECT_TEACHER_CLASS(), $this->GET_CLASS_TEACHER_CLASS());
		else
			$class_taught_arr = "";
				
		$PhaseUsers = $lgs->getSchemeUsers($assignment_id, "", $class_taught_arr);
		
		if($class_taught_arr != "")	
//		if(sizeof($PhaseUsers))
		{
			$UserList = implode(",", $PhaseUsers);
			$TotalUser = sizeof($PhaseUsers);
			$conds = " AND b.user_id IN ({$UserList})";
		}
		else
		{
			//$conds = " AND b.user_id IN ({$UserList})";
//			$TotalUser = 0;
			$TotalUser = sizeof($PhaseUsers);
			$conds = "";
		}
		
		$fieldname  = "a.title, ";
		$fieldname .= "a.assignment_id, ";
		$fieldname .= "a.starttime, ";
		$fieldname .= "a.deadline, ";
		$fieldname .= "a.marking_scheme, ";
		$fieldname .= "a.modified, ";
		$fieldname .= "a.instruction, ";
		$fieldname .= "$TotalUser as TotalUser, ";
		$fieldname .= "COUNT(b.handin_id) as HandinNum, ";
		$fieldname .= "a.parent_id, ";
		$fieldname .= "a.teacher_subject_id ";
		
		$sql = "SELECT $fieldname FROM $this->course_db.assignment AS a ";
		$sql .= "LEFT JOIN $this->course_db.handin as b ON a.assignment_id = b.assignment_id {$conds} ";
		$sql .= "WHERE a.parent_id='$assignment_id' ";
		$sql .= "GROUP BY a.assignment_id ";
		
		# Eric Yip: Sort by Start Time
		$sql .= "ORDER BY a.starttime ASC ";

		return $sql;
	} // end function GET SCHOOL BASED SCHEME WITH PHASE QUERY
	
	// get scheme layout open
	function GET_SCHEME_LAYOUT_OPEN($ParArr="")
	{		
		Global $image_path, $LAYOUT_SKIN, $iPort;
		
		$Title = $ParArr["title"];
		$Modified = $ParArr["modified"];
		$Instruction = $ParArr["instruction"];
		$Role = $ParArr["Role"];
		$Status = $ParArr["status"];
		$assignment_id = $ParArr["assignment_id"];
		$CurrentPage = $ParArr["CurrentPage"];
		$SchemeTitle = $ParArr["SchemeTitle"];
		
		if($CurrentPage == "PhaseView")
		$Title = $SchemeTitle;
		
		
		if($Instruction == "")
		$Instruction = "&nbsp;";
		
		if($Status == 0)
		{
			$SchemeLayout = "_off";
			$BtnStatus = $iPort["btn"]["activate"];
			$AlertMsg = $iPort["msg"]["confirm_activate"];
			$BtnClass = "scheme_act_btn_activate";
		}
		else
		{
			$Status = 1;
			$SchemeLayout = "";
			$BtnStatus = $iPort["btn"]["deactivate"];
			$AlertMsg = $iPort["msg"]["confirm_deactivate"];
			$BtnClass = "scheme_act_btn_deactivate";
		}
		
		$x = "";
		
		$x .= "<a name=\"scheme_$assignment_id\"></a>";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td width=\"7\" height=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_01".$SchemeLayout.".gif\" width=\"7\" height=\"7\"></td>";
		$x .= "<td height=\"7\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_02".$SchemeLayout.".gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_02".$SchemeLayout.".gif\" width=\"7\" height=\"7\"></td>";
		$x .= "<td width=\"7\" height=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_03".$SchemeLayout.".gif\" width=\"9\" height=\"7\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width=\"7\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_04".$SchemeLayout.".gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_04".$SchemeLayout.".gif\" width=\"7\" height=\"7\"></td>";
		$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_05".$SchemeLayout.".gif\"><span style=\"float:left\"><strong>".$Title."</strong></span>";
		
		if($Role == "TEACHER" && $CurrentPage != "PhaseView" && $this->IS_IPF_ADMIN())
		{
			$x .= "<span style=\"float:right;\" class=\"scheme_act_btn $BtnClass\">";
			$x .= "<a href=\"#scheme_$assignment_id\" onclick=\"jChangeStatus($Status, $assignment_id, '".$AlertMsg."');\">".$BtnStatus."</a>";
			$x .= "</span>";									
			$x .= "<span style=\"float:right;padding-right:10px\">";
			$x .= "<a href=\"copy_scheme.php?assignment_id=$assignment_id\"><img src=\"$image_path/$LAYOUT_SKIN/icon_copy_b.gif\" alt=\"".$iPort["btn"]["copy"]."\" border=\"0\"></a>";
			$x .= "<a href=\"javascript:jExportAll(".$assignment_id.")\"><img src=\"$image_path/$LAYOUT_SKIN/icon_export.gif\" alt=\"".$iPort["export_result"]."\" border=\"0\"></a>";
			$x .= "<a href=\"edit_scheme.php?assignment_id=$assignment_id\"><img src=\"$image_path/$LAYOUT_SKIN/icon_edit_b.gif\" alt=\"".$iPort["btn"]["edit"]."\" border=\"0\"></a>";
			$x .= "<img src=\"$image_path/$LAYOUT_SKIN/icon_delete_b.gif\" alt=\"".$iPort["btn"]["delete"]."\" border=\"0\" onMouseOver=\"style.cursor='hand'\" onclick=\"jCheckRemoveScheme('".$iPort["msg"]["confirm_delete"]."', $assignment_id);\">";
			$x .= "</span>";
			$x .= "<br style=\"clear:both\" />";
		}	
		
		$x .= "<span style=\"float:right\">".$iPort["last_updated"]." : ".$Modified."</span><br />";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"clear:both\">";
		$x .= "<tr>";
		$x .= "<td width=\"9\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_01.gif\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"10\"></td>";
		$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_02.gif\" >";
		$x .= "<div style=\" background-image:url($image_path/$LAYOUT_SKIN/iPortfolio/scheme/icon_clip.gif);background-repeat:no-repeat;padding-left:15px; padding-top:10px; padding-bottom:10px\">".$Instruction."</div>";
		$x .= "</td>";
		$x .= "<td width=\"8\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_03.gif\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"8\" height=\"10\"></td>";
		$x .= "</tr>";
		
		if($Role == "TEACHER" && $CurrentPage != "PhaseView" && $this->IS_IPF_ADMIN())
		{
			$x .= $this->GET_NEW_PHASE_BUTTON($ParArr);
		}
		
		$x .= "<tr>";
		$x .= "<td width=\"9\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_04.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_04.gif\" width=\"9\" height=\"10\"></td>";
		$x .= "<td bgcolor=\"#FFFFFF\" align=\"center\">";
		
		return $x;
	} // end function get scheme layout open
	
	// get scheme layout close
	function GET_SCHEME_LAYOUT_CLOSE($ParArr="")
	{	
		global $image_path, $LAYOUT_SKIN;
		$Status = $ParArr["status"];
		
		if($Status == 0)
		{
			$SchemeLayout = "_off";
		}
		else
		{
			$Status = 1;
			$SchemeLayout = "";
		}
		
		$x = "";
		
		$x .= "</td>";
		$x .= "<td width=\"8\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_06.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_06.gif\" width=\"8\" height=\"11\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width=\"9\" height=\"8\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_07.gif\" width=\"9\" height=\"8\"></td>";
		$x .= "<td height=\"8\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_08.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_08.gif\" width=\"9\" height=\"8\"></td>";
		$x .= "<td width=\"8\" height=\"8\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_09.gif\" width=\"8\" height=\"8\"></td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "<td width=\"7\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_06".$SchemeLayout.".gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_06".$SchemeLayout.".gif\" width=\"9\" height=\"7\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td width=\"7\" height=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_07".$SchemeLayout.".gif\" width=\"7\" height=\"7\"></td>";
		$x .= "<td height=\"7\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_08".$SchemeLayout.".gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_08".$SchemeLayout.".gif\" width=\"9\" height=\"7\"></td>";
		$x .= "<td width=\"7\" height=\"7\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_09".$SchemeLayout.".gif\" width=\"9\" height=\"7\"></td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	} // end function get scheme layout close
	
	// Scheme Row 1
	function GET_SCHEME_ROW_1($ParArr="")
	{
		Global $image_path, $LAYOUT_SKIN;
		
		$PhaseData = $ParArr["PhaseData"];
		$RowNum = $ParArr["RowNum"];
		$RowIndex = $ParArr["RowIndex"];
		$ColNum = $ParArr["ColNum"];
		$Role = $ParArr["Role"];
		$parent_id = $ParArr["assignment_id"];
		$StartIndex = $RowIndex * $ColNum;
		$count = $StartIndex;
		
		$x = "";
		$x .= "<tr valign=\"bottom\">";
		
		$InputArr["parent_id"] = $parent_id;
		
		for($i = 0; $i < $ColNum; $i++)
		{
			$Status = $this->GET_PHASE_STATUS($PhaseData[$count]); // done / on / off / none
			$assignment_id = $PhaseData[$count]["assignment_id"];
			$InputArr["Status"] = $Status;
			$InputArr["assignment_id"] = $assignment_id;
			
			if($Status == "on" || $Status == "off" || $Status == "done")
			{
				$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_01.gif\" width=\"9\" height=\"9\" /></td>";
				$x .= "<td height=\"18\" align=\"right\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_02.gif\" style=\"background-repeat:repeat-x;background-position:bottom\">";
				
				if($Role == "TEACHER")
				{
					$x .= $this->GET_PHASE_BUTTON($InputArr);
				}
				
				$x .= "</td>";
				
				$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_03.gif\" width=\"9\" height=\"9\" /></td>";
				$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"9\" /></td>";
			}
			else
			{
				if($RowNum == 1)
				break;
				
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
			}
			$count++;
		}
		
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"9\"></td>";
		$x .= "</tr>";
		
		return $x;
	}
	
	// Scheme Row 2 - set phase title
	function GET_SCHEME_ROW_2($ParArr="")
	{
		Global $image_path, $LAYOUT_SKIN, $iPort, $lgs, $ck_memberType;
		
		$PhaseData = $ParArr["PhaseData"];
		$RowNum = $ParArr["RowNum"];
		$RowIndex = $ParArr["RowIndex"];
		$ColNum = $ParArr["ColNum"];
		$parent_id = $ParArr["assignment_id"];
		$Role = $ParArr["Role"];
		$user_id = $this->getCourseUserID($ParArr["StudentID"]);
		$StartIndex = $RowIndex * $ColNum;
		$count = $StartIndex;
		
		$x = "";
		$x .= "<tr>";
		
		for($i = 0; $i < $ColNum; $i++)
		{
			$marking_scheme = $PhaseData[$count]["marking_scheme"];
			$Title = $PhaseData[$count]["title"];
			$assignment_id = $PhaseData[$count]["assignment_id"];
			
			$Status = $this->GET_PHASE_STATUS($PhaseData[$count]); // done / on / off / none
			$Status_original = $this->GET_PHASE_STATUS_ORIGINAL($PhaseData[$count]); // done / on / off / none
			
			$phase_obj = $lgs->getPhaseInfo($assignment_id); // assignment_id also is phase_id
			$phase_obj = $phase_obj[0];
			
			//if($marking_scheme == "CT")
			//$ignore_classname = true;
			//else if($marking_scheme == "ST")
			//$ignore_subject = true;
			
			$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj, $ignore_classname, $ignore_subject);

			# get handin object
			if($user_id != "")
			$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
			else
			$handin_obj = "";
			
			// class teacher or subject teacher
			if($marking_scheme == "CT" || $marking_scheme == "ST")
			{
				if($person_right == "DO")
				{
					$handin_obj = $lgs->getTeacherPhaseHandin($phase_obj);
					//debug_r($handin_obj);
					$IpfStudentID = $handin_obj[5];
					$ParStudentID = $handin_obj[6];
					$ParClassName = $lgs->returnClassOfStudent($IpfStudentID);
					$InputArr["StudentID"] = $ParStudentID;
					$InputArr["ClassName"] = $ParClassName;
				}
			}
			
			$ModifiedDate = $handin_obj[4];
			$phase_id = $assignment_id;
			$relevant_id = $phase_obj["answer"];
			
			$RelatedPhaseList = $lgs->GET_SELECTED_RELEVANT_PHASE($parent_id, $phase_id, $relevant_id, "array");
			
			//debug_r($RelatedPhaseList);
			
			if($Status == "on" || $Status == "off" || $Status == "done")
			{
				if($Status == "on")
				$TitleStatus = "on";
				else
				$TitleStatus = "off";
				
				// link
				if($Role == "TEACHER")
				{
					if($Status != "off" || $Status_original != "off")
					$CurrentLink = "phase_form.php?assignment_id=$assignment_id&parent_id=$parent_id";
					else
					$CurrentLink = "javascript:void(0)";
				}
				else
				{
					if($Status == "done")
					{
						$CurrentLink = "phase_form.php?Mode=view&assignment_id=$assignment_id&parent_id=$parent_id";
						
						# Eric Yip (20090218): Teacher use student identity to view student phase
						if($ck_memberType == "T")
						{
							$stu_obj = $this->returnUserNameAndClass($ParArr['StudentID']);
							$CurrentLink .= "&StudentID=".$ParArr['StudentID']."&ClassName=".$stu_obj['ClassName'];
						}
					}
					else if($Status == "on" && $ModifiedDate == "")
					$CurrentLink = "phase_form.php?Mode=edit&assignment_id=$assignment_id&parent_id=$parent_id";
					else
					{
						$CurrentLink = "phase_form.php?Mode=view&assignment_id=$assignment_id&parent_id=$parent_id";
						
						# Eric Yip (20090218): Teacher use student identity to view phase 
						if($ck_memberType == "T")
						{
							$stu_obj = $this->returnUserNameAndClass($ParArr['StudentID']);
							$CurrentLink .= "&StudentID=".$ParArr['StudentID']."&ClassName=".$stu_obj['ClassName'];
						}
					}
				}
				
				// icon
				if($Status == "on")
				$icon = "<img src=\"$image_path/$LAYOUT_SKIN/icon_edit_b.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">";
				else
				$icon = "";
				
				$x .= "<td width=\"9\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\" width=\"9\" height=\"9\" /></td>";
				
				$x .= "<td valign=\"top\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_05.gif\">";
				
				//  title
				//if($Role == "STUDENT" && ($marking_scheme == "CT" || $marking_scheme == "ST") && $Status != "done")
				//$x .= $Title;
				if($Status == "off" && $Status_original == "off")
				$x .= $Title;
				else if($Status == "on" && $ModifiedDate != "" && ($marking_scheme == "S" || $marking_scheme == "P") && $Role == "STUDENT")
				$x .= "<strong>".$Title."</strong>";
				else
				$x .= "<a href=\"".$CurrentLink."\" class=\"phase_title_".$TitleStatus."\">".$Title.$icon."</a>";
				
				// related phase
				//hdebug_r($RelatedPhaseList);
				for($j = 0;  $j < count($RelatedPhaseList); $j++)
				{
					$TmpPhaseId = $RelatedPhaseList[$j][0];
					$TmpPhaseTitle = $RelatedPhaseList[$j][1];
					$TmpMarkingScheme = $RelatedPhaseList[$j][2];
					
					$TmpViewLink = "phase_form.php?assignment_id=".$TmpPhaseId."&parent_id=".$parent_id;
					
					# Eric Yip (20090218): Teacher use student identity to view phase 
					if($ck_memberType == "T")
					{
						$stu_obj = $this->returnUserNameAndClass($ParArr['StudentID']);
						$TmpViewLink .= "&StudentID=".$ParArr['StudentID']."&ClassName=".$stu_obj['ClassName'];
					}
					
					//if($Role == "STUDENT" && ($TmpMarkingScheme == "CT" || $TmpMarkingScheme == "ST") && $Status != "done")
					//$x .= "<br /><span title=\"".$iPort["related_to"]." ".$TmpPhaseTitle."\"><img src=\"$image_path/$LAYOUT_SKIN/icon_arrow.gif\" width=\"15\" height=\"15\" border=\"0\" align=\"absmiddle\">".$TmpPhaseTitle."</span>";
					//else
					$x .= "<br /><a href=\"".$TmpViewLink."\" class=\"tabletool\" title=\"".$iPort["related_to"]." ".$TmpPhaseTitle."\"><img src=\"$image_path/$LAYOUT_SKIN/icon_arrow.gif\" width=\"15\" height=\"15\" border=\"0\" align=\"absmiddle\">".$TmpPhaseTitle."</a>";
				}
				
				if(count($RelatedPhaseList) > 0)
				$x .= "<br />";
				else
				$x .= "<br /><br />";
				
				$x .= "</td>";
				
				$x .= "<td width=\"9\" valign=\"middle\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_06.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_06.gif\" width=\"9\" height=\"9\" /></td>";
				$x .= "<td width=\"18\" valign=\"middle\">&nbsp;</td>";
			}
			else
			{
				break;
			}
			$count++;
		} // end for
		
		if($count != 0 && $RowNum == $RowIndex + 1)
		{
			if($i < $ColNum && $RowNum != 1)
			{
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
			}
			$x .= "<td>".$iPort["finished"]."</td>";
		}

		if($RowNum != 1)
		{
			$LeftCol = $ColNum - $i;
			for($i = 0; $i < $LeftCol; $i++)
			{
				if($i > 0)
				{
					$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
					$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				}
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
			}
		}
		
		if($RowNum == $RowIndex + 1 && $LeftCol == 0)
		{}
		else
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
		
		$x .= "</tr>";
		
		return $x;
	}
	
	// Scheme Row 3 - set phase arrow & role title
	function GET_SCHEME_ROW_3($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $ck_memberType;
		global $iPort, $lgs;
		
		$Role = $ParArr["Role"];
		$PhaseData = $ParArr["PhaseData"];
		$RowNum = $ParArr["RowNum"];
		$RowIndex = $ParArr["RowIndex"];
		$ColNum = $ParArr["ColNum"];
		$StartIndex = $RowIndex * $ColNum;
		$count = $StartIndex;
		
		$InputArr["StudentID"] = $ParArr["StudentID"];
		$InputArr["parent_id"] = $ParArr["assignment_id"];
		
		$user_id = $this->getCourseUserID($ParArr["StudentID"]);
		
		$x = "";
		$x .= "<tr valign=\"top\">";
		
		for($i = 0; $i < $ColNum; $i++)
		{
			$marking_scheme = $PhaseData[$count]["marking_scheme"];
			$TmpRole = $this->GET_ROLE_TYPE($PhaseData[$count]["marking_scheme"]);
			$RoleImage = $this->GET_ROLE_IMAGE($PhaseData[$count]["marking_scheme"]);
			$Status = $this->GET_PHASE_STATUS($PhaseData[$count]); // done / on / off / none
			$Status_original = $this->GET_PHASE_STATUS_ORIGINAL($PhaseData[$count]); // done / on / off / none
			$phase_id = $PhaseData[$count]["assignment_id"];
			
			$phase_obj = $lgs->getPhaseInfo($phase_id); // assignment_id also is phase_id
			$phase_obj = $phase_obj[0];
			
			//if($marking_scheme == "CT")
			//$ignore_classname = true;
			//else if($marking_scheme == "ST")
			//$ignore_subject = true;
			
			$person_right = $lgs->getPhaseRight($ck_memberType, $phase_obj, $ignore_classname, $ignore_subject);
			# get handin object
			if($user_id != "")
			$handin_obj = $lgs->getStudentPhaseHandin($user_id, $phase_obj);
			else
			$handin_obj = "";
			
			// class teacher or subject teacher
			if($marking_scheme == "CT" || $marking_scheme == "ST")
			{
				if($person_right == "DO")
				{
					$handin_obj = $lgs->getTeacherPhaseHandin($phase_obj);
					//debug_r($handin_obj);
					$IpfStudentID = $handin_obj[5];
					$ParStudentID = $handin_obj[6];
					$ParClassName = $lgs->returnClassOfStudent($IpfStudentID);
					$InputArr["StudentID"] = $ParStudentID;
					$InputArr["ClassName"] = $ParClassName;
				}
			}
			
			//hdebug_r($handin_obj);
			$InputArr["Status"] = $Status;
			$InputArr["phase_id"] = $phase_id;
			$InputArr["inputdate"] = $handin_obj[4];
			$InputArr["marking_scheme"] = $marking_scheme;
			$InputArr["Role"] = $Role;
			
			if($Status == "on" || $Status == "off" || $Status == "done")
			{
				if($Status == "on" || $Status == "done" || $Status_original != "off")
				$ArrowStatus = "on";
				else
				$ArrowStatus = "off";
				
				if($i == 0)
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\">&nbsp;</td>";
				else
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$ArrowStatus."_line.gif\" alt=\"\" width=\"9\" height=\"30\" /></td>";
				
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_05.gif\">";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				$x .= "<tr>";
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$ArrowStatus."_line.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/".$RoleImage."\" width=\"25\" height=\"29\" style=\" background-position:inherit;  background-repeat:repeat-x\" /></td>";
				$x .= "</tr>";
				$x .= "</table>";
				$x .= $TmpRole;
				$x .= "<br />";
	
				$x .= $this->GET_LAST_SUBMISSION($InputArr);
				
				$x .= "<br /></td>";
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_06.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$ArrowStatus."_line.gif\" width=\"9\" height=\"30\" /></td>";
				
				if($i < $ColNum - 1 || $RowNum == $RowIndex + 1)
				$x .= "<td width=\"18\" align=\"left\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$ArrowStatus.".gif\" width=\"18\" height=\"30\" /></td>";
				else
				$MissCol = 1;
			}
			else
			{
				break;
			}
			$count++;
		}
		
		if($count != 0 && $RowNum == $RowIndex + 1)
		{
			if($i < $ColNum && $RowNum != 1)
			{
				$x .= "<td>&nbsp;</td>";
			}
			$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/icon_scheme_off.gif\" width=\"25\" height=\"25\"></td>";
		}
		else
		{
			if($Status == "done" || $Status == "on" || $Status_original != "off")
			$ArrowStatus = "on";
			else if($Status == "none" || $Status == "off")
			$ArrowStatus = "off";
			else
			{}
			
			$x .= "<td align=\"left\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$ArrowStatus."_vert_line.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$ArrowStatus."_corner_01.gif\" width=\"18\" height=\"30\" /></td>";
		}
		
		if($RowNum != 1)
		{
			$LeftCol = $ColNum - $i;
			for($i = 0; $i < $LeftCol; $i++)
			{
				if($i > 0)
				{
					$x .= "<td width=\"9\" height=\"9\">&nbsp;</td>";
					$x .= "<td height=\"9\">&nbsp;</td>";
				}
				$x .= "<td width=\"9\" height=\"9\">&nbsp;</td>";
				$x .= "<td width=\"18\" height=\"9\">&nbsp;</td>";
			}
		}
		
		if($RowNum == $RowIndex + 1 && $LeftCol == 0)
		{}
		else
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
		
		$x .= "</tr>";
		
		return $x;
	}

	// Scheme Row 4 - image frame
	function GET_SCHEME_ROW_4($ParArr="")
	{
		Global $image_path, $LAYOUT_SKIN;
		
		$PhaseData = $ParArr["PhaseData"];
		$RowNum = $ParArr["RowNum"];
		$RowIndex = $ParArr["RowIndex"];
		$ColNum = $ParArr["ColNum"];
		$StartIndex = $RowIndex * $ColNum;
		$count = $StartIndex;
		
		$x = "";
		$x .= "<tr>";
		
		for($i = 0; $i < $ColNum; $i++)
		{
			$Status = $this->GET_PHASE_STATUS($PhaseData[$count]); // done / on / off / none
			$Status_original = $this->GET_PHASE_STATUS_ORIGINAL($PhaseData[$count]); // done / on / off / none
			
			if($Status == "on" || $Status == "off" || $Status == "done")
			{
				$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_07.gif\" width=\"9\" height=\"9\" /></td>";
				$x .= "<td height=\"9\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_08.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_08.gif\" width=\"9\" height=\"9\" /></td>";
				$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_09.gif\" width=\"9\" height=\"9\" /></td>";
			
				if($i < $ColNum - 1)
				$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"9\" /></td>";
				else
				$MissCol = 1;
			}
			else
			{
				break;	
			}
			$count++;	
		}
		
		if($count != 0 && $RowNum == $RowIndex + 1)
		{
			// do nth
		}
		else
		{
			if($Status == "done" || $Status == "on" || $Status_original != "off")
			$ArrowStatus = "on";
			else if($Status == "none" || $Status == "off")
			$ArrowStatus = "off";
			else
			{}
			
			$AddCOl = 1;
			$x .= "<td height=\"9\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$ArrowStatus."_vert_line.gif\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"9\" /></td>";
		}
		
		// space for extra end
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
		
		if($RowNum != 1)
		{
			$LeftCol = $ColNum - $i;
			for($i = 0; $i < $LeftCol; $i++)
			{
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
			}
		}
		
		if($MissCol == 1 && $AddCOl != 1)
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
		
		$x .= "</tr>";
		
		return $x;
	}
	
	// Scheme Row 5 - set date period
	function GET_SCHEME_ROW_5($ParArr="")
	{
		Global $image_path, $LAYOUT_SKIN;
		
		$PhaseData = $ParArr["PhaseData"];
		$RowNum = $ParArr["RowNum"];
		$RowIndex = $ParArr["RowIndex"];
		$ColNum = $ParArr["ColNum"];
		$StartIndex = $RowIndex * $ColNum;
		$count = $StartIndex;
		
		$x = "";
		$x .= "<tr>";
		
		for($i = 0; $i < $ColNum; $i++)
		{
			$starttime = $this->GET_DATE_DISPLAY($PhaseData[$count]["starttime"]);
			$deadline = $this->GET_DATE_DISPLAY($PhaseData[$count]["deadline"]);
			$Status = $this->GET_PHASE_STATUS($PhaseData[$count]); // done / on / off / none
			$Status_original = $this->GET_PHASE_STATUS_ORIGINAL($PhaseData[$count]); // done / on / off / none
			
			if($Status == "on")
			$switch = "on";
			else
			$switch = "off";
			
			if($Status == "on" || $Status == "off" || $Status == "done")
			{
				$x .= "<td width=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"9\" /></td>";
				$x .= "<td>";
				$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
				$x .= "<tr>";
				$x .= "<td width=\"40\" nowrap class=\"scheme_phase_time_".$switch."\">".$starttime."</td>";
				$x .= "<td align=\"right\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_timeline_bg_".$switch.".gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_timeline_arrow_".$switch.".gif\" width=\"8\" height=\"20\"></td>";
				$x .= "<td width=\"40\" nowrap class=\"scheme_phase_time_".$switch."\">".$deadline."</td>";
				$x .= "</tr>";
				$x .= "</table></td>";
				$x .= "<td width=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"9\" /></td>";
			
				if($i < $ColNum - 1)
				$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"9\" /></td>";
				else
				$MissCol = 1;
			}
			else
			{
				break;
			}
			$count++;
		}

		if($count != 0 && $RowNum == $RowIndex + 1)
		{
			// do nth
		}
		else
		{
			if($Status == "done" || $Status == "on" || $Status_original != "off")
			$ArrowStatus = "on";
			else if($Status == "none" || $Status == "off")
			$ArrowStatus = "off";
			else
			{}
			
			$AddCOl = 1;
			$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$ArrowStatus."_vert_line.gif\">&nbsp;</td>";
		}
		
		// space for extra end
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
		
		if($RowNum != 1)
		{
			$LeftCol = $ColNum - $i;
			for($i = 0; $i < $LeftCol; $i++)
			{
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
			}
		}
		
		if($MissCol == 1 && $AddCOl != 1)
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
		
		$x .= "</tr>";
		
		return $x;
	}
	
	// get phase status
	function GET_PHASE_STATUS($ParArr="")
	{
		global $ck_memberType, $lgs;
		
		$ParArr["person_response"] = $ParArr["marking_scheme"];
		$person_right = $lgs->getPhaseRight($ck_memberType, $ParArr, true, true);
		
		$starttime = $ParArr["starttime"];
		$deadline = $ParArr["deadline"];
		$HandinNum = $ParArr["HandinNum"];
		$assignment_id = $ParArr["assignment_id"];
		$marking_scheme = $ParArr["marking_scheme"];
		
		$Today = date("Y-m-d H:m:s");
		
		$diff1 = $this->DAYS_BETWEEN($starttime, $Today);
		$diff2 = $this->DAYS_BETWEEN($deadline, $Today);
		
		//echo $diff1." ### ".$diff2."<br />";
		
		if($assignment_id != "")
		{
			if($diff1 >= 0 && $diff2 <= 0)
			{
				if($person_right == "DO" || $person_right == "REDO")
				$Status = "on";
				else
				$Status = "off";
			}
			else if($diff1 < 0 && $diff2 < 0)
			{
				$Status = "off";
			}
			else
			{
				$Status = "done";
			}
		}
		else
		{
			$Status = "none";
		}
		
		return $Status;
	} // end function get phase status
	
	function GET_NEW_PHASE_BUTTON($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $iPort;
		
		$parent_id = $ParArr["assignment_id"];
		
		$x = "";
	
		$x .= "<tr>";
		$x .= "<td width=\"9\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_04.gif\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" /></td>";
		$x .= "<td bgcolor=\"#FFFFFF\">";
		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
		$x .= "<tr>";
		$x .= "<td><a href=\"new_phase.php?assignment_id=".$parent_id."\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">&nbsp;".$iPort["new_phase"]."</a></td>";
		//$x .= "<td><a href=\"print_phase_menu.php?assignment_id=".$parent_id."\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_print.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">&nbsp;".$iPort["print_result"]."</a></td>";
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" /></td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "<td width=\"8\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/scheme_board_b_06.gif\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"8\" /></td>";
		$x .= "</tr>";
		
		return $x;
	} // end function get new phase button
	
	// Get school based scheme
	function GET_TABLE_OPEN($ParArr="")
	{
		$x = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		return $x;
	} // end function get school based scheme
	
	// Get school based scheme
	function GET_TABLE_CLOSE($ParArr="")
	{
		$x = "</table>";
		return $x;
	} // end function get school based scheme
	
	// get phase status
	function GET_PHASE_STATUS_ORIGINAL($ParArr="")
	{
		global $ck_memberType, $lgs;
	
		$starttime = $ParArr["starttime"];
		$deadline = $ParArr["deadline"];
		$HandinNum = $ParArr["HandinNum"];
		$assignment_id = $ParArr["assignment_id"];
		$marking_scheme = $ParArr["marking_scheme"];
		
		$Today = date("Y-m-d H:m:s");
		
		$diff1 = $this->DAYS_BETWEEN($starttime, $Today);
		$diff2 = $this->DAYS_BETWEEN($deadline, $Today);
		
		//echo $diff1." ### ".$diff2."<br />";
		
		if($assignment_id != "")
		{
			if($diff1 >= 0 && $diff2 <= 0)
			{
				$Status = "on";
			}
			else if($diff1 < 0 && $diff2 < 0)
			{
				$Status = "off";
			}
			else
			{
				$Status = "done";
			}
		}
		else
		{
			$Status = "none";
		}
		
		return $Status;
	} // end function get phase status
	
	function DAYS_BETWEEN($FromDate, $ToDate)
	{
		$F_Year = date("Y", strtotime($FromDate));
		$F_Month = date("m", strtotime($FromDate));
		$F_Day = date("d", strtotime($FromDate));
		
		$T_Year = date("Y", strtotime($ToDate));
		$T_Month = date("m", strtotime($ToDate));
		$T_Day = date("d", strtotime($ToDate));
		
		$ReturnValue = (mktime ( 0, 0, 0, $T_Month, $T_Day, $T_Year) - mktime ( 0, 0, 0, $F_Month, $F_Day, $F_Year))/(60*60*24);
		
  		return $ReturnValue;
	} // end function Days Between
	
	//get edit delete button for pahse
	function GET_PHASE_BUTTON($ParArr="")
	{
		Global $image_path, $LAYOUT_SKIN, $iPort;
		
		$Status = $ParArr["Status"];
		$phase_id = $ParArr["assignment_id"];
		$parent_id = $ParArr["parent_id"];
		
		$x = "&nbsp;";
		
		$EditLink = "edit_phase.php?phase_id=".$phase_id."&parent_id=".$parent_id;
		$PrintLink = "javascript:jPrint(".$phase_id.",".$parent_id.")";
		$ExportLink = "javascript:jExport(".$phase_id.",".$parent_id.")";
		
		if($this->IS_IPF_ADMIN())
		{
		$x = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td width=\"14\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_tool_01.gif\" width=\"14\" height=\"23\"></td>";
		$x .= "<td valign=\"top\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_tool_02.gif\" style=\"background-position:right top\">";
		$x .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"1\">";
		$x .= "<tr>";

		// print
		$x .= "<td nowrap><a href=\"".$PrintLink."\" class=\"tabletool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_print.gif\" width=\"12\" height=\"12\" border=\"0\" alt=\"".$iPort["print_result"]."\" align=\"absmiddle\"></a></td>";
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"8\" height=\"18\"></td>";
		// export
		$x .= "<td nowrap><a href=\"".$ExportLink."\" class=\"tabletool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_export.gif\" width=\"12\" height=\"12\" border=\"0\" alt=\"".$iPort["export_result"]."\" align=\"absmiddle\"></a></td>";
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"8\" height=\"18\"></td>";
		// edit
		$x .= "<td nowrap><a href=\"".$EditLink."\" class=\"tabletool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" alt=\"".$iPort["btn"]["edit"]."\" align=\"absmiddle\"></a></td>";
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"8\" height=\"18\"></td>";
		// delete
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" alt=\"".$iPort["btn"]["delete"]."\" align=\"absmiddle\" onMouseOver=\"style.cursor='hand'\" onclick=\"jCheckRemovePhase('".$iPort["msg"]["confirm_delete"]."', $phase_id);\"></td>";
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"5\" height=\"18\"></td>";
		
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";	
		}
		
		return $x;
	} // end get edit delete button
	
	// Get role image by role id
	function GET_ROLE_IMAGE($RoleID="")
	{
		switch ($RoleID)
		{
			case "CT": $RoleImage = "icon_staff.gif"; break;
			case "P": $RoleImage = "icon_parent.gif"; break;
			case "ST": $RoleImage = "icon_staff.gif"; break;
			case "admin": $RoleImage = "icon_admin.gif"; break;
			default: $RoleImage = "icon_student.gif"; break;
		}
		return $RoleImage;
	}
	
	// Get date display in school based scheme
	function GET_DATE_DISPLAY($Date="", $Format="", $Type="")
	{
		$yyyy = substr($Date, 0, 4);
		$mm = substr($Date, 5, 2);
		$dd = substr($Date, 8, 2);
		
		$months = array(null, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		unset($months[0]);
		
		$mm = $months[(int)$mm];
		$str = $dd." ".$mm;
		
		return $str;
	} // end function
	
	// get last submitted
	function GET_LAST_SUBMISSION($ParArr="")
	{
		Global $image_path, $LAYOUT_SKIN, $iPort;
		
		$Status = $ParArr["Status"];
		$phase_id = $ParArr["phase_id"];
		$parent_id = $ParArr["parent_id"];
		$StudentID = $ParArr["StudentID"];
		$ModifiedDate = $ParArr["inputdate"];
		$ClassName = $ParArr["ClassName"];
		$marking_scheme = $ParArr["marking_scheme"];
    	$Role = $ParArr["Role"];
		
		//hdebug_r($phase_id." ".$ModifiedDate);
		
		//hdebug_r($ParArr);
		
		if($ModifiedDate == "")
		return "";
		
		//hdebug_r($phase_id." ".$StudentID);
	if(($marking_scheme == "S" || $marking_scheme == "P") && $Role == "STUDENT")
	{
		if($Status == "done")
		{
			$link = "phase_form.php?Mode=view&assignment_id=$phase_id&parent_id=$parent_id";
			if($StudentID && $ClassName != "")
			$link .= "&ClassName=$ClassName&StudentID=$StudentID";
			
			$x .= "<a href=\"$link\" class=\"tabletool\">".$iPort["last_submission"]."</a>";
			$x .= "<a href=\"$link\" class=\"phase_title_on\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" align=\"absmiddle\" border=\"0\" height=\"20\"></a>";
		}
		else if($Status == "on")
		{
			$link = "phase_form.php?Mode=edit&assignment_id=$phase_id&parent_id=$parent_id";
			if($StudentID && $ClassName != "")
			$link .= "&ClassName=$ClassName&StudentID=$StudentID";
			
			$x .= "<strong><a href=\"$link\" class=\"tabletool\">".$iPort["last_submission"]."</a>";
			$x .= "<a href=\"$link\" class=\"phase_title_on\"><img src=\"$image_path/$LAYOUT_SKIN/icon_edit_b.gif\" width=\"20\" align=\"absmiddle\" border=\"0\" height=\"20\"></a></strong>";
		}
		else
		{
			return "";
		}
	}
	else
	{
		return "";
	}
		
		$x .= "<br />".$ModifiedDate;
		
		return $x;
	}
	
	// get scheme row submitted - teacher mode
	function GET_SCHEME_ROW_SUBMITTED($ParArr="")
	{
		Global $image_path, $LAYOUT_SKIN, $iPort;
		
		$PhaseData = $ParArr["PhaseData"];
		$RowNum = $ParArr["RowNum"];
		$RowIndex = $ParArr["RowIndex"];
		$ColNum = $ParArr["ColNum"];
		$StartIndex = $RowIndex * $ColNum;
		$count = $StartIndex;
		$parent_id = $ParArr["assignment_id"];
		
		$x = "";
		$x .= "<tr valign=\"top\">";
		
		for($i = 0; $i < $ColNum; $i++)
		{
			$phase_id = $PhaseData[$count]["assignment_id"];
			$Status = $this->GET_PHASE_STATUS($PhaseData[$count]); // done / on / off / none
			$Status_original = $this->GET_PHASE_STATUS_ORIGINAL($PhaseData[$count]); // done / on / off / none
			$SubmittedNum = $PhaseData[$count]["HandinNum"];
			$TotalNum = $PhaseData[$count]["TotalUser"];
			$ViewLink = "handin_view.php?phase_id=$phase_id&parent_id=$parent_id";
			
			if($Status == "on" || $Status == "done" || $Status_original == "on" || $Status_original == "done")
			{
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\" alt=\"\" width=\"9\" height=\"9\" /></td>";
				$x .= "<td class=\"scheme_submit scheme_submit_".$Status."\">".$iPort["submitted"]."<br />";
				$x .= "<a href=\"".$ViewLink."\"><strong>".$SubmittedNum."</strong> / ".$TotalNum."<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a><br /></td>";
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_06.gif\">&nbsp;</td>";
				
				if($i < $ColNum - 1)
				$x .= "<td align=\"left\">&nbsp;</td>";
				else
				$MissCol = 1;
			}
			else if($Status == "off")
			{
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_off_04.gif\">&nbsp;</td>";
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_off_05.gif\">&nbsp;</td>";
				$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_off_06.gif\">&nbsp;</td>";
				
				if($i < $ColNum - 1)
				$x .= "<td align=\"left\">&nbsp;</td>";
				else
				$MissCol = 1;
			}
			else
			{
				break;
			}
			$count++;
		} // end for
		
		if($count != 0 && $RowNum == $RowIndex + 1)
		{
			// do nth
		}
		else
		{
			if($Status == "done" || $Status == "on" || $Status_original != "off")
			$ArrowStatus = "on";
			else if($Status == "none" || $Status == "off")
			$ArrowStatus = "off";
			else
			{}
			
			$AddCOl = 1;
			$x .= "<td align=\"left\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/arrow_".$ArrowStatus."_vert_line.gif\">&nbsp;</td>";
			$x .= "<td align=\"left\">&nbsp;</td>";
		}
		
		
		if($RowNum != 1)
		{
			$LeftCol = $ColNum - $i;
			
			for($i = 0; $i < $LeftCol; $i++)
			{
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
				$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
			}
			
			if($LeftCol > 0)
			{
				$x .= "<td align=\"left\">&nbsp;</td>";
			}
		}
		else
		{
			$LeftCol = $ColNum - $i;
			if($LeftCol > 0)
			{
				$x .= "<td align=\"left\">&nbsp;</td>";
			}
		}
		
		if(($MissCol == 1 && $AddCOl != 1))
		{
			$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" height=\"9\" /></td>";
			$x .= "<td align=\"left\">&nbsp;</td>";
		}
		
		$x .= "</tr>";
		
		return $x;
	} // end fnction get row submitted
	
	function GET_PHASE_INFO_QUERY($ParArr="")
	{
		global $lgs;
		
		$assignment_id = $ParArr["assignment_id"];
		$parent_id = $ParArr["parent_id"];
	
		//debug_r($ParArr);
		
		# get total number of user have to fill the phase
		if($this->IS_TEACHER() && !$this->IS_IPF_ADMIN())
			$class_taught_arr = array_merge($this->GET_SUBJECT_TEACHER_CLASS(), $this->GET_CLASS_TEACHER_CLASS());
		else
			$class_taught_arr = "";
				
		$PhaseUsers = $lgs->getSchemeUsers($assignment_id, "", $class_taught_arr);
		
		if($class_taught_arr != "")	
		{
			$UserList = implode(",", $PhaseUsers);
			$TotalUser = sizeof($PhaseUsers);
			$conds = " AND b.user_id IN ({$UserList})";
		}
		else
		{
			$TotalUser = sizeof($PhaseUsers);
			$conds = "";
		}
		
		/*
		if(sizeof($PhaseUsers))
		{
			$UserList = implode(",", $PhaseUsers);
			$TotalUser = sizeof($PhaseUsers);
			$conds = " AND b.user_id IN ({$UserList})";
		}
		else
		{
			$TotalUser = 0;
			$conds = "";
		}
		*/
		
		$fieldname  = "a.title, ";
		$fieldname .= "a.assignment_id, ";
		$fieldname .= "a.starttime, ";
		$fieldname .= "a.deadline, ";
		$fieldname .= "a.marking_scheme, ";
		$fieldname .= "a.modified, ";
		$fieldname .= "a.instruction, ";
		$fieldname .= "$TotalUser as TotalUser, ";
		$fieldname .= "COUNT(b.handin_id) as HandinNum, ";
		$fieldname .= "a.parent_id, ";
		$fieldname .= "a.teacher_subject_id ";
		
		$sql = "SELECT $fieldname FROM $this->course_db.assignment AS a ";
		$sql .= "LEFT JOIN $this->course_db.handin as b ON a.assignment_id = b.assignment_id {$conds} ";
		$sql .= "WHERE a.parent_id='$parent_id' AND a.assignment_id='$assignment_id'";
		$sql .= "GROUP BY a.assignment_id ";
		
		return $sql;
	} // end function GET PHASE INFO QUERY
	
	function GET_PHASE_INFO($ParArr="")
	{
		global $lgs, $lo, $ck_intranet_user_id;
	
		$sql = $this->GET_PHASE_INFO_QUERY($ParArr);
		
		$ReturnArr = $this->returnArray($sql);
		
		//debug_r($ReturnArr);
		
		$marking_scheme = $ReturnArr[0]["marking_scheme"];
		$teacher_subject_id = $ReturnArr[0]["teacher_subject_id"];
		
		$i = 0;
		# Eric Yip (20081208): get phase total users and handed-in users explicitly 
		# key [2008-12-12]: separate the class teacher and subject teacher
		# key [2009-02-09]: check the class is in selected group
		# Shing [2009-07-24]: call getSchemeUsers twice, one for class teacher classes, another for subject teacher classes,
		#   in order to handle subject groups properly and the case that a teacher who are both class teacher and subject teacher properly.
		if($this->IS_TEACHER() && !$this->IS_IPF_ADMIN())
		{
			if($marking_scheme == "CT")
			{
				$class_taught_arr = $this->GET_CLASS_TEACHER_CLASS();
				$st_class_taught_arr = array();
			}
			else if($marking_scheme == "ST")
			{
				$class_taught_arr = array();
				$st_class_taught_arr = $this->GET_SUBJECT_TEACHER_CLASS($teacher_subject_id);
			}
			else
			{
				$class_taught_arr = $this->GET_CLASS_TEACHER_CLASS();
				$st_class_taught_arr = $this->GET_SUBJECT_TEACHER_CLASS();
				$teacher_subject_id = "";
			}
		}
		else
		{
			$class_taught_arr = "";
		}
			
		$GroupListInData = $lo->getGroupsListIn_DataValue($ReturnArr[$i]['parent_id'], "A");
		$GroupIDs = array();
		for($j = 0; $j < count($GroupListInData); $j++)
		{
			$GroupIDs[] = $GroupListInData[$j]["group_id"];
		}
		$SelectedClassList = $lo->getClassListByGroupIDList($GroupIDs, $ReturnArr[$i]['parent_id']);

		// if the $class_taught_arr is not in $SelectedClassList, remove it
		if(count($SelectedClassList) > 0)
		{
			if($this->IS_IPF_ADMIN())
			{
				$class_taught_arr = $SelectedClassList;
			}
			else
			{
				if(is_array($class_taught_arr) && is_array($SelectedClassList))
				{
					$class_taught_arr = array_intersect($SelectedClassList, $class_taught_arr);
				}
				if(is_array($st_class_taught_arr) && is_array($SelectedClassList))
				{
					$st_class_taught_arr = array_intersect($SelectedClassList, $st_class_taught_arr);
				}
			}
		}

		//debug_r($class_taught_arr);
		//for($i=0; $i<count($ReturnArr); $i++)
		//{
			if (is_array($class_taught_arr)) {
				if (count($class_taught_arr) > 0) {
					$PhaseUsers_ct = $lgs->getSchemeUsers($ReturnArr[$i]['parent_id'], "", $class_taught_arr);
				}
				else {
					$PhaseUsers_ct = array();
				}
				if (count($st_class_taught_arr) > 0) {
					$PhaseUsers_st = $lgs->getSchemeUsers($ReturnArr[$i]['parent_id'], "", $st_class_taught_arr);
					// Only accept students actually taught by the subject teacher only.
					$subject_students = $this->IP_USER_ID_TO_EC_USER_ID_SET_OPERATION($this->GET_SUBJECT_TEACHER_STUDENT($ck_intranet_user_id, $teacher_subject_id));
					$PhaseUsers_st = array_intersect($PhaseUsers_st, $subject_students);
				}
				else {
					$PhaseUsers_st = array();
				}
				$PhaseUsers = array_unique(array_merge($PhaseUsers_ct, $PhaseUsers_st));
			}
			else {
				$PhaseUsers = $lgs->getSchemeUsers($ReturnArr[$i]['parent_id']);
			}
			$HandinUsers = $lgs->getHandedinUser($ReturnArr[$i]['assignment_id']);
			
			$ReturnArr[$i][7] = $ReturnArr[$i]['TotalUser'] = count($PhaseUsers);
			$ReturnArr[$i][8] = $ReturnArr[$i]['HandinNum'] = count(array_intersect($HandinUsers, $PhaseUsers));
		//}
		
		return $ReturnArr;
	} // end function GET PHASE INFO
	
	// get phase form - view / edit
	function GET_PHASE_FORM($ParArr="")
	{
		global $lpf;
		
		$PhaseData = $lpf->GET_PHASE_INFO($ParArr);
		
		$ParArr["title"] = $PhaseData[0]["title"];
		$ParArr["modified"] = $PhaseData[0]["modified"];
		$ParArr["instruction"] = $PhaseData[0]["instruction"];
		$ParArr["PhaseData"] = $PhaseData;
		
		//debug_r($ParArr["AlertMsg"]);
		
		$x = "";
		$x .= $this->GET_SCHEME_LAYOUT_OPEN($ParArr);
		
		if($ParArr["AlertMsg"] != "")
		{
			$x .= "<table border='0' width='100%' align='left'>";
			$x .= "<tr><td align='right' valign='top'><span class=\"tabletextrequire\">*</span></td>";
			$x .= "<td align='left'><span class='tabletextremark'>".$ParArr["AlertMsg"]."</span></td></tr>";
			$x .= "</table>";
			$x .= "<br />";
			$x .= "<br />";
			$x .= "<br />";
		}
		
		$x .= $this->GET_PHASE_INFO_TABLE($ParArr);
		$x .= "<br />";
		
		$x .= $this->GET_CLASS_STUDENT_SELECTION($ParArr);
		$x .= $this->GET_EDIT_FORM_TITLE();
		$x .= "<br />";
		$x .= $this->GET_PHASE_CONTENT($ParArr);
		$x .= $this->GET_FORM_BUTTON($ParArr);
		$x .= $this->GET_SCHEME_LAYOUT_CLOSE($ParArr);
		
		return $x;
	} // end function get phase form
	
	// get phase info for school based scheme form / view / edit
	function GET_PHASE_INFO_TABLE($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $iPort, $lgs;
		
		$PhaseData = $ParArr["PhaseData"];
		
		$Title = $PhaseData[0]["title"];
		
		$ModifiedDate = $PhaseData[0]["modified"];
		$TotalNum = $PhaseData[0]["TotalUser"];
		$HandinNum = $PhaseData[0]["HandinNum"];
		
		$Status = $this->GET_PHASE_STATUS($PhaseData[0]);
		$SchemeRole = $this->GET_ROLE_TYPE($PhaseData[0]["marking_scheme"]);
		$RoleImage = $this->GET_ROLE_IMAGE($PhaseData[0]["marking_scheme"]);
		$StartDate = $this->GET_DATE_DISPLAY($PhaseData[0]["starttime"]);
		$EndDate = $this->GET_DATE_DISPLAY($PhaseData[0]["deadline"]);
		
		$Role = $ParArr["Role"]; // (STUDENT / TEACHER)
		$ParUserID = $ParArr["ParUserID"]; // Course User ID
		$phase_id = $ParArr["phase_id"];
		
		if($phase_id == "")
		$phase_id = $ParArr["assignment_id"];
		
		$parent_id = $ParArr["parent_id"];
		$ViewLink = "handin_view.php?phase_id=$phase_id&parent_id=$parent_id";
		
		# added on 20090212 by Kit, Photo
		$PhotoLink = $ParArr["StudentPhoto"];
		$PhotoCell = ($ParUserID)?"<td rowspan=\"4\" width=\"9\"><table width=\"100%\" border=\"0\" style=\"border: 2px solid rgb(205, 205, 205);\"><tr><td>$PhotoLink</td></tr></table></td>":"";
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$phase_obj = $lgs->getPhaseInfo($phase_id); // assignment_id also is phase_id
		$phase_obj = $phase_obj[0];
		$relevant_id = $phase_obj["answer"];
		$RelatedPhaseList = $lgs->GET_SELECTED_RELEVANT_PHASE($parent_id, $phase_id, $relevant_id, "array");
	
		// related phase
		# modified on 20090212, related phase layout
		if(count($RelatedPhaseList) > 0)
			$RelativePhase .= "<br /><div class=\"relate_phase\"><h1>Related Phase </h1>";
	
		for($j = 0;  $j < count($RelatedPhaseList); $j++)
		{
			$TmpPhaseId = $RelatedPhaseList[$j][0];
			$TmpPhaseTitle = $RelatedPhaseList[$j][1];
			$TmpViewLink = "view_phase_popup.php?assignment_id=".$parent_id."&phase_id=".$TmpPhaseId."&ParUserID=".$ParUserID;
			$TmpJS = "javascript:newWindow('".$TmpViewLink."', 27);";	
			$RelativePhase .= ($j!=0)?"<br />":"";	
			$RelativePhase .= "<a href=\"".$TmpJS."\" class=\"tabletool\" title=\"".$iPort["related_to"]." ".$TmpPhaseTitle."\"><img src=\"$image_path/$LAYOUT_SKIN/icon_arrow.gif\" width=\"15\" height=\"15\" border=\"0\" align=\"absmiddle\">".$TmpPhaseTitle."</a>";
		}
		if(count($RelatedPhaseList) > 0)
		$RelativePhase .= "</div>";
		else
		$RelativePhase .= "<br /><br />";
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		
		if($Status == "on")
		$ArrowStatus = "on";
		else
		$ArrowStatus = "off";
		
		if($Status == "on")
		$switch = "on";
		else
		$switch = "off";

		$x = "";
		
		$x .= "<table width=\"80%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		
		// row 1 - layout background - top
		$x .= "<tr valign=\"bottom\">";
		$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_01.gif\" width=\"9\" height=\"9\"></td>";
		$x .= "<td height=\"9\" align=\"right\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_02.gif\" style=\"background-repeat:repeat-x;background-position:bottom\">&nbsp;</td>";
		$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_03.gif\" width=\"9\" height=\"9\"></td>";
		$x .= "</tr>";
		
		// row 2 -- title
		$x .= "<tr>";
		$x .= "<td width=\"9\" valign=\"middle\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\" width=\"9\" height=\"9\"></td>";
		
		$x .= "<td valign=\"middle\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_05.gif\"><strong>";
		$x .= $Title."</strong>";
		$x .= $RelativePhase;
		$x .= "</td>";
		
		$x .= "<td width=\"9\" valign=\"middle\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_06.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_06.gif\" width=\"9\" height=\"9\"></td>";
		$x .= $PhotoCell;
		$x .= "</tr>";
		
		// row 3 -- role
		$x .= "<tr valign=\"top\">";
		$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\">&nbsp;</td>";
		$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_05.gif\">";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/".$RoleImage."\" width=\"25\" height=\"29\" style=\" background-position:inherit;  background-repeat:repeat-x\"></td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= $SchemeRole;
		$x .= "</td>";
		$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_06.gif\">&nbsp;</td>";
		$x .= "</tr>";
		
		// row 4 - submit number for teacher mode
		if($Role == "TEACHER")
		{
			$x .= "<tr valign=\"top\">";
			$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_04.gif\">&nbsp;</td>";
			$x .= "<td  class=\"scheme_submit scheme_submit_".$Status."\">".$iPort["submitted"]."<br />";
			$x .= "<a href=\"".$ViewLink."\"><strong>".$HandinNum."</strong> / ".$TotalNum."<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a><br /></td>";
			$x .= "<td background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_06.gif\">&nbsp;</td>";
			$x .= "</tr>";
		}
		
		// row 5 - layout bottom
		$x .= "<tr>";
		$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_07.gif\" width=\"9\" height=\"9\"></td>";
		$x .= "<td height=\"9\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_08.gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_08.gif\" width=\"9\" height=\"9\"></td>";
		$x .= "<td width=\"9\" height=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_board_".$Status."_09.gif\" width=\"9\" height=\"9\"></td>";
		$x .= "</tr>";
		
		// row 6 - time line
		$x .= "<tr>";
		$x .= "<td width=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"9\"></td>";
		$x .= "<td>";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td width=\"40\" nowrap class=\"scheme_phase_time_".$switch."\">".$StartDate."</td>";
		$x .= "<td align=\"right\" background=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_timeline_bg_".$switch.".gif\"><img src=\"$image_path/$LAYOUT_SKIN/iPortfolio/scheme/phase_timeline_arrow_".$ArrowStatus.".gif\" width=\"8\" height=\"20\"></td>";
		$x .= "<td width=\"40\" nowrap class=\"scheme_phase_time_".$switch."\">".$EndDate."</td>";
		$x .= "</tr>";
		$x .= "</table></td>";
		$x .= "<td width=\"9\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"9\" height=\"9\"></td>";
		$x .= "</tr>";
		
		$x .= "</table>";
		
		return $x;
	} // end function get phase info table
	
	function GET_CLASS_STUDENT_SELECTION($ParArr="")
	{
		$ClassSelect = $ParArr["ClassSelect"];
		$StudentSelect = $ParArr["StudentSelect"];
		$assignment_id = $ParArr["assignment_id"];
		$phase_id = $ParArr["assignment_id"];
		$parent_id = $ParArr["parent_id"];
		$action_page = ($ParArr["selectionActionPage"])?$ParArr["selectionActionPage"]:"phase_form.php";
		$thisUserID = $ParArr["StudentID"];
		
		$x = "<form name=\"form1\" method=\"post\" action=\"".$action_page."\">";
		$x .= "<table width=\"100%\" height=\"30\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		$x .= "<tr>";
		$x .= "<td class=\"chi_content_15\" align=\"left\">";
		
		if($ClassSelect != "" && $StudentSelect != "")
		$x .= $ClassSelect."&nbsp;".$StudentSelect;
		
		$x .= "</td></tr></table>";
		
		$x .= "<input type=\"hidden\" name=\"ClassChanged\" value=\"0\" />";
		$x .= "<input type=\"hidden\" name=\"phase_id\" value=\"".$phase_id."\" />";
		$x .= "<input type=\"hidden\" name=\"assignment_id\" value=\"".$assignment_id."\" />";
		$x .= "<input type=\"hidden\" name=\"parent_id\" value=\"".$parent_id."\" />";
		$x .= "</form>";
		
		return $x;
	}
	
	function GET_EDIT_FORM_TITLE()
	{
		global $image_path, $LAYOUT_SKIN, $iPort;
		
		$x = "";
		$x .= "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
		$x .= "<tr>";
		# Eric Yip (20090812): No edit icon according to CS testing
		//$x .= "<td><img src=\"$image_path/$LAYOUT_SKIN/icon_edit_b.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"sectiontitle\">".$iPort["content"]."</span></td>";
		$x .= "<td><span class=\"sectiontitle\">".$iPort["content"]."</span></td>";
		$x .= "</tr>";
		$x .= "</table>";
		
		return $x;
	} // end fucntion get edit form title
	
	function GET_PHASE_CONTENT($ParArr="")
	{
		$x = "";
		$SheetData = $ParArr["SheetData"];
		$x .= $SheetData;
		return $x;
	} // end function get phase content
	
	function GET_FORM_BUTTON($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $iPort, $linterface;
		
		$Role = $ParArr["Role"]; // (TEACHER / STUDENT)
		$Mode = $ParArr["Mode"];
		$assignment_id = $ParArr["assignment_id"];
		$parent_id = $ParArr["parent_id"];
		$person_right = $ParArr["person_right"];
		
		$phaseObj = $ParArr["PhaseData"];
		$marking_scheme = $phaseObj[0]["marking_scheme"];
		
		$x = "";
		$x .= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
		$x .= "<tr>";
		$x .= "<td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>";
		$x .= "</tr>";
		$x .= "<tr>";
		$x .= "<td align=\"right\">";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
		$x .= "<tr>";
		$x .= "<td align=\"center\">";
		
		$is_menu = 0;
		
		if($this->IS_IPF_ADMIN())
		{
			if($Role == "TEACHER")
				$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmitForm();", "btn_submit");
			
			$is_menu = 1;
		}
		else if($person_right == "DO")
		{
			$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmitForm();", "btn_submit");
			$is_menu = 1;
		}
		/*
		else if($person_right == "VIEW_DOING" && $marking_scheme == "S" && $this->IS_CLASS_TEACHER())
		{
			$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["submit"], "button", "jSubmitForm();", "btn_submit");
			$is_menu = 1;
		}
		*/
		else if($person_right == "VIEW_DOING")
		{
			$is_menu = 1;
		}

		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["print"], "button", "jPrint($assignment_id, $parent_id, $is_menu);", "btn_print");
		
		$x .= "&nbsp;";
		$x .= $linterface->GET_ACTION_BTN($iPort["btn"]["cancel"], "button", "jCancelForm();", "btn_cancel");
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
																	
		return $x;
	} // end function get form button
	
	# Get OLE class list
	function GET_CLASS_LIST($ParYear="", $ParClassLevel="", $class_taught_arr=""){
		global $eclass_db, $intranet_db;
		$CLASSLEVEL_FIELD = 'y.YearName';
		//$CLASSNAME_FIELD = 'yc.ClassTitle'.strtoupper($_SESSION['intranet_session_language']);
		$CLASSNAME_FIELD = 'yc.ClassTitleEN';
		
		if($ParClassLevel != "")
			$conds .= " AND $CLASSLEVEL_FIELD = '".$ParClassLevel."'";
		if($ParYear != "")
		{
// It seems that there is no such alias "os.".
//			# Define date range for specific year
//			$YearRange = split("-", $ParYear);
//			$FirstDate = $YearRange[0]."-09-01";
//			$LastDate = $YearRange[1]."-08-31";
//			
//			$conds .=	"
//						AND os.StartDate >= '".$FirstDate."'
//						AND os.StartDate <= '".$LastDate."'
//						";
		}
		
		if(!$this->IS_IPF_ADMIN())
		{
			if($class_taught_arr != "")
			{
				$conds .= " AND $CLASSNAME_FIELD IN ('".implode("','", $class_taught_arr)."') ";
			}
			else
			{
				$conds .= " AND $CLASSNAME_FIELD IN ('".implode("','", array_merge($this->GET_CLASS_TEACHER_CLASS(), $this->GET_SUBJECT_TEACHER_CLASS()))."') ";
			}
		}
		
		$sql =	"
							SELECT DISTINCT
								$CLASSNAME_FIELD as ClassName
							FROM
								{$eclass_db}.PORTFOLIO_STUDENT as ps
							LEFT JOIN
								{$intranet_db}.INTRANET_USER as iu
							ON
								ps.UserID = iu.UserID
							join {$intranet_db}.YEAR_CLASS_USER as ycu on iu.UserID = ycu.UserID
							join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
							join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
							join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
							WHERE
								($CLASSNAME_FIELD != '' AND $CLASSNAME_FIELD IS NOT NULL)
								$conds
							ORDER BY
								$CLASSNAME_FIELD
						";
		$ReturnArr = $this->returnVector($sql);
		
		$TmpReturnArr = array();
		
		if($class_taught_arr != "" && sizeof($class_taught_arr) > 0 && is_array($class_taught_arr))
		{
			for($i = 0; $i < sizeof($ReturnArr); $i++)
			{
				if(in_array($ReturnArr[$i], $class_taught_arr))
				{
					$TmpReturnArr[] = $ReturnArr[$i];
				}	
			}
		}
		else
		{
			$TmpReturnArr = $ReturnArr;
		}
		
		return $TmpReturnArr;
	}
	
	# Generate selection list - class level
	function GEN_CLASSLEVEL_SELECTION($ParYear="", $ParClassLevel="", $ParTag="", $class_taught_arr="")
	{
		global $ec_iPortfolio;
	
		$ClassLevelArr = $this->GET_CLASSLEVEL_LIST($ParYear, $class_taught_arr);
		
		for($i=0; $i<count($ClassLevelArr); $i++)
		{
			$ClassLevelArr[$i] = array($ClassLevelArr[$i], $ClassLevelArr[$i]);
		}
	
		$ReturnStr = getSelectByArrayTitle($ClassLevelArr, $ParTag, $ec_iPortfolio['all_classlevel'], $ParClassLevel, false, 2);
		
		return $ReturnStr;
	}
	
		// get handin list view - view / edit
	function GET_PHASE_HANDIN_LIST($ParArr="")
	{
		global $lpf;
		
		$PhaseData = $lpf->GET_PHASE_INFO($ParArr);
		
		$ParArr["title"] = $PhaseData[0]["title"];
		$ParArr["modified"] = $PhaseData[0]["modified"];
		$ParArr["instruction"] = $PhaseData[0]["instruction"];
		$ParArr["PhaseData"] = $PhaseData;
		
		$x = "";
		$ParArr["Role"] = "STUDENT";
		$x .= $this->GET_SCHEME_LAYOUT_OPEN($ParArr);
		$ParArr["Role"] = "TEACHER";
		
		$x .= $this->GET_PHASE_INFO_TABLE($ParArr);
		$x .= "<br />";
		$x .= $this->GET_SUBMITTED_REVISE_TITLE($ParArr);
		
		return $x;
	} // end function get phase form
	
	function GET_SUBMITTED_REVISE_TITLE($ParArr="")
	{
		global $image_path, $LAYOUT_SKIN, $iPort, $ec_iPortfolio;
		
		$assignment_id = $ParArr["parent_id"];
		$phase_id = $ParArr["phase_id"];
		$person_right = $ParArr["person_right"];
		
		$x = "";
		if($person_right == "DO")
		{
		$x .= "<table width=\"95%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
		
		$x .= "<tr>";
		$x .= "<td>";
		$x .= "<a class='contenttool' href=\"revise_phase.php?phase_id=$phase_id&assignment_id=$assignment_id\">";
		$x .= "<img border='0' src=\"$image_path/$LAYOUT_SKIN/icon_edit_b.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span>".$ec_iPortfolio['revise_result']."</span>";
		$x .= "</a>";
		$x .= "</td>";
		$x .= "</tr>";
		
		$x .= "<tr>";
		$x .= "<td>";
		$x .= "<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"><span class=\"sectiontitle\">".$iPort["submitted_list"]."</span>";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		}
		return $x;
	}
	
	# Get Student List of a Class
	function returnStudentListData($my_class_name){
		global $intranet_db, $eclass_db;

		$namefield = getNameFieldByLang2("iu.");
		$ClassNumberField = getClassNumberField("iu.");
		$sql = "	SELECT DISTINCT
						iu.UserID,
						CONCAT('(', iu.ClassName, ' - ', $ClassNumberField, ') ', {$namefield}) as DisplayName
					FROM
						{$intranet_db}.INTRANET_USER AS iu,
						{$eclass_db}.PORTFOLIO_STUDENT AS ps
					WHERE
						iu.ClassName='$my_class_name'
						AND ps.UserID=iu.UserID
						AND ps.WebSAMSRegNo IS NOT NULL
						AND ps.UserKey IS NOT NULL
						AND iu.RecordType = '2'
						AND ps.IsSuspend = 0
					ORDER BY
						DisplayName
				";

		return $this->returnArray($sql);
	}
	
}

?>
