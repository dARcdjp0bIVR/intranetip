<?php
/*
 * Editing by Pun
 *
 * Modification Log:
 *  2018-12-03 (Pun) [ip.2.5.10.1.1]
 *  	- Modified getStudentSubmisstionInfo(), added NCS cust
 *  2017-12-05 (Pun)
 *  	- Modified getReportListSql(), added checkbox
 *  2017-05-29 (Paul)
 *  	- Created this file
 * 
 */

class libpf_third_party  extends libdb {
	
	static $attachment_url = "/file/iportfolio/";
	private $schoolyear = "";
	private $user_id = "";
	private static $unsafe_file_char = array('\\', '/', ':', '*', '?', '"', '<', '>', '|');
	
	# Initializing class variables to prevent throwing exceptions in get/set functions
	public function libpf_third_party(){
		$this->libdb();
		$this->schoolyear = $_SESSION['CurrentSchoolYearID'];
		$this->user_id = $_SESSION['UserID'];
	}
	
	function getAttachmentUrl(){
		return $this->attachment_url;
	}
	
	function getSaveFileName($file_name){
		return str_replace($this->unsafe_file_char, '', $file_name);
	}
	
	//Assessment Report Function
	function getClassInfo($ClassName=''){
		global $intranet_db;
		$cond = (!empty($ClassName))?" AND (ClassTitleEN like '%$ClassName%' OR ClassTitleB5 like '%$ClassName%')":"";
		$sql = "SELECT
				YearClassID as class_id,
				ClassTitleEN as class_name_en,
				ClassTitleB5 as class_name_b5
			FROM ".$intranet_db.".YEAR_CLASS
			WHERE
				 AcademicYearID = ".$this->schoolyear."
				".$cond."
			ORDER BY ClassTitleEN
			";
		return $this->returnArray($sql);
	}
	
	function getReportListSql($keyword,$status){
		global $eclass_db;
		$checkBoxField = "CONCAT('<input type=\"checkbox\" name=\"assessment_id[]\" value=\"', RecordID,'\">')";
		$titleLink = "CONCAT('<a href=\"detail.php?assessment_id=',RecordID,'\">',Title,'</a>') as Title";
		
		$sql = "SELECT $titleLink, Description, StartDate, EndDate, {$checkBoxField} FROM {$eclass_db}.THIRD_PARTY_REPORT ";
		if($keyword != ""){
			$sql .= " WHERE Title LIKE '%".$keyword."%'";
		}
		return $sql;
	}
	
	function removeAssessment($toBeRemoved){
		global $eclass_db;
		$isDone = true;
		$sql = "DELETE FROM {$eclass_db}.THIRD_PARTY_REPORT WHERE RecordID IN ($toBeRemoved)";
		$isDone =  $this->db_db_query($sql);
				
		return (isDone);
	}
	
	function getAssessmentInfo($assessment_id){
		global $eclass_db,$intranet_db;
		$sql = "SELECT Title, Description FROM {$eclass_db}.THIRD_PARTY_REPORT WHERE RecordID='".$assessment_id."'";	
		return current($this->returnArray($sql));
	}
	
	function getAssessmentReportName($assessment_id){
		global $eclass_db,$intranet_db;
		$sql = "SELECT Title FROM
		{$eclass_db}.THIRD_PARTY_REPORT
		WHERE RecordID=$assessment_id";
		return current($this->returnVector($sql));
	}
		
	function addAssessmentInfo($title, $remarks=""){}
	function updateAssessment($assessmentId,$title,$remarks){}
	
	function getStudentSubmisstionInfo($assessment_id, $keyword="", $status="all"){
		global $intranet_db,$eclass_db, $intranet_session_language;
		
		$name_sql = ($intranet_session_language=='en')?"u.EnglishName":"u.ChineseName";
		
		if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
		    $class_sql = 'u.UserLogin as Class';
		}else{
		    $class_sql = 'CONCAT(u.ClassName, ' - ', u.ClassNumber) as Class';
		}
		
		$mainSql = "SELECT $name_sql as UserName, {$class_sql}, tpr.Result, tpr.Remark 
					FROM {$eclass_db}.THIRD_PARTY_REPORT_RECORD tpr 
					INNER JOIN {$intranet_db}.INTRANET_USER u ON tpr.UserID=u.UserID
					WHERE tpr.ReportID='".$assessment_id."'";
		

		if($keyword!=""){
    		if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
    			$mainSql .= " AND (".$name_sql." LIKE '%".$keyword."%' OR u.UserLogin LIKE '%".$keyword."%')";
    		}else{
    			$mainSql .= " AND ".$name_sql." LIKE '%".$keyword."%'";
    		}
		}
		
		if($status!="all"){
			$tmp_sql = "SELECT UserID FROM YEAR_CLASS_USER WHERE YearClassID='".$status."'";
			$userList = $this->returnVector($tmp_sql);
			$mainSql .= " AND u.UserID IN ('".implode("','",$userList)."')";
		}
		return 	$mainSql;		
	}
	
	function getStudentAssessmentList($assessmentId='',$classId='',$studentIdAry=array(), $status='all', $keyword=''){}
	
	 function getUsers($params = array()){
			
		extract($params);
		$cond = "";
		$cond .= isset($user_id)&&!empty($user_id)? "u.UserID = $user_id AND ": "";
		$cond .= isset($user_ids)? "u.UserID IN ('".implode("','",$user_ids)."') AND ": "";
		$cond .= isset($user_type)? "u.RecordType = '".array_search($user_type, kis::$user_record_types)."' AND ": "";
		$cond .= isset($excludes)? "u.UserID NOT IN ('".implode("','",$excludes)."') AND ": "";
	
		if (isset($class_id)&&!empty($class_id)){
		  
			$tables .= "LEFT JOIN YEAR_CLASS_USER cu ON cu.UserID = u.UserID ";
			$cond .= "cu.YearClassID = $class_id AND ";
		  
		}
		if (isset($academic_year_id)&&!empty($academic_year_id)){
		  
			$tables .= "LEFT JOIN YEAR_CLASS_USER cu ON cu.UserID = u.UserID ";
			$tables .= "LEFT JOIN YEAR_CLASS yc ON yc.YearClassID = cu.YearClassID ";
			$cond .= "yc.AcademicYearID = $academic_year_id AND ";
		  
		}
	
		$sql = "SELECT
		u.UserID user_id,
		u.EnglishName user_name_en,
		u.ChineseName user_name_b5,
		u.Gender user_gender,
		u.PhotoLink user_photo,
		u.ClassName as user_class_name,
		u.ClassNumber as user_class_number,
		u.RecordType user_type
		FROM INTRANET_USER u
		$tables
		WHERE $cond
		u.RecordStatus = 1 AND
		(
		u.UserLogin LIKE '%$keyword%' OR
		u.EnglishName LIKE '%$keyword%' OR
		u.ChineseName LIKE '%$keyword%' OR
		u.ClassName LIKE '%$keyword%'
		)
		ORDER BY u.ClassName, u.EnglishName";
	
		return $this->returnArray($sql);
	
	}
	
	function saveStudentAssessment($assessmentId,$classId,$studentId,$file_name){
		$AssessmentArr = current($this->getStudentAssessmentList($assessmentId,$classId,array($studentId)));
		if(count($AssessmentArr['assessment'])>0){ //remove old file first
			$this->removeStudentAssessment($assessmentId,$studentId,$file_name);
		}
		$this->insertStudentAssessment($assessmentId,$classId,$studentId,$file_name);
	
	}
	
}	
?>