<?
// Modifying: ivan
//include_once($intranet_root."/includes/portfolio25/oea/oeaConfig.inc.php");

if (!defined("LIBOEA_STUDENT_DEFINED"))         // Preprocessor directives
{
  define("LIBOEA_STUDENT_DEFINED",true);

	class liboea_student {
		private $objDB = null;
		private $oea_studentID = null;
		
		private $ObjOEAItem = null;
		private $ObjAdditionalInfo = null;
		private $ObjAbility = null;
		private $ObjAcademic = null;
		private $ObjSuppInfo = null;
		
		private $InfoType_OEA = 'OeaItem';
		private $InfoType_AddiInfo = 'AddiInfo';
		private $InfoType_Ability = 'Ability';
		private $InfoType_Academic = 'Academic';
		private $InfoType_SuppInfo = 'SuppInfo';
		
		public function liboea_student($studentID = null){  
			global $eclass_db;
			$this->objDB = new libdb();

			//LOAD THE PROGRAM DETAILS FORM STOREAGE (EG DB)
			if($studentID != null)
			{
				$this->oea_studentID = intval($studentID);
				//$this->loadRecordFromStorage();
			}
		}

		private function loadRecordFromStorage($ParInfoType=''){
			global $PATH_WRT_ROOT, $eclass_db;
			
			$loadOEA = ($ParInfoType == '' || $ParInfoType == $this->InfoType_OEA)? true : false;
			$loadAddiInfo = ($ParInfoType == '' || $ParInfoType == $this->InfoType_AddiInfo)? true : false;
			$loadAbility = ($ParInfoType == '' || $ParInfoType == $this->InfoType_Ability)? true : false;
			$loadAcademic = ($ParInfoType == '' || $ParInfoType == $this->InfoType_Academic)? true : false;
			$loadSuppInfo = ($ParInfoType == '' || $ParInfoType == $this->InfoType_SuppInfo)? true : false;
			
			if ($loadOEA)
			{
				$this->ObjOEAItem = null;
				if ($this->oea_studentID != null)
				{
					
				}
			}
			
			if ($loadAddiInfo)
			{
				$this->ObjAdditionalInfo = null;
				if ($this->oea_studentID == null) {
					// do nth
				}
				else {
					$sql = "Select AdditionalInfoID From {$eclass_db}.OEA_ADDITIONAL_INFO Where StudentID = '".$this->oea_studentID."'";
					$AdditionalInfoArr = $this->objDB->returnArray($sql);
					$AdditionalInfoID = $AdditionalInfoArr[0]['AdditionalInfoID'];
				}
				$this->ObjAdditionalInfo = new liboea_additional_info($AdditionalInfoID);
				$this->ObjAdditionalInfo->setStudentID($this->oea_studentID);
			}
			
			if ($loadAbility)
			{
				$this->ObjAbility = null;
				if ($this->oea_studentID == null) {
					// do nth
				}
				else {
					$sql = "Select StudentAbilityID From {$eclass_db}.OEA_STUDENT_ABILITY Where StudentID = '".$this->oea_studentID."'";
					$InfoArr = $this->objDB->returnArray($sql);
					$StudentAbilityID = $InfoArr[0]['StudentAbilityID'];
				}
				$this->ObjAbility = new liboea_ability($StudentAbilityID);
				$this->ObjAbility->setStudentID($this->oea_studentID);
			}
			
			if ($loadAcademic)
			{
				$this->ObjAcademic = null;
				if ($this->oea_studentID == null) {
					// do nth
				}
				else {
					$sql = "Select StudentAcademicID From {$eclass_db}.OEA_STUDENT_ACADEMIC Where StudentID = '".$this->oea_studentID."'";
					$InfoArr = $this->objDB->returnArray($sql);
					$StudentAcademicID = $InfoArr[0]['StudentAcademicID'];
				}
				$this->ObjAcademic = new liboea_academic($StudentAcademicID);
				$this->ObjAcademic->setStudentID($this->oea_studentID);
			}
			
			if ($loadSuppInfo)
			{
				$this->ObjSupplementaryInfo = null;
				if ($this->oea_studentID == null) {
					// do nth
				}
				else {
					$sql = "Select SupplementaryInfoID From {$eclass_db}.OEA_SUPPLEMENTARY_INFO Where StudentID = '".$this->oea_studentID."'";
					$SuppInfoArr = $this->objDB->returnArray($sql);
					$SupplementaryInfoID = $SuppInfoArr[0]['SupplementaryInfoID'];
				}
				$this->ObjSupplementaryInfo = new liboea_supplementary_info($SupplementaryInfoID);
				$this->ObjSupplementaryInfo->setStudentID($this->oea_studentID);
			}
		}
		
		public function getOEAItem()
		{
			if ($this->ObjOEAItem == null) {
				$this->loadRecordFromStorage($this->InfoType_OEA);
			}
			
			return $this->ObjOEAItem;
		}
		
		public function getAdditionalInfo()
		{
			if ($this->ObjAdditionalInfo == null) {
				$this->loadRecordFromStorage($this->InfoType_AddiInfo);
			}
			
			return $this->ObjAdditionalInfo;
		}
		
		public function getAbility()
		{
			if ($this->ObjAbility == null) {
				$this->loadRecordFromStorage($this->InfoType_Ability);
			}
			
			return $this->ObjAbility;
		}
		
		public function getAcademic()
		{
			if ($this->ObjAcademic == null) {
				$this->loadRecordFromStorage($this->InfoType_Academic);
			}
			
			return $this->ObjAcademic;
		}
		
		public function getSupplementaryInfo()
		{
			if ($this->ObjSupplementaryInfo == null) {
				$this->loadRecordFromStorage($this->InfoType_SuppInfo);
			}
			
			return $this->ObjSupplementaryInfo;
		}
		public function getSelfAccount(){
			global $eclass_db;
			$sql = 'select recordid,title , details,DATE_format(ModifiedDate, "%Y-%m-%d") as "ModifiedDate" from '.$eclass_db.'.SELF_ACCOUNT_STUDENT where userid = '.$this->oea_studentID.' order by ModifiedDate desc';
			$result = $this->objDB->returnArray($sql);
			return $result;
		}
	}

}
?>