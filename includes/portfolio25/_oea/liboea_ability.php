<?
# modifying : ivan

if (!defined("LIBOEA_ABILITY_DEFINED"))         // Preprocessor directives
{
  define("LIBOEA_ABILITY_DEFINED",true);

	class liboea_ability {
		private $objDB;
		private $recordID;
		private $TableName;

		private $StudentID;
		private $AbilityValue;
		private $RecordStatus;
		private $InputDate;
		private $InputBy;
		private $ApprovalDate;
		private $ApprovedBy;
		private $ModifyDate;
		private $ModifiedBy;

		private $JUPAS_ability_items;		

		private $AbilityToCommunicate;
		private $AbilityToWorkWithOthers;
		private $AnalyticalPower;
		private $Conduct;
		private $Creativity;
		private $IndependenceOfMind;
		private $Industriousness;
		private $Initiative;
		private $Leadership;
		private $Maturity;
		private $Perseverance;
		private $SenseOfResponsibility;
		private $OverallEvaluation;

		public function liboea_ability($recordID = null){
			global $eclass_db, $oea_cfg;

			$this->TableName = $eclass_db.'.OEA_STUDENT_ABILITY';
			$this->ID_FieldName = 'StudentAbilityID';
			$this->TypeCode = liboea::getAbilityInternalCode();
			
			$this->objDB = new libdb();

			//LOAD THE OEA ABILITY FORM STOREAGE (EG DB)
			if($recordID != null)
			{
				$this->recordID = intval($recordID);
				$this->loadRecordFromStorage();
			}
		}

		private function getObjectTableName(){
			return $this->TableName;
		}
		public function getObjectIdFieldName(){
			return $this->ID_FieldName;
		}		
		public function getObjectID(){
			return $this->recordID;
		}
		public function getObjectInternalCode(){
			return $this->TypeCode;
		}

		public function setStudentID($val){
			$this->StudentID = $val;
		}
		public function getStudentID(){
			return $this->StudentID;
		}

		//FOR RETURN THE ABILITY VALUE , IT SHOULD BE PRIVATE NOW
		private function setAbilityValue($val){
			$this->AbilityValue = $val;
		}
		private function getAbilityValue(){
			return $this->AbilityValue;
		}


		public function setRecordStatus($val){
			$this->RecordStatus = $val;
		}
		public function getRecordStatus(){
			return $this->RecordStatus;
		}

		public function setInputDate($val){
			$this->InputDate = $val;
		}
		public function getInputDate(){
			return $this->InputDate;
		}

		public function setInputBy($val){
			$this->InputBy = $val;
		}
		public function getInputBy(){
			return $this->InputBy;
		}
		
		public function setApprovalDate($val){
			$this->ApprovalDate = $val;
		}
		public function getApprovalDate(){
			return $this->ApprovalDate;
		}
		
		public function setApprovedBy($intValue){
			$this->ApprovedBy = $intValue;
		}
		public function getApprovedBy(){
			return $this->ApprovedBy;
		}

		public function setModifyDate($val){
			$this->ModifyDate = $val;
		}
		public function getModifyDate(){
			return $this->ModifyDate;
		}
	
		public function setModifiedBy($val){
			$this->ModifiedBy = $val;
		}
		public function getModifiedBy(){
			return $this->ModifiedBy;
		}

		public function setAbilityToCommunicate($str){
			$this->AbilityToCommunicate = $str;
		}
		public function getAbilityToCommunicate(){
			return $this->AbilityToCommunicate;
		}


		public function setAbilityToWorkWithOthers($str){
			$this->AbilityToWorkWithOthers = $str;
		}
		public function getAbilityToWorkWithOthers(){

			return $this->AbilityToWorkWithOthers;
		}


		public function setAnalyticalPower($str){
			$this->AnalyticalPower= $str;
		}
		public function getAnalyticalPower(){
			return $this->AnalyticalPower;
		}
	
		public function setConduct($str){
			$this->Conduct= $str;
		}
		public function getConduct(){
			return $this->Conduct;
		}

		public function setCreativity($str){
			$this->Creativity = $str;
		}
		public function getCreativity(){
			return $this->Creativity;
		}

			public function setIndependenceOfMind($str){
			$this->IndependenceOfMind = $str;
		}
		public function getIndependenceOfMind(){
			return $this->IndependenceOfMind;
		}

		public function setIndustriousness($str){
			$this->Industriousness = $str;
		}
		public function getIndustriousness(){
			return $this->Industriousness;
		}

		public function setInitiative($str){
			$this->Initiative= $str;
		}
		public function getInitiative(){
			return $this->Initiative;
		}

		public function setLeadership($str){
			$this->Leadership= $str;
		}
		public function getLeadership(){
			return $this->Leadership;
		}

		public function setMaturity($str){
			$this->Maturity= $str;
		}
		public function getMaturity(){
			return $this->Maturity;
		}
		public function setPerseverance($str){
			$this->Perseverance= $str;
		}
		public function getPerseverance(){
			return $this->Perseverance;
		}
		public function setSenseOfResponsibility($str){
			$this->SenseOfResponsibility= $str;
		}
		public function getSenseOfResponsibility(){
			return $this->SenseOfResponsibility;
		}
		public function setOverallEvaluation($str){
			$this->OverallEvaluation= $str;
		}
		public function getOverallEvaluation(){
			return $this->OverallEvaluation;
		}
		
		public function get_pending_status() {
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_ABILITY_RecordStatus"]["Pending"];
		}
		public function get_approved_status() {
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_ABILITY_RecordStatus"]["Approved"];
		}
		public function get_rejected_status() {
			global $oea_cfg;
			return $oea_cfg["OEA_STUDENT_ABILITY_RecordStatus"]["Rejected"];
		}
		
		
		private function loadRecordFromStorage(){
			global $eclass_db;			
			$result = null;
			
			$_recordID = $this->recordID;

			if( (trim($_recordID) == "") || (intval($_recordID) < 0)) {
				//DO NOTHING
			}else{
				$ObjTable = $this->getObjectTableName();
				$ObjIdFieldName = $this->getObjectIdFieldName();
				$sql = "select		
							StudentAbilityID,
							StudentID,
							AbilityValue,
							RecordStatus,
							InputDate,
							InputBy,
							ModifyDate,
							ModifiedBy,
							ApprovedBy,
							ApprovalDate
						from 
							$ObjTable
						where 
							$ObjIdFieldName = ".$this->recordID;
				$resultSet = $this->objDB->returnArray($sql);
				$infoArr = $resultSet[0];
				
				foreach ((array)$infoArr as $_key => $_value)
				{
					if (!is_numeric($_key))
						$this->{$_key} = $_value;
				}
				
				$this->setAttributesValueToObject();
			}
		}

		public function save()
		{
			if( 
				(trim($this->recordID) != "") && 
					(intval($this->recordID) > 0)
				){
				$resultId = $this->update_record();
			}
			else{	
				$resultId = $this->new_record();
			}

			return $resultId;

		}
		
		private function update_record()
		{
			global $eclass_db;
			
			# Set object value
			$this->setModifyDate('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			$this->setAbilityValue($this->convertObjectAttributesToDBValue());
			
			# Prepare value for SQL update
			$DataArr = array();
			$DataArr["AbilityValue"]	= $this->objDB->pack_value($this->AbilityValue,"str");
			$DataArr["RecordStatus"]	= $this->objDB->pack_value($this->RecordStatus, "int");
			$DataArr["ModifyDate"]		= $this->objDB->pack_value($this->ModifyDate, "date");
			$DataArr["ModifiedBy"]		= $this->objDB->pack_value($this->ModifiedBy, "int");
			$DataArr["ApprovalDate"]	= $this->objDB->pack_value($this->ApprovalDate, "date");
			$DataArr["ApprovedBy"]		= $this->objDB->pack_value($this->ApprovedBy, "int");
			
			# Build field update values string
			$valueFieldArr = array();
			foreach ($DataArr as $_field => $_value)
			{
				$valueFieldArr[] = " $_field = $_value ";
			}
			$valueFieldText .= implode(',', $valueFieldArr);
			
			$ObjTable = $this->getObjectTableName();
			$ObjIdFieldName = $this->getObjectIdFieldName();
			$sql = "Update $ObjTable Set $valueFieldText Where $ObjIdFieldName = '".$this->recordID."'";
			$success = $this->objDB->db_db_query($sql);
			
			$this->loadRecordFromStorage();
			return $this->recordID;
		}
		
		private function new_record()
		{
			global $eclass_db, $UserID;

			$abilityValue = $this->convertObjectAttributesToDBValue();
			$this->setAbilityValue($abilityValue);
			
			$this->setRecordStatus($this->get_pending_status());
			$this->setInputDate('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setModifyDate('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			$this->setApprovalDate('null');
			$this->setApprovedBy('null');

			$DataArr = array();
			$DataArr["StudentID"]		= $this->objDB->pack_value($this->StudentID,"int");
			$DataArr["AbilityValue"]	= $this->objDB->pack_value($this->AbilityValue,"str");
			$DataArr["RecordStatus"]	= $this->objDB->pack_value($this->RecordStatus,"int");
			$DataArr["InputDate"]		= $this->objDB->pack_value($this->InputDate,"int");
			$DataArr["InputBy"]			= $this->objDB->pack_value($this->InputBy,"int");
			$DataArr["ApprovalDate"]	= $this->objDB->pack_value($this->ApprovalDate,"date");
			$DataArr["ApprovedBy"]		= $this->objDB->pack_value($this->ApprovedBy,"int");
			$DataArr["ModifyDate"]		= $this->objDB->pack_value($this->ModifyDate,"date");
			$DataArr["ModifiedBy"]		= $this->objDB->pack_value($this->ModifiedBy,"date");


			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = $value;
			}
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$ObjTable = $this->getObjectTableName();
			$sql = "Insert Into $ObjTable ($fieldText) Values ($valueText)";
			$success = $this->objDB->db_db_query($sql);
			
			$RecordID = $this->objDB->db_insert_id();
			$this->recordID = $RecordID;
			
			$this->loadRecordFromStorage();
			return $RecordID;
		}
		
		private function update_recordStatus($ParStatus) {
			$this->setRecordStatus($ParStatus);
			$this->setApprovalDate('now()');
			$this->setApprovedBy($_SESSION['UserID']);
			
			return $this->save();
		}
		
		public function approve_record(){
			return $this->update_recordStatus($this->get_approved_status());
		}
		
		public function reject_record(){
			return $this->update_recordStatus($this->get_rejected_status());
		}
		
		private function convertObjectAttributesToDBValue()
		{
			$AbilityAttributeInfoArr = $this->getJUPASAbilityAttributesConfigArr();
			$abilityArr = array();
			foreach ((array)$AbilityAttributeInfoArr as $_attributeInternalCode => $_attributeInfoArr){
				//WITH THIS FOR LOOP , it dynamic generates related ability GET Method name (eg. getAbilityToWorkWithOthers ,getAnalyticalPower)
				$getfunctionName = "get".$_attributeInternalCode;
				$abilityArr[] = $_attributeInternalCode."==".$this->{$getfunctionName}();
			}
			$abilityValue = implode('##', $abilityArr);
			
			return $abilityValue;
		}
		
		private function setAttributesValueToObject()
		{
			$AttributeValue = $this->getAbilityValue();
			
			if ($AttributeValue == '')
			{
				// no value => don't set value
			}
			else
			{
				$AttributeArr = explode('##', $AttributeValue);
				$numOfAttribute = count($AttributeArr);
				
				for ($i=0; $i<$numOfAttribute; $i++)
				{
					$_AttributeInfoArr = explode('==', $AttributeArr[$i]);
					$_AttributeInternalCode = $_AttributeInfoArr[0];
					$_RatingInternalValue = $_AttributeInfoArr[1];
					
					$_setFunctionName = "set".$_AttributeInternalCode;
					$this->{$_setFunctionName}($_RatingInternalValue);
				}
			}
		}
		
//		public function getAttributesValueArray()
//		{
//			$AbilityAttributeInfoArr = $this->getJUPASAbilityAttributes();
//			$abilityArr = array();
//			foreach ((array)$AbilityAttributeInfoArr as $_attributeJupasCode => $_attributeInternalCode){
//				//WITH THIS FOR LOOP , it dynamic generates related ability GET Method name (eg. getAbilityToWorkWithOthers ,getAnalyticalPower)
//				$getfunctionName = "get".$_attributeInternalCode;
//				$abilityArr[$_attributeJupasCode] = $this->{$getfunctionName}();
//			}
//			return $abilityArr;
//		}

		public function getAttributesValueArray($AttributeValue='')
		{
			$AttributeValue = ($AttributeValue == '')? $this->getAbilityValue() : $AttributeValue;
			
			$ReturnArr = array();
			if ($AttributeValue == '')
			{
				// no value => don't set value
			}
			else
			{
				$AttributeArr = explode('##', $AttributeValue);
				$numOfAttribute = count($AttributeArr);
				
				for ($i=0; $i<$numOfAttribute; $i++)
				{
					$_AttributeInfoArr = explode('==', $AttributeArr[$i]);
					$_AttributeInternalCode = $_AttributeInfoArr[0];
					$_RatingValue = $_AttributeInfoArr[1];
					
					$ReturnArr[$_AttributeInternalCode] = $_RatingValue;
				}
			}
			
			return $ReturnArr;
		}

		public static function getJUPASAbilityAttributesConfigArr(){
			global $oea_cfg;
			return $oea_cfg["Ability"]["Attribute"];
		}
		public static function getJUPASAbilityRatingsConfigArr(){
			global $oea_cfg;
			return $oea_cfg["Ability"]["Rating"];
		}
		
		public static function Get_Ability_Attribute_Name($InternalCode) {
			global $Lang;
			return $Lang['iPortfolio']['OEA']['AbilityArr']['AttributeArr'][$InternalCode];	
		}
		public static function Get_Ability_Rating_Name($InternalCode) {
			global $Lang;
			return $Lang['iPortfolio']['OEA']['AbilityArr']['RatingArr'][$InternalCode];	
		}
		
		public function Get_Ability_Attribute_InternalCode_By_JupasCode($TargetJupasCode) {
			$AbilityAttributeInfoArr = $this->getJUPASAbilityAttributesConfigArr();
			foreach ((array)$AbilityAttributeInfoArr as $_attributeInternalCode => $_attributeInfoArr){
				$_attributeJupasCode = $_attributeInfoArr['JupasCode'];
				
				if ($_attributeJupasCode == $TargetJupasCode) {
					return $_attributeInternalCode;
				}
			}
		}
		
		public static function Get_Ability_Attribute_JupasCode_By_InternalCode($TargetInternalCode) {
			global $oea_cfg;
			return $oea_cfg["Ability"]["Attribute"][$TargetInternalCode]['JupasCode'];
		}
		
		public static function Get_Ability_Rating_JupasCode_By_InternalCode($TargetInternalCode) {
			global $oea_cfg;
			return $oea_cfg["Ability"]["Rating"][$TargetInternalCode]['JupasCode'];
		}
	}

}
?>