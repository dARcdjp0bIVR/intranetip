<?
// Modifying: 

//include_once($intranet_root."/includes/portfolio25/oea/oeaConfig.inc.php");

if (!defined("LIBOEA_ADDITIONAL_INFO_DEFINED"))         // Preprocessor directives
{
  define("LIBOEA_ADDITIONAL_INFO_DEFINED",true);

	class liboea_additional_info{
		private $objDB;
		private $recordID;
		private $TableName;
		private $ID_FieldName;
		private $TypeCode;

		private $StudentID;
		private $FromSelfAccountID;
		private $Title;
		private $Details;
		private $RecordStatus;
		private $InputDate;
		private $InputBy;
		private $ModifyDate;
		private $ModifiedBy;
		private $ApprovalDate;
		private $ApprovedBy;
		

		public function liboea_additional_info($recordID = null){  
			global $eclass_db;
			$this->objDB = new libdb();
			
			$this->TableName = $eclass_db.'.OEA_ADDITIONAL_INFO';
			$this->ID_FieldName = 'AdditionalInfoID';
			$this->TypeCode = liboea::getAddiInfoInternalCode();

			//LOAD THE OEA DETAILS FORM STOREAGE (EG DB)
			if($recordID != null)
			{
				$this->recordID= intval($recordID);
				$this->loadRecordFromStorage();
			}
		}
		
		public function getObjectTableName(){
			return $this->TableName;
		}
		public function getObjectIdFieldName(){
			return $this->ID_FieldName;
		}		
		public function getObjectID(){
			return $this->recordID;
		}
		public function getObjectInternalCode(){
			return $this->TypeCode;
		}
		
		public function setStudentID($sID){
			$this->StudentID = $sID;
		}
		public function getStudentID(){
			return $this->StudentID;
		}
		
		public function setFromSelfAccountID ($str){
			$this->FromSelfAccountID = $str;
		}
		public function getFromSelfAccountID(){
			return $this->FromSelfAccountID;
		}
		
		public function setTitle($str){
			$this->Title = $str;
		}
		public function getTitle(){
			return $this->Title;
		}
		
		public function setDetails($str){
			$this->Details = $str;
		}
		public function getDetails(){
			return $this->Details;
		}

		public function setRecordStatus($intStatus){
			$this->RecordStatus = $intStatus;
		}
		public function getRecordStatus(){
			return $this->RecordStatus;
		}

		public function setInputDate($value){
			$this->InputDate = $value;
		}
		public function getInputDate(){
			return $this->InputDate;
		}

		public function setInputBy($intValue){
			$this->InputBy = $intValue;
		}
		public function getInputBy(){
			return $this->InputBy;
		}

		public function setApprovalDate($value){
			$this->ApprovalDate = $value;
		}
		public function getApprovalDate(){
			return $this->ApprovalDate;
		}

		public function setApprovedBy($intValue){
			$this->ApprovedBy = $intValue;
		}
		public function getApprovedBy(){
			return $this->ApprovedBy;
		}

		public function setModifyDate($value){
			$this->ModifyDate = $value;
		}
		public function getModifyDate(){
			return $this->ModifyDate;
		}

		public function setModifiedBy($intValue){
			$this->ModifiedBy = $intValue;
		}
		public function getModifiedBy(){
			return $this->ModifiedBy;
		}
		
		public static function get_pending_status() {
			global $oea_cfg;
			return $oea_cfg["OEA_ADDITIONAL_INFO_RecordStatus"]["Pending"];
		}
		public static function get_approved_status() {
			global $oea_cfg;
			return $oea_cfg["OEA_ADDITIONAL_INFO_RecordStatus"]["Approved"];
		}
		public static function get_rejected_status() {
			global $oea_cfg;
			return $oea_cfg["OEA_ADDITIONAL_INFO_RecordStatus"]["Rejected"];
		}
		
		public static function getDetailsMaxLength() {
			global $oea_cfg;
			return $oea_cfg["OEA_ADDITIONAL_INFO_Details"]["MaxLength"];
		}
		public static function getDetailsMaxWordCount() {
			global $oea_cfg;
			return $oea_cfg["OEA_ADDITIONAL_INFO_Details"]["MaxWordCount"];
		}

		private function loadRecordFromStorage(){
			global $eclass_db;			
			$result = null;
			
			$_recordID = $this->recordID;

			if( (trim($_recordID) == "") || (intval($_recordID) < 0)) {
				//DO NOTHING
			} else {
				$ObjTable = $this->getObjectTableName();
				$ObjIdFieldName = $this->getObjectIdFieldName();
				$sql = "select		
							AdditionalInfoID,
							StudentID,
							FromSelfAccountID,
							Title,
							Details,
							RecordStatus,
							InputDate,
							InputBy,
							ModifyDate,
							ModifiedBy,
							ApprovedBy,
							ApprovalDate
						from 
							$ObjTable
						where 
							$ObjIdFieldName = ".$this->recordID;
				$resultSet = $this->objDB->returnArray($sql);
				$infoArr = $resultSet[0];
				
				foreach ((array)$infoArr as $_key => $_value)
				{
					if (!is_numeric($_key))
						$this->{$_key} = $_value;
				}
			}
//			return $result;


		}
		
		public function save(){
			if( 
				(trim($this->recordID) != "") && 
					(intval($this->recordID) > 0)
				){
				$resultID = $this->update_record();
			}
			else{	
				$resultID = $this->new_record();
			}

			return $resultID;

		}
		
		private function new_record(){
			global $eclass_db;
			
			# Set object value
			$this->setRecordStatus($this->get_pending_status());
			$this->setInputDate('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setModifyDate('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			$this->setApprovalDate('null');
			$this->setApprovedBy('null');
						
			# Prepare value for SQL update
			$DataArr = array();
			$DataArr["StudentID"]			= $this->objDB->pack_value($this->StudentID, "int");
			$DataArr["FromSelfAccountID"]	= $this->objDB->pack_value($this->FromSelfAccountID, "int");
			$DataArr["Title"]				= $this->objDB->pack_value($this->Title, "str");
			$DataArr["Details"]				= $this->objDB->pack_value($this->Details, "str");
			$DataArr["RecordStatus"]		= $this->objDB->pack_value($this->RecordStatus, "int");
			$DataArr["InputDate"]			= $this->objDB->pack_value($this->InputDate, "date");
			$DataArr["InputBy"]				= $this->objDB->pack_value($this->InputBy, "int");
			$DataArr["ModifyDate"]			= $this->objDB->pack_value($this->ModifyDate, "date");
			$DataArr["ModifiedBy"]			= $this->objDB->pack_value($this->ModifiedBy, "int");
			$DataArr["ApprovalDate"]		= $this->objDB->pack_value($this->ApprovalDate, "date");
			$DataArr["ApprovedBy"]			= $this->objDB->pack_value($this->ApprovedBy, "int");
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = $value;
			}
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$ObjTable = $this->getObjectTableName();
			$sql = "Insert Into $ObjTable ($fieldText) Values ($valueText)";
			$success = $this->objDB->db_db_query($sql);
			
			$RecordID = $this->objDB->db_insert_id();
			$this->recordID = $RecordID;
			
			$this->loadRecordFromStorage();
			return $RecordID;
		}
		
		private function update_record() {
			global $eclass_db;
			
			# Set object value
			$this->setModifyDate('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			
			# Prepare value for SQL update
			$DataArr = array();
			$DataArr["FromSelfAccountID"]	= $this->objDB->pack_value($this->FromSelfAccountID, "int");
			$DataArr["Title"]				= $this->objDB->pack_value($this->Title, "str");
			$DataArr["Details"]				= $this->objDB->pack_value($this->Details, "str");
			$DataArr["RecordStatus"]		= $this->objDB->pack_value($this->RecordStatus, "int");
			$DataArr["ModifyDate"]			= $this->objDB->pack_value($this->ModifyDate, "date");
			$DataArr["ModifiedBy"]			= $this->objDB->pack_value($this->ModifiedBy, "int");
			$DataArr["ApprovalDate"]		= $this->objDB->pack_value($this->ApprovalDate, "date");
			$DataArr["ApprovedBy"]			= $this->objDB->pack_value($this->ApprovedBy, "int");
			
			# Build field update values string
			$valueFieldArr = array();
			foreach ($DataArr as $_field => $_value)
			{
				$valueFieldArr[] = " $_field = $_value ";
			}
			$valueFieldText .= implode(',', $valueFieldArr);
			
			$ObjTable = $this->getObjectTableName();
			$ObjIdFieldName = $this->getObjectIdFieldName();
			$sql = "Update $ObjTable Set $valueFieldText Where $ObjIdFieldName = '".$this->recordID."'";
			$success = $this->objDB->db_db_query($sql);
			
			$this->loadRecordFromStorage();
			return $this->recordID;
		}
		
		private function update_recordStatus($ParStatus) {
			$this->setRecordStatus($ParStatus);
			$this->setApprovalDate('now()');
			$this->setApprovedBy($_SESSION['UserID']);
			
			return $this->save();
		}
		
		public function approve_record(){
			return $this->update_recordStatus($this->get_approved_status());
		}
		
		public function reject_record(){
			return $this->update_recordStatus($this->get_rejected_status());
		}
				
		
		public function getLastModifiedInfoDisplay()
		{
			$ReturnStr = '';
			if ($this->getModifyDate() == '' || $this->getModifiedBy() == '') {
				// do nth
			}
			else {
				$ReturnStr = Get_Last_Modified_Remark($this->getModifyDate(), $byUserName='', $this->getModifiedBy());
			}
				
			return $ReturnStr;
		}
		
		public function getViewTable()
		{
			global $Lang, $iPort;
			$x = '';
			
			if ($this->recordID == '') {
				$x .= $Lang['General']['NoRecordAtThisMoment'];
			}
			else {
				
				$x .= '<table width="100%" border="0" cellspacing="6" cellpadding="0">'."\n";
//					$x .= '<tr>'."\n";
//						$x .= '<td class="sub_page_title">'.$this->getTitle().'</td>'."\n";
//						$x .= '<td class="tabletext" style="text-align:right;">'.$this->getLastModifiedInfoDisplay().'</td>'."\n";
//					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td colspan="2" class="form_field_content"><div id="AddiInfoDetailsDiv">'.nl2br($this->getDetails()).'</div></td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td colspan="2" class="tabletextremark">'.liboea::Get_Word_Count_Div('AddiInfo_WordCountSpan').'</td>'."\n";
					$x .= '</tr>'."\n";
					$x .= '<tr>'."\n";
						$x .= '<td colspan="2" class="tabletextremark">'.$this->getLastModifiedInfoDisplay().'</td>'."\n";
					$x .= '</tr>'."\n";
				$x .= '</table>'."\n";
			}
			
			return $x;
		}
		
		public function getEditTable()
		{
			global $Lang, $linterface,$image_path,$LAYOUT_SKIN,$i_general_title,$i_LastModified;
			$ObjStudent = new liboea_student($_SESSION['UserID']);
//			$ObjStudent = new liboea_student(89898989898);
			$selfAccountInfo = $ObjStudent->getSelfAccount();


			$x = '';
			$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="2">'."\n";
			if(sizeof($selfAccountInfo) > 0){			

				$x .= '<tr>';
				$x .= '<td>';
					$x .= '<table style="border:dashed 1px #999999" width="99%" border="0" cellpadding="2" cellspacing="0">';
					$x .= '<tr><td>';
//					$x .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" align="absmiddle">';
					$x .= $Lang['iPortfolio']['OEA']['ReferenceToSelfAccount'];
					$x .= '</td></tr>';
					$x .= '<tr><td>';
					for($i = 0,$i_max = sizeof($selfAccountInfo);$i < $i_max;$i++){
						$_no = $i +1;
						$x .= '<table width = "100%">';
						$x .= '<tr>';
						$x .= '<td align="left">'.$_no.') '.$i_general_title.': <a href="javascript:showSelfAccountInfoDetails('.$selfAccountInfo[$i]['recordid'].')" class="tablelink">'.$selfAccountInfo[$i]['title'].'</a></td><td align="right">'.$i_LastModified.': '.$selfAccountInfo[$i]['ModifiedDate'].'<td></td>';
						$x .= '</tr>';
						$x .= '<tr style="display:none" id="selfAccount_'.$selfAccountInfo[$i]['recordid'].'">';
						$x .= '<td class="form_field_content" colspan="2">'.$selfAccountInfo[$i]['details'].'</td>';
						$x .= '</tr>';
						$x .= '</table>';
					}
					$x .= '</td></tr>';
					$x .= '</table>';
				$x .= '</td>';
				$x .= '</tr>';
			}						
//				### OLE Info
//				$x .= '<tr>'."\n";
//					$x .= '<td>'."\n";
//						$x .= '<table width="100%" border="0" cellpadding="2" cellspacing="0" style="border:dashed 1px #999999;">'."\n";
//							$x .= '<tr>'."\n";
//								$x .= '<td>'."\n";
//									$x .= $linterface->Get_View_Image();
//									$x .= '<span class="tabletextremark">'.$ec_iPortfolio['view_my_record'].'<br></span>'."\n";
//									$x .= '<table border="0" cellspacing="0" cellpadding="5">'."\n";
//										$x .= '<tr>'."\n";
//											$x .= '<td align="center" nowrap>'.$linterface->Get_Arrow_More_Icon($ec_iPortfolio['activity'], "jDISPLAY_RECORDS('activity')").'</td>';
//											$x .= '<td align="center" nowrap>'.$linterface->Get_Arrow_More_Icon($ec_iPortfolio['award'], "jDISPLAY_RECORDS('award')").'</td>';
//											$x .= '<td align="center" nowrap>'.$linterface->Get_Arrow_More_Icon($ec_iPortfolio['title_teacher_comments'], "jDISPLAY_RECORDS('comment')").'</td>';
//											$x .= '<td align="center" nowrap>'.$linterface->Get_Arrow_More_Icon($ec_iPortfolio['ole'], "jDISPLAY_RECORDS('ole')").'</td>';
//										$x .= '</tr>'."\n";
//									$x .= '</table>'."\n";
//									$x .= '<div id="RecordsLayer" style="overflow:auto;" />'."\n";
//								$x .= '</td>'."\n";
//							$x .= '</tr>'."\n";
//						$x .= '</table>'."\n";
//					$x .= '</td>'."\n";
//				$x .= '</tr>'."\n";
				
				### Title
//				$x .= '<tr>'."\n";
//					$x .= '<td>'."\n";
//						$x .= '<table width="100%" border="0" cellpadding="2" cellspacing="0">'."\n";
//							$x .= '<tr>'."\n";
//								$x .= '<td width="5%"><b>'.$Lang['iPortfolio']['OEA']['Title'].'</b></td>'."\n";
//								$x .= '<td><input type="text" id="AddiInfoTitle" name="AddiInfoTitle" class="textboxtext" value="'.intranet_htmlspecialchars($this->getTitle()).'" /></td>'."\n";
//							$x .= '</tr>'."\n";
//						$x .= '</table>'."\n";
//					$x .= '</td>'."\n";
//				$x .= '</tr>'."\n";
				
				### Details
				$x .= '<tr><td>'.$Lang['iPortfolio']['OEA']['Content'].'</td></tr>'."\n";				
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$thisID = 'AddiInfoDetails';
						$thisWordContSpanID = 'AddiInfo_WordCountSpan';
						$thisTags = ' style="width:99%;" onkeyup="js_Onkeyup_Info_Textarea(\''.$thisID.'\', \''.$thisWordContSpanID.'\', \''.$this->getDetailsMaxWordCount().'\', \''.$this->getDetailsMaxLength().'\');" ';
						$x .= $linterface->GET_TEXTAREA($thisID, $this->getDetails(), $taCols=70, $taRows=15, $OnFocus="", $readonly="", $thisTags);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= liboea::Get_Word_Count_Div($thisWordContSpanID);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			return $x;
		}
	}

}
?>