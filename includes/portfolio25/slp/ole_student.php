<?
# Modifying By: fai


class ole_student{
	private $objDB;
	private $eclass_db;
	private $intranet_db;

	private $RecordID;
	private $UserID;
	private $Details;
	private $Role;
	private $Hours;
	private $Achievement;
	private $Attachment;
	private $Remark;
	private $ApprovedBy;
	private $RequestApprovedBy;
	private $RecordStatus;
	private $ProcessDate;
	private $InputDate;
	private $ModifiedDate;
	private $ProgramID;
	private $TeacherComment;
	private $GroupID;
	private $EnrolType;
	private $SLPOrder;
	private $ComeFrom;



	public function ole_student($recordId = null){  
		global $eclass_db,$intranet_db;
		$this->objDB = new libdb();
		$this->eclass_db = $eclass_db;
		$this->intranet_db = $intranet_db;
		//LOAD THE PROGRAM DETAILS FORM STOREAGE (EG DB)
		if($recordId != null)
		{
			$this->recordID = intval($recordID);
			$this->loadFromStorage();
		}
	}

	public function setRecordID($val) {
		$this->RecordID = $val;
	}
	public function getRecordID() {
		return $this->RecordID;
	}

	public function setUserID($val) {
		$this->UserID = $val;
	}
	public function getUserID() {
		return $this->UserID;
	}

	public function setDetails($val) {
		$this->Details = $val;
	}
	public function getDetails() {
		return $this->Details;
	}

	public function setRole($val) {
		$this->Role = $val;
	}
	public function getRole() {
		return $this->Role;
	}

	public function setHours($val) {
		$this->Hours = $val;
	}
	public function getHours() {
		return $this->Hours;
	}

	public function setAchievement($val) {
		$this->Achievement = $val;
	}
	public function getAchievement() {
		return $this->Achievement;
	}

	public function setAttachment($val) {
		$this->Attachment = $val;
	}
	public function getAttachment() {
		return $this->Attachment;
	}

	public function setRemark($val) {
		$this->Remark = $val;
	}
	public function getRemark() {
		return $this->Remark;
	}

	public function setApprovedBy($val) {
		$this->ApprovedBy = $val;
	}
	public function getApprovedBy() {
		return $this->ApprovedBy;
	}

	public function setRequestApprovedBy($val) {
		$this->RequestApprovedBy = $val;
	}
	public function getRequestApprovedBy() {
		return $this->RequestApprovedBy;
	}

	public function setRecordStatus($val) {
		$this->RecordStatus = $val;
	}
	public function getRecordStatus() {
		return $this->RecordStatus;
	}

	public function setProcessDate($val) {
		$this->ProcessDate = $val;
	}
	public function getProcessDate() {
		return $this->ProcessDate;
	}

	public function setInputDate($val) {
		$this->InputDate = $val;
	}
	public function getInputDate() {
		return $this->InputDate;
	}

	public function setModifiedDate($val) {
		$this->ModifiedDate = $val;
	}
	public function getModifiedDate() {
		return $this->ModifiedDate;
	}

	public function setProgramID($val) {
		$this->ProgramID = $val;
	}
	public function getProgramID() {
		return $this->ProgramID;
	}

	public function setTeacherComment($val) {
		$this->TeacherComment = $val;
	}
	public function getTeacherComment() {
		return $this->TeacherComment;
	}

	public function setGroupID($val) {
		$this->GroupID = $val;
	}
	public function getGroupID() {
		return $this->GroupID;
	}

	public function setEnrolType($val) {
		$this->EnrolType = $val;
	}
	public function getEnrolType() {
		return $this->EnrolType;
	}

	public function setSLPOrder($val) {
		$this->SLPOrder = $val;
	}
	public function getSLPOrder() {
		return $this->SLPOrder;
	}

	public function setComeFrom($val) {
		$this->ComeFrom = $val;
	}
	public function getComeFrom() {
		return $this->ComeFrom;
	}

	public function saveRecord()
	{
		if(($this->RecordID != "") && intval($this->RecordID) > 0)
		{
			$resultId = $this->updateRecord();
		}
		else
		{	
			$resultId = $this->newRecord();
		}
		return $resultId;
	}

	private function updateRecord(){
		$DataArr = array();

		$DataArr["UserID"]				= $this->objDB->pack_value($this->getUserID(), "int");
		$DataArr["Details"]				= $this->objDB->pack_value($this->getDetails(), "str");
		$DataArr["Role"]				= $this->objDB->pack_value($this->getRole(), "str");
		$DataArr["Hours"]				= $this->objDB->pack_value($this->getHours(), "int");
		$DataArr["Achievement"]			= $this->objDB->pack_value($this->getAchievement(), "str");
		$DataArr["Attachment"]			= $this->objDB->pack_value($this->getAttachment(), "str");
		$DataArr["Remark"]				= $this->objDB->pack_value($this->getRemark(), "str");
		$DataArr["ApprovedBy"]			= $this->objDB->pack_value($this->getApprovedBy(), "int");
		$DataArr["RequestApprovedBy"]	= $this->objDB->pack_value($this->getRequestApprovedBy(), "int");
		$DataArr["RecordStatus"]		= $this->objDB->pack_value($this->getRecordStatus(), "int");
		$DataArr["ProcessDate"]			= $this->objDB->pack_value($this->getProcessDate(), "date");
		$DataArr["InputDate"]			= $this->objDB->pack_value($this->getInputDate(), "date");
		$DataArr["ModifiedDate"]		= $this->objDB->pack_value($this->getModifiedDate(), "date");
		$DataArr["ProgramID"]			= $this->objDB->pack_value($this->getProgramID(), "int");
		$DataArr["TeacherComment"]		= $this->objDB->pack_value($this->getTeacherComment(), "str");
		$DataArr["GroupID"]				= $this->objDB->pack_value($this->getGroupID(), "date");
		$DataArr["EnrolType"]			= $this->objDB->pack_value($this->getEnrolType(), "int");
		$DataArr["SLPOrder"]			= $this->objDB->pack_value($this->getSLPOrder(), "int");
		$DataArr["ComeFrom"]			= $this->objDB->pack_value($this->getComeFrom(), "int");

		$updateDetails = "";
		foreach ($DataArr as $fieldName => $data)
		{
			$updateDetails .= $fieldName."=".$data.",";
		}

		//REMOVE LAST OCCURRENCE OF ",";
		$updateDetails = substr($updateDetails,0,-1);

		$sql = "update ".$this->eclass_db.".OLE_STUDENT set ".$updateDetails." where recordid = ".$this->getRecordID();

		$this->objDB->db_db_query($sql);
		return $this->getRecordID();
	}
	private function newRecord(){
		$DataArr["UserID"]						= $this->objDB->pack_value($this->getUserID(), "int");
		$DataArr["Details"]						= $this->objDB->pack_value($this->getDetails(), "str");
		$DataArr["Role"]						= $this->objDB->pack_value($this->getRole(), "str");
		$DataArr["Hours"]						= $this->objDB->pack_value($this->getHours(), "int");
		$DataArr["Achievement"]					= $this->objDB->pack_value($this->getAchievement(), "str");
		$DataArr["Attachment"]					= $this->objDB->pack_value($this->getAttachment(), "str");
		$DataArr["Remark"]						= $this->objDB->pack_value($this->getRemark(), "str");
		$DataArr["ApprovedBy"]					= $this->objDB->pack_value($this->getApprovedBy(), "int");
		$DataArr["RequestApprovedBy"]					= $this->objDB->pack_value($this->getRequestApprovedBy(), "int");
		$DataArr["RecordStatus"]					= $this->objDB->pack_value($this->getRecordStatus(), "int");
		$DataArr["ProcessDate"]					= $this->objDB->pack_value($this->getProcessDate(), "date");
		$DataArr["InputDate"]					= $this->objDB->pack_value($this->getInputDate(), "date");
		$DataArr["ModifiedDate"]					= $this->objDB->pack_value($this->getModifiedDate(), "date");
		$DataArr["ProgramID"]					= $this->objDB->pack_value($this->getProgramID(), "int");
		$DataArr["TeacherComment"]					= $this->objDB->pack_value($this->getTeacherComment(), "str");
		$DataArr["GroupID"]					= $this->objDB->pack_value($this->getGroupID(), "date");
		$DataArr["EnrolType"]					= $this->objDB->pack_value($this->getEnrolType(), "int");
		$DataArr["SLPOrder"]					= $this->objDB->pack_value($this->getSLPOrder(), "int");
		$DataArr["ComeFrom"]					= $this->objDB->pack_value($this->getComeFrom(), "int");
		$sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);

		$fieldStr= $sqlStrAry['sqlField'];
		$valueStr= $sqlStrAry['sqlValue'];


		$sql = 'Insert Into '.$this->eclass_db.'.OLE_STUDENT ('.$fieldStr.') Values ('.$valueStr.')';

		$success = $this->objDB->db_db_query($sql);
								
		$RecordID = $this->objDB->db_insert_id();

		$this->recordID = $RecordID;
					
		$this->loadFormStorage();
		return $this->recordID;


	}
	private function loadFormStorage(){
		$recordId = $this->getRecordID();

		if(trim($recordId) == "" || intval($recordId< 0))
		{
			//FOR THE PROGRAM ID IS NOT VALIDE VALUE
			$result = null;
		}
		else
		{
			$sql = "select 
						s.RecordID , 
						s.UserID, 
						s.Details,
						s.Role,
						s.Hours,
						s.Achievement,
						s.attachment,
						s.remark,
						s.approvedby , 
						s.requestApprovedBy , 
						s.recordstatus
						s.processDate , 
						s.inputdate , 
						s.modifiedDate , 
						s.programid , 
						s.teachercomment , 
						s.groupid , 
						s.enroltype , 
						s.SLPOrder , 
						s.comeFrom from
					OLE_STUDENT 
						inner join INTRANET_USER as i on i.userid = s.userid where s.recordid = ".$recordId;

			$rsTmp = $this->objDB->returnArray($sql);

			//suppose should return one reccord only
			if(is_array($rsTmp) && sizeof($rsTmp) == 1)
			{
				$rsTmp = current($rsTmp);

				$this->setUserID($rsTmp['UserID']);
				$this->setDetails($rsTmp['Details']);
				$this->setRole($rsTmp['Role']);
				$this->setHours($rsTmp['Hours']);
				$this->setAchievement($rsTmp['Achievement']);
				$this->setAttachment($rsTmp['Attachment']);
				$this->setRemark($rsTmp['Remark']);
				$this->setApprovedBy($rsTmp['ApprovedBy']);
				$this->setRequestApprovedBy($rsTmp['RequestApprovedBy']);
				$this->setRecordStatus($rsTmp['RecordStatus']);
				$this->setProcessDate($rsTmp['ProcessDate']);
				$this->setInputDate($rsTmp['InputDate']);
				$this->setModifiedDate($rsTmp['ModifiedDate']);
				$this->setProgramID($rsTmp['ProgramID']);
				$this->setTeacherComment($rsTmp['TeacherComment']);
				$this->setGroupID($rsTmp['GroupID']);
				$this->setEnrolType($rsTmp['EnrolType']);
				$this->setSLPOrder($rsTmp['SLPOrder']);
				$this->setSLPOrder($rsTmp['ComeFrom']);

			}
		}

	}

}
?>
