<?php
# modifying : connie

/*****************************************************************
 * Modification Log:
 * 
 * Connie (20111128) : added Get_Student_OLE_Info 
 * 
 * 
 *****************************************************************/

if (!defined("LIBPF_OLE_DEFINED"))         // Preprocessor directives
{
	define("LIBPF_OLE_DEFINED",true);

	class libpf_ole extends libdb {
		
		public function libpf_ole() 
		{
			$this->libdb();
		}
		
		private function Get_IntranetDB_Table_Name($ParTable) 
		{
			global $intranet_db;
			return $intranet_db.'.'.$ParTable;
		}
		
		private function Get_eClassDB_Table_Name($ParTable) 
		{
			global $eclass_db;
			return $eclass_db.'.'.$ParTable;
		}
		
		public function Get_OLE_Info($FormIDArr='', $ProgramIDArr='')
		{
			global $ipf_cfg;
			
			$OLE_PROGRAM = $this->Get_eClassDB_Table_Name('OLE_PROGRAM');
			$OLE_STUDENT = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
			$YEAR_CLASS_USER = $this->Get_IntranetDB_Table_Name('YEAR_CLASS_USER');
			$YEAR_CLASS = $this->Get_IntranetDB_Table_Name('YEAR_CLASS');
			
			if ($FormIDArr != '') {
				$sql = "Select
								op.ProgramID
						From
								$OLE_PROGRAM as op
								Inner Join $OLE_STUDENT as os On (op.ProgramID = os.ProgramID)
								Inner Join $YEAR_CLASS_USER as ycu On (os.UserID = ycu.UserID)
								Inner Join $YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Where
								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
								And yc.YearID In ('".implode("','", (array)$FormIDArr)."')
								And os.RecordStatus In ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."', '".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."')
						";
				$FormStudentProgramArr = $this->returnResultSet($sql);
				$FormStudentProgramIDArr = Get_Array_By_Key($FormStudentProgramArr, 'ProgramID');
				
				$conds_FormStudentProgramID = " And op.ProgramID In ('".implode("','", (array)$FormStudentProgramIDArr)."') ";
			}
			
			if ($ProgramIDArr != '') {
				$conds_ProgramID = " And op.ProgramID In ('".implode("','", (array)$ProgramIDArr)."') ";
			}
		
			$sql = "Select
							op.ProgramID,
							op.Title as TitleEn,
							op.TitleChi as TitleCh,
							op.IntExt,
							op.AcademicYearID,
							op.YearTermID,
							op.StartDate,
							op.EndDate,
							op.Category,
							op.ELE,
							op.Details,
							op.Organization
					From
							$OLE_PROGRAM as op
					Where
							1
							$conds_FormStudentProgramID
							$conds_ProgramID
					";
				
			return $this->returnResultSet($sql);
		}
		
		public function Get_Student_OLE_Info($StudentIDArr='', $RecordIDArr='', $forSlpOnly=true, $CategoryArr='', $AcademicYearIDAry='', $IntExt='', $OrderBy='')
		{			
			global $ipf_cfg;
			
			$OLE_PROGRAM = $this->Get_eClassDB_Table_Name('OLE_PROGRAM');
			$OLE_STUDENT = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
	
			if ($StudentIDArr != '') {
				$cond_StudentID = " And os.UserID In ('".implode("','", (array)$StudentIDArr)."') ";
			}
			
			if ($RecordIDArr != '') {
				$cond_RecordID = " And os.RecordID In ('".implode("','", (array)$RecordIDArr)."') ";
			}
			
			if ($forSlpOnly === '') {
				// return all data
			}
			else if ($forSlpOnly == true) {
				$conds_SLPOrder = " And os.SLPOrder is not null ";
			}
			else {
				$conds_SLPOrder = " And os.SLPOrder is null ";
			}
			
			if ($CategoryArr != '') {
				$cond_Category = " And op.Category In ('".implode("','", (array)$CategoryArr)."') ";
			}
			
			if ($AcademicYearIDAry != '') {
				$cond_AcademicYearID = " And op.AcademicYearID In ('".implode("','", (array)$AcademicYearIDAry)."') ";
			}
			
			if ($IntExt != '') {
				$cond_IntExt = " And op.IntExt In ('".implode("','", (array)$IntExt)."') ";
			}
			
			if (strtolower($OrderBy) == strtolower('StartDate')) {
				$OrderByField = 'op.StartDate asc';
			}
			else {
				$OrderByField = 'os.SLPOrder asc';
			}
			
			$sql = "Select 
							os.RecordID as OleStudentRecordID,
							op.ProgramID as ProgramID,
							op.Title as ProgramTitle,
							os.Organization as Organization,
							os.Role,
							os.Hours,
							os.Achievement,
							op.IntExt,
							op.ELE
					From
							$OLE_PROGRAM as op
							Inner Join $OLE_STUDENT as os On (op.ProgramID = os.ProgramID)
					Where 
							os.RecordStatus In ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."', '".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."')
							$cond_StudentID
							$cond_RecordID
							$conds_SLPOrder
							$cond_Category
							$cond_AcademicYearID
							$cond_IntExt
					Order by
							$OrderByField
						";
			return $this->returnResultSet($sql);
		}

		
		public function Get_ELE_Info($RecordStatusArr='', $OrderBy='')
		{
			$conds_RecordStatus = '';
			if ($RecordStatusArr != '' && count((array)$RecordStatusArr) > 0) {
				$conds_RecordStatus = " And ele.RecordStatus In (".implode(',', (array)$RecordStatusArr).") ";
			}
			
			if ($OrderBy == '') {
				$OrderByField = 'ele.EngTitle';
			}
			else {
				$OrderByField = 'ele.'.$OrderBy;
			}
			
			$OLE_ELE = $this->Get_eClassDB_Table_Name('OLE_ELE');
			$sql = "Select
							ele.RecordID,
							ele.ChiTitle,
							ele.EngTitle,
							ele.DefaultID,
							If (
									ele.DefaultID Is Null Or ele.DefaultID = '',
									Concat('[', ele.RecordID, ']'),
									ele.DefaultID
							) as ELECode
					From
							$OLE_ELE as ele
					Where
							1
							$conds_RecordStatus
					Order By
							$OrderByField
					";
			return $this->returnArray($sql);
		}
		
		
		
		/** Get distinct role and number of users  from OLE_STUDENT

        * @owner : Connie (20111010)

        * @param : $skipRoleArr (to determine which role should be skipped)
        * @param : $RoleArr (to determine which role should be included)

        * @return : Array(s) of <Option> (there may be more than one record)

        */	
		public function getOLE_RoleSelection($skipRoleArr='',$RoleStr='')
		{
			global $ipf_cfg; 
			
			$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');
			$OLE_OEA_mapping = $this->Get_eClassDB_Table_Name('OEA_OLE_ROLE_MAPPING');
			$RoleStr = $this->Get_Safe_Sql_Like_Query($RoleStr);
						
			if ($skipRoleArr !== '') {
				$conds_RoleSkip = " And role Not In ('".implode("','", (array)$skipRoleArr)."') ";
			}
			if ($RoleStr != '') {
				$conds_Role = " And role like '%".$RoleStr."%' ";
			}
			else
			{
				$conds_Role='AND 1';
			}
			
		
			
			$sql = "Select
							role,count(distinct(UserID)) as count
					From
							$OLE_STD 				
					Where
							1							
							$conds_Role
							$conds_RoleSkip
							And
							role!='' and role is not null
					group by role
					order by role
					";
			$roleData = $this->returnArray($sql,2);
			
			$roleMenu .= "<select id=\"RoleSelSel\" name=\"SelectedStudentArr[]\"  multiple=\"true\">";
//debug_r($sql);
//debug_r(sizeof($roleData));
			for($i=0; $i<sizeof($roleData); $i++) {			
				
				$thisRole = intranet_htmlspecialchars($roleData[$i]['role']);
				$roleMenu .= "<OPTION value='".$thisRole.":__:".$roleData[$i]['count']."'>".$thisRole."&nbsp;&nbsp;"."(".$roleData[$i]['count'].")"."</OPTION>";				
			}
			$roleMenu .= "</select>";
			

			return $roleMenu;
		}
		
		
		/** Get role and number of users  from OLE_STUDENT (inner join OEA_OLE_ROLE_MAPPING based on the role)

        * @owner : Connie (20111011)
        * @param : $roleType (eg: L,M,C)
        * @return : Array(s) of <Option> (there may be more than one record)
        */	
		public function getOLE_Role_selected($roleType)
		{
			global $ipf_cfg; 
			
			$OLE_OEA_mapping = $this->Get_eClassDB_Table_Name('OEA_OLE_ROLE_MAPPING');
			$OLE_STD = $this->Get_eClassDB_Table_Name('OLE_STUDENT');

			
			$sql = "Select
							OLE_ROLE,count(distinct(UserID)) as count
					From
							$OLE_OEA_mapping
							
					inner join 
							$OLE_STD on OLE_ROLE=role
					Where
							OEA_ROLE='$roleType'
							And
							role!='' and role is not null

					group by role
					order by role
					";


//debug_r($sql);

			$_selectID = 'SelectedRoleSel'.'_'.$roleType;
			$_selectNameArr = $roleType.'_SelectedRoleNameArr[]';
	
			$roleData = $this->returnArray($sql,2);
//$e = mysql_error();
//debug_r($e);
//if($e){
//	debug_r($e);
//	debug_r($sql);
//}
//			
//debug_r(count($roleData));			
			$roleMenu .= "<select id='".$_selectID."' name='".$_selectNameArr."'. size=\"10\" style=\"width: 378px;\" multiple=\"true\">";							
			
//debug_r(sizeof($roleData));				
			for($i=0,$i_MAX=sizeof($roleData); $i<$i_MAX; $i++) {			
							
				$thisRole = intranet_htmlspecialchars($roleData[$i]['OLE_ROLE']);
//debug_r($thisRole);		
				$roleMenu .= "<OPTION value='".$thisRole.":__:".$roleData[$i]['count']."'>".$thisRole."&nbsp;&nbsp;"."(".$roleData[$i]['count'].")"."</OPTION>";				
			
			}
			$roleMenu .= "</select>";
	
			return $roleMenu;
		}
		
		/** Get insert data into OEA_OLE_ROLE_MAPPING (firstly should delete all data in OEA_OLE_ROLE_MAPPING)

        * @owner : Connie (20111011)

        * @param : $OLE_Role (role from OLE_STUDENT)
        * @param : $OEA_Role 

        * @return : boolean $returnResult (true: success / false: fail)

        */	
		public function InsertInto_OEA_OLE_RoleMapping($OLE_Role,$OEA_Role)
		{
		
			$OLE_OEA_mapping = $this->Get_eClassDB_Table_Name('OEA_OLE_ROLE_MAPPING');
			$OLE_Role = $this->Get_Safe_Sql_Query($OLE_Role);
			$OEA_Role = $this->Get_Safe_Sql_Query($OEA_Role);			

			$sql = "insert into
							$OLE_OEA_mapping
							(OLE_ROLE, OEA_ROLE)
			
					values(							
							'$OLE_Role',
							'$OEA_Role'
							)
					";
			$returnResult=$this->db_db_query($sql);

			return $returnResult;
		}
		
		
		/** delete all data in OEA_OLE_ROLE_MAPPING

        * @owner : Connie (20111011)
        * @return : boolean $returnResult (true: success / false: fail)
        */	
		public function deleteAll_OEA_OLE_RoleMapping()
		{
			$OLE_OEA_mapping = $this->Get_eClassDB_Table_Name('OEA_OLE_ROLE_MAPPING');
			$sql="delete from $OLE_OEA_mapping";
			$returnResult=$this->db_db_query($sql);

			return $returnResult;
		}


	}
}
?>