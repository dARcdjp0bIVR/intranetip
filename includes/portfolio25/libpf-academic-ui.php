<?php
## Using By :
##############################################
##	Modification Log:
##	2017-02-07 Villa
##	- add showing WebSAMS code beside the subject name
##	2016-07-06 Omas
##	- modified Get_Settings_Subject_FullMark_Table() - fix show all form colspan wrong in different viewMode problem 
##############################################

class libpf_academic_ui extends interface_html {

	public function libpf_academic_ui(){
		parent::interface_html();
		
		$this->thickBoxWidth  = 750;
		$this->thickBoxHeight = 450;
		$this->toolCellWidth = 100;
	}
	
	public function Get_Settings_Subject_FullMark_Table($AcademicYearID, $YearID, $ViewMode='') 
	{
		global $Lang, $ec_iPortfolio, $libpf_academic;
		
		$libfcm = new form_class_manage();
		$libscm = new subject_class_mapping();
		
		$ShowMarkColumn = ($ViewMode=='' || $ViewMode=='Mark')? true : false;
		$ShowGradeColumn = ($ViewMode=='' || $ViewMode=='Grade')? true : false;
		
		
		$FormInfoArr = $libfcm->Get_Form_List(true, 1, '', $YearID); 
		$numOfForm = count($FormInfoArr);
		
		$SubjectInfoArr = $libscm->Get_Subject_List_With_Component();
		$numOfSubject = count($SubjectInfoArr);
		
		//$SubjectFullMarkInfoArr[$AcademicYearID][$YearID][$SubjectID]['FullMarkInt', 'FullMarkGrade'] = Value
		$SubjectFullMarkInfoArr = $libpf_academic->Get_Subject_Full_Mark($AcademicYearID);
	
		$SubjectColWidth = 20;
		$FormColWidth = floor( (100 - $SubjectColWidth) / ($numOfForm * 2) );	// each Form has Mark and Grade Columns
		
		$x = '';
		$hiddenExport = '';
		$x .= '<table id="ContentTable" class="common_table_list_v30 edit_table_list_v30">'."\n";
			$x .= '<col style="width:'.$SubjectColWidth.'%;">'."\n";
			for ($i=0; $i<$numOfForm; $i++)	{
				if ($ShowMarkColumn) {
					$x .= '<col style="width:'.($FormColWidth/2).'%;">'."\n";	// FullMark
					$x .= '<col style="width:'.($FormColWidth/2).'%;">'."\n";	// PassMark
				}
				
				if ($ShowGradeColumn) {
					$x .= '<col style="width:'.$FormColWidth.'%;">'."\n";	// Grade
				}
			}
			
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th rowspan="3" style="width:'.$SubjectColWidth.'%;">'."\n";
						$x .= '<span class="row_content_v30">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</span>'."\n";
					$x .= '</th>'."\n";
					for ($i=0; $i<$numOfForm; $i++)	
					{
						$thisFormName = $FormInfoArr[$i]['YearName'];
//						$thisColspan = ($ViewMode=='')? 2 : 1;
// 						$thisColspan = 3;
						$thisColspan = 0;
						if ($ShowMarkColumn){
							$thisColspan++;
							$thisColspan++;
						}
						if ($ShowGradeColumn){
							$thisColspan++;
						}
						
						
						$x .= '<th colspan="'.$thisColspan.'">'."\n";
							$x .= '<span class="row_content_v30">'.$thisFormName.'</span>'."\n";
						$x .= '</th>'."\n";
					}
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					for ($i=0; $i<$numOfForm; $i++)	
					{
						$thisFormName = intranet_htmlspecialchars($FormInfoArr[$i]['YearName']);
						$hiddenExport .= '<input type="hidden" name="ExportFormNameArr[]" value="'.$thisFormName.'" />'."\n";
						
						if ($ShowMarkColumn) {
							$x .= '<th><span class="row_content_v30">'.$Lang['iPortfolio']['SubjectFullMark'].'</span></th>'."\n";
							$x .= '<th><span class="row_content_v30">'.$ec_iPortfolio['PassMark'].'</span></th>'."\n";
						}
						
						if ($ShowGradeColumn) {
							$x .= '<th><span class="row_content_v30">'.$Lang['General']['Grade'].'</span></th>'."\n";
						}
					}
				$x .= '</tr>'."\n";
				
				$x .= '<tr class="edit_table_head_bulk">'."\n";
					for ($i=0; $i<$numOfForm; $i++)	
					{
						$thisYearID = $FormInfoArr[$i]['YearID'];
						
						// Mark
						if ($ShowMarkColumn) {
							$thisTbID = 'Global_FullMarkInt_'.$thisYearID;
							$thisTextbox = $this->GET_TEXTBOX_NUMBER($thisTbID, $thisName, '');
							
							$thisClass = 'FullMarkIntTb_'.$thisYearID;
							$thisHref = "javascript:Apply_Value_To_All('".$thisClass."', document.getElementById('".$thisTbID."').value);";
							$thisApplyAllIcon = $this->Get_Apply_All_Icon($thisHref, $Lang['Btn']['ApplyToAll']);
							
							$x .= '<th style="text-align:center;">'."\n";
								$x .= '<div style="float:left;">'.$thisTextbox.'</div>'."\n";
								$x .= $thisApplyAllIcon."\n";
							$x .= '</th>'."\n";
							
							$thisTbID = 'Global_PassMarkInt_'.$thisYearID;
							$thisTextbox = $this->GET_TEXTBOX_NUMBER($thisTbID, $thisName, '');
							
							$thisClass = 'PassMarkIntTb_'.$thisYearID;
							$thisHref = "javascript:Apply_Value_To_All('".$thisClass."', document.getElementById('".$thisTbID."').value);";
							$thisApplyAllIcon = $this->Get_Apply_All_Icon($thisHref, $Lang['Btn']['ApplyToAll']);
							
							$x .= '<th style="text-align:center;">'."\n";
								$x .= '<div style="float:left;">'.$thisTextbox.'</div>'."\n";
								$x .= $thisApplyAllIcon."\n";
							$x .= '</th>'."\n";
						}
						
						// Grade
						if ($ShowGradeColumn) {
							$thisTbID = 'Global_FullMarkGrade_'.$thisYearID;
							$thisTextbox = $this->GET_TEXTBOX_NUMBER($thisTbID, $thisName, '');
							
							$thisClass = 'FullMarkGradeTb_'.$thisYearID;
							$thisHref = "javascript:Apply_Value_To_All('".$thisClass."', document.getElementById('".$thisTbID."').value);";
							$thisApplyAllIcon = $this->Get_Apply_All_Icon($thisHref, $Lang['Btn']['ApplyToAll']);
							
							$x .= '<th style="text-align:center;">'."\n";
								$x .= '<div style="float:left;">'.$thisTextbox.'</div>'."\n";
								$x .= $thisApplyAllIcon."\n";
							$x .= '</th>'."\n";
						}
					}
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
				for ($i=0; $i<$numOfSubject; $i++)
				{
					$thisColumnCount = 0;
					$thisSubjectID = $SubjectInfoArr[$i]['SubjectID'];
					$thisSubjectName = Get_Lang_Selection($SubjectInfoArr[$i]['SubjectDescB5'], $SubjectInfoArr[$i]['SubjectDescEN']);
					$thisIsComponent = $SubjectInfoArr[$i]['IsComponent'];
					
					if ($thisIsComponent) {
						$thisPrefix = '&nbsp;&nbsp;&nbsp;';
						$thisPrefixExport = '  ';
						$thisRowCss = 'dataRow';
						$WebSAMSCode = $SubjectInfoArr[$i]['Cmp_WEBSAMSCode'];
					}
					else {
						$thisPrefix = '';
						$thisPrefixExport = '';
						$thisRowCss = 'dataRow row_drafted';
						$WebSAMSCode = $SubjectInfoArr[$i]['WEBSAMSCode'];
					}
					
					
					$x .= '<tr class="'.$thisRowCss.'">'."\n";
					
						$x .= '<td>'.$thisPrefix."[".$WebSAMSCode."]"."&nbsp;".$thisSubjectName.'</td>'."\n";
						$hiddenExport .= '<input type="hidden" name="ExportContentArr['.$i.']['.$thisColumnCount.']" value="'.$thisPrefixExport.intranet_htmlspecialchars($thisSubjectName).'" />'."\n";
						
						for ($j=0; $j<$numOfForm; $j++)	
						{
							$thisYearID = $FormInfoArr[$j]['YearID'];
							$thisFullMarkInt = $SubjectFullMarkInfoArr[$AcademicYearID][$thisYearID][$thisSubjectID]['FullMarkInt'];
							$thisPassMarkInt = $SubjectFullMarkInfoArr[$AcademicYearID][$thisYearID][$thisSubjectID]['PassMarkInt'];
							$thisFullMarkGrade = $SubjectFullMarkInfoArr[$AcademicYearID][$thisYearID][$thisSubjectID]['FullMarkGrade'];
							
							// Mark
							if ($ShowMarkColumn) {
								$thisID = 'mark['.$i.']['.$thisColumnCount.']';
								$thisName = 'FullMarkInfoArr['.$thisYearID.']['.$thisSubjectID.'][FullMarkInt]';
								$thisClass = 'FullMarkIntTb_'.$thisYearID;
								$thisOtherParArr = array(	'onpaste'=>'pasteContent(\''.$i.'\', \''.$thisColumnCount.'\');',
										    				'onkeyup'=>'isPasteContent(event, \''.$i.'\', \''.$thisColumnCount.'\');',
										    				'onkeypress'=>'isPasteContent(event, \''.$i.'\', \''.$thisColumnCount.'\');',
										    				'onkeydown'=>'isPasteContent(event, \''.$i.'\', \''.$thisColumnCount.'\');'
										    			);
								$thisTextbox = $this->GET_TEXTBOX_NUMBER($thisID, $thisName, $thisFullMarkInt, $thisClass, $thisOtherParArr);
								
								$x .= '<td>'.$thisPrefix.$thisTextbox.'</td>'."\n";
								$hiddenExport .= '<input type="hidden" name="ExportContentArr['.$i.']['.($thisColumnCount + 1).']" value="'.intranet_htmlspecialchars($thisFullMarkInt).'" />'."\n";
								$thisColumnCount++;
								
								$thisID = 'mark['.$i.']['.$thisColumnCount.']';
								$thisName = 'FullMarkInfoArr['.$thisYearID.']['.$thisSubjectID.'][PassMarkInt]';
								$thisClass = 'PassMarkIntTb_'.$thisYearID;
								$thisOtherParArr = array(	'onpaste'=>'pasteContent(\''.$i.'\', \''.$thisColumnCount.'\');',
										    				'onkeyup'=>'isPasteContent(event, \''.$i.'\', \''.$thisColumnCount.'\');',
										    				'onkeypress'=>'isPasteContent(event, \''.$i.'\', \''.$thisColumnCount.'\');',
										    				'onkeydown'=>'isPasteContent(event, \''.$i.'\', \''.$thisColumnCount.'\');'
										    			);
								$thisTextbox = $this->GET_TEXTBOX_NUMBER($thisID, $thisName, $thisPassMarkInt, $thisClass, $thisOtherParArr);
								
								$x .= '<td>'.$thisPrefix.$thisTextbox.'</td>'."\n";
								$hiddenExport .= '<input type="hidden" name="ExportContentArr['.$i.']['.($thisColumnCount + 1).']" value="'.intranet_htmlspecialchars($thisPassMarkInt).'" />'."\n";
								$thisColumnCount++;
							}
							
							// Grade
							if ($ShowGradeColumn) {
								$thisID = 'mark['.$i.']['.$thisColumnCount.']';
								$thisName = 'FullMarkInfoArr['.$thisYearID.']['.$thisSubjectID.'][FullMarkGrade]';
								$thisClass = 'FullMarkGradeTb_'.$thisYearID;
								$thisOtherParArr = array(	'onpaste'=>'pasteContent(\''.$i.'\', \''.$thisColumnCount.'\');',
										    				'onkeyup'=>'isPasteContent(event, \''.$i.'\', \''.$thisColumnCount.'\');',
										    				'onkeypress'=>'isPasteContent(event, \''.$i.'\', \''.$thisColumnCount.'\');',
										    				'onkeydown'=>'isPasteContent(event, \''.$i.'\', \''.$thisColumnCount.'\');'
										    			);
								$thisTextbox = $this->GET_TEXTBOX_NUMBER($thisID, $thisName, $thisFullMarkGrade, $thisClass, $thisOtherParArr);
								
								$x .= '<td>'.$thisPrefix.$thisTextbox.'</td>'."\n";
								$hiddenExport .= '<input type="hidden" name="ExportContentArr['.$i.']['.($thisColumnCount + 1).']" value="'.intranet_htmlspecialchars($thisFullMarkGrade).'" />'."\n";
								$thisColumnCount++;
							}
						}
					$x .= '</tr>'."\n";
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		### Hidden Field for Export
		$x .= $hiddenExport."\n";
		
		### Javascript for copy and paste
		$x .= '	<script language="javascript">
					var rowx = 0, coly = 0;			//init
					var xno = '.$numOfSubject.'; yno = '.$thisColumnCount.';		// set table size
					var startContent = "";
					var setStart = 1;
				</script>
				';

		return $x;
	} 
}
?>