<?php

include_once($intranet_root."/includes/libdb.php");
$ldb = new libdb();

class libportfolio {

  var $course_id;

  function __construct(){
    $this->SET_COURSE_ID();
  }
  
  function SET_COURSE_ID(){
    global $ldb, $eclass_db;
  
    $sql =  "
              SELECT
                course_id
              FROM
                {$eclass_db}.course
              WHERE
                RoomType = 4
            ";
    $row = $ldb->returnVector($sql);
    
    $this->SET_CLASS_VARIABLE("course_id", $row[0]);
  }
  
  function GET_COURSE_ID(){
    
    if(empty($this->course_id))
    {
      $this->SET_COURSE_ID();
    }
    
    $this->GET_CLASS_VARIABLE($this->course_id);
  }
  
  
  function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    $this->{$ParVariableName} = $ParValue;
  }
  
  function GET_CLASS_VARIABLE($ParVariableName)
  {
    return $this->{$ParVariableName};
  }
}

?>