<?php

/**
 * Intend to be extended by sub-classes which need auto class loading feature.
 */
class StudentReportClassLoader
{
	/**
	 * Sub-classes should override this variable to provide their own member to class mapping.
	 * This variable is used to find the class to instantiate when the corresponding member variable is accessed.
	 * For example, if $member_to_class_map is set to array('my_var' => 'MyClass'), and $this->my_var is accessed, then MyClass will be included and $this->my_var will be set to "new MyClass()".
	 */
	protected $member_to_class_map = array(
		// Member variable name => Class to instantiate
		'class_loader' => 'StudentReportClassLoader'
	);
	
	/**
	 * Just make use of $member_to_class_map to find the class to instantiate.
	 */
	function __get($name) {
		$class = $this->member_to_class_map[$name];
		if (!$class) throw new Exception("The member variable '$name' does not exist.");
		
		// Include the file with the class if needed.
		if (!class_exists($class)) {
			// Assume that the class file is in the same directory as this script.
			$class_file = dirname(__FILE__) . "/$class.php";
			if (!file_exists($class_file)) throw new Exception("Cannot find the file for the class '$class'.");
			
			include_once $class_file;
			if (!class_exists($class)) throw new Exception("Cannot find the class '$class'.");
		}
		
		// Instantiate the class, and save it in a member variable, so that this function will not be called again when the member variable is accessed again.
		$this->$name = new $class();
		
		return $this->$name;
	}
}

class StudentReportPublicClassLoader extends StudentReportClassLoader
{
  public $member_to_class_map;
}

?>
