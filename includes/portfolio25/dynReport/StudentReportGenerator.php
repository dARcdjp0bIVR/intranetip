<?php
class StudentReportGenerator
{
	protected function return_statistics_row_html($statisticsSettingsAry, $tableSectionAry, $tableDataAry) {
		$numOfStatisticsRow = count((array)$statisticsSettingsAry);
		$columnAry = $tableSectionAry['columns'];
		
		$x = '';
		for ($i=0; $i<$numOfStatisticsRow; $i++) {
			$_numOfStatisticsColumn = count((array)$statisticsSettingsAry[$i]);
			
			$x .= <<<HTMLEND
				<tr nobr="true" style="{$this->generate_css_style($tableSectionAry['row_statistics_style'])}">
HTMLEND;

			for ($j=0; $j<$_numOfStatisticsColumn; $j++) {
				$__statType = $statisticsSettingsAry[$i][$j]['statType'];
				$__statInputText = $statisticsSettingsAry[$i][$j]['statInputText'];
				
				$width_attribute = $columnAry[$j]['width'] ? ('width="' . $columnAry[$j]['width'] . '"') : '';
			
				$__cellDisplay = '';
				if ($__statType == 'text') {
					$__cellDisplay = $__statInputText;
				}
				else if ($__statType == 'sum') {
					$__cellDisplay = array_sum((array)$tableDataAry[$j]);
				}
				
				$x .= <<<HTMLEND
					<td $width_attribute>{$this->transform_table_cell_data($__cellDisplay, $print_criteria)}</td>
HTMLEND;
			}
										
			$x .= "
				</tr>
			";
		}
		
		return $x;
	}
	
	/**
	 * Transform the data in a table cell:
	 * - Trim the leading and trailing spaces.
	 * - nl2br. (after trim, to prevent new line after the last line)
	 * (Assume that the data has been HTML escaped here? So no need to run htmlspecialchars?)
	 */
	protected function transform_table_cell_data($data, $print_criteria=array()) {
		$EmptyDataSymbol = $print_criteria['EmptyDataSymbol'];
		
		$DataDisplay = nl2br(trim($data));
		if ($DataDisplay == '') {
			$DataDisplay = $EmptyDataSymbol;
		}
		
		return $DataDisplay;
		//return nl2br(trim($data));
	}

	/**
	 * Generate CSS style values for the HTML style attribute, given some style information.
	 * @param $style Actually come from StudentReportTemplate, like
	 * 		array(
	 * 			'font_family' => ?,
	 * 			'font_size' => ?,
	 * 			'is_bold' => ?,
	 * 			'is_italic' => ?,
	 * 			'is_underline' => ?,
	 * 			'text_color' => ?,
	 * 			'background_color' => ?
	 * 		)
	 * @return CSS style string, like 'background-color:...; font-size:...; ...'.
	 */
	protected function generate_css_style($style) {
		global $ipf_cfg,$sys_custom;
		
		$css = '';
		
		//C91783
		//if ($v = $style['font_family']) $css .= "font-family:$v;";
		if ($v = $style['font_family']) {
			if($sys_custom['iPf']['DynamicReport']['WholeReportUsingDefaultFont']){
				$newFontFamily = $ipf_cfg['dynReport']['fontMappingArr'][$v];				
			}
			
			$targetFontFamily = $v;
			if ($newFontFamily != '') {
				$targetFontFamily = $newFontFamily;
			}
			$css .= "font-family:$targetFontFamily;";
		}
		if ($v = $style['font_size']) $css .= "font-size:$v;";
		if ($v = $style['is_bold']) $css .= "font-weight:bold;";
		if ($v = $style['is_italic']) $css .= "font-style:italic;";
		if ($v = $style['is_underline']) $css .= "text-decoration:underline;";
		if ($v = $style['text_color']) $css .= "color:$v;";
		if ($v = $style['background_color']) $css .= "background-color:$v;";
		return $css;
	}
}
?>