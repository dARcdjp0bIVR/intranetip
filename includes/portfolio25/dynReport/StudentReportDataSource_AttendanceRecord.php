<?php

include_once 'StudentReportDataSourceBase_TableSection.php';
include_once 'srt_lang.php';

/**
 * The data source for accessing attendance record data.
 * This data source is supposed to be managed by StudentReportDataSourceManager.
 */
class StudentReportDataSource_AttendanceRecord extends StudentReportDataSourceBase_TableSection
{
	/**
	 * Get the display name of the data source.
	 */
	function get_display_name() {
		return srt_lang('Attendance');
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_field_information() for the detail description of this method.
	 */
	function get_field_information() {
		return array(
			'AttendanceDate' => array(
				'display_name' => srt_lang('Date'),
				'is_numeric' => false,
			),
			'DayType' => array(
				'display_name' => srt_lang('Period (Whole day/AM/PM)'),
				'is_numeric' => true,
			),
			'Reason' => array(
				'display_name' => srt_lang('Reason'),
				'is_numeric' => false,
			),
			'RecordType' => array(
				'display_name' => srt_lang('Attendance Status'),
				'is_numeric' => false,
			),
			'Remark' => array(
				'display_name' => srt_lang('Remark'),
				'is_numeric' => false,
			),
		);
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::load_data() for the detail description of this method.
	 */
	function load_data($student_id, $fields, $sort_by_field, $is_ascending, $max_num_of_records, $print_criteria, $filter_criteria, $sort_by_field2, $is_ascending2) {
		// Suppose that StudentReportDataSourceManager::load_data_from_data_source() has validated some of the parameters.
		
		global $eclass_db;
		
		// Transform some fields.
		$srt_lang = 'srt_lang';
		$field_mapping = array(
			'AttendanceDate' => "
				{$this->transform_date_field('AttendanceDate')} AS AttendanceDate
			",
			'DayType' => "
				(CASE DayType
				WHEN 1 THEN '{$srt_lang('WD')}'
				WHEN 2 THEN '{$srt_lang('AM')}'
				WHEN 3 THEN '{$srt_lang('PM')}'
				WHEN 4 THEN '--'
				ELSE DayType END) AS DayType
			",
			'RecordType' => "
				(CASE RecordType
				WHEN 1 THEN '{$srt_lang('Absent')}'
				WHEN 2 THEN '{$srt_lang('LATE')}'
				WHEN 3 THEN '{$srt_lang('Early Leave')}'
				ELSE RecordType END) AS RecordType
			",
		);
		$fields = $this->transform_fields_for_loading_data($fields, $field_mapping);
		
		$conds = $this->transform_print_criteria($print_criteria);
		
//		return $this->returnArray("
//			SELECT " . join(', ', $fields) . "
//			FROM {$eclass_db}.ATTENDANCE_STUDENT
//			WHERE UserID = '{$student_id}'
//			" . ($sort_by_field ? ("ORDER BY $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "") . "
//			" . ($max_num_of_records ? "LIMIT $max_num_of_records" : "") . "
//		");
		// Select the most recent records temporarily, as selecting records by academic year is not supported yet due to the possible schema changes related to academic year in the near future.
		return $this->returnArray("
			SELECT " . join(', ', $fields) . "
			FROM (
				SELECT
					AttendanceDate,
					DayType,
					Reason,
					RecordType,
					Remark
				FROM {$eclass_db}.ATTENDANCE_STUDENT
				WHERE UserID = '{$student_id}' {$conds}
				ORDER BY ModifiedDate DESC, InputDate DESC
				" . ($max_num_of_records ? "LIMIT $max_num_of_records" : "") . "
			) a
			" . ($sort_by_field ? ("ORDER BY $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "") . "
		");
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_total_count() for the detail description of this method.
	 */
	function get_total_count($student_id, $print_criteria) {
		
		global $eclass_db;
		
		$conds = $this->transform_print_criteria($print_criteria);
		
		if(func_num_args() > 2)
		{
		  $attend_type = func_get_arg(2);
      $conds .= " AND RecordType = ".$attend_type;
    }
		
		return current($this->returnVector("
			SELECT count(*) FROM {$eclass_db}.ATTENDANCE_STUDENT
			WHERE UserID = '{$student_id}' {$conds}
		"));
	}
	
	/**
	 * Override method of parent class
	 * @param $print_criteria from load_data().
	 */
	protected function transform_print_criteria($print_criteria) {
	
    $conds = "";
		foreach ($print_criteria as $field_name => &$field_values) {
		  switch($field_name) {
        case "AcademicYearIDs":
          $conds .= empty($field_values) ? " AND 0" : " AND AcademicYearID IN (" . join(', ', $field_values) .")";
          break;
        case "YearTermIDs":
          $conds .= empty($field_values) ? "" : " AND YearTermID IN (" . join(', ', $field_values) .")"; 
          break;
      }
		}
		return $conds;
  }
}

?>
