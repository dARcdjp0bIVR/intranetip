<?php
#Using: 
/*
 * 2013-01-08 [Ivan]
 * 	- added total hours statistics for table section
 */

include_once 'StudentReportTemplate.php';
include_once 'StudentReportDataSourceManager.php';
include_once 'StudentReportGenerator.php';

/**
 * Responsible for outputting PDF, given a report template and a set of students.
 */
class StudentReportHTMLGenerator extends StudentReportGenerator
{
	/**
	 * Output the report in PDF format.
	 * @param $report_template An object of StudentReportTemplate.
	 * @param $student_id_list array(student_id1, student_id2, ...)
	 * @param $print_criteria The options provided by the user through the Print Report page, like:
	 * 		array(
	 * 			'AcademicYearIDs' => array(...),
	 * 			'YearTermIDs' => array(...),
	 * 		)
	 * @param $progress_callback The callback for receiving progress information.
	 *   A float number from 0 to 1 will be passed to the callback as the first parameter.
	 * @return PDF content
	 */
	function generate_report($report_template, $student_id_list, $print_criteria, $progress_callback = null, $progress_percentage=null) {
		global $no_record_msg, $sys_custom, $ipf_cfg;
		// TODO: to be completed.
		
		$data_source_manager = new StudentReportDataSourceManager();
		
//		$PrintingDirection = $print_criteria['PrintingDirection'];
//		$pdf = new StudentReportPDFGenerator_TCPDF($PrintingDirection);
		
		if($report_template->get_header_height() != ''){ 
			$header_height = $report_template->get_header_height();
		}
		else{
			$header_height = PDF_MARGIN_TOP;
		}
		if($report_template->get_footer_height() != ''){ 
			$footer_height = $report_template->get_footer_height();
		}
		else{		
			$footer_height = PDF_MARGIN_FOOTER;
		}		
//		
//		if(!$report_template->is_to_show_footer_on_last_page_only()){
//			$pdf->SetAutoPageBreak(TRUE, $footer_height);
//			$pdf->SetFooterMargin($footer_height);				
//		}
		
		$PrintingDirection = $print_criteria['PrintingDirection'];
		if ($PrintingDirection == $ipf_cfg['dynReport']['printingDirection']['portrait']) {
			$pageWidth = 720;
			$pageHeight = 1100;
		}
		else if ($PrintingDirection == $ipf_cfg['dynReport']['printingDirection']['landscape']) {
			$pageWidth = 1100;
			$pageHeight = 720;
		}
		$contentHeight = $pageHeight - $header_height - $footer_height;
		
		
		$x = '';
		
		if (!$report_template->is_to_show_header_on_first_page_only()) {
			$x .= '<style type="text/css">'."\n";
				$x .= '@media print {'."\n";
				   $x .= 'thead { display: table-header-group; }'."\n";
				$x .= '}'."\n";
			$x .= '</style>'."\n";
		}
		if (!$report_template->is_to_show_footer_on_last_page_only()) {
			$x .= '<style type="text/css">'."\n";
				$x .= '@media print {'."\n";
				   $x .= 'tfoot { display: table-footer-group; }'."\n";
				$x .= '}'."\n";
			$x .= '</style>'."\n";
		}
		
		
		$h = 'htmlspecialchars';
		$nb = 'nl2br';
		$t = 'trim';
		$count = 'count';
		
		
		
		foreach ($student_id_list as $student_id) {		
			
			// Get student info
			$student_info = $data_source_manager->student_info->load_student_info($student_id, array(), $print_criteria, $ipf_cfg['dynReport']['batchPrintMode']['html']);
			
			$theadHtml = '';
			$theadHtml .= '<thead>'."\n";
				$theadHtml .= '<tr><td style="height:'.$header_height.'mm; vertical-align:top;">'.$report_template->assign_values_to_template($report_template->get_header_template(), $student_info).'</td></tr>';
			$theadHtml .= '</thead>'."\n";
			
			$tfootHtml = '';
			$tfootHtml .= '<tfoot>'."\n";
				$tfootHtml .= '<tr><td style="height:'.$footer_height.'mm; vertical-align:bottom;">'.$report_template->assign_values_to_template($report_template->get_footer_template(), $student_info).'</td></tr>';
			$tfootHtml .= '</tfoot>'."\n";
			
			
			$x .= '<div style="width:'.$pageWidth.'px; height:'.$pageHeight.'px; page-break-after:always;">'."\n";
				$x .= '<table style="width:100%; height:100%; vertical-align:top;">'."\n";
					$x .= $theadHtml;
					$x .= $tfootHtml;
					
					// Sections...
					$x .= '<tbody>'."\n";
						$x .= '<tr><td style="vertical-align:top; min-height:'.$contentHeight.'px; max-height:'.$contentHeight.'px;">'."\n";
							foreach ($report_template->get_sections() as $_section_num => $section) {
								if ($section['section_type'] == 'table') {
									$table_section = $section['table'];
									$columns = $table_section['columns'];
									$showStatistics = $table_section['statistics_show'];
									$statisticsInfoAry = $table_section['statistics_info'];
									
									if (count($table_section['data_sources']) > 0 && count($columns) > 0) {
										$records = $data_source_manager->load_data_from_data_sources($student_id, $table_section['data_sources'], $table_section['sort_by_column_number'], $table_section['is_ascending'], $print_criteria, $table_section['filter_criteria']);
										$numOfRecords = count($records);
										
										//if ($sys_custom['iPf']['DynamicReport']['DataSectionApplyConfigFontMapping']) {
											$styleArr = array('section_title_style', 'column_title_style', 'row_text_style');
											for ($i=0, $i_max=count($styleArr); $i<$i_max; $i++) {
												$_style = $styleArr[$i];
												
												$_orgFontFamily = $table_section[$_style]['font_family'];
												$_newFontFamily = $ipf_cfg['dynReport']['fontMappingArr'][$_orgFontFamily];
												
												if ($_newFontFamily != '') {
													$table_section[$_style]['font_family'] = $_newFontFamily;
												}
											}
										//}
										
										if ($print_criteria['ShowNoDataTable'] == 0 && $numOfRecords == 0) {
											// do nth
										}
										else {
											$x .= <<<HTMLEND
												<table class="border_table" width="100%" border="{$table_section['border_size']}" style="{$this->generate_css_style($table_section['section_title_style'])}" cellpadding="2">
												<tr><td style="{$this->generate_css_style($table_section['section_title_style'])}">{$nb($h($table_section['section_title']))}</td></tr>
												</table>
HTMLEND;
							
											// Henry Yuen (2010-06-30): Section description.						
											if($table_section['section_description'] != ''){
												$x .= <<<HTMLEND
													<table class="border_table" width="100%" border="{$table_section['border_size']}" style="{$this->generate_css_style($table_section['section_description_style'])}" cellpadding="2">
													<tr><td style="{$this->generate_css_style($table_section['section_description_style'])}">{$nb($h($t($table_section['section_description'])))}</td></tr>
													</table>
HTMLEND;
											}
											
											// The table.
											// It seems that border is a boolean value only, i.e., either show or hide the border.
											// TCPDF::SetLineWidth() may be used to set border size, but need to convert the unit of the parameter using TCPDF::getHTMLUnitToUnits().
											// It seems that border color does not work.
											$table_html = <<<HTMLEND
												<table class="border_table" width="100%" border="{$table_section['border_size']}" cellpadding="2">
													<thead>
														<tr style="{$this->generate_css_style($table_section['column_title_style'])}">
HTMLEND;
											foreach ($columns as $column) {
												$width_attribute = $column['width'] ? ('width="' . $column['width'] . '"') : '';
												$table_html .= <<<HTMLEND
															<th $width_attribute style="{$this->generate_css_style($table_section['column_title_style'])}">{$h($column['title'])}</th>
HTMLEND;
											}
											$table_html .= "
														</tr>
													</thead>
													<tbody>
											";
											if (count($records) > 0) {
												$_statisticsDataAry = array();
												$_rowCount = 0;
												foreach ($records as $record) {
													$table_html .= <<<HTMLEND
														<tr nobr="true" style="{$this->generate_css_style($table_section['row_text_style'])}">
HTMLEND;
													// As the records may come from several data sources, get the data by numeric index instead of field name.
													for ($i = 0; $i < count($columns); $i++) {
														$width_attribute = $columns[$i]['width'] ? ('width="' . $columns[$i]['width'] . '"') : '';
														$table_html .= <<<HTMLEND
															<td $width_attribute style="{$this->generate_css_style($table_section['row_text_style'])}">{$this->transform_table_cell_data($record[$i], $print_criteria)}</td>
HTMLEND;
														
														$_statisticsDataAry[$i][$_rowCount] = $record[$i];
													}
													$table_html .= "
														</tr>
													";
													
													$_rowCount++;
												}
												
												// statistics row
												$_tableSourceAry = $table_section['data_sources'];
												$_numOfTableSource = count($_tableSourceAry);
												if ($showStatistics) {
													$table_html .= $this->return_statistics_row_html($statisticsInfoAry, $table_section, $_statisticsDataAry);
												}
											}
											else {
												$table_html .= <<<HTMLEND
														<tr nobr="true" align="center" style="{$this->generate_css_style($table_section['row_text_style'])}">
															<td colspan="{$count($columns)}" style="{$this->generate_css_style($table_section['row_text_style'])}">{$print_criteria['NoDataTableContent']}</td>
														</tr>
HTMLEND;
											}
											$table_html .= "
													</tbody>
												</table>
											";
											
											$x .= $table_html;
																
											# Henry Yuen (2010-06-30): debug - Footer not shown if table just finished the current page 
											# details: tcpdf will delete the last page automatically if last page contains only <thead> tag
											$x .= '<br />';
										}
																					
									}
								}
								else if ($section['section_type'] == 'free_text') {
									$x .= '<table width="100%" cellpadding="2" border="0">';
										$x .= '<tr><td>'."\n";
											$x .= $report_template->assign_values_to_template($section['free_text']['text_template'], $student_info);
										$x .= '</td></tr>'."\n";
									$x .= '</table>'."\n";
									$x .= '<br />';
								}
								else if ($section['section_type'] == 'page_break') {									
									$x .= '</td></tr>'."\n";
									$x .= '</tbody>'."\n";
									$x .= '</table>'."\n";
									$x .= '</div>'."\n";
									
									$x .= '<div style="width:'.$pageWidth.'px; height:'.$pageHeight.'px; page-break-after:always;">'."\n";
										$x .= '<table style="width:100%; height:100%; vertical-align:top;">'."\n";
											$x .= $theadHtml;
											$x .= $tfootHtml;
												$x .= '<tbody>'."\n";
													$x .= '<tr><td style="vertical-align:top; min-height:'.$contentHeight.'px; max-height:'.$contentHeight.'px;">'."\n";
								}
								else {
									throw new Exception("The section type '{$section['section_type']}' is not supported.");
								}
							}
						$x .= '</td></tr>'."\n";
					$x .= '</tbody>'."\n";
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
		}
				
		return $x;
	}
}
?>
