<?php

include_once 'StudentReportDataSource_OLE.php';

/**
 * The data source for accessing data from Performance / Awards and Key Participation Outside School.
 * This data source is supposed to be managed by StudentReportDataSourceManager.
 */
class StudentReportDataSource_PAKPOutsideSchool extends StudentReportDataSource_OLE
{
	/**
	 * Get the display name of the data source.
	 */
	function get_display_name() {
		return srt_lang('Performance / Awards and Key Participation Outside School');
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_field_information() for the detail description of this method.
	 */
	function get_field_information() {
		$field_information = parent::get_field_information();
		
		// No hours information for this data source.
		unset($field_information['Hours']);
		
		// No ELE for this data source.
		unset($field_information['ELE']);
		
		//For extra field sample
		/*$field_information['DBFIELDNAME'] = array(
				'display_name' => srt_lang('Other Info'),
				'is_numeric' => false,
			);
		*/
		return $field_information;
	}
	
	/**
	 * See StudentReportDataSource_OLE::get_field_information() for the detail description of this method.
	 */
	protected function int_or_ext() {
		return 'EXT';
	}
}

?>
