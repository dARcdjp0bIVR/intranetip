<?php
#using: 

# modifications log
# Philips (2020-04-06) [V183010]: Modified load_student_info(), use personal settings instead of PORTFOLIO_STUDENT
# Anna (2018-11-30) [P153733]: Added $sys_custom['iPf']['DynamicReport']['NotShowSelfAccountTitle']
# Anna (2018-07-06) [Z142182] : Modified load_student_info()- added $sys_custom['iPf']['DynamicReport']['custShowDOBformat']
# Ivan (2018-04-13) [J137709] [ip.2.5.9.5.1]: modified load_student_info() to support $sys_custom['iPf']['DynamicReport']['StudentPhotoWidth'] and $sys_custom['iPf']['DynamicReport']['StudentPhotoHeight']
# Ivan (2016-07-06) [F98300] [ip.2.5.7.7.1]: modified get_field_information() and load_student_info(), added academic result from lower form options
# Ivan (2016-06-22) [M96941] [ip.2.5.7.7.1]: modified get_field_information() and load_student_info(), added class teahcer options
# Omas (2015-11-04): modified get_field_information() and load_student_info(), added class teacher name fields
# Omas (2015-11-04): modified load_student_info(), add teacher comments & conduct
# Henry (2012-07-10) : modified load_student_info(), replace space by "&nbsp;", for indentation [CRM : 2012-0524-1236-06066] 
# Ivan (2012-02-17): modified load_student_info() to add show / hide full mark display logic
# Ivan (2012-02-02): [2012-0202-1641-03066] improved: "Empty Data Symbol" settings is applicable to auto-full-in data now.
# Ivan (2012-01-06): [2012-0106-1754-33096] fixed failed to fill in "auto fill in" data due to HKID field are in both INTRANET_USER and PORTFOLIO_STUDENT
# Ivan (2011-07-11): [2011-0708-1515-03073] fixed cannot display Self-Account if the content has 全形 space
# Ivan (2011-04-11): added flag $sys_custom['iPf']['DynamicReport']['OLECategoryHour'] to control the display of OLE Category Hour selection
# Ivan (2011-02-15): add OLE Category Hours display
# Henry Yuen (2010-06-30): disable HKID if $special_feature['ava_hkid'] is off
# Henry Yuen (2010-06-21): add admission date


include_once 'StudentReportClassLoader.php';

/**
 * The data source for accessing the data for a student.
 * This data source differs from other StudentReportDataSource_* in that it is not supposed to be used for table sections.
 */
class StudentReportDataSource_StudentInfo extends libdb
{
  const group_StudentInfo = 'Student Info';
  const group_TotalCount = 'Total Count';
  const group_TotalHours = 'Total Hours';
  
  const count_Suffix = '_Count';
  const count_TypeSeperator = 'SePsEp';
  
  const total_Suffix = '_Total';
  const category_Suffix = '_CategoryTotal';
  
  // use merit/demerit prefix due to display limitation of "-" on merit type 
  const merit_meritPrefix = 'm';
  const merit_demeritPrefix = 'd';
  
  const max_space = 2; // max number of nbsp; shown in self account text for Dynamic Report dispaly 
  
  protected $class_loader;
  
  function __construct() {
    parent::libdb();
  
  	$this->class_loader = new StudentReportPublicClassLoader();
    $this->class_loader->member_to_class_map = array(
  		'OLE'.self::count_Suffix => 'StudentReportDataSource_OLE',
  		'OLEHours'.self::total_Suffix => 'StudentReportDataSource_OLE',
  		'OLECategoryHours'.self::category_Suffix => 'StudentReportDataSource_OLE',
  		'PAKP Outside School'.self::count_Suffix => 'StudentReportDataSource_PAKPOutsideSchool',
  		'Activity'.self::count_Suffix => 'StudentReportDataSource_Activity',
  		//'Attendance Record'.self::count_Suffix => 'StudentReportDataSource_AttendanceRecord',
  		'Attendance Record'.self::count_TypeSeperator.'1'.self::count_Suffix => 'StudentReportDataSource_AttendanceRecord',
  		'Attendance Record'.self::count_TypeSeperator.'2'.self::count_Suffix => 'StudentReportDataSource_AttendanceRecord',
  		'Attendance Record'.self::count_TypeSeperator.'3'.self::count_Suffix => 'StudentReportDataSource_AttendanceRecord',
  		'Award'.self::count_Suffix => 'StudentReportDataSource_Award',
  		'Merit'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'1'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'2'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'3'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'4'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'5'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'1'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'2'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'3'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'4'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'5'.self::count_Suffix => 'StudentReportDataSource_Merit',
  		'Service'.self::count_Suffix => 'StudentReportDataSource_Service',
  	);
  }

	/**
	 * Get the information of the fields in the data source.
	 * @return An array of field information:
	 * array(
	 * 		field_name1 => array(
	 * 			'display_name' => field_display_name1,	// The name to be shown in the dropdown list.
	 * 			...
	 * 		),
	 * 		field_name2 => array(
	 * 			...
	 * 		),
	 * 		...
	 * )
	 */
	function get_field_information() {
	    global $i_Merit_Merit, $i_Merit_MinorCredit, $i_Merit_MajorCredit, $i_Merit_SuperCredit, $i_Merit_UltraCredit;
	    global $i_Merit_BlackMark, $i_Merit_MinorDemerit, $i_Merit_MajorDemerit, $i_Merit_SuperDemerit, $i_Merit_UltraDemerit;
	    global $ec_iPortfolio;
	    global $special_feature;
	    global $sys_custom;
	    global $Lang;
	
		$fieldArray = array(
			'UserLogin' => array(
				'display_name' => srt_lang('Login ID'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'UserEmail' => array(
				'display_name' => srt_lang('Email'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'EnglishName' => array(
				'display_name' => srt_lang('English Name'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'ChineseName' => array(
				'display_name' => srt_lang('Chinese Name'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'NickName' => array(
				'display_name' => srt_lang('Nick Name'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'Gender' => array(
				'display_name' => srt_lang('Gender'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'Gender_en' => array(
				'display_name' => srt_lang('Gender_en'),
				'group' => srt_lang(self::group_StudentInfo),
			),

			'Gender_big5' => array(
				'display_name' => srt_lang('Gender_big5'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'DateOfBirth' => array(
				'display_name' => srt_lang('DOB'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'HomeTelNo' => array(
				'display_name' => srt_lang('Home Tel'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'Address' => array(
				'display_name' => srt_lang('Address'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'Country' => array(
				'display_name' => srt_lang('Country'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'Info' => array(
				'display_name' => srt_lang('Message'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'Remark' => array(
				'display_name' => srt_lang('Remark'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'ClassNumber' => array(
				'display_name' => srt_lang('Class No.'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'ClassName' => array(
				'display_name' => srt_lang('Class Name'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'ClassName_Big5' => array(
				'display_name' => srt_lang('Class_Name_big5'),
				'group' => srt_lang(self::group_StudentInfo),
			),

			'CardID' => array(
				'display_name' => srt_lang('Card ID'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'WebSAMSRegNo' => array(
				'display_name' => srt_lang('WebSAMS Reg. No.'),
				'group' => srt_lang(self::group_StudentInfo),
			),			
			'HKID' => array(
				'display_name' => srt_lang('HKID'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'STRN' => array(
				'display_name' => srt_lang('STRN'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'PhotoLink' => array(
				'display_name' => srt_lang('Student Photo'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'SelfAccount' => array(
				'display_name' => srt_lang('Self Account'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'Conduct' => array(
				'display_name' => srt_lang('Conduct'),
				'group' => srt_lang(self::group_StudentInfo),
			),			
			'TeacherCommentEng' => array(
				'display_name' => srt_lang('TeacherCommentEng'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'TeacherCommentChi' => array(
				'display_name' => srt_lang('TeacherCommentChi'),
				'group' => srt_lang(self::group_StudentInfo),
			),			
			'AdmissionDate' => array(
				'display_name' => srt_lang('Admission Date'),
				'group' => srt_lang(self::group_StudentInfo),
			),
//			'AcademicResult_Mark' => array(
//				'display_name' => $Lang['iPortfolio']['AcademicResult_Mark'],
//				'group' => srt_lang(self::group_StudentInfo),
//			),
//			'AcademicResult_Grade' => array(
//				'display_name' => $Lang['iPortfolio']['AcademicResult_Grade'],
//				'group' => srt_lang(self::group_StudentInfo),
//			),
			
			'AcademicResult_MarkThenGrade_LowerForm' => array(
				'display_name' => $Lang['iPortfolio']['AcademicResult_MarkThenGrade'].' ['.$Lang['iPortfolio']['LowerForm'].']',
				'group' => srt_lang(self::group_StudentInfo),
			),
			'AcademicResult_GradeThenMark_LowerForm' => array(
				'display_name' => $Lang['iPortfolio']['AcademicResult_GradeThenMark'].' ['.$Lang['iPortfolio']['LowerForm'].']',
				'group' => srt_lang(self::group_StudentInfo),
			),
			'AcademicResult_MarkThenGrade' => array(
				'display_name' => $Lang['iPortfolio']['AcademicResult_MarkThenGrade'].' ['.$Lang['iPortfolio']['HigherForm'].']',
				'group' => srt_lang(self::group_StudentInfo),
			),
			'AcademicResult_GradeThenMark' => array(
				'display_name' => $Lang['iPortfolio']['AcademicResult_GradeThenMark'].' ['.$Lang['iPortfolio']['HigherForm'].']',
				'group' => srt_lang(self::group_StudentInfo),
			),
			
			// Count total, key name matches StudentReportDataSourceBase_TableSection
			'OLE'.self::count_Suffix => array(
				'display_name' => srt_lang('OLE'),
				'group' => srt_lang(self::group_TotalCount),
			),
			'PAKP_Outside_School'.self::count_Suffix => array(
				'display_name' => srt_lang('Performance / Awards and Key Participation Outside School'),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Activity'.self::count_Suffix => array(
				'display_name' => srt_lang('Activity'),
				'group' => srt_lang(self::group_TotalCount),
			),
/*
			'Attendance_Record'.self::count_Suffix => array(
				'display_name' => srt_lang('Attendance Record'),
				'group' => srt_lang(self::group_TotalCount),
			),
*/
			'Attendance_Record'.self::count_TypeSeperator.'1'.self::count_Suffix => array(
				'display_name' => srt_lang($ec_iPortfolio['total_absent_count']),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Attendance_Record'.self::count_TypeSeperator.'2'.self::count_Suffix => array(
				'display_name' => srt_lang($ec_iPortfolio['total_late_count']),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Attendance_Record'.self::count_TypeSeperator.'3'.self::count_Suffix => array(
				'display_name' => srt_lang($ec_iPortfolio['total_earlyleave_count']),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Award'.self::count_Suffix => array(
				'display_name' => srt_lang('Award'),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Service'.self::count_Suffix => array(
				'display_name' => srt_lang('Service'),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'1'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_Merit),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'2'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_MinorCredit),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'3'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_MajorCredit),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'4'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_SuperCredit),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_meritPrefix.'5'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_UltraCredit),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'1'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_BlackMark),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'2'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_MinorDemerit),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'3'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_MajorDemerit),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'4'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_SuperDemerit),
				'group' => srt_lang(self::group_TotalCount),
			),
			'Merit'.self::count_TypeSeperator.self::merit_demeritPrefix.'5'.self::count_Suffix => array(
				'display_name' => srt_lang($i_Merit_UltraDemerit),
				'group' => srt_lang(self::group_TotalCount),
			),
			'OLEHours'.self::total_Suffix => array(
				'display_name' => srt_lang('OLE Total Hours'),
				'group' => srt_lang(self::group_TotalHours),
			),
			'OLECategoryHours'.self::category_Suffix => array(
				'display_name' => srt_lang('OLE Category Hours'),
				'group' => srt_lang(self::group_TotalHours),
			),	
			'IssueDate' => array(
				'display_name' => srt_lang('IssueDate'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'ClassTeacherEng' => array(
				'display_name' => srt_lang('Class Teacher (Eng)'),
				'group' => srt_lang(self::group_StudentInfo),
			),
			'ClassTeacherChi' => array(
				'display_name' => srt_lang('Class Teacher (Chi)'),
				'group' => srt_lang(self::group_StudentInfo),
			)
		);
				
		if(!$special_feature['ava_hkid']) {
			unset($fieldArray['HKID']);
		}
		
		if (!$sys_custom['iPf']['DynamicReport']['OLECategoryHour']) {
			unset($fieldArray['OLECategoryHours'.self::category_Suffix]);
		}
			
		
		return $fieldArray;
	}
	
	/**
	 * Get the list of student fields available for use in template.
	 */
	function get_fields($field_group=self::group_StudentInfo) {
		//return array_keys($this->get_field_information());
		
		$_field_array = $this->get_field_information();
		$result = array();
		foreach($_field_array as $field_name => $information) {
      if($information['group'] == $field_group) $result[] = $field_name;
    }
    return $result;
	}
	
	/**
	 * Get the map from the field name to its display name.
	 * @return array(
	 * 		field_name1 => field_display_name1,
	 * 		field_name2 => field_display_name2,
	 * 		...
	 * )
	 */
	function get_field_display_names() {
		$result = array();
		foreach ($this->get_field_information() as $field_name => $information) {
			$result[$field_name] = $information['display_name'];
		}
		return $result;
	}
	
	/**
	 * Get the map from the field name to its display name.
	 * @return array(
	 * 		group_name1 => array(field_name1, field_name2, ...)
	 * 		group_name2 => array(field_name3, field_name4, ...)
	 * 		...
	 * )
	 */
	function get_field_groups() {
		$result = array();
		foreach ($this->get_field_information() as $field_name => $information) {
			$result[$information['group']][] = $field_name;
		}
		return $result;
	}
	
	/**
	 * Get the map from data source to the list of fields provided by the data source for use in template.
	 * @return array (
	 * 		'Data Source 1' => array(Field11, Field12, ...),
	 * 		'Data Source 2' => array(Field21, Field22, ...),
	 * 		...
	 * )
	 */
	function get_data_source_to_fields_map() {
		$result = array();
		foreach ($this->class_loader->member_to_class_map as $data_source_member => $class) {
			if ($this->class_loader->$data_source_member instanceof StudentReportDataSourceBase_TableSection) {
				$result[$data_source_member] = $this->class_loader->$data_source_member->get_fields();
			}
		}
		return $result;
	}
	
	/**
	 * Load some student information for a student.
	 * @param $student_id A student ID. Retrieve the data for the student specified.
	 * @param $fields an array of field names. Used to specify which fields to retrieve. Get all fields if not specified.
	 * @return array('field1' => value1, 'field2' => value2, ...).
	 */
	function load_student_info($student_id, $fields, $print_criteria=array(), $output_format) {
		global $sys_custom,$ipf_cfg;
		global $intranet_session_language,$ec_iPortfolio;
		
		if (!$student_id) throw new Exception("The parameter student_id should be given.");
		if (count($unknown_fields = array_diff($fields, $this->get_fields())) > 0) throw new Exception("The following field(s) in the parameter fields are unknown: " . join(', ', $unknown_fields) . ".");
		
		if (count($fields) == 0) {
			$fields = $this->get_fields();
		}
		
		global $intranet_db, $intranet_root, $eclass_db;
		global $lpf_slp, $ipf_cfg;
		
		# Henry Yuen (2010-06-21): add admission date		
		$fieldCount = count($fields);

		//A FLAG to special handle display Chinese Class Name
		$requestDisplayChineseClassName = false;

		for($i = 0; $i < $fieldCount; $i++){
			if($fields[$i] == 'WebSAMSRegNo'){
				$fields[$i] = 'TRIM(LEADING \'#\' FROM I.WebSAMSRegNo) AS WebSAMSRegNo';
			}
			elseif($fields[$i] == 'SelfAccount'){
			    if($sys_custom['iPf']['DynamicReport']['NotShowSelfAccountTitle']){
			        $fields[$i] = 'S.Details AS SelfAccount';	        
			    }else{
			        $fields[$i] = 'if((trim(S.Title) = \'\' or (S.Title is null)),S.Details,CONCAT(\'<p><u>\', S.Title, \'</u></p>\', S.Details)) AS SelfAccount';
			        
			    }
				//DON"T display the title if title is empty
		//		$fields[$i] = 'CONCAT(\'aa <p><u>\', S.Title, \'</u></p>bb\', S.Details) AS SelfAccount';
			}
			elseif($fields[$i] == 'DateOfBirth'){
			    if($sys_custom['iPf']['DynamicReport']['custShowDOBformat']){
			    $fields[$i] = 'DATE_FORMAT(DateOfBirth,"%d / %m / %Y")  as DateOfBirth';			    
			 }else{
			     $fields[$i] = 'DATE(DateOfBirth) AS DateOfBirth';
			 }		
			}
			elseif($fields[$i] == 'AdmissionDate'){
			    if($sys_custom['iPf']['DynamicReport']['custShowDOBformat']){
			        $fields[$i] = 'DATE_FORMAT(PS.AdmissionDate,"%d / %m / %Y")  as AdmissionDate';
			    }else{
			        $fields[$i] = 'DATE(PS.AdmissionDate) AS AdmissionDate';
			    }	
				//$fields[$i] = 'DATE(PS.AdmissionDate) AS AdmissionDate';
			}
			elseif($fields[$i] == 'Gender'){
				$fields[$i] = 'if(UPPER(Gender) = \'M\',\''.srt_lang('Gender_M').'\',if(UPPER(Gender)=\'F\',\''.srt_lang('Gender_F').'\',\'\')) As Gender';
			}
			elseif($fields[$i] == 'Gender_big5'){
				$fields[$i] = 'if(UPPER(Gender) = \'M\',\''.srt_lang('Gender_M_Big5').'\',if(UPPER(Gender)=\'F\',\''.srt_lang('Gender_F_Big5').'\',\'\')) As Gender_big5';
			}
			elseif($fields[$i] == 'Gender_en'){
				$fields[$i] = 'if(UPPER(Gender) = \'M\',\''.srt_lang('Gender_M_En').'\',if(UPPER(Gender)=\'F\',\''.srt_lang('Gender_F_En').'\',\'\')) As Gender_en';
			}
			elseif($fields[$i] == 'IssueDate'){
				$_issueDate = (trim($print_criteria['IssueDate']) == '') ? date("Y-m-d") : trim($print_criteria['IssueDate']);
				$fields[$i] = '\''.$_issueDate.'\' as IssueDate';
			}
			elseif($fields[$i] == 'HKID'){
				$fields[$i] = 'I.HKID As HKID';
			}
			elseif($fields[$i] == 'STRN'){
				$fields[$i] = 'I.STRN As STRN';
			}
			elseif($fields[$i] == 'ClassName_Big5'){
				$fields[$i] = ' yc.ClassTitleB5 As ClassName_Big5 ';
				$requestDisplayChineseClassName = true;
			}
			elseif($fields[$i] == 'AcademicResult_Mark' || $fields[$i] == 'AcademicResult_Grade' || $fields[$i] == 'AcademicResult_MarkThenGrade' || $fields[$i] == 'AcademicResult_GradeThenMark' || $fields[$i] == 'AcademicResult_GradeThenMark_LowerForm' || $fields[$i] == 'AcademicResult_MarkThenGrade_LowerForm'){
				unset($fields[$i]);		// Academic Result retrieved from other function
			}
			elseif($fields[$i] == 'Conduct' || $fields[$i] == 'TeacherCommentChi' || $fields[$i] == 'TeacherCommentEng' || $fields[$i] == 'ClassTeacherChi' || $fields[$i] == 'ClassTeacherEng'){
				unset($fields[$i]);
			}
		}		
		
//		$currentYearID = Get_Current_Academic_Year_ID();
		$chiClassNameSQL = '';
		if($requestDisplayChineseClassName){
			$chiClassNameSQL = ' LEFT JOIN '.$intranet_db.'.YEAR_CLASS as yc on yc.ClassTitleEN = I.classname and yc.AcademicYearID = '.Get_Current_Academic_Year_ID().' ';
		}

		# Henry Yuen (2010-05-17): modify the select statement to include eclassDB.SELF_ACCOUNT_STUDENT				
		# Henry Yuen (2010-06-21): modify the select statement to include eclassDB.PORTFOLIO_STUDENT
		$records = $this->returnArray("
			SELECT " . join(', ', $fields) . "
			FROM {$intranet_db}.INTRANET_USER I
				LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT S ON I.UserID = S.UserID AND INSTR(S.DefaultSA, 'SLP')
				LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS PS ON I.UserID = PS.UserID
				{$chiClassNameSQL} 
			WHERE I.UserID = '{$student_id}'
		");
// 				$records = $this->returnArray("
// 			SELECT " . join(', ', $fields) . "
// 						FROM {$intranet_db}.INTRANET_USER I
// 						LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT S ON I.UserID = S.UserID AND INSTR(S.DefaultSA, 'SLP')
// 						LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT PS ON I.UserID = PS.UserID
// 						{$chiClassNameSQL}
// 						WHERE I.UserID = '{$student_id}'
// 						");
		//$records = $this->returnArray("
		//	SELECT " . join(', ', $fields) . "
		//	FROM {$intranet_db}.INTRANET_USER I
		//		LEFT JOIN {$eclass_db}.SELF_ACCOUNT_STUDENT S ON I.UserID = S.UserID AND INSTR(S.DefaultSA, 'SLP')
		//	WHERE I.UserID = '{$student_id}'
		//");		
				
		//$records = $this->returnArray("
		//	SELECT " . join(', ', $fields) . "
		//	FROM {$intranet_db}.INTRANET_USER
		//	WHERE UserID = '{$student_id}'
		//");				
		$result = $records[0];
				
		# Henry Yuen (2010-05-13): support adding student photo		
		$DefaultLink = "/images/2009a/iPortfolio/no_photo.jpg";
		$OriginalWidth = isset($sys_custom['iPf']['DynamicReport']['StudentPhotoWidth'])? $sys_custom['iPf']['DynamicReport']['StudentPhotoWidth'] : 100;
		$OriginalHeight = isset($sys_custom['iPf']['DynamicReport']['StudentPhotoHeight'])? $sys_custom['iPf']['DynamicReport']['StudentPhotoHeight'] : 130;
		$DefaultRatio = (isset($sys_custom['iPf']['DynamicReport']['StudentPhotoRatio']))? $sys_custom['iPf']['DynamicReport']['StudentPhotoRatio'] : 0.5;		
		$DefaultWidth = (int)($OriginalWidth * $DefaultRatio). "px";
		$DefaultHeight = (int)($OriginalHeight * $DefaultRatio). "px";									

		include_once($intranet_root."/includes/portfolio25/libpf-account-student.php");
		$lib_acc_stu = new libpf_account_student(); 
		$lib_acc_stu->SET_CLASS_VARIABLE("user_id", $student_id);
		$photolink = $lib_acc_stu->GET_IPORTFOLIO_PHOTO();
					
		$result['PhotoLink'] = '<img src="'. $photolink. '" width="'.$DefaultWidth.'" height="'.$DefaultHeight.'"/>';		
		
		// Eric Yip (20100706): replace full-width space of Chinese name
		// [CRM Ref No.: 2010-0705-1154]
		$result['ChineseName'] = str_replace('　', '', $result['ChineseName']);
		
		// Eric Yip (20100625): Remove tags of self account
		$result['SelfAccount'] = strip_tags($result['SelfAccount'], "<p><u><br>");
		
		# Henry Yuen (2011-01-28): remove p tab attributes and reduce number of &nbsp;
		$spacePattern = '';
		$spacePatternNew = '';		
		for($i = 1; $i < self::max_space; $i++){
			$spacePattern .= "&nbsp;\s*";
			$spacePatternNew .= "&nbsp;";
		}
		$spacePattern .= "(&nbsp;\s*)+";

//		error_log("------------> selfAccount [".$result['SelfAccount']."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");		$spacePatternNew .= "&nbsp;";
		
		$result['SelfAccount'] = preg_replace('/<p.*?>/', '<p>', $result['SelfAccount']);
		//$result['SelfAccount'] = preg_replace('/'. $spacePattern. '/', $spacePatternNew, $result['SelfAccount']);		
		//$result['SelfAccount'] = preg_replace('/&nbsp;\s*(&nbsp;\s*)+/', '&nbsp;&nbsp;', $result['SelfAccount']);							
		
		# Henry 2011-02-22: &nbsp; genereated by tcpdf engine will occupy unusual wide spacing, so explicitly adjust it by setting css font-size style.
		#					NOTE: Don't know why setting 100% works, but can adjust this number to control the space width    
//		$result['SelfAccount'] = str_replace('&nbsp;', "<span style=\"font-size:100%\">&nbsp;</span>", $result['SelfAccount']);
//		$result['SelfAccount'] = str_replace('　', "<span style=\"font-size:100%\">&nbsp;</span>", $result['SelfAccount']);
		$result['SelfAccount'] = str_replace("<br />", "<br/>", $result['SelfAccount']);

		// added by Henry Chow on 20120610 (Indentation of text)
		//$result['SelfAccount'] = str_replace(" ", "&nbsp;", $result['SelfAccount']);

		//$result['SelfAccount'] = str_replace("  ", "<span style=\"font-size:100%\">&nbsp;</span>", $result['SelfAccount']);

/*indentation moved to libportfolio as a function, spaceIndentationForPDF()
//indetation

			//$ar  temp variable for conversion
			preg_match_all('/./us', $result['SelfAccount'], $ar);
			$result['SelfAccount'] = join('',array_reverse($ar[0]));

			//replace four space to one "full chinese character space"
			$result['SelfAccount'] = str_replace('    ', "　", $result['SelfAccount']);
			preg_match_all('/./us', $result['SelfAccount'], $ar);
			$result['SelfAccount'] = join('',array_reverse($ar[0]));

			//replace three space to 75% width
			$result['SelfAccount'] = str_replace('   ', "<span style=\"font-size:75%\">　</span>", $result['SelfAccount']);

			//replace two space to 50% width
			$result['SelfAccount'] = str_replace('  ', "<span style=\"font-size:50%\">　</span>", $result['SelfAccount']);
//
*/
		$lpfObj = new libportfolio();
		$result['SelfAccount'] = $lpfObj->spaceIndentationForPDF($result['SelfAccount']);
		
		
//		error_log("selfAccount [".$result['SelfAccount']."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		//$result['ChineseName'] = trim($result['ChineseName']);		
		
		
		### Academic Result
		$lpf_slp = new libpf_slp();					// /includes/libpf-slp.php <-- no use here but is globalled in function GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID
		$libpf_report = new slp_report();			// /home/portfolio/profile/report/report_lib/slp_report.php
		$libpf_academic = new libpf_academic();		// /includes/portfolio25/libpf-academic.php
		
		$libpf_report->setPrintRequsetFrom($ipf_cfg['slpAcademicResultPrintRequestFrom']['DynamicReport']);
		$libpf_report->setReportStudentId($student_id);

		$headerRequestLang = $sys_custom['iPf']['DynamicReport']['AcademicReportSectionDisplayLang'];
		if(strtolower($headerRequestLang) == strtolower('b5') || strtolower($headerRequestLang) == strtolower('en') ){
			$libpf_report->setReportLang($headerRequestLang);  //<-- it is work , but enhance later by more request 20120228 [Fai]
		}

		$includedFormWebSamsCodeArr = '';
		if(isset($sys_custom['iPf']['DynamicReport']['AcademicReportSection']['DisplayFormWebSamsCodeArr'])){
		    $includedFormWebSamsCodeArr = $sys_custom['iPf']['DynamicReport']['AcademicReportSection']['DisplayFormWebSamsCodeArr'];
		}
		$ignoreAssessment = '';
		if(	$sys_custom['iPf']['DynamicReport']['AcedemicPerformanceIgnoreTermAssessment']){
		    $ignoreAssessment = false;
	    }

		// function GET_ACADEMIC_PERFORMANCE_SLP_IN_ID is located in "/includes/libpf-report.php"
	    list($SubjArr, $YearArr, $APArr, $SubjectInfoOrderedArr) = $libpf_report->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($student_id, false, $includedFormWebSamsCodeArr,$ignoreAssessment);
		list($SubjArr_lowerForm, $YearArr_lowerForm, $APArr_lowerForm, $SubjectInfoOrderedArr_lowerForm) = $libpf_report->GET_ACADEMIC_PERFORMANCE_SLP_IN_ID($student_id, $includeComponentSubject=false, array('S1', 'S2', 'S3', 'F1', 'F2', 'F3'));
		$SubjectFullMarkArr = $libpf_academic->Get_Subject_Full_Mark();
		$DisplayFullMark = $print_criteria['DisplayFullMark'];
		$EmptyDataSymbol = $print_criteria['EmptyDataSymbol'];
		
		// function GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID is located in "/includes/libpf-report.php"
//		$result['AcademicResult_Mark'] = $libpf_report->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID($SubjArr, $YearArr, $APArr[$student_id], $SubjectInfoOrderedArr, $SubjectFullMarkArr, $ipf_cfg['slpAcademicResultPrintMode']['mark'], $DisplayFullMark, $EmptyDataSymbol);
//		$result['AcademicResult_Grade'] = $libpf_report->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID($SubjArr, $YearArr, $APArr[$student_id], $SubjectInfoOrderedArr, $SubjectFullMarkArr, $ipf_cfg['slpAcademicResultPrintMode']['grade'], $DisplayFullMark, $EmptyDataSymbol);
		
		if ($output_format == $ipf_cfg['dynReport']['batchPrintMode']['pdf']) {
			$result['AcademicResult_MarkThenGrade'] = $libpf_report->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID($SubjArr, $YearArr, $APArr[$student_id], $SubjectInfoOrderedArr, $SubjectFullMarkArr, $ipf_cfg['slpAcademicResultPrintMode']['markThenGrade'], $DisplayFullMark, $EmptyDataSymbol);
			$result['AcademicResult_GradeThenMark'] = $libpf_report->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID($SubjArr, $YearArr, $APArr[$student_id], $SubjectInfoOrderedArr, $SubjectFullMarkArr, $ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark'], $DisplayFullMark, $EmptyDataSymbol);
			
			$result['AcademicResult_MarkThenGrade_LowerForm'] = $libpf_report->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID($SubjArr_lowerForm, $YearArr_lowerForm, $APArr_lowerForm[$student_id], $SubjectInfoOrderedArr_lowerForm, $SubjectFullMarkArr, $ipf_cfg['slpAcademicResultPrintMode']['markThenGrade'], $DisplayFullMark, $EmptyDataSymbol, $ParDisplayComponentSubject=false, $parLowerForm=true);
			$result['AcademicResult_GradeThenMark_LowerForm'] = $libpf_report->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_PDF_IN_ID($SubjArr_lowerForm, $YearArr_lowerForm, $APArr_lowerForm[$student_id], $SubjectInfoOrderedArr_lowerForm, $SubjectFullMarkArr, $ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark'], $DisplayFullMark, $EmptyDataSymbol, $ParDisplayComponentSubject=false, $parLowerForm=true);
		}
		else if ($output_format == $ipf_cfg['dynReport']['batchPrintMode']['html']) {
			$result['AcademicResult_MarkThenGrade'] = $libpf_report->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID($SubjArr, $YearArr, $APArr[$student_id], $SubjectInfoOrderedArr, $SubjectFullMarkArr, $ipf_cfg['slpAcademicResultPrintMode']['markThenGrade'], $DisplayFullMark, $EmptyDataSymbol);
			$result['AcademicResult_GradeThenMark'] = $libpf_report->GEN_ACADEMIC_PERFORMANCE_TABLE_SLP_IN_ID($SubjArr, $YearArr, $APArr[$student_id], $SubjectInfoOrderedArr, $SubjectFullMarkArr, $ipf_cfg['slpAcademicResultPrintMode']['gradeThenMark'], $DisplayFullMark, $EmptyDataSymbol);
		}
		
		// 2015-11-04 Omas Teacher comments
		$student_comment_arr = $lpf_slp->GET_STUDENT_COMMENT($student_id,$print_criteria['commentsAY'],$print_criteria['commentsSem']);
		$CommentEng = $student_comment_arr[0]['CommentEng'];
		$CommentChi = $student_comment_arr[0]['CommentChi'];
		$Conduct = $student_comment_arr[0]['Conduct'];
		$result['Conduct'] = $Conduct == ''? '':$Conduct ;
		$result['TeacherCommentChi'] = $CommentChi == ''? '':$CommentChi ;
		$result['TeacherCommentEng'] = $CommentEng == ''? '':$CommentEng ;
		
		// Class Teacher
		$sql = "Select
						iu.EnglishName, iu.ChineseName
				From
						YEAR_CLASS_USER as ycu
						Inner Join YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID)
						Inner Join YEAR_CLASS_TEACHER as yct ON (yct.YearClassID = yc.YearClassID) 
						Inner Join INTRANET_USER as iu ON (yct.UserID = iu.UserID)
				Where
						ycu.UserID = '".$student_id."'
						And yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
				";
		$classTeacherAry = $this->returnResultSet($sql);
		$classTeacherNameEng = implode(', ', Get_Array_By_Key($classTeacherAry, 'EnglishName'));
		$classTeacherNameChi = implode(', ', Get_Array_By_Key($classTeacherAry, 'ChineseName'));
		$result['ClassTeacherEng'] = $classTeacherNameEng == ''? '' : $classTeacherNameEng ;
		$result['ClassTeacherChi'] = $classTeacherNameChi == ''? '' : $classTeacherNameChi ;
		
		
    /*
    * Add record count of modules to result set
    */
		foreach ($this->get_data_source_to_fields_map() as $data_source => $fields) {
		  if(strpos($data_source, self::count_TypeSeperator) === false) {
		    if(strpos($data_source, self::count_Suffix) !== false)
		    {
    		  // Replace "_" with space in key values to match with StudentReportDataSource_StudentInfo 
    			$result[str_replace(" ", "_", $data_source)] = $this->class_loader->$data_source->get_total_count($student_id, $print_criteria);
    		}
    		else if(strpos($data_source, self::total_Suffix) !== false)
		    {
    		  // Replace "_" with space in key values to match with StudentReportDataSource_StudentInfo 
    			$result[str_replace(" ", "_", $data_source)] = $this->class_loader->$data_source->get_total_sum($student_id, $print_criteria);
    		}
    		else if (strpos($data_source, self::category_Suffix) !== false)
    		{
    			// Replace "_" with space in key values to match with StudentReportDataSource_StudentInfo 
    			$result[str_replace(" ", "_", $data_source)] = $this->class_loader->$data_source->get_category_sum($student_id, $print_criteria);
    		}
	      }
	      else {
			    // list($data_source, $count_type) = explode(self::count_TypeSeperator, str_replace(self::count_Suffix, "", $data_source));
	        preg_match_all('/(.*)'.self::count_TypeSeperator.'(.*)'.self::count_Suffix.'/U', $data_source, $reg_result);
	        $data_type = $reg_result[1][0];
	        $count_type =  $reg_result[2][0];
	
	  		  // Replace "_" with space in key values to match with StudentReportDataSource_StudentInfo 
	  			$result[str_replace(" ", "_", $data_source)] = $this->class_loader->$data_source->get_total_count($student_id, $print_criteria, $count_type);
	      }
		}
		
		foreach ($result as $key => $value) {
			if ($value == '') {
				$result[$key] = $print_criteria['EmptyDataSymbol'];
			}
		}
		
		return $result;
	}
}

?>