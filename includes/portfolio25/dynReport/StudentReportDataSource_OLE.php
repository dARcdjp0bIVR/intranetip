<?php
#using:
#
# modifications:
# Anna  (2018-05-11) modified load_data() - fix outerTableOrderBy cannot order by other fields when PHPversion 5.4 #139057
# Omas  (2018-04-18) modified load_data() - fix cannot show any record problem #N138216
# Omas  (2018-03-23) modified load_data() - fix fail to load EXT record causing by incorrect order statement #E137299
# Omas	(2018-01-22) modified load_data() - added sorting for 5.4 as maria db #B134536 
# Villa (2017-06-05) modified load_data() add flag to order by ELE  #F117604 
# Ivan (2012-03-15) [2012-0315-1222-26132]: modified get_total_count() to fix wrong counting of total OLE records
# Ivan (2011-07-11) [2011-0711-1023-36096]: OLE remarks retrieved from OLE_PROGRAM instead of OLE_STUDENT now 
# Henry Yuen (2010-04-19): added new field "Component of OLE"

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once 'StudentReportDataSourceBase_TableSection.php';
include_once 'srt_lang.php';

/**
 * The data source for accessing data from OLE.
 * This data source is supposed to be managed by StudentReportDataSourceManager.
 */
class StudentReportDataSource_OLE extends StudentReportDataSourceBase_TableSection
{
	/**
	 * Get the display name of the data source.
	 */
	function get_display_name() {
		return srt_lang('OLE');
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_field_information() for the detail description of this method.
	 */
	function get_field_information() {
		return array(
			'StartDate' => array(
				'display_name' => srt_lang('Start Date'),
				'is_numeric' => false,
			),
			'EndDate' => array(
				'display_name' => srt_lang('End Date'),
				'is_numeric' => false,
			),
			'AcademicYear' => array(
				'display_name' => srt_lang('Academic Year'),
				'is_numeric' => false,
			),
			'CategoryName' => array(
				'display_name' => srt_lang('Category'),
				'is_numeric' => false,
			),
			# Begin Henry Yuen (2010-04-19): added new field "Component of OLE"
			'ELE' => array(
				'display_name' => srt_lang('Component of OLE'),
				'is_numeric' => false,
			),
			# End Henry Yuen (2010-04-19): added new field "Component of OLE"
			'Title' => array(
				'display_name' => srt_lang('Title'),
				'is_numeric' => false,
			),
			'Details' => array(
				'display_name' => srt_lang('Details'),
				'is_numeric' => false,
			),
			'Role' => array(
				'display_name' => srt_lang('Role'),
				'is_numeric' => false,
			),
			'Hours' => array(
				'display_name' => srt_lang('Hours'),
				'is_numeric' => true,
			),
			'Achievement' => array(
				'display_name' => srt_lang('Achievement'),
				'is_numeric' => false,
			),
			'Remark' => array(
				'display_name' => srt_lang('Remark'),
				'is_numeric' => false,
			),
			'Organization' => array(
				'display_name' => srt_lang('Organization'),
				'is_numeric' => false,
			),
			'TeacherComment' => array(
				'display_name' => srt_lang('Teacher Comment'),
				'is_numeric' => false,
			),
		);
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::load_data() for the detail description of this method.
	 */
	function load_data($student_id, $fields, $sort_by_field, $is_ascending, $max_num_of_records, $print_criteria, $filter_criteria, $sort_by_field2, $is_ascending2) {
		// Suppose that StudentReportDataSourceManager::load_data_from_data_source() has validated some of the parameters.
		global $eclass_db, $intranet_db, $ipf_cfg,$sys_custom;
		// Transform some fields.
		$field_mapping = array(
			'StartDate' => "
				{$this->transform_date_field('StartDate')} AS StartDate
			",
			'EndDate' => "
				(CASE
				WHEN EndDate = '0000-00-00' OR EndDate IS NULL THEN
					{$this->transform_date_field('StartDate')}
				ELSE EndDate END) AS EndDate
			",
		);
		$fields = $this->transform_fields_for_loading_data($fields, $field_mapping);
		
		$conds = $this->transform_print_criteria($print_criteria, $filter_criteria);
		
		$innerTableOrderBy = ' os.ModifiedDate Desc, os.InputDate Desc ';
		
		$orderBySqlAry = array();
		$numOfOrderField = 0;
		
		//F117604 
		if(in_array('ELE',(array)$fields) && $sys_custom['iPf']['DynamicReport']['OLESortByELE']){
			//#F132704 fix for F117604(start)
			$sorting_order = ($is_ascending)? " ASC" : " DESC";

			if($sort_by_field){
				$orderBySqlAry[$numOfOrderField] = "{$sort_by_field} $sorting_order, ELE ";
			}else{
				$orderBySqlAry[$numOfOrderField] = "ELE $sorting_order";
			}

			//#F132704 fix for F117604
// 			if($is_ascending){
// 				//$outerTableOrderBy .=" ASC";
// 				$orderBySqlAry[$numOfOrderField] .= " ASC";
// 			}else{
// 				//$outerTableOrderBy .=" DESC";
// 				$orderBySqlAry[$numOfOrderField] .= " DESC";
// 			}
		}else{
			//regular
			//$outerTableOrderBy = ($sort_by_field)? ("ORDER BY $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "";
		    // E137299 fix fail to print EXT record probem
			//$orderBySqlAry[$numOfOrderField] = ($sort_by_field ? (" $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "");
			if(!empty($sort_by_field)){
			    $orderBySqlAry[$numOfOrderField] = " $sort_by_field " . ($is_ascending ? "ASC" : "DESC");
			}
		}
		$numOfOrderField++;
		
		if ($sort_by_field2 == "") {
			// don't include in array
		}
		else {
			$orderBySqlAry[$numOfOrderField] = ($sort_by_field2 ? (" $sort_by_field2 " . ($is_ascending2 ? "ASC" : "DESC")) : "");
			$numOfOrderField++;
		}
		
		//please don't change the sequence, the final ordering should be based on if client select the ordering same as SLP
		
		if($sort_by_field == 'AcademicYear'){
			$orderSeq = ($is_ascending)  ? ' asc ':' desc ';
			//$innerTableOrderBy = ' ay.Sequence '.$orderSeq;
			//$outerTableOrderBy = '';
			$outerTableOrderBy = '';
			
			//F117604 
			if(in_array('ELE',(array)$fields) && $sys_custom['iPf']['DynamicReport']['OLESortByELE']){
				//$outerTableOrderBy = "ORDER BY Sequence, ELE ";
				//$orderBySqlAry[$numOfOrderField] = " Sequence, ELE";
				//#F132704
				$orderBySqlAry[$numOfOrderField] = " ELE";
				if($is_ascending){
					//$outerTableOrderBy .=" ASC";
					$orderBySqlAry[$numOfOrderField] .= " ASC";
				}else{
					//$outerTableOrderBy .=" DESC";
					$orderBySqlAry[$numOfOrderField] .= " DESC";
				}
				$numOfOrderField++;
			}
			
			// Omas 20180314 as iPo admin may not know ay.Sequence(set in school setting)
			//$innerTableOrderBy = ' ay.Sequence '.$orderSeq;
			$innerTableOrderBy = ' ay.YearNameEN '.$orderSeq;
			if (count($orderBySqlAry) > 0) {
				$innerTableOrderBy .= ','.implode(',', (array)$orderBySqlAry);
			}
			
			$orderBySqlAry = array();
			$numOfOrderField = 0;
		}
		
		if (count($orderBySqlAry) > 0) {
			$outerTableOrderBy = "ORDER BY ".implode(',', (array)$orderBySqlAry);
		}
		
		if ($print_criteria['PrintSelRec'] && strtolower($print_criteria['DisplayOLEOrderOptions'])==strtolower('studentSelected')) {
			$innerTableOrderBy = ' os.SLPOrder Asc ';
			$outerTableOrderBy = '';
		}
		
	    if(intranet_phpversion_compare('5.4')=='SAME'){
	        if ($sort_by_field == 'AcademicYear') {
    			$newOrderSupportCentOS7 = " ORDER BY ".str_replace(array('os.', 'ay.'), array('a.', 'a.'), $innerTableOrderBy)."";
    			$outerTableOrderBy = $newOrderSupportCentOS7;
	        }
		}
	
		// Select the most recent records temporarily, as selecting records by academic year is not supported yet due to the possible schema changes related to academic year in the near future.
		//F117604 add field ay.sequence
		$sql = "
      SELECT " . join(', ', $fields) . "
      FROM (
        SELECT
          op.StartDate,
          op.EndDate,
          " . Get_Lang_Selection("ay.YearNameB5", "ay.YearNameEN") . " AS AcademicYear,
          " . Get_Lang_Selection("oc.ChiTitle", "oc.EngTitle") . " AS CategoryName,
          op.ELE,
          op.Title,
          op.Details,
          os.Role,
          os.Hours,
          os.Achievement,
          /* os.Remark, */
		  op.SchoolRemarks as Remark,
          op.Organization,
          os.TeacherComment,
		  ay.Sequence,
		  ay.YearNameEN,
		  os.SLPOrder,
		  os.ModifiedDate,
		  os.InputDate 
        FROM
          {$eclass_db}.OLE_STUDENT os
        INNER JOIN {$eclass_db}.OLE_PROGRAM op
          ON op.ProgramID = os.ProgramID
        LEFT JOIN {$eclass_db}.OLE_CATEGORY oc
          ON op.Category = oc.RecordID
        INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay
          ON op.AcademicYearID = ay.AcademicYearID
        WHERE
          os.UserID = '{$student_id}' AND
          os.RecordStatus IN ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."','".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."') AND
          op.IntExt = '{$this->int_or_ext()}'
          {$conds}
		Order By
		  $innerTableOrderBy
		" . ($max_num_of_records ? "LIMIT $max_num_of_records" : "") . "
      ) AS a
      $outerTableOrderBy
    ";
	$returnResult = $this->returnArray($sql);
	
	# Begin Henry Yuen (2010-04-19): added new field "Component of OLE"
	# convert ELE ID to Title
	$LibPortfolio = new libpf_slp();
	$DefaultELEArray = $LibPortfolio->GET_ELE();
	
	$fieldCount = sizeof($fields);
	$resultCount = sizeof($returnResult);	
	for($f = 0; $f < $fieldCount; $f++){
		if(trim($fields[$f]) == 'ELE'){
			for($i = 0; $i < $resultCount; $i++){
				$ele = $returnResult[$i][$f];	
				if($ele != '')
				{
					$ELEArr = explode(',', $ele);
					$countELE = sizeof($ELEArr);
					$ele_display = '';
					for($j = 0; $j < $countELE; $j++)
					{
						$t_ele = trim($ELEArr[$j]);						
						//$ele_display .= "- ".$DefaultELEArray[$t_ele]."<br />";
						//case 2011-0601-1606-08073 agree to remove the "-"
//						$ele_display .= $DefaultELEArray[$t_ele]."<br />";
						$ele_display .= $DefaultELEArray[$t_ele];

						if($sys_custom['iPf']['DynamicReport']['eleRemoveLastBr']){
							if($j < ($countELE - 1)){
								//append with <br /> only if it is not a last ele
								$ele_display .= "<br />";
							}
						}else{
							$ele_display .= "<br />";
						}
					}
				}
				else{ 
					$ele_display = "--";
				}	
				$returnResult[$i][$f] = $ele_display;							
			}			
		}
		else {
			for($i = 0; $i < $resultCount; $i++){
				$returnResult[$i][$f] = Get_Standardized_PDF_Table_Data_Display($returnResult[$i][$f]);
			}
		}
	}	
	# End Henry Yuen (2010-04-19): added new field "Component of OLE"				

	  return $returnResult;	
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_total_count() for the detail description of this method.
	 */
	function get_total_count($student_id, $print_criteria) {
		
		global $eclass_db, $ipf_cfg;
		
		$conds = $this->transform_print_criteria($print_criteria);
		
		//2012-0315-1222-26132
//		return current($this->returnVector("
//			SELECT count(*) FROM {$eclass_db}.OLE_STUDENT s
//			JOIN (
//				/*
//					The IntExt value in OLE_STUDENT may not be correct, so use the value in OLE_PROGRAM.
//					Use sub-query to prevent name clash.
//				*/
//				SELECT 
//						op.ProgramID 
//				FROM 
//						{$eclass_db}.OLE_PROGRAM as op
//						Inner Join
//						{$eclass_db}.OLE_STUDENT as os On (op.ProgramID = os.ProgramID)
//				WHERE 
//						op.IntExt = '{$this->int_or_ext()}' {$conds}
//			) p ON p.ProgramID = s.ProgramID
//			WHERE s.UserID = '{$student_id}' AND s.RecordStatus IN (2,4)
//		"));
		return current($this->returnVector("
			SELECT 
					count(*)
			FROM 
					{$eclass_db}.OLE_PROGRAM as op
					Inner Join
					{$eclass_db}.OLE_STUDENT as os On (op.ProgramID = os.ProgramID)
			WHERE 
					os.UserID = '{$student_id}' AND os.RecordStatus IN ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."','".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."') and op.IntExt = '{$this->int_or_ext()}' {$conds}
		"));
	}
	
	function get_total_sum($student_id, $print_criteria) {
		
		global $eclass_db, $ipf_cfg;
		
		$conds = $this->transform_print_criteria($print_criteria);
		$sql = "SELECT os.Hours FROM {$eclass_db}.OLE_STUDENT os
			      INNER JOIN {$eclass_db}.OLE_PROGRAM op ON op.ProgramID = os.ProgramID
			      WHERE op.IntExt = '{$this->int_or_ext()}' AND os.UserID = '{$student_id}' AND os.RecordStatus IN ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."','".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."') {$conds}";
		$hourArr = $this->returnVector($sql);
		
		return array_sum((array)$hourArr);
		
//		return current($this->returnVector("
//			SELECT SUM(os.Hours) FROM {$eclass_db}.OLE_STUDENT os
//      INNER JOIN {$eclass_db}.OLE_PROGRAM op ON op.ProgramID = os.ProgramID
//      WHERE op.IntExt = '{$this->int_or_ext()}' AND os.UserID = '{$student_id}' AND os.RecordStatus IN (2,4) {$conds}
//		"));
	}
	
	function get_category_sum($student_id, $print_criteria) {
		global $eclass_db, $ipf_cfg;
		
		$CategoryNameField = Get_Lang_Selection('oc.ChiTitle', 'oc.EngTitle');
		$sql = "Select
						$CategoryNameField as CategoryName,
						Sum(os.Hours) as CategoryHours
				From
						{$eclass_db}.OLE_STUDENT as os
						Inner Join
						{$eclass_db}.OLE_PROGRAM as op On (os.ProgramID = op.ProgramID)
						Inner Join
						{$eclass_db}.OLE_CATEGORY as oc On (op.Category = oc.RecordID)
				Where
						os.UserID = '".$student_id."'
						And os.RecordStatus IN ('".$ipf_cfg["OLE_STUDENT_RecordStatus"]["approved"]."','".$ipf_cfg["OLE_STUDENT_RecordStatus"]["teacherSubmit"]."')
						And os.IntExt = '".$this->int_or_ext()."'
						And oc.RecordStatus In ('".$ipf_cfg["OLE_CATEGORY_RecordStatus"]["enabled"]."', '".$ipf_cfg["OLE_CATEGORY_RecordStatus"]["default"]."')
				Group By
						oc.RecordID
				Having
						CategoryHours > 0
				Order By
						oc.EngTitle
				";
		$CategoryInfoArr = $this->returnArray($sql);
		$numOfCategory = count($CategoryInfoArr);
		
		$x = '';
		
		if ($numOfCategory > 0) {
			$x .= '<table class="border_table" style="width:50%; border:1px solid black; padding:0px 0px 0px 0px;">'."\n";
			for ($i=0; $i<$numOfCategory; $i++)
			{
				$thisCategoryName = $CategoryInfoArr[$i]['CategoryName'];
				$thisCategoryHours = $CategoryInfoArr[$i]['CategoryHours'];
				
				$x .= '<tr>'."\n";
					$x .= '<td>'.$thisCategoryName.'</td>'."\n";
					$x .= '<td>'.$thisCategoryHours.'</td>'."\n";
				$x .= '</tr>'."\n";
			}
			$x .= '</table>'."\n";		}
		
		return $x;
	}
	
	/**
	 * Return the value of the IntExt field of the OLE_PROGRAM table, for filtering results.
	 * For OLE, the value should be 'INT' (internal).
	 * For Performance / Awards and Key Participation Outside School, the value should be 'EXT' (external).
	 */
	protected function int_or_ext() {
		return 'INT';
	}
	
	/**
	 * Override method of parent class
	 * @param $print_criteria from load_data().
	 */
	protected function transform_print_criteria($print_criteria, $filter_criteria=null) {
	
	    $conds = "";
		foreach ($print_criteria as $field_name => &$field_values) {
			switch($field_name) {
				case "AcademicYearIDs":
					$conds .= empty($field_values) ? " AND 0 " : " AND op.AcademicYearID IN (" . join(', ', $field_values) .") ";
					break;
				case "YearTermIDs":
					$conds .= empty($field_values) ? "" : " AND op.YearTermID IN (" . join(', ', $field_values) .") "; 
					break;
				case "PrintSelRec":
					$conds .= ($field_values) ? " AND os.SLPOrder IS NOT NULL " : "";
					break;
			}
		}
		
		// Criteria Filtering
		if (is_array($filter_criteria)) {
			foreach ($filter_criteria as $_data_field => $_filter_criteria_arr) {
				// Get corresponding DB Field
				$_db_field = '';
				switch ($_data_field) {
					case 'CategoryName':
						$_db_field = 'op.Category';
						break;
					case 'ELE':
						$_db_field = 'op.ELE';
						break;
					case 'Title':
						$_db_field = 'op.Title';
						break;
					case 'Role':
						$_db_field = 'os.Role';
						break;
					case 'Achievement':
						$_db_field = 'os.Achievement';
						break;
				}
				
				// Build condition
				$conds .= $this->transform_filter_criteria($_data_field, $_db_field, $_filter_criteria_arr);
			}
		}
		
		return $conds;
	}
}

?>