<?php

include_once 'StudentReportDataSourceBase_TableSection.php';
include_once 'srt_lang.php';

/**
 * The data source for accessing data from Service.
 * This data source is supposed to be managed by StudentReportDataSourceManager.
 */
class StudentReportDataSource_Service extends StudentReportDataSourceBase_TableSection
{
	/**
	 * Get the display name of the data source.
	 */
	function get_display_name() {
		return srt_lang('Service');
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_field_information() for the detail description of this method.
	 */
	function get_field_information() {
		return array(
			# Being Henry Yuen (2010-05-04): add fields
			'Year' => array(
				'display_name' => srt_lang('School Year'),
				'is_numeric' => false,
			),
			'Semester' => array(
				'display_name' => srt_lang('Semester'),
				'is_numeric' => false,
			),
			# End Henry Yuen (2010-05-04): add fields
			'ServiceDate' => array(
				'display_name' => srt_lang('Service Date'),
				'is_numeric' => false,
			),
			'ServiceName' => array(
				'display_name' => srt_lang('Service Name'),
				'is_numeric' => false,
			),
			'Role' => array(
				'display_name' => srt_lang('Role'),
				'is_numeric' => false,
			),
			'Performance' => array(
				'display_name' => srt_lang('Performance'),
				'is_numeric' => false,
			),
			'Organization' => array(
				'display_name' => srt_lang('Organization'),
				'is_numeric' => false,
			),
			'Hours' => array(
				'display_name' => srt_lang('Hours'),
				'is_numeric' => true,
			),
			'Remark' => array(
				'display_name' => srt_lang('Remark'),
				'is_numeric' => false,
			),
		);
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::load_data() for the detail description of this method.
	 */
	function load_data($student_id, $fields, $sort_by_field, $is_ascending, $max_num_of_records, $print_criteria, $filter_criteria, $sort_by_field2, $is_ascending2) {
		// Suppose that StudentReportDataSourceManager::load_data_from_data_source() has validated some of the parameters.
		
		global $eclass_db, $Lang;
		
		// Transform some fields.
		$field_mapping = array(
			'ServiceDate' => "
				{$this->transform_date_field('ServiceDate')} AS ServiceDate
			",
		);
		$fields = $this->transform_fields_for_loading_data($fields, $field_mapping);
		
		$conds = $this->transform_print_criteria($print_criteria);



		$innerTableOrderBy = 'ss.ModifiedDate DESC, ss.InputDate DESC';
		//$outerTableOrderBy = ($sort_by_field ? ("ORDER BY $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "");
		$orderBySqlAry = array();
		$orderBySqlAry[] = ($sort_by_field ? (" $sort_by_field " . ($is_ascending ? "ASC" : "DESC")) : "");
		
		if ($sort_by_field2 == "") {
			// don't include in array
		}
		else {
			$orderBySqlAry[] = ($sort_by_field2 ? (" $sort_by_field2 " . ($is_ascending2 ? "ASC" : "DESC")) : "");
		}
		
		if (count($orderBySqlAry) > 0) {
			$outerTableOrderBy = "ORDER BY ".implode(',', (array)$orderBySqlAry);
		}

		if($sort_by_field == 'Year'){
			$orderSeq = ($is_ascending)  ? ' asc ':' desc ';
			$innerTableOrderBy = ' ay.Sequence '.$orderSeq;
			if (count($orderBySqlAry) > 0) {
				$innerTableOrderBy .= ','.implode(',', (array)$orderBySqlAry);
			}
			$outerTableOrderBy = '';
		}


		// Select the most recent records temporarily, as selecting records by academic year is not supported yet due to the possible schema changes related to academic year in the near future.
		$sql = "
			SELECT " . join(', ', $fields) . "
			FROM (
				SELECT
					" . Get_Lang_Selection("ay.YearNameB5", "ay.YearNameEN") . " AS Year,
					If (ayt.YearTermID Is Null, 
						if((ss.Semester='' OR  ss.Semester IS NULL), '".$Lang['General']['WholeYear']."', ss.Semester),
						".Get_Lang_Selection("ayt.YearTermNameB5", "ayt.YearTermNameEN")."
					) AS Semester,
					ss.ServiceDate,
					ss.ServiceName,
					ss.Role,
					ss.Performance,
					ss.Organization,
					ss.Hours,
					ss.Remark
				FROM 
					{$eclass_db}.SERVICE_STUDENT ss
					INNER JOIN {$intranet_db}.ACADEMIC_YEAR ay
          			ON ss.AcademicYearID = ay.AcademicYearID
        			Left Join {$intranet_db}.ACADEMIC_YEAR_TERM ayt
          			ON ss.YearTermID = ayt.YearTermID
				WHERE ss.UserID = '{$student_id}' {$conds}
				ORDER BY 
				$innerTableOrderBy

				" . ($max_num_of_records ? "LIMIT $max_num_of_records" : "") . "
			) a

			$outerTableOrderBy
		";
		return $this->returnArray($sql);
	}
	
	/**
	 * See StudentReportDataSourceBase_TableSection::get_total_count() for the detail description of this method.
	 */
	function get_total_count($student_id, $print_criteria) {
		
		global $eclass_db;
		
		$conds = $this->transform_print_criteria($print_criteria);
		
		return current($this->returnVector("
			SELECT count(*) FROM {$eclass_db}.SERVICE_STUDENT ss
			WHERE ss.UserID = '{$student_id}' {$conds}
		"));
	}
	
	/**
	 * Override method of parent class
	 * @param $print_criteria from load_data().
	 */
	protected function transform_print_criteria($print_criteria) {
	
    $conds = "";
		foreach ($print_criteria as $field_name => &$field_values) {
		  switch($field_name) {
        case "AcademicYearIDs":
          $conds .= empty($field_values) ? " AND 0" : " AND ss.AcademicYearID IN (" . join(', ', $field_values) .")";
          break;
        case "YearTermIDs":
          $conds .= empty($field_values) ? "" : " AND ss.YearTermID IN (" . join(', ', $field_values) .")"; 
          break;
      }
		}
		return $conds;
  }
}

?>
