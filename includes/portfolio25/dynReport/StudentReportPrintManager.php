<?php
#using: anna
#modifications: 
#   Anna (2019-07-12) : modified print_report_by_list_of_students() CHANGED to getEncryptedTextWithNoTimeChecking
#	Villa(2017-06-23): modified print_report_by_class_name() - die if no student selected
#	Omas (2015-05-11): new print_report_by_alumni_list() - for printing alumni report
#	Henry Yuen (2010-07-30): support print doc format

include_once 'StudentReportClassLoader.php';
include_once 'StudentReportTemplateManager.php';
include_once("$intranet_root/includes/libclass.php");
include_once("$intranet_root/includes/libfilesystem.php");
include_once("$intranet_root/includes/libportfolio.php");

/**
 * Responsible for determining which report to print for which students, and passing the data to appropriate StudentReportXXXGenerator class.
 */
class StudentReportPrintManager extends StudentReportClassLoader
{
	/**
	 * Setup $member_to_class_map for use by StudentReportClassLoader.
	 * Here, StudentReportClassLoader is used to find the report generator, given the output format.
	 */
	protected $member_to_class_map = array(
		// Output format => Report generator
		'pdf' => 'StudentReportPDFGenerator',
		'doc' => 'StudentReportDOCGenerator',  // Henry Yuen (2010-07-30)
		'html' => 'StudentReportHTMLGenerator'  // Ivan (2013-01-18)
	);
	
	/**
	 * Print a report based on a template and some options.
	 * @param $output_name The file name to use for output to browser.
	 *   If not given, the output name will be derived from the template title and the output format.
	 * @param $output_format The output format. Should be one of the keys in $member_to_class_map.
	 * @param $report_template_id The ID of a report template to use for report printing.
	 * @param $class_name Report will be printed for the students in the class.
	 * @param $print_criteria The options provided by the user through the Print Report page, like:
	 * 		array(
	 * 			'AcademicYearIDs' => array(...),
	 * 			'YearTermIDs' => array(...),
	 * 		)
	 * @param $progress_callback The callback for receiving progress information.
	 *   A float number from 0 to 1 will be passed to the callback as the first parameter.
	 * @return a ticket to the generated report.
	 */
	function print_report_by_class_name($output_name, $output_format, $report_template_id, $class_name, $print_criteria, $progress_callback = null, $batchPrintMode=null, $targetStudentAry=null) {
		global $sys_custom;
		if (!$output_format) throw new Exception("The parameter output_format should be given.");
		if (!$this->member_to_class_map[$output_format]) throw new Exception("The output format '$output_format' is not supported.");
		if (!$report_template_id) throw new Exception("The parameter report_template_id should be given.");
		if (!$class_name) throw new Exception("The parameter class_name should be given.");
		
		global $ipf_cfg, $intranet_root;
		
		$batchPrintMode = ($batchPrintMode=='')? $ipf_cfg['dynReport']['batchPrintMode_default'] : $batchPrintMode;
		
		$requestRecordStatus = '0,1,2';

		if($sys_custom['iPf']['DynamicReport']['dontPrintSuspended']){
			$requestRecordStatus = '1,2';
		}
		
		
		if ($targetStudentAry === null) {
			// get students from the class
			#A119072 
			echo '<!--error: no student-->';
			die;
// 			$libclass = new libclass();
// 			$student_id_list = $libclass->getClassStudentListOrderByClassNo($class_name , $requestRecordStatus);
		}
		else {
			// specific students list
			$student_id_list = $targetStudentAry;
		}
		
		if ($batchPrintMode == $ipf_cfg['dynReport']['batchPrintMode']['zip']) {
			$libfs = new libfilesystem();
			$success_arr = array();
			
			// prepare PDF temp folder
			$TempFolder = $intranet_root."/file/temp/ipf_dynRpt_pdf";
			$ZipFolderName = 'dynRpt_'.$class_name;
			$TempUserFolder = $TempFolder.'/'.$_SESSION['UserID'];
			$TempPdfFolder = $TempUserFolder.'/'.$ZipFolderName;
			if (!file_exists($TempPdfFolder)) {
				$success_arr['CreateTempPdfFolder'] = $libfs->folder_new($TempPdfFolder);
			}
			$ZipFileName = $ZipFolderName.'.zip';
			$ZipFilePath = $TempUserFolder.'/'.$ZipFileName;
			
			// generate report for each student
			$objUserArr = new libuser('', '', $student_id_list);
			$num_of_student = count($student_id_list);
			for ($i=0; $i<$num_of_student; $i++) {
				$_studentId = $student_id_list[$i];
				
				// get student class name and class number as the file name
				$objUserArr->LoadUserData($_studentId);  
				$fileName = $objUserArr->ClassName.'_'.$objUserArr->ClassNumber.'.'.$output_format;
				
				// prepare progress bar info
				$_progress_percentage = ($i + 1) / $num_of_student;
				
				// geenrate the report
				$_ticket = $this->print_report_by_list_of_students($fileName, $output_format, $report_template_id, array($_studentId), $print_criteria, $progress_callback, $_progress_percentage);
				
				// temp save the report content in the server
				list($report_name, $report_content) = $_SESSION['StudentReportPrintManager.tickets'][$_ticket];
				$success_arr[$report_name] = $libfs->file_write($report_content, $TempPdfFolder.'/'.$report_name);
			}
			
			// delete old zip file and zip the current csv files
			$success_arr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($ZipFilePath);
			$success_arr['ZipPdfFiles'] = $libfs->file_zip($ZipFolderName, $ZipFilePath, $TempUserFolder);
				
			// remove the temp folder
			$success_arr['DeleteTempPdfFiles'] = $libfs->folder_remove_recursive($TempPdfFolder);
			
			return $this->generate_report_ticket($ZipFileName, $ZipFilePath, $batchPrintMode);
		}
		else if ($batchPrintMode == $ipf_cfg['dynReport']['batchPrintMode']['all']) {
			return $this->print_report_by_list_of_students($output_name, $output_format, $report_template_id, $student_id_list, $print_criteria, $progress_callback);
		}
	}
	
	function print_report_by_alumni_list($output_name, $output_format, $report_template_id, $print_criteria, $progress_callback = null, $batchPrintMode=null, $targetStudentAry=null) {
		global $sys_custom;
		if (!$output_format) throw new Exception("The parameter output_format should be given.");
		if (!$this->member_to_class_map[$output_format]) throw new Exception("The output format '$output_format' is not supported.");
		if (!$report_template_id) throw new Exception("The parameter report_template_id should be given.");
		
		global $ipf_cfg, $intranet_root;
		
		$batchPrintMode = ($batchPrintMode=='')? $ipf_cfg['dynReport']['batchPrintMode_default'] : $batchPrintMode;
		
		$requestRecordStatus = '0,1,2';

		if($sys_custom['iPf']['DynamicReport']['dontPrintSuspended']){
			$requestRecordStatus = '1,2';
		}
		
		// specific students list
		$student_id_list = $targetStudentAry;
		
		if ($batchPrintMode == $ipf_cfg['dynReport']['batchPrintMode']['zip']) {
			$libfs = new libfilesystem();
			$success_arr = array();
			
			// prepare PDF temp folder
			$TempFolder = $intranet_root."/file/temp/ipf_dynRpt_pdf";
			$ZipFolderName = 'dynRpt_alumni';
			$TempUserFolder = $TempFolder.'/'.$_SESSION['UserID'];
			$TempPdfFolder = $TempUserFolder.'/'.$ZipFolderName;
			if (!file_exists($TempPdfFolder)) {
				$success_arr['CreateTempPdfFolder'] = $libfs->folder_new($TempPdfFolder);
			}
			$ZipFileName = $ZipFolderName.'.zip';
			$ZipFilePath = $TempUserFolder.'/'.$ZipFileName;
			
			// generate report for each student
			$objUserArr = new libuser('', '', $student_id_list);
			$num_of_student = count($student_id_list);
			for ($i=0; $i<$num_of_student; $i++) {
				
				$_studentId = $student_id_list[$i];
				
				// get student class name and class number as the file name
				$objUserArr->LoadUserData($_studentId);  
				$fileName = $objUserArr->StandardDisplayName.'.'.$output_format;

				
				// prepare progress bar info
				$_progress_percentage = ($i + 1) / $num_of_student;
				
				// geenrate the report
				$_ticket = $this->print_report_by_list_of_students($fileName, $output_format, $report_template_id, array($_studentId), $print_criteria, $progress_callback, $_progress_percentage);
				
				// temp save the report content in the server
				list($report_name, $report_content) = $_SESSION['StudentReportPrintManager.tickets'][$_ticket];
				$success_arr[$report_name] = $libfs->file_write($report_content, $TempPdfFolder.'/'.$report_name);
			}
			
			// delete old zip file and zip the current csv files
			$success_arr['DeleteLastGeneratedZipFile'] = $libfs->file_remove($ZipFilePath);
			$success_arr['ZipPdfFiles'] = $libfs->file_zip($ZipFolderName, $ZipFilePath, $TempUserFolder);
				
			// remove the temp folder
			$success_arr['DeleteTempPdfFiles'] = $libfs->folder_remove_recursive($TempPdfFolder);
			
			return $this->generate_report_ticket($ZipFileName, $ZipFilePath, $batchPrintMode);
		}
		else if ($batchPrintMode == $ipf_cfg['dynReport']['batchPrintMode']['all']) {
			return $this->print_report_by_list_of_students($output_name, $output_format, $report_template_id, $student_id_list, $print_criteria, $progress_callback);
		}
	}
	
	/**
	 * Print a report based on a template and some options.
	 * @param $output_name The file name to use for output to browser.
	 *   If not given, the output name will be derived from the template title and the output format.
	 * @param $output_format The output format. Should be one of the keys in $member_to_class_map.
	 * @param $report_template_id The ID of a report template to use for report printing.
	 * @param $student_id_list An array of the user IDs of the students for which report will be printed.
	 * @param $print_criteria The options provided by the user through the Print Report page, like:
	 * 		array(
	 * 			'AcademicYearIDs' => array(...),
	 * 			'YearTermIDs' => array(...),
	 * 		)
	 * @param $progress_callback The callback for receiving progress information.
	 *   A float number from 0 to 1 will be passed to the callback as the first parameter.
	 * @return a ticket to the generated report.
	 */
	function print_report_by_list_of_students($output_name, $output_format, $report_template_id, $student_id_list, $print_criteria, $progress_callback = null, $progress_percentage=null) {
		if (!$output_format) throw new Exception("The parameter output_format should be given.");
		if (!$this->member_to_class_map[$output_format]) throw new Exception("The output format '$output_format' is not supported.");
		if (!$report_template_id) throw new Exception("The parameter report_template_id should be given.");
		if (!is_array($student_id_list)) throw new Exception("The parameter student_id_list should be an array.");
		
		global $ipf_cfg, $eclass_httppath;
		
		// Get the report template.
		$template_manager = new StudentReportTemplateManager();
		$report_template = $template_manager->load($report_template_id);
		if ($report_template === false) throw new Exception("Cannot load the template with the ID: $report_template_id.");
		
		// Need to derive the output name?
		if (!$output_name) {
			$output_name = "{$report_template->get_template_title()}.{$output_format}";
			// Replace some characters in the output name with '_' to prevent problems.
			$output_name = str_replace(array('/', '\\', ':', '*', '?', '"', '<', '>', '|'), '_', $output_name);
		}
		
		// Generate the report.
		$report_content = $this->$output_format->generate_report($report_template, $student_id_list, $print_criteria, $progress_callback, $progress_percentage);
		
		// Generate PDF report to students' folder
		$releaseToStudent = $print_criteria['ReleaseToStudent'];
		$numOfStudent = count($student_id_list);
		$pdfReportContentAry = array();
		if ($releaseToStudent) {
			if ($numOfStudent > 1) {
				for ($i=0; $i<$numOfStudent; $i++) {
					$_studentId = $student_id_list[$i];
					
					$pdfReportContentAry[$_studentId] = $this->pdf->generate_report($report_template, array($_studentId), $print_criteria);
				}
			}
			else {
				$curStudentId = $student_id_list[0];
				$pdfReportContentAry[$curStudentId] = $report_content;
			}
			
			// store the pdf in the server
			$libfs = new libfilesystem();
			$filePath = '';
			$filePath .= $ipf_cfg['dynReport']['studentReportPath'];
			if (!file_exists($filePath)) {
				$successAry['createRootFolder'] = $libfs->folder_new($filePath);
			}
			
			$filePath .= '/'.$report_template_id;
			if (!file_exists($filePath)) {
				$successAry['createRootFolder'] = $libfs->folder_new($filePath);
			}
			global $eclass_db, $intranet_db;
			$libpf = new libportfolio();
			
			$successAry = array();
			foreach ((array)$pdfReportContentAry as $_studentId => $_pdfContent) {
			    
// 			    $sql = "select modifieddate from {$eclass_db}.student_report_generated_record 
//                     where template_id = '$report_template_id' and student_id = '$_studentId' order by modifieddate desc";
// 			    $libclass = new libclass();
			    
// 			    $modifieddate = $libclass->returnVector($sql);
// 			    $TimeStamp = strtotime($modifieddate[0]);
			    
// 			    $_fileName = getEncryptedText((string)$_studentId, $ipf_cfg['dynReport']['encryptKey'],false,$TimeStamp).'.pdf';
			    $_fileName = $libpf->getEncryptedTextWithNoTimeChecking((string)$_studentId, $ipf_cfg['dynReport']['encryptKey']).'.pdf';
			    
				if (file_exists($filePath.'/'.$_fileName)) {
					$libfs->file_remove($filePath.'/'.$_fileName);
				}
				$successAry[$_studentId] = $libfs->file_write($_pdfContent, $filePath.'/'.$_fileName);
			}
		}
		
		
		if ($output_format == $ipf_cfg['dynReport']['batchPrintMode']['pdf']) {
			// Return the ticket to the report.
			return $this->generate_report_ticket($output_name, $report_content, $ipf_cfg['dynReport']['batchPrintMode']['all']);
		}
		else if ($output_format == $ipf_cfg['dynReport']['batchPrintMode']['html']) {
			$output = '';
			$output .= "<html>\n";
			    $output .= "<head>\n";
				    $output .= returnHtmlMETA()."\n";
				    $output .= "<link href=\"http://{$eclass_httppath}/src/includes/css/style_portfolio_report.css\" rel=\"stylesheet\" type=\"text/css\">\n";
				    $output .= "<link href=\"http://{$eclass_httppath}/src/includes/css/style_portfolio_table.css\" rel=\"stylesheet\" type=\"text/css\">\n";
			    $output .= "</head>\n";
			    $output .= "<body>\n";
			    	$output .= '<style type="text/css">
									BODY, P, TD, INPUT, TEXTAREA, SELECT {
									    font-family: Arial,Verdana,Helvetica,Mingliu,Sans-Serif;
									}
									.report_header {
										padding:3px;
										font-size:13pt;
										font-weight:bold;
										font-family:Arial
									}
									.module_title {
										color:#FFFFFF;
										padding:3px;
										font-size:12pt;
										font-weight:bold;
										font-family:Arial
									}
									.padding_all {
										padding:3px
									}
									.border_table{
										border-collapse:collapse;
										border:1px solid #000000;
									}
									.border_table tr td, .border_table tr th{
										border:1px solid #000000;
									}
								</style>
								';
			    	$output .= $report_content."\n";
			    $output .= "</body>\n";
		    $output .= "</html>\n";
		    
			echo $output;
		}
	}
	
	/**
	 * This function saves a report, and returns a ticket, which can be used to retrieve the report later through output_report_by_ticket().
	 * This function will clear the previous report to save space.
	 * @return a ticket for retrieving the report later.
	 */
	function generate_report_ticket($report_name, $report_content, $printMode) {
		// Clear the previous report to save space.
		unset($_SESSION['StudentReportPrintManager.tickets']);
		
		// Generate a ticket, and save the report.
		// Just save the report into the session, so that we do not need to implement the garbage collection logic.
		// But this may cause problems if memory_limit is too low.
		$ticket = rand();
		$_SESSION['StudentReportPrintManager.tickets'][$ticket] = array($report_name, $report_content, $printMode);
		
		return $ticket;
	}
	
	/**
	 * Output a report, given a ticket.
	 * Clear the report afterwards.
	 * @param $ticket The ticket returned by generate_report_ticket().
	 */
	function output_report_by_ticket($ticket) {
		if (count($_SESSION['StudentReportPrintManager.tickets'][$ticket]) == 0) throw new Exception("Cannot find the report for the ticket '$ticket'.");
		
		global $ipf_cfg;
		
		// Get the report.
		list($report_name, $report_content, $output_mode) = $_SESSION['StudentReportPrintManager.tickets'][$ticket];
		
		// Clear the report.
		unset($_SESSION['StudentReportPrintManager.tickets'][$ticket]);
		
		// Output the report to the browser.
		if ($output_mode == $ipf_cfg['dynReport']['batchPrintMode']['zip']) {
			output2browser(get_file_content($report_content), $report_name);
		}
		else if ($output_mode == $ipf_cfg['dynReport']['batchPrintMode']['all']) {
			output2browser($report_content, $report_name);
		}		
	}
}

?>
