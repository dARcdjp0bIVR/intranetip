<?php
#Using: Henry
#Created by: Henry Yuen (2010-07-30): print student reports in doc format

include_once 'StudentReportTemplate.php';
include_once 'StudentReportDataSourceManager.php';
include_once 'StudentReportPDFGenerator_TCPDF.php';

/**
 * Responsible for outputting DOC, given a report template and a set of students.
 */
class StudentReportDOCGenerator
{
	/**
	 * Output the report in PDF format.
	 * @param $report_template An object of StudentReportTemplate.
	 * @param $student_id_list array(student_id1, student_id2, ...)
	 * @param $print_criteria The options provided by the user through the Print Report page, like:
	 * 		array(
	 * 			'AcademicYearIDs' => array(...),
	 * 			'YearTermIDs' => array(...),
	 * 		)
	 * @param $progress_callback The callback for receiving progress information.
	 *   A float number from 0 to 1 will be passed to the callback as the first parameter.
	 * @return PDF content
	 */
	function generate_report($report_template, $student_id_list, $print_criteria, $progress_callback = null) {
		global $no_record_msg;
		// TODO: to be completed.
		
		$data_source_manager = new StudentReportDataSourceManager();
		
		// Progress
		if (is_callable($progress_callback)) call_user_func($progress_callback, 0);
		$raw_progress = 0;
						
		$h = 'htmlspecialchars';
		$nb = 'nl2br';
		$t = 'trim';
		$count = 'count';
		
		$output = '';
		foreach ($student_id_list as $student_id) {		
			
			# start new page number for each student		
			// $pdf->startPageGroup();		
										
		  // Get student info
			$student_info = $data_source_manager->student_info->load_student_info($student_id, array(), $print_criteria);
							
			$output .= $report_template->assign_values_to_template($report_template->get_header_template(), $student_info);
						
			// Sections...
			foreach ($report_template->get_sections() as $section) {
				if ($section['section_type'] == 'table') {
					$table_section = $section['table'];
					$columns = $table_section['columns'];
					if (count($table_section['data_sources']) > 0 && count($columns) > 0) {
						$records = $data_source_manager->load_data_from_data_sources($student_id, $table_section['data_sources'], $table_section['sort_by_column_number'], $table_section['is_ascending'], $print_criteria);
																										
						// Section title.
						$output .= <<<HTMLEND
							<table width="100%" border="{$table_section['border_size']}" style="{$this->generate_css_style($table_section['section_title_style'])}" cellpadding="2">
							<tr><td>{$h($table_section['section_title'])}</td></tr>
							</table>
HTMLEND;
		
						if($table_section['section_description'] != ''){
						$output .= <<<HTMLEND
							<table width="100%" border="{$table_section['border_size']}" style="{$this->generate_css_style($table_section['section_description_style'])}" cellpadding="2">
							<tr><td>{$nb($h($t($table_section['section_description'])))}</td></tr>
							</table>
HTMLEND;
						}
						
								
						// The table.
						// It seems that border is a boolean value only, i.e., either show or hide the border.
						// TCPDF::SetLineWidth() may be used to set border size, but need to convert the unit of the parameter using TCPDF::getHTMLUnitToUnits().
						// It seems that border color does not work.
						$table_html = <<<HTMLEND
							<table border="{$table_section['border_size']}" cellpadding="2">
								<thead>
									<tr style="{$this->generate_css_style($table_section['column_title_style'])}">
HTMLEND;
						foreach ($columns as $column) {
							$width_attribute = $column['width'] ? ('width="' . $column['width'] . '"') : '';
							$table_html .= <<<HTMLEND
										<th $width_attribute>{$h($column['title'])}</th>
HTMLEND;
						}
						$table_html .= "
									</tr>
								</thead>
								<tbody>
						";
						if (count($records) > 0) {
							foreach ($records as $record) {
								$table_html .= <<<HTMLEND
									<tr nobr="true" style="{$this->generate_css_style($table_section['row_text_style'])}">
HTMLEND;
								// As the records may come from several data sources, get the data by numeric index instead of field name.
								for ($i = 0; $i < count($columns); $i++) {
									$width_attribute = $columns[$i]['width'] ? ('width="' . $columns[$i]['width'] . '"') : '';
									$table_html .= <<<HTMLEND
										<td $width_attribute>{$this->transform_table_cell_data($record[$i])}</td>
HTMLEND;
								}
								$table_html .= "
									</tr>
								";
							}
						}
						else {
							$table_html .= <<<HTMLEND
									<tr nobr="true" align="center" style="{$this->generate_css_style($table_section['row_text_style'])}">
										<td colspan="{$count($columns)}">{$no_record_msg}</td>
									</tr>
HTMLEND;
						}
						$table_html .= "
								</tbody>
							</table>
						";						
						$output .= $table_html;																	
					}
				}
				else if ($section['section_type'] == 'free_text') {
					$output .= $report_template->assign_values_to_template($section['free_text']['text_template'], $student_info);					
				}
				else if ($section['section_type'] == 'page_break') {
					//$pdf->AddPage();
				}
				else {
					throw new Exception("The section type '{$section['section_type']}' is not supported.");
				}
			}
			
			$output .= $report_template->assign_values_to_template($report_template->get_footer_template(), $student_info);			
			
			// Progress
			$raw_progress++;
			if (is_callable($progress_callback)) call_user_func($progress_callback, $raw_progress / count($student_id_list));
		}
		
		// Progress
		if (!$raw_progress && is_callable($progress_callback)) call_user_func($progress_callback, 1);
							
						
		$tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body");
	  	foreach ($tags_to_strip as $tag)
	  	{
	  		$output = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", "", $output);
	  	}	  		  	
	  	//$output = preg_replace("/(src=)(['\"]?).*(\/file\/official_photo.* ?)/", "$1$2http://{$_SERVER['SERVER_NAME']}$3", $output);	  
	  	//$output = "<HTML>\n";
	  	$final_output = "<HTML xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\">\n";
	  	$final_output .= "<HEAD>\n";
	  	$final_output .= returnHtmlMETA();
	  	$final_output .= "</HEAD>\n";
	  	$final_output .= "<BODY LANG=\"zh-HK\">\n";
	  	$final_output .= $output;
	  	$final_output .= "</BODY>\n";
	  	$final_output .= "</HTML>\n";  														
		return $final_output;		
		//return $pdf->Output(null, 'S');
	}
	
	/**
	 * Transform the data in a table cell:
	 * - Trim the leading and trailing spaces.
	 * - nl2br. (after trim, to prevent new line after the last line)
	 * (Assume that the data has been HTML escaped here? So no need to run htmlspecialchars?)
	 */
	private function transform_table_cell_data($data) {
		return nl2br(trim($data));
	}
	
	/**
	 * Generate CSS style values for the HTML style attribute, given some style information.
	 * @param $style Actually come from StudentReportTemplate, like
	 * 		array(
	 * 			'font_family' => ?,
	 * 			'font_size' => ?,
	 * 			'is_bold' => ?,
	 * 			'is_italic' => ?,
	 * 			'is_underline' => ?,
	 * 			'text_color' => ?,
	 * 			'background_color' => ?
	 * 		)
	 * @return CSS style string, like 'background-color:...; font-size:...; ...'.
	 */
	private function generate_css_style($style) {
		$css = '';
		if ($v = $style['font_family']) $css .= "font-family:$v;";
		if ($v = $style['font_size']) $css .= "font-size:$v;";
		if ($v = $style['is_bold']) $css .= "font-weight:bold;";
		if ($v = $style['is_italic']) $css .= "font-style:italic;";
		if ($v = $style['is_underline']) $css .= "text-decoration:underline;";
		if ($v = $style['text_color']) $css .= "color:$v;";
		if ($v = $style['background_color']) $css .= "background-color:$v;";
		return $css;
	}
}

?>
