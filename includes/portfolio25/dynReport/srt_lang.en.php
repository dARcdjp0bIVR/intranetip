<?php
// Using By : 
// This file will set $srt_lang for use by srt_lang.php.
// Note that this file is NOT included in the global scope, so you may need to global some variables.

// Translation for English is not really needed.
// Set $srt_lang, so that this file will not be included again.
$srt_lang[''] = '';
$srt_lang['Award Date'] = 'Issue Date';
$srt_lang['Class Name'] = 'Class Name(Eng)';
$srt_lang['Class_Name_big5'] = 'Class Name(Chi)';
$srt_lang['DemeritOnly'] = 'Demerit';
$srt_lang['Service'] = 'Service';
$srt_lang['Title'] = 'Programme Name';
$srt_lang['Reason'] = 'Issue Reason';
$srt_lang['Number of Unit'] = 'Amount Received';
$srt_lang['Merit'] = 'Merit & Demerit';
$srt_lang['MeritOnly'] = 'Merit';
$srt_lang['Merit Merit Date'] = 'Merit Date';
$srt_lang['Gender_big5'] = 'Gender(Chi)';
$srt_lang['Gender_en'] = 'Gender(Eng)';
$srt_lang['Gender_F'] = 'F';
$srt_lang['Gender_F_En'] = 'F';
$srt_lang['Gender_F_Big5'] = '女';
$srt_lang['Gender_M'] = 'M';
$srt_lang['Gender_M_En'] = 'M';
$srt_lang['Gender_M_Big5'] = '男';
$srt_lang['IssueDate'] = 'Date of Issue';
$srt_lang['WebSAMS Reg. No.'] = 'WebSAMSRegNo';
$srt_lang['Conduct'] = 'Conduct';
$srt_lang['TeacherCommentChi'] = 'Teacher Comment (Chi)';
$srt_lang['TeacherCommentEng'] = 'Teacher Comment (Eng)';
?>
