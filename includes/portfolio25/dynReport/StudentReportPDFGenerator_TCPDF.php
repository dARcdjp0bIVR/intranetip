<?php
#using:

# modifications:
# Anna (2018-12-17) $sys_custom['iPf']['DynamicReport']['PrintPageNumberFormat']['style2'] #P153733
# Omas (2017-02-16) $sys_custom['iPf']['DynamicReport']['PdfVersion'] - #F112819 
# Ivan (2013-07-10) [2013-0709-1144-36071]: modified function Footer() to fix the wrong footer image size problem 
# Henry Yuen (2010-06-29): change header/ footer/ freetext default font to msungstdlight

// This file may be included through ClassLoader, so need to global some variables.
global $intranet_root;
require_once("$intranet_root/includes/tcpdf/config/lang/eng.php");
require_once("$intranet_root/includes/tcpdf/tcpdf.php");

//require_once("$intranet_root/includes/tcpdf.201007141704/tcpdf.php");

/**
 * Create a sub-class of TCPDF to help customization later.
 */
class StudentReportPDFGenerator_TCPDF extends TCPDF
{
	/**
	 * The header for a student.
	 * Set by set_student_report_header(); Used by Header().
	 */
	protected $student_report_header_html;
	
	/**
	 * Indicate whether to show the header for the student once only.
	 */
	protected $is_to_show_header_once_only;
	
	/**
	 * The footer for a student.
	 * Set by set_student_report_footer(); Used by Footer().
	 */
	protected $student_report_footer_html;
	
	/**
	 * Indicate whether to show the footer for the student once only.
	 */
	protected $is_to_show_footer_once_only;
	
	# Henry Yuen (2010-06-21) 
	protected $header_height;	
	protected $footer_height;
	protected $is_first_page;
	protected $is_last_page;
	
	function __construct($PrintingDirection='') {
		global $sys_custom;
		
		// Just set properties here to simplify StudentReportPDFGenerator.
		
		// $PrintingDirection = 'P' / 'L'
		$PrintingDirection = ($PrintingDirection=='')? PDF_PAGE_ORIENTATION : $PrintingDirection;
		parent::__construct($PrintingDirection, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); 
		//parent::__construct('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//		$default_fontType = "arialunicid0-chi";
//		$default_fontType = "msungstdlight";  // support with chinese , but no arial in eng
//		$default_fontType = "stsongstdlight"; // support with chinese , but no arial in eng
//		$default_fontType = "arialunicid0";
//		$default_fontType = "droidsansfallback";               		         
//		$default_fontType = "droidserif-regular";

		if ($sys_custom['iPf']['DynamicReport']['DefaultFontStyle'] == '') {
			$default_fontType = "droidsansfallback"; 
		}
		else {
			$default_fontType = $sys_custom['iPf']['DynamicReport']['DefaultFontStyle']; 
		}

		// set document information
		if($sys_custom['iPf']['DynamicReport']['PdfVersion']){
			$this->setPDFVersion($sys_custom['iPf']['DynamicReport']['PdfVersion']);
		}
		$this->SetCreator(PDF_CREATOR);
		$this->SetAuthor('BroadLearning.com');
//		$this->SetTitle('TCPDF Example 048');
//		$this->SetSubject('TCPDF Tutorial');
//		$this->SetKeywords('TCPDF, PDF, example, test, guide');
		
		// set default header data
		$this->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		
		// set header and footer fonts
		//$this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		//$this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		//$this->setHeaderFont(Array('arialunicid0-chi', '', PDF_FONT_SIZE_MAIN));
		//$this->setFooterFont(Array('arialunicid0-chi', '', PDF_FONT_SIZE_DATA));
		//$this->setHeaderFont(Array('msungstdlight', '', PDF_FONT_SIZE_MAIN));
		//$this->setFooterFont(Array('msungstdlight', '', PDF_FONT_SIZE_DATA));		
		//$this->setHeaderFont(Array('stsongstdlight', '', PDF_FONT_SIZE_MAIN));
		//$this->setFooterFont(Array('stsongstdlight', '', PDF_FONT_SIZE_DATA));							
		$this->setHeaderFont(Array($default_fontType, '', PDF_FONT_SIZE_MAIN));
		$this->setFooterFont(Array($default_fontType, '', PDF_FONT_SIZE_DATA));		
		
		// set default monospaced font
		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);		
		$this->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
						
		//set image scale factor
		$this->setImageScale(PDF_IMAGE_SCALE_RATIO); 
		
		//set some language-dependent strings
//		$this->setLanguageArray($l); 
		
		// ---------------------------------------------------------
		
		// set font
		//$this->SetFont('helvetica', '', 8);
		//$this->SetFont('msungstdlight', '', 8);
		//$this->SetFont('arialunicid0-chi', '', 8);
		//$this->SetFont('stsongstdlight', '', 8);
		$this->SetFont($default_fontType, '', 8);		
	}
	
	public function AddPage($orientation='', $format='') {		
		parent::AddPage($orientation, $format);
		//$this->SetTopMargin(PDF_MARGIN_TOP); // default top margin
		
		/*
		if($this->is_first_page){
			$this->SetTopMargin($this->header_height);
			$this->set_is_first_page(false);
		}
		elseif(!$this->is_to_show_header_once_only){
			$this->SetTopMargin($this->header_height);
		}
		else{			
			$this->SetTopMargin(PDF_MARGIN_TOP); // default top margin					
		}		
		
		if($this->is_last_page){	
			$this->set_is_last_page(false);			
		}
		*/						
	}
	
	/**
	 * Override the header output function for the need of student report.
	 */
	public function Header() {					
		if($this->is_first_page){
			$this->writeHTML($this->student_report_header_html);
			$this->SetTopMargin($this->header_height);
			$this->set_is_first_page(false);
		}
		elseif(!$this->is_to_show_header_once_only){
			$this->writeHTML($this->student_report_header_html);
			$this->SetTopMargin($this->header_height);
		}
		else{			
			$this->SetTopMargin(PDF_MARGIN_TOP); // default top margin
			//$this->writeHTML('</br>');			
		}				
	}
	
	/**
	 * Override the footer output function for the need of student report.
	 */
	public function Footer() {
		global $sys_custom;
		
		// 2013-0709-1144-36071 - fix incorrect image size in footer
		// Reference: http://sourceforge.net/p/tcpdf/bugs/703/
//		if($this->is_last_page){
//			$this->writeHTML($this->student_report_footer_html);
//			
//			//echo '<br>footer';
//			$this->set_is_last_page(false);																
//		}
//		elseif(!$this->is_to_show_footer_once_only){
//			$this->writeHTML($this->student_report_footer_html);
//		}

 		//2013-1127-1123-34066
 		$this->bMargin = PDF_MARGIN_FOOTER + $this->footer_height;

		if($this->is_last_page || !$this->is_to_show_footer_once_only){
			// get the current page break margin
	        $bMargin = $this->getBreakMargin();
	
	        // get current auto-page-break mode
	        $auto_page_break = $this->AutoPageBreak;
	        // disable auto-page-break
	        $this->SetAutoPageBreak(false, 0);
	        
	        $this->writeHTML($this->student_report_footer_html);
			
			// restore auto-page-break status
        	$this->SetAutoPageBreak($auto_page_break, $bMargin);
			
			// 2013-0828-1211-49156
        	// set the starting point for the page content
        	//$this->setPageMark();
        	
			if ($this->is_last_page) {
				$this->set_is_last_page(false);
			}
		}
		
		if($sys_custom['iPf']['DynamicReport']['PrintPageNumber']){

				// print page number
		        //R140755 - customize page number position
		        //$this->SetY(-10);
		        $pageNumPosition = (isset($sys_custom['iPf']['DynamicReport']['PrintPageNumberPosition']))? $sys_custom['iPf']['DynamicReport']['PrintPageNumberPosition'] : -10;
		        $this->SetY($pageNumPosition);
				$ormargins = $this->getOriginalMargins();

				//Print page number
				if (empty($this->pagegroups)) {
					if($sys_custom['iPf']['DynamicReport']['PrintPageNumberFormat']['style1']){
						$pagenumtxt = $this->l['w_page'].' Page '.$this->getAliasNumPage().' of '.$this->getAliasNbPages();
					}elseif($sys_custom['iPf']['DynamicReport']['PrintPageNumberFormat']['style2']){
					    $pagenumtxt = $this->l['w_page'].' P.'.$this->getAliasNumPage();
					    
					}
					else{
						$pagenumtxt = $this->l['w_page'].' '.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
					}
				} else {

					$pagenumtxt = $this->l['w_page'].' '.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
				}		
					
				if ($this->getRTL()) {
					$this->SetX($ormargins['right']);
					$this->Cell(0, 0, $pagenumtxt, 0, 0, 'L');
				} else {
					$this->SetX($ormargins['left']);
					$this->Cell(0, 0, $pagenumtxt, 0, 0, 'R');
				}
		}
	}
	
	/**
	 * Set the header for student report.
	 * Should call this before Header() is called.
	 */
	 
	# Henry Yuen (2010-06-21): new parameter header height
	public function set_student_report_header($header_html, $is_to_show_header_once_only, $header_height) {
		$this->student_report_header_html = $header_html;
		$this->is_to_show_header_once_only = $is_to_show_header_once_only;
		$this->header_height = $header_height;
	}
	
	/**
	 * Set the footer for student report.
	 * Should call this before Footer() is called.
	 */
	public function set_student_report_footer($footer_html, $is_to_show_footer_once_only, $footer_height) {
		$this->student_report_footer_html = $footer_html;
		$this->is_to_show_footer_once_only = $is_to_show_footer_once_only;
		$this->footer_height = $footer_height;
	}
	
	# Henry Yuen (2010-06-21): functions for setting isFirstPage/ isLastPage for current page
	public function set_is_first_page($is_first_page){		
		$this->is_first_page = $is_first_page;
	}
	public function set_is_last_page($is_last_page){
		if($this->is_to_show_footer_once_only){				
			if($is_last_page){			
				$this->SetAutoPageBreak(TRUE, $this->footer_height);
				$this->SetFooterMargin($this->footer_height + 10);
				$this->writeHTML('</br>');
				//echo '<br>br';										
			}
			else{
				$this->SetAutoPageBreak(TRUE, PDF_MARGIN_FOOTER);
				$this->SetFooterMargin(PDF_MARGIN_FOOTER + 5);
			}													
			$this->is_last_page = $is_last_page;
		}			
	} 
}

?>