<?php
// Using: 

/*
 * Modification Log:
 */

/**
 * Represent a student report template. Provide helper methods to access template data.
 */
class StudentReport_ui extends interface_html {

	function StudentReport_ui(){
		parent::interface_html();
	}
	
//	function Get_DataContent_Criteria_Selection($DataSource, $DataField, $CriteriaNum, $SelectedCriteria='', $OnChange='') 
//	{
//		global $Lang;
//		
//		$CriteriaArr = StudentReportTemplate::get_critieria_options();
//		$numOfCriteriaSection = count($CriteriaArr);
//		$CriteriaNameArr = StudentReportTemplate::get_criteria_options_lang();
//		
//		$SelectionID = 'criteria_sel_'.str_replace(' ', '_', $DataSource).'_'.$DataField.'_'.$CriteriaNum;
//		$SelectionClass = 'criteria_sel_'.str_replace(' ', '_', $DataSource).'_'.$DataField;
//		
//		$x = '';
//		$x .= '<select id="'.$SelectionID.'" class="'.$SelectionClass.'" onchange="'.$OnChange.'">';
//			for ($i=0; $i<$numOfCriteriaSection; $i++)
//			{
//				$thisCriteriaArr = $CriteriaArr[$i];
//				$numOfCriteria = count($thisCriteriaArr);
//				
//				// Only first criteria has the "No Criteria" option
//				if ($CriteriaNum > 1 && $i == 0) {
//					continue;
//				}
//				
//				for ($j=0; $j<$numOfCriteria; $j++)
//				{
//					$thisCriteriaKey = $thisCriteriaArr[$j];
//					$thisCriteriaDisplay = $CriteriaNameArr[$thisCriteriaKey];
//					
//					$thisSelected = ($SelectedCriteria==$thisCriteriaKey)? 'selected' : '';
//					if ($i == 0) {
//						// No Criteria
//						$thisSelected = ($SelectedCriteria=='')? 'selected' : $thisSelected;
//					}					
//					
//					$x .= '<option value="'.$thisCriteriaKey.'" '.$thisSelected.'>'.$thisCriteriaDisplay.'</option>';
//				}
//				
//				if ($i < $numOfCriteriaSection - 1) {
//					$x .= '<option disabled>-----</option>'."\n";
//				}
//			}
//		$x .= '</select>';
//		
//		return $x;	
//	}
//	
//	function Get_DataContent_Criteria_Condition_Selection($DataSource, $DataField, $ConditionNum, $SelectedCondition='', $OnChange='') 
//	{
//		$ConditionArr = StudentReportTemplate::get_criteria_condition_options();
//		
//		$ID = 'condition_sel_'.str_replace(' ', '_', $DataSource).'_'.$DataField.'_'.$ConditionNum;
//		$Class = 'condition_sel_'.str_replace(' ', '_', $DataSource).'_'.$DataField;
//		$selectionTags = ' id="'.$ID.'" name="'.$ID.'" class="'.$Class.'" onchange="'.$OnChange.'" ';
//		
//		return getSelectByAssoArray($ConditionArr, $selectionTags, $SelectedCondition, $isAll=0, $noFirst=0);
//	}
	
	function Get_DataContent_Criteria_Layer()
	{
		global $Lang;
		
		$lpf_ui = new libportfolio_ui();
		$objDataSrc = new StudentReportDataSourceManager();
		
		
		### Get the Columns of each Items which can be filtered by criteria
		$CriteriaDataSourceArr = $objDataSrc->get_criteria_data_sources();
		
		### Get the display name mapping of the Columns
		$data_source_and_field_display_names = $objDataSrc->get_data_source_and_field_display_names();	
		
		
		### Loop each Data Source to create criteria table
		$MainDivClass = 'filter_div';
		$ContentDivClass = 'filter_content_div';



		$x = '';

		foreach ((array)$CriteriaDataSourceArr as $thisDataSource => $thisDataFieldArr) {

			$thisDataSourceDisplay = $data_source_and_field_display_names[$thisDataSource];

			$thisDataSourceWithoutSpace = str_replace(' ', '_', $thisDataSource);
			$thisMainDivID = 'filter_div_'.$thisDataSourceWithoutSpace;
			$thisContentDivID = 'filter_content_div_'.$thisDataSourceWithoutSpace;
			
			$x .= '<div id="'.$thisMainDivID.'" class="'.$MainDivClass.'" style="width:96%;">'."\n";
				$x .= '<div>'."\n";
					$x .= '<span style="float:left;">'.$this->GET_NAVIGATION2_IP25($thisDataSourceDisplay).'</span>'."\n";
					$x .= '<span style="float:left;">&nbsp;&nbsp;</span>'."\n";
					$x .= '<span style="float:left;">'."\n";
						$x .= '<span id="spanShowOption_'.$thisContentDivID.'" style="display:none;">'."\n";
							$x .= $this->Get_Show_Option_Link("javascript:js_Show_Option_Div('".$thisContentDivID."');", "", $Lang['Btn']['Show']);
						$x .= '</span>'."\n";
						$x .= '<span id="spanHideOption_'.$thisContentDivID.'">'."\n";
							$x .= $this->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('".$thisContentDivID."');", "", $Lang['Btn']['Hide']);
						$x .= '</span>'."\n";
					$x .= '</span>'."\n";
				$x .= '</div>'."\n";
				$x .= '<br style="clear:both;" />'."\n";
				
				$x .= '<div id="'.$thisContentDivID.'" class="'.$ContentDivClass.'" data_source="'.$thisDataSource.'">'."\n";
					$x .= '<table class="common_table_list_v30" style="margin:0 auto;">'."\n";
						$x .= '<thead>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<th style="width:30%;">'.$Lang['iPortfolio']['DataColumn'].'</th>'."\n";
								$x .= '<th>'.$Lang['iPortfolio']['DisplayCriteria'].'</th>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</thead>'."\n";
						$x .= '<tbody>'."\n";
						
							foreach ((array)$thisDataFieldArr as $thisDataFieldKey => $thisDataField) {
								$thisLangKey = $thisDataSource.'.'.$thisDataField;


								$thisDataFieldDisplay = $data_source_and_field_display_names[$thisLangKey];

								$thisTrClass = 'criteria_tr_'.$thisDataSourceWithoutSpace;
								$thisDataFieldDisplayDivID = 'criteria_td_div_'.$thisDataSourceWithoutSpace.'_'.$thisDataField;
								$thisDataFieldDisplayBottomDivID = 'criteria_td_bottom_div_'.$thisDataSourceWithoutSpace.'_'.$thisDataField;
														
								$thisCriteriaDisplay = '';
								if ($thisDataField == 'CategoryName') {
									$thisCriteriaDisplay .= $lpf_ui->Get_Category_Checkboxes_Table($thisDataSource.'_');
								}
								else if ($thisDataField == 'ELE') {
									$thisCriteriaDisplay .= $lpf_ui->Get_ELE_Checkboxes_Table($thisDataSource.'_');
								}
								else {
									//$thisCriteriaDisplay .= $this->Get_DataContent_Criteria_Display($thisDataSource, $thisDataField);
									// display nth => generated by JS
								}
								
								$x .= '<tr class="'.$thisTrClass.'" data_field="'.$thisDataField.'">'."\n";
									$x .= '<td>'.$thisDataFieldDisplay.'</td>'."\n";
									$x .= '<td>'."\n";
										$x .= '<div id="'.$thisDataFieldDisplayDivID.'">'."\n";
											$x .= $thisCriteriaDisplay."\n";
											$x .= '<div id="'.$thisDataFieldDisplayBottomDivID.'"></div>'."\n";
										$x .= '</div>'."\n";
									$x .= '</td>'."\n";
								$x .= '</tr>'."\n";
							}
					
						$x .= '</tbody>'."\n";
					$x .= '</table>'."\n";
				$x .= '</div>'."\n";
			$x .= '</div>'."\n";
			$x .= '<br />'."\n";
		}
		
		$x .= '<div id="no_criteria_settings_div" style="width:96%;">'."\n";
			$x .= $Lang['iPortfolio']['DynamicReport']['NoCriteriaSettingsRemarks']."\n";
		$x .= '</div>'."\n";
		
		return $x;
	}
	
//	Generated by JS now
//	private function Get_DataContent_Criteria_Display($DataSource, $DataField)
//	{
//		$DataSource = str_replace(' ', '_', $DataSource);
//		
//		$TbClass = 'CriteriaTb_'.$DataSource.'_'.$DataField;
//		
//		
//		$x = '';
//		$x .= '<div>'."\n";
//			
//			$thisCriteriaSelectionNum = 1;
//			$TbID = 'CriteriaTb_'.$DataSource.'_'.$DataField.'_'.$thisCriteriaSelectionNum;
//			$Tb = '<input type="text" id="'.$TbID.'" class="textboxtext '.$TbClass.'" style="display:none;" value="" />'."\n";
//			
//			$x .= '<div>'."\n";
//				$x .= '<span style="float:left;">'."\n";
//					$thisOnChange = "EditSectionPage.changed_criteria_selection('".$DataSource."', '".$DataField."', '".$thisCriteriaSelectionNum."', this.value);";
//					$x .= $this->Get_DataContent_Criteria_Selection($DataSource, $DataField, $thisCriteriaSelectionNum, '', $thisOnChange);
//				$x .= '</span>'."\n";
//				$x .= '<span style="float:left;width:60%;">'."\n";
//					$x .= $Tb."\n";
//				$x .= '</span>'."\n";
//			$x .= '<div>'."\n";
//			$x .= '<br style="clear:both;" />'."\n";
//			
//			
//			$thisCriteriaSelectionNum = 2;
//			$TbID = 'CriteriaTb_'.$DataSource.'_'.$DataField.'_'.$thisCriteriaSelectionNum;
//			$Tb = '<input type="text" id="'.$TbID.'" class="textboxtext '.$TbClass.'" value="" />'."\n";
//			
//			$thisDivID = 'CriteriaDiv_'.$DataSource.'_'.$DataField.'_'.$thisCriteriaSelectionNum;
//			$x .= '<div id="'.$thisDivID.'" style="display:none;">'."\n";
//			
//				$x .= '<br style="clear:both;" />'."\n";
//				$thisOnChange = "EditSectionPage.changed_criteria_condition_selection('".$DataSource."', '".$DataField."', '".($thisCriteriaSelectionNum)."', this.value);";
//				$x .= $this->Get_DataContent_Criteria_Condition_Selection($DataSource, $DataField, $thisCriteriaSelectionNum, $SelectedCondition='', $thisOnChange)."\n";
//				
//				$thisDivID = 'CriteriaOptionDiv_'.$DataSource.'_'.$DataField.'_'.$thisCriteriaSelectionNum;
//				$x .= '<div id="'.$thisDivID.'" style="display:none;">'."\n";
//					$x .= '<br style="clear:both;" />'."\n";
//					$x .= '<span style="float:left;">'."\n";
//						$thisOnChange = "EditSectionPage.changed_criteria_selection('".$DataSource."', '".$DataField."', '".$thisCriteriaSelectionNum."', this.value);";
//						$x .= $this->Get_DataContent_Criteria_Selection($DataSource, $DataField, $thisCriteriaSelectionNum, '', $thisOnChange);
//					$x .= '</span>'."\n";
//					$x .= '<span style="float:left;width:60%;">'."\n";
//						$x .= $Tb."\n";
//					$x .= '</span>'."\n";
//				$x .= '</div>'."\n";
//			$x .= '</div>'."\n";
//		$x .= '</div>'."\n";
//		
//		return $x;
//	}
}

?>