<?php

include_once($intranet_root."/includes/libdb.php");

class libpf_program_title {

  var $db;
  var $program_title_id;
  var $category_id;
  var $sub_category_id;
  var $chi_title;
  var $eng_title;
  var $record_status;
  var $component;
  
  function __construct(){
    $this->db = new libdb();
  }
  
  function SET_PROGRAM_TITLE_PROPERTY(){
    global $eclass_db;
    
    $program_title_id = $this->GET_CLASS_VARIABLE("program_title_id");
    if($program_title_id == "") return false;
    
    $sql =  "
              SELECT DISTINCT
                ProgramTitleID,
                CategoryID,
                SubCategoryID,
                ChiTitle,
                EngTitle,
                RecordStatus,
                ELE
              FROM
                {$eclass_db}.OLE_PROGRAM_TITLE
              WHERE
                ProgramTitleID = '".$program_title_id."'
            ";

    $prog_title_arr = $this->db->returnArray($sql);
    $prog_title_arr = $prog_title_arr[0];

    if(!empty($prog_title_arr))
    {
      $this->SET_CLASS_VARIABLE("program_title_id", $prog_title_arr["ProgramTitleID"]);
      $this->SET_CLASS_VARIABLE("category_id", $prog_title_arr["CategoryID"]);
      $this->SET_CLASS_VARIABLE("sub_category_id", $prog_title_arr["SubCategoryID"]);
      $this->SET_CLASS_VARIABLE("chi_title", $prog_title_arr["ChiTitle"]);
      $this->SET_CLASS_VARIABLE("eng_title", $prog_title_arr["EngTitle"]);
      $this->SET_CLASS_VARIABLE("record_status", $prog_title_arr["RecordStatus"]);
      $this->SET_CLASS_VARIABLE("component", explode(",", $prog_title_arr["ELE"]));
    }
  }
  
  function GET_PROGRAM_TITLE_LIST()
	{
		global $eclass_db;
		
		$t_category_id = $this->GET_CLASS_VARIABLE("category_id");
		$t_sub_category_id = $this->GET_CLASS_VARIABLE("sub_category_id");

		$sql =  "
              SELECT
                ProgramTitleID,
                CategoryID,
                SubCategoryID,
                ChiTitle,
                EngTitle
              FROM
                {$eclass_db}.OLE_PROGRAM_TITLE
            ";
    if($t_sub_category_id != "")
    {
      $sql .= " WHERE SubCategoryID = '".$t_sub_category_id."'";
    }
		else if($t_category_id != "")
		{
			$sql .= " WHERE CategoryID = '".$t_category_id."'";
		}
		$returnArray = $this->db->returnArray($sql);

		return $returnArray;
	}
	
  function ADD_PROGRAM_TITLE()
  {
    global $eclass_db;
    
    $t_category_id = $this->GET_CLASS_VARIABLE("category_id");
    $t_sub_category_id = $this->GET_CLASS_VARIABLE("sub_category_id");
    $t_sub_category_id = ($t_sub_category_id == "") ? "NULL" : $t_sub_category_id;
    $t_chi_title = $this->GET_CLASS_VARIABLE("chi_title");
    $t_eng_title = $this->GET_CLASS_VARIABLE("eng_title");
    $t_record_status = $this->GET_CLASS_VARIABLE("record_status");
    $t_component = $this->GET_CLASS_VARIABLE("component");
    $t_ele = is_array($t_component) ? implode(",", $t_component) : "";
    
    $fields = "(CategoryID, SubCategoryID, ChiTitle, EngTitle";
    $fields .= ", RecordStatus, ELE, InputDate, ModifiedDate)";
    
    $values = "('".$t_category_id."', ".$t_sub_category_id.", '".$t_chi_title."', '".$t_eng_title."'";
    $values .= ", '".$t_record_status."', '".$t_ele."', NOW(), NOW())";
    
    $sql = "INSERT INTO {$eclass_db}.OLE_PROGRAM_TITLE $fields VALUES $values";

    $this->db->db_db_query($sql);
    $t_programTitleID = $this->db->db_insert_id();
    
    $this->SET_CLASS_VARIABLE("program_title_id", $t_programTitleID);
  }
  
  function EDIT_PROGRAM_TITLE()
  {
    global $eclass_db;
    
    $t_program_title_id = $this->GET_CLASS_VARIABLE("program_title_id");
    if($t_program_title_id == "") return false;
    
    $t_category_id = $this->GET_CLASS_VARIABLE("category_id");
    $t_sub_category_id = $this->GET_CLASS_VARIABLE("sub_category_id");
    $t_sub_category_id = ($t_sub_category_id == "") ? "NULL" : $t_sub_category_id;
    $t_chi_title = $this->GET_CLASS_VARIABLE("chi_title");
    $t_eng_title = $this->GET_CLASS_VARIABLE("eng_title");
    $t_record_status = $this->GET_CLASS_VARIABLE("record_status");
    $t_component = $this->GET_CLASS_VARIABLE("component");
    $t_ele = is_array($t_component) ? implode(",", $t_component) : "";
    
    $field_value = "CategoryID = ".$t_category_id.", ";
    $field_value .= "SubCategoryID = ".$t_sub_category_id.", ";
    $field_value .= "ChiTitle = '".$t_chi_title."', ";
    $field_value .= "EngTitle = '".$t_eng_title."', ";
    $field_value .= "RecordStatus = '".$t_record_status."', ";
    $field_value .= "ELE = '".$t_ele."', ";
    $field_value .= "ModifiedDate = NOW()";
    
    $cond = "ProgramTitleID = '".$t_program_title_id."'";
    
    $sql = "UPDATE {$eclass_db}.OLE_PROGRAM_TITLE SET $field_value WHERE $cond";
    $this->db->db_db_query($sql);
  }

  function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    $this->{$ParVariableName} = $ParValue;
  }
  
  function GET_CLASS_VARIABLE($ParVariableName)
  {
    return $this->{$ParVariableName};
  }
  
##########################################################################
# Batch sub-category functions
##########################################################################  
  function DELETE_PROGRAM_TITLE($ParProgTitleID)
  {
    global $eclass_db;
    
    $progtitle_id_str = is_array($ParProgTitleID) ? implode(",", $ParProgTitleID) : $ParProgTitleID;
    
    $sql = "DELETE FROM {$eclass_db}.OLE_PROGRAM_TITLE WHERE ProgramTitleID IN (".$progtitle_id_str.") AND RecordStatus NOT IN (2,3)";
    
    $returnVal = $this->db->db_db_query($sql);
    
    return $returnVal;
  }
  
  function CHANGE_PROGRAM_TITLE_STATUS($ParProgTitleID, $ParStatus)
  {
    global $eclass_db;
  
    $progtitle_id_str = is_array($ParProgTitleID) ? implode(",", $ParProgTitleID) : $ParProgTitleID;
  
    if($ParStatus == "active")
    {
    	$fields_values = "RecordStatus = '1'";
    	$sql = "UPDATE {$eclass_db}.OLE_PROGRAM_TITLE SET $fields_values WHERE ProgramTitleID IN (".$progtitle_id_str.") AND RecordStatus = '0'";
    	$this->db->db_db_query($sql);
    	$fields_values = "RecordStatus = '2'";
    	$sql = "UPDATE {$eclass_db}.OLE_PROGRAM_TITLE SET $fields_values WHERE ProgramTitleID IN (".$progtitle_id_str.") AND RecordStatus = '3'";
    	$this->db->db_db_query($sql);
    }
    else if($ParStatus == "deactive")
    {
    	$fields_values = "RecordStatus = '0'";
    	$sql = "UPDATE {$eclass_db}.OLE_PROGRAM_TITLE SET $fields_values WHERE ProgramTitleID IN (".$progtitle_id_str.") AND RecordStatus = '1'";
    	$this->db->db_db_query($sql);
    	$fields_values = "RecordStatus = '3'";
    	$sql = "UPDATE {$eclass_db}.OLE_PROGRAM_TITLE SET $fields_values WHERE ProgramTitleID IN (".$progtitle_id_str.") AND RecordStatus = '2'";
    	$this->db->db_db_query($sql);
    }
  }
  
  function MOVE_PROGRAM_TITLE($ParProgTitleID){
    global $eclass_db;
  
    $progtitle_id_str = is_array($ParProgTitleID) ? implode(",", $ParProgTitleID) : $ParProgTitleID;  

    $t_category_id = $this->GET_CLASS_VARIABLE("category_id");
    $t_sub_category_id = $this->GET_CLASS_VARIABLE("sub_category_id");
    $t_sub_category_id = ($t_sub_category_id == "") ? "NULL" : $t_sub_category_id;
    
    $field_value = "CategoryID = ".$t_category_id.", ";
    $field_value .= "SubCategoryID = ".$t_sub_category_id;
  
    $cond = "ProgramTitleID IN (".$progtitle_id_str.")";
    
    $sql = "UPDATE {$eclass_db}.OLE_PROGRAM_TITLE SET $field_value WHERE $cond";
    $this->db->db_db_query($sql);
  }
}

?>