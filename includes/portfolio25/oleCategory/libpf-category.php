<?
###Modifing by:
/**
 * Change Log
 * 
 * Date: 2016-02-05 Kenneth
 * 	- add GET_SUBCATEGORY_LIST()
 *
 */

class Category{

  var $objDB;
  var $category_id;
  var $chi_title;
  var $eng_title;
  var $record_status;
  
  
  function Category(){
  
      $this->objDB = new libdb();
  }
  
  function SET_CATEGORY_PROPERTY()
  {
    global $eclass_db;
    
    $t_category_id = $this->GET_CLASS_VARIABLE("category_id");
    if($t_category_id == "") return false;
    
    $sql =  "
              SELECT DISTINCT
                RecordID,
                ChiTitle,
                EngTitle,
                RecordStatus
              FROM
                {$eclass_db}.OLE_CATEGORY
              WHERE
                RecordID = ".$t_category_id."
            ";

    $db = $this->objDB;
    $returnArray = $db->returnArray($sql);
    $category_arr = $returnArray[0];

    if(!empty($category_arr))
    {
      $this->SET_CLASS_VARIABLE("chi_title", $category_arr["ChiTitle"]);
      $this->SET_CLASS_VARIABLE("eng_title", $category_arr["EngTitle"]);
      $this->SET_CLASS_VARIABLE("record_status", $category_arr["RecordStatus"]);
    }
      
  }
  
  function GET_CATEGORY_LIST($ParShowInactive=false, $orderByField='')
  {
		global $eclass_db;
		
		$conds_RecordStatus = '';
		if(!$ParShowInactive) {
			$conds_RecordStatus = " And (RecordStatus = 1 OR RecordStatus = 2) ";
		}
		
		$orderByField = ($orderByField=='')? 'EngTitle' : $orderByField;

		$sql = "SELECT
						RecordID,
                		ChiTitle,
                		EngTitle,
                		RecordStatus
              	FROM
                		{$eclass_db}.OLE_CATEGORY
				WHERE
						1
						$conds_RecordStatus
				ORDER BY
						$orderByField
            ";
            
//		if(!$ParShowInactive)
//		{
//			$sql .= " WHERE RecordStatus = 1 OR RecordStatus = 2";
//		}
		
		$db = $this->objDB;
		$returnArray = $db->returnArray($sql);
		return $returnArray;
  }
  
  function GET_SUBCATEGORY_LIST($ParShowInactive=false, $orderByField='',$CatID=''){
  			global $eclass_db;
		
		$conds_RecordStatus = '';
		if(!$ParShowInactive) {
			$conds_RecordStatus = " And (subcat.RecordStatus = 1 OR subcat.RecordStatus = 2) ";
		}
		
		$orderByField = ($orderByField=='')? 'EngTitle' : $orderByField;
		
		$conds_CatID = ($CatID=='')?'': " AND CatID = ". $CatID. " ";
		
		$sql = "SELECT
						SubCatID,
						CatID,		
                		subcat.ChiTitle,
                		subcat.EngTitle,
                		cat.ChiTitle AS CategoryChiTitle,
						cat.EngTitle AS CategoryEngTitle,		
                		subcat.RecordStatus
              	FROM
                		{$eclass_db}.OLE_SUBCATEGORY AS subcat
                INNER JOIN 		
                		{$eclass_db}.OLE_CATEGORY AS cat
                ON		subcat.CatID = cat.RecordID						
				WHERE
						1
						$conds_RecordStatus
						$conds_CatID
				ORDER BY
						$orderByField
            ";
		//debug_pr($sql);
		$db = $this->objDB;
		$returnArray = $db->returnArray($sql);
		return $returnArray;
  }
  
  function ADD_CATEGORY()
  {
    global $eclass_db;
    
    $t_chi_title = $this->GET_CLASS_VARIABLE("chi_title");
    $t_eng_title = $this->GET_CLASS_VARIABLE("eng_title");
    $t_record_status = $this->GET_CLASS_VARIABLE("record_status");

    
    $fields = "(ChiTitle, EngTitle, RecordStatus, InputDate, ModifiedDate)";
    $values = "('".$t_chi_title."', '".$t_eng_title."', ".$t_record_status.", NOW(), NOW())";
    
    $sql = "INSERT INTO {$eclass_db}.OLE_CATEGORY $fields VALUES $values";
    $db = $this->objDB;
    $returnVal = $db->db_db_query($sql);
    $t_category_id = $db->db_insert_id();
    $this->SET_CLASS_VARIABLE("category_id", $t_category_id);
    return $returnVal;

  }
  
  function UPDATE_CATEGORY()
  {
    global $eclass_db;
    
    $t_category_id = $this->GET_CLASS_VARIABLE("category_id");
    if($t_category_id == "") return false;
    
    $t_chi_title = $this->GET_CLASS_VARIABLE("chi_title");
    $t_eng_title = $this->GET_CLASS_VARIABLE("eng_title");
    $t_record_status = $this->GET_CLASS_VARIABLE("record_status");

    $fields = " SET 
          ChiTitle = '".$t_chi_title."',
          EngTitle = '".$t_eng_title."',
          RecordStatus = '".$t_record_status."',
          ModifiedDate = NOW() 
    ";
    
    $sql = "UPDATE {$eclass_db}.OLE_CATEGORY $fields WHERE RecordID = '$t_category_id'";
    $db = $this->objDB;
    $returnVal = $db->db_db_query($sql);

    return $returnVal;
  }
  
  function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    $this->{$ParVariableName} = $ParValue;
  }
  
  function GET_CLASS_VARIABLE($ParVariableName)
  {
    return $this->{$ParVariableName};
  }
  
##########################################################################
# Batch category functions
##########################################################################
  function DELETE_CATEGORY($ParCatID)
  {
    global $eclass_db;
    
    $cat_id_str = is_array($ParCatID) ? implode(",", $ParCatID) : $ParCatID;
    
    $sql = "DELETE FROM {$eclass_db}.OLE_CATEGORY WHERE RecordID IN (".$cat_id_str.") AND RecordStatus NOT IN (2,3)";
    
    $db = $this->objDB;          
    $returnVal = $db->db_db_query($sql);
    
    return $returnVal;
  }
  
  function CHANGE_CATEGORY_STATUS($ParCatID, $ParStatus)
  {
    global $eclass_db;
  
    $cat_id_str = is_array($ParCatID) ? implode(",", $ParCatID) : $ParCatID;
    $db = $this->objDB;
  
    if($ParStatus == "active")
    {
    	$fields_values = "RecordStatus = '1'";
    	$sql = "UPDATE {$eclass_db}.OLE_CATEGORY SET $fields_values WHERE RecordID IN (".$cat_id_str.") AND RecordStatus = '0'";
    	$db->db_db_query($sql);
    	$fields_values = "RecordStatus = '2'";
    	$sql = "UPDATE {$eclass_db}.OLE_CATEGORY SET $fields_values WHERE RecordID IN (".$cat_id_str.") AND RecordStatus = '3'";
    	$db->db_db_query($sql);
    }
    else if($ParStatus == "deactive")
    {
    	$fields_values = "RecordStatus = '0'";
    	$sql = "UPDATE {$eclass_db}.OLE_CATEGORY SET $fields_values WHERE RecordID IN (".$cat_id_str.") AND RecordStatus = '1'";
    	$db->db_db_query($sql);
    	$fields_values = "RecordStatus = '3'";
    	$sql = "UPDATE {$eclass_db}.OLE_CATEGORY SET $fields_values WHERE RecordID IN (".$cat_id_str.") AND RecordStatus = '2'";
    	$db->db_db_query($sql);
    }
  }
}

?>