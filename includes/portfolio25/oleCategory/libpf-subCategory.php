<?
class subCategory{

  var $objDB;
  var $subcategory_id;
  var $category_id;
  var $chi_title;
  var $eng_title;
  var $record_status;
  
  
  function subCategory(){
  
      $this->objDB = new libdb();
  }
  
  function SET_SUBCATEGORY_PROPERTY()
  {
    global $eclass_db;
    
    $t_subcategory_id = $this->GET_CLASS_VARIABLE("subcategory_id");
    if($t_subcategory_id == "") return false;
    
    $sql =  "
              SELECT DISTINCT
                SubCatID,
                CatID,
                ChiTitle,
                EngTitle,
                RecordStatus
              FROM
                {$eclass_db}.OLE_SUBCATEGORY
              WHERE
                SubCatID = ".$t_subcategory_id."
            ";

    $db = $this->objDB;
    $returnArray = $db->returnArray($sql);
    $subcategory_arr = $returnArray[0];

    if(!empty($subcategory_arr))
    {
      $this->SET_CLASS_VARIABLE("category_id", $subcategory_arr["CatID"]);
      $this->SET_CLASS_VARIABLE("chi_title", $subcategory_arr["ChiTitle"]);
      $this->SET_CLASS_VARIABLE("eng_title", $subcategory_arr["EngTitle"]);
      $this->SET_CLASS_VARIABLE("record_status", $subcategory_arr["RecordStatus"]);
    }
      
  }
  
  function GET_SUBCATEGORY_LIST($ParShowInactive=false)
  {
		global $eclass_db;

    $t_cat_id = $this->GET_CLASS_VARIABLE("category_id");

		$sql =  "
              SELECT
                SubCatID,
                CatID,
                ChiTitle,
                EngTitle,
                RecordStatus
              FROM
                {$eclass_db}.OLE_SUBCATEGORY
              WHERE
                1
            ";
            
		if(!$ParShowInactive)
		{
			$sql .= " AND (RecordStatus = 1 OR RecordStatus = 2)";
		}
		if($t_cat_id != "")
		{
			$sql .= " AND CatID = ".$t_cat_id;
		}

		$db = $this->objDB;
		$returnArray = $db->returnArray($sql);

		return $returnArray;
  }
  
  function ADD_SUBCATEGORY()
  {
    global $eclass_db;
    
    $t_category_id = $this->GET_CLASS_VARIABLE("category_id");
    $t_chi_title = $this->GET_CLASS_VARIABLE("chi_title");
    $t_eng_title = $this->GET_CLASS_VARIABLE("eng_title");
    $t_record_status = $this->GET_CLASS_VARIABLE("record_status");

    
    $fields = "(CatID, ChiTitle, EngTitle, RecordStatus, InputDate, ModifiedDate)";
    $values = "('".$t_category_id."', '".$t_chi_title."', '".$t_eng_title."', ".$t_record_status.", NOW(), NOW())";
    
    $sql = "INSERT INTO {$eclass_db}.OLE_SUBCATEGORY $fields VALUES $values";
    $db = $this->objDB;
    $returnVal = $db->db_db_query($sql);
    $t_subcategory_id = $db->db_insert_id();
    
    $this->SET_CLASS_VARIABLE("subcategory_id", $t_subcategory_id);
    return $returnVal;

  }
  
  function UPDATE_SUBCATEGORY()
  {
    global $eclass_db;
    
    $t_subcategory_id = $this->GET_CLASS_VARIABLE("subcategory_id");
    if($t_subcategory_id == "") return false;
    
    $t_category_id = $this->GET_CLASS_VARIABLE("category_id");
    $t_chi_title = $this->GET_CLASS_VARIABLE("chi_title");
    $t_eng_title = $this->GET_CLASS_VARIABLE("eng_title");
    $t_record_status = $this->GET_CLASS_VARIABLE("record_status");

    $fields = " SET 
          CatID = '".$t_category_id."',
          ChiTitle = '".$t_chi_title."',
          EngTitle = '".$t_eng_title."',
          RecordStatus = '".$t_record_status."',
          ModifiedDate = NOW() 
    ";
    
    $sql = "UPDATE {$eclass_db}.OLE_SUBCATEGORY $fields WHERE SubCatID = '$t_subcategory_id'";
    $db = $this->objDB;
    $returnVal = $db->db_db_query($sql);

    return $returnVal;

  }
    
  function SET_CLASS_VARIABLE($ParVariableName, $ParValue)
  {
    $this->{$ParVariableName} = $ParValue;
  }
  
  function GET_CLASS_VARIABLE($ParVariableName)
  {
    return $this->{$ParVariableName};
  }

##########################################################################
# Batch sub-category functions
##########################################################################  
  function DELETE_SUBCATEGORY($ParSubCatID)
  {
    global $eclass_db;
    
    $subcat_id_str = is_array($ParSubCatID) ? implode(",", $ParSubCatID) : $ParSubCatID;
    
    $sql = "DELETE FROM {$eclass_db}.OLE_SUBCATEGORY WHERE SubCatID IN (".$subcat_id_str.") AND RecordStatus NOT IN (2,3)";
    
    $db = $this->objDB;          
    $returnVal = $db->db_db_query($sql);
    
    return $returnVal;
  }
  
  function CHANGE_SUBCATEGORY_STATUS($ParSubCatID, $ParStatus)
  {
    global $eclass_db;
  
    $subcat_id_str = is_array($ParSubCatID) ? implode(",", $ParSubCatID) : $ParSubCatID;
    $db = $this->objDB;
  
    if($ParStatus == "active")
    {
    	$fields_values = "RecordStatus = '1'";
    	$sql = "UPDATE {$eclass_db}.OLE_SUBCATEGORY SET $fields_values WHERE SubCatID IN (".$subcat_id_str.") AND RecordStatus = '0'";
    	$db->db_db_query($sql);
    	$fields_values = "RecordStatus = '2'";
    	$sql = "UPDATE {$eclass_db}.OLE_SUBCATEGORY SET $fields_values WHERE SubCatID IN (".$subcat_id_str.") AND RecordStatus = '3'";
    	$db->db_db_query($sql);
    }
    else if($ParStatus == "deactive")
    {
    	$fields_values = "RecordStatus = '0'";
    	$sql = "UPDATE {$eclass_db}.OLE_SUBCATEGORY SET $fields_values WHERE SubCatID IN (".$subcat_id_str.") AND RecordStatus = '1'";
    	$db->db_db_query($sql);
    	$fields_values = "RecordStatus = '3'";
    	$sql = "UPDATE {$eclass_db}.OLE_SUBCATEGORY SET $fields_values WHERE SubCatID IN (".$subcat_id_str.") AND RecordStatus = '2'";
    	$db->db_db_query($sql);
    }
  }
}

?>