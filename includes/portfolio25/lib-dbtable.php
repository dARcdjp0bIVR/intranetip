<?php

include_once($intranet_root."/includes/libdbtable.php");
include_once($intranet_root."/includes/libdbtable2007a.php");

class libpf_dbtable extends libdbtable2007{

  # override function
  function displayPlain(){
    $i=0;
    $row_valign = ($this->row_valign!="") ? "valign='".$this->row_valign."'" : "";
    $this->n_start=($this->pageNo-1)*$this->page_size;
    $this->rs=$this->db_db_query($this->built_sql());
    
    $total_col = ($this->noNumber) ? $this->no_col : $this->no_col-1;
    $bg_style1 = ($this->row_alt[0]!="") ? "bgcolor='".$this->row_alt[0]."' class='tabletext'" : "";
    $bg_style2 = ($this->row_alt[1]!="") ? "bgcolor='".$this->row_alt[1]."' class='tabletext'" : "";
    
    $x = ($this->table_tag!="") ? $this->table_tag : "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
    $x .= $this->displayPlainColumn(1);
  
    if ($this->db_num_rows()==0)
    {
      $x.="<tr><td $bg_style1 align=center colspan=".$this->no_col." height='80'>".$this->no_msg."</td></tr>\n";
    } else
    {
      while($row = $this->db_fetch_array())
      {
        $i++;
        if($i>$this->page_size) break;
        $x.="<tr>\n";
        $bg_style_row = ($i%2) ? $bg_style1 : $bg_style2;
        $x.= ($this->noNumber) ? "" : "<td $bg_style_row $row_valign align='center'>".($this->n_start+$i)."&nbsp;</td>\n" ;
        for($j=0; $j<$total_col; $j++){
          $t = $row[$j];
          $x .= $this->displayPlainCell($j, $t, $bg_style_row, 1);
        }
        $x .= "</tr>\n";
      }
    }
    $x .= "</table>\n";
    if ($this->db_num_rows()>0)
    {
      $this->navigationHTML = $this->navigation(1);
    }
    
    $this->db_free_result();
    return $x;
  }

}

?>