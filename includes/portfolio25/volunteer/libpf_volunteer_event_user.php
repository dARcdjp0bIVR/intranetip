<?
# modifying :  

if (!defined("LIBPF_VOLUNTEER_EVENT_USER_DEFINED"))         // Preprocessor directives
{
  define("LIBPF_VOLUNTEER_EVENT_USER_DEFINED",true);

	class libpf_volunteer_event_user {
		private $objDB;
		private $recordID;
		private $TableName;
		private $ID_FieldName;
		
		private $UserID;
		private $EventID;
		private $Hours;
		private $Role;
		private $Achievement;
		private $TeacherPIC;
		private $InputDate;
		private $InputBy;
		private $ComeFrom;
		private $ModifiedDate;
		private $ModifiedBy;
		private $ApproveStatus;
		private $ApprovedBy;
		private $ProcessDate;
		private $RemoveStatus;
		private $RemoveDate;
		private $RemovedBy;	
				
		
		public function __construct($recordID=null) {
			global $eclass_db, $ipf_cfg;
			
			$this->objDB = new libdb();
			
			$this->TableName = $eclass_db.'.VOLUNTEER_EVENT_USER';
			$this->ID_FieldName = 'RecordID';
			
			if($recordID != null && $recordID != '') {
				$this->recordID = intval($recordID);
				$this->loadRecordFromStorage();
			}
		}
		
				
		
		public function getObjectTableName(){
			return $this->TableName;
		}
		public function getObjectIdFieldName(){
			return $this->ID_FieldName;
		}		
		public function getObjectID(){
			return $this->recordID;
		}
		
		public function getUserID() {
			return $this->UserID;
		}
		public function setUserID($value) {
			$this->UserID = $value;
		}
		
		public function getEventID() {
			return $this->EventID;
		}
		public function setEventID($value) {
			$this->EventID = $value;
		}
		
		public function getHours() {
			return $this->Hours;
		}
		public function setHours($value) {
			$this->Hours = $value;
		}
		
		public function getRole() {
			return $this->Role;
		}
		public function setRole($value) {
			$this->Role = $value;
		}
		
		public function getAchievement() {
			return $this->Achievement;
		}
		public function setAchievement($value) {
			$this->Achievement = $value;
		}
		
		public function getTeacherPIC() {
			return $this->TeacherPIC;
		}
		public function setTeacherPIC($value) {
			$this->TeacherPIC = $value;
		}
		
		public function getInputDate() {
			return $this->InputDate;
		}
		public function setInputDate($value) {
			$this->InputDate = $value;
		}
		
		public function getInputBy() {
			return $this->InputBy;
		}
		public function setInputBy($value) {
			$this->InputBy = $value;
		}
		
		public function getComeFrom() {
			return $this->ComeFrom;
		}
		public function setComeFrom($value) {
			$this->ComeFrom = $value;
		}
		
		public function getModifiedDate() {
			return $this->ModifiedDate;
		}
		public function setModifiedDate($value) {
			$this->ModifiedDate = $value;
		}
		
		public function getModifiedBy() {
			return $this->ModifiedBy;
		}
		public function setModifiedBy($value) {
			$this->ModifiedBy = $value;
		}
		
		public function getApproveStatus() {
			return $this->ApproveStatus;
		}
		public function setApproveStatus($value) {
			$this->ApproveStatus = $value;
		}
		
		public function getApprovedBy() {
			return $this->ApprovedBy;
		}
		public function setApprovedBy($value) {
			$this->ApprovedBy = $value;
		}
		
		public function getProcessDate() {
			return $this->ProcessDate;
		}
		public function setProcessDate($value) {
			$this->ProcessDate = $value;
		}
		
		public function getRemoveStatus() {
			return $this->RemoveStatus;
		}
		public function setRemoveStatus($value) {
			$this->RemoveStatus = $value;
		}
		
		public function getRemoveDate() {
			return $this->RemoveDate;
		}
		public function setRemoveDate($value) {
			$this->RemoveDate = $value;
		}
		
		public function getRemovedBy() {
			return $this->RemovedBy;
		}
		public function setRemovedBy($value) {
			$this->RemovedBy = $value;
		}
		
		
		private function loadRecordFromStorage(){
			global $eclass_db;			
			
			$ObjRecordID = $this->getObjectID();
			if( (trim($ObjRecordID) == "") || (intval($ObjRecordID) < 0)) {
				//DO NOTHING
			} else {
				$ObjTable = $this->getObjectTableName();
				$ObjIdFieldName = $this->getObjectIdFieldName();
				$sql = "Select		
								RecordID,
								UserID,
								EventID,
								Hours,
								Role,
								Achievement,
								TeacherPIC,
								InputDate,
								InputBy,
								ComeFrom,
								ModifiedDate,
								ModifiedBy,
								ApproveStatus,
								ApprovedBy,
								ProcessDate,
								RemoveStatus,
								RemoveDate,
								RemovedBy
						From 
								$ObjTable
						Where 
								$ObjIdFieldName = '".$ObjRecordID."'
						";
				$resultSet = $this->objDB->returnArray($sql, null, $ResultArrayType=1);
				$infoArr = $resultSet[0];
				
				foreach ((array)$infoArr as $_key => $_value) {
					$this->{$_key} = $_value;
				}
			}
		}
		
		public function save($UpdateLastModified=1){
			$ObjRecordID = $this->getObjectID();
			
			if( (trim($ObjRecordID) != "") && (intval($ObjRecordID) > 0) ) {
				$resultID = $this->update_record($UpdateLastModified);
			}
			else{	
				$resultID = $this->new_record();
			}

			return $resultID;
		}
		
		private function new_record(){
			global $lpf_volunteer;
			
			# set Object value
			$this->setInputDate('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setModifiedDate('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			$this->setRemoveStatus($lpf_volunteer->getEventUserRemoveStatusCode_Active());

			# Prepare value for SQL update
			$DataArr = array();
			$DataArr["UserID"]				= $this->objDB->pack_value($this->getUserID(), "int");
			$DataArr["EventID"]				= $this->objDB->pack_value($this->getEventID(), "int");
			$DataArr["Hours"]				= $this->objDB->pack_value($this->getHours(), "int");
			$DataArr["Role"]				= $this->objDB->pack_value($this->getRole(), "str");
			$DataArr["Achievement"]			= $this->objDB->pack_value($this->getAchievement(), "str");
			$DataArr["TeacherPIC"]			= $this->objDB->pack_value($this->getTeacherPIC(), "int");
			$DataArr["InputDate"]			= $this->objDB->pack_value($this->getInputDate(), "date");
			$DataArr["InputBy"]				= $this->objDB->pack_value($this->getInputBy(), "int");
			$DataArr["ComeFrom"]			= $this->objDB->pack_value($this->getComeFrom(), "int");
			$DataArr["ModifiedDate"]		= $this->objDB->pack_value($this->getModifiedDate(), "date");
			$DataArr["ModifiedBy"]			= $this->objDB->pack_value($this->getModifiedBy(), "int");
			$DataArr["ApproveStatus"]		= $this->objDB->pack_value($this->getApproveStatus(), "int");
			$DataArr["ApprovedBy"]			= $this->objDB->pack_value($this->getApprovedBy(), "int");
			$DataArr["ProcessDate"]			= $this->objDB->pack_value($this->getProcessDate(), "date");
			$DataArr["RemoveStatus"]		= $this->objDB->pack_value($this->getRemoveStatus(), "str");
			$DataArr["RemoveDate"]			= $this->objDB->pack_value($this->getRemoveDate(), "date");
			$DataArr["RemovedBy"]			= $this->objDB->pack_value($this->getRemovedBy(), "int");
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value) {
				$fieldArr[] = $field;
				$valueArr[] = $value;
			}
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$ObjTable = $this->getObjectTableName();
			$sql = "Insert Into $ObjTable ($fieldText) Values ($valueText)";
			$success = $this->objDB->db_db_query($sql);
						
			$RecordID = $this->objDB->db_insert_id();
			$this->recordID = $RecordID;
			
			$this->loadRecordFromStorage();
			return $RecordID;
		}
		
		private function update_record($UpdateLastModified=1) {
			global $eclass_db;
			
			# Set object value
			if ($UpdateLastModified) {
				$this->setModifiedDate('now()');
				$this->setModifiedBy($_SESSION['UserID']);
			}
			
			# Prepare value for SQL update
			$DataArr = array();
			$DataArr["UserID"]				= $this->objDB->pack_value($this->getUserID(), "int");
			$DataArr["EventID"]				= $this->objDB->pack_value($this->getEventID(), "int");
			$DataArr["Hours"]				= $this->objDB->pack_value($this->getHours(), "int");
			$DataArr["Role"]				= $this->objDB->pack_value($this->getRole(), "str");
			$DataArr["Achievement"]			= $this->objDB->pack_value($this->getAchievement(), "str");
			$DataArr["TeacherPIC"]			= $this->objDB->pack_value($this->getTeacherPIC(), "int");
			$DataArr["InputDate"]			= $this->objDB->pack_value($this->getInputDate(), "date");
			$DataArr["InputBy"]				= $this->objDB->pack_value($this->getInputBy(), "int");
			$DataArr["ComeFrom"]			= $this->objDB->pack_value($this->getComeFrom(), "int");
			$DataArr["ModifiedDate"]		= $this->objDB->pack_value($this->getModifiedDate(), "date");
			$DataArr["ModifiedBy"]			= $this->objDB->pack_value($this->getModifiedBy(), "int");
			$DataArr["ApproveStatus"]		= $this->objDB->pack_value($this->getApproveStatus(), "int");
			$DataArr["ApprovedBy"]			= $this->objDB->pack_value($this->getApprovedBy(), "int");
			$DataArr["ProcessDate"]			= $this->objDB->pack_value($this->getProcessDate(), "date");
			$DataArr["RemoveStatus"]		= $this->objDB->pack_value($this->getRemoveStatus(), "str");
			$DataArr["RemoveDate"]			= $this->objDB->pack_value($this->getRemoveDate(), "date");
			$DataArr["RemovedBy"]			= $this->objDB->pack_value($this->getRemovedBy(), "int");
			
			# Build field update values string
			$valueFieldArr = array();
			foreach ($DataArr as $_field => $_value) {
				$valueFieldArr[] = " $_field = $_value ";
			}
			$valueFieldText .= implode(',', $valueFieldArr);
			
			$ObjRecordID = $this->getObjectID();
			$ObjTable = $this->getObjectTableName();
			$ObjIdFieldName = $this->getObjectIdFieldName();
			$sql = "Update $ObjTable Set $valueFieldText Where $ObjIdFieldName = '".$ObjRecordID."'";
			$success = $this->objDB->db_db_query($sql);
			
			$this->loadRecordFromStorage();
			return $ObjRecordID;
		}
		
		public function delete_record() {
			global $lpf_volunteer;
			
			$this->setRemoveStatus($lpf_volunteer->getEventUserRemoveStatusCode_Deleted());
			$this->setRemoveDate('now()');
			$this->setRemovedBy($_SESSION['UserID']);
			
			return $this->save();
		}
		
		public function approve_record() {
			global $lpf_volunteer;
			
			$this->setApproveStatus($lpf_volunteer->getEventUserApproveStatusCode_Approved());
			$this->setApprovedBy($_SESSION['UserID']);
			$this->setProcessDate('now()');
			
			return $this->save($UpdateLastModified=0);
		}
		
		public function reject_record() {
			global $lpf_volunteer;
			
			$this->setApproveStatus($lpf_volunteer->getEventUserApproveStatusCode_Rejected());
			$this->setApprovedBy($_SESSION['UserID']);
			$this->setProcessDate('now()');
			
			return $this->save($UpdateLastModified=0);
		}
	}
}
?>