<?
# modifying :  

if (!defined("LIBPF_VOLUNTEER_EVENT_DEFINED"))         // Preprocessor directives
{
  define("LIBPF_VOLUNTEER_EVENT_DEFINED",true);

	class libpf_volunteer_event {
		private $objDB;
		private $recordID;
		private $TableName;
		private $ID_FieldName;
		
		private $EventID;
		private $Code;
		private $Title;
		private $StartDate;
		private $Organization;
		private $AcademicYearID;
		private $YearTermID;
		private $ComeFrom;
		private $InputDate;
		private $CreatorID;
		private $CreatorType;
		private $ModifiedDate;
		private $ModifyBy;
		private $RemoveStatus;
		private $RemoveDate;
		private $RemovedBy;	
				
		
		public function __construct($recordID=null) {
			global $eclass_db, $ipf_cfg;
			
			$this->objDB = new libdb();
			
			$this->TableName = $eclass_db.'.VOLUNTEER_EVENT';
			$this->ID_FieldName = 'EventID';
			
			if($recordID != null && $recordID != '') {
				$this->recordID = intval($recordID);
				$this->loadRecordFromStorage();
			}
		}
		
				
		
		public function getObjectTableName(){
			return $this->TableName;
		}
		public function getObjectIdFieldName(){
			return $this->ID_FieldName;
		}		
		public function getObjectID(){
			return $this->recordID;
		}
		
		public function getCode() {
			return $this->Code;
		}
		public function setCode($value) {
			$this->Code = $value;
		}
		
		public function getTitle() {
			return $this->Title;
		}
		public function setTitle($value) {
			$this->Title = $value;
		}
		
		public function getStartDate() {
			return $this->StartDate;
		}
		public function setStartDate($value) {
			$this->StartDate = $value;
		}
		
		public function getOrganization() {
			return $this->Organization;
		}
		public function setOrganization($value) {
			$this->Organization = $value;
		}
		
		public function getAcademicYearID() {
			return $this->AcademicYearID;
		}
		public function setAcademicYearID($value) {
			$this->AcademicYearID = $value;
		}
		
		public function getYearTermID() {
			return $this->YearTermID;
		}
		public function setYearTermID($value) {
			$this->YearTermID = $value;
		}
		
		public function getComeFrom() {
			return $this->ComeFrom;
		}
		public function setComeFrom($value) {
			$this->ComeFrom = $value;
		}
		
		public function getInputDate() {
			return $this->InputDate;
		}
		public function setInputDate($value) {
			$this->InputDate = $value;
		}
		
		public function getCreatorID() {
			return $this->CreatorID;
		}
		public function setCreatorID($value) {
			$this->CreatorID = $value;
		}
		
		public function getCreatorType() {
			return $this->CreatorType;
		}
		public function setCreatorType($value) {
			$this->CreatorType = $value;
		}
		
		public function getModifiedDate() {
			return $this->ModifiedDate;
		}
		public function setModifiedDate($value) {
			$this->ModifiedDate = $value;
		}
		
		public function getModifyBy() {
			return $this->ModifyBy;
		}
		public function setModifyBy($value) {
			$this->ModifyBy = $value;
		}
		
		public function getRemoveStatus() {
			return $this->RemoveStatus;
		}
		public function setRemoveStatus($value) {
			$this->RemoveStatus = $value;
		}
		
		public function getRemoveDate() {
			return $this->RemoveDate;
		}
		public function setRemoveDate($value) {
			$this->RemoveDate = $value;
		}
		
		public function getRemovedBy() {
			return $this->RemovedBy;
		}
		public function setRemovedBy($value) {
			$this->RemovedBy = $value;
		}
		
		
		private function loadRecordFromStorage(){
			global $eclass_db;			
			
			$ObjRecordID = $this->getObjectID();
			if( (trim($ObjRecordID) == "") || (intval($ObjRecordID) < 0)) {
				//DO NOTHING
			} else {
				$ObjTable = $this->getObjectTableName();
				$ObjIdFieldName = $this->getObjectIdFieldName();
				$sql = "Select		
								EventID,
								Code,
								Title,
								StartDate,
								Organization,
								Details,
								Remark,
								InputDate,
								ModifiedDate,
								ModifyBy,
								CreatorID,
								CreatorType,
								AcademicYearID,
								YearTermID,
								ComeFrom,
								RemoveStatus,
								RemoveDate,
								RemovedBy
						From 
								$ObjTable
						Where 
								$ObjIdFieldName = '".$ObjRecordID."'
						";
				$resultSet = $this->objDB->returnArray($sql, null, $ResultArrayType=1);
				$infoArr = $resultSet[0];
				
				foreach ((array)$infoArr as $_key => $_value) {
					$this->{$_key} = $_value;
				}
			}
		}
		
		public function save(){
			$ObjRecordID = $this->getObjectID();
			
			if( (trim($ObjRecordID) != "") && (intval($ObjRecordID) > 0) ) {
				$resultID = $this->update_record();
			}
			else{	
				$resultID = $this->new_record();
			}

			return $resultID;
		}
		
		private function new_record(){
			global $lpf_volunteer;
			
			# set Object value
			$AcademicYearInfoArr = getAcademicYearAndYearTermByDate($this->getStartDate());
			$this->setAcademicYearID($AcademicYearInfoArr['AcademicYearID']);
			$this->setYearTermID($AcademicYearInfoArr['YearTermID']);
			
			$this->setInputDate('now()');
			$this->setCreatorID($_SESSION['UserID']);
			$this->setCreatorType($this->getCreatorTypeByUserID($_SESSION['UserID']));
			$this->setModifiedDate('now()');
			$this->setModifyBy($_SESSION['UserID']);
			$this->setRemoveStatus($lpf_volunteer->getEventRemoveStatusCode_Active());

			# Prepare value for SQL update
			$DataArr = array();
			$DataArr["Code"]				= $this->objDB->pack_value($this->getCode(), "str");
			$DataArr["Title"]				= $this->objDB->pack_value($this->getTitle(), "str");
			$DataArr["StartDate"]			= $this->objDB->pack_value($this->getStartDate(), "date");
			$DataArr["Organization"]		= $this->objDB->pack_value($this->getOrganization(), "str");
			$DataArr["AcademicYearID"]		= $this->objDB->pack_value($this->getAcademicYearID(), "int");
			$DataArr["YearTermID"]			= $this->objDB->pack_value($this->getYearTermID(), "int");
			$DataArr["ComeFrom"]			= $this->objDB->pack_value($this->getComeFrom(), "int");
			$DataArr["InputDate"]			= $this->objDB->pack_value($this->getInputDate(), "date");
			$DataArr["CreatorID"]			= $this->objDB->pack_value($this->getCreatorID(), "int");
			$DataArr["CreatorType"]			= $this->objDB->pack_value($this->getCreatorType(), "str");
			$DataArr["ModifiedDate"]		= $this->objDB->pack_value($this->getModifiedDate(), "date");
			$DataArr["ModifyBy"]			= $this->objDB->pack_value($this->getModifyBy(), "int");
			$DataArr["RemoveStatus"]		= $this->objDB->pack_value($this->getRemoveStatus(), "str");
			$DataArr["RemoveDate"]			= $this->objDB->pack_value($this->getRemoveDate(), "date");
			$DataArr["RemovedBy"]			= $this->objDB->pack_value($this->getRemovedBy(), "int");
			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = $value;
			}
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$ObjTable = $this->getObjectTableName();
			$sql = "Insert Into $ObjTable ($fieldText) Values ($valueText)";
			$success = $this->objDB->db_db_query($sql);
						
			$RecordID = $this->objDB->db_insert_id();
			$this->recordID = $RecordID;
			
			$this->loadRecordFromStorage();
			return $RecordID;
		}
		
		private function update_record() {
			global $eclass_db;
			
			# Set object value
			$this->setModifiedDate('now()');
			$this->setModifyBy($_SESSION['UserID']);
			
			# Prepare value for SQL update
			$DataArr = array();
			$DataArr["Code"]				= $this->objDB->pack_value($this->getCode(), "str");
			$DataArr["Title"]				= $this->objDB->pack_value($this->getTitle(), "str");
			$DataArr["StartDate"]			= $this->objDB->pack_value($this->getStartDate(), "date");
			$DataArr["Organization"]		= $this->objDB->pack_value($this->getOrganization(), "str");
			$DataArr["AcademicYearID"]		= $this->objDB->pack_value($this->getAcademicYearID(), "int");
			$DataArr["YearTermID"]			= $this->objDB->pack_value($this->getYearTermID(), "int");
			$DataArr["ComeFrom"]			= $this->objDB->pack_value($this->getComeFrom(), "int");
			$DataArr["InputDate"]			= $this->objDB->pack_value($this->getInputDate(), "date");
			$DataArr["CreatorID"]			= $this->objDB->pack_value($this->getCreatorID(), "int");
			$DataArr["CreatorType"]			= $this->objDB->pack_value($this->getCreatorType(), "str");
			$DataArr["ModifiedDate"]		= $this->objDB->pack_value($this->getModifiedDate(), "date");
			$DataArr["ModifyBy"]			= $this->objDB->pack_value($this->getModifyBy(), "int");
			$DataArr["RemoveStatus"]		= $this->objDB->pack_value($this->getRemoveStatus(), "str");
			$DataArr["RemoveDate"]			= $this->objDB->pack_value($this->getRemoveDate(), "date");
			$DataArr["RemovedBy"]			= $this->objDB->pack_value($this->getRemovedBy(), "int");
			
			# Build field update values string
			$valueFieldArr = array();
			foreach ($DataArr as $_field => $_value)
			{
				$valueFieldArr[] = " $_field = $_value ";
			}
			$valueFieldText .= implode(',', $valueFieldArr);
			
			$ObjRecordID = $this->getObjectID();
			$ObjTable = $this->getObjectTableName();
			$ObjIdFieldName = $this->getObjectIdFieldName();
			$sql = "Update $ObjTable Set $valueFieldText Where $ObjIdFieldName = '".$ObjRecordID."'";
			$success = $this->objDB->db_db_query($sql);
			
			$this->loadRecordFromStorage();
			return $ObjRecordID;
		}
		
		public function delete_record() {
			global $lpf_volunteer;
			
			$this->setRemoveStatus($lpf_volunteer->getEventRemoveStatusCode_Deleted());
			$this->setRemoveDate('now()');
			$this->setRemovedBy($_SESSION['UserID']);
			
			return $this->save();
		}
		
		public function getCreatorTypeByUserID($ParUserID) {
			global $lpf_volunteer;
			
			$objUser = new libuser($ParUserID);
			$UserType = $objUser->RecordType;
			
			switch ($UserType) {
				case USERTYPE_STAFF:
						$CreatorType = $lpf_volunteer->getEventCreatorTypeCode_Teacher();
						break;
				case USERTYPE_STUDENT:
						$CreatorType = $lpf_volunteer->getEventCreatorTypeCode_Student();
						break;
				case USERTYPE_PARENT:
						$CreatorType = $lpf_volunteer->getEventCreatorTypeCode_Parent();
						break;
				default:
						$CreatorType = '';
			}
			
			return $CreatorType;
		}
	}
}
?>