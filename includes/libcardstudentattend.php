<?php
if (!defined("LIBCARDSTUDENTATTEND_DEFINED"))                  // Preprocessor directives
{
        define("LIBCARDSTUDENTATTEND_DEFINED",true);

        class libcardstudentattend extends libclass {
                var $file_array;
                var $word_array;
                var $word_base_dir;

                function libcardstudentattend()
                {
                        $this->libclass();

                        # Make word templates array
                        global $intranet_root;
                        global $i_StudentAttendance_PresetWord_CardSite,
                               $i_StudentAttendance_PresetWord_OutingFromWhere,
                               $i_StudentAttendance_PresetWord_OutingLocation,
                               $i_StudentAttendance_PresetWord_OutingObjective,
                               $i_StudentAttendance_PresetWord_DetentionLocation,
                               $i_StudentAttendance_PresetWord_DetentionReason;
                        $this->file_array = array("site.txt",
                               "outing_from.txt",
                               "outing_loc.txt",
                               "outing_obj.txt",
                               "detent_loc.txt",
                               "detent_reason.txt"
                               );
                        $this->word_array = array($i_StudentAttendance_PresetWord_CardSite,
                               $i_StudentAttendance_PresetWord_OutingFromWhere,
                               $i_StudentAttendance_PresetWord_OutingLocation,
                               $i_StudentAttendance_PresetWord_OutingObjective,
                               $i_StudentAttendance_PresetWord_DetentionLocation,
                               $i_StudentAttendance_PresetWord_DetentionReason);
                        $this->word_base_dir = "$intranet_root/file/cardword/";
                }

                function getCardIDByStudentID($StudentID)
                {
                        $sql = "SELECT CardID FROM INTRANET_USER WHERE UserID = '$StudentID'";
                        $temp = $this->returnVector($sql);
                        return $temp[0];
                }

                // Query the first log time is card id is provided, otherwise first figure out the card id by student id
                function getCurrentDayFirstLogByID($CardID = null, $StudentID = null)
                {
                        if (is_null($CardID)) {
                                $CardID = $this->getCardIDByStudentID($StudentID);
                        }

                        if (!empty($CardID)) {
                                $sql = "SELECT DATE_FORMAT(MIN(RecordedTime),'%H:%i:%s') FROM CARD_STUDENT_LOG WHERE CardID = '$CardID' AND RecordedTime >= CURDATE()";
                                $temp = $this->returnVector($sql);
                                return $temp[0];
                        }

                        return;
                }

                // Query the last log time is card id is provided, otherwise first figure out the card id by student id
                function getCurrentDayLastLogByID($CardID = null, $StudentID = null)
                {
                        if (is_null($CardID)) {
                                $CardID = $this->getCardIDByStudentID($StudentID);
                        }

                        if (!empty($CardID)) {
                                $sql = "SELECT DATE_FORMAT(MAX(RecordedTime),'%H:%i:%s') FROM CARD_STUDENT_LOG WHERE CardID = '$CardID' AND RecordedTime >= CURDATE()";
                                $temp = $this->returnVector($sql);
                                return $temp[0];
                        }

                        return;
                }

           function retrieveDayAllRecord ($date)
           {
           }
           function retrieveClassDateOutingRecord($ClassID,$date)
           {
                    if ($ClassID != "" && $ClassID != 0)
                    {
                        $className = $this->getClassName($ClassID);
                        $conds = "AND b.ClassName = '$className'";
                    }
                    $sql = "SELECT a.UserID,a.OutTime FROM CARD_STUDENT_OUTING as a
                            LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                            WHERE a.RecordDate = '$date'
                            $conds
                            ORDER BY a.UserID";
                    $temp = $this->returnArray($sql,2);
                    return build_assoc_array($temp);
           }
           function retrieveStudentMonthlyOutingRecord($StudentID,$year, $month)
           {
                    $sql = "SELECT RecordDate, OutTime FROM CARD_STUDENT_OUTING
                            WHERE UserID = '$StudentID' AND Year(RecordDate) = '$year' AND Month(RecordDate) = '$month'";
                    $temp = $this->returnArray($sql,2);
                    return build_assoc_array($temp);
           }

           function retrieveDayClassRecord ($ClassID, $date, $slots)
           {
                                $slotList = implode(",",$slots);
                                $slot_num = sizeof($slots);

                                if ($ClassID != "" && $ClassID != 0)
                                {
                                        $className = $this->getClassName($ClassID);
                                        $conds = "AND a.ClassName = '$className'";
                                }
                                $namefield = getNameFieldByLang("a.");
                                $sql = "SELECT a.UserID,b.SlotID,
                                                           IF(b.RecordedTime IS NULL,'ABS',DATE_FORMAT(b.RecordedTime,'%H:%i:%s')),
                                                           b.MinDiffer
                                                FROM CARD_STUDENT_DAILYSLOTRECORD as b
                                                         LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                                                WHERE b.RecordDate = '$date' AND b.SlotID IN ($slotList)
                                                $conds
                                                ORDER BY a.ClassName, a.ClassNumber,a.EnglishName, a.UserID,b.SlotID";
                                $dailyrecords = $this->returnArray($sql,4);
                                $sql = "SELECT a.UserID, $namefield, a.ClassName, a.ClassNumber FROM INTRANET_USER as a
                                                WHERE a.RecordType = 2 $conds
                                                ORDER BY a.ClassName, a.ClassNumber, a.EnglishName, a.UserID";
                                $students = $this->returnArray($sql,4);
                                $pos = 0;
                                $slot_count = 0;

                                for ($i=0; $i<sizeof($students); $i++)
                                {
                                         list($studentID, $studentName, $className, $classNumber) = $students[$i];
                                         $slot_count = 0;
                                         #$class_str = ($className != "" && $classNumber != "")? "($className - $classNumber)":"";

                                         $data = $dailyrecords[$pos];
                                         list($d_uid,$d_slot,$d_time,$d_min) = $data;

                                         $personRecord = array($studentID,$studentName,$className,$classNumber);
                                         if ($d_uid != $studentID) # No record
                                         {
                                                 for ($j=0; $j<$slot_num; $j++)
                                                 {
                                                          $personRecord[] = "No record";
                                                          $personRecord[] = "No record";
                                                          $personRecord[] = 0;
                                                 }
                                         }
                                         else          # Matched record
                                         {
                                                 while ($slot_count < $slot_num)
                                                 {
                                                                $data = $dailyrecords[$pos];
                                                                list($d_uid,$d_slot,$d_time,$d_min) = $data;
                                                                if ($slots[$slot_count]==$d_slot)
                                                                {
                                                                        #$type_str = ($d_type==1?$i_SmartCard_Late:$i_SmartCard_EarlyLeave);
                                                                        if ($d_time == 'ABS')
                                                                        {
                                                                                $recordType = 1;
                                                                        }
                                                                        else
                                                                        {
                                                                                if ($d_min != 0)
                                                                                {
                                                                                        if ($d_slot == 1 || $d_slot == 2)           # AM || PM
                                                                                        {
                                                                                                $recordType = 2;
                                                                                        }
                                                                                        else                   # After school
                                                                                        {
                                                                                                $recordType = 3;
                                                                                        }
                                                                                }
                                                                                else
                                                                                {
                                                                                        $recordType = 0;
                                                                                }
                                                                        }
                                                                        $personRecord[] = $d_time;
                                                                        $personRecord[] = $recordType;
                                                                        $personRecord[] = $d_min;
                                                                        $pos++;
                                                                }
                                                                else
                                                                {
                                                                        $personRecord[] = "No record";
                                                                        $personRecord[] = "No record";
                                                                        $personRecord[] = 0;
                                                                }
                                                                $slot_count++;
                                                 }
                                         }
                                         $result[] = $personRecord;
                                }
                                return $result;
           }


                function retrieveStudentMonthlyRecord ($studentID, $year, $month, $slots, $order=1)
                {
                                $slotList = implode(",",$slots);
                                $slot_num = sizeof($slots);

                                $sql = "SELECT SlotID, RecordDate,
                                                           IF(RecordedTime IS NULL,'ABS',DATE_FORMAT(RecordedTime,'%H:%i:%s')),
                                                           MinDiffer
                                                FROM CARD_STUDENT_DAILYSLOTRECORD
                                                WHERE UserID = '$studentID' AND Year(RecordDate) = '$year' AND
                                                Month(RecordDate) = '$month' AND SlotID IN ($slotList)
                                                ORDER BY RecordDate";
                                if (!$order) $sql .= " DESC";
                                $sql .= ", SlotID";

                                $dailyrecords = $this->returnArray($sql,4);


                                for ($i=0; $i<sizeof($dailyrecords); $i++)
                                {
                                        list($slotID, $recordDate, $recordTime, $minDiffer) = $dailyrecords[$i];

                                        if ($recordTime == 'ABS')
                                        {
                                                $recordType = 1;
                                        }
                                        else
                                        {
                                                if ($minDiffer != 0)
                                                {
                                                        if ($slotID == 1 || $slotID == 2)           # AM || PM
                                                        {
                                                                $recordType = 2;
                                                        }
                                                        else                   # After school
                                                        {
                                                                $recordType = 3;
                                                        }
                                                }
                                                else
                                                {
                                                        $recordType = 0;
                                                }
                                        }

                                        $result[$recordDate][$slotID] = array($recordTime, $recordType, $minDiffer);
                                }

                                return $result;
           }
           function returnOutingRecord($StudentID, $targetDate)
           {
                    $sql = "SELECT OutingID, OutTime, BackTime, Location, FromWhere, Objective, PIC, Detail
                            FROM CARD_STUDENT_OUTING WHERE UserID = '$StudentID' AND RecordDate = '$targetDate'";
                    return $this->returnArray($sql,8);
           }
           function getWordList($type)
           {
                    $content = trim(get_file_content($this->word_base_dir.$this->file_array[$type]));
                    $array = explode("\n",$content);
                    for ($i=0; $i<sizeof($array); $i++)
                    {
                         $array[$i] = trim($array[$i]);
                    }
                    return $array;
           }
 }


} // End of directives
?>
