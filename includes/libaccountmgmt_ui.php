<?php
# modifying by : Marcus 
include_once('libinterface.php');

class libaccountmgmt_ui extends interface_html {
	
	var $Module;
	
	function libaccountmgmt_ui() {
		$this->Module = "StudentRegistry";
	}
	
	function Get_User_Extra_Info_Setting_UI()
	{
		global $Lang;
		
		$x .= '<div id="ExtraInfoTableDiv" class="table_broad">';
			$x .= $this->Get_Ajax_Loading_Image();
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_User_Extra_Info_Setting_Table()
	{
		global $laccount, $Lang;
		
		$ExtraInfoItemArr = $laccount->Get_User_Extra_Info_Item();
		$ExtraInfoCategoryArr = $laccount->Get_User_Extra_Info_Category();
		
		$ItemNameLang = Get_Lang_Selection("ItemNameCh","ItemNameEn");
		$CategoryNameLang = Get_Lang_Selection("CategoryNameCh","CategoryNameEn");
		
		$ExtraInfoItemArr = BuildMultiKeyAssoc($ExtraInfoItemArr, array("CategoryID","ItemID"));
		$ExtraInfoCategoryArr = BuildMultiKeyAssoc($ExtraInfoCategoryArr, array("CategoryID"));
		
		$thisTitle = $Lang['AccountMgmt']['EditCategory'];
		$thisOnclick="js_Reload_Category_Layer()";
//		$ThickBoxLink = $this->Get_Thickbox_Link(500, 750, 'setting_row', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
		
		$x .= '<table id="ExtraInfoTable" class="common_table_list_v30">'."\n";
			$x .= '<col width="20%">'."\n";
			$x .= '<col>'."\n";
			$x .= '<col>'."\n";
			$x .= '<col width="30px">'."\n";
			$x .= '<col width="100px">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'."\n";
						$x .= '<span class="row_content">'.$Lang['AccountMgmt']['Category'].'</span>'."\n";
						$x .= '<div class="table_row_tool row_content_tool">'.$ThickBoxLink.'</div>'."\n";
					$x .= '</th>'."\n";
					$x .= '<th class="sub_row_top" colspan="4">'.$Lang['AccountMgmt']['Item'].'</th>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['General']['Name'].'</th>'."\n";
					$x .= '<th class="sub_row_top">'.$Lang['General']['EnglishName'].'</th>'."\n";
					$x .= '<th class="sub_row_top">'.$Lang['General']['ChineseName'].'</th>'."\n";
					$x .= '<th class="sub_row_top">&nbsp;</th>'."\n";
					$x .= '<th class="sub_row_top">&nbsp;</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
				foreach((array)$ExtraInfoItemArr as $thisCatID => $thisItemInfoArr)
				{
					$x .= '<tbody class="Cat_'.$thisCatID.'" CategoryID="'.$thisCatID.'">'."\n";
						$RowSpan = count($thisItemInfoArr)+2;
						$x .= '<tr class="nodrop">'."\n";
							$x .= '<td rowspan="'.$RowSpan.'" class="CatName">'.$ExtraInfoCategoryArr[$thisCatID][$CategoryNameLang].'</td>';
						$x .= '</tr>'."\n";
						foreach((array)$thisItemInfoArr as $thisItemID => $thisItemInfo)
						{
							$DeleteLink = '<a href="javascript:void(0)" onclick="js_Delete_Item('.$thisItemID.')" class="delete_dim" title="'.$Lang['AccountMgmt']['DeleteItem'].'"></a>';
							$MoveLink = '<a href="javascript:void(0);" class="move_order_dim move_btn" title="'.$Lang['AccountMgmt']['MoveItem'].'"></a>';
							
							$x .= '<tr>'."\n";
								$x .= '<td><div id="ItemNameEn_'.$thisItemID.'" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);">'.$thisItemInfo['ItemNameEn'].'</div></td>';
								$x .= '<td><div id="ItemNameCh_'.$thisItemID.'" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);">'.$thisItemInfo['ItemNameCh'].'</div></td>';
								$x .= '<td class="Draggable"><div class="table_row_tool row_content_tool">'.$MoveLink.'</div></td>';
								$x .= '<td>';
									$x .= '<div class="table_row_tool row_content_tool">'.$DeleteLink.'</div>';
									$x .= '<input type="hidden" name="OrderItemIDArr[]" class="OrderItemID" value="'.$thisItemID.'">';
								$x .= '</td>';
							$x .= '</tr>'."\n";
						}
						
//						$thisTitle = $Lang['AccountMgmt']['AddItem'];
//						$thisOnclick="js_Add_Item($thisCatID)";
//						$ThickBoxLink = $this->Get_Thickbox_Link(400, 600, 'add_dim', $thisTitle, $thisOnclick, $InlineID="FakeLayer", $Content="");
						$AddBtn = '<a href="javascript:void(0);" onclick="js_Add_Item('.$thisCatID.');" class="add_dim" title="'.$Lang['AccountMgmt']['AddItem'].'"></a>';
						$x .= '<tr class="nodrop addItemRow">'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td><div class="table_row_tool row_content_tool">'.$AddBtn.'</div></td>';
						$x .= '</tr>'."\n";
					$x .= '</tbody>'."\n";
				}
			
		$x .= '</table>'."\n";	
		
		return $x;	
	}
	
	function Get_User_Extra_Info_Setting_Table_Add_Row()
	{
		global $Lang;
		
		$btn = $this->GET_SMALL_BTN($Lang['Btn']['Submit'],"button","js_Check_Item()");
		$btn .= $this->GET_SMALL_BTN($Lang['Btn']['Cancel'],"button","js_Cancel_Item()");
		
		$html .= '<tr class="tmp_new_row">';
			$html .= '<td>';
				$html .= '<input class="textbox_name" id="ItemNameEn" name="ItemNameEn">';
				$html .= $this->Get_Thickbox_Warning_Msg_Div("WarnItemNameEn",$Lang['AccountMgmt']['WarnMsg']['ItemNameEn']);
			$html .= '</td>';
			$html .= '<td>';
				$html .= '<input class="textbox_name" id="ItemNameCh" name="ItemNameCh">';
				$html .= $this->Get_Thickbox_Warning_Msg_Div("WarnItemNameCh",$Lang['AccountMgmt']['WarnMsg']['ItemNameCh']);
			$html .= '</td>';
			$html .= '<td>&nbsp;</td>';
			$html .= '<td>'.$btn.'</td>';
		$html .= '</tr>'; 
		
		$html = str_replace("'","\'",$html);
		return $html;
	}
	
	function Get_User_Extra_Info_Category_Setting_Layer()
	{
		global $laccount, $Lang;
		$ExtraInfoCategoryArr = $laccount->Get_User_Extra_Info_Category();
		
		$x .= $this->Get_Thickbox_Return_Message_Layer();
		$x .= '<table id="CategorySettingTable" class="common_table_list_v30">'."\n";
			$x .= '<col width="15%">'."\n";
			$x .= '<col>'."\n";
			$x .= '<col>'."\n";
			$x .= '<col width="100px">'."\n";
			$x .= '<col width="30px">'."\n";
			$x .= '<col width="100px">'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['AccountMgmt']['UserExtraInfoCategoryCode'].'</th>'."\n";
					$x .= '<th>'.$Lang['General']['EnglishName'].'</th>'."\n";
					$x .= '<th>'.$Lang['General']['ChineseName'].'</th>'."\n";
					$x .= '<th>'.$Lang['AccountMgmt']['MultiSelect'].'</th>'."\n";
					$x .= '<th>&nbsp;</th>'."\n";
					$x .= '<th>&nbsp;</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
				foreach($ExtraInfoCategoryArr as $thisCatInfo)
				{
					$thisCatID = $thisCatInfo['CategoryID'];
					$DeleteLink = '<a href="javascript:void(0)" onclick="js_Delete_Category('.$thisCatID.')" class="delete_dim" title="'.$Lang['AccountMgmt']['DeleteCategory'].'"></a>';
					$MoveLink = '<a href="javascript:void(0);" class="move_order_dim move_btn" title="'.$Lang['AccountMgmt']['MoveItem'].'"></a>';
					$ActiveItem = $thisCatInfo['RecordType']==1?"active_item":"inactive_item";
					$ActiveBtn = '<a href="javascript:void(0);" onclick="js_Update_Category_Multi_Select('.$thisCatID.',this)" class="'.$ActiveItem.'" title="'.$Lang['AccountMgmt']['EditMultiSelect'].'"></a>';
					
					$x .= '<tr>'."\n";
						$x .= '<td>'.$thisCatInfo['CategoryCode'].'</td>'."\n";
						$x .= '<td>'.$thisCatInfo['CategoryNameEn'].'</td>'."\n";
						$x .= '<td>'.$thisCatInfo['CategoryNameCh'].'</td>'."\n";
						$x .= '<td><div class="table_row_tool">'.$ActiveBtn.'</div></td>'."\n";
						$x .= '<td class="Draggable"><div class="table_row_tool row_content_tool">'.$MoveLink.'</div></td>';
							$x .= '<td>';
								$x .= '<div class="table_row_tool row_content_tool">'.$DeleteLink.'</div>';
								$x .= '<input type="hidden" name="OrderCategoryIDArr[]" class="OrderCategoryIDArr" value="'.$thisCatID.'">';
							$x .= '</td>';
					$x .= '</tr>'."\n";
				}
				$AddBtn = '<a href="javascript:void(0);" onclick="js_Add_Category();" class="add_dim" title="'.$Lang['AccountMgmt']['AddItem'].'"></a>';
				$x .= '<tr class="nodrop addItemRow">'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td><div class="table_row_tool row_content_tool">'.$AddBtn.'</div></td>';
				$x .= '</tr>'."\n";
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_User_Extra_Info_Category_Edit_Layer_Add_Row()
	{
		global $Lang;
		
		$btn = $this->GET_SMALL_BTN($Lang['Btn']['Submit'],"button","js_Check_Category()");
		$btn .= $this->GET_SMALL_BTN($Lang['Btn']['Cancel'],"button","js_Cancel_Category()");
		
		$html .= '<tr class="tmp_new_row">';
			$html .= '<td>';
				$html .= '<input class="Mandatory" id="CategoryCode" name="CategoryCode">';
				$html .= $this->Get_Thickbox_Warning_Msg_Div("WarnEmptyCategoryCode",$Lang['AccountMgmt']['WarnMsg']['CategoryCode'],"WarnMsg");
				$html .= $this->Get_Thickbox_Warning_Msg_Div("WarnInvalidCode",$Lang['AccountMgmt']['WarnMsg']['InvalidCode'],"WarnMsg");
			$html .= '</td>';
			$html .= '<td>';
				$html .= '<input class="Mandatory" id="CategoryNameEn" name="CategoryNameEn">';
				$html .= $this->Get_Thickbox_Warning_Msg_Div("WarnEmptyCategoryNameEn",$Lang['AccountMgmt']['WarnMsg']['CategoryNameEn'],"WarnMsg");
			$html .= '</td>';
			$html .= '<td>';
				$html .= '<input class="Mandatory" id="CategoryNameCh" name="CategoryNameCh">';
				$html .= $this->Get_Thickbox_Warning_Msg_Div("WarnEmptyCategoryNameCh",$Lang['AccountMgmt']['WarnMsg']['CategoryNameCh'],"WarnMsg");
			$html .= '</td>';
			$html .= '<td>';
				$html .= '<input type="checkbox" id="MultiSelect" name="MultiSelect" value=1>';
			$html .= '</td>';
			$html .= '<td>&nbsp;</td>';
			$html .= '<td>'.$btn.'</td>';
		$html .= '</tr>'; 
		
		$html = str_replace("'","\'",$html);
		return $html;
	}
	
}
?>