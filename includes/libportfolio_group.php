<?php

// Modifing by Key

// Copy from elcass30 (21-07-2008)

# change name from lib-groups.php to libportfolio_group.php

/*
function_type (length=10)
==============================
A:		assignment
B:		bulletin
EX:		quiz (exercise/exam)
MS:		marksheet
FM:		file management
NOTE:		content
RAWARD:	reading scheme award
RIGHTS:	user(teacher) with management right			** $id = 1 **
		user(teacher) with approval/grading right		** $id = 2 **
PC:		power concept
PORTw:	iPortfolio (web version)
*/


class libportfolio_group extends libdb{

	var $sql;
	var $db;
	var $rs;

	var $random_amount;



	function libportfolio_group($db){
		$this->db = $db;
	}

	function userList()
	{
		global $intranet_db;

		$sql = "SELECT user_id, firstname, lastname, nickname, ifnull(class_number,'') AS class_no, memberType FROM usermaster WHERE status is null ORDER BY memberType, class_number, CONCAT(lastname, firstname)";
		return $this->getUserSelection($sql);
	}


	function userListELP(){
		# should not change the field orders as IP is using this query but no associative array supported
		$sql = "SELECT u.user_id, u.firstname, u.lastname, u.nickname, ifnull(u.class_number,'') AS class_no, memberType FROM usermaster AS u LEFT JOIN user_group AS ug ON (u.memberType='S' AND ug.user_id=u.user_id) WHERE u.status is null AND ug.user_id IS NULL ORDER BY u.memberType, u.class_number, CONCAT(u.lastname, u.firstname)";

		return $this->getUserSelection($sql);
	}


	function userListIn($group_id){
		$sql  = "SELECT a.user_id, a.firstname, a.lastname, a.nickname, ifnull(a.class_number,'') AS class_no, b.leader, a.memberType ";
		$sql .= "FROM usermaster AS a, user_group AS b ";
		$sql .= "WHERE a.user_id = b.user_id AND b.group_id = '$group_id' AND a.status is null ";
		$sql .= "ORDER BY a.memberType, a.class_number, CONCAT(a.firstname, a.lastname)";

		return $this->getUserSelection($sql);
	}


	function userListOut($group_id){
		$sql  = "SELECT a.user_id, a.firstname, a.lastname, a.nickname, ifnull(a.class_number,'') AS class_no, a.memberType ";
		$sql .= "FROM usermaster AS a LEFT JOIN user_group AS b ";
		$sql .= "ON a.user_id = b.user_id AND b.group_id = '$group_id' WHERE b.user_id is null AND a.status is null ";
		$sql .= "ORDER BY a.memberType, a.class_number, CONCAT(a.firstname, a.lastname)";

		return $this->getUserSelection($sql);
	}

	function userListOutPortfolio($group_id){
		global $intranet_db, $eclass_db;

		# retrieve archived students according to year of left
		$sql  = "SELECT
					DISTINCT ps.CourseUserID, iu.YearOfLeft
				FROM
					{$intranet_db}.INTRANET_ARCHIVE_USER AS iu
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps ON ps.UserID=iu.UserID
				WHERE
					iu.RecordType='2'
					AND ps.WebSAMSRegNo IS NOT NULL
					AND ps.UserKey IS NOT NULL
					AND ps.IsSuspend = 0
				ORDER BY
					iu.YearOfLeft DESC, iu.ClassName
				";
		$ArchivedStudentArr = $this->returnArray($sql);

		$YearsArr = array("CURRENT");
		for ($i=0; $i<sizeof($ArchivedStudentArr); $i++)
		{
			$ArchivedYearArr[$ArchivedStudentArr[$i][0]] = $ArchivedStudentArr[$i][1];
			if (sizeof($YearsArr)<=0 || !in_array($ArchivedStudentArr[$i][1], $YearsArr))
			{
				$YearsArr[] = $ArchivedStudentArr[$i][1];
			}
		}

		# retrieve all students in iPortfolio
		if ($group_id!="")
		{
			$sql  = "SELECT a.user_id, a.firstname, a.lastname, a.nickname, ifnull(a.class_number,'') AS class_no, a.memberType ";
			$sql .= "FROM usermaster AS a LEFT JOIN user_group AS b ";
			$sql .= "ON a.user_id = b.user_id AND b.group_id = '$group_id' WHERE b.user_id is null AND a.status is null AND a.memberType='S' ";
			$sql .= "ORDER BY a.memberType, a.class_number, CONCAT(a.firstname, a.lastname)";
		} else
		{
			$sql = "SELECT user_id, firstname, lastname, nickname, ifnull(class_number,'') AS class_no, memberType FROM usermaster WHERE status is null AND memberType='S' ORDER BY memberType, class_number, CONCAT(lastname, firstname)";
		}
		$CourseStudentArr = $this->returnArray($sql);

		for ($i=0; $i<sizeof($CourseStudentArr); $i++)
		{
			$YearLeft = ($ArchivedYearArr[$CourseStudentArr[$i][0]]=="") ? "CURRENT" : $ArchivedYearArr[$CourseStudentArr[$i][0]];
			$StudentList[$YearLeft][] = $CourseStudentArr[$i];
			$StudentList[$YearLeft][sizeof($StudentList[$YearLeft])-1]["YearLeft"] = $YearLeft;
		}

		for ($i=0; $i<sizeof($YearsArr); $i++)
		{
			for ($j=0; $j<sizeof($StudentList[$YearsArr[$i]]); $j++)
			{
				$StudentSelectArr[] = $StudentList[$YearsArr[$i]][$j];
			}
		}

		return $this->getUserSelection("", $StudentSelectArr);
	}


	function userListOutELP($group_id){
		$sql  = "SELECT a.user_id, a.firstname, a.lastname, a.nickname, ifnull(a.class_number,'') AS class_no, a.memberType ";

		$sql .= "FROM usermaster AS a LEFT JOIN user_group AS b ";
		$sql .= "ON (a.user_id = b.user_id AND b.group_id = '$group_id') LEFT JOIN user_group AS ug ON (a.memberType='S' AND ug.user_id=a.user_id) WHERE (b.user_id is null AND ug.user_id IS NULL) AND a.status is null ";
		$sql .= "ORDER BY a.memberType, a.class_number, CONCAT(a.firstname, a.lastname)";

		return $this->getUserSelection($sql);
	}


	function studentAssistantList(){
		$sql = "SELECT user_id, firstname, lastname, nickname, ifnull(class_number,'') AS class_no, memberType
				FROM usermaster WHERE status is null AND (memberType='S' OR memberType='A')
				ORDER BY memberType, class_number, CONCAT(firstname, lastname)";
		$x = $this->getUserSelection($sql);
		return $x;
	}

	function studentAssistantListIn($id, $type){
		$sql = "SELECT u.user_id, u.firstname, u.lastname, u.nickname, ifnull(u.class_number,'') AS class_no, u.memberType
				FROM usermaster as u LEFT JOIN grouping_function as g ON u.user_id=g.group_id
				WHERE g.function_id='$id' AND g.function_type='$type' AND u.status is null AND (u.memberType='S' OR u.memberType='A')
				ORDER BY u.memberType, u.class_number, CONCAT(u.firstname, u.lastname)";
		$x = $this->getUserSelection($sql);

		return $x;
	}

	function studentAssistantListOut($id, $type){
		$sql = "SELECT u.user_id, u.firstname, u.lastname, u.nickname, ifnull(u.class_number,'') AS class_no, u.memberType FROM usermaster as u LEFT JOIN grouping_function as g ON u.user_id=g.group_id AND g.function_id='$id' AND g.function_type='$type' WHERE g.group_id IS NULL AND u.status is null AND (u.memberType='S' OR u.memberType='A') ORDER BY u.memberType, u.class_number, CONCAT(u.firstname, u.lastname)";
		$x = $this->getUserSelection($sql);

		return $x;
	}

	function teacherAssistantList(){
		$x = "";
		$sql = "SELECT user_id, firstname, lastname, nickname, ifnull(class_number,'') AS class_no FROM usermaster WHERE status is null AND (memberType='T' OR memberType='A') ORDER BY memberType, class_number, CONCAT(firstname, lastname)";
		$x .= $this->getUserSelection($sql);
		return $x;
	}

	function teacherAssistantListIn($id, $type){
		$x = "";
		$sql = "SELECT u.user_id, u.firstname, u.lastname, u.nickname, ifnull(u.class_number,'') AS class_no FROM usermaster as u LEFT JOIN grouping_function as g ON u.user_id=g.group_id WHERE g.function_id='$id' AND g.function_type='$type' AND u.status is null AND (u.memberType='T' OR u.memberType='A') ORDER BY u.memberType, u.class_number, CONCAT(u.firstname, u.lastname)";
		$x .= $this->getUserSelection($sql);

		return $x;
	}

	function teacherAssistantListOut($id, $type){
		$x = "";
		$sql = "SELECT u.user_id, u.firstname, u.lastname, u.nickname, ifnull(u.class_number,'') AS class_no FROM usermaster as u LEFT JOIN grouping_function as g ON u.user_id=g.group_id AND g.function_id='$id' AND g.function_type='$type' WHERE g.group_id IS NULL AND u.status is null AND (u.memberType='T' OR u.memberType='A') ORDER BY u.memberType, u.class_number, CONCAT(u.firstname, u.lastname)";
		$x .= $this->getUserSelection($sql);

		return $x;
	}

/*
	function getUserSelection($sql){
		$x = "";
		$this->rs = $this->db_db_query($sql);
		if ($this->rs && $this->db_num_rows()!=0){
			while($row = $this->db_fetch_array()) {
				$user_id = $row[0];
				$firstname = $row[1];
				$lastname = $row[2];
				$nickname = $row[3];
				$class_number = $row[4];
				if (sizeof($row)>=6) $leader = ($row[5]!='') ? true : false;
				if(trim($class_number)!='') $class_number = "&lt;$class_number&gt;";
				$x .= ($leader) ? "<option value='$user_id' selected>$class_number $lastname $firstname</option>\n" : "<option value='$user_id'>$class_number $lastname $firstname</option>\n";
			}
		}
		$this->db_free_result();
		return $x;
	}
*/
	function getUserSelection($sql, $user_array=null){
		global $usertype_t, $usertype_s, $usertype_a, $usertype_p, $ec_words;

		$rx = "";
		$type_flag = array();

		$row = ($user_array!=null && $sql=="") ? $user_array : $this->returnArray($sql);
		for ($i=0; $i<sizeof($row); $i++)
		{
			if (isset($row[$i]['user_id']))
			{
				$user_id = $row[$i]['user_id'];
				$firstname = $row[$i]['firstname'];
				$lastname = $row[$i]['lastname'];
				$nickname = $row[$i]['nickname'];
				$class_number = $row[$i]['class_no'];
				$memberType = trim($row[$i]['memberType']);
				$leader = $row[$i]['leader'];
			} else
			{
				$user_id = $row[$i][0];
				$firstname = $row[$i][1];
				$lastname = $row[$i][2];
				$nickname = $row[$i][3];
				$class_number = $row[$i][4];
				$memberType = trim($row[$i][5]);
				$leader = $row[$i][6];
			}
			$YearLeft = $row[$i]["YearLeft"];
			if ($memberType!="")
			{
				$member_title = ${"usertype_".strtolower($memberType)};
				if ($YearLeft!="CURRENT" && $YearLeft!="")
				{
					$member_title .= " (".$YearLeft.")";
				}
				if (!in_array($member_title, $type_flag))
				{
					if ($isOptionGroupBefore)
					{
						$rx .= "</optgroup>\n";
					}

					$rx .= "<optgroup label=\"".$member_title."\">\n";
					$type_flag[] = $member_title;
				}
				$isOptionGroupBefore = true;
			}
			if(trim($class_number)!='') $class_number = "&lt;$class_number&gt;";
			$rx .= ($leader) ? "<option value='$user_id' selected>$class_number $lastname $firstname</option>\n" : "<option value='$user_id'>$class_number $lastname $firstname</option>\n";
		}
		if ($isOptionGroupBefore)
		{
			$rx .= "</optgroup>\n";
			$rx .= "<optgroup label=\"".$ec_words['new_allocate']."\"></optgroup>\n";
		}

		return $rx;
	}

/*
	# not in used since 2007-10-04
	function getUserSelection($sql){
		global $usertype_t, $usertype_s, $usertype_a, $usertype_p, $ec_words;

		$rx = "";
		$type_flag = array();

		$row = $this->returnArray($sql);
		for ($i=0; $i<sizeof($row); $i++)
		{
			if (isset($row[$i]['user_id']))
			{
				$user_id = $row[$i]['user_id'];
				$firstname = $row[$i]['firstname'];
				$lastname = $row[$i]['lastname'];
				$nickname = $row[$i]['nickname'];
				$class_number = $row[$i]['class_no'];
				$memberType = trim($row[$i]['memberType']);
				$leader = $row[$i]['leader'];
			} else
			{
				$user_id = $row[$i][0];
				$firstname = $row[$i][1];
				$lastname = $row[$i][2];
				$nickname = $row[$i][3];
				$class_number = $row[$i][4];
				$memberType = trim($row[$i][5]);
				$leader = $row[$i][6];
			}
			if ($memberType!="")
			{
				if (!in_array($memberType, $type_flag))
				{
					if ($isOptionGroupBefore)
					{
						$rx .= "</optgroup>\n";
					}
					$member_title = ${"usertype_".strtolower($memberType)};
					$rx .= "<optgroup label=\"".$member_title."\">\n";
					$type_flag[] = $memberType;
				}
				$isOptionGroupBefore = true;
			}
			if(trim($class_number)!='') $class_number = "&lt;$class_number&gt;";
			$rx .= ($leader) ? "<option value='$user_id' selected>$class_number $lastname $firstname</option>\n" : "<option value='$user_id'>$class_number $lastname $firstname</option>\n";
		}
		if ($isOptionGroupBefore)
		{
			$rx .= "</optgroup>\n";
			$rx .= "<optgroup label=\"".$ec_words['new_allocate']."\"></optgroup>\n";
		}

		return $rx;
	}
*/
	function memberList($group_id){
		global $ec_words;

		$x = "";
		$sql  = "SELECT a.user_id, a.firstname, a.lastname, a.nickname, ifnull(a.class_number,''), a.user_email, b.leader ";
		$sql .= "FROM usermaster AS a, user_group AS b ";
		$sql .= "WHERE a.user_id = b.user_id AND b.group_id = '$group_id' AND a.status is null ";
		$sql .= "ORDER BY a.memberType, a.class_number, CONCAT(a.firstname, a.lastname)";
		$this->rs = $this->db_db_query($sql);
		if ($this->rs && $this->db_num_rows()!=0)
		{
			while($row = $this->db_fetch_array())
			{
				$user_id = $row[0];
				$firstname = $row[1];
				$lastname = $row[2];
				$nickname = $row[3];
				$class_number = $row[4];
				$user_email = $row[5];
				$is_leader = $row[6];
				$leader_mark = ($is_leader) ? "<span class='guide'>(".$ec_words['group_leader'].")</span>" : "";
				if(trim($class_number)!='') $class_number = "&lt;$class_number&gt;";
				$x .= " $class_number $lastname $firstname $leader_mark<br>\n";
			}
		}
		$this->db_free_result();
		return $x;
	}

	function groupList($user_id){
		$sql = "SELECT g.group_id FROM user_group as g, usermaster as u WHERE g.user_id=u.user_id AND u.user_id='$user_id'";
		$row = $this->returnArray($sql, 1);
		$x = "";
		for ($i=0; $i<sizeof($row); $i++)
		{
			$group_id = $row[$i][0];
			if ($group_id>0)
			{
				$x .= ($x!="") ? ",".$group_id : $group_id;
			}
		}

		return $x;
	}

	function getGroups(){
		$groups = array();
		$x = "";
		$j = 0;
		$sql = "SELECT group_id, group_name, group_desc FROM grouping ORDER BY group_name";
		$this->rs = $this->db_db_query($sql);
		if ($this->rs && $this->db_num_rows()!=0){
			$flag = 1;
			while($row = $this->db_fetch_array()){
				$group_id = $row[0];
				$group_name = $row[1];
				$group_desc = $this->convertAllLinks(nl2br($row[2]));
				$groups[$j++] = array($group_id, $group_name, $group_desc);
			}
		}
		$this->db_free_result();
		return $groups;
	}

	function displayGroups(){
		global $no_record_msg, $namelist_groups_name, $namelist_groups_desc, $namelist_groups_members;
		$x = "";
		$groups = $this->getGroups();
		$j = sizeof($groups);
		$x .= "<table width=95% border=0 cellpadding=2 cellspacing=1>\n";
		if($j == 0){
			$x .= "<tr><td>$no_record_msg</td></tr>\n";
		}else{
			$x .= "<tr>\n";
			$x .= "<td class=tableTitle>$namelist_groups_name</td>\n";
			$x .= "<td class=tableTitle width='50%'>$namelist_groups_desc</td>\n";
			$x .= "<td class=tableTitle>$namelist_groups_members</td>\n";
			$x .= "</tr>\n";
			for($i=0; $i<$j; $i++){
				$x .= "<tr>\n";
				$x .= "<td class=tableContent>".$groups[$i][1]."<br></td>\n";
				$x .= "<td class=tableContent>".$groups[$i][2]."<br></td>\n";
				$x .= "<td class=tableContent>".$this->memberList($groups[$i][0])."<br></td>\n";
				$x .= "</tr>\n";
			}
		}
		$x .= "</table>\n";
		return $x;
	}


	function getGroupsList(){
		global $new_implemented, $ck_group_id;

		$x = "";
		$groups = $this->getGroups();
		$j = sizeof($groups);
		if($j > 0){
			for($i=0; $i<sizeof($groups); $i++){
				$x .= ($ck_group_id!="" && $groups[$i][0]==$ck_group_id) ? "" : "<option value=".$groups[$i][0].">".$groups[$i][1]."</option>\n";
			}
		}
		return $x;
	}
	
	/*
	*	 Modified by Key (21-07-2008) for reset ipf 2.5 new scheme form
	*/
	function getGroupsList_DataValue()
	{
		global $new_implemented, $ck_group_id;

		$ReturnArr = array();
		$groups = $this->getGroups();
		$j = sizeof($groups);
		if($j > 0)
		{
			for($i=0; $i<sizeof($groups); $i++)
			{
				$ReturnArr[] = $groups[$i];
			}
		}
		return $ReturnArr;
	}

	function getProjectGroupsByID($ID)
	{
		$sql = "SELECT
				a.group_name
			FROM
				grouping as a
				LEFT JOIN project as b ON a.group_id = b.group_id
			WHERE
				b.assignment_id = '".$ID."'
			ORDER BY
				a.group_name";
		$row = $this->returnVector($sql);

		return $row;
	}

	function getGroupsByID($ID, $type)
	{
		$sql = "SELECT
				a.group_name
			FROM
				grouping as a
				LEFT JOIN grouping_function as b ON a.group_id = b.group_id
			WHERE
				b.function_id = '".$ID."'
				AND b.function_type = '".$type."'
			ORDER BY
				a.group_name";
		$row = $this->returnVector($sql);

		return $row;
	}

	# Retrieve project count by group ID
	function getProjectCountByGroup($group_id)
	{
		$sql = "SELECT
					COUNT(assignment_id)
				FROM
					project
				WHERE
					group_id = '$group_id'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	# Retrieve marksheet count by group ID
	function getMarksheetCountByGroup($group_id)
	{
		$sql = "SELECT
					COUNT(a.marksheet_id)
				FROM
					marksheet as a
					LEFT JOIN grouping_function as b ON a.marksheet_id = b.function_id
				WHERE
					b.group_id = '$group_id'
					AND b.function_type = 'MS'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	# Retrieve assignment count by group ID
	function getAssignmentCountByGroup($group_id, $type)
	{
		$sql = "SELECT
					COUNT(a.assignment_id)
				FROM
					assignment as a
					LEFT JOIN grouping_function as b ON a.assignment_id = b.function_id
				WHERE
					a.worktype='".$type."'
					AND b.group_id = '$group_id'
					AND b.function_type = 'A'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	# Retrieve exercise count by group ID
	function getExerciseCountByGroup($group_id, $type)
	{
		$sql = "SELECT
					COUNT(q.quiz_id)
				FROM
					quiz as q
					LEFT JOIN grouping_function as b ON q.quiz_id = b.function_id
				WHERE
					q.ttype='".$type."'
					AND b.group_id = '$group_id'
					AND b.function_type = 'EX'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	# Retrieve the group name by the group_id
	function getGroupName($group_id)
	{
		$sql = "SELECT
					group_name
				FROM
					grouping
				WHERE
					group_id = '$group_id'";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	#	Retrieve groups that have marked exam assigned
	function getToBeMarkedExamGroupsList($group_id, $type)
	{
		global $range_all, $no_group;

		$sql = "SELECT DISTINCT
					a.group_id,
					a.group_name
				FROM
					grouping as a
					LEFT JOIN grouping_function as b ON a.group_id = b.group_id
					LEFT JOIN quiz as c ON b.function_id = c.quiz_id
					LEFT JOIN quiz_result as d ON c.quiz_id = d.quiz_id
				WHERE
					c.ttype = '".$type."'
					AND b.function_type = 'EX'
					AND d.time_mark IS NULL
					AND c.ttype='1'
					AND quiz_result_id IS NOT NULL
				ORDER BY
					a.group_name
				";
		$row = $this->returnArray($sql);

		$x = "<SELECT name='group_id' onChange='document.form1.submit();'>";
		$x .= "<OPTION value='-1' style=\"color:'#FF6400'\" ".($group_id=='-1'?'SELECTED':'').">".$range_all."</OPTION>";
		$x .= "<OPTION value='-2' style=\"color:'#FF6400'\" ".($group_id=='-2'?'SELECTED':'').">".$no_group."</OPTION>";
		for($i=0; $i<sizeof($row); $i++)
		{
			list($gid, $gname) = $row[$i];
			$selected = ($group_id==$gid) ? "SELECTED" : "";
			$x .= "<OPTION value='".$gid."' $selected>".$gname."</OPTION>";
		}
		$x .= "</SELECT>";

		return $x;
	}

	#	Retrieve groups that have exercise assigned
	function getExerciseGroupsList($group_id, $type)
	{
		global $range_all, $no_group;

		$sql = "SELECT DISTINCT
					a.group_id,
					a.group_name
				FROM
					grouping as a
					LEFT JOIN grouping_function as b ON a.group_id = b.group_id
					LEFT JOIN quiz as c ON b.function_id = c.quiz_id
				WHERE
					c.ttype = '".$type."'
					AND b.function_type = 'EX'
				ORDER BY
					a.group_name
				";
		$row = $this->returnArray($sql);

		$x = "<SELECT name='group_id' onChange='document.form1.submit();'>";
		$x .= "<OPTION value='-1' style=\"color:'#FF6400'\" ".($group_id=='-1'?'SELECTED':'').">".$range_all."</OPTION>";
		$x .= "<OPTION value='-2' style=\"color:'#FF6400'\" ".($group_id=='-2'?'SELECTED':'').">".$no_group."</OPTION>";
		for($i=0; $i<sizeof($row); $i++)
		{
			list($gid, $gname) = $row[$i];
			$selected = ($group_id==$gid) ? "SELECTED" : "";
			$x .= "<OPTION value='".$gid."' $selected>".$gname."</OPTION>";
		}
		$x .= "</SELECT>";

		return $x;
	}

	# Retrieve groups that have assignment assigned
	function getMarksheetGroupsList($group_id)
	{
		global $range_all, $no_group;

		$sql = "SELECT DISTINCT
					a.group_id,
					a.group_name
				FROM
					grouping as a
					LEFT JOIN grouping_function as b ON a.group_id = b.group_id
					LEFT JOIN marksheet as c ON b.function_id = c.marksheet_id
				WHERE
					b.function_type = 'MS'
					AND c.marksheet_id IS NOT NULL
				ORDER BY
					a.group_name
				";
		$row = $this->returnArray($sql);

		$x = "<SELECT name='group_id' onChange='document.form1.submit();'>";
		$x .= "<OPTION value='-1' style=\"color:'#FF6400'\" ".($group_id=='-1'?'SELECTED':'').">".$range_all."</OPTION>";
		$x .= "<OPTION value='-2' style=\"color:'#FF6400'\" ".($group_id=='-2'?'SELECTED':'').">".$no_group."</OPTION>";
		for($i=0; $i<sizeof($row); $i++)
		{
			list($gid, $gname) = $row[$i];
			$selected = ($group_id==$gid) ? "SELECTED" : "";
			$x .= "<OPTION value='".$gid."' $selected>".$gname."</OPTION>";
		}
		$x .= "</SELECT>";

		return $x;
	}

	# Retrieve groups that have assignment assigned
	function getAssignmentGroupsList($group_id, $type)
	{
		global $range_all, $no_group;

		// $type refer to:
		// 1 - assignment
		// 2 - survey

		$sql = "SELECT DISTINCT
					a.group_id,
					a.group_name
				FROM
					grouping as a
					LEFT JOIN grouping_function as b ON a.group_id = b.group_id
					LEFT JOIN assignment as c ON b.function_id = c.assignment_id
				WHERE
					c.worktype = '".$type."'
					AND b.function_type = 'A'
				ORDER BY
					a.group_name
				";
		$row = $this->returnArray($sql);

		$x = "<SELECT name='group_id' onChange='document.form1.submit();'>";
		$x .= "<OPTION value='-1' style=\"color:'#FF6400'\" ".($group_id=='-1'?'SELECTED':'').">".$range_all."</OPTION>";
		$x .= "<OPTION value='-2' style=\"color:'#FF6400'\" ".($group_id=='-2'?'SELECTED':'').">".$no_group."</OPTION>";
		for($i=0; $i<sizeof($row); $i++)
		{
			list($gid, $gname) = $row[$i];
			$selected = ($group_id==$gid) ? "SELECTED" : "";
			$x .= "<OPTION value='".$gid."' $selected>".$gname."</OPTION>";
		}
		$x .= "</SELECT>";

		return $x;
	}

	# Retrieve groups that have project assigned
	function getProjectGroupsList($group_id)
	{
		global $range_all, $no_group;

		$sql = "SELECT DISTINCT
					a.group_id,
					a.group_name
				FROM
					grouping as a
					LEFT JOIN project as b ON a.group_id = b.group_id
				WHERE
					project_id IS NOT NULL
				ORDER BY
					a.group_name
				";
		$row = $this->returnArray($sql);

		$x = "<SELECT name='group_id' onChange='document.form1.submit();'>";
		$x .= "<OPTION value='-1' style=\"color:'#FF6400'\" ".($group_id=='-1'?'SELECTED':'').">".$range_all."</OPTION>";
		$x .= "<OPTION value='-2' style=\"color:'#FF6400'\" ".($group_id=='-2'?'SELECTED':'').">".$no_group."</OPTION>";
		for($i=0; $i<sizeof($row); $i++)
		{
			list($gid, $gname) = $row[$i];
			$selected = ($group_id==$gid) ? "SELECTED" : "";
			$x .= "<OPTION value='".$gid."' $selected>".$gname."</OPTION>";
		}
		$x .= "</SELECT>";

		return $x;
	}


	function getGroupsListIn($forum_id,$forum_type){
		$x = "";
		$sql  = "SELECT a.group_id, a.group_name ";
		$sql .= "FROM grouping AS a, grouping_function AS b ";
		$sql .= "WHERE a.group_id = b.group_id AND b.function_id = '$forum_id' AND b.function_type = '$forum_type' ";
		$sql .= "ORDER BY a.group_name ";
		$x .= $this->getGroupSelection($sql);

		return $x;
	}

	function getGroupsListOut($forum_id,$forum_type){
		$x = "";
		$sql  = "SELECT a.group_id, a.group_name ";
		$sql .= "FROM grouping AS a LEFT JOIN grouping_function AS b ";
		$sql .= "ON a.group_id = b.group_id AND b.function_id = '$forum_id' AND b.function_type = '$forum_type' WHERE b.group_id IS NULL ";
		$sql .= "ORDER BY a.group_name ";
		$x .= $this->getGroupSelection($sql);

		return $x;
	}

	function getGroupSelection($sql){
		$x = "";
		$this->rs = $this->db_db_query($sql);
		if ($this->rs && $this->db_num_rows()!=0){
			while($row = $this->db_fetch_array()) {
				$a = $row[0];
				$b = $row[1];
				$x .= "<option value=$a>$b</option>\n";
			}
		}
		$this->db_free_result();
		return $x;
	}
	
	/*
	*    Modified by Key (09-02-2009) for correct the selected class scheme
	*/
	function getClassListByGroupIDList($GroupIDs=array(), $assignment_id=""){
		global $eclass_db, $intranet_db;
		$x = array();
		
		$gids = (is_array($GroupIDs)) ? implode(",", $GroupIDs) : "";
		
		if($gids != "")
		{
			$conds = " AND b.group_id IN ($gids)";
		}
		
		// e.ClassID, d.ClassName, f.ClassLevelID, f.LevelName
		$sql = "SELECT DISTINCT 
					d.ClassName
				FROM 
					grouping_function as a 
					LEFT JOIN user_group as b ON a.group_id = b.group_id 
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT as c ON b.user_id = c.CourseUserID
					LEFT JOIN {$intranet_db}.INTRANET_USER as d ON c.UserID = d.UserID
					LEFT JOIN {$intranet_db}.INTRANET_CLASS as e ON d.ClassName = e.ClassName
					LEFT JOIN {$intranet_db}.INTRANET_CLASSLEVEL AS f ON f.ClassLevelID = e.ClassLevelID
				WHERE 
					a.function_id = '{$assignment_id}'
					AND a.function_type = 'A'
					AND b.user_id IS NOT NULL
					AND c.WebSAMSRegNo IS NOT NULL 
					AND c.UserKey IS NOT NULL 
					AND c.IsSuspend = '0'
					AND d.RecordType = '2'
					$conds
				ORDER BY
					d.ClassName
			";
			
		$x = $this->returnVector($sql);
		//$x = $this->returnArray($sql);
			
		return $x;
	}
	
	/*
	*    Modified by Key (21-07-2008) for reset ipf 2.5 new / edit scheme form
	*/
	function getGroupsListIn_DataValue($forum_id,$forum_type){
		$x = array();
		$sql  = "SELECT a.group_id, a.group_name ";
		$sql .= "FROM grouping AS a, grouping_function AS b ";
		$sql .= "WHERE a.group_id = b.group_id AND b.function_id = '$forum_id' AND b.function_type = '$forum_type' ";
		$sql .= "ORDER BY a.group_name ";
		$x = $this->getGroupSelection_DataValue($sql);

		return $x;
	}
	
	/*
	*    Modified by Key (21-07-2008) for reset ipf 2.5 new / edit scheme form
	*/
	function getGroupsListOut_DataValue($forum_id,$forum_type){
		$x = array();
		$sql  = "SELECT a.group_id, a.group_name ";
		$sql .= "FROM grouping AS a LEFT JOIN grouping_function AS b ";
		$sql .= "ON a.group_id = b.group_id AND b.function_id = '$forum_id' AND b.function_type = '$forum_type' WHERE b.group_id IS NULL ";
		$sql .= "ORDER BY a.group_name ";
		$x = $this->getGroupSelection_DataValue($sql);

		return $x;
	}

	/*
	*    Modified by Key (21-07-2008) for reset ipf 2.5 new / edit scheme form
	*/
	function getGroupSelection_DataValue($sql){
		$ReturnArr = array();
		$this->rs = $this->db_db_query($sql);
		if ($this->rs && $this->db_num_rows()!=0){
			while($row = $this->db_fetch_array()) {
				$ReturnArr[] = $row;
			}
		}
		$this->db_free_result();
		
		return $ReturnArr;
	}

	function getGroupsFile(){
		global $ck_memberType, $ck_user_id;
		$groups = array();
		$j = 0;
		if($ck_memberType=="A" || $ck_memberType=="T"){
			$sql = "SELECT group_id, group_name, group_desc FROM grouping ORDER BY group_name";
		} else {
			$sql  = "SELECT a.group_id, a.group_name, a.group_desc FROM grouping AS a, user_group AS b ";
			$sql .= " WHERE a.group_id = b.group_id AND b.user_id = '$ck_user_id'";
			$sql .= " ORDER BY group_name";
		}
		$this->rs = $this->db_db_query($sql);
		if ($this->rs && $this->db_num_rows()!=0){
			while($row = $this->db_fetch_array()){
				$group_id = $row[0];
				$group_name = $row[1];
				$group_desc = $this->convertAllLinks(nl2br($row[2]));
				$groups[$j++] = array($group_id, $group_name, $group_desc);
			}
		}
		$this->db_free_result();
		return $groups;
	}

	function isAccessable($folderID, $user_id){
		$sql = "SELECT user_group_id FROM user_group WHERE user_id = '$user_id' AND group_id = '$folderID'";
		$this->rs = $this->db_db_query($sql);
		if ($this->rs && $this->db_num_rows()!=0){
			$x = 1;
		}else{
			$x = 0;
		}
		$this->db_free_result();
		return $x;
	}

	function returnEmailListing($function_id,$function_type){
		$j = 0;
		$fieldname = "c.user_id, c.user_email, c.firstname, c.lastname, c.nickname, c.class_number";
		$sql  = "SELECT $fieldname ";
		$sql .= " FROM grouping_function AS a, user_group AS b, usermaster AS c ";
		$sql .= " WHERE a.function_id = '$function_id' ";
		$sql .= " AND a.function_type = '$function_type' ";
		$sql .= " AND a.group_id = b.group_id ";
		$sql .= " AND b.user_id = c.user_id ";
		$sql .= " AND (c.status is null OR c.status NOT IN ('deleted')) ";
		$sql .= " AND (c.memberType IN ('S')) ";
		$sql .= " GROUP BY c.user_id ";
		$sql .= " ORDER BY c.class_number, c.lastname ";

		$this->rs = $this->db_db_query($sql);
		if ($this->rs && $this->db_num_rows()!=0)
		{
			$flag = 0;
			while($row = $this->db_fetch_array())
			{
				$user_id = $row[0];
				$user_email = $row[1];
				$firstname = $row[2];
				$lastname = $row[3];
				$nickname = $row[4];
				$class_no = $row[5];
				$x[$j++] = array($user_id, $user_email, $firstname, $lastname, $nickname, $class_no);
			}
		} else
		{
			$flag = 1;
		}
		$this->db_free_result();
		if ($flag == 1)
		{
			$sql  = "SELECT $fieldname ";
			$sql .= " FROM usermaster AS c ";
			$sql .= " WHERE ";
			$sql .= " (c.status is null OR c.status NOT IN ('deleted')) ";
			$sql .= " AND (c.memberType IN ('S')) ";
			$sql .= " ORDER BY c.class_number, CONCAT(c.lastname, c.firstname) ";
			$this->rs = $this->db_db_query($sql);
			if ($this->rs && $this->db_num_rows()!=0)
			{
				while($row = $this->db_fetch_array())
				{
					$user_id = $row[0];
					$user_email = $row[1];
					$firstname = $row[2];
					$lastname = $row[3];
					$nickname = $row[4];
					$class_no = $row[5];
					$x[$j++] = array($user_id, $user_email, $firstname, $lastname, $nickname, $class_no);
				}
			}
			$this->db_free_result();
		}
		return $x;
	}

	function returnFunctionIDs($function_type, $user_id, $isEmptyGroupChecked=false){
		$sql  = "SELECT b.function_id";
		$sql .= " FROM user_group AS a, grouping_function AS b";
		$sql .= " WHERE a.group_id = b.group_id AND a.user_id = '$user_id' AND b.function_type = '$function_type'";
		$sql .= " GROUP BY b.function_id";
		$x = "0";

		$row = $this->returnVector($sql);
		if (sizeof($row)>0)
		{
			$x .= ",".implode(",",$row);
		}

		$sql  = "SELECT b.function_id";
		$sql .= " FROM user_group AS a, grouping_function AS b";
		$sql .= " WHERE a.group_id = b.group_id AND a.user_id <> '$user_id' AND b.function_type = '$function_type'";
		$sql .= " AND b.function_id NOT IN ($x)";
		$sql .= " GROUP BY b.function_id";
		$x = "0";
		$row = $this->returnVector($sql);
		if (sizeof($row)>0)
		{
			$x .= ",".implode(",",$row);
		}

		# add the group if no member inside
		if ($isEmptyGroupChecked)
		{
			$sql  = "SELECT b.function_id, COUNT(a.group_id) ";
			$sql .= " FROM grouping_function AS b LEFT JOIN user_group AS a ON a.group_id = b.group_id ";
			$sql .= " WHERE b.function_type = '$function_type'";
			$sql .= " GROUP BY b.function_id";
			$this->rs = $this->db_db_query($sql);
			if($this->rs && $this->db_num_rows()!=0)
			{
				while($row = $this->db_fetch_array())
				{
					if ($row[1]<=0)
					{
						$x .= ",".$row[0];
					}
				}
			}
			$this->db_free_result();
		}

		if ($function_type=="A") {
			//for projects
			$sql  = "SELECT p.assignment_id";
			$sql .= " FROM user_group AS g, project AS p";
			$sql .= " WHERE g.group_id=p.group_id AND g.user_id = '$user_id' ";
			$sql .= " GROUP BY p.assignment_id";
			$y = "0";

			$this->rs = $this->db_db_query($sql);
			if($this->rs && $this->db_num_rows()!=0)
			{
				while($row = $this->db_fetch_array())
				$y .= ",".$row[0];
			}
			$this->db_free_result();
			$sql  = "SELECT p.assignment_id";
			$sql .= " FROM user_group AS g, project AS p";
			$sql .= " WHERE g.group_id=p.group_id AND g.user_id<>'$user_id'";
			$sql .= " AND p.assignment_id NOT IN ($y)";
			$sql .= " GROUP BY p.assignment_id";
			$y = "";

			$this->rs = $this->db_db_query($sql);
			if($this->rs && $this->db_num_rows()!=0)
			{
				while($row = $this->db_fetch_array())
				$y .= ",".$row[0];
			}
			$x .= $y;
			$this->db_free_result();
		}
		return $x;
	}


	function returnUserListInFunction($function_type, $function_id){
		$sql = "SELECT DISTINCT ug.user_id FROM user_group AS ug, grouping_function AS gf WHERE gf.function_type='$function_type' AND gf.function_id='$function_id' AND ug.group_id=gf.group_id ";
		return $this->returnVector($sql);
	}


	function getDefaultStudentGroup($name, $objProperty){
		$sql = "SELECT g.group_id, CONCAT(g.group_name, ' (', COUNT(ug.user_id), ')')
				FROM grouping AS g
				LEFT JOIN user_group AS ug ON ug.group_id=g.group_id
				LEFT JOIN usermaster AS um ON (um.user_id=ug.user_id)
				WHERE g.Status=1 AND g.group_name NOT LIKE '%Teacher%'
				AND g.group_name NOT LIKE '%Assistant%'
				AND um.Status IS NULL AND um.memberType='S'
				GROUP BY g.group_id
				ORDER BY g.group_name ";
		$row = $this->returnArray($sql, 3);

		return returnSelection($row, "", $name, $objProperty);
	}


	function getRandomStudent($isFromGroups, $random_amount, $random_groups){
		# get all user_id
		$arr_user = $this->returnAvailableStudent($isFromGroups, $random_groups, 1);

		# set amount
		if (strstr($random_amount, "%"))
		{
			$random_amount = (int) (sizeof($arr_user)*str_replace("%", "", $random_amount)/100);
		}
		$this->random_amount = $random_amount;

		if ($random_amount<sizeof($arr_user) && sizeof($arr_user)>0)
		{
			# process random selection
			srand((double)microtime()*1000000);
			shuffle($arr_user);
		}
		$r_arr = $arr_user;

		return $r_arr;
	}


	function getRequiredStudent($isFromGroups, $classno_is, $classno_tail, $random_groups){
		list($reqA, $reqB) = $this->parseRequirement($classno_is, $classno_tail);

		# get all user_id
		$arr_user = $this->returnAvailableStudent($isFromGroups, $random_groups, 0);

		for ($i=0; $i<sizeof($arr_user); $i++)
		{
			list($user_id, $class_no) = $arr_user[$i];
			list($class, $class_no) = split("\-", $class_no);
			$class_no = (int) trim($class_no);
			if ($class_no!="" && ($reqA[$class_no] || $this->checkClassnoTail($reqB, $class_no)))
			{
				$r_arr[] = $user_id;
			}
		}

		return $r_arr;
	}


	function checkClassnoTail($reqB, $class_no){
		if (trim($class_no)!="")
		{
			for ($i=0; $i<sizeof($reqB); $i++)
			{
				$tmp = split($reqB[$i], $class_no);
				if ($tmp[sizeof($tmp)-1]=="" && sizeof($tmp)>1)
				{
					return true;
				}
			}
		}

		return false;
	}


	function parseRequirement($classno_is, $classno_tail){
		$tmp = split("\,", $classno_is);
		for ($i=0; $i<sizeof($tmp); $i++)
		{
			# set allowed class number
			$value=trim($tmp[$i]);
			if ($value!="")
			{
				$value = (int) $value;
				$reqA[$value] = true;
			}
		}

		$tmp = split("\,", $classno_tail);
		for ($i=0; $i<sizeof($tmp); $i++)
		{
			# record class number tail
			$value=trim($tmp[$i]);
			if ($value!="")
			{
				$reqB[] = $value;
			}
		}

		return array($reqA, $reqB);
	}

	function returnAvailableStudent($isFromGroups, $random_groups, $isVector){
		if ($isFromGroups)
		{
			# from selected groups
			$gids = (is_array($random_groups)) ? implode(",", $random_groups) : $random_groups;
			$sql = "SELECT DISTINCT ug.user_id, um.class_number FROM user_group AS ug
					LEFT JOIN usermaster AS um ON (um.user_id=ug.user_id)
					WHERE ug.group_id IN ($gids) AND um.Status is NULL
					ORDER BY um.class_number ";
		} else
		{
			# all students
			$sql = "SELECT user_id, class_number FROM usermaster WHERE status IS NULL AND memberType='S' ORDER BY class_number ";
		}
		$row = ($isVector) ? $this->returnVector($sql) : $this->returnArray($sql, 2);

		return $row;
	}
}
?>