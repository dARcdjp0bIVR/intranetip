<?php
# using : thomas

/* Modification Log:
 * 
 */

if (!defined("LIBEPOSTNEWSPAPER_DEFINED"))         // Preprocessor directives
{
	define("LIBEPOSTNEWSPAPER_DEFINED",true);
	include_once($intranet_root."/includes/ePost/config/config.php");
	
	class libepost_folders extends libepost
	{
		var $Folders;
		function libepost_folders(){
			# Initialize ePost Object
			$this->libepost();
			
			$folders_raw_ary = $this->get_folders_raw_ary();
			
			for($i=0;$i<count($folders_raw_ary);$i++){
				$this->Folders[$i] = new libepost_folder(0, $folders_raw_ary[$i]);
			}
		}
		
		function get_folders_raw_ary(){
			global $cfg_ePost;
			
			$NewspaperIDSeparator = '|=|';
			
			$sql = "SELECT
						ef.FolderID,
						ef.Title AS Folder_Title,
						ef.Status AS Folder_Status,
						ef.InputDate AS Folder_InputDate,
						ef.ModifiedBy AS Folder_ModifiedBy,
						ef.ModifiedDate AS Folder_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Folder_ModifiedByUser,
						iu.EnglishName AS Folder_EnglishName,
						iu.ClassName AS Folder_ClassName,
						iu.ClassNumber AS Folder_ClassNumber,
						GROUP_CONCAT(DISTINCT en.NewspaperID ORDER BY en.NewspaperID SEPARATOR '$NewspaperIDSeparator') AS Folder_NewspaperIDs,
						SUM(en.NoOfView) AS Folder_TotalNoOfView
					FROM
						EPOST_FOLDER AS ef LEFT JOIN
						EPOST_NEWSPAPER AS en ON ef.FolderID = en.FolderID AND en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."' LEFT JOIN
						INTRANET_USER AS iu ON ef.ModifiedBy = iu.UserID
					WHERE
						ef.Status = '".$cfg_ePost['BigNewspaper']['Folder_status']['exist']."'
					GROUP BY
						ef.FolderID
		";
			$result_ary = $this->returnArray($sql);
			
			for($i=0;$i<count($result_ary);$i++){
				$result_ary[$i]['Folder_NewspaperIDs'] = $result_ary[$i]['Folder_NewspaperIDs']? explode($NewspaperIDSeparator, $result_ary[$i]['Folder_NewspaperIDs']) : array();
			}
			
			return $result_ary;
		}
		
		function get_default_folder(){
			global $cfg_ePost;
			
			if(count($this->Folders) > 0){
				for($i=0;$i<count($this->Folders);$i++){
					if($this->Folders[$i]->Newspaper_Title == $cfg_ePost['BigNewspaper']['DefaultFolderTitle']){
						$default_folder = $this->Folders[$i];
					}
				}
			}
			else{
				$NewspaperIDSeparator = '|=|';
				
				$sql = "SELECT
							ef.FolderID,
							ef.Title AS Folder_Title,
							ef.Status AS Folder_Status,
							ef.InputDate AS Folder_InputDate,
							ef.ModifiedBy AS Folder_ModifiedBy,
							ef.ModifiedDate AS Folder_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Folder_ModifiedByUser,
							iu.EnglishName AS Folder_EnglishName,
							iu.ClassName AS Folder_ClassName,
							iu.ClassNumber AS Folder_ClassNumber,
							GROUP_CONCAT(DISTINCT en.NewspaperID ORDER BY en.NewspaperID SEPARATOR '$NewspaperIDSeparator') AS Folder_NewspaperIDs,
							SUM(en.NoOfView) AS Folder_TotalNoOfView
						FROM
							EPOST_FOLDER AS ef LEFT JOIN
							EPOST_NEWSPAPER AS en ON ef.FolderID = en.FolderID AND en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."' LEFT JOIN
							INTRANET_USER AS iu ON ef.ModifiedBy = iu.UserID
						WHERE
							ef.Title = '".$cfg_ePost['BigNewspaper']['DefaultFolderTitle']."'
						GROUP BY
							ef.FolderID";$this->returnArray($sql) or die(mysql_error());
				$result_ary = current($this->returnArray($sql));
				
				if(is_array($result_ary) && count($result_ary)>0)
					$result_ary['Folder_TotalNoOfView'] = $result_ary['Folder_TotalNoOfView']? explode($NewspaperIDSeparator, $result_ary['Folder_TotalNoOfView']) : array(); 
				
				$default_folder = new libepost_folder(0, $result_ary);
			}
			
			return $default_folder;
		}

	}
	
	class libepost_folder extends libepost_folders
	{
		var $FolderID;
		var $Folder_Title;
		var $Folder_Status;
		var $Folder_InputDate;
		var $Folder_ModifiedBy;
		var $Folder_ModifiedDate;
		var $Folder_ModifiedByUser;
		var $Folder_EnglishName;
		var $Folder_ClassName;
		var $Folder_ClassNumber;
		var $Folder_NewspaperIDs;
		var $Folder_Newspapers;
		var $Folder_TotalNoOfView;
		var $sortBy;
		var $orderBy;
		var $keyword;

		
		function libepost_folder($FolderID=0, $Folder_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $FolderID is inputed or not
			$Folder_Data_ary = $FolderID? $this->get_folder_raw_ary($FolderID) : $Folder_Data_ary;
			
			# Initialize Folder Object
			$this->initialize_folder_obj($Folder_Data_ary);
		}
		
		function initialize_folder_obj($Folder_Data_ary){
			$this->FolderID		 		 = $Folder_Data_ary['FolderID'];
			$this->Folder_Title		 	 = $Folder_Data_ary['Folder_Title'];
			$this->Folder_Status		 = $Folder_Data_ary['Folder_Status'];
			$this->Folder_InputDate	 	 = $Folder_Data_ary['Folder_InputDate'];
			$this->Folder_ModifiedBy	 = $Folder_Data_ary['Folder_ModifiedBy'];
			$this->Folder_ModifiedDate   = $Folder_Data_ary['Folder_ModifiedDate'];
			$this->Folder_ModifiedByUser = $Folder_Data_ary['Folder_ModifiedByUser'];
			$this->Folder_EnglishName	 = $Folder_Data_ary['Folder_EnglishName'];
			$this->Folder_ClassName	 	 = $Folder_Data_ary['Folder_ClassName'];
			$this->Folder_ClassNumber	 = $Folder_Data_ary['Folder_ClassNumber'];
			$this->Folder_NewspaperIDs   = $Folder_Data_ary['Folder_NewspaperIDs'];
			$this->Folder_TotalNoOfView  = $Folder_Data_ary['Folder_TotalNoOfView'];
		}
		
		function folder_obj_to_array(){
			$Folder_Data_ary['FolderID'] 			  = $this->FolderID;
			$Folder_Data_ary['Folder_Title'] 		  = $this->Folder_Title;
			$Folder_Data_ary['Folder_Status'] 		  = $this->Folder_Status;
			$Folder_Data_ary['Folder_InputDate'] 	  = $this->Folder_InputDate;
			$Folder_Data_ary['Folder_ModifiedBy'] 	  = $this->Folder_ModifiedBy;
			$Folder_Data_ary['Folder_ModifiedDate']   = $this->Folder_ModifiedDate;
			$Folder_Data_ary['Folder_ModifiedByUser'] = $this->Folder_ModifiedByUser;
			$Folder_Data_ary['Folder_EnglishName'] 	  = $this->Folder_EnglishName;
			$Folder_Data_ary['Folder_ClassName'] 	  = $this->Folder_ClassName;
			$Folder_Data_ary['Folder_ClassNumber'] 	  = $this->Folder_ClassNumber;
			$Folder_Data_ary['Folder_NewspaperIDs']   = $this->Folder_NewspaperIDs;
			$Folder_Data_ary['Folder_TotalNoOfView']  = $this->Folder_TotalNoOfView;
			
			return $Folder_Data_ary;
		}
		
		function get_folder_raw_ary($FolderID){
			global $cfg_ePost;
			
			$NewspaperIDSeparator = '|=|';
			
			$sql = "SELECT
						ef.FolderID,
						ef.Title AS Folder_Title,
						ef.Status AS Folder_Status,
						ef.InputDate AS Folder_InputDate,
						ef.ModifiedBy AS Folder_ModifiedBy,
						ef.ModifiedDate AS Folder_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Folder_ModifiedByUser,
						iu.EnglishName AS Folder_EnglishName,
						iu.ClassName AS Folder_ClassName,
						iu.ClassNumber AS Folder_ClassNumber,
						GROUP_CONCAT(DISTINCT en.NewspaperID ORDER BY en.NewspaperID SEPARATOR '$NewspaperIDSeparator') AS Folder_NewspaperIDs,
						SUM(en.NoOfView) AS Folder_TotalNoOfView
					FROM
						EPOST_FOLDER AS ef LEFT JOIN
						EPOST_NEWSPAPER AS en ON ef.FolderID = en.FolderID AND en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."' LEFT JOIN
						INTRANET_USER AS iu ON ef.ModifiedBy = iu.UserID
					WHERE
						ef.FolderID = '$FolderID'
					GROUP BY
						ef.FolderID";
			$result_ary = current($this->returnArray($sql));
			
			if(is_array($result_ary) && count($result_ary)>0)
				$result_ary['Folder_NewspaperIDs'] = $result_ary['Folder_NewspaperIDs']? explode($NewspaperIDSeparator, $result_ary['Folder_NewspaperIDs']) : array(); 
			
			return $result_ary;
		}
		
		function get_newspapers_raw_ary(){
			global $cfg_ePost;
			$result_ary = array();
			if(count($this->Folder_NewspaperIDs) > 0){
				$PageIDSeparator = '|=|';
				switch($this->sortBy){
					case 'title':$cond=' ORDER BY Newspaper_Title';break;
					case 'name':$cond=' ORDER BY Newspaper_Name';break;
					case 'date':$cond=' ORDER BY Newspaper_IssueDate';break;
					default:$cond='';
				}
				$cond .= ($this->orderBy==1)?' DESC':'';
				if(!empty($this->keyword)){
					$keyword_search = "
						AND(
								en.Title LIKE '%".$this->keyword."%'
							OR en.Name LIKE '%".$this->keyword."%'
							OR en.IssueDate LIKE '%".$this->keyword."%'
						)
					";
				}
				$sql = "SELECT
							en.NewspaperID,
							en.FolderID,
							en.Title AS Newspaper_Title,
							en.Name AS Newspaper_Name,
							en.IssueDate AS Newspaper_IssueDate,
							en.OpenToPublic AS Newspaper_OpenToPublic,
							en.Skin AS Newspaper_Skin,
							en.SchoolLogo AS Newspaper_SchoolLogo,
							en.CoverBanner AS Newspaper_CoverBanner,
							en.CoverBannerHideTitle AS Newspaper_CoverBannerHideTitle,
							en.CoverBannerHideName AS Newspaper_CoverBannerHideName,
							en.InsideBanner AS Newspaper_InsideBanner,
							en.InsideBannerHideTitle AS Newspaper_InsideBannerHideTitle,
							en.InsideBannerHideName AS Newspaper_InsideBannerHideName,
							en.InsideBannerHidePageCat AS Newspaper_InsideBannerHidePageCat,
							en.NoOfView AS Newspaper_NoOfView,
							en.PublishStatus AS Newspaper_PublishStatus,
							en.Status AS Newspaper_Status,
							en.InputDate AS Newspaper_InputDate,
							en.ModifiedBy AS Newspaper_ModifiedBy,
							en.ModifiedDate AS Newspaper_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Newspaper_ModifiedByUser,
							iu.EnglishName AS Newspaper_EnglishName,
							iu.ClassName AS Newspaper_ClassName,
							iu.ClassNumber AS Newspaper_ClassNumber,
							GROUP_CONCAT(DISTINCT ep.PageID ORDER BY ep.PageOrder SEPARATOR '$PageIDSeparator') AS Newspaper_PageIDs
						FROM
							EPOST_NEWSPAPER AS en INNER JOIN
							INTRANET_USER AS iu ON en.ModifiedBy = iu.UserID LEFT JOIN
							EPOST_NEWSPAPER_PAGE AS ep ON en.NewspaperID = ep.NewspaperID AND ep.Status = '".$cfg_ePost['BigNewspaper']['Page_status']['exist']."'
						WHERE
							en.NewspaperID IN ('".implode("','", $this->Folder_NewspaperIDs)."') AND
							en.Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['exist']."'
							".$keyword_search."
						GROUP BY
							en.NewspaperID
							".$cond."
						";
				$result_ary = $this->returnArray($sql); 
				
				# Get Newspaper Student Editor
				$sql = "SELECT
							esem.ManageItemID,
							iu.UserID,
							iu.EnglishName,
							iu.ClassName,
							iu.ClassNumber
						FROM
							EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
							EPOST_STUDENT_EDITOR AS ese ON esem.EditorID = ese.EditorID INNER JOIN
							INTRANET_USER AS iu ON ese.UserID = iu.UserID
						WHERE
							ese.Status = '".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']."' AND
							esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."' AND
							esem.Status = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."'
						ORDER BY
							iu.ClassName, iu.ClassNumber, iu.EnglishName";
				$StudentEditor_ary = $this->returnArray($sql);
				
				for($i=0;$i<count($result_ary);$i++){
					$result_ary[$i]['Newspaper_PageIDs'] 	= $result_ary[$i]['Newspaper_PageIDs']? explode($PageIDSeparator, $result_ary[$i]['Newspaper_PageIDs']) : array();
					$result_ary[$i]['Newspaper_StudentEditors'] 		= array();
					$result_ary[$i]['Newspaper_StudentEditorUserIDs'] = array();
					
					for($j=0;$j<count($StudentEditor_ary);$j++){
						if($result_ary[$i]['NewspaperID']==$StudentEditor_ary[$j]['ManageItemID']){
							$result_ary[$i]['Newspaper_StudentEditorUserIDs'][] = $StudentEditor_ary[$j]['UserID'];
							$result_ary[$i]['Newspaper_StudentEditors'][] 	    = array("UserID"	  => $StudentEditor_ary[$j]['UserID'],
																			  			"EnglishName" => $StudentEditor_ary[$j]['EnglishName'],
																			  			"ClassName"	  => $StudentEditor_ary[$j]['ClassName'],
																			  			"ClassNumber" => $StudentEditor_ary[$j]['ClassNumber']);
						}
					}
				}
			}
			
			return $result_ary;
		}
		
		function get_newspapers(){
			if(count($this->Folder_Newspapers)==0){
				$Folder_Data_ary    = $this->folder_obj_to_array();
				$newspapers_raw_ary = $this->get_newspapers_raw_ary();
				
				for($i=0;$i<count($newspapers_raw_ary);$i++){
					$Newspaper_Data_ary = array_merge($Folder_Data_ary, $newspapers_raw_ary[$i]);
					$this->Folder_Newspapers[$i] = new libepost_newspaper(0, $Newspaper_Data_ary);
				}
			}
			
			return $this->Folder_Newspapers;
		}
		
		function get_managing_newspapers(){
			global $UserID;
			
			$newspapers_ary = array();
			
			if($this->is_editor){
				$all_newspapers_ary = $this->get_newspapers();
				if($this->user_obj->isTeacherStaff()){
					$newspapers_ary = $all_newspapers_ary;
				}
				else{
					for($i=0;$i<count($all_newspapers_ary);$i++){
						$NewspaperObj = $all_newspapers_ary[$i];
						if(in_array($UserID, $NewspaperObj->Newspaper_StudentEditorUserIDs)){
							$newspapers_ary[] = $NewspaperObj;
						}
					}
				}
			}
			
			return $newspapers_ary;
		}
		
		function update_folder_db(){
			
		}
		
		function delete_folder(){
			global $cfg_ePost, $UserID;
			
			if($this->Folder_Title != $cfg_ePost['BigNewspaper']['DefaultFolderTitle']){
				$sql = "UPDATE
							EPOST_FOLDER AS ef
						SET
							ef.Status = ".$cfg_ePost['BigNewspaper']['Folder_status']['deleted'].",
							ef.ModifiedBy = '$UserID',
							ef.ModifiedDate = NOW()
						WHERE
							ef.FolderID = '".$this->FolderID."'";
				$result = $this->db_db_query($sql);
				
				if(count($this->Folder_NewspaperIDs)>0){
					$default_folder = $this->get_default_folder();
					
					$sql = "UPDATE
								EPOST_NEWSPAPER AS en
							SET
								en.FolderID = '".$default_folder->FolderID."'
							WHERE
								en.NewspaperID IN ('".implode("','", $this->Folder_NewspaperIDs)."')";
					$result = $this->db_db_query($sql);
				}
			}
			else{
				$result = false;
			}
			
			return $result;
		}
	}
	
	class libepost_newspaper extends libepost_folder
	{
		var $NewspaperID;
		var $Newspaper_Title;
		var $Newspaper_Name;
		var $Newspaper_IssueDate;
		var $Newspaper_OpenToPublic;
		var $Newspaper_Skin;
		var $Newspaper_SchoolLogo;
		var $Newspaper_CoverBanner;
		var $Newspaper_CoverBannerHideTitle;
		var $Newspaper_CoverBannerHideName;
		var $Newspaper_InsideBanner;
		var $Newspaper_InsideBannerHideTitle;
		var $Newspaper_InsideBannerHideName;
		var $Newspaper_InsideBannerHidePageCat;
		var $Newspaper_NoOfView;
		var $Newspaper_PublishStatus;
		var $Newspaper_Status;
		var $Newspaper_InputDate;
		var $Newspaper_ModifiedBy;
		var $Newspaper_ModifiedDate;
		var $Newspaper_ModifiedByUser;
		var $Newspaper_EnglishName;
		var $Newspaper_ClassName;
		var $Newspaper_ClassNumber;
		var $Newspaper_PageIDs;
		var $Newspaper_Pages;
		var $Newspaper_StudentEditorUserIDs;
		var $Newspaper_StudentEditors;
		
		function libepost_newspaper($NewspaperID=0, $Newspaper_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $NewspaperID is inputed or not
			$Newspaper_Data_ary = $NewspaperID? $this->get_newspaper_raw_ary($NewspaperID) : $Newspaper_Data_ary;
			
			# Initialize Newspaper Object
			$this->initialize_newspaper_obj($Newspaper_Data_ary);
		}
		
		function initialize_newspaper_obj($Newspaper_Data_ary){
			# Initialize Folder Object
			$this->initialize_folder_obj($Newspaper_Data_ary);
			
			$this->NewspaperID 			 		  	 = $Newspaper_Data_ary['NewspaperID'];
			$this->Newspaper_Title 		 		  	 = $Newspaper_Data_ary['Newspaper_Title'];
			$this->Newspaper_Name 		 		  	 = $Newspaper_Data_ary['Newspaper_Name'];
			$this->Newspaper_IssueDate 	  		  	 = $Newspaper_Data_ary['Newspaper_IssueDate'];
			$this->Newspaper_OpenToPublic 	  	  	 = $Newspaper_Data_ary['Newspaper_OpenToPublic'];
			$this->Newspaper_Skin 		  		  	 = $Newspaper_Data_ary['Newspaper_Skin'];
			$this->Newspaper_SchoolLogo			  	 = $Newspaper_Data_ary['Newspaper_SchoolLogo'];
			$this->Newspaper_CoverBanner		  	 = $Newspaper_Data_ary['Newspaper_CoverBanner'];
			$this->Newspaper_CoverBannerHideTitle 	 = $Newspaper_Data_ary['Newspaper_CoverBannerHideTitle'];
			$this->Newspaper_CoverBannerHideName  	 = $Newspaper_Data_ary['Newspaper_CoverBannerHideName'];
			$this->Newspaper_InsideBanner		  	 = $Newspaper_Data_ary['Newspaper_InsideBanner'];
			$this->Newspaper_InsideBannerHideTitle 	 = $Newspaper_Data_ary['Newspaper_InsideBannerHideTitle'];
			$this->Newspaper_InsideBannerHideName  	 = $Newspaper_Data_ary['Newspaper_InsideBannerHideName'];
			$this->Newspaper_InsideBannerHidePageCat = $Newspaper_Data_ary['Newspaper_InsideBannerHidePageCat'];
			$this->Newspaper_NoOfView 	  		  	 = $Newspaper_Data_ary['Newspaper_NoOfView'];
			$this->Newspaper_PublishStatus		  	 = $Newspaper_Data_ary['Newspaper_PublishStatus'];
			$this->Newspaper_Status 	  		  	 = $Newspaper_Data_ary['Newspaper_Status'];
			$this->Newspaper_InputDate 	  		  	 = $Newspaper_Data_ary['Newspaper_InputDate'];
			$this->Newspaper_ModifiedBy   		  	 = $Newspaper_Data_ary['Newspaper_ModifiedBy'];
			$this->Newspaper_ModifiedDate 		  	 = $Newspaper_Data_ary['Newspaper_ModifiedDate'];
			$this->Newspaper_ModifiedByUser		  	 = $Newspaper_Data_ary['Newspaper_ModifiedByUser'];
			$this->Newspaper_EnglishName  		  	 = $Newspaper_Data_ary['Newspaper_EnglishName'];
			$this->Newspaper_ClassName 	  		  	 = $Newspaper_Data_ary['Newspaper_ClassName'];
			$this->Newspaper_ClassNumber  		  	 = $Newspaper_Data_ary['Newspaper_ClassNumber'];
			$this->Newspaper_PageIDs	  		  	 = $Newspaper_Data_ary['Newspaper_PageIDs'];
			$this->Newspaper_StudentEditorUserIDs 	 = $Newspaper_Data_ary['Newspaper_StudentEditorUserIDs'];
			$this->Newspaper_StudentEditors 	  	 = $Newspaper_Data_ary['Newspaper_StudentEditors'];
		}
		
		function newspaper_obj_to_array(){
			# Get Folder Onject Array
			$Folder_Data_ary = $this->folder_obj_to_array();
			
			$Newspaper_Data_ary['NewspaperID'] 			 		  	 = $this->NewspaperID;
			$Newspaper_Data_ary['Newspaper_Title'] 				  	 = $this->Newspaper_Title;
			$Newspaper_Data_ary['Newspaper_Name'] 				  	 = $this->Newspaper_Name;
			$Newspaper_Data_ary['Newspaper_IssueDate'] 			  	 = $this->Newspaper_IssueDate;
			$Newspaper_Data_ary['Newspaper_OpenToPublic'] 		  	 = $this->Newspaper_OpenToPublic;
			$Newspaper_Data_ary['Newspaper_Skin'] 				  	 = $this->Newspaper_Skin;
			$Newspaper_Data_ary['Newspaper_SchoolLogo']			  	 = $this->Newspaper_SchoolLogo;
			$Newspaper_Data_ary['Newspaper_CoverBanner'] 			 = $this->Newspaper_CoverBanner;
			$Newspaper_Data_ary['Newspaper_CoverBannerHideTitle'] 	 = $this->Newspaper_CoverBannerHideTitle;
			$Newspaper_Data_ary['Newspaper_CoverBannerHideName'] 	 = $this->Newspaper_CoverBannerHideName;
			$Newspaper_Data_ary['Newspaper_InsideBanner'] 			 = $this->Newspaper_InsideBanner;
			$Newspaper_Data_ary['Newspaper_InsideBannerHideTitle'] 	 = $this->Newspaper_InsideBannerHideTitle;
			$Newspaper_Data_ary['Newspaper_InsideBannerHideName'] 	 = $this->Newspaper_InsideBannerHideName;
			$Newspaper_Data_ary['Newspaper_InsideBannerHidePageCat'] = $this->Newspaper_InsideBannerHidePageCat;
			$Newspaper_Data_ary['Newspaper_NoOfView'] 	  		  	 = $this->Newspaper_NoOfView;
			$Newspaper_Data_ary['Newspaper_PublishStatus'] 		  	 = $this->Newspaper_PublishStatus;
			$Newspaper_Data_ary['Newspaper_Status'] 	  		  	 = $this->Newspaper_Status;
			$Newspaper_Data_ary['Newspaper_InputDate'] 	  		  	 = $this->Newspaper_InputDate;
			$Newspaper_Data_ary['Newspaper_ModifiedBy']   		   	 = $this->Newspaper_ModifiedBy;
			$Newspaper_Data_ary['Newspaper_ModifiedDate'] 		  	 = $this->Newspaper_ModifiedDate;
			$Newspaper_Data_ary['Newspaper_ModifiedByUser']   	  	 = $this->Newspaper_ModifiedByUser;
			$Newspaper_Data_ary['Newspaper_EnglishName']  		  	 = $this->Newspaper_EnglishName;
			$Newspaper_Data_ary['Newspaper_ClassName']	  		  	 = $this->Newspaper_ClassName;
			$Newspaper_Data_ary['Newspaper_ClassNumber'] 		  	 = $this->Newspaper_ClassNumber;
			$Newspaper_Data_ary['Newspaper_PageIDs']	  		  	 = $this->Newspaper_PageIDs;
			$Newspaper_Data_ary['Newspaper_StudentEditorUserIDs'] 	 = $this->Newspaper_StudentEditorUserIDs;
			$Newspaper_Data_ary['Newspaper_StudentEditors'] 	  	 = $this->Newspaper_StudentEditors;
			
			$Newspaper_Data_ary = array_merge($Folder_Data_ary, $Newspaper_Data_ary);
			
			return $Newspaper_Data_ary;
		}
		
		function get_newspaper_raw_ary($NewspaperID){
			global $cfg_ePost;
			
			# Newspaper Raw Array
			$PageIDSeparator = '|=|';
			
			$sql = "SELECT
						en.NewspaperID,
						en.FolderID,
						en.Title AS Newspaper_Title,
						en.Name AS Newspaper_Name,
						en.IssueDate AS Newspaper_IssueDate,
						en.OpenToPublic AS Newspaper_OpenToPublic,
						en.Skin AS Newspaper_Skin,
						en.SchoolLogo AS Newspaper_SchoolLogo,
						en.CoverBanner AS Newspaper_CoverBanner,
						en.CoverBannerHideTitle AS Newspaper_CoverBannerHideTitle,
						en.CoverBannerHideName AS Newspaper_CoverBannerHideName,
						en.InsideBanner AS Newspaper_InsideBanner,
						en.InsideBannerHideTitle AS Newspaper_InsideBannerHideTitle,
						en.InsideBannerHideName AS Newspaper_InsideBannerHideName,
						en.InsideBannerHidePageCat AS Newspaper_InsideBannerHidePageCat,
						en.NoOfView AS Newspaper_NoOfView,
						en.PublishStatus AS Newspaper_PublishStatus,
						en.Status AS Newspaper_Status,
						en.InputDate AS Newspaper_InputDate,
						en.ModifiedBy AS Newspaper_ModifiedBy,
						en.ModifiedDate AS Newspaper_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Newspaper_ModifiedByUser,
						iu.EnglishName AS Newspaper_EnglishName,
						iu.ClassName AS Newspaper_ClassName,
						iu.ClassNumber AS Newspaper_ClassNumber,
						GROUP_CONCAT(DISTINCT ep.PageID ORDER BY ep.PageOrder SEPARATOR '$PageIDSeparator') AS Newspaper_PageIDs
					FROM
						EPOST_NEWSPAPER AS en INNER JOIN
						INTRANET_USER AS iu ON en.ModifiedBy = iu.UserID LEFT JOIN
						EPOST_NEWSPAPER_PAGE AS ep ON en.NewspaperID = ep.NewspaperID AND ep.Status = '".$cfg_ePost['BigNewspaper']['Page_status']['exist']."'
					WHERE
						en.NewspaperID = '$NewspaperID'
					GROUP By
						en.NewspaperID";
			$result_ary = current($this->returnArray($sql)); 
			
			# Get Newspaper Student Editor
			$sql = "SELECT
						esem.ManageItemID,
						iu.UserID,
						iu.EnglishName,
						iu.ClassName,
						iu.ClassNumber
					FROM
						EPOST_STUDENT_EDITOR_MANAGING AS esem INNER JOIN
						EPOST_STUDENT_EDITOR AS ese ON esem.EditorID = ese.EditorID INNER JOIN
						INTRANET_USER AS iu ON ese.UserID = iu.UserID
					WHERE
						ese.Status = '".$cfg_ePost['BigNewspaper']['StudentEditor_status']['exist']."' AND
						esem.ManageItemID = '".$result_ary['NewspaperID']."' AND
						esem.ManageType = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_type']['Newspaper']."' AND
						esem.Status = '".$cfg_ePost['BigNewspaper']['StudentEditorManaging_status']['exist']."'
					ORDER BY
						iu.ClassName, iu.ClassNumber, iu.EnglishName";
			$StudentEditor_ary = $this->returnArray($sql);
			
			$result_ary['Newspaper_PageIDs'] 			  = $result_ary['Newspaper_PageIDs']? explode($PageIDSeparator, $result_ary['Newspaper_PageIDs']) : array();
			$result_ary['Newspaper_StudentEditors'] 	  = array();
			$result_ary['Newspaper_StudentEditorUserIDs'] = array();
					
			for($i=0;$i<count($StudentEditor_ary);$i++){
				$result_ary['Newspaper_StudentEditorUserIDs'][] = $StudentEditor_ary[$i]['UserID'];
				$result_ary['Newspaper_StudentEditors'][] 	    = array("UserID"	  => $StudentEditor_ary[$i]['UserID'],
																  		"EnglishName" => $StudentEditor_ary[$i]['EnglishName'],
																  		"ClassName"	  => $StudentEditor_ary[$i]['ClassName'],
																  		"ClassNumber" => $StudentEditor_ary[$i]['ClassNumber']);
			}
			
			# Folder Raw Array
			$Folder_Data_ary = $this->get_folder_raw_ary($result_ary['FolderID']);
			
			$result_ary = array_merge($Folder_Data_ary, $result_ary);
			
			return $result_ary;
		}
		
		function get_pages_raw_ary(){
			global $cfg_ePost;
			
			$result_ary = array();
			
			if(count($this->Newspaper_PageIDs) > 0){
				$ArticleIDSeparator = '|=|';
				
				$sql = "SELECT
							ep.PageID,
							ep.NewspaperID,
							ep.Type AS Page_Type,
							ep.Layout AS Page_Layout,
							ep.Theme AS Page_Theme,
							ep.Name AS Page_Name,
							ep.PageOrder AS Page_PageOrder,
							ep.Status AS Page_Status,
							ep.InputDate AS Page_InputDate,
							ep.ModifiedBy AS Page_ModifiedeBy,
							ep.ModifiedDate AS Page_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Page_ModifiedByUser,
							iu.EnglishName AS Page_EnglishName,
							iu.ClassName AS Page_ClassName,
							iu.ClassNumber AS Page_ClassNumber,
							GROUP_CONCAT(DISTINCT ea.ArticleID ORDER BY ea.Position SEPARATOR '$ArticleIDSeparator') AS Page_ArticleIDs
						FROM
							EPOST_NEWSPAPER_PAGE AS ep INNER JOIN
							INTRANET_USER AS iu ON ep.ModifiedBy = iu.UserID LEFT JOIN
							EPOST_NEWSPAPER_PAGE_ARTICLE AS ea ON ep.PageID = ea.PageID AND ea.Status = '".$cfg_ePost['BigNewspaper']['Article_status']['exist']."'
						WHERE
							ep.PageID IN ('".implode("','", $this->Newspaper_PageIDs)."') AND
							ep.Status = '".$cfg_ePost['BigNewspaper']['Page_status']['exist']."'
						GROUP BY
							ep.PageID
						ORDER BY
							ep.PageOrder";
				$result_ary = $this->returnArray($sql); 
				
				# Process Page ArticleID array
				for($i=0;$i<count($result_ary);$i++){
					$result_ary[$i]['Page_ArticleIDs'] = $result_ary[$i]['Page_ArticleIDs']? explode($ArticleIDSeparator, $result_ary[$i]['Page_ArticleIDs']) : array();
				}
			}
			
			return $result_ary;
		}
		
		function get_pages(){
			if(count($this->Newspaper_Pages)==0){
				$Newspaper_Data_ary = $this->newspaper_obj_to_array();
				$pages_raw_ary 		= $this->get_pages_raw_ary();
				
				for($i=0;$i<count($pages_raw_ary);$i++){
					$Page_Data_ary = array_merge($Newspaper_Data_ary, $pages_raw_ary[$i]);
					$this->Newspaper_Pages[$i] = new libepost_page(0, $Page_Data_ary);
				}
			}
			
			return $this->Newspaper_Pages;
		}
		
		function update_NoOfView(){
			$this->Newspaper_NoOfView = $this->Newspaper_NoOfView + 1;
			$this->update_newspaper_db();
		}
		
		function update_PublishStatus($PublishStatus){
			$this->Newspaper_PublishStatus = $PublishStatus;
			$this->update_newspaper_db();
		}
		
		function update_newspaper_db(){
			global $PATH_WRT_ROOT, $cfg_ePost, $UserID;
			
			if($this->NewspaperID){
				$NewspaperID = $this->NewspaperID;
				$InputDate   = $this->Newspaper_InputDate;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_NEWSPAPER
						SET
							FolderID = '".$this->FolderID."',
							Title = '".$this->Newspaper_Title."',
							Name = '".$this->Newspaper_Name."',
							IssueDate = '".date('Y-m-d', strtotime($this->Newspaper_IssueDate))." 00:00:00',
							OpenToPublic = '".$this->Newspaper_OpenToPublic."',
							Skin = '".$this->Newspaper_Skin."',
							SchoolLogo = '".$this->Newspaper_SchoolLogo."',
							CoverBanner = '".$this->Newspaper_CoverBanner."',
							CoverBannerHideTitle = '".$this->Newspaper_CoverBannerHideTitle."',
							CoverBannerHideName = '".$this->Newspaper_CoverBannerHideName."',
							InsideBanner = '".$this->Newspaper_InsideBanner."',
							InsideBannerHideTitle = '".$this->Newspaper_InsideBannerHideTitle."',
							InsideBannerHideName = '".$this->Newspaper_InsideBannerHideName."',
							InsideBannerHidePageCat = '".$this->Newspaper_InsideBannerHidePageCat."',
							NoOfView = '".$this->Newspaper_NoOfView."',
							PublishStatus = '".$this->Newspaper_PublishStatus."',
							Status = '".$this->Newspaper_Status."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							NewspaperID = '".$this->NewspaperID."'";
				$result = $this->db_db_query($sql);
			}
			else{
				$InputDate   = date('Y-m-d H:i:s');
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_NEWSPAPER
							(
								FolderID, Title, Name, IssueDate, OpenToPublic,
								Skin, SchoolLogo, CoverBanner, CoverBannerHideTitle, CoverBannerHideName,
								InsideBanner, InsideBannerHideTitle, InsideBannerHideName, InsideBannerHidePageCat, NoOfView,
								PublishStatus, Status, Inputdate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								'".$this->FolderID."', '".$this->Newspaper_Title."', '".$this->Newspaper_Name."', '".date('Y-m-d', strtotime($this->Newspaper_IssueDate))." 00:00:00', '".$this->Newspaper_OpenToPublic."',
								'".$this->Newspaper_Skin."', '".$this->Newspaper_SchoolLogo."', '".$this->Newspaper_CoverBanner."', '".$this->Newspaper_CoverBannerHideTitle."', '".$this->Newspaper_CoverBannerHideName."',
								'".$this->Newspaper_InsideBanner."', '".$this->Newspaper_InsideBannerHideTitle."', '".$this->Newspaper_InsideBannerHideName."', '".$this->Newspaper_InsideBannerHidePageCat."', 0,
								'".$this->Newspaper_PublishStatus."', '".$this->Newspaper_Status."', '$InputDate', $UserID, '$ModifedDate'
							)";
				$result 	 = $this->db_db_query($sql);
				$NewspaperID = $this->db_insert_id();
			}
			
			if($result){
				$this->NewspaperID			  = $NewspaperID;
				$this->Newspaper_InputDate	  = $InputDate;
				$this->Newspaper_ModifiedBy   = $UserID;
				$this->Newspaper_ModifiedDate = $ModifedDate;
				
				# Handle SchoolLogo, CoverBanner and InsideBanner
				$this->Newspaper_SchoolLogo   = $this->update_newspaper_file($cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']);
				$this->Newspaper_CoverBanner  = $this->update_newspaper_file($cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']);
				$this->Newspaper_InsideBanner = $this->update_newspaper_file($cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']);
				
				# Update the DB for the real location
				$sql = "UPDATE
							EPOST_NEWSPAPER
						SET
							SchoolLogo = '".$this->Newspaper_SchoolLogo."',
							CoverBanner = '".$this->Newspaper_CoverBanner."',
							InsideBanner = '".$this->Newspaper_InsideBanner."'
						WHERE
							NewspaperID = '$NewspaperID'";
				
				if(!$this->db_db_query($sql)){
					$this->Newspaper_SchoolLogo   = '';
					$this->Newspaper_CoverBanner  = '';
					$this->Newspaper_InsideBanner = '';
				}
				
				$sql = "SELECT EnglishName, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				
				$this->Newspaper_EnglishName  = $ModifiedBy_ary['EnglishName'];
				$this->Newspaper_ClassName 	  = $ModifiedBy_ary['ClassName'];
				$this->Newspaper_ClassNumber  = $ModifiedBy_ary['ClassNumber'];
			}
			
			return $result;
		}
		
		function update_newspaper_file($FileType){
			global $PATH_WRT_ROOT, $cfg_ePost;
			
			switch($FileType){
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']: 
	    			$FileName = $this->Newspaper_SchoolLogo;
	    			break;
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']:
	    			$FileName = $this->Newspaper_CoverBanner;
	    			break;
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']:
	    			$FileName = $this->Newspaper_InsideBanner;
	    			break;
	    	}
	    	
			$result_file_name = '';
			
			# Check if upload attachment is image or not
			if(preg_match('/^\/tmp\/'.$FileType.'\//', $FileName) && !IsImage($PATH_WRT_ROOT.'file/ePost/newspaper_attachment'.$FileName)){
				return '';
			}
			
			if(!$FileName || preg_match('/^\/tmp\/'.$FileType.'\//', $FileName)){
				$dir = $PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID.'/'.$FileType.'/';
				
				# Clear All Previous Uploaded Files
				if(is_dir($dir)){
					$cur_dir = opendir($dir);
				
					while(($file = readdir($cur_dir))!==false){
						if($file!='.' && $file!='..'){
							chmod($dir.$file, 0777);
							if(!is_dir($dir.$file))
								unlink($dir.$file);
						}
					}
				}
					
				if($FileName){
					# Create corresponding folders if not exist
					if(!is_dir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID))
						 mkdir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID, 0777);
					if(!is_dir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID.'/'.$FileType))
						 mkdir($PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID.'/'.$FileType, 0777);
					
					# Move the school logo from temp location to real location
					$source_file 	  = $PATH_WRT_ROOT.'file/ePost/newspaper_attachment'.$FileName;
					$destination_file = $PATH_WRT_ROOT.'file/ePost/newspaper_attachment/'.$this->NewspaperID.'/'.$FileType.'/'.get_file_basename($FileName); 
						
					if(is_file($source_file)){
						rename($source_file, $destination_file);
						
						# Update the DB for the real location
						if(is_file($destination_file)){
							rmdir(dirname($source_file));
							$result_file_name = get_file_basename($FileName);
						}
					}
				}
				else{
					if(is_dir($dir)) rmdir($dir);
				}
			}
			else
				$result_file_name = $FileName;
					
			return $result_file_name;
		}
		
		function return_newspaper_attachment_http_path($FileType){
			global $cfg_ePost;
	    	
	    	switch($FileType){
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['SchoolLogo']: 
	    			$FileName = $this->Newspaper_SchoolLogo;
	    			break;
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['CoverBanner']:
	    			$FileName = $this->Newspaper_CoverBanner;
	    			break;
	    		case $cfg_ePost['BigNewspaper']['AttachmentType']['InsideBanner']:
	    			$FileName = $this->Newspaper_InsideBanner;
	    			break;
	    	}
	    	
	    	if(preg_match('/^\/tmp\/'.$FileType.'\//', $FileName))
	    		$path = "/file/ePost/newspaper_attachment/".rawurlencode(addslashes($FileName));
	    	else
	    		$path = "/file/ePost/newspaper_attachment/".$this->NewspaperID."/".$FileType."/".rawurlencode(addslashes($FileName));
	    	
	    	return $path;
		}
		
		function delete_newspaper(){
			global $cfg_ePost, $UserID;
			
			# Delete This Newspaper
			$sql = "UPDATE
						EPOST_NEWSPAPER
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Newspaper_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						NewspaperID = '".$this->NewspaperID."'";
			$result = $this->db_db_query($sql);
			
			# Delete All Pages in this newspaper
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Page_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						PageID IN ('".implode("','", $this->Newspaper_PageIDs)."')";
			$result = $this->db_db_query($sql);
			
			# Delete All Articles in this newspaper
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE_ARTICLE
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Article_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						PageID IN ('".implode("','", $this->Newspaper_PageIDs)."')";
			$result = $this->db_db_query($sql);
			
			return $result;
		}
		
		function update_pages_PageOrder(){
			$result = array();
			
			for($i=0;$i<count($this->Newspaper_PageIDs);$i++){
				$sql = "UPDATE EPOST_NEWSPAPER_PAGE SET PageOrder = '$i' WHERE PageID = '".$this->Newspaper_PageIDs[$i]."'";
				$result[] = $this->db_db_query($sql);
			}
			
			return !in_array(false, $result);
		}
		
		function move_target_page_after_ref_page($TargetPageID, $RefPageID){
			$tmp_PageIDs = array();
			$result 	 = array();
			$IsMoved 	 = false;
			
			# Rearranging the Pages
			if($RefPageID==0){
				$tmp_PageIDs[] = $TargetPageID;
				$IsMoved = true;
			}
			
			for($i=0;$i<count($this->Newspaper_PageIDs);$i++){
				if($this->Newspaper_PageIDs[$i] != $TargetPageID){
					$tmp_PageIDs[] = $this->Newspaper_PageIDs[$i];
				}
				
				if($this->Newspaper_PageIDs[$i] == $RefPageID){
					$tmp_PageIDs[] = $TargetPageID;
					$IsMoved 	   = true;
				}
			}
			
			if(!$IsMoved){
				$tmp_PageIDs[] = $TargetPageID;
			}
			
			# Update the instance of the object
			$this->Newspaper_PageIDs = $tmp_PageIDs;
			$this->Newspaper_Pages 	 = array();
			
			return $this->update_pages_PageOrder();
		}
		
		function get_page_class_suffix(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['page_class_suffix']:"";
			
			return $suffix;
		}
		
		function get_page_big_class_suffix(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['page_big_class_suffix']:"";
			
			return $suffix;
		}
		
		function get_page_body_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['body_class']:"";
			
			return $suffix;
		}
		
		function get_page_top_left_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['top_left_class']:"";
			
			return $suffix;
		}
		
		function get_page_top_right_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['top_right_class']:"";
			
			return $suffix;
		}
		
		function get_page_top_bg_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['top_bg_class']:"";
			
			return $suffix;
		}
		
		function get_page_bg_class(){
			global $cfg_ePost;
			
			$suffix = $this->Newspaper_Skin? $cfg_ePost['BigNewspaper']['skins'][$this->Newspaper_Skin]['bg_class']:"";
			
			return $suffix;
		}
		
		function is_published(){
			global $cfg_ePost;
			
			$result = (strtotime($this->Newspaper_IssueDate) <= time()) && 
					  (count($this->Newspaper_PageIDs)>0) && 
					  ($this->Newspaper_PublishStatus==$cfg_ePost['BigNewspaper']['Newspaper_PublishStatus']['Published']);
			
			return $result;
		}
		
		function is_open_to_public(){
			$result = $this->is_published() && $this->Newspaper_OpenToPublic;
			
			return $result;
		}
	}
	
	class libepost_page extends libepost_newspaper
	{
		var $PageID;
		var $Page_Type;
		var $Page_Layout;
		var $Page_Theme;
		var $Page_Name;
		var $Page_PageOrder;
		var $Page_Status;
		var $Page_InputDate;
		var $Page_ModifiedBy;
		var $Page_ModifiedDate;
		var $Page_ModifiedByUser;
		var $Page_EnglishName;
		var $Page_ClassName;
		var $Page_ClassNumber;
		var $Page_ArticleIDs;
		var $Page_Articles;
		
		function libepost_page($PageID=0, $Page_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $PageID is inputed or not
			$Page_Data_ary = $PageID? $this->get_page_raw_ary($PageID) : $Page_Data_ary;
			
			# Initialize Page Object
			$this->initialize_page_obj($Page_Data_ary);
		}
		
		function initialize_page_obj($Page_Data_ary){
			# Initialize Newspaper Object
			$this->initialize_newspaper_obj($Page_Data_ary);
			
			$this->PageID 			   = $Page_Data_ary['PageID'];
			$this->Page_Type 		   = $Page_Data_ary['Page_Type'];
			$this->Page_Layout 		   = $Page_Data_ary['Page_Layout'];
			$this->Page_Theme 		   = $Page_Data_ary['Page_Theme'];
			$this->Page_Name 		   = $Page_Data_ary['Page_Name'];
			$this->Page_PageOrder 	   = $Page_Data_ary['Page_PageOrder'];
			$this->Page_Status 		   = $Page_Data_ary['Page_Status'];
			$this->Page_InputDate 	   = $Page_Data_ary['Page_InputDate'];
			$this->Page_ModifiedBy 	   = $Page_Data_ary['Page_ModifiedBy'];
			$this->Page_ModifiedDate   = $Page_Data_ary['Page_ModifiedDate'];
			$this->Page_ModifiedByUser = $Page_Data_ary['Page_ModifiedByUser'];
			$this->Page_EnglishName    = $Page_Data_ary['Page_EnglishName'];
			$this->Page_ClassName 	   = $Page_Data_ary['Page_ClassName'];
			$this->Page_ClassNumber    = $Page_Data_ary['Page_ClassNumber'];
			$this->Page_ArticleIDs	   = $Page_Data_ary['Page_ArticleIDs'];
		}
		
		function page_obj_to_array(){
			# Get Newspaper Onject Array
			$Newspaper_Data_ary = $this->newspaper_obj_to_array();
			
			$Page_Data_ary['PageID'] 				  = $this->PageID;
			$Page_Data_ary['Page_Type'] 			  = $this->Page_Type;
			$Page_Data_ary['Page_Layout'] 			  = $this->Page_Layout;
			$Page_Data_ary['Page_Theme'] 			  = $this->Page_Theme;
			$Page_Data_ary['Page_Name'] 			  = $this->Page_Name;
			$Page_Data_ary['Page_PageOrder'] 		  = $this->Page_PageOrder;
			$Page_Data_ary['Page_Status'] 			  = $this->Page_Status;
			$Page_Data_ary['Page_InputDate'] 		  = $this->Page_InputDate;
			$Page_Data_ary['Page_ModifiedBy'] 		  = $this->Page_ModifiedBy;
			$Page_Data_ary['Page_ModifiedDate'] 	  = $this->Page_ModifiedDate;
			$Page_Data_ary['Page_ModifiedByUser'] 	  = $this->Page_ModifiedByUser;
			$Page_Data_ary['Page_EnglishName'] 		  = $this->Page_EnglishName;
			$Page_Data_ary['Page_ClassName'] 		  = $this->Page_ClassName;
			$Page_Data_ary['Page_ClassNumber'] 		  = $this->Page_ClassNumber;
			$Page_Data_ary['Page_ArticleIDs']		  = $this->Page_ArticleIDs;
			$Page_Data_ary['Page_ArticlePositionIDs'] = $this->Page_ArticlePositionIDs;
			
			$Page_Data_ary = array_merge($Newspaper_Data_ary, $Page_Data_ary);
			
			return $Page_Data_ary;
		}
		
		function get_page_raw_ary($PageID){
			global $cfg_ePost;
			
			$ArticleIDSeparator = '|=|';
				
			$sql = "SELECT
						ep.PageID,
						ep.NewspaperID,
						ep.Type AS Page_Type,
						ep.Layout AS Page_Layout,
						ep.Theme AS Page_Theme,
						ep.Name AS Page_Name,
						ep.PageOrder AS Page_PageOrder,
						ep.Status AS Page_Status,
						ep.InputDate AS Page_InputDate,
						ep.ModifiedBy AS Page_ModifiedBy,
						ep.ModifiedDate AS Page_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Page_ModifiedByUser,
						iu.EnglishName AS Page_EnglishName,
						iu.ClassName AS Page_ClassName,
						iu.ClassNumber AS Page_ClassNumber,
						GROUP_CONCAT(DISTINCT ea.ArticleID ORDER BY ea.Position SEPARATOR '$ArticleIDSeparator') AS Page_ArticleIDs
					FROM
						EPOST_NEWSPAPER_PAGE AS ep INNER JOIN
						INTRANET_USER AS iu ON ep.ModifiedBy = iu.UserID LEFT JOIN
						EPOST_NEWSPAPER_PAGE_ARTICLE AS ea ON ep.PageID = ea.PageID AND ea.Status = '".$cfg_ePost['BigNewspaper']['Article_status']['exist']."'
					WHERE
						ep.PageID = '$PageID'
					GROUP BY
						ep.PageID";
			$result_ary = current($this->returnArray($sql)); 
			
			# Process Page ArticleID array
			$result_ary['Page_ArticleIDs'] = $result_ary['Page_ArticleIDs']? explode($ArticleIDSeparator, $result_ary['Page_ArticleIDs']) : array();
			
			# Newspaper Raw Array
			$Newspaper_Data_ary = $this->get_newspaper_raw_ary($result_ary['NewspaperID']);
			
			$result_ary = array_merge($Newspaper_Data_ary, $result_ary);
			
			return $result_ary;
		}
		
		function get_articles_raw_ary(){
			global $cfg_ePost;
			
			$result_ary = array();
			
			if(count($this->Page_ArticleIDs) > 0){
				$sql = "SELECT
							ea.ArticleID,
							ea.ArticleShelfID,
							ea.PageID,
							ea.Position AS Article_Position,
							ea.DisplayAuthor AS Article_DisplayAuthor,
							ea.Alignment AS Article_Alignment,
							ea.Size AS Article_Size,
							ea.Type AS Article_Type,
							ea.Theme AS Article_Theme,
							ea.Color AS Article_Color,
							ea.Status AS Article_Status,
							ea.InputDate AS Article_InputDate,
							ea.ModifiedBy AS Article_ModifiedeBy,
							ea.ModifiedDate AS Article_ModifiedDate,
							".getNameFieldByLang('iu.')." AS Article_ModifiedByUser,
							iu.EnglishName AS Article_EnglishName,
							iu.ClassName AS Article_ClassName,
							iu.ClassNumber AS Article_ClassNumber
						FROM
							EPOST_NEWSPAPER_PAGE_ARTICLE AS ea INNER JOIN
							INTRANET_USER AS iu ON ea.ModifiedBy = iu.UserID
						WHERE
							ea.ArticleID IN ('".implode("','", $this->Page_ArticleIDs)."') AND
							ea.Status = '".$cfg_ePost['BigNewspaper']['Article_status']['exist']."'
						ORDER BY
							ea.Position";
				$result_ary = $this->returnArray($sql);
			}
			
			return $result_ary;
		}
		
		function get_articles(){
			if(count($this->Pages_Articles)==0){
				$Page_Data_ary = $this->page_obj_to_array();
				$articles_raw_ary = $this->get_articles_raw_ary();
				
				for($i=1;$i<=$this->return_page_max_article();$i++){
					$ArticleObj = null;
					
					for($j=0;$j<count($articles_raw_ary);$j++){
						if($i==$articles_raw_ary[$j]['Article_Position'])
						{
							$Article_Data_ary = array_merge($Page_Data_ary, $articles_raw_ary[$j]);
							$ArticleObj = new libepost_article(0, $Article_Data_ary);
							break;
						}
					}
					
					$this->Pages_Articles[$i] = $ArticleObj;
				}
			}
			
			return $this->Pages_Articles;
		}
		
		function update_page_db($PageRefPage=0, $IsLayoutChanged=false){
			global $UserID, $cfg_ePost;
			
			if($this->PageID){
				$PageID 	 = $this->PageID;
				$InputDate   = $this->Page_InputDate;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_NEWSPAPER_PAGE
						SET
							NewspaperID = '".$this->NewspaperID."',
							Type = '".$this->Page_Type."',
							Layout = '".$this->Page_Layout."',
							Theme = '".$this->Page_Theme."',
							Name = '".$this->Page_Name."',
							PageOrder = '".$this->Page_PageOrder."',
							Status = '".$this->Page_Status."',
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							PageID = '".$this->PageID."'";
				$result = $this->db_db_query($sql);
				
				# Remove All Articles if the Page Layout is changed
				$result = $result && $IsLayoutChanged? $this->delete_all_articles():$result;
			}
			else{
				$InputDate   = date('Y-m-d H:i:s');
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_NEWSPAPER_PAGE
							(
								NewspaperID, Type, Layout, Theme, Name,
								PageOrder, Status, Inputdate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								'".$this->NewspaperID."', '".$this->Page_Type."', '".$this->Page_Layout."', '".$this->Page_Theme."', '".$this->Page_Name."',
								'".$this->Page_PageOrder."', '".$this->Page_Status."', '$InputDate', '$UserID', '$ModifedDate'
							)";
				$result = $this->db_db_query($sql);
				$PageID = $this->db_insert_id();
			}
			
			$result = $result? $this->move_target_page_after_ref_page($PageID, $PageRefPage) : $result;
			
			if($result){
				$sql = "SELECT EnglishName, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				
				$this->PageID			 = $PageID;
				$this->Page_InputDate	 = $InputDate;
				$this->Page_ModifiedBy   = $UserID;
				$this->Page_ModifiedDate = $ModifedDate;
				$this->Page_EnglishName  = $ModifiedBy_ary['EnglishName'];
				$this->Page_ClassName 	 = $ModifiedBy_ary['ClassName'];
				$this->Page_ClassNumber  = $ModifiedBy_ary['ClassNumber'];
			}
			
			return $result;
		}
		
		function delete_page(){
			global $cfg_ePost, $UserID;
			
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Page_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						PageID = '".$this->PageID."'";
			$result = $this->db_db_query($sql);
			
			$result = $this->delete_all_articles();
			
			# Update Pages PageOrder
			$PageIDs_ary_key = array_search($this->PageID, $this->Newspaper_PageIDs);
			
			if($PageIDs_ary_key!==false){
				array_splice($this->Newspaper_PageIDs, $PageIDs_ary_key, 1);
				
				if(count($this->Newspaper_Pages)){
					array_splice($this->Newspaper_Pages, $PageIDs_ary_key, 1);
				}
				
				if(count($this->Newspaper_PageIDs)){
					$result = $this->update_pages_PageOrder();
				}
			}
			return $result;
		}
		
		function delete_all_articles(){
			global $cfg_ePost, $UserID;
			
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE_ARTICLE
					SET
						Status = '".$cfg_ePost['BigNewspaper']['Article_status']['deleted']."',
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						PageID = '".$this->PageID."'";
			$result = $this->db_db_query($sql);
			
			return $result;
		}
		
		function return_page_type(){
			return $this->Page_Type[0];
		}
		
		function return_page_max_article(){
			global $cfg_ePost;
			
			return count($cfg_ePost['BigNewspaper']['TypeLayout'][$this->Page_Type][$this->Page_Layout]['articles']);
		}
		
		function return_layout_image_path(){
			global $intranet_httppath, $cfg_ePost;
			
			$img_path = $cfg_ePost['BigNewspaper']['TypeLayout'][$this->Page_Type][$this->Page_Layout]['img']? $intranet_httppath."/images/ePost/issue_layout/".$cfg_ePost['BigNewspaper']['TypeLayout'][$this->Page_Type][$this->Page_Layout]['img'] : "";
			
			return $img_path;
		}
		
		function return_theme_image_path(){
			global $intranet_httppath, $cfg_ePost;
			
			$img_path = $cfg_ePost['BigNewspaper']['Theme'][$this->Page_Theme]['big_img']? $intranet_httppath."/images/ePost/newspaper/page_cat/".$cfg_ePost['BigNewspaper']['Theme'][$this->Page_Theme]['big_img'] : "";
			
			return $img_path;
		}
		
		function get_page_format(){
			global $cfg_ePost;
			
			if($this->return_page_type()==$cfg_ePost['BigNewspaper']['PageType']['Cover']){
				$page_format = 'C';
			}
			else{
				if(($this->Page_PageOrder % 2)==1)
					$page_format = 'L';
				else
					$page_format = 'R';
			}
			
			return $page_format;
		}
		
		function get_page_TypeLayout_articles_ary(){
			global $cfg_ePost;
			
			return $cfg_ePost['BigNewspaper']['TypeLayout'][$this->Page_Type][$this->Page_Layout]['articles'];
		}
		
		function get_article_restriction_name_ary($position){
			global $cfg_ePost;
			
			$article_cfg_ary = $this->get_page_TypeLayout_articles_ary();
			$article_cfg 	 = $article_cfg_ary[$position];
			$name_ary		 = array();
			
			$all_article_type_ary = $cfg_ePost['BigNewspaper']['ArticleType']['courseware']['types'];
			
			foreach($all_article_type_ary as $article_tpye=>$article_tpye_ary){
				if(!in_array($article_tpye, $article_cfg['restriction']))
					$name_ary[] = $article_tpye_ary['name'];
			}
			
			return $name_ary;
		}
	}
	
	class libepost_article extends libepost_page
	{
		var $ArticleID;
		var $ArticleShelfID;
		var $Article_Position;
		var $Article_DisplayAuthor;
		var $Article_Alignment;
		var $Article_Size;
		var $Article_Type;
		var $Article_Theme;
		var $Article_Color;
		var $Article_Status;
		var $Article_InputDate;
		var $Article_ModifiedBy;
		var $Article_ModifiedDate;
		var $Article_ModifiedByUser;
		var $Article_EnglishName;
		var $Article_ClassName;
		var $Article_ClassNumber;
		
		function libepost_article($ArticleID=0, $Article_Data_ary=array()){
			# Initialize ePost Object
			$this->libepost();
			
			# Check if $ArticleID is inputed or not
			$Article_Data_ary = $ArticleID? $this->get_article_raw_ary($ArticleID) : $Article_Data_ary;
			
			# Initialize Article Object
			$this->initialize_article_obj($Article_Data_ary);
		}
		
		function initialize_article_obj($Article_Data_ary){
			# Initialize Page Object
			$this->initialize_page_obj($Article_Data_ary);
			
			$this->ArticleID 			  = $Article_Data_ary['ArticleID'];
			$this->ArticleShelfID		  = $Article_Data_ary['ArticleShelfID'];
			$this->Article_Position 	  = $Article_Data_ary['Article_Position'];
			$this->Article_DisplayAuthor  = $Article_Data_ary['Article_DisplayAuthor'];
			$this->Article_Alignment 	  = $Article_Data_ary['Article_Alignment'];
			$this->Article_Size 		  = $Article_Data_ary['Article_Size'];
			$this->Article_Type 		  = $Article_Data_ary['Article_Type'];
			$this->Article_Theme 		  = $Article_Data_ary['Article_Theme'];
			$this->Article_Color 		  = $Article_Data_ary['Article_Color'];
			$this->Article_Status 		  = $Article_Data_ary['Article_Status'];
			$this->Article_InputDate 	  = $Article_Data_ary['Article_InputDate'];
			$this->Article_ModifiedBy 	  = $Article_Data_ary['Article_ModifiedBy'];
			$this->Article_ModifiedDate   = $Article_Data_ary['Article_ModifiedDate'];
			$this->Article_ModifiedByUser = $Article_Data_ary['Article_ModifiedByUser'];
			$this->Article_EnglishName 	  = $Article_Data_ary['Article_EnglishName'];
			$this->Article_ClassName 	  = $Article_Data_ary['Article_ClassName'];
			$this->Article_ClassNumber    = $Article_Data_ary['Article_ClassNumber'];
		}
		
		function article_obj_to_array(){
			# Get Page Object Array
			$Page_Data_ary = $this->page_obj_to_array();
			
			$Article_Data_ary['ArticleID'] 			    = $this->ArticleID;
			$Article_Data_ary['ArticleShelfID'] 	    = $this->ArticleShelfID;
			$Article_Data_ary['Article_Position'] 	    = $this->Article_Position;
			$Article_Data_ary['Article_DisplayAuthor']  = $this->Article_DisplayAuthor;
			$Article_Data_ary['Article_Alignment'] 	    = $this->Article_Alignment;
			$Article_Data_ary['Article_Size'] 		    = $this->Article_Size;
			$Article_Data_ary['Article_Type'] 		    = $this->Article_Type;
			$Article_Data_ary['Article_Theme'] 		    = $this->Article_Theme;
			$Article_Data_ary['Article_Color'] 		    = $this->Article_Color;
			$Article_Data_ary['Article_Status'] 	    = $this->Article_Status;
			$Article_Data_ary['Article_InputDate'] 	    = $this->Article_InputDate;
			$Article_Data_ary['Article_ModifiedBy']     = $this->Article_ModifiedBy;
			$Article_Data_ary['Article_ModifiedDate']   = $this->Article_ModifiedDate;
			$Article_Data_ary['Article_ModifiedByUser'] = $this->Article_ModifiedByUser;
			$Article_Data_ary['Article_EnglishName']    = $this->Article_EnglishName;
			$Article_Data_ary['Article_ClassName'] 	    = $this->Article_ClassName;
			$Article_Data_ary['Article_ClassNumber']    = $this->Article_ClassNumber;
			
			$Article_Data_ary = array_merge($Page_Data_ary, $Article_Data_ary);
			
			return $Article_Data_ary;
		}
		
		function get_article_raw_ary($ArticleID){
			
			global $cfg_ePost;
			
			$sql = "SELECT
						ea.ArticleID,
						ea.ArticleShelfID,
						ea.PageID,
						ea.Position AS Article_Position,
						ea.DisplayAuthor AS Article_DisplayAuthor,
						ea.Alignment AS Article_Alignment,
						ea.Size AS Article_Size,
						ea.Type AS Article_Type,
						ea.Theme AS Article_Theme,
						ea.Color AS Article_Color,
						ea.Status AS Article_Status,
						ea.InputDate AS Article_InputDate,
						ea.ModifiedBy AS Article_ModifiedeBy,
						ea.ModifiedDate AS Article_ModifiedDate,
						".getNameFieldByLang('iu.')." AS Article_ModifiedByUser,
						iu.EnglishName AS Article_EnglishName,
						iu.ClassName AS Article_ClassName,
						iu.ClassNumber AS Article_ClassNumber
					FROM
						EPOST_NEWSPAPER_PAGE_ARTICLE AS ea INNER JOIN
						INTRANET_USER AS iu ON ea.ModifiedBy = iu.UserID
					WHERE
						ea.ArticleID = '$ArticleID'
					ORDER BY
						ea.Position";
			$result_ary = current($this->returnArray($sql)); 
			
			# Page Raw Array
			$Page_Data_ary = $this->get_page_raw_ary($result_ary['PageID']);
			
			$result_ary = array_merge($Page_Data_ary, $result_ary);
			
			return $result_ary;
		}
		
		function update_article_db(){
			global $UserID, $cfg_ePost;
			
			if($this->ArticleID){
				$ArticleID 	 = $this->ArticleID;
				$InputDate   = $this->Article_InputDate;
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "UPDATE
							EPOST_NEWSPAPER_PAGE_ARTICLE
						SET
							ArticleShelfID = '".$this->ArticleShelfID."',
							PageID = '".$this->PageID."',
							Position = '".$this->Article_Position."',
							DisplayAuthor = '".$this->Article_DisplayAuthor."',
							Alignment = '".$this->Article_Alignment."',
							Size = ".(is_null($this->Article_Size)? "NULL":"'".$this->Article_Size."'").",
							Type = '".$this->Article_Type."',
							Theme = '".$this->Article_Theme."',
							Color = '".$this->Article_Color."',
							Status = ".$this->Article_Status.",
							ModifiedBy = '$UserID',
							ModifiedDate = '$ModifedDate'
						WHERE
							ArticleID = ".$this->ArticleID;
				$result = $this->db_db_query($sql);
			}
			else{
				$InputDate   = date('Y-m-d H:i:s');
				$ModifedDate = date('Y-m-d H:i:s');
				
				$sql = "INSERT INTO
							EPOST_NEWSPAPER_PAGE_ARTICLE
							(
								ArticleShelfID, PageID, DisplayAuthor, Position, Alignment,
								Size, Type, Theme, Color, Status,
								Inputdate, ModifiedBy, ModifiedDate
							)
						VALUES
							(
								'".$this->ArticleShelfID."', '".$this->PageID."', '".$this->Article_DisplayAuthor."', '".$this->Article_Position."', '".$this->Article_Alignment."',
								".(is_null($this->Article_Size)? "NULL":"'".$this->Article_Size."'").", '".$this->Article_Type."', '".$this->Article_Theme."', '".$this->Article_Color."', '".$this->Article_Status."',
								'$InputDate', '$UserID', '$ModifedDate'
							)";
				$result = $this->db_db_query($sql);
				$ArticleID = $this->db_insert_id();
			}
			
			if($result){
				$sql = "SELECT EnglishName, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$UserID'";
				$ModifiedBy_ary = current($this->returnArray($sql));
				
				$this->ArticleID		    = $ArticleID;
				$this->Article_InputDate	= $InputDate;
				$this->Article_ModifiedBy   = $UserID;
				$this->Article_ModifiedDate = $ModifedDate;
				$this->Article_EnglishName  = $ModifiedBy_ary['EnglishName'];
				$this->Article_ClassName 	= $ModifiedBy_ary['ClassName'];
				$this->Article_ClassNumber  = $ModifiedBy_ary['ClassNumber'];
			}
			
			return $result;
		}
		
		function delete_article(){
			global $cfg_ePost, $UserID;
			
			$sql = "UPDATE
						EPOST_NEWSPAPER_PAGE_ARTICLE
					SET
						Status = ".$cfg_ePost['BigNewspaper']['Article_status']['deleted'].",
						ModifiedBy = '$UserID',
						ModifiedDate = NOW()
					WHERE
						ArticleID = ".$this->ArticleID;
			$result = $this->db_db_query($sql);
			
			return $result;
		}
	}
}