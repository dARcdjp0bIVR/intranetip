<?php
/** [Modification Log] Modifying By: ivan
 * *******************************************

 * *******************************************
 */ 
if (!defined("LIBREPORTCARD_FILE"))         // Preprocessor directives
{
	define("LIBREPORTCARD_FILE", true);
	
	include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
	class libreportcard_file extends libdbobject {
		private $lreportcard;
		
		private $fileId;
		private $reportId;
		private $fileType;
		private $filePath;
		private $inputDate;
		private $inputBy;
		private $modifiedDate;
		private $modifiedBy;
		
		
		public function __construct($objectId='') {
			$this->lreportcard = new libreportcardcustom();
			parent::__construct($this->lreportcard->DBName.'.RC_REPORT_TEMPLATE_FILE', 'FileID', $this->returnFieldMappingAry(), $objectId);
		}
		
		public function setFileId($val) {
			$this->fileId = $val;
		}
		public function getFileId() {
			return $this->fileId;
		}
		
		public function setReportId($val) {
			$this->reportId = $val;
		}
		public function getReportId() {
			return $this->reportId;
		}
		
		public function setFileType($val) {
			$this->fileType = $val;
		}
		public function getFileType() {
			return $this->fileType;
		}
		
		public function setFilePath($val) {
			$this->filePath = $val;
		}
		public function getFilePath() {
			return $this->filePath;
		}
		
		public function setInputDate($val) {
			$this->inputDate = $val;
		}
		public function getInputDate() {
			return $this->inputDate;
		}
		
		public function setInputBy($val) {
			$this->inputBy = $val;
		}
		public function getInputBy() {
			return $this->inputBy;
		}
		
		public function setModifiedDate($val) {
			$this->modifiedDate = $val;
		}
		public function getModifiedDate() {
			return $this->modifiedDate;
		}
		
		public function setModifiedBy($val) {
			$this->modifiedBy = $val;
		}
		public function getModifiedBy() {
			return $this->modifiedBy;
		}
		
		private function returnFieldMappingAry() {
			$fieldMappingAry = array();
			$fieldMappingAry[] = array('FileID', 'int', 'setFileId', 'getFileId');
			$fieldMappingAry[] = array('ReportID', 'int', 'setReportId', 'getReportId');
			$fieldMappingAry[] = array('FileType', 'text', 'setFileType', 'getFileType');
			$fieldMappingAry[] = array('FilePath', 'text', 'setFilePath', 'getFilePath');
			$fieldMappingAry[] = array('InputDate', 'date', 'setInputDate', 'getInputDate');
			$fieldMappingAry[] = array('InputBy', 'int', 'setInputBy', 'getInputBy');
			$fieldMappingAry[] = array('ModifiedDate', 'date', 'setModifiedDate', 'getModifiedDate');
			$fieldMappingAry[] = array('ModifiedBy', 'int', 'setModifiedBy', 'getModifiedBy');
			return $fieldMappingAry;
		}
				
		protected function newRecordBeforeHandling() {
			$this->setInputDate('now()');
			$this->setInputBy($_SESSION['UserID']);
			$this->setModifiedDate('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			return true;
		}
		
		protected function updateRecordBeforeHandling() {
			$this->setModifiedDate('now()');
			$this->setModifiedBy($_SESSION['UserID']);
			return true;
		}
		
		
		public function uploadFile($fileLocation, $fileName) {
			global $PATH_WRT_ROOT, $intranet_root;
			
			$successAry = array();
			$path = $this->saveFileToServer($fileLocation, $fileName);
			if ($path == ''){
				$successAry['uploadFile'] = false;
			}
			else {
				$successAry['uploadFile'] = true;
				
				$this->setFilePath($path);
				$successAry['savePath'] = $this->save();
			}
			return in_array(false, $successAry)? false : true;
		}
		
		public function deleteFile() {
			global $PATH_WRT_ROOT, $intranet_root;
			
			$successAry = array();
			$successAry['deleteFile'] = $this->deleteFileFromServer();
			
			$this->setFilePath('');
			$successAry['savePath'] = $this->save();
			
			return in_array(false, $successAry)? false : true;
		}
		
		
		
		private function saveFileToServer($fileLocation, $fileName) {
			global $PATH_WRT_ROOT, $intranet_root;
			
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$fs = new libfilesystem();
			
			$reportId = $this->getReportID();
			$fileType = $this->getFileType();
			$successAry = array();
			
			### Create Folder
			$dbFolderPathPrefix = '/file/reportcard2008/'.$this->lreportcard->schoolYear.'/templateFile';
			$fullFolderPathPrefix = $intranet_root.$dbFolderPathPrefix;
			if (!file_exists($fullFolderPathPrefix)) {
				$successAry['createFolder1'] = $fs->folder_new($fullFolderPathPrefix);
			}
			$dbFolderPathPrefix .= '/'.$fileType;
			$fullFolderPathPrefix .= '/'.$fileType;
			if (!file_exists($fullFolderPathPrefix)) {
				$successAry['createFolder2'] = $fs->folder_new($fullFolderPathPrefix);
			}
			
			$ext = get_file_ext($fileName);
			$newFileName = $reportId.$ext;
			$successAry['copyFile'] = $fs->file_copy($fileLocation, $fullFolderPathPrefix.'/'.$newFileName);
			
			return in_array(false, $successAry)? '' : $dbFolderPathPrefix.'/'.$newFileName;
		}
		
		private function deleteFileFromServer() {
			global $PATH_WRT_ROOT, $intranet_root;
			
			$path = $this->getFilePath();
			
			$successAry = array();
			if ($path == '') {
				$successAry['noLink'] = true;
			}
			else {
				include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
				$fs = new libfilesystem();
				
				$orignialPath = $intranet_root.$path;
				$newPath = $intranet_root.$path.'.'.date('Ymd_His').'.'.$_SESSION['UserID'];
				$successAry['renameFile'] = $fs->file_rename($orignialPath, $newPath);
			}
			
			return in_array(false, $successAry)? false : true;
		}
		
		public function returnFileInfoAry($ReportIDAry='', $FileTypeAry='') {
			if ($ReportIDAry !== '') {
				$condsReportId = " And ReportID in ('".implode("','", (array)$ReportIDAry)."') ";
			}
			
			if ($FileTypeAry !== '') {
				$condsFileType = " And FileType in ('".implode("','", (array)$FileTypeAry)."') ";
			}
			
			$RC_REPORT_TEMPLATE_FILE = $this->getTableName();
			$sql = "Select * From $RC_REPORT_TEMPLATE_FILE Where 1 $condsReportId $condsFileType";
			return $this->lreportcard->returnResultSet($sql);
		}
		
		public function returnPreviewImage() {
			return $this->returnImageHtml('500px');
		}
		
		public function returnImageHtml($width='') {
			return $this->returnImage($width);
		}
		
		private function returnImage($width) {
			$link = $this->getFilePath();
			
			$image = '';
			if ($link=='') {
				$image = '&nbsp;';
			}
			else {
				$ts = strtotime(date('Y-m-d H:i:s'));
				$image = '<img src="'.$link.'?ts='.$ts.'" style="width:'.$width.';">';
			}
			return $image;
		}
		
		public function copyTemplateFile($fromReportId, $toReportId) {
			global $eRCTemplateSetting, $PATH_WRT_ROOT, $intranet_root;
			
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$fs = new libfilesystem();
			
			$successArr = array();
			
			$imageTypeAry = $eRCTemplateSetting['Settings']['TemplateSettings_ImageAry'];
			$numOfImageType = count((array)$imageTypeAry);
			for ($i=0; $i<$numOfImageType; $i++) {
				$_imageType = $imageTypeAry[$i];
				
				$_fromFileInfoAry = $this->returnFileInfoAry($fromReportId, $_imageType);
				$_fromFileId = $_fromFileInfoAry[0]['FileID'];
				
				$_fromFileObj = new libreportcard_file($_fromFileId);
				$_fromFilePath = $intranet_root.$_fromFileObj->getFilePath();
				$_fromFileName = get_file_basename($_fromFilePath);
				$_toFileName = str_replace($fromReportId, $toReportId, $_fromFileName);
				
				$_newFileObj = new libreportcard_file();
				$_newFileObj->setReportId($toReportId);
				$_newFileObj->setFileType($_imageType);
				$successArr['uploadImage'][$_imageType] = $_newFileObj->uploadFile($_fromFilePath, $_toFileName);
			}
			
			return !in_array(false, (array)$successArr);
		}
	}
}
?>