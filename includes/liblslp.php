<?php
// using : 

############# Change Log [Start] ################
#	Date:	2015-12-09	Henry
#			modified isInLicencePeriod() to change mktime() to time() [ip.2.5.7.1.1]
#
############# Change Log [End] ################
include_once($intranet_root."/includes/libfilesystem.php");
if (!class_exists('libuser')){
	include_once($intranet_root."/includes/libuser.php");
}
class lslp extends libdb
{		
	function lslp()
	{
		$this->libdb();
	}
	
	function getAdminUser()
	{
		global $intranet_root;

		$lf = new libfilesystem();
		$AdminUser = trim($lf->file_read($intranet_root."/file/lslp/admin_user.txt"));

		return $AdminUser;
	}
	
	function getTotalUsedLicense()
	{
		$sql = "select count(*) from LS_USER";
		$return_obj = $this->returnVector($sql);
		$used_license = $return_obj[0];	
		
		return $used_license;
	}
	
	function hasFullLicense()
	{
		global $lslp_license;
		
		if($lslp_license==9999)
			return true;
	}
	
	function hasUsedUpAllLicense()
	{
		global $lslp_license;
		
		if($lslp_license==9999)
		{
			return false;	
		}
		else
		{
			$used = $this->getTotalUsedLicense();
			if($used>=$lslp_license)
				return true;
		}
	}
	
	function getTotalLicenseLeft()
	{
		global $lslp_license;
		
		if($lslp_license==9999)
		{
			return 9999;	
		}
		else
		{
			$used = $this->getTotalUsedLicense();
			return $lslp_license - $used;
		}
	}
	
	
	function hasLSLPAccess()
	{
		global $UserID,$intranet_root,$intranet_db, $lu;
		
		if (isset($lu) && $lu->UserID==$UserID)
		{
			$lib_user = $lu;
		} else
		{
			include_once($intranet_root."/includes/libuser.php");
			$lib_user = new libuser($UserID);
			$lib_user->db = $intranet_db;
			$lib_user->UserID = $UserID;
		}
		
		if($lib_user->isTeacherStaff())
		{
			return true;
		}
		else
		{
			if(!$this->hasFullLicense())
			{
				global $UserID;
				
				$sql = " select count(*) from LS_USER where UserID = '".$UserID."'";
				$access_obj = $this->returnVector($sql);
				
				return $access_obj[0];
			}
			else
			return true;
		}
	}
	function isInLicencePeriod()
	{
		global $lslp_license_period;
		
		list($license_start,$license_end) = explode(",",$lslp_license_period);		
		list($license_start_y,$license_start_m,$license_start_d) = explode("-",$license_start);
		list($license_end_y,$license_end_m,$license_end_d) = explode("-",$license_end);
		
		$licence_start_time = mktime(0,0,0,$license_start_m,$license_start_d,$license_start_y);
		$licence_end_time = mktime(23,59,59,$license_end_m,$license_end_d,$license_end_y);
		$cur_time = time();
		
		if(($cur_time >= $licence_start_time) && ($cur_time <= $licence_end_time))
		{
			return true;
		
		}
		
	}
	
	function isAdminUser()
	{
		global $UserID;
		$admin_user = $this->getAdminUser();
		$admin_user_arr = explode(",",$admin_user);
		
		if(in_array($UserID,$admin_user_arr))
			return true;
	}
	
	function getLicenseType()
	{
		global $lslp_license,$i_LSLP;
		if(isset($lslp_license) && $lslp_license!='')
		{
			if($lslp_license==9999)
			{
				return $i_LSLP['school_license'];
			}
			else
			{
				return $i_LSLP['student_license'];	
			}
		}
	}
	function getLicensePeriod()
	{
		global $lslp_license_period,$i_To;
		
		list($license_start,$license_end) = explode(",",$lslp_license_period);		
		
		
		return $license_start." ".$i_To." ".$license_end;
	}
}

?>