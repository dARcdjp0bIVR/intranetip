<?php
// Editing by 

if (!defined("LIBALIPAY_DEFINED"))                     // Preprocessor directive
{
define("LIBALIPAY_DEFINED", true);

class libalipay
{
	var $alipay_config;
	
	function __construct($alipay_config)
	{
		$this->alipay_config = $alipay_config;
	}
	
    function libalipay($alipay_config) 
    {
    	$this->__construct($alipay_config);
    }
	
	
	/**
	 * sign  签名字符串
	 * @param $prestr 需要签名的字符串
	 * @param $key 私钥
	 * return 签名结果 sign generated
	 */
	function md5Sign($prestr, $key)
	{
		$prestr = $prestr . $key;
		return md5($prestr);
	}

	/**
	 * 验证签名 sign verify
	 * @param $prestr 需要签名的字符串pre-sign string
	 * @param $sign 签名结果
	 * @param $key 私钥
	 * return 签名结果sign generated
	 */
	function md5Verify($prestr, $sign, $key)
	{
		$prestr = $prestr . $key;
		$mysgin = md5($prestr);
	
		if($mysgin == $sign) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
	 * @param $para 需要拼接的数组 array needs to be connected
	 * return 拼接完成以后的字符串 String with connected parameters
	 * connect parameters with & like "parameter name=value"
	 */
	function createLinkstring($para)
	{
		$arg  = "";
		while (list ($key, $val) = each ($para)) {
			$arg.=$key."=".$val."&";
		}
		//去掉最后一个&字符
		//remove the last &
		$arg = substr($arg,0,count($arg)-2);
		//如果存在转义字符，那么去掉转义
		//remove escape character if there's any
		
		if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}
		
		return $arg;
	}
	
	
	/**
	 * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串，并对字符串做urlencode编码
	 * connect parameters to a string with & like "parameter name=value",get the string urlencoded
	 * @param $para 需要拼接的数组array needs to be connected
	 * return 拼接完成以后的字符串String with connected parameters
	 */
	function createLinkstringUrlencode($para)
	{
		$arg  = "";
		while (list ($key, $val) = each ($para)) {
			$arg.=$key."=".urlencode($val)."&";
		}
		//去掉最后一个&字符
		//remove the last &
		$arg = substr($arg,0,count($arg)-2);
		
		//如果存在转义字符，那么去掉转义
		//remove escape character if there's any
		if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}
		
		return $arg;
	}
	
	/**
	 * 除去数组中的空值和签名参数
	 * Remove the blank ,sign and sign_type
	 * @param $para 签名参数组A set of signature parameters
	 * return 去掉空值与签名参数后的新签名参数组The new signature paramaters with the blank ,sign and sign_type removed
	 */
	function paraFilter($para)
	{
		$para_filter = array();
		while (list ($key, $val) = each ($para)) {
			if($key == "sign" || $key == "sign_type" || $val == "")continue;
			else	$para_filter[$key] = $para[$key];
		}
		return $para_filter;
	}
	
	/**
	 * 对数组排序 rearrange
	 * @param $para 排序前的数组 before rearrange
	 * return 排序后的数组 rearranged
	 */
	function argSort($para)
	{
		ksort($para);
		reset($para);
		return $para;
	}
	
	
	
	/**
	 * 生成签名结果
	 * Generate the sign
	 * @param $para_sort 已排序要签名的数组Parameters to sign
	 * return 签名结果字符串sign generated
	 */
	function buildRequestMysign($para_sort)
	{
		//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
    	//Rearrange parameters in the data set alphabetically and connect rearranged parameters with & like "parametername=value"
		$prestr = $this->createLinkstring($para_sort);
		
		$mysign = "";
		switch (strtoupper(trim($this->alipay_config['sign_type']))) {
			case "MD5" :
				$mysign = $this->md5Sign($prestr, $this->alipay_config['key']);
				break;
			default :
				$mysign = "";
		}
		
		return $mysign;
	}

	/**
     * 生成要请求给支付宝的参数数组
	 * Generate a set of parameters need in the request of Alipay
     * @param $para_temp 请求前的参数数组Pre-sign string
     * @return 要请求的参数数组parameters need to be in the request
     */
	function buildRequestPara($para_temp)
	{
		//除去待签名参数数组中的空值和签名参数
		//Remove the blank ,sign and sign_type
		$para_filter = $this->paraFilter($para_temp);

		//对待签名参数数组排序
		//sort the presign string
		$para_sort = $this->argSort($para_filter);

		//生成签名结果
		//Generate the sign
		$mysign = $this->buildRequestMysign($para_sort);
		
		//签名结果与签名方式加入请求提交参数组中
		//Add the sign and sign_type into the sPara
		$para_sort['sign'] = $mysign;
		$para_sort['sign_type'] = strtoupper(trim($this->alipay_config['sign_type']));
		
		return $para_sort;
	}

	/**
     * 生成要请求给支付宝的参数数组
	 * Generate a set of parameters need in the request of Alipay
     * @param $para_temp 请求前的参数数组Pre-sign string
     * @return 要请求的参数数组字符串parameters need to be in the request
     */
	function buildRequestParaToString($para_temp)
	{
		//待请求参数数组
		//Pre-sign 
		$para = $this->buildRequestPara($para_temp);
		
		//把参数组中所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串，并对字符串做urlencode编码
		//connect rearranged parameters with & like "parametername=value",get the string urlencoded
		$request_data = $this->createLinkstringUrlencode($para);
		
		return $request_data;
	}
	
	
	/**
	 * 远程获取数据，POST模式
	 * remote data access,POST
	 * 注意：
	 * note:
	 * 1.使用Crul需要修改服务器中php.ini文件的设置，找到php_curl.dll去掉前面的";"就行了
	 * 2.文件夹中cacert.pem是SSL证书请保证其路径有效，目前默认路径是：getcwd().'\\cacert.pem'
	 * 1.To use Crul,you need to change the setting of php.ini,just remove the ";" of php_curl.dll
	 * 2.cacert.pem in the folder is SSL certificate,pls make sure it is accessible，the default path is：getcwd().'\\cacert.pem'
	 * @param $url 指定URL完整路径地址 the full path of URL
	 * @param $cacert_url 指定当前工作目录绝对路径 The absolute path to the current working directory
	 * @param $para 请求的数据 request data
	 * @param $input_charset 编码格式。默认值：空值 charset 
	 * return 远程输出的数据 remote output data
	 */
	function getHttpResponsePOST($url, $cacert_url, $para, $input_charset = '')
	{
	
		if (trim($input_charset) != '') {
			$url = $url."_input_charset=".$input_charset;
		}
		$curl = curl_init($url);
		//curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);//SSL证书认证SSL certificate authentication
		//curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);//严格认证Strict certification
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		//curl_setopt($curl, CURLOPT_CAINFO,$cacert_url);//证书地址Address of the certificate
		curl_setopt($curl, CURLOPT_HEADER, 0 ); // 过滤HTTP头 filter header
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);// 显示输出结果 show the output
		curl_setopt($curl, CURLOPT_POST,true); // post传输数据 transfer the data with post
		curl_setopt($curl, CURLOPT_POSTFIELDS,$para);// post传输数据transfer the data with post
		$responseText = curl_exec($curl);
		//var_dump( curl_error($curl) );//如果执行curl过程中出现异常，可打开此开关，以便查看异常内容uncomment this to check the error that happens when executing the curl
		curl_close($curl);
		
		return $responseText;
	}
	
	/**
	 * 远程获取数据，GET模式
	  * remote data access,GET
	 * 注意：
	 * note:
	 * 1.使用Crul需要修改服务器中php.ini文件的设置，找到php_curl.dll去掉前面的";"就行了
	 * 2.文件夹中cacert.pem是SSL证书请保证其路径有效，目前默认路径是：getcwd().'\\cacert.pem'
	 * 1.To use Crul,you need to change the setting of php.ini,just remove the ";" of php_curl.dll
	 * 2.cacert.pem in the folder is SSL certificate,pls make sure it is accessible，the default path is：getcwd().'\\cacert.pem'
	 * @param $url 指定URL完整路径地址the full path of URL
	 * @param $cacert_url 指定当前工作目录绝对路径The absolute path to the current working directory
	 * return 远程输出的数据remote output data
	 */
	function getHttpResponseGET($url,$cacert_url)
	{
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, 0 ); // 过滤HTTP头filter header
		curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);// 显示输出结果show the output
		//curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);//SSL证书认证SSL certificate authentication
		//curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);//严格认证Strict certification
		//curl_setopt($curl, CURLOPT_CAINFO,$cacert_url);//证书地址Address of the certificate
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		$responseText = curl_exec($curl);
		//var_dump( curl_error($curl) );//如果执行curl过程中出现异常，可打开此开关，以便查看异常内容uncomment this to check the error that happens when executing the curl
		curl_close($curl);
		
		return $responseText;
	}
	
	/**
	 * 实现多种字符编码方式
	 * to support variety of character encoding
	 * @param $input 需要编码的字符串 string needs to be encoded
	 * @param $_output_charset 输出的编码格式 
	 * @param $_input_charset 输入的编码格式
	 * return 编码后的字符串 the string encoded
	 */
	function charsetEncode($input,$_output_charset ,$_input_charset)
	{
		$output = "";
		if(!isset($_output_charset) )$_output_charset  = $_input_charset;
		if($_input_charset == $_output_charset || $input ==null ) {
			$output = $input;
		} elseif (function_exists("mb_convert_encoding")) {
			$output = mb_convert_encoding($input,$_output_charset,$_input_charset);
		} elseif(function_exists("iconv")) {
			$output = iconv($_input_charset,$_output_charset,$input);
		} else die("sorry, you have no libs support for charset change.");
		return $output;
	}
	
	/**
	 * 实现多种字符解码方式
	 * to support variety of character encoding
	 * @param $input 需要解码的字符串string needs to be encoded
	 * @param $_output_charset 输出的解码格式
	 * @param $_input_charset 输入的解码格式
	 * return 解码后的字符串the string encoded
	 */
	function charsetDecode($input,$_input_charset ,$_output_charset)
	{
		$output = "";
		if(!isset($_input_charset) )$_input_charset  = $_input_charset ;
		if($_input_charset == $_output_charset || $input ==null ) {
			$output = $input;
		} elseif (function_exists("mb_convert_encoding")) {
			$output = mb_convert_encoding($input,$_output_charset,$_input_charset);
		} elseif(function_exists("iconv")) {
			$output = iconv($_input_charset,$_output_charset,$input);
		} else die("sorry, you have no libs support for charset changes.");
		return $output;
	}
	 
	function buildRequestForm($para_temp, $form_id, $form_name, $method, $target)
	{
		//待请求参数数组pre-request params
		$para = $this->buildRequestPara($para_temp);
		
		$html = "<form id=\"$form_id\" name=\"$form_name\" action=\"".$this->alipay_config['gateway']."_input_charset=".trim(strtolower($this->alipay_config['input_charset']))."\" method=\"".$method."\" target=\"$target\">";
		while (list ($key, $val) = each ($para)) {
            $html.= "<input type='hidden' name='".$key."' value='".$val."'/>";
        }
        $html .= "</form>";
		return $html;
	}
	
	/**
     * 针对notify_url验证消息是否是支付宝发出的合法消息
	 * Verify whether it's a legal notification sent from Alipay
     * @return 验证结果The result of verification
     */
	function verifyNotify($data)
	{		
		if(empty($data)) {//判断POST来的数组是否为空check whether the info from POST is empty
			return false;
		}
		else {
			//验证MD5的结果
			//verify the MD5 sign
			$isSign = $this->getSignVeryfy($data, $data["sign"]);
			
			return $isSign;
			/*
			//获取支付宝远程服务器ATN结果（验证是否是支付宝发来的消息）
			//Get the remote server ATN result(verify whether it's a legal notification sent from Alipay)
			$responseTxt = 'false';
			if (! empty($data["notify_id"])) {$responseTxt = $this->getResponse($data["notify_id"]);}
			
			//写日志记录
			//write log
			//if ($isSign) {
			//	$isSignStr = 'true';
			//}
			//else {
			//	$isSignStr = 'false';
			//}
			//$log_text = "responseTxt=".$responseTxt."\n notify_url_log:isSign=".$isSignStr.",";
			//$log_text = $log_text.createLinkString($_POST);
			//logResult($log_text);
			
			//验证
			//verify
			//$responsetTxt的结果不是true，与服务器设置问题、合作身份者ID、notify_id一分钟失效有关
			//isSign的结果不是true，与安全校验码、请求时的参数格式（如：带自定义参数等）、编码格式有关
			//if responsetTxt is not true,the cause might be related to sever setting,merchant account and expiration time of notify_id(one minute).
            //if isSign is not true，the cause might be related to sign,charset and format of request str(eg:request with custom parameter etc.) 
			if (preg_match("/true$/i",$responseTxt) && $isSign) {
				return true;
			} else {
				return false;
			}
			*/
		}
	}
	
    /**
     * 针对return_url验证消息是否是支付宝发出的合法消息
	 * Verify whether it's a legal notification sent from Alipay
     * @return 验证结果result
     */
	function verifyReturn($data)
	{
		if(empty($data)) {//判断POST来的数组是否为空check the info from POST is blank
			return false;
		}
		else {
			//生成签名结果generate the sign
			$isSign = $this->getSignVeryfy($data, $data["sign"]);
			return $isSign;
		}
	}
	
    /**
     * 获取返回时的签名验证结果
	 * Generate sign from feedback
     * @param $para_temp 通知返回来的参数数组the params from the feedback notification
     * @param $sign 返回的签名结果the sign to be compared
     * @return 签名验证结果the result of verification
     */
	function getSignVeryfy($para_temp, $sign)
	{
		//除去待签名参数数组中的空值和签名参数
		//Filter parameters with null value ,sign and sign_type
		$para_filter = $this->paraFilter($para_temp);
		
		//对待签名参数数组排序
		//sort the to-be-signed 
		$para_sort = $this->argSort($para_filter);
		
		//把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
		//connect all the parameters with "&" like "parameter=value"
		$prestr = $this->createLinkstring($para_sort);
		
		$isSgin = false;
		switch (strtoupper(trim($this->alipay_config['sign_type']))) {
			case "MD5" :
				$isSgin = $this->md5Verify($prestr, $sign, $this->alipay_config['key']);
				break;
			default :
				$isSgin = false;
		}
		
		return $isSgin;
	}

    /**
     * 获取远程服务器ATN结果,验证返回URL
	* Get the remote server ATN result,return URL
     * @param $notify_id 通知校验IDThe ID for a particular notification. 
     * @return 服务器ATN结果Sever ATN result
     * 验证结果集：
	* Verification result:
     * invalid命令参数不对 出现这个错误，请检测返回处理中partner和key是否为空 
	* invalid Pls check whether the partner and key are null from notification 
     * true 返回正确信息
	* true return the right info
     * false 请检查防火墙或者是服务器阻止端口问题以及验证时间是否超过一分钟
	* false pls check the firewall or the server block certain port,also pls note the expiration time is 1 minute
     */
	function getResponse($notify_id)
	{
		$transport = strtolower(trim($this->alipay_config['transport']));
		$partner = trim($this->alipay_config['partner']);
		$verify_url = '';
		if($transport == 'https') {
			$verify_url = $this->alipay_config['https_verify_url'];
		}
		else {
			$verify_url = $this->alipay_config['http_verify_url'];
		}
		$verify_url = $verify_url."partner=" . $partner . "&notify_id=" . $notify_id;
		$responseTxt = $this->getHttpResponseGET($verify_url, $this->alipay_config['cacert']);
		
		return $responseTxt;
	}
	
}

} // End of directive
?>