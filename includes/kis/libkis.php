<?
// modifying by : 
/*
 * 2020-03-05 (Bill):  modified getPendingPaymentsDisplayLayer(), to exclude payment items of payment notice (same logic of parent app)     [2020-0227-0929-45235]
 * 2018-08-17 (Henry): bug fix for cannot show user and children english name who do not have chinese name
 * 2017-08-10 (Simon): update the getStudentDetail sql statement.
 * 2016-04-25 (Carlos): modified getPendingPaymentsDisplayLayer() - filter out payment items that have not yet started.
 * 2015-08-26 (Carlos): added getPendingPaymentsDisplayLayer() - for parent user to get the pending payment items for his/her children. Triggered at portal page for parent user.
 * 						a new session var $_SESSION['KIS_PARENT_PAYMENT_ALERT'] is introduced to mark the action has been called.
 */
include_once("$intranet_root/includes/libuser.php");
include_once("$intranet_root/includes/libclass.php");
class kis extends libdb{
        
    private $user_id, $user_type, $user_name, $user_children = array();
    public $app, $user;
    public $libuser;
    public static $user_types = array('teacher'=>'T','parent'=>'P','student'=>'S');
    public static $user_record_types = array(1=>'T','S','P','A');
    public static $personal_photo_url = '/file/photo/personal/';
    public static $personal_photo_width = 100;
    public static $personal_photo_height = 130;

    public static function callAppService($app, $service, $params){
	
	global $kis_config, $intranet_root, $kis_lang, $PATH_WRT_ROOT,$setting_path_ip_rel, $Lang;
	    
	if (in_array($app, $kis_config['apps'])){
	
	    include_once("$intranet_root/includes/kis/apps/libkis_$app.php");
	    return call_user_func(array("kis_$app", "service_$service"), $params);
	}
	    
    }
    public function __construct($user_id){

	global $intranet_db;

	$this->db = $intranet_db;
	$this->user_id = $user_id;
	$this->user = $this->getUserDetail();
	
	if (!$_SESSION['kis']['user']){
	    	
	    $_SESSION['kis']['user']['type'] = self::$user_record_types[$this->user['type']];
	    $_SESSION['kis']['user']['name']['en'] = $this->user['user_name_en'];
	    $_SESSION['kis']['user']['name']['b5'] = $this->user['user_name_b5']?$this->user['user_name_b5']:$this->user['user_name_en'];
	    
	}
	
	$this->user_type = $_SESSION['kis']['user']['type'];	
	$this->user_name = $_SESSION['kis']['user']['name'];
	
	switch ($this->user_type){
	    
	    case kis::$user_types['parent']:
		$this->student_id = $this->loadUserChildren();
	    break;
	    case kis::$user_types['teacher']:
		$this->student_id = $_REQUEST['student_id'];
	    break;
	    case kis::$user_types['student']:
		$this->student_id = $this->user_id;
	    break;
		
	}

    }
    private function loadUserChildren(){
	
	global $intranet_session_language;

	if (!isset($_SESSION['kis']['user']['children'])){
	    
	    $sql = "SELECT
			p.StudentID user_id,
			u.EnglishName as user_name_en,
			u.ChineseName as user_name_b5
		    FROM INTRANET_PARENTRELATION p
		    INNER JOIN INTRANET_USER u ON p.StudentID = u.UserID
		    WHERE
			u.ClassName IS NOT NULL AND
			u.ClassName <> '' AND
			p.ParentID = ".$this->user_id;
                        
	    $_SESSION['kis']['user']['children'] = $this->returnArray($sql);echo mysql_error();
	    
	}
	
	foreach ($_SESSION['kis']['user']['children'] as $child){
	    
	    $this->user_children[$child['user_id']] = $child['user_name_'.$intranet_session_language]?$child['user_name_'.$intranet_session_language]:$child['user_name_en'];
	}
	
	if (!$_SESSION['kis']['user']['current_child']){
	    
	    $_SESSION['kis']['user']['current_child'] = $_SESSION['kis']['user']['children'][0]['user_id'];
	}
	
	return $_SESSION['kis']['user']['current_child'];
    }
    public function getUserPhoto($UserLogin){
	    global $intranet_root;
	    $photo_filepath = "/file/user_photo/".$UserLogin.".jpg";
	    if(!file_exists($intranet_root.$photo_filepath)){
		    $photo_filepath = "/images/kis/blank.jpg";
	    }
	    return $photo_filepath;
    }
    public function getStudentDetail(){
		$student = $this->getUserDetail($this->student_id);
		$student['class_teachers'] = $this->getStudentClassTeacher();
		$student['photo'] = $this->getUserPhoto($student['user_login']);
		return $student;
    }
    public function getUserDetail($user_id=''){
		
	if (!$user_id) $user_id = $this->user_id;

	$sql = "SELECT
		    u.UserLogin user_login,
		    u.UserID user_id,
		    u.EnglishName user_name_en,
		    u.ChineseName user_name_b5,
			".getNameFieldByLang("u.")." as user_name,
		    u.NickName nick_name,
			u.PersonalPhotoLink personal_photo,
		    u.Gender gender,
		    u.Address address,
		    u.HomeTelNo home_tel,
		    u.OfficeTelNo office_tel,
		    u.MobileTelNo mobile_tel,
		    u.URL url,
		    u.FaxNo fax,
		    u.UserEmail email,
		    u.Country country,
		    u.RecordType type,
		    s.Nationality nationality,
		    s.PlaceOfBirth birth_place,
		    if (s.AdmissionDate=0,'',DATE_FORMAT(s.AdmissionDate, '%Y-%m-%d')) admission_date,
		    if (u.DateOfBirth=0,'',DATE_FORMAT(u.DateOfBirth, '%Y-%m-%d')) birth_date,
		    u.ClassName as class_name,
		    u.ClassNumber as class_number,
		    u.Remark as remark
		FROM INTRANET_USER u
		LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS s ON u.UserID = s.UserID
		WHERE u.UserID = $user_id";

	return current($this->returnArray($sql));
	    
    }
    public function getStudentClassTeacher(){
	
	$sql = "SELECT 
		    u.UserID user_id,
		    u.TitleEnglish title_en,
		    u.TitleChinese title_b5,
		    u.EnglishName user_name_en,
		    u.ChineseName user_name_b5
		FROM YEAR_CLASS_TEACHER t
		INNER JOIN YEAR_CLASS c ON t.YearClassID = c.YearClassID
		INNER JOIN YEAR_CLASS_USER cu ON cu.YearClassID = c.YearClassID
		INNER JOIN INTRANET_USER as u ON t.UserID = u.UserID 
		WHERE
		    cu.UserID = ".$this->student_id." AND
		    c.AcademicYearID = ".$_SESSION['CurrentSchoolYearID'];
		
	return $this->returnArray($sql);
		    
    }
    public function changeCurrentStudent($student_id){
	
	if ($this->user_children[$student_id] || $this->user_type == kis::$user_types['teacher']){
	    $this->student_id =  $_SESSION['kis']['user']['current_child'] = $student_id;
	}
	return $this;	
    }
    
    public function getVisibleApps(){
	
	global $kis_config, $intranet_root, $kis_lang, $intranet_session_language, $PATH_WRT_ROOT,$setting_path_ip_rel;

	$available_apps = array();
    
	if (!$_SESSION['kis']['available_apps']){
	    
	    foreach ($kis_config['apps'] as $app){
	    
		include_once("$intranet_root/includes/kis/apps/libkis_$app.php");
		$class_name = "kis_$app";
		$_SESSION['kis']['available_apps'][$app] = call_user_func(array($class_name, 'getAvailability'), $this->user_id, $this->user_type, $this->student_id);
		$_SESSION['kis']['admin_apps'][$app] = call_user_func(array($class_name, 'getAdminStatus'), $this->user_id, $this->user_type, $this->student_id);

	    }
	    
	}
	
	foreach ($_SESSION['kis']['available_apps'] as $app=>$available_app){
	    
	    if ($available_app){
		list($name, $image, $bg_theme, $href) = $available_app;
		$available_apps[$app] = array('title'=>$kis_lang['app_'.$name],'image'=>$image,'background'=>$bg_theme,'href'=>$href);
		
		if ($_SESSION['kis']['admin_apps'][$app]){
		    list($admin) = $_SESSION['kis']['admin_apps'][$app];
		    $available_apps[$app]['admin'] = $admin;
		}
	    }
	    
	}
	
	return $available_apps;
	
    }
    public function getNotificationCounts(){
	
	global $kis_config, $intranet_root, $intranet_session_language, $PATH_WRT_ROOT,$setting_path_ip_rel;
	$notifications = array();
	
	foreach ($kis_config['apps'] as $app){
	    
	    include_once("$intranet_root/includes/kis/apps/libkis_$app.php");
	    $class_name = "kis_$app";
	    
	    $notifications[$app] = call_user_func(array($class_name, 'getNotificationCount'), $this->user_id, $this->user_type, $this->student_id);	    
	    
	}
	return $notifications;
	
    }
    public function loadApp($app, $params=array()){
	global $kis_config, $intranet_root, $intranet_db, $kis_lang, $intranet_session_language, $PATH_WRT_ROOT,$setting_path_ip_rel;
	if (in_array($app, $kis_config['apps'])){
	    
	    include_once("$intranet_root/includes/kis/apps/libkis_{$app}.php");	
	    include_once("$intranet_root/lang/kis/apps/lang_{$app}_{$intranet_session_language}.php");
	    $class_name = "kis_$app";
	    $app = new $class_name($this->user_id, $this->user_type, $this->student_id, $params);
	   
	}
	return $app;
	
    }
    
    public function getUserSchool(){
	
	global $intranet_root;
	
	if (!$_SESSION['kis']['school']){
	    
	    $imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
	
	    $_SESSION['kis']['school']['logo'] = ($imgfile == "" ? "/images/kis/logo_kis.png" : "/file/$imgfile");
	    $_SESSION['kis']['school']['name'] = $_SESSION['SSV_PRIVILEGE']['school']['name'];
	}
	
	return $_SESSION['kis']['school'];

    }
    public function getUserInfo(){
	
	global $kis_lang, $kis_config, $intranet_session_language;
	
	$user_info['id'] = $this->user_id;
	$user_info['name'] = $this->user_name[$intranet_session_language];
	$user_info['type'] = $this->user_type;
	$user_info['type_title'] = $kis_lang['usertype_'.$this->user_type];
	$user_info['current_child'] = $this->student_id;
	$user_info['children'] = $this->user_children;

	
	return $user_info;
	
    }
    
    // for parent user only - get the pending payment items for his/her children
	public function getPendingPaymentsDisplayLayer()
    {
    	global $intranet_root, $Lang, $kis_lang, $plugin, $kis_config, $intranet_session_language, $PATH_WRT_ROOT, $setting_path_ip_rel;

    	if($_SESSION['KIS_PARENT_PAYMENT_ALERT'] || !$plugin['payment'] || $this->user_type != kis::$user_types['parent']) {
    	    return '';
        }
    	
    	include_once($intranet_root.'/lang/kis/apps/lang_epayment_'.$intranet_session_language.'.php');
    	include_once($intranet_root.'/includes/libgeneralsettings.php');
    	$GeneralSetting = new libgeneralsettings();
		$Settings = $GeneralSetting->Get_General_Setting('ePayment');

        // check whether the ePayment related setting is on
    	if(!$Settings['OutstandingPaymentAlert']) {
    	    return '';
        }
    	
    	$today = date('Y-m-d');
		$name_field = getNameFieldWithClassNumberByLang("u.");
		$sql = "SELECT u.UserID, $name_field as StudentName FROM INTRANET_USER as u INNER JOIN INTRANET_PARENTRELATION as r ON r.StudentID=u.UserID WHERE r.ParentID = '".$this->user_id."'";
		$students = $this->returnResultSet($sql);
		$studentIdAry = Get_Array_By_Key($students, 'UserID');
    	$student_count = count($studentIdAry);

    	$total_record = 0;
    	$rows = array();
    	for($i=0; $i<$student_count; $i++)
    	{
    		$sql = "SELECT 
					    a.PaymentID as id,
					    b.Name as item_name,
					    c.Name as category_name,
					    a.Amount as amount,
					    a.SubsidyAmount as subsidy,
					    b.EndDate as deadline,
					    a.RecordStatus as status,
					    a.PaidTime as time 
					FROM
					    PAYMENT_PAYMENT_ITEMSTUDENT as a
					    INNER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
					    LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
					WHERE
					    a.StudentID = '".$studentIdAry[$i]."' AND (a.RecordStatus = 0 OR a.RecordStatus IS NULL) AND b.StartDate <= '$today' AND b.NoticeID IS NULL
			        ORDER BY
                        deadline DESC, item_name ASC";
    		$ary = $this->returnResultSet($sql);
    		$total_record += count($ary);
    		$rows[] = $ary;
    	}
    	if($total_record == 0) {
    	    return '';
        }
    	
    	$x = '<div style="padding:5px;">';
    	$x.= '<p>'.$kis_lang['ThereArePendingPaymentItems'].'</p>';
    	for($i=0; $i<count($rows); $i++)
    	{
    		$records = $rows[$i];
    		$record_count = count($records);
    		$student_name = $students[$i]['StudentName'];

    		//$x .= '<p>'.$student_name.'</p>';
    		$x .= '<div class="table_board">
						<table class="common_table_list edit_table_list">
						<caption style="padding:8px">'.$student_name.'</caption>
						<colgroup><col nowrap="nowrap">
						</colgroup>
						<thead>
							<tr>
							  <th width="20">#</th>
							  <th>'.$kis_lang['item'].'</th>
							  <th>'.$kis_lang['category'].'</th>
							  <th>'.$kis_lang['amount'].'</th>
							  <th>'.$kis_lang['deadline'].'</th>
							  <th>'.$kis_lang['status'].'</th>
							</tr>
						</thead>
						<tbody>';
					$total = 0;
					for($j=0; $j<$record_count; $j++)
					{
						$total += $records[$j]['amount'];
						$x .= '<tr>';
							$x .= '<td>'.($j+1).'<input type="hidden" name="recordID[]" value="'.$records[$j]['id'].'" /></td>';
							$x .= '<td>'.kis_ui::displayTableField($records[$j]['item_name']).'</td>';
							$x .= '<td>'.kis_ui::displayTableField($records[$j]['category_name']).'</td>';
							$x .= '<td>'.kis_ui::displayTableField('$'.$records[$j]['amount']).'</td>';
							$x .= '<td>'.kis_ui::displayTableField($records[$j]['deadline']).'</td>';
							$x .= '<td>'.kis_ui::displayTableField($records[$j]['status']?$kis_lang['paid']:$kis_lang['unpaid']).'</td>';
						$x .='</tr>';
					}
					$x .= '<tr><td colspan="3" style="text-align:right;border-right:none;">'.$kis_lang['totalamount'].':</td><td colspan="3" style="text-align:left;border-left:none;">'.sprintf('$%.2f',$total).'</td></tr>';
				$x .= '</tbody>
					</table>
				  	<p class="spacer"></p>
			    </div>
			    <p class="spacer"></p>';
    	}
    	
    	$x .= '<div class="edit_bottom">         
			       <input name="cancelBtn" type="button" class="formsubbutton" onclick="parent.$.fancybox.close();" value="'.$kis_lang['close'].'" />
			  	</div>
			</div>';
    	
    	$_SESSION['KIS_PARENT_PAYMENT_ALERT'] = 1;
    	
    	return $x;
    }
}
?>