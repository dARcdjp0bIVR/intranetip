<?php
# modifying : 

### Change Log [Start] ###
#
#	Date:	2015-05-07 (Ivan)
#			- modified AppGetPhoto() to cache the result of album access right to improve performance
#
#	Date:	2015-03-18 (Henry) 
#			- File Created
#
#### Change Log [End] ####

include_once ($intranet_root . "/includes/libaccessright.php");

class kis_album_ws extends libaccessright {
	
	public static $photo_urls = array('photo'=>"/file/album/photos/",'thumbnail'=>"/file/album/thumbnails/",'original'=>"/file/album/originals/");
    public static $file_url = '/file/digital_channels/';
    public static $photo_max_size = "1000";
    public static $thumbnail_max_size = "184";
    public static $encrypt_key = "7bGa69zcn3S"; //must not be changed after client using Digital channels
    public static $videoFormat = array('3GP','WMV','MP4');
    
	function kis_album_ws($user_id=0)
	{
		parent::libaccessright();
	}
     
    /////////////////-----------------function for elcass app [start]--------------------//////////////
    public static function getPhotoFileName($type, $album, $photo){
	
	return self::getAlbumFileName($type, $album).'/'.base64_encode($photo['id'].'_'.$photo['input_date'].'_'.$photo['input_by']).'.jpg';
	
    }
    public static function getAlbumFileName($type, $album){
	
	return self::$photo_urls[$type].base64_encode($album['id'].'_'.$album['input_date'].'_'.$album['input_by']);
	
    }
    
    function Get_Album_Photo($AlbumPhotoID = '', $AlbumID = ''){
		if($AlbumID != ''){
			$cond .= ' AND  p.AlbumID ="'.$AlbumID.'"';
		}
		if($AlbumPhotoID != ''){
			if(is_array($AlbumPhotoID)){
				$cond .= ' AND  p.AlbumPhotoID IN ("'.implode('","',$AlbumPhotoID).'")';
			}
			else{
				$cond .= ' AND  p.AlbumPhotoID = "'.$AlbumPhotoID.'"';
			}
		}
	    $sql = "SELECT
		    p.AlbumID as album_id,
		    p.AlbumPhotoID id,
		    p.Title title,
		    p.Description description,
		    p.Size as size,
		    UNIX_TIMESTAMP(p.DateTaken) date_taken,
		    UNIX_TIMESTAMP(p.DateInput) date_uploaded,
		    UNIX_TIMESTAMP(p.DateInput) input_date,
		    p.InputBy input_by
		FROM INTRANET_ALBUM_PHOTO p
		WHERE 
					1 ".$cond." 
		ORDER BY $sort IF(Sequence IS NULL, -1, Sequence), DateInput";
	    return $this->returnResultSet($sql);
	}
     
	public function AppIsAlbumReadable($album_id, $parUserID){
		
		$sql = "Select UserID From ROLE_RIGHT r JOIN ROLE_MEMBER m ON r.RoleID = m.RoleID where r.FunctionName = 'eAdmin-PhotoAlbum'";
		$result = $this->returnVector($sql);
		
		if(!is_array($parUserID)){
			$parUserID = array($parUserID);
		}
		
		foreach($parUserID as $aParUserID){
			if(in_array($aParUserID,$result))
				return true;
		}
		
    	$sql = "SELECT InputBy,
		IF (SharedSince <= CURDATE() OR SharedSince IS NULL OR SharedSince = 0, 1, 0),
		IF (SharedUntil >= CURDATE() OR SharedUntil IS NULL OR SharedUntil = 0, 1, 0) 
		FROM INTRANET_ALBUM
		WHERE AlbumID = $album_id";
	
		$settings = current($this->returnArray($sql));echo mysql_error();
		
		foreach($parUserID as $aParUserID){	
			if ($settings[0]==$aParUserID){ return true; }
		}
		
		if (!$settings[1] || !$settings[2]){ return false; }
		
		$sql1 = "SELECT COUNT(*)
			FROM INTRANET_ALBUM_USER a
			WHERE a.AlbumID = $album_id AND a.RecordType = 'all'";
		
		$sql2 = "SELECT COUNT(*)
			FROM INTRANET_ALBUM_USER a
			INNER JOIN INTRANET_USERGROUP g ON a.RecordID = g.GroupID AND a.RecordType = 'group'
			WHERE a.AlbumID = $album_id AND (g.UserID IN ('".implode("','",$parUserID)."'))";
		
		$sql3 = "SELECT COUNT(*)
			FROM INTRANET_ALBUM_USER a
			WHERE a.AlbumID = $album_id AND a.RecordID IN ('".implode("','",$parUserID)."') AND a.RecordType = 'user'";
	
		return current($this->returnVector($sql1)) > 0 || current($this->returnVector($sql2)) > 0 || current($this->returnVector($sql3)) > 0;
    	
    }
     
	function AppGetAlbum($parUserID){ //return (AlbumID, CategoryID, Title, Description, DateTaken, NumOfPhoto, CoverPhotoPath)    
    	$sql = 'SELECT 
					AlbumID,
					"0" as CategoryID,
					Title, 
					Description, 
					DATE(DateTaken) as DateTaken,
					DateInput,
					InputBy,
					CoverPhotoID
				FROM 
					INTRANET_ALBUM
				WHERE 1
				ORDER BY DateInput DESC
				';

        $albumInfoAry = $this->returnResultSet($sql);
		$tempAlbumInfoAry = array();
		foreach($albumInfoAry as $aAlbumInfo){
			if(!$this->AppIsAlbumReadable($aAlbumInfo['AlbumID'],$parUserID)){
        		continue;
        	}
        	
        	$aAlbumInfo['Title'] = intranet_undo_htmlspecialchars($aAlbumInfo['Title']);
        	$ablumPhotoInfoAry = $this->Get_Album_Photo('',$aAlbumInfo['AlbumID']);
			
			$album = array(
			    "id" => $aAlbumInfo['AlbumID'],
			    "input_date" => strtotime($aAlbumInfo['DateInput']),
			    "input_by" => $aAlbumInfo['InputBy']
			);
			
			//get cover photo info
			$coverPhotoInfo = current($ablumPhotoInfoAry);
			
			$numOfPhoto=0;
			foreach($ablumPhotoInfoAry as $aPhoto){
				$o_ext = pathinfo($aPhoto['title'], PATHINFO_EXTENSION);
				//if(in_array(strtoupper($o_ext),libdigitalchannels::$videoFormat)){
					$numOfPhoto++;
				//}
				if($aPhoto['id'] == $aAlbumInfo['CoverPhotoID']){
					$coverPhotoInfo = $aPhoto;
				}
			}
			
			$coverPhotoInfo = ($coverPhotoInfo?$coverPhotoInfo:($ablumPhotoInfoAry?$ablumPhotoInfoAry[0]:''));
			
			$aAlbumInfo['NumOfPhoto'] = $numOfPhoto;
			$aAlbumInfo['CoverPhotoPath'] = $coverPhotoInfo?self::getPhotoFileName('thumbnail', $album, $coverPhotoInfo):'';
			
			//if($numOfPhoto > 0)
        		$tempAlbumInfoAry[] = $aAlbumInfo;
		}
		$albumInfoAry = $tempAlbumInfoAry;
		
		return $albumInfoAry;
	}
	
	function AppGetPhoto($parUserID, $AlbumID = '', $PhotoID = ''){ //return (PhotoID, AlbumID, Title, Description, Size, DateInput, FilePath, OriginalFilePath, Type)
		
		if($AlbumID){
			$cond.=" AND p.AlbumID = '".$AlbumID."'";
		}
		
		if($PhotoID){
			$cond.=" AND p.AlbumPhotoID = '".$PhotoID."'";
		}
		
	    $sql = "SELECT
		    p.AlbumPhotoID PhotoID,
			p.AlbumID as AlbumID,
		    p.Title Title,
		    p.Description Description,
		    p.Size as Size,
		    UNIX_TIMESTAMP(p.DateInput) DateInput,
			UNIX_TIMESTAMP(p.DateInput) input_date,
		    p.InputBy input_by,
			p.AlbumPhotoID id,
			p.Title title,
			UNIX_TIMESTAMP(a.DateInput) a_input_date,
			a.InputBy a_input_by
		FROM 
			INTRANET_ALBUM_PHOTO p
		JOIN
			INTRANET_ALBUM a
		ON
			a.AlbumID = p.AlbumID
		WHERE 
			1 $cond
		ORDER BY 
			$sort IF(Sequence IS NULL, -1, Sequence), p.DateInput";
	    $allPhoto = $this->returnResultSet($sql);

	    $tempArr = array();
	    $albumAccessAssoAry = array();
	    foreach($allPhoto as $aMostHit){
	    	
			$photo_ext = pathinfo($aMostHit['Title'], PATHINFO_EXTENSION);
			$isVideo = (in_array(strtoupper($photo_ext),self::$videoFormat))? true : false;
			
			# add path to the photo array
			$album = array(
			    "id" => $aMostHit['AlbumID'],
			    "input_date" => $aMostHit['a_input_date'],
			    "input_by" => $aMostHit['a_input_by']
			);
			
			# get the file path
			$filePath = self::getPhotoFileName('thumbnail', $album, $aMostHit);
			$originalFilePath = self::getPhotoFileName('photo', $album, $aMostHit);
			$aMostHit['FilePath'] = $filePath;
			$aMostHit['OriginalFilePath'] = $originalFilePath;
			$aMostHit['Type'] = ($isVideo?'Video':'Photo');
			
//			unset($aMostHit['6']);
//			unset($aMostHit['7']);
//			unset($aMostHit['8']);
//			unset($aMostHit['9']);
//			unset($aMostHit['10']);
//			unset($aMostHit['11']);
			unset($aMostHit['input_date']);
			unset($aMostHit['input_by']);
			unset($aMostHit['id']);
			unset($aMostHit['title']);
			unset($aMostHit['a_input_date']);
			unset($aMostHit['a_input_by']);
			
			if (!isset($albumAccessAssoAry[$aMostHit['AlbumID']])) {
				$albumAccessAssoAry[$aMostHit['AlbumID']] = $this->AppIsAlbumReadable($aMostHit['AlbumID'],$parUserID);
			}
			if($albumAccessAssoAry[$aMostHit['AlbumID']]){
				$tempArr[] = $aMostHit;
			}
	    }
	    
	    return $tempArr;
	}
	
	/////////////////-----------------function for elcass app [end]--------------------////////////// 
}

?>