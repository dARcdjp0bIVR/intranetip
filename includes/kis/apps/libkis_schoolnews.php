<?
// Editing by 
/*
 * 2016-05-13 (Bill): Modified getUserRelatedSchoolNews(), support Setting - Allow users to view past news	[2016-0513-1440-15206]
 */
class kis_schoolnews extends libdb implements kis_apps {
        
    private $user_id;
    
    public function __construct($user_id, $user_type, $student_id, $params){
		global $intranet_db;
	
		$this->db = $intranet_db;
		$this->user_id = $user_id;
    }
    
    public static function getAvailability($user_id, $user_type, $student_id){
    	return array('schoolnews', 'btn_schoolnews', '', '');
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id){
    	if ($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"]){
		    return array('/home/eAdmin/GeneralMgmt/schoolnews/');
		}
		return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id){
    	$libkis_schoolnews = new self($user_id, $user_type, $student_id, array());
    	
    	list($total, $newsAry) = $libkis_schoolnews->getUserRelatedSchoolNews($user_id);
    	$numOfNews = count($newsAry);
    	
    	$numOfUnread = 0;
    	for ($i=0; $i<$numOfNews; $i++) {
    		$_isRead = $newsAry[$i]['IsRead'];
    		
    		if ($_isRead) {
    			$numOfUnread++;
    		}
    	}
    	
		return $numOfUnread;
    }
    
    
    function getUserRelatedSchoolNews($user_id, $sortby='', $order='', $amount='', $page='', $keywords='', $newsType=''){
		global $kis_lang, $PATH_WRT_ROOT, $image_path, $LAYOUT_SKIN;
		
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		$userObj = new libuser($user_id);
		
		include_once($PATH_WRT_ROOT.'includes/libschoolnews.php');
		$lschoolnews = new libschoolnews();
		
		$sortby = $sortby==''? 'Title' : $sortby;
		$sort = $sortby? "$sortby $order" : $sortby;
		$limit = $page? " LIMIT ".(($page-1)*$amount).", $amount" : "";
		$newsType = $newsType==''? 'all' : $newsType;
		
		// [2016-0513-1440-15206] add checking for Setting - Allow users to view past news
		$endDateCond = $lschoolnews->allowUserToViewPastNews? " 1 " : " DATE(now()) <= DATE(a.EndDate) ";
		
		// get all group annonucements
		$sql = "SELECT AnnouncementID FROM INTRANET_GROUPANNOUNCEMENT Group By AnnouncementID";
		$allGroupAnnouncementIdAry = $this->returnVector($sql);
		
		// get user's school announcements
		$schoolAnnouncementIdAry = array();
		if ($newsType == 'all' || $newsType == 'all_public') {
			$sql = "SELECT 
						a.AnnouncementID
	            	FROM 
	            		INTRANET_ANNOUNCEMENT as a
	                 WHERE 
						DATE(a.AnnouncementDate) <= DATE(now()) AND ".$endDateCond."
	                 	AND a.AnnouncementID NOT IN ('".implode("','", (array)$allGroupAnnouncementIdAry)."') 
						AND a.RecordStatus = '1' 
	                 ORDER BY 
	                 	a.AnnouncementDate
					";
			$schoolAnnouncementIdAry = $this->returnVector($sql);
		}
		
		
		// get user's group announcements
		$groupIdAry = array();
		if ($newsType == 'all' || $newsType == 'all_group') {
			$groupIdAry = Get_Array_By_Key($userObj->returnGroups(), 'GroupID');
		}
		else if (is_numeric($newsType)) {
			$groupIdAry = array($newsType);
		}
		
		$sql = "SELECT 
					a.AnnouncementID
				FROM
					INTRANET_ANNOUNCEMENT AS a
		 			INNER JOIN INTRANET_GROUPANNOUNCEMENT AS b ON (b.AnnouncementID = a.AnnouncementID)
				WHERE 
					b.GroupID IN ('".implode("','", (array)$groupIdAry)."')
					AND DATE(a.AnnouncementDate) <= DATE(now()) AND ".$endDateCond."
					AND a.RecordStatus = '1' 
				GROUP BY
					a.AnnouncementID
				";
		$groupAnnouncementIdAry = $this->returnVector($sql);
		
		// get announcements info
		$nameField = Get_Lang_Selection('iu.ChineseName', 'iu.EnglishName');
		$groupNameField = Get_Lang_Selection('ig.TitleChinese', 'ig.Title');
		$isNewIcon = '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/alert_new.gif" align="absmiddle" border="0" />&nbsp;';
		
		if ($keywords !== '') {
			$conds_keywords = " AND
									(
										ia.Title like '%".$this->Get_Safe_Sql_Like_Query($keywords)."%'
										OR IF (ig.GroupID is not null, CONCAT($nameField, ' (', $groupNameField, ')'), $nameField) like '%".$this->Get_Safe_Sql_Like_Query($keywords)."%'
									)
								"; 
		}
		$userRelatedAnnouncementIdAry = array_values(array_merge($schoolAnnouncementIdAry, $groupAnnouncementIdAry));
		$sql = "SELECT
					ia.AnnouncementID,
					DATE(ia.AnnouncementDate) as AnnouncementDate,
					CONCAT(
							IF ((LOCATE(';$user_id;',ia.ReadFlag)=0 || ia.ReadFlag Is Null), '".$isNewIcon."', ''),
							'<a href=\"/home/view_announcement.php?AnnouncementID=', ia.AnnouncementID,'\" target=\"_blank\">', ia.Title, '</a>'
					) as NewsLink,
					IF (ig.GroupID is not null, CONCAT($nameField, ' (".$kis_lang['group2'].": ', $groupNameField, ')'), $nameField) as PostedBy,
					ia.Title,
					IF ((LOCATE(';$user_id;',ia.ReadFlag)=0 || ia.ReadFlag Is Null), 1, 0) as IsRead
				FROM
					INTRANET_ANNOUNCEMENT as ia
					LEFT OUTER JOIN INTRANET_USER as iu ON (ia.UserID = iu.UserID)
					LEFT OUTER JOIN INTRANET_GROUPANNOUNCEMENT AS iga ON (ia.AnnouncementID = iga.AnnouncementID)
					LEFT OUTER JOIN INTRANET_GROUP as ig ON (iga.GroupID = ig.GroupID)
				WHERE
					ia.AnnouncementID In ('".implode("','", (array)$userRelatedAnnouncementIdAry)."')
					$conds_keywords
				Order By
					$sort
				";
		$total = count($this->returnResultSet($sql)); 
		
		$sql .= $limit;
		$resultAry = $this->returnResultSet($sql);
		
		return array($total, $resultAry);
	}
	
	function getSchoolNewsTypeSelection($parId, $parName, $parSelected) {
		global $kis_lang, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		$userObj = new libuser($this->user_id);
		
		$optionAry = array();
		
		$optionAry['all'] = $kis_lang['all_news'];
		$optionAry['all_public'] = '&nbsp;&nbsp;&nbsp;'.$kis_lang['all_public_news'];
		$optionAry['all_group'] = '&nbsp;&nbsp;&nbsp;'.$kis_lang['all_group_news'];
		
		// get user's group announcements
		$groupAry = $userObj->returnGroups();
		$numOfGroup = count($groupAry);
		for ($i=0; $i<$numOfGroup; $i++) {
			$_groupId = $groupAry[$i]['GroupID'];
			$_titleEn = $groupAry[$i]['Title'];
			$_titleCh = $groupAry[$i]['TitleChinese'];
			$_title = Get_Lang_Selection($_titleCh, $_titleEn); 
			
			$optionAry[$_groupId] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$_title;
		}
		
		$selectionTags = ' id="'.$parId.'" name="'.$parName.'" ';
		return getSelectByAssoArray($optionAry, $selectionTags, $parSelected, $all=0, $noFirst=1);
	}
}
?>