<?
/*  Using:
 *
 *  2019-10-30 Bill [2019-1014-1517-02066]
 *      - modified getAvailability(), to allow class teacher access
 */

class kis_pushmessage extends libdb implements kis_apps {
        
    private $user_id;
    
    public static function getAvailability($user_id, $user_type, $student_id)
    {
		global $plugin;

        //if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"]){
        if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"]) {
		    return array('pushmessage', 'btn_pushmessage', '', '/home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/');
		}
		return array();
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id)
    {
		return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id)
    {
		return 0;
    }

    public function __construct($user_id, $user_type, $student_id, $params)
    {

    }

}
?>