<?
/*
 *  Using:
 *
 *  2019-10-16 Henry
 *      - add flag $sys_custom['ePOS']['enablePurchaseAndPickup'] to control open webview or not in getAvailability
 *
 *  2019-10-10 Cameron
 *      - add flag $sys_custom['ePOS']['exclude_webview'] to control open webview or not in getAvailability
 *
 *  2019-10-04 Cameron
 *      - allow parent to view ePOS webview by eClassApp
 */
class kis_pos extends libdb implements kis_apps {
        
    private $user_id;
    
    //not using now
    public static function getAvailability($user_id, $user_type, $student_id)
    {
    	global $plugin, $sys_custom;

        # eService > ePOS
        if($plugin['ePOS'] && ($user_type==kis::$user_types["parent"]) && !$sys_custom['ePOS']['exclude_webview'] && $sys_custom['ePOS']['enablePurchaseAndPickup']) {
            return array("pos", "btn_pos", "", "/home/eClassApp/common/ePOS/index.php?FromService=1");
        }
        else if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] && $plugin['ePOS'] && $plugin['payment']){
            return array('pos', 'btn_pos', '', '/home/eAdmin/GeneralMgmt/pos');
        }
    	return array();
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id)
    {
    	return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id)
    {
	    return 0;
    }

    public function __construct($user_id, $user_type, $student_id, $params)
    {

    }
    
}
?>