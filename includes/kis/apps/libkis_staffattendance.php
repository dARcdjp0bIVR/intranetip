<?php
// Editing by 
include_once($intranet_root."/includes/libstaffattend3.php");

class kis_staffattendance extends libdb implements kis_apps {
	
	private static $StaffAttend3 = 0;
	
	public function __construct($user_id, $user_type, $student_id, $params)
	{
		global $intranet_db;
		
		$this->db = $intranet_db;
	}
	
	public static function getAvailability($user_id, $user_type, $student_id)
	{
		global $plugin, $module_version;
		
		$StaffAttend3 = self::getLibStaffAttendanceInstance();
		// has module flag && version is 3.0 && (is admin or has management right)
		if ($_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] && $module_version['StaffAttendance'] == 3.0 &&
			 ($StaffAttend3->IS_ADMIN_USER() || $_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"])){
		    
		    if ($user_type == kis::$user_types['teacher']){
				return array('staffattendance', 'btn_staffattendance', 'wood', '/home/eAdmin/StaffMgmt/attendance/');
		    }
		}
		
		return array();
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id)
    {
    	global $plugin, $module_version;
    	$StaffAttend3 = self::getLibStaffAttendanceInstance();
		if ($_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] && $module_version['StaffAttendance'] == 3.0 && $StaffAttend3->IS_ADMIN_USER()){
		    return array('/home/eAdmin/StaffMgmt/attendance/');
		}
		return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id)
    {
		return 0;
    }
    
    public static function getLibStaffAttendanceInstance()
    {
    	if(!self::$StaffAttend3){
    		self::$StaffAttend3 = new libstaffattend3();
    	}
    	return self::$StaffAttend3;
    }
}

?>