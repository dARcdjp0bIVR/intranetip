<?php
// Editing by
/*
 * 2020-07-06 (Cameron): - modify updateAttendanceRecords() to use the same variable for $result
 * 2019-11-08 (Carlos): - modified getAvailability(), for staff user, only accessible if user is module admin or teaching staff.
 * 2019-09-27 (Cameron): - should call updateSchoolBusApplyLeaveToDelete() and updateSchoolBusTimeSlotApplyLeaveToDelete() when change status from early leave to present/late
 * 2019-09-26 (Cameron): - as change RecordStatus, AddedFrom, AttendanceRecordID, AttendanceApplyLeaveID to timeslot table,
 * should apply updateSchoolBusTimeSlotAttendanceRecordID() and updateSchoolBusTimeSlotApplyLeaveToDelete() to related logic for eSchoolBus
 * 2019-02-18 (Cameron): modify updateAttendanceRecords() to support sync eAttendance apply leave record to eSchoolBus
 * 2018-11-23 (Cameron): sync absent record to eSchoolBus and apply updateSchoolBusApplyLeaveToDelete to eSchoolBus in updateAttendanceRecords()
 *                      - fix: reason and remark should stripslashes before save in updateAttendanceRecords()
 * 2018-06-01 (Carlos): Create all necessary monthly tables in getAttendanceTableName($year, $month).
 * 2018-05-18 (Carlos): Fixed getAttendanceSummary() to follow setting ProfileAttendCount.
 * 2018-02-14 (Carlos): Log status change in updateAttendanceRecords().
 * 2017-10-10 (Carlos): modified updateAttendanceRecords($params, $apm), fix lunch out time / leave time 00:00:00 issue, should set it NULL to mark as not left yet.
 * 2017-09-11 (Carlos): modified loadAttendances() and updateAttendanceRecords() to handle AM and PM in/out times separately according to school attendance mode.  
 * 2017-08-22 (Carlos): added getLibcardstudentattendance() to expose class libcardstudentattend2 for public usage in controllers and views.
 * 						modified loadAttendances() to make non-confirmed attendance status,reason,remark to follow preset absence / outing records.
 * 2016-10-07 (Carlos): fixed updateAttendanceRecords($params, $apm) profile logic by reusing libcardstudentattend2->Set_Profile()
 * 2016-05-03 (Carlos): modified updateAttendanceRecords(), added ConfirmedUserID and IsConfirmed to update last confirmed person.
 * 2016-01-04 (Carlos): added getAttendanceMode() to get attendance mode. 
 * 						added getClassGroupSettings() for $sys_custom['KIS_ClassGroupAttendanceType'].
 * 2014-11-17 (Henry):	add flag to DisableParentAccess this modules
 * 2014-02-27 (Carlos): added updateClassConfirmRecord() and getClassConfirmRecord()
 * 2014-02-14 (Carlos): added getAvailableClassesByDate(), modified getAllClassesDayAttendanceSummary() to restrict teachers can only take own classes if corresponding setting is on
 * 2013-12-03 (Carlos): create daily_log table in getAttendanceTableName() 
 * 2013-10-16 (Carlos): added getStudentAcademicYearAttendanceSummary() for parent view monthly summary
 * 2013-10-08 (Carlos): added updateAttendanceRecords() to batch update student attendance records
 */
 
class kis_eattendance extends libdb implements kis_apps {
        
    private static $day_types = array(2=>'am','pm');
    private static $waive_types = array(1=>'absent', 'late', 'early');
    public static $attend_types = array('present','absent', 'late', 'outing');
    public static $leave_types = array(1=>'am','pm');
    private $user_id, $user_type, $student_id, $student_ids, $year, $month, $day, $summary, $academic_year_id, $academic_year_start, $academic_year_end;
    private $libcardstudentattend; 
    //not using now
    public static function getAvailability($user_id, $user_type, $student_id){
    
	global $plugin, $sys_custom;
	
	if ($plugin['attendancestudent']){
	    
	    if ($user_type == kis::$user_types['teacher']){
	    	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || $_SESSION['isTeaching']==1){ // student attendance admin or teaching staff
				return array('eattendance', 'btn_eattendance', 'wood', '');
	    	}else{
	    		return array();
	    	}
	    }else if ($user_type == kis::$user_types['parent'] && !$sys_custom['KIS_eAttendance']['DisableParentAccess']){
			return array('eattendance', 'btn_eattendance_p', '', '');
	    }else{
			return array();
	    }
	}
	
	return array();
    }
            
    public static function getAdminStatus($user_id, $user_type, $student_id){
    
	if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
	    return array('/home/eAdmin/StudentMgmt/attendance/');
	}
	return array();
    }

    public static function getNotificationCount($user_id, $user_type, $student_id){
    
	return 0;
	
    }
    private static function getAttendanceTableName($year, $month){
		global $intranet_root;
		
		include_once($intranet_root."/includes/libcardstudentattend2.php");
		
		$lcard = new libcardstudentattend2();
		$m = sprintf("%02d",$month);
		//$lcard->createTable_Card_Student_Daily_Log($year,$m);
		$lcard->createTable_LogAndConfirm($year,$m);
		
		return sprintf("CARD_STUDENT_DAILY_LOG_%d_%02d", $year, $month);
	
    }

    public function __construct($user_id, $user_type, $student_id, $params){

	global $intranet_db;
	
	$this->db = $intranet_db;
	$this->user_id = $user_id;
	$this->user_type = $user_type;
	$this->student_id = $student_id;
	$this->academic_year_id = $params['academic_year_id'];
	if($this->academic_year_id != '') {
		$this->academic_year_start = getStartOfAcademicYear('',$this->academic_year_id);
		$this->academic_year_end = getEndOfAcademicYear('',$this->academic_year_id);
	}
	
	$this->year = $params['year']? $params['year']: date('Y');
	$this->month = $params['month']? $params['month']: date('m');
	$this->day = $params['day']? $params['day']: date('d');
	$this->date = $this->year.'-'.sprintf('%02d',$this->month).'-'.sprintf('%02d',$this->day);
	$this->class_id = $params['class_id'];
	$this->table_name = self::getAttendanceTableName($this->year, $this->month);
	$this->permission = kis_utility::getGeneralSettings('StudentAttendance');
	
	if ($user_type==kis::$user_types['teacher'] && $params['class_id']){
	    $this->students = $this->getClassStudentIds($params['class_id']);
	}else{
	    $this->students = array($this->student_id);
	}
	
	$this->loadWaiveReords();
	
    }
    
    public function getLibcardstudentattendance()
    {
    	global $intranet_root;
    	include_once($intranet_root."/includes/libcardstudentattend2.php");
		
		if(empty($this->libcardstudentattend))
		{
			$this->libcardstudentattend = new libcardstudentattend2();
		}
		return $this->libcardstudentattend;
    }
    
    private function loadWaiveReords(){
	
	$start 	= mktime(0,0,0,1,1,$this->year);
	$end 	= mktime(0,0,0,1,1,$this->year+1);
	$users  = implode(",",$this->students);
	 
	$sql = "SELECT StudentID as user_id , UNIX_TIMESTAMP(RecordDate) as time , DayType as day_type, RecordType as type, RecordStatus as status
		FROM CARD_STUDENT_PROFILE_RECORD_REASON 
		WHERE RecordStatus='1' AND StudentID IN ($users) 
		AND UNIX_TIMESTAMP(RecordDate) BETWEEN $start AND $end";
		
	$records = $this->returnArray($sql);
	    
	foreach ($records as $record){
	    
	    $this->waive_record[$record['user_id']][date('n', $record['time'])][date('j', $record['time'])][self::$day_types[$record['day_type']]][self::$waive_types[$record['type']]] = $record['status'];
	    
	}
	
    }
    private function getClassStudentIds($class_id){
	
	$sql = "SELECT u.UserID
		FROM YEAR_CLASS_USER c
		INNER JOIN INTRANET_USER u ON u.UserID = c.UserID
		WHERE c.YearClassID = $class_id
		    AND u.RecordStatus='1' AND u.RecordType='2'";
		
	return $this->returnVector($sql);
	
    }
    private function getAttendanceSummary($students, $month, $day = null){
	
		$lcard = $this->getLibcardstudentattendance();
		$attendance_mode = $lcard->attendance_mode;
		$profile_count = $lcard->ProfileAttendCount == 1? 0.5 : 1;
		
		$modes = array();
		if(in_array($attendance_mode,array(0,2,3))){
			$modes[] = 'am';
		}
		if(in_array($attendance_mode,array(1,2,3))){
			$modes[] = 'pm';
		}
		$summary_values = array('present'=>0,'absent'=>0,'early_leave'=>0,'late'=>0,'outing'=>0);
		$summary = array('records_count'=>0,'total'=>$summary_values,'am'=>$summary_values,'pm'=>$summary_values);
		
		$attendances = $this->loadAttendances(array('students'=>$students,'day'=>$day), $month);
		
		foreach ($attendances as $i=>$a){
		    
		    if (!$a['id']) continue;
		    $summary['records_count']++;
		    	    
		    foreach ($modes as $t){
			
			$status = $a[$t.'_status'];
			$leave_status = $a['leave_status'];
			$modified = strtotime($a[$t.'_modified']);
			
			if ($summary[$t]['modified']<$modified){
			    
			    $summary[$t]['modified'] = $modified;
			    $summary[$t]['modified_by_b5'] = $a[$t.'_modified_by_b5'];
			    $summary[$t]['modified_by_en'] = $a[$t.'_modified_by_en'];
			    
			}
			if ($summary['total']['modified']<$modified){
			    
			    $summary['total']['modified'] = $modified;
			    $summary['total']['modified_by_b5'] = $a[$t.'_modified_by_b5'];
			    $summary['total']['modified_by_en'] = $a[$t.'_modified_by_en'];
			}
			
			if ($status == 'outing'){
			
			    $summary['total']['outing']+=$profile_count;
			    $summary[$t]['outing']++;
			    
			}else if ($status == 'absent'){
			    
			    if (!$this->waive_record[$a['user_id']][$month][$day][$t]['absent']){
				
				$summary['total']['absent']+=$profile_count;
				$summary[$t]['absent']++;
			    }
			    
			}else{	
			    
			    $present = $profile_count;
			    if ($leave_status==$t && !$this->waive_record[$a['user_id']][$month][$day][$t]['early']){
				$summary['total']['early_leave']++;
				$summary[$t]['early_leave']++;
				$present = 0;
				$a[$t.'_status'] = 'earlyleave';
				
			    }
			    if ($status=='late' && !$this->waive_record[$a['user_id']][$month][$day][$t]['late']){
				$summary['total']['late']++;
				$summary[$t]['late']++;
				$a[$t.'_status'] = $a[$t.'_status']=='earlyleave'? 'lateandearlyleave': $a[$t.'_status'];
				$present = 0;
			    }
			    
			    $summary['total']['present']+=$present;
			    $summary[$t]['present']+= ($profile_count == 0.5 ? $present*2 : $present);  
			}
			
		    }
		    
		}
		
		$summary['total']['modified'] = $summary['total']['modified']? date('Y-m-d H:i:s', $summary['total']['modified']): '';
		$summary['am']['modified'] = $summary['am']['modified']? date('Y-m-d H:i:s', $summary['am']['modified']): '';
		$summary['pm']['modified'] = $summary['pm']['modified']? date('Y-m-d H:i:s', $summary['pm']['modified']): '';
		
		
		return $summary;
    }
    
    private function getAttendances($students, $month, $day = null, $keyword='', $show_no_record=false, $sortby='', $order=''){
		
	$attendances = $this->loadAttendances(array('students'=>$students, 'day'=>$day, 'keyword'=>$keyword),
					      $month, $sortby, $order);
	
	$result_attendances = array();
	foreach ($attendances as $i=>$a){

	    if ($show_no_record || $a['id']){
		$result_attendances[] = $a;
	    }
	}
	
	return $result_attendances;
    }
    
    
    private function loadAttendances($params, $month, $sortby='', $order=''){
	
	extract($params);
	
	$lcardstudentattend = $this->getLibcardstudentattendance();
	$default_status = $lcardstudentattend->DefaultAttendanceStatus;
	if($default_status == '') $default_status = CARD_STATUS_ABSENT;
	$attendance_mode = $lcardstudentattend->attendance_mode;
	
	$table_name = self::getAttendanceTableName($this->year, $month);

	$sort .= $sortby? "$sortby $order, ":"";

	$cond .= isset($students)?  "u.UserID IN (".implode(",",$students).") AND": "";
	$cond .= isset($record_id)? "d.RecordID = $record_id AND":"";
	
	if (isset($day)){
	    
	    $day_number = " AND d.DayNumber = $day";
	    $date = $this->year.'-'.sprintf('%02d',$month).'-'.sprintf('%02d',$day);
	    
	    $fields .= "IF(ra.RecordID IS NOT NULL,ra.Remark,IF(lam.RecordID IS NOT NULL,lam.Remark,IF(outam.OutingID IS NOT NULL,outam.Detail,''))) as am_remark,
			IF(rp.RecordID IS NOT NULL,rp.Remark,IF(lpm.RecordID IS NOT NULL,lpm.Remark,IF(outpm.OutingID IS NOT NULL,outpm.Detail,''))) as pm_remark,
			IF(rsa.RecordID IS NOT NULL,rsa.Reason,IF(lam.RecordID IS NOT NULL,lam.Reason,IF(outam.OutingID IS NOT NULL,outam.Objective,''))) as am_reason,
			IF(rsp.RecordID IS NOT NULL,rsp.Reason,IF(lpm.RecordID IS NOT NULL,lpm.Reason,IF(outpm.OutingID IS NOT NULL,outpm.Objective,''))) as pm_reason,
			outam.OutingID as am_outing_id,
			outpm.OutingID as pm_outing_id,
			lam.RecordID as am_preset_leave_id,
			lpm.RecordID as pm_preset_leave_id,
			d.IsConfirmed as am_confirmed,
			d.PMIsConfirmed as pm_confirmed, ";
	    
	    $tables .= "LEFT JOIN CARD_STUDENT_DAILY_REMARK ra ON
			    ra.RecordDate = '$date' AND ra.StudentID = u.UserID
			    AND ra.DayType = '2'
			LEFT JOIN CARD_STUDENT_DAILY_REMARK rp ON
			    rp.RecordDate = '$date' AND rp.StudentID = u.UserID
			    AND rp.DayType = '3'
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON rsa ON 
			    rsa.RecordDate = '$date' AND rsa.StudentID = u.UserID 
			    AND rsa.DayType = '2'
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON rsp ON 
			    rsp.RecordDate = '$date' AND rsp.StudentID = u.UserID 
			    AND rsp.DayType = '3' ";
		$tables .= " LEFT JOIN CARD_STUDENT_OUTING AS outam ON (outam.UserID=u.UserID AND outam.RecordDate = '".$date."' AND outam.DayType = '2') 
					 LEFT JOIN CARD_STUDENT_OUTING AS outpm ON (outpm.UserID=u.UserID AND outpm.RecordDate = '".$date."' AND outpm.DayType = '3') 
					 LEFT JOIN CARD_STUDENT_PRESET_LEAVE as lam ON lam.StudentID=u.UserID AND lam.RecordDate='".$date."' AND lam.DayPeriod='2' 
					 LEFT JOIN CARD_STUDENT_PRESET_LEAVE as lpm ON lpm.StudentID=u.UserID AND lpm.RecordDate='".$date."' AND lpm.DayPeriod='3' ";
		    
	}else{
		$fields .= "ra.Remark as am_remark,
			rp.Remark as pm_remark,
			rsa.Reason as am_reason,
			rsp.Reason as pm_reason,";
	    
	    $tables .= "LEFT JOIN CARD_STUDENT_DAILY_REMARK ra ON
			    ra.RecordDate = DATE_FORMAT(CONCAT('".$this->year."-$month-',d.DayNumber),'%Y-%m-%d') AND ra.StudentID = u.UserID
			    AND ra.DayType = '2'
			LEFT JOIN CARD_STUDENT_DAILY_REMARK rp ON
			    rp.RecordDate = DATE_FORMAT(CONCAT('".$this->year."-$month-',d.DayNumber),'%Y-%m-%d') AND rp.StudentID = u.UserID
			    AND rp.DayType = '3'
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON rsa ON 
			    rsa.RecordDate = DATE_FORMAT(CONCAT('".$this->year."-$month-',d.DayNumber),'%Y-%m-%d') AND rsa.StudentID = u.UserID 
			    AND rsa.DayType = '2'
			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON rsp ON 
			    rsp.RecordDate = DATE_FORMAT(CONCAT('".$this->year."-$month-',d.DayNumber),'%Y-%m-%d') AND rsp.StudentID = u.UserID 
			    AND rsp.DayType = '3'";
	}
	
	$sql = "SELECT
		    d.RecordID id,
		    u.UserID user_id,
		    u.ClassNumber user_class_number,
		    u.PhotoLink user_photo,
		    u.ChineseName user_name_b5,
		    u.EnglishName user_name_en,
		    d.DayNumber day,
		    
		    IF(d.AMStatus IS NULL, IF (d.InSchoolTime IS NOT NULL , 0, '".$default_status."'), d.AMStatus) am_status,
		    d.DateModified am_modified,
		    au.ChineseName am_modified_by_b5,
		    au.EnglishName am_modified_by_en,
		    		   
		    IF(d.PMStatus IS NULL,";
	if($attendance_mode == 2 && $lcardstudentattend->PMStatusNotFollowAMStatus != 1){ // whole day with lunch mode and PM follow AM status
		$sql .=	" IF( (d.AMStatus IN (0,2) AND d.LeaveStatus IS NULL)
					    OR d.LunchBackTime IS NOT NULL 
					, 0, 
					IF(d.AMStatus='3',3,1) 
				  ) ";
	}else{ // PM do not follow AM status
		$sql .= " '$default_status' ";
	}
	$sql .= ", d.PMStatus) pm_status,
		    
		    d.PMDateModified pm_modified,
		    pu.ChineseName pm_modified_by_b5,
		    pu.EnglishName pm_modified_by_en,
		   
		    $fields
		    
		    d.LunchOutTime lunch_out_time,
		    d.LunchBackTime lunch_back_time,
		   
		    IF( (d.AMStatus IN (1,3) OR (d.AMStatus IS NULL AND d.InSchoolTime IS NULL)) AND
			(d.PMStatus IN (1,3) OR d.PMStatus IS NULL OR (d.LunchOutTime IS NOT NULL AND d.LunchBackTime IS NULL))
		    , '', d.InSchoolTime) in_school_time,
			
		    IF( (d.AMStatus IN (1,3) OR (d.AMStatus IS NULL AND d.InSchoolTime IS NULL)) AND
			(d.PMStatus IN (1,3) OR d.PMStatus IS NULL OR (d.LunchOutTime IS NOT NULL AND d.LunchBackTime IS NULL))
		    , '', d.LeaveSchoolTime) leave_school_time,
		    
		    d.LeaveStatus leave_status

		FROM INTRANET_USER u  
		LEFT JOIN $table_name d ON d.UserID=u.UserID $day_number
		LEFT JOIN INTRANET_USER au ON d.ModifyBy=au.UserID
		LEFT JOIN INTRANET_USER pu ON d.PMModifyBy=pu.UserID
		$tables 
		WHERE 
		    $cond (
			u.ClassNumber like '%$keyword%' OR
			u.ChineseName like '%$keyword%' OR
			u.EnglishName like '%$keyword%'
		    )
		GROUP BY u.UserID, day
		ORDER BY $sort u.ClassNumber";

	$items = $this->returnArray($sql);
	$attendances = array();

	foreach ($items as $item){
	   
	    $attendance = $item;
	    $attendance['leave_status'] = self::$leave_types[$item['leave_status']];
	    $attendance['am_status']    = self::$attend_types[$item['am_status']];
	    $attendance['pm_status']    = self::$attend_types[$item['pm_status']];

	    if ($attendance['leave_status']=='am'){
			$attendance['am_status'] = $attendance['am_status']=='late'?'lateandearlyleave': 'earlyleave';
	    }else if ($attendance['leave_status']=='pm'){
			$attendance['pm_status'] = $attendance['pm_status']=='late'?'lateandearlyleave': 'earlyleave';
	    }
	    if($attendance['am_confirmed']=='' && $attendance['am_preset_leave_id']!=''){
	    	$attendance['am_status'] = 'absent';
	    }
	    if($attendance['am_confirmed']=='' && $attendance['am_outing_id']!=''){
	    	$attendance['am_status'] = 'outing';
	    }
	    $attendance['am_here'] = in_array($attendance['am_status'], array('present', 'late'));
					      
	    if (!$attendance['pm_status']){
			$attendance['pm_status']  = $attendance['am_here'] && (!$item['lunch_out_time'] || $item['lunch_back_time'])? 'present': 'absent';
	    }
	    if($attendance['pm_confirmed']=='' && $attendance['pm_preset_leave_id']!=''){
	    	$attendance['pm_status'] = 'absent';
	    }
	    if($attendance['pm_confirmed']=='' && $attendance['pm_outing_id']!=''){
	    	$attendance['pm_status'] = 'outing';
	    }
	    $attendance['pm_here'] = in_array($attendance['pm_status'], array('present', 'late'));
	    
	    list($attendance['in_school_hour'],$attendance['in_school_min'],$attendance['in_school_sec']) = explode(':',$item['in_school_time']);
	    list($attendance['leave_school_hour'],$attendance['leave_school_min'],$attendance['leave_school_sec']) = explode(':',$item['leave_school_time']);
	    list($attendance['lunch_out_hour'],$attendance['lunch_out_min'],$attendance['lunch_out_sec']) = explode(':',$item['lunch_out_time']);
	    list($attendance['lunch_back_hour'],$attendance['lunch_back_min'],$attendance['lunch_back_sec']) = explode(':',$item['lunch_back_time']);
	    
	    $attendance['am_reason'] = $item['am_reason'];
	    $attendance['am_remark'] = $item['am_remark'];
	    $attendance['pm_reason'] = $item['pm_reason'];
	    $attendance['pm_remark'] = $item['pm_remark'];
	    	    
	    $attendances[] = $attendance;
		
	}

        return $attendances;
    }
    
    private function getAttendanceDefaultReason(){
	
    }
    
    public function getAttendance(){
	
	return current($this->loadAttendances(array('students'=>array($this->student_id),'day'=>$this->day), $this->month));
	
    }
    public function setAttendance($params, $apm){
	
	extract($params);
		
	$apm_prefix = $apm=='pm'?'PM':'';
	
	if ($status=='lateandearlyleave'){
	    $status = 'late';
	    $leave_status = array_search($apm, self::$leave_types);
	}
	if ($status=='earlyleave'){
	    $status = 'present';
	    $leave_status = array_search($apm, self::$leave_types);
	}

	$status = array_search($status, self::$attend_types);
	$day_type = array_search($apm, self::$day_types);
	
	$sql = "UPDATE ".$this->table_name." SET
		    InSchoolTime = '$in_school_time',
		    LeaveSchoolTime = '$leave_school_time',
		    LeaveStatus = '$leave_status',
		    ".strtoupper($apm)."Status = '$status',
		    {$apm_prefix}DateModified = now(),
		    {$apm_prefix}ModifyBy = ".$this->user_id."
		WHERE
		    UserID = ".$this->student_id." AND
		    DayNumber = '".$this->day."'";
		    
	$this->db_db_query($sql);
	
	$sql = "UPDATE %s SET
		    %s = '%s'
		WHERE
		    RecordDate = '".$this->date."' AND
		    StudentID = ".$this->student_id." AND
		    DayType = '$day_type'";
	
	$this->db_db_query(sprintf($sql, 'CARD_STUDENT_PROFILE_RECORD_REASON', 'Reason', $reason));
	$this->db_db_query(sprintf($sql, 'CARD_STUDENT_DAILY_REMARK', 'Remark',$remark));
	
		    
	if (!$this->db_affected_rows()){
	    
	    $sql = "INSERT INTO ".$this->table_name." (
			InSchoolTime, LeaveSchoolTime,
			LeaveStatus, ".strtoupper($apm)."Status,
			{$apm_prefix}DateModified, {$apm_prefix}ModifyBy,
			DateInput, InputBy,
			DayNumber, UserID
		    )VALUES(
			'$in_school_time', '$leave_school_time',
			'$leave_status', '$status',
			now(), ".$this->user_id.",
			now(), ".$this->user_id.",
			'".$this->day."', ".$this->student_id."
		    )";
		    
	    $this->db_db_query($sql);
	    
	    $sql = "INSERT INTO %s (
			%s, RecordDate,
			StudentID, DayType
		    )VALUES(
			'%s', '".$this->date."',
			".$this->student_id.", '%s'
		    )";
		    
	    $this->db_db_query(sprintf($sql, 'CARD_STUDENT_PROFILE_RECORD_REASON', 'Reason', $reason, 2));
	    $this->db_db_query(sprintf($sql, 'CARD_STUDENT_DAILY_REMARK', 'Remark', $remark, 2));
	    $this->db_db_query(sprintf($sql, 'CARD_STUDENT_PROFILE_RECORD_REASON', 'Reason', $reason, 3));
	    $this->db_db_query(sprintf($sql, 'CARD_STUDENT_DAILY_REMARK', 'Remark', $remark, 3));
	}
	
    }
	
	/*
	 * @param $date : YYYY-mm-dd
	 * @param $apm : 'AM' or 'PM'
	 * @param $params : 2D array of attendance data
	 * 					array('student_id'=>array(),'status'=>array(),'in_school_time'=>array(),'leave_school_time'=>array(),'reason'=>array(),'remark'=>array())
	 */
	public function updateAttendanceRecords($params, $apm)
	{
		global $intranet_root, $plugin, $sys_custom, $PATH_WRT_ROOT;
		
		if ($plugin['eSchoolBus']) {
		    global $schoolBusConfig;
		    include ($intranet_root. "/includes/eSchoolBus/schoolBusConfig.php");
		    include_once($intranet_root."/includes/eSchoolBus/libSchoolBus_db.php");
		    $leSchoolBus = new libSchoolBus_db();
		}
		
		include_once($intranet_root.'/includes/libcardstudentattend2.php');
		
		$lcard = new libcardstudentattend2();
		$attendance_mode = $lcard->attendance_mode;
		$has_magic_quotes = $lcard->is_magic_quotes_active;
		
		$profile_type_map = array('late'=>PROFILE_TYPE_LATE,'absent'=>PROFILE_TYPE_ABSENT,'earlyleave'=>PROFILE_TYPE_EARLY,'outing'=>CARD_STATUS_OUTING);
		
		extract($params);
		$record_count = sizeof($student_id);
		
		$apm_prefix = $apm=='pm'?'PM':'';
		
		$studentIdToDailyLog = $lcard->getDailyLogRecords(array('RecordDate'=>$this->date,'StudentID'=>$student_id,'StudentIDToRecord'=>1));

		$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' ';
		
		if($attendance_mode == 0){ // am only
			$in_time_field = 'InSchoolTime';
			$out_time_field = 'LeaveSchoolTime';
			// AMStatus
			// LeaveStatus 
		}else if($attendance_mode == 1){ // pm only
			$in_time_field = 'InSchoolTime';
			$out_time_field = 'LeaveSchoolTime';
			// PMStatus
			// LeaveStatus
		}else if($attendance_mode == 2){ // whole day with lunch time
			$in_time_field = $apm == 'am'? 'InSchoolTime' : 'LunchBackTime';
			$out_time_field = $apm == 'am'? 'LunchOutTime' : 'LeaveSchoolTime';
			// AMStatus
			// PMStatus
			// LeaveStatus
		}else if($attendance_mode == 3){ // whole day without lunch time
			$in_time_field = $apm == 'am'? 'InSchoolTime' : 'LunchBackTime';
			$out_time_field = 'LeaveSchoolTime';
			// AMStatus
			// PMStatus
			// LeaveStatus
		}
		
		$dayType = ($apm=='am') ? 2 : 3;
		
		$appliedLeaveStudentAry = $lcard->checkStudentApplyLeave($student_id,$this->date,$dayType);
		$appliedLeaveStudentIDAry = array_keys($appliedLeaveStudentAry);

		$result = array();
		for($i=0;$i<$record_count;++$i)
		{
			$raw_status = $status[$i];
			$leave_status = $leave_school_time[$i]!='00:00:00'? '0' : 'NULL';
			$isSchoolBusEarlyLeave = false;
			if ($status[$i]=='lateandearlyleave'){
			    $status[$i] = 'late';
			    $leave_status = array_search($apm, self::$leave_types);
			    $isSchoolBusEarlyLeave = true;
			}
			if ($status[$i]=='earlyleave'){
			    $status[$i] = 'present';
			    $leave_status = array_search($apm, self::$leave_types);
			    $isSchoolBusEarlyLeave = true;
			}

			$status[$i] = array_search($status[$i], self::$attend_types);
			$day_type = array_search($apm, self::$day_types);
			
			$sql = "UPDATE ".$this->table_name." SET
				    $in_time_field = ".($in_school_time[$i]=='00:00:00'? "NULL" : "'".$in_school_time[$i]."'").",
				    $out_time_field = ".($leave_school_time[$i]=='00:00:00'? "NULL" : "'".$leave_school_time[$i]."'").",
				    LeaveStatus = ".$leave_status.",
				    ".strtoupper($apm)."Status = '".$status[$i]."',
					{$apm_prefix}ConfirmedUserID = ".$this->user_id.",
					{$apm_prefix}IsConfirmed='1',
				    {$apm_prefix}DateModified = now(),
				    {$apm_prefix}ModifyBy = ".$this->user_id."
				WHERE
				    UserID = ".$student_id[$i]." AND
				    DayNumber = '".$this->day."'";
				    
			$result['UpdateCardLog_'.$i] = $this->db_db_query($sql);

            if($has_magic_quotes){
                $remark[$i] = stripslashes(trim($remark[$i]));
                $reason[$i] = stripslashes(trim($reason[$i]));
            }

			$sql = "UPDATE %s SET
				    %s = '%s' 
				WHERE
				    RecordDate = '".$this->date."' AND
				    StudentID = ".$student_id[$i]." AND
				    DayType = '$day_type'";
			
			//$result['UpdateProfile_'.$i] = $this->db_db_query(sprintf($sql, 'CARD_STUDENT_PROFILE_RECORD_REASON', 'Reason', $this->Get_Safe_Sql_Query($reason[$i])));
			$result['UpdateRemark_'.$i] = $this->db_db_query(sprintf($sql, 'CARD_STUDENT_DAILY_REMARK', 'Remark', $this->Get_Safe_Sql_Query($remark[$i])));

			$synceSchoolBusByUpdate = true;
			if (!$this->db_affected_rows()){        // this is not the affected nubmer of rows from updating $this->table_name but from updating remark
                $synceSchoolBusByUpdate = false;
			    $sql = "INSERT INTO ".$this->table_name." (
					$in_time_field, $out_time_field,
					LeaveStatus, ".strtoupper($apm)."Status,
					{$apm_prefix}ConfirmedUserID,{$apm_prefix}IsConfirmed,
					{$apm_prefix}DateModified, {$apm_prefix}ModifyBy,
					DateInput, InputBy,
					DayNumber, UserID
				    )VALUES(
					".($in_school_time[$i]=='00:00:00'? "NULL" : "'".$in_school_time[$i]."'").", ".($leave_school_time[$i]=='00:00:00' ? "NULL" : "'".$leave_school_time[$i]."'").",
					$leave_status, '".$status[$i]."',
					".$this->user_id.",'1',
					now(), ".$this->user_id.",
					now(), ".$this->user_id.",
					'".$this->day."', ".$student_id[$i]."
				    )";

			    $result['InsertCardLog_'.$i] = $this->db_db_query($sql);
			    $logRecordID = $this->db_insert_id();
			    
			    if ($logRecordID == 0) {     // attendance record already exist
			        $sql = "SELECT RecordID FROM ".$this->table_name." WHERE UserID='".$student_id[$i]."' AND DayNumber='".$this->day."'";
			        $attendanceAry = $this->returnResultSet($sql);
			        if (count($attendanceAry)) {
			            $logRecordID = $attendanceAry[0]['RecordID'];
			        }
			    }
			    
			    $sql = "INSERT INTO %s (
					%s, RecordDate,
					StudentID, DayType
				    )VALUES(
					'%s', '".$this->date."',
					".$student_id[$i].", '%s'
				    )";
				
				if($apm == 'am')
				{    
			    	//$result['InsertProfileAM_'.$i] = $this->db_db_query(sprintf($sql, 'CARD_STUDENT_PROFILE_RECORD_REASON', 'Reason', $this->Get_Safe_Sql_Query($reason[$i]), 2));
			    	$result['InsertRemarkAM_'.$i] = $this->db_db_query(sprintf($sql, 'CARD_STUDENT_DAILY_REMARK', 'Remark', $this->Get_Safe_Sql_Query($remark[$i]), 2));
				}
				if($apm == 'pm')
				{
			    	//$result['InsertProfilePM_'.$i] = $this->db_db_query(sprintf($sql, 'CARD_STUDENT_PROFILE_RECORD_REASON', 'Reason', $this->Get_Safe_Sql_Query($reason[$i]), 3));
			    	$result['InsertRemarkPM_'.$i] = $this->db_db_query(sprintf($sql, 'CARD_STUDENT_DAILY_REMARK', 'Remark', $this->Get_Safe_Sql_Query($remark[$i]), 3));
				}
				
				if ($plugin['eSchoolBus']) {
				    $currentStudentID = $student_id[$i];
				    
				    $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($currentStudentID);
				    if ($isTakingBusStudent) {
				        if ($status[$i] == CARD_STATUS_ABSENT || $status[$i] == CARD_STATUS_OUTING) {
				            $dayTypeStr = strtoupper($apm);
				            if ($logRecordID) {
    				            if (in_array($currentStudentID,(array)$appliedLeaveStudentIDAry)) {
    				                $appliedLeaveRecordID = $appliedLeaveStudentAry[$currentStudentID];
    				                $conditionAry = array();
    				                $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
    				                $conditionAry['StudentID'] = $currentStudentID;
    				                $conditionAry['StartDate'] = $this->date;
    				                $result['UpdateSchoolBusAttendanceRecordID_'.$i] = $leSchoolBus->updateSchoolBusAttendanceRecordID($logRecordID, $conditionAry);

                                    unset($conditionAry);
                                    $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                                    $conditionAry['t.LeaveDate'] = $this->date;
                                    $result['UpdateSchoolBusTimeSlotAttendanceRecordID_'.$i] = $leSchoolBus->updateSchoolBusTimeSlotAttendanceRecordID($currentStudentID, $logRecordID, $conditionAry);
    				            }
    				            else {
                                    $result['SyncAbsentRecordToeSchoolBus_'.$i] = $leSchoolBus->syncAbsentRecordFromAttendance($currentStudentID, $this->date, $reason[$i], $remark[$i], $dayTypeStr, $logRecordID);
                                }
				            }
				        }
				        else if ($status[$i]== CARD_STATUS_PRESENT || $status[$i]== CARD_STATUS_LATE) {
				            if ($isSchoolBusEarlyLeave) {       // regard as not take school bus (absent)
				                if ($logRecordID) {
    				                if (in_array($currentStudentID,(array)$appliedLeaveStudentIDAry)) {
    				                    $appliedLeaveRecordID = $appliedLeaveStudentAry[$currentStudentID];
    				                    if ($leSchoolBus->isSchoolBusApplyLeaveRecordExist($appliedLeaveRecordID, $currentStudentID, $this->date)) {
        				                    $conditionAry = array();
        				                    $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
        				                    $conditionAry['StudentID'] = $currentStudentID;
        				                    $conditionAry['StartDate'] = $this->date;
        				                    $result['UpdateSchoolBusAttendanceRecordID_'.$i] = $leSchoolBus->updateSchoolBusAttendanceRecordID($logRecordID, $conditionAry);
        				                    
        				                    // update reason and teacher's remark only
        				                    $result['UpdateReasonAndRemark_'.$logRecordID] = $leSchoolBus->updateReasonAndRemark($currentStudentID, $this->date, $logRecordID, $reason[$i], $remark[$i]);
    				                    }
    				                    else { // add new record
    				                        $dayTypeStr = 'PM';
    				                        $result['SyncAbsentRecordToeSchoolBus_'.$logRecordID] = $leSchoolBus->syncAbsentRecordFromAttendance($currentStudentID, $this->date, $reason[$i], $remark[$i], $dayTypeStr, $logRecordID, $attendanceType=3);
    				                    }
    				                }
    				                else {
    				                    $dayTypeStr = 'PM';
    				                    $result['SyncAbsentRecordToeSchoolBus_'.$logRecordID] = $leSchoolBus->syncAbsentRecordFromAttendance($currentStudentID, $this->date, $reason[$i], $remark[$i], $dayTypeStr, $logRecordID, $attendanceType=3);
    				                }
				                }
				            }
				            else if (in_array($currentStudentID,(array)$appliedLeaveStudentIDAry)) {
    				            $appliedLeaveRecordID = $appliedLeaveStudentAry[$currentStudentID];
    				            $conditionAry = array();
    				            $conditionAry['AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
    				            $conditionAry['StudentID'] = $currentStudentID;
    				            $conditionAry['StartDate'] = $this->date;
    				            $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
    				            $result['ChangeFromAbsentToOthers_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                                unset($conditionAry);
                                $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                                $conditionAry['t.LeaveDate'] = $this->date;
                                $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                                $result['ChangeFromAbsentToOthers_Timeslot_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($currentStudentID, $conditionAry);
				            }
				            else if ($logRecordID) {        // update from early leave / late and early leave to present / late
                                $conditionAry = array();
                                $conditionAry['AttendanceRecordID'] = $logRecordID;
                                $conditionAry['StudentID'] = $currentStudentID;
                                $conditionAry['StartDate'] = $this->date;
                                $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                                $result['ChangeFromAbsentToOnTime_'.$logRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                                unset($conditionAry);
                                $conditionAry['t.AttendanceRecordID'] = $logRecordID;
                                $conditionAry['t.LeaveDate'] = $this->date;
                                $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                                $result['ChangeFromAbsentToOthers_Timeslot_'.$logRecordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($currentStudentID, $conditionAry);
                            }
				        }
				    }
				}
				
			}


			if($raw_status == 'present')
			{
				$lcard->Clear_Profile($student_id[$i],$this->date,$day_type,false);
			}
			if(in_array($raw_status,array('absent','late','earlyleave','outing'))){
				$lcard->Set_Profile($student_id[$i],$this->date,$day_type,$profile_type_map[$raw_status],$reason[$i],$remark[$i],$LateSession="|**NULL**|",$HandInLetter="|**NULL**|",$in_school_time[$i],$UpdateLateTime=false,$Waive='|**NULL**|',$status[$i]=='earlyleave',$eDisUpdate=false);
			}
			if($raw_status == 'lateandearlyleave'){
				$lcard->Set_Profile($student_id[$i],$this->date,$day_type,$profile_type_map['late'],$reason[$i],$remark[$i],$LateSession="|**NULL**|",$HandInLetter="|**NULL**|",$in_school_time[$i],$UpdateLateTime=false,$Waive='|**NULL**|',true,$eDisUpdate=false);
				$lcard->Set_Profile($student_id[$i],$this->date,$day_type,$profile_type_map['earlyleave'],$reason[$i],$remark[$i],$LateSession="|**NULL**|",$HandInLetter="|**NULL**|",$in_school_time[$i],$UpdateLateTime=false,$Waive='|**NULL**|',true,$eDisUpdate=false);
			}
			
			$old_in_status = isset($studentIdToDailyLog[$student_id[$i]])? $studentIdToDailyLog[$student_id[$i]][strtoupper($apm)."Status"] : "NULL";
			
			$old_out_status = isset($studentIdToDailyLog[$student_id[$i]])? $studentIdToDailyLog[$student_id[$i]]['LeaveStatus'] : "NULL";
			$log_row .= '['.$this->date.' '.$day_type.' '.$student_id[$i].' '.$old_in_status.' '.$old_out_status.' '.$status[$i].' '.$leave_status.']';
			
			if ($plugin['eSchoolBus'] && count($studentIdToDailyLog) && $synceSchoolBusByUpdate) {
			    
			    $currentStudentID = $student_id[$i];
			    $logRecordID = $studentIdToDailyLog[$currentStudentID]['RecordID'];
			    
			    if ($logRecordID) {
			        $isTakingBusStudent = $leSchoolBus->isTakingBusStudent($currentStudentID);
			        if ($isTakingBusStudent) {

			            // update reason and teacher's remark only
			            $result['UpdateReasonAndRemark_'.$logRecordID] = $leSchoolBus->updateReasonAndRemark($currentStudentID, $this->date, $logRecordID, $reason[$i], $remark[$i]);

			            if ($old_in_status != $status[$i]) {
                            $deleteApplyLeave = false;
    			            # Late / Present -> Absent / Outgoing
    			            if (($old_in_status==CARD_STATUS_PRESENT || $old_in_status==CARD_STATUS_LATE ) && ($status[$i]==CARD_STATUS_ABSENT || $status[$i]==CARD_STATUS_OUTING)) {
    			                $dayTypeStr = strtoupper($apm);
    			                $attendanceApplyLeaveID = $appliedLeaveStudentAry[$currentStudentID];
    			                $isSchoolBusApplyLeaveRecordExist = $leSchoolBus->isSchoolBusApplyLeaveRecordExist($attendanceApplyLeaveID, $currentStudentID, $this->date, $dayTypeStr);

    			                if (in_array($currentStudentID,(array)$appliedLeaveStudentIDAry) && $isSchoolBusApplyLeaveRecordExist) {
    			                    $conditionAry = array();
    			                    $conditionAry['AttendanceRecordID'] = $logRecordID;
    			                    $conditionAry['StudentID'] = $currentStudentID;
    			                    $conditionAry['StartDate'] = $this->date;
    			                    $result['UpdateSchoolBusAttendanceRecordID_'.$i] = $leSchoolBus->updateSchoolBusAttendanceRecordID($logRecordID, $conditionAry);

                                    unset($conditionAry);
                                    $conditionAry['t.AttendanceApplyLeaveID'] = $appliedLeaveRecordID;
                                    $conditionAry['t.LeaveDate'] = $this->date;
                                    $result['UpdateSchoolBusTimeSlotAttendanceRecordID_'.$i] = $leSchoolBus->updateSchoolBusTimeSlotAttendanceRecordID($currentStudentID, $logRecordID, $conditionAry);
    			                }
    			                else {
    			                    $result['SyncAbsentRecordToeSchoolBus_'.$i] = $leSchoolBus->syncAbsentRecordFromAttendance($currentStudentID, $this->date, $reason[$i], $remark[$i], $dayTypeStr, $logRecordID);
    			                }
        	   		        }
        			    
        	   		        # Absent / Outgoing -> Late / Present 
        	   		        if (($old_in_status==CARD_STATUS_ABSENT  || $old_in_status ==CARD_STATUS_OUTING) && ($status[$i]==CARD_STATUS_PRESENT || $status[$i]==CARD_STATUS_LATE) && !$isSchoolBusEarlyLeave) {
        	   		            $conditionAry = array();
            			        $conditionAry['AttendanceRecordID'] = $logRecordID;     // delete only if the absent is created from eAttendance, otherwise, need to cancel the application by teacher
            			        $conditionAry['StudentID'] = $currentStudentID;
            			        $conditionAry['StartDate'] = $this->date;
            			        $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
        			            $result['ChangeFromAbsentToOthers_'.$logRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                                unset($conditionAry);
                                $conditionAry['t.AttendanceRecordID'] = $logRecordID;   // delete only if the absent is created from eAttendance, otherwise, need to cancel the application by teacher
                                $conditionAry['t.LeaveDate'] = $this->date;
                                $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                                $result['ChangeFromAbsentToOthers_Timeslot_'.$logRecordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($currentStudentID, $conditionAry);

                                $deleteApplyLeave = true;       // Use AttendanceApplyLeaveID to filter rather than AttendanceRecordID in case AttendanceRecordID is empty
                                $attendanceApplyLeaveID= $appliedLeaveStudentAry[$currentStudentID];
            			    }


            			    # Present -> Late && have applied leave
            			    if ($old_in_status==CARD_STATUS_PRESENT && $status[$i]==CARD_STATUS_LATE && !$isSchoolBusEarlyLeave && in_array($currentStudentID,(array)$appliedLeaveStudentIDAry)) {
            			        $deleteApplyLeave = true;
            			        $attendanceApplyLeaveID= $appliedLeaveStudentAry[$currentStudentID];
            			    }
            			    
            			    # Late -> Present && have applied leave
            			    if ($old_in_status==CARD_STATUS_LATE  && $status[$i]==CARD_STATUS_PRESENT && !$isSchoolBusEarlyLeave && in_array($currentStudentID,(array)$appliedLeaveStudentIDAry)) {
            			        $deleteApplyLeave = true;
            			        $attendanceApplyLeaveID = $appliedLeaveStudentAry[$currentStudentID];
            			    }
            			    
            			    if ($deleteApplyLeave) {
            			        $conditionAry = array();
            			        $conditionAry['AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
            			        $conditionAry['StudentID'] = $currentStudentID;
            			        $conditionAry['StartDate'] = $this->date;
            			        $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
            			        $result['ChangeFromAbsentToOthers_'.$logRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                                unset($conditionAry);
                                $conditionAry['t.AttendanceApplyLeaveID'] = $attendanceApplyLeaveID;
                                $conditionAry['t.LeaveDate'] = $this->date;
                                $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                                $result['ChangeFromAbsentToOthers_TimeSlot_'.$currentStudentID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($currentStudentID, $conditionAry);
            			    }
			            }
			            else {       // status not changed
                            if ($status[$i]==CARD_STATUS_ABSENT || $status[$i]==CARD_STATUS_OUTING || $isSchoolBusEarlyLeave) {
			                    $result['UpdateReasonAndRemark_'.$logRecordID] = $leSchoolBus->updateReasonAndRemark($currentStudentID, $this->date, $logRecordID, $reason[$i], $remark[$i]);
			                }
			                else if (($status[$i]==CARD_STATUS_PRESENT || $status[$i]==CARD_STATUS_LATE) && !$isSchoolBusEarlyLeave && in_array($currentStudentID,(array)$appliedLeaveStudentIDAry)) {
			                    $conditionAry = array();
			                    $conditionAry['AttendanceRecordID'] = $logRecordID;
			                    $conditionAry['StudentID'] = $currentStudentID;
			                    $conditionAry['StartDate'] = $this->date;
			                    $conditionAry['RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
			                    $result['ChangeFromAbsentToOthers_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusApplyLeaveToDelete($conditionAry);

                                unset($conditionAry);
                                $conditionAry['t.AttendanceRecordID'] = $logRecordID;
                                $conditionAry['t.LeaveDate'] = $this->date;
                                $conditionAry['t.RecordStatus'] = $schoolBusConfig['ApplyLeaveStatus']['Absent'];
                                $result['ChangeFromAbsentToOthers_TimeSlot_'.$appliedLeaveRecordID] = $leSchoolBus->updateSchoolBusTimeSlotApplyLeaveToDelete($currentStudentID, $conditionAry);
			                }
			            }
			        }
			    }
			}
			
		}
		$log_row .= ' '.(count($_POST)>0?str_replace(array("\r\n","\n","\r"),' ',serialize($_POST)):'');
		$lcard->log($log_row);
		
		return !in_array(false,$result);
	}
	
    public function getAllClassesDayAttendanceSummary($keyword='', $apm='total'){
		
		global $intranet_root;
		
		if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]){
			$target_date = sprintf("%4d-%02d-%02d", $this->year, $this->month, $this->day);
			$available_classId = $this->getAvailableClassesByDate($target_date);
		}
		
		$classes = kis_utility::getAcademicYearClasses(array('keyword'=>$keyword));
	
		$classes_summary = array();
		foreach ($classes as $class){
		   if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && !in_array($class['class_id'],$available_classId)) continue;
		    if (!$students = $this->getClassStudentIds($class['class_id'])) continue;
		    
		    $summary = $this->getAttendanceSummary($students, $this->month, $this->day);
		
		    $class['status_confirmed'] = sizeof($students) == $summary['records_count'];
		    $classes_summary[] = array_merge($summary[$apm], $class);
		    
		}
	
		return $classes_summary;
	
    }
    public function getClassDayAttendanceRecords($keyword, $sortby, $order){
	
	//$students = $this->getClassStudentIds($keyword);
	
	$attendances = $this->getAttendances($this->students, $this->month, $this->day, $keyword, true, $sortby, $order);
	
	return $attendances;
	
    }
    
    public function getStudentYearAttendanceSummary(){
	
	for ($i = 1; $i <= 12; $i++){
	    
	    $summary = $this->getAttendanceSummary($this->students, $i);
	    $months[$i] = $summary['total'];
	    
	}
	
	return $months;
	
    }
    
    public function getStudentAcademicYearAttendanceSummary(){
		
		$months = array();
		for ($ts_cur_date=$this->academic_year_start,$y=date("Y",$ts_cur_date),$m=date("n",$ts_cur_date);
				$y=date("Y",$ts_cur_date),$m=date("n",$ts_cur_date),$ts_cur_date<$this->academic_year_end;
				$ts_cur_date=strtotime("+1 month",$ts_cur_date) ){
		    
		    $this->year = $y;
			$this->month = $m;
			$this->date = $this->year.'-'.sprintf('%02d',$this->month).'-'.sprintf('%02d',$this->day);
			$this->table_name = self::getAttendanceTableName($this->year, $this->month);
		    
		    $summary = $this->getAttendanceSummary($this->students, $m);
		    $months[$y][$m] = $summary['total'];
		}
		
		return $months;
    }
    
    public function getStudentMonthAttendanceSummary($apm){

	$summary = $this->getAttendanceSummary($this->students, $this->month);
	
	return $summary[$apm];
    }
    public function getStudentMonthAttendances(){
	
	$attendances =  $this->getAttendances($this->students, $this->month);
	
	return $attendances;
	
    }
    public function getStudentDayAttendances($keyword){
	
	$attendances =  $this->getAttendances($this->students, $this->month, $this->day, $keyword);
	
	return $attendances;
	
    }
    public function getAttendanceRecordedYears(){
	$sql = "SELECT DISTINCT Year FROM CARD_STUDENT_RECORD_DATE_STORAGE ORDER BY Year";
	return $this->returnVector($sql);
    }
    
    public function getReminderRecords($params, $sortby='', $order='', $amount='', $page=''){
	
	extract($params);
	
	$sort = $sortby? "$sortby $order,":"";
	$limit = $amount? " LIMIT ".(($page-1)*$amount).", $amount": "";
	
	if ($status==1){
	    $cond .= " r.DateOfReminder < CURDATE() AND";
	}else if ($status==2){
	    $cond .= " r.DateOfReminder >= CURDATE() AND";
	}
	
	$cond .= $reminder_id? " r.ReminderID = $reminder_id AND ":"";
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS
		    r.ReminderID id,
		    r.DateOfReminder as date,
		    IF (r.DateOfReminder < CURDATE(), 1, 0) is_past_record,
		    r.Reason reason,
		    su.UserID student_user_id,
		    su.EnglishName student_user_name_en,
		    su.ChineseName student_user_name_b5,
		    tu.UserID teacher_user_id,
		    tu.EnglishName teacher_user_name_en,
		    tu.ChineseName teacher_user_name_b5
                FROM CARD_STUDENT_REMINDER as r
		INNER JOIN INTRANET_USER su ON su.UserID = r.StudentID
		INNER JOIN INTRANET_USER tu ON tu.UserID = r.TeacherID
                WHERE $cond
		    ( su.EnglishName LIKE '%$keyword%' OR su.ChineseName LIKE '%$keyword%' OR tu.EnglishName LIKE '%$keyword%' OR tu.ChineseName LIKE '%$keyword%'
		    OR su.ClassName = '$keyword' OR r.Reason LIKE '%$keyword%')
                ORDER BY $sort
		    r.DateOfReminder desc
		$limit";
		
	$records = $this->returnArray($sql); echo mysql_error();
	$total   = current($this->returnVector('SELECT FOUND_ROWS();'));
	
	return array($total, $records);

    }
    public function createDailyReminderRecords($student_ids, $teacher_id, $reason, $start_ts, $end_ts, $weekdays){
	
	$this->Start_Trans();
	
	$ts = $start_ts;
	while ($ts <= $end_ts){
	    
	    $date_info = getdate($ts);
	    $date = date('Y-m-d', $ts);
	    $ts += 86400;
	    
	    if ($weekdays[$date_info['wday']]){
		
		foreach ($student_ids as $student_id){
 
		    $sql = "INSERT INTO CARD_STUDENT_REMINDER(
				StudentID, DateOfReminder,TeacherID,Reason,DateInput,DateModified
			    ) VALUES (
				$student_id,'$date',$teacher_id,'$reason',now(),now()
			    )";
 
		    if (!$this->db_db_query($sql)){
			
			$this->RollBack_Trans();
			return false;
		    }
		}
	    }
	}
	
	$this->Commit_Trans();
	return true;
    }
    
    public function getReminderRecord($reminder_id){
	
	list(, $reminders) = $this->getReminderRecords(array('reminder_id'=>$reminder_id));
	
	return current($reminders);
	
    }
    
    public function updateReminderRecord($reminder_id, $teacher_id, $reason, $ts){
	
	$date = date('Y-m-d', $ts);
	
	$sql = "UPDATE CARD_STUDENT_REMINDER SET
		    DateOfReminder = '$date',
		    TeacherID = '$teacher_id',
		    Reason = '$reason',
		    DateModified = now()
		WHERE
		    ReminderID = $reminder_id";
		    
	$this->db_db_query($sql);
	
    }
    
    public function removeReminderRecord($reminder_id){
	
	$sql = "DELETE FROM CARD_STUDENT_REMINDER
		WHERE ReminderID = $reminder_id";
		
	$this->db_db_query($sql);
	
    }
    
    /*
     * @return array $available_classId_ary: class ids that the teacher can take attendance of
     */
    public function getAvailableClassesByDate($target_date, $target_userId='')
    {
    	global $intranet_root;
    	
		include_once($intranet_root."/includes/libcardstudentattend2.php");
		$lc = new libcardstudentattend2();
		
		if($target_userId == ''){
			$target_userId = $_SESSION['UserID'];
		}
		//$target_date = sprintf("%4d-%02d-%02d", $this->year, $this->month, $this->day);
		$available_classes = $lc->getClassListToTakeAttendanceByDate($target_date, $target_userId);
		$available_classId = Get_Array_By_Key($available_classes, 0);
		
		return $available_classId;
    }
    
    public function updateClassConfirmRecord($class_id, $target_year, $target_month, $target_day, $day_type)
    {
    	$year = sprintf("%d",$target_year);
    	$month = sprintf("%02d", $target_month);
    	$day = sprintf("%02d",$target_day);
    	
    	$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$year."_".$month;
    	
    	$sql = "INSERT INTO $card_student_daily_class_confirm
	            (
	                    ClassID,
	                    ConfirmedUserID,
	                    DayNumber,
	                    DayType,
	                    DateInput,
	                    DateModified
	            ) VALUES
	            (
	                    '$class_id',
	                    '".$_SESSION['UserID']."',
	                    '$day',
	                    '$day_type',
	                    NOW(),
	                    NOW()
				)
                ON DUPLICATE KEY UPDATE 
                	ConfirmedUserID='".$_SESSION['UserID']."', DateModified = NOW()";
       $success = $this->db_db_query($sql);
   		return $success;
    }
    
    public function getClassConfirmRecord($class_id, $target_year, $target_month, $target_day, $day_type)
    {
    	global $i_general_sysadmin, $Lang;
    	
    	$year = sprintf("%d",$target_year);
    	$month = sprintf("%02d", $target_month);
    	$day = sprintf("%02d",$target_day);
    	
    	$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$year."_".$month;
		$sql = "SELECT 
		            a.RecordID,
		            IF(a.ConfirmedUserID <> -1, ".getNameFieldWithClassNumberByLang("c.").", CONCAT('$i_general_sysadmin')) as ConfirmedUser,
		            a.DateModified,
					a.ClassID  
		        FROM
		            $card_student_daily_class_confirm as a
		            LEFT OUTER JOIN 
		            YEAR_CLASS as b 
		            ON (a.ClassID=b.YearClassID)
		            LEFT OUTER JOIN 
		            INTRANET_USER as c 
		            ON (a.ConfirmedUserID=c.UserID || a.ConfirmedUserID=-1)
		        WHERE a.ClassID IN (".implode(",",(array)$class_id).") AND a.DayNumber = '".$day."' AND a.DayType = '$day_type'";
		
		$records = $this->returnArray($sql);
		$record_count = count($records);
		$returnAry = array();
		for($i=0;$i<$record_count;$i++){
			$returnAry[$records[$i]['ClassID']] = $records[$i];
		}
		
		return $returnAry;
    }
    
    public function getAttendanceMode()
    {
    	global $intranet_root, $Lang;
    	
		include_once($intranet_root."/includes/libcardstudentattend2.php");
		$lc = new libcardstudentattend2();
		
		return $lc->attendance_mode;
    }
    
    public function getClassGroupSettings()
    {
    	if($this->academic_year_id == '')
    	{
    	 	$this->academic_year_id = Get_Current_Academic_Year_ID();
    	}
    	$sql = "SELECT 
					yc.YearClassID,
					yc.YearID,
					yc.ClassID,
					yc.ClassTitleEN,
					yc.ClassTitleB5,
					ycg.ClassGroupID,
					ycg.AttendanceType,
					ycg.Code,
					ycg.TitleEn as ClassGroupTitleEN,
					ycg.TitleCh as ClassGroupTitleCh 
				FROM YEAR_CLASS as yc 
				LEFT JOIN YEAR_CLASS_GROUP as ycg ON ycg.ClassGroupID=yc.ClassGroupID 
				WHERE yc.AcademicYearID='".$this->academic_year_id."' 
				ORDER BY yc.Sequence";
		$records = $this->returnArray($sql);
		$record_count  = count($records);
		$returnAry = array();
		for($i=0;$i<$record_count;$i++){
			$returnAry[$records[$i]['YearClassID']] = $records[$i];
		}
		
		return $returnAry;
    }
    
}
?>