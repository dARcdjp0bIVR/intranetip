<?php
## Modifing by :

########################################################################################
########################################################################################
############################# Must save this file in UTF-8 #############################
################## function spaceIndentationForPDF has 全型 space "　" ##################
########################################################################################
########################################################################################

########################################
##  2020-06-29 (Philips) [2020-0626-1828-20260]
##  modified GetStudentCrossYearResults(), fix score, standard score, meritForm diff error of CmpSubj
##
##	2020-05-08 (Philips)
##  modified GetStudentCrossYearResults(), move DeletedUserRemark to option form
##
##	2020-05-05 (Philips)
##  modified GetSubjectStats(), for display subject group of main subject ($sys_custom['SDAS']['SubjectStats']['DisplayMainSubjectSG'])
##  modified GetStudentCrossYearResults(), for display component subject result ($sys_custom['SDAS']['StudentPerformanceTracking']['HideEmptyTermColumn'])
##
##	2020-05-04 (Philips) [2019-0708-1528-22289]
##  modified GetStudentCrossYearResults(), add FormPositionPercentileSplineChart
##
##  2020-04-14 (Philips) [2020-0403-1355-01235]
##	modified getAcademicYearNameArrFromIP(), add parameter $order
##
##	2020-01-23 (Philips) [2020-0110-1359-33206]
##	modified GetInternalValueAddedStat(), use YearTermParam as $allScore array key to prevent duplicate YearTermID
##
##	2019-11-28 (Philips) [2019-0710-1725-23085]
##	modified GetStudentCrossYearResults() to allow highchart settings
##
##	2019-11-22 (Philips) [2019-0930-1703-18164]
##	added getSemesterNameArrFromIP2() group year and allow to use $untilNow
##
##  2019-08-30 (Bill)   [2019-0829-1147-43066]
##  modified GetTeacherStats(), roll back to group by subject group name
##
##  2019-07-19 (Bill)   [EJ DM#1260-1261]
##  modified GetSubjectStatsSelectYear_new(), GetSubjectStats(), GetSubjectStatsCompareByTerm(), to add special handling for Class Teacher > view own class & related subject group only
##
##  2019-07-12 Anna
##  added getEncryptedTextWithNoTimeChecking and getDecryptedTextWithNoTimeChecking
##
##  2019-06-25 Philips
##  Modified GetStudentCrossYearPercentileResults() & GetStudentCrossYearResults() & GetSubjectStatsCompareByTerm - Add result filtering for class teacher and subject group teacher
##
##  2019-05-10 Anna
##  Modified GetStudentCrossYearPercentileResults() - change yearTermID to null instead of '0' for overall result
##
##  2019-04-04 Isaac  
##  Added passing file name under GetStudentCrossYearResults() for export from different report
##
##  2019-03-25 (Bill) [2019-0322-1240-43235]
##  - modified GetTeacherStats(), to remove php warning msg - division by zero
##
##  2019-01-28 (Anna)
##  - Modified GetTeacherStats(), added $SubjectGroupResult[SubjectGroupID]]['SubjectID'] array 
##      when get term records, changed get SubjectGroupResult by groupname to groupid 
##
##  2019-01-11 (Anna) [J155618]
##  - Modified getStudentPercentile, added StudentID conditions to get student correspond year form student list
##
##  2018-11-30 (Anna) [P153733]
##  - modified spaceIndentationForPDF, added replace "　　"
##
##  2018-11-16 (Bill)   [2018-1114-1513-29235]
##  - modified GetSubjectStats(), GetSubjectStatsSelectYear_new(), to fix incorrect Pass / Full Mark display    (IPv1.1)
##
##  2018-11-16 (Marco)  
##  - modified GetTeacherStats()
##		for cater subject group name follows class name
##		e.g. the subject group name of 1A Chinese is "1A" while the subject group name of 1A English is "1A"
##		before this fix, only one subject group result could be shown in passing_stats_by_teacher as subject group name was used as the key!!
##	- fixed the bug of school customized report "Internal Value Added Index" of MSSCH
##
##  2018-10-31 (Bill)   [2017-1207-0956-53277]
##  - modified GET_MODULE_OBJ_ARR()
##
##  2018-08-29 (Bill)   [2017-1207-0956-53277]
##  - modified GET_MODULE_OBJ_ARR()
##
##	2018-02-07 (Cameron)
##	- add reading_records in HAS_SCHOOL_RECORD_VIEW_RIGHT() [case #E118376]
##	
##	2018-01-25 (Cameron)
##	- add reading_records in GET_MODULE_OBJ_ARR()  [case #E118376]
##
##	2018-01-17 (Anna)
##	- modified getAssessmentStatReportAccessRight()- added MonthlyReportPIC
##
##	2018-01-12 (Anna)
##	- modified GetStudentCrossYearPercentileResults() - add YearTermID = 0
##
##	2018-01-11 (Anna)
##	- modified GetSubjectStatsCompareByTerm,GetSubjectStats - get academicyear all terms 
##
##	2017-12-12 (Pun)
##	- modified GetSubjectStats(), GetSubjectStatsSelectYear_new(), GetTeacherStats(), added cust T1A3 hardcode full mark
##	2017-11-07 (Bill)	[2017-0403-1552-04240]
##	- added IS_HOY_GROUP_MEMBER_IPO(), IS_HOY_GROUP_MEMBER_ONLY(), to support HOY Group Member Checking	($sys_custom['iPf']['HKUGA_eDis_Tab'])
##	- modified GET_MODULE_OBJ_ARR(), HAS_RIGHT(), always allow HOY Group Member to access Student Info
##	2017-08-03 (Carlos)
##  - modified GET_MODULE_OBJ_ARR() - $sys_custom['LivingHomeopathy'] [School Record] first tag point to [Activity Record], and hide not required menu items.
##	2017-07-13 (Villa)
##	- modified GetClassCrossYearBySubject() - skip showing stardard score when score = -1
##	2017-05-10 (Villa) #E116764
##	- modified GetSubjectStatsCompareByTerm() - uncomment the $sql query avoiding displaying no result all the result for subject group
##  2017-03-07 (Villa) #X114059 
##	- modified GetTeacherStatsCompareByTerm() - fix SubjectID bug after supporting subject CMP in subject panel  
##	2017-03-03 (Villa) #M113990 
##	- modified GetTeacherStats - fix score = -1 count as Allscore and count to the calculation of average score
##	2017-02-27 (Villa)
##	- modified GetSubjectStatsAllYear - fix the function cannot follow the lasted Subject CMP Design. Also Fix cannot display when the client no TermAssessment
##	2017-02-24 (Villa)
##	- modified GetTeacherStatsCompareByTerm - fix bug
##
##	2017-02-21 (Villa)
##	- modified GetClassCrossYearBySubject() - add remarks
##	- modified GetSubjectStatsCompareByTerm() - add remarks
##	- modified GetTeacherStatsCompareByTerm() - add remarks
##	- modified GetSubjectStats() - add remarks
##	- modified GetTeacherStats() - add remarks
##	- modified GetSubjectStatsSelectYear_new() - add remarks
##	- modified GetStudentCrossYearResults() - add remarks
##
##	2017-01-19 (OMas)
##	- modified GetClassCrossYearBySubject() - #H111806
##
##	2017-01-12 (Villa)
##	- modified GetSubjectStatsCompareByTermDetails() - support subject component
##	- modified GetSubjectStatsCompareByTerm() - support subject component
##
##	2017-01-11 (Villa)
##	- modified GetStudentCrossYearResults()
##	- modified GetSubjectStats - only display the term having data
##	- add GetSubjectStatsSelectYear_new for GetSubjectStatsSelectYear for supporting subject component and huge bug fixing(no result if no set Assessment Term, show class missing)
##	
##	2017-01-10 (Villa)
##	- modified GetSubjectStats() - support subject component
##	
##	2016-12-22 (Villa)
##	- change emptysymbol to $Lang['General']['EmptySymbol'] 
##
##	2016-12-13 (Omas)
##	- modified ComputeSDMean() - #W110140
##
##	2016-12-08 (Omas)
##	- Modified GetTeacherStats() - #M110009
##
##	2016-11-18 (Cameron) 
##	- add Teacher_JinYing_Scheme in left side menu under management in  GET_MODULE_OBJ_ARR()
##
##	2016-09-08 (Villa)
##	- remove export buttion function in GetStudentCrossYearResults()
##
##	2016-09-08 (Omas)
##	- fixed session_register_intranet() missing value problem
##
##	2016-08-31 (Pun)
##  - Modified GetSubjectStatsCompareByTerm(), GetTeacherStatsCompareByTerm(), fixed cannot see the subject group result
##
##	2016-08-30 (Pun)
##  - Modified GetSubjectStats(), GetTeacherStats(), GetSubjectStatsCompareByTerm(), GetTeacherStatsCompareByTerm(),
##      fixed cannot display group name if the subject group has no chinese group name
##
##	2016-08-30 (Pun)
##  - Modified GetSubjectStatsCompareByTermDetails(), fixed change tab cannot hide old result tables
##
##	2016-08-15 (Ronald)
##  - Modified GetTeacherStats() - change table display format
##  - Modified GetClassCrossYearBySubject() - change table display format
##  - Added GetSubjectStatsSelectYear()
##
##	2016-06-28 (Omas)[ip.2.5.7.7.1]
##	- Modified ComputeSDMean() - fix $sql_yeartermID add yeartermID is null
##	- Modified GetClassCrossYearBySubject() -  changed to always display new years first - #G98134
##	- modified GetSubjectStatsCompareByTerm() - add no record is found when nothing
##
##  2016-06-06 (Cara)
##	- Change data table's CSS
##
##	2016-05-17 (Pun)[ip.2.5.7.7.1]
##	- Modified GetStudentCrossYearResults(), changed to always display new years first
##
##	2016-05-11 (Pun)[ip.2.5.7.7.1]
##	- Modified ComputeSDMean(), fixed cannot update SD/Mean if YearTerm=''
##
##	2016-05-11 (Omas)[ip.2.5.7.7.1]
##	- Modified getSemesterNameArrFromIP() - added sorting
##
##	2016-04-01 (Siuwan)[ip.2.5.7.4.1]
##	- Modified function GET_MODULE_OBJ_ARR(), replace split() by explode() for PHP5.4
##
##	2016-03-29 (Pun) [94385]
##	- Modified Get_Term_Assessment_Selection(), added order by
##
##	2016-03-29 (Pun) [94385]
##	- Modified GetTeacherStats(), added show number of student if the subject has only grade
##
##	2016-03-21 (Pun)
##	- Modified GetStudentCrossYearResults(), fixed cannot export report no TermAssessment
##
##	2016-03-21 (Pun)
##	- Modified GetStudentCrossYearResults(), fixed cannot show report if some data has NULL in AcademicYearID field.
##
##	2016-03-08 (Pun)
##	- Added GetStudentCrossYearPercentileResults() for Student Performance Tracking (Percentile)
##
##	2016-02-26 (Carlos)
##	- Modified GetTeacherStatsCompareByTerm(), added parameter $prepareChartData for SDAS to generate javascript data for bar chart generation.
##
##	2016-02-25 (Pun)
##	- Modified calculateStudentPercentile(), getStudentPercentile(), GetClassCrossYearBySubject(), fixed percentile rank wrong
##
##	2016-02-18 (Pun)
##	- Modified GetSubjectStats(), GetSubjectStatsAllYear(), added table background color
##	- Modified GetStudentCrossYearResults(), changed export merit "/" to "#"
##
##	2016-02-16 (Pun)
##	- Modified GetTeacherStats(), GetSubjectStats(), added not pass remarks
##	- Modified GetTeacherStats(), GetTeacherStatsCompareByTerm(), GetSubjectStatsAllYear(), change header lang
##	- Modified GetSubjectStatsCompareByTermDetails(), fixed the result did not filter subject component
##	- modify GetStudentCrossYearResults(), fixed IE cannot export result	
##
##	2016-01-28 (Cameron)
##	- modify buildAcademicYearTermAry() and getStudentByAcademicYear()
## 	- add function getStaffInfo()	
##
##	2016-01-26 (Cameron)
##	- add function getStudentByAcademicYear()
##
##	2016-01-26 (Pun)
##	- Modified GetClassCrossYearBySubject(), added display Age, Primary School, Percentile
##	- Modified GetStudentCrossYearResults(), 
##		added export function
##		added floatHeader
##		added Skip Term if no score in that term
##	- Modified GetSubjectStats(), fixed cannot show result if no TermAssessment data in the system
##	- Modified GetTeacherStats(), added $filterSubjectIdArr
##	- Modified GetTeacherStatsCompareByTerm(), added $filterSubjectIdArr
##	- Added getAllPercentileSetting(), getStudentPercentile(), calculateStudentPercentile()
##
##	2016-01-25 (Cameron)
##	- add function getAllAcademicYearTerm() and buildAcademicYearTermAry()
##
##	2016-01-05 (Pun)[ip.2.5.7.1.1] [90975]
##	- Modified GET_MODULE_OBJ_ARR(), hide "Assessment Statistic Report" if $sys_custom['iPf']['Report']['AssessmentStatisticReport']['AcademicProgress'] and $sys_custom['iPf']['Report']['AssessmentStatisticReport']['CCHPWSS'] is not on.
##
##	2015-11-27 (Pun)[ip.2.5.7.1.1]
##	- Modified GET_MODULE_OBJ_ARR(), hide "Assessment Statistic Report" if $plugin['StudentDataAnalysisSystem'] is on
##
##	2015-11-04 (Omas)[ip.2.5.7.1.1]
##	- Added getSemesterArrByAYId(), modified getAcademicYearNameArrFromIP() add parm to return english only
##
##	2015-11-02 (Siuwan)[ip.2.5.7.1.1]
##	- Modified function GET_MODULE_OBJ_ARR(), display left menu in KIS in group setting page
##	2015-09-23 (Pun)
##	- Fixed GetSubjectStatsAllYear() class order, cannot use type filter for whole year
##
##	2015-08-24 (Pun)
##	- Fixed getAssessmentStatReportAccessRight() remove checking the current academic year id 
##
##	2015-08-21 (Pun)
##	- Added GetSubjectStatsAllYear() for subject stats report, search by all year
##
##	2015-08-14 (Pun)
##	- Changed Internal Value Added Index from class to group 
##
##	2015-06-25 (Pun)
##	- Added access right module for assessment stat report
##
##	2015-06-09 (Pun)
##	- Fixed T1 data cannot display in T1 for subject stats report, GetSubjectStats() 
##
##	2015-06-03 (Omas)
##	- added $sys_custom['iPf']['css']['Report']['SLP'] in student iPo MenuArr
##
##	2015-06-02 (Pun)
##	- Added Groupping subject for report,  getReleatedSubjectID(), GetClassCrossYearBySubject() 
##
##	2015-05-22 (Pun)
##	- Change Student Progress compare with TermAssessment vs TermAssessment, Term vs Term
##	- Add flag to hide score difference
##	- Add merit difference
##	- Show grade if no marks for Student Progress
##
##	2015-04-21 (Pun)
##	- Change year order for Student Results
##	- Added popup details page for Teacher Stats Passing, Teacher Stats Improvement, Student Improvement
##
##	2015-04-21 (Pun)
##	- Added red color for marks different for Student Progress
##  - Added NoWrap for table for Student Progress
##	- Fix Standard Score different precision for Student Progress
##	- Added check box filter for Student Progress
##	- Added float header for Student Progress
##	- Show student name, class, class number in one cell for Student Progress
##
##	2015-04-13 (Pun) 2014-0808-1405-07066
##	- Added annual score compare for Teacher Improved
##
##	2015-03-27 (Pun) 2014-0808-1405-07066
##	- Added annual score compare for Subject Improved
##
##	2015-03-27 (Pun) 2014-0808-1405-07066
##	- Added annual score for Class Marksheets, Student Marksheets, Passing Stats by Teacher, Passing Stats by Teacher
##  - Added calculate isAnnual = 1 for ComputeSDMean()
##  - Added calculate isAnnual = 1 pass total for UpdatePassTotal()
##
##	2015-03-18 (Pun) 2014-0808-1405-07066
##	- Added getAllTermIndex() to get T1/T2... for report
##	- Fixed T1,T2 did not show in Class Marksheets
##	- Fixed Subject Statistics wrong pass rate for group
##
##	2015-03-09 (Pun)
##	- Added UpdatePassTotal(), to update the PassTotal in ASSESSMENT_SUBJECT_SD_MEAN
##
##	2015-03-06 (Pun)
##	- Modifed ComputeSDMean(), add pass mark while compute SD
##
##	2015-03-06 (Pun)
##	- Modifed GetSubjectStats(), add pass mark for school input
##
##	2015-03-06 (Pun)
##	- Modifed GetSubjectStatsCompareByTerm(), fix improvement counting wrong for Form and Class
##
##	2015-03-04 (Pun)
##	- Modifed Get_Term_Assessment_Selection(), change to show terms that contains data only
##
##	2015-03-04 (Pun)
##	- Modifed GetSubjectStats(), GetSubjectStatsCompareByTerm(), added report type filter
##
##	2015-01-23 (Bill)
##	- Modifed spaceIndentationForPDF(), fix space indentation problem (change ? to space) [Case #T74470]
##
##	2015-01-15 (Pun)
##	- Added GetInternalValueAddedStat() for MSS customization report
##	
##	2015-01-13 (Bill)
##	- Added showNgWahSASReport(), modified GET_MODULE_OBJ_ARR() - for students to print SAS Report if within SAS report release period
##
##	2015-01-06 (Bill)
##	- Modified GET_MODULE_OBJ_ARR(), Add Student Award Scheme Link
##
##	2014-12-23 (Bill)
##	- Modified isStudentSubmitOLESetting(), added period settings from DB for period checking
##
##  2014-11-18 (Pun)
##  - Modified GET_MODULE_OBJ_ARR(), Add Alumni Report   
##
##  2014-10-08 (Ryan)
##  - Modified GET_MODULE_OBJ_ARR(), Add Holistic Report Link and Custflag for SIS   
##
##	2014-06-30 (Bill)
##	- added returnStudentLicenseSummary() to get the summary of activated license of iPortfolio
##
##	2014-04-29 (Yuen)
##	- modified GetAcademicYears() to get the correct academic year ID(s) without affected by the year order difference used by schools (some are desc while some are asc!!) 
##
##	2014-04-04 (Ivan)
##	- modified getSemesterNameArrFromIP() to fix gb client cannot retrieve term data
##
##	2014-02-28 (Ivan)
##	- added isChildrenBelongsToParent() to check the student is really belongs to the current parent to improve security
##	- added single quote in all SQL statement
##
##	2014-02-19 (Ivan) [2014-0217-1046-45184]
##	- modified GET_STUDENT_CLASS_HISTORY() to retrieve class history data from class history table only
##
##	2013-03-25 (Ivan) [2013-0322-1414-53066]
##	- modified spaceIndentationForPDF() to change the moster code to empty space
##
##	2013-03-22 (Carlos)
##	- Added insertUpdateReleasedStudentDynamicReport(), encodeReleaseStudentDynamicReportSettings(), decodeReleaseStudentDynamicReportSettings()
##		getReleaseStudentDynamicReportSettings(), getReleaseStudentDynamicReportDBTableSql(), releaseDynamicReportToStudent(), 
##		deleteReleasedStudentDynamicReport(), getReleasedStudentDynamicReportGenerator()
##
##	2013-03-15 (YatWoon)
##	- modified getSemesterNameArrFromIP(), add parameter to retreive term name with Lang
##
##	2013-02-22 (YatWoon)
##	- modified isStudentSubmitOLESetting(), add $program_id to check the self submission period
##
##	2013-01-28 (YatWoon)
##	- add returnStudentWithPortfolioAccount()
##
##	2013-01-17 (YatWoon)
##	- Updated GET_STUDENT_CLASS_HISTORY(), use "INSERT IGNORE" instead of "INSERT" since the class history data maybe duplicate [Case#2013-0114-1237-31071]
##
##  2012-11-30 (Bill Mak)
##  - Add stringLanguageSplitByWords(), splits single string into 1 English string and 1 Chinese string
##
##  2012-07-19 (Bill Mak)
##  - Add spaceIndentationForPDF() for showing correct number of spaces in PDF
##
##	2012-07-09 (Henry Chow)
##	- Modified GET_STUDENT_CLASS_HISTORY(), set (UserID, Year, ClassName, ClassNumber) as Primary Key, rather than just (UserID, Year, ClassName) [CRM : 2012-0622-1442-24132]
##
##  2012-06-05 (Bill Mak)
##  - Added displaySelfAccountForHTML(), gives correct spacing in HTML display related to Self Account in iPortfolio
##
##	2012-05-22 (Henry Chow)
##	- Modified GET_STUDENT_CLASS_HISTORY(), also return AcademicYearID
##	2011-12-23 (Ivan)
##	- Modified GET_CLASS_STUDENT_ACTIVATED(), GET_CLASS_LIST_DATA(), GET_STUDENT_LIST_DATA(), fixed "ClassTitleGB" problem in SQL statement
##
##	2011-11-18 (Ivan)
##	- Modified GET_MODULE_OBJ_ARR(), display JUPAS report to teacher
##	
##	2011-11-17 (Ivan) [2011-1117-0925-05071]
##	- Modified GET_STUDENT_OBJECT(), changed GET_OFFICIAL_PHOTO to GET_OFFICIAL_PHOTO_BY_USER_ID so that students without WebSAMSRegNo can also display the student offical photo
##
##	2011-10-14 (Connie)
##	- Modified GET_ALL_TEACHER and GET_TEACHER_SELECTION (add @para: $ParOLE_teacherRight=false)
##
##	2011-08-12 (Ivan)
##	- Modified showJUPASForStudent() to show JUPAS to student only if "OEA" or "Additional Comment" has been enabled
##
##	2011-08-04 (Ivan) [2011-0801-1109-56096]
##	- Modified GET_STUDENT_CLASS_HISTORY() to fix monster code in class history due to the temp table is not utf8 encoding
##
##	2011-07-08 (Ivan)
## 	- Modified GET_MODULE_OBJ_ARR() to fix JUPAS Non-admin and Non-ClassTeacher can access JUPAS problem
##
##	2011-03-02 (Ivan)
##	- Modified GET_MODULE_OBJ_ARR(), display JUPAS Settings for teacher
##
##	2011-02-23 (Ivan)
##	- Modified GET_MODULE_OBJ_ARR(), display JUPAS for teacher
##
##	2011-02-18 (Henry Chow)
##	- Modified GET_MODULE_OBJ_ARR(), display JUPAS
##
##	2010-11-08 Max (201011081157)
##	- Function getClassTeacher_Current is added
##
##	2010-09-29 Max (201009290901)
##	- Added function isUseApprovalInSelfAccount
##
##	2010-09-06 Max (201008301600)
##	- Added customization Teacher Comment for # Chung Wing Kwong SLP Customization

##	2010-04-22 FAI (20100826)
##	- setStudentClassID() added
##	- getStudentClassID() added
##  - GET_CLASS_TEACHER_CLASSID() added
##  - modify isFormTeacher() , support with class whether the teacher is a specific class teacher by (setStudentClassID())

##	2010-04-22 Max (201004211152)
##	- isValidPeriod() added

##	2010-04-19 Max (201004191627)
##	- getDataFromStudentOleConfig() added

##	2010-01-21 Max (201001211402)
##	- Modified function [GET_CLASS_STUDENT()], [GET_STUDENT_LIST_DATA()], [GET_STUDENT_DATA()] to adopt language switching
#########################################
include_once("portfolio25/iPortfolioConfig.inc.php");
include_once("portfolio25/oea/oeaConfig.inc.php");
include_once("portfolio25/lib-portfolio_settings.php");

if (!defined("LIBPORTFOLIO_DEFINED"))         // Preprocessor directives
{
  define("LIBPORTFOLIO_DEFINED",true);
  define("USERID_CLASSNAME_SEPARATOR","###");
  
  class libportfolio extends libdb
  {
	var $StudentClassID = 0;  
	var $thisRegion = ''; //save for the region , HK , Macau ,Malaysia...
  	function libportfolio()
  	{
  		### Key's Settings ###
  		global $ck_course_id, $eclass_prefix;
  		//$this->ipf_db = $eclass_prefix."c".$ck_course_id;
  		### End Key's Settings ###
  		
  		$this->libdb();
  		### Eric's Settings ###
  		global $eclass_filepath, $eclass_httppath, $intranet_db;
  		
		$this->thisRegion = get_client_region();
  		$this->course_id = $ck_course_id;
  		$this->course_db = $eclass_prefix."c".$this->course_id;
  		$this->file_path = $eclass_filepath."/files";
  		$this->key_encrpyt = "protect8key9by9eclass";
      $this->YearDescending = true;
      $this->SemesterAscending = true;
      $this->ShowYearSem = true;
      $this->year_start = array (
                    							"month" => 9,
                    							"day" => 1
                    						);

      $this->semester_patterns = array("/Semester/", "/&#23416;&#26399;/");

  
  		$this->comment_template =	"
  																<HTML>
  																	<HEAD>
  																		<TITLE>eClass iPortfolio</TITLE>
  																		<META http-equiv='Content-Type' content='text/html; charset=UTF-8'>
  																		<link rel=stylesheet href='css/style_new.css'>
  																		<link rel=stylesheet href='css/style_calendar.css0>
  																	</HEAD>
  																	<BODY bgcolor='#FFFFCC'>
  																		<table cellSpacing=0 cellPadding=0 width='95%' border=0>
  																			STUDENT_COMMENT_LIST
  																		</table>
  																	</BODY>
  																</HTML>
  															";
  
  		# index template WITH comment for creating index page for CD burning
  		$this->index_template_full =	"
  																		<HTML>
  																			<HEAD><META http-equiv='Content-Type' content='text/html; charset=UTF-8'>
  																				<TITLE>eClass iPortfolio</TITLE>
  																			</HEAD>
  																			<script language='JavaScript'>
  																				function doOpenCommentPanel()
  																				{
  																					var frameObj = eClassMainFrame;
  																					if (frameObj.cols!='*,0')
  																					{
  																						frameObj.cols = '*,0';
  																					}
  																					else
  																					{
  																						frameObj.cols = '*,318';
  																					}
  																				}
  																			</script>
  																			<frameset rows='25,*' frameborder='NO' border='0' framespacing='0'>
  																				<frame name='eClassTopFrame' src='student_comment_menu.html' noresize scrolling='NO'>
  																				<frameset cols='*,0' name='eClassMainFrame' frameborder='NO' border='0' framespacing='0'>
  																					<frame name='eClassContentFrame' src='STUDENT_URL' noresize scrolling='YES'>
  																					<frame name='eClassCommentFrame' src='STUDENT_COMMENT' noresize scrolling='AUTO'>
  																				</frameset>
  																			</frameset>
  																		</HTML>
  																	";
  
  		# index template WITHOUT comment for creating index page for CD burning
  		$this->index_template =	"
  															<HTML>
  																<HEAD><META http-equiv='Content-Type' content='text/html; charset=UTF-8'>
  																	<TITLE>eClass iPortfolio</TITLE>
  																</HEAD>
  																<frameset rows='25,*' frameborder='NO' border='0' framespacing='0'>
  																	<frame name='eClassTopFrame' src='student_comment_menu.html' noresize scrolling='NO'>
  																	<frame name='eClassContentFrame' src='STUDENT_URL' noresize scrolling='YES'>
  																</frameset>
  															</HTML>
  														";
  
  		# menu template WITH comment for creating index page for CD burning
  		$this->menu_template_full =	"
  																	<HTML>
  																		<HEAD><META http-equiv='Content-Type' content='text/html; charset=UTF-8'>
  																			<TITLE>eClass iPortfolio</TITLE>
  																		</HEAD>
  																		<BODY bgcolor='#99CCFF' TOPMARGIN=1 RIGHTMARGIN=1 LEFTMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
  																			<script language='JavaScript'>
  																				function load_page(wid, uid)
  																				{
  																					var student_url = 'student_u'+uid+'_wp'+wid+'/index_main.html';
  																					var comment_url = 'student_u'+uid+'_wp'+wid+'/student_comment.html';
  																					parent.eClassContentFrame.location = student_url;
  																					parent.eClassCommentFrame.location = comment_url;
  																				}
  																			</script>
  																			<form name='form1' method='post' action='student_comment_menu.html'>
  																				<table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'>
  																					<tr>
  																						<td background='images/SliverBG.gif' align='right' valign='top'>WEB_PORTFOLIO_SELECT<a href='javascript:parent.doOpenCommentPanel()'><img src='images/icon_WriteComment.gif' border='0' hspace='5' vspace='1' title='View Comment'></a></td>
  																					</tr>
  																				</table>
  																			</form>
  																		</BODY>
  																	</HTML>
  																";
  
  		# menu template WITHOUT comment for creating index page for CD burning
  		$this->menu_template =	"
  															<HTML>
  																<HEAD><META http-equiv='Content-Type' content='text/html; charset=UTF-8'>
  																	<TITLE>eClass iPortfolio</TITLE>
  																</HEAD>
  																<BODY bgcolor='#99CCFF' TOPMARGIN=1 RIGHTMARGIN=1 LEFTMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
  																	<script language='JavaScript'>
  																		function load_page(wid, uid)
  																		{
  																			var student_url = 'student_u'+uid+'_wp'+wid+'/index_main.html';
  																			parent.eClassContentFrame.location = student_url;
  																		}
  																	</script>
  																	<form name='form1' method='post' action='student_comment_menu.html'>
  																		<table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'>
  																			<tr>
  																				<td background='images/SliverBG.gif' align='right' valign='top'>WEB_PORTFOLIO_SELECT</td>
  																			</tr>
  																		</table>
  																	</form>
  																</BODY>
  															</HTML>
  														";
  
  		$this->SLP_module_frame =	"
  																<table width='100%' border='2' bordercolor='#000000' cellpadding='0' cellspacing='0' class='table_no_page_break'>
  																	<!--Heading-->
  																	<!--Description-->
  																	<!--Content-->
  																</table>
  															";

			// Eric Yip (20101102): Semester Eng-Chi Map
			$this->semEN2B5map_arr = build_assoc_array($this->returnArray("SELECT DISTINCT YearTermNameEN, YearTermNameB5 FROM {$intranet_db}.ACADEMIC_YEAR_TERM"));
  		//$this->LOAD_ACCESS_RIGHT();
  		### End Eric's Settings ###
  		
  		### Hard code lang variable, will be moved to lang file ###
  		global $iPort, $ec_iPortfolio;
  		$iPort["revise_result"] = "Revise Result";
  		$iPort["copy_of"] = "Copy of ";
  		$iPort["ole_import_guide_int"] = $ec_iPortfolio['olr_reference_guide'];
  		$iPort["ole_import_guide_ext"] = "Please follow these codes to fill in cateory:";
  	}
  
#################################################################
####################  Access Right Functions ####################
#################################################################
  
  	# To be deprecated , while you use this function , please try to use IS_IPF_SUPERADMIN
  	function IS_IPF_ADMIN()
  	{
  		global $ck_function_rights;
  		
  		if(strstr($ck_function_rights, "Profile") || strstr($ck_function_rights, "Sharing") || strstr($ck_function_rights, "Growth")){
    		return true;
		}
  		else{
    		return false;
		}
  	}

	function IS_IPF_SUPERADMIN(){
		global $ck_is_iPortfolioSuperAdmin;

		//variable ck_is_iPortfolioSuperAdmin is registered in /home/web/eclass40/eclass30/src/includes/php/lib-courseinfo.php while click the iPortfolio iCon
		return $ck_is_iPortfolioSuperAdmin;
	}
  	function IS_SBS_ADMIN()
	{
		global $ck_function_rights;
  		
  		if(strstr($ck_function_rights, "Growth")){
    		return true;
		}
  		else{
    		return false;
		}
	}
	/*
	* Check if the user is system admin
	*/
	function IS_ADMIN($ParUserType = "0"){

		if ($ParUserType == "0")
		{
			$ParUserType = $_SESSION['intranet_iportfolio_usertype'];
		}

		$ReturnVal = ($ParUserType=="ADMIN");

		return $ReturnVal;
	}
	
  	function GET_ADMIN_USER()
  	{
  		global $intranet_root, $PATH_WRT_ROOT;
  
  		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
  		
  		$lf = new libfilesystem();
  		$AdminUser = trim($lf->file_read($intranet_root."/file/iportfolio/admin_user.txt"));
  
  		return $AdminUser;
  	}
  	
    function IS_ADMIN_USER($ParUserID)
    {
    	global $intranet_root;
    
    	$AdminUser = $this->GET_ADMIN_USER();
    	$IsAdmin = 0;
    	if(!empty($AdminUser))
    	{
    		$AdminArray = explode(",", $AdminUser);
    		$IsAdmin = (in_array($ParUserID, $AdminArray)) ? 1 : 0;
    	}
    
    	// not checking the right yet
    	return 1;
    	//return $IsAdmin;
    }
  	
  	# Check if admin accesses the page
  	# To do: Use different checking for ipf manage
  	function ADMIN_ACCESS_PAGE() 
  	{
  		if(!$this->IS_IPF_ADMIN())
  			header("Location: /home/portfolio/index.php");
  	}

  	# Control User's Access on Particular Pages
  	function ACCESS_CONTROL($my_operation) 
  	{
  		global $admin_root_path, $ec_iPortfolio_warning;
  
  		if (!$this->HAS_RIGHT($my_operation))
  		{
  			# if not such right, re-direct to iPortfolio index page
  			header("Location: /home/portfolio/index.php");
  			
  			die();
  		}
  	}
  	
  	/*
  	*	Modified by Wah, 2008-07-25
  	* 	Check whether a user can access a portfolio page  	
  	*/
  	function CHECK_ACCESS_IPORTFOLIO($RelatePath="")
  	{
  		global $intranet_root, $ck_room_type, $ck_has_iportfolio, $ck_room_type;
  		
  		
  		if (!isset($ck_has_iportfolio))
  		{		
  			if ($RelatePath == "")
  				header("Location: /home/portfolio/index.php");
  			else
  				header("Location: ".$RelatePath."home/portfolio/index.php");	
  			die();
  		}		
  		else
  		{
  			if (($ck_room_type != "iPORTFOLIO") || (!$ck_has_iportfolio))
  			{
  				
  				if ($RelatePath == "")
  					header("Location: /home/index.php");
  				else				
  					header("Location: ".$RelatePath."home/index.php");	
  					
  					
  				die();
  			}
  			
  		}		
  	}

  	# Check if the User Has Corresponding Right
  	function HAS_RIGHT($my_function) 
  	{
  		global $ck_user_rights_ext, $ck_role_user_id, $ck_function_rights, $ck_user_rights;
  
  		//hdebug_r($ck_function_rights);
  		
  		// format: student_info,merit,assessment_report,activity,award,teacher_comment,attendance,service, ole,print_report,learning_sharing,growth_scheme
  		global $ck_memberType;
  		global $StudentID, $ck_children_intranet_ids, $ck_function_rights, $eclass_db;
  
  		$user_rights = ",".$ck_user_rights_ext.",";
  		$identity_right = ",".$my_function.",";
  		$has_right = strstr($user_rights, $identity_right);
  		
  		//hdebug_r($ck_user_rights_ext);
  		//hdebug_r($identity_right);
  		if($has_right==false)
  		{
  			if($my_function=="growth_scheme")
  			{
  				$has_right = strstr($ck_function_rights, "Growth");
  			}
  			else if($my_function=="learning_sharing")
  			{
  				$has_right = strstr($ck_function_rights, "Sharing");
  			}
  			else if($my_function=="print_report")
  			{
  				$has_right = strstr($ck_function_rights, "Profile:ReportPrint");
  			}
  		}
  
  		# if it in student role mode, check differently
  		if(strpos($_SERVER['PHP_SELF'], "/contents_student/")!=false && $ck_role_user_id!="")
  		{
  			$has_right = strstr($user_rights, $my_function);
  		}
  
  		# ensure parent is viewing his/her children only
  		if ($ck_memberType=="P" && $StudentID!="" && !strstr($ck_children_intranet_ids, $StudentID))
  		{
  			$has_right = false;
  		}
  
  		# check for access right for upper identity (i.e. form-teacher & form/subject-teacher);
  		if(strstr($ck_user_rights, ":manage:"))
  		{
        	$has_right = true;
       	}
  		else if (!$has_right && $ck_memberType=="T")
  		{
  			# form-teacher only
  			$teacher_right = ",".$my_function.":form_t,";
  			if (strstr($user_rights, $teacher_right))
  			{
  				// verify if the teacher is the form-teacher of current class (classname)
  				$is_form_teacher = $this->Is_Form_Teacher();
  				$has_right = $is_form_teacher;
  			}
  
  			# form-teacher / subject-teacher
  			$teacher_right = ",".$my_function.":form_subject_t,";
  			if (strstr($user_rights, $teacher_right))
  			{
  				// verify if the teacher is the form-teacher or subject-teacher of current class (classname)
  				$is_form_teacher = $this->Is_Form_Teacher();
  				$is_subject_teacher = $this->isSubjectTeacher();
  				$has_right = ($is_form_teacher || $is_subject_teacher);
  			}
  			
  			# [2017-0403-1552-04240] HOY Group Member - allow access Student Info
  			global $sys_custom;
  			if($sys_custom['iPf']['HKUGA_eDis_Tab'] && $this->IS_HOY_GROUP_MEMBER_IPO() && $my_function=="student_info")
  			{
  				$has_right = true;
  			}
  		}
  
  		return $has_right;
  	}
  	
  	# Grant right for school record
  	function HAS_SCHOOL_RECORD_VIEW_RIGHT()
  	{
  		if(
  				$this->HAS_RIGHT("merit") ||
  				$this->HAS_RIGHT("assessment_report") ||
  				$this->HAS_RIGHT("activity") ||
  				$this->HAS_RIGHT("award") ||
  				$this->HAS_RIGHT("reading_records") ||
  				$this->HAS_RIGHT("teacher_comment") ||
  				$this->HAS_RIGHT("attendance") ||
  				$this->HAS_RIGHT("service")
  			)
  			return true;
  		else
  			return false;
  	}
  	
  	/*
  	* Check if the user is teacher
  	*/
  	function IS_TEACHER($ParUserType = "0"){
  
  		if ($ParUserType == "0")
  		{
  			$ParUserType = $_SESSION['intranet_iportfolio_usertype'];
  		}

  		$ReturnVal = ($ParUserType=="TEACHER");
  
  		return $ReturnVal;
  
  	}
  	
  	# Modified by Key [2008-08-21]
  	# Check if the user is class teacher
  	function Is_Form_Teacher()
  	{
  		return $this->isFormTeacher();
  	}
  	
  	# Check if the user is the class teacher of current class
  	function isFormTeacher() {
  
  		global $intranet_db, $ck_user_id, $ClassName, $ck_intranet_user_id;

  		if ($this->is_form_teacher==null)
  		{
  			// check from DB

			$_studentClassID = $this->getStudentClassID();
			if($_studentClassID != 0){
				//0 is a default value in libportfolio
				//if $studentClassID != 0, request to check whether this teacher is a specific class classTeacher($studentClassID)
				$row = $this->GET_CLASS_TEACHER_CLASSID();
				$this->is_form_teacher =  in_array($_studentClassID, $row);
			}else
			{
				$row = $this->GET_CLASS_TEACHER_CLASS();
				$this->is_form_teacher = ($ClassName == "") ? sizeof($row)>0 : in_array($ClassName, $row);
			}

  		}

  		return $this->is_form_teacher;
  	}

  	
  	# Check if the user is the subject teacher of current class
  	function isSubjectTeacher() {
  
  		global $intranet_db, $ck_user_id, $ClassName, $ck_intranet_user_id;
  
  		if ($this->is_subject_teacher==null)
  		{
  			// check from DB
  			$row = $this->GET_SUBJECT_TEACHER_CLASS();
  			$this->is_subject_teacher = ($ClassName == "") ? sizeof($row)>0 : in_array($ClassName, $row);
  		}
  
  		return $this->is_subject_teacher;
  	}
  	
  	function IS_CLASS_TEACHER()
  	{
  		return $this->Is_Form_Teacher();
  	}
  	
  	/* Remark (Max): In this function it does not check the academicyearid,
  	 * to find current year class teacher
  	 * Please use getClassTeacher_Current() */
  	function GET_CLASS_TEACHER($ClassName)
  	{
  		global $intranet_db;
  
  		$sql = "SELECT
  					a.UserID
  				FROM
  					{$intranet_db}.YEAR_CLASS_TEACHER as a
  					LEFT JOIN {$intranet_db}.YEAR_CLASS as b ON a.YearClassID = b.YearClassID
  				WHERE
  					b.ClassTitleEN = '$ClassName' OR b.ClassTitleB5 = '$ClassName'
  				";
  		$ReturnArray = $this->returnVector($sql);
  
  		return $ReturnArray;
  	}
  	/**
  	 * @param String $ParClassName - a class name
  	 * @return Array Class teachers
  	 */
  	function getClassTeacher_Current($ParClassName) {
  		global $intranet_db;
  		$sql = "SELECT YCT.USERID
				FROM {$intranet_db}.YEAR_CLASS_TEACHER AS YCT
					INNER JOIN {$intranet_db}.YEAR_CLASS AS YC
					ON YCT.YEARCLASSID = YC.YEARCLASSID
					INNER JOIN ACADEMIC_YEAR AY
					ON YC.ACADEMICYEARID = AY.ACADEMICYEARID
				WHERE
					(YC.CLASSTITLEEN = '$ParClassName' OR YC.CLASSTITLEB5 = '$ParClassName')
					AND AY.ACADEMICYEARID = ".Get_Current_Academic_Year_ID();
			
  		$ReturnArray = $this->returnVector($sql);
  
  		return $ReturnArray;
  	}
  	
	function GET_TEACHER_SELECTION($approved_by="",$TeacherRecordStatus = null)
	{
		$TeacherArray = $this->GET_ALL_TEACHER($TeacherRecordStatus);
		return getSelectByArrayTitle($TeacherArray, "name='request_approved_by'", "", $approved_by, false, 2);
	}
	
	function GET_ALL_TEACHER($TeacherRecordStatus = null)
	{
		global $intranet_db, $eclass_db, $ck_default_lang, $ck_ole_teacher_right;
		$cond2 = "";
		if($ck_ole_teacher_right==2)
		{
			$join_table = " LEFT JOIN {$intranet_db}.YEAR_CLASS_TEACHER as yct ON iu.UserID = yct.UserID";
			$cond = " AND yct.YearClassTeacherID IS NOT NULL";
		}
		if(trim($TeacherRecordStatus) == ""){
			$cond2 = "";
		}else{
			$cond2 = " and iu.RecordStatus = '".$TeacherRecordStatus."' ";
		}
    $username_field = getNameFieldWithClassNumberByLang("iu.");
    $sql = "SELECT DISTINCT
			iu.UserID,
			$username_field
		FROM
			{$intranet_db}.INTRANET_USER as iu
			$join_table
		WHERE
			iu.RecordType = 1 AND
			Teaching = '1'
			$cond2
			$cond

		ORDER BY
			TRIM(iu.EnglishName)
		";
    return $this->returnArray($sql,2);
	}
	
	function IS_HOY_GROUP_MEMBER_IPO()
	{
		return $_SESSION["SSV_PRIVILEGE"]["IN_HOY_GROUP"];
	}
	
	function IS_HOY_GROUP_MEMBER_ONLY()
	{
  		return ($this->IS_HOY_GROUP_MEMBER_IPO() && !($this->IS_IPF_SUPERADMIN() || $this->Is_Form_Teacher() || $this->isSubjectTeacher()));
	}
	
	private function Get_IntranetDB_Table_Name($ParTable) 
	{
		global $intranet_db;
		return $intranet_db.'.'.$ParTable;
	}
	
	private function Get_eClassDB_Table_Name($ParTable) 
	{
		global $eclass_db;
		return $eclass_db.'.'.$ParTable;
	}
	
	function GET_TEACHER_SELECTION_IPF($approved_by="")
	{
		$TeacherArray = $this->GET_ALL_TEACHER_IPF();
		return getSelectByArrayTitle($TeacherArray, "name='request_approved_by'", "", $approved_by, false, 2);
	}
	
	function GET_ALL_TEACHER_IPF()
	{
		global $intranet_db, $eclass_db, $ck_default_lang, $ck_ole_teacher_right;
		$cond2 = "";
		$TeacherRecordStatus=1;
		
		$course = $this->Get_eClassDB_Table_Name('course');
		$user_course = $this->Get_eClassDB_Table_Name('user_course');
		$intraUser = $this->Get_IntranetDB_Table_Name('INTRANET_USER');
		
		$sql_course ='select course_id from '.$course.' where RoomType=4';
		$courseArr = current($this->returnArray($sql_course,2));
		$course_id = $courseArr['course_id'];
	
		$username_field = getNameFieldWithClassNumberByLang("iu.");
		
		$join_eclass = " INNER JOIN {$user_course} as uc ON uc.user_email = iu.UserEmail";
	//	$join_table = " LEFT JOIN {$intranet_db}.YEAR_CLASS_TEACHER as yct ON iu.UserID = yct.UserID";
		
		$cond3 = "AND uc.memberType='T' AND course_id ='{$course_id}'";
		$cond2 = " and iu.RecordStatus = '".$TeacherRecordStatus."' ";

		   
    $sql = "SELECT DISTINCT
			iu.UserID,
			$username_field
		FROM
			{$intraUser} as iu
			$join_eclass
		WHERE
			iu.RecordType = 1 AND
			Teaching = '1'
			$cond2
			$cond
			$cond3

		ORDER BY
			TRIM(iu.EnglishName)
		";

    return $this->returnArray($sql,2);
	}
	
	function GET_TEACHER_NAME($TeacherUserID)
	{
		global $intranet_db;

		$namefield = getNameFieldWithClassNumberByLang();
		$sql = "SELECT $namefield FROM {$intranet_db}.INTRANET_USER WHERE UserID = '$TeacherUserID'";
		$ReturnValue = $this->returnVector($sql);

		return $ReturnValue[0];
	}
  	
########################################################################
####################  Class Level / Class Functions ####################
########################################################################
  	function getClassLevel($ClassName)
  	{
      global $intranet_db;
/* Eric Yip (20100430): CRM Ref. No. 2010-0430-1648
  		if ($this->level_name_by_classname==null)
  		{
  			$this->preloadClassLevelNameByClassName();
  		}
  
  		return $this->level_name_by_classname[$ClassName];
*/
      $sql =  "
                SELECT
                  y.YearID,
                  y.YearName,
                  '{$ClassName}'
                FROM
                  {$intranet_db}.YEAR y
                INNER JOIN
                  {$intranet_db}.YEAR_CLASS yc
                ON
                  y.YearID = yc.YearID
                WHERE
                  yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND
                  (yc.ClassTitleEN = '{$ClassName}' OR yc.ClassTitleB5 = '{$ClassName}')
              ";
      return current($this->returnArray($sql));
  	}
  	
  	# Get User Class Name from Intranet According to user email and UserID
  	function getClassName($UserID){
  		global $eclass_db, $intranet_db;
  
  		$sql = "	SELECT DISTINCT
  							iu.ClassName
  					FROM
  							{$intranet_db}.INTRANET_USER AS iu,
  							{$eclass_db}.user_course AS uc
  					WHERE
  							iu.UserID='$UserID'
  							AND iu.UserEmail=uc.user_email
  				";
  
  		$row = $this->returnArray($sql);
  
  		return $row[0];
  	}
  
  	function preloadClassLevelNameByClassName()
  	{
  		global $intranet_db;
  
  		$sql = "SELECT
  					a.YearID,
  					a.YearName,
  					b.ClassTitle".strtoupper($_SESSION['intranet_session_language'])." AS ClassName
  				FROM
  					{$intranet_db}.YEAR as a
  					LEFT JOIN {$intranet_db}.YEAR_CLASS as b ON a.YearID = b.YearID
 					WHERE
  					b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
  				";
  		$row = $this->returnArray($sql);
  		//echo "SQL_ClassLevel".$sql;
  
  		$Levels = array();
  		for ($i=0; $i<sizeof($row); $i++)
  		{
  			$Levels[$row[$i]['ClassName']] = $row[$i];
  		}
  
  		$this->level_name_by_classname = $Levels;
  	}
  	
  	# Eric Yip (20090819): Get teaching classes by access right
  	function GET_TEACHING_CLASS_BY_RIGHTS($ParAccessType)
  	{
  		global $ck_user_rights_ext;
  	
      if(strstr($ck_user_rights_ext, $ParAccessType.":form_t"))
      {
        $CT_ClassList = $this->GET_CLASS_TEACHER_CLASS();
        $ST_ClassList = array();
      }
      else if(strstr($ck_user_rights_ext, $ParAccessType.":form_subject_t"))
      {
        $CT_ClassList = $this->GET_CLASS_TEACHER_CLASS();
        $ST_ClassList = $this->GET_SUBJECT_TEACHER_CLASS();
      }
      else
      {
        $CT_ClassList = array();
        $ST_ClassList = array();
      }
  		
  		return array($CT_ClassList, $ST_ClassList);
    }

  	# Generate selection list for class levels
  	function GEN_CLASS_LEVEL_SELECTION($ParShowActivatedLevel, $ParClassLevelID="", $ParAllowAll=true)
  	{
  		global $i_general_WholeSchool, $ck_intranet_user_id, $ck_user_rights_ext;
  	
  		# Get classes with activated students
  		$ClassActivatedNo = $this->GET_CLASS_STUDENT_ACTIVATED();
  		if(is_array($ClassActivatedNo))
  		{
  			# Classes are stored in keys
  			$ClassActivated = array_keys($ClassActivatedNo);
  		
    		$isAllClass = strstr($ck_user_rights_ext, "student_info,");
    		if(!$isAllClass)
    		{
    		  # Eric Yip (20090819): Get teaching classes by access right
    		  list($CT_ClassList, $ST_ClassList) = $this->GET_TEACHING_CLASS_BY_RIGHTS("student_info");
    		}
  		
  			$ClassArr = $this->GET_CLASS_LIST_DATA();
  			if(is_array($ClassArr))
  			{
  				for($i=0; $i<count($ClassArr); $i++)
  				{
  					# If class level includes classes with activated students, add to array
  					if(
  							(!$ParShowActivatedLevel || in_array($ClassArr[$i]['ClassName'], $ClassActivated)) &&
  							(!isset($LevelActivated) || !in_array($ClassArr[$i]['YearName'], $LevelActivated)) &&
  							($isAllClass || in_array($ClassArr[$i]['ClassName'], $CT_ClassList) || in_array($ClassArr[$i]['ClassName'], $ST_ClassList))
  						)
  					{
  						$LevelIDActivated[] = $ClassArr[$i]['YearID'];
  						$LevelActivated[] = $ClassArr[$i]['YearName'];
  					}
  				}
  			}
  		}
  		
  		# Reorganize array for generating selection list
  		for($i=0; $i<count($LevelActivated); $i++)
  		{
  			$LevelActivated[$i] = array($LevelIDActivated[$i], $LevelActivated[$i]);
  		}
  		
  		if($ParAllowAll)
  			$ReturnStr = getSelectByArrayTitle($LevelActivated, "name='ClassLevelID' class='formtextbox' onChange='jCHANGE_FIELD(\"classlevel\")'", $i_general_WholeSchool, $ParClassLevelID, false, 2);
  		else
  			$ReturnStr = getSelectByArray($LevelActivated, "name='ClassLevelID' class='formtextbox' onChange='jCHANGE_FIELD(\"classlevel\")'", $ParClassLevelID, 0, 1);
  		
  		return $ReturnStr;
  	}
  	
  	# Get a class list of a class teacher taught (return YearTitle)
  	function GET_CLASS_TEACHER_CLASS() {
  		global $intranet_db, $ck_intranet_user_id, $intranet_session_language;

  		$sql =	"
  							SELECT
  								yc.ClassTitleEN 
  							FROM {$intranet_db}.YEAR_CLASS_TEACHER AS yct
  							INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
                  ON yc.YearClassID = yct.YearClassID
  							WHERE
  								yct.UserID = '".$ck_intranet_user_id."' AND
  								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
  						";
  		$ReturnArr = $this->returnVector($sql);

  		return $ReturnArr;
  	}

	# Get a class list of a class teacher taught (return YearClassID)
  	function GET_CLASS_TEACHER_CLASSID() {
  		global $intranet_db, $ck_intranet_user_id, $intranet_session_language;

  		$sql =	"
  							SELECT
  								yc.YearClassID 
  							FROM {$intranet_db}.YEAR_CLASS_TEACHER AS yct
  							INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
                  ON yc.YearClassID = yct.YearClassID
  							WHERE
  								yct.UserID = '".$ck_intranet_user_id."' AND
  								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
  						";
  		$ReturnArr = $this->returnVector($sql);

  		return $ReturnArr;
  	}
  	
  	# Generate selection list for classes, which have activated students
  	function GEN_CLASS_SELECTION_TEACHER($ParClassName, $ParYearID="", $ParAllowAll=true, $ParShowChoose=true, $ParShowActivatedClass=true, $ParShowAllClass=false)
  	{
  		global $i_alert_pleaseselect, $i_general_WholeSchool;
  		global $ck_user_rights_ext;
  	
  		if($ParShowActivatedClass)
  		{
  			# Get classes with activated students
  			$ClassActivatedNo = $this->GET_CLASS_STUDENT_ACTIVATED();
  			
  			if(is_array($ClassActivatedNo))
  			{
  				# Classes are stored in keys
  				$ClassActivated = array_keys($ClassActivatedNo);
  			
          $isAllClass = ($this->IS_IPF_ADMIN() || strstr($ck_user_rights_ext, "student_info,"));
  				if(!$isAllClass && !$ParShowAllClass)
  				{
  					//$CT_ClassList = $this->GET_CLASS_TEACHER_CLASS();
  					//$ST_ClassList = $this->GET_SUBJECT_TEACHER_CLASS();
            # Eric Yip (20090819): Get teaching classes by access right
            list($CT_ClassList, $ST_ClassList) = $this->GET_TEACHING_CLASS_BY_RIGHTS("student_info");
  				}
  			
  				$ClassArr = $this->GET_CLASS_LIST_DATA();
  				if(is_array($ClassArr))
  				{
  					for($i=0; $i<count($ClassArr); $i++)
  					{
  						# If class includes classes with activated students, add to array
  						if(
  								in_array($ClassArr[$i]['ClassName'], $ClassActivated) &&
  								($ParYearID == "" || $ParYearID == $ClassArr[$i]['YearID']) &&
  								(!isset($ClassNameActivated) || !in_array($ClassArr[$i]['ClassName'], $ClassNameActivated)) &&
  								($isAllClass || $ParShowAllClass || in_array($ClassArr[$i]['ClassName'], $CT_ClassList) || in_array($ClassArr[$i]['ClassName'], $ST_ClassList))
  							)
  						{
  							$ClassNameActivated[] = $ClassArr[$i]['ClassName'];
  						}
  					}
  				}
  			}
  			
  			# Reorganize array for generating selection list
  			for($i=0; $i<count($ClassNameActivated); $i++)
  			{
  				$ClassNameActivated[$i] = array($ClassNameActivated[$i], $ClassNameActivated[$i]);
  			}
  		}
  		else
  		{
  			# Get all classes
  			$ClassArr = $this->GET_CLASS_LIST_DATA();
  			
  			$isAllClass = ($this->IS_IPF_ADMIN() || strstr($ck_user_rights_ext, "student_info,"));
  			if(!$isAllClass)
  			{
  				//$CT_ClassList = $this->GET_CLASS_TEACHER_CLASS();
  				//$ST_ClassList = $this->GET_SUBJECT_TEACHER_CLASS();
  				# Eric Yip (20090819): Get teaching classes by access right
          list($CT_ClassList, $ST_ClassList) = $this->GET_TEACHING_CLASS_BY_RIGHTS("student_info");
  			}
  			
  			for($i=0; $i<count($ClassArr); $i++)
  			{
  				if(
  						(!isset($ClassNameActivated) || !in_array($ClassArr[$i]['ClassName'], $ClassNameActivated)) &&
  						($isAllClass || in_array($ClassArr[$i]['ClassName'], $CT_ClassList) || in_array($ClassArr[$i]['ClassName'], $ST_ClassList)) 
  					)
  					$ClassNameActivated[] = $ClassArr[$i]['ClassName'];
  			}
  			
  			
  			for($i=0; $i<count($ClassNameActivated); $i++)
  			{
  				$ClassNameActivated[$i] = array($ClassNameActivated[$i], $ClassNameActivated[$i]);
  			}
  		}
  
  		
  		if($ParAllowAll)
  			$ReturnStr = getSelectByArrayTitle($ClassNameActivated, "name='ClassName' id='ClassName' class='formtextbox' onChange='jCHANGE_FIELD(\"classname\")'", $i_general_WholeSchool, $ParClassName, false, 2);
  		else if($ParShowChoose)
  			$ReturnStr = getSelectByArrayTitle($ClassNameActivated, "name='ClassName' id='ClassName' class='formtextbox' onChange='jCHANGE_FIELD(\"classname\")'", $i_alert_pleaseselect, $ParClassName, false, 2);
  		else
  			$ReturnStr = getSelectByArray($ClassNameActivated, "name='ClassName' id='ClassName' class='formtextbox' onChange='jCHANGE_FIELD(\"classname\")'", $ParClassName, 0, 1);
  		
  		return $ReturnStr;
  	}
  	
  	# Count the number of student in Class-based
  	function GET_CLASS_STUDENT($ParYearID=""){
  		global $intranet_db, $intranet_session_language;
  
  		# If class level is specified, add condition
  		$extra_cond = ($ParYearID != "")?	"AND yc.YearID = '".$ParYearID."'":"";
  
  		# Get data from db
		$sql =	"
						SELECT DISTINCT
							yc.ClassTitle" . strtoupper($intranet_session_language) . " as ClassName,
							COUNT(iu.UserID) AS TotalStudent
						FROM {$intranet_db}.INTRANET_USER AS iu
						INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
							ON iu.UserID = ycu.UserID
						INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
							ON ycu.YearClassID = yc.YearClassID
						WHERE
							iu.RecordType = '2' AND
							iu.RecordStatus = '1' AND
							yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
							{$extra_cond}
						GROUP BY
							iu.ClassName
					";
  		$row = $this->returnArray($sql);

  		# Organize data in an associative array
  		for ($i=0; $i<sizeof($row); $i++)
  		{
  			$ReturnArr[$row[$i]["ClassName"]] = $row[$i]["TotalStudent"];
  		}
  
  		return $ReturnArr;
  	}
  	
  	# Count the number of student using iPortfolio in Class-based
  	function GET_CLASS_STUDENT_ACTIVATED($ParYearID=""){
  		global $eclass_db, $intranet_db, $ck_current_academic_year_id;
  
  		# If class level is specified, add condition
  		$extra_cond = ($ParYearID != "") ? "AND yc.YearID = '".$ParYearID."'" : "";
  		
  		//$classNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
		
		$AcademicYearID = $ck_current_academic_year_id; 
			
  		# Get data from db
  		$sql =	"
  							SELECT DISTINCT
  								/* yc.ClassTitle".strtoupper($_SESSION['intranet_session_language'])." AS ClassName, <-- this field will die for gb client */
								yc.ClassTitleB5 as ClassName,
  								COUNT(ps.RecordID) AS TotalActivated
  							FROM
  								{$intranet_db}.INTRANET_USER AS iu
  							INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
  								ON ycu.UserID = iu.UserID
  							INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
  								ON ycu.YearClassID = yc.YearClassID
  							LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
                                                                ON ps.UserID = iu.UserID
  							WHERE
  								iu.RecordType = '2' AND
  								iu.RecordStatus = '1' AND
  								ps.WebSAMSRegNo IS NOT NULL AND
  								ps.UserKey IS NOT NULL AND
  								ps.IsSuspend = 0 AND
  								yc.AcademicYearID = '$AcademicYearID'
  								{$extra_cond}
  							GROUP BY
  								iu.ClassName
  						";
  		$row = $this->returnArray($sql);
  
  		# Organize data in an associative array
  		for ($i=0; $i<sizeof($row); $i++)
  		{
  			$ReturnArr[$row[$i]["ClassName"]] = $row[$i]["TotalActivated"];
  		}
  
  		return $ReturnArr;
  	}
  	
  	# Get student list by Class Name
  	function GET_STUDENT_LIST_DATA($ParClassName="", $ParActivated=true, $ParSearchName="", $ParYearID="", $ParField=0, $ParOrder=1, $ParClassNameLang=''){
  		global $intranet_db, $eclass_db, $intranet_session_language;
  
  		$conds[] = "iu.RecordType = '2'";
  		$conds[] = "iu.RecordStatus = '1'";
  
//  		if($ParClassName != "")
//  			$conds[] = "iu.ClassName='$ParClassName'";
		if($ParClassName != "") {
			//$conds[] = "yc.ClassTitle" . $intranet_session_language . "='$ParClassName'";	<- will die for gb client
			
			if ($ParClassNameLang == 'en') {
				$ClassNameField = 'yc.ClassTitleEN';	
			}
			else {
				$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
			}
			$conds[] = $ClassNameField."='$ParClassName'";
		}
		
  		# Limit to display classes taught by the user
  		else if(!$this->IS_IPF_ADMIN())
  		{
  			$conds[] = "iu.ClassName IN ('".implode("','", array_merge($this->GET_CLASS_TEACHER_CLASS(), $this->GET_SUBJECT_TEACHER_CLASS()))."')";
  		}
  
  		if($ParActivated)
  		{
  			$conds[] = "ps.WebSAMSRegNo IS NOT NULL";
  			$conds[] = "ps.UserKey IS NOT NULL";
  			$conds[] = "ps.IsSuspend = 0";
  		}
  		if($ParSearchName != "")
  		{
  			$conds[] =	"
  										(iu.ChineseName LIKE '%".addslashes($ParSearchName)."%' OR
  										iu.EnglishName LIKE '%".addslashes($ParSearchName)."%')
  									";
  
  			if($ParYearID != "")
  			{
  				$conds[] =	"yc.YearID = '".$ParYearID."'";
  			}
  		}
  		$conds = implode(" AND ", $conds);
  
  		# Sorting criteria by parameter
  		switch($ParField)
  		{
  			case 0:
  				$displayBy = "iu.ClassNumber ";
  				break;
  			case 1:
  				if($intranet_session_language == "b5")
  					$displayBy = "ChineseName ";
  				else
  					$displayBy = "EnglishName ";
  				break;
  			case 2:
  				$displayBy = "WebSAMSRegNo ";
  				break;
  			case 3:
  				$displayBy = "ClassName ";
  				break;
  		}
  		if($ParOrder > 0)
  			$displayBy .= "ASC";
  		else
  			$displayBy .= "DESC";
  			
  			// Modified by Key [2008-11-13] , add one order for classname
  			if($ParField == 3)
  			{
  				$displayBy .= " ,iu.ClassNumber ASC ";
  			}
  
  
  		# Get the field name for name and class number
  		$namefield = getNameFieldByLang2("iu.");
  		//$ClassNumberField = getClassNumberField("iu.");
  
  		# Get the data from db
  		$sql = "	SELECT DISTINCT
  								iu.UserID,
  								CONCAT('(', iu.ClassName, ' - ', iu.ClassNumber, ') ', {$namefield}) as DisplayName,
  								ps.WebSAMSRegNo,
  								ps.UserKey,
  								ps.IsSuspend,
  								iu.ChineseName,
  								iu.EnglishName,
  								iu.ClassName
  							FROM
  								{$intranet_db}.INTRANET_USER AS iu
  							INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
  							  ON iu.UserID = ycu.UserID
  							INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
  						    ON ycu.YearClassID = yc.YearClassID
  							LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
    							ON ps.UserID = iu.UserID
  							WHERE
                  yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND
  								$conds
  							ORDER BY
  								$displayBy
  						";
  						//hdebug_pr($sql);
  						//hdebug_pr($this->returnArray($sql));
  		return $this->returnArray($sql);
  	}
  	
  	# Get Student List of a Class
  	function RETURN_STUDENT_LIST_DATA($my_class_name)
  	{
  		global $intranet_db, $eclass_db;
  
  		$namefield = getNameFieldByLang2("iu.");
  		//$ClassNumberField = getClassNumberField("iu.");
  		$sql = "	SELECT DISTINCT
  						iu.UserID,
  						CONCAT('(', iu.ClassName, ' - ', iu.ClassNumber, ') ', {$namefield}) as DisplayName
  					FROM
  						{$intranet_db}.INTRANET_USER AS iu,
  						{$eclass_db}.PORTFOLIO_STUDENT AS ps
  					WHERE
  						iu.ClassName='$my_class_name'
  						AND ps.UserID=iu.UserID
  						AND ps.WebSAMSRegNo IS NOT NULL
  						AND ps.UserKey IS NOT NULL
  						AND iu.RecordType = '2'
  						AND ps.IsSuspend = 0
  					ORDER BY
  						DisplayName
  				";
  
  		return $this->returnArray($sql);
  	}
  	
  	# Get a class list
  	function GET_CLASS_LIST_DATA($ParYearID=""){
  		global $intranet_db;
  
  		# If class level is specified, add condition
  		$conds = ($ParYearID!="") ? " AND yc.YearID = '$ParYearID'" : "";
  		
  		$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
  
  		# Get data from db
  		$sql =	"
  							SELECT DISTINCT
  								/* yc.ClassTitle".strtoupper($_SESSION['intranet_session_language'])." AS ClassName, <-- will die for gb client */
  								/* yc.ClassTitle".strtoupper($_SESSION['intranet_session_language'])." As ClassName_Title, */
								$ClassNameField as ClassName,
								$ClassNameField as ClassName_Title,
  								yc.YearID,
  								y.YearName
  							FROM
  								{$intranet_db}.YEAR_CLASS AS yc
  							LEFT JOIN {$intranet_db}.YEAR AS y
  								ON yc.YearID = y.YearID
  							WHERE
  								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
  								$conds
  							ORDER BY
  								/* y.YearName, yc.ClassTitle".strtoupper($_SESSION['intranet_session_language'])." */
								y.YearName, $ClassNameField
  						";
  
  		return $this->returnArray($sql);
  	}
  	
############################################################
####################  Subject Functions ####################
############################################################
  	
  	# Get a class list of a subject teacher taught
  	function GET_SUBJECT_TEACHER_CLASS($teacher_subject_id="") {
  		global $intranet_db, $ck_intranet_user_id;
  
  		if($teacher_subject_id != "")
  		{
  			$conds = " AND st.SubjectID='{$teacher_subject_id}'";	
  		}
  		
  		$sql =	"
  							SELECT DISTINCT
  								iu.ClassName
  							FROM {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER AS stct
  							INNER JOIN {$intranet_db}.SUBJECT_TERM AS st
                  ON stct.SubjectGroupID = st.SubjectGroupID
                INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt
                  ON st.YearTermID = ayt.YearTermID
                INNER JOIN {$intranet_db}.SUBJECT_TERM_CLASS_USER AS stcu
                  ON st.SubjectGroupID = stcu.SubjectGroupID
                INNER JOIN {$intranet_db}.INTRANET_USER AS iu
                  ON stcu.UserID = iu.UserID
                WHERE
  								stct.UserID = '".$ck_intranet_user_id."' AND
  								ayt.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
  								$conds
  						";
  		$ReturnArr = $this->returnVector($sql);
  
  		return $ReturnArr;
  	}
  	
  	# Get user ID of students who are taught by subject teacher
  	function GET_SUBJECT_TEACHER_STUDENT($teacher_user_id, $teacher_subject_id="", $a_year_id="", $y_term_id="") {
  		global $intranet_db, $ck_intranet_user_id;
  
      $curYearTerm = getCurrentAcademicYearAndYearTerm();
  
  		if($teacher_subject_id != "")
  		{
  			$conds = " AND st.SubjectID='{$teacher_subject_id}'";	
  		}
  		
  		if($a_year_id != "")
  		{
        $conds .= " AND ayt.AcademicYearID = '".$a_year_id."'";
      }
      else
      {
        $conds .= " AND ayt.AcademicYearID = '".$curYearTerm['AcademicYearID']."'";
      }
      
      if($y_term_id != "")
      {
        $conds .= " AND ayt.YearTermID = '".$y_term_id."'";
      }
      else
      {
        $conds .= " AND ayt.YearTermID = '".$curYearTerm['YearTermID']."'";
      }
  		
  		$sql =	"
  							SELECT DISTINCT
  								stcu.UserID
  							FROM {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER AS stct
  							INNER JOIN {$intranet_db}.SUBJECT_TERM AS st
                  ON stct.SubjectGroupID = st.SubjectGroupID
                INNER JOIN {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt
                  ON st.YearTermID = ayt.YearTermID
                INNER JOIN {$intranet_db}.SUBJECT_TERM_CLASS_USER AS stcu
                  ON st.SubjectGroupID = stcu.SubjectGroupID
                WHERE
  								stct.UserID = '".$teacher_user_id."'
  								$conds
  						";
  		$ReturnArr = $this->returnVector($sql);
  
  		return $ReturnArr;
  	}
  	
############################################################
####################  Student Functions ####################
############################################################
  	
  	# Get school house of students by User ID
  	function GET_STUDENT_HOUSE($ParUserID)
  	{
  		global $intranet_db;
  
  		# Capable of multiple students
  		$StudentIDs = (is_array($ParUserID)) ? implode(",", $ParUserID) : $ParUserID;
  
  		# Get data of given users from db
  		$sql =	"
  							SELECT
  								b.UserID, a.Title
  							FROM
  								{$intranet_db}.INTRANET_GROUP as a
  							LEFT JOIN
  								{$intranet_db}.INTRANET_USERGROUP as b
  							ON
  								a.GroupID = b.GroupID
  							WHERE
  								b.UserID IN ($StudentIDs) AND
  								a.RecordType = '4'
  						";
  		$HouseArr = $this->returnArray($sql);
  
  		# Organize data to an associative array
  		for ($i=0; $i<sizeof($HouseArr); $i++)
  		{
  		  list($id,$data) = $HouseArr[$i];
  		  $id = trim($id);
  		  $data = trim($data);
  
  		  $ReturnArr[$id] = $data;
  		}
  
  		return $ReturnArr;
  	}
  	
  	# Get the admission date of a student
  	function GET_STUDENT_ADMISSION_DATE($ParUserID)
  	{
  		global $eclass_db,$intranet_db;
  		
  		$sql =	"
	  				SELECT
	  					AdmissionDate
	  				FROM
	  					{$eclass_db}.PORTFOLIO_STUDENT
	  				WHERE UserID = '".$ParUserID."'
	  			";
	  	$ReturnVal = $this->returnVector($sql);
  		
  		return $ReturnVal[0];
  	}
  	
  	# Get class history of a student (Year, ClassName, ClassNumber)
  	function GET_STUDENT_CLASS_HISTORY($ParUserID, $ParYear="", $ParIncludeAcademicResult=true, $isAlumni=false)
  	{
  		global $eclass_db, $ec_iPortfolio, $intranet_db, $intranet_session_language;
  
  		# Get class number field
  		//$ClassNumberField = getClassNumberField();
  
  		# if particular year is selected, add a condition to query
  		if ($ParYear != "")
  		{
  			$Cond1 = " AND AcademicYear = '".$ParYear."' ";
  			$Cond2 = " AND Year = '".$ParYear."' ";
  		}
  		
      $sql = "CREATE TEMPORARY TABLE IF NOT EXISTS tempClassHistory (";
      $sql .= "Year varchar(255), ClassName varchar(255), ClassNumber int(8), UserID int(8), AcademicYearID int(11), ";
      $sql .= "PRIMARY KEY (UserID, Year, ClassName, ClassNumber)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
      $this->db_db_query($sql);

			//FOR safe , in case it is a loop for student , this temporary table will have previouse student class history, so remove all the record in the table
			$sql = "TRUNCATE TABLE tempClassHistory";
			$this->db_db_query($sql);

			if($isAlumni){
				$sql = "INSERT IGNORE INTO tempClassHistory ";
				$sql .= "SELECT DISTINCT AcademicYear AS Year, ClassName, ClassNumber, UserID, NULL AS AcademicYearID ";
				$sql .= "FROM {$intranet_db}.PROFILE_ARCHIVE_CLASS_HISTORY ";
				$sql .= "WHERE UserID IN ({$ParUserID}) {$Cond1} ";
				$sql .= "ORDER BY AcademicYear";
				$this->db_db_query($sql);
			}else{
				$sql = "INSERT IGNORE INTO tempClassHistory ";
				$sql .= "SELECT DISTINCT AcademicYear AS Year, ClassName, ClassNumber, UserID, AcademicYearID ";
				$sql .= "FROM {$intranet_db}.PROFILE_CLASS_HISTORY ";
				$sql .= "WHERE UserID IN ({$ParUserID}) {$Cond1} ";
				$sql .= "ORDER BY AcademicYear";
				$this->db_db_query($sql);
			}
      
		if ($ParIncludeAcademicResult) {
			$sql = "INSERT IGNORE INTO tempClassHistory ";
			$sql .= "SELECT DISTINCT Year, ClassName, ClassNumber, UserID, AcademicYearID ";
			$sql .= "FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ";
			$sql .= "WHERE UserID IN ({$ParUserID}) AND IFNULL(ClassName, '') <> '' AND IFNULL(ClassNumber, '') <> '' {$Cond2} ";
			$sql .= "ORDER BY Year ";
			$sql .= "ON DUPLICATE KEY UPDATE tempClassHistory.ClassNumber = IFNULL(tempClassHistory.ClassNumber, VALUES(ClassNumber))";
			$this->db_db_query($sql);	
		}


      $sql = "SELECT DISTINCT Year, ClassName, ClassNumber, UserID, AcademicYearID ";
      $sql .= "FROM tempClassHistory ORDER BY UserID, Year";
      $ReturnArr = $this->returnArray($sql);
    
		if(!$isAlumni){
			# Try to include current year data
			$curAYearID = Get_Current_Academic_Year_ID();
		    $curAYear = new academic_year($curAYearID);
	  		if($ParYear == "" || $ParYear == $curAYear->YearNameEN || $ParYear == $curAYear->YearNameB5)
	  		{
	  			$current_year_included = false;
	  			foreach($ReturnArr as $element)
	  			{
	  				if($element['Year'] == $curAYear->YearNameEN || $element['Year'] == $curAYear->YearNameB5)
	  				{
	  					$current_year_included = true;
	  					break;
	  				}
	  			}
	  
	  			if(!$current_year_included)
	  			{
	  				$sql =	"
	  									SELECT
	  										'".$curAYear->{'YearName'.strtoupper($intranet_session_language)}."' as Year,
	  										ClassName,
	  										ClassNumber,
	  										UserID
	  									FROM
	  										{$intranet_db}.INTRANET_USER
	  									WHERE
	  										UserID IN ($ParUserID)
	  								";
	  				$CurrentYrArr = $this->returnArray($sql);
	  				$ReturnArr[] = $CurrentYrArr[0];
	  			}
	  		}
		}
  
  		return $ReturnArr;
  	}
  	
  	# Get parent data by student User ID
  	function GET_PARENT_DATA($ParUserID)
  	{
  		global $eclass_db;
  
  		$sql =	"
  							SELECT
  								a.ChName,
  								a.EnName,
  								IF(CHAR_LENGTH(a.Relation) = 1, CONCAT('0', a.Relation), a.Relation) AS Relation,
  								a.Phone,
  								a.EmPhone,
  								a.IsMain
  							FROM
  								{$eclass_db}.GUARDIAN_STUDENT as a
  							INNER JOIN
  								{$eclass_db}.PORTFOLIO_STUDENT as b
  							ON
  								a.UserID = b.UserID
  							WHERE
  								a.UserID = '$ParUserID'
  							ORDER BY
  								a.IsMain DESC
  						";
  		$ReturnArr = $this->returnArray($sql);
  
  		return $ReturnArr;
  	}
  	
  	# Get complete parent objects
  	function GET_PARENT_OBJECT($ParUserID)
  	{
  		global $ec_guardian;
  
  		$parent_info_arr = $this->GET_PARENT_DATA($ParUserID);
  
  		for($i=0; $i<sizeof($parent_info_arr); $i++)
  		{
  			$parent = $parent_info_arr[$i];
  			$parent["Relation"] = $ec_guardian[$parent["Relation"]];
  			$parent["Address"] = "--";
  
  			$ReturnArr[] = $parent;
  		}
  
  		return $ReturnArr;
  	}
  	
  	# Get student data by User ID
	//$studentInCurrentYear = true , return student must be with academic year = current in t:YEAR_CLASS
	function GET_STUDENT_DATA($ParUserID,$studentInCurrentYear = true){
  		global $intranet_db, $eclass_db, $ck_is_alumni;
  		global $i_gender_male, $i_gender_female;
  		global $intranet_session_language;
  		# Capable of multiple students
  		$StudentIDs = (is_array($ParUserID))? implode(",", $ParUserID) : $ParUserID;
  
  		# Select different table for different user type
  		if($ck_is_alumni==1)
  		{
  			$user_table = "INTRANET_ARCHIVE_USER";
  		}
  		else
  		{
  			$user_table = "INTRANET_USER";

  			$extra_fields = "iu.DateModified, ";
  			$extra_fields2 = ", yc.YearID";
  		}
  
  		# Get class number field
  		//$ClassNumberField = getClassNumberField("iu.");
  
  		# Get data of given users from db
  		$sql =	"
  							SELECT
  								iu.UserID,
  								iu.ChineseName,
  								iu.EnglishName,
  								iu.WebSAMSRegNo,
  								IFNULL(iu.DateOfBirth, '--') AS DateOfBirth,
  								CASE iu.Gender
  									WHEN 'F' THEN '{$i_gender_female}'
  									WHEN 'M' THEN '{$i_gender_male}'
  									ELSE '--'
  								END AS Gender,
  								iu.PhotoLink,
  								$extra_fields
  								iu.Address,
  								IFNULL(ps.PlaceOfBirth, '--') AS PlaceOfBirth,
  								IFNULL(ps.Nationality, '--') AS Nationality,
  								ps.AdmissionDate,
  								IFNULL(iu.HomeTelNo, '--') AS HomeTelNo,
  								iu.ClassNumber,
  								/* yc.ClassTitle" . strtoupper($intranet_session_language) . " as ClassName */
  								yc.ClassTitleEN as ClassName
  								$extra_fields2
  							FROM
  								{$intranet_db}.$user_table AS iu
  							LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
                  ON ps.UserID=iu.UserID
                INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu
                  ON iu.UserID=ycu.UserID
                INNER JOIN {$intranet_db}.YEAR_CLASS AS yc
                  ON ycu.YearClassID=yc.YearClassID
  							WHERE
  								iu.UserID IN ($StudentIDs) AND
  								yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
  							ORDER BY
  								iu.ClassName,
  								iu.ClassNumber
  						";
		//new sql , support with printing report with ALUMNI , without where clase "yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'"
		if(!$studentInCurrentYear){
	  		$sql =	"
  							SELECT
  								iu.UserID,
  								iu.ChineseName,
  								iu.EnglishName,
  								iu.WebSAMSRegNo,
  								IFNULL(iu.DateOfBirth, '--') AS DateOfBirth,
  								CASE iu.Gender
  									WHEN 'F' THEN '{$i_gender_female}'
  									WHEN 'M' THEN '{$i_gender_male}'
  									ELSE '--'
  								END AS Gender,
  								iu.PhotoLink,
  								$extra_fields
  								iu.Address,
  								IFNULL(ps.PlaceOfBirth, '--') AS PlaceOfBirth,
  								IFNULL(ps.Nationality, '--') AS Nationality,
  								ps.AdmissionDate,
  								IFNULL(iu.HomeTelNo, '--') AS HomeTelNo,
  								iu.ClassNumber,

  								iu.ClassName as ClassName

  							FROM
  								{$intranet_db}.$user_table AS iu
  							LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps
                  ON ps.UserID=iu.UserID
  							WHERE
  								iu.UserID IN ($StudentIDs)
  							ORDER BY
  								iu.ClassName,
  								iu.ClassNumber
  						";
		}
  		$ReturArr = $this->returnArray($sql);
  
  		return $ReturArr;
  	}
    	
  	# Get complete student objects
  	function GET_STUDENT_OBJECT($ParUserID){
  		global $intranet_rel_path, $intranet_root, $ec_iPortfolio;
  
  		$ReturnArr = $this->GET_STUDENT_DATA($ParUserID);
  		$student_house = $this->GET_STUDENT_HOUSE($ParUserID);
  
  		for ($i=0; $i<sizeof($ReturnArr); $i++)
  		{
  			# Link of unofficial photo
  			$filepath = $intranet_root.$ReturnArr[$i]["PhotoLink"];
  			$file_url = $intranet_rel_path.$ReturnArr[$i]["PhotoLink"];
  
  			# Link of official photo
  			//$official_photo_arr = $this->GET_OFFICIAL_PHOTO($ReturnArr[$i]["WebSAMSRegNo"]);
  			$official_photo_arr = $this->GET_OFFICIAL_PHOTO_BY_USER_ID($ParUserID);
  
  			list($photo_filepath, $photo_url) = $official_photo_arr;
  
  			# Update link of photo
  			# 1. If official photo exists, use official photo
  			# 2. If unofficial photo exists, use unofficial photo
  			# 3. If no photo exists, dipslay "no photo" message
  			if (is_file($photo_filepath))
  			{
  				$ReturnArr[$i]["PhotoLink"] = "<img src=".$photo_url." border='1' width='100' height='130' <!--ImageStyle-->>";
  				$ReturnArr[$i]["PhotoURL"] = $photo_url;
  			}
  			else if(is_file($filepath))
  			{
  				$ReturnArr[$i]["PhotoLink"] = "<img src=".$file_url." border='1' width='100' height='130' <!--ImageStyle-->>";
  				$ReturnArr[$i]["PhotoURL"] = $file_url;
  			}
  			else
  			{
  				$ReturnArr[$i]["PhotoLink"] = "<i>".$ec_iPortfolio['student_photo_no']."</i>";
  				$ReturnArr[$i]["PhotoURL"] = "";
  			}
  
  			# Discard "#" in WebSAMS Registration Number for display
  			$ReturnArr[$i]["WebSAMSRegNo"] = str_replace("#","",$ReturnArr[$i]["WebSAMSRegNo"]);
  
  			# Discard time part for a datetime variable
  			$tempDOB = explode(" ", $ReturnArr[$i]["DateOfBirth"]);
  			$tempLM = explode(" ", $ReturnArr[$i]["DateModified"]);
  
  			$ReturnArr[$i]["DateOfBirth"] = $tempDOB[0];
  			$ReturnArr[$i]["DateModified"] = $tempLM[0];
  
  			$ReturnArr[$i]["House"] = $student_house[$ReturnArr[$i]["UserID"]];
  		}
  
  		# Return according to number of result records
  		# If there is only one record, return one object
  		# If more than one, return an array of objects
  		if(count($ReturnArr) == 1)
  			return $ReturnArr[0];
  		else
  			return $ReturnArr;
  	}
  	
  	# Get the link of official photo of a student by User ID
  	function GET_OFFICIAL_PHOTO_BY_USER_ID($ParUserID)
  	{
  		global $intranet_rel_path, $intranet_root,$PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libuser.php");

		$objUser = new libuser($ParUserID);
		return $objUser->GET_OFFICIAL_PHOTO_BY_USER_ID($ParUserID);


	/*
  		# Get data of given users from db
  		$sql =	"
  							SELECT
  								WebSAMSRegNo
  							FROM
  								{$intranet_db}.INTRANET_USER
  							WHERE
  								UserID = '".$ParUserID."'
  						";
  
  		$Regno = $this->returnVector($sql);
  
  		return $this->GET_OFFICIAL_PHOTO($Regno[0]);
	*/
  	}

  	# Get the link of official photo of a student by Registion Number
  	function GET_OFFICIAL_PHOTO($ParRegNo)
  	{
  		global $intranet_rel_path, $intranet_root,$PATH_WRT_ROOT;
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$objUser = new libuser($ParUserID);
		$_photoLink = $objUser->GET_OFFICIAL_PHOTO($ParRegNo);
		return $_photoLink;
		/*
  		$tmp_regno = trim(str_replace("#", "", $ParRegNo));
  		$photolink = "/file/official_photo/".$tmp_regno.".jpg";
  		$photo_filepath = $intranet_root.$photolink;
  		$photo_url = $intranet_rel_path.$photolink;

  		return array($photo_filepath, $photo_url);
		*/
  	}
  	
  	/*
  	* Modified by key 2008-09-29: Update the get photo function
  	*/
  	function GET_IPORTFOLIO_PHOTO_BY_USERID($UserID="")
  	{
  		 global $PATH_WRT_ROOT, $intranet_root;
          ### user
          include_once($PATH_WRT_ROOT."includes/libuser.php");
          $luser = new libuser($UserID);
          
          # Set student photo in left menu
  		if(is_object($luser))
  		{
  			$TmpPhotoLink = $this->GET_OFFICIAL_PHOTO_BY_USER_ID($UserID);
  			if($TmpPhotoLink !="")
  			$PhotoLink = str_replace($intranet_root, "", $TmpPhotoLink[0]);
  		}
          
          // get the student photo
          $DefaultPhotoLink = "/images/myaccount_personalinfo/samplephoto.gif";
      		if(!file_exists($TmpPhotoLink[0]))
      		{
          		$PhotoLink = "";
      		}
          if ($PhotoLink == "" && $luser->PhotoLink !="")
  		{
      		if (is_file($intranet_root.$luser->PhotoLink))
      		{
          		$PhotoLink = $luser->PhotoLink;
      		}
  
  		}
  
  		if($PhotoLink == "")
  		$PhotoLink = $DefaultPhotoLink;
  		
  		return $PhotoLink;
  	} // end function
  	
#########################################################
####################  Misc Functions ####################
#########################################################
  	
    /*
    *	Modified by Key 2008-08-14 (Left Menu Settings for student & teacher)
    *  Modified by Key 2008-09-29 (Add Parent Role)
    */
    function GET_MODULE_OBJ_ARR($Role="Student")
    {
    	global $UserID, $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $GroupID;
    	global $intranet_root, $eclass_filepath, $sys_custom, $eclass_httppath;
    	global $ck_function_rights, $ck_user_rights, $ck_user_rights_ext, $ck_memberType, $ck_course_id, $ck_current_children_id, $setting_path_ip_rel, $plugin;

    	### wordings
    	global $iPort, $Lang, $iportfolio_lp_version;
    	global $linterface;
          
    	### user
    	include_once($PATH_WRT_ROOT."includes/libuser.php");
    	$luser = new libuser($UserID);
          
    	if($ck_memberType == "P")
    	{
    		$Role = "Parent";
    		$parent_user_email = $luser->UserEmail;
    		$this->registerEClassChildren($ck_course_id, $parent_user_email);
    	}
          
      	/*
          # Set student photo in left menu
    	if(is_object($luser))
    	{
    		$TmpPhotoLink = $this->GET_OFFICIAL_PHOTO_BY_USER_ID($UserID);
    		if($TmpPhotoLink !="")
    		$PhotoLink = str_replace($intranet_root, "", $TmpPhotoLink[0]);
    	}
          
          // get the student photo
          $DefaultPhotoLink = "/images/myaccount_personalinfo/samplephoto.gif";
          if ($PhotoLink == "" && $luser->PhotoLink !="")
    	{
      		if (is_file($intranet_root.$luser->PhotoLink))
      		{
          		$PhotoLink = $luser->PhotoLink;
      		}
    	}
    
    	if($PhotoLink == "")
    	$PhotoLink = $DefaultPhotoLink;
    	*/
    	
    	$PhotoLink = $this->GET_IPORTFOLIO_PHOTO_BY_USERID($UserID);
    
    	
    	// get the student name
    	$lang = $_SESSION["intranet_session_language"];
    	
    	/*
    	if($lang == "en")
    	{
    		$UserName = $luser->EnglishName;
    	}
    	else
    	{
    		$UserName = $luser->ChineseName;
    	}
    	*/
    	
    	if($lang == "en")
    	{
    		$sName1 = $luser->EnglishName;
    		$sName2 = $luser->ChineseName;
    	}
    	else
    	{
    		$sName1 = $luser->ChineseName;
    		$sName2 = $luser->EnglishName;
    	}
    	
    	if($sName1 != "")
    		$UserName = $sName1;
    	else
    		$UserName = $sName2;
    
    	if($luser->ClassName != "")
    	{
    		if($luser->ClassNumber != "")
    		{
    			$UserName .= " (".$luser->ClassName."-".$luser->ClassNumber.")";
    		}
    		else
    		{
    			$UserName .= " (".$luser->ClassName.")";
    		}
    	}
    
    	$CurrentPageArr['iPortfolio'] = 1;
                  
    	# Current Page Information init
    	// Student Page
    	$Student_MyInformation = 0;
    	$Student_SchoolRecords = 0;
    	$Student_OLE = 0;
    	$Student_SelfAccount = 0;
    	$Student_AuditStudent = 0;
    	$Student_LearningPortfolio = 0;
    	$Student_SchoolBasedScheme = 0;
    	$Student_AddonModule = 0;
    	$Student_BurningCD = 0;
    	$Student_OEA = 0;
      	$Student_Volunteer = 0;
    	$Student_Report = 0;  
    	// Parent Page
    	$Parent_MyInformation = 0;
    	$Parent_SchoolRecords = 0;
    	$Parent_OLE = 0;
    	$Parent_SelfAccount = 0;
    	$Parent_LearningPortfolio = 0;
    	$Parent_SchoolBasedScheme = 0;
    	$Parent_BurningCD = 0;
          
    	// Teacher & Admin Page
    	$Teacher_StudentAccount = 0;
    	$Teacher_OLE = 0;
    	$Teacher_LearningPortfolio = 0;
    	$Teacher_SchoolBasedScheme = 0;
    	$Teacher_SelfAccount = 0;
    	$Teacher_TeacherComment = 0;
    	$Teacher_AuditStudent = 0;
    	$Teacher_StudentReportPrinting = 0;
      $Teacher_dynReport = 0;
      $Teacher_alumniReport = 0;
	  $Teacher_OEA_Report = 0;
    	$Teacher_OLEReport = 0;
    	$Teacher_HolisticReport = 0;
    	$Teacher_AssessmentStatisticReport = 0;
    	$Teacher_AuditStudentReport = 0;
    	$Teacher_UpdateInfo = 0;
    	$Teacher_DataExport = 0;
    	$Teacher_BurningCD = 0;
    	$Teacher_OEA = 0;
    	$Teacher_Volunteer = 0;
    	$Teacher_JinYing_Scheme = 0;
    	$Teacher_AssessmentReport = 0;
    	$Teacher_ThirdParty = 0;
    	$Teacher_PLQuizResult = 0;
    	$Teacher_Transcript_CurriculumsSetting = 0;
    	$Teacher_Transcript_StudentCurriculumsMapping = 0;
    	$Teacher_Transcript_DisableTranscriptPrinting = 0;
    	$Teacher_Transcript_GradingScaleForPreIBSubject = 0;
    	$Teacher_Transcript_ImageManagement = 0;
    	$Teacher_Transcript_PredictedGrade = 0;
    	$Teacher_Transcript_PrintReport = 0;
    	$Teacher_Transcript_DBSTranscript = 0;
    	
      $Settings_OLE = 0;
      $Settings_Reports = 0;
      $Settings_AssessmentStatisticReport = 0;
      $Settings_LearningPortfolio = 0;
      $Settings_SelfAccount = 0;
      $Settings_TeacherComment = 0;
      $Settings_BurningCD = 0;
      $Settings_Grouping = 0;
      $Settings_OEA = 0;
      $Settings_SAS = 0;
      
          
    	$Management = 0;
    	$Reports = 0;
    	$DataHandling = 0;
    	$Settings = 0;
    	$Transcript = 0;
    	
    	switch ($CurrentPage) 
    	{
    		// Student page
    		case "Student_MyInformation":
    			$Student_MyInformation = 1;
    			break;
              
    		case "Student_SchoolRecords":
    			$Student_SchoolRecords = 1;
    			break;
        
    		case "Student_OLE":
    			$Student_OLE = 1;
    			break;
    			
    		case "Student_ExtOLE":
    			$Student_ExtOLE = 1;
    			break;
              
    		case "Student_SelfAccount":
    			$Student_SelfAccount = 1;
    			break;
              
    		case "Student_AuditStudent":
    			$Student_AuditStudent = 1;
    			break;
    			
    		case "Student_LearningPortfolio":
    			$Student_LearningPortfolio = 1;
    			break;
              
    		case "Student_SchoolBasedScheme":
    			$Student_SchoolBasedScheme = 1;
    			break;
              
    		case "Student_AddonModule":
    			$Student_AddonModule = 1;
    			break;
    			
    		case "Student_BurningCD":
    			$Student_BurningCD = 1;
    			break;
    			
    		// parent page
    		case "Parent_MyInformation":
    			$Parent_MyInformation = 1;
    			break;
    		
    		case "Parent_SchoolRecords":
    			$Parent_SchoolRecords = 1;
    			break;
    			
    		case "Parent_OLE":
    			$Parent_OLE = 1;
    			break;
    			
    		case "Parent_ExtOLE":
    			$Parent_ExtOLE = 1;
    			break;
              
    		case "Parent_SelfAccount":
    			$Parent_SelfAccount = 1;
    			break;
              
    		case "Parent_LearningPortfolio":
    			$Parent_LearningPortfolio = 1;
    			break;
              
    		case "Parent_SchoolBasedScheme":
    			$Parent_SchoolBasedScheme = 1;
    			break;
              
    		case "Parent_AddonModule":
    			$Parent_AddonModule = 1;
    			break;
    			
    		case "Parent_BurningCD":
    			$Parent_BurningCD = 1;
    			break;
    
    		// teacher page
    		case "Management":
    			$Management = 1;
    			break;
              
    		case "Teacher_StudentAccount":
    			$Management = 1;
    			$Teacher_StudentAccount = 1;
    			break;
              
    		case "Teacher_BurningCD":
    			$Management = 1;
    //        	$Teacher_BurningCD = 1;
    			$Teacher_StudentAccount = 1;
    			break;
    
    		case "Teacher_UpdateInfo":
    			$Management = 1;
    			$Teacher_UpdateInfo = 1;
    			break;
              
    		case "Teacher_OLE":
    			$Management = 1;
    			$Teacher_OLE = 1;
    			break;
          	
    		case "Teacher_LearningPortfolio":
    			$Management = 1;
    			$Teacher_LearningPortfolio = 1;
    			break;
          	
    		case "Teacher_SchoolBasedScheme":
    			$Management = 1;
    			$Teacher_SchoolBasedScheme = 1;
    			break;
    			
    		case "Teacher_SelfAccount":
    		  $Management = 1;
    		  $Teacher_SelfAccount = 1;
    		  break;
    		case "Teacher_TeacherComment":
    		  $Management = 1;
    		  $Teacher_TeacherComment = 1;
    		  break;
          	
        # For Scared Heart school project customarization
    		case "Teacher_AuditStudent":
    			$Management = 1;
    			$Teacher_AuditStudent = 1;
    			break;
    			
    		case "Teacher_OEA":
    			$Management = 1;
    			$Teacher_OEA = 1;
    			break;
    			
    		case "Teacher_Volunteer":
    			$Management = 1;
    			$Teacher_Volunteer = 1;
    			break;

    		case "Teacher_JinYing_Scheme":
    			$Management = 1;
    			$Teacher_JinYing_Scheme = 1;
          		break;
            
      		# DBS Transcript customization
    		case "Transcript":
    		    $Transcript = 1;
    		    break;
    		    
    		case "Teacher_Transcript_CurriculumsSetting":
    		    $Transcript = 1;
    		    $Teacher_Transcript_CurriculumsSetting = 1;
    		    break;
    		    
    		case "Teacher_Transcript_StudentCurriculumsMapping":
    		    $Transcript = 1;
    		    $Teacher_Transcript_StudentCurriculumsMapping = 1;
    		    break;
    		    
    		case "Teacher_Transcript_DisableTranscriptPrinting":
    		    $Transcript = 1;
    		    $Teacher_Transcript_DisableTranscriptPrinting = 1;
    		    break;
    		    
    		case "Teacher_Transcript_GradingScaleForPreIBSubject":
    		    $Transcript = 1;
    		    $Teacher_Transcript_GradingScaleForPreIBSubject = 1;
    		    break;
    		    
    		case "Teacher_Transcript_ImageManagement":
    		    $Transcript = 1;
    		    $Teacher_Transcript_ImageManagement = 1;
    		    break;
    		    
    		case "Teacher_Transcript_PredictedGrade":
    		    $Transcript = 1;
    		    $Teacher_Transcript_PredictedGrade = 1;
    		    break;
    		    
    		case "Teacher_Transcript_PrintReport":
    		    $Transcript = 1;
    		    $Teacher_Transcript_PrintReport = 1;
    		    break;
    		    
    		case "Teacher_Transcript_DBSTranscript":
    		    $Transcript = 1;
    		    $Teacher_Transcript_DBSTranscript= 1;
    		    break;
          		
    		case "Reports":
    			$Reports = 1;
    			break;
          	
    		case "Teacher_StudentReportPrinting":
    			$Reports = 1;
    			$Teacher_StudentReportPrinting = 1;
    			break;
    		
        case "Teacher_dynReport":
				  $Reports = 1;
    			$Teacher_dynReport = 1;
				  break;
        case "Teacher_alumniReport":
				  $Reports = 1;
    			$Teacher_alumniReport = 1;
				  break;
		case "Teacher_OEA_Report":
				  $Reports = 1;
				  $Teacher_OEA_Report = 1;
				  break;
    		case "Teacher_OLEView":
    			$Reports = 1;
    			$Teacher_OLEView = 1;
    			break;
				
    		case "Teacher_OLEReport":
    			$Reports = 1;
    			$Teacher_OLEReport = 1;
    			break;
          	
    		case "Teacher_HolisticReport":
    			$Reports = 1;
    			$Teacher_HolisticReport = 1;
    			break;
          	
    		case "Teacher_AssessmentStatisticReport":
    			$Reports = 1;
    			$Teacher_AssessmentStatisticReport = 1;
    			break;
          	
    		case "Teacher_AuditStudentReport":
    			$Reports = 1;
    			$Teacher_AuditStudentReport = 1;
    			break;
    			
    		case "Teacher_JupasReport":
    			$Reports = 1;
    			$Teacher_JupasReport = 1;
    			break;
          	
    		case "DataHandling":
    			$DataHandling = 1;
    			break;
          	
    		case "Teacher_DataExport":
    			$DataHandling = 1;
    			$Teacher_DataExport = 1;
    			break;
          	            
    		case "Settings":
    			$Settings = 1;
    			break;
              
    		case "Settings_OLE":
    			$Settings = 1;
    			$Settings_OLE = 1;
    			break;
    		
    		case "Settings_Reports":
    			$Settings = 1;
    			$Settings_Reports = 1;
    			break;
    			break;
    		
    		case "Settings_AssessmentStatisticReport":
    			$Settings = 1;
    			$Settings_AssessmentStatisticReport = 1;
    			break;
          	
    		case "Settings_LearningPortfolio":
    			$Settings = 1;
    			$Settings_LearningPortfolio = 1;
    			break;
    			
    		case "Settings_SelfAccount":
    			$Settings = 1;
    			$Settings_SelfAccount = 1;
    			break;
    			
    		case "Settings_TeacherComment":
    			$Settings = 1;
    			$Settings_TeacherComment = 1;
    			break;
    			
    		case "Settings_BurningCD":
    			$Settings = 1;
    			$Settings_BurningCD = 1;
    			break;
    		
    		case "Settings_Grouping":
    			$Settings = 1;
    			$Settings_Grouping = 1;
    			break;
    		case "Settings_OEA":
    			$Settings = 1;
    			$Settings_OEA = 1;
    			break;
    		case "Settings_SAS":
    			$Settings = 1;
    			$Settings_SAS = 1;
    			break;
    			
    		case "Student_OEA":
    			$Student_OEA = 1;
    			break;
    		case "Student_Report":
				$Student_Report = 1;
				break;
    		case "Student_Volunteer":
    			$Student_Volunteer = 1;
    			break;
    		case "Teacher_AssessmentReport":
    			$Teacher_AssessmentReport = 1;
    			break;
    		case "Teacher_ThirdParty":
    			$Teacher_ThirdParty = 1;
    			break;
    		case "Teacher_PLQuizResult":
    			$Teacher_PLQuizResult = 1;
    			break;
    		default:
    			break;
    	}
    	
    	//Get CDburning right for student and parent
    	
		$prepare_CDburning_config = "$eclass_filepath/files/prepare_CDburning_config.txt";
		$filecontent = trim(get_file_content($prepare_CDburning_config));
		
		if(empty($filecontent)){
			$CDparent_right = $CDstudent_right = 0;	
		}
		else{
			list($CDstudent_right, $CDparent_right) = unserialize($filecontent);
			list(,$CDstudent_right) = explode(":", $CDstudent_right); 
			list(,$CDparent_right) = explode(":", $CDparent_right); 
		}
    		
    	// Check JUPAS display right for Teacher
    	$showJupasForTeacher = (($this->thisRegion == 'zh_HK') && ($this->IS_IPF_SUPERADMIN() || $this->showJUPASForTeacher($UserID) || strstr($ck_function_rights, "Jupas:All")))? true : false;
		// sis hide jupas
		if($sys_custom['ipf']['HideipfMenu']['JUPAS']){
			$showJupasForTeacher = false;
		}
    	///////////////////////////////////////////////////////////////
    	# Menu information
    	if($Role == "Teacher")
    	{
    		// check if teacher account, set below menu
    		if($this->HAS_RIGHT("student_info") ||
             (strstr($ck_function_rights, "Profile:ImportData") || strstr($ck_function_rights, "OLR")) || $this->HAS_RIGHT("ole") ||
             (strstr($ck_user_rights, ":web:") && strstr($ck_function_rights, "Sharing")) ||
             (strstr($ck_user_rights, ":growth:") && $this->HAS_RIGHT("growth_scheme")) ||
             (strstr($ck_user_rights, ":manage:") && $sys_custom['audit_student_needs'])
          	)
        {
      		$MenuArr["Management"] = array($iPort['menu']['management'], "#", $Management);
      		if(
      		    $this->HAS_RIGHT("student_info") && 
      		    (
      		        $this->IS_IPF_SUPERADMIN() ||
      		        !$sys_custom['lp_stem_learning_scheme']
      		    )
  		    )
      		$MenuArr["Management"]['Child']["Teacher_StudentAccount"] = array($iPort['menu']['student_account'], $PATH_WRT_ROOT."home/portfolio/school_records.php", $Teacher_StudentAccount);
      		
//      	if($this->IS_IPF_ADMIN())
//      		$MenuArr["Management"]['Child']["Teacher_UpdateInfo"] = array($iPort['menu']['update_info_eclass_websams'], $PATH_WRT_ROOT."home/portfolio/teacher/data_handling/update_info.php", $Teacher_UpdateInfo);
//      	$MenuArr["Management"]['Child']["Teacher_UpdateInfo"] = array($iPort['menu']['school_records'], $PATH_WRT_ROOT."home/portfolio/teacher/data_handling/update_info.php", $Teacher_UpdateInfo);
//      	if($this->HAS_RIGHT("ole") || strstr($ck_function_rights, "OLR"))
//      	if(strstr($ck_function_rights, "OLR"))
      		if(($this->IS_IPF_SUPERADMIN() || !$sys_custom['lp_stem_learning_scheme']) && !isset($_SESSION["ncs_role"]) && !$this->IS_HOY_GROUP_MEMBER_ONLY())
      			$MenuArr["Management"]['Child']["Teacher_OLE"] = array($iPort['menu']['slp'], $PATH_WRT_ROOT."home/portfolio/teacher/management/oleManagement.php", $Teacher_OLE);
//			else if(strstr($ck_function_rights, "Profile:ImportData"))
//				$MenuArr["Management"]['Child']["Teacher_OLE"] = array($iPort['menu']['slp'], $PATH_WRT_ROOT."home/portfolio/teacher/data_handling/update_info.php", $Teacher_OLE);
//      	
//      	if($this->HAS_RIGHT("learning_sharing"))
//      		debug($ck_function_rights);
      		
      		if($sys_custom['iPortfolio_assessment_report'] || (isset($_SESSION['ncs_role'])&&($_SESSION['ncs_role'] != ""))){
      			  $MenuArr["Management"]['Child']["Assessment_Report"] = array($iPort['menu']['assessment_report'], $PATH_WRT_ROOT."home/portfolio/teacher/management/assessment_report/assessment_report_list.php", $Teacher_AssessmentReport);
      		}
      		
      		if(isset($_SESSION['ncs_role'])&&($_SESSION['ncs_role'] != "")){
      			$MenuArr["Management"]['Child']["third_party_result"] = array($iPort['menu']['third_party'], $PATH_WRT_ROOT."home/portfolio/teacher/management/third_party/index.php", $Teacher_ThirdParty);
      		}
      		
      		if(strstr($ck_user_rights, ":web:") && strstr($ck_function_rights, "Sharing") && !$sys_custom['LivingHomeopathy'])
      		{
      			//if(strstr($ck_user_rights, ":web:") && strstr($ck_user_rights_ext, "learning_sharing"))
      			if(strstr($ck_function_rights, "Sharing:Content"))
      			{
					$learning_portfolio_url = $iportfolio_lp_version==2? "home/portfolio/learning_portfolio_v2/" : "home/portfolio/contents_admin/index_scheme.php";
      				$MenuArr["Management"]['Child']["Teacher_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT.$learning_portfolio_url, $Teacher_LearningPortfolio);
      			}
      			else if(strstr($ck_function_rights, "Sharing:Template"))
      			{
					$MenuArr["Management"]['Child']["Teacher_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT."home/portfolio/learning_portfolio/contents_admin/templates_manage.php", $Teacher_LearningPortfolio);
				}
      		}
      		
      		//if(strstr($ck_user_rights, ":growth:") && strstr($ck_user_rights_ext, "growth_scheme"))
      		if(strstr($ck_user_rights, ":growth:") && $this->HAS_RIGHT("growth_scheme") && !$sys_custom['LivingHomeopathy'])
      			$MenuArr["Management"]['Child']["Teacher_SchoolBasedScheme"] = array($iPort['menu']['school_based_scheme'], $PATH_WRT_ROOT."home/portfolio/teacher/management/sbs/index.php", $Teacher_SchoolBasedScheme);
      		
      		if(strstr($ck_function_rights, "Profile:SelfAccount")&&!$sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile']&&!$sys_custom['ipf']['HideipfMenu']['SelfAccountManagement'] && !$sys_custom['LivingHomeopathy'])
      		  $MenuArr["Management"]['Child']["Teacher_SelfAccount"] = array($iPort['menu']['self_account'], $PATH_WRT_ROOT."home/portfolio/teacher/management/sa/index.php", $Teacher_SelfAccount);
      		if($sys_custom['cwk_SLP'] && strstr($ck_function_rights, "Profile:TeacherComment"))
      		  $MenuArr["Management"]['Child']["Teacher_TeacherComment"] = array($Lang['iPortfolio']['TeacherComment'], $PATH_WRT_ROOT."home/portfolio/teacher/management/tc/index.php", $Teacher_TeacherComment);
      		if($sys_custom['audit_student_needs'] && strstr($ck_user_rights, ":manage:"))
      			$MenuArr["Management"]['Child']["Teacher_AuditStudent"] = array($iPort['menu']['audit_student'], "javascript:newWindow('http://".$eclass_httppath."src/ip2portfolio/audit_student_needs/manage/index.php',95)", $Teacher_AuditStudent);
      		
			//$showJupasForTeacher = $this->showJUPASForTeacher($UserID);
//      		if ($sys_custom['iPf']['JUPAS'] && ($this->IS_IPF_SUPERADMIN() || $_SESSION["USER_BASIC_INFO"]["is_class_teacher"] || strstr($ck_function_rights, "Jupas:All"))) {  // can remove after 20110809
			//if (($this->thisRegion == 'zh_HK') && ($this->IS_IPF_SUPERADMIN() || $showJupasForTeacher || strstr($ck_function_rights, "Jupas:All"))) {
			if ($showJupasForTeacher&&!$sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile'] && !$sys_custom['LivingHomeopathy']) {
      			$MenuArr["Management"]['Child']["Teacher_OEA"] = array($Lang['iPortfolio']['OEA']['JUPAS'], $PATH_WRT_ROOT."home/portfolio/oea/teacher/?clearCoo=1", $Teacher_OEA);
      		}
      		
      		if ($sys_custom['iPf']['Volunteer'] && !$sys_custom['LivingHomeopathy']) {
      			$MenuArr["Management"]['Child']["Teacher_Volunteer"] = array($Lang['iPortfolio']['VolunteerArr']['Volunteer'], $PATH_WRT_ROOT."home/portfolio/volunteer/teacher/?clearCoo=1", $Teacher_Volunteer);
      		}
      		
    		if ($sys_custom['iPf']['JinYingScheme'] && ($this->IS_IPF_SUPERADMIN() || $_SESSION['SSV_USER_ACCESS']['other-iPortfolio']) && !$sys_custom['LivingHomeopathy']) {
    			$MenuArr["Management"]['Child']["JinYingScheme"] = array($Lang['iPortfolio']['JinYing']['JinYingScheme'], $PATH_WRT_ROOT."home/portfolio/teacher/management/jinying/?clearCoo=1", $Teacher_JinYing_Scheme);
    		}
      		
      	}
      	
      	if($sys_custom['iPf']['DBSTranscript']) {
      	    if($this->IS_IPF_SUPERADMIN()) {
          	    $MenuArr["Transcript"] = array($Lang['iPortfolio']['DBSTranscript']['Title'], "#", $Transcript);
          	    $MenuArr["Transcript"]['Child']["CurriculumsSetting"] = array($Lang['iPortfolio']['DBSTranscript']['CurriculumsSetting']['Title'], $PATH_WRT_ROOT."home/portfolio/dbs_transcript/curriculums_setting/index.php", $Teacher_Transcript_CurriculumsSetting);
          	    $MenuArr["Transcript"]['Child']["StudentCurriculumsMapping"] = array($Lang['iPortfolio']['DBSTranscript']['StudentCurriculumsMapping']['Title'], $PATH_WRT_ROOT."home/portfolio/dbs_transcript/student_curriculums/index.php", $Teacher_Transcript_StudentCurriculumsMapping);
          	    $MenuArr["Transcript"]['Child']["DisableTranscriptPrinting"] = array($Lang['iPortfolio']['DBSTranscript']['DisableTranscriptPrinting']['Title'], $PATH_WRT_ROOT."home/portfolio/dbs_transcript/disable_printing/index.php", $Teacher_Transcript_DisableTranscriptPrinting);
          	    $MenuArr["Transcript"]['Child']["GradingScaleForPreIBSubject"] = array($Lang['iPortfolio']['DBSTranscript']['GradingScaleForPreIBSubject']['Title'], $PATH_WRT_ROOT."home/portfolio/dbs_transcript/grading_scale/index.php", $Teacher_Transcript_GradingScaleForPreIBSubject);
          	    $MenuArr["Transcript"]['Child']["ImageManagement"] = array($Lang['iPortfolio']['DBSTranscript']['ImageManagement']['Title'], $PATH_WRT_ROOT."home/portfolio/dbs_transcript/image_mgmt/back_cover.php", $Teacher_Transcript_ImageManagement);
          	    $MenuArr["Transcript"]['Child']["PredictedGrade"] = array($Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Title'], $PATH_WRT_ROOT."home/portfolio/dbs_transcript/predicted_grade/index.php", $Teacher_Transcript_PredictedGrade);
          	    $MenuArr["Transcript"]['Child']["PrintReport"] = array($Lang['iPortfolio']['DBSTranscript']['PrintReport']['Title'], $PATH_WRT_ROOT."home/portfolio/dbs_transcript/print_report/index.php", $Teacher_Transcript_PrintReport);
      	    } else if($this->isSubjectTeacher() || $this->IS_CLASS_TEACHER()) {
      	        $MenuArr["Transcript"] = array($Lang['iPortfolio']['DBSTranscript']['Title'], "#", $Transcript);
      	        $MenuArr["Transcript"]['Child']["PredictedGrade"] = array($Lang['iPortfolio']['DBSTranscript']['PredictedGradeMgmt']['Title'], $PATH_WRT_ROOT."home/portfolio/dbs_transcript/predicted_grade/index.php", $Teacher_Transcript_PredictedGrade);
      	    }
      	}
    	
      	// handle SIS Report Tab 20141008 
        if(($this->HAS_RIGHT("print_report") || $this->HAS_RIGHT("ole") || $sys_custom['audit_student_needs'] || strstr($ck_function_rights, "Profile:Stat")) && !$sys_custom['LivingHomeopathy'])
        {
    		  $MenuArr["Reports"] = array($iPort['menu']['reports'], "#", $Reports);
          if($this->HAS_RIGHT("print_report"))
          {
          	if(!$sys_custom['ipf']['HideipfMenu']['StudentReport']){
            	$MenuArr["Reports"]['Child']["Teacher_StudentReportPrinting"] = array($iPort['menu']['student_report_printing'], $PATH_WRT_ROOT."home/portfolio/profile/report/report_printing_menu.php", $Teacher_StudentReportPrinting);
          	}
          	if(!$sys_custom['ipf']['HideipfMenu']['DynamicReport']){
            	$MenuArr["Reports"]['Child']["Teacher_dynReport"] = array($iPort['menu']['dynReport'], $PATH_WRT_ROOT."home/portfolio/report/dynReport/index.php", $Teacher_dynReport);
          	}
          	if(strstr($ck_function_rights, "Alumni") && $sys_custom['iPortfolio_alumni_report']) {
	        	$MenuArr["Reports"]['Child']["Teacher_alumniReport"] = array($iPort['menu']['alumniReport'], $PATH_WRT_ROOT."home/portfolio/report/alumniReport/school_records_alumni.php", $Teacher_alumniReport);
          	}
          }


		if ($showJupasForTeacher&&!$sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile'] && !$sys_custom['LivingHomeopathy']) {
      			$MenuArr["Reports"]['Child']["Teacher_JupasReport"] = array($Lang['iPortfolio']['OEA']['JUPAS'], $PATH_WRT_ROOT."home/portfolio/oea/teacher/?task=report_studentOea", $Teacher_JupasReport);
      	}
 
          if($this->HAS_RIGHT("ole") && !$sys_custom['LivingHomeopathy'])
          {
          	if(!$sys_custom['ipf']['HideipfMenu']['OLEReport']){
	          	$MenuArr["Reports"]['Child']["Teacher_oleView"] = array($iPort['menu']['slp'], $PATH_WRT_ROOT."home/portfolio/teacher/management/oleView.php", $Teacher_OLEView);
            	$MenuArr["Reports"]['Child']["Teacher_OLEReport"] = array($iPort['menu']['ole_report'], $PATH_WRT_ROOT."home/portfolio/profile/ole/index_stat.php", $Teacher_OLEReport);
          	}
          }
          
          if(isset($_SESSION['ncs_role'])&&($_SESSION['ncs_role'] != "") && !$sys_custom['LivingHomeopathy']){
          	$MenuArr["Reports"]['Child']["pl2_quiz_report"] = array($iPort['menu']['pl2_quiz_report'], $PATH_WRT_ROOT."home/portfolio/profile/pl2_quiz_result/index.php", $Teacher_PLQuizResult);
          }
			  
		  ## Assessment Statistic Report START ##
		  if(
		  		!$plugin['StudentDataAnalysisSystem'] &&
          		(
	          		$sys_custom['iPf']['Report']['AssessmentStatisticReport']['AcademicProgress'] ||
	          		$sys_custom['iPf']['Report']['AssessmentStatisticReport']['CCHPWSS']
          		)
		  ){
			  $accessRight = $this->getAssessmentStatReportAccessRight();
		  	  $path = '';
			  if($accessRight['admin']){
			  	$path = 'home/portfolio/profile/management/score_list.php';
			  }else if($accessRight['classTeacher']){
			  	$path = 'home/portfolio/profile/management/score_list.php';
			  }else if($accessRight['subjectPanel']){
			  	$path = 'home/portfolio/profile/management/subject_stats.php';
			  }else if($accessRight['subjectTeacher']){
			  	$path = 'home/portfolio/profile/management/teacher_stats_passing.php';
			  }
	          if($path != '')
	          { 
	            $MenuArr["Reports"]['Child']["Teacher_AssessmentStatisticReport"] = array($iPort['menu']['assessment_statistic_report'], $PATH_WRT_ROOT.$path, $Teacher_AssessmentStatisticReport);
	          }
		  }
		  ## Assessment Statistic Report END ##
          
      		if($sys_custom['audit_student_needs'])
          {
            $MenuArr["Reports"]['Child']["Teacher_AuditStudentReport"] = array($iPort['menu']['audit_student_report'], "javascript:newWindow('http://".$eclass_httppath."src/ip2portfolio/audit_student_needs/teacher/index.php',95)", $Teacher_AuditStudentReport);
          }
          
          	if ($showJupasForTeacher) {
      			//$MenuArr["Reports"]['Child']["Teacher_JupasReport"] = array($Lang['iPortfolio']['OEA']['JUPAS'], $PATH_WRT_ROOT."home/portfolio/oea/teacher/?task=report_studentOea", $Teacher_JupasReport);
      		}
   			
			// SIS OLE REPORT LINK   	
  			if($sys_custom['ipf']['HideipfMenu']['Report_SIS'])
         	{
	            $MenuArr["Reports"]['Child']["Teacher_HolisticReport."] = array('Holistic Report Card', $PATH_WRT_ROOT."home/portfolio/profile/report_sis/holistic_reportcard.php", $Teacher_HolisticReport);
	            $MenuArr["Reports"]['Child']["Teacher_OLEReport"] = array($iPort['menu']['ole_report'], $PATH_WRT_ROOT."home/portfolio/profile/report_sis/ole_report.php", $Teacher_OLEReport);
          	}
          	
      	
        }
    		
    		if(strstr($ck_user_rights, ":manage:"))
    		{
    //			$MenuArr["DataHandling"] = array($iPort['menu']['data_handling'], "#", $DataHandling);
    //			$MenuArr["DataHandling"]['Child']["Teacher_DataExport"] = array($iPort['menu']['data_export'], $PATH_WRT_ROOT."home/portfolio/teacher/data_handling/data_export.php", $Teacher_DataExport);
    //			$MenuArr["DataHandling"]['Child']["Teacher_BurningCD"] = array($iPort['menu']['prepare_cd_rom_burning'], $PATH_WRT_ROOT."home/portfolio/learning_portfolio/contents_admin/prepare_CDburning.php", $Teacher_BurningCD);
    	
    			$MenuArr["Settings"] = array($iPort['menu']['settings'], "#", $Settings);
    			if(strstr($ck_function_rights, "Profile:OLR")) {
           			$MenuArr["Settings"]['Child']["Settings_OLE"] = array($iPort['menu']['slp'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/slp/ele.php", $Settings_OLE);
    			}
          		if(strstr($ck_function_rights, "Profile:ReportSetting") && !$sys_custom['LivingHomeopathy']) {
            		$MenuArr["Settings"]['Child']["Settings_Reports"] = array($iPort['menu']['reports'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/report/full_report_config.php", $Settings_Reports);
          		}
          		
          		//if(strstr($ck_function_rights, "Profile:ReportSetting")) {
          		if(
          			!$plugin['StudentDataAnalysisSystem'] &&
          			(
          				$sys_custom['iPf']['Report']['AssessmentStatisticReport']['AcademicProgress'] ||
          				$sys_custom['iPf']['Report']['AssessmentStatisticReport']['CCHPWSS']
          			)
          		){
	          		if(strstr($ck_function_rights, "Profile:Stat") && !$sys_custom['ipf']['HideipfMenu']['AssessmentStatisticReport'] && $this->IS_IPF_SUPERADMIN() && !$sys_custom['LivingHomeopathy']){
	            		$MenuArr["Settings"]['Child']["Settings_AssessmentStatReports"] = array($iPort['menu']['assessment_statistic_report'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/assessmentStatReport/accessRightConfigAdmin.php", $Settings_AssessmentStatisticReport);
	          		}
          		}
          		
          		if($sys_custom['NgWah_SAS']) {
          			$MenuArr["Settings"]['Child']["Settings_SAS"] = array($Lang['iPortfolio']['NgWah_SAS'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/SAS/category_credit_point.php", $Settings_SAS);
          		}
          		
    			if(strstr($ck_user_rights, ":web:") && strstr($ck_function_rights, "Sharing:PeerReview") && !$sys_custom['LivingHomeopathy']) {
    				$MenuArr["Settings"]['Child']["Settings_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT."home/portfolio/contents_admin/learning_portfolio_setting.php", $Settings_LearningPortfolio);
    			}
    			if(!$sys_custom['LivingHomeopathy']){
    				$MenuArr["Settings"]['Child']["Settings_SelfAccount"] = array($iPort['menu']['self_account'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/self_account/config.php", $Settings_SelfAccount);
    			}
    			if($sys_custom['cwk_SLP']) {
    				$MenuArr["Settings"]['Child']["Settings_TeacherComment"] = array($Lang['iPortfolio']['TeacherComment'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/teacher_comment/config.php", $Settings_TeacherComment);		
    			}
            
          		//CDburning setting
          		if(!$sys_custom['LivingHomeopathy']){
          			$MenuArr["Settings"]['Child']["Settings_BurningCD"] = array($Lang['iPortfolio']['Settings_BurningCD'], $PATH_WRT_ROOT."home/portfolio/learning_portfolio/contents_admin/prepare_CDburning_setting.php", $Settings_BurningCD);
          			$MenuArr["Settings"]['Child']["Settings_Grouping"] = array($Lang['iPortfolio']['Group'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/group/index.php", $Settings_Grouping);
          		}
    		}

			 //            	if ($sys_custom['iPf']['JUPAS'] && ($this->IS_IPF_SUPERADMIN() || $_SESSION["USER_BASIC_INFO"]["is_class_teacher"])) {
			if (($this->thisRegion == 'zh_HK') && ($this->IS_IPF_SUPERADMIN() || strstr($ck_function_rights, "Jupas:All"))&&!($sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile']) && !$sys_custom['LivingHomeopathy']) {
				if($MenuArr["Settings"] == ''){
					$MenuArr["Settings"] = array($iPort['menu']['settings'], "#", $Settings);
				}
            	$MenuArr["Settings"]['Child']["Settings_OEA"] = array($Lang['iPortfolio']['OEA']['JUPAS'], $PATH_WRT_ROOT."home/portfolio/oea/settings/index.php", $Settings_OEA);
            }
   		
    	}
    	else if($Role == "Parent")
    	{
    		// check if parent account, set below menu
    		$MenuArr["Data"]["Parent_MyInformation"] = array($iPort['menu']['my_information'], $PATH_WRT_ROOT."home/portfolio/profile/student_info_parent.php", $Parent_MyInformation);
    		if($this->HAS_SCHOOL_RECORD_VIEW_RIGHT())
    		{
    			$this->LOAD_ACCESS_RIGHT();
    			
    			# Check school record accessible
	    		if($this->access_config['merit'] == 1)
	    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/merit/index.php";
	    		else if($this->access_config['assessment_report'] == 1)
	    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/assessment/index.php";
	    		else if($this->access_config['activity'] == 1)
	    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/activity/index.php";
	    		else if($this->access_config['award'] == 1)
	    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/award/index.php";
	    		else if($this->access_config['reading_records'] == 1)
	    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/reading_records/index.php";
	    		else if($this->access_config['teacher_comment'] == 1)
	    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/comment/index.php";
	    		else if($this->access_config['attendance'] == 1)
	    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/attendance/index.php";
	    		else if($this->access_config['service'] == 1)
	    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/service/index.php";
	    		else
	    			$default_school_record_page = "";
    		  	
    		  	//$MenuArr["Data"]["Parent_SchoolRecords"] = array($iPort['menu']['school_records'], $PATH_WRT_ROOT."home/portfolio/profile/merit/index.php", $Parent_SchoolRecords);
    		  	$MenuArr["Data"]["Parent_SchoolRecords"] = array($iPort['menu']['school_records'], $default_school_record_page, $Parent_SchoolRecords);
    		}
  			if($this->HAS_RIGHT("ole"))
  			{
      		$MenuArr["Data"]["Parent_OLE"] = array($iPort['menu']['ole'], $PATH_WRT_ROOT."home/portfolio/student/ole.php", $Parent_OLE);
      		//$MenuArr["Data"]["Parent_ExtOLE"] = array($iPort["external_record"], $PATH_WRT_ROOT."home/portfolio/student/ole_record_ext.php", $Parent_ExtOLE);
      		$MenuArr["Data"]["Parent_ExtOLE"] = array($iPort["external_record"], $PATH_WRT_ROOT."home/portfolio/student/ole_record.php?IntExt=1", $Parent_ExtOLE);
      	}
    		$MenuArr["Data"]["Parent_SelfAccount"] = array($iPort['menu']['self_account'], $PATH_WRT_ROOT."home/portfolio/student/self_account/self_account.php", $Parent_SelfAccount);
    		if(strstr($ck_user_rights, ":web:") && $this->HAS_RIGHT("learning_sharing"))
    		  $MenuArr["Personal"]["Parent_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT."home/portfolio/learning_portfolio/index_parent.php", $Parent_LearningPortfolio);
    		if(strstr($ck_user_rights, ":growth:") && $this->HAS_RIGHT("growth_scheme"))
    		  $MenuArr["School"]["Parent_SchoolBasedScheme"] = array($iPort['menu']['school_based_scheme'], $PATH_WRT_ROOT."home/portfolio/student/sbs/index.php", $Parent_SchoolBasedScheme);
        if($sys_custom['audit_student_needs'])
    		{
    			include_once($eclass_filepath."/src/includes/php/lib-custom-audit.php");
    			$LibAudit = new audit();
    			if($LibAudit->STUDENT_CAN_ACCESS())
    		    $MenuArr["Addon"]["Parent_AddonModule"] = array($iPort['menu']['addon_module'], $PATH_WRT_ROOT."home/portfolio/student/addon_module.php", $Parent_AddonModule);
    		}
    	    if($CDparent_right){
    	    	$MenuArr["Data"]["Parent_BurningCD"] = array($Lang['iPortfolio']['Settings_BurningCD'], $PATH_WRT_ROOT."home/portfolio/learning_portfolio/contents_admin/prepare_CDburning_nonteacher.php", $Parent_BurningCD);    	    	
    	    }
    	}
    	else if($Role == "Student" && $sys_custom['lp_stem_learning_scheme'])
    	{
    	    $this->LOAD_ACCESS_RIGHT();
    	    $MenuArr["Data"] = array();
    	    $MenuArr["Personal"] = array();
    		if($this->access_config['student_info'] == 1)
    			$MenuArr["Data"]["Student_MyInformation"] = array($iPort['menu']['my_information'], $PATH_WRT_ROOT."home/portfolio/profile/student_info_student.php", $Student_MyInformation);
    	
    	    $MenuArr["Personal"]["Student_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT.'home/portfolio/learning_portfolio_v2/', $Student_LearningPortfolio);
    	}
    	else
    	{
        $this->LOAD_ACCESS_RIGHT();
        
    		// check if student account, set below menu
    		if($this->access_config['student_info'] == 1)
    			$MenuArr["Data"]["Student_MyInformation"] = array($iPort['menu']['my_information'], $PATH_WRT_ROOT."home/portfolio/profile/student_info_student.php", $Student_MyInformation);

    		# Check school record accessible
    		if($this->access_config['merit'] == 1)
    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/merit/index.php";
    		else if($this->access_config['assessment_report'] == 1)
    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/assessment/index.php";
    		else if($this->access_config['activity'] == 1)
    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/activity/index.php";
    		else if($this->access_config['award'] == 1)
    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/award/index.php";
    		else if($this->access_config['reading_records'] == 1)
    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/reading_records/index.php";
    		else if($this->access_config['teacher_comment'] == 1)
    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/comment/index.php";
    		else if($this->access_config['attendance'] == 1)
    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/attendance/index.php";
    		else if($this->access_config['service'] == 1)
    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/service/index.php";
    		else
    			$default_school_record_page = "";
    		if($sys_custom['LivingHomeopathy'] && $this->access_config['activity'] == 1){
    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/activity/index.php";
    		}
    		if($default_school_record_page != "")
    			$MenuArr["Data"]["Student_SchoolRecords"] = array($iPort['menu']['school_records'], $default_school_record_page, $Student_SchoolRecords);
    		
    		if($this->access_config['ole'] == 1 && !$sys_custom['LivingHomeopathy'])
    		{
    			if($sys_custom['iPf']['css']['Report']['SLP']){
		        	$display_Internal = $Lang['iPortfolio']['StudentMenu']['InternalOLE'];
					$display_External = $Lang['iPortfolio']['StudentMenu']['ExternalOLE'];
		            $MenuArr["Data"]["Student_OLE"] = array($display_Internal, $PATH_WRT_ROOT."home/portfolio/student/ole.php", $Student_OLE);
		            $MenuArr["Data"]["Student_ExtOLE"] = array($display_External, $PATH_WRT_ROOT."home/portfolio/student/ole_record.php?IntExt=1", $Student_ExtOLE);
		        }
		        else{
		      		$MenuArr["Data"]["Student_OLE"] = array($iPort['menu']['ole'], $PATH_WRT_ROOT."home/portfolio/student/ole.php", $Student_OLE);
		      		//$MenuArr["Data"]["Student_ExtOLE"] = array($iPort["external_record"], $PATH_WRT_ROOT."home/portfolio/student/ole_record_ext.php", $Student_ExtOLE);
		      		$MenuArr["Data"]["Student_ExtOLE"] = array($iPort["external_record"], $PATH_WRT_ROOT."home/portfolio/student/ole_record.php?IntExt=1", $Student_ExtOLE);
		        }
    			
    		}
			if(!$sys_custom['LivingHomeopathy']){
    		 	$MenuArr["Data"]["Student_SelfAccount"] = array($iPort['menu']['self_account'], $PATH_WRT_ROOT."home/portfolio/student/self_account/self_account.php", $Student_SelfAccount);
			}
    		if(strstr($ck_user_rights, ":web:") && $this->access_config['learning_sharing'] == 1 && !$sys_custom['LivingHomeopathy']){
		    
				$learning_portfolio_url = $iportfolio_lp_version==2? "home/portfolio/learning_portfolio_v2/" : "home/portfolio/learning_portfolio/index.php";
    			$MenuArr["Personal"]["Student_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT.$learning_portfolio_url, $Student_LearningPortfolio);
    		}
    		if(strstr($ck_user_rights, ":growth:") && $this->access_config['growth_scheme'] == 1 && !$sys_custom['LivingHomeopathy'])
    			$MenuArr["School"]["Student_SchoolBasedScheme"] = array($iPort['menu']['school_based_scheme'], $PATH_WRT_ROOT."home/portfolio/student/sbs/index.php", $Student_SchoolBasedScheme);
    		if($sys_custom['audit_student_needs'])
    		{
    			include_once($eclass_filepath."/src/includes/php/lib-custom-audit.php");
    			$LibAudit = new audit();
    			if($LibAudit->STUDENT_CAN_ACCESS())
    //					$MenuArr["Data"]["Student_AuditStudent"] = array($iPort['menu']['audit_student'], "javascript:newWindow('http://".$eclass_httppath."src/ip2portfolio/audit_student_needs/student/index.php',95)", $Student_AuditStudent);
    //					$MenuArr["Addon"]["Student_AddonModule"] = array($iPort['menu']['addon_module'], $PATH_WRT_ROOT."home/portfolio/student/addon_module.php", $Student_AddonModule);
    				$MenuArr["Addon"]["Student_AddonModule"] = array($iPort['menu']['audit_student'], "javascript:newWindow('http://".$eclass_httppath."src/ip2portfolio/audit_student_needs/student/index.php',95)", $Student_AuditStudent);
    		}
    		
    		if (($this->thisRegion == 'zh_HK')&&!($sys_custom['iPf']['StPaulCoeduPriSchool']['Report']['StudentLearningProfile']) && !$sys_custom['LivingHomeopathy']) {

				$showJupasForStudent = $this->showJUPASForStudent($luser);

				if($showJupasForStudent){
	    			$MenuArr["Data"]["JUPAS"] = array($Lang['iPortfolio']['OEA']['JUPAS'], $PATH_WRT_ROOT."home/portfolio/oea/student/", $Student_OEA);
				}
    		}
    		
			if (($this->thisRegion == 'zh_HK') && !$sys_custom['LivingHomeopathy']) {
				$showSLPReportPrintingForStduent = $this->showSLPReportPrintingForStduent($luser);
				$showDynamicReportPrintingForStudent = $this->showDynamicReportPrintingForStudent($luser);
				
				if($showSLPReportPrintingForStduent || $showDynamicReportPrintingForStudent){
					$MenuArr["Data"]["STUDENT_SLP_REPORT"] = array($iPort['menu']['reports'], $PATH_WRT_ROOT."home/portfolio/student/report.php", $Student_Report);
				} 
				# check only if SLP Report & Dynamic Report are disabled - for Ng Wah SAS Report
				else if(!$showSLPReportPrintingForStduent && !$showDynamicReportPrintingForStudent && $sys_custom['NgWah_SAS']){
					$showSASReportPrintForStudent =  $this->showNgWahSASReport($luser);
					if($showSASReportPrintForStudent){
						$MenuArr["Data"]["STUDENT_SLP_REPORT"] = array($iPort['menu']['reports'], $PATH_WRT_ROOT."home/portfolio/student/ngwah_sas_report.php", $Student_Report);
					}
				}
			}
    		if ($sys_custom['iPf']['Volunteer'] && !$sys_custom['LivingHomeopathy']) {
    			$MenuArr["Data"]["Volunteer"] = array($Lang['iPortfolio']['VolunteerArr']['Volunteer'], $PATH_WRT_ROOT."home/portfolio/volunteer/student/?clearCoo=1", $Student_Volunteer);
    		}

    		if($CDstudent_right){
    	    	$MenuArr["Data"]["Student_BurningCD"] = array($Lang['iPortfolio']['Settings_BurningCD'], $PATH_WRT_ROOT."home/portfolio/learning_portfolio/contents_admin/prepare_CDburning_nonteacher.php", $Student_BurningCD);    	    	
    	    }
    	}
    	/////////////////////////////////////////////////
    	if($_SESSION["platform"]=="KIS"&&$Role == "Teacher"){
    		$MenuArr = array();
    		$MenuArr["Management"] = array($iPort['menu']['management'], "#", $Management);
    		$learning_portfolio_url = "home/portfolio/learning_portfolio_v2/";
      		$MenuArr["Management"]['Child']["Teacher_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT.$learning_portfolio_url, $Teacher_LearningPortfolio);
    		if(strstr($ck_user_rights, ":manage:"))
    		{
    			$MenuArr["Settings"] = array($iPort['menu']['settings'], "#", $Settings);
    			$MenuArr["Settings"]['Child']["Settings_Grouping"] = array($Lang['iPortfolio']['Group'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/group/index.php?group_type=0", $Settings_Grouping);  	
    		}      		
    	}
    	
    	if(isset($_SESSION["ncs_role"]) && ($_SESSION["ncs_role"]=='A' || $_SESSION["ncs_role"]=='T')){
    		$MenuArr = array();
    		$MenuArr["Management"] = array($iPort['menu']['management'], "#", $Management);
    		if($this->HAS_RIGHT("student_info"))
    			$MenuArr["Management"]['Child']["Teacher_StudentAccount"] = array($iPort['menu']['student_account'], $PATH_WRT_ROOT."home/portfolio/school_records.php", $Teacher_StudentAccount);
    		
    		if(!isset($_SESSION["ncs_role"]))
    			$MenuArr["Management"]['Child']["Teacher_OLE"] = array($iPort['menu']['slp'], $PATH_WRT_ROOT."home/portfolio/teacher/management/oleManagement.php", $Teacher_OLE);
    			
    		if($sys_custom['iPortfolio_assessment_report'] || (isset($_SESSION['ncs_role'])&&($_SESSION['ncs_role'] != ""))){
    			$MenuArr["Management"]['Child']["Assessment_Report"] = array($iPort['menu']['assessment_report'], $PATH_WRT_ROOT."home/portfolio/teacher/management/assessment_report/assessment_report_list.php", $Teacher_AssessmentReport);
    		}
    		
    		if(isset($_SESSION['ncs_role'])&&($_SESSION['ncs_role'] != "")){
    			$MenuArr["Management"]['Child']["third_party_result"] = array($iPort['menu']['third_party'], $PATH_WRT_ROOT."home/portfolio/teacher/management/third_party/index.php", $Teacher_ThirdParty);
    		}
    		
    		$MenuArr["Management"]['Child']["Teacher_SchoolBasedScheme"] = array($iPort['menu']['school_based_scheme'], $PATH_WRT_ROOT."home/portfolio/teacher/management/sbs/index.php", $Teacher_SchoolBasedScheme);
    			
    		if(strstr($ck_user_rights, ":web:") && strstr($ck_function_rights, "Sharing") && !isset($_SESSION["ncs_role"])){
    			if(strstr($ck_function_rights, "Sharing:Content")){
    				$learning_portfolio_url = $iportfolio_lp_version==2? "home/portfolio/learning_portfolio_v2/" : "home/portfolio/contents_admin/index_scheme.php";
    				$MenuArr["Management"]['Child']["Teacher_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT.$learning_portfolio_url, $Teacher_LearningPortfolio);
    			}else if(strstr($ck_function_rights, "Sharing:Template")){
    				$MenuArr["Management"]['Child']["Teacher_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT."home/portfolio/learning_portfolio/contents_admin/templates_manage.php", $Teacher_LearningPortfolio);
    			}
    		}
    	   	
    		// handle SIS Report Tab 20141008
    		if($this->HAS_RIGHT("print_report") || $this->HAS_RIGHT("ole") || $sys_custom['audit_student_needs'] || strstr($ck_function_rights, "Profile:Stat"))
    		{
    			$MenuArr["Reports"] = array($iPort['menu']['reports'], "#", $Reports);
    			    		
    			if($this->HAS_RIGHT("ole") && !isset($_SESSION['ncs_role']))
    			{
    				if(!$sys_custom['ipf']['HideipfMenu']['OLEReport']){
    					$MenuArr["Reports"]['Child']["Teacher_oleView"] = array($iPort['menu']['slp'], $PATH_WRT_ROOT."home/portfolio/teacher/management/oleView.php", $Teacher_OLEView);
    					$MenuArr["Reports"]['Child']["Teacher_OLEReport"] = array($iPort['menu']['ole_report'], $PATH_WRT_ROOT."home/portfolio/profile/ole/index_stat.php", $Teacher_OLEReport);
    				}
    			}
    			
    			if(isset($_SESSION['ncs_role'])&&($_SESSION['ncs_role'] != "")){
    				$MenuArr["Reports"]['Child']["pl2_quiz_report"] = array($iPort['menu']['pl2_quiz_report'], $PATH_WRT_ROOT."home/portfolio/teacher/management/pl2_quiz_result/index.php", $Teacher_PLQuizResult);
    			}
    			
    			 
    		}
    		
    		if(strstr($ck_user_rights, ":manage:")){
    			$MenuArr["Settings"] = array($iPort['menu']['settings'], "#", $Settings);
    			$MenuArr["Settings"]['Child']["Settings_Grouping"] = array($Lang['iPortfolio']['Group'], $PATH_WRT_ROOT."home/portfolio/teacher/settings/group/index.php?group_type=0", $Settings_Grouping);
    		}
    	}elseif(isset($_SESSION["ncs_role"]) && ($_SESSION["ncs_role"]=='S')){
    		// check if student account, set below menu
    		$MenuArr = array();
    		if($this->access_config['student_info'] == 1)
    			$MenuArr["Data"]["Student_MyInformation"] = array($iPort['menu']['my_information'], $PATH_WRT_ROOT."home/portfolio/profile/student_info_student.php", $Student_MyInformation);
    		
    			# Check school record accessible
    			if($this->access_config['merit'] == 1)
    				$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/merit/index.php";
    			else if($this->access_config['assessment_report'] == 1)
    				$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/assessment/index.php";
    			else if($this->access_config['activity'] == 1)
    				$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/activity/index.php";
    			else if($this->access_config['award'] == 1)
    				$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/award/index.php";
	    		else if($this->access_config['reading_records'] == 1)
	    			$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/reading_records/index.php";
    			else if($this->access_config['teacher_comment'] == 1)
    				$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/comment/index.php";
    			else if($this->access_config['attendance'] == 1)
    				$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/attendance/index.php";
    			else if($this->access_config['service'] == 1)
    				$default_school_record_page = $PATH_WRT_ROOT."home/portfolio/profile/service/index.php";
    			else
    				$default_school_record_page = "";
    			if($default_school_record_page != "")
    				$MenuArr["Data"]["Student_SchoolRecords"] = array($iPort['menu']['school_records'], $default_school_record_page, $Student_SchoolRecords);
    		
    				if($this->access_config['ole'] == 1)
    				{
    					if($sys_custom['iPf']['css']['Report']['SLP']){
    						//$display_Internal = $Lang['iPortfolio']['StudentMenu']['InternalOLE'];
    						$display_External = $Lang['iPortfolio']['StudentMenu']['ExternalOLE'];
    						//$MenuArr["Data"]["Student_OLE"] = array($display_Internal, $PATH_WRT_ROOT."home/portfolio/student/ole.php", $Student_OLE);
    						$MenuArr["Data"]["Student_ExtOLE"] = array($display_External, $PATH_WRT_ROOT."home/portfolio/student/ole_record.php?IntExt=1", $Student_ExtOLE);
    				}
    				else{
    						//$MenuArr["Data"]["Student_OLE"] = array($iPort['menu']['ole'], $PATH_WRT_ROOT."home/portfolio/student/ole.php", $Student_OLE);
    						//$MenuArr["Data"]["Student_ExtOLE"] = array($iPort["external_record"], $PATH_WRT_ROOT."home/portfolio/student/ole_record_ext.php", $Student_ExtOLE);
    						$MenuArr["Data"]["Student_ExtOLE"] = array($iPort["external_record"], $PATH_WRT_ROOT."home/portfolio/student/ole_record.php?IntExt=1", $Student_ExtOLE);
    				}
    													 
    			}
    			
    			if(strstr($ck_user_rights, ":web:") && $this->access_config['learning_sharing'] == 1){
    		
    				$learning_portfolio_url = $iportfolio_lp_version==2? "home/portfolio/learning_portfolio_v2/" : "home/portfolio/learning_portfolio/index.php";
    				$MenuArr["Personal"]["Student_LearningPortfolio"] = array($iPort['menu']['learning_portfolio'], $PATH_WRT_ROOT.$learning_portfolio_url, $Student_LearningPortfolio);
    			}
    			if(strstr($ck_user_rights, ":growth:") && $this->access_config['growth_scheme'] == 1)
    				$MenuArr["School"]["Student_SchoolBasedScheme"] = array($iPort['menu']['school_based_scheme'], $PATH_WRT_ROOT."home/portfolio/student/sbs/index.php", $Student_SchoolBasedScheme);
    			    		   		
    			if (($this->thisRegion == 'zh_HK')) {
    				$showSLPReportPrintingForStduent = $this->showSLPReportPrintingForStduent($luser);
    				$showDynamicReportPrintingForStudent = $this->showDynamicReportPrintingForStudent($luser);
    		
    				if($showSLPReportPrintingForStduent || $showDynamicReportPrintingForStudent){
    					$MenuArr["Data"]["STUDENT_SLP_REPORT"] = array($iPort['menu']['reports'], $PATH_WRT_ROOT."home/portfolio/student/report.php", $Student_Report);
    				}
    			}
    	}

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass iPortfolio";'."\n";
        $js.= '</script>'."\n";
    
      # module information
      $MODULE_OBJ['title'] = $iPort['iPortfolio'].$js;
      
      if($Role == "Teacher")
      $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/iPortfolio/module_icon_b.gif";
      else
      $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/iPortfolio/module_icon.gif";
      
      $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/portfolio/index.php";
      $MODULE_OBJ['menu'] = $MenuArr;

      if($Role == "Parent")
      {
        // get the student id
        $ChildArr = $this->GET_CHILDREN_INFO();
        
        //hdebug_r($ChildArr);
        if($ck_current_children_id == "")
        {
        	if(count($ChildArr)  > 0)
        	$this->SET_CURRENT_CHILD_ID_BY_SESSION($ChildArr[0][0]);
      	}
      	else
      	$this->SET_CURRENT_CHILD_ID_BY_SESSION($ck_current_children_id);
        
         $TmpChildArr = array();
        // if more than one, set selection box
        for($i = 0; $i < count($ChildArr); $i++)
        {
	        $TmpChild_Class = $ChildArr[$i][1];
      		$TmpChild_ClassNumber = $ChildArr[$i][2];
	        $TmpChild_EnglishName = $ChildArr[$i][3];
      		$TmpChild_ChineseName = $ChildArr[$i][4];
      		
      		if($lang == "en")
    			{
    				$cName1 = $TmpChild_EnglishName;
    				$cName2 = $TmpChild_ChineseName;
    			}
    			else
    			{
    				$cName1 = $TmpChild_ChineseName;
    				$cName2 = $TmpChild_EnglishName;
    			}
    	
    			if($cName1 != "")
    			$TmpChild_Name = $cName1;
    			else
    			$TmpChild_Name = $cName2;
          	
    			if($TmpChild_Class != "")
    			{
    				if($TmpChild_ClassNumber != "")
    				{
    					$TmpChild_Name .= " (".$TmpChild_Class."-".$TmpChild_ClassNumber.")";
    				}
    				else
    				{
    					$TmpChild_Name .= " (".$TmpChild_Class.")";
    				}
    			}
    	        
          $TmpChildArr[] = array($ChildArr[$i][0], $TmpChild_Name);
          
          if($ck_current_children_id == $ChildArr[$i][0])
          {
            $child_Index = $i;
            $TmpChildID = $ChildArr[$i][0];
          }
        }
            
        $Child_Selection = $linterface->GET_SELECTION_BOX($TmpChildArr, "name='Child_Select' onChange='changeChild(this.value);' style='width:160px' ","", $TmpChildID);
        $Js_Function = "<script language='javascript'>";
        $Js_Function .= "function changeChild(id) {";
       // $Js_Function .= "alert(id);";
        $Js_Function .= "document.change_child_form.submit();";
        $Js_Function .= "}";
        $Js_Function .= "</script>";
        
        $FomHTML = "<form style=\"{margin: 0;}\" method=\"POST\" action=\"".$PATH_WRT_ROOT."home/portfolio/update_child_session.php\" name=\"change_child_form\" id=\"change_child_form\" >";
        $FomHTML .= $Child_Selection;
        $FomHTML .= "</form>";
        
        //echo $FomHTML;
       // echo $Js_Function;
        
        // get the select children name & photo
        if($child_Index == "")
      	{
        	$child_Index = 0;
      	}

      	$Child_Id = $ChildArr[$child_Index][0];
      	$Child_Class = $ChildArr[$child_Index][1];
      	$Child_ClassNumber = $ChildArr[$child_Index][2];
      	$Child_EnglishName = $ChildArr[$child_Index][3];
      	$Child_ChineseName = $ChildArr[$child_Index][4];
      	$Child_PhotoLink = $this->GET_IPORTFOLIO_PHOTO_BY_USERID($Child_Id);
      	
      	if($lang == "en")
    		{
    			$cName1 = $Child_EnglishName;
    			$cName2 = $Child_ChineseName;
    		}
    		else
    		{
    			$cName1 = $Child_ChineseName;
    			$cName2 = $Child_EnglishName;
    		}
    	
    		if($cName1 != "")
    		$Child_Name = $cName1;
    		else
    		$Child_Name = $cName2;
          	
    		if($Child_Class != "")
    		{
    			if($Child_ClassNumber != "")
    			{
    				$Child_Name .= " (".$Child_Class."-".$Child_ClassNumber.")";
    			}
    			else
    			{
    				$Child_Name .= " (".$Child_Class.")";
    			}
    		}
    		
      	$MODULE_OBJ['user_photo'] = $Child_PhotoLink;
      	
     	   //echo $FomHTML;
         // echo $Js_Function;
      	//$MODULE_OBJ['user_name'] = $Child_Name;
      	$MODULE_OBJ['user_name'] = $FomHTML.$Js_Function;
      	$MODULE_OBJ['title_css'] = "menu_opened";
      }
      else
      {
        // student role
      	$MODULE_OBJ['user_photo'] = $PhotoLink;
      	$MODULE_OBJ['user_name'] = $UserName;
      	$MODULE_OBJ['title_css'] = "menu_opened";
    	}
    	if($_SESSION["platform"]=="KIS"&&($Role!= "Teacher"||($Teacher_LearningPortfolio!=1&&$Settings_Grouping!=1))){
    		return array('title'=>$iPort['iPortfolio']);
    	}else{
    		return $MODULE_OBJ;
    	}		
    	
    } // end function GET_MODULE_OBJ_ARR
    
  	# Load access right settings
  	function LOAD_ACCESS_RIGHT()
  	{
  		global $eclass_filepath;	

		if(file_exists($eclass_filepath."/files/portfolio_right.txt"))
  		{		
  			$filecontent = trim(get_file_content($eclass_filepath."/files/portfolio_right.txt"));
  			$access_config_temp = unserialize($filecontent);
  			
  			if($this->IS_IPF_SUPERADMIN())
  				$this->access_config = $access_config_temp['admin'];
  			else if($_SESSION['UserType'] == 1)
  			{
  				if(is_array($access_config_temp))
  				{
  					foreach($access_config_temp['T'] as $key => $value)
  					{
  						switch($value)
  						{
  							case 0:
  							case 1:
  								$this->access_config[$key] = $value;
  								break;
  							case 2:
  								$this->access_config[$key] = $this->Is_Form_Teacher() ? 1 : 0;
  								break;
  							case 3:
  								$this->access_config[$key] = ($this->Is_Form_Teacher() || $this->isSubjectTeacher()) ? 1 : 0;
  								break;
  						}
  					}
  				}
  			}
  			else if($_SESSION['UserType'] == 2)
  				$this->access_config = $access_config_temp['S'];
  			else if($_SESSION['UserType'] == 3)
  				$this->access_config = $access_config_temp['P'];
  		}
  	}

  	# Translate User ID in IP to User ID in eClass
  	function IP_USER_ID_TO_EC_USER_ID($ParUserID){
  		global $eclass_db, $intranet_db,$ck_memberType;
  		# Select from PORTFOLIO_STUDENT as alumni does not hv record in user_course
  			$sql =	"
  							SELECT
  								CourseUserID as user_id
  							FROM
  								{$eclass_db}.PORTFOLIO_STUDENT
  							WHERE
  								UserID = '".$ParUserID."'
  						";			
  		

		
  		$ReturnVal = current($this->returnVector($sql));
  		
  		if(!$ReturnVal){
  			# Select from user course
  			$sql =	"
						SELECT
							uc.user_id
						FROM
							{$eclass_db}.user_course as uc
						LEFT JOIN
							{$intranet_db}.INTRANET_USER as iu
						ON
							iu.UserEmail = uc.user_email
						WHERE
							iu.UserID = '".$ParUserID."' AND
							uc.course_id = '".$this->course_id."'
					";
			$ReturnVal = current($this->returnVector($sql));		
  		}
  		
  
  		return $ReturnVal;
  	}

  	# Translate a set of User ID in IP to User ID in eClass
  	# Assume that User ID is an integer.
  	function IP_USER_ID_TO_EC_USER_ID_SET_OPERATION($ParUserIDSet){
  		global $eclass_db, $intranet_db;
  		
  		if (count($ParUserIDSet) == 0) return array();
  		
  		$ids_str = join(',', $ParUserIDSet);
  
  		# Select from user course
  		$sql =	"
  							SELECT
  								uc.user_id
  							FROM
  								{$eclass_db}.user_course as uc
  							LEFT JOIN
  								{$intranet_db}.INTRANET_USER as iu
  							ON
  								iu.UserEmail = uc.user_email
  							WHERE
  								iu.UserID IN ($ids_str) AND
  								uc.course_id = '".$this->course_id."'
  						";
  		$ReturnVal = $this->returnVector($sql);
  
  		return $ReturnVal;
  	}

  	# Translate User ID in eclass to User ID in IP
  	function EC_USER_ID_TO_IP_USER_ID($ParCourseUserID){
  		global $eclass_db, $intranet_db;

  		# Select from user course
  		$sql =	"
  							SELECT
  								iu.UserID
  							FROM
  								{$eclass_db}.user_course as uc
  							LEFT JOIN
  								{$intranet_db}.INTRANET_USER as iu
  							ON
  								iu.UserEmail = uc.user_email
  							WHERE
  								uc.user_id = '".$ParCourseUserID."' AND
  								uc.course_id = '".$this->course_id."'
  						";
  		$ReturnVal = $this->returnVector($sql);
  
  		return $ReturnVal[0];
  	}
  	
  	# Get number of learning portfolio by a student that are not viewed
  	function GET_LP_NOT_VIEW_COUNT_BY_USER_ID($ParUserID)
  	{
  		global $eclass_db, $intranet_db;
  
  		$sql =	"
  							SELECT
  								count(*)
  							FROM
  								{$intranet_db}.INTRANET_USER AS iu
  							LEFT JOIN
  								{$eclass_db}.PORTFOLIO_STUDENT AS ps
  							ON
  								ps.UserID=iu.UserID
  							LEFT JOIN
  								".$this->course_db.".portfolio_tracking_last AS ptl
  							ON
  								ptl.student_id=ps.CourseUserID AND
  								(ptl.teacher_id='".$this->IP_USER_ID_TO_EC_USER_ID($_SESSION['UserID'])."' OR ptl.portfolio_tracking_id IS NULL)
  							WHERE
  								iu.RecordType='2' AND
  								iu.RecordStatus = '1' AND
  								ps.WebSAMSRegNo IS NOT NULL AND
  								ps.UserKey IS NOT NULL AND
  								ps.IsSuspend = 0 AND
  								iu.UserID = '".$ParUserID."' AND
  								(ptl.portfolio_tracking_id IS NOT NULL AND (UNIX_TIMESTAMP(ptl.student_publish_time)>UNIX_TIMESTAMP(ptl.teacher_view_time) OR ptl.teacher_view_time IS NULL))
  						";
  
  		$ReturnVal = $this->returnVector($sql);
  
  		return $ReturnVal[0];
  	}
  	
  	# Get UserID in Course According to Intranet UserID
  	function getCourseUserID($my_user_id)
  	{
  		global $eclass_db;
  /*
  		$sql = "SELECT
  					um.user_id
  				FROM
  					$this->course_db.usermaster AS um
  					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps ON um.user_id = ps.CourseUserID
  				WHERE
  					ps.UserID='$my_user_id'
  				";
  */
  		$sql = "SELECT
  					ps.CourseUserID
  				FROM
  					{$eclass_db}.PORTFOLIO_STUDENT AS ps
  				WHERE
  					ps.UserID='$my_user_id'
  				";
  				
  		$row = $this->returnVector($sql);
  		
  		return $row[0];
  	}
  	
  	# Get StudentID and ClassName from Intranet According to user_id in Course
  	function GET_STUDENT_ID($my_user_id)
  	{
  		global $eclass_db, $intranet_db, $ck_course_id;
  
  		$sql = "	SELECT
  							iu.ClassName, iu.UserID
  					FROM
  							{$intranet_db}.INTRANET_USER AS iu,
  							{$eclass_db}.user_course AS uc
  					WHERE
  							uc.user_id='$my_user_id'
  							AND iu.UserEmail=uc.user_email
  							AND uc.course_id='$ck_course_id'
  				";
  		$row = $this->returnArray($sql);
  
  		return $row[0];
  	}
  	
  	function returnUserNameAndClass($UserID)
  	{
  		global $intranet_db;
  
  		//$ClassNumberField = getClassNumberField();
  		$sql = "SELECT
  					iu.EnglishName,
  					iu.ChineseName,
  					iu.ClassName,
  					iu.ClassNumber,
  					yc.YearClassID
  				FROM
  					{$intranet_db}.INTRANET_USER AS iu
  				INNER JOIN
  				  {$intranet_db}.YEAR_CLASS_USER AS ycu
  				ON
  				  ycu.UserID = iu.UserID
  				INNER JOIN
  				  {$intranet_db}.YEAR_CLASS AS yc
  				ON
  				  ycu.YearClassID = yc.YearClassID AND
  				  yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
  				WHERE
  					iu.UserID = '$UserID'
  				LIMIT 1
  				";
  		$ReturnArray = $this->returnArray($sql);
  
  		return $ReturnArray[0];
  	}
  	
  	// For revise SBS use
  	# get activated classes
  	function getActivatedClass($ClassLevelID="", $person_response="", $teacher_subject_id="")
  	{
  		$class_arr = $this->returnClassListData($ClassLevelID);
  		$class_activated_no = $this->returnClassStudentActivated();
  		
  		# Eric Yip : To select classes taught by teacher
  		# Modified by key [2008-12-11]: Separate the class & subject teacher list 
  		if($person_response == "CT")
  		$class_taught_arr = $this->GET_CLASS_TEACHER_CLASS();
  		else if($person_response == "ST")
  		$class_taught_arr = $this->GET_SUBJECT_TEACHER_CLASS($teacher_subject_id);
  		else
  		$class_taught_arr = array_merge($this->GET_SUBJECT_TEACHER_CLASS(), $this->GET_CLASS_TEACHER_CLASS());
  
  		for ($i=0; $i<sizeof($class_arr); $i++)
  		{
  			$class_obj = $class_arr[$i];
  			if ($class_activated_no[$class_obj["ClassName"]]>0 &&
  					($this->IS_IPF_ADMIN() || $this->IS_ADMIN() || in_array($class_obj["ClassName"], $class_taught_arr))
  			)
  			{
  				$active_class_arr[] = $class_obj;
  			}
  		}
  
  		return $active_class_arr;
  	}
  	
  	# Get Data of Class
  	function returnClassListData($ClassLevelID=""){
  		global $intranet_db;
  		$CLASSLEVELID_FIELD = 'y.YearID';
  		$CLASSLEVEL_FIELD = 'y.YearName';
  		//$CLASSNAME_FIELD = 'yc.ClassTitle'.strtoupper($_SESSION['intranet_session_language']);
  		$CLASSNAME_FIELD = 'yc.ClassTitleEN';
  
  		$conds = ($ClassLevelID!="") ? " AND $CLASSLEVELID_FIELD = '$ClassLevelID'" : "";
  		$sql = "SELECT DISTINCT
  					$CLASSNAME_FIELD AS ClassName,
  					$CLASSNAME_FIELD As ClassName_Title,
  					$CLASSLEVELID_FIELD as ClassLevelID,
  					$CLASSLEVEL_FIELD as LevelName
  				FROM
  					{$intranet_db}.YEAR_CLASS AS yc
  					LEFT JOIN {$intranet_db}.YEAR AS y ON y.YearID=yc.YearID
  					join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
  				WHERE
  					1 = 1
  					$conds
  				ORDER BY
  					$CLASSLEVEL_FIELD, $CLASSNAME_FIELD
  				";
  
  		return $this->returnArray($sql);
  	}
  	
  	# Count the number of student using iPortfolio in Class-based
  	function returnClassStudentActivated(){
  		global $eclass_db, $intranet_db;
  		//$CLASSNAME_FIELD = 'yc.ClassTitle'.strtoupper($_SESSION['intranet_session_language']);
  		$CLASSNAME_FIELD = 'yc.ClassTitleEN';
  
  		$sql = "SELECT
  					distinct ps.UserID,
  					$CLASSNAME_FIELD as ClassName,
  					COUNT(ps.RecordID) AS TotalActivated
  				FROM
  					{$intranet_db}.INTRANET_USER AS iu
  					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps ON ps.UserID=iu.UserID
					join {$intranet_db}.YEAR_CLASS_USER as ycu on iu.UserID = ycu.UserID
					join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
					join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
					join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
  				WHERE
  					iu.RecordType = '2'
  					AND iu.RecordStatus= '1'
  					AND ps.WebSAMSRegNo IS NOT NULL
  					AND ps.UserKey IS NOT NULL
  					AND ps.IsSuspend = 0
					AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
  				GROUP BY
  					$CLASSNAME_FIELD
  			";
  		$row = $this->returnArray($sql);
  
  		for ($i=0; $i<sizeof($row); $i++)
  		{
  			$ra[$row[$i]["ClassName"]] = $row[$i]["TotalActivated"];
  		}
  
  		return $ra;
  	}
  	
  	# Get OLE class level list
  	function GET_CLASSLEVEL_LIST($ParYear="", $class_taught_arr=""){
  		global $eclass_db, $intranet_db;
  		$CLASSLEVEL_FIELD = 'y.YearName';
  		$CLASSNAME_FIELD = 'yc.ClassTitle'.strtoupper($_SESSION['intranet_session_language']);
  		
  		if($ParYear != "")
  		{
  // It seems that there is no such alias "os.".
  //			# Define date range for specific year
  //			$YearRange = split("-", $ParYear);
  //			$FirstDate = $YearRange[0]."-09-01";
  //			$LastDate = $YearRange[1]."-08-31";
  //			
  //			$conds = " AND os.StartDate >= '".$FirstDate."'
  //						AND os.StartDate <= '".$LastDate."'
  //						";
  		}
  		
  		if(!$this->IS_IPF_ADMIN())
  		{
  			if($class_taught_arr != "")
  			{
  				$conds .= " AND $CLASSNAME_FIELD IN ('".implode("','", $class_taught_arr)."') ";
  			}
  			else
  			{
  				$conds .= " AND $CLASSNAME_FIELD IN ('".implode("','", array_merge($this->GET_CLASS_TEACHER_CLASS(), $this->GET_SUBJECT_TEACHER_CLASS()))."') ";
  			}
  		}
  		else
  		{
  			if($class_taught_arr != "")
  			{
  				$conds .= " AND $CLASSNAME_FIELD IN ('".implode("','", $class_taught_arr)."') ";
  			}
  		}
  		
  		$sql =	"
  							SELECT DISTINCT
  								$CLASSLEVEL_FIELD as ClassLevel
  							FROM
  								{$eclass_db}.PORTFOLIO_STUDENT as ps
  							LEFT JOIN
  								{$intranet_db}.INTRANET_USER as iu
  							ON
  								ps.UserID = iu.UserID
  							join {$intranet_db}.YEAR_CLASS_USER as ycu on iu.UserID = ycu.UserID
  							join {$intranet_db}.YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
  							join {$intranet_db}.YEAR as y on yc.YearID = y.YearID
  							join {$intranet_db}.ACADEMIC_YEAR as ay on yc.AcademicYearID = ay.AcademicYearID
  							WHERE
  								($CLASSLEVEL_FIELD != '' AND $CLASSLEVEL_FIELD IS NOT NULL)
  								$conds
  							ORDER BY
  								ClassLevel
  						";
  		$ReturnArr = $this->returnVector($sql);
  
  		return $ReturnArr;
  	}
  	
	# Get Semester Short Name and Full Name From IP
/*
	function getSemesterNameArrFromIP()
	{
		global $intranet_root;

		$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

		for($i=0; $i<sizeof($semester_data); $i++)
		{
			if ($semester_data[$i] != "")
			{
				$linedata = split("::",$semester_data[$i]);
				$sem = $linedata[0];
				$short_sem = preg_replace($this->semester_patterns, "", $linedata[0]);
				$semester_arr[] = trim($sem);
				$short_semester_arr[] = trim($short_sem);
			}
		}
		return array($semester_arr, $short_semester_arr);
	}
*/
    # New Implementation: retrieve from DB
  	function getSemesterNameArrFromIP($AcademicYear="", $withLang='')
  	{
  		global $intranet_db, $intranet_session_language;
  
      $cond = ($AcademicYear=="") ? "" : "WHERE ay.YearNameEN = '$AcademicYear' OR ay.YearNameB5 = '$AcademicYear'";
  
  //since there is chinese / english error , hardcode get YearTermNameEN to solve the problem
    /*  $sql =  " SELECT DISTINCT
                  ayt.YearTermName".strtoupper($intranet_session_language)."
                FROM {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt
                INNER JOIN {$intranet_db}.ACADEMIC_YEAR AS ay
                  ON ayt.AcademicYearID = ay.AcademicYearID
                $cond
              ";*/
    //2014-0321-1450-40184
	//$sql_lang = $withLang ? " ayt.YearTermName".strtoupper($intranet_session_language) : "ayt.YearTermNameEN";   
	if ($withLang) {
		$sql_lang = Get_Lang_Selection('ayt.YearTermNameB5', 'ayt.YearTermNameEN');
	}
	else {
		$sql_lang = 'ayt.YearTermNameEN';
	}
	
	$sql =  " SELECT DISTINCT
                  $sql_lang 
                FROM {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt
                INNER JOIN {$intranet_db}.ACADEMIC_YEAR AS ay
                  ON ayt.AcademicYearID = ay.AcademicYearID
                $cond
                Order by ayt.TermStart
              ";	
      $semester_arr = $this->returnVector($sql);
      
      for($i=0; $i<sizeof($semester_arr); $i++)
      {
        $short_sem = preg_replace($this->semester_patterns, "", $semester_arr[$i]);
        $short_semester_arr[] = trim($short_sem);
      }
              
  		return array($semester_arr, $short_semester_arr);
  	}
  	
  	function getSemesterNameArrFromIP2($AcademicYear="", $withLang='', $untilNow=false)
  	{
  		global $intranet_db, $intranet_session_language;
  		
  		$cond = "WHERE 1 ";
  		$cond .= ($AcademicYear=="") ? "" : "AND ( ay.YearNameEN = '$AcademicYear' OR ay.YearNameB5 = '$AcademicYear') ";
  		$cond .= ($untilNow) ? "AND NOW() > DATE(ayt.TermEnd) " : "";
  		if ($withLang) {
  			$sql_lang = Get_Lang_Selection('ayt.YearTermNameB5', 'ayt.YearTermNameEN');
  			$yr_lang = Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN');
  		}
  		else {
  			$sql_lang = 'ayt.YearTermNameEN';
  			$yr_lang = 'ay.YearNameEN';
  		}
  		$sql =  " SELECT
  						$yr_lang AS YearName, GROUP_CONCAT($sql_lang) AS YearTermName
		  		FROM {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt
		  		INNER JOIN {$intranet_db}.ACADEMIC_YEAR AS ay
		  		ON ayt.AcademicYearID = ay.AcademicYearID
		  		$cond
		  		GROUP BY $yr_lang
		  		Order by ayt.TermStart
  		";
  		$year_arr = $this->returnArray($sql);
  		$tmp_year_arr = array();
  		$short_semester_arr = array();
  		for($j=0; $j<sizeof($year_arr);$j++){
  			$yearName = $year_arr[$j]['YearName'];
  			$semester_arr = explode(",", $year_arr[$j]['YearTermName']);
  			$short_semester_arr[$yearName] = array();
  			$tmp_year_arr[$yearName] = $semester_arr;
  			for($i=0; $i<sizeof($semester_arr); $i++)
  			{
  				$short_sem = preg_replace($this->semester_patterns, "", $semester_arr[$i]);
  				$short_semester_arr[$yearName][] = trim($short_sem);
  			}
  		}
  		
  		return array($tmp_year_arr, $short_semester_arr);
  	}
  	
  	function getSemesterArrByAYId($AcademicYearId="", $withLang=''){
  		
  	  global $intranet_db, $intranet_session_language;
  
      $cond = ($AcademicYearId=="") ? "" : "WHERE ay.AcademicYearID = '$AcademicYearId'";
  
	  if ($withLang) {
		$sql_lang = Get_Lang_Selection('ayt.YearTermNameB5 as TermName', 'ayt.YearTermNameEN as TermName');
	  }
	  else {
		$sql_lang = 'ayt.YearTermNameEN as TermName';
	  }
	
	  $sql =  " SELECT DISTINCT
                  $sql_lang ,
					ayt.YearTermId as YearTermId
                FROM {$intranet_db}.ACADEMIC_YEAR_TERM AS ayt
                INNER JOIN {$intranet_db}.ACADEMIC_YEAR AS ay
                  ON ayt.AcademicYearID = ay.AcademicYearID
                $cond
              ";	
      $semester_arr = $this->returnResultSet($sql);
              
  	  return $semester_arr;
  	}
  	
   	## Get Year Name From IP
   	function getAcademicYearNameArrFromIP($singleLang='', $order='')
  	{
  		global $intranet_db, $intranet_session_language;
  		
  		if($singleLang){
  			$yearNameField = 'YearNameEN';
  		}
  		else{
	  		$yearNameField = Get_Lang_Selection('YearNameB5', 'YearNameEN');
  		}
  		if($order==1){
  			$orderBy = "Order By Sequence asc";
  		}
  		
      $sql =  " SELECT DISTINCT $yearNameField FROM {$intranet_db}.ACADEMIC_YEAR $orderBy";
      $year_arr = $this->returnVector($sql);
      
  		return $year_arr;
  	}
  	
  	# Change 2D array to 1D with the first column as index
  	function build_assoc_array($array, $emptyname="build_assoc_array_null")
  	{
  			 for ($i=0; $i<sizeof($array); $i++)
  			 {
  				  list($id,$data) = $array[$i];
  				  $id = trim($id);
  				  $data = trim($data);
  				  if ($id == "")
  				  {
  					  $id = $emptyname;
  				  }
  				  $result[$id] = $data;
  			 }
  			 return $result;
  	}
  	
  	/*
  	* Modified by key 2008-09-29: set the child id to session $ck_children_id
  	*/
  	function SET_CURRENT_CHILD_ID_BY_SESSION($Student_ID)
  	{
  		global $ck_current_children_id;
  		$ck_current_children_id = $Student_ID;
  		session_register_intranet("ck_current_children_id", $ck_current_children_id);
  	} // end function
  
  	/*
  	* Modified by key 2008-09-29: Copy function from /home/eclass30/eclass30/src/includes/php/lib-courseinfo.php
  	*/
  	function registerEClassChildren($course_id, $user_email)
  	{
  		global $intranet_db, $ck_children_ids, $ck_children_intranet_ids;
  
  		$course_db = $this->ipf_db;
  		$ck_children_ids = "";
  
  		$sql = "SELECT
  					ip.StudentID
  				FROM
  					{$intranet_db}.INTRANET_PARENTRELATION AS ip,
  					{$intranet_db}.INTRANET_USER AS iu
  				WHERE
  					UserEmail='$user_email' AND ip.ParentID=iu.UserID ";
  					
  		$row = $this->returnVector($sql);
  		if (sizeof($row)>0)
  		{
  			$children_ids = implode(",", $row);
  			$ck_children_intranet_ids = $children_ids;
  			$sql = "SELECT
  						um.user_id
  					FROM
  						{$course_db}.usermaster AS um,
  						{$intranet_db}.INTRANET_USER AS iu
  					WHERE
  						iu.UserID IN ($children_ids) AND um.User_Email=iu.UserEmail AND (um.status IS NULL OR um.status NOT IN ('deleted'))";
  			$row2 = $this->returnVector($sql);
  			if (sizeof($row2)>0)
  			{
  				$ck_children_ids = " ".implode(" , ", $row2)." ";
  			}
  		}
  		
  		session_register_intranet("ck_children_ids", $ck_children_ids);
  		session_register_intranet("ck_children_intranet_ids", $ck_children_intranet_ids);
  
  		return;
  	} // end function
  	
  	/*
  	* Modified by key 2008-09-29: Get the children info by session $ck_children_intranet_ids
  	*/
  	function GET_CHILDREN_INFO()
  	{
  		global $intranet_db, $eclass_db, $ck_children_intranet_ids;
  		
  		$ClassNumField = getClassNumberField("iu.");
  		// TABLE SQL
  		$fieldname .= "iu.UserID, ";
  		$fieldname .= "iu.ClassName, ";
  		$fieldname .= "iu.ClassNumber, ";
  		$fieldname .= "iu.EnglishName, ";
  		$fieldname .= "iu.ChineseName ";
  
  		$sql  = "SELECT 
  			$fieldname 
  		FROM 
  			{$intranet_db}.INTRANET_USER AS iu 
  			LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps ON ps.UserID=iu.UserID
  		WHERE iu.RecordType='2' 
  			AND ps.WebSAMSRegNo IS NOT NULL 
  			AND ps.UserKey IS NOT NULL 
  			AND ps.IsSuspend = 0
  			AND ps.UserID IN ($ck_children_intranet_ids)
  		ORDER BY
  			iu.ClassName,
  			$ClassNumField
  		";
  		$ChildrenArray = $this->returnArray($sql);
  		
  		return $ChildrenArray;
  	} // end function
  	
  
  	
  	
  	/**********************************************************/
  	/************** To be moved to utility class **************/
  	/**********************************************************/
    function GetYearClassTitleFieldByLang($prefix="")
    {
      $firstChoice = Get_Lang_Selection($prefix."ClassTitleB5", $prefix."ClassTitleEN");
      $altChoice = Get_Lang_Selection($prefix."ClassTitleEN", $prefix."ClassTitleB5");
      
      $year_class_title_field = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '', $altChoice, $firstChoice)";
    
      return $year_class_title_field;
    }
    
    /**
     * @param String $starttime datetime in format YYYY-MM-DD HH-mm-ss
     * @param String $endtime datetime in format YYYY-MM-DD HH-mm-ss
     * @return	true - time period is validated, vice versa
     */
    function isValidPeriod($starttime="",$endtime="") {
    	
      return (($starttime!="" && $starttime!="0000-00-00 00:00:00") || ($endtime!="" && $endtime!="0000-00-00 00:00:00")) ? validatePeriodByTimeStamp($starttime, $endtime) : 1;
    }
    
    function isStudentSubmitOLESetting ($ParIntExt=0, $ParMemberType="", $program_id="", $settingsArray="") 
    {
	    global $eclass_db;
	    
	    $self_start = "";
	    $self_end = "";
		if($program_id)
		{
			$sql = "select CanJoinStartDate, CanJoinEndDate from $eclass_db.OLE_PROGRAM where ProgramID=$program_id";
			$result = $this->returnArray($sql);
			list($self_start, $self_end) = $result[0];
			$self_start = $self_start=="0000-00-00" ? "" : $self_start . " 00:00:00";
			$self_end = $self_end=="0000-00-00" ? "" : $self_end . " 23:59:59";
		}
		
		if($self_start=="" && $self_end=="")
		{
			if(is_array($settingsArray)){
				$ValidPeriod = $this->isValidPeriod($settingsArray['StartDateTime'], $settingsArray['EndDateTime']);
				if (($ValidPeriod==1 && $settingsArray['AllowSubmit']) && $ParMemberType!="P") {
					return true;
				} else {
					return false;
				}
			}
			
			$submissionTimeArray = $this->getDataFromStudentOleConfig();
			
			$starttime = $submissionTimeArray["STARTTIME_TO_MINS"];
			$endtime = $submissionTimeArray["ENDTIME_TO_MINS"];
			$ValidPeriod = $this->isValidPeriod($starttime,$endtime);
			
			$ex_starttime = $submissionTimeArray["EX_STARTTIME_TO_MINS"];
			$ex_endtime = $submissionTimeArray["EX_ENDTIME_TO_MINS"];
			$ex_ValidPeriod = $this->isValidPeriod($ex_starttime,$ex_endtime);
			
			//ONCE $ParIntExt != 1 , THE PROGRAM IS INT
			if ((($ex_ValidPeriod==1 && $ParIntExt == 1 && $submissionTimeArray["EXT_ON"] == "on")
				 || ($ValidPeriod==1 && $ParIntExt != 1 && $submissionTimeArray["INT_ON"] == "on"))
				 && $ParMemberType!="P") {
				return true;
			} else {
				return false;
			}
		}
		else
		{
			return $this->isValidPeriod($self_start,$self_end);
		}
    }
	
    /**
  	 * @return Array OLE configuration from student_ole_config.txt
  	 */
  	function getDataFromStudentOleConfig() {
  		global $eclass_root,$ipf_cfg;
  		$student_ole_config_file = "$eclass_root/files/student_ole_config.txt";
  		$filecontent = trim(get_file_content($student_ole_config_file));
  		list($starttime, $sh, $sm, $endtime, $eh, $em, $int_on, $ext_on, $ex_starttime, $ex_sh, $ex_sm, $ex_endtime, $ex_eh, $ex_em, $temp) = unserialize($filecontent);

		$objIpfSetting = iportfolio_settings::getInstance();
		
		$SetRecordToSlpPeriod = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriod"]);
		$SetRecordToSlpPeriodIsAllowed = $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriodAllowSubmit"]);

		$SetRecordToSlpPeriodIsAllowedArr =  explode("##", $SetRecordToSlpPeriod);

		if(is_array($SetRecordToSlpPeriodIsAllowedArr))
		{
			$StartTimeDetailArr =explode(" ", $SetRecordToSlpPeriodIsAllowedArr[0]);
			$SetSLPPeriod_starttime = $StartTimeDetailArr[0]; 
			$StartTimeArr =explode(":", $StartTimeDetailArr[1]);
			$SetSLPPeriod_sh = $StartTimeArr[0];
			$SetSLPPeriod_sm = $StartTimeArr[1];
			
			$EndTimeDetailArr =explode(" ", $SetRecordToSlpPeriodIsAllowedArr[1]);
			$SetSLPPeriod_endtime = $EndTimeDetailArr[0]; 
			$EndTimeArr =explode(":", $EndTimeDetailArr[1]);
			$SetSLPPeriod_eh = $EndTimeArr[0];
			$SetSLPPeriod_em = $EndTimeArr[1];
		}

		
  		$starttimeUpToMins = (trim($starttime)!="") ? sprintf($starttime." %02s:%02s:00", $sh, $sm) : "";
  		$endtimeUpToMins = (trim($endtime)!="") ? sprintf($endtime." %02s:%02s:00", $eh, $em) : "";
  		$returnArray["STARTTIME"] = $starttime;
  		$returnArray["STARTTIME_TO_MINS"] = $starttimeUpToMins;
  		$returnArray["SH"] = $sh;
  		$returnArray["SM"] = $sm;
  		$returnArray["ENDTIME"] = $endtime;
  		$returnArray["ENDTIME_TO_MINS"] = $endtimeUpToMins;
  		$returnArray["EH"] = $eh;
  		$returnArray["EM"] = $em;
  		$returnArray["INT_ON"] = $int_on;
  		
  		$check_SetSLPPeriod = $SetRecordToSlpPeriodIsAllowed;
//  	$returnArray["SetSLPPeriod_starttime"] = $SetSLPPeriod_starttime;
//		$returnArray["SetSLPPeriod_sh"] = $SetSLPPeriod_sh;
//		$returnArray["SetSLPPeriod_sm"] = $SetSLPPeriod_sm;
//		$returnArray["SetSLPPeriod_endtime"] = $SetSLPPeriod_endtime;
//		$returnArray["SetSLPPeriod_eh"] = $SetSLPPeriod_eh;
//		$returnArray["SetSLPPeriod_em"] = $SetSLPPeriod_em;
//		$returnArray["check_SetSLPPeriod"] = $check_SetSLPPeriod;
//		
		$starttimeUpToMins = (trim($SetSLPPeriod_starttime)!="") ? sprintf($SetSLPPeriod_starttime." %02s:%02s:00", $SetSLPPeriod_sh, $SetSLPPeriod_sm) : "";
  		$endtimeUpToMins = (trim($SetSLPPeriod_endtime)!="") ? sprintf($SetSLPPeriod_endtime." %02s:%02s:00", $SetSLPPeriod_eh, $SetSLPPeriod_em) : "";
		$returnArray["SetSLPPeriod_STARTTIME"] = $SetSLPPeriod_starttime;
  		$returnArray["SetSLPPeriod_STARTTIME_TO_MINS"] = $starttimeUpToMins;
  		$returnArray["SetSLPPeriod_SH"] = $SetSLPPeriod_sh;
  		$returnArray["SetSLPPeriod_SM"] = $SetSLPPeriod_sm;
  		$returnArray["SetSLPPeriod_ENDTIME"] = $SetSLPPeriod_endtime;
  		$returnArray["SetSLPPeriod_ENDTIME_TO_MINS"] = $endtimeUpToMins;
  		$returnArray["SetSLPPeriod_EH"] = $SetSLPPeriod_eh;
  		$returnArray["SetSLPPeriod_EM"] = $SetSLPPeriod_em;
  		$returnArray["SetSLPPeriod_ON"] = $check_SetSLPPeriod;
		  		
	
  		$returnArray["EXT_ON"] = $ext_on;
  		///////////////////////////////////////////////////////////////////////////
  		// if $ex_starttime == NULL, (Start time is a must item when submitting the time, hence use this to check)
  		// that means it is older version 
  		// that does not support OLE submission time and External OLE separately
  		///////////////////////////////////////////////////////////////////////////
  		if (isset($ex_starttime)) {	// separated time limit settings
	  		$ex_starttimeUpToMins = (trim($ex_starttime)!="") ? sprintf($ex_starttime." %02s:%02s:00", $ex_sh, $ex_sm) : "";
	  		$ex_endtimeUpToMins = (trim($ex_endtime)!="") ? sprintf($ex_endtime." %02s:%02s:00", $ex_eh, $ex_em) : "";
	  		$returnArray["EX_STARTTIME"] = $ex_starttime;
	  		$returnArray["EX_STARTTIME_TO_MINS"] = $ex_starttimeUpToMins;
	  		$returnArray["EX_SH"] = $ex_sh;
	  		$returnArray["EX_SM"] = $ex_sm;
	  		$returnArray["EX_ENDTIME"] = $ex_endtime;
	  		$returnArray["EX_ENDTIME_TO_MINS"] = $ex_endtimeUpToMins;
	  		$returnArray["EX_EH"] = $ex_eh;
  			$returnArray["EX_EM"] = $ex_em;
  		} else {	// single time limit settings
  			$returnArray["EX_STARTTIME"] = $returnArray["STARTTIME"];
	  		$returnArray["EX_STARTTIME_TO_MINS"] = $returnArray["STARTTIME_TO_MINS"];
	  		$returnArray["EX_SH"] = $returnArray["SH"];
	  		$returnArray["EX_SM"] = $returnArray["SM"];
	  		$returnArray["EX_ENDTIME"] = $returnArray["ENDTIME"];
	  		$returnArray["EX_ENDTIME_TO_MINS"] = $returnArray["ENDTIME_TO_MINS"];
	  		$returnArray["EX_EH"] = $returnArray["EH"];
  			$returnArray["EX_EM"] = $returnArray["EM"];
  		}
  		
//  		debug_r($returnArray);
  		return $returnArray;
  	}
  	
	//A variable (StudentClassID) to store a student classID , for checking access right (eg teacher allow to edit / view this student or class)
	function setStudentClassID($ParStudentClassID)
	{
		$this->StudentClassID = $ParStudentClassID;
	}

	//Return $StudentClassID ;
	function getStudentClassID()
	{
		return $this->StudentClassID;
	}

	/**
	 * Check if need to turn on the function of approval in self account
	 * @return boolean true, need v.v.
	 */
  	function isUseApprovalInSelfAccount() {
  		global $sys_custom;	// temparate hardcoded return this variable, may be enhcance later to read config file
  		return $sys_custom['cwk_SLP'];
  	}
  	
  	
  	
  	
  	function Get_Student_With_iPortfolio($YearClassIDArr, $IsSuspend='', $withJupasAppNoOnly=false)
  	{
		global $eclass_db;
		
		if ($YearClassIDArr != '')
		{
			if (!is_array($YearClassIDArr))
				$YearClassIDArr = array($YearClassIDArr);
			$cond_YearClassID = " And yc.YearClassID In (".implode(',', $YearClassIDArr).") ";
		}
		
		if ($IsSuspend !== '') {
			$cond_IsSuspend = " And ps.IsSuspend = '".$IsSuspend."' ";
		}
		
		if ($withJupasAppNoOnly) {
			$cond_HKJApplNo = " And (u.HKJApplNo != '' And u.HKJApplNo is not null) ";
		}
		
		$NameField = getNameFieldByLang2('u.');
		$ArchiveNameField = getNameFieldByLang2('au.');
		
		$PORTFOLIO_STUDENT = $eclass_db.'.PORTFOLIO_STUDENT';
		$sql = "Select 
						ycu.UserID, 
						ycu.ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('<font style=\"color:red;\">*</font>', $ArchiveNameField) 
							WHEN u.RecordStatus = 3  THEN CONCAT('<font style=\"color:red;\">*</font>', $NameField) 
							ELSE $NameField 
						END as StudentName, 
						CASE 
							WHEN au.UserID IS NOT NULL then '1' 
							ELSE '0'
						END as ArchiveUser,
						yc.ClassTitleEN,
						yc.ClassTitleB5,
						If (ps.IsSuspend = 0 And ps.WebSAMSRegNo Is Not Null And ps.UserKey Is Not Null, 1, 0) as IsPortfolioActivated,
						u.HKJApplNo as JupasApplicationNumber,
						u.WebSAMSRegNo as WebSAMSRegNo
				From 
						YEAR_CLASS_USER as ycu
						Inner Join
						YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						LEFT JOIN
						$PORTFOLIO_STUDENT as ps On (ycu.UserID = ps.UserID)
						LEFT JOIN 
						INTRANET_USER as u ON (ycu.UserID = u.UserID)
						LEFT JOIN 
						INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID) 
				Where 
						1
						$cond_YearClassID
						$cond_IsSuspend
						$cond_HKJApplNo
				Order by 
						yc.Sequence, ycu.ClassNumber
				";
		return $this->returnArray($sql);
	}
	
	
	//Move iPortfolio Admin from grouping_function to Role
	function Move_iPF_Admin_from_to_Role(){
		global $PATH_WRT_ROOT, $intranet_db, $eclass_db;
		
		include_once($PATH_WRT_ROOT."includes/libeclass40.php");
				
		//Create Role 'iPortfolio Admin'
		$sql = "insert into {$intranet_db}.ROLE (RoleName, DateInput, InputBy, DateModified, ModifyBy) 
				values('iPortfolio Admin', NOW(), 1, NOW(), 1)";
		$this->db_db_query($sql);
		
		//add the right 'other-iPortfolio' to the role 'iPortfolio Admin' 
		$sql = "insert into {$intranet_db}.ROLE_RIGHT (RoleID, FunctionName, RightFlag, DateInput, InputBy, DateModified, ModifyBy) 
				select RoleID, 'other-iPortfolio', 1, NOW(), 1, NOW(), 1 from {$intranet_db}.ROLE 
				where RoleName='iPortfolio Admin'";
		$this->db_db_query($sql);
		
		//get the iPF course id
		$iPFCourseID = getEClassRoomID($iPFRoomType = 4);
		$objEclass = new libeclass($iPFCourseID);
		$course_db = $objEclass->db_prefix."c".$objEclass->course_id;
		
		//copy the admin from ipf admin table to ROLE_MEMBER
		$sql = "insert ignore into {$intranet_db}.ROLE_MEMBER        
				select distinct r.RoleID, i.UserID, NOW(), 1, NOW(), 1,         
				CASE
					WHEN i.RecordType = '2' then 'Student'
					WHEN i.RecordType = '3' then 'Parent'
					WHEN i.RecordType = '1' and i.Teaching = '1' then 'Teaching' 
					WHEN i.RecordType = '1' and (i.Teaching <> '1' or i.Teaching IS NULL) then 'NonTeaching' 
					ELSE 'Alumni' 
				END as IdentityType       
				from {$eclass_db}.user_course uc 
				left join {$course_db}.grouping_function cipf on uc.user_id = cipf.group_id 
				left join {$intranet_db}.INTRANET_USER i on i.UserEmail  = uc.user_email 
				left join {$intranet_db}.ROLE r on r.RoleName = 'iPortfolio Admin' 
				where cipf.function_id = 1 and cipf.function_type='RIGHTS' and uc.course_id = '".$iPFCourseID."' ";
		$this->db_db_query($sql);
	}
	
	//$objLibUser is a object of libuser
	function showJUPASForStudent($objLibUser){
		global $PATH_WRT_ROOT, $ipf_cfg, $intranet_root;
		$showJupasForStudent = false;

		include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
		$liboea = new liboea();
		
		if ($liboea->isEnabledOEAItem() || $liboea->isEnabledAdditionalInfo()) {
			$tmpID = $objLibUser->Get_User_Studying_Form();
			$yearID = intval($tmpID[0][0]);
			if(is_int($yearID) && $yearID > 0){
				//include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
				//$objYear = new Year($yearID);
				//$studentForm = $objYear->WEBSAMSCode;
	
				
				$JupasApplicableFormIDArr = $liboea->getJupasApplicableFormIDArr();
	
				if(($yearID != "") && in_array($yearID, $JupasApplicableFormIDArr)){
					$showJupasForStudent = true;
				}
			}
		}
		
		return $showJupasForStudent;
	}

	//return BOOLEAN
	function showSLPReportPrintingForStduent($objLibUser){
		global $PATH_WRT_ROOT, $ipf_cfg, $intranet_root;
		$showSLPReportPrintingForStduent = false;
		include_once($intranet_root."/includes/portfolio25/lib-portfolio_settings.php");

		$objIpfSetting = iportfolio_settings::getInstance();
		$reportIsAllowed		= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpIsAllowed"]);
		if($reportIsAllowed == 1){
			$tmpID = $objLibUser->Get_User_Studying_Form();
			$yearID = intval($tmpID[0][0]);
			if(is_numeric($yearID) && $yearID > 0){
				$_reportFormAllow	= $objIpfSetting->findSetting($ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["studentPrintSlpFormAllowed"]);

				$_reportFormAllowInfoAry = explode(',',$_reportFormAllow);

				if(in_array($yearID,$_reportFormAllowInfoAry)){

					$showSLPReportPrintingForStduent = true;
				}
			}				
		}
		return $showSLPReportPrintingForStduent;
	}
	
	function showDynamicReportPrintingForStudent($objLibUser) {
		global $eclass_db;
		
		$sql = "Select record_id From {$eclass_db}.student_report_generated_record Where record_status = 1 and student_id = '".$objLibUser->UserID."'";
		$resultAry = $this->returnResultSet($sql);
		
		return (count((array)$resultAry) > 0)? true : false;
	}
	
	function showNgWahSASReport($objLibUser){
		global $PATH_WRT_ROOT, $intranet_db, $eclass_db;
		
		# Get Class Name
		$className = trim($objLibUser->ClassName);
		
		$sql = "Select YearID From {$intranet_db}.YEAR_CLASS Where ClassTitleEN = '".$className."' AND AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
		$resultAry = $this->returnVector($sql);
		
		# Get Form
		$currentForm = $resultAry[0];
		
		$sql = "Select SettingValue From {$eclass_db}.OLE_SAS_SETTINGS Where SettingName = 'release' AND AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
		$resultAry = $this->returnVector($sql);
		
		$sas_release_date = explode(';;', $resultAry[0]);
		if(count($sas_release_date) > 0)
		{
			for($i = 0; $i<count($sas_release_date); $i++){
				$stored_value = explode('###', $sas_release_date[$i]);
				
				if($currentForm != $stored_value[0])
				{
					continue;
				} 
				# within allow submit period
				else 
				{
					$today = date('Y-m-d');
					$target_startdate = explode(' ', $stored_value[1]);
					$target_enddate = explode(' ', $stored_value[2]);
					
					# return
					return validatePeriod($stored_value[1], $stored_value[2]);		
				}
			}
		}
		return false;
	}
	
	//$objLibUser is a object of libuser
	function showJUPASForTeacher($ParUID){
		global $PATH_WRT_ROOT, $ipf_cfg, $intranet_root;
		$showJupasForTeacher = false;

		include_once($PATH_WRT_ROOT."includes/portfolio25/oea/liboea.php");
		$liboea = new liboea();
		$JupasApplicableFormIDArr = $liboea->getJupasApplicableFormIDArr();

		include_once($PATH_WRT_ROOT."includes/libteaching.php");
		$objTeaching = new libteaching();
		$teacherLevel = $objTeaching->returnTeacherClassWithLevel($ParUID);

		for($i =0,$i_max = count($teacherLevel);$i<$i_max; $i++){
			$_levelID = $teacherLevel[$i]['ClassLevelID'];
			if(is_numeric($_levelID) && ($_levelID > 0) && in_array($_levelID,$JupasApplicableFormIDArr)) {
				$showJupasForTeacher = true;
				break;
			}
		}
		return $showJupasForTeacher;
	}

	function deleteStudentCreatedProgram($programID , $creatorID){
		global $eclass_db,$ipf_cfg;

		if($programID  == '' || $creatorID == ''){
			return;
		}

		if(!is_array($programID)){
			$programIDAry = array($programID);
		}else{
			$programIDAry = $programID;
		}

		$programIDStr = implode(',',$programIDAry);

		//get those programid in OLE_PROGRAM that without any student associated with it
		//1) program id in the delete list (p.programid in('.$programIDStr.'))
		//2) no OLE_STUDENT associated with it (s.programid IS NULL )
		//3) the program must be come from student input (p.ComeFrom = '.$ipf_cfg["OLE_PROGRAM_COMEFROM"]["studentInput"].')
		//4) the program is created by the student(p.CreatorID = '.$creatorID.')

		$sql = 'select p.programid from '.$eclass_db.'.OLE_PROGRAM as `p` left join '.$eclass_db.'.OLE_STUDENT as `s` on p.programid = s.programid  where p.programid in ('.$programIDStr.') and s.programid IS NULL and (p.ComeFrom = '.$ipf_cfg["OLE_PROGRAM_COMEFROM"]["studentInput"].') and (p.CreatorID = '.$creatorID.')';

		$objDB = new libdb();
		$result = $objDB->returnVector($sql);

		if(is_array($result) && sizeof($result) > 0){
			$removeProgramIdList = implode(',',$result);

			$sql = 'delete from '.$eclass_db.'.OLE_PROGRAM where programid in ('.$removeProgramIdList.')';

			$deleteResult = $objDB->db_db_query($sql);
		}	

	}
	/* may need in the future 20110809
	function getProfileFunction(){

		//$student_profile_functions declare in /home/web/eclass40/intranetIP25/includes/portfolio25/iPortfolioConfig.inc.php
		global $student_profile_functions,$Lang;

		return $student_profile_functions;

	}*/
	function backToIPHome(){
		global $Lang;
		$msg = <<<HTML
			<script>
			alert("{$Lang['iPortfolio']['noAccessRightRedirectMsg']}");
			document.write("{$Lang['iPortfolio']['redirecting']}");
			window.location = '/home/index.php';
			</script>		
HTML;
		return $msg;
	}
	
	function displaySelfAccountForHTML($details){
		return str_replace("  ","&nbsp;&nbsp;",$details);
		
	}
	
	function spaceIndentationForPDF($str){
		//$ar is a temporary array
	/*	global $UserID;
		if($UserID==2650){
			//$str = preg_replace('/^[ ]{1}/','&nbsp;',$str);
			//$ss = preg_split("/./us", "??);
			//$ss = join('',array_reverse($ss)); 
			
			$ar = preg_split("//u", $str);
			$str = join('',array_reverse($ar));debug_r(htmlentities($str));
			$str = str_replace('    ', "??, $str);
			$ar = preg_split("//u", $str);
			$str = join('',array_reverse($ar)); 
			$str = str_replace('   ', "<span style=\"font-size:75%\">??/span>", $str);
			$str = str_replace('  ', "<span style=\"font-size:50%\">??/span>", $str);
			debug_r($str);
			
			
			exit();
		}else{
		preg_match_all('/./us', $str, $ar);
		$str = join('',array_reverse($ar[0]));
		$str = str_replace('    ', strrev('<span> </span><span> </span><span> </span><span> </span>'), $str);
		preg_match_all('/./us', $str, $ar);
		$str = join('',array_reverse($ar[0]));
		$str = str_replace('   ', '<span> </span><span> </span><span> </span>', $str);
		$str = str_replace('  ', '<span> </span><span> </span>', $str);
		}
		$str = str_replace('  ', '<span> </span><span> </span>', $str);*/
		preg_match_all('/./us', $str, $ar);
		$str = join('',array_reverse($ar[0]));
		$str = str_replace('    ', '　', $str);
		preg_match_all('/./us', $str, $ar);
		$str = join('',array_reverse($ar[0]));
		
		$str = str_replace('   ', '<span style="font-size:75%">　</span>', $str);
		$str = str_replace('  ', '<span style="font-size:50%">  </span>', $str);		
		$str = str_replace('　　', '<span>&nbsp;&nbsp;</span>', $str);
		$str = str_replace('        ', '<span style="font-size:50%">&nbsp;</span>', $str);
		$str = str_replace('      ', '<span style="font-size:50%">&nbsp;</span>', $str);
		$str = str_replace('    ', '<span style="font-size:50%"> </span>', $str);
		$str = str_replace('    ', '&nbsp;', $str);
		$str = str_replace('  ', '&nbsp;', $str);
		$str = str_replace('       ', '&nbsp;', $str);
		$str = str_replace('　', '', $str);
		
		return $str;
		
// 		$str = str_replace('　', '&nbsp;', $str);
// 		$str = '<table width="98%">
//                     <tr valign="top">
//                         <td>'.$str.'</td>
//                      </tr>
//                 </table>';
		
		// test for #S133387  
// 		$str = str_replace(array('<br>', '<br/>'), '<br />', $str);
// 		$strAry = explode("<br />", $str);
// 		$numOfSentence = count($strAry);
		
// 		$strFinal = '';
// 		$numOfCharPerLine = 65;
// 		for ($i=0; $i<$numOfSentence; $i++) {
// 		    $_str = trim($strAry[$i]);
		    
// 		    $_numOfSpace = substr_count($_str, "　");
// 		    $_strFirstLine = mb_substr($_str, 0, $numOfCharPerLine-$_numOfSpace, "UTF-8");
// 		    //debug_pr($_strFirstLine);
		    
// 		    if ($_str == $_strFirstLine) {
// 		        // sentenance that can display in one line
// 		        $strFinal .= $_str.'<br />';
// 		    }
// 		    else {  
// 		        // sentenance that cannot displayed in one line => count the wordings and then force a line break because TCPDF cannot determine the width of "&nbsp;"
// 		        $strFinal .= str_replace($_strFirstLine, $_strFirstLine.'<br />', $_str).'<br />';
// 		    }
// 		}
// 		$strFinal = str_replace('　', '&nbsp;', $strFinal);
		
// 		return $strFinal;
	}
	
	
	function stringLanguageSplitByWords($ParInputStr, $ParType='1', $ParLang='EN'){
		//pattern of a word contains other than letters, numbers and symbols
		$Pattern = '/\s*[\w[:punct:]]*[^[:punct:]\w\s]+[\S]*/';
		$ParLang = strtoupper($ParLang);
		$posAry = array();
		
		preg_match_all($Pattern, $ParInputStr, $match, PREG_OFFSET_CAPTURE);
		//find the position of the word matches the pattern
		foreach($match[0] as $val){
			$posAry[] = array($val[1],strlen($val[0]));
		}
		//if no matches, assumes input string is pure English
		if(empty($posAry)){
			if($ParLang=='EN'){
				$resultAry = array($ParInputStr,'');
			}else{
				$resultAry = array('',$ParInputStr);
			}
		}else{
			//split the string according to language
			$dePos = $posAry[count($posAry)-1][0]+$posAry[count($posAry)-1][1];
			$resultAry = array();
			
			if(in_array(0,$posAry[0])){//English word at the back
				$resultAry =  array(trim(substr($ParInputStr,0,$dePos)),trim(substr($ParInputStr,$dePos)));
				if($ParLang=='EN'){
					$resultAry = array_reverse($resultAry);
				}
			}else{//English word in the front
				$resultAry =  array(trim(substr($ParInputStr,0,$posAry[0][0])),trim(substr($ParInputStr,$posAry[0][0])));
				if($ParLang!='EN'){
					$resultAry = array_reverse($resultAry);
				}
			}
		}
		//$ParType = 1 returns array
		if($ParType=='1'){
			return $resultAry;
		}
		//other returns html
		return implode("<br/>",array_filter($resultAry));
	}
	
	function insertIntoIntranetUser($intranetUserId){
		global $intranet_db;
		$objDB = new libdb();


		//where sql case 1) i.userid is NULL ==> there is no userid exist in the INTRANET_USER
		$sql = 'select 
					 	a.UserID as `UserID`,
						a.UserLogin as `UserLogin`,
						a.UserPassword as `UserPassword` ,
						a.HashedPass as `HashedPass`,
						a.EnglishName as `EnglishName`,
						a.ChineseName as `ChineseName`,
						i.userid as `i_UserID`,
						a.ClassName, 
						a.ClassNumber
				from '.$intranet_db.'.INTRANET_ARCHIVE_USER as a 
					 left join '.$intranet_db.'.INTRANET_USER as i on i.userid = a.userid 
				where 
					a.userid = \''.$intranetUserId.'\' and i.userid is NULL';


//error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		$rs = $objDB->returnResultSet($sql);

		for($i = 0,$i_max = count($rs);$i < $i_max; $i++){
			$_userId = $rs[$i]['UserID'];
			$_userLogin = 'tmpUserLogin_'.$_userId;
			$_userEmail = 'tmpUserEmail_'.$_userId.'@tmp.com';

			$sql = 'insert into '.$intranet_db.'.INTRANET_USER
						(UserID,UserLogin,UserEmail,EnglishName,ChineseName,WebSAMSRegNo, ClassName, ClassNumber, DateOfBirth, HKID, Gender)
						select 
							a.UserID , \''.$_userLogin.'\', \''.$_userEmail.'\',a.EnglishName, a.ChineseName, a.WebSAMSRegNo,a.ClassName, a.ClassNumber, DateOfBirth, HKID, Gender
						from 
							'.$intranet_db.'.INTRANET_ARCHIVE_USER as a 
						where 
							a.userid ='.$_userId;
			$insertRs = $objDB->db_db_query($sql);
//error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
			return $insertRs;
		}
	}
	function removeFromIntranetUser($intranetUserId){
		global $intranet_db;
		
		$objDB = new libdb();
		$_userId = $intranetUserId;
		$_userLogin = 'tmpUserLogin_'.$intranetUserId;
		$_userEmail = 'tmpUserEmail_'.$intranetUserId.'@tmp.com';

		//those tmp User Login make sure don't delete exist INTRANET_USER USER
		$delSQL = 'delete from '.$intranet_db.'.INTRANET_USER where UserID ='.$_userId.' and UserLogin = \''.$_userLogin.'\' and UserEmail = \''.$_userEmail.'\'';
		$delResult = $objDB->db_db_query($delSQL);

		return $delResult;
	}
	
	function returnStudentWithPortfolioAccount($sid)
	{
		global $eclass_db, $intranet_db;

		$ClassNumberField = getClassNumberField("iu.");
		$sql = "SELECT count(*) 
				FROM
					{$intranet_db}.INTRANET_USER AS iu
					LEFT JOIN {$eclass_db}.PORTFOLIO_STUDENT AS ps ON ps.UserID=iu.UserID
				WHERE
					ps.WebSAMSRegNo IS NOT NULL
					AND ps.UserKey IS NOT NULL
					AND ps.IsSuspend = 0 
					and iu.UserID = '".$sid."'
				";
		$row = $this->returnVector($sql);
		return $row[0];
	}	
	
	function returnStudentLicenseSummary($types=0){
		
		global $intranet_db, $eclass_db;
		
		$objDB = new libdb();
		
		// Student: RecordType = 2 (INTRANET_USER)
		// Active: RecordStatus = 1
		// Suspanded: RecordStatus = 0
		// Left: RecordStatus = 3
		// Alumni: RecordType = 4
		
		// Query Conditions
		$active_con = "u.RecordStatus = 1 AND";
		$inactive_con = "(u.RecordStatus = 0 || u.RecordStatus = 3) AND";
		$student_con = "u.RecordType = 2 AND";
		$currentAcaYear_con = "yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND";
		
		$PORTFOLIO_STUDENT = $eclass_db.'.PORTFOLIO_STUDENT';
		
		// Definition of Types
		// 0: Get Number Summmary 
		// Type 1-5 for student details in addon
		// 1: Active Students with Class
		// 2: Inactive Students with Class
		// 3: Active Students without Class
		// 4: Inactive Students without Class
		// 5: Alumni
		
		$query_first_target = "u.UserID";
		$query_second_target = "u.UserID";
		
		// Get details for students with class or alumni
		if($types == 1 || $types == 2 || $types == 5){
			$query_first_target = "yc.ClassTitleEN, ycu.ClassNumber, u.EnglishName, u.UserID";
		}
		// Get details for students without class
		if($types == 3 || $types == 4){
			$query_second_target = "u.EnglishName, u.UserID";
		}
		
		$student_WithClass_Active_sql = 
					"SELECT
		             	$query_first_target
		             FROM
		                INTRANET_USER AS u
						Inner Join {$eclass_db}.PORTFOLIO_STUDENT AS ps On u.UserID = ps.UserID
		              	INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu ON u.UserID = ycu.UserID
		              	INNER JOIN {$intranet_db}.YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
		             WHERE
		                $student_con
		                $active_con 
						$currentAcaYear_con
						1
					 GROUP BY
					 	u.UserID
					 ORDER BY
						yc.ClassTitleEN, ycu.ClassNumber
					";
					
		if($types == 1){
			// Get four details
			$student_active = $objDB->returnArray($student_WithClass_Active_sql);
		} else{
			// Get UserID only
			$student_active = $objDB->returnVector($student_WithClass_Active_sql);
			$result[] = count($student_active);	
		}
		
		$student_WithClass_InActive_sql = 
				   "SELECT
		              $query_first_target
		            FROM
		                INTRANET_USER AS u
						Inner Join {$eclass_db}.PORTFOLIO_STUDENT AS ps On u.UserID = ps.UserID
		              	INNER JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu ON u.UserID = ycu.UserID
		              	INNER JOIN {$intranet_db}.YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
		            WHERE
		                $student_con
		                $inactive_con 
						$currentAcaYear_con
						1
		 			GROUP BY
						u.UserID 
					ORDER BY
						yc.ClassTitleEN, ycu.ClassNumber
					";
					
		if($types == 2){
			$student_inactive = $objDB->returnArray($student_WithClass_InActive_sql);
		} else{
			$student_inactive = $objDB->returnVector($student_WithClass_InActive_sql);
			$result[] = count($student_inactive);
		}			
			
		// Inactive students
		if($types == 0 || $types == 3 || $types == 4){
			
			// Condition For student without class
			if($result[0] > 0){
				$withoutclass_active_con = "u.UserID NOT IN (".implode(",", $student_active).")";
			} else {
				$withoutclass_active_con = "1";
			}
			if($result[1] > 0){
				$withoutclass_inactive_con = "u.UserID NOT IN (".implode(",", $student_inactive).")";
			} else {
				$withoutclass_inactive_con = "1";
			}
			
			$student_WithoutClass_Active_sql = 
						"SELECT
			               $query_second_target
						 FROM
			                INTRANET_USER AS u
							Inner Join {$eclass_db}.PORTFOLIO_STUDENT AS ps On u.UserID = ps.UserID     
			 			 WHERE
			                $student_con
			                $active_con        
							$withoutclass_active_con
			 			GROUP BY
							u.UserID	
						";
		
			if($types == 3){
				$student_active_withoutClass = $objDB->returnArray($student_WithoutClass_Active_sql);
			} else{
				$student_active_withoutClass = $objDB->returnVector($student_WithoutClass_Active_sql);
				$result[] = count($student_active_withoutClass);	
			}
			
			$student_WithoutClass_InActive_sql = 
						"SELECT
			               $query_second_target
						 FROM
			                INTRANET_USER AS u
							Inner Join {$eclass_db}.PORTFOLIO_STUDENT AS ps On u.UserID = ps.UserID     
			 			 WHERE
			                $student_con
			               	$inactive_con         
							$withoutclass_inactive_con
			 			 GROUP BY
							u.UserID
						";
						
			if($types == 4){
				$student_inactive_withoutClass = $objDB->returnArray($student_WithoutClass_InActive_sql);
			} else{
				$student_inactive_withoutClass = $objDB->returnVector($student_WithoutClass_InActive_sql);
				$result[] = count($student_inactive_withoutClass);	
			}
			
		}
		
		$Alumni_sql = "Select 
							$query_first_target
					   From 
					   		{$eclass_db}.PORTFOLIO_STUDENT as ps 
							Inner Join {$intranet_db}.INTRANET_ARCHIVE_USER as u On (u.UserID = ps.UserID)
			              	LEFT JOIN {$intranet_db}.YEAR_CLASS_USER AS ycu ON u.UserID = ycu.UserID
			              	LEFT JOIN {$intranet_db}.YEAR_CLASS AS yc ON ycu.YearClassID = yc.YearClassID
					   WHERE 
							$student_con 
							1
		 			   GROUP BY
							u.UserID
						";
						
		if($types == 5){
			$alumni_list = $objDB->returnArray($Alumni_sql);
		} else{
			$alumni_list = $objDB->returnVector($Alumni_sql);
			$result[] = count($alumni_list);	
		}
	
		
		if($types == 0){
			return $result;
		} 
		elseif($types == 1){
			return $student_active;
		}
		elseif($types == 2){
			return $student_inactive;
		}
		elseif($types == 3){
			return $student_active_withoutClass;
		}
		elseif($types == 4){
			return $student_inactive_withoutClass;
		}
		elseif($types == 5){
			return $alumni_list;
		}
		
	}
	
	function insertUpdateReleasedStudentDynamicReport($printOptions)
	{
		global $eclass_db, $intranet_db, $ipf_cfg, $UserID;
		
		$template_id = $printOptions['template_id'];
		$targetClass = $printOptions['targetClass'];
		$targetStudent = $printOptions['targetStudent'];
		$batchPrintMode = $printOptions['batchPrintMode'];
		$IssueDate = $printOptions['IssueDate'];
		$AcademicYearIDs = $printOptions['AcademicYearIDs'];
		$YearTermIDs = $printOptions['YearTermIDs'];
		$PrintSelRec = $printOptions['slpPrintRec'];
		$EmptyDataSymbol = trim(stripslashes($printOptions['EmptyDataSymbol']));
		$ShowNoDataTable = $printOptions['ShowNoDataTable'];
		$NoDataTableContent = trim(stripslashes($printOptions['NoDataTableContent']));
		$PrintingDirection = $printOptions['PrintingDirection'];
		$DisplayFullMark = ($printOptions['DisplayFullMark']);
		$DisplayOLEOrderOptions = $printOptions['rdDisplayOLEOrderOptions'];
		$ReleaseToStudent = ($printOptions['ReleaseToStudent']);
		$ReleaseStatus = ($printOptions['ReleaseStatus']);
		
		$resultAry = array();
		if($ReleaseToStudent == 1) {
			$currentAcademicYearID = Get_Current_Academic_Year_ID();
			
			$sql = "SELECT YearClassID FROM {$intranet_db}.YEAR_CLASS WHERE AcademicYearID='".$currentAcademicYearID."' AND ClassTitleEN='".$this->Get_Safe_Sql_Query($targetClass)."'";
			$yearClassIdAry = $this->returnVector($sql);
			$year_class_id = $yearClassIdAry[0];
			
			//$settings_string = $this->encodeReleaseStudentDynamicReportSettings($printOptions);
			
			$numOfStudent = count($targetStudent);
			
			for($i=0;$i<$numOfStudent;$i++) {
				$student_id = $targetStudent[$i];
				$objUser = new libuser($targetStudent[$i]);
				$report_title = $this->Get_Safe_Sql_Query($objUser->ClassName.'_'.$objUser->ClassNumber.'.'.$ipf_cfg['dynReport']['batchPrintMode']['pdf']);
				
				unset($printOptions['targetStudent']);
				$printOptions['targetStudent'] = array();
				$printOptions['targetStudent'][] = $student_id;
				$settings_string = $this->encodeReleaseStudentDynamicReportSettings($printOptions);
				
				$sql = "SELECT record_id FROM {$eclass_db}.student_report_generated_record WHERE template_id='".$template_id."' AND student_id='".$student_id."'";
				$recordIdAry = $this->returnVector($sql);
				if(count($recordIdAry)>0) {
					$record_id = $recordIdAry[0];
					$sql = "UPDATE {$eclass_db}.student_report_generated_record SET year_class_id='$year_class_id',report_title='".$report_title."',issue_date='$IssueDate',slp_record_type='$PrintSelRec',
								display_full_mark='$DisplayFullMark',record_status='$ReleaseStatus',generate_settings='$settings_string',modifieddate=NOW(),modifiedby='$UserID' 
								WHERE record_id='$record_id' ";
					$resultAry[$student_id.'_update_'.$record_id] = $this->db_db_query($sql);
					
					$sql = "DELETE FROM {$eclass_db}.student_report_generated_record_academic_year WHERE record_id='$record_id'";
					$resultAry[$student_id.'_delete_academicyear'] = $this->db_db_query($sql);	
				}else{
				
					$sql = "INSERT INTO {$eclass_db}.student_report_generated_record (template_id,student_id,year_class_id,report_title,issue_date,slp_record_type,display_full_mark,
								record_status,generate_settings,inputdate,inputby,modifieddate,modifiedby) 
								VALUES ('$template_id','$student_id','$year_class_id','$report_title','$IssueDate','$PrintSelRec','$DisplayFullMark','$ReleaseStatus','$settings_string',NOW(),'$UserID',NOW(),'$UserID')";
					$resultAry[$student_id.'_insert'] = $this->db_db_query($sql);
					
					$record_id = $this->db_insert_id();
				}
				if($record_id > 0) {
					$sql = "INSERT INTO {$eclass_db}.student_report_generated_record_academic_year (record_id,academic_year_id) VALUES ";
					$values = "";
					for($j=0;$j<count($AcademicYearIDs);$j++) {
						if($j == 0) {
							$values .= "('$record_id','".$AcademicYearIDs[$j]."')";
						}else{
							$values .= ",('$record_id','".$AcademicYearIDs[$j]."')";
						}
					}
					if($values != "") {
						$sql .= $values;
						$resultAry[$student_id.'_insert_academicyear'] = $this->db_db_query($sql);
					}
				}
			}
		}
		return !in_array(false,$resultAry);
	}
	
	function encodeReleaseStudentDynamicReportSettings($dataAry)
	{
		$setting_string = "";
		if(count($dataAry)>0) {
			foreach($dataAry as $key => $value) {
				if(is_array($value)) {
					foreach($value as $key2 => $value2) {
						$setting_string .= "&".$key."[]=$value2";
					}
				}else{
					$setting_string .= "&".$key."=".rawurlencode($value);
				}
			}
		}
		$setting_string = substr($setting_string,1); // remove first &
		return $setting_string;
	}
	
	// @return array
	function decodeReleaseStudentDynamicReportSettings($setting_string)	
	{
		$key_value_pairs = explode("&",$setting_string);
		$num_of_pair = count($key_value_pairs);
		$returnAry = array();
		for($i=0;$i<$num_of_pair;$i++) {
			$key_value = $key_value_pairs[$i];
			$key_value_ary = explode("=",$key_value);
			$tmp_key = $key_value_ary[0];
			$tmp_val = $key_value_ary[1];
			
			if(strstr($tmp_key,"[]") !== FALSE) {
				$key = str_replace("[]","",$tmp_key);
				if(!isset($returnAry[$key])) {
					$returnAry[$key] = array();
				}
				$returnAry[$key][] = $tmp_val;
			}else{
				$returnAry[$tmp_key] = rawurldecode($tmp_val);
			}
		}
		return $returnAry;
	}
	
	function getReleaseStudentDynamicReportSettings($recordIdAry)
	{
		global $eclass_db, $intranet_db, $Lang, $ipf_cfg, $UserID, $intranet_session_language, $iPort;
		
		$recordIdAry = (array)$recordIdAry;
		if(count($recordIdAry)==0) {
			return array();
		}
		
		$returnSettingsAry = array();
		$sql = "SELECT 
					generate_settings 
				FROM {$eclass_db}.student_report_generated_record 
				WHERE record_id IN (".implode(",",(array)$recordIdAry).")";
		$records = $this->returnVector($sql);
		
		for($i=0;$i<count($records);$i++) {
			$generate_settings = $records[$i];
			
			$decoded_settings = $this->decodeReleaseStudentDynamicReportSettings($generate_settings);
			$returnSettingsAry[] = $decoded_settings;
		}
		return $returnSettingsAry;
	}
	
	function getReleaseStudentDynamicReportDBTableSql($filterAry)
	{
		global $eclass_db, $intranet_db, $Lang, $ipf_cfg, $UserID, $intranet_session_language, $iPort;
		
		$name_field = getNameFieldByLang("u.");
		$generator_name_field = getNameFieldByLang("u2.");
		
		$classname_field = ( $intranet_session_language == "b5" || $intranet_session_language=="gb" )? "yc.ClassTitleB5" : "yc.ClassTitleEN";
		$yearname_field = ( $intranet_session_language == "b5" || $intranet_session_language=="gb" )? "y.YearNameB5" : "y.YearNameEN";
		
		$keyword = $filterAry['Keyword'];
		$release_status = $filterAry['ReleaseStatus'];
		$template_id = $filterAry['TemplateID'];
		$generate_by = $filterAry['GenerateBy'];
		$generate_start_date = $filterAry['GenerateStartDate'];
		$generate_end_date = $filterAry['GenerateEndDate'];
		
		$conds = "";
		if($template_id != "") {
			$conds .= " AND s.template_id='$template_id' ";
		}
		if($keyword != "") {
			$safe_keyword = $this->Get_Safe_Sql_Like_Query($keyword);
			$conds .= " AND (u.EnglishName LIKE '%$safe_keyword%' OR u.ChineseName LIKE '%$safe_keyword%'
							 OR yc.ClassTitleB5 LIKE '%$safe_keyword%' OR yc.ClassTitleEN LIKE '%$safe_keyword%' OR s.report_title LIKE '%$safe_keyword%') ";
		}
		if($generate_by != "") {
			$conds .= " AND (s.inputby='$generate_by' OR s.modifiedby='$generate_by') ";
		}
		if($generate_start_date != ""){
			$conds .= " AND (s.inputdate>='$generate_start_date' OR s.modifieddate>='$generate_start_date') ";
		}
		if($generate_end_date != ""){
			$conds .= " AND (s.inputdate<='$generate_end_date' OR s.modifieddate<='$generate_end_date') ";
		}
		
		if($release_status != "") {
			$conds .= " AND s.record_status='".$release_status."' ";
		}
		
		$sql = "SELECT 
					$classname_field as ClassTitle,
					ycu.ClassNumber,
					$name_field as StudentName,
					s.report_title as ReportTitle,
					t.template_title as TemplateTitle,
					GROUP_CONCAT($yearname_field ORDER BY y.Sequence SEPARATOR ', ') as YearName,
					DATE_FORMAT(s.issue_date,'%Y-%m-%d') as IssueDate,
					IF(s.slp_record_type='1','".$iPort["All"]."','".$iPort["Selected"]."') as SLPRecordType,
					IF(s.display_full_mark,'".$Lang['General']['Yes']."','".$Lang['General']['No']."') as DisplayFullMark,
					DATE_FORMAT(s.modifieddate,'%Y-%m-%d %H:%i:%s') as GenerateDate,
					$generator_name_field as GenerateBy,
					IF(s.record_status,'".$Lang['iPortfolio']['DynamicReport']['Released']."','".$Lang['iPortfolio']['DynamicReport']['Unrelease']."') as Status,
					CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',s.record_id,'\" />') as Checkbox,
					s.template_id,
					s.student_id 
				FROM {$eclass_db}.student_report_generated_record as s 
				INNER JOIN {$eclass_db}.student_report_template as t ON t.template_id=s.template_id 
				INNER JOIN {$intranet_db}.INTRANET_USER as u ON u.UserID=s.student_id 
				INNER JOIN {$eclass_db}.student_report_generated_record_academic_year as sy ON sy.record_id=s.record_id 
				INNER JOIN {$intranet_db}.ACADEMIC_YEAR as y ON y.AcademicYearID=sy.academic_year_id 
				INNER JOIN {$intranet_db}.YEAR_CLASS as yc ON yc.YearClassID=s.year_class_id 
				INNER JOIN {$intranet_db}.YEAR_CLASS_USER as ycu ON ycu.YearClassID=yc.YearClassID AND s.student_id=ycu.UserID 
				LEFT JOIN {$intranet_db}.INTRANET_USER as u2 ON u2.UserID=s.modifiedby 
				WHERE u.RecordStatus='1' AND u.RecordType='".USERTYPE_STUDENT."' $conds 
				GROUP BY s.record_id ";
		
		return $sql;
	}
	
	function releaseDynamicReportToStudent($recordIdAry,$releaseStatus)
	{
		global $eclass_db, $intranet_db, $Lang, $ipf_cfg, $UserID, $intranet_session_language, $iPort;
		$recordIdAry = (array)$recordIdAry;
		if(count($recordIdAry)==0) {
			return false;
		}
		
		$sql = "UPDATE {$eclass_db}.student_report_generated_record 
				SET record_status='$releaseStatus' 
				WHERE record_id IN (".implode(",",$recordIdAry).")";
		
		return $this->db_db_query($sql);
	}
	
	function getEncryptedTextWithNoTimeChecking($plainText='', $key='bXwKs7S93J2') {
	    global $intranet_root;

	    include_once($intranet_root."/includes/liburlparahandler.php");
	    
	    $lurlparahandler = new liburlparahandler(liburlparahandler::actionEncrypt, $plainText, $key);
	    return $lurlparahandler->getParaEncrypted();
	}
	
	function getDecryptedTextWithNoTimeChecking($encryptedText='', $key='bXwKs7S93J2') {
	    global $intranet_root;
	    include_once($intranet_root."/includes/liburlparahandler.php");
	    
	    $lurlparahandler = new liburlparahandler(liburlparahandler::actionDecrypt, $encryptedText, $key);
	    $decrypted = $lurlparahandler->getParaDecrypted();
	    
	    return $decrypted;
	}

	function deleteReleasedStudentDynamicReport($recordIdAry)
	{
		global $eclass_db, $intranet_db, $intranet_root, $PATH_WRT_ROOT, $Lang, $ipf_cfg, $UserID;
		
		include_once($intranet_root."/includes/libfilesystem.php");
		
		$lfs = new libfilesystem();
		
		$recordIdAry = (array)$recordIdAry;
		if(count($recordIdAry)==0) {
			return false;
		}
		
		$sql = "SELECT 
					record_id, template_id, student_id 
				FROM {$eclass_db}.student_report_generated_record 
				WHERE record_id IN (".implode(",",(array)$recordIdAry).")";
		$records = $this->returnResultSet($sql);		
		$record_count = count($records);
		//debug_r($records);
		$deleteRecordIdAry = array();
		for($i=0;$i<$record_count;$i++) {
			$record_id = $records[$i]['record_id'];
			$template_id = $records[$i]['template_id'];
			$student_id = $records[$i]['student_id'];
			
// 			$file_path = $ipf_cfg['dynReport']['studentReportPath'] . '/' . $template_id . '/'. getEncryptedText($student_id, $ipf_cfg['dynReport']['encryptKey']).'.pdf';
			$file_path = $ipf_cfg['dynReport']['studentReportPath'] . '/' . $template_id . '/'. $this->getEncryptedTextWithNoTimeChecking($student_id, $ipf_cfg['dynReport']['encryptKey']).'.pdf';
			
			//debug_r($file_path);
			if(file_exists($file_path) && is_file($file_path)) {
				$lfs->lfs_remove($file_path);
			}
			
			$deleteRecordIdAry[] = $record_id;
		}
		//debug_r($deleteRecordIdAry);
		$success = true;
		if(count($deleteRecordIdAry)>0) {
			$sql = "DELETE FROM {$eclass_db}.student_report_generated_record WHERE record_id IN (".implode(",",$deleteRecordIdAry).")";
			$success = $this->db_db_query($sql);
			//debug_r($sql);
			$sql = "DELETE FROM {$eclass_db}.student_report_generated_record_academic_year WHERE record_id IN (".implode(",",$deleteRecordIdAry).")";
			$success = $this->db_db_query($sql) && $success;
			//debug_r($sql);
		}
		return $success;
	}
	
	function getReleasedStudentDynamicReportGenerator()
	{
		global $eclass_db, $intranet_db, $intranet_root, $PATH_WRT_ROOT, $Lang, $ipf_cfg, $UserID;
		
		$name_field = getNameFieldByLang("u.");
		
		$sql = "SELECT DISTINCT u.UserID, $name_field as UserName
				FROM {$eclass_db}.student_report_generated_record as r 
				INNER JOIN {$intranet_db}.INTRANET_USER as u ON u.UserID=r.modifiedby OR u.UserID=r.inputby 
				WHERE u.RecordStatus='1' 
				ORDER BY UserName";
		
		$result = $this->returnArray($sql);
		return $result;
	}
	
	function CalculateSDMean($scoreArr){
		if(count($scoreArr) == 0){
			return array(
				'SD' => 0,
				'Mean' => 0,
				'Count' => 0,
			);
		}
		######## Get Mean START ########
		$sum = array_sum($scoreArr);
		$count = count($scoreArr);
		$mean = $sum / $count;
		######## Get Mean END ########
		
		
		######## Get SD START########
		$xArr = array();
		foreach($scoreArr as $score){
			$x = $score - $mean;
			$xArr[] = $x * $x;
		}
		$xSum = array_sum($xArr);
		
		$sd = sqrt( $xSum / $count );
		######## Get SD END ########
		
		return array(
			'SD' => $sd,
			'Mean' => $mean,
			'Count' => $count,
		);
	}
	
	//function ComputeSDMean($YearID, $SubjectID, $SubjectComponentID, $AcademicYearID, $YearTermID, $IsAnnual)
	function ComputeSDMean($YearID, $AcademicYearID, $YearTermID, $YearClassID=0)
	{
		global $intranet_db, $eclass_db;
		//debug($YearID, $AcademicYearID, $YearTermID, $YearClassID);
		
		# testing
		//$YearID = 1;  # class_level
		//$AcademicYearID = 9;
		//$YearTermID = 24;
		
		if ($YearClassID==0)
		{
			$sql = "SELECT YearClassID FROM {$intranet_db}.YEAR_CLASS WHERE YearID='$YearID'";
			$ClassIDArr = $this->returnVector($sql);
			if (sizeof($ClassIDArr)<1)
			{
				# no class in that form / year
				return false;
			}
		} else
		{
			$ClassIDArr = array($YearClassID);
		}
		$ClassIDinSQL = implode(",", $ClassIDArr);
		//debug_r($ClassIDArr);
		# remove previous data
		$sql = "DELETE FROM {$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN " .
				"WHERE AcademicYearID='{$AcademicYearID}' AND YearTermID='{$YearTermID}' AND ClassLevelID='{$YearID}' AND YearClassID='{$YearClassID}' ";
		$this->db_db_query($sql);
		//debug_r($sql);
		
		#### Get PassMarks START ####
		$sql = "SELECT SubjectID, FullMarkInt, PassMarkInt FROM {$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK " .
				"WHERE AcademicYearID='{$AcademicYearID}' AND YearID='{$YearID}' ";
		$SubectFullMarkArr = $this->returnResultSet($sql);
		for ($i=0; $i<sizeof($SubectFullMarkArr); $i++)
		{
			$SubectFullMarks[$SubectFullMarkArr[$i]["SubjectID"]] = $SubectFullMarkArr[$i]["FullMarkInt"];
			$passMark = ($SubectFullMarkArr[$i]["PassMarkInt"])? $SubectFullMarkArr[$i]["PassMarkInt"] : ((int)$SubectFullMarkArr[$i]["FullMarkInt"]) / 2;
			$SubectPassMarks[$SubectFullMarkArr[$i]["SubjectID"]] = $passMark;
		}
		//debug($sql);
		//debug_rt($SubectFullMarkArr);
		#### Get PassMarks END ####
		

		# get the total number of students
		$StudentToal = 0;
		
		if ($YearClassID==0)
		{
			$lib_year = new Year($YearID);
			$AllClasses = $lib_year->Get_All_Classes("", $AcademicYearID);
	//debug_r($AllClasses);
			for ($i=0; $i<sizeof($AllClasses); $i++)
			{
				$this->ComputeSDMean($YearID, $AcademicYearID, $YearTermID, $AllClasses[$i]["YearClassID"]);
				//debug($AllClasses[$i]["YearClassID"]);
				$lib_year_class = new year_class($AllClasses[$i]["YearClassID"], false, false, true);
				$StudentTotal += sizeof($lib_year_class->ClassStudentList);
			}
		} else
		{
			$lib_year_class = new year_class($YearClassID, false, false, true);
			$StudentTotal += sizeof($lib_year_class->ClassStudentList);
		}
		
		# for class
		//$lib_year_class = new year_class($YearID, false, false, true);
		//debug_r($lib_year_class->ClassStudentList);
		//die();
		
		# 1: subjects
		# 2: overall (Main)
		//$sql_SubjectComponentID = ($SubjectComponentID==0 || $SubjectComponentID=="") ? "SubjectComponentID IS NULL" : "SubjectComponentID='{$SubjectComponentID}'";
		
		$sql_yeartermID = ($YearTermID==NULL || $YearTermID=="" || $YearTermID=="NULL") ? " (YearTermID IS NULL OR YearTermID = 0) " : " YearTermID='{$YearTermID}' ";
		
		$IsMain = "0";
		# get the raw data
		$sql = "SELECT Score,  SubjectID, SubjectComponentID, TermAssessment, IsAnnual FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
				"AcademicYearID='{$AcademicYearID}' AND  {$sql_yeartermID}  " .
				"AND YearClassID IN ($ClassIDinSQL)  " .
				"ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
		$SubjectMarks = $this->ReturnResultSet($sql);
		//debug_rt($SubjectMarks);
		//debug($sql);
		$PreviousSubjectID = "";
		$PreviousSubjectComponentID = "";
		$PreviousTermAssessment = "";
		$PreviousIsAnnual = "";
		$MarkRecordArr = array("LOWEST"=>99999, "HIGHEST"=>0, "PassTotal"=>0, "AttemptTotal"=>0);
		
		for ($i=0; $i<sizeof($SubjectMarks); $i++)
		{
			$EntryObj = $SubjectMarks[$i];
			$SubjectID = $EntryObj["SubjectID"];
			$IsAnnual = $EntryObj["IsAnnual"];
			$SubjectComponentID = ($EntryObj["SubjectComponentID"]=="") ? "0" : $EntryObj["SubjectComponentID"];
			$TermAssessment = ($EntryObj["TermAssessment"]=="") ? "0" : $EntryObj["TermAssessment"];
			$ScoreArr[$SubjectID][$SubjectComponentID][$IsAnnual][$TermAssessment][] = $EntryObj["Score"];
			

			if ($i!=0 && ($PreviousSubjectID!=$SubjectID || $PreviousSubjectComponentID!=$SubjectComponentID || 
				$PreviousIsAnnual!=$IsAnnual || $PreviousTermAssessment!=$TermAssessment ||
				$i==sizeof($SubjectMarks)-1))
			{
				
				if ($i==sizeof($SubjectMarks)-1)
				{
					$dbSubjectID = $SubjectID;
					$dbSubjectComponentID = $SubjectComponentID;
					$dbTermAssessment =$TermAssessment;
					$dbIsAnnual = $IsAnnual;
					
					if ($MarkRecordArr["LOWEST"]>$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
					{
						$MarkRecordArr["LOWEST"] = $EntryObj["Score"];
					}
					
					if ($MarkRecordArr["HIGHEST"]<$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
					{
						$MarkRecordArr["HIGHEST"] = $EntryObj["Score"];
					}
					
					if ($EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
					{
						if ($EntryObj["Score"]>=$SubectPassMarks[$SubjectID])
						{
							$MarkRecordArr["PassTotal"] ++;
						}
						$MarkRecordArr["AttemptTotal"] ++;
					}
				} else
				{
					$dbSubjectID = $PreviousSubjectID;
					$dbSubjectComponentID = $PreviousSubjectComponentID;
					$dbTermAssessment =$PreviousTermAssessment;
					$dbIsAnnual = $PreviousIsAnnual;					
				}
				
				
				
				#cal SD and Mean
				$AllScores = $ScoreArr[$dbSubjectID][$dbSubjectComponentID][$dbIsAnnual][$dbTermAssessment];
				
				
				//debug($i);
				//debug_r($AllScores);
				$sd = round(standard_deviation($AllScores), 2);
				$mean = round(array_sum($AllScores)/sizeof($AllScores), 3);
				//debug($sd, $mean);
				# Yuen: save to database
				$sql = "INSERT INTO {$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN (SubjectID, SubjectComponentID, AcademicYearID, YearTermID, TermAssessment, " .
						"IsAnnual, IsMain, ClassLevelID, YearClassID,  SD, MEAN, HighestMark, LowestMark, PassTotal, AttemptTotal, StudentTotal, InputDate) " .
						"values ('{$dbSubjectID}', '{$dbSubjectComponentID}', '{$AcademicYearID}', '{$YearTermID}', '{$dbTermAssessment}', " .
						"'{$dbIsAnnual}', '{$IsMain}', '{$YearID}', '{$YearClassID}', '{$sd}', '{$mean}', '".$MarkRecordArr["HIGHEST"]."', '".$MarkRecordArr["LOWEST"]."', '".$MarkRecordArr["PassTotal"]."', '".$MarkRecordArr["AttemptTotal"]."', '{$StudentTotal}', now()) ";
				$this->db_db_query($sql);
				
				
				
				# reset
				$MarkRecordArr = array("LOWEST"=>9999, "HIGHEST"=>0, "PassTotal"=>0, "AttemptTotal"=>0);
			}
		
			if ($MarkRecordArr["LOWEST"]>$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
			{
				$MarkRecordArr["LOWEST"] = $EntryObj["Score"];
			}
			
			if ($MarkRecordArr["HIGHEST"]<$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
			{
				$MarkRecordArr["HIGHEST"] = $EntryObj["Score"];
			}
			
			if ($EntryObj["Score"]!="" && $EntryObj["Score"]>=00)
			{
				// #W110140
				//if ($EntryObj["Score"]>=$SubectFullMarks[$SubjectID]/2)
				if ($EntryObj["Score"]>=$SubectPassMarks[$SubjectID])
				{
					$MarkRecordArr["PassTotal"] ++;
				}
				$MarkRecordArr["AttemptTotal"] ++;
			}
			
			$PreviousSubjectID = $SubjectID;
			$PreviousSubjectComponentID = $SubjectComponentID;
			$PreviousTermAssessment = $TermAssessment;
			$PreviousIsAnnual = $IsAnnual;
		}
		
		
		$IsMain = "1";
		$sql = "SELECT Score, TermAssessment, IsAnnual FROM {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
				"AcademicYearID='{$AcademicYearID}' AND  {$sql_yeartermID}  " .
				"AND YearClassID IN ($ClassIDinSQL)  " .
				"ORDER BY TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
		$MainMarks = $this->ReturnResultSet($sql);
		
		//debug_r($MainMarks);
		$SubjectID = 0;
		$SubjectComponentID = 0;
		
		//debug($sql);
		$PreviousTermAssessment = "";
		$PreviousIsAnnual = "";
		
		$MarkRecordArr = array("LOWEST"=>9999, "HIGHEST"=>0, "PassTotal"=>0, "AttemptTotal"=>0);
		
		for ($i=0; $i<sizeof($MainMarks); $i++)
		{
			$EntryObj = $MainMarks[$i];
			$IsAnnual = $EntryObj["IsAnnual"];
			$TermAssessment = ($EntryObj["TermAssessment"]=="") ? "0" : $EntryObj["TermAssessment"];
			$MainScoreArr[$IsAnnual][$TermAssessment][] = $EntryObj["Score"];
			
			if ($i!=0 && ($PreviousTermAssessment!=$TermAssessment || $PreviousIsAnnual!=$IsAnnual || $i==sizeof($MainMarks)-1))
			{
				#cal SD and Mean
				$AllScores = $MainScoreArr[$PreviousIsAnnual][$PreviousTermAssessment];
				//debug($i);
				//debug_r($AllScores);
				$sd = round(standard_deviation($AllScores), 2);
				$mean = round(array_sum($AllScores)/sizeof($AllScores), 3);
				//debug($sd, $mean);
				
				if ($i==sizeof($MainMarks)-1)
				{
					$dbTermAssessment =$TermAssessment;
					$dbIsAnnual = $IsAnnual;	
				} else
				{
					$dbTermAssessment =$PreviousTermAssessment;
					$dbIsAnnual = $PreviousIsAnnual;					
				}
				# Yuen: save to database
				$sql = "INSERT INTO {$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN (SubjectID, SubjectComponentID, AcademicYearID,  YearTermID, TermAssessment, " .
						"IsAnnual, IsMain, ClassLevelID, YearClassID, SD, MEAN, InputDate) " .
						"values ('0', '0', '{$AcademicYearID}', '{$YearTermID}', '{$dbTermAssessment}', " .
						"'{$dbIsAnnual}', '{$IsMain}', '{$YearID}', '{$YearClassID}', '{$sd}', '{$mean}', now()) ";
						//debug($sql);
				$this->db_db_query($sql);
			}
			
			$PreviousTermAssessment = $TermAssessment;
			$PreviousIsAnnual = $IsAnnual;
		}
		
		# ToDo: no of passes, no of attempts, total number of student, max, min, full-mark
		
		//debug_r($ScoreArr);die();
		
		
		# update or insert record
		
		
		/*
		 * 
		 * if($sd>0)
			{
				# Calculate the standard score
				$mean = array_sum($ScoreArr)/sizeof($ScoreArr);
				$z = round((($score-$mean)/$sd), 2);
			}
		 */
	}
	
	function GetSDandMean($YearIDs, $IsYearLevelOnly=true)
	{
			global $eclass_db;
			
			$sql_year_ids = implode(",", $YearIDs);
			
		  	$sql = "SELECT SubjectID, SubjectComponentID, AcademicYearID,  YearTermID, TermAssessment, IsAnnual, IsMain, ClassLevelID, SD, MEAN, HighestMark, LowestMark, PassTotal, AttemptTotal, StudentTotal " .
		  			"FROM {$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN WHERE AcademicYearID IN ($sql_year_ids) ";
		  	if ($IsYearLevelOnly)
		  	{
		  		$sql .= " AND YearClassID=0 ";
		  	}
		  	
			$SDrows = $this->returnResultSet($sql);
			for ($i=0; $i<sizeof($SDrows); $i++)
			{
				$obj = $SDrows[$i];
				$returnArr[$obj["SubjectID"]][$obj["SubjectComponentID"]][$obj["AcademicYearID"]][$obj["YearTermID"]][$obj["TermAssessment"]][$obj["IsAnnual"]][$obj["IsMain"]][$obj["ClassLevelID"]]["SD"] = $obj["SD"];
				$returnArr[$obj["SubjectID"]][$obj["SubjectComponentID"]][$obj["AcademicYearID"]][$obj["YearTermID"]][$obj["TermAssessment"]][$obj["IsAnnual"]][$obj["IsMain"]][$obj["ClassLevelID"]]["MEAN"] = $obj["MEAN"];
			}
			
		  	return $returnArr;
	} 
	
	function UpdatePassTotal($AcademicYearID, $thisSubjectID, $ClassLevelID, $fullMarkInt, $passMarkInt){
		global $eclass_db;
		
		if($passMarkInt == 0){
			$passMarkInt = $fullMarkInt / 2;
			
		}
		
		
		######## Get Year Class START ########
		$sql = "SELECT * FROM YEAR_CLASS";
		$rs = $this->returnResultSet($sql);
		$allYearClass = array();
		foreach($rs as $r){
			$allYearClass[$r['AcademicYearID']][$r['YearID']][] = $r['YearClassID'];
		}
		######## Get Year Class END ########
		
		
		######## Get all patch record ID START ########
		$allWrongRecord = array();
		
		$sql = "SELECT 
			*
		FROM
			{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN
		WHERE
			IsMain = '0'
		AND
			AcademicYearID = '{$AcademicYearID}'
		AND 
			(
				SubjectID = '{$thisSubjectID}'
			OR
				SubjectComponentID = '{$thisSubjectID}'
			)
		AND
			ClassLevelID = '{$ClassLevelID}'
		";
		$rs = $this->returnResultSet($sql);
		foreach($rs as $r){
			$passTotal = 0;
			
			## Filter Class/Form START ##
			if($r['YearClassID'] === '0'){ //TODO: Form
				$formClass = $allYearClass[$r['AcademicYearID']][$r['ClassLevelID']];
				$formClassSQL = implode("','", $formClass);
				$yearClassSQL = " YearClassID IN ('{$formClassSQL}')";
			}else{ //TODO: Class
				$yearClassSQL = " YearClassID = '{$r['YearClassID']}'";
			}
			## Filter Class/Form END ##
			
			## Init SQL, pass score START ##
			if($r['SubjectComponentID'] === '0'){
				$componentSQL = " (SubjectComponentID IS NULL OR SubjectComponentID='0')";
			}else{
				$componentSQL = " SubjectComponentID='{$r['SubjectComponentID']}'";
			}
			if($r['TermAssessment'] === '0'){
				$termAssessmentSQL = " (TermAssessment IS NULL OR TermAssessment='0')";
			}else{
				$termAssessmentSQL = " TermAssessment='{$r['TermAssessment']}'";
			}
			if($r['YearTermID'] === '0'){
				$yearTermIdSQL = " (YearTermID IS NULL OR YearTermID='0')";
			}else{
				$yearTermIdSQL = " YearTermID='{$r['YearTermID']}'";
			}
			## Init SQL, pass score END ##
			
			## Get all score START ## 
			$sql = "SELECT 
				*
			FROM
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
			WHERE
				SubjectID = '{$r['SubjectID']}'
			AND
				{$componentSQL}
			AND
				AcademicYearID = '{$r['AcademicYearID']}'
			AND
				{$yearTermIdSQL}
			AND
				{$termAssessmentSQL}
			AND
				IsAnnual = '{$r['IsAnnual']}'
			AND
				{$yearClassSQL}
			";
			$marks = $this->returnResultSet($sql);
			## Get all score END ##
			
			## Calculate pass total START ##
			foreach($marks as $mark){
				if($mark['Score'] >= $passMarkInt){
					$passTotal++;
				}
			}
			## Calculate pass total END ##
		
			$allWrongRecord[$r['RecordID']] = $passTotal;
		}
		######## Get all patch record ID END ########
		
		$i=0;
		$result = true;
		foreach($allWrongRecord as $rid=>$score){
			$sql = "UPDATE {$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN SET PassTotal='{$score}' WHERE RecordID='{$rid}'";
			$result = $this->db_db_query($sql) && $result;
		}
		
		return $result;
	}

	function GetAcademicYears($FromAcademicYear, $ToAcademicYear)
	{
			# get year in order
			$lib_academic_year = new academic_year();
			# note, desc order now
			
			$AcademicYearArr = $lib_academic_year->Get_All_Year_List("", false, array(), "");
			//debug_r($AcademicYearArr);
			
			$StartAtID = 0;
			$IsCount = false;
		
			$IsCount = false;
			for ($i_year=sizeof($AcademicYearArr)-1; $i_year>=0; $i_year--)
		  	{
		  		$AcademicYearID = $AcademicYearArr[$i_year]["AcademicYearID"];
		  		$AcademicYearName = $AcademicYearArr[$i_year]["AcademicYearName"];
		  		
		  		//if ($AcademicYearID==$FromAcademicYear)
				if (!$IsCount && ($AcademicYearID==$FromAcademicYear || $AcademicYearID==$ToAcademicYear))		# for desc order data
		  		{
		  			$IsCount = true;
		  			$StartAtID = $AcademicYearID;
		  		}
		  		
		  		if ($IsCount)
		  		{
		  			$ReturnRx[] = $AcademicYearID;
		  		}
		  		
		  		//if ($IsCount && $AcademicYearID==$ToAcademicYear)
		  		if ($IsCount && $StartAtID!=$AcademicYearID && ($AcademicYearID==$FromAcademicYear || $AcademicYearID==$ToAcademicYear))	# for desc order data
		  		{
		  			$IsCount = false;
		  		}
		  		
		  		if ($IsCount && $FromAcademicYear==$ToAcademicYear)
		  		{
		  			$IsCount = false;
		  		}
				
		  	}
		  	
		  	return $ReturnRx;
	}
	
	/*
	 * This will return the releated Subject, e.g. Math---Math(Module 1)
	 */ 
	function getReleatedSubjectID(){
		global $intranet_db, $eclass_db, $sys_custom;
		if(count($sys_custom['iPortfolio_Group_Subject']) == 0){
			return array();
		}
		
		$sql = "SELECT 
			RecordID, 
			CODEID 
		FROM 
			{$intranet_db}.ASSESSMENT_SUBJECT
		WHERE
			CMP_CODEID IS NULL
		OR
			CMP_CODEID = ''";
		$_rs = $this->returnResultSet($sql);
		$rs = BuildMultiKeyAssoc($_rs, array('CODEID') , array('RecordID'), $SingleValue=1);
		
		$releatedSubjectID = array();
		foreach($sys_custom['iPortfolio_Group_Subject'] as $parentCode => $childCodeArr){
			foreach($childCodeArr as $childCode){
				$releatedSubjectID[ $rs[$parentCode] ][] = $rs[$childCode];
			}
		}
		return $releatedSubjectID;
	}
	function getParentReleatedSubject($childCodeID){
		global $intranet_db, $eclass_db, $sys_custom;
		if(count($sys_custom['iPortfolio_Group_Subject']) == 0){
			return array();
		}
		
		$parentCode = $childCodeID;
		foreach($sys_custom['iPortfolio_Group_Subject'] as $_parentCode => $childCodeArr){
			foreach($childCodeArr as $childCode){
				if($childCode == $childCodeID){
					$parentCode = $_parentCode;
					break;
				}
			}
		}
		
		$sql = "SELECT
			*
		FROM
			{$intranet_db}.ASSESSMENT_SUBJECT
		WHERE
			CODEID = '{$parentCode}'
		AND
		(
			CMP_CODEID IS NULL
		OR
			CMP_CODEID = ''
		)";
		$rs = $this->returnResultSet($sql);
		
		return (array)$rs[0];
	}
	
	  function GetClassCrossYearBySubject($AcademicYearIDs, $SubjectID, $YearClassID, $DataType, $filterColumnArr, $addStudentArr = array())
	  {
	  		global $intranet_db, $eclass_db, $Lang, $ec_iPortfolio, $iPort, $PATH_WRT_ROOT_ABS, $LAYOUT_SKIN;
	  		//echo implode(",",$AcademicYearIDs)."($AcademicYearIDs, $SubjectID, $YearClassID, $DataType)";
	  		$SymbolEmpty = $Lang['General']['EmptySymbol'] ;
	  		
	  		$filterColumnArr = (array)$filterColumnArr;
	  		$displayDataColumnCount = count($filterColumnArr);
	  		if(in_array('filterAge', $filterColumnArr)){
	  			$displayDataColumnCount--;
	  		}
	  		if(in_array('filterPrimarySchool', $filterColumnArr)){
	  			$displayDataColumnCount--;
	  		}
	  		if(in_array('filterPercentile', $filterColumnArr)){
	  			$displayDataColumnCount--;
	  		}

	  		
	  		#### Get releated subject START ####
	  		$_subjectIds = $this->getReleatedSubjectID();
	  		$subjectIdArr = array($SubjectID);
	  		foreach($_subjectIds as $parentSubj => $_subjIdArr){
	  			if(in_array($SubjectID, $_subjIdArr) || $parentSubj == $SubjectID){
	  				$subjectIdArr = $_subjIdArr;
	  				$subjectIdArr[] = $parentSubj;
	  				break;
	  			}
	  		}
	  		#### Get releated subject END ####
	  		
			#### get the student list START ####
			$lib_year_class = new year_class($YearClassID, false, false, true);
		  	$_ClassStudentList = $lib_year_class->ClassStudentList;
		  	
		  	$studentIdList = array();
		  	$ClassStudentArr = array(); # Only use studentID and studentName
		  	foreach($_ClassStudentList as $student){
		  		//if(!$student["ArchiveUser"]){
		  			$ClassStudentArr[] = $student;
		  			$studentIdList[] = $student['UserID'];
		  		//}
		  	}
		  	
		  	foreach((array)$addStudentArr as $studentID){
		  		if(!in_array($studentID, $studentIdList)){
		  			$student = new libuser($studentID);
		  			$studentName = $student->UserNameLang();
			  		$ClassStudentArr[] = array(
			  			'StudentName'=>$studentName,
			  			'UserID'=>$studentID
			  		);
		  			$studentIdList[] = $studentID;
			  	}
		  	}
			$sql_user_ids = implode(",", $studentIdList);
		  	
			## Other Student Info START ##
		  	$sql = "SELECT
		  		IU.UserID,
		  		TIMESTAMPDIFF(YEAR, IU.DateOfBirth, CURDATE()) AS Age,
		  		IU.PrimarySchool
		  	FROM
		  		{$intranet_db}.INTRANET_USER IU
		  	WHERE 
		  		IU.UserID IN ({$sql_user_ids})";
		  	$rs = $this->returnResultSet($sql);
		  	$_infoArr = BuildMultiKeyAssoc($rs, array('UserID') , array('Age','PrimarySchool'));
		  	foreach($ClassStudentArr as $index=>$student){
		  		$ClassStudentArr[$index]['Age'] = $_infoArr[$student['UserID']]['Age'];
		  		$ClassStudentArr[$index]['PrimarySchool'] = $_infoArr[$student['UserID']]['PrimarySchool'];
		  	}
			## Other Student Info END ##
			#### get the student list END ####
		
			# get year in order
			/* Old Method
			$lib_academic_year = new academic_year();
			# note, desc order now
			$AcademicYearArr = $lib_academic_year->Get_All_Year_List("", false, array(), $AcademicYearIDs);
			*/
			$sql_year_ids = implode(",", $AcademicYearIDs);
			$AcademicYearIdSql = implode("','", $AcademicYearIDs);
			$yearNameSQL = Get_Lang_Selection('YearNameB5', 'YearNameEN');
			$sql = "SELECT DISTINCT
						AY.AcademicYearID,
						AY.{$yearNameSQL} as AcademicYearName
					FROM
						ACADEMIC_YEAR AY
						INNER JOIN
						ACADEMIC_YEAR_TERM AYT
						ON
						AY.AcademicYearID = AYT.AcademicYearID
					WHERE
						AY.AcademicYearID IN ('{$AcademicYearIdSql}')
					ORDER BY
						AYT.TermStart DESC";
			$AcademicYearArr = $this->returnResultSet($sql);
			
			#### Get AcademicYearStart START ####
			$fcm = new form_class_manage();
			$rs = $fcm->Get_Academic_Year_List($AcademicYearIDs);
			$academicYearStartArr = BuildMultiKeyAssoc($rs, array('AcademicYearID') , array('AcademicYearStart'), $SingleValue=1);
			#### Get AcademicYearStart END ####
			
			
			$lib_year = new Year();
			$AllClasses = $lib_year->Get_All_Classes("", -1);
			for ($i=0; $i<sizeof($AllClasses); $i++)
			{
				$ClassMapYear[$AllClasses[$i]["YearClassID"]] = $AllClasses[$i]["YearID"];
			}
			//debug_r($ClassMapYear);
			
			# get assessment and term in order
			# studentIDs, academicyearIDs, 
			
			$sql_fields = "ASSR.UserID, ASSR.ClassName, ASSR.ClassNumber, ASSR.YearClassID, ASSR.AcademicYearID, ASSR.YearTermID, ASSR.TermAssessment, ASSR.Score, ASSR.Grade, ASSR.OrderMeritForm, ASSR.OrderMeritFormTotal, ASSR.SubjectID";
			/*for ($i=0; $i<sizeof($ClassStudentArr); $i++)
			{
				$user_ids[] = $ClassStudentArr[$i]["UserID"];
			}
			$sql_user_ids = implode(",", $user_ids);*/
			
			#### Get Percentile Rank START ####
			$studentRank = $this->getStudentPercentile($AcademicYearIDs, $SubjectID);
			#### Get Percentile Rank END ####
			

			# follow for subject component later
			$SubjectIdSQL = implode("','", $subjectIdArr);
			$sql_cond = "ASSR.UserID IN ({$sql_user_ids}) AND ASSR.SubjectComponentID IS NULL AND ASSR.AcademicYearID IN ($sql_year_ids) AND ASSR.SubjectID IN ('{$SubjectIdSQL}')";
			if($DataType != "ALL"){
				$sql_cond .= " AND ASSR.TermAssessment = 0";
			}
			$sql_order = "ASSR.ClassName, LENGTH(ASSR.ClassNumber), ASSR.ClassNumber, ISNULL(AYT.TermStart), AYT.TermStart, ISNULL(ASSR.TermAssessment), ASSR.TermAssessment";
				
				
			$sql = "SELECT 
				{$sql_fields} 
			FROM 
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR 
			LEFT JOIN
				{$intranet_db}.ACADEMIC_YEAR_TERM AYT
			ON
				ASSR.YearTermID = AYT.YearTermID
			WHERE 
				{$sql_cond} 
			ORDER BY 
				{$sql_order}";
			$rows = $this->returnResultSet($sql);
		  	
		  	# Get Student Year Class Info #H111806 
			$studentIDArr = Get_Array_By_Key($rows, 'UserID');
			$sql = "SELECT ycu.UserID, yc.ClassTitleEN as ClassName, ycu.ClassNumber as ClassNumber, yc.AcademicYearID
						FROM 
						 YEAR_CLASS_USER as ycu 
						INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
						WHERE ycu.USERID iN ('".implode("','",(array)$studentIDArr)."')
						AND yc.AcademicYearID IN ('".implode("','",$AcademicYearIDs)."')";
			$studentYearClassAssoc = BuildMultiKeyAssoc($this->returnResultSet($sql), array('UserID','AcademicYearID'), array('ClassName','ClassNumber')  );
			
		  	//debug_rt($rows);
		  	$classStudentIdArr = array();
		  	$orderClassStudentArr = array();
		  	$_prevID = '';
		  	foreach($rows as $r){
		  		if($_prevID != $r['UserID']){
			  		foreach($ClassStudentArr as $stu){
				  		if($r['UserID'] == $stu['UserID'] && !in_array($stu['UserID'], $classStudentIdArr)){
				  			$orderClassStudentArr[] = $stu;
				  			$classStudentIdArr[] = $stu['UserID'];
				  			break;
				  		}
		  			}
		  		}
		  		$_prevID = $r['UserID'];
		  	}
				
		  	# prepare student class and classname by year
		  	for ($i=0; $i<sizeof($rows); $i++)
		  	{
		  		// #H111806
		  		//$StudentYearClassInfo[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]] = array("ClassName"=>$rows[$i]["ClassName"], "ClassNumber"=>$rows[$i]["ClassNumber"]);
		  		$className = $studentYearClassAssoc[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]]['ClassName'];
		  		$classNumber = $studentYearClassAssoc[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]]['ClassNumber'];
		  		$StudentYearClassInfo[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]] = array("ClassName"=>$className, "ClassNumber"=>$classNumber);
		  		$TermAssessment = ($rows[$i]["TermAssessment"]=="" || $rows[$i]["TermAssessment"]==NULL) ? 0 : $rows[$i]["TermAssessment"];
		  		$YearTermID = ($rows[$i]["YearTermID"]=="" || $rows[$i]["YearTermID"]==NULL) ? 0 : $rows[$i]["YearTermID"];
		  		$RawMarkRows[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]][$YearTermID][$TermAssessment]["Score"] = $rows[$i]["Score"];
		  		$RawMarkRows[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]][$YearTermID][$TermAssessment]["Grade"] = $rows[$i]["Grade"];
		  		$RawMarkRows[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]][$YearTermID][$TermAssessment]["OrderMeritForm"] = $rows[$i]["OrderMeritForm"];
		  		$RawMarkRows[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]][$YearTermID][$TermAssessment]["OrderMeritFormTotal"] = $rows[$i]["OrderMeritFormTotal"];
		  		$RawMarkRows[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]][$YearTermID][$TermAssessment]["SubjectID"] = $rows[$i]["SubjectID"];
		  		$YearClassIDMapping[$rows[$i]["UserID"]][$rows[$i]["AcademicYearID"]] = $rows[$i]["YearClassID"];
		  	}
		  	//debug_r($StudentYearClassInfo); 
		  	//debug_r($RawMarkRows);
		  	//debug_r($YearClassIDMapping);
		  	
		  	$sql = "SELECT DISTINCT 
				ASSR.AcademicYearID, 
				ASSR.YearTermID, 
				ASSR.TermAssessment 
			FROM 
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
			LEFT JOIN
				{$intranet_db}.ACADEMIC_YEAR_TERM AYT
			ON
				ASSR.YearTermID = AYT.YearTermID
			WHERE 
				{$sql_cond} 
			ORDER BY 
				ASSR.AcademicYearID, ISNULL(ASSR.YearTermID), ASSR.YearTermID, AYT.TermStart, ISNULL(ASSR.TermAssessment), ASSR.TermAssessment";
			$HeaderRows = $this->returnResultSet($sql);
			//debug($sql);
			//debug_r($HeaderRows);
		  	
		  	#### Get all term index START ####
		  	$allTermIndex = $this->getAllTermIndex();
		  	#### Get all term index END ####
		  	
		  	# retrieve SD 
			$SDrows = $this->GetSDandMean($AcademicYearIDs);
		  	//debug_r($SDrows);
		  	
		  	# generate report
		  	# loop by student
		  	$std_counter = 0;
		  	$returnRX = "<div class='chart_tables'><table class='common_table_list_v30 view_table_list_v30' id='resultTable'>";
		  	$returnRX .= "<thead>";
		  	# header !!!		  	
		  	# determine colspan
		  	$tmpRX .= "<tr><th colspan='1'>&nbsp;</th>";
		  	$tmpRX2 .= "<th colspan='1'>&nbsp;</th>";
		  	for ($i_year=sizeof($AcademicYearArr)-1; $i_year>=0; $i_year--)
		  	{
		  		$tmpRXInside = "";
		  		$tmpRXInside2 = "";
		  		$AcademicYearID = $AcademicYearArr[$i_year]["AcademicYearID"];
		  		$AcademicYearName = $AcademicYearArr[$i_year]["AcademicYearName"];
		  		for ($i_header=0; $i_header<sizeof($HeaderRows); $i_header++)
  				{
  					$HeaderObj = $HeaderRows[$i_header];
  					if ($HeaderObj["AcademicYearID"]==$AcademicYearID)
  					{
  						$HeaderYearSpan[$AcademicYearID] += $displayDataColumnCount;
	  					if ($DataType=="ASSESSMENT"){
		  					if ($HeaderObj["TermAssessment"]!=""){
		  						$tmpRXInside .= "<th colspan='{$displayDataColumnCount}' align='center'>".$HeaderObj["TermAssessment"]."</th>";
		  					}
			  			} elseif ($DataType=="TERM"){
			  				if ($HeaderObj["TermAssessment"]==NULL && $HeaderObj["YearTermID"]!=""){
		  						$tmpRXInside .= "<th colspan='{$displayDataColumnCount}' align='center'>".$HeaderObj["TermAssessment"]."</th>";
		  					}
			  			} elseif($DataType=="ASSESSMENT,TERM"){
		  					if ($HeaderObj["TermAssessment"]!=""){
		  						$tmpRXInside .= "<th colspan='{$displayDataColumnCount}' align='center'>".$HeaderObj["TermAssessment"]."</th>";
		  					}else if ($HeaderObj["TermAssessment"]==NULL && $HeaderObj["YearTermID"]!=""){
		  						$tmpRXInside .= "<th colspan='{$displayDataColumnCount}' align='center'>T".$allTermIndex[$HeaderObj["AcademicYearID"]][$HeaderObj["YearTermID"]]."</th>";
		  					}
			  			} elseif($DataType == "ALL"){
		  					if ($HeaderObj["TermAssessment"]!=""){
		  						$tmpRXInside .= "<th colspan='{$displayDataColumnCount}' align='center'>".$HeaderObj["TermAssessment"]."</th>";
		  					}else if ($HeaderObj["TermAssessment"]==NULL && $HeaderObj["YearTermID"]!="") {
		  						$tmpRXInside .= "<th colspan='{$displayDataColumnCount}' align='center'>T".$allTermIndex[$HeaderObj["AcademicYearID"]][$HeaderObj["YearTermID"]]."</th>";
		  					}else if ($HeaderObj["TermAssessment"]==NULL && $HeaderObj["YearTermID"] == NULL) {
		  						$tmpRXInside .= "<th colspan='{$displayDataColumnCount}' align='center'>".$ec_iPortfolio['overall_result']."</th>";
		  					}
			  			}
			  			
						if(in_array('filterMark', $filterColumnArr))
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['Score']}</th>";
						if(in_array('filterMarkDiff', $filterColumnArr))
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['MarkDifferenceHTML']}</th>";
						if(in_array('filterStandardScore', $filterColumnArr))
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['StandardScore']}</th>";
						if(in_array('filterStandardScoreDiff', $filterColumnArr))
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['StandardScoreDifferenceHTML']}</th>";
						if(in_array('filterFormPosition', $filterColumnArr))
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['FormPosition']}</th>";
						if(in_array('filterFormPositionDiff', $filterColumnArr))
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['FormPositionDifference']}</th>";
  					}
  				}
  				if ($tmpRXInside!="")
  				{
  					$colspan = 1;
  					if(in_array('filterAge', $filterColumnArr)){
  						$colspan++;
  					}
  					if(in_array('filterPrimarySchool', $filterColumnArr)){
  						$colspan++;
  					}
  					if(in_array('filterPercentile', $filterColumnArr)){
  						$colspan++;
  					}
		  			$tmpRX .= "<th align='center' colspan='{$colspan}' NoWrap>{$iPort['usertype_s']}</th>";
		  			$tmpRX .= $tmpRXInside;
		  			$tmpRX2 .= "<th align='center'>&nbsp;</th>";
 		  			if(in_array('filterAge', $filterColumnArr)){
 		  				$tmpRX2 .= "<th align='center' NoWrap>{$ec_iPortfolio['age']}</td>";
 		  			}
 		  			if(in_array('filterPrimarySchool', $filterColumnArr)){
 		  				$tmpRX2 .= "<th align='center' NoWrap>{$ec_iPortfolio['primarySchool']}</td>";
 		  			}
 		  			if(in_array('filterPercentile', $filterColumnArr)){
 		  				$tmpRX2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['OEA']['AcademicArr']['OverallRating']}</th>";
 		  			}
		  			$tmpRX2 .= $tmpRXInside2;
  				}
		  	}
		  	$tmpRX .= "</tr>";
		  	$tmpRX2 .= "</tr>";
		  	
		  	# generate header of years
		  	$returnRX .= "<tr ><th colspan='1'>&nbsp;</th>";
		  	for ($i_year=sizeof($AcademicYearArr)-1; $i_year>=0; $i_year--)
		  	{
		  		$AcademicYearID = $AcademicYearArr[$i_year]["AcademicYearID"];
		  		$AcademicYearName = $AcademicYearArr[$i_year]["AcademicYearName"];
		  		if ($HeaderYearSpan[$AcademicYearID]>0)
		  		{
		  			$colspan = ($HeaderYearSpan[$AcademicYearID]+1);
		  			if(in_array('filterAge', $filterColumnArr)){
		  				$colspan++;
		  			}
		  			if(in_array('filterPrimarySchool', $filterColumnArr)){
		  				$colspan++;
		  			}
		  			if(in_array('filterPercentile', $filterColumnArr)){
		  				$colspan++;
		  			}
		  			$returnRX .= "<th colspan='".$colspan."' align='center'>".$AcademicYearName."</th>\n";
		  		}
		  	}
		  	$returnRX .= "</tr>";
		  	
		  	$returnRX .= $tmpRX . $tmpRX2;
		  	$returnRX.= "</thead>";
		  			
		  	$hasData = false;
		  	for ($i_std=0; $i_std<sizeof($orderClassStudentArr); $i_std++)
		  	{
		  		//debug_r($ClassStudentArr[$i_std]);
		  		$StdObj = $orderClassStudentArr[$i_std];
		  		$studentName = $StdObj['StudentName'];
		  		$studentID = $StdObj["UserID"];
		  		
		  		

		  		$HasResults = false;

		  		$returnTR = "<tr ".($std_counter%2==1 ? "class='row_waiting'" : "" )."><td>".($std_counter+1)."</td>";
		  		
		  		$StdPreMark[$studentID]["Score"] = "";
		  		$StdPreMark[$studentID]["StandardScore"] = "";
		  		
		  		//debug_r($StdObj);
		  		# loop by year
		  		for ($i_year=sizeof($AcademicYearArr)-1; $i_year>=0; $i_year--)
		  		{
		  			//debug_r($AcademicYearArr[$i_year]);
		  			# loop by Assessment or Term or overall
		  			
		  			$AcademicYearID = $AcademicYearArr[$i_year]["AcademicYearID"];
		  			$AcademicYearName = $AcademicYearArr[$i_year]["AcademicYearName"];
		  			//debug($AcademicYearID, $AcademicYearName);
		  			
		  			# class and class number in that year
		  			//debug_r($StudentYearClassInfo[$studentID]);
		  			$StdClassName = trim($StudentYearClassInfo[$studentID][$AcademicYearID]["ClassName"]);
		  			if ($StdClassName=="")
		  			{
		  				$StdClassName = $SymbolEmpty;
		  			}
		  			$StdClassNumber = $StudentYearClassInfo[$studentID][$AcademicYearID]["ClassNumber"];
		  			if ($StdClassNumber=="")
		  			{
		  				$StdClassNumber = $SymbolEmpty;
		  			}
		  			
		  			$RecordCells = "";
		  			
		  			# order by assessment title/code
		  			if ($DataType=="TERM" || $DataType=="ASSESSMENT" || $DataType=="ASSESSMENT,TERM" || $DataType=="ALL")
		  			{
		  				# loop records
		  				for ($i_header=0; $i_header<sizeof($HeaderRows); $i_header++)
		  				{
		  					$HeaderObj = $HeaderRows[$i_header];
		  					$TermAssessment = ($HeaderObj["TermAssessment"]=="" || $HeaderObj["TermAssessment"]==NULL) ? 0 :$HeaderObj["TermAssessment"];
		  					$YearTermID = ($HeaderObj["YearTermID"]=="" || $HeaderObj["YearTermID"]==NULL) ? 0 :$HeaderObj["YearTermID"];
		  					
		  					if (
		  						$HeaderObj["AcademicYearID"]==$AcademicYearID && 
		  						(
		  							($DataType=="ASSESSMENT" && $HeaderObj["TermAssessment"]!=NULL) || 
		  							($DataType=="TERM" && $TermAssessment===0 && $YearTermID>0) ||
		  							($DataType=="ASSESSMENT,TERM" && (
		  								($HeaderObj["TermAssessment"]!=NULL) || ($TermAssessment===0 && $YearTermID>0) 
	  								)) || 
		  							($DataType=="ALL")
		  						)
		  					){
		  						$isAnnual = 0;
		  						if($TermAssessment===0 && $YearTermID===0){
		  							$isAnnual = 1;
		  							$type = 'Overall';
		  						}else if($TermAssessment===0){
		  							$type = 'Term';
		  						}else{
		  							$type = 'TermAssessment';
		  						}
		  						//debug($AcademicYearID, $HeaderObj["YearTermID"], $HeaderObj["TermAssessment"]);
		  						$ScoreNow = $RawMarkRows[$studentID][$AcademicYearID][$YearTermID][$TermAssessment]["Score"];
		  						$Grade = $RawMarkRows[$studentID][$AcademicYearID][$YearTermID][$TermAssessment]["Grade"];
		  						$OrderMeritForm = $RawMarkRows[$studentID][$AcademicYearID][$YearTermID][$TermAssessment]["OrderMeritForm"];
		  						$OrderMeritFormTotal = $RawMarkRows[$studentID][$AcademicYearID][$YearTermID][$TermAssessment]["OrderMeritFormTotal"];
		  						$SubjectID = $RawMarkRows[$studentID][$AcademicYearID][$YearTermID][$TermAssessment]["SubjectID"];
		  						
		  						$PositionShow = $OrderMeritForm;
		  						if ($OrderMeritFormTotal!="" && $PositionShow!="")
		  						{
		  							$PositionShow .= "/".$OrderMeritFormTotal;
		  						}
		  						
		  						if ($ScoreNow!="" && $ScoreNow>=0 || ($ScoreNow == -1 && $Grade != ''))
		  						{
		  							$SubjectComponentID = 0; // not support subject component at this moment

		  							$Mean = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][$TermAssessment][$isAnnual][0][$ClassMapYear[$YearClassIDMapping[$studentID][$AcademicYearID]]]["MEAN"], 2);
		  							$SD = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][$TermAssessment][$isAnnual][0][$ClassMapYear[$YearClassIDMapping[$studentID][$AcademicYearID]]]["SD"], 2);
		  							
		  							if ($SD>0)
		  							{
		  								$StandardScore = round(($ScoreNow-$Mean)/$SD, 2);
		  							} else
		  							{
		  								$StandardScore = $SymbolEmpty;
		  							}
		  							//debug($ScoreNow, $Mean, $SD, $StandardScore);
									$ScoreDiffStyleA = "";
									$ScoreDiffStyleB = "";
									$StandardScoreDiffStyleA = "";
									$StandardScoreDiffStyleB = "";
									$OrderMeritFormDiffStyleA = "";
									$StandardScoreDiffStyleB = "";
			  						if ($StdPreMark[$studentID][$type]["Score"]!="" && $StdPreMark[$studentID][$type]["Score"] != 0)
			  						{
			  							$ScoreDiffPercent = ROUND(100*($ScoreNow-$StdPreMark[$studentID][$type]["Score"])/$StdPreMark[$studentID][$type]["Score"], 2);
										if ($ScoreDiffPercent>0)
										{
											$ScoreDiffStyleA = "<font color='green'>";
											$ScoreDiffStyleB = "</font>";
										} elseif ($ScoreDiffPercent<0)
										{
											$ScoreDiffStyleA = "<font color='red'>";
											$ScoreDiffStyleB = "</font>";											
										}
			  						} else
			  						{
			  							$ScoreDiffPercent = $SymbolEmpty;
			  						}
			  						//debug($StdPreMark[$StdObj["UserID"]]["StandardScore"]);
			  						if ($StdPreMark[$studentID][$type]["StandardScore"]!="")
			  						{
			  							$StandardScoreDiffPercent = round($StandardScore-$StdPreMark[$studentID][$type]["StandardScore"], 2);
			  							
			  							if ($StandardScoreDiffPercent>0)
										{
											$StandardScoreDiffStyleA = "<font color='green'>";
											$StandardScoreDiffStyleB = "</font>";
										} elseif ($StandardScoreDiffPercent<0)
										{
											$StandardScoreDiffStyleA = "<font color='red'>";
											$StandardScoreDiffStyleB = "</font>";											
										}
			  						} else
			  						{
			  							$StandardScoreDiffPercent = $SymbolEmpty;
			  						}
			  						
			  						if ($StdPreMark[$studentID][$type]["MeritForm"]!="")
			  						{
			  							$OrderMeritFormDiff = round($OrderMeritForm-$StdPreMark[$studentID][$type]["MeritForm"], 2);
			  							if($OrderMeritFormDiff != 0){
			  								$OrderMeritFormDiff *= -1;
			  							}

			  							if ($OrderMeritFormDiff>0)
										{
											$OrderMeritFormDiffStyleA = "<font color='green'>";
											$OrderMeritFormDiffStyleB = "</font>";
										} elseif ($OrderMeritFormDiff<0)
										{
											$OrderMeritFormDiffStyleA = "<font color='red'>";
											$OrderMeritFormDiffStyleB = "</font>";											
										}
			  						} else
			  						{
			  							$OrderMeritFormDiff = $SymbolEmpty;
			  						}
			  						
			  			
									if(in_array('filterMark', $filterColumnArr))
				  						$RecordCells .= "<td align='center' NoWrap>". (($ScoreNow == -1)?$Grade:$ScoreNow) ."</td>";  // score
									if(in_array('filterMarkDiff', $filterColumnArr))
				  						$RecordCells .= "<td align='center' NoWrap>".$ScoreDiffStyleA.$ScoreDiffPercent.($ScoreDiffPercent!=$SymbolEmpty ? "%" : "").$ScoreDiffStyleB."</td>";  // score difference
									if(in_array('filterStandardScore', $filterColumnArr))
										$RecordCells .= "<td align='center' NoWrap>".(($ScoreNow == -1)?$SymbolEmpty:$StandardScore)."</td>";  // standard score
									if(in_array('filterStandardScoreDiff', $filterColumnArr))
										$RecordCells .= "<td align='center' NoWrap>".(($ScoreNow == -1)?$SymbolEmpty:$StandardScoreDiffStyleA.$StandardScoreDiffPercent.$StandardScoreDiffStyleB)."</td>";  // standard score difference
									if(in_array('filterFormPosition', $filterColumnArr))
										$RecordCells .= "<td align='center' NoWrap>".(($PositionShow)?$PositionShow:$SymbolEmpty)."</td>";  //ranking in form
									if(in_array('filterFormPositionDiff', $filterColumnArr))
				  						$RecordCells .= "<td align='center' NoWrap>".$OrderMeritFormDiffStyleA.$OrderMeritFormDiff.$OrderMeritFormDiffStyleB."</td>";  //ranking in form
									//"<td></td>";
									$StdPreMark[$studentID][$type]["Score"] = $ScoreNow;									
									$StdPreMark[$studentID][$type]["StandardScore"] = $StandardScore;
									$StdPreMark[$studentID][$type]["MeritForm"] = $OrderMeritForm;
									$HasResults = true;
									$hasData= true;
		  						} else
		  						{
		  							foreach($filterColumnArr as $filter){
		  								if($filter == 'filterAge' || $filter == 'filterPrimarySchool' || $filter == 'filterPercentile'){
		  									continue;
		  								}
		  								$RecordCells .= "<td align='center' NoWrap>$SymbolEmpty</td>";
		  							}
		  							//$RecordCells .= "<td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td>";
								}
		  					}
		  				}
		  			}
		  			
		  			if ($RecordCells!="")
		  			{
		  				$studentInfo = "{$studentName}<br />{$StdClassName} - {$StdClassNumber}";
		  				$returnTR .= "<td align='center' NoWrap>".$studentInfo."</td>";
		  				
		  				if(in_array('filterAge', $filterColumnArr)){
		  					$startYear = substr($academicYearStartArr[$AcademicYearID], 0, 4);
		  					$yearDiff = date('Y') - $startYear;
		  					$age = ($StdObj['Age'])? ($StdObj['Age'] - $yearDiff) : $SymbolEmpty;
			  				$returnTR .= "<td align='center' NoWrap>{$age}</td>";		  					
		  				}
		  				
		  				if(in_array('filterPrimarySchool', $filterColumnArr)){
		  					$pschool = ($StdObj['PrimarySchool'])? $StdObj['PrimarySchool'] : $SymbolEmpty;
			  				$returnTR .= "<td align='center' NoWrap>{$pschool}</td>";		  					
		  				}
		  				if(in_array('filterPercentile', $filterColumnArr)){
			  				$meritDetails = $studentRank[$AcademicYearID][''][''][$studentID];
			  				$title = ($meritDetails['MeritRankTitle'])? $meritDetails['MeritRankTitle']: $SymbolEmpty;
			  				$returnTR .= "<td align='center' NoWrap>
			  					{$title}<br />
			  					<!-- [ {$meritDetails['OrderMeritForm']} / {$meritDetails['OrderMeritFormTotal']} ] --> 
			  				</td>";
		  				}
		  				$returnTR .= $RecordCells;
		  			}
		  			
		  		}
		  		
		  		if ($HasResults)
		  		{
		  			$returnRX .= $returnTR."</tr>\n";
		  			$std_counter ++;
		  		}
		  	} 

			if(!$hasData){
				$returnRX .= "<tr><td colspan='99' align='center'>{$Lang['General']['NoRecordFound']}</td></tr>\n";
			}
		  	$returnRX .= "</table></div>";
		  	$returnRX .= '<span class="tabletextremark">';
		  	$returnRX .= $ec_iPortfolio['OMFHighlightRemarks_Improve'];
		  	$returnRX .= "</br>";
		  	$returnRX .= $ec_iPortfolio['OMFHighlightRemarks'];
		  	$returnRX .= "</span>";
		  	$floatHeader = <<<HTML
		  		<link href="/templates/{$LAYOUT_SKIN}/css/content_30.css" rel="stylesheet" type="text/css">
  				<script>\$(function() {\$('#resultTable').floatHeader();});</script>
HTML;
		  	return $floatHeader.$returnRX;
	  	
	  }
	  
	  function GetStudentCrossYearResults($StudentID, $filterColumnArr = array(), $isExport = false, $fileName='')
	  {
		  	global $eclass_db, $Lang, $intranet_session_language, $ec_iPortfolio, $iPort, $PATH_WRT_ROOT, $intranet_root, $sys_custom, $LAYOUT_SKIN;
		  	$SymbolEmpty = $Lang['General']['EmptySymbol'] ;
	  		$filterColumnArr = (array)$filterColumnArr;
	  		$displayDataColumnCount = count($filterColumnArr);
	  		// Non Admin Checking
	  		$accessRight = $this->getAssessmentStatReportAccessRight();
	  		if(in_array('filterFormSplineDiagram', $filterColumnArr)){
	  			$displayDataColumnCount--;
	  		}
	  		if(in_array('filterFormPercentileSplineDiagram', $filterColumnArr)){
	  			$displayDataColumnCount--;
	  		}
	  		if(in_array('filterTeacher', $filterColumnArr)){
	  			$displayDataColumnCount--;
	  		}
	  		
	  		include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	  		$lrc = new libreportcard();
	  		include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
	  		$libinterface = new interface_html();
	  		
	  		$exportArr = array();
	  		$exportYearArr = array();
	  		
	  		$hideEmptyTermResult = $sys_custom['SDAS']['StudentPerformanceTracking']['HideEmptyTermColumn'];
	  		$showComponent = true;
	  		
	  		#### Get releated subject START ####
	  		$releatedSubjectIDs = $this->getReleatedSubjectID();
	  		#### Get releated subject END ####
	  			  		
		  	# Get Student Name
		  	$student = new libuser($StudentID);
		  	$studentName = $student->UserNameLang();
		  	
		  	$sql = "SELECT DISTINCT AcademicYearID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE UserID='{$StudentID}' ";
		  	$AcademicYearIDs = $this->returnVector($sql);
		  	$AcademicYearIDs = array_filter($AcademicYearIDs);
		  	
		  	# get year in order
			/* Old Method
    		  	$lib_academic_year = new academic_year();
    			# note, desc order now
    			$AcademicYearArr = $lib_academic_year->Get_All_Year_List("", false, array(), $AcademicYearIDs);
			*/
		  	$AcademicYearIdSql = implode("','", $AcademicYearIDs);
			$yearNameSQL = Get_Lang_Selection('YearNameB5', 'YearNameEN');
			$sql = "SELECT DISTINCT
				AY.AcademicYearID,
				AY.{$yearNameSQL} as AcademicYearName
            FROM 
            	ACADEMIC_YEAR AY
            INNER JOIN
            	ACADEMIC_YEAR_TERM AYT
            ON
            	AY.AcademicYearID = AYT.AcademicYearID
            WHERE
                AY.AcademicYearID IN ('{$AcademicYearIdSql}')
            ORDER BY
            	AYT.TermStart DESC";
			$AcademicYearArr = $this->returnResultSet($sql);
			
			
			# get assessment and term in order
			# studentIDs, academicyearIDs, 
			
			$sql = "SELECT DISTINCT AcademicYearID, YearTermID, TermAssessment  FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE UserID='{$StudentID}' ORDER BY YearTermID, TermAssessment";
		  	$AcademicYearTerms = $this->returnResultSet($sql);
// 			debug_r($AcademicYearTerms);
		  	
		  	
			$sql = "SELECT DISTINCT AcademicYearID, ClassName, ClassNumber FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE UserID='{$StudentID}'";
		  	$InfoRows = $this->returnResultSet($sql);
		  	for ($i=0; $i<sizeof($InfoRows); $i++)
		  	{
		  		$StudentClassInfo[$InfoRows[$i]["AcademicYearID"]] = array("ClassName"=>$InfoRows[$i]["ClassName"], "ClassNumber"=>$InfoRows[$i]["ClassNumber"]);
		  	}
			//debug_rt($InfoRows);
			//debug_r($StudentClassInfo);
		  	
			
			$sql_fields = "SubjectID, UserID, ClassName, ClassNumber, YearClassID, AcademicYearID, YearTermID, TermAssessment, Score, OrderMeritForm, OrderMeritFormTotal, Grade";
			
			$sql_fields .= ", SubjectComponentID";
			if($showComponent){
				$sql_cond = "UserID IN ({$StudentID}) ";
			} else { 
				$sql_cond = "UserID IN ({$StudentID}) AND SubjectComponentID IS NULL ";
	  		}
			if(!$accessRight['admin'] && !$accessRight['subjectPanel']){
			    $subjectIDAry = array();
			    include_once('subject_class_mapping.php');
			    $sbj = new subject();
			    foreach($accessRight['classTeacher'] as $class){
			        $cYearID = $class['YearID'];
			        $subjects = $sbj->Get_Subject_By_Form($cYearID);
			        $sIDAry = Get_Array_By_key($subjects, 'RecordID');
			        $subjectIDAry = array_merge($subjectIDAry, $sIDAry);
			    }
			    foreach($accessRight['subjectGroup'] as $subjectGroup){
			        $sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
			        $subjectIDAry[] = $sGroup->SubjectID;
			    }
			    if(sizeof($subjectIDAry) > 0){
			        $sql_cond .= " AND SubjectID In ('" . implode("','", $subjectIDAry) . "') ";
			    }
			}
//			$sql_cond = "UserID IN ({$StudentID}) AND SubjectComponentID IS NULL AND IsAnnual=0 ";
			$sql_order = "SubjectID, AcademicYearID, TermAssessment, RecordID";


			$sql = "SELECT DISTINCT {$sql_fields} FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE {$sql_cond} ORDER BY {$sql_order}";
			$StudentMarks = $this->returnResultSet($sql);
			//debug_rt($StudentMarks);
			$StudentMarksArr = BuildMultiKeyAssoc($StudentMarks, array('AcademicYearID', 'YearTermID', 'SubjectID', 'SubjectComponentID','TermAssessment') , array('Score','OrderMeritForm','OrderMeritFormTotal', 'Grade', 'YearClassID'));
// 			debug_pr($StudentMarksArr);

			# 2020-05-05 (Philips) [2020-0225-1050-31235] - Hide Empty Term Result Column
			$StudentTermMarksArr = BuildMultiKeyAssoc($StudentMarks, array('AcademicYearID', 'YearTermID','TermAssessment', 'SubjectID'));
			$lib_year = new Year();
			$AllClasses = $lib_year->Get_All_Classes("", -1);
			for ($i=0; $i<sizeof($AllClasses); $i++)
			{
				$ClassMapYear[$AllClasses[$i]["YearClassID"]] = $AllClasses[$i]["YearID"];
			}
			//debug_r($ClassMapYear);
		  	
		  	//debug_r($rows);
		  	
		  	$libsubject = new subject();
		  	$AllSubjects = $libsubject->Get_All_Subjects();
		  	$AllCmpSubjects = $libsubject->Get_Subject_Component();
		  	$AllCmpSubjects = BuildMultiKeyAssoc($AllCmpSubjects, 'RecordID');
		  	
		  	######### Report START ########
		  	$resultArr = array();
		  	$Stdrk = array();
		  	$formPercentileChartArr = array();
		  	$yearTermArr = array();
//		  	for ($i_year=0; $i_year<sizeof($AcademicYearArr); $i_year++)
		  	for ($i_year=sizeof($AcademicYearArr)-1; $i_year>=0; $i_year--)
		  	{
		  		$chartArr = array(); // For High Chart
		  		$AcademicYearID = $AcademicYearArr[$i_year]["AcademicYearID"];
		  		$AcademicYearName = $AcademicYearArr[$i_year]["AcademicYearName"];
		  		$chartArr['YearName'] = $AcademicYearName;
		  		$chartArr['Terms'] = array();
		  		$chartArr['Data'] = array();
		  	
				$sql = "SELECT DISTINCT SubjectID  FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE UserID='{$StudentID}' AND AcademicYearID='{$AcademicYearID}' ";
			  	$InvolvedSubjects = $this->returnVector($sql);
			  	$InvolvedCmpSubjectsArr = array();
			  	foreach($InvolvedSubjects as $tempSubj) $InvolvedCmpSubjectsArr[$tempSubj][] = '';
				//debug_r($InvolvedSubjects);
				
			  	if($showComponent){
				  	$sql = "SELECT SubjectID, SubjectComponentID  FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE UserID='{$StudentID}' AND AcademicYearID='{$AcademicYearID}' AND SubjectComponentID IS NOT NULL GROUP BY SubjectComponentID";
				  	$InvolvedCmpSubjects = $this->returnArray($sql);
				  	foreach($InvolvedCmpSubjects as $CmpSubj){
				  		$InvolvedCmpSubjectsArr[$CmpSubj['SubjectID']][] = $CmpSubj['SubjectComponentID'];
				  	}
			  	}
				
			  	# retrieve SD 
				$SDrows = $this->GetSDandMean(array($AcademicYearID));
		  		
		  		$lib_academic_year = new academic_year($AcademicYearID);
		  		$TermArr = ($lib_academic_year->Get_Term_List());
		  		$TermArr[0] = $ec_iPortfolio['overall_result'];
		  		//debug_r($TermArr);
		  		
		  		$YearColSpan = 0;
		  		if (sizeof($TermArr)>0)
		  		{
		  			foreach ($TermArr as $TermID => $TermName)
		  			{
		  				for ($i_yesr_term=0; $i_yesr_term<sizeof($AcademicYearTerms); $i_yesr_term++)
		  				{
		  					if ($AcademicYearTerms[$i_yesr_term]["YearTermID"]==$TermID && $AcademicYearTerms[$i_yesr_term]["TermAssessment"]!="")
		  					{
		  						$TermAssessmentHeaders[$TermID][] = $AcademicYearTerms[$i_yesr_term]["TermAssessment"];
		  						$YearColSpan += 5;
		  					}
		  				}
		  			}
		  		}
		  		
		  		$YearColSpan += 99; //sizeof($TermArr) * $displayDataColumnCount + 1;
		  		
		  		$returnRX = "<div class='chart_tables'><table class='common_table_list_v30 view_table_list_v30 resultTable' >";
			  	$returnRX .= "<thead>";
			  	# header !!!		  	
			  	# determine colspan
			  	$returnRX .= "<tr><th colspan='{$YearColSpan}'>{$AcademicYearName}</th></tr>";
			  	$exportRow = array(array($AcademicYearName));
			  	$_column = array();
			  	$_column2 = array();
			  	
			  	$_studentInfo = "{$studentName}<br />{$StudentClassInfo[$AcademicYearID]['ClassName']} - {$StudentClassInfo[$AcademicYearID]['ClassNumber']}";
			  	$returnRX .= "<tr><th NoWrap>{$_studentInfo}</th>";
			  	$_column[] = "{$studentName}\n{$StudentClassInfo[$AcademicYearID]['ClassName']} - {$StudentClassInfo[$AcademicYearID]['ClassNumber']}";
			  				  	
			  	$tmpRXInside2 = "<tr><th>".$Lang['SysMgr']['FormClassMapping']['Subject']."</th>";
			  	$_column2[] = $Lang['SysMgr']['FormClassMapping']['Subject'];
			  	
			  	if(in_array('filterTeacher', $filterColumnArr)){
			  		$returnRX .= "<th NoWrap>&nbsp;</th>";
			  		$_column[] = '';
			  		$tmpRXInside2 .= "<th>{$iPort['report_col']['teacher']}</th>";
			  		$_column2[] = $iPort['report_col']['teacher'];
			  	}
			  	
			  	$hasScore = array();
			  	$hasTermScore = array();
			  	
			  	if (sizeof($TermArr)>0)
		  		{
		  			foreach ($TermArr as $TermID => $TermName)
		  			{
		  				#### Skip term if no score START ####
		  				if($TermID == 0){
		  					$MarkObj = $StudentMarksArr[$AcademicYearID][''];
		  					
		  					# 2020-05-05 (Philips) [2020-0225-1050-31235] - Hide Empty Term Result Column
		  					$hasTermScore[$AcademicYearID][$TermID] = is_array($StudentTermMarksArr[$AcademicYearID]['']);
		  					#### Skip term if no score END ####
		  				}else{
		  					$MarkObj = $StudentMarksArr[$AcademicYearID][$TermID];
		  					
		  					# 2020-05-05 (Philips) [2020-0225-1050-31235] - Hide Empty Term Result Column
		  					$hasTermScore[$AcademicYearID][$TermID] = is_array($StudentTermMarksArr[$AcademicYearID][$TermID]['']);
		  					#### Skip term if no score END ####
		  				}
		  				if($MarkObj == null){
		  					continue;
		  				}
		  				$hasScore[$AcademicYearID][$TermID] = true;

		  				for ($i_term_id=0; $i_term_id<sizeof($TermAssessmentHeaders[$TermID]); $i_term_id++)
		  				{
		  					$returnRX .= "<th colspan='{$displayDataColumnCount}'>".$TermAssessmentHeaders[$TermID][$i_term_id]."</th>";
		  					$_column[] = $TermAssessmentHeaders[$TermID][$i_term_id];
		  					$chartArr['Terms'][] = $TermAssessmentHeaders[$TermID][$i_term_id];
		  					for ($i = 0, $iMax = $displayDataColumnCount-1; $i < $iMax; $i++) {
		  						$_column[] = '';
		  					}
		  					
							if(in_array('filterMark', $filterColumnArr)){
		  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['Score']}</th>";
		  						$_column2[] = $Lang['iPortfolio']['Score'];
							}
							if(in_array('filterMarkDiff', $filterColumnArr)){
		  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['MarkDifferenceHTML']}</th>";
		  						$_column2[] = $Lang['iPortfolio']['MarkDifference'];
							}
							if(in_array('filterStandardScore', $filterColumnArr)){
		  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['StandardScore']}</th>";
		  						$_column2[] = $Lang['iPortfolio']['StandardScore'];
							}
							if(in_array('filterStandardScoreDiff', $filterColumnArr)){
		  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['StandardScoreDifferenceHTML']}</th>";
		  						$_column2[] = $Lang['iPortfolio']['StandardScoreDifference'];
							}
							if(in_array('filterFormPosition', $filterColumnArr)){
		  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['FormPosition']}</th>";
		  						$_column2[] = $Lang['iPortfolio']['FormPosition'];
							}
							if(in_array('filterFormPositionDiff', $filterColumnArr)){
		  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['FormPositionDifference']}</th>";
		  						$_column2[] = $Lang['iPortfolio']['FormPositionDifference'];
							}
							if(in_array('filterFormPositionPercentile', $filterColumnArr)){
								$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['Percentile']['PercentileRank']}</th>";
								$_column2[] = $Lang['iPortfolio']['Percentile']['PercentileRank'];
							}
		  					//$tmpRXInside2 .= "<th align='center' NoWrap>".$Lang['iPortfolio']["Score"]."</th><th align='center' NoWrap>".$Lang['iPortfolio']["MarkDifferenceHTML"]."</th><th align='center' NoWrap>".$Lang['iPortfolio']["StandardScore"]."</th><th align='center' NoWrap>".$Lang['iPortfolio']["StandardScoreDifferenceHTML"]."</th><th align='center' NoWrap>".$Lang['iPortfolio']["FormPosition"]."</th>";
		  				}
		  				# 2020-05-05 (Philips) [2020-0225-1050-31235] - Hide Empty Term Result Column
		  				if(!$hasTermScore[$AcademicYearID][$TermID] && $hideEmptyTermResult){
		  					continue;
		  				}
		  				$returnRX .= "<th colspan='{$displayDataColumnCount}'>".$TermName."</th>";
		  				$_column[] = $TermName;
		  				$chartArr['Terms'][] = $TermName;
	  					for ($i = 0, $iMax = $displayDataColumnCount-1; $i < $iMax; $i++) {
	  						$_column[] = '';
	  					}
	  					
						if(in_array('filterMark', $filterColumnArr)){
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['Score']}</th>";
	  						$_column2[] = $Lang['iPortfolio']['Score'];
						}
						if(in_array('filterMarkDiff', $filterColumnArr)){
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['MarkDifferenceHTML']}</th>";
	  						$_column2[] = $Lang['iPortfolio']['MarkDifference'];
						}
						if(in_array('filterStandardScore', $filterColumnArr)){
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['StandardScore']}</th>";
	  						$_column2[] = $Lang['iPortfolio']['StandardScore'];
						}
						if(in_array('filterStandardScoreDiff', $filterColumnArr)){
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['StandardScoreDifferenceHTML']}</th>";
	  						$_column2[] = $Lang['iPortfolio']['StandardScoreDifference'];
						}
						if(in_array('filterFormPosition', $filterColumnArr)){
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['FormPosition']}</th>";
	  						$_column2[] = $Lang['iPortfolio']['FormPosition'];
						}
						if(in_array('filterFormPositionDiff', $filterColumnArr)){
	  						$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['FormPositionDifference']}</th>";
	  						$_column2[] = $Lang['iPortfolio']['FormPositionDifference'];
						}
						if(in_array('filterFormPositionPercentile', $filterColumnArr)){
							$tmpRXInside2 .= "<th align='center' NoWrap>{$Lang['iPortfolio']['Percentile']['PercentileRank']}</th>";
							$_column2[] = $Lang['iPortfolio']['Percentile']['PercentileRank'];
						}
		  				//$tmpRXInside2 .= "<th align='center' NoWrap>".$Lang['iPortfolio']["Score"]."</th><th align='center' NoWrap>".$Lang['iPortfolio']["MarkDifferenceHTML"]."</th><th align='center' NoWrap>".$Lang['iPortfolio']["StandardScore"]."</th><th align='center' NoWrap>".$Lang['iPortfolio']["StandardScoreDifferenceHTML"]."</th><th align='center' NoWrap>".$Lang['iPortfolio']["FormPosition"]."</th>";
		  			}
		  		}
		  		$returnRX .= "</tr>";
		  		$tmpRXInside2 .= "</tr>";
		  		$returnRX .= $tmpRXInside2;
			  	$returnRX .= "</thead>";
			  	$returnRX .= "<tbody>";
			  	$exportRow[] = $_column;
			  	$exportRow[] = $_column2;
			  	
			  	#### Table Content START ####
			  	$RecordCells = "";

			  	# results
		  		$_column = array();
			  	for ($i_subject=0; $i_subject<sizeof($AllSubjects); $i_subject++)
			  	{
			  		if(!empty($_column)){
			  			$exportRow[] = $_column;
			  		}
			  		
// 			  		debug_pr($AllSubjects[$i_subject]);
			  		
			  		$SubjectID = $AllSubjects[$i_subject]["RecordID"];
			  		if(!$accessRight['admin'] && !$accessRight['subjectPanel'] && sizeof($subjectIDAry) > 0){
			  		    if(!in_array($SubjectID, $subjectIDAry)){
			  		        continue;
			  		    }
			  		}
			  		$releatedSubjectArr = array($SubjectID);
			  		$_column = array();
			  		
			  		$parentSubjectID = 0;
			  		foreach($releatedSubjectIDs as $parentID => $releatedSubject){
			  			if(in_array($SubjectID, $releatedSubject)){
			  				$parentSubjectID = $parentID;
			  				break;
			  			}
			  		}
			  		if (in_array($SubjectID, $InvolvedSubjects))
			  		{
			  			$cmpSubjAry = $InvolvedCmpSubjectsArr[$SubjectID];
			  			foreach($cmpSubjAry as $cmpSubj){
			  				$CmpID = $cmpSubj;
			  				$SubjectComponentID = $cmpSubj == '' ? 0 : $cmpSubj;
			  			
			  				# 2020-05-08 (Philips) Prevent same subject Name
			  				if($CmpID != ''){
			  					$_MainsubjectName = (($intranet_session_language=="b5") ? $AllSubjects[$i_subject]["CH_DES"] : $AllSubjects[$i_subject]["EN_DES"]);
			  					$_subjectName = (($intranet_session_language=="b5") ? $AllCmpSubjects[$CmpID]["CH_DES"] : $AllCmpSubjects[$CmpID]["EN_DES"]);
			  					$returnRX .= "<tr><td NoWrap><span style='font-weight: normal;'>&nbsp;&nbsp;&nbsp;".$_subjectName."</span></td>";
			  					$chartFPDataObj = array("Subject" => $_subjectName . "($_MainsubjectName)", "Percentile" => array());
			  					$chartDataObj = array("Subject" => $_subjectName . "($_MainsubjectName)", "Score" => array());
			  				} else {
			  					$_subjectName = (($intranet_session_language=="b5") ? $AllSubjects[$i_subject]["CH_DES"] : $AllSubjects[$i_subject]["EN_DES"]);
			  					$chartFPDataObj = array("Subject" => $_subjectName, "Percentile" => array());
			  					$chartDataObj = array("Subject" => $_subjectName, "Score" => array());
			  					$returnRX .= "<tr><td NoWrap>".$_subjectName."</td>";
			  				}
				  			$_column[] = $_subjectName;
// 				  			$chartDataObj = array("Subject" => $_subjectName, "Score" => array());
// 				  			$chartFPDataObj = array("Subject" => $_subjectName, "Percentile" => array());
				  			
				  			#### Get teacher Name START ####
						  	if(in_array('filterTeacher', $filterColumnArr)){
						  		$teacherNameArr = array();
					  			foreach ((array)$TermArr as $YearTermID => $TermName)
					  			{
					  				if($YearTermID == 0){
					  					continue;
					  				}
					  				if(!$hasScore[$AcademicYearID][$YearTermID]){
					  					continue;
					  				}
					  				
							  		$rs = $lrc->Get_Student_Subject_Teacher_ArchiveTeacher($YearTermID, $StudentID, $SubjectID);
							  		foreach($rs as $r){
							  			$Intranet_User = Get_Lang_Selection($r['IU_ChineseName'],$r['IU_EnglishName']);
							  			$Archive = Get_Lang_Selection($r['IAU_ChineseName'],$r['IAU_EnglishName']);
							  			if($Intranet_User){
							  				$teacherNameArr[] = $Intranet_User;
							  			}else{
							  				$teacherNameArr[] = $libinterface->RequiredSymbol().$Archive;
							  			}
							  		}
					  			}
					  			$teacherNameArr = array_unique($teacherNameArr);
					  			$teacherNameHTML = implode('<br />', $teacherNameArr);
					  			$teacherName = implode(', ', $teacherNameArr);
						  		$returnRX .= "<td NoWrap>".$teacherNameHTML."</td>";
						  		$_column[] = $teacherName;
						  	}
				  			#### Get teacher Name END ####
						  	
							######## TermAssessment and Term START ########
				  			foreach ((array)$TermArr as $YearTermID => $TermName)
				  			{
				  				if($YearTermID == 0){
				  					continue;
				  				}
				  				if(!$hasScore[$AcademicYearID][$YearTermID]){
				  					continue;
				  				}
				  				
								######## TermAssessment START ########
				  				for ($i_term_id=0; $i_term_id<sizeof($TermAssessmentHeaders[$YearTermID]); $i_term_id++)
				  				{
				  					$returnAssessmentRX = "";
									$TermAssessment = $TermAssessmentHeaders[$YearTermID][$i_term_id];
	// 								$CmpID = '';
									$MarkObj = $StudentMarksArr[$AcademicYearID][$YearTermID][$SubjectID][$CmpID][$TermAssessment];
				  					if(empty($MarkObj)){
										for($i=0,$iMax=$displayDataColumnCount;$i<$iMax;$i++){
											$returnRX .= "<td align='center'>$SymbolEmpty</td>";
											$_column[] = $SymbolEmpty;
										}
										$chartDataObj['Score'][] = 'null';
										$chartFPDataObj['Percentile'][] = 'null';
										$returnRX .= $returnAssessmentRX;
										continue;
				  					}
				  					
									$ScoreNow = $MarkObj["Score"];
									$chartDataObj['Score'][] = $ScoreNow;
											
									$PositionShow = $MarkObj["OrderMeritForm"];
									$OrderMeritFormTotal = $MarkObj["OrderMeritFormTotal"];
									
									if ($OrderMeritFormTotal!="" && $PositionShow!="")
									{
										$PositionShow .= "/".$OrderMeritFormTotal;
										# 2020-04-29 (Philips) [2019-0628-0954-56098] Form Position Percentile
										$PositionPercentile = 0;
										if(!($PositionShow<='0' || $OrderMeritFormTotal<='0')){
											$PositionPercentile = ( (int)$OrderMeritFormTotal - (int)$PositionShow + 1) / (int)$OrderMeritFormTotal  * 100;
										}
										$chartFPDataObj['Percentile'][] = $PositionPercentile == 0 ? 'null' : number_format($PositionPercentile, 2);
										$PositionPercentile = number_format($PositionPercentile, 2) . '%';
									}else{
										$PositionShow = $SymbolEmpty;
										
										$PositionPercentile = $SymbolEmpty;
										$chartFPDataObj['Percentile'][] = 'null';
									}
	// 								$formPercentileChartArr[] = $chartFPDataObj;
					
									//debug($ScoreNow, $TermName, $i_marks);
	// 								$SubjectComponentID = 0; // not support subject component at this moment
	
		  							$Mean = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][$TermAssessment][0][0][$ClassMapYear[$MarkObj["YearClassID"]]]["MEAN"], 2);
		  							$SD = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][$TermAssessment][0][0][$ClassMapYear[$MarkObj["YearClassID"]]]["SD"], 2);
		  							if ($SD>0) {
		  								$StandardScore = round(($ScoreNow-$Mean)/$SD, 2);
		  							} else {
		  								$StandardScore = $SymbolEmpty;
		  							}
		  							if ($ScoreNow!="" && $ScoreNow>=0){
	// 		  							$SubjectComponentID = 0; // not support subject component at this moment
	
			  							$Mean = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][$TermAssessment][0][0][$ClassMapYear[$MarkObj["YearClassID"]]]["MEAN"], 2);
			  							$SD = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][$TermAssessment][0][0][$ClassMapYear[$MarkObj["YearClassID"]]]["SD"], 2);
			  							
			  							if ($SD>0) {
			  								$StandardScore = round(($ScoreNow-$Mean)/$SD, 2);
			  							} else {
			  								$StandardScore = $SymbolEmpty;
			  							}
			  							//debug($ScoreNow, $Mean, $SD, $StandardScore);
										$ScoreDiffStyleA = "";
										$ScoreDiffStyleB = "";
										$StandardScoreDiffStyleA = "";
										$StandardScoreDiffStyleB = "";
										$MeritFormDiffStyleA = "";
										$MeritFormDiffStyleB = "";
										
										$preMark = "";
										if($CmpID==''){
											if ($StdPreMark[$SubjectID]["Assessment"]["Score"]!="") {
												$preMark = $StdPreMark[$SubjectID]["Assessment"]["Score"];
											}else{
												if($StdPreMark[$parentSubjectID]["Assessment"]["Score"] != ''){
													$preMark = $StdPreMark[$parentSubjectID]["Assessment"]["Score"];
												}
											}
										} else {
											if ($StdPreMark[$CmpID]["Assessment"]["Score"]!="") {
												$preMark = $StdPreMark[$CmpID]["Assessment"]["Score"];
											}else{
												if($StdPreMark[$parentSubjectID]["Assessment"]["Score"] != ''){
													$preMark = $StdPreMark[$parentSubjectID]["Assessment"]["Score"];
												}
											}
										}
				  						if ($preMark!="" && $preMark!="0") {
				  							$ScoreDiffPercent = ROUND(100*($ScoreNow-$preMark)/$preMark, 2);
											if ($ScoreDiffPercent>0) {
												$ScoreDiffStyleA = "<font color='green'>";
												$ScoreDiffStyleB = "</font>";
											} elseif ($ScoreDiffPercent<0) {
												$ScoreDiffStyleA = "<font color='red'>";
												$ScoreDiffStyleB = "</font>";											
											}
				  						} else {
				  							$ScoreDiffPercent = $SymbolEmpty;
				  						}
										
				  						
				  						//debug($StdPreMark[$StudentID]["StandardScore"]);
				  						$preStandardScore = '';
				  						if($CmpID ==''){
											if ($StdPreMark[$SubjectID]["Assessment"]["StandardScore"]!="") {
												$preStandardScore = $StdPreMark[$SubjectID]["Assessment"]["StandardScore"];
											}else{
												if($StdPreMark[$parentSubjectID]["Assessment"]["StandardScore"] != ''){
													$preStandardScore = $StdPreMark[$parentSubjectID]["Assessment"]["StandardScore"];
												}
											}
				  						} else {
				  							if ($StdPreMark[$CmpID]["Assessment"]["StandardScore"]!="") {
				  								$preStandardScore = $StdPreMark[$CmpID]["Assessment"]["StandardScore"];
				  							}else{
				  								if($StdPreMark[$parentSubjectID]["Assessment"]["StandardScore"] != ''){
				  									$preStandardScore = $StdPreMark[$parentSubjectID]["Assessment"]["StandardScore"];
				  								}
				  							}
				  						}
				  						if ($preStandardScore!="") {
				  							$StandardScoreDiffPercent = $StandardScore-$preStandardScore;
				  							
				  							if ($StandardScoreDiffPercent>0)
											{
												$StandardScoreDiffStyleA = "<font color='green'>";
												$StandardScoreDiffStyleB = "</font>";
											} elseif ($StandardScoreDiffPercent<0)
											{
												$StandardScoreDiffStyleA = "<font color='red'>";
												$StandardScoreDiffStyleB = "</font>";											
											}
				  						} else {
				  							$StandardScoreDiffPercent = $SymbolEmpty;
				  						}
				  						
				  						
				  						$preMeritForm = '';
				  						if($CmpID ==''){
											if ($StdPreMark[$SubjectID]["Assessment"]["MeritForm"]!="") {
												$preMeritForm = $StdPreMark[$SubjectID]["Assessment"]["MeritForm"];
											}else{
												if($StdPreMark[$parentSubjectID]["Assessment"]["MeritForm"] != ''){
													$preMeritForm = $StdPreMark[$parentSubjectID]["Assessment"]["MeritForm"];
												}
											}
				  						} else {
				  							if ($StdPreMark[$CmpID]["Assessment"]["MeritForm"]!="") {
				  								$preMeritForm = $StdPreMark[$CmpID]["Assessment"]["MeritForm"];
				  							}else{
				  								if($StdPreMark[$parentSubjectID]["Assessment"]["MeritForm"] != ''){
				  									$preMeritForm = $StdPreMark[$parentSubjectID]["Assessment"]["MeritForm"];
				  								}
				  							}
				  						}
				  						if ($preMeritForm!="") {
				  							$MeritFormDiff = ($MarkObj["OrderMeritForm"]-$preMeritForm);
				  							if($MeritFormDiff != 0){
				  								$MeritFormDiff *= -1;
				  							}
				  							
				  							if ($MeritFormDiff>0)
											{
												$MeritFormDiffStyleA = "<font color='green'>";
												$MeritFormDiffStyleB = "</font>";
											} elseif ($MeritFormDiff<0)
											{
												$MeritFormDiffStyleA = "<font color='red'>";
												$MeritFormDiffStyleB = "</font>";											
											}
				  						} else {
				  							$MeritFormDiff = $SymbolEmpty;
				  						}
				  						
										if(in_array('filterMark', $filterColumnArr)){
					  						$returnAssessmentRX .= "<td align='center'>".$ScoreNow."</td>";  // score
					  						$_column[] = $ScoreNow;
										}
										if(in_array('filterMarkDiff', $filterColumnArr)){
					  						$returnAssessmentRX .= "<td align='center'>".$ScoreDiffStyleA.$ScoreDiffPercent.($ScoreDiffPercent!=$SymbolEmpty ? "%" : "").$ScoreDiffStyleB."</td>";  // score difference
					  						$_column[] = $ScoreDiffPercent.($ScoreDiffPercent!=$SymbolEmpty ? "%" : "");
										}
										if(in_array('filterStandardScore', $filterColumnArr)){
					  						$returnAssessmentRX .= "<td align='center'>".$StandardScore."</td>";  // standard score
					  						$_column[] = $StandardScore;
										}
										if(in_array('filterStandardScoreDiff', $filterColumnArr)){
					  						$returnAssessmentRX .= "<td align='center'>".$StandardScoreDiffStyleA.$StandardScoreDiffPercent.$StandardScoreDiffStyleB."</td>";  // standard score difference
					  						$_column[] = $StandardScoreDiffPercent;
										}
										if(in_array('filterFormPosition', $filterColumnArr)){
					  						$returnAssessmentRX .= "<td align='center'>".$PositionShow."</td>";  //ranking in form
					  						$_column[] = str_replace('/', '#', $PositionShow);
										}
										if(in_array('filterFormPositionDiff', $filterColumnArr)){
					  						$returnAssessmentRX .= "<td align='center'>".$MeritFormDiffStyleA.$MeritFormDiff.$MeritFormDiffStyleB."</td>";  //ranking in form
					  						$_column[] = $MeritFormDiff;
										}
										if(in_array('filterFormPositionPercentile', $filterColumnArr)){
											$returnAssessmentRX .= "<td align='center'>".$PositionPercentile."</td>";  //ranking in form
											$_column[] = $PositionPercentile;
										}
				  						//"<td></td>";
										if($ScoreNow != $SymbolEmpty){
											if($CmpID==''){
												$StdPreMark[$SubjectID]["Assessment"]["Score"] = $ScoreNow;	
											} else {
												$StdPreMark[$CmpID]["Assessment"]["Score"] = $ScoreNow;
											}
										}
										if($StandardScore != $SymbolEmpty){
											if($CmpID==''){
												$StdPreMark[$SubjectID]["Assessment"]["StandardScore"] = $StandardScore;
											} else {
												$StdPreMark[$CmpID]["Assessment"]["StandardScore"] = $StandardScore;
											}
										}
										if($MarkObj["OrderMeritForm"] != $SymbolEmpty){
											if($CmpID==''){
												$StdPreMark[$SubjectID]["Assessment"]["MeritForm"] = $MarkObj["OrderMeritForm"];
											}else{
												$StdPreMark[$CmpID]["Assessment"]["MeritForm"] = $MarkObj["OrderMeritForm"];
											}
										}
				  					}
				  					
				  					if ($returnAssessmentRX=="")
									{
										$GradeShown = $MarkObj["Grade"];
										$GradeShown =  ($GradeShown!="") ? $GradeShown : $SymbolEmpty;
										if(in_array('filterMark', $filterColumnArr)){
											$returnRX .= "<td align='center'>$GradeShown</td>";
					  						$_column[] = $GradeShown;
											for($i=0,$iMax=$displayDataColumnCount-1;$i<$iMax;$i++){
												$returnRX .= "<td align='center'>$SymbolEmpty</td>";
												$_column[] = $SymbolEmpty;
											}
										}else{
											for($i=0,$iMax=$displayDataColumnCount;$i<$iMax;$i++){
												$returnRX .= "<td align='center'>$SymbolEmpty</td>";
												$_column[] = $SymbolEmpty;
											}
										}
										//$returnRX .= "<td align='center'>$GradeShown</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td>";
									}else{
										$returnRX .= $returnAssessmentRX;
									}
				  				}
								######## TermAssessment END ########
	
								######## Term START ########
				  				# 2020-05-05 (Philips) [2020-0225-1050-31235] - Hide Empty Term Result Column
				  				if(!$hasTermScore[$AcademicYearID][$YearTermID] && $hideEmptyTermResult){
				  					continue;
				  				}
				  				
		  						$returnAssessmentRX = "";
		  						$MarkObj = $StudentMarksArr[$AcademicYearID][$YearTermID][$SubjectID][$CmpID][""];
			  					if(empty($MarkObj)){
									for($i=0,$iMax=$displayDataColumnCount;$i<$iMax;$i++){
										$returnRX .= "<td align='center'>$SymbolEmpty</td>";
										$_column[] = $SymbolEmpty;
									}
									$chartDataObj['Score'][] = 'null';
									$chartFPDataObj['Percentile'][] = 'null';
									$returnRX .= $returnAssessmentRX;
									continue;
			  					}
								$ScoreNow = $MarkObj["Score"];
								$chartDataObj['Score'][] = $ScoreNow;
										
								$PositionShow = $MarkObj["OrderMeritForm"];
								$OrderMeritFormTotal = $MarkObj["OrderMeritFormTotal"];
								
								if ($OrderMeritFormTotal!="" && $PositionShow!="")
								{
									$PositionShow .= "/".$OrderMeritFormTotal;
									# 2020-04-29 (Philips) [2019-0628-0954-56098] Form Position Percentile
									$PositionPercentile = 0;
									if(!($PositionShow<='0' || $OrderMeritFormTotal<='0')){
										$PositionPercentile = ( (int)$OrderMeritFormTotal - (int)$PositionShow + 1) / (int)$OrderMeritFormTotal  * 100;
									}
									$chartFPDataObj['Percentile'][] = $PositionPercentile == 0 ? 'null' : number_format($PositionPercentile, 2);
									$PositionPercentile = number_format($PositionPercentile, 2) . '%';
								}else{
									$PositionShow = $SymbolEmpty;
									
									$PositionPercentile = $SymbolEmpty;
									$chartFPDataObj['Percentile'][] = 'null';
								}
	// 							$formPercentileChartArr[] = $chartFPDataObj;
				
								//debug($ScoreNow, $TermName, $i_marks);
	// 							$SubjectComponentID = 0; // not support subject component at this moment
	
	  							$Mean = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][0][0][0][$ClassMapYear[$MarkObj["YearClassID"]]]["MEAN"], 2);
	  							$SD = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][0][0][0][$ClassMapYear[$MarkObj["YearClassID"]]]["SD"], 2);
	
	  							if ($SD>0) {
	  								$StandardScore = round(($ScoreNow-$Mean)/$SD, 2);
	  							} else {
	  								$StandardScore = $SymbolEmpty;
	  							}
	  							if ($ScoreNow!="" && $ScoreNow>=0){
	// 	  							$SubjectComponentID = 0; // not support subject component at this moment
	
		  							$Mean = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][0][0][0][$ClassMapYear[$MarkObj["YearClassID"]]]["MEAN"], 2);
		  							$SD = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][$YearTermID][0][0][0][$ClassMapYear[$MarkObj["YearClassID"]]]["SD"], 2);
		  							
		  							if ($SD>0) {
		  								$StandardScore = round(($ScoreNow-$Mean)/$SD, 2);
		  							} else {
		  								$StandardScore = $SymbolEmpty;
		  							}
		  							//debug($ScoreNow, $Mean, $SD, $StandardScore);
									$ScoreDiffStyleA = "";
									$ScoreDiffStyleB = "";
									$StandardScoreDiffStyleA = "";
									$StandardScoreDiffStyleB = "";
									$MeritFormDiffStyleA = "";
									$MeritFormDiffStyleB = "";
									
			  						$preMark = '';
			  						if($CmpID==''){
										if ($StdPreMark[$SubjectID]["Term"]["Score"]!="") {
											$preMark = $StdPreMark[$SubjectID]["Term"]["Score"];
										}else{
											if($StdPreMark[$parentSubjectID]["Term"]["Score"] != ''){
												$preMark = $StdPreMark[$parentSubjectID]["Term"]["Sc"];
											}
										}
			  						} else {
			  							if ($StdPreMark[$CmpID]["Term"]["Score"]!="") {
			  								$preMark = $StdPreMark[$CmpID]["Term"]["Score"];
			  							}else{
			  								if($StdPreMark[$parentSubjectID]["Term"]["Score"] != ''){
			  									$preMark = $StdPreMark[$parentSubjectID]["Term"]["Sc"];
			  								}
			  							}
			  						}
			  						if ($preMark!="" && $preMark!="0") {
			  							$ScoreDiffPercent = ROUND(100*($ScoreNow-$preMark)/$preMark, 2);
										if ($ScoreDiffPercent>0) {
											$ScoreDiffStyleA = "<font color='green'>";
											$ScoreDiffStyleB = "</font>";
										} elseif ($ScoreDiffPercent<0) {
											$ScoreDiffStyleA = "<font color='red'>";
											$ScoreDiffStyleB = "</font>";											
										}
			  						} else {
			  							$ScoreDiffPercent = $SymbolEmpty;
			  						}
			  						
			  						//debug($StdPreMark[$StudentID]["StandardScore"]);
			  						$preStandardScore = '';
			  						if($CmpID==''){
										if ($StdPreMark[$SubjectID]["Term"]["StandardScore"]!="") {
											$preStandardScore = $StdPreMark[$SubjectID]["Term"]["StandardScore"];
										}else{
											if($StdPreMark[$parentSubjectID]["Term"]["StandardScore"] != ''){
												$preStandardScore = $StdPreMark[$parentSubjectID]["Term"]["StandardScore"];
											}
										}
			  						} else {
			  							if ($StdPreMark[$CmpID]["Term"]["StandardScore"]!="") {
			  								$preStandardScore = $StdPreMark[$CmpID]["Term"]["StandardScore"];
			  							}else{
			  								if($StdPreMark[$parentSubjectID]["Term"]["StandardScore"] != ''){
			  									$preStandardScore = $StdPreMark[$parentSubjectID]["Term"]["StandardScore"];
			  								}
			  							}
			  						}
			  						if ($preStandardScore!="") {
			  							$StandardScoreDiffPercent = $StandardScore-$preStandardScore;
			  							
			  							if ($StandardScoreDiffPercent>0)
										{
											$StandardScoreDiffStyleA = "<font color='green'>";
											$StandardScoreDiffStyleB = "</font>";
										} elseif ($StandardScoreDiffPercent<0)
										{
											$StandardScoreDiffStyleA = "<font color='red'>";
											$StandardScoreDiffStyleB = "</font>";											
										}
			  						} else {
			  							$StandardScoreDiffPercent = $SymbolEmpty;
			  						}
			  						
			  						$preMeritForm = '';
			  						if($CmpID==''){
										if ($StdPreMark[$SubjectID]["Term"]["MeritForm"]!="") {
											$preMeritForm = $StdPreMark[$SubjectID]["Term"]["MeritForm"];
										}else{
											if($StdPreMark[$parentSubjectID]["Term"]["MeritForm"] != ''){
												$preMeritForm = $StdPreMark[$parentSubjectID]["Term"]["MeritForm"];
											}
										}
			  						} else {
			  							if ($StdPreMark[$CmpID]["Term"]["MeritForm"]!="") {
			  								$preMeritForm = $StdPreMark[$CmpID]["Term"]["MeritForm"];
			  							}else{
			  								if($StdPreMark[$parentSubjectID]["Term"]["MeritForm"] != ''){
			  									$preMeritForm = $StdPreMark[$parentSubjectID]["Term"]["MeritForm"];
			  								}
			  							}
			  						}
			  						if ($preMeritForm!="") {
			  							$MeritFormDiff = ($MarkObj["OrderMeritForm"]-$preMeritForm);
			  							if($MeritFormDiff != 0){
			  								$MeritFormDiff *= -1;
			  							}
			  							
			  							if ($MeritFormDiff>0)
										{
											$MeritFormDiffStyleA = "<font color='green'>";
											$MeritFormDiffStyleB = "</font>";
										} elseif ($MeritFormDiff<0)
										{
											$MeritFormDiffStyleA = "<font color='red'>";
											$MeritFormDiffStyleB = "</font>";											
										}
			  						} else {
			  							$MeritFormDiff = $SymbolEmpty;
			  						}
									if(in_array('filterMark', $filterColumnArr)){
				  						$returnAssessmentRX .= "<td align='center'>".$ScoreNow."</td>";  // score
				  						$_column[] = $ScoreNow;
									}
									if(in_array('filterMarkDiff', $filterColumnArr)){
				  						$returnAssessmentRX .= "<td align='center'>".$ScoreDiffStyleA.$ScoreDiffPercent.($ScoreDiffPercent!=$SymbolEmpty ? "%" : "").$ScoreDiffStyleB."</td>";  // score difference
				  						$_column[] = $ScoreDiffPercent.($ScoreDiffPercent!=$SymbolEmpty ? "%" : "");
									}
									if(in_array('filterStandardScore', $filterColumnArr)){
				  						$returnAssessmentRX .= "<td align='center'>".$StandardScore."</td>";  // standard score
				  						$_column[] = $StandardScore;
									}
									if(in_array('filterStandardScoreDiff', $filterColumnArr)){
				  						$returnAssessmentRX .= "<td align='center'>".$StandardScoreDiffStyleA.$StandardScoreDiffPercent.$StandardScoreDiffStyleB."</td>";  // standard score difference
				  						$_column[] = $StandardScoreDiffPercent;
									}
									if(in_array('filterFormPosition', $filterColumnArr)){
				  						$returnAssessmentRX .= "<td align='center'>".$PositionShow."</td>";  //ranking in form
				  						$_column[] = str_replace('/', '#', $PositionShow);
									}
									if(in_array('filterFormPositionDiff', $filterColumnArr)){
				  						$returnAssessmentRX .= "<td align='center'>".$MeritFormDiffStyleA.$MeritFormDiff.$MeritFormDiffStyleB."</td>";  //ranking in form
				  						$_column[] = $MeritFormDiff;
									}
									if(in_array('filterFormPositionPercentile', $filterColumnArr)){
										$returnAssessmentRX .= "<td align='center'>".$PositionPercentile."</td>";  //ranking in form
										$_column[] = $PositionPercentile;
									}
									//"<td></td>";
									if($ScoreNow != $SymbolEmpty){
										if($CmpID==''){
											$StdPreMark[$SubjectID]["Term"]["Score"] = $ScoreNow;		
										} else {
											$StdPreMark[$CmpID]["Term"]["Score"] = $ScoreNow;
										}
									}
									if($StandardScore != $SymbolEmpty){
										if($CmpID==''){
											$StdPreMark[$SubjectID]["Term"]["StandardScore"] = $StandardScore;
										} else {
											$StdPreMark[$CmpID]["Term"]["StandardScore"] = $StandardScore;
										}
									}
									if($MarkObj["OrderMeritForm"] != $SymbolEmpty){
										if($CmpID==''){
											$StdPreMark[$SubjectID]["Term"]["MeritForm"] = $MarkObj["OrderMeritForm"];
										} else {
											$StdPreMark[$CmpID]["Term"]["MeritForm"] = $MarkObj["OrderMeritForm"];
										}
									}
			  					}
			  					
			  					if ($returnAssessmentRX=="")
								{
									$GradeShown = $MarkObj["Grade"];
									$GradeShown =  ($GradeShown!="") ? $GradeShown : $SymbolEmpty;
									if(in_array('filterMark', $filterColumnArr)){
										$returnRX .= "<td align='center'>$GradeShown</td>";
										$_column[] = $GradeShown;
										for($i=0,$iMax=$displayDataColumnCount-1;$i<$iMax;$i++){
											$returnRX .= "<td align='center'>$SymbolEmpty</td>";
											$_column[] = $SymbolEmpty;
										}
									}else{
										for($i=0,$iMax=$displayDataColumnCount;$i<$iMax;$i++){
											$returnRX .= "<td align='center'>$SymbolEmpty</td>";
											$_column[] = $SymbolEmpty;
										}
									}
									//$returnRX .= "<td align='center'>$GradeShown</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td>";
								}else{
									$returnRX .= $returnAssessmentRX;
								}
								######## Term END ########
				  			}
							######## TermAssessment and Term END ########
							
								
							######## Total START ########
	  						$returnAssessmentRX = "";
	  						$MarkObj = $StudentMarksArr[$AcademicYearID][""][$SubjectID][$CmpID][""];
		  					if(!$hasScore[$AcademicYearID][0]){
		  						$chartArr['Data'][] = $chartDataObj; // No Header
		  						$formPercentileChartArr[$chartFPDataObj['Subject']][] = $chartFPDataObj;
			  					continue;
			  				}
	
		  					if(empty($MarkObj)){
								for($i=0,$iMax=$displayDataColumnCount;$i<$iMax;$i++){
									$returnRX .= "<td align='center'>$SymbolEmpty</td>";
									$_column[] = $SymbolEmpty;
								}
								$chartDataObj['Score'][] = 'null';
								$chartFPDataObj['Percentile'][] = 'null';
								$chartArr['Data'][] = $chartDataObj; // No Score
								$formPercentileChartArr[$chartFPDataObj['Subject']][] = $chartFPDataObj;
								$returnRX .= $returnAssessmentRX;
								continue;
		  					}
							$ScoreNow = $MarkObj["Score"];
							$chartDataObj['Score'][] = $ScoreNow;
							$chartArr['Data'][] = $chartDataObj;
							
							$PositionShow = $MarkObj["OrderMeritForm"];
							$OrderMeritFormTotal = $MarkObj["OrderMeritFormTotal"];
							
	  						if ($OrderMeritFormTotal!="" && $PositionShow!="")
	  						{
	  							$PositionShow .= "/".$OrderMeritFormTotal;
	  							# 2020-04-29 (Philips) [2019-0628-0954-56098] Form Position Percentile
	  							$PositionPercentile = 0;
	  							if(!($PositionShow<='0' || $OrderMeritFormTotal<='0')){
	  								$PositionPercentile = ( (int)$OrderMeritFormTotal - (int)$PositionShow + 1) / (int)$OrderMeritFormTotal  * 100;
	  							}
	  							$chartFPDataObj['Percentile'][] = $PositionPercentile == 0 ? 'null' : number_format($PositionPercentile, 2);
	  							$PositionPercentile = number_format($PositionPercentile, 2) . '%';
	  						}else{
	  							$PositionShow = $SymbolEmpty;
	  							
	  							$PositionPercentile = $SymbolEmpty;
	  							$chartFPDataObj['Percentile'][] = 'null';
	  						}
	  						$formPercentileChartArr[$chartFPDataObj['Subject']][] = $chartFPDataObj;
			
							//debug($ScoreNow, $TermName, $i_marks);
	// 						$SubjectComponentID = 0; // not support subject component at this moment
	
							$Mean = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][0][0][1][0][$ClassMapYear[$MarkObj["YearClassID"]]]["MEAN"], 2);
							$SD = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][0][0][1][0][$ClassMapYear[$MarkObj["YearClassID"]]]["SD"], 2);
							
							if ($SD>0) {
								$StandardScore = round(($ScoreNow-$Mean)/$SD, 2);
							} else {
								$StandardScore = $SymbolEmpty;
							}
							if ($ScoreNow!="" && $ScoreNow>=0){
	//   							$SubjectComponentID = 0; // not support subject component at this moment
	
	  							$Mean = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][0][0][1][0][$ClassMapYear[$MarkObj["YearClassID"]]]["MEAN"], 2);
	  							$SD = round($SDrows[$SubjectID][$SubjectComponentID][$AcademicYearID][0][0][1][0][$ClassMapYear[$MarkObj["YearClassID"]]]["SD"], 2);
	  							
	  							if ($SD>0) {
	  								$StandardScore = round(($ScoreNow-$Mean)/$SD, 2);
	  							} else {
	  								$StandardScore = $SymbolEmpty;
	  							}
	  							//debug($ScoreNow, $Mean, $SD, $StandardScore);
								$ScoreDiffStyleA = "";
								$ScoreDiffStyleB = "";
								$StandardScoreDiffStyleA = "";
								$StandardScoreDiffStyleB = "";
								$MeritFormDiffStyleA = "";
								$MeritFormDiffStyleB = "";
								
		  						$preMark = '';
		  						if($CmpID==''){
									if ($StdPreMark[$SubjectID]["Total"]["Score"]!="") {
										$preMark = $StdPreMark[$SubjectID]["Total"]["Score"];
									}else{
										if($StdPreMark[$parentSubjectID]["Total"]["Score"] != ''){
											$preMark = $StdPreMark[$parentSubjectID]["Total"]["Score"];
										}
									}
		  						} else {
		  							if ($StdPreMark[$CmpID]["Total"]["Score"]!="") {
		  								$preMark = $StdPreMark[$CmpID]["Total"]["Score"];
		  							}else{
		  								if($StdPreMark[$parentSubjectID]["Total"]["Score"] != ''){
		  									$preMark = $StdPreMark[$parentSubjectID]["Total"]["Score"];
		  								}
		  							}
		  						}
		  						if ($preMark!="" && $preMark!="0") {
		  							$ScoreDiffPercent = ROUND(100*($ScoreNow-$preMark)/$preMark, 2);
									if ($ScoreDiffPercent>0) {
										$ScoreDiffStyleA = "<font color='green'>";
										$ScoreDiffStyleB = "</font>";
									} elseif ($ScoreDiffPercent<0) {
										$ScoreDiffStyleA = "<font color='red'>";
										$ScoreDiffStyleB = "</font>";											
									}
		  						} else {
		  							$ScoreDiffPercent = $SymbolEmpty;
		  						}
		  						
		  						//debug($StdPreMark[$StudentID]["StandardScore"]);
		  						$preStandardScore = '';
		  						if($CmpID==''){
									if ($StdPreMark[$SubjectID]["Total"]["StandardScore"]!="") {
										$preStandardScore = $StdPreMark[$SubjectID]["Total"]["StandardScore"];
									}else{
										if($StdPreMark[$parentSubjectID]["Total"]["StandardScore"] != ''){
											$preStandardScore = $StdPreMark[$parentSubjectID]["Total"]["StandardScore"];
										}
									}
		  						} else {
		  							if ($StdPreMark[$CmpID]["Total"]["StandardScore"]!="") {
		  								$preStandardScore = $StdPreMark[$CmpID]["Total"]["StandardScore"];
		  							}else{
		  								if($StdPreMark[$parentSubjectID]["Total"]["StandardScore"] != ''){
		  									$preStandardScore = $StdPreMark[$parentSubjectID]["Total"]["StandardScore"];
		  								}
		  							}
		  						}
		  						if ($preStandardScore!="") {
		  							$StandardScoreDiffPercent = $StandardScore-$preStandardScore;
		  							
		  							if ($StandardScoreDiffPercent>0)
									{
										$StandardScoreDiffStyleA = "<font color='green'>";
										$StandardScoreDiffStyleB = "</font>";
									} elseif ($StandardScoreDiffPercent<0)
									{
										$StandardScoreDiffStyleA = "<font color='red'>";
										$StandardScoreDiffStyleB = "</font>";											
									}
		  						} else {
		  							$StandardScoreDiffPercent = $SymbolEmpty;
		  						}
		  						
		  						$preMeritForm = '';
		  						if($CmpID==''){
									if ($StdPreMark[$SubjectID]["Total"]["MeritForm"]!="") {
										$preMeritForm = $StdPreMark[$SubjectID]["Total"]["MeritForm"];
									}else{
										if($StdPreMark[$parentSubjectID]["Total"]["MeritForm"] != ''){
											$preMeritForm = $StdPreMark[$parentSubjectID]["Total"]["MeritForm"];
										}
									}
		  						} else {
		  							if ($StdPreMark[$CmpID]["Total"]["MeritForm"]!="") {
		  								$preMeritForm = $StdPreMark[$CmpID]["Total"]["MeritForm"];
		  							}else{
		  								if($StdPreMark[$parentSubjectID]["Total"]["MeritForm"] != ''){
		  									$preMeritForm = $StdPreMark[$parentSubjectID]["Total"]["MeritForm"];
		  								}
		  							}
		  						}
		  						if ($preMeritForm!="") {
		  							$MeritFormPercent = ($MarkObj["OrderMeritForm"]-$preMeritForm);
		  							if($MeritFormPercent != 0){
		  								$MeritFormPercent *= -1;
		  							}
		  							
		  							if ($MeritFormPercent>0)
									{
										$StandardScoreDiffStyleA = "<font color='green'>";
										$StandardScoreDiffStyleB = "</font>";
									} elseif ($MeritFormPercent<0)
									{
										$StandardScoreDiffStyleA = "<font color='red'>";
										$StandardScoreDiffStyleB = "</font>";											
									}elseif($MeritFormPercent==0){
										$StandardScoreDiffStyleA = "<font >";
										$StandardScoreDiffStyleB = "</font>";
									}
		  						} else {
		  							$MeritFormPercent = $SymbolEmpty;
		  						}
								if(in_array('filterMark', $filterColumnArr)){
			  						$returnAssessmentRX .= "<td align='center'>".$ScoreNow."</td>";  // score
									$_column[] = $ScoreNow;
								}
								if(in_array('filterMarkDiff', $filterColumnArr)){
			  						$returnAssessmentRX .= "<td align='center'>".$ScoreDiffStyleA.$ScoreDiffPercent.($ScoreDiffPercent!=$SymbolEmpty ? "%" : "").$ScoreDiffStyleB."</td>";  // score difference
									$_column[] = $ScoreDiffPercent.($ScoreDiffPercent!=$SymbolEmpty ? "%" : "");
								}
								if(in_array('filterStandardScore', $filterColumnArr)){
			  						$returnAssessmentRX .= "<td align='center'>".$StandardScore."</td>";  // standard score
									$_column[] = $StandardScore;
								}
								if(in_array('filterStandardScoreDiff', $filterColumnArr)){
			  						$returnAssessmentRX .= "<td align='center'>".$StandardScoreDiffStyleA.$StandardScoreDiffPercent.$StandardScoreDiffStyleB."</td>";  // standard score difference
									$_column[] = $StandardScoreDiffPercent;
								}
								if(in_array('filterFormPosition', $filterColumnArr)){
			  						$returnAssessmentRX .= "<td align='center'>".$PositionShow."</td>";  //ranking in form
									$_column[] = str_replace('/', '#', $PositionShow);
								}
								if(in_array('filterFormPositionDiff', $filterColumnArr)){
			  						$returnAssessmentRX .= "<td align='center'>".$StandardScoreDiffStyleA.$MeritFormPercent.$StandardScoreDiffStyleB."</td>";  //ranking in form
									$_column[] = $MeritFormPercent;
								}
								if(in_array('filterFormPositionPercentile', $filterColumnArr)){
									$returnAssessmentRX .= "<td align='center'>".$PositionPercentile."</td>";  //ranking in form
									$_column[] = $PositionPercentile;
								}
								//"<td></td>";
								if($ScoreNow != $SymbolEmpty){
									if($CmpID!=''){
										$StdPreMark[$SubjectID]["Total"]["Score"] = $ScoreNow;
									} else {
										$StdPreMark[$CmpID]["Total"]["Score"] = $ScoreNow;
									}
								}
								if($StandardScore != $SymbolEmpty){
									if($CmpID!=''){
										$StdPreMark[$SubjectID]["Total"]["StandardScore"] = $StandardScore;
									} else{
										$StdPreMark[$CmpID]["Total"]["StandardScore"] = $StandardScore;
									}
								}
								if($MarkObj["OrderMeritForm"] != $SymbolEmpty){
									if($CmpID!=''){
										$StdPreMark[$SubjectID]["Total"]["MeritForm"] = $MarkObj["OrderMeritForm"];
									} else {
										$StdPreMark[$CmpID]["Total"]["MeritForm"] = $MarkObj["OrderMeritForm"];
									}
								}
		  					}
		  					
		  					if ($returnAssessmentRX=="")
							{
								$GradeShown = $MarkObj["Grade"];
								$GradeShown =  ($GradeShown!="") ? $GradeShown : $SymbolEmpty;
								if(in_array('filterMark', $filterColumnArr)){
									$returnRX .= "<td align='center'>$GradeShown</td>";
									$_column[] = $GradeShown;
									for($i=0,$iMax=$displayDataColumnCount-1;$i<$iMax;$i++){
										$returnRX .= "<td align='center'>$SymbolEmpty</td>";
										$_column[] = $SymbolEmpty;
									}
								}else{
									for($i=0,$iMax=$displayDataColumnCount;$i<$iMax;$i++){
										$returnRX .= "<td align='center'>$SymbolEmpty</td>";
										$_column[] = $SymbolEmpty;
									}
								}
								//$returnRX .= "<td align='center'>$GradeShown</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td><td align='center'>$SymbolEmpty</td>";
							}else{
								$returnRX .= $returnAssessmentRX;
							}
							######## Total END ########
								
				  			$returnRX .= "</tr>";
				  		}
			  		}
			  	}
		  		if(!empty($_column)){
		  			$exportRow[] = $_column;
		  		}
			  	
			  	$returnRX .= "</tbody>";
			  	$returnRX .= "</table></div>";
			  	
			  	$returnRX .= "<br />
			  	<span class=\"tabletextremark\">
			  	{$ec_iPortfolio['OMFHighlightRemarks'] }
			  	</span>";
			  	
			  	$returnRX .= "<br />";
			  	
			  	### Highchart Start
			  	if(in_array('filterFormSplineDiagram', $filterColumnArr)){
			  		$chartID = "HighChart_".$AcademicYearID;
			  		$returnRX .= "<div id='$chartID' style='height:500px'></div>";
			  		$chartTerms = "'" . implode("','", $chartArr['Terms']) . "'";
			  		$scoreTitle = $Lang['iPortfolio']['Score'];
			  		$termLegend = $Lang['General']['Term'];
			  		$chartSetting = '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>';
			  		$seriesString = "";
			  		foreach($chartArr['Data'] as $SubjectScoreObj){
			  			foreach ($SubjectScoreObj['Score'] as &$score){
			  				if($score== '-1') $score = 'null'; // 2020-04-29 (Philips) fix connect null
			  			}
			  			$seriesString .= "{ name : '" . $SubjectScoreObj['Subject'] . "', ";
			  			$seriesString .= " data : [" . implode(",", $SubjectScoreObj['Score']) . "]}, ";
			  		}
			  		// Highchart script
			  		$chartScript = <<< EOD
						<script type='text/javascript'>
						xAxis = [$chartTerms];
						series = [$seriesString];
						$('#$chartID').highcharts({
							chart: {
						        type: '$chartType'
						    },
						    title: {
						        text: '$AcademicYearName'
						    },
						    xAxis: {
						     categories: xAxis,
						     crosshair: true,
						     title: {
						         text: '$termLegend'
						     }
						},
						    yAxis: {
						        min: 0,
						        max: 100,
						        title: {
						        	text: '$scoreTitle'
						        }
						    },
						    tooltip: {
						        pointFormat: '$chartSetting',
						        shared: true
						    },
						    plotOptions: {
						        column: {
						            stacking: 'normal'
						        },
						        series: {
						            connectNulls: true
						        }
						    },
						    series: series,
						    credits : {enabled: false,},
						});
						</script>
EOD;
			  		$returnRX .= $chartScript;
			  	}
			  	
// 			  	$yearTermArr = array_merge ($yearTermArr, $chartArr['Terms']);
			  	foreach($chartArr['Terms'] as $termName){
			  		$yearTermArr[] = $AcademicYearName . ' ' . $termName;
			  	}
			  	### Highchart End
			  	
			  	/*$returnRX .= "<br />
			  	<span class=\"tabletextremark\">
			  	{$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] }
			  	</br>
			  	{$ec_iPortfolio['OMFHighlightRemarks'] }
			  	</span>";*/
			  	$resultArr[] = $returnRX;
				$exportRow[] = '';
				$exportYearArr[] = $exportRow;
		  	}
		  	if(empty($resultArr)){
		  		$resultArr[] = "	<div class='chart_tables_2'>
										<table class='common_table_list_v30 view_table_list_v30'>
											<thead>
												<tr>
													<td align='center'>
														".$Lang['SDAS']['NoRecord']['A']."
													</td>
												</tr>
											<thead>
										</table>
									</div>";
		  		$exportYearArr[] = array($Lang['SDAS']['NoRecord']['A']);
		  		$returnRX = implode("", $resultArr);
		  		return $returnRX;
		  	} else {
		  	
			  	# 2020-05-04 (Philips) 
			  	if(in_array('filterFormPercentileSplineDiagram', $filterColumnArr)){
			  		$chartID = "HighChart_FormPercentile";
			  		$chartHtml = "<div id='$chartID' style='height:500px'></div>";
			  		$chartTerms = "'" . implode("','", $yearTermArr) . "'";
			  		$scoreTitle = $Lang['iPortfolio']['Percentile']['PercentileRank'].'(%)';
			  		$termLegend = $Lang['General']['Term'];
			  		$chartSetting = '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}%</b><br/>';
			  		$seriesString = "";
			  		foreach($formPercentileChartArr as $subj => $SubjectScoreObj){
			  			$seriesString .= "{ name : '" . $subj . "', ";
			  			$percentileAry = array();
			  			foreach($SubjectScoreObj as $objAry) $percentileAry= array_merge($percentileAry, $objAry['Percentile']);
			  			$seriesString .= " data : [" . implode(",", $percentileAry) . "]}, ";
			  		}
			  		// Highchart script
			  		$chartScript = <<< EOD
							<script type='text/javascript'>
							xAxis = [$chartTerms];
							series = [$seriesString];
							$('#$chartID').highcharts({
								chart: {
							        type: '$chartType'
							    },
							    title: {
							    text: '{$Lang['SDAS']['StudentPerformanceTracking']['FormPercentile']} {$Lang['SDAS']['SplineDiagram']}'
							    },
							    xAxis: {
							     categories: xAxis,
							     crosshair: true,
							     title: {
							         text: '$termLegend'
							     }
							},
							    yAxis: {
							        min: 0,
							        max: 100,
							        title: {
							        	text: '$scoreTitle'
							        }
							    },
							    tooltip: {
							        pointFormat: '$chartSetting',
							        shared: true
							    },
							    plotOptions: {
							        column: {
							            stacking: 'normal'
							        },
							        series: {
							            connectNulls: true
							        }
							    },
							    series: series,
							    credits : {enabled: false,},
							});
							</script>
EOD;
			  		$resultArr[] = $chartHtml.$chartScript;
			  	}
		  	
		  	$resultArr = array_reverse($resultArr);
		  	$exportYearArr = array_reverse($exportYearArr);
		  	$returnRX = implode("", $resultArr);
		  	
		  	foreach($exportYearArr as $data){
		  		foreach ($data as $d){
		  			$exportArr[] = $d;
		  		}
		  	}
		  	
		  	
		  	$floatHeader = <<<HTML
		  		<link href="/templates/{$LAYOUT_SKIN}/css/content_30.css" rel="stylesheet" type="text/css">
  				<script>\$(function() {\$('.resultTable').floatHeader();});</script>
HTML;
		  	
// 		  	$exportButton = <<<HTML
// 			  	<div class="Conntent_tool">
// 					<a href="javascript: exportCSV()" class="export tablelink" style="float:none;">{$Lang['Btn']['Export']}</a>
			  		
// 		  		</div>
// HTML;
		  	
		  	if($isExport){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
		  		$exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportArr, $exportColumn=array(), "\t", "\r\n", "\t", 0, "11",1);
		  		$lexport->EXPORT_FILE($fileName.'.csv', $exportContent);
		  		return '';
		  	}
		  	
		  	/*$returnRX .= "
				<br/>
				<span class=\"tabletextremark\">
			  	{$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] }
			  	</span>";*/
		  	
		  	return $floatHeader . $exportButton . $returnRX;
		  	}
	  }

		function GetStudentCrossYearPercentileResults($StudentID){
			global $eclass_db, $intranet_db, $Lang, $intranet_session_language, $ec_iPortfolio, $iPort, $PATH_WRT_ROOT, $intranet_root, $LAYOUT_SKIN;
			 
			$SymbolEmpty = $Lang['General']['EmptySymbol'] ;

			$fcm = new form_class_manage();
			
			######## Get Data START ########
			$accessRight = $this->getAssessmentStatReportAccessRight();
			if(!$accessRight['admin'] && !$accessRight['subjectPanel']){
			    $subjectIDAry = array();
			    include_once('subject_class_mapping.php');
			    $sbj = new subject();
			    foreach($accessRight['classTeacher'] as $class){
			        $cYearID = $class['YearID'];
			        $subjects = $sbj->Get_Subject_By_Form($cYearID);
			        $sIDAry = Get_Array_By_key($subjects, 'RecordID');
			        $subjectIDAry = array_merge($subjectIDAry, $sIDAry);
			    }
			    foreach($accessRight['subjectGroup'] as $subjectGroup){
			        $sGroup = new subject_term_class($subjectGroup, $getTeacherList = false, $getStudentList = true);
			        $subjectIDAry[] = $sGroup->SubjectID;
			    }
			    if(sizeof($subjectIDAry) > 0){
			        $sql_cond = " AND ASS.RecordID In ('" . implode("','", $subjectIDAry) . "') ";
			    }
			}
			#### Get Study Subject START ####
			$subjectNameSql = Get_Lang_Selection('ASS.CH_DES', 'ASS.EN_DES');
			$sql = "SELECT DISTINCT
				ASS.RecordID AS SubjectID,
				{$subjectNameSql} AS SubjectName
			FROM
				{$intranet_db}.SUBJECT_TERM_CLASS_USER STCU
			INNER JOIN
				{$intranet_db}.SUBJECT_TERM_CLASS STC
			ON
				STCU.SubjectGroupID = STC.SubjectGroupID
			INNER JOIN
				{$intranet_db}.SUBJECT_TERM ST
			ON
				STC.SubjectGroupID = ST.SubjectGroupID
			INNER JOIN
				{$intranet_db}.ASSESSMENT_SUBJECT ASS
			ON
				ST.SubjectID = ASS.RecordID
			AND
				(ASS.CMP_CODEID = '' OR ASS.CMP_CODEID IS NULL)
			WHERE
				STCU.UserID = '{$StudentID}'
                $sql_cond
			ORDER BY
				ASS.DisplayOrder
			";
			$allSubjectArr = $this->returnResultSet($sql);
			#### Get Study Subject END ####
			
			#### Get Assessment Record START ####
			$sql = "SELECT 
				* 
			FROM 
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
			WHERE
				UserID = '{$StudentID}'
			AND
				TermAssessment IS NULL
			AND
				SubjectComponentID IS NULL
			ORDER BY 
				Year";
			$rs = $this->returnResultSet($sql);
			$scoreArr = BuildMultiKeyAssoc($rs, array('SubjectID', 'AcademicYearID', 'YearTermID'));
			$academicYearIdArr = Get_Array_By_Key($rs, 'AcademicYearID');
			$yearTermIdArr = Get_Array_By_Key($rs, 'YearTermID');
			$yearDetailsArr = BuildMultiKeyAssoc($rs, array('AcademicYearID'), array('YearClassID', 'ClassName', 'ClassNumber', 'Year'));
			#### Get Assessment Record END ####
			
			#### Get term START ####
			$termArr = array();
			foreach($academicYearIdArr as $ayID){
				$termArr[$ayID] = $fcm->Get_Academic_Year_Term_List($ayID, $NoPastTerm=0, $PastTermOnly=0, $YearTermID_Compare='', $IncludeYearTermIDArr=$yearTermIdArr, $ExcludeYearTermIDArr='');
// 				$termArr[$ayID][] =array('YearTermID'=>'0','YearTermNameEN'=>$ec_iPortfolio['overall_result'],'YearTermNameB5'=>$ec_iPortfolio['overall_result'] ) ;
				$termArr[$ayID][] =array('YearTermID'=>'','YearTermNameEN'=>$ec_iPortfolio['overall_result'],'YearTermNameB5'=>$ec_iPortfolio['overall_result'] ) ;
				
			}
			#### Get term END ####
			
			#### Get Percentile START ####
			$rankArr = array();
			foreach ($allSubjectArr as $subject){
				$lastRank = null;
				foreach ($termArr as $academicYearId => $term){
					$subjectID = $subject['SubjectID'];
					$studentRank = $this->getStudentPercentile($academicYearId, $subjectID,'',$StudentID);
					
					foreach($term as $t){
						$yearTermID = $t['YearTermID'];
						$rank = $studentRank[$academicYearId][$yearTermID][''/*TermAssessment*/][$StudentID];
						
						if($lastRank !== null){
							$rank['rankDiff'] = $lastRank - $rank['MeritRank'];
						}else{
							$rank['rankDiff'] = 0;
						}
						$lastRank = $rank['MeritRank'];
						
						$rankArr[$subjectID][$academicYearId][$yearTermID] = $rank;
					}
				}
			}
			#### Get Percentile END ####
			
			######## Get Data END ########
			
			######## Gen Table START ########
			$returnRX = '';
			
			#### Table Header START ####
			$returnRX .= '<table class="common_table_list_v30 view_table_list_v30 resultTable">';
			$returnRX .= '<thead>';
			
			$returnRX .= '<tr>';
			
			$returnRX .= '<th rowspan="2">&nbsp;</th>';
			foreach($termArr as $ayID => $term){
				$className = $yearDetailsArr[$ayID]['ClassName'];
				$yearName = $yearDetailsArr[$ayID]['Year'];
				$termCount = count($term);
				$returnRX .= "<th colspan=\"{$termCount}\">{$className} ({$yearName})</th>";
			}
			$returnRX .= '</tr>';

			$returnRX .= '<tr>';
			foreach($termArr as $ayID => $term){
				foreach ($term as $t){
					$termName = Get_Lang_Selection($t['YearTermNameB5'], $t['YearTermNameEN']);
					$returnRX .= "<th>{$termName}</th>";
				}
			}
			$returnRX .= '</tr>';
			
			$returnRX .= '</thead>';
			#### Table Header END ####
			
			#### Data START ####
			$rankNameHtmlArr = array('1<sup>st</sup>', '2<sup>nd</sup>', '3<sup>rd</sup>', '4<sup>th</sup>', '5<sup>th</sup>');
			$rankNameArr = array('1st', '2nd', '3rd', '4th', '5th');
			foreach ($allSubjectArr as $subject){
				$subjectID = $subject['SubjectID'];
				$returnRX .= '<tr>';

				$returnRX .= "<td>{$subject['SubjectName']}</td>";
				foreach($termArr as $ayID => $term){
					foreach ($term as $t){
						$yearTermID = $t['YearTermID'];
						$score = $scoreArr[$subjectID][$ayID][$yearTermID]['Score'];
						
						if(is_null($score)){
							$returnRX .= "<td style=\"\">{$SymbolEmpty}</td>";
						}else if($score == -1){
							$grade = $scoreArr[$subjectID][$ayID][$yearTermID]['Grade'];
							$grade = ($grade)? $grade: $SymbolEmpty;
							$returnRX .= "<td style=\"\">{$grade}</td>";
						}else{
							$rankDetails = $rankArr[$subjectID][$ayID][$yearTermID]; 
							
							$rank = $rankDetails['MeritRank'];
	// 						$rankName = $rankNameHtmlArr[$rank];
							$rankTitle = $rankDetails['MeritRankTitle'];
							
							if($rankDetails['rankDiff'] > 0){
								$style = 'font-weight: bold;';
							}else if($rankDetails['rankDiff'] == 0){
								$style = '';
							}else if($rankDetails['rankDiff'] == -1){
								$style = 'background-color: yellow;';
							}else{
								$style = 'background-color: #FFA4A4;';
							}
							
							$returnRX .= "<td style=\"{$style}\">{$rankName} {$rankTitle}</td>";
						}
					}
				}
				
				$returnRX .= '</tr>';
			}
			#### Data END ####
			
			#### Table Footer START ####
			$returnRX .= '</table>';
			#### Table Footer END ####
			######## Gen Table END ########


			$floatHeader = <<<HTML
		  		<link href="/templates/{$LAYOUT_SKIN}/css/content_30.css" rel="stylesheet" type="text/css">
  				<script>\$(function() {\$('.resultTable').floatHeader();});</script>
HTML;
			$exportButton = <<<HTML
			  	<div class="Conntent_tool">
					<a href="javascript: exportCSV()" class="export tablelink" style="float:none;">{$Lang['Btn']['Export']}</a>
			 
		  		</div>
HTML;
			if($isExport){
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				$exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportArr, $exportColumn=array(), "\t", "\r\n", "\t", 0, "11",1);
				$lexport->EXPORT_FILE('export.csv', $exportContent);
				return '';
			}
		  	return $floatHeader /*. $exportButton*/ . $returnRX;
		}
		
		function GetSubjectStats($AcademicYearID, $SubjectID, $ResultType=array(), $filterYearId=array(), $filterYearClassId=array(), $filterSubjectGroupId=array())
		{
	  		global $eclass_db, $Lang, $iPort, $intranet_session_language, $ec_iPortfolio, $PATH_WRT_ROOT, $sys_custom;

		  	$SymbolEmpty = $Lang['General']['EmptySymbol'] ;
// 	  		$lib_academic_year = new academic_year($AcademicYearID);
// 	  		$TermArr = ($lib_academic_year->Get_Term_List());
// 	  		$TermArr[0] = $ec_iPortfolio['overall_result'];
	  		
// 		  	######### Group subject START #########
// 			$rs = $this->getReleatedSubjectID();
// 			$SubjectIdArr = array($SubjectID);
// 			$SubjectIdArr = array_merge($SubjectIdArr, (array)$rs[$SubjectID]);
// 			$SubjectIdList = implode("','", $SubjectIdArr);
			
// 	  		foreach($rs as $parentSubj => $_subjIdArr){
// 	  			if(in_array($SubjectID, $_subjIdArr) || $parentSubj == $SubjectID){
//   					$parentSubjID = $parentSubj;
//   					break;
// 	  			}
// 	  		}
// 			######### Group subject END #########
	  		
	  		######### Get releated subject START #########
	  		$_subjectIds = $this->getReleatedSubjectID();
	  		$Subject_sql_arr = array();
	  		$SubjectID_Arr = explode('_',$SubjectID);
	  		$MainSubjectID = $SubjectID_Arr[0];
	  		$ComSubjectId = $SubjectID_Arr[1];
	  		$cond = " AND SubjectID = '$MainSubjectID' ";
	  		if($ComSubjectId==='0') {
	  			$cond .= " AND SubjectComponentID IS NULL ";
// 	  			$Subject_sql_arr = array_merge($Subject_sql_arr,(array)$MainSubjectID);
	  			$isCmp = false;
	  		} else {
	  			$cond .= " AND SubjectComponentID = '$ComSubjectId' ";
// 	  			$Subject_sql_arr = array_merge($Subject_sql_arr,(array)$ComSubjectId);
	  			$isCmp = true;
	  		}
// 	  		$parentSubjID = $SubjectID;
// 	  		foreach($_subjectIds as $parentSubj => $_subjIdArr){
// 	  			if(in_array($SubjectID, $_subjIdArr) || $parentSubj == $SubjectID){
//   					$parentSubjID = $parentSubj;
// 	  				$subjectIdArr = $_subjIdArr;
// 	  				$subjectIdArr[] = $parentSubj;
// 	  				break;
// 	  			}
// 	  		}
// 	  		$SubjectIdList = implode("','", $Subject_sql_arr);
	  		######### Get releated subject END #########
			
//	  		$filterYearSql = '';
//	  		if($filterYearId){
//	  			$_yearList = implode("','", $filterYearId);
//	  			$filterYearSql = " AND YearClassID IN ('{$_yearList}')";
//	  		}
	  		
	  		#### Get assessment and term in order START ####
// 			$sql = "SELECT DISTINCT 
// 				AcademicYearID, 
// 				YearTermID, 
// 				TermAssessment  
// 			FROM 
// 				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
// 			WHERE 
// 				AcademicYearID='{$AcademicYearID}' 
// 			AND 
// 				SubjectID IN ('{$SubjectIdList}') 
// 			ORDER BY 
// 				YearTermID, TermAssessment";
//villa
			$sql = "SELECT DISTINCT
						AcademicYearID,
						YearTermID,
						TermAssessment
					FROM
						{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
					WHERE
						AcademicYearID='{$AcademicYearID}'
					    $cond
					ORDER BY
						YearTermID, TermAssessment";
		  	$AcademicYearTerms = $this->returnResultSet($sql);
		  	
		  	$lib_academic_year = new academic_year($AcademicYearID);
		  	$AcademicYearName = $lib_academic_year->Get_Academic_Year_Name();
		  	$TermArr = ($lib_academic_year->Get_Term_List());
		  	$TermArr[0] = $ec_iPortfolio['overall_result'];
		  	
		  	foreach ($TermArr AS $TermID => $TermName)
		  	{
		  	    if ($TermID==$FromYearTermID || $TermID==$ToYearTermID) {
		  	        $TermIDs[] = $TermID;
		  	    } else {
		  			$TermIDs[] = $TermID;
		  		}
		  	}
		  	$AllTermIDs = implode(',',$TermIDs);
		  	
		  	### Get Term which having Data ###
		  	$Temp = Get_Array_By_Key($AcademicYearTerms, 'YearTermID');
		  	$comma = '';
		  	foreach ($Temp as $_Temp) {
		  	    if($_Temp!='') {
		  			$Temp_sql .= $comma.$_Temp;
		  			$comma = ',';
				}
		  	}
		  	$sql = "SELECT
		  				YearTermID, YearTermNameEN, YearTermNameB5
		  			FROM
		  				ACADEMIC_YEAR_TERM 
		  			WHERE
		  				YearTermID in ($Temp_sql) ";
		  	$TempArr = $this->returnResultSet($sql);
		  	foreach ($TempArr as $_TempArr) {
		  		$TermArr[$_TempArr['YearTermID']] = Get_Lang_Selection($_TempArr['YearTermNameB5'], $_TempArr['YearTermNameEN']);
		  	}
		  	$TermArr[0] = $ec_iPortfolio['overall_result'];
		  	
		  	if (sizeof($TermArr) > 0)
	  		{
	  			foreach ($TermArr as $TermID => $TermName)
	  			{
	  				for ($i_yesr_term=0; $i_yesr_term<sizeof($AcademicYearTerms); $i_yesr_term++)
	  				{
	  					if ($AcademicYearTerms[$i_yesr_term]["YearTermID"]==$TermID && $AcademicYearTerms[$i_yesr_term]["TermAssessment"]!="")
	  					{
	  						$TermAssessmentHeaders[$TermID][] = $AcademicYearTerms[$i_yesr_term]["TermAssessment"];
	  						$YearColSpan += 9;
	  					}
	  				}
	  				$TermIDarr[] = $TermID;
	  			}
	  			$YearColSpan += sizeof($TermArr) * 9;
	  		}
	  		
	  		#### Get assessment and term in order END ####
	  		$lib_academic_year = new academic_year($AcademicYearID);
	  		$AcademicYearName = $lib_academic_year->Get_Academic_Year_Name();
	  		
	  		$lib_subject_class_mapping = new subject_class_mapping();	  		
		  	$SubjectGroupTeacherMap = $lib_subject_class_mapping->Get_Class_Group_Teachers($AcademicYearID);
		  	
		  	#### Get full marks, pass marks START ####
		  	// Villa
	  		$SubectFullMarks = array();
	  		$SubectPassMarks = array();
// 	  		$sql = "SELECT DISTINCT
// 	  		YearID, FullMarkInt, PassMarkInt, SubjectID
// 	  		FROM
// 	  		{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK " .
// 	  		"WHERE
// 	  		AcademicYearID='{$AcademicYearID}'
// 	  		AND
// 	  		SubjectID IN ('{$SubjectIdList}') ";
	  		if($isCmp) {
	  		    $Subid = $ComSubjectId;
	  		} else {
				$Subid = $MainSubjectID;
			}
	  		$sql = "SELECT DISTINCT 
	  					YearID, FullMarkInt, PassMarkInt, SubjectID 
	  				FROM 
	  					{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK " .
					"WHERE 
	  					AcademicYearID='{$AcademicYearID}' AND 
	  					SubjectID = '$Subid' ";
			$SubectFullMarkArr = $this->returnResultSet($sql);
			for ($i=0; $i<sizeof($SubectFullMarkArr); $i++)
			{
				$SubectFullMarks[$SubectFullMarkArr[$i]["YearID"]][$SubectFullMarkArr[$i]["SubjectID"]] = $SubectFullMarkArr[$i]["FullMarkInt"];
				$passMark = ($SubectFullMarkArr[$i]["PassMarkInt"])? $SubectFullMarkArr[$i]["PassMarkInt"] : ((int)$SubectFullMarkArr[$i]["FullMarkInt"]) / 2;
				$SubectPassMarks[$SubectFullMarkArr[$i]["YearID"]][$SubectFullMarkArr[$i]["SubjectID"]] = $passMark;
			}
			#### Get full marks, pass marks END ####
	  		
	  		# forms/years
	  		$lib_year = new Year();
	  		$AllYearLevels = $lib_year->Get_All_Year_List();
	  		
// 		  	$libsubject = new subject($parentSubjID);
		  	//$AllSubjects = $libsubject->Get_All_Subjects();
		  	//villa
		  	$sql = "Select 
		  				EN_DES, CH_DES
		  			FROM
		  				ASSESSMENT_SUBJECT 
		  			WHERE 
		  				RecordID = '$MainSubjectID'";
		  	$temp = $this->returnResultSet($sql);
		  	$SubjectName = Get_Lang_Selection($temp[0]['CH_DES'], $temp[0]['EN_DES']);
		  	if($isCmp)
		  	{
		  		$sql = "Select
		  					EN_DES, CH_DES
		  				FROM
		  					ASSESSMENT_SUBJECT
		  				WHERE
		  					RecordID = '$ComSubjectId'";
		  		$temp = $this->returnResultSet($sql);
		  		$SubjectName .= " - ";
		  		$SubjectName .= Get_Lang_Selection($temp[0]['CH_DES'], $temp[0]['EN_DES']);
		  	}
// 		  	$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
	  		
	  		$returnRX = "<div class='chart_tables'><table class='common_table_list_v30 view_table_list_v30' >";
		  	$returnRX .= "<thead>";
		  	# header !!!
//		  	$returnRX .= "<tr><th colspan='4' >&nbsp;</th><th colspan='$YearColSpan'>{$AcademicYearName}</th></tr>";
		  	$returnRX .= "<tr><th colspan='4' >&nbsp;</th><th colspan='99'>{$AcademicYearName}</th></tr>";
		  	$returnRX .= "<tr><th colspan='4'>&nbsp;</th>";
		  	$tmpRXInside2 .= "<th align='center' nowrap>".$iPort["FormClassSubjectGroup"]."</th><th align='center' nowrap>".$iPort["report_col"]["type"]."</th>" .
		  			"<th align='center' nowrap>".$iPort["report_col"]["subject"]."</th><th align='center' nowrap>".$iPort["report_col"]["teacher"]."</th>";
		  	
		  	if (sizeof($TermArr) > 0)
	  		{
	  			foreach ($TermArr as $TermID => $TermName)
	  			{
	  				for ($i_term_id=0; $i_term_id<sizeof($TermAssessmentHeaders[$TermID]); $i_term_id++)
	  				{
	  					$returnRX .= "<th colspan='9' nowrap>".$TermAssessmentHeaders[$TermID][$i_term_id]."</th>";
  					$tmpRXInside2 .= "<th align='center' nowrap>".$iPort["report_col"]["total_student"]."</th><th align='center' nowrap>".$iPort["report_col"]["total_attempt"]."</th><th align='center' nowrap>".$iPort["report_col"]["total_pass"]."</th>" .
  							"<th align='center' nowrap>".$iPort["report_col"]["passing_rate"]."</th><th align='center' nowrap>".$iPort["report_col"]["full_mark"]."</th>" .
  							"<th align='center' nowrap>".$iPort["report_col"]["average"]."</th><th align='center' nowrap>".$iPort["report_col"]["highest_mark"]."</th><th align='center' nowrap>".$iPort["report_col"]["lowest_mark"]."</th><th align='center' nowrap>".$iPort["report_col"]["SD"]."</th>";
  					}
  				$returnRX .= "<th colspan='9' nowrap>".$TermName."</th>";
  				$tmpRXInside2 .= "<th align='center' nowrap>".$iPort["report_col"]["total_student"]."</th><th align='center' nowrap>".$iPort["report_col"]["total_attempt"]."</th><th align='center' nowrap>".$iPort["report_col"]["total_pass"]."</th>" .
  							"<th align='center' nowrap>".$iPort["report_col"]["passing_rate"]."</th><th align='center' nowrap>".$iPort["report_col"]["full_mark"]."</th>" .
  							"<th align='center' nowrap>".$iPort["report_col"]["average"]."</th><th align='center' nowrap>".$iPort["report_col"]["highest_mark"]."</th><th align='center' nowrap>".$iPort["report_col"]["lowest_mark"]."</th><th align='center' nowrap>".$iPort["report_col"]["SD"]."</th>";
  							
	  			}
	  		}
	  		$returnRX .= "</tr>";
	  		$tmpRXInside2 .= "</tr>";
	  		$returnRX .= $tmpRXInside2;
		  	$returnRX .= "</thead>";
		  	$returnRX .= "<tbody>";
		  	
		  	
		  	# 1. prepare form stats
		  	if(in_array('form',$ResultType))
		  	{
		  	    // Villa
		  	    if(!$isCmp) {
		  	        $cond1 = str_replace('IS NULL', '= 0 ', $cond);
		  	    } else {
		  			$cond1 = $cond;
		  		}
		  		$sql = "SELECT
        		  			*
        		  		FROM
        		  			{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN
        		  		WHERE
        		  			AcademicYearID='{$AcademicYearID}'
        		  		    $cond1 AND
        		  			IsMain=0 AND
        		  			YearClassID=0
        		  		ORDER BY YearTermID, ClassLevelID, YearClassID, TermAssessment ";
// 		  		$sql = "
// 					  		SELECT 
// 					  			* 
// 					  		FROM 
// 					  			{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN 
// 					  		WHERE 
// 					  			AcademicYearID='{$AcademicYearID}' 
// 					  		AND 
// 					  			SubjectID IN ('{$SubjectIdList}') 
// 					  		AND 
// 					  			SubjectComponentID=0 
// 					  		AND 
// 					  			IsMain=0 
// 					  		AND 
// 					  			YearClassID=0 
// 					  		ORDER BY YearTermID, ClassLevelID, YearClassID, TermAssessment ";
		  		$StatsArrary = $this->returnResultSet($sql);
		  		//debug($sql);
			  	
	  			for ($i_form=0; $i_form<sizeof($AllYearLevels); $i_form++)
	  			{
	  			    $formObj = $AllYearLevels[$i_form];
	  			    if($filterYearId) {
	  					if(!in_array($formObj['YearID'], $filterYearId)){
	  						continue;
	  					}
	  				}
	  				
	  				// Display only with data 20140829
	  				$HasResults = false;
	  				$returnTR = "<tr><td style=\"background-color:#FFE7B2;\" nowrap>".$formObj["YearName"]."</td><td style=\"background-color:#FFE7B2;\" nowrap>".$ec_iPortfolio['form']."</td><td style=\"background-color:#FFE7B2;\" nowrap>{$SubjectName}</td><td style=\"background-color:#FFE7B2;\">&nbsp;</td>";
	  				if (sizeof($TermArr) > 0)
			  		{
		  				$TotalResultObj = array();
			  			foreach ($TermArr as $TermID => $TermName)
			  			{
			  				$TermResultObj = array();
			  				
		  					#### TermAssessment START ####
		  					for ($i_term_id=0; $i_term_id<sizeof($TermAssessmentHeaders[$TermID]); $i_term_id++)
			  				{
			  					$ResultFound = false;
			  					for ($i_result=0; $i_result<sizeof($StatsArrary); $i_result++)
			  					{
			  						$ResultObj = $StatsArrary[$i_result];
			  						if ($ResultObj["ClassLevelID"]==$formObj["YearID"] && $ResultObj["YearTermID"]==$TermID)
			  						{
			  							if ($ResultObj["TermAssessment"]==$TermAssessmentHeaders[$TermID][$i_term_id])
			  							{
			  							    $ResultFound = true;
			  							    if ($ResultObj["AttemptTotal"] > 0)
			  							    {
			  							        $PassingRate = round(100 * $ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
				  							}
				  							else
				  							{
				  								$PassingRate = $SymbolEmpty;
				  							}
				  							
				  							// [2018-1114-1513-29235] Get correct marks (both normal & component subjects)
				  							$curSubjectID = $ResultObj['SubjectComponentID'] > 0? $ResultObj['SubjectComponentID'] : $ResultObj['SubjectID'];
				  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$curSubjectID];
				  							if(!$FullMark) {
				  							    $FullMark = 100;
				  							}
				  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$curSubjectID];
				  							if(!$PassMark) {
				  							    $PassMark = round($FullMark * 0.5, 1);
				  							}
// 				  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
// 				  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
                                            
                                            if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
                                            	if(
                                            		($ResultObj["TermAssessment"] == 'T1A3' && strpos($formObj["YearName"], '6') !== false) ||
                                            		($ResultObj["TermAssessment"] == 'T2A3')
                                            	){
                                            		$FullMark = '100';
                                            		$PassMark = '50';
                                            	}
                                            }
				  							
		  									$td_style_bg = " style=\"background-color:#FFE7B2;\"";
		  									$failed_style_font1 = "";
		  									$failed_style_font2 = "";
		  									if ($FullMark > 0)
				  							{
				  							    # style for failed @ Mean
				  							    if ($ResultObj["MEAN"] < $PassMark)
				  								{
				  									$td_style_bg = "style='background:yellow'";
				  									$failed_style_font1 = "<font color='red'>";
				  									$failed_style_font2 = "</font>";
				  								}
				  							}
				  							
						  					$returnTR .= "<td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["StudentTotal"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["AttemptTotal"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["PassTotal"]."</td>" .
					  							"<td align='center' style=\"background-color:#FFE7B2;\">".$PassingRate."</td><td align='center' style=\"background-color:#FFE7B2;\">".$FullMark."</td>" .
					  							"<td align='center' $td_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["HighestMark"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["LowestMark"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".round($ResultObj["SD"],1)."</td>";
					  						$HasResults = true;
			  							} /*elseif ($ResultObj["TermAssessment"]=="0")
			  							{
			  								$TermResultObj = $ResultObj;
			  								//debug_r($TermResultObj);
			  							}*/
			  						}
			  						
			  						/*if ($ResultObj["ClassLevelID"]==$formObj["YearID"] && $ResultObj['IsAnnual'] == '1')
		  							{
		  								$TotalResultObj = $ResultObj;
		  							}*/
			  					}
			  					
			  					if (!$ResultFound)
			  					{
			  						for ($i_empty=0; $i_empty<9; $i_empty++)
			  						{
			  							$returnTR .= "<td align='center' style=\"background-color:#FFE7B2;\">".$SymbolEmpty."</td>";
			  						}
			  					}
		  					}
		  					#### TermAssessment END ####
		  					
		  					#### Term START ####
		  					for ($i_result=0; $i_result<sizeof($StatsArrary); $i_result++)
		  					{
		  						$ResultObj = $StatsArrary[$i_result];
		  						if (
	  								$ResultObj["ClassLevelID"]==$formObj["YearID"] && 
	  								$ResultObj["YearTermID"]==$TermID &&
	  								$ResultObj["TermAssessment"]=="0" && 
		  							$ResultObj['IsAnnual'] == '0'
		  						){
	  								$TermResultObj = $ResultObj;
	  								break;
	  							}
		  					}
		  					if (sizeof($TermResultObj) > 0)
		  					{
		  					    $ResultObj = $TermResultObj;
		  					    if ($ResultObj["AttemptTotal"] > 0)
	  							{
	  								$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
	  							}
	  							else
	  							{
	  								$PassingRate = $SymbolEmpty;
	  							}
	  							
	  							// [2018-1114-1513-29235] Get correct marks (both normal & component subjects)
	  							$curSubjectID = $ResultObj['SubjectComponentID'] > 0? $ResultObj['SubjectComponentID'] : $ResultObj['SubjectID'];
	  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$curSubjectID];
	  							if(!$FullMark) {
	  							    $FullMark = 100;
	  							}
	  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$curSubjectID];
	  							if(!$PassMark) {
	  							    $PassMark = round($FullMark * 0.5, 1);
	  							}
// 	  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
// 	  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
				  							
								$td_style_bg = " style=\"background-color:#FFE7B2;\"";
								$failed_style_font1 = "";
								$failed_style_font2 = "";
								if ($FullMark > 0)
	  							{
	  							    # style for failed @ Mean
	  							    if ($ResultObj["MEAN"] < $PassMark)
	  								{
	  									$td_style_bg = "style='background:yellow'";
	  									$failed_style_font1 = "<font color='red'>";
	  									$failed_style_font2 = "</font>";
	  								}
	  							}
			  					$returnTR .= "<td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["StudentTotal"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["AttemptTotal"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["PassTotal"]."</td>" .
		  							"<td align='center' style=\"background-color:#FFE7B2;\">".$PassingRate."</td><td align='center' style=\"background-color:#FFE7B2;\">".$FullMark."</td>" .
		  							"<td align='center' $td_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["HighestMark"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["LowestMark"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".round($ResultObj["SD"],1)."</td>";
		  						$HasResults = true;
		  					}
		  					else
		  					{
		  					    if($TermID != 0) { // No Total
			  						for ($i_empty=0; $i_empty<9; $i_empty++)
			  						{
			  							$returnTR .= "<td align='center' style=\"background-color:#FFE7B2;\">".$SymbolEmpty."</td>";
			  						}
		  						}
		  					}
		  					#### Term END　 ####
			  			}
			  			
	  					#### Total START ####
		  				for ($i_result=0; $i_result<sizeof($StatsArrary); $i_result++)
		  				{
		  					$ResultObj = $StatsArrary[$i_result];
		  					if (
	  							$ResultObj["ClassLevelID"]==$formObj["YearID"] && 
	  							$ResultObj['IsAnnual'] == '1'
  							){
		  						$TotalResultObj = $ResultObj;
		  						break;
		  					}
		  				}
		  				if (sizeof($TotalResultObj) > 0)
	  					{
	  					    $ResultObj = $TotalResultObj;
	  					    if ($ResultObj["AttemptTotal"] > 0)
  							{
  								$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
  							}
  							else
  							{
  								$PassingRate = $SymbolEmpty;
  							}
  							
  							// [2018-1114-1513-29235] Get correct marks (both normal & component subjects)
  							$curSubjectID = $ResultObj['SubjectComponentID'] > 0? $ResultObj['SubjectComponentID'] : $ResultObj['SubjectID'];
  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$curSubjectID];
  							if(!$FullMark) {
  							    $FullMark = 100;
  							}
  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$curSubjectID];
  							if(!$PassMark) {
  							    $PassMark = round($FullMark * 0.5, 1);
  							}
//   						$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
//   						$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
			  							
							$td_style_bg = " style=\"background-color:#FFE7B2;\"";
							$failed_style_font1 = "";
							$failed_style_font2 = "";
							if ($FullMark > 0)
  							{
  							    # style for failed @ Mean
  							    if ($ResultObj["MEAN"] < $PassMark)
  								{
  									$td_style_bg = "style='background:yellow'";
  									$failed_style_font1 = "<font color='red'>";
  									$failed_style_font2 = "</font>";
  								}
  							}
		  					$returnTR .= "<td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["StudentTotal"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["AttemptTotal"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["PassTotal"]."</td>" .
	  							"<td align='center' style=\"background-color:#FFE7B2;\">".$PassingRate."</td><td align='center' style=\"background-color:#FFE7B2;\">".$FullMark."</td>" .
	  							"<td align='center' $td_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["HighestMark"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".$ResultObj["LowestMark"]."</td><td align='center' style=\"background-color:#FFE7B2;\">".round($ResultObj["SD"],1)."</td>";
	  						$HasResults = true;
	  					}
	  					else
	  					{
	  						for ($i_empty=0; $i_empty<9; $i_empty++)
	  						{
	  							$returnTR .= "<td align='center' style=\"background-color:#FFE7B2;\">".$SymbolEmpty."</td>";
	  						}
	  					}
	  					#### Total END　 ####
			  			
			  		}
			  		if ($HasResults)
			  		{
	  					$returnRX .= $returnTR . "</tr>";
			  		}
	  			}
		  	} # END Form
  			
	  		# 2. prepare class stats
		  	if(in_array('class',$ResultType))
		  	{
		  		$lib_year_class = new year_class();
		  		$AllYearClasses = $lib_year_class->Get_All_Class_List($AcademicYearID);
		  		//debug_r($AllYearClasses);
		  		
		  		// Villa
		  		if(!$isCmp) {
		  		    $cond1 = str_replace('IS NULL', '= 0 ', $cond);
		  		} else {
		  			$cond1 = $cond;
		  		}
		  		$sql = "SELECT 
		  					* 
		  				FROM 
		  					{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN 
		  				WHERE 
		  					AcademicYearID='{$AcademicYearID}'
		  					$cond1 AND 
		  					IsMain=0 AND 
		  					YearClassID>0 
		  				ORDER BY 
		  					YearTermID, ClassLevelID, YearClassID, TermAssessment ";
// 		  		$sql = "SELECT * FROM {$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN WHERE AcademicYearID='{$AcademicYearID}' AND SubjectID IN ('{$SubjectIdList}') AND SubjectComponentID=0 AND IsMain=0 AND YearClassID>0 ORDER BY YearTermID, ClassLevelID, YearClassID, TermAssessment ";
		  		$StatsArrary = $this->returnResultSet($sql);
		  		
		  		for ($i_form=0; $i_form<sizeof($AllYearClasses); $i_form++)
	  			{
	  			    $formObj = $AllYearClasses[$i_form];
	  			    if($filterYearId) {
	  			        if(!in_array($formObj['YearID'], $filterYearId)) {
	  						continue;
	  					}
	  				}
                    // [EJ DM#1260-1261] [Class Teacher] SKIP > not subject panel + not current class teacher
                    if($filterYearId && $filterYearClassId && isset($filterYearClassId[$formObj['YearID']])){
                        if(!in_array($formObj["YearClassID"], (array)$filterYearClassId[$formObj['YearID']])){
                            continue;
                        }
                    }
	  				
					$lib_year_class = new year_class($formObj["YearClassID"], false, $GetClassTeacherList=false,$GetClassStudentList=true,$GetLockedSGArr=false);
	  				$ClassStudentArr = $lib_year_class->ClassStudentList;
	  				
	  				$StudentIDsArr = array();
	  				$SubjectTeachersArr = array();
	  				for ($i_cs=0; $i_cs<sizeof($ClassStudentArr); $i_cs++)
	  				{
	  					$StudentIDsArr[] = $ClassStudentArr[$i_cs]["UserID"];
	  				}
	  				if (sizeof($StudentIDsArr) > 0)
	  				{
	  					$sql_user_ids = implode(",", $StudentIDsArr);
	  					$sql_term_ids = implode(",", $TermIDarr);
	  					
	  					// Villa
// 	  					$sql = "SELECT DISTINCT stcu.SubjectGroupID " .
// 	  							" FROM SUBJECT_TERM_CLASS_USER AS stcu, SUBJECT_TERM AS st " .
// 	  							" WHERE stcu.UserID IN ($sql_user_ids) AND st.SubjectID IN ('{$SubjectIdList}') AND st.SubjectGroupID=stcu.SubjectGroupID AND st.YearTermID IN ($sql_term_ids) ";
	  					$sql = "SELECT 
	  								DISTINCT stcu.SubjectGroupID " .
	  							" FROM 
	  									SUBJECT_TERM_CLASS_USER AS stcu, SUBJECT_TERM AS st " .
	  							" WHERE 
	  									stcu.UserID IN ($sql_user_ids) 
	  							AND st.SubjectID = '$MainSubjectID'
	  							AND st.SubjectGroupID=stcu.SubjectGroupID 
	  							AND st.YearTermID IN ($AllTermIDs) ";
	  					$SubjectGroupsArr = $this->returnVector($sql);
	  					for ($i_sg=0; $i_sg<sizeof($SubjectGroupsArr); $i_sg++)
	  					{
	  						if (is_array($SubjectTeachersArr) && is_array($SubjectGroupTeacherMap[$SubjectGroupsArr[$i_sg]]))
	  						{
	  							$SubjectTeachersArr = array_merge($SubjectTeachersArr, $SubjectGroupTeacherMap[$SubjectGroupsArr[$i_sg]]);
	  						}
	  						elseif (is_array($SubjectTeachersArr))
	  						{
	  							$SubjectTeachersArr = $SubjectTeachersArr;
	  						}
	  						elseif (is_array($SubjectGroupTeacherMap[$SubjectGroupsArr[$i_sg]]))
	  						{
	  							$SubjectTeachersArr = $SubjectGroupTeacherMap[$SubjectGroupsArr[$i_sg]];
	  						}
	  					}
	  					
	  					# another term
	  					/*
	  					if ($FromYearTermID!=$ToYearTermID)
	  					{
		  					$sql_user_ids = implode(",", $StudentIDsArr);
		  					$sql = "SELECT DISTINCT stcu.SubjectGroupID " .
		  							" FROM SUBJECT_TERM_CLASS_USER AS stcu, SUBJECT_TERM AS st " .
		  							" WHERE stcu.UserID IN ($sql_user_ids) AND st.SubjectID='$SubjectID' AND st.SubjectGroupID=stcu.SubjectGroupID AND st.YearTermID='$ToYearTermID' ";
		  					$SubjectGroupsArr = $this->returnVector($sql);
		  					for ($i_sg=0; $i_sg<sizeof($SubjectGroupsArr); $i_sg++)
		  					{
		  						$SubjectTeachersArr = array_merge($SubjectTeachersArr, $SubjectGroupTeacherMap[$SubjectGroupsArr[$i_sg]]);
		  					}
	  					}
	  					*/
	  				}
	  				$Teachers = $this->GetTeacherNames($SubjectTeachersArr);  				
	  				
	  				$HasResults = false;
	  				$returnTR = "<tr><td style=\"background-color:#DAEEFF;\" nowrap>".$formObj["ClassTitle"]."</td><td style=\"background-color:#DAEEFF;\" nowrap>".$ec_iPortfolio['class']."</td><td style=\"background-color:#DAEEFF;\" nowrap>{$SubjectName}</td><td style=\"background-color:#DAEEFF;\" nowrap>{$Teachers}</td>";
	  				if (sizeof($TermArr)>0)
			  		{
			  			foreach ($TermArr as $TermID => $TermName)
			  			{
			  				$TermResultObj = array();
			  				
			  				#### TermAssessment START ####
			  				for ($i_term_id=0; $i_term_id<sizeof($TermAssessmentHeaders[$TermID]); $i_term_id++)
			  				{
			  					$ResultFound = false;
			  					for ($i_result=0; $i_result<sizeof($StatsArrary); $i_result++)
			  					{
			  						$ResultObj = $StatsArrary[$i_result];
			  						if ($ResultObj["YearClassID"]==$formObj["YearClassID"] && $ResultObj["YearTermID"]==$TermID)
			  						{
			  							if ($ResultObj["TermAssessment"]==$TermAssessmentHeaders[$TermID][$i_term_id])
			  							{
			  							    $ResultFound = true;
			  							    if ($ResultObj["AttemptTotal"] > 0)
				  							{
				  								$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
				  							}
				  							else
				  							{
				  								$PassingRate = $SymbolEmpty;
				  							}
				  							
				  							// [2018-1114-1513-29235] Get correct marks (both normal & component subjects)
				  							$curSubjectID = $ResultObj['SubjectComponentID'] > 0? $ResultObj['SubjectComponentID'] : $ResultObj['SubjectID'];
				  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$curSubjectID];
				  							if(!$FullMark) {
				  							    $FullMark = 100;
				  							}
				  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$curSubjectID];
				  							if(!$PassMark) {
				  							    $PassMark = round($FullMark * 0.5, 1);
				  							}
// 				  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
// 				  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
                                            
                                            if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
                                            	if(
                                            		($ResultObj["TermAssessment"] == 'T1A3' && strpos($formObj["ClassTitle"], '6') !== false) ||
                                            		($ResultObj["TermAssessment"] == 'T2A3')
                                            	){
                                            		$FullMark = '100';
                                            		$PassMark = '50';
                                            	}
                                            }
				  							
		  									$td_style_bg = " style=\"background-color:#DAEEFF;\"";
		  									$failed_style_font1 = "";
		  									$failed_style_font2 = "";
		  									if ($FullMark > 0)
				  							{
				  							    # style for failed @ Mean
				  							    if ($ResultObj["MEAN"] < $PassMark)
				  								{
				  									$td_style_bg = "style='background:yellow'";
				  									$failed_style_font1 = "<font color='red'>";
				  									$failed_style_font2 = "</font>";
				  								}
				  							}
						  					$returnTR .= "<td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["StudentTotal"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["AttemptTotal"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["PassTotal"]."</td>" .
					  							"<td align='center' style=\"background-color:#DAEEFF;\">".$PassingRate."</td><td align='center' style=\"background-color:#DAEEFF;\">".$FullMark."</td>" .
					  							"<td align='center' $td_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["HighestMark"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["LowestMark"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".round($ResultObj["SD"],1)."</td>";
					  						$HasResults = true;
			  							}/* elseif ($ResultObj["TermAssessment"]=="0")
			  							{
			  								$TermResultObj = $ResultObj;
			  								//debug_r($TermResultObj);
			  							}*/
			  						}
			  						
			  						/*if ($ResultObj["YearClassID"]==$formObj["YearClassID"] && $ResultObj['IsAnnual'] == '1')
		  							{
		  								$TotalResultObj = $ResultObj;
		  							}*/
			  					}
			  					
			  					if (!$ResultFound)
			  					{
			  						for ($i_empty=0; $i_empty<9; $i_empty++)
			  						{
			  							$returnTR .= "<td align='center' style=\"background-color:#DAEEFF;\">".$SymbolEmpty."</td>";
			  						}
			  					}
		  					}
			  				#### TermAssessment END ####
			  				
			  				#### Term START ####
		  					for ($i_result=0; $i_result<sizeof($StatsArrary); $i_result++)
		  					{
		  						$ResultObj = $StatsArrary[$i_result];
		  						if (
	  								$ResultObj["YearClassID"]==$formObj["YearClassID"] && 
		  							$ResultObj["YearTermID"]==$TermID &&
	  								$ResultObj["TermAssessment"]=="0" && 
		  							$ResultObj['IsAnnual'] == '0'
		  						){
	  								$TermResultObj = $ResultObj;
	  								break;
	  							}
		  					}
		  					if (sizeof($TermResultObj) > 0)
		  					{
		  					    $ResultObj = $TermResultObj;
		  					    if ($ResultObj["AttemptTotal"] > 0)
	  							{
	  								$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
	  							}
	  							else
	  							{
	  								$PassingRate = $SymbolEmpty;
	  							}
	  							
	  							// [2018-1114-1513-29235] Get correct marks (both normal & component subjects)
	  							$curSubjectID = $ResultObj['SubjectComponentID'] > 0? $ResultObj['SubjectComponentID'] : $ResultObj['SubjectID'];
	  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$curSubjectID];
	  							if(!$FullMark) {
	  							    $FullMark = 100;
	  							}
	  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$curSubjectID];
	  							if(!$PassMark) {
	  							    $PassMark = round($FullMark * 0.5, 1);
	  							}
// 	  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
// 	  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
				  				
								$td_style_bg = " style=\"background-color:#DAEEFF;\"";
								$failed_style_font1 = "";
								$failed_style_font2 = "";
								if ($FullMark > 0)
	  							{
	  							    # style for failed @ Mean
	  							    if ($ResultObj["MEAN"] < $PassMark)
	  								{
	  									$td_style_bg = "style='background:yellow'";
	  									$failed_style_font1 = "<font color='red'>";
	  									$failed_style_font2 = "</font>";
	  								}
	  							}
			  					$returnTR .= "<td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["StudentTotal"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["AttemptTotal"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["PassTotal"]."</td>" .
		  							"<td align='center' style=\"background-color:#DAEEFF;\">".$PassingRate."</td><td align='center' style=\"background-color:#DAEEFF;\">".$FullMark."</td>" .
		  							"<td align='center' $td_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["HighestMark"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["LowestMark"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".round($ResultObj["SD"],1)."</td>";
		  						$HasResults = true;
		  					}
		  					else
		  					{
		  					    if($TermID != 0) { // No Total
			  						for ($i_empty=0; $i_empty<9; $i_empty++)
			  						{
			  							$returnTR .= "<td style=\"background-color:#DAEEFF;\" align='center'>".$SymbolEmpty."</td>";
			  						}
		  						}
		  					}
			  				#### Term END ####
			  			}
			  			
		  				#### Total START ####
		  				for ($i_result=0; $i_result<sizeof($StatsArrary); $i_result++)
		  				{
		  					$ResultObj = $StatsArrary[$i_result];
		  					if (
	  							$ResultObj["YearClassID"]==$formObj["YearClassID"] && 
	  							$ResultObj['IsAnnual'] == '1'
  							){
		  						$TotalResultObj = $ResultObj;
		  						break;
		  					}
		  				}
		  				if (sizeof($TotalResultObj) > 0)
	  					{
	  					    $ResultObj = $TotalResultObj;
	  					    if ($ResultObj["AttemptTotal"] > 0)
  							{
  								$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
  							}
  							else
  							{
  								$PassingRate = $SymbolEmpty;
  							}
  							
  							// [2018-1114-1513-29235] Get correct marks (both normal & component subjects)
  							$curSubjectID = $ResultObj['SubjectComponentID'] > 0? $ResultObj['SubjectComponentID'] : $ResultObj['SubjectID'];
  							$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$curSubjectID];
  							if(!$FullMark) {
  							    $FullMark = 100;
  							}
  							$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$curSubjectID];
  							if(!$PassMark) {
  							    $PassMark = round($FullMark * 0.5, 1);
  							}
//   						$FullMark = (int) $SubectFullMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
//   						$PassMark = (int) $SubectPassMarks[$formObj["YearID"]][$ResultObj['SubjectID']];
			  				
							$td_style_bg = " style=\"background-color:#DAEEFF;\"";
							$failed_style_font1 = "";
							$failed_style_font2 = "";
							if ($FullMark > 0)
  							{
  							    # style for failed @ Mean
  							    if ($ResultObj["MEAN"] < $PassMark)
  								{
  									$td_style_bg = "style='background:yellow'";
  									$failed_style_font1 = "<font color='red'>";
  									$failed_style_font2 = "</font>";
  								}
  							}
		  					$returnTR .= "<td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["StudentTotal"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["AttemptTotal"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["PassTotal"]."</td>" .
	  							"<td align='center' style=\"background-color:#DAEEFF;\">".$PassingRate."</td><td align='center' style=\"background-color:#DAEEFF;\">".$FullMark."</td>" .
	  							"<td align='center' $td_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["HighestMark"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".$ResultObj["LowestMark"]."</td><td align='center' style=\"background-color:#DAEEFF;\">".round($ResultObj["SD"],1)."</td>";
	  						//$HasResults = true;
	  					}
	  					else
	  					{
	  						for ($i_empty=0; $i_empty<9; $i_empty++)
	  						{
	  							$returnTR .= "<td align='center' style=\"background-color:#DAEEFF;\">".$SymbolEmpty."</td>";
	  						}
	  					}
		  				#### Total END ####
			  		}
			  		if ($HasResults)
			  		{
	  					$returnRX .= $returnTR . "</tr>";
			  		}
	  			}
		  	}
  			
	  		
		  	# 3. prepare subject group stats
		  	if(in_array('group',$ResultType))
		  	{
		  		if ($sql_term_ids=="")
		  		{
	  				$sql_term_ids = implode(",", $TermIDarr);
		  		}
		  		
		  		// Villa
		  		if(!$isCmp) {
		  		    $cond1 = str_replace('IS NULL', '= 0 ', $cond);
		  		} else {
		  			$cond1 = $cond;
		  			
		  			#	2020-05-05 (Philips) [2020-0225-1050-31235] - Display Subject Group of Main Subject
		  			if($sys_custom['SDAS']['SubjectStats']['DisplayMainSubjectSG']){
		  				$cond1 = " AND SubjectID = '$MainSubjectID' ";
		  			}
		  		}
// 		  		$sql =  " SELECT " .
// 		  				"	st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 " .
// 		  				" FROM " .
// 		  				"	SUBJECT_TERM as st " .
// 		  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
// 		  				" WHERE " .
// 		  				"	st.YearTermID IN ($sql_term_ids) AND st.SubjectID IN ('{$SubjectIdList}') AND st.SubjectComponentID=0 " .
// 		  				" ORDER BY " .
// 		  				"	stc.ClassTitleEN ";
		  		$sql =  " SELECT " .
		  				"	st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 " .
		  				" FROM " .
		  				"	SUBJECT_TERM as st " .
		  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
		  				" WHERE " .
		  				"	st.YearTermID IN ($AllTermIDs) $cond1 " .
		  				" ORDER BY " .
		  				"	stc.ClassTitleEN ";
			 	$SubjectGroupArr = $this->returnResultSet($sql);
		  		//debug_rt($SubjectGroupArr);
                
			 	//	$FlagDone = array();
			 	$SubjectGroupRows = array();
		  		for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
		  		{
                    // [EJ DM#1260-1261] [Class / Subject Teacher] SKIP > not subject panel + not current class / subject group teacher
                    if($filterYearId && $filterSubjectGroupId){
                        if(!in_array($SubjectGroupArr[$i]["SubjectGroupID"], (array)$filterSubjectGroupId)){
                            continue;
                        }
                    }

                    $SubjectGroupArr[$i]['SubjectClassName'] = Get_Lang_Selection($SubjectGroupArr[$i]['ClassTitleB5'], $SubjectGroupArr[$i]['ClassTitleEN']);
		  			$SubjectGroupObj = $SubjectGroupArr[$i];
		  			
		  			$SubjectGroupID = $SubjectGroupObj["SubjectGroupID"];
		  			$YearTermID = $SubjectGroupObj["YearTermID"];
		  			$_SubjectID = $SubjectGroupObj["SubjectID"];
		  			$InvolvedStudentArr = $lib_subject_class_mapping->Get_Subject_Group_Student_List(array($SubjectGroupID));
	                
		  			$StudentIDs = array();
		  			for ($i_std=0; $i_std<sizeof($InvolvedStudentArr); $i_std++)
		  			{
		  				$StudentIDs[] = $InvolvedStudentArr[$i_std]["UserID"];
		  			}
		  			//$SubjectGroupResult[$SubjectGroupObj["SubjectClassName"]][$YearTermID]["StudentTotal"] = sizeof($StudentIDs);
		  			
		  			if (sizeof($StudentIDs) > 0)
		  			{
						$sql_student_ids = implode(",", $StudentIDs);
						$SubjectFormFullMark = "";
		  				
		  				# get the result from each group
		  				//villa
// 						$sql = "SELECT Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID, UserID, IsAnnual FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
// 								"AcademicYearID='{$AcademicYearID}' AND (YearTermID='{$YearTermID}' OR YearTermID IS NULL) " .
// 								"AND UserID IN ({$sql_student_ids}) AND SubjectID IN ('{$SubjectIdList}') AND SubjectComponentID IS NULL " .
// 								"ORDER BY TermAssessment, SubjectID, SubjectComponentID,  IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
						$sql = "SELECT 
									Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID, UserID, IsAnnual 
								FROM 
									{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
								WHERE 
									Score>=0 
								AND 
									Score IS NOT NULL 
								AND " .
									"AcademicYearID='{$AcademicYearID}' 
								AND (YearTermID='{$YearTermID}' OR YearTermID IS NULL) " .
							"AND 
								UserID IN ({$sql_student_ids}) 
							$cond " .
							"ORDER BY TermAssessment, SubjectID, SubjectComponentID,  IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
						$SubjectMarks = $this->ReturnResultSet($sql);
						
						$PreviousTermAssessment = "";
						$PreviousIsAnnual = "";
						$MarkRecordArr = array("LOWEST"=>9999, "HIGHEST"=>0, "PassTotal"=>0, "AttemptTotal"=>0);
						
						$AllScores = array();
						for ($i_mark=0; $i_mark<sizeof($SubjectMarks); $i_mark++)
						{
							$EntryObj = $SubjectMarks[$i_mark];
							if ($SubjectFormFullMark=="")
							{
								# supposed all students in that subject group are from the same class level (form)
								$lib_year_class = new year_class($EntryObj["YearClassID"]);
								
								// [2018-1114-1513-29235] Get correct marks (both normal & component subjects)
								$curSubjectID = $EntryObj['SubjectComponentID'] > 0? $EntryObj['SubjectComponentID'] : $EntryObj['SubjectID'];
								$SubjectFormFullMark = $SubectFullMarks[(int)$lib_year_class->YearID][$curSubjectID];
								if(!$SubjectFormFullMark) {
								    $SubjectFormFullMark = 100;
								}
								$SubjectFormPassMark = $SubectPassMarks[(int)$lib_year_class->YearID][$curSubjectID];
								if(!$SubjectFormPassMark) {
								    $SubjectFormPassMark = round($SubjectFormFullMark * 0.5, 1);
								}
// 								$SubjectFormFullMark = $SubectFullMarks[(int)$lib_year_class->YearID][$EntryObj['SubjectID']];
// 								$SubjectFormPassMark = $SubectPassMarks[(int)$lib_year_class->YearID][$EntryObj['SubjectID']];
							}
							
							# term
							$TermAssessment = ($EntryObj["TermAssessment"]=="") ? "0" : $EntryObj["TermAssessment"];
							/*
							if ($TermAssessment!="0" && !$FlagDone[$YearTermID][$TermAssessment])
							{
								$TermAssessmentHeaders[$YearTermID][] = $TermAssessment;
								$FlagDone[$YearTermID][$TermAssessment] = true;
							}
							*/
                            if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
								$lib_year_class = new year_class($EntryObj["YearClassID"]);
                            	if(
                            		($PreviousTermAssessment == 'T1A3' && strpos($lib_year_class->YearName, '6') !== false) ||
                            		($PreviousTermAssessment == 'T2A3')
                            	){
                            		$SubjectFormFullMark = '100';
                            		$SubjectFormPassMark = '50';
                            	}
                            }
				
							# assessement
							if (($i_mark!=0 && ($PreviousTermAssessment!=$TermAssessment || $PreviousIsAnnual != $EntryObj['IsAnnual'])) || $i_mark==sizeof($SubjectMarks)-1)
							{
								if ($i_mark==sizeof($SubjectMarks)-1)
								{
									$AllScores[] = $EntryObj["Score"];
									$PreviousIsAnnual = $EntryObj['IsAnnual'];
									$PreviousTermAssessment = $TermAssessment;
									
									if ($MarkRecordArr["LOWEST"]>$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
									{
										$MarkRecordArr["LOWEST"] = $EntryObj["Score"];
									}
									
									if ($MarkRecordArr["HIGHEST"]<$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
									{
										$MarkRecordArr["HIGHEST"] = $EntryObj["Score"];
									}
									
									if ($EntryObj["Score"]!="" && $EntryObj["Score"]>=00)
									{
										if ($EntryObj["Score"]>=$SubjectFormPassMark)
										{
											$MarkRecordArr["PassTotal"] ++;
										}
										$MarkRecordArr["AttemptTotal"] ++;
									}
								}
								$sd = round(standard_deviation($AllScores), 2);
								//debug($sd);
								$mean = round(array_sum($AllScores)/sizeof($AllScores), 3);
								
								$MarkRecordArr["SubjectID"] = $_SubjectID;
								$MarkRecordArr["SubjectGroupID"] = $SubjectGroupID;
								$MarkRecordArr["FullMark"] = $SubjectFormFullMark;
								$MarkRecordArr["PassMark"] = $SubjectFormPassMark;
								$MarkRecordArr["StudentTotal"] = sizeof($StudentIDs);
								$MarkRecordArr["MEAN"] = $mean;
								$MarkRecordArr["SD"] = $sd;
								$MarkRecordArr["IsAnnual"] = $PreviousIsAnnual;
								if ($PreviousIsAnnual) {
									$tmpYearTermID = 0;
									$tmpPreviousTermAssessment = 0;
								}
								else {
									$tmpYearTermID = $YearTermID;
									$tmpPreviousTermAssessment = $PreviousTermAssessment;
								}
								
								$SubjectGroupResult[$SubjectGroupObj["SubjectClassName"]][$tmpYearTermID][$tmpPreviousTermAssessment] = $MarkRecordArr;
								$SubjectGroupResult[$SubjectGroupObj["SubjectClassName"]]["SubjectID"] = $_SubjectID;
								
								if (!in_array($SubjectGroupObj["SubjectClassName"], $SubjectGroupRows))
								{
									$SubjectGroupRows[] = $SubjectGroupObj["SubjectClassName"];
									$SubjectGroupIDRows[$SubjectGroupObj["SubjectClassName"]] = $SubjectGroupObj["SubjectGroupID"];
								}
								$MarkRecordArr = array("LOWEST"=>9999, "HIGHEST"=>0, "PassTotal"=>0, "AttemptTotal"=>0);
								
								$AllScores = array();
							}
							$AllScores[] = $EntryObj["Score"];
							
							if ($MarkRecordArr["LOWEST"]>$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
							{
								$MarkRecordArr["LOWEST"] = $EntryObj["Score"];
							}
							
							if ($MarkRecordArr["HIGHEST"]<$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
							{
								$MarkRecordArr["HIGHEST"] = $EntryObj["Score"];
							}
							
							if ($EntryObj["Score"]!="" && $EntryObj["Score"]>=00)
							{
								if ($EntryObj["Score"]>=$SubjectFormPassMark)
								{
									$MarkRecordArr["PassTotal"] ++;
								}
								$MarkRecordArr["AttemptTotal"] ++;
							}
							$PreviousTermAssessment = $TermAssessment;
							$PreviousIsAnnual = $EntryObj['IsAnnual'];
						}
		  			}
		  		} 
		  	}
	  		
	  		# show results
  			for ($i=0; $i<sizeof($SubjectGroupRows); $i++)
	  		{
	  			$SubjectGroupName = $SubjectGroupRows[$i];
	  			$SubjectGroupID = $SubjectGroupIDRows[$SubjectGroupName];
	  			$ShownSubjectName = false;
	  			
	  			$returnRX .= "<tr><td nowrap>{$SubjectGroupName}</td><td nowrap>".$iPort["SubjectGroup"]."</td>";
	  			
	  			$TotalResultObj = array();
	  			if (sizeof($TermArr) > 0)
		  		{
		  			foreach ($TermArr as $TermID => $TermName)
		  			{
		  				$TermResultObj = array();
		  				
		  				# assessment results
		  				
		  				#### TermAssessment START ####
		  				for ($i_term_id=0; $i_term_id<sizeof($TermAssessmentHeaders[$TermID]); $i_term_id++)
		  				{
		  					$SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupName][$TermID];
		  					if (!$ShownSubjectName)
		  					{
// 							  	$libsubject = new subject($SubjectGroupResult[$SubjectGroupName]["SubjectID"]);
							  	$libsubject = new subject($parentSubjID);
							  	$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
							  	
								$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID]);
	  							$returnRX .= "<td>{$SubjectName}</td><td>{$Teachers}</td>";
	  							
	  							$ShownSubjectName = true;
		  					}
		  					
		  					$ResultFound = false;
		  					if (sizeof($SubjectGroupResultObj) > 0)
		  					{
			  					foreach($SubjectGroupResultObj AS $TermAssessment => $ResultObj)
			  					{
		  							if ($TermAssessment!="0" && $TermAssessment==$TermAssessmentHeaders[$TermID][$i_term_id])
		  							{
		  							    $ResultFound = true;
		  							    if ($ResultObj["AttemptTotal"] > 0)
			  							{
			  								$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
			  							}
			  							else
			  							{
			  								$PassingRate = $SymbolEmpty;
			  							}
			  							$FullMark = (int) $ResultObj["FullMark"];
			  							$PassMark = (int) $ResultObj["PassMark"];
			  							
										$failed_style_bg = "";
										$failed_style_font1 = "";
										$failed_style_font2 = "";
			  							if ($FullMark>0)
			  							{
			  								# style for failed @ Mean
			  								if ($ResultObj["MEAN"]<$PassMark)
			  								{
			  									$failed_style_bg = "style='background:yellow'";
			  									$failed_style_font1 = "<font color='red'>";
			  									$failed_style_font2 = "</font>";
			  								}
			  							}
					  					$returnRX .= "<td align='center'>".$ResultObj["StudentTotal"]."</td><td align='center'>".$ResultObj["AttemptTotal"]."</td><td align='center'>".$ResultObj["PassTotal"]."</td>" .
				  							"<td align='center'>".$PassingRate."</td><td align='center'>".$FullMark."</td>" .
				  							"<td align='center' $failed_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center'>".$ResultObj["HIGHEST"]."</td><td align='center'>".$ResultObj["LOWEST"]."</td><td align='center'>".round($ResultObj["SD"],1)."</td>";
		  							}/* elseif ($TermAssessment=="0")
		  							{
		  								$TermResultObj = $ResultObj;
		  								//debug_r($TermResultObj);
		  							}*/
			  					}
		  					}
		  					
		  					if (!$ResultFound)
		  					{
		  						
		  						for ($i_empty=0; $i_empty<9; $i_empty++)
		  						{
		  							$returnRX .= "<td align='center'>".$SymbolEmpty."</td>";
		  						}
		  					}
		  				}
  						if ($SubjectGroupResult[$SubjectGroupName][0][0]['IsAnnual'] == '1')
						{
							$TotalResultObj = $SubjectGroupResult[$SubjectGroupName][0][0];
						}
		  				#### TermAssessment END ####
		  				
		  				#### Term START ####
  						if (!$ShownSubjectName)
	  					{
						  	$libsubject = new subject($SubjectGroupResult[$SubjectGroupName]["SubjectID"]);
						  	$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
						  	
							$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID]);
  							$returnRX .= "<td>{$SubjectName}</td><td>{$Teachers}</td>";
  							
  							$ShownSubjectName = true;
	  					}
	  					$SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupName][$TermID];
	  					if (sizeof($SubjectGroupResultObj)>0)
	  					{
		  					foreach($SubjectGroupResultObj AS $TermAssessment => $ResultObj)
		  					{
		  						if (
	  								$TermAssessment=="0" &&
	  								$ResultObj['IsAnnual'] == '0'
  								){
		  							$TermResultObj = $ResultObj;
		  							break;
		  						}
		  					}
	  					}
		  				if (sizeof($TermResultObj)>0)
	  					{
	  						$ResultObj = $TermResultObj;
							if ($ResultObj["AttemptTotal"]>0)
  							{
  								$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
  							}
  							else
  							{
  								$PassingRate = $SymbolEmpty;
  							}
  							$FullMark = (int) $ResultObj["FullMark"];
							$PassMark = $ResultObj['PassMark'];
			  							
							$failed_style_bg = "";
							$failed_style_font1 = "";
							$failed_style_font2 = "";
							if ($FullMark > 0)
  							{
  								# style for failed @ Mean
  								if ($ResultObj["MEAN"] < $PassMark)
  								{
  									$failed_style_bg = "style='background:yellow'";
  									$failed_style_font1 = "<font color='red'>";
  									$failed_style_font2 = "</font>";
  								}
  							}
		  					$returnRX .= "<td align='center'>".$ResultObj["StudentTotal"]."</td><td align='center'>".$ResultObj["AttemptTotal"]."</td><td align='center'>".$ResultObj["PassTotal"]."</td>" .
	  							"<td align='center'>".$PassingRate."</td><td align='center'>".$FullMark."</td>" .
	  							"<td align='center' $failed_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center'>".$ResultObj["HIGHEST"]."</td><td align='center'>".$ResultObj["LOWEST"]."</td><td align='center'>".round($ResultObj["SD"],1)."</td>";
	  					}
	  					else
	  					{
	  						if (!$ShownSubjectName)
		  					{
							  	$libsubject = new subject($SubjectGroupResult[$SubjectGroupName]["SubjectID"]);
							  	$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
							  	
								$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID]);
	  							$returnRX .= "<td>{$SubjectName}</td><td>{$Teachers}</td>";
	  							
	  							$ShownSubjectName = true;
		  					}
	  						if($TermID != 0){ // No Total
		  						for ($i_empty=0; $i_empty<9; $i_empty++)
		  						{
		  							$returnRX .= "<td align='center'>".$SymbolEmpty."</td>";
		  						}
	  						}
	  					}
		  				#### Term END ####
		  			}
		  			
		  			#### Total START ####
		  			if (sizeof($TotalResultObj) > 0)
  					{
  					    $ResultObj = $TotalResultObj;
  					    if ($ResultObj["AttemptTotal"] > 0)
						{
							$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
						}
						else
						{
							$PassingRate = $SymbolEmpty;
						}
						$FullMark = (int) $ResultObj["FullMark"];
						$PassMark = $ResultObj['PassMark'];
		  							
						$failed_style_bg = "";
						$failed_style_font1 = "";
						$failed_style_font2 = "";
						if ($FullMark > 0)
						{
						    # style for failed @ Mean
						    if ($ResultObj["MEAN"] < $PassMark)
							{
								$failed_style_bg = "style='background:yellow'";
								$failed_style_font1 = "<font color='red'>";
								$failed_style_font2 = "</font>";
							}
						}
	  					$returnRX .= "<td align='center'>".$ResultObj["StudentTotal"]."</td><td align='center'>".$ResultObj["AttemptTotal"]."</td><td align='center'>".$ResultObj["PassTotal"]."</td>" .
  							"<td align='center'>".$PassingRate."</td><td align='center'>".$FullMark."</td>" .
  							"<td align='center' $failed_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center'>".$ResultObj["HIGHEST"]."</td><td align='center'>".$ResultObj["LOWEST"]."</td><td align='center'>".round($ResultObj["SD"],1)."</td>";
  					}
  					else
  					{
  						if (!$ShownSubjectName)
	  					{
						  	$libsubject = new subject($SubjectGroupResult[$SubjectGroupName]["SubjectID"]);
						  	$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
						  	
							$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID]);
  							$returnRX .= "<td>{$SubjectName}</td><td>{$Teachers}</td>";
  							
  							$ShownSubjectName = true;
	  					}
  						for ($i_empty=0; $i_empty<9; $i_empty++)
  						{
  							$returnRX .= "<td align='center'>".$SymbolEmpty."</td>";
  						}
  					}
	  				#### Total END ####
		  		}
		  		$returnRX .= "</tr>";
	  		}
  			
  			$returnRX .= "</tbody>";
  			$returnRX .= "</table>";
  			$returnRX .= "<br />
				<span class=\"tabletextremark\">
					{$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] }
					</br> 
					{$ec_iPortfolio['scoreHighlightRemarks']}
				</span>"; 
  			$returnRX .= "</div>";
	  		
	  		return $returnRX;
	  }
	  
	  function GetSubjectStatsAllYear($SubjectID, $ResultType=array(), $filterYearId=array(), $filterYearClassId=array(), $filterSubjectGroupId=array())
	  {
	  		global $eclass_db, $intranet_db, $Lang, $iPort, $intranet_session_language, $ec_iPortfolio;
		  	$SymbolEmpty = $Lang['General']['EmptySymbol'];
		  	$currentAcademicYearID = Get_Current_Academic_Year_ID();
		  	
		  	
// 	  		######## Group Subject START ########
// 			$rs = $this->getReleatedSubjectID();
// 			$SubjectIdArr = array($SubjectID);
// 			$SubjectIdArr = array_merge($SubjectIdArr, (array)$rs[$SubjectID]);
// 			$SubjectIdList = implode("','", $SubjectIdArr);
// 	  		######## Group Subject END ########
	  		######### Get releated subject START #########
	  		$_subjectIds = $this->getReleatedSubjectID();
	  		$subjectIdArr = array($SubjectID);
	  		foreach ($subjectIdArr as $_subjectIdArr){
	  			$subID = explode('_',$_subjectIdArr);
	  			$subjectIdArr[] = $subID[0];
	  		}
// 	  		foreach($_subjectIds as $parentSubj => $_subjIdArr){
// 	  			if(in_array($SubjectID, $_subjIdArr) || $parentSubj == $SubjectID){
// 	  				$subjectIdArr = $_subjIdArr;
// 	  				$subjectIdArr[] = $parentSubj;
// 	  				break;
// 	  			}
// 	  		}
	  		$SubjectIdList = implode("','", $subjectIdArr);
	  		######### Get releated subject END #########
			
			#### Get full marks, pass marks START ####
			$SubectFullMarks = array();
			$SubectPassMarks = array();
			$sql = "SELECT DISTINCT 
				AcademicYearID,
				YearID, 
				FullMarkInt, 
				PassMarkInt, 
				SubjectID 
			FROM 
				{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK
			WHERE 
				SubjectID IN ('{$SubjectIdList}')";
			$SubectFullMarkArr = $this->returnResultSet($sql);
			for ($i=0; $i<sizeof($SubectFullMarkArr); $i++)
			{
				$SubectFullMarks[$SubectFullMarkArr[$i]["AcademicYearID"]][$SubectFullMarkArr[$i]["YearID"]][$SubectFullMarkArr[$i]["SubjectID"]] = $SubectFullMarkArr[$i]["FullMarkInt"];
				$passMark = ($SubectFullMarkArr[$i]["PassMarkInt"])? $SubectFullMarkArr[$i]["PassMarkInt"] : ((int)$SubectFullMarkArr[$i]["FullMarkInt"]) / 2;
				$SubectPassMarks[$SubectFullMarkArr[$i]["AcademicYearID"]][$SubectFullMarkArr[$i]["YearID"]][$SubectFullMarkArr[$i]["SubjectID"]] = $passMark;
			}
			#### Get full marks, pass marks END ####
			
			
			######## Get Data START ########
			#### Get all TermAssessment Header START ####
	  		$lib_academic_year = new academic_year($AcademicYearID);
	  		$TermArr = ($lib_academic_year->Get_Term_List());
	  		$TermArr[0] = $ec_iPortfolio['overall_result'];
	  		
			$sql = "SELECT DISTINCT 
				TermAssessment
			FROM 
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
			WHERE 
				SubjectID IN ('{$SubjectIdList}') 
			/*AND
				TermAssessment IS NOT NULL*/
			ORDER BY 
				TermAssessment";
		  	$rs = $this->returnVector($sql);
		  	
		  	$termAssessmentArr = array();
		  	$lastTa = $rs[0];
		  	foreach($rs as $r){
		  		if($lastTa != $r){
		  			$termAssessmentArr[] = substr($lastTa, 0, 2); // Get the term
		  		}
		  		$termAssessmentArr[] = $r;
		  		$lastTa = $r;
		  	}
	  		if($lastTa != ''){
	  			$termAssessmentArr[] = substr($lastTa, 0, 2); // Get the term
	  			$termAssessmentArr[] = $ec_iPortfolio['overall_result'];
	  		}
			#### Get all TermAssessment Header END ####
			
	  		
	  		#### Get all YearTerm-TermAssessment START ####
	  		$sql = "SELECT
	  			DISTINCT
  				YearTermID,
	  			TermAssessment
	  		FROM
	  			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
	  		WHERE
	  			SubjectID IN ('{$SubjectIdList}')
	  		/* AND
	  			TermAssessment IS NOT NULL */";
  			$rs = $this->returnResultSet($sql);
  			$rs = BuildMultiKeyAssoc($rs, array('YearTermID') , array('TermAssessment'), $SingleValue=1);
  			
  			$yearTermTaArr = array();
  			foreach ($rs as $YearTermID => $TermAssessment){
  				$yearTermTaArr[$YearTermID] = substr($TermAssessment, 0, 2);
  			}
	  		#### Get all YearTerm-TermAssessment END ####
	  		
	  		
	  		#### Get all Academic Year Name START ####
	  		$sql = "SELECT 
		  		AcademicYearID, 
		  		YearNameB5, 
		  		YearNameEN 
	  		FROM 
	  			{$intranet_db}.ACADEMIC_YEAR 
	  		ORDER BY 
	  			Sequence";
	  		$rs = $this->returnResultSet($sql);

	  		$_nameField = Get_Lang_Selection('YearNameB5','YearNameEN');
	  		$academicYearNameArr = BuildMultiKeyAssoc($rs, array('AcademicYearID') , array($_nameField), $SingleValue=1);
	  		#### Get all Academic Year Name END ####
			
	  		
	  		#### Get all Year Name START ####
	  		$sql = "SELECT 
	  			YearID, 
	  			YearName 
	  		FROM 
	  			{$intranet_db}.YEAR 
	  		ORDER BY 
	  			Sequence";
	  		$rs = $this->returnResultSet($sql);
	  		
	  		$yearNameArr = BuildMultiKeyAssoc($rs, array('YearID') , array('YearName'), $SingleValue=1);
	  		#### Get all Year Name END ####
	  		
	  		
	  		#### Get all Class Name START ####
	  		$sql = "SELECT DISTINCT
		  		YC.ClassTitleB5,
		  		YC.ClassTitleEN
	  		FROM
	  			{$intranet_db}.YEAR_CLASS YC
  			INNER JOIN 
  				{$intranet_db}.YEAR Y
  			ON 
  				YC.YearID = Y.YearID
  			WHERE
	  			AcademicYearID='{$currentAcademicYearID}'
	  		ORDER BY 
	  			Y.Sequence, YC.Sequence";
  			$rs = $this->returnResultSet($sql);
  			$currentClassArr = array();
  			
	  		foreach($rs as $r){
		  		$_nameField = Get_Lang_Selection($r['ClassTitleB5'],$r['ClassTitleEN']);
	  			$currentClassArr[$_nameField] = 1;
	  		}
	  		$currentClassArr = array_keys($currentClassArr);
	  		
	  		$sql = "SELECT 
	  			YC.*
	  		FROM 
	  			{$intranet_db}.YEAR_CLASS YC
  			INNER JOIN 
  				{$intranet_db}.ACADEMIC_YEAR AY
  			ON
  				YC.AcademicYearID = AY.AcademicYearID
	  		ORDER BY 
	  			AcademicYearID, Sequence"; // Get all classes, current year first
	  		$rs = $this->returnResultSet($sql);
	  		
	  		$yearClassNameArr = array();
	  		foreach($rs as $r){
		  		$_nameField = Get_Lang_Selection($r['ClassTitleB5'],$r['ClassTitleEN']);
	  			if(in_array($_nameField, $currentClassArr)){
	  				$yearClassNameArr[ $_nameField ][] = $r;
	  			}
	  		}
	  		#### Get all Class Name END ####
	  		
	  		
	  		#### Get all result data START ####
	  		$filterYearSql = '';
	  		if($filterYearId){
	  			$filterYearIdList = implode("','", $filterYearId);
	  			$filterYearSql = " AND ClassLevelID IN ('{$filterYearIdList}')";
	  		}
	  		$sql = "SELECT 
	  			* 
	  		FROM 
	  			{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN 
	  		WHERE 
	  			SubjectID IN ('{$SubjectIdList}') 
	  		AND 
	  			SubjectComponentID=0 
	  		AND 
	  			IsMain=0 
	  			{$filterYearSql}
	  		ORDER BY 
	 	 		AcademicYearID, YearTermID, ClassLevelID, YearClassID, TermAssessment ";
	  		$rs = $this->returnResultSet($sql);
	  		
	  		$rawData = array();
	  		foreach($rs as $r){
	  			$yearID = $r['ClassLevelID'];
	  			$academicYearId = $r['AcademicYearID'];
	  			$yearClassID = $r['YearClassID'];
	  			if($r['TermAssessment']){
	  				$termAssessment = $r['TermAssessment'];
	  			}else if($r['YearTermID']){
	  				$termAssessment = $yearTermTaArr[$r['YearTermID']];
	  			}else{
	  				$termAssessment = $ec_iPortfolio['overall_result'];
	  			}
	  			$rawData[$yearID][$yearClassID][$academicYearId][$termAssessment] = $r;
	  		}
	  		#### Get all result data END ####
			######## Get Data END ########
			
			######## Group Data START ########
			$dataArr = array();
			
			#### Form Data START ####
		  	if(in_array('form',$ResultType)){
				foreach($yearNameArr as $yearID=>$yearName){
					foreach($academicYearNameArr as $academicYearId => $academicYearName){
						foreach($termAssessmentArr as $termAssessment){
							$d = $rawData[$yearID][0][$academicYearId][$termAssessment];
							if(!$d){
								continue;
							}
							
							$data = array();
							$data['StudentTotal'] = $d['StudentTotal'];
							$data['AttemptTotal'] = $d['AttemptTotal'];
							$data['PassTotal'] = $d['PassTotal'];
							$data['PassRate'] = ($d['AttemptTotal'])? $d['PassTotal'] * 100 / $d['AttemptTotal']:null;
							if($data['PassRate'] !== null){
								$data['PassRate'] = round($data['PassRate'], 1);
							}
							$data['FullMark'] = $SubectFullMarks[$academicYearId][$yearID][$d['SubjectID']];
							if(!$data['FullMark']){
								$data['FullMark'] = 100;
							}
							$data['PassMark'] = $SubectPassMarks[$academicYearId][$yearID][$d['SubjectID']];
							if(!$data['PassMark']){
								$data['PassMark'] = round($data['FullMark'] * 0.5, 1);
							}
							$data['Mean'] = number_format($d['MEAN'], 1);
							if($data['Mean'] < $data['PassMark']){
								$data['MeanLessThenPass'] = true;
							}
							$data['HighestMark'] = $d['HighestMark'];
							$data['LowestMark'] = $d['LowestMark'];
							$data['SD'] = $d['SD'];
	
							$dataArr[$yearName][$ec_iPortfolio['form']][$academicYearName][$termAssessment] = $data;
						}
					}
				}
		  	}
			#### Form Data END ####
			
			
			#### Class Data START ####
		  	if(in_array('class',$ResultType)){
				$_dataArr = array();
				foreach($currentClassArr as $className){
					foreach ($yearClassNameArr[$className] as $yearClass){
                        // [EJ DM#1260-1261] [Class Teacher] SKIP > not subject panel + not current class teacher
                        if($filterYearId && $filterYearClassId && isset($filterYearClassId[$yearClass['YearID']])){
                            if(!in_array($yearClass['YearClassID'], (array)$filterYearClassId[$yearClass['YearID']])){
                                continue;
                            }
                        }

						foreach($termAssessmentArr as $termAssessment){
							$yearID = $yearClass['YearID'];
							$yearClassID = $yearClass['YearClassID'];
							$academicYearId = $yearClass['AcademicYearID'];
							$academicYearName = $academicYearNameArr[$academicYearId];
							$d = $rawData[$yearID][$yearClassID][$academicYearId][$termAssessment];
							if(!$d){
								continue;
							}
	
							$data = array();
							$data['StudentTotal'] = $d['StudentTotal'];
							$data['AttemptTotal'] = $d['AttemptTotal'];
							$data['PassTotal'] = $d['PassTotal'];
							$data['PassRate'] = ($d['AttemptTotal'])? $d['PassTotal'] * 100 / $d['AttemptTotal']:null;
							if($data['PassRate'] !== null){
								$data['PassRate'] = round($data['PassRate'], 1);
							}
							$data['FullMark'] = $SubectFullMarks[$academicYearId][$yearID][$d['SubjectID']];
							if(!$data['FullMark']){
								$data['FullMark'] = 100;
							}
							$data['PassMark'] = $SubectPassMarks[$academicYearId][$yearID][$d['SubjectID']];
							if(!$data['PassMark']){
								$data['PassMark'] = round($data['FullMark'] * 0.5, 1);
							}
							$data['Mean'] = number_format($d['MEAN'], 1);
							if($data['Mean'] < $data['PassMark']){
								$data['MeanLessThenPass'] = true;
							}
							$data['HighestMark'] = $d['HighestMark'];
							$data['LowestMark'] = $d['LowestMark'];
							$data['SD'] = $d['SD'];
	
							$_dataArr[$className][$ec_iPortfolio['class']][$academicYearName][$termAssessment] = $data;
						}
					}
				}

				## Order by academic year desc START ##
				foreach($currentClassArr as $className){
					foreach($academicYearNameArr as $academicYearId => $academicYearName){
						$d = $_dataArr[$className][$ec_iPortfolio['class']][$academicYearName];
						if(!$d){
							continue;
						}
						$dataArr[$className][$ec_iPortfolio['class']][$academicYearName] = $d;
					}
				}
				## Order by academic year desc END ##
		  	}
			#### Class Data END ####
			######## Group Data END ########
			
			######## Display Data START########
	  		@ob_start();
			?>
<div class="chart_tables">
	<table class="common_table_list_v30 view_table_list_v30" id='resultTable'>
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<?php foreach($termAssessmentArr as $header){ ?>
				<th colspan="9"><?=$header?></th>
				<?php } ?>
			</tr>
			<tr>
				<th nowrap><?=$iPort["report_col"]["form_class_group"]?></th>
				<th nowrap><?=$iPort["report_col"]["type"]?></th>
				<th nowrap><?=$Lang['General']['SchoolYear']?></th>

				<?php foreach($termAssessmentArr as $header){ ?>
				<th nowrap><?=$iPort["report_col"]["total_student"]?></th>
				<th nowrap><?=$iPort["report_col"]["total_attempt"]?></th>
				<th nowrap><?=$iPort["report_col"]["total_pass"]?></th>
				<th nowrap><?=$iPort["report_col"]["passing_rate"]?></th>
				<th nowrap><?=$iPort["report_col"]["full_mark"]?></th>
				<th nowrap><?=$iPort["report_col"]["average"]?></th>
				<th nowrap><?=$iPort["report_col"]["highest_mark"]?></th>
				<th nowrap><?=$iPort["report_col"]["lowest_mark"]?></th>
				<th nowrap><?=$iPort["report_col"]["SD"]?></th>
				<?php } ?>
			</tr>
		</thead>
		<tbody>
			<?php 
			$lastFormClass = '';
			foreach($dataArr as $yearName=>$d1){
				foreach ($d1 as $type=>$d2){
					foreach ($d2 as $academicYearName=>$data){
						if($type == $ec_iPortfolio['form']){
							$tdColor = 'background-color:#FFE7B2;';
						}else if($type == $ec_iPortfolio['class']){
							$tdColor = 'background-color:#DAEEFF;';
						}else{
							$tdColor = '';
						}
			?>
			<tr>
				<td style="<?=$tdColor ?>" nowrap><?=($yearName == $lastFormClass)?'&nbsp;':$yearName?></td>
				<td style="<?=$tdColor ?>" nowrap><?=($yearName == $lastFormClass)?'&nbsp;':$type?></td>
				<td style="<?=$tdColor ?>" nowrap><?=$academicYearName?></td>
				
				<?php 
				foreach($termAssessmentArr as $header){ 
					$d = $data[$header];
					if($d){
				?>
				<td align="center" style="<?=$tdColor ?>"><?=$d['StudentTotal']?>&nbsp;</td>
				<td align="center" style="<?=$tdColor ?>"><?=$d['AttemptTotal']?>&nbsp;</td>
				<td align="center" style="<?=$tdColor ?>"><?=$d['PassTotal']?>&nbsp;</td>
				<td align="center" style="<?=$tdColor ?>"><?=($d['PassRate'] !== null)? $d['PassRate'].'%' : '' ?>&nbsp;</td>
				<td align="center" style="<?=$tdColor ?>"><?=$d['FullMark']?>&nbsp;</td>
				<td align="center" style="<?=($d['MeanLessThenPass'])? 'background:yellow' : $tdColor ?>">
					<?php 
						if($d['MeanLessThenPass']){
							echo '<font color="red">' . $d['Mean'] . '</font>';
						}else{
							echo $d['Mean'];
						}
					?>&nbsp;
				</td>
				<td align="center" style="<?=$tdColor ?>"><?=$d['HighestMark']?>&nbsp;</td>
				<td align="center" style="<?=$tdColor ?>"><?=$d['LowestMark']?>&nbsp;</td>
				<td align="center" style="<?=$tdColor ?>"><?=$d['SD']?>&nbsp;</td>
				<?php 
					}else{ 
				?>
				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
				<?php 
					}
				?>
				<?php } ?>
			</tr>
			<?php 	
				$lastFormClass = $yearName;
					}
				}
			}
			?>
		</tbody>
	</table>
</div>
<script>$(function() {$('#resultTable').floatHeader();});</script>
	  		<?php 
	  		$returnRX = ob_get_clean();
			######## Display Data END ########
			return $returnRX;
	  }
	  function GetSubjectStatsSelectYear($SubjectID, $ResultType=array(), $filterYearId=array(), $selectYearId=array())
	  {
	  	global $eclass_db, $intranet_db, $Lang, $iPort, $intranet_session_language, $ec_iPortfolio;
	  	$SymbolEmpty = $Lang['General']['EmptySymbol'] ;
	  	$currentAcademicYearID = Get_Current_Academic_Year_ID();
	  	 
	  	 
	  	// 	  		######## Group Subject START ########
	  	// 			$rs = $this->getReleatedSubjectID();
	  	// 			$SubjectIdArr = array($SubjectID);
	  	// 			$SubjectIdArr = array_merge($SubjectIdArr, (array)$rs[$SubjectID]);
	  	// 			$SubjectIdList = implode("','", $SubjectIdArr);
	  	// 	  		######## Group Subject END ########
	  	######### Get releated subject START #########
	  	$_subjectIds = $this->getReleatedSubjectID();
	  	$subjectIdArr = array($SubjectID);
	  	foreach($_subjectIds as $parentSubj => $_subjIdArr){
	  		if(in_array($SubjectID, $_subjIdArr) || $parentSubj == $SubjectID){
	  			$subjectIdArr = $_subjIdArr;
	  			$subjectIdArr[] = $parentSubj;
	  			break;
	  		}
	  	}
	  	$SubjectIdList = implode("','", $subjectIdArr);
	  	######### Get releated subject END #########
	  	$selectYearId = implode("','", $selectYearId);
	  	 
	  	#### Get full marks, pass marks START ####
	  	$SubectFullMarks = array();
	  	$SubectPassMarks = array();
	  	$sql = "SELECT DISTINCT
	  	AcademicYearID,
	  	YearID,
	  	FullMarkInt,
	  	PassMarkInt,
	  	SubjectID
	  	FROM
	  	{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK
	  	WHERE
	  	SubjectID IN ('{$SubjectIdList}') AND AcademicYearID IN ('{$selectYearId}')";
	  	$SubectFullMarkArr = $this->returnResultSet($sql);
	  	for ($i=0; $i<sizeof($SubectFullMarkArr); $i++)
	  	{
	  		$SubectFullMarks[$SubectFullMarkArr[$i]["AcademicYearID"]][$SubectFullMarkArr[$i]["YearID"]][$SubectFullMarkArr[$i]["SubjectID"]] = $SubectFullMarkArr[$i]["FullMarkInt"];
	  		$passMark = ($SubectFullMarkArr[$i]["PassMarkInt"])? $SubectFullMarkArr[$i]["PassMarkInt"] : ((int)$SubectFullMarkArr[$i]["FullMarkInt"]) / 2;
	  		$SubectPassMarks[$SubectFullMarkArr[$i]["AcademicYearID"]][$SubectFullMarkArr[$i]["YearID"]][$SubectFullMarkArr[$i]["SubjectID"]] = $passMark;
	  	}
	  	#### Get full marks, pass marks END ####
	  	 
	  	 
	  	######## Get Data START ########
	  	#### Get all TermAssessment Header START ####
	  	$lib_academic_year = new academic_year($AcademicYearID);
	  	$TermArr = ($lib_academic_year->Get_Term_List());
	  	$TermArr[0] = $ec_iPortfolio['overall_result'];
	  	 
	  	$sql = "SELECT DISTINCT
	  	TermAssessment
	  	FROM
	  	{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
	  	WHERE
	  	SubjectID IN ('{$SubjectIdList}')
	  	AND
	  	TermAssessment IS NOT NULL
	  	ORDER BY
	  	TermAssessment";
	  	$rs = $this->returnVector($sql);
	  
	  	$termAssessmentArr = array();
	  	$lastTa = $rs[0];
	  	foreach($rs as $r){
	  		if($lastTa != $r){
	  			$termAssessmentArr[] = substr($lastTa, 0, 2); // Get the term
	  		}
	  		$termAssessmentArr[] = $r;
	  		$lastTa = $r;
	  	}
	  	if($lastTa != ''){
	  		$termAssessmentArr[] = substr($lastTa, 0, 2); // Get the term
	  		$termAssessmentArr[] = $ec_iPortfolio['overall_result'];
	  	}else{
	  		$termAssessmentArr[] = $ec_iPortfolio['overall_result'];
	  	}
	  	#### Get all TermAssessment Header END ####
	  	 
	  	 
	  	#### Get all YearTerm-TermAssessment START ####
	  	$sql = "SELECT
	  	YearTermID,
	  	TermAssessment
	  	FROM
	  	{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
	  	WHERE
	  	SubjectID IN ('{$SubjectIdList}')
	  	AND
	  	TermAssessment IS NOT NULL";
	  	$rs = $this->returnResultSet($sql);
	  	$rs = BuildMultiKeyAssoc($rs, array('YearTermID') , array('TermAssessment'), $SingleValue=1);
	  	 
	  	$yearTermTaArr = array();
	  	foreach ($rs as $YearTermID => $TermAssessment){
	  		$yearTermTaArr[$YearTermID] = substr($TermAssessment, 0, 2);
	  	}
	  	#### Get all YearTerm-TermAssessment END ####
	  	 
	  	#### Get all Academic Year Name START ####
	  	$sql = "SELECT
	  	AcademicYearID,
	  	YearNameB5,
	  	YearNameEN
	  	FROM
	  	{$intranet_db}.ACADEMIC_YEAR
	  	WHERE
	  	AcademicYearID IN ('{$selectYearId}')
	  	ORDER BY
	  	Sequence";
	  	$rs = $this->returnResultSet($sql);
	  	 
	  	$_nameField = Get_Lang_Selection('YearNameB5','YearNameEN');
	  	$academicYearNameArr = BuildMultiKeyAssoc($rs, array('AcademicYearID') , array($_nameField), $SingleValue=1);
	  	#### Get all Academic Year Name END ####
	  	 
	  	 
	  	#### Get all Year Name START ####
	  	$sql = "SELECT
	  	YearID,
	  	YearName
	  	FROM
	  	{$intranet_db}.YEAR
	  	ORDER BY
	  	Sequence";
	  	$rs = $this->returnResultSet($sql);
	  	 
	  	$yearNameArr = BuildMultiKeyAssoc($rs, array('YearID') , array('YearName'), $SingleValue=1);
	  	#### Get all Year Name END ####
	  	 
	  	 
	  	#### Get all Class Name START ####
	  	$sql = "SELECT DISTINCT
	  	YC.ClassTitleB5,
	  	YC.ClassTitleEN
	  	FROM
	  	{$intranet_db}.YEAR_CLASS YC
	  	INNER JOIN
	  	{$intranet_db}.YEAR Y
	  	ON
	  	YC.YearID = Y.YearID
	  	WHERE
	  	AcademicYearID='{$currentAcademicYearID}'
	  	ORDER BY
	  	Y.Sequence, YC.Sequence";
	  	$rs = $this->returnResultSet($sql);
	  	$currentClassArr = array();
	  	 
	  	foreach($rs as $r){
	  		$_nameField = Get_Lang_Selection($r['ClassTitleB5'],$r['ClassTitleEN']);
	  		$currentClassArr[$_nameField] = 1;
	  	}
	  	$currentClassArr = array_keys($currentClassArr);
	  	 
	  	$sql = "SELECT
	  	YC.*
	  	FROM
	  	{$intranet_db}.YEAR_CLASS YC
	  	INNER JOIN
	  	{$intranet_db}.ACADEMIC_YEAR AY
	  	ON
	  	YC.AcademicYearID = AY.AcademicYearID
	  	ORDER BY
	  	AcademicYearID, Sequence"; // Get all classes, current year first
	  	$rs = $this->returnResultSet($sql);
	  	 
	  	$yearClassNameArr = array();
	  	foreach($rs as $r){
	  		$_nameField = Get_Lang_Selection($r['ClassTitleB5'],$r['ClassTitleEN']);
	  		if(in_array($_nameField, $currentClassArr)){
	  			$yearClassNameArr[ $_nameField ][] = $r;
	  		}
	  	}
	  	#### Get all Class Name END ####
	  	 
	  	 
	  	#### Get all result data START ####
	  	$filterYearSql = '';
	  	if($filterYearId){
	  		$filterYearIdList = implode("','", $filterYearId);
	  		$filterYearSql = " AND ClassLevelID IN ('{$filterYearIdList}')";
	  	}
	  	$sql = "SELECT
	  	*
	  	FROM
	  	{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN
	  	WHERE
	  	SubjectID IN ('{$SubjectIdList}')
	  	AND
	  	SubjectComponentID=0
	  	AND
	  	IsMain=0
	  	{$filterYearSql}
	  	ORDER BY
	  	AcademicYearID, YearTermID, ClassLevelID, YearClassID, TermAssessment ";
	  	$rs = $this->returnResultSet($sql);
	  	$rawData = array();
	  	foreach($rs as $r){
	  		$yearID = $r['ClassLevelID'];
	  		$academicYearId = $r['AcademicYearID'];
	  		$yearClassID = $r['YearClassID'];
	  		if($r['TermAssessment']){
	  			$termAssessment = $r['TermAssessment'];
	  		}else if($r['YearTermID']){
	  			$termAssessment = $yearTermTaArr[$r['YearTermID']];
	  		}else{
	  			$termAssessment = $ec_iPortfolio['overall_result'];
	  		}
	  		$rawData[$yearID][$yearClassID][$academicYearId][$termAssessment] = $r;
	  	}
	  	#### Get all result data END ####
	  	######## Get Data END ########
	  	 
	  	######## Group Data START ########
	  	$dataArr = array();
	  	 
	  	#### Form Data START ####
	  	if(in_array('form',$ResultType)){
	  		foreach($yearNameArr as $yearID=>$yearName){
	  			foreach($academicYearNameArr as $academicYearId => $academicYearName){
	  				foreach($termAssessmentArr as $termAssessment){
	  					$d = $rawData[$yearID][0][$academicYearId][$termAssessment];
	  					if(!$d){
	  						continue;
	  					}
	  						
	  					$data = array();
	  					$data['StudentTotal'] = $d['StudentTotal'];
	  					$data['AttemptTotal'] = $d['AttemptTotal'];
	  					$data['PassTotal'] = $d['PassTotal'];
	  					$data['PassRate'] = ($d['AttemptTotal'])? $d['PassTotal'] * 100 / $d['AttemptTotal']:null;
	  					if($data['PassRate'] !== null){
	  						$data['PassRate'] = round($data['PassRate'], 1);
	  					}
	  					$data['FullMark'] = $SubectFullMarks[$academicYearId][$yearID][$d['SubjectID']];
	  					if(!$data['FullMark']){
	  						$data['FullMark'] = 100;
	  					}
	  					$data['PassMark'] = $SubectPassMarks[$academicYearId][$yearID][$d['SubjectID']];
	  					if(!$data['PassMark']){
	  						$data['PassMark'] = round($data['FullMark'] * 0.5, 1);
	  					}
	  					$data['Mean'] = number_format($d['MEAN'], 1);
	  					if($data['Mean'] < $data['PassMark']){
	  						$data['MeanLessThenPass'] = true;
	  					}
	  					$data['HighestMark'] = $d['HighestMark'];
	  					$data['LowestMark'] = $d['LowestMark'];
	  					$data['SD'] = $d['SD'];
	  					 
	  					$dataArr[$yearName][$ec_iPortfolio['form']][$academicYearName][$termAssessment] = $data;
	  				}
	  			}
	  		}
	  	}
	  	#### Form Data END ####
	  	 
	  	 
	  	#### Class Data START ####
	  	if(in_array('class',$ResultType)){
	  		$_dataArr = array();
	  		foreach($currentClassArr as $className){
	  			foreach ($yearClassNameArr[$className] as $yearClass){
	  				foreach($termAssessmentArr as $termAssessment){
	  					$yearID = $yearClass['YearID'];
	  					$yearClassID = $yearClass['YearClassID'];
	  					$academicYearId = $yearClass['AcademicYearID'];
	  					$academicYearName = $academicYearNameArr[$academicYearId];
	  					$d = $rawData[$yearID][$yearClassID][$academicYearId][$termAssessment];
	  					if(!$d){
	  						continue;
	  					}
	  					 
	  					$data = array();
	  					$data['StudentTotal'] = $d['StudentTotal'];
	  					$data['AttemptTotal'] = $d['AttemptTotal'];
	  					$data['PassTotal'] = $d['PassTotal'];
	  					$data['PassRate'] = ($d['AttemptTotal'])? $d['PassTotal'] * 100 / $d['AttemptTotal']:null;
	  					if($data['PassRate'] !== null){
	  						$data['PassRate'] = round($data['PassRate'], 1);
	  					}
	  					$data['FullMark'] = $SubectFullMarks[$academicYearId][$yearID][$d['SubjectID']];
	  					if(!$data['FullMark']){
	  						$data['FullMark'] = 100;
	  					}
	  					$data['PassMark'] = $SubectPassMarks[$academicYearId][$yearID][$d['SubjectID']];
	  					if(!$data['PassMark']){
	  						$data['PassMark'] = round($data['FullMark'] * 0.5, 1);
	  					}
	  					$data['Mean'] = number_format($d['MEAN'], 1);
	  					if($data['Mean'] < $data['PassMark']){
	  						$data['MeanLessThenPass'] = true;
	  					}
	  					$data['HighestMark'] = $d['HighestMark'];
	  					$data['LowestMark'] = $d['LowestMark'];
	  					$data['SD'] = $d['SD'];
	  					 
	  					$_dataArr[$className][$ec_iPortfolio['class']][$academicYearName][$termAssessment] = $data;
	  				}
	  			}
	  		}
	  		 
	  		## Order by academic year desc START ##
	  		foreach($currentClassArr as $className){
	  			foreach($academicYearNameArr as $academicYearId => $academicYearName){
	  				$d = $_dataArr[$className][$ec_iPortfolio['class']][$academicYearName];
	  				if(!$d){
	  					continue;
	  				}
	  				$dataArr[$className][$ec_iPortfolio['class']][$academicYearName] = $d;
	  			}
	  		}
	  		## Order by academic year desc END ##
	  	}
	  	#### Class Data END ####
	  	######## Group Data END ########
	  	 
	  	######## Display Data START########
	  	@ob_start();
	  	?>
	  	  <div class="chart_tables">
	  	  	<table class="common_table_list_v30 view_table_list_v30" id='resultTable'>
	  	  		<thead>
	  	  			<tr>
	  	  				<th>&nbsp;</th>
	  	  				<th>&nbsp;</th>
	  	  				<th>&nbsp;</th>
	  	  				<?php foreach($termAssessmentArr as $header){ ?>
	  	  				<th colspan="9"><?=$header?></th>
	  	  				<?php } ?>
	  	  			</tr>
	  	  			<tr>
	  	  				<th nowrap><?=$iPort["report_col"]["form_class_group"]?></th>
	  	  				<th nowrap><?=$iPort["report_col"]["type"]?></th>
	  	  				<th nowrap><?=$Lang['General']['SchoolYear']?></th>
	  	  
	  	  				<?php foreach($termAssessmentArr as $header){ ?>
	  	  				<th nowrap><?=$iPort["report_col"]["total_student"]?></th>
	  	  				<th nowrap><?=$iPort["report_col"]["total_attempt"]?></th>
	  	  				<th nowrap><?=$iPort["report_col"]["total_pass"]?></th>
	  	  				<th nowrap><?=$iPort["report_col"]["passing_rate"]?></th>
	  	  				<th nowrap><?=$iPort["report_col"]["full_mark"]?></th>
	  	  				<th nowrap><?=$iPort["report_col"]["average"]?></th>
	  	  				<th nowrap><?=$iPort["report_col"]["highest_mark"]?></th>
	  	  				<th nowrap><?=$iPort["report_col"]["lowest_mark"]?></th>
	  	  				<th nowrap><?=$iPort["report_col"]["SD"]?></th>
	  	  				<?php } ?>
	  	  			</tr>
	  	  		</thead>
	  	  		<tbody>
	  	  			<?php 
	  	  			$lastFormClass = '';
	  	  			foreach($dataArr as $yearName=>$d1){
	  	  				foreach ($d1 as $type=>$d2){
	  	  					foreach ($d2 as $academicYearName=>$data){
	  	  						if($type == $ec_iPortfolio['form']){
	  	  							$tdColor = 'background-color:#FFE7B2;';
	  	  						}else if($type == $ec_iPortfolio['class']){
	  	  							$tdColor = 'background-color:#DAEEFF;';
	  	  						}else{
	  	  							$tdColor = '';
	  	  						}
	  	  			?>
	  	  			<tr>
	  	  				<td style="<?=$tdColor ?>" nowrap><?=($yearName == $lastFormClass)?'&nbsp;':$yearName?></td>
	  	  				<td style="<?=$tdColor ?>" nowrap><?=($yearName == $lastFormClass)?'&nbsp;':$type?></td>
	  	  				<td style="<?=$tdColor ?>" nowrap><?=$academicYearName?></td>
	  	  				
	  	  				<?php 
	  	  				foreach($termAssessmentArr as $header){ 
	  	  					$d = $data[$header];
	  	  					if($d){
	  	  				?>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$d['StudentTotal']?>&nbsp;</td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$d['AttemptTotal']?>&nbsp;</td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$d['PassTotal']?>&nbsp;</td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=($d['PassRate'] !== null)? $d['PassRate'].'%' : '' ?>&nbsp;</td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$d['FullMark']?>&nbsp;</td>
	  	  				<td align="center" style="<?=($d['MeanLessThenPass'])? 'background:yellow' : $tdColor ?>">
	  	  					<?php 
	  	  						if($d['MeanLessThenPass']){
	  	  							echo '<font color="red">' . $d['Mean'] . '</font>';
	  	  						}else{
	  	  							echo $d['Mean'];
	  	  						}
	  	  					?>&nbsp;
	  	  				</td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$d['HighestMark']?>&nbsp;</td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$d['LowestMark']?>&nbsp;</td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$d['SD']?>&nbsp;</td>
	  	  				<?php 
	  	  					}else{ 
	  	  				?>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  	  				<?php 
	  	  					}
	  	  				?>
	  	  				<?php } ?>
	  	  			</tr>
	  	  			<?php 	
	  	  				$lastFormClass = $yearName;
	  	  					}
	  	  				}
	  	  			}
	  	  			?>
	  	  		</tbody>
	  	  	</table>
	  	  </div>
	  	  <script>$(function() {$('#resultTable').floatHeader();});</script>
	  	  	  		<?php 
	  	  	  		$returnRX = ob_get_clean();
	  	  			######## Display Data END ########
	  	  			return $returnRX;
	  	  	  }
  	  
	  function GetSubjectStatsSelectYear_new($SubjectID, $ResultType=array(), $filterYearId=array(), $selectYearId=array(), $filterYearClassId=array(), $filterSubjectGroupId=array())
	  {
	  	global $eclass_db, $intranet_db, $Lang, $iPort, $intranet_session_language, $ec_iPortfolio, $sys_custom;
	  	
	  	$SymbolEmpty = $Lang['General']['EmptySymbol'] ;
	  	$currentAcademicYearID = Get_Current_Academic_Year_ID();
        
	  	######### Get releated subject START #########
	  	$temp = explode('-',$SubjectID);
		$Subject_sql_arr = array();
	  	$SubjectID_Arr = explode('_',$SubjectID);
	  	$MainSubjectID = $SubjectID_Arr[0];
	  	$ComSubjectId = $SubjectID_Arr[1];
	  	$cond = " AND SubjectID = '$MainSubjectID' ";
	  	if($ComSubjectId==='0') {
	  		$cond .= " AND SubjectComponentID IS NULL ";
	  		$Subject_sql_arr = array_merge($Subject_sql_arr,(array)$MainSubjectID);
	  		$isCmp = false;
	  	} else {
	  		$cond .= " AND SubjectComponentID = '$ComSubjectId' ";
	  		$Subject_sql_arr = array_merge($Subject_sql_arr,(array)$ComSubjectId);
	  		$isCmp = true;
	  	}
        
	  	######### Get releated subject END #########
	  	$selectYearId = implode("','", $selectYearId);
	  		
	  	#### Get full marks, pass marks START ####
	  	$SubectFullMarks = array();
	  	$SubectPassMarks = array();
	  	
	  	//2017-01-11 Villa
	  	if($isCmp) {
	  	    $SubID = $ComSubjectId;
	  	} else {
	  		$SubID = $MainSubjectID;
	  	}
	  	$sql = "SELECT DISTINCT
				  	AcademicYearID,
				  	YearID,
				  	FullMarkInt,
				  	PassMarkInt,
				  	SubjectID
	  			FROM
	  				{$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK
	  			WHERE
	  				SubjectID = '$SubID' AND AcademicYearID IN ('{$selectYearId}')";
	  	$SubectFullMarkArr = $this->returnResultSet($sql);
	  	for ($i=0; $i<sizeof($SubectFullMarkArr); $i++)
	  	{
	  		$SubectFullMarks[$SubectFullMarkArr[$i]["AcademicYearID"]][$SubectFullMarkArr[$i]["YearID"]][$SubectFullMarkArr[$i]["SubjectID"]] = $SubectFullMarkArr[$i]["FullMarkInt"];
	  		$passMark = ($SubectFullMarkArr[$i]["PassMarkInt"])? $SubectFullMarkArr[$i]["PassMarkInt"] : ((int)$SubectFullMarkArr[$i]["FullMarkInt"]) / 2;
	  		$SubectPassMarks[$SubectFullMarkArr[$i]["AcademicYearID"]][$SubectFullMarkArr[$i]["YearID"]][$SubectFullMarkArr[$i]["SubjectID"]] = $passMark;
	  	}
	  	#### Get full marks, pass marks END ####
	  	
	  	######## Get Data START ########
	  	#### Get all TermAssessment Header START ####
	  	$lib_academic_year = new academic_year($AcademicYearID);
	  	$TermArr = ($lib_academic_year->Get_Term_List());
	  	$TermArr[0] = $ec_iPortfolio['overall_result'];
	  	
	  	// 2017-01-11 Villa
	  	$sql = "SELECT 
	  		YearTermID,
	  		TermAssessment,
	  		AcademicYearID
	  	FROM
	  		{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
	  	WHERE
	  		1
	  		$cond AND
	  		AcademicYearID IN ('{$selectYearId}') AND
	  		Score >0
	  	GROUP BY 
	  		 AcademicYearID, YearTermID, TermAssessment ";
	  	$rs = $this->returnResultSet($sql);

	  	$sql = "SELECT 
					AcademicYearID, YearTermID, TermStart, TermEnd
				FROM 
					ACADEMIC_YEAR_TERM 
				ORDER BY 
					AcademicYearID, TermStart ";
	  	$rs2 = $this->returnResultSet($sql);
	  	foreach($rs2 as $_rs2) {
			$termSortingArr[$_rs2['AcademicYearID']][]= $_rs2['YearTermID'];
	  	}
	  	
	  	$termAssessmentArr = array();
	  	$lastTa = $rs[0];
	  	foreach($rs as $_rs)
	  	{
	  	    if($_rs['TermAssessment']=='') {
	  	        if( $_rs['YearTermID']) {
	  	            foreach ($termSortingArr[$_rs['AcademicYearID']] as $_termSortingArrKEY => $_termSortingArr) {
	  	                if($_rs['YearTermID'] == $_termSortingArr) {
	  						$termAssessmentArr[] = 'T'.($_termSortingArrKEY+1);
	  					}
	  				}
	  	        }
	  	    } else {
	  			$termAssessmentArr[] = $_rs['TermAssessment'];
	  		}
	  	}
	  	asort($termAssessmentArr);
	  	$termAssessmentArr[] = $ec_iPortfolio['overall_result'];
	  	$termAssessmentArr = array_unique($termAssessmentArr);
	  	#### Get all TermAssessment Header END ####
	  	
	  	#### Get all YearTerm-TermAssessment START ####
	  	// 2017-01-11 Villa
	  	$sql = "SELECT
	  				DISTINCT
				  	YearTermID,
				  	Semester,
				  	TermAssessment,
				  	AcademicYearID
	  			FROM
	  				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
	  			WHERE
	  				YearTermID IS NOT NULL 
	  				$cond ";
	  	$rs = $this->returnResultSet($sql);
	  	
	  	$yearTermTaArr = array();
	  	foreach($rs as $_rs) {
	  	    if($_rs['Semester']) {
	  	        foreach ($termSortingArr[$_rs['AcademicYearID']] as $_termSortingArrKEY => $_termSortingArr) {
	  	            if($_rs['YearTermID'] == $_termSortingArr) {
	  					$yearTermTaArr[$_rs['YearTermID']] = 'T'.($_termSortingArrKEY+1);
	  				}
	  			}
	  		}
	  	}
	  	#### Get all YearTerm-TermAssessment END ####
	  	
	  	#### Get all Academic Year Name START ####
	  	$sql = "SELECT
				  	AcademicYearID,
	  				YearNameB5,
	  				YearNameEN
	  			FROM
	  				{$intranet_db}.ACADEMIC_YEAR
	  			WHERE
	  				AcademicYearID IN ('{$selectYearId}')
	  			ORDER BY
	  				Sequence";
	  	$rs = $this->returnResultSet($sql);
	  
	  	$_nameField = Get_Lang_Selection('YearNameB5','YearNameEN');
	  	$academicYearNameArr = BuildMultiKeyAssoc($rs, array('AcademicYearID') , array($_nameField), $SingleValue=1);
	  	#### Get all Academic Year Name END ####
	  	
	  	#### Get all Year Name START ####
	  	$sql = "SELECT
	  				YearID,
	  				YearName
	  			FROM
	  				{$intranet_db}.YEAR
	  			ORDER BY
	  				Sequence";
	  	$rs = $this->returnResultSet($sql);
	  	$yearNameArr = BuildMultiKeyAssoc($rs, array('YearID') , array('YearName'), $SingleValue=1);
	  	#### Get all Year Name END ####
	  	
	  	#### Get all Class Name START ####
	  	$sql = "SELECT DISTINCT
	  				YC.ClassTitleB5,
	  				YC.ClassTitleEN
	  			FROM
	  				{$intranet_db}.YEAR_CLASS YC
	  			INNER JOIN
	  				{$intranet_db}.YEAR Y
	  					ON
	  						YC.YearID = Y.YearID
	  			WHERE
	  				AcademicYearID='{$currentAcademicYearID}'
	  			ORDER BY
	  				Y.Sequence, YC.Sequence";
	  	$rs = $this->returnResultSet($sql);
	  	$currentClassArr = array();
	  	
	  	foreach($rs as $r) {
	  		$_nameField = Get_Lang_Selection($r['ClassTitleB5'],$r['ClassTitleEN']);
	  		$currentClassArr[$_nameField] = 1;
	  	}
	  	$currentClassArr = array_keys($currentClassArr);
	  	#### Get all Class Name END ####
	  	
	  	#### Get all result data START ####
	  	$filterYearSql = '';
	  	if($filterYearId) {
	  		$filterYearIdList = implode("','", $filterYearId);
	  		$filterYearSql = " AND ClassLevelID IN ('{$filterYearIdList}')";
	  	}
	  	
// 	  	2017-01-11 Villa
	  	if(!$isCmp) {
	  	    $cond1 = str_replace("IS NULL", "= '0' ", $cond);
	  	} else {
	  		$cond1 = $cond;
	  	}
	  	$sql = "SELECT
	  				*
	  			FROM
	  				{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN
	  			WHERE
	  				1
	  				$cond1
	  			AND
	  				IsMain=0
	  				{$filterYearSql}
	  			ORDER BY
	  				AcademicYearID, YearTermID, ClassLevelID, YearClassID, TermAssessment ";
	  	$rs = $this->returnResultSet($sql);
	  	
	  	
	  	// modified data to array with the index of $yearID $yearClassID $academicYearId $termAssessment
	  	$rawData = array();
	  	foreach($rs as $r) {
	  		$yearID = $r['ClassLevelID'];
	  		$academicYearId = $r['AcademicYearID'];
	  		$yearClassID = $r['YearClassID'];
	  		if($r['TermAssessment']) {
	  		    $termAssessment = $r['TermAssessment'];
	  		} else if($r['YearTermID']) {
	  			$termAssessment = $yearTermTaArr[$r['YearTermID']];
	  		} else {
	  			$termAssessment = $ec_iPortfolio['overall_result'];
	  		}
	  		$rawData[$yearID][$yearClassID][$academicYearId][$termAssessment] = $r;
	  	}
	  	
	  	// Gat all Class in SD_MEAN
	  	$YearClassID = Get_Array_By_Key($rs, 'YearClassID');
	  	$YearClassID = array_unique($YearClassID);
	  	$YearClassID_sql = implode("','",(array)$YearClassID);
	  	$sql = "SELECT
			  		YC.*
			  	FROM
			  		{$intranet_db}.YEAR_CLASS YC
			  	INNER JOIN
			  		{$intranet_db}.ACADEMIC_YEAR AY ON
					  		YC.AcademicYearID = AY.AcademicYearID
			  	WHERE
			  		YC.YearClassID in ('{$YearClassID_sql}')
			  	ORDER BY
			  		AcademicYearID, Sequence"; // Get all classes having data in SD_MEAN
	  	$rs = $this->returnResultSet($sql);
	  	
	  	$yearClassNameArr = array();
	  	foreach($rs as $r) {
	  		$yearClassNameArr[ $r['YearClassID'] ][] = $r;
	  	}
	  	#### Get all result data END ####
	  	######## Get Data END ########
	  	
	  	######## Group Data START ########
	  	$dataArr = array();
	  	
	  	#### Form Data START ####
	  	if(in_array('form',$ResultType))
	  	{
	  	    foreach($yearNameArr as $yearID => $yearName)
	  	    {
// 	  			debug_pr($yearName);
	  	        foreach($academicYearNameArr as $academicYearId => $academicYearName) {
	  	            foreach($termAssessmentArr as $termAssessment) {
	  					$d = $rawData[$yearID][0][$academicYearId][$termAssessment];
	  					if(!$d) {
	  						continue;
	  					}
	  					
	  					$data = array();
	  					$data['StudentTotal'] = $d['StudentTotal'];
	  					$data['AttemptTotal'] = $d['AttemptTotal'];
	  					$data['PassTotal'] = $d['PassTotal'];
	  					$data['PassRate'] = ($d['AttemptTotal'])? $d['PassTotal'] * 100 / $d['AttemptTotal']:null;
	  					if($data['PassRate'] !== null) {
	  						$data['PassRate'] = round($data['PassRate'], 1);
	  					}
	  					
	  					// [2018-1114-1513-29235] Get correct marks (both normal & component subjects)
	  					$curSubjectID = $d['SubjectComponentID'] > 0? $d['SubjectComponentID'] : $d['SubjectID'];
	  					$data['FullMark'] = $SubectFullMarks[$academicYearId][$yearID][$curSubjectID];
	  					if(!$data['FullMark']) {
	  						$data['FullMark'] = 100;
	  					}
	  					$data['PassMark'] = $SubectPassMarks[$academicYearId][$yearID][$curSubjectID];
	  					if(!$data['PassMark']) {
	  						$data['PassMark'] = round($data['FullMark'] * 0.5, 1);
	  					}
	  					
	  					if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
	  					    if(
	  					        ($termAssessment == 'T1A3' && strpos($yearName, '6') !== false) ||
	  					        ($termAssessment == 'T2A3')
	  					    ){
	  					        $data['FullMark'] = '100';
	  					        $data['PassMark'] = '50';
	  					    }
	  					}
	  					
	  					$data['Mean'] = number_format($d['MEAN'], 1);
	  					if($data['Mean'] < $data['PassMark']){
	  						$data['MeanLessThenPass'] = true;
	  					}
	  					$data['HighestMark'] = $d['HighestMark'];
	  					$data['LowestMark'] = $d['LowestMark'];
	  					$data['SD'] = $d['SD'];
                        
	  					$dataArr[$yearName][$ec_iPortfolio['form']][$academicYearName][$termAssessment] = $data;
	  				}
	  			}
	  		}
	  	}
	  	#### Form Data END ####
	  		
	  	#### Class Data START ####
	  	if(in_array('class',$ResultType))
	  	{
	  	    $_dataArr = array();
	  	    foreach ($YearClassID as $_YearClassID) {
	  	        if($_YearClassID=='0') {
	  				continue;
	  	        }
	  	        foreach ((array)$yearClassNameArr[$_YearClassID] as $yearClass) {
                    // [EJ DM#1260-1261] [Class Teacher] SKIP > not subject panel + not current class teacher
                    if($filterYearId && $filterYearClassId && isset($filterYearClassId[$yearClass['YearID']])) {
                        if(!in_array($yearClass['YearClassID'], (array)$filterYearClassId[$yearClass['YearID']])){
                            continue;
                        }
                    }

	  	            foreach($termAssessmentArr as $termAssessment) {
	  					$yearID = $yearClass['YearID'];
	  					$yearClassID = $yearClass['YearClassID'];
	  					$className = Get_Lang_Selection($yearClass['ClassTitleB5'], $yearClass['ClassTitleEN']);
	  					$academicYearId = $yearClass['AcademicYearID'];
	  					$academicYearName = $academicYearNameArr[$academicYearId];
	  					$d = $rawData[$yearID][$yearClassID][$academicYearId][$termAssessment];
	  					if(!$d) {
	  						continue;
	  					}
	                    
	  					$data = array();
	  					$data['StudentTotal'] = $d['StudentTotal'];
	  					$data['AttemptTotal'] = $d['AttemptTotal'];
	  					$data['PassTotal'] = $d['PassTotal'];
	  					$data['PassRate'] = ($d['AttemptTotal'])? $d['PassTotal'] * 100 / $d['AttemptTotal']:null;
	  					if($data['PassRate'] !== null){
	  						$data['PassRate'] = round($data['PassRate'], 1);
	  					}
	  					
	  					// [2018-1114-1513-29235] Get correct marks (both normal & component subjects)
	  					$curSubjectID = $d['SubjectComponentID'] > 0? $d['SubjectComponentID'] : $d['SubjectID'];
	  					$data['FullMark'] = $SubectFullMarks[$academicYearId][$yearID][$curSubjectID];
	  					if(!$data['FullMark']) {
	  						$data['FullMark'] = 100;
	  					}
	  					$data['PassMark'] = $SubectPassMarks[$academicYearId][$yearID][$curSubjectID];
	  					if(!$data['PassMark']) {
	  						$data['PassMark'] = round($data['FullMark'] * 0.5, 1);
	  					}
	  					
	  					if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
	  					    if(
	  					        ($termAssessment == 'T1A3' && strpos($className, '6') !== false) ||
	  					        ($termAssessment == 'T2A3')
	  					    ){
	  					        $data['FullMark'] = '100';
	  					        $data['PassMark'] = '50';
	  					    }
	  					}
	  					
	  					$data['Mean'] = number_format($d['MEAN'], 1);
	  					if($data['Mean'] < $data['PassMark']){
	  						$data['MeanLessThenPass'] = true;
	  					}
	  					$data['HighestMark'] = $d['HighestMark'];
	  					$data['LowestMark'] = $d['LowestMark'];
	  					$data['SD'] = $d['SD'];
                        
	  					$_dataArr[$className][$ec_iPortfolio['class']][$academicYearName][$termAssessment] = $data;
	  				}
	  			}
	  		}
	  		
	  		## Order by academic year desc START ##
	  		foreach($YearClassID as $_YearClassID) {
	  		    foreach($academicYearNameArr as $academicYearId => $academicYearName) {
	  				$ClassName = Get_Lang_Selection($yearClassNameArr[$_YearClassID][0]['ClassTitleB5'], $yearClassNameArr[$_YearClassID][0]['ClassTitleEN']);
	  				$d = $_dataArr[$ClassName][$ec_iPortfolio['class']][$academicYearName];
	  				if(!$d) {
	  					continue;
	  				}
	  				$dataArr[$ClassName][$ec_iPortfolio['class']][$academicYearName] = $d;
	  			}
	  		}
	  		## Order by academic year desc END ##
	  	}
	  	#### Class Data END ####
	  	######## Group Data END ########
	  	
	  	######## Display Data START########
	  	@ob_start();
	  	?>
	  <div class="chart_tables">
	  	<table class="common_table_list_v30 view_table_list_v30" id='resultTable'>
	  		<thead>
	  			<tr>
	  				<th>&nbsp;</th>
	  				<th>&nbsp;</th>
	  				<th>&nbsp;</th>
	  				<?php foreach($termAssessmentArr as $header){ ?>
	  				<th colspan="9"><?=$header?></th>
	  				<?php } ?>
	  			</tr>
	  			<tr>
	  				<th nowrap><?=$iPort["report_col"]["form_class_group"]?></th>
	  				<th nowrap><?=$iPort["report_col"]["type"]?></th>
	  				<th nowrap><?=$Lang['General']['SchoolYear']?></th>
	  
	  				<?php foreach($termAssessmentArr as $header){ ?>
	  				<th nowrap><?=$iPort["report_col"]["total_student"]?></th>
	  				<th nowrap><?=$iPort["report_col"]["total_attempt"]?></th>
	  				<th nowrap><?=$iPort["report_col"]["total_pass"]?></th>
	  				<th nowrap><?=$iPort["report_col"]["passing_rate"]?></th>
	  				<th nowrap><?=$iPort["report_col"]["full_mark"]?></th>
	  				<th nowrap><?=$iPort["report_col"]["average"]?></th>
	  				<th nowrap><?=$iPort["report_col"]["highest_mark"]?></th>
	  				<th nowrap><?=$iPort["report_col"]["lowest_mark"]?></th>
	  				<th nowrap><?=$iPort["report_col"]["SD"]?></th>
	  				<?php } ?>
	  			</tr>
	  		</thead>
	  		<tbody>
	  			<?php 
	  			$lastFormClass = '';
	  			foreach($dataArr as $yearName=>$d1){
	  				foreach ($d1 as $type=>$d2){
	  					foreach ($d2 as $academicYearName=>$data){
	  						if($type == $ec_iPortfolio['form']){
	  							$tdColor = 'background-color:#FFE7B2;';
	  						}else if($type == $ec_iPortfolio['class']){
	  							$tdColor = 'background-color:#DAEEFF;';
	  						}else{
	  							$tdColor = '';
	  						}
	  			?>
	  			<tr>
	  				<td style="<?=$tdColor ?>" nowrap><?=($yearName == $lastFormClass)?'&nbsp;':$yearName?></td>
	  				<td style="<?=$tdColor ?>" nowrap><?=($yearName == $lastFormClass)?'&nbsp;':$type?></td>
	  				<td style="<?=$tdColor ?>" nowrap><?=$academicYearName?></td>
	  				
	  				<?php 
	  				foreach($termAssessmentArr as $header){ 
	  					$d = $data[$header];
	  					if($d){
	  				?>
	  				<td align="center" style="<?=$tdColor ?>"><?=$d['StudentTotal']?>&nbsp;</td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$d['AttemptTotal']?>&nbsp;</td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$d['PassTotal']?>&nbsp;</td>
	  				<td align="center" style="<?=$tdColor ?>"><?=($d['PassRate'] !== null)? $d['PassRate'].'%' : '' ?>&nbsp;</td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$d['FullMark']?>&nbsp;</td>
	  				<td align="center" style="<?=($d['MeanLessThenPass'])? 'background:yellow' : $tdColor ?>">
	  					<?php 
	  						if($d['MeanLessThenPass']){
	  							echo '<font color="red">' . $d['Mean'] . '</font>';
	  						}else{
	  							echo $d['Mean'];
	  						}
	  					?>&nbsp;
	  				</td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$d['HighestMark']?>&nbsp;</td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$d['LowestMark']?>&nbsp;</td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$d['SD']?>&nbsp;</td>
	  				<?php 
	  					}else{ 
	  				?>
	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  				<td align="center" style="<?=$tdColor ?>"><?=$SymbolEmpty?></td>
	  				<?php 
	  					}
	  				?>
	  				<?php } ?>
	  			</tr>
	  			<?php 	
	  				$lastFormClass = $yearName;
	  					}
	  				}
	  			}
	  			?>
	  		</tbody>
	  	</table>
	  	<?php 
	  		$returnRX = 
	  		"<br />
				<span class=\"tabletextremark\">
					{$ec_iPortfolio['scoreHighlightRemarks']}
				</span>";
  			echo $returnRX;
  		?>
	  </div>
	  <script>$(function() {$('#resultTable').floatHeader();});</script>
	  	  		<?php 
  	  		$returnRX = ob_get_clean();
  			######## Display Data END ########
  			return $returnRX;
  	  }
  	  
	  function GetTeacherStats($AcademicYearID, $TeacherID, $filterSubjectIdArr=array())
	  {
		  	global $eclass_db, $Lang, $intranet_session_language, $iPort, $ec_iPortfolio, $PATH_WRT_ROOT, $sys_custom;
		  	$SymbolEmpty = $Lang['General']['EmptySymbol'] ;
		  	
		  	# get terms		  	
	  		$lib_academic_year = new academic_year($AcademicYearID);
	  		$TermArr = ($lib_academic_year->Get_Term_List());
	  		$TermArr[0] = $ec_iPortfolio['overall_result'];
	  		
	  		$AcademicYearName = $lib_academic_year->Get_Academic_Year_Name();
	  		
	  		if (sizeof($TermArr)>0)
	  		{
	  			foreach ($TermArr AS $TermID => $TermName)
	  			{
	  				$TermIDs[] = $TermID;
	  			}
	  			$sql_term_ids = implode(",", $TermIDs);
	  		}
	  		
	  		$SubjectGroupRows = array();
		  
	  		# get the subject groups
	  		
			
	  		$sql =  " SELECT " .
	  				"	st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 " .
	  				" FROM " .
	  				"	SUBJECT_TERM as st LEFT JOIN SUBJECT_TERM_CLASS_TEACHER AS stct on stct.SubjectGroupID=st.SubjectGroupID " .
	  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
	  				" WHERE " .
	  				"	st.YearTermID IN ($sql_term_ids) AND stct.UserID='{$TeacherID}' AND st.SubjectComponentID=0 " .
	  				" ORDER BY " .
	  				"	stc.ClassTitleEN ";
		 	$SubjectGroupArr = $this->returnResultSet($sql);
	  		//debug($sql);
	  		//debug_rt($SubjectGroupArr);
		
	  		$lib_subject_class_mapping = new subject_class_mapping();
	  		
		  	$SubjectGroupTeacherMap = $lib_subject_class_mapping->Get_Class_Group_Teachers($AcademicYearID);
	  		
	  		# Get full marks
	  		$sql = "SELECT DISTINCT YearID, SubjectID, FullMarkInt, PassMarkInt FROM {$eclass_db}.ASSESSMENT_SUBJECT_FULLMARK " .
				"WHERE AcademicYearID='{$AcademicYearID}' ";
			$SubectFullMarkArr = $this->returnResultSet($sql);
			for ($i=0; $i<sizeof($SubectFullMarkArr); $i++)
			{
				$SubectFullMarks[$SubectFullMarkArr[$i]["SubjectID"]][$SubectFullMarkArr[$i]["YearID"]] = $SubectFullMarkArr[$i]["FullMarkInt"];
				if($SubectFullMarkArr[$i]["PassMarkInt"] == ''){
					$SubectPassMarks[$SubectFullMarkArr[$i]["SubjectID"]][$SubectFullMarkArr[$i]["YearID"]] = $SubectFullMarkArr[$i]["FullMarkInt"] / 2;
				}else{
					$SubectPassMarks[$SubectFullMarkArr[$i]["SubjectID"]][$SubectFullMarkArr[$i]["YearID"]] = $SubectFullMarkArr[$i]["PassMarkInt"];
				}
			}
		  	
		  	$TermAssessmentHeaders = array();
		  	$FlagDone = array();
		  	$TermAssessmentHeaders[0] = '';	// YearTermID = 0 means annual result, empty string title used for "continue" in later for loop
	  		
	  		# get the student and check if it is class instead of subject group
	  		$allStudentIdArr = array();
	  		for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
	  		{
	  		    $SubjectGroupArr[$i]['SubjectClassName'] = Get_Lang_Selection($SubjectGroupArr[$i]['ClassTitleB5'], $SubjectGroupArr[$i]['ClassTitleEN']);
	  			$SubjectGroupObj = $SubjectGroupArr[$i];
	  			
	  			$SubjectGroupID = $SubjectGroupObj["SubjectGroupID"];
	  			$YearTermID = $SubjectGroupObj["YearTermID"];
	  			$SubjectID = $SubjectGroupObj["SubjectID"];
	  			$InvolvedStudentArr = $lib_subject_class_mapping->Get_Subject_Group_Student_List(array($SubjectGroupID));
	  			
	  			$StudentIDs = array();
/*
				
						if (!in_array($SubjectGroupObj["SubjectClassName"], $SubjectGroupRows))
							{
								$SubjectGroupRows[] = $SubjectGroupObj["SubjectClassName"];
								$SubjectGroupIDRows[$SubjectGroupObj["SubjectClassName"]] = $SubjectGroupObj["SubjectGroupID"];
								$SubjectGroupResult[$SubjectGroupObj["SubjectClassName"]]["SubjectID"] = $SubjectID;
							}
							*/
							
						if (!in_array($SubjectGroupObj["SubjectClassName"], $SubjectGroupRows))
							{
								$SubjectGroupRows[] = $SubjectGroupObj["SubjectGroupID"];  // changed to ID
								$SubjectGroupIDRows[$SubjectGroupObj["SubjectGroupID"]] = $SubjectGroupObj["SubjectGroupID"];
								$SubjectGroupResult[$SubjectGroupObj["SubjectClassName"]]["SubjectID"] = $SubjectID;
								$SubjectGroupResult[$SubjectGroupObj["SubjectGroupID"]]['SubjectID'] = $SubjectID;								
								$SubjectGroupNames[$SubjectGroupObj["SubjectGroupID"]] = $SubjectGroupObj["SubjectClassName"];
							}
/*
	  			$SubjectGroupName = $SubjectGroupRows[$i];
	  			$SubjectGroupID = $SubjectGroupIDRows[$SubjectGroupName];
				
				$SubjectGroupID  = $SubjectGroupRows[$i];
				$SubjectGroupName = $SubjectGroupNames[$SubjectGroupID];
				*/
	  			for ($i_std=0; $i_std<sizeof($InvolvedStudentArr); $i_std++)
	  			{
	  				$StudentIDs[] = $InvolvedStudentArr[$i_std]["UserID"];
	  			}
	  			
	  			//$SubjectGroupResult[$SubjectGroupObj["SubjectClassName"]][$YearTermID]["StudentTotal"] = sizeof($StudentIDs);
	  			
	  			if (sizeof($StudentIDs)>0)
	  			{
	  				$allStudentIdArr[$SubjectGroupID] = $StudentIDs;
					$sql_student_ids = implode(",", $StudentIDs);
					
					$SubjectFormFullMark = "";
	  				
	  				# get the result from each group
					$sql = "SELECT Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID, UserID, YearTermID, Grade FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE 1=1 AND Score IS NOT NULL AND " .
							"AcademicYearID='{$AcademicYearID}' AND (YearTermID='{$YearTermID}' OR YearTermID is null) " .
							"AND UserID IN ({$sql_student_ids}) AND SubjectID='{$SubjectID}' AND SubjectComponentID IS NULL " .
							"ORDER BY TermAssessment, SubjectID, SubjectComponentID,  IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
					$SubjectMarks = $this->ReturnResultSet($sql);
					//debug($sql);
					//debug_rt($SubjectMarks);
					$PreviousTermAssessment = "";
					$PreviousIsAnnual = "";
					$MarkRecordArr = array("LOWEST"=>9999, "HIGHEST"=>0, "PassTotal"=>0, "AttemptTotal"=>0);
					
					$AllScores = array();
					
					
					
					for ($i_mark=0; $i_mark<sizeof($SubjectMarks); $i_mark++)
					{
						$EntryObj = $SubjectMarks[$i_mark];
						if ($SubjectFormFullMark=="")
						{
							# supposed all students in that subject group are from the same class level (form)
							$lib_year_class = new year_class($EntryObj["YearClassID"]);
							$SubjectFormFullMark = $SubectFullMarks[$EntryObj["SubjectID"]][$lib_year_class->YearID];							
							$SubjectFormPassMark = $SubectPassMarks[$EntryObj["SubjectID"]][$lib_year_class->YearID];							
						}
						
						
						# term header
						$TermAssessment = ($EntryObj["TermAssessment"]=="") ? "0" : $EntryObj["TermAssessment"];
						if ($TermAssessment!="0" && !$FlagDone[$YearTermID][$TermAssessment])
						{
							$TermAssessmentHeaders[$YearTermID][] = $TermAssessment;
							$FlagDone[$YearTermID][$TermAssessment] = true;
						}

						if($sys_custom['SDAS']['SKHTST']['CustFullMarks']){
						    if(
						        ($PreviousTermAssessment == 'T1A3' && strpos($lib_year_class->YearName, '6') !== false) ||
						        ($PreviousTermAssessment == 'T2A3')
						    ){
						        $SubjectFormFullMark = '100';
						        $SubjectFormPassMark = '50';
						    }
						}
						
						# marks
						# This if() is to group the marks of previous TermAssessment/Term/Total,
						#   then calculate the data. see " debug_rt($SubjectMarks); "
						if (($i_mark!=0 && ($PreviousTermAssessment!=$TermAssessment || $PreviousIsAnnual != $EntryObj['IsAnnual']) ) || $i_mark==sizeof($SubjectMarks)-1) //Last Student of the Subject Group
						{
							if ($i_mark==sizeof($SubjectMarks)-1)
							{
								//M113990 Not Allow Score = -1 count into score
								if($EntryObj["Score"] != -1 ){
									$AllScores[] = $EntryObj["Score"];
								}
// 								$AllScores[] = $EntryObj["Score"];
								$PreviousTermAssessment = $TermAssessment;
								$PreviousIsAnnual = $EntryObj['IsAnnual'];
								
								if ($MarkRecordArr["LOWEST"]>$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
								{
									$MarkRecordArr["LOWEST"] = $EntryObj["Score"];
								}
								
								if ($MarkRecordArr["HIGHEST"]<$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
								{
									$MarkRecordArr["HIGHEST"] = $EntryObj["Score"];
								}
								
								if ($EntryObj["Score"]!="" && $EntryObj["Score"]>=00)
								{
									if ($EntryObj["Score"]>=$SubjectFormPassMark)
									{
										$MarkRecordArr["PassTotal"] ++;
									}
									$MarkRecordArr["AttemptTotal"] ++;
								}else if($EntryObj["Score"] == -1 && $EntryObj["Grade"] != ''){
									$MarkRecordArr["AttemptTotal"] ++;
								}
							}
							//debug($i_mark);
							//debug_r($MarkRecordArr);
							
							//$AllScores = $ScoreArr[$dbSubjectID][$dbSubjectComponentID][$dbIsAnnual][$dbTermAssessment];
				
							//debug($i);
							//debug_r($AllScores);
							$sd = round(standard_deviation($AllScores), 2);
							//debug($sd);
							
							// [2019-0322-1240-43235]
							//$mean = round(array_sum($AllScores)/sizeof($AllScores), 3);
							if (sizeof($AllScores) > 0)
							{
							    $mean = round(array_sum($AllScores)/sizeof($AllScores), 3);
							}
							else
							{
							    $mean = $SymbolEmpty;
							}
							
							//$SubjectGroupResult[$SubjectGroupObj["SubjectClassName"]][$YearTermID]["StudentTotal"] = sizeof($StudentIDs);
							$MarkRecordArr["SubjectID"] = $SubjectID;
							$MarkRecordArr["SubjectGroupID"] = $SubjectGroupID;
							$MarkRecordArr["FullMark"] = $SubjectFormFullMark;
							// #M110009
							$MarkRecordArr["PassMark"] = $SubjectFormPassMark;
							$MarkRecordArr["StudentTotal"] = sizeof($StudentIDs);
							$MarkRecordArr["MEAN"] = $mean;
							$MarkRecordArr["SD"] = $sd;
							
							if ($PreviousIsAnnual) {
								$tmpYearTermID = 0;
								$tmpPreviousTermAssessment = 0;
							}
							else {
								$tmpYearTermID = $YearTermID;
								$tmpPreviousTermAssessment = $PreviousTermAssessment;
							}
							
							//$SubjectGroupResult[$SubjectGroupObj["SubjectClassName"]][$tmpYearTermID][$tmpPreviousTermAssessment] = $MarkRecordArr;
							//$SubjectGroupResult[$SubjectGroupObj["SubjectClassName"]]["SubjectID"] = $SubjectID;
							//$SubjectGroupResult[$SubjectGroupID][$tmpYearTermID][$tmpPreviousTermAssessment] = $MarkRecordArr;
							$SubjectGroupResult[$SubjectGroupID]["SubjectID"] = $SubjectID;

                            // [2019-0829-1147-43066]
                            $SubjectGroupClassIndex = $SubjectID.'_'.trim($SubjectGroupObj["SubjectClassName"]);
                            $SubjectGroupResult[$SubjectGroupClassIndex][$tmpYearTermID][$tmpPreviousTermAssessment] = $MarkRecordArr;

                            $MarkRecordArr = array("LOWEST"=>9999, "HIGHEST"=>0, "PassTotal"=>0, "AttemptTotal"=>0);
							
							$AllScores = array();
						}
						
						//M113990 Not Allow Score = -1 count into score
						if($EntryObj["Score"] != -1 ){
							$AllScores[] = $EntryObj["Score"];
						}
// 						$AllScores[] = $EntryObj["Score"];
						
						if ($MarkRecordArr["LOWEST"]>$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
						{
							$MarkRecordArr["LOWEST"] = $EntryObj["Score"];
						}
						
						if ($MarkRecordArr["HIGHEST"]<$EntryObj["Score"] && $EntryObj["Score"]!="" && $EntryObj["Score"]>=0)
						{
							$MarkRecordArr["HIGHEST"] = $EntryObj["Score"];
						}
						
						if ($EntryObj["Score"]!="" && $EntryObj["Score"]>=00)
						{
							if ($EntryObj["Score"]>=$SubjectFormPassMark)
							{
								$MarkRecordArr["PassTotal"] ++;
							}
							$MarkRecordArr["AttemptTotal"] ++;
						}else if($EntryObj["Score"] == -1 && $EntryObj["Grade"] != ''){
							$MarkRecordArr["AttemptTotal"] ++;
						}

						$PreviousTermAssessment = $TermAssessment;
						$PreviousIsAnnual = $EntryObj['IsAnnual'];
					}	// end for ($i_mark=0; $i_mark<sizeof($SubjectMarks); $i_mark++)
	  			}	// end if (sizeof($StudentIDs)>0)
				
	  		}	// for ($i=0; $i<sizeof($SubjectGroupArr); $i++) 
	  		//debug_r($TermAssessmentHeaders);
	  		//debug_pr($SubjectGroupResult);
	  		//debug_pr($SubjectGroupNames);
	  		
	  		$YearColSpan = 0;
	  		foreach ($TermArr as $TermID => $TermName)
  			{
  				$YearColSpan += sizeof($TermAssessmentHeaders[$TermID])*9;
  			}
	  		$YearColSpan += sizeof($TermArr)*9;
	  		
	  		$returnRX = "<div class='chart_tables'><table class='common_table_list_v30 view_table_list_v30' >";
		  	$returnRX .= "<thead>";
		  	# header !!!
		  	$returnRX .= "<tr><th colspan='3' >&nbsp;</th><th colspan='{$YearColSpan}'>{$AcademicYearName}</th></tr>";
		  	
		  	$returnRX .= "<tr><th colspan='3'>&nbsp;</th>";
		  	//$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["form_class_group"]."</th><th align='center' NoWrap>".$iPort["report_col"]["type"]."</th>";
		  	$tmpRXInside2 .= "<th align='center' NoWrap>{$iPort["SubjectGroup"]}</th>";
		  	$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["subject"]."</th><th align='center' NoWrap>".$iPort["report_col"]["teacher"]."</th>";
 
		  	if (sizeof($TermArr)>0)
	  		{
	  			foreach ($TermArr as $TermID => $TermName)
	  			{
	  				for ($i_term_id=0; $i_term_id<sizeof($TermAssessmentHeaders[$TermID]); $i_term_id++)
		  			{
		  				if ($TermID==0 && empty($TermAssessmentHeaders[$TermID][$i_term_id])) {
		  					continue;
		  				}
		  					$returnRX .= "<th colspan='9'>".$TermAssessmentHeaders[$TermID][$i_term_id]."</th>";
	  						$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["total_student"]."</th>";
	  						$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["total_attempt"]."</th>";
	  						$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["total_pass"]."</th>";
	  						$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["passing_rate"]."</th>";
	  						$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["full_mark"]."</th>";
	  						$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["average"]."</th>";
	  						$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["highest_mark"]."</th>";
	  						$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["lowest_mark"]."</th>";
	  						$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["SD"]."</th>";
	  				}
	  				$returnRX .= "<th colspan='9'>".$TermName."</th>";
	  				
	  				/*$tmpRXInside2 .= "<th align='center'>".$iPort["report_col"]["total_student"]."</th><th align='center'>".$iPort["report_col"]["total_attempt"]."</th><th align='center'>".$iPort["report_col"]["total_pass"]."</th>" .
	  							"<th align='center'>".$iPort["report_col"]["passing_rate"]."</th><th align='center'>".$iPort["report_col"]["full_mark"]."</th>" .
	  							"<th align='center'>".$iPort["report_col"]["average"]."</th><th align='center'>".$iPort["report_col"]["highest_mark"]."</th><th align='center'>".$iPort["report_col"]["lowest_mark"]."</th><th align='center'>".$iPort["report_col"]["SD"]."</th>";*/
					$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["total_student"]."</th>";
					$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["total_attempt"]."</th>";
					$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["total_pass"]."</th>";
					$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["passing_rate"]."</th>";
					$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["full_mark"]."</th>";
					$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["average"]."</th>";
					$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["highest_mark"]."</th>";
					$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["lowest_mark"]."</th>";
					$tmpRXInside2 .= "<th align='center' NoWrap>".$iPort["report_col"]["SD"]."</th>";
	  			}
	  		}
	  		$returnRX .= "</tr>";
	  		$tmpRXInside2 .= "</tr>";
	  		$returnRX .= $tmpRXInside2;
		  	$returnRX .= "</thead>";
		  	$returnRX .= "<tbody>";
		  	
	  		
	  		//debug_r($SubjectGroupResult);
	  		
  			# show results
            $displayedSubjectGroupClassIndexArr = array();
  			for ($i=0; $i<sizeof($SubjectGroupRows); $i++)
	  		{
				/*
	  			$SubjectGroupName = $SubjectGroupRows[$i];
	  			$SubjectGroupID = $SubjectGroupIDRows[$SubjectGroupName];
				*/
				
				// $SubjectGroupName IS $SubjectGroupID indeed
				$SubjectGroupID  = $SubjectGroupRows[$i];
				$SubjectGroupName = trim($SubjectGroupNames[$SubjectGroupID]);
				
	  			$ShownSubjectName = false;
	  			//$subjectID = $SubjectGroupResult[$SubjectGroupName]["SubjectID"];
				$subjectID = $SubjectGroupResult[$SubjectGroupID]["SubjectID"];
	  			
	  			if(count($filterSubjectIdArr) && !in_array($subjectID, $filterSubjectIdArr)){
	  				continue;
	  			}

	  			// [2019-0829-1147-43066]
                $SubjectGroupClassIndex = $subjectID.'_'.$SubjectGroupName;
                if(in_array($SubjectGroupClassIndex, $displayedSubjectGroupClassIndexArr)) {
                    continue;
                }
                $displayedSubjectGroupClassIndexArr[] = $SubjectGroupClassIndex;

			  	$libsubject = new subject($subjectID);
			  	$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
			  	
			  	$allStudentIdList = implode(',', (array)$allStudentIdArr[$SubjectGroupID]);
	  			$returnRX .= "<tr>";
	  			$popupLink = "javascript:submitDetailForm({$AcademicYearID},{$subjectID}, {$SubjectGroupID});";
	  			$returnRX .= "<td NoWrap><a href=\"{$popupLink}\">{$SubjectGroupName}</a></td>";
	  			//$returnRX .= "<td NoWrap>".$iPort["report_col"]["type_group"]."</td>";
				if (sizeof($TermArr)>0)
		  		{
		  			$TotalResultObj = array();
		  			
		  			foreach ($TermArr as $TermID => $TermName)
		  			{
		  				$TermResultObj = array();
		  				
		  				#### Assessment START ####
		  				for ($i_term_id=0; $i_term_id<sizeof($TermAssessmentHeaders[$TermID]); $i_term_id++)
		  				{
		  					
//		  					if ($TermID==0 && !empty($TermAssessmentHeaders[$TermID][$i_term_id])) {
//			  					continue;
//			  				}

                            // [2019-0829-1147-43066]
							//$SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupName][$TermID];
							//$SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupID][$TermID];
                            $SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupClassIndex][$TermID];

                            //debug_r($TermID);
		  					//debug_rt($SubjectGroupResultObj);
		  					
		  					if (!$ShownSubjectName)
		  					{
							  	//$libsubject = new subject($SubjectGroupResult[$SubjectGroupName]["SubjectID"]);
							  	//$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
							  	
								$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID], ', <br />');
	  							$returnRX .= "<td NoWrap>{$SubjectName}</td><td NoWrap>{$Teachers}</td>";
	  							
	  							$ShownSubjectName = true;
		  					}
		  					
		  					$ResultFound = false;
		  					if (sizeof($SubjectGroupResultObj)>0)
		  					{
			  					foreach($SubjectGroupResultObj AS $TermAssessment => $ResultObj)
			  					{
			  						
			  						if ($TermAssessment!="0" && $TermAssessment==$TermAssessmentHeaders[$TermID][$i_term_id])
		  							{
			  							$ResultFound = true;
			  							if ($ResultObj["AttemptTotal"]>0)
			  							{
			  								$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
			  							} else
			  							{
			  								$PassingRate = $SymbolEmpty;
			  							}
			  							$FullMark = (int) $ResultObj["FullMark"];
			  							$PassMark = $ResultObj["PassMark"];
			  							
										$failed_style_bg = "";
										$failed_style_font1 = "";
										$failed_style_font2 = "";
			  							if ($PassMark>0)
			  							{
			  								# style for failed @ Mean
			  								// #M110009 
			  								//if ($ResultObj["MEAN"]<$FullMark/2)
			  								if ($ResultObj["MEAN"] < $PassMark)
			  								{
			  									$failed_style_bg = "style='background:yellow'";
			  									$failed_style_font1 = "<font color='red'>";
			  									$failed_style_font2 = "</font>";
			  								}
			  							}
					  					$returnRX .= "<td align='center'>".$ResultObj["StudentTotal"]."</td>";
							  			$returnRX .= "<td align='center'>".$ResultObj["AttemptTotal"]."</td>";
							  			if($ResultObj["HIGHEST"] < $ResultObj["LOWEST"]){ // Only Grade
				  							for ($i_empty=0; $i_empty<7; $i_empty++)
					  						{
					  							$returnRX .= "<td align='center' NoWrap>".$SymbolEmpty."</td>";
					  						}
							  			}else{
								  			$returnRX .= "<td align='center'>".$ResultObj["PassTotal"]."</td>";
					  						$returnRX .= "<td align='center'>".$PassingRate."</td>";
								  			$returnRX .= "<td align='center'>".$FullMark."</td>";
				  							$returnRX .= "<td align='center' $failed_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td>";
				  							$returnRX .= "<td align='center'>".$ResultObj["HIGHEST"]."</td>";
				  							$returnRX .= "<td align='center'>".$ResultObj["LOWEST"]."</td>";
				  							$returnRX .= "<td align='center'>".round($ResultObj["SD"],1)."</td>";
							  			}
		  							} /*elseif ($TermID != '0' && $TermAssessment=="0")
		  							{
		  								$TermResultObj = $ResultObj;
		  								//debug_r($TermResultObj);
		  							}elseif ($TermID == '0'){
		  								$TotalResultObj = $ResultObj;
		  							}*/
			  					}
		  					}
		  					
		  					if (!$ResultFound)
		  					{
		  						if (empty($TermAssessmentHeaders[$TermID][$i_term_id])) {
		  							// do nth
		  						}
		  						else {
		  							for ($i_empty=0; $i_empty<9; $i_empty++)
			  						{
			  							$returnRX .= "<td align='center' NoWrap>".$SymbolEmpty."</td>";
			  						}
		  						}
		  					}
		  				}
		  				#### Assessment END ####
		  				

		  				#### Term START ####
                        // [2019-0829-1147-43066]
// 		  				$SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupName][$TermID];
//		  				$SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupID][$TermID];
                        $SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupClassIndex][$TermID];

		  				if (!$ShownSubjectName)
		  				{
		  					$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID], ', <br />');
		  					$returnRX .= "<td NoWrap>{$SubjectName}</td><td NoWrap>{$Teachers}</td>";
		  				
		  					$ShownSubjectName = true;
		  				}
		  				$ResultFound = false;
		  				if (sizeof($SubjectGroupResultObj)>0)
		  				{
		  					foreach($SubjectGroupResultObj AS $TermAssessment => $ResultObj)
		  					{
	  							if($TermID != '0' && $TermAssessment=="0"){
	  								$TermResultObj = $ResultObj;
	  							}
		  					}
		  				}
		  				if (sizeof($TermResultObj)>0)
	  					{
	  						$ResultObj = $TermResultObj;
							if ($ResultObj["AttemptTotal"]>0)
  							{
  								$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
  							} else
  							{
  								$PassingRate = $SymbolEmpty;
  							}
  							$FullMark = (int) $ResultObj["FullMark"];
  							$PassMark = $ResultObj["PassMark"];
  							
							$failed_style_bg = "";
							$failed_style_font1 = "";
							$failed_style_font2 = "";
  							if ($PassMark>0)
  							{
  								# style for failed @ Mean
  								// #M110009
  								//if ($ResultObj["MEAN"]<$FullMark/2)
  								if ($ResultObj["MEAN"] < $PassMark)
  								{
  									$failed_style_bg = "style='background:yellow'";
  									$failed_style_font1 = "<font color='red'>";
  									$failed_style_font2 = "</font>";
  								}
  							}
		  					/*$returnRX .= "<td align='center'>".$ResultObj["StudentTotal"]."</td><td align='center'>".$ResultObj["AttemptTotal"]."</td><td align='center'>".$ResultObj["PassTotal"]."</td>" .
	  							"<td align='center'>".$PassingRate."</td><td align='center'>".$FullMark."</td>" .
	  							"<td align='center' $failed_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center'>".$ResultObj["HIGHEST"]."</td><td align='center'>".$ResultObj["LOWEST"]."</td><td align='center'>".round($ResultObj["SD"],1)."</td>";*/

  							$returnRX .= "<td align='center'>".$ResultObj["StudentTotal"]."</td>";
  							$returnRX .= "<td align='center'>".$ResultObj["AttemptTotal"]."</td>";
		  					if($ResultObj["HIGHEST"] < $ResultObj["LOWEST"]){ // Only Grade
		  						for ($i_empty=0; $i_empty<7; $i_empty++)
		  						{
		  							$returnRX .= "<td align='center' NoWrap>".$SymbolEmpty."</td>";
		  						}
		  					}else{
		  						$returnRX .= "<td align='center'>".$ResultObj["PassTotal"]."</td>";
		  						$returnRX .= "<td align='center'>".$PassingRate."</td>";
		  						$returnRX .= "<td align='center'>".$FullMark."</td>";
		  						$returnRX .= "<td align='center' $failed_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td>";
		  						$returnRX .= "<td align='center'>".$ResultObj["HIGHEST"]."</td>";
		  						$returnRX .= "<td align='center'>".$ResultObj["LOWEST"]."</td>";
		  						$returnRX .= "<td align='center'>".round($ResultObj["SD"],1)."</td>";
		  					}
	  					} else
	  					{
	  						if (!$ShownSubjectName)
		  					{
							  	$libsubject = new subject($SubjectGroupResult[$SubjectGroupName]["SubjectID"]);
							  	$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
							  	
								$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID], ', <br />');
	  							$returnRX .= "<td NoWrap>{$SubjectName}</td><td NoWrap>{$Teachers}</td>";
	  							
	  							$ShownSubjectName = true;
		  					}
		  					if($TermID != '0'){
		  						for ($i_empty=0; $i_empty<9; $i_empty++)
		  						{
		  							$returnRX .= "<td align='center' NoWrap>".$SymbolEmpty."</td>";
		  						}
		  					}
	  					}
		  				#### Term END ####
		  			}
		  			
		  			#### Total START ####
                    // [2019-0829-1147-43066]
// 		  			$SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupName][$TermID];
//		  			$SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupID][$TermID];
                    $SubjectGroupResultObj = $SubjectGroupResult[$SubjectGroupClassIndex][$TermID];

		  			if (!$ShownSubjectName)
		  			{
		  				$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID], ', <br />');
		  				$returnRX .= "<td NoWrap>{$SubjectName}</td><td NoWrap>{$Teachers}</td>";
		  			
		  				$ShownSubjectName = true;
		  			}
		  			$ResultFound = false;
		  			if (sizeof($SubjectGroupResultObj)>0)
		  			{
		  				foreach($SubjectGroupResultObj AS $TermAssessment => $ResultObj)
		  				{
		  					if($TermID == "0"){
		  						$TotalResultObj = $ResultObj;
		  					}
		  				}
		  			}
		  			if (sizeof($TotalResultObj)>0)
  					{
  						$ResultObj = $TotalResultObj;
						if ($ResultObj["AttemptTotal"]>0)
						{
							$PassingRate = round(100*$ResultObj["PassTotal"] / $ResultObj["AttemptTotal"], 1) ."%";
						} else
						{
							$PassingRate = $SymbolEmpty;
						}
						$FullMark = (int) $ResultObj["FullMark"];
						$PassMark = $ResultObj["PassMark"];
		  							
						$failed_style_bg = "";
						$failed_style_font1 = "";
						$failed_style_font2 = "";
						if ($PassMark>0)
						{
							# style for failed @ Mean
							//#M110009
							//if ($ResultObj["MEAN"]<$FullMark/2)
							if ($ResultObj["MEAN"] < $PassMark)
							{
								$failed_style_bg = "style='background:yellow'";
								$failed_style_font1 = "<font color='red'>";
								$failed_style_font2 = "</font>";
							}
						}
	  					/*$returnRX .= "<td align='center'>".$ResultObj["StudentTotal"]."</td><td align='center'>".$ResultObj["AttemptTotal"]."</td><td align='center'>".$ResultObj["PassTotal"]."</td>" .
  							"<td align='center'>".$PassingRate."</td><td align='center'>".$FullMark."</td>" .
  							"<td align='center' $failed_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td><td align='center'>".$ResultObj["HIGHEST"]."</td><td align='center'>".$ResultObj["LOWEST"]."</td><td align='center'>".round($ResultObj["SD"],1)."</td>";*/
	  					$returnRX .= "<td align='center'>".$ResultObj["StudentTotal"]."</td>";
	  					$returnRX .= "<td align='center'>".$ResultObj["AttemptTotal"]."</td>";
	  					if($ResultObj["HIGHEST"] < $ResultObj["LOWEST"]){ // Only Grade
	  						for ($i_empty=0; $i_empty<7; $i_empty++)
	  						{
	  							$returnRX .= "<td align='center' NoWrap>".$SymbolEmpty."</td>";
	  						}
	  					}else{
	  						$returnRX .= "<td align='center'>".$ResultObj["PassTotal"]."</td>";
	  						$returnRX .= "<td align='center'>".$PassingRate."</td>";
	  						$returnRX .= "<td align='center'>".$FullMark."</td>";
	  						$returnRX .= "<td align='center' $failed_style_bg >$failed_style_font1".round($ResultObj["MEAN"],1)."$failed_style_font2</td>";
	  						$returnRX .= "<td align='center'>".$ResultObj["HIGHEST"]."</td>";
	  						$returnRX .= "<td align='center'>".$ResultObj["LOWEST"]."</td>";
	  						$returnRX .= "<td align='center'>".round($ResultObj["SD"],1)."</td>";
	  					}
  					} else
  					{
  						if (!$ShownSubjectName)
	  					{
						  	$libsubject = new subject($SubjectGroupResult[$SubjectGroupName]["SubjectID"]);
						  	$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
						  	
							$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID], ', <br />');
  							$returnRX .= "<td NoWrap>{$SubjectName}</td><td NoWrap>{$Teachers}</td>";
  							
  							$ShownSubjectName = true;
	  					}
  						for ($i_empty=0; $i_empty<9; $i_empty++)
  						{
  							$returnRX .= "<td align='center' NoWrap>".$SymbolEmpty."</td>";
  						}
  					}
		  			#### Total END ####
		  		}
		  		$returnRX .= "</tr>";
	  			
	  		}
  			
  			$returnRX .= "</tbody>";
  			$returnRX .= "</table>";
//   			$returnRX .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'];
  			$returnRX .= "<br />
				<span class=\"tabletextremark\">
					{$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']}
					<br>
					{$ec_iPortfolio['scoreHighlightRemarks']}
				</span>";
  			$returnRX .= "</div>";
	  		
	  		return $returnRX;
	  }
	  
	  
	  function GetTeacherStatsCompareByTerm($AcademicYearID, $TeacherID, $FromYearTermID, $FromYearTermAssessment, $ToYearTermID, $ToYearTermAssessment, $filterSubjectIdArr=array(), $prepareChartData=false)
	  {
	  		global $eclass_db, $Lang, $intranet_session_language, $iPort, $ec_iPortfolio, $PATH_WRT_ROOT;
		  	$SymbolEmpty = $Lang['General']['EmptySymbol'] ;
		  	if($prepareChartData){
		  		$js = "\n".'<script type="text/javascript" language="javascript">'."\n";
		  		$js.= 'var settings = {"data":[],
										"xColumnName":"x",
										"yColumnName":"%",
									   "barColors":[{"id":"gradient1","stops":[{"color":"#3182bd","offset":0},{"color":"#9ecae1","offset":1}]}]
						};'."\n";
		  	}
		  	
		  	# get terms		  	
	  		$lib_academic_year = new academic_year($AcademicYearID);
	  		$TermArr = ($lib_academic_year->Get_Term_List());
	  		$TermArr[0] = $ec_iPortfolio['overall_result'];
	  		
	  		$AcademicYearName = $lib_academic_year->Get_Academic_Year_Name();

	  		$TermArrTmp = array();
  			$TermIDs = array();
  			$CompareTermsArr = array();
	  		if (sizeof($TermArr)>0)
	  		{
	  			foreach ($TermArr AS $TermID => $TermName)
	  			{
	  				if ($TermID==$FromYearTermID || $TermID==$ToYearTermID)
	  				{
	  					$TermIDs[] = $TermID;
	  					$TermArrTmp[$TermID] = $TermName;
	  					$CompareTermsArr[] = $TermName;
	  				}
	  			}
	  			$sql_term_ids = implode(",", $TermIDs);
	  			$TermArr = $TermArrTmp;
	  			$SameTerm = (sizeof($TermArr)==1);
	  			$CompareTerms = ($SameTerm) ? $FromYearTermAssessment." , ".$ToYearTermAssessment : implode(" , ", $CompareTermsArr);
	  		}
	  		
	  		$CompareFromName = ($FromYearTermAssessment=="") ?  $TermArrTmp[$FromYearTermID]: $FromYearTermAssessment;
	  		$CompareToName = ($ToYearTermAssessment=="") ?  $TermArrTmp[$ToYearTermID]: $ToYearTermAssessment;
	  		$CompareTerms = $CompareFromName." , ".$CompareToName;
	  		
	  		$SubjectGroupRows = array();
		  	
	  		# get the subject groups
	  		$lib_subject_class_mapping = new subject_class_mapping();
		  	$SubjectGroupTeacherMap = $lib_subject_class_mapping->Get_Class_Group_Teachers($AcademicYearID);
	  		
			if ($SameTerm || $FromYearTermID == 0 || $ToYearTermID == 0)
			{
		  		$sql =  " SELECT " .
		  				"	DISTINCT st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 " .
		  				" FROM " .
		  				"	SUBJECT_TERM as st LEFT JOIN SUBJECT_TERM_CLASS_TEACHER AS stct on stct.SubjectGroupID=st.SubjectGroupID " .
		  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
		  				" WHERE " .
		  				"	st.YearTermID IN ($sql_term_ids) AND stct.UserID='{$TeacherID}' AND st.SubjectComponentID=0 " .
		  				" ORDER BY " .
		  				"	stc.ClassTitleEN ";
			 	$SubjectGroupArr = $this->returnResultSet($sql);
			 	
			 	for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
		  		{
		  		    $SubjectGroupArr[$i]['SubjectClassName'] = Get_Lang_Selection($SubjectGroupArr[$i]['ClassTitleB5'], $SubjectGroupArr[$i]['ClassTitleEN']);
	  				$SubjectGroupArr[$i]["CompareToSubjectGroupID"] = $SubjectGroupArr[$i]["SubjectGroupID"];
		  		}
			} else
			{
				# if compare from different terms, find corresponding subject groups!!
		  		$sql =  " SELECT " .
		  				"	st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 " .
		  				" FROM " .
		  				"	SUBJECT_TERM as st LEFT JOIN SUBJECT_TERM_CLASS_TEACHER AS stct on stct.SubjectGroupID=st.SubjectGroupID " .
		  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
		  				" WHERE " .
		  				"	st.YearTermID IN ($FromYearTermID) AND stct.UserID='{$TeacherID}' AND st.SubjectComponentID=0 " .
		  				" ORDER BY " .
		  				"	stc.ClassTitleEN ";
			 	$SubjectGroupArr = $this->returnResultSet($sql);
			 	
			 	for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
		  		{
		  		    $SubjectGroupArr[$i]['SubjectClassName'] = Get_Lang_Selection($SubjectGroupArr[$i]['ClassTitleB5'], $SubjectGroupArr[$i]['ClassTitleEN']);
		  			$SubjectGroupObj = $SubjectGroupArr[$i];
		  			
		  			$SubjectClassName = $SubjectGroupObj["SubjectClassName"];
		  			$SubjectID = $SubjectGroupObj["SubjectID"];
		  			$another_term_groups[] = " ( (stc.ClassTitleEN='".addslashes($SubjectClassName)."' OR stc.ClassTitleB5='".addslashes($SubjectClassName)."') AND st.SubjectID='{$SubjectID}') ";
		  		}
		  		if (sizeof($another_term_groups)>0)
		  		{
		  			$sql_another_term_group = " AND ( ". implode(" OR ", $another_term_groups) . " )";
		  			
			  		$sql =  " SELECT " .
			  				"	DISTINCT st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 " .
			  				" FROM " .
			  				"	SUBJECT_TERM as st LEFT JOIN SUBJECT_TERM_CLASS_TEACHER AS stct on stct.SubjectGroupID=st.SubjectGroupID " .
			  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
			  				" WHERE " .
			  				"	st.YearTermID IN ($ToYearTermID) AND stct.UserID='{$TeacherID}' AND st.SubjectComponentID=0 {$sql_another_term_group} " .
			  				" ORDER BY " .
			  				"	stc.ClassTitleEN ";
				 	$SubjectGroupArrTo = $this->returnResultSet($sql);
				 	for ($i=0; $i<sizeof($SubjectGroupArrTo); $i++){
				 	    $SubjectGroupArrTo[$i]['SubjectClassName'] = Get_Lang_Selection($SubjectGroupArrTo[$i]['ClassTitleB5'], $SubjectGroupArrTo[$i]['ClassTitleEN']);
				 	}

				 	for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
			  		{
			  			$SubjectGroupObj = $SubjectGroupArr[$i];
			  			
			  			$SubjectClassName = $SubjectGroupObj["SubjectClassName"];
			  			$SubjectID = $SubjectGroupObj["SubjectID"];
			  			
			  			for ($k=0; $k<sizeof($SubjectGroupArrTo); $k++)
			  			{
			  			    $SubjectGroupArrTo[$i]['SubjectClassName'] = Get_Lang_Selection($SubjectGroupArrTo[$i]['ClassTitleB5'], $SubjectGroupArrTo[$i]['ClassTitleEN']);
			  					if ($SubjectGroupArrTo[$k]["SubjectClassName"]==$SubjectClassName && $SubjectGroupArrTo[$k]["SubjectID"]==$SubjectID)
			  					{
			  						$SubjectGroupArr[$i]["CompareToSubjectGroupID"] = $SubjectGroupArrTo[$k]["SubjectGroupID"];
			  					}
			  			}
			  		}
		  		}
			}
	  		//debug($sql);
	  		//debug_r($SubjectGroupArr);
	  		
	  		
	  		$TermAssessmentHeaders[$FromYearTermID][] = $FromYearTermAssessment;
	  		$TermAssessmentHeaders[$ToYearTermID][] = $ToYearTermAssessment;
	  		if ($FromYearTermAssessment=="")
	  		{
	  			$FromYearTermAssessment = "0";
	  		}
	  		if ($ToYearTermAssessment=="")
	  		{
	  			$ToYearTermAssessment = "0";
	  		}

			$SDrows = $this->GetSDandMean(array($AcademicYearID));
			
			$returnRX = "<div class='chart_tables'><table class='common_table_list_v30 view_table_list_v30' >";
		  	$returnRX .= "<thead>";
		  	# header !!!
		  	$returnRX .= "<tr><th colspan='3' >&nbsp;</th><th colspan='3'>{$AcademicYearName}</th></tr>";
		  	$returnRX .= "<tr><th colspan='3' >&nbsp;</th><th colspan='3'>".$iPort["report_col"]["compare"] ." : {$CompareTerms}</th></tr>";
		  	
		  	$returnRX .= "<tr>";
// 		  	$returnRX .= "<th align='center'>".$iPort["report_col"]["form_class_group"]."</th><th align='center'>".$iPort["report_col"]["type"]."</th>";
		  	$returnRX .= "<th align='center'>{$iPort["SubjectGroup"]}</th>";
		  	$returnRX .= "<th align='center'>".$iPort["report_col"]["subject"]."</th><th align='center'>".$iPort["report_col"]["teacher"]."</th>";
		  		$returnRX .= "<th align='center' nowrap='nowrap'>".$iPort["report_col"]["attempted_both"]."</th><th align='center' nowrap='nowrap'>".$iPort["report_col"]["improved_by_ss"]."</th><th align='center' nowrap='nowrap'>".$iPort["report_col"]["improved_by_percentage"]."</th>";
	  		$returnRX .= "</tr>";
		  	$returnRX .= "</thead>";
		  	$returnRX .= "<tbody>";
			
			if($prepareChartData){
				$js .= 'settings["title"] = "'.$AcademicYearName.' - '.$iPort["report_col"]["compare"] .' : '.$CompareTerms.'";'."\n";
			}
			
	  		# get the student and check if it is class instead of subject group
		  	$hasRecord = false;
	  		for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
	  		{
	  			$SubjectGroupObj = $SubjectGroupArr[$i];
	  			$SubjectGroupID = $SubjectGroupObj["SubjectGroupID"];
	  			$SubjectGroupID2nd = $SubjectGroupObj["CompareToSubjectGroupID"];
	  			$YearTermID = $SubjectGroupObj["YearTermID"];
	  			$SubjectID_Raw = $SubjectGroupObj["SubjectID"];
	  			$SubjectID = $SubjectID_Raw.'_0';
	  			$SubjectGroupName = $SubjectGroupObj["SubjectClassName"];
	  			
	  			if ($SubjectGroupID2nd=="" || $SubjectGroupID2nd<1)
	  			{
	  				continue;
	  			}

	  			if(count($filterSubjectIdArr) && !in_array($SubjectID_Raw, $filterSubjectIdArr)){
	  				continue;
	  			}
	  			
	  			# first batch students
	  			$InvolvedStudentArr = $lib_subject_class_mapping->Get_Subject_Group_Student_List(array($SubjectGroupID));	  			
	  			$StudentIDs = array();
	  			for ($i_std=0; $i_std<sizeof($InvolvedStudentArr); $i_std++)
	  			{
	  				$StudentIDs[] = $InvolvedStudentArr[$i_std]["UserID"];
	  			}
	  			  			
	  			# second batch students
		  		$StudentIDs2nd = array();
	  			if ($SubjectGroupID2nd==$SubjectGroupID)
	  			{
	  				$StudentIDs2nd = $StudentIDs;
	  			} else
	  			{
		  			$InvolvedStudentArr = $lib_subject_class_mapping->Get_Subject_Group_Student_List(array($SubjectGroupID2nd));	
		  			for ($i_std=0; $i_std<sizeof($InvolvedStudentArr); $i_std++)
		  			{
		  				$StudentIDs2nd[] = $InvolvedStudentArr[$i_std]["UserID"];
		  			}
	  			}
	  			//debug_r($StudentIDs);
	  			//debug('------------');
	  			//debug_r($StudentIDs2nd);
	  			
	  			if (sizeof($StudentIDs)>0 && sizeof($StudentIDs2nd)>0)
	  			{
					$sql_student_ids = implode(",", $StudentIDs);
					$sql_student_ids2nd = implode(",", $StudentIDs2nd);
					
					$SubjectFormFullMark = "";
					
					$sql_term_assessment = ($FromYearTermAssessment=="0" || $FromYearTermAssessment=="") ? " TermAssessment IS NULL " : " TermAssessment='{$FromYearTermAssessment}' ";
					$sql_yearTermID = ($FromYearTermID=="0" || $FromYearTermID=="") ? " YearTermID IS NULL " : " YearTermID='{$FromYearTermID}' ";
	  				
	  				# get the result from each group
					$sql = "SELECT UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
							"AcademicYearID='{$AcademicYearID}' AND {$sql_yearTermID}" .
							"AND UserID IN ({$sql_student_ids}) AND SubjectID='{$SubjectID}' AND {$sql_term_assessment} AND SubjectComponentID is null  " .
							"ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
					$SubjectMarksFrom = $this->ReturnResultSet($sql);
					//debug($sql);
					//debug_rt($SubjectMarksFrom);
					
					$YearClasses = array();
					for ($i_from=0; $i_from<sizeof($SubjectMarksFrom); $i_from++)
					{
						if (!in_array($SubjectMarksFrom[$i_from]["YearClassID"], $YearClasses))
						{
							$YearClasses[] = $SubjectMarksFrom[$i_from]["YearClassID"];
						}
					}
					
					$sql_term_assessment = ($ToYearTermAssessment=="0" || $ToYearTermAssessment=="") ? " TermAssessment IS NULL " : " TermAssessment='{$ToYearTermAssessment}' ";
					$sql_yearTermID = ($ToYearTermID=="0" || $ToYearTermID=="") ? " YearTermID IS NULL " : " YearTermID='{$ToYearTermID}' ";
	  				
	  				# get the result from each group
					$sql = "SELECT UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
							"AcademicYearID='{$AcademicYearID}' AND {$sql_yearTermID} " .
							"AND UserID IN ({$sql_student_ids2nd}) AND SubjectID='{$SubjectID}' AND {$sql_term_assessment} AND SubjectComponentID is null " .
							"ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
					$SubjectMarksTo = $this->ReturnResultSet($sql);
					//debug($sql);
					//debug_r($SubjectMarksTo);
					
					for ($i_to=0; $i_to<sizeof($SubjectMarksTo); $i_to++)
					{
						if (!in_array($SubjectMarksTo[$i_to]["YearClassID"], $YearClasses))
						{
							$YearClasses[] = $SubjectMarksTo[$i_to]["YearClassID"];
						}
					}
					
					if (sizeof($YearClasses)>0)
					{
						$sql_yearclassID = implode(", ", $YearClasses);
						$sql = "SELECT DISTINCT YearClassID, YearID FROm YEAR_CLASS WHERE YearClassID IN ($sql_yearclassID) ";
						$YearIDs = $this->returnResultSet($sql);
						for ($i_year=0; $i_year<sizeof($YearIDs); $i_year++)
						{
							$ClasYearMapping[$YearIDs[$i_year]["YearClassID"]] = $YearIDs[$i_year]["YearID"];
						}
					} else
					{
						# no class and year setting!
					}
					//debug_r($SubjectMarksFrom);
					//debug_r($SubjectMarksTo);
					
					$studentIdArr = array();
					$Stats = array("AttemptTotal"=>0, "ImprovedTotal"=>0);
					for ($i_from=0; $i_from<sizeof($SubjectMarksFrom); $i_from++)
					{
						// UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID
						$SubjectMarksFromObj = $SubjectMarksFrom[$i_from];
						for ($i_to=0; $i_to<sizeof($SubjectMarksTo); $i_to++)
						{
							$SubjectMarksToObj = $SubjectMarksTo[$i_to];
							if ($SubjectMarksFromObj["UserID"]==$SubjectMarksToObj["UserID"])
							{
								$Stats["AttemptTotal"] ++;
								
								# Standard Score of first assessment
								$isAnnual = ($FromYearTermID == 0 && $FromYearTermAssessment == 0);
								$Mean = round($SDrows[$SubjectMarksFromObj["SubjectID"]][0][$AcademicYearID][$FromYearTermID][$FromYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksFromObj["YearClassID"]]]["MEAN"], 2);
	  							$SD = round($SDrows[$SubjectMarksFromObj["SubjectID"]][0][$AcademicYearID][$FromYearTermID][$FromYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksFromObj["YearClassID"]]]["SD"], 2);
	  							$StandardScoreFrom = 0;
	  							if ($SD>0)
	  							{
	  								$StandardScoreFrom = round(($SubjectMarksFromObj["Score"]-$Mean)/$SD, 2);
	  							}
	  							
	  							
								# Standard Score of 2nd assessment
								$isAnnual = ($ToYearTermID == 0 && $ToYearTermAssessment == 0);
								$Mean = round($SDrows[$SubjectMarksToObj["SubjectID"]][0][$AcademicYearID][$ToYearTermID][$ToYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksToObj["YearClassID"]]]["MEAN"], 2);
	  							$SD = round($SDrows[$SubjectMarksToObj["SubjectID"]][0][$AcademicYearID][$ToYearTermID][$ToYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksToObj["YearClassID"]]]["SD"], 2);
	  							$StandardScoreTo = 0;
	  							if ($SD>0)
	  							{
	  								$StandardScoreTo = round(($SubjectMarksToObj["Score"]-$Mean)/$SD, 2);
	  							}
	  							
								if ($StandardScoreTo>$StandardScoreFrom)
								{
									$Stats["ImprovedTotal"] ++;
								}
								
								//debug($StandardScoreFrom, $StandardScoreTo, $SubjectID);
								$studentIdArr[] = $SubjectMarksFromObj["UserID"];
							}
						}
					}
					$libsubject = new subject($SubjectID);
					$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
					$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID]);
					$improveRate = ($Stats["AttemptTotal"]>0) ? round(100*$Stats["ImprovedTotal"]/$Stats["AttemptTotal"],2)."%" : $SymbolEmpty;
					$returnRX .= "<tr>";
					$studentIdList = implode(',', $studentIdArr);
					$detailsLink = "javascript:submitSearchDetail(\"{$SubjectID}\", [{$studentIdList}]);";
					$returnRX .= "<td align='center'><a href='{$detailsLink}'>{$SubjectGroupName}</a></td>";
					//$returnRX .= "<td align='center'>".$iPort["report_col"]["type_group"]."</td>";
					$returnRX .= "<td align='center'>{$SubjectName}</td>";
					$returnRX .= "<td align='center'>{$Teachers}</td>";
					$returnRX .= "<td align='center'>".$Stats["AttemptTotal"]."</td>";
					$returnRX .= "<td align='center'>".$Stats["ImprovedTotal"]."</td>";
					$returnRX .= "<td align='center'>{$improveRate}</td>";
					$returnRX .= "</tr>";
					$rowDisplayCount++;
					$hasRecord = true;
					
					if($prepareChartData){
						$js .= 'settings["data"].push({"x":"'.str_replace('"','\"',$SubjectGroupName.' '.$SubjectName.' '.strip_tags($Teachers)).'","'.$iPort["report_col"]["improved_by_percentage"].'":'.intval($improveRate).'});'."\n";
					}
	  			}
	  		} // End for
	  		
	  		if(!$hasRecord){
	  			$returnRX .= "<tr>";
	  			$returnRX .= "<td align='center' colspan='99'>{$Lang['General']['NoRecordFound']}</td>";
	  			$returnRX .= "</tr>";
	  		}
	  		
		  	$returnRX .= "</tbody>";
		  	$returnRX .= "</table></div>";
		  	$returnRX .= '<span class="tabletextremark">';
		  	$returnRX .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'];
		  	$returnRX .= "</br>";
		  	$returnRX .= $ec_iPortfolio['scoreHighlightRemarks'];
		  	$returnRX .= "</span>";
		  	
		  	if($prepareChartData){
		  		$js .= '</script>';
		  		$returnRX .= $js;
		  	}
		  	
		  	return $returnRX;
	  }
	  
	  function GetSubjectStatsCompareByTerm($AcademicYearID, $SubjectID, $FromYearTermID, $FromYearTermAssessment, $ToYearTermID, $ToYearTermAssessment, $ResultType = array(), $filterYearId=array(), $filterYearClassId=array(), $filterSubjectGroupId=array())
	  {
	  		global $eclass_db, $Lang, $intranet_session_language, $iPort, $ec_iPortfolio, $PATH_WRT_ROOT;
		  	$SymbolEmpty = $Lang['General']['EmptySymbol'] ;
		  	$rowDisplaycount = 0;
		  	# get terms		  	
	  		$lib_academic_year = new academic_year($AcademicYearID);
	  		$TermArr = ($lib_academic_year->Get_Term_List());
	  		$TermArr[0] = $ec_iPortfolio['overall_result'];

	  		$AcademicYearName = $lib_academic_year->Get_Academic_Year_Name();
	  		$lib_academic_year = new academic_year($AcademicYearID);
	  		$TermArr = ($lib_academic_year->Get_Term_List());
	  		$TermArr[0] = $ec_iPortfolio['overall_result'];

	  		
	  		$accessRight = $this->getAssessmentStatReportAccessRight();
	  		if(!$accessRight['admin']){
	  		    if($accessRight['subjectPanel'] && sizeof($accessRight['subjectPanel'] > 0)){
	  		        $subjectPanelCond = "OR ( st.SubjectID IN ('" . implode("','", $accessRight['subjectPanel']) . "') )";   
	  		    }
	  		    if($accessRight['subjectGroup'] && sizeof($accessRight['subjectGroup']) > 0){
	  		        $subjectGroupCond = " AND (st.SubjectGroupID IN ('" . implode("','", $accessRight['subjectGroup']) . "')  $subjectPanelCond)";
	  		    } else if($filterYearId && $filterSubjectGroupId && !empty($filterSubjectGroupId)) {
                    // [DM#1260-1261] [Class / Subject Teacher] SKIP > not subject panel + not current class / subject group teacher
                    $subjectGroupCond = " AND (st.SubjectGroupID IN ('" . implode("','", $filterSubjectGroupId) . "')  $subjectPanelCond)";
                } else {
	  		        $subjectGroupCond = " AND ( 1 = 0 ) ";
	  		    }
	  		}
	  			
	  		######### Get releated subject START #########
// 	  		$_subjectIds = $this->getReleatedSubjectID();
// 	  		$parentSubjID = $SubjectID;
// 	  		$subjectIdArr = array($SubjectID);
// 	  		foreach($_subjectIds as $parentSubj => $_subjIdArr){
// 	  			if(in_array($SubjectID, $_subjIdArr) || $parentSubj == $SubjectID){
// 	  				$parentSubjID = $parentSubj;
// 	  				$subjectIdArr = $_subjIdArr;
// 	  				$subjectIdArr[] = $parentSubj;
// 	  				break;
// 	  			}
// 	  		}
// 	  		$SubjectIdList = implode("','", $subjectIdArr);
	  		
	  		$temp = explode("_",$SubjectID);
	  		$MainSubjectID = $temp[0];
	  		$CmpSubjectID = $temp[1];
	  		$cond = " SubjectID = '$MainSubjectID'";
	  		if($CmpSubjectID){
	  			$isCmp = true;
	  			$cond .= " AND SubjectComponentID = '$CmpSubjectID' ";
	  		}else{
	  			$isCmp = false;
	  			$cond .= " AND SubjectComponentID IS NULL ";
	  		}
	  		######### Get releated subject END #########
	  		
	  		$TermArrTmp = array();
	  		if (sizeof($TermArr)>0)
	  		{
	  			foreach ($TermArr AS $TermID => $TermName)
	  			{
	  				if ($TermID==$FromYearTermID || $TermID==$ToYearTermID)
	  				{
	  					$TermIDs[] = $TermID;
	  					$TermArrTmp[$TermID] = $TermName;
	  					$CompareTermsArr[] = $TermName;
	  				}else{
	  					$TermIDs[] = $TermID;
	  				}
	  			}
	  			$sql_term_ids = implode(",", (array)$TermIDs);
	  			$TermArr = $TermArrTmp;
	  			$SameTerm = (sizeof($TermArr)==1);
	  			//$CompareTerms = ($SameTerm) ? $FromYearTermAssessment." , ".$ToYearTermAssessment : implode(" , ", $CompareTermsArr);
	  		}


	  		$CompareFromName = ($FromYearTermAssessment=="") ?  $TermArrTmp[$FromYearTermID]: $FromYearTermAssessment;
	  		$CompareToName = ($ToYearTermAssessment=="") ?  $TermArrTmp[$ToYearTermID]: $ToYearTermAssessment;
	  		$CompareTerms = $CompareFromName." vs ".$CompareToName;
	  		
	  		$lib_subject_class_mapping = new subject_class_mapping();
		  	$SubjectGroupTeacherMap = $lib_subject_class_mapping->Get_Class_Group_Teachers($AcademicYearID);
	  		
		  	$libsubject = new subject($parentSubjID);
		  	$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
	  		

			$SDrows = $this->GetSDandMean(array($AcademicYearID));
			
			$returnRX = "<div class='chart_tables'><table class='common_table_list_v30 view_table_list_v30' >";
		  	$returnRX .= "<thead>";
		  	# header !!!
		  	$returnRX .= "<tr><th colspan='3' rowspan='2'>{$SubjectName}</th><th colspan='3'>{$AcademicYearName}</th></tr>";
		  	$returnRX .= "<tr><th colspan='3'>".$iPort["report_col"]["compare"] ." : {$CompareTerms}</th></tr>";
		  	
		  	$returnRX .= "<tr>";
		  	$returnRX .= "<th align='center'>".$iPort["FormClassSubjectGroup"]."</th><th align='center'>".$iPort["report_col"]["type"]."</th>" .
		  			"<th align='center'>".$iPort["report_col"]["teacher"]."</th>";
		  		$returnRX .= "<th align='center' nowrap='nowrap'>".$iPort["report_col"]["attempted_both"]."</th><th align='center' nowrap='nowrap'>".$iPort["report_col"]["improved_by_ss"]."</th><th align='center' nowrap='nowrap'>".$iPort["zScoreImprovementPercent"]."</th>";
	  		$returnRX .= "</tr>";
		  	$returnRX .= "</thead>";
		  	$returnRX .= "<tbody>";
	  		
	  		if ($FromYearTermAssessment=="")
	  		{
	  			$FromYearTermAssessment = "0";
	  		}
	  		if ($ToYearTermAssessment=="")
	  		{
	  			$ToYearTermAssessment = "0";
	  		}
	  		
	  		# 1. form stats
	  		
	  		# forms/years
	  		$lib_year = new Year();
	  		$AllYearLevels = $lib_year->Get_All_Year_List();
	  		
	  		for ($i_form=0; $i_form<sizeof($AllYearLevels); $i_form++)
  			{
  				$formObj = $AllYearLevels[$i_form];
  				if($filterYearId){
  					if(!in_array($formObj['YearID'], $filterYearId)){
  						continue;
  					}
  				}
  				
  				$lib_year = new Year($formObj["YearID"]);
				$AllClasses = $lib_year->Get_All_Classes("", $AcademicYearID);
				
				$YearClasses = array();
				for ($i_class=0; $i_class<sizeof($AllClasses); $i_class++)
				{
					$YearClasses[] = $AllClasses[$i_class]["YearClassID"];
				}
				$sql_yearclassID = implode(",", $YearClasses);
				$sql_term_assessment = ($FromYearTermAssessment=="0" || $FromYearTermAssessment=="") ? " TermAssessment IS NULL " : " TermAssessment='{$FromYearTermAssessment}' ";
	  			$sql_yearTermID = ($FromYearTermID=="0" || $FromYearTermID=="") ? " YearTermID IS NULL " : " YearTermID='{$FromYearTermID}' ";
	  				
  				# get the result from each group
  				$sql = "
  							SELECT 
  								UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID 
  							FROM 
  								{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
							WHERE 
								Score>=0 
							AND 
								Score IS NOT NULL 
							AND 
								AcademicYearID='{$AcademicYearID}' 
							AND 
								{$sql_yearTermID} 
							AND 
								YearClassID IN ({$sql_yearclassID}) 
							AND 
								$cond
							AND 
								{$sql_term_assessment} 
							
							ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual 
  												";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
// 				$sql = "SELECT UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
// 						"AcademicYearID='{$AcademicYearID}' AND {$sql_yearTermID} " .
// 						"AND YearClassID IN ({$sql_yearclassID}) AND SubjectID IN ('{$SubjectIdList}') AND {$sql_term_assessment} AND SubjectComponentID is null " .
// 						"ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
				$SubjectMarksFrom = $this->ReturnResultSet($sql);
				
				
				$sql_term_assessment = ($ToYearTermAssessment=="0" || $ToYearTermAssessment=="") ? " TermAssessment IS NULL " : " TermAssessment='{$ToYearTermAssessment}' ";
				$sql_yearTermID = ($ToYearTermID=="0" || $ToYearTermID=="") ? " YearTermID IS NULL " : " YearTermID='{$ToYearTermID}' ";
  				
  				# get the result from each group
				$sql = "SELECT 
							UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID 
						FROM 
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
						WHERE
							Score>=0 
						AND
							Score IS NOT NULL
						AND
							AcademicYearID='{$AcademicYearID}' 
						AND
							{$sql_yearTermID}
						AND
							YearClassID IN ({$sql_yearclassID})
						AND
							$cond
						AND
							{$sql_term_assessment}
						
						ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
// 				$sql = "SELECT UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
// 						"AcademicYearID='{$AcademicYearID}' AND {$sql_yearTermID} " .
// 						"AND YearClassID IN ({$sql_yearclassID}) AND SubjectID IN ('{$SubjectIdList}') AND {$sql_term_assessment} AND SubjectComponentID is null  " .
// 						"ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
				$SubjectMarksTo = $this->ReturnResultSet($sql);
				
				if (sizeof($YearClasses)>0)
				{
					$sql_yearclassID = implode(", ", $YearClasses);
					$sql = "SELECT 
								DISTINCT YearClassID, YearID 
							FROM 
								YEAR_CLASS 
							WHERE 
								YearClassID IN ($sql_yearclassID) ";
					$YearIDs = $this->returnResultSet($sql);
					for ($i_year=0; $i_year<sizeof($YearIDs); $i_year++)
					{
						$ClasYearMapping[$YearIDs[$i_year]["YearClassID"]] = $YearIDs[$i_year]["YearID"];
					}
				} else
				{
					# no class and year setting!
				}
				
				$studentIdArr = array();
				$Stats = array("AttemptTotal"=>0, "ImprovedTotal"=>0);
				for ($i_from=0; $i_from<sizeof($SubjectMarksFrom); $i_from++)
				{
					// UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID
					$SubjectMarksFromObj = $SubjectMarksFrom[$i_from];
					for ($i_to=0; $i_to<sizeof($SubjectMarksTo); $i_to++)
					{
						$SubjectMarksToObj = $SubjectMarksTo[$i_to];
						if ($SubjectMarksFromObj["UserID"]==$SubjectMarksToObj["UserID"])
						{
							$Stats["AttemptTotal"] ++;
							
							# Standard Score of first assessment
							$isAnnual = ($FromYearTermID == 0 && $FromYearTermAssessment == 0)? 1 : 0;
							$Mean = round($SDrows[$SubjectMarksFromObj["SubjectID"]][0][$AcademicYearID][$FromYearTermID][$FromYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksFromObj["YearClassID"]]]["MEAN"], 2);
  							$SD = round($SDrows[$SubjectMarksFromObj["SubjectID"]][0][$AcademicYearID][$FromYearTermID][$FromYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksFromObj["YearClassID"]]]["SD"], 2);
  							$StandardScoreFrom = 0;
  							if ($SD>0)
  							{
  								$StandardScoreFrom = round(($SubjectMarksFromObj["Score"]-$Mean)/$SD, 2);
  							}
  							
  							
							# Standard Score of 2nd assessment
							$isAnnual = ($ToYearTermID == 0 && $ToYearTermAssessment == 0)? 1 : 0;
							$Mean = round($SDrows[$SubjectMarksToObj["SubjectID"]][0][$AcademicYearID][$ToYearTermID][$ToYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksToObj["YearClassID"]]]["MEAN"], 2);
  							$SD = round($SDrows[$SubjectMarksToObj["SubjectID"]][0][$AcademicYearID][$ToYearTermID][$ToYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksToObj["YearClassID"]]]["SD"], 2);
  							$StandardScoreTo = 0;
  							if ($SD>0)
  							{
  								$StandardScoreTo = round(($SubjectMarksToObj["Score"]-$Mean)/$SD, 2);
  							}
							if ($StandardScoreTo>$StandardScoreFrom)
							{
								$Stats["ImprovedTotal"] ++;
							}
							
							//debug($StandardScoreFrom, $StandardScoreTo, $SubjectID);
							$studentIdArr[] = $SubjectMarksFromObj["UserID"];
						}
					}
				}
				//$libsubject = new subject($SubjectID);
				//$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
				if ($Stats["AttemptTotal"]>0)
				{
					$improveRate = ($Stats["AttemptTotal"]>0) ? round(100*$Stats["ImprovedTotal"]/$Stats["AttemptTotal"],2)."%" : $SymbolEmpty;
				  	if(in_array('form',$ResultType)){
				  		$backgroundColorStyle = 'background-color:#FFE7B2;';
						$returnRX .= "<tr>";
						$studentIdList = implode(',', $studentIdArr);
						$detailsLink = "javascript:submitSearchDetail([{$studentIdList}]);";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'><a href=\"{$detailsLink}\">".$formObj["YearName"]."</a></td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>".$ec_iPortfolio['form']."</td>";
						//$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>{$SubjectName}</td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>&nbsp;</td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>".$Stats["AttemptTotal"]."</td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>".$Stats["ImprovedTotal"]."</td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>{$improveRate}</td>";
						$returnRX .= "</tr>";
						$rowDisplayCount++;
				  	}
				}

  			}
	  		
	  		# 2. class stats
	  		$lib_year_class = new year_class();
	  		$AllYearClasses = $lib_year_class->Get_All_Class_List($AcademicYearID);
	  		//debug_r($AllYearClasses);
	  		for ($i_year_class=0; $i_year_class<sizeof($AllYearClasses); $i_year_class++)
  			{
  				$YearClassObj = $AllYearClasses[$i_year_class];
  				if($filterYearId){
  					if(!in_array($YearClassObj['YearID'], $filterYearId)){
  						continue;
  					}
  				}
                // [EJ DM#1260-1261] SKIP > not subject panel + not current class teacher
                if($filterYearId && $filterYearClassId && isset($filterYearClassId[$YearClassObj['YearID']])) {
                    if(!in_array($YearClassObj["YearClassID"], (array)$filterYearClassId[$YearClassObj['YearID']])){
                        continue;
                    }
                }

  				# get corresponding subject group(s)
				$sql_yearclassID = $YearClassObj["YearClassID"];
				$sql_term_assessment = ($FromYearTermAssessment=="0" || $FromYearTermAssessment=="") ? " TermAssessment IS NULL " : " TermAssessment='{$FromYearTermAssessment}' ";
	  			$sql_yearTermID = ($FromYearTermID=="0" || $FromYearTermID=="") ? " YearTermID IS NULL " : " YearTermID='{$FromYearTermID}' ";
	  				
  				# get the result from each group
  				$sql = "
						SELECT 
							UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID 
						FROM
							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
						WHERE 
							Score>=0 
						AND 
							Score IS NOT NULL 
						AND
							AcademicYearID='{$AcademicYearID}'
						AND
							$sql_yearTermID 
						AND
							YearClassID IN ({$sql_yearclassID}) 
						AND
							$cond
						AND
							{$sql_term_assessment} 
						
						ORDER BY
							SubjectID, SubjectComponentID, TermAssessment, IsAnnual 
  				";// ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
							
// 				$sql = "SELECT UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
// 						"AcademicYearID='{$AcademicYearID}' AND $sql_yearTermID " .
// 						"AND YearClassID IN ({$sql_yearclassID}) AND SubjectID IN ('{$SubjectIdList}') AND {$sql_term_assessment} AND SubjectComponentID is null  " .
// 						"ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  
				$SubjectMarksFrom = $this->ReturnResultSet($sql);
				
				
				$sql_term_assessment = ($ToYearTermAssessment=="0" || $ToYearTermAssessment=="") ? " TermAssessment IS NULL " : " TermAssessment='{$ToYearTermAssessment}' ";
				$sql_yearTermID = ($ToYearTermID=="0" || $ToYearTermID=="") ? " YearTermID IS NULL " : " YearTermID='{$ToYearTermID}' ";
  				
  				# get the result from each group
  				$sql = "SELECT 
		  					UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID 
		  				FROM 
		  					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
		  				WHERE 
		  					Score>=0 
		  				AND 
		  					Score IS NOT NULL 
		  				AND 
							AcademicYearID='{$AcademicYearID}' 
						AND 
							{$sql_yearTermID} 
						AND 
							YearClassID IN ({$sql_yearclassID}) 
						AND
							$cond
						AND
							{$sql_term_assessment}
						
						ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";
// 				$sql = "SELECT UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
// 						"AcademicYearID='{$AcademicYearID}' AND {$sql_yearTermID} " .
// 						"AND YearClassID IN ({$sql_yearclassID}) AND SubjectID IN ('{$SubjectIdList}') AND {$sql_term_assessment} AND SubjectComponentID is null " .
// 						"ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
				$SubjectMarksTo = $this->ReturnResultSet($sql);
				//debug($sql);
// 				debug_pr($SubjectMarksTo);die();
				
				if (sizeof($YearClasses)>0 || $sql_yearclassID!="")
				{
					$sql_yearclassID = implode(", ", $YearClasses);
					$sql = "SELECT DISTINCT YearClassID, YearID FROm YEAR_CLASS WHERE YearClassID IN ($sql_yearclassID) ";
					$YearIDs = $this->returnResultSet($sql);
					for ($i_year=0; $i_year<sizeof($YearIDs); $i_year++)
					{
						$ClasYearMapping[$YearIDs[$i_year]["YearClassID"]] = $YearIDs[$i_year]["YearID"];
					}
				} else
				{
					# no class and year setting!
				}
				
				$studentIdArr = array();
				$Stats = array("AttemptTotal"=>0, "ImprovedTotal"=>0);
				for ($i_from=0; $i_from<sizeof($SubjectMarksFrom); $i_from++)
				{
					// UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID
					$SubjectMarksFromObj = $SubjectMarksFrom[$i_from];
					for ($i_to=0; $i_to<sizeof($SubjectMarksTo); $i_to++)
					{
						$SubjectMarksToObj = $SubjectMarksTo[$i_to];
						if ($SubjectMarksFromObj["UserID"]==$SubjectMarksToObj["UserID"])
						{
							$Stats["AttemptTotal"] ++;
							
							# Standard Score of first assessment
							$isAnnual = ($FromYearTermID == 0 && $FromYearTermAssessment == 0)? 1 : 0;
							$Mean = round($SDrows[$SubjectMarksFromObj["SubjectID"]][0][$AcademicYearID][$FromYearTermID][$FromYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksFromObj["YearClassID"]]]["MEAN"], 2);
  							$SD = round($SDrows[$SubjectMarksFromObj["SubjectID"]][0][$AcademicYearID][$FromYearTermID][$FromYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksFromObj["YearClassID"]]]["SD"], 2);
  							$StandardScoreFrom = 0;
  							if ($SD>0)
  							{
  								$StandardScoreFrom = round(($SubjectMarksFromObj["Score"]-$Mean)/$SD, 2);
  							}
  							
  							
							# Standard Score of 2nd assessment
							$isAnnual = ($ToYearTermID == 0 && $ToYearTermAssessment == 0)? 1 : 0;
							$Mean = round($SDrows[$SubjectMarksToObj["SubjectID"]][0][$AcademicYearID][$ToYearTermID][$ToYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksToObj["YearClassID"]]]["MEAN"], 2);
  							$SD = round($SDrows[$SubjectMarksToObj["SubjectID"]][0][$AcademicYearID][$ToYearTermID][$ToYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksToObj["YearClassID"]]]["SD"], 2);
  							$StandardScoreTo = 0;
  							if ($SD>0)
  							{
  								$StandardScoreTo = round(($SubjectMarksToObj["Score"]-$Mean)/$SD, 2);
  							}
  							
							if ($StandardScoreTo>$StandardScoreFrom)
							{
								$Stats["ImprovedTotal"] ++;
							}
							
							//debug($StandardScoreFrom, $StandardScoreTo, $SubjectID);
							$studentIdArr[] = $SubjectMarksFromObj["UserID"];
						}
					}
				}

				if ($Stats["AttemptTotal"]>0)
				{
					# get class students
	  				$lib_year_class = new year_class($YearClassObj["YearClassID"], false, $GetClassTeacherList=false,$GetClassStudentList=true,$GetLockedSGArr=false);
	  				$ClassStudentArr = $lib_year_class->ClassStudentList;
	  				$StudentIDsArr = array();
	  				$SubjectTeachersArr = array();
	  				for ($i_cs=0; $i_cs<sizeof($ClassStudentArr); $i_cs++)
	  				{
	  					$StudentIDsArr[] = $ClassStudentArr[$i_cs]["UserID"];
	  				}
	  				if (sizeof($StudentIDsArr)>0)
	  				{
	  					$sql_user_ids = implode(",", $StudentIDsArr);
	  					$sql = "SELECT
	  								DISTINCT stcu.SubjectGroupID 
	  							 FROM
	  							 	SUBJECT_TERM_CLASS_USER AS stcu
	  							 INNER JOIN 
	  							 	SUBJECT_TERM AS st 
	  							 		ON
	  							 			st.SubjectGroupID=stcu.SubjectGroupID
	  							 WHERE
	  								stcu.UserID IN ($sql_user_ids) 
			  					 AND
			  						st.SubjectID = $MainSubjectID
			  					 AND
			  						st.YearTermID='$FromYearTermID'
                                 $subjectGroupCond
	  					            ";
// 	  					$sql = "SELECT DISTINCT stcu.SubjectGroupID " .
// 	  							" FROM SUBJECT_TERM_CLASS_USER AS stcu, SUBJECT_TERM AS st " .
// 	  							" WHERE stcu.UserID IN ($sql_user_ids) AND st.SubjectID IN ('{$SubjectIdList}') AND st.SubjectGroupID=stcu.SubjectGroupID AND st.YearTermID='$FromYearTermID' ";
	  					$SubjectGroupsArr = $this->returnVector($sql);
	  					for ($i_sg=0; $i_sg<sizeof($SubjectGroupsArr); $i_sg++)
	  					{
	  						$SubjectTeachersArr = array_merge((array)$SubjectTeachersArr, (array)$SubjectGroupTeacherMap[$SubjectGroupsArr[$i_sg]]);
	  					}
	  					
	  					# another term
	  					if ($FromYearTermID!=$ToYearTermID)
	  					{
		  					$sql_user_ids = implode(",", $StudentIDsArr);
		  					$sql = "SELECT 
		  								DISTINCT stcu.SubjectGroupID 
		  							FROM 
		  								SUBJECT_TERM_CLASS_USER AS stcu
		  							INNER JOIN
		  								SUBJECT_TERM AS st 
		  									ON
		  										st.SubjectGroupID=stcu.SubjectGroupID
		  							WHERE
		  								stcu.UserID IN ($sql_user_ids) 
		  							AND 
		  								st.SubjectID = $MainSubjectID
		  							AND 
		  								st.YearTermID='$ToYearTermID' ";
// 		  					$sql = "SELECT DISTINCT stcu.SubjectGroupID " .
// 		  							" FROM SUBJECT_TERM_CLASS_USER AS stcu, SUBJECT_TERM AS st " .
// 		  							" WHERE stcu.UserID IN ($sql_user_ids) AND st.SubjectID IN ('{$SubjectIdList}') AND st.SubjectGroupID=stcu.SubjectGroupID AND st.YearTermID='$ToYearTermID' ";
		  					$SubjectGroupsArr = $this->returnVector($sql);
		  					for ($i_sg=0; $i_sg<sizeof($SubjectGroupsArr); $i_sg++)
		  					{
		  						if (is_array($SubjectGroupTeacherMap[$SubjectGroupsArr[$i_sg]]))
		  						{
		  							$SubjectTeachersArr = array_merge((array)$SubjectTeachersArr, (array)$SubjectGroupTeacherMap[$SubjectGroupsArr[$i_sg]]);
		  						}
		  					}
	  					}
	  				}
	  				$Teachers = $this->GetTeacherNames($SubjectTeachersArr);
	  				
	  				
					$improveRate = ($Stats["AttemptTotal"]>0) ? round(100*$Stats["ImprovedTotal"]/$Stats["AttemptTotal"],2)."%" : $SymbolEmpty;
					if(in_array('class',$ResultType)){
				  		$backgroundColorStyle = 'background-color:#DAEEFF;';
						$returnRX .= "<tr>";
						$studentIdList = implode(',', $studentIdArr);
						$detailsLink = "javascript:submitSearchDetail([{$studentIdList}]);";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'><a href=\"{$detailsLink}\">".$YearClassObj["ClassTitle"]."</a></td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>".$ec_iPortfolio['class']."</td>";
						//$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>{$SubjectName}</td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>{$Teachers}</td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>".$Stats["AttemptTotal"]."</td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>".$Stats["ImprovedTotal"]."</td>";
						$returnRX .= "<td align='center' style='{$backgroundColorStyle}'>{$improveRate}</td>";
						$returnRX .= "</tr>";
						$rowDisplayCount++;
					}
				}

  			}
	  		
	  		# 3. subject group stats
	  		
	  		$SubjectGroupRows = array();
		  	
	  		# get the subject groups
			if ($SameTerm || $FromYearTermID == 0 || $ToYearTermID == 0)
			{
				$sql =  "SELECT 
							DISTINCT st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 
						FROM 
							SUBJECT_TERM as st 
						LEFT JOIN 
							SUBJECT_TERM_CLASS_TEACHER AS stct 
								on 
									stct.SubjectGroupID=st.SubjectGroupID
						LEFT JOIN 
							SUBJECT_TERM_CLASS AS stc 
								on 
									stc.SubjectGroupID=st.SubjectGroupID 
						WHERE 
							st.YearTermID IN ($sql_term_ids) 
						AND
							st.SubjectID = $MainSubjectID
						AND 
							st.SubjectComponentID=0
                        $subjectGroupCond
						ORDER BY
							stc.ClassTitleEN ";
// 		  		$sql =  " SELECT " .
// 		  				"	DISTINCT st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 " .
// 		  				" FROM " .
// 		  				"	SUBJECT_TERM as st LEFT JOIN SUBJECT_TERM_CLASS_TEACHER AS stct on stct.SubjectGroupID=st.SubjectGroupID " .
// 		  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
// 		  				" WHERE " .
// 		  				"	st.YearTermID IN ($sql_term_ids) AND st.SubjectID IN ('{$SubjectIdList}') AND st.SubjectComponentID=0 " .
// 		  				" ORDER BY " .
// 		  				"	stc.ClassTitleEN ";
			 	$SubjectGroupArr = $this->returnResultSet($sql);			 	

			 	for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
		  		{
		  		    $SubjectGroupArr[$i]['SubjectClassName'] = Get_Lang_Selection($SubjectGroupArr[$i]['ClassTitleB5'], $SubjectGroupArr[$i]['ClassTitleEN']);
	  				$SubjectGroupArr[$i]["CompareToSubjectGroupID"] = $SubjectGroupArr[$i]["SubjectGroupID"];
		  		}
			} else
			{
				# if compare from different terms, find corresponding subject groups!!
				$sql =  "SELECT 
							DISTINCT st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5
						FROM
							SUBJECT_TERM as st 
						LEFT JOIN 
							SUBJECT_TERM_CLASS_TEACHER AS stct 
								on stct.SubjectGroupID=st.SubjectGroupID
						LEFT JOIN 
							SUBJECT_TERM_CLASS AS stc 
								on stc.SubjectGroupID=st.SubjectGroupID 
						WHERE
							st.YearTermID IN ($FromYearTermID) 
						AND 
							st.SubjectID = $MainSubjectID
						AND 
							st.SubjectComponentID=0 
                        $subjectGroupCond
						ORDER BY
							stc.ClassTitleEN ";
// 		  		$sql =  " SELECT " .
// 		  				"	DISTINCT st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 " .
// 		  				" FROM " .
// 		  				"	SUBJECT_TERM as st LEFT JOIN SUBJECT_TERM_CLASS_TEACHER AS stct on stct.SubjectGroupID=st.SubjectGroupID " .
// 		  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
// 		  				" WHERE " .
// 		  				"	st.YearTermID IN ($FromYearTermID) AND st.SubjectID IN ('{$SubjectIdList}') AND st.SubjectComponentID=0 " .
// 		  				" ORDER BY " .
// 		  				"	stc.ClassTitleEN ";
			 	$SubjectGroupArr = $this->returnResultSet($sql);
			 	
			 	for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
		  		{
		  		    $SubjectGroupArr[$i]['SubjectClassName'] = Get_Lang_Selection($SubjectGroupArr[$i]['ClassTitleB5'], $SubjectGroupArr[$i]['ClassTitleEN']);
		  			$SubjectGroupObj = $SubjectGroupArr[$i];
		  			
		  			$SubjectClassName = $SubjectGroupObj["SubjectClassName"];
		  			$SubjectID = $SubjectGroupObj["SubjectID"];
		  			$another_term_groups[] = " ( (stc.ClassTitleEN='".addslashes($SubjectClassName)."' OR stc.ClassTitleB5='".addslashes($SubjectClassName)."') AND st.SubjectID='{$SubjectID}') ";
		  		}
		  		if (sizeof($another_term_groups)>0)
		  		{
		  			$sql_another_term_group = " AND ( ". implode(" OR ", $another_term_groups) . " )";
		  			$sql =  " SELECT 
		  							st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5
		  						FROM 
		  							SUBJECT_TERM as st 
		  						LEFT JOIN SUBJECT_TERM_CLASS_TEACHER AS stct 
		  							on 
		  								stct.SubjectGroupID=st.SubjectGroupID 
		  						LEFT JOIN SUBJECT_TERM_CLASS AS stc 
		  							on 
		  								stc.SubjectGroupID=st.SubjectGroupID
		  					 	WHERE
		  							st.YearTermID IN ($ToYearTermID) 
					  			AND 
					  				st.SubjectID = $MainSubjectID
					  			AND 
					  				st.SubjectComponentID=0 {$sql_another_term_group}
		  					 ORDER BY
		  						stc.ClassTitleEN ";
// 			  		$sql =  " SELECT " .
// 			  				"	st.SubjectTermID, st.SubjectID, st.SubjectComponentID, st.YearTermID, st.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5 " .
// 			  				" FROM " .
// 			  				"	SUBJECT_TERM as st LEFT JOIN SUBJECT_TERM_CLASS_TEACHER AS stct on stct.SubjectGroupID=st.SubjectGroupID " .
// 			  				"	LEFT JOIN SUBJECT_TERM_CLASS AS stc on stc.SubjectGroupID=st.SubjectGroupID " .
// 			  				" WHERE " .
// 			  				"	st.YearTermID IN ($ToYearTermID) AND st.SubjectID IN ('{$SubjectIdList}') AND st.SubjectComponentID=0 {$sql_another_term_group} " .
// 			  				" ORDER BY " .
// 			  				"	stc.ClassTitleEN ";
				 	$SubjectGroupArrTo = $this->returnResultSet($sql);
				 	
				 	for ($i=0; $i<sizeof($SubjectGroupArrTo); $i++){
				 	    $SubjectGroupArrTo[$i]['SubjectClassName'] = Get_Lang_Selection($SubjectGroupArrTo[$i]['ClassTitleB5'], $SubjectGroupArrTo[$i]['ClassTitleEN']);
				 	}
				 	for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
			  		{
			  			$SubjectGroupObj = $SubjectGroupArr[$i];
			  			$SubjectClassName = $SubjectGroupObj["SubjectClassName"];
			  			$SubjectID = $SubjectGroupObj["SubjectID"];
			  			
			  			for ($k=0; $k<sizeof($SubjectGroupArrTo); $k++)
			  			{
		  					if ($SubjectGroupArrTo[$k]["SubjectClassName"]==$SubjectClassName && $SubjectGroupArrTo[$k]["SubjectID"]==$SubjectID){
		  						$SubjectGroupArr[$i]["CompareToSubjectGroupID"] = $SubjectGroupArrTo[$k]["SubjectGroupID"];
		  					}
			  			}
			  		}
		  		}
			}
	  		
	  		$TermAssessmentHeaders[$FromYearTermID][] = $FromYearTermAssessment;
	  		$TermAssessmentHeaders[$ToYearTermID][] = $ToYearTermAssessment;

	  		# get the student and check if it is class instead of subject group
	  		for ($i=0; $i<sizeof($SubjectGroupArr); $i++)
	  		{
	  			$SubjectGroupObj = $SubjectGroupArr[$i];
	  			$SubjectGroupID = $SubjectGroupObj["SubjectGroupID"];
	  			$SubjectGroupID2nd = $SubjectGroupObj["CompareToSubjectGroupID"];
	  			$YearTermID = $SubjectGroupObj["YearTermID"];
	  			$SubjectID = $SubjectGroupObj["SubjectID"];
	  			$SubjectGroupName = $SubjectGroupObj["SubjectClassName"];
	  			
	  			if ($SubjectGroupID2nd=="" || $SubjectGroupID2nd<1)
	  			{
	  				continue;
	  			}
	  			
	  			# first batch students
	  			$InvolvedStudentArr = $lib_subject_class_mapping->Get_Subject_Group_Student_List(array($SubjectGroupID));	  			
	  			$StudentIDs = array();
	  			for ($i_std=0; $i_std<sizeof($InvolvedStudentArr); $i_std++)
	  			{
	  				$StudentIDs[] = $InvolvedStudentArr[$i_std]["UserID"];
	  			}
	  			  			
	  			# second batch students
		  		$StudentIDs2nd = array();
	  			if ($SubjectGroupID2nd==$SubjectGroupID)
	  			{
	  				$StudentIDs2nd = $StudentIDs;
	  			} else
	  			{
		  			$InvolvedStudentArr = $lib_subject_class_mapping->Get_Subject_Group_Student_List(array($SubjectGroupID2nd));	
		  			for ($i_std=0; $i_std<sizeof($InvolvedStudentArr); $i_std++)
		  			{
		  				$StudentIDs2nd[] = $InvolvedStudentArr[$i_std]["UserID"];
		  			}
	  			}
	  			
	  			if (sizeof($StudentIDs)>0 && sizeof($StudentIDs2nd)>0)
	  			{
					$sql_student_ids = implode(",", $StudentIDs);
					$sql_student_ids2nd = implode(",", $StudentIDs2nd);
					
					$sql_term_assessment = ($FromYearTermAssessment=="0" || $FromYearTermAssessment=="") ? " TermAssessment IS NULL " : " TermAssessment='{$FromYearTermAssessment}' ";
	  				$sql_yearTermID = ($FromYearTermID=="0" || $FromYearTermID=="") ? " YearTermID IS NULL " : " YearTermID='{$FromYearTermID}' ";
	  				
	  				# get the result from each group
					$sql = "SELECT 
								UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID 
							FROM 
								{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
							WHERE 
								Score>=0 
							AND 
								Score IS NOT NULL 
							AND 
								AcademicYearID='{$AcademicYearID}' 
							AND 
								$sql_yearTermID 
							AND 
								UserID IN ({$sql_student_ids}) 
							AND 
								$cond
							AND 
								{$sql_term_assessment} 
						
							ORDER BY 
								SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
// 					$sql = "SELECT UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
// 					"AcademicYearID='{$AcademicYearID}' AND $sql_yearTermID " .
// 					"AND UserID IN ({$sql_student_ids}) AND SubjectID IN ('{$SubjectIdList}') AND {$sql_term_assessment} AND SubjectComponentID is null  " .
// 					"ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
					$SubjectMarksFrom = $this->ReturnResultSet($sql);
					//debug($sql);
					//debug_r($SubjectMarksFrom);
					
					$YearClasses = array();
					for ($i_from=0; $i_from<sizeof($SubjectMarksFrom); $i_from++)
					{
						if (!in_array($SubjectMarksFrom[$i_from]["YearClassID"], $YearClasses))
						{
							$YearClasses[] = $SubjectMarksFrom[$i_from]["YearClassID"];
						}
					}
					
					$sql_term_assessment = ($ToYearTermAssessment=="0" || $ToYearTermAssessment=="") ? " TermAssessment IS NULL " : " TermAssessment='{$ToYearTermAssessment}' ";
	  				$sql_yearTermID = ($ToYearTermID=="0" || $ToYearTermID=="") ? " YearTermID IS NULL " : " YearTermID='{$ToYearTermID}' ";
	  				
	  				# get the result from each group
	  				$sql = "SELECT 
	  							UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID 
			  				FROM 
			  					{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD 
			  				WHERE 
			  					Score>=0 
			  				AND 
			  					Score IS NOT NULL 
			  				AND 
			  					AcademicYearID='{$AcademicYearID}' 
			  				AND 
			  					{$sql_yearTermID}
			  				AND 
			  					UserID IN ({$sql_student_ids2nd}) 
			  				AND 
			  					$cond
			  				AND 
			  					{$sql_term_assessment} 
			  				
			  				ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
			  		$SubjectMarksTo = $this->ReturnResultSet($sql);
// 					$sql = "SELECT UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID FROM {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD WHERE Score>=0 AND Score IS NOT NULL AND " .
// 							"AcademicYearID='{$AcademicYearID}' AND {$sql_yearTermID} " .
// 							"AND UserID IN ({$sql_student_ids2nd}) AND SubjectID IN ('{$SubjectIdList}') AND {$sql_term_assessment} AND SubjectComponentID is null " .
// 							"ORDER BY SubjectID, SubjectComponentID, TermAssessment, IsAnnual ";  // ORDER IS IMPORTANT FOR BELOW CALCULATION LOGIC
// 					$SubjectMarksTo = $this->ReturnResultSet($sql);
					//debug($sql);
					//debug_r($SubjectMarksTo);
					
					for ($i_to=0; $i_to<sizeof($SubjectMarksTo); $i_to++)
					{
						if (!in_array($SubjectMarksTo[$i_to]["YearClassID"], $YearClasses))
						{
							$YearClasses[] = $SubjectMarksTo[$i_to]["YearClassID"];
						}
					}
					
					if (sizeof($YearClasses)>0)
					{
						$sql_yearclassID = implode(", ", $YearClasses);
						$sql = "SELECT 
									DISTINCT YearClassID, YearID 
								FROm
									YEAR_CLASS 
								WHERE
									YearClassID IN ($sql_yearclassID) ";
						$YearIDs = $this->returnResultSet($sql);
						for ($i_year=0; $i_year<sizeof($YearIDs); $i_year++)
						{
							$ClasYearMapping[$YearIDs[$i_year]["YearClassID"]] = $YearIDs[$i_year]["YearID"];
						}
					} else
					{
						# no class and year setting!
					}
					
					$studentIdArr = array();
					$Stats = array("AttemptTotal"=>0, "ImprovedTotal"=>0);
					for ($i_from=0; $i_from<sizeof($SubjectMarksFrom); $i_from++)
					{
						// UserID, Score, SubjectID, SubjectComponentID, TermAssessment, IsAnnual, YearClassID
						$SubjectMarksFromObj = $SubjectMarksFrom[$i_from];
						for ($i_to=0; $i_to<sizeof($SubjectMarksTo); $i_to++)
						{
							$SubjectMarksToObj = $SubjectMarksTo[$i_to];
							if ($SubjectMarksFromObj["UserID"]==$SubjectMarksToObj["UserID"])
							{
								$Stats["AttemptTotal"] ++;
								
								# Standard Score of first assessment
								$isAnnual = ($FromYearTermID == 0 && $FromYearTermAssessment == 0)? 1 : 0;
								$Mean = round($SDrows[$SubjectMarksFromObj["SubjectID"]][0][$AcademicYearID][$FromYearTermID][$FromYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksFromObj["YearClassID"]]]["MEAN"], 2);
	  							$SD = round($SDrows[$SubjectMarksFromObj["SubjectID"]][0][$AcademicYearID][$FromYearTermID][$FromYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksFromObj["YearClassID"]]]["SD"], 2);
	  							$StandardScoreFrom = 0;
	  							if ($SD>0)
	  							{
	  								$StandardScoreFrom = round(($SubjectMarksFromObj["Score"]-$Mean)/$SD, 2);
	  							}
	  							
	  							
								# Standard Score of 2nd assessment
								$isAnnual = ($ToYearTermID == 0 && $ToYearTermAssessment == 0)? 1 : 0;
								$Mean = round($SDrows[$SubjectMarksToObj["SubjectID"]][0][$AcademicYearID][$ToYearTermID][$ToYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksToObj["YearClassID"]]]["MEAN"], 2);
	  							$SD = round($SDrows[$SubjectMarksToObj["SubjectID"]][0][$AcademicYearID][$ToYearTermID][$ToYearTermAssessment][$isAnnual][0][$ClasYearMapping[$SubjectMarksToObj["YearClassID"]]]["SD"], 2);
	  							$StandardScoreTo = 0;
	  							if ($SD>0)
	  							{
	  								$StandardScoreTo = round(($SubjectMarksToObj["Score"]-$Mean)/$SD, 2);
	  							}
	  							
								if ($StandardScoreTo>$StandardScoreFrom)
								{
									$Stats["ImprovedTotal"] ++;
								}
								
								//debug($StandardScoreFrom, $StandardScoreTo, $SubjectID);
								$studentIdArr[] = $SubjectMarksFromObj["UserID"];
							}
						}
					}
					//$libsubject = new subject($SubjectID);
					//$SubjectName = ($intranet_session_language=="b5") ? $libsubject->CH_DES : $libsubject->EN_DES;
					if ($Stats["AttemptTotal"]>0)
					{
						$Teachers = $this->GetTeacherNames($SubjectGroupTeacherMap[$SubjectGroupID]);
						$improveRate = ($Stats["AttemptTotal"]>0) ? round(100*$Stats["ImprovedTotal"]/$Stats["AttemptTotal"],2)."%" : $SymbolEmpty;
						if(in_array('group',$ResultType)){
							$returnRX .= "<tr>";
							$studentIdList = implode(',', $studentIdArr);
							$detailsLink = "javascript:submitSearchDetail([{$studentIdList}]);";
							$returnRX .= "<td align='center'><a href=\"{$detailsLink}\">{$SubjectGroupName}</a></td>";
							$returnRX .= "<td align='center'>".$iPort["SubjectGroup"]."</td>";
							//$returnRX .= "<td align='center'>{$SubjectName}</td>";
							$returnRX .= "<td align='center'>{$Teachers}</td>";
							$returnRX .= "<td align='center'>".$Stats["AttemptTotal"]."</td>";
							$returnRX .= "<td align='center'>".$Stats["ImprovedTotal"]."</td>";
							$returnRX .= "<td align='center'>{$improveRate}</td>";
							$returnRX .= "</tr>";
							$rowDisplayCount++;
						}
						$isEmpty = false;
					}
					else{
						if(!isset($isEmpty)){
							$isEmpty = true;
						}
					}
	  			}
	  		}
	  		if($rowDisplayCount == 0){
// 	  		if($isEmpty && in_array('group', $ResultType)){
	  			$returnRX .= "<tr>";
	  			$returnRX .= "<td colspan='6'>".$Lang['General']['NoRecordFound']."</td>";
	  			$returnRX .= "</tr>";
	  		}
		  	$returnRX .= "</tbody>";
		  	$returnRX .= "</table></div>";
		  	$returnRX .= '<span class="tabletextremark">';
		  	$returnRX .= $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'];
// 		  	$returnRX .= "<br />";
// 		  	$returnRX .=$ec_iPortfolio['scoreHighlightRemarks'];
		  	$returnRX .= "</span>";
		  	return $returnRX;
	  }
	  
	  function GetSubjectStatsCompareByTermDetails($AcademicYearID, $SubjectID, $FromYearTermID, $FromYearTermAssessment, $ToYearTermID, $ToYearTermAssessment, $studentIdArr)
	  {
  		global $eclass_db, $Lang, $intranet_session_language, $iPort, $ec_iPortfolio;
		
//   		#### Group Subject START ####
//   		$rs = $this->getReleatedSubjectID();
//   		$SubjectIdArr = array($SubjectID);
//   		$SubjectIdArr = array_merge($SubjectIdArr, (array)$rs[$SubjectID]);
//   		$SubjectIdList = implode("','", $SubjectIdArr);
//   		#### Group Subject END ####
  		
  		######### Get releated subject START #########
  		$temp = explode('_',$SubjectID);
  		$MainSubjectID = $temp[0];
  		$CmpSubjectID = $temp[1];
  		$cond = " AND SubjectID = '$MainSubjectID'";
  		if($CmpSubjectID=='0'){
  			$cond .= " AND SubjectComponentID IS NULL "; 
  			$isCMP = false;
  		}else{
  			$cond .= " AND SubjectComponentID = '$CmpSubjectID' ";
  			$isCMP = true;
  		}
//   		$_subjectIds = $this->getReleatedSubjectID();
//   		$subjectIdArr = array($SubjectID);
//   		foreach($_subjectIds as $parentSubj => $_subjIdArr){
//   			if(in_array($SubjectID, $_subjIdArr) || $parentSubj == $SubjectID){
//   				$subjectIdArr = $_subjIdArr;
//   				$subjectIdArr[] = $parentSubj;
//   				break;
//   			}
//   		}
//   		$SubjectIdList = implode("','", $subjectIdArr);
  		######### Get releated subject END #########
  		
  		
  		#### Get Data From DB START ####
	  	if($FromYearTermID == '0'){
	  		$FromYearTermIdSQL = "ASSR.YearTermID IS NULL";
	  		$FromTermAssessmentSQL = "ASSR.TermAssessment IS NULL";
	  		$FromYearTermID = '';
	  	}else{
	  		$FromYearTermIdSQL = "ASSR.YearTermID = '{$FromYearTermID}'";
	  		if($FromYearTermAssessment == ''){
	  			$FromTermAssessmentSQL = "ASSR.TermAssessment IS NULL";
	  		}else{
	  			$FromTermAssessmentSQL = "ASSR.TermAssessment = '{$FromYearTermAssessment}'";
	  		}
	  	}
	  	if($ToYearTermID == '0'){
	  		$ToYearTermIdSQL = "ASSR.YearTermID IS NULL";
	  		$ToTermAssessmentSQL = "ASSR.TermAssessment IS NULL";
	  		$ToYearTermID = '';
	  	}else{
	  		$ToYearTermIdSQL = "ASSR.YearTermID = '{$ToYearTermID}'";
	  		if($ToYearTermAssessment == ''){
	  			$ToTermAssessmentSQL = "ASSR.TermAssessment IS NULL";
	  		}else{
	  			$ToTermAssessmentSQL = "ASSR.TermAssessment = '{$ToYearTermAssessment}'";
	  		}
	  	}
	  	$StudentIdList = implode("','", (array)$studentIdArr);
	  	$sql = "SELECT 
			*
		FROM 
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
		WHERE
			ASSR.AcademicYearID = '{$AcademicYearID}'
		AND
			(
				({$FromYearTermIdSQL} AND {$FromTermAssessmentSQL})
			OR
				({$ToYearTermIdSQL} AND {$ToTermAssessmentSQL})
			)
			$cond
		AND
			ASSR.UserID IN ('{$StudentIdList}')
		ORDER BY ASSR.ClassName, LENGTH(ASSR.ClassNumber), ASSR.ClassNumber, ISNULL(ASSR.YearTermID), ASSR.Semester, ISNULL(ASSR.TermAssessment), ASSR.TermAssessment";
// 			$sql = "SELECT
// 			*
// 			FROM
// 			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
// 			WHERE
// 			ASSR.AcademicYearID = '{$AcademicYearID}'
// 			AND
// 			(
// 			({$FromYearTermIdSQL} AND {$FromTermAssessmentSQL})
// 			OR
// 			({$ToYearTermIdSQL} AND {$ToTermAssessmentSQL})
// 			)
// 			AND
// 			ASSR.SubjectID IN ('{$SubjectIdList}')
// 			AND
// 			ASSR.SubjectComponentID IS NULL
// 			AND
// 			ASSR.UserID IN ('{$StudentIdList}')
// 			ORDER BY ASSR.ClassName, LENGTH(ASSR.ClassNumber), ASSR.ClassNumber, ISNULL(ASSR.YearTermID), ASSR.Semester, ISNULL(ASSR.TermAssessment), ASSR.TermAssessment";
	  	$rs = $this->returnResultSet($sql);
	  	//debug_rt($rs);
	  	
	  	$studentIdArr = array();
	  	$marksArr = array();
	  	$yearClassIdArr = array();
	  	foreach($rs as $r){
	  		$marksArr[ $r['AcademicYearID'] ][ $r['YearTermID'] ][ $r['TermAssessment'] ][ $r['UserID'] ] = $r;
	  		if(!in_array($r['UserID'], $studentIdArr)){
	  			$studentIdArr[] = $r['UserID'];
	  		}
	  		$yearClassIdArr[$r['YearClassID']] = 1;
	  	}
	  	$yearClassIdArr = array_keys($yearClassIdArr);
  		#### Get Data From DB END　####
	  	
	  	
	  	#### Get SD, Mean, ClassLevel data START ####
		$SDrows = $this->GetSDandMean(array($AcademicYearID));
		
		$sql_yearclassID = implode(", ", $yearClassIdArr);
		$sql = "SELECT DISTINCT YearClassID, YearID FROm YEAR_CLASS WHERE YearClassID IN ($sql_yearclassID) ";
		$YearIDs = $this->returnResultSet($sql);
		for ($i_year=0; $i_year<sizeof($YearIDs); $i_year++)
		{
			$ClassLevelMapping[$YearIDs[$i_year]["YearClassID"]] = $YearIDs[$i_year]["YearID"];
		}
	  	#### Get SD, Mean, ClassLevel data END####
	  	
	  	
	  	#### Calculate SD, Mean, Z-score START ####
		$isAnnual = ($FromYearTermID == 0 && $FromYearTermAssessment == 0)? 1 : 0;
	  	foreach((array)$marksArr[ $AcademicYearID ][ $FromYearTermID ][ $FromYearTermAssessment ] as $sID=>$mark){
	  		$_FromYearTermID = ($FromYearTermID=='')?0:$FromYearTermID;
	  		$_FromYearTermAssessment = ($FromYearTermAssessment=='')?0:$FromYearTermAssessment;
			$FromSD = round($SDrows[$mark['SubjectID']][0][$AcademicYearID][$_FromYearTermID][$_FromYearTermAssessment][$isAnnual][0][$ClassLevelMapping[$mark["YearClassID"]]]["SD"], 2);
			$FromMean = round($SDrows[$mark['SubjectID']][0][$AcademicYearID][$_FromYearTermID][$_FromYearTermAssessment][$isAnnual][0][$ClassLevelMapping[$mark["YearClassID"]]]["MEAN"], 2);
			if($FromSD>0){
				$zScore = round(($mark['Score']-$FromMean)/$FromSD, 2);
			}else{
				$zScore = '--';
			} 		
			$marksArr[ $AcademicYearID ][ $FromYearTermID ][ $FromYearTermAssessment ][ $sID ]['StandardScore'] = $zScore;
	  	}
	  	
		$isAnnual = ($ToYearTermID == 0 && $ToYearTermAssessment == 0)? 1 : 0;
	  	foreach((array)$marksArr[ $AcademicYearID ][ $ToYearTermID ][ $ToYearTermAssessment ] as $sID=>$mark){
	  		$_ToYearTermID = ($ToYearTermID == '')?0:$ToYearTermID;
	  		$_ToYearTermAssessment = ($ToYearTermAssessment == '')?0:$ToYearTermAssessment;
			$ToSD = round($SDrows[$mark['SubjectID']][0][$AcademicYearID][$_ToYearTermID][$_ToYearTermAssessment][$isAnnual][0][$ClassLevelMapping[$mark["YearClassID"]]]["SD"], 2);
			$ToMean = round($SDrows[$mark['SubjectID']][0][$AcademicYearID][$_ToYearTermID][$_ToYearTermAssessment][$isAnnual][0][$ClassLevelMapping[$mark["YearClassID"]]]["MEAN"], 2);
			if($ToSD > 0){
	  			$zScore = round(($mark['Score']-$ToMean)/$ToSD, 2);
			}else{
				$zScore = '--';
			}
	  		$marksArr[ $AcademicYearID ][ $ToYearTermID ][ $ToYearTermAssessment ][ $sID ]['StandardScore'] = $zScore;
	  	}
	  	#### Calculate SD, Mean, Z-score END ####
	  	
	  	
	  	#### Return result START ####
	  	$result = array();
	  	foreach((array)$studentIdArr as $studentID){
	  		$markFrom = $marksArr[ $AcademicYearID ][ $FromYearTermID ][ $FromYearTermAssessment ][ $studentID ];
	  		$markTo = $marksArr[ $AcademicYearID ][ $ToYearTermID ][ $ToYearTermAssessment ][ $studentID ];
	  		$result[] = array(
	  			'from' => $markFrom,
	  			'to' => $markTo
	  		);
	  	}
	  	
		return $result;
	  }

	  function GetInternalValueAddedStat($FromYearTermID, $ToYearTermID, $YearID, $SubjectID){
	      global $eclass_db, $intranet_db, $intranet_session_language;
	      global $Lang, $ec_iPortfolio;
	  
	      # support assessments since 12 Nov 2018-01-11
	      //$FromTermAssessment=null, $ToTermAssessment=null
	      $FromTermAssessment = null;
	      $ToTermAssessment = null;
	      # 2020-01-23 Philips [2020-0110-1359-33206] - prevent same year term different assessment
	      # For $allcore use as array key
	      $FromYearTermParam = $FromYearTermID;
	      $ToYearTermParam = $ToYearTermID;
	      if (strstr($FromYearTermID, "_"))
	      {
	          $tmpArr = explode("_", $FromYearTermID);
	          $FromYearTermID = trim($tmpArr[0]);
	          $FromTermAssessment = trim($tmpArr[1]);
	      }
	      if (strstr($ToYearTermID, "_"))
	      {
	          $tmpArr = explode("_", $ToYearTermID);
	          $ToYearTermID = trim($tmpArr[0]);
	          $ToTermAssessment = trim($tmpArr[1]);
	      }
	  
	      $fromAcademicYear = new academic_year_term($FromYearTermID);
	      $fromAcademicYearID = $fromAcademicYear->AcademicYearID;
	      $toAcademicYear = new academic_year_term($ToYearTermID);
	      $toAcademicYearID = $toAcademicYear->AcademicYearID;
	  
	      $subject_class_mapping = new subject_class_mapping();
	  
	  
	    		######### Get releated subject START #########
	    		$_subjectIds = $this->getReleatedSubjectID();
	    		$subjectIdArr = array($SubjectID);
	    		foreach($_subjectIds as $parentSubj => $_subjIdArr){
	    		    if(in_array($SubjectID, $_subjIdArr) || $parentSubj == $SubjectID){
	    		        $subjectIdArr = $_subjIdArr;
	    		        $subjectIdArr[] = $parentSubj;
	    		        break;
	    		    }
	    		}
	    		$SubjectIdList = implode("','", $subjectIdArr);
	    		######### Get releated subject END #########
	  
	  
	    		######### Get all data START #########
	    		$allScore = array();
	    		$allStudentID = array();
	    		$allClassStudent = array();
	    		#### Get New Score START ####
	    			
	    		if ($ToTermAssessment!=null)
	    		{
	    		    $sql_match_assessment = "(ASSR.TermAssessment = '{$ToTermAssessment}')";
	    		} else
	    		{
	    		    $sql_match_assessment = "(ASSR.TermAssessment IS NULL OR ASSR.TermAssessment = '')";
	    		}
	  
	    		$sql = "SELECT
	    		ASSR.SubjectID,
	    		ASSR.UserID,
	    		ASSR.Score,
	    		PCH.ClassName,
	    		PCH.ClassNumber,
	    		PCH.YearClassID
	    		FROM
	    		{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
	    		INNER JOIN
	    		{$intranet_db}.PROFILE_CLASS_HISTORY PCH
	    		ON
	    		ASSR.UserID = PCH.UserID
	    		AND
	    		PCH.AcademicYearID = '{$toAcademicYearID}'
	    		AND
	    		ASSR.SubjectComponentID IS NULL
	    		AND
	    		ASSR.Score >= 0
	    		INNER JOIN
	    		{$intranet_db}.YEAR_CLASS YC
	    		ON
	    		PCH.YearClassID = YC.YearClassID
	    		AND
	    		YC.YearID = '{$YearID}'
	    		AND
	    		YC.AcademicYearID = '{$toAcademicYearID}'
	    		WHERE
	    		{$sql_match_assessment}
	    		AND
	    		ASSR.SubjectID IN ('{$SubjectIdList}')
	    		AND
	    		ASSR.AcademicYearID = '{$toAcademicYearID}'
	    		AND
	    		(
	    		YearTermID = '{$ToYearTermID}'
	    		)
	    		ORDER BY
	    		PCH.ClassName,
	    		PCH.ClassNumber
	    		";
	    		// [MC] $FromTermAssessment=null, $ToTermAssessment=null
	    		$rs = $this->returnArray($sql);
	    		foreach($rs as $r){
	    			$allScore[$ToYearTermParam][ $r['UserID'] ] = $r;
	    		    $allStudentID[] = $r['UserID'];
	    		    $allClassStudent[$r['ClassName']][$r['UserID']] = 1;
	    		    	
	    		    $toSubjectID = $r['SubjectID'];
	    		}
	    		#### Get New Score END ####
	  
	    		#### Get Old Score START ####
	    		$allStudentIdList = implode("','", $allStudentID);
	    		if ($FromTermAssessment!=null)
	    		{
	    		    $sql_match_assessment = "(ASSR.TermAssessment = '{$FromTermAssessment}')";
	    		} else
	    		{
	    		    $sql_match_assessment = "(ASSR.TermAssessment IS NULL OR ASSR.TermAssessment = '')";
	    		}
	    		$sql = "SELECT
	    		ASSR.SubjectID,
	    		ASSR.UserID,
	    		ASSR.Score,
	    		PCH.ClassName,
	    		PCH.ClassNumber,
	    		PCH.YearClassID
	    		FROM
	    		{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
	    		INNER JOIN
	    		{$intranet_db}.PROFILE_CLASS_HISTORY PCH
	    		ON
	    		ASSR.UserID = PCH.UserID
	    		AND
	    		PCH.AcademicYearID = '{$fromAcademicYearID}'
	    		AND
	    		ASSR.SubjectComponentID IS NULL
	    		AND
	    		ASSR.Score >= 0
	    		AND
	    		PCH.UserID IN ('{$allStudentIdList}')
	    		WHERE
	    		{$sql_match_assessment}
	    		AND
	    		ASSR.SubjectID IN ('{$SubjectIdList}')
	    		AND
	    		ASSR.AcademicYearID = '{$fromAcademicYearID}'
	    		AND
	    		(
	    		YearTermID = '{$FromYearTermID}'
	    		)
	    		ORDER BY
	    		PCH.ClassName,
	    		PCH.ClassNumber
	    		";
	  
	    		$rs = $this->returnArray($sql);
	    		foreach($rs as $r){
	    		    $allScore[$FromYearTermParam][ $r['UserID'] ] = $r;
	    		}
	    		#### Get Old Score END ####
	  
	  
	    		#### Remove Data if not both term contains data START ####
	    		foreach ((array)$allScore[$FromYearTermParam] as $userID=>$r){
	    			if(!$allScore[$ToYearTermParam][ $userID ]){
	    		        unset($allScore[$FromYearTermParam][ $userID ]);
	    		    }
	    		}
	    		foreach ((array)$allScore[$ToYearTermParam] as $userID=>$r){
	    		    if(!$allScore[$FromYearTermParam][ $userID ]){
	    		    	unset($allScore[$ToYearTermParam][ $userID ]);
	    		    }
	    		}
	    		#### Remove Data if not both term contains data END ####
	  
	  
	    		#### Update change class array START ####
	    		foreach ((array)$allScore[$FromYearTermParam] as $userID=>$r){
	    		    $oldClassFull = $r['ClassName'];
	    		    $oldClass = ltrim($oldClassFull, '0123456789');
	    		    $newClassFull = $allScore[$ToYearTermParam][ $r['UserID'] ]['ClassName'];
	    		    $newClass = ltrim($newClassFull, '0123456789');
	    		    if($oldClassFull != $newClassFull && $oldClass != $newClass){
	    		        $allClassStudent[ $Lang['iPortfolio']['ChangeClassCompare'] ][$r['UserID']] = 1; # Change Class Compare
	    		    }
	    		}
	    		#### Update change class array END ####
	  
	  
	    		#### Get Student Name START ####
	    		$nameField = getNameFieldByLang2().' As Name';
	    		$sql = "SELECT
	    		UserID,
	    		{$nameField}
	    		FROM
	    		{$intranet_db}.INTRANET_USER
	    		WHERE
	    		UserID IN ('{$allStudentIdList}')";
	    		$rs = $this->returnArray($sql);
	    		$allStudentName = BuildMultiKeyAssoc($rs, array('UserID') , array('Name'), $SingleValue=1);
	    		#### Get Student Name END ####
	  
	  
	    		$_lastAcademicYearID = $fromAcademicYearID;
	    		if($fromAcademicYearID == $toAcademicYearID){
	    		    $sql = "SELECT DISTINCT
	    		    AcademicYearID
	    		    FROM
	    		    {$intranet_db}.ACADEMIC_YEAR_TERM
	    		    ORDER BY
	    		    TermStart";
	    		    $rs = $this->returnVector($sql);
	    		    $_lastAcademicYearID = 0;
	    		    foreach ($rs as $_academicYearID){
	    		        if($_academicYearID == $fromAcademicYearID){
	    		            break;
	    		        }
	    		        $_lastAcademicYearID = $_academicYearID;
	    		    }
	    		}
	  
	    		$sql = "SELECT
	    		YC.YearID,
	    		PCH.UserID
	    		FROM
	    		{$intranet_db}.PROFILE_CLASS_HISTORY PCH
	    		INNER JOIN
	    		{$intranet_db}.YEAR_CLASS YC
	    		ON
	    		PCH.YearClassID = YC.YearClassID
	    		AND
	    		PCH.UserID IN ('{$allStudentIdList}')
	    		AND
	    		YC.AcademicYearID IN ('{$_lastAcademicYearID}', '{$toAcademicYearID}')";
	    		$rs = $this->returnResultSet($sql);
	    			
	    		$studentYearArr = array();
	    		foreach ($rs as $r){
	    		    $studentYearArr[$r['UserID']][] = $r['YearID'];
	    		}
	  
	    		$allRepeatStudentId = array();
	    		foreach($studentYearArr as $UserID=>$yearIds){
	    		    if($yearIds[0] == $yearIds[1]){
	    		        $allRepeatStudentId[] = $UserID;
	    		        unset($allScore[$ToYearTermParam][ $UserID ]);
	    		        unset($allScore[$FromYearTermParam][ $UserID ]);
	    		        unset($allClassStudent[ $Lang['iPortfolio']['ChangeClassCompare'] ][ $UserID ]);
	    		    }
	    		}
	    		/* */
	    		#### Get Year Repeated Student END ####
	  
	  
	    		#### Get All Subject Group START ####
	    		$subject = new subject($toSubjectID);
	    		$rs = $subject->Get_Subject_Group_List($ToYearTermID, $YearID);
	  
	    		$subjectGroupArr = array();
	    		foreach($rs as $group){
	    		    $subjectGroupArr[ $group['SubjectGroupID'] ] = $group;
	    		}
	    		$subjectGroupIdArr = array_keys($subjectGroupArr);
	  
	    		$rs = $subject_class_mapping->Get_Subject_Group_Teacher_ListArr($subjectGroupIdArr);
	    		$subjectGroupTeacher = array();
	    		foreach($rs as $SubjectGroupID => $teachers){
	    		    $str = '';
	    		    foreach($teachers as $teacher){
	    		        $str .= "{$teacher['TeacherName']}###";
	    		    }
	    		    $str = trim($str, '#');
	    		    $str = str_replace('###', ',<br />', $str);
	    		    	
	    		    $subjectGroupTeacher[$SubjectGroupID] = $str;
	    		}
	    		#### Get All Subject Group END ####
	    		######### Get all data END #########
	  
	    		######### Put Student into Subject Group START #########
	    		$rs = $subject_class_mapping->Get_Subject_Group_Student_List($subjectGroupIdArr);
	    		$groupStudentIdArr = array();
	    		$groupAssessmentRecordArr = array();
	    		$studentIdGroupIdArr = array();
	    		foreach($rs as $r){
	    		    $SubjectGroupID = $r['SubjectGroupID'];
	    		    $UserID = $r['UserID'];
	    		    if($allScore[$FromYearTermParam][$UserID] === NULL && $allScore[$ToYearTermParam][$UserID] === NULL){
	    		        continue;
	    		    }
	    		    	
	    		    $groupStudentIdArr[ $SubjectGroupID ][] = $UserID;
	    		    	
	    		    $groupAssessmentRecordArr['From'][ $SubjectGroupID ][ $UserID ] = $allScore[$FromYearTermParam][$UserID];
	    		    $groupAssessmentRecordArr['To'][ $SubjectGroupID ][ $UserID ] = $allScore[$ToYearTermParam][$UserID];
	    		    $studentIdGroupArr[$UserID] = $SubjectGroupID;
	    		}
	    		######### Put Student into Subject Group END #########
	  
	    		######### Calculate Subject Group SD START #########
	    		// 		$subjectGroupSD = array();
	    		// 		foreach($subjectGroupIdArr as $SubjectGroupID){
	    		// 			foreach( array('From', 'To') as $_ft){
	    		// 				$scoreArr = array();
	    		// 				$scores = $groupAssessmentRecordArr[$_ft][ $SubjectGroupID ];
	    		// 				foreach((array)$scores as $UserID=>$score){
	    		// 					$scoreArr[] = $score['Score'];
	    		// 				}
	    		// 				$subjectGroupSD[$_ft][ $SubjectGroupID ] = $this->CalculateSDMean($scoreArr);
	    		// 			}
	    		// 		}
	    		######### Calculate Subject Group SD END #########
	  
	  
	    		######### Calculate display result START #########
	    		#### Calculate Whole Form Summary START ####
	    		$toScoreArr = array();
	    		foreach((array)$allScore[$ToYearTermParam] as $UserID=>$score){
	    		    if(!in_array($UserID, $allRepeatStudentId)){
	    		        $toScoreArr[] = $score['Score'];
	    		    }
	    		}
	    		$toSdMean = $this->CalculateSDMean($toScoreArr);
	    		$toWholeFormAvg = number_format($toSdMean['Mean'], 2);
	    		$toWholeFormSD = number_format($toSdMean['SD'], 2);
	  
	    		$fromScoreArr = array();
	    		foreach((array)$allScore[$FromYearTermParam] as $UserID=>$score){
	    		    if(!in_array($UserID, $allRepeatStudentId)){
	    		        $fromScoreArr[] = $score['Score'];
	    		    }
	    		}
	    		$fromSdMean = $this->CalculateSDMean($fromScoreArr);
	    		$fromWholeFormAvg = number_format($fromSdMean['Mean'], 2);
	    		$fromWholeFormSD = number_format($fromSdMean['SD'], 2);
	    		#### Calculate Whole Form Summary END ####
	  
	  
	    		$result = array(
	    		'From' => array(/* SubjectGroupID => UserID => Z-score */),
	    		'To' => array(/* SubjectGroupID => UserID => Z-score */),
	    		'Diff' => array(/* SubjectGroupID => UserID => Z-score different */),
	    		'Merit' => array(/* SubjectGroupID => UserID => Z-score different merit */),
	    		);
	  
	    		#### Calculate Standard Scord START ####
	    		foreach( array('From', 'To') as $_ft){
	    		    foreach($subjectGroupIdArr as $SubjectGroupID){
	    		        $hasData = false;
	    		        foreach((array)$groupAssessmentRecordArr[ $_ft ][ $SubjectGroupID ] as $userID => $_score){
	    		            $score = $_score['Score'];
	    		            // 					$sdMean = $subjectGroupSD[$_ft][ $SubjectGroupID ];
	    		            $sdMean = ($_ft == 'From')?$fromSdMean:$toSdMean;
	    		            if($sdMean['Count'] == 0){
	    		                $result[$_ft][$SubjectGroupID][$userID] = NULL;
	    		            }else{
	    		                $hasData = true;
	    		                if($sdMean['SD']){
	    		                    $result[$_ft][$SubjectGroupID][$userID] = ( $score - $sdMean['Mean'] ) / $sdMean['SD'];
	    		                }else{
	    		                    $result[$_ft][$SubjectGroupID][$userID] = '';
	    		                }
	    		            }
	    		        }
	    		        if(!$hasData){ // Clear all user if there are no marks in this subject group
	    		            unset($result[$_ft][$SubjectGroupID]);
	    		        }
	    		    }
	    		}
	    		#### Calculate Standard Scord END ####
	  
	    		#### Calculate Standard Scord Different START ####
	    		$diffArr = array();
	    		foreach($groupStudentIdArr as $SubjectGroupID => $users){
	    		    foreach($users as $UserID){
	    		        $fromStandardScore = $result['From'][$SubjectGroupID][$UserID];
	    		        $toStandardScore = $result['To'][$SubjectGroupID][$UserID];
	    		        if(is_null($fromStandardScore) || is_null($toStandardScore)){
	    		            $result['Diff'][$SubjectGroupID][$UserID] = NULL;
	    		        }else{
	    		            $diff = strval($toStandardScore - $fromStandardScore);
	    		            $result['Diff'][$SubjectGroupID][$UserID] = $diff;
	    		            $diffArr[$SubjectGroupID][] = $diff;
	    		        }
	    		    }
	    		    if(is_array($diffArr[$SubjectGroupID])){
	    		        rsort($diffArr[$SubjectGroupID]);
	    		    }
	    		}
	    		#### Calculate Standard Scord Different END ####
	  
	    		#### Calculate Merit START ####
	    		foreach($result['Diff'] as $SubjectGroupID => $score){
	    		    foreach($score as $UserID => $diff){
	    		        if(is_null($diff)){
	    		            $result['Merit'][$SubjectGroupID][$UserID] = NULL;
	    		        }else{
	    		            $result['Merit'][$SubjectGroupID][$UserID] = array_search($diff, $diffArr[$SubjectGroupID]) + 1;
	    		        }
	    		    }
	    		}
	    		#### Calculate Merit END ####
	  
	    		#### Calculate Subject Group Summary START ####
	    		$diffAvg = array();
	    		$newStandardScoreArr = array();
	    		$gradeArr = array();
	    		$smallGradeArr = array();
	    		foreach($groupStudentIdArr as $SubjectGroupID => $users){
	    		    //$tmp_str_ids .= ": ". $SubjectGroupID;
	    		    if(count($result['To'][$SubjectGroupID]) == 0){
	    		        $diffAvg[$SubjectGroupID] = NULL;
	    		    }else{
	    		        $avg = array_sum($diffArr[$SubjectGroupID]) / count($diffArr[$SubjectGroupID]);
	    		        $diffAvg[$SubjectGroupID] = $avg;
	    		        $newStandardScoreArr[$SubjectGroupID] = number_format($avg / sqrt(2), 2);
	    		        $gradeArr[$SubjectGroupID] = $newStandardScoreArr[$SubjectGroupID] * 2 + 5;
	  
	    		        if($gradeArr[$SubjectGroupID] >= 6){
	    		            $smallGradeArr[$SubjectGroupID] = '9+';
	    		        }else if($gradeArr[$SubjectGroupID] >= 5.77777 && $gradeArr[$SubjectGroupID] < 6){
	    		            $smallGradeArr[$SubjectGroupID] = '9';
	    		        }else if($gradeArr[$SubjectGroupID] >= 5.55555 && $gradeArr[$SubjectGroupID] < 5.77777){
	    		            $smallGradeArr[$SubjectGroupID] = '8';
	    		        }else if($gradeArr[$SubjectGroupID] >= 5.33333 && $gradeArr[$SubjectGroupID] < 5.55555){
	    		            $smallGradeArr[$SubjectGroupID] = '7';
	    		        }else if($gradeArr[$SubjectGroupID] >= 5.11111 && $gradeArr[$SubjectGroupID] < 5.33333){
	    		            $smallGradeArr[$SubjectGroupID] = '6';
	    		        }else if($gradeArr[$SubjectGroupID] >= 4.88888 && $gradeArr[$SubjectGroupID] < 5.11111){
	    		            $smallGradeArr[$SubjectGroupID] = '5';
	    		        }else if($gradeArr[$SubjectGroupID] >= 4.66666 && $gradeArr[$SubjectGroupID] < 4.88888){
	    		            $smallGradeArr[$SubjectGroupID] = '4';
	    		        }else if($gradeArr[$SubjectGroupID] >= 4.44444 && $gradeArr[$SubjectGroupID] < 4.66666){
	    		            $smallGradeArr[$SubjectGroupID] = '3';
	    		        }else if($gradeArr[$SubjectGroupID] >= 4.22222 && $gradeArr[$SubjectGroupID] < 4.44444){
	    		            $smallGradeArr[$SubjectGroupID] = '2';
	    		        }else if($gradeArr[$SubjectGroupID] >= 4.0 && $gradeArr[$SubjectGroupID] < 4.22222){
	    		            $smallGradeArr[$SubjectGroupID] = '1';
	    		        }else{
	    		            $smallGradeArr[$SubjectGroupID] = '1-';
	    		        }
	    		        //debug_rt($smallGradeArr);
	    		        //debug_r($smallGradeArr);
	    		        $smallGradeArr_Used = $smallGradeArr;
	    		    }
	    		}
	    		#### Calculate Subject Group Summary END ####
	    		######### Calculate display result END #########
	  
	  
	    		######### Display Result START #########
	    		#### Check Data exists START ####
	    		if(count($result['To']) == 0){
	    		    return "<p align='center'>{$Lang['General']['NoRecordFound']}</p>";
	    		}
	    		#### Check Data exists END ####
	  
	    		#### Display Tabs START ####
	    		$selected = ' class="selected"';
	    		$x = '<div class="shadetabs" style="margin: 0 auto; width: 95%;">';
	    		$x .= '<ul id="reportTab">';
	    		foreach($result['To'] as $SubjectGroupID=>$score){
	    		    $subjectGroup = $subjectGroupArr[$SubjectGroupID];
	    		    $name = Get_Lang_Selection($subjectGroup['ClassTitleB5'],$subjectGroup['ClassTitleEN']);
	    		    $x .= "<li {$selected}>";
	    		    $x .= "<a href=\"javascript:void(0);\" id=\"Group_{$SubjectGroupID}\"><strong>{$name}</strong></a>";
	    		    $x .= '</li>';
	    		    $selected = '';
	    		}
	    		$x .= '<li>';
	    		$x .= "<a href=\"javascript:void(0);\" id=\"Group_0\"><strong>{$Lang['iPortfolio']['ChangeClassCompare']}</strong></a>";
	    		$x .= '</li>';
	    		$x .= '</ul>';
	    		$x .= '</div><br />';
	    		#### Display Tabs END ####
	  
	    		#### Display Content START ####
	    		$display = 'display:block;';
	    		$yearNameField = Get_Lang_Selection('YearNameB5','YearNameEN');
	    		$yearTermNameField = Get_Lang_Selection('YearTermNameB5','YearTermNameEN');
	  
	    		if ($FromTermAssessment!=null)
	    		{
	    		    $fromTermDisplayTitle = $FromTermAssessment;
	    		} else
	    		{
	    		    $fromTermDisplayTitle = $fromAcademicYear->$yearTermNameField;
	    		}
	  
	    		if ($ToTermAssessment!=null)
	    		{
	    		    $toTermDisplayTitle = $ToTermAssessment;
	    		} else
	    		{
	    		    $toTermDisplayTitle = $toAcademicYear->$yearTermNameField;
	    		}
	  
	    		## Display class table START ##
	    		foreach($result['To'] as $SubjectGroupID=>$score){
	    		    $subjectGroup = $subjectGroupArr[$SubjectGroupID];
	    		    $name = Get_Lang_Selection($subjectGroup['ClassTitleB5'],$subjectGroup['ClassTitleEN']);
	    		    	
	    		    $x .= "<div id=\"reportGroup_{$SubjectGroupID}\" class=\"chart_tables\" style=\"margin: 0 auto; width: 95%; {$display}\">";
	    		    $x .= '<table class="common_table_list_v30 view_table_list_v30">';
	    		    	
	    		    ## Display Header START ##
	    		    $x .= '<tr>';
	    		    $x .= "<th style=\"width:5%\">{$Lang['Header']['Menu']['Class']}</th>";
	    		    $x .= "<th style=\"width:5%\">{$Lang['General']['ClassNumber']}</th>";
	    		    $x .= "<th style=\"width:10%\">{$Lang['SysMgr']['FormClassMapping']['StudentName']}</th>";
	    		    $x .= "<th style=\"width:10%\">{$fromAcademicYear->$yearNameField} {$fromTermDisplayTitle}</th>";
	    		    $x .= "<th style=\"width:10%\">{$ec_iPortfolio['stand_score']}</th>";
	    		    $x .= "<th style=\"width:10%\">{$toAcademicYear->$yearNameField} {$toTermDisplayTitle}</th>";
	    		    $x .= "<th style=\"width:10%\">{$ec_iPortfolio['stand_score']}</th>";
	    		    $x .= "<th style=\"width:10%\">{$Lang['iPortfolio']['StandardScoreDiff']}</th>";
	    		    $x .= "<th style=\"width:10%\">{$Lang['iPortfolio']['increaseRank']}</th>";
	    		    $x .= "<th style=\"width:10%\">&nbsp;</th>";
	    		    $x .= '</tr>';
	    		    ## Display Header END ##
	    		    	
	    		    ## Display Score Data START ##
	    		    if(count($score) == 0){
	    		        $x .= "<tr><td colspan=\"99\" align=\"center\">{$Lang['General']['NoRecordFound']}</td></tr>";
	    		        $x .= '</table>';
	    		        $x .= '</div>';
	    		        $display = 'display:none;';
	    		        continue;
	    		    }
	    		    $trClass = '';
	    		    $overallGroupSdAvg = number_format($diffAvg[$SubjectGroupID], 2);
	    		    $overallGroupNewSd = number_format($newStandardScoreArr[$SubjectGroupID], 2);
	    		    $overallGroupGrade = number_format($gradeArr[$SubjectGroupID], 2);
	    		    	
	    		    //$smallGrade = $smallGradeArr[$SubjectGroupID];
	    		    	
	    		    $smallGrade = $smallGradeArr[$SubjectGroupID];
	    		    //debug_r($tmp_str_ids);
	    		    //debug_r($tmp_str_ids2);
	    		    //debug_r($smallGradeArr[$SubjectGroupID]);
	    		    foreach($score as $UserID => $_){
	    		        $studentName = $allStudentName[$UserID];
	    		        $className = $groupAssessmentRecordArr['To'][ $SubjectGroupID ][ $UserID ]['ClassName'];
	    		        $classNumber = $groupAssessmentRecordArr['To'][ $SubjectGroupID ][ $UserID ]['ClassNumber'];
	  
	    		        $toScore = $groupAssessmentRecordArr['To'][ $SubjectGroupID ][ $UserID ]['Score'];
	    		        if($toScore !== NULL){
	    		            $toZScore = number_format($result['To'][ $SubjectGroupID ][ $UserID ], 2);
	    		        }
	  
	    		        $fromScore = $groupAssessmentRecordArr['From'][ $SubjectGroupID ][ $UserID ]['Score'];
	    		        if($fromScore !== NULL){
	    		            $fromZScore = number_format($result['From'][ $SubjectGroupID ][ $UserID ], 2);
	    		        }
	  
	    		        $diffZScore = number_format($result['Diff'][ $SubjectGroupID ][ $UserID ], 2);
	    		        $zScoreMerit = $result['Merit'][ $SubjectGroupID ][ $UserID ];
	  
	    		        $x .= '<tr class="' . $trClass . '">';
	  
	    		        $x .= "<td>{$className} <span style=\"display:none;\">UserID: {$UserID}</span></td>";
	    		        $x .= "<td>{$classNumber}</td>";
	    		        $x .= "<td>{$studentName}</td>";
	  
	    		        $x .= '<td style="text-align:center;">';
	    		        $x .= ($fromScore !== NULL)?$fromScore:'--';
	    		        $x .= '</td>';
	  
	    		        $x .= '<td style="text-align:center;">';
	    		        $x .= ($fromZScore)?$fromZScore:'--';
	    		        $x .= '</td>';
	  
	    		        $x .= '<td style="text-align:center;">';
	    		        $x .= (in_array($UserID, $allRepeatStudentId))?'<span class="repeatedStudent" style="display:none; color:red;">(REPEATED)</span>':'';
	    		        $x .= ($toScore !== NULL)?$toScore:'--';
	    		        $x .= '</td>';
	  
	    		        $x .= '<td style="text-align:center;">';
	    		        $x .= ($toZScore)?$toZScore:'--';
	    		        $x .= '</td>';
	  
	    		        $x .= '<td style="text-align:center;">';
	    		        $x .= ($diffZScore)?$diffZScore:'--';
	    		        $x .= '</td>';
	  
	    		        $x .= '<td style="text-align:center;">';
	    		        $x .= ($zScoreMerit)?$zScoreMerit:'--';
	    		        $x .= '</td>';
	  
	    		        $x .= '<td>&nbsp;</td>';
	  
	    		        $x .= '</tr>';
	  
	    		        $trClass = ($trClass)?'':'row_waiting';
	    		    }
	    		    ## Display Score Data END ##
	    		    	
	    		    	
	    		    ## Display Summary Data START ##
	    		    $x .= '<tr><td colspan="99" align="center">&nbsp;</td></tr>';
	    		    	
	    		    $x .= '<tr>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td>{$Lang['iPortfolio']['LevelAvageScore']}</td>";
	    		    $x .= "<td style=\"text-align:center;\">{$fromWholeFormAvg}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td style=\"text-align:center;\">{$toWholeFormAvg}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td>{$Lang['iPortfolio']['Avg']}</td>";
	    		    $x .= "<td>{$Lang['iPortfolio']['NewStandardScore']}</td>";
	    		    $x .= "<td>{$Lang['General']['Grade']}</td>";
	    		    $x .= '</tr>';
	    		    	
	    		    $x .= '<tr>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td>{$Lang['iPortfolio']['LevelAvageSD']}</td>";
	    		    $x .= "<td style=\"text-align:center;\">{$fromWholeFormSD}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td style=\"text-align:center;\">{$toWholeFormSD}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td style=\"text-align:center;\">{$overallGroupSdAvg}</td>";
	    		    $x .= "<td style=\"text-align:center;\">{$overallGroupNewSd}</td>";
	    		    $x .= "<td style=\"text-align:center;\">{$overallGroupGrade}</td>";
	    		    $x .= '</tr>';
	    		    	
	    		    $x .= '<tr><td colspan="99" align="center">&nbsp;</td></tr>';
	    		    	
	    		    $x .= '<tr>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td rowspan="2">';
	    		    $x .= $subjectGroupTeacher[$SubjectGroupID];
	    		    $x .= '</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td>{$Lang['iPortfolio']['SmallGrade']}</td>";
	    		    $x .= "<td colspan=\"2\">{$Lang['iPortfolio']['SmallGradeDetails']}</td>";
	    		    $x .= '</tr>';
	    		    	
	    		    $x .= '<tr>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td style=\"text-align:center;\">{$smallGrade}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '</tr>';
	    		    ## Display Summary Data END ##
	    		    	
	    		    $x .= '</table>';
	    		    $x .= '</div>';
	    		    $display = 'display:none;';
	    		}
	    		## Display class table END ##
	  
	  
	    		## Display Change Class Table START ##
	    		$x .= "<div id=\"reportGroup_0\" class=\"chart_tables\" style=\"margin: 0 auto; width: 95%; {$display}\">";
	    		$x .= '<table class="common_table_list_v30 view_table_list_v30">';
	  
	    		$x .= '<tr>';
	    		$x .= "<th style=\"width:5%\">{$Lang['Header']['Menu']['Class']}</th>";
	    		$x .= "<th style=\"width:5%\">{$Lang['General']['ClassNumber']}</th>";
	    		$x .= "<th style=\"width:10%\">{$Lang['SysMgr']['FormClassMapping']['StudentName']}</th>";
	    		$x .= "<th style=\"width:10%\">{$fromAcademicYear->$yearNameField} {$fromAcademicYear->$yearTermNameField}</th>";
	    		$x .= "<th style=\"width:10%\">{$ec_iPortfolio['stand_score']}</th>";
	    		$x .= "<th style=\"width:10%\">{$toAcademicYear->$yearNameField} {$toAcademicYear->$yearTermNameField}</th>";
	    		$x .= "<th style=\"width:10%\">{$ec_iPortfolio['stand_score']}</th>";
	    		$x .= "<th style=\"width:10%\">{$Lang['iPortfolio']['StandardScoreDiff']}</th>";
	    		$x .= "<th style=\"width:10%\">{$Lang['iPortfolio']['oldClass']}</th>";
	    		$x .= "<th style=\"width:10%\">&nbsp;</th>";
	    		$x .= '</tr>';
	  
	    		$hasData = false;
	    		$changeClassDiffZScore = array();
	    		$trClass = '';
	    		foreach($allScore[$ToYearTermParam] as $UserID=>$score){
	    		    $className = $score['ClassName'];
	    		    $classNameTrim = ltrim($className, '0123456789');
	    		    $oldClass = $allScore[$FromYearTermParam][ $UserID ]['ClassName'];
	    		    $oldClassNameTrim = ltrim($oldClass, '0123456789');
	    		    	
	    		    $SubjectGroupID = $studentIdGroupArr[$UserID];
	    		    $classNumber = $score['ClassNumber'];
	    		    $studentName = $allStudentName[$UserID];
	    		    	
	    		    $toScore = $groupAssessmentRecordArr['To'][ $SubjectGroupID ][ $UserID ]['Score'];
	    		    if($toScore !== NULL){
	    		        $toZScore = number_format($result['To'][ $SubjectGroupID ][ $UserID ], 2);
	    		    }
	    		    $fromScore = $groupAssessmentRecordArr['From'][ $SubjectGroupID ][ $UserID ]['Score'];
	    		    if($fromScore !== NULL){
	    		        $fromZScore = number_format($result['From'][ $SubjectGroupID ][ $UserID ], 2);
	    		    }
	    		    	
	    		    if(
	    		        in_array($UserID, $allRepeatStudentId) ||
	    		        $classNameTrim == $oldClassNameTrim ||
	    		        ($toScore == -1 && $fromScore == -1)
	    		    ){
	    		        continue;
	    		    }
	  
	    		    $diffZScore = number_format($result['Diff'][ $SubjectGroupID ][ $UserID ], 2);
	    		    $changeClassDiffZScore[] = $toZScore - $fromZScore;
	    		    	
	    		    $hasData = true;
	    		    ## Display Score Data START ##
	    		    	
	    		    $x .= '<tr class="' . $trClass . '">';
	    		    	
	    		    $x .= "<td>{$className}</td>";
	    		    $x .= "<td>{$classNumber}</td>";
	    		    $x .= "<td>{$studentName}</td>";
	    		    	
	    		    $x .= '<td style="text-align:center;">';
	    		    $x .= ($fromScore !== NULL)?$fromScore:'--';
	    		    $x .= '</td>';
	    		    	
	    		    $x .= '<td style="text-align:center;">';
	    		    $x .= ($fromZScore)?$fromZScore:'--';
	    		    $x .= '</td>';
	    		    	
	    		    $x .= '<td style="text-align:center;">';
	    		    $x .= ($toScore !== NULL)?$toScore:'--';
	    		    $x .= '</td>';
	    		    	
	    		    $x .= '<td style="text-align:center;">';
	    		    $x .= ($toZScore)?$toZScore:'--';
	    		    $x .= '</td>';
	    		    	
	    		    $x .= '<td style="text-align:center;">';
	    		    $x .= ($diffZScore)?$diffZScore:'--';
	    		    $x .= '</td>';
	    		    	
	    		    $x .= '<td style="text-align:center;">';
	    		    $x .= ($oldClass)?$oldClass:'--';
	    		    $x .= '</td>';
	    		    	
	    		    $x .= '<td>&nbsp;</td>';
	    		    	
	    		    $x .= '</tr>';
	    		    	
	    		    $trClass = ($trClass)?'':'row_waiting';
	    		    ## Display Score Data END ##
	    		}
	    		if($hasData){
	    		    ## Calculate Change Class Summary START ##
	    		    if(count($changeClassDiffZScore) == 0){
	    		        $diffAvg = NULL;
	    		    }else{
	    		        $avg = array_sum($changeClassDiffZScore) / count($changeClassDiffZScore);
	    		        $diffAvg = $avg;
	    		        $newStandardScore = number_format($avg / sqrt(2), 2);
	    		        $grade = $newStandardScore * 2 + 5;
	    		    }
	    		    $diffAvg = number_format($diffAvg, 2);
	    		    $newStandardScore = number_format($newStandardScore, 2);
	    		    $grade = number_format($grade, 2);
	  
	    		    if($grade >= 6){
	    		        $smallGrade = '9+';
	    		    }else if($grade >= 5.77777 && $grade < 6){
	    		        $smallGrade = '9';
	    		    }else if($grade >= 5.55555 && $grade < 5.77777){
	    		        $smallGrade = '8';
	    		    }else if($grade >= 5.33333 && $grade < 5.55555){
	    		        $smallGrade = '7';
	    		    }else if($grade >= 5.11111 && $grade < 5.33333){
	    		        $smallGrade = '6';
	    		    }else if($grade >= 4.88888 && $grade < 5.11111){
	    		        $smallGrade = '5';
	    		    }else if($grade >= 4.66666 && $grade < 4.88888){
	    		        $smallGrade = '4';
	    		    }else if($grade >= 4.44444 && $grade < 4.66666){
	    		        $smallGrade = '3';
	    		    }else if($grade >= 4.22222 && $grade < 4.44444){
	    		        $smallGrade = '2';
	    		    }else if($grade >= 4.0 && $grade < 4.22222){
	    		        $smallGrade = '1';
	    		    }else{
	    		        $smallGrade = '1-';
	    		    }
	    		    ## Calculate Change Class Summary END ##
	    		    	
	    		    ## Display Summary Data START ##
	    		    $x .= '<tr><td colspan="99" align="center">&nbsp;</td></tr>';
	    		    	
	    		    $x .= '<tr>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td>{$Lang['iPortfolio']['LevelAvageScore']}</td>";
	    		    $x .= "<td style=\"text-align:center;\">{$fromWholeFormAvg}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td style=\"text-align:center;\">{$toWholeFormAvg}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td>{$Lang['iPortfolio']['Avg']}</td>";
	    		    $x .= "<td>{$Lang['iPortfolio']['NewStandardScore']}</td>";
	    		    $x .= "<td>{$Lang['General']['Grade']}</td>";
	    		    $x .= '</tr>';
	    		    	
	    		    $x .= '<tr>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td>{$Lang['iPortfolio']['LevelAvageSD']}</td>";
	    		    $x .= "<td style=\"text-align:center;\">{$fromWholeFormSD}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td style=\"text-align:center;\">{$toWholeFormSD}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td style=\"text-align:center;\">{$diffAvg}</td>";
	    		    $x .= "<td style=\"text-align:center;\">{$newStandardScore}</td>";
	    		    $x .= "<td style=\"text-align:center;\">{$grade}</td>";
	    		    $x .= '</tr>';
	    		    	
	    		    $x .= '<tr><td colspan="99" align="center">&nbsp;</td></tr>';
	    		    	
	    		    $x .= '<tr>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td>{$Lang['iPortfolio']['SmallGrade']}</td>";
	    		    $x .= "<td colspan=\"2\">{$Lang['iPortfolio']['SmallGradeDetails']}</td>";
	    		    $x .= '</tr>';
	    		    	
	    		    $x .= '<tr>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= "<td style=\"text-align:center;\">{$smallGrade}</td>";
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '<td>&nbsp;</td>';
	    		    $x .= '</tr>';
	    		    ## Display Summary Data END ##
	    		}else{
	    		    $x .= "<tr><td colspan=\"99\" align=\"center\">{$Lang['General']['NoRecordFound']}</td></tr>";
	    		}
	    		$x .= '</table>';
	    		$x .= '</div>';
	    		## Display Change Class Table END ##
	    		#### Display Content END ####
	  
	    		## Change Tab JS START ##
	    		$x .= <<<HTML
	<script>
		$('#reportTab a').click(function(){
			$('#reportTab > li').removeClass('selected');
			$(this).parent().addClass('selected');
			$('.chart_tables').hide();
			$('#report' + $(this).attr('id')).show();
		});
	</script>
HTML;
	    		## Change Tab JS END ##
	    		######### Display Result END #########
	    		return $x;
	  } // End GetInternalValueAddedStat()
	  
	  /* Old Code * /
	  function OLD____GetInternalValueAddedStat($FromYearTermID, $ToYearTermID, $YearID, $SubjectID){
		global $eclass_db, $intranet_db, $intranet_session_language;
		global $Lang, $ec_iPortfolio;
		
		$fromAcademicYear = new academic_year_term($FromYearTermID);
		$fromAcademicYearID = $fromAcademicYear->AcademicYearID;
		$toAcademicYear = new academic_year_term($ToYearTermID);
		$toAcademicYearID = $toAcademicYear->AcademicYearID;
		
		######### Get all data START #########
		#### Get SD, Mean START ####
		$sql = "SELECT 
			SD,
			MEAN,
			YearTermID
		FROM 
			{$eclass_db}.ASSESSMENT_SUBJECT_SD_MEAN
		WHERE
			ClassLevelID = '{$YearID}'
		AND
			SubjectID = '{$SubjectID}'
		AND
			TermAssessment = '0'
		AND
			YearClassID = '0'
		AND
			(
				(AcademicYearID = '{$fromAcademicYearID}' AND YearTermID = '{$FromYearTermID}')
			OR
				(AcademicYearID = '{$toAcademicYearID}' AND YearTermID = '{$ToYearTermID}')
			)
		";
		$rs = $this->returnArray($sql);
		$allSdMean = BuildMultiKeyAssoc($rs, array('YearTermID') , array('SD', 'MEAN'));
		#### Get SD, Mean END ####
		
		$allScore = array();
		$allStudentID = array();
		$allClassStudent = array();
		#### Get New Score START ####
		$sql = "SELECT 
			ASSR.UserID,
			ASSR.Score,
			PCH.ClassName,
			PCH.ClassNumber
		FROM 
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
		INNER JOIN
			{$intranet_db}.PROFILE_CLASS_HISTORY PCH
		ON
			ASSR.UserID = PCH.UserID
		AND
			PCH.AcademicYearID = '{$toAcademicYearID}'
		INNER JOIN 
			{$intranet_db}.YEAR_CLASS YC
		ON
			PCH.YearClassID = YC.YearClassID
		AND
			YC.YearID = '{$YearID}'
		AND
			YC.AcademicYearID = '{$toAcademicYearID}'
		WHERE
			(ASSR.TermAssessment IS NULL OR ASSR.TermAssessment = '')
		AND
			ASSR.SubjectID = '{$SubjectID}'
		AND
			ASSR.AcademicYearID = '{$toAcademicYearID}'
		AND
			(
				YearTermID = '{$ToYearTermID}'
			)
		ORDER BY 
			PCH.ClassName,
			PCH.ClassNumber
		";
			
		$rs = $this->returnArray($sql);
		foreach($rs as $r){
			$allScore[ $ToYearTermID ][ $r['UserID'] ] = $r;
			$allStudentID[] = $r['UserID'];
			$allClassStudent[$r['ClassName']][$r['UserID']] = 1;
		}
		#### Get New Score END ####
		
		#### Get Old Score START ####
		$allStudentIdList = implode("','", $allStudentID);
		$sql = "SELECT 
			ASSR.UserID,
			ASSR.Score,
			PCH.ClassName,
			PCH.ClassNumber
		FROM 
			{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
		INNER JOIN
			{$intranet_db}.PROFILE_CLASS_HISTORY PCH
		ON
			ASSR.UserID = PCH.UserID
		AND
			PCH.AcademicYearID = '{$fromAcademicYearID}'
		AND
			PCH.UserID IN ('{$allStudentIdList}')
		WHERE
			(ASSR.TermAssessment IS NULL OR ASSR.TermAssessment = '')
		AND
			ASSR.SubjectID = '{$SubjectID}'
		AND
			ASSR.AcademicYearID = '{$fromAcademicYearID}'
		AND
			(
				YearTermID = '{$FromYearTermID}'
			)
		ORDER BY 
			PCH.ClassName,
			PCH.ClassNumber
		";

		$rs = $this->returnArray($sql);
		foreach($rs as $r){
			$allScore[ $FromYearTermID ][ $r['UserID'] ] = $r;
			$oldClass = ltrim($r['ClassName'] , '0123456789');
			$newClass = ltrim($allScore[ $ToYearTermID ][ $r['UserID'] ]['ClassName'] , '0123456789');
			if($oldClass != $newClass){
				$allClassStudent[ $Lang['iPortfolio']['ChangeClassCompare'] ][$r['UserID']] = 1; # Change Class Compare
			}
		}
		#### Get Old Score END ####
		
		#### Get All Class Student START ####
		foreach($allClassStudent as $className=>$uID){
			$allClassStudent[$className] = array_keys($allClassStudent[$className]);
		}
		#### Get All Class Student END ####
		
		#### Get All Subject Group Teacher START ####
		$scm = new subject_class_mapping();
		$Temp = $scm->Get_Subject_Group_Stat($FromYearTermID,'',$SubjectID);
		$fromSubjectGroupStat = is_array($Temp[0])? $Temp[0]:array();
		$fromSubjectGroupList = (array)$fromSubjectGroupStat[$SubjectID];
		$fromSubjectGroupID = array_keys($fromSubjectGroupList);
		$allSubjectGroupTeacherList = array();
		foreach ($fromSubjectGroupList as $SubjectGroupID => $SubjectGroupDetail) {
			$TeacherList = $scm->Get_Subject_Group_Teacher_List($SubjectGroupID);
			foreach($TeacherList as $teacher){
				$allSubjectGroupTeacherList[$SubjectGroupID][] = $teacher;
			}
		}
		
		$Temp = $scm->Get_Subject_Group_Stat($ToYearTermID,'',$SubjectID);
		$toSubjectGroupStat = is_array($Temp[0])? $Temp[0]:array();
		$toSubjectGroupList = (array)$toSubjectGroupStat[$SubjectID];
		$toSubjectGroupID = array_keys($toSubjectGroupList);
		foreach ($toSubjectGroupList as $SubjectGroupID => $SubjectGroupDetail) {
			$TeacherList = $scm->Get_Subject_Group_Teacher_List($SubjectGroupID);
			foreach($TeacherList as $teacher){
				$allSubjectGroupTeacherList[$SubjectGroupID][] = $teacher;
			}
		}
		foreach($allSubjectGroupTeacherList as $SubjectGroupID => $teacher){
			$allSubjectGroupTeacherList[$SubjectGroupID] = array_unique($allSubjectGroupTeacherList[$SubjectGroupID]);
		}
		#### Get All Subject Group Teacher END ####

		#### Get All Subject Group Class START ####
		$sql = "SELECT DISTINCT 
			stc.SubjectGroupID,
			stcu.UserID,
			st.YearTermID
		FROM
			SUBJECT_TERM_CLASS_YEAR_RELATION stcyr 
		INNER JOIN
			SUBJECT_TERM st
		ON
			st.SubjectGroupID=stcyr.SubjectGroupID
		INNER JOIN
			SUBJECT_TERM_CLASS stc 
		ON 
			stc.SubjectGroupID=st.SubjectGroupID
		INNER JOIN
			SUBJECT_TERM_CLASS_USER stcu
		ON
			stc.SubjectGroupID = stcu.SubjectGroupID
		AND
			UserID IN ('{$allStudentIdList}')
		WHERE
			stcyr.YearID='{$YearID}'
		AND 
			st.YearTermID IN ('{$FromYearTermID}', '{$ToYearTermID}') 
		AND 
			st.SubjectID='{$SubjectID}'";
		$rs = $this->returnArray($sql);
		$allStudentSubjectGroup = array();
		$allClassSubjectGroup = array();
		foreach($rs as $r){
			$allStudentSubjectGroup[ $r['UserID'] ][ $r['YearTermID'] ] = $r['SubjectGroupID'];
		}
		foreach($allClassStudent as $class=>$d2){
			foreach($d2 as $uID){
				$allClassSubjectGroup[$class][$FromYearTermID][] = $allStudentSubjectGroup[$uID][$FromYearTermID];
				$allClassSubjectGroup[$class][$ToYearTermID][] = $allStudentSubjectGroup[$uID][$ToYearTermID];
			}
		}
		#### Get All Subject Group Class END ####

		#### Get Student Info START ####
		$nameField = getNameFieldByLang2().' As Name';
		$sql = "SELECT 
			UserID,
			{$nameField}
		FROM 
			INTRANET_USER
		WHERE 
			UserID IN ('{$allStudentIdList}')";
		$rs = $this->returnArray($sql);
		$allStudentName = BuildMultiKeyAssoc($rs, array('UserID') , array('Name'), $SingleValue=1);
		#### Get Student Info END ####
		
		########## Check has data START ##########
		if(count($allScore) == 0){
			echo "<p align='center'>{$Lang['General']['NoRecordFound']}</p>";
			return;
		}
		########## Check has data END ##########
		
		#### Get Class Info START ####
		$allClass = array();
		foreach($allScore[$ToYearTermID] as $uID=>$studentInfo){
			$allClass[ $studentInfo['ClassName'] ] = 1;
		}
		$allClass[ $Lang['iPortfolio']['ChangeClassCompare'] ] = 1; # Change Class Compare
		$allClass = array_keys($allClass);
		#### Get Class Info END ####
		
		#### Calculate Standard Score START ####
		foreach($allScore as $yearTermID=>$d2){
			foreach($d2 as $uID=>$d3){
				$score = $d3['Score'];
				$sd = $allSdMean[$yearTermID]['SD'];
				$mean = $allSdMean[$yearTermID]['MEAN'];
				if($sd == 0){
					$allScore[$yearTermID][$uID]['StandardScore'] = null;
				}else{
					$allScore[$yearTermID][$uID]['StandardScore'] = ($score - $mean) / $sd;
				}
			}
		}
		#### Calculate Standard Score END ####
		
		#### Calculate Standard Score Difference START ####
		$diffArr = array();
		foreach($allScore[$ToYearTermID] as $uID=>$studentInfo){
			$diff = number_format($studentInfo['StandardScore'] - $allScore[$FromYearTermID][$uID]['StandardScore'], 2);
			$allScore[$ToYearTermID][$uID]['StandardScoreDiff'] = $diff;
			if($studentInfo['StandardScore'] !== null && $allScore[$FromYearTermID][$uID]['StandardScore'] !== null){
				$diffArr[$studentInfo['ClassName']][] = $diff;
			}
			
			// For change class table
			$oldClass = ltrim($allScore[ $FromYearTermID ][ $uID ]['ClassName'] , '0123456789');
			$newClass = ltrim($studentInfo['ClassName'] , '0123456789');
			if($oldClass != $newClass){
				$diffArr[ $Lang['iPortfolio']['ChangeClassCompare'] ][] = $diff;
			}
		}
		#### Calculate Standard Score Difference END ####
		
		$allSummary = array();
		foreach($diffArr as $className=>$diff){
			#### Calculate rank START ####
			/*
			 * $rankArr = array(
			 * 		score => rank,
			 * 		score => rank,
			 * 		...
			 * )
			 * /
			array_multisort($diff);
			$rankArr = array($diff[0]=>1);
			$rank = 2;
			for($i=1,$iMax = count($diff);$i<$iMax;$i++){
				# if standard score == last standard score, they are in same rank
				if($diff[$i] == $diff[$i-1]){ 
					$rankArr[$diff[$i]] = $rankArr[$diff[$i-1]]; // same rank with last rank
				}else{
					$rankArr[$diff[$i]] = $rank;
				}
				$rank++;
			}
			#### Calculate rank END ####
				
			#### Calculate diff average START ####
			$sum = array_sum($diff);
			$diffAvg = 0;
			if(count($diff)){
				$diffAvg = number_format($sum / count($diff), 2);
			}
			#### Calculate diff average END ####
			
			#### Calculate New Standard Score / Grade START ####
			$newStandardScore = number_format($diffAvg / sqrt(2), 2);
			$grade = $newStandardScore * 2 + 5;
			#### Calculate New Standard Score / Grade END ####
			
			#### Calculate Small Grade START ####
			if($grade >= 6){
				$smallGrade = '9+';
			}else if($grade >= 5.77777 && $grade < 6){
				$smallGrade = '9';
			}else if($grade >= 5.55555 && $grade < 5.77777){
				$smallGrade = '8';
			}else if($grade >= 5.33333 && $grade < 5.55555){
				$smallGrade = '7';
			}else if($grade >= 5.11111 && $grade < 5.33333){
				$smallGrade = '6';
			}else if($grade >= 4.88888 && $grade < 5.11111){
				$smallGrade = '5';
			}else if($grade >= 4.66666 && $grade < 4.88888){
				$smallGrade = '4';
			}else if($grade >= 4.44444 && $grade < 4.66666){
				$smallGrade = '3';
			}else if($grade >= 4.22222 && $grade < 4.44444){
				$smallGrade = '2';
			}else if($grade >= 4.0 && $grade < 4.22222){
				$smallGrade = '1';
			}else{
				$smallGrade = '1-';
			}
			#### Calculate Small Grade END ####
			$allSummary[$className]['RankArr'] = $rankArr;
			$allSummary[$className]['DiffAvg'] = $diffAvg;
			$allSummary[$className]['Grade'] = $grade;
			$allSummary[$className]['SmallGrade'] = $smallGrade;
			$allSummary[$className]['NewStandardScore'] = $newStandardScore;
		}
		######### Get all data END #########
		
		
		######### Display data START #########
		$x = '<div class="shadetabs" style="margin: 0 auto; width: 95%;">';
		$x .= '<ul id="reportTab">';
		foreach($allClass as $index=>$class){
			if($index == 0){
				$x .= '<li class="selected">';
				$x .= "<a href=\"javascript:void(0);\" id=\"Class_{$index}\"><strong>{$class}</strong></a>";
				$x .= '</li>';
			}else{
				$x .= '<li>';
				$x .= "<a href=\"javascript:void(0);\" id=\"Class_{$index}\"><strong>{$class}</strong></a>";
				$x .= '</li>';
			}
		}
//		$x .= '<li>';
//		$x .= "<a href=\"#\" id=\"change_class\">{$Lang['iPortfolio']['ChangeClassCompare']}</a>";
//		$x .= '</li>';
		$x .= '</ul>';
		$x .= '</div><br />';

		$yearNameField = Get_Lang_Selection('YearNameB5','YearNameEN');
		$yearTermNameField = Get_Lang_Selection('YearTermNameB5','YearTermNameEN');
		
		foreach($allClass as $index=>$class){
			if($index > 0){
				$hideDiv = 'display: none;';
			}
			$x .= "<div id=\"reportClass_{$index}\" class=\"reportDiv\" style=\"margin: 0 auto; width: 95%; {$hideDiv}\">";
			############ Display class table START ############
			$x .= '<table class="common_table_list_v30 view_table_list_v30">';
			$x .= '<tr>';
			$x .= "<th>{$Lang['Header']['Menu']['Class']}</th>";
			$x .= "<th>{$Lang['General']['ClassNumber']}</th>";
			$x .= "<th>{$Lang['SysMgr']['FormClassMapping']['StudentName']}</th>";
			$x .= "<th>{$fromAcademicYear->$yearNameField} {$fromAcademicYear->$yearTermNameField}</th>";
			$x .= "<th>{$ec_iPortfolio['stand_score']}</th>";
			$x .= "<th>{$toAcademicYear->$yearNameField} {$toAcademicYear->$yearTermNameField}</th>";
			$x .= "<th>{$ec_iPortfolio['stand_score']}</th>";
			$x .= "<th>{$Lang['iPortfolio']['StandardScoreDiff']}</th>";
			if($class == $Lang['iPortfolio']['ChangeClassCompare']){
				$x .= "<th>{$Lang['iPortfolio']['oldClass']}</th>";
			}else{
				$x .= "<th>{$Lang['iPortfolio']['increaseRank']}</th>";
			}
			$x .= "<th>&nbsp;</th>";
			$x .= '</tr>';

			#### Display class data START ####
			if(empty($allClassStudent[$class])){
				$x .= "<tr><td colspan=\"10\" align=\"center\">{$Lang['General']['NoRecordFound']}</td></tr>";
			}else{
				foreach((array)$allClassStudent[$class] as $uID){
					$new = $allScore[$ToYearTermID][$uID];
	//				foreach($allScore[ $ToYearTermID ][$uID] as $score){
						$old = $allScore[$FromYearTermID][$uID];
						$x .= '<tr>';
						$x .= "<td>{$new['ClassName']}</td>";
						$x .= "<td>{$new['ClassNumber']}</td>";
						$x .= "<td>{$allStudentName[$uID]}</td>";
						if($old['Score'] === NULL){
							$x .= "<td>--</td>";
						}else{
							$x .= "<td>{$old['Score']}</td>";
						}
						if($old['StandardScore'] === NULL){
							$x .= "<td>--</td>";
						}else{
							$oldStandardScore = number_format($old['StandardScore'], 2);
							$x .= "<td>{$oldStandardScore}</td>";
						}
						if($new['Score'] === NULL){
							$x .= "<td>--</td>";
						}else{
							$x .= "<td>{$new['Score']}</td>";
						}
						if($new['StandardScore'] === NULL){
							$x .= "<td>--</td>";
						}else{
							$newStandardScore = number_format($new['StandardScore'], 2);
							$x .= "<td>{$newStandardScore}</td>";
						}
						if($old['StandardScore'] === NULL || $new['StandardScore'] === NULL){
							$x .= "<td>--</td>";
						}else{
							$x .= "<td>{$new['StandardScoreDiff']}</td>";
						}
						if($class == $Lang['iPortfolio']['ChangeClassCompare']){
							$x .= "<td>{$old['ClassName']}</td>";
						}else{
							if($old['StandardScore'] === NULL || $new['StandardScore'] === NULL){
								$x .= "<td>--</td>";
							}else{
								$x .= "<td>{$allSummary[$class]['RankArr'][ $new['StandardScoreDiff'] ]}</td>";
							}
						}
						$x .= "<td>&nbsp;</td>";
						$x .= '</tr>';
	//				}
				}

				$x .= '<tr><td colspan="10">&nbsp;</td></tr>';
				$x .= '<tr>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= "<td>{$Lang['iPortfolio']['LevelAvageScore']}</td>";
				$x .= "<td>{$allSdMean[$FromYearTermID]['MEAN']}</td>";
				$x .= '<td>&nbsp;</td>';
				$x .= "<td>{$allSdMean[$ToYearTermID]['MEAN']}</td>";
				$x .= '<td>&nbsp;</td>';
				$x .= "<td>{$Lang['iPortfolio']['Avg']}</td>";
				$x .= "<td>{$Lang['iPortfolio']['NewStandardScore']}</td>";
				$x .= "<td>{$Lang['General']['Grade']}</td>";
				$x .= '</tr>';
				
				$x .= '<tr>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= "<td>{$Lang['iPortfolio']['LevelAvageSD']}</td>";
				$x .= "<td>{$allSdMean[$FromYearTermID]['SD']}</td>";
				$x .= '<td>&nbsp;</td>';
				$x .= "<td>{$allSdMean[$ToYearTermID]['SD']}</td>";
				$x .= '<td>&nbsp;</td>';
				$x .= "<td>{$allSummary[$class]['DiffAvg']}</td>";
				$x .= "<td>{$allSummary[$class]['NewStandardScore']}</td>";
				$x .= "<td>{$allSummary[$class]['Grade']}</td>";
				$x .= '</tr>';
				
				$x .= '<tr><td colspan="10">&nbsp;</td></tr>';
				$x .= '<tr>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				if($class == $Lang['iPortfolio']['ChangeClassCompare']){
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
				}else{
					$subjectGroupID = $allClassSubjectGroup[$class][$FromYearTermID];
					$subjectGroupID = array_unique($subjectGroupID);
					$teacherList = '';
					foreach($subjectGroupID as $subjID){
						foreach((array)$allSubjectGroupTeacherList[$subjID] as $teacher){
							$teacherList .= $teacher['TeacherName'] . ',<br />';
						}
					}
					$teacherList = substr($teacherList, 0, -7);
					$x .= "<td>{$teacherList}</td>";
					$x .= '<td>&nbsp;</td>';
					$subjectGroupID = $allClassSubjectGroup[$class][$ToYearTermID];
					$subjectGroupID = array_unique($subjectGroupID);
					$teacherList = '';
					foreach($subjectGroupID as $subjID){
						foreach((array)$allSubjectGroupTeacherList[$subjID] as $teacher){
							$teacherList .= $teacher['TeacherName'] . ',<br />';
						}
					}
					$teacherList = substr($teacherList, 0, -7);
					$x .= "<td>{$teacherList}</td>";
				}
				$x .= '<td>&nbsp;</td>';
				$x .= "<td>{$Lang['iPortfolio']['SmallGrade']}</td>";
				$x .= "<td>{$Lang['iPortfolio']['SmallGradeDetails']}</td>";
				$x .= '<td>&nbsp;</td>';
				$x .= '</tr>';
				
				$x .= '<tr>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= "<td>{$allSummary[$class]['SmallGrade']}</td>";
				$x .= '<td>&nbsp;</td>';
				$x .= '<td>&nbsp;</td>';
				$x .= '</tr>';
			}
			#### Display class data END ####
			$x .= '</table>';
			############ Display class table END ############
			$x .= '</div>';
		}
		
		$x .= <<<HTML
	<script>
		$('#reportTab a').click(function(){
			$('#reportTab > li').removeClass('selected');
			$(this).parent().addClass('selected');
			$('.reportDiv').hide();
			$('#report' + $(this).attr('id')).show();
		});
	</script>
HTML;
		return $x;
	  }/* */
	  
	  function GetTeacherNames($TeacherList, $separator = ', ')
	  {
	  	 $DisplayedFlag = array();
	  	 for ($i=0; $i<sizeof($TeacherList); $i++)
	  	 {
	  	 	if (!$DisplayedFlag[$TeacherList[$i]])
	  	 	{
	  	 		$ReturnRX .= (($ReturnRX!="") ? $separator : "") . $TeacherList[$i];
	  	 		$DisplayedFlag[$TeacherList[$i]] = true;
	  	 	}
	  	 }
	  	 
	  	 return $ReturnRX;
	  }
	  
	  
	  function Get_Term_Assessment_Selection($AcademicYearID, $showOnlyHasRecord = false, $showOverAll = false)
	  {
	  		global $eclass_db, $intranet_db, $ec_iPortfolio;
	  		
	  		
	  		$lib_academic_year = new academic_year($AcademicYearID);
	  		$TermArr = ($lib_academic_year->Get_Term_List());
	  		
	  		# get assessment and term in order
			$sql = "SELECT DISTINCT 
				assr.AcademicYearID, 
				assr.YearTermID, 
				assr.TermAssessment  
			FROM 
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD assr
			LEFT JOIN
				{$intranet_db}.ACADEMIC_YEAR_TERM ayt
			ON
				assr.YearTermID = ayt.YearTermID
			WHERE 
				assr.AcademicYearID='{$AcademicYearID}' 
			ORDER BY
				ayt.YearTermID IS NULL, ayt.TermStart, assr.TermAssessment IS NULL";
		  	$AcademicYearTerms = $this->returnResultSet($sql);
			
			$allYearTermID = array();
			foreach($AcademicYearTerms as $d){
				$allYearTermID[] = $d['YearTermID'];
			}
			$allYearTermID = array_unique($allYearTermID);
			
			$allTermAssessment = array();
			foreach($AcademicYearTerms as $d){
				$allTermAssessment[$d['YearTermID']][] = $d['TermAssessment'];
			}

		  	if (sizeof($TermArr)>0)
	  		{
	  			foreach ($TermArr as $TermID => $TermName)
	  			{
	  				for ($i_yesr_term=0; $i_yesr_term<sizeof($AcademicYearTerms); $i_yesr_term++)
	  				{
		  				if($showOnlyHasRecord && (!in_array($AcademicYearTerms[$i_yesr_term]["TermAssessment"], (array)$allTermAssessment[$TermID])) ){
		  					continue;
		  				}
	  					if ($AcademicYearTerms[$i_yesr_term]["YearTermID"]==$TermID && $AcademicYearTerms[$i_yesr_term]["TermAssessment"]!="")
	  					{
	  						$TermAssessmentHeaders[] = array($TermID."_".$AcademicYearTerms[$i_yesr_term]["TermAssessment"], $AcademicYearTerms[$i_yesr_term]["TermAssessment"]);
	  					}
	  				}
	  				if(!$showOnlyHasRecord || (in_array(null, (array)$allTermAssessment[$TermID], true))){
  						$TermAssessmentHeaders[] = array($TermID, $TermName);
	  				}
	  			}	  			
	  		}

			if($showOverAll){
				foreach($AcademicYearTerms as $ayt){
			  		if( ($ayt["YearTermID"] === null) && ($ayt["TermAssessment"] === null) ){
			  			$TermAssessmentHeaders[] = array(0, $ec_iPortfolio['overall_result']);
			  			break;
			  		}
				}
			}
	  	
	  		return $TermAssessmentHeaders;
	  }
	  
	  function isChildrenBelongsToParent() {
	  	global $ck_user_id, $ck_current_children_id;
	  	
	  	$childrenInfoAry = $this->GET_CHILDREN_INFO();
	  	$childrenIdAry = Get_Array_By_Key($childrenInfoAry, 'UserID');
	  	
	  	return in_array($ck_current_children_id, (array)$childrenIdAry);
	  }
	  
	  
	  // This function is to get all T1,T2... 
	  // return $allTermIndex[AcademicYearID][YearTermID] = ...
	  function getAllTermIndex(){
	  	global $intranet_db;
	  	
	  	$sql = "SELECT
			AcademicYearID,
			YearTermID
		FROM 
			{$intranet_db}.ACADEMIC_YEAR_TERM
		ORDER BY 
			TermStart";
		$rs = $this->returnResultSet($sql);
		$allTermIndex = array();
		$_lastAcademicYearID = 0;
		foreach($rs as $r){
			if($r['AcademicYearID'] != $_lastAcademicYearID){
				$_lastAcademicYearID = $r['AcademicYearID'];
				$index = 1;
			}
			$allTermIndex[$r['AcademicYearID']][$r['YearTermID']] = $index++;
		}
		return $allTermIndex;
	  }
	  
	function getAllPercentileSetting(){
		global $intranet_db;
		$sql = "SELECT
			*
		FROM
			{$intranet_db}.ASSESSMENT_PERCENTILE_SETTING
		ORDER BY
			PercentOrder";
		$rs = $this->returnResultSet($sql);
		return $rs;
	}
	
	function updateAllPercentileSetting($data){
		extract($data);
		
		$Percent = IntegerSafe($Percent);
		
		#### Pack Data to Array START ####
		$allData = array();
		for ($i = 0, $iMax = count($Percent); $i < $iMax; $i++) {
			$Title[$i] = trim( htmlspecialchars( $Title[$i] , ENT_QUOTES, 'UTF-8') );
			$allData[] = array(
				'Percent' => $Percent[$i],
				'Title' => $Title[$i],
				'PercentOrder' => $i
			);
		}
		#### Pack Data to Array END ####
		
		
		#### Remove Deleted Record START #### 
		$maxPercentOrder = count($allData) - 1;
		$sql = "DELETE FROM ASSESSMENT_PERCENTILE_SETTING WHERE PercentOrder > '{$maxPercentOrder}'";
		$result[] = $this->db_db_query($sql);
		#### Remove Deleted Record END #### 
		
		
		#### Insert to DB START ####
		$result = array();
		foreach($allData as $d){ // INSERT data or UPDATE the row if the PercentOrder is duplicate
			$sql = "INSERT INTO ASSESSMENT_PERCENTILE_SETTING (
				Percent,
				Title,
				PercentOrder,
				InputDate,
				Createdby
			) VALUES (
				'{$d['Percent']}',
				'{$d['Title']}',
				'{$d['PercentOrder']}',
				NOW(),
				'{$_SESSION['UserID']}'
			)ON DUPLICATE KEY UPDATE
				Percent = '{$d['Percent']}',
				Title = '{$d['Title']}',
				ModifiedDate = NOW(),
				ModifiedBy = '{$_SESSION['UserID']}'
			";
			$result[] = $this->db_db_query($sql);
		}
		#### Insert to DB END####
		
		return $result;
	}
	
	/**
	 * Get the Student's Percentile rank
	 * @return $result[$academicYearID][$YearTermID][$TermAssessment][studentID] = [merit,OrderMeritForm,OrderMeritFormTotal]
	 */
	function getStudentPercentile($AcademicYearIdArr, $SubjectID='', $SubjectComponentID='',$StudentID = ''){
		global $intranet_db, $eclass_db;

		######## Get all Percentile Setting Start ########
		$percentileArr = $this->getAllPercentileSetting();
		######## Get all Percentile Setting END ########
		
		######## Get all score data START ########
		$AcademicYearIdList = implode("','", (array)$AcademicYearIdArr);
		
		if($SubjectID == ''){
			$sql = "SELECT
	  			ASMR.UserID,
	  			ASMR.OrderMeritForm,
	  			ASMR.OrderMeritFormTotal,
	  			ASMR.AcademicYearID,
	  			ASMR.YearTermID,
	  			ASMR.TermAssessment
			FROM
				{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD ASMR
			WHERE
				ASMR.AcademicYearID IN ('{$AcademicYearIdList}')
				{$filterUserIdSQL}
			ORDER BY 
				ASMR.AcademicYearID, ASMR.YearTermID, ASMR.TermAssessment
	  		";
		}else{
		    if($StudentID != ''){
		        $SQL = "
    		        SELECT 
                        YearClassID 
                    FROM 
                        YEAR_CLASS  
                    WHERE 
                        YearID IN (
            		        SELECT yc.YearID
            		        FROM
            		        YEAR_CLASS_USER AS ycu
            		        INNER JOIN YEAR_CLASS AS yc ON (ycu.YearClassID = yc.YearClassID)
            		        WHERE 
                            yc.AcademicYearID  IN ('{$AcademicYearIdList}') AND ycu.UserID = '$StudentID'
		                  )
		        ";
		        $YearClassIDAry = $this->returnVector($SQL);
		        $YearClassIDList = implode("','", (array)$YearClassIDAry);
		        $filterYearClassIDSQL = " AND ASSR.YearClassID IN ('{$YearClassIDList}')";
		        // 		        $filterUserIdSQL = "AND ASMR.UserID IN ('')";
		    }
			$filterSubjectIdSQL = " AND ASSR.SubjectID = '{$SubjectID}'";
			if($SubjectComponentID != ''){
				$filterSubjectComponentIdSQL = " AND ASSR.SubjectComponentID = '{$SubjectComponentID}'";
			}else{
				$filterSubjectComponentIdSQL = " AND ASSR.SubjectComponentID IS NULL";
			}
			$sql = "SELECT
				ASSR.UserID,
				ASSR.OrderMeritForm,
				ASSR.OrderMeritFormTotal,
				ASSR.AcademicYearID,
				ASSR.YearTermID,
				ASSR.TermAssessment
			FROM
				{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD ASSR
			WHERE
				ASSR.AcademicYearID IN ('{$AcademicYearIdList}')
				{$filterUserIdSQL}
				{$filterSubjectIdSQL}
				{$filterSubjectComponentIdSQL}
				{$filterYearClassIDSQL}
			ORDER BY
				ASSR.AcademicYearID, ASSR.YearTermID, ASSR.TermAssessment
			";
		}
		$rs = $this->returnResultSet($sql);
		$meritArr = BuildMultiKeyAssoc($rs, array('AcademicYearID','YearTermID','TermAssessment','UserID'), array('OrderMeritForm'), $SingleValue=1);
		$omfArr = BuildMultiKeyAssoc($rs, array('AcademicYearID','YearTermID','TermAssessment','UserID'));
		######## Get all score data END ########
// 		debug_pr($percentileArr);
		######## Calcualte Student Rank START ########
		$result = array();
		foreach($meritArr as $academicYearID => $d1){
			foreach($d1 as $YearTermID => $d2){
				foreach($d2 as $TermAssessment => $studentMeritArr){
					$studentMeritArr = array_filter($studentMeritArr);
					$meritRankArr = $this->calculateStudentPercentile($studentMeritArr);
					foreach($meritRankArr as $studentId => $rank){
						$omfDetails = $omfArr[$academicYearID][$YearTermID][$TermAssessment][$studentId];
						$result[$academicYearID][$YearTermID][$TermAssessment][$studentId] = array(
							'MeritRank' => $rank,
							'MeritRankTitle' => $percentileArr[$rank]['Title'],
							'OrderMeritForm' => $omfDetails['OrderMeritForm'],
							'OrderMeritFormTotal' => $omfDetails['OrderMeritFormTotal'],
						);
					}
				}
			}
		}
		######## Calcualte Student Rank END ########
		
		return $result;
	}
	
	/**
	 * Calculate student's percentile
	 * @param [studentID => merit] $meritArr	The merit data
	 * 
	 * @return [ studentId => meritRank(PercentOrder) ]
	 */
	function calculateStudentPercentile($meritArr, $percentileArr=array()){
		
		######## Get all Percentile Setting Start ########
		if(empty($percentileArr)){
			$setting = $this->getAllPercentileSetting();
			$percentileArr = Get_Array_By_Key($setting,'Percent');
		}
		######## Get all Percentile Setting END ########
		
		
		######## Calculate Percentile START ########
		$allUniqueMerit = array_filter(array_unique(array_values($meritArr)));
		$countUniqueData = count($allUniqueMerit);
		$sortUniqueMerit = $allUniqueMerit;
		rsort($sortUniqueMerit);
		
		#### Calc number of student for each rank START ####
		$rankStudentCount = array(); // Number of student of that rank
		foreach($percentileArr as $PercentOrder => $percentage){
			$rankStudentCount[$PercentOrder] = round($countUniqueData * $percentage / 100.0);
		}
		#### Calc number of student END ####
		
		#### Map merit to rank for each rank START ####
		$meritRankArr = array(); // Which rank for the score
		foreach($rankStudentCount as $rank => $numOfStudent){
			for ($i = 0; $i < $numOfStudent; $i++) {
				$_merit = array_pop($sortUniqueMerit);
				$meritRankArr[$_merit] = $rank;
			}
		}
		#### Map merit to rank END ####
// 		debug_pr($percentileArr);
		#### Map student to rank START ####
		$percentileResult = array();
		foreach($meritArr as $studentID => $merit){
			$rank = $meritRankArr[$merit];
			$percentileResult[$studentID] = $rank;
		}
		#### Map student to rank END ####
		######## Calculate Percentile END ########
		
		return $percentileResult;
	}
	  
	function getAssessmentStatReportAccessRight($userID = ''){
		if($userID == ''){
			$userID = $_SESSION['UserID'];
		}
		$academicYearID = Get_Current_Academic_Year_ID();
		
		$accessRight = array();
		
		######### Get access right data START #########
		$sql = "SELECT 
			RecordID,
			UserID,
			AccessType,
			SubjectID,
			YearID 
		FROM 
			ASSESSMENT_ACCESS_RIGHT 
		WHERE
			UserID = '{$userID}'";
		$rs = $this->returnResultSet($sql);
		$rs = BuildMultiKeyAssoc($rs, array('UserID', 'AccessType', 'SubjectID', 'YearID') , array('RecordID'), $SingleValue=1);
		######### Get access right data END #########
		
		
		######### Get Admin Access Right START #########
		if(count($rs[$userID]['0']) > 0 || $_SESSION["SSV_USER_ACCESS"]["other-SDAS"]){ // || $_SESSION["SSV_USER_ACCESS"]["other-SDAS"]
			$accessRight['admin'] = true;
		}
		######### Get Admin Access Right END #########
		
		
		######### Get Subject Panel Access Right START #########
		$accessRight['subjectPanel'] = $rs[$userID]['1']; // $accessRight['subjectPanel'][SubjectID][YearID]
		######### Get Subject Panel Access Right END #########
		
		
		######### Get Teaching Access Right START #########
		$sql = "SELECT DISTINCT
			ST.SubjectID
		FROM
			SUBJECT_TERM_CLASS_TEACHER STCT
		INNER JOIN
			SUBJECT_TERM ST
		ON
			STCT.SubjectGroupID = ST.SubjectGroupID
		AND
			STCT.UserID = '{$userID}'";
		$rs = $this->returnVector($sql);
		$accessRight['subjectTeacher'] = $rs;
		######### Get Teaching Access Right END #########
		
		
		######### Get Class Teacher Access Right START #########
		$sql = "SELECT
			YC.*
		FROM 
			YEAR_CLASS_TEACHER YCT
		INNER JOIN
			YEAR_CLASS YC
		ON 
			YCT.YearClassID = YC.YearClassID
		INNER JOIN 
			YEAR Y 
		ON 
			YC.YearID = Y.YearID
		WHERE
			YCT.UserID = '{$userID}'
		AND
			YC.AcademicYearID = '{$academicYearID}'
		ORDER BY
			Y.Sequence, YC.Sequence";
		$rs = $this->returnResultSet($sql);
		$accessRight['classTeacher'] = $rs;
		######### Get Class Teacher Access Right END #########
		
		######### Get Subject Class Group START #########
		$sql = "SELECT SubjectGroupID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID = '{$userID}'";
		$rs = $this->returnVector($sql);
		$accessRight['subjectGroup'] = $rs;
		######### Get Subject Class Group END #########
		
		
		###### MonthlyReport Access Right Start ######
		$sql = "SELECT  SettingValue FROM GENERAL_SETTING WHERE SettingName = 'MonthlyReportPIC'";
		$TeacherIDList = $this->returnVector($sql);
		$TeacherIDAry = explode(',',$TeacherIDList[0]);
		if(in_array($userID,$TeacherIDAry)){
			$accessRight['MonthlyReportPIC'] = true;
		}
	
		###### MonthlyReport Access Right End ######
		
		return $accessRight;
	}
	  
	/*
	 * This function is for hiding the tab with no access right
	 */
	function updateTabAccessRight($accessRight){
		global $ipf_cfg;
		if($accessRight['admin']){
			return; // Admin show all tabs
		}
		unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['AcademicProgress']);
		
		if(!$accessRight['classTeacher']){
			unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['score_list']);
			unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['overall_performance']);
			unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['AcademicProgressByStd']);
			unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['AssessmentResultOfStd']);
		}
		if(!$accessRight['subjectPanel']){
			unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['SubjectStats']);
			unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['ImprovementStatsBySbj']);
			unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['INTERNAL_VALUE_ADDED_STAT']);
		}
		if(!$accessRight['subjectTeacher']){
			unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['ImprovementStatsByTeacher']);			
			unset($ipf_cfg["MODULE_TAB"]["ASSESSMENT_STAT"]['TeacherStats']);
		}
	}
  

	function getAllAcademicYearTerm() {
		$sql = "SELECT 
						a.AcademicYearID, 
						a.YearNameEN, 
						SUBSTRING(a.YearNameEN,1,4) AS YearName4,
						SUBSTRING(a.YearNameEN,3,2) AS YearName2,
						t.YearTermID,
						t.YearTermNameEN
				FROM 
						ACADEMIC_YEAR AS a 
				INNER JOIN 
						ACADEMIC_YEAR_TERM AS t ON (t.AcademicYearID = a.AcademicYearID)
				ORDER BY 
						a.AcademicYearID, t.YearTermID";
		$rs = $this->returnArray($sql);
		return $rs;		
	}
	
	/*
	 * 	1. There're {n} terms for an academic year
	 * 	2. return array like following:
	 * Array (
	 * 	[2016] => Array
	        (
	            [AcademicYearID] => 21
	            [YearNameEN] => 2016-2017
	            [YearName4] => 2016
	            [YearName2] => 16
	            [Term1ID] => 44
	            [Term1Name] => Term1
	            ...
	            [Term{n}ID] => 48
	            [Term{n}Name] => Term{n}
	        )
		)
	 */ 
	function buildAcademicYearTermAry() {
		$ret = array();
		$rs = $this->getAllAcademicYearTerm();
		$j = 1;
		$prev_y = '';	// year field of previous record 
		
		for ($i=0,$iMax=count($rs); $i<$iMax; $i++) {
			$r = $rs[$i];
			if(preg_match("/^\d{4}[-]\d{4}$/",$r['YearNameEN'])) {	// yyyy-yyyy pattern
				$y = substr($r['YearNameEN'],0,4);
			}
			else {	// assume yy/yy pattern
				$y = substr(date('Y'),0,2).substr($r['YearNameEN'],0,2);
			}

			if (!isset($ret[$y])) {						
				$ret[$y] = array (	'AcademicYearID'=>$r['AcademicYearID'],
									'YearNameEN'=>$r['YearNameEN'],
									'YearName4'=>$r['YearName4'],
									'YearName2'=>$r['YearName2']);
			}
			if ($y != $prev_y) {
				$j = 1;
			}
			else {
				$j++;
			}
			if (!isset($ret[$y]["Term{$j}ID"])) {
				$ret[$y]["Term{$j}ID"] = $r['YearTermID'];
				$ret[$y]["Term{$j}Name"] = $r['YearTermNameEN'];
			}
			$prev_y = $y;
		}
		
		return $ret;
	}

	function getStudentByAcademicYear($ParAcademicYearID, $ParClassID="", $ParStudentID="", $ReturnAsso=0, $isShowStyle=0) {
		$cond = '';
		if($ParStudentID!="") {

			$ParStudentIDAry = explode(',', trim($ParStudentID));
			$cond = " AND ycu.UserID IN ('".implode("','", (array)$ParStudentIDAry)."') ";
		} else if($ParClassID!="") {
			if(is_array($ParClassID))
			{
				$cond = " AND yc.YearClassID IN ('".implode("','", (array)$ParClassID)."') ";
			}
			else
				$cond = " AND yc.YearClassID = '$ParClassID'";
		}
		
		if ($isShowStyle==0)
			$starHTML = '*';
		else
			$starHTML = '<font style="color:red;">*</font>';
			
		$sql = "SELECT  DISTINCT
						y.YearName as ClassLevel,
						yc.ClassTitleEN as ClassName,
						ycu.UserID, 
						ycu.ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML', au.EnglishName) 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML', u.EnglishName) 
							ELSE u.EnglishName
						END as StudentNameEng,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML', au.ChineseName) 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML', u.ChineseName) 
							ELSE u.ChineseName
						END as StudentNameChi,
						CASE 
							WHEN u.WebSAMSRegNo IS NULL then CONCAT('##',u.UserID)
							WHEN u.WebSAMSRegNo='' then CONCAT('##',u.UserID)
							WHEN LEFT(u.WebSAMSRegNo,1) <>'#' then CONCAT('#',u.WebSAMSRegNo)
							ELSE u.WebSAMSRegNo
						END as WebSAMSRegNo
				FROM 
						YEAR y
						INNER JOIN YEAR_CLASS as yc ON (yc.YearID=y.YearID)
						INNER JOIN YEAR_CLASS_USER as ycu ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '".$ParAcademicYearID."')
						LEFT JOIN INTRANET_USER as u ON (ycu.UserID = u.UserID)
						Left Join INTRANET_ARCHIVE_USER as au ON (ycu.UserID = au.UserID) 
				WHERE 
						1=1
						$cond
				ORDER BY
						y.YearName,
						yc.ClassTitleEN, 
						ycu.ClassNumber
				";
		$row_student = $this->returnArray($sql);
		
		$ReturnArr = array();
		if ($ReturnAsso==1) {
			foreach ($row_student as $key => $thisStudentInfoArr) {
				$thisStudentID = $thisStudentInfoArr['UserID'];
				$ReturnArr[$thisStudentID] = $thisStudentInfoArr;
			}
		}
		else {
			$ReturnArr = $row_student;
		}
		return $ReturnArr;
	}

	function getStaffInfo($ReturnAsso=0) {
		
		$sql = "SELECT  DISTINCT
						CASE 
							WHEN u.StaffCode IS NULL then '##'
							WHEN u.StaffCode='' then '##'
							WHEN LEFT(u.StaffCode,1) <>'#' then CONCAT('#',u.StaffCode)
							ELSE u.StaffCode
						END as StaffCode,
						u.UserID,
						u.EnglishName,
						u.ChineseName
				FROM 
						INTRANET_USER as u  
				WHERE 
						RecordType='".USERTYPE_STAFF."'
				ORDER BY
						u.StaffCode
				";
		$row_staff = $this->returnArray($sql);
		
		$ReturnArr = array();
		if ($ReturnAsso==1) {
			foreach ($row_staff as $key => $thisStaffInfoArr) {
				$thisStaffID = $thisStaffInfoArr['UserID'];
				$ReturnArr[$thisStaffID] = $thisStaffInfoArr;
			}
		}
		else {
			$ReturnArr = $row_staff;
		}
		return $ReturnArr;
	}

  } // End Class
}
?>