<?php
// editing by yuen
include_once("libdb.php");

define("RECORD_TYPE_FORGOT_BOOK","F");
define("RECORD_TYPE_EXCUSE_LESSON","E");
define("RECORD_TYPE_NO_MEDIUM_INSTRUCTION","M");
define("RECORD_TYPE_ABSENT_AM","1");
define("RECORD_TYPE_ABSENT_PM","2");
define("RECORD_TYPE_LATE","3");
define("RECORD_TYPE_EARLYLEAVE","4");

class libclassdiary extends libdb
{
	var $MAX_NUMBER_OF_LESSON;
	var $Current_Academic_Year_ID;
	
	function libclassdiary()
	{
		parent::libdb();
		
		$this->MAX_NUMBER_OF_LESSON = 7;
		$this->Current_Academic_Year_ID = false;
	}
	
	function html_special_chars($text, $pad_empty=true,$pad_symbol='-')
	{
		if($text == ''){ 
			if($pad_empty) return $pad_symbol;
			return $text;
		}
		$x = htmlspecialchars($text,ENT_QUOTES);
		if($pad_empty && $x == ''){
			$x = $pad_symbol;
		}
		return $x;
	}
	
	function Get_Current_Academic_Year_ID()
	{
		if(!$this->Current_Academic_Year_ID){
			$this->Current_Academic_Year_ID = Get_Current_Academic_Year_ID();
		}
		return $this->Current_Academic_Year_ID;
	}
	
	function Get_Class_Diarys($DiaryID='',$ClassID='',$StartDate='',$EndDate='')
	{
		$class_title_field = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEn");
		$creator_name_field = getNameFieldByLang("u1.");
		$modify_name_field = getNameFieldByLang("u2.");
		
		$cond_diaryid = "";
		if(is_array($DiaryID) && count($DiaryID)>0){
			$cond_diaryid = " AND d.DiaryID IN (".implode(",",$DiaryID).") ";
		}else if(is_numeric($DiaryID) && $DiaryID > 0){
			$cond_diaryid = " AND d.DiaryID='".$DiaryID."' ";
		}
		
		$cond_classid = "";
		if(is_array($ClassID) && count($ClassID)>0){
			$cond_classid = " AND d.ClassID IN (".implode(",",$ClassID).") ";
		}else if(is_numeric($ClassID) && $ClassID > 0){
			$cond_classid = " AND d.ClassID='".$ClassID."' ";
		}
		
		$cond_date = "";
		if($StartDate != ""){
			$cond_date .= " AND d.DiaryDate >= '".$StartDate."' ";
		}
		if($EndDate != ""){
			$cond_date .= " AND d.DiaryDate <= '".$EndDate."' ";
		}
		
		$sql = "SELECT 
					d.DiaryID,
					d.ClassID,
					d.DiaryDate,
					".$class_title_field." as ClassName,
					$creator_name_field as CreatorName,
					$modify_name_field as ModifyName,
					DATE_FORMAT(d.DateModified,'%Y-%m-%d %H:%i:%s') as DateModified 
				FROM INTRANET_CLASS_DIARY as d 
				INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=d.ClassID 
				LEFT JOIN INTRANET_USER as u1 ON u1.UserID=d.InputBy 
				LEFT JOIN INTRANET_USER as u2 ON u2.UserID=d.ModifiedBy 
				WHERE 1 $cond_diaryid $cond_classid $cond_date
				ORDER BY d.DiaryDate DESC,".$class_title_field." ";
		//debug_r($sql);
		return $this->returnArray($sql);
	}
	
	function Get_Class_Diary_Lessons($DiaryID, $IsAll=false)
	{
		// Class Diary and its Lessons
		$class_title_field = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEn");
		$subject_field = Get_Lang_Selection("s.CH_ABBR","s.EN_ABBR");
		$subject_short_field = Get_Lang_Selection("s.CH_SNAME","s.EN_SNAME");
		
		$InputBy = $_SESSION['UserID'];
		if (!$IsAll)
		{
			$sql_cond = " AND c.InputBy='$InputBy' ";
		}
		
		$sql = "SELECT 
					d.DiaryID,
					d.ClassID,
					d.DiaryDate,
					$class_title_field as ClassName,
					c.LessonID,
					c.LessonNumber,
					s.RecordID as SubjectID,
					$subject_field as Subject,
					$subject_short_field as SubjectShort,
					c.InstructionMediumP,
					c.InstructionMediumE,
					c.InstructionMediumC,
					c.InstructionMediumB,
					c.Assignment,
					c.SubmissionDate,
					c.Others,
					c.InputBy
				FROM INTRANET_CLASS_DIARY as d 
				INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=d.ClassID 
				LEFT JOIN INTRANET_CLASS_DIARY_CLASS_LESSON c ON c.DiaryID=d.DiaryID 
				LEFT JOIN ASSESSMENT_SUBJECT as s ON s.RecordID=c.SubjectID 
				WHERE d.DiaryID='".$DiaryID."'  {$sql_cond}
				ORDER BY c.LessonNumber ";
		$lessons = $this->returnArray($sql);
		$name_field = getNameFieldByLang("u.");

		$LessonIDToUser = array();
		$LessonIDArray = array();
		$lesson_students = array();
		// Lesson students
		for($i=0;$i<count($lessons);$i++) {
			$lesson_id = $lessons[$i]['LessonID'];
			if($lesson_id !='' && $lesson_id > 0){
				$LessonIDArray[] = $lesson_id;
				$LessonIDToUser[$lesson_id] = array();
				$LessonIDToUser[$lesson_id][RECORD_TYPE_FORGOT_BOOK] = array();
				$LessonIDToUser[$lesson_id][RECORD_TYPE_EXCUSE_LESSON] = array();
				$LessonIDToUser[$lesson_id][RECORD_TYPE_NO_MEDIUM_INSTRUCTION] = array();
			}
		}
		if(count($LessonIDArray)>0){
			$sql = "SELECT 
						s.LessonID,
						u.UserID,
						".$name_field." as StudentName,
						u.ClassName,
						u.ClassNumber,
						s.RecordType 
					FROM INTRANET_USER as u 
					INNER JOIN INTRANET_CLASS_DIARY_LESSON_STUDENT as s ON s.UserID=u.UserID 
					WHERE u.RecordStatus=1 AND u.RecordType='".USERTYPE_STUDENT."' 
						AND s.LessonID IN (".implode(",",$LessonIDArray).") 
					ORDER BY s.LessonID,u.ClassNumber,s.RecordType";
			$lesson_students = $this->returnArray($sql);
			//debug_pr($sql);
		}
		//debug_pr($lesson_students);
		for($i=0;$i<count($lesson_students);$i++) {
			$lesson_id = $lesson_students[$i]['LessonID'];
			$user_id = $lesson_students[$i]['UserID'];
			$student_name = $lesson_students[$i]['StudentName'];
			$record_type = $lesson_students[$i]['RecordType'];
			$LessonIDToUser[$lesson_id][$record_type][] = $lesson_students[$i];
		}
		
		// Profile students
		$AbsentAMStudents = array();
		$AbsentPMStudents = array();
		$LateStudents = array();
		$EarlyleaveStudents = array();
		$sql = "SELECT 
					u.UserID,
					$name_field as StudentName,
					u.ClassName,
					u.ClassNumber,
					s.RecordType 
				FROM INTRANET_USER as u 
				INNER JOIN INTRANET_CLASS_DIARY_PROFILE_STUDENT as s ON s.UserID=u.UserID 
				WHERE s.DiaryID='".$DiaryID."' 
				ORDER BY u.ClassNumber,s.RecordType";
		$students = $this->returnArray($sql);
		
		for($i=0;$i<count($students);$i++)
		{
			$record_type = $students[$i]['RecordType'];
			if($record_type == RECORD_TYPE_ABSENT_AM){
				$AbsentAMStudents[] = $students[$i];
			}else if($record_type == RECORD_TYPE_ABSENT_PM){
				$AbsentPMStudents[] = $students[$i];
			}else if($record_type == RECORD_TYPE_LATE){
				$LateStudents[] = $students[$i];
			}else if($record_type == RECORD_TYPE_EARLYLEAVE){
				$EarlyleaveStudents[] = $students[$i];
			}
		}
		
		$DiaryDetail = array();
		// build lession number to detail array
		$LessonNumberToDetail = array();
		for($i=0;$i<count($lessons);$i++) {
			if(!isset($DiaryDetail['DiaryID'])){
				$DiaryDetail['DiaryID'] = $lessons[$i]['DiaryID'];
				$DiaryDetail['ClassID'] = $lessons[$i]['ClassID'];
				$DiaryDetail['DiaryDate'] = $lessons[$i]['DiaryDate'];
				$DiaryDetail['ClassName'] = $lessons[$i]['ClassName'];
				$DiaryDetail['LessonNumberToDetail'] = array();
				$DiaryDetail['LessonIDToUser'] = $LessonIDToUser;
				$DiaryDetail['AbsentAMStudents'] = $AbsentAMStudents;
				$DiaryDetail['AbsentPMStudents'] = $AbsentPMStudents;
				$DiaryDetail['LateStudents'] = $LateStudents;
				$DiaryDetail['EarlyleaveStudents'] = $EarlyleaveStudents;
			}
			if($lessons[$i]['LessonID'] != '' && $lessons[$i]['LessonID'] > 0){
				//debug($lessons[$i]['LessonNumber']);
				$LessonNumberToDetail[$lessons[$i]['LessonNumber']][] = $lessons[$i];
			}
		}
		$DiaryDetail['LessonNumberToDetail'] = $LessonNumberToDetail;
		
		return $DiaryDetail;
	}
	
	function Delete_Class_Diary($ParDiaryID)
	{
		$DiaryID = (array)$ParDiaryID;
		$diary_id_csv = implode(",",$DiaryID);
		
		$sql = "SELECT LessonID FROM INTRANET_CLASS_DIARY_CLASS_LESSON WHERE DiaryID IN (".$diary_id_csv.")";
		$LessonIDArray = $this->returnVector($sql);
		
		$success = array();
		if(count($LessonIDArray)>0){
			$sql = "DELETE FROM INTRANET_CLASS_DIARY_LESSON_STUDENT WHERE LessonID IN (".implode(",",$LessonIDArray).")";
			$success['DeleteLessonStudent'] = $this->db_db_query($sql);
		}
		
		$sql = "DELETE FROM INTRANET_CLASS_DIARY_PROFILE_STUDENT WHERE DiaryID IN (".$diary_id_csv.")";
		$success['DeleteProfileStudent'] = $this->db_db_query($sql);
		
		$sql = "DELETE FROM INTRANET_CLASS_DIARY_CLASS_LESSON WHERE DiaryID IN (".$diary_id_csv.")";
		$success['DeleteLesson'] = $this->db_db_query($sql);
		
		$sql = "DELETE FROM INTRANET_CLASS_DIARY WHERE DiaryID IN (".$diary_id_csv.")";
		$success['DeleteDiary'] = $this->db_db_query($sql);
		
		return !in_array(false,$success);
	}
	
	function Get_Students_By_ClassID($ClassID,$StudentID=array())
	{
		$NameField = getNameFieldByLang2('u.');
		$ClassTitleField = Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN");
		
		$studentID_cond = "";
		if(is_array($StudentID) && count($StudentID)>0){
			$studentID_cond = " AND u.UserID IN (".implode(",",$StudentID).") ";
		}
		
		$sql = "SELECT  
					ycu.UserID, 
					ycu.ClassNumber,
					$NameField as StudentName, 
					$ClassTitleField as ClassName,
					yc.YearClassID 
				FROM 
					YEAR_CLASS_USER as ycu
					INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID)
					INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
					INNER JOIN INTRANET_USER as u ON (ycu.UserID = u.UserID)
				WHERE 
					yc.YearClassID='".$ClassID."' $studentID_cond
				ORDER BY 
					y.Sequence, yc.Sequence, ycu.ClassNumber";
		return $this->returnArray($sql);
	}
	
	function Get_Class_DiaryID($ClassID,$DiaryDate)
	{
		$sql = "SELECT DiaryID FROM INTRANET_CLASS_DIARY WHERE ClassID='".$ClassID."' AND DiaryDate='".$DiaryDate."' ";
		$result = $this->returnVector($sql);
		
		return $result[0];
	}
	
	function Is_Class_Diary_Duplicated($ClassID,$DiaryDate)
	{
		$DiaryID = $this->Get_Class_DiaryID($ClassID,$DiaryDate);
		return ($DiaryID>0 && $DiaryID!=""?true:false);
	}
	
	function Update_Class_Diary($PostData)
	{
		$DiaryID = $PostData['DiaryID'];
		$ClassID = $PostData['ClassID'];
		$DiaryDate = $PostData['DiaryDate'];
		$AbsentAMStudent = $PostData['AbsentAMStudent'];
		$AbsentPMStudent = $PostData['AbsentPMStudent'];
		$LateStudent = $PostData['LateStudent'];
		$EarlyleaveStudent = $PostData['EarlyleaveStudent'];
		$LessonID = $PostData['LessonID'];
		$SubjectID = $PostData['SubjectID'];
		$InstructionMediumP = $PostData['InstructionMediumP'];
		$InstructionMediumE = $PostData['InstructionMediumE'];
		$InstructionMediumC = $PostData['InstructionMediumC'];
		$InstructionMediumB = $PostData['InstructionMediumB'];
		$IsAttendanceEdit = $PostData['edit_class_attendance'];
		$Assignment = $PostData['Assignment'];
		//$SubmissionDate = $PostData['SubmissionDate'];
		$SubmissionDate = "NULL";
		
		$StudentF = $PostData['StudentF'];
		$StudentE = $PostData['StudentE'];
		$StudentM = $PostData['StudentM'];
		$Others = $PostData['Others'];
		
		$isEdit = $DiaryID != '';
		$InputBy = $_SESSION['UserID'];
		$Success = array();
		
		# check if class of same day exists and then use the existing ID
		if (!$isEdit)
		{
			$DiaryID = $this->Get_Class_DiaryID($ClassID,$DiaryDate);
			if ($DiaryID!="" && $DiaryID>0)
			{
				$isEdit = true;
			}
		}

		if ($isEdit)
		{
			# to change
			if ($IsAttendanceEdit)
			{
				$sql = "DELETE FROM INTRANET_CLASS_DIARY_PROFILE_STUDENT WHERE DiaryID='".$DiaryID."' ";
				$this->db_db_query($sql);
				
				$sql = "INSERT INTO INTRANET_CLASS_DIARY_PROFILE_STUDENT (DiaryID,UserID,RecordType) VALUES ";
				$valus = "";
				$delimiter = "";
				if($AbsentAMStudent != ''){
					$AbsentAMStudentArray = explode(",",$AbsentAMStudent);
					for($i=0;$i<count($AbsentAMStudentArray);$i++){
						$values .= $delimiter."('".$DiaryID."','".$AbsentAMStudentArray[$i]."','".RECORD_TYPE_ABSENT_AM."')";
						$delimiter = ",";
					}
				}
				if($AbsentPMStudent != ''){
					$AbsentPMStudentArray = explode(",",$AbsentPMStudent);
					for($i=0;$i<count($AbsentPMStudentArray);$i++){
						$values .= $delimiter."('".$DiaryID."','".$AbsentPMStudentArray[$i]."','".RECORD_TYPE_ABSENT_PM."')";
						$delimiter = ",";
					}
				}
				if($LateStudent != ''){
					$LateStudentArray = explode(",",$LateStudent);
					for($i=0;$i<count($LateStudentArray);$i++){
						$values .= $delimiter."('".$DiaryID."','".$LateStudentArray[$i]."','".RECORD_TYPE_LATE."')";
						$delimiter = ",";
					}
				}
				if($EarlyleaveStudent != ''){
					$EarlyleaveStudentArray = explode(",",$EarlyleaveStudent);
					for($i=0;$i<count($EarlyleaveStudentArray);$i++){
						$values .= $delimiter."('".$DiaryID."','".$EarlyleaveStudentArray[$i]."','".RECORD_TYPE_EARLYLEAVE."')";
						$delimiter = ",";
					}
				}
				if($values != ''){
					$sql .= $values;
					$Success['InsertProfileStudent'] = $this->db_db_query($sql);
				}
			}
			
			for($i=0;$i<count($LessonID);$i++){
				$assignment = $this->Get_Safe_Sql_Query(trim(rawurldecode(stripslashes($Assignment[$i]))));
				$others = $this->Get_Safe_Sql_Query(trim(rawurldecode(stripslashes($Others[$i]))));
				$submission_date_value = ($assignment==''? "NULL" : "'".$SubmissionDate[$i]."'");
				if($LessonID[$i] != ''){
					if($SubjectID[$i] == ''){
						//$sql = "SELECT LessonID FROM INTRANET_CLASS_DIARY_CLASS_LESSON WHERE DiaryID='".$DiaryID."' AND LessonID='".$LessonID[$i]."' AND InputBy='".$InputBy."'";
						//$LessonsDel = $this->db_db_query($sql);
						$sql = "DELETE FROM INTRANET_CLASS_DIARY_CLASS_LESSON WHERE DiaryID='".$DiaryID."' AND LessonID='".$LessonID[$i]."' AND InputBy='".$InputBy."'";
						$this->db_db_query($sql);
						$sql = "DELETE FROM INTRANET_CLASS_DIARY_LESSON_STUDENT WHERE LessonID='".$LessonID[$i]."'";
						$this->db_db_query($sql);
					} else
					{
						$sql = "UPDATE INTRANET_CLASS_DIARY_CLASS_LESSON SET 
									SubjectID='".$SubjectID[$i]."',
									InstructionMediumP='".$InstructionMediumP[$i]."',
									InstructionMediumE='".$InstructionMediumE[$i]."',
									InstructionMediumC='".$InstructionMediumC[$i]."',
									InstructionMediumB='".$InstructionMediumB[$i]."',
									Assignment='".$assignment."',
									SubmissionDate=".$submission_date_value.",
									Others='".$others."',
									DateModified=NOW(),
									ModifiedBy='".$InputBy."' 
								WHERE DiaryID='".$DiaryID."' AND LessonID='".$LessonID[$i]."'";
						$Success['UpdateLesson'.$i] = $this->db_db_query($sql);
					}
				}else if($SubjectID[$i] != ''){
					$sql = "INSERT INTO INTRANET_CLASS_DIARY_CLASS_LESSON (DiaryID,LessonNumber,SubjectID,
								InstructionMediumP,InstructionMediumE,InstructionMediumC,InstructionMediumB,Assignment,
								SubmissionDate,Others,DateInput,DateModified,InputBy,ModifiedBy) VALUES (";
					$sql .= "'".$DiaryID."','".($i+1)."','".$SubjectID[$i]."','".
								$InstructionMediumP[$i]."','".$InstructionMediumE[$i]."','".$InstructionMediumC[$i]."','".$InstructionMediumB[$i]."','".$assignment."',
								".$submission_date_value.",'".$others."',NOW(),NOW(),'".$InputBy."','".$InputBy."'";
					$sql .= ")";
					$Success['UpdateLesson'.$i] = $this->db_db_query($sql);
					$LessonID[$i] = $this->db_insert_id();
				}
				if($Success['UpdateLesson'.$i] && $LessonID[$i]!='' && $SubjectID[$i] != ''){
					$sql = "DELETE FROM INTRANET_CLASS_DIARY_LESSON_STUDENT WHERE LessonID='".$LessonID[$i]."'";
					$this->db_db_query($sql);
					
					$sql = "INSERT INTO INTRANET_CLASS_DIARY_LESSON_STUDENT(LessonID,UserID,RecordType) VALUES ";
					$values = "";
					$delimiter = "";
					if($StudentF[$i] != ''){
						$StudentFArray = explode(",",$StudentF[$i]);
						for($j=0;$j<count($StudentFArray);$j++){
							$values .= $delimiter."('".$LessonID[$i]."','".$StudentFArray[$j]."','".RECORD_TYPE_FORGOT_BOOK."')";
							$delimiter=",";
						}
					}
					if($StudentE[$i] != ''){
						$StudentEArray = explode(",",$StudentE[$i]);
						for($j=0;$j<count($StudentEArray);$j++){
							$values .= $delimiter."('".$LessonID[$i]."','".$StudentEArray[$j]."','".RECORD_TYPE_EXCUSE_LESSON."')";
							$delimiter=",";
						}
					}
					if($StudentM[$i] != ''){
						$StudentMArray = explode(",",$StudentM[$i]);
						for($j=0;$j<count($StudentMArray);$j++){
							$values .= $delimiter."('".$LessonID[$i]."','".$StudentMArray[$j]."','".RECORD_TYPE_NO_MEDIUM_INSTRUCTION."')";
							$delimiter=",";
						}
					}
					if($values != ''){
						$sql .= $values;
						$Success['InsertLessonStudent'] = $this->db_db_query($sql);
					}
				}
			}
			$sql = "UPDATE INTRANET_CLASS_DIARY SET DateModified=now(),ModifiedBy='".$InputBy."' WHERE DiaryID='".$DiaryID."'";
			$Success['UpdatetDiary'] = $this->db_db_query($sql);
		}else{
			$sql = "INSERT INTO INTRANET_CLASS_DIARY (ClassID,DiaryDate,DateInput,DateModified,InputBy,ModifiedBy)
					VALUES('".$ClassID."','".$DiaryDate."',NOW(),NOW(),'".$InputBy."','".$InputBy."')";
			$Success['InsertDiary'] = $this->db_db_query($sql);
			if($Success['InsertDiary']){
				$DiaryID = $this->db_insert_id();
				
				$sql = "INSERT INTO INTRANET_CLASS_DIARY_PROFILE_STUDENT (DiaryID,UserID,RecordType) VALUES ";
				$valus = "";
				$delimiter = "";
				if($AbsentAMStudent != ''){
					$AbsentAMStudentArray = explode(",",$AbsentAMStudent);
					for($i=0;$i<count($AbsentAMStudentArray);$i++){
						$values .= $delimiter."('".$DiaryID."','".$AbsentAMStudentArray[$i]."','".RECORD_TYPE_ABSENT_AM."')";
						$delimiter = ",";
					}
				}
				if($AbsentPMStudent != ''){
					$AbsentPMStudentArray = explode(",",$AbsentPMStudent);
					for($i=0;$i<count($AbsentPMStudentArray);$i++){
						$values .= $delimiter."('".$DiaryID."','".$AbsentPMStudentArray[$i]."','".RECORD_TYPE_ABSENT_PM."')";
						$delimiter = ",";
					}
				}
				if($LateStudent != ''){
					$LateStudentArray = explode(",",$LateStudent);
					for($i=0;$i<count($LateStudentArray);$i++){
						$values .= $delimiter."('".$DiaryID."','".$LateStudentArray[$i]."','".RECORD_TYPE_LATE."')";
						$delimiter = ",";
					}
				}
				if($EarlyleaveStudent != ''){
					$EarlyleaveStudentArray = explode(",",$EarlyleaveStudent);
					for($i=0;$i<count($EarlyleaveStudentArray);$i++){
						$values .= $delimiter."('".$DiaryID."','".$EarlyleaveStudentArray[$i]."','".RECORD_TYPE_EARLYLEAVE."')";
						$delimiter = ",";
					}
				}
				if($values != ''){
					$sql .= $values;
					$Success['InsertProfileStudent'] = $this->db_db_query($sql);
				}
				
				for($i=0;$i<count($LessonID);$i++){
					$lesson_number = $i+1;
					if(trim($SubjectID[$i]) == ''){
						continue;
					}
					$assignment = $this->Get_Safe_Sql_Query(trim(rawurldecode(stripslashes($Assignment[$i]))));
					$others = $this->Get_Safe_Sql_Query(trim(rawurldecode(stripslashes($Others[$i]))));
					$submission_date_value = ($assignment==''? "NULL" : "'".$SubmissionDate[$i]."'");
					$sql = "INSERT INTO INTRANET_CLASS_DIARY_CLASS_LESSON (DiaryID,LessonNumber,SubjectID,
								InstructionMediumP,InstructionMediumE,InstructionMediumC,InstructionMediumB,Assignment,
								SubmissionDate,Others,DateInput,DateModified,InputBy,ModifiedBy) VALUES (";
					$sql .= "'".$DiaryID."','".$lesson_number."','".$SubjectID[$i]."','".
								$InstructionMediumP[$i]."','".$InstructionMediumE[$i]."','".$InstructionMediumC[$i]."','".$InstructionMediumB[$i]."','".$assignment."',
								".$submission_date_value.",'".$others."',NOW(),NOW(),'".$InputBy."','".$InputBy."'";
					$sql .= ")";
					$Success['InsertLesson'.$lesson_number] = $this->db_db_query($sql);
					if($Success['InsertLesson'.$lesson_number]){
						$LessonID[$i] = $this->db_insert_id();
						
						$sql = "INSERT INTO INTRANET_CLASS_DIARY_LESSON_STUDENT(LessonID,UserID,RecordType) VALUES ";
						$values = "";
						$delimiter = "";
						if($StudentF[$i] != ''){
							$StudentFArray = explode(",",$StudentF[$i]);
							for($j=0;$j<count($StudentFArray);$j++){
								$values .= $delimiter."('".$LessonID[$i]."','".$StudentFArray[$j]."','".RECORD_TYPE_FORGOT_BOOK."')";
								$delimiter=",";
							}
						}
						if($StudentE[$i] != ''){
							$StudentEArray = explode(",",$StudentE[$i]);
							for($j=0;$j<count($StudentEArray);$j++){
								$values .= $delimiter."('".$LessonID[$i]."','".$StudentEArray[$j]."','".RECORD_TYPE_EXCUSE_LESSON."')";
								$delimiter=",";
							}
						}
						if($StudentM[$i] != ''){
							$StudentMArray = explode(",",$StudentM[$i]);
							for($j=0;$j<count($StudentMArray);$j++){
								$values .= $delimiter."('".$LessonID[$i]."','".$StudentMArray[$j]."','".RECORD_TYPE_NO_MEDIUM_INSTRUCTION."')";
								$delimiter=",";
							}
						}
						if($values != ''){
							$sql .= $values;
							$Success['InsertLessonStudent'] = $this->db_db_query($sql);
						}
					} // insert lesson
				} // insert lessons
			} // insert diary success
		} // insert new
		
		return !in_array(false,$Success);
	}
	
	function Count_Student_Records($StudentID,$FromDate,$ToDate,$RecordType, $LogSubjectOnF="", $GetEHomework=false)
	{
		$StudentIDArray = (array)$StudentID;
		$RecordTypeArray = (array)$RecordType;
		
		$sql = "SELECT 
					s.UserID,
					s.RecordType,
					DATE_FORMAT(d.DiaryDate, '%e/%c/%Y') AS DiaryDate,
					c.SubjectID
				FROM INTRANET_CLASS_DIARY as d 
				INNER JOIN INTRANET_CLASS_DIARY_CLASS_LESSON as c ON c.DiaryID=d.DiaryID 
				INNER JOIN INTRANET_CLASS_DIARY_LESSON_STUDENT s ON s.LessonID=c.LessonID 
				WHERE s.UserID IN (".implode(",",$StudentIDArray).") 
					AND d.DiaryDate >= '".$FromDate."' AND d.DiaryDate <= '".$ToDate."' 
					AND s.RecordType IN ('".implode("','",$RecordTypeArray)."') 
				ORDER BY s.UserID,d.DiaryDate ";

		$result = $this->returnArray($sql);
		
		if ($LogSubjectOnF!="")
		{
			$sql = "SELECT RecordID, CH_ABBR FROM ASSESSMENT_SUBJECT ORDER BY RecordID";
			$rows = $this->returnArray($sql);
			$SubjectArr = build_assoc_array($rows);
			//debug_r($SubjectArr);
		}
		
		$StudentIDToTypeRecord = array();
		for($i=0;$i<count($result);$i++){
			$user_id = $result[$i]['UserID'];
			$record_type = $result[$i]['RecordType'];
			$diary_date = $result[$i]['DiaryDate'];
			
			if(!isset($StudentIDToTypeRecord[$user_id])){
				$StudentIDToTypeRecord[$user_id] = array();
				foreach($RecordTypeArray as $type){
					$StudentIDToTypeRecord[$user_id][$type] = array();
				}
			}
			
			//$StudentIDToTypeRecord[$user_id][$record_type][] = $diary_date;
			if (strstr($LogSubjectOnF, $record_type))
			{
				$StudentIDToTypeRecord[$user_id][$record_type][] = $diary_date . " ".$SubjectArr[$result[$i]['SubjectID']];
			} else
			{
				$StudentIDToTypeRecord[$user_id][$record_type][] = $diary_date;
			}
		}
		
		$sql = "SELECT 
					p.UserID,
					p.RecordType,
					DATE_FORMAT(d.DiaryDate, '%e/%c/%Y') AS DiaryDate
				FROM INTRANET_CLASS_DIARY as d 
				INNER JOIN INTRANET_CLASS_DIARY_PROFILE_STUDENT as p ON p.DiaryID=d.DiaryID 
				WHERE p.UserID IN (".implode(",",$StudentIDArray).") AND d.DiaryDate >= '".$FromDate."' 
					AND d.DiaryDate <= '".$ToDate."' 
					AND p.RecordType IN ('".implode("','",$RecordTypeArray)."') ";

		$result2 = $this->returnArray($sql);
		
		for($i=0;$i<count($result2);$i++){
			$user_id = $result2[$i]['UserID'];
			$record_type = $result2[$i]['RecordType'];
			$diary_date = $result2[$i]['DiaryDate'];
			
			if(!isset($StudentIDToTypeRecord[$user_id])){
				$StudentIDToTypeRecord[$user_id] = array();
				foreach($RecordTypeArray as $type){
					$StudentIDToTypeRecord[$user_id][$type] = array();
				}
			}
			$StudentIDToTypeRecord[$user_id][$record_type][] = $diary_date;
		}
		
		if ($GetEHomework)
		{
			$sql = "select hhl.StudentID, ih.Title, ih.SubjectID, DATE_FORMAT( ih.DueDate, '%e/%c/%Y') as DueDate " .
					"FROM INTRANET_HOMEWORK_HANDIN_LIST AS hhl, INTRANET_HOMEWORK AS ih
					WHERE hhl.recordstatus=-1 and hhl.StudentID IN (".implode(",",$StudentIDArray).") AND ih.HomeworkID=hhl.HomeworkID and ih.HandinRequired=1 AND UNIX_TIMESTAMP('{$FromDate}')<=UNIX_TIMESTAMP(ih.DueDate) AND UNIX_TIMESTAMP('{$ToDate}')>=UNIX_TIMESTAMP(ih.DueDate) " .
					"ORDER by ih.DueDate";
			$result = $this->returnArray($sql);
			//debug($sql);
			//debug_r($result);
			for($i=0;$i<count($result);$i++){
				$user_id = $result[$i]['StudentID'];
				$record_type = "EH";
				$diary_date = $result[$i]['DueDate'];
				$homework_title = "[".$SubjectArr[$result[$i]['SubjectID']] . "] ". $result[$i]['Title'];
				
				if(!isset($StudentIDToTypeRecord[$user_id])){
					$StudentIDToTypeRecord[$user_id] = array();
					foreach($RecordTypeArray as $type){
						$StudentIDToTypeRecord[$user_id][$type] = array();
					}
				}
				$StudentIDToTypeRecord[$user_id][$record_type][] = $diary_date . " ".$homework_title;
				
			}
		}
		
		return $StudentIDToTypeRecord;
	}
	
	function Count_Student_Records2($StudentID,$FromDate,$ToDate,$RecordType, $LogSubjectOnF="")
	{
		$StudentIDArray = (array)$StudentID;
		$RecordTypeArray = (array)$RecordType;
		
		$sql = "SELECT 
					s.UserID,
					s.RecordType,
					DATE_FORMAT(d.DiaryDate, '%e/%c/%Y') AS DiaryDate,
					c.SubjectID
				FROM INTRANET_CLASS_DIARY as d 
				INNER JOIN INTRANET_CLASS_DIARY_CLASS_LESSON as c ON c.DiaryID=d.DiaryID 
				INNER JOIN INTRANET_CLASS_DIARY_LESSON_STUDENT s ON s.LessonID=c.LessonID 
				WHERE s.UserID IN (".implode(",",$StudentIDArray).") 
					AND d.DiaryDate >= '".$FromDate."' AND d.DiaryDate <= '".$ToDate."' 
					AND s.RecordType IN ('".implode("','",$RecordTypeArray)."') 
				ORDER BY s.UserID,d.DiaryDate ";

		$result = $this->returnArray($sql);
		
		if ($LogSubjectOnF!="")
		{
			$sql = "SELECT RecordID, CH_ABBR FROM ASSESSMENT_SUBJECT ORDER BY RecordID";
			$rows = $this->returnArray($sql);
			$SubjectArr = build_assoc_array($rows);
			//debug_r($SubjectArr);
		}

		$StudentIDToTypeRecord = array();
		for($i=0;$i<count($result);$i++){
			$user_id = $result[$i]['UserID'];
			$record_type = $result[$i]['RecordType'];
			$diary_date = $result[$i]['DiaryDate'];
			
			if(!isset($StudentIDToTypeRecord[$user_id])){
				$StudentIDToTypeRecord[$user_id] = array();
				foreach($RecordTypeArray as $type){
					$StudentIDToTypeRecord[$user_id][$type] = array();
				}
			}
			
			if (strstr($LogSubjectOnF, $record_type))
			{
				$StudentIDToTypeRecord[$user_id][$record_type][] = $diary_date . " ".$SubjectArr[$result[$i]['SubjectID']];
			} else
			{
				$StudentIDToTypeRecord[$user_id][$record_type][] = $diary_date;
			}
		}
		
		$sql = "SELECT 
					p.UserID,
					p.RecordType,
					DATE_FORMAT(d.DiaryDate, '%e/%c/%Y') AS DiaryDate
				FROM INTRANET_CLASS_DIARY as d 
				INNER JOIN INTRANET_CLASS_DIARY_PROFILE_STUDENT as p ON p.DiaryID=d.DiaryID 
				WHERE p.UserID IN (".implode(",",$StudentIDArray).") AND d.DiaryDate >= '".$FromDate."' 
					AND d.DiaryDate <= '".$ToDate."' 
					AND p.RecordType IN ('".implode("','",$RecordTypeArray)."') ORDER BY s.UserID,d.DiaryDate ";

		$result2 = $this->returnArray($sql);
		
		for($i=0;$i<count($result2);$i++){
			$user_id = $result2[$i]['UserID'];
			$record_type = $result2[$i]['RecordType'];
			$diary_date = $result2[$i]['DiaryDate'];
			
			if(!isset($StudentIDToTypeRecord[$user_id])){
				$StudentIDToTypeRecord[$user_id] = array();
				foreach($RecordTypeArray as $type){
					$StudentIDToTypeRecord[$user_id][$type] = array();
				}
			}
			$StudentIDToTypeRecord[$user_id][$record_type][] = $diary_date;
		}
		
		return $StudentIDToTypeRecord;
	}
	
	function Get_Class_Diary_Student_Lessons($StudentID,$FromDate,$ToDate)
	{
		include_once("form_class_manage.php");
		$form_class_manage = new form_class_manage();
		
		$YearClassID = $form_class_manage->Get_Student_Studying_Class($StudentID);
		$result = array();
		if(count($YearClassID)>0){
			
			$subject_field = Get_Lang_Selection("a.CH_DES","a.EN_DES");
			$subject_short_field = Get_Lang_Selection("a.CH_SNAME","a.EN_SNAME");
			$sql = "SELECT 
						d.DiaryID,
						d.ClassID,
						d.DiaryDate,
						c.LessonID,
						c.LessonNumber,
						$subject_field as Subject,
						$subject_short_field as SubjectShort,
						c.Assignment,
						c.SubmissionDate,
						GROUP_CONCAT(s.RecordType) as LessonRecordType,
						GROUP_CONCAT(p.RecordType) as ProfileRecordType  
					FROM INTRANET_CLASS_DIARY as d 
					INNER JOIN INTRANET_CLASS_DIARY_CLASS_LESSON as c ON c.DiaryID=d.DiaryID 
					INNER JOIN ASSESSMENT_SUBJECT as a ON a.RecordID=c.SubjectID 
					LEFT JOIN INTRANET_CLASS_DIARY_LESSON_STUDENT as s ON s.LessonID=c.LessonID AND s.UserID='".$StudentID."' 
					LEFT JOIN INTRANET_CLASS_DIARY_PROFILE_STUDENT as p ON p.DiaryID=d.DiaryID AND p.UserID='".$StudentID."' 
					WHERE d.ClassID IN (".implode(",",(array)$YearClassID).") 
						AND d.DiaryDate >= '".$FromDate."' AND d.DiaryDate <= '".$ToDate."' 
					GROUP BY d.DiaryID,c.LessonID 
					ORDER BY d.DiaryDate DESC,c.LessonNumber ";
			//debug_r($sql);
			$result = $this->returnArray($sql);
			//debug_r($result);
			return $result;
		}
		return $result;
	}
	
	function IS_ADMIN_USER()
	{
		return ($_SESSION['UserType'] == USERTYPE_STAFF && $_SESSION['isTeaching']==1);
		
		//return $_SESSION["SSV_USER_ACCESS"]["eAdmin-ClassDiary"];
	}
	
	function IS_eService_USER($ParUserID='')
	{
		if($ParUserID == ''){
			$ParUserID = $_SESSION['UserID'];
		}
		$IsUser = false;
		
		if($_SESSION['UserType'] == USERTYPE_STUDENT) {
			$IsUser = true;
		}
		if($_SESSION['UserType'] == USERTYPE_PARENT) {
			$sql = "SELECT COUNT(*) FROM INTRANET_PARENTRELATION as r 
					INNER JOIN INTRANET_USER as u ON u.UserID=r.StudentID 
					AND u.RecordStatus=1 AND u.RecordType='".USERTYPE_STUDENT."' 
					WHERE r.ParentID='".$_SESSION['UserID']."' ";
			$count = $this->returnVector($sql);
			$IsUser = $count[0] > 0;
		}
		
		return $IsUser;
	}
}

?>