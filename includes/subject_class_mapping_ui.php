<?
# Using: Bill

### Change log [Start] ###
/*
 * Date     :   2019-07-29 (Bill)   [2018-1002-1146-08277]
 *              Modified Get_Subject_Group_Selection(), to add option to set first title
 *
 * Date     :   2019-07-26 (Ray)
 *              Modified Get_Subject_Selection all subject group
 *
 * Date     :   2018-08-29 (Bill)   [2017-1207-0956-53277]
 *              Modified Get_SchoolSettings_Subject_Tab_Array(), to add new tab
 * 
 * Date     :   2018-07-31 (Bill)
 *              Modified Get_Subject_Group_SelectionBox_By_Academic_Year_Term_ID(), added 3 parms - $SelectionIDName, $OnChange, $IsMultiple
 * 
 * Date		:	2018-06-25 (Pun) [ip.2.5.9.7.1]
 * 				Added Get_Subject_Group_SelectionBox_By_Academic_Year_ID()
 * 
 * Date		:	2017-06-09 (Icarus)
 * 				modified Get_Learning_Category_Add_Edit_Layer(), move the "close button" to the bottom of the thickbox
 * 				modified Get_Subject_Add_Edit_Layer(), <div class="edit_pop_board_write" style="height:340px;">
 * 
 * Date		:	2017-05-23	(Villa) #T116794 
 * 				modified Get_Current_Year_Subject_Group_SelectionBox: Arrange the selection box ordering
 * 
 * Date		:	2017-02-24	(Villa)
 * 				add Get_Subject_Selection_ForOldSDAS
 * 
 * Date		:	2016-12-28 	(Villa)
 * 				modified Get_Current_Year_Subject_Group_SelectionBox() - return lang if no result && fix the subject group duplicate problem
 * 
 * Date		:	2016-12-23	(Villa)
 * 				fix repeated subject class problem
 * 
 * Date		:	2016-12-12 (Villa)
 * 				modified Get_Current_Year_Subject_Group_SelectionBox() - support subject panel
 * 
 * Date		:	2016-12-09 (Villa)
 * 				modified Get_Current_Year_Subject_Group_SelectionBox() - improve the sql ->reduce the query
 * 
 * Date		:	2016-12-07 (Villa)
 * 				add Get_Current_Year_Subject_Group_SelectionBox() - get current year subject group selection box
 * 				modified Get_Current_Year_Subject_Group_SelectionBox() - filter subject group
 * 
 * Date		:	2016-01-11 (Carlos)
 * 				modified Get_Subject_Class_Detail(), enable edit and remove subject group for admin.
 * 
 * Date		:	2015-12-30 (Henry)
 * 				modified Get_Subject_Class_Detail() to fix php 5.4 warning
 * 				trim() expects parameter 1 to be string, array given
 * 				ip.2.5.7.1.1
 * 
 * Date		:	2015-12-18 (Bill)	[2015-1111-1214-16164]
 * 				modify Get_Form_Subject_Component_Setting_Table(), display Subject Code when $sys_custom['Subject']['DisplaySubjectCode'] = true
 * 				ip.2.5.7.1.1
 * 
 * Date		:	2015-10-19 (Pun)
 * 				modify Get_Class_Form() added flag for default check link to eClass
 * 				ip.2.5.6.10.1
 * 
 * Date		:	2015-09-04 (Ivan)
 * 				added function Get_SchoolSettings_Subject_Tab_Array()
 * 				ip.2.5.6.10.1
 * 
 * Date		:	2015-08-17 (Pun)
 * 				added $ExtrudeSubjectIDArr to Get_Subject_Selection()
 * 
 * Date		:	2014-08-08 (Bill)
 * 				modified Get_Class_Form(), add select all check box
 * 
 * Date		:	2011-01-18 (Henry Chow)
 * Details	:	modifed Get_Subject_Group_Mapping(), will display warning message, but will not display "By coping from previous data" if settings in "Setting > Subject > Form Subject" is not completed 
 *  
 * Date		:	2011-01-17 (Henry Chow)
 * Details	:	modifed Get_Subject_Class_Mapping_Table(), if setting on Form-Subject relation is not finished, will not display "Add Subject Group" option  
 * 
 * Date		:	2010-12-29 (Henry Chow)
 * Details	:	modifed Get_Form_Subject_Table(), modified the display of table 
 * 
 * Date		:	2010-12-23 (Henry Chow)
 * Details	:	added Get_Form_Subject_Table(), display Edit "Form-Subject" table 
 * 
 * Date		:	20101215 (Ivan)
 * Details	:	added Get_User_Accessible_Subject_Group_Selection(), selection of the user teaching / studying Subject Group
 * 
 * Date		: 	20101213 (Henry Chow)
 * Details	:	added Get_Form_Subject_Component_Setting_Table(), display the relationship between "Form" & "Subject" 
 * 
 * Date		: 	20101109 (Marcus)
 * Details	:	modified Get_Subject_Selection(), moved Get_Subject_Group_List before the looping to save queries, skip optgroup if all options of it are skipped 
 * 
 * Date		: 	20101103 (Ivan)
 * Details	:	modified Get_Class_List_Selection() to support getting the whole Form student
 * 
 * Date		: 	20101007 (Ivan)
 * Details	:	added Get_Delete_Subject_Group_Confirm_UI() to display the Delete Subject Group Confirmation Page
 * 
 * Date		:	20100830 (Ivan)
 * Details	:	modified Get_Subject_Group_Mapping() to re-arrange the import and export subject group icons
 * 
 * Date		:	20100826 (Ivan)
 * Details	:	modified Get_Subject_Class_Mapping_Table() to add delete subject group icon
 * 
 * Date		:	20100803 (Ivan)
 * Details	:	added function Get_Subject_Component_Selection()
 * 				modified function Get_Subject_Class_Detail() and Get_Class_Form() to support mapping of Subject Component to Subject Group
 * 
 * Date		:	20100707 (Henry Chow)
 * Details	:	modified Get_Subject_Class_Detail(), edit UI wording of the "Edit" button
 *
 */
### Change log [End] ###

include_once('form_class_manage.php');
include_once('subject_class_mapping.php');

include_once('libinterface.php');

class subject_class_mapping_ui {
	function subject_class_mapping_ui() {
		$this->thickBoxWidth = 750;
		$this->thickBoxHeight = 450;
		
		$this->maxLength['WebSAMSCode'] = 10;
		$this->toolCellWidth = "120px";
	}
	
	function Include_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x = '
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/script.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>
			
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" />
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
			';
			
		return $x;
	}
	
	function Get_Subject_Component_Setting_Table($ParentSubjectID='') {
		
		# Determine the current view
		$table = "";
		
		if ($ParentSubjectID=='')
		{
			$table .= $this->Get_All_Subject_View_Table();
		}
		else
		{
			$table .= $this->Get_Parent_Subject_View_Table($ParentSubjectID);
		}
		
		$table .= '<div class="FakeLayer"></div>'."\n";
		
		return $table;
	}
	
	function Get_All_Subject_View_Table() {
		global $Lang;
		
		$libinterface = new interface_html();
		$viewMode = "AllSubjects";
			
		# Get all Learning Category
		$libLC = new Learning_Category();
		$LC_InfoArr = $libLC->Get_All_Learning_Category();
		$numOfLC = count($LC_InfoArr);
		
		# Get All Subjects Info
		$libSubject = new Subject();
		# $subjectInfoArr[LearningCategoryID][SubjectID] = $infoArr[Title, Code....]
		$subjectInfoArr = $libSubject->Get_Subject_List($returnAsso=1);
		$numOfSubject = count($subjectInfoArr);
		
		$x = '';
		$x .= '<table class="common_table_list" id="SubjectContentTable">'."\n";
			$x .= $this->Get_Content_Table_Property();
					
			# Table Header
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'."\n";
						$x .= '<span class="row_content" style="float:left">'.$Lang['SysMgr']['SubjectClassMapping']['LearningCategory'].'</span>'."\n";
						
						# Add / Edit Learning Category
						$x .= '<span class="table_row_tool row_content_tool" style="float:right">'."\n";
							$x .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "setting_row", 
									$Lang['SysMgr']['SubjectClassMapping']['Edit']['LearningCategory'], "js_Show_Learning_Category_Add_Edit_Layer(); return false;");
						$x .= '</span>'."\n";
					$x .= '</th>'."\n";
					$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCode'].'</th>'."\n";
					$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['Title'].'</th>'."\n";
					$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['Abbreviation'].'</th>'."\n";
					$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['ShortForm'].'</th>'."\n";
					$x .= '<th>'."\n";
						# Add Subject Button
						$x .= '<div class="table_row_tool row_content_tool">'."\n";
							$x .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add", 
									$Lang['SysMgr']['SubjectClassMapping']['Add']['Subject'], "js_Show_Subject_Add_Edit_Layer('', '', '0', ''); return false;");
						$x .= '</div>'."\n";
					$x .= '</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			## Table Content
			//if ($numOfLC == 0)
			//{
			//	$x .= '<tr><td colspan="8" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			//}
			//else
			//{
				# Loop each Learning Category
				for ($i=0; $i<$numOfLC; $i++)
				{
					$thisLearningCateoryID = $LC_InfoArr[$i]['LearningCategoryID'];
					$thisCategorySubjectInfoArr = $subjectInfoArr[$thisLearningCateoryID];
					if (count($thisCategorySubjectInfoArr)==0)
						$thisCategorySubjectInfoArr = array();
					
					//$thisLC_Obj = new Learning_Category($thisLearningCateoryID);
					//$thisLearningCategoryName = Get_Lang_Selection($thisLC_Obj->NameChi, $thisLC_Obj->NameEng);
					$thisLearningCategoryName = Get_Lang_Selection($LC_InfoArr[$i]['NameChi'], $LC_InfoArr[$i]['NameEng']);
					
					$numOfSubjectInThisCategory = count($thisCategorySubjectInfoArr);
					
					$firstAddRowDisplayStyle = '';
					if ($numOfSubjectInThisCategory > 0)
					{
						$firstAddRowDisplayStyle = 'style="display:none"';
						$rowspan = $numOfSubjectInThisCategory + 2;
					}
					else
					{
						$rowspan = 1;
					}
					
					$x .= '<tbody>'."\n";
						$x .= '<tr class="nodrop nodrag">'."\n";
							$x .= '<td rowspan="'.$rowspan.'">'.$thisLearningCategoryName.'</td>'."\n";
							$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
							$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
							$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
							$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
							# Add Subject Button 
							$x .= '<td '.$firstAddRowDisplayStyle.'>'."\n";
								$x .= '<div class="table_row_tool row_content_tool">'."\n";
									$x .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
											$Lang['SysMgr']['SubjectClassMapping']['Add']['SubjectInLearningCategory'], "js_Show_Subject_Add_Edit_Layer('', '$thisLearningCateoryID', '0', ''); return false;");
								$x .= '</div>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					
						# Display each Subject in this Learning Category
						foreach($thisCategorySubjectInfoArr as $thisSubjectID => $thisSubjectInfoArr)
						{
							$x .= $this->Generate_Subject_Display_Row($thisSubjectInfoArr, $viewMode, 1);
						}
						
						# Add Subject Button 
						$lastAddRowDisplayStyle = '';
						if ($numOfSubjectInThisCategory == 0)
							$lastAddRowDisplayStyle = 'style="display:none"';
						
						$x .= '<tr class="nodrop nodrag" '.$lastAddRowDisplayStyle.'>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							# Add Subject Button 
							$x .= '<td>'."\n";
								$x .= '<div class="table_row_tool row_content_tool">'."\n";
									$x .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
												$Lang['SysMgr']['SubjectClassMapping']['Add']['SubjectInLearningCategory'], 
												"js_Show_Subject_Add_Edit_Layer('', '$thisLearningCateoryID', '0', ''); return false;");
								$x .= '</div>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</tbody>'."\n";
				}
				
				# Subjects which do not have learning category
				$numOfTempSubject = count($subjectInfoArr['temp']);
				if ($numOfTempSubject > 0)
				{
					$x .= '<tbody>'."\n";
						$x .= '<tr class="nodrop nodrag">'."\n";
							$x .= '<td rowspan="'.($numOfTempSubject + 1).'">'.$Lang['SysMgr']['SubjectClassMapping']['NoLearningCategory'].'</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
						$x .= '</tr>'."\n";
					
					# Display each Subject in not classified category
					foreach($subjectInfoArr['temp'] as $thisSubjectID => $thisSubjectInfoArr)
					{
						$x .= $this->Generate_Subject_Display_Row($thisSubjectInfoArr, $viewMode, 1);
					}
					$x .= '</tbody>'."\n";
				}
			//}
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_Parent_Subject_View_Table($ParentSubjectID) {
		global $Lang;
		
		$libinterface = new interface_html();
		$viewMode = "ParentSubject";
			
		# Get all Learning Category
		$libLC = new Learning_Category();
		$LC_InfoArr = $libLC->Get_All_Learning_Category();
		$numOfLC = count($LC_InfoArr);
		
		$ParentSubject_Obj = new subject($ParentSubjectID);
		$ParentSubjectNameEN = $ParentSubject_Obj->EN_DES;
		$ParentSubjectNameB5 = $ParentSubject_Obj->CH_DES;
		$ParentSubjectNameDisplay = Get_Lang_Selection($ParentSubjectNameB5, $ParentSubjectNameEN);
		
		$thisLearningCategoryID = $ParentSubject_Obj->LearningCategoryID;
		
		# Componenet Subject Info
		$ComponentSubjectInfoArr = $ParentSubject_Obj->Get_Subject_Component();
		$numOfComponent = count($ComponentSubjectInfoArr);
		
		$x = '';
		$x .= '<table class="common_table_list" id="SubjectComponentContentTable">'."\n";
			$x .= $this->Get_Content_Table_Property();
					
			# Table Header
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'."\n";
						$x .= '<span class="row_content">'.$Lang['SysMgr']['SubjectClassMapping']['ParentSubject'].'</span>'."\n";
					$x .= '</th>'."\n";
					$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCode'].'</th>'."\n";
					$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['ComponentName'].'</th>'."\n";
					$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['Abbreviation'].'</th>'."\n";
					$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['ShortForm'].'</th>'."\n";
					$x .= '<th>'."\n";
						$x .= '&nbsp;';
					$x .= '</th>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			
			$firstAddRowDisplayStyle = '';
			if ($numOfComponent > 0)
			{
				$firstAddRowDisplayStyle = 'style="display:none"';
				$rowspan = $numOfComponent + 2;
			}
			else
			{
				$rowspan = 1;
			}
			
			$x .= '<tbody>'."\n";
				$x .= '<tr class="nodrop nodrag">'."\n";
					$x .= '<td rowspan="'.$rowspan.'">'."\n";
						# Back to All Subjects View
						$onClick = 'js_Reload_Content_Table(\'\'); return false;';
						$title = $Lang['SysMgr']['SubjectClassMapping']['View']['BackToAllSubjectsView'];
						$x .= '<a href="#" id="'.$ParentSubjectID.'_Link" onclick="'.$onClick.'" title="'.$title.'">'."\n";
							$x .= $ParentSubjectNameDisplay;
						$x .= '</a>'."\n";
					$x .= '</td>'."\n";
					$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
					$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
					$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
					$x .= '<td '.$firstAddRowDisplayStyle.'>&nbsp;</td>'."\n";
					# Add Component Subject Button 
					$x .= '<td '.$firstAddRowDisplayStyle.'>'."\n";
						$x .= '<div class="table_row_tool row_content_tool">'."\n";
							$x .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
									$Lang['SysMgr']['SubjectClassMapping']['Add']['SubjectComponent'], "js_Show_Subject_Add_Edit_Layer('', '$thisLearningCateoryID', '1', '$ParentSubjectID'); return false;");
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			
				$thisSubjectInfoArr = $subjectInfoArr[$thisLearningCategoryID][$ParentSubjectID];
				$x .= $this->Generate_Subject_Display_Row($thisSubjectInfoArr, $viewMode, 0);
				
				for ($i=0; $i<$numOfComponent; $i++)
				{
					$thisSubjectInfoArr = $ComponentSubjectInfoArr[$i];
					$x .= $this->Generate_Subject_Display_Row($thisSubjectInfoArr, $viewMode, 1, 1);
				}
				
				# Add Subject Button 
				$lastAddRowDisplayStyle = '';
				if ($numOfComponent == 0)
					$lastAddRowDisplayStyle = 'style="display:none"';
				
				$x .= '<tr class="nodrop nodrag" '.$lastAddRowDisplayStyle.'>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					$x .= '<td>&nbsp;</td>'."\n";
					# Add Component Subject Button 
					$x .= '<td>'."\n";
						$x .= '<div class="table_row_tool row_content_tool">'."\n";
							$x .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", 
									$Lang['SysMgr']['SubjectClassMapping']['Add']['SubjectComponent'], "js_Show_Subject_Add_Edit_Layer('', '$thisLearningCateoryID', '1', '$ParentSubjectID'); return false;");
						$x .= '</div>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		
		$x .= '<br />';
		
		# Back Btn
		$x .= '<div class="edit_bottom">'."\n";
			$x .= '<span></span>'."\n";
			$x .= '<p class="spacer"></p>'."\n";
				$x .= $libinterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", $onclick="js_Reload_Content_Table('');", $id="Btn_Back")."\n";
			$x .= '<p class="spacer"></p>'."\n";
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Content_Table_Property()
	{
		$x = '';
		
		$x .= '<col align="left" style="width:20%"></col>';
		$x .= '<col align="left" style="width:12%"></col>';
		$x .= '<col align="left"></col>';
		$x .= '<col align="left" style="width:12%"></col>';
		$x .= '<col align="left" style="width:12%"></col>';
		$x .= '<col align="left" style="width:'.$this->toolCellWidth.'"></col>';
		
		return $x;
	}
	
	
	function Generate_Subject_Display_Row($SubjectInfoArr=array(), $viewMode, $isSubRow=0, $isComponent=0)
	{
		global $Lang;
		
		$libinterface = new interface_html();
		
		if (count($SubjectInfoArr)==0)
			return "";
			
		$thisLearningCategoryID = $SubjectInfoArr['LearningCategoryID'];
		
		$thisSubjectID 		= $SubjectInfoArr['RecordID'];
		$thisCode 			= ($isComponent)? $SubjectInfoArr['CMP_CODEID'] : $SubjectInfoArr['CODEID'];
		$thisDescEN 		= $SubjectInfoArr['EN_DES'];
		$thisDescB5 		= $SubjectInfoArr['CH_DES'];
		$thisAbbrEN 		= $SubjectInfoArr['EN_ABBR'];
		$thisAbbrB5 		= $SubjectInfoArr['CH_ABBR'];
		$thisShortFormEN 	= $SubjectInfoArr['EN_SNAME'];
		$thisShortFormB5 	= $SubjectInfoArr['CH_SNAME'];
		
		/*
		$thisDescDisplay = $this->Get_Bilingual_Name_Display($thisDescB5, $thisDescEN);
		$thisAbbrDisplay = $this->Get_Bilingual_Name_Display($thisAbbrB5, $thisAbbrEN);
		$thisShortFormDisplay = $this->Get_Bilingual_Name_Display($thisShortFormB5, $thisShortFormEN);
		*/
		$thisDescDisplay = Get_Lang_Selection($thisDescB5, $thisDescEN);
		$thisAbbrDisplay = Get_Lang_Selection($thisAbbrB5, $thisAbbrEN);
		$thisShortFormDisplay = Get_Lang_Selection($thisShortFormB5, $thisShortFormEN);
		
		# Parent Subject Row in Parent Subject View
		if ($viewMode == "ParentSubject" && !$isComponent)
		{
			$AddOnly = true;
			$no_drop_drag_class = 'nodrop nodrag';
			$rowID_attr = '';
		}
		else
		{
			$AddOnly = false;
			$no_drop_drag_class = '';
			$rowID_attr = 'id="SubjectRow_'.$thisSubjectID.'"';
		}
		
		if (!$isComponent)
		{
			# Get_Subject_Component for Main Subject
			$Subject_Obj = new subject($thisSubjectID);
			$componentSubjectArr = $Subject_Obj->Get_Subject_Component();
			$numOfComponent = count($componentSubjectArr);
			$isParentSubject = ($numOfComponent==0)? false : true;
			
			$thisWebSAMSCode = $Subject_Obj->WEBSAMSCode;
			
			# Check if the subject can be deleted
			# If the subject has been linked to related subject group in terms / any years => cannot delete
			$scm = new subject_class_mapping();
			$subjectGroupArr = $scm->Get_Subject_Group_List('', $thisSubjectID);
			
			$canDelete = (count((array)$subjectGroupArr['SubjectGroupList']) == 0)? true : false;
			
			$reorderTooltips = $Lang['SysMgr']['SubjectClassMapping']['Reorder']['Subject'];
			$editTooltips = $Lang['Btn']['Edit'];
			$deleteToolTips = $Lang['SysMgr']['SubjectClassMapping']['Delete']['Subject'];
		}
		else
		{
			# Subject Component
			$Subject_Obj = new Subject_Component($thisSubjectID);
			$parentSubjectInfoArr = $Subject_Obj->Get_Parent_Subject();
			//$parentWebSAMSCode = $parentSubjectInfoArr['CODEID'];
			$parentSubjectID = $parentSubjectInfoArr['RecordID'];
			$canDelete = true;
			
			$reorderTooltips = $Lang['SysMgr']['SubjectClassMapping']['Reorder']['SubjectComponent'];
			$editTooltips = $Lang['SysMgr']['SubjectClassMapping']['Edit']['SubjectComponent'];
			$deleteToolTips = $Lang['SysMgr']['SubjectClassMapping']['Delete']['SubjectComponent'];
		}
		
		$class_css = '';
		if ($isSubRow)
			$class_css = ' class="sub_row '.$no_drop_drag_class.'" ';
		
		$x = '';
		$x .= '<tr '.$rowID_attr.' '.$class_css.'>';
		$x .= '<td>'.$thisCode.'</td>';
			# Subject Title (Display Show Component Icon if it is a Parent Subject)
			$x .= '<td>';
				if (!$isComponent)
				{
					$onClick = ($viewMode == "AllSubjects")? 'js_Reload_Content_Table(\''.$thisSubjectID.'\'); return false;' : 'js_Reload_Content_Table(\'\'); return false;';
					$title = ($viewMode == "AllSubjects")? $Lang['SysMgr']['SubjectClassMapping']['View']['SubjectComponent'] : $Lang['SysMgr']['SubjectClassMapping']['View']['BackToAllSubjectsView'];
					
					$x .= '<a href="#" id="'.$thisSubjectID.'_Link" onclick="'.$onClick.'" title="'.$title.'">';
						$x .= $thisDescDisplay;
						if ($numOfComponent > 0)
							$x .= " (".$numOfComponent.")";
					$x .= '</a>';
				}
				else
				{
					$x .= $thisDescDisplay;
				}
			$x .= '</td>';
			$x .= '<td>'.$thisAbbrDisplay.'</td>';
			$x .= '<td>'.$thisShortFormDisplay.'</td>';
			
			# Functional Icon
			$class = ($AddOnly)? "" : 'class="Dragable"';
			$x .= '<td '.$class.'>';
				$x .= '<div class="table_row_tool">';
					if (!$AddOnly)
					{
						# Move
						$x .= $libinterface->GET_LNK_MOVE("#", $reorderTooltips);
						# Edit Subject Button
						$x .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "edit_dim", $editTooltips, 
								"js_Show_Subject_Add_Edit_Layer('$thisSubjectID', '$thisLearningCategoryID', $isComponent, '$parentSubjectID'); return false;");
						# Show Delete button if the Subject is not a Parent Subject
						if (!$isParentSubject && $canDelete)
						{
							$x .= $libinterface->GET_LNK_DELETE("#", $deleteToolTips, "js_Delete_Subject('$thisSubjectID', '$isComponent'); return false;");;
						}
					}
					/* Hidden the add subject icon on 4 Jun 2009
					if (!$isComponent)
					{
						# Add Subject Component
						$x .= '<div class="table_row_tool row_content_tool">';
							$x .= $libinterface->Get_Thickbox_Link($this->thickBoxHeight, $this->thickBoxWidth, "add_dim", $Lang['SysMgr']['SubjectClassMapping']['Add']['Subject'], 
									"js_Show_Subject_Add_Edit_Layer('', '$thisLearningCategoryID', 1, '$thisSubjectID'); return false;");
						$x .= '</div>';
					}
					*/
				$x .= '</div>';
			$x .= '</td>';
		
		$x .= '</tr>';
		
		return $x;
	}
	
	function Get_Learning_Category_Add_Edit_Layer()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$libinterface = new interface_html();
		
		$x = "";
		$x .= '<div id="debugArea"></div>';
		
		$x .= '<div class="edit_pop_board" style="height:410px;">';
			$x .= $libinterface->Get_Thickbox_Return_Message_Layer();
			//$x .= '<h1> '.$Lang['SysMgr']['SubjectClassMapping']['LearningCategory'].' &gt; <span>'.$Lang['SysMgr']['SubjectClassMapping']['Setting']['LearningCategory'].'</span></h1>';
			$x .= '<div class="edit_pop_board_write" style="height:340px;">';
				$x .= '<div id="LearningCategoryInfoSettingLayer">';
					$x .= $this->Get_Learning_Category_Add_Edit_Table();
				$x .= '</div>';
				$x .= '<br />';
				
				
/*********************************** THE ORIGINAL CODE				
				# Cancel Btn
				$x .= '<div class="edit_bottom">'."\n";
					$x .= '<span></span>'."\n";
					$x .= '<p class="spacer"></p>'."\n";
						$x .= $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Hide_ThickBox()", $id="Btn_Cancel")."\n";
					$x .= '<p class="spacer"></p>'."\n";
				$x .= '</div>';
*************************************************************/


	
				/*
				# Last Updated Info Div
				$LastModifiedInfoArr = $libBuilding->Get_Last_Modified_Info();
				if ($LastModifiedInfoArr != NULL)
				{
					$lastModifiedDate = $LastModifiedInfoArr[0]['DateModified'];
					$lastModifiedBy = $LastModifiedInfoArr[0]['LastModifiedBy'];
					$lastModifiedName = $this->Get_UserName_By_UserID($lastModifiedBy);
					
					$LastModifiedDisplay = Get_Last_Modified_Remark($lastModifiedDate, $lastModifiedName);
					$x .= '<div class="edit_bottom">';
						$x .= '<span>'.$LastModifiedDisplay.'</span>';
						$x .= '<p class="spacer"></p>';
					$x .= '</div>';
				}
				*/
			
			$x .= '</div>';
			
			#the action button > close
			$htmlAry['closeBtn'] = $libinterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'closeBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");	
			
			$x .= '<div class="edit_bottom_v30">';
				$x .= '<span>'.'</span>';
				$x .= '<p class="spacer"></p>';
				$x .= $htmlAry['closeBtn'];					
				$x .= '<p class="spacer"></p>';
			$x .= '</div>';
			
			
		$x .= '</div>';
			
		return $x;
	}
	
	function Get_Learning_Category_Add_Edit_Table()
	{
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$libinterface = new interface_html();
		
		$libLC = new Learning_Category();
		$LC_InfoArr = $libLC->Get_All_Learning_Category();
		$numOfLC = count($LC_InfoArr);
		
		$x .= '<table id="LearningCategoryInfoTable" class="common_table_list" style="width:95%">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th style="width:20%">'.$Lang['SysMgr']['SubjectClassMapping']['SubjectWEBSAMSCode'].'</th>';
					$x .= '<th style="width:35%">'.$Lang['SysMgr']['Location']['TitleEn'].'</th>';
					$x .= '<th style="width:35%">'.$Lang['SysMgr']['Location']['TitleCh'].'</th>';
					$x .= '<th style="width:'.$this->toolCellWidth.'">&nbsp;</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
			
			# Display Learning Category Info
			if ($numOfLC > 0)
			{
				//$moveToolTips = $Lang['SysMgr']['SubjectClassMapping']['Reorder']['LearningCategory'];
				//$deleteToolTips = $Lang['SysMgr']['SubjectClassMapping']['Delete']['LearningCategory'];
				
				$moveToolTips = $Lang['Btn']['Move'];
				$deleteToolTips = $Lang['Btn']['Delete'];
				
				for ($i=0; $i<$numOfLC; $i++)
				{
					$thisLearningCategoryID = $LC_InfoArr[$i]['LearningCategoryID'];
					$thisTitleEn = $LC_InfoArr[$i]['NameEng'];
					$thisTitleCh = $LC_InfoArr[$i]['NameChi'];
					$thisCode = $LC_InfoArr[$i]['Code'];
					
					# show delete icon if the Learning Category has no related subject
					$thisLC_Obj = new Learning_Category($thisLearningCategoryID);
					$subjectList = $thisLC_Obj->Get_Subject_List();
					$numOfSubject = count($subjectList);
					$showDeleteIcon = ($numOfSubject==0)? true : false;
					
					$x .= '<tr id="LC_Row_'.$thisLearningCategoryID.'">';
						# Code
						$x .= '<td>';
							$x .= $libinterface->Get_Thickbox_Edit_Div("Code_".$thisLearningCategoryID, "Code_".$thisLearningCategoryID, "jEditCode", $thisCode);
							$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("Code_WarningDiv_".$thisLearningCategoryID);
						$x .= '</td>';
						# Title (English)
						$x .= '<td>';
							$x .= $libinterface->Get_Thickbox_Edit_Div("TitleEn_".$thisLearningCategoryID, "TitleEn_".$thisLearningCategoryID, "jEditTitleEn", $thisTitleEn);
							$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleEn_WarningDiv_".$thisLearningCategoryID);
						$x .= '</td>';
						# Title (Chinese)
						$x .= '<td>';
							$x .= $libinterface->Get_Thickbox_Edit_Div("TitleCh_".$thisLearningCategoryID, "TitleCh_".$thisLearningCategoryID, "jEditTitleCh", $thisTitleCh);
							$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleCh_WarningDiv_".$thisLearningCategoryID);
						$x .= '</td>';
						
						# Functional icons
						$x .= '<td class="Dragable" align="left">';
							# Move
							$x .= $libinterface->GET_LNK_MOVE("#", $moveToolTips);
							# Delete
							if ($showDeleteIcon)
							{
								$x .= $libinterface->GET_LNK_DELETE("#", $deleteToolTips, "js_Delete_Learning_Category('$thisLearningCategoryID'); return false;");
							}
						$x .= '</td>';
					$x .= '</tr>';
				}
			}
				# Add Learning Category Button
				$x .= '<tr id="AddLeraningCategoryRow" class="nodrop nodrag">';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>&nbsp;</td>';
					$x .= '<td>';
						$x .= '<div class="table_row_tool row_content_tool">';
							$x .= '<a href="#" onclick="js_Add_Learning_Category_Row(); return false;" class="add_dim" 
										title="'.$Lang['Btn']['Add'].'"></a>';
						$x .= '</div>';
					$x .= '</td>';
				$x .= '</tr>';
			$x .= '</tbody>';
		$x .= '</table>';
		
		return $x;
	}
	
	function Get_Subject_Add_Edit_Layer($SubjectID='', $LearningCategoryID='', $isComponent=0, $ParentSubjectID='')
	{
		global $Lang, $PATH_WRT_ROOT, $intranet_session_language;
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$libinterface = new interface_html();
		
		# Add / Edit
		$isEdit = ($SubjectID=='')? false : true;
		
		# Title
		if ($isComponent)
		{
			$thisTitle = ($isEdit)? $Lang['SysMgr']['SubjectClassMapping']['Edit']['SubjectComponent'] : $Lang['SysMgr']['SubjectClassMapping']['Add']['SubjectComponent'];
			$NameDisplay = $Lang['SysMgr']['SubjectClassMapping']['ComponentName'];
			$WebSAMSCodeDisplay = $Lang['SysMgr']['SubjectClassMapping']['WebSAMSCodeComponent'];
		}
		else
		{
			$thisTitle = ($isEdit)? $Lang['SysMgr']['SubjectClassMapping']['Edit']['Subject'] : $Lang['SysMgr']['SubjectClassMapping']['Add']['Subject'];
			$NameDisplay = $Lang['SysMgr']['SubjectClassMapping']['Title'];
			$WebSAMSCodeDisplay = $Lang['SysMgr']['SubjectClassMapping']['WebSAMSCode'];
		}
			
		# Learning Category Selection
		$LC_Selection = $this->Get_Learning_Category_Selection($LearningCategoryID, "LC_Selected");
		
		# Show Room info if it is edit mode
		if ($isEdit)
		{
			if ($isComponent)
			{
				# Get Parent Subject Name
				$thisSubjectObj = new Subject_Component($SubjectID);
				$parentSubjectInfo = $thisSubjectObj->Get_Parent_Subject();
				
				$parentSubjectName = Get_Lang_Selection($parentSubjectInfo['CH_DES'], $parentSubjectInfo['EN_DES']);
				$parentSubjectCode = $parentSubjectInfo['CODEID'];
			}
			else
			{
				$thisSubjectObj = new subject($SubjectID);
			}
			
			$thisCode = ($isComponent)? $thisSubjectObj->CMP_CODEID : $thisSubjectObj->CODEID;
			$thisDescEN = $thisSubjectObj->EN_DES;
			$thisDescB5 = $thisSubjectObj->CH_DES;
			$thisAbbrEN = $thisSubjectObj->EN_ABBR;
			$thisAbbrB5 = $thisSubjectObj->CH_ABBR;
			$thisShortFormEN = $thisSubjectObj->EN_SNAME;
			$thisShortFormB5 = $thisSubjectObj->CH_SNAME;
			
			$thisCode = intranet_htmlspecialchars($thisCode);
			$thisDescEN = intranet_htmlspecialchars($thisDescEN);
			$thisDescB5 = intranet_htmlspecialchars($thisDescB5);
			$thisAbbrEN = intranet_htmlspecialchars($thisAbbrEN);
			$thisAbbrB5 = intranet_htmlspecialchars($thisAbbrB5);
			$thisShortFormEN = intranet_htmlspecialchars($thisShortFormEN);
			$thisShortFormB5 = intranet_htmlspecialchars($thisShortFormB5);
		}
		else
		{
			if ($isComponent)
			{
				# Get Parent Subject Name
				$thisSubjectObj = new subject($ParentSubjectID);
				$parentSubjectName = Get_Lang_Selection($thisSubjectObj->CH_DES, $thisSubjectObj->EN_DES);
				$parentSubjectCode = $thisSubjectObj->CODEID;
			}
		}
		
		# Buttons
		if ($isEdit)
		{
			# Button Submit
			$btn_Edit = $libinterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", 
								$onclick="Insert_Edit_Subject('".$SubjectID."', 0, $isComponent, '".$ParentSubjectID."')", $id="Btn_Edit");
		}
		else
		{
			# Button Add & Finish
			$btn_Add_Finish = $libinterface->GET_ACTION_BTN($Lang['SysMgr']['SubjectClassMapping']['Button']['Add&Finish'], "button", 
								$onclick="Insert_Edit_Subject('', 0, $isComponent, '".$ParentSubjectID."')", $id="Btn_AddFinish");
			# Button Add & Add More Location
			$btn_Add_AddMore = $libinterface->GET_ACTION_BTN($Lang['SysMgr']['SubjectClassMapping']['Button']['Add&AddMoreSubject'], "button", 
								$onclick="Insert_Edit_Subject('', 1, $isComponent, '".$ParentSubjectID."')", $id="Btn_AddMore");
		}
		# Reset
		//$btn_Reset = $libinterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "button", 
		//					$onclick="document.SubjectForm.reset(); $('#CodeWarningDiv').hide('fast');", $id="Btn_Reset");
		# Cancel
		$btn_Cancel = $libinterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", 
							$onclick="js_Hide_ThickBox()", $id="Btn_Cancel");
							
		$x = '';
		$x .= '<div id="debugArea"></div>';
		$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
			$x .= $libinterface->Get_Thickbox_Return_Message_Layer();
			//$x .= '<h1> '.$Lang['SysMgr']['SubjectClassMapping']['Subject'].' &gt; <span>'.$thisTitle.'</span></h1>'."\n";
			$x .= '<div class="edit_pop_board_write" style="height:340px;">'."\n";
				$x .= '<form id="SubjectForm" name="SubjectForm">'."\n";
					$x .= '<table class="form_table">'."\n";
						$x .= '<col class="field_title" />'."\n";
						$x .= '<col class="field_c" />'."\n";
						$x .= '<col style="text-align:center" />'."\n";
						$x .= '<col style="text-align:center; width:280px" />'."\n";
						
						if ($isComponent)
						{
							# Show Parent Subject Info
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['ParentSubject'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td colspan="2">'.$parentSubjectName.'</td>'."\n"; 
							$x .= '</tr>'."\n";
						}
						else
						{
							# Learning Category selection
							$x .= '<tr>'."\n";
								$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['LearningCategory'].'</td>'."\n";
								$x .= '<td>:</td>'."\n";
								$x .= '<td colspan="2">'."\n";
									$x .= $LC_Selection."\n";
									$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("LC_WarningDiv")."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						}
						
						# Code input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$WebSAMSCodeDisplay.'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td colspan="2">'."\n";
								$x .= '<input id="SubjectCode_'.$SubjectID.'" name="SubjectCode_'.$SubjectID.'" type="text" class="textbox" value="'.$thisCode.'" maxlength="'.$this->maxLength['WebSAMSCode'].'"/>'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv")."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						$x .= '<tr><td colspan="4">&nbsp;</td></tr>';
						
						$x .= '<tr>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td>&nbsp;</td>'."\n";
							$x .= '<td style="text-align:center">'.$Lang['General']['English'].'</td>'."\n";
							$x .= '<td style="text-align:center">'.$Lang['General']['Chinese'].'</td>'."\n";
						$x .= '</tr>'."\n";
						
						# Title input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$NameDisplay.'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Subject_TitleEn" name="Subject_TitleEn" type="text" class="textbox" value="'.$thisDescEN.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleEnWarningDiv")."\n";
							$x .= '</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Subject_TitleCh" name="Subject_TitleCh" type="text" class="textbox" value="'.$thisDescB5.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("TitleChWarningDiv")."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						/*
						# TitleCh input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['TitleCh'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Subject_TitleCh" name="Subject_TitleCh" type="text" class="textbox" value="'.$thisDescB5.'" />'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						*/
						# Abbr input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['Abbreviation'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Subject_AbbrEn" name="Subject_AbbrEn" type="text" class="textbox" value="'.$thisAbbrEN.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("AbbrEnWarningDiv")."\n";
							$x .= '</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Subject_AbbrCh" name="Subject_AbbrCh" type="text" class="textbox" value="'.$thisAbbrB5.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("AbbrChWarningDiv")."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						/*
						# AbbrCh input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['AbbreviationCh'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Subject_AbbrCh" name="Subject_AbbrCh" type="text" class="textbox" value="'.$thisAbbrB5.'" />'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						*/
						# Short Form input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['ShortForm'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Subject_ShortFormEn" name="Subject_ShortFormEn" type="text" class="textbox" value="'.$thisShortFormEN.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("ShortFormEnWarningDiv")."\n";
							$x .= '</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Subject_ShortFormCh" name="Subject_ShortFormCh" type="text" class="textbox" value="'.$thisShortFormB5.'" />'."\n";
								$x .= $libinterface->Get_Thickbox_Warning_Msg_Div("ShortFormChWarningDiv")."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						/*
						# Short Form Ch input
						$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['SysMgr']['SubjectClassMapping']['ShortFormCh'].'</td>'."\n";
							$x .= '<td>:</td>'."\n";
							$x .= '<td>'."\n";
								$x .= '<input id="Subject_ShortFormCh" name="Subject_ShortFormCh" type="text" class="textbox" value="'.$thisShortFormB5.'" />'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						*/
					$x .= '</table>'."\n";
				$x .= '</form>'."\n";
			$x .= '</div>'."\n";
			
			$x .= '<div class="edit_bottom">'."\n";
				$x .= '<span></span>'."\n";
				$x .= '<p class="spacer"></p>'."\n";
				if ($isEdit)
				{
					$x .= $btn_Edit."\n";
				}
				else
				{
					$x .= $btn_Add_Finish."\n";
					$x .= $btn_Add_AddMore."\n";
				}
				$x .= $btn_Reset."\n";
				$x .= $btn_Cancel."\n";
				$x .= '<p class="spacer"></p>'."\n";
			$x .= '</div>';
		$x .= '</div>';
		
		return $x;
	}
	
	function Get_Learning_Category_Selection($SelectedLearningCategoryID='', $ParID='', $noFirst=0)
	{
		global $intranet_session_language, $Lang;
		
		$libLC = new Learning_Category();
		$all_LC_InfoArr = $libLC->Get_All_Learning_Category();
		$numOfLC = count($all_LC_InfoArr);
		
		$selectionArr = array();
		for ($i=0; $i<$numOfLC; $i++)
		{
			$thisLearningCategoryID = $all_LC_InfoArr[$i]['LearningCategoryID'];
			$thisTitle = ($intranet_session_language=='en')? $all_LC_InfoArr[$i]['NameEng'] : $all_LC_InfoArr[$i]['NameChi'];
			
			$selectionArr[$thisLearningCategoryID] = $thisTitle;
		}
		
		$selectionTags = ' id="'.$ParID.'" name="'.$ParID.'" ';
		$firstTitle = $this->Get_Selection_First_Title($Lang['SysMgr']['SubjectClassMapping']['Select']['LearningCategory']);
		
		$LC_Selection = getSelectByAssoArray($selectionArr, $selectionTags, $SelectedLearningCategoryID, $all=0, $noFirst, $firstTitle);
		
		return $LC_Selection;
	}
	
	function Get_Selection_First_Title($Title)
	{
		return '- '.$Title.' -';
	}
	
	function Get_Bilingual_Name_Display($NameCh, $NameEn)
	{
		global $intranet_session_language;
		
		$returnString = '';
		if ($intranet_session_language == 'en')
			$returnString = $NameEn.'<br />'.$NameCh;
		else
			$returnString = $NameCh.'<br />'.$NameEn;
			
		return $returnString;
	}
	
	
	function Get_Subject_Group_Mapping($AcademicYearID='',$YearTermID='') {
		global $Lang,$PATH_WRT_ROOT,$eComm;
		//echo 'hello';
		$NoCheckCurrent = (trim($AcademicYearID) == "")? false:true;
		$fcm = new form_class_manage();
		$sbj = new subject();
		
		$libinterface = new interface_html();
		
		# if changed the academic year selection => no term selected => hide the tool bar
		$toolbarDisplay = '';
		if ( ($AcademicYearID != '') && ($YearTermID == '') && ($AcademicYearID != Get_Current_Academic_Year_ID()) )
			$toolbarDisplay = ' style="display:none" ';
		
		$x = '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/2009a/js/subject_class_mapping.js"></script>
					<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
					<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>
					<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
					
					<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" />
					<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />';
		
		$x .= '<div id="SubjectGroupTableLayer">';
		$x .= '<table width="99%" border="0" cellspacing="0" cellpadding="0">';
			$x .= '<tr> ';
				$x .= '<td class="main_content"><div class="content_top_tool">';
					$x .= '<div class="Conntent_tool" id="ToolBarDiv" '.$toolbarDisplay.'>';
						/*
						$x .= '<div id="BatchCreateAndImportDiv">';
							$x .= '<div id="BatchCreateIconDiv" style="float:left">';
								$x .= '<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox new" onclick="Get_Batch_Create_Form();" title="'.$Lang['SysMgr']['SubjectClassMapping']['BatchCreate'].'">'.$Lang['SysMgr']['SubjectClassMapping']['BatchCreate'].'</a>';
							$x .= '</div>';
							$x .= '<div id="QuickEditDiv" style="float:left">';
								$x .= '<a href="javascript:js_Go_QuickEdit()" class="import" title="'.$Lang['SysMgr']['SubjectClassMapping']['QuickEdit'].'">'.$Lang['SysMgr']['SubjectClassMapping']['QuickEdit'].'</a>';
							$x .= '</div>';
							$x .= '<div id="CopyIconDiv" style="float:left">';
								$x .= '<a href="javascript:js_Go_Copy_SubjectGroup()" class="copy" title="'.$Lang['SysMgr']['SubjectClassMapping']['CopyFromOtherYearTerm'].'">'.$Lang['SysMgr']['SubjectClassMapping']['CopyFromOtherYearTerm'].'</a>';
							$x .= '</div>';
							$x .= '<br style="clear:both" />';
							$x .= '<div id="ImportSGIconDiv" style="float:left">';
								$x .= '<a href="javascript:js_GoImport_SubjectGroup()" class="import"> '.$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectGroup'].'</a>';
							$x .= '</div>';
							$x .= '<div id="ImportUserIconDiv" style="float:left">';
								$x .= '<a href="javascript:js_GoImport_SubjectGroupUser()" class="import"> '.$Lang['SysMgr']['SubjectClassMapping']['ImportTeacherStudent'].'</a>';
							$x .= '</div>';
							$x .= '<br style="clear:both" />';
						$x .= '</div>';
						$x .= '<div id="ExportUserIconDiv" style="float:left">';
							$x .= '<a href="javascript:js_GoExport_SubjectGroup()" class="export"> '.$Lang['SysMgr']['SubjectClassMapping']['ExportSubjectGroup'].'</a>';
						$x .= '</div>';
						$x .= '<div id="ExportUserIconDiv" style="float:left">';
							$x .= '<a href="javascript:js_GoExport_SubjectGroupUser()" class="export"> '.$Lang['SysMgr']['SubjectClassMapping']['ExportTeacherStudent'].'</a>';
						$x .= '</div>';
						*/
						
						// Create Subject Group From Classes (invisible link just called in js_Batch_Create_Subject_Group() to open the thickbox)
						$x .= '<div style="float:left;display:none;">';
							$x .= '<a id="BatchCreateSubjectGroupLink" href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox new" onclick="Get_Batch_Create_Form();" title="'.$Lang['SysMgr']['SubjectClassMapping']['FromClass'].'">'.$Lang['SysMgr']['SubjectClassMapping']['BatchCreate'].'</a>';
						$x .= '</div>';
						
						#
						
						$subjectFormAry = $sbj->getAllSubjectFormRelation();
						// Build
						$ImportOptionArr = array();
						$ImportOptionArr[] = array('javascript:js_Go_Batch_Create_Subject_Group();', $Lang['SysMgr']['SubjectClassMapping']['FromClass']);
						$ImportOptionArr[] = array('javascript:js_GoImport_SubjectGroup();', $Lang['SysMgr']['SubjectClassMapping']['ByImport']);
						if(sizeof($subjectFormAry)>0)
							$ImportOptionArr[] = array('javascript:js_Go_Copy_SubjectGroup();', $Lang['SysMgr']['SubjectClassMapping']['CopyFromOtherYearTerm']);
						$x .= $libinterface->Get_Content_Tool_v30('new', $href="javascript:void(0);", $Lang['SysMgr']['SubjectClassMapping']['BuildGroups'], $ImportOptionArr, $other="", $divID='BuildDiv');
						
						// Add Member
						$x .= '<div id="ImportMemberDiv" style="float:left;">'."\n";
							$x .= $libinterface->Get_Content_Tool_v30('new', $href="javascript:js_GoImport_SubjectGroupUser();", $Lang['SysMgr']['SubjectClassMapping']['ImportTeacherStudent'], $options=array(), $other="");
						$x .= '</div>'."\n";				
						
						// Revise Info
						$x .= '<div id="QuickEditDiv" style="float:left;">'."\n";
							$x .= $libinterface->Get_Content_Tool_v30('edit_content', $href="javascript:js_Go_QuickEdit();", $Lang['SysMgr']['SubjectClassMapping']['QuickEdit'], $options=array(), $other="");
						$x .= '</div>'."\n";
 					
						// Export
						$ExportOptionArr = array();
						$ExportOptionArr[] = array('javascript:js_GoExport_SubjectGroup();', $Lang['SysMgr']['SubjectClassMapping']['GroupDetails']);
						$ExportOptionArr[] = array('javascript:js_GoExport_SubjectGroupUser();', $Lang['SysMgr']['SubjectClassMapping']['TeacherStudent']);
						$x .= $libinterface->Get_Content_Tool_v30('export', $href="javascript:void(0);", $text="", $ExportOptionArr, $other="", $divID='ExportDiv');
						
						$ImportOptionArr[] = array('javascript:js_Go_QuickEdit();', $Lang['SysMgr']['SubjectClassMapping']['QuickEdit']);
					$x .= '</div>';
                	$x .= '<div class="Conntent_search">';
                		$x .= '<!--<input name="SearchValue" id="SearchValue" type="text"/>-->';
               		$x .= '</div>';
                	$x .= '<br style="clear:both" />';
				$x .= '</div>';
				if(sizeof($subjectFormAry)==0)
					$x .= showWarningMsg($eComm['Warning'], $Lang['SysMgr']['FormClassMapping']['FormSubjectSettingNotComplete']);
                $x .= '<div class="table_board">';
					$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
						$x .= '<tr>';
							$x .= '<td valign="bottom">';
							$x .= '<div class="table_filter">';
										
		// get school year selection
		$SchoolYearList = $fcm->Get_Academic_Year_List();
		$x .= '				<select name="AcademicYearID" id="AcademicYearID" onchange="Switch_Academic_Year(this.value);">';
		$x .= '					<option value="">'.$Lang['SysMgr']['FormClassMapping']['SelectSchoolYear'].'</option>';
		for ($i=0; $i < sizeof($SchoolYearList); $i++) {
			$SchoolYearName = Get_Lang_Selection($SchoolYearList[$i]['YearNameB5'],$SchoolYearList[$i]['YearNameEN']);
			
			if (!$NoCheckCurrent) {
				if ($SchoolYearList[$i]['CurrentSchoolYear'] == '1') 
					$AcademicYearID = $SchoolYearList[$i]['AcademicYearID'];
			}
			
			unset($Selected);
			$Selected = ($AcademicYearID == $SchoolYearList[$i]['AcademicYearID'])? 'selected':'';
			$x .= '				<option value="'.$SchoolYearList[$i]['AcademicYearID'].'" '.$Selected.'>'.$SchoolYearName.'</option>';
		}
		$x .= '				</select>';
		
		// get school year term selection
		$YearTermList = $fcm->Get_Academic_Year_Term_List($AcademicYearID);
		$x .= '				<select name="YearTerm" id="YearTerm" onchange="Refresh_Subject_Group_Table(); Show_Hide_ToolBar();">';
		$x .= '					<option value="">'.$Lang['SysMgr']['FormClassMapping']['SelectSchoolYearTerm'].'</option>';
		for ($i=0; $i < sizeof($YearTermList); $i++) {
			$YearTermName = Get_Lang_Selection($YearTermList[$i]['YearTermNameB5'],$YearTermList[$i]['YearTermNameEN']);
			
			if (trim($YearTermID) == "") {
				if ($YearTermList[$i]['CurrentTerm'] == '1') 
					$YearTermID = $YearTermList[$i]['YearTermID'];
			}
			
			unset($Selected);
			$Selected = ($YearTermID == $YearTermList[$i]['YearTermID'])? 'selected':'';
			$x .= '				<option value="'.$YearTermList[$i]['YearTermID'].'" '.$Selected.'>'.$YearTermName.'</option>';
		}
		$x .= '				</select>';
		//$x .= ' | ';
		$FormList = $fcm->Get_Form_List(false);
		$x .= '				<!-- Draft Detail layer-->
										<span class="member_list_group_layer" id="FormFilterLayer" style="display:none; visibility: visible; width:280px;">
											<h1>
												<span>'.$Lang['SysMgr']['SubjectClassMapping']['ViewSubjectGroupWithStudentFrom'].'</span>
												<span class="close_group_list"></span>
												<a href="#" title="'.$Lang['SysMgr']['SubjectClassMapping']['ClearFilter'].'" onclick="document.getElementById(\'FilterForm\').reset(); return false;">['.$Lang['Btn']['Clear'].']</a>
						      	 		<a href="#" title="'.$Lang['SysMgr']['SubjectClassMapping']['CloseFilter'].'" onclick="$(\'span#FormFilterLayer\').hide(\'slow\'); return false;">['.$Lang['Btn']['Close'].']</a>
						         		<p class="spacer"></p>
						         	</h1>
						         	<form name="FilterForm" id="FilterForm" onsubmit="return false;">
						          <div class="select_group" align="left">
					';
		for ($i=0; $i< sizeof($FormList); $i++) {				           		
			$x .= '
							<input type="checkbox" name="FormID[]" id="FormID'.$i.'" value="'.$FormList[$i]['YearID'].'"> <label for="FormID'.$i.'">'.$FormList[$i]['YearName'].'</label>
							<br>';
		}
		$x .= '
											<center>
												<input id="FilterFormBtn" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="'.$Lang['Btn']['View'].'" onclick="$(\'span#FormFilterLayer\').hide(\'slow\'); Refresh_Subject_Group_Table();"/>
											</center>
					           	</div>
					           	</form>
						       	</span>
						       <!--Draft Detail layer end -->
						       
						       <!--<span class="member_list_group">
						       	<span class="group_list_name">
						       		<a href="#" class="no_of_draft" onclick="$(\'span#FormFilterLayer\').show(\'slow\'); return false;"> '.$Lang['SysMgr']['SubjectClassMapping']['FilterFormList'].'</a>
						       	</span> 
						       </span>-->';
		//$x .= '				<input type="checkbox" id="HiddenNonRelatedSubject" name="HiddenNonRelatedSubject" value="1" onclick="Refresh_Subject_Group_Table();"> <label for="HiddenNonRelatedSubject">'.$Lang['SysMgr']['SubjectClassMapping']['HideNonRelatedSubject'].'</label>';
		$x .= '				
									</div>
									</td>
									<td valign="bottom">
										<div class="thumb_list" id="ExpandHideGroupBtnLayer">';
		$x .= '					</div></td>
									</tr>
								</table>
					';
		$x .= '<div id="DetailLayer" name="DetailLayer">';
		$x .= $this->Get_Subject_Class_Mapping_Table($AcademicYearID,$YearTermID);
		$x .= '</div>';
		$x .= '			  <!--<div class="common_table_bottom">
									<div class="record_no">Record(s) 1- 15, Total 15</div>
                      			<div class="page_no"> <a href="#" class="prev">&nbsp;</a> <span> Page </span> <span>
                      				<select name="PageNo">
                      					<option>1</option>
                      					</select>
                      				</span> <a href="#" class="next">&nbsp;</a> <span> | Display </span> <span>
                      					<select name="No_of_Page">
                      						<option>10</option>
                      						<option selected="selected">15</option>
                      						</select>
                      					</span> <span>/ Page</span> </div>
                      			</div>-->
                      		</div></td>
                    </tr>
                    </table>';
    $x .= '</div>';
    
    // layer to store class detail
    $x .= '<div id="SubjectGroupDetailLayer" style="display:none;"></div>';
    
    
    return $x;
	}
	
	// disabled on 20090703, by kenneth chung, as replaced by "Get_Subject_Class_Mapping_Table"
	/*function Get_Subject_Class_Mapping_Table1($AcademicYearID,$YearTermID='',$HideGroupLayer=true,$FormID=array()) {
		global $Lang;
		if (trim($YearTermID) == "") return;
		
		$fcm = new form_class_manage();
		$scm = new subject_class_mapping();
		$SubjectList = $scm->Get_Subject_List();
		$FormList = $fcm->Get_Form_List(false);
		$YearTerm = new academic_year_term($YearTermID,false);
		
		$x .= '<table class="common_table_list">
           	<thead>
							<tr>
							  <th colspan="3">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</th>
							  <!--<th colspan="2" class="sub_row_top">
							  <span class="row_content">'.$Lang['SysMgr']['SubjectClassMapping']['Group'].'</span>
			          </th>-->
			          <th colspan="'.(sizeof($FormList)+3).'" class="sub_row_top">&nbsp;</th>
						  </tr>
          	</thead>
          	<col class="num_check" />
          	<col class="num_check" />
          	<col  class="record_title" />
          	<colgroup span="'.(sizeof($FormList)+3).'" class="num_class">
	   			  <tr>
                <th><a href="#" class="sort_asc">'.$Lang['SysMgr']['SubjectClassMapping']['Order'].'</a></th>
      			    <th><a href="#">'.$Lang['SysMgr']['SubjectClassMapping']['SubjectWEBSAMSCode'].'</a></th>
      			    <th><a href="#">'.$Lang['SysMgr']['SubjectClassMapping']['SubjectDescription'].'</a></th>';
    $x .= '	    <th class="sub_row_top">'.$Lang['SysMgr']['SubjectClassMapping']['Group'].'</th>
      			    <th class="sub_row_top">'.$Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'].'</th>
      			    <th class="sub_row_top">'.$Lang['SysMgr']['SubjectClassMapping']['ClassStudent'].'</th>';
    for ($i=0; $i< sizeof($FormList); $i++) {
			$x .= '<th class="sub_row_top">'.$FormList[$i]['YearName'].'</th>';
		}
  	$x .= '    </tr>
                             		
										
						<tbody>';
		for ($i=0; $i< sizeof($SubjectList); $i++) {
			$ShowThisSubject = false;
			$TotalStudentCountOnSubject = 0;
			$TotalStudentCountOnSubjectByForm = array();
			$SubjectGroupDisplay = "";
			
			// Get Subject Group List
			$SubjectGroup = $scm->Get_Subject_Group_List($YearTermID,$SubjectList[$i]['SubjectID']);
			$SubjectGroupList = (is_array($SubjectGroup['SubjectGroupList']))? $SubjectGroup['SubjectGroupList']:array();
			
			// get html for subject group list display
			// coz need to calculate grand total state, so will preload and calculate the total status first
			if (sizeof($SubjectGroupList) > 0) {
				$SubjectGroupDisplay = '	<tr class="sub_row '.$SubjectList[$i]['SubjectID'].'SubjectGroupList" style="display:none;">';
				$SubjectGroupDisplay .= ' <td rowspan="'.sizeof($SubjectGroupList).'" style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
				$SubjectGroupDisplay .= ' <td rowspan="'.sizeof($SubjectGroupList).'" style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
				$SubjectGroupDisplay .= ' <td rowspan="'.sizeof($SubjectGroupList).'" style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
				$IsFirst = 0;
				foreach ($SubjectGroupList as $Key => $Val) {
					$FormStat = $scm->Get_Subject_Group_Stat($YearTermID,$Key);
					$TotalStudentInSubjectGroup = 0;
					$StatDisplay = "";
					for($k=0; $k< sizeof($FormList); $k++) {
						if ((in_array($FormList[$k]['YearID'],$FormID) && $FormStat[$FormList[$k]['YearID']] > 0 && sizeof($FormID) > 0) || sizeof($FormID) == 0) {
							$ShowThisSubject = true;
						}
						
						$StudCount = ($FormStat[$FormList[$k]['YearID']] > 0)? $FormStat[$FormList[$k]['YearID']]:'&nbsp;';
						$Class = ($FormStat[$FormList[$k]['YearID']] > 0)? 'class="rights_selected"':'';
						
						// calculate subject group total student and subject total student by form
						$TotalStudentInSubjectGroup += $FormStat[$FormList[$k]['YearID']];
						$TotalStudentCountOnSubjectByForm[$FormList[$k]['YearID']] += $FormStat[$FormList[$k]['YearID']];
						
						$StatDisplay .= '<td '.$Class.'>'.$StudCount.'</td>';
					}
					
					// calculate Subject Total student
					$TotalStudentCountOnSubject += $TotalStudentInSubjectGroup;
					
					
					$StaffName = (trim($Val['StaffName']) == "")? '&nbsp;':$Val['StaffName'];
					
					if ($IsFirst == 1) {
						$SubjectGroupDisplay .= '	<tr class="sub_row '.$SubjectList[$i]['SubjectID'].'SubjectGroupList" style="display:none;">';
					}
					$SubjectGroupDisplay .= '  	<td><a href="#" onclick="Show_Class_Detail(\''.$Key.'\');">'.Get_Lang_Selection($Val['ClassTitleB5'],$Val['ClassTitleEN']).'</a></td>
									  <td>'.$StaffName.'</td>'; 
					$SubjectGroupDisplay .= '<td>'.$TotalStudentInSubjectGroup.'</td>';
					$SubjectGroupDisplay .= $StatDisplay;
					$SubjectGroupDisplay .= ' </tr>';
					$IsFirst = 1;
				}
			}
			
			if ($ShowThisSubject || sizeof($FormID) == 0) {
				$x .= '		<tr>
										<td>'.$SubjectList[$i]['Sequence'].'&nbsp;</td>
										<td>'.$SubjectList[$i]['WEBSAMSCode'].'</td>
										<td>
											'.$SubjectList[$i]['SubjectDescEN'].'<br>'.$SubjectList[$i]['SubjectDescB5'].'
										</td>
										<td width="25%">
										';
				if (sizeof($SubjectGroupList) > 0) {
					$x .= '
								<span class="row_link_tool">
								<a href="#" id="'.$SubjectList[$i]['SubjectID'].'Link" onclick="Toogle_Show_Subject_Group_Detail(\''.$SubjectList[$i]['SubjectID'].'\'); return false;" class="zoom_in">
								'.sizeof($SubjectGroupList).'
								</a>
								</span>';
				}
				else {
					$x .= '<span style="display:inline; float:left;">
								0
								</span>';
				}
					
											
				if (!$YearTerm->TermPassed) {	
					$x .= '				<span class="table_row_tool row_content_tool" id="'.$SubjectList[$i]['SubjectID'].'CollapseLayer" style="float:right;">
													<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Class_Form(\'\',\''.$SubjectList[$i]['SubjectID'].'\',\'\');" class="add_dim thickbox" title="'.$Lang['SysMgr']['SubjectClassMapping']['AddClass'].'"></a>
												</span>';
				}
				$x .= '				</td>
										<td>
											<span style="float:left;">
											'.$SubjectGroup['StaffCount'].'
											</span>';
				if (!$YearTerm->TermPassed) {	
					$x .= '				<span class="table_row_tool row_content_tool" id="'.$SubjectList[$i]['SubjectID'].'ExpandedLayer" style="display:none; float:right;">
													<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Class_Form(\'\',\''.$SubjectList[$i]['SubjectID'].'\',\'\');" class="add_dim thickbox" title="'.$Lang['SysMgr']['SubjectClassMapping']['AddClass'].'"></a>
												</span>';
				}
				$x .= '			</td>
										<td>'.$TotalStudentCountOnSubject.'</td>';
				for($k=0; $k< sizeof($FormList); $k++) {
					$x .= '			<td>'.$TotalStudentCountOnSubjectByForm[$FormList[$k]['YearID']].'&nbsp;</td>';
				}
				$x .= '	  </tr>';
				
				$x .= $SubjectGroupDisplay;	
			}
		}

		$x .= '	</tbody>
					</table>';
								
		// layer to fake thick box script
		$x .= '<div class="FakeLayer"></div>';
		return $x;
	}*/
	
	function Get_Subject_Class_Mapping_Table($AcademicYearID,$YearTermID='',$HideGroupLayer=true,$FilterFormID=array(),$HideNonRelate=false,$SortingOrder='asc') {
		global $Lang, $sys_custom;
		if (trim($YearTermID) == "") return;
		
		$linterface = new interface_html();
		$fcm = new form_class_manage();
		$scm = new subject_class_mapping();
		$sbj = new subject();
		$SubjectList = $scm->Get_Subject_List($SortingOrder);
		//$FormList = $fcm->Get_Form_List(false);
		//$Temp = $scm->Get_Subject_Group_Stat($YearTermID,$FilterFormID);
		//$YearClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
		//$SubjectGroupStat = is_array($Temp[0])? $Temp[0]:array();
		$SubjectGroupStat = $scm->Get_Subject_Group_Index_By_SubjectID($YearTermID);
		//$FormHasStat = is_array($Temp[1])? $Temp[1]:array();
		//$ClassHasStat = is_array($Temp[2])? $Temp[2]:array();
		$YearTerm = new academic_year_term($YearTermID,false);
		
		### special handling of uccke -> lau sir account can edit past subject group info
		if (in_array($_SESSION['UserID'], (array)$sys_custom['SubjectGroup']['CanEditPastSubjectGroupUserIDArr']))
			$HasTermPast = 0;
		else
			$HasTermPast = $YearTerm->TermPassed;
		
		//for ($i=0; $i< sizeof($YearClassList); $i++) {
		//	$YearClassListByForm[$YearClassList[$i]['YearID']][] = $YearClassList[$i];
		//}
		
		$Colspan = sizeof($FormHasStat)+sizeof($ClassHasStat)+3;
		$SortingClass = ($SortingOrder=='asc')? 'sort_asc' : 'sort_dec';
		$x .= '<table class="common_table_list">
           	<thead>
							<tr>
							  <th colspan="3">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</th>
							  <!--<th colspan="2" class="sub_row_top">
							  <span class="row_content">'.$Lang['SysMgr']['SubjectClassMapping']['Group'].'</span>
			          </th>-->
			          <th class="sub_row_top">&nbsp;</th>
						  </tr>
          	</thead>
          	
          	<col class="num_check" style="width:5%" />
          	<col class="num_check" style="width:5%" />
          	<col class="record_title" style="width:15%" />
          	<colgroup class="num_class" />
	   			  <tr>
                <th><a href="javascript:Refresh_Subject_Group_Table(1);" class="'.$SortingClass.'">'.$Lang['SysMgr']['SubjectClassMapping']['Order'].'</a></th>
      			    <th><a href="#">'.$Lang['SysMgr']['SubjectClassMapping']['SubjectWEBSAMSCode'].'</a></th>
      			    <th><a href="#">'.$Lang['SysMgr']['SubjectClassMapping']['SubjectDescription'].'</a></th>';
    $x .= '	    <th class="sub_row_top">'.$Lang['SysMgr']['SubjectClassMapping']['Group'].'</th>';
      		/*	    <th class="sub_row_top">'.$Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'].'</th>
      			    <th class="sub_row_top">'.$Lang['SysMgr']['SubjectClassMapping']['ClassStudent'].'</th>';
    for ($i=0; $i< sizeof($FormList); $i++) {
    	if (in_array($FormList[$i]['YearID'],$FormHasStat)) {
    		$x .= '<th class="sub_row_top">
    						<a href="#" onclick="Toggle_Class_Stat(\''.$FormList[$i]['YearID'].'\'); return false;">'.$FormList[$i]['YearName'].'</a>
    						<input type="hidden" id="'.$FormList[$i]['YearID'].'ToggleStatus" value="0">
    					</th>';
    		for ($j=0; $j< sizeof($YearClassListByForm[$FormList[$i]['YearID']]); $j++) {
    			if (in_array($YearClassListByForm[$FormList[$i]['YearID']][$j]['YearClassID'],$ClassHasStat)) {
    				$FormClassDisplay[$FormList[$i]['YearID']][] = $YearClassListByForm[$FormList[$i]['YearID']][$j]['YearClassID'];
    				$ClassName = Get_Lang_Selection($YearClassListByForm[$FormList[$i]['YearID']][$j]['ClassTitleB5'],$YearClassListByForm[$FormList[$i]['YearID']][$j]['ClassTitleEN']);
    				$x .= '<th class="sub_row_top '.$FormList[$i]['YearID'].'ClassStatCol" style="display:none">'.$ClassName.'</th>';
    			}
    		}
    	}
	}*/
  	$x .= '    </tr>
						<tbody>';
		
		for ($i=0; $i< sizeof($SubjectList); $i++) {
			$thisSubjectID = $SubjectList[$i]['SubjectID'];
			$SubjectGroupList = $SubjectGroupStat[$thisSubjectID];
			$TotalStudentCountOnSubject = 0;
			$TotalTeacherCountOnSubject = 0;
			$YearClassTotal = array();
			$TotalStudentCountOnSubjectByForm = array();
			$SubjectGroupDisplay = "";
			$EmptySubject = true;
			global $i_subjectGroupMapping_showStat;
			
			$numOfSubjectGroup = count($SubjectGroupList);
			
			// get html for subject group list display
			// coz need to calculate grand total state, so will preload and calculate the total status first
			if (sizeof($SubjectGroupList) > 0) {
				$SubjectGroupDisplay = '	<tr class="sub_row '.$SubjectList[$i]['SubjectID'].'SubjectGroupList" style="display:none;">';
				/*$SubjectGroupDisplay .= ' <td rowspan="'.sizeof($SubjectGroupList).'" style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
				$SubjectGroupDisplay .= ' <td rowspan="'.sizeof($SubjectGroupList).'" style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
				$SubjectGroupDisplay .= ' <td rowspan="'.sizeof($SubjectGroupList).'" style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';*/
				$SubjectGroupDisplay .= ' <td style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
				$SubjectGroupDisplay .= ' <td style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
				$SubjectGroupDisplay .= ' <td style="background-color:#FFFFFF; border-right:1px solid #F1F1F1;">&nbsp;</td>';
				$IsFirst = 0;
				$SubjectGroupDisplay .= '<td style="padding:0px" id="subjectGroupDetails_'.$SubjectList[$i]['SubjectID'].'" >
				<div style="text-align:right;width:100%;"><a href="javascript:void(0)" style="border-bottom:1px solid #CCCCCC;padding-right:3px" class="showStatLink" onclick="showGroupStat('.$SubjectList[$i]['SubjectID'].','.$YearTermID.','.$AcademicYearID.',this)">'.$i_subjectGroupMapping_showStat.'</a></div><table style="border:0px;width:100%;"><tr>';
				$cnt = 0; 
				foreach ($SubjectGroupList as $SubjectGroupID => $SubjectGroupDetail) {
					//$TeacherList = $scm->Get_Subject_Group_Teacher_List($SubjectGroupID);
					//$TotalStudentInSubjectGroup = 0;
					//$TotalTeacherCountOnSubject += sizeof($TeacherList);
					//$StatDisplay = "";
					//$FirstDetail = $SubjectGroupDetail[key($SubjectGroupDetail)][key($SubjectGroupDetail[key($SubjectGroupDetail)])];
					
					$ClassTitle = Get_Lang_Selection($SubjectGroupDetail['SubjectGroupTitleB5'],$SubjectGroupDetail['SubjectGroupTitleEN']);
					
				/*	if (is_array($FormClassDisplay)) {
						foreach ($FormClassDisplay as $YearID => $ClassArray) {
							$ClassStatDisplay = "";
							$FormTotal = 0;
							if (sizeof($SubjectGroupDetail[$YearID]) == 0) {
								$StatDisplay .= '<td>&nbsp;</td>';
								foreach ($ClassArray as $Key => $ClassID) {
									$StatDisplay .= '<td class="'.$YearID.'ClassStatCol" style="display:none;">&nbsp;</td>';
								}
							}
							else {
								foreach ($ClassArray as $Key => $ClassID) {
									$EmptySubject = false;
									$ClassDetail = $SubjectGroupDetail[$YearID][$ClassID];
									$FormTotal += $ClassDetail['ClassTotal'];
									$YearClassTotal[$ClassID] += $ClassDetail['ClassTotal'];
									$ClassStatDisplay .= '<td class="'.$YearID.'ClassStatCol" style="display:none;">'.(($ClassDetail['ClassTotal'] > 0)? $ClassDetail['ClassTotal']:'&nbsp;').'</td>';
								}
								$Class = ($FormTotal > 0)? 'class="rights_selected"':'';
								
								// calculate subject group total student and subject total student by form
								$TotalStudentInSubjectGroup += $FormTotal;
								$TotalStudentCountOnSubjectByForm[$YearID] += $FormTotal;
								
								$StatDisplay .= '<td '.$Class.'>'.$FormTotal.'</td>';
								$StatDisplay .= $ClassStatDisplay;
							}
						}
					} 

					 calculate Subject Total student
					$TotalStudentCountOnSubject += $TotalStudentInSubjectGroup;

					$StaffName = "&nbsp;";
/*					for ($j=0; $j< sizeof($TeacherList); $j++) {
						if ($j == 0)
							$StaffName = $TeacherList[$j]['TeacherName'];
						else 
							$StaffName .= ', <br>'.$TeacherList[$j]['TeacherName'];
					}*/
					if ($IsFirst == 1) {
						$SubjectGroupDisplay .= '	<tr class="sub_row '.$SubjectList[$i]['SubjectID'].'SubjectGroupList" style="display:none;">';
					}
					if ($cnt == count($SubjectGroupList)-1)
						$SubjectGroupDisplay .= '  	<td style="border:0px"><a href="#" onclick="Show_Class_Detail(\''.$SubjectGroupID.'\');">'.$ClassTitle.'</a></td>';
					else
						$SubjectGroupDisplay .= '  	<td style="border-right:0px"><a href="#" onclick="Show_Class_Detail(\''.$SubjectGroupID.'\');">'.$ClassTitle.'</a></td>';
				/*					  									<td>'.$StaffName.'</td>'; 
					$SubjectGroupDisplay .= '<td>'.$TotalStudentInSubjectGroup.'</td>';
					$SubjectGroupDisplay .= $StatDisplay;*/
					$SubjectGroupDisplay .= ' </tr>';
					$IsFirst = 1;
					$cnt++;
				}
				$SubjectGroupDisplay .= '</table> </td></tr>';
			}
			 
			if (!$EmptySubject || !$HideNonRelate) {
				$x .= '		<tr>
										<td>'.$SubjectList[$i]['Sequence'].'&nbsp;</td>
										<td>'.$SubjectList[$i]['WEBSAMSCode'].'</td>
										<td>
											'.$SubjectList[$i]['SubjectDescEN'].'<br>'.$SubjectList[$i]['SubjectDescB5'].'
										</td>
										<td width="25%">
										';
				if (sizeof($SubjectGroupList) > 0) {					
					$x .= '
								<span class="row_link_tool">
								<a href="#" id="'.$SubjectList[$i]['SubjectID'].'Link" onclick="Toogle_Show_Subject_Group_Detail(\''.$SubjectList[$i]['SubjectID'].'\'); return false;" class="zoom_in" title="'.$Lang['SysMgr']['SubjectClassMapping']['ViewGroup'].'">
								'.sizeof($SubjectGroupList).'
								</a>								
								</span>';
				}
				else {
					$x .= '<span style="display:inline; float:left;">
								0
								</span>';
				}
					
											
			if (!$HasTermPast) {	
				$x .= '<span class="table_row_tool row_content_tool" id="'.$SubjectList[$i]['SubjectID'].'CollapseLayer" style="float:right;">';
					### Delete Subject Group
					if ($numOfSubjectGroup == 0)
						$Style = 'style="visibility:hidden;float:right;"';
					else
						$Style = 'style="float:right;"';
					$x .= '<span '.$Style.'>'."\n";
						$x .= $linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['SysMgr']['SubjectClassMapping']['Delete']['SubjectGroupOfThisSubject'], "Delete_Subject_Group('', '$YearTermID', '$thisSubjectID');", $ParClass="", $WithSpan=0);
					$x .= '</span>';
					
					# check Form-Subject linkage
					$formAry = $sbj->Get_Form_By_Subject($thisSubjectID);
					
					$formLinked = (sizeof($formAry)==0) ? 0 : 1; 
					if($formLinked) { # setting finished
						### Add Subject Group
						$x .= '<span style="float:right;">'."\n";
							$x .= '<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Class_Form(\'\',\''.$SubjectList[$i]['SubjectID'].'\',\'\');" class="add_dim thickbox" title="'.$Lang['Btn']['Add'].'"></a>';
						$x .= '</span>'; 
					}
				$x .= '</span>';
			}
				/*$x .= '				</td>
										<td>
											<span style="float:left;">
											'.$TotalTeacherCountOnSubject.'
											</span>';*/
				if (!$HasTermPast) {	
					$x .= '<span class="table_row_tool row_content_tool" id="'.$SubjectList[$i]['SubjectID'].'ExpandedLayer" style="display:none; float:right;">';
						### Delete Subject Group
						if ($numOfSubjectGroup == 0)
							$Style = 'style="visibility:hidden;float:right;"';
						else
							$Style = 'style="float:right;"';
						$x .= '<span '.$Style.'>'."\n";
							$x .= $linterface->GET_LNK_DELETE("javascript:void(0);", $Lang['SysMgr']['SubjectClassMapping']['Delete']['SubjectGroupOfThisSubject'], "Delete_Subject_Group('', '$YearTermID', '$thisSubjectID');", $ParClass="", $WithSpan=0);
						$x .= '</span>';
												
						### Add Subject Group
						$x .= '<span style="float:right;">'."\n";
							$x .= '<a href="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Class_Form(\'\',\''.$SubjectList[$i]['SubjectID'].'\',\'\');" class="add_dim thickbox" title="'.$Lang['Btn']['Add'].'"></a>';
						$x .= '</span>';
						
					$x .= '</span>';
				}
				/*$x .= '			</td>
										<td>'.$TotalStudentCountOnSubject.'</td>';
				for($k=0; $k< sizeof($FormList); $k++) {
					
				}
				for ($j=0; $j< sizeof($FormList); $j++) {
					if (in_array($FormList[$j]['YearID'],$FormHasStat)) {
						$x .= '<td>'.$TotalStudentCountOnSubjectByForm[$FormList[$j]['YearID']].'&nbsp;</td>';
						for ($k=0; $k< sizeof($YearClassListByForm[$FormList[$j]['YearID']]); $k++) {
							if (in_array($YearClassListByForm[$FormList[$j]['YearID']][$k]['YearClassID'],$ClassHasStat)) {
								$YearClassTotalDisplay = ($YearClassTotal[$YearClassListByForm[$FormList[$j]['YearID']][$k]['YearClassID']] == "")? '&nbsp;':$YearClassTotal[$YearClassListByForm[$FormList[$j]['YearID']][$k]['YearClassID']];
								$x .= '<td class="sub_row_top '.$FormList[$j]['YearID'].'ClassStatCol" style="display:none">'.$YearClassTotalDisplay.'</td>';
							}
						}
					}
				}*/
				$x .= '</td> </tr>';
				
				$x .= $SubjectGroupDisplay;	
			}
		}

		$x .= '	</tbody>
					</table>
					 <div id="ajaxBufferingContent" name ="ajaxBufferingContent" style="display:none"></div>
					'.$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'];
								
		// layer to fake thick box script
		$x .= '<div class="FakeLayer"></div>';
		return $x;
	}
	
	/*
	function Get_Class_Short_Info($SubjectGroupID) {
		global $Lang;
		
		$Class = new subject_term_class($SubjectGroupID,true,true);
		
		$x = '<span>
							<u>'.$Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'].'</u><br />';
		for ($i=0; $i< sizeof($Class->ClassTeacherList); $i++) {
			$User = new libuser($Class->ClassTeacherList[$i]);
			$x .= $User->UserNameLang().'<br>';
		}
		$x .= '		<u>'.$Lang['SysMgr']['SubjectClassMapping']['ClassStudent'].'</u><br />
							'.sizeof($Class->ClassStudentList).'													
						</span>';
						
		return $x;
	}*/
	function Get_Teacher_Ownership_Box($teacherList){
		global $i_ec_file_exist_teacher,$i_ec_file_not_exist_teacher,$i_ec_file_no_transfer;
		$row_e = $teacherList['in_class'];
		$row_i = $teacherList['out_class'];//debug_r($row_e); 
		
		$teacher_exist_list = "<select id='teacher_benefit' name='teacher_benefit'>\n";
		$teacher_exist_list .= "<option value='0'>".$i_ec_file_no_transfer."</option>\n";
		$teacher_exist_list .= "<option value=''>____________________</option>";
		$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_exist_teacher." --&nbsp;</option>\n";
		$cours_teacher_size = sizeof($row_e);
		$exclus_email = array();
		for ($i=0; $i<$cours_teacher_size; $i++)
		{
			$teacher_exist_list .= "<option value='".$row_e[$i][0]."'>".$row_e[$i][1]."</option>\n";
			$exclus_email[] = trim($row_e[$i][2]);
		}
		$teacher_exist_list .= "<option value=''>____________________</option>";
		$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_not_exist_teacher." --&nbsp;</option>\n";
		for ($i=0; $i<sizeof($row_i); $i++)
		{
			if (!in_array($row_i[$i][3], $exclus_email))
			{
				$chiname = ($row_i[$i][2]==""? "": "(".$row_i[$i][2].")" );
				$teacher_exist_list .= "<option value='".$row_i[$i][0]."'>".$row_i[$i][1]." $chiname</option>\n";
			}
		}

		$teacher_exist_list .= "</select>\n";
		$teacher_exist_list .= "<input type='hidden' id='noClasseTeacher' name='noClasseTeacher' value='".$cours_teacher_size."'>";
		return $teacher_exist_list;
	}
	
	function Get_Class_Form($YearTermID,$YearID,$SubjectID,$SubjectGroupID,$AcademicYearID) {
		global $Lang,$i_transfer_file_ownership, $PATH_WRT_ROOT, $sys_custom;
		global $eclass_version, $eclass_filepath;		// for libeclass.php
		
		include_once($PATH_WRT_ROOT.'includes/libeclass40.php');
		$leclass = new libeclass();
		
		$fcm = new form_class_manage();
		$SubjectTermClass = new subject_term_class($SubjectGroupID,true,true);
		
		/*
		if ($SubjectGroupID == "") 
			$Subject = new subject($SubjectID);
		else 
			$Subject = new subject($SubjectTermClass->SubjectID);
		*/
		$TargetSubjectID = ($SubjectGroupID == "")? $SubjectID : $SubjectTermClass->SubjectID;
		$Subject = new subject($TargetSubjectID,true);
		
		$SubjectName = $Subject->Get_Subject_Desc();
		$hasEClass = $SubjectTermClass->Has_eclass();
				
		$x .= '<div class="edit_pop_board" style="height:445px;">
						<form name="ClassForm" id="ClassForm" action="" onsubmit="return false;">
						<input type=hidden name="YearTermID" id="YearTermID" value="'.$YearTermID.'">';
		/*
		if ($SubjectGroupID == "") 
			$x .= '<input type=hidden name="SubjectID" id="SubjectID" value="'.$SubjectID.'">';
		else 
			$x .= '<input type=hidden name="SubjectID" id="SubjectID" value="'.$SubjectTermClass->SubjectID.'">';
		*/
		$x .= '<input type=hidden name="SubjectID" id="SubjectID" value="'.$TargetSubjectID.'">';
		$x .= '	<input type=hidden name="SubjectGroupID" id="SubjectGroupID" value="'.$SubjectGroupID.'">';
		/*
		if ($SubjectGroupID == "")
			$x .= '	<h1>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].' &gt; '.$Subject->Get_Subject_Desc().' &gt; <span>'.$Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroup'].'</span></h1>';
		else 
			$x .= '	<h1>'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].' &gt; '.$Subject->Get_Subject_Desc().' &gt; <span>'.$Lang['SysMgr']['SubjectClassMapping']['EditSubjectGroup'].'</span></h1>';
		*/
		$x .= '	<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
							<table class="form_table">
								<tr>
									<td>'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>
									<td>:</td>
									<td>'.$SubjectName.'</td> 
								</tr>'."\n";
								
		if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'])
		{
			$x .= '				<tr>
									<td>'.$Lang['SysMgr']['SubjectClassMapping']['ComponentSubject'].'</td>
									<td>:</td>
									<td>'.$this->Get_Subject_Component_Selection('SubjectComponentID', $TargetSubjectID, $SubjectTermClass->SubjectComponentID).'</td> 
								</tr>'."\n";
		}
								
		$x .= '					<tr>
									<td>'.$Lang['SysMgr']['SubjectClassMapping']['ClassCode'].'</td>
									<td>:</td>
									<td><input name="ClassCode" type="text" id="ClassCode" value="'.htmlspecialchars($SubjectTermClass->ClassCode,ENT_QUOTES).'" onkeyup="Check_Class_Code();" class="textbox"/></td> 
								</tr>
								<tr id="ClassCodeWarningRow" name="ClassCodeWarningRow" style="display:none;">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>
									<span id="ClassCodeWarningLayer" name="ClassCodeWarningLayer" style="color:red;"></span>
									</td>
								</tr>
								<col class="field_title" />
								<col class="field_c" />
								<tr>
									<td>'.$Lang['SysMgr']['SubjectClassMapping']['ClassTitleEN'].'</td>
									<td>:</td>
									<td ><input name="ClassTitleEN" type="text" id="ClassTitleEN" value="'.htmlspecialchars($SubjectTermClass->ClassTitleEN,ENT_QUOTES).'" onkeyup="Check_Class_Title();" class="textbox"/></td> 
								</tr>
								<tr>
									<td>'.$Lang['SysMgr']['SubjectClassMapping']['ClassTitleB5'].'</td>
									<td>:</td>
									<td ><input name="ClassTitleB5" type="text" id="ClassTitleB5" value="'.htmlspecialchars($SubjectTermClass->ClassTitleB5,ENT_QUOTES).'" onkeyup="Check_Class_Title();" class="textbox"/></td> 
								</tr>
								<tr id="ClassTitleWarningRow" name="ClassTitleWarningRow" style="display:none;">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>
									<span id="ClassTitleWarningLayer" name="ClassTitleWarningLayer" style="color:red;"></span>
									</td>
								</tr>
								<tr>
									<td>'.$Lang['SysMgr']['SubjectClassMapping']['FormAssociatedTo'].'</td>
									<td>:</td>
									<td>';
									
		// For select all form
		$x .= '<input type="checkbox" name="checkFormAllBox" value ="1" id="div_checkFormAllBox" onclick="checkAllFormBox(this.checked)"><label for ="div_checkFormAllFormBox">Check All</label><br/>';
		
		// get form association
		$FormList = $Subject->YearRelationDetail;
		for ($i=0; $i< sizeof($FormList); $i++) {
			if (!in_array($FormList[$i]['YearID'],$SubjectTermClass->YearID) && $YearID != $FormList[$i]['YearID'] && $SubjectGroupID != "") 
				$checked = '';
			else 
				$checked = ' checked="checked" ';
				
			$x .= '<input onclick="Update_Class_List(); Check_Selected_Student();" type="checkbox" name="YearID[]" id="YearID[]" '.$checked.' value="'.$FormList[$i]['YearID'].'">'.$FormList[$i]['YearName'];
		}
									
		$x .= '			</td>
							</tr>
							<tr id="YearIDWarningRow" class="YearIDWarningRow" style="display:none;">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>
								<span id="YearIDWarningLayer" name="YearIDWarningLayer" style="color:red;"></span>
								</td>
							</tr>
								<tr>
					<td>'.$Lang['SysMgr']['SubjectClassMapping']['elcassCreated'].'</td>
					<td>:</td>
					<td>';
					/*if ($SubjectGroupID != ""&&$hasEClass){
						$x .= $Lang['General']['Yes']; 
					}
					else{*/
						if ($leclass->license != '' && $leclass->license > 0)
						{
							$QuotaLeft = $leclass->ticket();
							$LicenseMsg = $this->Get_eClass_Licence_Quota_Message($leclass->license, $QuotaLeft);
							
							if ($QuotaLeft==0)
							{
								$thisStyle = 'style="color:red;"';
								$thisDisabled = 'disabled';
							}
							else
							{
								$thisStyle = 'class="tabletextremark"';
								$thisDisabled = '';
							}
							
							$eClassLicenseWarning = ' <span '.$thisStyle.'>('.$LicenseMsg.')</span>';
						}
						
						$x .= "<div>";
							$x .= "<input type='radio' value='1' name='isCreateClass' ".($SubjectGroupID != ""&&$hasEClass?'checked="checked"':'')." onclick='$(this).nextAll(\"div\").show()' > ".$Lang['General']['Yes'];
							$x .= "<input type='radio' value='0' name='isCreateClass' ".($SubjectGroupID != ""&&$hasEClass?'':'checked="checked"')." onclick='$(this).nextAll(\"div\").hide()'> ".$Lang['General']['No'];
							$x .= "&nbsp;&nbsp;";
							$x .= $eClassLicenseWarning;
							$x.="<div style='".($SubjectGroupID != ""&&$hasEClass?'':'display:none;')."' >";
							$x .= "<input type='radio' onclick='$(\"#eclassSelecting\").attr(\"disabled\",\"disabled\")' value='1' name='createNew' $thisDisabled > ".$Lang['SysMgr']['SubjectClassMapping']['createNew'];
							$x .= "<input type='radio' value='0' name='createNew' onclick='$(\"#eclassSelecting\").attr(\"disabled\",\"\")' checked=\"checked\"> ".$Lang['SysMgr']['SubjectClassMapping']['selectFromExisting'];
							$x.=$leclass->getSelectEclass('id="eclassSelecting" name="eclass"',$SubjectTermClass->course_id);
							
							$checked = ($sys_custom['SubjectGroup']['DefaultSyncStudentToClassroom'])?'checked="checked"':'';
							$x .= "<br /><input type='checkbox' value='1' name='addStd' {$checked}> ".$Lang['SysMgr']['SubjectClassMapping']['newStd2eclass']."
							</div>";
						$x .= "</div>";
						
					//}
			$x .= "</td>";
		$x .= "</tr>";
		$x .= '<tr>
					<td>'.$Lang['SysMgr']['SubjectClassMapping']['GroupTeacher'].'</td>
					<td>:</td>
					<td>';
		$x .= '<div id="teacherListDiv">';
		// get teacher list
		$TeacherList = $fcm->Get_Teaching_Staff_List();
		
		$x .= '<select name="TeacherList" id="TeacherList" onchange="Add_Teaching_Teacher();">
						<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
		for ($i=0; $i< sizeof($TeacherList); $i++) {
			$Hide = false;
			for ($j=0; $j< sizeof($SubjectTermClass->ClassTeacherList); $j++) {
				if ($TeacherList[$i]['UserID'] == $SubjectTermClass->ClassTeacherList[$j]['UserID']) {
					$Hide = true;
					break;
				}
			}
			if (!$Hide) 
				$x .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
		}
		$x .= '</select>';
		$x .= '</div>';		
							
		$x .= '			<p class="spacer"></p>';
		// tempory selection box to store class teacher
		$x .= '     <select name="SelectedClassTeacher[]" size="3" id="SelectedClassTeacher[]" style="width:50%" multiple="true">';
		for ($i=0; $i< sizeof($SubjectTermClass->ClassTeacherList); $i++) {
			$x .= '<option value="'.$SubjectTermClass->ClassTeacherList[$i]['UserID'].'">'.$SubjectTermClass->ClassTeacherList[$i]['TeacherName'].'</option>';
		}
		$x .= '			</select>';
								
		$x .= '			<p class="spacer"></p>
								<input onclick="Remove_Teaching_Teacher();" name="submit2" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="'.$Lang['Btn']['RemoveSelected'].'"/></td></tr>';
		if($hasEClass){
			$teacher_List = $SubjectTermClass->Get_Existing_Teacher_List();
			$listBox = $this->Get_Teacher_Ownership_Box($teacher_List);
			$x .= '<tr>
					<td>'.$i_transfer_file_ownership.'</td><td>:</td>
					<td>
						<div id="transferOwnership" style="display:block;">	
						<input type="hidden" name="is_user_import" id="is_user_import" value="0">
							'.$listBox.'
						</div>
					 </td>
					</tr> 
					';
		}			
				$x .= '				
							<tr>
								<td>'.$Lang['SysMgr']['FormClassMapping']['ClassStudent'].'</td>
								<td>:</td>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td width="50%" bgcolor="#EEEEEE">
											<div name="YearClassSelectLayer" id="YearClassSelectLayer">';
			
		if (trim($SubjectGroupID) == "") 
			$x .= $this->Get_Class_List_Selection($AcademicYearID, array($YearID), $Lang['SysMgr']['FormClassMapping']['AddFromFormClass']);
		else 
	 		$x .= $this->Get_Class_List_Selection($AcademicYearID, $SubjectTermClass->YearID, $Lang['SysMgr']['FormClassMapping']['AddFromFormClass']);
		
	  $x .= '						</div>
											</td>
											<td width="40"></td>
											<td width="50%" bgcolor="#EFFEE2" class="steptitletext">'.$Lang['SysMgr']['FormClassMapping']['StudentSelected'].' </td>
										</tr>
										<tr>
											<td bgcolor="#EEEEEE" align="center">';
		// class student list of selected class
		$x .= '						<div id="ClassStudentList">
											<select name="ClassStudent" id="ClassStudent" size="10" style="width:99%" multiple="true">';
		$x .= '						</select>
											</div>';
											
		$x .= '
											</td>
											<td><input name="AddAll" onclick="Add_All_Student();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;&gt;" style="width:40px;" title="'.$Lang['Btn']['AddAll'].'"/>
												<br />
												<input name="Add" onclick="Add_Selected_Class_Student();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&gt;" style="width:40px;" title="'.$Lang['Btn']['AddSelected'].'"/>
												<br /><br />
												<input name="Remove" onclick="Remove_Selected_Student(); Check_Selected_Student();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;" style="width:40px;" title="'.$Lang['Btn']['RemoveSelected'].'"/>
												<br />
												<input name="RemoveAll" onclick="Remove_All_Student(); Check_Selected_Student();" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="&lt;&lt;" style="width:40px;" title="'.$Lang['Btn']['RemoveAll'].'"/></td>
											<td bgcolor="#EFFEE2">';
		// selected student list									
		$x .= '<select name="StudentSelected[]" id="StudentSelected[]" size="10" style="width:99%" multiple="true">';
		for ($i=0; $i< sizeof($SubjectTermClass->ClassStudentList); $i++) {
			//$TempUser = new libuser($SubjectTermClass->ClassStudentList[$i]);
			$UserName = $SubjectTermClass->ClassStudentList[$i]['StudentName'];
			$thisClassName = Get_Lang_Selection($SubjectTermClass->ClassStudentList[$i]['ClassTitleEN'], $SubjectTermClass->ClassStudentList[$i]['ClassTitleB5']);
			$thisClassNumber = $SubjectTermClass->ClassStudentList[$i]['ClassNumber'];
			
			$thisDisplay = $UserName.' ('.$thisClassName.'-'.$thisClassNumber.')';
			
			$x .= '<option value="'.$SubjectTermClass->ClassStudentList[$i]['UserID'].'">'.$thisDisplay.'</option>';
		}
		$x .= '</select>';
		$x .= '						</td>
										</tr>
										<tr>
											<td width="50%" bgcolor="#EEEEEE">';
		$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].' </span><br />';
		$x .= $Lang['SysMgr']['FormClassMapping']['Or'].'<Br>';
		$x .= $Lang['SysMgr']['FormClassMapping']['SearchStudent'];
		
		$x .= '
												<!--<div class="Conntent_search" style="float:left;">-->
												<div style="float:left;">
													<input type="text" id="StudentSearch" name="StudentSearch" value=""/>
												</div>
											</td>
											<td>&nbsp;</td>
											<td><div id="StudentSelectedWarningLayer" name="StudentSelectedWarningLayer" style="color:red;"></div></td>
										</tr>
									</table>
									
									<p class="spacer"></p>
									</td>
							</tr>';
		$x .= '		</table>
							<br />
						</form>
						</div>
						<div class="edit_bottom">';
		if (trim($SubjectGroupID) != "") 
			$x .=  '<span> '.Get_Last_Modified_Remark($SubjectTermClass->DateModified,$SubjectTermClass->ModifiedByName).'</span>';
		
		$x .= '	<p class="spacer"></p>';
		if ($SubjectGroupID == "") 
			$x .= '		<input onclick="Create_Subject_Group();" name="SubmitBtn" id="SubmitBtn" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Submit'].'" />';
		else 
			$x .= '		<input onclick="Check_Timetable_Conflict();" name="SubmitBtn" id="SubmitBtn" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Submit'].'" />';
		$x .= '		<input onclick="window.top.tb_remove();" name="submit2" type="button" class="formbutton" onclick="" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />';
							
		//$x .= '		<input name="submit2" type="button" class="formbutton" onclick="MM_goToURL(\'parent\',\'school_setting_subject_class.htm\');return document.MM_returnValue" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="Remove this class" />';
		
		$x .= '		<p class="spacer"></p>
						</div>
					</div>';
		
		return $x;
	}
	
	function Get_Class_Student_Selection($YearClassID,$YearTermID,$SubjectID,$SelectedStudent,$SubjectGroupID,$WithFormID=0) {
		$scm = new subject_class_mapping();
		
		$UserList = $scm->Get_Avaliable_Year_Class_User($YearClassID,$YearTermID,$SubjectID,$SelectedStudent,$SubjectGroupID,$WithFormID);
		$x .= '<select name="ClassStudent" id="ClassStudent" size="10" style="width:99%" multiple="true">';
		for ($i=0; $i< sizeof($UserList); $i++) {
			$TempUser = new libuser($UserList[$i]['UserID']);
			$x .= '<option value="'.$UserList[$i]['UserID'].'">';
			$x .= $TempUser->UserNameLang().' ('.Get_Lang_Selection($UserList[$i]['ClassTitleB5'],$UserList[$i]['ClassTitleEN']).'-'.$UserList[$i]['ClassNumber'].')';
			$x .= '</option>';
		}
		$x .= '</select>';
											
		return $x;
	}
	
	function Get_Class_List_Selection($AcademicYearID,$YearIDs=array(),$Title='') {
//		global $Lang; 
//		
//		$fcm = new form_class_manage();
//		
//		if ($Title == '')
//			$Title = $Lang['SysMgr']['SubjectClassMapping']['SelectClass'];
//		
//		// get class list 
//		$ClassList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
//		$x .= '<select name="YearClassSelect" id="YearClassSelect" class="formtextbox" onchange="Get_Class_Student_List();">';
//		$x .= '<option value="">'.$Title.'</option>';
//	  for ($i=0; $i< sizeof($ClassList); $i++) {
//	  	if (in_array($ClassList[$i]['YearID'],$YearIDs)) {
//		  	if ($ClassList[$i]['YearID'] != $ClassList[$i-1]['YearID']) {
//		  		if ($i == 0) 
//		  			$x .= '<optgroup label="'.$ClassList[$i]['YearName'].'">';
//		  		else {
//		  			$x .= '</optgroup>';
//		  			$x .= '<optgroup label="'.$ClassList[$i]['YearName'].'">';
//		  		}
//		  	}
//		  	
//		  	$x .= '<option value="'.$ClassList[$i]['YearClassID'].'">'.Get_Lang_Selection($ClassList[$i]['ClassTitleB5'],$ClassList[$i]['ClassTitleEN']).'</option>';
//		  	
//		  	if ($i == (sizeof($ClassList)-1)) 
//		  		$x .= '</optgroup>';
//		  }
//	  }
//	  $x .= '</select>';

		global $PATH_WRT_ROOT, $Lang; 
		
		include_once($PATH_WRT_ROOT.'includes/libclass.php');
		$lclass = new libclass();
		
		$Title = ($Title=='')? $Lang['SysMgr']['FormClassMapping']['AddFromFormClass'] : $Title;

		$SelectionTag = 'name="YearClassSelect" id="YearClassSelect" class="formtextbox" onchange="Get_Class_Student_List();"';								
		$x = $lclass->getSelectClassWithWholeForm($SelectionTag, $selected="", $Title, $pleaseSelect="", $AcademicYearID, $YearIDs);
	  
	  return $x;
	}
	
	function Get_Subject_Class_Detail($AcademicYearID, $YearTermID, $SubjectGroupID, $OrderByStudentName, $SortingOrder) {
		global $Lang, $PATH_WRT_ROOT, $LAYOUT_SKIN, $sys_custom; 
		
		$libFCM = new form_class_manage();
		
		$AcademicYearTerm = new academic_year_term($YearTermID,true);
		$SubjectGroup = new subject_term_class($SubjectGroupID,true);
		
		$objSubject = new subject($SubjectGroup->SubjectID);
		$SubjectName = $objSubject->Get_Subject_Desc();
		
		$OnChange = "Show_Class_Detail(this.value);";
		$SubjectGroupSelection = $this->Get_Subject_Group_Selection($SubjectGroup->SubjectID, 'SubjectGroupID', $SubjectGroupID, $OnChange, $YearTermID);
		
		if ($SubjectGroup->Has_eclass()){
			$x .= '<input type="hidden" value="1" name="has_eclass" id="has_eclass">';
		}
		else{
			$x .= '<input type="hidden" value="0" name="has_eclass" id="has_eclass">';
		}
		$x .= '<table width="99%" border="0" cellspacing="0" cellpadding="0">
						<tr> 
						<td class="main_content">
							<div class="navigation">
								<img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif" width="15" height="15" align="absmiddle" />
								<a href="#" onclick="Refresh_Subject_Group_Table(\'Table\'); return false;">
									'.$AcademicYearTerm->Get_Academic_Year_Name().' ('.$AcademicYearTerm->Get_Year_Term_Name().')
									'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupList'].'
								</a>
								<img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif" width="15" height="15" align="absmiddle" />
								'.$SubjectName.'
								<img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif" width="15" height="15" align="absmiddle" />
								'.$SubjectGroup->Get_Class_Title().'
								<br />
								<br />
							</div>
              <div class="content_top_tool">
								<div class="Conntent_tool">
									<!--<a href="#" class="export"> '.$Lang['SysMgr']['SubjectClassMapping']['Export'].'</a> 
									<a href="#" class="print"> '.$Lang['SysMgr']['SubjectClassMapping']['Print'].'</a> -->
								</div>
              	<div class="Conntent_search">
									<!--<input name="SubjectGroupDetailSearchValue" type="text"/>-->
								</div>
               <br style="clear:both" />
							</div>
              <div class="table_board">
               	<table class="form_table">
                  <tr>
                  <td colspan="3" class="form_sub_title">
                  	<em>'.$Lang['SysMgr']['FormClassMapping']['BasicClassInfo'].'</em>
                    <p class="spacer"></p>
                    <span class="row_content tabletextremark"> '.Get_Last_Modified_Remark($SubjectGroup->DateModified,$SubjectGroup->ModifiedByName).'</span>
                    <div class="table_row_tool row_content_tool">';
                    
		if ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] || (in_array($_SESSION['UserID'], (array)$sys_custom['SubjectGroup']['CanEditPastSubjectGroupUserIDArr'])))
			$showEditDeleteBtn = true;
		else
			$showEditDeleteBtn = !$AcademicYearTerm->TermPassed;
		            
		if ($showEditDeleteBtn) {
    	$x .= '         <input alt="#TB_inline?height=450&width=750&inlineId=FakeLayer" onclick="Get_Class_Form(\'\',\'\',\''.$SubjectGroup->SubjectGroupID.'\');" name="Edit" id="Edit" type="button" class="thickbox formsmallbutton" onmouseover="this.className=\'thickbox formsmallbuttonon\'" onmouseout="this.className=\'thickbox formsmallbutton\'" value="'.$Lang['Btn']['EditThisGroup'].'" title="'.$Lang['Btn']['EditThisGroup'].'"/>
                      <input onclick="Delete_Subject_Group(\''.$SubjectGroup->SubjectGroupID.'\');" name="submit" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="'.$Lang['SysMgr']['SubjectClassMapping']['RemoveThisGroup'].'" />';
    }
    $x .= '         </div>
                  </td>
                  </tr>
                  <tr>
                  	<td>'.$Lang['SysMgr']['SubjectClassMapping']['ClassCode'].' </td>
                  	<td>:</td>
                  	<td>
	                  	<span class="table_filter">
	                   	'.$SubjectGroup->ClassCode.'
	                  	</span>
	                  </td>
                  </tr>
                  <col class="field_title" />
                  <col class="field_c" />
                  <tr>
                    <td nowrap>'.$Lang['SysMgr']['SubjectClassMapping']['ClassTitle'].'</td>
                    <td>:</td>
                    <td >
						<span class="table_filter">
                      		'.$SubjectGroupSelection.'
                    	</span>
					</td>
                  </tr>'."\n";
                  
		if ($sys_custom['SubjectGroup']['CanMapSubjectGroupToSubjectComponent'])
		{
			if ($SubjectGroup->SubjectComponentID > 0)
			{
				$ObjSubjectComponent = new Subject_Component($SubjectGroup->SubjectComponentID);
				$thisDisplay = Get_Lang_Selection($ObjSubjectComponent->CH_DES, $ObjSubjectComponent->EN_DES);
			}
			else
			{
				$thisDisplay = '---';
			}
			
			$x .= '	  <tr>
	                    <td nowrap>'.$Lang['SysMgr']['SubjectClassMapping']['ComponentSubject'].'</td>
	                    <td>:</td>
	                    <td >
	                      '.$thisDisplay.' 
	                   </td>
	                  </tr>'."\n";
		}
		
		### Locked YearClass
		if ($sys_custom['SubjectGroup']['LockLogic'])
		{
			if ($SubjectGroup->LockedYearClassID == '')
				$thisDisplay = $Lang['General']['EmptySymbol'];
			else
			{
				$YearClassObj = new year_class($SubjectGroup->LockedYearClassID);
				$thisDisplay = $YearClassObj->Get_Class_Name();
			}
			$x .= '	  <tr>
	                    <td nowrap>'.$Lang['SysMgr']['SubjectClassMapping']['LinkedClass'].'</td>
	                    <td>:</td>
	                    <td >
	                      '.$thisDisplay.' 
	                   </td>
	                  </tr>';
		}
		
		
		$x .= '	  <tr>
                    <td nowrap>'.$Lang['SysMgr']['SubjectClassMapping']['elcassCreated'].'</td>
                    <td>:</td>
                    <td >
                      '.($SubjectGroup->Has_eclass()?$Lang['General']['Yes']:$Lang['General']['No']).' 
                   </td>
                  </tr>
                  <tr>
                    <td>'.$Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'].'</td>
                    <td>:</td>
                    <td>';
		for ($i=0; $i< sizeof($SubjectGroup->ClassTeacherList); $i++) {
			if ($i != 0) 
				$x .= ', <br>';
			$x .= $SubjectGroup->ClassTeacherList[$i]['TeacherName'];
		}
		
	$ClassNameClass = '';
    $StudentNameClass = '';
   if ($OrderByStudentName == 1)
    {
		$StudentNameClass = ($SortingOrder=='asc')? 'class="sort_asc"' : 'class="sort_dec"';
	}
    else
    {
		$ClassNameClass = ($SortingOrder=='asc')? 'class="sort_asc"' : 'class="sort_dec"';
	}
    
	
    
    $x .= '         </td>
                  </tr>
                          <tr>
                  <td>'.$Lang['SysMgr']['FormClassMapping']['ClassStudent'].'</td>
                  <td>:</td>
                  <td><table class="common_table_list">
                    <thead>
                      <tr>
                      	<th width="5%">#</th>
                        <th width="10%"><a href="javascript:Show_Class_Detail('.$SubjectGroupID.', 0, \''.$SortingOrder.'\')" '.$ClassNameClass.'>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</a></th>
                        <th width="35%"><a href="javascript:Show_Class_Detail('.$SubjectGroupID.', 1, \''.$SortingOrder.'\')" '.$StudentNameClass.'>'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</a></th>
                        <th class="num_check">&nbsp;</th>
                        <th width="5%">#</th>
                        <th width="10%"><a href="javascript:Show_Class_Detail('.$SubjectGroupID.', 0, \''.$SortingOrder.'\')" '.$ClassNameClass.'>'.$Lang['SysMgr']['FormClassMapping']['ClassNo'].'</a></th>
                        <th width="35%"><a href="javascript:Show_Class_Detail('.$SubjectGroupID.', 1, \''.$SortingOrder.'\')" '.$StudentNameClass.'>'.$Lang['SysMgr']['FormClassMapping']['StudentName'].'</a></th>
                      </tr>
                    </thead>
                    <tbody>';
                    
                    
    # Get the student list with ordering   
    $SubjectGroup->ClassStudentList = $SubjectGroup->Get_Subject_Group_Student_List($OrderByStudentName, $SortingOrder);
    
    for ($i=0; $i< ((sizeof($SubjectGroup->ClassStudentList)/2)); $i++) {
    	$FirstIndex = $i;
    	$SecondIndex = round(sizeof($SubjectGroup->ClassStudentList)/2) + $i;
    	
    	$firstIndexStudentID = $SubjectGroup->ClassStudentList[$FirstIndex]['UserID'];
    	$secondIndexStudentID = $SubjectGroup->ClassStudentList[$SecondIndex]['UserID'];
    	
    	//$User1 = new libuser($firstIndexStudentID);
    	//$User2 = new libuser($secondIndexStudentID);
    	$UserName1 = $SubjectGroup->ClassStudentList[$FirstIndex]['StudentName'];
    	$UserName2 = $SubjectGroup->ClassStudentList[$SecondIndex]['StudentName'];
    	
    	/*
    	$ClassNumber1 = (trim($User1->ClassNumber)=="")? $Lang['SysMgr']['FormClassMapping']['ClassNumberNotYetSet']:$User1->ClassName.'-'.$User1->ClassNumber;
    	if (trim($SubjectGroup->ClassStudentList[$SecondIndex]) != "")
    		$ClassNumber2 = (trim($User2->ClassNumber)=="")? $Lang['SysMgr']['FormClassMapping']['ClassNumberNotYetSet']:$User2->ClassName.'-'.$User2->ClassNumber;
    	else 
    		$ClassNumber2 = '&nbsp;';
    	*/
    	
    	$ClassName1 = Get_Lang_Selection($SubjectGroup->ClassStudentList[$FirstIndex]['ClassTitleB5'], $SubjectGroup->ClassStudentList[$FirstIndex]['ClassTitleEN']);
    	$ClassName2 = Get_Lang_Selection($SubjectGroup->ClassStudentList[$SecondIndex]['ClassTitleB5'], $SubjectGroup->ClassStudentList[$SecondIndex]['ClassTitleEN']);
    	$ClassNumber1 = $SubjectGroup->ClassStudentList[$FirstIndex]['ClassNumber'];
    	$ClassNumber2 = $SubjectGroup->ClassStudentList[$SecondIndex]['ClassNumber'];
    	$IndexDisplay1 = $SubjectGroup->ClassStudentList[$FirstIndex]?($FirstIndex+1):'&nbsp;';
    	$IndexDisplay2 = $SubjectGroup->ClassStudentList[$SecondIndex]?($SecondIndex+1):'&nbsp;';
    	
    	$ClassNumber1 = (trim($ClassNumber1)=="")? $Lang['SysMgr']['FormClassMapping']['ClassNumberNotYetSet'] : $ClassName1.'-'.$ClassNumber1;
    	if ($SubjectGroup->ClassStudentList[$SecondIndex])
    		$ClassNumber2 = (trim($ClassNumber2)=="")? $Lang['SysMgr']['FormClassMapping']['ClassNumberNotYetSet'] : $ClassName2.'-'.$ClassNumber2;
    	else 
    		$ClassNumber2 = '&nbsp;';
    	
    	$x .= '         <tr>
    					<td width="5%">'.$IndexDisplay1.'</td>
                        <td width="10%">'.$ClassNumber1.'</td>
                        <td width="39%">'.$UserName1.'&nbsp;</td>
                        <td width="2%">&nbsp;</td>
                        <td width="5%">'.$IndexDisplay2.'</td>
                        <td width="10%">'.$ClassNumber2.'</td>
                        <td width="39%">'.$UserName2.'&nbsp;</td>
                      </tr>';
    }
		$x .= '						</tbody>
                  </table>
                  '.$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'].'
                  <br />
                  <br />
                <a href="#"></a> </td>
              </tr>
                        </table>
           	            <p class="spacer"></p>
								</td>
					</tr>
					</table>';
		
		return $x;
	}
	
	function Get_Batch_Create_Class($AcademicYearID,$YearTermID) {
		global $Lang,$PATH_WRT_ROOT,$LAYOUT_SKIN; 
		global $eclass_version, $eclass_filepath;		// for libeclass.php
		
		include_once($PATH_WRT_ROOT.'includes/libeclass40.php');
		$leclass = new libeclass();
		
		$scm = new subject_class_mapping();
		$fcm = new form_class_manage();
		$x .= '<div class="edit_pop_board" style="height:400px;">
					<div class="edit_pop_board_write" style="height:374px;">
					<form name="BatchCreateClassForm" id="BatchCreateClassForm" onsubmit="return false;">
					<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="'.$AcademicYearID.'">
					<input type="hidden" name="YearTermID" id="YearTermID" value="'.$YearTermID.'">
					<fieldset class="instruction_box">
						<legend class="instruction_title">'.$Lang['SysMgr']['SubjectClassMapping']['Instruction'].'</legend>
							'.$Lang['SysMgr']['SubjectClassMapping']['Instruction1'].'<br>
							<span class="tabletextremark">'.$Lang['SysMgr']['SubjectClassMapping']['Note'].': '.$Lang['SysMgr']['SubjectClassMapping']['BatchCreateNote'].'</span>
					</fieldset>
					
						<table width="97%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td width="50%" align="left" valign="top"><span class="steptitletext">'.$Lang['SysMgr']['SubjectClassMapping']['SelectSubjects'].'</span></td>
								<td width="50%" align="left" valign="top" class="steptitletext">'.$Lang['SysMgr']['SubjectClassMapping']['SelectClasses'].'</td>
							</tr>
							<tr>
								<td align="left" valign="top">
								<div style="height:221px; overflow:auto;">
								<table class="common_table_list" style="width:320px;">
									<tbody>';
		$SubjectList = $scm->Get_Subject_List();
		for ($i=0; $i< sizeof($SubjectList); $i++) {
			$x .= '				<tr>
											<td><input type="checkbox" name="SubjectID[]" id="SubjectID[]" class="SubjectChk" value="'.$SubjectList[$i]['SubjectID'].'"/></td>
											<td>'.$SubjectList[$i]['WEBSAMSCode'].'</td>
											<td>'.$SubjectList[$i]['SubjectDescEN'].'<br />
												'.$SubjectList[$i]['SubjectDescB5'].'</td>
											</tr>';
		}
		$x .= '				</tbody>
								</table>
								</div>
								</td>
								<td align="left" valign="top">
								<div style="height:221px; overflow:auto;">
								<table class="common_table_list" style="width:320px;">
									<tbody>';
		$FormList = $fcm->Get_Form_List(false);
		for($i=0; $i< sizeof($FormList); $i++) {
			$thisYearID = $FormList[$i]['YearID'];
			$thisYearName = $FormList[$i]['YearName'];
			
			$x .= '						<tr>
												<td>'.$thisYearName.'</td>
												<td><input type="checkbox" name="'.$thisYearID.'-SelectAll" id="'.$thisYearID.'-SelectAll" value="1" onclick="js_Select_All_Class('.$thisYearID.', this.checked)" />
													'.$Lang['SysMgr']['FormClassMapping']['AllClass'].'<br /> ';
			$ClassList = $fcm->Get_Class_List($AcademicYearID, $thisYearID);
			for ($j=0; $j< sizeof($ClassList); $j++) {
				$thisYearClassID = $ClassList[$j]['YearClassID'];
				$x .= ' <input type="checkbox" name="'.$thisYearID.'[]" id="'.$thisYearID.'[]" value="'.$thisYearClassID.'" class="ClassChk ClassChk_'.$thisYearID.'" onclick="js_UnCheck_SelectAll('.$thisYearID.', this.checked)" />'.Get_Lang_Selection($ClassList[$j]['ClassTitleB5'],$ClassList[$j]['ClassTitleEN']);
			}
			$x .= '					</td>
										</tr>';
		}
		$x .= '
									</tbody>
								</table>
								</div>
								</td>
							</tr>
						</table>
						<br />
						<input type="checkbox" name="isCreateEclass" id="isCreateEclass" /><label for="isCreateEclass">'.$Lang['SysMgr']['SubjectClassMapping']['Createelcass'].'</label>';
		
		### Show quota of eClass
		if ($leclass->license != '' && $leclass->license > 0)
		{
			$QuotaLeft = $leclass->ticket();
			$LicenseMsg = $this->Get_eClass_Licence_Quota_Message($leclass->license, $QuotaLeft);
			
			$thisStyle = ($QuotaLeft==0)? 'style="color:red;"' : 'class="tabletextremark"';
			$x .= ' <span '.$thisStyle.'>('.$LicenseMsg.')</span>';
			$x .= ' <input type="hidden" id="eClassLicenseLeft" name="eClassLicenseLeft" value="'.$QuotaLeft.'" /> ';
		}
		
				
		$x .= '		</form>
					</div>
					<div class="edit_bottom" style="height:10px;">
						<p class="spacer"></p>
						<input name="submit2" type="button" class="formbutton" onclick="Check_Batch_Create_Subject_Group();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Submit'].'" />
						<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
						<!--<p class="spacer"></p>-->
					</div>
				</div>';
		return $x;
	}
	
	function Get_Subject_Selection_ForOldSDAS($ID_Name, $Selected='', $OnChange='', $noFirst='0', $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=1, $IsMultiple=0, $IncludeSubjectIDArr='', $ExtrudeSubjectIDArr=array())
	{
		global $Lang;
		
		if ($OnFocus != '')
			$onfocus = 'onfocus="'.$OnFocus.'"';
		
		if ($OnChange != '')
			$onchange = 'onchange="'.$OnChange.'"';
			
		if ($IsMultiple == 1)
			$multiple = 'multiple="1" size="10"';
			
		# Use current term if not specified
		if ($YearTermID == '')
		{
			$YearTermArr = getCurrentAcademicYearAndYearTerm();
			$YearTermID = $YearTermArr['YearTermID'];
		}
		
		$libLC = new Learning_Category();
		$LC_Arr = $libLC->Get_All_Learning_Category();
		$numOfLC = count($LC_Arr);
		
		$libSubject = new subject();
		$SubjectArr = $libSubject->Get_Subject_List(1);
		
		$select = '';
		$select .= '<select name="'.$ID_Name.'" id="'.$ID_Name.'" class="formtextbox" '.$onchange.' '.$onfocus.' '.$multiple.'>';
			
		if (!$noFirst)
			$select .= '<option value="">'.Get_Selection_First_Title($firstTitle).'</option>';
			
		$scm = new subject_class_mapping();
		$SubjectGroupInfoArr = $scm->Get_Subject_Group_Info();

		if($FilterSubjectWithoutSG==1)
		{
			$AllSubjectGroupInfoArr = $scm->Get_Subject_Group_List($YearTermID);
			$AllSubjectGroupInfoArr = BuildMultiKeyAssoc($AllSubjectGroupInfoArr['SubjectGroupList'], array("SubjectID","SubjectGroupID"));
		}
		
		$subjectCounter = 1;

		for ($i=0; $i<$numOfLC; $i++)
		{
			$thisLC_ID = $LC_Arr[$i]['LearningCategoryID'];
			$thisLC_Name = Get_Lang_Selection($LC_Arr[$i]['NameChi'], $LC_Arr[$i]['NameEng']);
			$thisSubjectArr = $SubjectArr[$thisLC_ID];
	
			if (!is_array($thisSubjectArr) || count($thisSubjectArr) == 0) continue;
			
//			$select .= '<optgroup label="'.$thisLC_Name.'">';

			$options = '';
			foreach($thisSubjectArr as $thisSubjectID => $thisSubjectInfoArr)
			{
				if (is_array($IncludeSubjectIDArr) && !in_array($thisSubjectID, $IncludeSubjectIDArr))
					continue;
				
				if (is_array($ExtrudeSubjectIDArr) && in_array($thisSubjectID, $ExtrudeSubjectIDArr))
					continue;
					
//				$SubjectGroupInfoArr = $scm->Get_Subject_Group_List($YearTermID,$thisSubjectID);
//				$SubjectGroupArr = $SubjectGroupInfoArr['SubjectGroupList'];
				
				# cannot select subject which do not have subject group
				if ($FilterSubjectWithoutSG==1)
				{
					$SubjectGroupArr = $AllSubjectGroupInfoArr[$thisSubjectID];
					if(count($SubjectGroupArr) == 0)
						continue;
				}	
				
				$thisNameChi = intranet_htmlspecialchars($thisSubjectInfoArr['CH_DES']);
				$thisNameEng = intranet_htmlspecialchars($thisSubjectInfoArr['EN_DES']);
				$thisSubjectName = Get_Lang_Selection($thisNameChi, $thisNameEng);
				
				$select_tag = "";
				if ($thisSubjectID == $Selected || ($subjectCounter==1 && $noFirst))
					$select_tag = "selected";
					
				$options .= '<option value="'.$thisSubjectID."_0".'" '.$select_tag.'>'.$thisSubjectName.'</option>';
				$subjectCounter++;
			}
			if(trim($options)!='')
			{
				$select .= '<optgroup label="'.$thisLC_Name.'">';
					$select .= $options;
				$select .= '</optgroup>';
			}
		}
		
		$select .= '</select>';
		
		return $select;
	}
	
	function Get_Subject_Selection($ID_Name, $Selected='', $OnChange='', $noFirst='0', $firstTitle='', $YearTermID='', $OnFocus='', $FilterSubjectWithoutSG=1, $IsMultiple=0, $IncludeSubjectIDArr='', $ExtrudeSubjectIDArr=array(), $showAllItem=false)
	{
		global $Lang;
	
		if ($OnFocus != '')
			$onfocus = 'onfocus="'.$OnFocus.'"';
	
			if ($OnChange != '')
				$onchange = 'onchange="'.$OnChange.'"';
					
				if ($IsMultiple == 1)
					$multiple = 'multiple="1" size="10"';
						
					# Use current term if not specified
					if ($YearTermID == '')
					{
						$YearTermArr = getCurrentAcademicYearAndYearTerm();
						$YearTermID = $YearTermArr['YearTermID'];
					}
	
					$libLC = new Learning_Category();
					$LC_Arr = $libLC->Get_All_Learning_Category();
					$numOfLC = count($LC_Arr);
	
					$libSubject = new subject();
					$SubjectArr = $libSubject->Get_Subject_List(1);
	
					$select = '';
					$select .= '<select name="'.$ID_Name.'" id="'.$ID_Name.'" class="formtextbox" '.$onchange.' '.$onfocus.' '.$multiple.'>';
						
					if (!$noFirst)
						$select .= '<option value="">'.Get_Selection_First_Title($firstTitle).'</option>';

						if($showAllItem) {
							$select .= '<option value="" data-showall="1">'.$Lang['General']['All'].'</option>';
						}

						$scm = new subject_class_mapping();
						$SubjectGroupInfoArr = $scm->Get_Subject_Group_Info();
	
						if($FilterSubjectWithoutSG==1)
						{
							$AllSubjectGroupInfoArr = $scm->Get_Subject_Group_List($YearTermID);
							$AllSubjectGroupInfoArr = BuildMultiKeyAssoc($AllSubjectGroupInfoArr['SubjectGroupList'], array("SubjectID","SubjectGroupID"));
						}
	
						$subjectCounter = 1;
	
						for ($i=0; $i<$numOfLC; $i++)
						{
							$thisLC_ID = $LC_Arr[$i]['LearningCategoryID'];
							$thisLC_Name = Get_Lang_Selection($LC_Arr[$i]['NameChi'], $LC_Arr[$i]['NameEng']);
							$thisSubjectArr = $SubjectArr[$thisLC_ID];
	
							if (!is_array($thisSubjectArr) || count($thisSubjectArr) == 0) continue;
								
							//			$select .= '<optgroup label="'.$thisLC_Name.'">';
	
							$options = '';
							foreach($thisSubjectArr as $thisSubjectID => $thisSubjectInfoArr)
							{
								if (is_array($IncludeSubjectIDArr) && !in_array($thisSubjectID, $IncludeSubjectIDArr))
									continue;
	
									if (is_array($ExtrudeSubjectIDArr) && in_array($thisSubjectID, $ExtrudeSubjectIDArr))
										continue;
											
										//				$SubjectGroupInfoArr = $scm->Get_Subject_Group_List($YearTermID,$thisSubjectID);
										//				$SubjectGroupArr = $SubjectGroupInfoArr['SubjectGroupList'];
	
										# cannot select subject which do not have subject group
										if ($FilterSubjectWithoutSG==1)
										{
											$SubjectGroupArr = $AllSubjectGroupInfoArr[$thisSubjectID];
											if(count($SubjectGroupArr) == 0)
												continue;
										}
	
										$thisNameChi = intranet_htmlspecialchars($thisSubjectInfoArr['CH_DES']);
										$thisNameEng = intranet_htmlspecialchars($thisSubjectInfoArr['EN_DES']);
										$thisSubjectName = Get_Lang_Selection($thisNameChi, $thisNameEng);
	
										$select_tag = "";
										if ($thisSubjectID == $Selected || ($subjectCounter==1 && $noFirst))
											$select_tag = "selected";
												
											$options .= '<option value="'.$thisSubjectID.'" '.$select_tag.'>'.$thisSubjectName.'</option>';
											$subjectCounter++;
							}
							if(trim($options)!='')
							{
								$select .= '<optgroup label="'.$thisLC_Name.'">';
								$select .= $options;
								$select .= '</optgroup>';
							}
						}
	
						$select .= '</select>';
	
						return $select;
	}
	
	function Get_Subject_Component_Selection($ID_Name, $SubjectID, $SelectedComponentID='')
	{
		global $Lang;
		
		$ObjSubject = new subject($SubjectID);
		$SubjectCmpInfoArr = $ObjSubject->Get_Subject_Component();
		$numOfSubjectCmp = count($SubjectCmpInfoArr);
		
		$SelectionArr = array();
		for ($i=0; $i<$numOfSubjectCmp; $i++)
		{
			$thisSubjectCmpID = $SubjectCmpInfoArr[$i]['RecordID'];
			$thisName = Get_Lang_Selection($SubjectCmpInfoArr[$i]['CH_DES'], $SubjectCmpInfoArr[$i]['EN_DES']);
			
			$SelectionArr[$thisSubjectCmpID] = $thisName;
		}
		
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'"';
		$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['SubjectClassMapping']['Select']['SubjectComponent']);
		
		return getSelectByAssoArray($SelectionArr, $selectionTags, $SelectedComponentID, $all=0, $noFirst=0, $firstTitle);
	}
	
	function Get_Subject_Group_Selection($SubjectID, $ID_Name, $Selected='', $OnChange='', $YearTermID='', $IsMultiple=0, $AcademicYearID='', $noFirst=1, $DisplayTermInfo=0, $TeachingOnly=0, $firstTitle='')
	{
		global $Lang, $PATH_WRT_ROOT;
		
		if ($AcademicYearID != '' && $YearTermID == '')
		{
			include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
			$ObjAcademicYear = new academic_year($AcademicYearID);
			$YearTermInfoArr = $ObjAcademicYear->Get_Term_List($returnAsso=0);
			$YearTermID = Get_Array_By_Key($YearTermInfoArr, 'YearTermID');
		}
		else if ($AcademicYearID == '' && $YearTermID == '')
		{
			$CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
			$YearTermID = $CurrentYearTermArr['YearTermID'];
		}
		
		$scm = new subject_class_mapping();
		$SubjectGroupInfoArr = $scm->Get_Subject_Group_List($YearTermID, $SubjectID, $returnAssociate=1, $TeachingOnly);
		$SubjectGroupArr = $SubjectGroupInfoArr['SubjectGroupList'];
		$numOfSubjectGroup = count($SubjectGroupArr);
		
		$selectArr = array();
		if ($numOfSubjectGroup > 0)
		{
			foreach ($SubjectGroupArr as $thisSubjectGroupID => $thisSubjectGroupArr)
			{
				$thisSubjectGroupID = $thisSubjectGroupArr['SubjectGroupID'];
				$thisSubjectGroupNameEN = intranet_htmlspecialchars($thisSubjectGroupArr['ClassTitleEN']);
				$thisSubjectGroupNameB5 = intranet_htmlspecialchars($thisSubjectGroupArr['ClassTitleB5']);
				$thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupNameB5, $thisSubjectGroupNameEN);
				
				if ($DisplayTermInfo==1)
				{
					$thisTermName = Get_Lang_Selection($thisSubjectGroupArr['YearTermNameB5'], $thisSubjectGroupArr['YearTermNameEN']);
					$thisSubjectGroupName .= " ($thisTermName)";
				} 
				$selectArr[$thisSubjectGroupID] = $thisSubjectGroupName;
			}
		}
		
		$multiple = '';
		if ($IsMultiple)
			$multiple = ' multiple="true" size="10"';
			
		$onchange = '';
		if ($OnChange != "")
			$onchange = ' onchange="'.$OnChange.'" ';
			
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$multiple.' '.$onchange;
		$firstTitle = $firstTitle? $firstTitle : Get_Selection_First_Title($Lang['SysMgr']['SubjectClassMapping']['Select']['SubjectGroup']);
		
		$subjectGroupSelection = getSelectByAssoArray($selectArr, $selectionTags, $Selected, $all=0, $noFirst, $firstTitle);
		
		return $subjectGroupSelection;
	}
	
	function Get_Subject_Group_Student_Selection($ID_Name, $SubjectGroupID, $SelectedStudentID='', $OnChange='', $IsMultiple=0, $IsAll=1, $NoFirst=0)
	{
		global $Lang;
		
		$ObjSG = new subject_term_class($SubjectGroupID, $GetTeacherList=false, $GetStudentList=true);
		$StudentInfoArr = $ObjSG->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=0);
		$numOfStudent = count($StudentInfoArr);
		
		$selectArr = array();
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$thisStudentID = $StudentInfoArr[$i]['UserID'];
			$thisStudentName = $StudentInfoArr[$i]['StudentName'];
			$thisClassName = Get_Lang_Selection($StudentInfoArr[$i]['ClassTitleB5'], $StudentInfoArr[$i]['ClassTitleEN']);
			$thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
			
			$selectArr[$thisStudentID] = $thisStudentName.' ('.$thisClassName.' - '.$thisClassNumber.')';
		}
		
		$multiple = '';
		if ($IsMultiple)
			$multiple = ' multiple="true" size="10"';
			
		$onchange = '';
		if ($OnChange != "")
			$onchange = ' onchange="'.$OnChange.'" ';
			
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$multiple.' '.$onchange;
		$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['RoleManagement']['AllStudent']);
		
		$subjectGroupSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedStudentID, $IsAll, $NoFirst, $firstTitle);
		
		return $subjectGroupSelection;
	}
	
	function Get_Term_Selection($ID_Name, $AcademicYearID='', $SelectedYearTermID='', $OnChange='', $NoFirst=0, $NoPastTerm=0, $displayAll=0, $AllTitle='', $PastTermOnly=0, $CompareYearTermID='', $IsMultiple=0, $IncludeYearTermIDArr='', $ExcludeYearTermIDArr='')
	{
		global $Lang;
		$fcm = new form_class_manage();
		$CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
		
		# Set to currect Academic Year if there is no target Academic Year
		if ($AcademicYearID == '')
			$AcademicYearID = $CurrentYearTermArr['AcademicYearID'];
		
		# Set to currect Semester if there is no target semester
		if ($SelectedYearTermID == '')
			$SelectedYearTermID = $CurrentYearTermArr['YearTermID'];
		
		if ($CompareYearTermID=='')
			$CompareYearTermID = $SelectedYearTermID;
			
		$YearTermArr = $fcm->Get_Academic_Year_Term_List($AcademicYearID, $NoPastTerm, $PastTermOnly, $CompareYearTermID, $IncludeYearTermIDArr, $ExcludeYearTermIDArr);
		$numOfYearTerm = count($YearTermArr);
		
		$selectArr = array();
		for ($i=0; $i<$numOfYearTerm; $i++)
		{
			$thisYearTermID = $YearTermArr[$i]['YearTermID'];
			$thisYearTermName = Get_Lang_Selection($YearTermArr[$i]['YearTermNameB5'],$YearTermArr[$i]['YearTermNameEN']);
			
			$selectArr[$thisYearTermID] = $thisYearTermName;
		}
		
		$onchange = '';
		if ($OnChange != "")
			$onchange = 'onchange="'.$OnChange.'"';
			
		$multiple = '';
		if ($IsMultiple)
			$multiple = ' multiple="true" size="4"';
				
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange.' '.$multiple;
		
		if ($displayAll==1 && $AllTitle=='')
			$AllTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['AllTerm']);
		$firstTitle = $AllTitle ? $AllTitle : Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Term']);
		
		$semesterSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedYearTermID, $displayAll, $NoFirst, $firstTitle);
		
		return $semesterSelection;
	}
	
	function Get_Copy_Subject_Group_Table($YearTermID, $AllowCopyStudent=1, $Global_Arr='',
											$Copy_Subject_Arr='', $Teacher_Subject_Arr='', $Student_Subject_Arr='',
											$Copy_SubjectGroup_Arr='', $Teacher_SubjectGroup_Arr='', $Student_SubjectGroup_Arr='',$Leader_Subject_Arr='',$Leader_SubjectGroup_Arr='')
	{
		global $Lang;
		
		$SubjectGroupPrefix = '&nbsp;&nbsp;&nbsp;&nbsp;';
		$linterface = new interface_html();
		
		### Get Subject List
		$libSubject = new subject();
		$SubjectArr = $libSubject->Get_Subject_List();
		$numOfSubject = count($SubjectArr);
		$sbj = new subject();
		
		### Disable and Uncheck Student if specified
		$CheckedStudentChk = ($AllowCopyStudent==1)? 1 : 0;
		$CheckedLeaderChk = ($AllowCopyStudent==1)? 1 : 0;
		$DisabledStudentChk = 0;//($AllowCopyStudent==1)? 0 : 1;
		//echo $AllowCopyStudent.' '.$DisabledStudentChk;
		$x = '';
		$x .= '<table class="common_table_list" style="width:100%">'."\n";
			$x .= '<col align="left" style="width:60%;" />'."\n";
			$x .= '<col style="width:10%;" />'."\n";
			$x .= '<col style="width:10%;" />'."\n";
			$x .= '<col style="width:10%;" />'."\n";
			$x .= '<col style="width:10%;" />'."\n";
			
			## Header
			$x .= '<tr>'."\n";
				$x .= '<th>'.$Lang['Header']['Menu']['Subject'].'</th>'."\n";
				
				# Copy
				$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopySGChk');";
				$thisChecked = ($Global_Arr == '' || ($Global_Arr != '' && $Global_Arr['Copy'] == 1))? 1 : 0;
				$x .= '<th style="text-align:center">'."\n";
					$x .= $Lang['SysMgr']['SubjectClassMapping']['Copy'];
					$x .= '<br />'."\n";
					$x .= $linterface->Get_Checkbox('Copy_Global', "Global_Arr[Copy]", $Value=1, $thisChecked, $Class='CopySGChk_Global', $Display='', $thisOnclick);
				$x .= '</th>'."\n";
				
				# Teacher
				$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyTeacherChk');";
				$thisChecked = ($Global_Arr == '' || ($Global_Arr != '' && $Global_Arr['Teacher'] == 1))? 1 : 0;
				$x .= '<th style="text-align:center">'."\n";
					$x .= $Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'];
					$x .= '<br />'."\n";
					$x .= $linterface->Get_Checkbox('Teacher_Global', "Global_Arr[Teacher]", $Value=1, $thisChecked, $Class='CopyTeacherChk_Global', $Display='', $thisOnclick);
				$x .= '</th>'."\n";
				
				# Student
				$thisCopyStudent_ID = ($DisabledStudentChk==1)? '' : 'Student_Global';
				$thisCopyStudent_Name = ($DisabledStudentChk==1)? '' : 'Global_Arr[Student]';
				$thisCopyStudent_Class = ($DisabledStudentChk==1)? '' : 'CopyStudentChk_Global';
				$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyStudentChk');";
				if ($Global_Arr == '')
					$thisChecked = $CheckedStudentChk;
				else
					$thisChecked = $Global_Arr['Student'];
				$x .= '<th style="text-align:center">'."\n";
					$x .= $Lang['SysMgr']['SubjectClassMapping']['ClassStudent'];
					$x .= '<br />'."\n";
					$x .= $linterface->Get_Checkbox($thisCopyStudent_ID, $thisCopyStudent_Name, $Value=1, $thisChecked, $thisCopyStudent_Class, $Display='', $thisOnclick, $DisabledStudentChk);
				$x .= '</th>'."\n";
				
				# subject leader
				if ($Global_Arr == '')
					$LeaderChecked = $CheckedStudentChk;
				else
					$LeaderChecked = $Global_Arr['Leader'];
				$thisOnclick= "js_Change_Checkbox_Status(this.checked, 'CopyLeaderChk');";
				$x .= '<th style="text-align:center">'."\n";
					$x .= rtrim($Lang['Group']['SubjectLeader'],'.');
					$x .= '<br />'."\n";
					$x .= $linterface->Get_Checkbox('Leader_Global', 'Global_Arr[Leader]', $Value=1, $LeaderChecked, 'CopyLeaderChk_Global', '', $thisOnclick, !$thisChecked||$DisabledStudentChk);
				$x .= '</th>'."\n";
				
				
				
			$x .= '</tr>'."\n";
			
			if ($numOfSubject > 0)
			{
				for ($i=0; $i<$numOfSubject; $i++)
				{
					$thisSubjectID = $SubjectArr[$i]['RecordID'];
					$thisNameEn = $SubjectArr[$i]['EN_DES'];
					$thisNameCh = $SubjectArr[$i]['CH_DES'];
					$thisName = Get_Lang_Selection($thisNameCh, $thisNameEn);
					$formAry = $sbj->Get_Form_By_Subject($thisSubjectID);
					
					if(sizeof($formAry)>0) {
						### Get Subject Group Info
						$thisObjSubject = new subject($thisSubjectID);
						$thisSubjectGroupArr = $thisObjSubject->Get_Subject_Group_List($YearTermID, $ClassLevelID='', $SubjectGroupID='', $TeacherID='');
						$numOfSubjectGroup = count($thisSubjectGroupArr);
						
						# Skip Subject which do not have any subject groups
						if ($numOfSubjectGroup == 0)
							continue;
						
						# Subject Row
						$x .= '<tr>'."\n";
							$x .= '<td>'."\n";
								$x .= '<span style="float:left">'.$thisName.'&nbsp;&nbsp;</span>';
								$x .= '<span class="row_link_tool" style="float:left">'."\n";
									$x .= '<a href="#" id="zoom_'.$thisSubjectID.'" class="zoom_in" onClick="js_Show_Subject_Group_Row(\''.$thisSubjectID.'\'); return false;" >'."\n";
										$x .= $numOfSubjectGroup."\n";
									$x .= '</a>'."\n";
								$x .= '</span>';
							$x .= '</td>'."\n";
							
							# Copy (within subject) Checkbox
							$thisCopySG_ID = 'Copy_Subject_'.$thisSubjectID;
							$thisCopySG_Name = 'Copy_Subject_Arr['.$thisSubjectID.']';
							$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopySGChk', ".$thisSubjectID.");";
							$thisChecked = ($Copy_Subject_Arr == '' || ($Copy_Subject_Arr != '' && $Copy_Subject_Arr[$thisSubjectID] == 1))? 1 : 0;
							$x .= '<td style="text-align:center">'."\n";
								$x .= $linterface->Get_Checkbox($thisCopySG_ID, $thisCopySG_Name, 1, $thisChecked, $Class='CopySGChk', $Display='', $thisOnclick);
							$x .= '</td>'."\n";
							
							# Teacher (within subject) Checkbox
							$thisCopyTeacher_ID = 'Teacher_Subject_'.$thisSubjectID;
							$thisCopyTeacher_Name = 'Teacher_Subject_Arr['.$thisSubjectID.']';
							$thisOnclick = "js_Change_Checkbox_Status(this.checked, 'CopyTeacherChk', ".$thisSubjectID.");";
							$thisChecked = ($Teacher_Subject_Arr == '' || ($Teacher_Subject_Arr != '' && $Teacher_Subject_Arr[$thisSubjectID] == 1))? 1 : 0;
							$x .= '<td style="text-align:center">'."\n";
								$x .= $linterface->Get_Checkbox($thisCopyTeacher_ID, $thisCopyTeacher_Name, 1, $thisChecked, $Class='CopyTeacherChk', $Display='', $thisOnclick);
							$x .= '</td>'."\n";
							
							# Student (within subject) Checkbox
							$thisCopyStudent_ID = ($DisabledStudentChk==1)? '' : 'Student_Subject_'.$thisSubjectID;
							$thisCopyStudent_Name = ($DisabledStudentChk==1)? '' : 'Student_Subject_Arr['.$thisSubjectID.']';
							$thisCopyStudent_Class = ($DisabledStudentChk==1)? '' : 'CopyStudentChk';
							$thisOnclick = ($DisabledStudentChk==1)? '' : "js_Change_Checkbox_Status(this.checked, '".$thisCopyStudent_Class."', '".$thisSubjectID."');";
							if ($Student_Subject_Arr == '')
								$thisChecked = $CheckedStudentChk;
							else
								$thisChecked = $Student_Subject_Arr[$thisSubjectID];
							$x .= '<td style="text-align:center">'."\n";
								$x .= $linterface->Get_Checkbox($thisCopyStudent_ID, $thisCopyStudent_Name, 1, $thisChecked, $thisCopyStudent_Class, $Display='', $thisOnclick, $DisabledStudentChk);
							$x .= '</td>'."\n";
							
							
							# Leader Checkbox
							$thisCopyLeader_ID = ($DisabledStudentChk==1)? '' : 'Leader_Subject_'.$thisSubjectID;
							$thisCopyLeader_Name = ($DisabledStudentChk==1)? '' : 'Leader_Subject_Arr['.$thisSubjectID.']';
							$thisCopyLeader_Class = ($DisabledStudentChk==1)? '' : 'CopyLeaderChk';
							$thisOnclick = ($DisabledStudentChk==1)? '' : "js_Change_Checkbox_Status(this.checked, '".$thisCopyLeader_Class."', '".$thisSubjectID."');";
							$thisDisabled = ($thisChecked)? $DisabledStudentChk : 'disabled';
							if ($Leader_Subject_Arr == '')
								$thisChecked = $CheckedLeaderChk;
							else
								$thisChecked = $Leader_Subject_Arr[$thisSubjectID];
							$x .= '<td style="text-align:center">'."\n";
								$x .= $linterface->Get_Checkbox($thisCopyLeader_ID, $thisCopyLeader_Name, 1, $thisChecked, $thisCopyLeader_Class, $Display='', $thisOnclick, $thisDisabled);
							$x .= '</td>'."\n";
							
							
						$x .= '</tr>'."\n";
						
						# Subject Group Row
						for ($j=0; $j<$numOfSubjectGroup; $j++)
						{
							$thisSubjectGroupID = $thisSubjectGroupArr[$j]['SubjectGroupID'];
							$thisSubjectGroupNameEn = $thisSubjectGroupArr[$j]['ClassTitleEN'];
							$thisSubjectGroupNameCh = $thisSubjectGroupArr[$j]['ClassTitleB5'];
							$thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupNameCh, $thisSubjectGroupNameEn);
							
							$thisCourseID = $thisSubjectGroupArr[$j]['course_id'];
							$thisCopyChkClass = ($thisCourseID!='' && $thisCourseID!=0)? '' : 'CopySGChk_WithoutEClass';
							
							$x .= '<tr class="sub_row SubjectSubRow_'.$thisSubjectID.'" style="display:none">'."\n";
								$x .= '<td>'.$SubjectGroupPrefix.$thisSubjectGroupName.'</td>'."\n";
								
								# Copy Checkbox
								$thisCopySG_ID = 'Copy_SubjectGroup_'.$thisSubjectGroupID;
								$thisCopySG_Name = 'Copy_SubjectGroup_Arr['.$thisSubjectGroupID.']';
								$thisOnclick = "js_Check_Uncheck_Subject_Group(this.checked, 'Copy', '$thisSubjectID', '$thisSubjectGroupID');";
								$thisCopyChecked = ($Copy_SubjectGroup_Arr == '' || ($Copy_SubjectGroup_Arr != '' && $Copy_SubjectGroup_Arr[$thisSubjectGroupID] == 1))? 1 : 0;
								$x .= '<td style="text-align:center">'."\n";
									$x .= $linterface->Get_Checkbox($thisCopySG_ID, $thisCopySG_Name, 1, $thisCopyChecked, $Class='CopySGChk '.$thisCopyChkClass.' CopySGChk_'.$thisSubjectID, $Display='', $thisOnclick);
								$x .= '</td>'."\n";
								
								# Teacher Checkbox
								$thisCopyTeacher_ID = 'Teacher_SubjectGroup_'.$thisSubjectGroupID;
								$thisCopyTeacher_Name = 'Teacher_SubjectGroup_Arr['.$thisSubjectGroupID.']';
								$thisOnclick = "js_Check_Uncheck_Subject_Group(this.checked, 'Teacher', '$thisSubjectID', '$thisSubjectGroupID');";
								$thisChecked = ($Teacher_SubjectGroup_Arr == '' || ($Teacher_SubjectGroup_Arr != '' && $Teacher_SubjectGroup_Arr[$thisSubjectGroupID] == 1))? 1 : 0;
								$thisDisabled = ($thisCopyChecked==1)? '' : 'disabled';
								$x .= '<td style="text-align:center">'."\n";
									$x .= $linterface->Get_Checkbox($thisCopyTeacher_ID, $thisCopyTeacher_Name, 1, $thisChecked, $Class='CopyTeacherChk CopyTeacherChk_'.$thisSubjectID, $Display='', $thisOnclick, $thisDisabled);
								$x .= '</td>'."\n";
								
								# Student Checkbox
								$thisCopyStudent_ID = ($DisabledStudentChk==1)? '' : 'Student_SubjectGroup_'.$thisSubjectGroupID;
								$thisCopyStudent_Name = ($DisabledStudentChk==1)? '' : 'Student_SubjectGroup_Arr['.$thisSubjectGroupID.']';
								$thisCopyStudent_Class = ($DisabledStudentChk==1)? '' : 'CopyStudentChk CopyStudentChk_'.$thisSubjectID;
								$thisOnclick = ($DisabledStudentChk==1)? '' : "js_Check_Uncheck_Subject_Group(this.checked, 'Student', '$thisSubjectID', '$thisSubjectGroupID');";
								if ($Student_SubjectGroup_Arr == '')
									$thisChecked = $CheckedStudentChk;
								else
									$thisChecked = $Student_SubjectGroup_Arr[$thisSubjectGroupID];
								$thisDisabled = ($thisCopyChecked==1)? $DisabledStudentChk : 'disabled';
								$x .= '<td style="text-align:center">'."\n";
									$x .= $linterface->Get_Checkbox($thisCopyStudent_ID, $thisCopyStudent_Name, 1, $thisChecked, $thisCopyStudent_Class, $Display='', $thisOnclick, $thisDisabled);
								$x .= '</td>'."\n";
								
								
								# Leader Checkbox
								$thisCopyLeader_ID = ($DisabledStudentChk==1)? '' : 'Leader_SubjectGroup_'.$thisSubjectGroupID;
								$thisCopyLeader_Name = ($DisabledStudentChk==1)? '' : 'Leader_SubjectGroup_Arr['.$thisSubjectGroupID.']';
								$thisCopyLeader_Class = ($DisabledStudentChk==1)? '' : 'CopyLeaderChk CopyLeaderChk_'.$thisSubjectID;
								$thisOnclick = ($DisabledStudentChk==1)? '' : "js_Check_Uncheck_Subject_Group(this.checked, 'Leader', '$thisSubjectID', '$thisSubjectGroupID');";
								if ($Leader_SubjectGroup_Arr == '')
									$LeaderChecked = $CheckedLeaderChk;
								else
									$LeaderChecked = $Leader_SubjectGroup_Arr[$thisSubjectGroupID];
								$thisDisabled = ($thisChecked)? $DisabledStudentChk : 'disabled';
								$x .= '<td style="text-align:center">'."\n";
									$x .= $linterface->Get_Checkbox($thisCopyLeader_ID, $thisCopyLeader_Name, 1, $LeaderChecked, $thisCopyLeader_Class, $Display='', $thisOnclick, $thisDisabled);
								$x .= '</td>'."\n";
								
							$x .= '</tr>'."\n";
						}
					}
				}
			}
			else
			{
				$x .= '<tr><td colspan="4" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			}
			
		$x .= '</table>'."\n";
		
		return $x;
	}
	
	function Get_eClass_Licence_Quota_Message($TotalQuota, $QuotaLeft)
	{
		global $Lang;
		
		$QuotaUsed = $TotalQuota - $QuotaLeft;
		$Msg = $Lang['SysMgr']['SubjectClassMapping']['eClassLicenseLeftMessage'];
		$Msg = str_replace('<!--TotalQuota-->', $TotalQuota, $Msg);
		$Msg = str_replace('<!--QuotaLeft-->', $QuotaLeft, $Msg);
		$Msg = str_replace('<!--QuotaUsed-->', $QuotaUsed, $Msg);
		
		return $Msg;
	}
	
	function Get_Delete_Subject_Group_Confirm_UI($AcademicYearID, $YearTermID, $SubjectID)
	{
		global $PATH_WRT_ROOT, $Lang, $linterface;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		
		### Navaigtion
	 	$PAGE_NAVIGATION[] = array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupList'], "javascript:js_Go_Back();");
	 	$PAGE_NAVIGATION[] = array($Lang['SysMgr']['SubjectClassMapping']['DeleteClass'], '', 1);
	 	
	 	### Warning Msg Box
	 	$WarningBox = $linterface->GET_WARNING_TABLE('', $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']);
	 	
	 	### Warning Msg
	 	$WarningMsg = $linterface->Get_Warning_Message_Box('', $Lang['SysMgr']['SubjectClassMapping']['Warning']['Delete']['MultipleSubjectGroup']);
		
		### Get Academic Year Info
		$AcademicYearObj = new academic_year($AcademicYearID);
		$AcademicYearName = $AcademicYearObj->Get_Academic_Year_Name();
		
		### Get Term Info
		$YearTermObj = new academic_year_term($YearTermID);
		$YearTermName = $YearTermObj->Get_Year_Term_Name();
		
		### Get Subject Info
		$SubjectObj = new subject($SubjectID);
		$SubjectName = $SubjectObj->Get_Subject_Desc();
		
		### Buttons
		$DeleteBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['DeleteAll'], "button", "js_Delete_Subject_Group();", "", "", "", "formbutton_alert");
		$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();", "", "", "", "formbutton_v30");
		
		$x = '';
		$x .= '<form id="form1" name="form1" method="post">'."\n";
			$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
				$x .= '<tr><td>'.$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION).'</td></tr>'."\n";
				$x .= '<tr><td>'.$WarningBox.'</td></tr>'."\n";
				$x .= '<tr><td>'.$WarningMsg.'</td></tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						### Selected Info
						$x .= '<table width="95%" class="tabletext" cellpadding="2">'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="formfieldtitle" width="30%">'.$Lang['General']['SchoolYear'].'</td>'."\n";
								$x .= '<td>'.$AcademicYearName.'</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="formfieldtitle" width="30%">'.$Lang['SysMgr']['AcademicYear']['FieldTitle']['Term'].'</td>'."\n";
								$x .= '<td>'.$YearTermName.'</td>'."\n";
							$x .= '</tr>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td class="formfieldtitle" width="30%">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\n";
								$x .= '<td>'.$SubjectName.'</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td>'."\n";
						$x .= $this->Get_Delete_Subject_Group_Confirm_Table($YearTermID, $SubjectID);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<div class="edit_bottom_v30">';
				$x .= $DeleteBtn."\n";
				$x .= $CancelBtn."\n";
			$x .= '</div>';
		
		$x .= '</form>'."\n";
		$x .= '<br style="clear:both;" />'."\n";
		
		return $x;
	}
	
	function Get_Delete_Subject_Group_Confirm_Table($YearTermID, $SubjectID)
	{
		global $PATH_WRT_ROOT, $Lang, $linterface;
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		
		### Get Subject's Subject Group Info
		$libSCM = new subject_class_mapping();
		$SubjectGroupInfoArr = $libSCM->Get_Subject_Group_List($YearTermID, $SubjectID);
		$SubjectGroupInfoArr = $SubjectGroupInfoArr['SubjectGroupList'];
		$numOfSubjectGroup = count($SubjectGroupInfoArr);
		
		$x = '';
		$x .= '<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
			$x .= '<tr><td class="tabletext">'.$linterface->GET_NAVIGATION2($Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']).'</td></tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div id="SubjectGroupTableDiv">'."\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30" style="width:100%">'."\n";
							$x .= '<col align="left" style="width:20%;" />'."\n";
							$x .= '<col align="left" style="width:50%;" />'."\n";
							$x .= '<col align="left" style="width:20%;" />'."\n";
							$x .= '<col align="left" style="width:10%;" />'."\n";
							
							## Header
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['ClassCode'].'</th>'."\n";
								$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['ClassName'].'</th>'."\n";
								$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'].'</th>'."\n";
								$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['ClassStudentNumber'].'</th>'."\n";
								
							$x .= '</tr>'."\n";
							
							if ($numOfSubjectGroup > 0)
							{
								foreach((array)$SubjectGroupInfoArr as $thisSubjectGroupID => $thisSubjectGroupInfoArr)
								{
									$thisCode = $thisSubjectGroupInfoArr['ClassCode'];
									$thisName = Get_Lang_Selection($thisSubjectGroupInfoArr['ClassTitleB5'], $thisSubjectGroupInfoArr['ClassTitleEN']);
									$thisStaff = ($thisSubjectGroupInfoArr['StaffName']=='')? $Lang['General']['EmptySymbol'] : $thisSubjectGroupInfoArr['StaffName'];
									
									$thisSGObj = new subject_term_class($thisSubjectGroupID, false, true);
									$thisNumOfStudent = count($thisSGObj->ClassStudentList);
									
									$x .= '<tr>'."\n";
										$x .= '<td>'.$thisCode.'</td>'."\n";
										$x .= '<td>'.$thisName.'</td>'."\n";
										$x .= '<td>'.$thisStaff.'</td>'."\n";
										$x .= '<td>'.$thisNumOfStudent.'</td>'."\n";
									$x .= '</tr>'."\n";
								}
							}
							else
							{
								$x .= '<tr><td colspan="4" style="text-align:center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		
		$x .= '<input type="hidden" id="SubjectGroupIDList" name="SubjectGroupIDList" value="" />'."\n";
		
		return $x;
	}
	
	function Get_Form_Subject_Component_Setting_Table()
	{
		global $sys_custom, $Lang, $button_edit, $button_update, $button_cancel, $image_path, $LAYOUT_SKIN;
		
		$linterface = new interface_html();
		$fcm = new form_class_manage();
		$scm = new subject_class_mapping();
		$sbj = new subject();
		
		
		$x = '
		<script language="javascript">
		<!--
		function EditInfo()
		{
			$(\'#EditBtn\').attr(\'style\', \'visibility: hidden\'); 
			$(\'.Edit_Hide\').attr(\'style\', \'display: none\');
			$(\'.Edit_Show\').attr(\'style\', \'\');
			
			$(\'#UpdateBtn\').attr(\'style\', \'\');
			$(\'#cancelbtn\').attr(\'style\', \'\');
		}		
		
		function ResetInfo()
		{
			document.FormSubject.reset();
			$(\'#EditBtn\').attr(\'style\', \'\');
			$(\'.Edit_Hide\').attr(\'style\', \'\');
			$(\'.Edit_Show\').attr(\'style\', \'display: none\');
			
			$(\'#UpdateBtn\').attr(\'style\', \'display: none\');
			$(\'#cancelbtn\').attr(\'style\', \'display: none\');
		}
		//-->
		</script>		
		';
		$x .= '
			<div class="table_row_tool row_content_tool">'.
			$linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn").'
			</div>
			<p class="spacer"></p>		' ;
		$x .= '<form name="FormSubject" id="FormSubject">';
		$x .= '<div id="spanFormSubject">';
		$x .= '<table class="common_table_list" id="SubjectContentTable">'."\n";
		$x .= $this->Get_Content_Table_Property();
		/*
		# Table Header
		$x .= '<thead>'."\n";
		$x .= '<tr>'."\n";
		$x .= '<th width="30%">'.$Lang['SysMgr']['FormClassMapping']['Form'].'</th>'."\n";
		$x .= '<th width="65%">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</th>'."\n";
		$x .= '<th width="5%">&nbsp;</th>'."\n";			
		$x .= '</tr>'."\n";
		$x .= '</thead>'."\n";
		$x .= '<tbody>'."\n";
		# table content
		$FormList = $fcm->Get_Form_List();
		
		for ($i=0; $i< sizeof($FormList); $i++) {
			list($YearID, $YearName) = $FormList[$i];
			$subjectLinked = $sbj->Get_Subject_By_Form($YearID);
			
			$RowSpan = (sizeof($subjectLinked) > 0)? 'rowspan="'.(sizeof($subjectLinked)+1).'"' : '';
			$FirstDisplay = (sizeof($subjectLinked) > 0)? 'display:none;' : '';
			
			
			$x .= '<tr class="nodrag nodrop">';
			$x .= '	<td '.$RowSpan.'>'.$FormList[$i]['YearName'].'</td>
					<td style="'.$FirstDisplay.'">&nbsp;</td>';
					
			if(sizeof($subjectLinked)==0)
				$x .= "<td class='table_row_tool'><a href='#TB_inline?height=450&width=750&inlineId=FakeLayer' class='thickbox edit_dim' title='".$button_edit."' onclick=\"Edit_Form_Subject(".$YearID."); return false;\"></a></td>";
			$x .= '	</tr>';
			
			$subjectTable = "&nbsp;";
			for($j=0; $j<sizeof($subjectLinked); $j++) {
				list($subjectID, $subjectCH, $subjectEN) = $subjectLinked[$j];
				$x .= "<tr class='sub_row'><td>".Get_Lang_Selection($subjectCH, $subjectEN)."</td>";
				if($j==0) {
					$rowSpan2 = " rowspan='".sizeof($subjectLinked)."'";
					$x .= "<td class='table_row_tool' $rowSpan2>
								<a href='#TB_inline?height=450&width=750&inlineId=FakeLayer' class='thickbox edit_dim' title='".$button_edit."' onclick=\"Edit_Form_Subject(".$YearID."); return false;\"></a>	
							</td></tr>";
				}
			}
	
		
		}
		*/
		
		
		# Table Header
		$FormList = $fcm->Get_Form_List();
		
		$linkedAry = array();
		$x .= '<thead>'."\n";
		$x .= '<tr>'."\n";
		$x .= '<th><span class="Edit_Show" style="display:none">'.$linterface->Get_Checkbox('CheckAll_Option', 'CheckAll_Option', '1', $isChecked=0, $Class='', $Display='', $Onclick='js_Select_All(this.checked);').'</span></th>'."\n";
		//$x .= '<th>&nbsp;</th>'."\n";
		for($i=0; $i<sizeof($FormList); $i++) {
			$x .= '<th width="5%"><span class="Edit_Show" style="display:none"><input type="checkbox" name="" id="colAllChk_'.$FormList[$i]['YearID'].'" onClick="colChecked(\''.$FormList[$i]['YearID'].'\', this.checked)"></span><label for="colAllChk_'.$FormList[$i]['YearID'].'">'.$FormList[$i]['YearName'].'</label></th>'."\n";
			//$x .= '<th width="5%">'.$FormList[$i]['YearName'].'</th>'."\n";
			$subjectLinked = $sbj->Get_Subject_By_Form($FormList[$i]['YearID']);
			
			for($j=0; $j<sizeof($subjectLinked); $j++) {
				$linkedAry[$FormList[$i]['YearID']][$subjectLinked[$j]['RecordID']] = 1;	
			}
		}					
		$x .= '</tr>'."\n";
		$x .= '</thead>'."\n";
		$x .= '<tbody>'."\n";
		
		# table content 
		if($sys_custom['Subject']['DisplaySubjectCode']){
			// [2015-1111-1214-16164] Get Subject Code
			$SubjectList = $sbj->Get_Subject_List();
		}
		else{
			$SubjectList = $sbj->Get_All_Subjects();
		}

		$YearTermID = GetCurrentSemesterID();
		
		$tickImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/ereportcard/icon_present_bw.gif' width='10'>";
		$firstColumnWidth = 5;
		if(sizeof($SubjectList)>0)
		{
			$width = (100-$firstColumnWidth)/(sizeof($SubjectList))."%";
		}
		
		for($i=0; $i<sizeof($SubjectList); $i++) {
			$x .= '<tr class="nodrag nodrop" id="tr_'.$SubjectList[$i]['RecordID'].'">';
			$x .= '<td width="'.$firstColumnWidth.'%" id="td_'.$SubjectList[$i]['RecordID'].'_'.$i.'"><span class="Edit_Show" style="display:none"><input type="checkbox" name="rowCheckBox" id="rowCheckBox_'.$SubjectList[$i]['RecordID'].'" value ="1" onClick="rowChecked(\'td_'.$SubjectList[$i]['RecordID'].'_'.$i.'\',this.checked)"></span>';
				$x .= '<label for="rowCheckBox_'.$SubjectList[$i]['RecordID'].'">'.Get_Lang_Selection($SubjectList[$i]['CH_DES'],$SubjectList[$i]['EN_DES']).($sys_custom['Subject']['DisplaySubjectCode']? "<br />(".$SubjectList[$i]['CODEID'].")" : "").'</label></td>';
			//$x .= '<td width="'.$firstColumnWidth.'%">'.Get_Lang_Selection($SubjectList[$i]['CH_DES'],$SubjectList[$i]['EN_DES']).'</td>';
			for($j=0; $j<sizeof($FormList); $j++) {
				$x .= '<td width="'.$width.'">';
				if((isset($linkedAry[$FormList[$j]['YearID']][$SubjectList[$i]['RecordID']]) && $linkedAry[$FormList[$j]['YearID']][$SubjectList[$i]['RecordID']])) {
					$x .= '<span class="Edit_Hide">'.$tickImg.'</span>';
					$x .= '<span class="Edit_Show" style="display:none"><input type="checkbox" name="SubjectGroup_'.$SubjectList[$i]['RecordID'].'_'.$FormList[$j]['YearID'].'" id="SubjectGroup_'.$SubjectList[$i]['RecordID'].'_'.$FormList[$j]['YearID'].'" value ="1" checked></span>';
				} else {
					$x .= '<span class="Edit_Hide">&nbsp;</span>';
					$x .= '<span class="Edit_Show" style="display:none"><input type="checkbox" name="SubjectGroup_'.$SubjectList[$i]['RecordID'].'_'.$FormList[$j]['YearID'].'" id="SubjectGroup_'.$SubjectList[$i]['RecordID'].'_'.$FormList[$j]['YearID'].'" value="1"></span>';
				}
				$x .= '</td>'."\n";
			}		
			$x .= '</tr>';
		}
		
		
		$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= "</div>"."\n";

		$x .= '
				<div class="edit_bottom_v30">
				<p class="spacer"></p>';
		$x .= $linterface->GET_ACTION_BTN($button_update, "button","self.location.href='#';Update_Form_Subject()","UpdateBtn", "style='display: none'").'&nbsp;';
		$x .=  $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'");
		$x .= '	<p class="spacer"></p>
				</div>
				</form>		
				';
				
		return $x;	
	}
	
	// $YearTermIDArr can be an array or a value
	function Get_User_Accessible_Subject_Group_Selection($ID, $Name, $ParUserID, $AcademicYearID, $YearTermIDArr='', $OnChange='', $IsMultiple=0)
	{
		$libSCM = new subject_class_mapping();
		$SubjectGroupInfoArr = $libSCM->Get_User_Accessible_Subject_Group($ParUserID, $AcademicYearID, $YearTermIDArr);
		$SubjectGroupAssoArr = BuildMultiKeyAssoc($SubjectGroupInfoArr, array('SubjectID', 'SubjectGroupID'));
		
		if ($OnChange != '')
			$onchange = ' onchange="'.$OnChange.'" ';
		if ($IsMultiple != '')
			$multiple = ' multiple="1" size="10" ';
		
		$x = '';
		$x .= '<select name="'.$Name.'" id="'.$ID.'" class="formtextbox" '.$onchange.' '.$multiple.'>'."\n";
			foreach ((array)$SubjectGroupAssoArr as $thisSubjectID => $thisSubjectInfoArr)
			{
				$thisSubjectGroupCounter = 0;
				foreach ((array)$thisSubjectInfoArr as $thisSubjectGroupID => $thisSubjectGroupInfoArr)
				{
					if ($thisSubjectGroupCounter == 0)
					{
						// Show Subject info
						$thisSubjectName = Get_Lang_Selection($thisSubjectGroupInfoArr['SubjectNameCh'], $thisSubjectGroupInfoArr['SubjectNameEn']);
						$x .= '<optgroup label="'.$thisSubjectName.'">'."\n";
					}
					
					// Show Subject Group Option
					$thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupInfoArr['SubjectGroupNameCh'], $thisSubjectGroupInfoArr['SubjectGroupNameEn']);
					$x .= '<option value="'.$thisSubjectGroupID.'" selected>'.$thisSubjectGroupName.'</option>';
					$thisSubjectGroupCounter++;
				}
				$x .= '</optgroup>'."\n";
			}
		$x .= '</select>'."\n";
		
		return $x;
	}
	
	function Get_Form_Subject_Table($YearID="")
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN, $Lang;
		if($YearID!="") {
			$scm = new subject();
			$linterface = new interface_html();
			
			# all subjects
			$allSubjects = $scm->Get_All_Subjects();

			
			# linked subjects
			$linkedSubjectIDAry = array();
			$result = $scm->Get_Subject_By_Form($YearID);
			for($i=0; $i<sizeof($result); $i++) {
				$linkedSubjectIDAry[] = $result[$i][0];	
			}
						
			$x = "<br>";
			$x .= '<form name="Form_FormSubject" id="Form_FormSubject">';
			$x .= '<table class="common_table_list">';
			
			# Table Header
			$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
			//$x .= '<th>'.$Lang['SysMgr']['FormClassMapping']['Form'].'</th>'."\n";
			$x .= '<th colspan="2">'.$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</th>'."\n";
			$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
			$x .= '<tr>'."\n";
			
			for($i=0; $i<sizeof($allSubjects); $i++) {
				list($subjectID, $CH_Name, $EN_Name) = $allSubjects[$i];
				$x .= '<td width="50%"><input type="checkbox" name="subjectID[]" id="subjectID_'.$subjectID.'" value="'.$subjectID.'"'.((sizeof($linkedSubjectIDAry)>0 && in_array($subjectID, $linkedSubjectIDAry) ? " checked" : "")).'><label for="subjectID_'.$subjectID.'">'.Get_Lang_Selection($CH_Name, $EN_Name).'</label></td>';
				if($i%2==1) $x .= '</tr><tr>';
			}
			$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
			
			$x .= '<div class="edit_bottom_v30">';
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "Form_Subject_Action($YearID);", "", "", "", "formbutton_v30")."&nbsp;";
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.top.tb_remove();", "", "", "", "formbutton_v30");			
			$x .= '</div>';
			$x .= '</form>';
			
			return $x;
		}
	}
	
	function Get_SchoolSettings_Subject_Tab_Array($curTab) {
		global $Lang, $sys_custom;
		
		$tagAry = array();
		$tagAry[] = array($Lang['SysMgr']['SubjectClassMapping']['Subject&ComponentTitle'], "index.php", ($curTab=='subject'));
		$tagAry[] = array($Lang['formClassMapping']['FormSubject'], "form_subject.php", ($curTab=='subjectForm'));
		$tagAry[] = array($Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'], "subject_group_mapping.php", ($curTab=='subjectGroup'));
		
		if ($sys_custom['Subject']['SubjectPanel']) {
			$tagAry[] = array($Lang['General']['SubjectPanel'], "subject_panel_list.php", ($curTab=='subjectPanel'));
		}
		// [2017-1207-0956-53277]
		if($sys_custom['iPf']['DBSTranscript']) {
		    $tagAry[] = array($Lang['SysMgr']['FormClassMapping']['StudentSubjectLevel'], "student_subject_level.php", ($curTab=='studentSubjectLevel'));
		}
		
		return $tagAry;
	}
	
	function Get_Current_Year_Subject_Group_SelectionBox($SubjectGroupArr='', $SubjectIDArr=''){
		return $this->Get_Subject_Group_SelectionBox_By_Academic_Year_Term_ID($YearTermID='', $SubjectGroupArr, $SubjectIDArr);
	}
	
	function Get_Subject_Group_SelectionBox_By_Academic_Year_Term_ID($YearTermID='', $SubjectGroupArr='', $SubjectIDArr='', $SelectionIDName='', $OnChange='', $IsMultiple=0){
		global $Lang, $PATH_WRT_ROOT;
		$scm = new subject_class_mapping();
		
		if($YearTermID == '') {
    		$temp = getCurrentAcademicYearAndYearTerm();
    		$YearTermID = $temp['YearTermID'];
		}
		
		if($SubjectGroupArr != '' || $SubjectIDArr != '') {
			$SubjectGroupInfoArr = array();
			if($SubjectGroupArr != '') { // get subject Group in current year
// 				$YearTermID_sql = implode('","',$YearTermID);
				$cond = implode('","', $SubjectGroupArr);
				$sql = 'SELECT 
							stc.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5, st.SubjectID
						FROM 
							SUBJECT_TERM_CLASS stc
						INNER JOIN
							SUBJECT_TERM st ON (st.SubjectGroupID = stc.SubjectGroupID)
						WHERE 
							stc.SubjectGroupID IN ("'.$cond.'")
						AND 
							st.YearTermID = "'.$YearTermID.'"
						ORDER BY
							st.SubjectID, stc.ClassTitleEN ';
				$temp = $scm->returnResultSet($sql);
				foreach($temp as $_temp) {
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['SubjectGroupID'] = $_temp['SubjectGroupID'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['ClassTitleEN'] = $_temp['ClassTitleEN'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['ClassTitleB5'] = $_temp['ClassTitleB5'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['ClassCode'] = $_temp['ClassCode'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['YearTermID'] = $_temp['YearTermID'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['YearTermNameEN'] = $_temp['YearTermNameEN'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['YearTermNameB5'] = $_temp['YearTermNameB5'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['SubjectID'] = $_temp['SubjectID'];
				}
// 				$SubjectGroupInfoArr = array_merge($SubjectGroupInfoArr,$temp);
			}

            // get subject Group in current year
			if($SubjectIDArr != '') {
// 				$YearTermID_sql = implode('","',$YearTermID);
				$cond = implode('","',$SubjectIDArr);
				$sql = 'SELECT
							stc.SubjectGroupID, stc.ClassTitleEN, stc.ClassTitleB5, st.SubjectID
						FROM 
							SUBJECT_TERM_CLASS stc
						INNER JOIN
							SUBJECT_TERM st ON (st.SubjectGroupID = stc.SubjectGroupID)
						WHERE 
							st.SubjectID IN ("'.$cond.'")
						AND 
							st.YearTermID = "'.$YearTermID.'"
						ORDER BY
							st.SubjectID, stc.ClassTitleEN ';
				$temp = $scm->returnResultSet($sql);
				foreach($temp as $_temp){
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['SubjectGroupID'] = $_temp['SubjectGroupID'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['ClassTitleEN'] = $_temp['ClassTitleEN'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['ClassTitleB5'] = $_temp['ClassTitleB5'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['ClassCode'] = $_temp['ClassCode'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['YearTermID'] = $_temp['YearTermID'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['YearTermNameEN'] = $_temp['YearTermNameEN'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['YearTermNameB5'] = $_temp['YearTermNameB5'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['SubjectID'] = $_temp['SubjectID'];
				}
// 				$SubjectGroupInfoArr = array_merge($SubjectGroupInfoArr,$temp);
			}
// 			$SubjectGroupInfoArr = $SubjectGroupInfoArr['SubjectGroupList'];
		} else {
// 				$SubjectGroupInfoArr = $scm->Get_Subject_Group_List($YearTermID,'','','','st.SubjectID');
// 				$temp = $scm->Get_Subject_Group_List($CurrentTerm,'','','','st.SubjectID, stc.ClassTitleEN');
				$temp = $scm->Get_Subject_Group_List($YearTermID,'','','','');
				foreach($temp as $_temp){
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['SubjectGroupID'] = $_temp['SubjectGroupID'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['ClassTitleEN'] = $_temp['ClassTitleEN'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['ClassTitleB5'] = $_temp['ClassTitleB5'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['ClassCode'] = $_temp['ClassCode'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['YearTermID'] = $_temp['YearTermID'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['YearTermNameEN'] = $_temp['YearTermNameEN'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['YearTermNameB5'] = $_temp['YearTermNameB5'];
					$SubjectGroupInfoArr[$_temp['SubjectGroupID']]['SubjectID'] = $_temp['SubjectID'];
				}
		}
		
		if(empty($SubjectGroupInfoArr)) {
			$x = $Lang['SDAS']['Error']['noSubject'];
			return $x;
		}
		foreach($SubjectGroupInfoArr as $_SubjectGroupInfoArr) {
			$SubjectID_Arr[] = $_SubjectGroupInfoArr['SubjectID'];
		}
// 		$SubjectID_Arr = Get_Array_By_Key($SubjectGroupInfoArr, 'SubjectID');
		$SubjectID_Arr = array_unique($SubjectID_Arr);
		$SubjectID_Arr = implode("','",$SubjectID_Arr);
		$sql = "
			SELECT
				EN_DES, CH_DES, RecordID
			FROM
				ASSESSMENT_SUBJECT
			WHERE
				RecordID in "."('".$SubjectID_Arr."') ";
		$rs =  $scm->returnResultSet($sql);
		$SujectName = Get_Lang_Selection('CH_DES', 'EN_DES');
		$SubjectArr = BuildMultiKeyAssoc($rs,'RecordID',array($SujectName),1);
		
		$SelectionIDName = $SelectionIDName != ''? $SelectionIDName : 'subjectGroup_box';
		$onchange = $OnChange != ''? ' onchange="'.$OnChange.'" ' : '';
		$multiple = $IsMultiple != ''? ' multiple="1" size="10" ' : '';

		$x = '';
		$x .= '<select id="'.$SelectionIDName.'" name="'.$SelectionIDName.'" '.$onchange.' '.$multiple.'>';
		foreach($SubjectGroupInfoArr as $_SubjectGroupInfoArr) {
			$SubjectGroupInfoArr_BySubjectID[$_SubjectGroupInfoArr['SubjectID']][] =  $_SubjectGroupInfoArr;
		}
		foreach ($SubjectGroupInfoArr_BySubjectID as $SubjectID => $_SubjectGroupInfoArr_BySubjectID) { //subjectID
			$x .= "<optgroup label='".$SubjectArr[$SubjectID]."'>";
				foreach($_SubjectGroupInfoArr_BySubjectID as $__SubjectGroupInfoArr_BySubjectID) { //everySubject
					$x .="<option value='".$__SubjectGroupInfoArr_BySubjectID['SubjectGroupID']."'>";
					$x .= Get_Lang_Selection($__SubjectGroupInfoArr_BySubjectID['ClassTitleB5'], $__SubjectGroupInfoArr_BySubjectID['ClassTitleEN']);
					$x .= "</option>";
				}
			$x .= "</optgroup>";
		}
// 		foreach($SubjectGroupInfoArr_BySubjectID as $SubjectID=>$_SubjectGroupInfoArr_BySubjectID){
// 			debug_pr($_SubjectGroupInfoArr_BySubjectID);
// 			if($SubjectGroupArr!=''){
// 				foreach((array)$SubjectGroupArr as $_SubjectGroupArr){
// 					if($_SubjectGroupInfoArr['SubjectGroupID']==$_SubjectGroupArr){
// 						$x .= "<optgroup label='".$SubjectArr[$_SubjectGroupInfoArr['SubjectID']]."'>";
// 							$x .= "<option value='".$_SubjectGroupInfoArr['SubjectGroupID']."'>";
// 							$x .= Get_Lang_Selection($_SubjectGroupInfoArr['ClassTitleB5'], $_SubjectGroupInfoArr['ClassTitleEN']);
// 							$x .= "</option>";
// 						$x .= "</optgroup>";
// 					}
// 				}
// 			}else{
// 				$x .= "<option value='".$_SubjectGroupInfoArr['SubjectGroupID']."'>";
// 				if($_SubjectGroupInfoArr['ClassTitleB5']!=''){
// 					$x .= Get_Lang_Selection($_SubjectGroupInfoArr['ClassTitleB5'], $_SubjectGroupInfoArr['ClassTitleEN']);
// 				}else{
// 					$x .= $_SubjectGroupInfoArr['ClassTitleEN'];
// 				}
// 				$x .= "</option>";
// 			}
// 		}
		$x .= '</select>';
		return $x;
	}
}
?>