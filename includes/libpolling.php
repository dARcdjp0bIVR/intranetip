<?php
// editing: 

####################################
#
#   Date:   2019-04-03 Bill     [2019-0327-0907-21066]
#           modified returnPolling_IP25(), to display poll release date
#
#	Date:	2017-11-15 Anna
#			Added returnReleaseResult()
#			modified displayResultChart() - added ReleasePoll
#
#	Date:	2017-04-03
#			Add returnTargetGroupsArray, isPollingEditable, isPollingFilled, returnPolling_IP25 for Selected Group 
#
#	Date:	2016-06-29 Catherine
#			new getCount(), for Highcahrts graphs to get countValue
#
#	Date:	2012-05-21 YatWoon
#			update returnResultDetails(), fix for duplicate count for same user result but in different group [Case#2012-0515-1211-03132]
#
#	Date:	2010-12-20 YatWoon
#			added returnResultDetails(), return more information for the polling result (copied from EJ)
#
#	Date:	2011-04-13 YatWoon
#			update returnPolling(), add fields K to Z
#			update displayResultChart(), display result K to Z
#
#	Date:	2010-12-29 YatWoon
#			update displayResultChart(), update images path
#	
#	Date:	2010-12-28	YatWoon
#			add $Attachment variable
#
####################################

include_once($PATH_WRT_ROOT."includes/libuser.php");
class libpolling extends libdb{

        var $Result;
        var $Polling;
        var $PollingID;
        var $Question;
        var $AnswerA;
        var $AnswerB;
        var $AnswerC;
        var $AnswerD;
        var $AnswerE;
        var $AnswerF;
        var $AnswerG;
        var $AnswerH;
        var $AnswerI;
        var $AnswerJ;
        var $Reference;
        var $DateStart;
        var $DateEnd;
        var $DateRelease;
        var $RecordType;
        var $RecordStatus;
        var $DateInput;
        var $DateModified;
        var $Attachment;
		var $AnswerK;
		var $AnswerL;
		var $AnswerM;
		var $AnswerN;
		var $AnswerO;
		var $AnswerP;
		var $AnswerQ;
		var $AnswerR;
		var $AnswerS;
		var $AnswerT;
		var $AnswerU;
		var $AnswerV;
		var $AnswerW;
		var $AnswerX;
		var $AnswerY;
		var $AnswerZ;
		
        function libpolling($PollingID=""){
                $this->libdb();
                if($PollingID<>""){
                        $this->Polling = $this->returnPolling($PollingID);
                        $this->PollingID = $this->Polling[0][0];
                        $this->Question = $this->Polling[0][1];
                        $this->AnswerA = $this->Polling[0][2];
                        $this->AnswerB = $this->Polling[0][3];
                        $this->AnswerC = $this->Polling[0][4];
                        $this->AnswerD = $this->Polling[0][5];
                        $this->AnswerE = $this->Polling[0][6];
                        $this->AnswerF = $this->Polling[0][7];
                        $this->AnswerG = $this->Polling[0][8];
                        $this->AnswerH = $this->Polling[0][9];
                        $this->AnswerI = $this->Polling[0][10];
                        $this->AnswerJ = $this->Polling[0][11];
                        $this->Reference = $this->Polling[0][12];
                        $this->DateStart = $this->Polling[0][13];
                        $this->DateEnd = $this->Polling[0][14];
                        $this->RecordType = $this->Polling[0][15];
                        $this->RecordStatus = $this->Polling[0][16];
                        $this->DateInput = $this->Polling[0][17];
                        $this->DateModified = $this->Polling[0][18];
                        $this->DateRelease = $this->Polling[0][19];
                        $this->Attachment = $this->Polling[0][20];
                        
                        $this->AnswerK = $this->Polling[0][21];
                        $this->AnswerL = $this->Polling[0][22];
                        $this->AnswerM = $this->Polling[0][23];
                        $this->AnswerN = $this->Polling[0][24];
                        $this->AnswerO = $this->Polling[0][25];
                        $this->AnswerP = $this->Polling[0][26];
                        $this->AnswerQ = $this->Polling[0][27];
                        $this->AnswerR = $this->Polling[0][28];
                        $this->AnswerS = $this->Polling[0][29];
                        $this->AnswerT = $this->Polling[0][30];
                        $this->AnswerU = $this->Polling[0][31];
                        $this->AnswerV = $this->Polling[0][32];
                        $this->AnswerW = $this->Polling[0][33];
                        $this->AnswerX = $this->Polling[0][34];
                        $this->AnswerY = $this->Polling[0][35];
                        $this->AnswerZ = $this->Polling[0][36];
                }
        }

        function returnPolling($PollingID)
        {
			for($i=75;$i<=90;$i++)	# K to Z
			{
				$fields .= ",Answer". chr($i);	
			}
	
            $sql = "SELECT PollingID, Question, AnswerA, AnswerB, AnswerC, AnswerD, AnswerE, AnswerF, AnswerG, AnswerH, AnswerI, AnswerJ, Reference, DATE_FORMAT(DateStart, '%Y-%m-%d'), DATE_FORMAT(DateEnd, '%Y-%m-%d'), RecordType, RecordStatus, DateInput, DateModified, DATE_FORMAT(DateRelease, '%Y-%m-%d'), Attachment 
            		$fields
            		FROM 
            		INTRANET_POLLING WHERE PollingID = '$PollingID'";
            return $this->returnArray($sql);
        }

        function returnResult($PollingID){

                $sql = "SELECT Answer FROM INTRANET_POLLINGRESULT WHERE PollingID = '$PollingID'";
                return $this->returnArray($sql,1);
        }
        function returnReleaseResult($PollingID){
        	
        	$sql = "SELECT Answer FROM INTRANET_POLLINGRESULT AS ipr
					INNER JOIN  INTRANET_POLLING AS ip ON (ip.PollingID = ipr.PollingID) 
					WHERE ipr.PollingID = '$PollingID' AND ip.DateRelease<=DATE_FORMAT(now(), '%Y-%m-%d')";
        	return $this->returnArray($sql,1);
        }
        
        function countResult($AnswerTitle, $AnswerValue, $Result){
                $total = sizeof($Result);
                $count = 0;
                for($i=0; $i<$total; $i++){ if($AnswerValue == $Result[$i][0]) $count++; }
                $percent = ($total==0) ? 0 : 100*number_format(($count/$total),2);
                $width = number_format($percent);
                $width = ($width==0) ? 1 : $width;
                $AnswerTitle = intranet_wordwrap($AnswerTitle,30,"\n",1);
                $bar = "<img src=/images/frontpage/stat_green_spacer.gif border=0 height=12 width=$width align=absmiddle hspace=2>";
                $x .= "<tr><td><br></td><td colspan=2>$AnswerTitle</td></tr>\n";
                $x .= "<tr><td><br></td><td>$bar</td><td align=right>$percent%($count)</td></tr>\n";
                $x .= "<tr><td colspan=3><br></td></tr>\n";
                return $x;
        }

        function countResult2($AnswerTitle, $AnswerValue, $Result, $barno)
        {
        	global $image_path, $LAYOUT_SKIN;
                
                $total = sizeof($Result);
                $count = 0;
                for($i=0; $i<$total; $i++)
                { 
                	if($AnswerValue == $Result[$i][0]) 
                        	$count++; 
		}
                $percent = ($total==0) ? 0 : 100*number_format(($count/$total),2);
                $width = number_format($percent);
                ### $width = ($width==0) ? "1%" : $width."%";
                $width = ($width==0) ? "2" : 700*$width/100;
                $AnswerTitle = intranet_wordwrap($AnswerTitle,30,"\n",1);
                $bar = "<img src='$image_path/$LAYOUT_SKIN/barchart/bar_h_$barno.gif' border='0' height='19' width='100%'>";
                
                $x .= "<tr> ";
                $x .= "<td>&nbsp;</td>";
		$x .= "	<td width='1' background='{$image_path}/{$LAYOUT_SKIN}/barchart/line.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/barchart/line.gif' width='1' height='1'></td>";
		$x .= "	<td align='left' valign='bottom'> ";
		$x .= "		<table width='100%' border='0' cellspacing='0' cellpadding='2'>";
                $x .= "		<tr>";
		$x .= "			<td height='30' valign='bottom'><span class='barnameh'>$AnswerTitle</span></td>";
                $x .= "         </tr>";
                $x .= "         </table>";

		$x .= "		<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
		$x .= "		<tr> ";
		$x .= "			<td width='$width'>$bar</td>";
		$x .= "			<td align='left' class='barnum' nowrap>$percent%($count)</td>";
		$x .= "		</tr>";
		$x .= "		</table>";
		$x .= "	</td>";
		$x .= "</tr>";

                return $x;
        }
        
        function getCount($AnswerValue, $Result){
        	$total = sizeof($Result);
        	$count = 0;
        	for($i=0; $i<$total; $i++){ 
        		if($AnswerValue == $Result[$i][0]) { 
        			$count++; 
        		}
        	}
        	return $count;
        }
        
        ###############################################################################

        function display(){
                global $i_frontpage_image_vote, $i_frontpage_button_result, $i_PollingDateStart, $i_PollingDateEnd;
                global $i_PollingQuestion, $i_PollingAnswerA, $i_PollingAnswerB, $i_PollingAnswerC, $i_PollingAnswerD, $i_PollingAnswerE;
                global $i_PollingAnswerF, $i_PollingAnswerG, $i_PollingAnswerH, $i_PollingAnswerI, $i_PollingAnswerJ, $i_PollingReference;
                global $i_PollingPeriod, $i_PollingTopic;
                $question = intranet_wordwrap($this->Question,13,"\n",1);
                $x .= "<form action=poll.php method=post onSubmit=\"return checkform(this);\">\n";
                $x .= "<table width=85% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= "<tr><td><img src=/images/frontpage/vote_icon.gif border=0 width=14 height=26></td><td colspan=2>$i_PollingPeriod: ".$this->DateStart." ~ ".$this->DateEnd."<br>$i_PollingTopic: ".$question."</td></tr>\n";
                $x .= "<tr><td><br></td><td></td><td></td></tr>\n";
                $answer_wrap_length = 13;
                
                $x .= ($this->AnswerA=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=A></td><td>".intranet_wordwrap($this->AnswerA,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->AnswerB=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=B></td><td>".intranet_wordwrap($this->AnswerB,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->AnswerC=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=C></td><td>".intranet_wordwrap($this->AnswerC,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->AnswerD=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=D></td><td>".intranet_wordwrap($this->AnswerD,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->AnswerE=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=E></td><td>".intranet_wordwrap($this->AnswerE,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->AnswerF=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=F></td><td>".intranet_wordwrap($this->AnswerF,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->AnswerG=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=G></td><td>".intranet_wordwrap($this->AnswerG,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->AnswerH=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=H></td><td>".intranet_wordwrap($this->AnswerH,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->AnswerI=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=I></td><td>".intranet_wordwrap($this->AnswerI,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->AnswerJ=="") ? "" : "<tr><td><br></td><td><input type=radio name=Answer value=J></td><td>".intranet_wordwrap($this->AnswerJ,$answer_wrap_length,"\n",1)."</td></tr>\n";
                $x .= ($this->Reference=="") ? "" : "<tr><td><br></td><td colspan=2 align=right><a href=javascript:newWindow('".$this->Reference."',5)>$i_PollingReference</a></td></tr>\n";
                $x .= "<tr><td><br></td><td colspan=2>\n";
                $x .= "<input type=image src=$i_frontpage_image_vote border=0>\n";
                $x .= "<a href=javascript:onClick=fe_view_polling(".$this->PollingID.")>$i_frontpage_button_result</a>\n";
                $x .= "</td></tr>\n";
                $x .= "</table>\n";
                $x .= "<input type=hidden name=PollingID value=".$this->PollingID.">\n";
                global $SCRIPT_NAME;
                $x .= "<input type=hidden name=back_url value=\"".$SCRIPT_NAME."\">\n";
                $x .= "</form>\n";
                return $x;
        }

        function displayResult(){
                global $i_PollingDateModified, $i_PollingDateStart, $i_PollingDateEnd, $i_PollingReference;
                global $i_PollingPeriod, $i_PollingTopic,$i_PollingResult;
                $this->Result = $this->returnResult($this->PollingID);
                $x .= "<table width=85% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= "<tr><td><img src=/images/frontpage/vote_icon.gif border=0 width=14 height=26></td><td colspan=2>$i_PollingPeriod: ".$this->DateStart." ~ ".$this->DateEnd."<br>$i_PollingTopic: ".intranet_wordwrap($this->Question,30,"\n",1)." (".sizeof($this->Result).")</td></tr>\n";
                $x .= "<tr><td></td><td colspan=2>$i_PollingResult</td></tr>\n";
                $x .= "<tr><td colspan=3><br></td></tr>\n";
                $x .= ($this->AnswerA=="") ? "" : $this->countResult($this->AnswerA, "A", $this->Result);
                $x .= ($this->AnswerB=="") ? "" : $this->countResult($this->AnswerB, "B", $this->Result);
                $x .= ($this->AnswerC=="") ? "" : $this->countResult($this->AnswerC, "C", $this->Result);
                $x .= ($this->AnswerD=="") ? "" : $this->countResult($this->AnswerD, "D", $this->Result);
                $x .= ($this->AnswerE=="") ? "" : $this->countResult($this->AnswerE, "E", $this->Result);
                $x .= ($this->AnswerF=="") ? "" : $this->countResult($this->AnswerF, "F", $this->Result);
                $x .= ($this->AnswerG=="") ? "" : $this->countResult($this->AnswerG, "G", $this->Result);
                $x .= ($this->AnswerH=="") ? "" : $this->countResult($this->AnswerH, "H", $this->Result);
                $x .= ($this->AnswerI=="") ? "" : $this->countResult($this->AnswerI, "I", $this->Result);
                $x .= ($this->AnswerJ=="") ? "" : $this->countResult($this->AnswerJ, "J", $this->Result);
                $x .= ($this->Reference=="") ? "" : "<tr><td colspan=3 align=right><a href=javascript:newWindow('".$this->Reference."',5)>$i_PollingReference</a></td></tr>\n";
                $x .= "</table>\n";
                return $x;
        }

        function displayResultChart($ReleasePoll='')
        {
        	global $image_path, $LAYOUT_SKIN;
        		if($ReleasePoll == ''){
        			$this->Result = $this->returnResult($this->PollingID);
        		}else{
        		
        			$this->Result = $this->returnReleaseResult($this->PollingID);
        		}
        		
                $x .= "<table width='100%' border='0' cellpadding='2' cellspacing='1'><tr><td><br />\n";
                
                $x .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>\n";
                
                for($i=65;$i<=90;$i++)
                {
	                $barno = ($i-64) % 10;
	                $barno = $barno==0 ? 10 : "0".$barno;
	                $x .= ($this->{"Answer".chr($i)}=="") ? "" : $this->countResult2($this->{"Answer".chr($i)}, chr($i), $this->Result, $barno);
	                
	                /*
	                $x .= ($this->AnswerA=="") ? "" : $this->countResult2($this->AnswerA, "A", $this->Result, '01');
	                $x .= ($this->AnswerB=="") ? "" : $this->countResult2($this->AnswerB, "B", $this->Result, '02');
	                $x .= ($this->AnswerC=="") ? "" : $this->countResult2($this->AnswerC, "C", $this->Result, '03');
	                $x .= ($this->AnswerD=="") ? "" : $this->countResult2($this->AnswerD, "D", $this->Result, '04');
	                $x .= ($this->AnswerE=="") ? "" : $this->countResult2($this->AnswerE, "E", $this->Result, '05');
	                $x .= ($this->AnswerF=="") ? "" : $this->countResult2($this->AnswerF, "F", $this->Result, '06');
	                $x .= ($this->AnswerG=="") ? "" : $this->countResult2($this->AnswerG, "G", $this->Result, '07');
	                $x .= ($this->AnswerH=="") ? "" : $this->countResult2($this->AnswerH, "H", $this->Result, '08');
	                $x .= ($this->AnswerI=="") ? "" : $this->countResult2($this->AnswerI, "I", $this->Result, '09');
	                $x .= ($this->AnswerJ=="") ? "" : $this->countResult2($this->AnswerJ, "J", $this->Result, '10');
	                */
                }
                
                $x .= "<tr height='1' background='/images/'>"; 
		$x .= "<td width='5' height='1'><img src='$image_path/$LAYOUT_SKIN/barchart/line.gif' width='5' height='1'></td>";
		$x .= "<td width='1' height='1' background='$image_path/$LAYOUT_SKIN/barchart/line.gif'><img src='$image_path/$LAYOUT_SKIN/barchart/line.gif' width='1' height='1'></td>";
		$x .= "<td align='center' valign='middle' background='$image_path/$LAYOUT_SKIN/barchart/line.gif'><img src='$image_path/$LAYOUT_SKIN/barchart/line.gif' width='1' height='1'></td>";
		$x .= "</tr>";
                
                $x .= "<tr>"; 
		$x .= "<td height='5'><img src='$image_path/$LAYOUT_SKIN/10x10.gif' height='5'></td>";
		$x .= "<td width='1' height='5'><img src='$image_path/$LAYOUT_SKIN/barchart/line.gif' width='1' height='5'></td>";
		$x .= "<td height='5' align='center' valign='middle'><img src='$image_path/$LAYOUT_SKIN/10x10.gif' height='5'></td>";
		$x .= "</tr>";
                
                $x .= ($this->Reference=="") ? "" : "<tr><td colspan=3 align=right><a href=javascript:newWindow('".$this->Reference."',5)>$i_PollingReference</a></td></tr>\n";
                $x .= "</table>\n";
                
                $x .= "</td></tr></table>\n";
                return $x;
        }
        
        ###############################################################################

        function returnPollingList($UserID){
                $GroupPollingIDs = $this->db_sub_select("SELECT PollingID FROM INTRANET_GROUPPOLLING");
                $sql  = "SELECT a.PollingID, b.UserID
                FROM INTRANET_POLLING AS a LEFT OUTER JOIN INTRANET_POLLINGRESULT AS b
                ON a.PollingID = b.PollingID AND b.UserID = $UserID
                WHERE a.DateStart <= CURDATE()
                AND a.DateEnd >= CURDATE()
                AND a.PollingID NOT IN ($GroupPollingIDs)
                ORDER BY a.DateStart DESC, a.DateEnd DESC, a.DateModified ASC";
                return $this->returnArray($sql,2);
        }

        function displayPollingList($UserID){
                global $i_frontpage_home_title_vote, $i_frontpage_home_title_pastvote,$i_frontpage_pastpoll,$i_frontpage_home_title_vote2;
                $row = $this->returnPollingList($UserID);
                if(sizeof($row)<>0){
                        $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>\n";
                        $x .= "<tr><td class=td_bottom align=right>$i_frontpage_home_title_vote</td>
                        <td width=125 align=center><a href=javascript:fe_pastpoll();>$i_frontpage_home_title_pastvote <span class=decription>$i_frontpage_pastpoll</span></a></td></tr>\n";
                        $x .= "</table>\n";
                        $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>
                                 <tr>
                                   <td>$i_frontpage_home_title_vote2</td>
                                 </tr>
                               </table>\n";
                        for($i=0; $i<sizeof($row); $i++){
                                //if($i == 1) break;
                                $PollingID = $row[$i][0];
                                $IsPolled = $row[$i][1];
                                $this->libpolling($PollingID);
                                $x .= "<table width=203 border=0 cellpadding=0 cellspacing=0 background=/images/index/pollingtb.gif>\n";
                               // $x .= "<tr><td><img src=/images/frontpage/vote_bg_t.jpg border=0 width=189 height=6></td></tr>\n";
                                $x .= "<tr><td align=center>\n";
                                if ($IsPolled && $this->DateRelease <= date('Y-m-d'))
                                {
                                    $x .= $this->displayResult();
                                }
                                else if (!$IsPolled)
                                {
                                     $x .= $this->display();
                                }
                                #$x .=  ? $this->displayResult() : $this->display();
                                $x .= "</td></tr>\n";
//                                $x .= "<tr><td><img src=/images/frontpage/vote_bg_b.jpg border=0 width=189 height=6></td></tr>\n";
                                $x .= "</table>\n";
                        }
                        $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>
                                 <tr>
                                   <td><img src=/images/index/pollingbottom.gif width=203 height=8></td>
                                 </tr>
                               </table>\n";
                }
                return $x;
        }
        
        
        function displayPollingList2($UserID){
                global $i_frontpage_home_title_vote, $i_frontpage_home_title_pastvote,$i_frontpage_pastpoll,$i_frontpage_home_title_vote2;
                
                $row = $this->returnPollingList($UserID);
                
                if(sizeof($row)<>0){
                        $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>\n";
                        $x .= "<tr><td class=td_bottom align=right>$i_frontpage_home_title_vote</td>
                        <td width=125 align=center><a href=javascript:fe_pastpoll();>$i_frontpage_home_title_pastvote <span class=decription>$i_frontpage_pastpoll</span></a></td></tr>\n";
                        $x .= "</table>\n";
                        $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>
                                 <tr>
                                   <td>$i_frontpage_home_title_vote2</td>
                                 </tr>
                               </table>\n";
                        for($i=0; $i<sizeof($row); $i++){
                                $PollingID = $row[$i][0];
                                $IsPolled = $row[$i][1];
                                $this->libpolling($PollingID);
                                $x .= "<table width=203 border=0 cellpadding=0 cellspacing=0 background=/images/index/pollingtb.gif>\n";
                                $x .= "<tr><td align=center>\n";
                                if ($IsPolled && $this->DateRelease <= date('Y-m-d'))
                                {
                                    $x .= $this->displayResult();
                                }
                                else if (!$IsPolled)
                                {
                                     $x .= $this->display();
                                }

                                $x .= "</td></tr>\n";
                                $x .= "</table>\n";
                        }
                        $x .= "<table width=203 border=0 cellspacing=0 cellpadding=0>
                                 <tr>
                                   <td><img src=/images/index/pollingbottom.gif width=203 height=8></td>
                                 </tr>
                               </table>\n";
                }
                return $x;
        }

        ###############################################################################

        function returnPastPoll(){
                $GroupPollingIDs = $this->db_sub_select("SELECT PollingID FROM INTRANET_GROUPPOLLING");
                $sql  = "SELECT PollingID, Question, DATE_FORMAT(DateStart, '%Y-%m-%d'), DATE_FORMAT(DateEnd, '%Y-%m-%d')
                FROM INTRANET_POLLING WHERE PollingID NOT IN ($GroupPollingIDs) AND DateEnd < CURDATE()
                ORDER BY DateStart DESC, DateEnd DESC, DateModified ASC";
                return $this->returnArray($sql,4);
        }

        function displayPastPoll(){
                $row = $this->returnPastPoll();
                $x .= "<table width=90% border=0 cellpadding=2 cellspacing=1>\n";
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($row); $i++){
                                $PollingID = $row[$i][0];
                                $Question = intranet_wordwrap($row[$i][1],50,"\n",1);
                                $DateStart = $row[$i][2];
                                $DateEnd = $row[$i][3];
                                $x .= "<tr><td><img src=/images/frontpage/vote_icon.gif border=0 width=14 height=26><br><br></td><td>$DateStart ~ $DateEnd<br><a href=javascript:onClick=fe_view_polling($PollingID)>$Question</a></td></tr>\n";
                        }
                }
                $x .= "</table>\n";
                return $x;
        }
        
        function GET_ADMIN_USER()
	{
		global $intranet_root;

		$lf = new libfilesystem();
		$AdminUser = trim($lf->file_read($intranet_root."/file/polling/admin_user.txt"));
		
		return $AdminUser;
	}
        
        function hasAccessRight($UserID)
	{
		$AdminUser = $this->GET_ADMIN_USER();
                return (in_array($UserID, explode(",",$AdminUser)));

	}
        
        function UserisVoted($UserID)
        {
		$sql = "SELECT Answer FROM INTRANET_POLLINGRESULT WHERE PollingID = '$this->PollingID' and UserID='$UserID'";
                return $this->returnArray($sql,1) ? true : false;        	
        }
        
        function returnUnPollingList($UserID){
                $GroupPollingIDs = $this->db_sub_select("SELECT PollingID FROM INTRANET_GROUPPOLLING");
                
                $sql  = "SELECT a.PollingID, b.UserID
                FROM INTRANET_POLLING AS a LEFT OUTER JOIN INTRANET_POLLINGRESULT AS b
                ON a.PollingID = b.PollingID AND b.UserID = $UserID
                WHERE a.DateStart <= CURDATE()
                AND a.DateEnd >= CURDATE()
                AND a.PollingID NOT IN ($GroupPollingIDs)
                and b.UserID is null
                ";
                return $this->returnArray($sql,2);
        }
		
		function GET_MODULE_OBJ_ARR(){
			global $image_path, $LAYOUT_SKIN, $i_adminmenu_im_polling, $PATH_WRT_ROOT, $CurrentPageArr, $UserID, $i_frontpage_currentpoll, $i_frontpage_pastpoll, $CurrentPage;
			$lu = new libuser($UserID);
			$PagePast = 0;
			$PageCurrent = 0;
			switch ($CurrentPage) {					
				case "ePolling":
					$CurrentPageArr['ePolling'] = 1;
					break;
					
				case "PagePollingPast":
					$CurrentPageArr['PagePolling'] = 1;
					$PagePast =1;
					break;
				case "PagePollingCurrent":
					$CurrentPageArr['PagePolling'] = 1;
					$PageCurrent =1;
					break;
			};
			
			$MenuArr = Array();
			$MODULE_OBJ =  Array();
			 if($CurrentPageArr['ePolling']==1)
			{
				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"]) {
					$MenuArr["Polling"] 	= array($i_adminmenu_im_polling, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/polling/index.php", 1);
				}
			}
			elseif ($CurrentPageArr['PagePolling'] == 1)
			{
				if (!$lu->isParent())
				{
					$MenuArr["Polling"] 							= array($i_adminmenu_im_polling, $PATH_WRT_ROOT."home/eService/polling/index.php", 1);
// 					$MenuArr["Polling"]["Child"]["CurrentPolls"] 	= array($i_frontpage_currentpoll, $PATH_WRT_ROOT."home/eService/polling/index.php", $PageCurrent, "$image_path/$LAYOUT_SKIN/polling/icon_currentpolls.gif");
// 					$MenuArr["Polling"]["Child"]["PastPolls"] 		= array($i_frontpage_pastpoll, $PATH_WRT_ROOT."home/eService/polling/past_polls.php", $PagePast, "$image_path/$LAYOUT_SKIN/polling/icon_pastpolls.gif");
					$MenuArr["Polling"]["Child"]["CurrentPolls"] 	= array($i_frontpage_currentpoll, $PATH_WRT_ROOT."home/eService/polling/index.php", $PageCurrent);
					$MenuArr["Polling"]["Child"]["PastPolls"] 		= array($i_frontpage_pastpoll, $PATH_WRT_ROOT."home/eService/polling/past_polls.php", $PagePast);
				}	
			}

            # change page web title
            $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
            $js.= 'document.title="eClass Poll";'."\n";
            $js.= '</script>'."\n";

			### module information
			$MODULE_OBJ['title'] = $i_adminmenu_im_polling.$js;
			$MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eoffice.gif";
			if($CurrentPageArr['ePolling']==1)
				$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/polling/index.php";
			else
				$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eService/polling/index.php";
			$MODULE_OBJ['menu'] = $MenuArr;
			$MODULE_OBJ['title_css'] = "menu_opened";
			return $MODULE_OBJ;
		
		}
		
		function returnResultDetails($PollingID)
        {
	        # whole school or Group polling?
	        $sql = "select GroupID from INTRANET_GROUPPOLLING where PollingID='$PollingID'";
	        $result = $this->returnVector($sql);
	        if(sizeof($result))		# Group
	        {
	        	$GroupStr = implode(",",$result);
	        	$group_cond = " inner join INTRANET_USERGROUP as b on (b.UserID = a.UserID and b.GroupID in ($GroupStr)) ";
        	}
	        	
	        $sql = "select 
	        				distinct(a.UserID),
							a.EnglishName,
							a.ChineseName,
							a.ClassName,
							a.ClassNumber,
							a.RecordType,
							c.Answer, 
							c.DateInput
						from
							INTRANET_USER as a 
							$group_cond
							left join INTRANET_POLLINGRESULT as c on (c.UserID = a.UserID and c.PollingID='$PollingID')
						order by 
							a.EnglishName
		        		";
	        $result1 = $this->returnArray($sql);
	        
	        # retireve "ghost student" records XD
	         $sql = "select 
	         				distinct(b.UserID),
							'--',
							'--',
							'--',
							'--',
							'--',
							a.Answer, 
							a.DateInput
						from
							INTRANET_POLLINGRESULT as a
							left join INTRANET_USER as b on (b.UserID=a.UserID)
						where
							a.PollingID='$PollingID' and 
							b.UserID is NULL
		        		";
	        $result2 = $this->returnArray($sql);
	        
	        $result = array_merge($result1, $result2);
	        
	        # rebuild
			$result3 = array();
			for($i=0;$i<sizeof($result);$i++)
			{
				$result3[$i] = array_slice($result[$i],2);
			}
			
	        return $result;
        }
        
        function returnTargetGroupsArray($PollingID)
        {
        	if ($PollingID == "") return array();
        	$sql = "SELECT DISTINCT a.GroupID, a.Title
                         FROM INTRANET_GROUPPOLLING as b
                              LEFT OUTER JOIN INTRANET_GROUP as a ON b.GroupID = a.GroupID
                         WHERE PollingID = '".$PollingID."'";
        	return $this->returnArray($sql,2);
        }
        
        function isPollingEditable($PollingID)
        {
        	return !$this->isPollingFilled($PollingID);
        }
        function isPollingFilled($PollingID)
        {
        	$sql = "SELECT COUNT(PollingResultID) FROM INTRANET_POLLINGRESULT WHERE PollingID = '".$PollingID."'";
        	$result = $this->returnVector($sql);
        	return ($result[0]!=0);
        }
        
        function returnPolling_IP25($returnSQLonly=0, $currPolls = true)
        {
        	global $UserID, $Lang, $button_view;
            
        	if ($currPolls) {
        		$date_conds = " AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()";
        	} else {
        		// $date_conds = " AND a.DateEnd < CURDATE()";
        	}
            
        	# retrieve WHOLE SCHOOL survey
        	$sql = "SELECT a.PollingID FROM INTRANET_POLLING as a
                	   LEFT OUTER JOIN INTRANET_GROUPPOLLING as b ON a.PollingID = b.PollingID
                	WHERE b.PollingID IS NULL AND a.RecordStatus NOT IN (2,3) $date_conds";
        	$idschool = $this->returnVector($sql);
            
        	# retrieve GROUP survey
        	// $sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$UserID' and GroupID is not NULL";
        	$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$UserID' and GroupID is not NULL";
        	$groups = $this->returnVector($sql);
        	if (sizeof($groups) != 0)
        	{
        		$group_list = implode(",",$groups);
        
        		$sql = "SELECT DISTINCT a.PollingID FROM INTRANET_POLLING as a, INTRANET_GROUPPOLLING as b
        		          WHERE a.PollingID = b.PollingID AND b.GroupID IN ($group_list) AND a.RecordStatus NOT IN (2,3)  $date_conds";
        		$idgroup = $this->returnVector($sql);
        		$overall = array_merge($idschool,$idgroup);
        	}
        	else
        	{
        		$overall = $idschool;
        	}
            
        	if ($returnSQLonly == 0)
        	{
        		if (sizeof($overall) == 0) {
        			return 0;
        		} else {
    				return $overall;
                }
        	}
            
        	if(sizeof($overall) > 0) {
        		$list = implode(",", $overall);
        	} else {
        		$list = -999;
        	}
        	
        	$namefield = getNameFieldWithClassNumberByLang("c.");
        	
        	$sql ="set @c=0";
        	$this->db_db_query($sql);
            
        	if ($currPolls)
        	{
	        	$sql = "SELECT
	        				CONCAT('<nobr>', DATE_FORMAT(a.DateStart, '%Y-%m-%d'), '</nobr>'),
							CONCAT('<nobr>', DATE_FORMAT(a.DateEnd, '%Y-%m-%d'), '</nobr>'),
							IF(b.UserID IS NULL,
								CONCAT('<a class=\'tablegreenlink\' href=\'poll.php?PollingID[]=', a.PollingID, '\'>', a.Question, '</a>'),
								a.Question
							),
							IF(a.DateRelease <= CURDATE(),
								CONCAT('<a class=\'tablegreenlink\' href=\'result.php?fr=1&PollingID=', a.PollingID, '\'>$button_view</a>'),
								CONCAT('{$Lang['Polling']['ToBeReleasedOn']}<br />', DATE_FORMAT(a.DateRelease, '%Y-%m-%d'))
							)
			        	FROM
			        		INTRANET_POLLING as a
    	        			LEFT OUTER JOIN INTRANET_POLLINGRESULT as b ON a.PollingID = b.PollingID AND b.UserID = '$UserID'
    	        			LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = b.UserID
	        			WHERE
	        				a.PollingID IN ($list) AND a.DateEnd >= CURDATE() ";
        	}
        	else
        	{
        		$sql = "SELECT
			        		CONCAT('<nobr>', DATE_FORMAT(a.DateStart, '%Y-%m-%d'), '</nobr>'),
							CONCAT('<nobr>', DATE_FORMAT(a.DateEnd, '%Y-%m-%d'), '</nobr>'),
							a.Question,
							if(a.DateRelease <= CURDATE(), 
								CONCAT('<a class=\'tablebluelink\' href=\'result.php?fr=0&PollingID=', a.PollingID, '\'>$button_view</a>'),
								CONCAT('{$Lang['Polling']['ToBeReleasedOn']}<br />', DATE_FORMAT(a.DateRelease, '%Y-%m-%d'))
							)
			        	FROM
			        		INTRANET_POLLING as a
    			        	LEFT OUTER JOIN INTRANET_POLLINGRESULT as b ON a.PollingID = b.PollingID AND b.UserID = '$UserID'
    			        	LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = b.UserID
			        	WHERE
			        		a.PollingID IN ($list) AND a.DateEnd < CURDATE() ";
        	}
        	// echo $sql;
        	//LEFT OUTER JOIN INTRANET_GROUP as d ON d.GroupID = a.OwnerGroupID
        
        	// ORDER BY a.DateEnd";
        	if($returnSQLonly) {
        		return $sql;
        	} else {
    			return $this->returnArray($sql);
        	}
        }
}
?>