<?php
#  Editing by C

class libreportcardrubrics_topic extends libreportcardrubrics {
	
	/**
	 * Constructor
	 */
	function libreportcardrubrics_topic($AcademicYearID='') {
		parent:: libreportcardrubrics($AcademicYearID);
	}
	
	function Get_Topic_Level_Name($Level, $ParLang='')
	{
		global $Lang;
		
		$NameEn = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$Level]['TitleEn'];
		$NameCh = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$Level]['TitleCh'];
		
		if (strtoupper($ParLang) == 'EN')
			return $NameEn;
		else if (strtoupper($ParLang) == 'CH')
			return $NameCh;
		else if ($ParLang == '')
			return Get_Lang_Selection($NameCh, $NameEn);
	}
	
	function Get_Topic_Info($SubjectIDArr='', $TopicIDArr='', $Level='', $TopicCode='', $PreviousLevelTopicID='', $ParentTopicID='', $ReturnAsso=1, $MaxLevel='', $YearIDArr='')
	{
		$RC_TOPIC = $this->Get_Table_Name('RC_TOPIC');
		//$RC_TOPIC_PARENT = $this->Get_Table_Name('RC_TOPIC');
		$RC_TOPIC_APPLICABLE_FORM_MAPPING = $this->Get_Table_Name('RC_TOPIC_APPLICABLE_FORM_MAPPING');
		
		$conds_SubjectID = '';
		if ($SubjectIDArr != '' )
			$conds_SubjectID = " And t.SubjectID IN (".implode(",", (array)$SubjectIDArr).") ";
		
		$conds_TopicID = '';
		if ($TopicIDArr != '' )
			$conds_TopicID = " And t.TopicID IN (".implode(",", (array)$TopicIDArr).") ";
			
		$conds_Level = '';
		if ($Level != '' )
			$conds_Level = " And t.Level = '$Level' ";
			
		$conds_TopicCode = '';
		if ($TopicCode != '' )
			$conds_TopicCode = " And t.TopicCode IN ('".implode("','",(array)$TopicCode)."') ";
			
		$conds_PreviousLevelTopicID = '';
		if ($PreviousLevelTopicID != '' )
			$conds_PreviousLevelTopicID = " And t.PreviousLevelTopicID = '$PreviousLevelTopicID' ";
			
		$conds_ParentTopicID = '';
		if ($ParentTopicID != '' )
			$conds_ParentTopicID = " And t.ParentTopicID = '$ParentTopicID' ";
			
		$conds_MaxLevel = '';
		if ($MaxLevel != '' )
			$conds_MaxLevel = " And t.Level <= $MaxLevel ";
		
		$Join_RC_TOPIC_APPLICABLE_FORM_MAPPING = '';
		$conds_YearID = '';
		if ($YearIDArr != '')
		{
			$Join_RC_TOPIC_APPLICABLE_FORM_MAPPING = " Left Outer Join $RC_TOPIC_APPLICABLE_FORM_MAPPING as tafm On (t.TopicID = tafm.TopicID) ";
			$conds_YearID = " And tafm.YearID In (".implode(',', (array)$YearIDArr).") ";
		}
		
		$sql = "Select
						t.*
				From
						$RC_TOPIC as t
						$Join_RC_TOPIC_APPLICABLE_FORM_MAPPING
				Where
						t.RecordStatus = 1
						$conds_SubjectID
						$conds_TopicID
						$conds_Level
						$conds_TopicCode
						$conds_PreviousLevelTopicID
						$conds_ParentTopicID
						$conds_MaxLevel
						$conds_YearID
				Order By
						t.Level Asc, t.DisplayOrder Asc
				";
		$ResultArr = $this->returnArray($sql);
//debug_r($sql);
		if ($ReturnAsso == 1)
		{
			$numOfTopic = count($ResultArr);
			$TopicAssoArr = array();
			for ($i=0; $i<$numOfTopic; $i++)
			{
				$thisTopicID = $ResultArr[$i]['TopicID'];			
				$TopicAssoArr[$thisTopicID] = $ResultArr[$i];
			}
			
			return $TopicAssoArr;
		}
		else
		{
			return $ResultArr;
		}
	}
	
	function Get_Topic_Applicable_Form_Mapping($TopicIDArr='')
	{
		if ($TopicIDArr != '')
			$conds_TopicID = " And tafm.TopicID In (".implode(',', (array)$TopicIDArr).") ";
		
		$RC_TOPIC_APPLICABLE_FORM_MAPPING = $this->Get_Table_Name('RC_TOPIC_APPLICABLE_FORM_MAPPING');
		$sql = "Select
						tafm.TopicID, 
						y.YearID, 
						y.YearName
				From
						$RC_TOPIC_APPLICABLE_FORM_MAPPING as tafm
						Inner Join
						YEAR as y On (tafm.YearID = y.YearID)
				Where
						1
				Order by
						y.Sequence
				";
		$MappingInfoArr = $this->returnArray($sql);
		return BuildMultiKeyAssoc($MappingInfoArr, 'TopicID', array(), $SingleValue=0, $BuildNumericArray=1); 
	}
	
	/*
	 * Recursively Get the Topic Structure
	 */
	function Get_Topic_Tree($SubjectIDArr, $ParentTopicIDArr='', $MaxLevel='', $YearIDArr='')
	{
		global $eRC_Rubrics_ConfigArr;
		
		$RC_TOPIC = $this->Get_Table_Name('RC_TOPIC');
		$RC_TOPIC_APPLICABLE_FORM_MAPPING = $this->Get_Table_Name('RC_TOPIC_APPLICABLE_FORM_MAPPING');		
		
		$conds_ParentTopicIDArr = '';
		if ($ParentTopicIDArr != '')
		{
			if (!is_array($ParentTopicIDArr))
				$ParentTopicIDArr = array($ParentTopicIDArr);
				
			$conds_ParentTopicIDArr = " And t.ParentTopicID In (".implode(',', $ParentTopicIDArr).") ";
		}
		
		$conds_MaxLevel = '';
		if ($MaxLevel != '')
			$conds_MaxLevel = " And t.Level <= $MaxLevel ";
			
		if (!is_array($SubjectIDArr))
			$TargetSubjectIDArr = array($SubjectIDArr);
		else
			$TargetSubjectIDArr = $SubjectIDArr;
			
		$Join_RC_TOPIC_APPLICABLE_FORM_MAPPING = '';
		$conds_YearID = '';
		if ($YearIDArr != '')
		{
			$MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
			
			$Join_RC_TOPIC_APPLICABLE_FORM_MAPPING = " Left Outer Join $RC_TOPIC_APPLICABLE_FORM_MAPPING as tafm On (t.TopicID = tafm.TopicID) ";
			$conds_YearID = " And (t.Level < ".$MaxTopicLevel." || tafm.YearID In (".implode(',', (array)$YearIDArr).")) ";
		}
		
		
		$sql = "Select
						t.TopicID,
						t.PreviousLevelTopicID,
						t.SubjectID
				From
						$RC_TOPIC as t
						$Join_RC_TOPIC_APPLICABLE_FORM_MAPPING
				Where
						t.SubjectID In (".implode(',', $TargetSubjectIDArr).")
						And
						t.RecordStatus = 1
						$conds_ParentTopicIDArr
						$conds_MaxLevel
						$conds_YearID
				Order By
						t.Level Asc, t.DisplayOrder Asc
				";
		$TopicInfoArr = $this->returnArray($sql);
		$numOfTopic = count($TopicInfoArr);
		
		$TopicMappingArr = array();
		for ($i=0; $i<$numOfTopic; $i++)
		{
			$thisTopicID				= $TopicInfoArr[$i]['TopicID'];
			$thisPreviousLevelTopicID 	= $TopicInfoArr[$i]['PreviousLevelTopicID'];
			$thisSubjectID				= $TopicInfoArr[$i]['SubjectID'];
			
			if ($thisPreviousLevelTopicID == $thisTopicID)
				(array)$TopicMappingArr[$thisSubjectID][0][] = $thisTopicID;
			else
				(array)$TopicMappingArr[$thisSubjectID][$thisPreviousLevelTopicID][] = $thisTopicID;
		}
		
		$ReturnArr = array();
		$numOfSubject = count($TargetSubjectIDArr);
		for ($i=0; $i<$numOfSubject; $i++)
		{
			$thisSubjectID = $TargetSubjectIDArr[$i];
			$thisTopicMappingArr = $TopicMappingArr[$thisSubjectID];
			
			$ReturnArr[$thisSubjectID] = $this->Get_Topic_Tree_Recursion(0, $thisTopicMappingArr, array(), 1);
		}
		
		if (is_array($SubjectIDArr))
			return $ReturnArr;
		else
			return $ReturnArr[$SubjectIDArr];
	}
	
	function Get_Topic_Tree_Recursion($TopicID, $TopicMappingArr, $ResultArr, $Level)
	{
		global $eRC_Rubrics_ConfigArr;
		
		$LinkedTopicArr = $TopicMappingArr[$TopicID];
		$numOfLinkedTopic = count($LinkedTopicArr);
		
			
		if ($numOfLinkedTopic > 0)
		{
			for ($i=0; $i<$numOfLinkedTopic; $i++)
			{
				$thisTopicID = $LinkedTopicArr[$i];
				
				if ($Level > $eRC_Rubrics_ConfigArr['MaxLevel'])
					continue;
				
				$ResultArrTmp = $this->Get_Topic_Tree_Recursion($thisTopicID, $TopicMappingArr, $ResultArr[$thisTopicID]=array(), $Level, $LastLevelTopicID);
				$ResultArr[$thisTopicID] = $ResultArrTmp;
			}
		}
		
		$Level++;
		return $ResultArr;
	}
	
	function Is_Topic_Code_Valid($Value, $ExcludeTopicID='')
	{
		$Value = $this->Get_Safe_Sql_Query(trim($Value));
		
		$conds_ExcludeTopicID = '';
		if ($ExcludeTopicID != '')
			$conds_ExcludeTopicID = " And TopicID != '$ExcludeTopicID' ";
		
		$TopicTable = $this->Get_Table_Name('RC_TOPIC');
		$sql = "Select
						TopicID
				From
						$TopicTable
				Where
						TopicCode = '$Value'
						And
						RecordStatus = 1
						$conds_ExcludeTopicID
				";
		$ResultArr = $this->returnArray($sql);
		return (count($ResultArr) > 0)? false : true;
	}
	
	function Get_Topic_Last_Modified($SubjectID, $Level, $PreviousLevelTopicID='')
	{
		$conds_PreviousLevelTopicID = '';
		if ($PreviousLevelTopicID != '')
			$conds_PreviousLevelTopicID = " And PreviousLevelTopicID = '$PreviousLevelTopicID' ";
		
		$TopicTable = $this->Get_Table_Name('RC_TOPIC');
		$sql = "Select 
						DateModified, LastModifiedBy
				From
						$TopicTable
				Where
						DateModified IN 
								(SELECT MAX(DateModified) FROM $TopicTable Where SubjectID = '$SubjectID' And Level = '$Level' $conds_PreviousLevelTopicID)
				";
		return $this->returnArray($sql);
	}
	
	function Get_Topic_Form_Mapping_Last_Modified($TopicID)
	{
		$RC_TOPIC_APPLICABLE_FORM_MAPPING = $this->Get_Table_Name('RC_TOPIC_APPLICABLE_FORM_MAPPING');
		$sql = "Select 
						DateModified, LastModifiedBy
				From
						$RC_TOPIC_APPLICABLE_FORM_MAPPING
				Where
						DateModified IN 
								(SELECT MAX(DateModified) FROM $RC_TOPIC_APPLICABLE_FORM_MAPPING Where TopicID = '$TopicID')
				";
		return $this->returnArray($sql);
	}
	
	function Get_Topic_Max_Display_Order($Subject, $Level)
	{
		$TopicTable = $this->Get_Table_Name('RC_TOPIC');
		$sql = "SELECT max(DisplayOrder) FROM $TopicTable Where SubjectID = '$Subject' And Level = '$Level' And RecordStatus = 1";
		$MaxDisplayOrder = $this->returnVector($sql);
		
		return $MaxDisplayOrder[0];
	}
	
	function Add_Topic($DataArr, $YearIDArr='')
	{
		global $eRC_Rubrics_ConfigArr;
		
		$MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		if (!is_array($DataArr) || count($DataArr) == 0)
			return false;
		
		# set field and value string
		$fieldArr = array();
		$valueArr = array();
		foreach ($DataArr as $field => $value)
		{
			$fieldArr[] = $field;
			$valueArr[] = '\''.$this->Get_Safe_Sql_Query($value).'\'';
		}
		
		## set others fields
		# DateInput
		$fieldArr[] = 'DateInput';
		$valueArr[] = 'now()';
		# InputBy
		$fieldArr[] = 'InputBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		# DateModified
		$fieldArr[] = 'DateModified';
		$valueArr[] = 'now()';
		# LastModifiedBy
		$fieldArr[] = 'LastModifiedBy';
		$valueArr[] = "'".$_SESSION['UserID']."'";
		
		$fieldText = implode(", ", $fieldArr);
		$valueText = implode(", ", $valueArr);
		
		# Insert Record
		$this->Start_Trans();
		
		$TopicTable = $this->Get_Table_Name('RC_TOPIC');
		$sql = "Insert Into $TopicTable ($fieldText) Values ($valueText)";
		$success = $this->db_db_query($sql);
		if ($success == false)
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$insertedID = $this->db_insert_id();
			
			// For Level 1 Topic, TopicID == ParentTopicID == PreviousLevelTopicID
			if ($DataArr['Level'] == 1)
			{
				$UpdateDataArr = array();
				$UpdateDataArr['ParentTopicID'] = $insertedID;
				$UpdateDataArr['PreviousLevelTopicID'] = $insertedID;
				$SuccessUpdate = $this->Update_Topic($insertedID, $UpdateDataArr, $UpdateLastModified=0);
				if ($SuccessUpdate)
				{
					$this->Commit_Trans();
				}
				else
				{
					$this->RollBack_Trans();
					return 0;
				}
			}
			else
			{
				$this->Commit_Trans();
				
				if ($DataArr['Level'] == $MaxTopicLevel)
				{
					$this->Save_Topic_Form_Mapping($insertedID, $YearIDArr);
				}
			}
			
			return $insertedID;
		}
	}
	
	function Update_Topic($TopicID, $DataArr, $UpdateLastModified=1)
	{
		if (!is_array($DataArr) || count($DataArr) == 0)
			return false;
		
		# Build field update values string
		$valueFieldArr = array();
		foreach ($DataArr as $field => $value)
		{
			$valueFieldArr[] = " $field = '".$this->Get_Safe_Sql_Query($value)."' ";
		}
		
		if ($UpdateLastModified==1)
		{
			$valueFieldArr[] = " DateModified = now() ";
			$valueFieldArr[] = " LastModifiedBy = '".$_SESSION['UserID']."' ";
		}
		
		$valueFieldText .= implode(',', $valueFieldArr);
		
		$TopicTable = $this->Get_Table_Name('RC_TOPIC');
		$sql = "Update $TopicTable Set $valueFieldText Where TopicID = '$TopicID'";
		$success = $this->db_db_query($sql);
		return $success;
	}
	
	function Delete_Topic($TopicID, $DeleteRelated=1)
	{
		$DataArr = array();
		$DataArr['RecordStatus'] = 0;
		$SuccessArr['Delete_Topic'] = $this->Update_Topic($TopicID, $DataArr);
		
		if ($DeleteRelated==1)
		{
			$RelatedTopicInfoArr = $this->Get_Topic_Info($SubjectID='', '', $Level='', $TopicCode='', $PreviousLevelTopicID=$TopicID, $ParentTopicID='', $ReturnAsso=0);
			$numOfTopic = count($RelatedTopicInfoArr);
			
			for ($i=0; $i<$numOfTopic; $i++)
			{
				$thisTopicID = $RelatedTopicInfoArr[$i]['TopicID'];
				$SuccessArr['Delete_Related_Topic'][$thisTopicID] = $this->Delete_Topic($thisTopicID, $DeleteRelated=1);
			}
		}
		
		return !in_array(false, $SuccessArr);
	}
	
	// para: $FilterTopicInfoText = 'Level,TopicID::Level,TopicID::'
	function Get_Topic_FilterArr_From_FilterText($FilterTopicInfoText)
	{
		$FilterLevelTopicArr = array();
		$FilterTopicInfoText = trim($FilterTopicInfoText);
		
		if ($FilterTopicInfoText != '')
		{
			### Get the Parent Topic to Filter Table
			$FilterTopicInfoText = substr($FilterTopicInfoText, 0, strlen($FilterTopicInfoText) - 2);	// remove the last "::"
			$FilterLevelInfoArr = explode('::', $FilterTopicInfoText);
			$numOfLevel = count($FilterLevelInfoArr);
			
			$FilterLevelTopicArr = array();		// $FilterLevelTopicArr[$Level][$TopicID] = 1;
			for ($i=0; $i<$numOfLevel; $i++)
			{
				$thisText = $FilterLevelInfoArr[$i];
				$thisTmpArr = explode(',', $thisText);
				$thisLevel = $thisTmpArr[0];
				$thisTopicID = $thisTmpArr[1];
				
				$FilterLevelTopicArr[$thisLevel][$thisTopicID] = 1;
			}
		}
		
		return $FilterLevelTopicArr;
	}
	
	function Get_Topic_Import_Export_Column_Property($forGetCsvContent=0)
	{
		global $eRC_Rubrics_ConfigArr;
		
		$MaxTopicLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
		
		$PropertyArr = array();		// 1: Required, 2: Reference, 3: Optional
		
		# Subject Code, Subject NameEn, Subject NameCh
		$PropertyArr[] = 1;
		$PropertyArr[] = ($forGetCsvContent)? 1 : 2;
		$PropertyArr[] = ($forGetCsvContent)? 1 : 2;
		
		# Topic Info
		for ($i=0; $i<$MaxTopicLevel; $i++)
		{
			# Code
			$PropertyArr[] = 1;
			
			# NameEn
			$PropertyArr[] = 1;
			
			# NameCh
			$PropertyArr[] = 1;
			
			# Remarks
			if ($i == 0)
				$PropertyArr[] = ($forGetCsvContent)? 1 : 3;
				
			# Form Mapping
			if ($i == $eRC_Rubrics_ConfigArr['MaxLevel'] - 1)
				$PropertyArr[] = 1;
		}
		
		return $PropertyArr;
	}
	
	function Get_Topic_Import_Export_Column_Title_Arr($ParLang='', $TargetPropertyArr='')
	{
		global $Lang, $eRC_Rubrics_ConfigArr;
		
		$ColumnArr = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportExportColumn'];
		for ($i=1; $i<=$eRC_Rubrics_ConfigArr['MaxLevel']; $i++)
		{
			$thisTopicTitleEn = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['TitleEn'];
			$thisTopicTitleCh = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$i]['TitleCh'];
			
			$ColumnArr['En'][] = $thisTopicTitleEn.' '.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['CodeEn'];
			$ColumnArr['En'][] = $thisTopicTitleEn.' '.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Eng)En'];
			$ColumnArr['En'][] = $thisTopicTitleEn.' '.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Chi)En'];
			$ColumnArr['Ch'][] = $thisTopicTitleCh.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['CodeCh'];
			$ColumnArr['Ch'][] = $thisTopicTitleCh.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Eng)Ch'];
			$ColumnArr['Ch'][] = $thisTopicTitleCh.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['Name(Chi)Ch'];
			
			if ($i == 1)
			{
				$ColumnArr['En'][] = $thisTopicTitleEn.' '.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['RemarksEn'];
				$ColumnArr['Ch'][] = $thisTopicTitleCh.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['RemarksCh'];
			}
			
			if ($i == $eRC_Rubrics_ConfigArr['MaxLevel'])
			{
				# Form Mapping
				$ColumnArr['En'][] = $thisTopicTitleEn.' '.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ApplicableFormEn'];
				$ColumnArr['Ch'][] = $thisTopicTitleCh.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ApplicableFormCh'];
			}
		}
		
		
		
		### Get specfic target column if specified (e.g. Required and Optional Columns only for import header checking)
		$ReturnArr = array();
		if ($TargetPropertyArr != '')
		{
			if (!is_array($TargetPropertyArr))
				$TargetPropertyArr = array($TargetPropertyArr);
				
			$numOfColumn = count($ColumnArr['En']);
			$ColumnPropertyArr = $this->Get_Topic_Import_Export_Column_Property();
			for ($i=0; $i<$numOfColumn; $i++)
			{
				if (in_array($ColumnPropertyArr[$i], $TargetPropertyArr))
				{
					(array)$ReturnArr['En'][] = $ColumnArr['En'][$i];
					(array)$ReturnArr['Ch'][] = $ColumnArr['Ch'][$i];
				}
			}
		}
		else
			$ReturnArr = $ColumnArr;
		
		if ($ParLang != '')
			$ReturnArr = $ReturnArr[$ParLang];
		
		return $ReturnArr;
	}
	
	function Get_Export_Content_Arr($SubjectIDArr, $SubjectInfoArr, $TopicTreeArr, $TopicInfoArr, $FilterTopicArr, $IncludeNoTopicSubject=0)
	{
		$ExportDataArr = array();
		$numOfSubject = count($SubjectIDArr);
		
		$TopicIDArr = array_keys($TopicInfoArr);
		$TopicFormMappingAssoArr = $this->Get_Topic_Applicable_Form_Mapping($TopicIDArr);
		
		for ($i=0; $i<$numOfSubject; $i++)
		{
			$thisSubjectID = $SubjectIDArr[$i];
			$thisSubjectCode = $SubjectInfoArr[$thisSubjectID]['CODEID'];
			$thisSubjectNameEn = $SubjectInfoArr[$thisSubjectID]['EN_DES'];
			$thisSubjectNameCh = $SubjectInfoArr[$thisSubjectID]['CH_DES'];
			
			### Prepare the Subject info for export
			$TempRowArr = array();
			$TempRowArr[] = $thisSubjectCode;
			$TempRowArr[] = $thisSubjectNameEn;
			$TempRowArr[] = $thisSubjectNameCh;
				
			$thisSubjectTopicTree = $TopicTreeArr[$thisSubjectID];
			if ($IncludeNoTopicSubject && (!is_array($thisSubjectTopicTree) || count($thisSubjectTopicTree)==0))
			{
				### No Topic setup => Also output the Subject info for reference (csv sample for import)
				$ExportDataArr[] = $TempRowArr;
			}
			else
			{
				### Loop all Topics in the Subject			
				$ExportDataArr = $this->Get_Export_Topic_Content_Arr_Recursion($thisSubjectTopicTree, $TopicInfoArr, $TopicFormMappingAssoArr, $FilterTopicArr, $TempRowArr, $ExportDataArr);
			}
		}
		
		return $ExportDataArr;
	}
	
	function Get_Export_Topic_Content_Arr_Recursion($TopicTreeArr, $TopicInfoArr, $TopicFormMappingAssoArr, $FilterTopicArr, $TempRowArr='', $ExportDataArr='')
	{
		global $Lang;
		
		$TempRowArr = ($TempRowArr=='')? array() : $TempRowArr;
		$ExportDataArr = ($ExportDataArr=='')? array() : $ExportDataArr;
		$MaxLevel = $this->MaxTopicLevel;
		
		foreach ($TopicTreeArr as $thisTopicID => $thisLinkedTopicIDArr)
		{
			$thisTopicLevel					= $TopicInfoArr[$thisTopicID]['Level'];
			$thisTopicCode					= $TopicInfoArr[$thisTopicID]['TopicCode'];
			$thisTopicNameEn 				= $TopicInfoArr[$thisTopicID]['TopicNameEn'];
			$thisTopicNameCh 				= $TopicInfoArr[$thisTopicID]['TopicNameCh'];
			$thisTopicRemarks				= $TopicInfoArr[$thisTopicID]['Remarks'];
			$thisApplicableFormNameArr		= Get_Array_By_Key($TopicFormMappingAssoArr[$thisTopicID], 'YearName');
			$thisApplicableFormNameList		= implode($Lang['General']['ImportContentSeparator'], (array)$thisApplicableFormNameArr);
			
			if ($thisTopicLevel > $MaxLevel)
				continue; 
				
			### $FilterLevelTopicArr != '' means have filtering selection
			### is_array($FilterLevelTopicArr[$thisLevel]) means have filtering selection in this Topic Level
			### $FilterLevelTopicArr[$thisLevel][$thisTopicID] != 1 means the filtering is not selected this Topic
			if ($FilterTopicArr != '' && is_array($FilterTopicArr[$thisTopicLevel]) && $FilterTopicArr[$thisTopicLevel][$thisTopicID] != 1)
				continue;
				
			if (count($thisLinkedTopicIDArr) == 0)
			{
				$thisRowArr = $TempRowArr;
				$thisRowArr[] = $thisTopicCode;
				$thisRowArr[] = $thisTopicNameEn;
				$thisRowArr[] = $thisTopicNameCh;
				
				if ($thisTopicLevel == 1)
					$thisRowArr[] = $thisTopicRemarks;
					
				if ($thisTopicLevel == $MaxLevel)
					$thisRowArr[] = $thisApplicableFormNameList;
					
				$ExportDataArr[] = $thisRowArr;
			}
			else if (count($thisLinkedTopicIDArr) > 0)
			{
				$thisRowArr = $TempRowArr;
				$thisRowArr[] = $thisTopicCode;
				$thisRowArr[] = $thisTopicNameEn;
				$thisRowArr[] = $thisTopicNameCh;
				
				if ($thisTopicLevel == 1)
					$thisRowArr[] = $thisTopicRemarks;
					
				if ($thisTopicLevel == $MaxLevel)
					$thisRowArr[] = $thisApplicableFormNameList;
					
				$ExportDataArr = $this->Get_Export_Topic_Content_Arr_Recursion($thisLinkedTopicIDArr, $TopicInfoArr, $TopicFormMappingAssoArr, $FilterTopicArr, $thisRowArr, $ExportDataArr);
			}
		}
		
		return $ExportDataArr;
	}
	
	function Delete_Import_Temp_Data()
	{
		$TEMP_IMPORT_TOPIC = $this->Get_Table_Name('TEMP_IMPORT_TOPIC');
		$sql = "Delete From $TEMP_IMPORT_TOPIC Where ImportUserID = '".$_SESSION["UserID"]."'";
		$Success = $this->db_db_query($sql);
				
		return $Success;
	}
	
	function Save_Topic_Form_Mapping($TopicID, $YearIDArr)
	{
		$this->Start_Trans();
		$Success['DeletePreviousMapping'] = $this->Delete_Topic_Form_Mapping($TopicID);
		
		$YearIDArr = array_remove_empty((array)$YearIDArr);
		$numOfApplicableForm = count($YearIDArr);
		if ($numOfApplicableForm > 0)
		{
			$InsertArr = array();
			for ($i=0; $i<$numOfApplicableForm; $i++)
			{
				$thisYearID = $YearIDArr[$i];
				$InsertArr[] = " ('$TopicID', '$thisYearID', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."') ";
			}
			
			$RC_TOPIC_APPLICABLE_FORM_MAPPING = $this->Get_Table_Name('RC_TOPIC_APPLICABLE_FORM_MAPPING');
			$sql = "Insert Into	$RC_TOPIC_APPLICABLE_FORM_MAPPING
						(TopicID, YearID, DateInput, InputBy, DateModified, LastModifiedBy)
					Values
						".implode(',', (array)$InsertArr)."
					";
			$Success['InsertNewMapping'] = $this->db_db_query($sql);
		}
		
		if (in_array(false, $Success))
		{
			$this->RollBack_Trans();
			return 0;
		}
		else
		{
			$this->Commit_Trans();
			return 1;
		}
	}
	
	function Delete_Topic_Form_Mapping($TopicID)
	{
		$RC_TOPIC_APPLICABLE_FORM_MAPPING = $this->Get_Table_Name('RC_TOPIC_APPLICABLE_FORM_MAPPING');
		$sql = "Delete From $RC_TOPIC_APPLICABLE_FORM_MAPPING Where TopicID = '".$TopicID."'";
		
		return $this->db_db_query($sql);
	}
	
	
	function Get_TopicPathArr_by_LastTopicID($TopicIDArr)
	{
		// must get all topic and subject info for mapping
		$TopicInfoAssoArr = $this->Get_Topic_Info($SubjectIDArr='', '', $Level='', $TopicCode='', $PreviousLevelTopicID='', $ParentTopicID='', $ReturnAsso=1, $MaxLevel='', $YearIDArr='');		
		$SubjectInfoAssoArr = $this->Get_Subject_Info();
		
		$TopicFullInfoArr = array();
		for($a=0,$a_MAX=count($TopicIDArr);$a<$a_MAX;$a++) {	
			$thisTopicID = $TopicIDArr[$a];
			
			$thisSubjectID = $TopicInfoAssoArr[$thisTopicID]['SubjectID'];
			$thisLevel = $TopicInfoAssoArr[$thisTopicID]['Level'];
		    $thisPreviousLevelTopicID = $TopicInfoAssoArr[$thisTopicID]['PreviousLevelTopicID'];
			$thisParentTopicID = $TopicInfoAssoArr[$thisTopicID]['ParentTopicID'];
	
			$TopicFullInfoArr[$thisTopicID]['Subject']['SubjectID'] = $thisSubjectID;	
			$TopicFullInfoArr[$thisTopicID]['Subject']['SubjectCode'] = $SubjectInfoAssoArr[$thisSubjectID]['CODEID'];
			$TopicFullInfoArr[$thisTopicID]['Subject']['SubjectNameEn'] = $SubjectInfoAssoArr[$thisSubjectID]['EN_DES'];	 
			$TopicFullInfoArr[$thisTopicID]['Subject']['SubjectNameCh'] = $SubjectInfoAssoArr[$thisSubjectID]['CH_DES'];	
			$TopicFullInfoArr[$thisTopicID]['Subject']['SubjectName'] = Get_Lang_Selection($SubjectInfoAssoArr[$thisSubjectID]['CH_DES'], $SubjectInfoAssoArr[$thisSubjectID]['EN_DES']);
				
			$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicID'] = $thisTopicID;
			$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicCode'] = $TopicInfoAssoArr[$thisTopicID]['TopicCode'];
			$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicNameEn'] = $TopicInfoAssoArr[$thisTopicID]['TopicNameEn'];
			$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicNameCh'] = $TopicInfoAssoArr[$thisTopicID]['TopicNameCh'];
			$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicName'] = Get_Lang_Selection($TopicInfoAssoArr[$thisTopicID]['TopicNameCh'], $TopicInfoAssoArr[$thisTopicID]['TopicNameEn']);
			
			for ($i=0; $i<$this->MaxTopicLevel; $i++) {		// at most loop max topic level
				$thisLevel = $TopicInfoAssoArr[$thisPreviousLevelTopicID]['Level'];
       			$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicID'] = $thisPreviousLevelTopicID;
				$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicCode'] = $TopicInfoAssoArr[$thisPreviousLevelTopicID]['TopicCode'];	
				$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicNameEn'] = $TopicInfoAssoArr[$thisPreviousLevelTopicID]['TopicNameEn'];
				$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicNameCh'] = $TopicInfoAssoArr[$thisPreviousLevelTopicID]['TopicNameCh'];
				$TopicFullInfoArr[$thisTopicID]['Level'][$thisLevel]['TopicName'] = Get_Lang_Selection($TopicInfoAssoArr[$thisPreviousLevelTopicID]['TopicNameCh'], $TopicInfoAssoArr[$thisPreviousLevelTopicID]['TopicNameEn']);
				
				if($thisPreviousLevelTopicID == $thisParentTopicID) {
					break;
				}
				else {
					// set to next previous level topic id
					$thisPreviousLevelTopicID = $TopicInfoAssoArr[$thisPreviousLevelTopicID]['PreviousLevelTopicID'];
				}
			}
		}
		
		return $TopicFullInfoArr;
	}
	
	
	/***********************************************************************
	 * @owner : connie 20111201
	 * @para  : $TopicID (Last Level)
	 * @return: $LevelTopicIDArr
	 * 
	 * @desc  : if you want to get Name based on Lang , set $basedOnLang true. 
	 * 			if you want to get both names, set $basedOnLang false.
	 ***************************************************************************/
//	function Get_TopicPathArr_by_LastTopicID ($TopicID,$basedOnLang=false)
//	{
//		global $Lang, $eRC_Rubrics_ConfigArr, $PATH_WRT_ROOT;		
//		
//		$LevelTopicIDArr = array();
//		$TopicInfoArr = $this->Get_Topic_Info($SubjectIDArr='', $TopicID, $Level='', $TopicCode='', $PreviousLevelTopicID='', $ParentTopicID='', $ReturnAsso=1, $MaxLevel='', $YearIDArr='');
//		$TopicInfoArr = current($TopicInfoArr);
//		$SubjectID = $TopicInfoArr['SubjectID'];
//		$thisLevel = $TopicInfoArr['Level'];
//	    $PreviousLevelTopicID = $TopicInfoArr['PreviousLevelTopicID'];
//		$ParentTopicID = $TopicInfoArr['ParentTopicID'];
//		
//		$SubjectTitleArr = $this->Get_Subject_Info($SubjectID);
//		$SubjectTitleArr = current($SubjectTitleArr);
//		
//		
//		$LevelTopicIDArr[$TopicID]['Subject']['SubjectID'] = $SubjectID;	
//		$LevelTopicIDArr[$TopicID]['Subject']['SubjectCode'] = $SubjectTitleArr['CODEID'];
//		$LevelTopicIDArr[$TopicID]['Level'][$thisLevel]['TopicID'] = $TopicID;
//		
//		if($basedOnLang) {
//			$thisName = Get_Lang_Selection($SubjectTitleArr['CH_DES'], $SubjectTitleArr['EN_DES']);
//			$LevelTopicIDArr[$TopicID]['Subject']['SubjectName'] = $thisName;
//			
//			$thisName = Get_Lang_Selection($TopicInfoArr['TopicNameCh'], $TopicInfoArr['TopicNameEn']);
//			$LevelTopicIDArr[$TopicID]['Level'][$thisLevel]['TopicName'] = $thisName;			
//		}
//		else {
//			$LevelTopicIDArr[$TopicID]['Subject']['SubjectNameEn'] = $SubjectTitleArr['EN_DES'];	 
//			$LevelTopicIDArr[$TopicID]['Subject']['SubjectNameCh'] = $SubjectTitleArr['CH_DES'];	
//			
//			$LevelTopicIDArr[$TopicID]['Level'][$thisLevel]['TopicNameEn'] = $TopicInfoArr['TopicNameEn'];
//			$LevelTopicIDArr[$TopicID]['Level'][$thisLevel]['TopicNameCh'] = $TopicInfoArr['TopicNameCh'];			
//		} 
//		$LevelTopicIDArr[$TopicID]['Level'][$thisLevel]['TopicCode'] = $TopicInfoArr['TopicCode'];
//
//
//		$this_topicID = $TopicID;
//		//for ($i=1; $i<100; $i++)
//		for ($i=1; $i<=$this->MaxTopicLevel; $i++)
//		{	
//			if($this_topicID==$ParentTopicID) {
//				break;
//			}
//			else {
//				$TopicInfoArr = $this->Get_Topic_Info($SubjectIDArr='', $PreviousLevelTopicID, $Level='', $TopicCode='', $PreviousLevelTopicID='', $ParentTopicID='', $ReturnAsso=1, $MaxLevel='', $YearIDArr='');
//				$TopicInfoArr = current($TopicInfoArr);
//				$PreviousLevelTopicID = $TopicInfoArr['PreviousLevelTopicID'];
//				$ParentTopicID = $TopicInfoArr['ParentTopicID'];
//				$this_topicID = $TopicInfoArr['TopicID'];
//				$thisLevel = $TopicInfoArr['Level'];
//				        
//				$LevelTopicIDArr[$TopicID]['Level'][$thisLevel]['TopicID'] = $this_topicID;
//					
//				if($basedOnLang)
//				{
//					$thisName = Get_Lang_Selection($TopicInfoArr['TopicNameCh'], $TopicInfoArr['TopicNameEn']);
//					$LevelTopicIDArr[$TopicID]['Level'][$thisLevel]['TopicName'] = $thisName;			
//				}  			
//				else
//				{
//					$LevelTopicIDArr[$TopicID]['Level'][$thisLevel]['TopicNameEn'] = $TopicInfoArr['TopicNameEn'];
//					$LevelTopicIDArr[$TopicID]['Level'][$thisLevel]['TopicNameCh'] = $TopicInfoArr['TopicNameCh'];			
//				} 
//				$LevelTopicIDArr['Level'][$thisLevel]['TopicCode'] = $TopicInfoArr['TopicCode'];
//			}
//		}
//		
//		if (!is_array($TopicID)) {
//			$LevelTopicIDArr = $LevelTopicIDArr[$TopicID];
//		}
//		
//		return $LevelTopicIDArr;
//	}
//	
	public function Get_TopicID_By_Filtering_Text($FilterText, $SubjectIDArr='', $IncludeTopicIDArr='', $FilterLevelSeparator='', $FilterInfoSeparator='') {
		if ($SubjectIDArr != '' && !is_array($SubjectIDArr)) {
			$SubjectIDArr = array($SubjectIDArr);
		}
		
		$FilterLevelSeparator = ($FilterLevelSeparator=='')? '::' : $FilterLevelSeparator;
		$FilterInfoSeparator = ($FilterInfoSeparator=='')? ',' : $FilterInfoSeparator;
		
		$IncludeTopicInfoArr = $this->Get_Topic_Info($SubjectIDArr, $IncludeTopicIDArr, $this->MaxTopicLevel, $TopicCode='', $PreviousLevelTopicID='', $ParentTopicID='', $ReturnAsso=0);
		$IncludeTopicIDArr = Get_Array_By_Key($IncludeTopicInfoArr, 'TopicID');
		
		$TopicTreeInfoArr = $this->Get_TopicPathArr_by_LastTopicID($IncludeTopicIDArr);
		
		// Build Filtering Asso Array
		$FilterTextArr = explode($FilterLevelSeparator, $FilterText);
		$numOfFilterText = count($FilterTextArr);
		$FilterAssoArr = array();
		for($a=0,$a_MAX=$numOfFilterText; $a<$a_MAX; $a++) {
			$FilterTextArrPieces = explode($FilterInfoSeparator, $FilterTextArr[$a]);
			
			$thisFilterLevel = $FilterTextArrPieces[0];
			$thisFilterTopicID = $FilterTextArrPieces[1];
			
			$FilterAssoArr[$thisFilterLevel] = $thisFilterTopicID;
		}
		
		$returnTopicIdArr = array();
		for($i=0,$i_MAX=count($IncludeTopicIDArr); $i<$i_MAX; $i++) {
			$_topicId = $IncludeTopicInfoArr[$i]['TopicID'];
			$_subjectID = $IncludeTopicInfoArr[$i]['SubjectID'];		
			
			if (is_array($SubjectIDArr) && !in_array($_subjectID, $SubjectIDArr)) {
				continue;
			}
			
			if ($FilterText == '') {
				// no filtering
				$returnTopicIdArr[] = $_topicId;
			}
			else {
				// check filtering
				$_valid = true;						
				foreach((array)$FilterAssoArr as $__filterLevel => $__filterTopicID) {
					$__topicFilterLevelTopicId = $TopicTreeInfoArr[$_topicId]['Level'][$__filterLevel]['TopicID'];
					
					if($__topicFilterLevelTopicId == $__filterTopicID) {
						$_valid = true;
					}
					else {
						$_valid = false;
						break;
					}
				}
				
				if ($_valid) {
					$returnTopicIdArr[] = $_topicId;
				}
			}
		}
		
		return $returnTopicIdArr;
	}
}

?>