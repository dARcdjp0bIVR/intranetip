<?php
/*
 *  2018-07-06 Cameron
 *      - add $enrolConfigAry['CurriculumTemplate']*
 */
$enrolConfigAry = array();

$enrolConfigAry['Club'] = 'Club';
$enrolConfigAry['Activity'] = 'Activity';

$enrolConfigAry['ajaxDataSeparator'] = '<!--s-->';
$enrolConfigAry['AttendanceImportCode']['Present'] = 'Present';
$enrolConfigAry['AttendanceImportCode']['Absent'] = 'Absent';
$enrolConfigAry['AttendanceImportCode']['Exempt'] = 'Exempt';

$enrolConfigAry['encryptionKey'] = 'eEnrolKey_!S^g4b3'.$_SESSION['UserID'];

$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['TitleLang']['English'] = 'E';
$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['TitleLang']['Chinese'] = 'C';
$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['IntExt']['Internal'] = 'INT';
$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['IntExt']['External'] = 'EXT';

$enrolConfigAry['EnrolmentRecordStatus_Waiting'] = 0;
$enrolConfigAry['EnrolmentRecordStatus_Rejected'] = 1;
$enrolConfigAry['EnrolmentRecordStatus_Approved'] = 2;

$enrolConfigAry['MeetingDateType']['Single'] = 'single';
$enrolConfigAry['MeetingDateType']['Periodic'] = 'periodic';
$enrolConfigAry['MeetingDate_RecordStatus']['Active'] = 1;
$enrolConfigAry['MeetingDate_RecordStatus']['Deleted'] = 0;
$enrolConfigAry['MeetingDateNewRowNumTempCode'] = 'tempNewRowNum';
$enrolConfigAry['MeetingDateNewRowNumTempDateStart'] = 'xxxx-xx-xx';
$enrolConfigAry['MeetingDateNewRowNumTempDateEnd'] = 'yyyy-yy-yy';

$enrolConfigAry['Settings_PersonType']['Student'] = 0;
$enrolConfigAry['Settings_PersonType']['Parent'] = 1;

$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'] = 'P';
$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement'] = 'A';
$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment'] = 'C';

$enrolConfigAry['CurriculumTemplate']['CourseLength'] = 189;    // in days
$enrolConfigAry['CurriculumTemplate']['DaysPerWeek'] = 7;
$enrolConfigAry['CurriculumTemplate']['MaxNumberOfClassStudent'] = 99;
$enrolConfigAry['CurriculumTemplate']['MaxNumberOfLesson'] = 30;
$enrolConfigAry['CurriculumTemplate']['MaxNumberOfWeek'] = 52;
$enrolConfigAry['Setting']['TimeLimit'] = 'TimeLimitSetting';
?>