<?php
# using: 

########### Change Log #######
#
#   Date:   2018-11-12  Philips     [2018-1105-1041-02206]
#           modified returnAnswers() for sorting by AnswerID
#
#	Date:	2017-11-02	Pun
#			modified GET_MODULE_OBJ_ARR added L&T cust case
#
#	Date:	2017-05-24	Pun
#			modified GET_MODULE_OBJ_ARR added toSDAS case
#
#	Date:	2017-05-08	Frankie	[2017-0410-1438-31236]
#			added isSurveyFilledDraft() and Modified isSurveyFilledDraft for "Save as Draft"
#
#	Date:	2017-02-23 	Anna	[2016-0928-1637-22240]
#			added returnSurveyForMapping(), added checkDateFormat()
#
#	Date:	2016-05-04	Bill	[2016-0428-1355-16207]
#			added returnSurveyTemplates() to return SurveyID and Title of templates
#
#	Date:	2015-12-16	Pun
#			modified function splitQuestion() to add number of answer for ordering
#
#	Date:	2015-03-24	Siuwan
#			modified function splitQuestion() to add ordering
#
#	Date:	2014-04-02	YatWoon
#			add returnTargetGroupIDs()
#
#	Date:	2013-04-19	Yuen
#			add returnEmailNotificationData() to support notification by email
#
#	Date:	2012-09-05	YatWoon
#			update returnSurvey_IP25(), add GroupID checking (not null) to prevnet abnormal GroupID=NULL data
#
#	Date:	2011-06-02	YatWoon
#			add option allow re-sign the survey or not ($lsurvey->NotAllowReSign)
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#
#	Date: 2010-09-13 YatWoon
#			update parseAnswerStr(), add ";" to separate the answers
#
##############################

if (!defined("LIBSURVEY_DEFINED"))         // Preprocessor directives
{

 define("LIBSURVEY_DEFINED",true);

 class libsurvey extends libdb{
       var $SurveyID;
       var $Title;
       var $Description;
       var $Question;
       var $DateStart;
       var $DateEnd;
       var $OwnerGroupID;
       var $PosterID;
       var $PosterName;
       var $Internal;
       var $RecordType;
       var $RecordStatus;
       var $AllowSaveDraft;
       var $question_array;
       var $AllFieldsReq;
       var $DisplayQuestionNumber;

       var $MaxReplySlipOption;
       var $NotAllowReSign;
       var $DefaultAllowSaveDraft;
       var $QuestionType;
       
       function libsurvey($sid="")
       {
                $this->libdb();
                $this->Module = "eSurvey";
                
                if (!isset($_SESSION["SSV_PRIVILEGE"]["eSurvey"]))
             	{
					include_once("libgeneralsettings.php");
					$lgeneralsettings = new libgeneralsettings();
					
					$settings_ary = $lgeneralsettings->Get_General_Setting($this->Module);
					if(sizeof($settings_ary))
					{
						foreach($settings_ary as $key=>$data)
						{
							$_SESSION["SSV_PRIVILEGE"]["eSurvey"][$key] = $data;
							$this->$key = $data; 
						}
					}
					else
					{
						$this->MaxReplySlipOption = 50;
	                 	$this->NotAllowReSign = true;
	                 	$this->DefaultAllowSaveDraft= false;
					}
				}
				else
				{
					$this->MaxReplySlipOption = $_SESSION["SSV_PRIVILEGE"]["eSurvey"]["MaxReplySlipOption"];
					$this->NotAllowReSign = $_SESSION["SSV_PRIVILEGE"]["eSurvey"]["NotAllowReSign"];
					$this->DefaultAllowSaveDraft = $_SESSION["SSV_PRIVILEGE"]["eSurvey"]["DefaultAllowSaveDraft"];
                 }
                
                if ($sid != "")
                {
                    $this->retrieveRecord($sid);
                }
       }

	function retrieveRecord($sid)
	{
		global $sys_custom;
		if ($sys_custom['SurveySaveWithDraft']) {
			$sql = "SELECT Title, Description, Question, AllowSaveDraft, DATE_FORMAT(DateStart,'%Y-%m-%d'), DATE_FORMAT(DateEnd,'%Y-%m-%d'), OwnerGroupID,
						PosterID, PosterName, Internal, RecordType, RecordStatus, AllFieldsReq, DisplayQuestionNumber, QuestionType
						FROM INTRANET_SURVEY WHERE SurveyID = '$sid'";
		} else {
			$sql = "SELECT Title, Description, Question, DATE_FORMAT(DateStart,'%Y-%m-%d'), DATE_FORMAT(DateEnd,'%Y-%m-%d'), OwnerGroupID,
						PosterID, PosterName, Internal, RecordType, RecordStatus, AllFieldsReq, DisplayQuestionNumber, QuestionType
						FROM INTRANET_SURVEY WHERE SurveyID = '$sid'";
		}
		$result = $this->returnArray($sql,12);
		$this->SurveyID = $sid;
		if ($sys_custom['SurveySaveWithDraft']) {
			list ($this->Title, $this->Description, $this->Question, $this->AllowSaveDraft,
					$this->DateStart, $this->DateEnd,$this->OwnerGroupID,
					$this->PosterID, $this->PosterName,$this->Internal,$this->RecordType,
					$this->RecordStatus, $this->AllFieldsReq, $this->DisplayQuestionNumber, $this->QuestionType
					) = $result[0];
		} else {
			list ($this->Title, $this->Description, $this->Question,
					$this->DateStart, $this->DateEnd,$this->OwnerGroupID,
					$this->PosterID, $this->PosterName,$this->Internal,$this->RecordType,
					$this->RecordStatus, $this->AllFieldsReq, $this->DisplayQuestionNumber, $this->QuestionType
					) = $result[0];
		}
	}

		function returnSurveyTemplates()
		{
                $sql = "SELECT SurveyID, Title FROM INTRANET_SURVEY WHERE RecordStatus = 3";
                return $this->returnArray($sql,2);
		}

       function returnTemplates()
       {
                $sql = "SELECT Title, Question FROM INTRANET_SURVEY WHERE RecordStatus = 3";
                return $this->returnArray($sql,2);
       }
       
       function returnSurveyForMapping()
       {
       			$sql = "SELECT SurveyID, Title, DateStart, DateEnd FROM INTRANET_SURVEY WHERE RecordStatus != 3";
       			return $this->returnArray($sql,2);
       }
       
       function getTemplatesInJS()
       {
                $result = $this->returnTemplates();
                $x = "var form_templates = new Array();\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($name,$que) = $result[$i];
                     $name = str_replace("\n",'<br>',$name);
                     $name = str_replace("\r",'',$name);
                     $que = str_replace("\n",'<br>',$que);
                     $que = str_replace("\r",'',$que);

                     $x .= "form_templates[$i] = new Array(\"$name\",\"$que\");";
                }
                return $x;
       }
       function isSurveyEditable()
       {
                return !$this->isSurveyFilled();
       }
       
	function isSurveyFilled()
	{
		global $sys_custom;
		if ($sys_custom['SurveySaveWithDraft']) {
			$sql = "SELECT COUNT(AnswerID) FROM INTRANET_SURVEYANSWER WHERE SurveyID = '".$this->SurveyID."' AND isDraft='0'";
		} else {
			$sql = "SELECT COUNT(AnswerID) FROM INTRANET_SURVEYANSWER WHERE SurveyID = '".$this->SurveyID."'";
		}
		$result = $this->returnVector($sql);
		return ($result[0]!=0);
	}
	
	function isSurveyFilledDraft()
	{
		$sql = "SELECT COUNT(AnswerID) FROM INTRANET_SURVEYANSWER WHERE SurveyID = '".$this->SurveyID."' AND isDraft='1'";
		$result = $this->returnVector($sql);
		return ($result[0]!=0);
	}
	
       function returnSurvey($returnSizeOnly = 0)
       {
                global $UserID;
                
                $date_conds = " AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()";
                $sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$UserID'";
                $groups = $this->returnVector($sql);
                if (sizeof($groups)!=0) $group_list = implode(",",$groups);
                $sql = "SELECT a.SurveyID FROM INTRANET_SURVEY as a
                        LEFT OUTER JOIN INTRANET_GROUPSURVEY as b ON a.SurveyID = b.SurveyID
                        WHERE b.SurveyID IS NULL AND a.RecordStatus = 1 $date_conds";
                $idschool = $this->returnVector($sql);
                if ($group_list != "")
                {
                    $sql = "SELECT 
                    			DISTINCT a.SurveyID 
		                    FROM 
		                    	INTRANET_SURVEY as a, 
		                    	INTRANET_GROUPSURVEY as b,
		                    	INTRANET_SURVEYANSWER as c
                            WHERE 
                            	a.SurveyID = b.SurveyID AND 
                            	b.GroupID IN ($group_list) AND 
                            	a.RecordStatus = 1  
                            	$date_conds
                            ";
					// AND c.SurveyID=a.SurveyID		
                    $idgroup = $this->returnVector($sql);
                    $overall = array_merge($idschool,$idgroup);
                }
                else
                {
                    $overall = $idschool;
                }
                if (sizeof($overall)==0) 
                {
	                if($returnSizeOnly)
	                	return 0;
	                else
                		return array();
            	}
                /*
                if($returnSizeOnly)
                {
	             	return sizeof($overall);
                }
                */
                
                $list = implode(",",$overall);
                $namefield = getNameFieldWithClassNumberByLang("c.");
                $sql = "SELECT a.SurveyID, a.Title,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),
                        IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$namefield),
                        d.Title, a.RecordType FROM INTRANET_SURVEY as a
                        LEFT OUTER JOIN INTRANET_SURVEYANSWER as b ON a.SurveyID = b.SurveyID AND b.UserID = '$UserID'
                        LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.PosterID
                        LEFT OUTER JOIN INTRANET_GROUP as d ON d.GroupID = a.OwnerGroupID
                        WHERE a.SurveyID IN ($list) AND b.SurveyID IS NULL
                        ORDER BY a.DateEnd";
                $result = $this->returnArray($sql,7);
                if($returnSizeOnly)
	             	return sizeof($result);
	             else
	             	return $result;
       }
        function returnPosterName()
        {
                 global $i_general_sysadmin;
                 if ($this->PosterID == "") return $i_general_sysadmin;
                 $name_field = getNameFieldWithClassNumberByLang();
                 $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '".$this->PosterID."'";
                 $result = $this->returnVector($sql);
                 return ($result[0]==""? $this->PosterName : $result[0]);
        }
        function returnOwnerGroup()
        {
                 if ($this->OwnerGroupID == "") return "";
                 $sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID = '".$this->OwnerGroupID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        function returnTargetGroups()
        {
                 if ($this->SurveyID == "") return array();
                 $sql = "SELECT DISTINCT a.Title
                         FROM INTRANET_GROUPSURVEY as b
                              LEFT OUTER JOIN INTRANET_GROUP as a ON b.GroupID = a.GroupID
                         WHERE SurveyID = '".$this->SurveyID."'";
                 return $this->returnVector($sql);
        }
        function returnTargetSubjectGroups()
        {
                 global $intranet_session_language, $sys_custom;
                 if ($this->SurveyID == "" || $sys_custom['iPf']['learnAndTeachReport']==false) return array();
                 $titleSql = ($intranet_session_language == 'en')?"a.ClassTitleEN":"a.ClassTitleB5";
                 $sql = "SELECT DISTINCT $titleSql as Title
                         FROM INTRANET_SUBJECTGROUPSURVEY as b
                              LEFT OUTER JOIN SUBJECT_TERM_CLASS as a ON b.SubjectGroupID = a.SubjectGroupID
                         WHERE SurveyID = '".$this->SurveyID."'";
                 return $this->returnVector($sql);
        }
       
        function returnTargetGroupIDs()
        {
                 if ($this->SurveyID == "") return array();
                 $sql = "SELECT DISTINCT a.GroupID
                         FROM INTRANET_GROUPSURVEY as b
                              LEFT OUTER JOIN INTRANET_GROUP as a ON b.GroupID = a.GroupID
                         WHERE SurveyID = '".$this->SurveyID."'";
                 return $this->returnVector($sql);
        }
        function returnTargetGroupsArray()
        {
                 if ($this->SurveyID == "") return array();
                 $sql = "SELECT DISTINCT a.GroupID, a.Title
                         FROM INTRANET_GROUPSURVEY as b
                              LEFT OUTER JOIN INTRANET_GROUP as a ON b.GroupID = a.GroupID
                         WHERE SurveyID = '".$this->SurveyID."'";
                 return $this->returnArray($sql,2);
        }
        function returnTargetSubjectGroupsArray()
        {
            global $intranet_session_language, $sys_custom;    
            if ($this->SurveyID == "" || $sys_custom['iPf']['learnAndTeachReport']==false) return array();
            $titleSql = ($intranet_session_language == 'en')?"a.ClassTitleEN":"a.ClassTitleB5";
            $sql = "SELECT DISTINCT a.SubjectGroupID, {$titleSql} as Title
                    FROM INTRANET_SUBJECTGROUPSURVEY as b
                    LEFT OUTER JOIN SUBJECT_TERM_CLASS as a ON b.SubjectGroupID = a.SubjectGroupID
                    WHERE SurveyID = '".$this->SurveyID."'";
            return $this->returnArray($sql,2);
        }
		function returnTargetClassList()
		{
        	global $sys_custom;
			if ($this->SurveyID == "") return array();
			
			if ($sys_custom['SurveySaveWithDraft']) {
               	$sql = "SELECT DISTINCT b.ClassName FROM INTRANET_SURVEYANSWER as a
                       	LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                       	WHERE b.ClassName <> '' AND SurveyID = '".$this->SurveyID."' AND isDraft != '1'";
			} else {
				$sql = "SELECT DISTINCT b.ClassName FROM INTRANET_SURVEYANSWER as a
                       	LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                       	WHERE b.ClassName <> '' AND SurveyID = '".$this->SurveyID."'";
			}
			return $this->returnVector($sql);

        }
       function displaySurvey()
       {
                global $i_general_startdate,$i_general_enddate,$i_general_title,$i_Survey_Poster,$i_general_sysadmin;
                global $i_Survey_Anonymous;
                $survey = $this->returnSurvey();

                $x = "<table width=\"570\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\" bordercolorlight=\"#8CC7C9\" bordercolordark=\"#FFFFFF\" class=\"body\">\n";
                $x .= "<tr bgcolor=\"#FEF9DD\">
                <td width=\"100\"><strong>$i_general_startdate</strong></td>
                <td width=\"100\"><strong>$i_general_enddate</strong></td>
                <td><strong>$i_general_title</strong></td>
                <td width=\"100\"><strong>$i_Survey_Poster</strong></td>
              </tr>\n";
                if (sizeof($survey)==0)
                {
                    global $i_Survey_NoSurveyAvailable;
                    return "$x<tr><td colspan=4>$i_Survey_NoSurveyAvailable</td></tr></table>\n";
                }
                for ($i=0; $i<sizeof($survey); $i++)
                {
                     list ($id,$title,$start,$end,$name,$group,$recordType) = $survey[$i];
                     $poster = ($name!="" || $group!= "")? "$name <br> $group":"$i_general_sysadmin";
                     if ($recordType == 1)
                         $anonymousStr = "<br><b>[$i_Survey_Anonymous]</b>";
                     else $anonymousStr = "";
                     $x .= "<tr>
                              <td>$start</td>
                              <td>$end</td>
                              <td><a class=functionlink href=javascript:openSurvey($id)>$title</a>$anonymousStr</td>
                              <td>$poster</td>
                            </tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       function displaySurvey2()
       {
                global $i_general_startdate,$i_general_enddate,$i_general_title,$i_Survey_Poster,$i_general_sysadmin;
                global $i_Survey_Anonymous;
                $survey = $this->returnSurvey();

                $x = "<table width=\"570\" border=\"1\" cellpadding=\"5\" cellspacing=\"0\" bgcolor=#ffffff class='13-black' bordercolordark=#ffffff bordercolorlight=#5DA5C9>\n";
                $x .= "<tr bgcolor=\"#E3DB9C\">
                <td class='13-black-bold' width=\"100\"><strong>$i_general_startdate</strong></td>
                <td class='13-black-bold' width=\"100\"><strong>$i_general_enddate</strong></td>
                <td class='13-black-bold' ><strong>$i_general_title</strong></td>
                <td class='13-black-bold' width=\"100\"><strong>$i_Survey_Poster</strong></td>
              </tr>\n";
                if (sizeof($survey)==0)
                {
                    global $i_Survey_NoSurveyAvailable;
                    return "$x<tr><td colspan=4>$i_Survey_NoSurveyAvailable</td></tr></table>\n";
                }
                for ($i=0; $i<sizeof($survey); $i++)
                {
                     list ($id,$title,$start,$end,$name,$group,$recordType) = $survey[$i];
                     $poster = ($name!="" || $group!= "")? "$name <br> $group":"$i_general_sysadmin";
                     if ($recordType == 1)
                         $anonymousStr = "<br><b>[$i_Survey_Anonymous]</b>";
                     else $anonymousStr = "";
                     $x .= "<tr>
                              <td>$start</td>
                              <td>$end</td>
                              <td><a class=functionlink href=javascript:openSurvey($id)>$title</a>$anonymousStr</td>
                              <td>$poster</td>
                            </tr>\n";
                }
                $x .= "</table>\n";
                return $x;
       }
       
    	function returnAnswer()
    	{
    		global $sys_custom;
    		if ($sys_custom['SurveySaveWithDraft']) {
    		    $sql = "SELECT a.Answer FROM INTRANET_SURVEYANSWER as a
    	        			,INTRANET_USER as b WHERE a.UserID = b.UserID AND a.SurveyID = '".$this->SurveyID .
    						"' AND a.Answer IS NOT NULL AND a.Answer != '' AND isDraft != '1' ORDER BY b.ClassName, b.ClassNumber";
    		} else {
    		    $sql = "SELECT a.Answer FROM INTRANET_SURVEYANSWER as a
    	        			,INTRANET_USER as b WHERE a.UserID = b.UserID AND a.SurveyID = '".$this->SurveyID .
    	        			"' AND a.Answer IS NOT NULL AND a.Answer != '' ORDER BY b.ClassName, b.ClassNumber";
    		}
    		$result = $this->returnArray($sql,1);
    		return $this->returnAnswerInJS($result);
    	}
	
       function returnAnswerInJS($result)
       {
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list ($answer) = $result[$i];
                     $answer = trim($answer);
                     if ($answer == "") continue;
                     $answer = str_replace('"','&quot;',$answer);
                     $answer = str_replace("\n",'<br>',$answer);
                     $answer = str_replace("\r",'',$answer);
                     $answer = str_replace("\\",'&#92;',$answer);
                     $x .= "myAns[myAns.length] = \"$answer\";\n";
                }
                return $x;
       }
       function returnAnswerForGroup($GroupID)
       {
       		global $sys_custom;
       		if ($sys_custom['SurveySaveWithDraft']) {
                $sql = "SELECT a.Answer FROM INTRANET_SURVEYANSWER as a
                               LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                               LEFT OUTER JOIN INTRANET_USERGROUP as c ON b.UserID = c.UserID
                               WHERE c.GroupID = '$GroupID' AND
                               b.UserID IS NOT NULL AND
                               a.SurveyID = '".$this->SurveyID .
                               "' AND a.isDraft != '1' ORDER BY b.ClassName, b.ClassNumber";
       		} else {
       			$sql = "SELECT a.Answer FROM INTRANET_SURVEYANSWER as a
       			LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
       			LEFT OUTER JOIN INTRANET_USERGROUP as c ON b.UserID = c.UserID
       			WHERE c.GroupID = '$GroupID' AND
       			b.UserID IS NOT NULL AND
       			a.SurveyID = '".$this->SurveyID .
       			"' ORDER BY b.ClassName, b.ClassNumber";
       		}
			$result = $this->returnArray($sql,1);
			return $this->returnAnswerInJS($result);
       }
       function returnAnswerForUserType($type)
       {
			global $sys_custom;
       		if ($sys_custom['SurveySaveWithDraft']) {
       			$sql = "SELECT a.Answer FROM INTRANET_SURVEYANSWER as a
							,INTRANET_USER as b WHERE a.UserID = b.UserID AND
							b.RecordType = '$type' AND
							a.SurveyID = '".$this->SurveyID .
							"' AND a.isDraft != '1' ORDER BY b.ClassName, b.ClassNumber";
       		} else {
       			$sql = "SELECT a.Answer FROM INTRANET_SURVEYANSWER as a
       						,INTRANET_USER as b WHERE a.UserID = b.UserID AND
       						b.RecordType = '$type' AND
       						a.SurveyID = '".$this->SurveyID .
       						"' ORDER BY b.ClassName, b.ClassNumber";
       		}
			$result = $this->returnArray($sql,1);
			return $this->returnAnswerInJS($result);
       }

       function returnAnswerForClass($ClassName)
       {
       		global $sys_custom;
       		if ($sys_custom['SurveySaveWithDraft']) {
                $sql = "SELECT a.Answer FROM INTRANET_SURVEYANSWER as a
                               ,INTRANET_USER as b WHERE a.UserID = b.UserID AND
                               b.ClassName = '$ClassName' AND
                               a.SurveyID = '".$this->SurveyID .
                               "' AND a.isDraft != '1' ORDER BY b.ClassName, b.ClassNumber";
       		} else {
       			$sql = "SELECT a.Answer FROM INTRANET_SURVEYANSWER as a
       						,INTRANET_USER as b WHERE a.UserID = b.UserID AND
       						b.ClassName = '$ClassName' AND
       						a.SurveyID = '".$this->SurveyID .
       						"' ORDER BY b.ClassName, b.ClassNumber";
       		}
			$result = $this->returnArray($sql,1);
			return $this->returnAnswerInJS($result);
       }
       function returnUserList()
       {
       		global $sys_custom;
			$name = getNameFieldWithClassNumberByLang("b.");
			if ($sys_custom['SurveySaveWithDraft']) {
                $sql = "SELECT a.UserID,
                        IF(b.UserID IS NULL,CONCAT('<I>',a.UserName,'</I>'),$name)
                        FROM INTRANET_SURVEYANSWER as a
                        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        WHERE a.SurveyID = '".$this->SurveyID."' AND a.isDraft != '1'";
			} else {
				$sql = "SELECT a.UserID,
				IF(b.UserID IS NULL,CONCAT('<I>',a.UserName,'</I>'),$name)
				FROM INTRANET_SURVEYANSWER as a
				LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
				WHERE a.SurveyID = '".$this->SurveyID."'";
			}
			return $this->returnArray($sql,2);
       }

       function returnUserListByGroup($GroupID)
       {
       		global $sys_custom;
            $name = getNameFieldWithClassNumberByLang("b.");
                
			if ($sys_custom['SurveySaveWithDraft']) {
                $sql = "SELECT b.UserID, $name FROM INTRANET_SURVEYANSWER as a
                    	    LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        	LEFT OUTER JOIN INTRANET_USERGROUP as c ON b.UserID = c.UserID AND GroupID = $GroupID
                        	WHERE b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND a.SurveyID = '".$this->SurveyID."' AND a.isDraft != '1'";
			} else {
               	$sql = "SELECT b.UserID, $name FROM INTRANET_SURVEYANSWER as a
		                	LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
		                	LEFT OUTER JOIN INTRANET_USERGROUP as c ON b.UserID = c.UserID AND GroupID = $GroupID
		                	WHERE b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND a.SurveyID = '".$this->SurveyID."'";
			}
			return $this->returnArray($sql,2);
       }
       function returnUserListByUserType($usertype)
       {
                $name = getNameFieldWithClassNumberByLang("b.");
                global $sys_custom;
                if ($sys_custom['SurveySaveWithDraft']) {
                	$sql = "SELECT b.UserID, $name FROM INTRANET_SURVEYANSWER as a
                        		LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        		WHERE b.UserID IS NOT NULL AND b.RecordType = $usertype AND a.SurveyID = '".$this->SurveyID."' AND a.isDraft != '1'";
                } else {
                	$sql = "SELECT b.UserID, $name FROM INTRANET_SURVEYANSWER as a
                				LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                				WHERE b.UserID IS NOT NULL AND b.RecordType = $usertype AND a.SurveyID = '".$this->SurveyID."'";
                }
                return $this->returnArray($sql,2);
       }
       function returnUserListByClassName($ClassName)
       {
       		global $sys_custom;
       		$name = getNameFieldWithClassNumberByLang("b.");
       		if ($sys_custom['SurveySaveWithDraft']) {
                $sql = "SELECT b.UserID, $name FROM INTRANET_SURVEYANSWER as a
                        LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
                        WHERE b.UserID IS NOT NULL AND b.ClassName = '$ClassName' AND a.SurveyID = '".$this->SurveyID."' AND a.isDraft != '1'";
       		} else {
       			$sql = "SELECT b.UserID, $name FROM INTRANET_SURVEYANSWER as a
       					LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
       					WHERE b.UserID IS NOT NULL AND b.ClassName = '$ClassName' AND a.SurveyID = '".$this->SurveyID."'";
       		}
			return $this->returnArray($sql,2);
       }
       
       function returnLTSurveyTeacher($SurveyID){
       		$sql = "SELECT TeacherID FROM INTRANET_SUBJECTGROUPSURVEY WHERE SurveyID={$SurveyID}";
       		return current($this->returnVector($sql));
       }
       
        function splitQuestion($qStr)
        {
                 $qSeparator = "#QUE#";
                 $pSeparator = "||";
                 $oSeparator = "#OPT#";
                 $questions = explode($qSeparator,$qStr);
                 for ($i=1; $i<sizeof($questions); $i++)
                 {
                      $que = $questions[$i];
                      $parts = explode($pSeparator,$que);
                      $temp = explode(",",$parts[0]);
                      $type = $temp[0];
                      $numOp = 0;
                      $options = "";
                      switch ($type)
                      {
                              case 1:
                                   $numOp = 2;
                                   $opStr = $parts[2];
                                   $options = explode($oSeparator,$opStr);
                                   array_shift($options);
                                   break;
                              case 2:
                              case 3:
                              case 7:
                                   $numOp = $temp[1];
                                   $opStr = $parts[2];
                                   $options = explode($oSeparator,$opStr);
                                   array_shift($options);
                                   break;
                              case 4:
                              case 5:
                              case 6:
                                   $options = array();
                      }
                      $mainQ = $parts[1];
                      $arr = array($type, $mainQ, $options);
                      
                      #### Add number of answer to the question_array START ####
                      if($type == 7){ // For ording type
                      	$arr[] = ($temp[2])?$temp[2]:$temp[1];
                      }
                      #### Add number of answer to the question_array END ####
                      
                      $result[] = $arr;
                 }
                 $this->question_array = $result;
                 return $result;
        }
        function parseAnswerStr ($aStr)
        {
                 if ($aStr == "") return array();
                 $aSeparator = "#ANS#";
                 $answers = explode($aSeparator,$aStr);
                 for ($i=1; $i<sizeof($answers); $i++)
                 {
                      $qType = $this->question_array[$i-1][0];
                      if ($qType == 2 || $qType == 1)
                      {
                          $options = $this->question_array[$i-1][2];
                          $ansPart = $options[$answers[$i]];
                      }
                      else if ($qType == 3)
                      {
                          $options = $this->question_array[$i-1][2];
                          $selected = explode(",",$answers[$i]);
                          $ansPart = "";
                          $sep = "";
                          for ($j=0; $j<sizeof($selected); $j++)
                          {
                               $ansPart .= $sep . $options[$selected[$j]];
                               $sep = "; ";
                          }
                      }
                      else if ($qType == 6)
                      {
                           $ansPart = "";
                      }
                      else
                      {
                          $ansPart = $answers[$i];
                      }
                      $result[] = $ansPart;
                 }
                 return $result;
        }

		function returnAnswers()
		{
        	global $sys_custom;
        	
			$namefield = getNameFieldByLang("b.");
			$orderBy = ($this->RecordType != 1)? "b.ClassName, b.ClassNumber, b.EnglishName" : "a.AnswerID";
			
			if ($sys_custom['SurveySaveWithDraft']) {
				$sql = "SELECT b.UserID, $namefield, b.ClassName, b.ClassNumber, a.Answer, a.DateModified, a.isDraft
							FROM INTRANET_SURVEYANSWER as a
							LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
							WHERE a.SurveyID = '".$this->SurveyID."' AND b.UserID IS NOT NULL
							ORDER BY $orderBy";
            }
            else {
            	$sql = "SELECT b.UserID, $namefield, b.ClassName, b.ClassNumber, a.Answer, a.DateModified
            				FROM INTRANET_SURVEYANSWER as a
            				LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
            				WHERE a.SurveyID = '".$this->SurveyID."' AND b.UserID IS NOT NULL
							ORDER BY $orderBy";
            }
			return $this->returnArray($sql,6);
        }

        function getRawDataTable()
        {
        	global $sys_custom;
                 $this->splitQuestion($this->Question);
                 $answers = $this->returnAnswers();
                 for ($i=0; $i<sizeof($answers); $i++)
                 {
					if ($sys_custom['SurveySaveWithDraft']) {
						list($id, $name, $class, $classnumber, $astr, $fillTime, $isDraft) = $answers[$i];
						$parsed = $this->parseAnswerStr($astr);
						$result[] = array($id,$name,$class,$classnumber,$parsed,$fillTime,$isDraft);
					} else {
						list($id, $name, $class, $classnumber, $astr, $fillTime) = $answers[$i];
						$parsed = $this->parseAnswerStr($astr);
						$result[] = array($id,$name,$class,$classnumber,$parsed,$fillTime);
					}
					
                 }
                 return $result;


        }

                function reqFillAllFields(){
                        $sql = "SELECT AllFieldsReq FROM INTRANET_SURVEY WHERE SurveyID = '$this->SurveyID'";
                        $result = $this->returnVector($sql);
                        if( $result[0] == "1" )
                                return true;
                        return false;
                }
                
                
	#########################################################################
    # Start in IP25
    #########################################################################
    /*
    * Get MODULE_OBJ array
    */
    function GET_MODULE_OBJ_ARR()
    {
            global $UserID, $plugin, $PATH_WRT_ROOT, $CurrentPage, $LAYOUT_SKIN, $image_path, $CurrentPageArr, $intranet_session_language, $intranet_root, $top_menu_mode, $special_feature;
            
            ### wordings 
            global  $Lang, $sys_custom;
            
            # Current Page Information init
            $PageSurveyList = 0;
    	    $PageLearnAndTeach = 0;
    	    $PageLearnAndTeachSurveyList = 0;
            $PageLearnAndTeachToSDAS = 0;
            
            if($CurrentPageArr['eAdmineSurvey'])		### eAdmin > eSurvey
            {
	            switch ($CurrentPage) 
	            {
                	case "PageSurveyList":
	                	$PageSurveyList = 1;
	                	break;
                	case "PageLearnAndTeachSurveyList":
                	    $PageLearnAndTeach = 1;
                	    $PageLearnAndTeachSurveyList = 1;
                	    break;
                	case "PageLearnAndTeachToSDAS":
                	    $PageLearnAndTeach = 1;
                	    $PageLearnAndTeachToSDAS = 1;
                	    break;
	                case "PageeSurveySettings_BasicSettings":
	                	$PageeSurveySettings = 1;
	                	$PageeSurveySettings_BasicSettings = 1;
	                	break;
	                
            	}
            
                $MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/eSurvey/";
				$MenuArr["SurveyList"] 	= array($Lang['eSurvey']['SurveyList'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/eSurvey/", $PageSurveyList);
				
				if($_SESSION['SSV_USER_ACCESS']['eAdmin-eSurvey'])
				{
				    if($sys_custom['iPf']['learnAndTeachReport']){
    					$MenuArr["LearnAndTeach"] 	= array($Lang['eSurvey']['LearnAndTeach']['LearnAndTeach'], "#", $PageLearnAndTeach);
				        $MenuArr["LearnAndTeach"]["Child"]["LearnAndTeachSurveyList"] = array($Lang['eSurvey']['SurveyList'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/eSurvey/?QuestionType=LT", $PageLearnAndTeachSurveyList);
				        $MenuArr["LearnAndTeach"]["Child"]["LearnAndTeachToSDAS"] = array($Lang['eSurvey']['LearnAndTeach']['ToSDAS'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/eSurvey/toSDAS/", $PageLearnAndTeachToSDAS);
				    }
					$MenuArr["Settings"] 	= array($Lang['eSurvey']['Settings'], "#", $PageeSurveySettings);
					$MenuArr["Settings"]["Child"]["BasicSettings"] 	= array($Lang['eSurvey']['BasicSettings'], $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/eSurvey/settings/basic_settings/", $PageeSurveySettings_BasicSettings);
				}
			}
			else										### eService > eSurvey
			{
				switch ($CurrentPage) 
	            {
                	case "PageNonAnswerSurvey":
	                	$PageNonAnswerSurvey = 1;
	                	break;
	                case "PageAnsweredSurvey":
	                	$PageAnsweredSurvey = 1;
	                	break;
            	}
            	
				$MODULE_OBJ['root_path'] = $PATH_WRT_ROOT."home/eService/eSurvey/";
				
				$MenuArr["NonAnswerSurvey"] 	= array($Lang['eSurvey']['NonAnswerSurvey'], $PATH_WRT_ROOT."home/eService/eSurvey/", $PageNonAnswerSurvey);
				$MenuArr["AnsweredSurvey"] 	= array($Lang['eSurvey']['AnsweredSurvey'], $PATH_WRT_ROOT."home/eService/eSurvey/completed_index.php", $PageAnsweredSurvey);
			}

        # change page web title
        $js = '<script type="text/JavaScript" language="JavaScript">'."\n";
        $js.= 'document.title="eClass eSurvey";'."\n";
        $js.= '</script>'."\n";
            
            ### module information
            $MODULE_OBJ['title'] = $Lang['Header']['Menu']['eSurvey'].$js;
            $MODULE_OBJ['title_css'] = "menu_opened";
            $MODULE_OBJ['logo'] = "{$image_path}/{$LAYOUT_SKIN}/leftmenu/icon_eoffice.gif";
            
            $MODULE_OBJ['menu'] = $MenuArr;
			
            return $MODULE_OBJ;
    }
    
    function returnSurvey_IP25($returnSQLonly=0, $hideCreatorAndTargetGroup = 0)
	{
		global $UserID, $Lang, $sys_custom;
		$date_conds = " AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()";
		
		# retrieve WHOLE SCHOOL survey
		$sql = "SELECT a.SurveyID FROM INTRANET_SURVEY as a
		        LEFT OUTER JOIN INTRANET_GROUPSURVEY as b ON a.SurveyID = b.SurveyID
		        WHERE b.SurveyID IS NULL AND a.RecordStatus = 1 $date_conds ";
		if($sys_custom['iPf']['learnAndTeachReport']){
			$sql .= "  AND a.QuestionType <> 'LT'";
		}	        
		$idschool = $this->returnVector($sql);
		
		# retrieve GROUP survey
		$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '$UserID' and GroupID is not NULL";
		$groups = $this->returnVector($sql);
		if (sizeof($groups)!=0) 
		{
			$group_list = implode(",",$groups);
		
		    $sql = "SELECT DISTINCT a.SurveyID FROM INTRANET_SURVEY as a, INTRANET_GROUPSURVEY as b
		            WHERE a.SurveyID = b.SurveyID AND b.GroupID IN ($group_list) AND a.RecordStatus = 1  $date_conds";
		    
		    $idgroup = $this->returnVector($sql);
		    $overall = array_merge($idschool,$idgroup);
		}
		else
		{
		    $overall = $idschool;
		}
		
		# retrieve SUBJECT GROUP survey
		if($sys_custom['iPf']['learnAndTeachReport']){
			$sql = "SELECT SubjectGroupID FROM SUBJECT_TERM_CLASS_USER WHERE UserID = '$UserID'";
			$groups = $this->returnVector($sql);
			if (sizeof($groups)!=0) 
			{
				$group_list = implode(",",$groups);
			
			    $sql = "SELECT DISTINCT a.SurveyID FROM INTRANET_SURVEY as a, INTRANET_SUBJECTGROUPSURVEY as b
			            WHERE a.SurveyID = b.SurveyID AND b.SubjectGroupID IN ($group_list) AND a.RecordStatus = 1  $date_conds";
			    
			    $idgroup = $this->returnVector($sql);
			    $overall = array_merge($overall,$idgroup);
			}
			else
			{
			    $overall = $overall;
			}
		}
		
		if ($returnSQLonly == 0)
		{
			if (sizeof($overall)==0) 
				return 0;
			else
				return $overall;
		}
		
		if(sizeof($overall)>0){
			$list = implode(",",$overall);
		}else{
			$list = -999;
		}
		$namefield = getNameFieldWithClassNumberByLang("c.");
		$sql ="set @c=0";
		$this->db_db_query($sql);
		
		if ($sys_custom['SurveySaveWithDraft']) {
			$sql = "SELECT
						DATE_FORMAT(a.DateStart,'%Y-%m-%d') as StartDate,
						DATE_FORMAT(a.DateEnd,'%Y-%m-%d') as EndDate,
						concat('<a href=\"javascript:openSurvey(', a.SurveyID,', ', @c := @c + 1 ,');\">', a.Title, '</a>'),";
            if(!$hideCreatorAndTargetGroup){
                $sql .= "IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$namefield),";
            }
            $sql .=     "IF(b.isDraft='1', '". $Lang['eSurvey']['Draft'] ."', '". $Lang['eSurvey']['NotAnswered'] ."'),
						a.SurveyID as SurveyID
					FROM
						INTRANET_SURVEY as a
					LEFT OUTER JOIN INTRANET_SURVEYANSWER as b ON a.SurveyID = b.SurveyID AND b.UserID = $UserID
					LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.PosterID
					WHERE
						a.SurveyID IN ($list) AND (b.SurveyID IS NULL OR b.isDraft = '1')";
		} else {
			$sql = "SELECT
						DATE_FORMAT(a.DateStart,'%Y-%m-%d') as StartDate,
						DATE_FORMAT(a.DateEnd,'%Y-%m-%d') as EndDate,
						concat('<a href=\"javascript:openSurvey(', a.SurveyID,', ', @c := @c + 1 ,');\">', a.Title, '</a>'),";
			if(!$hideCreatorAndTargetGroup){
                $sql .= "IF(c.UserID IS NULL,CONCAT('<I>',a.PosterName,'</I>'),$namefield),";
            }
            $sql .=     "	'". $Lang['eSurvey']['NotAnswered'] ."',
						a.SurveyID as SurveyID
					FROM
						INTRANET_SURVEY as a
					LEFT OUTER JOIN INTRANET_SURVEYANSWER as b ON a.SurveyID = b.SurveyID AND b.UserID = $UserID
					LEFT OUTER JOIN INTRANET_USER as c ON c.UserID = a.PosterID
					WHERE
						a.SurveyID IN ($list) AND b.SurveyID IS NULL ";
		}
		
		//LEFT OUTER JOIN INTRANET_GROUP as d ON d.GroupID = a.OwnerGroupID

		// ORDER BY a.DateEnd";
		if($returnSQLonly)
			return $sql;
		else
			return $this->returnArray($sql);
	}
	
	function returnUserAnswer($viewUserID="", $SurveyID="")
	{
		global $sys_custom;
		if(!$SurveyID)
			$SurveyID = $this->SurveyID;
		if($SurveyID && $viewUserID)
		{
			if ($sys_custom['SurveySaveWithDraft']) {
				$sql = "SELECT Answer, UserName, isDraft FROM INTRANET_SURVEYANSWER WHERE SurveyID = '$SurveyID' AND UserID = '$viewUserID'";
			} else {
				$sql = "SELECT Answer,UserName FROM INTRANET_SURVEYANSWER WHERE SurveyID = '$SurveyID' AND UserID = '$viewUserID'";
			}
			return $this->returnArray($sql);
		}
		else
			return array();
	}
	
	
		
		function returnEmailNotificationData($date, $title, $type='', $groups='')
        {
	        global $Lang;

	        $website = get_website();
	        $webmaster = get_webmaster();
	        
	        $email_subject = $Lang['EmailNotification']['eSurvey']['Subject'];
	        $email_subject = str_replace("__TYPE__", $type, $email_subject);
	        $email_subject = str_replace("__DATE__", $date, $email_subject);
	        
	        if(empty($groups))
	        {
		        $email_content = $Lang['EmailNotification']['eSurvey']['Content']['SCHOOL'];
	        }
	        else
	        {
	        	$email_content = $Lang['EmailNotification']['eSurvey']['Content']['GROUP'];
	        	$list = implode(",",$groups);
	        	$email_content = str_replace("__LIST__", $list, $email_content);
        	}
        	
	        $email_content = str_replace("__TITLE__", $title, $email_content);
	        $email_content = str_replace("__DATE__", $date, $email_content);
	        $email_content = str_replace("__WEBSITE__", $website, $email_content);
	        
	        $email_footer = $Lang['EmailNotification']['Footer'];
			$email_footer = str_replace("__WEBSITE__", $website, $email_footer);
			$email_footer = str_replace("__WEBMASTER__", $webmaster, $email_footer);
			
	        $email_content .= $email_footer;
			return array($email_subject, $email_content);
        }
        
        function checkDateFormat($RecordDate)
        {
        	// replace split() by explode() - for PHP 5.4
        	list($year, $month, $day) = explode('-',$RecordDate);
        
        	if(strlen($year)==4 && strlen($month)==2 && strlen($day)==2)
        	{
        		if(checkdate($month,$day,$year))
        		{
        			return true;
        		}
        	}
        	return false;
        }
 }

} // End of directives
?>