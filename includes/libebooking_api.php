<?php 
/*
 * Using:        
 * 
 * Change Log:
 *
 * Date:    2019-06-18 Cameron
 *          - modify CheckAvailableOverallPeriod(), bypass special booking period and regular booking period checking if EverydayTimetable is set
 *          - add TimeSlotID in InsertEbookingRecord()
 * Date:    2019-06-17 Cameron
 *          - modify GetTimeslotData() to return timeSlotInfo for Everyday Timetable [case #M141869]
 * Date:    2019-03-15 Cameron
 *          - apply natsort to NameEng in GetAllItem()
 * Date:    2019-02-26 Cameron
 *          - call Check_If_Booking_Need_Approval() from parent class in GetAutoApproveSetting() to make approval requirement be consistent with web
 *          (e.g. Not need to approve if one of the room booking rule is set to not require)
 * Date:    2019-01-24 Cameron
 *          - return sending email result in RequestSendingEmail()          
 * Date:    2019-01-07 Cameron
 *          - don't get record from INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE in CheckAvailableGeneralPeriod() for ej as this table not exist in ej
 *          - fix: sql in CheckAvailableSpecialPeriod() should break into room and item case    [case #P154856]
 *          - add AutoApprove checking for items in InsertEbookingRecord()
 * Date:    2019-01-02 Cameron
 *          - fix: should check $BookingType before assigning $Check result to return in CheckAvailableSpecialPeriod()      [case #P154856]
 * Date:    2018-12-28 Cameron
 *          - fix: reset CanBeBooked to true if there's at least one available timeslot in CheckAvailableSpecialPeriod()    [case #P154856]
 * Date:    2018-12-12 Cameron
 *          - fix: should add ProcessDate and ProcessDateTime in INTRANET_EBOOKING_BOOKING_DETAILS in InsertEbookingRecord()
 * Date:    2018-08-31 Isaac
 *          -added Check_If_Facility_Is_Available() to GetAvailableRoom()'s checking to check if the room is bookable.
 * Date:    2018-03-08 Omas
 *          -modified CheckIsBookedRoom() - fix fail to check some time clash problem 
 * Date:	2017-06-14 Villa
 * 			-Modified CheckAvailableGeneralPeriod(): support cross timeslote 
 * Date:	2017-05-12 Villa
 * 			-Finish the booking Part
 * 			-Fix No cycle date problem
 * Date:	2017-05-08 Villa
 * 			-Finish Checking Part
 * Date:	2017-05-04 Villa
 * 			-Open the File
 */

class libebooking_api extends libebooking{

    #Variable
	private $AllLocationArr = array();
	private $AllItemArr = array();
	private $UserInfo;
	private $UserID;
	private $EbookingSetting = array();
	
	#construtor
	public function libebooking_api(){
		$this->libdb();
		$this->UserID = $_SESSION['UserID'];
		$this->UserInfo = $this->GetUserInfo('');
		$this->GetEbookingSetting();
		
	} 
	
	### Private Function Start
		###General Function Start
		private function isEJ(){
			global $junior_mck;
			
			return isset($junior_mck);
		}
		private function GetEbookingSetting(){
			if(!empty($this->$EbookingSetting)){
				
			}else{
				$sql = 'Select
							*
						FROM
							INTRANET_EBOOKING_SETTING 
						';
				$rs = BuildMultiKeyAssoc($this->returnResultSet($sql), 'SettingName','SettingValue',1);
				$this->EbookingSetting = $rs;
			}
			return $this->EbookingSetting;
		}
		private function GetAllItem(){
			if(!empty($this->AllItemArr)){
				
			}else{
				$sql = '
						Select
							Item.ItemID, 
							Item.eInventoryItemID, 
							Item.NameChi, 
							Item.NameEng, 
							Item.LocationID, 
							Item.AllowBooking, 
							Item.AllowBookingIndependence,
							Cat.CategoryID, 
							Cat.NameChi as catNameChi, 
							Cat.NameEng as catNameEng, 
							SubCat.NameChi as cat2NameChi, 
							SubCat.Category2ID as cat2CategoryID, 
							SubCat.NameEng as cat2NameEng
						FROM
							INVENTORY_CATEGORY AS Cat
							INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS SubCat ON (Cat.CategoryID = SubCat.CategoryID)
							INNER JOIN INTRANET_EBOOKING_ITEM  AS Item ON (Cat.CategoryID = Item.CategoryID AND SubCat.Category2ID = Item.Category2ID)
							INNER JOIN INVENTORY_LOCATION as room On (Item.LocationID = room.LocationID)
							INNER JOIN INVENTORY_LOCATION_LEVEL as floor On (room.LocationLevelID = floor.LocationLevelID)
							INNER JOIN INVENTORY_LOCATION_BUILDING as building On (floor.BuildingID = building.BuildingID)
						WHERE
							Item.RecordStatus = 1 
						ORDER BY
							Cat.DisplayOrder,
							SubCat.DisplayOrder,
                            Item.NameEng
				';
				$rs_temp = $this->returnResultSet($sql);
				$nameEngAry = array();
				$itemIDIndex = array();
				foreach ((array)$rs_temp as $_rs_temp){
					foreach((array)$_rs_temp as $keyName=>$keyValue){
						if($this->isEJ()){
							$keyValue = convert2unicode($keyValue,1,1);
						}
						$rs[$_rs_temp['ItemID']][$keyName] = $keyValue;
					}
					$nameEngAry[] = $_rs_temp['NameEng'];
					$itemIDIndex[] = $_rs_temp['ItemID'];
				}
				natsort($nameEngAry);
				
				$newRsAry = array();
				foreach((array)$nameEngAry as $_index => $_value) {
				    $_itemID = $itemIDIndex[$_index];
				    $newRsAry[$_itemID] = $rs[$_itemID];
				}
				
				$this->AllItemArr= $newRsAry;
			}
			return $this->AllItemArr;
		}
		private function GetAllLocation(){
			if(!empty($this->AllLocationArr)){
				
			}else{
				$sql = 'Select 
							il.LocationID, il.NameChi, il.NameEng, il.LocationID, ill.NameChi as LevelNameChi, ill.NameEng as LevelNameEng, ill.LocationLevelID, ilb.NameChi as BuildingNameChi, ilb.NameEng as BuildingNameEng, ilb.BuildingID, il.AllowBooking
						From 
							INVENTORY_LOCATION  il
						INNER JOIN 
						 	INVENTORY_LOCATION_LEVEL  ill
								ON ill.LocationLevelID = il.LocationLevelID
						INNER JOIN
							INVENTORY_LOCATION_BUILDING ilb
								ON ilb.BuildingID = ill.BuildingID
						Where 
							il.RecordStatus = "1"
						';
				$rs_temp = $this->returnResultSet($sql);
				foreach ((array)$rs_temp as $_rs_temp){
					foreach ((array)$_rs_temp as $keyName => $keyValue){
						if($this->isEJ()){
							$keyValue = convert2unicode($keyValue,1,1);
						}
						$rs[$_rs_temp['LocationID']][$keyName] = $keyValue;
					}
				}
				$this->AllLocationArr = $rs;
			}
			return $this->AllLocationArr;
		}
		private function GetAllLocationRuleRelationArray(){
			$sql = 'Select 
						il.LocationID, ielubrr.UserBookingRuleID, ielbrr.BookingRuleID
					From
						INVENTORY_LOCATION  il
					LEFT JOIN
						INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION ielubrr ON il.LocationID = ielubrr.LocationID
					LEFT JOIN
						INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION ielbrr ON il.LocationID = ielbrr.LocationID
					Where 
						il.RecordStatus = "1"
					AND 
						il.AllowBooking = "1"
					AND
						ielbrr.RecordStatus = "1"
';
			$rs =  $this->returnResultSet($sql);
			foreach((array)$rs as $_rs){
				$rs['UserRule'] [$_rs['LocationID']]= $_rs['UserBookingRuleID'];
				$rs['GeneralRule'][$_rs['LocationID']] = $_rs['BookingRuleID'];
			}
			return $rs;
						
		}
		private function GetUserInfo($userID){
		    $intrnaetUserTable = 'INTRANET_USER';
		    if($this->isEJ()){
		        $intrnaetUserTable = 'INTRANET_USER_UTF8';
		    }
			if($userID){
				
			}else{
				$userID = $this->UserID;
			}
			$AcademicYear = Get_Current_Academic_Year_ID();
			$sql = '
					Select 
						iu.EnglishName, iu.ChineseName, iu.RecordType, yc.YearID
					FROM 
						'.$intrnaetUserTable.' iu
					LEFT JOIN
						YEAR_CLASS_USER ycu ON ycu.UserID = iu. UserID
					LEFT JOIN
						YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = "'.$AcademicYear.'"
					WHERE
						iu.UserID = "'.$userID.'"
					
			';
			$rs =  $this->returnResultSet($sql);
			return $rs[0];
			
		}
		private function GetLocationIdFromBookingID($BookingID){
			$sql = '
				Select NameChi, NameEng, BookingStatus, BookingID From INTRANET_EBOOKING_BOOKING_DETAILS iebd
				INNER JOIN 
					INVENTORY_LOCATION  ill ON ill.LocationID = iebd.FacilityID
				WHERE
					iebd.BookingID = "'.$BookingID.'";
			';
			return $this->returnResultSet($sql);
		}
		###General Function End
		
		###Checkong Function Start
		
		#Time Period Checking Start
		private function CheckAvailableGeneralPeriod($BookingDetails,$Mode){
			#Mode 1=> General
			#Default (NOt SET) => Specific Room
			$Date = $BookingDetails['Date'];
			$StartTime = $BookingDetails['StartTime'];
			$EndTime = $BookingDetails['EndTime'];
			$BookingType = $BookingDetails['BookingType'];
			if($BookingType == 'item'){
				$AllLocationArr = $this->GetAllItem();
			}else{
				$AllLocationArr = $this->GetAllLocation();
			}
			###Convert the date to Day			
			$sql = 'Select 
						CycleDay
					FROM
						INTRANET_CYCLE_DAYS 
					WHERE 
						RecordDate = "'.$Date.'"
			';
			$day = $this->returnResultSet($sql);
			$day = $day[0]['CycleDay'];
			// if it is a cycle day
			if(!$day){
				$day = date('w',strtotime($Date));
			}
			
			###Time Day
			if(!$this->isEJ()){
    			$sql = 'Select
    						SpecialDate
    					From
    						INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE  itst 
    					WHERE
    						"'.$Date.'" = itst.SpecialDate';
    			$rs = $this->returnResultSet($sql);
    			$DateArrUsingSpecialTimeTable = Get_Array_By_Key($rs, 'SpecialDate');
			}
			else {
			    $DateArrUsingSpecialTimeTable = array();
			}
			
			###Get All The Record on that time(Normal TimeTable)
			if($this->isEJ()){
				
				$sql = 'Select
						ieabts.FacilityID, ieabts.FacilityType, itt.SlotID  as TimeSlotID, SUBSTRING_INDEX(itt.TimeRange,"-",1) as StartTime, SUBSTRING_INDEX(SUBSTRING_INDEX(itt.TimeRange,"-",2),"-",-1) as EndTime
					From
						INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING ieabts
					INNER JOIN
						INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT ieabtst
							on ieabts.TimezoneSettingID = ieabtst.TimezoneSettingID
					INNER JOIN
						INTRANET_CYCLE_GENERATION_PERIOD icgp
							ON icgp.PeriodID = ieabts.TimezoneID
					INNER JOIN
						INTRANET_SLOT itt
							ON itt.SlotID = ieabtst.TimeSlotID
					WHERE
						"'.$Date.'" BETWEEN icgp.PeriodStart AND icgp.PeriodEND
					AND
						ieabtst.Day = "'.$day.'"';
				
			}else{
				$sql = 'Select
							ieabts.FacilityID, ieabts.FacilityType, itt.TimeSlotID, itt.StartTime, itt.EndTime
						From
							INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING ieabts
						INNER JOIN
							INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT ieabtst
								on ieabts.TimezoneSettingID = ieabtst.TimezoneSettingID
						INNER JOIN
							INTRANET_CYCLE_GENERATION_PERIOD icgp
								ON icgp.PeriodID = ieabts.TimezoneID
						INNER JOIN
							INTRANET_TIMETABLE_TIMESLOT itt
								ON itt.TimeSlotID = ieabtst.TimeSlotID
						WHERE
							"'.$Date.'" BETWEEN icgp.PeriodStart AND icgp.PeriodEND
						AND
							ieabtst.Day = "'.$day.'"
						';
				
			}
			$rs = $this->returnResultSet($sql);
			
			#repack the raw rs to data[facility][date][] style
			foreach((array)$rs as $_rs){
				if(!in_array($Date, $DateArrUsingSpecialTimeTable)){
					if($_rs['FacilityType']=='2'){
						if($BookingType=='item'){
							$timeslotArr_temp[$_rs['FacilityID']][$Date][] = $_rs;
						}
					}else{
						$timeslotArr_temp[$_rs['FacilityID']][$Date][] = $_rs;
					}
				}
			}
			//Get Merged Timeslot
			foreach ((array)$timeslotArr_temp as $facilityID => $_timeslotArr_temp){
				$timeslotArr[$facilityID] = $this->Union_Time_Range_Of_Date_Assoc($_timeslotArr_temp);
			}
			//check if the start time/ end time is in the merge timeslot
			foreach((array)$timeslotArr as $facilityID => $_timeslotArr){
				foreach((array)$_timeslotArr as $date => $__timeslotArr){
					foreach ((array)$__timeslotArr as $___timeslotArr){ //each merged timeslote
						$___timeslotArr_temp = explode('-',$___timeslotArr);
						$___timeslotArr_StartTime = $___timeslotArr_temp[0].":00";
						$___timeslotArr_End = $___timeslotArr_temp[1].":00";
						//checking
						if(strtotime($StartTime)>=strtotime($___timeslotArr_StartTime) && strtotime($___timeslotArr_End)>=strtotime($EndTime)){
							$temp[$Date][$facilityID]['FacilityID'] = $facilityID;
							$temp[$Date][$facilityID]['CanBeBooked'] = '1';
						}
					}
				}
			}
			unset($timeslotArr_temp);
			unset($timeslotArr);
			
			###Get All the record on that time(Special Time Table)
			if($this->isEJ()){
				$sql = 'Select
						ieabts.FacilityID, ieabts.FacilityType, itt.SlotID  as TimeSlotID, SUBSTRING_INDEX(itt.TimeRange,"-",1) as StartTime, SUBSTRING_INDEX(SUBSTRING_INDEX(itt.TimeRange,"-",2),"-",-1) as EndTime
					From
						INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING ieabts
					INNER JOIN
						INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT  ieabtst
							on ieabts.TimezoneSettingID = ieabtst.TimezoneSettingID
					INNER JOIN
						INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE  itst
							on ieabts.SpecialTimetableSettingsID = itst.SpecialTimetableSettingsID
					INNER JOIN
						INTRANET_SLOT itt
							ON itt.SlotID = ieabtst.TimeSlotID
					WHERE
						"'.$Date.'" = itst.SpecialDate
			';
			}else{
				$sql = 'Select
							ieabts.FacilityID, ieabts.FacilityType, itt.TimeSlotID, itt.StartTime, itt.EndTime
						From 
							INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING ieabts
						INNER JOIN
							INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT  ieabtst 
								on ieabts.TimezoneSettingID = ieabtst.TimezoneSettingID
						INNER JOIN 
							INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE  itst 
								on ieabts.SpecialTimetableSettingsID = itst.SpecialTimetableSettingsID
						INNER JOIN
							INTRANET_TIMETABLE_TIMESLOT itt
								ON itt.TimeSlotID = ieabtst.TimeSlotID
						WHERE
							"'.$Date.'" = itst.SpecialDate
				';
			}
			$rs = $this->returnResultSet($sql);
			###Overwrite the Date on Special Time Table
			foreach((array)$rs as $_rs){
				if(in_array($Date, $DateArrUsingSpecialTimeTable)){
					if($_rs['FacilityType']=='2'){
						if($BookingType=='item'){
							$timeslotArr_temp[$_rs['FacilityID']][$Date][] = $_rs;
						}
					}else{
						$timeslotArr_temp[$_rs['FacilityID']][$Date][] = $_rs;
					}
				}
			}
			//Get Merged Timeslot
			foreach ((array)$timeslotArr_temp as $facilityID => $_timeslotArr_temp){
				$timeslotArr[$facilityID] = $this->Union_Time_Range_Of_Date_Assoc($_timeslotArr_temp);
			}
			//check if the start time/ end time is in the merge timeslot
			foreach((array)$timeslotArr as $facilityID => $_timeslotArr){
				foreach((array)$_timeslotArr as $date => $__timeslotArr){
					foreach ((array)$__timeslotArr as $___timeslotArr){ //each merged timeslote
						$___timeslotArr_temp = explode('-',$___timeslotArr);
						$___timeslotArr_StartTime = $___timeslotArr_temp[0].":00";
						$___timeslotArr_End = $___timeslotArr_temp[1].":00";
						//checking
						if(strtotime($StartTime)>=strtotime($___timeslotArr_StartTime) && strtotime($___timeslotArr_End)>=strtotime($EndTime)){
							$temp[$Date][$facilityID]['FacilityID'] = $facilityID;
							$temp[$Date][$facilityID]['CanBeBooked'] = '1';
						}
					}
				}
			}
			
			if($Mode=='1'){
				foreach ($AllLocationArr as $_roomID => $_room){
					if(isset($temp[$Date][0]['CanBeBooked'])){
						$Check[$Date][$_roomID] = true;
					}else{
						$Check[$Date][$_roomID] = false;
					}
				}
			}else{
				foreach ((array)$temp[$Date] as $_temp){
					$_roomID = $_temp['FacilityID'];
					if(isset($temp[$Date][$_roomID]['CanBeBooked'])){
						$Check[$Date][$_roomID] = true;
					}else{
						$Check[$Date][$_roomID] = false;
					}
				}
			}
			
			return $Check;
		}
		
		private function CheckAvailableSpecialPeriod($BookingDetails, $Mode){
			
			$Date = $BookingDetails['Date'];
			$StartTime = $BookingDetails['StartTime'];
			$EndTime = $BookingDetails['EndTime'];
			$BookingType = $BookingDetails['BookingType'];
			
			if($BookingType == 'item'){
			    /*
			     *    RelatedItemID=0 AND RelatedSubLocationID=0 --> have set available booking time slot in Booking Period > Default Period > Special Booking
			     *    RelatedItemID>0 AND RelatedSubLocationID=0 --> have set available booking time slot in Booking Period > Room / Item Period > Item > Special Booking
			     *    RelatedItemID=0 AND RelatedSubLocationID>0 --> have set available booking time slot in Booking Period > Room / Item Period > Room > Special Booking
			     */
			    $sql = 'Select RelatedSubLocationID, RelatedItemID, StartTime, EndTime From INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RecordDate = "'.$Date.'"';
			    $sql .= " AND (RelatedItemID>0 OR (RelatedItemID=0 AND RelatedSubLocationID=0))";
			    $rs = $this->returnResultSet($sql);
			    
			    foreach ((array)$rs as $_rs){
			        $_relatedItemID = $_rs['RelatedItemID'];     // $_relatedItemID can be the same (e.g. 0) in the whole $rs
			        if (isset($temp[$Date][$_relatedItemID]['CanBeBooked']) && $temp[$Date][$_relatedItemID]['CanBeBooked'] == 1) {
			            $_tempCanBeBooked = true;
			        }
			        else{
			            $_tempCanBeBooked = false;
			        }
			        
			        $temp[$Date][$_relatedItemID] = $_rs;
			        if(($StartTime>=$_rs['StartTime']&&$StartTime<=$_rs['EndTime']) && ($EndTime>=$_rs['StartTime']&&$EndTime<=$_rs['EndTime'])){
			            $temp[$Date][$_relatedItemID]['CanBeBooked'] = 1;
			        }
			        
			        if ($_tempCanBeBooked) {
			            $temp[$Date][$_relatedItemID]['CanBeBooked'] = 1;        // reset it to be true if there's at least one available timeslot
			        }
			    }
			    
			    $AllLocationArr = $this->GetAllItem();
			} else {
			    $sql = 'Select RelatedSubLocationID, RelatedItemID, StartTime, EndTime From INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RecordDate = "'.$Date.'"';
			    $sql .= " AND (RelatedSubLocationID>0 OR (RelatedItemID=0 AND RelatedSubLocationID=0))";
			    $rs = $this->returnResultSet($sql);
			    
			    foreach ((array)$rs as $_rs){
			        $_relatedSubLocationID = $_rs['RelatedSubLocationID'];
			        if (isset($temp[$Date][$_relatedSubLocationID]['CanBeBooked']) && $temp[$Date][$_relatedSubLocationID]['CanBeBooked'] == 1) {
			            $_tempCanBeBooked = true;
			        }
			        else{
			            $_tempCanBeBooked = false;
			        }
			        
			        $temp[$Date][$_relatedSubLocationID] = $_rs;
			        if(($StartTime>=$_rs['StartTime']&&$StartTime<=$_rs['EndTime']) && ($EndTime>=$_rs['StartTime']&&$EndTime<=$_rs['EndTime'])){
			            $temp[$Date][$_relatedSubLocationID]['CanBeBooked'] = 1;
			        }
			        
			        if ($_tempCanBeBooked) {
			            $temp[$Date][$_relatedSubLocationID]['CanBeBooked'] = 1;
			        }
			    }
			    
			    $AllLocationArr = $this->GetAllLocation();
			}
			
			if($Mode=='1'){      // special, all items or locations
				foreach ($AllLocationArr as $_roomID => $_room){
					if(isset($temp[$Date][0]['CanBeBooked'])){
						$Check[$Date][$_roomID] = true;
					}else{
						if(isset($temp[$Date][0])){
							$Check[$Date][$_roomID] = false;
						}else{
							//Do Nth
						}
					}
				}
			}else{               // specific special, items or locations in INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD only
			    if ($BookingType == 'item') {
			        foreach((array)$temp[$Date] as $_temp){
			            $_itemID = $_temp['RelatedItemID'];
		                $Check[$Date][$_itemID] = isset($temp[$Date][$_itemID]['CanBeBooked']) ? true : false;
			        }
			    }
			    else {       // room
    				foreach((array)$temp[$Date] as $_temp){
    				    $_roomID = $_temp['RelatedSubLocationID'];
    				    $Check[$Date][$_roomID] = isset($temp[$Date][$_roomID]['CanBeBooked']) ? true : false;
    				}
			    }
			}
			return $Check;
		}
		
		private function CheckAvailableSuspendPeriod($BookingDetails, $Mode){
			$Date = $BookingDetails['Date'];
			$StartTime = $BookingDetails['StartTime'];
			$EndTime = $BookingDetails['EndTime'];
			$BookingType = $BookingDetails['BookingType'];
			
			$sql ='Select RelatedSubLocationID, RelatedItemID From INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION WHERE RecordDate = "'.$Date.'"';
			$rs = $this->returnResultSet($sql);
			foreach ((array)$rs as $_rs){
				if($_rs['RelatedItemID']){
					if($BookingType == 'item'){
						$temp[$Date][$_rs['RelatedItemID']] = $_rs;
						$temp[$Date][$_rs['RelatedItemID']]['IsSuspend'] = 1;
					}else{
						//do nothing
					}
				}else{
					// room/ general 
					$temp[$Date][$_rs['RelatedSubLocationID']] = $_rs;
					$temp[$Date][$_rs['RelatedSubLocationID']]['IsSuspend'] = 1;
				}
				
			}
			
			if($BookingType=='item'){
				$AllLocationArr = $this->GetAllItem();
			}else{
				$AllLocationArr = $this->GetAllLocation();
			}
			
			if($Mode=='1'){      // cover all location or all items
				foreach ($AllLocationArr as $_roomID => $_room){
					if(isset($temp[$Date][0]['IsSuspend']) ){
						$Check[$Date][$_roomID] = false;
					}else{
						$Check[$Date][$_roomID] = true;
					}
				}
			}else{               // cover the record in the specified date only in INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION
				foreach ((array)$temp[$Date] as $_temp){
					if($BookingType == 'item'){
						$_roomID = $_temp['RelatedItemID']; //itemID
					}else{
						$_roomID = $_temp['RelatedSubLocationID']; //locationID
					}
					if($_temp['IsSuspend']){
						$Check[$Date][$_roomID] = false;
					}else{
						$Check[$Date][$_roomID] = true;
					}
				}
			}
			return $Check;
		}
		
		private function CheckAvailableOverallPeriod($BookingDetails){
		    global $sys_custom;
		    
			$Date = $BookingDetails['Date'];
			$StartTime = $BookingDetails['StartTime'];
			$EndTime = $BookingDetails['EndTime'];
			$BookingType = $BookingDetails['BookingType'];       // room / item
			
			if ($sys_custom['eBooking']['EverydayTimetable']) {      // ignore special period setting and general setting
			    $SpecificSpecial = array();
			    $SpecificGeneral = array();
			    $Special = array();
			    $General = array();
			}
			else {
			    $SpecificSpecial = $this->CheckAvailableSpecialPeriod($BookingDetails,0);    // cover records of the specified date in INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD only
			    $SpecificGeneral = $this->CheckAvailableGeneralPeriod($BookingDetails,0);
			    $Special = $this->CheckAvailableSpecialPeriod($BookingDetails,1);
			    $General = $this->CheckAvailableGeneralPeriod($BookingDetails,1);
			}
			$SpecificSuspend = $this->CheckAvailableSuspendPeriod($BookingDetails,0);    // cover records of the specified date in INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION only
			$Suspend = $this->CheckAvailableSuspendPeriod($BookingDetails,1);            // cover all items or locations
			

			if($BookingType=='item'){
				//item
				$AllLocationArr = $this->GetAllItem();      // associated array by ItemID
			}else{
				//room
				$AllLocationArr = $this->GetAllLocation();  // associated array by LocationID
			}
			
			foreach ($AllLocationArr as $_roomID => $_room){
				if(isset($SpecificSuspend[$Date][$_roomID])){
					$Check[$Date][$_roomID] = $SpecificSuspend[$Date][$_roomID];
					continue;
				}
				if(isset($SpecificSpecial[$Date][$_roomID])){
					$Check[$Date][$_roomID] = $SpecificSpecial[$Date][$_roomID];         // once match, will ignore that in $Special
					continue;
				}
				if(isset($SpecificGeneral[$Date][$_roomID])){
					$Check[$Date][$_roomID] = $SpecificGeneral[$Date][$_roomID];
					continue;
				}
				
				if(isset($Suspend[$Date][$_roomID])){
					if(!$Suspend[$Date][$_roomID]){
						$Check[$Date][$_roomID] = $Suspend[$Date][$_roomID];
						continue;
					}
				}
				if(isset($Special[$Date][$_roomID])){
					$Check[$Date][$_roomID] = $Special[$Date][$_roomID];
					continue;
				}
				if(isset($General[$Date][$_roomID])){
					$Check[$Date][$_roomID] = $General[$Date][$_roomID];
					continue;
				}
				
				if ($sys_custom['eBooking']['EverydayTimetable']) {
				    $Check[$Date][$_roomID] = 1;        // set room/facility to be bookable if not suspended
				    continue;
				}
			}
			
			return $Check;
		}
		#Time Period Checking End
		
		#IsBookingChase Checking Start
		private function CheckIsBookedRoom($BookingDetails){
			
			$Date = $BookingDetails['Date'];
			$StartTime = $BookingDetails['StartTime'];
			$EndTime = $BookingDetails['EndTime'];
			$BookingType = $BookingDetails['BookingType'];
			if($BookingType=='item'){
				//item
				$AllLocationArr = $this->GetAllItem();
				$FacilityTypeCond = ' AND iebd.FacilityType = "2" ';
			}else{
				//room
				$AllLocationArr = $this->GetAllLocation();
				$FacilityTypeCond = ' AND iebd.FacilityType = "1" ';
			}
			
			$sql = 'Select 
						iebd.FacilityID, iebd.FacilityType
					FROM 
						INTRANET_EBOOKING_RECORD ier
					INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS iebd
						ON	ier.BookingID = iebd.BookingID
					WHERE 
						ier.Date = "'.$Date.'" 
					AND 
						(
							("'.$StartTime.'" = ier.StartTime AND "'.$EndTime.'" =ier.EndTime )
							OR ("'.$StartTime.'" >= ier.StartTime AND ier.EndTime > "'.$StartTime.'") 
						    OR ("'.$EndTime.'" > ier.StartTime AND ier.EndTime > "'.$EndTime.'")
							OR ("'.$StartTime.'" < ier.StartTime AND "'.$EndTime.'" >= ier.EndTime )
						)
					AND 
						ier.RecordStatus="1" 
					AND 
						iebd.RecordStatus="1"'.$FacilityTypeCond;
			$rs = $this->returnResultSet($sql);
			$BookedFacilityID = Get_Array_By_Key($rs, 'FacilityID');
			foreach ($AllLocationArr as $_roomID => $_room){
				if(!in_array($_roomID, $BookedFacilityID)){
					$Check[$Date][$_roomID] = true;
				}else{
					$Check[$Date][$_roomID] = false;
				}
			}
			return $Check;
		}
		#IsBookingChase Checking End
		
		#Booking Rule Checking Start
		private function CheckBookingUserRule($BookingDetails){
		
			$Date = $BookingDetails['Date'];
			$StartTime = $BookingDetails['StartTime'];
			$EndTime = $BookingDetails['EndTime'];
			$BookingType = $BookingDetails['BookingType'];
			$AllLocationArr = $this->GetAllLocation();
			if($BookingType=='item'){
				//item
				$AllLocationArr = $this->GetAllItem();
				$BookingTypeID = 'ItemID';
				$BookingTypeTable = 'INTRANET_EBOOKING_ITEM';
				$BookingTypeInnerJoinTable = 'INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE ielubrr ON ielubrr.ItemID = il.ItemID';
				$BookingTypeInnerJoinTableKey = 'RuleID';
			}else{
				//room
				$AllLocationArr = $this->GetAllLocation();
				$BookingTypeID = 'LocationID';
				$BookingTypeTable = 'INVENTORY_LOCATION';
				$BookingTypeInnerJoinTable = 'INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION ielubrr ON ielubrr.LocationID = il.LocationID';
				$BookingTypeInnerJoinTableKey = 'UserBookingRuleID';
			}
			
			
			$UserID = $this->UserID;
			$UserYearID = $this->UserInfo['YearID'];
			$UserRecordType = $this->UserInfo['RecordType'];
			if($UserYearID){
				$UserYearIDCond = ' AND ieubrtu.YearID="'.$UserYearID.'"';
			}else{
				$UserYearIDCond = '';
			}
			#Type 1,2,3 
			$sql = 'Select 
						il.'.$BookingTypeID.', IF(MIN(ieubr.DaysBeforeUse)=0, 0, MAX(ieubr.DaysBeforeUse)) as DaysBeforeUse, MAX(ieubr.DisableBooking) as DisableBooking
					From 
						'.$BookingTypeTable.' il
					INNER JOIN
						'.$BookingTypeInnerJoinTable.'
					INNER JOIN
						INTRANET_EBOOKING_USER_BOOKING_RULE ieubr ON ielubrr.'.$BookingTypeInnerJoinTableKey.' = ieubr.RuleID
					INNER JOIN
						INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER ieubrtu ON ieubr.RuleID = ieubrtu.RuleID
					WHERE
						il.RecordStatus = "1"
					AND
						il.AllowBooking = "1"
					AND 
						ieubrtu.UserType = "'.$UserRecordType.'"
					'.$UserYearIDCond.'
					GROUP BY
						il.'.$BookingTypeID.'
					';
			$rs_temp =  $this->returnResultSet($sql);
			
			$rs = array();
			foreach ((array)$rs_temp as $_rs_temp){
				$rs[$_rs_temp[$BookingTypeID]][$BookingTypeID] = $_rs_temp[$BookingTypeID];
				if(!$_rs_temp['DisableBooking']){
					//allow booking(always overwrite disablebooking)
					$rs[$_rs_temp[$BookingTypeID]]['DisableBooking'] = $_rs_temp['DisableBooking'];
					//get the max daysBeforeUser for the allow booking record
					if($_rs_temp['DaysBeforeUse']==='0'){
						$rs[$_rs_temp[$BookingTypeID]]['DaysBeforeUse'] = '0';
					}elseif($rs[$_rs_temp[$BookingTypeID]]['DaysBeforeUse'] === '0'){
						//do nth
					}elseif($_rs_temp['DaysBeforeUse']>$rs[$_rs_temp[$BookingTypeID]]['DaysBeforeUse']){
						$rs[$_rs_temp[$BookingTypeID]]['DaysBeforeUse'] = $_rs_temp['DaysBeforeUse'];
					}
				}else{
					if(isset($rs[$_rs_temp[$BookingTypeID]]['DisableBooking'])){
						//do nothing
					}else{
						$rs[$_rs_temp[$BookingTypeID]]['DisableBooking'] = $_rs_temp['DisableBooking'];
					}
				}
			}
			#Type 4
			$sql = 'Select 
						il.'.$BookingTypeID.', ieubr.DaysBeforeUse as DaysBeforeUse, ieubr.DisableBooking as DisableBooking
					From 
						'.$BookingTypeTable.' il
					INNER JOIN
						'.$BookingTypeInnerJoinTable.'
					INNER JOIN
						INTRANET_EBOOKING_USER_BOOKING_RULE ieubr ON ielubrr.'.$BookingTypeInnerJoinTableKey.' = ieubr.RuleID
					INNER JOIN
						INTRANET_EBOOKING_USER_BOOKING_RULE_CUSTOM_GROUP_USER ieubrcgu ON ieubr.RuleID = ieubrcgu.RuleID
					WHERE 
						il.RecordStatus = "1"
					AND
						il.AllowBooking = "1"
					AND
						ieubrcgu.UserID = "'.$UserID.'"
			';
			$rs2_temp = $this->returnResultSet($sql);
			
			$rs2 = array();
			foreach ((array)$rs2_temp as $_rs2_temp){
				$rs2[$_rs2_temp[$BookingTypeID]][$BookingTypeID] = $_rs2_temp[$BookingTypeID];
				if(!$_rs2_temp['DisableBooking']){
					//allow booking(always overwrite disablebooking)
					$rs2[$_rs2_temp[$BookingTypeID]]['DisableBooking'] = $_rs2_temp['DisableBooking'];
					//get the max daysBeforeUser for the allow booking record
					if($_rs2_temp['DaysBeforeUse']==='0'){
						$rs2[$_rs2_temp[$BookingTypeID]]['DaysBeforeUse'] = '0';
					}elseif($rs2[$_rs2_temp[$BookingTypeID]]['DaysBeforeUse'] === '0'){
						//do nth
					}elseif($_rs2_temp['DaysBeforeUse']>$rs2[$_rs2_temp[$BookingTypeID]]['DaysBeforeUse']){
						$rs2[$_rs2_temp[$BookingTypeID]]['DaysBeforeUse'] = $_rs2_temp['DaysBeforeUse'];
					}
				}else{
					if(isset($rs2[$_rs2_temp[$BookingTypeID]]['DisableBooking'])){
						//do nothing
					}else{
						//disable booking
						$rs2[$_rs2_temp[$BookingTypeID]]['DisableBooking'] = $_rs2_temp['DisableBooking'];
					}
				}
			}
			foreach ($AllLocationArr as $_roomID => $_room){
				if($rs[$_roomID]||$rs2[$_roomID]){
					if($rs[$_roomID]['DaysBeforeUse']==='0'||$rs2[$_roomID]['DaysBeforeUse']==='0'){
						$LastDateToBook = '9999';
					}else{
						if($rs[$_roomID]['DaysBeforeUse']>$rs2[$_roomID]['DaysBeforeUse']){
							$LastDateToBook = $rs[$_roomID]['DaysBeforeUse'];
						}else{
							$LastDateToBook= $rs2[$_roomID]['DaysBeforeUse'];
						}
					}
					$LastDateToBook= date('Y-m-d',strtotime($Date)- strtotime("+ ".$LastDateToBook." days",0)); // The Last Date Can book
				}else{
					$LastDateToBook= date('Y-m-d',strtotime($Date)- strtotime("- 1 days",0)); // The Last Date Can book
				}
				
				// 				$LastDateToBook= date('Y-m-d',strtotime($Date)- strtotime("+ ".$LastDateToBook." days",0)); // The Last Date Can book
				
				if(strtotime(date('Y-m-d'))-strtotime($LastDateToBook)>=0){ //Check is Today is after The Last Date Can book
					$check[$Date][$_roomID] = true;
				}else{
					$check[$Date][$_roomID] = false;
				}
				if($rs[$_roomID]['DisableBooking'] && $rs2[$_roomID]['DisableBooking']){
					$check[$Date][$_roomID] = false;
				}
			}
			return $check;
		}
		private function CheckBookingGeneralRule($BookingDetails){
			$Date = $BookingDetails['Date'];
			$StartTime = $BookingDetails['StartTime'];
			$EndTime = $BookingDetails['EndTime'];
			$BookingType = $BookingDetails['BookingType'];
			if($BookingType == 'item'){
				$AllLocationArr = $this->GetAllItem();
				$sql = '
						Select 
							il.ItemID, il.BookingDayBeforehand as DaysBeforeUse, il.AllowBooking, il.AllowBookingIndependence
						From
							INTRANET_EBOOKING_ITEM il
						Where 
							il.RecordStatus = "1"
						';
				$rs = $this->returnResultSet($sql);
				$rs = BuildMultiKeyAssoc($rs,'ItemID');
			}else{
				$AllLocationArr = $this->GetAllLocation();
				$sql = 'Select
							il.LocationID, ielbrr.BookingRuleID, MAX(ielbr.DaysBeforeUse) as DaysBeforeUse, ielbr.RuleID, il.AllowBooking	
						From
							INVENTORY_LOCATION  il
						LEFT JOIN
							INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION ielbrr ON il.LocationID = ielbrr.LocationID
						LEFT JOIN
							INTRANET_EBOOKING_LOCATION_BOOKING_RULE ielbr ON ielbr.RuleID = ielbrr.BookingRuleID
						Where
							il.RecordStatus = "1"
						Group By
							il.LocationID
				';
				$rs =  $this->returnResultSet($sql);
				$rs = BuildMultiKeyAssoc($rs,'LocationID');
			}
			foreach ($AllLocationArr as $_roomID => $_room){
				$DaysBefore = isset($rs[$_roomID])? $rs[$_roomID]['DaysBeforeUse'] : 0;
				$DateBefore = date('Y-m-d',strtotime($Date)- strtotime("+ ".$DaysBefore." days",0)); // The Last Date to book
				
				if(strtotime(date('Y-m-d'))-strtotime($DateBefore)<=0){ //Check is Today is before DateBefore
					$check[$Date][$_roomID] = true;
				}else{
					$check[$Date][$_roomID] = false;
				}
				if($rs[$_roomID]['AllowBooking']){
					//do nth
					if(!$rs[$_roomID]['AllowBookingIndependence'] && $BookingType == 'item'){
						//for item only
						//disable the item which is not allowed booking independence
						$check[$Date][$_roomID] = false;
					}
				}else{
					$check[$Date][$_roomID] = false;
				}
			}
			return $check;
			
		}
		private function CheckBookingRule($BookingDetails){
			$Date = $BookingDetails['Date'];
			$StartTime = $BookingDetails['StartTime'];
			$EndTime = $BookingDetails['EndTime'];
			$BookingType = $BookingDetails['BookingType'];
			if($BookingType == 'item'){
				$AllLocationArr = $this->GetAllItem();
			}else{
				$AllLocationArr = $this->GetAllLocation();
			}
			$UserRule = $this->CheckBookingUserRule($BookingDetails);
			$GeneralRule = $this->CheckBookingGeneralRule($BookingDetails);
			
			foreach ($AllLocationArr as $_roomID => $_room){
				if($UserRule[$Date][$_roomID] && $GeneralRule[$Date][$_roomID]){
					$check[$Date][$_roomID] = true;
				}else{
					$check[$Date][$_roomID] = false;
				}
			}
			return $check;
		}
		#Booking Rule Checking End
		
		###Checking Function End
		
		###Booking Function Start
		private function InsertEbookingRecord($BookingDetails, $UserID, $Attachment, $Remarks, $BookingFrom, $RelatedTo){
			$UserID = $this->UserID;
			if($this->isEJ()){
				$Remarks = iconv('UTF-8','BIG5-HKSCS//IGNORE',$Remarks);
			}
			$Remarks = $this->Get_Safe_Sql_Query(htmlspecialchars($Remarks));
			
			$sql = 'INSERT INTO INTRANET_EBOOKING_RECORD 
						(Date, StartTime, EndTime, Remark, Attachment, RecordStatus, RequestedBy, InputBy, ModifiedBy, DateInput, DateModified, RequestDate, ResponsibleUID, TimeSlotID) 
					VALUES 
						("'.$BookingDetails['Date'].'","'.$BookingDetails['StartTime'].'","'.$BookingDetails['EndTime'].'","'.$Remarks.'","'.$Attachment.'","1","'.$UserID.'","'.$UserID.'","'.$UserID.'",now(),now(),now(),"'.$UserID.'","'.$BookingDetails['TimeSlotID'].'")';
			$this->db_db_query($sql);
			
			//Get Booking ID
			$sql = 'Select 
						BookingID 
					FROM 
						INTRANET_EBOOKING_RECORD 
					ORDER BY BookingID Desc Limit 1';
			$rs = $this->returnResultSet($sql);
			$BookingID = $rs[0]['BookingID'];
			//Update The RelatedTo @ INTRANET_EBOOKING_RECORD
			if(!$RelatedTo){
				$RelatedTo = $BookingID;
			}
			$sql = 'Update 
						INTRANET_EBOOKING_RECORD 
					SET 
						RelatedTo = "'.$RelatedTo.'" 
					Where 
						BookingID = "'.$BookingID.'"';
			$this->db_db_query($sql);
			
			//INSERT Record @ INTRANET_EBOOKING_BOOKING_DETAILS
			if($BookingDetails['RoomID']){
				$AutoApprove= $this->GetAutoApproveSetting($BookingDetails['RoomID']);
				if ($AutoApprove) {
				    $ProcessDate = 'now()';
				    $ProcessDateTime = 'now()';
				}
				else {
				    $ProcessDate = 'Null';
				    $ProcessDateTime = 'Null';
				}
				
				$sql = 'INSERT INTO INTRANET_EBOOKING_BOOKING_DETAILS
						(BookingID, FacilityType, FacilityID, RecordStatus, ProcessDate, BookingStatus, InputBy, ModifiedBy, DateInput, DateModified, BookingFrom, ProcessDateTime)
					VALUES
						("'.$BookingID.'","1","'.$BookingDetails['RoomID'].'","1", '.$ProcessDate.', "'.$AutoApprove.'","'.$UserID.'","'.$UserID.'",now(),now(),"'.$BookingFrom.'", '.$ProcessDateTime.')
					';
				$this->db_db_query($sql);
			}elseif($BookingDetails['ItemID']){
//				$AutoApprove= 0;
				foreach ((array)$BookingDetails['ItemID'] as $itemID){
				    
				    $itemNeedApproval = $this->Check_If_Booking_Need_Approval($UserID, $itemID, $RoomID = '');
				    if ($itemNeedApproval) {
				        $AutoApprove = 0;
				        $ProcessDate = 'Null';
				        $ProcessDateTime = 'Null';
				    }
				    else {
				        $AutoApprove = 1;
				        $ProcessDate = 'now()';
				        $ProcessDateTime = 'now()';
				    }
				    
					$sql = 'INSERT INTO INTRANET_EBOOKING_BOOKING_DETAILS
							(BookingID, FacilityType, FacilityID, RecordStatus, ProcessDate, BookingStatus, InputBy, ModifiedBy, DateInput, DateModified, BookingFrom, ProcessDateTime)
						VALUES
							("'.$BookingID.'","2","'.$itemID.'","1", '.$ProcessDate.',"'.$AutoApprove.'","'.$UserID.'","'.$UserID.'",now(),now(),"'.$BookingFrom.'", '.$ProcessDateTime.')
						';
					$this->db_db_query($sql);
				}
			}
			$result['BookingID'] = $BookingID;
			$result['AutoApprove'] = $AutoApprove;
			return $result;
		}
		private function UpdateEbookingRecord($BookingID,$FieldArr1,$FieldArr2){
			$UserID = $this->UserID;
			if($BookingID){
				if(isset($FieldArr1)){
					$update_statment = '';
					$comma = '';
					foreach ((array)$FieldArr1 as $FieldName => $Values){
						$update_statment .= $comma.$FieldName .'= "'.$Values.'"';
						$comma = ',';
					}
					$update_statment .= ',DateModified = now()';
					$update_statment .= ',ModifiedBy = "'.$UserID.'"';
					$sql = 'UPDATE INTRANET_EBOOKING_RECORD SET '.$update_statment.' WHERE BookingID = "'.$BookingID.'"';
					$respond[1] = $this->db_db_query($sql);
				}
				if(isset($FieldArr2)){
					$update_statment = '';
					$comma = '';
					foreach ((array)$FieldArr2 as $FieldName => $Values){
						$update_statment .= $comma.$FieldName .'= '.$Values;
						$comma = ',';
					}
					$update_statment .= ',DateModified = now()';
					$update_statment .= ',ModifiedBy = "'.$UserID.'"';
					
					$sql = 'UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET '.$update_statment.' WHERE BookingID = "'.$BookingID.'"';
					$respond[2] = $this->db_db_query($sql);
				}
				return $respond;
			}else{
				return false;
			}
		
		}
		
		###Booking Function End
		
		###Get AutoApprove Start: for room only \
		private function GetAutoApproveSetting($RoomID){
		    
		    $isNeedApprove = $this->Check_If_Booking_Need_Approval($this->UserID, $ItemID = '', $RoomID);
		    $AutoApprove= $isNeedApprove ? '0' : '1';
            return $AutoApprove;
		    
// 			$Date = $BookingDetails['Date'];
// 			$StartTime = $BookingDetails['StartTime'];
// 			$EndTime = $BookingDetails['EndTime'];
			
// 			$UserID = $this->UserID;
// 			$UserYearID = $this->UserInfo['YearID'];
// 			$UserRecordType = $this->UserInfo['RecordType'];
// 			if($UserYearID){
// 				$UserYearIDCond = ' AND ieubrtu.YearID="'.$UserYearID.'"';
// 			}else{
// 				$UserYearIDCond = '';
// 			}
// 			#Type 1,2,3
// 			$sql = 'Select
// 						il.LocationID, MAX(ieubr.NeedApproval) as NeedApproval
// 					From
// 						INVENTORY_LOCATION  il
// 					INNER JOIN
// 						INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION ielubrr ON ielubrr.LocationID = il.LocationID
// 					INNER JOIN
// 						INTRANET_EBOOKING_USER_BOOKING_RULE ieubr ON ielubrr.UserBookingRuleID = ieubr.RuleID
// 					INNER JOIN
// 						INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER ieubrtu ON ieubr.RuleID = ieubrtu.RuleID
// 					WHERE
// 						il.RecordStatus = "1"
// 					AND
// 						il.AllowBooking = "1"
// 					AND
// 						ieubrtu.UserType = "'.$UserRecordType.'"
// 					AND
// 						il.LocationID = "'.$RoomID.'"
// 					'.$UserYearIDCond.'
// 					GROUP BY
// 						il.LocationID
// 					';
// 			$rs =  $this->returnResultSet($sql);
// 			$rs = BuildMultiKeyAssoc($rs,'LocationID');
			
// 			#Type 4
// 			$sql = 'Select
// 						il.LocationID,  MAX(ieubr.NeedApproval) as NeedApproval
// 					From
// 						INVENTORY_LOCATION  il
// 					INNER JOIN
// 						INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION ielubrr ON ielubrr.LocationID = il.LocationID
// 					INNER JOIN
// 						INTRANET_EBOOKING_USER_BOOKING_RULE ieubr ON ielubrr.UserBookingRuleID = ieubr.RuleID
// 					INNER JOIN
// 						INTRANET_EBOOKING_USER_BOOKING_RULE_CUSTOM_GROUP_USER ieubrcgu ON ieubr.RuleID = ieubrcgu.RuleID
// 					WHERE
// 						il.RecordStatus = "1"
// 					AND
// 						il.AllowBooking = "1"
// 					AND
// 						ieubrcgu.UserID = "'.$UserID.'"
// 					AND
// 						il.LocationID = "'.$RoomID.'"
// 					GROUP BY
// 						il.LocationID
// ';
// 			$rs2 = $this->returnResultSet($sql);
// 			$rs2 = BuildMultiKeyAssoc($rs2,'LocationID');
			
// 			if($rs[$RoomID]['NeedApproval']||$rs2[$RoomID]['NeedApproval']){
// 				$AutoApprove= '0';
// 			}else{
// 				$AutoApprove = '1';
// 			}
// 			return $AutoApprove;
			
		}
		###Get AutoApprove End
		
		###Get MyBookingRecord Start
		private function GetUserBookingRecord($BookingID){
			if($BookingID){
				$cond = ' AND ier.BookingID = '.$BookingID.' ';
			}
			$sql = "SELECT
					ier.BookingID,
					ier.Date,
					ier.StartTime,
					ier.EndTime,
					ier.Remark,
					ier.InputBy,
					ier.RequestedBy,
					ier.Remark,
					iebd.FacilityID,
					iebd.FacilityType,
					iebd.BookingStatus
			FROM
				INTRANET_EBOOKING_RECORD ier
			INNER JOIN
				INTRANET_EBOOKING_BOOKING_DETAILS iebd
					ON ier.BookingID = iebd.BookingID
			WHERE
				(ier.InputBy = ".$this->UserID ." OR ier.RequestedBy = ".$this->UserID .")
			AND
				ier.Date >= DATE_FORMAT(Now(),'%Y-%m-%d')
			AND
				ier.RecordStatus = '1'
				$cond
			ORDER BY
				ier.RelatedTo, ier.BookingID desc
			";
			$recordTemp = $this->returnResultSet($sql);
			$AllLocationArr = $this->GetAllLocation();
			$AllItemArr = $this->GetAllItem();
			foreach ((array)$recordTemp as $index => $_recordTemp){
				$locationInfo = $AllLocationArr[$_recordTemp['FacilityID']];
				$itemInfo = $AllItemArr[$_recordTemp['FacilityID']];
				$userByInfo = $this->GetUserInfo($_recordTemp['InputBy']);
				$requestedByInfo = $this->GetUserInfo( $_recordTemp['RequestedBy']);
				if($_recordTemp['FacilityType']=='2'){
					//item
					$recordData[$index]['room'] = Get_Lang_Selection($itemInfo['NameChi'], $itemInfo['NameEng']);
				}else{
					//room
					$recordData[$index]['room'] ='';
					if($this->EbookingSetting['DisplayBuildingName']){
						$recordData[$index]['room'] .= Get_Lang_Selection($locationInfo['BuildingNameChi'], $locationInfo['BuildingNameEng']).' > ';
					}
					if($this->EbookingSetting['DisplayFloorName']){
						$recordData[$index]['room'] .= Get_Lang_Selection($locationInfo['LevelNameChi'], $locationInfo['LevelNameEng']).' > ';
					}
					$recordData[$index]['room'] .= Get_Lang_Selection($locationInfo['NameChi'], $locationInfo['NameEng']);
				}
				$recordData[$index]['date'] =  $_recordTemp['Date'];
				$recordData[$index]['datetime'] =  $_recordTemp['Date'].'<br>'.date("H:i",strtotime($_recordTemp['StartTime']) ).'-'. date("H:i",strtotime($_recordTemp['EndTime']) );
				$recordData[$index]['time'] =  date("H:i",strtotime($_recordTemp['StartTime']) ).'-'. date("H:i",strtotime($_recordTemp['EndTime']) );
				$recordData[$index]['bookingBy'] =  Get_Lang_Selection($requestedByInfo['ChineseName'], $requestedByInfo['EnglishName']);
				$recordData[$index]['status'] =  $_recordTemp['BookingStatus'];
				if($this->isEJ()){
				    $recordData[$index]['remarks'] = iconv('BIG5-HKSCS','UTF8//IGNORE',$_recordTemp['Remark']);
				}else{
				    $recordData[$index]['remarks'] = $_recordTemp['Remark'];
				}
				$recordData[$index]['bookingID'] =  $_recordTemp['BookingID'];
				$recordData[$index]['startTime'] = $_recordTemp['StartTime'];
				$recordData[$index]['endTime'] = $_recordTemp['EndTime'];
				if($_recordTemp['FacilityType']=='2'){
					$recordData[$index]['itemID'] = $_recordTemp['FacilityID'];
				}else{
					$recordData[$index]['locationID'] = $_recordTemp['FacilityID'];
				}
				$recordData[$index]['facilityType'] = $_recordTemp['FacilityType'];
				
			}
			return $recordData;
		}
		###Get MyBookingRecord END
		
		private function GetTimeslotData($DateArr){
		    global $sys_custom;
		    
			###Cross TimesZone Checking
			$sql = 'Select
							PeriodID, PeriodStart, PeriodEND
						FROM
							INTRANET_CYCLE_GENERATION_PERIOD
						';
			$periodAsso = $this->returnResultSet($sql);
			
			###Special Date
			foreach ((array)$DateArr as $Date){
				$sql = 'Select
							SpecialDate
						From
							INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE  itst
						WHERE
							"'.$Date.'" = itst.SpecialDate';
				$rs = $this->returnResultSet($sql);
				if($rs[0]['SpecialDate']){
					$DateArrUsingSpecialTimeTable[] = $rs[0]['SpecialDate'];
				}
			}
			
			foreach ((array)$DateArr as $Date){
				foreach ((array)$periodAsso as $_periodAsso){
					if(strtotime($Date)<=strtotime($_periodAsso['PeriodEND']) && strtotime($_periodAsso['PeriodStart'])<=strtotime($Date) && !in_array($Date,(array)$DateArrUsingSpecialTimeTable)){
						$currentPeriodID = $_periodAsso['PeriodID'];
						break;
					}
				}
				
				if(isset($previousPeriodID) && $previousPeriodID != $currentPeriodID){
					return false; //cross time zone
				}
				$previousPeriodID = $currentPeriodID;
			}

			if ($sys_custom['eBooking']['EverydayTimetable']) {
			    $timeslotDataAry = array();
			    foreach ((array)$DateArr as $_date){
			        
			        // 1. check suspension date
			        $sql ='Select RelatedSubLocationID, RelatedItemID From INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION WHERE RecordDate = "'.$_date.'"';
			        $suspensionDateAry = $this->returnResultSet($sql);
			        if (count($suspensionDateAry) == 1) {
			            if (($suspensionDateAry[0]['RelatedSubLocationID'] == 0) && ($suspensionDateAry[0]['RelatedItemID'] == 0)) { // no specific room/item suspension, suspension all in the date
                            return false;			                
			            }
			        }
			        
			        // 2. check "Skip School Day" in holiday/event
			        $sql = "SELECT
            			        EventDate
        			        FROM
            			        INTRANET_EVENT
        			        WHERE
            			        RecordStatus='1'
            			        AND isSkipCycle=1
            			        AND EventDate='$_date'";
			        $skipSchoolDayAry = $this->returnResultSet($sql);
			        if (count($skipSchoolDayAry) > 0) {
			            return false;
			        }
			        
			        // 3. get timeslot from everyday timetable
			        $sql = "SELECT
                                    r.TimetableID,
                                    s.TimeSlotID,
                                    s.TimeSlotName,
                                    s.StartTime,
                                    s.EndTime
                            FROM
                                    INTRANET_TIMETABLE_TIMESLOT s
        	                INNER JOIN
        				            INTRANET_TIMETABLE_TIMESLOT_RELATION r ON (r.TimeSlotID = s.TimeSlotID)
        	                INNER JOIN
        				            INTRANET_SCHOOL_EVERYDAY_TIMETABLE t ON (IF(t.TimetableID>0,t.TimetableID,t.DefaultTimetableID)=r.TimetableID)
        				    WHERE
        				            t.RecordDate='".$_date."'
                                    ORDER BY s.StartTime";
			        $timeSlotInfoAry = $this->returnResultSet($sql);
			        $timeSlotInfoAssoc = BuildMultiKeyAssoc($timeSlotInfoAry, array('TimetableID', 'TimeSlotID'));
			        
			        foreach((array) $timeSlotInfoAssoc as $_timetableID=>$_timeSlotInfo) {
			            if (!isset($timeslotDataAry[$_timetableID])) {
			                $timeslotDataAry[$_timetableID] = $_timeSlotInfo;
			            }
			        }
			    }
			    return $timeslotDataAry;
			}
			
			//general timeslot
			if($currentPeriodID){
				$sql = 'SELECT 
	
						DISTINCT
							ittr.TimetableID, itt.TimeSlotID, itt.TimeSlotName, itt.StartTime, itt.EndTime
	
						FROM
							INTRANET_PERIOD_TIMETABLE_RELATION iptr
						INNER JOIN 
							INTRANET_TIMETABLE_TIMESLOT_RELATION ittr
								ON	ittr.TimetableID = iptr.TimetableID
						INNER JOIN
							INTRANET_TIMETABLE_TIMESLOT itt
								ON ittr.TimeSlotID = itt.TimeSlotID
						WHERE
							iptr.PeriodID = "'.$currentPeriodID.'"
						ORDER BY 
							itt.StartTime
						';
				$rs = $this->returnResultSet($sql);
				$dataAsso = BuildMultiKeyAssoc($rs, 'TimeSlotID');
				if(!empty($dataAsso)){
					$TimeslotData[$rs[0]['TimetableID']] = $dataAsso;
				}
			}
			
			if(!empty($DateArrUsingSpecialTimeTable) ){
				foreach ((array)$DateArrUsingSpecialTimeTable as $_DateUsingSpecialTimeTable){
					$sql = 'Select
								ittr.TimetableID, itt.TimeSlotName, itt.TimeSlotID, itt.StartTime, itt.EndTime
							From
								INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS itsts
							INNER JOIN
								INTRANET_TIMETABLE_TIMESLOT_RELATION ittr
									on ittr.TimetableID = itsts.TimetableID
							INNER JOIN
								INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE  itst
									on itsts.SpecialTimetableSettingsID = itst.SpecialTimetableSettingsID
							INNER JOIN
								INTRANET_TIMETABLE_TIMESLOT itt
									ON itt.TimeSlotID = ittr.TimeSlotID
							WHERE
								"'.$_DateUsingSpecialTimeTable.'" = itst.SpecialDate
					';

					$rs = $this->returnResultSet($sql);
					if(!empty($rs)){
						$TimeslotData[$rs[0]['TimetableID']] = BuildMultiKeyAssoc($rs, 'TimeSlotID');
					}
				}
			}
			
			return $TimeslotData;
		}
		
		private function GetTimeslotDataEJ(){
			$sql = "SELECT batch.BatchID as TimetableID,
							slot.SlotID as TimeSlotID,
							slot.Title as TimeSlotName, 
							slot.TimeRange 
					FROM INTRANET_SLOT_BATCH as batch
					INNER JOIN INTRANET_SLOT as slot on slot.BatchID = batch.BatchID
					WHERE batch.RecordStatus = 1
					ORDER BY SlotSeq";
			$rs = $this->returnResultSet($sql);
			if(!empty($rs)){
				foreach((array)$rs as $_key => $_val){
					$_timeInfoArr = $this->GetTimeByTimeRangeForEJ($_val['TimeRange']);
					$rs[$_key]['StartTime'] = $_timeInfoArr['StartTime'];
					$rs[$_key]['EndTime'] = $_timeInfoArr['EndTime'];
					$rs[$_key]['TimeSlotName'] = convert2unicode($_val['TimeSlotName'],1,1);
				}
				$TimeslotData[$rs[0]['TimetableID']] = BuildMultiKeyAssoc($rs, 'TimeSlotID');
			}
			return $TimeslotData;
		}
		
		private function GetTimeByTimeRangeForEJ($timeRangeString){
			$_timeInfoArr = explode('-',$timeRangeString);
			$result['StartTime'] = trim($_timeInfoArr[0]).':00';
			$result['EndTime'] = trim($_timeInfoArr[1]).':00';
			return $result;
		}
	### Private Function End
	
	### Public function Start
	public function GetAvailableRoom($BookingDetails){
	
		$Date = $BookingDetails['Date'];
		$StartTime = $BookingDetails['StartTime'];
		$EndTime = $BookingDetails['EndTime'];
		$BookingType = $BookingDetails['BookingType'];
		$PeriodCheck = $this->CheckAvailableOverallPeriod($BookingDetails);
		$IsBookedCheck = $this->CheckIsBookedRoom($BookingDetails);
		$BookingRuleCheck = $this->CheckBookingRule($BookingDetails);

		if($BookingType == 'item'){
		    $AllLocationArr = $this->GetAllItem();
		    $FacilityType = LIBEBOOKING_FACILITY_TYPE_ITEM;
		}else{
		    $AllLocationArr = $this->GetAllLocation();
		    $FacilityType = LIBEBOOKING_FACILITY_TYPE_ROOM;
		}
		foreach ($AllLocationArr as $_roomID => $_room){
// 			if($PeriodCheck[$Date][$_roomID] && $IsBookedCheck[$Date][$_roomID] && $BookingRuleCheck[$Date][$_roomID]){
		    if($IsBookedCheck[$Date][$_roomID]  && $PeriodCheck[$Date][$_roomID] && $BookingRuleCheck[$Date][$_roomID] &&
		        $this->Check_If_Facility_Is_Available($FacilityType, $_roomID, $Date, $StartTime, $EndTime)
		       ){
		        $check[$Date][$_roomID]['BookAvailable'] = true;
			}else{
				$check[$Date][$_roomID]['BookAvailable']= false;
				if(!$PeriodCheck[$Date][$_roomID]){
					$check[$Date][$_roomID]['ErrorCode'][] = '101'; //Unavialable Period
				}elseif(!$IsBookedCheck[$Date][$_roomID]){
					$check[$Date][$_roomID]['ErrorCode'][] = '201'; //Booked Room
				}elseif(!$BookingRuleCheck[$Date][$_roomID]){
					$check[$Date][$_roomID]['ErrorCode'][] = '301'; //Booking Rule
				}else{
					$check[$Date][$_roomID]['ErrorCode'][] = '999'; //unknown error
				}
			}
		}

		return $check;
	}
	public function RequestBooking($Request){
		global $intranet_root, $PATH_WRT_ROOT, $Lang;
		$this->Load_General_Setting();
		$BookingDetails = $Request['BookingDetails'];
// 		$Attachment = $Request['UserID'];
		$UserID = $Request['UserID'];
		$Remarks = $Request['Remarks'];
		$BookingFrom = $Request['BookingFrom'];
		
		$emailBookingIdAry = array();
		$emailApproveIdAry = array();
		foreach ((array)$BookingDetails as $key=> $Each_BookingDetail){
			$check = $this->GetAvailableRoom($Each_BookingDetail);
			if($Each_BookingDetail['BookingType']=='item'){
				$FacilityID = $Each_BookingDetail['ItemID'];
				$FacilityAvailCheck = true;
				foreach ((array)$FacilityID as $_FacilityID){
					if(!$check[$Each_BookingDetail['Date']][$_FacilityID]['BookAvailable']){
						$FacilityAvailCheck = false;
					}
				}
			}else{
				$FacilityID = $Each_BookingDetail['RoomID'];
				if($check[$Each_BookingDetail['Date']][$FacilityID]['BookAvailable']){
					$FacilityAvailCheck = true;
				}else{
					$FacilityAvailCheck = false;
				}
			}
			if($FacilityAvailCheck){
				//Booking 
				$rs = $this->InsertEbookingRecord($Each_BookingDetail, $UserID, $Attachment, $Remarks, $BookingFrom, $RelatedTo);
				$BookingID = $rs['BookingID'];
				$AutoApprove = $rs['AutoApprove'];
				$Respond[$key]['BookingID'] = $BookingID;
				$Respond[$key]['Status'] = '1';
				$Respond[$key]['Error'] = '';
				if(!$RelatedTo){
					$RelatedTo  = $BookingID;
				}
				if($BookingID&& $AutoApprove){
					$Respond[$key]['emailBookingIDArr']= $BookingID;
				}elseif($this->SettingArr['ApprovalNotification'] && $BookingID){
					$Respond[$key]['emailApprovedIDArr'] = $BookingID;
				}
			}else{
				//Return Error
				$Respond[$key]['BookingID'] = '';
				$Respond[$key]['Status'] = '0';
				$Respond[$key]['Error'] = $check[$Each_BookingDetail['Date']][$Each_BookingDetail['RoomID']]['ErrorCode'];
			}
		}
		return $Respond;
	}
	public function RequestDeleteBooking($BookingID){
		$FieldArr1 = array('RecordStatus'=>'0'); //For INTRANET_EBOOKING_RECORD
		$FieldArr2 = array('RecordStatus'=>'0'); //For INTRANET_EBOOKING_BOOKING_DETAIL
		return $this->UpdateEbookingRecord($BookingID,$FieldArr1,$FieldArr2);
		
	}
	
	public function ReturnAllLocation($AllowBooking=true){
		$AllLocationArr = $this->GetAllLocation();
		if($AllowBooking){
			foreach ((array)$AllLocationArr as $key => $_AllLocationArr){
				if(!$_AllLocationArr['AllowBooking']){
					unset($AllLocationArr[$key]);
				}
			}
		}
		return $AllLocationArr;
	}
	public function ReturnAllItem($AllowBooking=true){
		$AllItemArr = $this->GetAllItem();
		if(!$this->EbookingSetting['AllowToBookRepairingItem'] || !$this->EbookingSetting['AllowToBookDamagedItem']){
			//allowrepair // damage item checking
			$sql = 'SELECT 
						DISTINCT 
							t1.ItemID, t1.NewStatus
					FROM INVENTORY_ITEM_SINGLE_STATUS_LOG t1
						LEFT OUTER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG  t2
							ON (t1.ItemID= t2.ItemID AND t1.DateModified < t2.DateModified)
					WHERE 
						t2.ItemID IS NULL 
					ORDER BY 
						t1.itemID;';
			$rs = $this->returnResultSet($sql);
			$itemStatusArr = BuildMultiKeyAssoc($rs, 'ItemID','NewStatus',1);
		}
		if($AllowBooking){
			foreach ((array)$AllItemArr as $itemID => $_AllItemArr){
				if(!($_AllItemArr['AllowBooking']  && $_AllItemArr['AllowBookingIndependence']) ){
					unset($AllItemArr[$itemID]);
				}
				if(!$this->EbookingSetting['AllowToBookDamagedItem'] && $itemStatusArr[$itemID]=='2'){
					unset($AllItemArr[$itemID]);
				}
				if(!$this->EbookingSetting['AllowToBookRepairingItem'] &&  $itemStatusArr[$_AllItemArr['eInventoryItemID']]=='3'){
					unset($AllItemArr[$itemID]);
				}
			}
		}
		unset($itemStatusArr);
		unset($rs);
		return $AllItemArr;
	}
	public function ReturnLocationIdFromBookingID($BookingID){
		$LocationInfo = $this->GetLocationIdFromBookingID($BookingID);
		return $LocationInfo[0];
	}
	
	public function ReturnUserBookingRecord($BookingID){
		return $this->GetUserBookingRecord($BookingID);
	}
	
	public function RequestUpdateBookingDetails($BookingID, $Remarks, $Attachment){
		if($this->isEJ()){
			$Remarks = iconv('UTF-8','BIG5-HKSCS//IGNORE',$Remarks);
		}
		$Remarks = $this->Get_Safe_Sql_Query(htmlspecialchars($Remarks));
		$FieldArr1 = array('Remark'=>$Remarks); //For INTRANET_EBOOKING_RECORD
		$FieldArr2 = array(); //For INTRANET_EBOOKING_BOOKING_DETAIL
		return  $this->UpdateEbookingRecord($BookingID,$FieldArr1,$FieldArr2);
	}
	public function RequestSendingEmail($EmailBookingIDAry, $EmailApproveIDAry){
	    $resultAry = array();
	    
// 		send email handling
		if (count($EmailBookingIDAry) > 0) {
			// return true even failed to send email
		    $resultAry[] = $this->Email_Booking_Result_Merged($EmailBookingIDAry);
		}
		if (count($EmailBookingIDAry) > 0) {
			// return true even failed to send email
		    $resultAry[] = $this->SendEmailToFollowUpGroup_Merged($EmailBookingIDAry);
		}
		if (count($EmailApproveIDAry) > 0) {
			// return true even failed to send email
		    $resultAry[] = $this->SendMailToManageGroup_Merged($EmailApproveIDAry);
		}
		
		return in_array(false, $resultAry) ? false : true;
	}
	public function ReturnTimeslotDetail($TimeslotArr){
		if($this->isEJ()){
			
			$sql = 'SELECT
						SlotID as TimeSlotID, Title as TimeSlotName, TimeRange
					FROM
						INTRANET_SLOT
					WHERE
						SlotID in ("'.implode('","',(array)$TimeslotArr).'");
					';
			$rs = $this->returnResultSet($sql);
			if(!empty($rs)){
				foreach ($rs as $_key => $_val){
					$_timeInfoArr = $this->GetTimeByTimeRangeForEJ($_val['TimeRange']);
					$rs[$_key]['StartTime'] = $_timeInfoArr['StartTime'];
					$rs[$_key]['EndTime'] = $_timeInfoArr['EndTime'];
					$rs[$_key]['TimeSlotName'] = convert2unicode($_val['TimeSlotName'],1,1);
				}
				return $rs;
			}
		}else{
			$sql = 'SELECT 
						TimeSlotID, TimeSlotName, StartTime, EndTime
					FROM 
						INTRANET_TIMETABLE_TIMESLOT
					WHERE
						TimeSlotID in ("'.implode('","',(array)$TimeslotArr).'");
					';
			return BuildMultiKeyAssoc($this->returnResultSet($sql), 'TimeSlotID');
		}
	}
	public function ReturnEbookingSetting(){
		return $this->GetEbookingSetting();
	}
	public function ReturnTimeslotData($DateArr){
		if($this->isEJ()){
			return $this->GetTimeslotDataEJ($DateArr);
		}else{
			return $this->GetTimeslotData($DateArr);
		}
	}
	### Public function END
	
}
?>