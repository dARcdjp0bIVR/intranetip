<?php
// editing by  
/*****************************************************************************
 * Modification Log:
 *		Date : 2017-11-21 Frankie
 *		-	modify getSLRSExchangeInfoForTimeTable() for Timetable 
 * 
 * 		Date : 2017-02-08 Frankie
 * 		-	Add getSLRSInfoForTimeTable for Table Time  
 * 
 *****************************************************************************/

if (!defined("LIBSLRS_UI_DEFINED")) {
	define("LIBSLRS_UI_DEFINED", true);
	
	class libslrs_ui extends interface_html {
		
		function libslrs_ui($parTemplate='') {
			$template = ($parTemplate=='')? 'default.html' : $parTemplate;
			$this->interface_html($template);
		}
		
		function echoModuleLayoutStart($parPageCode, $parReturnMsg='', $parForPopup=false) {
			global $indexVar, $CurrentPage, $MODULE_OBJ, $TAGS_OBJ, $CurrentPageArr, $PATH_WRT_ROOT, $intranet_session_language;
			
			$CurrentPage = $parPageCode;
			$MODULE_OBJ = $indexVar['libslrs']->getModuleObjArr();
			$CurrentPageArr['eAdminSLRS'] = 1;
			
			if ($parForPopup) {
				$this->interface_html('popup.html');
			}
			
			$this->LAYOUT_START($parReturnMsg);
			echo '<!-- task: '.$indexVar['taskScript'].'-->';
			echo '<!-- template: '.$indexVar['templateScript'].'-->';
		}
		
		function echoModuleLayoutStop() {
			$this->LAYOUT_STOP();
		}
		
		
		function getSLRSCancelInfoForTimeTable($SLRS_CancelInfos = array())
		{
		    global $Lang, $intranet_session_language, $PATH_WRT_ROOT, $junior_mck;
		    $htmlTable = "";
		    if ($junior_mck) {
		        $htmlTable .= "** " . convert2Big5($Lang['SLRS']['CancelLessonDes']['msg']["alreadyCancel"]) . " **";
		    } else {
		        $htmlTable .= "** " . $Lang['SLRS']['CancelLessonDes']['msg']["alreadyCancel"]. " **";
		    }
		    if (count($SLRS_CancelInfos) > 0)
		    {
		        $htmlTable .= '<table id="ContentSLRSTable_' . $thisTimeSlotID . '" class="common_table_list"><tbody>';
		        foreach ($SLRS_CancelInfos as $key => $val)
		        {
		            $htmlTable .= '<tr>';
		            $htmlTable .= '<td style="width:50%; padding:3px;">';
		            $htmlTable .= '<div>';
		            $htmlTable .= '<div class="SubjectGroupNameDisplayDiv"><span class="SubjectNameDisplaySpan">'.$val["Subject"].'</span> - <span class="SubjectGroupNameDisplaySpan">' . $val["LessonInfo"] . '</span></div>'."\n";
		            $htmlTable .= '<div class="StaffNameDisplayDiv">'.$val["TeacherName"].'</div>'."\n";
		            $htmlTable .= '<div class="RemarkDiv" style="font-style:italic">*** '. str_replace("\n", "<br>", $val["CancelRemark"]).'</div>'."\n";
		            $htmlTable .= '</div>';
		            $htmlTable .= '</td>';
		            $htmlTable .= '</tr>';
		        }
		        $htmlTable .= '</tbody></table>';
		    }
		    
		    if (!empty($htmlTable)) {
		        return "<div style='padding:2px; background-color:#ffb4c9; border:1px solid #CCC'>" . $htmlTable . "</div>";
		    } else {
		        return "";
		    }
		}
		
		function getSLRSInfoForTimeTable($SLRS_ArrangeInfos = array(), $SubjectGroupInfoArr = array(), $LocationInfoArr = array(), $SubjectGroupID_DisplayFilterArr = array(), $LocationID_DisplayFilterArr = array(), $OthersLocationFilter = array()) {
			global $Lang, $intranet_session_language, $PATH_WRT_ROOT, $junior_mck;
			
			if (!isset($Lang["SLRS"])) {
				include_once($PATH_WRT_ROOT."lang/slrs_lang.$intranet_session_language.php");
			}
			$htmlTable = "";
			if (count($SLRS_ArrangeInfos) > 0) {
				foreach ($SLRS_ArrangeInfos as $SLRS_ArrangeInfo) {
					$thisLocationDisplay = "";
					# Subject Info
					$thisSubjectGroupID = $SLRS_ArrangeInfo["SubjectGroupID"];
					$thisSubjectNameEN = $SubjectGroupInfoArr[$thisSubjectGroupID]['SubjectDescEN'];
					$thisSubjectNameB5 = $SubjectGroupInfoArr[$thisSubjectGroupID]['SubjectDescB5'];
					$thisSubjectName = Get_Lang_Selection($thisSubjectNameB5, $thisSubjectNameEN);
					$thisTimeSlotID = $SLRS_ArrangeInfo['TimeSlotID'];
					$cycDate = $SLRS_ArrangeInfo['cycDate'];
					
					# Subject Group Info
					$thisSubjectGroupNameEN = $SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleEN'];
					$thisSubjectGroupNameB5 = $SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleB5'];
					$thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupNameB5, $thisSubjectGroupNameEN);
					$thisNumOfStudents = ($SubjectGroupInfoArr[$thisSubjectGroupID]['Total']=='')? 0 : $SubjectGroupInfoArr[$thisSubjectGroupID]['Total'];
					
					//$thisSubjectGroupDisplay = $thisSubjectName." - ".$thisSubjectGroupName;
					$thisSubjectGroupDisplay = '';
					$thisSubjectGroupDisplay .= '<span class="SubjectNameDisplaySpan">';
					$thisSubjectGroupDisplay .= $thisSubjectName." - ";
					$thisSubjectGroupDisplay .= '</span>';
					$thisSubjectGroupDisplay .= '<span class="SubjectGroupNameDisplaySpan">';
					$thisSubjectGroupDisplay .= $thisSubjectGroupName;
					$thisSubjectGroupDisplay .= '</span>';
					$thisSubjectGroupDisplay .= '<span class="SubjectGroupNumOfStudentDisplaySpan">';
					$thisSubjectGroupDisplay .= " ($thisNumOfStudents)";
					$thisSubjectGroupDisplay .= '</span>';
					# no edit link
					$thisRoomAllocationDisplay = $thisSubjectGroupDisplay;
						
					
					# Teachers Info
					if ($SLRS_ArrangeInfo["ArrangedTo_UserID"] == -999) {
						if ($junior_mck) {
							$thisStaffNameDisplay = "<span style='color:#909090;'>( " . convert2Big5($Lang['SLRS']['SubstitutionArrangementDes']['noSubstitutedTeacher']) . " )</span>";
						} else {
							$thisStaffNameDisplay = "<span style='color:#909090;'>( " . $Lang['SLRS']['SubstitutionArrangementDes']['noSubstitutedTeacher'] . " )</span>";
						}
					} else {
						if ($junior_mck) {
							$thisStaffNameDisplay = convert2Big5($Lang['SLRS']['ReportAssignedTo']) . ": " . Get_Lang_Selection($SLRS_ArrangeInfo["ArrChineseName"], $SLRS_ArrangeInfo["ArrEnglishName"]) . "";
						} else {
							$thisStaffNameDisplay = $Lang['SLRS']['ReportAssignedTo'] . ": " . Get_Lang_Selection($SLRS_ArrangeInfo["ArrChineseName"], $SLRS_ArrangeInfo["ArrEnglishName"]) . "";
						}
					}
					# Location Info
					$diffLocation = false;
					if ($SLRS_ArrangeInfo['LocationID'] != $SLRS_ArrangeInfo['ArrangedTo_LocationID']) {
						$thisLocationID = $SLRS_ArrangeInfo['ArrangedTo_LocationID'];
						$diffLocation = true;
					} else {
						$thisLocationID = $SLRS_ArrangeInfo['LocationID'];
					}
						
					if ($thisLocationID != 0)
					{
						if (isset($LocationInfoArr[$thisLocationID])) {
							$thisLocationInfoArr = $LocationInfoArr[$thisLocationID];
						} else {
							$thisLocationInfoArr = $SLRS_ArrangeInfo["locationInfo"][$thisLocationID];
						}
						$thisBuildingName = Get_Lang_Selection($thisLocationInfoArr['BuildingNameChi'], $thisLocationInfoArr['BuildingNameEng']);
						$thisFloorName = Get_Lang_Selection($thisLocationInfoArr['FloorNameChi'], $thisLocationInfoArr['FloorNameEng']);
						$thisRoomName = Get_Lang_Selection($thisLocationInfoArr['RoomNameChi'], $thisLocationInfoArr['RoomNameEng']);
						//$thisLocationDisplay = $thisBuildingName." > ".$thisFloorName." > ".$thisRoomName;
						$thisLocationDisplay .= '<span class="BuildingNameDisplaySpan">';
						$thisLocationDisplay .= $thisBuildingName." > ";
						$thisLocationDisplay .= '</span>';
						$thisLocationDisplay .= '<span class="FloorNameDisplaySpan">';
						$thisLocationDisplay .= $thisFloorName." > ";
						$thisLocationDisplay .= '</span>';
						$thisLocationDisplay .= '<span class="RoomNameDisplaySpan">';
						$thisLocationDisplay .= $thisRoomName;
						$thisLocationDisplay .= '</span>';
					}
					if ($junior_mck) {
						$htmlTable .= "** " . convert2Big5($Lang['SLRS']['SubstitutionArrangement']) . " **";
					} else {
						$htmlTable .= "** " . $Lang['SLRS']['SubstitutionArrangement'] . " **";
					}
					$htmlTable .= '<table id="ContentSLRSTable_' . $thisTimeSlotID . '" class="common_table_list">';
					$htmlTable .= '<tbody><tr>';
					$htmlTable .= '<td style="width:50%; padding:3px;">';
					$htmlTable .= '<div>';
					$htmlTable .= '<div class="SubjectGroupNameDisplayDiv">'.$thisRoomAllocationDisplay.'</div>'."\n";
					if ($diffLocation) {
						$htmlTable .= '<div class="LocationDisplayDiv" style="color:#ff8800;">' . $thisLocationDisplay.'</div>'."\n";
					} else {
						$htmlTable .= '<div class="LocationDisplayDiv">'.$thisLocationDisplay.'</div>'."\n";
					}
					$htmlTable .= '<div class="StaffNameDisplayDiv">'.$thisStaffNameDisplay.'</div>'."\n";
					$htmlTable .= '<div class="StaffUserLoginDisplayDiv" style="display:none;">'.$thisStaffUserLoginDisplay.'</div>'."\n";
					$htmlTable .= '</div>';
					$htmlTable .= '</td>';
					$htmlTable .= '</tr></tbody></table>';
				}
			}
			if (!empty($htmlTable)) {
				return "<div style='padding:2px; background-color:#add8e6; border:1px solid #CCC'>" . $htmlTable . "</div>";
			} else {
				return "";
			}
		}
		
		function getSLRSExchangeInfoForTimeTable($SLRS_ExchangeInfos = array(), $SubjectGroupInfoArr = array(), $LocationInfoArr = array(), $SubjectGroupID_DisplayFilterArr = array(), $LocationID_DisplayFilterArr = array(), $OthersLocationFilter = array(), $ignoreColor = false) {
			global $Lang, $intranet_session_language, $PATH_WRT_ROOT, $junior_mck;
				
			if (!isset($Lang["SLRS"])) {
				include_once($PATH_WRT_ROOT."lang/slrs_lang.$intranet_session_language.php");
			}
			
			$htmlTable = "";
			if (count($SLRS_ExchangeInfos) > 0) {
				foreach ($SLRS_ExchangeInfos as $SLRS_ExchangeInfo) {
					# Subject Info
					$thisLocationDisplay = "";
					$thisSubjectGroupID = $SLRS_ExchangeInfo["SubjectGroupID"];
					
					$thisSubjectNameEN = $SubjectGroupInfoArr[$thisSubjectGroupID]['SubjectDescEN'];
					$thisSubjectNameB5 = $SubjectGroupInfoArr[$thisSubjectGroupID]['SubjectDescB5'];
					$thisSubjectName = Get_Lang_Selection($thisSubjectNameB5, $thisSubjectNameEN);
					
					$thisTimeSlotID = $SLRS_ExchangeInfo['TimeSlotID'];
					$cycDate = $SLRS_ExchangeInfo['cycDate'];
						
					# Subject Group Info
					$thisSubjectGroupNameEN = $SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleEN'];
					$thisSubjectGroupNameB5 = $SubjectGroupInfoArr[$thisSubjectGroupID]['ClassTitleB5'];
					$thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupNameB5, $thisSubjectGroupNameEN);
					$thisNumOfStudents = ($SubjectGroupInfoArr[$thisSubjectGroupID]['Total']=='')? 0 : $SubjectGroupInfoArr[$thisSubjectGroupID]['Total'];
						
					//$thisSubjectGroupDisplay = $thisSubjectName." - ".$thisSubjectGroupName;
					$thisSubjectGroupDisplay = '';
					$thisSubjectGroupDisplay .= '<span class="SubjectNameDisplaySpan">';
					$thisSubjectGroupDisplay .= $thisSubjectName." - ";
					$thisSubjectGroupDisplay .= '</span>';
					$thisSubjectGroupDisplay .= '<span class="SubjectGroupNameDisplaySpan">';
					$thisSubjectGroupDisplay .= $thisSubjectGroupName;
					$thisSubjectGroupDisplay .= '</span>';
					$thisSubjectGroupDisplay .= '<span class="SubjectGroupNumOfStudentDisplaySpan">';
					$thisSubjectGroupDisplay .= " ($thisNumOfStudents)";
					$thisSubjectGroupDisplay .= '</span>';
					# no edit link
					$thisRoomAllocationDisplay = $thisSubjectGroupDisplay;
		
					# Teachers Info
					$thisStaffNameDisplay = Get_Lang_Selection($SLRS_ExchangeInfo["ChineseName"], $SLRS_ExchangeInfo["EnglishName"]) . "";
						
					# Location Info
					$thisLocationID = $SLRS_ExchangeInfo['LocationID'];
					
					if ($thisLocationID != 0)
					{
						if (isset($LocationInfoArr[$thisLocationID])) {
							$thisLocationInfoArr = $LocationInfoArr[$thisLocationID];
						} else {
							$thisLocationInfoArr = $SLRS_ExchangeInfo["locationInfo"][$thisLocationID];
						}
						
						$thisBuildingName = Get_Lang_Selection($thisLocationInfoArr['BuildingNameChi'], $thisLocationInfoArr['BuildingNameEng']);
						$thisFloorName = Get_Lang_Selection($thisLocationInfoArr['FloorNameChi'], $thisLocationInfoArr['FloorNameEng']);
						$thisRoomName = Get_Lang_Selection($thisLocationInfoArr['RoomNameChi'], $thisLocationInfoArr['RoomNameEng']);
						//$thisLocationDisplay = $thisBuildingName." > ".$thisFloorName." > ".$thisRoomName;
						$thisLocationDisplay .= '<span class="BuildingNameDisplaySpan">';
						$thisLocationDisplay .= $thisBuildingName. $thisLocationID ." > ";
						$thisLocationDisplay .= '</span>';
						$thisLocationDisplay .= '<span class="FloorNameDisplaySpan">';
						$thisLocationDisplay .= $thisFloorName." > ";
						$thisLocationDisplay .= '</span>';
						$thisLocationDisplay .= '<span class="RoomNameDisplaySpan">';
						$thisLocationDisplay .= $thisRoomName;
						$thisLocationDisplay .= '</span>';
					}
					if ($junior_mck) {
						$htmlTable .= "** " . convert2Big5($Lang['SLRS']['LessonExchange']) . " **";
						if ($ignoreColor)
						{
							$htmlTable .= "<br><span style='color:#f00'>(" . convert2Big5($Lang['SLRS']['SubstitutionArrangementAlreadySubstitution']) . ")</span>";
						}
					} else {
						$htmlTable .= "** " . $Lang['SLRS']['LessonExchange'] . " **";
						if ($ignoreColor)
						{
							$htmlTable .= "<br><span style='color:#f00'>(" . $Lang['SLRS']['SubstitutionArrangementAlreadySubstitution'] . ")</span>";
						}
					}
					
					$htmlTable .= '<table id="ContentSLRSTable_' . $thisTimeSlotID . '" class="common_table_list">';
					$htmlTable .= '<tbody><tr>';
					$htmlTable .= '<td style="width:50%; padding:3px;">';
					$htmlTable .= '<div>';
					$htmlTable .= '<div class="SubjectGroupNameDisplayDiv">'.$thisRoomAllocationDisplay.'</div>'."\n";
					$htmlTable .= '<div class="LocationDisplayDiv">'.$thisLocationDisplay.'</div>'."\n";
					$htmlTable .= '<div class="StaffNameDisplayDiv">'.$thisStaffNameDisplay.'</div>'."\n";
					$htmlTable .= '<div class="StaffUserLoginDisplayDiv" style="display:none;">'.$thisStaffUserLoginDisplay.'</div>'."\n";
					$htmlTable .= '</div>';
					$htmlTable .= '</td>';
					$htmlTable .= '</tr></tbody></table>';
				}
			}
			if (!empty($htmlTable)) {
				if ($ignoreColor)
				{
					return "<div style='padding:2px; background-color:#efefef; border:1px solid #CCC'>" . $htmlTable . "</div>";
				} else {
					return "<div style='padding:2px; background-color:#ffe4b5; border:1px solid #CCC'>" . $htmlTable . "</div>";
				}
			} else {
				return "";
			}
		}
		
		public function checkWithCancelLessonInfo($param)
		{
		    print_r($param);
		    exit;
		}
	}
}
?>