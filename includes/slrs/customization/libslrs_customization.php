<?php
// editing by
/********************
 * Customization for ppaulvi 
 *
 * Date : 
 * Description: 
  /********************/
if (defined("LIBSLRS_DEFINED")) {
	
	class libslrs_custom_common extends libslrs {
	
		public function getModuleObjArr() {
			$MODULE_OBJ = parent::getModuleObjArr();
			global $PATH_WRT_ROOT, $intranet_root, $image_path, $LAYOUT_SKIN, $CurrentPage;
			global $plugin, $special_feature, $slrsConfig, $indexVar;
			global $intranet_session_language, $Lang, $sys_custom;
			
			$currentPageAry = explode($slrsConfig['taskSeparator'], $CurrentPage);
			$pageFunction = $currentPageAry[0];
			$pageMenu = $currentPageAry[0].$slrsConfig['taskSeparator'].$currentPageAry[1];

			
			if ($sys_custom['SLRS']["StudentDailyReport"]) {
				$menuTask = 'reports'.$slrsConfig['taskSeparator'].'student_daily_report'.$slrsConfig['taskSeparator'].'list';
				$MODULE_OBJ["menu"]["Reports"]["Child"]["StudentDailyReport"] = array($Lang['SLRS']['StudentDailyReport'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			}
			
			if ($sys_custom['SLRS']["defaultToPresetLocation"]) {
				// Close Location
				$menuTask = 'settings'.$slrsConfig['taskSeparator'].'closed_location'.$slrsConfig['taskSeparator'].'list';
				$MODULE_OBJ["menu"]["Settings"]["Child"]["ClosedLocation"] = array($Lang['SLRS']['ClosedLocation'], '?task='.$menuTask, (stripos($menuTask, $pageMenu)!==false));
			}
			
			return $MODULE_OBJ;
		}
		
		
		public function getSubstituteUserByDate($date = null, $name_field = null, $filterByUserId = null)
		{
			global $slrsConfig;
			$result = null;
			if ($date != null && !empty($date) && $name_field != null && !empty($name_field))
			{
				$dateArr[0] = $date;
				$sql = "SELECT RecordDate,CycleDay,LeaveID,isl.UserID,DateLeave,ReasonCode,Duration,".$name_field." as userName
						FROM(".$this->getDaySQL($dateArr,"").") as icd
						LEFT JOIN(
							SELECT LeaveID,UserID,DateLeave,ReasonCode,Duration FROM INTRANET_SLRS_LEAVE
						) as isl ON icd.RecordDate=isl.DateLeave
						LEFT JOIN(
							SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']."
						) as iu ON isl.UserID=iu.UserID	";
				if ($filterByUserId != null)
				{
					$sql .= " WHERE isl.UserID in (" . $filterByUserId . ")";
				}
				else
				{
					$sql .= " WHERE isl.UserID > 0 and isl.UserID is not null";
				}
				$result = $this->returnResultSet($sql);
				$UserArr = BuildMultiKeyAssoc($result, 'UserID');
				
				$notAvail = $this->getNotAvailableTeacherRecordByDate($date, $date, (!empty($filterByUserId)) ? $filterByUserId : 'all');
				if (count($notAvail) > 0)
				{
					foreach ($notAvail as $kk => $vv)
					{
						if (!isset($UserArr[$vv["UserID"]]))
						{
							$result[] = array(
								"RecordDate" => $vv["DateLeave"],
								"CycleDay" => "",
								"LeaveID" => "",
								"UserID" => $vv["UserID"],
								"DateLeave" => $date,
								"ReasonCode" => $vv["ReasonCode"],
								"Duration" => "",
								"userName" => $vv["UserName"]
							);
						}
					}
				}
			}
			return $result;
		}
		
		/* Remove from Customization
		public function getNotAvailUserByTimeSlot($date, $timeSlotID, $LessonArrangementID='') {
			global $sys_custom, $PATH_WRT_ROOT;
			$ignored_userID = array();
			if ($sys_custom['SLRS']["NoAvailTeachers"]) {
				$strSQL = "SELECT StartTime, EndTime FROM INTRANET_TIMETABLE_TIMESLOT WHERE TimeSlotID='" . $timeSlotID . "'";
				$result = $this->returnResultSet($strSQL);
				if (count($result) > 0) {
					$startTime = $date . " " . $result[0]["StartTime"];
					$endTime = $date . " " . $result[0]["StartTime"];
					$ignored_userArr = $this->getNotAvailableTeacherRecordByDate($startTime, $endTime);
					if (count($ignored_userArr) > 0) {
						$ignored_userArr = BuildMultiKeyAssoc($ignored_userArr, 'UserID');
						$ignored_userID = array_keys($ignored_userArr);
					}
				}
			}
			$strSQL = "SELECT UserID, ArrangedTo_UserID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE TimeSlotID='" . $timeSlotID . "' AND Date(TimeStart)='" . $date . "'";
			if ($LessonArrangementID > 0)
			{
				$strSQL .= " AND LessonArrangementID != '" . $LessonArrangementID . "'";
			}
			$result = $this->returnResultSet($strSQL);
			if (count($result) > 0) {
				foreach ($result as $kk => $vv) {
					$ignored_userID[] = $vv["UserID"];
					if (empty($vv["ArrangedTo_UserID"]) && $vv["ArrangedTo_UserID"] > 0) {
						$ignored_userID[] = $vv["ArrangedTo_UserID"];
					}
				}
			}
			return $ignored_userID;
		}
		
		public function getLessonTeacher($date, $timeSlotID, $LessonArrangementID = '')
		{
			$UserID = parent::getLessonTeacher($date, $timeSlotID, $LessonArrangementID);
			if ($UserID == "''") $UserID = "";
			$ignored_userID = $this->getNotAvailUserByTimeSlot($date, $timeSlotID, $LessonArrangementID);
			if (count($ignored_userID) > 0) {
				if (!empty($UserID)) $UserID .= ",";
				$UserID .= implode(",", $ignored_userID);
			}
			if (empty($UserID)) $UserID = "''";
			return $UserID;
		}
		*/
	}
}