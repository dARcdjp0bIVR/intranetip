<?php
/*
 *  using
 * 	Log:
 * 	Date: 2013-12-11 [Cameron] copy from subject class mapping, for use of fakelayer
 */

include_once($intranet_root.'/includes/libinterface.php');


class common_ui {
	private $thickBoxWidth;
	private $thickBoxHeight;
	private $toolCellWidth;
	
	public function setThickBoxWidth($val){$this->thickBoxWidth=$val;}
	public function getThickBoxWidth(){return $this->thickBoxWidth;}

	public function setThickBoxHeight($val){$this->thickBoxHeight=$val;}
	public function getThickBoxHeight(){return $this->thickBoxHeight;}
	
	public function setToolCellWidth($val){$this->ToolCellWidth=$val;}
	public function getToolCellWidth(){return $this->ToolCellWidth;}
	
	function common_ui() {
		$this->thickBoxWidth = 600;
		$this->thickBoxHeight = 395;
		$this->toolCellWidth = "120px";
	}
	
	function Include_JS_CSS()
	{
		global $PATH_WRT_ROOT, $LAYOUT_SKIN;
		
		$x = '
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/'.$LAYOUT_SKIN.'/js/script.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.jeditable.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.tablednd_0_5.js"></script>
			<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.colorPicker.js"></script>
			
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.autocomplete.css" type="text/css" />
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/thickbox.css" type="text/css" media="screen" />
			<link rel="stylesheet" href="'.$PATH_WRT_ROOT.'templates/jquery/jquery.colorPicker.css" type="text/css" />
			';
			
		return $x;
	}
	
	function generatePopUpBox($data){
		global $image_path, $LAYOUT_SKIN;
		$output = <<<HTML
			<table width="280px" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td height="19">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="5" height="19">
									<img src="{$image_path}/{$LAYOUT_SKIN}/can_board_01.gif" width="5" height="19">
								</td>
								<td height="19" valign="middle" background="{$image_path}/{$LAYOUT_SKIN}/can_board_02.gif">
									&nbsp;
								</td>
								<td width="19" height="19">
									<a href="javascript:Hide_Window()">
										<img src="{$image_path}/{$LAYOUT_SKIN}/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1" />
									</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="5" background="{$image_path}/{$LAYOUT_SKIN}/can_board_04.gif">
									<img src="{$image_path}/{$LAYOUT_SKIN}/can_board_04.gif" width="5" height="19" />
								</td>
								<td align="left" bgcolor="#FFFFF7">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="150" align="left" valign="top">
												<div style="max-height: 300px; overflow-y: auto">
													{$data}
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<br/ >
												<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
													<tr>
														<td height="1" class="dotline">
															<img src="{$image_path}/{$LAYOUT_SKIN}/10x10.gif" width="10" height="1" />
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
								<td width="6" background="{$image_path}/{$LAYOUT_SKIN}/can_board_06.gif">
									<img src="{$image_path}/{$LAYOUT_SKIN}/can_board_06.gif" width="6" height="6" />
								</td>
							</tr>
							<tr>
								<td width="5" height="6">
									<img src="{$image_path}/{$LAYOUT_SKIN}/can_board_07.gif" width="5" height="6" />
								</td>
								<td height="6" background="{$image_path}/{$LAYOUT_SKIN}/can_board_08.gif">
									<img src="{$image_path}/{$LAYOUT_SKIN}/can_board_08.gif" width="5" height="6" />
								</td>
								<td width="6" height="6">
									<img src="{$image_path}/{$LAYOUT_SKIN}/can_board_09.gif" width="6" height="6" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
HTML;
		return $output;
	}
}
?>