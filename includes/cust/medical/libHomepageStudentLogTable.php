<?php
// Using:
/*
 * 2018-04-27 [Cameron]
 * - apply stripslashes to Lev1Name and Lev2Name
 *
 * 2018-03-19 [Cameron]
 * - show PIC directly if there's only one PIC in the record [case #133828]
 * - add function getPicName()
 */
if (! defined("LIBHOMEPAGESTUDENTLOGTABLE_DEFINED")) // Preprocessor directive
{
    define("LIBHOMEPAGESTUDENTLOGTABLE_DEFINED", true);

    class libHomepageStudentLogTable extends libdbtable2007
    {

        private $objDB;

        private $recordID;

        private $userID;

        private $recordTime;

        function libHomepageStudentLogTable($field, $order, $pageNo)
        {
            $this->libdbtable2007($field, $order, $pageNo);
            $this->objDB = new libdb();
        }

        function getPicName($userID)
        {
            $nameField = getNameFieldByLang2("IU.") . " As Name";
            $sql = "SELECT {$nameField} FROM INTRANET_USER IU WHERE UserID='{$userID}'";
            $rs = $this->objDB->returnResultSet($sql);
            return count($rs) ? $rs[0]['Name'] : '';
        }

        function displayCell($i, $data, $css = "", $other = "")
        {
            global $Lang, $objMedical;
            if ($i == 0) { // RecordID. Do not display this column, save recordID to variable instead
                $this->recordID = $data;
                return;
            } else if ($i == 1) { // UserID. Do not display this column, save UserID to variable instead
                $this->userID = $data;
                return;
            } else if ($i == 2) { // RecordTime
                $this->recordTime = $data;
            } else if ($i == 4) { // Log Type
                if ($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTLOG_MANAGEMENT')) {
                    $date = substr($this->recordTime, 0, 10); // Get the date YYYY-MM-DD
                    $data = stripslashes($data);
                    $data = "<a href=\"/home/eAdmin/StudentMgmt/medical/?t=management.studentLogAddItem&eventIDArray={$this->recordID}&sid={$this->userID}&date={$date}#event_{$this->recordID}\">{$data}</a>";
                }
            } else if ($i == 5) { // Log Item (Lev2Item)
                $data = stripslashes($data);
            } else if ($i == 7) { // PIC
                if ($data == '') {
                    $data = '--';
                } else {
                    $dataAry = explode(',', $data);
                    $numberOfPic = count($dataAry);
                    if ($numberOfPic > 1) {
                        $data = "<a class=\"viewStudentLogNoticePIC\" href=\"javascript:void(0);\" data-picid=\"{$data}\">{$Lang['medical']['general']['viewPic']}</a>";
                    } elseif ($numberOfPic == 1) {
                        $data = $this->getPicName($dataAry[0]);
                    } else {
                        $data = '--';
                    }
                }
            }
            return parent::displayCell($i, $data, $css, $other);
        }
    }
}
?>