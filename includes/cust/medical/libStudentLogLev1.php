<?php
// updated by :
/*
 * Log
 *
 * Date: 2018-04-27 [Cameron]
 * - fix: apply stripslashes() to Lev1Name in getHTMLSelection()
 *
 * Date: 2013-12-18 [Cameron]
 */
if (! defined("LIBSTUDENTLOGLEV1_DEFINED")) // Preprocessor directive
{
    define("LIBSTUDENTLOGLEV1_DEFINED", true);

    class studentLogLev1
    {

        private $objDB;

        private $Lev1ID;

        private $Lev1Name;

        private $Lev1Code;

        private $ItemQty;

        private $RecordStatus;

        private $DeletedFlag;

        private $DeletedBy;

        private $DeletedDate;

        private $InputBy;

        private $DateInput;

        private $DateModified;

        private $LastModifiedBy;

        public function studentLogLev1($lev1ID = NULL)
        {
            $this->objDB = new libdb();
            
            if ($lev1ID != '') {
                $this->setLev1ID($lev1ID);
                $this->loadDataFormStorage();
            }
        }

        public function setLev1ID($val)
        {
            $this->Lev1ID = $val;
        }

        public function getLev1ID()
        {
            return $this->Lev1ID;
        }

        public function setLev1Name($val)
        {
            $this->Lev1Name = $val;
        }

        public function getLev1Name()
        {
            return $this->Lev1Name;
        }

        public function setLev1Code($val)
        {
            $this->Lev1Code = $val;
        }

        public function getLev1Code()
        {
            return $this->Lev1Code;
        }

        public function setItemQty($val)
        {
            $this->ItemQty = $val;
        }

        public function getItemQty()
        {
            return $this->ItemQty;
        }

        public function setRecordStatus($val)
        {
            $this->RecordStatus = $val;
        }

        public function getRecordStatus()
        {
            return $this->RecordStatus;
        }

        public function setDeletedFlag($val)
        {
            $this->DeletedFlag = $val;
        }

        public function getDeletedFlag()
        {
            return $this->DeletedFlag;
        }

        public function setDeletedBy($val)
        {
            $this->DeletedBy = $val;
        }

        public function getDeletedBy()
        {
            return $this->DeletedBy;
        }

        public function setDeletedDate($val)
        {
            $this->DeletedDate = $val;
        }

        public function getDeletedDate()
        {
            return $this->DeletedDate;
        }

        public function setInputBy($val)
        {
            $this->InputBy = $val;
        }

        public function getInputBy()
        {
            return $this->InputBy;
        }

        public function setDateInput($val)
        {
            $this->DateInput = $val;
        }

        public function getDateInput()
        {
            return $this->DateInput;
        }

        public function setDateModified($val)
        {
            $this->DateModified = $val;
        }

        public function getDateModified()
        {
            return $this->DateModified;
        }

        public function setLastModifiedBy($val)
        {
            $this->LastModifiedBy = $val;
        }

        public function getLastModifiedBy()
        {
            return $this->LastModifiedBy;
        }

        private function loadDataFormStorage($loadObject = true, $whereCriteria = NULL, $orderCriteria = NULL)
        {
            $conds = '';
            if ($loadObject) {
                $conds = 'and Lev1ID = ' . $this->getLev1ID();
            }
            
            if ($whereCriteria !== NULL) {
                $conds .= ' and (' . $whereCriteria . ')';
            }
            
            $orderBy = '';
            if ($orderCriteria !== NULL) {
                $orderBy = ' ORDER BY ' . $orderCriteria;
            }
            
            $sql = 'select 
							  Lev1ID,
							  Lev1Name,
							  Lev1Code,
							  ItemQty,
							  RecordStatus,
							  DeletedFlag,
							  DeletedBy,
							  DeletedDate,
							  InputBy,
							  DateInput,
							  DateModified,
							  LastModifiedBy 
						  from MEDICAL_STUDENT_LOG_LEV1 where 1=1 ' . $conds . $orderBy;
            $rs = $this->objDB->returnResultSet($sql);
            if ($loadObject) {
                if (count($rs) > 1) {
                    // since the statusid is the key ,return resultset cannot more than 1
                    $errMsg = 'SQL Result! support return zero not or one record but failed [' . count($rs) . '] ' . $sql . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                    alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                    return false;
                }
                
                if (count($rs) == 1) {
                    $rs = current($rs);
                    $this->setLev1ID($rs['Lev1ID']);
                    $this->setLev1Name($rs['Lev1Name']);
                    $this->setLev1Code($rs['Lev1Code']);
                    $this->setItemQty($rs['ItemQty']);
                    $this->setRecordStatus($rs['RecordStatus']);
                    $this->setDeletedFlag($rs['DeletedFlag']);
                    $this->setDeletedBy($rs['DeletedBy']);
                    $this->setDeletedDate($rs['DeletedDate']);
                    $this->setInputBy($rs['InputBy']);
                    $this->setDateInput($rs['DateInput']);
                    $this->setDateModified($rs['DateModified']);
                    $this->setLastModifiedBy($rs['LastModifiedBy']);
                    return $this;
                } else {
                    return NULL;
                }
            } else {
                return $rs;
            }
        }

        public function save()
        {
            if ($this->getLev1ID() > 0) {
                $ret = $this->updateRecord();
            } else {
                $ret = $this->insertRecord();
            }
            return $ret;
        }

        /**
         * Return a array with all active status, default is order by status code desc
         * @owner : Fai (20131212)
         * 
         * @param
         *            : String $orderCriteria (default order by status code desc)
         * @return : Resultset DB array for all active status (not deleted)
         *        
         */
        public function getActiveStatus($orderCriteria = NULL)
        {
            $orderCriteria = ($orderCriteria == NULL) ? ' Lev1Code desc ' : $orderCriteria;
            return $this->getAllStatus($whereCriteria = ' recordstatus = 1 and DeletedFlag = 0 ', $orderCriteria);
        }

        public function getAllStatus($whereCriteria = NULL, $orderCriteria = NULL)
        {
            $rs = $this->loadDataFormStorage($loadObject = false, $whereCriteria, $orderCriteria);
            return $rs;
        }

        public function getHTMLSelection($htmlName, $selectedValue = NULL, $otherAttribute = NULL)
        {
            global $Lang;
            
            $rs = $this->getActiveStatus($orderCriteria = 'Lev1Code asc ');
            
            $html = '<select name="' . $htmlName . '" id="' . $htmlName . '" ' . $otherAttribute . '>' . "\n";
            if (count($rs) > 0) {
                $withDefaultSelected = false;
                for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                    $_selected = ($rs[$i]['Lev1ID'] == $selectedValue) ? ' SELECTED ' : '';
                    $html .= '<option value= "' . $rs[$i]['Lev1ID'] . '" ' . $_selected . ' >' . stripslashes($rs[$i]['Lev1Name']) . '</option>' . "\n";
                }
            } else {
                $html .= '<option value= "" ' . $_selected . '>' . $Lang['General']['NoRecordFound'] . '</option>' . "\n";
            }
            
            $html .= '</select>';
            return $html;
        }

        /**
         * Checking for duplicate ['Lev1Name'] and ['Lev1Code']
         * @owner : Pun (20140110)
         * 
         * @param
         *            : bool $checkName (true: check for duplicate name)
         * @param
         *            : bool $checkCode (true: check for duplicate code)
         * @return : true if the checking pass(no duplicated record)
         *        
         */
        /*
         * Work but not beautiful
         * private function recordDuplicateChecking($checkName = true, $checkCode = true, ){
         * if(!$checkName && !$checkCode)
         * {
         * return true;
         * }
         *
         * if($checkName)
         * {
         * $checkingNameSQL .= ' Lev1Name = \'' . $this->getLev1Name() . '\'';
         * }
         * else
         * {
         * $checkingNameSQL .= '1=0';
         * }
         *
         * if($checkCode)
         * {
         * $checkingCodeSQL .= ' Lev1Code = \'' . $this->getLev1Code() . '\'';
         * }
         * else
         * {
         * $checkingCodeSQL = '1=0';
         * }
         *
         * $sql = 'SELECT Lev1Name,Lev1Code FROM MEDICAL_STUDENT_LOG_LEV1 WHERE DeletedFlag = 0 AND (' . $checkingNameSQL . ' OR ' . $checkingCodeSQL . ')';
         * $rs = $this->objDB->returnResultSet($sql);
         * return !count($rs);
         * }
         */
        
        /**
         * Checking for duplicate ['Lev1Name'] and ['Lev1Code']
         * @owner : Pun (20140110)
         * 
         * @param
         *            : String $testName (Name to test for duplicate)
         * @param
         *            : String $testCode (Code to test for duplicate)
         * @param
         *            : String $Lev1ID (Record to check, for holding old record)
         * @return : 0 if the checking pass(no duplicated record), 1 if Lev1Name duplicated, 2 if Lev1Code duplicated, 3 if both duplicated
         *        
         */
        public function recordDuplicateChecking($testName, $testCode = '', $Lev1ID = '')
        {
            if ($testName == '' && $testCode == '') {
                return true;
            }
            
            $checkingResult = 0;
            $addtionSQL = '';
            if ($Lev1ID != '') {
                $addtionSQL = ' AND Lev1ID != \'' . $Lev1ID . '\'';
            }
            
            $sql = 'SELECT COUNT(*) FROM MEDICAL_STUDENT_LOG_LEV1 WHERE DeletedFlag = 0 AND Lev1Name=\'' . trim($testName, '\'') . '\'' . $addtionSQL;
            $rs = $this->objDB->returnResultSet($sql);
            if ($rs[0]['COUNT(*)'] > 0) {
                $checkingResult += 1;
            }
            // error_log("\n\n\n---------------\n", 3, "/tmp/debug_a.txt");
            // error_log('$sql -->'.print_r($sql,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
            // error_log('$rs -->'.print_r($rs,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
            // error_log('$checkingResult -->'.print_r($checkingResult,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
            
            if ($testCode != '' && $testCode != '\'\'') {
                $sql = 'SELECT COUNT(*) FROM MEDICAL_STUDENT_LOG_LEV1 WHERE DeletedFlag = 0 AND Lev1Code=\'' . trim($testCode, '\'') . '\'' . $addtionSQL;
                $rs = $this->objDB->returnResultSet($sql);
                if ($rs[0]['COUNT(*)'] > 0) {
                    $checkingResult += 2;
                }
            }
            
            return $checkingResult;
        }

        /*
         * Add duplicate checking - by Pun
         */
        private function insertRecord()
        {
            global $cfg_medical;
            $DataArr = array();
            
            if ($this->recordDuplicateChecking($this->getLev1Name(), $this->getLev1Code())) {
                return false;
            }
            
            $DataArr['Lev1Name'] = $this->objDB->pack_value($this->getLev1Name(), 'str');
            $DataArr['Lev1Code'] = $this->objDB->pack_value($this->getLev1Code(), 'str');
            $DataArr['ItemQty'] = $this->objDB->pack_value(0, 'str'); // default no item
            $DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(), 'int');
            $DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(), 'int');
            $DataArr['DateInput'] = $this->objDB->pack_value('now()', 'date');
            // $DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
            // $DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(),'int');
            
            $sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);
            
            $fieldStr = $sqlStrAry['sqlField'];
            $valueStr = $sqlStrAry['sqlValue'];
            
            $sql = 'Insert Into MEDICAL_STUDENT_LOG_LEV1 (' . $fieldStr . ') Values (' . $valueStr . ')';
            
            $success = $this->objDB->db_db_query($sql);
            if ($success != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            
            $RecordID = $this->objDB->db_insert_id();
            $this->setLev1ID($RecordID);
            return $RecordID;
        }

        /*
         * Add duplicate checking - by Pun
         */
        private function updateRecord()
        {
            global $cfg_medical;
            $DataArr = array();
            
            $DataArr['Lev1Name'] = $this->objDB->pack_value($this->getLev1Name(), 'str');
            $DataArr['Lev1Code'] = $this->objDB->pack_value($this->getLev1Code(), 'str');
            // $DataArr['ItemQty'] = $this->objDB->pack_value($this->getItemQty(),'str');
            $DataArr['RecordStatus'] = $this->objDB->pack_value($this->getRecordStatus(), 'int');
            // $DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
            $DataArr['DateModified'] = $this->objDB->pack_value('now()', 'date');
            $DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(), 'int');
            
            if ($this->recordDuplicateChecking($DataArr['Lev1Name'], $DataArr['Lev1Code'], $this->getLev1ID())) {
                return false;
            }
            
            foreach ($DataArr as $fieldName => $data) {
                $updateDetails .= $fieldName . "=" . $data . ",";
            }
            
            // REMOVE LAST OCCURRENCE OF ",";
            $updateDetails = substr($updateDetails, 0, - 1);
            
            $sql = "update MEDICAL_STUDENT_LOG_LEV1 set " . $updateDetails . " where Lev1ID = " . $this->getLev1ID();
            
            $result = $this->objDB->db_db_query($sql);
            
            if ($result != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            
            return $result;
        }

        public function updateItemQty($itemQty)
        {
            $sql = "UPDATE MEDICAL_STUDENT_LOG_LEV1 SET ItemQty=" . $itemQty . " WHERE Lev1ID=" . $this->getLev1ID();
            
            $result = $this->objDB->db_db_query($sql);
            if ($result != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            
            return $result;
        }
    }
}
?>