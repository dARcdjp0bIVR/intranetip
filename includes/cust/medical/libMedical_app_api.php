<?php

/*
 * Using:
 *
 * 2018-11-08 [Cameron]
 * - fix: change error message in addStudentLogRecord() and updateStudentLogRecord() 
 * 
 * 2018-05-02 [Cameron]
 * - fix gender condition in getSleepLogList()
 * - fix set first item color as default if there's no default sleep status in getStudentSleepLev1(), getBowelStatus()
 * - must pass $forceUtf8 = true when apply intranet_htmlspecialchars to remark 
 *
 * 2018-04-30 Cameron
 * - apply intranet_htmlspecialchars to getDefaultSleepRemarks(), getDefaultBowelRemarks() and getDefaultStudentLogRemarks() to avoid javascript attack
 * - fix Lang of "Please Select" for ej in getStudentLogLev3()
 * - simulate $_POST to save data in case addStudentLogRecord and updateStudentLogRecord
 *
 * 2017-12-04 Cameron
 * - create this file
 */
class libMedical_app_api extends libMedical
{

    private $UserID;

    private $ImagePath;

    // Constructor
    public function libMedical_app_api()
    {
        $this->libMedical();
        $this->UserID = $_SESSION["UserID"];
    }

    // Store Image Path
    public function setMedicalImagePath($path)
    {
        $this->ImagePath = $path;
    }

    private function isEJ()
    {
        global $junior_mck;
        return isset($junior_mck);
    }

    public function getBowelStatus($selectedValue = NULL)
    {
        global $intranet_root, $Lang;
        include_once $intranet_root . "/includes/cust/medical/libBowelStatus.php";
        
        $objBowelStatus = new bowelStatus();
        $rs = $objBowelStatus->getActiveStatus(' StatusCode ASC ');
        $_selected = '';
        $html = '';
        $selectedColor = '';
        if (count($rs) > 0) {
            $withDefaultSelected = false;
            for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                $_selected = '';
                // without user default value
                if ($selectedValue == NULL) {
                    // have not set selected before
                    if (! $withDefaultSelected) {
                        if ($rs[$i]['IsDefault'] == 1) {
                            $_selected = ' SELECTED ';
                            $withDefaultSelected = true;
                            $selectedColor = $rs[$i]['Color'];
                        }
                    }
                } else {
                    if ($rs[$i]['StatusID'] == $selectedValue) {
                        $_selected = ' SELECTED ';
                        $selectedColor = $rs[$i]['Color'];
                    } else {
                        $_selected = '';
                    }
                }
                
                $html .= '<option value= "' . $rs[$i]['StatusID'] . '" ' . $_selected . '  data-color="' . $rs[$i]['Color'] . '">' . stripslashes($rs[$i]['StatusName']) . '</option>' . "\n";
            }
            
            if ($selectedColor == '') {
                $selectedColor = $rs[0]['Color'];
            }
        } else {
            $html .= '<option value= "" ' . $_selected . '>' . $Lang['General']['NoRecordFound'] . '</option>' . "\n";
        }
        return array(
            $selectedColor,
            $html
        );
    }

    public function getBowelTimeDisplay($recordTime)
    {
        $ret = "";
        if (strlen($recordTime) == 19) {
            $date = substr($recordTime, 0, 10);
            $time_HH = substr($recordTime, 11, 2);
            $time_MM = substr($recordTime, 14, 2);
            $timeFormat = $this->getBowelTimeInterval();
            $displayMinute = "";
            for ($i = 0; $i < 59; $i += $timeFormat) {
                $min = $i;
                $max = $i + $timeFormat - 1;
                $value = $max;
                $minDisplay = str_pad($min, 2, '0', STR_PAD_LEFT);
                $maxDisplay = str_pad($max, 2, '0', STR_PAD_LEFT);
                
                if ($time_MM >= $min && $time_MM <= $max) {
                    $displayMinute = "{$minDisplay}-{$maxDisplay}";
                }
            }
            $ret = $date . " " . $time_HH . ":" . $displayMinute;
        }
        return $ret;
    }

    public function createTimeSelectionBoxHTML($time = '1900-00-00 00:00:00')
    {
        $html_hour = "<select name='bowel_time_hour' id='bowel_time_hour'>";
        $time_HH = date('H', strtotime($time));
        $time_MM = date('i', strtotime($time));
        
        for ($hh = 0, $hhMax = 24; $hh < $hhMax; $hh ++) {
            if ($hh == $time_HH) {
                $isSelected = ' SELECTED ';
            } else {
                $isSelected = '';
            }
            $html_hour .= "<option value='" . sprintf("%02d", $hh) . "'$isSelected>" . sprintf("%02d", $hh) . "</option>";
        }
        $html_hour .= "</select>";
        
        $html_minute = "<select name='bowel_time_minute' id='bowel_time_minute'>";
        $timeFormat = $this->getBowelTimeInterval();
        for ($i = 0; $i < 59; $i += $timeFormat) {
            $min = $i;
            $max = $i + $timeFormat - 1;
            $value = $max;
            $minDisplay = str_pad($min, 2, '0', STR_PAD_LEFT);
            $maxDisplay = str_pad($max, 2, '0', STR_PAD_LEFT);
            $html_minute .= "<option value='{$value}' " . (($time_MM >= $min && $time_MM <= $max) ? 'SELECTED' : '') . ">{$minDisplay}-{$maxDisplay}</option>";
        }
        $html_minute .= '</select></div>';
        return array(
            $html_hour,
            $html_minute
        );
    }

    public function getDefaultBowelRemarks()
    {
        global $intranet_root, $medical_cfg;
        
        $defaultRemark = $this->getDefaultRemarks($medical_cfg['general']['module']['bowel']);
        $defaultRemarkArr = explode("\n", $defaultRemark[0]);
        
        $x = '';
        if (count($defaultRemarkArr)) {
            $x .= '<div class="card remark_top default_remarks" id="default_remarks"><div class="card-content">';
            foreach ($defaultRemarkArr as $remark) {
                if ($this->isEJ()) {
                    $remark = intranet_htmlspecialchars($remark, $forceUtf8 = true); // ej store remark in orginal form (not convert to htmlspecialchars)
                }
                $x .= '<a class="btn btn-xs blue lighten-1 waves-effect waves-orange remarks" onClick="SetRemarks(this)">' . $remark . '</a>&nbsp;';
            }
            $x .= '</div></div>';
        }
        return $x;
    }

    public function addBowelRecord($data)
    {
        global $intranet_root, $w2_cfg; // must set $w2_cfg be global
        
        $studentID = $data['sld_student'][0];
        $date = $data['date'];
        
        if (! libMedicalShowInput::attendanceDependence('bowel') || libMedicalShowInput::absentAllowedInput('bowel')) {
            $allowAdd = true;
        } else {
            $rs = $this->getBowelDailyLogList($date, $gender = '', $sleep = 1, $classID = '', $studentID);
            if (count($rs)) {
                $allowAdd = $rs[0]['Present'];
            } else {
                $allowAdd = false;
            }
        }
        
        $error = '';
        if ($allowAdd) {
            $recordTime = $data['bowel_time_hour'] . ':' . $data['bowel_time_minute'] . ':00';
            $recordDateTime = $date . ' ' . $recordTime;
            $objBowelLog = new BowelLog($recordDateTime);
            
            $checkData = array();
            $checkData['StudentID'] = $studentID;
            $checkData['BowelID'] = $data['bowel_status'];
            $checkData['RecordDate'] = $date;
            $checkData['RecordTime'] = $recordTime;
            
            if (! $objBowelLog->isBowelLogExist($checkData)) {
                $objBowelLog->setUserID($studentID);
                $objBowelLog->setRecordTime($recordDateTime);
                $objBowelLog->setBowelID($data['bowel_status']);
                $objBowelLog->setRemarks(intranet_htmlspecialchars(trim($data['remarks']), $forceUtf8 = true));
                $objBowelLog->setModifiedBy($_SESSION['UserID']);
                $objBowelLog->setInputBy($_SESSION['UserID']);
                $ret = $objBowelLog->save(true); // create table
                
                if (! $ret) {
                    $error = 'failInsert';
                } else {
                    // $recordID = $ret; // not used yet
                }
            } else {
                $error = 'recordExist';
            }
        } else {
            $error = 'notAllow';
        }
        return $error;
    }

    public function updateBowelRecord($data)
    {
        global $intranet_root, $w2_cfg; // must set $w2_cfg be global
        
        $studentID = $data['sld_student'][0];
        $date = $data['date']; // new date
        $old_date = substr($data['RecordTime'], 0, 10);
        $recordID = $data['RecordID'];
        $recordDateTime = $data['RecordTime']; // old time
        
        if (! libMedicalShowInput::attendanceDependence('bowel') || libMedicalShowInput::absentAllowedInput('bowel')) {
            $allowAdd = true;
        } else {
            $rs = $this->getBowelDailyLogList($date, $gender = '', $sleep = 1, $classID = '', $studentID);
            if (count($rs)) {
                $allowAdd = $rs[0]['Present'];
            } else {
                $allowAdd = false;
            }
        }
        
        $error = '';
        if ($allowAdd) {
            $recordTime = $data['bowel_time_hour'] . ':' . $data['bowel_time_minute'] . ':00';
            $recordDateTime = $date . ' ' . $recordTime;
            
            $checkData = array();
            $checkData['StudentID'] = $studentID;
            $checkData['BowelID'] = $data['bowel_status'];
            $checkData['RecordDate'] = $date;
            $checkData['RecordTime'] = $recordTime;
            
            $objBowelLog = new BowelLog($recordDateTime, $recordID);
            if (substr($date, 0, 7) == substr($old_date, 0, 7)) { // table is the same
                $checkData['RecordID'] = $recordID;
                $tableChange = false;
            } else {
                $tableChange = true; // need to do delete old and insert new
            }
            
            if ($objBowelLog->isBowelLogExist($checkData)) {
                $error = 'recordExist';
            }
            
            if ($error == '') {
                if ($tableChange) {
                    $delResult = $objBowelLog->deleteRecord($old_date, $recordID);
                    $objBowelLog = null;
                    
                    $objBowelLog = new BowelLog($recordDateTime);
                }
                
                $objBowelLog->setUserID($studentID);
                $objBowelLog->setRecordTime($recordDateTime);
                $objBowelLog->setBowelID($data['bowel_status']);
                $objBowelLog->setRemarks(intranet_htmlspecialchars(trim($data['remarks']), $forceUtf8 = true));
                $objBowelLog->setModifiedBy($_SESSION['UserID']);
                $objBowelLog->setInputBy($_SESSION['UserID']);
                $ret = $objBowelLog->save(true); // create table
                
                if (! $ret) {
                    $error = 'failUpdate';
                } else {
                    $recordID = $objBowelLog->getRecordID();
                }
            } else {
                $error = 'recordExist';
            }
        } else {
            $error = 'notAllow';
        }
        $ret = array();
        $ret['error'] = $error;
        $ret['RecordID'] = $recordID;
        $ret['RecordTime'] = $recordDateTime;
        return $ret;
    }

    public function getTargetUserDropDownOption($userID)
    {
        $rs = $this->getStudentInfoByID($userID);
        $student_option = '';
        if (count($rs)) {
            $student_option .= " <option value=\"" . $rs['UserID'] . "\" selected>" . $rs['Name'] . "(" . $rs['ClassName'] . '-' . $rs['ClassNumber'] . ")</option> \n\r ";
        }
        return $student_option;
    }

    public function getStudentLogLev1($selected_item = '')
    {
        global $intranet_root;
        
        $objLogLev1 = new studentLogLev1();
        $lev1_Sel = $objLogLev1->getHTMLSelection("event_level1_select", $selected_item, 'class="select_ajaxload" rel="event_level2_select" target-module="medical_student_log"');
        return $lev1_Sel;
    }

    public function getStudentLogLev2($selected_item = '', $levelOneId = '')
    {
        global $intranet_root;
        
        $objLogLev2 = new studentLogLev2();
        $lev2_Sel = $objLogLev2->getHTMLSelection("event_level2_select", $selected_item, $levelOneId, 'class="level2"');
        return $lev2_Sel;
    }

    public function getStudentLogLev3($htmlName, $selectedValue = NULL, $otherAttribute = NULL)
    {
        global $Lang;
        
        $Lev3List = $this->getLev3List();
        
        $html = '<select name="' . $htmlName . '" id="' . $htmlName . '" ' . $otherAttribute . '>' . "\n";
        $_selected = ($selectedValue == NULL) ? 'SELECTED' : '';
        $html .= '<option value= "" ' . $_selected . '>' . $Lang['medical']['App']['PleaseSelect'] . '</option>' . "\n";
        
        foreach ((array) $Lev3List as $Lev3) {
            $_selected = ($Lev3['Lev3ID'] == $selectedValue) ? 'SELECTED' : '';
            $html .= '<option value= "' . $Lev3['Lev3ID'] . '" ' . $_selected . ' >' . $Lev3['Lev3Name'] . '</option>' . "\n";
        }
        $html .= '</select>';
        return $html;
    }

    public function getDefaultStudentLogRemarks()
    {
        global $intranet_root, $medical_cfg;
        
        $defaultRemark = $this->getDefaultRemarks($medical_cfg['general']['module']['studentlog']);
        $defaultRemarkArr = explode("\n", $defaultRemark[0]);
        
        $x = '';
        if (count($defaultRemarkArr)) {
            $x .= '<div class="card remark_top default_remarks" id="default_remarks"><div class="card-content">';
            foreach ($defaultRemarkArr as $remark) {
                if ($this->isEJ()) {
                    $remark = intranet_htmlspecialchars($remark, $forceUtf8 = true); // ej store remark in orginal form (not convert to htmlspecialchars)
                }
                $x .= '<a class="btn btn-xs blue lighten-1 waves-effect waves-orange remarks" onClick="SetRemarks(this)">' . $remark . '</a>&nbsp;';
            }
            $x .= '</div></div>';
        }
        return $x;
    }

    public function getBodySelectionLayout($itemNr = '1', $labelNr = '1', $prefix = 'n', $level3ID = null, $studentLogID = null)
    {
        global $Lang;
        
        $Lev4Options = '';
        if ($level3ID && $studentLogID) {
            $Lev4List = $this->getLev4List($level3ID); // get Lev4 names
            $Lev4SelectedList = $this->getLev4SelectedList($studentLogID, $level3ID);
            
            foreach ((array) $Lev4List as $Lev4) {
                $selected = '';
                foreach ((array) $Lev4SelectedList as $Lev4Selected) {
                    if ($Lev4['Lev4ID'] == $Lev4Selected['Level4ID']) {
                        $selected = 'selected';
                        break;
                    }
                }
                $Lev4Options .= '<option value="' . $Lev4['Lev4ID'] . '" ' . $selected . '>' . $Lev4['Lev4Name'] . '</option>';
            }
        }
        
        $x = '';
        $x .= '
		<div class="input-field col col-xs-10 col-sm-5" id="dropdown_opt3[' . $prefix . $itemNr . ']" rel="event_level4_list[' . $prefix . $itemNr . ']">
			<div class="select-wrapper">' . $this->getStudentLogLev3('event_level3_select[' . $prefix . $itemNr . ']', $selectedValue = $level3ID, $otherAttribute = 'class="select_ajaxloadlist" rel="dropdown_opt3[' . $prefix . $itemNr . ']" target-module="medical_student_log"') . '
			</div>
			<label id="LabelItemNr_' . $prefix . $labelNr . '" class="ItemNr">' . $Lang['medical']['studentLog']['bodyParts'] . ' ' . $labelNr . '</label>
		</div>

		<div class="input-field col col-xs-2 col-sm-1" id="del_dropdown_opt3[' . $prefix . $itemNr . ']">
			<a title="' . $Lang['Btn']['Delete'] . '" ' . ($prefix == 'n' ? '' : 'href="#myModal" class="delete_item"') . '" data-id="' . $itemNr . '" ' . ($prefix == 'n' ? 'onClick="DeleteNewItem(' . $itemNr . ')"' : '') . '>
				<img src="/home/eClassApp/common/web_module/assets/img/icon_trash.png" class="img_target" data-id="' . $prefix . $itemNr . '" width="20px">
			</a>
		</div>

		<div class="input-field col col-xs-12 col-sm-12" id="event_level4_div_' . $prefix . $itemNr . '">
			 <select id="event_level4_list[' . $prefix . $itemNr . ']" name="event_level4_list[' . $prefix . $itemNr . '][]" multiple readyonly class="">
				<option value="" disabled selected>' . $Lang['medical']['App']['PleaseSelectSyndrome'] . '</option>' . $Lev4Options . '
			</select>
			<label>' . $Lang['medical']['studentlog_setting']['convulsion']['syndrome'] . '</label>
		</div>';
        
        return $x;
    }

    public function getPICDropDownOption($pic)
    {
        $picList = $this->getPICInfo($pic);
        $options = '';
        if (count($picList)) {
            foreach ((array) $picList as $p) {
                $options .= " <option value=\"" . $p[0] . "\" selected>" . $p[1] . "</option> \n\r ";
            }
        }
        return $options;
    }

    public function addStudentLogRecord($data)
    {
        global $Lang, $intranet_session_language, $intranet_root, $PATH_WRT_ROOT;
        global $w2_cfg, $medical_cfg, $plugin, $sys_custom;
        
        $studentID = $data['sld_student'][0];
        $date = $data['date'];
        
        if (! libMedicalShowInput::attendanceDependence('studentLog') || libMedicalShowInput::absentAllowedInput('studentLog')) {
            $allowAdd = true;
        } else {
            $rs = $this->getStudentLogList($date, $gender = '', $sleep = '', $classID = '', $studentID);
            if (count($rs)) {
                $allowAdd = $rs[0]['Present'];
            } else {
                $allowAdd = false;
            }
        }
        
        $record = array();
        $record['level1'] = $data['event_level1_select'];
        $record['level2'] = $data['event_level2_select'];
        
        for ($i = 1; $i <= $data['NrItem']; $i ++) {
            $level3ID = $data["event_level3_select[n{$i}]"];
            if ($level3ID) {
                foreach ((array) $data["event_level4_list[n{$i}]"] as $level4ID) {
                    $record['level4'][$level3ID][$level4ID] = $level4ID;
                }
            }
        }
        
        if (empty($record['level2'])) {
            $allowAdd = false;
            return 'EmptyLev2';
        }
        
        $specialStatusIDList = $this->getLev2IDByName($medical_cfg['studentLog']['level2']['specialStatus']);
        if (in_array($record['level2'], $specialStatusIDList) && ! $plugin['medical_module']['AlwaysShowBodyPart'] && count($record['level4']) == 0) {
            $allowAdd = false;
            return 'EmptyLev4';
        }
        
        $error = '';
        if ($allowAdd) {
            $objMedical = new libMedical();
            
            $event = array(
                'n1' => array()
            );
            
            $sid = $data['sld_student'][0];
            $_POST['date'] = $data['date'];
            list ($record['time_hour'], $record['time_min']) = explode(":", $data['startTime']);
            $record['remarks'] = $data['remarks'];
            $record['timelasted_min'] = $data['event_lasted_min'];
            $record['timelasted_sec'] = $data['event_lasted_sec'];
            $record['PIC'] = $data['pics'];
            $medical_currentUserId = $_SESSION['UserID'];
            
            $record['email2pic'] = $data['email2pic'];
            
            $event['n1'] = $record;
            $_POST['event'] = $event; // simulate $_POST to save data in following file
            
            include ($intranet_root . "/home/eAdmin/StudentMgmt/medical/management/studentLogAddItemSave.php");
            
            if (count($data['StudentLogDocumentID'])) {
                $sql = "UPDATE MEDICAL_STUDENT_LOG_DOCUMENT SET StudentLogID='" . $_logRecordId . "' WHERE RecordID IN (" . $data['StudentLogDocumentID'][0] . ")";
                $result = $this->objDB->db_db_query($sql);
            }
            
            if ($Msg == $Lang['General']['ReturnMessage']['UpdateSuccess']) {
                $success = true;
            } else {
//                $param["error"] = $jsErrorMsg;
                $error = 'failToAdd';
            }
        } else {
            $error = 'notAllow';
        }
        
        return $error;
    }

    public function updateStudentLogRecord($data)
    {
        global $Lang, $intranet_session_language, $intranet_root, $PATH_WRT_ROOT;
        global $w2_cfg, $medical_cfg, $plugin, $sys_custom;
        
        $studentID = $data['sld_student'][0];
        $date = $data['date'];
        $ret = array();
        $ret['RecordID'] = $data['RecordID'];
        
        if (! libMedicalShowInput::attendanceDependence('studentLog') || libMedicalShowInput::absentAllowedInput('studentLog')) {
            $allowAdd = true;
        } else {
            $rs = $this->getStudentLogList($date, $gender = '', $sleep = '', $classID = '', $studentID);
            if (count($rs)) {
                $allowAdd = $rs[0]['Present'];
            } else {
                $allowAdd = false;
            }
        }
        
        $record = array();
        $record['level1'] = $data['event_level1_select'];
        $record['level2'] = $data['event_level2_select'];
        
        for ($i = 1; $i <= $data['NrItem']; $i ++) {
            $level3ID = $data["event_level3_select[n{$i}]"];
            if ($level3ID) {
                foreach ((array) $data["event_level4_list[n{$i}]"] as $level4ID) {
                    $record['level4'][$level3ID][$level4ID] = $level4ID;
                }
            }
        }
        
        if (empty($record['level2'])) {
            $allowAdd = false;
            $ret['error'] = 'EmptyLev2';
            return $ret;
        }
        
        $specialStatusIDList = $this->getLev2IDByName($medical_cfg['studentLog']['level2']['specialStatus']);
        if (in_array($record['level2'], $specialStatusIDList) && ! $plugin['medical_module']['AlwaysShowBodyPart'] && count($record['level4']) == 0) {
            $allowAdd = false;
            $ret['error'] = 'EmptyLev4';
            return $ret;
        }
        
        $error = '';
        if ($allowAdd) {
            $objMedical = new libMedical();
            
            $event = array();
            
            $sid = $data['sld_student'][0];
            $_POST['date'] = $data['date'];
            list ($record['time_hour'], $record['time_min']) = explode(":", $data['startTime']);
            $record['remarks'] = $data['remarks'];
            $record['timelasted_min'] = $data['event_lasted_min'];
            $record['timelasted_sec'] = $data['event_lasted_sec'];
            $record['PIC'] = $data['pics'];
            $medical_currentUserId = $_SESSION['UserID'];
            $record['email2pic'] = $data['email2pic'];
            
            // get attachment id to delete
            if ($data['AttachmentToDelete'][0]) {
                $attachmentToDeleteAry = explode(",", $data['AttachmentToDelete'][0]);
                $attachmentToDelete = array();
                foreach ((array) $attachmentToDeleteAry as $attachmentID) {
                    $attachmentToDelete[$attachmentID] = ''; // use key for further process
                }
                $record['isfileUploadedPresent'] = $attachmentToDelete;
            }
            
            $eventIDArray = "";
            $event[$data['RecordID']] = $record;
            $_POST['event'] = $event; // simulate $_POST to save data in following file
            
            include ($intranet_root . "/home/eAdmin/StudentMgmt/medical/management/studentLogAddItemSave.php");
            
            if (count($data['StudentLogDocumentID'])) {
                $sql = "UPDATE MEDICAL_STUDENT_LOG_DOCUMENT SET StudentLogID='" . $_logRecordId . "' WHERE RecordID IN (" . $data['StudentLogDocumentID'][0] . ")";
                $result = $this->objDB->db_db_query($sql);
            }
            
            if ($Msg == $Lang['General']['ReturnMessage']['UpdateSuccess']) {
                $success = true;
            } else {
//                $param["error"] = $jsErrorMsg;
                $error = 'failToUpdate';
            }
        } else {
            $error = 'notAllow';
        }
        $ret = array();
        $ret['error'] = $error;
        return $ret;
    }

    public function getStudentSleepLev1($selectedValue = '')
    {
        global $intranet_root, $Lang;
        include_once $intranet_root . "/includes/cust/medical/libStudentSleepLev1.php";
        
        $objStudentSleepLev1 = new studentSleepLev1();
        $rs = $objStudentSleepLev1->getActiveStatus($orderCriteria = ' StatusCode ASC ', $showDefault = true);
        $_selected = '';
        $html = '';
        $selectedColor = '';
        if (count($rs) > 0) {
            $withDefaultSelected = false;
            for ($i = 0, $iMax = count($rs); $i < $iMax; $i ++) {
                $_selected = '';
                // without user default value
                if ($selectedValue == NULL) {
                    // have not set selected before
                    if (! $withDefaultSelected) {
                        if ($rs[$i]['IsDefault'] == 1) {
                            $_selected = ' SELECTED ';
                            $withDefaultSelected = true;
                            $selectedColor = $rs[$i]['Color'];
                        }
                    }
                } else {
                    if ($rs[$i]['StatusID'] == $selectedValue) {
                        $_selected = ' SELECTED ';
                        $selectedColor = $rs[$i]['Color'];
                    } else {
                        $_selected = '';
                    }
                }
                
                $html .= '<option value= "' . $rs[$i]['StatusID'] . '" ' . $_selected . '  data-color="' . $rs[$i]['Color'] . '">' . stripslashes($rs[$i]['StatusName']) . '</option>' . "\n";
            }
            
            if ($selectedColor == '') {
                $selectedColor = $rs[0]['Color'];
            }
        } else {
            $html .= '<option value= "" ' . $_selected . '>' . $Lang['General']['NoRecordFound'] . '</option>' . "\n";
        }
        
        return array(
            $selectedColor,
            $html
        );
    }

    public function getStudentSleepLev2($selected_item = '', $levelOneId = '')
    {
        global $intranet_root;
        
        $objStudentSleepLev2 = new studentSleepLev2();
        $lev2_Sel = $objStudentSleepLev2->getHTMLSelection("sleep_reason", $selected_item, $levelOneId, 'class="level2"');
        return $lev2_Sel;
    }

    public function getSleepFrequency($selected_frequency = '')
    {
        global $medical_cfg;
        
        $frequenceHTML = '<select name="sleep_frequency" id="sleep_frequency">';
        for ($i = 0; $i < $medical_cfg['general']['student_sleep']['frequency']; $i ++) {
            $selected = '';
            if ($selected_frequency == $i + 1) {
                $selected = ' SELECTED ';
            }
            $frequenceHTML .= '<option value="' . ($i + 1) . '"' . $selected . '>' . ($i + 1) . '</option>';
        }
        $frequenceHTML .= '</select>';
        
        return $frequenceHTML;
    }

    public function getDefaultSleepRemarks()
    {
        global $intranet_root, $medical_cfg;
        
        $defaultRemark = $this->getDefaultRemarks($medical_cfg['general']['module']['sleep']);
        $defaultRemarkArr = explode("\n", $defaultRemark[0]);
        
        $x = '';
        if (count($defaultRemarkArr)) {
            $x .= '<div class="card remark_top default_remarks" id="default_remarks"><div class="card-content">';
            foreach ($defaultRemarkArr as $remark) {
                if ($this->isEJ()) {
                    $remark = intranet_htmlspecialchars($remark, $forceUtf8 = true); // ej store remark in orginal form (not convert to htmlspecialchars)
                }
                $x .= '<a class="btn btn-xs blue lighten-1 waves-effect waves-orange remarks" onClick="SetRemarks(this)">' . $remark . '</a>&nbsp;';
            }
            $x .= '</div></div>';
        }
        return $x;
    }

    public function addSleepRecord($data)
    {
        global $intranet_root, $w2_cfg; // must set $w2_cfg be global
        
        $studentID = $data['sld_student'][0];
        $date = $data['date'];
        
        if (! libMedicalShowInput::attendanceDependence('sleep') || libMedicalShowInput::absentAllowedInput('sleep')) {
            $allowAdd = true;
        } else {
            $rs = $this->getSleepLogList($date, $gender = '1', $sleep = 1, $classID = '', $studentID); // $gender='1' denotes "all gender"
            if (count($rs)) {
                $allowAdd = $rs[0]['Present'];
            } else {
                $allowAdd = false;
            }
        }
        
        $error = '';
        if ($allowAdd) {
            $objStudentSleepLog = new StudentSleepLog();
            $objStudentSleepLog->setUserID($studentID);
            $objStudentSleepLog->setRecordTime($date);
            $objStudentSleepLog->setSleepID($data['sleep_status']);
            $objStudentSleepLog->setReasonID($data['sleep_reason']);
            $objStudentSleepLog->setFrequency($data['sleep_frequency']);
            $objStudentSleepLog->setRemarks(intranet_htmlspecialchars(trim($data['remarks']), $forceUtf8 = true));
            $objStudentSleepLog->setModifiedBy($_SESSION['UserID']);
            $objStudentSleepLog->setInputBy($_SESSION['UserID']);
            $ret = $objStudentSleepLog->save();
            
            if (! $ret) {
                $error = 'failInsert';
            } else {
                // $recordID = $ret; // not used yet
            }
        } else {
            $error = 'notAllow';
        }
        return $error;
    }

    public function updateSleepRecord($data)
    {
        global $intranet_root, $w2_cfg; // must set $w2_cfg be global
        
        $studentID = $data['sld_student'][0];
        $date = $data['date'];
        
        if (! libMedicalShowInput::attendanceDependence('sleep') || libMedicalShowInput::absentAllowedInput('sleep')) {
            $allowAdd = true;
        } else {
            $rs = $this->getSleepLogList($date, $gender = '1', $sleep = 1, $classID = '', $studentID);
            if (count($rs)) {
                $allowAdd = $rs[0]['Present'];
            } else {
                $allowAdd = false;
            }
        }
        
        $error = '';
        if ($allowAdd) {
            $objStudentSleepLog = new StudentSleepLog($data['RecordID']);
            $objStudentSleepLog->setUserID($studentID);
            $objStudentSleepLog->setRecordTime($date);
            $objStudentSleepLog->setSleepID($data['sleep_status']);
            $objStudentSleepLog->setReasonID($data['sleep_reason']);
            $objStudentSleepLog->setFrequency($data['sleep_frequency']);
            $objStudentSleepLog->setRemarks(intranet_htmlspecialchars(trim($data['remarks']), $forceUtf8 = true));
            $objStudentSleepLog->setModifiedBy($_SESSION['UserID']);
            $objStudentSleepLog->setInputBy($_SESSION['UserID']);
            $ret = $objStudentSleepLog->save();
            
            if (! $ret) {
                $error = 'failUpdate';
            } else {
                // successful
            }
        } else {
            $error = 'notAllow';
        }
        $ret = array();
        $ret['error'] = $error;
        $ret['RecordID'] = $data['RecordID'];
        return $ret;
    }
}
?>