<?php 
// Using:  
/*
 * 	2016-05-10 [Cameron]
 * 		- fix bug to support php5.4: Fatal error: Call to undefined method libReadSendMessageTable::libdbtable2007() in...
 * 			by calling parent::libdbtable2007() instead of $this->libdbtable2007()    
 */

	class libReadSendMessageTable extends libdbtable2007
	{	
		private $objDB;
		private $userNameArr;
		
		function libReadSendMessageTable($field, $order, $pageNo){
//			$this->libdbtable2007($field, $order, $pageNo);
			parent::libdbtable2007($field, $order, $pageNo);
			
			$this->objDB = new libdb();
			$this->userNameArr = array();
		}
		
		function displayCell($i,$data, $css="", $other=""){
			if($i == 0){
				$receiver = '';
				
				$receiverIdArr = explode(',',$data);
				$receiverNameField = getNameFieldByLang2('iu.');
				foreach($receiverIdArr as $id){
					if(empty($this->userNameArr[$id])){
						$sql = "SELECT {$receiverNameField} AS Name FROM INTRANET_USER iu WHERE UserID='{$id}'";
						$rs = $this->objDB->returnResultSet($sql);
						$this->userNameArr[$id] = $rs[0]['Name'];
					}
					$receiver .= $this->userNameArr[$id] . '<br />';
				}
				$data = trim($receiver,'<br />');
			}
			
			return parent::displayCell($i,$data, $css, $other);
		}
		
	}

?>