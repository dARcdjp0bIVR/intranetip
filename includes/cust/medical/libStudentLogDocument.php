<?php
//libStudentLogDocument.php
if (!defined("LIBSTUDENTLOGDOCUMENT_DEFINED"))                     // Preprocessor directive
{
	define("LIBSTUDENTLOGDOCUMENT_DEFINED", true);
	class StudentLogDocument
    {
		private $objDB;

		private $RecordID;
		private $StudentLogID;
		private $RenameFile;
		private $OrgFileName;
		private $uploadFileType;
		private $uploadFileSize;
		private $FolderPath;
		private $DateInput;
		private $InputBy;

		public function StudentLogDocument()
		{
			$this->objDB= new libdb();
		}


		public function setRecordID($val){$this->RecordID=$val;} 
		public function getRecordID(){return $this->RecordID;}

		public function setStudentLogID($val){$this->StudentLogID=$val;} 
		public function getStudentLogID(){return $this->StudentLogID;}

		public function setRenameFile($val){$this->RenameFile=$val;} 
		public function getRenameFile(){return $this->RenameFile;}

		public function setOrgFileName($val){$this->OrgFileName=$val;} 
		public function getOrgFileName(){return $this->OrgFileName;}

		public function setFileType($val){$this->uploadFileType=$val;} 
		public function getFileType(){return $this->uploadFileType;}

		public function setFileSize($val){$this->uploadFileSize=$val;} 
		public function getFileSize(){return $this->uploadFileSize;}

		public function setFolderPath($val){$this->FolderPath=$val;} 
		public function getFolderPath(){return $this->FolderPath;}

		public function setDateInput($val){$this->DateInput=$val;} 
		public function getDateInput(){return $this->DateInput;}

		public function setInputBy($val){$this->InputBy=$val;} 
		public function getInputBy(){return $this->InputBy;}

		public function save()
		{
			return $this->insertRecord();
		}

		private function insertRecord()
		{
			global $cfg_medical;

			$DataArr['StudentLogID'] = $this->objDB->pack_value($this->getStudentLogID(),'int');
			$DataArr['RenameFile'] = $this->objDB->pack_value($this->getRenameFile(),'str');
			$DataArr['OrgFileName'] = $this->objDB->pack_value($this->getOrgFileName(),'str');
			$DataArr['FileType'] = $this->objDB->pack_value($this->getFileType(),'str');
			$DataArr['FileSize'] = $this->objDB->pack_value($this->getFileSize(),'int');
			$DataArr['FolderPath'] = $this->objDB->pack_value($this->getFolderPath(),'str');
			$DataArr['DateInput'] = $this->objDB->pack_value($this->getDateInput(),'date');
			$DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');

			$sql = 'Insert Into MEDICAL_STUDENT_LOG_DOCUMENT ('.implode(',',array_keys($DataArr)).') Values ('.implode(',',array_values($DataArr)).')';
//debug_r($sql);
			$success = $this->objDB->db_db_query($sql);

			if($success != 1){
					$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			        alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
					 return false;
			}
					
				$RecordID = $this->objDB->db_insert_id();
				$this->setRecordID($RecordID);
				return $RecordID;

		}
	}

}
?>