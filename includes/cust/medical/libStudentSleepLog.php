<?php
// updated by :
/*
 * Log
 *
 * 2018-04-25 Cameron
 * - setModifiedBy in loadDataFormStorage()
 */
if (! defined("LIBSTUDNETSLEEPLOG_DEFINED")) // Preprocessor directive
{
    define("LIBSTUDNETSLEEPLOG_DEFINED", true);

    class StudentSleepLog
    {

        private $objDB;

        private $RecordID;

        private $UserID;

        private $RecordTime;

        private $SleepID;

        private $Frequency;

        private $ReasonID;

        private $Remarks;

        private $InputBy;

        private $DateInput;

        private $DateModified;

        private $ModifiedBy;

        private $OldSleepID;

        private $OldFrequency;

        private $OldReasonID;

        private $OldRemarks;

        public function StudentSleepLog($recordID = NULL)
        {
            $this->objDB = new libdb();
            
            if ($recordID != '') {
                $this->setRecordID($recordID);
                $this->loadDataFormStorage();
            }
        }

        public function setRecordID($val)
        {
            $this->RecordID = $val;
        }

        public function getRecordID()
        {
            return $this->RecordID;
        }

        public function setUserID($val)
        {
            $this->UserID = $val;
        }

        public function getUserID()
        {
            return $this->UserID;
        }

        public function setRecordTime($val)
        {
            $this->RecordTime = $val;
        }

        public function getRecordTime()
        {
            return $this->RecordTime;
        }

        public function setSleepID($val)
        {
            $this->SleepID = $val;
        }

        public function getSleepID()
        {
            return $this->SleepID;
        }

        public function setFrequency($val)
        {
            $this->Frequency = $val;
        }

        public function getFrequency()
        {
            return $this->Frequency;
        }

        public function setReasonID($val)
        {
            $this->ReasonID = $val;
        }

        public function getReasonID()
        {
            return $this->ReasonID;
        }

        public function setRemarks($val)
        {
            $this->Remarks = $val;
        }

        public function getRemarks()
        {
            return $this->Remarks;
        }

        public function setDeletedDate($val)
        {
            $this->DeletedDate = $val;
        }

        public function getDeletedDate()
        {
            return $this->DeletedDate;
        }

        public function setInputBy($val)
        {
            $this->InputBy = $val;
        }

        public function getInputBy()
        {
            return $this->InputBy;
        }

        public function setDateInput($val)
        {
            $this->DateInput = $val;
        }

        public function getDateInput()
        {
            return $this->DateInput;
        }

        public function setDateModified($val)
        {
            $this->DateModified = $val;
        }

        public function getDateModified()
        {
            return $this->DateModified;
        }

        public function setModifiedBy($val)
        {
            $this->ModifiedBy = $val;
        }

        public function getModifiedBy()
        {
            return $this->ModifiedBy;
        }

        private function setOldSleepID($val)
        {
            $this->OldSleepID = $val;
        }

        private function getOldSleepID()
        {
            return $this->OldSleepID;
        }

        private function setOldFrequency($val)
        {
            $this->OldFrequency = $val;
        }

        private function getOldFrequency()
        {
            return $this->OldFrequency;
        }

        private function setOldReasonID($val)
        {
            $this->OldReasonID = $val;
        }

        private function getOldReasonID()
        {
            return $this->OldReasonID;
        }

        private function setOldRemarks($val)
        {
            $this->OldRemarks = $val;
        }

        private function getOldRemarks()
        {
            return $this->OldRemarks;
        }

        private function loadDataFormStorage($loadObject = true, $whereCriteria = NULL, $orderCriteria = NULL)
        {
            $conds = '';
            if ($loadObject) {
                $conds = 'and RecordID = ' . $this->getRecordID();
            }
            
            if ($whereCriteria !== NULL) {
                $conds .= ' and (' . $whereCriteria . ')';
            }
            
            $orderBy = '';
            if ($orderCriteria !== NULL) {
                $orderBy = ' ORDER BY ' . $orderCriteria;
            }
            
            $sql = 'select 
							RecordID,
							UserID,
							RecordTime,
							Remarks,
							SleepID,
							Frequency,
							ReasonID,
							InputBy,
							DateInput,
							DateModified,
							ModifyBy 
						  from MEDICAL_STUDENT_SLEEP where 1=1 ' . $conds . $orderBy;
            
            $rs = $this->objDB->returnResultSet($sql);
            
            if ($loadObject) {
                if (count($rs) > 1) {
                    // since the statusid is the key ,return resultset cannot more than 1
                    $errMsg = 'SQL Result! support return zero not or one record but failed [' . count($rs) . '] ' . $sql . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                    alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                    return false;
                }
                
                if (count($rs) == 1) {
                    $rs = current($rs);
                    $this->setRecordID($rs['RecordID']);
                    $this->setUserID($rs['UserID']);
                    $this->setRecordTime($rs['RecordTime']);
                    $this->setRemarks($rs['Remarks']);
                    $this->setSleepID($rs['SleepID']);
                    
                    $this->setFrequency($rs['Frequency']);
                    $this->setReasonID($rs['ReasonID']);
                    $this->setInputBy($rs['InputBy']);
                    $this->setDateInput($rs['DateInput']);
                    $this->setDateModified($rs['DateModified']);
                    $this->setModifiedBy($rs['ModifyBy']);
                    
                    $this->setOldSleepID($rs['SleepID']);
                    $this->setOldFrequency($rs['Frequency']);
                    $this->setOldReasonID($rs['ReasonID']);
                    $this->setOldRemarks($rs['Remarks']);
                    
                    return $this;
                } else {
                    return NULL;
                }
            } else {
                return $rs;
            }
        }

        public function addRecord($userID, $date, $SleepID, $ReasonID, $Frequency, $Remarks, $InputBy, $recordID = '')
        {
            $this->setUserID($userID);
            $this->setRecordTime($date);
            
            $this->setSleepID($SleepID);
            $this->setReasonID($ReasonID);
            $this->setFrequency($Frequency);
            $this->setRemarks($Remarks);
            $this->setInputBy($InputBy);
            $this->setModifiedBy($InputBy);
            
            $this->setRecordID($recordID);
        }

        public function deleteRecord($RecordID)
        {
            if ($RecordID == '') {
                return false;
            }
            $sql = "DELETE FROM MEDICAL_STUDENT_SLEEP where RecordID = " . $RecordID;
            $result = $this->objDB->db_db_query($sql);
            if ($result != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            
            return $result;
        }

        public function save()
        {
            if ($this->getRecordID() > 0) {
                $ret = $this->updateRecord();
            } else {
                $ret = $this->insertRecord();
            }
            return $ret;
        }

        /**
         * Return a array with all active status, default is order by status code desc
         * @owner : Fai (20131212)
         *
         * @param
         *            : String $orderCriteria (default order by status code desc)
         * @return : Resultset DB array for all active status (not deleted)
         *        
         */
        public function getStudentSleepLogByDateAndUserID($date, $UserID)
        {
            $whereCriteria = ".
							recordtime >=  '{$date} 00:00:00'
						And
							recordtime <= '{$date} 23:59:59'
						And
							UserID = {$UserID}
					";
            $orderCriteria = 'recordTime';
            $rs = $this->loadDataFormStorage($loadObject = false, $whereCriteria, $orderCriteria);
            return $rs;
        }

        private function insertRecord()
        {
            global $cfg_medical;
            $DataArr = array();
            
            $DataArr['UserID'] = $this->objDB->pack_value($this->getUserID(), 'int');
            $DataArr['RecordTime'] = $this->objDB->pack_value($this->getRecordTime(), 'date');
            
            $DataArr['Remarks'] = $this->objDB->pack_value($this->getRemarks(), 'str');
            $DataArr['SleepID'] = $this->objDB->pack_value($this->getSleepID(), 'int');
            $DataArr['Frequency'] = $this->objDB->pack_value($this->getFrequency(), 'int');
            $DataArr['ReasonID'] = $this->objDB->pack_value($this->getReasonID(), 'int');
            
            $DataArr['DateInput'] = $this->objDB->pack_value('now()', 'date');
            $DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(), 'int');
            $DataArr['DateModified'] = $this->objDB->pack_value('now()', 'date');
            $DataArr['ModifyBy'] = $this->objDB->pack_value($this->getModifiedBy(), 'int');
            
            $sqlStrAry = $this->objDB->concatFieldValueToSqlStr($DataArr);
            $fieldStr = $sqlStrAry['sqlField'];
            $valueStr = $sqlStrAry['sqlValue'];
            $sql = 'Insert Into MEDICAL_STUDENT_SLEEP (' . $fieldStr . ') Values (' . $valueStr . ')';
            // debug_r($sql);
            $success = $this->objDB->db_db_query($sql);
            if ($success != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            
            $RecordID = $this->objDB->db_insert_id();
            $this->setRecordID($RecordID);
            return $RecordID;
        }

        private function updateRecord()
        {
            global $cfg_medical;
            
            // Update the modified recored only.
            if ($this->getOldSleepID() == $this->getSleepID() && $this->getOldFrequency() == $this->getFrequency() && $this->getOldReasonID() == $this->getReasonID() && $this->getOldRemarks() == $this->getRemarks()) {
                return true;
            }
            
            $DataArr = array();
            
            $DataArr['RecordID'] = $this->objDB->pack_value($this->getRecordID(), 'int');
            $DataArr['UserID'] = $this->objDB->pack_value($this->getUserID(), 'int');
            $DataArr['RecordTime'] = $this->objDB->pack_value($this->getRecordTime(), 'date');
            
            $DataArr['Remarks'] = $this->objDB->pack_value($this->getRemarks(), 'str');
            $DataArr['SleepID'] = $this->objDB->pack_value($this->getSleepID(), 'int');
            $DataArr['Frequency'] = $this->objDB->pack_value($this->getFrequency(), 'int');
            $DataArr['ReasonID'] = $this->objDB->pack_value($this->getReasonID(), 'int');
            
            // $DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
            $DataArr['DateModified'] = $this->objDB->pack_value('now()', 'date');
            $DataArr['ModifyBy'] = $this->objDB->pack_value($this->getModifiedBy(), 'int');
            
            foreach ($DataArr as $fieldName => $data) {
                $updateDetails .= $fieldName . "=" . $data . ",";
            }
            
            // REMOVE LAST OCCURRENCE OF ",";
            $updateDetails = substr($updateDetails, 0, - 1);
            
            $sql = "update MEDICAL_STUDENT_SLEEP set " . $updateDetails . " where RecordID = " . $this->getRecordID();
            $result = $this->objDB->db_db_query($sql);
            
            if ($result != 1) {
                $errMsg = 'SQL Error! ' . $sql . ' error[' . mysql_error() . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                return false;
            }
            return $result;
        }
    }
}
?>