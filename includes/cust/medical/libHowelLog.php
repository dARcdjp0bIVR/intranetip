<?php
//updated by : Adam
/*
 * 	Log
 * 	Date:	2013-12-13 [Cameron] Add function checkIsDefault()
 */
if (!defined("LIBHOWELLOG_DEFINED"))                     // Preprocessor directive
{
        define("LIBHOWELLOG_DEFINED", true);
		class howelLog
        {
                private $objDB;

				private $RecordID;
				private $UserID;
				private $RecordTime;
				private $HowelID;
				private $Remarks;

				private $InputBy;
				private $DateInput;
				private $DateModified;
				private $ModifiedBy;


				public function howelLog($recordID = NULL){
					$this->objDB = new libdb();

					if($recordID != ''){
						$this->setRecordID($recordID);
						$this->loadDataFormStorage();
					}
				}

				public function setRecordID($val){$this->RecordID=$val;}
				public function getRecordID(){return $this->RecordID;}

				public function setUserID($val){$this->UserID=$val;}
				public function getUserID(){return $this->UserID;}

				public function setRecordTime($val){$this->RecordTime=$val;}
				public function getRecordTime(){return $this->RecordTime;}

				public function setHowelID($val){$this->HowelID=$val;}
				public function getHowelID(){return $this->HowelID;}

				public function setRemarks($val){$this->Remarks=$val;}
				public function getRemarks(){return $this->Remarks;}

				public function setDeletedDate($val){$this->DeletedDate=$val;}
				public function getDeletedDate(){return $this->DeletedDate;}

				public function setInputBy($val){$this->InputBy=$val;}
				public function getInputBy(){return $this->InputBy;}
				
				public function setDateInput($val){$this->DateInput=$val;}
				public function getDateInput(){return $this->DateInput;}

				public function setDateModified($val){$this->DateModified=$val;}
				public function getDateModified(){return $this->DateModified;}

				public function setModifiedBy($val){$this->ModifiedBy=$val;}
				public function getModifiedBy(){return $this->ModifiedBy;}

				private function loadDataFormStorage($loadObject=true, $whereCriteria= NULL,$orderCriteria = NULL)
				{

					$conds = '';
					if($loadObject){
						$conds = 'and RecordID = '.$this->getRecordID();
					}

					if($whereCriteria !== NULL){
							$conds .= ' and ('.$whereCriteria.')';
					}
					
					$orderBy = '';
					if ($orderCriteria !== NULL){
						$orderBy = ' ORDER BY ' . $orderCriteria;
					}

					$sql='select 
							RecordID,
							UserID,
							RecordTime,
							Remarks,
							HowelID,
							InputBy,
							DateInput,
							DateModified,
							ModifiedBy 
						  from MEDICAL_STUDENT_HOWEL where 1=1 '.$conds . $orderBy;

					$rs = $this->objDB->returnResultSet($sql);

					if($loadObject){
						if(count($rs) > 1){
							//since the statusid is the key  ,return resultset cannot more than 1
							  $errMsg = 'SQL Result! support return zero not or one record but failed ['.count($rs).'] '.$sql." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
							  return false;
						}
						
						if(count($rs) ==1){
							$rs = current($rs);
							$this->setRecordID($rs['RecordID']);
							$this->setUserID($rs['UserID']);
							$this->setRecordTime($rs['RecordTime']);
							$this->setRemarks($rs['Remarks']);
							$this->setHowelID($rs['HowelID']);

							$this->setInputBy($rs['InputBy']);
							$this->setDateInput($rs['DateInput']);
							$this->setDateModified($rs['DateModified']);
							
							$this->setModifiedBy($rs['ModifiedBy']);	
							return $this;
						}else{
							return NULL;
						}
					}
					else{
						return $rs;	
					}
				}

				public function save()
				{
					if($this->getRecordID()>0){
						$ret = $this->updateRecord();
					}else{
						$ret = $this->insertRecord();
					}
					return $ret;
				}
				
	
			   /**
				* Return a array with all active status, default is order by status code desc
				* @owner : Fai (20131212)
				* @param : String $orderCriteria (default order by status code desc)
				* @return : Resultset DB array for all active status (not deleted)
				* 
				*/
				public function printStudentSleepLogReport($startDate, $endDate, $userIDList='', $howelCriteria=''){
					
					if($userIDList!=''){
						$userIDCconds = " UserID In ('".implode("','",$userIDList)."')";
					}
					if($howelCriteria!=''){
						$howelCconds = " HowelID In ('".implode("','",$howelCriteria)."')";
					}
					$whereCriteria= ".
							recordtime >=  '{$startDate} 00:00:00'
						And
							recordtime <= '{$endDate} 23:59:59'
						{$userIDCconds}
						{$howelCconds}
					";
					$orderCriteria = 'RecordTime, UserID';
					$rs = $this->loadDataFormStorage($loadObject=false, $whereCriteria,$orderCriteria);
					return $rs;
				}

				private function insertRecord(){
					global $cfg_medical;
					$DataArr = array();
			
					$DataArr['RecordID'] = $this->objDB->pack_value($this->getRecordID(),'int');
					$DataArr['UserID'] = $this->objDB->pack_value($this->getUserID(),'int');
					$DataArr['RecordTime'] = $this->objDB->pack_value($this->getRecordTime(),'date');
					$DataArr['Remarks'] = $this->objDB->pack_value($this->getRemarks(),'str');
					$DataArr['HowelID'] = $this->objDB->pack_value($this->getHowelID(),'int');

//					$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
//					$DataArr['LastModifiedBy'] = $this->objDB->pack_value($this->getLastModifiedBy(),'int');


					$sql = 'Insert Into MEDICAL_STUDENT_HOWEL ('.implode(',',array_keys($DataArr)).') Values (\''.implode('\',\'',array_values($DataArr)).'\')';
					
					$success = $this->objDB->db_db_query($sql);
					if($success != 1){
						  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						  return false;
					}
					
					$RecordID = $this->objDB->db_insert_id();
					$this->setStatusID($RecordID);
					return $RecordID;
				}

				private function updateRecord(){
					global $cfg_medical;

					$DataArr = array();

					$DataArr['RecordID'] = $this->objDB->pack_value($this->getRecordID(),'int');
					$DataArr['UserID'] = $this->objDB->pack_value($this->getUserID(),'int');
					$DataArr['RecordTime'] = $this->objDB->pack_value($this->getRecordTime(),'date');
					$DataArr['Remarks'] = $this->objDB->pack_value($this->getRemarks(),'str');
					$DataArr['HowelID'] = $this->objDB->pack_value($this->getHowelID(),'int');

//					$DataArr['InputBy'] = $this->objDB->pack_value($this->getInputBy(),'int');
					$DataArr['DateModified'] = $this->objDB->pack_value('now()','date');
					$DataArr['ModifiedBy'] = $this->objDB->pack_value($this->getModifiedBy(),'int');

					foreach ($DataArr as $fieldName => $data)
					{
						$updateDetails .= $fieldName."=".$data.",";
					}

					//REMOVE LAST OCCURRENCE OF ",";
					$updateDetails = substr($updateDetails,0,-1);

					$sql = "update MEDICAL_STUDENT_HOWEL set ".$updateDetails." where RecordID = ".$this->getRecordID();

					$result = $this->objDB->db_db_query($sql);
								
					if($result != 1){
						  $errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
			              alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
						  return false;
					}

					return $result;

				}
		}

}
?>