<?php
/*
 * 	Modified:
 * 	Log
 * 	 
 * 	Date: 2017-07-27 [Cameron]
 * 		- change $plugin['medical_module']['discipline'] to $sys_custom['medical']['swapBowelAndSleep']
 * 
 * 	Date: 2017-07-18 [Cameron] 
 * 		- add $plugin['medical_module']['discipline'] to change csv column title [case #F116540]
 * 		
 * 	Date: 2016-05-10 [Cameron] replace split with explode to support php 5.4
 */
 
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
//$TargetFilePath = $_POST["TargetFilePath"];


///////////////////////////////
//////////////new file here
///////////////////////////////

if (!defined("LIBSTUDENTSLEEPIMPORT_DEFINED"))                     // Preprocessor directive
{
	define("LIBSTUDENTSLEEPIMPORT_DEFINED", true);
	class libStudentSleepImport
	{

		function ReadStudentSleepImportData($TargetFilePath)
		{
			global $sys_custom;
			
			if (file_exists($TargetFilePath))
			{
		//		$libfs = new libfilesystem();
				$libimport = new libimporttext();
				$reasonTitle = $sys_custom['medical']['swapBowelAndSleep'] ? 'Place Code' : 'Reason Code';
				$DefaultCsvHeaderArr= array('Class Name','Class Number','Date','Status Code',$reasonTitle,'Frequency','Remarks');
				$ColumnPropertyArr = array(1,1,1,1,1,1,1);	// 0 - Ref Field, not for actual import				
				
				$CsvData = $libimport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
//debug_r($CsvData);
				$CsvHeaderArr = array_shift($CsvData);
				$numOfCsvData = count($CsvData);
				$CsvHeaderWrong = false;
				for($i=0; $i<count($DefaultCsvHeaderArr); $i++) {
					if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i]) {
						$CsvHeaderWrong = true;
						break;
					}
				}
//debug_r($CsvHeaderArr);
//debug_r($DefaultCsvHeaderArr);
//print "header wrong = $CsvHeaderWrong<br>";
				$ret = array();		
//				if ($CsvHeaderWrong)
				$ret["HeaderError"] = $CsvHeaderWrong;
				$ret["NumOfData"] 	= $numOfCsvData;
		//		if($CsvHeaderWrong || $numOfCsvData==0)
		//		{
		//			$ReturnMsgKey = ($CsvHeaderWrong)? 'WrongCSVHeader' : 'CSVFileNoData';
		//			intranet_closedb();
		//			header('location: '.$ReturnPath.'&ReturnMsgKey='.$ReturnMsgKey);
		//			exit();
		//		}				
				$ret["Data"] = $this->ValidateStudentSleepImportData($CsvData);
				return $ret;		
			}
			else
			{
				return false;
			}
		}

		function ValidateStudentSleepImportData($CsvData)
		{
			global $Lang, $medical_cfg;	
			$objMedical = new libMedical();		
			$ret = array();
			
			$AllStatusCode = $objMedical->getAllActiveSleepStatus();
	
			for($i = 0,$iMax = count($CsvData);$i < $iMax; $i++)
			{
				$classname 		= $CsvData[$i][0]; // column 0=> ClassName
				$classnum 		= $CsvData[$i][1]; // column 1=> ClassNumber
				$date 			= $CsvData[$i][2]; // column 2=> Date
				$status 		= $CsvData[$i][3]; // column 3=> Status Code
				$reason 		= $CsvData[$i][4]; // column 4=> Reason Code
				$frequency		= $CsvData[$i][5]; // column 5=> Frequency
				$remarks		= $CsvData[$i][6]; // column 6=> Remarks
				
				$classRs = $objMedical->getClassNameClassNumber(); // All ClassName, ClassNumber (RecordType = 2 And RecordStatus = 1)
				$error = array(); // For holding error message in each row
				
				$date = str_replace('/','-',$date);
				
				//////////////////
				//check name exist
				//////////////////
				$nameExist = false;
				for($j = 0, $jMax = count($classRs); $j < $jMax; $j++)
				{
					if($classRs[$j]['ClassName'] == $classname){
						$nameExist = true;
						break;
					}
				}
				if(!$nameExist)
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidClassName']);
				}
			
				////////////////
				//check classnum 
				////////////////
				$validStudent = true;
				$studentID = $objMedical->getUserIDByClassNameAndNumber($classname, $classnum);
				if($classnum == '' || !$studentID)
				{
					$validStudent = false;
					array_push($error, $Lang['medical']['studentLog']['import']['invalidClassNum']);
				}
				
				////////////////
				//check date 
				////////////////
				$date = $this->convertDateFormat($date);
				if(!$this->checkDateFormat($date))
				{
					array_push($error, $Lang['medical']['studentLog']['import']['invalidDate']);
				}
			
				/////////////////
				//check student attendance
				/////////////////
				if(!libMedicalShowInput::absentAllowedInput('sleep')){
					if($validStudent && $objMedical->getAttendanceStatusOfUser($studentID,$date,'PM') == 0)
					{
						$validStudent = false;
						array_push($error, $Lang['medical']['studentSleep']['import']['invalidAttendance']);
					}
				}
				
				////////////////
				//check status
				////////////////
				
				if(!in_array(array('StatusCode' => $status), $AllStatusCode))
				{
					array_push($error, $Lang['medical']['studentSleep']['import']['invalidStatus']);
				}
				
				////////////////
				//check reason
				////////////////
				$statusID = $objMedical->getSleepIDByCode($status);
				$statusID2 = $this->getStatusIDByReasonCode($reason);
				$isReasonPass = false;
				if($statusID2){
					foreach($statusID2 as $id){
						if($statusID == $id['StatusID']){
							$isReasonPass = true;
							break;
						}
					}
				}
				if(!$isReasonPass)//== '' || $statusID2 != $statusID)
				{
					array_push($error, $Lang['medical']['studentSleep']['import']['invalidReason']);
				}
				////////////////
				//check frequence
				////////////////
				if($frequency != ''){
					$intFrequency = (int)$frequency;
					if( !is_numeric($frequency) || is_float($frequency) || $intFrequency < 1 || $intFrequency > $medical_cfg['general']['student_sleep']['frequency'])
					{
						array_push($error, $Lang['medical']['studentSleep']['import']['invalidFrequency']);
					}
				}else{
					$frequency = '1';
				}
				
				////////////////
				//check Remarks
				////////////////
				/*
				if($remarks == '')
				{
					array_push($error, 'Empty Remarks');
				}
				*/
				
				$pass = !count($error);
				$ret[$i]["pass"] 					= $pass;
				$ret[$i]["reason"] 					= $error;
				$ret[$i]["rawData"]["ClassName"] 	= $classname;
				$ret[$i]["rawData"]["ClassNum"]		= $classnum;
				$ret[$i]["rawData"]["Date"] 		= $date;
				$ret[$i]["rawData"]["Status"] 		= $status;
				$ret[$i]["rawData"]["Reason"] 		= $reason;
				$ret[$i]["rawData"]["Frequency"] 	= $frequency;
				$ret[$i]["rawData"]["Remarks"] 		= $remarks;
				$ret[$i]["rawData"]["Error"] 		= $this->errorToRawHTML($error);
				
			}
			return $ret;	
		}
	
		function convertDateFormat($RecordDate)
		{
			if(substr_count($RecordDate, "-") != 0)
				list($a, $b, $c) = explode('-',$RecordDate);
			else 
				list($a, $b, $c) = explode('/',$RecordDate);
			
			if(strlen($a) == 4){
				return sprintf("%04d/%02d/%02d",$a,$b,$c);
			}
				return sprintf("%04d/%02d/%02d",$c,$b,$a);
		}
		
		function checkDateFormat($RecordDate)
		{
			if(substr_count($RecordDate, "-") != 0)
				list($year, $month, $day) = explode('-',$RecordDate);
			else 
				list($year, $month, $day) = explode('/',$RecordDate);
								
			if((strlen($year)==4 && strlen($month)==2 && strlen($day)==2))
			{
				if(checkdate($month,$day,$year))
				{
					return true;
				}
			}
			return false;
		}
		
		function checkDurationFormat($RecordTime)
		{
			list($minutes, $second) = explode(':',$RecordTime);
			if(strlen($minutes)==2 && strlen($second)==2)
			{
				if($minutes>=0 && $minutes<=59 && $second>=0 && $second<=59)
				{
					return true;
				}
			}
			return false;
		}
		
		function errorToRawHTML($errorArray)
		{
			if(empty($errorArray))
			{
				return '';
			}
			
			$raw = '*)' . $errorArray[0];
			for($i = 1,$iMax = count($errorArray); $i < $iMax; $i++)
			{
				$raw .= '<br />*)' . $errorArray[$i];
			}
			return $raw;
		}
		
		
		
	
		// Add active-checking to the SQL, by Pun
		function getStatusIDByReasonCode($code) // to libmedical.php
		{
			if ($code == "")
			{
				return false;
			}
			
			$sql = "SELECT StatusID FROM MEDICAL_SLEEP_STATUS_REASON WHERE ReasonCode='$code' AND recordstatus = 1 AND DeletedFlag = 0";
			$objdb = new libdb();
			$rs = $objdb->returnResultSet($sql);
//			$rs = $this->objDB->returnResultSet($sql);
			return $rs;
		}
		
		
		
		
		
		
		
	}//close class
}



?>