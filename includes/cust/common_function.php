<?php
// using: Pun
function alert_error_log($projectName='',$errorMsg='',$errorType=''){
		global $plugin,$system_env;

//        $mail[] = 'mfkhoe@broadlearning.com';
        $mail[] = 'cameronlau@broadlearning.com';
//        $mail[] = 'adamwan@g4.broadlearning.com';
//        $mail[] = 'hpmak@g2.broadlearning.com';
//		$mail[] = 'ryanchan@g2.broadlearning.com';

        $_env = ($system_env['ENV'] == '') ? ' Live ' : $system_env['ENV'];

		if($plugin['alert_error_log'])
		{		
			mail(implode(',',$mail)," Env [".$_env."] Project {$projectName} - ".date("F j, Y, g:i a")."\n",$errorMsg);
		}

        return true;
}

function hasDailyLogTableByName($tableName){
	$objDB = new libdb();
//	$tableName = 'MEDICAL_STUDENT_DAILY_MEAL_LOG_'.date('Y', strtotime($date)).'_'.date('m', strtotime($date));
	$sql = 'Show Tables like "'.$tableName.'"';
	$rs = $objDB->returnResultSet($sql);
	return count($rs[0])>0;
}


function studentAttendanceToTempTable($startDate/* YYYY-MM-DD */,$endDate){
	$objDB = new libdb();
	$sql = 'DROP TABLE IF EXISTS TEMP_STUDENT_ATTENDANCE';
	$objDB->db_db_query($sql);
	$sql = 'CREATE TEMPORARY TABLE TEMP_STUDENT_ATTENDANCE(
		UserID int(11) NOT NULL,
		RECORD_DAY DATE NOT NULL,
		AMStatus int(11) default NULL,
		PMStatus int(11) default NULL,
		PRIMARY KEY (UserID,RECORD_DAY)
	)ENGINE=MyISAM';
	$objDB->db_db_query($sql);
	
	$yearMonth = returnListOfYearMonth($startDate,$endDate);
	
	foreach ($yearMonth as $ym) {
		$year = substr($ym, 0, 4);
		$month = substr($ym, 5);
		$sql = "INSERT INTO
			TEMP_STUDENT_ATTENDANCE
		(
			UserID,
			RECORD_DAY,
			AMStatus,
			PMStatus
		) SELECT 
			UserID,
			CONCAT('{$year}-{$month}-', DayNumber),
			AMStatus,
			PMStatus
		FROM 
			CARD_STUDENT_DAILY_LOG_{$ym}
		";
		$objDB->db_db_query($sql);
		$sql = "SELECT 
			UserID,
			CONCAT('{$year}-{$month}-', DayNumber),
			AMStatus,
			PMStatus
		FROM 
			CARD_STUDENT_DAILY_LOG_{$ym} where userid=4291";
	$rs = $objDB->returnResultSet($sql);
//	debug_r($rs);
	
	}
	
	$sql = 'SELECT * FROM TEMP_STUDENT_ATTENDANCE where userid=4291';
	$rs = $objDB->returnResultSet($sql);	
	//debug_r($rs);
	
}

/**
* Generate a list with year and month (return with Array)
* @owner : Fai (20131210)
* @param : String $startDate , start Date , Must be with format YYYY-MM-DD
* @param : String $endDate , end Date , Must be with format YYYY-MM-DD
* @return : ARRAY of string eg [0] = '2013_11', [1] = '2013_12',[2] = '2014_01',[3] = '2014_02'
* 
*/
function returnListOfYearMonth($startDate,$endDate)
{
//	$s_date = strtotime($startDate);
//	$e_date = strtotime($endDate);
//	$months = 0;
//
//	$returnAry = array();
//	
//	while (($s_date = strtotime('+1 MONTH', $s_date)) <= $e_date)
//	{
//		$months++;
//	}
//	for($i = 0;$i<=$months; $i++)
//	{
//		$returnAry[] = date ('Y_m' , strtotime ($startDate.' +'.$i.' month') );
//	}
//	return $returnAry;
		
	
	$s_date = date ('Y-m-01',strtotime($startDate));
	$e_date = date ('Y-m-01',strtotime($endDate));


	$returnAry = array();
	$returnAry[] = date ('Y_m' , strtotime ($s_date));

	$n_date = date('Y-m-d', strtotime(date("Y-m-01", strtotime($s_date)) . " +1 month"));

//	$i=0;

	while ($n_date <= $e_date)
	{
//		if(++$i>10)
//			return false;
		$returnAry[] = date ('Y_m' , strtotime ($n_date) );
		$n_date = date('Y-m-d', strtotime(date("Y-m-01", strtotime($n_date)) . " +1 month"));
	}
	
	return $returnAry;
}


function getCommonSelectionBox($id, $name, $valueArray, $valueSelected="", $class="", $otherFeatures=""){
	$returnStr = '';
	$returnStr .= "<select class='{$class}' id='{$id}' name='{$name}' {$otherFeatures}>";
	foreach( (array)$valueArray as $key=>$valueItem){
			$selected ='';
			if( (string)$key === $valueSelected){
				$selected ='selected';
			}
			$returnStr .= "<option $selected value='{$key}'>{$valueItem}</option>";
	}
	$returnStr .= "</select>";
	
	return $returnStr;
}

// Get UserName by UserID and session language
function getUserInfoByLang($userID)
{
	if ($userID)
	{
		$objDB = new libdb();
		$sql = "SELECT ".getNameFieldByLang2()." as UserName, RecordType FROM INTRANET_USER WHERE UserID = '".$userID."'";
		$rs = $objDB->returnResultSet($sql);
		if (count($rs) == 1)
		{
			return $rs[0];
		}
		else
		{
			return false;
		}		
	}
	else
	{
		return false;
	}
	
}

/**
 * Input StartAcademicYearID and EndAcademicYearID
 * Output the AcademicYearID list from start to end (inclusive).
 */
function getAcademicYearIdList($startAcademicYearID, $endAcademicYearID){
	global $PATH_WRT_ROOT;
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	$start_academic_year =  new academic_year($startAcademicYearID);
	$end_academic_year =  new academic_year($endAcademicYearID);
	
	$startDate = $start_academic_year->AcademicYearStart;
	$endDate = $end_academic_year->AcademicYearEnd;
	$objDB = new libdb();
	
	$sql = "SELECT 
		DISTINCT AcademicYearID
	FROM 
		ACADEMIC_YEAR_TERM 
	WHERE
		TermStart BETWEEN '{$startDate}' AND '{$endDate}'
	ORDER BY 
		TermStart";
	$rs = $objDB->returnVector($sql);

	return $rs;
}

/**
 * Get the AcademicYear name with lang 
 */
function getAcademicYearNameList($AcademicYearIdArr){
	$AcademicYearIdArr = (array)IntegerSafe($AcademicYearIdArr);
	$AcademicYearIdList = implode("','", $AcademicYearIdArr);
	
	$objDB = new libdb();
	$sql = "SELECT 
		AcademicYearID,
		YearNameB5,
		YearNameEN
	FROM 
		ACADEMIC_YEAR
	WHERE 
		AcademicYearID IN ('{$AcademicYearIdList}')";
	$rs = $objDB->returnResultSet($sql);
	
	$nameList = array();
	foreach($rs as $r){
		$nameList[$r['AcademicYearID']] = Get_Lang_Selection($r['YearNameB5'], $r['YearNameEN']);
	}
	return $nameList;
}

function getAcademicYearIdByDate($date){
	$objDB = new libdb();
	
	$sql = "SELECT 
		AcademicYearID
	FROM 
		ACADEMIC_YEAR_TERM
	WHERE
		'{$date}' BETWEEN TermStart AND TermEnd
	ORDER BY 
		TermStart";
	$rs = $objDB->returnVector($sql);

	return $rs[0];
}

function getNameByUserID($userIdArr){
	$userIdArr = (array)IntegerSafe($userIdArr);
	$objDB = new libdb();
	
	$userIdList = implode("','", $userIdArr);
	$nameField = getNameFieldByLang2('IU.').' As Name';
	$sql = "SELECT
		IU.UserID,
		{$nameField}
	FROM 
		INTRANET_USER IU
	WHERE
		IU.UserID IN ('{$userIdList}')";
	$rs = $objDB->returnResultSet($sql);
	
	$nameList = array();
	foreach($rs as $r){
		$nameList[ $r['UserID'] ] = $r['Name'];
	}
	return $nameList;
}

?>