<?php
// updating : Pun
/**
 * Change Log:
 * 2016-01-18 (Pun)
 *  - Modified checkDepositAccessRight(), always return false to hide this module
 */
if (!defined("LIBMsschPRINTING_ACCESSRIGHT_DEFINED"))                     // Preprocessor directive
{
	define("LIBMsschPRINTING_ACCESSRIGHT_DEFINED", true);
	class libMsschPrintingAccessRight
	{
		public static function checkPrintImportAccessRight($userID = ''){
			if(empty($UserID)){
				$UserID = $_SESSION['UserID'];
			}
			
			if(self::isSuperAdmin()){
				return true;
			}
			return false;
		}
		
		public static function checkDepositAccessRight($userID = ''){
			return false; // Since the client don't use this function, so we hide it.
			
			if(empty($UserID)){
				$UserID = $_SESSION['UserID'];
			}
			
			if(self::isSuperAdmin()){
				return true;
			}
			return false;
		}
		
		public static function checkPrintingSummaryAccessRight($userID = ''){
			if(empty($UserID)){
				$UserID = $_SESSION['UserID'];
			}
			
			if(self::isSuperAdmin()){
				return true;
			}
			return false;
		}
		
		public static function checkGroupSettingAccessRight($GroupID = '', $UserID = ''){
			global $objDB, $cfg_msschPrint;
			
			if(self::isSuperAdmin()){
				return true;
			}
			
			return $_SESSION['UserType'] == USERTYPE_STAFF;
			
			/*
			$GroupID = IntegerSafe($GroupID);
			$UserID = IntegerSafe($UserID);
			if(empty($UserID)){
				$UserID = $_SESSION['UserID'];
			}
			if($GroupID){
				$groupFilter = "AND PGM.GROUP_ID = '{$GroupID}'";
			}
			
			$sql = "SELECT 
				*
			FROM
				PRINTING_GROUP PG
			INNER JOIN
				PRINTING_GROUP_MEMBER PGM
			ON
				PG.GROUP_ID = PGM.GROUP_ID
			WHERE
				PGM.UserID = '{$UserID}'
			AND
				PGM.MEMBER_TYPE = '{$cfg_msschPrint['group']['memberType']['PIC']}'
				{$groupFilter}
			";
			$rs = $objDB->returnResultSet($sql);
			return (count($rs) == 1);
			*/
		}
		
		
		public static function isSuperAdmin($userID = ''){
			if(empty($UserID)){
				$UserID = $_SESSION['UserID'];
			}
			
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-msschPrinting"]){
				return true;
			}
			return false;
		}
	} // End class
}
?>