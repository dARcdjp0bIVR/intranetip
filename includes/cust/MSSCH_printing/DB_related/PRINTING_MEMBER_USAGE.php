<?php
// updating : Pun
if (!defined("LIB_PRINTING_MEMBER_USAGE_DEFINED"))                     // Preprocessor directive
{
	define("LIB_PRINTING_MEMBER_USAGE_DEFINED", true);
	class PRINTING_MEMBER_USAGE{
		
		public static function insertUsage($RECORD_ID, $academicYearID, $AMOUNT){
			global $objDB, $cfg_msschPrint;
			
			$RECORD_ID = IntegerSafe($RECORD_ID);
			$academicYearID = IntegerSafe($academicYearID);
			$AMOUNT = trim( htmlentities( $AMOUNT , ENT_QUOTES, 'UTF-8'));
			
			//////// Get members START ////////
			$sql = "SELECT
				PGM.UserID
			FROM 
				PRINTING_GROUP_USAGE PGU
			INNER JOIN
				PRINTING_GROUP_MEMBER PGM
			ON
				PGU.GROUP_ID = PGM.GROUP_ID
			WHERE
				PGU.RECORD_ID = '{$RECORD_ID}'
			AND
				PGM.MEMBER_TYPE = '{$cfg_msschPrint['group']['memberType']['Member']}'";
			$rs = $objDB->returnVector($sql);
			//////// Get members END ////////
			
			//////// Delete all records START ////////
			$result = self::deleteAllMemberUsage($RECORD_ID);
			//////// Delete all records END ////////
			
			//////// Save data START ////////
			if($result){
				$allSuccess = true;
				$errSQL = array();
				
				foreach((array)$rs as $uid){
					$sql = "INSERT INTO
						PRINTING_MEMBER_USAGE
					(
						RECORD_ID,
						AcademicYearID,
						UserID,
						AMOUNT,
						DATE_INPUT,
						INPUT_BY
					) VALUES (
						'{$RECORD_ID}',
						'{$academicYearID}',
						'{$uid}',
						'{$AMOUNT}',
						NOW(),
						'{$_SESSION['UserID']}'
					)";
					$result = $objDB->db_db_query($sql);
					if($result != 1){
						$allSuccess = false;
						$errSQL[] = $sql;
					}
				}
				if(!$allSuccess){
						$errSQL = implode('<br />', $errSQL);
						$errMsg = 'SQL Error! <br /><br />'.$errSQL.'<br /><br /> error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
						//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
						return false;
				}
				return $allSuccess;
			}
			return false;
		}
		
		public static function deleteAllMemberUsage($RECORD_ID){
			global $objDB, $cfg_msschPrint;
			
			$RECORD_ID = (array)IntegerSafe($RECORD_ID);
			$RECORD_ID = array_filter($RECORD_ID); // Remove all '0' elements
			$RECORD_ID = implode("','",$RECORD_ID);
			
			$sql = "DELETE FROM 
				PRINTING_MEMBER_USAGE
			WHERE
				RECORD_ID IN ('{$RECORD_ID}')";
			
			$result = $objDB->db_db_query($sql);

			if($result != 1){
				$errMsg = 'SQL Error! <br />'.$sql.'<br /> error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
				return false;
			}
			return $result;
		}
		
		public static function getUserPrintingRecord($userID, $academicYearID){
			global $objDB, $cfg_msschPrint;
			
			$userID = IntegerSafe($userID);
			$academicYearID = (array)IntegerSafe($academicYearID);
			
			$printingRecord = array();
			foreach($academicYearID as $yearID){
				$sql = "SELECT
					PGU.GROUP_ID,
					MONTH(PGU.RECORD_DATE) AS MONTH,
					--PG.NAME AS GROUP_NAME,
					PMU.AMOUNT
				FROM 
					PRINTING_MEMBER_USAGE PMU
				INNER JOIN
					PRINTING_GROUP_USAGE PGU
				ON
					PMU.RECORD_ID = PGU.RECORD_ID
				INNER JOIN
					PRINTING_GROUP PG
				ON
					PGU.GROUP_ID = PG.GROUP_ID
				WHERE 
					PMU.AcademicYearID = '{$yearID}'
				AND 
					PMU.UserID = '{$userID}'
				ORDER BY 
					PGU.RECORD_DATE,
					PG.NAME";
				$rs = $objDB->returnResultSet($sql);
				foreach($rs as $r){
					$printingRecord[ $yearID ][ $r['MONTH'] ][ $r['GROUP_ID'] ] = $r['AMOUNT'];
				}
			}
			return $printingRecord;
		}
	} // End class
	
}