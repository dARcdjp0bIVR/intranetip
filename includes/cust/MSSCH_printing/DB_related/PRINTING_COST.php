<?php
// updating : Pun
if (!defined("LIB_PRINTING_COST_DEFINED"))                     // Preprocessor directive
{
	define("LIB_PRINTING_COST_DEFINED", true);
	class PRINTING_COST{
		public $COST_ID;
		public $AcademicYearID;
		public $YearMonth;
		public $Type;
		public $Cost;
        public $DATE_INPUT;
        public $INPUT_BY;

		public function setCOST_ID($COST_ID) { $this->COST_ID = $COST_ID; }
		public function getCOST_ID() { return $this->COST_ID; }
		public function setAcademicYearID($AcademicYearID) { $this->AcademicYearID = $AcademicYearID; }
		public function getAcademicYearID() { return $this->AcademicYearID; }
        public function setYearMonth($yearMonth) { $this->YearMonth = $yearMonth; }
        public function getYearMonth() { return $this->YearMonth; }
        public function setType($type) { $this->Type = $type; }
        public function getType() { return $this->Type; }
        public function setCost($cost) { $this->Cost = $cost; }
        public function getCost() { return $this->Cost; }

        private function setDATE_INPUT($DATE_INPUT) { $this->DATE_INPUT = $DATE_INPUT; }
        public function getDATE_INPUT() { return $this->DATE_INPUT; }
        private function setINPUT_BY($INPUT_BY) { $this->INPUT_BY = $INPUT_BY; }
        public function getINPUT_BY() { return $this->INPUT_BY; }


		public function __construct($COST_ID){
            if($COST_ID){
                $this->setCOST_ID($COST_ID);
                $this->loadFromStorage();
            }else{
                $this->setDefaultValue();
            }
        }

		public function save(){
		    return $this->delete() && $this->insert();
		}

		private function insert(){
			global $objDB;
			if($this->Cost === ''){
			    return true;
            }
			$DataArr = array();

			$DataArr['AcademicYearID'] = $objDB->pack_value($this->AcademicYearID,'int');
			$DataArr['YearMonth'] = $objDB->pack_value($this->YearMonth,'str');
			$DataArr['Type'] = $objDB->pack_value($this->Type,'str');
			$DataArr['Cost'] = $objDB->pack_value($this->Cost,'str');
			$DataArr['DATE_INPUT'] = $objDB->pack_value('now()','date');
			$DataArr['INPUT_BY'] = $objDB->pack_value($_SESSION['UserID'],'int');

			$key = implode(",",array_keys($DataArr));
			$value = implode(",",array_values($DataArr));
			$sql = "INSERT INTO PRINTING_COST ({$key}) VALUES ($value)";
			$result = $objDB->db_db_query($sql);

			if($result != 1){
					$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
					//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
					return false;
			}
			$this->COST_ID = $objDB->db_insert_id();

			#### Log START ####
            $DataArr['COST_ID'] = $this->COST_ID;
            $key = implode(",",array_keys($DataArr));
            $value = implode(",",array_values($DataArr));
            $sql = "INSERT INTO PRINTING_COST_LOG ({$key}) VALUES ($value)";
            $objDB->db_db_query($sql);
			#### Log END ####

			return $this->COST_ID;
		}

		private function delete(){
			global $objDB;

			$DataArr = array();

			$DataArr['AcademicYearID'] = $objDB->pack_value($this->AcademicYearID,'int');
			$DataArr['YearMonth'] = $objDB->pack_value($this->YearMonth,'str');
			$DataArr['Type'] = $objDB->pack_value($this->Type,'str');

			$sql = "DELETE FROM PRINTING_COST WHERE AcademicYearID={$DataArr['AcademicYearID']} AND YearMonth={$DataArr['YearMonth']} AND Type={$DataArr['Type']}";
			$result = $objDB->db_db_query($sql);

			if($result != 1){
					$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
					//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
					return false;
			}
			return $result;
		}

		private function loadFromStorage(){
			global $objDB;

			$sql = "SELECT
                *
			FROM 
				PRINTING_COST
			WHERE 
				COST_ID = {$this->COST_ID}";
			$rs = $objDB->returnResultSet($sql);

			if(count($rs) != 1){
				$this->setDefaultValue();
				return;
			}

			$rs = current($rs);
			$this->COST_ID = $rs['COST_ID'];
			$this->AcademicYearID = $rs['AcademicYearID'];
			$this->YearMonth = $rs['YearMonth'];
			$this->Type = $rs['Type'];
			$this->Cost = $rs['Cost'];
			$this->DATE_INPUT = $rs['DATE_INPUT'];
			$this->INPUT_BY = $rs['INPUT_BY'];

			$this->setDefaultValue();
			return true;
		}

		// Null object pattern
		private function NullObject(){
			$obj = new self();
			$obj->setDefaultValue();
			return $obj;
		}
		private function setDefaultValue(){
            if(!$this->COST_ID)
				$this->COST_ID = 0;
            if(!$this->AcademicYearID)
                $this->AcademicYearID = Get_Current_Academic_Year_ID();
            if(!$this->YearMonth)
                $this->YearMonth = '';
            if(!$this->Type)
				$this->Type = '';
            if(!$this->Cost)
				$this->Cost = '0.000';
			if(!$this->DATE_INPUT)
				$this->DATE_INPUT = '1970-01-01 00:00:00';
			if(!$this->INPUT_BY)
				$this->INPUT_BY = 0;
		}

	} // End class

}
