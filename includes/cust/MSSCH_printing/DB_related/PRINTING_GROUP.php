<?php
// updating : Pun
if (!defined("LIB_PRINTING_GROUP_DEFINED"))                     // Preprocessor directive
{
	define("LIB_PRINTING_GROUP_DEFINED", true);
	class PRINTING_GROUP{
		public $GROUP_ID;
		public $AcademicYearID;
		public $MFP_CODE;
		public $NAME;
		public $DATE_INPUT;
		public $INPUT_BY;
		public $DATE_MODIFIED;
		public $MODIFIED_BY;
		public $SUBJECT_GROUP_ID;

		private function setGROUP_ID($GROUP_ID) { $this->GROUP_ID = $GROUP_ID; }
		public function getGROUP_ID() { return $this->GROUP_ID; }
		public function setAcademicYearID($AcademicYearID) { $this->AcademicYearID = $AcademicYearID; }
		public function getAcademicYearID() { return $this->AcademicYearID; }
		public function setMFP_CODE($MFP_CODE) { $this->MFP_CODE = $MFP_CODE; }
		public function getMFP_CODE() { return $this->MFP_CODE; }
		public function setNAME($NAME) { $this->NAME = $NAME; }
		public function getNAME() { return $this->NAME; }
		private function setDATE_INPUT($DATE_INPUT) { $this->DATE_INPUT = $DATE_INPUT; }
		public function getDATE_INPUT() { return $this->DATE_INPUT; }
		private function setINPUT_BY($INPUT_BY) { $this->INPUT_BY = $INPUT_BY; }
		public function getINPUT_BY() { return $this->INPUT_BY; }
		private function setDATE_MODIFIED($DATE_MODIFIED) { $this->DATE_MODIFIED = $DATE_MODIFIED; }
		public function getDATE_MODIFIED() { return $this->DATE_MODIFIED; }
		private function setMODIFIED_BY($MODIFIED_BY) { $this->MODIFIED_BY = $MODIFIED_BY; }
		public function getMODIFIED_BY() { return $this->MODIFIED_BY; }
        public function setSUBJECT_GROUP_ID($SUBJECT_GROUP_ID) { $this->SUBJECT_GROUP_ID = $SUBJECT_GROUP_ID; }
		public function getSUBJECT_GROUP_ID() { return $this->SUBJECT_GROUP_ID; }


		public function __construct($GROUP_ID){
			if($GROUP_ID){
				$this->setGROUP_ID($GROUP_ID);
				$this->loadFromStorage();
			}else{
				$this->setDefaultValue();
			}
		}
		
		public function save(){
			if($this->GROUP_ID){
				return $this->update();
			}
			return $this->insert();
		}
		
		private function insert(){
			global $objDB;
			
			$DataArr = array();

			$DataArr['AcademicYearID'] = $objDB->pack_value($this->AcademicYearID,'int');
			$DataArr['MFP_CODE'] = $objDB->pack_value($this->MFP_CODE,'str');
			$DataArr['NAME'] = $objDB->pack_value($this->NAME,'str');
			$DataArr['DATE_INPUT'] = $objDB->pack_value('now()','date');
			$DataArr['INPUT_BY'] = $objDB->pack_value($_SESSION['UserID'],'int');
			$DataArr['DATE_MODIFIED'] = $objDB->pack_value('now()','date');
			$DataArr['MODIFIED_BY'] = $objDB->pack_value($_SESSION['UserID'],'int');
			$DataArr['SUBJECT_GROUP_ID'] = $objDB->pack_value($this->SUBJECT_GROUP_ID,'int');

			$key = implode(",",array_keys($DataArr));
			$value = implode(",",array_values($DataArr));
			$sql = "INSERT INTO PRINTING_GROUP ({$key}) VALUES ($value)";

			$result = $objDB->db_db_query($sql);
			if($result != 1){
					$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
					//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
					return false;
			}
			$this->GROUP_ID = $objDB->db_insert_id();
			return $this->GROUP_ID;
		}
		
		private function update(){
			global $objDB;
			
			$DataArr = array();

			$DataArr['AcademicYearID'] = $objDB->pack_value($this->AcademicYearID,'int');
			$DataArr['MFP_CODE'] = $objDB->pack_value($this->MFP_CODE,'str');
			$DataArr['NAME'] = $objDB->pack_value($this->NAME,'str');
			$DataArr['DATE_MODIFIED'] = $objDB->pack_value('now()','date');
			$DataArr['MODIFIED_BY'] = $objDB->pack_value($_SESSION['UserID'],'int');
            $DataArr['SUBJECT_GROUP_ID'] = $objDB->pack_value($this->SUBJECT_GROUP_ID,'int');

			$updateStr = '';
			foreach($DataArr as $key=>$value){
				$updateStr .= $key.'='.$value.',';
			}
			$updateStr = trim($updateStr, ',');

			$sql = "UPDATE PRINTING_GROUP SET {$updateStr} WHERE GROUP_ID={$this->GROUP_ID}";
			$result = $objDB->db_db_query($sql);
			if($result != 1){
					$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
					//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
					return false;
			}
			
			return $this->GROUP_ID;
		}
		
		public function delete(){
			global $objDB;
			$sql = "DELETE FROM PRINTING_GROUP WHERE GROUP_ID={$this->GROUP_ID}";
			$result = $objDB->db_db_query($sql);
			if($result != 1){
					$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
					//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
					return false;
			}
			return $result;
		}
		
		
		
		private function loadFromStorage(){
			global $objDB;
			
			$sql = "SELECT
				GROUP_ID,
				MFP_CODE,
				NAME,
				DATE_INPUT,
				INPUT_BY,
				DATE_MODIFIED,
				MODIFIED_BY,
                SUBJECT_GROUP_ID
			FROM 
				PRINTING_GROUP
			WHERE 
				GROUP_ID = {$this->GROUP_ID}";
			$rs = $objDB->returnResultSet($sql);

			if(count($rs) != 1){
				$this->setDefaultValue();
				return;
			}
		
			$rs = current($rs);

			$this->GROUP_ID = $rs['GROUP_ID'];
			$this->MFP_CODE = $rs['MFP_CODE'];
			$this->NAME = $rs['NAME'];
			$this->DATE_INPUT = $rs['DATE_INPUT'];
			$this->INPUT_BY = $rs['INPUT_BY'];
			$this->DATE_MODIFIED = $rs['DATE_MODIFIED'];
			$this->MODIFIED_BY = $rs['MODIFIED_BY'];
			$this->SUBJECT_GROUP_ID = $rs['SUBJECT_GROUP_ID'];

			$this->setDefaultValue();
			return true;
		}
		
		// Null object pattern
		private function NullObject(){
			$obj = new self();
			$obj->setDefaultValue();
			return $obj;
		}
		private function setDefaultValue(){
			if(!$this->GROUP_ID)
				$this->GROUP_ID = 0;
			if(strlen($this->MFP_CODE) == 0)
				$this->MFP_CODE = '';
			if(strlen($this->NAME) == 0)
				$this->NAME = '';
			if(!$this->DATE_INPUT)
				$this->DATE_INPUT = '1970-01-01 00:00:00';
			if(!$this->INPUT_BY)
				$this->INPUT_BY = 0;
			if(!$this->DATE_MODIFIED)
				$this->DATE_MODIFIED = '1970-01-01 00:00:00';
			if(!$this->MODIFIED_BY)
				$this->MODIFIED_BY = 0;
			if(!$this->SUBJECT_GROUP_ID)
				$this->SUBJECT_GROUP_ID = 0;
		}
		
	} // End class
	
}