<?php
// updating : Pun
if (!defined("LIB_PRINTING_BATCH_DEPOSIT_DEFINED"))                     // Preprocessor directive
{
	define("LIB_PRINTING_BATCH_DEPOSIT_DEFINED", true);
	class PRINTING_BATCH_DEPOSIT{
		
		public static function insertDeposit($title, $amount){
			global $objDB;
			
			$title = trim( htmlentities( $title , ENT_QUOTES, 'UTF-8'));
			$amount = trim( htmlentities( $amount , ENT_QUOTES, 'UTF-8'));
			$currentAcademicYearID = Get_Current_Academic_Year_ID();
			
			$sql = "INSERT INTO PRINTING_BATCH_DEPOSIT (
				AcademicYearID,
				TITLE,
				AMOUNT, 
				DATE_INPUT, 
				INPUT_BY
			) VALUES (
				'{$currentAcademicYearID}',
				'{$title}',
				'{$amount}',
				NOW(),
				'{$_SESSION['UserID']}'
			)";
			$result = $objDB->db_db_query($sql);
			
			if($result != 1){
				$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
				return false;
			}
			
			return $objDB->db_insert_id();
		}
		public static function deleteDeposit($BATCH_ID){
			global $objDB;
			
			$BATCH_ID = IntegerSafe($BATCH_ID);
			$batchIdArr = implode("','", (array)$BATCH_ID);
			
			$sql = "DELETE FROM 
				PRINTING_BATCH_DEPOSIT
			WHERE
				BATCH_ID IN ('{$batchIdArr}')
			";
			error_log('$sql -->'.print_r($sql,true)."<----\n\t".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
			
			$result = $objDB->db_db_query($sql);
			if($result != 1){
				$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
				return false;
			}
			return $result;
		}
		
		
		
	} // End class
	
}