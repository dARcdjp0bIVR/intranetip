<?php
// updating : Pun
if (!defined("LIB_PRINTING_USER_DEPOSIT_DEFINED"))                     // Preprocessor directive
{
	define("LIB_PRINTING_USER_DEPOSIT_DEFINED", true);
	class PRINTING_USER_DEPOSIT{
		
		public static function insertDeposit($batchID, $amount, $userID){
			global $objDB;
			
			$batchID = IntegerSafe($batchID);
			$amount = trim( htmlentities( $amount , ENT_QUOTES, 'UTF-8'));
			$userID = (array)IntegerSafe($userID);
			$userID = array_filter($userID); // Remove all '0' elements
			
			$allSuccess = true;
			$errSQL = array();

			$currentAcademicYearID = Get_Current_Academic_Year_ID();
			foreach($userID as $user){
				$sql = "INSERT INTO PRINTING_USER_DEPOSIT (
					UserID, 
					BATCH_ID, 
					AMOUNT, 
					DATE_INPUT, 
					INPUT_BY,
					AcademicYearID
				) VALUES (
					'{$user}',
					'{$batchID}',
					'{$amount}',
					NOW(),
					'{$_SESSION['UserID']}',
					'{$currentAcademicYearID}'
				)";
				$result = $objDB->db_db_query($sql);
				if($result != 1){
					$allSuccess = false;
					$errSQL[] = $sql;
				}
			}
			
			if(!$allSuccess){
					$errSQL = implode('<br />', $errSQL);
					$errMsg = 'SQL Error! <br /><br />'.$errSQL.'<br /><br /> error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
					//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
					return false;
			}
			return $allSuccess;
		}
		public static function deleteDepositByBatchID($BATCH_ID){
			global $objDB;
			
			$BATCH_ID = IntegerSafe($BATCH_ID);
			$batchIdArr = implode("','", (array)$BATCH_ID);
			
			$sql = "DELETE FROM 
				PRINTING_USER_DEPOSIT
			WHERE
				BATCH_ID IN ('{$batchIdArr}')
			";
			error_log('$sql -->'.print_r($sql,true)."<----\n\t".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");
			
			$result = $objDB->db_db_query($sql);
			if($result != 1){
				$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				//alert_error_log($projectName='MSSCH_Printing',$errMsg,$errorType='');
				return false;
			}
			return $result;
		}
		
		public static function getUserDeposit($userID, $academicYearID){
			global $objDB, $cfg_msschPrint;
			
			$userID = IntegerSafe($userID);
			$academicYearID = (array)IntegerSafe($academicYearID);
			$academicYearID = implode("','",$academicYearID);
			
			$nameField = getNameFieldByLang2("IU.")." As INPUT_BY";
			
			$sql = "SELECT
				PUD.DATE_INPUT,
				PUD.AMOUNT,
				{$nameField}
			FROM 
				PRINTING_USER_DEPOSIT PUD
			INNER JOIN
				INTRANET_USER IU
			ON 
				PUD.INPUT_BY = IU.UserID
			WHERE 
				PUD.AcademicYearID IN ('{$academicYearID}')
			AND 
				PUD.UserID = '{$userID}'";
			$rs = $objDB->returnResultSet($sql);
			return $rs;
		}
		
		public static function getUserIDsByBatchID($batchID){
			global $objDB;
			
			$batchID = IntegerSafe($batchID);
			
			$userArr = array();
			$sql = "SELECT UserID FROM PRINTING_USER_DEPOSIT WHERE BATCH_ID IN ('$batchID')";
			$rs = $objDB->returnVector($sql);
			return $rs;
		}
		
	} // End class
	
}