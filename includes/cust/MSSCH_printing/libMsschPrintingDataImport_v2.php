<?php
// Using:
/**
 * Change Log:
 * 2016-01-13 Pun [91242]
 *    - Modified processImportData(), fixed floting point subtract error
 */

include_once($PATH_WRT_ROOT . "includes/libimporttext.php");
include_once($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once($PATH_WRT_ROOT . "includes/libimport.php");
include_once($PATH_WRT_ROOT . "includes/libftp.php");


///////////////////////////////
//////////////new file here
///////////////////////////////

if (!defined("LIB_MSSCH_PRINTING_DATA_IMPORT_DEFINED"))                     // Preprocessor directive
{
    define("LIB_MSSCH_PRINTING_DATA_IMPORT_DEFINED", true);

    class libMsschPrintingDataImport
    {

        function ReadImportData($TargetFilePath, $AcademicYearId=0)
        {
            if (!file_exists($TargetFilePath)) {
                return false;
            }
            $libimport = new libimporttext();

            $DefaultCsvHeaderArr = array('lastSyncTime', 'code01', 'code02', 'code03', 'code04', 'jobType', 'jobTypeDetail', 'status', 'colorMode', 'pages', 'sheet', 'paperSize', 'statusReason');
            $ColumnPropertyArr = array(1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0);    // 0 - Ref Field, not for actual import

            $CsvData = $libimport->GET_IMPORT_TXT($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
            $CsvHeaderArr = array_shift($CsvData);
            $CsvHeaderWrong = false;
            for ($i = 0; $i < count($DefaultCsvHeaderArr); $i++) {
                if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i]) {
                    $CsvHeaderWrong = true;
                    break;
                }
            }

            $numOfEmptyMfpData = 0;
            foreach($CsvData as $d){
                if($d[3] == '' && $d[4] == ''){
                    $numOfEmptyMfpData++;
                }
            }

            $CsvData = $this->processImportData($CsvData); // Subtract the old value of the amount
            $numOfCsvData = count($CsvData);


            $ret = array();
            $ret["HeaderError"] = $CsvHeaderWrong;
            $ret["NumOfData"] = $numOfCsvData;
            $ret["NumOfEmptyMfpData"] = $numOfEmptyMfpData;

            $ret["Data"] = $this->ValidateImportData($CsvData, $AcademicYearId);

            return $ret;
        }

        function processImportData($data)
        {
            $result = array();
            foreach ($data as $index => $d) {
                if (($d[3] == '' && $d[4] == '') || $d[9] == 0 || $d[10] == 0) {
                    continue;
                }
                $result[$index] = $d;
            }
            return $result;
        }

        function ValidateImportData($CsvData, $AcademicYearId)
        {
            global $Lang, $plugin, $objPrinting, $cfg_msschPrint;

            $ret = array();
            if(!$AcademicYearId) {
                $AcademicYearId = Get_Current_Academic_Year_ID();
            }
            $allMfpCode = $objPrinting->getAllGroupMfpCode($AcademicYearId);
            $allGroupUserCount = $objPrinting->getAllGroupUserCountArr($AcademicYearId);

            foreach ($CsvData as $i => $data) {
                $date = $_POST['selectMonth'];
                $data[0] = str_replace('/','-',$data[0]);
                $mfpCode = ($data[4]) ? $data[4] : $data[3];
                $colorMode = $data[8];
                $pages = $data[9];
                $sheet = $data[10];
                $paperSize = $data[11];

                if (!$mfpCode) {
                    continue;
                }

                $error = array(); // For holding error message in each row

                //// Change date to two digit START ////
                preg_match('#(\d{4})([-/])(\d{1,2})([-/])(\d{1,2}) (\d{1,2}):(\d{1,2})#', $data[0], $match);

                $match[1] = (strlen($match[1]) == 1) ? "0{$match[1]}" : $match[1];
                $match[3] = (strlen($match[3]) == 1) ? "0{$match[3]}" : $match[3];
                $match[5] = (strlen($match[5]) == 1) ? "0{$match[5]}" : $match[5];
                $match[6] = (strlen($match[6]) == 1) ? "0{$match[6]}" : $match[6];
                $match[7] = (strlen($match[7]) == 1) ? "0{$match[7]}" : $match[7];
                $data[0] = "{$match[1]}{$match[2]}{$match[3]}{$match[4]}{$match[5]} {$match[6]}:{$match[7]}";
                $recordDateArr = explode(' ', $data[0]);
                //// Change date to two digit END ////

                //////////////////
                //Check date and change date to 'YYYY-MM-01'
                //////////////////
                if (!preg_match('/^(\d{4})-(0[1-9]|1[0-2])$/', $date, $_dateArr)) {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['dateFormatError'];
                }
                if ($_dateArr[2] == '00') {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['dateFormatError'];
                }
                if (strcmp("{$_dateArr[1]}-{$_dateArr[2]}", date('Y-m')) > 0) {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['dateFuture'];
                }

                //////////////////
                //Check lastSyncTime
                //////////////////
                if (!checkDateIsValidFor2Format($recordDateArr[0]) || !checkTimeIsValid("{$recordDateArr[1]}:00")) {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['dateFormatError'];
                }elseif(strpos($recordDateArr[0], $date) !== 0){
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['dateFormatError'];
                }

                //////////////////
                //Check mfp
                //////////////////
                $isMfpCorrect = true;
                if (!in_array($mfpCode, $allMfpCode)) {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['mfpNotFound'];
                    $isMfpCorrect = false;
                }

                //////////////////
                //Check group member count
                //////////////////
                if ($isMfpCorrect && $allGroupUserCount[$mfpCode] == 0) {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['groupNoMember'];
                }

                //////////////////
                //Check color mode
                //////////////////
                if ($colorMode != 'Full Color' && $colorMode != 'B/W') {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['colorModeWrong'];
                }

                //////////////////
                //Check pages
                //////////////////
                if (((int)$pages) != $pages || $pages <= 0) {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['pagesWrong'];
                }

                //////////////////
                //Check sheet
                //////////////////
                if (((int)$sheet) != $sheet || $sheet <= 0) {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['sheetWrong'];
                }

                //////////////////
                //Check paperSize
                //////////////////
                if ($paperSize != 'A3' && $paperSize != 'A4' && $paperSize != 'OTHER') {
                    $error[] = $Lang['MsschPrint']['management']['print']['importError']['paperSizeWrong'];
                }

                $pass = !count($error);
                $ret[$i]["pass"] = $pass;
                $ret[$i]["reason"] = $error;
                $ret[$i]["rawData"]["Error"] = $this->errorToRawHTML($error);


                $ret[$i]["rawData"]["Date"] = $data[0];
                $ret[$i]["rawData"]["MfpCode"] = $mfpCode;
                $ret[$i]["rawData"]["ColorMode"] = $colorMode;
                $ret[$i]["rawData"]["Pages"] = $pages;
                $ret[$i]["rawData"]["Sheet"] = $sheet;
                $ret[$i]["rawData"]["PaperSize"] = $paperSize;
            }
            return $ret;
        }

        function errorToRawHTML($errorArray)
        {
            if (empty($errorArray)) {
                return '';
            }

            $raw = '*)' . $errorArray[0];
            for ($i = 1, $iMax = count($errorArray); $i < $iMax; $i++) {
                $raw .= '<br />*)' . $errorArray[$i];
            }
            return $raw;
        }


    }//close class
}
