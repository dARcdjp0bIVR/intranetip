<?php

$cfg_msschPrint['path'] = "home/eAdmin/GeneralMgmt/MsschPrinting/";
$cfg_msschPrint['importTempFolder'] = "MsschPrinting/print/";

$cfg_msschPrint['group']['memberType']['PIC'] = '1';
$cfg_msschPrint['group']['memberType']['Member'] = '2';

$cfg_msschPrint['moneyFormat']['decimals'] = 3;
$cfg_msschPrint['moneyFormat']['dec_point'] = '.';
$cfg_msschPrint['moneyFormat']['thousands_sep'] = ',';
?>