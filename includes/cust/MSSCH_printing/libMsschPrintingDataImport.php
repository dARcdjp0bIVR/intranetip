<?php
// Using: 
/**
 * Change Log:
 * 2016-01-13 Pun [91242]
 * 	- Modified processImportData(), fixed floting point subtract error
 */

include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");


///////////////////////////////
//////////////new file here
///////////////////////////////

if (!defined("LIB_MSSCH_PRINTING_DATA_IMPORT_DEFINED"))                     // Preprocessor directive
{
	define("LIB_MSSCH_PRINTING_DATA_IMPORT_DEFINED", true);
	class libMsschPrintingDataImport
	{

		function ReadImportData($TargetFilePath)
		{
			if (file_exists($TargetFilePath))
			{
				$libimport = new libimporttext();

				$DefaultCsvHeaderArr= array('MFP Code','Amount');
				$ColumnPropertyArr = array(1,1);	// 0 - Ref Field, not for actual import

				$CsvData = $libimport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
				$CsvHeaderArr = array_shift($CsvData);
				$numOfCsvData = count($CsvData);
				$CsvHeaderWrong = false;
				for($i=0; $i<count($DefaultCsvHeaderArr); $i++) {
					if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i]) {
						$CsvHeaderWrong = true;
						break;
					}
				}

				$CsvData = $this->processImportData($CsvData); // Subtract the old value of the amount

				$ret = array();
				$ret["HeaderError"] = $CsvHeaderWrong;
				$ret["NumOfData"] 	= $numOfCsvData;

				$ret["Data"] = $this->ValidateImportData($CsvData);
				return $ret;
			}
			else
			{
				return false;
			}
		}

		function processImportData($data){
			global $objPrinting;
			$currentAcademicYearID = Get_Current_Academic_Year_ID();
			$groupUsage = $objPrinting->getAllGroupUsage($currentAcademicYearID, $_POST['selectMonth']);
			foreach($data as $index=>$d){
				$_mfp = $d[0];
				$_amount = $d[1];
				if($groupUsage[$_mfp]){
					$data[$index][1] = round($_amount - $groupUsage[$_mfp], 2);
				}
			}
			return $data;
		}

		function ValidateImportData($CsvData)
		{
			global $Lang, $plugin, $objPrinting;

			$ret = array();
			$currentAcademicYearID = Get_Current_Academic_Year_ID();
			$allMfpCode = $objPrinting->getAllGroupMfpCode($currentAcademicYearID);
			$allGroupUserCount = $objPrinting->getAllGroupUserCountArr($currentAcademicYearID);

			for($i = 0,$iMax = count($CsvData);$i < $iMax; $i++)
			{
				$mfpCode	= trim($CsvData[$i][0]); // column 0=> MFP Code
				$amount		= trim($CsvData[$i][1]); // column 1=> Amount
				$date 		= $_POST['selectMonth'];

				$error = array(); // For holding error message in each row

				$fullDate = '';

				//////////////////
				//Check date and change date to 'YYYY-MM-01'
				//////////////////
				if(!preg_match('/^(\d{4})-(0[1-9]|1[0-2])$/', $date, $_dateArr)){
					$error[] = $Lang['MsschPrint']['management']['print']['importError']['dateFormatError'];
				}
				if($_dateArr[2] == '00'){
					$error[] = $Lang['MsschPrint']['management']['print']['importError']['dateFormatError'];
				}
				if( strcmp("{$_dateArr[1]}-{$_dateArr[2]}", date('Y-m')) > 0 ){
					$error[] = $Lang['MsschPrint']['management']['print']['importError']['dateFuture'];
				}
				$fullDate = "{$_dateArr[1]}-{$_dateArr[2]}-01";

				//////////////////
				//Check mfp
				//////////////////
				$isMfpCorrect = true;
				if(!in_array($mfpCode, $allMfpCode)){
					$error[] = $Lang['MsschPrint']['management']['print']['importError']['mfpNotFound'];
					$isMfpCorrect = false;
				}

				//////////////////
				//Check group member count
				//////////////////
				if($isMfpCorrect && $allGroupUserCount[$mfpCode] == 0){
					$error[] = $Lang['MsschPrint']['management']['print']['importError']['groupNoMember'];
				}

				//////////////////
				//Check amount
				//////////////////
				if(!preg_match('/^\d*(\.\d{0,2})?$/', $amount)){
					$error[] = $Lang['MsschPrint']['management']['print']['importError']['amountFormatError'];
				}

				$pass = !count($error);
				$ret[$i]["pass"] 					= $pass;
				$ret[$i]["reason"] 					= $error;
				$ret[$i]["rawData"]["Error"] 		= $this->errorToRawHTML($error);


				$ret[$i]["rawData"]["Date"] 		= $fullDate;
				$ret[$i]["rawData"]["MfpCode"] 		= $mfpCode;
				$ret[$i]["rawData"]["Amount"] 		= $amount;
			}
			return $ret;
		}

		function errorToRawHTML($errorArray)
		{
			if(empty($errorArray))
			{
				return '';
			}

			$raw = '*)' . $errorArray[0];
			for($i = 1,$iMax = count($errorArray); $i < $iMax; $i++)
			{
				$raw .= '<br />*)' . $errorArray[$i];
			}
			return $raw;
		}



	}//close class
}



?>