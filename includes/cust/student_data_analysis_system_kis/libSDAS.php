<?php
// Using : 
// ################ Change Log [Start] #####
//  Date:2020-04-16 [Philips]
//  Create this file
//
// ################# Change Log [End] ######
if (! defined("LIBSDAS_DEFINED")) // Preprocessor directives
{
    define("LIBSDAS_DEFINED", true);

    class libSDAS extends libdb
    {
        /**
         * Get the Left Sidebar, please set this variable before calling this.
         * $PATH_WRT_ROOT: The relative path to base
         * $CurrentPage: The current page (For highlighting)
         */
        function GET_MODULE_OBJ_ARR()
        {
            global $PATH_WRT_ROOT, $Lang, $CurrentPage, $LAYOUT_SKIN, $plugin, $sys_custom, $intranet_root;
            global $ec_iPortfolio;
            global $_SESSION;
            
            // ### Init START ####
            $ModulePath = $PATH_WRT_ROOT . "home/student_data_analysis_system_kis/";
            $user = new libuser($_SESSION['UserID']);
            // ### Init END ####
        
            // ### Define Page START ####
            // management
            $pagesArr['management'][] = array(
            		'id' => 'management.CEES',
            		'lang' => $Lang['SDAS']['menu']['CEES'],
            		'path' => $ModulePath . '?t=management.to_cees.index'
            );
            $pagesArr['management'][] = array(
                'id' => 'management.MonthlyReport',
                'lang' => $Lang['SDAS']['CEES']['MonthlyReport'],
                'path' => $PATH_WRT_ROOT . 'home/cees_kis/monthlyreport/',
                'extraAttr' => 'target="_blank"'
            );
            
            $pagesArr['management'][] = array(
            		'id' => 'management.inter_school_activity_report',
            		'lang' => $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool'] . ')',
            		'path' => $ModulePath . '?t=management.inter_school_activity_report.index'
            );
            
            $pagesArr['management'][] = array(
            		'id' => 'management.intra_school_activity_report',
            		'lang' => $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'] . ')',
            		'path' => $ModulePath . '?t=management.intra_school_activity_report.index'
            );
            $pagesArr['management'][] = array(
            		'id' => 'management.monthly_school_activity_report',
            		'lang' => $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Title'],
            		'path' => $ModulePath . '?t=management.monthly_school_activity_report.index'
            );

            // settings
            $pagesArr['settings'][] = array(
                'id' => 'settings.assessmentStatReport',
                'lang' => $Lang['SDAS']['menu']['accessRight'],
                'path' => $ModulePath . '?t=settings.assessmentStatReport.accessRightConfigMonthlyReport'
            );
            // ### Define Page END ####
            
            // ### Initialize START ####
            // Set variable to zero, just for safe.
            foreach ($pagesArr as $pageGroup => $pages) {
                $$pageGroup = 0;
                foreach ($pages as $page) {
                    ${$page['id']} = 0;
                }
            }
            // ### Initialize END ####
            
            // ### Highlight the Current Page START ####
            // Set the releative group and page to 1 (It will Highlight the hyperlink)
            foreach ($pagesArr as $pageGroup => $pages) {
                foreach ($pages as $page) {
                    if ($page['id'] == $CurrentPage) {
                        $$pageGroup = 1;
                        ${$page['id']} = 1;
                        break 2;
                    }
                }
            }
            // ### Highlight the Current Page END ####
        
            // ### Menu Display START ####
            $MenuArr = array();
            foreach ($pagesArr as $pageGroup => $pages) {
                if ($this->checkAccessRight($pageGroup)) {
                    // # Check The group has sub-item or not START ##
                    $hasSubItem = false;
                    foreach ($pages as $p) {
                        if ($this->checkAccessRight($pageGroup, $p['id'])) {
                            $hasSubItem = true;
                            break;
                        }
                    }
                    // # Check The group has sub-item or not END ##
                    
                    // # Show The group START ##
                    if ($hasSubItem) {
                        $MenuArr[$pageGroup] = array(
                            $Lang['SDAS']['menu'][$pageGroup],
                            "",
                            $$pageGroup
                        );
                    }
                    // # Show The group END ##
                    
                    // # Show The sub-item START ##
                    if ($hasSubItem) {
                        foreach ($pages as $p) {
                            if ($this->checkAccessRight($pageGroup, $p['id'])) {
                                // 3 is for customize icon on menu
                                $MenuArr[$pageGroup]["Child"][$p['id']] = array(
                                    $p['lang'],
                                    $p['path'],
                                    ${$p['id']},
                                    "",
                                    $p['extraAttr']
                                );
                            }
                        }
                    }
                    // # Show The sub-item END ##
                }
            }
            
            // ### Menu Display END ####
            
            // ### Module Information START ####
            $sdasSkin = ($plugin['StudentDataAnalysisSystem_Style']) ? $plugin['StudentDataAnalysisSystem_Style'] : 'general';
            $MODULE_OBJ['title'] = $Lang['Header']['Menu']['StudentDataAnalysisSystem'][$sdasSkin];
            $MODULE_OBJ['title_css'] = "menu_opened";
            if ($plugin['StudentDataAnalysisSystem_Style_User'][$user->UserLogin]) {
                $iconFolder = $plugin['StudentDataAnalysisSystem_Style_User'][$user->UserLogin];
                $iconPath = "{$PATH_WRT_ROOT}/images/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$iconFolder}/";
            } else 
                if ($sdasSkin) {
                    $iconPath = "{$PATH_WRT_ROOT}/images/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/{$sdasSkin}/";
                } else {
                    $iconPath = "{$PATH_WRT_ROOT}/images/{$LAYOUT_SKIN}/StudentAnalysisDataSystem/general/";
                }
            $MODULE_OBJ['logo'] = "{$iconPath}/icon_student.png";
            $MODULE_OBJ['selectedMenuIcon'] = "{$iconPath}/icon_sub_on.gif";
            $MODULE_OBJ['menu'] = $MenuArr;
            // ### Module Information END ####
            return $MODULE_OBJ;
        }
        // End GET_MODULE_OBJ_ARR()

        function isSuperAdmin()
        {
            return $_SESSION["SSV_USER_ACCESS"]["other-SDAS"];
        }
        // End isSuperAdmin()
        
        /**
         * This function is for checking the access right.
         */
        function checkAccessRight($module = '', $page = '', $subpage = '')
        {
            global $plugin, $sys_custom, $intranet_db;
            if (! $plugin['StudentDataAnalysisSystem']) {
                return false;
            }
            
            if ($module == 'ajax') {
                return true;
            }

            // ### Cust Module END ####

            // ### Admin AccessRight START ####
            if ($this->isSuperAdmin()) {
                return true;
            }
            $accessRight = $this->getAssessmentStatReportAccessRight();
            if ($accessRight['admin']) {
                return true;
            }
           
            return false;
        }
        // End checkAccessRight()
        
        private $accessRightUserArr;

        function getAssessmentStatReportAccessRight($userID = '')
        {
            if (isset($this->accessRightUserArr[$userID])) {
                $accessRight = $this->accessRightUserArr[$userID];
            } else {
                global $PATH_WRT_ROOT;
                include_once ($PATH_WRT_ROOT . "includes/libportfolio.php");
                
                $lpf = new libportfolio();
                $accessRight = $lpf->getAssessmentStatReportAccessRight($userID);
                
                $this->accessRightUserArr[$userID] = $accessRight;
                // Monitoring Group
                $rs = $this->getMonitoringGroupMember('1', '');
                foreach ((array) $rs as $_rs) {
                    $MonitorArr[$_rs['UserID']][$_rs['GroupID']] = $_rs['GroupID'];
                    $MonitorArr['Admin'][$_rs['GroupID']] = $_rs['GroupID'];
                }
                if ($this->accessRightUserArr[$userID]['admin']) {
                    $this->accessRightUserArr[$userID]['MonitoringGroupPIC'] = $MonitorArr['Admin'];
                } else {
                    $this->accessRightUserArr[$userID]['MonitoringGroupPIC'] = $MonitorArr[$_SESSION['UserID']];
                }
                $accessRight = $this->accessRightUserArr[$userID];
            }
            
            return $accessRight;
        }
 // End getAssessmentStatReportAccessRight()

        function checkZeroDot($value)
        {
            $x = explode('.', $value);
            if (count($x > 0)) {
                if ($x[1] > 0) {
                    return $value; // 100.1
                } else {
                    return $x[0]; // 100.0
                }
            } else {
                return $value;
            }
        }

        function getMonitoringGroup($GroupID, $AcademicYearID)
        {
            global $intranet_db, $eclass_db, $intranet_root;
            if ($GroupID) {
                $GroupIDCond = ' AND mg.GroupID = "' . $GroupID . '"';
            }
            if ($AcademicYearID) {
                $AcademicYearIDCond = ' AND mg.AcademicYearID = "' . $AcademicYearID . '" ';
            }
            $sql = 'Select
						mg.GroupID,
						mg.AcademicYearID,
						mg.GroupNameEN,
						mg.GroupNameCH,
						mg.DateModified,
						ay.YearNameEN,
						ay.YearNameB5
					FROM
						MONITORING_GROUP mg
					INNER JOIN 
						ACADEMIC_YEAR ay 
							ON ay.AcademicYearID = mg.AcademicYearID
					WHERE
						mg.Isdeleted = "0"
						' . $GroupIDCond . $AcademicYearIDCond . '
					Order by 
						ay.Sequence
					';
            $rs = $this->returnResultSet($sql);
            return $rs;
        }

        function getMonitoringGroupMember($RecordType, $GroupID)
        {
            global $intranet_db, $eclass_db, $intranet_root;
            
            if ($RecordType) {
                $RecordTypeCond = ' AND mgm.RecordType = "' . $RecordType . '" ';
            }
            if ($GroupID) {
                $GroupIDCond = ' AND mgm.GroupID = "' . $GroupID . '" ';
            }
            
            $sql = 'SELECT
				Distinct
				iu.UserID, iu.EnglishName, iu.ChineseName, mgm.GroupID, mgm.RecordType, mgm.UserID, mgm.DateModified, ycycu.ClassTitleEN, ycycu.ClassTitleB5, ycycu.ClassNumber, ycycu.YearID
			FROM
				MONITORING_GROUP_MEMBER mgm
			INNER JOIN
				MONITORING_GROUP mg
					ON mgm.GroupID = mg.GroupID
			INNER JOIN
				INTRANET_USER iu
					ON	iu.UserID = mgm.UserID
			LEFT JOIN
				(SELECT
					ycu.UserID, yc.ClassTitleEN, yc.ClassTitleB5, ycu.ClassNumber, yc.AcademicYearID, yc.YearID
				FROM
					YEAR_CLASS_USER ycu
					LEFT JOIN
						YEAR_CLASS yc
							ON yc.YearClassID = ycu.YearClassID) ycycu
				ON ycycu.UserID = mgm.UserID  and ycycu.AcademicYearID = mg.AcademicYearID
					
			Where
				mgm.IsDeleted = "0"
				' . $RecordTypeCond . '
				' . $GroupIDCond . '
			ORDER BY
				ycycu.ClassTitleEN desc, ycycu.ClassNumber asc
			';
            $rs = $this->returnResultSet($sql);
            
            return $rs;
        }
        
        function getMonthlyReportPIC(){
            $sql = "select SettingValue from GENERAL_SETTING where SettingName = 'MonthlyReportPIC'";
            $TeacherList =  $this->returnVector($sql);
            if($TeacherList[0] == '' ){
                $TeacherCond = '0';
            } else {
                $TeacherCond = str_replace(",", "','", $TeacherList[0]);
            }
            
            $sql = "SELECT UserID, ChineseName, EnglishName
                    FROM INTRANET_USER
                    WHERE 1
                    AND UserID IN ('$TeacherCond')
                    ";
            $result = $this->returnArray($sql);
            $resultArr = array();
            foreach($result as $rs){
                $resultArr[$rs['UserID']] = $rs;
            }
            return $resultArr;
        }
        
        function isMonthlyReportPIC($id){
            $PICArr = $this->getMonthlyReportPIC();
            $SectionArr = $this->getMonthlyReportSectionRight($id);
            return !(empty($PICArr[$id])) || !(empty($SectionArr));
//             return true;    // for testing
        }
        
        function getMonthlyReportSectionPIC($section=''){
//             $namefield = Get_Lang_Selection('iu.ChineseName', 'iu.EnglishName');
            $namefield= "  If (iu.ChineseName IS NULL Or iu.ChineseName = '', iu.EnglishName, ".Get_Lang_Selection('iu.ChineseName','iu.EnglishName').") ";
                        
            $table = "CEES_SCHOOL_MONTHLY_REPORT_USER_ACCESS_RIGHT cees";
            $join1 = "INTRANET_USER iu
                        ON iu.UserID = cees.UserID";
            $cols = "cees.RecordID, cees.SectionName, cees.UserID, $namefield AS Name";
            if($section!=''){
                $conds = "WHERE cees.SectionName = '$section'";
            }
            
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                        INNER JOIN $join1
                    $conds";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function getMonthlyReportSectionRight($userID){
            $table = "CEES_SCHOOL_MONTHLY_REPORT_USER_ACCESS_RIGHT";
            $cols = "SectionName";
            $conds = "UserID = '$userID'";
            
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                    WHERE
                        $conds
                    ";
            $result = $this->returnVector($sql);
            return $result;
        }
        
        // Start of Access Group
        
        function getAccessGroup($groupid){
            $cols = "GroupID, Name_ch, Name_en, Description";
            $table = "ASSESSMENT_ACCESS_GROUP";
            $cond = "isDeleted = '0'";
            $cond .= " AND GroupID = '$groupid'";
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                    WHERE
                        $cond
                    ";
//             debug_pr($sql);die();
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function getAccessGroupRights($groupid){
            $cols = "aggr.AcademicStatisticName, aggr.RightType";
            $table = "ASSESSMENT_ACCESS_GROUP AS aag";
            $joinTable1 = "ASSESSMENT_ACCESS_GROUP_RIGHT AS aggr";
            $joinCond1 = "aag.GroupID = aggr.GroupID";
            $cond = "aag.isDeleted = '0'";
            $cond .= " AND aag.GroupID = '$groupid'";
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                    INNER JOIN
                        $joinTable1
                        ON $joinCond1
                    WHERE
                        $cond
                    ";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function getAccessGroupMemberRights($uid = ''){
            if($uid == ''){
                global $_SESSION;
                $uid = $_SESSION['UserID'];
            }
            $cols = "aagr.AcademicStatisticName, aagr.RightType";
            $table = "ASSESSMENT_ACCESS_GROUP_MEMBER AS aagm";
            $joinTable1 = "ASSESSMENT_ACCESS_GROUP_RIGHT AS aagr";
            $joinCond1 = "aagm.GroupID = aagr.GroupID";
            $joinTable2 = "ASSESSMENT_ACCESS_GROUP AS aag";
            $joinCond2 = "aagm.GroupID = aag.GroupID";
            $cond = "aag.isDeleted = '0'";
            $cond .= " AND aagm.UserID = '$uid'";
            $cond .= " AND aagr.RightType = '1'";
            $sql = "SELECT
                        $cols
                    FROM
                        $table
                        INNER JOIN $joinTable1
                        ON $joinCond1
                        INNER JOIN $joinTable2
                        ON $joinCond2
                    WHERE
                        $cond
                    ";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function isAccessGroupMember($uid = ''){
            if($uid == ''){
                global $_SESSION;
                $uid = $_SESSION['UserID'];
            }
            $rights = $this->getAccessGroupMemberRights($uid);
            return sizeof($rights) > 0;
        }
        
        function getAccessGroupMember($groupid){
            $namefield = 'iu.' . Get_Lang_Selection('ChineseName', 'EnglishName');
            $backupfield = 'iu.' . Get_Lang_Selection('EnglishName', 'ChineseName');
            $cols = "aag.GroupID, IF($namefield = '', $backupfield, $namefield) as Name, iu.UserID";
            $table = "ASSESSMENT_ACCESS_GROUP AS aag";
            $joinTable1 = "ASSESSMENT_ACCESS_GROUP_MEMBER AS aggm";
            $joinCond1 = "aag.GroupID = aggm.GroupID";
            $joinTable2 = "INTRANET_USER iu";
            $joinCond2 = "aggm.userID = iu.userID";
            $cond = "aag.isDeleted = '0'";
            $cond .= " AND aag.GroupID = '$groupid'";
            $sql = "SELECT 
                        $cols
                    FROM
                        $table
                    INNER JOIN
                        $joinTable1
                        ON $joinCond1
                    INNER JOIN
                        $joinTable2
                        ON $joinCond2
                    WHERE
                        $cond";
            $result = $this->returnArray($sql);
            return $result;
        }
        
        function insertAccessGroup($arr){
            global $_SESSION;
            $table = "ASSESSMENT_ACCESS_GROUP";
            $cols = array('Name_ch', 'Name_en', 'Description', 'InputDate', 'InputBy');
            $values = array($arr['Name_ch'], $arr['Name_en'], $arr['Description'], 'NOW()', $_SESSION['UserID']);
            
            $sql = "INSERT INTO $table(" . implode(",", $cols).") VALUES ('" . implode("','", $values)."')";
            $sql = str_replace("'NOW()'", "NOW()", $sql);
            $this->db_db_query($sql);
            return $this->db_insert_id();
        }
        
        function insertAccessGroupRight($arr, $groupID){
            $table = "ASSESSMENT_ACCESS_GROUP_RIGHT";
            $cols = "GroupID, AcademicStatisticName, RightType";
            $values = array();
            foreach($arr as $name => $right){
                $values[] = "('$groupID', '$name', '$right')";
            }
            $sql = "INSERT INTO $table($cols) VALUES " . implode(',', $values);
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function updateAccessGroup($arr){
            global $_SESSION;
            $table = "ASSESSMENT_ACCESS_GROUP";
            $cols = array('Name_ch', 'Name_en', 'Description', 'ModifiedDate', 'ModifiedBy');
            $values = array($arr['Name_ch'], $arr['Name_en'], $arr['Description'], 'NOW()', $_SESSION['UserID']);
            $update = "";
            for($i=0;$i<sizeof($cols);$i++){
                $update .= "$cols[$i] = '$values[$i]',";
            }
            $update = substr($update, 0, -1);
            $sql = "UPDATE $table
                    SET
                        $update
                    WHERE
                        GroupID = '$arr[GroupID]'";
            $sql = str_replace("'NOW()'", "NOW()", $sql);
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function updateAccessGroupRight($arr, $groupID){
            $this->deleteAccessGroupRight($groupID);
            $result = $this->insertAccessGroupRight($arr, $groupID);
            return $result;
        }
        
        function updateAccessGroupMember($arr, $groupID){
            $curMemberArr = $this->getAccessGroupMember($groupID);
            $curMemberArr = Get_Array_By_Key($curMemberArr, 'UserID');
            
            $insertArr = array_diff($arr, $curMemberArr);
            $deleteArr = array_diff($curMemberArr, $arr);
            
            $result = array(
                'insert' => $this->insertAccessGroupMember($insertArr, $groupID),
                'delete' => $this->deleteAccessGroupMember($deleteArr, $groupID)
            );
            return $result;
        }
        
        
        function insertAccessGroupMember($arr, $groupID){
            if(sizeof($arr) == 0)
                return true;
            $table = "ASSESSMENT_ACCESS_GROUP_MEMBER";
            $cols = "GroupID, UserID";
            $values = "";
            foreach($arr as $member){
                $values .= "('$groupID', '$member'),";
            }
            $values = substr($values, 0, -1);
            $sql = "INSERT INTO $table($cols) VALUES$values";
            $result = $this->db_db_query($sql);
//             debug_pr($sql);die();
            return $result;
        }
        
        function deleteAccessGroupMember($arr, $groupID){
            if(sizeof($arr) == 0)
                return true;
            $table = "ASSESSMENT_ACCESS_GROUP_MEMBER";
            $conds = "GroupID = '$groupID' ";
            $conds .= "AND UserID IN ('" . implode("','", $arr) . "')";
            $sql = "DELETE FROM $table WHERE $conds";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function deleteAccessGroup($groupIDArr){
            $table = "ASSESSMENT_ACCESS_GROUP";
            $sql = "UPDATE $table SET isDeleted = '1' Where GroupID IN ('".implode("','", $groupIDArr)."')";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        function deleteAccessGroupRight($groupID){
            $table = "ASSESSMENT_ACCESS_GROUP_RIGHT";
            $sql = "DELETE FROM $table WHERE GroupID = '$groupID'";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        // End of Access Group
        // Start of ECA
        function getECACategorySelect($attribute='',$selectedValue='', $nature=1){
        	global $Lang;
        	// nature: 1=>intra, 0=>inter, 2=> other
        	if($nature==1){
        		$optionArr = array(
        			'O1' => array('ManagementAndOrganization', array('ProfessionalDevelopmentAndTrainning')),
        			'O2' => 'LearnAndTeach',
        			'O3' => 'StudentSupport'
        		);
        	} else {
        		$optionArr = array(
        			'I1' => array('ManagementAndOrganization', 
        					array('SchoolAdministration','ProfessionalDevelopmentAndTrainning','InterflowAndSharing')
        			),
        			'I2' => array('LearnAndTeach',
        					array('InsideSchoolActivity', 'OutsideSchoolActivity')
        			),
        			'I3' => array('StudentSupport', 
        					array('ParentSchoolActivity','ParentEducation','OtherSupportActivity','Others')
        			),
        		);
        	}
        	foreach($optionArr as $catKey => $catArr){
        		if(!is_array($catArr)){
        			$selectedStr = ($selectedValue == $catKey.'_'.$catArr) ? 'selected' : '';
        			$options .= '<option value="' . $catKey . '_' .$catArr .'" ' . $selectedStr .'>' . $Lang['CEES']['Management']['SchoolActivityReport']['Category'][$catArr] . '</option>';
        		} else {
        			$options .= '<optgroup label="'.$Lang['CEES']['Management']['SchoolActivityReport']['Category'][$catArr[0]].'">';
        			foreach($catArr[1] as $cat){
        				$selectedStr = ($selectedValue == $catKey.'_'.$cat) ? 'selected' : '';
        				$options .= '<option value="' . $catKey . '_' .$cat .'" ' . $selectedStr .'>' . $Lang['CEES']['Management']['SchoolActivityReport']['Category'][$cat] . '</option>';
        			}
        			$options .= '</optgroup>';
        		}
        	}
        	$select = '<select ' . $attribute .'>' . $options . '</select>';
        	return $select;
        }
        // Start of InterSchool ECA
        function insertInterSchoolECA($AcademicYearID, $InfoData, $RecordAry, $ReportID = '', $isNew = false){
        	$existReport = $this->getInterSchoolECA($AcademicYearID, $ReportID);
        	if(!empty($existReport) && !$isNew){
        		// Update Report
        		$reportID = $existReport['ReportID'];
        		$this->updateInterSchoolECA($reportID, $InfoData);
        	} else {
        		// Init Report
        		$reportID = $this->initInterSchoolECA($AcademicYearID, $InfoData['ReportStartDate'], $InfoData['ReportEndDate']);
        	}
        	$this->clearInterSchoolECA($reportID);
        	// Remove Existing Record
        	$result = $this->insertInterSchoolECARecord($reportID, $RecordAry);
        	return $reportID;
        }
        function insertInterSchoolECARecord($ReportID, $RecordAry){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTERSCHOOL_RECORD";
        	$fieldArray = array('ReportID', 'Section', 'RecordDate', 'Event', 'Venue', 'Guest', 'ParticipatedSchool', 'Target', 'TotalParticipant');
        	$cols = implode(",", $fieldArray);
//         	$cols = "ReportID, Section, RecordDate, Event, Venue, Guest, ParticipatedSchool, Target, TotalParticipant";
        	$vals = "";
        	foreach($RecordAry as $record){
        		$vals .= "(";
        		foreach($fieldArray as $field){
        			if($field == 'ReportID'){
        				$vals .= "'$ReportID',";
        			} else {
        				$vals .= "'".( isset($record[$field]) ? $record[$field] : '')."',";
        			}
        		}
        		if(strlen($vals) != 0) $vals = substr($vals, 0, -1);
        		$vals .= "),";
//         		$vals .= "('$ReportID', '" . implode("','", $record) ."'),";
        	}
        	if(strlen($vals) != 0) $vals = substr($vals, 0, -1);
        	$sql = "INSERT INTO $table($cols) VALUES $vals";
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function getInterSchoolECA($AcademicYearID, $ReportID = ''){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTERSCHOOL";
        	$col = "ReportID, Year, StartDate, EndDate, AcademicYearID, SubmitStatus, DraftStatus, SubmittedToCeesDate,ReportKey, ModifiedDate";
        	$conds = "AcademicYearID = '$AcademicYearID'";
        	$order = "StartDate ASC";
        	if($ReportID != ''){
        		$conds .= "AND ReportID = '$ReportID'";
        	}
        	$sql = "SELECT $col FROM $table WHERE $conds ORDER BY $order";
        	$result = $this->returnArray($sql);
        	if($ReportID != ''){
        		return $result[0];
        	} else {
        		return $result;
        	}
        }
        function getInterSchoolECARecord($ReportID){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTERSCHOOL_RECORD";
        	$col = "ReportID, Section, RecordDate, Event, Venue, Guest, ParticipatedSchool, Target, TotalParticipant";
        	$conds = "ReportID = '$ReportID'";
        	$sql = "SELECT $col FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result;
        }
        function initInterSchoolECA($AcademicYearID, $StartDate, $EndDate){
        	$yearName = getAcademicYearByAcademicYearID($AcademicYearID);
        	$yearName = substr($yearName, 0, 4);
        	$randomMD5 = md5(time());
        	
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTERSCHOOL";
        	$colVal = array(
        			'ReportKey' => $randomMD5,
        			'Year' => $yearName, 
        			'StartDate' => $StartDate, 
        			'EndDate' => $EndDate, 
        			'AcademicYearID' => $AcademicYearID, 
        			'SubmitStatus' => '0', 
        			'DraftStatus' => '0',
        			'InputBy' => $_SESSION['UserID'],
        			'InputDate' => 'NOW()',
        			'ModifiedBy' => $_SESSION['UserID'],
        			'ModifiedDate' => 'NOW()'
        	);
        	$col = "";
        	$val = "";
        	foreach($colVal as $c => $v){
        		$col .= $c.",";
        		if($c != 'ModifiedDate' && $c != 'InputDate'){
        			$val .= "'$v',";
        		} else {
        			$val .= $v.',';
        		}
        	}
        	$col = substr($col,0,-1);
        	$val= substr($val,0,-1);
        	$sql = "INSERT INTO $table($col) VALUES($val)";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }
        function updateInterSchoolECA($ReportID, $InfoData){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTERSCHOOL";
        	$conds = "ReportID = '$ReportID'";
        	$sql = "UPDATE $table
        			SET ";
        	if($InfoData['ReportStartDate']){
        		$sql .= "StartDate = '".$InfoData['ReportStartDate']."', ";
        	}
        	if($InfoData['ReportStartDate']){
        		$sql .= "EndDate = '" . $InfoData['ReportEndDate']."', ";
        	}
        	if(isset($InfoData['DraftStatus'])){
        		$sql .= "DraftStatus = '" . $InfoData['DraftStatus']."', ";
        	}
        	$sql .= "ModifiedBy = '" . $_SESSION['UserID'] . "', ";
        	$sql .= "ModifiedDate = NOW() ";
        	$sql .= "WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function clearInterSchoolECA($ReportID){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTERSCHOOL_RECORD";
        	$sql = "DELETE FROM $table WHERE ReportID = '$ReportID'";
        	return $this->db_db_query($sql);
        }
        function removeInterSchoolECA($ReportID){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTERSCHOOL";
        	$sql = "DELETE FROM $table WHERE ReportID = '$ReportID'";
        	return $this->db_db_query($sql);
        }
        // End of InterSchool ECA
        // Start of IntraSchool ECA
        function insertIntraSchoolECA($AcademicYearID, $InfoData, $RecordAry, $ReportID = '', $isNew = false){
        	$existReport = $this->getIntraSchoolECA($AcademicYearID,$ReportID);
        	if(!empty($existReport) && !$isNew){
        		// Update Report
        		$reportID = $existReport['ReportID'];
        		$this->updateIntraSchoolECA($reportID, $InfoData);
        	} else {
        		// Init Report
        		$reportID = $this->initIntraSchoolECA($AcademicYearID, $InfoData['ReportStartDate'], $InfoData['ReportEndDate']);
        	}
        	$this->clearIntraSchoolECA($reportID);
        	// Remove Existing Record
        	$result = $this->insertIntraSchoolECARecord($reportID, $RecordAry);
        	return $reportID;
        }
        function getIntraSchoolECA($AcademicYearID,$ReportID = ''){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTRASCHOOL";
        	$col = "ReportID, Year, StartDate, EndDate, AcademicYearID, SubmitStatus, DraftStatus, SubmittedToCeesDate,ReportKey, ModifiedDate";
        	$conds = "AcademicYearID = '$AcademicYearID'";
        	$order = "StartDate ASC";
        	if($ReportID != ''){
        		$conds .= "AND ReportID = '$ReportID'";
        	}
        	$sql = "SELECT $col FROM $table WHERE $conds ORDER BY $order";
        	$result = $this->returnArray($sql);
        	if($ReportID != ''){
        		return $result[0];
        	} else {
        		return $result;
        	}
        }
        function getIntraSchoolECARecord($ReportID){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTRASCHOOL_RECORD";
        	$col = "ReportID,Section,RecordDate,Event,Venue,Guest,Target,ParticipatedSchool,Participant,Organization,ParticipatedGroup,Award,Name,Position,Class";
        	$conds = "ReportID = '$ReportID'";
        	$sql = "SELECT $col FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result;
        }
        function insertIntraSchoolECARecord($ReportID, $RecordAry){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTRASCHOOL_RECORD";
        	$fieldArray = array('ReportID','Section','RecordDate','Event','Venue','Guest','Target','ParticipatedSchool','Participant','Organization','ParticipatedGroup','Award','Name','Position','Class');
        	$cols = implode(",", $fieldArray);
        	//         	$cols = "ReportID, Section, RecordDate, Event, Venue, Guest, ParticipatedSchool, Target, TotalParticipant";
        	$vals = "";
        	foreach($RecordAry as $record){
        		$vals .= "(";
        		foreach($fieldArray as $field){
        			if($field == 'ReportID'){
        				$vals .= "'$ReportID',";
        			} else {
        				$vals .= "'".( isset($record[$field]) ? (string)$record[$field] : '')."',";
        			}
        		}
        		if(strlen($vals) != 0) $vals = substr($vals, 0, -1);
        		$vals .= "),";
        		//         		$vals .= "('$ReportID', '" . implode("','", $record) ."'),";
        	}
        	if(strlen($vals) != 0) $vals = substr($vals, 0, -1);
        	$sql = "INSERT INTO $table($cols) VALUES $vals";
        	$result = $this->db_db_query($sql);
//         	debug_pr($sql);
        	return $result;
        }
        function updateIntraSchoolECA($ReportID, $InfoData){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTRASCHOOL";
        	$conds = "ReportID = '$ReportID'";
        	$sql = "UPDATE $table
        	SET ";
        	if($InfoData['ReportStartDate']){
        		$sql .= "StartDate = '".$InfoData['ReportStartDate']."', ";
        	}
        	if($InfoData['ReportStartDate']){
        		$sql .= "EndDate = '" . $InfoData['ReportEndDate']."', ";
        	}
        	if(isset($InfoData['DraftStatus'])){
        		$sql .= "DraftStatus = '" . $InfoData['DraftStatus']."', ";
        	}
        	$sql .= "ModifiedBy = '" . $_SESSION['UserID'] . "', ";
        	$sql .= "ModifiedDate = NOW() ";
        	$sql .= "WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function initIntraSchoolECA($AcademicYearID, $StartDate, $EndDate){
        	$yearName = getAcademicYearByAcademicYearID($AcademicYearID);
        	$yearName = substr($yearName, 0, 4);
        	$randomMD5 = md5(time());
        	
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTRASCHOOL";
        	$colVal = array(
        			'ReportKey' => $randomMD5,
        			'Year' => $yearName,
        			'StartDate' => $StartDate,
        			'EndDate' => $EndDate,
        			'AcademicYearID' => $AcademicYearID,
        			'SubmitStatus' => '0',
        			'DraftStatus' => '0',
        			'InputBy' => $_SESSION['UserID'],
        			'InputDate' => 'NOW()',
        			'ModifiedBy' => $_SESSION['UserID'],
        			'ModifiedDate' => 'NOW()'
        	);
        	$col = "";
        	$val = "";
        	foreach($colVal as $c => $v){
        		$col .= $c.",";
        		if($c != 'ModifiedDate' && $c != 'InputDate'){
        			$val .= "'$v',";
        		} else {
        			$val .= $v.',';
        		}
        	}
        	$col = substr($col,0,-1);
        	$val= substr($val,0,-1);
        	$sql = "INSERT INTO $table($col) VALUES($val)";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }
        function clearIntraSchoolECA($ReportID,$exceptAward=false,$exceptActivity=false){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTRASCHOOL_RECORD";
        	if($exceptAward){
        		$extraCond = " AND Section NOT LIKE '4_%' ";
        	}
        	if($exceptActivity) {
        		$extraCond = " AND Section IN ('4_1','4_3') ";
        	}
        	$sql = "DELETE FROM $table WHERE ReportID = '$ReportID' $extraCond";
        	return $this->db_db_query($sql);
        }
        function removeIntraSchoolECA($ReportID){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_INTRASCHOOL";
        	$sql = "DELETE FROM $table WHERE ReportID = '$ReportID'";
        	return $this->db_db_query($sql);
        }
        // End of IntraSchool ECA
        // Start of Monthly ECA
        function insertMonthlyECA($AcademicYearID, $ReportMonth, $InfoData, $RecordAry){
        	$existReport = $this->getMonthlyECA($AcademicYearID, $ReportMonth);
        	if(!empty($existReport)){
        		// Update Report
        		$reportID = $existReport['ReportID'];
        		$this->updateMonthlyECA($reportID, $InfoData);
        	} else {
        		// Init Report
        		$reportID = $this->initMonthlyECA($AcademicYearID, $ReportMonth, $InfoData['ReportDate']);
        	}
        		$this->clearMonthlyECA($reportID);
        		// Remove Existing Record
        		$result = $this->insertMonthlyECARecord($reportID, $RecordAry);
        	return $reportID;
        }
        function getMonthlyECA($AcademicYearID, $ReportMonth){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_MONTHLY_REPORT";
        	$col = "ReportID, Year, Month, ReportDate, AcademicYearID, SubmitStatus, DraftStatus, SubmittedToCeesDate, ModifiedDate";
        	$conds = "AcademicYearID = '$AcademicYearID' AND Month = '$ReportMonth' ";
        	$sql = "SELECT $col FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result[0];
        }
        function getMonthlyECARecord($ReportID){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_MONTHLY_REPORT_RECORD";
        	$col = "ReportID,Section,RecordDate,Event,Venue,Guest,Target,ParticipatedSchool,Participant,Organization,ParticipatedGroup,Award,Name,Position,Class,Remark";
        	$conds = "ReportID = '$ReportID'";
        	$sql = "SELECT $col FROM $table WHERE $conds";
        	$result = $this->returnArray($sql);
        	return $result;
        }
        function insertMonthlyECARecord($ReportID, $RecordAry){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_MONTHLY_REPORT_RECORD";
        	$fieldArray = array('ReportID','Section','RecordDate','Event','Venue','Guest','Target','ParticipatedSchool','Participant','Organization','ParticipatedGroup','Award','Name','Position','Class','Remark');
        	$cols = implode(",", $fieldArray);
        	//         	$cols = "ReportID, Section, RecordDate, Event, Venue, Guest, ParticipatedSchool, Target, TotalParticipant";
        	$vals = "";
        	foreach($RecordAry as $record){
        		$vals .= "(";
        		foreach($fieldArray as $field){
        			if($field == 'ReportID'){
        				$vals .= "'$ReportID',";
        			} else {
        				$vals .= "'".( isset($record[$field]) ? (string)$record[$field] : '')."',";
        			}
        		}
        		if(strlen($vals) != 0) $vals = substr($vals, 0, -1);	
        		$vals .= "),";
        		//         		$vals .= "('$ReportID', '" . implode("','", $record) ."'),";
        	}
        	if(strlen($vals) != 0) $vals = substr($vals, 0, -1);
        	$sql = "INSERT INTO $table($cols) VALUES $vals";
        	$result = $this->db_db_query($sql);
//         	        	debug_pr($sql);
        	return $result;
        }
        function updateMonthlyECA($ReportID, $InfoData){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_MONTHLY_REPORT";
        	$conds = "ReportID = '$ReportID'";
        	$sql = "UPDATE $table
        	SET ";
        	if($InfoData['ReportDate']){
        		$sql .= "ReportDate = '" . $InfoData['ReportDate']."', ";
        	}
        	if(isset($InfoData['DraftStatus'])){
        		$sql .= "DraftStatus = '" . $InfoData['DraftStatus']."', ";
        	}
        	$sql .= "ModifiedBy = '" . $_SESSION['UserID'] . "', ";
        	$sql .= "ModifiedDate = NOW() ";
        	$sql .= "WHERE $conds";
        	$result = $this->db_db_query($sql);
        	return $result;
        }
        function initMonthlyECA($AcademicYearID, $ReportMonth, $ReportDate){
        	$yearName = getAcademicYearByAcademicYearID($AcademicYearID);
        	$yearName = substr($yearName, 0, 4);
        	$nextYr = strval($yearName) + 1;
        	if($ReportMonth < 9) $yearName = $nextYr;
        	
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_MONTHLY_REPORT";
        	$colVal = array(
        			'Year' => $yearName,
        			'Month' => $ReportMonth,
        			'ReportDate' => $ReportDate,
        			'AcademicYearID' => $AcademicYearID,
        			'SubmitStatus' => '0',
        			'DraftStatus' => '0',
        			'InputBy' => $_SESSION['UserID'],
        			'InputDate' => 'NOW()',
        			'ModifiedBy' => $_SESSION['UserID'],
        			'ModifiedDate' => 'NOW()'
        	);
        	$col = "";
        	$val = "";
        	foreach($colVal as $c => $v){
        		$col .= $c.",";
        		if($c != 'ModifiedDate' && $c != 'InputDate'){
        			$val .= "'$v',";
        		} else {
        			$val .= $v.',';
        		}
        	}
        	$col = substr($col,0,-1);
        	$val= substr($val,0,-1);
        	$sql = "INSERT INTO $table($col) VALUES($val)";
        	$result = $this->db_db_query($sql);
        	return $this->db_insert_id();
        }
        function clearMonthlyECA($ReportID, $exceptAward = false, $exceptActivity = false){
        	$table = "CEES_KIS_MONTHLY_REPORT_ECA_MONTHLY_REPORT_RECORD";
        	if($exceptAward){
        		$extraCond = " AND Section NOT LIKE '4_%' ";
        	}
        	if($exceptActivity) {
        		$extraCond = " AND Section IN ('4_1','4_3') ";
        	}
        	$sql = "DELETE FROM $table WHERE ReportID = '$ReportID' $extraCond";
        	return $this->db_db_query($sql);
        }
        // End of Monthly ECA
        // End of ECA
        function getMonthSelection($AcademicYearID, $selectedMonth='', $Attribute=''){
        	$yearName = getAcademicYearByAcademicYearID($AcademicYearID);
        	$yearName = substr($yearName,0,4);
        	$nextYr = strval($yearName) + 1;
        	$returnStr = '<select ' . $Attribute . '>';
        	for($i=1;$i<=12;$i++){
        		$selected = ($i==$selectedMonth) ? 'selected' : '';
        		if($i<9){
        			$yr = $nextYr;
        			$returnStr .= '<option value="'.$i.'" '.$selected.'>0'.$i.'</option>';
        		}else {
        			$yr = $yearName;
        			$returnStr .= '<option value="'.$i.'" '.$selected.'>'.($i<10?'0':'').$i.'</option>';
        		}
        	}
        	$returnStr .= '</select>';
        	return $returnStr;
        }
    }
 // End Class
}

?>