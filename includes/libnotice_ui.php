<?
# modifying : 

##### Change Log [Start] #####
#
#   Date    :   2020-11-09 (Bill) [2020-1009-1753-46096]
#               modified Get_Print_Module_Notice_UI(), support send push message with scheduled delay   ($sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay'])
#
#   Date    :   2019-03-20 (Bill) [2019-0313-1725-12066]
#               modified Get_Print_Module_Notice_UI(), support print multiple notice on same page   ($sys_custom['eDiscipline']['PrintNumOfDisciplineNoticeInSamePaper'])
#
#   Date    :   2018-10-08 (Bill) [2018-0918-0921-13207]
#               modified Get_Print_Module_Notice_UI(), updated sql to improve performance
#
#	Date	:	2016-12-07 (Ivan) [Q109755] [ip.2.5.8.1.1.0]
#				modified Get_Print_Module_Notice_UI() js function sendPushMessageToParent, removed an extra "," to fix js error in IE
#
#	Date	:	2016-09-08 (Bill)
#				modified Get_Print_Module_Notice_UI(), support print notice when target date range within 1 day
#
#	Date	:	2016-08-17 (Bill)	[2016-0801-1134-09054]
#				modified Get_Print_Module_Notice_UI(), support send push message to parents for discipline notice
#
#	Date	:	2016-08-03 (Bill)
#				modified Get_Print_Module_Notice_UI(), support eNotice using DateTime
#
#	Date	:	2015-07-29 (Evan)
#				modified Get_Print_Module_Notice_UI(), Update search bar style
#
#	Date	:	2014-09-19 (YatWoon)
#				modified Get_Print_Module_Notice_UI(), change icons [Case#H61852]
#				Deploy: IPv10.1
#
#	Date	:	2013-07-19 (YatWoon)
#				modified Get_Print_Module_Notice_UI(), add "Print Envelope" function for MST ($sys_custom['eDiscipline']['MST_envelope'])
#
#	Date	:	2011-11-10 (Henry Chow)
#				modified Get_Print_Module_Notice_UI(), add "Class" filter
#
#	Date	:	2011-01-11 (Henry Chow)
#				modified Get_Print_Module_Notice_UI(), preset the default values for parameters.
#
#	Date	:	2010-11-18 (Henry Chow)
#				modified Get_Print_Module_Notice_UI(), modofied the display of "Manage Signature Footer"
#
#	Date	:	2010-11-02 (Henry Chow)
#				modified Get_Print_Module_Notice_UI(), add display of "Manage Signature Footer"
#
#	Date	:	2010-10-08 Henry Chow
#				modified Get_Print_Module_Notice_UI(), change the table header from <td> to <th>
#
###### Change Log [End] ######
include_once("libinterface.php");

class libnotice_ui extends interface_html {
	function libnotice_ui() {
		parent::interface_html();
	}
	
	function Get_Print_Module_Notice_UI($ModuleName,$title,$status,$sign_status,$print_status,$StartDate,$EndDate,$keyword, $submitOptionFlag="", $updateOption="", $option=array(), $targetClass="")
	{
		// setting
		global $PATH_WRT_ROOT, $Lang, $eComm, $image_path, $LAYOUT_SKIN, $plugin, $sys_custom;
		// db table 
		global $field, $order, $pageNo, $numPerPage, $page_size_change, $ck_page_size, $page_size;
		// lang
		global $i_Notice_Signed, $i_Notice_Unsigned, $button_preview, $i_Notice_DateStart, $i_identity_student, $i_Notice_Title, $button_submit;
		global $button_find, $eNotice, $i_invalid_date, $i_Notice_StatusPublished, $i_Notice_StatusSuspended, $button_print_selected, $i_Discipline_System_Award_Punishment_All_Classes;
		global $button_print_all, $i_con_msg_date_startend_wrong_alert, $i_Discipline_System_Notice_Warning_Please_Select, $msg_check_at_least_one_item;
		
		include_once($PATH_WRT_ROOT."includes/libnotice.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable.php");
		include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		
		$lclass = new libclass();
		$select_class = $lclass->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"this.form.submit();\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes)."&nbsp;\n";
		
		//$sign_status = $sign_status ? $sign_status : 0;
		$status = $status ? $status : "";
		if($sign_status=="") $sign_status = -1;
		if($print_status=="") $print_status = -1;
		$StartDate = $StartDate!="" ? $StartDate : date("Y-m-d");
		$EndDate = $EndDate!="" ? $EndDate : date("Y-m-d");
		
		// [2016-0801-1134-09054]
		$displayPushMessageBtn = $plugin['eClassApp'] && $ModuleName=='DISCIPLINE';
		
		// [2019-0313-1725-12066]
		$print_notice_num_parms = '';
		if($sys_custom['eDiscipline']['PrintNumOfDisciplineNoticeInSamePaper'] && $ModuleName=='DISCIPLINE') {
		    $print_notice_num_parms = '+\'&extra_print_notice_num='.$sys_custom['eDiscipline']['PrintNumOfDisciplineNoticeInSamePaper'].'\'';
		}

        // [2020-1009-1753-46096]
		$send_push_msg_parms = '';
		if(isset($sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay']) && $sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay'] > 0 && $ModuleName=='DISCIPLINE') {
            $send_push_msg_parms = ' , scheduledDelay : '.$sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay'].' ';
        }
		
		# TABLE INFO
		if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
		$pageSizeChangeEnabled = true;
		if($field=="") $field = 0;
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array("a.DateStart");
		
		$x = "";
		$name_field = getNameFieldWithClassNumberByLang("c.");
		if($sign_status != -1) {
			$sign_status_cond = " AND " . ($sign_status ? "b.RecordType IS NOT NULL" : "b.RecordType IS NULL");
		}
		if($print_status != -1) {
			$print_status_cond = " AND " . ($print_status ? "b.PrintDate IS NOT NULL" : "b.PrintDate IS NULL");
		}
		
		# 20091230 YatWoon
		$title = $title ? $title : -1;
		if($title != -1) {
			$title_cond = " AND a.Title = '". $title ."'";
		}		
		if($targetClass != "" && $targetClass != "0") {
			$studentAry = $lclass->storeStudent(0, $targetClass);
			if(count($studentAry) > 0) {
				$class_cond = " AND c.UserID IN (".implode(',', $studentAry).")";	
			}
		}
		
		// Use time range as eNotice using DateTime 
		$StartDateTime = $StartDate." 00:00:00";
		$EndDateTime = $EndDate." 23:59:59";
		
		$viewIcon = "<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" alt=\"".$button_preview."\" border=0>";                     
		$sql = "SELECT 
    				LEFT(a.DateStart, 10),
    				$name_field as studentname,
    				CONCAT('<a href=\"javascript:ViewNoticePrintPreview(', a.NoticeID, ');\">". $viewIcon ."</a> ', a.Title),
    				IF(b.RecordType IS NOT NULL, '".$i_Notice_Signed."', '".$i_Notice_Unsigned."'),
    				IF(b.PrintDate IS NOT NULL, b.PrintDate, '". $Lang['eNotice']['NonPrint'] ."'),
    				a.NoticeID
				FROM 
					INTRANET_NOTICE as a 
					INNER JOIN INTRANET_NOTICE_REPLY as b on (b.NoticeID = a.NoticeID)
					/*inner join INTRANET_USER as c on (concat('U',c.UserID) = a.RecipientID)*/
                    INNER JOIN INTRANET_USER as c on (c.UserID = b.StudentID)
				WHERE
					a.DateStart >= '".$StartDateTime."' AND a.DateStart <= '".$EndDateTime."' ";
		if ($status != "") {
			$sql .= " AND a.RecordStatus = ".$status." ";
		}
		$sql .= "	AND a.Module = '".$ModuleName."' AND isModule = 1 
					".$sign_status_cond." 
					".$print_status_cond." 
					".$title_cond."
					".$class_cond." ";
		
		$keyword = stripslashes(trim($keyword));
		if ($keyword != "")
		{
		    $sqlkeyword = intranet_htmlspecialchars($keyword);
		    
		    $sql2 = "  SELECT DISTINCT(c.UserID) FROM INTRANET_USER as c
		              WHERE c.RecordType = '".USERTYPE_STUDENT."' AND c.RecordStatus IN (0, 1) AND 
                            ($name_field LIKE '%".$sqlkeyword."%' OR c.EnglishName LIKE '%".$sqlkeyword."%' OR c.ChineseName LIKE '%".$sqlkeyword."%')";
		    $keyword_student_ids = $lclass->returnVector($sql2);
		    $keyword_student_ids = implode("', '", $keyword_student_ids);
		    $searchCond = $keyword_student_ids == ""? "" : " OR b.StudentID IN ('".$keyword_student_ids."')";
		    
// 			$sql .= "
// 						and (studentname like '%".$sqlkeyword."%' or a.Title like '%".$sqlkeyword."%' or c.EnglishName like '%".$sqlkeyword."%' or c.ChineseName like '%".$sqlkeyword."%')
// 						";
			$sql .= "	AND (a.Title LIKE '%".$sqlkeyword."%' ".$searchCond.") ";
		}
		//debug_pr($sql);exit;
		$li->sql = $sql;
		
		// [2016-0801-1134-09054]
		$li->no_col = ($displayPushMessageBtn)? 7 : 6;
		$li->IsColOff = "eNoticedisplayNoticePrint";
		
		// TABLE COLUMN
		$li->column_list .= "<th>".$i_Notice_DateStart."</th>\n";
		$li->column_list .= "<th>".$i_identity_student."</th>\n";
		$li->column_list .= "<th>".$i_Notice_Title."</th>\n";
		$li->column_list .= "<th>".$i_Notice_Signed."/".$i_Notice_Unsigned."</th>\n";
		$li->column_list .= "<th>".$Lang['eNotice']['PrintDate']."</th>\n";
		
		// [2016-0801-1134-09054]
		if($displayPushMessageBtn){
			$li->column_list .= "<th>".$Lang['AppNotifyMessage']['SendTime']."</th>\n";
		}
		$li->column_list .= "<th>".$li->check("NoticeIDArr[]")."</th>\n";
		
		### Button
		$PrintBtn_All		= "<a href=\"javascript:printAll()\" class='contenttool'><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_print_all.gif\" border=\"0\" align=\"absmiddle\" /> " . $button_print_all . "</a>";
		$PrintBtn_Selected	= "<a href=\"javascript:printSelected()\" class='contenttool'><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_print_selected.gif\" border=\"0\" align=\"absmiddle\" /> " . $button_print_selected . "</a>";
		if($sys_custom['eDiscipline']['MST_envelope']) {
			$PrintEnvelope = "<a href=\"javascript:printEnvelope()\" class='contenttool'><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_print_envelope.gif\" border=\"0\" align=\"absmiddle\" /> " . $Lang['Btn']['PrintEnvelope'] . "</a>";
		}
		
		//	[2016-0801-1134-09054]
		$sendMessageBtn = "&nbsp;";
		if($displayPushMessageBtn) {
			$sendMessageBtn = "<span id=\"messageBtn\">".$this->GET_SMALL_BTN($Lang['AppNotifyMessage']['button'], "button", "sendPushMessageToParent()")."</span>";
			$sendMessageBtn .= "<span id=\"messageBtnMsg\" style=\"display:none\"><img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\">Loading...</span>";
		}
		
		### Search
		$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><td>";
		$searchTag 	.= "<div class=\"Conntent_search\"><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes(intranet_htmlspecialchars($keyword))."\" onFocus=\"this.form.pageNo.value=1;\"></div></td>";
		$searchTag 	.= "</table>";
		
		### Filter - Status
		$status_select = "<SELECT name='status' onChange=this.form.submit()>\n";
		$status_select.= "<OPTION value='' ".($status==""? "SELECTED":"").">-- ".$Lang['General']['All']." --</OPTION>\n";
		$status_select.= "<OPTION value=1 ".($status==1? "SELECTED":"").">".$i_Notice_StatusPublished."</OPTION>\n";
		$status_select.= "<OPTION value=2 ".($status==2? "SELECTED":"").">".$i_Notice_StatusSuspended."</OPTION>\n";
		$status_select.= "</SELECT>&nbsp;\n";
		
		### Filter - Signed / Unsign
		$sign_status_select = "<SELECT name='sign_status' onChange=this.form.submit()>\n";
		$sign_status_select.= "<OPTION value=-1 ".(strlen($sign_status)==0 || $sign_status==-1? "SELECTED":"").">". $Lang['eNotice']['AllSignStatus'] ."</OPTION>\n";
		$sign_status_select.= "<OPTION value=0 ".(strlen($sign_status) && $sign_status==0? "SELECTED":"").">$i_Notice_Unsigned</OPTION>\n";
		$sign_status_select.= "<OPTION value=1 ".($sign_status==1? "SELECTED":"").">$i_Notice_Signed</OPTION>\n";
		$sign_status_select.= "</SELECT>&nbsp;\n";
		
		### Filter - Print / non-print
		$print_status_select = "<SELECT name='print_status' onChange=this.form.submit()>\n";
		$print_status_select.= "<OPTION value=-1 ".(strlen($print_status)==0 || $print_status==-1? "SELECTED":"").">". $Lang['eNotice']['AllPrintStatus'] ."</OPTION>\n";
		$print_status_select.= "<OPTION value=0 ".(strlen($print_status) && $print_status==0? "SELECTED":"").">". $Lang['eNotice']['NonPrint'] ."</OPTION>\n";
		$print_status_select.= "<OPTION value=1 ".($print_status==1? "SELECTED":"").">". $Lang['eNotice']['Printed'] ."</OPTION>\n";
		$print_status_select.= "</SELECT>&nbsp;\n";
		
		# 20091230 YatWoon
		### Filter - Title
		$sql = "SELECT DISTINCT(Title) FROM INTRANET_NOTICE WHERE Module = '". $ModuleName ."' AND isModule = 1 ORDER BY Title";
		$result = $li->returnArray($sql);
		$title_select = "<SELECT name='title' onChange=this.form.submit()>\n";
		$title_select .= "<option value='-1'>". $Lang['eNotice']['AllNoticeTitle'] ."</option>";
		foreach($result as $k => $d) {
			if(trim($d['Title'])=="")	continue;
		 	$title_select .= "<option value='". $d['Title'] ."' ". ($title==$d['Title'] ? "selected":"") .">". $d['Title'] ."</option>";
		}
		$title_select .= "</select>";
		
		### Filter - Date Range
		$date_select = $eNotice['Period_Start'] .": ";
		$date_select .= $this->GET_DATE_PICKER("StartDate", $StartDate) . "&nbsp;";
		$date_select .= $eNotice['Period_End']."&nbsp;";
		$date_select .= $this->GET_DATE_PICKER("EndDate", $EndDate);
		$date_select .= $this->GET_SMALL_BTN($Lang['Btn']['Submit'], "submit");
		
		//for($i=0; $i<sizeof($Lang['eDiscipline']['NoticeFooterArray']); $i++) {
		foreach($Lang['eDiscipline']['NoticeFooterArray'] as $id=>$name) {
			$optionField .= '<tr><td class="'.(($updateOption==2) ? "" : "tabletextremark").'"><input type="checkbox" '.(($updateOption==2) ? "" : "disabled").' name="option[]" id="option'.($id).'" value="'.($id).'" '.(sizeof($option)>0 && in_array(($id), $option) ? "checked" : "").'><label for="option'.($id).'">'.$name[0]." / ".str_replace("<br>","",$name[1]).'</td></tr>';
		}
		
		$x = '
		<SCRIPT LANGUAGE=Javascript>
		<!--//
		function printAll()
		{
		var c = "";
		var optionStr = "";
		';
		
		if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature'])
		{
			$x .= '
				len = document.form1.elements[\'option[]\'].length;
				var total = 0;
				var optionStr = "";
				for(i=1; i<=len; i++) {
					//total += (eval("document.getElementById(\'option"+i+"\')").checked==true) ? 1 : 0;
					if(eval("document.getElementById(\'option"+i+"\')").checked==true) {
						total++;
						optionStr += i+",";
					}
				}
	
				if(document.form1.updateOption[1].checked==true && total==0) {
					alert("'.$Lang['eDiscipline']['SelectFooterWarningMsg'].'");
				} else {
					var c = document.getElementById(\'updateOption1\').checked==true ? 1 : 2;
					optionStr = (optionStr!="") ? optionStr.substr(0,(optionStr.length-1)) : optionStr;
			';
		}
		
		$x .= '
			/*
			len = document.form1.elements[\'option[]\'].length;
			var total = 0;
			var optionStr = "";
			for(i=1; i<=len; i++) {
				//total += (eval("document.getElementById(\'option"+i+"\')").checked==true) ? 1 : 0;
				if(eval("document.getElementById(\'option"+i+"\')").checked==true) {
					total++;
					optionStr += i+",";
				}
			}

			if(document.form1.updateOption[1].checked==true && total==0) {
				alert("'.$Lang['eDiscipline']['SelectFooterWarningMsg'].'");
			} else {
			
				var c = document.getElementById(\'updateOption1\').checked==true ? 1 : 2;
				optionStr = (optionStr!="") ? optionStr.substr(0,(optionStr.length-1)) : optionStr;
			*/
				if(confirm("'.$Lang['eNotice']['ConfirmForPrint'].'"))
					newWindow(\''.$PATH_WRT_ROOT.'home/eService/notice/module_print_all_preview.php?keyword='.$keyword.'&status='.$status.'&sign_status='.$sign_status.'&print_status='.$print_status.'&StartDate='.$StartDate.' 00:00:00&EndDate='.$EndDate.' 23:59:59&thisModule='.$ModuleName.'\'+\'&updateOption=\'+c+\'&option=\'+optionStr'.$print_notice_num_parms.',10);';
			if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature']) 
				$x .= '}';
			$x .= '
			//}
		}
		
		function printSelected()
		{
			var s=\'\';
			var c = "";
			var optionStr = "";
			var x=document.getElementsByName("NoticeIDArr[]");
			for(i=0;i<x.length;i++)
			{
				if(x[i].checked)
				{
					s = s +x[i].value 	+",";
				}
			}
			
			if(s==\'\')
			{
				alert("'.$i_Discipline_System_Notice_Warning_Please_Select.'");
			}
			else
			{';
		
		if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature'])
		{
			$x .= '
				len = document.form1.elements[\'option[]\'].length;
				var total = 0;
				var optionStr = "";

				for(i=1; i<=len; i++) {
					//total += (eval("document.getElementById(\'option"+i+"\')").checked==true) ? 1 : 0;
					if(eval("document.getElementById(\'option"+i+"\')").checked==true) {
						total++;
						optionStr += i+",";
					}
				}
				if(document.form1.updateOption[1].checked==true && total==0) {
					alert("'.$Lang['eDiscipline']['SelectFooterWarningMsg'].'");
				} else {
					var c = document.getElementById(\'updateOption1\').checked==true ? 1 : 2;
					optionStr = (optionStr!="") ? optionStr.substr(0,(optionStr.length-1)) : optionStr;
			';
		}

		$x .= '
			/*
				len = document.form1.elements[\'option[]\'].length;
				var total = 0;
				var optionStr = "";
				for(i=1; i<=len; i++) {
					//total += (eval("document.getElementById(\'option"+i+"\')").checked==true) ? 1 : 0;
					if(eval("document.getElementById(\'option"+i+"\')").checked==true) {
						total++;
						optionStr += i+",";
					}
				}
				if(document.form1.updateOption[1].checked==true && total==0) {
					alert("'.$Lang['eDiscipline']['SelectFooterWarningMsg'].'");
				} else {
					var c = document.getElementById(\'updateOption1\').checked==true ? 1 : 2;
					optionStr = (optionStr!="") ? optionStr.substr(0,(optionStr.length-1)) : optionStr;
			*/
					if(confirm("'.$Lang['eNotice']['ConfirmForPrint'].'"))
						newWindow(\''.$PATH_WRT_ROOT.'home/eService/notice/module_print_all_preview.php?NoticeIDStr=\'+s+\'&updateOption=\'+c+\'&option=\'+optionStr'.$print_notice_num_parms.',10);';
		if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature']) 
			$x .= '}';				
		$x .= '
				//}
			}
			
		}';
		
		// [2016-0801-1134-09054]
		if($displayPushMessageBtn)
		{
		$x .= ' 
			function sendPushMessageToParent()
			{
				var validNotice = false;
				var NoticeIDList = "";

				// loop notice checkbox
				var x=document.getElementsByName("NoticeIDArr[]");
				for(i=0; i<x.length; i++)
				{
					if(x[i].checked)
					{
						validNotice = true;
						NoticeIDList = NoticeIDList + x[i].value + ",";
					}
				}
				
				$(\'#returnMsg\').html(\'\');

				// Send notification
				if(validNotice)
				{
					$(\'#messageBtn\').hide();
					$(\'#messageBtnMsg\').show();
					$.ajax({
					  		type: "POST",
					  		url: "../send_notification.php",
					  		data: { NoticeIDs : NoticeIDList '.$send_push_msg_parms.'},
						    success: function(response) {
								$(\'#returnMsg\').html(response);
								$(\'#messageBtn\').show();
								$(\'#messageBtnMsg\').hide();
						    },
						    error: function(xhr) {
								alert(\'request error\');
								$(\'#messageBtn\').show();
								$(\'#messageBtnMsg\').hide();
						    }
					});
				}
				else
				{
					alert("'.$i_Discipline_System_Notice_Warning_Please_Select.'");
				}
			}';
		}
		
		$x .=' 
		function printEnvelope()
		{
			var s=\'\';
			var c = "";
			var optionStr = "";
			var x=document.getElementsByName("NoticeIDArr[]");
			for(i=0;i<x.length;i++)
			{
				if(x[i].checked)
				{
					s = s +x[i].value 	+",";
				}
			}
			
			if(s==\'\')
			{
				alert("'.$i_Discipline_System_Notice_Warning_Please_Select.'");
			}
			else
			{
				newWindow(\''.$PATH_WRT_ROOT.'home/eService/notice/mst_print_envelope.php?NoticeIDStr=\'+s+\'&updateOption=\'+c+\'&option=\'+optionStr,10);
			}
		}
		
		function ViewNoticePrintPreview(id)
		{
			if(confirm("'.$Lang['eNotice']['ConfirmForPrint'].'"))
				newWindow(\''.$PATH_WRT_ROOT.'home/eService/notice/module_print_preview.php?NoticeID=\'+id+\'&\',10);
		}
		
		function check_form()
		{
			obj = document.form1;
			
			if(!check_date(obj.StartDate,"'.$i_invalid_date.'"))
			{
				obj.StartDate.focus();
				return false;
			}
			
			if(!check_date(obj.EndDate,"'.$i_invalid_date.'"))
			{
				obj.EndDate.focus();
				return false;
			}
			
			if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
				obj.StartDate.focus();
				alert("'.$i_con_msg_date_startend_wrong_alert.'");
				return false;
			}	
			
			return true;
		}

		function showHideLayer(layerName, flag) {
			if(flag=="") {
				flag = (document.form1.DisplayFooter.value==0) ? 1 : 0;
			}

			if(flag==1) {
				document.getElementById(layerName).style.visibility = "visible";
				document.getElementById("DisplayFooter").value = 1;
			} else {
				document.getElementById(layerName).style.visibility = "hidden";
				document.getElementById("DisplayFooter").value = 0;
			}			
		}

		function changeEnable(flag) {
			if(flag==1) {
				disableFlag = false;
				textClass = "";
			} else {
				disableFlag = true;
				textClass = "tabletextremark";
			}

			len = document.form1.elements[\'option[]\'].length;
			for(i=1; i<=len; i++) {
				eval("document.getElementById(\'option"+i+"\')").disabled = disableFlag;
				eval("document.getElementById(\'option"+i+"\')").parentNode.className = textClass;
			}
		}


		//-->
		</SCRIPT>
		
		<br />
		<form name="form1" method="get" action="index.php" onSubmit="return check_form();">
		
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
	    <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" class="tabletext">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
    				<tr>
              <td colspan="2">
                <table border="0" cellspacing="0" cellpadding="2" width="100%">
                <tr>
                	<td id="returnMsg" colspan="2" align="right">&nbsp;</td>
				</tr>
                <tr>
                	<td width="70%"><p>'.$PrintBtn_All.' '.$PrintBtn_Selected.' ' . $PrintEnvelope .'</p></td>
					<td width="30%" align="right"">'.$searchTag.'</td>
				</tr>
               	</table>
              </td>
              <td align="right" valign="bottom">'.$this->GET_SYS_MSG($xmsg, $xmsg2).'</td>
						</tr>

            <tr>
              <td align="left" valign="bottom" style="padding-left: 15px">'.$select_class.$status_select.$sign_status_select.$print_status_select.$title_select.'<br>'.$date_select.'</td>';
		
		if($sys_custom['eDisciplinev12_SunKei_eNoticeSignature'])
		{
			$x .= '      
              <td align="right" valign="bottom" height="28"><a href="javascript:;" class="tablelink" onclick="showHideLayer(\'DisplayOptionDiv\',\'\');">'.$Lang['eDiscipline']['ManageNoticeSignature'].'</a>&nbsp;'.$sendMessageBtn.'<br>';

			$x .= '
				<div id="DisplayOptionTableDivTableDiv" style="float:right;position:relative;left:-285px;top:2px;z-index:20;">
				<table id="DisplayOptionDivTable">
				<tr>
				<td>
				<div id="DisplayOptionDiv" class="selectbox_layer" style="float:right;width:280px;left:100">
				<table id="FilterSelectionTable" width="100%" cellpadding="2" cellspacing="0" border="0">
				<tr>
				<td style="text-align:right">
				<a href="javascript:;" class="tablelink" onclick="showHideLayer(\'DisplayOptionDiv\',0);"> '.$eComm['Close'].'</a>
				</td>
				</tr>
				<tr>
				<td style="text-align:left">
				<input type="radio" value="1" id="updateOption1" name="updateOption" class="DisplayOptionChkBox" onclick="changeEnable(0)" '.(($updateOption==1 || $updateOption=="") ? "checked" : "").' /><label for="updateOption1"> '.$Lang['eDiscipline']['RemainSignatureOption'].'</label></td>
				</tr>
				<tr>
				<td style="text-align:left">
				<input type="radio" value="2" id="updateOption2" name="updateOption" class="DisplayOptionChkBox" onclick="changeEnable(1)" '.(($updateOption==2) ? "checked" : "").' /><label for="updateOption2"> '.$Lang['eDiscipline']['UpdateSignatureOption'].'</label></td>
				</tr>
					'.$optionField.'
				</table>
				</div>
					
				</td>
				</tr>
				</table>
				</div>
				</td>';
		}
		else
		{
			$x .= '<td valign="bottom" align="right">'.$sendMessageBtn.'</td>';
		}
        
        $x .= '
            </tr>
            <tr>
	            <td colspan="2" style="padding-left: 15px; padding-top: 15px">
								'.$li->display($displayPushMessageBtn).'
	            </td>
            </tr>
						</table>
				  </td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
		<br />
		
		<input type="hidden" name="NoticeID" value="" />
		<input type="hidden" name="DisplayFooter" id="DisplayFooter" value="0" />
		<input type="hidden" name="submitOptionFlag" id="submitOptionFlag" value="0" />
		<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />
		<input type="hidden" name="field" value="'.$li->field.'" />
		<input type="hidden" name="page_size_change" value="'.$page_size_change.'" />
		<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />
		
		</form>';
		
		return $x;
	}
}
?>