<?php
#  Editing by 
/* ***************************************************
 * Modification Log
 */
if (!defined("LIBREPORTCARD_PC"))         // Preprocessor directives
{
	define("LIBREPORTCARD_PC", true);

	class libreportcard_pc extends libreportcard {
		
		public function __construct() {
			$this->libreportcard();
		}
		
		public function Get_Personal_Characteristics_Scale($ScaleIDArr='', $CodeArr='', $IsDefault='', $ExcludeScaleID='') {
			if ($ScaleIDArr !== '') {
				$conds_ScaleID = " And ScaleID In ('".implode("','", (array)$ScaleIDArr)."') ";
			}
			
			if ($CodeArr !== '') {
				$conds_Code = " And Code In ('".implode("','", (array)$CodeArr)."') ";
			}
			
			if ($IsDefault !== '') {
				$conds_IsDefault = " And IsDefault = '".$IsDefault."' ";
			}
			
			if ($ExcludeScaleID !== '') {
				$conds_EXcludeScaleID = " And ScaleID Not In ('".implode("','", (array)$ScaleIDArr)."') ";
			}
			
			
			$sql = "Select
							ScaleID,
							Code,
							NameEn,
							NameCh,
							IsDefault,
							DisplayOrder
					From
							".$this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE
					Where
							1
							$conds_ScaleID
							$conds_Code
							$conds_IsDefault
							$conds_EXcludeScaleID
					Order By
							DisplayOrder
					";
			return $this->returnResultSet($sql);
		}
		
		public function Insert_Scale_Record($DataArr) {
			if (count((array)$DataArr) == 0)
				return false;
				
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value) {
				$fieldArr[] = $field;
				$valueArr[] = "'".$this->Get_Safe_Sql_Query($value)."'";
			}
			
			## set others fields
			$InsertUserID = ($_SESSION['UserID'])? "'".$_SESSION['UserID']."'" : 'NULL';
			# Input By
			$fieldArr[] = 'InputBy';
			$valueArr[] = $InsertUserID;
			# DateInput
			$fieldArr[] = 'DateInput';
			$valueArr[] = 'now()';
			# Last Modified By
			$fieldArr[] = 'ModifiedBy';
			$valueArr[] = $InsertUserID;		
			# DateModified
			$fieldArr[] = 'DateModified';
			$valueArr[] = 'now()';
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$RC_PERSONAL_CHARACTERISTICS_SCALE = $this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE";
			$sql = "Insert Into $RC_PERSONAL_CHARACTERISTICS_SCALE 
						( ".$fieldText." )
						Values
						( ".$valueText." )
					";		
			$success = $this->db_db_query($sql);
			
			if ($success == false) {
				return 0;
			}
			else {
				$insertedID = $this->db_insert_id();
				return $insertedID;
			}
		}
		
		public function Update_Scale_Record($ScaleID, $DataArr)
		{
			if (count($DataArr) == 0)
				return false;
				
			# Build field update values string
			$valueFieldText = '';
			foreach ($DataArr as $field => $value)
			{
				$valueFieldText .= $field.' = \''.$this->Get_Safe_Sql_Query($value).'\', ';
			}
			$valueFieldText .= ' DateModified = now(), ';
			$LastModifiedBy = ($_SESSION['UserID'])? '\''.$_SESSION['UserID'].'\'' : 'NULL';
			$valueFieldText .= ' ModifiedBy = '.$LastModifiedBy.' ';
			
			
			$RC_PERSONAL_CHARACTERISTICS_SCALE = $this->DBName.'.RC_PERSONAL_CHARACTERISTICS_SCALE';
			$sql = "Update $RC_PERSONAL_CHARACTERISTICS_SCALE Set $valueFieldText Where ScaleID = '".$ScaleID."'";
			return $this->db_db_query($sql);
		}
		
		public function Delete_Scale($ScaleIDArr) {
			$SuccessArr = array();
			
			$sql = "Delete From	".$this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE Where ScaleID In ('".implode("','", (array)$ScaleIDArr)."')";
			$SuccessArr['DeleteScaleInfo'] = $this->db_db_query($sql);
			
			$sql = "Delete From	".$this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER Where ScaleID In ('".implode("','", (array)$ScaleIDArr)."')";
			$SuccessArr['DeleteMgmtUser'] = $this->db_db_query($sql);
			
			return (in_array(false, $SuccessArr))? false : true;
		}
		
		public function Is_Scale_Code_Valid($Code, $ExcludeScaleID) {
			$Code = $this->Get_Safe_Sql_Query(trim($Code));
		
			if ($ExcludeScaleID != '') {
				$conds_ExcludeScaleID = " And ScaleID != '$ExcludeScaleID' ";
			}
			
			$sql = "Select
							ScaleID
					From
							".$this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE
					Where
							Code = '".$Code."'
							$conds_ExcludeScaleID
					";
			$ResultArr = $this->returnResultSet($sql);
			return (count($ResultArr) > 0)? false : true;
		}
		
		public function Get_Scale_Max_DisplayOrder() {
			$RC_PERSONAL_CHARACTERISTICS_SCALE = $this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE";
			$sql = "Select Max(DisplayOrder) as MaxDisplayOrder From $RC_PERSONAL_CHARACTERISTICS_SCALE";
			$infoArr = $this->returnResultSet($sql);
			
			return $infoArr[0]['MaxDisplayOrder'];
		}
		
		public function Get_Personal_Characteristics_Scale_Mgmt_Available_User($ScaleID, $SearchUserLogin='', $ExcludeUserIDArr='', $TargetUserType='')
		{
			if (!is_array($ExcludeUserIDArr))
				$ExcludeUserIDArr = array();
				
			### Exclude Current Member
			$MemberInfoArr = $this->Get_Personal_Characteristics_Scale_Mgmt_Group_Member($ScaleID);
			$MemberUserIDArr = Get_Array_By_Key($MemberInfoArr, 'UserID');
			if (count($MemberUserIDArr) > 0) {
				$MemberList = implode(',', $MemberUserIDArr);
				$conds_excludeMember = " And UserID Not In ('".implode("','", (array)$MemberUserIDArr)."') ";
			}
			
			### Exclude Other Users
			$ExcludeUserIDArr = array_remove_empty((array)$ExcludeUserIDArr);
			if (is_array($ExcludeUserIDArr) && count($ExcludeUserIDArr) > 0) {
				$excludeUserList = implode(',', $ExcludeUserIDArr);
				$conds_excludeUserID = " And UserID Not In ('".implode("','", (array)$ExcludeUserIDArr)."') ";
			}
			
			if ($SearchUserLogin != '') {
				$conds_userlogin = " And UserLogin Like '%$SearchUserLogin%' ";
			}
	
			if ($TargetUserType != '') {
				$conds_UserType = " And RecordType In ('".implode("','", (array)$TargetUserType)."') ";
			}
	
			$name_field = getNameFieldByLang();
			$sql = "Select
							UserID,
							$name_field as UserName,
							UserLogin
					From
							INTRANET_USER
					Where
							RecordStatus = 1
							$conds_excludeUserID
							$conds_excludeMember
							$conds_userlogin
							$conds_UserType
					Order By
							EnglishName
					";
			$returnArr = $this->returnResultSet($sql);
			return $returnArr;
		}
		
		public function Get_Personal_Characteristics_Scale_Mgmt_Group_Member($ScaleIDArr='') {
			if ($ScaleIDArr != '') {
				$conds_ScaleID = " And ScaleID In ('".implode("','", (array)$ScaleIDArr)."') ";
			}
			
			$sql = "Select 
							ScaleID, UserID
					From
							".$this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER
					Where
							1
							$conds_ScaleID
					";
			return $this->returnResultSet($sql);
		}
		
		public function Get_Invalid_Scale_Mgmt_Member_Selection($ScaleID, $SelectionList) {
			$InvalidSelectionArr = array();
		
			$SelectionArr = explode(',', $SelectionList);
			$SelectionArr = array_remove_empty((array)$SelectionArr);
			$numOfSelection = count($SelectionArr);
			
			if ($numOfSelection > 0)
			{
				$GroupMemberInfoArr = $this->Get_Personal_Characteristics_Scale_Mgmt_Group_Member($ScaleID);
				$GroupMemberUserIDArr = Get_Array_By_Key($GroupMemberInfoArr, 'UserID');
				
				for ($i=0; $i<$numOfSelection; $i++)
				{
					$thisSelection = $SelectionArr[$i];
					$thisUserIDArr = Get_User_Array_From_Common_Choose(array($thisSelection), $UserTypeArr=array(USERTYPE_STAFF));
					
					# $thisIsMemberUserIDArr contains the UserID which is member and is in the user selection => which is an invalid selection
					$thisIsMemberUserIDArr = array_intersect($GroupMemberUserIDArr, $thisUserIDArr);
					
					if (count($thisIsMemberUserIDArr)) {
						$InvalidSelectionArr[] = $thisSelection;
					}
				}
			}
			
			$InvalidSelectionList = implode(',', $InvalidSelectionArr);
			return $InvalidSelectionList;
		}
		
		public function Add_Personal_Characteristics_Scale_Mgmt_Member($ScaleID, $TargetMemberArr) {
			$TargetMemberArr = array_remove_empty((array)$TargetMemberArr);
			$UserIDArr = Get_User_Array_From_Common_Choose($TargetMemberArr, $UserTypeArr=array(USERTYPE_STAFF));
			$numOfUser = count($UserIDArr);
			
			if ($numOfUser > 0)
			{
				$InsertValueArr = array();
				for ($i=0; $i<$numOfUser; $i++)
				{
					$thisUserID = $UserIDArr[$i];
					$InsertValueArr[] = "('".$ScaleID."', '".$thisUserID."', now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')";
				}
				
				$insertValueList = implode(', ', $InsertValueArr);
				$RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER = $this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER";
				$sql = "Insert Into $RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER
							(ScaleID, UserID, DateInput, InputBy, DateModified, ModifiedBy)
						Values
							$insertValueList
						";
				$success = $this->db_db_query($sql);
			}
			
			return $success;
		}
		
		function Get_Personal_Characteristics_Scale_Mgmt_User_DBTable_Sql($ScaleID, $Keyword='') {
			global $Lang;
			
			if ($Keyword != '') {
				$conds_Keyword = " And ( iu.EnglishName Like '%$Keyword%' Or iu.ChineseName Like '%$Keyword%' ) ";
			}
			
			$RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER = $this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER";
			$sql = "
				SELECT 
					iu.EnglishName,
					iu.ChineseName,
					CONCAT('<input type=\"checkbox\" name=\"UserIDArr[]\" value=\"', iu.UserID, '\">') as CheckBox
				From
					$RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER as mu
					Inner Join INTRANET_USER as iu On (mu.UserID = iu.UserID)
				Where
					mu.ScaleID = '".$ScaleID."'
					And iu.RecordType = '".USERTYPE_STAFF."'
					And iu.RecordStatus = 1
					$conds_Keyword		
			";
			return $sql;
		}
	
		
	
	
		public function Delete_Personal_Characteristics_Scale_Mgmt_User($ScaleID, $UserIDArr)
		{
			if (count($UserIDArr) == 0)
				return false;
				
			$UserIDList = implode(', ', $UserIDArr);
			$RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER = $this->DBName.".RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER";
			$sql = "Delete From 
							$RC_PERSONAL_CHARACTERISTICS_SCALE_MGMT_USER
					Where
							ScaleID = '".$ScaleID."'
							And UserID In ('".implode("','", (array)$UserIDArr)."')
					";
			return $this->db_db_query($sql);
		}
		
		public function Is_Scale_Linked_Data($ScaleIDArr) {
			$RC_PERSONAL_CHARACTERISTICS_DATA = $this->DBName.".RC_PERSONAL_CHARACTERISTICS_DATA";
			$sql = "Select CharData From $RC_PERSONAL_CHARACTERISTICS_DATA";
			$CharDataInfoArr = $this->returnResultSet($sql);
			$numOfData = count($CharDataInfoArr);
			
			for ($i=0; $i<$numOfData; $i++) {
				$thisCharData = $CharDataInfoArr[$i];
				
				$thisCharDataArr = explode(',', $thisCharData);
				$thisNumOfChar = count($thisCharDataArr);
				for ($j=0; $j<$thisNumOfChar; $j++) {
					$thisCharItemData = $thisCharDataArr[$j];
					$thisCharItemDataArr = explode(':', $thisCharItemData);
					
					$thisScaleID = $thisCharItemDataArr[1];
					
					if (in_array($thisScaleID, (array)$ScaleIDArr)) {
						return true;
					}
				}
			}
			
			return false;
		}
		
		public function Get_User_Applicable_Personal_Characteristics_Scale($ParUserID) {
			$PreloadArrKey = 'Get_User_Applicable_Personal_Characteristics_Scale';
			$FuncArgArr = get_defined_vars();
			$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    	if ($returnArr !== false) {
	    		return $returnArr;
	    	}
	    	
	    	$lreportcardCust = new libreportcardcustom();
			$IsAdminUser = $this->IS_ADMIN_USER($ParUserID);
			
			$ScaleInfoArr = $this->Get_Personal_Characteristics_Scale();
			$numOfScale = count($ScaleInfoArr);
			
			$ScaleMemberInfoArr = $this->Get_Personal_Characteristics_Scale_Mgmt_Group_Member();
			$ScaleMemberAssoArr = BuildMultiKeyAssoc($ScaleMemberInfoArr, array('ScaleID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			
			$ApplicableScaleIDArr = array();
			$Counter = 0;
			for ($i=0; $i<$numOfScale; $i++) {
				$thisScaleID = $ScaleInfoArr[$i]['ScaleID'];
				$thisScaleCode = $ScaleInfoArr[$i]['Code'];
				
				if (!$IsAdminUser && in_array($thisScaleCode, (array)$lreportcardCust->PersonalCharAdminOnlyGradeArr)) {
					continue;
				}
				
				$thisScaleMemberInfoArr = $ScaleMemberAssoArr[$thisScaleID];
				$thisNumOfMember = count($thisScaleMemberInfoArr);
				
				//if ($thisNumOfMember==0 && $IsAdminUser) {	// no member => admin only
				if ($thisNumOfMember==0) {						// no member => all users can select this scale
					$ApplicableScaleIDArr[$Counter]['ScaleID'] = $thisScaleID;
					$ApplicableScaleIDArr[$Counter++]['Code'] = $thisScaleCode;
				}
				else {											// have member => if admin not selected also no right to input this grade
					$thisMemberUserIDArr = Get_Array_By_Key($thisScaleMemberInfoArr, 'UserID');
					if (in_array($ParUserID, (array)$thisMemberUserIDArr)) {
						$ApplicableScaleIDArr[$Counter]['ScaleID'] = $thisScaleID;
						$ApplicableScaleIDArr[$Counter++]['Code'] = $thisScaleCode;
					}
				}
			}
			
			$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ApplicableScaleIDArr);
			return $ApplicableScaleIDArr;
		}
	
	}
	
}
?>