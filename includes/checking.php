<?php

# modifying by: Jason

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

@session_start();
$UserLastAccessTime = $_SESSION['UserLastAccessTime'];

$NOT_KEEP_ALIVE = true;

include_once("global.php");
include_once("libdb.php");


$client_ip = getenv("REMOTE_ADDR");
if (trim($client_ip)=="")
{
	$client_ip = $_SERVER["REMOTE_ADDR"];
}




intranet_opendb();


$libdb = new libdb();

function GetGreatestDate($arr_schema)
{
	$last_date = "2000-01-01";
	for ($i=0; $i<sizeof($arr_schema); $i++)
	{
		if (strtotime($arr_schema[$i][0])>strtotime($last_date))
		{
			$last_date = $arr_schema[$i][0];
		}
	}
	return $last_date;
}


function is_count_file($filename)
{
	$ext_allowed = array("PHP", "JS", "TPL");
	$filename = strtoupper($filename);
	$filetmp = explode(".", $filename);
	$file_extension = $filetmp[sizeof($filetmp)-1];
	return in_array($file_extension, $ext_allowed);
}

function get_modified_by($file_path)
{
	$colleague = "&nbsp;";
	
	$file_contents = get_file_content($file_path);
	
	$fc_arr = explode("modifying by", $file_contents);
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("Modifying by", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("modifying:", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("Modifying:", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("modifying :", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("Modifying :", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("using by", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("Using by", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("using :", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("using:", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("editing by", $file_contents);
	}
	if (sizeof($fc_arr)<2)
	{
		$fc_arr = explode("Editing by", $file_contents);
	}
	
	if (sizeof($fc_arr)>1)
	{
		$fc_arr2 = explode("\n", $fc_arr[1]);
		$colleague = trim(str_replace(":","",$fc_arr2[0]));
	}
	
	return $colleague;
}

##########################################################################################
########################### reserved for development team's use ##########################
##########################################################################################

if (file_exists("../file/school_data.txt"))
{
	$school_data = explode("\n",get_file_content("../file/school_data.txt"));
	$school_name = $school_data[0];
	$data_collected[] = array("School name", $school_name);
}

# ip 2.5 version number
if (file_exists("version.php"))
{
	$JustWantVersionData = true;
	include_once("version.php");
	$ip_src_code_version = $versions[0][0];
	$data_collected[] = array("Version number", $ip_src_code_version);
}

$server_time = date("Y-m-d H:i:s e");
$data_collected[] = array("Server Time", $server_time);


$time_out_durction = ini_get("session.gc_maxlifetime") . " (".ini_get("session.gc_maxlifetime")/60 . " mins)";
$data_collected[] = array("System Timeout duration", $time_out_durction);


# Intranet addon schema update 
# load history file of Intranet
$history_file = "../file/db_update_history.txt";
$history_content = trim(get_file_content($history_file));
if ($history_content!="")
{
	# find if schema should be updated
	$tmp_arr = explode("\n", $history_content);
	list($last_schema_date, $last_update, $last_update_ip, $run_on_src_version, $schema_update_type, $data_patch_type, $status_final, $complete_time) = explode(",", $tmp_arr[sizeof($tmp_arr)-1]);
	$last_schema_date_updated = $last_schema_date;
	
	
	$is_new_available = false;
	if ($run_on_src_version!="" && $status_final!="")
	{
		if ($status_final=="[STARTED]")
		{
			$is_new_available = true;
		}
	}	
	if (!$is_new_available)
	{	
		include_once('../addon/ip25/sql_table_update.php');	
		if (strtotime($sql_eClass_update[sizeof($sql_eClass_update)-1][0])>strtotime($last_schema_date))
		{
			$is_new_available = true;
			$new_schema_date = $sql_eClass_update[sizeof($sql_eClass_update)-1][0];
		}
		if (strtotime($sql_eClassIP_update[sizeof($sql_eClassIP_update)-1][0])>strtotime($last_schema_date))
		{
			$is_new_available = true;
			$new_schema_date = $sql_eClassIP_update[sizeof($sql_eClassIP_update)-1][0];
		}
	}
	
	$IntranetAddonSchemaStatus = ($is_new_available) ? "intranet_not_updated" : "intranet_updated_completed";
	$update_includes = ($schema_update_type!="") ? "\n".(($status_final=="[STARTED]")?"<font color='red'>NOT completed</font>" : "Completed")." and involved: $schema_update_type, $data_patch_type" : "";
	$last_update_done = ($complete_time!="") ? $complete_time : $last_update;	
	$new_update = ($is_new_available) ? "<font color='red'>Yes ($new_schema_date)</font>" : "No";
	$data_collected[] = array("IP 2.5 DB schema last update", $last_update_done.$update_includes);
	$src_code_used = ($run_on_src_version!="") ? "\nsource code used: ".(($run_on_src_version!=$ip_src_code_version)?"<font color='red'>$run_on_src_version</font>":$run_on_src_version) : "";
	$data_collected[] = array("IP 2.5 lastest DB schema updated", $last_schema_date_updated.$src_code_used);
	$data_collected[] = array("IP 2.5 New schema available", $new_update);
	
	$history_content_ip = $history_content;
}


# eClass addon schema update 
# load history file of eClass
$history_file = "../../eclass40/files/db_update_history.txt";
$history_content = trim(get_file_content($history_file));
if ($history_content!="")
{
	# find if schema should be updated
	$tmp_arr = explode("\n", $history_content);
	list($last_schema_date, $last_update, $last_update_ip, $run_on_src_version, $schema_update_type, $data_patch_type, $status_final, $complete_time) = explode(",", $tmp_arr[sizeof($tmp_arr)-1]);
	$last_schema_date_updated = $last_schema_date;
	
	# assume addon schema must be executed for each roll out
	$is_new_available = false;
	if ($run_on_src_version!="" && $status_final!="")
	{
		if ($status_final=="[STARTED]")
		{
			$is_new_available = true;
		}
	}	

	if (!$is_new_available)
	{
		unset($sql_eClass_update);
		unset($sql_course_update);
		# find if schema should be updated
		include_once('../../eclass40/addon/schema/sql_table_update.php');
		
		$main_db_last_date = GetGreatestDate($sql_eClass_update);
		$course_db_last_date = GetGreatestDate($sql_course_update);
		if (strtotime($main_db_last_date)>strtotime($last_schema_date)
			|| strtotime($course_db_last_date)>strtotime($last_schema_date))
		{
			$is_new_available = true;
			$last_schema_date = (strtotime($main_db_last_date)>strtotime($course_db_last_date)) ? $main_db_last_date:$course_db_last_date;
		}
	
		# also find the special rooms
	
		# find the special room types
		$sql = "SELECT DISTINCT RoomType FROM ".$eclass_db.".course ";
		$row_roomtype = $libdb->returnVector($sql);
		for ($i=0; $i<sizeof($row_roomtype); $i++)
		{
			$type_arr = ${"sql_course_sr".$row_roomtype[$i]};
			
			$special_db_last_date = GetGreatestDate($type_arr);
			if (is_array($type_arr) && strtotime($special_db_last_date)>strtotime($last_schema_date))
			{
				$is_new_available = true;
				$last_schema_date = $special_db_last_date;
			}
		}
	}
	$eClassCourseAddonSchemaStatus = ($is_new_available) ? "eclass_not_updated" : "eclas_updated_completed";
	$new_update = ($is_new_available) ? "<font color='red'>Yes ($last_schema_date)</font>" : "No";
	$update_includes = ($schema_update_type!="") ? "\n".(($status_final=="[STARTED]")?"<font color='red'>NOT completed</font>" : "Completed")." and involved: $schema_update_type, $data_patch_type" : "";
	$last_update_done = ($complete_time!="") ? $complete_time : $last_update;
	$data_collected[] = array("eClass 4.0+ DB schema last update", $last_update_done.$update_includes);
	$src_code_used = ($run_on_src_version!="") ? "\nsource code used: ".(($run_on_src_version!=$ip_src_code_version)?"<font color='red'>$run_on_src_version</font>":$run_on_src_version) : "";
	$data_collected[] = array("eClass 4.0+ lastest DB schema updated", $last_schema_date_updated.$src_code_used);
	
	$data_collected[] = array("eClass 4.0+ New schema available", $new_update);
	
	$history_content_ec = $history_content;
}

# version numbers
$data_collected[] = array("Intranet version", $intranet_version);
if (isset($iportfolio_version))
	$data_collected[] = array("iPortfolio version", $iportfolio_version);
if ($plugin['attendancestaff'])
	$data_collected[] = array("Staff Attendance version", $module_version['StaffAttendance']);

$sql = "SELECT DISTINCT Module FROM ECLASS_MODULE_VERSION_LOG";
$module = $libdb->returnVector($sql);

# get the module version from DB (new mechanism since Feb 2011)
for($i=0; $i<sizeof($module); $i++)
{
	$version = getEclassModuleVersionInUse($module[0]);
	$_SESSION['VERSION'][$module[0]] = $version;
	$data_collected[] = array($module[0]." version", $version);
}

$data_collected[] = array("intranet_default_lang", $intranet_default_lang);




# get the region (since ip.2.5.2.3.1)
if (function_exists("get_client_region"))
{
	$client_region = get_client_region();
	$data_collected[] = array("Region of client", $client_region);
}

$data_collected[] = array("expiry", $expiry);

$data_collected[] = array("g_encoding_unicode", $g_encoding_unicode);

$data_collected[] = array("UserLastAccessTime", date("H:i:s",$UserLastAccessTime));



# PHP Configs



if (function_exists("mysql_get_server_info"))
	$data_collected[] = array("MySQL version", mysql_get_server_info());
$data_collected[] = array("SERVER_SOFTWARE", $_SERVER['SERVER_SOFTWARE']);
$data_collected[] = array("PHP version", phpversion());
$data_collected[] = array("SERVER HD free space", ConvertBytes(disk_free_space("/")));
$session__free_space = disk_free_space(session_save_path());
$data_collected[] = array("Session save path (".session_save_path().") free space", ConvertBytes($session__free_space));
$mb_string_enabled = function_exists("mb_substr");
$data_collected[] = array("mb_string", $mb_string_enabled);

$max_upload = (int)(ini_get('upload_max_filesize'));
$max_post = (int)(ini_get('post_max_size'));
$memory_limit = (int)(ini_get('memory_limit'));
$data_collected[] = array("upload_max_filesize", $max_upload . "MB");
$data_collected[] = array("post_max_size", $max_post . "MB");
$data_collected[] = array("memory_limit", $memory_limit . "MB");
$upload_mb = min($max_upload, $max_post, $memory_limit);
$data_collected[] = array("upload limit (conclusion)", $upload_mb . "MB");




$hwinfo=ep_hwinfo($intranet_root);

# HARDWARE
if (sizeof($hwinfo)>0)
{
	foreach ($hwinfo as $plugin_title =>$plugin_value)
	{	
		if (is_array($plugin_value) && sizeof($plugin_value)>0)
		{
			$value_str = "";
			foreach ($plugin_value as $value_title =>$value_value)
			{
				$value_str .= "['".$value_title."'] = {$value_value}\n";
			}
			$plugin_value = $value_str;
		}	
		$data_collected[] = array("HARDWARE::".$plugin_title, $plugin_value);
	}
}


# PLUGIN
if (sizeof($plugin)>0)
{
	foreach ($plugin as $plugin_title =>$plugin_value)
	{	
		if (is_array($plugin_value) && sizeof($plugin_value)>0)
		{
			$value_str = "";
			foreach ($plugin_value as $value_title =>$value_value)
			{
				$value_str .= "['".$value_title."'] = {$value_value}\n";
			}
			$plugin_value = $value_str;
		}	
		$data_collected[] = array("PLUGIN::".$plugin_title, $plugin_value);
	}
}

# $special_feature
if (sizeof($special_feature)>0)
{
	foreach ($special_feature as $feature_title =>$feature_value)
	{
		if (is_array($feature_value) && sizeof($feature_value)>0)
		{
			$value_str = "";
			foreach ($feature_value as $value_title =>$value_value)
			{
				$value_str .= "['".$value_title."'] = {$value_value}\n";
			}
			$feature_value = $value_str;
		}
		$data_collected[] = array("SPECIAL::".$feature_title, $feature_value);
	}
}

# CUSTOMIZATION
if (sizeof($sys_custom)>0)
{
	foreach ($sys_custom as $custom_title =>$custom_value)
	{
		if (is_array($custom_value) && sizeof($custom_value)>0)
		{
			$value_str = "";
			foreach ($custom_value as $value_title =>$value_value)
			{
				$value_str .= "['".$value_title."'] = {$value_value}\n";
			}
			$custom_value = $value_str;
		}
		$data_collected[] = array("CUSTOM::".$custom_title, $custom_value);
	}
}

# CUSTOMIZATION
if (sizeof($stand_alone)>0)
{
	foreach ($stand_alone as $alone_title =>$alone_value)
	{
		if (is_array($alone_value) && sizeof($alone_value)>0)
		{
			$value_str = "";
			foreach ($alone_value as $value_title =>$value_value)
			{
				$value_str .= "['".$value_title."'] = {$value_value}\n";
			}
			$alone_value = $value_str;
		}
		$data_collected[] = array("STAND-ALONE::".$alone_title, $alone_value);
	}
}

# SMS
$data_collected[] = array("SMS::sms_vendor", $sms_vendor);
$data_collected[] = array("SMS::sms_use_reply_message", $sms_use_reply_message);
$data_collected[] = array("SMS::sms_vendor_for_ReplyMessage", $sms_vendor_for_ReplyMessage);
$data_collected[] = array("SMS::sms_phone_number_checking_scheme", implode(', ', (array)$sms_phone_number_checking_scheme));
$data_collected[] = array("SMS::bypass_phone_validation", $bypass_phone_validation);
$data_collected[] = array("SMS::Trim_First_Zero", $smsConfig['Trim_First_Zero']);
$data_collected[] = array("SMS::sms_".strtolower($sms_vendor)."_simulate_only", ${'sms_'.strtolower($sms_vendor).'_simulate_only'});



# MISC SETTINNGS

$data_collected[] = array("lslp_license", $lslp_license);
$data_collected[] = array("lslp_license_period", $lslp_license_period);
$data_collected[] = array("lslp_httppath", $lslp_httppath);
$data_collected[] = array("config_school_code", $config_school_code);
$data_collected[] = array("special_announce_public_allowed", $special_announce_public_allowed);
$data_collected[] = array("bl_sms_version", $bl_sms_version);
$data_collected[] = array("config_school_type", $config_school_type);

$data_collected[] = array("ReportCardTemplate", $ReportCardTemplate);
$data_collected[] = array("ReportCardConfirmDisabled", $ReportCardConfirmDisabled);
$data_collected[] = array("ReportCardCustomSchoolName", $ReportCardCustomSchoolName);

$data_collected[] = array("tts_server_url", $tts_server_url);
$data_collected[] = array("tts_user", $tts_user);

$data_collected[] = array("ex_api['infoapi']", $ex_api['infoapi']);
$data_collected[] = array("external_path['WV']", $external_path['WV']);



############# OUTPUT OR DISPLAY

if (!isset($output_format))
{
	echo "Your IP: ".$client_ip."<br />\n";
	
	exec ("find ../ -user root", $ResultStringIP);
	if (is_array($ResultStringIP) && sizeof($ResultStringIP)>0)
	{
		$IsSourceCodeByRoot = true;
	} else
	{
		exec ("find ../../eclass40/ -user root", $ResultStringEC40);
		if (is_array($ResultStringEC40) && sizeof($ResultStringEC40)>0)
		{
			$IsSourceCodeByRoot = true;
		} else
		{
			exec ("find ../../eclass30/ -user root", $ResultStringEC30);
			if (is_array($ResultStringEC40) && sizeof($ResultStringEC40)>0)
			{
				$IsSourceCodeByRoot = true;
			}
		}
	}
	
	# default print to browser
	echo "<table width='900' border='0' cellpadding='5' cellspacing='0' style='font-family:arial;'>";
	echo "<tr><td colspan='2' style='border-bottom:2px D2C8C1 dotted;'>&nbsp;</td></tr>\n";
	
	# display additional inform on request
	echo "<tr><td colspan='2' bgcolor='#F2E8E1'><font color='#D2C8C1'>&gt;</front> <a href='checking.php?output_format=all_schema_logs' target='DB_SCHEMA_LOG'> View DB schema update logs</a></td></tr>\n";
	echo "<tr><td colspan='2' bgcolor='#F2E8E1'><font color='#D2C8C1'>&gt;</front> <a href='checking.php?output_format=is_addon_schema_latest' target='DB_SCHEMA_LOG'> Are schema all updated?</a></td></tr>\n";
	echo "<tr><td colspan='2' bgcolor='#F2E8E1'><font color='#D2C8C1'>&gt;</front> <a href='../addon/ip25/' target='intranet_addon_schema'> Run IP 2.5 (Intranet) addon schema patch</a></td></tr>\n";
	echo "<tr><td colspan='2' bgcolor='#F2E8E1'><font color='#D2C8C1'>&gt;</front> <a href='http://".$eclass40_httppath."/addon/schema/' target='ec_addon_schema'> Run eClass 4.0+ addon schema patch</a></td></tr>\n";
	echo "<tr><td colspan='2' bgcolor='#F2E8E1'><font color='#D2C8C1'>&gt;</front> <a href='checking.php?output_format=check_high_usage' target='usage'> View usage of email and login</a></td></tr>\n";
	echo "<tr><td colspan='2' bgcolor='#F2E8E1'><font color='#D2C8C1'>&gt;</front> <a href='checking.php?output_format=php_information_dev' target='PHPInfo'> View PHP information</a></td></tr>\n";
	echo "<tr><td colspan='2' bgcolor='#F2E8E1'><font color='#D2C8C1'>&gt;</front> <a href='checking.php?output_format=file_modified&last=10' target='FileModified'> View latest files modified</a></td></tr>\n";
	echo "<tr><td colspan='2' bgcolor='#F2E8E1'><font color='#D2C8C1'>&gt;</front> <a href='checking.php?output_format=active_usage' target='ActiveUsage'> View active usage within 40 mins</a></td></tr>\n";
	
	echo "<tr><td colspan='2' style='border-top:2px #D2C8C1 dotted;'>&nbsp;</td></tr>\n";

	echo "<tr><td style='border-bottom:1px solid #559955' nowrap='nowrap' bgcolor='#E0F0D3'>Source code ownership :</td>" .
				"<td style='border-bottom:1px solid #559955' width='700'>".($IsSourceCodeByRoot?"<font color='red'>Some files are owned by root account! </font><a href='checking.php?output_format=source_code_root' target='SrcCodeRoot'>[ View ]</a>":"No problem found.")."</td></tr>\n";

	for ($i=0; $i<sizeof($data_collected); $i++)
	{
		list($display_title, $display_value) = $data_collected[$i];
		if (strstr($display_title, "PLUGIN"))
		{
			$display_title = "<font color='blue'>".$display_title."</font>";
		} elseif (strstr($display_title, "SPECIAL"))
		{
			$display_title = "<font color='green'>".$display_title."</font>";
		} elseif (strstr($display_title, "CUSTOM"))
		{
			$display_title = "<font color='purple'>".$display_title."</font>";
		} elseif (strstr($display_title, "STAND-ALONE"))
		{
			$display_title = "<font color='red'>".$display_title."</font>";
		} elseif (strstr($display_title, "Session save path") && $session__free_space<1024*1024*1024)
		{
			$display_value = "<font color='red'>".$display_value."</font>";
		} elseif (strstr($display_title, "HARDWARE"))
		{
			$display_title = "<font color='purple'>".$display_title."</font>";
		}
		elseif (strstr($display_title, "SMS"))
		{
			$display_title = "<font color='SlateBlue'>".$display_title."</font>";
		}
		
		if (trim($display_value)=="")
		{
			$display_value = "&nbsp;";
		}
		echo "<tr><td style='border-bottom:1px solid #559955' nowrap='nowrap' bgcolor='#E0F0D3'>".$display_title." :</td>" .
				"<td style='border-bottom:1px solid #559955' width='700'>".nl2br($display_value)."</td></tr>\n";
	}
	echo "</table>";
	echo "<p><font color='orange'>p.s. 1 for true; '' for false!</font></p>";
} elseif ($output_format=="serial")
{
	echo serialize($data_collected);
} elseif ($output_format=="source_code_root")
{
	
	function OutputRootFile($resultString, $docFolder)
	{
		$count_i = 0;
		if (sizeof($resultString)>0)
		{
			echo "<tr><td nowrap='nowrap' colspan='2' style='border-bottom:1px #BBBB77 solid;' bgcolor='#EEEEFF'><font face='arial' size='5'><b>{$docFolder}/</b></font></td></tr>\n";
			
			# echo intranetIP files
			for ($i=0; $i<sizeof($resultString); $i++)
			{
				$count_i ++;
				$file_now = $resultString[$i];
				$bg_color = ($count_i%2) ? "#FFFFF6" : "#FFFFFF";
				echo "<tr><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #BBBB77 solid;'>{$count_i}. &nbsp; &nbsp;</td><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #BBBB77 solid;'>".str_replace("../", "", $file_now)." &nbsp; &nbsp;</td></tr>\n";
			}
		}
	}
	
	echo "<table border='0' cellpadding='2' cellspacing='0'>";
	
	echo "<tr><td nowrap='nowrap' colspan='2' style='border-bottom:1px #BBBB77 solid;' bgcolor='#EEEEFF' align='center'><font face='arial' size='5' color='red'><b>Some files are owned by root account!</b> <br />ATTENTION!!! They will cause eClass Update failed to work!</font></td></tr>\n";
	
	# intranetIP
	exec ("find ../ -user root -printf \"%TY-%Tm-%Td %TH:%TM :::: %p\n\" | sort -n", $resultStringIP);
	OutputRootFile($resultStringIP, "intranetIP");
	
	# eclass40
	exec ("find ../../eclass40 -user root -printf \"%TY-%Tm-%Td %TH:%TM :::: %p\n\" | sort -n", $resultStringEC40);
	OutputRootFile($resultStringEC40, "eclass40");
	
	# eclass30
	exec ("find ../../eclass30 -user root -printf \"%TY-%Tm-%Td %TH:%TM :::: %p\n\" | sort -n", $resultStringEC30);
	OutputRootFile($resultStringEC30, "eclass30");
	
	
	echo "</table>";
} elseif ($output_format=="all_schema_logs")
{
	echo "<table width='100%' border='0' cellpadding='10' cellspacing='0'>";
	echo "<tr><td colspan='2'><u>DATABASE SCHEMA UPDATE LOG</u><br />Columns (comma separated): (1) date of last schema array, (2) start time, (3) IP address of user, (4) source code version, (5) schema scope, (6) data patch, (7) status, (8) complete time </td></tr>\n";
	echo "<tr><td width='50%' bgcolor='#9999FF'><b>IP 2.5</b></td><td width='50%' bgcolor='#99FF99'><b>eClass 4.0+</b></td></tr>\n";
	echo "<tr><td width='50%' valign='top' bgcolor='#E1E1FF'><font face='arial'>".nl2br(str_replace("[STARTED]", "<font color='red'>[STARTED]</font>", $history_content_ip))."</font></td><td width='50%' valign='top' bgcolor='#E5FFE5'><font face='arial'>".nl2br((str_replace("[STARTED]", "<font color='red'>[STARTED]</font>", $history_content_ec)))."</font></td></tr>\n";
	echo "</table>";
} elseif ($output_format=="is_addon_schema_latest")
{
	# up to the latest
	# intranet done, eclass done
	echo "$IntranetAddonSchemaStatus,$eClassCourseAddonSchemaStatus";
	
} elseif ($output_format=="check_high_usage")
{
	unset($data_collected);
	$data_collected = array();
	
	# mail usage (DB)
	$sql = "SELECT count(*) FROM INTRANET_CAMPUSMAIL";
	$result = $libdb->returnVector($sql);
	$totalmails = $result[0];
	$data_collected[] =  array("Total # of mails stored in DB", (($totalmails>300000) ? "<font color='red'>".number_format($totalmails)."</font>": number_format($totalmails)));
	
	
	# login records kept
	$sql = "SELECT count(*) FROM INTRANET_LOGIN_SESSION";
	$result = $libdb->returnVector($sql);
	$totallogins = $result[0];
	$data_collected[] =  array("Total # of login records", (($totallogins>300000) ? "<font color='red'>".number_format($totallogins)."</font>": number_format($totallogins)));


	# default print to browser
	echo "<table width='900' border='0' cellpadding='5' cellspacing='0' style='font-family:arial;'>";
	echo "<tr><td colspan='2' style='border-top:2px #D2C8C1 dotted;'><b>Usage</b></td></tr>\n";

	for ($i=0; $i<sizeof($data_collected); $i++)
	{
		list($display_title, $display_value) = $data_collected[$i];
		if (trim($display_value)=="")
		{
			$display_value = "&nbsp;";
		}
		echo "<tr><td style='border-bottom:1px solid #559955' nowrap='nowrap' bgcolor='#E0F0D3'>".$display_title." :</td>" .
				"<td style='border-bottom:1px solid #559955' width='700'>".nl2br($display_value)."</td></tr>\n";
	}
	echo "</table>";
}elseif ($output_format=="php_information_dev")
{
	phpinfo();
}elseif ($output_format=="file_modified")
{
	echo "<u><font color='Orange'>Page loads at ".date("H:i")."</font></u>";
	
	include_once("../includes/libfilesystem.php");

	$fs = new libfilesystem();
	
	$fs->set_get_filetime(true);
	$filesarr = $fs->return_folderlist("../addon");
	$filesarr = $fs->return_folderlist("../admin");
	$filesarr = $fs->return_folderlist("../api");
	$filesarr = $fs->return_folderlist("../cardapi");
	$filesarr = $fs->return_folderlist("../home");
	$filesarr = $fs->return_folderlist("../includes");
	$filesarr = $fs->return_folderlist("../lang");
	$filesarr = $fs->return_folderlist("../plugins");
	$filesarr = $fs->return_folderlist("../rifolder");
	$filesarr = $fs->return_folderlist("../templates");
	$filesarr = $fs->return_folderlist("../tools");
	$filesarr = $fs->return_folderlist("../webserviceapi");
	$fs->set_subfolder_recursive(true);
	$filesarr = $fs->return_folderlist("..");
	
	$filestimearr = $fs->return_filetime();
	
	arsort($filestimearr);
	
	if ($last=="")
	{
		$last = 50;
	}
	
	$count_i = 0;
	$count_old_i = 0;
	
	echo "<h1>/intranetIP</h1>\n";
	echo "<font face='Verdana'>".number_format(sizeof($filesarr))." files are found! The latest modified source codes are: </font><br />";
	
	
	if (file_exists("version.php"))
	{
		$file_version_datetime = filemtime("version.php");
		//$deploy_ver_time = date("Y-m-d H:i:s",$file_version_datetime+300);
		$deploy_ver_time = $file_version_datetime+300;
	} else
	{
		debug("version.php cannot bet found!!!");
	}
	
	echo "<table border='0' cellpadding='2' cellspacing='0'>";
	foreach ($filestimearr as $key => $val)
	{
		if (!is_count_file($key))
		{
			continue;
		}
	    $count_i ++;

	    $is_today = (date("Y-m-d", $val)==date("Y-m-d"));
	    $style_today = ($is_today) ? "red" : "";
	    if (!$is_today)
	    {
	    	if ($val>$deploy_ver_time)
	    	{
	    		$style_today = "blue";
	    	} else
	    	{
	    		$style_today = "black";
	    		$count_old_i ++;
	    	}
	    }
	    if ($is_today && ($server_ip=="192.168.0.146" || $server_ip=="192.168.0.149"))
	    {
	    	$colleague = get_modified_by($key);
	    } else
	    {
	    	$colleague = "&nbsp;";
	    }
	    $file_now = str_replace("../", "intranetIP/", $key);
	    $bg_color = ($count_i%2) ? "#FFFFF6" : "#FFFFFF";
	    echo "<tr><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #BBBB77 solid;'><font color='{$style_today}'>{$count_i}</font>. &nbsp; &nbsp;</td><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #BBBB77 solid;'><font color='{$style_today}'>".date("Y-m-d H:i:s", $val)."</font> &nbsp; &nbsp;</td><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #BBBB77 solid;'><font color='{$style_today}'>{$file_now}</font> &nbsp; &nbsp;</td><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #BBBB77 solid;'><font color='{$style_today}'>{$colleague}</font></td></tr>\n";
	    if ($count_old_i>=$last || $count_i>=500)
	    {
	    	break;
	    }
	}
	echo "</table>";

	
	
	$fs->set_subfolder_recursive(false);
	$filesarr = $fs->return_folderlist("../../eclass40/addon", true);
	$filesarr = $fs->return_folderlist("../../eclass40/src");
	$filesarr = $fs->return_folderlist("../../eclass40/system");
	$filesarr = $fs->return_folderlist("../../eclass40/css");
	$filesarr = $fs->return_folderlist("../../eclass40/js");	
	$fs->set_subfolder_recursive(true);
	$filesarr = $fs->return_folderlist("../../eclass40");
	
	$filestimearr = $fs->return_filetime();
	
	arsort($filestimearr);
	
	if ($last=="")
	{
		$last = 50;
	}
	
	$count_i = 0;
	$count_old_i = 0;
	
	
	echo "<h1>/eclass40</h1>\n";
	echo "<font face='Verdana'>".number_format(sizeof($filesarr))." files are found! The latest modified source codes are: </font><br />";
	
	echo "<table border='0' cellpadding='2' cellspacing='0'>";
	//$deploy_ver_time = strtotime($versions[0][1].":00");
	
	foreach ($filestimearr as $key => $val)
	{
		if (!is_count_file($key))
		{
			continue;
		}
	    $count_i ++;
	    $colleague = "&nbsp;";
	    $is_today = (date("Y-m-d", $val)==date("Y-m-d"));
	    $style_today = ($is_today) ? "red" : "";
	     if (!$is_today)
	    {
	    	if ($val>$deploy_ver_time)
	    	{
	    		$style_today = "blue";
	    	} else
	    	{
	    		$style_today = "black";
	    		$count_old_i ++;
	    	}
	    }    
	    if ($is_today && ($server_ip=="192.168.0.146" || $server_ip=="192.168.0.149"))
	    {
	    	$colleague = get_modified_by($key);
	    } else
	    {
	    	$colleague = "&nbsp;";
	    }
	    $file_now = str_replace("../../", "", $key);
	    $bg_color = ($count_i%2) ? "#FFFCFF" : "#FFFFFF";
	    echo "<tr><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #CC88CC solid;'><font color='{$style_today}'>{$count_i}</font>.&nbsp; &nbsp; &nbsp; </td><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #CC88CC solid;'><font color='{$style_today}'>".date("Y-m-d H:i:s", $val)."</font> &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #CC88CC solid;'><font color='{$style_today}'>{$file_now}</font></td><td nowrap='nowrap' bgcolor='{$bg_color}' style='border-bottom:1px #CC88CC solid;'><font color='{$style_today}'>{$colleague}</font></td></tr>\n";
	    if ($count_old_i>=$last || $count_i>=500)
	    {
	    	break;
	    }
	}
	echo "</table>";
	echo "<hr /><h1>Legend</h1>\n";
	echo "<p><font color='red'>* Files in red are the files mofidied today;</font></p>"; 
	echo "<p><font color='blue'>* Files in blue are the files modified after the deployment (regarding to version);</font></p>"; 
	echo "<p><font color='black'>* Files in black are the files modified before the deployment.</font></p>";
}elseif ($output_format=="active_usage")
{
	echo "<u><font color='Orange'>Page loads at ".date("H:i")."</font></u>";
	
	$identity_title[1] = "<font color='red'>Staff</font>";
	$identity_title[2] = "<font color='blue'>Student</font>";
	$identity_title[3] = "<font color='green'>Parent</font>";
	$identity_title[4] = "Alumni";
	
	
	$ec_identity_title["T"] = "<font color='red'>Teacher</font>";
	$ec_identity_title["S"] = "<font color='blue'>Student</font>";
	$ec_identity_title["A"] = "<font color='orange'>Assistant</font>";


	$time_within = 2400; // 40 mins
	$sql = "select UserID, count(*) from INTRANET_LOGIN_SESSION where unix_timestamp(DateModified)>unix_timestamp(now())-{$time_within} GROUP BY UserID";
	$recent_use_intranet = sizeof($libdb->returnVector($sql));
	echo "<p><font face='arial'>There are {$recent_use_intranet} user(s) using Intranet recently (within 40mins).</font></p>";
	
	if ($recent_use_intranet>0)
	{
		$sql = "select ils.StartTime, ils.DateModified, iu.EnglishName, iu.ChineseName, iu.UserLogin, iu.RecordType from INTRANET_LOGIN_SESSION AS ils, INTRANET_USER AS iu where unix_timestamp(ils.DateModified)>unix_timestamp(now())-2400 AND iu.UserID=ils.userid group by ils.userid ORDER BY ils.DateModified DESC limit 200";
		$intranet_usages = $libdb->returnArray($sql);
		
		echo "<table border='0' cellpadding='0' cellspacing='0'>";
		echo "<tr><td nowrap='nowrap'>#</td><td nowrap='nowrap'>Login Time &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>Last Use</td><td nowrap='nowrap'>Name</td><td nowrap='nowrap'>LoginID</td><td nowrap='nowrap'>User Type</td></tr>\n";
		echo "<tr><td colspan='6' style='border-top:1px solid #888888;'><img src='/images/space.gif' border='0' height='1' width='1' /></td></tr>\n";
		for ($i=0; $i<sizeof($intranet_usages); $i++)
		{
			$userobj = $intranet_usages[$i];
			$UserType = $identity_title[$userobj["RecordType"]];
			echo "<tr><td nowrap='nowrap'>".($i+1).". &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$userobj["StartTime"]." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$userobj["DateModified"]." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$userobj["ChineseName"]." ".$userobj["EnglishName"]." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$userobj["UserLogin"]." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$UserType."</td></tr>\n";
		}
		echo "</table>";
	}

	$sql = "select user_id, count(*) from ".$eclass_db.".user_course where unix_timestamp(lastused)>unix_timestamp(now())-{$time_within} GROUP BY course_id, user_id ";
	$recent_use_eclass = sizeof($libdb->returnVector($sql));
	echo "<br /><p><font face='arial'>There are {$recent_use_eclass} user(s) using eClass recently (within 40mins).</font></p>";
	
	if ($recent_use_eclass>0)
	{
		$sql = "select uc.lastused, uc.chinesename, uc.firstname, uc.user_email, uc.memberType, c.course_name from ".$eclass_db.".user_course AS uc, ".$eclass_db.".course AS c where unix_timestamp(uc.lastused)>unix_timestamp(now())-{$time_within}  AND uc.course_id=c.course_id GROUP BY uc.course_id, uc.user_id order by uc.lastused desc limit 200";
		$eclass_usages = $libdb->returnArray($sql);
		
		echo "<table border='0' cellpadding='0' cellspacing='0'>";
		echo "<tr><td nowrap='nowrap'>#</td><td nowrap='nowrap'>Login Time &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>Name</td><td nowrap='nowrap'>Email</td><td nowrap='nowrap'>User Type &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>Course</td></tr>\n";
		echo "<tr><td colspan='6' style='border-top:1px solid #888888;'><img src='/images/space.gif' border='0' height='1' width='1' /></td></tr>\n";
		for ($i=0; $i<sizeof($eclass_usages); $i++)
		{
			$userobj = $eclass_usages[$i];
			$UserType = $ec_identity_title[$userobj["memberType"]];
			echo "<tr><td nowrap='nowrap'>".($i+1).". &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$userobj["lastused"]." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$userobj["chinesename"]." ".$userobj["firstname"]." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$userobj["user_email"]." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$UserType." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'>".$userobj["course_name"]." &nbsp; &nbsp; &nbsp;</td></tr>\n";
		}
		echo "</table>";
	}
	

}

intranet_closedb();



?>