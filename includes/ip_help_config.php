<?php
// using yat
# define types of online help
$OnlineHType["news"]["eng"] = "News";
$OnlineHType["news"]["big5"] = "更新";
$OnlineHType["news"]["gb"] = "更新";

$OnlineHType["basics"]["eng"] = "Basics";
$OnlineHType["basics"]["big5"] = "基礎知識";
$OnlineHType["basics"]["gb"] = "基礎知識";

$OnlineHType["howto"]["eng"] = "How-to";
$OnlineHType["howto"]["big5"] = "執行工作";
$OnlineHType["howto"]["gb"] = "執行工作";

$OnlineHType["faqtips"]["eng"] = "FAQs/Tips";
$OnlineHType["faqtips"]["big5"] = "常見問題/提示";
$OnlineHType["faqtips"]["gb"] = "常見問題/提示";

#####ENGLISH#####
## Home page
$OnlineH["home"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[eClass 4.0 Home Page Basics]]";

##Assessment > Assessment List
$OnlineH["assessment"]["assessment_list"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/##[[Assessment Management Basics]]";
$OnlineH["assessment"]["assessment_list"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/##[[Assessment Management How-to]]";
$OnlineH["assessment"]["assessment_list"]["faqtips"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Management%20FAQs%2FTips]]";

##Assessment > New Assessment
$OnlineH["assessment"]["new_assessment"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Creation%20Basics]]";
$OnlineH["assessment"]["new_assessment"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Creation%20How-to]]";
$OnlineH["assessment"]["new_assessment"]["faqtips"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Creation%20FAQs%2FTips]]";

##Assessment > Assessment Details
$OnlineH["assessment"]["assessment_details"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Marking%20Basics]]";
$OnlineH["assessment"]["assessment_details"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Marking%20How-to]]";

##eContent Home Page
$OnlineH["econtent"]["econtent_home"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Electronic%20Content%20Basics]]";
$OnlineH["econtent"]["econtent_home"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Electronic%20Content%20How-to]]";

##eContent > New Page
$OnlineH["econtent"]["new_page"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Content%20Creation%20Basics]]";
$OnlineH["econtent"]["new_page"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Content%20Creation%20How-to]]";

##Resources > eClass Files
$OnlineH["resources"]["files_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["files_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Resources > PR
$OnlineH["resources"]["publicres_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["resources"]["publicres_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Resources > QBank
$OnlineH["resources"]["qbank_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["resources"]["qbank_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Resources > Rubrics
$OnlineH["resources"]["rubrics_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["resources"]["rubrics_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Resources > WLinks
$OnlineH["resources"]["weblinks_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["resources"]["weblinks_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Reports > Mark Book
$OnlineH["report"]["markbook_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Mark%20Book%20Basics]]";
$OnlineH["report"]["markbook_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Mark%20Book%20How-to]]";

##Settings > Basic
$OnlineH["settings"]["basic_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["basic_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Using%20Basic%20Settings]]";

##Settings > Access Rights
$OnlineH["settings"]["rights_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["rights_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Controlling%20Access]]";

##Settings > Features
$OnlineH["settings"]["features_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["features_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Changing%20Classroom%20Properties]]";

##Settings > Archive
$OnlineH["settings"]["archive_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["archive_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Using%20Classroom%20Archival%20and%20Restoration]]";

##Settings > Template
$OnlineH["settings"]["template_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["template_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Using%20Word%20Templates]]";

##Settings > House-keeping
$OnlineH["settings"]["housekeep_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";

#####CHINESE BIG5#####
## Home page
$OnlineH["home"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[eClass 4.0 Home Page Basics]]";

##Assessment > Assessment List
$OnlineH["assessment"]["assessment_list"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/##[[Assessment Management Basics]]";
$OnlineH["assessment"]["assessment_list"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/##[[Assessment Management How-to]]";
$OnlineH["assessment"]["assessment_list"]["faqtips"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Management%20FAQs%2FTips]]";

##Assessment > New Assessment
$OnlineH["assessment"]["new_assessment"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Creation%20Basics]]";
$OnlineH["assessment"]["new_assessment"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Creation%20How-to]]";
$OnlineH["assessment"]["new_assessment"]["faqtips"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Creation%20FAQs%2FTips]]";

##Assessment > Assessment Details
$OnlineH["assessment"]["assessment_details"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Marking%20Basics]]";
$OnlineH["assessment"]["assessment_details"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Assessment%20Marking%20How-to]]";

##eContent Home Page
$OnlineH["econtent"]["econtent_home"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Electronic%20Content%20Basics]]";
$OnlineH["econtent"]["econtent_home"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Electronic%20Content%20How-to]]";

##eContent > New Page
$OnlineH["econtent"]["new_page"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Content%20Creation%20Basics]]";
$OnlineH["econtent"]["new_page"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Content%20Creation%20How-to]]";

##Resources > eClass Files
$OnlineH["resources"]["files_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["files_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Resources > PR
$OnlineH["resources"]["publicres_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["resources"]["publicres_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Resources > QBank
$OnlineH["resources"]["qbank_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["resources"]["qbank_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Resources > Rubrics
$OnlineH["resources"]["rubrics_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["resources"]["rubrics_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Resources > WLinks
$OnlineH["resources"]["weblinks_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20Basics]]";
$OnlineH["resources"]["weblinks_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Resources%20How-to]]";

##Reports > Mark Book
$OnlineH["report"]["markbook_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Mark%20Book%20Basics]]";
$OnlineH["report"]["markbook_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Mark%20Book%20How-to]]";

##Settings > Basic
$OnlineH["settings"]["basic_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["basic_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Using%20Basic%20Settings]]";

##Settings > Access Rights
$OnlineH["settings"]["rights_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["rights_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Controlling%20Access]]";

##Settings > Features
$OnlineH["settings"]["features_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["features_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Changing%20Classroom%20Properties]]";

##Settings > Archive
$OnlineH["settings"]["archive_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["archive_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Using%20Classroom%20Archival%20and%20Restoration]]";

##Settings > Template
$OnlineH["settings"]["template_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";
$OnlineH["settings"]["template_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Using%20Word%20Templates]]";

##Settings > House-keeping
$OnlineH["settings"]["housekeep_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/eclass4/#[[Classroom%20Settings%20Basics]]";

# END: define types of online help

# define online help available
# for example
// start of staff attendance v3 helps
##Mgmt > Daily Record
$OnlineH["staff_attendance"]["attendance_record_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Attendance%20Records%20Basics]]";
$OnlineH["staff_attendance"]["attendance_record_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Managing%20Attendance%20Records]]";
$OnlineH["staff_attendance"]["attendance_record_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Attendance%20Records%20Basics]]";
$OnlineH["staff_attendance"]["attendance_record_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Managing%20Attendance%20Records]]";

##Mgmt > OT Record
$OnlineH["staff_attendance"]["ot_record"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Managing%20Overtime%20Work%20Records]]";
$OnlineH["staff_attendance"]["ot_record"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Managing%20Overtime%20Work%20Records]]";

##Mgmt > Leave Record
$OnlineH["staff_attendance"]["leave_record"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Managing%20Leave%20Records]]";
$OnlineH["staff_attendance"]["leave_record"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Managing%20Leave%20Records]]";

##Duty Setup > Regular > Setup Status
$OnlineH["staff_attendance"]["setup_status_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Understanding%20Staff%20Duty]]";
$OnlineH["staff_attendance"]["setup_status_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Up%20Regular%20Duty]]";
$OnlineH["staff_attendance"]["setup_status_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Understanding%20Staff%20Duty]]";
$OnlineH["staff_attendance"]["setup_status_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Up%20Regular%20Duty]]";

##Duty Setup > Regular > Staff Information
$OnlineH["staff_attendance"]["staffinfo_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["staffinfo_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Registering%20Smart%20Card%20IDs]]";
$OnlineH["staff_attendance"]["staffinfo_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["staffinfo_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Registering%20Smart%20Card%20IDs]]";

##Duty Setup > Special > Left tab
$OnlineH["staff_attendance"]["groupduty_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Special%20Duty%20Handling%20Basics]]";
$OnlineH["staff_attendance"]["groupduty_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Up%20Special%20Duty]]";
$OnlineH["staff_attendance"]["groupduty_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Special%20Duty%20Handling%20Basics]]";
$OnlineH["staff_attendance"]["groupduty_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Up%20Special%20Duty]]";

##Duty Setup > Special > Right tab
$OnlineH["staff_attendance"]["schoolduty_tab"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Special%20Duty%20Handling%20Basics]]";
$OnlineH["staff_attendance"]["schoolduty_tab"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Up%20Special%20Duty]]";
$OnlineH["staff_attendance"]["schoolduty_tab"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Special%20Duty%20Handling%20Basics]]";
$OnlineH["staff_attendance"]["schoolduty_tab"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Up%20Special%20Duty]]";

##Duty Setup > Employment Period
$OnlineH["staff_attendance"]["employment"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["employment"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Employment%20Periods]]";
$OnlineH["staff_attendance"]["employment"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["employment"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Employment%20Periods]]";

##Settings > Access Rights
$OnlineH["staff_attendance"]["accessrights"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["accessrights"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Managing%20Access%20Rights]]";
$OnlineH["staff_attendance"]["accessrights"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["accessrights"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Managing%20Access%20Rights]]";

##Settings > Terminal
$OnlineH["staff_attendance"]["terminal"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["terminal"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Up%20Terminals]]";
$OnlineH["staff_attendance"]["terminal"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["terminal"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Setting%20Up%20Terminals]]";

##Settings > Reason
$OnlineH["staff_attendance"]["reason"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["reason"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Defining%20Non-presence%20Reasons]]";
$OnlineH["staff_attendance"]["reason"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["reason"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Defining%20Non-presence%20Reasons]]";

##Settings > OT Rule
$OnlineH["staff_attendance"]["ot_rule"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["ot_rule"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Defining%20Overtime%20Work%20Rules]]";
$OnlineH["staff_attendance"]["ot_rule"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[First%20Time%20Setup%20Overview]]";
$OnlineH["staff_attendance"]["ot_rule"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Defining%20Overtime%20Work%20Rules]]";

##Report > Attendance Report
$OnlineH["staff_attendance"]["attendance_rpt"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["attendance_rpt"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Attendance%20Report]]";
$OnlineH["staff_attendance"]["attendance_rpt"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["attendance_rpt"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Attendance%20Report]]";

##Report > Cust. Report
$OnlineH["staff_attendance"]["cust_rpt"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["cust_rpt"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Customized%20Report]]";
$OnlineH["staff_attendance"]["cust_rpt"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["cust_rpt"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Customized%20Report]]";

##Report > Roster Report
$OnlineH["staff_attendance"]["roster_rpt"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["roster_rpt"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Roster%20Report]]";
$OnlineH["staff_attendance"]["roster_rpt"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["roster_rpt"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Roster%20Report]]";

##Report > OT Report
$OnlineH["staff_attendance"]["ot_rpt"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["ot_rpt"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Overtime%20Work%20Report]]";
$OnlineH["staff_attendance"]["ot_rpt"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["ot_rpt"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Overtime%20Work%20Report]]";

##Report > Attendance Stat
$OnlineH["staff_attendance"]["attendance_stat"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["attendance_stat"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Attendance%20Statistics]]";
$OnlineH["staff_attendance"]["attendance_stat"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["attendance_stat"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Attendance%20Statistics]]";

##Report > Entry Log
$OnlineH["staff_attendance"]["entry_log"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["entry_log"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Entry%20Log]]";
$OnlineH["staff_attendance"]["entry_log"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["entry_log"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Entry%20Log]]";

##Report > Daily Log
$OnlineH["staff_attendance"]["daily_log"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["daily_log"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Daily%20Attendance%20Log]]";
$OnlineH["staff_attendance"]["daily_log"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["daily_log"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Daily%20Attendance%20Log]]";

##Report > Absence Log
$OnlineH["staff_attendance"]["abs_log"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["abs_log"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Absence%20Log]]";
$OnlineH["staff_attendance"]["abs_log"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["abs_log"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Absence%20Log]]";

##Report > Leave/abs Stat
$OnlineH["staff_attendance"]["leaveabs_stat"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["leaveabs_stat"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Leave%20Absence%20Statistics]]";
$OnlineH["staff_attendance"]["leaveabs_stat"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Reports%20Basics]]";
$OnlineH["staff_attendance"]["leaveabs_stat"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/sa3/#[[Using%20Leave%20Absence%20Statistics]]";
#####CHINESE BIG5#####

// end of staff attendance v3 helps
# $OnlineH["staff_attendance"]["add"]["basics"]["eng"]  = "http://www.abc.com/bcd.html#[[xxxx]]";
# $OnlineH["staff_attendance"]["add"]["basics"]["big5"]  = "http://www.abc.com/bcd.html#[[xxxxcht]]";

############# 2010-06-21 [Start]
#####ENGLISH#####
##Home page of iCalendar
$OnlineH["icalendar"]["home"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[iCalendar%C2%A9%20Basics]]";

##iCalendar > Clicked "New Event"
$OnlineH["icalendar"]["new_event"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20Events%20in%20iCalendar%C2%A9]]";

##eNotice (eAdmin)
$OnlineH["enotice_admin"]["home"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20eNotice]]";
$OnlineH["enotice_admin"]["home"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20Electronic%20Notices]]";

##eNotice (eAdmin) > Clicked New > Chosen audience and whether to use template or not
$OnlineH["enotice_admin"]["new_settings"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Settings%20for%20Creating%20Electronic%20Notices]]";

##eNotice (eAdmin) > Clicked New > Chosen audience and whether to use template or not > Clicked "Edit (Reply Slip)"
$OnlineH["enotice_admin"]["edit_slip"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20Reply%20Slips%20%28eNotice%29]]";

##SS > Class
$OnlineH["school_settings"]["class"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Forms%20and%20Classes]]";

##SS > Subject (both tabs)
$OnlineH["school_settings"]["subject"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Subject%20and%20Subject%20Group]]";
$OnlineH["school_settings"]["subject"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Subjects%20and%20Subject%20Groups]]";

##SS > Group (all tabs)
$OnlineH["school_settings"]["group"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20User%20Group]]";
$OnlineH["school_settings"]["group"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20User%20Groups]]";

##SS > School Calendar > School Year/Term
$OnlineH["school_settings"]["year_term"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20School%20Year%20and%20Term]]";
$OnlineH["school_settings"]["year_term"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20School%20Years%20and%20Terms]]";

##SS > School Calendar > Holiday/Event
$OnlineH["school_settings"]["event"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Holiday%20and%20Event]]";
$OnlineH["school_settings"]["event"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Holidays%20and%20Events]]";

##SS > School Calendar > Time Zone
$OnlineH["school_settings"]["time_zone"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Time%20Zone]]";
$OnlineH["school_settings"]["time_zone"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Time%20Zones]]";

##SS > Timetable
$OnlineH["school_settings"]["timetable"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Timetable]]";
$OnlineH["school_settings"]["timetable"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Timetables]]";

##SS > Campus
$OnlineH["school_settings"]["campus"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Campus%20Location]]";
$OnlineH["school_settings"]["campus"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Campus%20Locations]]";

##SS > Role
$OnlineH["school_settings"]["role_home"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Role%20Basics]]";
$OnlineH["school_settings"]["role_home"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Controlling%20Access%20Rights%20and%20Email%20Delivery]]";

##eAdmin > User Mangement > Parent
$OnlineH["user"]["parent"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20User%20Management]]";
$OnlineH["user"]["parent"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20User%20Accounts]]";

##eAdmin > User Mangement > Staff
$OnlineH["user"]["staff"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20User%20Management]]";
$OnlineH["user"]["staff"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20User%20Accounts]]";

##eAdmin > User Mangement > Student
$OnlineH["user"]["student"]["basics"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20User%20Management]]";
$OnlineH["user"]["student"]["howto"]["eng"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20User%20Accounts]]";

#####BIG 5#####
##Home page of iCalendar
$OnlineH["icalendar"]["home"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[iCalendar%C2%A9%20Basics]]";

##iCalendar > Clicked "New Event"
$OnlineH["icalendar"]["new_event"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20Events%20in%20iCalendar%C2%A9]]";

##eNotice (eAdmin)
$OnlineH["enotice_admin"]["home"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20eNotice]]";
$OnlineH["enotice_admin"]["home"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20Electronic%20Notices]]";

##eNotice (eAdmin) > Clicked New > Chosen audience and whether to use template or not
$OnlineH["enotice_admin"]["new_settings"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Settings%20for%20Creating%20Electronic%20Notices]]";

##eNotice (eAdmin) > Clicked New > Chosen audience and whether to use template or not > Clicked "Edit (Reply Slip)"
$OnlineH["enotice_admin"]["edit_slip"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20Reply%20Slips%20%28eNotice%29]]";

##SS > Class
$OnlineH["school_settings"]["class"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Forms%20and%20Classes]]";

##SS > Subject (both tabs)
$OnlineH["school_settings"]["subject"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Subject%20and%20Subject%20Group]]";
$OnlineH["school_settings"]["subject"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Subjects%20and%20Subject%20Groups]]";

##SS > Group (all tabs)
$OnlineH["school_settings"]["group"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20User%20Group]]";
$OnlineH["school_settings"]["group"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20User%20Groups]]";

##SS > School Calendar > School Year/Term
$OnlineH["school_settings"]["year_term"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20School%20Year%20and%20Term]]";
$OnlineH["school_settings"]["year_term"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20School%20Years%20and%20Terms]]";

##SS > School Calendar > Holiday/Event
$OnlineH["school_settings"]["event"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Holiday%20and%20Event]]";
$OnlineH["school_settings"]["event"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Holidays%20and%20Events]]";

##SS > School Calendar > Time Zone
$OnlineH["school_settings"]["time_zone"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Time%20Zone]]";
$OnlineH["school_settings"]["time_zone"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Time%20Zones]]";

##SS > Timetable
$OnlineH["school_settings"]["timetable"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Timetable]]";
$OnlineH["school_settings"]["timetable"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Timetables]]";

##SS > Campus
$OnlineH["school_settings"]["campus"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20Campus%20Location]]";
$OnlineH["school_settings"]["campus"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Setting%20Up%20Campus%20Locations]]";

##SS > Role
$OnlineH["school_settings"]["role_home"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Role%20Basics]]";
$OnlineH["school_settings"]["role_home"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Controlling%20Access%20Rights%20and%20Email%20Delivery]]";

##eAdmin > User Mangement > Parent
$OnlineH["user"]["parent"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20User%20Management]]";
$OnlineH["user"]["parent"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20User%20Accounts]]";

##eAdmin > User Mangement > Staff
$OnlineH["user"]["staff"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20User%20Management]]";
$OnlineH["user"]["staff"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20User%20Accounts]]";

##eAdmin > User Mangement > Student
$OnlineH["user"]["student"]["basics"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[About%20User%20Management]]";
$OnlineH["user"]["student"]["howto"]["big5"] = "http://support.broadlearning.com/doc/help/intranet/#[[Creating%20User%20Accounts]]";

############# 2010-06-21 [End]
?>