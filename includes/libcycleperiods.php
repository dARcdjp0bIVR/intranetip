<?php
// Using: 

/*
 *********************	Change Log	********************
 *
 *  Date    :   2019-07-29 [Cameron]
 *              - fix: should set global var $intranet_session_language in updateSchoolHolidayEvent() and deleteSchoolHolidayEvent() because libebooking need it
 *              - should reject booking when change from non "Skip School Day" to "Skip School Day" or vice versa
 *
 *  Date    :   2019-07-18 [Bill]
 *              - modified Get_Preload_Result(), Set_Preload_Result(), Check_PreloadInfoArr_Exist(), to remove ';' to prevent eval() security problem
 *
 *  Date    :   2019-07-02 (Cameron)
 *              - add reject booking logic in deleteSchoolHolidayEvent() if Skip School Day and using EverydayTimetable ($sys_custom['eBooking']['EverydayTimetable'])
 *
 *  Date    :   2019-06-16 (Cameron)
 *              - add function getTimeSlotByDefaultTimetable(), getDefaultTimeSlotByDate()
 *
 *  Date    :   2019-06-14 (Cameron)
 *              - add logic to handle changing "Skip School Day" in updateSchoolHolidayEvent() for $sys_custom['eBooking']['EverydayTimetable']
 *              - add defaultTimetableID to EverydayTimetable if not exist when delete "Skip School Day" event in deleteSchoolHolidayEvent() and DeleteAllIntranetEventByDateRange()
 *              - add function getDefaultTimetableIDFromTimezone(), getDefaultTimetableIDByDate(), getTimeSlotByDate()
 *              - fix commit condition in DeleteAllIntranetEventByDateRange(), use in_array to check result 
 *  
 *  Date    :   2019-06-13 (Cameron)
 *              - add reject booking logic in createSchoolHolidayEvent() if Skip School Day and using EverydayTimetable ($sys_custom['eBooking']['EverydayTimetable']) 
 *  
 *  Date    :   2019-05-29 (Cameron)
 *              - add function getDateInEverydayTimetable()
 *              
 *  Date    :   2019-05-27 (Cameron)
 *              - add function updateEverydayDefaultTimetableID()
 *              
 *  Date    :   2019-05-23 (Cameron)
 *              - add customization in Save_Special_Timetable_Settings()
 *              
 *  Date    :   2019-05-22 (Cameron)
 *              - add function updateEverydayTimetableRecord(),  deleteEverydayDefaultTimetableID(), getDefaultTimetableID()
 *              - add customization in createPeriod(), editPeriod() and deletePeriod()
 *              
 *  Date    :   2019-05-21 (Cameron)
 *              - call addEverydayTimetableRecord() in createPeriod(), editPeriod() [case #M141869]
 *              - add function addEverydayTimetableRecord()
 *
 *  Date    :   2019-03-01  (Isaac)
 *              modified Update_AcademicYear_DisplayOrder(), added ModifyBy to the sql update statement. 
 *
 *  Date    :   2019-01-09 (Anna)
 *              modified createSchoolHolidayEvent(), updateSchoolHolidayEvent(),createTmpSchoolHolidayEventTable - added EventNatureEng, DescriptionEng for general
 *
 *  Date    :   2018-07-09  (Philips)
 *              modified createSchoolHolidayEvent(), added sfoc cust
 *
 *  Date    :   2018-04-09  (Anna)
 *              modified updateSchoolHolidayEvent(),createSchoolHolidayEvent(), added sfoc cust
 *              
 *	Date	:	2017-03-09	(Anna)
 *				modified checkTermIsInUse()
 *
 *	Date	:	2016-07-04	(OMas) [ip.2.5.6.7.1]
 *				modified Cache_getProductionCycleInfo() - fix php 5.4 trim warning
 *
 *  Date	:	2016-01-04	(Omas) [ip.2.5.7.1.1] 
 * 				Add function IsFutureTerm() , modified checkTermIsInUse() add $skipCheckClass
 *
 *	Date	:	2014-11-04	(Ivan)
 *				Update function DeleteAllIntranetEventByDateRange() to add sync data to eLib+ coding
 *
  *	Date	:	2014-03-17 (Fai)
 *				Update function newAcademicYear add ' to the insert sql "userid"
 *					 
 *
 *	Date	:	2013-10-04 (Carlos)
 *				[KIS] modified createSchoolHolidayEvent(),updateSchoolHolidayEvent(), createTmpSchoolHolidayEventTable(), insertDataToTmpSchoolHolidayEventTable(),
 *					 importDataToINTRANET_EVENT(), add parameter $ClassGroupID
 *
 *	Date	:	2013-07-23 (Ivan) [2013-0723-1346-16132]
 *				updated insertDataToTmpSchoolHolidayEventTable() to support date format "D/M/YYYY" and "DD/MM/YYYY"
 *
 *	Date	:	2013-07-12 (Ivan) [2013-0712-1109-49167]
 *				updated importDataToINTRANET_EVENT(), fixed failed to insert data to INTRANET_GROUPEVENT due to $Type is overwritten
 *
 *	Date	:	2012-09-12 (YAtWoon)
 *				updated importDataToINTRANET_EVENT(), only GE can insert into INTRANET_GROUPEVENT
 *
 *	Date	:	2012-09-12 (YatWoon)
 *				updated updateSchoolHolidayEvent()
 *					- delete records in INTRANET_GROUPEVENT no matter which event type and with group id or not
 *					- insert records in INTRANET_GROUPEVENT only for event type is 2 (group event)
 *
 *	Date	:	2012-08-01 (Ivan)
 *				updated function importDataToINTRANET_EVENT, deleteSchoolHolidayEvent, createSchoolHolidayEvent, updateSchoolHolidayEvent to support syn holidays to library system
 *
 *	Date	: 	2012-06-25 (Ivan)
 *				updated function checkTermOverlap() to change the wrong variable name "$TermStart" to "$TermStartDate" to fix the wrong term overlap checking problem
 *
 *	Date	:	2011-09-26 (Ivan)
 *				updated importDataToINTRANET_EVENT() to add re-generate special timetable logic if the imported date skip school date
 *
 *	Date	:	2011-09-20 (YatWoon)
 *				update createSchoolHolidayEvent(), updateSchoolHolidayEvent(), remove "cater skip" checking (since handle in layout)
 *				update insertDataToTmpSchoolHolidayEventTable(), add "cater skip" logic
 *
 *	Date	:	2011-09-08 (YatWoon)
 *				update updateSchoolHolidayEvent(), createSchoolHolidayEvent(), refer to # remarks in interface layout
 
 *	Date	:	2011-08-26 (YatWoon)
 *				updated importDataToINTRANET_EVENT(), missing to call generatePreview() and generateProduction()
 *
 *	Date	:	2011-08-04 (Ivan)
 *	Details	:	added special timetable-related functions
 *
 *	Date	: 	2011-07-14 (Ivan)
 *	Details	:	added special timetable-related functions
 *				modified getCycleInfoByDateRange() to group the coding to use the same function to retrieve cycle day info
 *
 *	Date	:	2011-07-12 (Marcus)
 *	Details	:	added Get_All_TimeZone_By_Academic_Year 
 *
 *	Date	:	2011-07-11 (Marcus)
 *	Details	:	updated generateProduction, generatePreview, makeProduction, insert CycleDay and PeriodID 
 *
 *	Date	:	2011-07-06 (YatWoon)
 *	Details	:	updated createSchoolHolidayEvent(), updateSchoolHolidayEvent(), createTmpSchoolHolidayEventTable(), importDataToINTRANET_EVENT(), insertDataToTmpSchoolHolidayEventTable()
 *				==> add "isSkipSAT" and "isSkipSUN"
 *
 *	Date	: 	2011-06-28 (Ivan)
 *	Details	:	added function Check_School_Settings_Access_Right()
 *
 * 	Date	:	2010-10-06 (Ronald)
 * 	Details :	new function GetTotalNumOfEventsByDateRange($StartDate,$EndDate,$EventType=''),
 * 				used to return the total number of events
 *  
 * 	Date	:	2010-10-05 (Ronald)
 * 	Details :	new function checkIntranetEventExistByDateRange($StartDate,$EndDate),
 * 				used to check if there any events exist in the given date range
 * 
 * 	Date	:	2010-10-05 (Ronald)
 * 	Details	:	new function DeleteAllIntranetEventByDateRange($StartDate, $EndDate), 
 * 				used to delete ALL the events / holidays in school calender by date range 
 * 
 */


if (!defined("LIBCYCLEPERIODS_DEFINED"))         // Preprocessor directives
{

 define("LIBCYCLEPERIODS_DEFINED",true);

 class libcycleperiods extends libdb{
       var $PeriodID;
       var $PeriodStart;
       var $PeriodEnd;
       var $PeriodType;
       var $CycleType;
       var $PeriodDays;
       var $FirstDay;
       var $SaturdayCounted;

       var $array_numeric;
       var $array_alphabet;
       var $array_roman;
       
       var $PreloadInfoArr;

       function libcycleperiods($pid="")
       {
                $this->libdb();
                $this->tableFormat = array(
                "width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0"
                );
                $this->titleClass = array(
                "tableTitle"
                );

                for ($i=0; $i<26; $i++)
                {
                     $this->array_numeric[] = $i+1;
                }
                $this->array_alphabet = array("A","B","C","D","E","F","G","H","I","J",
                                              "K","L","M","N","O","P","Q","R","S","T",
                                              "U","V","W","X","Y","Z");
                $this->array_roman = array("I","II","III","IV","V","VI","VII","VIII","IX","X",
                                           "XI","XII","XIII","XIV","XV","XVI","XVII","XVIII","XIX","XX",
                                           "XXI","XXII","XXIII","XXIV","XXV","XXVI" );

                if ($pid!="")
                {
                    $this->retrieveRecord($pid);
                }
                
                // Preload Info Array
				$this->PreloadInfoArr = array();
       }
       function retrieveRecord($pid)
       {
                $sql = "SELECT PeriodID, PeriodStart, PeriodEnd, PeriodType,
                               CycleType, PeriodDays, FirstDay, SaturdayCounted
                               FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE PeriodID = $pid";
                $temp = $this->returnArray($sql,8);
                list($this->PeriodID,$this->PeriodStart,$this->PeriodEnd,$this->PeriodType,
                     $this->CycleType, $this->PeriodDays,$this->FirstDay,$this->SaturdayCounted) = $temp[0];
       }
       function returnPeriods()
       {
                $sql = "SELECT PeriodID, PeriodStart, PeriodEnd, PeriodType, CycleType, PeriodDays, FirstDay, SaturdayCounted
                               FROM INTRANET_CYCLE_GENERATION_PERIOD ORDER BY PeriodStart";
                return $this->returnArray($sql,8);
       }
       function displayPeriodsTableForEdit($format=0)
       {
                global $i_no_record_exists_msg,$image_path;
                global $i_CycleNew_Field_PeriodStart,$i_CycleNew_Field_PeriodEnd,$i_CycleNew_Field_PeriodType,
                       $i_CycleNew_Field_CycleType,$i_CycleNew_Field_PeriodDays,$i_CycleNew_Field_SaturdayCounted,
                       $i_CycleNew_Field_FirstDay;
                global $i_CycleNew_PeriodType_NoCycle,$i_CycleNew_PeriodType_Generation,$i_CycleNew_PeriodType_FileImport,
                       $i_CycleNew_CycleType_Numeric,$i_CycleNew_CycleType_Alphabetic,$i_CycleNew_CycleType_Roman;
                global $i_CycleNew_Alert_RemoveConfirm;

                $periods = $this->returnPeriods();

                $x = "<table ".$this->tableFormat[$format]." >\n";
                $x .= "<tr><td width=120 class=".$this->titleClass[$format].">$i_CycleNew_Field_PeriodStart</td>
                        <td width=80 class=".$this->titleClass[$format].">$i_CycleNew_Field_PeriodEnd</td>
                        <td class=".$this->titleClass[$format].">$i_CycleNew_Field_PeriodType</td>
                        <td class=".$this->titleClass[$format].">$i_CycleNew_Field_CycleType</td>
                        <td class=".$this->titleClass[$format].">$i_CycleNew_Field_PeriodDays</td>
                        <td class=".$this->titleClass[$format].">$i_CycleNew_Field_FirstDay</td>
                        <td class=".$this->titleClass[$format].">$i_CycleNew_Field_SaturdayCounted</td>
                        </tr>\n";
                for ($i=0; $i<sizeof($periods); $i++)
                {
                     $css = ($i%2==0? "":"2");
                     list($pid,$start,$end,$type,$cycletype,$days,$firstDay,$satCount) = $periods[$i];
                     switch ($type)
                     {
                           case 0: $str_type = $i_CycleNew_PeriodType_NoCycle; break;
                           case 1: $str_type = $i_CycleNew_PeriodType_Generation; break;
                           case 2: $str_type = $i_CycleNew_PeriodType_FileImport; break;
                           default: $str_type = $i_CycleNew_PeriodType_NoCycle; break;
                     }
                     switch ($cycletype)
                     {
                           case 0: $str_cycletype = $i_CycleNew_CycleType_Numeric; break;
                           case 1: $str_cycletype = $i_CycleNew_CycleType_Alphabetic; break;
                           case 2: $str_cycletype = $i_CycleNew_CycleType_Roman; break;
                           default: $str_cycletype = $i_CycleNew_CycleType_Numeric; break;
                     }
                     $str_satCount = ($satCount==1? "<img src=\"$image_path/campusmail/btn_tick.gif\">":"&nbsp;");
                     if ($type==0)
                     {
                         $str_cycletype = "--";
                         $days = "--";
                         $str_satCount = "--";
                         $firstDay = "--";
                     }
                     else if ($type==2)
                     {
                          $str_cycletype = "--";
                          $days = "--";
                          $str_satCount = "--";
                          $firstDay = "--";
                     }
                     else
                     {
                         if ($cycletype==1)
                         {
                             $firstDay = $this->array_alphabet[$firstDay];
                         }
                         else if ($cycletype==2)
                         {
                             $firstDay = $this->array_roman[$firstDay];
                         }
                         else
                         {
                             $firstDay = $this->array_numeric[$firstDay];
                         }
                     }
                     $link = "";
                     $link .= "<a class=functionlink href=\"edit.php?pid=$pid\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
                     $link .= "<a class=functionlink href=\"javascript:removePeriod($pid)\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
                     $x .= "<tr class=tableContent$css>
                              <td>$start $link</td><td>$end</td><td>$str_type</td>
                              <td>$str_cycletype</td><td>$days</td><td>$firstDay</td><td>$str_satCount</td>
                            </tr>\n";
                }
                if (sizeof($periods)==0)
                {
                    $x .= "<tr><td colspan=6 align=center>$i_no_record_exists_msg</td></tr>\n";
                }
                $x .= "</table>\n";
                $x .= "<SCRIPT language=Javascript>
                function removePeriod(id)
                {
                         if (confirm('$i_CycleNew_Alert_RemoveConfirm'))
                         {
                             location.href = 'remove.php?pid='+id;
                         }
                }
                </SCRIPT>
                ";
                return $x;
       }
       function retrieveImportRecords($start,$end)
       {
                $sql = "SELECT RecordID, RecordDate, TextEng, TextChi, TextShort
                               FROM INTRANET_CYCLE_IMPORT_RECORD WHERE RecordDate >= '$start' AND RecordDate <= '$end'
                               ORDER BY RecordDate";
                return $this->returnArray($sql,5);
       }
       function clearPreview()
       {
                $sql = "DELETE FROM INTRANET_CYCLE_TEMP_DAYS_VIEW";
                $this->db_db_query($sql);
       }
       function clearProduction()
       {
                $sql = "DELETE FROM INTRANET_CYCLE_DAYS";
                $this->db_db_query($sql);
       }
       function makeProduction()
       {
                $sql = "INSERT IGNORE INTO INTRANET_CYCLE_DAYS (RecordDate, TextEng, TextChi, TextShort, CycleDay, PeriodID)
                               SELECT RecordDate, TextEng, TextChi, TextShort, CycleDay, PeriodID FROM
                                      INTRANET_CYCLE_TEMP_DAYS_VIEW";
                $this->db_db_query($sql);
       }
       # For Resource Booking use
       function returnCriteriaString($value, $start, $end)
       {
                global $intranet_session_language,$i_BookingType_Every;
                # Get
                $fieldname = ($intranet_session_language=="en"?"TextEng":"TextChi");
                $sql = "SELECT DISTINCT $fieldname FROM INTRANET_CYCLE_DAYS
                               WHERE RecordDate >= '$start' AND RecordDate <= '$end' AND TextShort = '$value'";
                $result = $this->returnVector($sql);
                $num = sizeof($result);
                if ($num==0 || $result[0]=="")
                {
                    return;
                }
                if ($num==1)
                {
                    return "$i_BookingType_Every ".$result[0];
                }
                else
                {
                    return "$i_BookingType_Every ".implode(",",$result)." ($value)";
                }
       }
       function getNonCycleDays($start,$end)
       {
                $sql = "SELECT DISTINCT EventDate FROM INTRANET_EVENT
                               WHERE EventDate >= '$start' AND EventDate <= '$end'
                                     AND RecordStatus = 1 AND
                                     (
                                       (isSkipCycle IS NULL AND RecordType = 2) OR (isSkipCycle = 1)
                                     )";
                return $this->returnVector($sql);
       }
       function getPreviewCycleInfo($year,$month)
       {
                $sql = "SELECT DATE_FORMAT(RecordDate,'%Y-%m-%d'), TextEng, TextChi, TextShort
                               FROM INTRANET_CYCLE_TEMP_DAYS_VIEW
                               WHERE YEAR(RecordDate) = '$year' AND MONTH(RecordDate) = '$month' ORDER BY RecordDate";
                $data = $this->returnArray($sql,4);
                for ($i=0; $i<sizeof($data); $i++)
                {
                     list($date, $eng, $chi, $short) = $data[$i];
                     $result[$date] = array($eng, $chi, $short);
                }
                return $result;
       }
       function getNonCycleDaysInfo($year, $month)
       {
                global $i_EventTypeSchool,$i_EventTypeAcademic,$i_EventTypeHoliday,$i_EventTypeGroup;
                $type_array = array($i_EventTypeSchool,$i_EventTypeAcademic,$i_EventTypeHoliday,$i_EventTypeGroup);

                $sql = "SELECT DATE_FORMAT(EventDate,'%Y-%m-%d'), Title, RecordType FROM INTRANET_EVENT
                               WHERE YEAR(EventDate) = '$year' AND MONTH(EventDate) = '$month'
                                     AND RecordStatus = 1 AND
                                     (
                                       (isSkipCycle IS NULL AND RecordType = 2) OR (isSkipCycle = 1)
                                     )";
                $data = $this->returnArray($sql,3);
                for ($i=0; $i<sizeof($data); $i++)
                {
                     list($date, $title, $type) = $data[$i];
                     $string = "$title [<i>".$type_array[$type]."</i>]";
                     if ($result[$date]!="")
                     {
                         $result[$date] .= ",";
                     }
                     $result[$date] .= $string;
                }
                return $result;
       }
       function displayMonth($year,$month)
       {
                global $i_frontpage_day,$i_CycleNew_Field_TxtChi,$i_CycleNew_Field_TxtEng;
                $tempInfo = $this->getPreviewCycleInfo($year,$month);
                $nonCycleInfo = $this->getNonCycleDaysInfo($year,$month);
                $ts = mktime(0,0,0,$month,1,$year);
                $x = "$month/$year\n";
                $x .= "<table width=80% border=1 bordercolorlight=#B3B692 bordercolordark=#F8FBD6 cellspacing=0 cellpadding=0>\n";
                $x .= "<tr align=center valign=middle>\n";
                $x .= "<td align=center>".$i_frontpage_day[0]."</td>\n";
                $x .= "<td align=center>".$i_frontpage_day[1]."</td>\n";
                $x .= "<td align=center>".$i_frontpage_day[2]."</td>\n";
                $x .= "<td align=center>".$i_frontpage_day[3]."</td>\n";
                $x .= "<td align=center>".$i_frontpage_day[4]."</td>\n";
                $x .= "<td align=center>".$i_frontpage_day[5]."</td>\n";
                $x .= "<td align=center>".$i_frontpage_day[6]."</td>\n";
                $x .= "</tr>\n";

                # Get the first weekday
                $weekday = date("w",$ts);
                for ($i=0; $i<$weekday; $i++)
                {
                     $x .= "<td bgcolor=gray>&nbsp;</td>";
                }

                while (date("m",$ts)==$month)
                {
                       $date_string = date("Y-m-d",$ts);
                       $day = date("d",$ts);
                       list($cycle_eng, $cycle_chi,$cycle_day) = $tempInfo[$date_string];
                       $holiday_string = $nonCycleInfo[$date_string];
                       $layerText = "";

                       if ($holiday_string != "")
                       {
                           $layerText = "$holiday_string<br>";
                       }
                       if ($cycle_day == "")
                       {
                           $cycle_day = "&nbsp;";
                       }
                       else
                       {
                           $layerText .= "$i_CycleNew_Field_TxtEng : $cycle_eng<br>";
                           $layerText .= "$i_CycleNew_Field_TxtChi : $cycle_chi";
                       }
                       $cycle_day = "<i><font color=#0046A0>$cycle_day</font></i>";

                       $toolTipShown = ($layerText!=""? "onMouseMove='overhere()' onMouseOut='hideTip()' onBlur='hideTip()' onMouseOver=\"showTip('ToolTip',makeTip('$layerText'))\"":"");
                       $x .= "<td><table width=100% border=0 cellpadding=0 cellspacing=0>
                       <tr><td align=left>$cycle_day</td></tr>
                       <tr><td align=center><a $toolTipShown href=#>$day</a></td></tr>
                       </table></td>";
                       $weekday = date("w",$ts);
                       if ($weekday == 6)
                       {
                           $x .= "</tr>";
                       }
                       $ts += 86400;
                }
                for ($i=$weekday; $i<6; $i++)
                {
                       $x .= "<td bgcolor=gray>&nbsp;</td>";
                }
                if ($weekday != 6) $x .= "</tr>";
                $x .= "</table>";
                return $x;
       }
       function getCycleInfoByYearMonth($year,$month)
       {
                $sql = "SELECT DATE_FORMAT(RecordDate,'%Y-%m-%d'), TextEng, TextChi, TextShort
                               FROM INTRANET_CYCLE_DAYS
                               WHERE YEAR(RecordDate) = '$year' AND MONTH(RecordDate) = '$month' ORDER BY RecordDate";
                $data = $this->returnArray($sql,4);
                for ($i=0; $i<sizeof($data); $i++)
                {
                     list($date, $eng, $chi, $short) = $data[$i];
                     $result[$date] = array($eng, $chi, $short);
                }
                return $result;
       }
       function getCycleDayStringByDate($date)
       {
                global $intranet_session_language;
                $temp = $this->getCycleInfoByDate($date);
                return ($intranet_session_language=="b5"?$temp[1]:$temp[0]);
       }

       function getCycleInfoByDate($date)
       {
                $sql = "SELECT DATE_FORMAT(RecordDate,'%Y-%m-%d'), TextEng, TextChi, TextShort, CycleDay
                               FROM INTRANET_CYCLE_DAYS
                               WHERE RecordDate = '$date' ORDER BY RecordDate";
                $data = $this->returnArray($sql);
                return array($data[0][1],$data[0][2],$data[0][3], 'CycleDay' => $data[0]['CycleDay']);
       }
       function getCycleInfoByDateArray($dates)
       {
                $list = "'".implode("','",$dates)."'";
                $sql = "SELECT DATE_FORMAT(RecordDate,'%Y-%m-%d'), TextEng, TextChi, TextShort
                               FROM INTRANET_CYCLE_DAYS
                               WHERE RecordDate IN ($list) ORDER BY RecordDate";
                $data = $this->returnArray($sql,4);
                for ($i=0; $i<sizeof($data); $i++)
                {
                     list($date, $eng, $chi, $short) = $data[$i];
                     $result[$date] = array($eng, $chi, $short);
                }
                return $result;
       }
       function getCycleInfoByDateRange($start, $end)
       {
//                $sql = "SELECT DATE_FORMAT(RecordDate,'%Y-%m-%d'), TextEng, TextChi, TextShort
//                               FROM INTRANET_CYCLE_DAYS
//                               WHERE RecordDate >= '$start' AND RecordDate <= '$end' ORDER BY RecordDate";
//                $data = $this->returnArray($sql,4);
//				
//                for ($i=0; $i<sizeof($data); $i++)
//                {
//                     list($date, $eng, $chi, $short) = $data[$i];
//                     $result[$date] = array($eng, $chi, $short);
//                }

			$data = $this->Get_Cycle_Day_Info_By_Date_Range($start, $end);
			$numOfData = count($data);
			$result = array();
			for ($i=0; $i<$numOfData; $i++) {
				$thisRecordDate = $data[$i]['RecordDate'];
				$thisTextEng = $data[$i]['TextEng'];
				$thisTextChi = $data[$i]['TextChi'];
				$thisTextShort = $data[$i]['TextShort'];
				
				$result[$thisRecordDate] = array($thisTextEng, $thisTextChi, $thisTextShort);
			}
			return $result;
       }
       function getCycleNamesByDateRange($start,$end)
       {
                global $intranet_session_language;
                $fieldname = ($intranet_session_language=="en"? "TextEng":"TextChi");
                $sql = "SELECT DISTINCT TextShort, $fieldname FROM INTRANET_CYCLE_DAYS
                               WHERE RecordDate >= '$start' AND RecordDate <= '$end' ORDER BY TextShort";
                return $this->returnArray($sql,2);
       }
       function getDatesForCycleDay ($start,$end,$cycle_day)
       {
                $sql = "SELECT RecordDate FROM INTRANET_CYCLE_DAYS
                               WHERE RecordDate >= '$start' AND RecordDate <= '$end' AND TextShort = '$cycle_day'
                               ORDER BY RecordDate";
                return $this->returnVector($sql);
       }
       function getDay()
       {
                global $i_DayType0;
                return $i_DayType0[date("w")];
       }

       function display()
       {
                global $intranet_session_language;
                $date = date("Y-m-d");
                $info = $this->getCycleInfoByDate($date);
                list ($txtEng, $txtChi, $txtShort) = $info;

                $weekday = $this->getDay();
                $month = date('Y-m');
                $day = date('d');
                if ($intranet_session_language=="en")
                {
                    if ($txtEng != "")
                        $name = $txtEng;
                    else if ($txtChi != "")
                         $name = $txtChi;
                }
                else
                {
                    if ($txtChi != "")
                        $name = $txtChi;
                    else if ($txtEng != "")
                         $name = $txtEng;
                }
                $name = ($name==""? $txtShort:$name);

                $x = "<table width=183 border=0 cellspacing=0 cellpadding=0 background=/images/index/daily.gif>
                         <tr>
                           <td width=12 height=23><img src=/images/spacer.gif width=12 height=1></td>
                           <td width=63 height=23><img src=/images/spacer.gif width=63 height=1></td>
                           <td width=28 height=23><img src=/images/spacer.gif width=28 height=1></td>
                           <td width=76 align=center height=23 class=td_body>$month</td>
                           <td width=4 height=23><img src=/images/spacer.gif width=4 height=1></td>
                         </tr>
                         <tr>
                           <td width=12 height=46>&nbsp;</td>
                           <td width=63 align=center height=46 valign=top><font color=#FF0000 class=body>$name</font></td>
                           <td width=28 height=46>&nbsp;</td>
                           <td class=td_center_middle width=76 align=center height=46><b><font size=4 face='Verdana, Arial, Helvetica, sans-serif, �s�ө���'>$day</font></b><br>
                           <font size=2>$weekday</font></td>
                           <td width=4 height=46>&nbsp;</td>
                         </tr>
                         <tr>
                           <td colspan=5 height=3><img src=/images/spacer.gif width=183 height=3></td>
                         </tr>
                      </table>";
                return $x;
       }
       
       
       ## Below is for IP2.5 ##
       function returnPeriods_NEW($PeriodID='', $PublishedOnly=0, $AcademicYearID='', $StartDate='', $EndDate='')
       {
   			$PreloadArrKey = 'returnPeriods_NEW';
			$FuncArgArr = get_defined_vars();
			$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    	if ($returnArr !== false) {
	    		return $returnArr;
	    	}
	    	
	    	
       		if ($PeriodID != "") {
       			$conds_PeriodID = " AND PeriodID = '$PeriodID' ";
       		}
       			
       		if ($PublishedOnly) {
       			$conds_RecordStatus = " AND RecordStatus = '1' ";
       		}
       			
       		if ($AcademicYearID != '')
       		{
	       		include_once("form_class_manage.php");
				$Obj_AcademicYear = new academic_year($AcademicYearID);
				# Year-Month-Day only, exclude the time
				$academicYearStart = substr($Obj_AcademicYear->AcademicYearStart, 0, 10);
				$academicYearEnd = substr($Obj_AcademicYear->AcademicYearEnd, 0, 10);
				
				$conds_PeriodRange = " AND '$academicYearStart' <= PeriodStart AND PeriodEnd <= '$academicYearEnd' ";
       		}
       		
       		if ($StartDate != '' && $EndDate != '') {
       			$conds_DateRange = " And ('$StartDate' <= PeriodEnd AND PeriodStart <= '$EndDate') ";
       		}
       			
            $sql = "SELECT PeriodID, PeriodStart, PeriodEnd, PeriodType, CycleType, PeriodDays, FirstDay, SaturdayCounted, SkipOnWeekday
                           FROM INTRANET_CYCLE_GENERATION_PERIOD 
                           WHERE 	1 
                           			$conds_PeriodID
									$conds_RecordStatus
									$conds_PeriodRange
									$conds_DateRange
                           ORDER BY PeriodStart";
            $ReturnArr = $this->returnArray($sql,9);
            
            $this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
            return $ReturnArr;
       }
       
       function Get_Timezone_Date_Range($TimezoneIDArr) {
       		$PreloadArrKey = 'Get_Timezone_Date_Range';
			$FuncArgArr = get_defined_vars();
			$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    	if ($returnArr !== false) {
	    		return $returnArr;
	    	}
	    	
	    	
       		$sql = "Select
							Min(PeriodStart) as StartDate,
							Max(PeriodEnd) as EndDate
					From 
							INTRANET_CYCLE_GENERATION_PERIOD
					Where
							PeriodID In ('".implode("','", (array)$TimezoneIDArr)."')
					";
            $ReturnArr = $this->returnArray($sql);
            
            $this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
            return $ReturnArr;
       }
	   
	   function createNewPeriod($PeriodStart,$PeriodEnd,$PeriodType,$CycleType=0,$PeriodDays=0,$FirstDay=0,$SatCount=0)
	   {
				$li->Start_Trans();
				
				$sql = "INSERT INTO 
								INTRANET_CYCLE_GENERATION_PERIOD 
								(PeriodStart, PeriodEnd, PeriodType, CycleType, PeriodDays, FirstDay, SaturdayCounted, DateInput, DateModified) 
						VALUES 
								('$PeriodStart','$PeriodEnd','$PeriodType','$CycleType','$PeriodDays','$FirstDay','$SatCount',NOW(),NOW())";
				$result['NewPeriod'] = $li->db_db_query($sql);
	
				if (in_array(false,$result)) {
					$li->RollBack_Trans();
					echo 0;
				}
				else {
					$li->Commit_Trans();
					echo 1;
				}
	   }
//	   function getProductionCycleInfo($year,$month)
//       {
//                $sql = "SELECT DATE_FORMAT(RecordDate,'%Y-%m-%d'), TextEng, TextChi, TextShort
//                               FROM INTRANET_CYCLE_DAYS
//                               WHERE YEAR(RecordDate) = '$year' AND MONTH(RecordDate) = '$month' ORDER BY RecordDate";
//                $data = $this->returnArray($sql,4);
//                for ($i=0; $i<sizeof($data); $i++)
//                {
//                     list($date, $eng, $chi, $short) = $data[$i];
//                     $result[$date] = array($eng, $chi, $short);
//                }
//                return $result;
//       }

	   function getProductionCycleInfo($year,$month)
       {
			if(!isset($this->Cache_ProductionCycleInfo_Arr[$year]))
			{
				$this->Cache_getProductionCycleInfo($year);
			}
			return $this->Cache_ProductionCycleInfo_Arr[$year][$month];
       }
              
       function Cache_getProductionCycleInfo($year='')
       {
       		//if(trim($year)!='')
       		if(!empty($year))
       			$cond_year = " AND YEAR(RecordDate) IN ('".implode("','",(array)$year)."') ";
       	
			$sql = "SELECT DATE_FORMAT(RecordDate,'%Y-%m-%d'), TextEng, TextChi, TextShort, CycleDay, PeriodID, YEAR(RecordDate) Year, MONTH(RecordDate) Month
                           FROM INTRANET_CYCLE_DAYS
                           WHERE 1 $cond_year ORDER BY RecordDate";
            $data = $this->returnArray($sql,4);
            
            $data = BuildMultiKeyAssoc($data, array("Year","Month"),'',0,1);
            foreach($data as $thisYear => $MonthArr)
            {
	            foreach($MonthArr as $thisMonth => $InfoArr)
	            {
		            for ($i=0; $i<sizeof($InfoArr); $i++)
		            {
		                 list($date, $eng, $chi, $short, $CycleDay, $PeriodID) = $InfoArr[$i];
		                 $result[$date] = array($eng, $chi, $short, $CycleDay, $PeriodID);
		                 $this->Cache_ProductionCycleInfo_Arr[$thisYear][$thisMonth] = $result;
		            }
	            }
            }
            
            
       }
       
       function getLastPublishDate()
       {
		       $sql = "SELECT DateInput FROM INTRANET_CYCLE_DAYS_PRODUCTION_LOG WHERE LogID = 1";
		       $arrResult = $this->returnVector($sql);
		       
		       if(sizeof($arrResult)>0){
			       $x = $arrResult[0];
		       }else{
			       $x = '';
		       }
		       return $x;
       }
       
       function getTimeZoneLastModifiedInfo()
       {
		       $sql = "SELECT MAX(DateModified) FROM INTRANET_CYCLE_GENERATION_PERIOD";
		       $arrResult = $this->returnVector($sql);
		       
		       if(sizeof($arrResult)>0){
			       $LastModifiedDate = $arrResult[0];
		       }else{
			       $LastModifiedDate = '';
		       }
		       $sql = "SELECT DISTINCT ".getNameFieldByLang("b.")." FROM INTRANET_CYCLE_GENERATION_PERIOD AS a INNER JOIN INTRANET_USER AS b ON (a.ModifyBy = b.UserID) WHERE a.DateModified = '$LastModifiedDate'";
		       $result = $this->returnVector($sql);
		       $LastModifiedPerson = $result[0];
		       
		       $ans[] = $LastModifiedDate;
		       $ans[] = $LastModifiedPerson;
		       
		       return $ans;
       }
       
       function getHolidayLastPublishDate()
       {
		       $sql = "SELECT Max(DateInput) FROM INTRANET_EVENT";
		       $arrResult = $this->returnVector($sql);
		       
		       if(sizeof($arrResult)>0){
			       $x = $arrResult[0];
		       }else{
			       $x = '';
		       }
		       return $x;
       }
       
       function getHolidayLastModifiedInfo()
       {
		       $sql = "SELECT MAX(DateModified) FROM INTRANET_EVENT";
		       $arrResult = $this->returnVector($sql);
		       
		       if(sizeof($arrResult)>0){
			       $LastModifiedDate = $arrResult[0];
		       }else{
			       $LastModifiedDate = '';
		       }
		       
		       $sql = "SELECT DISTINCT ".getNameFieldByLang("b.")." FROM INTRANET_EVENT AS a INNER JOIN INTRANET_USER AS b ON (a.ModifyBy = b.UserID) WHERE a.DateModified = '$LastModifiedDate'";
		       $result = $this->returnVector($sql);
		       $LastModifiedPerson = $result[0];
		       
		       $ans[] = $LastModifiedDate;
		       $ans[] = $LastModifiedPerson;
		       		       
		       return $ans;
       }
       
		function getCurrentCyclePeriod()
		{
				$sql = "SELECT 
								*
						FROM 	
								INTRANET_CYCLE_GENERATION_PERIOD
						WHERE
								DATE_FORMAT(NOW(),'%Y-%m-%d') BETWEEN PeriodStart AND PeriodEnd
						";
				$resultSet = $this->returnArray($sql);
				
				return $resultSet;
		}
		
		function generatePreview()
		{
				# Clear Preview table
				$this->clearPreview();
				
				# Get Periods
				$periods = $this->returnPeriods_NEW();
				
				# Handle each period
				for ($i=0; $i<sizeof($periods); $i++)
				{
				     list($PeriodID, $PeriodStart, $PeriodEnd, $PeriodType, $CycleType, $PeriodDays, $FirstDay, $SaturdayCounted, $SkipOnWeekday) = $periods[$i];
				     if ($PeriodDays < 1) continue;
				     if ($PeriodType == 1)         # Calculation
				     {
				         if ($CycleType == 1)
				         {
				             $txtArray = $this->array_alphabet;
				         }
				         else if ($CycleType == 2)
				         {
				              $txtArray = $this->array_roman;
				         }
				         else
				         {
				             $txtArray = $this->array_numeric;
				         }
				
				         # Get Non-Cycle Days
				         $temp = $this->getNonCycleDays($PeriodStart,$PeriodEnd);
				         
				         for ($j=0; $j<sizeof($temp); $j++)
				         {
				              $ts = strtotime($temp[$j]);
				              $non_cycle_days[$ts] = "1";
				         }
				         $ts_start = strtotime($PeriodStart);
				         $ts_end = strtotime($PeriodEnd);
				         $ts_current = $ts_start;
				         $current_weekday = date("w",$ts_current);
				         $current_cycleday = $FirstDay;
				         $values = "";
				         $delim = "";

				         while ($ts_current <= $ts_end)
				         {
				                #$recordDate = date("Y-m-d",$ts_current);
				
				                # Check is non-cycle day
				                if ($non_cycle_days[$ts_current]==1) # Skip
				                {
				                    #echo "$recordDate -> Non cycle<br>\n";
				                    $current_weekday = ($current_weekday+1)%7;
				                    $ts_current += 86400;  # 1 Day
				                    continue;
				                }
				                # Check is weekend
				                //if ($current_weekday==0 || ($current_weekday==6 && $SaturdayCounted!=1))
				                if ($current_weekday==0)
				                {
				                    #echo "$recordDate -> Weekend<br>\n";
				                    $ts_current += 86400;  # 1 Day
				                    $current_weekday = ($current_weekday+1)%7;
				                    continue;
				                }
				                # Check if skip on weekday
				                $arrSkipOnWeekday = array();
				                $arrSkipOnWeekday = explode(",",$SkipOnWeekday);
				                if(in_array($current_weekday,$arrSkipOnWeekday)){
				                	$ts_current += 86400;  # 1 Day
				                    $current_weekday = ($current_weekday+1)%7;
				                    continue;
				                }
				                # Put in Database
				                $recordDate = date("Y-m-d",$ts_current);
				                $txtCycle = $txtArray[$current_cycleday];
				                
				                $values .= "$delim ('$recordDate','$i_CycleNew_Prefix_Eng$txtCycle','$i_CycleNew_Prefix_Chi$txtCycle','$txtCycle','".($current_cycleday+1)."','$PeriodID')";
				                $delim = ",";
				
				                # Next iteration
				                $current_weekday = ($current_weekday+1)%7;
				                $current_cycleday = ($current_cycleday+1)%$PeriodDays;
				                $ts_current += 86400;  # 1 Day
				         }
				         $sql = "INSERT INTO INTRANET_CYCLE_TEMP_DAYS_VIEW (RecordDate, TextEng, TextChi, TextShort, CycleDay, PeriodID)
				                        VALUES $values";
				                        
				         $this->db_db_query($sql);
				     }
				     else if ($PeriodType == 2)      # File import
				     {
				          $sql = "INSERT IGNORE INTO INTRANET_CYCLE_TEMP_DAYS_VIEW (RecordDate,TextEng,TextChi,TextShort, CycleDay, PeriodID)
				                         SELECT RecordDate, TextEng, TextChi, TextShort FROM INTRANET_CYCLE_IMPORT_RECORD
				                                WHERE RecordDate >= '$PeriodStart' AND RecordDate <= '$PeriodEnd'";
				          $this->db_db_query($sql);
				     }
				     else
				     {
				         # Nothing to do
				     }
				}
		}
		
		function generateProduction()
		{
				# Clear Production table
				$this->clearProduction();
				
				### Old version -> run makeProduction() to copy from preview to production
				# Copy to production
				//$lc->makeProduction();
				
				### New Version -> directly copy the setting to production
				# Get Periods
				$periods = $this->returnPeriods_NEW();
				
				# Handle each period
				for ($i=0; $i<sizeof($periods); $i++)
				{
				     list($PeriodID, $PeriodStart, $PeriodEnd, $PeriodType, $CycleType, $PeriodDays, $FirstDay, $SaturdayCounted, $SkipOnWeekday) = $periods[$i];
				     if ($PeriodDays < 1) continue;
					 if ($PeriodType == 1)         # Use Cycle Day
					 {
				         if ($CycleType == 1)
				         {
				             $txtArray = $this->array_alphabet;
				         }
				         else if ($CycleType == 2)
				         {
				              $txtArray = $this->array_roman;
				         }
				         else
				         {
				             $txtArray = $this->array_numeric;
				         }
				
				         # Get Non-Cycle Days
				         $temp = $this->getNonCycleDays($PeriodStart,$PeriodEnd);
				         for ($j=0; $j<sizeof($temp); $j++)
				         {
				              $ts = strtotime($temp[$j]);
				              $non_cycle_days[$ts] = "1";
				         }
				         $ts_start = strtotime($PeriodStart);
				         $ts_end = strtotime($PeriodEnd);
				         $ts_current = $ts_start;
				         $current_weekday = date("w",$ts_current);
				         $current_cycleday = $FirstDay;
				         $values = "";
				         $delim = "";
				         while ($ts_current <= $ts_end)
				         {
				                #$recordDate = date("Y-m-d",$ts_current);
				
				                # Check is non-cycle day
				                if ($non_cycle_days[$ts_current]==1) # Skip
				                {
				                    #echo "$recordDate -> Non cycle<br>\n";
				                    $current_weekday = ($current_weekday+1)%7;
				                    $ts_current += 86400;  # 1 Day
				                    continue;
				                }
				                # Check is weekend
				                //if ($current_weekday==0 || ($current_weekday==6 && $SaturdayCounted!=1))
				                if ($current_weekday==0)
				                {
				                    #echo "$recordDate -> Weekend<br>\n";
				                    $ts_current += 86400;  # 1 Day
				                    $current_weekday = ($current_weekday+1)%7;
				                    continue;
				                }
				                # Check is Skip on Weekday
				                $arrSkipOnWeekday = array();
				                $arrSkipOnWeekday = explode(",",$SkipOnWeekday);
				                if(in_array($current_weekday,$arrSkipOnWeekday)){
				                	$ts_current += 86400;  # 1 Day
				                    $current_weekday = ($current_weekday+1)%7;
				                    continue;
				                }
				                # Put in Database
				                $recordDate = date("Y-m-d",$ts_current);
				                $txtCycle = $txtArray[$current_cycleday];
				                $values .= "$delim ('$recordDate','$i_CycleNew_Prefix_Eng$txtCycle','$i_CycleNew_Prefix_Chi$txtCycle','$txtCycle','".($current_cycleday+1)."','$PeriodID')";
				                $delim = ",";
				
				                # Next iteration
				                $current_weekday = ($current_weekday+1)%7;
				                $current_cycleday = ($current_cycleday+1)%$PeriodDays;
				                $ts_current += 86400;  # 1 Day
				         }
				         $sql = "INSERT INTO INTRANET_CYCLE_DAYS (RecordDate, TextEng, TextChi, TextShort, CycleDay, PeriodID)
				                        VALUES $values";
				         $this->db_db_query($sql);
				         
				         $arrTargetPeriod[] .= $PeriodID;
				         
				         ### update the publish log ###
				         $sql = "INSERT INTO INTRANET_CYCLE_DAYS_PRODUCTION_LOG (LogID,DateInput) VALUES (1,NOW()) ON DUPLICATE KEY UPDATE DateInput = NOW()";
				         $this->db_db_query($sql);
			         }
				}
				for ($i=0; $i<sizeof($periods); $i++)
				{
					list($PeriodID, $PeriodStart, $PeriodEnd, $PeriodType, $CycleType, $PeriodDays, $FirstDay, $SaturdayCounted) = $periods[$i];
					
					$sql = "UPDATE INTRANET_CYCLE_GENERATION_PERIOD SET RecordStatus = 1 WHERE PeriodID = $PeriodID";
				    $this->db_db_query($sql);
				}
		}
		
		function createSchoolHolidayEvent($EventDate,$EventEndDate,$RecordType,$TargetGroupID='',$Title,$EventLocationID='',$EventVenue='',$EventNature='',$Description='',$isSkipCycle,$RecordStatus,$isSkipSAT='',$isSkipSUN='',$ClassGroupID='',
		   $EventAssociation='',$EventStartTime='',$EventEndTime='',$EventEmail='',$EventContact='',$EventWebsite='',$EventToBeDetermined='',$EventTitleEng = '', $EventAssociationEng = '', $EventVenueEng = '',$EventNatureEng = '',$DescriptionEng = '')
		{
		    global $UserID, $PATH_WRT_ROOT,$sys_custom, $intranet_root, $intranet_session_language;
				
				include_once($PATH_WRT_ROOT."includes/libcal.php");
				include_once($PATH_WRT_ROOT."includes/libcalevent.php");
				include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
				$lcalevent = new libcalevent2007();
				
				if ($sys_custom['eBooking']['EverydayTimetable']) {
				    include_once($PATH_WRT_ROOT."includes/libebooking.php");
				    $libebooking = new libebooking();
				}
				
				## calculate the num of day different between start date and end date
				$NumOfDay = round( abs(strtotime($EventEndDate)-strtotime($EventDate)) / 86400, 0 );
				
				if($TargetGroupID != "")
				{
					$GroupID_Arr = explode(",",$TargetGroupID);
				}
				
				/*
				$cater_skip = 1;
				if(sizeof($NumOfDay)==1)	$cater_skip=0;
				if(sizeof($NumOfDay)==2)
				{
					# check the first day is sat or not
					# sat > skip check isSkip
					
					$startDayWeek = date("w",$ts_TargetEventDate);
					if($startDayWeek==6)	$cater_skip=0;
				}	
				*/
				if($sys_custom['eClassApp']['SFOC']){

				    $SFOCFields = ",EventAssociation,EventStartTime,EventEndTime,Email,ContactNo,Website,EventStatus, EventAssociationEng";
				    $SFOCValues = ",'".$this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($EventAssociation))))."'
                                   ,'".$this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($EventStartTime))))."'
                                   ,'".$this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($EventEndTime))))."'
                                   ,'".$this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($EventEmail))))."'
                                   ,'".$this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($EventContact))))."'
                                   ,'".$this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($EventWebsite))))."'
                                   ,'".$EventToBeDetermined."'
                                   ,'".$this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($EventAssociationEng))))."'";
				}
				
				$eventIdAry = array();
				for($i=0; $i<=$NumOfDay; $i++) {
					## Calculate the Event Target Date
					$ts_EventStartDate = strtotime($EventDate);
					$ts_TargetEventDate = $ts_EventStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
					$TargetEventDate = date("Y-m-d",$ts_TargetEventDate);
					$Title = trim(stripslashes(urldecode($Title)));
					$EventVenue = trim(stripslashes(urldecode($EventVenue)));
					$EventNature = trim(stripslashes(urldecode($EventNature)));
					$EventNatureEng= trim(stripslashes(urldecode($EventNatureEng)));
					$Description = trim(stripslashes(urldecode($Description)));
					$DescriptionEng = trim(stripslashes(urldecode($DescriptionEng)));
					$thisDayWeek = date("w",$ts_TargetEventDate);
										
					//if($cater_skip)
					//{	
						# check isSkipSAT
						if($isSkipSAT && $thisDayWeek==6)	continue;
						
						# check isSkipSUN
						if($isSkipSUN && $thisDayWeek==0)	continue;
					//}
					
					$sql = "INSERT INTO 
								INTRANET_EVENT 
								(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,EventNatureEng,Description,DescriptionEng,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy,isSkipSAT,isSkipSUN,ClassGroupID , TitleEng,  EventVenueEng $SFOCFields)
							VALUES
								('$TargetEventDate',$RecordType,'".
					           $this->Get_Safe_Sql_Query($Title)."','$EventLocationID','".
					           $this->Get_Safe_Sql_Query($EventVenue)."','".
					           $this->Get_Safe_Sql_Query($EventNature)."','".
					           $this->Get_Safe_Sql_Query($EventNatureEng)."','".
					           $this->Get_Safe_Sql_Query($Description)."','".
					           $this->Get_Safe_Sql_Query($DescriptionEng)."',
					           $isSkipCycle,$RecordStatus,NOW(),$UserID,NOW(),$UserID,$isSkipSAT,$isSkipSUN,'$ClassGroupID', '".
                               $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($EventTitleEng)))). "','".
                               $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($EventVenueEng)))). "'$SFOCValues)";
                               $this->db_db_query($sql);
					$CurrentEventID = $this->db_insert_id();
					$ParentEventID = $ParentEventID ? $ParentEventID : $CurrentEventID;
					$eventIdAry[] = $CurrentEventID;
					
					/*
					if($i==0) {
						$ParentEventID = $this->db_insert_id();
						$CurrentEventID = $this->db_insert_id();
					} else {
						$CurrentEventID = $this->db_insert_id();
					}
					*/
					
					
					if(sizeof($GroupID_Arr)>0){
						$value = "";
						$delimiter = "";
						for($j=0; $j<sizeof($GroupID_Arr); $j++){		
							$value .= $delimiter."($GroupID_Arr[$j],$CurrentEventID,NOW(),NOW())";
							$delimiter = ",";
						}
						
						$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
						$this->db_db_query($sql);
					}
					
					$sql = "UPDATE INTRANET_EVENT 
							SET 
								RelatedTo = $ParentEventID,
								DateModified = NOW(),
								ModifyBy = $UserID
							WHERE 
								EventID = $CurrentEventID";
					$result['createSchoolHolidayEvent'] = $this->db_db_query($sql);
					
					// Skip School Day and using EverydayTimetable, reject related bookings and delete record in EverydayTimetable
					if ($sys_custom['eBooking']['EverydayTimetable'] && $isSkipCycle) {
					    $timeSlotIDAry = $this->getTimeSlotByDate($TargetEventDate);
					    if (count($timeSlotIDAry)) {
					        foreach($timeSlotIDAry as $_timeSlotIDAry) {
					            $_timeSlotID = $_timeSlotIDAry['TimeSlotID'];
					            $result['rejectBooking_timeSlotID_'.$_timeSlotID] = $libebooking->rejectBookingByTimeSlot($_timeSlotID, $TargetEventDate);
					        }
					    }
					 
					    $sql = "DELETE FROM INTRANET_SCHOOL_EVERYDAY_TIMETABLE WHERE RecordDate='".$TargetEventDate."'";
					    $result['deleteRecordInEverydayTimetable'] = $this->db_db_query($sql);
					}
				}
				$this->generatePreview();
				$this->generateProduction();
				
				// Re-generate the Special Timetable Settings
				if ($isSkipCycle && $this->Has_Special_Timetable_Settings_With_Cycle_Day($EventDate, $EventEndDate)) {
					$result['Regenerate_Special_Timetable_Settings'] = $this->ReGenerate_Special_Timetable_Date_By_Date_Range($EventDate, $EventEndDate);
				}
				
				$result['Syn_Event_To_Modules'] = $lcalevent->synEventToModules($eventIdAry);
				
				return $result;
		}
				
		function retriveEventsByDate($TargetDate)
		{
				$sql = "SELECT EventID, Title, RecordType, isSkipCycle FROM INTRANET_EVENT WHERE EventDate = '$TargetDate' ORDER BY RecordType DESC";
				$result = $this->returnArray($sql,4);
				
				return $result;
		}
		
		function updateSchoolHolidayEvent($EventID,$RecordType,$TargetGroupID='',$Title,$EventLocationID,$EventVenue='',$EventNature='',$Description='',$isSkipCycle,$RecordStatus,$ApplyToRelatedEvents, $isSkipSAT, $isSkipSUN, $ClassGroupID='',
		   $EventAssociation='',$EventStartTime='',$EventEndTime='',$EventEmail='',$EventContact='',$EventWebsite='',$EventToBeDetermined='', $TitleEng = '', $EventVenueEng = '',$EventNatureEng = '',$DescriptionEng = '')
		{
		    global $UserID, $PATH_WRT_ROOT, $sys_custom, $intranet_root, $intranet_session_language;

			include_once($PATH_WRT_ROOT."includes/libcal.php");
			include_once($PATH_WRT_ROOT."includes/libcalevent.php");
			include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
			$lcalevent = new libcalevent2007();
			
			if($TargetGroupID != "")
			{
				$GroupID_Arr = explode(",",$TargetGroupID);
			}
			

			$Title = trim(stripslashes(urldecode($Title)));
			$TitleEng = trim(stripslashes(urldecode($TitleEng)));
			$EventVenue = trim(stripslashes(urldecode($EventVenue)));
			$EventVenueEng = trim(stripslashes(urldecode($EventVenueEng)));
			$EventNature = trim(stripslashes(urldecode($EventNature)));
			$Description = trim(stripslashes(urldecode($Description)));
			$EventNatureEng = trim(stripslashes(urldecode($EventNatureEng)));
			$DescriptionEng = trim(stripslashes(urldecode($DescriptionEng)));
			
			$values = "";
			$delimiter = "";
			if($ApplyToRelatedEvents == 0) {
				$targetEventIDs = $EventID;
				for($i=0; $i<sizeof($GroupID_Arr); $i++)
				{
					$values .= $delimiter."($GroupID_Arr[$i],$targetEventIDs,NOW(),NOW())";
					$delimiter = ",";
				}
			}else{
				$arrTargetEventIDs = $this->getAllRelatedEvents($EventID);
				$targetEventIDs = implode(",",$arrTargetEventIDs);	
				
				for($i=0; $i<sizeof($arrTargetEventIDs); $i++){
					for($j=0; $j<sizeof($GroupID_Arr); $j++){
						$values .= $delimiter."($GroupID_Arr[$j],$arrTargetEventIDs[$i],NOW(),NOW())";
						$delimiter = ",";
					}
				}
			}
			
			
			### Get Original Event Info for Special Timetable Settings
			$OriginalEventInfoArr = $this->Get_Event_Info($EventID);
			$TargetEventIDArr = explode(',', $targetEventIDs);
			
			$OrignialDateRangeInfoArr = $this->Get_Event_Date_Range($TargetEventIDArr);

			### Get Original Event Info for reject booking if change to Skip School Day
			if ($sys_custom['eBooking']['EverydayTimetable']) {
			    $originalEventInfoAry = $this->Get_Event_Info($TargetEventIDArr);
			}
			
			$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN ($targetEventIDs)";
			$this->db_db_query($sql);
			
			if($TargetGroupID != "" && $RecordType==2)
			{
				$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $values";
				$this->db_db_query($sql);
			}
			if($sys_custom['eClassApp']['SFOC']){
			    $SFOCsql = ",EventAssociation = '".$this->Get_Safe_Sql_Query($EventAssociation)."',
                            EventStartTime = '".$this->Get_Safe_Sql_Query($EventStartTime)."',
                            EventEndTime = '".$this->Get_Safe_Sql_Query($EventEndTime)."',
                            Email = '".$this->Get_Safe_Sql_Query($EventEmail)."',
                            ContactNo = '".$this->Get_Safe_Sql_Query($EventContact)."',
                            Website = '".$this->Get_Safe_Sql_Query($EventWebsite)."',
                            EventStatus = '$EventToBeDetermined'";		    
			}else{
			    $SFOCsql = "";
			}

			$sql = "UPDATE 
							INTRANET_EVENT
						SET 
							RecordType = $RecordType,
							Title = '".$this->Get_Safe_Sql_Query($Title)."',
                            TitleEng = '".$this->Get_Safe_Sql_Query($TitleEng)."',
							EventLocationID = '$EventLocationID',
							EventVenue = '".$this->Get_Safe_Sql_Query($EventVenue)."',
                            EventVenueEng = '".$this->Get_Safe_Sql_Query($EventVenueEng)."',
							EventNature = '".$this->Get_Safe_Sql_Query($EventNature)."',
							EventNatureEng = '".$this->Get_Safe_Sql_Query($EventNatureEng)."',
							Description = '".$this->Get_Safe_Sql_Query($Description)."',
                            DescriptionEng = '".$this->Get_Safe_Sql_Query($DescriptionEng)."',
							isSkipCycle = $isSkipCycle,
							isSkipSAT = $isSkipSAT,
							isSkipSUN = $isSkipSUN,
							ClassGroupID = '$ClassGroupID',
							RecordStatus = $RecordStatus,
							DateModified = NOW(),
                            ModifyBy = ". $UserID ."
                            $SFOCsql
						WHERE
							EventID IN ($targetEventIDs)";

			$result['updateSchoolHolidayEvent'] = $this->db_db_query($sql);
			
			
			# found out RelatedTo ID
			$sql = "select min(RelatedTo) from INTRANET_EVENT where EventID in ($targetEventIDs)";
			$rs = $this->returnVector($sql);
			$RelatedTo = $rs[0];
		
			/*
			$cater_skip = 1;
			
			if(sizeof($TargetEventIDArr)==1)	$cater_skip=0;
			if(sizeof($TargetEventIDArr)==2)
			{
				$cater_skip = 0;
				# check the first day is sat or not
				# sat > skip check isSkip
				
				# retrieve event date within the period
				$sql = "select EventID, EventDate from INTRANET_EVENT where EventID in ($targetEventIDs)";
				$rs1 = $this->returnArray($sql);
				foreach($rs1 as $k=>$d)
				{
					list($thisEventID, $thisEventDate) = $d;
					
					$ts_TargetEventDate = strtotime($thisEventDate);
					$thisDayWeek = date("w",$ts_TargetEventDate);
					if(!($thisDayWeek==6 || $thisDayWeek==0))	$cater_skip=1;
				}
			}	
			*/
				
			# check isSkipSAT and isSkipSUN
			//if($cater_skip && ($isSkipSAT || $isSkipSUN))
			if(($isSkipSAT || $isSkipSUN))
			{	
				# retrieve event date within the period
				$sql = "select EventID, EventDate from INTRANET_EVENT where EventID in ($targetEventIDs)";
				$rs1 = $this->returnArray($sql);
				
				foreach($rs1 as $k=>$d)
				{
					list($thisEventID, $thisEventDate) = $d;
					
					$ts_TargetEventDate = strtotime($thisEventDate);
					$thisDayWeek = date("w",$ts_TargetEventDate);
					if(!($thisDayWeek==6 or $thisDayWeek==0))	continue;
					
					# check skip SAT / SUN
					if( ($isSkipSAT && $thisDayWeek==6) || ($isSkipSUN && $thisDayWeek==0) ) 
					{
						$sql = "DELETE FROM INTRANET_EVENT WHERE EventID = $thisEventID";
						$this->db_db_query($sql);
						
						$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID = $thisEventID";
						$this->db_db_query($sql);
					}
				}
			}
			
			# check not skip SAT / SUN (need add back records)
			if(!$isSkipSAT || !$isSkipSUN)
			{
				$sql = "select min(EventDate), max(EventDate) from INTRANET_EVENT where EventID in ($targetEventIDs)";
				$rs = $this->returnArray($sql);
				list($startdate, $enddate) = $rs[0];
				$NumOfDay = round( abs(strtotime($enddate)-strtotime($startdate)) / 86400, 0 );
				
				for($i=0; $i<=$NumOfDay; $i++) 
				{
					## Calculate the Event Target Date
					$ts_EventStartDate = strtotime($startdate);
					$ts_TargetEventDate = $ts_EventStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
					$TargetEventDate = date("Y-m-d",$ts_TargetEventDate);
					$thisDayWeek = date("w",$ts_TargetEventDate);
					if(!($thisDayWeek==6 or $thisDayWeek==0))	continue;
					
					if( (!$isSkipSAT && $thisDayWeek==6) || (!$isSkipSUN && $thisDayWeek==0) ) 
					{
						# check $TargetEventDate is exists or not, exists > ignore, no exists > add record
						$sql = "select EventID from INTRANET_EVENT where RelatedTo=$RelatedTo and DATE_FORMAT(EventDate, '%Y-%m-%d')='$TargetEventDate'";
						$rs = $this->returnVector($sql);
						if(!empty($rs[0]))	continue;
						
						# add record
						$sql = "INSERT INTO 
									INTRANET_EVENT 
									(EventDate,RecordType,Title,EventLocationID,EventVenue,EventNature,Description,isSkipCycle,RecordStatus,DateInput,InputBy,DateModified,ModifyBy,isSkipSAT,isSkipSUN, RelatedTo, ClassGroupID, TitleEng, EventVenueEng,
                                    EventNature,Description)
								VALUES
									('$TargetEventDate',$RecordType,'".$this->Get_Safe_Sql_Query($Title)."','$EventLocationID','".$this->Get_Safe_Sql_Query($EventVenue)."','".$this->Get_Safe_Sql_Query($EventNature)."','".$this->Get_Safe_Sql_Query($Description)."',$isSkipCycle,$RecordStatus,NOW(),$UserID,NOW(),$UserID,$isSkipSAT,$isSkipSUN,$RelatedTo,'$ClassGroupID','".$this->Get_Safe_Sql_Query($TitleEng)."','".$this->Get_Safe_Sql_Query($EventVenueEng)."',
                                    '".$this->Get_Safe_Sql_Query($EventNature)."','".$this->Get_Safe_Sql_Query($Description)."')";
						$this->db_db_query($sql);
						$thisEventID = $this->db_insert_id();
						
						if($TargetGroupID != "")
						{
							for($j=0; $j<sizeof($GroupID_Arr); $j++)
							{
								$values .= $delimiter."($GroupID_Arr[$j],$thisEventID,NOW(),NOW())";
								$delimiter = ",";
							}
								
							$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $values";
							$this->db_db_query($sql);
						}
					}
				}
			}
			
			# double check the RelatedTo data
			$sql = "select min(EventID) from INTRANET_EVENT where RelatedTo=$RelatedTo";
			$rs = $this->returnVector($sql);
			$new_RelatedTo = $rs[0];
			$sql = "update INTRANET_EVENT set RelatedTo=$new_RelatedTo where RelatedTo=$RelatedTo";
			$this->db_db_query($sql);
			
			
			$this->generatePreview();
			$this->generateProduction();
			
			
			### Re-generate the Special Timetable Settings if the Skip Cycle Settings has been changed
			$OriginalIsSkipCycle = $OriginalEventInfoArr[0]['isSkipCycle'];
			$EventStartDate = $OrignialDateRangeInfoArr[0]['StartDate'];
			$EventEndDate = $OrignialDateRangeInfoArr[0]['EndDate'];
			if ($isSkipCycle != $OriginalIsSkipCycle && $this->Has_Special_Timetable_Settings_With_Cycle_Day($EventStartDate, $EventEndDate)) {
				$result['Regenerate_Special_Timetable_Settings'] = $this->ReGenerate_Special_Timetable_Date_By_Date_Range($EventStartDate, $EventEndDate);
			}
			
			$result['Syn_Event_To_Modules'] = $lcalevent->synEventToModules(explode(',', $targetEventIDs));

			if ($sys_custom['eBooking']['EverydayTimetable']) {
			    include_once($intranet_root."/includes/libebooking.php");
			    $libebooking = new libebooking();
			    
			    foreach($originalEventInfoAry as $_originalEventInfoAry) {
			        $_eventID = $_originalEventInfoAry['EventID'];
			        $_isSkipCycle = $_originalEventInfoAry['isSkipCycle'];
			        $_eventDate = substr($_originalEventInfoAry['EventDate'],0,10);
			        if ($isSkipCycle && !$_isSkipCycle) {    // change from not skip cycle to skip cycle
			            $timeSlotIDAry = $this->getTimeSlotByDate($_eventDate);
			            if (count($timeSlotIDAry)) {
			                foreach($timeSlotIDAry as $_timeSlotIDAry) {
			                    $_timeSlotID = $_timeSlotIDAry['TimeSlotID'];
			                    $result['rejectBooking_timeSlotID_'.$_timeSlotID] = $libebooking->rejectBookingByTimeSlot($_timeSlotID, $_eventDate);
			                }
			            }
			        }
			        else if (!$isSkipCycle && $_isSkipCycle) {   // change from skip cycle to not skip cycle, need to set default timetable
			            $timetableID = $this->getDefaultTimetableIDFromTimezone($_eventDate);
			            if ($timetableID) {
    			            $defaultTimetableAry = $this->getDefaultTimetableInfo($_eventDate, $_eventDate);
    			            if (count($defaultTimetableAry) == 0) {  // not set yet
    			                $result['insertTimetable'] = $this->insertEverydayDefaultTimetable($timetableID, $_eventDate);
    			            }
			            }
                        $result['rejectBooking_by_date_'.$_eventDate] = $libebooking->rejectBookingByDate($_eventDate);
			        }
			    }
			}

			return $result;
		}
		
		function getAllRelatedEvents($EventID)
		{
		    $sql = "SELECT a.EventID FROM INTRANET_EVENT AS a WHERE a.EventID = '".IntegerSafe($EventID)."' OR a.RelatedTo = (SELECT ie.RelatedTo FROM INTRANET_EVENT AS ie WHERE ie.EventID = '".IntegerSafe($EventID)."')";
				$result = $this->returnVector($sql);
								
				return $result;
		}
		
		function deleteSchoolHolidayEvent($EventID,$DeleteRelatedEvent)
		{
		    global $UserID, $PATH_WRT_ROOT, $sys_custom, $intranet_root, $intranet_session_language;
				
				include_once($PATH_WRT_ROOT."includes/libcal.php");
				include_once($PATH_WRT_ROOT."includes/libcalevent.php");
				include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
				$lcalevent = new libcalevent2007();
				
				if($DeleteRelatedEvent == 1) {
					$arrTargetEventIDs = $this->getAllRelatedEvents($EventID);
					$targetEventIDs = implode(",",$arrTargetEventIDs);
				}else{
					$targetEventIDs = $EventID;
					
					### check if the target EventID is a parent event or not
					$sql = "SELECT COUNT(*) FROM INTRANET_EVENT WHERE RelatedTo = $targetEventIDs";
					$tmp_result = $this->returnVector($sql);
					if(sizeof($tmp_result)>0)
					{
						## get a new parent event ID
						$sql = "SELECT EventID, EventDate FROM INTRANET_EVENT WHERE RelatedTo = $targetEventIDs ORDER BY EVENTDATE ASC LIMIT 1,1";
						$tmp_result = $this->returnVector($sql);
						
						$newParentEventID = $tmp_result[0];
						## update all the related event ID to a new parent event ID
						$sql = "UPDATE 
									INTRANET_EVENT 
								SET 
									RelatedTo = $newParentEventID,
									DateModified = now(),
									ModifyBy = $UserID
								WHERE RelatedTo = $targetEventIDs";
						$this->db_db_query($sql);
					}
				}
				
				### Get Event Date Range for Special Timetable handling
				$OriginalEventInfoArr = $this->Get_Event_Info($EventID);
				$TargetEventIDArr = explode(',', $targetEventIDs);
				$OrignialDateRangeInfoArr = $this->Get_Event_Date_Range($TargetEventIDArr);
				
				if ($sys_custom['eBooking']['EverydayTimetable']) {
				    $originalEventInfoAry = $this->Get_Event_Info($TargetEventIDArr);
				}
				
				$sql = "DELETE FROM INTRANET_EVENT WHERE EventID IN ($targetEventIDs)";
				$result['deleteSchoolHolidayEvent'] = $this->db_db_query($sql);
				
				$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE SchoolEventID IN ($targetEventIDs)";
				$result['deleteRelationship'] = $this->db_db_query($sql);
				
				# 2012-09-12 YatWoon
				$sql = "DELETE FROM INTRANET_GROUPEVENT WHERE EventID IN ($targetEventIDs)";
				$this->db_db_query($sql);
				
				$this->generatePreview();
				$this->generateProduction();
				
				
				### Re-generate the Special Timetable Settings
				$isSkipCycle = $OriginalEventInfoArr[0]['isSkipCycle'];
				$EventStartDate = $OrignialDateRangeInfoArr[0]['StartDate'];
				$EventEndDate = $OrignialDateRangeInfoArr[0]['EndDate'];
				if ($isSkipCycle && $this->Has_Special_Timetable_Settings_With_Cycle_Day($EventStartDate, $EventEndDate)) {
					$result['Regenerate_Special_Timetable_Settings'] = $this->ReGenerate_Special_Timetable_Date_By_Date_Range($EventStartDate, $EventEndDate);
				}
				
				$result['Syn_Event_To_Modules'] = $lcalevent->synEventToModules(explode(',', $targetEventIDs));
				
				if ($sys_custom['eBooking']['EverydayTimetable']) {
				    include_once($intranet_root."/includes/libebooking.php");
				    $libebooking = new libebooking();
				    
				    foreach($originalEventInfoAry as $_originalEventInfoAry) {
				        $_eventID = $_originalEventInfoAry['EventID'];
				        $_isSkipCycle = $_originalEventInfoAry['isSkipCycle'];
				        $_eventDate = substr($_originalEventInfoAry['EventDate'],0,10);
				        if ($_isSkipCycle) { 
				            $timetableID = $this->getDefaultTimetableIDFromTimezone($_eventDate);
				            if ($timetableID) {
				                $defaultTimetableAry = $this->getDefaultTimetableInfo($_eventDate, $_eventDate);
				                if (count($defaultTimetableAry) == 0) {  // not set yet
				                    $result['insertTimetable'] = $this->insertEverydayDefaultTimetable($timetableID, $_eventDate);
				                }
				            }
				            
				            $result['rejectBooking_'.$_eventDate] = $libebooking->rejectBookingByDate($_eventDate);
				        }
				    }
				    
				}
				return $result;
		}
		
		function getLastPeriodID()
		{
				$sql = "SELECT MAX(PeriodID) FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE RecordStatus = 1";
				$periodID = $this->returnVector($sql);
				
				return $periodID[0];
		}
		
		function createPeriod($PeriodStart,$PeriodEnd,$PeriodType,$CycleType,$PeriodDays,$FirstDay,$SatCount,$ColorCode,$TimetableTemplate,$SkipOnWeekday)
		{
		    global $UserID, $sys_custom;
			# insert the date range into DB #
			$sql = "INSERT INTO 
							INTRANET_CYCLE_GENERATION_PERIOD 
							(PeriodStart, PeriodEnd, PeriodType, CycleType, PeriodDays, FirstDay, SaturdayCounted, SkipOnWeekday, ColorCode, DateInput, InputBy, DateModified, ModifyBy) 
					VALUES 
							('$PeriodStart','$PeriodEnd','$PeriodType','$CycleType','$PeriodDays','$FirstDay','$SatCount', '$SkipOnWeekday', '$ColorCode',NOW(),$UserID,NOW(),$UserID)";
			$result['NewPeriod'] = $this->db_db_query($sql);
			
			### generate Preview ###
			$this->generatePreview();
			$this->generateProduction();
			
			### Get the insert PeriodID
			$period_id = $this->getLastPeriodID();
			if($TimetableTemplate != '') {
				$sql = "INSERT INTO 
								INTRANET_PERIOD_TIMETABLE_RELATION
								(PeriodID, TimetableID, DateInput, DateModified)
						VALUES
								($period_id,$TimetableTemplate,NOW(),NOW())";
				$this->db_db_query($sql);
				
				if ($sys_custom['eBooking']['EverydayTimetable']) {
				    $this->addEverydayTimetableRecord($PeriodStart,$PeriodEnd,$PeriodType,$PeriodDays,$FirstDay,$SkipOnWeekday,$TimetableTemplate);
				}
			}
			return $result;
		}
		
		function editPeriod($PeriodID,$PeriodStart,$PeriodEnd,$PeriodType,$CycleType,$PeriodDays,$FirstDay,$SatCount,$ColorCode,$TimetableTemplate,$SkipOnWeekday)
		{
		    global $UserID, $sys_custom;
			
		    if ($sys_custom['eBooking']['EverydayTimetable']) {
		        $oldPeriodAry = $this->returnPeriods_NEW($PeriodID);
		        if (count($oldPeriodAry)) {
		            $oldPeriodAry = $oldPeriodAry[0];
		            $oldDateStart = $oldPeriodAry['PeriodStart'];
		            $oldDateEnd = $oldPeriodAry['PeriodEnd'];
		        }
		        else {
		            $oldDateStart = $PeriodStart;
		            $oldDateEnd = $PeriodEnd;
		        }
		    }
		    
			### Delete the Special Timetable Settings if the date is not in the new Time Zone date range
			$SpecialDateAssoArr = $this->Get_Special_Timetable_Settings_Date_Info($SpecialTimetableSettingsIDArr='', $PeriodID);
			$SpecialDateArr = array();
			
			foreach ((array)$SpecialDateAssoArr as $thisDate => $thisDateInfoArr) {
				// Remove the date outside the new time zone date range only
				if ($thisDate < $PeriodStart || $thisDate > $PeriodEnd) {
					$SpecialDateArr[] = $thisDate;
				}
			}
			$result['DeleteSpecialTimetableSettingsOutsideNewDateRange'] = $this->Delete_Special_Timetable_Date($SpecialDateArr);
			
			### Edit Time Zone Info
			$sql = "UPDATE 
							INTRANET_CYCLE_GENERATION_PERIOD 
					SET 
							PeriodStart = '$PeriodStart',
							PeriodEnd = '$PeriodEnd',
							PeriodType = '$PeriodType',
							CycleType = '$CycleType',
							PeriodDays = '$PeriodDays', 
							FirstDay = '$FirstDay', 
							SaturdayCounted = '$SatCount',
							SkipOnWeekday = '$SkipOnWeekday',
							ColorCode = '$ColorCode',
							DateModified = NOW(),
							ModifyBy = '$UserID'
					WHERE
							PeriodID = '$PeriodID'";
			$result['EditPeriod'] = $this->db_db_query($sql);

			## Generate perview			
			$this->generatePreview();
			$this->generateProduction();
			
			if($TimetableTemplate != '') {
				$sql = "INSERT INTO 
							INTRANET_PERIOD_TIMETABLE_RELATION
							(PeriodID, TimetableID, DateInput, DateModified)
						VALUES
							('$PeriodID','$TimetableTemplate',NOW(),NOW())
						ON DUPLICATE KEY UPDATE
								TimetableID = $TimetableTemplate,
								DateModified = NOW()";
				$result['EditTimetableRelation'] = $this->db_db_query($sql);
				
				if ($sys_custom['eBooking']['EverydayTimetable']) {
				    
				    $result['UpdateEverydayTimetable'] = $this->updateEverydayTimetableRecord($PeriodStart,$PeriodEnd,$PeriodType,$PeriodDays,$FirstDay,$SkipOnWeekday,$TimetableTemplate,$oldDateStart,$oldDateEnd,$PeriodID);
				}
				
			}
			return $result;
		}
		
		function deletePeriod($PeriodID)
		{
		    global $sys_custom, $intranet_root, $intranet_session_language;
		    
		    if ($sys_custom['eBooking']['EverydayTimetable']) {
		        include_once($intranet_root."/includes/libebooking.php");
		        $libebooking = new libebooking();
		        
		        $sql = "SELECT PeriodStart, PeriodEnd FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE PeriodID IN ('".implode("','",(array)$PeriodID)."')";
		        $periodAry = $this->returnResultSet($sql);
		    }
			$sql = "DELETE FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE PeriodID IN ($PeriodID)";
			$this->db_db_query($sql);
			
			## Generate perview			
			$this->generatePreview();
			$this->generateProduction();
			
			$sql = "DELETE FROM INTRANET_PERIOD_TIMETABLE_RELATION WHERE PeriodID IN ($PeriodID)";
			$result['DeletePeriod'] = $this->db_db_query($sql);
			
			## Delete Special Timetable Settings of this Timezone
			$SpecialTimetableInfoArr = $this->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsIDArr='', $PeriodID);
			$SpecialTimetableSettingsIDArr = array_keys($SpecialTimetableInfoArr);

			$result['DeleteSpecialTimetableSettings'] = $this->Delete_Special_Timetable_Settings($SpecialTimetableSettingsIDArr);
			
			if ($sys_custom['eBooking']['EverydayTimetable']) {
			    if (count($periodAry)) {
			        foreach((array)$periodAry as $_periodAry) {
			            $_periodStart = $_periodAry['PeriodStart'];
			            $_periodEnd = $_periodAry['PeriodEnd'];
			            $defaultTimetableAssoc = $this->getDefaultTimetableID($_periodStart,$_periodEnd);
			            
			            $ts_start = strtotime($_periodStart);
			            $ts_end = strtotime($_periodEnd);
			            $ts_current = $ts_start;
			            while ($ts_current <= $ts_end) {
			                $recordDate = date("Y-m-d",$ts_current);
			                
			                if (isset($defaultTimetableAssoc[$recordDate])) {
			                    $timeSlotIDAry = $this->getDefaultTimeSlotByDate($recordDate);
			                    if (count($timeSlotIDAry)) {
			                        foreach($timeSlotIDAry as $_timeSlotIDAry) {
			                            $_timeSlotID = $_timeSlotIDAry['TimeSlotID'];
			                            $result['rejectBooking_timeSlotID_'.$_timeSlotID] = $libebooking->rejectBookingByTimeSlot($_timeSlotID, $recordDate);
			                        }
			                    }
			                    $deleteResult[] = $this->deleteEverydayDefaultTimetableID($recordDate,$defaultTimetableAssoc[$recordDate]);     // delete old TimetableID
			                }
			                
			                $ts_current += 86400;  # 1 Day
			            }
			        }
			    }
			}
			
			return $result;
		}
		
		function updateAcademicYearTitle($RecordType, $AcademicYearID, $UpdatedTitle)
		{
			global $UserID;
			$UpdatedTitle = trim(stripslashes(urldecode($UpdatedTitle)));
			
//			if($RecordType == 'Chinese'){
//				$sql = "UPDATE 
//							ACADEMIC_YEAR 
//						SET 
//							YearNameB5 = '".$this->Get_Safe_Sql_Query($UpdatedTitle)."',
//							DateModified = NOW(),
//							ModifyBy = $UserID
//						WHERE 
//							AcademicYearID = $AcademicYearID";
//				$result['updateAcademicYearTitle'] = $this->db_db_query($sql);
//			}else if($RecordType == 'English'){
//				$sql = "UPDATE 
//							ACADEMIC_YEAR 
//						SET 
//							YearNameEN = '".$this->Get_Safe_Sql_Query($UpdatedTitle)."',
//							DateModified = NOW(),
//							ModifyBy = $UserID
//						WHERE 
//							AcademicYearID = $AcademicYearID";
//				$result['updateAcademicYearTitle'] = $this->db_db_query($sql);
//			}else{
//			}

			if ($RecordType == 'Chinese' || $RecordType == 'English') {
				$YearNameDBField = ($RecordType == 'Chinese')? 'YearNameB5' : 'YearNameEN';
				
				$sql = "UPDATE 
							ACADEMIC_YEAR 
						SET 
							$YearNameDBField = '".$this->Get_Safe_Sql_Query($UpdatedTitle)."',
							DateModified = NOW(),
							ModifyBy = '".$UserID."'
						WHERE 
							AcademicYearID = '".$AcademicYearID."'
						";
				$result['updateAcademicYearTitle'] = $this->db_db_query($sql);
			}
			else {
				
			}
			return $result;
		}
		
		function Get_Update_Display_Order_Arr($OrderText, $Separator=",", $Prefix="")
		{
			if ($OrderText == "")
				return false;
				
			$orderArr = explode($Separator, $OrderText);
			$orderArr = array_remove_empty($orderArr);
			$orderArr = Array_Trim($orderArr);
			
			$numOfOrder = count($orderArr);
			# display order starts from 1
			$counter = 1;
			$newOrderArr = array();
			for ($i=0; $i<$numOfOrder; $i++)
			{
				$thisID = str_replace($Prefix, "", $orderArr[$i]);
				if (is_numeric($thisID))
				{
					$newOrderArr[$counter] = $thisID;
					$counter++;
				}
			}
			
			return $newOrderArr;
		}
		
		function Update_AcademicYear_DisplayOrder($DisplayOrderArr=array())
		{
			global $UserID;
			if (count($DisplayOrderArr) == 0)
				return false;
				
			for ($i=1; $i<=sizeof($DisplayOrderArr); $i++) {
				$AcademicYearID = $DisplayOrderArr[$i];
				
				$sql = 'UPDATE ACADEMIC_YEAR SET 
									Sequence = \''.$i.'\',
                                    ModifyBy = \''.$UserID.'\'
								WHERE 
									AcademicYearID = \''.$AcademicYearID.'\'';
				$result['ReorderResult'.$i] = $this->db_db_query($sql);
			}
			
			return $result;
		}
		
		function deleteAcademicYear($AcademicYearID)
		{
			$sql = "DELETE FROM ACADEMIC_YEAR WHERE AcademicYearID = $AcademicYearID";
			$result['deleteAcademicYear'] = $this->db_db_query($sql);
			
			return $result;
		}
		
		function newAcademicYear($YearNameEN, $YearNameB5)
		{
			global $UserID;
			$YearNameEN = trim(stripslashes(urldecode($YearNameEN)));
			$YearNameB5 = trim(stripslashes(urldecode($YearNameB5)));
			$sql = "INSERT INTO 
						ACADEMIC_YEAR 
						(YearNameEN, YearNameB5, Sequence, DateInput, InputBy, DateModified, ModifyBy) 
					VALUES 
						('".$this->Get_Safe_Sql_Query($YearNameEN)."', '".$this->Get_Safe_Sql_Query($YearNameB5)."', (Select MAX(a.Sequence)+1 FROM ACADEMIC_YEAR AS a), NOW(), '{$UserID}', NOW(), '{$UserID}')";

			$result['newAcademicYear'] = $this->db_db_query($sql);
			
			return $result;
		}
		
		function checkTermOverlap($YearTermID,$TermStartDate,$TermEndDate)
		{
			if($YearTermID != ""){
				$sql = "SELECT TermStart, TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID != $YearTermID";
			}else{
				$sql = "SELECT TermStart, TermEnd FROM ACADEMIC_YEAR_TERM";
			}
			$arrResult = $this->returnArray($sql,2);
	
			$term_overlapped = 0;		## init a overlap checking counter
			
			if(sizeof($arrResult)>0){
				for($i=0; $i<sizeof($arrResult); $i++){
					list($existTermStart, $existTermEnd) = $arrResult[$i];
					$existTermStart = substr($existTermStart,0,10);
					$existTermEnd = substr($existTermEnd,0,10);
					
					if((($TermStartDate <= $existTermStart) && ($existTermStart <= $TermEndDate)) && (($TermStartDate <= $existTermEnd ) && ($existTermEnd <= $TermEndDate))){
						$term_overlapped = 1;
					}
					if((($existTermStart <= $TermStartDate) && ($TermStartDate <= $existTermEnd)) && (($TermStartDate <= $existTermEnd) && ($existTermEnd <= $TermEndDate))){
						$term_overlapped = 1;
					}
					if((($TermStartDate <= $existTermStart) && ($existTermStart <= $TermEndDate)) && (($existTermStart <= $TermEndDate) && ($TermEndDate <= $existTermEnd))){
						$term_overlapped = 1;
					}
					if((($existTermStart <= $TermStartDate) && ($TermStartDate <= $existTermEnd)) && (($existTermStart <= $TermEndDate)&&($TermEndDate <= $existTermEnd))){
						$term_overlapped = 1;
					}
				}
			}
			
			return $term_overlapped;
		}
		
		function insertEditTerm($EditMode,$AcademicYearID,$YearTermID='',$TermTitleCh,$TermTitleEn,$TermStartDate,$TermEndDate)
		{
			global $UserID;
			$TermTitleEn = trim(stripslashes(urldecode($TermTitleEn)));
			$TermTitleCh = trim(stripslashes(urldecode($TermTitleCh)));
			$TermEndDate = $TermEndDate." 23:59:59";
			if($EditMode == 1){
				$sql = "UPDATE 
							ACADEMIC_YEAR_TERM 
						SET 
							YearTermNameEN = '".$this->Get_Safe_Sql_Query($TermTitleEn)."',
							YearTermNameB5 = '".$this->Get_Safe_Sql_Query($TermTitleCh)."',
							TermStart = '$TermStartDate',
							TermEnd = '$TermEndDate',
							DateModified = NOW(),
							ModifyBy = '$UserID'
						WHERE
							YearTermID = '$YearTermID'";
			}
			if($EditMode == 0){
				$sql = "INSERT INTO 
							ACADEMIC_YEAR_TERM 
							(AcademicYearID,YearTermNameEN,YearTermNameB5,TermStart,TermEnd,DateInput,InputBy,DateModified,ModifyBy) 
						VALUES
							('$AcademicYearID','".$this->Get_Safe_Sql_Query($TermTitleEn)."','".$this->Get_Safe_Sql_Query($TermTitleCh)."','$TermStartDate','$TermEndDate',NOW(),'$UserID',NOW(),'$UserID')";
			}
			$result['insertEditTerm'] = $this->db_db_query($sql);
			
			return $result;
		}
		
		function deleteTerm($YearTermID)
		{
			$sql = "DELETE FROM ACADEMIC_YEAR_TERM WHERE YearTermID = $YearTermID";
			$result['deleteTerm'] = $this->db_db_query($sql);
			
			return $result;
		}
		
		# Return the type of code existed
		# Return "" if there are no buildings, floors and rooms using the code
		function Is_Title_Existed($RecordType, $TargetID, $TargetTitle, $TargetTitleEn='')
		{
			$TargetTitle = $this->Get_Safe_Sql_Query(trim($TargetTitle));
			
			if($TargetTitleEn!="")
				$TargetTitleEn = $this->Get_Safe_Sql_Query(trim($TargetTitleEn));
			
			if ($RecordType == "English")
				$cond = " YearNameEN = '$TargetTitle' ";
			else if ($RecordType == "Chinese")
				$cond = " YearNameB5 = '$TargetTitle' ";
			else if ($RecordType == "Both")
				$cond = " (YearNameEN = '$TargetTitleEn' OR YearNameB5 = '$TargetTitle') ";
								
			if($TargetID != "")
				$cond .= " AND AcademicYearID != $TargetID ";
			
			$sql = "SELECT DISTINCT(AcademicYearID) FROM ACADEMIC_YEAR WHERE $cond";
			$academicYearArr = $this->returnVector($sql);
			
			$isExist = (count($academicYearArr)==0)? 0 : 1;
			
			return $isExist;
		}
		
		function getAcademaicYearSetttingLastUpdatedDate()
		{
			//$sql = "SELECT if(MAX(a.DateModified)>MAX(b.DateModified),MAX(a.DateModified),MAX(b.DateModified)) FROM ACADEMIC_YEAR AS a JOIN ACADEMIC_YEAR_TERM AS b";
			$sql = "SELECT MAX(DateModified) FROM ACADEMIC_YEAR_TERM";
			$result = $this->returnVector($sql);
			if(sizeof($result)>0){
				$TermLastUpdateDate = $result[0];
				$sql = "SELECT DISTINCT ".getNameFieldByLang("b.")." FROM ACADEMIC_YEAR_TERM AS a INNER JOIN INTRANET_USER AS b ON (a.ModifyBy = b.UserID) WHERE a.DateModified = '$TermLastUpdateDate'";
				$result = $this->returnVector($sql);
				$TermLastUpdatePerson = $result[0];
			}
			$sql = "SELECT MAX(DateModified) FROM ACADEMIC_YEAR";
			$result = $this->returnVector($sql);
			if(sizeof($result)>0){
				$YearLastUpdateDate = $result[0];
				$sql = "SELECT DISTINCT ".getNameFieldByLang("b.")." FROM ACADEMIC_YEAR AS a INNER JOIN INTRANET_USER AS b ON (a.ModifyBy = b.UserID) WHERE a.DateModified = '$YearLastUpdateDate'";
				$result = $this->returnVector($sql);
				$YearLastUpdatePerson = $result[0];
			}
			if($YearLastUpdateDate >= $TermLastUpdateDate){
				$ans[] = $YearLastUpdateDate;
				$ans[] = $YearLastUpdatePerson;
			}else{
				$ans[] = $TermLastUpdateDate;
				$ans[] = $TermLastUpdatePerson;
			}
			
			return $ans;
		}
		
		function checkTermIsInUse($YearTermID,$skipCheckClass='')
		{
			## Check Any Time Zone is set ##
// 			$time_zone_check = 0;
// 			$sql = "SELECT PeriodStart, PeriodEnd FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE RecordStatus = 1";
// 			$result = $this->returnArray($sql,2);
// 			if(sizeof($result)>0){
// 				for($i=0; $i<sizeof($result); $i++){
// 					list($time_zone_start, $time_zone_end) = $result[$i];
// 					$sql = "SELECT IF('$time_zone_start' BETWEEN TermStart AND TermEnd, 1, IF('$time_zone_end' BETWEEN TermStart AND TermEnd, 1, 0) ) FROM ACADEMIC_YEAR_TERM WHERE YearTermID = '$YearTermID'";
// 					$result2 = $this->returnVector($sql);
// 					if($result2[0]>0)
// 					{
// 						$time_zone_check++;
// 					}
// 				}
// 			}
// 			if($time_zone_check>0)
// 				return true;	
			

		$sql = "SELECT TermStart, TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID = '$YearTermID'";
		$result = $this->returnResultSet($sql);
		$TermStart = $result[0]['TermStart']; 
		$TermEnd = $result[0]['TermEnd'];
		
		$sql = "SELECT
					PeriodStart, PeriodEnd
				FROM INTRANET_CYCLE_GENERATION_PERIOD
				WHERE (PeriodStart BETWEEN '$TermStart' AND '$TermEnd')
				OR (PeriodEnd BETWEEN '$TermStart' AND '$TermEnd')
			";
		$result2 = $this->returnVector($sql);
		if (count($result2) > 0) {
			return true;
		}
			
					
			## Check Any Subject Term Setting In The Selected Year Term ##
			$sql = "SELECT COUNT(a.SubjectTermID) FROM SUBJECT_TERM AS a INNER JOIN ACADEMIC_YEAR_TERM AS b ON (a.YearTermID = b.YearTermID) WHERE b.YearTermID = '$YearTermID'";
			$result = $this->returnVector($sql);
			$result[0];
			if($result[0]>0)
				return true;
			
			if(!$skipCheckClass){
				## Check Any Form Class Setting In The Selected Year Term ##
				$sql = "SELECT COUNT(a.YearClassID) FROM YEAR_CLASS AS a INNER JOIN ACADEMIC_YEAR_TERM AS b ON (a.AcademicYearID = b.AcademicYearID) WHERE b.YearTermID = '$YearTermID'";
				$result = $this->returnVector($sql);
				if($result[0]>0)
					return true;
			}	
			
			## Check Any Time Table Is Set In the Selected Year Term ##
			$sql = "SELECT COUNT(a.TimetableID) from INTRANET_SCHOOL_TIMETABLE AS a INNER JOIN ACADEMIC_YEAR_TERM AS b ON (a.YearTermID = b.YearTermID) WHERE b.YearTermID = '$YearTermID'";
			$result = $this->returnVector($sql);
			if($result[0]>0)
				return true;	
			
			/*
			## Check Any Time Table Is Set In the Selected Year Term ##
			$sql = "SELECT COUNT(a.TimetableID) from INTRANET_SCHOOL_TIMETABLE AS a INNER JOIN ACADEMIC_YEAR_TERM AS b ON (a.YearTermID = b.YearTermID) WHERE b.YearTermID = '$YearTermID'";
			$result = $this->returnVector($sql);
			if($result[0]>0)
				return true;
			*/
			
			return false;
		}
		
		function IsAcademicYearSet()
		{
			$sql = "SELECT COUNT(*) FROM ACADEMIC_YEAR";
			$result = $this->returnVector($sql);
			
			if($result[0] == 0){
				return 0;
			} else {
				return 1;
			}
		}
		
		function IsCurrentDateInTerm()
		{
			$sql = "SELECT COUNT(*) FROM ACADEMIC_YEAR_TERM WHERE NOW() BETWEEN TermStart AND TermEnd";
			$result = $this->returnVector($sql);
			
			if($result[0] == 0){
				return 0;
			} else {
				return 1;
			}
		}
		
		function IsFutureTerm($YearTermID)
		{
			if ($this->IsFutureTermSqlResult == null) {
				# Get Future YearTermID
				$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE TermStart > NOW()";
				$this->IsFutureTermSqlResult = $this->returnVector($sql);
			}
			$result = $this->IsFutureTermSqlResult;
			
			
			if(in_array($YearTermID,$result)){
				return true;
			}
			else{
				return false;
			}
		}
		
		function CheckDuplicateChiTermName($AcademicYearID,$YearTermID,$TermTitleCh)
		{
			if($YearTermID != "")
			{
				$cond = " AND YearTermID != $YearTermID";
			}
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE YearTermNameB5 = '$TermTitleCh' AND AcademicYearID = $AcademicYearID $cond";
			$result = $this->returnVector($sql);
			
			if(sizeof($result)>0)
				return 1;
			else
				return 0;
		}
		
		function CheckDuplicateEngTermName($AcademicYearID,$YearTermID,$TermTitleEn)
		{
			if($YearTermID != "")
			{
				$cond = " AND YearTermID != $YearTermID";
			}
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE YearTermNameEN = '$TermTitleEn' AND AcademicYearID = $AcademicYearID $cond";
			$result = $this->returnVector($sql);
			
			if(sizeof($result)>0)
				return 1;
			else
				return 0;
		}
		
		function createTmpSchoolHolidayEventTable()
		{
			$sql = "DROP TABLE TEMP_INTRANET_EVENT";
			$this->db_db_query($sql);
			
			$sql = "CREATE TABLE IF NOT EXISTS TEMP_INTRANET_EVENT (
						TempEventID int(11) NOT NULL auto_increment,
						RowNum int(11),
						EventDate datetime,
						EventType char(2),
                        Association varchar(100),
                        AssociationEng varchar(100),
						Title varchar(255),
                        TitleEng varchar(255),
						EventVenue varchar(100),
                        EventVenueEng varchar(100),
						EventNature varchar(100),
                        EventNatureEng  varchar(100),
						Description text,
                        DescriptionEng varchar(255),
						isSkipCycle varchar(2),
						GroupID text,
						RelatedTo int(8),
						isSkipSAT varchar(2),
						isSkipSUN varchar(2),
                        Email varchar(255),
                        ContactNo varchar(255),
                        Website varchar(255),
						ClassGroupCode varchar(10),
						PRIMARY KEY (TempEventID)
					)ENGINE=InnoDB DEFAULT CHARSET=utf8";
			$this->db_db_query($sql);
		}
		
		function insertDataToTmpSchoolHolidayEventTable($csv_data)
		{
			global $sys_custom;
			$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
			
			foreach($csv_data as $row=>$value)
			{
			    if($sys_custom['eClassApp']['SFOC']){
//     			    if($isKIS) {
//     			        //list($StartDate, $EndDate, $Type, $Title, $Venue, $Nature, $Description, $SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $ClassGroupCode) = $value;
//     			        list($StartDate, $EndDate, $Type, $SportType, $Association, $AssociationEng, $Title, $TitleEng, $Venue, $VenueEng, $Nature, $Description, $SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $Email, $ContactNo, $Website, $ClassGroupCode) = $value;
//     			    }else{
    			        //list($StartDate, $EndDate, $Type, $Title, $Venue, $Nature, $Description, $SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN) = $value;
			        list($StartDate, $EndDate, $Type, $Association, $AssociationEng, $Title, $TitleEng, $Venue, $VenueEng, $Nature,$NatureEng, $Description,$DescriptionEng, $SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $Email, $ContactNo, $Website) = $value;
//     			    }
			    } else {
			        if($isKIS) {
			            list($StartDate, $EndDate, $Type, $Title, $TitleEng, $Venue, $VenueEng, $Nature,$NatureEng,$Description,$DescriptionEng,$SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN, $ClassGroupCode) = $value;
			        }else{
			            list($StartDate, $EndDate, $Type, $Title, $TitleEng, $Venue, $VenueEng, $Nature, $NatureEng, $Description,$DescriptionEng, $SkipSchoolDay, $GroupID, $SkipSAT, $SkipSUN) = $value;
			        }
			    }
				$RowNum++;
				
				$StartDate = getDefaultDateFormat($StartDate);
				$StartDate = trim($StartDate);
				$EndDate = getDefaultDateFormat($EndDate);
				$EndDate = trim($EndDate);
// 				$SportType = trim($SportType);
				$Association = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Association))));
				$AssociationEng = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($AssociationEng))));
				$Title = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Title))));
				$TitleEng = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($TitleEng))));
				$Type = trim($Type);
				$Venue = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Venue))));
				$VenueEng = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($VenueEng))));
				$Nature = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Nature))));
				$NatureEng = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($NatureEng))));
				$Description = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Description))));
				$DescriptionEng= $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($DescriptionEng))));
				$SkipSchoolDay = strtoupper(trim($SkipSchoolDay));
				$GroupID = trim($GroupID);
				$SkipSAT = strtoupper(trim($SkipSAT));
				$SkipSUN = strtoupper(trim($SkipSUN));
				$ParentEventID = "";
				$Email = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Email))));
				$ContactNo = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($ContactNo))));
				$Website = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Website))));
				$ClassGroupCode = $this->Get_Safe_Sql_Query($ClassGroupCode);
				
				if(substr($Venue, 0, 6) != 'Others' && $Venue != $VenueEng){
				    $VenueEng = $Venue;
				}
				
				$NumOfDay = round( abs(strtotime($EndDate)-strtotime($StartDate)) / 86400, 0 );
				for($i=0; $i<=$NumOfDay; $i++) {
					$ts_EventStartDate = strtotime($StartDate);
					$ts_TargetEventDate = $ts_EventStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
					$TargetEventDate = date("Y-m-d",$ts_TargetEventDate);

					# check SkipSAT and SkipSUN
					$isSkipSAT = $SkipSAT=="Y" ? 1 : 0;
					$isSkipSUN = $SkipSUN=="Y" ? 1 : 0;
					
					$cater_skip = 1;
					if($NumOfDay==0)	$cater_skip=0; 
					if($NumOfDay==1)
					{
						# check the first day is sat or not
						# sat > skip check isSkip
						$startDayWeek = date("w",$ts_TargetEventDate);
						if($startDayWeek==6)	$cater_skip=0;
					}
					if(!$cater_skip)
					{
						$isSkipSAT = 0;
						$SkipSAT = "N";
						$isSkipSUN = 0;
						$SkipSUN = "N";
					}
					$thisDayWeek = date("w",$ts_TargetEventDate);
					if( ($isSkipSAT && $thisDayWeek==6) || ($isSkipSUN && $thisDayWeek==0))
					{
						continue;
					}
					
					//$sql_value = "($RowNum, '$TargetEventDate', '$Type', '$Title', '$Venue', '$Nature', '$Description', '$SkipSchoolDay', '$GroupID', '$SkipSAT', '$SkipSUN', '$ClassGroupCode')";
					//$sql = "INSERT INTO TEMP_INTRANET_EVENT (RowNum,EventDate,EventType,Title,EventVenue,EventNature,Description,isSkipCycle,GroupID, isSkipSAT, isSkipSUN, ClassGroupCode) VALUES $sql_value ";
					$sql_value = "($RowNum, '$TargetEventDate', '$Type', '$Association', '$AssociationEng', '$Title', '$TitleEng','$Venue', '$VenueEng', '$Nature', '$NatureEng', '$Description', '$DescriptionEng', '$SkipSchoolDay', '$GroupID', '$SkipSAT', '$SkipSUN', '$Email', '$ContactNo', '$Website', '$ClassGroupCode')";
					$sql = "INSERT INTO TEMP_INTRANET_EVENT (RowNum,EventDate,EventType, Association, AssociationEng, Title, TitleEng,EventVenue, EventVenueEng, EventNature,EventNatureEng, Description, DescriptionEng, isSkipCycle,GroupID, isSkipSAT, isSkipSUN, Email, ContactNo, Website, ClassGroupCode) VALUES $sql_value ";

					$this->db_db_query($sql);
					
					$CurrentEventID = $this->db_insert_id();
					$ParentEventID = $ParentEventID ? $ParentEventID : $CurrentEventID;
					
					/*
					if($i==0) {
						$ParentEventID = $this->db_insert_id();
						$CurrentEventID = $this->db_insert_id();
					} else {
						$CurrentEventID = $this->db_insert_id();
					}
					*/
					
					$sql = "UPDATE TEMP_INTRANET_EVENT 
							SET 
								RelatedTo = $ParentEventID
							WHERE 
								TempEventID = $CurrentEventID";
					$this->db_db_query($sql);
				}
			}
		}
		
		function importDataToINTRANET_EVENT()
		{
			global $UserID, $PATH_WRT_ROOT, $sys_custom;
			
			include_once($PATH_WRT_ROOT."includes/libcal.php");
			include_once($PATH_WRT_ROOT."includes/libcalevent.php");
			include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
			$lcalevent = new libcalevent2007();
			
			$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['Class']['ClassGroupSettings'];
			if($isKIS) {
				include_once($PATH_WRT_ROOT."includes/libclassgroup.php");
				$lclassgroup = new Class_Group();
			}
			
			/*
			# rebuild Temp data, check with isSkipSAT and isSkipSUN (remove record in Temp table first)
			$sql = "SELECT TempEventID, EventDate, RelatedTo, isSkipSAT, isSkipSUN FROM TEMP_INTRANET_EVENT";
			$TempEventArr = $this->returnArray($sql);
			foreach($TempEventArr as $k => $d)
			{
				list($TempEventID, $EventDate, $RelatedTo, $isSkipSAT, $isSkipSUN) = $d;
				
				$ts_EventDate = strtotime($EventDate);
				$thisDayWeek = date("w",$ts_EventDate);
				
				if( ($isSkipSAT && $thisDayWeek==6) || ($isSkipSUN && $thisDayWeek==0))
				{
					# maybe need to recalculate RelatedTo
					if($RelatedTo == $TempEventID)	# need update RelatedTo
					{
						$sql = "select min(TempEventID) from TEMP_INTRANET_EVENT where RelatedTo=$RelatedTo and TempEventID!=$TempEventID";
						debug_pr($sql);
						$rs = $this->returnVector($sql);
						$new_RelatedTo = $rs[0];
						debug_pr($new_RelatedTo);
						$sql = "update TEMP_INTRANET_EVENT set RelatedTo=$new_RelatedTo where RelatedTo=$RelatedTo";
						debug_pr($sql);
						$this->db_db_query($sql);
					}
					
					$sql = "delete from TEMP_INTRANET_EVENT where TempEventID=$TempEventID";
					debug_pr($sql);
					$this->db_db_query($sql);
				}
				exit;
			}
			*/
			
			//$sql = "SELECT TempEventID, EventDate, EventType, Title, EventVenue, EventNature, Description, isSkipCycle, GroupID, RelatedTo, isSkipSAT, isSkipSUN, ClassGroupCode FROM TEMP_INTRANET_EVENT";
			$sql = "SELECT TempEventID, EventDate, EventType, Association, AssociationEng, Title, TitleEng, EventVenue, EventVenueEng, EventNature, EventNatureEng, Description,DescriptionEng, isSkipCycle, GroupID, RelatedTo, isSkipSAT, isSkipSUN, Email, ContactNo, Website, ClassGroupCode FROM TEMP_INTRANET_EVENT";
			$TempEventArr = $this->returnArray($sql);
			
			$eventIdAry = array();
			if(sizeof($TempEventArr)>0){
				foreach($TempEventArr as $row=>$value)
				{
					//list($TempEventID, $EventDate, $Type, $Title, $Venue, $Nature, $Description, $isSkipCycle, $GroupID, $RelatedTo, $isSkipSAT, $isSkipSUN, $ClassGroupCode) = $value;
				    list($TempEventID, $EventDate, $Type, $Association, $AssociationEng, $Title, $TitleEng, $Venue, $VenueEng, $Nature, $NatureEng, $Description,$DescriptionEng,$isSkipCycle, $GroupID, $RelatedTo, $isSkipSAT, $isSkipSUN, $Email, $ContactNo, $Website, $ClassGroupCode) = $value;
					$RoomID = "";
					$VenueLocation = "";
					
					$Title = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Title))));
					$TitleEng = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($TitleEng))));
					$Type = trim($Type);
// 					$SportType = trim($SportType);
					$Venue = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Venue))));
					$VenueEng = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($VenueEng))));
					$Nature = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Nature))));
					$NatureEng = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($NatureEng))));
					$Description = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Description))));
					$DescriptionEng= $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($DescriptionEng))));
					$SkipSchoolDay = strtoupper(trim($SkipSchoolDay));
					$GroupID = trim($GroupID);
					$isSkipSAT = strtoupper(trim($isSkipSAT));
					$isSkipSUN = strtoupper(trim($isSkipSUN));
					$Email = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Email))));
					$ContactNo = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($ContactNo))));
					$Website = $this->Get_Safe_Sql_Query(trim(stripslashes(urldecode($Website))));
					$ClassGroupCode = $this->Get_Safe_Sql_Query($ClassGroupCode);
					
					$Venue = trim($Venue);
					$location_type = substr($Venue, 0, strpos($Venue,">"));
								
					if(strtoupper($location_type) != "OTHERS")
					{
						$sql = "SELECT BuildingID FROM INVENTORY_LOCATION_BUILDING WHERE Code = '$location_type'";
						$result = $this->returnVector($sql);
						if(sizeof($result)>0){
							$BuildingCode = $location_type;
							$Venue2 = substr($Venue,strpos($Venue,">")+1,strlen($Venue));
							$FloorCode = substr($Venue2,0,strpos($Venue2,">"));
							$RoomCode = substr($Venue2,strpos($Venue2,">")+1,strlen($Venue));
							
							$sql = "SELECT LocationID FROM INVENTORY_LOCATION AS room INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (room.LocationLevelID = floor.LocationLevelID) WHERE floor.Code = '$FloorCode' AND room.Code = '$RoomCode'";
							$result = $this->returnVector($sql);
							if(sizeof($result)>0){
								$RoomID = $result[0];
							}
						}
					} else {
						$VenueLocation = substr($Venue, strpos($Venue,">")+1, strlen($Venue));
						$VenueEng = substr($VenueEng, strpos($VenueEng,">")+1, strlen($VenueEng));
					}
					
					if($isSkipCycle == "Y")
						$isSkipCycle = 1;
					else
						$isSkipCycle = 0;
						
					if($isSkipSAT == "Y")
						$isSkipSAT = 1;
					else
						$isSkipSAT = 0;
					
					if($isSkipSUN == "Y")
						$isSkipSUN = 1;
					else
						$isSkipSUN = 0;
						
					switch ($Type) {
						case "SE": $TypeNum = 0; break;
						case "AE": $TypeNum = 1; break;
						case "GE": $TypeNum = 2; break;
						case "PH": $TypeNum = 3; break;
						case "SH": $TypeNum = 4; break;
					}
					
					if($isKIS && $ClassGroupCode != '') {
						$ClassGroupID = $lclassgroup->Get_Class_Group_ID_By_Code($ClassGroupCode);
						/*$sql = "INSERT INTO INTRANET_EVENT 
										(Title, Description, EventDate, EventLocationID, EventVenue, EventNature, isSkipCycle, RecordType, RecordStatus, DateInput, InputBy, DateModified, ModifyBy, isSkipSAT, isSkipSUN, ClassGroupID) 
								VALUES 
										('$Title','$Description','$EventDate','$RoomID','$VenueLocation','$Nature',$isSkipCycle,$TypeNum,1,NOW(),$UserID,NOW(),$UserID, $isSkipSAT, $isSkipSUN, '$ClassGroupID')";*/
						$sql = "INSERT INTO INTRANET_EVENT
										( Title, TitleEng, EventAssociation, EventAssociationEng, Description,DescriptionEng, EventDate, EventLocationID, EventVenue, EventVenueEng, EventNature,EventNatureEng, isSkipCycle, RecordType, RecordStatus, DateInput, InputBy, DateModified, ModifyBy, isSkipSAT, isSkipSUN, Email, ContactNo, Website, ClassGroupID)
								VALUES
										( '$Title', '$TitleEng', '$Association', '$AssociationEng','$Description','$DescriptionEng','$EventDate','$RoomID','$VenueLocation' , '$VenueEng','$Nature','$NatureEng',$isSkipCycle,$TypeNum,1,NOW(),$UserID,NOW(),$UserID, $isSkipSAT, $isSkipSUN, '$Email', '$ContactNo', '$Website', '$ClassGroupID')";
					}else{
						/*$sql = "INSERT INTO INTRANET_EVENT 
										(Title, Description, EventDate, EventLocationID, EventVenue, EventVenueEng, EventNature, isSkipCycle, RecordType, RecordStatus, DateInput, InputBy, DateModified, ModifyBy, isSkipSAT, isSkipSUN) 
								VALUES 
										('$Title','$Description','$EventDate','$RoomID','$VenueLocation', '$VenueEng','$Nature',$isSkipCycle,$TypeNum,1,NOW(),$UserID,NOW(),$UserID, $isSkipSAT, $isSkipSUN)";*/
					    $sql = "INSERT INTO INTRANET_EVENT
					    ( Title, TitleEng, EventAssociation, EventAssociationEng, Description,DescriptionEng, EventDate, EventLocationID, EventVenue, EventVenueEng, EventNature,EventNatureEng, isSkipCycle, RecordType, RecordStatus, DateInput, InputBy, DateModified, ModifyBy, isSkipSAT, isSkipSUN, Email, ContactNo, Website)
								VALUES
										( '$Title', '$TitleEng', '$Association', '$AssociationEng','$Description','$DescriptionEng','$EventDate','$RoomID','$VenueLocation' , '$VenueEng','$Nature','$NatureEng',$isSkipCycle,$TypeNum,1,NOW(),$UserID,NOW(),$UserID, $isSkipSAT, $isSkipSUN, '$Email', '$ContactNo', '$Website')";
					}
					$this->db_db_query($sql);
					$CurrentEventID = $this->db_insert_id();
					//debug_pr($sql);
					//debug_pr($CurrentEventID);
					//die();
					
					$CurrentID = $TempEventID;
					
					if($RelatedTo == $ParentID)
					{
						### if the event's record is a child
						$CurrentEventID = $this->db_insert_id();
					}else{
						### if the event's record is a parent
						$ParentID = $CurrentID;
						$ParentEventID = $this->db_insert_id();
						$CurrentEventID = $this->db_insert_id();
					}
					$eventIdAry[] = $CurrentEventID;
					
					if($GroupID != "" && $Type=="GE"){
						$GroupArray = explode(",",$GroupID);
						$value = "";
						$delimiter = "";
						for($j=0; $j<sizeof($GroupArray); $j++){		
							$value .= $delimiter."($GroupArray[$j],$CurrentEventID,NOW(),NOW())";
							$delimiter = ",";
						}
						
						$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID, DateInput, DateModified) VALUES $value";
						$success = $this->db_db_query($sql);
					}
					
					$sql = "UPDATE INTRANET_EVENT 
							SET 
								RelatedTo = $ParentEventID
							WHERE 
								EventID = $CurrentEventID";
					$result[] = $this->db_db_query($sql);
				}
			}
			$this->generatePreview();
			$this->generateProduction();
			
			
			### Re-generate the Special Timetable Settings
			if(sizeof($TempEventArr)>0){
				foreach($TempEventArr as $row => $value) {
				    list($TempEventID, $EventDate, $Type, $Title, $Venue, $Nature, $NatureEng, $DescriptionEng,$Description, $isSkipCycle, $GroupID, $RelatedTo, $isSkipSAT, $isSkipSUN) = $value;
					
					if ($isSkipCycle=="Y" && $this->Has_Special_Timetable_Settings_With_Cycle_Day($EventDate, $EventDate)) {
						$result['Regenerate_Special_Timetable_Settings'] = $this->ReGenerate_Special_Timetable_Date_By_Date_Range($EventDate, $EventDate);
					}
				}
			}
			
			$result['Syn_Event_To_Modules'] = $lcalevent->synEventToModules($eventIdAry);
				
			$sql = "DELETE FROM TEMP_INTRANET_EVENT";
			$this->db_db_query($sql);
			
			return $result;
		}
		
		function CheckTermIsConsistency($AcademicYearID, $TermStartDate, $TermEndDate)
		{
			$sql = "SELECT COUNT(*) FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$AcademicYearID'";
			$arrTemp = $this->returnVector($sql);
			if($arrTemp[0]>0)
			{
				### check any term before in the current year
				$PreviousTermEndYear = substr($TermStartDate,0,4);
				$PreviousTermEndMonth = substr($TermStartDate,5,2);
				$PreviousTermEndDay = substr($TermStartDate,8,2);
				$PreviousTermEndDate = date("Y-m-d H:i:s",mktime(23,59,59,$PreviousTermEndMonth,$PreviousTermEndDay-1,$PreviousTermEndYear));
				
				$sql = "SELECT COUNT(*) FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$AcademicYearID' AND TermEnd = '$PreviousTermEndDate'";
				$arrPreviousTermExist = $this->returnVector($sql);
								
				### check any term after in the current year
				$NextTermStartYear = substr($TermEndDate,0,4);
				$NextTermStartMonth = substr($TermEndDate,5,2);
				$NextTermStartDay = substr($TermEndDate,8,2);
				$NextTermStartDate = date("Y-m-d H:i:s",mktime(00,00,00,$NextTermStartMonth,$NextTermStartDay+1,$NextTermStartYear));
				
				$sql = "SELECT COUNT(*) FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$AcademicYearID' AND TermStart = '$NextTermStartDate'";
				$arrNextTermExist = $this->returnVector($sql);
				
				if(($arrPreviousTermExist[0] == 1) || ($arrNextTermExist[0] == 1))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return true;
			}
		}
		
		function DeleteAllIntranetEventByDateRange($StartDate, $EndDate)
		{
		    global $PATH_WRT_ROOT, $sys_custom;
			
			$this->Start_Trans();
			
			$sql = "Select EventID From INTRANET_EVENT WHERE EventDate BETWEEN '$StartDate' AND '$EndDate'";
			$eventIdAry = Get_Array_By_Key($this->returnResultSet($sql), 'EventID');
			
			if ($sys_custom['eBooking']['EverydayTimetable']) {
			    $originalEventInfoAry = $this->Get_Event_Info($eventIdAry);
			}
			
			$sql = "DELETE FROM INTRANET_EVENT WHERE EventDate BETWEEN '$StartDate' AND '$EndDate'";
			$result['deleteAllEvents'] = $this->db_db_query($sql);
			
			$sql = "DELETE FROM CALENDAR_EVENT_SCHOOL_EVENT_RELATION WHERE SchoolEventID IN ('".implode("', '", (array)$eventIdAry)."')";
			$result['deleteRelationship'] = $this->db_db_query($sql);
			
			$this->generatePreview();
			$this->generateProduction();
			
			include_once($PATH_WRT_ROOT."includes/libcal.php");
			include_once($PATH_WRT_ROOT."includes/libcalevent.php");
			include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
			$lcalevent = new libcalevent2007();
			$result['Syn_Event_To_Modules'] = $lcalevent->synEventToModules($eventIdAry);
				
			if ($sys_custom['eBooking']['EverydayTimetable']) {
			    
			    foreach($originalEventInfoAry as $_originalEventInfoAry) {
			        $_eventID = $_originalEventInfoAry['EventID'];
			        $_isSkipCycle = $_originalEventInfoAry['isSkipCycle'];
			        $_eventDate = substr($_originalEventInfoAry['EventDate'],0,10);
			        if ($_isSkipCycle) {
			            $timetableID = $this->getDefaultTimetableIDFromTimezone($_eventDate);
			            if ($timetableID) {
			                $defaultTimetableAry = $this->getDefaultTimetableInfo($_eventDate, $_eventDate);
			                if (count($defaultTimetableAry) == 0) {  // not set yet
			                    $result['insertTimetable'] = $this->insertEverydayDefaultTimetable($timetableID, $_eventDate);
			                }
			            }
			        }
			    }
			}
			
			if(!in_array(false,$result)){
				$this->Commit_Trans();
				return true;
			}else{
				$this->RollBack_Trans();
				return false;
			}
		}
		
		function checkIntranetEventExistByDateRange($StartDate, $EndDate)
		{
			$sql = "SELECT COUNT(*) FROM INTRANET_EVENT WHERE EventDate BETWEEN '$StartDate' AND '$EndDate'";
			$arrRecordsExist = $this->returnVector($sql);
			if($arrRecordsExist[0] >0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		function GetTotalNumOfEventsByDateRange($StartDate,$EndDate,$EventType="")
		{
			if(is_int($EventType) && $EventType >= 0){
				$cond_EventType = ' AND RecordType = '.$EventType;
			}
			$sql = "SELECT COUNT(*) FROM INTRANET_EVENT WHERE (EventDate BETWEEN '$StartDate' AND '$EndDate')".$cond_EventType;
			$arrNoOfEvents = $this->returnVector($sql);
			
			return $arrNoOfEvents[0]; 
		}
		
		
		
		public function Check_School_Settings_Access_Right() {
			global $PATH_WRT_ROOT;
			
			if(!($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]))
			{
				include_once($PATH_WRT_ROOT."includes/libaccessright.php");
				$laccessright = new libaccessright();
				$laccessright->NO_ACCESS_RIGHT_REDIRECT();
				exit;
			}
		}
		
		public function Get_Academic_Year_And_Term_Info_By_TimeZone($TimezoneIDArr) {
			$sql = "Select
							tz.PeriodID as TimezoneID,
							ay.AcademicYearID,
							ay.YearNameEN,
							ay.YearNameB5,
							ayt.YearTermID,
							ayt.YearTermNameEN as YearTermNameEn,
							ayt.YearTermNameB5 as YearTermNameCh
					From
							INTRANET_CYCLE_GENERATION_PERIOD as tz
							Inner Join ACADEMIC_YEAR_TERM as ayt On (tz.PeriodStart Between ayt.TermStart AND ayt.TermEnd)
							Inner Join ACADEMIC_YEAR as ay On (ayt.AcademicYearID = ay.AcademicYearID)
					Where
							tz.PeriodID In ('".implode("','", (array)$TimezoneIDArr)."') 
					";
			return $this->returnArray($sql);
		}
		
		// Return array index start from 1
		public function Get_Cycle_Day_By_TimeZone($TimezoneID) {
			global $Lang;
			
			$TimezoneInfoArr = $this->returnPeriods_NEW($TimezoneID);
			$PeriodType = $TimezoneInfoArr[0]['PeriodType'];
			$PeriodTypeText = $this->Get_Period_Type_Text($PeriodType);
			$CycleType = $TimezoneInfoArr[0]['CycleType'];
			$NumOfCycleDay = $TimezoneInfoArr[0]['PeriodDays'];
			
			$ReturnArr = array();
			if ($PeriodType == 0) {
				$WeekdayDisplayArr = $Lang['General']['DayType4'];
				$numOfWeekday = count($WeekdayDisplayArr);
				
				for ($i=0; $i<$numOfWeekday; $i++) {
					$ReturnArr[$i+1] = $WeekdayDisplayArr[$i];
				}
			}
			else if ($PeriodType == 1) {
				
				$CycleTextArr = array();
				switch ($CycleType) {
		    		case 1:
		    			$CycleTextArr = $this->array_alphabet;
		    			break;
		    		case 2:
		    			$CycleTextArr = $this->array_roman;
		    			break;
		    		default:
		    			$CycleTextArr = $this->array_numeric;
		    			break;
		    	}
		    	
		    	for ($i=0; $i<$NumOfCycleDay; $i++) {
		    		$ReturnArr[$i+1] = $CycleTextArr[$i];
		    	}
		    }
		    
		    return $ReturnArr;		
		}
		
		public function Get_Dates_By_TimeZone($TimezoneID, $CycleDayArr='', $ReturnAssocWithDay=0, $PeriodTypeText='') {
			$funcParamAry = get_defined_vars();
			$cacheResultAry = $this->getCacheResult(__FUNCTION__, $funcParamAry);
	    	if ($cacheResultAry !== null) {
	    		return $cacheResultAry;
	    	}
	    	
			$TimezoneInfoArr = $this->returnPeriods_NEW($TimezoneID);
			$PeriodStart = $TimezoneInfoArr[0]['PeriodStart'];
			$PeriodEnd = $TimezoneInfoArr[0]['PeriodEnd'];
			
			if ($PeriodTypeText=='') {
				$PeriodType = $TimezoneInfoArr[0]['PeriodType'];
				$PeriodTypeText = $this->Get_Period_Type_Text($PeriodType);
			}
			
			
			$ReturnArr = array();
			if ($PeriodTypeText == 'weekday') {
				
				$PeriodStartTS = strtotime($PeriodStart);
				$PeriodEndTS = strtotime($PeriodEnd);
				$thisDateTS = $PeriodStartTS; 
				while($thisDateTS <= $PeriodEndTS) 
				{
					$thisDate = date('Y-m-d', $thisDateTS);
					$thisDateDay = date('w', $thisDateTS);
					
					if ($CycleDayArr=='' || (is_array($CycleDayArr) && in_array($thisDateDay, (array)$CycleDayArr))) {
						if($ReturnAssocWithDay==1)
							$ReturnArr[$thisDate] = $thisDateDay;
						else
							$ReturnArr[] = $thisDate;
					}
					
					$thisDateTS = strtotime("+1 day", $thisDateTS);
				}
			}
			else if ($PeriodTypeText == 'cycle') {
				$DateInfoArr = $this->Get_Cycle_Day_Info_By_Date_Range($PeriodStart, $PeriodEnd, $CycleDayArr);
				if($ReturnAssocWithDay==1)
					$ReturnArr = BuildMultiKeyAssoc($DateInfoArr, 'RecordDate', 'CycleDay',1);
				else
					$ReturnArr = Get_Array_By_Key($DateInfoArr, 'RecordDate');
			}
			
			$this->setCacheResult(__FUNCTION__, $funcParamAry, $ReturnArr);
			return $ReturnArr;
		}
		
		public function Get_Period_Type_Text($PeriodType) {
			if ($PeriodType === '') {
				$ReturnText = '';
			}
			else if ($PeriodType == 1) {
				$ReturnText = 'cycle';
			}
			else if ($PeriodType == 0) {
				$ReturnText = 'weekday';
			}
			return $ReturnText;
		}
		
		public function Get_Cycle_Day_Info_By_Date_Range($ParStartDate, $ParEndDate, $ParCycleDayArr='') {
			
			if ($ParCycleDayArr != '') {
				$conds_CycleDay = " And CycleDay In ('".implode("','", (array)$ParCycleDayArr)."')";
			}
			
			$sql = "Select
							DATE_FORMAT(RecordDate,'%Y-%m-%d') as RecordDate,
							TextEng,
							TextChi,
							TextShort,
							PeriodID,
							CycleDay
					From
							INTRANET_CYCLE_DAYS
					Where
							RecordDate >= '$ParStartDate' AND RecordDate <= '$ParEndDate'
							$conds_CycleDay
					Order By
							RecordDate
					";
            return $this->returnArray($sql);
		}
		
		function Get_All_TimeZone_By_Academic_Year($AcademicYearID='')
		{
			if($AcademicYearID=='') {
				$AcademicYearID = Get_Current_Academic_Year_ID();
			}
				
			$sql = "
				SELECT
					tz.PeriodID,
					tz.PeriodStart,
					tz.PeriodEnd,
					tz.ColorCode
				FROM
					ACADEMIC_YEAR ay 
					INNER JOIN ACADEMIC_YEAR_TERM ayt ON ayt.AcademicYearID = ay.AcademicYearID
					INNER JOIN INTRANET_CYCLE_GENERATION_PERIOD as tz ON (tz.PeriodStart Between ayt.TermStart AND ayt.TermEnd) AND tz.RecordStatus = 1
				WHERE
					ay.AcademicYearID IN ('".implode("','", (array)$AcademicYearID)."') 
				Order By
					tz.PeriodStart
			";				
			
			$result = $this->returnArray($sql);
			return $result;
		}
		
		public function Save_Special_Timetable_Settings($SpecialTimetableSettingsID, $TimezoneID, $TimetableID, $BgColor, $RepeatType, $RepeatDaySettingsArr, $RepeatDateArr) {
		    global $sys_custom, $intranet_root, $intranet_session_language;
		    
            if ($sys_custom['eBooking']['EverydayTimetable']) {
                $oldSpecialTimetableAry = $this->Get_Special_Timetable_Settings_Date_Info($SpecialTimetableSettingsID, $TimezoneID);
                
                $oldSpecialDateAry = array_keys($oldSpecialTimetableAry);
                $timetableAssoc = $this->getOriginalTimeTableIDBySpecialTimetableSettingsID($SpecialTimetableSettingsID);
               
                // get affected date list, used to find out if their DefaultTimetableID will be changed at this update 
                $affectedDateAry = array_unique(array_merge($oldSpecialDateAry,$RepeatDateArr));
                $affectedTimetableBeforeAry = $this->getDefaultTimetableIDByDate($affectedDateAry);

                include_once($intranet_root."/includes/libebooking.php");
                $libebooking = new libebooking();
            }

			$DataArr = array();
			$DataArr['TimezoneID'] = $TimezoneID;
			$DataArr['TimetableID'] = $TimetableID;
			$DataArr['BgColor'] = $BgColor;
			$DataArr['RepeatType'] = $RepeatType;
			
			if ($SpecialTimetableSettingsID == '') {
				$SpecialTimetableSettingsID = $this->Insert_Special_Timetable_Settings($DataArr);
			}
			else {
				$SpecialTimetableSettingsID = $this->Update_Special_Timetable_Settings($SpecialTimetableSettingsID, $DataArr);
			}
			
			if ($SpecialTimetableSettingsID) {
				$SuccessArr['Update_Special_Timetable_Settings'] = true;
				
				$SuccessArr['Update_Special_Timetable_Repeat_Day_Settings'] = $this->Update_Special_Timetable_Repeat_Day_Settings($SpecialTimetableSettingsID, $RepeatDaySettingsArr);
				$SuccessArr['Update_Special_Timetable_Date_Info'] = $this->Generate_Special_Timetable_Date_Info($SpecialTimetableSettingsID, $RepeatDateArr);
			}
			else {
				$SuccessArr['Update_Special_Timetable_Settings'] = false;
			}
			
			if ($sys_custom['eBooking']['EverydayTimetable']) {
			    include_once($intranet_root."/includes/libtimetable.php");
			    $existingDateAry = $this->getDateInEverydayTimetable($RepeatDateArr);
			    
			    $dateToAddAry = array_diff($RepeatDateArr, $existingDateAry);
			    
			    if (count($dateToAddAry)) {
			        $SuccessArr['AddEverydayTimetable'] = $this->insertEverydayDefaultTimetable($TimetableID, $dateToAddAry);
			    }
			    
			    $SuccessArr['UpdateEverydayTimetable'] = $this->updateEverydayDefaultTimetableID($existingDateAry, $TimetableID);
			  
			    // dates that are not selected for special timetable
			    $defaultDateAry = array_diff($oldSpecialDateAry,$RepeatDateArr);
			    
			    if (count($defaultDateAry)) {
			        foreach($defaultDateAry as $_date) {
			            $_cycleNameAry = $this->getCycleNamesByDateRange($_date,$_date);
			            if (count($_cycleNameAry)) {
			                $SuccessArr['UpdateEverydayTimetable_'.$_date] = $this->updateEverydayDefaultTimetableID($_date, $timetableAssoc[$_date]['TimetableID']);
			            }
			            else {
			                $timeSlotIDAry = $this->getTimeSlotByDefaultTimetable($_date,$timetableAssoc[$_date]['SpecialTimetableID']);
			                
			                $SuccessArr['DeleteEverydayTimetable_'.$_date] = $this->deleteEverydayDefaultTimetableID($_date,$timetableAssoc[$_date]['SpecialTimetableID']);

			                // reject bookings
			                foreach ((array)$timeSlotIDAry as $_timeSlotID) {
			                    $SuccessArr['rejectBookingByTimeSlot_'.$_timeSlotID] = $libebooking->rejectBookingByTimeSlot($_timeSlotID, $_date);
			                }
			            }
			        }
			    }
			    
			    // to find out which date has changed DefaultTimeTableID and not set TimetableID in EverydayTimetable, then update related bookings
			    $affectedTimetableAfterAry = $this->getDefaultTimetableIDByDate($affectedDateAry);
			    $libtimetable = new Timetable();
			    foreach((array)$affectedTimetableAfterAry as $_date => $_newTimetableID) {
			        if (isset($affectedTimetableBeforeAry[$_date]) && $affectedTimetableBeforeAry[$_date] != $_newTimetableID) {
			            $SuccessArr['updateBookingTimeslot_'.$date] = $libtimetable->updateBookingTimeslotByTimetable($_date, $affectedTimetableBeforeAry[$_date], $_newTimetableID);
			        }
			    }
			}
			
			if (!in_array(false, $SuccessArr)) {
				return true;
			}
			else {
				return false;
			}
		}
		
		private function Insert_Special_Timetable_Settings($DataArr) {
			if (count($DataArr) == 0)
				return false;
				
			## insert data			
			# set field and value string
			$fieldArr = array();
			$valueArr = array();
			foreach ($DataArr as $field => $value)
			{
				$fieldArr[] = $field;
				$valueArr[] = "'".$this->Get_Safe_Sql_Query($value)."'";
			}
			
			## set others fields
			$fieldArr[] = 'InputBy';
			$valueArr[] = $_SESSION['UserID'];
			$fieldArr[] = 'InputDate';
			$valueArr[] = 'now()';
			$fieldArr[] = 'ModifiedBy';
			$valueArr[] = $_SESSION['UserID'];
			$fieldArr[] = 'ModifyDate';
			$valueArr[] = 'now()';
			
			$fieldText = implode(", ", $fieldArr);
			$valueText = implode(", ", $valueArr);
			
			# Insert Record
			$this->Start_Trans();
			
			$sql = "Insert Into INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS
						($fieldText) Values ($valueText)
					";
			$success = $this->db_db_query($sql);
			
			if ($success == false) {
				$SpecialTimetableSettingsID = false;
				$this->RollBack_Trans();
			}				
			else {
				$SpecialTimetableSettingsID = $this->db_insert_id();
				$this->Commit_Trans();
			}
			
			return $SpecialTimetableSettingsID;
		}
		
		private function Update_Special_Timetable_Settings($SpecialTimetableID, $DataArr) {
			if (count($DataArr) == 0)
				return false;
				
			# Build field update values string
			$valueFieldText = '';
			foreach ($DataArr as $field => $value) {
				$valueFieldText .= $field." = '".$this->Get_Safe_Sql_Query($value)."', ";
			}
			$valueFieldText .= ' ModifyDate = now(), ';
			$LastModifiedBy = ($_SESSION['UserID'])? "'".$_SESSION['UserID']."'" : 'NULL';
			$valueFieldText .= ' ModifiedBy = '.$LastModifiedBy.' ';
			
			$sql = "Update 
							INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS
					Set 
							$valueFieldText
					Where 
							SpecialTimetableSettingsID = '".$SpecialTimetableID."'
					";
			$success = $this->db_db_query($sql);
			
			return ($success)? $SpecialTimetableID : false;
		}
		
		private function Update_Special_Timetable_Repeat_Day_Settings($SpecialTimetableSettingsID, $RepeatDaySettingsArr) {
			$numOfRepeatDay = count($RepeatDaySettingsArr);
			if ($SpecialTimetableSettingsID=='' || $SpecialTimetableSettingsID==false || $numOfRepeatDay == 0) {
				return false;
			}
				
			$this->Start_Trans();
				
			$sql = "Delete From INTRANET_TIMEZONE_SPECIAL_TIMETABLE_REPEAT_DAY Where SpecialTimetableSettingsID = '".$SpecialTimetableSettingsID."'";
			$SuccessArr['Delete_Old_Repeat_Day_Settings'] = $this->db_db_query($sql);
			
			$InsertValueArr = array();
			for ($i=0; $i<$numOfRepeatDay; $i++) {
				$thisRepeatDay = $RepeatDaySettingsArr[$i];
				
				$thisValueArr = array();
				$thisValueArr[] = $SpecialTimetableSettingsID;
				$thisValueArr[] = $thisRepeatDay;
				$thisValueArr[] = $_SESSION['UserID'];
				$thisValueArr[] = "'now()'";
				$thisValueArr[] = $_SESSION['UserID'];
				$thisValueArr[] = "'now()'";
				
				$InsertValueArr[] = '('.implode(',', (array)$thisValueArr).')';
			}
			
			$sql = "Insert Into INTRANET_TIMEZONE_SPECIAL_TIMETABLE_REPEAT_DAY
						(SpecialTimetableSettingsID, RepeatDay, InputBy, InputDate, ModifiedBy, ModifyDate)
						Values
						".implode(',', (array)$InsertValueArr)."
					";
			$SuccessArr['Insert_Repeat_Day_Settings'] = $this->db_db_query($sql);
			
			if (in_array(false, (array)$SuccessArr)) {
				$this->RollBack_Trans();
				return false;
			}				
			else {
				$this->Commit_Trans();
				return true;
			}
		}
		
		private function Generate_Special_Timetable_Date_Info($SpecialTimetableSettingsID, $SpecialDateArr, $KeepCurrentDate=0) {
			$numOfSpecialDay = count($SpecialDateArr);
			if ($SpecialTimetableSettingsID=='' || $SpecialTimetableSettingsID==false || $numOfSpecialDay == 0) {
				return false;
			}
				
			$this->Start_Trans();
				
			if ($KeepCurrentDate == 0) {
				$sql = "Delete From INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE Where SpecialTimetableSettingsID = '".$SpecialTimetableSettingsID."'";
				$SuccessArr['Delete_Old_Date_Info'] = $this->db_db_query($sql);
			}
			
			$InsertValueArr = array();
			for ($i=0; $i<$numOfSpecialDay; $i++) {
				$thisDate = $SpecialDateArr[$i];
				
				$thisValueArr = array();
				$thisValueArr[] = $SpecialTimetableSettingsID;
				$thisValueArr[] = "'$thisDate'";
				$thisValueArr[] = $_SESSION['UserID'];
				$thisValueArr[] = "'now()'";
				
				$InsertValueArr[] = '('.implode(',', (array)$thisValueArr).')';
			}
			
			$sql = "Insert Into INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE
						(SpecialTimetableSettingsID, SpecialDate, InputBy, InputDate)
						Values
						".implode(',', (array)$InsertValueArr)."
					";
			$SuccessArr['Insert_New_Date_Info'] = $this->db_db_query($sql);
			
			if (in_array(false, (array)$SuccessArr)) {
				$this->RollBack_Trans();
				return false;
			}				
			else {
				$this->Commit_Trans();
				return true;
			}
		}
		
		public function ReGenerate_Special_Timetable_Date_By_Date_Range($StartDate, $EndDate) {
			### Get all Time Zone within the Date Range
			$TimezoneInfoArr = $this->returnPeriods_NEW($PeriodID='', $PublishedOnly=0, $AcademicYearID='', $StartDate, $EndDate);
			
			### Get all Special Timetable Settings involved (repeat in "cycle" only)
			$TimezoneIDArr = Get_Array_By_Key($TimezoneInfoArr, 'PeriodID');
			$SpecialTimetableSettingsInfoArr = $this->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsIDArr='', $TimezoneIDArr, 'cycle');
			
			$SuccessArr = array();
			foreach ((array)$SpecialTimetableSettingsInfoArr as $thisSettingsID => $thisSettingsInfoArr) {
				$SuccessArr[$thisSettingsID] = $this->ReGenerate_Special_Timetable_Date_By_Settings($thisSettingsID, $StartDate);
			}
			
			return (in_array(false, $SuccessArr))? false : true;
		}
		
		private function ReGenerate_Special_Timetable_Date_By_Settings($SpecialTimetableSettingsID, $StartDate='') {
			$SpecialTimetableSettingsInfoArr = $this->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsID);
			$TimezoneID = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['TimezoneID'];
			$RepeatType = $SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['BasicInfo']['RepeatType'];
			
			
			$SuccessArr = array();
			
			# Delete Old Special Timetable Date starting from the start date (do not affect the old special timetable settings)
			$CurSpecialDateArr = Get_Array_By_Key($SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['SpecialDateInfo'], 'SpecialDate');
			$SuccessArr['DeleteOldSpecialDate'] = $this->Delete_Special_Timetable_Date($CurSpecialDateArr, $SpecialTimetableSettingsID, $StartDate);
			
			# Get Current Special Timetable Settings to avoid 2 Settings in the same day
			$TimezoneSpecialDateArr = $this->Get_Special_Timetable_Settings_Date_Info('', $TimezoneID, '', $RefreshPreload=1);
			
			# Get the related dates according to the Special Timetable Repeat Day Settings
			$RepeatDayArr = Get_Array_By_Key($SpecialTimetableSettingsInfoArr[$SpecialTimetableSettingsID]['RepeatDayInfo'], 'RepeatDay');
			$DateArr = $this->Get_Dates_By_TimeZone($TimezoneID, $RepeatDayArr, $ReturnAssocWithDay=1, $RepeatType);
			
			$SpecialDateArr = array();
			foreach ((array)$DateArr as $thisDate => $thisDay) {
				if (isset($TimezoneSpecialDateArr[$thisDate])) {
					// The date has Special Timetable Settings already
					continue;
				}
				
				if ($StartDate != '' && $thisDate < $StartDate) {
					// The date is before the Start Date
					continue;
				}
				
				$SpecialDateArr[] = $thisDate;
			}
			if (count($SpecialDateArr) > 0) {
				$SuccessArr['Insert_Special_Date'] = $this->Generate_Special_Timetable_Date_Info($SpecialTimetableSettingsID, $SpecialDateArr, $KeepCurrentDate=1);
			}
			
			return (in_array(false, $SuccessArr))? false : true ;
		}
		
		public function Delete_Special_Timetable_Settings($SpecialTimetableSettingsIDArr) {
		    global $sys_custom, $intranet_root, $intranet_session_language;;

		    if ($sys_custom['eBooking']['EverydayTimetable']) {
		        
		        include_once($intranet_root."/includes/libebooking.php");
		        include_once($intranet_root."/includes/libtimetable.php");
		        $libebooking = new libebooking();
		        $libtimetable = new Timetable();
		        
		        // get back original TimetableID if special timetable setting is deleted 
		        $timetableAssoc = $this->getOriginalTimeTableIDBySpecialTimetableSettingsID($SpecialTimetableSettingsIDArr);
		        
		        foreach((array)$timetableAssoc as $_recordDate=>$_timetableIDAry) {
		            $_timetableID = $_timetableIDAry['TimetableID'];                      // original TimetableID
		            $_specialTimetableID = $_timetableIDAry['SpecialTimetableID'];        // special TimetableID
		            $_cycleNameAry = $this->getCycleNamesByDateRange($_recordDate,$_recordDate);
		            
		            if (count($_cycleNameAry)) {  // record exist, i.e. not in the excluded dates
    		            $sql = "UPDATE INTRANET_SCHOOL_EVERYDAY_TIMETABLE SET DefaultTimetableID='".$_timetableID."', DateModified=NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE RecordDate='".$_recordDate."'";
    		            $SuccessArr['UpdateOriginalTimetableID'] = $this->db_db_query($sql);
    		            
    		            // update booking timetable and TimeslotID & time slot: SpecialTimetableID back to normal TimetableID 
    		            $SuccessArr['UpdateOriginalTimetableIDToBooking'] = $libtimetable->updateBookingTimeslotByTimetable($_recordDate, $_specialTimetableID, $_timetableID);
		            }
		            else {    // in skipped dates
		                $timeSlotIDAry = $this->getTimeSlotByDefaultTimetable($_recordDate,$_specialTimetableID);
		                
		                $SuccessArr['DeleteSpecialTimetableID'] = $this->deleteEverydayDefaultTimetableID($_recordDate,$_specialTimetableID);
		                
		                // reject bookings
		                foreach ((array)$timeSlotIDAry as $_timeSlotID) {
		                    $SuccessArr['rejectBookingByTimeSlot_'.$_timeSlotID] = $libebooking->rejectBookingByTimeSlot($_timeSlotID, $_recordDate);
		                }
		            }
		        }
		    }
		    
			$this->Start_Trans();
			
			$sql = "Delete From INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS Where SpecialTimetableSettingsID In ('".implode("','", (array)$SpecialTimetableSettingsIDArr)."')";
			$SuccessArr['Delete_Basic_Settings'] = $this->db_db_query($sql);
			
			$sql = "Delete From INTRANET_TIMEZONE_SPECIAL_TIMETABLE_REPEAT_DAY Where SpecialTimetableSettingsID In ('".implode("','", (array)$SpecialTimetableSettingsIDArr)."')";
			$SuccessArr['Delete_Repeat_Day_Settings'] = $this->db_db_query($sql);
			
			$sql = "Delete From	INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE Where SpecialTimetableSettingsID In ('".implode("','", (array)$SpecialTimetableSettingsIDArr)."')";
			$SuccessArr['Delete_Special_Dates'] = $this->db_db_query($sql);
			
			if (in_array(false, (array)$SuccessArr)) {
				$this->RollBack_Trans();
				return false;
			}				
			else {
				$this->Commit_Trans();
				return true;
			}
		}
		
		private function Delete_Special_Timetable_Date($DateArr, $SpecialTimetableSettingsID='', $StartDate='') {
			if ($SpecialTimetableSettingsID != '') {
				$conds_SpecialTimetableSettingsID = " And SpecialTimetableSettingsID = '".$SpecialTimetableSettingsID."' ";
			}
			if ($StartDate != '') {
				$conds_SpecialDate = " And SpecialDate >= '".$StartDate."' ";
			}
			
			$sql = "Delete From 
							INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE 
					Where 
							SpecialDate In ('".implode("','", (array)$DateArr)."')
							$conds_SpecialTimetableSettingsID
							$conds_SpecialDate
					";
			$success = $this->db_db_query($sql);
			return $success;
		}
		
		public function Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsIDArr='', $TimezoneIDArr='', $RepeatType='', $RefreshPreload=0) {
			$PreloadArrKey = 'Get_Special_Timetable_Settings_Info';
			$FuncArgArr = get_defined_vars();
			$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    	if ($returnArr !== false && $RefreshPreload==0) {
	    		return $returnArr;
	    	}
	    	
	    	if ($SpecialTimetableSettingsIDArr != '') {
	    		$conds_SpecialTimetableSettingsID = " And SpecialTimetableSettingsID In ('".implode("','", (array)$SpecialTimetableSettingsIDArr)."') ";
	    	}
	    	if ($TimezoneIDArr != '') {
	    		$conds_TimezoneID = " And TimezoneID In ('".implode("','", (array)$TimezoneIDArr)."') ";
	    	}
	    	if ($RepeatType != '') {
	    		$conds_RepeatType = " And RepeatType = '".$RepeatType."' ";
	    	}
	    	
			$sql = "Select
							SpecialTimetableSettingsID,
							TimezoneID,
							TimetableID,
							BgColor,
							RepeatType,
							ModifiedBy,
							ModifyDate
					From
							INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS
					Where
							1
							$conds_SpecialTimetableSettingsID
							$conds_TimezoneID
							$conds_RepeatType
					";
			$BasicInfoArr = $this->returnArray($sql);
			$BasicInfoAssoArr = BuildMultiKeyAssoc($BasicInfoArr, 'SpecialTimetableSettingsID');
			
			if ($SpecialTimetableSettingsIDArr=='') {
				$SpecialTimetableSettingsIDArr = Get_Array_By_Key($BasicInfoArr, 'SpecialTimetableSettingsID');
			}
		//	debug_pr($SpecialTimetableSettingsIDArr);

			$varKey = 'Get_Special_Timetable_Settings_Info_repeatDay_'.implode("_", (array)$SpecialTimetableSettingsIDArr);
			if ($this->{$varKey} === null) {
				$sql = "Select
							SpecialTimetableSettingsID,
							RepeatDay
					From
							INTRANET_TIMEZONE_SPECIAL_TIMETABLE_REPEAT_DAY
					Where
							SpecialTimetableSettingsID In ('".implode("','", (array)$SpecialTimetableSettingsIDArr)."')
					Order By
							RepeatDay
					";
				$RepeatDayInfoArr = $this->returnArray($sql);
				$this->{$varKey} = BuildMultiKeyAssoc($RepeatDayInfoArr, 'SpecialTimetableSettingsID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			}
			$RepeatDayInfoAssoArr = $this->{$varKey};
				
			$varKeyspecial = 'Get_Special_Timetable_Settings_Info_specialDay_'.implode("','", (array)$SpecialTimetableSettingsIDArr);
			if ($this->{$varKeyspecial} === null) {
				$sql = "Select
							SpecialTimetableSettingsID,
							SpecialDate
					From
							INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE
					Where
							SpecialTimetableSettingsID In ('".implode("','", (array)$SpecialTimetableSettingsIDArr)."')
					Order By
							SpecialDate
					";
				$SpecialDateInfoArr = $this->returnArray($sql);
				$this->{$varKeyspecial} = BuildMultiKeyAssoc($SpecialDateInfoArr, 'SpecialTimetableSettingsID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
			}
			
			$SpecialDateInfoAssoArr = $this->{$varKeyspecial};
			
			$SpecialTimetableSettingsIDArr = Get_Array_By_Key($BasicInfoArr, 'SpecialTimetableSettingsID');
			$ReturnArr = array();
			$numOfSettings = count($SpecialTimetableSettingsIDArr);
			for ($i=0; $i<$numOfSettings; $i++) {
				$thisSettingsID = $SpecialTimetableSettingsIDArr[$i];
				
				$ReturnArr[$thisSettingsID]['BasicInfo'] = $BasicInfoAssoArr[$thisSettingsID];
				$ReturnArr[$thisSettingsID]['RepeatDayInfo'] = $RepeatDayInfoAssoArr[$thisSettingsID];
				$ReturnArr[$thisSettingsID]['SpecialDateInfo'] = $SpecialDateInfoAssoArr[$thisSettingsID];
				$ReturnArr[$thisSettingsID]['FirstSpecialDate'] = $SpecialDateInfoArr[0]['SpecialDate'];
			}
			
			$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
			return $ReturnArr;
		}
		
		public function Get_Special_Timetable_Settings_Date_Info($SpecialTimetableSettingsIDArr='', $TimezoneIDArr='', $DateArr='', $RefreshPreload=0) {
			$PreloadArrKey = 'Get_Special_Timetable_Settings_Date_Info';
			$FuncArgArr = get_defined_vars();
			$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    	if ($returnArr !== false && $RefreshPreload==0) {
	    		return $returnArr;
	    	}
	    	
	    	$SpecialTimetableSettingsArr = $this->Get_Special_Timetable_Settings_Info($SpecialTimetableSettingsIDArr, $TimezoneIDArr, $RefreshPreload);
	    	
	    	$SettingsIDArr = array();
	    	$ReturnArr = array();
	    	foreach ((array)$SpecialTimetableSettingsArr as $thisSettingsID => $thisInfoArr) {
	    		$thisTimetableID = $thisInfoArr['BasicInfo']['TimetableID'];
	    		$thisBgColor = $thisInfoArr['BasicInfo']['BgColor'];
	    		
	    		$thisSpecialDateInfoArr = $thisInfoArr['SpecialDateInfo'];
	    		$numOfSpecialDate = count($thisSpecialDateInfoArr);
	    		
	    		for ($i=0; $i<$numOfSpecialDate; $i++) {
	    			$thisSpecialDate = $thisSpecialDateInfoArr[$i]['SpecialDate'];
	    			
	    			if ($DateArr!='' && !in_array($thisSpecialDate, (array)$DateArr)) {
	    				continue;
	    			}
	    			
	    			$ReturnArr[$thisSpecialDate]['SpecialTimetableSettingsID'] = $thisSettingsID;
	    			$ReturnArr[$thisSpecialDate]['TimetableID'] = $thisTimetableID;
		    		$ReturnArr[$thisSpecialDate]['BgColor'] = $thisBgColor;
		    		$ReturnArr[$thisSpecialDate]['FirstDateInSpecialSettings'] = (in_array($thisSettingsID, (array)$SettingsIDArr))? 0 : 1;
		    		$SettingsIDArr[] = $thisSettingsID;
	    		}
	    	}
	    	
	    	$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
			return $ReturnArr;
		}
		
		public function Has_Special_Timetable_Settings_With_Cycle_Day($StartDate, $EndDate) {
			// Get the involved Time Zones of the date range
			$TimezoneInfoArr = $this->returnPeriods_NEW($PeriodID='', $PublishedOnly=0, $AcademicYearID='', $StartDate, $EndDate);
			$numOfTimezone = count($TimezoneInfoArr);
			
			$AffectSpecialTimetable = false;
			for ($i=0; $i<$numOfTimezone; $i++) {
				$thisTimezoneID = $TimezoneInfoArr[$i]['PeriodID'];
				$thisTimezoneType = $this->Get_Period_Type_Text($TimezoneInfoArr[$i]['PeriodType']);
				
				// Check the involved Special Timetable in each Time Zones
				if ($thisTimezoneType == 'cycle') {
					$thisSpecialTimetableInfoArr = $this->Get_Special_Timetable_Settings_Info('', $thisTimezoneID);
				
					foreach ((array)$thisSpecialTimetableInfoArr as $thisSettingsID => $thisSettingsInfoArr) {
						$thisRepeatType = $thisSettingsInfoArr['BasicInfo']['RepeatType'];
						
						if ($thisRepeatType == 'cycle') {
							$AffectSpecialTimetable = true;
							break;
						}
					}
				}
			}
			
			return $AffectSpecialTimetable;
		}
		
		private function Get_Event_Info($EventIDArr) {
			$PreloadArrKey = 'Get_Event_Info';
			$FuncArgArr = get_defined_vars();
			$returnArr = $this->Get_Preload_Result($PreloadArrKey, $FuncArgArr);
	    	if ($returnArr !== false) {
	    		return $returnArr;
	    	}
	    	
			$sql = "Select
							EventID,
							Title,
							Description,
							EventDate,
							isSkipCycle
					From
							INTRANET_EVENT
					Where
							EventID In ('".implode("','", (array)$EventIDArr)."')
					";
			$ReturnArr = $this->returnArray($sql);
			
			$this->Set_Preload_Result($PreloadArrKey, $FuncArgArr, $ReturnArr);
			return $ReturnArr;
		}
		
		public function Get_Event_Date_Range($EventIDArr) {
			$sql = "Select 
							DATE_FORMAT(min(EventDate), '%Y-%m-%d') as StartDate, DATE_FORMAT(max(EventDate), '%Y-%m-%d') as EndDate 
					From 
							INTRANET_EVENT
					Where
							EventID In ('".implode("','", (array)$EventIDArr)."')
					";
			return $this->returnArray($sql); 
		}

		private function Get_Preload_Result($ArrayKey, $FunctionArgumentArr) {
			$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
	    	$PreloadInfoArrExist = $this->Check_PreloadInfoArr_Exist($ArrayKey, $FunctionArgumentArr);
	    	
	    	$returnAry = array();
	    	if ($PreloadInfoArrExist)
	    	{
                $PreloadInfoArrVariableStr = str_replace(';','',$PreloadInfoArrVariableStr);
	    		eval('$returnAry = '.$PreloadInfoArrVariableStr.';');
	    		return $returnAry;
	    	}
	    	else {
	    		return false;
	    	}
		}
		
		private function Set_Preload_Result($ArrayKey, $FunctionArgumentArr, $ResultArr) {
			$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
            $PreloadInfoArrVariableStr = str_replace(';','',$PreloadInfoArrVariableStr);
			eval($PreloadInfoArrVariableStr.' = $ResultArr;');
		}
		
		private function Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr) {
			$PreloadInfoArrVariableStr = '$this->PreloadInfoArr[\''.$ArrayKey.'\']';
	    	foreach( (array)$FunctionArgumentArr as  $arg_key => $arg_val)
	    	{
	    		if (is_array($arg_val)) {
	    			$key_name = implode(',', $arg_val);
	    		}
	    		else {
	    			$key_name = trim($arg_val);
	    		}
	    		$PreloadInfoArrVariableStr .= "['".addslashes($key_name)."']";
	    	}
	    	
	    	return $PreloadInfoArrVariableStr;
		}
		
		private function Check_PreloadInfoArr_Exist($ArrayKey, $FunctionArgumentArr) {
			$PreloadInfoArrVariableStr = $this->Get_PreloadInfoArrVariableStr($ArrayKey, $FunctionArgumentArr);
            $PreloadInfoArrVariableStr = str_replace(';','',$PreloadInfoArrVariableStr);
			
			$PreloadInfoArrExist = false;
	    	eval('$PreloadInfoArrExist = isset('.$PreloadInfoArrVariableStr.');');
	    	
	    	return $PreloadInfoArrExist;
		}
		
		private function addEverydayTimetableRecord($PeriodStart,$PeriodEnd,$PeriodType,$PeriodDays,$FirstDay,$SkipOnWeekday,$TimetableTemplate)
		{
		    if ($PeriodType == 1) {         # Use Cycle Day
		        if ($PeriodDays < 1) return;
		    }
		    else {    // weekdays
		        $PeriodDays = 7;
		    }
		    
	        # Get Non-Cycle Days
		    $non_cycle_days = array();
	        $nonCycleDateAry = $this->getNonCycleDays($PeriodStart,$PeriodEnd);
	        for ($j=0, $jMax=count($nonCycleDateAry); $j<$jMax; $j++) {
	            $ts = strtotime($nonCycleDateAry[$j]);
	            $non_cycle_days[$ts] = 1;
	        }
	        $ts_start = strtotime($PeriodStart);
	        $ts_end = strtotime($PeriodEnd);
	        $ts_current = $ts_start;
	        $current_weekday = date("w",$ts_current);
	        $current_cycleday = $FirstDay;
	        $dateToAddAry = array();
	        while ($ts_current <= $ts_end) {
	            # Check is non-cycle day
	            if ($non_cycle_days[$ts_current]==1) { # Skip
	                $current_weekday = ($current_weekday+1)%7;
	                $ts_current += 86400;  # 1 Day
	                continue;
	            }
	            
                # Check is Sunday & Saturday
                if ($current_weekday==0 || $current_weekday==6) {
                    $ts_current += 86400;  # 1 Day
                    $current_weekday = ($current_weekday+1)%7;
                    continue;
                }
	                
	            // cycle days
	            if ($PeriodType == 1) {
	                # Check is Skip on Weekday
    	            $arrSkipOnWeekday = array();
    	            $arrSkipOnWeekday = explode(",",$SkipOnWeekday);
    	            if(in_array($current_weekday,$arrSkipOnWeekday)){
    	                $ts_current += 86400;  # 1 Day
    	                $current_weekday = ($current_weekday+1)%7;
    	                continue;
    	            }
	            }
	        
	            # Put in Database
	            $recordDate = date("Y-m-d",$ts_current);
	            $dateToAddAry[] = $recordDate;
	            
	            # Next iteration
	            $current_weekday = ($current_weekday+1)%7;
	            $current_cycleday = ($current_cycleday+1)%$PeriodDays;
	            $ts_current += 86400;  # 1 Day
	        }
	        
	        if (count($dateToAddAry)) {
	            $this->insertEverydayDefaultTimetable($TimetableTemplate, $dateToAddAry);
	        }
		}

		private function updateEverydayTimetableRecord($newDateStart,$newDateEnd,$periodType,$periodDays,$firstDay,$skipOnWeekday,$newTimetableID,$oldDateStart,$oldDateEnd,$TimezoneID='')
		{
		    global $intranet_root, $intranet_session_language;
		    
		    include_once($intranet_root."/includes/libebooking.php");
		    $libebooking = new libebooking();
		    include_once($intranet_root."/includes/libtimetable.php");
		    $libtimetable = new Timetable();
		    
		    if ($periodType == 1) {         # Use Cycle Day
		        if ($periodDays < 1) return;
		    }
		    else {    // weekdays
		        $periodDays = 7;
		    }
		    
		    $dateStart = $newDateStart > $oldDateStart ? $oldDateStart : $newDateStart;     // choose early one (min)
		    $dateEnd = $newDateEnd > $oldDateEnd ? $newDateEnd : $oldDateEnd;               // choose later one (max)
		    $defaultTimetableAssoc = $this->getDefaultTimetableInfo($dateStart,$dateEnd);

		    $specialTimetableAry = $this->Get_Special_Timetable_Settings_Date_Info($SpecialTimetableSettingsIDArr='', $TimezoneID, $DateArr='', $RefreshPreload=0);
		    
		    # Get Non-Cycle Days
		    $non_cycle_days = array();
		    $nonCycleDateAry = $this->getNonCycleDays($dateStart,$dateEnd);
		    for ($j=0, $jMax=count($nonCycleDateAry); $j<$jMax; $j++) {
		        $ts = strtotime($nonCycleDateAry[$j]);
		        $non_cycle_days[$ts] = 1;
		    }
		    $ts_start = strtotime($dateStart);
		    $ts_end = strtotime($dateEnd);
		    $ts_current = $ts_start;

		    $current_weekday = date("w",$ts_current);
		    $current_cycleday = $firstDay;
		    $values = "";
		    $delim = "";
		    $result = array();
		    while ($ts_current <= $ts_end) {
		        $recordDate = date("Y-m-d",$ts_current);
		        
		        # Check is non-cycle day
		        if ($non_cycle_days[$ts_current]==1) { # Skip
		            $current_weekday = ($current_weekday+1)%7;
		            $ts_current += 86400;  # 1 Day
		            continue;
		        }
		        
		        // cycle days
		        if ($periodType == 1) {
		            # Check is Sunday or Saturday
		            if ($current_weekday==0 || $current_weekday==6) {
		                $ts_current += 86400;  # 1 Day
		                $current_weekday = ($current_weekday+1)%7;
		                continue;
		            }
		            
		            # Check is Skip on Weekday
		            $arrSkipOnWeekday = array();
		            $arrSkipOnWeekday = explode(",",$skipOnWeekday);
		            if(in_array($current_weekday,$arrSkipOnWeekday)){
		                $ts_current += 86400;  # 1 Day
		                $current_weekday = ($current_weekday+1)%7;
		                
		                // don't process if special timetable has been set as it has higher priority
		                if (isset($defaultTimetableAssoc[$recordDate]) && !isset($specialTimetableAry[$recordDate]['TimetableID'])) {
		                    if ($defaultTimetableAssoc[$recordDate]['TimetableID']) {     // everyday timetable has been set, just update DefaultTimetableID
		                        $result[] = $this->updateEverydayDefaultTimetableID($recordDate); // update DefaultTimetableID to 0 for skip date
		                    }
		                    else {
		                        $timeSlotIDAry = $this->getDefaultTimeSlotByDate($recordDate);
		                        if (count($timeSlotIDAry)) {
		                            foreach($timeSlotIDAry as $_timeSlotIDAry) {
		                                $_timeSlotID = $_timeSlotIDAry['TimeSlotID'];
		                                $result['rejectBooking_timeSlotID_'.$_timeSlotID] = $libebooking->rejectBookingByTimeSlot($_timeSlotID, $recordDate);
		                            }
		                        }
                                $result[] = $this->deleteEverydayDefaultTimetableID($recordDate,$defaultTimetableAssoc[$recordDate]['DefaultTimetableID']);
		                    }
		                }
		                continue;
		            }
		        }
		        
		        if (isset($defaultTimetableAssoc[$recordDate])) {
		            if ($recordDate < $newDateStart || $recordDate > $newDateEnd) { // timezone date range is smaller than original, treat as delete for less part even special timetable has been set
		                if ($defaultTimetableAssoc[$recordDate]['TimetableID']) {
		                    $result[] = $this->updateEverydayDefaultTimetableID($recordDate); // update DefaultTimetableID to 0 for skip date
		                }
		                else {
		                    $timeSlotIDAry = $this->getDefaultTimeSlotByDate($recordDate);
		                    if (count($timeSlotIDAry)) {
		                        foreach($timeSlotIDAry as $_timeSlotIDAry) {
		                            $_timeSlotID = $_timeSlotIDAry['TimeSlotID'];
		                            $result['rejectBooking_timeSlotID_'.$_timeSlotID] = $libebooking->rejectBookingByTimeSlot($_timeSlotID, $recordDate);
		                        }
		                    }
		                    $result[] = $this->deleteEverydayDefaultTimetableID($recordDate,$defaultTimetableAssoc[$recordDate]['DefaultTimetableID']);
		                }
		            }
		            else if ($defaultTimetableAssoc[$recordDate]['DefaultTimetableID'] != $newTimetableID) {
		                if (!$defaultTimetableAssoc[$recordDate]['TimetableID'] && !isset($specialTimetableAry[$recordDate]['TimetableID'])) {       // TimetableID exist or special timetable is set for the date, don't update as it has higher priority
		                    $result[] = $this->updateEverydayDefaultTimetableID($recordDate, $newTimetableID);
		                    $result['updateBookingTimeslot_'.$recordDate] = $libtimetable->updateBookingTimeslotByTimetable($recordDate, $defaultTimetableAssoc[$recordDate]['DefaultTimetableID'], $newTimetableID);
		                }
		            }
		        }
		        else {
		            $result[] = $this->insertEverydayDefaultTimetable($newTimetableID, $recordDate);
		        }
		        
		        # Next iteration
		        $current_weekday = ($current_weekday+1)%7;
		        $current_cycleday = ($current_cycleday+1)%$periodDays;
		        $ts_current += 86400;  # 1 Day
		    }
		    return in_array(false,$result) ? false : true;
		}
		
// 		private function rejecteBooking($timetableID)
//         {
//             $timetableID = IntegerSafe($timetableID);
//             $sql = "UPDATE 
//                             INTRANET_EBOOKING_BOOKING_DETAILS d
//                     INNER JOIN 
//                             INTRANET_EBOOKING_RECORD b ON b.BookingID=d.BookingID
//                     INNER JOIN
//                             INTRANET_TIMETABLE_TIMESLOT_RELATION r ON r.TimesSlotID=b.TimeSlotID
//                     SET 
//                             d.BookingStatus=-1, 
//                             d.PIC='" . $_SESSION['UserID'] . "', 
//                             d.ProcessDate=NOW(), 
//                             d.ProcessDateTime=NOW() 
//                     WHERE 
//                             r.TimetableID='".$timetableID."'";
//             $result = $this->db_db_query($sql);
//             return $result;
//         }

//         private function updateeBookingInfo($oldTimetableID, $newTimetableID)
//         {
//             $oldTimetableID = IntegerSafe($oldTimetableID);
//             $newTimetableID = IntegerSafe($newTimetableID);
//             $sql = "SELECT TimeSlotID FROM INTRANET_TIMETABLE_TIMESLOT_RELATION WHERE TimetableID='".$oldTimetableID."' ORDER BY TimeSlotID";
//             $timeslotIDAry = $this->returnResultSet($sql);
//             if (count($timeslotIDAry)) {
                
//             }
//             return $result;
//         }
        
        private function deleteEverydayDefaultTimetableID($recordDate,$defaultTimetableID)
        {
            $defaultTimetableID = IntegerSafe($defaultTimetableID);
            $cond = "RecordDate='".$this->Get_Safe_Sql_Query($recordDate)."' 
                    AND DefaultTimetableID='".$defaultTimetableID."'
                    AND TimetableID<>DefaultTimetableID
                    AND TimetableID=0";
            
            // delete records of which default timetable is set, no specific timetable
            $sql = "DELETE FROM 
                            INTRANET_SCHOOL_EVERYDAY_TIMETABLE 
                    WHERE ".$cond;
            $result = $this->db_db_query($sql);
            return $result;
        }
		
        private function insertEverydayDefaultTimetable($timetableID, $dateAry)
        {
            $_dateAry = array();
            if (!is_array($dateAry)) {
                $dateAry = array($dateAry);
            }
            
            foreach($dateAry as $_date) {
                $_dateAry[] = "('$_date','$timetableID',NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')";
            }
            $values = implode(",", $_dateAry);
            $sql = "INSERT IGNORE INTO INTRANET_SCHOOL_EVERYDAY_TIMETABLE (RecordDate, DefaultTimetableID, DateInput, InputBy, DateModified, ModifiedBy) VALUES $values";
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        private function updateEverydayDefaultTimetableID($recordDate, $timetableID=0)
        {
            $timetableID = IntegerSafe($timetableID);
            if (!is_array($recordDate)) {
                $recordDate = array($recordDate);
            }
            $recordDateAry = implode("','",$recordDate);
            
            $sql = "UPDATE INTRANET_SCHOOL_EVERYDAY_TIMETABLE SET DefaultTimetableID='".$timetableID."', DateModified=NOW(), ModifiedBy='".$_SESSION['UserID']."' WHERE RecordDate IN ('".$recordDateAry."')";
            
            $result = $this->db_db_query($sql);
            return $result;
        }
        
        public function getDefaultTimetableID($dateStart,$dateEnd)
        {
            $sql = "SELECT
                            RecordDate, 
                            DefaultTimetableID
                    FROM
                            INTRANET_SCHOOL_EVERYDAY_TIMETABLE
                    WHERE
                            RecordDate BETWEEN '".$this->Get_Safe_Sql_Query($dateStart)."' AND '".$this->Get_Safe_Sql_Query($dateEnd)."'
                            ORDER BY RecordDate";
            $timetableArr = $this->returnResultSet($sql);
            $timetableAssoc = BuildMultiKeyAssoc($timetableArr,array('RecordDate'),array('DefaultTimetableID'),1);
            return $timetableAssoc;
        }

        // contains DefaultTimetableID only, TimetableID not yet set 
        public function getDefaultTimetableIDByDate($dateAry)
        {
            $sql = "SELECT
                            RecordDate,
                            DefaultTimetableID
                    FROM
                            INTRANET_SCHOOL_EVERYDAY_TIMETABLE
                    WHERE
                            RecordDate IN ('".implode("','",$dateAry)."')
                            AND TimetableID=0
                            ORDER BY RecordDate";
            $timetableArr = $this->returnResultSet($sql);
            $timetableAssoc = BuildMultiKeyAssoc($timetableArr,array('RecordDate'),array('DefaultTimetableID'),1);
            return $timetableAssoc;
        }
        
        public function getDefaultTimetableInfo($dateStart,$dateEnd)
        {
            $sql = "SELECT
                            RecordDate,
                            DefaultTimetableID,
                            TimetableID
                    FROM
                            INTRANET_SCHOOL_EVERYDAY_TIMETABLE
                    WHERE
                            RecordDate BETWEEN '".$this->Get_Safe_Sql_Query($dateStart)."' AND '".$this->Get_Safe_Sql_Query($dateEnd)."'
                            ORDER BY RecordDate";
            $timetableArr = $this->returnResultSet($sql);
            $timetableAssoc = BuildMultiKeyAssoc($timetableArr,array('RecordDate'),array('DefaultTimetableID','TimetableID'));
            return $timetableAssoc;
        }
        
        private function getOriginalTimeTableIDBySpecialTimetableSettingsID($SpecialTimetableSettingsIDArr)
        {
            $sql = "SELECT
                                d.SpecialDate,
                                r.TimetableID,
                                s.TimetableID as SpecialTimetableID
                        FROM
                                INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS s
                        INNER JOIN
                                INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE d ON d.SpecialTimetableSettingsID=s.SpecialTimetableSettingsID
                        INNER JOIN
                                INTRANET_PERIOD_TIMETABLE_RELATION r ON r.PeriodID=s.TimezoneID
                
                        WHERE
                                s.SpecialTimetableSettingsID IN ('".implode("','",(array)$SpecialTimetableSettingsIDArr)."')";
            $timetableIDAry = $this->returnResultSet($sql);
            $timetableAssoc = BuildMultiKeyAssoc($timetableIDAry,array('SpecialDate'),array('TimetableID', 'SpecialTimetableID'));
            return $timetableAssoc;
        }
        
        private function getDateInEverydayTimetable($dateAry)
        {
            $sql = "SELECT RecordDate FROM INTRANET_SCHOOL_EVERYDAY_TIMETABLE WHERE RecordDate IN ('".implode("','",(array)$dateAry)."')";
            $recordDateAry = $this->returnResultSet($sql);
            $recordDateAssoc = BuildMultiKeyAssoc($recordDateAry,array('RecordDate'),array('RecordDate'),1);
            return $recordDateAssoc;
            
        }
        
        function getTimeSlotByDate($date)
        {
            $sql = "SELECT
                            s.TimeSlotID
                    FROM
                            INTRANET_TIMETABLE_TIMESLOT s
	                INNER JOIN
				            INTRANET_TIMETABLE_TIMESLOT_RELATION r ON (r.TimeSlotID = s.TimeSlotID)
	                INNER JOIN
				            INTRANET_SCHOOL_EVERYDAY_TIMETABLE t ON (IF(t.TimetableID>0,t.TimetableID,t.DefaultTimetableID)=r.TimetableID)
				    WHERE
				            t.RecordDate='".$date."'
                            ORDER BY s.StartTime";
            $timeSlotIDAry = $this->returnResultSet($sql);
            return $timeSlotIDAry;
        }

        function getDefaultTimeSlotByDate($date)
        {
            $sql = "SELECT
                            s.TimeSlotID
                    FROM
                            INTRANET_TIMETABLE_TIMESLOT s
	                INNER JOIN
				            INTRANET_TIMETABLE_TIMESLOT_RELATION r ON (r.TimeSlotID = s.TimeSlotID)
	                INNER JOIN
				            INTRANET_SCHOOL_EVERYDAY_TIMETABLE t ON t.DefaultTimetableID=r.TimetableID
				    WHERE
				            t.RecordDate='".$date."'
                            ORDER BY s.StartTime";
            $timeSlotIDAry = $this->returnResultSet($sql);
            return $timeSlotIDAry;
        }
        
        // priority: special time zone > regular timezone
        function getDefaultTimetableIDFromTimezone($date)
        {
            $timetableID = 0;
            $sql = "SELECT
                        s.TimetableID
                    FROM
                        INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS s
                    INNER JOIN
                        INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE d ON d.SpecialTimetableSettingsID=s.SpecialTimetableSettingsID
                    WHERE
                        d.SpecialDate='".$date."'";
            $specialTimetableIDAry = $this->returnResultSet($sql);
            if (count($specialTimetableIDAry)) {
                $timetableID = $specialTimetableIDAry[0]['TimetableID'];
            }
            else {
                $sql = "SELECT
                            r.TimetableID
                        FROM
                            INTRANET_PERIOD_TIMETABLE_RELATION r
                        INNER JOIN
                            INTRANET_CYCLE_GENERATION_PERIOD p ON p.PeriodID=r.PeriodID
                        WHERE
                            '".$date."' BETWEEN p.PeriodStart AND p.PeriodEnd
                            AND p.RecordStatus=1 LIMIT 1";
                $timetableIDAry = $this->returnResultSet($sql);
                if (count($timetableIDAry)) {
                    $timetableID = $timetableIDAry[0]['TimetableID'];
                }
            }
            
            return $timetableID;
        }
        
        function getTimeSlotByDefaultTimetable($date, $defaultTimeTableID)
        {
            $sql = "SELET
                        r.TimeSlotID
                    FROM
                        INTRANET_TIMETABLE_TIMESLOT_RELATION r
                    INNER JOIN
                        INTRANET_SCHOOL_EVERYDAY_TIMETABLE t ON t.DefaultTimetableID=r.TimetableID
                    WHERE
                        t.RecordDate='".$date."'
                        AND t.DefaultTimetableID='".$defaultTimeTableID."'
                        AND t.TimetableID=0
                        ORDER BY r.TimeSlotID";
            $timeSlotIDAry = $this->returnResultSet($sql);
            $timeSlotIDArr = Get_Array_By_Key($timeSlotIDAry, 'TimeSlotID');
            return $timeSlotIDArr;
        }
        
        
    }   // end class

} // End of directives
?>