<?
// using by kenenth chung
include_once("libdb.php");

class staff_special_template extends libdb {
	var $TemplateID;
	var $TemplateName;
	var $SlotList;
	var $DateAssociated;
	var $InvolvedInfo;
	
	function staff_special_template($TemplateID="",$GetSlotInfo=false,$GetDateInfo=false,$GetInvolvedInfo=false,$GetPastedDateInfo=false) {
		parent::libdb();
		if ($TemplateID != "") {
			$sql = 'select 
								TemplateName 
							From 
								CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE 
							where 
								TemplateID = \''.$TemplateID.'\'
								';
			$Result = $this->returnArray($sql);
			
			$this->TemplateID = $TemplateID;
			$this->TemplateName = $Result[0]['TemplateName'];
			
			if ($GetSlotInfo) {
				$sql = 'select 
									SlotID,
									SlotName,
									SlotStart,
									SlotEnd,
									DutyCount,
									InWavie,
									OutWavie 
								from 
									CARD_STAFF_ATTENDANCE3_SLOT 
								where 
									TemplateID = \''.$TemplateID.'\' 
								order by 
									SlotStart';
				$this->SlotList = $this->returnArray($sql);
			}
			
			if ($GetDateInfo) {
				$sql = 'select 
									TargetDate 
								from 
									CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION 
								where 
									TemplateID = \''.$TemplateID.'\' ';
				if ($GetPastedDateInfo) {
					$sql .= '
									and 
									TargetDate >= CURDATE()
									';
				}
				$sql .= '
								order by 
									TargetDate desc';
				$this->DateAssociated = $this->returnVector($sql);
			}
			
			if ($GetInvolvedInfo) {
				$sql = 'select 
									SlotID,
									ObjID,
									IF(GroupOrIndividual = \'G\',\'Group\',\'Individual\') as GroupOrIndividual
								from 
									CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET 
								where 
									TemplateID = \''.$TemplateID.'\'';
				$Temp = $this->returnArray($sql);
				
				for ($i=0; $i< sizeof($Temp); $i++) {
					$this->InvolvedInfo[$Temp[$i]['SlotID']][$Temp[$i]['GroupOrIndividual']][] = $Temp[$i]['ObjID'];
				}
			}
		}
	}
}
?>